unit pbInterfaces;

interface

uses isBaseClasses;

type
  IpbProduct = interface;
  IpbProject = interface;

  // List of Products
  IpbProducts = interface
  ['{9BF04482-80DD-485A-B598-B5DB34A283BB}']
    procedure Add(const aItem: IpbProduct);
    procedure Delete(const aItem: IpbProduct);
    function  Count: Integer;
    function  Find(const aName: String): IpbProduct;
    function  GetItem(Index: Integer): IpbProduct;
    property  Item[Index: Integer]: IpbProduct read GetItem; default;
  end;


  // Product descriptor
  IpbProduct = interface
  ['{78F2810B-CBF1-4744-BD7A-3729A6AFE87B}']
    function  Name: String;
    procedure AddProject(const aProject: IpbProject);
    procedure DeleteProject(const aProject: IpbProject);
    function  FindProject(const aName: String): IpbProject;
    function  ProjectCount: Integer;
    function  GetProject(Index: Integer): IpbProject;
    property  Projects[Index: Integer]: IpbProject read GetProject;
  end;


  // Version descriptor
  IpbVersion = interface
  ['{92638D4C-82FE-4A0D-BF5B-C6AE920F8B29}']
    function  Major: Word;
    function  Minor: Word;
    function  Patch: Word;
    function  Build: Word;
    function  NextBuild: IpbVersion;
    function  NextRelease: IpbVersion;
    function  NextHotFix: IpbVersion;
    function  AsString(const AFmtNbr: Integer = 0): String;
  end;


  // Project descriptor
  TpbProjectType = (ptUnknown, ptAbstract, ptDevelopment, ptFeature, ptHotfix, ptBinBuild, ptBinRelease);

  IpbProject = interface
  ['{28CDD991-E2DD-4091-9101-24FEBE3FBA8B}']
    function  Product: IpbProduct;
    function  Name: String;
    function  Version: IpbVersion;
    function  Description: String;
    function  ProjectType: TpbProjectType;
    function  Properties: IisListOfValues;
    function  GetPropValue(const APropName: String; const ADefault: String = #0): String;
  end;


  // Task
  TpbTaskType = (ttUnknown, ttBuild, ttReleaseBinProject, ttPublishBinProject,
                 ttDeleteBinProject, ttPrepareHotFix);

  IpbTask = interface
  ['{DB6B2816-08C4-42C0-9765-2580FE34A88E}']
    function  TaskType: TpbTaskType;
    function  Params: IisListOfValues;
    function  Caption: String;
    procedure SetProgressData(const AStatus, ADetails: String; const AProgress: Integer);
    function  GetExecuting: Boolean;
    procedure SetExecuting(const AValue: Boolean);
    function  ProgressStatus: String;
    function  ProgressDetails: String;
    function  Progress: Integer;
    property  Executing: Boolean read GetExecuting write SetExecuting;
  end;

  
  // Issue tracking
  IpbIssueTracking = interface
  ['{2E527AA0-E461-4887-9C60-C87E7E0C626E}']
    function  GetUserList: IisListOfValues;
    function  GetIssues(const AQuery: String): IisParamsCollection;
    function  AddIssue(const AIssue: IisListOfValues): IisListOfValues;
    procedure UpdateIssue(const AIssue: IisListOfValues);
    function  StreamExists(const AName: String): Boolean;
    procedure CreateStream(const AName: String; const AParentName: String);
    procedure MoveStream(const AName: String; const AParentName: String);
    procedure DeleteStream(const AName: String);
  end;


  // Project Builder
  TpbUserRole = (urUnknown, urDeveloper, urQATester, urQAManager, urAdministrator);

  IpbUserInfo = interface
  ['{80EE7DB1-5AC5-4965-973A-137FDCF2C592}']
    function User: String;
    function Password: String;    
    function EMail: String;
    function Role: TpbUserRole;
    function AllowedProducts: IisStringList;
  end;

  IpbProjectBuilder = interface
  ['{C12046E2-E430-4025-9401-65E4B99290F8}']
    function  CurrentUserInfo: IpbUserInfo;
    procedure ChangeUserPassword(const AOldPassword, ANewPassword: String);
    function  GetProducts: IpbProducts;
    procedure UpdateProjects(const AProduct: IpbProduct);
    function  GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;
    procedure RunTask(const ATask: IpbTask);
    function  GetCurrentTask: IpbTask;
    function  IssueTracking: IpbIssueTracking;
  end;

implementation

end.
