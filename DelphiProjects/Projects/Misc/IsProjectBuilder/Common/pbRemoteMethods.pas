unit pbRemoteMethods;

interface

const
  METHOD_RESULT                         = 'Result';

  METHOD_CURRENTUSERINFO                = 'CurrentUserInfo';
  METHOD_CHANGEUSERPASSWORD             = 'ChangeUserPassword';

  METHOD_GETPRODUCTS                    = 'GetProducts';
  METHOD_UPDATEPROJECTS                 = 'UpdateProjects';
  METHOD_GETUPDATEDISSUES               = 'GetUpdatedIssues';

  METHOD_RUNTASK                        = 'RunTask';
  METHOD_GETCURRENTTASK                 = 'GetCurrentTask';

  METHOD_ISSUETRACKING                  = 'IssueTracking';
  METHOD_ISSUETRACKING_GETUSERLIST      = METHOD_ISSUETRACKING + '.GetUserList';
  METHOD_ISSUETRACKING_GETISSUES        = METHOD_ISSUETRACKING + '.GetIssues';
  METHOD_ISSUETRACKING_ADDISSUE         = METHOD_ISSUETRACKING + '.AddIssue';
  METHOD_ISSUETRACKING_UPDATEISSUE      = METHOD_ISSUETRACKING + '.UpdateIssue';
  METHOD_ISSUETRACKING_STREAMEXISTS     = METHOD_ISSUETRACKING + '.StreamExists';
  METHOD_ISSUETRACKING_CREATESTREAM     = METHOD_ISSUETRACKING + '.CreateStream';
  METHOD_ISSUETRACKING_MOVESTREAM       = METHOD_ISSUETRACKING + '.MoveStream';
  METHOD_ISSUETRACKING_DELETESTREAM     = METHOD_ISSUETRACKING + '.DeleteStream';

implementation

end.