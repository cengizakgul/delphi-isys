unit pbClasses;

interface

uses SysUtils, isBaseClasses, pbInterfaces, EvStreamUtils, isBasicUtils;

type
  TpbProducts = class(TisCollection, IpbProducts)
  protected
    procedure Add(const aItem: IpbProduct);
    procedure Delete(const aItem: IpbProduct);
    function  Count: Integer;
    function  Find(const aName: String): IpbProduct;
    function  GetItem(Index: Integer): IpbProduct;
  public
    class function GetTypeID: String; override;
  end;


  TpbProduct = class(TisCollection, IpbProduct)
  protected
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function  Name: String;
    procedure AddProject(const aProject: IpbProject);
    procedure DeleteProject(const aProject: IpbProject);
    function  FindProject(const aName: String): IpbProject;
    function  ProjectCount: Integer;
    function  GetProject(Index: Integer): IpbProject;
  public
    constructor Create(const aAppName: String); reintroduce;
    class function GetTypeID: String; override;
  end;


  TpbVersion = class(TisInterfacedObject, IpbVersion)
  private
    FVersion: String;
    function GetVersionPart(const aNbr: Integer): Word;
  protected
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function Major: Word;
    function Minor: Word;
    function Patch: Word;
    function Build: Word;
    function NextBuild: IpbVersion;
    function NextRelease: IpbVersion;
    function NextHotFix: IpbVersion;
    function AsString(const AFmtNbr: Integer = 0): String;
  public
    constructor Create(const aVersion: String); reintroduce;
    class function GetTypeID: String; override;
  end;


  TpbProject = class(TisInterfacedObject, IpbProject)
  private
    FProperties: IisListOfValues;
    FProjectType: TpbProjectType;
    function  SubstParams(const AValue: String): String;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    function  ParamToValue(const AParamName: String): String; virtual;
    procedure SetProjectType(const AValue: TpbProjectType);

    function  Product: IpbProduct;
    function  Name: String;
    function  Version: IpbVersion;
    function  Description: String;
    function  ProjectType: TpbProjectType;
    function  Properties: IisListOfValues;
    function  GetPropValue(const APropName: String; const ADefault: String = #0): String;
  public
    constructor Create(const aProjectType: TpbProjectType); reintroduce;
    class function GetTypeID: String; override;
  end;


  TpbTask = class(TisInterfacedObject, IpbTask)
  private
    FTaskType: TpbTaskType;
    FParams: IisListOfValues;
    FCaption: String;
    FProgressStatus: String;
    FProgressDetails: String;
    FProgress: Integer;
    FExecuting: Boolean;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function  TaskType: TpbTaskType;
    function  Params: IisListOfValues;
    function  Caption: String;
    procedure SetProgressData(const AStatus, ADetails: String; const AProgress: Integer);
    function  ProgressStatus: String;
    function  ProgressDetails: String;
    function  Progress: Integer;
    function  GetExecuting: Boolean;
    procedure SetExecuting(const AValue: Boolean);
  public
    constructor Create(const aTaskType: TpbTaskType; const aCaption: String); reintroduce;
    class function GetTypeID: String; override;
  end;


  TpbUserInfo = class(TisInterfacedObject, IpbUserInfo)
  private
    FUser: String;
    FPassword: String;
    FEMail: String;
    FRole: TpbUserRole;
    FAllowedProducts: IisStringList;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function User: String;
    function Password: String;
    function EMail: String;
    function Role: TpbUserRole;
    function AllowedProducts: IisStringList;
  public
    constructor Create(const aUser, aPassword, aEMail: String; const aRole: TpbUserRole;
                       const aAllowedProducts: String); reintroduce;
    class function GetTypeID: String; override;
  end;

implementation

{ TpbProduct }

function TpbProduct.Name: String;
begin
  Result := GetName;
end;

constructor TpbProduct.Create(const aAppName: String);
begin
  SetName(aAppName);
  inherited Create;
end;

class function TpbProduct.GetTypeID: String;
begin
  Result := 'pbProduct';
end;

procedure TpbProduct.ReadSelfFromStream(const AStream: IisStream;
  const ARevision: TisRevision);
begin
  inherited;
  SetName(AStream.ReadShortString);
end;

procedure TpbProduct.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteShortString(Name);
end;

procedure TpbProduct.AddProject(const aProject: IpbProject);
begin
  AddChild(aProject as IisInterfacedObject);
end;

function TpbProduct.GetProject(Index: Integer): IpbProject;
begin
  Result := GetChild(Index) as IpbProject;
end;

function TpbProduct.ProjectCount: Integer;
begin
  Result := ChildCount;
end;

procedure TpbProduct.DeleteProject(const aProject: IpbProject);
begin
  RemoveChild(aProject as IisInterfacedObject);
end;

function TpbProduct.FindProject(const aName: String): IpbProject;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to ProjectCount - 1 do
    if AnsiSameText(GetProject(i).Name, aName) then
    begin
      Result := GetProject(i);
      break;
    end;
end;

{ TpbVersion }

function TpbVersion.AsString(const AFmtNbr: Integer = 0): String;
var
  s: String;
begin
  if AFmtNbr = 0 then
    Result := FVersion
  else
  begin
    s := StringOfChar('0', AFmtNbr);
    Result := FormatFloat(s, Major) + '.' + FormatFloat(s, Minor) + '.' + FormatFloat(s, Patch) + '.' + FormatFloat(s, Build);
  end;
end;

function TpbVersion.Build: Word;
begin
  Result := GetVersionPart(4);
end;

constructor TpbVersion.Create(const aVersion: String);
begin
  inherited Create;
  FVersion := aVersion;
  FVersion := IntToStr(Major) + '.' + IntToStr(Minor) + '.' + IntToStr(Patch) + '.' + IntToStr(Build);
end;

class function TpbVersion.GetTypeID: String;
begin
  Result := 'pbVersion';
end;

function TpbVersion.GetVersionPart(const aNbr: Integer): Word;
var
  i:Integer;
  s: String;
begin
  Result := 0;
  s := FVersion;
  for i := 1 to aNbr do
    Result := StrToIntDef(GetNextStrValue(s, '.'), 0);
end;

function TpbVersion.Major: Word;
begin
  Result := GetVersionPart(1);
end;

function TpbVersion.Minor: Word;
begin
  Result := GetVersionPart(2);
end;

function TpbVersion.NextBuild: IpbVersion;
begin
  Result := TpbVersion.Create(Format('%d.%d.%d.%d', [Major, Minor, Patch, Build + 1]));
end;

function TpbVersion.NextHotFix: IpbVersion;
begin
  Result := TpbVersion.Create(Format('%d.%d.%d.%d', [Major, Minor, Patch + 1, 1]));
end;

function TpbVersion.NextRelease: IpbVersion;
begin
  Result := TpbVersion.Create(Format('%d.%d.%d.%d', [Major, Minor + 1, 0, 1]));
end;

function TpbVersion.Patch: Word;
begin
  Result := GetVersionPart(3);
end;

procedure TpbVersion.ReadSelfFromStream(const AStream: IisStream;
  const ARevision: TisRevision);
begin
  inherited;
  FVersion := AStream.ReadShortString;
end;

procedure TpbVersion.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteShortString(FVersion);
end;

{ TpbProject }

function TpbProject.Product: IpbProduct;
begin
  Result := GetOwner as IpbProduct;
end;

constructor TpbProject.Create(const aProjectType: TpbProjectType);
begin
  inherited Create;
  SetProjectType(aProjectType);
end;

function TpbProject.Description: String;
begin
  Result := GetPropValue('Description', '');
end;

procedure TpbProject.DoOnConstruction;
begin
  inherited;
  FProperties := TisListOfValues.Create;
end;

class function TpbProject.GetTypeID: String;
begin
  Result := 'pbProject';
end;

function TpbProject.Name: String;
begin
  Result := GetPropValue('Name', '');
end;

procedure TpbProject.ReadSelfFromStream(const AStream: IisStream;
  const ARevision: TisRevision);
begin
  inherited;
  FProjectType := TpbProjectType(AStream.ReadByte);  
  AStream.ReadObject(FProperties);  
end;

function TpbProject.Version: IpbVersion;
begin
  Result := TpbVersion.Create(GetPropValue('Version', ''));
end;

procedure TpbProject.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteByte(Ord(FProjectType));
  AStream.WriteObject(FProperties);
end;

function TpbProject.Properties: IisListOfValues;
begin
  Result := FProperties;
end;

function TpbProject.GetPropValue(const APropName: String; const ADefault: String): String;
var
  s: String;
begin
  if ADefault = #0 then
    s := Properties.Value[APropName]
  else
    s := Properties.TryGetValue(APropName, ADefault);

  Result := SubstParams(s);
end;

function TpbProject.SubstParams(const AValue: String): String;
var
 Param, s, val: String;
begin
  Result := '';
  s := AValue;
  while s <> '' do
  begin
    Result := Result + GetNextStrValue(s, '[');
    Param := GetNextStrValue(s, ']');
    if Param <> '' then
    begin
      val := ParamToValue(Param);
      if val = Param then
        val := '[' +  Param + ']';
      Result := Result + val;
    end;
  end;
end;

function TpbProject.ParamToValue(const AParamName: String): String;
begin
  if Properties.ValueExists(AParamName) then
    Result := GetPropValue(AParamName)
  else
    Result := AParamName;
end;

function TpbProject.ProjectType: TpbProjectType;
begin
  Result := FProjectType;
end;

procedure TpbProject.SetProjectType(const AValue: TpbProjectType);
begin
  FProjectType := AValue;
end;

{ TpbProducts }

procedure TpbProducts.Add(const aItem: IpbProduct);
begin
  AddChild(aItem as IisInterfacedObject);
end;

function TpbProducts.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TpbProducts.Delete(const aItem: IpbProduct);
begin
  RemoveChild(aItem as IisInterfacedObject);
end;

function TpbProducts.Find(const aName: String): IpbProduct;
begin
  Result := FindChildByName(aName) as IpbProduct;
end;

function TpbProducts.GetItem(Index: Integer): IpbProduct;
begin
  Result := GetChild(Index) as IpbProduct;
end;

class function TpbProducts.GetTypeID: String;
begin
  Result := 'pbProducts';
end;

{ TpbTask }

function TpbTask.Caption: String;
begin
  Result := FCaption;
end;

constructor TpbTask.Create(const aTaskType: TpbTaskType; const aCaption: String);
begin
  inherited Create(nil, True);
  FTaskType := aTaskType;
  FCaption := aCaption;
  FParams := TisListOfValues.Create;
end;

procedure TpbTask.DoOnConstruction;
begin
  inherited;
  FParams := TisListOfValues.Create;
end;

function TpbTask.GetExecuting: Boolean;
begin
  Result := FExecuting;
end;

class function TpbTask.GetTypeID: String;
begin
  Result := 'pbTask';
end;

function TpbTask.Params: IisListOfValues;
begin
  Result := FParams;
end;

function TpbTask.Progress: Integer;
begin
  Result := FProgress;
end;

function TpbTask.ProgressDetails: String;
begin
  Result := FProgressDetails;
end;

function TpbTask.ProgressStatus: String;
begin
  Result := FProgressStatus;
end;

procedure TpbTask.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
begin
  inherited;
  FTaskType := TpbTaskType(AStream.ReadByte);
  FParams.AsStream := AStream.ReadStream(nil);
  FCaption := AStream.ReadShortString;
  FExecuting := AStream.ReadBoolean;  
  FProgressStatus := AStream.ReadString;
  FProgressDetails := AStream.ReadString;
  FProgress := AStream.ReadInteger;
end;

procedure TpbTask.SetExecuting(const AValue: Boolean);
begin
  FExecuting := AValue; 
end;

procedure TpbTask.SetProgressData(const AStatus, ADetails: String; const AProgress: Integer);
begin
  Lock;
  try
    FProgressStatus := AStatus;
    FProgressDetails := ADetails;
    FProgress := AProgress;
  finally
    Unlock;
  end;
end;

function TpbTask.TaskType: TpbTaskType;
begin
  Result := FTaskType;
end;

procedure TpbTask.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteByte(Ord(FTaskType));
  AStream.WriteStream(FParams.AsStream);
  AStream.WriteShortString(FCaption);
  AStream.WriteBoolean(FExecuting);
  AStream.WriteString(FProgressStatus);
  AStream.WriteString(FProgressDetails);
  AStream.WriteInteger(FProgress);
end;

{ TpbUserInfo }

function TpbUserInfo.AllowedProducts: IisStringList;
begin
  Result := FAllowedProducts;
end;

constructor TpbUserInfo.Create(const aUser, aPassword, aEMail: String;
  const aRole: TpbUserRole; const aAllowedProducts: String);
begin
  inherited Create;
  FUser := aUser;
  FPassword := aPassword;
  FEMail := aEMail;
  FRole := aRole;
  FAllowedProducts.CommaText := aAllowedProducts;
end;

procedure TpbUserInfo.DoOnConstruction;
begin
  inherited;
  FAllowedProducts := TisStringList.Create;
  FAllowedProducts.CaseSensitive := False;
end;

function TpbUserInfo.EMail: String;
begin
  Result := FEMail;
end;

class function TpbUserInfo.GetTypeID: String;
begin
  Result := 'pbUserInfo';
end;

function TpbUserInfo.Password: String;
begin
  Result := FPassword;
end;

procedure TpbUserInfo.ReadSelfFromStream(const AStream: IisStream;
  const ARevision: TisRevision);
begin
  inherited;
  FUser := AStream.ReadShortString;
  FPassword := AStream.ReadShortString;
  FEMail := AStream.ReadString;
  FRole := TpbUserRole(AStream.ReadByte);
  FAllowedProducts.Text := AStream.ReadString;
end;

function TpbUserInfo.Role: TpbUserRole;
begin
  Result := FRole;
end;

function TpbUserInfo.User: String;
begin
  Result := FUser;
end;

procedure TpbUserInfo.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteShortString(FUser);
  AStream.WriteShortString(FPassword);  
  AStream.WriteString(FEMail);
  AStream.WriteByte(Ord(FRole));
  AStream.WriteString(FAllowedProducts.Text);
end;

initialization
  ObjectFactory.Register([TpbProducts, TpbProduct, TpbVersion, TpbProject, TpbTask, TpbUserInfo]);

finalization
  ObjectFactory.UnRegister([TpbProducts, TpbProduct, TpbVersion, TpbProject, TpbTask, TpbUserInfo]);

end.
