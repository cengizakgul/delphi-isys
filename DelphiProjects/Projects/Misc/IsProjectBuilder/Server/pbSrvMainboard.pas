unit pbSrvMainboard;

interface

uses SysUtils, isBaseClasses, pbInterfaces, isThreadManager, isSettings, isBasicUtils,
     isLogFile, isSMTPClient, pbServer, pbProjectBuilder, pbALMCompleteWatchDog;

type
  IpbSrvMainboard = interface
  ['{130292F1-3309-4CFD-A63F-BCC1242DBDE2}']
    procedure Initialize;
    procedure Finalize;
    function  LogFile: ILogFile;
    function  Config: IisSettings;
    function  Settings: IisSettings;    
    function  TCPServer: IpbTCPServer;
    procedure SendEMail(const aTo, aSubject, aBody: String);
    function  ProjectBuilder: IpbProjectBuilderExt;
  end;

  function  Mainboard: IpbSrvMainboard;
  procedure ActivateMainboard;
  procedure DeactivateMainboard;

implementation

type
  TpbSrvMainboard = class(TisInterfacedObject, IpbSrvMainboard)
  private
    FLogFile: ILogFile;
    FConfig: IisSettings;
    FTCPServer: IpbTCPServer;
    FProjectBuilder: IpbProjectBuilderExt;
    FSettings: IisSettings;
    FALMCompleteWatchdog: IpbALMCompleteWatchdog;
  protected
    procedure  DoOnConstruction; override;
    procedure  DoOnDestruction; override;

    procedure Initialize;
    procedure Finalize;
    function  LogFile: ILogFile;
    function  Config: IisSettings;
    function  Settings: IisSettings;
    function  TCPServer: IpbTCPServer;
    procedure SendEMail(const aTo, aSubject, aBody: String);
    function  ProjectBuilder: IpbProjectBuilderExt;
  end;

var
  vMainboard: IpbSrvMainboard;

function  Mainboard: IpbSrvMainboard;
begin
  if not Assigned(vMainboard) then
    ActivateMainboard;
  Result := vMainboard;
end;

procedure ActivateMainboard;
begin
  if not Assigned(vMainboard) then
    vMainboard := TpbSrvMainboard.Create;
end;

procedure DeactivateMainboard;
begin
  vMainboard := nil;
end;


{ TpbSrvMainboard }

procedure TpbSrvMainboard.DoOnConstruction;
begin
  inherited;
  FLogFile := TLogFile.Create(ChangeFileExt(AppFileName, '.log'));
  SetGlobalLogFile(FLogFile);

  FSettings := TisSettingsINI.Create(ChangeFileExt(AppFileName, '.ini'));
  FConfig := TisSettingsXML.Create(AppDir + 'pbConfig.xml');
  FTCPServer := CreateTCPServer;
end;

procedure TpbSrvMainboard.Finalize;
begin
  FTCPServer.Active := False;
//  FALMCompleteWatchdog.Active := False;

  GlobalThreadManager.TerminateTask;
  GlobalThreadManager.WaitForTaskEnd;

  FALMCompleteWatchdog := nil;
  FProjectBuilder := nil;

  FLogFile.AddEvent(etInformation, 'Diagnostic', 'Service stopped', '');
end;

procedure TpbSrvMainboard.Initialize;
begin
  FProjectBuilder := CreateProjectBuilder;
//  FALMCompleteWatchdog := CreateALMCompleteWatchdog;

  FTCPServer.Active := True;
//  FALMCompleteWatchdog.Active := True;

  FLogFile.AddEvent(etInformation, 'Diagnostic', 'Service started', '');
end;

function TpbSrvMainboard.Config: IisSettings;
begin
  Result := FConfig;
end;

function TpbSrvMainboard.LogFile: ILogFile;
begin
  Result := FLogFile;
end;

procedure TpbSrvMainboard.DoOnDestruction;
begin
  SetGlobalLogFile(nil);
  FLogFile := nil;
  inherited;
end;

procedure TpbSrvMainboard.SendEMail(const aTo, aSubject, aBody: String);
var
  Client: TIsSMTPClient;   
  sFrom: String;
begin
  try
    sFrom := Format('Project Builder <ProjectBuilder@%s.com>', [GetComputerNameString]);

    Client := TIsSMTPClient.Create(
      Mainboard.Config.AsString['Config\SMTPHost'],
      Mainboard.Config.GetValue('Config\SMTPPort', 25),
      Mainboard.Config.AsString['Config\SMTPUser'],
      Mainboard.Config.AsString['Config\SMTPPassword'],
      atAutoSelect);
    try
      Client.SendQuickMessage(sFrom, aTo, aSubject, aBody);
    finally
      Client.Free;
    end;
  except
    on E: Exception do
      Mainboard.LogFile.AddEvent(etError, 'Emailer', E.Message, '');
  end;
end;

function TpbSrvMainboard.TCPServer: IpbTCPServer;
begin
  Result := FTCPServer;
end;

function TpbSrvMainboard.ProjectBuilder: IpbProjectBuilderExt;
begin
  Result := FProjectBuilder;
end;

function TpbSrvMainboard.Settings: IisSettings;
begin
  Result := FSettings;
end;

end.
