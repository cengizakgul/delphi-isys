unit pbBuilder;

interface

uses SysUtils, Windows, isBaseClasses, pbInterfaces, isBasicUtils, EvStreamUtils, isSettings,
     isLogFile, IsErrorUtils, pbUtils;

type
  IpbBuilder = interface
  ['{29561D3F-916F-4D0D-AA9D-10DF3D7DD044}']
    procedure Build(const AProject: IpbProject; const ABuildDescription: String);
    function  Status: String;
    function  Details: String;
  end;

  function  CreateBuilder: IpbBuilder;

implementation

uses pbSrvMainboard;

type
  TpbBuilder = class(TpbProcessExec, IpbBuilder)
  private
    FProject: IpbProject;
    FBuildDescription: String;
    procedure DoRun;
    procedure SendEMail(const aSuccsess: Boolean; const aBody: String);
  protected
    procedure Build(const AProject: IpbProject; const ABuildDescription: String);
  end;

function CreateBuilder: IpbBuilder;
begin
  Result := TpbBuilder.Create;
end;


{ TpbBuilder }

procedure TpbBuilder.Build(const AProject: IpbProject; const ABuildDescription: String);
begin
  FProject := AProject;
  FBuildDescription := ABuildDescription;
  try
    try
      // Check if there are untested issues
      if FProject.Properties.ValueExists('Issues') then
      begin
        Mainboard.ProjectBuilder.Repository.CheckIssueStatus(
          Mainboard.ProjectBuilder.Repository.GetUpdatedIssues(AProject),
          [], ['Code Complete - Not checked in']);
      end;

      UpdateProgress('*Receiving the source code');
      Mainboard.ProjectBuilder.Repository.CheckOutProject(FProject);

      UpdateProgress('*Building the project');
      DoRun;

      UpdateProgress('*Copying binaries to repository');
      Mainboard.ProjectBuilder.Repository.CheckInBinaries(FProject);

      SendEMail(True, '');
    except
      on E: Exception do
      begin
        SendEMail(False, 'Error details:' + #13#13 + E.Message);
        Mainboard.LogFile.AddEvent(etError, 'Builder', 'Build process has failed', BuildStackedErrorStr(E));
        raise;
      end;
    end;
  finally
    FProject := nil;
  end;
end;

procedure TpbBuilder.DoRun;
var
  BuildInfo: IisSettings;
  Issues: IisParamsCollection;
  Issue: IisListOfValues;
  i, j: Integer;
  sIssueList, sIssueNbr: String;
begin
  // Run command
  Run(FProject.GetPropValue('BuildCmd'), FProject.GetPropValue('BuildCmdParams'));
  FProject.Properties.AddValue('BuildTime', Now);

  // Create build info file
  BuildInfo := TisSettingsIni.Create(NormalizePath(FProject.GetPropValue('BinDir')) + 'BuildInfo.ini');
  BuildInfo.AsString['Project\BaseProject'] := FProject.Name;
  BuildInfo.AsString['Project\Stream'] := FProject.GetPropValue('Stream');
  if FProject.Properties.ValueExists('CheckOutTime') then
    BuildInfo.AsString['Project\CheckOutTime'] := FProject.Properties.Value['CheckOutTime'];
  BuildInfo.AsString['Project\BuildTime'] := FProject.Properties.Value['BuildTime'];
  BuildInfo.AsString['Project\Version'] := FProject.Version.AsString;
  BuildInfo.AsString['Project\Description'] := FBuildDescription;

  // Add Issue info
  if FProject.Properties.ValueExists('Issues') then
  begin
    Issues := IInterface(FProject.Properties.Value['Issues']) as IisParamsCollection;
    sIssueList := '';
    for i := 0 to Issues.Count - 1 do
    begin
      sIssueNbr := Issues.ParamName(i);
      Issue := Issues[i];
      AddStrValue(sIssueList, sIssueNbr, ',');

      // Add issue info
      for j := 0 to Issue.Count - 1 do
        BuildInfo.AsString['Issue_' + sIssueNbr + '\' + Issue[j].Name] := Issue[j].Value;
    end;
    BuildInfo.AsString['Project\Issues'] := sIssueList;
  end;
end;

procedure TpbBuilder.SendEMail(const aSuccsess: Boolean; const aBody: String);
var
  sTo, sSubj, sBodyHeader, sStatus: String;
begin
  if aSuccsess then
  begin
    sSubj := Format('%s %s built successfully', [FProject.Product.Name, FProject.Version.AsString]);
    sStatus := 'Finished successfully'
  end
  else
  begin
    sSubj := Format('%s %s build failed', [FProject.Product.Name, FProject.Version.AsString]);
    sStatus := 'Finished with errors';
  end;

  sTo := FProject.GetPropValue('BuildEmail');
  if sTo <> '' then
  begin
    sBodyHeader := Format('Product:'#9'%s'#13'Build type:'#9'%s'#13'Version:'#9'%s'#13'Status:'#9'%s',
      [FProject.Product.Name, FProject.Name, FProject.Version.AsString, sStatus]);

    Mainboard.SendEMail(sTo, sSubj, sBodyHeader + #13#13 + aBody);
  end;
end;

end.
