unit pbProjectBuilder;

interface

uses SysUtils, isBaseClasses, pbInterfaces, isBasicUtils, isThreadManager, pbRepository,
     isUtils, pbClasses, SimpleXML, SEncryptionRoutines;

type
  IpbProjectBuilderExt = interface(IpbProjectBuilder)
  ['{A7A8F1F7-0A6A-472D-96E8-8662E98C2A03}']
    function  FindUser(const AUser: String): IpbUserInfo;
    procedure SetCurrentUser(const AUserInfo: IpbUserInfo);
    function  Repository: IpbRepository;
    procedure OnTaskStatusChange(const AStatusLine: String; const AProgress: Integer);
    procedure OnTaskDetailChange(const ADetailLine: String);
  end;

function CreateProjectBuilder: IpbProjectBuilderExt;

implementation

uses pbBuilder, pbSrvMainboard;

type
  TpbProjectBuilder = class(TisInterfacedObject, IpbProjectBuilderExt)
  private
    FRepository: IpbRepository;
    FIssueTracking: IpbIssueTracking;
    FBuilder: IpbBuilder;
    FCurrentTask: IpbTask;
    FTaskID: TTaskID;
    FUsers: IisStringList;

    procedure BuildUserList;

    procedure SendEMail(const aTaskName: String; const aSuccsess: Boolean; const aBody: String);
    procedure TaskThread(const Params: TTaskParamList);
    function  GetProjectFormTaskParams: IpbProject;
    procedure TskBuild;
    procedure TskReleaseBinProject;
    procedure TskPublishBinProject;
    procedure TskDeleteBinProject;
    procedure TskPrepareHotFix;

  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;

    function  CurrentUserInfo: IpbUserInfo;
    procedure ChangeUserPassword(const AOldPassword, ANewPassword: String);

    function  GetProducts: IpbProducts;
    procedure UpdateProjects(const AProduct: IpbProduct);
    function  GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;

    procedure RunTask(const ATask: IpbTask);
    function  GetCurrentTask: IpbTask;

    function  IssueTracking: IpbIssueTracking;

    function  FindUser(const AUser: String): IpbUserInfo;
    procedure SetCurrentUser(const AUserInfo: IpbUserInfo);
    function  Repository: IpbRepository;
    procedure OnTaskStatusChange(const AStatusLine: String; const AProgress: Integer);
    procedure OnTaskDetailChange(const ADetailLine: String);
  end;

threadvar
  FCurrentUserInfo: IpbUserInfo;

function CreateProjectBuilder: IpbProjectBuilderExt;
begin
  Result := TpbProjectBuilder.Create(nil, True);
end;


{ TpbProjectBuilder }

procedure TpbProjectBuilder.TskBuild;
var
  Ver: String;
  Product: IpbProduct;
  Project: IpbProject;
  i: Integer;
begin
  Project := GetProjectFormTaskParams;

  if FCurrentTask.Params.ValueExists('Version') then
    Project.Properties.Value['Version'] := FCurrentTask.Params.Value['Version'];

  FBuilder.Build(Project, FCurrentTask.Params.TryGetValue('Description', ''));

  if FCurrentTask.Params.TryGetValue('Publish', False) then
  begin
    Product := Project.Product;
    Ver := Project.Version.AsString;
    Project := nil;
    Repository.UpdateProjects(Product);

    for i := 0 to Product.ProjectCount - 1 do
    begin
      if (Product.Projects[i].ProjectType = ptBinBuild) and (Product.Projects[i].Version.AsString = Ver) then
      begin
        Project := Product.Projects[i];
        break;
      end;
    end;

    if Assigned(Project) then
      Repository.PublishBinProject(Project)
    else
      raise Exception.Create('Cannot find the build: ' + Product.Name + ' ' + Ver);
  end;
end;

procedure TpbProjectBuilder.DoOnConstruction;
begin
  inherited;
  BuildUserList;
  FBuilder := CreateBuilder;
  FRepository := CreateRepository;
  FIssueTracking := FRepository as IpbIssueTracking;
end;

procedure TpbProjectBuilder.DoOnDestruction;
begin
  if FTaskID <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FTaskID);
    GlobalThreadManager.WaitForTaskEnd(FTaskID);
  end;

  inherited;
end;

function TpbProjectBuilder.GetCurrentTask: IpbTask;
begin
  Lock;
  try
    Result := FCurrentTask;
  finally
    Unlock;
  end;
end;

function TpbProjectBuilder.GetProducts: IpbProducts;
var
  i: Integer;
begin
  Result := (Repository.GetProducts as IisInterfacedObject).GetClone as IpbProducts;

  if CurrentUserInfo.AllowedProducts.IndexOf('*') = -1 then
    for i := Result.Count - 1 downto 0 do
      if CurrentUserInfo.AllowedProducts.IndexOf(Result[i].Name) = -1 then
        Result.Delete(Result[i]);
end;

procedure TpbProjectBuilder.OnTaskDetailChange(const ADetailLine: String);
var
  s: String;
begin
  if Assigned(FCurrentTask) then
  begin
    s := FCurrentTask.ProgressDetails;
    AddStrValue(s, FormatDateTime('hh:nn:ss', Now) + '  ' + ADetailLine, #13);
    FCurrentTask.SetProgressData(FCurrentTask.ProgressStatus, s, FCurrentTask.Progress);
  end;
end;

procedure TpbProjectBuilder.OnTaskStatusChange(const AStatusLine: String; const AProgress: Integer);
begin
  if Assigned(FCurrentTask) then
  begin
    FCurrentTask.SetProgressData(AStatusLine, FCurrentTask.ProgressDetails, AProgress);
    OnTaskDetailChange('Status: '+ AStatusLine);
  end;
end;

function TpbProjectBuilder.Repository: IpbRepository;
begin
  Result := FRepository;
end;

procedure TpbProjectBuilder.RunTask(const ATask: IpbTask);
begin
  Lock;
  try
    if Assigned(FCurrentTask) and FCurrentTask.Executing then
      raise Exception.Create('A task is already executing. Please try again later.');

    FCurrentTask := ATask;
    FTaskID := 0;

    if Assigned(FCurrentTask) then
    begin
      FCurrentTask.Executing := True;
      FCurrentTask.Params.AddValue('User', CurrentUserInfo.User);
      FTaskID := GlobalThreadManager.RunTask(TaskThread, Self, MakeTaskParams([]), 'Task');
    end;
  finally
    Unlock;
  end;
end;

procedure TpbProjectBuilder.TaskThread(const Params: TTaskParamList);
begin
  SetCurrentUser(FindUser(FCurrentTask.Params.Value['User']));
  CheckCondition(CurrentUserInfo <> nil, Format('User %s is not registered', [FCurrentTask.Params.Value['User']]));

  OnTaskStatusChange('Task started', 0);
  try
    try
      case FCurrentTask.TaskType of
        ttBuild:              TskBuild;
        ttReleaseBinProject:  TskReleaseBinProject;
        ttPublishBinProject:  TskPublishBinProject;
        ttDeleteBinProject:   TskDeleteBinProject;
        ttPrepareHotFix:      TskPrepareHotFix;
      end;
      OnTaskStatusChange('Task finished successfully', 100);
      SendEMail(FCurrentTask.Caption, True, '');
    except
      on E: Exception do
      begin
        OnTaskDetailChange(E.Message);
        OnTaskStatusChange('Task finished with errors', FCurrentTask.Progress);
        SendEMail(FCurrentTask.Caption, False, E.Message);        
      end;
    end;
  finally
    FCurrentTask.Executing := False;
    SetCurrentUser(nil);
  end;
end;

procedure TpbProjectBuilder.UpdateProjects(const AProduct: IpbProduct);
begin
  Repository.UpdateProjects(AProduct);
end;

function TpbProjectBuilder.IssueTracking: IpbIssueTracking;
begin
  Result := FIssueTracking;
end;

procedure TpbProjectBuilder.TskDeleteBinProject;
var
  Product: IpbProduct;
  Project: IpbProject;
  ProjectNames: IisStringList;
  i: Integer;
begin
  Product := Repository.GetProducts.Find(FCurrentTask.Params.Value['ProductName']);
  CheckCondition(Assigned(Product), 'Cannot find the product: ' + FCurrentTask.Params.Value['ProductName']);
  ProjectNames := IInterface(FCurrentTask.Params.Value['ProjectNames']) as IisStringList;

  for i := 0 to ProjectNames.Count - 1 do
  begin
    Project := Product.FindProject(ProjectNames[i]);
    if Assigned(Project) then
      Repository.DeleteBinProject(Project);
  end;    
end;

procedure TpbProjectBuilder.TskPrepareHotFix;
begin
  Repository.PrepareHotFix(GetProjectFormTaskParams);
end;

procedure TpbProjectBuilder.TskPublishBinProject;
begin
  Repository.PublishBinProject(GetProjectFormTaskParams);
end;

procedure TpbProjectBuilder.TskReleaseBinProject;
begin
  Repository.ReleaseBinProject(GetProjectFormTaskParams);
end;

function TpbProjectBuilder.GetProjectFormTaskParams: IpbProject;
var
  Product: IpbProduct;
begin
  Product := Repository.GetProducts.Find(FCurrentTask.Params.Value['ProductName']);
  CheckCondition(Assigned(Product), 'Cannot find the product: ' + FCurrentTask.Params.Value['ProductName']);
  Result := Product.FindProject(FCurrentTask.Params.Value['ProjectName']);
  CheckCondition(Assigned(Result), 'Cannot find the project: ' + FCurrentTask.Params.Value['ProjectName']);
end;

function TpbProjectBuilder.CurrentUserInfo: IpbUserInfo;
begin
  Result := FCurrentUserInfo;
end;

procedure TpbProjectBuilder.BuildUserList;

  function NameToRole(const AName: String): TpbUserRole;
  const
    RoleNames: array [Low(TpbUserRole) .. High(TpbUserRole)] of string =
      ('Unknown', 'Developer', 'QATester', 'QAManager', 'Administrator');
  var
    i: TpbUserRole;
  begin
    Result := urUnknown;
    for i := Low(RoleNames) to High(RoleNames) do
      if AnsiSameText(RoleNames[i], AName) then
      begin
        Result := i;
        break;
      end;
  end;

var
  Users: IisStringListRO;
  Res: IisStringList;
  i: Integer;
  UserNode: IXmlElement;
  UserInfo: IpbUserInfo;
  sUser, sPassword: String;
begin
  Res := TisStringList.CreateAsIndex;

  Users := Mainboard.Config.GetChildNodes('Users');
  for i := 0 to Users.Count - 1 do
  begin
    UserNode := Users.Objects[i] as IXmlElement;
    sUser := UserNode.getAttr('Name');
    sPassword := DecryptStringLocaly(BinToStr(Mainboard.Settings.AsString['UserPasswords\' + sUser]), '');
    UserInfo := TpbUserInfo.Create(sUser, sPassword, UserNode.getAttr('Email'),
       NameToRole(UserNode.getAttr('Role')), UserNode.getAttr('AllowedProducts'));
    Res.AddObject(UserInfo.User, UserInfo);
  end;

  Lock;
  try
    FUsers := Res;
  finally
    Unlock;
  end;
end;


function TpbProjectBuilder.FindUser(const AUser: String): IpbUserInfo;
var
  i: Integer;
begin
  Lock;
  try
    i := FUsers.IndexOf(AUser);
    if i <> -1 then
      Result := FUsers.Objects[i] as IpbUserInfo
    else
      Result := nil;
  finally
    Unlock;
  end;
end;

procedure TpbProjectBuilder.SetCurrentUser(const AUserInfo: IpbUserInfo);
begin
  FCurrentUserInfo := AUserInfo;
end;

procedure TpbProjectBuilder.ChangeUserPassword(const AOldPassword, ANewPassword: String);
begin
  if CurrentUserInfo.Password <> '' then
    CheckCondition(CurrentUserInfo.Password = AOldPassword, 'Old password does not match');
  CheckCondition(ANewPassword <> '', 'New password cannot be empty');

  Mainboard.Settings.AsString['UserPasswords\' + CurrentUserInfo.User] := StrToBin(EncryptStringLocaly(ANewPassword));

  BuildUserList;
end;

procedure TpbProjectBuilder.SendEMail(const aTaskName: String; const aSuccsess: Boolean; const aBody: String);
var
  sTo, sSubj, sBodyHeader, sStatus: String;
begin
  if aSuccsess then
  begin
    sSubj := 'Task has finished successfully';
    sStatus := 'Finished successfully';
  end
  else
  begin
    sSubj := 'Task has finished with errors';
    sStatus := 'Finished with errors';
  end;

  sTo := Mainboard.ProjectBuilder.CurrentUserInfo.EMail;
  if sTo <> '' then
  begin
    sBodyHeader := Format('Task:'#9#9'%s'#13'Status:'#9'%s', [aTaskName, sStatus]);
    Mainboard.SendEMail(sTo, sSubj, sBodyHeader + #13#13 + aBody);
  end;
end;

function TpbProjectBuilder.GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;
begin
  Result := Repository.GetUpdatedIssues(AProject);
end;

end.
