unit pbRepository;

interface

uses Windows, isBaseClasses, pbInterfaces, pbClasses, EvStreamUtils, isBasicUtils, SimpleXML,
     isExceptions, isThreadManager, IsUtils, SEncryptionRoutines, pbUtils;

type
  IpbRepository = interface
  ['{E2C88CB6-49BF-4604-96FB-419694478493}']
    function  GetProducts: IpbProducts;
    procedure CheckOutProject(const AProject: IpbProject);
    procedure CheckInBinaries(const AProject: IpbProject);
    procedure UpdateProjects(const AProduct: IpbProduct);
    function  GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;
    procedure ReleaseBinProject(const AProject: IpbProject);
    procedure PublishBinProject(const AProject: IpbProject);
    procedure DeleteBinProject(const AProject: IpbProject);
    procedure PrepareHotFix(const AProject: IpbProject);
    procedure CheckIssueStatus(const AIssues: IisParamsCollection; const AValidStatuses, AInValidStatuses: array of string);
  end;

  function CreateRepository: IpbRepository;

implementation

uses SysUtils, Classes, pbSrvMainboard, isSettings, Math;

type
  IpbServerResponse = interface
  ['{77B08AED-A7DA-4DF6-9417-A617C8B57531}']
     function Data: IisStream;
     function Lines: IisStringList;
     function XML: IXmlDocument;
  end;

  IpbAccuRev = interface
  ['{F4079D3E-2CDF-40B2-BC51-E841BE219A02}']
    function  ExecCmd(const aCommand: String): IpbServerResponse;
    function  ExecXMLRequest(const aXMLrequest: String): IpbServerResponse;
  end;

  TpbAccuRevRepository = class(TisInterfacedObject, IpbRepository, IpbIssueTracking, IpbAccuRev)
  private
    FProducts: IpbProducts;
    FIssueFields: IisListOfValues;
    function  ExecSimpleCmd(const aCommand: String; out aResult: IisStream): Boolean;
    function  ExecCmd(const aCommand: String): IpbServerResponse;
    function  ExecXMLRequest(const aXMLrequest: String): IpbServerResponse;
    procedure Login;
    procedure Logout;
    procedure AddChildProjects(const ABaseProject: IpbProject);
    function  CalcNextBuildVersion(const ABaseVersion: IpbVersion; const AProduct: IpbProduct): IpbVersion;
    function  GetProjectContent(const AProject: IpbProject): IisParamsCollection;
    procedure CreateBuildNotes(const AProject: IpbProject);
    procedure UpdateIssueList(const AProject: IpbProject);
    function  IssueFields: IisListOfValues;
  protected
    procedure  DoOnDestruction; override;

    function  GetProducts: IpbProducts;
    procedure CheckOutProject(const AProject: IpbProject);
    procedure CheckInBinaries(const AProject: IpbProject);
    procedure UpdateProjects(const AProduct: IpbProduct);
    function  GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;
    procedure ReleaseBinProject(const AProject: IpbProject);
    procedure PublishBinProject(const AProject: IpbProject);
    procedure DeleteBinProject(const AProject: IpbProject);
    procedure PrepareHotFix(const AProject: IpbProject);
    procedure CheckIssueStatus(const AIssues: IisParamsCollection; const AValidStatuses, AInValidStatuses: array of string);    

    function  StreamExists(const AName: String): Boolean;
    procedure CreateStream(const AName: String; const AParentName: String);
    procedure MoveStream(const AName: String; const AParentName: String);
    procedure DeleteStream(const AName: String);

    function  GetUserList: IisListOfValues;
    function  GetIssues(const AQuery: String): IisParamsCollection;
    function  AddIssue(const AIssue: IisListOfValues): IisListOfValues;
    procedure UpdateIssue(const AIssue: IisListOfValues);
  end;


  TpbServerResponse = class(TisInterfacedObject, IpbServerResponse)
  private
    FData: IisStream;
    FLines: IisStringList;
    FXML: IXmlDocument;
  protected
    procedure DoOnDestruction; override;
    function  Data: IisStream;
    function  Lines: IisStringList;
    function  XML: IXmlDocument;
  public
    constructor Create(const AData: IisStream); reintroduce;
  end;


  TpbProjectExt = class(TpbProject)
  protected
    function  ParamToValue(const AParamName: String): String; override;
  end;


  TpbProductsExt = class(TpbProducts)
  public
    constructor Create; reintroduce;
  end;


function CreateRepository: IpbRepository;
begin
  Result := TpbAccuRevRepository.Create(nil, True);
end;


{ TpbRepository }

procedure TpbAccuRevRepository.CheckInBinaries(const AProject: IpbProject);
var
  s: String;
begin
  // Copy binaries to repository
  Mainboard.ProjectBuilder.OnTaskDetailChange('Copying folder: ' + AProject.GetPropValue('BinDir'));
  s := NormalizePath(Mainboard.Config.AsString['Config\BCMBuildDir']) +  AProject.Product.Name + '\' + AProject.Version.AsString;
  ForceDirectories(s);
  CopyFolderContent(AProject.GetPropValue('BinDir'), s);
end;


procedure TpbAccuRevRepository.CheckOutProject(const AProject: IpbProject);
var
  cmd, sStreamName: String;
  sTargetFolder: String;
begin
  sTargetFolder := Mainboard.Config.AsString['Config\SCMWorkspaceDir'];
  ForceDirectories(sTargetFolder);
  ClearDir(sTargetFolder);

  sStreamName := AProject.Properties.TryGetValue('Stream', '');

  if sStreamName <> '' then
  begin
    Mainboard.ProjectBuilder.OnTaskDetailChange('Checking out from stream: ' + sStreamName);

    // Populate work folder
    cmd := Format('pop -H -v "%s" -L "%s" -R .', [sStreamName, sTargetFolder]);
    AProject.Properties.AddValue('CheckOutTime', Now);
    ExecCmd(cmd);
  end;  
end;

{       Check out to Workspace (may be used later)

procedure TpbAccuRevRepository.CheckOutProject(const AProject: IpbProject);
const
  cBuildWorkspace = 'ProjectBuilder';
var
  cmd: String;
  Res: IpbServerResponse;
  N: IXmlNode;
  WSName: String;
  sTargetFolder: String;
begin
  Mainboard.GUI.ProgressCallback.OnDetailChange('Checking out from stream: ' + AProject.Properties.Value['Stream']);

  sTargetFolder := Mainboard.Config.AsString['Config\SCMWorkspaceDir'];
  ForceDirectories(sTargetFolder);
  ClearDir(sTargetFolder);

  // Prepare workspase
  WSName := cBuildWorkspace + '_' + Mainboard.Config.AsString['Config\SCMUser'];
  Res := ExecCmd('show -H -fx wspaces');

  N := Res.XML.DocumentElement.SelectNode(Format('/Element[@Name="%s"]', [WSName])) as IXmlNode;

  if not Assigned(N) then
  begin
    // Create if it's absent
    cmd := Format('mkws -H -w "%s" -b "%s" -l "%s"', [cBuildWorkspace, AProject.Properties.Value['Stream'], sTargetFolder]);
    ExecCmd(cmd);
  end
  else
  begin
    // Update path if it's different
    cmd := Format('chws -H -w "%s" -b "%s" -l "%s"', [cBuildWorkspace, AProject.Properties.Value['Stream'], sTargetFolder]);
    ExecCmd(cmd);
  end;

  // Open workspace
  cmd := Format('start -H -c pwd -r "%s"', [WSName]);
  ExecCmd(cmd);

  // Update workspace
  cmd := Format('update -H -Z -G -L "%s"', [sTargetFolder]);
  ExecCmd(cmd);

  // Populate workspace
  cmd := Format('pop -H -O -R -L "%s" "%s"', [sTargetFolder, sTargetFolder]);
  AProject.Properties.AddValue('CheckOutTime', Now);
  ExecCmd(cmd);
end;
}

procedure TpbAccuRevRepository.DoOnDestruction;
begin
  inherited;
  Logout;
end;

function TpbAccuRevRepository.ExecCmd(const aCommand: String): IpbServerResponse;
var
  Res: IisStream;
  sError: String;
  i: Integer;
begin
  Result := nil;

  for i := 1 to 2 do
  begin
    if ExecSimpleCmd(aCommand, Res) then
    begin
      Result := TpbServerResponse.Create(Res);
      break;
    end
    else
    begin
      sError := Res.AsString;
      if StartsWith(sError, 'Not authenticated') or
         StartsWith(sError, 'Failed authentication') or
         Contains(sError, 'expired') then
        Login
      else if StartsWith(sError, '<') then
      begin
        Result := TpbServerResponse.Create(Res);
        break;
      end
      else
        raise EisException.Create(sError);
    end;
  end;

  if not Assigned(Result) then
    raise EisException.Create('AccuRev command has failed');
end;

function TpbAccuRevRepository.ExecSimpleCmd(const aCommand: String; out aResult: IisStream): Boolean;
var
  Err: IisStream;
  Cmd: String;
begin
  Cmd := StringReplace(aCommand, '-H ',
    Format('-H %s:%s ', [Mainboard.Config.AsString['Config\SCMHost'], Mainboard.Config.GetValue('Config\SCMPort', '5050')]), [rfReplaceAll]);
  aResult := TisStream.CreateInMemory;
  Err := TisStream.CreateInMemory;
  Result := RunProcess('accurev.exe', Cmd, [roHideWindow, roWaitForClose], aResult, Err);
  if not Result and (Err.Size > 0) then
    aResult := Err;
end;

procedure TpbAccuRevRepository.UpdateProjects(const AProduct: IpbProduct);
var
  i: Integer;
  Prj: IpbProject;
  Version: IpbVersion;

  procedure ScanFolder(const AFolder: String; const APrjType: TpbProjectType);
  var
    Dirs: IisStringList;
    i, j: Integer;
    Prj, BasePrj: IpbProject;
    BuildInfo: IisSettings;
    s, sIssueList, sIssueNbr: String;
    Issues: IisParamsCollection;
    Issue: IisListOfValues;
    ValNames: IisStringListRO;
  begin
    Dirs := GetSubDirs(AFolder);
    for i := 0 to Dirs.Count - 1 do
    begin
      s := NormalizePath(Dirs[i]) + 'BuildInfo.ini';
      if FileExists(s) then
      begin
        BuildInfo := TisSettingsIni.Create(s);
        BasePrj := AProduct.FindProject(BuildInfo.AsString['Project\BaseProject']);
        if Assigned(BasePrj) then
        begin
          Prj := TpbProjectExt.Create(APrjType);
          Prj.Properties.AsStream := BasePrj.Properties.AsStream;

          ValNames := BuildInfo.GetValueNames('Project');
          for j := 0 to ValNames.Count - 1 do
            Prj.Properties.AddValue(ValNames[j], BuildInfo.AsString['Project\' + ValNames[j]]);
          Prj.Properties.AddValue('BinDir', Dirs[i]);

          if APrjType = ptBinRelease then
            Prj.Properties.AddValue('Name', 'Release ' + BuildInfo.AsString['Project\Version'])
          else
            Prj.Properties.AddValue('Name', 'Build ' + BuildInfo.AsString['Project\Version'] + ' (' + BasePrj.Name + ')');


          // Add issue info
          Issues := TisParamsCollection.Create;
          Prj.Properties.AddValue('Issues', Issues);
          sIssueList := BuildInfo.AsString['Project\Issues'];
          while sIssueList <> '' do
          begin
            sIssueNbr := GetNextStrValue(sIssueList, ',');
            Issue := Issues.AddParams(sIssueNbr);
            ValNames := BuildInfo.GetValueNames('Issue_' + sIssueNbr);
            for j := 0 to ValNames.Count - 1 do
              Issue.AddValue(ValNames[j], BuildInfo.AsString['Issue_' + sIssueNbr + '\' + ValNames[j]]);
          end;

          AProduct.AddProject(Prj);
        end;
      end;
    end;
  end;

begin
  // Delete all dynamically added projects
  for i := AProduct.ProjectCount - 1 downto 0 do
  begin
   Prj := AProduct.Projects[i];
    if not (Prj.ProjectType in [ptAbstract, ptDevelopment]) then
      AProduct.DeleteProject(Prj);
  end;

  //Add feature, released and hot-fix projects
  for i := 0 to AProduct.ProjectCount - 1 do
  begin
   Prj := AProduct.Projects[i];
    if Prj.ProjectType = ptDevelopment then
      AddChildProjects(Prj);
  end;

  // Populate bin projects
  ScanFolder(NormalizePath(NormalizePath(Mainboard.Config.AsString['Config\BCMReleaseDir']) + AProduct.Name),
            ptBinRelease);
  ScanFolder(NormalizePath(NormalizePath(Mainboard.Config.AsString['Config\BCMBuildDir']) + AProduct.Name),
            ptBinBuild);

  // Update version of development, feature and hotfix projects
  for i := 0 to AProduct.ProjectCount - 1 do
  begin
    Prj := AProduct.Projects[i];
    if Prj.ProjectType in [ptDevelopment, ptFeature, ptHotfix] then
    begin
      if Prj.ProjectType in [ptDevelopment, ptFeature] then
        Version := TpbVersion.Create(Prj.GetPropValue('MajorVersion', '0') + '.0.0.1')
      else if Prj.ProjectType = ptHotfix then
        Version := Prj.Version;
      Version := CalcNextBuildVersion(Version, AProduct);
      Prj.Properties.AddValue('Version', Version.AsString);
    end
  end;

  // Update issue list of development, feature and hotfix projects
  for i := AProduct.ProjectCount - 1 downto 0 do
  begin
    Prj := AProduct.Projects[i];
    if Prj.ProjectType in [ptDevelopment, ptFeature, ptHotfix] then
    begin
      Prj.Properties.AddValue('Issues', GetProjectContent(Prj));
      if (Prj.ProjectType = ptFeature) and ((IInterface(Prj.Properties.Value['Issues']) as IisParamsCollection).Count = 0) then
        AProduct.DeleteProject(Prj);
    end;
  end;
end;

function TpbAccuRevRepository.GetProducts: IpbProducts;
begin
  Lock;
  try
    if not Assigned(FProducts) then
      FProducts := TpbProductsExt.Create;
  finally
    Unlock;
  end;

  Result := FProducts;  
end;

procedure TpbAccuRevRepository.Login;

  procedure DoLogin;
  var
    Res: IisStream;
    sCmd: String;
  begin
    sCmd := Format('login -H "%s" "%s"', [Mainboard.Config.AsString['Config\SCMUser'],
                                          Mainboard.Config.AsString['Config\SCMPassword']]);
    if not ExecSimpleCmd(sCmd, Res) then
      raise EisException.Create(Res.AsString);
  end;

begin
  Lock;
  try
    Logout;
    DoLogin;
  finally
    UnLock;
  end;
end;

procedure TpbAccuRevRepository.Logout;
var
  Res: IisStream;
  sCmd: String;
begin
  Lock;
  try
    sCmd := 'logout -H ';
    if not ExecSimpleCmd(sCmd, Res) then
      raise EisException.Create(Res.AsString);
  finally
    UnLock;  
  end;
end;

{ TpbServerResponse }

constructor TpbServerResponse.Create(const AData: IisStream);
begin
  inherited Create;
  FData := AData;
end;

function TpbServerResponse.Data: IisStream;
begin

end;

procedure TpbServerResponse.DoOnDestruction;
begin
  inherited;
end;

function TpbServerResponse.Lines: IisStringList;
begin
  if not Assigned(FLines) then
  begin
    FLines := TisStringList.Create;
    FData.Position := 0;
    FLines.LoadFromStream(FData.RealStream);
  end;
  Result := FLines;
end;

function TpbServerResponse.XML: IXmlDocument;
begin
  if not Assigned(FXML) then
  begin
    FXML := CreateXmlDocument;
    FData.Position := 0;
    FXML.Load(FData);
  end;
  Result := FXML;
end;

{ TpbProductsExt }

constructor TpbProductsExt.Create;
var
  Projects: IisStringListRO;
  i: Integer;

  function AddProj(const aProj: IXmlElement): IpbProject;
  const
    ProjTypes: array [Low(TpbProjectType)..High(TpbProjectType)] of string =
      ('', 'abstract', 'development', 'feature', 'hotfix', 'bin-build', 'bin-release');
  var
    BaseProj: IXmlElement;
    i: Integer;
    Product: IpbProduct;
    BasePrj, Prj: IpbProject;
    BaseProjectName, ProductName: String;
    ProjType: TpbProjectType;
  begin
    BaseProjectName := aProj.GetAttr('BaseProject');
    ProjType := TpbProjectType(aProj.GetEnumAttr('Type', ['', 'abstract', 'development', 'feature', 'hotfix', 'bin-build', 'bin-release'], 2));
    Prj := TpbProjectExt.Create(ProjType);

    if BaseProjectName <> '' then
    begin
      for i := 0 to Projects.Count - 1 do
      begin
        BaseProj := Projects.Objects[i] as IXmlElement;
        CheckCondition(BaseProj.AttrExists('Name'), 'Invalid configuration. Attribute Name is required.');
        if AnsiSameText(BaseProj.GetAttr('Name'), BaseProjectName) then
        begin
          BasePrj := AddProj(BaseProj);
          break;
        end
        else
          BaseProj := nil;
      end;

      CheckCondition(Assigned(BaseProj), Format('Invalid configuration. Base project %s not found.', [BaseProjectName]));
      Prj.Properties.AsStream := BasePrj.Properties.AsStream;
    end;

    for i := 0 to aProj.AttrCount - 1 do
      Prj.Properties.AddValue(aProj.AttrNames[i], aProj.GetAttr(aProj.AttrNames[i]));
    Prj.Properties.AddValue('Type', ProjTypes[ProjType]);

    ProductName := Prj.GetPropValue('ProductName');
    CheckCondition(ProductName <> '', 'Invalid configuration. ProductName is empty.');
    Product := Find(ProductName);
    if not Assigned(Product) then
    begin
      Product := TpbProduct.Create(ProductName);
      Add(Product);
    end;

    Result := Product.FindProject(Prj.Name);
    if not Assigned(Result) then
    begin
      Product.AddProject(Prj);
      Result := Prj;
    end;
  end;

begin
  inherited Create;

  Projects := Mainboard.Config.GetChildNodes('Projects');

  for i := 0 to Projects.Count - 1 do
    AddProj(Projects.Objects[i] as IXmlElement);

  for i := 0 to Count - 1 do
    Mainboard.ProjectBuilder.UpdateProjects(GetItem(i));
end;


{ TpbCustomApp }

procedure TpbAccuRevRepository.DeleteBinProject(const AProject: IpbProject);
var
  s: String;
begin
  CheckCondition(AProject.ProjectType in [ptBinBuild, ptBinRelease], 'Wrong project type');
  s := AProject.GetPropValue('BinDir');
  Mainboard.ProjectBuilder.OnTaskDetailChange('Deleting folder: ' + s);
  ClearDir(s, True);

  AProject.Product.DeleteProject(AProject);

  Mainboard.ProjectBuilder.OnTaskDetailChange('Folder deleted');
end;

procedure TpbAccuRevRepository.ReleaseBinProject(const AProject: IpbProject);
var
  SrcFolder, DestFolder: String;
  cmd, s, sSnapShot, sStreamName: String;
  SnapshotTime: String;
  i: Integer;
  Prj: IpbProject;
  BuildInfo: IisSettings;
  BaseProject: IpbProject;
begin
  CheckCondition(AProject.ProjectType = ptBinBuild, 'Wrong project type');

  // Update issues
  UpdateIssueList(AProject);

  // Check if there are untested issues
  CheckIssueStatus(IInterface(AProject.Properties.Value['Issues']) as IisParamsCollection,
    ['Closed', 'To Be Documented', 'Verified'], []);

  s := AProject.GetPropValue('BaseProject');
  BaseProject := AProject.Product.FindProject(s);
  CheckCondition(Assigned(BaseProject), Format('Base project %s not found', [s]));
  if BaseProject.ProjectType = ptHotfix then
  begin
    s := BaseProject.GetPropValue('BaseProject');
    BaseProject := BaseProject.Product.FindProject(s);
    CheckCondition(Assigned(BaseProject), Format('Base project %s not found', [s]));
  end;

  sStreamName := AProject.Properties.TryGetValue('Stream', '');
  if sStreamName <> '' then
  begin
    sSnapShot := AProject.GetPropValue('ProductAbbr', AProject.Product.Name);
    sSnapShot := sSnapShot + '_' + AProject.Version.AsString;

    Mainboard.ProjectBuilder.OnTaskDetailChange('Creating snapshot: ' + sSnapShot);

    // Create source code snapshot
    SnapshotTime := FormatDateTime('yyyy/m/d h:n:s', StrToDateTime(AProject.Properties.Value['CheckOutTime']));

    cmd := Format('mksnap -H -s "%s" -b "%s" -t "%s"',
      [sSnapShot, sStreamName, SnapshotTime]);

    ExecCmd(cmd);
  end;  

  // Move binary folder
  SrcFolder := AProject.Properties.Value['BinDir'];
  Mainboard.ProjectBuilder.OnTaskDetailChange('Moving folder: ' + SrcFolder);
  DestFolder := NormalizePath(Mainboard.Config.AsString['Config\BCMReleaseDir']) +
    AProject.Product.Name + '\' + AProject.Version.AsString;
  ForceDirectories(DestFolder);
  MoveFolderContent(SrcFolder, DestFolder);
  ClearDir(SrcFolder, True);

  TpbProjectExt((AProject as IisInterfacedObject).GetImplementation).SetProjectType(ptBinRelease);
  AProject.Properties.Value['BinDir'] := DestFolder;

  BuildInfo := TisSettingsIni.Create(NormalizePath(DestFolder) + 'BuildInfo.ini');
  BuildInfo.AsString['Project\BaseProject'] := BaseProject.Name;
  BuildInfo.AsString['Project\ReleaseTime'] := DateTimeToStr(Now);
  BuildInfo := nil;

  // Delete all similar versions
  for i := AProject.Product.ProjectCount - 1 downto 0 do
  begin
    Prj := AProject.Product.Projects[i];
    if Prj.ProjectType = ptBinBuild then
    begin
      if (Prj.Version.Major = AProject.Version.Major) and
         (Prj.Version.Minor = AProject.Version.Minor) and
         (Prj.Version.Patch = AProject.Version.Patch) then
        DeleteBinProject(Prj);
    end;
  end;
end;

procedure TpbAccuRevRepository.PrepareHotFix(const AProject: IpbProject);
var
  cmd, StreamPrefix, HFStreamName: String;
begin
  CheckCondition(AProject.ProjectType = ptBinRelease, 'Wrong project type');

  StreamPrefix := AProject.GetPropValue('ProductAbbr', AProject.Product.Name);
  HFStreamName := StreamPrefix + '_hf_' + AProject.Version.AsString;

  Mainboard.ProjectBuilder.OnTaskDetailChange('Creating stream: ' + HFStreamName);

  // create working stream fro hot fix
  cmd := Format('mkstream -H -s "%s" -b "%s"',
    [HFStreamName, StreamPrefix + '_' + AProject.Version.AsString]);
  ExecCmd(cmd);
  Mainboard.ProjectBuilder.OnTaskDetailChange('New stream is created');
end;

procedure TpbAccuRevRepository.AddChildProjects(const ABaseProject: IpbProject);
var
  Streams: IisParamsCollection;
  i: Integer;
  BaseStream: String;
  Prj: IpbProject;

  function GetStreams: IisParamsCollection;
  var
    Res: IpbServerResponse;
    i: Integer;
    N, ParentStreamNode: IXmlNode;
    StreamName, sVer, sResoNbr: String;
    StreamInfo: IisListOfValues;
    Version: IpbVersion;

    function IsActiveHotfix(const AStreamNode: IXmlNode): Boolean;
    var
      Nodes: IisList;
      N: IXmlNode;
    begin
      Nodes := Res.XML.DocumentElement.SelectNodes(
        Format('/stream[@basis="%s" and @type!="workspace"]', [AStreamNode.GetAttr('name')]));

      Result := Nodes.Count = 0;

      if Result then
      begin
        N := Res.XML.DocumentElement.SelectNode(
          Format('/stream[@name="%s" and @type="snapshot"]', [AStreamNode.GetAttr('basis')])) as IXmlNode;
        Result := Assigned(N);
      end;
    end;

  begin
    Result := TisParamsCollection.Create;
    Res := (Mainboard.ProjectBuilder.Repository as IpbAccuRev).ExecCmd('show -H -fx -s "' + BaseStream + '" -R streams');

    for i := 0 to Res.XML.DocumentElement.ChildNodes.Count - 1 do
    begin
      N := Res.XML.DocumentElement.ChildNodes[i];
      StreamName := N.GetAttr('name');
      if StreamName = BaseStream then
        Continue;

{ This is for released versions from repository
  Probably will use this in future for rebuilding deleted releases

      if N.GetAttr('type') = 'snapshot' then
      begin
        StreamInfo := Result.AddParams(StreamName);
        sVer := StreamName;
        sVer := GetPrevStrValue(sVer, '_');
        StreamInfo.AddValue('Name', 'Deleted release ' + sVer);
        StreamInfo.AddValue('Version', sVer);
        StreamInfo.AddValue('Type', ptRelease);
      end
}
      if (N.GetAttr('type') = 'normal') and IsActiveHotfix(N) then
      begin
        ParentStreamNode := Res.XML.DocumentElement.SelectNode(
          Format('/stream[@name="%s"]', [N.GetAttr('basis')])) as IXmlNode;

        sVer := ParentStreamNode.GetAttr('name');
        sVer := GetPrevStrValue(sVer, '_');
        Version := TpbVersion.Create(sVer);
        if Version.AsString <> '0.0.0.0' then  // 0.0.0.0 is invalid stream name
        begin
          StreamInfo := Result.AddParams(StreamName);
          Version := Version.NextHotFix;
          StreamInfo.AddValue('Name', 'Hot-fix for ' + sVer);
          StreamInfo.AddValue('Version', Version.AsString);
          StreamInfo.AddValue('Type', ptHotfix);
        end;
      end

      else if (N.GetAttr('type') = 'normal') and StartsWith(StreamName, 'res_') then
      begin
        sResoNbr := Copy(StreamName, 5, Length(StreamName));

        StreamInfo := Result.AddParams(StreamName);
        StreamInfo.AddValue('Name', 'Reso #' + sResoNbr);
        StreamInfo.AddValue('Description', 'Special build for Reso #' + sResoNbr);
        StreamInfo.AddValue('Version', '');
        StreamInfo.AddValue('Type', ptFeature);
      end;
    end;
  end;


begin
  BaseStream := ABaseProject.Properties.TryGetValue('Stream', '');

  if BaseStream <> '' then
  begin
    Streams := GetStreams;
    for i := 0 to Streams.Count - 1 do
    begin
      Prj := TpbProjectExt.Create(TpbProjectType(Streams[i].Value['Type']));
      Prj.Properties.AsStream := ABaseProject.Properties.AsStream; // copy all properties from base project
      Prj.Properties.AddValue('BaseProject', ABaseProject.Name);
      Prj.Properties.AddValue('Name', Streams[i].Value['Name']);

      if Streams[i].ValueExists('Description') then
        Prj.Properties.AddValue('Description', Streams[i].Value['Description']);

      Prj.Properties.AddValue('Version', Streams[i].Value['Version']);
      Prj.Properties.AddValue('Stream', Streams.ParamName(i));

      ABaseProject.Product.AddProject(Prj);
    end;
  end;  
end;

function TpbAccuRevRepository.CalcNextBuildVersion(const ABaseVersion: IpbVersion;
  const AProduct: IpbProduct): IpbVersion;
var
  KnownVersions: IisStringList;
  i: Integer;
  Prj: IpbProject;
begin
  Result := ABaseVersion;

  KnownVersions := TisStringList.Create;

  if ABaseVersion.Patch = 0 then // development and feature build
  begin
    for i := 0 to AProduct.ProjectCount - 1 do
    begin
      Prj := AProduct.Projects[i];
      if (Prj.ProjectType in [ptBinBuild, ptBinRelease]) and
         (Prj.Version.Major = ABaseVersion.Major) and (Prj.Version.Patch = 0) then
        KnownVersions.AddObject(Prj.Version.AsString(4), Prj);
    end;

    if KnownVersions.Count > 0 then
    begin
      KnownVersions.Sort;
      Prj := KnownVersions.Objects[KnownVersions.Count - 1] as IpbProject;
      if Prj.ProjectType = ptBinRelease then
        Result := Prj.Version.NextRelease
      else if Prj.ProjectType = ptBinBuild then
        Result := Prj.Version.NextBuild;
    end;
  end

  else  // hot-fix build
  begin
    for i := 0 to AProduct.ProjectCount - 1 do
    begin
      Prj := AProduct.Projects[i];
      if (Prj.ProjectType in [ptBinBuild, ptBinRelease]) and
         (Prj.Version.Major = ABaseVersion.Major) and (Prj.Version.Minor = ABaseVersion.Minor) and
         (Prj.Version.Patch >= ABaseVersion.Patch) then
        KnownVersions.AddObject(Prj.Version.AsString(4), Prj);
    end;

    if KnownVersions.Count > 0 then
    begin
      KnownVersions.Sort;
      Prj := KnownVersions.Objects[KnownVersions.Count - 1] as IpbProject;
      if Prj.ProjectType = ptBinRelease then
        Result := Prj.Version.NextHotFix
      else if Prj.ProjectType = ptBinBuild then
        Result := Prj.Version.NextBuild;
    end;
  end
end;

{ TpbProjectExt }

function TpbProjectExt.ParamToValue(const AParamName: String): String;
begin
  Result := inherited ParamToValue(AParamName);
  if AParamName = Result then
    Result := Mainboard.Config.GetValue('Config\' + AParamName, AParamName);
end;

procedure TpbAccuRevRepository.PublishBinProject(const AProject: IpbProject);

  procedure SendEMail;
  var
    sTo, sSubj, sBody: String;
    BuildNotes: IisStream;
  begin
    sSubj := Format('%s %s', [AProject.Product.Name, AProject.Version.AsString]);
    if AProject.ProjectType = ptBinBuild then
      sSubj := sSubj + ' is ready for testing'
    else
      sSubj := sSubj + ' has been published';

    sTo := AProject.GetPropValue('PublishEmail');

    if sTo <> '' then
    begin
      BuildNotes := TisStream.CreateFromFile(NormalizePath(AProject.GetPropValue('BinDir')) + 'BuildNotes.html');
      sBody := BuildNotes.AsString;
      Mainboard.SendEMail(sTo, sSubj, sBody);
    end;
  end;

var
  Cmd, Params: String;
  ProcessExec: IpbProcessExec;
begin
  if AProject.ProjectType = ptBinRelease then
  begin
    // Update issues
    UpdateIssueList(AProject);

    // Check if there are opened issues
    CheckIssueStatus( IInterface(AProject.Properties.Value['Issues']) as IisParamsCollection,
      ['To Be Documented', 'Closed'], []);
  end;

  // Create release notes
  CreateBuildNotes(AProject);

  if AProject.ProjectType = ptBinBuild then
  begin
    Cmd := AProject.GetPropValue('PublishBuildCmd');
    Params := AProject.GetPropValue('PublishBuildCmdParams');
  end

  else if AProject.ProjectType = ptBinRelease then
  begin
    Cmd := AProject.GetPropValue('PublishReleaseCmd');
    Params := AProject.GetPropValue('PublishReleaseCmdParams');
  end;

  if Trim(Cmd) = '' then
  begin
    Mainboard.ProjectBuilder.OnTaskDetailChange('Publish method is not defined');
    Exit;
  end;

  if ExtractFilePath(Cmd) = '' then
    Cmd := ChangeFilePath(Cmd, AppDir);

  SetCurrentDir(ExtractFilePath(cmd));
  Mainboard.ProjectBuilder.OnTaskDetailChange(cmd + ' ' + Params);

  ProcessExec := TpbProcessExec.Create;
  ProcessExec.Run(cmd, Params);

  SendEMail;
end;

function TpbAccuRevRepository.GetProjectContent(const AProject: IpbProject): IisParamsCollection;
const
  ACCUREV_STREAM_ISSUES = '<acRequest><issuelist show_active="true" show="complete,incomplete"><stream1>%s</stream1></issuelist></acRequest>';
var
  Res: IpbServerResponse;
  L: IisList;
  i: Integer;
  Issue: IisListOfValues;
  IssueElem, IssueProp: IXmlNode;
  IssueNbr: String;
begin
  Result := TisParamsCollection.Create;

  if AProject.GetPropValue('Stream') <> '' then
  begin
    Res := (Mainboard.ProjectBuilder.Repository as IpbAccuRev).ExecXMLRequest(Format(ACCUREV_STREAM_ISSUES, [AProject.GetPropValue('Stream')]));

    L := Res.XML.DocumentElement.SelectNodes('/issues/issue');
    for i := 0 to L.Count - 1 do
    begin
      IssueElem := L[i] as IXmlNode;

      IssueProp := IssueElem.SelectNode('/issueNum') as IXmlNode;
      if Assigned(IssueProp) then
      begin
        IssueNbr := IssueProp.Text;
        IssueProp := IssueElem.SelectNode('/resoNum') as IXmlNode;
        if Assigned(IssueProp) and (IssueProp.Text <> '') and (IssueProp.Text <> '0') then
        begin
          Issue := Result.AddParams(IssueNbr);
          Issue.AddValue('ResoNbr', IssueProp.Text);
          Issue.AddValue('Complete', IssueElem.GetBoolAttr('complete'));

          IssueProp := IssueElem.SelectNode('/shortDescription') as IXmlNode;
          if Assigned(IssueProp) then
            Issue.AddValue('Description', IssueProp.Text);

          IssueProp := IssueElem.SelectNode('/status') as IXmlNode;
          if Assigned(IssueProp) then
            Issue.AddValue('Status', IssueProp.Text)
          else
            Issue.AddValue('Status', '');
        end;
      end;
    end;
  end;
end;

function TpbAccuRevRepository.ExecXMLRequest(const aXMLrequest: String): IpbServerResponse;
var
  Cmd: String;
  XmlRequestFile: IisStream;
  XmlRequestFileName: String;
  N: IXmlNode;
begin
  XmlRequestFileName := AppTempFolder + GetUniqueID + '.xml';
  XmlRequestFile := TisStream.CreateFromNewFile(XmlRequestFileName);
  XmlRequestFile.AsString := aXMLrequest;
  XmlRequestFile := nil;

  Cmd := Format('xml -H -l "%s"', [XmlRequestFileName]);
  try
    Result := ExecCmd(Cmd);
  finally
    DeleteFile(XmlRequestFileName);
  end;

  if Assigned( Result.XML.DocumentElement) then
  begin
    if Result.XML.DocumentElement.NodeName = 'badFormat' then
    begin
      N := Result.XML.DocumentElement;
      N.Text := 'Bad command format';
    end
    else
    begin
      N := Result.XML.DocumentElement.SelectNode('/Message') as IXmlNode;
      if not Assigned(N) then
        N := Result.XML.DocumentElement.SelectNode('/warning') as IXmlNode;
    end;

    if Assigned(N) then
      raise EisException.CreateFmt('AccuRev command has failed. %s', [N.Text]);
  end;
end;

procedure TpbAccuRevRepository.CreateBuildNotes(const AProject: IpbProject);
var
  BuildNotes: IisStream;
  Issues: IisParamsCollection;
  Issue: IisListOfValues;
  i: Integer;
  IssueList: IisStringList;
  sPage: String;
  sTableContent: String;
  HR: HRSRC;
  SR: Cardinal;
  PR: Pointer;

  procedure BuildGroup(const AStatus, ADescription: String);
  var
    sIssueList, s: String;
    UsedItems: IisStringList;
    Issue: IisListOfValues;    
    i: Integer;
  begin
    UsedItems := TisStringList.Create;
    sIssueList := '';
    for i := 0 to IssueList.Count - 1 do
    begin
      Issue := IssueList.Objects[i] as IisListOfValues;
      if (AStatus = '') or AnsiSameText(Issue.TryGetValue('Status', ''), AStatus) then
      begin
        if not Issue.Value['Complete'] then
          s := 'class="IncompleteIssue"'
        else
          s := '';
        s := Format('<tr %s><td>%s</td><td>%s</td><td>%s</td></tr>',
          [s, Issue.Value['ResoNbr'], Issue.TryGetValue('Status', ''), EncodeToHTML(Issue.Value['Description'])]);
        AddStrValue(sIssueList, s, #13);

        UsedItems.Add(IssueList[i]);
      end;
    end;

    for i := 0 to UsedItems.Count - 1 do
      IssueList.Delete(IssueList.IndexOf(UsedItems[i]));

    if sIssueList <> '' then
    begin
      AddStrValue(sTableContent, Format('<tr><th bgcolor="#FFFFCC" colspan="3"><strong>%s</strong></th></tr>', [ADescription]), #13);
      AddStrValue(sTableContent, sIssueList, #13);
    end;
  end;


begin
  HR := FindResource(HINSTANCE, 'build_notes', 'HTML');
  Assert(HR <> 0);
  SR := SizeofResource(HINSTANCE, HR);
  Assert(SR > 0);
  PR := Pointer(LoadResource(HINSTANCE, HR));
  SetLength(sPage, SR);
  CopyMemory(@sPage[1], PR, SR);

  sPage := StringReplace(sPage, '$product$', AProject.Product.Name, [rfReplaceAll]);
  sPage := StringReplace(sPage, '$version$', AProject.Version.AsString, [rfReplaceAll]);
  sPage := StringReplace(sPage, '$notes$', EncodeToHTML(AProject.Description), [rfReplaceAll]);

  Issues := IInterface(AProject.Properties.Value['Issues']) as IisParamsCollection;
  IssueList := TisStringList.Create;
  for i := 0 to Issues.Count - 1 do
  begin
    Issue := Issues[i];
    IssueList.AddObject(FormatFloat('000000000', Issue.Value['ResoNbr']), Issue);
  end;
  IssueList.Sort;

  sTableContent := '';
  BuildGroup('Testing', 'Ready For Testing');
  BuildGroup('Programming', 'Went Back To Development');
  BuildGroup('Verified', 'Verified');
  BuildGroup('To Be Documented',  'To Be Documented');
  BuildGroup('Closed', 'Closed');
  BuildGroup('', 'Miscellaneous');

  sPage := StringReplace(sPage, '$issue_list$', sTableContent, [rfReplaceAll]);

  BuildNotes := TisStream.CreateFromNewFile(NormalizePath(AProject.GetPropValue('BinDir')) + 'BuildNotes.html');
  BuildNotes.AsString := sPage;
end;

function TpbAccuRevRepository.AddIssue(const AIssue: IisListOfValues): IisListOfValues;
const
  ACCUREV_NEW_ISSUE = '<newIssue issueDB="%s"><issue>%s</issue><action name="none"></action></newIssue>';
var
  Res: IpbServerResponse;
  i: Integer;
  IssueElem: IXmlNode;
  sCmd: String;

  function IssueDescr: String;
  var
    i: Integer;
    sFldName, sFldVal, sFldID: String;
  begin
    Result := '';
    for i := 0 to AIssue.Count - 1 do
    begin
      sFldName := AIssue[i].Name;
      sFldVal := AIssue[i].Value;
      sFldID := IssueFields.Value[sFldName];
      Result := Result + Format('<%s fid="%s">%s</%s>', [sFldName, sFldID, sFldVal, sFldName]);
    end;
  end;

begin
  sCmd := Format(ACCUREV_NEW_ISSUE, [Mainboard.Config.AsString['Config\SCMDepot'], IssueDescr]);
  Res := (Mainboard.ProjectBuilder.Repository as IpbAccuRev).ExecXMLRequest(sCmd);

  Result := AIssue;
  IssueElem := Res.XML.DocumentElement;
  for i := 0 to IssueElem.ChildNodes.Count - 1 do
    Result.AddValue(IssueElem.ChildNodes[i].NodeName, IssueElem.ChildNodes[i].Text);
end;

function TpbAccuRevRepository.GetIssues(const AQuery: String): IisParamsCollection;
const
  ACCUREV_ISSUE_QUERY = '<queryIssue issueDB="%s" useAltQuery="false">%s</queryIssue>';
var
  Res: IpbServerResponse;
  L: IisList;
  i, j: Integer;
  Issue: IisListOfValues;
  IssueElem, IssueProp: IXmlNode;
  sQuery: String;

  function PatchQuery(const AQuery: String): String;
  var
    i: Integer;
  begin
    // Patch quotes
    Result := StringReplace(AQuery, '"', '&quot;', [rfReplaceAll]);

    for i := IssueFields.Count - 1 downto 0 do  // start from longest field names
      Result := StringReplace(Result, IssueFields[i].Name, IssueFields[i].Value, [rfReplaceAll, rfIgnoreCase]);

    if not Contains(Result, '<condition>') then
      Result := '<AND><condition>' + Result + '</condition></AND>';
  end;

begin
  Result := TisParamsCollection.Create;

  sQuery := Format(ACCUREV_ISSUE_QUERY, [Mainboard.Config.AsString['Config\SCMDepot'], PatchQuery(AQuery)]);
  Res := (Mainboard.ProjectBuilder.Repository as IpbAccuRev).ExecXMLRequest(sQuery);

  L := Res.XML.DocumentElement.SelectNodes('/issue');
  for i := 0 to L.Count - 1 do
  begin
    IssueElem := L[i] as IXmlNode;

    IssueProp := IssueElem.SelectNode('/issueNum') as IXmlNode;
    if Assigned(IssueProp) then
    begin
      Issue := Result.AddParams(IssueProp.Text);
      for j := 0 to IssueElem.ChildNodes.Count - 1 do
        Issue.AddValue(IssueElem.ChildNodes[j].NodeName, IssueElem.ChildNodes[j].Text);
    end;
  end;
end;

procedure TpbAccuRevRepository.UpdateIssue(const AIssue: IisListOfValues);
const
  ACCUREV_MODIFY_ISSUE = '<modifyIssue issueDB="%s"><issue>%s</issue><action name="none"></action></modifyIssue>';
var
  sCmd: String;

  function IssueDescr: String;
  var
    i: Integer;
    sFldName, sFldVal, sFldID: String;
  begin
    Result := '';
    for i := 0 to AIssue.Count - 1 do
    begin
      sFldName := AIssue[i].Name;
      sFldVal := AIssue[i].Value;
      sFldID := IssueFields.Value[sFldName];
      Result := Result + Format('<%s fid="%s">%s</%s>', [sFldName, sFldID, sFldVal, sFldName]);
    end;
  end;

begin
  sCmd := Format(ACCUREV_MODIFY_ISSUE, [Mainboard.Config.AsString['Config\SCMDepot'], IssueDescr]);
  (Mainboard.ProjectBuilder.Repository as IpbAccuRev).ExecXMLRequest(sCmd);
end;

function TpbAccuRevRepository.IssueFields: IisListOfValues;

  procedure BuildIssueFields;
  var
    Res: IpbServerResponse;
    L: IisList;
    i: Integer;
    FieldElem: IXmlNode;
  begin
    Res := ExecCmd(Format('getconfig -p %s -r schema.xml', [Mainboard.Config.AsString['Config\SCMDepot']]));

    FIssueFields := TisListOfValues.Create;
    L := Res.XML.DocumentElement.SelectNodes('/field');
    for i := 0 to L.Count - 1 do
    begin
      FieldElem := L[i] as IXmlNode;
      FIssueFields.AddValue(FieldElem.GetAttr('name'), FieldElem.GetAttr('fid'));
    end;
    FIssueFields.Sort;
  end;

begin
  if not Assigned(FIssueFields) then
    BuildIssueFields;

  Result := FIssueFields;
end;

function TpbAccuRevRepository.GetUserList: IisListOfValues;
var
  Res: IpbServerResponse;
  i: Integer;
  Elem: IXmlNode;
begin
  Result := TisListOfValues.Create;

  Res := ExecCmd('show -H -fx users');

  for i := 0 to Res.XML.DocumentElement.ChildNodes.Count - 1 do
  begin
    Elem := Res.XML.DocumentElement.ChildNodes[i];
    Result.AddValue(Elem.GetAttr('Name'), Elem.GetAttr('Number'));
  end;
end;

procedure TpbAccuRevRepository.CreateStream(const AName, AParentName: String);
var
  cmd: String;
  Res: IpbServerResponse;
begin
  // Check if it is deleted stream
  cmd := Format('show -H -fx -fi -s "%s" streams', [AName]);
  Res := ExecCmd(cmd);
  if (Res.XML.DocumentElement.ChildNodes.Count = 1) and
      Res.XML.DocumentElement.ChildNodes[0].GetBoolAttr('hidden') then
  begin
    // Undelete stream
    cmd := Format('reactivate -H stream "%s"', [AName, AParentName]);
    ExecCmd(cmd);

    // Reparent stream
    MoveStream(AName, AParentName);
  end

  else
  begin
    // Create new stream
    cmd := Format('mkstream -H -s "%s" -b "%s"', [AName, AParentName]);
    ExecCmd(cmd);
  end;  
end;

procedure TpbAccuRevRepository.DeleteStream(const AName: String);
var
  cmd: String;
begin
  cmd := Format('remove -H stream "%s"', [AName]);
  ExecCmd(cmd);
end;

procedure TpbAccuRevRepository.MoveStream(const AName, AParentName: String);
var
  cmd, OldParentName: String;
  Res: IpbServerResponse;
  i: Integer;
  Node: IXmlNode;
begin
  // Detach worspaces
  cmd := Format('show -H -fx -R -s "%s" streams', [AName]);
  Res := ExecCmd(cmd);

  Node := Res.XML.DocumentElement.SelectNode(Format('/stream[@name="%s"]', [AName])) as IXmlNode;
  if not Assigned(Node) then
    raise EisException.CreateFmt('Stream %s is not found', [AName]);
  OldParentName := Node.GetAttr('basis');

  if OldParentName <> AParentName then
  begin
    for i := 0 to Res.XML.DocumentElement.ChildNodes.Count - 1 do
    begin
      Node := Res.XML.DocumentElement.ChildNodes[i];
      if (Node.GetAttr('type') = 'workspace') and (Node.GetAttr('basis') = AName) then
      begin
        cmd := Format('chws -H -s "%s" -b "%s"', [Node.GetAttr('name'), OldParentName]);
        ExecCmd(cmd);
      end;
    end;

    // Reparent stream
    cmd := Format('chstream -H -s "%s" -b "%s"', [AName, AParentName]);
    ExecCmd(cmd);
  end;
end;

function TpbAccuRevRepository.StreamExists(const AName: String): Boolean;
var
  cmd: String;
  Res: IpbServerResponse;
begin
  cmd := Format('show -H -fx -s "%s" streams', [AName]);
  Res := ExecCmd(cmd);
  Result := (Res.XML.DocumentElement.ChildNodes.Count = 1) and
    not Res.XML.DocumentElement.ChildNodes[0].GetBoolAttr('hidden');
end;

function TpbAccuRevRepository.GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;
var
  ProjIssues, CurrentIssues: IisParamsCollection;
  ProjIssue, CurIssue: IisListOfValues;
  MinNum, MaxNum: Integer;
  i: Integer;
begin
  ProjIssues := IInterface(AProject.Properties.Value['Issues']) as IisParamsCollection;
  Result := ProjIssues.GetClone;

  if Result.Count = 0 then
    Exit;

  MinNum := StrToIntDef(Result.ParamName(0), 0);
  MaxNum := StrToIntDef(Result.ParamName(Result.Count - 1), 100000000);

  CurrentIssues := GetIssues(Format('<AND><condition>issueNum &gt;= "%d"</condition><condition>issueNum &lt;= "%d"</condition></AND>', [MinNum, MaxNum]));

  for i := 0 to Result.Count - 1 do
  begin
    CurIssue := CurrentIssues.ParamsByName(Result.ParamName(i));
    if Assigned(CurIssue) then
    begin
      ProjIssue := Result[i];
      ProjIssue.Value['ResoNbr'] := CurIssue.Value['resoNum'];
      ProjIssue.Value['Description'] := CurIssue.Value['shortDescription'];
      ProjIssue.Value['Status'] := CurIssue.Value['Status'];
    end;
  end;
end;

procedure TpbAccuRevRepository.UpdateIssueList(const AProject: IpbProject);
var
  BuildInfo: IisSettings;
  Issues: IisParamsCollection;
  Issue: IisListOfValues;
  i, j: Integer;
  sIssueNbr: String;
begin
  if not AProject.Properties.ValueExists('Issues') then
    Exit;

  Issues := GetUpdatedIssues(AProject);
  AProject.Properties.Value['Issues'] := Issues;

  // Update Issue info
  BuildInfo := TisSettingsIni.Create(NormalizePath(AProject.GetPropValue('BinDir')) + 'BuildInfo.ini');

  for i := 0 to Issues.Count - 1 do
  begin
    sIssueNbr := Issues.ParamName(i);
    Issue := Issues[i];
    for j := 0 to Issue.Count - 1 do
      BuildInfo.AsString['Issue_' + sIssueNbr + '\' + Issue[j].Name] := Issue[j].Value;
  end;
end;

procedure TpbAccuRevRepository.CheckIssueStatus(const AIssues: IisParamsCollection;
  const AValidStatuses, AInValidStatuses: array of string);
var
  Issue: IisListOfValues;
  s: String;
  i, j: Integer;
  bValid: Boolean;
  sError: String;
begin
  sError := '';
  for i := 0 to AIssues.Count - 1 do
  begin
    Issue := AIssues[i];
    s := Issue.Value['Status'];

    if Length(AValidStatuses) > 0 then
    begin
      bValid := False;
      for j := Low(AValidStatuses) to High(AValidStatuses) do
        if AnsiSameText(s, AValidStatuses[j]) then
        begin
          bValid := True;
          break;
        end;
    end
    else
      bValid := True;

    if bValid and (Length(AInValidStatuses) > 0) then
    begin
      for j := Low(AInValidStatuses) to High(AInValidStatuses) do
        if AnsiSameText(s, AInValidStatuses[j]) then
        begin
          bValid := False;
          break;
        end;
    end;

    if not bValid then
      AddStrValue(sError, Format('Issue #%s has invalid status "%s".', [Issue.Value['ResoNbr'], s]), #13);
  end;

  if sError <> '' then
    raise EisException.Create(sError);
end;

end.
