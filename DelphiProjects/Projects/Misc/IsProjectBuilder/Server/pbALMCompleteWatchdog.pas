unit pbALMCompleteWatchdog;

interface

uses
  Windows, SysUtils, ADODB, ActiveX,
  isLogFile, isBaseClasses, isThreadManager, isErrorUtils, isBasicUtils,
  isSocket;

type
  IpbALMCompleteWatchdog = interface
  ['{45287A44-404F-4F73-A8A6-A8B871B4D6ED}']
    procedure SetActive(const AValue: Boolean);
    function  GetActive: Boolean;
    property  Active: Boolean read GetActive write SetActive;
  end;

  function CreateALMCompleteWatchdog: IpbALMCompleteWatchdog;

implementation

uses pbSrvMainboard, isSettings, DB;

type
  TALMDBCheckQuery = class;
  TpbALMChangeListener = class;
  TpbALMSyncThread = class;

  TpbALMCompleteWatchdog = class(TisInterfacedObject, IpbALMCompleteWatchdog)
  private
    FALMChangeListener: TpbALMChangeListener;
  protected
    procedure DoOnDestruction; override;
    procedure SetActive(const AValue: Boolean);
    function  GetActive: Boolean;
  end;


  TpbALMSyncThread = class(TReactiveThread)
  private
    FDBConnection: TADOConnection;
    FCheckDBQuery: TALMDBCheckQuery;
    FLastChangeTime: TDateTime;
  protected
    procedure DoOnStart; override;
    procedure DoWork; override;
    procedure DoOnStop; override;
  end;


  TALMDBCheckQuery = class(TADOQuery)
  private
    FSQL: String;
    FUserList: IisListOfValues;
    procedure UpdateIssue;
    function  GetIssueNum(const AAlmId: String): String;
    procedure SyncIssueStream;
    function  CreateIssueFields: IisListOfValues;
    function  GetUserID(AUser: String): String;
  public
    procedure AfterConstruction; override;

    procedure Prepare;
    procedure Open(const ALastChangeDate: TDateTime); reintroduce;
    procedure PromoteChangesToIssueTracking;
    function  GetLatestChange: TDateTime;

    // Current record fields
    function  ID: String;
    function  Title: String;
    function  Description: String;
    function  ProductName: String;
    function  Status: String;
    function  CreatedBy: String;
    function  AssignedTo: String;
    function  DateUpdated: TDateTime;
  end;


  TpbALMChangeListener = class
  private
    FALMSyncThread: TpbALMSyncThread;
    FTaskID: TTaskID;
    function  IsActive: Boolean;
    procedure ChangesListeningThread(const Params: TTaskParamList);
    procedure Start;
    procedure Stop;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;


  TpbALMEventDatagram = record
    Ver:  String;
    ID:   String;
    Data: String;
  end;



function CreateALMCompleteWatchdog: IpbALMCompleteWatchdog;
begin
  Result := TpbALMCompleteWatchdog.Create;
end;

{ TpbALMCompleteWatchdog }

procedure TpbALMCompleteWatchdog.DoOnDestruction;
begin
  SetActive(False);
  inherited;
end;

function TpbALMCompleteWatchdog.GetActive: Boolean;
begin
  Result := Assigned(FALMChangeListener);
end;

procedure TpbALMCompleteWatchdog.SetActive(const AValue: Boolean);
begin
  if GetActive <> AValue then
  begin
    if AValue then
      FALMChangeListener := TpbALMChangeListener.Create
    else
      FreeAndNil(FALMChangeListener);
  end;
end;

{ TALMDBCheckQuery }

procedure TALMDBCheckQuery.AfterConstruction;
begin
  inherited;

  FSQL :=
      'SELECT'#13 +
      '  CAST(''D'' as CHAR(1)) as ItemType,'#13 +
      '  b.BugId as ID,'#13 +
      '  b.Title as Title,'#13 +
      '  b.Description as Description,'#13 +
      '  f.FolderName as ProductName,'#13 +
      '  b.StatusCode as Status,'#13 +
      '  p1.Custom1 as CreatedBy,'#13 +
      '  p2.Custom1 as AssignedTo,'#13 +
      '  b.DateUpdated as DateUpdated'#13 +
      'FROM Bugs b, Folders f, Persons p1, Persons p2'#13 +
      'WHERE'#13 +
      '  b.FolderId = f.FolderId AND'#13 +
      '  b.OwnerUserId = p1.UserId AND'#13 +
      '  b.AssigneeUserId = p2.UserId AND'#13 +
      '  b.Custom1 = ''Yes'' AND'#13 +
      '  b.DateUpdated > :LastChangeDate1'#13 +

      'UNION ALL'#13 +

      'SELECT'#13 +
      '  CAST('#39'R'#39' as CHAR(1)) as ItemType,'#13 +
      '  s.FunctSpecId as ID,'#13 +
      '  s.Title as Title,'#13 +
      '  s.Description as Description,'#13 +
      '  f.FolderName as ProductName,'#13 +
      '  s.StatusCode as Status,'#13 +
      '  p1.Custom1 as CreatedBy,'#13 +
      '  p2.Custom1 as AssignedTo,'#13 +
      '  s.DateUpdated as DateUpdated'#13 +
      'FROM FunctionalSpecs s, Folders f, Persons p1, Persons p2'#13 +
      'WHERE'#13 +
      '  s.FolderId = f.FolderId AND'#13 +
      '  s.OwnerUserId = p1.UserId AND'#13 +
      '  s.AssigneeUserId = p2.UserId AND'#13 +
      '  s.Custom1 = ''Yes'' AND'#13 +
      '  s.DateUpdated > :LastChangeDate2'#13 +

      'ORDER BY DateUpdated';

  FUserList := Mainboard.ProjectBuilder.IssueTracking.GetUserList;
end;

function TALMDBCheckQuery.CreateIssueFields: IisListOfValues;
var
  s: String;
begin
  Result := TisListOfValues.Create;

  Result.AddValue('status', Status);
  Result.AddValue('shortDescription', Title);
  Result.AddValue('state', 'Open');
  Result.AddValue('productType', ProductName);

  if StartsWith(ID, 'D') then
    s := 'Software Defect'
  else if StartsWith(ID, 'R') then
    s := 'Software Enhancement'
  else
    s := '';
  Result.AddValue('type', s);

  Result.AddValue('severity', '');
  Result.AddValue('priority', '');
  Result.AddValue('submittedBy', GetUserID(CreatedBy));
  Result.AddValue('assignedTo', GetUserID(AssignedTo));
  Result.AddValue('description', Description);
  Result.AddValue('foundInRelease', '');
  Result.AddValue('targetRelease', '');
  Result.AddValue('category', '');
  Result.AddValue('resoNum', ID);
  Result.AddValue('dateSubmitted', DateTimeToUnixEpoch(DateUpdated));
end;

function TALMDBCheckQuery.ID: String;
begin
  Result := FieldByName('ItemType').AsString + FieldByName('ID').AsString;
end;

function TALMDBCheckQuery.GetIssueNum(const AAlmId: String): String;
var
  IssueList: IisParamsCollection;
begin
  IssueList := Mainboard.ProjectBuilder.IssueTracking.GetIssues(Format('<OR><condition>resoNum == "%s"</condition></OR>', [AAlmId]));
  if IssueList.Count > 0 then
    Result := IssueList[0].Value['issueNum']
  else
    Result := '';
end;

function TALMDBCheckQuery.GetLatestChange: TDateTime;
var
  BM: Pointer;
begin
  BM := GetBookmark;
  try
    Last;
    Result := DateUpdated;
    GotoBookmark(BM);
  finally
    FreeBookmark(BM);
  end;
end;

procedure TALMDBCheckQuery.SyncIssueStream;
var
  IssueStreamName: String;
  ParentStreamName: String;
  bStreamExists: Boolean;
  sProductAbbr, sUser: String;
  ProductsInfo: IisListOfValues;

  function IsProgrammingIssue: Boolean;
  begin
    Result := AnsiSameText(Status, 'Programming');
  end;

  function IsVerifiedIssue: Boolean;
  begin
    Result := AnsiSameText(Status, 'Verified');
  end;

  function IsClosedIssue: Boolean;
  begin
    Result := AnsiSameText(Status, 'Closed');
  end;

begin
  // Products, which comply with issue streams rules

  ProductsInfo := TisListOfValues.Create;
  ProductsInfo.AddValue('Big Brother/Licensing', 'evo');
  ProductsInfo.AddValue('EvoTools', 'evo');
  ProductsInfo.AddValue('Evolution', 'evo');
  ProductsInfo.AddValue('Resolution', 'reso');
  ProductsInfo.AddValue('Report Writer', 'rw');

  sProductAbbr := ProductsInfo.TryGetValue(ProductName, '');
  if sProductAbbr = '' then
    Exit;

  IssueStreamName := 'res_' + ID;

  sUser := AssignedTo;

  if sUser = '' then
    ParentStreamName := ''
  else
  begin
    ParentStreamName := sProductAbbr + '_d_' + sUser;
    if not Mainboard.ProjectBuilder.IssueTracking.StreamExists(ParentStreamName) then
      ParentStreamName := sProductAbbr + '_qa_int';
  end;

  bStreamExists := Mainboard.ProjectBuilder.IssueTracking.StreamExists(IssueStreamName);

  if not bStreamExists and IsProgrammingIssue then
  begin
    if (ParentStreamName <> '') and Mainboard.ProjectBuilder.IssueTracking.StreamExists(ParentStreamName) and
       (GetIssueNum(ID) <> '') then
      Mainboard.ProjectBuilder.IssueTracking.CreateStream(IssueStreamName, ParentStreamName);
  end

  else if bStreamExists and IsProgrammingIssue then
  begin
    if (ParentStreamName <> '') and Mainboard.ProjectBuilder.IssueTracking.StreamExists(ParentStreamName) then
      Mainboard.ProjectBuilder.IssueTracking.MoveStream(IssueStreamName, ParentStreamName);
  end

  else if bStreamExists and IsVerifiedIssue then
    Mainboard.ProjectBuilder.IssueTracking.MoveStream(IssueStreamName, 'evo_d_tested')

  else if bStreamExists and IsClosedIssue then
  begin
    Mainboard.ProjectBuilder.IssueTracking.MoveStream(IssueStreamName, 'evo_d_tested');
    Mainboard.ProjectBuilder.IssueTracking.DeleteStream(IssueStreamName);
  end;
end;

procedure TALMDBCheckQuery.UpdateIssue;
var
  Fields: IisListOfValues;
  IssueNum: String;
begin
  Fields := CreateIssueFields;

  IssueNum := GetIssueNum(ID);
  if IssueNum = '' then
    Fields := Mainboard.ProjectBuilder.IssueTracking.AddIssue(Fields)
  else
  begin
    Fields.AddValue('issueNum', IssueNum);
    Mainboard.ProjectBuilder.IssueTracking.UpdateIssue(Fields);
  end;

  SyncIssueStream;
end;

function TALMDBCheckQuery.AssignedTo: String;
begin
  Result := FieldByName('AssignedTo').AsString;
end;

function TALMDBCheckQuery.CreatedBy: String;
begin
  Result := FieldByName('CreatedBy').AsString;
end;

function TALMDBCheckQuery.DateUpdated: TDateTime;
begin
  Result := FieldByName('DateUpdated').AsDateTime;
end;

function TALMDBCheckQuery.Description: String;
begin
  Result := FieldByName('Description').AsString;
end;

function TALMDBCheckQuery.ProductName: String;
begin
  Result := FieldByName('ProductName').AsString;
end;

function TALMDBCheckQuery.Status: String;
begin
  Result := FieldByName('Status').AsString;
end;

function TALMDBCheckQuery.Title: String;
begin
  Result := FieldByName('Title').AsString;
end;

function TALMDBCheckQuery.GetUserID(AUser: String): String;
begin
   if not FUserList.ValueExists(AUser) then
     AUser := 'admin';
     
   Result := FUserList.Value[AUser];
end;

procedure TALMDBCheckQuery.Open(const ALastChangeDate: TDateTime);
begin
  Close;

  Parameters.ParamByName('LastChangeDate1').Value := ALastChangeDate;
  Parameters.ParamByName('LastChangeDate2').Value := ALastChangeDate;

  inherited Open;
end;

procedure TALMDBCheckQuery.PromoteChangesToIssueTracking;
begin
  First;
  while not Eof do
  begin
    UpdateIssue;
    Next;
  end;
end;

procedure TALMDBCheckQuery.Prepare;
begin
  Prepared := False;

  SQL.Text := '';
  SQL.Text := FSQL;

  Prepared := True;
end;

{ TpbALMChangeListener }

procedure TpbALMChangeListener.AfterConstruction;
begin
  inherited;
  Start;
end;

procedure TpbALMChangeListener.BeforeDestruction;
begin
  Stop;
  inherited;
end;

procedure TpbALMChangeListener.ChangesListeningThread(const Params: TTaskParamList);
var
  DataStr: String;
  Datagram: TpbALMEventDatagram;
  Socket: IisUDPSocket;
begin
  Socket := TisUDPSocket.Create('', 9499);

  while not CurrentThreadTaskTerminated do
  begin
    DataStr := Socket.ReceiveString;

    if DataStr <> '' then
    begin
      ZeroMemory(@Datagram, SizeOf(Datagram));
      if GetNextStrValue(DataStr, ' ') = 'ALMEVENT' then
      begin
        Datagram.Ver := GetNamedValue(DataStr, 'VER', ' ');

        if Datagram.VER = '1' then
        begin
          GetNextStrValue(DataStr, ' ');
          Datagram.ID := GetNamedValue(DataStr, 'ID', ' ');
          GetNextStrValue(DataStr, ' ');
          Datagram.DATA := DataStr;

          if Datagram.ID = 'TBLCH' then
            if Assigned(FALMSyncThread) then
              FALMSyncThread.Wakeup;
        end;
      end;
    end;
  end;
end;

function TpbALMChangeListener.IsActive: Boolean;
begin
  Result := (FTaskID <> 0) and not GlobalThreadManager.TaskIsTerminated(FTaskID);
  if not Result then
    FTaskID := 0;
end;

procedure TpbALMChangeListener.Start;
begin
  if not IsActive then
  begin
    FALMSyncThread := TpbALMSyncThread.Create('ALM Data Synchronizer');
    FALMSyncThread.WaitTime := Mainboard.Config.GetValue('Config\ALMAsyncCheckInterval', 0) * 1000;

    FTaskID := GlobalThreadManager.RunTask(ChangesListeningThread, Self, MakeTaskParams([]), 'DB Change Listener');
  end;
end;

procedure TpbALMChangeListener.Stop;
begin
  if IsActive then
  begin
    GlobalThreadManager.TerminateTask(FTaskID);
    GlobalThreadManager.WaitForTaskEnd(FTaskID);
    FTaskID := 0;

    FreeAndNil(FALMSyncThread);
  end;
end;

{ TpbALMSyncThread }

procedure TpbALMSyncThread.DoOnStart;
begin
  inherited;
  CoInitialize(nil);

  FDBConnection := TADOConnection.Create(nil);
  FDBConnection.LoginPrompt := False;

  FCheckDBQuery := TALMDBCheckQuery.Create(nil);
  FCheckDBQuery.Connection := FDBConnection;

  FLastChangeTime := Mainboard.Settings.GetValue('ALMComplete\LastChangeTime', Now);
end;

procedure TpbALMSyncThread.DoOnStop;
begin
  FreeAndNil(FCheckDBQuery);
  FreeAndNil(FDBConnection);
  CoUninitialize;
  inherited;
end;

procedure TpbALMSyncThread.DoWork;
begin
  try
    if not FDBConnection.Connected then
    begin
      FDBConnection.ConnectionString := Mainboard.Config.AsString['Config\ALMConnectionString'];
      FDBConnection.Open;
      FCheckDBQuery.Prepare;
    end;

    FCheckDBQuery.Open(FLastChangeTime);
    try
      if FCheckDBQuery.RecordCount > 0 then
      begin
        FCheckDBQuery.PromoteChangesToIssueTracking;

        FLastChangeTime := FCheckDBQuery.GetLatestChange;
        Mainboard.Settings.AsDateTime['ALMComplete\LastChangeTime'] := FLastChangeTime;
      end;
    finally
      FCheckDBQuery.Close;
    end;
  except
    on E: Exception do
      Mainboard.LogFile.AddEvent(etError, 'ALM Data Synchronizer', E.Message, GetStack(E));
  end;
end;

end.
