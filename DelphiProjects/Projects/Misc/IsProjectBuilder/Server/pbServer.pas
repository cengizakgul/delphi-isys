unit pbServer;

interface

uses SysUtils, isBaseClasses, isBasicUtils, EvTransportShared, EvTransportDatagrams, evTransportInterfaces,
     pbRemoteMethods, pbInterfaces;

type
  IpbTCPServer = interface
  ['{29CDB8C4-C830-4540-A73D-06189C4B2342}']
    function  GetPort: String;
    procedure SetPort(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  Active: Boolean read GetActive write SetActive;
    property  Port: String read GetPort write SetPort;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
  end;

function CreateTCPServer: IpbTCPServer;

implementation

uses pbSrvMainboard, Classes, pbProjectBuilder;

type
  TpbServerDispatcher = class(TevDatagramDispatcher)
  private
    function GetProductParam(const ADatagram: IevDatagram): IpbProduct;
    function GetProjectParam(const ADatagram: IevDatagram): IpbProject;
  protected
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;

    procedure Broadcast(const ADatagram: IevDatagram);

    procedure dmCurrentUserInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmChangeUserPassword(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmGetProducts(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmUpdateProjects(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetUpdatedIssues(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRunTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetCurrentTask(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmIssueTracking_GetUserList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_GetIssues(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_AddIssue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_UpdateIssue(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_StreamExists(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_CreateStream(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_MoveStream(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIssueTracking_DeleteStream(const ADatagram: IevDatagram; out AResult: IevDatagram);

    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;
    procedure RegisterHandlers; override;
  end;


  TpbTCPServer = class(TevCustomTCPServer, IpbTCPServer)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


function CreateTCPServer: IpbTCPServer;
begin
  Result := TpbTCPServer.Create;
  Result.Port := '9498';
end;

{ TpbTCPServer }

function TpbTCPServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TpbServerDispatcher.Create;
end;


{ TpbServerDispatcher }

procedure TpbServerDispatcher.dmIssueTracking_AddIssue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.ProjectBuilder.IssueTracking.AddIssue(IInterface(ADatagram.Params.Value['AIssue']) as IisListOfValues);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.dmIssueTracking_GetIssues(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := Mainboard.ProjectBuilder.IssueTracking.GetIssues(ADatagram.Params.Value['AQuery']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.dmIssueTracking_GetUserList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.ProjectBuilder.IssueTracking.GetUserList;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.dmIssueTracking_UpdateIssue(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ProjectBuilder.IssueTracking.UpdateIssue(IInterface(ADatagram.Params.Value['AIssue']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TpbServerDispatcher.dmIssueTracking_CreateStream(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ProjectBuilder.IssueTracking.CreateStream(ADatagram.Params.Value['AName'], ADatagram.Params.Value['AParentName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TpbServerDispatcher.dmIssueTracking_DeleteStream(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ProjectBuilder.IssueTracking.DeleteStream(ADatagram.Params.Value['AName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TpbServerDispatcher.dmGetProducts(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IpbProducts;
begin
  Res := Mainboard.ProjectBuilder.GetProducts;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.dmIssueTracking_MoveStream(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ProjectBuilder.IssueTracking.MoveStream(ADatagram.Params.Value['AName'], ADatagram.Params.Value['AParentName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TpbServerDispatcher.dmIssueTracking_StreamExists(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := Mainboard.ProjectBuilder.IssueTracking.StreamExists(ADatagram.Params.Value['AName']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.dmUpdateProjects(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Product: IpbProduct;
begin
  Product := GetProductParam(ADatagram);
  Mainboard.ProjectBuilder.UpdateProjects(Product);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('ProductData', (Product as IisInterfacedObject).AsStream);
end;

function TpbServerDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
var
  UserInfo: IpbUserInfo;
begin
  Result := inherited DoOnLogin(ADatagram);
  if Result then
  begin
    UserInfo := Mainboard.ProjectBuilder.FindUser(ADatagram.Header.User);
    Result := Assigned(UserInfo) and ((UserInfo.Password  = '') or (UserInfo.Password = ThisSession.Password));
  end;
end;

procedure TpbServerDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_CURRENTUSERINFO, dmCurrentUserInfo);
  AddHandler(METHOD_CHANGEUSERPASSWORD, dmChangeUserPassword);

  AddHandler(METHOD_GETPRODUCTS, dmGetProducts);
  AddHandler(METHOD_GETUPDATEDISSUES, dmGetUpdatedIssues);
  AddHandler(METHOD_UPDATEPROJECTS, dmUpdateProjects);

  AddHandler(METHOD_RUNTASK, dmRunTask);
  AddHandler(METHOD_GETCURRENTTASK, dmGetCurrentTask);

  AddHandler(METHOD_ISSUETRACKING_GETUSERLIST, dmIssueTracking_GetUserList);
  AddHandler(METHOD_ISSUETRACKING_GETISSUES, dmIssueTracking_GetIssues);
  AddHandler(METHOD_ISSUETRACKING_ADDISSUE, dmIssueTracking_AddIssue);
  AddHandler(METHOD_ISSUETRACKING_UPDATEISSUE, dmIssueTracking_UpdateIssue);
  AddHandler(METHOD_ISSUETRACKING_STREAMEXISTS, dmIssueTracking_StreamExists);
  AddHandler(METHOD_ISSUETRACKING_CREATESTREAM, dmIssueTracking_CreateStream);
  AddHandler(METHOD_ISSUETRACKING_MOVESTREAM, dmIssueTracking_MoveStream);
  AddHandler(METHOD_ISSUETRACKING_DELETESTREAM, dmIssueTracking_DeleteStream);
end;

procedure TpbServerDispatcher.Broadcast(const ADatagram: IevDatagram);
var
  i: Integer;
  Sessions: IisList;
  S: IevSession;
begin
  Sessions := GetSessions;
  for i := 0 to Sessions.Count - 1 do
    try
      S := Sessions[i] as IevSession;
      ADatagram.Header.SessionID := S.SessionID;
      S.SendDatagram(ADatagram);
    except
    end;
end;

procedure TpbServerDispatcher.dmGetCurrentTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Task, Res: IpbTask;
begin
  Task := Mainboard.ProjectBuilder.GetCurrentTask;
  if Assigned(Task) then
  begin
    (Task as IisInterfacedObject).Lock;
    try
      Res := (Task as IisInterfacedObject).GetClone as IpbTask;
    finally
      (Task as IisInterfacedObject).Unlock;
    end;
  end
  else
    Res := nil;
    
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.dmRunTask(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ProjectBuilder.RunTask(IInterface(ADatagram.Params.Value['ATask']) as IpbTask);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TpbServerDispatcher.dmCurrentUserInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IpbUserInfo;
begin
  Res := Mainboard.ProjectBuilder.CurrentUserInfo;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TpbServerDispatcher.AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram);
begin
  Mainboard.ProjectBuilder.SetCurrentUser(nil);
  inherited;
end;

procedure TpbServerDispatcher.BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader);
var
  Usr: IpbUserInfo;
begin
  inherited;

  if ThisSession.Login <> '' then
  begin
    Usr := Mainboard.ProjectBuilder.FindUser(ThisSession.Login);
    CheckCondition(Assigned(Usr), 'User information is not found');
  end
  else
    Usr := nil;

  Mainboard.ProjectBuilder.SetCurrentUser(Usr);
end;

procedure TpbServerDispatcher.dmChangeUserPassword(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ProjectBuilder.ChangeUserPassword(
    ADatagram.Params.Value['AOldPassword'], ADatagram.Params.Value['ANewPassword']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TpbServerDispatcher.dmGetUpdatedIssues(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Project: IpbProject;
  Res: IisParamsCollection;
begin
  Project := GetProjectParam(ADatagram);
  Res := Mainboard.ProjectBuilder.GetUpdatedIssues(Project);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

function TpbServerDispatcher.GetProductParam(const ADatagram: IevDatagram): IpbProduct;
var
  ProdName: String;
begin
  ProdName := ADatagram.Params.Value['ProductName'];
  Result := Mainboard.ProjectBuilder.Repository.GetProducts.Find(ProdName);
  CheckCondition(Assigned(Result), Format('Product %s is not found', [ProdName]));
end;

function TpbServerDispatcher.GetProjectParam(const ADatagram: IevDatagram): IpbProject;
var
  P: IpbProduct;
  ProjName: String;
begin
  P := GetProductParam(ADatagram);
  ProjName := ADatagram.Params.Value['ProjectName'];
  Result := P.FindProject(ProjName);
  CheckCondition(Assigned(Result), Format('Project %s is not found', [ProjName]));
end;

end.
