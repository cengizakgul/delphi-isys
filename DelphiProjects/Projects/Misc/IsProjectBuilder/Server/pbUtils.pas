unit pbUtils;

interface

uses Windows, SysUtils, isBaseClasses, isBasicUtils;

type
  IpbProcessExec = interface
  ['{950BF7C6-007D-4198-8378-834CB8021990}']
    procedure Run(const ACmd, AParams: String);
    procedure UpdateProgress(const AText: String);
    function  Status: String;
    function  Details: String;
  end;

  TpbProcessExec = class(TisInterfacedObject, IpbProcessExec)
  private
    FLock: IisLock;
    FStatus:  IisStringList;
    FDetails: IisStringList;
    FRawOutput: String;
    procedure ClearProgressInfo;
    procedure ProcessCallBackFragment(const AFragment: String);
  protected
    procedure DoOnConstruction; override;
    procedure Run(const ACmd, AParams: String);
    procedure UpdateProgress(const AText: String);
    function  Status: String;
    function  Details: String;
  end;


function RemoveCR(const AText: String): String;
function EncodeToHTML(const AText: String): String;

implementation

uses pbSrvMainboard;

function RemoveCR(const AText: String): String;
begin
  Result := StringReplace(AText, #13, ' ', [rfReplaceAll]);
  Result := StringReplace(Result, #10, ' ', [rfReplaceAll]);
end;

function EncodeToHTML(const AText: String): String;
begin
  Result := StringReplace(AText, #10, #13, [rfReplaceAll]);
  Result := StringReplace(Result, #13#13, #13, [rfReplaceAll]);
  Result := StringReplace(Result, #13, '<br>', [rfReplaceAll]);
end;

{ TpbProcessExec }

procedure TpbProcessExec.ClearProgressInfo;
begin
  FStatus.Clear;
  FDetails.Clear;
end;

function TpbProcessExec.Details: String;
begin
  FLock.Lock;
  try
    Result := FDetails.Text;
  finally
    FLock.Unlock;
  end;
end;

procedure TpbProcessExec.DoOnConstruction;
begin
  inherited;
  FLock := TisLock.Create;
  FStatus :=  TisStringList.Create;
  FDetails := TisStringList.Create;
end;

procedure TpbProcessExec.ProcessCallBackFragment(const AFragment: String);
var
  sLine: String;
  Fragment: IisStringList;
  i: Integer;
begin
  FRawOutput := FRawOutput + AFragment;

  if FRawOutput = '' then
    Exit;

  // Convert to lined format
  Fragment := TisStringList.Create;
  Fragment.Text := FRawOutput;

  // Keep unfinished line for the next time
  if FRawOutput[Length(FRawOutput)] <> #13 then
  begin
    FRawOutput := Fragment[Fragment.Count - 1];
    Fragment.Delete(Fragment.Count - 1);
  end
  else
    FRawOutput := '';

  // Analyze content
  FLock.Lock;
  try
    for i := 0 to Fragment.Count - 1 do
    begin
      sLine := Fragment[i];
      if StartsWith(sLine, '*') then
      begin
        Delete(sLine, 1, 1);
        FStatus.Add(sLine);
        Mainboard.ProjectBuilder.OnTaskStatusChange(sLine, 0);
      end
      else
      begin
        FDetails.Add(sLine);
        Mainboard.ProjectBuilder.OnTaskDetailChange(sLine);
      end;
    end;
  finally
    FLock.Unlock;
  end;
end;


threadvar Builder: TpbProcessExec;

procedure TpbProcessExec.Run(const ACmd, AParams: String);

  procedure ProgressCallback(const AData: Pointer; const ALength: Integer);
  var
    sFragment: String;
  begin
    // Collect frament
    SetLength(sFragment, ALength);
    CopyMemory(@sFragment[1], AData, ALength);
    sFragment := StringReplace(sFragment, #10, '', [rfReplaceAll]);
    Builder.ProcessCallBackFragment(sFragment);
  end;

begin
  ClearProgressInfo;

  // Run command
  FRawOutput := '';
  SetCurrentDir(ExtractFilePath(ACmd));
  Builder := Self;
  try
    UpdateProgress(ACmd + ' ' + AParams);
    RunProcess(ACmd, AParams, [roHideWindow, roWaitForClose], nil, nil, @ProgressCallback);
  finally
    Builder := nil;
    ProcessCallBackFragment('');
  end;
end;

function TpbProcessExec.Status: String;
begin
  FLock.Lock;
  try
    Result := FStatus.Text;
  finally
    FLock.Unlock;
  end;
end;

procedure TpbProcessExec.UpdateProgress(const AText: String);
begin
  ProcessCallBackFragment(AText + #13);
end;

end.
