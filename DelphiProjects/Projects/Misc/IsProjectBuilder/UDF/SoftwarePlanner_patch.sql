/* Broadcast functionality for Project Builder*/

sp_configure 'clr enable', 1
GO

RECONFIGURE
GO

ALTER DATABASE SoftwarePlanner SET TRUSTWORTHY ON
GO

CREATE ASSEMBLY asm_ISystemsALMComplete from 'c:\ISystems.ALMComplete.dll' WITH PERMISSION_SET = UNSAFE
GO

CREATE PROC sp_UDPBroadcast(@data as nvarchar(4000), @port as int)
AS
EXTERNAL NAME asm_ISystemsALMComplete.StoredProcedures.UDPBroadcast
GO

-- =============================================
-- Author:      Aleksey Suvorov, iSystems LLC
-- Create date: 2/16/2012
-- Description:	Sends UDP broadcast notification
-- =============================================
CREATE TRIGGER Bugs_ChangeNotify ON Bugs AFTER INSERT,UPDATE
AS 
BEGIN
  SET NOCOUNT ON;
  
  DECLARE @data nvarchar(4000), @Id int, @IdList varchar(1000);

  DECLARE Changes_Cursor CURSOR FOR SELECT BugId FROM inserted WHERE Custom1 = 'Yes';  
  OPEN Changes_Cursor;
  
  SET @IdList = '';
  FETCH NEXT FROM Changes_Cursor INTO @Id
  WHILE @@FETCH_STATUS = 0
  BEGIN
    IF @IdList != '' 
    BEGIN    
      SET @IdList = @IdList + ',';
    END
      
    SET @IdList = @IdList + CAST(@Id as varchar);
    
    FETCH NEXT FROM Changes_Cursor INTO @Id; 
  END;

  CLOSE Changes_Cursor;
  DEALLOCATE Changes_Cursor;
  
  IF @IdList != ''
  BEGIN
    SET @data = 'ALMEVENT VER=1 ID=TBLCH TBL=Bugs ID=' + @IdList;  
    EXEC sp_UDPBroadcast @data, 9499;
  END
END
GO

-- =============================================
-- Author:      Aleksey Suvorov, iSystems LLC
-- Create date: 2/16/2012
-- Description:	Sends UDP broadcast notification
-- =============================================
CREATE TRIGGER FunctionalSpecs_ChangeNotify ON FunctionalSpecs AFTER INSERT,UPDATE
AS 
BEGIN
  SET NOCOUNT ON;
  
  DECLARE @data nvarchar(4000), @Id int, @IdList varchar(1000);

  DECLARE Changes_Cursor CURSOR FOR SELECT FunctSpecId FROM inserted WHERE Custom1 = 'Yes';  
  OPEN Changes_Cursor;
  
  SET @IdList = '';
  FETCH NEXT FROM Changes_Cursor INTO @Id
  WHILE @@FETCH_STATUS = 0
  BEGIN
    IF @IdList != '' 
    BEGIN    
      SET @IdList = @IdList + ',';
    END
      
    SET @IdList = @IdList + CAST(@Id as varchar);
    
    FETCH NEXT FROM Changes_Cursor INTO @Id; 
  END;

  CLOSE Changes_Cursor;
  DEALLOCATE Changes_Cursor;
  
  IF @IdList != ''
  BEGIN
    SET @data = 'ALMEVENT VER=1 ID=TBLCH TBL=FunctionalSpecs ID=' + @IdList;
    EXEC sp_UDPBroadcast @data, 9499;
  END
END
GO
