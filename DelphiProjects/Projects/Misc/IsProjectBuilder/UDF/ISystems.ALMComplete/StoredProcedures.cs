﻿using System.Net;
using System.Net.Sockets;
using System.Text;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void UDPBroadcast(string data, int port)
    {
        UdpClient client = new UdpClient(0, AddressFamily.InterNetwork);
        IPEndPoint groupEp = new IPEndPoint(IPAddress.Broadcast, (int)port);
        client.Connect(groupEp);

        var dataArr = Encoding.UTF8.GetBytes((string)data);

        client.Send(dataArr, dataArr.Length);
        client.Close();
    }
};