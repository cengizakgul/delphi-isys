unit pbCltMainboard;

interface

uses Windows, SysUtils, isBaseClasses, pbInterfaces, isThreadManager, isSettings, isBasicUtils,
     isLogFile, pbClient, pbGUI, ISUtils;

type
  IpbCltMainboard = interface
  ['{FDB673BA-C709-420F-9746-D4EEC7786D83}']
    procedure Initialize;
    procedure Finalize;
    function  LogFile: ILogFile;
    function  Settings: IisSettings;
    function  UserSettings: IisSettings;
    function  ProjectBuilder: IpbProjectBuilder;
    function  GUI:IpbGUI;
  end;

  function  Mainboard: IpbCltMainboard;
  procedure ActivateMainboard;
  procedure DeactivateMainboard;


implementation

type
  TpbCltMainboard = class(TisInterfacedObject, IpbCltMainboard)
  private
    FLogFile: ILogFile;
    FSettings: IisSettings;
    FUserSettings: IisSettings;    
    FProjectBuilder: IpbProjectBuilder;
    FGUI: IpbGUI;
    FUser: String;
    FPassword: String;
    function   CreatePB: IpbProjectBuilder;
  protected
    procedure  DoOnConstruction; override;
    procedure  DoOnDestruction; override;

    procedure Initialize;
    procedure Finalize;
    function  LogFile: ILogFile;
    function  Settings: IisSettings;
    function  UserSettings: IisSettings;
    function  ProjectBuilder: IpbProjectBuilder;
    function  GUI:IpbGUI;    
  end;


var
  vMainboard: IpbCltMainboard;

function Mainboard: IpbCltMainboard;
begin
  if not Assigned(vMainboard) then
    ActivateMainboard;
  Result := vMainboard;
end;

procedure ActivateMainboard;
begin
  if not Assigned(vMainboard) then
    vMainboard := TpbCltMainboard.Create;
end;

procedure DeactivateMainboard;
begin
  vMainboard := nil;
end;


{ TpbCltMainboard }

function TpbCltMainboard.CreatePB: IpbProjectBuilder;
const
  cDefaultPort = 9498;
var
  s, host: String;
  port: Integer;
begin
  Result := nil;
  host := FUserSettings.AsString['Host'];
  port := FUserSettings.GetValue('Port', cDefaultPort);

  if host = '' then
  begin
    host := FSettings.AsString['Server\Host'];
    port := FSettings.GetValue('Server\Port', cDefaultPort);
  end;  

  if CurrentThreadIsMain and ((host = '') or (FUser = '') or (FPassword = '')) then
  begin
    if FUser = '' then
      FUser := FUserSettings.AsString['User']; 

    while GUI.ShowLoginForm(host, FUser, FPassword) do
    begin
      s := host;
      host := GetNextStrValue(s, ':');
      port := StrToIntDef(s, port);

      Result := CreateProjectBuilder(host, port, FUser, FPassword);
      try
        Result.CurrentUserInfo; // try to connect

        FUserSettings.AsString['User'] := FUser;

        if FSettings.AsString['Server\Host'] <> host then
        begin
          FUserSettings.AsString['Host'] := host;
          if port <> cDefaultPort then
            FUserSettings.AsInteger['Port'] := port
          else
            FUserSettings.DeleteValue('Port');
        end
        else
        begin
          FUserSettings.DeleteValue('Host');
          FUserSettings.DeleteValue('Port');
        end;

        break;
      except
        on E: Exception do
        begin
          IsExceptMessage(E);
          Result := nil;
        end;
      end;
    end;
  end;
end;

procedure TpbCltMainboard.DoOnConstruction;
begin
  FLogFile := TLogFile.Create(ChangeFileExt(AppFileName, '.log'));
  SetGlobalLogFile(FLogFile);
  FSettings := TisSettingsINI.Create(ChangeFileExt(AppFileName, '.ini'));
  FUserSettings := TisSettingsRegistry.Create(HKEY_CURRENT_USER, 'SOFTWARE\iSystems\Project Builder');  
  FGUI := CreateGUI;  
end;

procedure TpbCltMainboard.DoOnDestruction;
begin
  inherited;

end;

procedure TpbCltMainboard.Finalize;
begin
  GlobalThreadManager.TerminateTask;
  GlobalThreadManager.WaitForTaskEnd;

  FGUI.Finalize;
  FGUI := nil;
  FProjectBuilder := nil;

  FLogFile.AddEvent(etInformation, 'Diagnostic', 'Application closed', '');
end;

function TpbCltMainboard.GUI: IpbGUI;
begin
  Result := FGUI;
end;

procedure TpbCltMainboard.Initialize;
begin
  FProjectBuilder := CreatePB;

  if Assigned(FProjectBuilder) then
  begin
    FGUI.Initialize;
    FLogFile.AddEvent(etInformation, 'Diagnostic', 'Application started', '');
  end;  
end;

function TpbCltMainboard.LogFile: ILogFile;
begin
  Result := FLogFile;
end;

function TpbCltMainboard.ProjectBuilder: IpbProjectBuilder;
begin
  Result := FProjectBuilder;
end;

function TpbCltMainboard.Settings: IisSettings;
begin
  Result := FSettings;
end;

function TpbCltMainboard.UserSettings: IisSettings;
begin
  Result := FUserSettings;
end;

end.
