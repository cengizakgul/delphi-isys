inherited pbWizBuildPrjSelect: TpbWizBuildPrjSelect
  Width = 414
  OnResize = FrameResize
  object Label2: TLabel
    Left = 40
    Top = 20
    Width = 250
    Height = 16
    Caption = 'Please select product you want to build'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 40
    Top = 81
    Width = 49
    Height = 16
    Caption = 'Product'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 40
    Top = 125
    Width = 67
    Height = 16
    Caption = 'Build Type'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 40
    Top = 167
    Width = 79
    Height = 16
    Caption = 'New Version'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lNewVesion: TLabel
    Left = 132
    Top = 167
    Width = 18
    Height = 16
    Caption = '12.'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 40
    Top = 210
    Width = 107
    Height = 16
    Caption = 'Build description'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbProducts: TComboBox
    Left = 122
    Top = 78
    Width = 248
    Height = 24
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 0
    OnChange = cbProductsChange
  end
  object cbProjects: TComboBox
    Left = 122
    Top = 121
    Width = 248
    Height = 24
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 1
    OnChange = cbProjectsChange
  end
  object btnIssues: TButton
    Left = 264
    Top = 163
    Width = 106
    Height = 27
    Caption = 'Show Issues...'
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = btnIssuesClick
  end
  object memDescr: TMemo
    Left = 40
    Top = 234
    Width = 330
    Height = 71
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object chbPublish: TCheckBox
    Left = 40
    Top = 315
    Width = 131
    Height = 17
    Caption = 'Publish for testing'
    Checked = True
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 4
  end
  object edVersion: TMaskEdit
    Left = 152
    Top = 164
    Width = 63
    Height = 24
    EditMask = '99\.99\.99;1; '
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    MaxLength = 8
    ParentFont = False
    TabOrder = 5
    Text = '  .  .  '
    OnExit = edVersionExit
  end
end
