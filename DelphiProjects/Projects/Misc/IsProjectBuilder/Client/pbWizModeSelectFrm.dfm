inherited pbWizModeSelect: TpbWizModeSelect
  Width = 414
  object Label2: TLabel
    Left = 40
    Top = 20
    Width = 225
    Height = 16
    Caption = 'Please choose what you want to do'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object gbModes: TGroupBox
    Left = 40
    Top = 67
    Width = 333
    Height = 248
    Caption = 'Modes'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object rbBuild: TRadioButton
      Left = 55
      Top = 33
      Width = 244
      Height = 17
      Caption = 'Build new version of a product'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = rbBuildClick
    end
    object rbRelease: TRadioButton
      Left = 55
      Top = 104
      Width = 244
      Height = 17
      Caption = 'Release tested version of a product'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = rbBuildClick
    end
    object rbVersRepos: TRadioButton
      Left = 55
      Top = 202
      Width = 244
      Height = 17
      Caption = 'Administrate version repository'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = rbBuildClick
    end
    object rbHotFix: TRadioButton
      Left = 55
      Top = 177
      Width = 244
      Height = 17
      Caption = 'Prepare SCM for hot-fix'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = rbBuildClick
    end
    object rbPublish: TRadioButton
      Left = 55
      Top = 129
      Width = 244
      Height = 17
      Caption = 'Publish released version'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = rbBuildClick
    end
    object rbSendToQA: TRadioButton
      Left = 55
      Top = 59
      Width = 244
      Height = 17
      Caption = 'Publish build version for testing'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = rbBuildClick
    end
  end
end
