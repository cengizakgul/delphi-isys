unit pbWizReleasePrjSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbCltMainboard, pbInterfaces, isBaseClasses,
  pbWizIssuesFrm, pbClasses;

type
  TpbWizReleasePrjSelect = class(TpbWizardPage)
    Label2: TLabel;
    cbProducts: TComboBox;
    Label3: TLabel;
    Label1: TLabel;
    cbProjects: TComboBox;
    btnIssues: TButton;
    Label5: TLabel;
    memDescr: TMemo;
    procedure cbProductsChange(Sender: TObject);
    procedure cbProjectsChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure btnIssuesClick(Sender: TObject);
  private
    FProducts: IpbProducts;  
    function  SelectedProject: IpbProject;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TpbWizardPage; override;
  end;

implementation

uses pbWizWrapUpFrm;

{$R *.dfm}

{ TpbWizReleasePrjSelect }

function TpbWizReleasePrjSelect.GetNextPage: TpbWizardPage;
var
  Params, Info: IisListOfValues;
  Task: IpbTask;
begin
  Task := TpbTask.Create(ttReleaseBinProject,
    Format('Release %s %s', [SelectedProject.Product.Name, SelectedProject.Version.AsString]));

  Task.Params.AddValue('ProductName', SelectedProject.Product.Name);
  Task.Params.AddValue('ProjectName', SelectedProject.Name);

  Info := TisListOfValues.Create;
  Info.AddValue('Action', 'Release version of a product');
  Info.AddValue('Product', SelectedProject.Product.Name);
  Info.AddValue('Build type', SelectedProject.Name);
  Info.AddValue('Version', SelectedProject.Version.AsString);

  Params := TisListOfValues.Create;
  Params.AddValue('NextButtonName', 'Release Now!');
  Params.AddValue('Info', Info);
  Params.AddValue('Task', Task);  

  Result := TpbWizWrapUp.Create(Owner, Params);
end;

procedure TpbWizReleasePrjSelect.Init;
var
  i: Integer;
begin
  inherited;
  FProducts := Mainboard.ProjectBuilder.GetProducts;
  cbProducts.Clear;
  for i := 0 to FProducts.Count - 1 do
    cbProducts.AddItem(FProducts[i].Name, Pointer(FProducts[i]));
  cbProductsChange(nil);
end;

procedure TpbWizReleasePrjSelect.cbProductsChange(Sender: TObject);
var
  i: Integer;
  Prod: IpbProduct;
  Prj: IpbProject;
begin
  if cbProducts.ItemIndex <> -1 then
  begin
    Prod := IpbProduct(Pointer(cbProducts.Items.Objects[cbProducts.ItemIndex]));
    Mainboard.ProjectBuilder.UpdateProjects(Prod);
    
    cbProjects.Clear;
    Mainboard.ProjectBuilder.UpdateProjects(Prod);
    for i := 0 to Prod.ProjectCount - 1 do
    begin
      Prj := Prod.Projects[i];
      if Prj.ProjectType = ptBinBuild then
        cbProjects.AddItem(Prj.Name, Pointer(Prj));
    end;
  end;
  cbProjectsChange(nil);
end;

function TpbWizReleasePrjSelect.CanMoveForward: Boolean;
begin
  Result := cbProjects.ItemIndex <> -1;
end;

procedure TpbWizReleasePrjSelect.cbProjectsChange(Sender: TObject);
begin
  if SelectedProject <> nil then
  begin
    memDescr.Enabled := True;
    memDescr.Text := SelectedProject.Description;
    btnIssues.Enabled := (IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection).Count > 0;
  end
  else
  begin
    memDescr.Enabled := False;
    btnIssues.Enabled := False;
  end;

  NotifyChange;
end;

function TpbWizReleasePrjSelect.SelectedProject: IpbProject;
begin
  if cbProjects.ItemIndex <> - 1 then
    Result := IpbProject(Pointer(cbProjects.Items.Objects[cbProjects.ItemIndex]))
  else
    Result := nil;
end;

procedure TpbWizReleasePrjSelect.FrameResize(Sender: TObject);
begin
  inherited;
  memDescr.Width := ClientWidth - memDescr.Left * 2;
  memDescr.Height := ClientHeight - memDescr.Top - memDescr.Left * 2;
end;

procedure TpbWizReleasePrjSelect.btnIssuesClick(Sender: TObject);
begin
  inherited;
  ShowIssueList('Issues are included in ' + SelectedProject.Version.AsString,
    Mainboard.ProjectBuilder.GetUpdatedIssues(SelectedProject));
end;

end.
