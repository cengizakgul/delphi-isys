inherited pbLogin: TpbLogin
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Login to Project Builder server'
  ClientHeight = 209
  ClientWidth = 302
  Color = clSkyBlue
  Font.Charset = RUSSIAN_CHARSET
  Font.Height = -13
  Font.Name = 'Arial'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 30
    Top = 64
    Width = 68
    Height = 16
    Caption = 'User name'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 30
    Top = 104
    Width = 61
    Height = 16
    Caption = 'Password'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 30
    Top = 26
    Width = 42
    Height = 16
    Caption = 'Server'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edUser: TEdit
    Left = 117
    Top = 62
    Width = 153
    Height = 24
    TabOrder = 1
  end
  object edPassword: TEdit
    Left = 117
    Top = 100
    Width = 153
    Height = 24
    PasswordChar = '*'
    TabOrder = 2
  end
  object btnOK: TButton
    Left = 83
    Top = 155
    Width = 85
    Height = 27
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object btnCancel: TButton
    Left = 186
    Top = 155
    Width = 85
    Height = 27
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object edServer: TEdit
    Left = 117
    Top = 24
    Width = 153
    Height = 24
    TabOrder = 0
  end
end
