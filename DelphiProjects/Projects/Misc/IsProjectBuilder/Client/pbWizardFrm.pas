unit pbWizardFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbFormFrm, ExtCtrls, StdCtrls, jpeg, isBasicUtils, isBaseClasses,
  pbWizardPageFrm, pbInterfaces, isUtils, ComCtrls;

type
  TpbWizard = class(TpbForm)
    btnBack: TButton;
    btnNext: TButton;
    imPict: TImage;
    pnlPages: TPanel;
    sbStatus: TStatusBar;
    tmConnectionWatch: TTimer;
    imConnectedIcon: TImage;
    imDisconnectedIcon: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure sbStatusDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure tmConnectionWatchTimer(Sender: TObject);
  private
    FHist: TList;
    FCurrentPage: TpbWizardPage;
    FConnected: Boolean;
    procedure ShowPage(const APage: TpbWizardPage);
    procedure GoToNextPage;
    procedure GoToPreviousPage;
    procedure SyncButtons;
    procedure Init;
  public
    procedure DoOnChange;
    procedure ClearHistory;
  end;

implementation

{$R *.dfm}

uses pbCltMainboard, pbWizModeSelectFrm, pbWizProgressFrm, pbWizSettingsFrm, pbClient;

{ TpbWizard }

procedure TpbWizard.Init;
var
  Tsk: IpbTask;
begin
  sbStatus.Panels[0].Text := Format('%s:%d', [(Mainboard.ProjectBuilder as IpbProxyObject).Host, (Mainboard.ProjectBuilder as IpbProxyObject).Port]);
  sbStatus.Panels[1].Text := Mainboard.ProjectBuilder.CurrentUserInfo.User;
  FConnected := (Mainboard.ProjectBuilder as IpbProxyObject).Connected;

  if Mainboard.ProjectBuilder.CurrentUserInfo.Password = '' then
    FCurrentPage := TpbWizSettings.Create(Self, nil)  
  else
  begin
    Tsk := Mainboard.ProjectBuilder.GetCurrentTask;
    if Assigned(Tsk) then
      FCurrentPage := TpbWizProgress.Create(Self, nil)
    else
      FCurrentPage := TpbWizModeSelect.Create(Self, nil);
  end;    

  try
    FCurrentPage.Init;
  finally
    ShowPage(FCurrentPage);
    SyncButtons;
  end;
end;

procedure TpbWizard.ShowPage(const APage: TpbWizardPage);
begin
  APage.Align := alClient;
  APage.Parent := pnlPages;
end;

procedure TpbWizard.FormCreate(Sender: TObject);
begin
  inherited;
  Caption := Application.Title + ' ' + AppVersion;
  FHist := TList.Create;
  Init;
end;

procedure TpbWizard.FormDestroy(Sender: TObject);
begin
  FHist.Free;
  inherited;
end;

procedure TpbWizard.GoToNextPage;
var
  Page: TpbWizardPage;
begin
  if Assigned(FCurrentPage) then
  begin
    Page := FCurrentPage.GetNextPage;
    if Assigned(Page) then
    begin
      FCurrentPage.Parent := nil;
      FHist.Add(FCurrentPage);
      FCurrentPage := Page;
      ShowPage(FCurrentPage);
      SyncButtons;
      try
        FCurrentPage.Init;
      except
        on E: Exception do
        begin
          IsExceptMessage(E, False);
          GoToPreviousPage;
        end;
      end;
    end
    else
      Init;
  end;
end;

procedure TpbWizard.SyncButtons;
begin
  btnBack.Visible := FHist.Count > 0;
  btnNext.Enabled := Assigned(FCurrentPage) and FCurrentPage.CanMoveForward;
  btnNext.Caption := FCurrentPage.NextButtonName;
end;

procedure TpbWizard.DoOnChange;
begin
  SyncButtons;
end;

procedure TpbWizard.btnNextClick(Sender: TObject);
begin
  GoToNextPage;
end;

procedure TpbWizard.btnBackClick(Sender: TObject);
begin
  GoToPreviousPage;
end;

procedure TpbWizard.GoToPreviousPage;
var
  Page: TpbWizardPage;
begin
  if FHist.Count > 0 then
  begin
    Page := TpbWizardPage(FHist[FHist.Count - 1]);
    FHist.Delete(FHist.Count - 1);
    ShowPage(Page);
  end
  else
    Page := nil;

  if Assigned(FCurrentPage) then
    FCurrentPage.Free;

  FCurrentPage := Page;
  SyncButtons;  
end;

procedure TpbWizard.ClearHistory;
var
  i: Integer;
begin
  for i := 0 to FHist.Count - 1 do
    TpbWizardPage(FHist[i]).Free;
  FHist.Clear;
  SyncButtons;  
end;

procedure TpbWizard.sbStatusDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);

  procedure DrawSessionState;
  var
    Pict: TGraphic;
  begin
    if FConnected then
      Pict := imConnectedIcon.Picture.Graphic
    else
      Pict := imDisconnectedIcon.Picture.Graphic;

    sbStatus.Canvas.Draw(Rect.Left, Rect.Top, Pict)
  end;

begin
  if Panel.ID = 2 then
    DrawSessionState;
end;

procedure TpbWizard.tmConnectionWatchTimer(Sender: TObject);
var
  CurConnected: Boolean;
begin
  CurConnected := (Mainboard.ProjectBuilder as IpbProxyObject).Connected;
  if FConnected <> CurConnected then
  begin
    FConnected := CurConnected;
    sbStatus.Invalidate;
  end;
end;

end.
