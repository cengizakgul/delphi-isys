unit pbWizPublishPrjSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbCltMainboard, pbInterfaces, isBaseClasses,
  pbWizIssuesFrm, pbClasses;

type
  TpbWizPublishPrjSelect = class(TpbWizardPage)
    Label2: TLabel;
    cbProducts: TComboBox;
    Label3: TLabel;
    Label1: TLabel;
    cbProjects: TComboBox;
    btnIssues: TButton;
    Label5: TLabel;
    memDescr: TMemo;
    procedure cbProductsChange(Sender: TObject);
    procedure cbProjectsChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure btnIssuesClick(Sender: TObject);
  private
    FProducts: IpbProducts;
    function  SelectedProject: IpbProject;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TpbWizardPage; override;
  end;

implementation

uses pbWizWrapUpFrm;

{$R *.dfm}

{ TpbWizReleasePrjSelect }

function TpbWizPublishPrjSelect.GetNextPage: TpbWizardPage;
var
  Params, Info: IisListOfValues;
  Task: IpbTask;
begin
  Task := TpbTask.Create(ttPublishBinProject,
    Format('Publish %s %s', [SelectedProject.Product.Name, SelectedProject.Version.AsString]));

  Task.Params.AddValue('ProductName', SelectedProject.Product.Name);
  Task.Params.AddValue('ProjectName', SelectedProject.Name);

  Info := TisListOfValues.Create;
  Info.AddValue('Action', 'Publish released version of a product');
  Info.AddValue('Product', SelectedProject.Product.Name);
  Info.AddValue('Build type', SelectedProject.Name);
  Info.AddValue('Version', SelectedProject.Version.AsString);

  Params := TisListOfValues.Create;
  Params.AddValue('NextButtonName', 'Publish Now!');
  Params.AddValue('Info', Info);
  Params.AddValue('Task', Task);

  Result := TpbWizWrapUp.Create(Owner, Params);
end;

procedure TpbWizPublishPrjSelect.Init;
var
  i: Integer;
begin
  inherited;
  FProducts := Mainboard.ProjectBuilder.GetProducts;
  cbProducts.Clear;
  for i := 0 to FProducts.Count - 1 do
    cbProducts.AddItem(FProducts[i].Name, Pointer(FProducts[i]));
  cbProductsChange(nil);
end;

procedure TpbWizPublishPrjSelect.cbProductsChange(Sender: TObject);
var
  i: Integer;
  Prod: IpbProduct;
  Prj: IpbProject;
begin
  if cbProducts.ItemIndex <> -1 then
  begin
    Prod := IpbProduct(Pointer(cbProducts.Items.Objects[cbProducts.ItemIndex]));
    Mainboard.ProjectBuilder.UpdateProjects(Prod);
    
    cbProjects.Clear;
    Mainboard.ProjectBuilder.UpdateProjects(Prod);
    for i := 0 to Prod.ProjectCount - 1 do
    begin
      Prj := Prod.Projects[i];
      if Prj.ProjectType = ptBinRelease then
        cbProjects.AddItem(Prj.Name, Pointer(Prj));
    end;
  end;
  cbProjectsChange(nil);
end;

function TpbWizPublishPrjSelect.CanMoveForward: Boolean;
begin
  Result := cbProjects.ItemIndex <> -1;
end;

procedure TpbWizPublishPrjSelect.cbProjectsChange(Sender: TObject);
begin
  if SelectedProject <> nil then
  begin
    memDescr.Enabled := True;
    memDescr.Text := SelectedProject.Description;
    btnIssues.Enabled := (IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection).Count > 0;
  end
  else
  begin
    btnIssues.Enabled := False;
    memDescr.Enabled := False;
  end;

  NotifyChange;
end;

function TpbWizPublishPrjSelect.SelectedProject: IpbProject;
begin
  if cbProjects.ItemIndex <> - 1 then
    Result := IpbProject(Pointer(cbProjects.Items.Objects[cbProjects.ItemIndex]))
  else
    Result := nil;
end;

procedure TpbWizPublishPrjSelect.FrameResize(Sender: TObject);
begin
  inherited;
  memDescr.Width := ClientWidth - memDescr.Left * 2;
  memDescr.Height := ClientHeight - memDescr.Top - memDescr.Left * 2;
end;

procedure TpbWizPublishPrjSelect.btnIssuesClick(Sender: TObject);
begin
  inherited;
  ShowIssueList('Issues are included in ' + SelectedProject.Version.AsString,
    Mainboard.ProjectBuilder.GetUpdatedIssues(SelectedProject));
end;

end.
