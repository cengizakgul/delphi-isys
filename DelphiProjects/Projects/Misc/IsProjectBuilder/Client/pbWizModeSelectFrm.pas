unit pbWizModeSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbCltMainboard, pbInterfaces, ExtCtrls,
  isBaseClasses;

type
  TpbWizModeSelect = class(TpbWizardPage)
    Label2: TLabel;
    gbModes: TGroupBox;
    rbBuild: TRadioButton;
    rbRelease: TRadioButton;
    rbVersRepos: TRadioButton;
    rbHotFix: TRadioButton;
    rbPublish: TRadioButton;
    rbSendToQA: TRadioButton;
    procedure rbBuildClick(Sender: TObject);
  private
    function GetNextPageClass: TpbWizardPageClass;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TpbWizardPage; override;
  end;

implementation

uses pbWizBuildPrjSelectFrm, pbWizReleasePrjSelectFrm, pbWizAdmBinPrjSelectFrm,
     pbWizHFPrepPrjSelectFrm, pbWizardFrm, pbWizPublishPrjSelectFrm, pbWizQAPrjSelectFrm;

{$R *.dfm}

{ TpbWizModeSelect }

function TpbWizModeSelect.GetNextPage: TpbWizardPage;
begin
  Result := GetNextPageClass.Create(Owner, nil);
end;

procedure TpbWizModeSelect.Init;
var
  UserInfo: IpbUserInfo;
begin
  inherited;
  TpbWizard(Owner).ClearHistory;

  UserInfo := Mainboard.ProjectBuilder.CurrentUserInfo;

  rbBuild.Enabled := False;
  rbSendToQA.Enabled := False;
  rbRelease.Enabled := False;
  rbPublish.Enabled := False;
  rbHotFix.Enabled := False;
  rbVersRepos.Enabled := False;

  case UserInfo.Role of
    urDeveloper:
      begin
        rbBuild.Enabled := True;
        rbSendToQA.Enabled := True;
      end;

    urQATester:
      begin
        rbBuild.Enabled := True;
        rbSendToQA.Enabled := True;
        rbRelease.Enabled := True;
      end;

    urQAManager:
      begin
        rbBuild.Enabled := True;
        rbSendToQA.Enabled := True;
        rbRelease.Enabled := True;
        rbPublish.Enabled := True;
      end;

    urAdministrator:
      begin
        rbBuild.Enabled := True;
        rbSendToQA.Enabled := True;
        rbRelease.Enabled := True;
        rbPublish.Enabled := True;
        rbHotFix.Enabled := True;
        rbVersRepos.Enabled := True;
      end;
  end;
end;

function TpbWizModeSelect.CanMoveForward: Boolean;
begin
  Result := GetNextPageClass <> nil;
end;

function TpbWizModeSelect.GetNextPageClass: TpbWizardPageClass;
begin
  if rbBuild.Checked then
    Result := TpbWizBuildPrjSelect
  else if rbSendToQA.Checked then
    Result := TpbWizQAPrjSelect
  else if rbRelease.Checked then
    Result := TpbWizReleasePrjSelect
  else if rbPublish.Checked then
    Result := TpbWizPublishPrjSelect
  else if rbHotFix.Checked then
    Result := TpbWizHFPrepPrjSelect
  else if rbVersRepos.Checked then
    Result := TpbWizAdmBinPrjSelect
  else
    Result := nil;  
end;

procedure TpbWizModeSelect.rbBuildClick(Sender: TObject);
begin
  NotifyChange;
end;

end.
