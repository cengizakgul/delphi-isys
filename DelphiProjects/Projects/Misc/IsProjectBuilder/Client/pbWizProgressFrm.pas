unit pbWizProgressFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbInterfaces, pbCltMainboard, isThreadManager,
  isBaseClasses, isBasicUtils, ExtCtrls, isLogFile, GIFImage;

const CM_PROGRESS_UPDATE = WM_USER + 1;

type
  TpbWizProgress = class(TpbWizardPage)
    mDetails: TMemo;
    lLStatus: TLabel;
    lCaption: TLabel;
    lLDetails: TLabel;
    lStatus: TLabel;
    imGears: TImage;
    lOwner: TLabel;
    procedure FrameResize(Sender: TObject);
  private
    FActionTaskID: TTaskID;
    procedure DoAction(const Params: TTaskParamList);
    procedure StopAction;
    procedure CMProgressUpdate(var Message: TMessage); message CM_PROGRESS_UPDATE;
    procedure UpdateProgress(const ATask: IpbTask);
  public
    procedure  Init; override;
    function   CanMoveForward: Boolean; override;
    function   GetNextPage: TpbWizardPage; override;
    function   NextButtonName: String; override;
    destructor Destroy; override;
  end;

implementation

uses pbWizardFrm;

{$R *.dfm}

{ TpbWizBuild }

function TpbWizProgress.CanMoveForward: Boolean;
begin
  Result := FActionTaskID = 0;
  (imGears.Picture.Graphic as TGIFImage).Animate := not Result;
end;

procedure TpbWizProgress.CMProgressUpdate(var Message: TMessage);
var
  Task: IpbTask;
  s: String;
begin
  Task := IpbTask(Pointer(Message.WParam));

  if Assigned(Task) then
  begin
    lCaption.Caption := Task.Caption;

    s := Task.Params.TryGetValue('User', '');
    if s = '' then
      lOwner.Caption := ''
    else
      lOwner.Caption := 'Started by ' + s;    

    mDetails.Lines.Text := Task.ProgressDetails;
    SendMessage(mDetails.Handle, WM_VSCROLL, SB_BOTTOM, 0);

    lStatus.Caption := Task.ProgressStatus;
    if Contains(lStatus.Caption, 'error') then
      lStatus.Font.Color := clRed;

    Task._Release;
  end

  else
  begin
    FActionTaskID := 0;
    NotifyChange;
  end
end;

destructor TpbWizProgress.Destroy;
begin
  StopAction;
  inherited;
end;

procedure TpbWizProgress.DoAction(const Params: TTaskParamList);

  function CheckTask: Boolean;
  var
    Tsk: IpbTask;
  begin
    Tsk := Mainboard.ProjectBuilder.GetCurrentTask;
    if Assigned(Tsk) then
    begin
      UpdateProgress(Tsk);
      Result := Tsk.Executing;
    end
    else
      Result := False;
  end;

begin
  try
    while CheckTask do
      Snooze(1000);
  finally
    UpdateProgress(nil);
  end;
end;

function TpbWizProgress.GetNextPage: TpbWizardPage;
begin
  Mainboard.ProjectBuilder.RunTask(nil);  // in order to clear last task info
  Result := nil;
end;

procedure TpbWizProgress.Init;
begin
  inherited;
  lCaption.Caption := '';
  lStatus.Caption := '';
  TpbWizard(Owner).ClearHistory;
  FActionTaskID := GlobalThreadManager.RunTask(DoAction, Self, MakeTaskParams([]), 'ExecAction');
  NotifyChange;
end;

procedure TpbWizProgress.UpdateProgress(const ATask: IpbTask);
begin
  if Assigned(ATask) then
    ATask._AddRef;
  PostMessage(Handle, CM_PROGRESS_UPDATE, Integer(Pointer(ATask)), 0);
end;

function TpbWizProgress.NextButtonName: String;
begin
  Result := 'Finished';
end;

procedure TpbWizProgress.StopAction;
begin
  if FActionTaskID <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FActionTaskID);
    GlobalThreadManager.WaitForTaskEnd(FActionTaskID);
  end;  
end;

procedure TpbWizProgress.FrameResize(Sender: TObject);
begin
  mDetails.Width := ClientWidth - mDetails.Left * 2;
  mDetails.Height := ClientHeight - mDetails.Left * 2 - mDetails.Top;

  imGears.Left := mDetails.BoundsRect.Right - imGears.Width;
end;

end.
 