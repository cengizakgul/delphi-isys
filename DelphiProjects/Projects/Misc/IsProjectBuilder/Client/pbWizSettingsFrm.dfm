inherited pbWizSettings: TpbWizSettings
  Width = 414
  object lCaption: TLabel
    Left = 40
    Top = 20
    Width = 174
    Height = 16
    Caption = 'Please reset your password'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lOldPassword: TLabel
    Left = 40
    Top = 81
    Width = 87
    Height = 16
    Caption = 'Old Password'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lNewPassword: TLabel
    Left = 40
    Top = 120
    Width = 93
    Height = 16
    Caption = 'New Password'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtOldPassword: TEdit
    Left = 146
    Top = 78
    Width = 157
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 0
    OnChange = edtOldPasswordChange
  end
  object edtNewPassword: TEdit
    Left = 146
    Top = 117
    Width = 157
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 1
    OnChange = edtOldPasswordChange
  end
end
