unit pbWizSettingsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, pbInterfaces, StdCtrls, isBaseClasses;

type
  TpbWizSettings = class(TpbWizardPage)
    lCaption: TLabel;
    lOldPassword: TLabel;
    edtOldPassword: TEdit;
    lNewPassword: TLabel;
    edtNewPassword: TEdit;
    procedure edtOldPasswordChange(Sender: TObject);
  private
  public
    procedure Init; override;
    function  GetNextPage: TpbWizardPage; override;
    function  CanMoveForward: Boolean; override;
    function  NextButtonName: String; override;
  end;


implementation

uses pbCltMainboard, pbWizProgressFrm;

{$R *.dfm}


{ TpbWizWrapUp }

function TpbWizSettings.CanMoveForward: Boolean;
begin
  Result := edtNewPassword.Text <> '';
end;

procedure TpbWizSettings.Init;
begin
  inherited;
  if Mainboard.ProjectBuilder.CurrentUserInfo.Password = '' then
  begin
    edtOldPassword.Enabled := False;
    lOldPassword.Enabled := False;
  end;
end;

function TpbWizSettings.NextButtonName: String;
begin
  Result := 'Apply';
end;

function TpbWizSettings.GetNextPage: TpbWizardPage;
begin
  Mainboard.ProjectBuilder.ChangeUserPassword(edtOldPassword.Text, edtNewPassword.Text);
  Result := nil;
end;

procedure TpbWizSettings.edtOldPasswordChange(Sender: TObject);
begin
  NotifyChange;
end;

end.
 