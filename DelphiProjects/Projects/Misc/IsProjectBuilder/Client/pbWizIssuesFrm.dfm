inherited pbWizIssues: TpbWizIssues
  Left = 328
  Top = 182
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'pbWizIssues'
  ClientHeight = 466
  ClientWidth = 702
  Color = clSkyBlue
  Font.Charset = RUSSIAN_CHARSET
  Font.Height = -13
  Font.Name = 'Arial'
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 16
  object btnClose: TButton
    Left = 599
    Top = 422
    Width = 80
    Height = 27
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
  end
  object tvIssues: TTreeView
    Left = 20
    Top = 20
    Width = 241
    Height = 385
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    HideSelection = False
    Indent = 19
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    OnChange = tvIssuesChange
    OnCompare = tvIssuesCompare
    OnCustomDrawItem = tvIssuesCustomDrawItem
  end
  object gbDetails: TGroupBox
    Left = 284
    Top = 20
    Width = 393
    Height = 385
    Caption = 'Issue Details'
    TabOrder = 2
    object Label1: TLabel
      Left = 22
      Top = 32
      Width = 42
      Height = 16
      Caption = 'Reso #'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 22
      Top = 64
      Width = 39
      Height = 16
      Caption = 'Status'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 22
      Top = 128
      Width = 71
      Height = 16
      Caption = 'Description'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lResoNbr: TLabel
      Left = 92
      Top = 32
      Width = 53
      Height = 16
      Caption = 'lResoNbr'
    end
    object lStatus: TLabel
      Left = 92
      Top = 64
      Width = 41
      Height = 16
      Caption = 'lStatus'
    end
    object memDescription: TMemo
      Left = 22
      Top = 153
      Width = 349
      Height = 209
      Lines.Strings = (
        '')
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
end
