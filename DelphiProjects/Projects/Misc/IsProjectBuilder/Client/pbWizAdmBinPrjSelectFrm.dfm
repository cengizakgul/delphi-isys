inherited pbWizAdmBinPrjSelect: TpbWizAdmBinPrjSelect
  Width = 414
  object Label2: TLabel
    Left = 40
    Top = 20
    Width = 330
    Height = 16
    Caption = 'Please select versions of product you want to delete'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 40
    Top = 81
    Width = 49
    Height = 16
    Caption = 'Product'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbProducts: TComboBox
    Left = 102
    Top = 78
    Width = 266
    Height = 24
    Style = csDropDownList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 0
    OnChange = cbProductsChange
  end
  object chbVersions: TCheckListBox
    Left = 40
    Top = 155
    Width = 327
    Height = 148
    OnClickCheck = chbVersionsClickCheck
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 3
  end
  object rbBuilds: TRadioButton
    Left = 40
    Top = 130
    Width = 81
    Height = 17
    Caption = 'Builds'
    Checked = True
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    TabStop = True
    OnClick = cbProductsChange
  end
  object rbReleases: TRadioButton
    Left = 129
    Top = 130
    Width = 81
    Height = 17
    Caption = 'Releases'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = cbProductsChange
  end
end
