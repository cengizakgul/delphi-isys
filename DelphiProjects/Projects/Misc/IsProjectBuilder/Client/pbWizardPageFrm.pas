unit pbWizardPageFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isBaseClasses;

type
  TpbWizardPage = class(TFrame)
  protected
    FParams: IisListOfValues;
  public
    procedure Init; virtual;
    function  GetNextPage: TpbWizardPage; virtual;
    function  CanMoveForward: Boolean; virtual;
    procedure NotifyChange;
    function  NextButtonName: String; virtual;

    constructor Create(const AWizard: TComponent; const AParams: IisListOfValues); reintroduce;
  end;

  TpbWizardPageClass = class of TpbWizardPage;

implementation

{$R *.dfm}

uses pbWizardFrm;

{ TpbWizardPage }

function TpbWizardPage.CanMoveForward: Boolean;
begin
  Result := True;
end;

function TpbWizardPage.NextButtonName: String;
begin
  Result := 'Next';
end;

function TpbWizardPage.GetNextPage: TpbWizardPage;
begin
  Result := nil;
end;

procedure TpbWizardPage.Init;
begin
  Update;
end;

procedure TpbWizardPage.NotifyChange;
begin
  TpbWizard(Owner).DoOnChange;
end;

constructor TpbWizardPage.Create(const AWizard: TComponent; const AParams: IisListOfValues);
begin
  inherited Create(AWizard);
  FParams := AParams;
  Name := '';
end;

end.
