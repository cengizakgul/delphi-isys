unit pbWizAdmBinPrjSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbCltMainboard, pbInterfaces, isBaseClasses,
  CheckLst, isBasicUtils, pbClasses;

type
  TpbWizAdmBinPrjSelect = class(TpbWizardPage)
    Label2: TLabel;
    cbProducts: TComboBox;
    Label3: TLabel;
    chbVersions: TCheckListBox;
    rbBuilds: TRadioButton;
    rbReleases: TRadioButton;
    procedure cbProductsChange(Sender: TObject);
    procedure chbVersionsClickCheck(Sender: TObject);
  private
    function  SelectedProd: IpbProduct;
    procedure RefreshVersionList;
  private
    FProducts: IpbProducts;  
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TpbWizardPage; override;
  end;

implementation

uses pbWizWrapUpFrm;

{$R *.dfm}

{ TpbWizReleasePrjSelect }

function TpbWizAdmBinPrjSelect.GetNextPage: TpbWizardPage;
var
  Params, Info: IisListOfValues;
  sVers, sType: String;
  i: Integer;
  ProjectList: IisStringList;
  Prj: IpbProject;
  Task: IpbTask;
begin
  Task := TpbTask.Create(ttDeleteBinProject, 'Delete versions');

  ProjectList := TisStringList.Create;
  Task.Params.AddValue('ProductName', SelectedProd.Name);
  Task.Params.AddValue('ProjectNames', ProjectList);

  sVers := '';
  for i := 0 to chbVersions.Count - 1 do
    if chbVersions.Checked[i] then
    begin
      Prj := IpbProject(Pointer(chbVersions.Items.Objects[i]));
      ProjectList.Add(Prj.Name);
      AddStrValue(sVers, Prj.Version.AsString, ',');
    end;

  if rbReleases.Checked then
    sType := 'Releases'
  else
    sType := 'Builds';

  Info := TisListOfValues.Create;
  Info.AddValue('Action', 'Delete versions of a product');
  Info.AddValue('Product', SelectedProd.Name);
  Info.AddValue(sType, sVers);

  Params := TisListOfValues.Create;
  Params.AddValue('NextButtonName', 'Delete Now!');
  Params.AddValue('Info', Info);
  Params.AddValue('Task', Task);  

  Result := TpbWizWrapUp.Create(Owner, Params);
end;

procedure TpbWizAdmBinPrjSelect.Init;
var
  i: Integer;
begin
  inherited;
  FProducts := Mainboard.ProjectBuilder.GetProducts;
  cbProducts.Clear;
  for i := 0 to FProducts.Count - 1 do
    cbProducts.AddItem(FProducts[i].Name, Pointer(FProducts[i]));
  cbProductsChange(nil);
end;

procedure TpbWizAdmBinPrjSelect.cbProductsChange(Sender: TObject);
begin
  RefreshVersionList;
end;

function TpbWizAdmBinPrjSelect.CanMoveForward: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to chbVersions.Count - 1 do
    if chbVersions.Checked[i] then
    begin
      Result := True;
      break;
    end;
end;

function TpbWizAdmBinPrjSelect.SelectedProd: IpbProduct;
begin
  if cbProducts.ItemIndex = -1 then
    Result := nil
  else
    Result := IpbProduct(Pointer(cbProducts.Items.Objects[cbProducts.ItemIndex]));
end;

procedure TpbWizAdmBinPrjSelect.RefreshVersionList;
var
  i: Integer;
  Prod: IpbProduct;
  Prj: IpbProject;
  L: IisStringList;
begin
  chbVersions.Clear;
  Prod := SelectedProd;
  if not Assigned(Prod) then
  begin
    rbBuilds.Enabled := False;
    rbReleases.Enabled := False;
    Exit;
  end;

  rbBuilds.Enabled := True;
  rbReleases.Enabled := True;

  L := TisStringList.Create;
  Mainboard.ProjectBuilder.UpdateProjects(Prod);
  for i := 0 to Prod.ProjectCount - 1 do
  begin
    Prj := Prod.Projects[i];
    if (Prj.ProjectType = ptBinRelease) and rbReleases.Checked or
       (Prj.ProjectType = ptBinBuild) and rbBuilds.Checked then
      L.AddObject(Prj.Version.AsString(4), Prj);
  end;

  L.Sort;
  for i := 0 to L.Count - 1 do
  begin
    Prj := L.Objects[i] as IpbProject;
    chbVersions.AddItem(Prj.Name + '  ' + Prj.Description, Pointer(Prj));
  end;

  // do not let delete the last one! It is used for calculating next version number
  if chbVersions.Count > 0 then
    chbVersions.ItemEnabled[chbVersions.Count - 1] := False;

  NotifyChange;
end;

procedure TpbWizAdmBinPrjSelect.chbVersionsClickCheck(Sender: TObject);
begin
  NotifyChange;
end;

end.
