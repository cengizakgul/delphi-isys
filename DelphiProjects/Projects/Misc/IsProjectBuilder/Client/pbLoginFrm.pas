unit pbLoginFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbFormFrm, StdCtrls;

type
  TpbLogin = class(TpbForm)
    edUser: TEdit;
    edPassword: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    btnOK: TButton;
    btnCancel: TButton;
    Label3: TLabel;
    edServer: TEdit;
  private
  public
  end;

  function  ShowLoginForm(var aHostPort, aUserName, aPassword: String): Boolean;

implementation

uses ComObj;

{$R *.dfm}

function  ShowLoginForm(var aHostPort, aUserName, aPassword: String): Boolean;
var
  Frm: TpbLogin;
begin
  Frm := TpbLogin.Create(Application);
  try
    Frm.edServer.Text := aHostPort;
    Frm.edUser.Text := aUserName;
    Frm.edPassword.Text := aPassword;

    if Frm.edServer.Text = '' then
      Frm.ActiveControl := Frm.edServer
    else if Frm.edUser.Text = '' then
      Frm.ActiveControl := Frm.edUser
    else
      Frm.ActiveControl := Frm.edPassword;
                    
    Result := Frm.ShowModal = mrOk;
    if Result then
    begin
      aHostPort := Trim(Frm.edServer.Text);
      aUserName := Trim(Frm.edUser.Text);
      aPassword := Frm.edPassword.Text;
    end;  
  finally
    Frm.Free;
  end;
end;

end.
