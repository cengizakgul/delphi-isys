unit pbGUI;

interface

uses SysUtils, Forms, isBaseClasses, pbInterfaces, pbWizardFrm, isUtils;

type
  IpbGUI = interface
  ['{F76723D8-4B4F-4D64-9890-7AB8D7284380}']
    procedure Initialize;
    procedure Finalize;
    function  ShowLoginForm(var aHostPort, aUserName, aPassword: String): Boolean;
  end;


  function CreateGUI: IpbGUI;

implementation

uses pbCltMainboard, pbLoginFrm;

type
  TpbGUI = class(TisInterfacedObject, IpbGUI)
  private
    FWizardForm: TpbWizard;
  protected
    procedure Initialize;
    procedure Finalize;
    function  ShowLoginForm(var aHostPort, aUserName, aPassword: String): Boolean;
  end;


function CreateGUI: IpbGUI;
begin
  Result := TpbGUI.Create;
end;

  
{ TpbGUI }

procedure TpbGUI.Finalize;
begin
  FreeAndNil(FWizardForm);
end;

procedure TpbGUI.Initialize;
begin
  Application.CreateForm(TpbWizard, FWizardForm);
  FWizardForm.Show;
end;

function TpbGUI.ShowLoginForm(var aHostPort, aUserName, aPassword: String): Boolean;
begin
  Result := pbLoginFrm.ShowLoginForm(aHostPort, aUserName, aPassword);
end;

end.

