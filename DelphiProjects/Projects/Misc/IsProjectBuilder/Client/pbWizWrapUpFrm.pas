unit pbWizWrapUpFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, pbInterfaces, StdCtrls, isBaseClasses;

type
  TpbWizWrapUp = class(TpbWizardPage)
    lTitle: TLabel;
    chbConfirm: TCheckBox;
    procedure chbConfirmClick(Sender: TObject);
  private
    FTask: IpbTask;
  public
    procedure Init; override;
    function CanMoveForward: Boolean; override;
    function GetNextPage: TpbWizardPage; override;
    function NextButtonName: String; override;
  end;


implementation

uses pbCltMainboard, pbWizProgressFrm;

{$R *.dfm}


{ TpbWizWrapUp }

function TpbWizWrapUp.CanMoveForward: Boolean;
begin
  Result := chbConfirm.Checked;
end;

function TpbWizWrapUp.GetNextPage: TpbWizardPage;
begin
  Mainboard.ProjectBuilder.RunTask(FTask);
  Result := TpbWizProgress.Create(Owner, nil);
end;

procedure TpbWizWrapUp.Init;
var
  Info: IisListOfValues;
  i: Integer;
  L: TLabel;
  t: Integer;
begin
  inherited;
  FTask := IInterface(FParams.Value['Task']) as IpbTask;

  Info := IInterface(FParams.Value['Info']) as IisListOfValues;
  t := 81;
  for i := 0 to Info.Count - 1 do
  begin
    L := TLabel.Create(Self);
    L.Caption := Info[i].Name;
    L.Left := lTitle.Left;
    L.Top := t;
    L.Font.Name := 'Arial';
    L.Font.Size := 10;
    L.Font.Style := [fsBold];
    L.Parent := Self;

    L := TLabel.Create(Self);
    L.Caption := Info[i].Value;
    L.Left := 130;
    L.Top := t;
    L.Font.Name := 'Arial';
    L.Font.Size := 10;
    L.Parent := Self;

    Inc(t, L.Height + 10);
  end;
end;

function TpbWizWrapUp.NextButtonName: String;
begin
  Result := FParams.TryGetValue('NextButtonName', 'Next');
end;

procedure TpbWizWrapUp.chbConfirmClick(Sender: TObject);
begin
  NotifyChange;
end;

end.
 