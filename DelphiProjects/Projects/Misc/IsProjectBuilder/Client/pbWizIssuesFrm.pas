unit pbWizIssuesFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbFormFrm, ComCtrls, ImgList, isBaseClasses, StdCtrls;

type
  TpbWizIssues = class(TpbForm)
    btnClose: TButton;
    tvIssues: TTreeView;
    gbDetails: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    memDescription: TMemo;
    Label3: TLabel;
    lResoNbr: TLabel;
    lStatus: TLabel;
    procedure tvIssuesChange(Sender: TObject; Node: TTreeNode);
    procedure tvIssuesCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure tvIssuesCompare(Sender: TObject; Node1, Node2: TTreeNode;
      Data: Integer; var Compare: Integer);
  private
    procedure Init(const aIssues: IisParamsCollection);
    procedure UpdateDetails(const aIssue: IisListOfValues);
  public
  end;

  procedure ShowIssueList(const ACaption: String; const aIssues: IisParamsCollection);

implementation

uses Math;

{$R *.dfm}

type
  TIssueNode = class(TTreeNode)
  private
    FIssue: IisListOfValues;
    FGroup: String;
    FSortNbr: Integer;
  public
    constructor Create(AOwner: TTreeNodes; AParent: TTreeNode; aIssue: IisListOfValues); overload;
    constructor Create(AOwner: TTreeNodes; const aGroup, aDescription: String; const ASortNbr: Integer); overload;
  end;


procedure ShowIssueList(const ACaption: String; const aIssues: IisParamsCollection);
var
  Frm: TpbWizIssues;
begin
  Frm := TpbWizIssues.Create(nil);
  try
    Frm.Caption := ACaption;
    Frm.Init(aIssues);
    Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

{ TIssueNode }

constructor TIssueNode.Create(AOwner: TTreeNodes; AParent: TTreeNode; aIssue: IisListOfValues);
var
  s: String;
begin
  inherited Create(AOwner);
  FIssue := aIssue;
  s := 'Unassigned';
  if Assigned(aIssue) then
    s := FIssue.TryGetValue('ResoNbr', s);
  AOwner.AddNode(Self, AParent, s, nil, naAddChild);
end;


constructor TIssueNode.Create(AOwner: TTreeNodes; const aGroup, aDescription: String; const ASortNbr: Integer);
begin
  inherited Create(AOwner);
  FGroup := aGroup;
  FSortNbr := ASortNbr;
  AOwner.AddNode(Self, nil, aDescription, nil, naAdd);
end;

{ TpbWizIssues }

procedure TpbWizIssues.Init(const aIssues: IisParamsCollection);
var
  UsedItems: IisStringList;
  IncompleteNode: TTreeNode;

  procedure BuildGroup(const AStatus, ADescription: String; const ACheckIncomplete: Boolean);
  var
    i: Integer;
    Issue: IisListOfValues;
    TopNode: TTreeNode;
  begin
    TopNode := nil;
    for i := 0 to aIssues.Count - 1 do
    begin
      Issue := aIssues[i];
      if ((AStatus = '') or AnsiSameText(Issue.TryGetValue('Status', ''), AStatus))
         and (UsedItems.IndexOf(aIssues.ParamName(i)) = -1) then
      begin
        if ACheckIncomplete and not Issue.Value['Complete'] then
          TIssueNode.Create(tvIssues.Items, IncompleteNode, Issue)
        else
        begin
          if not Assigned(TopNode) then
            TopNode := TIssueNode.Create(tvIssues.Items, AStatus, ADescription, 0);
          TIssueNode.Create(tvIssues.Items, TopNode, Issue);
        end;

        UsedItems.Add(aIssues.ParamName(i));
      end;
    end;
  end;

begin
  tvIssues.Items.BeginUpdate;
  try
    tvIssues.SortType := stNone;

    tvIssues.Items.Clear;

    if Assigned(aIssues) then
    begin
      UsedItems := TisStringList.CreateAsIndex;
      IncompleteNode := TIssueNode.Create(tvIssues.Items, 'Incomplete', 'Incomplete', 0);

      BuildGroup('Testing', 'Ready For Testing', True);
      BuildGroup('Programming', 'Went Back To Development', False);
      BuildGroup('Verified', 'Verified', True);
      BuildGroup('To Be Documented',  'To Be Documented', True);
      BuildGroup('Closed', 'Closed', True);
      BuildGroup('', 'Miscellaneous', True);
    end;

    if IncompleteNode.Count = 0 then
      IncompleteNode.Free;

    tvIssues.SortType := stText;
  finally
    tvIssues.Items.EndUpdate;
  end;

  if tvIssues.Items.Count > 0 then
  begin
    tvIssues.Items[0].Expand(False);
    tvIssues.Items[0].Selected := True;
  end;
end;

procedure TpbWizIssues.tvIssuesChange(Sender: TObject; Node: TTreeNode);
begin
  UpdateDetails((Node as TIssueNode).FIssue);
end;

procedure TpbWizIssues.UpdateDetails(const aIssue: IisListOfValues);
begin
  if Assigned(aIssue) then
  begin
    lResoNbr.Caption := aIssue.TryGetValue('ResoNbr', '');
    lStatus.Caption := aIssue.TryGetValue('Status', '');
    memDescription.Text := aIssue.TryGetValue('Description', '');
  end
  else
  begin
    lResoNbr.Caption := '';
    lStatus.Caption := '';
    memDescription.Text := '';
  end;
end;

procedure TpbWizIssues.tvIssuesCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
var
  N: TIssueNode;
  Clr: TColor;
begin
  N := Node as TIssueNode;
  if N.FGroup <> '' then
    Sender.Canvas.Font.Style := Sender.Canvas.Font.Style + [fsBold];

  if not (cdsSelected in State) then
  begin
    if (N.FGroup = 'Incomplete') or (Assigned(N.Parent) and ((N.Parent as TIssueNode).FGroup = 'Incomplete')) then
      Clr := clRed
    else if (N.FGroup = 'Testing') or (Assigned(N.Parent) and ((N.Parent as TIssueNode).FGroup = 'Testing')) then
      Clr := clBlue
    else if (N.FGroup = 'Programming') or (Assigned(N.Parent) and ((N.Parent as TIssueNode).FGroup = 'Programming')) then
      Clr := clMaroon
    else if (N.FGroup = 'Verified') or (Assigned(N.Parent) and ((N.Parent as TIssueNode).FGroup = 'Verified')) then
      Clr := clGreen
    else if (N.FGroup = 'To Be Documented') or (Assigned(N.Parent) and ((N.Parent as TIssueNode).FGroup = 'To Be Documented')) then
      Clr := clGray
    else if (N.FGroup = 'Closed') or (Assigned(N.Parent) and ((N.Parent as TIssueNode).FGroup = 'Closed')) then
      Clr := clSilver
    else
      Clr := Sender.Canvas.Font.Color;

    Sender.Canvas.Font.Color := Clr;
  end;
end;

procedure TpbWizIssues.tvIssuesCompare(Sender: TObject; Node1,
  Node2: TTreeNode; Data: Integer; var Compare: Integer);
var
  N1, N2: TIssueNode;
begin
  N1 := Node1 as TIssueNode;
  N2 := Node2 as TIssueNode;
  if N1.FGroup <> '' then
    Compare := 0
  else
    Compare := CompareText(N1.Text, N2.Text);
end;

end.
