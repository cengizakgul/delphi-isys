unit pbWizHFPrepPrjSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbCltMainboard, pbInterfaces, isBaseClasses,
  pbWizIssuesFrm, pbClasses;

type
  TpbWizHFPrepPrjSelect = class(TpbWizardPage)
    Label2: TLabel;
    cbProducts: TComboBox;
    Label3: TLabel;
    Label1: TLabel;
    cbProjects: TComboBox;
    Label4: TLabel;
    lNewVesion: TLabel;
    btnIssues: TButton;
    Label5: TLabel;
    memDescr: TMemo;
    procedure cbProductsChange(Sender: TObject);
    procedure cbProjectsChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure btnIssuesClick(Sender: TObject);
  private
    FProducts: IpbProducts;
    function  SelectedProject: IpbProject;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TpbWizardPage; override;
  end;

implementation

uses pbWizWrapUpFrm;

{$R *.dfm}

{ TpbWizReleasePrjSelect }

function TpbWizHFPrepPrjSelect.GetNextPage: TpbWizardPage;
var
  Params, Info: IisListOfValues;
  Task: IpbTask;
begin
  Task := TpbTask.Create(ttPrepareHotFix,
    Format('Prepare for hot-fix %s %s', [SelectedProject.Product.Name, SelectedProject.Version.AsString]));

  Task.Params.AddValue('ProductName', SelectedProject.Product.Name);
  Task.Params.AddValue('ProjectName', SelectedProject.Name);

  Info := TisListOfValues.Create;
  Info.AddValue('Action', 'Prepare SCM for hot-fix');
  Info.AddValue('Product', SelectedProject.Product.Name);
  Info.AddValue('Released ver', SelectedProject.Version.AsString);
  Info.AddValue('Hot-fix ver', SelectedProject.Version.NextHotFix.AsString);

  Params := TisListOfValues.Create;
  Params.AddValue('NextButtonName', 'Run');
  Params.AddValue('Info', Info);
  Params.AddValue('Task', Task);

  Result := TpbWizWrapUp.Create(Owner, Params);
end;

procedure TpbWizHFPrepPrjSelect.Init;
var
  i: Integer;
begin
  inherited;
  FProducts := Mainboard.ProjectBuilder.GetProducts;
  cbProducts.Clear;
  for i := 0 to FProducts.Count - 1 do
    cbProducts.AddItem(FProducts[i].Name, Pointer(FProducts[i]));
  cbProductsChange(nil);
end;

procedure TpbWizHFPrepPrjSelect.cbProductsChange(Sender: TObject);
var
  i, j: Integer;
  Prod: IpbProduct;
  Prj, LastPrj: IpbProject;
  HFActive: Boolean;
  L: IisStringList;
begin
  if cbProducts.ItemIndex <> -1 then
  begin
    Prod := IpbProduct(Pointer(cbProducts.Items.Objects[cbProducts.ItemIndex]));
    cbProjects.Clear;
    Mainboard.ProjectBuilder.UpdateProjects(Prod);

    // Collect all released versions
    L := TisStringList.Create;
    for i := 0 to Prod.ProjectCount - 1 do
    begin
      Prj := Prod.Projects[i];
      if Prj.ProjectType = ptBinRelease then
        L.AddObject(Prj.Version.AsString(4), Prj);
    end;

    // Add only not hot fixed versions and latest hot fixes
    L.Sort;
    LastPrj := nil;
    for i := L.Count - 1 downto 0 do
    begin
      Prj := L.Objects[i] as IpbProject;
      if not Assigned(LastPrj) or
        (Prj.Version.Major <> LastPrj.Version.Major) or (Prj.Version.Minor <> LastPrj.Version.Minor) then
      begin
        LastPrj := Prj;
        // Check if HF is already active
        HFActive := False;
        for j := 0 to Prod.ProjectCount - 1 do
        begin
          Prj := Prod.Projects[j];
          if (Prj.ProjectType = ptHotfix) and
             (Prj.Version.Major = LastPrj.Version.NextHotFix.Major) and
             (Prj.Version.Minor = LastPrj.Version.NextHotFix.Minor) and
             (Prj.Version.Patch = LastPrj.Version.NextHotFix.Patch) then
          begin
            HFActive := True;
            break;
          end;
        end;

        if not HFActive then
          cbProjects.AddItem(LastPrj.Version.AsString, Pointer(LastPrj));
      end;
    end;
  end;
  cbProjectsChange(nil);
end;

function TpbWizHFPrepPrjSelect.CanMoveForward: Boolean;
begin
  Result := cbProjects.ItemIndex <> -1;
end;

procedure TpbWizHFPrepPrjSelect.cbProjectsChange(Sender: TObject);
begin
  if SelectedProject <> nil then
  begin
    lNewVesion.Caption := SelectedProject.Version.NextHotFix.AsString;
    memDescr.Enabled := True;
    memDescr.Text := SelectedProject.Description;
    btnIssues.Enabled := (IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection).Count > 0;
  end
  else
  begin
    lNewVesion.Caption := '';
    memDescr.Enabled := False;    
    btnIssues.Enabled := False;
  end;

  NotifyChange;
end;

function TpbWizHFPrepPrjSelect.SelectedProject: IpbProject;
begin
  if cbProjects.ItemIndex <> - 1 then
    Result := IpbProject(Pointer(cbProjects.Items.Objects[cbProjects.ItemIndex]))
  else
    Result := nil;
end;

procedure TpbWizHFPrepPrjSelect.FrameResize(Sender: TObject);
begin
  inherited;
  memDescr.Width := ClientWidth - memDescr.Left * 2;
  memDescr.Height := ClientHeight - memDescr.Top - memDescr.Left * 2;
end;

procedure TpbWizHFPrepPrjSelect.btnIssuesClick(Sender: TObject);
begin
  ShowIssueList('Issues to be included in ' + SelectedProject.Version.AsString,
    IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection);
end;

end.
