unit pbWizBuildPrjSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pbWizardPageFrm, StdCtrls, pbCltMainboard, pbInterfaces, isBaseClasses,
  pbWizIssuesFrm, pbClasses, Mask;

type
  TpbWizBuildPrjSelect = class(TpbWizardPage)
    Label2: TLabel;
    cbProducts: TComboBox;
    Label3: TLabel;
    Label1: TLabel;
    cbProjects: TComboBox;
    Label4: TLabel;
    lNewVesion: TLabel;
    btnIssues: TButton;
    Label5: TLabel;
    memDescr: TMemo;
    chbPublish: TCheckBox;
    edVersion: TMaskEdit;
    procedure cbProductsChange(Sender: TObject);
    procedure cbProjectsChange(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure btnIssuesClick(Sender: TObject);
    procedure edVersionExit(Sender: TObject);
  private
    FProducts: IpbProducts;
    function  SelectedProject: IpbProject;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TpbWizardPage; override;
  end;

implementation

uses pbWizWrapUpFrm, Types;

{$R *.dfm}

{ TpbWizBuildPrjSelect }

function TpbWizBuildPrjSelect.GetNextPage: TpbWizardPage;
var
  Params, Info: IisListOfValues;
  Task: IpbTask;
begin
  Task := TpbTask.Create(ttBuild,
    Format('Build %s %s (%s)', [SelectedProject.Product.Name, SelectedProject.Version.AsString,
                                SelectedProject.Name]));

  Task.Params.AddValue('ProductName', SelectedProject.Product.Name);
  Task.Params.AddValue('ProjectName', SelectedProject.Name);
  Task.Params.AddValue('Description', memDescr.Text);
  Task.Params.AddValue('Publish', chbPublish.Checked);

  if edVersion.Visible then
    Task.Params.AddValue('Version', SelectedProject.Version.AsString);

  Info := TisListOfValues.Create;
  Info.AddValue('Action', 'Build new version of a product');
  Info.AddValue('Product', SelectedProject.Product.Name);
  Info.AddValue('Build type', SelectedProject.Name);
  Info.AddValue('Build version', SelectedProject.Version.AsString);
  if chbPublish.Checked then
    Info.AddValue('Publish', 'Yes');

  Params := TisListOfValues.Create;
  Params.AddValue('Info', Info);
  Params.AddValue('NextButtonName', 'Build Now!');
  Params.AddValue('Task', Task);

  Result := TpbWizWrapUp.Create(Owner, Params);
end;

procedure TpbWizBuildPrjSelect.Init;
var
  i: Integer;
begin
  lNewVesion.Caption := '';
  edVersion.Visible := False;
  inherited;
  FProducts := Mainboard.ProjectBuilder.GetProducts;
  cbProducts.Clear;
  for i := 0 to FProducts.Count - 1 do
    cbProducts.AddItem(FProducts[i].Name, Pointer(FProducts[i]));
  cbProductsChange(nil);
end;

procedure TpbWizBuildPrjSelect.cbProductsChange(Sender: TObject);
var
  i, j: Integer;
  Prod: IpbProduct;
  Prj: IpbProject;
  Issues: IisParamsCollection;
  bAdd: Boolean;
begin
  if cbProducts.ItemIndex <> -1 then
  begin
    Prod := IpbProduct(Pointer(cbProducts.Items.Objects[cbProducts.ItemIndex]));
    Mainboard.ProjectBuilder.UpdateProjects(Prod);

    cbProjects.Clear;
    for i := 0 to Prod.ProjectCount - 1 do
    begin
      Prj := Prod.Projects[i];
      if Prj.Properties.ValueExists('BuildCmd') and (Prj.GetPropValue('BuildCmd') <> '') then
      begin
        if Prj.ProjectType in [ptDevelopment, ptHotfix] then
          bAdd := True

        else if Prj.ProjectType = ptFeature then
        begin
          Issues := IInterface(Prj.Properties.Value['Issues']) as IisParamsCollection;
          bAdd := True;
          for j := 0 to Issues.Count - 1 do
            if not AnsiSameText(Issues[j].Value['Status'], 'Testing') then
            begin
              bAdd := False;
              break;
            end
        end

        else
          bAdd := False;

        if bAdd then
          cbProjects.AddItem(Prj.Name, Pointer(Prj));
      end;
    end;

    // Sort Projects
    cbProjects.Sorted := True;
    cbProjects.Sorted := False;

    j := 0;
    for i := 0 to cbProjects.Items.Count - 1 do
    begin
      if IpbProject(Pointer(cbProjects.Items.Objects[i])).ProjectType = ptDevelopment then
      begin
        cbProjects.Items.Move(i, j);
        Inc(j); 
      end;
    end;

    for i := 0 to cbProjects.Items.Count - 1 do
    begin
      if IpbProject(Pointer(cbProjects.Items.Objects[i])).ProjectType = ptHotfix then
      begin
        cbProjects.Items.Move(i, j);
        Inc(j);
      end;
    end;
  end;


  cbProjectsChange(nil);
end;

function TpbWizBuildPrjSelect.CanMoveForward: Boolean;
var
  CurrProject: IpbProject;
  Issues: IisParamsCollection;
  i: Integer;
  Issue: IisListOfValues;
begin
  CurrProject := SelectedProject;
  if Assigned(CurrProject) then
  begin
    Result := True;
    Issues := IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection;
    for i := 0 to Issues.Count - 1 do
    begin
      Issue := Issues[i];
      if not AnsiSameText(Issue.TryGetValue('Status', ''), 'Programming') and not Issue.Value['Complete'] then
      begin
        Result := False;
        break;
      end;
    end;
  end
  else
    Result := False;
end;

procedure TpbWizBuildPrjSelect.cbProjectsChange(Sender: TObject);
begin
  if SelectedProject <> nil then
  begin
    if SelectedProject.Properties.TryGetValue('VersionOverride', False) then
    begin
      lNewVesion.Caption := IntToStr(SelectedProject.Version.Major) + '.';
      edVersion.Text := IntToStr(SelectedProject.Version.Minor) + '.' + IntToStr(SelectedProject.Version.Patch) + '.' + IntToStr(SelectedProject.Version.Build);
      edVersion.Visible := True;
    end
    else
    begin
      lNewVesion.Caption := SelectedProject.Version.AsString;
      edVersion.Visible := False;
    end;

    memDescr.Enabled := True;
    memDescr.Text := SelectedProject.Description;
    btnIssues.Enabled := (IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection).Count > 0;
  end
  else
  begin
    lNewVesion.Caption := '';
    memDescr.Enabled := False;
    btnIssues.Enabled := False;
    edVersion.Visible := False;
  end;

  NotifyChange;
end;

function TpbWizBuildPrjSelect.SelectedProject: IpbProject;
begin
  if cbProjects.ItemIndex <> - 1 then
    Result := IpbProject(Pointer(cbProjects.Items.Objects[cbProjects.ItemIndex]))
  else
    Result := nil;
end;

procedure TpbWizBuildPrjSelect.FrameResize(Sender: TObject);
begin
  inherited;
  memDescr.Width := ClientWidth - memDescr.Left * 2;
  memDescr.Height := ClientHeight - memDescr.Top - memDescr.Left * 2;
  chbPublish.Top :=  memDescr.BoundsRect.Bottom + 10;
end;

procedure TpbWizBuildPrjSelect.btnIssuesClick(Sender: TObject);
begin
  ShowIssueList('Issues to be included in ' + SelectedProject.Version.AsString,
    IInterface(SelectedProject.Properties.Value['Issues']) as IisParamsCollection);
end;

procedure TpbWizBuildPrjSelect.edVersionExit(Sender: TObject);
var
  s: String;
  Ver: IpbVersion;
begin
  s :=IntToStr(SelectedProject.Version.Major) + '.' + StringReplace(edVersion.Text, ' ', '', [rfReplaceAll]);
  Ver := TpbVersion.Create(s);
  SelectedProject.Properties.Value['Version'] := Ver.AsString;
end;

end.
