unit pbClient;

interface

uses SysUtils, Controls, Forms,
     isBaseClasses, EvTransportShared, EvTransportInterfaces, EvTransportDatagrams,
     pbInterfaces, pbRemoteMethods, EvStreamUtils, isThreadManager;

type
   IpbProxyObject = interface
   ['{1D671E95-99EF-429D-BBA1-75E8D0F618B9}']
     function Host: String;
     function Port: Integer;
     function Connected: Boolean;          
   end;

function CreateProjectBuilder(const AHost: String; const APort: Integer; const AUser, APassword: String): IpbProjectBuilder; stdcall;
     
implementation

uses pbCltMainboard;

type
  TpbClientDispatcher = class(TevDatagramDispatcher)
  end;


  IpbTCPClient = interface
  ['{DF8DEA1E-DB00-4202-BB15-8B8EDC687B95}']
    function  FindSession(const ASessionID: TisGUID): IevSession;
    function  CreateConnection(const AHost, APort, AUser, APassword: String;
                               const ASessionID, AContextID: TisGUID;
                               const ALocalIPAddr: String = ''; const AAppID: String = ''): TisGUID;
    procedure Disconnect(const ASessionID: String);
    function  GetSession(const ASessionID: TisGUID): IevSession;
  end;


  TpbTCPClient = class(TevCustomTCPClient, IpbTCPClient)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


  TpbProjectBuilderProxy = class(TisInterfacedObject, IpbProxyObject, IpbProjectBuilder, IpbIssueTracking)
  private
    FHost: String;
    FPort: Integer;
    FUser: String;
    FPassword: String;
    FSessionID: TisGUID;
    function  GetSession: IevSession;
    function  CreateDatagram(const AMethod: String): IevDatagram;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
  protected
    function  Host: String;
    function  Port: Integer;
    function  Connected: Boolean;

    function  CurrentUserInfo: IpbUserInfo;
    procedure ChangeUserPassword(const AOldPassword, ANewPassword: String);

    function  GetProducts: IpbProducts;
    procedure UpdateProjects(const AProduct: IpbProduct);
    function  GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;

    procedure RunTask(const ATask: IpbTask);
    function  GetCurrentTask: IpbTask;

    function  IssueTracking: IpbIssueTracking;

    function  GetUserList: IisListOfValues;
    function  GetIssues(const AQuery: String): IisParamsCollection;
    function  AddIssue(const AIssue: IisListOfValues): IisListOfValues;
    procedure UpdateIssue(const AIssue: IisListOfValues);
    function  StreamExists(const AName: String): Boolean;
    procedure CreateStream(const AName: String; const AParentName: String);
    procedure MoveStream(const AName: String; const AParentName: String);
    procedure DeleteStream(const AName: String);
  public
    constructor Create(const AHost: String; const APort: Integer; const AUser, APassword: String); reintroduce;
  end;


var
  FTCPClient: IpbTCPClient;


function CreateProjectBuilder(const AHost: String; const APort: Integer; const AUser, APassword: String): IpbProjectBuilder; stdcall;
begin
  Result := TpbProjectBuilderProxy.Create(AHost, APort, AUser, APassword);
end;

exports CreateProjectBuilder;


{ TpbTCPClient }

function TpbTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TpbClientDispatcher.Create;
end;


{ TpbProjectBuilderProxy }

function TpbProjectBuilderProxy.AddIssue(const AIssue: IisListOfValues): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_ADDISSUE);
  D.Params.AddValue('AIssue', AIssue);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TpbProjectBuilderProxy.ChangeUserPassword(const AOldPassword, ANewPassword: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_CHANGEUSERPASSWORD);
  D.Params.AddValue('AOldPassword', AOldPassword);
  D.Params.AddValue('ANewPassword', ANewPassword);
  D := SendRequest(D);
end;

function TpbProjectBuilderProxy.Connected: Boolean;
var
  Session: IevSession;
begin
  Session := FTCPClient.GetSession(FSessionID);
  Result := Assigned(Session) and (Session.State = ssActive);
end;

constructor TpbProjectBuilderProxy.Create(const AHost: String; const APort: Integer; const AUser, APassword: String);
begin
  inherited Create;
  FHost := AHost;
  FPort := APort;
  FUser := AUser;
  FPassword := APassword;
end;

function TpbProjectBuilderProxy.CreateDatagram(const AMethod: String): IevDatagram;
begin
  Result := TevDatagram.Create(AMethod);
  Result.Header.User := FUser;
  Result.Header.Password := FPassword;
end;

procedure TpbProjectBuilderProxy.CreateStream(const AName, AParentName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_CREATESTREAM);
  D.Params.AddValue('AName', AName);
  D.Params.AddValue('AParentName', AParentName);
  D := SendRequest(D);
end;

function TpbProjectBuilderProxy.CurrentUserInfo: IpbUserInfo;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_CURRENTUSERINFO);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IpbUserInfo;
end;

procedure TpbProjectBuilderProxy.DeleteStream(const AName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_DELETESTREAM);
  D.Params.AddValue('AName', AName);
  D := SendRequest(D);
end;

function TpbProjectBuilderProxy.GetCurrentTask: IpbTask;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GETCURRENTTASK);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IpbTask;
end;

function TpbProjectBuilderProxy.GetIssues(const AQuery: String): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_GETISSUES);
  D.Params.AddValue('AQuery', AQuery);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TpbProjectBuilderProxy.GetProducts: IpbProducts;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GETPRODUCTS);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IpbProducts;
end;

function TpbProjectBuilderProxy.GetSession: IevSession;
begin
  Result := FTCPClient.FindSession(FSessionID);
  if not Assigned(Result) or (Result.State <> ssActive) then
  begin
    FSessionID := FTCPClient.CreateConnection(FHost, IntToStr(FPort), FUser, FPassword, '', '');
    Result := FTCPClient.FindSession(FSessionID);
  end;
end;

function TpbProjectBuilderProxy.GetUpdatedIssues(const AProject: IpbProject): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GETUPDATEDISSUES);
  D.Params.AddValue('ProductName', AProject.Product.Name);
  D.Params.AddValue('ProjectName', AProject.Name);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TpbProjectBuilderProxy.GetUserList: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_GETUSERLIST);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TpbProjectBuilderProxy.Host: String;
begin
  Result := FHost;
end;

function TpbProjectBuilderProxy.IssueTracking: IpbIssueTracking;
begin
  Result := Self;
end;

procedure TpbProjectBuilderProxy.MoveStream(const AName, AParentName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_MOVESTREAM);
  D.Params.AddValue('AName', AName);
  D.Params.AddValue('AParentName', AParentName);
  D := SendRequest(D);
end;

function TpbProjectBuilderProxy.Port: Integer;
begin
  Result := FPort;
end;

procedure TpbProjectBuilderProxy.RunTask(const ATask: IpbTask);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RUNTASK);
  D.Params.AddValue('ATask', ATask);
  D := SendRequest(D);
end;

function TpbProjectBuilderProxy.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
  PrevCursor: TCursor;
begin
  if CurrentThreadIsMain then
  begin
    PrevCursor := Screen.Cursor;
    Screen.Cursor := crHourGlass;
  end
  else
    PrevCursor := crDefault;

  try
    S := GetSession;
    ADatagram.Header.SessionID := S.SessionID;
    Result := S.SendRequest(ADatagram);
  finally
    if CurrentThreadIsMain then
      Screen.Cursor := PrevCursor;
  end;
end;

function TpbProjectBuilderProxy.StreamExists(const AName: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_STREAMEXISTS);
  D.Params.AddValue('AName', AName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TpbProjectBuilderProxy.UpdateIssue(const AIssue: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ISSUETRACKING_UPDATEISSUE);
  D.Params.AddValue('AIssue', AIssue);
  D := SendRequest(D);
end;

procedure TpbProjectBuilderProxy.UpdateProjects(const AProduct: IpbProduct);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_UPDATEPROJECTS);
  D.Params.AddValue('ProductName', AProduct.Name);
  D := SendRequest(D);
  (AProduct as IisInterfacedObject).AsStream := IInterface(D.Params.Value['ProductData']) as IisStream;
end;

initialization
  FTCPClient := TpbTCPClient.Create;

finalization
  FTCPClient := nil;

end.