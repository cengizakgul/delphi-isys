inherited pbWizWrapUp: TpbWizWrapUp
  Width = 414
  object lTitle: TLabel
    Left = 40
    Top = 20
    Width = 214
    Height = 16
    Caption = 'Please verify your selected action'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object chbConfirm: TCheckBox
    Left = 40
    Top = 279
    Width = 169
    Height = 17
    Caption = 'Yes, this is correct'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = chbConfirmClick
  end
end
