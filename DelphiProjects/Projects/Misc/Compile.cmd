@echo off

echo *Compiling
..\..\Common\OpenProject\isOpenProject.exe Compile.ops %1

IF ERRORLEVEL 1 GOTO error

echo *Patching binaries
xcopy /Y /Q .\AppProjects\*.mes ..\..\Bin\Misc\*.* > nul
for %%a in (..\..\Bin\Misc\*.exe) do ..\..\Common\External\Utils\StripReloc.exe /B %%a > nul
for %%a in (..\..\Bin\Misc\*.exe) do ..\..\Common\External\MadExcept\madExceptPatch.exe %%a > nul
for %%a in (..\..\Bin\Misc\*.dll) do ..\..\Common\External\MadExcept\madExceptPatch.exe %%a > nul
for %%a in (..\..\Bin\Misc\*.mes) do del /F /Q ..\..\Bin\Misc\*.mes
for %%a in (..\..\Bin\Misc\*.map) do del /F /Q ..\..\Bin\Misc\*.map

..\..\Common\External\Utils\UPX.exe -9 ..\..\Bin\Misc\isOpenProject.exe
..\..\Common\External\Utils\UPX.exe -9 ..\..\Bin\Misc\RegDelphi7.exe
..\..\Common\External\Utils\UPX.exe -9 ..\..\Bin\Misc\isExeProtector.exe

GOTO end

:error
PAUSE
EXIT 1

:end
PAUSE
