object MainForm: TMainForm
  Left = 375
  Top = 161
  Width = 870
  Height = 640
  Caption = 'ISystems Log File Viewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 337
    Width = 862
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object MainMenuBar: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 862
    Height = 24
    UseSystemFont = False
    ActionManager = ActionManager
    Caption = 'MainMenuBar'
    ColorMap.HighlightColor = 15660791
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = 15660791
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Spacing = 0
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 587
    Width = 862
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 10000
      end
      item
        Width = 50
      end>
  end
  object wwLogGrid: TwwDBGrid
    Left = 0
    Top = 53
    Width = 862
    Height = 284
    DisableThemesInTitle = True
    LineStyle = gls3D
    ControlInfoInDataset = False
    Selected.Strings = (
      'EventType'#9'20'#9'Event Type'#9'F'
      'TimeStamp'#9'24'#9'Time Stamp'#9'F'
      'EventClass'#9'25'#9'Event Class'#9'F'
      'Text'#9'61'#9'Text'
      'Details'#9'8000'#9'Details')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    OnRowChanged = wwLogGridRowChanged
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = LogDataSource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    KeyOptions = []
    MultiSelectOptions = [msoAutoUnselect, msoShiftSelect]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect]
    ParentFont = False
    TabOrder = 2
    TitleAlignment = taLeftJustify
    TitleFont.Charset = RUSSIAN_CHARSET
    TitleFont.Color = clBlack
    TitleFont.Height = -11
    TitleFont.Name = 'Comic Sans MS'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = wwLogGridCalcCellColors
    object wwLogGridIButton: TwwIButton
      Left = 0
      Top = 0
      Width = 13
      Height = 22
      AllowAllUp = True
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 24
    Width = 862
    Height = 29
    Caption = 'ToolBar1'
    Images = ImageList1
    TabOrder = 3
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Action = FileOpenAction
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 2
      Action = FindAction
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 340
    Width = 862
    Height = 247
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 4
    object StringMemo: TMemo
      Left = 1
      Top = 1
      Width = 860
      Height = 245
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object ActionManager: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Items = <
              item
                Action = FileOpenAction
                Caption = '&Open'
                ImageIndex = 0
              end
              item
                Action = Action1
              end
              item
                Caption = '-'
              end
              item
                Action = ExitAction
                Caption = '&Exit'
              end>
            Caption = '&File'
          end
          item
            Items = <
              item
                Action = FindAction
                Caption = '&Find...'
                ImageIndex = 1
                ShortCut = 16454
              end
              item
                Action = FindNextAction
                Caption = 'F&ind Next'
                ShortCut = 114
              end
              item
                Action = FilterAction
                Caption = 'Fi&lter...'
              end>
            Caption = '&View'
          end
          item
            Items = <
              item
                Action = AboutAction
                Caption = '&About'
              end>
            Caption = '&Help'
          end>
        ActionBar = MainMenuBar
      end
      item
      end>
    Images = ImageList1
    Left = 704
    Top = 24
    StyleName = 'XP Style'
    object AboutAction: TAction
      Category = 'Help'
      Caption = 'About'
      OnExecute = AboutActionExecute
    end
    object FileOpenAction: TAction
      Category = 'File'
      Caption = 'Open'
      ImageIndex = 0
      OnExecute = FileOpenActionExecute
    end
    object FindAction: TAction
      Category = 'View'
      Caption = 'Find...'
      ImageIndex = 1
      ShortCut = 16454
      OnExecute = FindActionExecute
      OnUpdate = FindActionUpdate
    end
    object FindNextAction: TAction
      Category = 'View'
      Caption = 'Find Next'
      ShortCut = 114
      OnExecute = FindNextActionExecute
      OnUpdate = FindNextActionUpdate
    end
    object FilterAction: TAction
      Category = 'View'
      Caption = 'Filter...'
      OnExecute = FilterActionExecute
      OnUpdate = FilterActionUpdate
    end
    object Action1: TAction
      Category = 'File'
      Caption = 'Export Events'
      OnExecute = Action1Execute
    end
    object ExitAction: TAction
      Category = 'File'
      Caption = 'Exit'
      OnExecute = ExitActionExecute
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'log'
    FileName = '*.log'
    Filter = 'ISystems Log Files|*.log'
    Title = 'Open Log File'
    Left = 672
    Top = 24
  end
  object ImageList1: TImageList
    Left = 736
    Top = 24
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001001000000000000008
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07F1863E07F1863E07F
      1863E07F1863E07F1863E07F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07F0000E07F1863E07F1863
      E07F1863E07F1863E07F1863E07F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000001863E07F1863E07F
      1863E07F1863E07F1863E07F1863000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07F000000001863E07F1863
      E07F1863E07F1863E07F1863E07F186300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07F0000000000000000
      000000001863E07F1863E07F1863E07F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07F0000E07F0000E07F0000
      E07F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07F0000E07F0000E07F
      0000E07F0000E07F0000E07F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07F0000E07F0000E07F0000
      E07F0000E07F0000E07F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07F0000E07F0000E07F
      0000E07F0000E07F0000E07F0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000E07F0000E07F0000E07F0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFFFFF00000000
      800707C100000000000347D100000000000147D1000000004001010100000000
      2000A0410000000050002241000000002A812241000000005553800300000000
      2AABD147000000005553C107000000002807E38F0000000083FFEBAF00000000
      FFFFE38F00000000FFFFFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object LogDataSource: TDataSource
    Left = 600
    Top = 24
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'log'
    FileName = '*.log'
    Filter = 'ISystems Log Files|*.log'
    Left = 672
    Top = 56
  end
end
