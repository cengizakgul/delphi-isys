unit FilterDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFilterDlg = class(TForm)
    InformationBox: TCheckBox;
    WarningBox: TCheckBox;
    ErrorBox: TCheckBox;
    FatalErrorBox: TCheckBox;
    UnknownBox: TCheckBox;
    OKBtn: TButton;
    CalcelBtn: TButton;
    TextEdit: TEdit;
    TextBox: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FilterDlg: TFilterDlg;

implementation

{$R *.dfm}

end.
