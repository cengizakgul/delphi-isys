unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ToolWin, ActnMan, ActnCtrls, ActnMenus, ActnList,
  XPStyleActnCtrls, ComCtrls, ImgList, isLogFile, isLogFileDataSet,
  DB, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, StdCtrls, Buttons;

type
  TMainForm = class(TForm)
    MainMenuBar: TActionMainMenuBar;
    ActionManager: TActionManager;
    FilterAction: TAction;
    AboutAction: TAction;
    FileOpenAction: TAction;
    StatusBar1: TStatusBar;
    OpenDialog: TOpenDialog;
    ImageList1: TImageList;
    wwLogGrid: TwwDBGrid;
    ToolBar1: TToolBar;
    LogDataSource: TDataSource;
    ToolButton1: TToolButton;
    Splitter1: TSplitter;
    Panel1: TPanel;
    StringMemo: TMemo;
    ToolButton2: TToolButton;
    FindAction: TAction;
    FindNextAction: TAction;
    wwLogGridIButton: TwwIButton;
    ExitAction: TAction;
    Action1: TAction;
    SaveDialog1: TSaveDialog;
    procedure FileOpenActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
    procedure FilterActionExecute(Sender: TObject);
    procedure wwLogGridRowChanged(Sender: TObject);
    procedure FindActionExecute(Sender: TObject);
    procedure FindActionUpdate(Sender: TObject);
    procedure FindNextActionExecute(Sender: TObject);
    procedure FindNextActionUpdate(Sender: TObject);
    procedure FilterActionUpdate(Sender: TObject);
    procedure wwLogGridCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ExitActionExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
  private
  public
    LogDataSet: TisLogDataSet;
    procedure FindText;
  end;

var
  MainForm: TMainForm;

implementation

uses FindDialog, FilterDialog;

{$R *.dfm}

procedure TMainForm.FileOpenActionExecute(Sender: TObject);
begin
  if OpenDialog.Execute then
  begin
    wwLogGrid.UnselectAll;  
    if LogDataSet.Active then
      LogDataSet.Close;
    LogDataSet.Free;
    LogDataSet := TisLogDataSet.Create(nil);
    LogDataSource.DataSet := LogDataSet;
    LogDataSet.FileName := OpenDialog.FileName;
    LogDataSet.Open;
    StatusBar1.Panels[0].Text := Format('Record: %d/%d',[LogDataSet.RecNo+1, LogDataSet.RecordCount]);
  end;
end;

procedure TMainForm.AboutActionExecute(Sender: TObject);
begin
  ShowMessage('ISystems Log File Viewer');
end;

procedure TMainForm.FilterActionExecute(Sender: TObject);
var
  AText: String;
begin
  if FilterDlg.ShowModal = mrOk then
  try
    Screen.Cursor := crHourGlass;
    LogDataSet.DisableControls;
    if FilterDlg.TextBox.Enabled then
      AText := FilterDlg.TextEdit.Text
    else
      AText := '';
    LogDataSet.ApplyFilter(FilterDlg.UnknownBox.Checked,
                           FilterDlg.InformationBox.Checked,
                           FilterDlg.WarningBox.Checked,
                           FilterDlg.ErrorBox.Checked,
                           FilterDlg.FatalErrorBox.Checked,
                           AText);
  finally
    Screen.Cursor := crDefault;
    LogDataSet.EnableControls;
  end;
end;

procedure TMainForm.wwLogGridRowChanged(Sender: TObject);
begin
  if not LogDataSet.Active then Exit;
  if LogDataSet.IsEmpty then
  begin
    StringMemo.Clear;
    Exit;
  end;
  if not LogDataSet.Eof and not LogDataSet.Bof then
  begin
    StatusBar1.Panels[0].Text := Format('Record: %d/%d',[LogDataSet.RecNo+1, LogDataSet.RecordCount]);
    StringMemo.Lines.Text :=
      'Event Type  : '+LogDataSet.FieldByName('EventType').AsString+#13#10+
      'Time Stamp  : '+LogDataSet.FieldByName('TimeStamp').AsString+#13#10+
      'Event class : '+LogDataSet.FieldByName('EventClass').AsString+#13#10+
      'Text        : '+LogDataSet.FieldByName('Text').AsString+#13#10+
      'Details     : '+#13#10+LogDataSet.FieldByName('Details').AsString;
  end;
end;

procedure TMainForm.FindActionExecute(Sender: TObject);
begin
  if FindDlg.ShowModal = mrOk then
    FindText;
end;

procedure TMainForm.FindActionUpdate(Sender: TObject);
begin
  FindAction.Enabled := LogDataSet.Active;
end;

procedure TMainForm.FindText;
var
  Found: Boolean;
  RecNo: Integer;
begin
  if Length(FindDlg.TextEdit.Text) > 0 then
  try
    Screen.Cursor := crHourGlass;
    LogDataSet.DisableControls;
    Found := False;
    RecNo := LogDataSet.RecNo;
    while not LogDataSet.Eof do
    begin
      LogDataSet.Next;
      if (pos(UpperCase(FindDlg.TextEdit.Text), UpperCase(LogDataSet.FieldByName('Text').AsString)) <> 0)
      or (pos(UpperCase(FindDlg.TextEdit.Text), UpperCase(LogDataSet.FieldByName('Details').AsString)) <> 0) then
      begin
        Found := True;
        break;
      end;
    end;
    if not Found then
    begin
      LogDataSet.RecNo := RecNo;
      MessageBox(Handle, 'No matches found!', 'Find', MB_OK + MB_ICONSTOP);
    end;
  finally
    Screen.Cursor := crDefault;
    LogDataSet.EnableControls;
  end;
end;

procedure TMainForm.FindNextActionExecute(Sender: TObject);
begin
  FindText;
end;

procedure TMainForm.FindNextActionUpdate(Sender: TObject);
begin
  FindNextAction.Enabled := Length(FindDlg.TextEdit.Text) > 0;
end;

procedure TMainForm.FilterActionUpdate(Sender: TObject);
begin
  FilterAction.Enabled := LogDataSet.Active;
end;

procedure TMainForm.wwLogGridCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  if Assigned(LogDataSet) then
  if LogDataSet.Active then
  if not LogDataSet.IsEmpty then
  if not (gdSelected in State) and not (gdFocused in State) and not Highlight then
  begin
    if LogDataSet.FieldByName('EventType').AsString = 'Error' then
      ABrush.Color := 10855411
    else
    if LogDataSet.FieldByName('EventType').AsString = 'Information' then
      ABrush.Color := clWhite
    else
    if LogDataSet.FieldByName('EventType').AsString = 'Warning' then
      ABrush.Color := 12320767
    else
    if LogDataSet.FieldByName('EventType').AsString = 'FatalError' then
      ABrush.Color := clRed;
  end;
  if Highlight then
    AFont.Style := [fsBold]
  else
    AFont.Style := [];
end;

procedure TMainForm.ExitActionExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  LogDataSet := TisLogDataSet.Create(nil);
  LogDataSource.DataSet := LogDataSet;
end;

procedure TMainForm.Action1Execute(Sender: TObject);
var
  L: ILogFile;
  i: Integer;
begin
  if SaveDialog1.Execute then
  begin
    L := TLogFile.Create(SaveDialog1.FileName);
    if wwLogGrid.SelectedList.Count > 0 then
      for i := 0 to wwLogGrid.SelectedList.Count - 1 do
      begin
        LogDataSet.GotoBookmark(wwLogGrid.SelectedList[i]);
        L.AddEventAs(LogDataSet.LogFile.Items[LogDataSet.FieldByName('PhisicalIndex').AsInteger]);
      end
    else
    begin
      L.AddEventAs(LogDataSet.LogFile.Items[LogDataSet.FieldByName('PhisicalIndex').AsInteger]);
    end;
    i := wwLogGrid.SelectedList.Count;
    L := nil;
    MessageBox(Handle, PChar(Format('%d events saved to %s',
      [i, SaveDialog1.FileName])), 'Export Events', MB_OK);
  end;
end;

end.
