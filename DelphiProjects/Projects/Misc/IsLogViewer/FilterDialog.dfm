object FilterDlg: TFilterDlg
  Left = 574
  Top = 302
  Width = 337
  Height = 195
  Caption = 'Filter'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object InformationBox: TCheckBox
    Left = 16
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Information'
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object WarningBox: TCheckBox
    Left = 16
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Warning'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object ErrorBox: TCheckBox
    Left = 120
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Error'
    Checked = True
    State = cbChecked
    TabOrder = 2
  end
  object FatalErrorBox: TCheckBox
    Left = 120
    Top = 80
    Width = 97
    Height = 17
    Caption = 'FatalError'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object UnknownBox: TCheckBox
    Left = 224
    Top = 56
    Width = 81
    Height = 17
    Caption = 'Unknown'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object OKBtn: TButton
    Left = 16
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Apply'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object CalcelBtn: TButton
    Left = 104
    Top = 120
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Calcel'
    ModalResult = 2
    TabOrder = 6
  end
  object TextEdit: TEdit
    Left = 112
    Top = 16
    Width = 193
    Height = 21
    TabOrder = 7
  end
  object TextBox: TCheckBox
    Left = 16
    Top = 16
    Width = 89
    Height = 17
    Caption = 'Contains text'
    TabOrder = 8
  end
end
