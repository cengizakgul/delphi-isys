unit FindDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFindDlg = class(TForm)
    Label1: TLabel;
    TextEdit: TEdit;
    FindBtn: TButton;
    CancelBtn: TButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FindDlg: TFindDlg;

implementation

{$R *.dfm}

procedure TFindDlg.FormShow(Sender: TObject);
begin
  TextEdit.SetFocus;
end;

end.
