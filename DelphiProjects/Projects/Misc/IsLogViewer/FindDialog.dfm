object FindDlg: TFindDlg
  Left = 528
  Top = 287
  BorderStyle = bsDialog
  Caption = 'Find'
  ClientHeight = 102
  ClientWidth = 311
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 21
    Height = 13
    Caption = 'Text'
  end
  object TextEdit: TEdit
    Left = 16
    Top = 32
    Width = 281
    Height = 21
    TabOrder = 0
  end
  object FindBtn: TButton
    Left = 224
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Find'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 144
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
