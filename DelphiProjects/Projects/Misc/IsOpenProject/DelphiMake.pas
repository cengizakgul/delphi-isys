unit DelphiMake;

interface

uses Classes, SysUtils, Dialogs, IniFiles, Windows, IsBaseClasses, isBasicUtils, DateUtils,
     isSettings, evStreamUtils, Variants;

type
  TOnCompileProgress = procedure (const AProgressStatus: String; const Progress: Integer) of object;

  TProject = record
    OutPutName: String;
    ProjectName: String;
  end;

  TProjectList = array of TProject;

  IBPGCompile = interface
  ['{149BFF24-1C6D-4107-9201-6D361C01965F}']
    procedure Build(const AProjectFile: String; const AVersion: String);
    function  ParseBPG(const AProjectFile: String): TProjectList;
    procedure CompileResources(const ARCFile: String);
  end;

  TBPGCompile = class(TInterfacedObject, IBPGCompile)
  private
    FOnProgressCallback: TOnCompileProgress;
    FProjectFile: String;
    FDelphiPath: String;
    FVersion: String;
    procedure Compile(const ACmd, AFile: String);
    procedure DoBuild;
    procedure CreateCfgFile(DofFile: String);
    function  CreateNewFileName(aFile, aExt: String): String;
    function  FindProjectIndexByName(const AProjectList: TProjectList; const AProjectOutPutName: String): Integer;
  protected
    procedure Build(const AProjectFile: String; const AVersion: String);
    function  ParseBPG(const AProjectFile: String): TProjectList;
    procedure CompileResources(const ARCFile: String);
    function  ParseDP(const AProjectFile: String): TProject;
  public
    constructor Create(const ADelphiPath: String; const OnProgressCallback: TOnCompileProgress = nil);
  end;

implementation

{ TBPGCompile }

procedure TBPGCompile.Build(const AProjectFile: String; const AVersion: String);
begin
  FProjectFile := AProjectFile;
  FVersion := Trim(AVersion);
  DoBuild;
end;

constructor TBPGCompile.Create(const ADelphiPath: String; const OnProgressCallback: TOnCompileProgress = nil);
begin
  FDelphiPath := DenormalizePath(ADelphiPath);
  FOnProgressCallback := OnProgressCallback;
end;

procedure TBPGCompile.CreateCfgFile(DofFile: String);
var
  Dof: IisSettings;
  Cfg: IisStream;
  sDelphiDebugPath: String;

  procedure WriteOption(const AOption: String);
  begin
    Cfg.WriteLn('-' + AOption);
  end;

  procedure CheckBoolValue(const AName, ACfgOption: String);
  var
    s: String;
    v: Variant;
  begin
    v := Dof.GetValue(AName, Null);
    if v = Null then
      Exit;

    s := ACfgOption;
    if v = '0' then
      s := s + '-'
    else if  v = '1' then
      s := s + '+'
    else
      s := s + v;  
    WriteOption(s);
  end;

  procedure CheckStringValue(const AName, ACfgOption: String);
  var
    s: String;
    V: String;
  begin
    V := Dof.GetValue(AName, '');
    if V = '' then
      Exit;

    v := StringReplace(V, '$(DELPHI)', FDelphiPath, [rfReplaceAll, rfIgnoreCase]);
    if Contains(V, ' ') then
      V := '"' + V + '"';

    s := ACfgOption + v;
    WriteOption(s);
  end;

  function MadExceptOn: Boolean;
  var
    s: String;
    Mes: IisSettings;
  begin
    Result := False;
    s := ChangeFileExt(DofFile, '.mes');
    if FileExists(s) then
    begin
      Mes := TisSettingsINI.Create(s);
      Result := Mes.AsString['GeneralSettings\HandleExceptions'] = '1';
    end;
  end;

begin
  Dof := TisSettingsINI.Create(DofFile);
  Cfg := TisStream.CreateFromNewFile(ChangeFileExt(DofFile, '.cfg'));

  CheckStringValue('Compiler\A', '$A');  // Align fields
  CheckBoolValue('Compiler\B', '$B');    // Boolean short-circuit evaluation
  CheckBoolValue('Compiler\C', '$C');    // Assert directives

  if MadExceptOn then           // Debug information
  begin
    WriteOption('$D+');
    sDelphiDebugPath := Format('"%s\Lib\Debug";', [FDelphiPath]);
  end
  else
  begin
    WriteOption('$D-');
    sDelphiDebugPath := '';
  end;

  CheckBoolValue('Compiler\E', '$E');    // Executable extension
  CheckBoolValue('Compiler\G', '$G');    // Imported data
  CheckBoolValue('Compiler\H', '$H');    // Long strings
  CheckBoolValue('Compiler\J', '$J');    // Writeable typed constants
  WriteOption('$O+');                    // Optimization
  CheckBoolValue('Compiler\P', '$P');    // Open String Parameters
  WriteOption('$Q-');                    // Overflow checking
  WriteOption('$R-');                    // Range checking
  CheckBoolValue('Compiler\T', '$T');    // Type-checked pointers
  CheckBoolValue('Compiler\U', '$U');    // Pentium-safe FDIV operations
  CheckBoolValue('Compiler\V', '$V');    // Var-string checking
  WriteOption('$W+');                    // Stack frames
  CheckBoolValue('Compiler\X', '$X');    // Extended syntax
  WriteOption('$Y-');                    // Symbol declaration and cross-reference information
  CheckStringValue('Compiler\Z', '$Z');  // Minimum enumeration size

  WriteOption('$M' + Dof.GetValue('Linker\MinStackSize', '16384') + ',' +
                     Dof.GetValue('Linker\MaxStackSize', '1048576')); //Memory allocation sizes

  WriteOption('GD');                     // Detailed map file

  case Dof.AsInteger['Linker\ConsoleApp'] of
  0:  WriteOption('CC');                     // GUI Target
  1:  WriteOption('CG');                     // Console Target
  end;

  case Dof.AsInteger['Linker\OutPutObjs'] of   // Output Obj files
  9:  WriteOption('J');
  10: WriteOption('JP');
  14: WriteOption('JPN');
  26: WriteOption('JPE');
  30: WriteOption('JPNE');
  end;

  WriteOption('H+');                     // Output hint messages
  WriteOption('W+');                     // Output warning messages
  WriteOption('M');                      // Make modified units
  WriteOption('B');                      // Build all units
  WriteOption('Q');                      // Quiet

  CheckStringValue('Compiler\UnitAliases', 'A');              // Unit alias
  WriteOption('K$' + IntToHex(Dof.GetValue('Linker\ImageBase', 4194304), 8)); // Image base addr
  CheckStringValue('Directories\OutputDir', 'E');             // EXE output directory
  CheckStringValue('Directories\UnitOutputDir', 'N');         // DCU output directory
  CheckStringValue('Directories\PackageDLLOutputDir', 'LE');  // BPL output directory
  CheckStringValue('Directories\PackageDCPOutputDir', 'LN');  // DCP output directory
  CheckStringValue('Directories\SearchPath', 'U' + sDelphiDebugPath);  // Unit directories
  CheckStringValue('Directories\SearchPath', 'O' + sDelphiDebugPath);  // Object directories
  CheckStringValue('Directories\SearchPath', 'I' + sDelphiDebugPath);  // Include directories
  CheckStringValue('Directories\SearchPath', 'R' + sDelphiDebugPath);  // Resource directories
  CheckStringValue('Directories\Conditionals', 'D');          // Define conditionals
end;

function TBPGCompile.CreateNewFileName(aFile, aExt: String): String;
var
  fPath: String;
  fName: String;
  fExt: String;
begin
  fPath := ExtractFilePath(aFile);
  fName := ExtractFileName(aFile);
  fExt := ExtractFileExt(aFile);
  Delete(fName, Pos(ExtractFileExt(aFile), fName), Length(ExtractFileExt(aFile)));
  if aExt <> 'prj' then
    fExt := '.'+aExt
  else
    if TrimRight(fExt) = '.bpl' then
      fExt := '.dpk'
    else
      fExt := '.dpr';
  result := fPath+fName+fExt;
end;

procedure TBPGCompile.DoBuild;
var
  i: Integer;
  PrcntStep, Prcnt: Real;
  PJ: TProjectList;
  ResFile: String;
  ResFileCont: IisStringList;
  s: String;

  procedure UpdateProgress(const BaseMessage: String);
  begin
    if Assigned(FOnProgressCallback) then
      FOnProgressCallback(BaseMessage + ExtractFileName(PJ[i].ProjectName), Round(Prcnt));
  end;

  procedure PatchResLine(const APattern, Value: String);
  var
    i: Integer;
  begin
    for i := 0 to ResFileCont.Count - 1 do
      if StartsWith(ResFileCont[i], APattern) then
      begin
        ResFileCont[i] := APattern + Value;
        break;
      end;
  end;

begin
  // Parse BPG
  if AnsiSameText(ExtractFileExt(FProjectFile), '.BPG') then
    PJ := ParseBPG(FProjectFile)
  else
  begin
    SetLength(PJ, 1);
    PJ[0] := ParseDP(FProjectFile);
  end;

  Prcnt := 0;
  PrcntStep := 100 / Length(PJ);

  // loop through project list and compile
  for i := Low(PJ) to High(PJ) do
  begin
    UpdateProgress('Compiling: ');

    CreateCfgFile(CreateNewFileName(PJ[i].ProjectName, 'dof'));
    ChDir(ExtractFilePath(PJ[i].ProjectName));

    // Compile resources
    ResFile := ChangeFileExt(PJ[i].ProjectName, '.rc');
    if FileExists(ResFile) then
    begin
      // Patch version number
      if FVersion <> '' then
      begin
        ResFileCont := TisStringList.Create;
        ResFileCont.LoadFromFile(ResFile);
        s := FVersion;
        PatchResLine('#define VER_MAJOR ', GetNextStrValue(s, '.'));
        PatchResLine('#define VER_MINOR ', GetNextStrValue(s, '.'));
        PatchResLine('#define VER_PATCH ', GetNextStrValue(s, '.'));
        PatchResLine('#define VER_BUILD ', s);
        PatchResLine('#define VER_CURRENT_YEAR ', '"' + IntToStr(YearOf(Today)) + '"');
        ResFileCont.SaveToFile(ResFile);
        ResFileCont := nil;
      end;

      CompileResources(ResFile);
    end;

    Compile(NormalizePath(FDelphiPath) + '\Bin\dcc32.exe', PJ[i].ProjectName);

    if i = High(PJ) then
      Prcnt := 100
    else
      Prcnt := Prcnt + PrcntStep;
  end;
end;


function TBPGCompile.ParseBPG(const AProjectFile: String): TProjectList;
var
  i, j: Integer;
  PrjOutPutName, PrjName: String;
  ProjectMarker: Boolean;
  BPGList: TStringList;
  PJ: TProjectList;
  procedure AddProjectLine(AString: String);
  var
    tmp: String;
  begin
    if Pos('PROJECTS =', AString) <> 0 then
      Delete(AString, 1, Pos('PROJECTS =', AString) + Length('PROJECTS ='));
    while Pos(' ', AString) > 0 do
    begin
      AString := Trim(StringReplace(AString, ' \', '', [rfReplaceAll, rfIgnoreCase]));
      tmp := Copy(AString, 1, Pos(' ', AString) -1);
      if tmp <> '' then
      begin
        SetLength(PJ, Length(PJ) + 1);
        PJ[Length(PJ) - 1].OutPutName := tmp;
      end;

      AString := Trim(StringReplace(AString, tmp, '', [rfReplaceAll, rfIgnoreCase]))
    end;
    SetLength(PJ, Length(PJ) + 1);
    PJ[Length(PJ) - 1].OutPutName := AString;
    AString := Trim(StringReplace(AString, tmp, '', [rfReplaceAll, rfIgnoreCase]))
  end;
begin
  SetLength(PJ, 0);

  ProjectMarker := False;

  BPGList := TStringList.Create;
  try
    BPGList.LoadFromFile(AProjectFile);

    ChDir(ExtractFilePath(AProjectFile));
    for i := 0 to BPGList.Count - 1 do
    begin
      if Pos('PROJECTS =', BPGList[i]) <> 0 then
        ProjectMarker := True;

      if Pos('#', BPGList[i]) <> 0 then
        ProjectMarker := False;

      if ProjectMarker then
        AddProjectLine(BPGList[i]);

      if Pos('$(DCC)', BPGList[i]) <> 0 then
      begin

        PrjOutPutName := Trim(Copy(BPGList[i-1], 1, Pos(':', BPGList[i-1])-1));

        PrjName := Trim(Copy(BPGList[i-1], Pos(':', BPGList[i-1])+1, Length(BPGList[i-1])));
        PrjName := ExpandFileName(PrjName);

        j := FindProjectIndexByName(PJ, PrjOutPutName);
        if j = -1 then
          raise Exception.Create('Project "' + PrjOutPutName + '" is invalid');

        PJ[j].ProjectName := PrjName;
      end;
    end;
    Result := PJ;
  finally
    BPGList.Free;
  end;
end;

function TBPGCompile.FindProjectIndexByName(const AProjectList: TProjectList; const AProjectOutPutName: String): Integer;
var
  i: Integer;
begin
  result := - 1;
  for i := Low(AProjectList) to High(AProjectList) do
  begin
    if AProjectList[i].OutPutName = AProjectOutPutName then
    begin
      result := i;
      Exit;
    end;
  end;
end;


function TBPGCompile.ParseDP(const AProjectFile: String): TProject;
var
  DP: IisStringList;
  i: Integer;
  s: String;
begin
  DP := TisStringList.Create;
  DP.LoadFromFile(AProjectFile);

  Result.ProjectName := AProjectFile;
  Result.OutPutName := '';

  for i := 0 to DP.Count - 1 do
  begin
    s := Trim(DP[i]);
    if StartsWith(s, 'program ') then
    begin
      Result.OutPutName := ChangeFileExt(ExtractFileName(AProjectFile), '.exe');
      break;
    end
    else if StartsWith(s, 'package ') then
    begin
      Result.OutPutName := ChangeFileExt(ExtractFileName(AProjectFile), '.bpl');
      break;
    end
    else if StartsWith(s, 'library ') then
    begin
      Result.OutPutName := ChangeFileExt(ExtractFileName(AProjectFile), '.dll');
      break;
    end;
  end;
end;

procedure TBPGCompile.CompileResources(const ARCFile: String);
begin
  Compile(NormalizePath(FDelphiPath) + '\Bin\brcc32.exe', ARCFile);
end;

procedure TBPGCompile.Compile(const ACmd, AFile: String);
var
  OutRes: IisStream;
  ResLine: String;
begin
  OutRes := TisStream.CreateInMemory;
  try
    RunProcess(ACmd, '"' + AFile + '"', [roHideWindow, roWaitForClose], OutRes);
    OutRes.Position := 0;
    while not OutRes.EOS do
    begin
      ResLine := StringReplace(OutRes.ReadLn(13), #10, '', [rfReplaceAll]);
      if StartsWith(ResLine, 'Error ', True) or
         Contains(ResLine, 'Fatal: ', True) or
         Contains(ResLine, ') Error: ', True) or
         Contains(ResLine, ') Warning: ', True) or
         Contains(ResLine, ') Hint: ', True) then
        raise Exception.Create(ResLine);
    end;
  except
    on E: Exception do
    begin
      E.Message := E.Message + sLineBreak + 'Project: ' + AFile;
      raise;
    end;
  end;
end;

end.
