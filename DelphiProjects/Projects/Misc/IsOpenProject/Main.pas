unit Main;

interface

uses Classes, Windows, SysUtils, Registry, AbUnzper, AbArcTyp, DelphiMake,
     isBasicUtils, isBaseClasses;

type
  TisVariable = class(TCollectionItem)
  private
    FName: String;
    FValue: Variant;
    FProtected: Boolean;
  public
    property Name: String read FName;
    property Value: Variant read FValue write FValue;
  end;

  TisVariables = class(TCollection)
  public
    function  FindVar(const AVarName: String): TisVariable;
    procedure SetVar(const AVarName: String; const AValue: Variant);
    function  GetVar(const AVarName: String): Variant;
  end;


  TisProjectOpener = class
  private
    FDelphiDir: String;
    FDelphiExe: String;
    FPathVar: String;
    FLibSearchPath: String;
    FLibBrowsePath: String;
    FScriptFileName: String;
    FVerboseMode: Boolean;
    FVariables: TisVariables;
    procedure CallScript(const AParams: array of String);
    procedure WriteOutput(const AText: String = '');
    procedure WriteLnOutput(const AText: String = '');
    procedure SyncRegistry;
  private
    function  GetNextStrValue(var AStr: string; const ADelim: String): String;
    function  ClearDir(const Path: string; Delete: Boolean = False): Boolean;
    function  FindFiles(const APath, APattern: String): String;
    procedure ProcessCmd(const Cmd: String; const Params: array of String);
    procedure FixupGlobalVars(var AStr: String);
    procedure FixupPath(var AStr: String);
    procedure OnProgress(const AProgressStatus: String; const Progress: Integer);
    procedure InstallPackage(const Params: array of String);
    procedure UnInstallPackage(const Params: array of String);
    procedure InstallHelp(const Params: array of String);
    procedure AddPath(const Params: array of String);
    procedure AddLibPath(const Params: array of String);
    procedure AddSearchPath(const Params: array of String);
    procedure CopyFile(const Params: array of String);
    procedure CopyFolder(const Params: array of String);
    procedure CopyFolderContent(const Params: array of String);
    procedure Unzip(const Params: array of String);
    procedure ExitScript(const Params: array of String);
    procedure RunDelphi(const Params: array of String);
    procedure Say(const Params: array of String);
    procedure Compile(const Params: array of String);
    procedure CompileRes(const Params: array of String);
    procedure CompileAllRes(const Params: array of String);
    procedure SetVar(const Params: array of String);
    procedure InputText(const Params: array of String);
    procedure IfThen(const Params: array of String);
    procedure Exec(const Params: array of String);
    procedure MakeDir(const Params: array of String);

  protected
    procedure DoFirstEntryInit;
    procedure DoProcess;
    procedure ProcessScript;
    procedure ShowHelp(AFullHelp: Boolean);
  public
    procedure   RunScript(const AFileName: String; const AParams: array of string);
    constructor Create;
    destructor  Destroy; override;
  end;


procedure Run;

implementation

{$R help.res}

const
  cfgFileName = 'isOpenProject.ops';
  PrifOpenProjectPackage = '(isOpenProject)';


function ReadFromRegistry(const Key, KeyField: string; Root: DWORD = HKEY_CURRENT_USER): string;
var
  TempRegistry: TRegistry;
begin
  Result := '';

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := Root;
      if OpenKeyReadOnly(Key) and ValueExists(KeyField) then
        Result := ReadString(KeyField);
    end;
  finally
    TempRegistry.Free;
  end;
end;


procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; Root: DWORD = HKEY_CURRENT_USER);
var
  TempRegistry: TRegistry;
begin
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := Root;
      if OpenKey(Key, NewValue <> '') then
        if NewValue = '' then
          DeleteValue(KeyField)
        else
          WriteString(KeyField, NewValue);
    end
  finally
    TempRegistry.Free;
  end;
end;


procedure ReadKeyFieldNames(const Key: string; AList: TStringList);
var
  TempRegistry: TRegistry;
begin
  AList.Clear;
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := HKEY_CURRENT_USER;
      if OpenKey(Key, False) then
        GetValueNames(AList);
    end;
  finally
    TempRegistry.Free;
  end;
end;

procedure CleanKeyFieldInRegistry(const Key, KeyField: string);
var
  TempRegistry: TRegistry;
begin
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := HKEY_CURRENT_USER;

      if OpenKey(Key, False) then
        if ValueExists(KeyField) then
          DeleteValue(KeyField);
    end
  finally
    TempRegistry.Free;
  end;
end;



procedure Run;
var
  Opener: TisProjectOpener;
  ProjectDir: String;
  Params: array of String;
  i: Integer;
begin
  Opener := TisProjectOpener.Create;
  try
    ProjectDir := ParamStr(1);

    if ProjectDir = '' then
    begin
      RegisterDefaultApp(AppFileName, '.ops', 'isOpenProject Script File');
      Opener.ShowHelp(False);
      exit;
    end
    else if ProjectDir = '/?' then
    begin
      Opener.ShowHelp(True);
      exit;
    end;

    Opener.FVerboseMode := FindCmdLineSwitch('V', ['/'], True);

    if ExtractFileName(ProjectDir) = '' then
      ProjectDir := NormalizePath(ExtractFilePath(ProjectDir)) + cfgFileName;

    Opener.DoFirstEntryInit;

    SetLength(Params, ParamCount - 1);
    for i := 2 to ParamCount do
      Params[i - 2] := ParamStr(i);

    Opener.RunScript(ProjectDir, Params);
  finally
    Opener.Free;
  end;
end;



{ TisProjectOpener }

procedure TisProjectOpener.RunScript(const AFileName: String; const AParams: array of string);
var
  i: Integer;
  PrevDir, CurrDir: String;
begin
  if not FileExists(AFileName) then
    raise Exception.Create('Script file "' + AFileName + '" is not found');

  PrevDir := GetCurrentDir;
  try
    SetCurrentDir(ExtractFilePath(AFileName));
    FScriptFileName := AFileName;

    // Internal variables
    FVariables.Clear;
    for i := Low(AParams) to High(AParams) do
      FVariables.SetVar('PARAM' + IntToStr(i + 1), AParams[i]);

    CurrDir := NormalizePath(GetCurrentDir);
    FixupPath(CurrDir);
    FVariables.SetVar('CURRENT_DIR', CurrDir);

    FVariables.SetVar('DELPHI_ROOT', FDelphiDir);

    for i := 0 to FVariables.Count - 1 do
      TisVariable(FVariables.Items[i]).FProtected := True;

    DoProcess;
  finally
    SetCurrentDir(PrevDir);
  end;
end;


procedure TisProjectOpener.ShowHelp(AFullHelp: Boolean);
var
  s: String;
  RS: TResourceStream;
begin
  if not AFullHelp then
  begin
    s := 'This utility helps to open Delphi''s 7 projects. Please use /? parameter to see the full help';
    MessageBox(0, PAnsiChar(s), PAnsiChar(ExtractFileName(ParamStr(0))), MB_OK or MB_ICONINFORMATION or MB_APPLMODAL);
  end

  else
  begin
    RS := TResourceStream.Create(HInstance, 'HELP', RT_RCDATA);
    try
      SetLength(s, RS.Size);
      RS.ReadBuffer(s[1], RS.Size);
    finally
      RS.Free;
    end;
    WriteLnOutput(s);
    WriteLnOutput;
    WriteLnOutput;
    WriteLnOutput('Press Enter...');
    Readln;
  end;
end;

procedure TisProjectOpener.DoProcess;
begin
  ProcessScript;
end;

procedure TisProjectOpener.RunDelphi(const Params: array of String);
var
  ProjectFile: String;
begin
  if FDelphiExe = '' then
    raise Exception.Create('Delphi 7 is not registered');

 ProjectFile := '';
 if (Length(Params) >= 1) then
 begin
  ProjectFile := Params[0];
  FixupGlobalVars(ProjectFile);
  FixupPath(ProjectFile);
  ProjectFile := '"' + ProjectFile + '"';
 end;

 RunProcess(FDelphiExe, ProjectFile, []);
end;

procedure TisProjectOpener.ProcessScript;
var
  Scrpt: Text;
  CmdLine, Cmd: String;
  Params: array of String;
  CmdCount: Integer;
begin
  if FScriptFileName = '' then
    Exit;

  CmdCount := 0;
  try
    AssignFile(Scrpt, FScriptFileName);
    try
      Reset(Scrpt);

      CleanKeyFieldInRegistry('\Software\Borland\Delphi\7.0\Environment Variables', 'Path');

      while not Eof(Scrpt) do
      begin
        Readln(Scrpt, CmdLine);
        if FVerboseMode then
          WriteLnOutput(CmdLine);
                  
        Inc(CmdCount);
        CmdLine := StringReplace(CmdLine, #9, ' ', [rfReplaceAll, rfIgnoreCase]);
        CmdLine := Trim(CmdLine);

        if (CmdLine <> '') and (CmdLine[1] <> '#') then
        begin
          Cmd := GetNextStrValue(CmdLine, ' ');
          CmdLine := Trim(CmdLine);
          SetLength(Params, 0);
          while CmdLine <> '' do
          begin
            CmdLine := TrimLeft(CmdLine);
            SetLength(Params, Length(Params) + 1);
            Params[High(Params)] := GetNextStrValue(CmdLine, ' ');
          end;
          ProcessCmd(Cmd, Params);
        end;
      end;
    finally
      CloseFile(Scrpt);
    end;

  except
    on E: EAbort do;
    on E: Exception do
    begin
      E.Message := E.Message + sLineBreak + ExtractFileName(FScriptFileName) + ': line ' + IntToStr(CmdCount);
      raise;
    end;
  end;
end;

function TisProjectOpener.GetNextStrValue(var AStr: string; const ADelim: String): String;
var
  j: Integer;
begin
   if (Length(AStr) > 0) and (AStr[1] = '"') then
   begin
     Delete(AStr, 1, 1);
     j := Pos('"', AStr);
     if j = 0 then
       raise Exception.Create('Closing quote is not found');
     Result := Copy(AStr, 1, j - 1);
     Delete(AStr, 1, j);
     Exit;
   end;

   j := Pos(ADelim, AStr);
   if j = 0 then
   begin
     Result := AStr;
     AStr := '';
   end

   else
   begin
     Result := Copy(AStr, 1, j - 1);
     Delete(AStr, 1, j-1+Length(ADelim));
   end;
end;

procedure TisProjectOpener.ProcessCmd(const Cmd: String; const Params: array of String);
begin
  if AnsiSameText(Cmd, 'InstallPackage') then
    InstallPackage(Params)

  else if AnsiSameText(Cmd, 'UninstallPackage') then
    UnInstallPackage(Params)

  else if AnsiSameText(Cmd, 'InstallHelp') then
    InstallHelp(Params)

  else if AnsiSameText(Cmd, 'AddPath') then
    AddPath(Params)

  else if AnsiSameText(Cmd, 'AddLibPath') then
    AddLibPath(Params)

  else if AnsiSameText(Cmd, 'AddSearchPath') then
    AddSearchPath(Params)

  else if AnsiSameText(Cmd, 'CopyFile') then
    CopyFile(Params)

  else if AnsiSameText(Cmd, 'CopyFolder') then
    CopyFolder(Params)

  else if AnsiSameText(Cmd, 'CopyFolderContent') then
    CopyFolderContent(Params)

  else if AnsiSameText(Cmd, 'Unzip') then
    Unzip(Params)

  else if AnsiSameText(Cmd, 'Exit') then
    ExitScript(Params)

  else if AnsiSameText(Cmd, 'RunDelphi') then
    RunDelphi(Params)

  else if AnsiSameText(Cmd, 'Say') then
    Say(Params)

  else if AnsiSameText(Cmd, 'Compile') then
    Compile(Params)

  else if AnsiSameText(Cmd, 'CompileRes') then
    CompileRes(Params)

  else if AnsiSameText(Cmd, 'CompileAllRes') then
    CompileAllRes(Params)

  else if AnsiSameText(Cmd, 'SetVar') then
    SetVar(Params)

  else if AnsiSameText(Cmd, 'InputText') then
    InputText(Params)

  else if AnsiSameText(Cmd, 'If') then
    IfThen(Params)

  else if AnsiSameText(Cmd, 'Call') then
    CallScript(Params)

  else if AnsiSameText(Cmd, 'Exec') then
    Exec(Params)

  else if AnsiSameText(Cmd, 'MakeDir') then
    MakeDir(Params)

  else
    raise Exception.Create('Unknown command "' + Cmd + '"');
end;

procedure TisProjectOpener.InstallPackage(const Params: array of String);
var
  PkgFullName, PkgName, PkgDescr, s: String;
  L: TStringList;
  j: Integer;
begin
  UnInstallPackage(Params);

  PkgFullName := Params[0];
  PkgDescr := Params[1];

  if (Length(Params) > 2) and AnsiSameText(Params[2], '/C') then
  begin
    for j := 3 to High(Params) do
    begin
      s := Params[j];
      FixupGlobalVars(s);
      FixupPath(s);      
      if not FileExists(s) then
        exit;
    end;
  end;

  FixupGlobalVars(PkgFullName);
  FixupPath(PkgFullName);

  // Clean up Disabled Package
  L := TStringList.Create;
  try
    PkgName := ExtractFileName(PkgFullName);
    ReadKeyFieldNames('Software\Borland\Delphi\7.0\Disabled Packages', L);
    for j := L.Count - 1 downto 0 do
    begin
      s := ExtractFileName(L[j]);
      if AnsiSameText(s, PkgName) then
      begin
        CleanKeyFieldInRegistry('Software\Borland\Delphi\7.0\Disabled Packages', L[j]);
        L.Delete(j);
      end;
    end;
  finally
    FreeAndNil(L);
  end;

 // Register package
  if FileExists(PkgFullName) then
  begin
    if PkgDescr <> '' then
      PkgDescr := PkgDescr + ' ';
    PkgDescr := PkgDescr + PrifOpenProjectPackage;

    WriteToRegistry('Software\Borland\Delphi\7.0\Known Packages', PkgFullName, PkgDescr);
  end;
end;

procedure TisProjectOpener.FixupGlobalVars(var AStr: String);
var
  s: String;
  v: String;
begin
  s := AStr;

  while s <> '' do
  begin
    GetNextStrValue(s, '$');
    if s = '' then
      break;

    v := GetNextStrValue(s, '$');

    AStr := StringReplace(AStr, '$' + v + '$', FVariables.GetVar(v), [rfReplaceAll, rfIgnoreCase]);
  end;
end;

procedure TisProjectOpener.UnInstallPackage(const Params: array of String);
var
  PkgName, s: String;
  L: TStringList;
  j: Integer;
begin
  PkgName := ExtractFileName(Params[0]);

  L := TStringList.Create;
  try
    ReadKeyFieldNames('Software\Borland\Delphi\7.0\Known Packages', L);
    for j := L.Count - 1 downto 0 do
    begin
      s := ExtractFileName(L[j]);
      if AnsiSameText(s, PkgName) then
      begin
        CleanKeyFieldInRegistry('Software\Borland\Delphi\7.0\Known Packages', L[j]);
        L.Delete(j);
      end;
    end;
  finally
    FreeAndNil(L);
  end;
end;

procedure TisProjectOpener.AddPath(const Params: array of String);
var
  Path, s, s1: String;
begin
  Path := NormalizePath(Params[0]);
  FixupGlobalVars(Path);
  FixupPath(Path);

  s := FPathVar;
  while s <> '' do
  begin
    s1 := NormalizePath(GetNextStrValue(s, ';'));
    if AnsiSameText(s1, Path) then
      Exit;
  end;

  FPathVar := Path + ';' + FPathVar;

  SyncRegistry;
end;

procedure TisProjectOpener.AddLibPath(const Params: array of String);
var
  Path, s, s1: String;

  procedure AddPathToDCC32cfg;
  var
    F: Text;
  begin
    AssignFile(F, FDelphiDir + '\bin\dcc32.cfg');
    try
      Append(F);
      WriteLn(F, '-u"' + Path + '"');
      WriteLn(F, '-r"' + Path + '"');
    finally
      CloseFile(F);
    end;
  end;

begin
  Path := NormalizePath(Params[0]);
  FixupGlobalVars(Path);
  FixupPath(Path);

  s := FLibSearchPath;
  while s <> '' do
  begin
    s1 := NormalizePath(GetNextStrValue(s, ';'));
    if AnsiSameText(s1, Path) then
      Exit;
  end;

  FLibSearchPath := FLibSearchPath + ';' + Path;

  SyncRegistry;
  AddPathToDCC32cfg;
end;

procedure TisProjectOpener.CopyFile(const Params: array of String);
type
  TCopyMode = (umForce);
var
  FileName, Dest, s: String;
  Mode: set of TCopyMode;
begin
  FileName := Params[0];
  FixupGlobalVars(FileName);
  FixupPath(FileName);
  Dest := NormalizePath(Params[1]);
  FixupGlobalVars(Dest);
  FixupPath(Dest);

  Mode := [];
  if Length(Params) >= 3 then
  begin
    s := Params[2];
    if (Length(s) > 1) and (s[1] = '/') then
    begin
      case s[2] of
        'O': Mode := Mode + [umForce];
      else
        raise Exception.Create('Unknown parameter "' + s + '"');
      end;
    end;
  end;

  if not FileExists(FileName) then
    raise Exception.Create('File "' + FileName + '" is not found');

  if not (umForce in Mode) and FileExists(Dest + ExtractFileName(FileName)) then
    Exit;

  Dest := Dest + ExtractFileName(FileName);
  isBasicUtils.CopyFile(FileName, Dest, False);
end;

procedure TisProjectOpener.Unzip(const Params: array of String);
type
  TUnzipMode = (umOverride, umCheckTime);
var
  UnZipper: TAbUnZipper;
  FileName, Dest, s: String;
  Mode: set of TUnzipMode;

  function FileDateTime(const AFileName: String): TDateTime;
  var
    Data: TWin32FindData;
    H: THandle;
    ST: TSystemTime;
  begin
    H := FindFirstFile(PChar(AFileName), Data);
    if H <> INVALID_HANDLE_VALUE then begin
      try
        FileTimeToSystemTime(Data.ftLastWriteTime, ST);
        Result := SystemTimeToDateTime(ST);
      finally
        Windows.FindClose(H);
      end
    end
    else
      Result := 0;
  end;

begin
  FileName := Params[0];
  FixupGlobalVars(FileName);
  FixupPath(FileName);
  Dest := Params[1];
  FixupGlobalVars(Dest);
  FixupPath(Dest);
  Mode := [];

  if Length(Params) >= 3 then
  begin
    s := Params[2];
    if (Length(s) > 1) and (s[1] = '/') then
    begin
      case s[2] of
        'O': Mode := Mode + [umOverride];
        'T': Mode := Mode + [umCheckTime];
      else
        raise Exception.Create('Unknown parameter "' + s + '"');
      end;
    end;
  end;

  if DirectoryExists(Dest) then
     if not (umOverride in Mode) then
       if umCheckTime in Mode then
       begin
         if FileDateTime(Dest) > FileDateTime(FileName) then
           Exit
       end
       else
         Exit;

  ClearDir(Dest, True);
  ForceDirectories(Dest);

  UnZipper := TAbUnZipper.Create(nil);
  try
    UnZipper.BaseDirectory := Dest;
    UnZipper.FileName := FileName;
    UnZipper.ExtractOptions := [eoCreateDirs, eoRestorePath];
    UnZipper.ExtractFiles('*.*');
  finally
    UnZipper.Free;
  end;
end;

procedure TisProjectOpener.ExitScript(const Params: array of String);
begin
  Abort;
end;

function TisProjectOpener.ClearDir(const Path: string; Delete: Boolean): Boolean;
const
  FileNotFound = 18;
var
  FileInfo: TSearchRec;
  DosCode: Integer;
begin
  Result := DirectoryExists(Path);

  if not Result then
    Exit;

  DosCode := SysUtils.FindFirst(NormalizePath(Path) + '*.*', faAnyFile, FileInfo);
  try
    while DosCode = 0 do
    begin
      if (FileInfo.Name[1] <> '.') then
      begin
        if (FileInfo.Attr and faDirectory = faDirectory) then
          Result := ClearDir(NormalizePath(Path) + FileInfo.Name, True) and Result
        else
          Result := Sysutils.DeleteFile(NormalizePath(Path) + FileInfo.Name) and Result;
      end;
      DosCode := SysUtils.FindNext(FileInfo);
    end;
  finally
    Sysutils.FindClose(FileInfo);
  end;

  if Delete and Result and (DosCode = FileNotFound) and not ((Length(Path) = 2) and (Path[2] = ':')) then
  begin
    Result := RemoveDir(Path);
  end;
end;

procedure TisProjectOpener.AddSearchPath(const Params: array of String);
var
  Path, s, s1: String;
begin
  Path := NormalizePath(Params[0]);
  FixupGlobalVars(Path);
  FixupPath(Path);

  s := FLibBrowsePath;
  while s <> '' do
  begin
    s1 := NormalizePath(GetNextStrValue(s, ';'));
    if AnsiSameText(s1, Path) then
      Exit;
  end;

  FLibBrowsePath := FLibBrowsePath + ';' + Path;

  SyncRegistry;
end;

procedure TisProjectOpener.Compile(const Params: array of String);
var
  Comp: IBPGCompile;
  ProjectFile: String;
  CompOption: String;
  Version: String;
  PrevCurDir: String;
begin
  ProjectFile := Params[0];
  FixupGlobalVars(ProjectFile);
  FixupPath(ProjectFile);

  Version := '';
  CompOption := '';
  if Length(Params) > 1 then
  begin
    Version := Params[1];
    FixupGlobalVars(Version);
    if Length(Params) > 2 then
    begin
      CompOption := Params[2];
      FixupGlobalVars(CompOption);
    end;
  end;

  PrevCurDir := GetCurrentDir;
  try
    SetCurrentDir(ExtractFilePath(ProjectFile));
    Comp := TBPGCompile.Create(FDelphiDir, OnProgress);
    Comp.Build(ProjectFile, Version);
  finally
    SetCurrentDir(PrevCurDir);
  end;
end;

procedure TisProjectOpener.InstallHelp(const Params: array of String);
var
  HelpName: String;


  procedure RegHelpFile(const d7FileName, Line: String);
  var
    F: Text;
    s: String;
  begin
    AssignFile(F, FDelphiDir + 'Help\' + d7FileName);
    try
      Reset(F);
      while not Eof(F) do
      begin
        Readln(F, s);
        if AnsiSameText(s, Line) then
          Exit;
      end;

      Append(F);
      WriteLn(F, Line);

    finally
      CloseFile(F);
    end;
  end;


  procedure InstallFile(const HelpFile: String);
  var
    s, CntFile: String;
  begin
    CntFile := ChangeFileExt(HelpFile, '.cnt');
    if not FileExists(CntFile) then
      Exit;

    WriteToRegistry('\SOFTWARE\Microsoft\Windows\Help', ExtractFileName(HelpFile), ExtractFilePath(HelpFile), HKEY_LOCAL_MACHINE);
    WriteToRegistry('\SOFTWARE\Microsoft\Windows\Help', ExtractFileName(CntFile), ExtractFilePath(CntFile), HKEY_LOCAL_MACHINE);

    s := ':INDEX ' + ExtractFileName(ChangeFileExt(HelpFile, '')) + '=' + ExtractFileName(HelpFile);
    RegHelpFile('d7.ohi', s);
    RegHelpFile('d7.ohl', ':LINK ' + ExtractFileName(HelpFile));
  end;


  procedure InstallDir(const HelpDir: String);
  var
   Files: String;
  begin
    Files := FindFiles(HelpDir, '*.hlp');
    while Files <> '' do
      InstallFile(HelpDir + GetNextStrValue(Files, ','));
  end;

begin
  HelpName := Params[0];
  FixupGlobalVars(HelpName);
  FixupPath(HelpName);

  if DirectoryExists(HelpName) then
    InstallDir(NormalizePath(HelpName))

  else if FileExists(HelpName) then
    InstallFile(HelpName);
end;

function TisProjectOpener.FindFiles(const APath, APattern: String): String;
var
  s: String;

  procedure UsePattern(const Pattern: String);
  var
    FileInfo: TSearchRec;
    DosCode: Integer;
  begin
    DosCode := SysUtils.FindFirst(NormalizePath(APath) + Pattern, faAnyFile, FileInfo);
    try
      while DosCode = 0 do
      begin
        if (FileInfo.Name[1] <> '.') then
        begin
          if Result <> '' then
            Result := Result + ',';
          Result := Result + FileInfo.Name;
        end;
         DosCode := SysUtils.FindNext(FileInfo);
      end;
    finally
      Sysutils.FindClose(FileInfo);
    end;
  end;

begin
  Result := '';
  s := APattern;
  while s <> '' do
    UsePattern(GetNextStrValue(s, ';'));
end;

procedure TisProjectOpener.Say(const Params: array of String);
var
  s: String;
begin
  if Length(Params) > 0 then
  begin
    s := Params[0];
    FixupGlobalVars(s);
    WriteLnOutput(s)
  end
  else
    WriteLnOutput;
end;

procedure TisProjectOpener.OnProgress(const AProgressStatus: String; const Progress: Integer);
begin
  WriteLnOutput(AProgressStatus + '   Done: ' + IntToStr(Progress) + '%');
end;

procedure TisProjectOpener.InputText(const Params: array of String);
var
  s: String;
begin
  if Length(Params) > 1 then
  begin
    s := Params[1];
    FixupGlobalVars(s);
    WriteOutput(s);
  end;

  Readln(s);

  if Length(Params) > 0 then
    FVariables.SetVar(Params[0], s);
end;

constructor TisProjectOpener.Create;
begin
  FVariables := TisVariables.Create(TisVariable);
end;

destructor TisProjectOpener.Destroy;
begin
  FreeAndNil(FVariables);
  inherited;
end;


procedure TisProjectOpener.FixupPath(var AStr: String);
begin
  AStr := ExpandFileName(StringReplace(AStr, '\\', '\', [rfReplaceAll, rfIgnoreCase]));
end;

procedure TisProjectOpener.SetVar(const Params: array of String);
var
  s: String;
begin
  s := Params[1];
  FixupGlobalVars(s);
  FVariables.SetVar(Params[0], s);
end;

procedure TisProjectOpener.IfThen(const Params: array of String);
var
  LeftExpr: String;
  RightExpr: String;
  Operation, Cmd: String;
  CmdParams: array of String;
  pi, i: Integer;
  CompareRes: Boolean;
  NotFlag: Boolean;
begin
  pi := 0;

  LeftExpr := Params[pi];

  if AnsiSameText(LeftExpr, 'NOT') then
  begin
    NotFlag := True;
    Inc(pi);
    LeftExpr := Params[pi];    
  end
  else
    NotFlag := False;
  Inc(pi);

  Operation := Params[pi];
  Inc(pi);
  RightExpr := Params[pi];
  Inc(pi);

  FixupGlobalVars(LeftExpr);
  FixupGlobalVars(RightExpr);

  if Operation = '==' then
    CompareRes := LeftExpr = RightExpr
  else if Operation = '!=' then
    CompareRes := LeftExpr <> RightExpr
  else if Operation = '>' then
    CompareRes := StrToIntDef(LeftExpr, 0) > StrToIntDef(RightExpr, 0)
  else if Operation = '>=' then
    CompareRes := StrToIntDef(LeftExpr, 0) >= StrToIntDef(RightExpr, 0)
  else if Operation = '<' then
    CompareRes := StrToIntDef(LeftExpr, 0) < StrToIntDef(RightExpr, 0)
  else if Operation = '<=' then
    CompareRes := StrToIntDef(LeftExpr, 0) <= StrToIntDef(RightExpr, 0)
  else
    raise Exception.Create('Unknown comparison operation ' + Operation);

  if NotFlag then
    CompareRes := not CompareRes; 

  if CompareRes then
  begin
    Cmd := Params[pi];
    Inc(pi);
    SetLength(CmdParams, Length(Params) - pi);
    for i := Low(Params) + pi to High(Params) do
      CmdParams[i - pi] := Params[i];

    ProcessCmd(Cmd, CmdParams);
  end;  
end;

procedure TisProjectOpener.CallScript(const AParams: array of String);
var
  Opener: TisProjectOpener;
  ScriptFile: String;
  Params: array of String;
  i: Integer;
begin
  Opener := TisProjectOpener.Create;
  try
    ScriptFile := AParams[0];
    FixupGlobalVars(ScriptFile);
    FixupPath(ScriptFile);
    Opener.FVerboseMode := FVerboseMode;
    Opener.FDelphiDir := FDelphiDir;
    Opener.FDelphiExe := FDelphiExe;
    Opener.FPathVar := FPathVar;
    Opener.FLibSearchPath := FLibSearchPath;
    Opener.FLibBrowsePath := FLibBrowsePath;

    SetLength(Params, Length(AParams) - 1);
    for i := 1 to High(AParams) do
    begin
      Params[i - 1] := AParams[i];
      FixupGlobalVars(Params[i - 1]);
    end;

    Opener.RunScript(ScriptFile, Params);

    FPathVar := Opener.FPathVar;
    FLibSearchPath := Opener.FLibSearchPath;
    FLibBrowsePath := Opener.FLibBrowsePath;

    SyncRegistry;
  finally
    Opener.Free;
  end;
end;

procedure TisProjectOpener.DoFirstEntryInit;

  procedure InitDCC32cfg;
  var
    F: Text;
  begin
    AssignFile(F, FDelphiDir + '\bin\dcc32.cfg');
    try
      Rewrite(F);
      Writeln(F, '-aWinTypes=Windows;WinProcs=Windows;DbiProcs=BDE;DbiTypes=BDE;DbiErrs=BDE');
      Writeln(F, '-u"' + FDelphiDir + 'lib";"' + FDelphiDir + 'lib\Obj"');
      Writeln(F, '-r"' + FDelphiDir + 'lib";"' + FDelphiDir + 'lib\Obj"');
    finally
      CloseFile(F);
    end;
  end;


  procedure UnregisterOpenProjectPackages;
  var
    L: TStringList;
    j: Integer;
    PackageDescr: String;
  begin
    L := TStringList.Create;
    try
      ReadKeyFieldNames('Software\Borland\Delphi\7.0\Known Packages', L);
      for j := 0 to L.Count - 1 do
      begin
        PackageDescr := ReadFromRegistry('Software\Borland\Delphi\7.0\Known Packages', L[j]);
        if Pos(PrifOpenProjectPackage, PackageDescr) > 0 then
          CleanKeyFieldInRegistry('Software\Borland\Delphi\7.0\Known Packages', L[j]);
      end;
    finally
      FreeAndNil(L);
    end;
  end;

begin
  FDelphiDir := NormalizePath(ReadFromRegistry('Software\Borland\Delphi\7.0', 'RootDir'));
  FDelphiExe := ReadFromRegistry('Software\Borland\Delphi\7.0', 'App');

  FPathVar := GetEnvironmentVariable('Path');

  FLibSearchPath := '$(DELPHI)\Lib;$(DELPHI)\Bin;$(DELPHI)\Imports;$(DELPHI)\Projects\Bpl';

  FLibBrowsePath := '$(DELPHI)\source\vcl;$(DELPHI)\Source\Rtl;$(DELPHI)\source\rtl\Sys;'+
                    '$(DELPHI)\source\rtl\Win;$(DELPHI)\source\rtl\common;$(DELPHI)\source\Internet;'+
                    '$(DELPHI)\source\clx;$(DELPHI)\source\websnap;$(DELPHI)\Source\WebMidas;'+
                    '$(DELPHI)\Source\Samples;$(DELPHI)\Source\Soap;$(DELPHI)\Source\ToolsAPI;$(DELPHI)\Source\Xml';

  InitDCC32cfg;
  UnregisterOpenProjectPackages;
end;

procedure TisProjectOpener.Exec(const Params: array of String);
var
  s, Cmd, CmdParams: String;
  i: Integer;
begin
 if (Length(Params) >= 1) then
 begin
   Cmd := Params[0];
   FixupGlobalVars(Cmd);
   FixupPath(Cmd);
 end;

 CmdParams := '';
 for i := Low(Params) + 1 to High(Params) do
 begin
  s := Params[i];
  FixupGlobalVars(s);
  if Contains(s, ' ') then
    s := '"' + s + '"';
  CmdParams := CmdParams + ' ' + s;
 end;

 RunProcess(Cmd, CmdParams, [roHideWindow, roWaitForClose]);
end;

procedure TisProjectOpener.WriteOutput(const AText: String);
begin
  Write(Output, AText);
end;

procedure TisProjectOpener.WriteLnOutput(const AText: String);
begin
  Writeln(Output, AText);
  Flush(Output);  
end;

procedure TisProjectOpener.MakeDir(const Params: array of String);
var
  s: String;
begin
  if Length(Params) > 0 then
  begin
    s := Params[0];
    FixupGlobalVars(s);
    ForceDirectories(s);
  end
end;

procedure TisProjectOpener.CompileRes(const Params: array of String);
var
  Comp: IBPGCompile;
  RCFile: String;
  PrevCurDir: String;
begin
  RCFile := Params[0];
  FixupGlobalVars(RCFile);
  FixupPath(RCFile);

  PrevCurDir := GetCurrentDir;
  try
    SetCurrentDir(ExtractFilePath(RCFile));
    Comp := TBPGCompile.Create(FDelphiDir, OnProgress);
    Comp.CompileResources(RCFile);
  finally
    SetCurrentDir(PrevCurDir);
  end;
end;

procedure TisProjectOpener.CompileAllRes(const Params: array of String);
var
  Comp: IBPGCompile;
  Folder: String;
  PrevCurDir: String;

  procedure ProcessFolder(const AFolder: string);
  var
    L: IisStringList;
    i: Integer;
  begin
    SetCurrentDir(AFolder);

    L := GetFilesByMask(AFolder + '*.rc');
    for i := 0 to L.Count - 1 do
      Comp.CompileResources(L[i]);

    L := GetSubDirs(AFolder);
    for i := 0 to L.Count - 1 do
      ProcessFolder(NormalizePath(L[i]));
  end;

begin
  Folder := Params[0];
  FixupGlobalVars(Folder);
  FixupPath(Folder);

  PrevCurDir := GetCurrentDir;
  try
    Comp := TBPGCompile.Create(FDelphiDir, OnProgress);
    ProcessFolder(NormalizePath(Folder));
  finally
    SetCurrentDir(PrevCurDir);
  end;
end;

procedure TisProjectOpener.SyncRegistry;
begin
  WriteToRegistry('\Software\Borland\Delphi\7.0\Environment Variables', 'Path', FPathVar);
  WriteToRegistry('\Software\Borland\Delphi\7.0\Library', 'Search Path', FLibSearchPath);
  WriteToRegistry('\Software\Borland\Delphi\7.0\Library', 'Browsing Path', FLibBrowsePath);
end;

procedure TisProjectOpener.CopyFolder(const Params: array of String);
type
  TCopyMode = (umForce);
var
  FolderName, Dest, s: String;
  Mode: set of TCopyMode;
begin
  FolderName := Params[0];
  FixupGlobalVars(FolderName);
  FixupPath(FolderName);
  Dest := NormalizePath(Params[1]);
  FixupGlobalVars(Dest);
  FixupPath(Dest);

  Mode := [];
  if Length(Params) >= 3 then
  begin
    s := Params[2];
    if (Length(s) > 1) and (s[1] = '/') then
    begin
      case s[2] of
        'O': Mode := Mode + [umForce];
      else
        raise Exception.Create('Unknown parameter "' + s + '"');
      end;
    end;
  end;

  if not DirectoryExists(FolderName) then
    raise Exception.Create('Folder "' + FolderName + '" is not found');

  if DirectoryExists(Dest + ExtractFileName(FolderName)) then
    if umForce in Mode then
      ClearDir(Dest + ExtractFileName(FolderName), True)    
    else
      Exit;

  isBasicUtils.CopyFolder(FolderName, Dest);
end;

procedure TisProjectOpener.CopyFolderContent(const Params: array of String);
var
  FolderName, Dest: String;
begin
  FolderName := Params[0];
  FixupGlobalVars(FolderName);
  FixupPath(FolderName);
  Dest := NormalizePath(Params[1]);
  FixupGlobalVars(Dest);
  FixupPath(Dest);

  if not DirectoryExists(FolderName) then
    raise Exception.Create('Folder "' + FolderName + '" is not found');

  isBasicUtils.CopyFolderContent(FolderName, Dest);
end;

{ TisVariables }

function TisVariables.FindVar(const AVarName: String): TisVariable;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameText(TisVariable(Items[i]).Name, AVarName) then
    begin
      Result := TisVariable(Items[i]);
      break;
    end;
end;

function TisVariables.GetVar(const AVarName: String): Variant;
var
  V: TisVariable;
begin
  V := FindVar(AVarName);
  if Assigned(V) then
    Result := V.Value
  else if StartsWith(AVarName, 'Param') then
    Result := ''
  else
    raise Exception.Create('Variable "' + AVarName + '" is not found');
end;

procedure TisVariables.SetVar(const AVarName: String; const AValue: Variant);
var
  V: TisVariable;
begin
  V := FindVar(AVarName);
  if not Assigned(V) then
  begin
    V := TisVariable(Add);
    V.FName := AVarName;
  end;

  V.Value := AValue;
end;

end.
