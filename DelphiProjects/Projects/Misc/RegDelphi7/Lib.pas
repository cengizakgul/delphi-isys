unit Lib;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses Windows, SysUtils, Registry;

function  GetMyProfileFolder: string;
function  ReadFromRegistry(const Key, KeyField: string; Root: DWORD = HKEY_CURRENT_USER): string;
procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; Root: DWORD = HKEY_CURRENT_USER; ExpandStr: Boolean = False);
function  GetNextStrValue(var AStr: string; const ADelim: String): String;
function  NormalDir(const DirName: string): string;


// Win API functions are missing in Delphi
{$EXTERNALSYM SHGetFolderPath}
function SHGetFolderPath(hwndOwner: HWND; nFolder: Integer; hToken: THandle;
                         dwFlags: DWORD; pszPath: LPTSTR): HRESULT; stdcall;
function SHGetFolderPath; external 'shell32.dll' name 'SHGetFolderPathA';


implementation

function GetMyProfileFolder: string;
const
  CSIDL_PROFILE = $0028;
var
  pPath: PAnsiChar;
begin
  GetMem(pPath, MAX_PATH);
  try
    Win32Check(SHGetFolderPath(0, CSIDL_PROFILE, 0, 0, pPath) = S_OK);
    Result := pPath;
    Result := NormalDir(Result);
  finally
    FreeMem(pPath);
  end;
end;


function ReadFromRegistry(const Key, KeyField: string; Root: DWORD = HKEY_CURRENT_USER): string;
var
  TempRegistry: TRegistry;
begin
  Result := '';

  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := Root;
      if OpenKeyReadOnly(Key) and ValueExists(KeyField) then
        Result := ReadString(KeyField);
    end;
  finally
    TempRegistry.Free;
  end;
end;


procedure WriteToRegistry(const Key, KeyField: string; NewValue: string; Root: DWORD = HKEY_CURRENT_USER; ExpandStr: Boolean = False);
var
  TempRegistry: TRegistry;
begin
  TempRegistry := TRegistry.Create;
  try
    with TempRegistry do
    begin
      RootKey := Root;
      if OpenKey(Key, NewValue <> '') then
        if NewValue = '' then
          DeleteValue(KeyField)
        else
          if ExpandStr then
            WriteExpandString(KeyField, NewValue)
          else
            WriteString(KeyField, NewValue);
    end
  finally
    TempRegistry.Free;
  end;
end;


function  GetNextStrValue(var AStr: string; const ADelim: String): String;
var
  j: Integer;
begin
   if (Length(AStr) > 0) and (AStr[1] = '"') then
   begin
     Delete(AStr, 1, 1);
     j := Pos('"', AStr);
     if j = 0 then
       raise Exception.Create('Closing quote is not found');
     Result := Copy(AStr, 1, j - 1);
     Delete(AStr, 1, j);
     Exit;
   end;

   j := Pos(ADelim, AStr);
   if j = 0 then
   begin
     Result := AStr;
     AStr := '';
   end

   else
   begin
     Result := Copy(AStr, 1, j - 1);
     Delete(AStr, 1, j-1+Length(ADelim));
   end;
end;


function NormalDir(const DirName: string): string;
begin
  Result := DirName;
  if (Result <> '') and not (AnsiLastChar(Result)^ in [':', '\']) then
  begin
    if (Length(Result) = 1) and (UpCase(Result[1]) in ['A'..'Z']) then
      Result := Result + ':\'
    else
      Result := Result + PathDelim;
  end;
end;


end.
 