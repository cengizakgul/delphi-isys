program RegDelphi7;

{$APPTYPE CONSOLE}
{$R ..\RegDelphi7\DelphiRegister.RES}

uses
  Windows,
  Classes,
  SysUtils,
  Lib in '..\RegDelphi7\Lib.pas';

procedure RegDelphi;
var
  PathVar, DelphiPath: String;
  QuietMode: Boolean;

  procedure AddPath(const APath: STring);
  var
    s, s1: String;
  begin
    s := PathVar;
    while s <> '' do
    begin
      s1 := NormalDir(GetNextStrValue(s, ';'));
      if AnsiSameText(s1, APath) then
        Exit;
    end;

    PathVar := APath + ';' + PathVar;
  end;

  procedure ApplyRegistrySettings;
  var
    RS: TResourceStream;
    F: TFileStream;
    Res, s: String;
    SI: STARTUPINFO;
    PI: PROCESS_INFORMATION;
    CmdLineStr: String;
    TempRegFile: String;
  begin
    // prepare reg info
    RS := TResourceStream.Create(HInstance, 'delphi_reg', 'FILE');
    try
      SetLength(Res, RS.Size);
      RS.ReadBuffer(Res[1], RS.Size);
    finally
      RS.Free;
    end;

    TempRegFile := NormalDir(GetEnvironmentVariable('Temp')) + 'delphi.reg';
    s := Copy(DelphiPath, 1, Length(DelphiPath) - 1);
    s := StringReplace(s, '\', '\\', [rfReplaceAll]);
    Res := StringReplace(Res, '$MACRO_DELPHI_ROOT$', s, [rfReplaceAll]);
    F := TFileStream.Create(TempRegFile, fmCreate);
    try
      F.WriteBuffer(Res[1], Length(Res));
    finally
      F.Free;
    end;

    // run reg
    ZeroMemory(@SI, SizeOf(SI));
    ZeroMemory(@PI, SizeOf(PI));
    CmdLineStr := 'reg import ' + '"' + TempRegFile + '"';
    if CreateProcess(nil, PChar(CmdLineStr), nil, nil, False, 0, nil, PAnsiChar(ExtractFilePath(TempRegFile)), SI, PI) then
    begin
      WaitForSingleObject(PI.hProcess, INFINITE);
      CloseHandle(PI.hThread);
      CloseHandle(PI.hProcess);
      DeleteFile(TempRegFile);
    end

    else
      raise  Exception.Create('Delphi registry settings have not been applied');
  end;


  procedure RegisterDebugger;
  var
    SI: STARTUPINFO;
    PI: PROCESS_INFORMATION;
    CmdLineStr: String;
  begin
    ZeroMemory(@SI, SizeOf(SI));
    ZeroMemory(@PI, SizeOf(PI));
    CmdLineStr := DelphiPath + 'BIN\tregsvr.exe -q "' + DelphiPath + 'Debugger\BORdbk70.dll"';
    if CreateProcess(nil, PChar(CmdLineStr), nil, nil, False, 0, nil, PAnsiChar(DelphiPath), SI, PI) then
    begin
      WaitForSingleObject(PI.hProcess, INFINITE);
      CloseHandle(PI.hThread);
      CloseHandle(PI.hProcess);
    end
  end;


  procedure BackupPrevSettings;
  var
    SI: STARTUPINFO;
    PI: PROCESS_INFORMATION;
    CmdLineStr: String;
    TempRegFile: String;
    BkpFolder: String;
    BkpCmd: Text;
  begin
    BkpFolder := ReadFromRegistry('SOFTWARE\Borland\Delphi\7.0', 'RootDir');
    if (BkpFolder <> '') and not AnsiSameText(NormalDir(BkpFolder), DelphiPath) and
       (QuietMode or
       (MessageBox(0, 'Another Delphi 7 registration is detected. Do you want to make a backup?', PAnsiChar(ExtractFileName(ParamStr(0))), MB_YESNO or MB_ICONQUESTION or MB_APPLMODAL) = IDYES))
        then
    begin
      Writeln('Creating a backup...');

      BkpFolder := NormalDir(BkpFolder) + 'Backup';
      ForceDirectories(BkpFolder);
      AssignFile(BkpCmd, BkpFolder + '\restore.cmd');
      try
        Rewrite(BkpCmd);
        Writeln(BkpCmd, '; Run this file if you need to restoring Delphi settings under current user');
        Writeln(BkpCmd, 'CD ' + BkpFolder);

        // Register Settings
        ZeroMemory(@SI, SizeOf(SI));
        ZeroMemory(@PI, SizeOf(PI));
        TempRegFile := BkpFolder + '\DelphiCurrentUser.reg';
        CmdLineStr := 'reg export HKCU\Software\Borland\Delphi\7.0 ' + '"' + TempRegFile + '"';
        if CreateProcess(nil, PChar(CmdLineStr), nil, nil, False, 0, nil, PAnsiChar(ExtractFilePath(TempRegFile)), SI, PI) then
        begin
          WaitForSingleObject(PI.hProcess, INFINITE);
          CloseHandle(PI.hThread);
          CloseHandle(PI.hProcess);
        end
        else
          raise  Exception.Create('Backup has failed!');
        Writeln(BkpCmd, 'reg delete HKCU\Software\Borland\Delphi\7.0 /f');
        Writeln(BkpCmd, 'reg import DelphiCurrentUser.reg');
      finally
        CloseFile(BkpCmd);
        Writeln('Done');
      end;

      if not QuietMode then
        MessageBox(0, PAnsiChar('Backup files have been saved to "' + BkpFolder + '"'), PAnsiChar(ExtractFileName(ParamStr(0))), MB_OK or MB_ICONINFORMATION or MB_APPLMODAL)
    end;
  end;

  procedure AddProductRegistration;
  var
    s: String;
    RS: TResourceStream;
  begin
    s := GetMyProfileFolder + '.borland\';
    ForceDirectories(s);

    RS := TResourceStream.Create(HInstance, 'registry_dat', 'FILE');
    try
      RS.SaveToFile(s + 'registry.dat');
    finally
      RS.Free;
    end;

    RS := TResourceStream.Create(HInstance, 'registry_slm', 'FILE');
    try
      RS.SaveToFile(s + 'registry.slm');
    finally
      RS.Free;
    end;
  end;

begin
  if ParamCount > 0 then
    QuietMode := ParamStr(1) = '/Q'
  else
    QuietMode := False;

  PathVar := ReadFromRegistry('Environment', 'Path');
  DelphiPath := NormalDir(ExtractFilePath(ParamStr(0)));

  if not FileExists(DelphiPath + 'Bin\delphi32.exe') then
    raise  Exception.Create('This utility has to reside in the Delphi 7 root folder');

  if QuietMode or
     (MessageBox(0, 'This utility will register Delphi 7 for current user. Continue?',
        PAnsiChar(ExtractFileName(ParamStr(0))), MB_YESNO or MB_ICONQUESTION or MB_APPLMODAL) = IDYES) then
  begin
    BackupPrevSettings;

    Writeln('Registring Delphi...');

    AddProductRegistration;
    AddPath(DelphiPath + 'Bin\');
    AddPath(DelphiPath + 'Projects\Bpl\');
    WriteToRegistry('Environment', 'Path', PathVar, HKEY_CURRENT_USER, True);

    ApplyRegistrySettings;
    RegisterDebugger;

    Writeln('Done');

    if not QuietMode then
      MessageBox(0, 'Delphi 7 has been registered successfully!', PAnsiChar(ExtractFileName(ParamStr(0))), MB_OK or MB_ICONINFORMATION or MB_APPLMODAL);
  end;
end;


begin
  try
    RegDelphi;
  except
    on E: Exception do
      MessageBox(0, PAnsiChar(E.Message), PAnsiChar(ExtractFileName(ParamStr(0))), MB_OK or MB_ICONERROR or MB_APPLMODAL);
  end;
end.
