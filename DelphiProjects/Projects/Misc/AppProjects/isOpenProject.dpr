program isOpenProject;

{$APPTYPE CONSOLE}

{$R *.RES}

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManager.inc}
  SysUtils,
  Main in '..\IsOpenProject\Main.pas',
  DelphiMake in '..\IsOpenProject\DelphiMake.pas';

begin
  try
    Run;
  except
    on E: Exception do
    begin
      Writeln(ErrOutput, E.Message);
      ExitCode := 1;
    end;
  end;
end.
