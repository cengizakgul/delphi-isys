program isExeProtector;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  SysUtils,
  Windows,
  ogproexe,
  SEncryptionRoutines,
  isBasicUtils;


function FileSupportProtection(const AFileName: String): Boolean;
var
  n, VerSize: Cardinal;
  VerInfo: Pointer;
  VerData: Pointer;
  VerDataLen: Cardinal;
  s: String;
begin
  Result := False;
  try
    VerSize := GetFileVersionInfoSize(PAnsiChar(AFileName), n);
    if VerSize <> 0 then
    begin
      VerInfo := AllocMem(VerSize);
      try
        GetFileVersionInfo(PAnsiChar(AFileName), 0, VerSize, VerInfo);
        VerQueryValue(VerInfo, '\StringFileInfo\040904E4\CompanyName', VerData, VerDataLen);

        if VerDataLen = 0 then
          Exit;

        SetLength(s, VerDataLen - 1);
        StrLCopy(PChar(@s[1]), PChar(VerData),  VerDataLen - 1);

        if Pos('ISYSTEMS', AnsiUpperCase(s)) = 0 then
          Exit;

        GetFileVersionInfo(PAnsiChar(AFileName), 0, VerSize, VerInfo);
        VerQueryValue(VerInfo, '\StringFileInfo\040904E4\Protected', VerData, VerDataLen);

        if VerDataLen = 0 then
          Exit;

        SetLength(s, VerDataLen - 1);
        StrLCopy(PChar(@s[1]), PChar(VerData),  VerDataLen - 1);

        if s <> '1' then
          Exit;

        Result := True;
      finally
        FreeMem(VerInfo);
      end;
    end;
  except
  end;
end;


function StrToCodes(const aStr: String): String;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(aStr) do
    Result := Result + '#' + IntToStr(Ord(aStr[i]));
end;

function CodesToStr(aStr: String): String;
var
  i: Integer;
  s: String;
begin
  Result := '';
  for i := 1 to Length(aStr) do
  begin
    s := GetNextStrValue(aStr, '#');
    if s <> '' then
      Result := Result + Chr(StrToInt(s));
  end;
end;



procedure Run(const aCmd, aParam: String);
begin
  if aCmd = 'P' then
  begin
    if not FileSupportProtection(aParam) then
    begin
      Writeln('This file does not support tampering protection');
      Exit;
    end;
    ProtectExe(aParam, False);
    Writeln('Tampering protection code has been added');
  end

  else if aCmd = 'U' then
  begin
    if not FileSupportProtection(aParam) then
    begin
      ExitCode := 2;
      raise Exception.Create('This file does not support tampering protection');
    end;
    UnprotectExe(aParam);
    Writeln('Tampering protection code has been removed');
  end

  else if aCmd = 'E' then
    Writeln(StrToCodes(EncryptString(aParam)))

  else if aCmd = 'D' then
    Writeln(DecryptString(CodesToStr(aParam)))

  else
    raise Exception.CreateFmt('Command "%s" is not valid', [aCmd]);
end;

begin
  try
    if ParamCount <> 2 then
      raise Exception.Create('*** EXE/DLL tampering protection utility ***'#13#10 +
                             ''#13#10 +
                             'USAGE:     isExeProtector.exe <command> <param>'#13#10 +
                             'Commands: '#13#10 +
                             '     p <file name>     Protect EXE file'#13#10 +
                             '     u <file name>     Unprotect EXE file'#13#10 +
                             '     e <string>        Encrypt string'#13#10 +
                             '     d <string>        Dencrypt string');

    Run(AnsiUpperCase(ParamStr(1)), ParamStr(2))
  except
    on E: Exception do
    begin
      if ExitCode = 0 then
        ExitCode := 1;
      Writeln(ErrOutput, E.Message);
    end;
  end;
end.
