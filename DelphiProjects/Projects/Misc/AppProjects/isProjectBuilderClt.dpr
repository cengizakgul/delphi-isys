program isProjectBuilderClt;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  pbClasses,
  pbCltMainboard;

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Project Builder';
  
  ActivateMainboard;
  try
    Mainboard.Initialize;
    try
      Application.Run;
    finally
      Mainboard.Finalize;
    end;
  finally
    DeactivateMainboard;
  end;
end.
