program isLogViewer;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  main in '..\isLogViewer\main.pas' {MainForm},
  FindDialog in '..\isLogViewer\FindDialog.pas' {FindDlg},
  FilterDialog in '..\isLogViewer\FilterDialog.pas' {FilterDlg};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFindDlg, FindDlg);
  Application.CreateForm(TFilterDlg, FilterDlg);
  Application.Run;
end.
