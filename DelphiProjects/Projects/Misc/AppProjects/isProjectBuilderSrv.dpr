program isProjectBuilderSrv;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  pbSrvMainboard,
  isAppIDs,
  isServerServiceFrm;

{$R *.res}
{$R ..\IsProjectBuilder\Server\pbServer.res}

procedure _Init;
begin
  ActivateMainboard;
  Mainboard.Initialize;
end;

procedure _Deinit;
begin
  Mainboard.Finalize;
  DeactivateMainboard;
end;

begin
  RunISServer(ProjectBuilderAppInfo.Name, 'Project Builder', ProjectBuilderAppInfo.AppID, @_Init, @_Deinit, nil);
end.
