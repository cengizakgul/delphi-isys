unit isDesignTimeClasses;

interface

uses
  ISBasicClasses;

  procedure Register;

implementation

procedure Register;                                                               
begin
  ISBasicClasses.Register;
end;

end.
