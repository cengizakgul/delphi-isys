unit MainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame,
  EvolutionPrPrBatchFrame, FileOpenFrame, CustomBinderBaseFrame,
  FieldFrame, mjd, extappdecl, timeclockimport,
  ExtAppOptionsFrame, mjdparser, FileOpenLeftBtnFrame, OptionsBaseFrame;

type
  TMainFm = class(TEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    pnlBottom: TPanel;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshEdMap: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    pnlPayrollBatch: TPanel;
    PayrollBatchFrame: TEvolutionPrPrBatchFrm;
    BitBtn1: TBitBtn;
    FieldFrame: TFieldFrm;
    Bevel1: TBevel;
    pnlSetups: TPanel;
    ExtAppOptionsFrame: TExtAppOptionsFrm;
    Bevel3: TBevel;
    pnlTop: TPanel;
    pnlSpacer: TPanel;
    FileOpenFrame: TFileOpenLeftBtnFrm;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
  private
    FEvoData: TEvoData;
    FExtData: TMJDFile;

    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    function GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);
    procedure HandleFileSelected(filename: string);
    procedure HandleFieldMappingChanged;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}
uses
  isSettings, EeUtilityLoggerViewFrame, dialogs,
  userActionHelpers, gdyBinder, variants, RateImportOptionFrame;


procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  FEvoData.Connect(EvoFrame.Param);
  ConnectToEvo.Caption := 'Refresh Evolution data';
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEeUtilityLoggerViewFrm;
  inherited;
  FEvoData := TEvoData.Create(Logger);
  FEvoData.AddDS(TMP_PRDesc);
  FEvoData.AddDS(PR_BATCHDesc);
  FEvoData.AddDS(CL_E_DSDesc);
  FEvoData.AddDS(CUSTOM_DBDTDesc);

  CompanyFrame.Init(FEvoData);
  PayrollBatchFrame.Init(FEvoData);
  FileOpenFrame.OnFileSelected := HandleFileSelected;
  FieldFrame.OnMatchTableChanged := HandleFieldMappingChanged;
  FEvoData.Advise(HandleCompanyChanged);

  //designer crashed repeatedly when I tried to do this by editing the form
  FileOpenFrame.FileOpen1.Dialog.Filter := 'MJD files (*.csv)|*.csv';
  FileOpenFrame.FileOpen1.Hint := 'Open|Opens MJD file';

  if EvoFrame.IsValid then
    PageControl1.TabIndex := 2
  else
    PageControl1.TabIndex := 0;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
{
  try
    ExtAppOptionsFrame.Options := LoadExtAppOptions( FSettings, CompanyUniquePath(Value) + '\MJDOptions');
  except
    Logger.StopException;
  end;}
  if FExtData.SourceFileName <> '' then
    FieldFrame.Init( FSettings.AsString[ClientUniquePath(Value)+'\FieldMapping\DataPacket'], GetEvoDBDTInfo(Logger, FEvoData), FEvoData.DS[CL_E_DSDesc.Name], FExtData.Header );
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  Logger.LogEntry('Current company changed');
  try
    try
      Logger.LogContextItem('Company selected', BoolToStr(new.HasValue, true));
      if new.HasValue then
      begin
        LogEvoCompanyDef(Logger, new.Value);
        LoadPerCompanySettings(new.Value)
      end
      else
        FieldFrame.UnInit;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetPrBatch and FileOpenFrame.IsValid and FieldFrame.CanCreateMatcher;
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
var
  opt: TExtAppOptions;
  FieldMatcher: IMatcher;
begin
  FieldMatcher := FieldFrame.Matcher;
  if not VarIsNull(FieldMatcher.LeftMatch(VarArrayOf([evoFieldTypeRate, '']))) then
    opt.RateImport := rateUseExternal
  else
    opt.RateImport := rateUseEvo;
  opt.Summarize := false;
  Result := mjd.GetTimeClockData(Logger, FExtData, opt{ExtAppOptionsFrame.Options}, FieldMatcher, FEvoData.GetClCo.CUSTOM_COMPANY_NUMBER);
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      Assert(FileOpenFrame.IsValid);
      RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData, FEvoData.Connection, false);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleFileSelected(filename: string);
begin
  FExtData := LoadMJDFile(Logger, filename);
  FieldFrame.Uninit;
  if FEvoData.CanGetClCo then
    FieldFrame.Init( FSettings.AsString[ClientUniquePath(FEvoData.GetClCo)+'\FieldMapping\DataPacket'], GetEvoDBDTInfo(Logger, FEvoData), FEvoData.DS[CL_E_DSDesc.Name], FExtData.Header );
end;

{
  try
    SaveExtAppOptions( ExtAppOptionsFrame.Options, FSettings, CompanyUniquePath(oldValue) + '\MJDOptions');
  except
    Logger.StopException;
  end;
 }
procedure TMainFm.HandleFieldMappingChanged;
begin
  try
    FSettings.AsString[ClientUniquePath(FEvoData.GetClCo) + '\FieldMapping\DataPacket'] := FieldFrame.SaveToString;
  except
    Logger.StopException;
  end;
end;

end.
