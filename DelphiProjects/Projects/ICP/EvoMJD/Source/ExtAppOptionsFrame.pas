unit ExtAppOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, extappdecl;

type
  TExtAppOptionsFrm = class(TFrame)
    cbSummarize: TCheckBox;
  private
    function GetOptions: TExtAppOptions;
    procedure SetOptions(const Value: TExtAppOptions);
  public
    property Options: TExtAppOptions read GetOptions write SetOptions;
  end;

implementation

{$R *.dfm}

{ TExtAppOptionsFrm }

function TExtAppOptionsFrm.GetOptions: TExtAppOptions;
begin
  Result.Summarize := cbSummarize.Checked;
end;

procedure TExtAppOptionsFrm.SetOptions(const Value: TExtAppOptions);
begin
  cbSummarize.Checked := Value.Summarize;
end;

end.
