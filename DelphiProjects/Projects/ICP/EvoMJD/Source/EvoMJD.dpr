program EvoMJD;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm},
  mjd in 'mjd.pas',
  FieldFrame in 'FieldFrame.pas' {FieldFrm: TFrame},
  ExtAppOptionsFrame in 'ExtAppOptionsFrame.pas' {ExtAppOptionsFrm: TFrame},
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  extappdecl in 'extappdecl.pas',
  mjdparser in 'mjdparser.pas',
  MidasLib,
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  FileOpenLeftBtnFrame in '..\..\common\FileOpenLeftBtnFrame.pas' {FileOpenLeftBtnFrm: TFrame};

{$R *.res}

begin
  LicenseKey := '7BD84037E877497282AAE249214E5889'; //swipeclock's

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution MJD Time Import';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
