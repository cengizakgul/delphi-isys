inherited FieldFrm: TFieldFrm
  Width = 598
  Height = 513
  inherited BinderFrm1: TBinderFrm
    Width = 598
    Height = 513
    inherited evSplitter2: TSplitter
      Top = 249
      Width = 598
    end
    inherited pnltop: TPanel
      Width = 598
      Height = 249
      inherited evSplitter1: TSplitter
        Height = 249
      end
      inherited pnlTopLeft: TPanel
        Height = 249
        inherited dgLeft: TReDBGrid
          Height = 224
        end
      end
      inherited pnlTopRight: TPanel
        Width = 328
        Height = 249
        inherited evPanel4: TPanel
          Width = 328
        end
        inherited dgRight: TReDBGrid
          Width = 328
          Height = 224
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 254
      Width = 598
      inherited pnlbottom: TPanel
        Width = 598
        inherited evPanel5: TPanel
          Width = 598
        end
        inherited dgBottom: TReDBGrid
          Width = 598
        end
      end
      inherited pnlMiddle: TPanel
        Width = 598
      end
    end
  end
  object cdExtAppFieldNames: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdExtAppFieldNamesDESCRIPTION: TStringField
      DisplayLabel = 'MJD field'
      DisplayWidth = 40
      FieldName = 'FIELDNAME'
      Size = 40
    end
    object cdExtAppFieldNamesUSERFIELDNAME: TStringField
      DisplayLabel = 'MJD field'
      FieldName = 'USERFIELDNAME'
      Size = 40
    end
  end
  object cdEvoFields: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 160
    object cdEvoFieldsCUSTOM_E_D_CODE_NUMBER: TStringField
      DisplayLabel = 'E/D code'
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object cdEvoFieldsTYPE: TStringField
      DisplayLabel = 'Field type'
      DisplayWidth = 10
      FieldName = 'TYPE'
      Size = 10
    end
    object cdEvoFieldsUSERFIELDNAME: TStringField
      DisplayLabel = 'Evolution field'
      FieldName = 'USERFIELDNAME'
      Size = 70
    end
  end
end
