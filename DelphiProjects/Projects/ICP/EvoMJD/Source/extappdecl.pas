unit extappdecl;

interface

uses
  gdyCommonLogger, isSettings, RateImportOptionFrame;

type
  TExtAppOptions = record
    Summarize: boolean;
    RateImport: TRateImportOption;
  end;

const
  ExtAppName = 'MJD';

const
  evoFieldTypeCompanyCode = 'CO_CODE';
  evoFieldTypeEENbr = 'EE_CODE';
  evoFieldTypeSSN = 'SSN';
  evoFieldTypeRate = 'RATE';
  evoFieldTypeJob = 'JOB';
  evoFieldTypePunchDate = 'PUNCHDATE';
  evoFieldTypeEDAmount = 'ED_AMOUNT';
  evoFieldTypeEDHours = 'ED_HOURS';


procedure LogExtAppOptions(logger: ICommonLogger; options: TExtAppOptions);

function LoadExtAppOptions(conf: IisSettings; root: string): TExtAppOptions;
procedure SaveExtAppOptions( const options: TExtAppOptions; conf: IisSettings; root: string);

implementation

uses
  sysutils, gdycommon;

procedure LogExtAppOptions(logger: ICommonLogger; options: TExtAppOptions);
begin
  logger.LogContextItem('Summarize', BoolToStr(options.Summarize,true));
  LogRateImportOption(logger, options.RateImport, ExtAppName);
end;

function LoadExtAppOptions(conf: IisSettings; root: string): TExtAppOptions;
begin
  root := root + IIF(root='','','\');
  Result.Summarize := conf.AsBoolean[root+'Summarize'];
  Result.RateImport := TRateImportOption(conf.AsInteger[root+'RateImport']);
  if (Result.RateImport < low(TRateImportOption)) or (Result.RateImport > high(TRateImportOption)) then
    Result.RateImport := rateUseExternal;
end;

procedure SaveExtAppOptions( const options: TExtAppOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\');
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsInteger[root+'RateImport'] := ord(options.RateImport);
end;

end.
