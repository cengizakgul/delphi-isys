unit FieldFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, gdyclasses,
  CustomBinderBaseFrame, common;

type
  TFieldFrm = class(TCustomBinderBaseFrm)
    cdExtAppFieldNames: TClientDataSet;
    cdExtAppFieldNamesDESCRIPTION: TStringField;
    cdEvoFields: TClientDataSet;
    cdEvoFieldsCUSTOM_E_D_CODE_NUMBER: TStringField;
    cdEvoFieldsTYPE: TStringField;
    cdEvoFieldsUSERFIELDNAME: TStringField;
    cdExtAppFieldNamesUSERFIELDNAME: TStringField;
  private
    procedure InitExtAppFieldNames(Header: IStr);
    procedure InitEvoFieldNames(evo: TEvoDBDTInfo; CL_E_DS: TDataSet);
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; evo: TEvoDBDTInfo; CL_E_DS: TDataSet; Header: IStr);
  end;

implementation

{$R *.dfm}
uses
  extappdecl, typinfo, evodata;

{ TFieldFrm }

const
  FieldBinding: TBindingDesc =
    ( Name: 'FieldBinding';
      Caption: 'MJD fields mapped to Evolution fields';
      LeftDesc: ( Caption: 'MJD fields'; Unique: true; KeyFields: 'FIELDNAME'; ListFields: 'USERFIELDNAME');
      RightDesc: ( Caption: 'Evolution fields'; Unique: true; KeyFields: 'TYPE;CUSTOM_E_D_CODE_NUMBER'; ListFields: 'USERFIELDNAME');
      HideKeyFields: true;
    );

constructor TFieldFrm.Create(Owner: TComponent);
begin
  inherited;
end;

procedure TFieldFrm.Init(matchTableContent: string; evo: TEvoDBDTInfo; CL_E_DS: TDataSet; Header: IStr);
begin
  InitExtAppFieldNames(Header);
  InitEvoFieldNames(evo, CL_E_DS);
  FBindingKeeper := CreateBindingKeeper( FieldBinding, TClientDataSet );
  FBindingKeeper.SetTables(cdExtAppFieldNames, cdEvoFields);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TFieldFrm.InitExtAppFieldNames(Header: IStr);
var
  i: integer;
begin
  CreateOrEmptyDataSet( cdExtAppFieldNames );
  for i := 0 to header.Count-1 do
    Append(cdExtAppFieldNames, 'USERFIELDNAME;FIELDNAME', [Header.Str[i], AnsiUpperCase(Header.Str[i])]);
end;

procedure TFieldFrm.InitEvoFieldNames(evo: TEvoDBDTInfo; CL_E_DS: TDataSet);
var
  level: char;
begin
  CreateOrEmptyDataSet(cdEvoFields);
  Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeCompanyCode, '', 'Company Code']);
  Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeEENbr, '', 'Employee Number']);
  Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeSSN, '', 'SSN']);
  Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeRate, '', 'Rate']);
  Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeJob, '', 'Job']);
  Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypePunchDate, '', 'Line Item Date']);

  for level := low(evo.Levels) to high(evo.Levels) do
    if evo.Levels[level].Enabled then
      Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [Copy(CDBDTMetadata[level].Table, 1, cdEvoFieldsTYPE.Size), '', CDBDTMetadata[level].Name]);
  CL_E_DS.First;
  while not CL_E_DS.Eof do
  begin
    Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeEDAmount, CL_E_DS['CUSTOM_E_D_CODE_NUMBER'], 'E/D - ' + CL_E_DS['DESCRIPTION'] + ' - Amount']);
    Append(cdEvoFields, 'TYPE;CUSTOM_E_D_CODE_NUMBER;USERFIELDNAME', [evoFieldTypeEDHours, CL_E_DS['CUSTOM_E_D_CODE_NUMBER'], 'E/D - ' + CL_E_DS['DESCRIPTION'] + ' - Hours']);
    CL_E_DS.Next;
  end;
end;

end.



