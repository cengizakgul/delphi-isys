unit mjdparser;

interface

uses
  gdyCommonLogger, gdyclasses;

type
  TMJDFile = record
    SourceFileName: string;
    Header: IStr;
    Data: array of IStr;
  end;

function LoadMJDFile(logger: ICommonLogger; fn: string): TMJDFile;
function MoneyStrToFloat(s: string): extended;
function GetColumnIdxByName(header: IStr; fn: string): integer;

implementation

uses
  gdycommon, SysUtils, StrUtils, gdystrset, common;

function GetColumnIdxByName(header: IStr; fn: string): integer;
var
  i: integer;
begin
  for i := 0 to header.Count-1 do
    if AnsiSameText(Header.str[i], trim(fn)) then
    begin
      Result := i;
      exit;
    end;
  raise Exception.CreateFmt('Cannot find MID field with name %s',[fn]);
end;

function MoneyStrToFloat(s: string): extended;
var
  r: string;
  i: integer;
begin
  r := '';
  for i := 1 to Length(s) do
    if s[i] in ['0'..'9', '.', '-'] then
      r := r + s[i];
  if r = '' then
    Result := 0
  else
    Result := StrToFloat(r);
end;

function ParseMJDFile(logger: ICommonLogger; content: string): TMJDFile;
var
  lines: IStr;
  i: integer;
begin
  logger.LogDebug('MJD File', content);
  lines := SplitToLines( content );
  while (lines.Count > 0) and (trim(lines.Str[lines.Count-1]) = '') do
    lines.Delete(lines.Count-1);

  if lines.Count = 0 then
    raise Exception.Create('MJD file is empty');

  if lines.Count = 1 then
    raise Exception.Create('MJD file must contain more lines than just one with the field names');

  SetLength(Result.Data, lines.Count-1);  //don't count header
  Result.SourceFileName := '';

  Result.Header := StdCsvAsStr(lines.Str[0]);
  Result.Header.TrimAll;

  for i := 1 to lines.Count-1 do
  begin
    Result.Data[i-1] := StdCsvAsStr(lines.Str[i]);
    Result.Data[i-1].TrimAll;
  end
end;

function LoadMJDFile(logger: ICommonLogger; fn: string): TMJDFile;
begin
  logger.LogEntry('Loading MJD file');
  try
    try
      logger.LogContextItem('MJD file', fn);
      Result := ParseMJDFile( logger, FileToString(fn) );
      Result.SourceFileName := fn;
    except
      logger.PassthroughException;
    end
  finally
    logger.LogExit;
  end;
end;

end.


