inherited MainFm: TMainFm
  Left = 250
  Top = 302
  Height = 600
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 89
    Height = 454
    ActivePage = tbshEdMap
    object tbshEdMap: TTabSheet [1]
      Caption = 'Fields mapping'
      ImageIndex = 3
      inline FieldFrame: TFieldFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 426
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 776
          Height = 426
          inherited evSplitter2: TSplitter
            Top = 162
            Width = 776
          end
          inherited pnltop: TPanel
            Width = 776
            Height = 162
            inherited evSplitter1: TSplitter
              Height = 162
            end
            inherited pnlTopLeft: TPanel
              Height = 162
              inherited dgLeft: TReDBGrid
                Height = 137
              end
            end
            inherited pnlTopRight: TPanel
              Width = 506
              Height = 162
              inherited evPanel4: TPanel
                Width = 506
              end
              inherited dgRight: TReDBGrid
                Width = 506
                Height = 137
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 167
            Width = 776
            inherited pnlbottom: TPanel
              Width = 776
              inherited evPanel5: TPanel
                Width = 776
              end
              inherited dgBottom: TReDBGrid
                Width = 776
              end
            end
            inherited pnlMiddle: TPanel
              Width = 776
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Import'
      object Bevel1: TBevel
        Left = 0
        Top = 386
        Width = 776
        Height = 6
        Align = alBottom
        Shape = bsBottomLine
      end
      object pnlBottom: TPanel
        Left = 0
        Top = 392
        Width = 776
        Height = 34
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Left = 8
          Top = 6
          Width = 129
          Height = 25
          Action = RunAction
          Caption = 'Import timeclock data'
          TabOrder = 0
        end
      end
      object pnlPayrollBatch: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 340
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        inline PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Left = 0
          Top = 0
          Width = 776
          Height = 340
          Align = alClient
          TabOrder = 0
          inherited Splitter1: TSplitter
            Height = 340
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 340
            inherited dgGrid: TReDBGrid
              Height = 315
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 457
            Height = 340
            inherited Panel1: TPanel
              Width = 457
            end
            inherited dgGrid: TReDBGrid
              Width = 457
              Height = 315
            end
          end
        end
      end
      object pnlSetups: TPanel
        Left = 0
        Top = 340
        Width = 776
        Height = 46
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object Bevel3: TBevel
          Left = 0
          Top = 0
          Width = 8
          Height = 46
          Align = alLeft
          Shape = bsLeftLine
        end
        inline ExtAppOptionsFrame: TExtAppOptionsFrm
          Left = 8
          Top = 0
          Width = 768
          Height = 46
          Align = alClient
          TabOrder = 0
          inherited cbSummarize: TCheckBox
            Left = 2
          end
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 543
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 158
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 13
        Top = 8
        Width = 140
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 158
      Top = 0
      Width = 626
      Height = 41
      Align = alClient
      TabOrder = 1
      inherited dblcCO: TwwDBLookupCombo
        Width = 225
      end
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 41
    Width = 784
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    inline FileOpenFrame: TFileOpenLeftBtnFrm
      Left = 0
      Top = 0
      Width = 784
      Height = 40
      Align = alClient
      TabOrder = 0
      DesignSize = (
        784
        40)
      inherited BitBtn1: TBitBtn
        Left = 14
        Width = 139
      end
      inherited edtFile: TEdit
        Left = 167
        Width = 613
      end
    end
  end
  object pnlSpacer: TPanel
    Left = 0
    Top = 81
    Width = 784
    Height = 8
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object RunAction: TAction
      Caption = 'Import timeclock data'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object RunEmployeeExport: TAction
      Caption = 'Update XYZ Employee Records for Selected Company'
    end
  end
end
