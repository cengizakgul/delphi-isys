unit mjd;

interface

uses
  gdyCommonLogger, extappdecl, mjdparser, common, timeclockimport, gdyBinder,
  evodata;

function GetTimeClockData(Logger: ICommonLogger; mjdFile: TMJDFile; options: TExtAppOptions; FieldMatcher: IMatcher; companyCode: string): TTimeClockImportData;

implementation

uses
  gdycommon, SysUtils, gdyclasses, StrUtils, gdystrset, variants, evConsts, typinfo, RateImportOptionFrame;

function MJDDateToDate(s: string): TDateTime;
var
  fs: TFormatSettings;
begin
//  GetLocaleFormatSettings( ,fs);
  fs.DateSeparator := '/';
  fs.ShortDateFormat := 'yyyy/mm/dd';
  Result := StrToDate(s, fs);
end;

function ConvertToEvoTC(Logger: ICommonLogger; mjdFile: TMJDFile; FieldMatcher: IMatcher; companyCode: string): TTimeClockRecs;

  function GetColumnIdx(type_: string): integer;
  var
    v: variant;
  begin
    v := FieldMatcher.LeftMatch(VarArrayOf([type_, '']));
    if VarIsNull(v) then
      Result := -1
    else
      Result := GetColumnIdxByName(mjdFile.Header, v);
  end;

var
  d: IStr;
  cur: PTimeClockRec;
  idxEENbr, idxSSN, idxCompanyCode, idxRate, idxJob, idxPunchDate: integer;
  idxDBDT: array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of integer;

  procedure AddRec(edcode: string);
  begin
    SetLength(Result, Length(Result)+1 ); //initialized with zeroes
    cur := @Result[high(Result)];
    if idxEENbr <> -1 then
      cur.CUSTOM_EMPLOYEE_NUMBER := d.Str[idxEENbr];
    if idxSSN <> -1 then
      cur.SSN := d.Str[idxSSN];
    if (idxRate <> -1) and (trim(d.Str[idxRate]) <> '') then
      cur.Rate := StrToFloat(d.Str[idxRate]);
    if idxJob <> -1 then
      cur.Job := d.Str[idxJob];
    if (idxPunchDate <> -1) and (trim(d.Str[idxPunchDate]) <> '') then
      cur.PunchDate := MJDDateToDate(d.Str[idxPunchDate]);
    cur.EDCode := edcode;
    if idxDBDT[CLIENT_LEVEL_DIVISION] <> -1 then
      cur.Division := d.Str[idxDBDT[CLIENT_LEVEL_DIVISION]];
    if idxDBDT[CLIENT_LEVEL_BRANCH] <> -1 then
      cur.Branch := d.Str[idxDBDT[CLIENT_LEVEL_BRANCH]];
    if idxDBDT[CLIENT_LEVEL_DEPT] <> -1 then
      cur.Department := d.Str[idxDBDT[CLIENT_LEVEL_DEPT]];
    if idxDBDT[CLIENT_LEVEL_TEAM] <> -1 then
      cur.Team := d.Str[idxDBDT[CLIENT_LEVEL_TEAM]];
    //Empty rate, Amount or hours is OK
  end;

var
  i: integer;
  j: integer;
  v: Variant;
  level: char;
begin
  SetLength(Result, 0);

  idxEENbr := GetColumnIdx(evoFieldTypeEENbr);
  idxSSN := GetColumnIdx(evoFieldTypeSSN);
  if (idxEENbr = -1) and (idxSSN = -1) then
    raise Exception.Create('Employee number or SSN needs to be mapped to an MJD field');
  idxCompanyCode := GetColumnIdx(evoFieldTypeCompanyCode);
  idxRate := GetColumnIdx(evoFieldTypeRate);
  idxJob := GetColumnIdx(evoFieldTypeJob);
  idxPunchDate := GetColumnIdx(evoFieldTypePunchDate);

  for level := low(idxDBDT) to high(idxDBDT) do
    idxDBDT[level] := GetColumnIdx(Copy(CDBDTMetadata[level].Table, 1, 10 {cdEvoFieldsTYPE.Size}));

  for i := 0 to high(mjdFile.Data) do
  begin
    logger.LogEntry('Converting MJD record to Evolution format');
    try
      try
        d := mjdFile.Data[i];

        if idxEENbr <> -1 then
          logger.LogContextItem(sCtxEECode, d.Str[idxEENbr]);
        if idxSSN <> -1 then
          logger.LogContextItem('SSN', d.Str[idxSSN]);
        if idxCompanyCode <> -1 then
          logger.LogContextItem('Company code', d.Str[idxCompanyCode]);

        if (idxCompanyCode <> -1) and not AnsiSameText(d.Str[idxCompanyCode], companyCode ) then
          continue;

        for j := 0 to d.Count-1 do
        begin
          if (j >= mjdFile.Header.Count) and not ((d.Count = mjdFile.Header.Count+1) and (d.Str[d.Count-1] = '')) then //forgive trailing comma
            raise Exception.CreateFmt('Number of values in a row (%d) does not match the number of columns (%d)', [d.Count, mjdFile.Header.Count]);

          if (j = d.Count-1) and (d.Str[j] = '') then //forgive trailing comma
            continue;

          v := FieldMatcher.RightMatch( AnsiUpperCase(mjdFile.Header.Str[j]) );
          if not VarIsNull(v) and InSet(v[0], [evoFieldTypeEDAmount, evoFieldTypeEDHours]) then
          begin
            AddRec(v[1]);
            if v[0] = evoFieldTypeEDAmount then
              cur.Amount := MoneyStrToFloat(d.Str[j])
            else
              cur.Hours := MoneyStrToFloat(d.Str[j])
          end;
        end;
      except
        logger.PassthroughException;
      end;
    finally
      logger.LogExit;
    end;
  end;
end;

function GetTimeClockData(Logger: ICommonLogger; mjdFile: TMJDFile; options: TExtAppOptions; FieldMatcher: IMatcher; companyCode: string): TTimeClockImportData;
var
  recs: TTimeClockRecs;
begin
  logger.LogEntry('Converting MJD data to Evolution format');
  try
    try
      Logger.LogContextItem('MJD file', mjdFile.SourceFileName);
      Logger.LogDebug('Field mapping', FieldMatcher.Dump);
      LogExtAppOptions(Logger, options);

      recs := ConvertToEvoTC(logger, mjdFile, FieldMatcher, companyCode);
      if options.Summarize then
        recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
      Result := BuildTimeClockImportData(recs, options.RateImport = rateUseEvoEE, false, logger); //false if rateUseExternal; it was this way before and everybody was happy...
      if not VarIsNull( FieldMatcher.LeftMatch(VarArrayOf([evoFieldTypeSSN, '']))) then
        Result.APIOptions.LookupOption := tciLookupBySSN
      else
        Result.APIOptions.LookupOption := tciLookupByNnumber;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

end.
