<Include>

<!-- Global Variables -->
<?define softwareName = "EvoUtils" ?>
<?define productManufacturer = "ISystems, LLC." ?>
<?define productManufacturerAbbr = "iSystems" ?>
<?define defaultVolume = "C:\Program Files\" ?>
<?define DelphiProjects =  "..\..\..\..\..\" ?>
<?define ThisProject =     "..\..\" ?>
<?define resourceFileDir = "$(var.DelphiProjects)Projects\Evolution\Installer\Resources\" ?>

<?define customActionDLL = $(var.resourceFileDir)isMsiCustomAction.dll ?>

</Include>
