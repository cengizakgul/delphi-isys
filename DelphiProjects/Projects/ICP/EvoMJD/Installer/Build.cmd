@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoMJD
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoMJD.exe
cd ..\..\..\Projects\ICP\EvoMJD\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoMJD.msi 

"%pWiXDir%\candle.exe" .\EvoMJD.wxs -wx -out ..\..\..\..\..\Tmp\EvoMJD.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoMJDRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoMJDRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoMJDRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoMJD.wixobj ..\..\..\..\..\Tmp\EvoMJDRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoMJD.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoMJD.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoMJD_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoMJD_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoMJD.msi EvoMJD_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
