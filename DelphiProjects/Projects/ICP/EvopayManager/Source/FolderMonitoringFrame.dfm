object FolderMonitoringFrm: TFolderMonitoringFrm
  Left = 0
  Top = 0
  Width = 509
  Height = 153
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 509
    Height = 153
    Align = alClient
    Caption = 'Input'
    TabOrder = 0
    inline DirOpenFrame: TDirOpenFrm
      Left = 2
      Top = 15
      Width = 505
      Height = 42
      Align = alTop
      TabOrder = 0
      inherited Panel1: TPanel
        Left = 113
        Width = 352
        inherited edtFile: TEdit
          Width = 456
        end
      end
      inherited Panel2: TPanel
        Width = 113
        inherited Label1: TLabel
          Width = 96
          Caption = 'Folder with data files'
        end
      end
      inherited Panel3: TPanel
        Left = 465
      end
    end
    object BitBtn1: TBitBtn
      Left = 8
      Top = 64
      Width = 129
      Height = 25
      Action = actMonitor
      Caption = 'Start folder monitoring'
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 168
    Top = 72
    object actMonitor: TAction
      Caption = 'Start folder monitoring'
      Checked = True
      OnExecute = actMonitorExecute
      OnUpdate = actMonitorUpdate
    end
  end
end
