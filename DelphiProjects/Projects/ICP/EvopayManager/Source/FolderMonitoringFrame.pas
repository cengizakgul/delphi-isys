unit FolderMonitoringFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, Buttons, DirOpenFrame, rxnotify, issettings,
  gdyCommonLogger;

type
  TFolderMonitoringSettings = record
    FolderName: string;
    Active: boolean;
  end;

  TFolderScanEvent = procedure(folder: string; isBackground: boolean) of object;

  TFolderMonitoringFrm = class(TFrame)
    GroupBox1: TGroupBox;
    DirOpenFrame: TDirOpenFrm;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    actMonitor: TAction;
    procedure actMonitorUpdate(Sender: TObject);
    procedure actMonitorExecute(Sender: TObject);
  private
    FChangeNotifier: TRxFolderMonitor;
    FOnFolderScan: TFolderScanEvent;
    FLogger: ICOmmonLogger;

    procedure HandleFolderContentChanged(Sender: TObject);
    procedure HandleDirChanged(dirname: string);
    function GetSettings: TFolderMonitoringSettings;
    procedure SetSettings(const Value: TFolderMonitoringSettings);
    procedure SetMonitoring(active: boolean);
    procedure SetLogger(const Value: ICommonLogger);
  public
    constructor Create(Owner: TComponent); override;
    property OnFolderScan: TFolderScanEvent read FOnFolderScan write FOnFolderScan;
    property Settings: TFolderMonitoringSettings read GetSettings write SetSettings;
    property Logger: ICommonLogger read FLogger write SetLogger;
  end;

procedure SaveFolderMonitoringSettings(param: TFolderMonitoringSettings; conf: IisSettings; root: string );
function LoadFolderMonitoringSettings(conf: IisSettings; root: string): TFolderMonitoringSettings;

implementation

uses
  gdycommon;
{$R *.dfm}

procedure SaveFolderMonitoringSettings(param: TFolderMonitoringSettings; conf: IisSettings; root: string );
begin
  root := root + IIF(root='','','\') + 'FolderMonitor\';
  conf.AsString[root+'FolderName'] := param.FolderName;
  conf.AsBoolean[root+'Active'] := param.Active;
end;

function LoadFolderMonitoringSettings(conf: IisSettings; root: string): TFolderMonitoringSettings;
begin
  root := root + IIF(root='','','\') + 'FolderMonitor\';
  Result.FolderName := conf.AsString[root+'FolderName'];
  Result.Active := conf.AsBoolean[root+'Active'];
end;

constructor TFolderMonitoringFrm.Create(Owner: TComponent);
begin
  inherited;
  DirOpenFrame.OnDirChanged := HandleDirChanged;
  FChangeNotifier := TRxFolderMonitor.Create(Self);
  FChangeNotifier.Active := false;
  FChangeNotifier.Filter := [fnFileName];
  FChangeNotifier.MonitorSubtree := false;
  FChangeNotifier.OnChange := HandleFolderContentChanged;
end;

procedure TFolderMonitoringFrm.HandleDirChanged(dirname: string);
var
  wasActive: boolean;
begin
  Logger.LogEntry('User changed monitored folder');
  try
    try
      Logger.LogContextItem('Folder name', dirname);
      wasActive := FChangeNotifier.Active;
      SetMonitoring(false);
      FChangeNotifier.FolderName := dirname;
      SetMonitoring(wasActive);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end
end;

procedure TFolderMonitoringFrm.HandleFolderContentChanged(Sender: TObject);
begin
  if assigned(FOnFolderScan) then
    FOnFolderScan(DirOpenFrame.FolderName, Sender <> nil);
end;

function TFolderMonitoringFrm.GetSettings: TFolderMonitoringSettings;
begin
  Result.FolderName := DirOpenFrame.FolderName;
  Result.Active := FChangeNotifier.Active;
end;

procedure TFolderMonitoringFrm.SetSettings(const Value: TFolderMonitoringSettings);
begin
  Logger.LogEntry('Applying folder monitoring settings');
  try
    try
      Logger.LogContextItem('Folder name', Value.FolderName);
      Logger.LogContextItem('Active', BoolToStr(Value.Active, true));

      SetMonitoring(false);
      DirOpenFrame.FolderName := Value.FolderName;
      FChangeNotifier.FolderName := DirOpenFrame.FolderName;
      SetMonitoring(Value.Active);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end
end;

procedure TFolderMonitoringFrm.actMonitorUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := DirOpenFrame.IsValid;
  if FChangeNotifier.Active then
    (Sender as TCustomAction).Caption := 'Stop folder monitoring'
  else
    (Sender as TCustomAction).Caption := 'Start folder monitoring';
end;

procedure TFolderMonitoringFrm.actMonitorExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      SetMonitoring(not FChangeNotifier.Active);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TFolderMonitoringFrm.SetMonitoring(active: boolean);
begin
  if FChangeNotifier.Active <> active then
  begin
    if not DirectoryExists(FChangeNotifier.FolderName) then
      raise Exception.CreateFmt('Folder "%s" doesn''t exist', [FChangeNotifier.FolderName]);
    FChangeNotifier.Active := active;
    
    if FChangeNotifier.Active = active then
    begin
      if FChangeNotifier.Active then
        Logger.LogEventFmt('Folder monitoring started: %s', [FChangeNotifier.FolderName])
      else
        Logger.LogEventFmt('Folder monitoring stopped: %s', [FChangeNotifier.FolderName]);
    end
    else
    begin
      if active then
        Logger.LogEventFmt('Failed to start folder monitoring: %s', [FChangeNotifier.FolderName])
      else
        Logger.LogEventFmt('Failed to stop folder monitoring: %s', [FChangeNotifier.FolderName]);
    end;
    
    if FChangeNotifier.Active then
      HandleFolderContentChanged(nil);
  end;
end;

procedure TFolderMonitoringFrm.SetLogger(const Value: ICommonLogger);
begin
  FLogger := Value;
  DirOpenFrame.Logger := FLogger;
end;

end.
