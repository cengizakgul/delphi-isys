program EvopayManager;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  MainForm in 'MainForm.pas' {MainFm},
  DirOpenFrame in '..\..\common\DirOpenFrame.pas' {DirOpenFrm: TFrame},
  FolderMonitoringFrame in 'FolderMonitoringFrame.pas' {FolderMonitoringFrm: TFrame},
  OptionsFrams in 'OptionsFrams.pas' {OptionsFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame};

{$R *.res}

begin
  LicenseKey := '7BD84037E877497282AAE249214E5889'; //swipeclock's

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evopay Manager';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
