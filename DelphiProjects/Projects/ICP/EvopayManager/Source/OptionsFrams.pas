unit OptionsFrams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DirOpenFrame, StdCtrls, isSettings, ActnList, Buttons, gdyCommonLogger;

type
  TOptions = record
    OutputFolder: string;
  end;

  TOptionsFrm = class(TFrame)
    GroupBox1: TGroupBox;
    DirOpenFrm1: TDirOpenFrm;
    ActionList1: TActionList;
    BitBtn1: TBitBtn;
    actOpenFolder: TAction;
    procedure actOpenFolderUpdate(Sender: TObject);
    procedure actOpenFolderExecute(Sender: TObject);
  private
    FLogger: ICommonLogger;
    function GetOptions: TOptions;
    procedure SetOptions(const Value: TOptions);
    procedure SetLogger(const Value: ICommonLogger);
  public
    property Options: TOptions read GetOptions write SetOptions;
    property Logger: ICommonLogger read FLogger write SetLogger;
    procedure Check;
  end;

procedure SaveOptions(param: TOptions; conf: IisSettings; root: string );
function LoadOptions(conf: IisSettings; root: string): TOptions;

implementation

uses
  gdyUtils, gdycommon;
{$R *.dfm}

{ TOptionsFrm }

procedure TOptionsFrm.Check;
begin
  Logger.LogEntry('Checking folder to store logs');
  try
    try
      Logger.LogDebug('FolderName is <' + DirOpenFrm1.FolderName + '>'); // ok even if is not valid

      if not DirOpenFrm1.IsValid then
        raise Exception.Create('Folder to store logs and imported files is not specified');
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TOptionsFrm.GetOptions: TOptions;
begin
  Result.OutputFolder := DirOpenFrm1.FolderName;
end;

procedure TOptionsFrm.SetOptions(const Value: TOptions);
begin
  DirOpenFrm1.FolderName := Value.OutputFolder;
end;

procedure SaveOptions(param: TOptions; conf: IisSettings; root: string );
begin
  root := root + IIF(root='','','\') + 'Options\';
  conf.AsString[root+'OutputFolder'] := param.OutputFolder;
end;

function LoadOptions(conf: IisSettings; root: string): TOptions;
begin
  root := root + IIF(root='','','\') + 'Options\';
  Result.OutputFolder := conf.AsString[root+'OutputFolder'];
end;

procedure TOptionsFrm.actOpenFolderUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := DirOpenFrm1.IsValid;
end;

procedure TOptionsFrm.actOpenFolderExecute(Sender: TObject);
begin
  OpenDoc(Self, DirOpenFrm1.FolderName);
end;

procedure TOptionsFrm.SetLogger(const Value: ICommonLogger);
begin
  FLogger := Value;
  DirOpenFrm1.Logger := FLogger;
end;

end.
