{$WARN SYMBOL_PLATFORM OFF}
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientMainForm, ComCtrls, EvoAPIConnectionParamFrame,
  CoolTrayIcon, StdActns, ActnList, Menus, FolderMonitoringFrame, StdCtrls,
  evoapiconnection, Buttons, OptionsFrams, SmtpConfigFrame;

type
  TImportResult = record
    EmailNotification: boolean;
    CopyStuff: boolean;
  end;

  TMainFm = class(TEvoAPIClientMainFm)
    TrayIcon: TCoolTrayIcon;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    actOpen: TAction;
    Open1: TMenuItem;
    Exit1: TMenuItem;
    actExit: TAction;
    FolderMonitoringFrame: TFolderMonitoringFrm;
    btnTestConnection: TBitBtn;
    OptionsFrame: TOptionsFrm;
    SmtpConfigFrame: TSmtpConfigFrm;
    procedure actOpenExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actOpenUpdate(Sender: TObject);
    procedure btnTestConnectionClick(Sender: TObject);
    procedure actExitUpdate(Sender: TObject);
  private
    FScanning: boolean;
    procedure HandleFolderScan(folder: string; isBackground: boolean);
    procedure ScanFolder(folder: Variant);
    procedure ImportFile(conn: IEvoAPIConnection; filename: string; bk: boolean);
    function DoImportFile(conn: IEvoAPIConnection;
      filename: string; bk: boolean): TImportResult;
    procedure CopyStuffToLogFolder(srcfilename, htmllog: string);
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

uses
  UtilityLoggerViewFrame, gdyDeferredCall, evowaitform, gdycommon, gdyRedir,
  common, gdyCommonLogger, evoapiconnectionutils, gdyGlobalWaitIndicator;

{$R *.dfm}

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TUtilityLoggerViewFrm;
  inherited;
  if EvoFrame.IsValid then
    PageControl1.TabIndex := 1
  else
    PageControl1.TabIndex := 0;

  OptionsFrame.Logger := Logger;

  OptionsFrame.Options := LoadOptions(FSettings, '');
  SmtpConfigFrame.Config := LoadSmtpConfig(FSettings, '');
  FolderMonitoringFrame.Logger := Logger;
  FolderMonitoringFrame.OnFolderScan := HandleFolderScan;
  FolderMonitoringFrame.Settings := LoadFolderMonitoringSettings(FSettings, ''); //must be the last to load
end;

destructor TMainFm.Destroy;
begin
  SaveFolderMonitoringSettings(FolderMonitoringFrame.Settings, FSettings, '');
  SaveOptions(OptionsFrame.Options, FSettings, '');
  SaveSmtpConfig(SmtpConfigFrame.Config, FSettings, '');
  inherited;
end;

procedure TMainFm.actOpenExecute(Sender: TObject);
begin
  Show;
end;

procedure TMainFm.actExitUpdate(Sender: TObject);
begin
 (Sender as TCustomAction).Enabled := not FScanning;
end;

procedure TMainFm.actExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainFm.actOpenUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := not Visible and not FScanning;
end;

procedure TMainFm.HandleFolderScan(folder: string; isBackground: boolean);
begin
  DeferredCall(ScanFolder, VarArrayOf([folder, isBackground]));
end;

procedure GetFiles(folder: string; files: TStringList);
var
  SR: TSearchRec;
  isFound: Boolean;
begin
  files.Clear;
  isFound := FindFirst(folder + '\*.csv', faReadOnly + faArchive, SR) = 0;
  try
    while IsFound do
    begin
      if ExtractFileExt(SR.Name) = '.csv' then
        files.Add(folder  + '\' + SR.Name);
      IsFound := FindNext(SR) = 0;
    end;
  finally
    FindClose(SR);
  end;
end;

procedure TMainFm.btnTestConnectionClick(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TButton).Caption]) );
  try
    try
      EvoFrame.Check;
      CreateEvoAPIConnection(EvoFrame.Param, Logger);
      Logger.LogDebug('Connected successfully!');
      ShowMessage('Connected successfully!');
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end
end;

procedure TMainFm.ScanFolder(folder: Variant);
var
  d: string;
  bk: boolean;
  files: TStringList;
  i: integer;
  conn: IEvoAPIConnection;
begin
  bk := false;
  if FScanning then
    exit;
  FScanning := true;
  try
    Logger.LogEntry('Scanning folder');
    try
      try
        d := VarToStr(folder[0]);
        bk := folder[1];
        Logger.LogContextItem('Folder', d);
        EvoFrame.Check;
        OptionsFrame.Check;
        files := TStringList.Create;
        try
          GetFiles(d, files);
          for i := 0 to files.Count-1 do
          begin
            if i = 0 then
              conn := CreateEvoAPIConnection(EvoFrame.Param, Logger);
            ImportFile(conn, files[i], bk);
          end;
        finally
          FreeAndNil(files)
        end;
      except
        if bk then
        begin
          Logger.StopException;
          TrayIcon.ShowBalloonHint('Evopay Manager', 'Import failed', bitNone, 10);
        end
        else
          Logger.PassthroughException;
      end;
    finally
      Logger.LogExit;
    end;
  finally
    FScanning := false;
  end;
end;

function TMainFm.DoImportFile(conn: IEvoAPIConnection; filename: string; bk: boolean): TImportResult;
var
  files: TEvoXImportInputFiles;
  res: IEvoXImportResultsWrapper;
  istr: TFileStream;
  data: string;
  status: string;
begin
  Result.CopyStuff := false;
  Result.EmailNotification := true;

  Logger.LogEntry('Importing employee records');
  try
    try
//      Logger.LogContextItem('Filename', filename); //already logged
      Logger.LogEventFmt('Employee records import started: %s', [filename]);
      if bk then
        TrayIcon.ShowBalloonHint('Evopay Manager', Format('Importing %s', [filename]), bitNone, 10);
      SetLength(files, 1);

      istr := TFileStream.Create( filename, fmOpenRead or fmShareExclusive );
      try
        SetLength(data, istr.Size);
        istr.Read(pchar(data)^, Length(data));

        files[0].Filename := filename;
        files[0].Filedata := data;
        //too bad that because of modal loop RunEvoXImport returns nil instead of throwing exception
        res := CreateGUIEvoXImportExecutor(conn, Self, Logger).RunEvoXImport(files, FileToString(Redirection.GetFilename(sEvoXMapFileAlias)), 'Employee' );
        if res <> nil then
        begin
          Assert(res.Count = 1);
          status := LogEvoXImportResults(Logger, res.Result[0], 'Employee records');
          if bk then
            TrayIcon.ShowBalloonHint('Evopay Manager', status, bitNone, 10);
          Result.CopyStuff := res.Result[0].ImportedRowsCount > 0;
          Result.EmailNotification := res.Result[0].ImportedRowsCount < res.Result[0].InputRowsCount;
        end
        else //!! sends email even if user manually cancelled operation
        begin
          if bk then
            TrayIcon.ShowBalloonHint('Evopay Manager', 'Import failed', bitNone, 10);
          Logger.LogWarningFmt('Employee records import failed', []);
        end
      finally
        FreeAndNil(istr);
      end;
    except
      if bk then
        TrayIcon.ShowBalloonHint('Evopay Manager', 'Import failed', bitNone, 10);
      Logger.PassthroughExceptionAndWarnFmt('Employee records import failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.CopyStuffToLogFolder(srcfilename, htmllog: string);
var
  destdir: string;
  destfn: string;
begin
  Logger.LogEntry('Storing log and input files');
  try
    try
      Logger.LogContextItem('Folder to store logs and imported files', OptionsFrame.Options.OutputFolder);
      if not DirectoryExists(OptionsFrame.Options.OutputFolder) then
        raise Exception.CreateFmt('Folder "%s" doesn''t exist', [OptionsFrame.Options.OutputFolder]);

      destdir := CreateSomeDirectory( WithTrailingSlash(OptionsFrame.Options.OutputFolder) + FormatDateTime('yyyy-mm-dd-hhnn', now) );

      destfn := WithTrailingSlash(destdir) + ExtractFileName(srcfilename);
      Win32Check(MoveFileEx(PChar(srcfilename), PChar(destfn), MOVEFILE_COPY_ALLOWED));

      SaveLogArchiveTo(destdir);

      SaveHtmlLogFileTo(htmllog, WithTrailingSlash(destdir) + 'Log.html');
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.ImportFile(conn: IEvoAPIConnection; filename: string; bk: boolean);
  procedure SendEmailNotification(fromExcept: boolean); //pretty ugly
    procedure doit;
    begin
      if SmtpConfigFrame.Config.Use then
      begin
        WaitIndicator.StartWait('Sending email');
        try
          sendmail(SmtpConfigFrame.Config, 'Evopay Manager - import failed',
                         Format('This message was automatically sent by Evopay Manager.'#13#10'Import failed for "%s".', [filename]), '' );
        finally
          WaitIndicator.EndWait;
        end;
      end;
    end;
  begin

    if fromExcept then //cannot call LogEntry, otherwise the original exception will be reported twice, see comments in the log implementation
    begin
      try
        doit
      except
        on E: Exception do Logger.LogError('Failed to send email notification:' + E.Message, SmtpConfigToStr(SmtpConfigFrame.Config))
      end
    end
    else  //most of the time use traditional way, for uniformity only
    begin
      Logger.LogEntry('Sending email notification');
      try
        try
          LogSmtpConfig(logger, SmtpConfigFrame.Config);
          Logger.LogContextItem(sCtxComponent, 'Email notification');
          doit;
        except
          Logger.StopException;
        end;
      finally
        Logger.LogExit;
      end;
    end
  end;
var
  res: TImportResult;
  htmllog: string;
begin
  htmllog := '';
  try
    Logger.LogEntry('Processing file');
    try
      try
        Logger.LogContextItem('Filename', filename);
        (Self as ILogRecorder).Start(true{html});
        try
          res := DoImportFile(conn, filename, bk);
        finally
          htmllog := (Self as ILogRecorder).Stop;
        end;
        if res.CopyStuff then
          CopyStuffToLogFolder(filename, htmllog);
        if res.EmailNotification then
          SendEmailNotification(false);
      except
        Logger.PassthroughException;
      end;
    finally
      Logger.LogExit;
    end;
  except
    SendEmailNotification(true);
    raise;
  end;
end;

end.
