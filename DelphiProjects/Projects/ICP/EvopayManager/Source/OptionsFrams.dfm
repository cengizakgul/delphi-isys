object OptionsFrm: TOptionsFrm
  Left = 0
  Top = 0
  Width = 579
  Height = 92
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 579
    Height = 92
    Align = alClient
    Caption = 'Logs'
    TabOrder = 0
    inline DirOpenFrm1: TDirOpenFrm
      Left = 2
      Top = 15
      Width = 575
      Height = 42
      Align = alTop
      TabOrder = 0
      inherited Panel1: TPanel
        Left = 185
        Width = 350
        inherited edtFile: TEdit
          Left = 24
          Width = 319
        end
      end
      inherited Panel2: TPanel
        Width = 185
        inherited Label1: TLabel
          Width = 174
          Caption = 'Folder to store logs and imported files'
        end
      end
      inherited Panel3: TPanel
        Left = 535
      end
    end
    object BitBtn1: TBitBtn
      Left = 12
      Top = 56
      Width = 75
      Height = 25
      Action = actOpenFolder
      Caption = 'Open folder'
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 288
    Top = 16
    object actOpenFolder: TAction
      Caption = 'Open folder'
      OnExecute = actOpenFolderExecute
      OnUpdate = actOpenFolderUpdate
    end
  end
end
