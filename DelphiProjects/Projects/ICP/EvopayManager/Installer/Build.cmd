@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvopayManager
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvopayManager.exe
cd ..\..\..\Projects\ICP\EvopayManager\Installer\Projects

SET TEMPPATH=..\..\..\..\..\Tmp

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvopayManager.msi 

"%pWiXDir%\candle.exe" .\EvopayManager.wxs -wx -out %TEMPPATH%\EvopayManager.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir "..\..\..\Common\HTML Templates" -gg -sfrag -srd -cg HTMLTemplatesGroup -dr HTMLTEMPLATESDIR -var var.Path -out %TEMPPATH%\EvopayManagerHTMLTemplates.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvopayManagerHTMLTemplates.wxs -wx -out %TEMPPATH%\EvopayManagerHTMLTemplates.wixobj -dPath="..\..\..\Common\HTML Templates"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" %TEMPPATH%\EvopayManager.wixobj %TEMPPATH%\EvopayManagerHTMLTemplates.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvopayManager.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvopayManager.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvopayManager_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvopayManager_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvopayManager.msi EvopayManager_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
