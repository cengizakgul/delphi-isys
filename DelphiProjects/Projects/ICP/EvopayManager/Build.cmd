@ECHO OFF
SET pAppName=EvopayManager

IF "%1" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. Application Version
ECHO    2. DevTools folder (optional)
GOTO end

:start

call ..\Common\Build\Build3.cmd %pAppName% %1 %2

:end
