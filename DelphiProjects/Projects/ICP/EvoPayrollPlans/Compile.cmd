@ECHO OFF
REM Parameters: 
REM    1. Version(optional)
REM    2. Flags: (R)elease or (D)ebug (optional)

SET pAppVersion=%1
IF "%pAppVersion%" == "" GOTO compile

SET pCompileOptions=-DPRODUCTION_LICENCE
IF "%2" == "R" SET pCompileOptions=%pCompileOptions% -DFINAL_RELEASE

:compile

IF NOT EXIST ..\..\..\Bin\ICP\EvoPayrollPlans mkdir ..\..\..\Bin\ICP\EvoPayrollPlans
IF NOT EXIST ..\..\..\Bin\ICP\EvoPayrollPlans\Queries mkdir ..\..\..\Bin\ICP\EvoPayrollPlans\Queries
IF NOT EXIST "..\..\..\Bin\ICP\EvoPayrollPlans\HTML Templates" mkdir "..\..\..\Bin\ICP\EvoPayrollPlans\HTML Templates"
IF NOT EXIST "..\..\..\Bin\ICP\EvoPayrollPlans\HTML Templates\Log_files" mkdir "..\..\..\Bin\ICP\EvoPayrollPlans\HTML Templates\Log_files"

ECHO *Compiling
..\..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion% "%pCompileOptions%"

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
REM XCOPY /Y /Q .\Source\*.mes ..\..\..\Bin\ICP\EvoPayrollPlans\*.* > NUL
FOR %%a IN (..\..\..\Bin\ICP\EvoPayrollPlans\*.exe) DO ..\..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoPayrollPlans\*.exe) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoPayrollPlans\*.dll) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoPayrollPlans\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\EvoPayrollPlans\*.mes
REM FOR %%a IN (..\..\..\Bin\ICP\EvoPayrollPlans\*.map) DO DEL /F /Q ..\..\..\Bin\ICP\EvoPayrollPlans\*.map

GOTO end

:error
IF "%pAppVersion%" == "" PAUSE
EXIT 1

:end
IF "%pAppVersion%" == "" PAUSE
