unit ScheduledEDFileLoader;

interface

uses DB, kbmMemTable, gdyCommonLogger, SysUtils, EvoAPIConnection,
  Classes, Types;

type
  FieldDescr = record
    C: string;     // Caption
    M: boolean;    // Mandatory Field
    T: TFieldType; // Type
    S: integer;    // Size
    N: string;     // Name
    Tb: string;    // Table Name
    FileColumn: integer;
  end;

const
  idxCoNumber = 1;
  idxEeCode = 2;
  idxEdCode = 3;
  idxAmount = 4;

  FieldsToImport: array[1..4] of FieldDescr =
  (
    (C: 'Custom Company Number'; M: True; T: ftString; S: 20; N: 'CUSTOM_COMPANY_NUMBER'; Tb: 'CO'; FileColumn: 11),
    (C: 'Ee Code'; M: True; T: ftString; S: 10; N: 'CUSTOM_EMPLOYEE_NUMBER'; Tb: 'EE'; FileColumn: 0),
    (C: 'Ed Code'; M: True; T: ftString; S: 10; N: 'CUSTOM_E_D_CODE_NUMBER'; Tb: 'CL_E_DS'; FileColumn: 2),
    (C: 'Amount'; M: False; T: ftFloat; S: 0; N: 'AMOUNT'; Tb: 'EE_SCHEDULED_E_DS'; FileColumn: 3)
  );

type
  TScheduledEDFileLoader = class
  private
    FProcessedRecord: integer;
    FScheduledEDData: TkbmCustomMemTable;
    FLogger: ICommonLogger;

    procedure CreateFields;

    function GetIsReady: boolean;
    function GetRecordCount: integer;

    procedure ProcessRecord(rec: TStrings);
    procedure LoadDataFromFile(const aFileName: string);

    function GetCompanyNbr: string;
    function GetEmployeeCode: string;
    function GetEDCode: string;
    function GetAmount: double;
  public
    constructor Create(logger: ICommonLogger; const aFileName: string);
    destructor Destroy; override;

    procedure First;
    function Eof: boolean;
    procedure Next;

    property IsReady: boolean read GetIsReady;
    property RecordCount: integer read GetRecordCount;
    property ProcessedRecordCount: integer read FProcessedRecord;

    property CompanyNbr: string read GetCompanyNbr;
    property EmployeeCode: string read GetEmployeeCode;
    property EDCode: string read GetEDCode;
    property Amount: double read GetAmount;
  end;

implementation

uses common, TypInfo, PayrollPlansProcessing, gdyRedir,
  DateUtils;

{ TScheduledEDFileLoader }

constructor TScheduledEDFileLoader.Create(logger: ICommonLogger; const aFileName: string);
begin
  FLogger := logger;
  FScheduledEDData := TkbmCustomMemTable.Create(nil);

  CreateFields;

  LoadDataFromFile( aFileName );
end;

procedure TScheduledEDFileLoader.CreateFields;

  procedure CreateMapFields(MapArray: Array of FieldDescr; DS: TkbmCustomMemTable);
  var
    i: integer;
  begin
    for i := Low(MapArray) to High(MapArray) do
    if MapArray[i].N <> '' then
    case MapArray[i].T of
      ftInteger:
        CreateIntegerField( DS, MapArray[i].N, MapArray[i].C );
      ftString:
        CreateStringField( DS, MapArray[i].N, MapArray[i].C, MapArray[i].S );
      ftDateTime:
        CreateDateTimeField( DS, MapArray[i].N, MapArray[i].C );
      ftFloat:
        CreateFloatField( DS, MapArray[i].N, MapArray[i].C );
      ftBoolean:
        CreateBooleanField( DS, MapArray[i].N, MapArray[i].C );
    end;
  end;

begin
  CreateMapFields( FieldsToImport, FScheduledEDData );
end;

destructor TScheduledEDFileLoader.Destroy;
begin
  FreeAndNil( FScheduledEDData );
  inherited;
end;

function TScheduledEDFileLoader.Eof: boolean;
begin
  Result := FScheduledEDData.Eof;
end;

procedure TScheduledEDFileLoader.First;
begin
  FScheduledEDData.First;
end;

function TScheduledEDFileLoader.GetAmount: double;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxAmount].N).AsFloat;
end;

function TScheduledEDFileLoader.GetCompanyNbr: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxCoNumber].N).AsString;
end;

function TScheduledEDFileLoader.GetEDCode: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxEdCode].N).AsString;
end;

function TScheduledEDFileLoader.GetEmployeeCode: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxEeCode].N).AsString;
end;

function TScheduledEDFileLoader.GetIsReady: boolean;
begin
  Result := Assigned(FScheduledEDData) and (FScheduledEDData.Active);
end;

function TScheduledEDFileLoader.GetRecordCount: integer;
begin
  Result := -1;
  if GetIsReady then
    Result := FScheduledEDData.RecordCount;
end;

procedure TScheduledEDFileLoader.LoadDataFromFile(const aFileName: string);
const
  BatchSize = $FF;
var
  f: TfileStream;
  s: array[0..BatchSize] of char;
  i, j, rest: Dword;
  Text: String;
  sFile: TStrings;
  Rec: TStrings;
begin
  FProcessedRecord := 0;
  if Assigned(  FScheduledEDData ) then
  try
    FLogger.LogEvent('Load data from file');
    FScheduledEDData.Close;
    FScheduledEDData.Open;

    f := TfileStream.Create(aFileName, fmOpenRead);
    try
      i := f.Size div BatchSize;
      rest := f.Size mod BatchSize;
      for j := 1 to i do
      begin
        f.Read(s, BatchSize);
        Text := Text + s;
      end;
      if rest > 0 then
      begin
        f.Read(s, rest);
        Text := Text + Copy(s, 1, rest);
      end;
    finally
      f.Free;
    end;

    sFile := TStringList.Create;
    Rec := TStringList.Create;
    try
      sFile.Text := Text;
      Rec.Delimiter := sDelimiter;
      for i := 0 to sFile.Count - 1 do
      begin
        Rec.DelimitedText := sFile[i];
        ProcessRecord( Rec );
      end;
    finally
      sFile.Free;
      Rec.Free;
    end;

    if FScheduledEDData.State in [dsEdit, dsInsert] then
      FScheduledEDData.Post;

    FLogger.LogEvent('Load data from file is done successfully');
  except
    on e: Exception do
      FLogger.LogError('Error when loading data from file', e.Message);
  end;
end;

procedure TScheduledEDFileLoader.Next;
begin
  FScheduledEDData.Next;
end;

procedure TScheduledEDFileLoader.ProcessRecord(rec: TStrings);
var
  curValue: Currency;
  datValue: TDatetime;

  function GetExcludeWeekFieldValue(aWeekNbr: integer; const aImportedValue: string): string;
  begin
    Result := Copy(aImportedValue, aWeekNbr, 1);
    if Result = '' then
      Result := 'Y';
  end;

  procedure SetFieldValue(aIdx: integer);
  begin
    if FieldsToImport[aIdx].T = ftString then
      FScheduledEDData.FieldByName( FieldsToImport[aIdx].N ).AsString := Trim(rec[ FieldsToImport[aIdx].FileColumn ])
    else if FieldsToImport[aIdx].T = ftFloat then
    begin
      if TryStrToCurr(Trim(rec[ FieldsToImport[aIdx].FileColumn ]), curValue) then
        FScheduledEDData.FieldByName( FieldsToImport[aIdx].N ).AsFloat := curValue
      else
        raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. "'+ FieldsToImport[aIdx].C +'" should contain a currency value.');
    end
    else if FieldsToImport[aIdx].T = ftDateTime then
    begin
      if TryStrToDateTime(Trim(rec[ FieldsToImport[aIdx].FileColumn ]), datValue) then
        FScheduledEDData.FieldByName( FieldsToImport[aIdx].N ).AsFloat := datValue
      else
        raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. "'+ FieldsToImport[aIdx].C +'" should contain a datatime value.');
    end
  end;
begin
  FProcessedRecord := FProcessedRecord + 1;

  if (Trim(rec[ FieldsToImport[idxCoNumber].FileColumn ]) = '') then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. Company Code can''t be empty.')
  else if (Trim(rec[ FieldsToImport[idxEeCode].FileColumn ]) = '') then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. Employee Code can''t be empty.')
  else if (Trim(rec[ FieldsToImport[idxEdCode].FileColumn ]) = '') then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. E/D Code can''t be empty.')
  else begin
    try
      FScheduledEDData.Append;
      FScheduledEDData.FieldByName( FieldsToImport[idxCoNumber].N ).AsString := Trim(rec[ FieldsToImport[idxCoNumber].FileColumn ]);
      FScheduledEDData.FieldByName( FieldsToImport[idxEeCode].N ).AsString := Trim(rec[ FieldsToImport[idxEeCode].FileColumn ]);
      FScheduledEDData.FieldByName( FieldsToImport[idxEdCode].N ).AsString := 'D' + Trim(rec[ FieldsToImport[idxEdCode].FileColumn ]);

      SetFieldValue( idxAmount );

      FScheduledEDData.Post;
    except
      on E: Exception do
      begin
        if FScheduledEDData.State in [dsEdit, dsInsert] then
          FScheduledEDData.Cancel;
        FLogger.LogError(E.Message);
      end;
    end;
  end;
end;

end.
