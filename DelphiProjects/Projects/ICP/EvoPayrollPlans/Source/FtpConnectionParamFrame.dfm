inherited FtpConnectionParamFrm: TFtpConnectionParamFrm
  Width = 562
  Height = 186
  object gbFTPServer: TGroupBox
    Left = 0
    Top = 0
    Width = 562
    Height = 186
    Align = alClient
    Caption = 'FTP Settings'
    TabOrder = 0
    object lblAddress: TLabel
      Left = 16
      Top = 26
      Width = 94
      Height = 13
      Caption = 'FTP Server address'
    end
    object lblUser: TLabel
      Left = 16
      Top = 51
      Width = 48
      Height = 13
      Caption = 'Username'
    end
    object lblPassword: TLabel
      Left = 16
      Top = 75
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object lblPortCaption: TLabel
      Left = 16
      Top = 102
      Width = 19
      Height = 13
      Caption = 'Port'
    end
    object Label1: TLabel
      Left = 16
      Top = 138
      Width = 29
      Height = 13
      Caption = 'Folder'
    end
    object edFTPServerAddress: TEdit
      Left = 143
      Top = 22
      Width = 124
      Height = 21
      TabOrder = 0
    end
    object edFtpUsername: TEdit
      Left = 143
      Top = 47
      Width = 124
      Height = 21
      TabOrder = 1
    end
    object edFtpPassword: TPasswordEdit
      Left = 143
      Top = 72
      Width = 124
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object cbFtpSavePassword: TCheckBox
      Left = 272
      Top = 74
      Width = 100
      Height = 17
      Caption = 'Save password'
      TabOrder = 3
    end
    object btnTestConnection: TBitBtn
      Left = 427
      Top = 21
      Width = 72
      Height = 25
      Action = Test
      Caption = 'Test'
      TabOrder = 4
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD6666666800
        08DDDD8888888DFFFDDDD666666660BBB0DDD88888888D888FDD666EEEEE70BB
        B0DD888DDDDDDD888FDD66EDDDDDDD0008DD88DDDDDDDDFFFDDD66DD666660BB
        B0DD88DD88888D888FDD66D6666660BBB08D88D888888D888FDD66D66E66760B
        BB0888D88D88D8D888FD66D66D66D660BBB088D88D88D88D888F66D66D60006D
        0BB088D88D8DFFFDD88F66D66660BB000BB088D8888D88FFF88F66DE6660BBBB
        BBB088DD888D8888888D66DDEE7D0BBBBB0D88DDDDDDD88888DD666DDDDD6000
        00DD888DDDDD8DDDDDDDE6666666667DDDDDD888888888DDDDDDDE6666666EDD
        DDDDDD8888888DDDDDDDDDEEEEEEEDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
    end
    object edFTPPort: TEdit
      Left = 143
      Top = 97
      Width = 124
      Height = 21
      TabOrder = 5
    end
    object cbSecureFTP: TCheckBox
      Left = 272
      Top = 25
      Width = 100
      Height = 17
      Caption = 'Secure FTP'
      TabOrder = 6
    end
    object edFolder: TEdit
      Left = 143
      Top = 133
      Width = 124
      Height = 21
      TabOrder = 7
    end
  end
  object Actions: TActionList
    Left = 440
    Top = 72
    object Test: TAction
      Caption = 'Test'
      Hint = 'Test FTP Connection'
      OnExecute = TestExecute
      OnUpdate = TestUpdate
    end
  end
end
