unit PayrollPlansTasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, common,
  evodata, classes, scheduledCustomTask, FtpConnection,
  FtpConnectionParamFrame, evoapiconnectionutils, PayrollPlansProcessing;

type
  TPayrollPlansTaskAdapter = class(TTaskAdapterCustomBase)
  private
    function GetFtpConnectionParam: TFtpConnectionParam;
  published
    procedure UploadEligibilityFile_Execute;
    function UploadEligibilityFile_Describe: string;
    procedure UploadDeductionsFile_Execute;
    function UploadDeductionsFile_Describe: string;
    procedure ProcessEnrollmentFile_Execute;
    function ProcessEnrollmentFile_Describe: string;
  end;


function NewUploadEligibilityFileTask(ClCo: TEvoCompanyDef): IScheduledTask;
function NewUploadDeductionsFileTask(ClCo: TEvoCompanyDef): IScheduledTask;
function NewProcessEnrollmentFileTask(ClCo: TEvoCompanyDef): IScheduledTask;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection,
  gdyclasses, gdyCommon, dialogs,
  gdyDialogEngine, controls, kbmMemTable, userActionHelpers;


function NewUploadEligibilityFileTask(ClCo: TEvoCompanyDef{; EEExportOptions: TEEExportOptions}): IScheduledTask;
begin
  Result := NewTask('UploadEligibilityFile', 'Upload Eligibility file', ClCo);
end;

function NewUploadDeductionsFileTask(ClCo: TEvoCompanyDef{; EEImportOptions: TEEImportOptions}): IScheduledTask;
begin
  Result := NewTask('UploadDeductionsFile', 'Upload Deductions file', ClCo);
end;

function NewProcessEnrollmentFileTask(ClCo: TEvoCompanyDef): IScheduledTask;
begin
  Result := NewTask('ProcessEnrollmentFile', 'Process Enrollment/Deduction file', ClCo);
end;

{
procedure EditEEExportTask(task: IScheduledTask; Owner: TComponent);
var
  dlg: TEEExportDlg;
begin
  dlg := TEEExportDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.EEExportOptionsFrame.Options := LoadEEExportOptions(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveEEExportOptions( dlg.EEExportOptionsFrame.Options, task.ParamSettings, '' );
  finally
    FreeAndNil(dlg);
  end;
end;
}

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);
begin
  if task.Name = 'UploadEligibilityFile' then
    ShowMessage('Upload Eligibility file task has no parameters')
  else if task.Name = 'UploadDeductionsFile' then
    ShowMessage('Upload Deductions file task has no parameters')
  else if task.Name = 'ProcessEnrollmentFile' then
    ShowMessage('Process Enrollment/Deduction file task has no parameters')
  else
    Assert(false);
end;

{ TPayrollPlansTaskAdapter }

function TPayrollPlansTaskAdapter.GetFtpConnectionParam: TFtpConnectionParam;
begin
  Result := LoadFtpConnectionParam(FSettings, '');
end;

function TPayrollPlansTaskAdapter.ProcessEnrollmentFile_Describe: string;
begin
  Result := '';
end;

procedure TPayrollPlansTaskAdapter.ProcessEnrollmentFile_Execute;
begin
  PayrollPlansProcessing.DoDownloadEligibilityChangeFile(FLogger, FTask.Company, GetEvoAPICOnnection, GetFtpConnectionParam, NullProgressIndicator, false);
end;

function TPayrollPlansTaskAdapter.UploadDeductionsFile_Describe: string;
begin
  Result := '';
end;

procedure TPayrollPlansTaskAdapter.UploadDeductionsFile_Execute;
begin
  PayrollPlansProcessing.DoUploadDeductionFile(FLogger, FTask.Company, GetEvoAPICOnnection, GetFtpConnectionParam, NullProgressIndicator, false);
end;

function TPayrollPlansTaskAdapter.UploadEligibilityFile_Describe: string;
begin
  Result := '';
end;

procedure TPayrollPlansTaskAdapter.UploadEligibilityFile_Execute;
begin
  PayrollPlansProcessing.DoUploadEligibilityFile(FLogger, FTask.Company, GetEvoAPICOnnection, GetFtpConnectionParam, NullProgressIndicator, false);
end;

end.
