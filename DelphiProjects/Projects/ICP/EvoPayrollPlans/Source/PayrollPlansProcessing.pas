unit PayrollPlansProcessing;

interface

uses
  Evconsts, evodata, Classes, ScheduledEDFileLoader, XmlRpcTypes,
  gdycommonlogger, PlannedActions, FtpConnection, EeHolder,
  evoapiconnectionutils, gdyClasses, common, kbmMemTable, EvoAPIConnection;

const
  sTmpDir = 'TmpDir';
  sDelimiter = ',';

type
  TEvoCompany = record
    CL_NBR: integer;
    CO_NBR: integer;
    Custom_Company_Number: string
  end;

  TStatisticAmounts = record
    EdProcessed: integer;
    EdUpdated: integer;
    EdInserted: integer;
    EdErrors: integer;
    EdWarnings: integer;
  end;

  TImportProcessing = class
  private
    FLogger: ICommonLogger;
    FCurrentCompany: TEvoCompany;

    FEvoAPI: IEvoAPIConnection;

    FStat: TStatisticAmounts;
    FImportData: TScheduledEDFileLoader;
    EvoCoData: TEvoCoData;

    procedure InitImport;
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;

    function CheckForCompany: boolean;
    function BuildPacket(var aChanges: IRpcArray; aPacketSize: integer = 0): boolean;
    procedure ApplyChangePacket(aChanges: IRpcArray);

    procedure SetSchedDefaultED(var aFields: IRpcStruct; EvoCo: TEvoCoData);
    procedure SetSchedDefault(var aFields: IRpcStruct);

    procedure PostScheduledEDsToEvo;
  public
    constructor Create(logger: ICommonLogger; EvoConn: IEvoAPIConnection);
    destructor Destroy; override;

    procedure ImportScheduledEDs(const aFileName: string);
  end;

  procedure UploadQueryResult(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; const aQueryFile: string; const aResFile: string; aIncludeHeader: boolean = True);

  procedure DoUploadEligibilityFile(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; pi: IProgressIndicator; showGUI: boolean);
  procedure DoUploadDeductionFile(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; pi: IProgressIndicator; showGUI: boolean);
  procedure DoDownloadEligibilityChangeFile(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; pi: IProgressIndicator; showGUI: boolean);

implementation

uses
  sysutils, variants, gdycommon,
  EvoWaitForm, gdyRedir, gdyGlobalWaitIndicator, userActionHelpers,
  XmlRpcCommon, PlannedActionConfirmationForm, forms, dateutils, DB;

procedure UploadQueryResult(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam;
  const aQueryFile: string; const aResFile: string; aIncludeHeader: boolean = True);
var
  ds: TkbmCustomMemTable;
  params: IRpcStruct;
  i: integer;
  sl: TStrings;
  s: string;
  ftp: TFtpConnection;
begin
  try
    Logger.LogEventFmt('Put the query result %s to the file %s', [aQueryFile, aResFile]);
    evoConn.OpenClient( company.ClNbr );
    params := TRpcStruct.Create;
    sl := TStringList.Create;
    try
      params.AddItem('co_nbr', company.CoNbr);
      ds := evoConn.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aQueryFile, params);

      if aIncludeHeader then
      begin
        s := '';
        for i := 0 to ds.FieldCount - 1 do
          s := s + sDelimiter + '"'+ ds.Fields[i].FieldName + '"';
        sl.Add( Copy(s, 2, Length(s)) );
      end;

      ds.First;
      while not ds.Eof do
      begin
        s := '';
        for i := 0 to ds.FieldCount - 1 do
        if (ds.Fields[i] is TDateField) or (ds.Fields[i] is TDateTimeField) then
          s := s + sDelimiter + '"' + FormatDateTime('YYYYMMDD', ds.Fields[i].AsDateTime) + '"'
        else
          s := s + sDelimiter + '"' + ds.Fields[i].AsString + '"';
        sl.Add( Copy(s, 2, Length(s)) );

        ds.Next;
      end;

      s := Redirection.GetDirectory(sTmpDir) + aResFile;
      if FileExists(s) then
        DeleteFile(s);

      sl.SaveToFile(s);

      // Upload file
      ftp := TFtpConnection.Create(FtpParam, Logger);
      try
        ftp.UploadFile(s);
      finally
        FreeAndNil(ftp);
      end;
    finally
      params := nil;
      FreeAndNil(sl);
    end;
  except
    Logger.PassthroughExceptionAndWarnFmt('Getting the query result %s in the file %s failed', [aQueryFile, aResFile]);
  end;
end;

procedure DoUploadEligibilityFile(Logger: ICommonLogger; company: TEvoCompanyDef;
  evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; pi: IProgressIndicator; showGUI: boolean);
var
  opname: string;
begin
  opname := 'Upload Eligibility File (PRDELG.csv)';
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      if showGUI then
        WaitIndicator.StartWait('Build and Upload Eligibility file');
      try
        UploadQueryResult(Logger, company, evoConn, FtpParam, 'PRDELG.rwq', 'PRDELG.csv', False);
      finally
        if showGUI then
          WaitIndicator.EndWait;
      end;

      Logger.LogEventFmt('%s done', [opname]);
//      ReportSyncStat(Logger, stat, opname, showGUI);
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure DoUploadDeductionFile(Logger: ICommonLogger; company: TEvoCompanyDef;
  evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; pi: IProgressIndicator; showGUI: boolean);
var
  opname: string;
begin
  opname := 'Upload Deduction File (PRDED.csv)';
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      if showGUI then
        WaitIndicator.StartWait('Build and Upload Deduction file');
      try
        UploadQueryResult(Logger, company, evoConn, FtpParam, 'PRDED.rwq', 'PRDED.csv', False);
      finally
        if showGUI then
          WaitIndicator.EndWait;
      end;

      Logger.LogEventFmt('%s done', [opname]);
//      ReportSyncStat(Logger, stat, opname, showGUI);
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure DoDownloadEligibilityChangeFile(Logger: ICommonLogger; company: TEvoCompanyDef;
  evoConn: IEvoAPIConnection; FtpParam: TFtpConnectionParam; pi: IProgressIndicator; showGUI: boolean);
var
  s: string;
  ftp: TFtpConnection;
  ImportFromFile: TImportProcessing;
begin
  Logger.LogEntry('Process Eligibility Change File');
  try
    try
      if showGUI then
        WaitIndicator.StartWait('Download Eligibility Change file (ELGCHANGE.csv)');
      try
        s := Redirection.GetDirectory(sTmpDir) + 'ELGCHANGE.csv';
        if FileExists(s) then
          DeleteFile(s);
        ftp := TFtpConnection.Create(FtpParam, Logger);
        try
          ftp.DownloadFile(s);
        finally
          FreeAndNil(ftp);
        end;

        if showGUI then
        begin
          WaitIndicator.EndWait;
          WaitIndicator.StartWait('Import Scheduled E/Ds from ELGCHANGE.csv to Evolution');
        end;

        ImportFromFile := TImportProcessing.Create( Logger, evoConn );
        try
          ImportFromFile.ImportScheduledEDs( s );
        finally
          FreeAndNil(ImportFromFile)
        end;
      finally
        if showGUI then
          WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

{ TImportProcessing }

procedure TImportProcessing.ApplyChangePacket(aChanges: IRpcArray);
begin
  if aChanges.Count > 0 then
  try
    FEvoAPI.applyDataChangePacket( aChanges );
  except
    on e: Exception do begin
      FLogger.LogError('Apply Data Change Packet error!', e.Message);
      FStat.EdErrors := FStat.EdErrors + aChanges.Count;
    end;
  end;
end;

function TImportProcessing.BuildPacket(var aChanges: IRpcArray;
  aPacketSize: integer): boolean;
var
  lRecords, lPacketSize, Ee_Nbr, Cl_E_DS_Nbr, Ee_Scheduled_E_Ds_Nbr: integer;
  RowChange, Fields: IRpcStruct;

begin
  aChanges.Clear;
  lRecords := 0;
  if (aPacketSize = 0) or (aPacketSize > FImportData.RecordCount) then
    lPacketSize := FImportData.RecordCount
  else
    lPacketSize := aPacketSize;

  while (not FImportData.Eof) and (lRecords < lPacketSize) do
  begin
    try
      RowChange := TRpcStruct.Create;
      Fields := TRpcStruct.Create;

      if CheckForCompany then
      begin
        // Get Employee Number
        Ee_Nbr := EvoCoData.GetEmployeeNbr( FImportData.EmployeeCode );
        if Ee_Nbr < 0 then
        begin
          FLogger.LogWarning('Employee: ' + FImportData.EmployeeCode + ' is not found.');
          FStat.EdWarnings := FStat.EdWarnings + 1;
        end
        else begin
          // Get ED Code
          Cl_E_DS_Nbr := EvoCoData.GetClEDsNbr( FImportData.EDCode );
          if Cl_E_DS_Nbr < 0 then
          begin
            FLogger.LogWarning('ED Code: ' + FImportData.EDCode + ' is not found.');
            FStat.EdWarnings := FStat.EdWarnings + 1;
          end
          else begin
            // Try to find Scheduled E/D by Ee_Nbr and Cl_E_Ds_Nbr
            Ee_Scheduled_E_Ds_Nbr := EvoCoData.GetScheduledEDNbr(Ee_Nbr, Cl_E_DS_Nbr);

            if Ee_Scheduled_E_Ds_Nbr < 0 then
            begin // insert
              Fields.AddItem('EE_NBR', Ee_Nbr);
              Fields.AddItem('CL_E_DS_NBR', Cl_E_DS_Nbr);
              Fields.AddItem('AMOUNT', FImportData.Amount);

              SetSchedDefault(Fields);
              SetSchedDefaultED(Fields, EvoCoData);

              RowChange.AddItem('T', 'I');
              FStat.EdInserted := FStat.EdInserted + 1;
            end
            else if FImportData.Amount <> EvoCoData.GetAmount then
            begin
              Fields.AddItem('AMOUNT', FImportData.Amount);
              RowChange.AddItem('T', 'U');
              RowChange.AddItem('K', Ee_Scheduled_E_Ds_Nbr);
              FStat.EdUpdated := FStat.EdUpdated + 1;
            end;

            RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
            RowChange.AddItem('F', Fields);

            if Fields.Count > 0 then
              aChanges.AddItem( RowChange );
          end;
        end;
      end;
    except
      on e: Exception do
      begin
        FLogger.LogError('Build packet error!', e.Message);
        FStat.EdErrors := FStat.EdErrors + 1;
      end;
    end;
    FImportData.Next;
    lRecords := lRecords + 1;
    FStat.EdProcessed := FStat.EdProcessed + 1;
  end;
  Result := FImportData.Eof;
end;

function TImportProcessing.CheckForCompany: boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
  ReOpenCo, ReOpenCl: boolean;

  procedure ProcessCompanyID(const aCompanyID: string);
  begin
    FCurrentCompany.Custom_Company_Number := aCompanyID;
    FCurrentCompany.CO_NBR := -1;

    param := TRpcStruct.Create;
    Param.AddItem( 'CoNumber', FCurrentCompany.Custom_Company_Number );
    FClCoData := RunQuery('TmpCoQuery.rwq', Param);
    try
      if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
      begin
        FCurrentCompany.CO_NBR := FClCoData.FindField('CO_NBR').AsInteger;
        if FCurrentCompany.CL_NBR <> FClCoData.FindField('CL_NBR').AsInteger then
        begin
          FCurrentCompany.CL_NBR := FClCoData.FindField('CL_NBR').AsInteger;
          ReOpenCl := True;
        end;
        ReOpenCo := True;
      end
      else begin
        FLogger.LogWarning('Company "' + FCurrentCompany.Custom_Company_Number + '" has not been found!');
        FStat.EdWarnings := FStat.EdWarnings + 1;
      end;
    finally
      FreeAndNil( FClCoData );
      Result := FCurrentCompany.CO_NBR > -1;
    end;
  end;
begin
  ReOpenCo := False;
  ReOpenCl := False;
  Result := FCurrentCompany.CL_NBR > -1;

  try
    if (FCurrentCompany.Custom_Company_Number <> FImportData.CompanyNbr) then
      ProcessCompanyID(FImportData.CompanyNbr {+ 'S'});

    if ReOpenCl then
    begin
      FEvoAPI.OpenClient(FCurrentCompany.CL_NBR);
      EvoCoData.OpenClientTables;
    end;

    if ReOpenCo then
      EvoCoData.OpenCompanyTables( FCurrentCompany.CO_NBR );
  except
    on e: Exception do
    begin
      FLogger.LogError('Open company error: ' + e.Message);
      FCurrentCompany.CL_NBR := -1;
      FCurrentCompany.CO_NBR := -1;
      FCurrentCompany.Custom_Company_Number := '';
      Result := False;
      FStat.EdErrors := FStat.EdErrors + 1;
    end;
  end;
end;

constructor TImportProcessing.Create(logger: ICommonLogger;
  EvoConn: IEvoAPIConnection);
begin
  FLogger := logger;
  FEvoAPI := EvoConn;
end;

destructor TImportProcessing.Destroy;
begin
  FreeAndNil( FImportData );
  inherited;
end;

procedure TImportProcessing.ImportScheduledEDs(const aFileName: string);
begin
  FImportData := TScheduledEDFileLoader.Create(FLogger, aFileName);
  EvoCoData := TEvoCoData.Create( FLogger, FEvoAPI );

  if FImportData.IsReady and (FImportData.RecordCount > 0) then
    PostScheduledEDsToEvo
  else
    FLogger.LogEvent('No Scheduled E/D data loaded from the file');
end;

procedure TImportProcessing.InitImport;
begin
  FStat.EdProcessed := 0;
  FStat.EdUpdated := 0;
  FStat.EdInserted := 0;
  FStat.EdErrors := 0;
  FStat.EdWarnings := 0;
  FCurrentCompany.CL_NBR := -1;
  FCurrentCompany.CO_NBR := -1;
  FCurrentCompany.Custom_Company_Number := '';
end;

procedure TImportProcessing.PostScheduledEDsToEvo;
var
  Changes: IRpcArray;
begin
  InitImport;
  Changes := TRpcArray.Create;
  FImportData.First;
  try
    while not BuildPacket(Changes, 100) do
      ApplyChangePacket(Changes);

    ApplyChangePacket(Changes);
  finally
    Changes := nil;
  end;
end;

function TImportProcessing.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

procedure TImportProcessing.SetSchedDefault(var aFields: IRpcStruct);
begin
  aFields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
  aFields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
  aFields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
end;

procedure TImportProcessing.SetSchedDefaultED(var aFields: IRpcStruct;
  EvoCo: TEvoCoData);
begin
  aFields.AddItem('CALCULATION_TYPE', EvoCo.CALCULATION_TYPE);
  aFields.AddItem('FREQUENCY', EvoCo.FREQUENCY);

  aFields.AddItem('EXCLUDE_WEEK_1', EvoCo.EXCLUDE_WEEK_1);
  aFields.AddItem('EXCLUDE_WEEK_2', EvoCo.EXCLUDE_WEEK_2);
  aFields.AddItem('EXCLUDE_WEEK_3', EvoCo.EXCLUDE_WEEK_3);
  aFields.AddItem('EXCLUDE_WEEK_4', EvoCo.EXCLUDE_WEEK_4);
  aFields.AddItem('EXCLUDE_WEEK_5', EvoCo.EXCLUDE_WEEK_5);

  aFields.AddItem('ALWAYS_PAY', EvoCo.ALWAYS_PAY);
  aFields.AddItem('DEDUCTIONS_TO_ZERO', EvoCo.DEDUCTIONS_TO_ZERO);
  aFields.AddItem('DEDUCT_WHOLE_CHECK', EvoCo.DEDUCT_WHOLE_CHECK);

  if EvoCo.CL_E_D_GROUPS_NBR > 0 then
    aFields.AddItem('CL_E_D_GROUPS_NBR', EvoCo.CL_E_D_GROUPS_NBR);
  if EvoCo.THRESHOLD_E_D_GROUPS_NBR > 0 then
    aFields.AddItem('THRESHOLD_E_D_GROUPS_NBR', EvoCo.THRESHOLD_E_D_GROUPS_NBR);

  aFields.AddItemDateTime('EFFECTIVE_START_DATE', EvoCo.EFFECTIVE_START_DATE);

  aFields.AddItem('PLAN_TYPE', EvoCo.PLAN_TYPE);
  aFields.AddItem('WHICH_CHECKS', EvoCo.WHICH_CHECKS);
  aFields.AddItem('USE_PENSION_LIMIT', EvoCo.USE_PENSION_LIMIT);
end;

end.

