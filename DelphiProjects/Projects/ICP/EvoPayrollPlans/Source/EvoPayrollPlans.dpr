program EvoPayrollPlans;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  common,
  evoapiconnection,
  jclfileutils,
  gdyredir,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionClCoFrame in '..\..\common\EvolutionClCoFrame.pas' {EvolutionClCoFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  sevenzip in '..\..\common\gdycommon\sevenzip\sevenzip.pas',
  EvoAPIClientNewMainForm in '..\..\common\EvoAPIClientNewMainForm.pas' {EvoAPIClientNewMainFm: TEvoAPIClientMainFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvoWaitForm in '..\..\common\EvoWaitForm.pas' {EvoWaitFm},
  scheduledTask in '..\..\common\scheduledTask.pas',
  PayrollPlansProcessing in 'PayrollPlansProcessing.pas',
  MainForm in 'MainForm.pas' {MainFm: TEvoAPIClientNewMainFm},
  PayrollPlansTasks in 'PayrollPlansTasks.pas',
  FtpConnectionParamFrame in 'FtpConnectionParamFrame.pas' {FtpConnectionParamFrm: TFrame},
  FtpConnection in 'FtpConnection.pas',
  ScheduledEDFileLoader in 'ScheduledEDFileLoader.pas',
  EeHolder in 'EeHolder.pas';

{$R *.res}
begin
//  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License
  LicenseKey := 'B84B2431B89748F4A9D5E3865120D0C0'; // EvoPayrollPlans License

  ConfigVersion := 4;

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  ForceDirectories( Redirection.GetDirectory(sTmpDir) );

  Application.Initialize;
  Application.Title := 'EvoPayrollPlans';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
    Application.Run;
  end
  else begin
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TPayrollPlansTaskAdapter.Create));
  end
end.
