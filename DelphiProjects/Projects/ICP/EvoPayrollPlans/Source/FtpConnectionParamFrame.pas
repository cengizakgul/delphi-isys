unit FtpConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OptionsBaseFrame, StdCtrls, Buttons, PasswordEdit, FtpConnection,
  issettings, ActnList, gdyCommonLogger, SBSimpleSftp;

type
  TFtpConnectionParamFrm = class(TOptionsBaseFrm)
    gbFTPServer: TGroupBox;
    lblAddress: TLabel;
    lblUser: TLabel;
    lblPassword: TLabel;
    lblPortCaption: TLabel;
    edFTPServerAddress: TEdit;
    edFtpUsername: TEdit;
    edFtpPassword: TPasswordEdit;
    cbFtpSavePassword: TCheckBox;
    btnTestConnection: TBitBtn;
    edFTPPort: TEdit;
    cbSecureFTP: TCheckBox;
    Actions: TActionList;
    Test: TAction;
    Label1: TLabel;
    edFolder: TEdit;
    procedure TestUpdate(Sender: TObject);
    procedure TestExecute(Sender: TObject);
  private
    FLogger: ICommonLogger;
    function GetParam: TFtpConnectionParam;
    procedure SetParam(const Value: TFtpConnectionParam);
  public
    property Param: TFtpConnectionParam read GetParam write SetParam;
    property Logger: ICommonLogger read FLogger write FLogger;
    function IsValid: boolean;
    procedure Check;
    procedure ForceSavePassword;
  end;

procedure SaveFtpConnectionParam(param: TFtpConnectionParam; conf: IisSettings; root: string );
function LoadFtpConnectionParam(conf: IisSettings; root: string): TFtpConnectionParam;

implementation

{$R *.dfm}

uses
  cswriter, printers, gdyclasses, strutils, gdycommon, gdyRedir, gdyCrypt,
  gdyUtils, gdyGlobalWaitIndicator;

const
  cKey='D1eg0';

procedure SaveFtpConnectionParam(param: TFtpConnectionParam; conf: IisSettings; root: string );
begin
  root := root + IIF(root='','','\') + 'FtpConnection\';
  conf.AsString[root+'FtpServerAddress'] := param.FtpServerAddress;
  conf.AsInteger[root+'Port'] := param.Port;
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
  conf.AsString[root+'Folder'] := param.Folder;
end;

function LoadFtpConnectionParam(conf: IisSettings; root: string): TFtpConnectionParam;
begin
  root := root + IIF(root='','','\') + 'FtpConnection\';

  Result.FtpServerAddress := conf.AsString[root+'FtpServerAddress'];
  Result.Port := conf.AsInteger[root+'Port'];
  Result.Username := conf.AsString[root+'Username'];
  if conf.GetValueNames(WithoutTrailingSlash(root)).IndexOf('Password') <> -1 then
    Result.Password := conf.AsString[root+'Password']
  else
    Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  Result.Folder := conf.AsString[root+'Folder'];
end;

{ TFtpConnectionParamFrm }

procedure TFtpConnectionParamFrm.Check;
var
  err: string;
  procedure AddErr(s: string);
  begin
    if err <> '' then
      err := err + ',';
    err := err + #13#10 + s;
  end;
begin
  err := '';
  if trim(edFtpServerAddress.Text) = '' then
    AddErr(lblAddress.Caption + ' is not specified');
  if trim(edFtpUsername.Text) = '' then
    AddErr(lblUser.Caption + ' is not specified');
  if trim(edFtpPassword.Password) = '' then
    AddErr(lblPassword.Caption + ' is not specified');
  if err <> '' then
    raise Exception.Create('Invalid FTP connection settings:'+err);
end;

procedure TFtpConnectionParamFrm.ForceSavePassword;
begin
  cbFtpSavePassword.Checked := true;
end;

function TFtpConnectionParamFrm.GetParam: TFtpConnectionParam;
var
  port: integer;
begin
  Result.FtpServerAddress := Trim(edFTPServerAddress.Text);
  Result.Username := Trim(edFtpUsername.Text);
  Result.Password := edFtpPassword.Password;
  Result.SavePassword := cbFtpSavePassword.Checked;
  Result.FtpConnectionType := ftcFtpS;
  if TryStrToInt(edFTPPort.Text, port) then
    Result.Port := port
  else
    Result.Port := 22;
  Result.Folder := Trim(edFolder.Text);  
end;

function TFtpConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(edFTPServerAddress.Text) <> '') and
            (trim(edFtpUsername.Text) <> '') and
            (edFtpPassword.Password <> '');
end;

procedure TFtpConnectionParamFrm.SetParam(
  const Value: TFtpConnectionParam);
begin
  edFTPServerAddress.Text := Value.FtpServerAddress;
  edFTPPort.Text := IntToStr( Value.Port );
  edFtpUsername.Text := Value.Username;
  edFtpPassword.Password := Value.Password;
  cbFtpSavePassword.Checked := Value.SavePassword;
  edFolder.Text := Value.Folder;
//  cbSecureFTP.Checked := Value.FtpConnectionType;
end;

procedure TFtpConnectionParamFrm.TestUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := IsValid;
end;

procedure TFtpConnectionParamFrm.TestExecute(Sender: TObject);
var
  ftp: TFtpConnection;
begin
  WaitIndicator.StartWait('Testing FTP connection');
  ftp := TFtpConnection.Create( Param, FLogger);
  try
    if ftp.TestConnection then
      ShowMessage('FTP Connection test successful!')
    else
      ShowMessage('FTP Connection test fail!');
  finally
    FreeAndNil(ftp);
    WaitIndicator.EndWait;
  end;
end;

end.
