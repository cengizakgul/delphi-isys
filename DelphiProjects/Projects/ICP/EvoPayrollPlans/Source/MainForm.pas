unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientNewMainForm, ActnList,
  EvolutionCompanySelectorFrame, StdCtrls, Buttons, ComCtrls,
  SmtpConfigFrame, SchedulerFrame, ExtCtrls, OptionsBaseFrame,
  EvoAPIConnectionParamFrame, scheduledTask,
  evoapiconnectionutils, common, FtpConnectionParamFrame, PayrollPlansTasks;

type
  TMainFm = class(TEvoAPIClientNewMainFm)
    GroupBox1: TGroupBox;
    actUploadDeductionFile: TAction;
    actProcessEnrollmentFile: TAction;
    actScheduleElgFile: TAction;
    actScheduleDeductionFile: TAction;
    actUploadEligibilityFile: TAction;
    actScheduleEnrollmentFile: TAction;
    pnlSpace: TPanel;
    FtpFrame: TFtpConnectionParamFrm;
    gbJustFrame: TGroupBox;
    btnUploadEligibilityfile: TBitBtn;
    btnUploadDeductionFile: TBitBtn;
    btnEnrollmentFile: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn7: TBitBtn;
    procedure actUploadDeductionFileExecute(Sender: TObject);
    procedure actNeedExtAppAndCoUpdate(Sender: TObject);
    procedure actProcessEnrollmentFileExecute(Sender: TObject);
    procedure actScheduleElgFileExecute(Sender: TObject);
    procedure actScheduleDeductionFileExecute(Sender: TObject);
    procedure actUploadEligibilityFileExecute(Sender: TObject);
    procedure actScheduleEnrollmentFileExecute(Sender: TObject);
    procedure actUploadEligibilityFileUpdate(Sender: TObject);
  private
    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleFtpConnectionParamChangedByUser(Sender: TObject);

    procedure ForceSavePasswords;
  protected
    procedure UnInitPerCompanySettings; override;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef); override;
  public
    constructor Create( Owner: TComponent ); override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  PayrollPlansProcessing, userActionHelpers, 
  evodata, PlannedActionConfirmationForm, gdyGlobalWaitIndicator,
  gdyClasses, EvoWaitForm, EvoAPIClientMainForm, gdyDialogEngine;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
  try
    SchedulerFrame.Configure(Logger, TPayrollPlansTaskAdapter.Create, HandleEditParameters);
  except
    Logger.StopException;
  end;

  FtpFrame.Logger := Logger;
  FtpFrame.Param := LoadFtpConnectionParam( FSettings, '' );
  FtpFrame.OnChangeByUser := HandleFtpConnectionParamChangedByUser;

  if EvoFrame.IsValid and FtpFrame.IsValid then
    PageControl1.ActivePageIndex := 2
  else
    PageControl1.ActivePageIndex := 0;
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  PayrollPlansTasks.EditTask(task, Logger, Self);
end;

procedure TMainFm.actUploadDeductionFileExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      DoUploadDeductionFile(Logger, FEvoData.GetClCo, FEvoData.Connection, FtpFrame.Param, Self as IProgressIndicator, true);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actNeedExtAppAndCoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and FtpFrame.IsValid and FEvoData.CanGetClCo;
end;

procedure TMainFm.actProcessEnrollmentFileExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      DoDownloadEligibilityChangeFile(Logger, FEvoData.GetClCo, FEvoData.Connection, FtpFrame.Param, Self as IProgressIndicator, true);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.ForceSavePasswords;
begin
  EvoFrame.ForceSavePassword;
  FtpFrame.ForceSavePassword;
end;

procedure TMainFm.actScheduleElgFileExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewUploadEligibilityFileTask(FEvoData.GetClCo) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleDeductionFileExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewUploadDeductionsFileTask(FEvoData.GetClCo) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;


procedure TMainFm.actUploadEligibilityFileExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      DoUploadEligibilityFile(Logger, FEvoData.GetClCo, FEvoData.Connection, FtpFrame.Param, Self as IProgressIndicator, true);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleEnrollmentFileExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewProcessEnrollmentFileTask(FEvoData.GetClCo) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleFtpConnectionParamChangedByUser(Sender: TObject);
begin
  try
    SaveFtpConnectionParam( FtpFrame.Param, FSettings, '');
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.actUploadEligibilityFileUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and FtpFrame.IsValid and FEvoData.CanGetClCo;
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  inherited;

end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  inherited;

end;

end.
