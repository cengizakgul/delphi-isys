inherited MainFm: TMainFm
  Left = 376
  Top = 139
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabSheet3: TTabSheet
      object pnlSpace: TPanel
        Left = 0
        Top = 106
        Width = 784
        Height = 5
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
      inline FtpFrame: TFtpConnectionParamFrm
        Left = 0
        Top = 111
        Width = 784
        Height = 194
        Align = alTop
        TabOrder = 2
        inherited gbFTPServer: TGroupBox
          Width = 784
          Height = 194
          inherited Label1: TLabel
            Top = 162
          end
          inherited cbFtpSavePassword: TCheckBox
            Width = 273
            Caption = 'Save password (mandatory for scheduling tasks)'
          end
          inherited btnTestConnection: TBitBtn
            Left = 195
            Top = 124
            Height = 24
          end
          inherited cbSecureFTP: TCheckBox
            Caption = 'Use FTPS'
            Visible = False
          end
          inherited edFolder: TEdit
            Top = 157
          end
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      TabVisible = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 89
        Align = alTop
        Caption = 'EvoPayrollPlans connection'
        TabOrder = 0
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Files'
      inherited pnlBottom: TPanel
        Top = 427
        Height = 1
      end
      object gbJustFrame: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 427
        Align = alClient
        TabOrder = 1
        object btnUploadEligibilityfile: TBitBtn
          Left = 23
          Top = 26
          Width = 256
          Height = 28
          Action = actUploadEligibilityFile
          Caption = 'Generate and upload Eligibility File'
          TabOrder = 0
        end
        object btnUploadDeductionFile: TBitBtn
          Left = 23
          Top = 108
          Width = 256
          Height = 28
          Action = actUploadDeductionFile
          Caption = 'Generate and upload Deduction File'
          TabOrder = 1
        end
        object btnEnrollmentFile: TBitBtn
          Left = 23
          Top = 67
          Width = 256
          Height = 28
          Action = actProcessEnrollmentFile
          Caption = 'Download and process Enrollment/Deduction file'
          TabOrder = 2
        end
        object BitBtn5: TBitBtn
          Left = 320
          Top = 108
          Width = 97
          Height = 28
          Action = actScheduleDeductionFile
          Caption = 'Create task'
          TabOrder = 3
        end
        object BitBtn3: TBitBtn
          Left = 320
          Top = 67
          Width = 97
          Height = 28
          Action = actScheduleEnrollmentFile
          Caption = 'Create task'
          TabOrder = 4
        end
        object BitBtn7: TBitBtn
          Left = 320
          Top = 26
          Width = 97
          Height = 28
          Action = actScheduleElgFile
          Caption = 'Create task'
          TabOrder = 5
        end
      end
    end
    inherited tbshSchedulerSettings: TTabSheet
      inherited SmtpConfigFrame: TSmtpConfigFrm
        Width = 776
        inherited GroupBox1: TGroupBox
          Width = 776
        end
      end
    end
  end
  inherited pnlCompany: TPanel
    inherited Panel1: TPanel
      Width = 99
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 99
      Width = 693
    end
  end
  inherited ActionList1: TActionList
    Left = 516
    Top = 88
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    object actUploadDeductionFile: TAction
      Caption = 'Generate and upload Deduction File'
      OnExecute = actUploadDeductionFileExecute
      OnUpdate = actUploadEligibilityFileUpdate
    end
    object actProcessEnrollmentFile: TAction
      Caption = 'Download and process Enrollment/Deduction file'
      OnExecute = actProcessEnrollmentFileExecute
      OnUpdate = actUploadEligibilityFileUpdate
    end
    object actScheduleElgFile: TAction
      Caption = 'Create task'
      OnExecute = actScheduleElgFileExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
    object actScheduleDeductionFile: TAction
      Caption = 'Create task'
      OnExecute = actScheduleDeductionFileExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
    object actUploadEligibilityFile: TAction
      Caption = 'Generate and upload Eligibility File'
      OnExecute = actUploadEligibilityFileExecute
      OnUpdate = actUploadEligibilityFileUpdate
    end
    object actScheduleEnrollmentFile: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEnrollmentFileExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
  end
end
