unit FtpConnection;

interface

uses
  Sysutils, gdycommonlogger, ftpsend, ssl_openssl;

type
  TFtpType = (ftcFtp, ftcSFtp, ftcFtpS);

  TFtpConnectionParam = record
    FtpServerAddress: string;
    Username: string;
    Password: string;
    Port: integer;
    SavePassword: boolean;
    FtpConnectionType: TftpType;
    Folder: string;
  end;

  TFtpConnection = class
  private
    FMyFtpS: TFTPSend;
    FLogger: ICommonLogger;
    FFolder: string;
  public
    constructor Create(const param: TFtpConnectionParam; logger: ICommonLogger);
    destructor Destroy; override;

    function TestConnection: boolean;
    function UploadFile(const aFileName: string): boolean;
    function DownloadFile(const aFileName: string): boolean;
  end;

implementation

{ TFtpConnection }

constructor TFtpConnection.Create(const param: TFtpConnectionParam;
  logger: ICommonLogger);
begin
  FLogger := logger;
  FFolder := Trim(param.Folder);
  if FFolder <> '' then
    FFolder := FFolder + '\';

  FMyFtpS := TFTPSend.Create();
  FMyFtpS.TargetHost := Param.FtpServerAddress;
  FMyFtpS.UserName := Param.Username;
  FMyFtpS.Password := Param.Password;
  FMyFtpS.TargetPort := IntToStr(Param.Port);
  FMyFtpS.FullSSL := True;
end;

destructor TFtpConnection.Destroy;
begin
  FreeAndNil(FMyFtpS);

  inherited;
end;

function TFtpConnection.DownloadFile(const aFileName: string): boolean;
begin
  Result := False;
  FLogger.LogEntry('Download File');
  try
    FLogger.LogEventFmt('File: %s', [aFileName]);
    try
      Result := FMyFtpS.Login;
      if Result then
      begin
        FMyFtpS.RetrieveFile( FFolder + ExtractFileName(aFileName), False );
        FLogger.LogDebug('Download file');
        if FileExists( aFileName ) then
          DeleteFile( aFileName );
        FMyFtpS.DataStream.SaveToFile( aFileName );
        FLogger.LogEvent('Done');
        Result := True;
      end
      else
        FLogger.LogError('FTP login failed');
    except
      FLogger.PassthroughExceptionAndWarnFmt('Upload %s failed', [ExtractFileName(aFileName)]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TFtpConnection.TestConnection: boolean;
begin
  Result := False;
  try
{    if FtpType = ftcSFtp then
    begin
      FMySFtp.Open;
      FMySFtp.Close;
      Result := True;
    end
    else if FtpType = ftcFtp then
    begin
      FMyFtp.Connect;
      FMyFtp.Disconnect;
      Result := True;
    end
    else begin}
      Result := FMyFtpS.Login;
//    end;

    if Result then
      FLogger.LogEvent('Connected to FTP Server')
    else
      FLogger.LogEvent('Unable to connect to FTP Server');
  except
    on E: Exception do
      FLogger.LogError('Connection failed!', E.Message);
  end;
end;

function TFtpConnection.UploadFile(const aFileName: string): boolean;
begin
  Result := False;
  FLogger.LogEntry('Upload File');
  try
    FLogger.LogEventFmt('File: %s', [aFileName]);
    try
      Result := FMyFtpS.Login;
      if Result then
      begin
        FMyFtpS.DataStream.LoadFromFile( aFileName );
        FLogger.LogDebug('Load to stream');
        FMyFtpS.StoreFile( FFolder + ExtractFileName(aFileName), false );
        FLogger.LogEvent('Done');
        Result := True;
      end
      else
        FLogger.LogError('FTP login failed');
    except
      FLogger.PassthroughExceptionAndWarnFmt('Upload %s failed', [ExtractFileName(aFileName)]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

end.
