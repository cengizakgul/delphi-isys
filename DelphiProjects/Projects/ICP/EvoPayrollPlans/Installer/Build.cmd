@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoPayrollPlans.msi 
"%pWiXDir%\candle.exe" .\EvoPayrollPlans.wxs -wx -out ..\..\..\..\..\Tmp\EvoPayrollPlans.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoPayrollPlans.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoPayrollPlans.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoPayrollPlans.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoPayrollPlans%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoPayrollPlans%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoPayrollPlans.msi EvoPayrollPlans%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
