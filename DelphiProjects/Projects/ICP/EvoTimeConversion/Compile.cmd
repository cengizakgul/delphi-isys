@ECHO OFF


REM Parameters: 
REM    1. Version(optional)
REM    2. Flags: (R)elease or (D)ebug (optional)

SET pAppVersion=%1
IF "%pAppVersion%" == "" GOTO compile

SET pCompileOptions=-DPRODUCTION_LICENCE
IF "%2" == "R" SET pCompileOptions=%pCompileOptions% -DFINAL_RELEASE

:compile
ECHO *Compiling
..\..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion% "%pCompileOptions%"

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.exe) DO ..\..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL

REM XCOPY /Y /Q .\Source\*.mes ..\..\..\Bin\ICP\%pAppName%\*.* > NUL
FOR %%a IN (..\..\..\Bin\ICP\EvoTimeConversion\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\EvoTimeConversion\*.mes

FOR %%a IN (..\..\..\Bin\ICP\EvoTimeConversion\*.exe) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoTimeConversion\*.dll) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL

REM FOR %%a IN (..\..\..\Bin\ICP\EvoTimeConversion\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\EvoTimeConversion\*.mes
FOR %%a IN (..\..\..\Bin\ICP\EvoTimeConversion\*.map) DO DEL /F /Q ..\..\..\Bin\ICP\EvoTimeConversion\*.map

GOTO end

:error
ECHO *ERROR DETECTED
IF "%pAppVersion%" == "" PAUSE

EXIT 1

:end
ECHO SUCCESS COMPILE.CMD 
IF "%pAppVersion%" == "" PAUSE
