@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoTimeConversion.msi 
"%pWiXDir%\candle.exe" .\EvoTimeConversion.wxs -wx -out ..\..\..\..\..\Tmp\EvoTimeConversion.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoTimeConversion.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoTimeConversion.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoTimeConversion.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoTimeConversion%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoTimeConversion%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoTimeConversion.msi EvoTimeConversion%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
