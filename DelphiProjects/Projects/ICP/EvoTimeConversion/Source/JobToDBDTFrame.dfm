inherited JobToDBDTFrm: TJobToDBDTFrm
  Width = 747
  Height = 513
  inherited BinderFrm1: TBinderFrm
    Top = 65
    Width = 747
    Height = 448
    OnResize = BinderFrm1Resize
    inherited evSplitter2: TSplitter
      Top = 211
      Width = 747
    end
    inherited pnltop: TPanel
      Width = 747
      Height = 211
      inherited evSplitter1: TSplitter
        Left = 153
        Height = 211
      end
      inherited pnlTopLeft: TPanel
        Width = 153
        Height = 211
        inherited evPanel3: TPanel
          Width = 153
        end
        inherited dgLeft: TReDBGrid
          Width = 153
          Height = 186
        end
      end
      inherited pnlTopRight: TPanel
        Left = 158
        Width = 589
        Height = 211
        inherited evPanel4: TPanel
          Width = 589
        end
        inherited dgRight: TReDBGrid
          Width = 589
          Height = 186
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 216
      Width = 747
      Height = 232
      inherited pnlbottom: TPanel
        Width = 747
        Height = 191
        inherited evPanel5: TPanel
          Width = 747
        end
        inherited dgBottom: TReDBGrid
          Width = 747
          Height = 166
        end
      end
      inherited pnlMiddle: TPanel
        Width = 747
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 747
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      747
      65)
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Map Aloha jobs to'
    end
    object Label2: TLabel
      Left = 208
      Top = 8
      Width = 29
      Height = 13
      Caption = 'codes'
    end
    object Label3: TLabel
      Left = 8
      Top = 32
      Width = 720
      Height = 26
      Anchors = [akLeft, akTop, akRight]
      Caption = 
        'If Aloha jobs are not mapped to the lowest level of the D/B/D/T ' +
        'hierarchy then codes of employee home D/B/D/Ts will be used to f' +
        'ill in levels below the selected one. If "(None)" is selected th' +
        'en check line'#39's D/B/D/Ts are left blank.'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      WordWrap = True
    end
    object cbLevel: TComboBox
      Left = 104
      Top = 5
      Width = 97
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = '(None)'
      OnSelect = cbLevelSelect
      Items.Strings = (
        '(None)')
    end
  end
  object cdAlohaJobCodes: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdAlohaJobCodesCODE: TStringField
      DisplayLabel = 'Aloha job'
      FieldName = 'CODE'
      Size = 6
    end
  end
end
