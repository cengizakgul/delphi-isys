unit alohaparser;

interface

uses
  gdyCommonLogger;

type
  TAlohaDataItem = record
    EECode: string;
    SSN: Variant;
    JobCode: Variant;
    ShiftCode: Variant;
    PayRate: Variant;
    RegularHours: Variant;
    OTHours: Variant;
    DeclaredTips: Variant;
    CashTips: Variant;
    Sales: Variant;
    CreditCardTips: Variant;
  end;

  PAlohaDataItem = ^TAlohaDataItem;

  TAlohaBatch = record
    CompanyCode: string;
    BatchID: string;
    SourceFileName: string;
    Data: array of TAlohaDataItem;
  end;

function LoadAlohaBatch(logger: ICommonLogger; fn: string): TAlohaBatch;

type
  TAlohaFieldType = (
    alohaEECode = 3,
    alohaPayCode = 4,
    alohaSSN = 5,
    alohaJobCode = 8,
    alohaShiftCode = 10,
    alohaPayRate = 11,

    alohaRegularHours = 12,
    alohaOvertimeHours = 13,
    alohaDeclaredTips = 20,
    alohaCashTips = 21,
    alohaSales = 28,
    alohaCreditCardTips = 29);

function GetAlohaFieldName(fieldType: TAlohaFieldType): string;

implementation

uses
  gdycommon, SysUtils, gdyclasses, StrUtils, gdystrset;

type
  TStringArray = array of string;

  TADPField = record
    ID: integer;
    Value: string;
  end;

function ParseADPFile(logger: ICommonLogger; content: string): TStringArray;
var
  lines: IStr;
  i: integer;
  s: string;
  rec: string;
  p: integer;
  padding: string;
begin
  lines := SplitToLines( content );
  while (lines.Count > 0) and (trim(lines.Str[lines.Count-1]) = '') do
    lines.Delete(lines.Count-1);

  if lines.Count = 0  then
    raise Exception.Create('Aloha export file is empty');

  Result := nil;
  i := 0;
  p := 0; //to make compiler happy
  while i < lines.Count do
  begin
    rec := '';
    while i < lines.Count do
    begin
      s := lines.Str[i];
      if Length(s) <> 80 then
        raise Exception.CreateFmt('Expected line length to be 80, got %d', [Length(s)]);
      inc(i);
      p := Pos('z', s);
      if p > 0 then
      begin
        rec := rec + copy(s, 1, p-1); //don't copy 'z'
        padding := copy(s, p+1, 80-p - ord(i=1)); //i already incremented
        if trim(padding) <> '' then
          raise Exception.CreateFmt('Expected spaces after end of logical record but got "%s"', [padding]);
        break;
      end
      else
        rec := rec + s;
    end;
    if p = 0 then
      raise Exception.Create('Marker of the end of logical record expected, but end of file found');
    if Length(rec) = 0 then
      raise Exception.Create('Logical record is empty');
    SetLength(Result, Length(Result)+1);
    Result[high(Result)] := rec;
  end;
end;

function TrimLeadingZeroes(s: string): string;
begin
  Result := s;
  while (Length(Result) > 0) and (Result[1] = '0') do
    Result := RightStr(Result, Length(Result)-1);
end;

function ParseAlohaBatch(logger: ICommonLogger; content: string): TAlohaBatch;
var
  i: integer;
  recs: TStringArray;
  tail: string;
  fetched: TStringSet;
  allFetched: TStringSet;
  data: PAlohaDataItem;

  function Fetch(len: integer): string;
  begin
    if Length(tail) < 3+len then
      raise Exception.CreateFmt('Expected field of length %d but there is only %d symbols left in the record', [len, Length(tail)-3]);
    if InSet(LeftStr(tail,3), fetched) then
      raise Exception.CreateFmt('Field %s occured twice in a record', [LeftStr(tail,3)]);
    SetInclude(fetched, LeftStr(tail,3));
    SetInclude(allFetched, LeftStr(tail,3));
    Result := Copy(tail, 4, len);
    tail := RightStr(tail, Length(tail)-3-len);
    logger.LogDebug( Format('Fetched (%d): %s', [len, Result]));
  end;

  function FetchPrefixed(prefix: string; len: integer; ft: TAlohaFieldType): string;
  var
    actual: string;
  begin
    actual := Fetch(Length(prefix) + len);

    if LeftStr(actual,Length(prefix)) <> prefix then
      raise Exception.CreateFmt('Expected <%s> but got <%s> in the %s field', [prefix, LeftStr(actual,Length(prefix)), GetAlohaFieldName(ft)]);

    Result := Copy(actual, Length(prefix)+1, len);
  end;

var
  ft: TAlohaFieldType;
begin
  logger.LogEntry('Parsing Aloha export file');
  try
    try
      logger.LogDebug('Aloha export file content', content);

      recs := ParseADPFile(logger, content);
      if recs[0][1] <> 'A' then
        raise Exception.CreateFmt('Expected batch header record but got <%s> record', [copy(recs[0],1,1)]);

      tail := RightStr(recs[0], Length(recs[0])-1 );
      if LeftStr(tail, 3) <> '001' then
        raise Exception.CreateFmt('Expected ''001'' at the beginning of header record but got %s', [LeftStr(tail,3)]);
      Result.CompanyCode := Fetch(3);
      if LeftStr(tail, 3) <> '002' then
        raise Exception.CreateFmt('Expected ''002'' after the ''001'' field in the header record but got %s', [LeftStr(tail, 3)]);
      Result.BatchID := Fetch(2);

      SetLength(Result.Data, 0);
      i := 1;
      allFetched := nil;
      while (i <= high(recs)) and (recs[i][1] = 'B') do
      begin
        logger.LogEntry('Parsing Aloha record');
        try
          try
            logger.LogContextItem('Record', recs[i]);
            SetLength(Result.Data, Length(Result.Data)+1);
            data := @Result.Data[high(Result.Data)];
            fetched := nil;

            tail := RightStr(recs[i], Length(recs[i])-1 );
            while Length(tail) > 0 do
            begin
              if Length(tail) < 3 then
                raise Exception.Create('Expected 3-digit field code');

              logger.LogDebug('Tail: <'+ tail+'>');
              ft := TAlohaFieldType(StrToInt(LeftStr(tail, 3)));
              case ft of
               alohaEECode:         data.EECode := TrimLeadingZeroes(Fetch(6));
               alohaPayCode:        Fetch(1);//Pay Code, This is always 1.
               alohaSSN:            data.SSN := Fetch(15);
               alohaJobCode:        data.JobCode := TrimLeadingZeroes(Fetch(6));
               alohaShiftCode:      data.ShiftCode := Fetch(1);
               alohaPayRate:        data.PayRate := Fetch(8);
               alohaRegularHours:   data.RegularHours := Fetch(8);
               alohaOvertimeHours:  data.OTHours := Fetch(8);
               alohaDeclaredTips:   data.DeclaredTips := FetchPrefixed(' T', 8, ft);
               alohaCashTips:       data.CashTips := FetchPrefixed(' G', 8, ft);
               alohaSales:          data.Sales := FetchPrefixed('192 ', 8, ft); //v6, in v4 it is '192'
               alohaCreditCardTips: data.CreditCardTips := FetchPrefixed('195 ', 8, ft);
              else
                raise Exception.CreateFmt('Unknown field: %s', [LeftStr(tail, 3)]);
              end;
            end;

          except
            logger.PassthroughException;
          end
        finally
          logger.LogExit;
        end;
        inc(i);
      end;
      if i > high(recs) then
        raise Exception.Create('Expected batch total record but end of file found');
      if recs[i][1] <> 'C' then
        raise Exception.CreateFmt('Expected batch total record after data records but <%s> record found', [LeftStr(recs[i],1)]);
      if i <> high(recs) then
        raise Exception.CreateFmt('Expected end of file after batch total record but <%s> record found', [LeftStr(recs[i+1],1)]);
    //  logger.LogEvent('Fields: '+ SetToStr(allFetched, ', '));
    except
      logger.PassthroughException;
    end
  finally
    logger.LogExit;
  end;
end;

function LoadAlohaBatch(logger: ICommonLogger; fn: string): TAlohaBatch;
var
  ext: string;
begin
  logger.LogEntry('Loading Aloha export file');
  try
    try
      logger.LogContextItem('Aloha export file', fn);
      Result := ParseAlohaBatch( logger, FileToString(fn) );
      Result.SourceFileName := fn;
      ext := trim(ExtractFileExt(fn));
      ext := Copy(ext, 2, Length(ext)-1);
      if ext <> trim(Result.CompanyCode) then
        Logger.LogWarningFmt('Company code in the batch header record (%s) doesn''t match file extention (%s)', [trim(Result.CompanyCode), ext]);
    except
      logger.PassthroughException;
    end
  finally
    logger.LogExit;
  end;
end;

function GetAlohaFieldName(fieldType: TAlohaFieldType): string;
begin
  case fieldType of
    alohaEECode:         Result := 'EE Code';
    alohaPayCode:        Result := 'Pay Code';
    alohaSSN:            Result := 'SSN';
    alohaJobCode:        Result := 'Job Code';
    alohaShiftCode:      Result := 'Shift Code';
    alohaPayRate:        Result := 'Pay Rate';
    alohaRegularHours:   Result := 'Regular Hours';
    alohaOvertimeHours:  Result := 'Overtime Hours';
    alohaDeclaredTips:   Result := 'Declared Tips';
    alohaCashTips:       Result := 'Cash Tips';
    alohaSales:          Result := 'Sales';
    alohaCreditCardTips: Result := 'Credit Card Tips';
  else
    Assert(false);
  end;
end;

end.


