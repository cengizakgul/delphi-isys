unit EvoCsvTransformMapperClasses;

interface
uses
  SysUtils
 ,Classes
 ,Windows
 ,Messages,  Variants,  Graphics, Controls, Forms
 ,Dialogs
 ,StdCtrls
 ,ExtCtrls
 ,xmldom, XMLIntf, msxmldom, XMLDoc
 ,EvoCsvTransformMapper
 ,VarHelperFunctions
  ;

type


  TeabAlgorithmClass=class(   TeaAlgorithmClass
                            , IEACSV_ColOut_Mapping
                            , IEACSV_LookupTable_Mapping
                            , IEaCSVLineProducer
                            , IEaCSVHorzFieldValueArbitrator
                            )
  private
    FPanel:TPanel;
    function CheckMappingPopulation: string;
    function GetFirstChildValue(node: IXmlNode;
      const childNodeName: string): string;
    procedure SetXMLEDMappingValue(indexstring:string; const Field, Value:string);
    function GetGlobalInputIDFromName(InputFieldName: string): string;

  protected

    function InputFieldConsistencyError():string;
    function GetColOutCodesMappingNamesValues(indexString:string):string;

    procedure PopulateUniqueSortedPairList(
                            const FieldNodesList: IXMLNodeList; SortedList,
                            errorList: TStringList; const AttId, AttName: string
                                          );
    procedure CheckFieldConsistency(
                            errorList: TStringList; const nodeName,
                            AttrId, AttrName: string
                                   );
    {IEACSV_ColOut_Mapping interface}
    function getColumnOutMapping(index:string):string;            virtual;
    procedure setColumnOutMapping(index:string; Value:string);    virtual;
    {end of IEACSV_ColOut_Mapping interface}

    {IEACSV_LookupTable_Mapping interface}
    function getLookupTableMapping(index:string):string;           virtual;
    procedure setLookupTableMapping(index:string; Value:string);   virtual;
    {end of IEACSV_LookupTable_Mapping interface}

   {IEaCSVLineProducer interface}
    function ProducersCount : integer; virtual;
    function  getProducer(index : integer):string; virtual;
    procedure setProducer(index :integer; ListText:string); virtual;
   {end ofIEaCSVLineProducer interface}


   {IEaCSVHorzFieldValueArbitrator}
    function GetHorzFieldValueArbitrator(indexstring:string):string; virtual;
    procedure SetHorzFieldValueArbitrator(indexstring:string; IntakeNameEqIdCrLf:string); virtual;
   {end of IEaCSVHorzFieldValueArbitrator}
    procedure SetEDMapList(indexString:string; const Value: TStrings); virtual;

  public

    constructor Create(Aowner:TComponent); override;
    destructor Destroy(); override;
    function AlgorithmValidationError():string; override;
    function ColumnMapping:IEACSV_ColOut_Mapping; override;
    function LookupTableMapping:IEACSV_LookupTable_Mapping; override;
    function Producer: IEaCSVLineProducer; override;
    function HorzFieldValueArbitrator: IEaCSVHorzFieldValueArbitrator; override;
  end;




implementation



{ TeabAlgorithmClass }


constructor TeabAlgorithmClass.Create(Aowner:TComponent);
begin
   inherited ;
   FPanel:=TPanel.Create(Self);
   Self.XMLDocument:=TXMLDocument.Create(FPanel);

end;

destructor TeabAlgorithmClass.Destroy;
begin
//  FPanel.Free;
  inherited;
end;


function TeabAlgorithmClass.AlgorithmValidationError: string;
begin
// ColumnMapping tables should not be empty
// there may be ColumnMapping tables as Lookup tables
// and ColumnMapping tables for fields
//
// verification for both
//
// output is error message containing most complete information
//
  result:=InputFieldConsistencyError();

end;




procedure TeabAlgorithmClass.PopulateUniqueSortedPairList(
                                        const FieldNodesList:IXMLNodeList;
                                        SortedList:TStringList;
                                        errorList:TStringList;
                                        const AttId:string;
                                        const AttName:string
                                                    );
var

  i:integer;
begin

    Sortedlist.Clear;
    SortedList.Duplicates:=dupIgnore ;
    SortedList.CaseSensitive:= false;
    SortedList.Sorted:=true;

    for i := 0 to FieldNodesList.Count-1 do
    begin
       if VarIsNull( FieldNodesList[i].Attributes[AttId]) then
          errorList.Add('Missed '+AttId+' attribute :'+FieldNodesList[i].XML)
       else
       if VarIsnull( FieldNodesList[i].Attributes[AttName]) then
          errorList.Add('Missed '+AttName+' attribute :'
            +FieldNodesList[i].XML)
       else
       if NOT checkVarInt( FieldNodesList[i].Attributes[AttId]) then
            errorList.Add('Not integer '+AttId+' attribute : ' +FieldNodesList[i].XML)
       else
       begin
           SortedList.Add( FieldNodesList[i].Attributes[AttId]+'='+ FieldNodesList[i].Attributes[AttName]);
       end;
    end;
end;

procedure TeabAlgorithmClass.CheckFieldConsistency(errorList:TStringList; const nodeName, AttrId, AttrName:string);
var
  queryNode     : IXMLNode;
  aSList:TStringList;
  inputFieldNodes: IXMLNodeList;
  i:integer;
begin
  aSList:=TStringList.Create;
  try

    if NOT Self.XMLDocument.Active then Self.XMLDocument.Active:=true;

    queryNode:= Self.XMLDocument.DocumentElement;
    inputFieldNodes := SelectNodes( queryNode, WideString('//'+nodeName));

    Self.PopulateUniqueSortedPairList( inputFieldNodes, aSList, errorList, AttrId, AttrName);

    if aSList.Count>1 then
      for i := 1 to aSList.Count-1 do
      begin
         if CompareText(aSList.Names[i], aSList.Names[i-1]) = 0 then
         begin
          // duplicate index attr in unique name-list
          errorList.Add(nodeName+' fields has mismatched field names : "'+aSList[i-1]+'"<->"'+aSList[i]+'"');
         end;
      end;
  finally
    aSList.Free;
  end;
end;

function TeabAlgorithmClass.CheckMappingPopulation:string;
begin
  // ColumnMapping should verify fields
  Result:='';
end;

function TeabAlgorithmClass.InputFieldConsistencyError: string;
var
  errorList:TStringlist;
begin
  Result:='';
  errorList:=TStringList.Create;
  try

    CheckFieldConsistency(errorList,'input_field', 'input_id', 'input_name');
    CheckFieldConsistency(errorList,'output_field', 'output_id', 'output_name');
    Result:=Trim(errorList.Text);
    Result:=Result+CheckMappingPopulation();

  finally

    errorlist.Free;
  end;
end;

function TeabAlgorithmClass.GetFirstChildValue(node:IXmlNode;
                                               const childNodeName:string
                                               ):string;
var
  iChildList:IXMLNodeList;
  iChild:IXMLNode;
begin
    Result:='';
    iChildList:=SelectNodes( node, WideString('./'+childNodeName));
    if iChildList.Count>0 then
    begin
      iChild:=iChildList[0];
      if NOT VarIsNull(iChild.NodeValue) then
        Result:=iChild.NodeValue;
    end;
end;

function TeabAlgorithmClass.GetColOutCodesMappingNamesValues(indexString:string): string;
var
  queryNode:IXMLNode;
  MapNodes:IXMLNodeList;
  iMap:integer;
  selectorNode:IXMLNode;
  MapContentNodes:IXMLNodeList;
  iVal:integer;
  parentDomNode:IDomNode;
  attrnode: IDomNode;
  outputName:string;
  inputFieldNode:IXMLNode;
begin
    Result:='';
    if NOT Self.XMLDocument.Active then Self.XMLDocument.Active:=true;

    queryNode:= Self.XMLDocument.DocumentElement;
    MapNodes := SelectNodes( queryNode, WideString(
        '//output_field/input_description/selector_of_fields_with_content_fixedvalue'));
    for iMap := 0 to MapNodes.Count-1 do
    begin
        selectorNode:=MapNodes[iMap];
        parentDomNode:= selectorNode.GetDomNode.ParentNode.ParentNode;//.ParentNode;
        if Not VarIsNull( parentDomNode.Attributes.getNamedItem('output_name')) then
        begin
          attrnode:=parentDomNode.Attributes.getNamedItem('output_name');
          outputName:= attrnode.get_NodeValue();
          if CompareText( indexString, outputName ) = 0 then
          begin
              // it is ColumnMapping to E/D output field, for ED Code
              MapContentNodes:= SelectNodes( MapNodes[iMap], WideString(
               './input_field'));
              for iVal:=0 to MapContentNodes.Count-1 do begin
                 // existence of input_name attributes validated before
                 inputFieldNode:= MapContentNodes[iVal];
                 Result:=Result
                                + inputFieldNode.Attributes['input_name']
                                + '='
                                + GetFirstChildValue(
                                           inputFieldNode
                                          ,'output_value_if_content_exists'
                                                    )
                               +#13#10;
              end;
          end;
          if Length(Result) > 0 then
            Result := Copy(Result,1, Length(Result)-2);
        end;
    end;
end;

function TeabAlgorithmClass.ColumnMapping: IEACSV_ColOut_Mapping;
begin
  if Self.XMLDocument.Active then
    Result:=Self
  else
    Result:=nil;
end;

function TeabAlgorithmClass.getColumnOutMapping(index:string): string;
begin
  result:=GetColOutCodesMappingNamesValues( index) ;
end;

procedure TeabAlgorithmClass.setColumnOutMapping(index:string; Value: string);
var
  aList:TStringlist;
begin
  aList:=TStringList.Create;
  try
    aList.Text:=Value;
    SetEDMapList(index, aList);
  finally
    aList.Free;
  end;
end;



procedure TeabAlgorithmClass.SetXMLEDMappingValue(indexString:string; const Field, Value:string);
var
  vNodelist:IXMLNodelist;
  SelectorNode:IXMLNode;
  FieldNodeList:IXMLNodeList;
  FieldNode:iXMLNode;
  ContentNodeList:IXMLNodeList;
  ContentNode:IXMLnode;
  sInputId:string;
begin
  if Self.XMLDocument.Active then
  begin
  // we must repopulate list of nodes, inserting non existing ones if necessary
   vNodelist := SelectNodes( Self.XMLDocument.DocumentElement, WideString(
'//output_field[@output_name='''+indexString+''']/input_description/'
    +'selector_of_fields_with_content_fixedvalue'));
   if vNodeList.Count > 0 then
   begin
       SelectorNode := vNodeList[0];
       FieldNodeList := SelectNodes( SelectorNode, WideString(
       './input_field[@input_name='''+Field+''']'));
       if FieldNodeList.Count > 0 then
       begin
          FieldNode:=FieldNodeList[0];
          ContentNodeList :=  SelectNodes( FieldNode, WideString(
            './output_value_if_content_exists'));
          if ContentNodeList.Count > 0 then
          begin
             ContentNode:=ContentNodeList[0];
             ContentNode.NodeValue := Value;
          end;
       end
       else
       begin
          sInputId := GetGlobalInputIDFromName(Field);
          FieldNode:=SelectorNode.AddChild('input_field');
          FieldNode.Attributes['input_name'] := Field;
          FieldNode.Attributes['input_id'] := sInputId;
          ContentNode := FieldNode.AddChild('output_value_if_content_exists');
          ContentNode.NodeValue := Value;
       end;
   end;
  end;
end;

function TeabAlgorithmClass.GetGlobalInputIDFromName(InputFieldName:string):string;
var
  INodeList:IXMLNodeList;
  iNode:IXMLNode;
begin
  Result := '';
  INodeList:= SelectNodes( Self.XMLDocument.DocumentElement, WideString(
  '//input_field[@input_name='''+InputFieldName+''']'));
  if INodeList.Count > 0 then
  begin
      iNode := INodeList[0];
      Result:=VarToStr(iNode.Attributes['input_id']);
  end;
end;

procedure TeabAlgorithmClass.SetEDMapList(indexString:string; const Value: TStrings);
var
  i: integer;
  ParentNodeList: IXMLNodelist;
  ParentNode: IXMLNode;
begin
// This is horizontal, column-name mapping on limited set of values only,
// as it is specified in original strategy XML document

// clearing off old values
  ParentNodeList := SelectNodes( Self.XMLDocument.DocumentElement, WideString(
      '//output_field[@output_name='''+indexString+''']/input_description/'
    +'selector_of_fields_with_content_fixedvalue'));

  if ParentNodeList.Count>0 then
  begin

    ParentNode := ParentNodeList[0];
    while ParentNode.ChildNodes.Count > 0 do
      ParentNode.ChildNodes.Delete(0);

    for i := 0 to Value.Count-1 do
       Self.SetXMLEdMappingValue(indexString, Value.Names[i], Value.Values[Value.Names[i]] );

  end;
end;

procedure TeabAlgorithmClass.setLookupTableMapping(index:string; Value: string);
var
  aLookupList:TStringList;
  aLookupNodelist:IXMLnodeList;
  alookupReplacementContainer:IXMLNode;
  iPair:integer;
  aNewNode:IXMLNode;
  aNewKey:IXMLNode;
  aNewValue:IXMLNode;
  iFieldNode:iXMLNode;
  iDescriptNode:IXMLNode;
  iLookupReplacement:IXMLNode;

  iKeyValueList:IXMLNodeList;
  iKeyValueFieldNode:IXMLNode;
  iNewKeyValueFieldNode:IXMLNode;
  iNewKeyValueInputField:IXMLNode;
begin
  aLookupList := TstringList.Create;
  try
    alookupList.Text := Value;

    aLookupNodeList := SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'//output_field[@output_name='''+index
+''']'
                                          )
                                 );
     if aLookupNodelist.Count > 0 then
     begin
        iFieldNode := aLookupNodelist[0];
// retrieve key value descriptor
        iKeyValueList := SelectNodes( iFieldNode,
                                      WideString(
        './input_description/lookup_table_replacement/key_value_field'
                                                ));
        if iKeyValuelist.Count=0 then
          Raise
                Exception.Create('Missing Key Value for output field "'
                +index+'" lookup replacement description in algorithm XML"'
                                );
        if iKeyValuelist.Count>1 then
          Raise
                Exception.Create('Multiple Key Value for output field "'
                +index+'" lookup replacement description in algorithm XML"'
                                );
       iKeyValueFieldNode := iKeyValuelist[0];

       // clear input description list
       while iFieldNode.ChildNodes.Count > 0 do
          iFieldNode.ChildNodes.Delete(0);

       iDescriptNode := iFieldNode.AddChild('input_description');
       iLookupReplacement := iDescriptNode.AddChild('lookup_table_replacement');
       iNewKeyValueFieldNode := iLookupReplacement.AddChild('key_value_field');
       iNewKeyValueInputField := iNewKeyValueFieldNode.AddChild('input_field');
       iNewKeyValueInputField.Attributes['input_name'] :=
                  iKeyValueFieldNode.ChildNodes[0].Attributes['input_name'];
       iNewKeyValueInputField.Attributes['input_id'] :=
                  iKeyValueFieldNode.ChildNodes[0].Attributes['input_id'];

       alookupReplacementContainer:=iLookupReplacement.AddChild('replacement_list');
     end else
     begin
      // erroneous field name
         Exit;
     end;
     for iPair:= aLookupList.Count-1 downto 0 do
     begin
       aNewNode:=alookupReplacementContainer.AddChild('replacement_pair');
         aNewKey:=aNewNode.AddChild('key_value',0);
         aNewKey.NodeValue:=aLookupList.Names[iPair];
         aNewValue:=aNewnode.AddChild('replacement_value',1);
         aNewValue.NodeValue:= copy(aLookupList[iPair],
                                    Pos('=',aLookupList[iPair])+1,
                                    Length(aLookupList[iPair])
                                    );
     end;
  finally
    alookuplist.Free;
  end;
end;

function TeabAlgorithmClass.getLookupTableMapping(index:string): string;
var
  aLookupList:TStringList;
  aLookupNodelist:IXMLnodeList;
  alookupReplacementContainer:IXMLNode;
  iPair:integer;
  aPair:IXMLNode;

  keyValueList:IXMLNodeList;
  ReplacementValueList:IXMLNodeList;

  sName,sValue:string;
begin
  aLookupList:=TstringList.Create;
  try
    aLookupNodeList:=SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'//output_field[@output_name='''+index
+''']/input_description/lookup_table_replacement/replacement_list'
                                           )
                                 );
     if aLookupNodelist.Count>0 then begin
       alookupReplacementContainer:=aLookupNodelist[0];
       for iPair:=0 to alookupReplacementContainer.ChildNodes.Count-1 do
       begin
          sName:=''; sValue:='';
          aPair:= alookupReplacementContainer.ChildNodes.Get(iPair);
          keyValueList:= SelectNodes( aPair, WideString('./key_value'));
          if KeyValueList.Count > 0 then
            if NOT VarIsNull(KeyValueList[0].NodeValue) then
              sName:=KeyValueList[0].NodeValue;

          ReplacementValueList:= SelectNodes( aPair,
                                            WideString('./replacement_value')
                                             );
          if ReplacementValueList.Count > 0 then
            if NOT VarIsNull(ReplacementValueList[0].NodeValue) then
              sValue:=ReplacementValueList[0].NodeValue;

          if (Length(sName) > 0) and (Length(sValue) > 0) then
            aLookupList.Add(sName+'='+sValue);
       end;
     end;

     Result:=aLookupList.Text;

  finally
    aLookupList.Free;
  end;
end;


function TeabAlgorithmClass.LookupTableMapping: IEACSV_LookupTable_Mapping;
begin
  Result:=Self;
end;

function TeabAlgorithmClass.Producer:IEaCSVLineProducer;
begin
    Result:=Self;
end;

function TeabAlgorithmClass.ProducersCount: integer;
var
  iProducersList:IXMLNodeList;
begin
  Result := 0;
  if self.XMLDocument.Active then
  begin
    iProducersList := SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'./input_field_new_line_producers/input_field_new_line_producer'));
    Result := iProducersList.Count;
  end;
end;

function TeabAlgorithmClass.getProducer(index: integer): string;
var
  iProducersList : IXMLNodeList;
  producerNode : IXMLNode;
  iFieldList : IXMLNodeList;
  jField : integer;
  FieldNode : IXMLNode;
  ResultList : TStringList;
begin
  Result:='';
  iProducersList := SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'./input_field_new_line_producers/input_field_new_line_producer[@input_field_new_line_producer_id="'+IntToStr(index)+'"]'));
  if iProducersList.Count > 0 then
  begin
     producerNode := iProducersList[0];
     iFieldList := SelectNodes( producerNode, WideString(
      './input_field_new_line_producer_fields/input_field'));
     ResultList:=TStringList.Create;
     try
       for jField:=0 to iFieldList.Count-1 do
       begin
          FieldNode:=iFieldList[jField];
          if Length( VarToStr(FieldNode.Attributes['input_name']))>0 then
            ResultList.Add(VarToStr(FieldNode.Attributes['input_name'])
                           +'='
                           +VarToStr(FieldNode.Attributes['input_id'])
                           ) ;
       end;
       Result:=ResultList.Text;
     finally
       ResultList.Free;
     end;
  end;
end;


procedure TeabAlgorithmClass.setProducer(index: integer; ListText: string);
// producer : TStringList of input_name=input_id
// old values to be overriden
var
  iProducersList:IXMLNodeList;
  iProducerNode:IXMLNode;
  iFieldNodeList:IXMLNodeList;
  jField:integer;
  iFieldNode:IXMLNode;
  aListText:TStringList;
begin
    iProducersList := SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'./input_field_new_line_producers/input_field_new_line_producer[@input_field_new_line_producer_id="'+IntToStr(index)+'"]/input_field_new_line_producer_fields'));
  if iProducersList.Count > 0 then
  begin
    iProducerNode:=iProducersList[0];
    iFieldNodeList:=SelectNodes(iProducerNode, Widestring('./input_field'));
    while iFieldNodeList.Count > 0 do
    begin
       iProducerNode.ChildNodes.Delete(0);
       iFieldNodeList:=SelectNodes(iProducerNode, Widestring('./input_field'));
    end;
  end;
  if Assigned(iProducerNode) then
  begin
    aListText:=TStringList.Create;
    try
      aListText.Text := ListText;
      for jField := 0 to aListText.Count-1 do
      begin
        iFieldNode:=iProducerNode.AddChild('input_field');
        iFieldNode.Attributes['input_name']:=aListText.Names[jField];
        iFieldNode.Attributes['input_id']:=System.Copy( aListText[jField],
                                                        Pos('=',aListText[jField])+1,
                                                        Length(aListText[jField]));
    end;
    finally
      aListText.Free;
    end;
  end;
end;



function TeabAlgorithmClass.GetHorzFieldValueArbitrator(
  indexstring: string): string;
var
  ArbitratorNodeList:IXMLNodelist;
  ArbitratorNode:IXMLnode;
  FieldSetNodeList:IXMLNodeList;
  FieldSetNode:IXMLNode;
  FieldNode:IXMLNode;
  jNode:integer;
  aResultList:TStringList;
  FieldNodeList:IXMLNodelist;
begin
  Result:='';
  ArbitratorNodeList := SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'./output_fields/output_field[@output_name='''+indexstring+''']'));
  if ArbitratorNodeList.Count > 0 then
  begin
    ArbitratorNode:=ArbitratorNodeList[0];
    FieldSetNodeList:= SelectNodes(ArbitratorNode, Widestring(
'./input_description/selector_of_fields_with_content'));

    if FieldSetNodeList.Count = 0 then Exit;

    FieldSetNode := FieldSetNodeList[0];

    aResultList:=TStringList.Create;
    try
      FieldNodeList :=SelectNodes(FieldSetNode, './input_field');

      for jNode := 0 to FieldNodelist.Count - 1 do
      begin
          FieldNode := FieldNodelist[jNode];
          aResultList.Add( VarToStr(FieldNode.Attributes['input_name'])
                          + '='
                          + VarToStr(FieldNode.Attributes['input_id'])
                         );
      end;
      Result := aResultList.Text;
    finally
      aResultList.Free;
    end;
  end;
end;

procedure TeabAlgorithmClass.SetHorzFieldValueArbitrator(indexstring,
  IntakeNameEqIdCrLf: string);
var
  aResultList:TStringList;

  ArbitratorNodeList:IXMLNodelist;
  ArbitratorNode:IXMLnode;
  SelectorNodeList:IXMLNodeList;
  FieldSetNode:IXMLNode;
  jLine:integer;
  FieldNodeList:IXMLNodelist;
  FieldNode:IXMLNode;

begin

  ArbitratorNodeList := SelectNodes( Self.XMLDocument.DocumentElement,
                                WideString(
'./output_fields/output_field[@output_name='''+indexstring+''']'));
  if ArbitratorNodeList.Count > 0 then
  begin
    ArbitratorNode:=ArbitratorNodeList[0];
    SelectorNodeList:= SelectNodes(ArbitratorNode, Widestring(
'./input_description/selector_of_fields_with_content'));
    if SelectorNodeList.Count > 0 then
        FieldSetNode := SelectorNodeList[0];
  end;

  if NOT Assigned (FieldSetNode) then Exit;

   FieldNodeList := SelectNodes(FieldSetNode, WideString( './input_field'));
   while FieldNodeList.Count > 0 do
   begin
       FieldNode := FieldNodeList[0];
       FieldSetNode.ChildNodes.Remove(FieldNode);
       FieldNodeList := SelectNodes(FieldSetNode, WideString( './input_field'));
   end;

  aResultList:=TStringList.Create;
  try
    aResultList.Text:=IntakeNameEqIdCrLf;
    for jLine:=0 to aResultList.Count-1 do
    begin
       FieldNode := FieldsetNode.AddChild('input_field');
       FieldNode.Attributes['input_name'] := aResultList.Names[jLine];
       FieldNode.Attributes['input_id'] := System.Copy(
                                  aResultList[jLine],
                                  Pos('=', aResultList[jLine])+1,
                                  Length(aResultList[jLine])
                                                      );
    end;
  finally
    aresultList.Free;
  end;
end;

function TeabAlgorithmClass.HorzFieldValueArbitrator: IEaCSVHorzFieldValueArbitrator;
begin
  Result:=Self;
end;

end.
