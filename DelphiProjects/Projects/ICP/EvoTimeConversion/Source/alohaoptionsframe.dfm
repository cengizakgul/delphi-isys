object AlohaOptionsFrm: TAlohaOptionsFrm
  Left = 0
  Top = 0
  Width = 699
  Height = 161
  TabOrder = 0
  inline RateImportOptionFrame: TRateImportOptionFrm
    Left = 5
    Top = 2
    Width = 206
    Height = 79
    TabOrder = 0
    inherited RadioGroup1: TRadioGroup
      Items.Strings = (
        'Use Aloha rates'
        'Use Evolution DBDT rate override'
        'Use Evolution employee pay rates')
    end
  end
  object cbSummarize: TCheckBox
    Left = 226
    Top = 58
    Width = 81
    Height = 17
    Caption = 'Summarize'
    TabOrder = 3
  end
  object cbImportJobCodes: TCheckBox
    Left = 226
    Top = 10
    Width = 103
    Height = 17
    Caption = 'Import job codes'
    TabOrder = 1
  end
  object cbIgnoreNotMappedFields: TCheckBox
    Left = 226
    Top = 34
    Width = 263
    Height = 17
    Caption = 'Ignore not mapped Aloha tips and sales fields'
    TabOrder = 2
  end
end
