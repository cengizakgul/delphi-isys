unit XMLSerial;

interface
uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common,
  evodata,
  //JobsFrame,
   DBDTFrame, FilesOpenFrame, OptionsBaseFrame,
   //JobToDBDTFrame,
  // AlohaDBDTSourceFrame,
   csvmaploader, xmldom, XMLIntf, msxmldom, XMLDoc
  ,varHelperFunctions
  ,DBClient
  ,kbmMemTable, isDataSet, isBaseClasses, EvStreamUtils
  ;

const
  Codes64 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/';

function Encode64(S: string): string;
function Decode64(S: string): string;

function DSSerialize(const Ds:TkbmCustomMemTable):string;
function DSDeSerialize(sBase64:string):IDataset;

procedure copyClientDataset(origDs, DestDs:TClientDataset);

implementation

function DSSerialize(const Ds:TkbmCustomMemTable):string;
var
  StreamingDS:IDataSet;
  S: IisStream;
begin
  StreamingDS := TisDataSet.Create(Ds);
  S := TisStream.Create;
  S.WriteObject(StreamingDS);

  Result := Encode64(S.AsString);
end;

function DSDeSerialize(sBase64:string):IDataset;
var
   S: IisStream;
begin
  S := TisStream.Create;
// For some reason on transformations below first byte of the stream got eaten;
// so I put extra character in front of stream, so name of the object type -IisDataset
// would read in correct way ; here #9 + come
  S.AsString := Decode64(sBase64);
  S.Position := 0;

//  Result:= TisDataSet.Create();
//  S.Position := 0;
  Result:=s.ReadObject(nil) as IDataset;
end;

function Encode64(S: string): string;
var
  i: Integer;
  a: Integer;
  x: Integer;
  b: Integer;
begin
  Result := '';
  a := 0;
  b := 0;
  for i := 1 to Length(s) do
  begin
    x := Ord(s[i]);
    b := b * 256 + x;
    a := a + 8;
    while a >= 6 do
    begin
      a := a - 6;
      x := b div (1 shl a);
      b := b mod (1 shl a);
      Result := Result + Codes64[x + 1];
    end;
  end;
  if a > 0 then
  begin
    x := b shl (6 - a);
    Result := Result + Codes64[x + 1];
  end;
end;

function Decode64(S: string): string;
var
  i: Integer;
  a: Integer;
  x: Integer;
  b: Integer;
begin
  Result := '';
  a := 0;
  b := 0;
  for i := 1 to Length(s) do
  begin
    x := Pos(s[i], codes64) - 1;
    if x >= 0 then
    begin
      b := b * 64 + x;
      a := a + 6;
      if a >= 8 then
      begin
        a := a - 8;
        x := b shr a;
        b := b mod (1 shl a);
        x := x mod 256;
        Result := Result + chr(x);
      end;
    end
    else
      Exit;
  end;
end;

procedure copyClientDataset(origDs, DestDs:TClientDataset);
var
  sStream:TStringStream;
begin
  sStream:=TStringStream.Create('');
  try
    origDs.SaveToStream(sStream,dfXML);
    sStream.Position:=0;
    DestDs.LoadFromStream(sStream);
  finally
    sStream.Free;
  end;
end;

end.
