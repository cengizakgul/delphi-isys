unit VarHelperFunctions;

interface
uses
  SysUtils
 ,Classes
 ,Variants
 ,StrUtils
 ,ShellApi
 ,Windows
 ,ExtCtrls
 ,xmldom, XMLIntf, msxmldom, XMLDoc
 ,DBClient
 ,EaTextStreaming
 ;

function checkVarInt(V:Variant):boolean;
function DeleteShellTree(Handle:THandle; DirPath:string):boolean;
function SelectNodes(xnRoot: IXmlNode; const nodePath: WideString): IXMLNodeList;
procedure SplitSingleLine(sLine:string; sDelimiter:string; List:TStrings);
function DataSetColPairToString(        sDatasetDataPacket:string;
                                  const LeftNameCol:string;
                                  const RightValueCol:string
                               ):string;

function getResourceXML:string;

// remove offending file name characters
function StrRectify(s:string):string;
function CompanyRelatedFileName(UserInput, company, ext:string):string;

procedure getCSVExtraHorzMapList( CSVFileName:string;
                                  BorderFieldNumber:integer;
                                  var aList:TStrings);

function getCSVFileExtraHorzMapNameIdString(
                                  CSVFileName:string;
                                  BorderFieldNumber:integer
                                           ):string;

function NameValueListTextToComma(NameValueListText:string):string;

function MergeAddComma(oldCommaNameValue, newCommaNameValue:string):string;

function ClientDatasetSaveToString(ClientTable:TClientDataset): string;

function NameListSubtract(ListSutractFrom, ListWithout:string):string;

function CommaListOfNames(aListLine:string):string;

function getFirstValInCols(CommaLine: string; ColsCrLf:string; Header:string):string;

implementation

  function checkVarInt(V:Variant):boolean;
  var
    s:string;
    ires:integer;
    ier:integer;
  begin
     s := V;
     Val(s,iRes,ier);
     // to avoid compiler complaints; it is string verification only
     if iRes = 0 then
     begin
     end;
     Result := ier = 0;
  end;

function DeleteShellTree(Handle:THandle; DirPath:string):boolean;
// returns true if deletion of the folder tree success
// Handle may be 0
const FOF_NO_UI = 1556;
var
  ShOp: TSHFileOpStruct;
begin
  ShOp.Wnd := //Self.
              Handle;
  ShOp.wFunc := FO_DELETE;
  ShOp.pFrom := PChar(DirPath+#0);
  ShOp.pTo := nil;
  ShOp.fFlags := FOF_NO_UI or FOF_NOCONFIRMATION;
  Result:= SHFileOperation(ShOp) = 0;
end;


function SelectNodes(xnRoot: IXmlNode; const nodePath: WideString): IXMLNodeList;
var
  intfSelect : IDomNodeSelect;
  intfAccess : IXmlNodeAccess;
  dnlResult  : IDomNodeList;
  intfDocAccess : IXmlDocumentAccess;
  doc: TXmlDocument;
  i : Integer;
  dn : IDomNode;
begin
  Result := nil;
  if not Assigned(xnRoot)
    or not Supports(xnRoot, IXmlNodeAccess, intfAccess)
    or not Supports(xnRoot.DOMNode, IDomNodeSelect, intfSelect) then
    Exit;

  dnlResult := intfSelect.selectNodes(nodePath);
  if Assigned(dnlResult) then
  begin
    Result := TXmlNodeList.Create(intfAccess.GetNodeObject, '', nil);
    if Supports(xnRoot.OwnerDocument, IXmlDocumentAccess, intfDocAccess) then
      doc := intfDocAccess.DocumentObject
    else
      doc := nil;

    for i := 0 to dnlResult.length - 1 do
    begin
      dn := dnlResult.item[i];
      Result.Add(TXmlNode.Create(dn, nil, doc));
    end;
  end;
end;

procedure SplitSingleLine(sLine:string; sDelimiter:string; List:TStrings);
begin
  List.Clear;
  List.Text:=AnsiReplaceText(sLine,sDelimiter, #13#10);
  if AnsiEndsText(sDelimiter,sLine) then
    List.Add('');
end;

function DataSetColPairToString(       sDatasetDataPacket:string;
                                      const LeftNameCol:string;
                                      const RightValueCol:string
                                ):string;
var
  MapList:TStrings;
  doc:TXMLDocument;
  Panel:TPanel;
  sStream:TStringStream;
  iRows:IXMLNodeList;
  j:integer;
  iRow:IXMLNode;
  vLeft,vRight:Variant;
begin
    Result:='';
    if length ( sDatasetDataPacket) > 0 then
    begin
        Panel := TPanel.Create(nil);
        try
          doc := TXMLDocument.Create(Panel);
          sStream := TStringStream.Create(sDatasetDataPacket);
          try
            doc.LoadFromStream(sStream);
            doc.Active := true;
            iRows := SelectNodes(doc.DocumentElement,'//DATAPACKET/ROWDATA/ROW');
            if iRows.Count > 0 then begin
               MapList:=TStringList.Create;
               try
                 MapList.Clear;
                 for j := 0 to iRows.Count-1 do
                 begin
                     iRow := iRows[j];
                     vLeft := iRow.Attributes[LeftNameCol];
                     vRight := iRow.Attributes[RightValueCol];
                     if NOT ( VarIsNull(vLeft) ) then
                     MapList.Add(VarToStr(vLeft)+'='+Trim(VarTostr(vRight)));
                 end;
                 Result := Maplist.Text;
               finally
                 MapList.Free;
               end;
            end;
          finally
            sStream.Free;
          end;
        finally
          Panel.Free;
        end;
    end;
end;

function getResourceXML:string;
var
  sR:TResourceStream;
  sS:TStringStream;
begin
   Result:='';
   sr:=TResourceStream.Create(hInstance, 'CSV2CSV', RT_RCDATA);
   try
      SS:=TStringStream.Create('');
      try
        ss.CopyFrom(sR,0);
        Result:=ss.DataString;
      finally
        sS.Free;
      end;
   finally
      sr.Free;
   end;
end;

function StrRectify(s:string):string;
begin
  Result:=AnsiReplaceText(s,'\',' ');
  Result:=AnsiReplaceText(Result,'<',' ');
  Result:=AnsiReplaceText(Result,'>',' ');
  Result:=AnsiReplaceText(Result,':',' ');
  Result:=AnsiReplaceText(Result,'"',' ');
  Result:=AnsiReplaceText(Result,'/',' ');
  Result:=AnsiReplaceText(Result,'|',' ');
  Result:=AnsiReplaceText(Result,'?',' ');
  Result:=AnsiReplaceText(Result,'*',' ');

end;

function CompanyRelatedFileName(UserInput, company, ext:string):string;
begin
   Result:=UserInput;
   if NOT AnsiEndsText(ext,Result) then
   begin
    while AnsiEndsStr('.', Result) do
      System.Delete(Result,Length(Result),1);
    Result:=Result+ext;
   end;

   if NOT AnsiContainsText( Result, company) then
          Result:=System.Copy(Result,1,Length(Result)-4)
                          +'-'+StrRectify(company)
                          +' '+FormatDateTime('yyyy-mm-dd hh-nn-ss ',Now());
   while AnsiEndsStr('.', Result) do
      System.Delete(Result,Length(Result),1);
   Result:=Trim(Result);
   if NOT AnsiEndsText(ext, Result) then
                        Result:=Result+ext;
end;



procedure getCSVExtraHorzMapList( CSVFileName:string;
                                  BorderFieldNumber:integer;
                                  var aList:TStrings);
var
  sLine:string;
  tsFile:TTextStream;
  bFirstRead:boolean;
  j:integer;
  aValues:TStringList;
begin
    tsFile := TTextStream.Create(TFileStream.Create(CSVFileName, fmOpenRead or fmShareDenyNone));
    try
       bFirstRead:=tsFile.ReadLn(sLine);
       if bFirstRead then
       begin
         aValues:=TStringList.Create;
         try
            SplitSingleLine(sLine, ',', aValues);
            aList.Clear;
            for j:=BorderFieldNumber-1 to aValues.Count-1 do
            begin
              alist.Add(aValues[j]+'='+IntToStr(j+1));
            end;
         finally
           aValues.Free;
         end;
       end;
    finally
       tsFile.free;
    end;
end;

function getCSVFileExtraHorzMapNameIdString(
                                  CSVFileName:string;
                                  BorderFieldNumber:integer
                                           ):string;
var
  aList:TStringList;
begin
  aList:=TStringList.Create;
  try
    getCSVExtraHorzMapList( CSVFileName,BorderFieldNumber,TStrings(aList));
    Result:=aList.Text;
  finally
    aList.Free;
  end;
end;

function NameValueListTextToComma(NameValueListText:string):string;
var
  aList:TStringList;
  jLine:integer;
begin
  Result := '';
  aList := TStringList.Create;
  try
    alist.Text := NameValueListText;
    for jLine := 0 to aList.Count-1 do
       Result := Result + aList[jLine]+',';
    if Length(Result) > 0 then
      Result := System.Copy(Result, 1, Length(Result)-1);
  finally
    aList.Free;
  end;
end;

function MergeAddComma(oldCommaNameValue, newCommaNameValue:string):string;
var
  aList, bList : TStringlist;
  jLine:integer;
  jTarget:integer;
begin
  aList:=TStringList.Create();
  try
    bList:=TStringList.Create();
    try
       SplitSingleLine(oldCommaNameValue,',' , aList);
       SplitSingleLine(newCommaNameValue,',' , bList);
       for jLine:=0 to bList.Count-1 do
       begin
         jTarget:=aList.IndexOfName(bList.Names[jLine]);
         if jTarget >=0 then
           aList[jTarget] := bList[jLine]
         else
          aList.Add(bList[jLine]);
       end;
       Result:= NameValueListTextToComma( aList.Text );
    finally
      bList.Free;
    end;
  finally
    aList.Free;
  end;
end;


function ClientDatasetSaveToString(ClientTable:TClientDataset): string;
var
  ss: TStringStream;
begin
  ss := TStringStream.Create('');
  try
    ClientTable.SaveToStream(ss, dfXML);
    Result := ss.DataString;
  finally
    FreeAndNil(ss);
  end;
  ClientTable.MergeChangeLog;
end;

function NameListSubtract(ListSutractFrom, ListWithout:string):string;
var
  aList, bList:TStringList;
  jLine:integer;
  jDel:integer;
begin
  aList := TStringList.Create();
  try
    bList := TStringList.Create();
    try
      aList.Text := ListSutractFrom;
      bList.Text := ListWithout;
      for jLine := 0 to bList.Count - 1 do
      begin
        jDel := aList.IndexOfName(bList.Names[jLine]);
        if jDel >=0 then
          aList.Delete(jDel);
      end;
      Result := aList.Text;
    finally
      bList.Free;
    end;
  finally
    aList.Free;
  end;
end;

function CommaListOfNames(aListLine:string):string;
var
  j:integer;
  aList:TStringList;
begin
  Result := '';
  aList:=TStringList.Create;
  try
    aList.Text := aListLine;
    for j := 0 to aList.Count-1 do begin
        Result:=Result + aList.Names[j] + ',';
    end;
    if Length(result) > 0 then
      Result := System.Copy(Result, 1, Length(Result)-1);
  finally
    aList.Free;
  end;
end;

function getFirstValInCols(CommaLine: string; ColsCrLf:string; Header:string):string;
var
  aCols:TStringList;
  aVals:TStringList;
  aHeader:TStringList;
  j:integer;
  jCol:integer;
begin
  Result := '';
  aCols := TStringList.Create;
  try
    aVals := TStringList.Create;
    try
      aHeader := TStringList.Create;
      try
      // search by name in pair(register-insenitive), so we add = sign
        SplitSingleLine(
                        AnsiReplacestr(Header,',','=,'),
                        ',',
                        aHeader);
        SplitSingleLine(CommaLine, ',', aVals);
        aCols.Text := ColsCrLf;
        for j := 0 to aCols.Count-1 do
        begin
        // search supposed to be register-insensitive
          jCol := aHeader.IndexOfName(aCols.Names[j]);
          if jCol >= 0 then
          begin
            if Length(aVals[jCol]) > 0 then
            begin
              Result := ' Value "'
                    +System.Copy(aHeader[jCol], 1, Length(aHeader[jCol])-1)
                    +'"='''
                    +aVals[jCol]+'''';
              Exit;
            end;
          end;
        end;

      finally
        aHeader.Free;
      end;
    finally
      aVals.Free;
    end;
  finally
    aCols.Free;
  end;
end;
end.
