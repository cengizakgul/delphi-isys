inherited EDCodeFrm: TEDCodeFrm
  Width = 795
  Height = 514
  inherited BinderFrm1: TBinderFrm
    Width = 795
    Height = 514
    inherited evSplitter2: TSplitter
      Top = 250
      Width = 795
    end
    inherited pnltop: TPanel
      Width = 795
      Height = 250
      inherited evSplitter1: TSplitter
        Height = 250
      end
      inherited pnlTopLeft: TPanel
        Height = 250
        inherited dgLeft: TReDBGrid
          Height = 225
        end
      end
      inherited pnlTopRight: TPanel
        Width = 525
        Height = 250
        inherited evPanel4: TPanel
          Width = 525
        end
        inherited dgRight: TReDBGrid
          Width = 525
          Height = 225
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 255
      Width = 795
      inherited pnlbottom: TPanel
        Width = 795
        inherited evPanel5: TPanel
          Width = 795
        end
        inherited dgBottom: TReDBGrid
          Width = 795
        end
      end
      inherited pnlMiddle: TPanel
        Width = 795
      end
    end
  end
  object cdAlohaFieldNames: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdAlohaFieldNamesCODE: TIntegerField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
    end
    object cdAlohaFieldNamesDESCRIPTION: TStringField
      DisplayLabel = 'Aloha Field Name'
      DisplayWidth = 20
      FieldName = 'DESCRIPTION'
      Size = 40
    end
  end
end
