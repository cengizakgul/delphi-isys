program EvoTimeConversion;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  XEvoAPIClientMainForm in 'XEvoAPIClientMainForm.pas' {XEvoAPIClientMainFm},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm},
  EDcsvCodeFrame in 'EDcsvCodeFrame.pas' {EDCodeFrm: TFrame},
  alohaparser in 'alohaparser.pas',
  MidasLib,
  DBDTFrame in 'DBDTFrame.pas' {DBDTFrm: TFrame},
  FilesOpenFrame in '..\..\common\FilesOpenFrame.pas' {FilesOpenFrm: TFrame},
  XgdySendMailLoggerView in 'XgdySendMailLoggerView.pas' {XSendMailLoggerViewFrame: TCommonLoggerViewFrame},
  CsvUtilityLoggerViewFrame in 'CsvUtilityLoggerViewFrame.pas' {CsvUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  csvmaploader in 'csvmaploader.pas',
  EvoCsvTransformMapper in 'EvoCsvTransformMapper.pas',
  EvoCsvTransformMapperClasses in 'EvoCsvTransformMapperClasses.pas',
  EvoCsvTransformWrkMapperClasses in 'EvoCsvTransformWrkMapperClasses.pas',
  XMLSerial in 'XMLSerial.pas';

{$R *.res}

begin
  LicenseKey := 'A7EC03975A60478D845855A6CC71DBF3'; 

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution Custom CSV file transformation';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
