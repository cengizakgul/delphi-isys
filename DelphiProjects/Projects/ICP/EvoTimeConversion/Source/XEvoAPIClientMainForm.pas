unit XEvoAPIClientMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,

  XgdySendMailLoggerView,

  gdyCommonLoggerView, gdyCommonLogger,
  EvoAPIConnectionParamFrame, ActnList, gdyBaseFrame,
  ExtCtrls, evoapiconnection, gdyclasses, Buttons, gdyLoggerImpl, isSettings,
  common, OptionsBaseFrame
  ;

type
  TXEvoAPIClientMainFm = class(TForm, IProgressIndicator, ILogRecorder)
    PageControl1: TPageControl;
    tbshLog: TTabSheet;
    StatusBar1: TStatusBar;
    TabSheet3: TTabSheet;
    EvoFrame: TEvoAPIConnectionParamFrm;
  private
    { Private dec larations }
    FProgressIndicator: IProgressIndicator;
    FLogOutput: TStringList;
    FLogOutputWriter: ILoggerEventSink;
    FLoggerFrame: TXSendMailLoggerViewFrame;

    procedure HandleEvoFrameChangedByUser(Sender: TObject);

    {ILogRecorder}
    procedure LogRecorder_Start(html: boolean);
    function LogRecorder_Stop: string;
    procedure ILogRecorder.Start = LogRecorder_Start;
    function ILogRecorder.Stop = LogRecorder_Stop;
    procedure HandleModalBegin(Sender: TObject);
  protected
    FSettings: IisSettings;
    FLoggerFrameClass: TXSendMailLoggerViewFrameClass;
    function Logger: ICommonLogger;
    procedure SaveLogArchiveTo(filename: string);
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    property ProgressIndicator: IProgressIndicator read FProgressIndicator implements IProgressIndicator;
  end;

implementation

uses gdycommon, dateutils, gdyGlobalWaitIndicator,
     gdyredir, gdyLogWriters, printers, gdyWaitForm, JclFileUtils, isBasicUtils;

{$R *.dfm}

constructor TXEvoAPIClientMainFm.Create(Owner: TComponent);
begin
  Application.OnModalBegin := HandleModalBegin;

  if FLoggerFrameClass = nil then
    FLoggerFrameClass := TXSendMailLoggerViewFrame;
  FLogOutput := TStringList.Create;
  inherited;
  FLoggerFrame := FLoggerFrameClass.Create(tbshLog);
  FLoggerFrame.Parent := tbshLog;
  FLoggerFrame.Align := alClient;
  SetWaitIndicator( TCombinedWaitIndicator.Create( TLargeWaitIndicator.Create(Self), THourglassWaitIndicator.Create) );

  FSettings := AppSettings;

  Caption := Format('%s %s %s', [Application.Title, VersionFixedFileInfoString(GetAppFilename, vfFull), FeatureSetName] );

  FProgressIndicator := TWaitProgressIndicator.Create(
         TStatusBarProgressIndicator.Create( StatusBar1 ) as IProgressIndicator,
         WaitIndicator);

  StatusBar1.DoubleBuffered := true;
  PageControl1.TabIndex := 1;

{$ifndef FINAL_RELEASE}
  try
    FLoggerFrame.LoggerKeeper.StartDevLoggingToFile( Redirection.GetFilename(sDebugLogFileAlias) );
  except
    Logger.StopException;
  end;
{$endif}
  try
    FLoggerFrame.LoggerKeeper.StartLoggingToFile( Redirection.GetFilename(sLogFileAlias) );
  except
    Logger.StopException;
  end;
  FLoggerFrame.Password := GetISystemsPassword;
  FLoggerFrame.EMail := GetISystemsEmail;
  Logger.LogDebug('Caption: ' + Caption );
  Logger.LogDebug('Timestamp: ' + DateTimeToStr(Now) );

  EvoFrame.Param := LoadEvoAPIConnectionParam( Logger, FSettings, '' );

  EvoFrame.OnChangeByUser := HandleEvoFrameChangedByUser;
end;

destructor TXEvoAPIClientMainFm.Destroy;
begin
  FLoggerFrame.LoggerKeeper.StopLoggingToFile;
{$ifndef FINAL_RELEASE}
  FLoggerFrame.LoggerKeeper.StopDevLoggingToFile;
{$endif}
  SetWaitIndicator(nil);
  FProgressIndicator := nil;
  inherited;
  FreeAndNil(FLogOutput);
end;

procedure TXEvoAPIClientMainFm.HandleEvoFrameChangedByUser(Sender: TObject);
begin
  try
    SaveEvoAPIConnectionParam( EvoFrame.Param, FSettings, '' );
  except
    Logger.StopException;
  end;
end;

function TXEvoAPIClientMainFm.Logger: ICommonLogger;
begin
  Result := FLoggerFrame.LoggerKeeper.Logger;
end;

procedure TXEvoAPIClientMainFm.LogRecorder_Start(html: boolean);
var
  output: ILogOutput;
begin
  Assert(FLogOutputWriter = nil);
  FLogOutput.Clear;
  output := TStringsLogOutput.Create(FLogOutput);
  if html then
    FLogOutputWriter := THtmlUserLogWriter.Create(output, Redirection.GetDirectory(sHtmlTemplatesDirAlias), Application.Title + ' log' )
  else
    FLogOutputWriter := TPlainTextUserLogWriter.Create(output);
  FLoggerFrame.LoggerKeeper.StatefulLogger.Advise( FLogOutputWriter );
end;

function TXEvoAPIClientMainFm.LogRecorder_Stop: string;
begin
  FLoggerFrame.LoggerKeeper.StatefulLogger.UnAdvise(FLogOutputWriter);
  FLogOutputWriter := nil;
  Result := FLogOutput.Text;
end;

procedure TXEvoAPIClientMainFm.SaveLogArchiveTo(filename: string);
begin
  Assert(FLoggerFrame <> nil);
  FLoggerFrame.SaveArchiveTo(filename);
end;

procedure TXEvoAPIClientMainFm.HandleModalBegin(Sender: TObject);
begin
  DispatchAppPaint;
end;

end.


