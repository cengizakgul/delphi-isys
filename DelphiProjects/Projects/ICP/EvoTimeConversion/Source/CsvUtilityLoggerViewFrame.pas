unit CsvUtilityLoggerViewFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XgdySendMailLoggerView, gdyLoggerRichView, ExtCtrls, common,
  comctrls, gdyLoggerImpl, StdCtrls, Buttons, ActnList,
  NewSevenZip
  ;

type
  TCsvUtilityLoggerViewFrm = class(TXSendMailLoggerViewFrame)
  private
    procedure HandleMessageLogged(item: TListItem; aLogState: TLogState);
  public
    constructor Create( Owner: TComponent ); override;
    procedure AddUserLogToArch(Arch: I7zOutArchive);override;
  end;

implementation

{$R *.dfm}

{ TEeUtilityLoggerViewFrm }

procedure TCsvUtilityLoggerViewFrm.AddUserLogToArch(Arch: I7zOutArchive);
begin
  inherited;
end;

constructor TCsvUtilityLoggerViewFrm.Create(Owner: TComponent);
begin
  inherited;
  LoggerRichView.OnMessageLogged := HandleMessageLogged;
end;

procedure TCsvUtilityLoggerViewFrm.HandleMessageLogged(item: TListItem; aLogState: TLogState);
begin
  item.Caption := '';
  item.SubItems.Add('');
  item.SubItems.Add( MakePrefixedMessage(aLogState, sCtxComponent) );
  try
    item.SubItems[0] := ExtractUniqueContextItem(aLogState, sCtxEECode)
  except
    item.SubItems[0] := '?';
    raise;
  end;
end;

end.
