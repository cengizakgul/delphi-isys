inherited CsvUtilityLoggerViewFrm: TCsvUtilityLoggerViewFrm
  inherited pnlLog: TPanel
    inherited LoggerRichView: TLoggerRichViewFrame
      inherited pnlUserView: TPanel
        inherited pnlTopPart: TPanel
          inherited lvMessages: TListView
            Columns = <
              item
                Caption = '*'
                Width = 20
              end
              item
                Caption = 'Employee #'
                Width = -2
                WidthType = (
                  -2)
              end
              item
                Caption = 'Message'
                Width = -1
                WidthType = (
                  -1)
              end>
          end
        end
        inherited pnlBottom: TPanel
          inherited pnlLeft: TPanel
            inherited mmDetails: TMemo
              ScrollBars = ssVertical
            end
          end
          inherited pnlRight: TPanel
            inherited mmContext: TMemo
              ScrollBars = ssVertical
            end
          end
        end
      end
    end
  end
end
