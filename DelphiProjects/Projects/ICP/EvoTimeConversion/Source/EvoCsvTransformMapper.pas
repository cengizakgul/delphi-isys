unit EvoCsvTransformMapper;

interface
uses
  SysUtils
 ,Classes
 ,Windows
 ,xmldom, XMLIntf, msxmldom, XMLDoc
 ;

 resourcestring
    CUSTOMAMOUNTBORDERFIELD='18';


  type

  TeTransformState=(
                    isStateInactive,
                    isStateAlgorithmLoaded,
                    isStateAlgorithmUnmapped,
                    isStateAlgorithmVerified,
                    isStateInputVerified,
                    isStateOutputGenerated
                     );

   IEACSV_ColOut_Mapping=interface
   ['{98DBFFEF-1972-4F99-806B-846995A86A6E}']
      function getColumnOutMapping(index:string):string;
      procedure setColumnOutMapping(index:string;Value:string);
      property ColumnOutMapping[index: string]:string read getColumnOutMapping write setColumnOutMapping;
   end;

   IEACSV_LookupTable_Mapping=interface
   ['{5DEF8744-BC4E-4921-AAF5-9818E50B7B12}']
      function getLookupTableMapping(index:string):string;
      procedure setLookupTableMapping(index:string;Value:string);
      property LookupTableMapping[index:string]:string read getLookupTableMapping write setLookupTableMapping;
   end;

   IEaCSVTransform=interface
   ['{1151B5BD-7EB9-4157-A40D-8DA6A28FB0B5}']
    function CSVTransformApply(CSVinputFile, CSVoutputFile,ErrLogFile:string):integer;
    function setProperty(nodeName:string; nodeValue:string):boolean;
    function getProperty(nodeName:string):string;
   end;

   IEaCSVLineProducer=interface
   ['{23436060-8DED-481C-8E9B-275776432D4C}']
    function ProducersCount : integer;
    function  getProducer(index : integer):string;
    procedure setProducer(index :integer; ListText:string);
    property Producer[index : integer] : string read getProducer write setProducer;
   end;

   IEaCSVHorzFieldValueArbitrator=interface
   ['{495C45F5-1297-4337-8BDC-84F9069D6875}']
     function GetHorzFieldValueArbitrator(indexsring:string):string;
     procedure SetHorzFieldValueArbitrator( indexstring:string;
                                            IntakeNameEqIdCrLf:string);
     property HorzFieldValueArbitrator[indexstring:string]:string
             read GetHorzFieldValueArbitrator write SetHorzFieldValueArbitrator;
   end;

  IeaAlgorithm=interface
  ['{B277E2BF-925A-43FE-A98E-471F08286298}']
     function getInternalState(): TeTransformState;
     procedure ReadFromAlgorithmFile(filename:string);
     function GetAlgorithm():string;
     function AlgorithmValidationError():string;
     function ColumnMapping:IEACSV_ColOut_Mapping;
     function LookupTableMapping:IEACSV_LookupTable_Mapping;
     function Transformer:IEaCSVTransform;
     function SaveToFile(fileName:string):boolean;
     function Producer:IEaCSVLineProducer;
     function HorzFieldValueArbitrator:IEaCSVHorzFieldValueArbitrator;
   end;

   TeaAlgorithmClass = class(//TInterfacedObject
                              TComponent
                              ,IeaAlgorithm)
   private
    FInternalState:TeTransformState;
    FDocument:TXMLDocument;
   protected
   public
    //    FAlgorithm:string;
     procedure SetInternalState(Value:TeTransformState);
     procedure SetAlgorithm(Value:string);
     procedure ReadFromAlgorithmStream(streAlgorithm: TStream); virtual;
     procedure ReadFromAlgorithmString(sAlgorithm: string);     virtual;
      {IeaAlgorithm}
     function getInternalState(): TeTransformState;           virtual;
     procedure ReadFromAlgorithmFile(filename:string);        virtual;
     function GetAlgorithm():string;                          virtual;
     function AlgorithmValidationError():string;              virtual; abstract;
     function ColumnMapping:IEACSV_ColOut_Mapping;            virtual; abstract;
     function LookupTableMapping:IEACSV_LookupTable_Mapping;  virtual; abstract;
     function Transformer:IEaCSVTransform;                    virtual; abstract;
     function SaveToFile(fileName:string):boolean;            virtual;
     function Producer:IEaCSVLineProducer;                    virtual;
     function HorzFieldValueArbitrator:IEaCSVHorzFieldValueArbitrator; virtual;
     {-- end of IeaAlgorithm}
     property XMLDocument:TXMLDocument read FDocument write FDocument;
      destructor Destroy(); override;
     property InternalState: TeTransformState read getInternalState;
   end;


implementation

{ TeaAlgorithmClass }


destructor TeaAlgorithmClass.Destroy;
begin
  inherited;
end;

function TeaAlgorithmClass.GetAlgorithm: string;
begin
   Result:=FDocument.XML.Text;
end;

function TeaAlgorithmClass.getInternalState: TeTransformState;
begin
   Result:=FInternalState;
end;

function TeaAlgorithmClass.HorzFieldValueArbitrator: IEaCSVHorzFieldValueArbitrator;
begin
// to be overriden by descendant
end;

function TeaAlgorithmClass.Producer: IEaCSVLineProducer;
begin
  Result:=nil;
end;

procedure TeaAlgorithmClass.ReadFromAlgorithmFile(filename: string);

begin
    SetInternalState(isStateInactive);
    Self.XMLDocument.LoadFromFile(filename);
    Self.XMLDocument.Active:=true; // check for well-formed Algo doc
    SetInternalState(isStateAlgorithmLoaded);
end;

procedure TeaAlgorithmClass.ReadFromAlgorithmStream(streAlgorithm:TStream);
begin
    SetInternalState(isStateInactive);
    Self.XMLDocument.LoadFromStream(streAlgorithm);
    Self.XMLDocument.Active:=true; // check for well-formed Algo doc
    SetInternalState(isStateAlgorithmLoaded);
end;

procedure TeaAlgorithmClass.ReadFromAlgorithmString(sAlgorithm:string);
var
  sStream:TStringStream;
begin
  sStream:=TStringStream.Create(sAlgorithm);
  try
    Self.ReadFromAlgorithmStream(sStream);
  finally
    sStream.Free;
  end;
end;

function TeaAlgorithmClass.SaveToFile(fileName: string): boolean;
begin
   result:=false;
   if Assigned(Self.XMLDocument) then
    if Length(Self.XMLDocument.XML.Text)>0 then
      try
        Self.XMLDocument.SaveToFile(fileName);
        Result:=true;
      except
      end;
end;

procedure TeaAlgorithmClass.SetAlgorithm(Value: string);
begin
  SetInternalState(isStateInactive);
//  Self.XMLDocument.Options:=Self.XMLDocument.Options+[doNodeAutoIndent];
  Self.XMLDocument.Active:=False;
  Self.XMLDocument.XML.Text:=Value;
  Self.XMLDocument.Active:=True;
  SetInternalState(isStateAlgorithmLoaded);
end;

procedure TeaAlgorithmClass.SetInternalState(Value: TeTransformState);
begin
  FInternalState:=Value;
end;


end.
