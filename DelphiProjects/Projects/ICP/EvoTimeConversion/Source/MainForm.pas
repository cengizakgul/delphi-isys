unit MainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList,
  XEvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  StrUtils,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame,
  FileOpenFrame, CustomBinderBaseFrame,
  EDcsvCodeFrame,
  alohaparser,
  DBDTFrame, FilesOpenFrame, OptionsBaseFrame
  ,EvoCsvTransformMapper
  ,EvoCsvTransformWrkMapperClasses
  ,csvmaploader, xmldom, XMLIntf, msxmldom, XMLDoc
  ,varHelperFunctions
  ,DB
  ,DBClient
  ,kbmMemTable, isDataSet, isBaseClasses, EvStreamUtils
  ,XMLSerial
  ;

type
  TmainFormState=(frmSinitial, // nothing connected; can connect or open file
             frmSjustConnected, // can open Map file, Save Map file, open CSV, but not save cSV
                                // full navigation over controls
             frmSMapConnected, // can get Connected, save Map file,
                               //open another Map file, open CSV, but not save SCV
             frmsOpenCSV     // full control
             ) ;

  TMainFm = class(TXEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshEdMap: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    EDCodeFrame: TEDCodeFrm;
    tbshBatchIDtoDBDTMapping: TTabSheet;
    DBDTFrame: TDBDTFrm;
    pnlTop: TPanel;
    FilesOpenFrame: TFilesOpenFrm;
    pnlSpacer: TPanel;
    bbSaveTransform: TBitBtn;
    bbOpenData: TBitBtn;
    bbSaveData: TBitBtn;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure bbSaveTransformClick(Sender: TObject);
    procedure bbOpenDataClick(Sender: TObject);
    procedure bbSaveDataClick(Sender: TObject);
  private
    FFormState:TmainFormState;
    FMemoCompany,FMemoClient:TMemo;
    FEvoData: TEvoData;
    FAlohaBatches: array of TAlohaBatch;
    FMapFile:string;
    FcsvTransformAlg:TeacAlgorithmClass;
    FIntakeRecordsProcessed:integer;
    FOutputRecordsProcessed:integer;
    FErrorsDetected:integer;
    FWarningsDetected:integer;
    FTempLocator:TTempLocator;
    FCSVFileToTransform:string;

    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure LoadPerCompanyMappings(const Value: TEvoCompanyDef);
    procedure HandleFilesSelected(files: TStrings);
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);
    procedure SavePerCompanySettings(const oldValue: TEvoCompanyDef);
    procedure AlohaDBDTSourceChanged;
    function  CompanyInProcess: TEvoCompanyDef;
    procedure OutputLineProcessed(Sender: TObject;
      RowProcessedCount: integer);
    procedure OutputLinearErrorDetected(Sender: TObject; RowProcessedCount,
      Column: integer; Err: TCsvError; Row, ErrorMessage: string);
    procedure EDCoMapToAlgorithm;
    procedure DBDTToAlgorithm();
    procedure IntakeWarningDetected(Sender: TObject; RowProcessedCount,
      Column: integer; Err: TCsvError; Row, ErrorMessage: string);
    procedure SetStaticText(id, inputCount, errorCount: integer);
    procedure IntakeLineProcessed(Sender: TObject;
      RowProcessedCount: integer);
    procedure RestoreDBDTfromAlg;
    procedure ButtonEnableCenterProc(Sender: TObject);
    function DeptCodesToString(ArrayOfAlohaBatches: array of TAlohaBatch):string;
    procedure DeptCodesFromString(deptCodesString: string);
    procedure RemapEDfromCSV(CSVFileName: string);
    function RealignEDFieldCodes(newCommaFieldList,
      OldMapDsString:string): string;
    function EDMapToProducerLine: string;
    function MatchAlgToFile(XMLFileName: string): Boolean;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}
{$R CSV2CSV.res}

uses
  isSettings, CsvUtilityLoggerViewFrame, dialogs,
  userActionHelpers, gdyBinder, variants
  ;


procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
var
  OldCompanyDef:TEvoCompanyDef;
begin
  if Length(FMapFile) > 0 then
  begin
  // we should provide smooth transition between conneced and disconnected states,
  // so we remember company
  //  clear off everything
  // connect and switch to previous company
    OldCompanyDef.ClNbr := 0; OldCompanyDef.CoNbr := 0;
    if NOT FEvoData.Connected then
    begin
      SavePerCompanySettings( OldCompanyDef);
      OldCompanyDef := CompanyInProcess();
    end;
  end;

  if FEvoData.CanGetClCo then
    SavePerCompanySettings(FEvoData.GetClCo);


  FEvoData.Connect(EvoFrame.Param);
  ConnectToEvo.Caption := 'Refresh Evolution data';

// restoration to previously mapped company
  if NOT ((OldCompanyDef.ClNbr = 0) and (OldCompanyDef.CoNbr = 0)) then
     FEvoData.SetClCo(OldCompanyDef);

  DBDTFrame.cbLevel.Enabled:=true;
  if Assigned(FMemoClient) then
    FreeAndNil(FMemoClient);
  if Assigned(FMemoCompany) then
    FreeAndNil(FMemoCompany);

  if FFormState = frmSinitial then
    FFormState := frmSjustConnected;
  if (Length(FCSVFileToTransform) > 0) and FileExists(FCSVFileToTransform) then
    FFormState:=frmSOpenCSV;
  ButtonEnableCenterProc(nil);

end;

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TCsvUtilityLoggerViewFrm;
  inherited;
  FEvoData := TEvoData.Create(Logger);
  FEvoData.AddDS(TMP_PRDesc);

  FEvoData.AddDS(CO_E_D_CODESDesc);

  FEvoData.AddDS(CUSTOM_DBDTDesc);

  CompanyFrame.Init(FEvoData);

  FEvoData.Advise(HandleCompanyChanged);

  //designer crashed repeatedly when I tried to do this by editing the form
  FilesOpenFrame.FileOpen1.Dialog.Filter := //'Aloha export files (extpay*.*)|extpay*.*';
                                            'CSV mapping file (*.xml)|*.xml';
  FilesOpenFrame.FileOpen1.Hint := //'Open|Opens an Aloha export file';
                                  'Open|Opens CSV mapping file';
  FilesOpenFrame.OnFilesSelected := HandleFilesSelected;


  PageControl1.TabIndex := 0;

  FcsvTransformAlg := TeacAlgorithmClass.Create(Self);
  FcsvTransformAlg.OnIntakeRecordProcessed := IntakeLineProcessed;
  FcsvTransformAlg.OnRecordProcessed := OutputLineProcessed;
  FcsvTransformAlg.OnErrorDetected := OutputLinearErrorDetected;

  SetStaticText(0,0,0);
  SetStaticText(1,0,0);

  AlohaDBDTSourceChanged;

  FFormState:=frmSinitial;
  ButtonEnableCenterProc(nil);
  tbshBatchIDtoDBDTMapping.TabVisible := True;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

procedure TMainFm.SavePerCompanySettings(const oldValue: TEvoCompanyDef);
var
  sOldEDMapFields : string;
  sNewEDMapFields : string;
begin
// comma-list of horisontal mapping from previous CSV
  sOldEDMapFields := FSettings.AsString[ClientUniquePath(oldValue)+'\EDcsvCodeMapping\EDMapFields'];

  if Assigned(FcsvTransformAlg) and FcsvTransformAlg.XMLDocument.Active then
  begin
   sNewEDMapFields := (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1];
   sOldEDMapFields :=  MergeAddComma(sNewEDMapFields, sOldEDMapFields);
   sNewEDMapFields := '';
  end;

  sNewEDMapFields := DataSetColPairToString(
                       ClientDatasetSaveToString(EDCodeFrame.cdAlohaFieldNames),
                       'DESCRIPTION',
                       'CODE'
                                           );

  sNewEDMapFields := NameValueListTextToComma( sNewEDMapFields);
  sNewEDMapFields:= MergeAddComma( sOldEDMapFields, sNewEDMapFields);

end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
var
  sOldValues:string;
  sOldEDFields:string;
begin
// fix to prevent old registry values from QA to interfere with present values
// - we do not store old mappings in registry any more
// Pretend that we have all as in first time
  sOldValues := '';
  sOldEDFields := '';

  EDCodeFrame.Init(sOldEDFields, sOldValues, FEvoData.DS[CO_E_D_CODESDesc.Name] );
  LoadPerCompanyMappings(Value);
end;

function TMainFm.DeptCodesToString( ArrayOfAlohaBatches: array of  TAlohaBatch):string;
var
  a:TStringList;
  i:integer;
begin
  a:=TStringList.Create();
  try
    for i:=0 to Length(ArrayOfAlohaBatches)-1 do
    begin
      a.Add(ArrayOfAlohaBatches[i].BatchID)
    end;
    Result:=Encode64(a.Text);
  finally
    a.Free;
  end;
end;

procedure TMainFm.DeptCodesFromString( deptCodesString:string);
var
  a:TStringList;
  i:integer;
begin
  a := TStringList.Create();
  try
    a.Text := Decode64(deptCodesString);
    SetLength( FAlohaBatches, a.Count);
    for i:=0 to a.Count-1 do
    begin
       FAlohaBatches[i].BatchID:=a[i];
    end;
  finally
    a.Free;
  end;
end;


//needed for HandleFilesSelected
procedure TMainFm.LoadPerCompanyMappings(const Value: TEvoCompanyDef);
var
  OldCompanySpecificMapping: string;
  OldCompanyCodes: string;
begin

  DBDTFrame.UnInit(); // otherwise it cannot be initialized properly

  // Pretend that we do not have stored values, and never will have
  OldCompanySpecificMapping := '';
  OldCompanyCodes := '';

  DeptCodesFromString(OldCompanyCodes);
  DBDTFrame.Init( OldCompanySpecificMapping,
                  FEvoData.DS[CUSTOM_DBDTDesc.Name],
                  FAlohaBatches );
end;



procedure TMainFm.RunActionUpdate(Sender: TObject);
begin

  (Sender as TCustomAction).Enabled := ( Length(FAlohaBatches) > 0) and EDCodeFrame.CanCreateMatcher and
                                      DBDTFrame.IsInitialized
                                      ;

end;


procedure TMainFm.AlohaDBDTSourceChanged;
begin
  tbshBatchIDtoDBDTMapping.TabVisible :=  True;
end;



procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  Logger.LogEntry('Current company changed');
  try
    try

      LogNullableEvoCompanyDef(Logger, new);

      if old.HasValue then
        SavePerCompanySettings(old.Value);
      if new.HasValue then
      begin

        EDCodeFrame.UnInit;
        DBDTFrame.UnInit;

        LoadPerCompanySettings(new.Value);
        tbshBatchIDtoDBDTMapping.TabVisible := True;
      end
      else
      begin
        EDCodeFrame.UnInit;
        DBDTFrame.UnInit;
      end;

// if company changed and we are connected, we must forget about old Map File
      if EvoData.Connected then
        if NOT (  (old.Value.ClNbr = new.Value.ClNbr)
                  and
                  (old.Value.CoNbr = new.Value.ClNbr)
               )
        then
        begin
          FMapFile := '';
          FFormState := frmSjustConnected;
          ButtonEnableCenterProc(nil);
        end;

    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;


procedure TMainFm.RestoreDBDTfromAlg();
var
   sDBDT:string;
   dsDBDT:TkbmCustomMemTable;
   IDBDTDs:IDataSet;
   DeptMatchDisplay:string;
begin
        DeptMatchDisplay := Decode64(FcsvTransformAlg.Transformer.getProperty('DBDTMatchDataset'));
        sDBDT := FcsvTransformAlg.Transformer.getProperty('CUSTOM_DBDTDesc');
        IDBDTDs := DSDeSerialize(sDBDT);
        dsDBDT :=  TkbmCustomMemTable(IDBDTDs.vclDataSet);

        DBDTFrame.Uninit();
        DBDTFrame.Init( DeptMatchDisplay, dsDBDT, FAlohaBatches );

        tbshBatchIDtoDBDTMapping.TabVisible:=true;
end;

procedure TMainFm.HandleFilesSelected(files: TStrings);
var
  i: integer;
  sLoad:string;
  CurrentCompany:TEvoCompanyDef;
  LoadedCompany:TEvoCompanyDef;
  jer:integer;
  oldNullableCompany:TNullableEvoCompanyDef;
  LoadedNullableCompany: TNullableEvoCompanyDef;

  DeptMatch:TStringList;
  DeptMatchDisplay:string;
  EDMatchDisplay:string;

  sNewClientName, sNewcompanyName:string;

  sEd:string;
  dsED:TkbmCustomMemTable;
  IEDDs:IDataSet;

  sEDMapFields:string;

  OldDBDTMapping:string;
begin
  Logger.LogEntry('User selected CSV mapping file');
  try
    try
      for i := 0 to files.Count-1 do
      begin
        Logger.LogContextItem( Format('File #%d', [i]), files[i] );

        if Comparetext(ExtractFileExt(files[i]),'.xml')=0 then
          FMapFile:=  files[i];
      end;
      if FileExists(FMapFile) then
      begin

        { TODO : Logging and except handling }
        FcsvTransformAlg.ReadFromAlgorithmFile(FMapFile);
        sLoad := FcsvTransformAlg.Transformer.getProperty('ClNbr');
        Val(sLoad, LoadedCompany.ClNbr,jer);
        sLoad := FcsvTransformAlg.Transformer.getProperty('CoNbr');
        Val(sLoad, LoadedCompany.CoNbr,jer);

      end;

        sNewClientName := FcsvTransformAlg.Transformer.getProperty('ClName');
        sNewcompanyName := FcsvTransformAlg.Transformer.getProperty('CoName');

      if Not FEvoData.Connected then begin
        if NOT Assigned( FMemoCompany) then
        begin
          FMemoCompany := TMemo.Create(Self);
          FMemoCompany.Parent := CompanyFrame.dblcCO;
          FMemoCompany.Align := alClient;
          FMemoCompany.Text := sNewCompanyName ;
          FmemoCompany.Ctl3d := False;
        end;
        if NOT Assigned( FMemoClient) then
        begin
          FMemoClient := TMemo.Create(Self);
          FMemoClient.Parent := CompanyFrame.dblcCL;
          FMemoClient.Align := alClient;
          FMemoClient.Text := sNewClientName ;
          FMemoClient.Ctl3d := False;
        end;

        DeptCodesFromString(FcsvTransformAlg.Transformer.getProperty('Dept_Codes_List'));

        sED := FcsvTransformAlg.Transformer.getProperty('CO_E_D_CODESDesc');
        IEDDs := DSDeSerialize(sED);
        dsED :=  TkbmCustomMemTable(IEDDS.vclDataSet);

        EDMatchDisplay :=  Decode64(FcsvTransformAlg.Transformer.getProperty('EDMatchDataset'));

        sEDMapFields := (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1];
        sEDMapFields := NameValueListTextToComma(sEDMapFields);
        EDCodeFrame.Init( sEDMapFields,
                          EDMatchDisplay,
                          dsED
                        );

        Setlength(FAlohaBatches,0);
        RestoreDBDTfromAlg();
        DBDTFrame.cbLevel.Enabled:=false;

        PageControl1.ActivePage:= tbshBatchIDtoDBDTMapping;

        // we are working offlain, all settings read from XML file for company
        FFormState:=frmSMapConnected;
        ButtonEnableCenterProc(nil);
        Exit;
      end;


      if FEvoData.CanGetClCo then
      begin
        CurrentCompany := FEvoData.GetClCo;
//        SavePerCompanyMappings(CurrentCompany);
        LoadPerCompanyMappings(CurrentCompany);

        if Not( (CurrentCompany.CoNbr = LoadedCompany.CoNbr) and (CurrentCompany.ClNbr=Loadedcompany.ClNbr)) then
        begin
            // change company to match loaded from file
            oldNullableCompany.Value := CurrentCompany;
            LoadedNullableCompany.Value := LoadedCompany;

            FEvoData.SetClCo( LoadedCompany );

        end;

        DeptMatchDisplay := Decode64(FcsvTransformAlg.Transformer.getProperty('DBDTMatchDataset'));
        EDMatchDisplay :=  Decode64(FcsvTransformAlg.Transformer.getProperty('EDMatchDataset'));
        if Length(EDMatchDisplay) > 0 then
        begin
           sEDMapFields := (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1];
           sEDMapFields := NameValueListTextToComma(sEDMapFields);
           EDCodeFrame.Init(  sEDMapFields,
                              EDMatchDisplay,
                              FEvoData.DS[CO_E_D_CODESDesc.Name]
                           );
        end;

        DeptMatch := TStringList.Create;
        try
          DeptMatch.Text := (FcsvTransformAlg as IeaAlgorithm).LookupTableMapping.LookupTableMapping['Override Dept'];
          SetLength(FAlohaBatches, DeptMatch.Count);
          for i := 0 to DeptMatch.Count-1 do
          begin
             FAlohaBatches[i].BatchID := DeptMatch.Names[i];
          end;
          if Length ( DeptMatchDisplay) = 0 then
          begin
             OldDBDTMapping := FSettings.AsString[CompanyUniquePath(LoadedCompany)+'\DBDTMapping\DataPacket'];

// Pretend we never met with this company before; we should read mapping from map file
             OldDBDTMapping := '';

             DBDTFrame.Uninit();
             DBDTFrame.Init(  OldDBDTMapping
                            , FEvoData.DS[CUSTOM_DBDTDesc.Name]
                            , FAlohaBatches
                           );
          end
          else
          begin
            DBDTFrame.Uninit();
            DBDTFrame.Init( DeptMatchDisplay, FEvoData.DS[CUSTOM_DBDTDesc.Name], FAlohaBatches )
          end;
        finally
          DeptMatch.Free;
        end;

      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.bbSaveTransformClick(Sender: TObject);
var
  SaveDialog:TSaveDialog;
  CurrentCompany:TEvoCompanyDef;
  DestinationXML:string;

  sCO_E_D_CODESDesc:string;
  sCUSTOM_DBDTDesc:string;

begin
//  inherited;
  if Assigned( FcsvTransformAlg) then
  begin
    SaveDialog:=TsaveDialog.Create(Self);
    try
      SaveDialog.FileName:=FMapFile;
      SaveDialog.Filter:='Mapping file for transformation (*.xml)|*.xml';
      SaveDialog.Options:=SaveDialog.Options+[ofDontAddToRecent];

      CurrentCompany := CompanyInProcess();
      if Length(FMapFile)> 0 then
      begin
        sCO_E_D_CODESDesc:=FcsvTransformAlg.Transformer.getProperty('CO_E_D_CODESDesc');
        sCUSTOM_DBDTDesc:=FcsvTransformAlg.Transformer.getProperty('CUSTOM_DBDTDesc');
        SaveDialog.InitialDir:=ExtractFileDir(FMapFile);
        SaveDialog.FileName:=FMapFile;
      end
      else
      begin
         Savedialog.InitialDir:=ExtractFileDir(Application.ExeName);
      end;

      if SaveDialog.Execute then
      begin
         DestinationXML:=SaveDialog.FileName;
// to avoid chances to have distorted data in attempt-mapping-and-reversal situation
         DBDTFrame.MergeChanges();
         EDCodeFrame.MergeChanges;

         DestinationXML:=CompanyRelatedFileName(DestinationXML, CurrentCompany.Name, '.xml');


         FcsvTransformAlg.SetAlgorithm(getResourceXML());
         FcsvTransformAlg.Transformer.setProperty('ClNbr', IntToStr(CurrentCompany.ClNbr));
         FcsvTransformAlg.Transformer.setProperty('CoNbr',  IntToStr(CurrentCompany.CoNbr) );

         FcsvTransformAlg.Transformer.setProperty('ClName',  CurrentCompany.ClientName );
         FcsvTransformAlg.Transformer.setProperty('CoName',  CurrentCompany.Name );

         FcsvTransformAlg.Transformer.setProperty('DBDTMatchDataset', Encode64(DBDTFrame.SaveToString()));
         FcsvTransformAlg.Transformer.setProperty('EDMatchDataset', Encode64(EDCodeFrame.SaveToString()));

         if FEvoData.Connected then begin
            FcsvTransformAlg.Transformer.setProperty('CO_E_D_CODESDesc',DSSerialize(FEvoData.DS[CO_E_D_CODESDesc.Name]));
            FcsvTransformAlg.Transformer.setProperty('CUSTOM_DBDTDesc',DSSerialize(FEvoData.DS[CUSTOM_DBDTDesc.Name]));
         end
         else
         begin
           FcsvTransformAlg.Transformer.setProperty('CO_E_D_CODESDesc',sCO_E_D_CODESDesc);
           FcsvTransformAlg.Transformer.setProperty('CUSTOM_DBDTDesc', sCUSTOM_DBDTDesc);
         end;

         FcsvTransformAlg.Transformer.setProperty('Dept_Codes_List', DeptCodesToString(FAlohaBatches));

         EDCoMapToAlgorithm();
         DBDTToAlgorithm();
         if Not ((FcsvTransformAlg as IeaAlgorithm).SaveToFile(DestinationXML)) then
         begin
           ShowMessage('Sorry, could not save to file : '+DestinationXML);
           Exit;
         end;

         FMapFile := DestinationXML;
         FilesOpenFrame.mmFiles.Lines.Add('Mapping saved to : '+DestinationXML);
      end;
    finally
        SaveDialog.Free;
    end;
  end;
end;

procedure TMainFm.bbOpenDataClick(Sender: TObject);
var
  OpenDialog:TOpenDialog;
  aEELookupList:TstringList;
  ExistingDBDTMapping:string;
  i:integer;
  sSampleAlgorithm:string;
  sOldDBDTMapping: string;

begin
//  inherited;
  if Assigned( FcsvTransformAlg) then
  begin
    OpenDialog := TOpenDialog.Create(Self);
    try
      OpenDialog.Filter := 'CSV input file (*.csv)|*.csv';
      if OpenDialog.Execute then
      begin
        FOutputRecordsProcessed := 0;
        FerrorsDetected := 0;
        FWarningsDetected:=0;
        FIntakeRecordsProcessed:=0;

        Application.ProcessMessages;
        FCSVFileToTransform := OpenDialog.FileName;

            // we are generating new transformation file,
            // so we are populating lookup table
            FilesOpenFrame.mmFiles.Lines.Add(FCSVFileToTransform);
            FOutputRecordsProcessed := 0;
            if Length(FMapFile)=0 then
            begin
              sSampleAlgorithm:=getResourceXML();
              FcsvTransformAlg.SetAlgorithm(sSampleAlgorithm);//ReadFromLagorithmString(sSampleAlgorithm);
            end;
            FcsvTransformAlg.PopulateLocator(FTempLocator);


            aEELookupList:=TStringList.Create;
            try
              aEELookupList.Duplicates:=dupIgnore;
              aEELookupList.Sorted:=True;
              GetlistOfValuesFromCsvFileCol(OpenDialog.FileName, 'Dept Code', aEELookupList);

              if Length(FAlohaBatches)=0 then
              begin // new mapping

                 ExistingDBDTMapping:=DBDTFrame.SaveToString();
                 if CompareText('NONE', ExistingDBDTMapping)=0 then
                 begin
                     if Not FEvoData.Connected then
                      ShowMessage('Connection to Evolution Necessary for initial mapping set');
                     // brand-new mapping
                 end
                 else
                 begin

                      SetLength(FAlohaBatches, aEELookupList.Count);
                      for i := 0 to aEELookupList.Count-1 do
                      begin
                         FAlohaBatches[i].BatchID := aEELookupList[i];
                      end;


                      if FEvoData.Connected then
                      begin
                        sOldDBDTMapping := FSettings.AsString[CompanyUniquePath(CompanyInProcess())+'\DBDTMapping\DataPacket'];
// pretend we never met this company before; should be stored in map file
                        sOldDBDTMapping := '';

                        DBDTFrame.Uninit();
                        DBDTFrame.Init( sOldDBDTMapping, // stored package
                                        FEvoData.DS[CUSTOM_DBDTDesc.Name],
                                        FAlohaBatches );
                      end
                      else
                         RestoreDBDTfromAlg();

                 end;
              end;

              try
                  FcsvTransformAlg.OnErrorDetected := IntakeWarningDetected;
                  FcsvTransformAlg.OnRecordProcessed:=nil;

                  ReMapEDfromCSV(FCSVFileToTransform);

                  EDCoMapToAlgorithm();
                  DBDTToAlgorithm();

                  FcsvTransformAlg.Transformer.CSVTransformApply(FCSVFileToTransform,
                                                 FTempLocator.OutputFile,
                                                 FTempLocator.ErrorLog);
              finally

              FcsvTransformAlg.OnRecordProcessed := OutputLineProcessed;
              FcsvTransformAlg.OnErrorDetected := OutputLinearErrorDetected;

              end;
            finally
              aEELookupList.Free;
            end;

        FOutputRecordsProcessed:=0;
        FerrorsDetected:=0;

        SetStaticText(0, FIntakeRecordsProcessed, FWarningsDetected);
        SetStaticText(1, FOutputRecordsProcessed, FErrorsDetected);

        FFormState:=frmSOpenCSV;
        ButtonEnableCenterProc(nil);
        Exit;

      end;
    finally
        OpenDialog.Free;
    end;
  end;
end;

procedure TMainFm.RemapEDfromCSV(CSVFileName:string);
var
   aExtraHorzMapList:TstringList;
   aOldMapList:TStringList;
   j:integer;
   jReplace:integer;

   sOldEdMapping:string;

   newCommaFieldList:string;

   sEd:string;
   dsED:TkbmCustomMemTable;
   IEDDs:IDataSet;
begin
  aExtraHorzMapList:=TStringList.Create;
  try
    getCSVExtraHorzMapList( CSVFileName,
                  StrToInt(EvoCsvTransformMapper.CUSTOMAMOUNTBORDERFIELD),
                  TStrings(aExtraHorzMapList));
    aOldMapList:=TStringList.Create;
    try
      // indexes of producers starts from 1
      // in current app only one producer expected
      // new field ids got ovverriden by fact ones
      aOldMapList.Text := (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1];
      for j:=0 to aExtraHorzMapList.Count-1 do begin
          jReplace:= aOldMapList.IndexOfName(aExtraHorzMapList.Names[j]);
          if  jReplace < 0 then
            aOldMapList.Add( aExtraHorzMapList[j])
          else
            aOldMapList[jReplace] := aExtraHorzMapList[j];
      end;
      (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1] := aOldMapList.Text;
      newCommaFieldList := NameValueListTextToComma(aOldMapList.Text);
      // in case we have different set of mapping fields than before, to avoid code confusion
      newCommaFieldList := RealignEDFieldCodes(newCommaFieldList, EDCodeframe.SaveToString());

    finally
      aOldMapList.Free;
    end;

    sOldEdMapping:=EDCodeframe.SaveToString();
    EDCodeFrame.Uninit;
    if FEvoData.Connected then
      EDCodeFrame.Init(newCommaFieldList, sOldEdMapping ,FEvoData.DS[CO_E_D_CODESDesc.Name])
    else
    begin
        sED := FcsvTransformAlg.Transformer.getProperty('CO_E_D_CODESDesc');
        IEDDs := DSDeSerialize(sED);
        dsED :=  TkbmCustomMemTable(IEDDS.vclDataSet);
        EDCodeFrame.Init(newCommaFieldList, sOldEdMapping, dsED);
    end;


  finally
    aExtraHorzMapList.Free;
  end;
end;

function TMainFm.CompanyInProcess():TEvoCompanyDef;
begin
  if FEvoData.Connected then
      Result := FEvoData.GetClCo
  else
    if Length(FMapFile) > 0 then
      if FileExists(FMapFile) then
      begin
          Result.ClNbr := StrToInt( FcsvTransformAlg.Transformer.getProperty('ClNbr'));
          Result.CoNbr := StrToInt( FcsvTransformAlg.Transformer.getProperty('CoNbr'));
          Result.ClientName := FcsvTransformAlg.Transformer.getProperty('ClName' );
          Result.Name := FcsvTransformAlg.Transformer.getProperty('CoName');
      end;
end;

procedure  TMainFm.EDCoMapToAlgorithm();
var
  sEDMapData: string;
  AlgData: string;
  ProducerLine: string;
  HourLine: string;
  sProducerUpdate: string;


begin
// to merge change log into dtatset; without it fresh changes may be get lost,
// especially if they are kind of attempt-and-reversal kind
    EDCodeFrame.MergeChanges;
    sEDMapData := EDCodeFrame.SaveToString();
    if length ( sEDMapData) > 0 then
    begin
      AlgData := DataSetColPairToString(  sEDMapData,
                                      'LEFT_DESCRIPTION',
                                      'RIGHT_CUSTOM_E_D_CODE_NUMBER'
                                       );

      FcsvTransformAlg.ColumnMapping.ColumnOutMapping['D/E'] := AlgData;
    end;

    ProducerLine := EDMapToProducerLine();
    (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1] := ProducerLine;


    HourLine := (FcsvTransformAlg as IEaCSVHorzFieldValueArbitrator)
                                            .HorzFieldValueArbitrator['Hours'];

    sProducerUpdate := NameListSubtract(ProducerLine,HourLine);

    (FcsvTransformAlg as IEaCSVHorzFieldValueArbitrator)
                                            .HorzFieldValueArbitrator['Amount']
                := sProducerUpdate;
end;

procedure TMainFm.DBDTToAlgorithm;
var sDBDTMapData:string;
    sDivision,
    sBranch,
    sDepartment,
    sTeam:string;
begin
    // to avoid loss of attempt-reversal kind of changes in mapping
    DBDTFrame.MergeChanges();
//'Override Division', 'Override Branch', 'Override Department', 'OverrideTeam'
    sDBDTMapData := DBDTFrame.SaveToString();
    sDivision := DataSetColPairToString(  sDBDTMapData,
                                      'LEFT_CODE',
                                      'RIGHT_CUSTOM_DIVISION_NUMBER'
                                       );
    sBranch := DataSetColPairToString(  sDBDTMapData,
                                      'LEFT_CODE',
                                      'RIGHT_CUSTOM_BRANCH_NUMBER'
                                       );
    sDepartment := DataSetColPairToString(  sDBDTMapData,
                                      'LEFT_CODE',
                                      'RIGHT_CUSTOM_DEPARTMENT_NUMBER'
                                       );
    sTeam := DataSetColPairToString(  sDBDTMapData,
                                      'LEFT_CODE',
                                      'RIGHT_CUSTOM_TEAM_NUMBER'
                                       );

    FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Division']:=sDivision;//ColumnMapping.ColumnOutMapping['Override Division']:=sDivision;
    FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Branch']:=sBranch;
    FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Dept']:=sDepartment;
    FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Team']:=sTeam;
end;



procedure  TMainFm.OutputLineProcessed(Sender:TObject; RowProcessedCount:integer);
begin
  FOutputRecordsProcessed := RowProcessedCount;

  FWarningsDetected:=0;
  SetStaticText(0, FIntakeRecordsProcessed,  FWarningsDetected);
  SetStaticText(1, FOutputRecordsProcessed, FerrorsDetected);
  Application.Processmessages;
end;

procedure  TMainFm.IntakeLineProcessed(Sender:TObject; RowProcessedCount:integer);
begin
  FIntakeRecordsProcessed := RowProcessedCount;
  SetStaticText(0, FIntakeRecordsProcessed, FWarningsDetected);
  Application.Processmessages;
end;

procedure  TMainFm.OutputLinearErrorDetected( Sender:TObject;
                                RowProcessedCount:integer;
                                Column:integer;
                                Err:TCsvError;
                                Row:string;
                                ErrorMessage:string
                                );
var
  errCode:string;
begin
    case Err of
        csvErrColMap : errCode:='Column mapping';
        csvErrLookup : errCode:='Lookup';
        csvErrUnknown: errCode:='Unknown';
    end;
     FErrorsDetected:=FErrorsDetected+1;

    try
      Raise Exception.Create(
                            Format('CSV Transform output error : internal row %d,  Column %d, ',[RowProcessedCount,Column])
                            +errcode+'; '+ErrorMessage

                      );
    except
      logger.StopException;
    end;
end;

procedure  TMainFm.IntakeWarningDetected( Sender:TObject;
                                RowProcessedCount:integer;
                                Column:integer;
                                Err:TCsvError;
                                Row:string;
                                ErrorMessage:string
                                );
var
  errCode:string;
  sSuspiciousCols:string;
  sHeaderLine:string;
begin
    case Err of
        csvErrColMap : errCode:='Column mapping';
        csvErrLookup : errCode:='Lookup missed';
        csvErrUnknown: errCode:='Unknown';
    end;

    if  AnsiContainsText(ErrorMessage, ' Missed horizontal mapping for out column ')
        and
        AnsiContainsText(ErrorMessage,'[D/E]')
    then
    begin
// extend error message by specifying which exactly column is missed -
// we know that all columns in producer must contain at least one value
        sSuspiciousCols := (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1];
        sHeaderLine := FcsvTransformAlg.IntakeHeaderLine;

        ErrorMessage := ErrorMessage
                        + ' '
                        + getFirstValInCols(Row, sSuspiciousCols, sHeaderLine);
    end;

     FWarningsDetected:=FWarningsDetected+1;

     logger.LogWarning(
        Format('CSV Transform input warning : internal row %d,  Column %d, ',[RowProcessedCount,Column])
        +errcode+'; '+ErrorMessage
                      );


end;


procedure TMainFm.bbSaveDataClick(Sender: TObject);
var
  SaveDialog:TSaveDialog;
  SaveTimeclockName:string;
  SaveClockErrorLog:string;
  SaveExt:string;
  CompanyName:string;
  sNewEDMapFields:string;
  sMappedFields:string;
  sResidualColMapping:string;
  sUnmappedCols:string;
begin
  //inherited;

  if      (Length(FCSVFileTotransform) = 0)
      or  (NOT FileExists(FCSVFileTotransform))
  then
    Raise Exception.Create('Missing initial .CSV file to get data from');

  if Length (FMapFile)=0 then
    Raise Exception.Create(' Missing saved transform .xml file. Please save current settings.');

// check if existing mapping is enough
// in case there were changes in the mapping
  EDCoMapToAlgorithm();
  DBDTToAlgorithm();

  if NOT Self.MatchAlgToFile(FMapFile) then
    Raise
      Exception.Create(' Mapping changed. Please save current settings.');

  sNewEDMapFields := DataSetColPairToString(
                       ClientDatasetSaveToString(EDCodeFrame.cdAlohaFieldNames),
                       'DESCRIPTION',
                       'CODE');
  sMappedFields := DataSetColPairToString(
                            EDCodeFrame.SaveToString(),
                            'LEFT_DESCRIPTION',
                            'LEFT_CODE'
                                         );
  sResidualColMapping := NameListSubtract(sNewEDMapFields,sMappedFields);

  sUnmappedCols := CommaListOfNames(sResidualColMapping);

  if Length(sUnmappedCols) > 0 then
    if mrOk <> MessageDlg( 'Are you sure ?'+#13#10 +
                           'Unmapped columns : '+#13#10
                            +sUnmappedCols,
                           mtWarning,
                           [mbOk, mbCancel],0
                          )
    then
      Exit;

  SaveDialog:=TSaveDialog.Create(Self);
  try
     SaveDialog.Filter := ' Timeclock CSV file (*.csv)|*.csv';
     if SaveDialog.Execute then
     begin
        SaveTimeclockName:=SaveDialog.FileName;

        CompanyName:= FcsvTransformAlg.Transformer.getProperty('CoName');

        SaveTimeclockName:=CompanyRelatedFileName(SaveTimeclockName, companyName,'.Timeclock.csv');

        SaveExt:=ExtractFileExt( SaveTimeclockName);


        SaveClockErrorLog := System.Copy(SaveTimeclockName,
                                         1,
                                         Length(SaveTimeclockName)-Length(SaveExt)
                                        )
                            +'.ErrorLog.txt';
// all prepared settings must be already set up on controls


        try
          FcsvTransformAlg.OnIntakeRecordProcessed := nil;
          FcsvTransformAlg.Transformer.CSVTransformApply(FCSVFileToTransform,
                                                         SaveTimeclockName,
                                                         SaveClockErrorLog);
          FilesOpenFrame.mmFiles.Lines.Add('Timeclock saved to : '+SaveTimeclockName);

          if FileExists (SaveClockErrorLog) then
            FilesOpenFrame.mmFiles.Lines.Add('Error Log saved to : '+SaveClockErrorLog);

        finally
           FcsvTransformAlg.OnIntakeRecordProcessed := IntakeLineProcessed
        end;
     end;
  finally
    SaveDialog.Free;
  end;

end;

procedure TMainFm.SetStaticText(id:integer;inputCount:integer; errorCount:integer);
var s:string;
begin
   if id = 0 then begin
     s:= 'Intake CSV lines: '+IntToStr(inputCount)+' ; Warnings: '+IntToStr(errorcount);
     StaticText1.Caption := s;
   end
   else
   begin
     s:= 'Output CSV lines: '+IntToStr(inputCount)+' ; Errors: '+IntToStr(errorcount);
     StaticText2.Caption :=s;
   end;
end;

procedure TMainFm.ButtonEnableCenterProc(Sender:TObject);
begin

  if FFormState = frmSinitial then begin
     BitBtn4.Enabled := true; // Get Evolution Data
     FilesOpenFrame.BitBtn1.Enabled := true; // Open Map XML
     bbOpenData.Enabled := false; // open CSV
     bbSaveTransform.Enabled := false; // Save CSV
     bbSaveData.Enabled:=false;
  end;

  if FFormState = frmSjustConnected then begin
     BitBtn4.Enabled := true; // Get Evolution Data
     FilesOpenFrame.BitBtn1.Enabled := true; // Open Map XML
     bbOpenData.Enabled := true; // open CSV
     bbSaveTransform.Enabled := true; // Save CSV
     bbSaveData.Enabled:=false;
  end;

  if FFormState = frmSMapConnected then begin
     BitBtn4.Enabled := true; // Get Evolution Data
     FilesOpenFrame.BitBtn1.Enabled := true; // Open Map XML
     bbOpenData.Enabled := true; // open CSV
     bbSaveTransform.Enabled := true; // Save CSV
     bbSaveData.Enabled:=false;
  end;

  if FFormState = frmSOpenCSV then begin
     BitBtn4.Enabled := true; // Get Evolution Data
     FilesOpenFrame.BitBtn1.Enabled := true; // Open Map XML
     bbOpenData.Enabled := true; // open CSV
     bbSaveTransform.Enabled := true; // Save CSV
     bbSaveData.Enabled:=true;
  end;

end;

function TMainFm.RealignEDFieldCodes(newCommaFieldList, OldMapDsString:string):string;
var
  CopyDS:TClientDataset;
  sStream:TStringStream;
  aNewFieldList:TStringList;
  j:integer;
  aName:string;
  aCode:string;
  jMaxExistingCode:integer;
  jCode:integer;
  jer:integer;
  aDuplicateCheckList:TStringList;

  function maxOldCode:integer;
  var
    imaxCode:integer;
    CurrCode:string;
    ier:integer;
    iCode:integer;
  begin
    iMaxCode:=-1;
    CopyDs.First;
    while NOT CopyDs.Eof do
    begin
      CurrCode:=CopyDs.FieldByName('LEFT_CODE').AsString;
      Val(CurrCode, iCode,ier);
      if ier=0 then
        if iCode > iMaxCode then
          iMaxCode:=iCode;
      CopyDs.Next;
    end;
    CopyDs.First;
    Result:=iMaxCode;
  end;

begin
// this function is to check, if we have match between registered names and ids
// and if unknown code will be found for existing id, then unique id will be produced

// if actual code found with different ID, then id got reassigned
  Result:='';
  CopyDS:=TClientDataset.Create(nil);
  try
    sStream:=TStringStream.Create(OldMapDsString);
    sStream.Position := 0;
    CopyDs.LoadFromStream(sStream);
    aNewFieldList:=TStringList.Create;
    try
      aDuplicateCheckList:=TstringList.Create();
      try
        aDuplicateCheckList.Duplicates := dupIgnore;
        aDuplicateCheckList.Sorted:=True;

        jMaxExistingCode := maxOldCode();
        SplitSingleline(newCommaFieldList,',', aNewFieldList);
        for j := 0 to aNewFieldList.Count-1 do
        begin
          aName := aNewfieldList.Names[j];
          aCode := System.Copy( aNewfieldList[j],
                                Pos('=',aNewfieldList[j])+1,
                                Length(aNewfieldList[j])
                              );
          Val( aCode, jCode, jer);
          if CopyDs.Locate('LEFT_DESCRIPTION', aName, [loCaseInsensitive]) then
          begin
            // check if new-read code match registered
            if CompareText(CopyDs.FieldByName('LEFT_CODE').AsString,
                            aCode) <> 0 then
            begin
                // order of fields alteration detected
                // so we ajust id according to existing one
                aNewFieldList[j] := aNewfieldList.Names[j]+'='
                          +CopyDs.FieldByName('LEFT_CODE').AsString;
            end;
          end
          else
          begin
              // new column name detected, not matched before
              jMaxExistingCode:=jMaxExistingCode+1;
              if jMaxExistingCode < jCode then
                jMaxExistingCode := jCode;
              aNewFieldList[j] := aNewfieldList.Names[j]+'='
                                          +IntToStr(jMaxExistingCode);
          end;

          Result := Result + aNewFieldList[j] + ',';
        end;

        if Length(Result) > 0 then
          Result := System.Copy(Result, 1, Length(Result)-1);
      finally
        aDuplicateCheckList.Free;
      end;
    finally
      aNewFieldList.Free;
    end;
  finally
    CopyDs.Free;
  end;
end;

function TMainFm.EDMapToProducerLine():string;
var
  CopyDS:TClientDataset;
  sStream:TStringStream;
  unMapStream:TStringStream;
  unMapDs:TClientDataset;
  aResultList:TStringList;
begin
  Result:='';
  aResultList:=TstringList.Create;
  try
    CopyDS:=TClientDataset.Create(nil);
    try
      sStream:=TStringStream.Create(EDCodeFrame.SaveToString() );
      try
        sStream.Position := 0;
        CopyDs.LoadFromStream(sStream);
        CopyDs.First;
        while NOT CopyDS.Eof do
        begin
           aResultList.Add(
                    CopyDs.FieldByName('LEFT_DESCRIPTION').AsString
                  + '='
                  + CopyDs.FieldByName('LEFT_CODE').AsString
                          );
          CopyDS.Next;
        end;

      finally
        sStream.Free;
      end;
    finally
      CopyDS.Free;
    end;
  // and now add to producer unmapped fields, so we will get error on
  // unmapped value trying to save
    unMapStream:=TStringStream.Create('');
    try
      (EDCodeFrame.BinderFrm1.dgLeft.DataSource.Dataset as TClientDataset).SaveToStream(unMapStream, dfXML);
      if unMapStream.Size > 4 then // not NONE
      begin
        unMapDs:=TClientDataset.Create(nil);
        try
          unMapStream.Position := 0;
          unMapDs.LoadFromStream( unMapStream);
          unMapDs.First;
          while NOT unMapDs.Eof do
          begin
            if aResultList.IndexOfName(
                  unMapDs.FieldByName('DESCRIPTION').AsString
                                      ) < 0 then
            begin
               aResultList.Add(
                        unMapDs.FieldByName('DESCRIPTION').AsString
                      + '='
                      + unMapDs.FieldByName('CODE').AsString
                              );
            end;
            unMapDs.Next;
          end;
        finally
          unMapDs.Free;
        end;
      end;
    finally
      unMapStream.Free;
    end;

  Result := aResultList.Text;

  finally
    aResultList.Free;
  end;
end;

function TMainFm.MatchAlgToFile(XMLFileName:string):Boolean;
var
  locAlg:TeacAlgorithmClass;
  sInternal:string;
  sExternal:string;
begin
  Result := False;
  locAlg := TeacAlgorithmClass.Create(nil);
  try
    locAlg.XMLDocument.LoadFromFile(XMLFileName);
    sInternal := FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Division'];
    sExternal := locAlg.LookupTableMapping.LookupTableMapping['Override Division'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    sInternal := FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Branch'];
    sExternal := locAlg.LookupTableMapping.LookupTableMapping['Override Branch'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    sInternal := FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Dept'];
    sExternal := locAlg.LookupTableMapping.LookupTableMapping['Override Dept'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    sInternal := FcsvTransformAlg.LookupTableMapping.LookupTableMapping['Override Team'];
    sExternal := locAlg.LookupTableMapping.LookupTableMapping['Override Team'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;


    sInternal := FcsvTransformAlg.ColumnMapping.ColumnOutMapping['D/E'];
    sExternal := locAlg.ColumnMapping.ColumnOutMapping['D/E'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    sInternal := (FcsvTransformAlg as IeaAlgorithm).Producer.Producer[1];
    sExternal := locAlg.Producer.Producer[1];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    sInternal := (FcsvTransformAlg as IEaCSVHorzFieldValueArbitrator)
                                            .HorzFieldValueArbitrator['Hours'];
    sExternal := (locAlg as IEaCSVHorzFieldValueArbitrator)
                                            .HorzFieldValueArbitrator['Hours'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    sInternal := (FcsvTransformAlg as IEaCSVHorzFieldValueArbitrator)
                                            .HorzFieldValueArbitrator['Amount'];
    sExternal := (locAlg as IEaCSVHorzFieldValueArbitrator)
                                            .HorzFieldValueArbitrator['Amount'];
    if CompareText(sinternal, sExternal) <> 0 then Exit;

    result:= true;
  finally
    locAlg.Free;
  end;
end;

end.
