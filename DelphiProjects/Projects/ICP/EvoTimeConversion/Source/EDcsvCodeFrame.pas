unit EDcsvCodeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame
  ,VarHelperFunctions
  ;



type
  TEDCodeFrm = class(TCustomBinderBaseFrm)
    cdAlohaFieldNames: TClientDataSet;
    cdAlohaFieldNamesCODE: TIntegerField;
    cdAlohaFieldNamesDESCRIPTION: TStringField;
  private
    procedure InitCSVfielsNames;
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(CommaListDescriptionCode:string; matchTableContent: string; CO_E_D_CODES: TDataSet);
    procedure MergeChanges();
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, alohaparser;

{ TEeStatusFrm }

const
  EEStatusBinding: TBindingDesc =
    ( Name: 'EDCodeBinding';
      Caption: 'CSV field names mapped to Evolution E/D Codes';
      LeftDesc: ( Caption: 'CSV field names'; Unique: true; KeyFields: 'CODE'; ListFields: 'DESCRIPTION');
      RightDesc: ( Caption: 'Evolution company E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION');
      HideKeyFields: true;
    );

procedure TEDCodeFrm.MergeChanges;
begin
  (Self.BinderFrm1.dsBottom.DataSet as TClientDataSet).MergeChangeLog();
end;

constructor TEDCodeFrm.Create(Owner: TComponent);
begin
  inherited;
  InitCSVfielsNames;
end;

procedure TEDCodeFrm.Init(CommaListDescriptionCode:string; matchTableContent: string; CO_E_D_CODES: TDataSet);
const
  INIT_ED_FIELDS='Reg Hrs=10,OT Hrs=11,DT Hrs=12';
var
  sFieldsExtComma:string;
  aFieldList:TStringList;
  jLine:integer;
  jField:integer;
  sFieldId:string;
begin
  sFieldsExtComma:=MergeAddComma( INIT_ED_FIELDS, CommaListDescriptionCode);
  while cdAlohaFieldNames.RecordCount > 0 do
    cdAlohaFieldNames.Delete;
  cdAlohaFieldNames.MergeChangeLog;

  aFieldList:=TStringList.Create;
  try
    SplitSingleLine(sFieldsExtComma,',', aFieldlist);
    for jLine := 0 to aFieldList.Count - 1 do
    begin
       sFieldId:=System.Copy( aFieldList[jLine],
                              Pos('=',aFieldList[jLine])+1,
                              Length(aFieldList[jLine]));
       jField:= StrToInt(sFieldId);
       Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [jField,aFieldList.Names[jLine]]);
    end;
    cdAlohaFieldNames.MergeChangeLog;
  finally
    aFieldlist.Free;
  end;

  FBindingKeeper := CreateBindingKeeper( EEStatusBinding, TClientDataSet );
  CO_E_D_CODES.FieldByName('CUSTOM_E_D_CODE_NUMBER').DisplayLabel := 'E/D Code';
  CO_E_D_CODES.FieldByName('DESCRIPTION').DisplayLabel := 'Description';
  FBindingKeeper.SetTables(cdAlohaFieldNames, CO_E_D_CODES);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TEDCodeFrm.InitCSVfielsNames;
begin
  CreateOrEmptyDataSet( cdAlohaFieldNames );
// override what was read from component stream
  cdAlohaFieldNames.Fields[1].DisplayLabel:='CSV Field Name';

  Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [10,'Reg Hrs']);
  Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [11,'OT Hrs']);
  Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [12,'DT Hrs']);
//  Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [18,'Decl Tips']);
//  Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [19,'Meal Break Penalty']);
//  Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [20,'Split Pay']);


end;

end.



