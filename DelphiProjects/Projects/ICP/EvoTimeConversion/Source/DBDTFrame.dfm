inherited DBDTFrm: TDBDTFrm
  Width = 726
  Height = 513
  inherited BinderFrm1: TBinderFrm
    Top = 65
    Width = 726
    Height = 448
    OnResize = BinderFrm1Resize
    inherited evSplitter2: TSplitter
      Top = 211
      Width = 726
    end
    inherited pnltop: TPanel
      Width = 726
      Height = 211
      inherited evSplitter1: TSplitter
        Left = 153
        Height = 211
      end
      inherited pnlTopLeft: TPanel
        Width = 153
        Height = 211
        inherited evPanel3: TPanel
          Width = 153
        end
        inherited dgLeft: TReDBGrid
          Width = 153
          Height = 186
        end
      end
      inherited pnlTopRight: TPanel
        Left = 158
        Width = 568
        Height = 211
        inherited evPanel4: TPanel
          Width = 568
        end
        inherited dgRight: TReDBGrid
          Width = 568
          Height = 186
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 216
      Width = 726
      Height = 232
      inherited pnlbottom: TPanel
        Width = 726
        Height = 191
        inherited evPanel5: TPanel
          Width = 726
        end
        inherited dgBottom: TReDBGrid
          Width = 726
          Height = 166
        end
      end
      inherited pnlMiddle: TPanel
        Width = 726
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 726
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 208
      Height = 13
      Caption = 'Map Dept Code field from CSV file header to'
    end
    object Label2: TLabel
      Left = 328
      Top = 8
      Width = 24
      Height = 13
      Caption = 'code'
    end
    object cbLevel: TComboBox
      Left = 224
      Top = 5
      Width = 97
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = '(None)'
      OnSelect = cbLevelSelect
      Items.Strings = (
        '(None)')
    end
  end
  object cdAlohaBranchCodes: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdAlohaBranchCodesCODE: TStringField
      DisplayLabel = 'Dept Code'
      FieldName = 'CODE'
      Size = 6
    end
  end
end
