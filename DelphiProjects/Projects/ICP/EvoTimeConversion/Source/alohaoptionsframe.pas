unit alohaoptionsframe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, RateImportOptionFrame, alohadecl;

type

//ignores TAlohaOptions.TakeDBDTFromJobCodes

  TAlohaOptionsFrm = class(TFrame)
    RateImportOptionFrame: TRateImportOptionFrm;
    cbSummarize: TCheckBox;
    cbImportJobCodes: TCheckBox;
    cbIgnoreNotMappedFields: TCheckBox;
  private
    function GetOptions: TAlohaOptions;
    procedure SetOptions(const Value: TAlohaOptions);
  public
    property Options: TAlohaOptions read GetOptions write SetOptions;
  end;

implementation

{$R *.dfm}

{ TAlohaOptionsFrm }

function TAlohaOptionsFrm.GetOptions: TAlohaOptions;
begin
  Result.Summarize := cbSummarize.Checked;
  Result.ImportJobCodes := cbImportJobCodes.Checked;
  Result.RateImport := RateImportOptionFrame.Value;
  Result.IgnoreNotMappedFields := cbIgnoreNotMappedFields.Checked;
end;

procedure TAlohaOptionsFrm.SetOptions(const Value: TAlohaOptions);
begin
  cbSummarize.Checked := Value.Summarize;
  cbImportJobCodes.Checked := Value.ImportJobCodes and not Value.TakeDBDTFromJobCodes;
  cbImportJobCodes.Enabled := not Value.TakeDBDTFromJobCodes;
  RateImportOptionFrame.Value := Value.RateImport;
  cbIgnoreNotMappedFields.Checked := Value.IgnoreNotMappedFields;
end;

end.
