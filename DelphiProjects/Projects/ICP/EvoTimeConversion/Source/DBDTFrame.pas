unit DBDTFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient,
  CustomBinderBaseFrame, evodata, alohaparser, StdCtrls, ExtCtrls, evConsts,
  kbmMemTable;

type
  TDBDTFrm = class(TCustomBinderBaseFrm)
    cdAlohaBranchCodes: TClientDataSet;
    cdAlohaBranchCodesCODE: TStringField;
    Panel1: TPanel;
    cbLevel: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    procedure BinderFrm1Resize(Sender: TObject);
    procedure cbLevelSelect(Sender: TObject);
  private
    FDBDT: TkbmCustomMemTable;
    FMappingsContent: array [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] of string;
    procedure InitAlohaBatchIDs(alohaBatches: array of TAlohaBatch);
    procedure FilterDBDT(dbdtLevel: char);
    procedure HandleFilterDBDT(DataSet: TDataSet; var Accept: Boolean);
    procedure InitLevelCombo(maxlevel: char);
    procedure InitForCurrentLevel;
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; aDBDT: TkbmCustomMemTable; alohaBatches: array of TAlohaBatch);
    procedure UnInit;
    function SaveToString: string;
    function IsInitialized: boolean;
    procedure MergeChanges();
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, gdydbcommon, evoapiconnectionutils, gdyCommon, math;

{ TJobsFrm }

var
  DBDTBinding: TBindingDesc =
    ( Name: 'DBDTBinding';
      Caption: 'Aloha Batch IDs mapped to D/B/D/Ts';
      LeftDesc: ( Caption: 'Aloha Batch IDs'; Unique: true; KeyFields: 'CODE'; ListFields: 'CODE');

// Oleg Mitin - 2013-10-08 - Customer demand DBDT binding to be repeatable, so Unique is set to False
      RightDesc: ( Caption: 'Evolution D/B/D/Ts'; Unique: False; KeyFields: ''; ListFields: '');
      HideKeyFields: true;
    );

const
  sNoneOption = '(None)';
  sNoneMarker = 'NONE';

constructor TDBDTFrm.Create(Owner: TComponent);
begin
  inherited;
  cbLevel.Items.Text := sNoneOption;
  cbLevel.ItemIndex := 0;
end;

function GenKeyFields(dbdtLevel: char): string;
var
  level: char;
begin
  Result := '';
  for level := CLIENT_LEVEL_DIVISION to dbdtLevel do
  begin
    if Result <> '' then
      Result := Result + ';';
    Result := Result + CDBDTMetadata[level].CodeField;
  end
end;

function GenListFields(dbdtLevel: char): string;
var
  level: char;
begin
  Result := '';
  for level := CLIENT_LEVEL_DIVISION to dbdtLevel do
  begin
    if Result <> '' then
      Result := Result + ';';
    Result := Result + CDBDTMetadata[level].CodeField + ';' + CDBDTMetadata[level].NameField;
  end
end;

procedure TDBDTFrm.FilterDBDT(dbdtLevel: char);
var
  nFields: integer;
  function AreDifferent(v1, v2: Variant): boolean;
  var
    i: integer;
  begin
    if VarIsArray(v1) then
    begin
      for i := 0 to nFields-1 do
        if not VarIsEqual(v1[i], v2[i]) then
        begin
          Result := true;
          exit;
        end;
      Result := false;
    end
    else
      Result := not VarIsEqual(v1, v2);
  end;
var
  keyFields: string;
  keyValues: Variant;
  newKeyValues: Variant;
begin
  nFields := Ord(dbdtLevel) - Ord(CLIENT_LEVEL_COMPANY);
  keyFields := GenKeyFields(dbdtlevel);
  keyValues := Unassigned;
  FDBDT.IndexFieldNames := keyFields;
  FDBDT.Filtered := false;
  FDBDT.First;
  while not FDBDT.Eof do
  begin
    newKeyValues := FDBDT.FieldValues[keyFields];
    FDBDT.Edit;
    try
      FDBDT['TAG'] := Ord(VarIsEmpty(keyValues) or AreDifferent(keyValues, newKeyValues));
      FDBDT.Post;
    except
      FDBDT.Cancel;
      raise;
    end;
    keyValues := newKeyValues;
    FDBDT.Next;
  end;
  FDBDT.OnFilterRecord := HandleFilterDBDT;
  FDBDT.Filtered := true;
end;

procedure TDBDTFrm.HandleFilterDBDT(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := Dataset['TAG'] <> 0;
end;

procedure TDBDTFrm.InitLevelCombo(maxlevel: char);
var
  i: char;
begin
  cbLevel.Items.Text := sNoneOption;
  for i := CLIENT_LEVEL_DIVISION to maxlevel do
    cbLevel.Items.Add( AnsiLowerCase(CDBDTMetadata[i].Name) );
end;

function GetMatchTableDBDTLevel(matchTableContent: string): Variant;
var
  ss: TStringStream;
  matchTable: TClientDataSet;
  level: char;
begin
  if Length(Trim(matchTableContent)) = 0 then
  begin
// it is commonplace now, at the first connection to establish  
     Result := Null;
     Exit;
  end;

  if (matchTableContent = sNoneMarker)
  then
    Result := CLIENT_LEVEL_COMPANY
  else
  begin
    try
      Result := Null; //to make compiler happy
      ss := TStringStream.Create(matchTableContent);
      try
        matchTable := TClientDataSet.Create(nil);
        try
          matchTable.LoadFromStream(ss);
          for level := CLIENT_LEVEL_TEAM downto CLIENT_LEVEL_DIVISION do
            if matchTable.FindField('RIGHT_' + CDBDTMetadata[level].CodeField) <> nil then
            begin
              Result := level;
              exit;
            end;
          Assert(false);
        finally
          FreeAndNil(matchTable);
        end;
      finally
        FreeAndNil(ss);
      end;
    except
      Result := Null;
    end;
  end;
end;

procedure TDBDTFrm.Init(matchTableContent: string; aDBDT: TkbmCustomMemTable; alohaBatches: array of TAlohaBatch);
var
  maxlevel: char;
  v: Variant;
  level: char;
begin
  Assert(aDBDT <> nil);

  InitAlohaBatchIDs(alohaBatches);

  FDBDT := aDBDT;
  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
  begin
    FDBDT.FieldByName(CDBDTMetadata[level].CodeField).DisplayLabel := CDBDTMetadata[level].Name +' Code';
    FDBDT.FieldByName(CDBDTMetadata[level].NameField).DisplayLabel := CDBDTMetadata[level].Name +' Name';
  end;

  for level := low(FMappingsContent) to high(FMappingsContent) do
    FMappingsContent[level] := '';

  //need FDBDT here only to get company DBDT_LEVEL
  FDBDT.Filtered := false;
  FDBDT.First;

  level := CLIENT_LEVEL_COMPANY;
  maxlevel := //CLIENT_LEVEL_COMPANY;
              CLIENT_LEVEL_TEAM;
  if not FDBDT.Eof then
  begin
    Assert( Length(FDBDT.FieldByName('DBDT_LEVEL').AsString) = 1 );
//    maxlevel := FDBDT.FieldByName('DBDT_LEVEL').AsString[1];
    Assert( maxlevel in [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] );

    level := maxlevel;
    v := GetMatchTableDBDTLevel(matchTableContent);
    if not VarIsNull(v) and (VarToStr(v)[1] <= maxlevel) then
    begin
      level := VarToStr(v)[1];
      FMappingsContent[level] := matchTableContent;
    end;
  end;
  InitLevelCombo(maxlevel);
  cbLevel.ItemIndex := Ord(level) - Ord(CLIENT_LEVEL_COMPANY);
  InitForCurrentLevel;
end;

procedure TDBDTFrm.cbLevelSelect(Sender: TObject);
begin
  InitForCurrentLevel;
end;

procedure TDBDTFrm.InitForCurrentLevel;
var
  level: char;
  s: string;
  v: string;
begin
  if FBindingKeeper <> nil then
  begin
    s := FBindingKeeper.SaveMatchTableToString; //doesn't save here anything but this doesn't matter because frame will be reinitialized
    v := GetMatchTableDBDTLevel(s);
    Assert(not VarIsNull(v));
    FMappingsContent[ VarToStr(v)[1] ] := s;
  end;

  level := Char( Ord(CLIENT_LEVEL_COMPANY) + cbLevel.ItemIndex );
  if level <> CLIENT_LEVEL_COMPANY then
  begin
    if FBindingKeeper <> nil then
      FBindingKeeper.SetTables(nil, nil);  //we are about to work with FDBDT

    FilterDBDT(level);

    DBDTBinding.RightDesc.KeyFields := GenKeyFields(level);
    DBDTBinding.RightDesc.ListFields := GenListFields(level);
    DBDTBinding.RightDesc.Caption := 'Evolution ' + Plural(CDBDTMetadata[level].Name);
    DBDTBinding.Caption := 'Aloha Batch IDs mapped to Evolution ' + Plural(CDBDTMetadata[level].Name);
    FBindingKeeper := CreateBindingKeeper( DBDTBinding, TClientDataSet );
    FBindingKeeper.SetTables(cdAlohaBranchCodes, FDBDT);
    FBindingKeeper.SetVisualBinder(BinderFrm1);
    FBindingKeeper.SetMatchTableFromString(FMappingsContent[level]);
    FBindingKeeper.SetConnected(true);
  end
  else
    inherited UnInit;
end;

function TDBDTFrm.SaveToString: string;
begin
  if FBindingKeeper <> nil then //it is called from
    Result := inherited SaveToString
  else
    Result := sNoneMarker;
end;

procedure TDBDTFrm.UnInit;
begin
  inherited;
  InitLevelCombo(CLIENT_LEVEL_COMPANY);
  cbLevel.ItemIndex := 0;
  FDBDT := nil;
end;

procedure TDBDTFrm.InitAlohaBatchIDs(alohaBatches: array of TAlohaBatch);
var
  n: integer;
begin
  CreateOrEmptyDataSet( cdAlohaBranchCodes );
  for n := 0 to high(alohaBatches) do
    if trim(alohaBatches[n].BatchID) <> '' then
      if not cdAlohaBranchCodes.Locate('CODE', alohaBatches[n].BatchID, []) then
        Append(cdAlohaBranchCodes, 'CODE', [alohaBatches[n].BatchID]);

  ResortRequested(cdAlohaBranchCodes, 'CODE', false);
end;

procedure TDBDTFrm.BinderFrm1Resize(Sender: TObject);
begin
//don't call inherited
end;


function TDBDTFrm.IsInitialized: boolean;
begin
  Result := FDBDT <> nil;
end;

procedure TDBDTFrm.MergeChanges;
begin
  (Self.BinderFrm1.dsBottom.DataSet as TClientDataSet).MergeChangeLog();
end;

end.



