unit EvoCsvTransformWrkMapperClasses;

interface
uses
  SysUtils
 ,Classes
 ,Windows
 ,Variants
 ,Math
 ,ExtCtrls
 ,StrUtils
 ,xmldom, XMLIntf, msxmldom, XMLDoc
 ,VarHelperFunctions
 ,EvoCsvTransformMapper
 ,EvoCsvTransformMapperClasses
 ,EaTextStreaming

 ,Forms, Dialogs
 ;

type
  TTempLocator = record
                   TempErrLog:string;
                   TempFolder:string;
                   OutputFile:string;
                   ErrorLog:string;
                 end;

  TCsvError=(csvErrColMap, csvErrLookup, csvErrUnknown);

  ECsvTransformException=class(Exception)
  private
    FKind:TCsvError;
  public
    property Kind:TCsvError read FKind write FKind;
    constructor Create( Kind: TCSVError; Message:string); 
  end;

  TCsvRecordProcessed=procedure (Sender:TObject; RowProcessedCount:integer) of object;
  TCsvErrorDetected=procedure ( Sender:TObject;
                                RowProcessedCount:integer;
                                Column:integer;
                                Err:TCsvError;
                                Row:string;
                                ErrorMessage:string
                                ) of object;


  THeadLineUse=(headLineUseUnknown,headLineUseNotUsed,headLineUseUsed);

  TConditionFunction = function(Content:string):boolean of object;

  TeacAlgorithmClass=class(TeabAlgorithmClass, IEaCSVTransform)
  private
    FErrorOutputExcludeList: TStringList;
    
    FIntakeHeaderLine: string;
    FIntakeHeaderLines: TStringList;
    FOutToInpLine: TStringList;

    FDelimiter: string;
    FSourceHeaderUse: THeadLineUse;
    FTargetHeaderUse: THeadLineUse;
    FTempFolder: string;
    FTempFileName: string;
    FSourceFile: string;
    FTargetFile: string;
    FErrorLogFile: string;
    FErrorList: TStrings;

    FErrorStream: TFileStream;
    FInputCounterFlag: boolean;
    FInputCounter: integer;

    FOutputLineCounter: integer;

    FErrorDetected: TCsvErrorDetected;
    FRecordProcessed: TCsvRecordProcessed;
    FIntakeRecordProcessed: TCsvRecordProcessed;

    procedure UpdateInputCounter();
    function GetInputLineMessagePart(jOutputCounter: integer): string;
    function OutputIdToOutputName(outID: integer): string;
    function inputIDToInputName(jIn: integer): string;
    procedure SetIntakeHeaderLineFromFile(OriginalFileName: string);
    function InputFieldNodeCalculatedID(CheckFieldNode: IXMLNode): integer;
    function argXMLValueFromNameList(aInputValues: TStrings;
      argNode: IXMLNode): string;
    function argXMLValueIntByName(argNode: IXMLNode): integer;
    function OutputLineNumberWordingExcludeErrorLines(
      FOutputLineCounter: integer): string;
  protected

    function BooleanAttributeIsYes(node: IXMLNode; AttrName:string): boolean;
    function ContentEmpty(content:string):boolean;
    procedure ExecSubstLeaderEmptiesFollowersOnCondition(
      var sList: Tstrings; Leader: IXMLNode; FollowerList: IXMLNodeList;
      funct: TConditionFunction; inputOutput:string);
    function ContentNotEmpty(content: string): boolean;
    procedure OutputGeneration( node:IXMLNode; sLine:string; writeStream:TStream);
    procedure OutputGenerationFirstLineProcess(node: IXMLNode;
      sLine: string; writeStream: TStream);
    function OutputHeaders: string;
    procedure WriteString(writeStream: TStream; sWrite: string);
    procedure ProcessOutputDescriptor(sLine:string;jOutputId: integer;
      iDescriptNode: IXMLNode; var aOutList: TStrings; OutFieldNode:IXMLNode);
    function argXMLValueFromList(aInputValues: TStrings;
      argNode: IXMLNode): string;
    function ConcatFields(aInputValues: TStrings;
      concatDescriptionNode: IXMLNode): string;
    function LookupReplace(aInputValues: Tstrings; iLookupNode: IXMLNode):string;
    function argXMLValueInt(argNode: IXMLNode): integer;
    function SelectorConstReplace(aInputValues: TStrings;
      iSelectorNode: IXMLNode;
      jOutputId:integer; // id of output column for error message generation
      OutFieldNode:IXMLNode
      ): string;
    function SelectorContentReplace(aInputValues: TStrings;
      iSelectorNode: IXMLNode): string;
    function StaticValueReplace(iStaticContentNode: IXMLNode): string;
    function CommaCount(s: string): integer;
    function GetOutputColCount: integer;

     procedure ProducerFirstLineProcess(producerFieldList: iXmlNodeList;
      sLine: string; writeStream: TStream);
     procedure ApplyProducerOnLine(iFieldList: IXMLNodeList; sLine: string;
      writeStream: TStream; jReadLineCounter:integer);
     procedure WriteStringList(writeStream:TStream;sList:TStrings;ColCount:integer=0);
     procedure ExeLinkRecordSubstitutions(sLine: string; node: IXMLNode);

     function isStringEmptyOrZero(s:string):boolean ;
     procedure ExeProducersNode(node:IXMLNode);
     procedure ExeLinkSubstitutions(node: IXMLNode; InputOutput:string);
     procedure ExeOutputGeneration(node:IXMLNode);
     procedure TransferOutputFromTemp();
     function  GetCurrentIntakeFile:string;
     procedure GenerateTargetHeaderLine( writeStream:TStream);
     procedure ProcessSingleProducer( iProducerNode:IXMLNode );
     procedure ApplyProducerOnCurrentLine(
                                  iFieldList:IXMLNodeList;
                                  sList:TStrings;
                                  writeStream:TStream;
                                  jReadLineCounter:integer
                                         );

    procedure LinkSubstitution(allLinksNode: iXmlNode;
      sLine: string; writeStream: TStream; inputOutput:string);
    procedure ElementaryLinkSubstitution(elemSubstNode: IXMLNode;
      var sList: TStrings; writeStream: TStream; inputOutput:string);

    procedure LinkSubstitutionFirstLineProcess(
                                  allLinksNode:iXmlNode;
                                  sLine:string;
                                  writeStream:TStream;
                                  inputOutput:string
                                                     );
     class function IsLineListHeadersOnly(List:TStrings): boolean;



  { IEaCSVTransform interface}
     function CSVTransformApply(CSVinputFile, CSVoutputFile,ErrLogFile:string):integer;
  { end of IEaCSVTransform interface}
     property SourceHeaderUse:THeadLineUse read FSourceHeaderUse write FSourceHeaderUse;
     property TargetHeaderUse:THeadLineUse read FTargetHeaderUse write FTargetHeaderUse;
     property Delimiter:string read FDelimiter write FDelimiter;
  public

    {IeaAlgorithm interface}
     function Transformer:IEaCSVTransform; override;
     function setProperty(nodeName:string; nodeValue:string):boolean; virtual;
     function getProperty(nodeName:string):string; virtual;
    { end of  IeaAlgorithm interface}

    class function GetTmpFolder():string;
    function GetNewTmpFileName():string;
    procedure PopulateLocator(var Locator: TTempLocator);

    constructor Create(Aowner:TComponent); override;
    destructor Destroy; override;
    property ErrorList:TStrings read FErrorList;
    property OnErrorDetected:TCsvErrorDetected read FErrorDetected write FErrorDetected;
    property OnRecordProcessed:TCsvRecordProcessed read FRecordProcessed write FRecordProcessed;
    property OnIntakeRecordProcessed:TCsvRecordProcessed read FIntakeRecordProcessed write FIntakeRecordProcessed;
    property OutToInpList:TStringList read FOutToInpLine;
    property IntakeHeaderLine:string read FIntakeHeaderLine;
  end;

  procedure GetlistOfValuesFromCsvFileCol(FileName:string; ColName:string; OutList:TStringList);

implementation


procedure GetlistOfValuesFromCsvFileCol(FileName:string; ColName:string; OutList:TStringList);
var
  tsFile:TTextStream;
  sLine:string;
  aLine:TStringList;
  iCol:integer;
  bFirstRead:Boolean;
begin
  aLine:=TStringlist.Create;
  try
    tsFile := TTextStream.Create(TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite));
    try
       bFirstRead:=tsFile.ReadLn(sLine);
       if bFirstRead then begin
          SplitSingleLine(UpperCase(sLine), ',', aLine);
          iCol:=aLine.IndexOf(UpperCase(ColName));
          if iCol < 0 then
            Raise Exception.Create('Wrong format of CSV file : missing column '+ColName);

          while tsFile.ReadLn(sLine) do
          begin
             SplitSingleLine(sLine, ',', aLine);
             if aLine.Count > iCol then
              OutList.Add(aLine[iCol]);
          end;
       end;
    finally
      tsFile.Free;
    end;
  finally
    aLine.Free;
  end;
end;

{ ECsvTransformException }

constructor ECsvTransformException.Create(Kind: TCSVError;
  Message: string);
begin
  inherited Create(Message);
  Self.FKind:=Kind;
end;


{ TeacAlgorithmClass }

function TeacAlgorithmClass.Transformer: IEaCSVTransform;
begin
  Result:=Self;
end;

procedure TeacAlgorithmClass.PopulateLocator( var Locator:TTempLocator);
begin

   Locator.TempFolder := GetTmpFolder();
   ForceDirectories(Locator.TempFolder);
   Locator.OutputFile := Locator.TempFolder+GetNewTmpFileName();
   Locator.ErrorLog := Locator.TempFolder+GetNewTmpFileName();
   Locator.TempErrLog := Locator.TempFolder+GetNewTmpFileName();
end;

function TeacAlgorithmClass.CSVTransformApply(CSVinputFile, CSVoutputFile,
  ErrLogFile: string): integer;
var
  RootNode:IXMLNode;
  iNode:integer;
  operNode:IXMLNode;

begin
   FInputCounterFlag:=false;
   FInputCounter:=0;
   FOutToInpLine.Clear;
   FErrorOutputExcludeList.Clear;

   FTempFolder:=GetTmpFolder();
   ForceDirectories(FTempFolder);
   if Not FileExists(CSVinputFile) then
   begin
     Raise Exception.Create('Missing file :'+CSVinputFile);
   end;
   FSourceFile:=CSVinputFile;
   FTargetFile:=CSVoutputFile;
   FErrorLogFile:=ErrLogFile;
   FErrorStream:=nil;

   SetIntakeHeaderLineFromFile( CSVinputFile );

   try
     Result:=-1;
     RootNode:=Self.XMLDocument.DocumentElement;


     for  iNode:=0 to RootNode.ChildNodes.Count-1 do
     begin
       operNode:= RootNode.ChildNodes[iNode];
       if operNode.NodeType=ntElement then
       begin
           // split input rows into multiple rows, if necessary
           if CompareText(operNode.NodeName,'input_field_new_line_producers') = 0
           then
             ExeProducersNode(opernode)
           else
            // field conditional operations, if necessary
           if CompareText(operNode.NodeName, 'input_field_links') = 0 then
             ExeLinkSubstitutions(opernode,'input')
           else
           // produce output
           if CompareText(operNode.NodeName, 'output_fields') = 0 then
             ExeOutputGeneration(opernode);
            // field conditional operations, if necessary
           if CompareText(operNode.NodeName, 'output_field_links') = 0 then
             ExeLinkSubstitutions(opernode,'output')
       end;
     end;
     TransferOutputFromTemp();
   finally
      // delete temp folder with all content, nil parent handler
     DeleteShellTree(0, FTempFolder);
     // clear off
     FTempFolder := '';
     FTempFileName:='';
     FIntakeHeaderLines.Clear;
     FIntakeHeaderLine:='';
   end;
end;



procedure TeacAlgorithmClass.ExeProducersNode(node:IXMLNode);
var
  iProducerNodeList:IXMLNodeList;
  iProducerNode:IXMLNode;
  iPro:integer;
begin
 // split input rows into multiple rows, if necessary
  iProducerNodeList:=SelectNodes( node, WideString(
         './input_field_new_line_producer'             )
                                );
  for iPro:=0 to iProducerNodeList.Count-1 do
  begin
    iProducernode:=iProducerNodeList[iPro];
    ProcessSingleProducer( iProducerNode );
  end;
end;

procedure TeacAlgorithmClass.ExeLinkRecordSubstitutions(sLine:string; node:IXMLNode);
var
  iNode:integer;
  iLinkNode:IXMLNode;
begin
// code for conditional fields transformations
  for iNode:=0 to node.ChildNodes.Count-1 do
  begin
     iLinkNode:=node.ChildNodes[iNode];
     if NOT VarIsNull( iLinkNode.Attributes
                          ['empty_follower_if_leader_is_empty']  )
     then
     begin
        if AnsiCompareText ( iLinkNode.Attributes
                                  ['empty_follower_if_leader_is_empty'],
                             'yes')=0
        then
        begin

        end;
     end;
     if NOT VarIsNull( iLinkNode.Attributes
                                  ['empty_follower_if_leader_has_content'] )
     then
     begin
        if AnsiCompareText ( iLinkNode.Attributes
                                  ['empty_follower_if_leader_has_content'],
                             'yes')=0
        then
        begin

        end;
     end;
  end;
end;

procedure TeacAlgorithmClass.ExeOutputGeneration(node:IXMLNode);
var
  CurrentFileName:string;
  bFirstRead:boolean;
  tsFile:TTextStream;
  writeStream:TFileStream;
  sLine:string;
begin
  FOutputLineCounter:=0;
  CurrentFileName:= GetCurrentIntakeFile();
  FTempFileName:=GetNewTmpFileName();
  writeStream:=TFileStream.Create(FTempFileName, fmCreate	 or fmShareDenyWrite	);
  try
    tsFile := TTextStream.Create(TFileStream.Create(CurrentFileName, fmOpenRead or fmShareDenyWrite));
    try
        bFirstRead:=tsFile.ReadLn(sLine);
        FOutputLineCounter:=FOutputLineCounter+1;
        if  bFirstRead then
        begin
          OutputGenerationFirstLineProcess(node, sLine, writeStream);
          if Assigned(FRecordprocessed) then
                FRecordprocessed(Self, FOutputLineCounter);

          while tsFile.ReadLn(sLine) do
          begin
            FOutputLineCounter:=FOutputLineCounter+1;
            OutputGeneration( node, sLine, writeStream);
            if Assigned(FRecordprocessed) then
                FRecordprocessed(Self, FOutputLineCounter);
          end;
        end
        else
          Raise ECsvTransformException.Create(csvErrUnknown,'Intermediate transformation failed');
    finally
      tsFile.Free;
      FInputCounterFlag:=True;
    end;
  finally
    //writeStream.Free;
    FreeAndNil(writeStream);  
    if Assigned(FErrorStream) then
      FreeAndNil(FErrorStream);
  end;
end;

procedure TeacAlgorithmClass.TransferOutputFromTemp;
var
 sLine:string;
 tsFile:TTextStream;
 CurrentFileName:string;
 writeStream:TFileStream;
 sList:TStringList;
 ColumnCount:integer;
begin
// we generate new intermediate file, for exact comma number control
      ColumnCount:=Self.GetOutputColCount();
      CurrentFileName:= GetCurrentIntakeFile();
      FTempFileName:=GetNewTmpFileName();
      writeStream:=TFileStream.Create(FTempFileName, fmCreate	 or fmShareDenyWrite	);
      try
        tsFile := TTextStream.Create(TFileStream.Create(CurrentFileName, fmOpenRead or fmShareDenyWrite));
        try
          while tsFile.ReadLn(sLine) do
          begin
            sList:=TStringList.Create;
            try
              SplitSingleLine(sLine, Self.Delimiter, sList);
              WriteStringList(writeStream, sList,  ColumnCount);
            finally
              sList.Free;
            end;
          end;
        finally
          tsFile.Free;
        end;
      finally
        //writeStream.Free;
        FreeAndNil(writeStream);
      end;


    CopyFile(PChar(FTempFileName), PChar(FTargetFile), false);
end;

class function TeacAlgorithmClass.GetTmpFolder: string;
var
  tempFolder: array[0..MAX_PATH] of Char;
  BaseFolder:string;
  Guid: TGUID;
  sGuid:string;
begin
  GetTempPath(MAX_PATH, @tempFolder);
  BaseFolder := string(tempFolder)+'ISystems\EvoA2CSV\';
  CreateGuid(Guid);
  sGuid:=AnsiReplaceText(AnsiReplaceText( GUIDToString(Guid), '{',''), '}','');

  Result:=IncludeTrailingPathDelimiter(BaseFolder)+sGuid+'\';
end;

function TeacAlgorithmClass.GetCurrentIntakeFile: string;
begin
  if Length( FTempFileName ) = 0 then
    Result:=FSourcefile
  else
    Result:= FTempFileName;
end;

function TeacAlgorithmClass.GetNewTmpFileName: string;
var
  Guid: TGUID;
  sGuid:string;
begin
  CreateGuid(Guid);
  sGuid:=AnsiReplaceText(AnsiReplaceText( GUIDToString(Guid), '{',''), '}','');
  Result:= FTempFolder+sGuid+'.csv';
end;


class function TeacAlgorithmClass.IsLineListHeadersOnly(List:TStrings): boolean;
var
  iRow:integer;
  iCol:integer;
  c:Char;
begin
// headers means at least one no-number no-space in each field
  Result:=true;
  for iRow:=0 to List.Count-1 do
  begin
     if Length(List[iRow])=0 then
     begin
       // there can not be empty headers in header line
        Result:=false;
        exit;
     end;
     Result:=False;
     for iCol:=1 to Length(List[iRow]) do
     begin
        c := List[iRow][iCol];
        if
            (Ord(c) > Ord(' '))
        then
        begin
          Result:=true;
          Break;
        end;
     end;
  end;
end;


procedure TeacAlgorithmClass.ProcessSingleProducer(
  iProducerNode: IXMLNode);
var
 iFieldList:IXMLNodeList;
 bFirstRead:boolean;
 sLine:string;
 tsFile:TTextStream;
 Headerlist:TStringList;
 CurrentFileName:string;
 writeStream:TFileStream;
 jReadLineCounter:integer;
begin
// we generate new intermediate file, splitting lines
// and leaving only one value per listed columns
  HeaderList:=TStringList.Create;
  try
    iFieldList:=SelectNodes( iProducerNode, WideString(
           './input_field_new_line_producer_fields/input_field'             )
                                  );
    if iFieldList.Count>0 then
    begin
      CurrentFileName:= GetCurrentIntakeFile();
      FTempFileName:=GetNewTmpFileName();
      jReadLineCounter := 0;
      FOutputLineCounter:=0;
      writeStream:=TFileStream.Create(FTempFileName, fmCreate	 or fmShareDenyWrite	);
      try

      tsFile := TTextStream.Create(TFileStream.Create(CurrentFileName, fmOpenRead or fmShareDenyWrite));
      try
        // First line may contain headers only
        bFirstRead:=tsFile.ReadLn(sLine);
        if bFirstRead then
          // at least one line in document
        begin
           Inc(jReadLineCounter);
        // check for headers
           ProducerFirstLineProcess(iFieldList,sLine,writeStream);
        end
        else
        begin
           // generate error line displaying that empty document processed
        end;

        while tsFile.ReadLn(sLine) do
        begin
          //sLine is your line
          Inc(jReadLineCounter);
          ApplyProducerOnLine(iFieldList, sLine, writeStream, jReadLineCounter);

        end;
      finally
        tsFile.Free;
        FInputCounterFlag:=true;

      end;
      finally
        //writeStream.Free;
        FreeAndNil(writeStream);
      end;

    end;
  finally
    HeaderList.Free;
  end;
end;




procedure TeacAlgorithmClass.ProducerFirstLineProcess(
                                  producerFieldList:iXmlNodeList;
                                  sLine:string;
                                  writeStream:TStream
                                                     );
var
  SList:TStringList;
begin
  sList:=TStringList.Create;
  SplitSingleLine(sLine, Self.Delimiter, sList);
  try
    case Self.SourceHeaderUse of
        headLineUseUnknown:
            begin
                if IsLineListHeadersOnly(sList) then
                begin
                   Self.SourceHeaderUse:=headLineUseUsed;
                   case Self.TargetHeaderUse of
                   headLineUseUnknown: begin
                                         Self.TargetHeaderUse:=headLineUseNotUsed;
                                         // and do not copy header line to output;
                                       end;
                   headLineUseUsed: begin
                                       GenerateTargetHeaderLine(writeStream);
                                    end;
                    headLineUseNotUsed:begin
                                        // do nothing - output header generation omitted
                                       end;
                   end;
                end;
            end;
        headLineUseNotUsed:
            begin
               ApplyProducerOnCurrentLine( producerFieldList, sList, writeStream, 1);
            end;
        headLineUseUsed:
            begin
                GenerateTargetHeaderLine(writeStream);
            end;
    end;

  finally
    sList.Free;
  end;
end;

procedure TeacAlgorithmClass.GenerateTargetHeaderLine( writeStream:TStream);
begin
{ TODO : Insert code for generating output header line from algorithm }
end;

procedure  TeacAlgorithmClass.ApplyProducerOnLine(
                                        iFieldList: IXMLNodeList;
                                        sLine: string;
                                        writeStream: TStream;
                                        jReadLineCounter:integer
                                        );
var
  sList:TStringList;
begin
  sList:=TStringList.Create;
  try
    SplitSingleLine(sLine, Self.Delimiter, sList);
    ApplyProducerOnCurrentLine(iFieldList,sList, writeStream, jReadLineCounter);
    UpdateInputCounter();
  finally
      sList.Free;
  end;
end;

function TeacAlgorithmClass.CommaCount(s:string):integer;
var
  j:integer;
begin
  Result:=0;
  for j:=1 to Length(s) do
        if s[j]=',' then Result:=Result+1;
end;

function TeacAlgorithmClass.GetOutputColCount:integer;
var
  nodeList:IXMLNodelist;
  aList:TStringList;
  nNode:integer;
  i:integer;
begin
   if NOT Self.XMLDocument.Active then
      Self.XMLDocument.Active := true;

   nodeList:=SelectNodes(Self.XMLDocument.DocumentElement , WideString(
   '//output_field/@output_id'));

   aList:=TstringList.Create;
   try
    aList.Sorted:=true;
    aList.Duplicates:=dupIgnore	;
    for i:=0 to nodeList.Count-1 do
    begin
        nNode:=argXMLValueInt(nodeList[i]);
        aList.Add(Format('%6.6d',[nNode]));
    end;
    Result:=StrToInt(aList[aList.Count-1]);
   finally
      aList.Free;
   end;
end;

procedure TeacAlgorithmClass.WriteStringList(writeStream:TStream;sList:TStrings;ColCount:integer=0);
var
    sWrite:string;
    pWrite:PChar;
    jCommas:integer;
    i:integer;
begin
    sWrite:=sList.Text;
    sWrite:=AnsiReplaceText(sWrite,#13#10,',');
// it is last operation in output producing -
// we may need to remove last extra comma at the end of line
    if ColCount > 0 then
    begin
      jCommas:=CommaCount(sWrite);
      for i:=1 to ColCount-1-jCommas do
        sWrite:=sWrite+',';

      while (CommaCount(sWrite)> ColCount-1) and (AnsiEndsStr(',', sWrite)) do
        sWrite:=System.Copy(sWrite, 1, Length(sWrite)-1);
    end;

    if NOT AnsiEndsText(#13#10, sWrite) then
       sWrite:=sWrite+#13#10;
    pWrite:=PChar(sWrite);
    writestream.WriteBuffer(pWrite^, Length(swrite));
end;

procedure TeacAlgorithmClass.WriteString(writeStream:TStream;sWrite:string);
var
    pWrite:PChar;
begin
    if AnsiEndsText(#13#10, sWrite) then
      pWrite := PChar(sWrite)
    else
      pWrite:=PChar(sWrite+#13#10);
    writestream.WriteBuffer(pWrite^, Length(swrite));
end;

procedure TeacAlgorithmClass.ApplyProducerOnCurrentLine(
  iFieldList: IXMLNodeList; sList: TStrings; writeStream: TStream;
  jReadLineCounter:integer);
var
  iProdField:integer;
  aCopyList:TStringList;
  CheckFieldNode:IXMLNode;
  idCheckField:integer;
  bOutputProduced:boolean;
  sCheckValue:string;
  iCounterEmpty:integer;
  idFieldToEmpty:integer;

begin
  if iFieldlist.Count=0 then
  begin

    WriteStringList(writeStream, sList);
  end
  else
  begin
    bOutputProduced:=false;
    aCopyList:=TStringList.Create;
    try
      for iProdField:=0 to iFieldList.Count-1 do
      begin
         aCopyList.Assign(sList);
         CheckFieldNode:=iFieldList[ iProdField];
         idCheckField := InputFieldNodeCalculatedID(CheckFieldNode);
         if idCheckField >= 0 then
         begin
         // in case header list Has more fields than regular line
           while aCopyList.Count <  idCheckField + 1 do
              aCopyList.Add('');

           sCheckValue:=aCopyList[idCheckField];
           if NOT isStringEmptyOrZero(sCheckValue) then
           begin
              // empty all Producer fields except one in consideration
              bOutputProduced:=true;
              for  iCounterEmpty:=0 to  iFieldList.Count-1 do
              begin
                idFieldToEmpty:=InputFieldNodeCalculatedID(iFieldList[iCounterEmpty]);
                if idFieldToEmpty >= 0 then
                  if idFieldToEmpty <> idCheckField then
                  begin
                  // in case list of fields is bigger than regular line
                      while aCopyList.Count < idFieldToEmpty + 1 do
                        aCopyList.Add('');
                      aCopyList[idFieldToEmpty]:='';
                  end;
              end;
              // generate extra line - number of lines increased
              Inc(FOutputLineCounter);
              FOutToInpLine.Add(IntTostr(FOutputLineCounter) + '=' + IntToStr(jReadLineCounter));
              WriteStringList(writeStream, aCopyList);
           end;
         end;
      end;

      if NOT bOutputProduced and (sList.Count > 0) then // all Producer fields are empty;
      begin
         Inc(FOutputLineCounter);
         FOutToInpLine.Add(IntTostr(FOutputLineCounter) + '=' + IntToStr(jReadLineCounter));
         WriteStringList(writeStream, sList);  // we provide copy of intake string, then
      end;
    finally
    end;
  end;
end;

function TeacAlgorithmClass.isStringEmptyOrZero(s: string): boolean;
var
  z:double;
  ier:integer;
begin
  result:=False;
  if Length(s)=0 then
  begin
      Result:=true;
      Exit;
  end;
  Val(s,z,ier); // some string
  if ier<>0 then begin
      Result:=true;
      Exit;
  end;

  if Round(z *100)=0.0 then
    Result:=true;

end;
 { TODO : Insert crosslink between producers so only one producer would generate output for all-empty-values }
 // so far we are getting out with single producer in algo

 // input_field_link
procedure TeacAlgorithmClass.ExeLinkSubstitutions(node: IXMLNode; InputOutput:string);
var
  CurrentFileName:string;
  bFirstRead:boolean;
  tsFile:TTextStream;
  writeStream:TFileStream;
  sLine:string;
begin
  FOutputLineCounter := 0;
  CurrentFileName:= GetCurrentIntakeFile();
  FTempFileName:=GetNewTmpFileName();
  writeStream:=TFileStream.Create(FTempFileName, fmCreate	 or fmShareDenyWrite	);
  try
    tsFile := TTextStream.Create(TFileStream.Create(CurrentFileName, fmOpenRead or fmShareDenyWrite));
    try
        try
          bFirstRead:=tsFile.ReadLn(sLine);
          FOutputLineCounter:=FOutputLineCounter+1;
          if  bFirstRead then
          begin
            LinkSubstitutionFirstLineProcess(node, sLine, writeStream, InputOutput);
            while tsFile.ReadLn(sLine) do
            begin
              FOutputLineCounter:=FOutputLineCounter+1;
              LinkSubstitution( node, sLine, writeStream, InputOutput);
            end;
          end
          else
            Raise ECsvTransformException.Create(csvErrUnknown,'Intermediate transformation failed');
        except
          on E:Exception do
          begin
             Raise Exception.Create('Output line '+IntToStr(FOutputLineCounter)+';'+E.Message);
          end;
        end;
    finally
      tsFile.Free;
      FInputCounterFlag:=true;
    end;
  finally
    //writeStream.Free;
    FreeAndNil(writeStream);
  end;
end;



procedure TeacAlgorithmClass.LinkSubstitutionFirstLineProcess(
                                  allLinksNode:iXmlNode;
                                  sLine:string;
                                  writeStream:TStream;
                                  inputOutput:string
                                                     );
var
  SList:TStringList;
  isFirstLineHeaders:boolean;
begin


  sList:=TStringList.Create;
  try
    SplitSingleLine(sLine, Self.Delimiter, sList);
    isFirstLineHeaders:=IsLineListHeadersOnly(sList);

    if isFirstLineHeaders then
    begin
       WriteStringList(writeStream,sList); // just copy headers
    end
    else
    begin
        LinkSubstitution( allLinksNode, sLine, writeStream, inputOutput);
    end;

  finally
    sList.Free;
  end;
end;

procedure TeacAlgorithmClass.LinkSubstitution( allLinksNode:iXmlNode;
                                  sLine:string;
                                  writeStream:TStream;
                                  inputOutput:string
                                  );
var
  SList:TStrings;
  SingleLinkList:IXMLNodeList;
  elemSubstNode:IXMLNode;
  iNode:integer;

begin
  sList:=TStringList.Create;
  try
      SplitSingleLine(sLine, Self.Delimiter, sList);
      SingleLinkList := SelectNodes( allLinksNode, WideString(
        './'+inputOutput+'_field_link'));

      for iNode := 0 to SingleLinkList.Count - 1 do
      begin

            elemSubstNode:=SingleLinkList[iNode];
            ElementaryLinkSubstitution( elemSubstNode,  sList, writeStream,inputOutput);

      end;
      WriteStringList(writeStream, sList);
      UpdateInputCounter();
  finally
    sList.Free;
  end;
end;

function TeacAlgorithmClass.BooleanAttributeIsYes(node:IXMLNode; AttrName:string):boolean;
begin
  result:=false;
  if NOT VarIsNull(node.Attributes[AttrName])  then
  begin
    Result := CompareText(node.Attributes[AttrName],'yes')=0
  end
end;

procedure TeacAlgorithmClass.ElementaryLinkSubstitution( elemSubstNode:IXMLNode;
                                                         var sList:TStrings;
                                                         writeStream:TStream;
                                                         inputOutput:string
                                                         );
var
   LeaderList:IXMLNodeList;
   Followerlist:IXMLNodeList;
begin
   LeaderList:= SelectNodes( elemSubstNode, WideString(
    './'+inputOutput+'_field_link_leader/'+inputOutput+'_field'));
   FollowerList:= SelectNodes( elemSubstNode, WideString(
   './'+inputOutput+'_field_link_follower/'+inputOutput+'_field/@'+inputOutput+'_id'));

   if (LeaderList.Count>0) and ( FollowerList.Count > 0) then
   begin
      if BooleanAttributeIsYes(
              elemSubstNode, 'empty_follower_if_leader_is_empty') then
           ExecSubstLeaderEmptiesFollowersOnCondition(sList, LeaderList[0],FollowerList,ContentEmpty,inputOutput) ;
      if BooleanAttributeIsYes(
              elemSubstNode, 'empty_follower_if_leader_has_content') then
           ExecSubstLeaderEmptiesFollowersOnCondition(sList, LeaderList[0],FollowerList,ContentNotEmpty,inputOutput) ;
   end;
end;

procedure TeacAlgorithmClass.ExecSubstLeaderEmptiesFollowersOnCondition
                                ( var sList:Tstrings;
                                  Leader:IXMLNode;
                                  FollowerList:IXMLNodeList;
                                  funct:TConditionFunction;
                                  inputOutput:string
                                  );
var
  idNode:IXMLNode;
  i:integer;
  aIdList:TStringList;
  iLeaderId:integer;
begin
  aIdList:=TStringList.Create;
  try
    iLeaderId:=StrToInt(Leader.Attributes[inputOutput+'_id']);
    if // indexing in XML starts from 1; indexing in Lists starts from 0
        funct( sList[iLeaderId-1])
    then
    begin
      for i:=0 to FollowerList.Count-1 do
      begin
          idNode:= FollowerList[i];
              aIdList.Add(copy(idNode.XML, Pos('=',idNode.XML)+1, Length(idNode.XML)));
              aIdList[aidlist.Count-1]:=AnsiReplaceStr( aIdList[aidlist.Count-1],'"','');
      end;
      for i:=0 to aIdList.Count-1 do
      begin
      // empty followers if condidtion met
          sList[StrToInt(aIdList[i])-1]:='';
      end;
    end
  finally
     aIdList.Free;
  end;
end;

function TeacAlgorithmClass.ContentEmpty(content: string): boolean;
begin
  Result:= Length(content)=0;
end;

function TeacAlgorithmClass.ContentNotEmpty(content: string): boolean;
begin
  Result:= Length(content)<>0;
end;

function TeacAlgorithmClass.GetInputLineMessagePart(jOutputCounter:integer):string;
var i:integer;
begin
  Result:=' ';
  for i:=0 to FOutToInpLine.Count-1 do
  begin
    if AnsiStartsText(IntToStr(jOutputCounter)+'=', FOutToInpLine[i]) then
    begin
      Result:=Result + 'Intake line '+ System.Copy(FOutToInpLine[i],
                                                   Pos('=',FOutToInpLine[i])+1,
                                                   Length(FOutToInpLine[i])
                                                   )
                                     +' ';
    end;
  end;
end;

procedure TeacAlgorithmClass.OutputGeneration( node:IXMLNode; sLine:string; writeStream:TStream);
var
  iOutputFields:IXMLNodeList;
  iDescriptList:IXMLNodeList;
  iDescriptNode:IXMLNode;
  jOut:integer;
  aOutList:TStrings;
  jOutputId:integer;
  ErrorMessage:string;
  aErrList:TStringList;
  jEr:integer;
begin

  iOutputFields:=SelectNodes( node, WideString(
         './output_field'));

  aOutList:=TStringList.Create;
  try
    ErrorMessage:='';
    for jOut:=0 to iOutputFields.Count-1 do
    begin
        jOutputId:=StrToInt(iOutputFields[jOut].Attributes['output_id']);
        iDescriptList:=  SelectNodes(iOutputFields[jOut], WideString(
         './input_description'));
        if  iDescriptList.Count>0 then
        begin
           iDescriptNode:= iDescriptList[0];
           try
              ProcessOutputDescriptor(sLine,jOutputId,iDescriptNode,aOutList, iOutputFields[jOut]);
           except
             on E:ECsvTransformException do
                 begin
                   FErrorOutputExcludeList.Add(Format('%.6d',[FOutputLineCounter]));
                   ErrorMessage:=ErrorMessage
                                 + GetInputLineMessagePart(FOutputLineCounter)
                                 + OutputLineNumberWordingExcludeErrorLines(FOutputLineCounter)
                                 +' Col '+Format('%3d',[jOut+1])
                                 + ':'
                                 +E.Message+#13#10
                                 ;
                  if Assigned(FErrorDetected) then
                    FErrorDetected(Self, FOutputLineCounter, jOut,
                                   E.Kind,
                                   sLine,
                                   ErrorMessage
                                   );
                 end;
             on E:Exception do
                 begin
                   FErrorOutputExcludeList.Add(Format('%.6d',[FOutputLineCounter]));
                   ErrorMessage:=ErrorMessage
                                 + GetInputLineMessagePart(FOutputLineCounter)
                                 + OutputLineNumberWordingExcludeErrorLines(FOutputLineCounter)
                                 +' Col '+Format('%3d',[jOut+1])
                                 + ':'
                                 +E.Message
                                 +#13#10;

                  if Assigned(FErrorDetected) then
                    FErrorDetected(Self, FOutputLineCounter, jOut,
                                   csvErrUnknown,
                                   sLine,
                                   ErrorMessage
                                   );
                 end;
           end;
        end
        else // output field just named, but not specified - we consider them empty
        begin
            while aOutList.Count<jOutputId do
              aOutList.Add('');
        end;
    end;

//  only records without errors go to output
    if Length(ErrorMessage)=0 then
       WriteStringList( writeStream, aOutList);

    if  Length(ErrorMessage)>0 then
    begin
      aErrList:=TstringList.Create;
      try
        aErrList.Text:=ErrorMessage;
        for jEr:=0 to aErrList.Count-1 do begin
                  aErrList[jEr]:=aErrList[jEr]+','+sLine;
        end;
        FErrorList.Append(aErrList.Text);

        if Not Assigned(FErrorStream) then
          FErrorstream:=TFileStream.Create(FErrorLogFile, fmCreate or fmShareDenyWrite	);

        WriteString(FErrorStream, aErrList.Text);
      finally
        aErrList.Free;
      end;
    end;
  finally
    aOutList.Free;
  end;
end;

function TeacAlgorithmClass.OutputLineNumberWordingExcludeErrorLines( FOutputLineCounter:integer):string;
var
  jPreviousErrLineCount: integer;
  j: integer;
begin
// erroneous lines are no longer part of output file
// we should address error location in output file
// in terms of reference to error-free lines
// like "Before first line" or "After line NN"
//
// All lines already listed in FOutToInpLine in outputBeforeExclusion=input format
  jPreviousErrLineCount := 0; // number of lines excluded from output
  if FErrorOutputExcludeList.Count = 0 then
  begin
    // regular operation - no errors whatsoever ...
    // Hardly can be, because there no need in error message then
    Result := ' Output line '+Format('%6d',[FOutputLineCounter]);
    Exit;
  end
  else
  begin
    for j := 0 to FErrorOutputExcludeList.Count-1 do // exclude list ordered
    begin
      jPreviousErrLineCount := j+1;
      if StrToInt(FErrorOutputExcludeList[j]) >= FOutputLineCounter then
        Break;
    end;
  end;

  if jPreviousErrLineCount >= FOutputLineCounter then
    Result := ' Before first output line '
  else
    Result := ' After output line '+IntToStr(FOutputLineCounter - jPreviousErrLineCount) + ' ';

end;

procedure TeacAlgorithmClass.OutputGenerationFirstLineProcess( node:IXMLNode; sLine:string; writeStream:TStream);
var
  SList:TStringList;
  isFirstLineHeaders:boolean;
  sHeaders:string;
begin
  sList:=TStringList.Create;
  try
    SplitSingleLine(sLine, Self.Delimiter, sList);
    isFirstLineHeaders:=IsLineListHeadersOnly(sList);

    if TargetHeaderUse=headLineUseUsed then begin
        sHeaders:=OutputHeaders();
        WriteString(writeStream, sHeaders);
    end;

    if NOT isFirstLineHeaders then
    begin
        OutputGeneration( node, sLine, writeStream);
    end;

  finally
    sList.Free;
  end;
end;

function TeacAlgorithmClass.argXMLValueFromList(  aInputValues:TStrings;
                                                  argNode:IXMLNode
                                                ):string;
var
  sX1:string;
begin
      sX1:=Copy(argNode.XML,
                Pos('=',argNode.XML)+1,
                Length(argNode.XML)
               );
               // index in arg values starts from 1, and indexing in TStrings from 0
      Result:=aInputValues[StrToInt(AnsiReplaceText(sX1,'"',''))-1];
end;

function TeacAlgorithmClass.argXMLValueFromNameList(  aInputValues:TStrings;
                                                      argNode:IXMLNode
                                                   ):string;
var
  sX1:string;
  j:integer;
begin
  Result := '';
  sX1 := Copy(argNode.XML,
            Pos('=',argNode.XML)+1,
            Length(argNode.XML)
              );
  sX1 := AnsiReplaceText(sX1,'"','');
  if FIntakeHeaderlines.Count > 0 then
  begin
     for j := 0 to FIntakeHeaderLines.Count - 1 do
       if CompareText(FIntakeHeaderLines[j], sX1) = 0 then
       begin
          Result:=aInputValues[j];
          Exit;
       end;
  end;
end;

function TeacAlgorithmClass.argXMLValueInt(argNode:IXMLNode):integer;
var
  sX1:string;
begin
      sX1:=Copy(argNode.XML,
                Pos('=',argNode.XML)+1,
                Length(argNode.XML)
               );
      Result:=StrToInt(AnsiReplaceText(sX1,'"',''));
end;

function TeacAlgorithmClass.argXMLValueIntByName(argNode:IXMLNode):integer;
var
  sX1:string;
  j:integer;
begin
  Result:=0; // it is supposed to be the number of intake field, so it starts from 1
  sX1:=Copy(argNode.XML,
            Pos('=',argNode.XML)+1,
            Length(argNode.XML)
           );
  sX1 := AnsiReplaceText(sX1,'"','');
  for j := 0 to FIntakeHeaderLines.Count-1 do
  begin
    if CompareText(FIntakeHeaderLines[j], sX1) = 0 then
    begin
       Result := j+1; // it is number, not index - so increase by 1
       Exit;
    end;
  end;
end;


function TeacAlgorithmClass.OutputHeaders():string;
begin
{ TODO : Insert function generating output headers }
  Result:='';
end;

function TeacAlgorithmClass.ConcatFields (aInputValues:TStrings;concatDescriptionNode:IXMLNode):string;
var
  Divider:string;
  iInputIdList:IXMLNodeList;
  i:integer;
  iNode:IXMLNode;
begin
  Result:='';
  Divider:='';
  if NOT VarIsNull(concatDescriptionNode.Attributes['input_concatenation_divider'])
  then
      Divider := concatDescriptionNode.Attributes['input_concatenation_divider'];

  iInputIdList:= SelectNodes(concatDescriptionNode, WideString(
      './input_field/@input_name'));
  if iInputIdList.Count>0 then
  begin
    for i:=0 to iInputIdList.Count-1 do
    begin
      iNode := iInputIdList[i];
      Result:=Result+argXMLValueFromNameList( aInputValues, iNode)+Divider;
    end;
    Result:=Copy(Result, 1, Length(Result)-Length(Divider));
  end;
end;



function TeacAlgorithmClass.LookupReplace(aInputValues:Tstrings;iLookupNode:IXMLNode):string;
var
  iKeyIdNodeList:IXMLNodeList;
  iKeyNode:IXMLNode;
  jIdInput:integer;
  ValueToReplace:string;
  iReplaceNodeList:IXMLNodeList;
  jPair:integer;
  iPairNode:IXMLNode;
  iKeyValueNodeList:IXMLNodeList;
  sKeyValueCheck:string;
  iReplaceValueList:IXMLNodeList;
begin
  Result:='';
  iKeyIdNodeList:=SelectNodes(iLookupNode, WideString(
    './key_value_field/input_field/@input_name'));
  if iKeyIdNodeList.Count>0 then
  begin
    iKeyNode:= iKeyIdNodeList[0];
    jIdInput := argXMLValueIntByName(iKeyNode);

    ValueToReplace:=aInputValues[jIdInput-1];
// list of all replacement pairs
    iReplaceNodeList:= SelectNodes(iLookupNode, WideString(
      './replacement_list/replacement_pair'));

    if iReplaceNodeList.Count=0 then
      Raise
            Exception.Create('Missing replacement assignation for input field '+intToStr(jIdInput)
                              +'['+InputIDToInputName(jIdInput)+'] value "'
                              +ValueToreplace+'"'
                            );
    for jPair:=0 to iReplaceNodeList.Count-1 do
    begin
       iPairNode:= iReplaceNodeList[jPair];
         iKeyValueNodeList:=SelectNodes(iPairNode, Widestring(
         './key_value'));
         if iKeyValueNodeList.Count>0 then
         begin
            sKeyValueCheck := VarToStr ( iKeyValueNodeList[0].NodeValue);
            if Length(sKeyValueCheck)>0 then
            begin
               if Comparetext(sKeyValueCheck,ValueToReplace)=0 then
               begin
                 iReplaceValueList:=SelectNodes(iPairNode, WideString(
                 './replacement_value'));
                 if iReplaceValueList.Count>0 then
                 begin
                    Result:=VarToStr(iReplaceValueList[0].NodeValue);
                    Exit;
                 end;
               end;
            end;
         end;
    end;
    // if we come here we were unable to find replacement
    Raise
      Exception.Create('Missing replacement value for input field '+intToStr(jIdInput)
                              +'['+InputIDToInputName(jIdInput)+'] '
                              + GetInputLineMessagePart(FOutputLineCounter)
                              +' value "'
                              +ValueToreplace+'"'
                      );
  end;
end;

function TeacAlgorithmClass.OutputIdToOutputName(outID:integer):string;
var
  iNodeList:IXMLNodeList;
  iOut:IXMLNode;
begin
  Result:='';
  iNodeList:=SelectNodes(Self.XMLDocument.DocumentElement, WideString(
    '//output_field[@output_id='''+IntToStr(outID)+''']'));
  if iNodeList.Count>0 then
  begin
      iOut:=iNodeList[0];
      Result:=VarToStr(iOut.Attributes['output_name']);
  end;
end;

function TeacAlgorithmClass.inputIDToInputName(jIn:integer):string;
var
  iNodeList:IXMLNodeList;
  iOut:IXMLNode;
begin
// Requirement shift : all numbering, if possible, by header - columns location can alter!
  Result:='';
  if FIntakeHeaderLines.Count > 0 then
  begin
    if (0 < jIn) and (jIn <= FIntakeHeaderLines.Count) then
    begin
      Result:=  FIntakeHeaderLines[jIn-1];
    end;
  end
  else
  begin
  // here is old code, before input_id become undefined until physical CSV
    iNodeList:=SelectNodes(Self.XMLDocument.DocumentElement, WideString(
      '//input_field[@input_id='''+IntToStr(jIn)+''']'));
    if iNodeList.Count>0 then
    begin
        iOut:=iNodeList[0];
        Result:=VarToStr(iOut.Attributes['input_name']);
    end;
  end;
end;

function TeacAlgorithmClass.SelectorConstReplace(
                                      aInputValues:TStrings;
                                      iSelectorNode:IXMLNode;
                                      jOutputId:integer;
                                      OutFieldNode:IXMLNode
                                                ):string;
const
  MISSEDHORMAPPINGMESSAGE = ' Missed horizontal mapping for out column ';
var
  iFieldNodeList:IXMLNodeList;
  iFieldNode:IXMLNode;
  jField:integer;
  jInputId:integer;
  sInputVal:string;
  iReplNodeList:IXMLNodeList;
  iReplNode:IXMLNode;

  sFieldName:string;
  j:integer;
  Mandatory:string;
begin
  Result:='';
  iFieldNodeList:=SelectNodes(iSelectorNode, WideString(
      './input_field'));
  if iFieldNodeList.Count>0 then begin
     for jField:=0 to iFieldNodeList.Count-1 do
     begin
        iFieldNode:= iFieldNodeList[jField];
//        jInputId:=StrToInt(iFieldNode.Attributes['input_id']);
        sFieldName := VarToStr(iFieldNode.Attributes['input_name']);
        if Length(sFieldName) > 0 then
        begin
          for j := 0 to FIntakeHeaderLines.Count-1 do
          begin
            if CompareText(FIntakeHeaderLines[j],sFieldName) = 0 then
            begin
              jInputId := j+1 ;// because jInputID is Field number and starts from 1

              sInputVal:=aInputValues[jInputId-1];
              if Length(sInputVal)>0 then
              begin
                iReplNodeList:=SelectNodes(iFieldNode, WideString(
                './output_value_if_content_exists'));
                if iReplNodeList.Count>0 then
                begin
                  iReplNode:=iReplNodeList[0];
                  if NOT VarIsNull(iReplNode.NodeValue) then
                  begin
                    Result:=iReplNode.NodeValue;
                    Exit;
                  end
                  else
                    Raise ECsvTransformException.Create(csvErrColMap ,MISSEDHORMAPPINGMESSAGE + IntToStr(jOutputID) + '['+OutputIdToOutputName(jOutputID)+']'+' from intake column '+IntToStr(jInputId)+' ['+VarToStr(iFieldNode.Attributes['input_name'])+'] '+' (input value '+' '''+sInputVal+''') ');
                end;
              end;
            end;
          end;
        end;
     end;

     Mandatory := VarToStr(OutFieldNode.Attributes['output_mandatory']);
     if CompareText(Mandatory, 'yes')=0 then
     begin
// if we come here, no mapping happened at all
       Raise ECsvTransformException.Create(
                  csvErrColMap ,
                  MISSEDHORMAPPINGMESSAGE
                 + IntToStr(jOutputID) + '['+OutputIdToOutputName(jOutputID)+']'
                                          );
     end;
  end;
end;

function TeacAlgorithmClass.SelectorContentReplace(aInputValues:TStrings;iSelectorNode:IXMLNode):string;
var
  iFieldNodeList:IXMLNodeList;
  iFieldNode:IXMLNode;
  jField:integer;
  sInputVal:string;
  sInputName:string;
  j:integer;
begin
  Result:='';
  iFieldNodeList:=SelectNodes(iSelectorNode, WideString(
      './input_field'));
  if iFieldNodeList.Count>0 then begin
     for jField:=0 to iFieldNodeList.Count-1 do
     begin
        iFieldNode := iFieldNodeList[jField];
        sInputName := VarToStr(iFieldNode.Attributes['input_name']);
        if Length(sInputName) > 0 then
        begin
          for j := 0 to FIntakeHeaderLines.Count-1 do
          begin
            if CompareText(FIntakeHeaderLines[j], sInputName) = 0 then
            begin
              sInputVal:=aInputValues[j];
              if Length(sInputVal)>0 then
              begin
                Result:=sInputVal;
                Exit;
              end;
            end;
          end;
        end;
     end;
  end;
end;

function TeacAlgorithmClass.StaticValueReplace(iStaticContentNode:IXMLNode):string;
begin
  Result:='';
  if NOT VarIsNull(iStaticContentNode.NodeValue) then
    Result:= iStaticContentNode.NodeValue;
end;

procedure TeacAlgorithmClass.ProcessOutputDescriptor(
                      sLine:string;
                      jOutputId:integer;
                      iDescriptNode:IXMLNode;
                      var aOutList:TStrings;
                      OutFieldNode:IXMLNode
                                                    );
var
  iSimpleNodelist:IXMLNodeList;
  iConcatenationNodeList:IXMLNodeList;
  iSelectorContentConst:IXMLNodelist;
  iSelectorContent:IXMLNodelist;
  iStaticContent:IXMLNodeList;
  iLookupTableList:IXMLNodeList;
  aInputValues:TStringList;

  iSimpleNode:IXMLNode;
begin
  while aOutList.Count < jOutputId do
    aOutlist.Add('');


  aInputValues:=TStringList.Create();
  try
    SplitSingleLine(sLine, Self.Delimiter, aInputValues);
    iSimpleNodelist:=SelectNodes(iDescriptNode, WideString(
            './input_field/@input_name'));
    if iSimpleNodeList.Count>0 then
    begin
      iSimpleNode:= iSimpleNodeList[0];
      aOutList[jOutputId-1]:=argXMLValueFromNameList( aInputValues, iSimpleNode );
    end
    else
    begin
      iConcatenationNodeList:=SelectNodes(iDescriptNode, WideString(
        './input_concatenation'));
      if iConcatenationNodeList.Count>0 then
      begin
         aOutList[jOutputId-1]:=ConcatFields(aInputValues,iConcatenationNodeList[0]);
      end
      else
      begin
        iLookupTableList:=SelectNodes(iDescriptNode, WideString(
          './lookup_table_replacement'));
        if iLookupTableList.Count>0 then
        begin
            aOutList[jOutputId-1]:=LookupReplace(aInputValues,iLookupTableList[0]);
        end
        else
        begin
          iSelectorContentConst:=SelectNodes(iDescriptNode, WideString(
          './selector_of_fields_with_content_fixedvalue'));
          if iSelectorContentConst.Count>0 then
          begin
            aOutList[jOutputId-1]:=SelectorConstReplace(
                                          aInputValues,
                                          iSelectorContentConst[0],
                                          jOutputId,
                                          OutFieldNode
                                          );
          end
          else
          begin
            iSelectorContent:=SelectNodes(iDescriptNode, WideString(
              './selector_of_fields_with_content'));
            if iSelectorContent.Count>0 then
            begin
              aOutList[jOutputId-1]:=SelectorContentReplace(aInputValues,iSelectorContent[0]);
            end
            else begin
              iStaticContent:=SelectNodes(iDescriptNode, WideString(
              './static_content'));
              if iStaticContent.Count>0 then
              begin
                 aOutList[jOutputId-1]:=StaticValueReplace(iStaticContent[0]);
              end;
            end;
          end;
        end;
      end;
    end;
  finally
    ainputValues.Free;
  end;

end;

constructor TeacAlgorithmClass.Create(Aowner:TComponent);
begin
  inherited;
  self.Delimiter := ',';
  FErrorList := TStringList.Create;
  FOutToInpLine := TStringList.Create;
  FIntakeHeaderLines := TStringList.Create;
  FErrorOutputExcludeList := TStringList.Create;
  FErrorOutputExcludeList.Sorted := true;
  FErrorOutputExcludeList.Duplicates:=dupIgnore;
end;

destructor TeacAlgorithmClass.Destroy;
begin
  FErrorOutputExcludeList.Free;
  FIntakeHeaderLines.Free;
  FOutToInpLine.Free;
  FErrorList.Free;
  inherited;
end;

function TeacAlgorithmClass.getProperty(nodeName: string): string;
var
  propNodeList:IXMLNodeList;
begin
  Result:='';
  try
    propNodeList := SelectNodes(Self.XMLDocument.DocumentElement, WideString(
                    './'+nodeName));
    if propNodelist.Count>0 then
      if NOT VarIsNull(propNodelist[0].NodeValue)then
        Result:=propNodelist[0].NodeValue;
  except
  end;
end;

function TeacAlgorithmClass.setProperty(nodeName, nodeValue: string):boolean;
var
  propNodeList:IXMLNodeList;
  propNode:IXMLNode;
begin
  Result:=false;
  propNodeList := SelectNodes(Self.XMLDocument.DocumentElement, WideString(
                  './'+nodeName));
  if propNodelist.Count>0 then begin
    propNode:=propNodeList[0];
  end
  else
  begin
     propNode:=Self.XMLDocument.DocumentElement.AddChild(nodeName);
  end;
  try
    propNode.NodeValue:=nodeValue;
  except
    Result:=true;
  end;

end;

procedure TeacAlgorithmClass.UpdateInputCounter;
begin
  if Not FInputCounterFlag then
  begin
    FInputCounter:=FInputCounter+1;
    if Assigned( FIntakeRecordProcessed) then
      FIntakerecordProcessed(Self,  FInputCounter);
  end;
end;

procedure TeacAlgorithmClass.SetIntakeHeaderLineFromFile(OriginalFileName:string);
var
  bFirstRead:boolean;
  tsFile:TTextStream;
  sLine:string;
begin
    FIntakeHeaderline:='';
    FIntakeHeaderLines.Clear;
    tsFile := TTextStream.Create(TFileStream.Create(OriginalFileName, fmOpenRead or fmShareDenyNone));
    try
        bFirstRead:=tsFile.ReadLn(sLine);
        if  bFirstRead then
        begin
           FIntakeHeaderline:=sLine;
           SplitSingleLine(sLine,',', FIntakeHeaderLines);
        end;
    finally
      tsFile.Free;
    end;
end;

function TeacAlgorithmClass.InputFieldNodeCalculatedID(CheckFieldNode:IXMLNode):integer;
var
  sFieldName:string;
  j:integer;
begin
  Result:=-1;
  sFieldName:=VarToStr(CheckFieldNode.Attributes['input_name']);
  if Length(sFieldName) > 0 then
    if FIntakeHeaderLines.Count > 0 then
       for j:=0 to FIntakeHeaderLines.Count-1 do begin
          if CompareText( FIntakeHeaderLines[j],sFieldName) = 0 then
          begin
             Result := j;
             Exit;
          end;
       end
    else
     Result:=StrToInt(CheckFieldNode.Attributes['input_id'])-1;
end;

end.
