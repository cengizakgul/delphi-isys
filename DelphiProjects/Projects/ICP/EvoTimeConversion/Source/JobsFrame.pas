unit JobsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient,
  CustomBinderBaseFrame, alohaparser;

type
  TJobsFrm = class(TCustomBinderBaseFrm)
    cdAlohaJobCodes: TClientDataSet;
    cdAlohaJobCodesCODE: TStringField;
  private
    procedure InitAlohaJobCodes(alohaBatches: array of TAlohaBatch);
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; CO_JOBS: TDataSet; alohaBatches: array of TAlohaBatch);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, gdydbcommon;

{ TJobsFrm }

const
  CoJobsBinding: TBindingDesc =
    ( Name: 'JobsBinding';
      Caption: 'Mapped Job Codes';
      LeftDesc: ( Caption: 'Aloha Job Codes'; Unique: true; KeyFields: 'CODE'; ListFields: 'CODE');
      RightDesc: ( Caption: 'Evolution Company Jobs'; Unique: false; KeyFields: 'DESCRIPTION'; ListFields: 'DESCRIPTION;TRUE_DESCRIPTION');
      HideKeyFields: true;
    );

constructor TJobsFrm.Create(Owner: TComponent);
begin
  inherited;
end;

procedure TJobsFrm.Init(matchTableContent: string; CO_JOBS: TDataSet; alohaBatches: array of TAlohaBatch);
begin
  FBindingKeeper := CreateBindingKeeper( CoJobsBinding, TClientDataSet );
  InitAlohaJobCodes(alohaBatches);
  CO_JOBS.FieldByName('DESCRIPTION').DisplayLabel := 'Job Code';
  CO_JOBS.FieldByName('TRUE_DESCRIPTION').DisplayLabel := 'Description';
  FBindingKeeper.SetTables(cdAlohaJobCodes, CO_JOBS);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TJobsFrm.InitAlohaJobCodes(alohaBatches: array of TAlohaBatch);
var
  n: integer;
  i: integer;
  code: string;
begin
  CreateOrEmptyDataSet( cdAlohaJobCodes );
  for n := 0 to high(alohaBatches) do
    for i := 0 to High(alohaBatches[n].Data) do
    begin
      code := alohaBatches[n].Data[i].JobCode;
      if trim(code) <> '' then
        if not cdAlohaJobCodes.Locate('CODE', code, []) then
          Append(cdAlohaJobCodes, 'CODE', [code]);
    end;
  ResortRequested(cdAlohaJobCodes, 'CODE', false);
end;

end.



