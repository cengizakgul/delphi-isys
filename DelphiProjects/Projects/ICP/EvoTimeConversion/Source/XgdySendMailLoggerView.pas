{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

unit XgdySendMailLoggerView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyCommonLoggerView, gdyLoggerRichView, ExtCtrls, StdCtrls,
  Buttons, gdyLoggerImpl, NewSevenZip, ActnList;

type
  TXSendMailLoggerViewFrame = class(TCommonLoggerViewFrame)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ActionList1: TActionList;
    actSaveUserLog: TAction;
    BitBtn3: TBitBtn;
    sdUserLog: TSaveDialog;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure actSaveUserLogUpdate(Sender: TObject);
    procedure actSaveUserLogExecute(Sender: TObject);
  private
    FDebugLog: TMemoryStream;
    FUserLog: TMemoryStream;
    FDebugSink: ILoggerEventSink;
    FUserSink: ILoggerEventSink;

  protected
    FExtraFiles:TStrings;
    procedure AddLogsToArch(Arch: I7zOutArchive); virtual;
    function SaveArchiveToInternal(folder: string; dt: TDateTime): string; virtual;
    procedure SetExtraFiles(Value:TStrings);virtual; 
  public
    Password: string;
    EMail: string;
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    function SaveArchiveTo(folder: string): string;  virtual;
    procedure AddUserLogToArch(Arch: I7zOutArchive); virtual;
    property ExtraFiles:TStrings read FExtraFiles write SetExtraFiles;
  end;

  TXSendMailLoggerViewFrameClass = class of TXSendMailLoggerViewFrame;

implementation

{$R *.dfm}
uses
  gdymail, gdyRedir, common, gdyLogWriters, gdyGlobalWaitIndicator, gdycommon,gdyUtils;

procedure TXSendMailLoggerViewFrame.AddLogsToArch(Arch: I7zOutArchive);
begin
  if LoggerKeeper.UserLogFileName <> '' then
  begin
    LoggerKeeper.CloseUserLogFileTemporarily;
    Arch.AddFile(LoggerKeeper.UserLogFileName, ExtractFileName(LoggerKeeper.UserLogFileName) );
  end;
  Arch.AddStream(FUserLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, 'session-user.log', false, false);

  if LoggerKeeper.DevLogFileName <> '' then
  begin
    LoggerKeeper.CloseDevLogFileTemporarily;
    Arch.AddFile(LoggerKeeper.DevLogFileName, ExtractFileName(LoggerKeeper.DevLogFileName));
  end;
  Arch.AddStream(FDebugLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, 'session-debug.log', false, false);
//  Arch.SetProgressCallback();
end;

function TXSendMailLoggerViewFrame.SaveArchiveTo(folder: string): string;
begin
  Result := SaveArchiveToInternal(folder, Now);
end;

function TXSendMailLoggerViewFrame.SaveArchiveToInternal(folder: string; dt: TDateTime): string;
var
  Arch: I7zOutArchive;
  base: string;
  i:integer;
begin
  WaitIndicator.StartWait('Packing log files');
  try
    Assert(Password <> '');

    Arch := CreateOutArchive(CLSID_CFormat7z, Redirection.GetFilename(sSevenZipDllAlias));
    SetCompressionLevel(Arch, 5);
    Arch.SetPassword(Password);
    AddLogsToArch(Arch);
    base := ChangeFileExt(GetAppFilename,'') + '-debug-log-';
    Result := WithTrailingSlash(folder) + base + FormatDateTime('yyyy-mm-dd-hhnn', dt) + '.7z';
    ForceDirectories(folder);
    if Assigned (FExtraFiles) then
    begin
      for i:=0 to FExtraFiles.Count-1 do begin
        if FileExists( FExtraFiles[i]) then
           Arch.AddFile(FExtraFiles[i], ExtractFileName(FExtraFiles[i]));
      end;
    end;
    Arch.SaveToFile(Result);
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TXSendMailLoggerViewFrame.BitBtn1Click(Sender: TObject);
var
  fn: string;
  dt: TDateTime;
begin
  LoggerKeeper.Logger.LogEntry( Format('User clicked "%s"', [(Sender as TBitBtn).Caption] ));
  try
    try
      dt := Now;
      fn := SaveArchiveToInternal(Redirection.GetDirectory(sLogArchiveDirAlias), dt);
      try
        SimpleMailTo(EMail, Format('Debug log - %s, %s', [Application.Title, FormatDateTime('yyyy.mm.dd hh:nn',dt)]), '', fn);
      finally
        DeleteFile(fn);
      end;
    except
      LoggerKeeper.Logger.PassthroughException;
    end;
  finally
    LoggerKeeper.Logger.LogExit;
  end;
end;

procedure TXSendMailLoggerViewFrame.BitBtn2Click(Sender: TObject);
begin
  LoggerKeeper.Logger.LogEntry( Format('User clicked "%s"', [(Sender as TBitBtn).Caption] ));
  try
    try
      OpenInExplorer( SaveArchiveTo( Redirection.GetDirectory(sLogArchiveDirAlias)) );
    except
      LoggerKeeper.Logger.PassthroughException;
    end;
  finally
    LoggerKeeper.Logger.LogExit;
  end;
end;

constructor TXSendMailLoggerViewFrame.Create(Owner: TComponent);
begin
  inherited;
  FDebugLog := TMemoryStream.Create;
  FDebugSink := TPlainTextDevLogWriter.Create(TStreamLogOutput.Create(FDebugLog));
  LoggerKeeper.StatefulLogger.Advise(FDebugSink);

  FUserLog := TMemoryStream.Create;
  FUserSink := TPlainTextUserLogWriter.Create(TStreamLogOutput.Create(FUserLog));
  LoggerKeeper.StatefulLogger.Advise(FUserSink);
  FExtraFiles:=TStringList.Create;
end;

destructor TXSendMailLoggerViewFrame.Destroy;
begin
  LoggerKeeper.StatefulLogger.UnAdvise(FDebugSink);
  LoggerKeeper.StatefulLogger.UnAdvise(FUserSink);
  FDebugSink := nil;
  FUserSink := nil;
  FreeAndNil( FDebugLog );
  FreeAndNil( FUserLog );
  inherited;
end;


procedure TXSendMailLoggerViewFrame.actSaveUserLogUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := LoggerRichView.MessagesCount > 0;
end;

procedure TXSendMailLoggerViewFrame.actSaveUserLogExecute(Sender: TObject);
begin
  sdUserLog.FileName := ChangeFileExt(GetAppFilename,'') + '-user-log-' + FormatDateTime('yyyy-mm-dd-hhnn', Now) + '.txt';
  if sdUserLog.Execute then
  begin
    StringToFile(StreamToString(FUserLog), sdUserLog.FileName);
    OpenDoc(Application.MainForm, sdUserLog.FileName);
  end;
end;

procedure TXSendMailLoggerViewFrame.AddUserLogToArch(Arch: I7zOutArchive);
begin
  Arch.AddStream(FUserLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, 'task-user.log', false, false);
end;

procedure TXSendMailLoggerViewFrame.SetExtraFiles(Value: TStrings);
begin
end;

end.
