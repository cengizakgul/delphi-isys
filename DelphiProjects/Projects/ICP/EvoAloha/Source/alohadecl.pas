unit alohadecl;

interface

uses
  RateImportOptionFrame, gdyCommonLogger, isSettings;

type
  TAlohaOptions = record
    Summarize: boolean;
    ImportJobCodes: boolean;
    RateImport: TRateImportOption;
    IgnoreNotMappedFields: boolean;
    TakeDBDTFromJobCodes: boolean;
  end;

procedure LogAlohaOptions(logger: ICommonLogger; options: TAlohaOptions);
function LoadAlohaOptions(conf: IisSettings; root: string; defTakeDBDTFromJobCodes: boolean): TAlohaOptions;
procedure SaveAlohaOptions( const options: TAlohaOptions; conf: IisSettings; root: string);

implementation

uses
  sysutils;

procedure LogAlohaOptions(logger: ICommonLogger; options: TAlohaOptions);
begin
  logger.LogContextItem('Summarize', BoolToStr(options.Summarize,true));
  logger.LogContextItem('Take DBDT from job codes', BoolToStr(options.TakeDBDTFromJobCodes,true));
  if not options.TakeDBDTFromJobCodes then
    logger.LogContextItem('Import job codes', BoolToStr(options.ImportJobCodes,true));
  LogRateImportOption(logger, options.RateImport, 'Aloha');
  logger.LogContextItem('Ignore not mapped fields', BoolToStr(options.IgnoreNotMappedFields,true));
end;

function LoadAlohaOptions(conf: IisSettings; root: string; defTakeDBDTFromJobCodes: boolean): TAlohaOptions;
begin
  root := root + '\';
  Result.Summarize := conf.AsBoolean[root+'Summarize'];
  if conf.GetValueNames(root).IndexOf('TakeDBDTFromJobCodes') <> -1 then
    Result.TakeDBDTFromJobCodes := conf.AsBoolean[root+'TakeDBDTFromJobCodes']
  else
    Result.TakeDBDTFromJobCodes := defTakeDBDTFromJobCodes;
  Result.ImportJobCodes := conf.AsBoolean[root+'ImportJobCodes'] and not Result.TakeDBDTFromJobCodes;

  Result.RateImport := TRateImportOption(conf.AsInteger[root+'RateImport']);
  if (Result.RateImport < low(TRateImportOption)) or (Result.RateImport > high(TRateImportOption)) then
    Result.RateImport := rateUseExternal;

  Result.IgnoreNotMappedFields := conf.AsBoolean[root+'IgnoreNotMappedFields'];
end;

procedure SaveAlohaOptions( const options: TAlohaOptions; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsBoolean[root+'TakeDBDTFromJobCodes'] := options.TakeDBDTFromJobCodes;
  conf.AsBoolean[root+'ImportJobCodes'] := options.ImportJobCodes;
  conf.AsInteger[root+'RateImport'] := ord(options.RateImport);
  conf.AsBoolean[root+'IgnoreNotMappedFields'] := options.IgnoreNotMappedFields;
end;

end.
