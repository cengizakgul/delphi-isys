unit MainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame,
  EvolutionPrPrBatchFrame, FileOpenFrame, CustomBinderBaseFrame,
  EDCodeFrame, alohaoptionsframe, alohaparser, aloha, alohadecl, timeclockimport,
  JobsFrame, DBDTFrame, FilesOpenFrame, OptionsBaseFrame, JobToDBDTFrame,
  AlohaDBDTSourceFrame;

type
  TMainFm = class(TEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    pnlBottom: TPanel;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshEdMap: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    pnlPayrollBatch: TPanel;
    PayrollBatchFrame: TEvolutionPrPrBatchFrm;
    BitBtn1: TBitBtn;
    EDCodeFrame: TEDCodeFrm;
    Bevel1: TBevel;
    pnlSetups: TPanel;
    AlohaOptionsFrame: TAlohaOptionsFrm;
    Bevel3: TBevel;
    tbshBatchIDtoDBDTMapping: TTabSheet;
    tbshJobMapping: TTabSheet;
    DBDTFrame: TDBDTFrm;
    JobsFrame: TJobsFrm;
    pnlTop: TPanel;
    FilesOpenFrame: TFilesOpenFrm;
    pnlSpacer: TPanel;
    tbshJobToDBDTMapiing: TTabSheet;
    JobToDBDTFrame: TJobToDBDTFrm;
    AlohaDBDTSourceFrame: TAlohaDBDTSourceFrm;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
  private
    FEvoData: TEvoData;
    FAlohaBatches: array of TAlohaBatch;

    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    function GetTimeClockData(period: TPayPeriod;
      company: TEvoCompanyDef): TTimeClockImportData;
    procedure SavePerCompanyMappings(const oldValue: TEvoCompanyDef);
    procedure LoadPerCompanyMappings(const Value: TEvoCompanyDef);
    procedure HandleFilesSelected(files: TStrings);
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);
    procedure SavePerCompanySettings(const oldValue: TEvoCompanyDef);
    function AlohaOptions: TAlohaOptions;
    procedure HandleAlohaDBDTSourceChangedByUser(Sender: TObject);
    procedure AlohaDBDTSourceChanged;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}
uses
  isSettings, EeUtilityLoggerViewFrame, dialogs,
  userActionHelpers, gdyBinder, variants, RateImportOptionFrame;


procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  if FEvoData.CanGetClCo then
    SavePerCompanySettings(FEvoData.GetClCo);

  FEvoData.Connect(EvoFrame.Param);
  ConnectToEvo.Caption := 'Refresh Evolution data';
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEeUtilityLoggerViewFrm;
  inherited;
  FEvoData := TEvoData.Create(Logger);
  FEvoData.AddDS(TMP_PRDesc);
  FEvoData.AddDS(PR_BATCHDesc);
  FEvoData.AddDS(CO_E_D_CODESDesc);
  FEvoData.AddDS(CO_JOBSDesc);
  FEvoData.AddDS(CUSTOM_DBDTDesc);

  CompanyFrame.Init(FEvoData);
  PayrollBatchFrame.Init(FEvoData);
  FEvoData.Advise(HandleCompanyChanged);
  AlohaDBDTSourceFrame.OnChangeByUser := HandleAlohaDBDTSourceChangedByUser;

  //designer crashed repeatedly when I tried to do this by editing the form
  FilesOpenFrame.FileOpen1.Dialog.Filter := 'Aloha export files (extpay*.*)|extpay*.*';
  FilesOpenFrame.FileOpen1.Hint := 'Open|Opens an Aloha export file';
  FilesOpenFrame.OnFilesSelected := HandleFilesSelected;

  if EvoFrame.IsValid then
    PageControl1.TabIndex := TabSheet2.PageIndex
  else
    PageControl1.TabIndex := 0;

  AlohaDBDTSourceChanged;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

function TMainFm.AlohaOptions: TAlohaOptions;
begin
  Result := AlohaOptionsFrame.Options;
  Result.TakeDBDTFromJobCodes := AlohaDBDTSourceFrame.TakeDBDTFromJobCodes;
end;

procedure TMainFm.SavePerCompanySettings(const oldValue: TEvoCompanyDef);
begin
  try
    SaveAlohaOptions( AlohaOptions, FSettings, CompanyUniquePath(oldValue) + '\AlohaOptions');
  except
    Logger.StopException;
  end;
  try
    FSettings.AsString[ClientUniquePath(oldValue) + '\EDCodeMapping\DataPacket'] := EDCodeFrame.SaveToString;
  except
    Logger.StopException;
  end;
  SavePerCompanyMappings(oldValue);
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
var
  defTakeDBDTFromJobCodes: boolean;
  ao: TAlohaOptions;
begin
  try
    if FSettings.GetChildNodes(CompanyUniquePath(Value)).IndexOf('JobToDBDTMapping') <> -1 then
      defTakeDBDTFromJobCodes := true
    else if FSettings.GetChildNodes(CompanyUniquePath(Value)).IndexOf('DBDTMapping') <> -1 then
      defTakeDBDTFromJobCodes := false
    else
      defTakeDBDTFromJobCodes := AlohaDBDTSourceFrame.TakeDBDTFromJobCodes;

    ao := LoadAlohaOptions( FSettings, CompanyUniquePath(Value) + '\AlohaOptions', defTakeDBDTFromJobCodes);
    AlohaOptionsFrame.Options := ao;
    AlohaDBDTSourceFrame.TakeDBDTFromJobCodes := ao.TakeDBDTFromJobCodes;
  except
    Logger.StopException;
  end;
  EDCodeFrame.Init( FSettings.AsString[ClientUniquePath(Value)+'\EDCodeMapping\DataPacket'], FEvoData.DS[CO_E_D_CODESDesc.Name] );
  LoadPerCompanyMappings(Value);
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  Logger.LogEntry('Current company changed');
  try
    try
      LogNullableEvoCompanyDef(Logger, new);

      if old.HasValue then
        SavePerCompanySettings(old.Value);

      if new.HasValue then
      begin
        LoadPerCompanySettings(new.Value);
        tbshBatchIDtoDBDTMapping.TabVisible := not AlohaOptions.TakeDBDTFromJobCodes;
        tbshJobMapping.TabVisible := not AlohaOptions.TakeDBDTFromJobCodes;
        tbshJobToDBDTMapiing.TabVisible := AlohaOptions.TakeDBDTFromJobCodes;
      end
      else
      begin
        EDCodeFrame.UnInit;
        JobsFrame.UnInit;
        DBDTFrame.UnInit;
        JobToDBDTFrame.UnInit;
      end
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.SavePerCompanyMappings(const oldValue: TEvoCompanyDef);
begin
  try
    FSettings.AsString[CompanyUniquePath(oldValue) + '\JobsMapping\DataPacket'] := JobsFrame.SaveToString;
  except
    Logger.StopException;
  end;
  try
    FSettings.AsString[CompanyUniquePath(oldValue) + '\DBDTMapping\DataPacket'] := DBDTFrame.SaveToString;
  except
    Logger.StopException;
  end;
  try
    FSettings.AsString[CompanyUniquePath(oldValue) + '\JobToDBDTMapping\DataPacket'] := JobToDBDTFrame.SaveToString;
  except
    Logger.StopException;
  end;
end;

//needed for HandleFilesSelected
procedure TMainFm.LoadPerCompanyMappings(const Value: TEvoCompanyDef);
begin
  JobsFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\JobsMapping\DataPacket'], FEvoData.DS[CO_JOBSDesc.Name], FAlohaBatches );
  DBDTFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\DBDTMapping\DataPacket'], FEvoData.DS[CUSTOM_DBDTDesc.Name], FAlohaBatches );
  JobToDBDTFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\JobToDBDTMapping\DataPacket'], FEvoData.DS[CUSTOM_DBDTDesc.Name], FAlohaBatches );
end;

procedure TMainFm.HandleFilesSelected(files: TStrings);
var
  i: integer;
  company: Variant;
begin
  Logger.LogEntry('User selected Aloha export files');
  try
    try
      for i := 0 to files.Count-1 do
        Logger.LogContextItem( Format('File #%d', [i]), files[i] );

      try
        SetLength(FAlohaBatches, files.Count);
        for i := 0 to files.Count-1 do
          FAlohaBatches[i] := LoadAlohaBatch(Logger, files[i]);

        company := Null;
        for i := 0 to files.Count-1 do
        begin
          if not VarIsNull(company) and (company <> FAlohaBatches[i].CompanyCode) then
            raise Exception.CreateFmt('Files %s and %s contain data for different companies (%s and %s)', [files[i-1], files[i], FAlohaBatches[i-1].CompanyCode, FAlohaBatches[i].CompanyCode]);

          company := FAlohaBatches[i].CompanyCode;
        end
      except
        SetLength(FAlohaBatches, 0);
        raise;
      end;

      //add new jobs and batch IDs if any
      if FEvoData.CanGetClCo then
      begin
        SavePerCompanyMappings(FEvoData.GetClCo);
        LoadPerCompanyMappings(FEvoData.GetClCo);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetPrBatch and (Length(FAlohaBatches) > 0) and EDCodeFrame.CanCreateMatcher and
              (AlohaOptions.TakeDBDTFromJobCodes and JobToDBDTFrame.IsInitialized or
               not AlohaOptions.TakeDBDTFromJobCodes and JobsFrame.CanCreateMatcher and DBDTFrame.IsInitialized);
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
var
  eedata: TEvoEEData;
  DBDTMatcher: IMatcher;
  JobToDBDTMatcher: IMatcher;
  n: integer;
  data: TTimeClockImportData;
begin
  eedata := FEvoData.CreateEEData;
  try
    DBDTMatcher := nil;
    JobToDBDTMatcher := nil;
    if AlohaOptions.TakeDBDTFromJobCodes then
    begin
      if JobToDBDTFrame.CanCreateMatcher then
        JobToDBDTMatcher := JobToDBDTFrame.Matcher
      else
      begin
        if AlohaOptionsFrame.Options.RateImport = rateUseEvoEE then //duplicates the logic of aloha.GetTimeClockData - Result.UseEmployeePayRates := options.RateImport = rateUseEvoEE
          raise Exception.Create('If the "Use Evolution employee pay rates" option is selected then Aloha jobs must be mapped to Evolution D/B/D/Ts.')
        else
          Logger.LogWarning('Aloha jobs are not mapped to Evolution D/B/D/Ts');
      end;
    end
    else
    begin
      if DBDTFrame.CanCreateMatcher then
        DBDTMatcher := DBDTFrame.Matcher
      else
      begin
        if AlohaOptionsFrame.Options.RateImport = rateUseEvoEE then //duplicates the logic of aloha.GetTimeClockData - Result.UseEmployeePayRates := options.RateImport = rateUseEvoEE
          raise Exception.Create('If the "Use Evolution employee pay rates" option is selected then Aloha batch IDs must be mapped to Evolution D/B/D/Ts.')
        else
          Logger.LogWarning('Aloha batch IDs are not mapped to Evolution D/B/D/Ts');
      end;
    end;
    Result.Text := '';
    for n := 0 to High(FAlohaBatches) do
    begin
      data := aloha.GetTimeClockData(Logger, FAlohaBatches[n], AlohaOptions, EDCodeFrame.Matcher, JobsFrame.Matcher, DBDTMatcher, JobToDBDTMatcher, eedata );
      Assert( (n=0) or (data.APIOptions.UseEmployeePayRates = Result.APIOptions.UseEmployeePayRates)); //depends only on Options; should compare all APIOptions here
      Result.APIOptions := data.APIOptions;
      Result.Text := Result.Text + data.Text;
    end;
  finally
    FreeAndNil(eedata);
  end;
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
var
  company: TEvoCompanyDef;
  msg: string;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      LogAlohaOptions(Logger, AlohaOptionsFrame.Options);
      company := FEvoData.GetPrBatch.Company;
      SavePerCompanySettings(company);
      
      //companies are the same in all files
      msg := Format('You are about to import timeclock information from "%s" company into "%s" (%s) company. Do you want to proceed?',
                    [trim(FAlohaBatches[0].CompanyCode), trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER)] );

      if AnsiSameText( trim(FAlohaBatches[0].CompanyCode), trim(company.CUSTOM_COMPANY_NUMBER) ) or
         AnsiSameText( trim(FAlohaBatches[0].CompanyCode), trim(company.Name) ) or
         (MessageDlg( msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
      begin
        RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData, FEvoData.Connection, false);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleAlohaDBDTSourceChangedByUser(Sender: TObject);
begin
  AlohaDBDTSourceChanged;
end;

procedure TMainFm.AlohaDBDTSourceChanged;
begin
  tbshBatchIDtoDBDTMapping.TabVisible := not AlohaDBDTSourceFrame.TakeDBDTFromJobCodes;
  tbshJobMapping.TabVisible := not AlohaDBDTSourceFrame.TakeDBDTFromJobCodes;
  tbshJobToDBDTMapiing.TabVisible := AlohaDBDTSourceFrame.TakeDBDTFromJobCodes;
  AlohaOptionsFrame.Options := AlohaOptions;
end;

end.
