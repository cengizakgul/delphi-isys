unit EDCodeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame;

type
  TEDCodeFrm = class(TCustomBinderBaseFrm)
    cdAlohaFieldNames: TClientDataSet;
    cdAlohaFieldNamesCODE: TIntegerField;
    cdAlohaFieldNamesDESCRIPTION: TStringField;
  private
    procedure InitAlohaFieldNames;
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; CO_E_D_CODES: TDataSet);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, alohaparser;

{ TEeStatusFrm }

const
  EEStatusBinding: TBindingDesc =
    ( Name: 'EDCodeBinding';
      Caption: 'Aloha field names mapped to Evolution E/D Codes';
      LeftDesc: ( Caption: 'Aloha field names'; Unique: true; KeyFields: 'CODE'; ListFields: 'DESCRIPTION');
      RightDesc: ( Caption: 'Evolution company E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION');
      HideKeyFields: true;
    );

constructor TEDCodeFrm.Create(Owner: TComponent);
begin
  inherited;
  InitAlohaFieldNames;
end;

procedure TEDCodeFrm.Init(matchTableContent: string; CO_E_D_CODES: TDataSet);
begin
  FBindingKeeper := CreateBindingKeeper( EEStatusBinding, TClientDataSet );
  CO_E_D_CODES.FieldByName('CUSTOM_E_D_CODE_NUMBER').DisplayLabel := 'E/D Code';
  CO_E_D_CODES.FieldByName('DESCRIPTION').DisplayLabel := 'Description';
  FBindingKeeper.SetTables(cdAlohaFieldNames, CO_E_D_CODES);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TEDCodeFrm.InitAlohaFieldNames;
  procedure AddRec(fieldType: TAlohaFieldType);
  begin
    Append(cdAlohaFieldNames, 'CODE;DESCRIPTION', [fieldType, GetAlohaFieldName(fieldType)]);
  end;
begin
  CreateOrEmptyDataSet( cdAlohaFieldNames );
  AddRec(alohaRegularHours);
  AddRec(alohaOvertimeHours);
  AddRec(alohaDeclaredTips);
  AddRec(alohaCashTips);
  AddRec(alohaSales);
  AddRec(alohaCreditCardTips);
end;

end.



