unit AlohaDBDTSourceFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OptionsBaseFrame, StdCtrls, ExtCtrls;

type
  TAlohaDBDTSourceFrm = class(TOptionsBaseFrm)
    rgSource: TRadioGroup;
  private
    function GetTakeDBDTFromJobCodes: boolean;
    procedure SetTakeDBDTFromJobCodes(const Value: boolean);
  public
    property TakeDBDTFromJobCodes: boolean read GetTakeDBDTFromJobCodes write SetTakeDBDTFromJobCodes;
    procedure Clear(enable: boolean);
  end;

implementation

uses
  gdyCommon;
{$R *.dfm}

{ TAlohaDBDTSourceFrm }

procedure TAlohaDBDTSourceFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    rgSource.ItemIndex := -1;
  finally
    FBlockOnChange := false;
  end;
end;

function TAlohaDBDTSourceFrm.GetTakeDBDTFromJobCodes: boolean;
begin
  Result := rgSource.ItemIndex = 1;
end;

procedure TAlohaDBDTSourceFrm.SetTakeDBDTFromJobCodes(const Value: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    rgSource.ItemIndex := ord(Value);
  finally
    FBlockOnChange := false;
  end;

end;

end.
 