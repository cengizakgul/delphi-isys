inherited MainFm: TMainFm
  Left = 545
  Top = 288
  Height = 600
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 145
    Height = 398
    ActivePage = tbshBatchIDtoDBDTMapping
    object tbshEdMap: TTabSheet [1]
      Caption = 'E/D Mapping'
      ImageIndex = 3
      inline EDCodeFrame: TEDCodeFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 370
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 776
          Height = 370
          inherited evSplitter2: TSplitter
            Top = 106
            Width = 776
          end
          inherited pnltop: TPanel
            Width = 776
            Height = 106
            inherited evSplitter1: TSplitter
              Height = 130
            end
            inherited pnlTopLeft: TPanel
              Height = 130
              inherited dgLeft: TReDBGrid
                Height = 105
              end
            end
            inherited pnlTopRight: TPanel
              Width = 506
              Height = 130
              inherited evPanel4: TPanel
                Width = 506
              end
              inherited dgRight: TReDBGrid
                Width = 506
                Height = 105
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 111
            Width = 776
            inherited pnlbottom: TPanel
              Width = 776
              inherited evPanel5: TPanel
                Width = 776
              end
              inherited dgBottom: TReDBGrid
                Width = 776
              end
            end
            inherited pnlMiddle: TPanel
              Width = 776
            end
          end
        end
      end
    end
    object tbshJobToDBDTMapiing: TTabSheet [2]
      Caption = 'Job to D/B/D/T Mapiing'
      ImageIndex = 6
      inline JobToDBDTFrame: TJobToDBDTFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 370
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 776
          Height = 305
          inherited evSplitter2: TSplitter
            Top = 68
            Width = 776
          end
          inherited pnltop: TPanel
            Width = 776
            Height = 68
            inherited evSplitter1: TSplitter
              Height = 68
            end
            inherited pnlTopLeft: TPanel
              Height = 68
              inherited dgLeft: TReDBGrid
                Height = 43
              end
            end
            inherited pnlTopRight: TPanel
              Width = 618
              Height = 68
              inherited evPanel4: TPanel
                Width = 618
              end
              inherited dgRight: TReDBGrid
                Width = 618
                Height = 43
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 73
            Width = 776
            inherited pnlbottom: TPanel
              Width = 776
              inherited evPanel5: TPanel
                Width = 776
              end
              inherited dgBottom: TReDBGrid
                Width = 776
              end
            end
            inherited pnlMiddle: TPanel
              Width = 776
            end
          end
        end
        inherited Panel1: TPanel
          Width = 776
        end
      end
    end
    object tbshJobMapping: TTabSheet [3]
      Caption = 'Job Mapping'
      ImageIndex = 5
      inline JobsFrame: TJobsFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 370
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 776
          Height = 370
          inherited evSplitter2: TSplitter
            Top = 160
            Width = 776
          end
          inherited pnltop: TPanel
            Width = 776
            Height = 160
            inherited evSplitter1: TSplitter
              Height = 184
            end
            inherited pnlTopLeft: TPanel
              Height = 184
              inherited dgLeft: TReDBGrid
                Height = 159
              end
            end
            inherited pnlTopRight: TPanel
              Width = 506
              Height = 184
              inherited evPanel4: TPanel
                Width = 506
              end
              inherited dgRight: TReDBGrid
                Width = 506
                Height = 159
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 165
            Width = 776
            Height = 205
            inherited pnlbottom: TPanel
              Width = 776
              Height = 164
              inherited evPanel5: TPanel
                Width = 776
              end
              inherited dgBottom: TReDBGrid
                Width = 776
                Height = 139
              end
            end
            inherited pnlMiddle: TPanel
              Width = 776
            end
          end
        end
      end
    end
    object tbshBatchIDtoDBDTMapping: TTabSheet [4]
      Caption = 'Batch ID to D/B/D/T Mapping'
      ImageIndex = 4
      inline DBDTFrame: TDBDTFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 370
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 776
          Height = 305
          inherited evSplitter2: TSplitter
            Top = 95
            Width = 776
          end
          inherited pnltop: TPanel
            Width = 776
            Height = 95
            inherited evSplitter1: TSplitter
              Height = 95
            end
            inherited pnlTopLeft: TPanel
              Height = 95
              inherited dgLeft: TReDBGrid
                Height = 70
              end
            end
            inherited pnlTopRight: TPanel
              Width = 618
              Height = 95
              inherited evPanel4: TPanel
                Width = 618
              end
              inherited dgRight: TReDBGrid
                Width = 618
                Height = 70
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 100
            Width = 776
            Height = 205
            inherited pnlbottom: TPanel
              Width = 776
              Height = 164
              inherited evPanel5: TPanel
                Width = 776
              end
              inherited dgBottom: TReDBGrid
                Width = 776
                Height = 139
              end
            end
            inherited pnlMiddle: TPanel
              Width = 776
            end
          end
        end
        inherited Panel1: TPanel
          Width = 776
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Import'
      object Bevel1: TBevel
        Left = 0
        Top = 330
        Width = 776
        Height = 6
        Align = alBottom
        Shape = bsBottomLine
      end
      object pnlBottom: TPanel
        Left = 0
        Top = 336
        Width = 776
        Height = 34
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Left = 8
          Top = 6
          Width = 129
          Height = 25
          Action = RunAction
          Caption = 'Import timeclock data'
          TabOrder = 0
        end
      end
      object pnlPayrollBatch: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 248
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        inline PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Left = 0
          Top = 0
          Width = 776
          Height = 248
          Align = alClient
          TabOrder = 0
          inherited Splitter1: TSplitter
            Height = 248
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 248
            inherited dgGrid: TReDBGrid
              Height = 223
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 457
            Height = 248
            inherited Panel1: TPanel
              Width = 457
            end
            inherited dgGrid: TReDBGrid
              Width = 457
              Height = 223
            end
          end
        end
      end
      object pnlSetups: TPanel
        Left = 0
        Top = 248
        Width = 776
        Height = 82
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object Bevel3: TBevel
          Left = 0
          Top = 0
          Width = 8
          Height = 82
          Align = alLeft
          Shape = bsLeftLine
        end
        inline AlohaOptionsFrame: TAlohaOptionsFrm
          Left = 8
          Top = 0
          Width = 768
          Height = 82
          Align = alClient
          TabOrder = 0
          inherited cbSummarize: TCheckBox
            Top = 50
          end
          inherited cbIgnoreNotMappedFields: TCheckBox
            Top = 30
          end
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 543
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 158
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 13
        Top = 8
        Width = 140
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 158
      Top = 0
      Width = 626
      Height = 41
      Align = alClient
      TabOrder = 1
      inherited dblcCO: TwwDBLookupCombo
        Width = 225
      end
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 41
    Width = 784
    Height = 96
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    inline FilesOpenFrame: TFilesOpenFrm
      Left = 0
      Top = 0
      Width = 784
      Height = 96
      Align = alClient
      TabOrder = 0
      inherited Panel1: TPanel
        Width = 784
        Height = 96
        inherited Panel2: TPanel
          Width = 166
          Height = 96
          inherited BitBtn1: TBitBtn
            Left = 12
            Width = 141
            Caption = '&Open Aloha files'
          end
        end
        inherited mmFiles: TMemo
          Left = 166
          Width = 608
          Height = 96
        end
        inherited Panel3: TPanel
          Left = 774
          Width = 10
          Height = 96
        end
      end
      inherited ActionList1: TActionList
        inherited FileOpen1: TFileOpen
          Caption = '&Open Aloha files'
          Hint = 'Open|Opens Aloha files'
        end
      end
    end
  end
  object pnlSpacer: TPanel
    Left = 0
    Top = 137
    Width = 784
    Height = 8
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
  end
  inline AlohaDBDTSourceFrame: TAlohaDBDTSourceFrm
    Left = 0
    Top = 89
    Width = 161
    Height = 56
    TabOrder = 5
    inherited rgSource: TRadioGroup
      Left = 12
      Width = 142
      Height = 49
    end
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object RunAction: TAction
      Caption = 'Import timeclock data'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object RunEmployeeExport: TAction
      Caption = 'Update XYZ Employee Records for Selected Company'
    end
  end
end
