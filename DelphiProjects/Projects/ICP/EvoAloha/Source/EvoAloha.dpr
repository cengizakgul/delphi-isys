program EvoAloha;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm},
  aloha in 'aloha.pas',
  EDCodeFrame in 'EDCodeFrame.pas' {EDCodeFrm: TFrame},
  alohaoptionsframe in 'alohaoptionsframe.pas' {AlohaOptionsFrm: TFrame},
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  alohadecl in 'alohadecl.pas',
  alohaparser in 'alohaparser.pas',
  JobsFrame in 'JobsFrame.pas' {JobsFrm: TFrame},
  MidasLib,
  DBDTFrame in 'DBDTFrame.pas' {DBDTFrm: TFrame},
  FilesOpenFrame in '..\..\common\FilesOpenFrame.pas' {FilesOpenFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  JobToDBDTFrame in 'JobToDBDTFrame.pas' {JobToDBDTFrm: TFrame},
  AlohaDBDTSourceFrame in 'AlohaDBDTSourceFrame.pas' {AlohaDBDTSourceFrm: TFrame};

{$R *.res}

begin
  LicenseKey := 'A7EC03975A60478D845855A6CC71DBF3'; 

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution Aloha Import';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
