unit aloha;

interface

uses
  gdyCommonLogger, alohadecl, alohaparser, common, timeclockimport, gdyBinder,
  evodata;

function GetTimeClockData(Logger: ICommonLogger; alohaBatch: TAlohaBatch; options: TAlohaOptions; EDCodeMatcher: IMatcher; JobMatcher: IMatcher; BatchToDBDTMatcher: IMatcher; JobToDBDTMatcher: IMatcher; eedata: TEvoEEData): TTimeClockImportData;

implementation

uses
  gdycommon, SysUtils, gdyclasses, StrUtils, gdystrset,
  RateImportOptionFrame, variants, evConsts;

function FigureDBDTLevel(dbdt: Variant): char;
var
  sz: integer;
begin
  Result := CLIENT_LEVEL_COMPANY; //to make compiler happy
  if not VarHasValue(dbdt) then
    Result := CLIENT_LEVEL_COMPANY
  else if not VarIsArray(dbdt) then
    Result := CLIENT_LEVEL_DIVISION
  else
  begin
    sz := VarArrayHighBound(dbdt,1) - VarArrayLowBound(dbdt,1) + 1;
    case sz of
      1: Assert(false);
      2: Result := CLIENT_LEVEL_BRANCH;
      3: Result := CLIENT_LEVEL_DEPT;
      4: Result := CLIENT_LEVEL_TEAM;
    else
      Assert(false);
    end;
  end;
end;

function ConvertToEvoTC(Logger: ICommonLogger; alohaBatch: TAlohaBatch; options: TAlohaOptions; EDCodeMatcher: IMatcher; JobMatcher: IMatcher; BatchToDBDTMatcher: IMatcher; JobToDBDTMatcher: IMatcher;eedata: TEvoEEData): TTimeClockRecs;
var
  i: integer;
  d: TAlohaDataItem;
  cur: PTimeClockRec;
  jobcode: Variant;

  procedure AddRec(fieldType: TAlohaFieldType);
  var
    edcode: variant;
    level: char;
    dbdt: Variant;
  begin
    SetLength(Result, Length(Result)+1 ); //initialized with zeroes
    cur := @Result[high(Result)];
    cur.CUSTOM_EMPLOYEE_NUMBER := d.EECode;
    cur.SSN := d.SSN;

    dbdt := Null;
    if Options.TakeDBDTFromJobCodes then
    begin
      if (JobToDBDTMatcher <> nil) and VarHasValue(d.JobCode) then
      begin
        dbdt := JobToDBDTMatcher.RightMatch(d.JobCode);
        if not VarHasValue(dbdt) then
          raise Exception.CreateFmt('Aloha job ''%s'' isn''t mapped to an Evolution D/B/D/T',[VarToStr(d.JobCode)]);
      end
    end
    else
    begin
      if BatchToDBDTMatcher <> nil then
      begin
        dbdt := BatchToDBDTMatcher.RightMatch(alohaBatch.BatchID);
        if not VarHasValue(dbdt) then
          raise Exception.CreateFmt('Aloha Batch ID ''%s'' is not mapped to an Evolution D/B/D/T Code',[alohaBatch.BatchID]);
      end
    end;

    if not VarIsNull(dbdt) then
    begin
      if eedata.LocateEE(d.EECode) then
      begin
        cur.Division := eedata.EEs.FieldByName('CUSTOM_DIVISION_NUMBER').AsString;
        cur.Branch := eedata.EEs.FieldByName('CUSTOM_BRANCH_NUMBER').AsString;
        cur.Department := eedata.EEs.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString;
        cur.Team := eedata.EEs.FieldByName('CUSTOM_TEAM_NUMBER').AsString;
      end;
      //else tc import will give an error that this employee isn't found

      level := FigureDBDTLevel(dbdt);
      if level = CLIENT_LEVEL_DIVISION then
        cur.Division := dbdt;
      if level > CLIENT_LEVEL_DIVISION then
        cur.Division := dbdt[0];
      if level >= CLIENT_LEVEL_BRANCH then
        cur.Branch := dbdt[1];
      if level >= CLIENT_LEVEL_DEPT then
        cur.Department := dbdt[2];
      if level >= CLIENT_LEVEL_TEAM then
        cur.Team := dbdt[3];
    end;

    //Empty rate, Amount or hours is OK
    edcode := EDCodeMatcher.RightMatch(fieldType);
    if not VarIsNull(edcode) then
      cur.EDCode := edcode
    else
      raise Exception.CreateFmt('Aloha field ''%s'' isn''t mapped to an Evolution Client E/D Code.',[GetAlohaFieldName(fieldType)]);
  end;

  procedure AddHours(hours: variant; fieldType: TAlohaFieldType);
  begin
    if VarHasValue(hours) then
    begin
      AddRec(fieldType);
      cur.Hours := StrToFloat(hours) / 100.0;
      if options.RateImport = rateUseExternal then
        if VarHasValue(d.PayRate) then
          cur.Rate := StrToFloat(d.PayRate) / 10000.0;
    end;
  end;

  procedure AddAmount(amount: variant; fieldType: TAlohaFieldType);
  begin
    if VarHasValue(amount) and not ( options.IgnoreNotMappedFields and VarIsNull(EDCodeMatcher.RightMatch(fieldType)) ) then
    begin
      AddRec(fieldType);
      cur.Amount := StrToFloat(amount) / 100.0;
    end;
  end;

begin
  SetLength(Result, 0);
  for i := 0 to high(alohaBatch.Data) do
  begin
    logger.LogEntry('Converting Aloha record to Evolution format');
    try
      try
        d := alohaBatch.Data[i];
        logger.LogContextItem(sCtxEECode, d.EECode);
        if VarHasValue(d.SSN) then
          logger.LogContextItem('SSN', d.SSN );

        AddHours(d.RegularHours, alohaRegularHours);
        if options.ImportJobCodes and VarHasValue(d.JobCode) then
        begin
          Assert(not options.TakeDBDTFromJobCodes);
          jobcode := JobMatcher.RightMatch(d.JobCode);
          if not VarHasValue(jobcode) then
            raise Exception.CreateFmt('Aloha Job Code ''%s'' isn''t mapped to an Evolution Job Code',[VarToStr(d.JobCode)]);
          cur.Job := jobcode;
        end;
        AddHours(d.OTHours, alohaOvertimeHours);

        AddAmount(d.DeclaredTips, alohaDeclaredTips);
        AddAmount(d.CashTips, alohaCashTips);
        AddAmount(d.Sales, alohaSales);
        AddAmount(d.CreditCardTips, alohaCreditCardTips);
      except
        logger.PassthroughException;
      end;
    finally
      logger.LogExit;
    end;
  end;
end;

function GetTimeClockData(Logger: ICommonLogger; alohaBatch: TAlohaBatch; options: TAlohaOptions; EDCodeMatcher: IMatcher; JobMatcher: IMatcher; BatchToDBDTMatcher: IMatcher; JobToDBDTMatcher: IMatcher; eedata: TEvoEEData): TTimeClockImportData;
var
  recs: TTimeClockRecs;
begin
  SetLength(recs, 0);
  logger.LogEntry('Converting Aloha data to Evolution format');
  try
    try
      Logger.LogContextItem('Aloha file', alohaBatch.SourceFileName);
      Logger.LogDebug('E/D mapping', EDCodeMatcher.Dump);

      Assert(not Options.TakeDBDTFromJobCodes or not Options.ImportJobCodes);

      if Options.TakeDBDTFromJobCodes then
      begin
        if JobToDBDTMatcher <> nil then
          Logger.LogDebug('Job to DBDT mapping', JobToDBDTMatcher.Dump)
        else
          Logger.LogDebug('No job to DBDT mapping');
      end
      else
      begin
        Logger.LogDebug('Job mapping', JobMatcher.Dump);
        if BatchToDBDTMatcher <> nil then
          Logger.LogDebug('Batch ID to DBDT mapping', BatchToDBDTMatcher.Dump)
        else
          Logger.LogDebug('No Batch ID to DBDT mapping');
      end;

      Assert(not Options.TakeDBDTFromJobCodes or not Options.ImportJobCodes);

      recs := ConvertToEvoTC( logger, alohaBatch, options, EDCodeMatcher, JobMatcher, BatchToDBDTMatcher, JobToDBDTMatcher, eedata);
      if options.Summarize then
        recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
      Result := BuildTimeClockImportData(recs, options.RateImport = rateUseEvoEE, false, logger); //false if rateUseExternal; it was this way before and everybody was happy...
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

end.
