@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoAloha
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoAloha.exe
cd ..\..\..\Projects\ICP\EvoAloha\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoAloha.msi 

"%pWiXDir%\candle.exe" .\EvoAloha.wxs -wx -out ..\..\..\..\..\Tmp\EvoAloha.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoAlohaRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoAlohaRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoAlohaRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoAloha.wixobj ..\..\..\..\..\Tmp\EvoAlohaRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoAloha.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoAloha.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoAloha_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoAloha_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoAloha.msi EvoAloha_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
