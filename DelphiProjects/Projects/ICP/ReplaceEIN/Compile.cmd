@ECHO OFF
REM Parameters: 
REM    1. Version(optional)
REM    2. Flags: (R)elease or (D)ebug (optional)

SET pAppVersion=%1
IF "%pAppVersion%" == "" GOTO compile

SET pCompileOptions=-DPRODUCTION_LICENCE
IF "%2" == "R" SET pCompileOptions=%pCompileOptions% -DFINAL_RELEASE

:compile

ECHO *Compiling
..\..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion% "%pCompileOptions%"

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
REM XCOPY /Y /Q .\Source\*.mes ..\..\..\Bin\ICP\ReplaceEINinACH\*.* > NUL
FOR %%a IN (..\..\..\Bin\ICP\ReplaceEINinACH\*.exe) DO ..\..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\ReplaceEINinACH\*.exe) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\ReplaceEINinACH\*.dll) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\ReplaceEINinACH\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\ReplaceEINinACH\*.mes
REM FOR %%a IN (..\..\..\Bin\ICP\ReplaceEINinACH\*.map) DO DEL /F /Q ..\..\..\Bin\ICP\ReplaceEINinACH\*.map

GOTO end

:error
IF "%pAppVersion%" == "" PAUSE
EXIT 1

:end
IF "%pAppVersion%" == "" PAUSE
