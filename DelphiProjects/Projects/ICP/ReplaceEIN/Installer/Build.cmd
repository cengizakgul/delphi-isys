@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating ReplaceEINinACH.msi 
"%pWiXDir%\candle.exe" .\ReplaceEINinACH.wxs -wx -out ..\..\..\..\..\Tmp\ReplaceEINinACH.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\ReplaceEINinACH.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\ReplaceEINinACH.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\ReplaceEINinACH.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\ReplaceEINinACH%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\ReplaceEINinACH%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\ReplaceEINinACH.msi ReplaceEINinACH%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
