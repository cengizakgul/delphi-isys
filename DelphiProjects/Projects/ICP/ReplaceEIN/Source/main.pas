unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, ActnList, Buttons, ExtCtrls, ComCtrls;

const
  sGUISection = 'GUI';
  EINOptionCount = 8;

type
  TfrmMain = class(TForm)
    gbInputACH: TGroupBox;
    gbResult: TGroupBox;
    edInputFile: TEdit;
    sbInputFile: TSpeedButton;
    Actions: TActionList;
    acOpenInputFile: TAction;
    Images: TImageList;
    rbtnModifyOriginalFile: TRadioButton;
    rbtnSaveAs: TRadioButton;
    edResultFile: TEdit;
    sbResultFile: TSpeedButton;
    acResultFile: TAction;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    btnProcessFile: TButton;
    acProcess: TAction;
    btnExit: TButton;
    odACHFile: TOpenDialog;
    sdSaveAS: TSaveDialog;
    pcMain: TPageControl;
    tsChangeEIN: TTabSheet;
    tsRemoveAchOffsets: TTabSheet;
    gbChangeEIN: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    chbLineType1: TCheckBox;
    edLineType1: TEdit;
    edEIN1: TEdit;
    chbLineType2: TCheckBox;
    edLineType2: TEdit;
    edEIN2: TEdit;
    chbLineType8: TCheckBox;
    edLineType8: TEdit;
    edEIN8: TEdit;
    gbStatus: TGroupBox;
    lblCount1: TLabel;
    lblCountCaption1: TLabel;
    lblCount2: TLabel;
    lblCountCaption2: TLabel;
    lblCount3: TLabel;
    lblCountCaption3: TLabel;
    lblCount4: TLabel;
    lblCountCaption4: TLabel;
    lblCount5: TLabel;
    lblCountCaption5: TLabel;
    lblCount6: TLabel;
    lblCountCaption6: TLabel;
    lblCount7: TLabel;
    lblCountCaption7: TLabel;
    lblCountTotal: TLabel;
    lblCountCaption9: TLabel;
    lblCountCaption8: TLabel;
    lblCount8: TLabel;
    chbLineType3: TCheckBox;
    edLineType3: TEdit;
    edEIN3: TEdit;
    chbLineType4: TCheckBox;
    edLineType4: TEdit;
    edEIN4: TEdit;
    chbLineType5: TCheckBox;
    edLineType5: TEdit;
    edEIN5: TEdit;
    chbLineType6: TCheckBox;
    edLineType6: TEdit;
    edEIN6: TEdit;
    chbLineType7: TCheckBox;
    edLineType7: TEdit;
    edEIN7: TEdit;
    mmLog: TMemo;
    procedure btnExitClick(Sender: TObject);
    procedure acProcessExecute(Sender: TObject);
    procedure acOpenInputFileExecute(Sender: TObject);
    procedure acResultFileExecute(Sender: TObject);
    procedure rbtnModifyOriginalFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbLineType1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SaveACHFile(sl: TStrings);
  private
    Total: array[1..EINOptionCount] of integer;
    procedure ProcessFile(Action: integer);
    procedure ChangeEIN;
    procedure RemoveACHOffsets;
    function GetIniFileName: string;
    procedure UpdateCountStatus(k: integer);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses IniFiles;

{$R *.dfm}

function ZeroFillInt64ToStr(n: Int64; len: Integer): string;
begin
  // 123,7 = "0000123"
  Result := IntToStr(n);
  if Length(Result) > len then
    Delete(Result, 1, Length(Result) - len);
  while Length(Result) < len do
    Result := '0' + Result;
end;

procedure IncHashTotal(aABA: string; var aTotal: Int64);
var
  iAba: Int64;
begin
  // make sure only first 8 digits are passed into total (9th is a check-digit)
  if Length(Trim(aABA)) = 9 then
    iAba := StrToInt(Copy(aABA, 1, 8))
  else
    iAba := StrToInt(aABA);

  aTotal := aTotal + iAba;
end;


procedure TfrmMain.btnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.ProcessFile(Action: integer);
begin
  if Action = 0 then
    ChangeEIN
  else if Action = 1 then
    RemoveACHOffsets;
end;

procedure TfrmMain.acProcessExecute(Sender: TObject);
begin
  if not FileExists( edInputFile.Text ) then
    ShowMessage('Input ACH file is not found! Please, select an existing ACH file.')
  else
    ProcessFile(pcMain.ActivePageIndex);
end;

procedure TfrmMain.acOpenInputFileExecute(Sender: TObject);
begin
  if odACHFile.Execute then
    edInputFile.Text := odACHFile.FileName;
end;

procedure TfrmMain.acResultFileExecute(Sender: TObject);
begin
  if sdSaveAS.Execute then
    edResultFile.Text := sdSaveAS.FileName;
end;

procedure TfrmMain.rbtnModifyOriginalFileClick(Sender: TObject);
begin
  edResultFile.Enabled := not rbtnModifyOriginalFile.Checked;
  sbResultFile.Enabled := not rbtnModifyOriginalFile.Checked;
  if not edResultFile.Enabled then
    edResultFile.Color := clBtnFace
  else
    edResultFile.Color := clWindow;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  ini: TIniFile;
  fName: string;
  i: integer;
  cmt: TComponent;
  s: string;
begin
  fName := GetIniFileName;
  if FileExists(fName) then
  begin
    ini := TIniFile.Create(fName);
    try
      rbtnModifyOriginalFile.Checked := ini.ReadBool(sGUISection, rbtnModifyOriginalFile.Name, True);
      rbtnSaveAs.Checked := not rbtnModifyOriginalFile.Checked;
      for i := 1 to EINOptionCount do
      begin
        cmt := Self.FindComponent('edEIN' + IntToStr(i));
        if Assigned(cmt) and (Cmt is TEdit) then
          (cmt as TEdit).Text := ini.ReadString(sGUISection, cmt.Name, '');

        cmt := Self.FindComponent('edLineType' + IntToStr(i));
        if Assigned(cmt) and (Cmt is TEdit) then
        begin
          s := ini.ReadString(sGUISection, cmt.Name, '');
          (cmt as TEdit).Text := s;
        end;

        cmt := Self.FindComponent('chbLineType' + IntToStr(i));
        if Assigned(cmt) and (Cmt is TCheckBox) then
        begin
          (cmt as TCheckBox).Checked := ini.ReadBool(sGUISection, cmt.Name, True);
          if s = '' then
            (cmt as TCheckBox).Checked := False;
        end;
      end;
    pcMain.ActivePageIndex := ini.ReadInteger(sGUISection, pcMain.Name, 0);
    finally
      ini.Free;
    end;
    rbtnModifyOriginalFileClick(Sender);
  end;  
end;

function TfrmMain.GetIniFileName: string;
begin
  Result := Application.ExeName;
  Result := Copy(Result, 1, Length(Result) - 3) + 'ini';
end;

procedure TfrmMain.chbLineType1Click(Sender: TObject);
var
  i: string;
  cmt: TComponent;
begin
  i := Copy((Sender as TCheckBox).Name, 12, 1);
  cmt := Self.FindComponent('edEIN' + i);
  if Assigned(cmt) and (Cmt is TEdit) then
  begin
    (cmt as TEdit).Enabled := (Sender as TCheckbox).Checked;
    if (cmt as TEdit).Enabled then
      (cmt as TEdit).Color := clWindow
    else
      (cmt as TEdit).Color := clBtnFace;
  end;
  cmt := Self.FindComponent('edLineType' + i);
  if Assigned(cmt) and (Cmt is TEdit) then
  begin
    (cmt as TEdit).Enabled := (Sender as TCheckbox).Checked;
    if (cmt as TEdit).Enabled then
      (cmt as TEdit).Color := clWindow
    else
      (cmt as TEdit).Color := clBtnFace;
  end;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ini: TIniFile;
  fName: string;
  i: integer;
  cmt: TComponent;
begin
  fName := GetIniFileName;
  ini := TIniFile.Create(fName);
  try
    ini.WriteBool(sGUISection, rbtnModifyOriginalFile.Name, rbtnModifyOriginalFile.Checked);
    for i := 1 to EINOptionCount do
    begin
      cmt := Self.FindComponent('edEIN' + IntToStr(i));
      if Assigned(cmt) and (Cmt is TEdit) then
        ini.WriteString(sGUISection, cmt.Name, (cmt as TEdit).Text);

      cmt := Self.FindComponent('edLineType' + IntToStr(i));
      if Assigned(cmt) and (Cmt is TEdit) then
        ini.WriteString(sGUISection, cmt.Name, (cmt as TEdit).Text);

      cmt := Self.FindComponent('chbLineType' + IntToStr(i));
      if Assigned(cmt) and (Cmt is TCheckBox) then
        ini.WriteBool(sGUISection, cmt.Name, (cmt as TCheckBox).Checked);
    end;
    ini.WriteInteger(sGUISection, pcMain.Name, pcMain.ActivePageIndex);
  finally
    ini.Free;
  end;
end;

procedure TfrmMain.UpdateCountStatus(k: integer);
var
  cmt: TComponent;
  r, i: integer;
begin
  cmt := Self.FindComponent('lblCount' + IntToStr(k));
  if Assigned(cmt) and (cmt is TLabel) then
    (cmt as TLabel).Caption := IntToStr(Total[k]);

  r := 0;
  for i := 1 to EINOptionCount do
    r := r + Total[i];

  lblCountTotal.Caption := IntToStr(r);
end;

procedure TfrmMain.ChangeEIN;
var
  ACH: TStrings;
  ReplaceValues: TStrings;
  i, k: integer;
  cmt: TComponent;
  s: string;
begin
  ReplaceValues := TStringList.Create;
  ACH := TStringList.Create;
  try
    for k := 1 to EINOptionCount do
    begin
      Total[k] := 0;
      UpdateCountStatus(k);
      s := '';
      cmt := Self.FindComponent('chbLineType' + IntToStr(k));
      if Assigned(cmt) and (Cmt is TCheckBox) and (cmt as TCheckBox).Checked then
      begin
        cmt := Self.FindComponent('edLineType' + IntToStr(k));
        if Assigned(cmt) and (Cmt is TEdit) then
          s := (Cmt as TEdit).Text;
        if Trim(s) <> '' then
        begin
          cmt := Self.FindComponent('edEIN' + IntToStr(k));
          if Assigned(cmt) and (Cmt is TEdit) then
            s := s + ReplaceValues.NameValueSeparator + (cmt as TEdit).Text
        end;
      end;
      ReplaceValues.Add( s );
    end;

    ACH.LoadFromFile( edInputFile.Text );
    for i := 0 to ACH.Count - 1 do
    if ACH[i][1] = '5' then
      for k := 1 to EINOptionCount do
        if (ReplaceValues.Names[k-1] <> '') and (Pos(ReplaceValues.Names[k-1], Copy(ACH[i], 54, Length(ACH[i]))) = 1) then
        begin
          ACH[i] := Copy(ACH[i], 1, 41) + Copy(ReplaceValues.ValueFromIndex[k-1] + '         ', 1, 9) + Copy(ACH[i], 51, Length(ACH[i]));
          Total[k] := Total[k] + 1;
          UpdateCountStatus(k);
        end;

    SaveACHFile( ACH );
  finally
    ACH.Free;
    ReplaceValues.Free;
  end;
end;

procedure TfrmMain.RemoveACHOffsets;
var
  ACH: TStringList;
  i, k, DetailRecords, BatchRecords: integer;
  Hash, Db, Cr: Int64;
//  PriorBatchwasACHOffset, PriorBatchwasReversalPlus: boolean;
begin
  mmLog.Lines.Clear;
  ACH := TStringList.Create;
  try
    ACH.LoadFromFile( edInputFile.Text );
    k := ACH.Count - 1;
    i := 0;
    DetailRecords := 0;
    BatchRecords := 0;
    Hash := 0;
    DB := 0;
    CR := 0;
//    PriorBatchwasACHOffset := False;
//    PriorBatchwasReversalPlus := False;
    while i <= k do
    begin
      if (ACH[i][1] = '5') and (
        (Pos('ACHOFFSET', ACH[i]) > 0) or
        (Pos('REVERSAL+D', ACH[i]) > 0) or
        (Pos('REVERSAL+A', ACH[i]) > 0) {or
        ( ( (Pos('PAYROLL', ACH[i]) > 0) or
            (Pos('NET=PAY', ACH[i]) > 0) )
          and PriorBatchwasACHOffset) or
        ( ( (Pos('REVERSAL-A', ACH[i]) > 0) or
            (Pos('REVERSAL-D', ACH[i]) > 0) )
          and PriorBatchwasReversalPlus)}) then
      begin
//        if not PriorBatchwasACHOffset then
//          PriorBatchwasACHOffset := (Pos('ACHOFFSET', ACH[i]) > 0);
//        else
//          PriorBatchwasACHOffset := False;

//        PriorBatchwasReversalPlus := (Pos('REVERSAL+D', ACH[i]) > 0) or (Pos('REVERSAL+A', ACH[i]) > 0);
        ACH.Delete(i);
        while ACH[i][1] = '6' do
        begin
          ACH.Delete(i);
          Inc(DetailRecords);
        end;
        if ACH[i][1] = '8' then
        begin
          Db := Db + StrToInt64(Copy(ACH[i], 21, 12));
          Cr := Cr + StrToInt64(Copy(ACH[i], 33, 12));
          ACH.Delete(i);
          Inc(BatchRecords);
        end;
        k := ACH.Count - 1;
      end
      else if (ACH[i] = StringOfChar('9', 94))  then
      begin
        ACH.Delete(i);
        k := ACH.Count - 1;
      end
      else begin
        if (ACH[i][1] = '5') then
        begin
//          PriorBatchwasACHOffset := False;
//          PriorBatchwasReversalPlus := False;
        end;
        if ACH[i][1] = '6' then
          IncHashTotal(Copy(ACH[i], 4, 9), Hash);

        i := i + 1;
      end;
    end;

    mmLog.Lines.Add('Remove ' + IntToStr(DetailRecords) + ' detail records');

    if ACH[k][1] = '9' then
    begin

      Db := StrToInt64(Copy(ACH[k], 32, 12)) - Db;
      Cr := StrToInt64(Copy(ACH[k], 44, 12)) - Cr;
      DetailRecords := StrToInt(Copy(ACH[k], 14, 8)) - DetailRecords;
      BatchRecords := StrToInt(Copy(ACH[k], 2, 6)) - BatchRecords;

      ACH[k] := '9' + ZeroFillInt64ToStr(BatchRecords, 6)
        + ZeroFillInt64ToStr((k + 9) div 10, 6) + ZeroFillInt64ToStr(DetailRecords, 8)
        + ZeroFillInt64ToStr(Hash, 10) + ZeroFillInt64ToStr(Db, 12)
        + ZeroFillInt64ToStr(Cr, 12) + Copy(ACH[k], 56, 39);

      mmLog.Lines.Add('Update file totals');

      k := ACH.Count;
      if k mod 10  > 0 then
      begin
        for i := 1 to 10 - k mod 10 do
          ACH.Add( StringOfChar('9', 94) );
      end;
    end;
    mmLog.Lines.Add('Done!');

    SaveACHFile( ACH );
  finally
    ACH.Free;
  end;
end;

procedure TfrmMain.SaveACHFile(sl: TStrings);
begin
  if rbtnSaveAs.Checked then
    sl.SaveToFile( edResultFile.Text )
  else
    sl.SaveToFile( edInputFile.Text );
end;

end.
