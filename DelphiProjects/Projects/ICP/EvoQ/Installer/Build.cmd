@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoQ
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoQ.exe
cd ..\..\..\Projects\ICP\EvoQ\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoQ.msi 

"%pWiXDir%\candle.exe" EvoQ.wxs -wx -out ..\..\..\..\..\Tmp\EvoQ.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoQRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoQRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoQRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoQ.wixobj ..\..\..\..\..\Tmp\EvoQRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoQ.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoQ.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoQ_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoQ_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoQ.msi EvoQ_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
