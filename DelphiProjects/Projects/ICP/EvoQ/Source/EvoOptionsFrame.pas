unit EvoOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, qbdecl, ExtCtrls;

type
  TEvoOptionsFrm = class(TFrame)
    rbEEChecks: TRadioButton;
    rbAgencyChecks: TRadioButton;
    rbAllChecks: TRadioButton;
  private
    function GetOptions: TEvoChecksExportOptions;
    procedure SetOptions(const Value: TEvoChecksExportOptions);
  public
    property Options: TEvoChecksExportOptions read GetOptions write SetOptions;
  end;

implementation

{$R *.dfm}

uses
  gdycommon;

{ TEvoOptionsFrm }

function TEvoOptionsFrm.GetOptions: TEvoChecksExportOptions;
begin
  Assert( ord(rbEEChecks.Checked) + ord(rbAgencyChecks.Checked) + ord(rbAllChecks.Checked) = 1);
  if rbEEChecks.Checked then
    Result.Kind := evoEEChecksExport
  else if rbAgencyChecks.Checked then
    Result.Kind := evoAgencyChecksExport
  else if rbAllChecks.Checked then
    Result.Kind := evoAllChecksExport
  else
    Assert(false);
end;

procedure TEvoOptionsFrm.SetOptions(const Value: TEvoChecksExportOptions);
begin
  rbEEChecks.Checked := Value.Kind = evoEEChecksExport;
  rbAgencyChecks.Checked := Value.Kind = evoAgencyChecksExport;
  rbAllChecks.Checked := Value.Kind = evoAllChecksExport;
end;

end.
