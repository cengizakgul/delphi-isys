unit QBExportBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FileOpenFrame, ComCtrls, Buttons,
  StdCtrls, ExtCtrls, QBGLExportParamFrame, ActnList, EvoAPIConnectionParamFrame,
  gdycommonlogger, common, evoapiconnection, EvolutionMultiplePayrollFrame,
  EvoPayrollFilterFrame;

type
  TQBExportBaseFrm = class(TFrame)
    Panel1: TPanel;
    Button1: TButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    pgSource: TPageControl;
    TabSheet4: TTabSheet;
    Panel2: TPanel;
    Button5: TButton;
    TabSheet5: TTabSheet;
    FileOpenFrame: TFileOpenFrm;
    ActionList1: TActionList;
    RunAction: TAction;
    PrintReportAction: TAction;
    PrintLogAction: TAction;
    ConnectToEvo: TAction;
    PayrollFrame: TEvolutionMultiplePayrollFrm;
    EvoPayrollFilterFrame: TEvoPayrollFilterFrm;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure PrintReportActionUpdate(Sender: TObject);
    procedure PrintReportActionExecute(Sender: TObject);
    procedure PrintLogActionUpdate(Sender: TObject);
    procedure PrintLogActionExecute(Sender: TObject);
  protected
    FReport: Variant;
    FLog: Variant;
    FEvoFrame: TEvoAPIConnectionParamFrm;
    FLogger: ICommonLogger;
    FLogRecorder: ILogRecorder;
    function EvoIsOk: boolean;
  public
    procedure Init(logger: ICommonLogger; EvoFrame: TEvoAPIConnectionParamFrm; logRecorder: ILogRecorder); virtual;
  end;

implementation

{$R *.dfm}

uses
  evoapiconnectionutils, gdyGlobalWaitIndicator;

procedure TQBExportBaseFrm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoFrame.IsValid;
end;

procedure TQBExportBaseFrm.ConnectToEvoExecute(Sender: TObject);
begin
  WaitIndicator.StartWait('Getting payroll list');
  try
    PayrollFrame.FillTables(CreateEvoAPIConnection(FEvoFrame.Param, FLogger), EvoPayrollFilterFrame.Filter);
    ConnectToEvo.Caption := 'Refresh Evolution payroll list';
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TQBExportBaseFrm.PrintReportActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FReport <> '';
end;

procedure TQBExportBaseFrm.PrintReportActionExecute(Sender: TObject);
begin
  printString(FReport);
end;

procedure TQBExportBaseFrm.PrintLogActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FLog <> '';
end;

procedure TQBExportBaseFrm.PrintLogActionExecute(Sender: TObject);
begin
  printString(FLog);
end;

////////////

procedure TQBExportBaseFrm.Init(logger: ICommonLogger; EvoFrame: TEvoAPIConnectionParamFrm; logRecorder: ILogRecorder);
begin
  FLogger := logger;
  FEvoFrame := EvoFrame;
  FLogRecorder := logRecorder;
  PayrollFrame.Logger := FLogger;
  pgSource.TabIndex := ord(not FEvoFrame.IsValid);
end;

function TQBExportBaseFrm.EvoIsOk: boolean;
begin
  Result :=
      ( (pgSource.TabIndex = 0) and FEvoFrame.IsValid and PayrollFrame.CanGetPayrollsDef )
      or
      ( (pgSource.TabIndex = 1) and FileOpenFrame.IsValid );
end;

end.

