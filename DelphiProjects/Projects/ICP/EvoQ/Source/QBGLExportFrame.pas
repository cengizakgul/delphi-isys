unit QBGLExportFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QBExportBaseFrame, ActnList, FileOpenFrame,
  EvolutionMultiplePayrollFrame, ComCtrls, Buttons, StdCtrls, ExtCtrls,
  QBGLExportParamFrame, EvoPayrollFilterFrame;

type
  TQBGLExportFrm = class(TQBExportBaseFrm)
    QBParamFrame: TQBGLExportParamFrm;
    Panel4: TPanel;
    procedure RunActionUpdate(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
  private
    procedure DoExport;
  public
		constructor Create(Owner: TComponent); override;
  end;

implementation

{$R *.dfm}
uses
  EvoGLParser, gdycommon, dateutils, gdyGlobalWaitIndicator,
  qbconnection, gdyredir, EvoWaitForm, evoapiconnectionutils,
  XmlRpcTypes, common, evoapiconnection, qbdecl;

function BuildS2601ReportDef(payrolls: TEvoPayrollsDef; param: TQBGLExportParam): TEvoReportDef;
begin
  Result.Params := BuildPayrollsReportParams(payrolls);
  Result.Params.AddItem('Format', 1); //csv
  Result.Params.AddItem('bExcludeManualChecks', param.ExcludeManualChecks);
  Result.Params.AddItem('bBillingByDBDT', param.SplitBillingByDBDT);
  if param.EmployeeDetails then
    Result.Params.AddItem('ShowDetail', 2)
  else
    Result.Params.AddItem('ShowDetail', 0);

  Result.Level := 'S';
  Result.Number := 2601;
end;

procedure TQBGLExportFrm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoIsOk;
end;

procedure TQBGLExportFrm.RunActionExecute(Sender: TObject);
var
  opname: string;
begin
  opname := 'Evolution Journal Export';
  FLog := Unassigned;
  FReport := Unassigned;
  FLogRecorder.Start(false);
  try
    FLogger.LogEntry( opname );
    try
      try
			  LogQBGLExportParam(FLogger, QBParamFrame.Data);
        FLogger.LogEvent( opname + ' started' );
        case pgSource.TabIndex of
          0: begin
               LogEvoPayrollsDef(FLogger, PayrollFrame.GetPayrollsDef);
               FReport := GetEvoReport(Self, FLogger, CreateEvoAPIConnection(FEvoFrame.Param, Flogger), BuildS2601ReportDef(PayrollFrame.GetPayrollsDef, QBParamFrame.Data), 'Please wait while Evolution generates GL Report');
             end;
          1: begin
               Flogger.LogContextItem('Evolution Journal file', FileOpenFrame.Filename);
               FReport := MakeASCIIReportReallyEmpty(FileToString(FileOpenFrame.Filename));
             end;
        end;
        if not VarIsEmpty(FReport) then
        begin
          WaitIndicator.StartWait('Creating QuickBooks Transaction');
          try
            DoExport;
          finally
            WaitIndicator.EndWait;
          end;
          FLogger.LogEvent(opname + ' completed');
          ShowMessage(opname + ' completed');
        end
        else
          FLogger.LogWarning(opname + ' failed'); //message is already shown by GetEvoReport
      except
        FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
      end;
    finally
      FLogger.LogExit;
    end;
  finally
    FLog := FLogRecorder.Stop;
  end;
end;

procedure TQBGLExportFrm.DoExport;
var
  conn: TQBConnection;
  recs: TGLRecordArray;
  msg: string;
begin
  Assert( not VarIsEmpty(FReport) );
  recs := ParseGLRecords(FLogger, FReport, QBParamFrame.Data);
  conn := TQBConnection.Create( FLogger );
  try
    if not conn.ValidateGL(recs, QBParamFrame.Data) then
      raise Exception.Create('Evolution Journal validation failed');

    if conn.CheckIfAlreadyExported(recs) then
    begin
      msg := Format('Journal Entry with Date = %s already exists. You may be creating a duplicate entry.',[DateToStr(recs[0].Date)]);
      FLogger.LogWarning(msg);
      if MessageDlg( msg + 'Continue?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
        raise Exception.Create('Operation was aborted by user');
    end;
    conn.ExportGL( recs, QBParamFrame.Data )
  finally
    FreeAndNil(conn);
  end;
end;

constructor TQBGLExportFrm.Create(Owner: TComponent);
begin
  inherited;
  //designer crashed repeatedly when I tried to do this by editing the form
  FileOpenFrame.FileOpen1.Dialog.Filter := 'GL Journal files (*.csv,*.txt)|*.csv;*.txt';
  FileOpenFrame.FileOpen1.Hint := 'Open|Opens a GL Journal file';
end;

end.
