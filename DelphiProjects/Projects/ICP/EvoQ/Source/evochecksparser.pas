unit evochecksparser;

interface

uses
  classes, gdyCommonLogger, common;

type
  //these records come from Evolution
  TCheckRecord = record
    IsEECheck: boolean;
    //true
    FirstName: string;
    MiddleInitial: string;
    LastName: string;
    //false
    Payee: string;

    Memo: string;
    Date: TDateTime;
    CheckNumber: string;
    Amount: Currency;
  end;

  TCheckRecords = array of TCheckRecord;

  TChecksSource = (srcEmployeeReport, srcAgencyReport, srcFile);

function ParseCheckRecords(logger: ICommonLogger; report: string; checkSrc: TChecksSource): TCheckRecords;
function GetCheckRecordsDateRange(recs: TCheckRecords): TDateRange;


implementation

uses
  sysutils, windows, strutils, gdycommon, gdyclasses;

type
  TStringArray = array of string;



function ParseCheckRecord(logger: ICommonLogger; rec: string; nf: integer): TCheckRecord;
var
  fields: IStr;
begin
  Logger.LogEntry( 'Parsing check record' );
  try
    try
      logger.LogContextItem('Check record', rec);
      fields := CsvAsStr(rec);
      if fields.Count <> nf then
        raise Exception.CreateFmt('Wrong number of fields. Expected %d fields, got %d.', [nf, fields.Count] );
      if nf = 5 then
      begin
        Result.IsEECheck := false;
        Result.Payee := trim(fields.Str[0]);

        Result.Memo := fields.Str[1];
        Result.Date := StrToDate(fields.Str[2]);
        Result.CheckNumber := trim(fields.Str[3]);
        Result.Amount := StrToCurr(fields.Str[4]);
      end
      else if nf = 7 then
      begin
        Result.IsEECheck := true;
        Result.FirstName := trim(fields.Str[0]);
        Result.MiddleInitial := trim(fields.Str[1]);
        Result.LastName := trim(fields.Str[2]);

        Result.Memo := fields.Str[3];
        Result.Date := StrToDate(fields.Str[4]);
        Result.CheckNumber := trim(fields.Str[5]);
        Result.Amount := StrToCurr(fields.Str[6]);
      end
      else
        Assert(false);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function ParseCheckRecords(logger: ICommonLogger; report: string; checkSrc: TChecksSource ): TCheckRecords;
var
  lines: IStr;
  i: integer;
  firstLine: IStr;
begin
  lines := SplitToLines( MakeASCIIReportReallyEmpty(report) );
  while (lines.Count > 0) and (trim(lines.Str[lines.Count-1]) = '') do
    lines.Delete(lines.Count-1);

  if lines.Count = 0  then
    raise Exception.Create('Evolution Checks report is empty');

  firstLine := CsvAsStr(lines.Str[0]);
  case checkSrc of
    srcEmployeeReport:
      if firstLine.Count <> 7 then
        raise Exception.CreateFmt('Wrong number of fields. Expected 7 fields, got %d.', [firstLine.Count] );
    srcAgencyReport:
      if firstLine.Count <> 5 then
        raise Exception.CreateFmt('Wrong number of fields. Expected 5 fields, got %d.', [firstLine.Count] );
    srcFile:
    begin
      if not (firstLine.Count in [5, 7]) then
        raise Exception.CreateFmt('Wrong number of fields. Expected 5 (for Agency Checks) or 7 fields (for Employee Checks), got %d.', [firstLine.Count] );
      if firstLine.Count = 5 then
        logger.LogEvent('Report recognized as S1911 Agency Checks Report');
      if firstLine.Count = 7 then
        logger.LogEvent('Report recognized as S1910 Employee Checks Report');
    end
  end;

  if (firstLine.Count = 5) and (lines.Count = 1) and
     (firstLine.Str[0] = '') and (firstLine.Str[1] = 'Payroll Agency') and
     (firstLine.Str[2] = '') and (firstLine.Str[3] = '') and (firstLine.Str[4] = '') then
    raise Exception.Create('Evolution Agency Checks report is empty');

  SetLength(Result, lines.Count);
  for i := 0 to lines.Count-1 do
    Result[i] := ParseCheckRecord(logger, lines.Str[i], firstLine.Count);
end;

function GetCheckRecordsDateRange(recs: TCheckRecords): TDateRange;
var
  i: integer;
begin
  Assert( Length(recs)>0 );
  Result.FromDate := StrToDate('1/1/2100'); //any
  Result.ToDate := StrToDate('1/1/1900');  //any
  for i := low(recs) to high(recs) do
  begin
    if recs[i].Date < Result.FromDate then
      Result.FromDate := recs[i].Date;
    if recs[i].Date > Result.ToDate then
      Result.ToDate := recs[i].Date;
  end
end;

end.
