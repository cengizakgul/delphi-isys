program EvoQ;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  QBFC13Lib_TLB in 'QBFC13Lib_TLB.pas',
  EvoGLParser in 'EvoGLParser.pas',
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TOptionsBaseFrm},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  QBConnection in 'QBConnection.pas',
  QBGLExportParamFrame in 'QBGLExportParamFrame.pas' {QBGLExportParamFrm: TFrame},
  FileOpenFrame in '..\..\common\FileOpenFrame.pas' {FileOpenFrm: TFrame},
  EvoWaitForm in 'EvoWaitForm.pas' {EvoWaitFm},
  WaitForm in '..\..\common\WaitForm.pas' {WaitFm},
  QBExportBaseFrame in 'QBExportBaseFrame.pas' {QBExportBaseFrm: TFrame},
  EvolutionMultiplePayrollFrame in '..\..\common\EvolutionMultiplePayrollFrame.pas' {EvolutionMultiplePayrollFrm: TFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  QBGLExportFrame in 'QBGLExportFrame.pas' {QBGLExportFrm: TQBExportBaseFrm},
  QBChecksExportFrame in 'QBChecksExportFrame.pas' {QBChecksExportFrm: TQBExportBaseFrm},
  QBChecksExportParamFrame in 'QBChecksExportParamFrame.pas' {QBChecksExportParamFrm: TFrame},
  QBDecl in 'QBDecl.pas',
  evochecksparser in 'evochecksparser.pas',
  EvoOptionsFrame in 'EvoOptionsFrame.pas' {EvoOptionsFrm: TFrame},
  EvoPayrollFilterFrame in '..\..\common\EvoPayrollFilterFrame.pas' {EvoPayrollFilterFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  UtilityLoggerViewFrame in '..\..\common\UtilityLoggerViewFrame.pas' {UtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  MainForm in 'MainForm.pas' {MainFm};

{$R *.res}

begin
  LicenseKey := '5B408E0E8C5A45C281DF32F292421F50';

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution QB Export';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
