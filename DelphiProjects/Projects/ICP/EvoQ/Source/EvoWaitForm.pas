unit EvoWaitForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, evoapiconnection, gdyCommonLogger, ExtCtrls;

type
  TEvoWaitFm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    bbCancel: TBitBtn;
    Timer1: TTimer;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure Panel2Resize(Sender: TObject);
  private
    FReportTask: TEvoReportTask;
    procedure HandleReportReady(rpt: Variant);
    procedure HandleReportFailed;
  public
    FReport: string;
    FLogger: ICommonLogger;
    FConnection: IEvoAPIConnection;
    FReportDef: TEvoReportDef;

    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;

    procedure SetText(t: string);
  end;

function GetEvoReport(Owner: TComponent; logger: ICommonLogger; conn: IEvoAPIConnection; reportDef: TEvoReportDef; text: string): Variant;

implementation

uses
  common;

{$R *.dfm}

destructor TEvoWaitFm.Destroy;
begin
  FreeAndNil(FReportTask);
  inherited;
end;

procedure TEvoWaitFm.HandleReportFailed;
begin
  FreeAndNil(FReportTask); //it's OK, we are called by DeferredCall function
  FReport := '';
  ModalResult := mrNo;
end;

procedure TEvoWaitFm.HandleReportReady(rpt: Variant);
begin
  FreeAndNil(FReportTask); //it's OK, we are called by DeferredCall function
  FReport := rpt;
  ModalResult := mrYes;
end;

procedure TEvoWaitFm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not assigned(FReportTask) or (MessageDlg('Cancel?', mtConfirmation, [mbYes, mbNo], 0) = mrYes);
end;

procedure TEvoWaitFm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := false;
  try
    FReportTask := TEvoReportTask.Create(FConnection, FReportDef, HandleReportReady, HandleReportFailed, FLogger);
    FReportTask.Start;
  except
    ModalResult := mrNo;
    raise;
  end;
end;

constructor TEvoWaitFm.Create(Owner: TComponent);
begin
  inherited;
  Caption := Application.Title;
end;

procedure TEvoWaitFm.SetText(t: string);
begin
  Panel1.Caption := t;
end;

function GetEvoReport(Owner: TComponent; logger: ICommonLogger; conn: IEvoAPIConnection; reportDef: TEvoReportDef; text: string): Variant;
var
  fm: TEvoWaitFm;
begin
  Result := Unassigned;
  fm := TEvoWaitFm.Create(Owner);
  try
    fm.FConnection := conn;
    fm.FReportDef := reportDef;
    fm.FLogger := Logger;
    fm.SetText(text);
    case fm.ShowModal of
      mrYes: Result := MakeASCIIReportReallyEmpty(fm.FReport);
      mrCancel: logger.LogWarning('User cancelled operation');
      mrNo: ;     //already logged an error
    else
      Assert(false);
    end
  finally
    FreeAndNil(fm);
  end;
end;

procedure TEvoWaitFm.Panel2Resize(Sender: TObject);
begin
  bbCancel.Left := (Panel2.Width - bbCancel.Width) div 2;
end;

end.
