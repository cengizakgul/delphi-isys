unit QBChecksExportFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QBExportBaseFrame, ActnList, FileOpenFrame,
  EvolutionMultiplePayrollFrame, ComCtrls, Buttons, StdCtrls, ExtCtrls,
  gdyCommonLogger, QBChecksExportParamFrame, evoapiconnectionutils,
  evoapiconnection, common, EvoAPIConnectionParamFrame, EvoOptionsFrame,
  EvoPayrollFilterFrame, evochecksparser;

type
  TQBChecksExportFrm = class(TQBExportBaseFrm)
    QBParamFrame: TQBChecksExportParamFrm;
    EvoOptionsFrame: TEvoOptionsFrm;
    Panel4: TPanel;
    procedure RunActionUpdate(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
  private
    procedure ExportChecks(src: TChecksSource);
  public
    procedure Init(logger: ICommonLogger; EvoFrame: TEvoAPIConnectionParamFrm; logRecorder: ILogRecorder); override;
  end;

implementation

uses
  EvoWaitForm, gdycommon, qbconnection, gdyDeferredCall, qbdecl, gdyGlobalWaitIndicator;

{$R *.dfm}

function BuildEvChecksEEExportReportDef(payrolls: TEvoPayrollsDef): TEvoReportDef;
begin
  Result.Params := BuildPayrollsReportParams(payrolls);
  Result.Level := 'S';
  Result.Number := 1910;
end;

function BuildEvChecksAgencyExportReportDef(payrolls: TEvoPayrollsDef): TEvoReportDef;
begin
  Result.Params := BuildPayrollsReportParams(payrolls);
  Result.Level := 'S';
  Result.Number := 1911;
end;

procedure TQBChecksExportFrm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoIsOk and QBParamFrame.IsValid;
end;

procedure TQBChecksExportFrm.RunActionExecute(Sender: TObject);
begin
  FReport := Unassigned;
  FLog := Unassigned;
  FLogRecorder.Start(false);
  try
    case pgSource.TabIndex of
      0: case EvoOptionsFrame.Options.Kind of
           evoEEChecksExport: ExportChecks(srcEmployeeReport);
           evoAgencyChecksExport: ExportChecks(srcAgencyReport);
           evoAllChecksExport:
           begin
             ExportChecks(srcEmployeeReport);
             ExportChecks(srcAgencyReport);
           end;
         else
           Assert(false);
         end;
      1: ExportChecks(srcFile);
    else
      Assert(false);
    end;
  finally
    FLog := FLogRecorder.Stop;
  end;
end;

procedure TQBChecksExportFrm.ExportChecks(src: TChecksSource);
var
  repname: string;
  report: Variant;
  s: string;
  opname: string;
  conn: TQBConnection;
  stat: TExportChecksStat;
begin
  case src of
    srcEmployeeReport: repname := 'Employee Checks';
    srcAgencyReport: repname := 'Agency Checks';
    srcFile: repname := 'Checks';
  else
    Assert(false);
  end;
  opname := Format('Evolution %s Export', [repname]);

  FLogger.LogEntry( opname );
  try
    try
      FLogger.LogEvent(opname + ' started');
      case src of
        srcEmployeeReport:
          begin
            LogEvoPayrollsDef(FLogger, PayrollFrame.GetPayrollsDef);
            report := GetEvoReport(Self, FLogger, CreateEvoAPIConnection(FEvoFrame.Param, Flogger), BuildEvChecksEEExportReportDef(PayrollFrame.GetPayrollsDef), Format('Please wait while Evolution generates %s report',[repname]));;
          end;
        srcAgencyReport:
          begin
            LogEvoPayrollsDef(FLogger, PayrollFrame.GetPayrollsDef);
            report := GetEvoReport(Self, FLogger, CreateEvoAPIConnection(FEvoFrame.Param, Flogger), BuildEvChecksAgencyExportReportDef(PayrollFrame.GetPayrollsDef), Format('Please wait while Evolution generates %s report',[repname]));
          end;
        srcFile:
          begin
            Flogger.LogContextItem('Evolution Checks file', FileOpenFrame.Filename);
            report := MakeASCIIReportReallyEmpty(FileToString(FileOpenFrame.Filename));
            Flogger.LogDebug('Evolution Checks file', report);
          end;
      else
        Assert(false);
      end;

      if trim(report) = '' then
        s := Format('Evolution %s report is empty',[repname])
      else
        s := Format('Evolution %s report:'#13#10'%s',[repname, report]);

      if trim(FReport) <> '' then
        FReport := FReport + #12{form feed} + s
      else
        FReport := s;

      if not VarIsEmpty(report) then
      begin
        WaitIndicator.StartWait('Creating QuickBooks Transactions');
        try
          conn := TQBConnection.Create( FLogger );
          try
            stat := conn.ExportChecks( ParseCheckRecords(FLogger, report, src), QBParamFrame.Data);
          finally
            FreeAndNil(conn);
          end;
        finally
          WaitIndicator.EndWait;
        end;
        if stat.Errors = 0 then
        begin
          FLogger.LogEvent(opname + ' ' + ExportChecksStatToString(stat));
          ShowMessage(opname + ' successfully completed');
        end
        else
        begin
          FLogger.LogWarning(opname + ' ' + ExportChecksStatToString(stat));
	        MessageDlg(opname + ' ' + ExportChecksStatToString(stat), mtError, [mbOk], 0);
        end;
      end
      else
        FLogger.LogWarning(opname + ' failed');
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TQBChecksExportFrm.Init(logger: ICommonLogger;
  EvoFrame: TEvoAPIConnectionParamFrm; logRecorder: ILogRecorder);
begin
  inherited;
  //designer crashed repeatedly when I tried to do this by editing the form
  FileOpenFrame.FileOpen1.Dialog.Filter := 'Checks files (*.csv)|*.csv';
  FileOpenFrame.FileOpen1.Hint := 'Open|Opens a Checks file';
  QBParamFrame.Logger := logger;
  DeferredCall(QBParamFrame.FillLists);
end;

end.
