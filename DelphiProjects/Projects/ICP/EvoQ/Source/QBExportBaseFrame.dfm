object QBExportBaseFrm: TQBExportBaseFrm
  Left = 0
  Top = 0
  Width = 753
  Height = 528
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 490
    Width = 753
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Action = RunAction
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 96
      Top = 8
      Width = 81
      Height = 25
      Action = PrintReportAction
      Caption = 'Print XXX'
      TabOrder = 1
    end
    object BitBtn2: TBitBtn
      Left = 192
      Top = 8
      Width = 81
      Height = 25
      Action = PrintLogAction
      Caption = 'Print Log'
      TabOrder = 2
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 753
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 
      'You can get Evolution XXX either directly from Evolution or from' +
      ' a file'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object pgSource: TPageControl
    Left = 0
    Top = 25
    Width = 753
    Height = 465
    ActivePage = TabSheet4
    Align = alClient
    TabOrder = 2
    object TabSheet4: TTabSheet
      Caption = 'Direct connection'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 745
        Height = 65
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Button5: TButton
          Left = 327
          Top = 21
          Width = 153
          Height = 25
          Action = ConnectToEvo
          TabOrder = 0
        end
        inline EvoPayrollFilterFrame: TEvoPayrollFilterFrm
          Left = 0
          Top = 0
          Width = 314
          Height = 65
          Align = alLeft
          TabOrder = 1
        end
      end
      inline PayrollFrame: TEvolutionMultiplePayrollFrm
        Left = 0
        Top = 65
        Width = 745
        Height = 372
        Align = alClient
        TabOrder = 1
        inherited Splitter1: TSplitter
          Height = 372
        end
        inherited EvoCompany: TEvolutionCompanyFrm
          Height = 372
          inherited Splitter1: TSplitter
            Height = 372
          end
          inherited frmCL: TEvolutionDsFrm
            Height = 372
            inherited dgGrid: TreDBGrid
              Height = 347
            end
          end
          inherited frmCO: TEvolutionDsFrm
            Height = 372
            inherited dgGrid: TreDBGrid
              Height = 347
            end
          end
        end
        inherited frmPR: TEvolutionDsFrm
          Width = 106
          Height = 372
          inherited Panel1: TPanel
            Width = 106
          end
          inherited dgGrid: TreDBGrid
            Width = 106
            Height = 347
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'File'
      ImageIndex = 1
      inline FileOpenFrame: TFileOpenFrm
        Left = 0
        Top = 0
        Width = 745
        Height = 49
        Align = alTop
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 745
          inherited BitBtn1: TBitBtn
            Left = 661
          end
          inherited edtFile: TEdit
            Width = 644
          end
        end
      end
    end
  end
  object ActionList1: TActionList
    Left = 388
    Top = 576
    object RunAction: TAction
      Caption = 'Run'
    end
    object PrintReportAction: TAction
      Caption = 'Print XXX'
      OnExecute = PrintReportActionExecute
      OnUpdate = PrintReportActionUpdate
    end
    object PrintLogAction: TAction
      Caption = 'Print Log'
      OnExecute = PrintLogActionExecute
      OnUpdate = PrintLogActionUpdate
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution payroll list'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
  end
end
