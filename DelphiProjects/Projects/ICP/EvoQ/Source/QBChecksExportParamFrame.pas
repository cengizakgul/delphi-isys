unit QBChecksExportParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, qbconnection, StdCtrls, qbdecl, gdyCommonLogger, Buttons;

type
  TQBChecksExportParamFrm = class(TFrame)
    GroupBox1: TGroupBox;
    cbCleared: TCheckBox;
    Label1: TLabel;
    cbCashAccount: TComboBox;
    Label2: TLabel;
    cbLiabilityAccount: TComboBox;
    cbIncludeDD: TCheckBox;
    cbIncludeEENames: TCheckBox;
    bbGetAccountList: TBitBtn;
    procedure bbGetAccountListClick(Sender: TObject);
    procedure cbCashAccountChange(Sender: TObject);
    procedure cbLiabilityAccountChange(Sender: TObject);
    procedure cbIncludeDDClick(Sender: TObject);
    procedure cbIncludeEENamesClick(Sender: TObject);
    procedure cbClearedClick(Sender: TObject);
  private
    FData: TQBChecksExportParam;
    function GetData: TQBChecksExportParam;
    procedure SetData(const Value: TQBChecksExportParam);
    procedure FillListsJob;
    procedure DataToControls;
  public
    Logger: ICommonLogger;
    property Data: TQBChecksExportParam read GetData write SetData;
    procedure FillLists;
    function IsValid: boolean;
  end;

implementation

uses
  waitform;
{$R *.dfm}

{ TQBChecksExportParamFrm }

procedure TQBChecksExportParamFrm.FillLists;
begin
  ExecuteWithWaitForm( Self, FillListsJob, 'Getting Account list from QuickBooks' );
end;

procedure TQBChecksExportParamFrm.FillListsJob;
var
  conn: TQBConnection;
begin
  conn := TQBConnection.Create( Logger );
  try
    conn.GetAccountFullNameList( cbCashAccount.Items );
    cbLiabilityAccount.Items.Assign(cbCashAccount.Items);
    DataToControls; //data wasn't updated actually
  finally
    FreeAndNil(conn);
  end;
  bbGetAccountList.Caption := 'Refresh Account list';
end;

function TQBChecksExportParamFrm.GetData: TQBChecksExportParam;
begin
  Result := FData;
end;

function TQBChecksExportParamFrm.IsValid: boolean;
begin
  Result := (cbCashAccount.ItemIndex <> -1) and (cbLiabilityAccount.ItemIndex <> -1);
end;

procedure TQBChecksExportParamFrm.SetData(const Value: TQBChecksExportParam);
begin
  FData := Value;
  DataToControls;
end;

procedure TQBChecksExportParamFrm.bbGetAccountListClick(Sender: TObject);
begin
  FillLists;
end;

procedure TQBChecksExportParamFrm.DataToControls;
begin
  cbCleared.Checked := FData.Clear;
  cbCashAccount.ItemIndex := cbCashAccount.Items.IndexOf(FData.CashAccount);
  cbLiabilityAccount.ItemIndex := cbLiabilityAccount.Items.IndexOf(FData.LiabilityAccount);
  cbIncludeDD.Checked := FData.IncludeDD;
  cbIncludeEENames.Checked := FData.IncludeEENames;
end;

procedure TQBChecksExportParamFrm.cbCashAccountChange(Sender: TObject);
begin
  FData.CashAccount := cbCashAccount.Text;
end;

procedure TQBChecksExportParamFrm.cbLiabilityAccountChange(
  Sender: TObject);
begin
  FData.LiabilityAccount := cbLiabilityAccount.Text;
end;

procedure TQBChecksExportParamFrm.cbIncludeDDClick(Sender: TObject);
begin
  FData.IncludeDD := cbIncludeDD.Checked;
end;

procedure TQBChecksExportParamFrm.cbIncludeEENamesClick(Sender: TObject);
begin
  FData.IncludeEENames := cbIncludeEENames.Checked;
end;

procedure TQBChecksExportParamFrm.cbClearedClick(Sender: TObject);
begin
  FData.Clear := cbCleared.Checked;
end;

end.

