unit EvoGLParser;

interface

uses
  classes, qbdecl, gdycommonLogger;

type
  //these records come from Evolution
  TGLRecord = record
    CompanyCode: string;
    Date: TDateTime;
    Division: string;
    Branch: string;
    Department: string;
    GLTag: string;
    Hours: Currency;
    Debit: Currency;
    Credit: Currency;
    LastName: string;
    FirstName: string;
    PayPeriodEndDate: TDateTime;

    RecNo: integer; //in the input file
    Amount: Currency; //!!COMPAT //Debit-Credit, possibly rounded by summarize

    Account: string;
    TrClass: string;
    TrName: string;

  end;

  TGLRecordArray = array of TGLRecord;

function ParseGLRecords(logger: ICommonLogger; report: string; param: TQBGLExportParam): TGLRecordArray;

implementation

uses
  sysutils, windows, strutils, gdycommon, common, gdyclasses;

type
  TStringArray = array of string;

function ConvertToGLRecord(sa: TStringArray): TGLRecord;
  function parseDate( dt: string ): TDateTime;
  var
    s: string;
  begin
    s := trim(dt);
    if Length(s) <> 8 then
      raise Exception.CreateFmt('Wrong date: %s',[dt]);
    Result := EncodeDate( strtoint(LeftStr(s, 4)), strtoint(MidStr(s, 5, 2)), strtoint(RightStr(s, 2)) );
  end;

  function parseCurr( v: string ): Currency;
  begin
    if trim(v) = '' then
    begin
      Result := 0;
    end
    else
    begin
      Result := StrToCurr(trim(v));
    end

  end;
begin
  Result.CompanyCode := sa[0];
  Result.Date := parseDate(sa[1]);
  Result.Division := sa[2];
  Result.Branch := sa[3];
  Result.Department := sa[4];
  Result.GLTag := sa[5];
  Result.Hours := parseCurr(sa[6]);
  Result.Debit := parseCurr(sa[7]);
  Result.Credit := parseCurr(sa[8]);
  Result.LastName := sa[9];
  Result.FirstName := sa[10];
  if trim(sa[11]) <> '' then
    Result.PayPeriodEndDate := parseDate(sa[11])
  else
    Result.PayPeriodEndDate := 0;

  Result.Amount := Result.Debit - Result.Credit;
  Result.Account := trim(Result.GLTag);   //!!
  Result.TrClass := '';
  Result.TrName := '';
end;

//copy-paste from csreader.pas, fills array of string instead of TStringList,
//doesn't ignore [#1..' ']
function ParseCSVLine( Value: string; expected: integer): TStringArray;
var
  P, P1: PChar;
  S: string;
  n: integer;
  r: TStringArray;

  procedure add( v: string);
  begin
    if n = Length(r) then
      raise Exception.CreateFmt('Wrong number of fields. Got more than %d fields while parsing <%s>', [expected, Value] );
    r[n] := v;
    n := n + 1;
  end;
begin
  SetLength(r, expected);
  n := 0;

  P := PChar(Value);
  while P^ <> #0 do
  begin
    if P^ = '"' then
      S := AnsiExtractQuotedStr(P, '"')
    else
    begin
      P1 := P;
      while (P^ <> #0) and (P^ <> ',') do P := CharNext(P);
      SetString(S, P1, P - P1);
    end;
    add(S);
    if P^ = ',' then
    begin
      P := CharNext(P);
      if P^ = #0 then
        add('');
    end;
  end;
  if n < expected then
    raise Exception.CreateFmt('Wrong number of fields. Expected %d fields, got %d.', [expected, n] );
  Result := r;
end;

function ParseGLFixedWidthLine( Value: string): TStringArray;
  function fetch(first, last: integer): string;
  begin
    Result := copy(Value, first, last-first+1);
  end;
begin
  if Length(Value) <> 125 then
    raise Exception.CreateFmt('Wrong length of fixed width record. Expected %d, got %d.', [125, Length(Value)] );
  SetLength(Result, 12);
  Result[0] := fetch( 1,  4);
  Result[1] := fetch( 5, 12);
  Result[2] := fetch(13, 18);
  Result[3] := fetch(19, 24);
  Result[4] := fetch(25, 30);
  Result[5] := fetch(31, 45);
  Result[6] := fetch(46, 59);
  Result[7] := fetch(60, 73);
  Result[8] := fetch(74, 87);
  Result[9] := fetch(88, 102);
  Result[10] := fetch(103, 117);
  Result[11] := fetch(118, 125);
end;

function FixFullName(trClass: string): string;
var
  i: integer;
begin
  Result := '';
  with SplitByChar(trClass, ':') do
    for i := 0 to Count-1 do
      if i = 0 then
        Result := trim(Str[i])
      else
        Result := Result + ':' + trim(Str[i]);
//  ODS( Format('FixTrClass: "%s" -> "%s"', [trClass, Result]));
end;

function ParseGLRecords(logger: ICommonLogger; report: string; param: TQBGLExportParam): TGLRecordArray;
var
  Lines: TStringList;
  IsCSV: boolean;
  i: integer;
  delim: string;
  p: integer;
begin
  logger.LogEntry('Parsing GL records');
  try
    try
      Assert( (param.GLTagParsing = parseNone) or (Length(param.DelimiterInGLTag) <= 1) );
      delim := param.DelimiterInGLTag;
      if delim = '' then
        delim := ' ';

      Lines := TStringList.Create;
      Lines.Text := MakeASCIIReportReallyEmpty(report);
      try
        while (Lines.Count > 0) and (trim(Lines[Lines.Count-1]) = '') do
        begin
          Lines.Delete(Lines.Count-1);
        end;

        if Lines.Count = 0 then
          raise Exception.Create('Evolution Journal is empty');

        IsCSV := false;
        try
          ParseCSVLine(Lines[0], 12);
          IsCSV := true;
        except
          on E1: Exception do
          begin
            try
              ParseGLFixedWidthLine(lines[0]);
            except
              on E2: Exception do
                raise Exception.CreateFmt('Wrong format of Evolution Journal file. The file is neither comma-separated nor fixed-width Evolution Journal file.'#13#10'Error messages are:'#13#10'%s'#13#10'%s', [E1.Message, E2.Message]);
            end;
          end;
        end;

        SetLength(Result, Lines.Count);

        for i := 0 to Lines.Count-1 do
        begin
          logger.LogEntry('Parsing GL record');
          try
            try
              logger.LogContextItem('GL record', Lines[i]);
              if IsCSV then
                Result[i] := ConvertToGLRecord( ParseCSVLine(Lines[i], 12) )
              else
                Result[i] := ConvertToGLRecord( ParseGLFixedWidthLine(Lines[i]) );
              Result[i].RecNo := i + 1;

              if param.GLTagParsing <> parseNone then
              begin
                p := Pos(delim, Result[i].Account);
                if p <> 0 then
                begin
                  Result[i].TrClass := LeftStr(Result[i].Account, p-1);
                  Result[i].TrName := Result[i].TrClass;
                  Result[i].Account := RightStr(Result[i].Account, Length(Result[i].Account)-p);

                  p := pos(delim, Result[i].Account);
                  if p <> 0 then
                  begin
                    Result[i].TrName := LeftStr(Result[i].Account, p-1);
                    Result[i].Account := RightStr(Result[i].Account, Length(Result[i].Account)-p);

                    if pos(delim, Result[i].Account) <> 0 then
                      logger.LogWarningFmt('Multiple occurences of delimiter "%s" in GL tag ("%s"). Parsed as Class ("%s"), Customer ("%s") and Account ("%s").',
                       [delim, Result[i].TrClass + delim + Result[i].TrName + delim + Result[i].Account, Result[i].TrClass, Result[i].TrName, Result[i].Account]);
                  end;
                end;
              end;

              Result[i].TrClass := FixFullName(Result[i].TrClass);
              Result[i].TrName := FixFullName(Result[i].TrName);
              Result[i].Account := FixFullName(Result[i].Account);
            except
              logger.PassthroughException;
            end;
          finally
            logger.LogExit
          end
        end;
      finally
        FreeAndNil(Lines);
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit
  end
end;


end.

