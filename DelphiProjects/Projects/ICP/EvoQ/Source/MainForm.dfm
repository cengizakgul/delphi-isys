inherited MainFm: TMainFm
  Left = 144
  Top = 405
  Caption = 'EvoQ'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = TabSheet2
    inherited TabSheet2: TTabSheet
      Caption = 'GL Export'
      inline QBGLExportFrame: TQBGLExportFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 465
        Align = alClient
        TabOrder = 0
        inherited Panel1: TPanel
          Top = 427
          Width = 776
        end
        inherited Panel3: TPanel
          Width = 776
        end
        inherited pgSource: TPageControl
          Width = 776
          Height = 263
          inherited TabSheet4: TTabSheet
            inherited PayrollFrame: TEvolutionMultiplePayrollFrm
              Height = 230
              inherited Splitter1: TSplitter
                Height = 230
              end
              inherited EvoCompany: TEvolutionCompanyFrm
                Height = 230
                inherited Splitter1: TSplitter
                  Height = 230
                end
                inherited frmCL: TEvolutionDsFrm
                  Height = 230
                  inherited dgGrid: TReDBGrid
                    Height = 205
                  end
                end
                inherited frmCO: TEvolutionDsFrm
                  Height = 230
                  inherited dgGrid: TReDBGrid
                    Height = 205
                  end
                end
              end
              inherited frmPR: TEvolutionDsFrm
                Height = 230
                inherited dgGrid: TReDBGrid
                  Height = 205
                end
              end
            end
          end
          inherited TabSheet5: TTabSheet
            inherited FileOpenFrame: TFileOpenFrm
              Width = 768
              inherited Panel1: TPanel
                Width = 768
                inherited BitBtn1: TBitBtn
                  Left = 698
                end
                inherited edtFile: TEdit
                  Width = 633
                end
              end
            end
            inherited Panel4: TPanel
              Width = 768
            end
          end
        end
        inherited QBParamFrame: TQBGLExportParamFrm
          Top = 288
          Width = 776
          inherited GroupBox1: TGroupBox
            Width = 776
          end
        end
      end
    end
    object TabSheet4: TTabSheet [2]
      Caption = 'Checks Export'
      ImageIndex = 3
      inline QBChecksExportFrame: TQBChecksExportFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 465
        Align = alClient
        TabOrder = 0
        inherited Panel1: TPanel
          Top = 331
          Width = 776
        end
        inherited Panel3: TPanel
          Width = 776
        end
        inherited pgSource: TPageControl
          Width = 776
          Height = 306
          inherited TabSheet5: TTabSheet
            inherited FileOpenFrame: TFileOpenFrm
              Width = 768
              inherited Panel1: TPanel
                Width = 768
                inherited BitBtn1: TBitBtn
                  Left = 698
                end
                inherited edtFile: TEdit
                  Width = 633
                end
              end
            end
            inherited Panel4: TPanel
              Width = 768
            end
          end
        end
        inherited QBParamFrame: TQBChecksExportParamFrm
          Top = 369
          Width = 776
          inherited GroupBox1: TGroupBox
            Width = 776
          end
        end
      end
    end
  end
end
