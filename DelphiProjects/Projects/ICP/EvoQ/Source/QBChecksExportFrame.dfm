inherited QBChecksExportFrm: TQBChecksExportFrm
  Width = 846
  Height = 667
  inherited Panel1: TPanel
    Top = 629
    Width = 846
    inherited BitBtn1: TBitBtn
      Caption = 'Print Checks'
    end
  end
  inherited Panel3: TPanel
    Width = 846
    Caption = 
      'You can get Evolution Checks either directly from Evolution or f' +
      'rom a file'
  end
  inherited pgSource: TPageControl
    Width = 846
    Height = 508
    ActivePage = TabSheet5
    inherited TabSheet4: TTabSheet
      inherited Panel2: TPanel
        Width = 838
      end
      inherited PayrollFrame: TEvolutionMultiplePayrollFrm
        Width = 838
        Height = 369
        inherited Splitter1: TSplitter
          Height = 369
        end
        inherited EvoCompany: TEvolutionCompanyFrm
          Height = 369
          inherited Splitter1: TSplitter
            Height = 369
          end
          inherited frmCL: TEvolutionDsFrm
            Height = 369
            inherited dgGrid: TReDBGrid
              Height = 344
            end
          end
          inherited frmCO: TEvolutionDsFrm
            Height = 369
            inherited dgGrid: TReDBGrid
              Height = 344
            end
          end
        end
        inherited frmPR: TEvolutionDsFrm
          Width = 199
          Height = 369
          inherited Panel1: TPanel
            Width = 199
          end
          inherited dgGrid: TReDBGrid
            Width = 199
            Height = 344
          end
        end
      end
      inline EvoOptionsFrame: TEvoOptionsFrm
        Left = 0
        Top = 434
        Width = 838
        Height = 46
        Align = alBottom
        TabOrder = 2
      end
    end
    inherited TabSheet5: TTabSheet
      inherited FileOpenFrame: TFileOpenFrm
        Top = 17
        Width = 838
        inherited Panel1: TPanel
          Width = 838
          inherited BitBtn1: TBitBtn
            Left = 756
          end
          inherited edtFile: TEdit
            Left = 8
            Width = 737
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 838
        Height = 17
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '   Select a S1910 or S1911 report'
        TabOrder = 1
      end
    end
  end
  inline QBParamFrame: TQBChecksExportParamFrm [3]
    Left = 0
    Top = 533
    Width = 846
    Height = 96
    Align = alBottom
    TabOrder = 3
    inherited GroupBox1: TGroupBox
      Width = 846
    end
  end
  inherited ActionList1: TActionList
    inherited RunAction: TAction
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited PrintReportAction: TAction
      Caption = 'Print Checks'
    end
  end
end
