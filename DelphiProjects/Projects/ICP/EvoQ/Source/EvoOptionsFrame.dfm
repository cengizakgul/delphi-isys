object EvoOptionsFrm: TEvoOptionsFrm
  Left = 0
  Top = 0
  Width = 380
  Height = 46
  TabOrder = 0
  object rbEEChecks: TRadioButton
    Left = 104
    Top = 16
    Width = 113
    Height = 17
    Caption = 'Employee Checks'
    Checked = True
    TabOrder = 0
    TabStop = True
  end
  object rbAgencyChecks: TRadioButton
    Left = 240
    Top = 16
    Width = 113
    Height = 17
    Caption = 'Agency Checks'
    TabOrder = 1
  end
  object rbAllChecks: TRadioButton
    Left = 8
    Top = 16
    Width = 73
    Height = 17
    Caption = 'All Checks'
    TabOrder = 2
  end
end
