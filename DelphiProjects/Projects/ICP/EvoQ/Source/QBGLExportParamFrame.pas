unit QBGLExportParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, qbconnection, StdCtrls, qbdecl, ExtCtrls;

type
  TQBGLExportParamFrm = class(TFrame)
    GroupBox1: TGroupBox;
    cbCleared: TCheckBox;
    rgUseAccountNumber: TRadioGroup;
    GroupBox2: TGroupBox;
    rbAssignClass: TRadioButton;
    rbAssignCustomer: TRadioButton;
    rbDontParse: TRadioButton;
    Label1: TLabel;
    edtDelimiter: TEdit;
    Label2: TLabel;
    cbManualChecks: TCheckBox;
    cbSplitBillingByDBDT: TCheckBox;
    cbEmployeeDetails: TCheckBox;
    rbAssignClassCustomer: TRadioButton;
  private
    function GetData: TQBGLExportParam;
    procedure SetData(const Value: TQBGLExportParam);
  public
    property Data: TQBGLExportParam read GetData write SetData;
  end;

implementation

{$R *.dfm}

{ TQBGLExportParamFrm }

function TQBGLExportParamFrm.GetData: TQBGLExportParam;
begin
  Result.Clear := cbCleared.Checked;
  Result.UseAccountNumbers := rgUseAccountNumber.ItemIndex = 1;
  if rbAssignClass.Checked then
    Result.GLTagParsing := parseClass
  else if rbAssignCustomer.Checked then
    Result.GLTagParsing := parseName
  else if rbAssignClassCustomer.Checked then
    Result.GLTagParsing := parseClassName
  else
    Result.GLTagParsing := parseNone;
  Result.DelimiterInGLTag := edtDelimiter.Text;
  Result.ExcludeManualChecks := not cbManualChecks.Checked;
  Result.SplitBillingByDBDT := cbSplitBillingByDBDT.Checked;
  Result.EmployeeDetails := cbEmployeeDetails.Checked;
end;

procedure TQBGLExportParamFrm.SetData(const Value: TQBGLExportParam);
begin
  cbCleared.Checked := Value.Clear;
  rgUseAccountNumber.ItemIndex := ord(Value.UseAccountNumbers);
  rbAssignClass.Checked := Value.GLTagParsing = parseClass;
  rbAssignCustomer.Checked := Value.GLTagParsing = parseName;
  rbAssignClassCustomer.Checked := Value.GLTagParsing = parseClassName;
  rbDontParse.Checked := Value.GLTagParsing = parseNone;
  edtDelimiter.Text := Value.DelimiterInGLTag;
  cbManualChecks.Checked := not Value.ExcludeManualChecks;
  cbSplitBillingByDBDT.Checked := Value.SplitBillingByDBDT;
  cbEmployeeDetails.Checked := Value.EmployeeDetails;
end;

end.
