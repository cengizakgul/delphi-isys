unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientMainForm, ComCtrls, gdyCommonLoggerView,
  EvoAPIConnectionParamFrame, QBGLExportFrame, QBChecksExportFrame,
  QBExportBaseFrame, gdySendMailLoggerView, OptionsBaseFrame;

type
  TMainFm = class(TEvoAPIClientMainFm)
    QBGLExportFrame: TQBGLExportFrm;
    TabSheet4: TTabSheet;
    QBChecksExportFrame: TQBChecksExportFrm;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  common, qbdecl, issettings;

{ TMainFm }


function LoadEvoPayrollFilter(conf: IisSettings; root: string): TEvoPayrollFilter;
begin
  root := root + '\';
  Result.FromDate := StrToDate(conf.AsString[root+'DateFrom']);
  Result.ToDate := StrToDate(conf.AsString[root+'DateTo']);
end;

procedure SaveEvoPayrollFilter( const filter: TEvoPayrollFilter; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsString[root+'DateFrom'] := DateToStr(filter.FromDate);
  conf.AsString[root+'DateTo'] := DateToStr(filter.ToDate);
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
  QBGLExportFrame.Init(Logger, EvoFrame, Self as ILogRecorder);
  QBChecksExportFrame.Init(Logger, EvoFrame, Self as ILogRecorder);

  try
    QBGLExportFrame.QBParamFrame.Data := LoadQBGLExportOptions( FSettings, 'QBGLExportOptions' );
  except
    Logger.StopException;
  end;
  try
    QBGLExportFrame.EvoPayrollFilterFrame.Filter := LoadEvoPayrollFilter( FSettings, 'EvoGLExportPayrollFilter' );
  except
    Logger.StopException;
  end;

  try
    QBChecksExportFrame.QBParamFrame.Data := LoadQBChecksExportOptions( FSettings, 'QBChecksExportOptions' );
  except
    Logger.StopException;
  end;
  try
    QBChecksExportFrame.EvoOptionsFrame.Options := LoadEvoChecksExportOptions( FSettings, 'EvoChecksExportOptions' );
  except
    Logger.StopException;
  end;
  try
    QBChecksExportFrame.EvoPayrollFilterFrame.Filter := LoadEvoPayrollFilter( FSettings, 'EvoChecksExportPayrollFilter' );
  except
    Logger.StopException;
  end;
end;

destructor TMainFm.Destroy;
begin
  try
    SaveQBGLExportOptions( QBGLExportFrame.QBParamFrame.Data, FSettings, 'QBGLExportOptions' );
  except
    Logger.StopException;
  end;
  try
    SaveEvoPayrollFilter( QBGLExportFrame.EvoPayrollFilterFrame.Filter, FSettings, 'EvoGLExportPayrollFilter' );
  except
    Logger.StopException;
  end;
  
  try
    SaveQBChecksExportOptions( QBChecksExportFrame.QBParamFrame.Data, FSettings, 'QBChecksExportOptions' );
  except
    Logger.StopException;
  end;
  try
    SaveEvoChecksExportOptions( QBChecksExportFrame.EvoOptionsFrame.Options, FSettings, 'EvoChecksExportOptions' );
  except
    Logger.StopException;
  end;
  try
    SaveEvoPayrollFilter( QBChecksExportFrame.EvoPayrollFilterFrame.Filter, FSettings, 'EvoChecksExportPayrollFilter' );
  except
    Logger.StopException;
  end;
  inherited;
end;

end.
