unit QBConnection;

interface

uses
  EvoGLParser, OleServer, QBFC13Lib_TLB, gdycommonlogger, classes,
  qbdecl, evochecksparser, common;

type
  TExportChecksStat = record
    ChecksAdded: integer;
    ChecksAlreadyExisted: integer;

    ChecksVoided: integer;
    ChecksAlreadyVoided: integer;

    DepositsAdded: integer;
    DepositsAlreadyExisted: integer;

    Errors: integer;
  end;

  TQBConnection = class
  public
    constructor Create(logger: ICommonLogger);
    destructor Destroy; override;

    function ValidateGL(recs: TGLRecordArray; param: TQBGLExportParam): boolean;
    function CheckIfAlreadyExported(recs: TGLRecordArray): boolean;
    procedure ExportGL(recs: TGLRecordArray; const param: TQBGLExportParam);

    function ExportChecks(recs: TCheckRecords; param: TQBChecksExportParam): TExportChecksStat;
    procedure GetAccountFullNameList(list: TStrings);
  private
    FQB: TQBSessionManager;
    FLogger: ICommonLogger;
    FMajor, FMinor: integer;

    procedure TestVersion;
    procedure LogResponse(reslist: IMsgSetResponse);
    function CheckAccounts(recs: TGLRecordArray; param: TQBGLExportParam): boolean;
    function CheckClasses(recs: TGLRecordArray): boolean;
    function CheckCustomers(recs: TGLRecordArray): boolean;

    function CreateMsgSetRequest: IMsgSetRequest;
    function DoRequests(rq: IMsgSetRequest): IMsgSetResponse;
    procedure ClearTransaction(TxnID: widestring);
    function GetAccountList: IAccountRetList;
    procedure GetAccountNumberList(list: TStrings);
    function GetClassList: IClassRetList;
    function GetCustomerList: ICustomerRetList;
    procedure GetClassFullNameList(list: TStrings);

    function GetEmployeeList: IEmployeeRetList;
//    function GetTransactionList(flt: TDateRange): ITransactionRetList;
    function GetDepositList(flt: TDateRange): IDepositRetList;
    function GetCheckList(flt: TDateRange): ICheckRetList;
    function GetJournalEntryList(flt: TDateRange): IJournalEntryRetList;

    function FindCheck(checkNumber: string): ICheckRetList;
    procedure VoidCheck(check: ICheckRet);
    procedure GetCustomerFullNameList(list: TStrings);
  end;

function ExportChecksStatToString(stat: TExportChecksStat): string;

implementation

uses
  sysutils, windows, dateutils, strutils, gdycommon, varutils, activeX,
  math, forms, gdystrset, comobj;


{ TQBSDKGenerator }

constructor TQBConnection.Create(logger: ICommonLogger);
begin
  FLogger := logger;
  FLogger.LogEntry( 'Connecting to QB' );
  try
    try
      FQB := TQBSessionManager.Create(nil);
      try
        try
          FQB.OpenConnection2('', Application.Title, ctLocalQBD);
        except
          on E: EOleSysError do
          begin
            FLogger.StopException;
            raise Exception.Create('Cannot access QuickBooks. Most likely the QuickBooks QBFC library is not installed. Please download qbsdk130.exe from http://support.isystemsllc.com/icp' );
          end
          else
            raise;
        end;
        try
          FQB.BeginSession('', omDontCare); //!! ??
        except
          FQB.CloseConnection;
          raise;
        end;
      except
        FreeAndNil(FQB);
        raise;
      end;
      TestVersion;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

destructor TQBConnection.Destroy;
begin
  if assigned(FQB) then
  begin
    FQB.EndSession;
    FQB.CloseConnection;
    FreeAndNil(FQB);
  end;
  inherited;
end;

procedure TQBConnection.TestVersion;
var
  psa: PSafeArray;
  ptr: Pointer;
  lbound, ubound: integer;
  i: integer;
  s: string;
  v: string;
  p: integer;
  ver: integer;
begin
  psa := FQB.QBXMLVersionsForSession;
  SafeArrayAccessData( psa, ptr );
  try
    SafeArrayGetLBound(psa, 1, lbound);
    SafeArrayGetUBound(psa, 1, ubound);
    s := '';
    ver := 0;
    for i := lbound to ubound do
    begin
      v := WideString(POleStrList(ptr)^[i]);
      p := Pos('.', v);
      Assert(p > 0);
      ver := max(ver, strtoint( copy(v, 1, p-1) )*1000 + strtoint( copy(v, p+1, length(v)-p) ));

      if s <> '' then
        s := s + ', ';
      s := s + v;
    end;
    FMajor := ver div 1000;
    FMinor := ver mod 1000;

    FLogger.LogDebug('The following qbXML versions are supported by the user''s version of QuickBooks: ' + s);
  finally
    SafeArrayUnaccessData(psa);
  end;
end;

const
  glmemo: string = 'Payroll from Evolution';

function TQBConnection.CheckIfAlreadyExported(recs: TGLRecordArray): boolean;
var
  list: IJournalEntryRetList;
  dr: TDateRange;
begin
  dr.FromDate := recs[0].Date; //they are all the same
  dr.ToDate := recs[0].Date;
  list := GetJournalEntryList(dr);
  Result := (list <> nil) and (list.Count > 0);
end;

procedure TQBConnection.ExportGL(recs: TGLRecordArray; const param: TQBGLExportParam);
var
  accounts: IAccountRetList;

  procedure SetAccountRef(ref: IQBBaseRef; acc: string);
  var
    i: integer;
  begin
    if param.UseAccountNumbers then
    begin
      Assert(accounts <> nil);
      acc := AnsiLowerCase(acc);
      for i := 0 to accounts.Count-1 do
        if (accounts.GetAt(i).AccountNumber <> nil) and (AnsiLowerCase(accounts.GetAt(i).AccountNumber.GetValue) = acc) then
        begin
          ref.ListID.SetValue( accounts.GetAt(i).ListID.GetValue );
          exit;
        end;
      Assert(false); //Validate checks account names
    end
    else
      ref.FullName.SetValue(acc);
  end;
var
  rq: IMsgSetRequest;
  jea: IJournalEntryAdd;
  debit: IJournalDebitLine;
  credit: IJournalCreditLine;
  i: integer;
  reslist: IMsgSetResponse;
  note: string;
begin
  Assert( length(recs) > 0 );

  FLogger.LogEntry( 'Exporting to QB' );
  try
    try
      if param.UseAccountNumbers then
        accounts := GetAccountList;

      rq := CreateMsgSetRequest;
      jea := rq.AppendJournalEntryAddRq;
      jea.defMacro.SetValue('TxnId:0');
      jea.TxnDate.SetValue( recs[0].Date );
      jea.Memo.SetValue(glmemo);

      for i := 0 to high(recs) do
      begin
        note := '';
        if recs[i].LastName <> '' then
          note := recs[i].LastName + ', ' + recs[i].FirstName + ' ';
        if recs[i].PayPeriodEndDate <> 0 then
          note := note + 'PE ' + DateToStr(recs[i].PayPeriodEndDate);
        if note = '' then
          note := glmemo;

        if recs[i].Debit <> 0 then
        begin
          debit := jea.ORJournalLineList.Append.JournalDebitLine;
          debit.Amount.SetValue( recs[i].Debit );

          SetAccountRef(debit.AccountRef, recs[i].Account);
          debit.Memo.SetValue(note);
          if recs[i].TrClass <> '' then
            case param.GLTagParsing of
              parseClass: debit.ClassRef.FullName.SetValue(recs[i].TrClass);
              parseName: debit.EntityRef.FullName.SetValue(recs[i].TrName);
              parseClassName: begin
                debit.ClassRef.FullName.SetValue(recs[i].TrClass);
                debit.EntityRef.FullName.SetValue(recs[i].TrName);
              end;
              parseNone: ;
            else
              Assert(false);
            end;
        end;
        if recs[i].Credit <> 0 then
        begin
          credit := jea.ORJournalLineList.Append.JournalCreditLine;
          credit.Amount.SetValue( recs[i].Credit );
          SetAccountRef(credit.AccountRef, recs[i].Account);
          credit.Memo.SetValue(note);
          if recs[i].TrClass <> '' then
            case param.GLTagParsing of
              parseClass: credit.ClassRef.FullName.SetValue(recs[i].TrClass);
              parseName: credit.EntityRef.FullName.SetValue(recs[i].TrName);
              parseClassName: begin
                credit.ClassRef.FullName.SetValue(recs[i].TrClass);
                credit.EntityRef.FullName.SetValue(recs[i].TrName);
              end;
              parseNone: ;
            else
              Assert(false);
            end;
        end;
      end;

    (*
      //got strange error  -  [3150] There is a missing element: "TxnID".
      if FParam.Clear then
      begin
        csa := rq.AppendClearedStatusModRq;
        csa.TxnID.SetValueUseMacro('TxnId:0');
        csa.ClearedStatus.SetValue(csCleared);
      end;
      *)

      reslist := DoRequests(rq);
      if param.Clear then
      begin
        Assert( assigned(reslist.ResponseList.GetAt(0).Detail) );
        ClearTransaction( (reslist.ResponseList.GetAt(0).Detail as IJournalEntryRet).TxnID.GetValue );
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TQBConnection.LogResponse(reslist: IMsgSetResponse);
var
  i: integer;
  res: IResponse;
  msg: string;
begin
  FLogger.LogDebug('qbXML response received', reslist.ToXMLString);
  for i := 0 to reslist.ResponseList.Count-1 do
  begin
    res := reslist.ResponseList.GetAt(i);
    msg := Format('QB: [%d] %s', [res.StatusCode, res.StatusMessage]);
    if AnsiUpperCase(res.StatusSeverity) = 'INFO' then
    begin
      if res.StatusCode <> 0 then
{$ifdef PRODUCTION_LICENCE}
        FLogger.LogDebug(msg);
{$else}
        FLogger.LogEvent(msg);
{$endif}
    end
    else if AnsiUpperCase(res.StatusSeverity) = 'WARN' then
      FLogger.LogWarning(msg)
    else if AnsiUpperCase(res.StatusSeverity) = 'ERROR' then
    begin
      raise Exception.Create(msg);
    end
    else
      FLogger.LogWarningFmt('QB %s: [%d] %s', [res.StatusSeverity, res.StatusCode, res.StatusMessage]);
  end;
end;

function TQBConnection.ValidateGL(recs: TGLRecordArray; param: TQBGLExportParam): boolean;
var
  i: integer;
  aCompanyCode: string;
  aDate: TDateTime;
begin
  Result := false; //to make compiler happy
  FLogger.LogEntry( 'Validating Evolution Journal' );
  try
    try
      Assert( length(recs) > 0 );
      Result := true;

      aCompanyCode := recs[0].CompanyCode;
      aDate := recs[0].Date;
      for i := 1 to high(recs) do
      begin
        if recs[i].CompanyCode <> aCompanyCode then
        begin
          FLogger.LogErrorFmt('Company code must be the same for all GL records. Found <%s> and <%s>', [aCompanyCode, recs[i].CompanyCode]);
          Result := false;
          break;
        end;
      end;
      for i := 1 to high(recs) do
      begin
        if recs[i].Date <> aDate then
        begin
          FLogger.LogErrorFmt('Date must be the same for all GL records. Found <%s> and <%s>', [DateToStr(aDate), DateToStr(recs[i].Date)]);
          Result := false;
          break;
        end;
      end;
      Result := Result and CheckAccounts(recs, param);
      case param.GLTagParsing of
        parseClass: Result := Result and CheckClasses(recs);
        parseName: Result := Result and CheckCustomers(recs);
        parseClassName: Result := Result and CheckClasses(recs) and CheckCustomers(recs);
        parseNone: ;
      else
        Assert(false);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TQBConnection.GetAccountFullNameList(list: TStrings);
var
  acc: IAccountRetList;
  i: integer;
begin
  list.Clear;
  acc := GetAccountList;
  if acc <> nil then
    for i := 0 to acc.Count-1 do
      list.Add( acc.GetAt(i).FullName.GetValue );
end;

procedure TQBConnection.GetClassFullNameList(list: TStrings);
var
  classes: IClassRetList;
  i: integer;
begin
  list.Clear;
  classes := GetClassList;
  if classes <> nil then
    for i := 0 to classes.Count-1 do
      if classes.GetAt(i).FullName <> nil then //I think it can't be nil but just in case...
        list.Add( classes.GetAt(i).FullName.GetValue );
end;

procedure TQBConnection.GetCustomerFullNameList(list: TStrings);
var
  customers: ICustomerRetList;
  i: integer;
begin
  list.Clear;
  customers := GetCustomerList;
  if customers <> nil then
    for i := 0 to customers.Count-1 do
      if customers.GetAt(i).FullName <> nil then //I think it can't be nil but just in case...
        list.Add( customers.GetAt(i).FullName.GetValue );
end;

procedure TQBConnection.GetAccountNumberList(list: TStrings);
var
  acc: IAccountRetList;
  i: integer;
begin
  list.Clear;
  acc := GetAccountList;
  if acc <> nil then
    for i := 0 to acc.Count-1 do
      if acc.GetAt(i).AccountNumber <> nil then
        list.Add( acc.GetAt(i).AccountNumber.GetValue );
end;

function TQBConnection.CheckAccounts(recs: TGLRecordArray; param: TQBGLExportParam): boolean;
var
  accounts: TStringList;
  i: integer;
  missing: TStringList;
begin
  accounts := TStringList.Create;
  try
    if param.UseAccountNumbers then
      GetAccountNumberList(accounts)
    else
      GetAccountFullNameList(accounts);

    for i := 0 to accounts.Count-1 do
      accounts[i] := AnsiLowerCase(accounts[i]);

    missing := TStringList.Create;
    try
      missing.Duplicates := dupIgnore;
      missing.Sorted := true;
      for i := 0 to high(recs) do
        if accounts.IndexOf(AnsiLowerCase(recs[i].Account)) = -1 then
          missing.Add(recs[i].Account);

      if missing.Count = 1 then
        FLogger.LogErrorFmt('Account with %s <%s> doesn''t exist in QB',[IIF(param.UseAccountNumbers, 'number',  'full name'), missing[0]])
      else if missing.Count > 1 then
        FLogger.LogErrorFmt('%d accounts don''t exist in QB. See details for missing accounts list.', [missing.Count], missing.Text);
      Result := missing.Count = 0;
    finally
      FreeAndNil(missing);
    end;
  finally
    FreeAndNil(accounts);
  end;
end;

function TQBConnection.CheckCustomers(recs: TGLRecordArray): boolean;
var
  customers: TStringList;
  i: integer;
  missing: TStringList;
  cust: string;
  p: integer;
begin
  customers := TStringList.Create;
  try
    GetCustomerFullNameList(customers);

    for i := 0 to customers.Count-1 do
      customers[i] := AnsiLowerCase(customers[i]);

    missing := TStringList.Create;
    try
      missing.Duplicates := dupIgnore;
      missing.Sorted := true;
      for i := 0 to high(recs) do
        if recs[i].TrName <> '' then
        begin
          cust := recs[i].TrName;
          while cust <> '' do
          begin
            if customers.IndexOf(AnsiLowerCase(cust)) <> -1 then
              break;
            p := LastDelimiter(':', cust);
            cust := Copy(cust, 1, p-1);
          end;
          if cust = '' then
            missing.Add(recs[i].TrName);
        end;
      if missing.Count = 1 then
        FLogger.LogErrorFmt('Customer or job with full name <%s> doesn''t exist in QB',[missing[0]])
      else if missing.Count > 1 then
        FLogger.LogErrorFmt('%d customers or jobs don''t exist in QB. See details for missing customer list.', [missing.Count], missing.Text);
      Result := missing.Count = 0;
    finally
      FreeAndNil(missing);
    end;
  finally
    FreeAndNil(customers);
  end;
end;

function TQBConnection.CheckClasses(recs: TGLRecordArray): boolean;
var
  classes: TStringList;
  i: integer;
  missing: TStringList;
begin
  classes := TStringList.Create;
  try
    GetClassFullNameList(classes);

    for i := 0 to classes.Count-1 do
      classes[i] := AnsiLowerCase(classes[i]);

    missing := TStringList.Create;
    try
      missing.Duplicates := dupIgnore;
      missing.Sorted := true;
      for i := 0 to high(recs) do
        if recs[i].TrClass <> '' then
          if classes.IndexOf( AnsiLowerCase(recs[i].TrClass) ) = -1 then
            missing.Add(recs[i].TrClass);

      if missing.Count = 1 then
        FLogger.LogErrorFmt('Class with full name <%s> doesn''t exist in QB',[missing[0]])
      else if missing.Count > 1 then
        FLogger.LogErrorFmt('%d classes don''t exist in QB. See details for missing classes list.', [missing.Count], missing.Text);
      Result := missing.Count = 0;
    finally
      FreeAndNil(missing);
    end;
  finally
    FreeAndNil(classes);
  end;
end;


function BuildClearExportChecksStat: TExportChecksStat;
begin
  Result.ChecksAdded := 0;
  Result.ChecksVoided := 0;
  Result.ChecksAlreadyExisted := 0;
  Result.ChecksAlreadyVoided := 0;
  Result.DepositsAdded := 0;
  Result.DepositsAlreadyExisted := 0;
  Result.Errors := 0;
end;

function ExportChecksStatToString(stat: TExportChecksStat): string;
  procedure Add(const s: string; const Args: array of const);
  begin
    if Result <> '' then
      Result := Result + ', ';
    Result := Result + Format(s, args);
  end;

  procedure Log(always: boolean; n: integer; act: string; obj: string);
  begin
    if always or (n > 0) then
      Add('%d %s %s', [n, Plural(obj, n), act]);
  end;
begin
  Result := '';
  Log(false, stat.Errors, 'occured', 'error');
  Log(true, stat.ChecksAdded, 'added', 'check');
  Log(true, stat.ChecksVoided, 'voided', 'check');
  Log(true, stat.DepositsAdded, 'added', 'deposit');
  Log(true, stat.ChecksAlreadyExisted, 'already existed', 'check');
  Log(true, stat.ChecksAlreadyVoided, IIF(stat.ChecksAlreadyVoided=1, 'was', 'were') + ' already voided', 'check'); 
  Log(true, stat.DepositsAlreadyExisted, 'already existed', 'deposit');
  if stat.Errors = 0 then
    Result := 'successfully completed: ' + Result
  else
    if stat.ChecksAdded + stat.ChecksVoided + stat.DepositsAdded +
       stat.ChecksAlreadyExisted + stat.ChecksAlreadyVoided + stat.DepositsAlreadyExisted > 0 then
      Result := 'partially completed: ' + Result
    else
      Result := 'failed: ' + Result;
end;

type
  TQBEEName = record
    FirstName: string;
    MiddleName: string;
    LastName: string;
  end;

function MakeQBEEName(ee: IEmployeeRet): TQBEEName;
begin
  Result.FirstName := '';
  Result.MiddleName := '';
  Result.LastName := '';
  if ee = nil then
    Exit;

  if ee.FirstName <> nil then
    Result.FirstName := trim(ee.FirstName.GetValue);
  if ee.MiddleName <> nil then
    Result.MiddleName := trim(ee.MiddleName.GetValue);
  if ee.LastName <> nil then
    Result.LastName := trim(ee.LastName.GetValue);

  if (Length(Result.MiddleName) > 1) and (Result.MiddleName[Length(Result.MiddleName)] = '.') then
    Result.MiddleName := copy(Result.MiddleName, 1, length(Result.MiddleName)-1);
end;

function SameEE(const nm: TQBEEName; const rec: TCheckRecord): boolean;
begin
  Assert(rec.IsEECheck);
  Result := (nm.FirstName = rec.FirstName) and (nm.MiddleName = rec.MiddleInitial) and (nm.LastName = rec.LastName);
end;

function GetEE(ees: IEmployeeRetList; const rec: TCheckRecord): IEmployeeRet;
var
  i: integer;
begin
  Assert(rec.IsEECheck);
  Result := nil;
  if ees <> nil then
    for i := 0 to ees.Count-1 do
      if SameEE( MakeQBEEName(ees.GetAt(i)), rec) then
      begin
        Result := ees.GetAt(i);
        exit;
      end;
  raise Exception.CreateFmt('Cannot find QB employee record with First Name = ''%s'', Middle Initial = ''%s'', Last Name = ''%s''', [rec.FirstName, rec.MiddleInitial, rec.LastName] );
end;

function FindEEByRef(ees: IEmployeeRetList; ref: IQBBaseRef): IEmployeeRet;
var
  i: integer;
begin
  Result := nil;
  if ees <> nil then
    for i := 0 to ees.Count-1 do
    begin
      if ees.GetAt(i).ListID.GetValue = ref.ListID.GetValue then
      begin
        Result := ees.GetAt(i);
        exit;
      end;
    end;
end;

function TQBConnection.ExportChecks(recs: TCheckRecords; param: TQBChecksExportParam): TExportChecksStat;
var
  ees: IEmployeeRetList;
  deps: IDepositRetList;
  checks: ICheckRetList;
  stat: TExportChecksStat;
var
  changedFields: array of string;

  function diff(s1: IQBStringType; s2: string): boolean; overload;
  var
    s: string;
  begin
    if s1 <> nil then
      s := trim(s1.GetValue)
    else
      s := '';
    Result := s <> trim(s2);
  end;

  function diffPayeeOrEE(entity: IQBBaseRef; const rec: TCheckRecord): boolean;
  var
    ee: IEmployeeRet;
  begin
    if rec.IsEECheck then
    begin
      if entity <> nil then
      begin
        ee := FindEEByRef(ees, entity);
        if ee <> nil then
          Result := not SameEE(MakeQBEEName(ee), rec)
        else
          Result := true;  //not an employee
      end
      else
        Result := not SameEE(MakeQBEEName(nil), rec);
    end
    else
    begin
      if entity <> nil then
        Result := diff(entity.FullName, rec.Payee)
      else
        Result := diff(nil, rec.Payee)
    end
  end;

  function diff(amount: IQBAmountType; evoAmount: currency): boolean; overload;
  begin
    Result := (amount = nil) or (abs(amount.GetValue-evoAmount)>1e-10);
  end;
  function diff(date: IQBDateType; evoDate: TDateTime): boolean; overload;
  begin
    Result := (date = nil) or (date.GetValue <> evoDate);
  end;
  procedure LogDiff(d: boolean; fn: string);
  begin
    if d then
    begin
      SetLength(changedFields, Length(changedFields)+1);
      changedFields[high(changedFields)] := fn;
    end
  end;

  function GetPayee(const rec: TCheckRecord): widestring;
  begin
    if rec.IsEECheck then
      Result := GetEE(ees, rec).Name.GetValue
    else
      Result := rec.Payee;
  end;

  function DepositExists(const rec: TCheckRecord): boolean;
  var
    i: integer;
    dep: IDepositRet;
    lines: IDepositLineRetList;
    j: integer;
    line: IDepositLineRet;
    num: string;
  begin
    Result := false;
    if deps = nil then
      Exit;
    num := trim(rec.CheckNumber);
    for i := 0 to deps.Count-1 do
    begin
      dep := deps.GetAt(i);

      if (dep.DepositToAccountRef = nil) or (trim(dep.DepositToAccountRef.FullName.GetValue) <> trim(param.CashAccount)) then
        continue;

      lines := dep.DepositLineRetList;
      if lines <> nil then
      begin
        for j := 0 to lines.Count-1 do
        begin
          line := lines.GetAt(j);
          if (line.CheckNumber <> nil) and (trim(line.CheckNumber.GetValue) = num) then
          begin
            Result := true;
            break;
          end
        end;
      end;
      if Result then
        break;
    end;
    if Result then
    begin
      changedFields := nil;
      LogDiff( lines.Count <> 1, 'Line Count' );

      LogDiff( diff(dep.TxnDate, rec.Date), 'Date' );
      LogDiff( diff(dep.Memo, rec.Memo), 'Memo' );

      LogDiff( diff(line.AccountRef.FullName, param.LiabilityAccount), 'Deposit From Account' );
      LogDiff( diff(line.Amount, -rec.Amount), 'Amount' );
      LogDiff( diff(line.Memo, rec.Memo), 'Line''s Memo' );

      if param.IncludeEENames then //most likely user doesn't care about ee names at all if she doesn't want to export them
        LogDiff( diffPayeeOrEE(line.EntityRef, rec), 'Received From' );
      if Length(changedFields) > 0 then
        FLogger.LogWarningFmt('Deposit to account %s with a line with Check Number %s already exists and some fields are different (%s)', [param.CashAccount, num, SetToStr(changedFields, ', ')] );
    end;
  end;

  procedure AddDeposit( const rec: TCheckRecord);
  var
    rq: IMsgSetRequest;
    deposit: IDepositAdd;
    line: IDepositInfo;
    reslist: IMsgSetResponse;
  begin
    Assert(rec.Amount <= 0);
    rq := CreateMsgSetRequest;
    deposit := rq.AppendDepositAddRq;
    deposit.TxnDate.SetValue( rec.Date );
    deposit.DepositToAccountRef.FullName.SetValue( param.CashAccount );
    deposit.Memo.SetValue(rec.Memo);
    line := deposit.DepositLineAddList.Append.ORDepositLineAdd.DepositInfo;
    line.AccountRef.FullName.SetValue(param.LiabilityAccount);
    line.Amount.SetValue(-rec.Amount);
    line.Memo.SetValue(rec.Memo);
    line.CheckNumber.SetValue(rec.CheckNumber);
    if param.IncludeEENames then
      line.EntityRef.FullName.SetValue( GetPayee(rec) );
    reslist := DoRequests(rq);
    inc(stat.DepositsAdded);
    if param.Clear then
    begin
      Assert( assigned(reslist.ResponseList.GetAt(0).Detail) );
      ClearTransaction( (reslist.ResponseList.GetAt(0).Detail as IDepositRet).TxnID.GetValue );
    end;
  end;

  function CheckExists(const rec: TCheckRecord): boolean;
  var
    i: integer;
    chk: ICheckRet;
    refnum: string;
    line: IExpenseLineRet;
    lineCount: integer;
  begin
    Result := false;
    if checks = nil then
      Exit;
    refnum := trim(rec.CheckNumber);
    for i := 0 to checks.Count-1 do
    begin
      chk := checks.GetAt(i);
      if (chk.RefNumber <> nil) and (trim(chk.RefNumber.GetValue) = refnum) and
         (chk.AccountRef <> nil) and (trim(chk.AccountRef.FullName.GetValue) = trim(param.CashAccount)) then
      begin
        Result := true;
        break;
      end;
    end;
    if Result then
    begin
      LogDiff( diff(chk.Amount, rec.Amount), 'Amount' );
      if param.IncludeEENames then //most likely user doesn't care about ee names at all if she doesn't want to export them
        LogDiff( diffPayeeOrEE(chk.PayeeEntityRef, rec), 'Payee' );
      LogDiff( diff(chk.TxnDate, rec.Date), 'Date' );
      LogDiff( diff(chk.Memo, rec.Memo), 'Memo' );

      if chk.ExpenseLineRetList <> nil then
        lineCount := chk.ExpenseLineRetList.Count
      else
        lineCount := 0;
      LogDiff( lineCount <> 1, 'Expense Line Count' );
      if lineCount = 1 then
      begin
        line := chk.ExpenseLineRetList.getAt(0);
        LogDiff( diff(line.AccountRef.FullName, param.LiabilityAccount), 'Expense Account' );
        LogDiff( diff(line.Memo, rec.Memo), 'Expense Memo' );
      end;

      if Length(changedFields) > 0 then
        FLogger.LogWarningFmt('Check with Bank Acoount %s and Number %s already exists and some fields are different (%s)', [param.CashAccount, refnum, SetToStr(changedFields, ', ')] );
    end;
  end;

  procedure AddCheck(const rec: TCheckRecord);
  var
    rq: IMsgSetRequest;
    check: ICheckAdd;
    line: IExpenseLineAdd;
    reslist: IMsgSetResponse;
  begin
    rq := CreateMsgSetRequest;
    check := rq.AppendCheckAddRq;
    check.TxnDate.SetValue( rec.Date );
    check.AccountRef.FullName.SetValue( param.CashAccount );
    check.RefNumber.SetValue(rec.CheckNumber);
    check.Memo.SetValue(rec.Memo);
    check.IsToBePrinted.SetValue(false);
    if param.IncludeEENames then
      check.PayeeEntityRef.FullName.SetValue( GetPayee(rec) );
    line := check.ExpenseLineAddList.Append;
    line.AccountRef.FullName.SetValue( param.LiabilityAccount );
    line.Amount.SetValue(rec.Amount);
    line.Memo.SetValue(rec.Memo);
    reslist := DoRequests(rq);
    inc(stat.ChecksAdded);
    if param.Clear then
    begin
      Assert( assigned(reslist.ResponseList.GetAt(0).Detail) );
      ClearTransaction( (reslist.ResponseList.GetAt(0).Detail as ICheckRet).TxnID.GetValue );
    end;
  end;

  function VoidIfFound(const rec: TCheckRecord): boolean;
  var
    checks: ICheckRetList;
    check: ICheckRet;
    i: integer;
    payee: string;
  begin
    Assert(rec.Amount < 0);
    Result := false;
    if param.IncludeEENames then
      payee := GetPayee(rec)
    else 
      payee := '';
    checks := FindCheck(rec.CheckNumber);
    if checks = nil then
      exit;

    for i := 0 to checks.Count-1 do
    begin
      check := checks.GetAt(i);
      if check.Amount = nil then
        continue;
      if (check.AccountRef = nil) or (trim(check.AccountRef.FullName.GetValue) <> trim(param.CashAccount)) then
        continue;
      if param.IncludeEENames then
      begin
        if (check.PayeeEntityRef = nil) or (trim(check.PayeeEntityRef.FullName.GetValue) <> trim(payee)) then
          continue;
      end
      else
      begin
        if check.PayeeEntityRef <> nil then
          continue;
      end;
      if abs(check.Amount.GetValue - -rec.Amount) < 1e-10 then
      begin
        VoidCheck(check);
        Result := true;
        inc(stat.ChecksVoided);
        break;
      end
      else if check.Amount.GetValue = 0 then
      begin
        Result := true;
        inc(stat.ChecksAlreadyVoided);
        break;
      end
      else
        continue;
    end;
  end;

  function AllowedToCreate(const rec: TCheckRecord): boolean;
  begin
    // Direct Deposit Check Numbers are 8 digits long or negative
    Result := (Length(rec.CheckNumber) <> 8) and (pos('-', rec.CheckNumber) = 0) or param.IncludeDD;
  end;
var
 i: integer;
 dr: TDateRange;
begin
  stat := BuildClearExportChecksStat;

  FLogger.LogEntry( 'Exporting Checks/Deposits to QB' );
  try
    try
      ees := GetEmployeeList;
      //!!filtered by date! if some one has changed the date of already exported check/deposit
      //and the date is now out of renge then this check/deposit will be exported again!
      dr := GetCheckRecordsDateRange(recs);
      deps := GetDepositList( dr );
      checks := GetCheckList( dr );

      for i := 0 to high(recs) do
      begin
        FLogger.LogEntry( 'Exporting Check/Deposit to QB' );
        try
          try
            if recs[i].IsEECheck then
            begin
              FLogger.LogContextItem( 'First Name', recs[i].FirstName );
              FLogger.LogContextItem( 'Middle Initial', recs[i].MiddleInitial );
              FLogger.LogContextItem( 'Last Name', recs[i].LastName );
            end
            else
            begin
              FLogger.LogContextItem( 'Payee', recs[i].Payee );
            end;
            FLogger.LogContextItem( 'Memo', recs[i].Memo );
            FLogger.LogContextItem( 'CheckNumber', recs[i].CheckNumber );
            FLogger.LogContextItem( 'Date', DateToStr(recs[i].Date) );
            FLogger.LogContextItem( 'Amount', CurrToStr(recs[i].Amount) );

            if recs[i].Amount < 0 then //can't create check with negative amount
            begin
              if not VoidIfFound(recs[i]) then
              begin
                if AllowedToCreate(recs[i]) then
                begin
                  if not DepositExists(recs[i]) then
                    AddDeposit(recs[i])
                  else
                    inc(stat.DepositsAlreadyExisted);
                end
              end;
            end
            else
            begin
              if AllowedToCreate(recs[i]) then
              begin
                if not CheckExists(recs[i]) then
                  AddCheck(recs[i])
                else
                  inc(stat.ChecksAlreadyExisted);
              end
            end
          except
            inc(stat.Errors);
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
  Result := stat;
end;

function TQBConnection.CreateMsgSetRequest: IMsgSetRequest;
begin
  Result := FQB.CreateMsgSetRequest('US', FMajor, FMinor);
  Result.Attributes.OnError := roeStop; //roeRollback - got error: QuickBooks2006 does not support the "rollbackOnError" value of the "onError" attribute
end;

function TQBConnection.DoRequests(rq: IMsgSetRequest): IMsgSetResponse;
begin
  FLogger.LogDebug('qbXML request sent', rq.ToXMLString);
  Result := FQB.DoRequests( rq );
  LogResponse(Result);
end;

procedure TQBConnection.ClearTransaction(TxnID: widestring);
var
  rq: IMsgSetRequest;
  csa: IClearedStatusMod;
begin
  rq := CreateMsgSetRequest;
  csa := rq.AppendClearedStatusModRq;
  csa.TxnID.SetValue( TxnID );
  csa.ClearedStatus.SetValue(csCleared);
  DoRequests(rq);
end;

function TQBConnection.GetAccountList: IAccountRetList;
var
  rq: IMsgSetRequest;
begin
  FLogger.LogEntry( 'Getting QuickBooks Account list' );
  try
    try
      rq := CreateMsgSetRequest;
      rq.AppendAccountQueryRq;
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as IAccountRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TQBConnection.GetClassList: IClassRetList;
var
  rq: IMsgSetRequest;
begin
  FLogger.LogEntry( 'Getting QuickBooks Class list' );
  try
    try
      rq := CreateMsgSetRequest;
      rq.AppendClassQueryRq;
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as IClassRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TQBConnection.GetCustomerList: ICustomerRetList;
var
  rq: IMsgSetRequest;
  cq: ICustomerQuery;
begin
  FLogger.LogEntry( 'Getting QuickBooks Customer list' );
  try
    try
      rq := CreateMsgSetRequest;
      cq := rq.AppendCustomerQueryRq;
//      cq.ORCustomerListQuery.CustomerListFilter.FromModifiedDate.SetValue(25934 {1/1/1971}, True);
      cq.IncludeRetElementList.Add('ListID');
      cq.IncludeRetElementList.Add('FullName');
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as ICustomerRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TQBConnection.GetEmployeeList: IEmployeeRetList;
var
  rq: IMsgSetRequest;
begin
  FLogger.LogEntry( 'Getting QuickBooks Employee list' );
  try
    try
      rq := CreateMsgSetRequest;
      rq.AppendEmployeeQueryRq;
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as IEmployeeRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;
{
function TQBConnection.GetTransactionList(flt: TDateRange): ITransactionRetList;
var
  rq: IMsgSetRequest;
  q: ITransactionQuery;
begin
  FLogger.LogEntry( 'Getting QuickBooks Transaction list' );
  try
    try
      rq := CreateMsgSetRequest;
      q := rq.AppendTransactionQueryRq;
      with q.ORTransactionQuery.TransactionFilter.TransactionDateRangeFilter.ORTransactionDateRangeFilter.TxnDateRange do
      begin
        FromTxnDate.SetValue(flt.FromDate);
        ToTxnDate.SetValue(flt.ToDate);
      end;
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as ITransactionRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;
}
procedure SetRangeFilter(rangeflt: IORDateRangeFilter; range: TDateRange);
begin
  with rangeflt.TxnDateRangeFilter.ORTxnDateRangeFilter.TxnDateFilter do
  begin
    FromTxnDate.SetValue(range.FromDate);
    ToTxnDate.SetValue(range.ToDate);
  end;
end;

function TQBConnection.GetDepositList(flt: TDateRange): IDepositRetList;
var
  rq: IMsgSetRequest;
  q: IDepositQuery;
begin
  FLogger.LogEntry( 'Getting QuickBooks Deposit list' );
  try
    try
      rq := CreateMsgSetRequest;
      q := rq.AppendDepositQueryRq;
      SetRangeFilter(q.ORDepositQuery.DepositFilter.ORDateRangeFilter, flt);
      q.IncludeLineItems.SetValue(true);
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as IDepositRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TQBConnection.GetCheckList( flt: TDateRange): ICheckRetList;
var
  rq: IMsgSetRequest;
  q: ICheckQuery;
begin
  FLogger.LogEntry( 'Getting QuickBooks Check list' );
  try
    try
      rq := CreateMsgSetRequest;
      q := rq.AppendCheckQueryRq;
      SetRangeFilter(q.ORTxnQuery.TxnFilter.ORDateRangeFilter, flt);
      q.IncludeLineItems.SetValue(true);
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as ICheckRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TQBConnection.GetJournalEntryList(flt: TDateRange): IJournalEntryRetList;
var
  rq: IMsgSetRequest;
  q: IJournalEntryQuery;
begin
  FLogger.LogEntry( 'Getting QuickBooks Journal Entry list' );
  try
    try
      rq := CreateMsgSetRequest;
      q := rq.AppendJournalEntryQueryRq;
      SetRangeFilter(q.ORTxnQuery.TxnFilter.ORDateRangeFilter, flt);
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as IJournalEntryRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TQBConnection.FindCheck(checkNumber: string): ICheckRetList;
var
  rq: IMsgSetRequest;
  q: ICheckQuery;
begin
  FLogger.LogEntry( 'Querying QuickBooks Check' );
  try
    try
      rq := CreateMsgSetRequest;
      q := rq.AppendCheckQueryRq;
      q.ORTxnQuery.RefNumberList.Add(checkNumber);
      Result := DoRequests(rq).ResponseList.GetAt(0).Detail as ICheckRetList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TQBConnection.VoidCheck(check: ICheckRet);
var
  rq: IMsgSetRequest;
  voidrq: ITxnVoid;
begin
  rq := CreateMsgSetRequest;
  voidrq := rq.AppendTxnVoidRq;
  voidrq.TxnID.SetValue( check.TxnID.GetValue );
  voidrq.TxnVoidType.SetValue( tvtCheck );
  DoRequests(rq);
end;

end.


