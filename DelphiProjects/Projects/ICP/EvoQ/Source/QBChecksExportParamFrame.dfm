object QBChecksExportParamFrm: TQBChecksExportParamFrm
  Left = 0
  Top = 0
  Width = 743
  Height = 96
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 743
    Height = 96
    Align = alClient
    Caption = 'QuickBooks '
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 67
      Height = 13
      Caption = 'Cash Account'
    end
    object Label2: TLabel
      Left = 248
      Top = 24
      Width = 77
      Height = 13
      Caption = 'Liability Account'
    end
    object cbCleared: TCheckBox
      Left = 376
      Top = 72
      Width = 57
      Height = 17
      Caption = 'Clear'
      TabOrder = 0
      OnClick = cbClearedClick
    end
    object cbCashAccount: TComboBox
      Left = 8
      Top = 40
      Width = 225
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cbCashAccountChange
    end
    object cbLiabilityAccount: TComboBox
      Left = 248
      Top = 40
      Width = 225
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = cbLiabilityAccountChange
    end
    object cbIncludeDD: TCheckBox
      Left = 8
      Top = 72
      Width = 185
      Height = 17
      Caption = 'Create Direct Deposit Checks'
      TabOrder = 3
      OnClick = cbIncludeDDClick
    end
    object cbIncludeEENames: TCheckBox
      Left = 208
      Top = 72
      Width = 161
      Height = 17
      Caption = 'Include Payee Names'
      TabOrder = 4
      OnClick = cbIncludeEENamesClick
    end
    object bbGetAccountList: TBitBtn
      Left = 512
      Top = 36
      Width = 113
      Height = 25
      Caption = 'Get Account list'
      TabOrder = 5
      OnClick = bbGetAccountListClick
    end
  end
end
