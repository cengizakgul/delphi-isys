object QBGLExportParamFrm: TQBGLExportParamFrm
  Left = 0
  Top = 0
  Width = 731
  Height = 139
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 731
    Height = 139
    Align = alClient
    Caption = 'QuickBooks '
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object cbCleared: TCheckBox
      Left = 161
      Top = 113
      Width = 58
      Height = 17
      Caption = 'Clear'
      TabOrder = 1
    end
    object rgUseAccountNumber: TRadioGroup
      Left = 8
      Top = 15
      Width = 169
      Height = 92
      Caption = 'Treat Evolution GL Tags as'
      ItemIndex = 0
      Items.Strings = (
        'Account Full Names'
        'Account Numbers')
      TabOrder = 0
    end
    object GroupBox2: TGroupBox
      Left = 192
      Top = 15
      Width = 529
      Height = 92
      Caption = 'Evolution GL Tag Parsing'
      TabOrder = 2
      object Label1: TLabel
        Left = 159
        Top = 16
        Width = 66
        Height = 13
        Hint = 
          'Characters to the left of the delimiter will be assigned to the ' +
          'QuickBooks CLASS field.'
        Caption = 'Delimiter used'
      end
      object Label2: TLabel
        Left = 264
        Top = 13
        Width = 238
        Height = 26
        Caption = 
          'Part of GL tag before the delimiter will be assigned to the Quic' +
          'kBooks Class or Name field.'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        WordWrap = True
      end
      object rbAssignClass: TRadioButton
        Left = 16
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Assign Class'
        TabOrder = 0
      end
      object rbAssignCustomer: TRadioButton
        Left = 16
        Top = 33
        Width = 113
        Height = 17
        Caption = 'Assign Customer'
        TabOrder = 1
      end
      object rbDontParse: TRadioButton
        Left = 16
        Top = 69
        Width = 113
        Height = 17
        Caption = 'Don'#39't parse'
        TabOrder = 3
      end
      object edtDelimiter: TEdit
        Left = 232
        Top = 13
        Width = 17
        Height = 21
        Hint = 
          'Characters to the left of the delimiter will be assigned to the ' +
          'QuickBooks CLASS field.'
        MaxLength = 1
        TabOrder = 4
        Text = '-'
      end
      object rbAssignClassCustomer: TRadioButton
        Left = 16
        Top = 51
        Width = 177
        Height = 17
        Caption = 'Assign both Class and Customer'
        TabOrder = 2
      end
    end
    object cbManualChecks: TCheckBox
      Left = 9
      Top = 113
      Width = 144
      Height = 17
      Caption = 'Export Manual Checks'
      TabOrder = 3
    end
    object cbSplitBillingByDBDT: TCheckBox
      Left = 233
      Top = 113
      Width = 152
      Height = 17
      Caption = 'Split billing by D/B/D/T'
      TabOrder = 4
    end
    object cbEmployeeDetails: TCheckBox
      Left = 393
      Top = 113
      Width = 112
      Height = 17
      Caption = 'Employee details'
      TabOrder = 5
    end
  end
end
