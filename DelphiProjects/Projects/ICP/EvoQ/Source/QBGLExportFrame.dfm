inherited QBGLExportFrm: TQBGLExportFrm
  Width = 769
  inherited Panel1: TPanel
    Top = 351
    Width = 769
    inherited BitBtn1: TBitBtn
      Caption = 'Print Journal'
    end
  end
  inherited Panel3: TPanel
    Width = 769
    Caption = 
      'You can get Evolution Journal either directly from Evolution or ' +
      'from a file'
  end
  inherited pgSource: TPageControl
    Width = 769
    Height = 326
    ActivePage = TabSheet5
    inherited TabSheet4: TTabSheet
      inherited Panel2: TPanel
        Width = 761
      end
      inherited PayrollFrame: TEvolutionMultiplePayrollFrm
        Width = 761
        Height = 233
        inherited Splitter1: TSplitter
          Height = 233
        end
        inherited EvoCompany: TEvolutionCompanyFrm
          Height = 233
          inherited Splitter1: TSplitter
            Height = 233
          end
          inherited frmCL: TEvolutionDsFrm
            Height = 233
            inherited dgGrid: TReDBGrid
              Height = 208
            end
          end
          inherited frmCO: TEvolutionDsFrm
            Height = 233
            inherited dgGrid: TReDBGrid
              Height = 208
            end
          end
        end
        inherited frmPR: TEvolutionDsFrm
          Width = 122
          Height = 233
          inherited Panel1: TPanel
            Width = 122
          end
          inherited dgGrid: TReDBGrid
            Width = 122
            Height = 208
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited FileOpenFrame: TFileOpenFrm
        Top = 17
        Width = 761
        inherited Panel1: TPanel
          Width = 761
          inherited BitBtn1: TBitBtn
            Left = 677
          end
          inherited edtFile: TEdit
            Left = 8
            Width = 657
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 761
        Height = 17
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '   Select a S2601 report'
        TabOrder = 1
      end
    end
  end
  inline QBParamFrame: TQBGLExportParamFrm [3]
    Left = 0
    Top = 389
    Width = 769
    Height = 139
    Align = alBottom
    TabOrder = 3
    inherited GroupBox1: TGroupBox
      Width = 769
    end
  end
  inherited ActionList1: TActionList
    inherited RunAction: TAction
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited PrintReportAction: TAction
      Caption = 'Print Journal'
    end
  end
end
