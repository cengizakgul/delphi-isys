unit FileOpenFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, StdCtrls, ActnList, StdActns, Buttons;

type
  TFileOpenFrm = class(TFrame)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    edtFile: TEdit;
    procedure FileOpen1Accept(Sender: TObject);
  private
    function GetFilename: string;
    { Private declarations }
  public
    function IsValid: boolean;
    property Filename: string read GetFilename;
  end;

implementation

{$R *.dfm}

{ TFileOpenFrm }

function TFileOpenFrm.IsValid: boolean;
begin
  Result := edtFile.Text <> '';
end;

procedure TFileOpenFrm.FileOpen1Accept(Sender: TObject);
begin
  edtFile.Text := (Sender as TFileOpen).Dialog.FileName;
end;

function TFileOpenFrm.GetFilename: string;
begin
  Assert(IsValid);
  Result := edtFile.Text;
end;

end.
