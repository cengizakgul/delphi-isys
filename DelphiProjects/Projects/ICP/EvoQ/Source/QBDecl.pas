unit QBDecl;

interface

uses
  gdycommonlogger, issettings;

type
  TQBChecksExportParam = record
    Clear: boolean;       // ClearedStatus
    CashAccount: string;
    LiabilityAccount: string;
    IncludeDD: boolean;
    IncludeEENames: boolean;
  end;

  TGLTagParsing = (parseNone, parseClass, parseName, parseClassName);

  TQBGLExportParam = record
    Clear: boolean;       // ClearedStatus
    UseAccountNumbers: boolean; //Under Edit then Preferences it is in the Company Preferences Tab in the Accounting area.
    GLTagParsing: TGLTagParsing;
    DelimiterInGLTag: string;
    ExcludeManualChecks: boolean;
    SplitBillingByDBDT: boolean;
    EmployeeDetails: boolean;
  end;

  TEvoChecksExportKind = (evoAllChecksExport, evoEEChecksExport, evoAgencyChecksExport);

  TEvoChecksExportOptions = record
    Kind: TEvoChecksExportKind;
  end;

procedure LogQBGLExportParam(logger: ICommonLogger; param: TQBGLExportParam);
function LoadQBGLExportOptions(conf: IisSettings; root: string): TQBGLExportParam;
procedure SaveQBGLExportOptions( const param: TQBGLExportParam; conf: IisSettings; root: string);

procedure LogQBChecksExportParam(logger: ICommonLogger; param: TQBChecksExportParam);
function LoadQBChecksExportOptions(conf: IisSettings; root: string): TQBChecksExportParam;
procedure SaveQBChecksExportOptions( const param: TQBChecksExportParam; conf: IisSettings; root: string);

function LoadEvoChecksExportOptions(conf: IisSettings; root: string): TEvoChecksExportOptions;
procedure SaveEvoChecksExportOptions( const options: TEvoChecksExportOptions; conf: IisSettings; root: string);

implementation

uses
  sysutils, gdyCommon;

procedure LogQBGLExportParam(logger: ICommonLogger; param: TQBGLExportParam);
begin
  Logger.LogContextItem( 'Clear', BoolToStr(param.Clear, true) );
  Logger.LogContextItem( 'Treat GL tags as', IIF(param.UseAccountNumbers,'account numbers','account full names') );
  case param.GLTagParsing of
    parseNone:  Logger.LogContextItem( 'GL tag parsing', 'Don''t parse');
    parseClass: Logger.LogContextItem( 'GL tag parsing', 'Assign class');
    parseClassName: Logger.LogContextItem( 'GL tag parsing', 'Assign both class and customer');
    parseName:  Logger.LogContextItem( 'GL tag parsing', 'Assign name');
  else
    Assert(false);
  end;
  Logger.LogContextItem( 'Delimiter', param.DelimiterInGLTag );
  Logger.LogContextItem( 'Exclude manual checks', BoolToStr(param.ExcludeManualChecks, true) );
  Logger.LogContextItem( 'Split billing by D/B/D/T', BoolToStr(param.SplitBillingByDBDT, true) );
  Logger.LogContextItem( 'Employee details', BoolToStr(param.EmployeeDetails, true) );
end;

function LoadQBGLExportOptions(conf: IisSettings; root: string): TQBGLExportParam;
begin
  root := root + '\';
  Result.Clear := conf.AsBoolean[root+'Clear'];
  if conf.GetValueNames(root).IndexOf('UseAccountNumbers') <> -1 then
    Result.UseAccountNumbers := conf.AsBoolean[root+'UseAccountNumbers']
  else
    Result.UseAccountNumbers := true;

  if conf.GetValueNames(root).IndexOf('GLTagParsing') <> -1 then
  begin
    Result.GLTagParsing := TGLTagParsing(conf.AsInteger[root+'GLTagParsing'])
  end
  else
  begin
    if conf.AsBoolean[root+'ExtractClassFromGLTag'] then
      Result.GLTagParsing := parseClass
    else
      Result.GLTagParsing := parseNone;
  end;
  if (Result.GLTagParsing < low(TGLTagParsing)) or (Result.GLTagParsing > high(TGLTagParsing)) then
    Result.GLTagParsing := parseNone;

  Result.DelimiterInGLTag := Copy(conf.AsString[root+'DelimiterInGLTag'],1,1);
  Result.ExcludeManualChecks := conf.AsBoolean[root+'ExcludeManualChecks'];
  Result.SplitBillingByDBDT := conf.AsBoolean[root+'SplitBillingByDBDT'];
  Result.EmployeeDetails := conf.AsBoolean[root+'EmployeeDetails'];
end;

procedure SaveQBGLExportOptions( const param: TQBGLExportParam; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsBoolean[root+'Clear'] := param.Clear;
  conf.AsBoolean[root+'UseAccountNumbers'] := param.UseAccountNumbers;
  conf.AsInteger[root+'GLTagParsing'] := Ord(param.GLTagParsing);
  conf.AsString[root+'DelimiterInGLTag'] := param.DelimiterInGLTag;
  conf.AsBoolean[root+'ExcludeManualChecks'] := param.ExcludeManualChecks;
  conf.AsBoolean[root+'SplitBillingByDBDT'] := param.SplitBillingByDBDT;
  conf.AsBoolean[root+'EmployeeDetails'] := param.EmployeeDetails;
end;

/////

procedure LogQBChecksExportParam(logger: ICommonLogger; param: TQBChecksExportParam);
begin
  Logger.LogContextItem( 'Clear', BoolToStr(param.Clear, true) );
  Logger.LogContextItem( 'Cash account', param.CashAccount );
  Logger.LogContextItem( 'Liability account', param.LiabilityAccount );
  Logger.LogContextItem( 'Create direct deposit checks', BoolToStr(param.IncludeDD, true) );
  Logger.LogContextItem( 'Include payee names', BoolToStr(param.IncludeEENames, true) );
end;

function LoadQBChecksExportOptions(conf: IisSettings; root: string): TQBChecksExportParam;
begin
  root := root + '\';
  Result.Clear := conf.AsBoolean[root+'Clear'];
  Result.CashAccount := conf.AsString[root+'CashAccount'];
  Result.LiabilityAccount := conf.AsString[root+'LiabilityAccount'];
  Result.IncludeDD := conf.AsBoolean[root+'IncludeDD'];
  Result.IncludeEENames := conf.GetValue(root+'IncludeEENames', True);
end;

procedure SaveQBChecksExportOptions( const param: TQBChecksExportParam; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsBoolean[root+'Clear'] := param.Clear;
  conf.AsString[root+'CashAccount'] := param.CashAccount;
  conf.AsString[root+'LiabilityAccount'] := param.LiabilityAccount;
  conf.AsBoolean[root+'IncludeDD'] := param.IncludeDD;
  conf.AsBoolean[root+'IncludeEENames'] := param.IncludeEENames;
end;

/////////

function LoadEvoChecksExportOptions(conf: IisSettings; root: string): TEvoChecksExportOptions;
begin
  root := root + '\';
  Result.Kind := TEvoChecksExportKind(conf.AsInteger[root+'Kind']);
  if (Result.Kind < low(TEvoChecksExportKind)) or (Result.Kind > high(TEvoChecksExportKind)) then
    Result.Kind := evoEEChecksExport;
end;

procedure SaveEvoChecksExportOptions( const options: TEvoChecksExportOptions; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsInteger[root+'Kind'] := ord(options.Kind);
end;




end.
