#define APPBASE "EvoQ"
#define APPNAME "Evolution QB Export"

#define NO_HTML_TEMPLATES

[Run]
Filename: "{src}\QBFC7_0Installer.exe"; Description: "Install QuickBooks client library (mandatory unless you already have it installed)"; Flags: postinstall 

#include <..\..\Common\Installer\common.iss>

 