inherited MainFm: TMainFm
  Left = 545
  Top = 223
  Width = 867
  Height = 600
  Caption = 'MainFm'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 42
    Width = 859
    Height = 505
    ActivePage = TabSheet2
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 851
        Align = alTop
        inherited GroupBox1: TGroupBox
          Width = 851
        end
      end
    end
    object tbshEdMap: TTabSheet [1]
      Caption = 'Client E/D mapping'
      ImageIndex = 3
      inline EDCodeFrame: TEDCodeFrm
        Left = 0
        Top = 0
        Width = 851
        Height = 477
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 851
          Height = 477
          inherited evSplitter2: TSplitter
            Top = 213
            Width = 851
          end
          inherited pnltop: TPanel
            Width = 851
            Height = 213
            inherited evSplitter1: TSplitter
              Height = 213
            end
            inherited pnlTopLeft: TPanel
              Height = 213
              inherited dgLeft: TReDBGrid
                Height = 188
              end
            end
            inherited pnlTopRight: TPanel
              Width = 581
              Height = 213
              inherited evPanel4: TPanel
                Width = 581
              end
              inherited dgRight: TReDBGrid
                Width = 581
                Height = 188
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 218
            Width = 851
            inherited pnlbottom: TPanel
              Width = 851
              inherited evPanel5: TPanel
                Width = 851
              end
              inherited dgBottom: TReDBGrid
                Width = 851
              end
            end
            inherited pnlMiddle: TPanel
              Width = 851
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Import'
      object Bevel1: TBevel
        Left = 0
        Top = 437
        Width = 851
        Height = 6
        Align = alBottom
        Shape = bsBottomLine
      end
      object Bevel2: TBevel
        Left = 0
        Top = 40
        Width = 851
        Height = 6
        Align = alTop
        Shape = bsBottomLine
      end
      object pnlBottom: TPanel
        Left = 0
        Top = 443
        Width = 851
        Height = 34
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Left = 8
          Top = 6
          Width = 97
          Height = 25
          Action = RunAction
          Caption = 'Import - week 1'
          TabOrder = 0
        end
        object BitBtn2: TBitBtn
          Left = 127
          Top = 6
          Width = 97
          Height = 25
          Action = actTCImportWeek2
          Caption = 'Import - week 2'
          TabOrder = 1
        end
      end
      object pnlPayrollBatch: TPanel
        Left = 0
        Top = 46
        Width = 851
        Height = 391
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        inline PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Left = 0
          Top = 0
          Width = 851
          Height = 391
          Align = alClient
          TabOrder = 0
          inherited Splitter1: TSplitter
            Height = 391
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 391
            inherited dgGrid: TReDBGrid
              Height = 366
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 532
            Height = 391
            inherited Panel1: TPanel
              Width = 532
            end
            inherited dgGrid: TReDBGrid
              Width = 532
              Height = 366
            end
          end
        end
      end
      object pnlTop: TPanel
        Left = 0
        Top = 0
        Width = 851
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        inline FileOpenFrame: TFileOpenLeftBtnFrm
          Left = 0
          Top = 0
          Width = 851
          Height = 40
          Align = alClient
          TabOrder = 0
          DesignSize = (
            851
            40)
          inherited BitBtn1: TBitBtn
            Left = 6
            Width = 139
          end
          inherited edtFile: TEdit
            Left = 160
            Width = 689
          end
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 547
    Width = 859
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 859
    Height = 34
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 158
      Height = 34
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 13
        Top = 8
        Width = 140
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 158
      Top = 0
      Width = 701
      Height = 34
      Align = alClient
      TabOrder = 1
      inherited dblcCO: TwwDBLookupCombo
        Width = 225
      end
    end
  end
  object pnlSpacer: TPanel
    Left = 0
    Top = 34
    Width = 859
    Height = 8
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object RunAction: TAction
      Caption = 'Import - week 1'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object RunEmployeeExport: TAction
      Caption = 'Update XYZ Employee Records for Selected Company'
    end
    object actTCImportWeek2: TAction
      Caption = 'Import - week 2'
      OnExecute = actTCImportWeek2Execute
      OnUpdate = actTCImportWeek2Update
    end
  end
end
