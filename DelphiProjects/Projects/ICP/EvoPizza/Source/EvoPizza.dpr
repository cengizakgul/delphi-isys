program EvoPizza;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  common,
  evoapiconnection,
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm: TForm},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm: TEvoAPIClientMainFm},
  EDCodeFrame in 'EDCodeFrame.pas' {EDCodeFrm: TFrame},
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  MidasLib,
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  FileOpenLeftBtnFrame in '..\..\common\FileOpenLeftBtnFrame.pas' {FileOpenLeftBtnFrm: TFrame},
  TCFile in 'TCFile.pas';

{$R *.res}

begin
  LicenseKey := 'F0B388DD829946C2B907C3A7E9E532C2'; //EvoADI Basic

  ConfigVersion := 1;

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution Pizza';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
