unit EDCodeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame;

type
  TEDCodeFrm = class(TCustomBinderBaseFrm)
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; CL_E_DS: TDataSet);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, typinfo;

{ TEDCodeFrm }

const
  EECodeBinding: TBindingDesc =
    ( Name: 'EDCodeBinding';
      Caption: 'Week 1 E/D codes - Week 2 E/D codes';
      LeftDesc: ( Caption: 'Week 1 E/D codes'; Unique: true; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION');
      RightDesc: ( Caption: 'Week 2 E/D codes'; Unique: true; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION'); //CL_E_DS
      HideKeyFields: true;
    );

constructor TEDCodeFrm.Create(Owner: TComponent);
begin
  inherited;
end;

procedure TEDCodeFrm.Init(matchTableContent: string; CL_E_DS: TDataSet);
begin
  FBindingKeeper := CreateBindingKeeper( EECodeBinding, TClientDataSet );
  CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').DisplayLabel := 'E/D code';
  CL_E_DS.FieldByName('DESCRIPTION').DisplayLabel := 'Description';
  FBindingKeeper.SetTables(CL_E_DS, CL_E_DS);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

end.



