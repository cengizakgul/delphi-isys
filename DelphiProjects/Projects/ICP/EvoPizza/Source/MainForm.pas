unit MainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame,
  EvolutionPrPrBatchFrame, FileOpenFrame, CustomBinderBaseFrame,
  EDCodeFrame, timeclockimport,
  FileOpenLeftBtnFrame, OptionsBaseFrame;

type
  TMainFm = class(TEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    pnlBottom: TPanel;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshEdMap: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    pnlPayrollBatch: TPanel;
    PayrollBatchFrame: TEvolutionPrPrBatchFrm;
    BitBtn1: TBitBtn;
    EDCodeFrame: TEDCodeFrm;
    Bevel1: TBevel;
    pnlSpacer: TPanel;
    pnlTop: TPanel;
    Bevel2: TBevel;
    FileOpenFrame: TFileOpenLeftBtnFrm;
    actTCImportWeek2: TAction;
    BitBtn2: TBitBtn;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
    procedure actTCImportWeek2Update(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actTCImportWeek2Execute(Sender: TObject);
  private
    FEvoData: TEvoData;

    procedure AutoConnect;
    procedure HandleEDCodeMappingChanged;

    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure LoadPerClientSettings(const Value: TEvoCompanyDef);
    function GetTimeClockData1(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
    function GetTimeClockData2(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}
uses
  isSettings, EeUtilityLoggerViewFrame, dialogs,
  userActionHelpers, gdyBinder, variants, gdyDeferredCall, gdyCommon, tcfile;


procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      FEvoData.Connect(EvoFrame.Param);
      ConnectToEvo.Caption := 'Refresh Evolution data';
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEeUtilityLoggerViewFrm;
  inherited;
  FEvoData := TEvoData.Create(Logger);
  FEvoData.AddDS(TMP_PRDesc);
  FEvoData.AddDS(PR_BATCHDesc);
  FEvoData.AddDS(CL_E_DSDesc);

  CompanyFrame.Init(FEvoData);
  PayrollBatchFrame.Init(FEvoData);
  FEvoData.Advise(HandleCompanyChanged);

  EDCodeFrame.OnMatchTableChanged := HandleEDCodeMappingChanged;

  //designer crashed repeatedly when I tried to do this by editing the form
  FileOpenFrame.FileOpen1.Dialog.Filter := 'Timeclock files (*.csv)|*.csv|All files (*.*)|*.*';
  FileOpenFrame.FileOpen1.Hint := 'Open|Opens timeclock file';

  if EvoFrame.IsValid then
    PageControl1.TabIndex := 2
  else
    PageControl1.TabIndex := 0;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

procedure TMainFm.LoadPerClientSettings(const Value: TEvoCompanyDef);
begin
  EDCodeFrame.Init( FSettings.AsString[ClientUniquePath(Value)+'\EDCodeMapping\DataPacket'], FEvoData.DS[CL_E_DSDesc.Name] );
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  Logger.LogEntry('Current company changed');
  try
    try
      LogNullableEvoCompanyDef(Logger, new);

      if not NullableEvoCompanyDefsClientsAreEqual(old, new) then
      begin
        EDCodeFrame.UnInit;
        Repaint;
        if new.HasValue then
          LoadPerClientSettings(new.Value);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetPrBatch and FileOpenFrame.IsValid;
end;

function BuildTCImportAPIOptions: TTCImportAPIOptions;
begin
  Result.LookupOption := tciLookupBySSN;
  Result.DBDTOption := tciDBDTSmart;
  Result.FourDigitYear := false;
  Result.FileFormat := tciFileFormatCommaDelimited;
  Result.AutoImportJobCodes := false;
  Result.UseEmployeePayRates := true;
end;

function TMainFm.GetTimeClockData1(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result.Text := FileToString(FileOpenFrame.Filename);
  Result.APIOptions := BuildTCImportAPIOptions;
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      Assert(FileOpenFrame.IsValid);
      Logger.LogContextItem('Filename', FileOpenFrame.Filename);
      RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData1, FEvoData.Connection, false);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TMainFm.GetTimeClockData2(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result.Text := tcfile.ReplaceEDs(FileToString(FileOpenFrame.Filename), EDCodeFrame.Matcher, Logger);
  Result.APIOptions := BuildTCImportAPIOptions;
end;

procedure TMainFm.actTCImportWeek2Execute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      Assert(FileOpenFrame.IsValid);
      Logger.LogContextItem('Filename', FileOpenFrame.Filename);
      RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData2, FEvoData.Connection, false);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleEDCodeMappingChanged;
begin
  FSettings.AsString[ClientUniquePath(FEvoData.GetClCo) + '\EDCodeMapping\DataPacket'] := EDCodeFrame.SaveToString;
end;

procedure TMainFm.actTCImportWeek2Update(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetPrBatch and FileOpenFrame.IsValid and EDCodeFrame.CanCreateMatcher;
end;

procedure TMainFm.AutoConnect;
begin
  Logger.LogEntry('Application opened');
  try
    try
      Repaint;
      if EvoFrame.IsValid then
      begin
        FEvoData.Connect(EvoFrame.Param);
        ConnectToEvo.Caption := 'Refresh Evolution data';
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.FormShow(Sender: TObject);
begin
  DeferredCall(AutoConnect);
end;


end.
