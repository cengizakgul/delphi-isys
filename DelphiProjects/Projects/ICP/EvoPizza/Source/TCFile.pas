unit TCFile;

interface

uses
  timeclockimport, gdybinder, gdyCommonLogger;

function ReplaceEDs(text: string; EDCodeMatcher: IMatcher; logger: ICommonLogger): string;

implementation

uses
  gdyClasses, SysUtils, variants;

function ReplaceEDs(text: string; EDCodeMatcher: IMatcher; logger: ICommonLogger): string;
  function p(s: string): string;
  begin
    Result := StringReplace(s, '|', ' ', [rfReplaceAll]);
  end;
var
  lines: IStr;
  i: integer;
  s: string;
  line: IStr;
  edcode: string;
  mappedEDCode: Variant;
  j: integer;
  newline: string;
begin
  logger.LogEntry('Processing timeclock file');
  try
    try
      logger.LogDebug('EDCode mapping:', EDCodeMatcher.Dump);
      lines := SplitToLines(text);

      Result := '';
      for i := 0 to lines.Count-1 do
      begin
        logger.LogEntry('Processing timeclock line');
        try
          try
            logger.LogContextItem('Line', lines.Str[i]);
            s := StringReplace(lines.Str[i], ' ', '|', [rfReplaceAll]); //mimics SProcessTCImport
            line := CsvAsStr(s);
            if line.Count = 0 then
              raise Exception.Create('Unexpected empty line');
            edcode := trim(p(line.Str[5])) + trim(p(line.Str[6]));
            mappedEDCode := EDCodeMatcher.RightMatch(edcode);
            if VarIsNull(mappedEDCode) then
              Result := Result + lines.Str[i] + #13#10
            else
            begin
              newline := '';
              for j := 0 to line.Count-1 do
              begin
                if j = 5 then
                  newline := newline + ',' + copy(mappedEDCode,1,1)
                else if j = 6 then
                  newline := newline + ',' + copy(mappedEDCode,2,9999)
                else
                  newline := newline + ',' + AnsiQuotedStr(p(line.str[j]), '"');
              end;
              Result := Result + copy(newline,2,9999) + #13#10;
            end;
          except
            logger.PassthroughException;
          end;
        finally
          logger.LogExit;
        end
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end
end;

end.
