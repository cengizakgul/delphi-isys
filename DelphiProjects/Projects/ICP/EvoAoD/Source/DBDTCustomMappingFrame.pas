unit DBDTCustomMappingFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, Wwdbigrd, Wwdbgrid, dbcomp, ExtCtrls, ActnList,
  ImgList, StdCtrls, Buttons, DBClient;

type
  TDBDTCustomMappingFrm = class(TFrame)
    dsExpression: TDataSource;
    dsEvoTables: TDataSource;
    pnlList: TPanel;
    pnlEdit: TPanel;
    dgExpression: TReDBGrid;
    BitBtn1: TBitBtn;
    ImageList1: TImageList;
    ActionList1: TActionList;
    actAppendCode: TAction;
    actAppendText: TAction;
    BitBtn2: TBitBtn;
    actDelete: TAction;
    BitBtn3: TBitBtn;
    actEdit: TAction;
    BitBtn4: TBitBtn;
    procedure actAppendCodeExecute(Sender: TObject);
    procedure actAppendTextExecute(Sender: TObject);
    procedure EnabledIfNotEmpty(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure EnabledIfInitialized(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
    procedure dgExpressionDblClick(Sender: TObject);
  private
    procedure ShowCodeDlg(verb: string);
    procedure ShowTextDlg(verb: string);
    { Private declarations }
  public
    constructor Create( Owner: TComponent ); override;
//    destructor Destroy; override;
    procedure SetMappingDS(expr, evoTables: TDataSet);
  end;

implementation

uses
  common, DBDTMapper, DBDTMappingCodeDialog, DBDTMappingTextDialog, gdyDialogEngine;

{$R *.dfm}

{ TDBDTCustomMappingFrm }

constructor TDBDTCustomMappingFrm.Create(Owner: TComponent);
begin
  inherited;

end;

procedure TDBDTCustomMappingFrm.SetMappingDS(expr, evoTables: TDataSet);
begin
  dsExpression.DataSet := expr;
  dsEvoTables.DataSet := evoTables;

  dgExpression.Selected.Clear;
  AddSelected(dgExpression.Selected, 'SUMMARY', 100, 'Parts', true);
end;

procedure TDBDTCustomMappingFrm.actAppendCodeExecute(Sender: TObject);
begin
  dsExpression.DataSet.Append;
  dsExpression.DataSet['TYPE'] := ord(partTable);
  dsExpression.DataSet['TABLE'] := 'CO';
  ShowCodeDlg('Append');
end;

procedure TDBDTCustomMappingFrm.actAppendTextExecute(Sender: TObject);
begin
  dsExpression.DataSet.Append;
  dsExpression.DataSet['TYPE'] := ord(partLiteral);
  dsExpression.DataSet['LITERAL'] := '';
  ShowTextDlg('Append');
end;

procedure TDBDTCustomMappingFrm.EnabledIfNotEmpty(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (dsExpression.DataSet <> nil) and (dsExpression.DataSet.RecordCount > 0);
end;

procedure TDBDTCustomMappingFrm.actDeleteExecute(Sender: TObject);
begin
  dsExpression.DataSet.Delete;
end;

procedure TDBDTCustomMappingFrm.ShowCodeDlg(verb: string);
var
  dlg: TDBDTMappingCodeDlg;
begin
  try
    dlg := TDBDTMappingCodeDlg.Create(nil);
    try
      dlg.Caption := verb + ' Code';
      dlg.SetMappingDS(dsExpression.DataSet, dsEvoTables.DataSet);
      with DialogEngine(dlg, Owner) do
        if ShowModal = mrOk then
          dsExpression.DataSet.Post
        else
          dsExpression.DataSet.Cancel;
    finally
      FreeAndNil(dlg);
    end;
  except
    dsExpression.DataSet.Cancel;
    raise;
  end;
end;

procedure TDBDTCustomMappingFrm.ShowTextDlg(verb: string);
var
  dlg: TDBDTMappingTextDlg;
begin
  try
    dlg := TDBDTMappingTextDlg.Create(nil);
    try
      dlg.Caption := verb + ' Text';
      dlg.SetMappingDS(dsExpression.DataSet);
      with DialogEngine(dlg, Owner) do
        if ShowModal = mrOk then
          dsExpression.DataSet.Post
        else
          dsExpression.DataSet.Cancel;
    finally
      FreeAndNil(dlg);
    end;
  except
    dsExpression.DataSet.Cancel;
    raise;
  end;
end;

procedure TDBDTCustomMappingFrm.EnabledIfInitialized(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := dsExpression.DataSet <> nil;
end;

procedure TDBDTCustomMappingFrm.actEditExecute(Sender: TObject);
begin
  if dsExpression.DataSet['TYPE'] = ord(partLiteral) then
    ShowTextDlg('Edit')
  else if dsExpression.DataSet['TYPE'] = ord(partTable) then
    ShowCodeDlg('Edit')
end;

procedure TDBDTCustomMappingFrm.dgExpressionDblClick(Sender: TObject);
begin
  actEdit.Execute;
end;

end.
