inherited MainFm: TMainFm
  Left = 276
  Top = 18
  Width = 861
  Height = 790
  Caption = 'MainFm'
  Constraints.MinHeight = 790
  Constraints.MinWidth = 850
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 845
    Height = 692
    ActivePage = tbshCompanySettings
    OnChange = PageControl1Change
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 837
        inherited GroupBox1: TGroupBox
          Width = 837
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      object pcCompany: TPageControl
        Left = 0
        Top = 105
        Width = 837
        Height = 559
        ActivePage = tbshEEStatus
        Align = alClient
        TabOrder = 1
        object tbshWorkgroup: TTabSheet
          Caption = 'Workgroups mapping'
          inline WorkgroupFrame: TWorkgroupFrm
            Left = 0
            Top = 0
            Width = 829
            Height = 421
            Align = alClient
            TabOrder = 0
            inherited Bevel3: TBevel
              Width = 829
            end
            inherited Panel1: TPanel
              Width = 829
              Height = 417
              inherited Bevel1: TBevel
                Height = 417
              end
              inherited pnlWGList: TPanel
                Height = 417
                inherited dgWGList: TReDBGrid
                  Height = 412
                end
              end
              inherited pnlRight: TPanel
                Width = 460
                Height = 417
                inherited DBDTMappingFrame: TDBDTMappingFrm
                  Width = 460
                  Height = 417
                  inherited GroupBox1: TGroupBox
                    Width = 460
                    Height = 376
                  end
                  inherited pnlTop: TPanel
                    Width = 460
                  end
                end
              end
            end
          end
          inline WorkgroupNotesFrame: TNotesFrm
            Left = 0
            Top = 421
            Width = 829
            Height = 110
            Align = alBottom
            TabOrder = 1
            inherited mmNotes: TMemo
              Width = 875
              Height = 85
            end
            inherited pnlNotes: TPanel
              Width = 875
            end
          end
        end
        object tbshEEStatus: TTabSheet
          Caption = 'Employee statuses mapping'
          ImageIndex = 1
          inline EeStatusFrame: TEeStatusFrm
            Left = 0
            Top = 0
            Width = 829
            Height = 531
            Align = alClient
            TabOrder = 0
            inherited BinderFrm1: TBinderFrm
              Width = 829
              Height = 531
              inherited evSplitter2: TSplitter
                Top = 267
                Width = 829
              end
              inherited pnltop: TPanel
                Width = 829
                Height = 267
                inherited evSplitter1: TSplitter
                  Height = 267
                end
                inherited pnlTopLeft: TPanel
                  Height = 267
                  inherited dgLeft: TReDBGrid
                    Height = 242
                  end
                end
                inherited pnlTopRight: TPanel
                  Width = 559
                  Height = 267
                  inherited evPanel4: TPanel
                    Width = 559
                  end
                  inherited dgRight: TReDBGrid
                    Width = 559
                    Height = 242
                  end
                end
              end
              inherited evPanel1: TPanel
                Top = 272
                Width = 829
                inherited pnlbottom: TPanel
                  Width = 829
                  inherited evPanel5: TPanel
                    Width = 829
                  end
                  inherited dgBottom: TReDBGrid
                    Width = 829
                  end
                end
                inherited pnlMiddle: TPanel
                  Width = 829
                end
              end
            end
          end
        end
        object tbshEDCode: TTabSheet
          Caption = 'E/D codes mapping'
          ImageIndex = 3
          inline EDCodeFrame: TEDCodeFrm
            Left = 0
            Top = 0
            Width = 829
            Height = 531
            Align = alClient
            TabOrder = 0
            inherited BinderFrm1: TBinderFrm
              Width = 829
              Height = 531
              inherited evSplitter2: TSplitter
                Top = 267
                Width = 829
              end
              inherited pnltop: TPanel
                Width = 829
                Height = 267
                inherited evSplitter1: TSplitter
                  Height = 81
                end
                inherited pnlTopLeft: TPanel
                  Height = 81
                  inherited dgLeft: TReDBGrid
                    Height = 56
                  end
                end
                inherited pnlTopRight: TPanel
                  Width = 506
                  Height = 81
                  inherited evPanel4: TPanel
                    Width = 506
                  end
                  inherited dgRight: TReDBGrid
                    Width = 506
                    Height = 56
                  end
                end
              end
              inherited evPanel1: TPanel
                Top = 272
                Width = 829
                inherited pnlbottom: TPanel
                  Width = 776
                  inherited evPanel5: TPanel
                    Width = 776
                  end
                  inherited dgBottom: TReDBGrid
                    Width = 776
                  end
                end
                inherited pnlMiddle: TPanel
                  Width = 776
                end
              end
            end
          end
        end
        object tbshTOAType: TTabSheet
          Caption = 'TOA types mapping'
          ImageIndex = 2
          inline TOAFrame: TTOAFrm
            Left = 0
            Top = 0
            Width = 829
            Height = 531
            Align = alClient
            TabOrder = 0
            inherited BinderFrm1: TBinderFrm
              Width = 829
              Height = 531
              inherited evSplitter2: TSplitter
                Top = 267
                Width = 829
              end
              inherited pnltop: TPanel
                Width = 829
                Height = 267
                inherited evSplitter1: TSplitter
                  Height = 81
                end
                inherited pnlTopLeft: TPanel
                  Height = 81
                  inherited dgLeft: TReDBGrid
                    Height = 56
                  end
                end
                inherited pnlTopRight: TPanel
                  Width = 613
                  Height = 81
                  inherited evPanel4: TPanel
                    Width = 613
                  end
                  inherited dgRight: TReDBGrid
                    Width = 613
                    Height = 56
                  end
                end
              end
              inherited evPanel1: TPanel
                Top = 272
                Width = 829
                inherited pnlbottom: TPanel
                  Width = 883
                  inherited evPanel5: TPanel
                    Width = 883
                  end
                  inherited dgBottom: TReDBGrid
                    Width = 883
                  end
                end
                inherited pnlMiddle: TPanel
                  Width = 883
                end
              end
            end
          end
        end
        object tbshCustomFields: TTabSheet
          Caption = 'Custom fields mapping'
          ImageIndex = 4
          inline CustomFieldsFrame: TCustomFieldsFrm
            Left = 0
            Top = 0
            Width = 829
            Height = 421
            Align = alClient
            TabOrder = 0
            inherited BinderFrm1: TBinderFrm
              Width = 829
              Height = 421
              inherited evSplitter2: TSplitter
                Top = 157
                Width = 829
              end
              inherited pnltop: TPanel
                Width = 829
                Height = 157
                inherited evSplitter1: TSplitter
                  Height = 145
                end
                inherited pnlTopLeft: TPanel
                  Height = 145
                  inherited dgLeft: TReDBGrid
                    Height = 120
                  end
                end
                inherited pnlTopRight: TPanel
                  Width = 605
                  Height = 145
                  inherited evPanel4: TPanel
                    Width = 605
                  end
                  inherited dgRight: TReDBGrid
                    Width = 605
                    Height = 120
                  end
                end
              end
              inherited evPanel1: TPanel
                Top = 162
                Width = 829
                inherited pnlbottom: TPanel
                  Width = 875
                  inherited evPanel5: TPanel
                    Width = 875
                  end
                  inherited dgBottom: TReDBGrid
                    Width = 875
                  end
                end
                inherited pnlMiddle: TPanel
                  Width = 875
                end
              end
            end
          end
          inline CustomFieldsNotesFrame: TNotesFrm
            Left = 0
            Top = 421
            Width = 829
            Height = 110
            Align = alBottom
            TabOrder = 1
            inherited mmNotes: TMemo
              Width = 875
              Height = 85
            end
            inherited pnlNotes: TPanel
              Width = 875
            end
          end
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 837
        Height = 105
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 5
          Top = 0
          Width = 505
          Height = 105
          Caption = 'Attendance on Demand'
          TabOrder = 0
          inline ExtAppFrame: TExtAppConnectionParamFrm
            Left = 11
            Top = 17
            Width = 478
            Height = 82
            TabOrder = 0
            inherited lblPasswordComment: TLabel
              Left = 219
              Width = 238
              Caption = 'Optional, required only for running scheduled tasks'
            end
            inherited PasswordEdit: TPasswordEdit
              Top = 51
            end
          end
        end
        inline ExtAppOptionsFrame: TExtAppOptionsFrm
          Left = 518
          Top = 0
          Width = 318
          Height = 105
          TabOrder = 1
          inherited rgTOA: TRadioGroup
            Height = 105
          end
          inherited gbHyperquery: TGroupBox
            Left = 143
            Top = 57
            Height = 48
            inherited cmbHyperqueries: TComboBox
              Top = 19
            end
          end
        end
      end
    end
    object tbshEEExport: TTabSheet [2]
      Caption = 'Employee export'
      ImageIndex = 4
      object Panel2: TPanel
        Left = 0
        Top = 121
        Width = 837
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn5: TBitBtn
          Left = 9
          Top = 0
          Width = 97
          Height = 25
          Action = RunEmployeeExport
          Caption = 'Run export'
          TabOrder = 0
        end
        object BitBtn2: TBitBtn
          Left = 120
          Top = 0
          Width = 97
          Height = 25
          Action = actScheduleEEExport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      inline EEExportOptionsFrame: TEEExportOptionsFrm
        Left = 0
        Top = 0
        Width = 837
        Height = 121
        Align = alTop
        TabOrder = 1
        inherited BadgeExportOptionFrame: TBadgeExportOptionFrm
          inherited RadioGroup1: TRadioGroup
            Width = 209
          end
        end
      end
    end
    object tbshTOA: TTabSheet [3]
      Caption = 'TOA transfer'
      ImageIndex = 7
      object BitBtn6: TBitBtn
        Left = 8
        Top = 16
        Width = 137
        Height = 25
        Action = actTOAExport
        Caption = 'Evolution to AoD transfer'
        TabOrder = 0
      end
      object BitBtn7: TBitBtn
        Left = 160
        Top = 16
        Width = 193
        Height = 25
        Action = actScheduleTOAExport
        Caption = 'Create Evolution to AoD transfer task'
        TabOrder = 1
      end
      object BitBtn8: TBitBtn
        Left = 8
        Top = 56
        Width = 137
        Height = 25
        Action = actTOAImport
        Caption = 'AoD to Evolution transfer'
        TabOrder = 2
      end
      object BitBtn9: TBitBtn
        Left = 160
        Top = 56
        Width = 193
        Height = 25
        Action = actScheduleTOAImport
        Caption = 'Create AoD to Evolution transfer task'
        TabOrder = 3
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Timeclock data import'
      inherited pnlBottom: TPanel
        Top = 626
        Width = 837
        Height = 38
        object BitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 97
          Height = 25
          Action = RunAction
          Caption = 'Run import'
          TabOrder = 0
        end
        object BitBtn3: TBitBtn
          Left = 120
          Top = 8
          Width = 97
          Height = 25
          Action = actScheduleTCImport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      inherited pnlPayrollBatch: TPanel
        Width = 837
        Height = 626
        inherited PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Width = 837
          Height = 538
          inherited Splitter1: TSplitter
            Height = 538
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 538
            inherited dgGrid: TReDBGrid
              Height = 513
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 518
            Height = 538
            inherited Panel1: TPanel
              Width = 518
            end
            inherited dgGrid: TReDBGrid
              Width = 518
              Height = 513
            end
          end
        end
        inline TCImportOptionsFrame: TTCImportOptionsFrm
          Left = 0
          Top = 538
          Width = 837
          Height = 88
          Align = alBottom
          TabOrder = 1
          inherited cbAllowImport: TCheckBox
            Width = 487
          end
        end
      end
    end
    object tbshScheduler: TTabSheet [5]
      Caption = 'Scheduled tasks'
      ImageIndex = 5
      inline SchedulerFrame: TSchedulerFrm
        Left = 0
        Top = 0
        Width = 837
        Height = 664
        Align = alClient
        TabOrder = 0
        inherited pnlTasksControl: TPanel
          Width = 837
        end
        inherited PageControl2: TPageControl
          Width = 837
          Height = 623
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 369
              Width = 829
            end
            inherited pnlTasks: TPanel
              Width = 829
              Height = 369
              inherited dgTasks: TReDBGrid
                Width = 829
                Height = 328
              end
              inherited Panel1: TPanel
                Top = 328
                Width = 829
                inherited BitBtn17: TBitBtn
                  Left = 624
                  Top = 8
                end
              end
            end
            inherited pnlDetails: TPanel
              Top = 376
              Width = 829
              inherited pcTaskDetails: TPageControl
                Width = 829
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 654
                  end
                  inherited dgResults: TReDBGrid
                    Width = 654
                  end
                end
              end
            end
          end
        end
      end
    end
    object tbshSchedulerSettings: TTabSheet [6]
      Caption = 'Scheduler settings'
      ImageIndex = 6
      inline SmtpConfigFrame: TSmtpConfigFrm
        Left = 0
        Top = 0
        Width = 837
        Height = 180
        Align = alTop
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 837
        end
      end
    end
    object tbshWebService: TTabSheet [7]
      Caption = 'Web service'
      ImageIndex = 8
      OnShow = tbshWebServiceShow
      inline ServiceControlFrame: TServiceControlFrm
        Left = 0
        Top = 0
        Width = 837
        Height = 664
        Align = alClient
        TabOrder = 0
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 733
    Width = 845
  end
  inherited pnlCompany: TPanel
    Width = 845
    inherited Panel1: TPanel
      Width = 105
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 105
      Width = 740
    end
  end
  inherited ActionList1: TActionList
    inherited RunAction: TAction
      Caption = 'Run import'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    inherited RunEmployeeExport: TAction
      Caption = 'Run export'
      OnExecute = RunEmployeeExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
    object actScheduleTCImport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleTCImportExecute
      OnUpdate = actScheduleTCImportUpdate
    end
    object actScheduleEEExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEEExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
    object actTOAExport: TAction
      Caption = 'Evolution to AoD transfer'
      OnExecute = actTOAExportExecute
      OnUpdate = actTOAExportUpdate
    end
    object actScheduleTOAExport: TAction
      Caption = 'Create Evolution to AoD transfer task'
      OnExecute = actScheduleTOAExportExecute
      OnUpdate = actTOAExportUpdate
    end
    object actTOAImport: TAction
      Caption = 'AoD to Evolution transfer'
      OnExecute = actTOAImportExecute
      OnUpdate = actTOAImportUpdate
    end
    object actScheduleTOAImport: TAction
      Caption = 'Create AoD to Evolution transfer task'
      OnExecute = actScheduleTOAImportExecute
      OnUpdate = actTOAImportUpdate
    end
  end
end
