unit TOAFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame, ExtAppdecl;

type
  TTOAFrm = class(TCustomBinderBaseFrm)
    cdBenefits: TClientDataSet;
    cdBenefitsCODE: TIntegerField;
    cdBenefitsDESCRIPTION: TStringField;
  private
    procedure InitExtAppBenefitTypes(extConn: IExtAppConnection);
  public
    procedure Init(matchTableContent: string; CO_TIME_OFF_ACCRUAL: TDataSet; extConn: IExtAppConnection);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, IAeXMLBridge1;

{ TTOAFrm }

procedure TTOAFrm.Init(matchTableContent: string; CO_TIME_OFF_ACCRUAL: TDataSet; extConn: IExtAppConnection);
begin
  FBindingKeeper := CreateBindingKeeper( TOABinding, TClientDataSet );
  CO_TIME_OFF_ACCRUAL.FieldByName('DESCRIPTION').DisplayLabel := 'Evolution TOA type';

  InitExtAppBenefitTypes(extConn);

  FBindingKeeper.SetTables(CO_TIME_OFF_ACCRUAL, cdBenefits);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TTOAFrm.InitExtAppBenefitTypes(extConn: IExtAppConnection);
var
  i: integer;
  items: TAeBasicDataItemsAry;
begin
  CreateOrEmptyDataSet(cdBenefits);

  items := extConn.getBenefitsSimple;
  try
    for i := 0 to high(items) do
      if Format('Benefit %.2d',[items[i].Num]) <> items[i].NameLabel then
        Append(cdBenefits, 'CODE;DESCRIPTION', [items[i].Num, items[i].NameLabel]);
  finally
    for i := 0 to high(items) do
      FreeAndNil(items[i]);
  end;
end;

end.



