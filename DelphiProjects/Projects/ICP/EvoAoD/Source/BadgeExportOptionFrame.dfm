object BadgeExportOptionFrm: TBadgeExportOptionFrm
  Left = 0
  Top = 0
  Width = 220
  Height = 112
  TabOrder = 0
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 0
    Width = 201
    Height = 105
    Caption = 'Export as AoD badge number'
    ItemIndex = 0
    Items.Strings = (
      'Employee code'
      'Badge ID'
      'Timeclock number'
      'None')
    TabOrder = 0
  end
end
