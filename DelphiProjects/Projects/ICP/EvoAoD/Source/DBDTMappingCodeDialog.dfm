inherited DBDTMappingCodeDlg: TDBDTMappingCodeDlg
  Width = 347
  Height = 86
  AutoSize = True
  object Label1: TLabel
    Left = 0
    Top = 6
    Width = 25
    Height = 13
    Caption = 'Code'
  end
  object Label2: TLabel
    Left = 0
    Top = 38
    Width = 62
    Height = 13
    Caption = 'From position'
  end
  object Label3: TLabel
    Left = 0
    Top = 70
    Width = 52
    Height = 13
    Caption = 'To position'
  end
  object Label4: TLabel
    Left = 200
    Top = 38
    Width = 147
    Height = 13
    Caption = '(from the beginning if left blank)'
  end
  object Label5: TLabel
    Left = 200
    Top = 70
    Width = 108
    Height = 13
    Caption = '(to the end if left blank)'
  end
  object dblcCode: TDBLookupComboBox
    Left = 72
    Top = 0
    Width = 121
    Height = 21
    DataField = 'TABLE'
    DataSource = dsExpression
    KeyField = 'TABLE'
    ListField = 'NAME'
    ListSource = dsEvoTables
    TabOrder = 0
  end
  object dbedStart: TDBEdit
    Left = 72
    Top = 33
    Width = 121
    Height = 21
    DataField = 'START'
    DataSource = dsExpression
    TabOrder = 1
  end
  object dbedEnd: TDBEdit
    Left = 72
    Top = 65
    Width = 121
    Height = 21
    DataField = 'END'
    DataSource = dsExpression
    TabOrder = 2
  end
  object dsExpression: TDataSource
    Left = 264
    Top = 8
  end
  object dsEvoTables: TDataSource
    AutoEdit = False
    Left = 216
    Top = 8
  end
end
