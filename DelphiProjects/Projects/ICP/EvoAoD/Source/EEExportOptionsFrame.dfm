inherited EEExportOptionsFrm: TEEExportOptionsFrm
  Width = 433
  Height = 116
  object cbExportSSN: TCheckBox
    Left = 236
    Top = 6
    Width = 97
    Height = 17
    Caption = 'Export SSN'
    TabOrder = 0
  end
  object cbExportRates: TCheckBox
    Left = 236
    Top = 28
    Width = 129
    Height = 17
    Caption = 'Export pay rates'
    TabOrder = 1
    OnClick = cbExportRatesClick
  end
  object cbExportStandardHours: TCheckBox
    Left = 236
    Top = 91
    Width = 129
    Height = 17
    Caption = 'Export standard hours'
    TabOrder = 2
  end
  inline BadgeExportOptionFrame: TBadgeExportOptionFrm
    Left = 9
    Top = 4
    Width = 220
    Height = 112
    TabOrder = 3
  end
  object rbSalaryAsRate: TRadioButton
    Left = 256
    Top = 48
    Width = 169
    Height = 17
    Caption = 'Export Salary as Rate Amount'
    TabOrder = 4
  end
  object rbSalaryAsSalary: TRadioButton
    Left = 256
    Top = 69
    Width = 171
    Height = 17
    Caption = 'Export Salary as Salary Amount'
    TabOrder = 5
  end
end
