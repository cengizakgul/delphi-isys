unit ServiceControlFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, gdyCommonLogger, ActnList, WebServiceCredentialsDialog,
  webServiceCommon;

type
  TServiceControlFrm = class(TFrame)
    btnActivate: TButton;
    btnDeactivate: TButton;
    ActionList1: TActionList;
    actActivateService: TAction;
    actDeactivateService: TAction;
    actUpdateStatus: TAction;
    btnUpdateStatus: TButton;
    lblStatus: TLabel;
    actConfiguration: TAction;
    Button1: TButton;
    edServiceURL: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edWSDLURL: TEdit;
    procedure actActivateServiceExecute(Sender: TObject);
    procedure actDeactivateServiceExecute(Sender: TObject);
    procedure actUpdateStatusExecute(Sender: TObject);
    procedure actConfigurationExecute(Sender: TObject);
  private
    procedure SetStatusText(s: string);
    function GetWebServiceCredentials: TWebServiceCredentials;
//    procedure TryEnablePrivilege(cred: TWebServiceCredentials);
  public
    Logger: ICommonLogger;
    procedure UpdateStatus;
  end;

implementation

uses
  JclSvcCtrl, gdyRedir, common, gdycommon, extappdecl, gdyGlobalWaitIndicator,
  gdyDialogEngine, isSettings, WebServiceConfigurationDialog{, JwsclToken, jwaWindows};

{$R *.dfm}

type
  TMyServiceStatus = (stNotInstalled, stInstalledNotRunning, stRunning);

  TMyScManager = class
  private
    FSCManager: TJclSCManager;

  public
    constructor Create;
    destructor Destroy; override;

    function GetStatus: TMyServiceStatus;
    function GetStatusAsString: string;

    procedure Install(cred: TWebServiceCredentials);
    function GetService: TJclNTService;

  end;

procedure TServiceControlFrm.actActivateServiceExecute(Sender: TObject);
var
  scm: TMyScManager;
  cred: TWebServiceCredentials;
  msg: string;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      WaitIndicator.StartWait('Activating service');
      try
        scm := TMyScManager.Create;
        try
          try
            case scm.GetStatus of
              stNotInstalled:
              begin
                cred := GetWebServiceCredentials;
//                TryEnablePrivilege(cred);
                scm.Install(cred);
                SetStatusText(scm.GetStatusAsString);
                try
                  scm.GetService.Start;
                except
                  on E: Exception do
                  begin
                    msg := Format('You may need to grant the SeServiceLogonRight "logon as a service" privilege to %s', [cred.Account]);
                    Logger.LogWarning(msg);
                    E.Message := E.Message + #13#10 + msg;
                    raise;
                  end;
                end;
              end;
              stInstalledNotRunning: scm.GetService.Start;
              stRunning: ;
            else
              Assert(false);
            end;
          finally
            SetStatusText(scm.GetStatusAsString);
          end;
        finally
          FreeAndNil(scm);
        end;
      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TServiceControlFrm.actDeactivateServiceExecute(Sender: TObject);
var
  scm: TMyScManager;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      WaitIndicator.StartWait('Deactivating service');
      try
        scm := TMyScManager.Create;
        try
          try
            case scm.GetStatus of
              stNotInstalled: ;
              stInstalledNotRunning: scm.GetService.Delete;
              stRunning:
              begin
                scm.GetService.Stop;
                SetStatusText(scm.GetStatusAsString);
                scm.GetService.Delete;
              end;
            else
              Assert(false);
            end;
          finally
            SetStatusText(scm.GetStatusAsString);
          end;
        finally
          FreeAndNil(scm);
        end;
      finally
        WaitIndicator.EndWait;
      end;

    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TServiceControlFrm.actUpdateStatusExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      UpdateStatus;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

{ TMyScManager }

constructor TMyScManager.Create;
begin
  FSCManager := TJclSCManager.Create('',AdministratorsSCMDesiredAccess);
  FSCManager.Refresh(True);
end;

function TServiceControlFrm.GetWebServiceCredentials: TWebServiceCredentials;
var
  dlg: TWebServiceCredentialsDlg;
  cred: TWebServiceCredentials;
begin
  dlg := TWebServiceCredentialsDlg.Create(nil);
  try
    cred.Account := gdycommon.GetUserName(NameSamCompatible);
    cred.Password := '';
    dlg.Credentials := cred;
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        Result := dlg.Credentials
      else
        Abort;
  finally
    FreeAndNil(dlg);
  end;
end;

procedure TServiceControlFrm.SetStatusText(s: string);
var
  port: integer;
begin
  lblStatus.Caption := 'Status: ' + s;
  port := LoadWebServiceConfiguration(SiblingAppSettings(Redirection.GetFilename(sWebServiceFileAlias)), '').Port;
  edServiceURL.Text := Format('http://localhost:%d/soap/IEvolutionAoDImport', [port]);
  edWSDLURL.Text := Format('http://localhost:%d/wsdl/IEvolutionAoDImport', [port]);
end;

procedure TServiceControlFrm.UpdateStatus;
begin
  Logger.LogEntry('Updating status of web service');
  try
    try
      Repaint;
      with TMyScManager.Create do
      try
        SetStatusText( GetStatusAsString );
      finally
        Free;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

destructor TMyScManager.Destroy;
begin
  FreeAndNil(FSCManager);
  inherited;
end;

function TMyScManager.GetStatus: TMyServiceStatus;
var
  svc: TJclNTService;
begin
  FSCManager.Refresh(True);
  if FSCManager.FindService(IntegrationServiceName, svc) then
  begin
    if svc.ServiceState = ssRunning then
      Result := stRunning
    else
      Result := stInstalledNotRunning;
  end
  else
    Result := stNotInstalled;
end;

function TMyScManager.GetStatusAsString: string;
begin
  case GetStatus of
    stNotInstalled: Result := 'Not installed';
    stInstalledNotRunning: Result := 'Installed, stopped';
    stRunning: Result := 'Running';
  else
    Assert(false);
  end;
end;

procedure TMyScManager.Install(cred: TWebServiceCredentials);
begin
  FSCManager.Install(IntegrationServiceName, IntegrationServiceDisplayName, Redirection.GetFilename(sWebServiceFileAlias), 'iSystems LLC''s EvoAoD Web Service. Allows export and import tasks to be activated remotely',
    [stWin32OwnProcess], sstAuto, ectNormal, DefaultSvcDesiredAccess, nil, nil, PAnsiChar(cred.Account), PAnsiChar(cred.Password) );
end;

function TMyScManager.GetService: TJclNTService;
begin
  FSCManager.Refresh(true);
  if not FSCManager.FindService(IntegrationServiceName, Result) then
    raise Exception.CreateFmt('Cannot find service <%s>', [IntegrationServiceName]);
end;

procedure TServiceControlFrm.actConfigurationExecute(Sender: TObject);
var
  dlg: TWebServiceConfigurationDlg;
  settings: IisSettings;
  isRunning: boolean;
  scm: TMyScManager;
  conf: TWebServiceConfiguration;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      settings := SiblingAppSettings(Redirection.GetFilename(sWebServiceFileAlias));

      dlg := TWebServiceConfigurationDlg.Create(nil);
      try
        conf := LoadWebServiceConfiguration(settings, '');
        dlg.Configuration := conf;
        with DialogEngine(dlg, Owner) do
          if ShowModal = mrOk then
          begin
            WaitIndicator.StartWait('Applying new configuration');
            try
              scm := TMyScManager.Create;
              try
                try
                  isRunning := scm.GetStatus = stRunning;
                  if isRunning then
                    scm.GetService.Stop;
                  SaveWebServiceConfiguration(dlg.Configuration, settings, '');
                  settings := nil;    //flush before updating status
                  if isRunning then
                    scm.GetService.Start;
                finally
                  SetStatusText(scm.GetStatusAsString);
                end;
              finally
                FreeAndNil(scm);
              end;
            finally
              WaitIndicator.EndWait;
            end;
          end;
      finally
        FreeAndNil(dlg);
      end;

    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

{
procedure TServiceControlFrm.TryEnablePrivilege(cred: TWebServiceCredentials);
var
  acc: string;
begin
  Logger.LogEntry('Enabling privilege');
  try
    try
      Logger.LogContextItem('User', cred.Account);
      acc := gdycommon.GetUserName(NameSamCompatible);
      if cred.Account = acc then
        JwEnablePrivilege(SE_SERVICE_LOGON_NAME, pst_Disable)
      else
        ShowMessage( Format('You may need to enable the SeServiceLogonRight "logon as a service" privilege for %s', [cred.Account]) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;

end;
}

end.
