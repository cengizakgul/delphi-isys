program EvoAoD;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  common,
  ExtAppTasks in 'ExtAppTasks.pas',
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  timeclockimport in '..\..\common\timeclockimport.pas',
  EvoTCImportMainNewForm in '..\..\common\EvoTCImportMainNewForm.pas' {EvoTCImportMainNewFm: TEvoAPIClientMainFm},
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  userActionHelpers in '..\..\common\userActionHelpers.pas',
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  MidasLib,
  EvoWaitForm in '..\..\common\EvoWaitForm.pas' {EvoWaitFm},
  PlannedActionConfirmationForm in '..\..\common\PlannedActionConfirmationForm.pas' {ConfirmationFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  scheduler in '..\..\common\scheduler.pas',
  scheduledTask in '..\..\common\scheduledTask.pas',
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  ExtAppParamFrame in 'ExtAppParamFrame.pas' {ExtAppConnectionParamFrm: TOptionsBaseFrm},
  TCImportOptionsFrame in 'TCImportOptionsFrame.pas' {TCImportOptionsFrm: TOptionsBaseFrm},
  EEExportOptionsFrame in 'EEExportOptionsFrame.pas' {EEExportOptionsFrm: TOptionsBaseFrm},
  EEExportDialog in 'EEExportDialog.pas' {EEExportDlg: TFrame},
  TCImportDialog in 'TCImportDialog.pas' {TCImportDlg: TFrame},
  MainForm in 'MainForm.pas' {MainFm},
  IAeXMLBridge1 in 'IAeXMLBridge1.pas',
  ExtAppConnection in 'ExtAppConnection.pas',
  WorkgroupFrame in 'WorkgroupFrame.pas' {WorkgroupFrm: TFrame},
  EeStatusFrame in 'EeStatusFrame.pas' {EeStatusFrm: TFrame},
  extapp in 'extapp.pas',
  ExtAppParamDialog in 'ExtAppParamDialog.pas' {ExtAppConnectionParamDlg: TFrame},
  passwordcache in '..\..\common\passwordcache.pas',
  extappdecl in 'extappdecl.pas',
  EDCodeFrame in 'EDCodeFrame.pas' {EDCodeFrm: TFrame},
  TOAFrame in 'TOAFrame.pas' {TOAFrm: TFrame},
  toaimport in '..\..\common\toaimport.pas',
  ServiceControlFrame in 'ServiceControlFrame.pas' {ServiceControlFrm: TFrame},
  WebServiceCredentialsDialog in '..\..\common\webservice\WebServiceCredentialsDialog.pas' {WebServiceCredentialsDlg: TFrame},
  WebServiceConfigurationDialog in '..\..\common\webservice\WebServiceConfigurationDialog.pas' {WebServiceConfigurationDlg: TFrame},
  webserviceCommon in '..\..\common\webservice\webserviceCommon.pas',
  ExtAppOptionsFrame in 'ExtAppOptionsFrame.pas' {ExtAppOptionsFrm: TFrame},
  CustomFieldsFrame in 'CustomFieldsFrame.pas' {CustomFieldsFrm: TFrame},
  comboChoices in '..\..\common\comboChoices.pas',
  BadgeExportOptionFrame in 'BadgeExportOptionFrame.pas' {BadgeExportOptionFrm: TFrame},
  DBDTMapper in 'DBDTMapper.pas',
  DBDTMappingFrame in 'DBDTMappingFrame.pas' {DBDTMappingFrm: TFrame},
  DBDTCustomMappingFrame in 'DBDTCustomMappingFrame.pas' {DBDTCustomMappingFrm: TFrame},
  DBDTMappingCodeDialog in 'DBDTMappingCodeDialog.pas' {DBDTMappingCodeDlg: TFrame},
  DBDTMappingTextDialog in 'DBDTMappingTextDialog.pas' {DBDTMappingTextDlg: TFrame},
  dbcomp in '..\..\common\recomp\dbcomp.pas',
  NotesFrame in 'NotesFrame.pas' {NotesFrm: TFrame};

{$R *.res}

begin
//{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}

  LicenseKey := 'BA5BBB00547A4A8B96FA14DEC7BB8379'; 
//  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License

  ConfigVersion := 1;

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution AoD Import';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
  Application.Run;
  end
  else
  begin
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TExtAppTaskAdapter.Create(False), false));
  end
end.
