program EvoAoDService;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  SvcMgr,
  common in '..\..\common\common.pas',
  gdycommon in '..\..\common\gdycommon\gdycommon.pas',
  IdHTTPWebBrokerBridge in '..\..\common\webservice\IdHTTPWebBrokerBridge.pas',
  MyServiceU in '..\..\common\webservice\MyServiceU.pas' {MyService: TService},
  EvoAoDWebService in 'EvoAoDWebService.pas' {MyAppServerSoapDataModule: TSoapDataModule},
  MyWebModuleU in '..\..\common\webservice\MyWebModuleU.pas' {MyWebModule: TWebModule},
  extappdecl in 'extappdecl.pas',
  webserviceCommon in '..\..\common\webservice\webserviceCommon.pas';

{$R *.RES}

begin
  ConfigVersion := 1; //and the product version must be the same as in EvoAoD

  Application.Initialize;
  Application.CreateForm(TMyService, MyService);
  MyService.Name := IntegrationServiceName;
  MyService.DisplayName := IntegrationServiceDisplayName;

  Application.CreateForm(TMyWebModule, MyWebModule);
  Application.Run;
end.
