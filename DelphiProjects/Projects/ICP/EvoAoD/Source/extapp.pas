unit extapp;

interface

uses
  extappdecl, gdycommonlogger, timeclockimport{TPayPeriod},
  classes, evodata, evoapiconnection, evoapiconnectionutils, extappconnection, common,
  PlannedActions, gdyClasses, IAeXMLBridge1, gdyBinder, toaimport, dbdtmapper;

type
  TAnalyzeResult = record
    Errors: integer;
    Actions: ISyncActionsRO;
  end;

  TWorkgroupLevelRec = record
    Details: TAeWorkgroupLevel;
    Data: TAeWorkgroupAry;
    Fetched: boolean;
  end;

  TWorkgroupCache = class
  private
    FConn: IExtAppConnection;
    FLogger: ICOmmonLogger;
    FWorkgroupLevels: array [1..7] of TWorkgroupLevelRec;
    FMaxWorkgroupLevel: integer;
    function GetWorkgroupLevel(i: integer): TWorkgroupLevelRec;
    function _GetWGNumByCode(wglevel: integer; code: string): integer;
    function _GetWGNumByName(wglevel: integer; name: string): integer;
  public
    constructor Create(conn: IExtAppConnection; logger: ICommonLogger);
    property WorkgroupLevel[i: integer]: TWorkgroupLevelRec read GetWorkgroupLevel;
    property MaxWorkgroupLevel: integer read FMaxWorkgroupLevel;
    function GetWGIdentityByNum(wglevel: integer; idKind: TWGIdentityKind; num: integer): string;
    function GetWGNumByIdentity(wglevel: integer; idKind: TWGIdentityKind; val: Variant): integer;
  end;

  TExtAppEmployeeUpdater = class(TWorkgroupCache)
  private
    function MapEEStatus(code: string): Variant;
  private
    FConn: IExtAppConnection;
    FLogger: ICOmmonLogger;
    FOptions: TEEExportOptions;
    FExtAppOptions: TExtAppOptions;
    FEEData: TEvoEEData;
    FExtAppEEs: TAeEmployeeDetail2Ary;
    FEEStatusMatcher: IMatcher;
    FDBDTMapper: IDBDTMapper;
    FCustomFieldsMatcher: IMatcher;

    FPayClasses: TAeBasicDataItemsAry;
    FHourlyStatusTypes: TAeBasicDataItemsAry;
    FClockGroups: TAeBasicDataItemsAry;
    FSchedulePatterns: TAeBasicDataItemsAry;

    function GetIDByName(ary: TAeBasicDataItemsAry; name: Variant; typename: string): integer;
    function GetNameByID(ary: TAeBasicDataItemsAry; id: integer; typename: string): string;

    procedure ValidateExtAppEECodes;
    procedure BuildEE(newEE: TAeEmployeeDetail; EEData: TEvoEEData; giveWarnings: boolean);
  public
    constructor Create(conn: IExtAppConnection; const options: TEEExportOptions; const ExtAppOptions: TExtAppOptions; EEStatusMatcher: IMatcher; DBDTMApper: IDBDTMapper; CustomFieldsMatcher: IMatcher; logger: ICommonLogger; EEData: TEvoEEData);
    destructor Destroy; override;
    function Analyze: TAnalyzeResult;
    function Apply(actions: ISyncActionsRO; pi: IProgressIndicator): TUpdateEmployeesStat;
  end;


function GetTimeClockData(conn: IExtAppConnection; const options: TTCImportOptions; EDCodeMatcher: IMatcher; DBDTMapper: IDBDTMapper; company: TEvoCompanyDef; logger: ICommonLogger; period: TPayPeriod): TTimeClockImportData;

function ExportTOA(conn: IExtAppConnection; evoConn: IEvoAPIConnection; TOAMatcher: IMatcher; ExtAppOptions: TExtAppOptions; company: TEvoCompanyDef; logger: ICommonLogger; pi: IProgressIndicator): TTOAStat;
function GetTOAData(conn: IExtAppConnection; evoConn: IEvoAPIConnection; TOAMatcher: IMatcher; DBDTMapper: IDBDTMapper; company: TEvoCompanyDef; logger: ICommonLogger; pi: IProgressIndicator): TTOAImportData;

implementation

uses
  sysutils, math, gdystrset, variants, dateutils, RateImportOptionFrame,
  gdyGlobalWaitIndicator, evConsts, gdycommon, kbmMemTable, windows, comboChoices,
  DB;

function DigitsOnly(const aStr: WideString): WideString;
var
  i: integer;
begin
  Result := '';
  for i := 1 to Length(aStr) do
    if Pos(aStr[i], '0123456789') > 0 then
      Result := Result + aStr[i]; 
end;

function DateToExtAppDate(dt: TDateTime): string;
begin
  Result := DateToStdDate(dt) + ' 00:00:00.000';
end;

function GetWrkWG(line: TAePayLine; wglevel: integer): integer;
begin
  Result := 0; //to make compiler happy
  case wglevel of
    1: Result := line.WrkWG1;
    2: Result := line.WrkWG2;
    3: Result := line.WrkWG3;
    4: Result := line.WrkWG4;
    5: Result := line.WrkWG5;
    6: Result := line.WrkWG6;
    7: Result := line.WrkWG7;
  else
    Assert(false);
  end;
end;

function ThisIsWorkCompany(logger: ICommonLogger; wgs: TWorkgroupCache; DBDTMapper: IDBDTMapper; const Company: TEvoCompanyDef; const line: TAePayLine): boolean;
var
  wglevel: Variant;
  wgIdentity: string;
begin
  wglevel := DBDTMapper.GetCompanyWorkgroupLevel;
  if VarIsNull(wglevel) then
    Result := true
  else
  begin
    wgIdentity := trim(wgs.GetWGIdentityByNum(wglevel, DBDTMapper.GetWorkgroupIdentityKind(wglevel), GetWrkWG(line, wglevel)));
    if wgIdentity = '' then
      Logger.LogWarning('Multi-company mode is on, but work company workgroup field is not set');
    Result := wgIdentity = trim(Company.CUSTOM_COMPANY_NUMBER);
  end
end;

function GetTimeClockData(conn: IExtAppConnection; const options: TTCImportOptions; EDCodeMatcher: IMatcher; DBDTMapper: IDBDTMapper; company: TEvoCompanyDef; logger: ICommonLogger; period: TPayPeriod): TTimeClockImportData;
var
  extAppEEs: TAeEmployeeBasicAry;
  i: integer;
  j: integer;
  dt: TDateTime;
  paylines: TAePayLineAry;
  wgs: TWorkgroupCache;
  recs: TTimeClockRecs;
  wgIdentities: TWGIdentities;

  procedure AddRec(line: TAePayLine);
  var
    edcode: Variant;
    cur: PTimeClockRec;
    i: integer;
  begin
    SetLength(recs, Length(recs)+1 ); //initialized with zeroes
    cur := @recs[high(recs)];
    cur.CUSTOM_EMPLOYEE_NUMBER := trim(line.EmpID);
    cur.FirstName := line.FirstName;
    cur.LastName := line.LastName;
    cur.SSN := line.SSN;

    if line.PayDesNum > 0 then
    begin
      edcode := EDCodeMatcher.LeftMatch(line.PayDesNum);
      if VarIsNull(edcode) then
        raise Exception.CreateFmt('%s pay designation <%s> is not mapped to an Evolution client E/D code', [ExtAppName, line.PayDesName]);
      cur.EDCode := edcode;
    end;

    for i := 1 to 7 do
      wgIdentities[i] := wgs.GetWGIdentityByNum(i, DBDTMapper.GetWorkgroupIdentityKind(i), GetWrkWG(line, i));
    cur.Division :=   VarToStr(DBDTMapper.CalcTable(CDBDTMetadata[CLIENT_LEVEL_DIVISION].Table, wgIdentities));
    cur.Branch :=     VarToStr(DBDTMapper.CalcTable(CDBDTMetadata[CLIENT_LEVEL_BRANCH].Table, wgIdentities));
    cur.Department := VarToStr(DBDTMapper.CalcTable(CDBDTMetadata[CLIENT_LEVEL_DEPT].Table, wgIdentities));
    cur.Team :=       VarToStr(DBDTMapper.CalcTable(CDBDTMetadata[CLIENT_LEVEL_TEAM].Table, wgIdentities));
    cur.job :=        VarToStr(DBDTMapper.CalcTable('CO_JOBS', wgIdentities));

    if abs(line.Hours) > eps then
    begin
      cur.Hours := line.HoursHund;
      if options.RateImport = rateUseExternal then
        cur.Rate := line.BaseRate;
    end
    else
    begin
      cur.Amount := line.Dollars;
    end;

  end;

begin
  SetLength(recs, 0);
  logger.LogEntry('Getting timeclock data');
  try
    try
      extAppEEs := conn.getAllEmployeesList;
      try
        wgs := TWorkgroupCache.Create(conn, logger);
        try
          for i := 0 to high(extAppEEs) do
          begin
            logger.LogEntry('Getting employee timeclock data');
            try
              try
//                if extAppEEs[i].ActiveStatus = 1 then //!!for debug only
//                  continue;
                logger.LogContextItem(sCtxEECode, extAppEEs[i].EmpID);
                logger.LogContextItem('Name', extAppEEs[i].EmpName);
//                dt := EncodeDate(2006,3, 6);
//                while dt <= EncodeDate(2006,3,9) do
//                dt := EncodeDate(2004,6, 6);
//                while dt <= EncodeDate(2004,6,14) do
                dt := period.PeriodBegin;
                while dt <= period.PeriodEnd do
                begin
                  logger.LogEntry('Getting employee daily timeclock data');
                  try
                    try
                      logger.LogContextItem('Date', DateToStr(dt));
                      paylines := conn.extractEmployeeDailySummsByFilekey(extAppEEs[i].Filekey, dt );
                      try
                        for j := 0 to high(paylines) do
                          if ThisIsWorkCompany(logger, wgs, DBDTMapper, company, paylines[j]) then
                            AddRec(paylines[j]);
                      finally
                        for j := 0 to high(paylines) do
                          FreeAndNil(paylines[j]);
                      end;
                    except
                      logger.PassthroughException;
                    end;
                  finally
                    logger.LogExit;
                  end;
                  dt := IncDay(dt);
                end;
              except
                logger.PassthroughException;
              end;
            finally
              logger.LogExit;
            end;
          end;
        finally
          FreeAndNil(wgs);
        end;
      finally
        for i := 0 to high(extAppEEs) do
          FreeAndNil(extAppEEs[i]);
      end;

      if options.Summarize then
        recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
      Result := BuildTimeClockImportData(recs, options.RateImport = rateUseEvoEE, false, logger); //false if rateUseExternal; it was this way before and everybody was happy...
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

{ TExtAppEmployeeUpdater }

procedure TExtAppEmployeeUpdater.ValidateExtAppEECodes;
var
  i: integer;
  j: integer;
begin
  FLogger.LogEntry( Format('Validating %s employee codes', [ExtAppName]));
  try
    try
      for i := 0 to high(FExtAppEEs) do
        if trim(FExtAppEEs[i].EmpID) = ''  then
          FLogger.LogWarningFmt('Employee %s %s doesn''t have Employee Code',[FExtAppEEs[i].FirstName, FExtAppEEs[i].LastName]);

      for i := 0 to high(FExtAppEEs) do
        for j := i+1 to high(FExtAppEEs) do
          if (trim(FExtAppEEs[i].EmpID) <> '') and
             (trim(FExtAppEEs[j].EmpID) <> '') and
             (trim(FExtAppEEs[i].EmpID) = trim(FExtAppEEs[j].EmpID)) then
            FLogger.LogWarningFmt('Employees %s %s and %s %s have the same Employee Code (%s)',
             [FExtAppEEs[i].FirstName, FExtAppEEs[i].LastName, FExtAppEEs[j].FirstName, FExtAppEEs[j].LastName, FExtAppEEs[i].EmpID]);
    except
      FLogger.StopException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function IsInExtApp(const basicEEs: TAeEmployeeBasicAry; code: string): boolean;
var
  i:integer;
begin
  Result := false;
  for i := 0 to high(basicEEs) do
    if trim(basicEEs[i].EmpID) = trim(code) then
    begin
      Result := true;
      Exit;
    end;
end;

procedure SetWG(ee: TAeEmployeeBasic; wglevel, wgnum: integer);
begin
  case wglevel of
    1: ee.WG1 := wgnum;
    2: ee.WG2 := wgnum;
    3: ee.WG3 := wgnum;
    4: ee.WG4 := wgnum;
    5: ee.WG5 := wgnum;
    6: ee.WG6 := wgnum;
    7: ee.WG7 := wgnum;
  else
    Assert(false);
  end;
end;

function GetWG(ee: TAeEmployeeBasic; wglevel: integer): integer;
begin
  Result := 0; //to make compiler happy
  case wglevel of
    1: Result := ee.WG1;
    2: Result := ee.WG2;
    3: Result := ee.WG3;
    4: Result := ee.WG4;
    5: Result := ee.WG5;
    6: Result := ee.WG6;
    7: Result := ee.WG7;
  else
    Assert(false);
  end;
end;

function TExtAppEmployeeUpdater.MapEEStatus(code: string): Variant;
var
  EvoEEStatus: string;
begin
  Result := FEEStatusMatcher.RightMatch(code);
  if VarIsNull(Result) then
  begin
    EvoEEStatus := GetEvoNameByCode(Copy(code, 1, 1), EE_TerminationCodeX_ComboChoices);
    case code[2] of
      GROUP_BOX_YES: EvoEEStatus := EvoEEStatus + ' with Rehire is Yes';
      GROUP_BOX_NO: EvoEEStatus := EvoEEStatus + 'with Rehire is No';
      GROUP_BOX_CONDITIONAL: EvoEEStatus := EvoEEStatus + ' with Rehire is Conditional';
    end;
    raise Exception.CreateFmt('Evolution employee status <%s> is not mapped to an AoD employee status', [EvoEEStatus]);
  end;
end;

procedure TExtAppEmployeeUpdater.BuildEE(newEE: TAeEmployeeDetail; EEData: TEvoEEData; giveWarnings: boolean);
  function VarToCustomField(v: Variant): widestring;
  begin
    if VarType(v) = varDate then
      Result := DateToExtAppDate(v)
    else
      Result := VarToStr(v);
    if Result = '' then
      Result := ' ';  //Bill Baron wrote: We have a fix that allows for clearing of string field by sending a single space
  end;
var
  status: variant;
  level: char;
  customFieldName: Variant;
  badge: string;
  evoCodes: TEvoCodes;
  i: integer;
  wgvalue: TWGValue;
  v: Variant;
  SalaryAmount: Double;
begin
  newEE.EmpID := trim(EEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
  newEE.LastName := EEData.EEs['LAST_NAME'];
  newEE.FirstName := EEData.EEs['FIRST_NAME'];
  newEE.Initial := EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString;
  if FOptions.ExportSSN then
    newEE.SSN :=  StringReplace( EEData.EEs['SOCIAL_SECURITY_NUMBER'], '-', '', [rfReplaceAll] );
  newEE.DateOfHire := DateToExtAppDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime);

  status := MapEEStatus(FEEData.EEs['CURRENT_TERMINATION_CODE'] + FEEData.EEs['ELIGIBLE_FOR_REHIRE']);
  newEE.ActiveStatus := status[0];
  if newEE.ActiveStatus = 0 then //active
    newEE.ActiveStatusConditionID := status[1]
  else
  begin
    newEE.InactiveStatusConditionID := status[1];
    newEE.ActiveStatusConditionEffDate := DateToExtAppDate(EEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime);
  end;

  if EEData.EEs.FieldByName('BIRTH_DATE').AsDateTime > 0 then
    newEE.BirthDate := DateToExtAppDate(EEData.EEs.FieldByName('BIRTH_DATE').AsDateTime)
  else
    newEE.BirthDate := DateToExtAppDate(EncodeDate(1919,1,19)); //magic date from John

  newEE.AddressStateProv := EEData.EEs.FieldByName('STATE').AsString;
  newEE.AddressCity := EEData.EEs.FieldByName('CITY').AsString;
  newEE.AddressZIPPC := EEData.EEs.FieldByName('ZIP_CODE').AsString;
  newEE.Address1 := EEData.EEs.FieldByName('ADDRESS1').AsString;
  newEE.Address2 := EEData.EEs.FieldByName('ADDRESS2').AsString;
  newEE.Phone1 := DigitsOnly( EEData.EEs.FieldByName('PHONE1').AsString );
  newEE.Phone2 := DigitsOnly( EEData.EEs.FieldByName('PHONE2').AsString );
  if FOptions.ExportRates then
  begin
    SalaryAmount := EEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat;
    if SalaryAmount = 0 then
    begin
      newEE.CurrentRate := EEData.GetPrimaryRate;
      newEE.PayTypeID := 0; //Hourly
    end else
    begin
      if FOptions.ExportSalary = esSalaryAsRate then
      begin
        newEE.PayTypeID := 0; //Hourly
        newEE.CurrentRate := EEData.GetPrimaryRate;
      end else
      begin
        newEE.PayTypeID := 1; //Salaried
        newEE.CurrentRate := SalaryAmount;
        newEE.CurrentRateEffDate := DateToExtAppDate( EEData.EEs.FieldByName('EFFECTIVE_DATE').AsDateTime );
      end;
    end;
//    newEE.CurrentRateEffDate := DateToExtAppDate(DateOf(Now));  //see a comment in TExtAppEmployeeUpdater.Apply
  end;

  evoCodes.cdbdt[CLIENT_LEVEL_COMPANY] := FEEData.Company.CUSTOM_COMPANY_NUMBER;
  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
    evoCodes.cdbdt[level] := trim(EEData.EEs.FieldByName(CDBDTMetadata[level].CodeField).AsString);
  evoCodes.Job := EEData.EEs['JOB_CODE'];
  evoCodes.Position := EEData.EEs['POSITION_DESCRIPTION'];
  evoCodes.Shift := EEData.EEs['SHIFT_NAME'];

  for i := 1 to MaxWorkgroupLevel do
  begin
    wgvalue := FDBDTMapper.CalcWorkgroup(i, evoCodes);
    if not VarIsEmpty(wgvalue.Value) then
      v := wgvalue.Value
    else
      v := FEEData.GetEEAdditionalField(wgvalue.AdditionalFieldName);
    if not VarIsNull(v) then
      SetWG(newEE, i, GetWGNumByIdentity(i, wgvalue.IdentityKind, v));
  end;

  if FOptions.ExportStandardHours then
    newEE.AvgWeeklyHrs := Round( EEData.EEs.FieldByName('STANDARD_HOURS').AsFloat*60 );

  case FOptions.Badge of
    badgeFromEmployeeCode: badge := EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'];
    badgeFromBadgeID: badge := EEData.EEs.FieldByName('BADGE_ID').AsString;
    badgeFromTimeclockNumber: badge := EEData.EEs.FieldByName('TIME_CLOCK_NUMBER').AsString;
    badgeNone: badge := '';
  else
    Assert(false);
  end;
  badge := trim(badge);
  if badge <> '' then
  begin
    if IsInteger(badge) then
      newee.Badge := StrToInt(badge)
    else
    begin
      if giveWarnings then
        FLogger.LogWarningFmt('Badge number "%s" cannot be exported because it is not a number', [badge]);
    end
  end;

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField1Code);
  if VarHasValue(customFieldName) then
    newEE.StaticCustom1 := VarToCustomField( FEEData.GetEEAdditionalField(customFieldName) );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField2Code);
  if VarHasValue(customFieldName) then
    newEE.StaticCustom2 := VarToCustomField( FEEData.GetEEAdditionalField(customFieldName) );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField3Code);
  if VarHasValue(customFieldName) then
    newEE.StaticCustom3 := VarToCustomField( FEEData.GetEEAdditionalField(customFieldName) );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField4Code);
  if VarHasValue(customFieldName) then
    newEE.StaticCustom4 := VarToCustomField( FEEData.GetEEAdditionalField(customFieldName) );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField5Code);
  if VarHasValue(customFieldName) then
    newEE.StaticCustom5 := VarToCustomField( FEEData.GetEEAdditionalField(customFieldName) );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField6Code);
  if VarHasValue(customFieldName) then
    newEE.StaticCustom6 := VarToCustomField( FEEData.GetEEAdditionalField(customFieldName) );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppPayClassCode);
  if VarHasValue(customFieldName) then
    newEE.PayClassID := GetIDByName( FPayClasses, FEEData.GetEEAdditionalField(customFieldName), 'pay class' )
  else
    if not EEData.EEs.FieldByName('POSITION_STATUS').IsNull and (EEData.EEs.FieldByName('POSITION_STATUS').AsString <> POSITION_STATUS_NA) then
      newEE.PayClassID := GetIDByName( FPayClasses, GetEvoNameByCode(EEData.EEs.FieldByName('POSITION_STATUS').AsString, PositionStatus_ComboChoices), 'pay class' );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppHourlyStatusCode);
  if VarHasValue(customFieldName) then
    newEE.HourlyStatusID := GetIDByName( FHourlyStatusTypes, FEEData.GetEEAdditionalField(customFieldName), 'hourly status type' );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppClockGroupCode);
  if VarHasValue(customFieldName) then
    newEE.ClockGroupID := GetIDByName( FClockGroups, FEEData.GetEEAdditionalField(customFieldName), 'clock group' );

  customFieldName := FCustomFieldsMatcher.LeftMatch(sExtAppSchedulePatternCode);
  if VarHasValue(customFieldName) then
    newEE.SchPatternID := GetIDByName( FSchedulePatterns, FEEData.GetEEAdditionalField(customFieldName), 'schedule pattern' );
end;

function ThisIsPrimaryCompany(logger: ICommonLogger; wgs: TWorkgroupCache; DBDTMapper: IDBDTMapper; const Company: TEvoCompanyDef; const extAppEE: TAeEmployeeBasic): boolean;
var
  wglevel: Variant;
  wgIdentity: string;
begin
  wglevel := DBDTMapper.GetCompanyWorkgroupLevel;
  if VarIsNull(wglevel) then
    Result := true
  else
  begin
    wgIdentity := trim(wgs.GetWGIdentityByNum(wglevel, DBDTMapper.GetWorkgroupIdentityKind(wglevel),GetWG(extAppEE, wglevel)));
    if wgIdentity = '' then
      Logger.LogWarning('Multi-company mode is on, but home company workgroup field is not set');
    Result := wgIdentity = trim(Company.CUSTOM_COMPANY_NUMBER);
  end
end;

function TExtAppEmployeeUpdater.Analyze: TAnalyzeResult;

  function CompareFieldsForMissingEE(const extAppEE: TAeEmployeeDetail): TChangedFieldRecs;
  var
    status: variant;
  begin
    SetLength(Result, 0);
    status := FEEStatusMatcher.RightMatch(EE_TERM_X_MISSING);
    if VarIsNull(status) then
      exit; //user probably doesn't want to touch them

    CheckIfChanged(Result, extAppEE.ActiveStatus, status[0], 'Active status');
    if (extAppEE.ActiveStatus = 0) and (status[0] = 0) then
      CheckIfChanged(Result, extAppEE.ActiveStatusConditionID, status[1], sActiveStatusConditionID)
    else if (extAppEE.ActiveStatus = 1) and (status[0] = 1) then
      CheckIfChanged(Result, extAppEE.InactiveStatusConditionID, status[1], sInactiveStatusConditionID);
  end;

  function CompareFields(const extAppEE: TAeEmployeeDetail2; EEData: TEvoEEData): TChangedFieldRecs;
  var
    evoEE: TAeEmployeeDetail;
    i: integer;
    idKind: TWGIdentityKind;
  begin
    Assert( trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(extAppEE.EmpID) );
    SetLength(Result, 0);
    evoEE := TAeEmployeeDetail.Create;
    try
      evoEE.Badge := extAppEE.Badge; //Badge number is not exported if it is empty
      //wg is not exported if it is not mapped
      evoEE.WG1 := extAppEE.WG1;
      evoEE.WG2 := extAppEE.WG2;
      evoEE.WG3 := extAppEE.WG3;
      evoEE.WG4 := extAppEE.WG4;
      evoEE.WG5 := extAppEE.WG5;
      evoEE.WG6 := extAppEE.WG6;
      evoEE.WG7 := extAppEE.WG7;

      BuildEE(evoEE, EEData, true);
      CheckIfChanged(Result, extAppEE.LastName, evoEE.LastName, 'Last name');
      CheckIfChanged(Result, extAppEE.FirstName, evoEE.FirstName, 'First name');
      CheckIfChanged(Result, extAppEE.Initial, evoEE.Initial, 'Middle initial');
      if FOptions.ExportSSN then
        CheckIfSSNChanged(Result, extAppEE.SSN, evoEE.SSN, 'SSN');

      CheckIfChanged(Result, extAppEE.DateOfHire, evoEE.DateOfHire, 'Date of hire');
      CheckIfChanged(Result, extAppEE.ActiveStatus, evoEE.ActiveStatus, sActiveStatus);
      if (extAppEE.ActiveStatus = 0) and (evoEE.ActiveStatus = 0) then
        CheckIfChanged(Result, extAppEE.ActiveStatusConditionID, evoEE.ActiveStatusConditionID, sActiveStatusConditionID)
      else if (extAppEE.ActiveStatus = 1) and (evoEE.ActiveStatus = 1) then
        CheckIfChanged(Result, extAppEE.InactiveStatusConditionID, evoEE.InactiveStatusConditionID, sInactiveStatusConditionID);
      CheckIfChanged(Result, extAppEE.BirthDate, evoEE.BirthDate, 'Birth date');
      CheckIfChanged(Result, extAppEE.AddressStateProv, evoEE.AddressStateProv, 'State');
      CheckIfChanged(Result, extAppEE.AddressCity, evoEE.AddressCity, 'City');
      CheckIfChanged(Result, extAppEE.AddressZIPPC, evoEE.AddressZIPPC, 'Zip code');
      CheckIfChanged(Result, extAppEE.Address1, evoEE.Address1, 'Address 1');
      CheckIfChanged(Result, extAppEE.Address2, evoEE.Address2, 'Address 2');
      CheckIfChanged(Result, DigitsOnly(extAppEE.Phone1), evoEE.Phone1, 'Phone 1');
      CheckIfChanged(Result, DigitsOnly(extAppEE.Phone2), evoEE.Phone2, 'Phone 2');

      if FOptions.ExportRates then
      begin
        CheckIfFloatChanged(Result, extAppEE.CurrentRate, evoEE.CurrentRate, sCurrenPayRate, 1e-2);
        CheckIfChanged(Result, extAppEE.PayTypeID, evoEE.PayTypeID, sPayType);
//        CheckIfChanged(Result, extAppEE.CurrentRateEffDate, evoEE.CurrentRateEffDate, 'Current pay rate effective date'); //see a comment in TExtAppEmployeeUpdater.Apply
      end;

      for i := 1 to MaxWorkgroupLevel do
      begin
        idKind := FDBDTMapper.GetWorkgroupIdentityKind(i);
        CheckIfChanged(Result, GetWGIdentityByNum(i, idKind, GetWG(extAppEE,i)), GetWGIdentityByNum(i, idKind, GetWG(evoEE,i)), WorkgroupLevel[i].Details.WGLevelName);
      end;

      CheckIfChanged(Result, extAppEE.EMAIL, EEData.EEs.FieldByName('E_MAIL_ADDRESS').AsString, 'E-mail');

      if FOptions.ExportStandardHours then
        CheckIfChanged(Result, extAppEE.AvgWeeklyHrs, evoEE.AvgWeeklyHrs, 'Standard hours/average weekly hours');

      CheckIfChanged(Result, extAppEE.Badge, evoEE.Badge, 'Badge number');

      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField1Code)) then
        CheckIfChanged(Result, extAppEE.StaticCustom1, evoEE.StaticCustom1, 'Static custom field 1');
      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField2Code)) then
        CheckIfChanged(Result, extAppEE.StaticCustom1, evoEE.StaticCustom2, 'Static custom field 2');
      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField3Code)) then
        CheckIfChanged(Result, extAppEE.StaticCustom1, evoEE.StaticCustom3, 'Static custom field 3');
      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField4Code)) then
        CheckIfChanged(Result, extAppEE.StaticCustom1, evoEE.StaticCustom4, 'Static custom field 4');
      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField5Code)) then
        CheckIfChanged(Result, extAppEE.StaticCustom1, evoEE.StaticCustom5, 'Static custom field 5');
      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppStaticCustomField6Code)) then
        CheckIfChanged(Result, extAppEE.StaticCustom1, evoEE.StaticCustom6, 'Static custom field 6');

      CheckIfChanged(Result, GetNameByID(FPayClasses, extAppEE.PayClassID, 'pay class'), GetNameByID(FPayClasses, evoEE.PayClassID, 'pay class'), sPayClass);

      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppHourlyStatusCode)) then
        CheckIfChanged(Result, GetNameByID(FHourlyStatusTypes, extAppEE.HourlyStatusID, 'hourly status type'), GetNameByID(FHourlyStatusTypes, evoEE.HourlyStatusID, 'hourly status type'), sHourlyStatus);

      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppClockGroupCode)) then
        CheckIfChanged(Result, GetNameByID(FClockGroups, extAppEE.ClockGroupID, 'clock group'), GetNameByID(FClockGroups, evoEE.ClockGroupID, 'clock group'), 'Clock group');

      if VarHasValue(FCustomFieldsMatcher.LeftMatch(sExtAppSchedulePatternCode)) then
        CheckIfChanged(Result, GetNameByID(FSchedulePatterns, extAppEE.SchPatternID, 'schedule pattern'), GetNameByID(FSchedulePatterns, evoEE.SchPatternID, 'schedule pattern'), 'Schedule pattern');
    finally
      FreeAndNil(evoEE);
    end;
  end;

var
  basicEEs: TAeEmployeeBasicAry;
  i: integer;
  changedFields: TChangedFieldRecs;
begin
  Result.Errors := 0;
  Result.Actions := CreateSyncActions(ExtAppName,'Employee');
  FLogger.LogEntry('Analyzing employee records');
  try
    try
      LogEEExportOptions(FLogger, FOptions);
      LogExtAppOptions(FLogger, FExtAppOptions);

      FPayClasses := FConn.getPayClassesSimple;
      FHourlyStatusTypes := FConn.getHourlyStatusTypesSimple;
      FClockGroups := FConn.getClockGroupsSimple;
      FSchedulePatterns := FConn.getSchedulePatternsSimple;

      if FExtAppOptions.Hyperquery <> NotSelected then
        basicEEs := FConn.getEmployeesListBasicFromHyperQuery(FExtAppOptions.Hyperquery)
      else
        basicEEs := FConn.getAllEmployeesList;

      try
        ValidateExtAppEECodes;

        if FExtAppOptions.Hyperquery <> NotSelected then
        begin
          FExtAppEEs := FConn.getEmployeesListDetail2FromHyperQuery(FExtAppOptions.Hyperquery);
          Assert(Length(basicEEs) = Length(FExtAppEEs));
        end
        else
          SetLength( FExtAppEEs, Length(basicEEs) );

        for i := 0 to high(basicEEs) do
        begin
          FLogger.LogEntry( Format('Analyzing %s employee record', [ExtAppName]));
          try
            try
              FLogger.LogContextItem('Name', basicEEs[i].EmpName);
              FLogger.LogContextItem(sCtxEECode, basicEEs[i].EmpID);
{$ifndef PRODUCTION_LICENCE}
//              if basicEEs[i].ActiveStatus = 1 then //!!for debug only
//                continue;
{$endif}
              if not ThisIsPrimaryCompany(FLogger, self, FDBDTMapper, FEEData.Company, basicEEs[i]) then
                continue;

              if FExtAppOptions.Hyperquery = NotSelected then
                FExtAppEEs[i] := FConn.getEmployeeDetail2ByFilekey(basicEEs[i].Filekey);

              if FEEData.LocateEE(basicEEs[i].EmpID) then
              begin
                if (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) or (basicEEs[i].ActiveStatus = 0{active}) or FExtAppOptions.TransferTerminatedEEs then
                  changedFields := CompareFields(FExtAppEEs[i], FEEData)
              end
              else
                changedFields := CompareFieldsForMissingEE(FExtAppEEs[i]);
              if Length(changedFields) > 0 then
                (Result.Actions as ISyncActions).Add(basicEEs[i].EmpID, euaUpdate, changedFields, i);
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end
        end;

        FEEData.EEs.First;
        while not FEEData.EEs.Eof do
        begin
          FLogger.LogEntry('Analyzing Evolution employee record');
          try
            try
              FLogger.LogContextItem(sCtxEECode, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
              if (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) or FExtAppOptions.TransferTerminatedEEs then
                if not IsInExtApp( basicEEs, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER'] ) then
                  (Result.Actions as ISyncActions).Add( FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER'], euaCreate, nil);
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          FEEData.EEs.Next;
        end;
      finally
        for i := 0 to high(basicEEs) do
          FreeAndNil( basicEEs[i] );
      end
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function ExtAppToday: string;
begin
  Result := DateToExtAppDate(DateOf(Now));
end;

function TExtAppEmployeeUpdater.Apply(actions: ISyncActionsRO; pi: IProgressIndicator): TUpdateEmployeesStat;
var
  evoEE: TAeEmployeeDetail;
  i: integer;
  status: variant;

begin
  Result := BuildEmptySyncStat;
  FLogger.LogEntry(Format('Synchronizing %s employee records', [ExtAppName]));
  try
    try
      pi.StartWait( Format('Synchronizing %s employee records', [ExtAppName]), actions.Count);
      try
        for i := 0 to actions.Count-1 do
        begin
          FLogger.LogEntry('Processing employee');
          try
            try
              FLogger.LogContextItem(sCtxEECode, actions.Get(i).Code);
              FLogger.LogContextItem('Action', SyncActionNames[actions.Get(i).Action]);
              case actions.Get(i).Action of
                euaCreate:
                  begin
                    if not FEEData.LocateEE(actions.Get(i).Code) then
                      Assert(false);
                    evoEE := TAeEmployeeDetail.Create;
                    try
                      BuildEE(evoEE, FEEData, true);
                      evoEE.CurrentRateEffDate := ExtAppToday;
                      evoEE.ActiveStatusConditionEffDate := ExtAppToday;
                      evoEE.HourlyStatusEffDate := ExtAppToday;
                      evoEE.PayClassEffDate := ExtAppToday;
                      evoEE.PayTypeEffDate := ExtAppToday;

                      FConn.AddEmployee(evoEE, FOptions.Badge = badgeNone);
                    finally
                      FreeAndNil(EvoEE);
                    end;
                    FConn.setEmployeeESSEMailByIDNum( actions.Get(i).Code, trim(FEEData.EEs.FieldByName('E_MAIL_ADDRESS').AsString) );
                    inc(Result.Created);
                  end;
                euaUpdate:
                  begin
                    if FEEData.LocateEE(actions.Get(i).Code) then
                    begin
                      BuildEE(FExtAppEEs[actions.Get(i).Tag], FEEData, false);
(*
John Ross:
The rate and rate effective date of today is only supposed to go across if there is a rate change...
If you send today's date with a rate that was changed back in June... picture what is going to happen.
The clock is going to look at the rate...which has been in effect since June... as effective today... all records for dates prior to today are going to pull the rate that was in effect immediately prior to the change that went in to effect back in June.
Example
June 26th ee gets a raise from 11.00 to 12.00
New rate has effective date of June 26
Now in Dec EVOAOD changes 12.00 rate effective date to Dec 5
All unposted records prior to Dec 5th will now display 11.00 as the rate because 12.00 does not take effect until Dec 5.
*)
                      if ExistsChangedField(actions.Get(i).ChangedFields, sCurrenPayRate) then //set it only the current rate will be changed           //sInactiveStatusConditionID
                        FExtAppEEs[actions.Get(i).Tag].CurrentRateEffDate := ExtAppToday;
                      if ExistsChangedField(actions.Get(i).ChangedFields, sActiveStatusConditionID) or ExistsChangedField(actions.Get(i).ChangedFields, sActiveStatus) then
                        FExtAppEEs[actions.Get(i).Tag].ActiveStatusConditionEffDate := ExtAppToday;
                      if ExistsChangedField(actions.Get(i).ChangedFields, sInactiveStatusConditionID) or ExistsChangedField(actions.Get(i).ChangedFields, sActiveStatus) then
                        if (FExtAppEEs[actions.Get(i).Tag].ActiveStatus = 1) {inactive} and FEEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').IsNull then
                          FExtAppEEs[actions.Get(i).Tag].ActiveStatusConditionEffDate := ExtAppToday;

                      if ExistsChangedField(actions.Get(i).ChangedFields, sHourlyStatus) then
                        FExtAppEEs[actions.Get(i).Tag].HourlyStatusEffDate := ExtAppToday;
                      if ExistsChangedField(actions.Get(i).ChangedFields, sPayClass) then
                        FExtAppEEs[actions.Get(i).Tag].PayClassEffDate := ExtAppToday;
                      if ExistsChangedField(actions.Get(i).ChangedFields, sPayType) then
                        FExtAppEEs[actions.Get(i).Tag].PayTypeEffDate := ExtAppToday;

                      FConn.UpdateEmployee( FExtAppEEs[actions.Get(i).Tag], FOptions.Badge = badgeNone );
                      FConn.setEmployeeESSEMailByFilekey( FExtAppEEs[actions.Get(i).Tag].FileKey, trim(FEEData.EEs.FieldByName('E_MAIL_ADDRESS').AsString) );
                    end
                    else
                    begin
                      status := MapEEStatus(EE_TERM_X_MISSING);
                      FExtAppEEs[actions.Get(i).Tag].ActiveStatus := status[0];
                      if status[0] = 0 then //active
                        FExtAppEEs[actions.Get(i).Tag].ActiveStatusConditionID := status[1]
                      else
                        FExtAppEEs[actions.Get(i).Tag].InactiveStatusConditionID := status[1];
                      FConn.UpdateEmployee( FExtAppEEs[actions.Get(i).Tag], FOptions.Badge = badgeNone);
                    end;
                    inc(Result.Modified);
                  end;
                euaDelete:
                  Assert(false);
              else
                Assert(false);
              end;
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          pi.Step(i+1);
        end;
      finally
        pi.EndWait;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

constructor TExtAppEmployeeUpdater.Create( conn: IExtAppConnection; const options: TEEExportOptions; const ExtAppOptions: TExtAppOptions; EEStatusMatcher: IMatcher; DBDTMApper: IDBDTMapper; CustomFieldsMatcher: IMatcher; logger: ICommonLogger; EEData: TEvoEEData);
begin
  FLogger := logger;
  FConn := conn;
  FOptions := options;
  FExtAppOptions := ExtAppOptions;

  if Trim(FExtAppOptions.Hyperquery) = '' then
    FExtAppOptions.Hyperquery := NotSelected;

  FEEData := EEData;
  FEEStatusMatcher := EEStatusMatcher;
  FDBDTMapper := DBDTMapper;
  FCustomFieldsMatcher := CustomFieldsMatcher;

  FLogger.LogDebug('DBDT mapping', FDBDTMapper.Dump);
  FLogger.LogDebug('EEStatus mapping', FEEStatusMatcher.Dump);
  FLogger.LogDebug('CustomFields mapping', FCustomFieldsMatcher.Dump);
  
  inherited Create(FConn, FLogger);
end;

destructor TExtAppEmployeeUpdater.Destroy;
var
  i: integer;
begin
  for i := 0 to high(FExtAppEEs) do
    FreeAndNil(FExtAppEEs[i]);
  inherited;
end;

function TExtAppEmployeeUpdater.GetIDByName(ary: TAeBasicDataItemsAry; name: Variant; typename: string): integer;
var
  i: integer;
begin
  name := trim(VarToStr(name));
  
  if name = '' then
  begin
    Result := 0;
    exit;
  end;

  for i := 0 to high(ary) do
    if AnsiSameText(trim(ary[i].NameLabel), name) then
    begin
      Result := ary[i].Num;
      exit;
    end;
  raise Exception.CreateFmt( 'Cannot find AoD %s with name <%s>', [typename, name] );
end;

function TExtAppEmployeeUpdater.GetNameByID(ary: TAeBasicDataItemsAry; id: integer; typename: string): string;
var
  i: integer;
begin
  if id = 0 then
  begin
    Result := '';
    exit;
  end;
  for i := 0 to high(ary) do
    if ary[i].Num = ID then
    begin
      Result := ary[i].NameLabel;
      exit;
    end;
  raise Exception.CreateFmt( 'Cannot find AoD %s with ID <%d>', [typename, id] );
end;

{ TWorkgroupCache }

constructor TWorkgroupCache.Create(conn: IExtAppConnection;
  logger: ICommonLogger);
begin
  FConn := conn;
  FLogger := logger;
  FMaxWorkgroupLevel := FConn.getBaseClientData.WorkgroupLevels;
end;

function TWorkgroupCache.GetWorkgroupLevel(i: integer): TWorkgroupLevelRec;
begin
  Assert( (i >= 1) and (i <= 7) );
  if not FWorkgroupLevels[i].Fetched then
  begin
    FWorkgroupLevels[i].Data := FConn.getWorkgroups(i, wgsName);
    FWorkgroupLevels[i].Details := FConn.getWorkgroupLevelDetails(i);
    FWorkgroupLevels[i].Fetched := true;
  end;
  Result := FWorkgroupLevels[i];
end;

function TWorkgroupCache.GetWGNumByIdentity(wglevel: integer; idKind: TWGIdentityKind; val: Variant): integer;
begin
  case idKind of
    wgCode: Result := _GetWGNumByCode(wglevel, VarToStr(val));
    wgName: Result := _GetWGNumByName(wglevel, VarToStr(val));
    wgNumber: Result := val;
  else
    Assert(false);
    Result := 0; // to make compiler happy
  end;
end;

function TWorkgroupCache._GetWGNumByCode(wglevel: integer; code: string): integer;
var
  i: integer;
  wg: TAeWorkgroupAry;
begin
  code := trim(code);
  if code = '' then
    code := 'Payroll';

  wg := WorkgroupLevel[wglevel].Data;

  for i := 0 to high(wg) do
    if AnsiSameText(trim(wg[i].WGCode), code) then
    begin
      Result := wg[i].WGNum;
      exit;
    end;
  raise Exception.CreateFmt( 'Cannot find AoD %s with code <%s>', [WorkgroupLevel[wglevel].Details.WGLevelName, code] );
end;

function TWorkgroupCache._GetWGNumByName(wglevel: integer; name: string): integer;
var
  i: integer;
  wg: TAeWorkgroupAry;
begin
  name := trim(name);
  if name = '' then
    name := 'Payroll';

  wg := WorkgroupLevel[wglevel].Data;

  for i := 0 to high(wg) do
    if AnsiSameText(trim(wg[i].WGName), name) then
    begin
      Result := wg[i].WGNum;
      exit;
    end;
  raise Exception.CreateFmt( 'Cannot find AoD %s with name <%s>', [WorkgroupLevel[wglevel].Details.WGLevelName, name] );
end;

function TWorkgroupCache.GetWGIdentityByNum(wglevel: integer; idKind: TWGIdentityKind; num: integer): string;
var
  i: integer;
  wg: TAeWorkgroupAry;
begin
  if num = 0 then
  begin
    Result := '';
    exit;
  end;

  wg := WorkgroupLevel[wglevel].Data;

  for i := 0 to high(wg) do
    if wg[i].WGNum = num then
    begin
      case idKind of
        wgCode: Result := wg[i].WGCode;
        wgName: Result := wg[i].WGName;
        wgNumber: Result := IntToStr(wg[i].WGNum);
      else
        Assert(false);
      end;
      exit;
    end;

  raise Exception.CreateFmt( 'Cannot find AoD %s with internal number <%d>', [WorkgroupLevel[wglevel].Details.WGLevelName, num] );
end;

function ExportTOA(conn: IExtAppConnection; evoConn: IEvoAPIConnection; TOAMatcher: IMatcher; ExtAppOptions: TExtAppOptions; company: TEvoCompanyDef; logger: ICommonLogger; pi: IProgressIndicator): TTOAStat;
var
  eecode: string;
  TOA: TkbmCustomMemTable;
  benefitId: Variant;
  dt: TDateTime;
  accrued, used: double;
  period: TAePayPeriodFrame;
//  newBalance: double;
begin
  Result := BuildEmptyTOAStat;
  Logger.LogEntry(Format('Updating %s TOA records', [ExtAppName]));
  try
    try
		  Logger.LogDebug('TOA mapping', TOAMatcher.Dump);
      LogExtAppOptions(Logger, ExtAppOptions);
      TOA := ExecCoQuery(evoConn, Logger, Company, 'CUSTOM_EE_TIME_OFF_ACCRUAL.rwq', 'Getting TOA records from Evolution');
      try
        pi.StartWait( Format('Updating %s TOA records', [ExtAppName]), TOA.RecordCount);
        try
          TOA.First;
          while not TOA.Eof do
          begin
            Logger.LogEntry('Processing TOA record');
            try
              try
                eecode := trim(TOA['CUSTOM_EMPLOYEE_NUMBER']);
                Logger.LogContextItem(sCtxEECode, eecode);
                Logger.LogContextItem('Evolution TOA pay type', TOA.FieldByName('DESCRIPTION').AsString);
                Logger.LogContextItem('Last accrual change date', DateToStdDate(TOA.FieldByName('MAX_ACCRUAL_DATE').AsDateTime));

                if not TOA.FieldByName('MAX_ACCRUAL_DATE').IsNull and
                   (ExtAppOptions.TransferInactiveTOA or (TOA['STATUS'] = GROUP_BOX_YES)) and
                   (ExtAppOptions.TransferTerminatedEEs or (TOA['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE))
                   then
                begin
                  benefitId := TOAMatcher.RightMatch( trim(TOA.FieldByName('DESCRIPTION').AsString) );
                  if VarIsNull(benefitId) then
                    raise Exception.CreateFmt('Evolution TOA pay type <%s> is not mapped to an %s benefit type', [TOA.FieldByName('DESCRIPTION').AsString, ExtAppName]);

                  accrued := TOA.FieldByName('REAL_CURRENT_ACCRUED').AsFloat;
                  used := TOA.FieldByName('REAL_CURRENT_USED').AsFloat;

                  period := conn.extractEmployeePeriodFrameByIDNum(eecode);
                  try
                    Logger.LogContextItem('Current period start date', period.CurrStart);
                    dt := StdDateToDate(period.CurrStart);
                  finally
                    FreeAndNil(period);
                  end;
                  conn.adjustEmployeeBenefitBalanceByIDNum(eecode, benefitId, dt, batSetBalance, accrued-used);

                  {
                  conn.adjustEmployeeBenefitBalanceByIDNum(trim(TOA['CUSTOM_EMPLOYEE_NUMBER']), benefitId, dt, batSetBalance, 0);
                  if accrued <> 0 then
                  begin
                    conn.adjustEmployeeBenefitBalanceByIDNum(trim(TOA['CUSTOM_EMPLOYEE_NUMBER']), benefitId, dt, batCredit, accrued);
                  end;
                  if used <> 0 then
                  begin
                    conn.adjustEmployeeBenefitBalanceByIDNum(trim(TOA['CUSTOM_EMPLOYEE_NUMBER']), benefitId, dt, batDebit, used);
                  end;
                  newBalance := conn.employeeBenefitBalanceAsOfByIDNum( trim(TOA['CUSTOM_EMPLOYEE_NUMBER']), benefitId, dt);
                  if abs(newBalance - (accrued-used)) > 1e-2 then
                    raise Exception.CreateFmt('Failed to set benefit balance for "%s" (Evolution''s name) as of %s', [TOA.FieldByName('DESCRIPTION').AsString, DateToStdDate(dt)]);
                  Logger.LogEventFmt('Balance set to %f', [accrued-used]);
                  }
	                inc(Result.Updated);
                end;
              except
                inc(Result.Errors);
                Logger.StopException;
              end;
            finally
              Logger.LogExit;
            end;
            TOA.Next;
            pi.Step(TOA.RecNo+1);
          end;
        finally
          pi.EndWait;
        end;
      finally
        FreeAndNil(TOA);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

type
  TNullableTOADataRec = record
    HasValue: boolean;
    Value: TTOADataRec;
  end;

function GetTOAData(conn: IExtAppConnection; evoConn: IEvoAPIConnection; TOAMatcher: IMatcher; DBDTMapper: IDBDTMapper; company: TEvoCompanyDef; logger: ICommonLogger; pi: IProgressIndicator): TTOAImportData;

  function GetSingleTOA(Filekey: integer; benefit: TAeBasicDataItem): TNullableTOADataRec;
  var
    activities: TAeEmpBenefitActivityAry;
    k: integer;
  begin
    Result.HasValue := false;
    activities := conn.extractEmployeeBenefitActivityByFilekey(Filekey, benefit.Num, EncodeDate(1900,1,1), IncDay(DateOf(Now)));  //tomorrow!
//Bill from AoD:
//What I would do in this instance is this:
//For setting the balance on your side to what we currently show for a benefit, use the employeeBenefitBalanceAsOfByFilekey method specifying tomorrow as the date to get our current balance.
    try
      if Length(activities) > 0 then
      begin

        Result.HasValue := true;
        Result.Value.Accrued := 0;
        Result.Value.Used := 0;
        for k := 0 to high(activities) do
        begin
          if not activities[k].IsHours then
            raise Exception.CreateFmt('Benefit %s isn''t TOA (IsHours = false)', [benefit.NameLabel]);

          if (activities[k].CreditHoursHund = 0) and (activities[k].DebitHoursHund = 0) then
          begin
            Result.Value.Accrued := 0;
            Result.Value.Used := 0;
            if activities[k].BalanceHoursHund > 0 then
              Result.Value.Accrued := activities[k].BalanceHoursHund
            else
              Result.Value.Used := -activities[k].BalanceHoursHund;
{
//we can have such a record, but then BalanceHours will be equeal to the current balance so it is OK
            if SameText(trim(activities[k].Category), 'Credit Balance') then
              raise Exception.Create('Unexpected benefit activity record encountered: Category is ''Credit Balance'' but CreditHours is 0. Please send the debug log to the developer.');
            if SameText(trim(activities[k].Category), 'Debit Balance') then
              raise Exception.Create('Unexpected benefit activity record encountered: Category is ''Debit Balance'' but DebitHours is 0. Please send the debug log to the developer.');
}
          end
          else
          begin
            Result.Value.Accrued := Result.Value.Accrued + activities[k].CreditHoursHund;
            Result.Value.Used := Result.Value.Used + activities[k].DebitHoursHund;
          end;
        end;
      end;
    finally
      for k := 0 to high(activities) do
        FreeAndNil(activities[k]);
    end;
  end;

var
  TOA: TkbmCustomMemTable;

  function FigureEvoTOAType(eecode: string; benefit: TAeBasicDataItem): Variant;
  var
    v: Variant;
    i: integer;
  begin
    Result := Null;
    v := TOAMatcher.LeftMatch(benefit.Num);
    if not VarIsNull(v) then
    begin
      if VarIsArray(v) then
      begin
        TOA.First;
        while not TOA.Eof do
        begin
          if AnsiSameText( trim(TOA['CUSTOM_EMPLOYEE_NUMBER']), trim(eecode) ) then
          begin
            for i := VarArrayLowBound(v,1) to VarArrayHighBound(v,1) do
              if AnsiSameText( trim(TOA['DESCRIPTION']), trim(v[i]) ) then
                if VarIsNull(Result) then
                begin
                  Result := v[i];
                  break;
                end
                else
                begin
                  if not AnsiSameText(trim(Result), trim(v[i])) then //else it is Evo's problem
                    raise Exception.CreateFmt('Cannot uniquely identify employee''s Evolution TOA type for AoD benefit <%s>. Both <%s> and <%s> are mapped to this AoD benefit', [benefit.NameLabel, VarToStr(Result), VarToStr(v[i])]);
                end;
          end;
          TOA.Next;
        end;
        if VarIsNull(Result) then
          raise Exception.CreateFmt('None of the TOA types mapped to the AoD benefit <%s> are specified for this employee', [benefit.NameLabel]);
      end
      else
        Result := v;
    end
  end;
var
  wgs: TWorkgroupCache;
  extAppEEs: TAeEmployeeBasicAry;
  i: integer;
  j: integer;
  benefits: TAeBasicDataItemsAry;
  curEETOA: ^TEETOADataRec;
  thisTOA: TNullableTOADataRec;
  evoTOAType: Variant;
begin
  SetLength(Result.Recs, 0);
  Result.Errors := 0;

  logger.LogEntry( Format('Getting %s TOA records', [ExtAppName]) );
  try
    try
		  Logger.LogDebug('TOA mapping', TOAMatcher.Dump);
      TOA := ExecCoQuery(evoConn, Logger, Company, 'EE_TIME_OFF_ACCRUAL.rwq', 'Getting TOA records from Evolution');
      try
        wgs := TWorkgroupCache.Create(conn, logger);
        try
          benefits := conn.getBenefitsSimple;
          try
            extAppEEs := conn.getAllEmployeesList;
            try
              pi.StartWait( Format('Getting %s TOA records', [ExtAppName]), Length(extAppEEs)*Length(benefits) );
              try
                for i := 0 to high(extAppEEs) do
                begin
                  curEETOA := nil;
                  logger.LogEntry('Getting employee''s TOA records');
                  try
                    try
  {$ifndef PRODUCTION_LICENCE}
                      if extAppEEs[i].ActiveStatus = 1 then //!!for debug only
                        continue;
  {$endif}
                      logger.LogContextItem(sCtxEECode, extAppEEs[i].EmpID);
                      logger.LogContextItem('Name', extAppEEs[i].EmpName);

                      if not ThisIsPrimaryCompany(logger, wgs, DBDTMapper, company, extAppEEs[i]) then
                        continue;

                      for j := 0 to high(benefits) do
                      begin
                        logger.LogEntry('Getting employee''s TOA record');
                        try
                          try
                            logger.LogContextItem('Benefit', benefits[j].NameLabel);

                            evoTOAType := FigureEvoTOAType(extAppEEs[i].EmpID, benefits[j]);
                            if not VarIsNull(evoTOAType) then
                            begin
                              thisTOA := GetSingleTOA(extAppEEs[i].Filekey, benefits[j]);
                              if thisTOA.HasValue then
                              begin
                                if curEETOA = nil then
                                begin
                                  SetLength(Result.Recs, Length(Result.Recs)+1);
                                  curEETOA := @Result.Recs[high(Result.Recs)];
                                  curEETOA.EECode := trim(extAppEEs[i].EmpID);
                                end;
                                SetLength(curEETOA.Data, Length(curEETOA.Data)+1);
                                curEETOA.Data[high(curEETOA.Data)] := thisTOA.Value;
                                curEETOA.Data[high(curEETOA.Data)].TypeDescription := evoTOAType; //!!may be a problem - it's been trimmed when put to the mapping table
                              end;
                            end;

                          except
                            Result.Errors := Result.Errors + 1;
                            logger.StopException;
                          end;
                        finally
                          logger.LogExit;
                        end;
                        pi.Step(i*Length(benefits) + j + 1);
                      end; //end for

                    except
                      Result.Errors := Result.Errors + 1;
                      logger.StopException;
                    end;
                  finally
                    logger.LogExit;
                  end;
                end;
              finally
                pi.EndWait;
              end;
            finally
              for i := 0 to high(extAppEEs) do
                FreeAndNil(extAppEEs[i]);
            end;
          finally
            for j := 0 to high(benefits) do
              FreeAndNil(benefits[j]);
          end;
        finally
          FreeAndNil(wgs);
        end
      finally
        FreeAndNil(TOA);
      end
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;


end.
