// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\job\IS\integration\EvoAoD\xml\IAeXMLBridge.wsdl
// Encoding : UTF-8
// Version  : 1.0
// (3/22/2010 11:37:22 AM - 1.33.2.5)
// ************************************************************************ //

unit IAeXMLBridge1;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"
  // !:double          - "http://www.w3.org/2001/XMLSchema"

  TAeSiteEnvironment   = class;                 { "urn:AeXMLBridgeIntf" }
  TAeClientData        = class;                 { "urn:AeXMLBridgeIntf" }
  TAeAccessAccount     = class;                 { "urn:AeXMLBridgeIntf" }
  TAeClientReport      = class;                 { "urn:AeXMLBridgeIntf" }
  TAeClientExport      = class;                 { "urn:AeXMLBridgeIntf" }
  TAeClientVendorOp    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeBasicDataItem     = class;                 { "urn:AeXMLBridgeIntf" }
  TAeBrowserMenuItem   = class;                 { "urn:AeXMLBridgeIntf" }
  TAePayPeriodFrame    = class;                 { "urn:AeXMLBridgeIntf" }
  TAePayPeriodInfo     = class;                 { "urn:AeXMLBridgeIntf" }
  TAeEmployeeBasic     = class;                 { "urn:AeXMLBridgeIntf" }
  TAeEmployeeDetail    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeEmployeeDetail2   = class;                 { "urn:AeXMLBridgeIntf" }
  TAePayLine           = class;                 { "urn:AeXMLBridgeIntf" }
  TAeShiftLineSimple   = class;                 { "urn:AeXMLBridgeIntf" }
  TAeSchedule          = class;                 { "urn:AeXMLBridgeIntf" }
  TAeEdit              = class;                 { "urn:AeXMLBridgeIntf" }
  TAeEmpTransaction    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeEmpBenefitActivity = class;                { "urn:AeXMLBridgeIntf" }
  TAeTimeOffRequest    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeWorkgroupAssignment = class;               { "urn:AeXMLBridgeIntf" }
  TAeRatePayTypeAssignment = class;             { "urn:AeXMLBridgeIntf" }
  TAeWorkgroupLevel    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeWorkgroup         = class;                 { "urn:AeXMLBridgeIntf" }
  TAeTransaction       = class;                 { "urn:AeXMLBridgeIntf" }
  TAeClientVendorOpResults = class;             { "urn:AeXMLBridgeIntf" }
  TAePayPeriodCloseInfo = class;                { "urn:AeXMLBridgeIntf" }
  TAeWorkgroupSet      = class;                 { "urn:AeXMLBridgeIntf" }
  TAeRateAssignment    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeUsageItem         = class;                 { "urn:AeXMLBridgeIntf" }
  TAeAccessAccountActivity = class;             { "urn:AeXMLBridgeIntf" }
  TAeTimeClockStation  = class;                 { "urn:AeXMLBridgeIntf" }
  TAeBillingItem       = class;                 { "urn:AeXMLBridgeIntf" }
  TAeShiftLineDetail   = class;                 { "urn:AeXMLBridgeIntf" }
  TAeShiftTransaction  = class;                 { "urn:AeXMLBridgeIntf" }
  TAeShiftSchedule     = class;                 { "urn:AeXMLBridgeIntf" }
  TAeTimeOffRequestState = class;               { "urn:AeXMLBridgeIntf" }
  TAeExchangeStruct    = class;                 { "urn:AeXMLBridgeIntf" }
  TAeWorkgroupXLate    = class;                 { "urn:AeXMLBridgeIntf" }
  TSessionHeader       = class;                 { "urn:AeXMLBridgeIntf"[H] }

  { "urn:AeXMLBridgeIntf" }
  TBrowserProfileTypeEnum = (bptPrivate, bptSystem);

  { "urn:AeXMLBridgeIntf" }
  TDateRangeEnum = (
      drCustom, 
      drPrevPeriod, 
      drCurrPeriod, 
      drNextPeriod, 
      drYesterday, 
      drToday, 
      drTomorrow, 
      drPrevWeek, 
      drCurrWeek, 
      drNextWeek, 
      drPrevBiWeek, 
      drCurrBiWeek, 
      drNextBiWeek, 
      drPrevMonth, 
      drCurrMonth, 
      drNextMonth, 
      drPrevYear, 
      drCurrYear, 
      drNextYear, 
      drFMLAPeriod, 
      drPrevCurr, 
      drWeekBeforeLast, 
      drTwoDaysAgo, 
      drThreeDaysAgo, 
      drFourDaysAgo, 
      drFiveDaysAgo, 
      drSixDaysAgo, 
      drLastTwoDays, 
      drLastThreeDays, 
      drLastFourDays, 
      drLastFiveDays, 
      drLastSixDays, 
      drNextWeekend, 
      drLastWeekend, 
      drTwoWeekendsAgo, 
      drThreeWeekendsAgo, 
      drThisWeekWeekdays, 
      drLastWeekWeekdays, 
      drNextWeekWeekdays, 
      drThisMonday, 
      drThisTuesday, 
      drThisWednesday, 
      drThisThursday, 
      drThisFriday, 
      drThisSaturday, 
      drThisSunday, 
      drNextMonday, 
      drNextTuesday, 
      drNextWednesday, 
      drNextThursday, 
      drNextFriday, 
      drNextSaturday, 
      drNextSunday, 
      drLastMonday, 
      drLastTuesday, 
      drLastWednesday, 
      drLastThursday, 
      drLastFriday, 
      drLastSaturday, 
      drLastSunday, 
      drPrevCurrYear, 
      drFMLAEndOfCurrPeriod, 
      drFMLAEndOfNextPeriod, 
      drFMLAEndOfCurrMonth, 
      drFMLAEndOfNextMonth, 
      drLastSevenDays, 
      drLastEightDays, 
      drLastNineDays, 
      drLastTenDays, 
      drPrevCurrMonth, 
      drCurrNextMonth, 
      drThreeWeeksAgo, 
      drePrevEmploymentYear, 
      dreCurrEmploymentYear, 
      dreNextEmploymentYear, 
      drePrevBirthYear, 
      dreCurrBirthYear, 
      dreNextBirthYear, 
      dreDurationOfEmployment
);

  { "urn:AeXMLBridgeIntf" }
  TDayOfWeekEnum = (dowSunday, dowMonday, dowTuesday, dowWednesday, dowThursday, dowFriday, dowSaturday);

  { "urn:AeXMLBridgeIntf" }
  TDestinationOptionsEnum = (doDirect, doEmail);

  { "urn:AeXMLBridgeIntf" }
  TFileConversionOptionsEnum = (fcoInHex, fcoNative);

  { "urn:AeXMLBridgeIntf" }
  TFileFormatEnum = (ffDefault, ffCSV, ffXMLSS);

  { "urn:AeXMLBridgeIntf" }
  TSchTypeEnum = (steNormal, steFlex, steAbsPlnBen, steAbsPlnPayDes);

  { "urn:AeXMLBridgeIntf" }
  TRequestTimeOffTypeEnum = (rtoDays, rtoPartialDay, rtoOneDay, rtoExtended);

  { "urn:AeXMLBridgeIntf" }
  TRTOShiftPartTypeEnum = (rspBeginning, rspEnding, rspMiddle);

  { "urn:AeXMLBridgeIntf" }
  TAddEmpMode = (aemAuto, aemAdd, aemEdit);

  { "urn:AeXMLBridgeIntf" }
  TBadgeManagement = (bmAuto, bmManual);

  { "urn:AeXMLBridgeIntf" }
  TMaintainEmployeeResult = (merEditOk, merAddOk, merAddedWithErrors, merBadParam, merDupID, merDupBadge, merCap, merUnknown);

  { "urn:AeXMLBridgeIntf" }
  TPayPeriodEnum = (ppePrevious, ppeCurrent, ppeNext);

  { "urn:AeXMLBridgeIntf" }
  TWGSortingOption = (wgsNum, wgsCode, wgsName);

  { "urn:AeXMLBridgeIntf" }
  TPayLineStateEnum = (plsCalculated, plsAsSaved);

  { "urn:AeXMLBridgeIntf" }
  TCalcedDataTypeEnum = (cdtNormal, cdtScheduled, cdtEstimated);

  { "urn:AeXMLBridgeIntf" }
  TNoActivityInclusion = (naiSkip, naiInclude);

  { "urn:AeXMLBridgeIntf" }
  TBenAdjTypeEnum = (batCredit, batDebit, batSetBalance);

  { "urn:AeXMLBridgeIntf" }
  TAuthTimeCardOptionEnum = (atcoSupervisor, atcoSupervisorLock, atcoEmployee);

  { "urn:AeXMLBridgeIntf" }
  TRTOUsageStateEnum = (rusSubmitted, rusPending, rusClosed);

  { "urn:AeXMLBridgeIntf" }
  TActiveStatusEnum = (asActive, asInactive, asAll);

  { "urn:AeXMLBridgeIntf" }
  TRTOFilterEnum = (rfState, rfCreated, rfRequested, rfStateCreated);

  { "urn:AeXMLBridgeIntf" }
  TDurationDetailEnum = (ddePayDes, ddePayDesWG, ddePayDesWGRate, ddeWGOnly);



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeSiteEnvironment = class(TRemotable)
  private
    FMajorVer: Integer;
    FMinorVer: Integer;
    FBuildNumber: Integer;
    FComputerName: WideString;
    FServerDateTime: WideString;
    FClientDateTime: WideString;
    FSiteNum: Integer;
  published
    property MajorVer: Integer read FMajorVer write FMajorVer;
    property MinorVer: Integer read FMinorVer write FMinorVer;
    property BuildNumber: Integer read FBuildNumber write FBuildNumber;
    property ComputerName: WideString read FComputerName write FComputerName;
    property ServerDateTime: WideString read FServerDateTime write FServerDateTime;
    property ClientDateTime: WideString read FClientDateTime write FClientDateTime;
    property SiteNum: Integer read FSiteNum write FSiteNum;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeClientData = class(TRemotable)
  private
    FCompanyName: WideString;
    FCompanyPrimaryPhone: WideString;
    FCompanyPrimaryFAX: WideString;
    FPrimaryContactName: WideString;
    FPrimaryContactPrimaryPhone: WideString;
    FCompanyAddr1: WideString;
    FCompanyAddr2: WideString;
    FCompanyCity: WideString;
    FCompanyStateProv: WideString;
    FCompanyZIPPC: WideString;
    FTotalEmployees: Integer;
    FActiveEmployees: Integer;
    FInactiveEmployees: Integer;
    FPayPeriodClasses: Integer;
    FWorkgroupLevels: Integer;
    FDemoMode: Boolean;
    FProductionMode: Boolean;
  published
    property CompanyName: WideString read FCompanyName write FCompanyName;
    property CompanyPrimaryPhone: WideString read FCompanyPrimaryPhone write FCompanyPrimaryPhone;
    property CompanyPrimaryFAX: WideString read FCompanyPrimaryFAX write FCompanyPrimaryFAX;
    property PrimaryContactName: WideString read FPrimaryContactName write FPrimaryContactName;
    property PrimaryContactPrimaryPhone: WideString read FPrimaryContactPrimaryPhone write FPrimaryContactPrimaryPhone;
    property CompanyAddr1: WideString read FCompanyAddr1 write FCompanyAddr1;
    property CompanyAddr2: WideString read FCompanyAddr2 write FCompanyAddr2;
    property CompanyCity: WideString read FCompanyCity write FCompanyCity;
    property CompanyStateProv: WideString read FCompanyStateProv write FCompanyStateProv;
    property CompanyZIPPC: WideString read FCompanyZIPPC write FCompanyZIPPC;
    property TotalEmployees: Integer read FTotalEmployees write FTotalEmployees;
    property ActiveEmployees: Integer read FActiveEmployees write FActiveEmployees;
    property InactiveEmployees: Integer read FInactiveEmployees write FInactiveEmployees;
    property PayPeriodClasses: Integer read FPayPeriodClasses write FPayPeriodClasses;
    property WorkgroupLevels: Integer read FWorkgroupLevels write FWorkgroupLevels;
    property DemoMode: Boolean read FDemoMode write FDemoMode;
    property ProductionMode: Boolean read FProductionMode write FProductionMode;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeAccessAccount = class(TRemotable)
  private
    FBrowserProfileType: TBrowserProfileTypeEnum;
    FDomainName: WideString;
    FAccountCode: WideString;
    FFriendlyName: WideString;
    FPassword: WideString;
    FBrowserProfile: WideString;
    FeMailAddress: WideString;
    FAccountType: Integer;
    FUserGroupID: Integer;
    FUniqueID: Integer;
  published
    property BrowserProfileType: TBrowserProfileTypeEnum read FBrowserProfileType write FBrowserProfileType;
    property DomainName: WideString read FDomainName write FDomainName;
    property AccountCode: WideString read FAccountCode write FAccountCode;
    property FriendlyName: WideString read FFriendlyName write FFriendlyName;
    property Password: WideString read FPassword write FPassword;
    property BrowserProfile: WideString read FBrowserProfile write FBrowserProfile;
    property eMailAddress: WideString read FeMailAddress write FeMailAddress;
    property AccountType: Integer read FAccountType write FAccountType;
    property UserGroupID: Integer read FUserGroupID write FUserGroupID;
    property UniqueID: Integer read FUniqueID write FUniqueID;
  end;

  TAeAccessAccountAry = array of TAeAccessAccount;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeClientReport = class(TRemotable)
  private
    FApplyOverrides: Boolean;
    FDateRange: TDateRangeEnum;
    FMinDate: WideString;
    FMaxDate: WideString;
    FWeekBegins: TDayOfWeekEnum;
    FBiWeekBegins: WideString;
    FPayPerClass: Integer;
    FReportTitle: WideString;
    FReportGroup: WideString;
    FReportHint: WideString;
    FeMailRecipients: WideString;
    FeMailSender: WideString;
    FeMailBodyText: WideString;
    FeMailSubjectLine: WideString;
    FDestinationOptionsEnum: TDestinationOptionsEnum;
    FFileConversionOptionsEnum: TFileConversionOptionsEnum;
  published
    property ApplyOverrides: Boolean read FApplyOverrides write FApplyOverrides;
    property DateRange: TDateRangeEnum read FDateRange write FDateRange;
    property MinDate: WideString read FMinDate write FMinDate;
    property MaxDate: WideString read FMaxDate write FMaxDate;
    property WeekBegins: TDayOfWeekEnum read FWeekBegins write FWeekBegins;
    property BiWeekBegins: WideString read FBiWeekBegins write FBiWeekBegins;
    property PayPerClass: Integer read FPayPerClass write FPayPerClass;
    property ReportTitle: WideString read FReportTitle write FReportTitle;
    property ReportGroup: WideString read FReportGroup write FReportGroup;
    property ReportHint: WideString read FReportHint write FReportHint;
    property eMailRecipients: WideString read FeMailRecipients write FeMailRecipients;
    property eMailSender: WideString read FeMailSender write FeMailSender;
    property eMailBodyText: WideString read FeMailBodyText write FeMailBodyText;
    property eMailSubjectLine: WideString read FeMailSubjectLine write FeMailSubjectLine;
    property DestinationOptionsEnum: TDestinationOptionsEnum read FDestinationOptionsEnum write FDestinationOptionsEnum;
    property FileConversionOptionsEnum: TFileConversionOptionsEnum read FFileConversionOptionsEnum write FFileConversionOptionsEnum;
  end;

  TAeClientReportAry = array of TAeClientReport;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeClientExport = class(TRemotable)
  private
    FApplyOverrides: Boolean;
    FDateRange: TDateRangeEnum;
    FMinDate: WideString;
    FMaxDate: WideString;
    FWeekBegins: TDayOfWeekEnum;
    FBiWeekBegins: WideString;
    FPayPerClass: Integer;
    FExportTitle: WideString;
    FExportGroup: WideString;
    FExportHint: WideString;
    FeMailRecipients: WideString;
    FeMailSender: WideString;
    FeMailBodyText: WideString;
    FeMailSubjectLine: WideString;
    FDestinationOptionsEnum: TDestinationOptionsEnum;
    FFileFormatEnum: TFileFormatEnum;
    FFileConversionOptionsEnum: TFileConversionOptionsEnum;
  published
    property ApplyOverrides: Boolean read FApplyOverrides write FApplyOverrides;
    property DateRange: TDateRangeEnum read FDateRange write FDateRange;
    property MinDate: WideString read FMinDate write FMinDate;
    property MaxDate: WideString read FMaxDate write FMaxDate;
    property WeekBegins: TDayOfWeekEnum read FWeekBegins write FWeekBegins;
    property BiWeekBegins: WideString read FBiWeekBegins write FBiWeekBegins;
    property PayPerClass: Integer read FPayPerClass write FPayPerClass;
    property ExportTitle: WideString read FExportTitle write FExportTitle;
    property ExportGroup: WideString read FExportGroup write FExportGroup;
    property ExportHint: WideString read FExportHint write FExportHint;
    property eMailRecipients: WideString read FeMailRecipients write FeMailRecipients;
    property eMailSender: WideString read FeMailSender write FeMailSender;
    property eMailBodyText: WideString read FeMailBodyText write FeMailBodyText;
    property eMailSubjectLine: WideString read FeMailSubjectLine write FeMailSubjectLine;
    property DestinationOptionsEnum: TDestinationOptionsEnum read FDestinationOptionsEnum write FDestinationOptionsEnum;
    property FileFormatEnum: TFileFormatEnum read FFileFormatEnum write FFileFormatEnum;
    property FileConversionOptionsEnum: TFileConversionOptionsEnum read FFileConversionOptionsEnum write FFileConversionOptionsEnum;
  end;

  TAeClientExportAry = array of TAeClientExport;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeClientVendorOp = class(TRemotable)
  private
    FApplyOverrides: Boolean;
    FDateRange: TDateRangeEnum;
    FMinDate: WideString;
    FMaxDate: WideString;
    FWeekBegins: TDayOfWeekEnum;
    FBiWeekBegins: WideString;
    FPayPerClass: Integer;
    FVendorOpName: WideString;
    FVendorOpGroup: WideString;
    FVendorOpHint: WideString;
    FeMailRecipients: WideString;
    FeMailSender: WideString;
    FeMailBodyText: WideString;
    FeMailSubjectLine: WideString;
    FDestinationOptionsEnum: TDestinationOptionsEnum;
  published
    property ApplyOverrides: Boolean read FApplyOverrides write FApplyOverrides;
    property DateRange: TDateRangeEnum read FDateRange write FDateRange;
    property MinDate: WideString read FMinDate write FMinDate;
    property MaxDate: WideString read FMaxDate write FMaxDate;
    property WeekBegins: TDayOfWeekEnum read FWeekBegins write FWeekBegins;
    property BiWeekBegins: WideString read FBiWeekBegins write FBiWeekBegins;
    property PayPerClass: Integer read FPayPerClass write FPayPerClass;
    property VendorOpName: WideString read FVendorOpName write FVendorOpName;
    property VendorOpGroup: WideString read FVendorOpGroup write FVendorOpGroup;
    property VendorOpHint: WideString read FVendorOpHint write FVendorOpHint;
    property eMailRecipients: WideString read FeMailRecipients write FeMailRecipients;
    property eMailSender: WideString read FeMailSender write FeMailSender;
    property eMailBodyText: WideString read FeMailBodyText write FeMailBodyText;
    property eMailSubjectLine: WideString read FeMailSubjectLine write FeMailSubjectLine;
    property DestinationOptionsEnum: TDestinationOptionsEnum read FDestinationOptionsEnum write FDestinationOptionsEnum;
  end;

  TAeClientVendorOpAry = array of TAeClientVendorOp;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeBasicDataItem = class(TRemotable)
  private
    FNameLabel: WideString;
    FNum: Integer;
  published
    property NameLabel: WideString read FNameLabel write FNameLabel;
    property Num: Integer read FNum write FNum;
  end;

  TAeBasicDataItemsAry = array of TAeBasicDataItem;   { "urn:AeXMLBridgeIntf" }
  TStringArray = array of WideString;           { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeBrowserMenuItem = class(TRemotable)
  private
    FMenuCaption: WideString;
    FMenuHint: WideString;
    FMenuGUID: WideString;
    FMenuGroup: WideString;
  published
    property MenuCaption: WideString read FMenuCaption write FMenuCaption;
    property MenuHint: WideString read FMenuHint write FMenuHint;
    property MenuGUID: WideString read FMenuGUID write FMenuGUID;
    property MenuGroup: WideString read FMenuGroup write FMenuGroup;
  end;

  TAeBrowserMenuItemsAry = array of TAeBrowserMenuItem;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAePayPeriodFrame = class(TRemotable)
  private
    FPrevStart: WideString;
    FPrevEnd: WideString;
    FCurrStart: WideString;
    FCurrEnd: WideString;
    FNextStart: WideString;
    FNextEnd: WideString;
    FNextPost: WideString;
  published
    property PrevStart: WideString read FPrevStart write FPrevStart;
    property PrevEnd: WideString read FPrevEnd write FPrevEnd;
    property CurrStart: WideString read FCurrStart write FCurrStart;
    property CurrEnd: WideString read FCurrEnd write FCurrEnd;
    property NextStart: WideString read FNextStart write FNextStart;
    property NextEnd: WideString read FNextEnd write FNextEnd;
    property NextPost: WideString read FNextPost write FNextPost;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAePayPeriodInfo = class(TAePayPeriodFrame)
  private
    FPayPeriodClassNum: Integer;
    FPayPeriodClassName: WideString;
    FActiveEmployees: Integer;
    FInactiveEmployees: Integer;
    FPayPeriodLength: Integer;
  published
    property PayPeriodClassNum: Integer read FPayPeriodClassNum write FPayPeriodClassNum;
    property PayPeriodClassName: WideString read FPayPeriodClassName write FPayPeriodClassName;
    property ActiveEmployees: Integer read FActiveEmployees write FActiveEmployees;
    property InactiveEmployees: Integer read FInactiveEmployees write FInactiveEmployees;
    property PayPeriodLength: Integer read FPayPeriodLength write FPayPeriodLength;
  end;

  TAePayPeriodInfoAry = array of TAePayPeriodInfo;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeEmployeeBasic = class(TRemotable)
  private
    FEmpName: WideString;
    FLastName: WideString;
    FFirstName: WideString;
    FInitial: WideString;
    FEmpID: WideString;
    FSSN: WideString;
    FBadge: Integer;
    FFilekey: Integer;
    FActiveStatus: Integer;
    FDateOfHire: WideString;
    FWG1: Integer;
    FWG2: Integer;
    FWG3: Integer;
    FWG4: Integer;
    FWG5: Integer;
    FWG6: Integer;
    FWG7: Integer;
    FWGDescr: WideString;
  published
    property EmpName: WideString read FEmpName write FEmpName;
    property LastName: WideString read FLastName write FLastName;
    property FirstName: WideString read FFirstName write FFirstName;
    property Initial: WideString read FInitial write FInitial;
    property EmpID: WideString read FEmpID write FEmpID;
    property SSN: WideString read FSSN write FSSN;
    property Badge: Integer read FBadge write FBadge;
    property Filekey: Integer read FFilekey write FFilekey;
    property ActiveStatus: Integer read FActiveStatus write FActiveStatus;
    property DateOfHire: WideString read FDateOfHire write FDateOfHire;
    property WG1: Integer read FWG1 write FWG1;
    property WG2: Integer read FWG2 write FWG2;
    property WG3: Integer read FWG3 write FWG3;
    property WG4: Integer read FWG4 write FWG4;
    property WG5: Integer read FWG5 write FWG5;
    property WG6: Integer read FWG6 write FWG6;
    property WG7: Integer read FWG7 write FWG7;
    property WGDescr: WideString read FWGDescr write FWGDescr;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeEmployeeDetail = class(TAeEmployeeBasic)
  private
    FCurrentRate: Double;
    FCurrentRateEffDate: WideString;
    FActiveStatusConditionID: Integer;
    FInactiveStatusConditionID: Integer;
    FActiveStatusConditionEffDate: WideString;
    FPayTypeID: Integer;
    FPayTypeEffDate: WideString;
    FPayClassID: Integer;
    FPayClassEffDate: WideString;
    FSchPatternID: Integer;
    FSchPatternEffDate: WideString;
    FHourlyStatusID: Integer;
    FHourlyStatusEffDate: WideString;
    FAvgWeeklyHrs: Integer;
    FClockGroupID: Integer;
    FBirthDate: WideString;
    FWGEffDate: WideString;
    FPhone1: WideString;
    FPhone2: WideString;
    FEmergencyContact: WideString;
    FAddress1: WideString;
    FAddress2: WideString;
    FAddress3: WideString;
    FAddressCity: WideString;
    FAddressStateProv: WideString;
    FAddressZIPPC: WideString;
    FUnionCode: WideString;
    FStaticCustom1: WideString;
    FStaticCustom2: WideString;
    FStaticCustom3: WideString;
    FStaticCustom4: WideString;
    FStaticCustom5: WideString;
    FStaticCustom6: WideString;
  published
    property CurrentRate: Double read FCurrentRate write FCurrentRate;
    property CurrentRateEffDate: WideString read FCurrentRateEffDate write FCurrentRateEffDate;
    property ActiveStatusConditionID: Integer read FActiveStatusConditionID write FActiveStatusConditionID;
    property InactiveStatusConditionID: Integer read FInactiveStatusConditionID write FInactiveStatusConditionID;
    property ActiveStatusConditionEffDate: WideString read FActiveStatusConditionEffDate write FActiveStatusConditionEffDate;
    property PayTypeID: Integer read FPayTypeID write FPayTypeID;
    property PayTypeEffDate: WideString read FPayTypeEffDate write FPayTypeEffDate;
    property PayClassID: Integer read FPayClassID write FPayClassID;
    property PayClassEffDate: WideString read FPayClassEffDate write FPayClassEffDate;
    property SchPatternID: Integer read FSchPatternID write FSchPatternID;
    property SchPatternEffDate: WideString read FSchPatternEffDate write FSchPatternEffDate;
    property HourlyStatusID: Integer read FHourlyStatusID write FHourlyStatusID;
    property HourlyStatusEffDate: WideString read FHourlyStatusEffDate write FHourlyStatusEffDate;
    property AvgWeeklyHrs: Integer read FAvgWeeklyHrs write FAvgWeeklyHrs;
    property ClockGroupID: Integer read FClockGroupID write FClockGroupID;
    property BirthDate: WideString read FBirthDate write FBirthDate;
    property WGEffDate: WideString read FWGEffDate write FWGEffDate;
    property Phone1: WideString read FPhone1 write FPhone1;
    property Phone2: WideString read FPhone2 write FPhone2;
    property EmergencyContact: WideString read FEmergencyContact write FEmergencyContact;
    property Address1: WideString read FAddress1 write FAddress1;
    property Address2: WideString read FAddress2 write FAddress2;
    property Address3: WideString read FAddress3 write FAddress3;
    property AddressCity: WideString read FAddressCity write FAddressCity;
    property AddressStateProv: WideString read FAddressStateProv write FAddressStateProv;
    property AddressZIPPC: WideString read FAddressZIPPC write FAddressZIPPC;
    property UnionCode: WideString read FUnionCode write FUnionCode;
    property StaticCustom1: WideString read FStaticCustom1 write FStaticCustom1;
    property StaticCustom2: WideString read FStaticCustom2 write FStaticCustom2;
    property StaticCustom3: WideString read FStaticCustom3 write FStaticCustom3;
    property StaticCustom4: WideString read FStaticCustom4 write FStaticCustom4;
    property StaticCustom5: WideString read FStaticCustom5 write FStaticCustom5;
    property StaticCustom6: WideString read FStaticCustom6 write FStaticCustom6;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeEmployeeDetail2 = class(TAeEmployeeDetail)
  private
    FESSPIN: WideString;
    FEMAIL: WideString;
    FObservesDST: Boolean;
    FTimeZoneOffset: Integer;
    FSpareStr1: WideString;
    FSpareInt1: Integer;
  published
    property ESSPIN: WideString read FESSPIN write FESSPIN;
    property EMAIL: WideString read FEMAIL write FEMAIL;
    property ObservesDST: Boolean read FObservesDST write FObservesDST;
    property TimeZoneOffset: Integer read FTimeZoneOffset write FTimeZoneOffset;
    property SpareStr1: WideString read FSpareStr1 write FSpareStr1;
    property SpareInt1: Integer read FSpareInt1 write FSpareInt1;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAePayLine = class(TAeEmployeeBasic)
  private
    FBaseRate: Double;
    FDate: WideString;
    FPayDesNum: Integer;
    FPayDesName: WideString;
    FWrkWG1: Integer;
    FWrkWG2: Integer;
    FWrkWG3: Integer;
    FWrkWG4: Integer;
    FWrkWG5: Integer;
    FWrkWG6: Integer;
    FWrkWG7: Integer;
    FWrkWGDescr: WideString;
    FHours: Integer;
    FWrkRate: Double;
    FHoursHund: Double;
    FDollars: Double;
  published
    property BaseRate: Double read FBaseRate write FBaseRate;
    property Date: WideString read FDate write FDate;
    property PayDesNum: Integer read FPayDesNum write FPayDesNum;
    property PayDesName: WideString read FPayDesName write FPayDesName;
    property WrkWG1: Integer read FWrkWG1 write FWrkWG1;
    property WrkWG2: Integer read FWrkWG2 write FWrkWG2;
    property WrkWG3: Integer read FWrkWG3 write FWrkWG3;
    property WrkWG4: Integer read FWrkWG4 write FWrkWG4;
    property WrkWG5: Integer read FWrkWG5 write FWrkWG5;
    property WrkWG6: Integer read FWrkWG6 write FWrkWG6;
    property WrkWG7: Integer read FWrkWG7 write FWrkWG7;
    property WrkWGDescr: WideString read FWrkWGDescr write FWrkWGDescr;
    property Hours: Integer read FHours write FHours;
    property WrkRate: Double read FWrkRate write FWrkRate;
    property HoursHund: Double read FHoursHund write FHoursHund;
    property Dollars: Double read FDollars write FDollars;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeShiftLineSimple = class(TAeEmployeeBasic)
  private
    FDate: WideString;
    FTransactionCount: Integer;
    FPunch1: WideString;
    FPunch2: WideString;
    FPunch3: WideString;
    FPunch4: WideString;
    FPunch5: WideString;
    FPunch6: WideString;
    FPunch7: WideString;
    FPunch8: WideString;
    FExceptionDescr: WideString;
    FDop: Integer;
    FDayOfWeek: TDayOfWeekEnum;
    FShiftNum: Integer;
    FHours: Integer;
    FHoursHund: Double;
  published
    property Date: WideString read FDate write FDate;
    property TransactionCount: Integer read FTransactionCount write FTransactionCount;
    property Punch1: WideString read FPunch1 write FPunch1;
    property Punch2: WideString read FPunch2 write FPunch2;
    property Punch3: WideString read FPunch3 write FPunch3;
    property Punch4: WideString read FPunch4 write FPunch4;
    property Punch5: WideString read FPunch5 write FPunch5;
    property Punch6: WideString read FPunch6 write FPunch6;
    property Punch7: WideString read FPunch7 write FPunch7;
    property Punch8: WideString read FPunch8 write FPunch8;
    property ExceptionDescr: WideString read FExceptionDescr write FExceptionDescr;
    property Dop: Integer read FDop write FDop;
    property DayOfWeek: TDayOfWeekEnum read FDayOfWeek write FDayOfWeek;
    property ShiftNum: Integer read FShiftNum write FShiftNum;
    property Hours: Integer read FHours write FHours;
    property HoursHund: Double read FHoursHund write FHoursHund;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeSchedule = class(TAeEmployeeBasic)
  private
    FSchDate: WideString;
    FSchStartTime: WideString;
    FSchEndTime: WideString;
    FSchHours: Integer;
    FSchRate: Double;
    FSchHoursHund: Double;
    FSchType: TSchTypeEnum;
    FSchStyle: Integer;
    FSchPattID: Integer;
    FBenefitID: Integer;
    FPayDesID: Integer;
    FSchWG1: Integer;
    FSchWG2: Integer;
    FSchWG3: Integer;
    FSchWG4: Integer;
    FSchWG5: Integer;
    FSchWG6: Integer;
    FSchWG7: Integer;
    FSchWGDescr: WideString;
  published
    property SchDate: WideString read FSchDate write FSchDate;
    property SchStartTime: WideString read FSchStartTime write FSchStartTime;
    property SchEndTime: WideString read FSchEndTime write FSchEndTime;
    property SchHours: Integer read FSchHours write FSchHours;
    property SchRate: Double read FSchRate write FSchRate;
    property SchHoursHund: Double read FSchHoursHund write FSchHoursHund;
    property SchType: TSchTypeEnum read FSchType write FSchType;
    property SchStyle: Integer read FSchStyle write FSchStyle;
    property SchPattID: Integer read FSchPattID write FSchPattID;
    property BenefitID: Integer read FBenefitID write FBenefitID;
    property PayDesID: Integer read FPayDesID write FPayDesID;
    property SchWG1: Integer read FSchWG1 write FSchWG1;
    property SchWG2: Integer read FSchWG2 write FSchWG2;
    property SchWG3: Integer read FSchWG3 write FSchWG3;
    property SchWG4: Integer read FSchWG4 write FSchWG4;
    property SchWG5: Integer read FSchWG5 write FSchWG5;
    property SchWG6: Integer read FSchWG6 write FSchWG6;
    property SchWG7: Integer read FSchWG7 write FSchWG7;
    property SchWGDescr: WideString read FSchWGDescr write FSchWGDescr;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeEdit = class(TAeEmployeeBasic)
  private
    FEditNameDescr: WideString;
    FEditOpDescr: WideString;
    FAccountCode: WideString;
    FEffDate: WideString;
    FEffTime: WideString;
    FEditTimeStamp: WideString;
    FHours: Integer;
    FEditRate: Double;
    FEditDollars: Double;
    FHoursHund: Double;
    FEditType: Integer;
    FClkSup: Integer;
    FSiteID: Integer;
    FPayDesID: Integer;
    FPrevPayDesID: Integer;
    FReasonCodeID: Integer;
    FEditWG1: Integer;
    FEditWG2: Integer;
    FEditWG3: Integer;
    FEditWG4: Integer;
    FEditWG5: Integer;
    FEditWG6: Integer;
    FEditWG7: Integer;
    FEditWGDescr: WideString;
  published
    property EditNameDescr: WideString read FEditNameDescr write FEditNameDescr;
    property EditOpDescr: WideString read FEditOpDescr write FEditOpDescr;
    property AccountCode: WideString read FAccountCode write FAccountCode;
    property EffDate: WideString read FEffDate write FEffDate;
    property EffTime: WideString read FEffTime write FEffTime;
    property EditTimeStamp: WideString read FEditTimeStamp write FEditTimeStamp;
    property Hours: Integer read FHours write FHours;
    property EditRate: Double read FEditRate write FEditRate;
    property EditDollars: Double read FEditDollars write FEditDollars;
    property HoursHund: Double read FHoursHund write FHoursHund;
    property EditType: Integer read FEditType write FEditType;
    property ClkSup: Integer read FClkSup write FClkSup;
    property SiteID: Integer read FSiteID write FSiteID;
    property PayDesID: Integer read FPayDesID write FPayDesID;
    property PrevPayDesID: Integer read FPrevPayDesID write FPrevPayDesID;
    property ReasonCodeID: Integer read FReasonCodeID write FReasonCodeID;
    property EditWG1: Integer read FEditWG1 write FEditWG1;
    property EditWG2: Integer read FEditWG2 write FEditWG2;
    property EditWG3: Integer read FEditWG3 write FEditWG3;
    property EditWG4: Integer read FEditWG4 write FEditWG4;
    property EditWG5: Integer read FEditWG5 write FEditWG5;
    property EditWG6: Integer read FEditWG6 write FEditWG6;
    property EditWG7: Integer read FEditWG7 write FEditWG7;
    property EditWGDescr: WideString read FEditWGDescr write FEditWGDescr;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeEmpTransaction = class(TAeEmployeeBasic)
  private
    FTimeStamp: WideString;
    FStation: Integer;
    FTransType: Integer;
  published
    property TimeStamp: WideString read FTimeStamp write FTimeStamp;
    property Station: Integer read FStation write FStation;
    property TransType: Integer read FTransType write FTransType;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeEmpBenefitActivity = class(TAeEmployeeBasic)
  private
    FCreditHours: Integer;
    FDebitHours: Integer;
    FBalanceHours: Integer;
    FCreditHoursHund: Double;
    FDebitHoursHund: Double;
    FBalanceHoursHund: Double;
    FCreditDollars: Double;
    FDebitDollars: Double;
    FBalanceDollars: Double;
    FDescr: WideString;
    FAbb: WideString;
    FCategory: WideString;
    FEventDate: WideString;
    FIsHours: Boolean;
    FLastInGroup: Boolean;
    FBenefitID: Integer;
  published
    property CreditHours: Integer read FCreditHours write FCreditHours;
    property DebitHours: Integer read FDebitHours write FDebitHours;
    property BalanceHours: Integer read FBalanceHours write FBalanceHours;
    property CreditHoursHund: Double read FCreditHoursHund write FCreditHoursHund;
    property DebitHoursHund: Double read FDebitHoursHund write FDebitHoursHund;
    property BalanceHoursHund: Double read FBalanceHoursHund write FBalanceHoursHund;
    property CreditDollars: Double read FCreditDollars write FCreditDollars;
    property DebitDollars: Double read FDebitDollars write FDebitDollars;
    property BalanceDollars: Double read FBalanceDollars write FBalanceDollars;
    property Descr: WideString read FDescr write FDescr;
    property Abb: WideString read FAbb write FAbb;
    property Category: WideString read FCategory write FCategory;
    property EventDate: WideString read FEventDate write FEventDate;
    property IsHours: Boolean read FIsHours write FIsHours;
    property LastInGroup: Boolean read FLastInGroup write FLastInGroup;
    property BenefitID: Integer read FBenefitID write FBenefitID;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeTimeOffRequest = class(TAeEmployeeBasic)
  private
    FUniqueID: Integer;
    FRequestUse: Integer;
    FBenefitID: Integer;
    FPayDesID: Integer;
    FRequestTimeOffTypeEnum: TRequestTimeOffTypeEnum;
    FRTOShiftPartTypeEnum: TRTOShiftPartTypeEnum;
    FPosTime: Integer;
    FReqState: Integer;
    FHours: Integer;
    FHoursHund: Double;
    FDollars: Double;
    FStartDate: WideString;
    FEndDate: WideString;
    FComments: WideString;
    FCreated: WideString;
    FClosedOn: WideString;
    FClosedBy: WideString;
  published
    property UniqueID: Integer read FUniqueID write FUniqueID;
    property RequestUse: Integer read FRequestUse write FRequestUse;
    property BenefitID: Integer read FBenefitID write FBenefitID;
    property PayDesID: Integer read FPayDesID write FPayDesID;
    property RequestTimeOffTypeEnum: TRequestTimeOffTypeEnum read FRequestTimeOffTypeEnum write FRequestTimeOffTypeEnum;
    property RTOShiftPartTypeEnum: TRTOShiftPartTypeEnum read FRTOShiftPartTypeEnum write FRTOShiftPartTypeEnum;
    property PosTime: Integer read FPosTime write FPosTime;
    property ReqState: Integer read FReqState write FReqState;
    property Hours: Integer read FHours write FHours;
    property HoursHund: Double read FHoursHund write FHoursHund;
    property Dollars: Double read FDollars write FDollars;
    property StartDate: WideString read FStartDate write FStartDate;
    property EndDate: WideString read FEndDate write FEndDate;
    property Comments: WideString read FComments write FComments;
    property Created: WideString read FCreated write FCreated;
    property ClosedOn: WideString read FClosedOn write FClosedOn;
    property ClosedBy: WideString read FClosedBy write FClosedBy;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeWorkgroupAssignment = class(TRemotable)
  private
    FFilekey: Integer;
    FEmpID: WideString;
    FEffDate: WideString;
    FComments: WideString;
    FWG1: Integer;
    FWG2: Integer;
    FWG3: Integer;
    FWG4: Integer;
    FWG5: Integer;
    FWG6: Integer;
    FWG7: Integer;
  published
    property Filekey: Integer read FFilekey write FFilekey;
    property EmpID: WideString read FEmpID write FEmpID;
    property EffDate: WideString read FEffDate write FEffDate;
    property Comments: WideString read FComments write FComments;
    property WG1: Integer read FWG1 write FWG1;
    property WG2: Integer read FWG2 write FWG2;
    property WG3: Integer read FWG3 write FWG3;
    property WG4: Integer read FWG4 write FWG4;
    property WG5: Integer read FWG5 write FWG5;
    property WG6: Integer read FWG6 write FWG6;
    property WG7: Integer read FWG7 write FWG7;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeRatePayTypeAssignment = class(TRemotable)
  private
    FFilekey: Integer;
    FEmpID: WideString;
    FRateEffDate: WideString;
    FPayTypeEffDate: WideString;
    FComments: WideString;
    FPayTypeID: Integer;
    FRate: Double;
  published
    property Filekey: Integer read FFilekey write FFilekey;
    property EmpID: WideString read FEmpID write FEmpID;
    property RateEffDate: WideString read FRateEffDate write FRateEffDate;
    property PayTypeEffDate: WideString read FPayTypeEffDate write FPayTypeEffDate;
    property Comments: WideString read FComments write FComments;
    property PayTypeID: Integer read FPayTypeID write FPayTypeID;
    property Rate: Double read FRate write FRate;
  end;

  TAeEmployeeBasicAry = array of TAeEmployeeBasic;   { "urn:AeXMLBridgeIntf" }
  TAeEmployeeDetailAry = array of TAeEmployeeDetail;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeWorkgroupLevel = class(TRemotable)
  private
    FWGLevel: Integer;
    FWGLevelName: WideString;
    FWGLevelPluralName: WideString;
  published
    property WGLevel: Integer read FWGLevel write FWGLevel;
    property WGLevelName: WideString read FWGLevelName write FWGLevelName;
    property WGLevelPluralName: WideString read FWGLevelPluralName write FWGLevelPluralName;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeWorkgroup = class(TRemotable)
  private
    FWGLevel: Integer;
    FWGNum: Integer;
    FWGName: WideString;
    FWGCode: WideString;
    FWGRate: Double;
    FExpires: WideString;
  published
    property WGLevel: Integer read FWGLevel write FWGLevel;
    property WGNum: Integer read FWGNum write FWGNum;
    property WGName: WideString read FWGName write FWGName;
    property WGCode: WideString read FWGCode write FWGCode;
    property WGRate: Double read FWGRate write FWGRate;
    property Expires: WideString read FExpires write FExpires;
  end;

  TAeWorkgroupAry = array of TAeWorkgroup;      { "urn:AeXMLBridgeIntf" }
  TAePayLineAry = array of TAePayLine;          { "urn:AeXMLBridgeIntf" }
  TAeShiftLineSimpleAry = array of TAeShiftLineSimple;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeTransaction = class(TRemotable)
  private
    FTimeStamp: WideString;
    FStation: Integer;
    FTransType: Integer;
  published
    property TimeStamp: WideString read FTimeStamp write FTimeStamp;
    property Station: Integer read FStation write FStation;
    property TransType: Integer read FTransType write FTransType;
  end;

  TAeTransactionAry = array of TAeTransaction;   { "urn:AeXMLBridgeIntf" }
  TAeScheduleAry = array of TAeSchedule;        { "urn:AeXMLBridgeIntf" }
  TAeEditAry = array of TAeEdit;                { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeClientVendorOpResults = class(TRemotable)
  private
    FFileContents: WideString;
    FFileName: WideString;
  published
    property FileContents: WideString read FFileContents write FFileContents;
    property FileName: WideString read FFileName write FFileName;
  end;

  TAeClientVendorOpResultsAry = array of TAeClientVendorOpResults;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAePayPeriodCloseInfo = class(TRemotable)
  private
    FHintInfo: WideString;
    FOpenIssues: Integer;
    FOpenCriticalIssues: Integer;
    FPayPeriodClassNum: Integer;
  published
    property HintInfo: WideString read FHintInfo write FHintInfo;
    property OpenIssues: Integer read FOpenIssues write FOpenIssues;
    property OpenCriticalIssues: Integer read FOpenCriticalIssues write FOpenCriticalIssues;
    property PayPeriodClassNum: Integer read FPayPeriodClassNum write FPayPeriodClassNum;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeWorkgroupSet = class(TRemotable)
  private
    FWG1: Integer;
    FWG2: Integer;
    FWG3: Integer;
    FWG4: Integer;
    FWG5: Integer;
    FWG6: Integer;
    FWG7: Integer;
  published
    property WG1: Integer read FWG1 write FWG1;
    property WG2: Integer read FWG2 write FWG2;
    property WG3: Integer read FWG3 write FWG3;
    property WG4: Integer read FWG4 write FWG4;
    property WG5: Integer read FWG5 write FWG5;
    property WG6: Integer read FWG6 write FWG6;
    property WG7: Integer read FWG7 write FWG7;
  end;

  TAeWorkgroupSetAry = array of TAeWorkgroupSet;   { "urn:AeXMLBridgeIntf" }
  TAeEmpTransactionAry = array of TAeEmpTransaction;   { "urn:AeXMLBridgeIntf" }
  TAeEmpBenefitActivityAry = array of TAeEmpBenefitActivity;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeRateAssignment = class(TRemotable)
  private
    FFilekey: Integer;
    FEmpID: WideString;
    FRate: Double;
    FPayType: Integer;
    FRateEffDate: WideString;
    FComments: WideString;
  published
    property Filekey: Integer read FFilekey write FFilekey;
    property EmpID: WideString read FEmpID write FEmpID;
    property Rate: Double read FRate write FRate;
    property PayType: Integer read FPayType write FPayType;
    property RateEffDate: WideString read FRateEffDate write FRateEffDate;
    property Comments: WideString read FComments write FComments;
  end;

  TAeRateAssignmentAry = array of TAeRateAssignment;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeUsageItem = class(TRemotable)
  private
    FEventDate: WideString;
    FInstances: Integer;
    FUsageItemID: Integer;
    FItemValue: Double;
  published
    property EventDate: WideString read FEventDate write FEventDate;
    property Instances: Integer read FInstances write FInstances;
    property UsageItemID: Integer read FUsageItemID write FUsageItemID;
    property ItemValue: Double read FItemValue write FItemValue;
  end;

  TAeUsageItemAry = array of TAeUsageItem;      { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeAccessAccountActivity = class(TRemotable)
  private
    FEventDate: WideString;
    FOperation: Integer;
    FSource: Integer;
    FUniqueID: Integer;
    FUserName: WideString;
    FExpires: WideString;
  published
    property EventDate: WideString read FEventDate write FEventDate;
    property Operation: Integer read FOperation write FOperation;
    property Source: Integer read FSource write FSource;
    property UniqueID: Integer read FUniqueID write FUniqueID;
    property UserName: WideString read FUserName write FUserName;
    property Expires: WideString read FExpires write FExpires;
  end;

  TAeAccessAccountActivityAry = array of TAeAccessAccountActivity;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeTimeClockStation = class(TRemotable)
  private
    FStationNum: Integer;
    FTerminalType: Integer;
    FUniqueID: Integer;
    FStationName: WideString;
    FSerialNo: WideString;
    FCreated: WideString;
    FSyncMode: Integer;
    FSyncInterval: Integer;
    FSyncOffset: Integer;
    FLogFlags: Integer;
    FCapacity: Integer;
    FClockProps: WideString;
  published
    property StationNum: Integer read FStationNum write FStationNum;
    property TerminalType: Integer read FTerminalType write FTerminalType;
    property UniqueID: Integer read FUniqueID write FUniqueID;
    property StationName: WideString read FStationName write FStationName;
    property SerialNo: WideString read FSerialNo write FSerialNo;
    property Created: WideString read FCreated write FCreated;
    property SyncMode: Integer read FSyncMode write FSyncMode;
    property SyncInterval: Integer read FSyncInterval write FSyncInterval;
    property SyncOffset: Integer read FSyncOffset write FSyncOffset;
    property LogFlags: Integer read FLogFlags write FLogFlags;
    property Capacity: Integer read FCapacity write FCapacity;
    property ClockProps: WideString read FClockProps write FClockProps;
  end;

  TAeTimeClockStationAry = array of TAeTimeClockStation;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeBillingItem = class(TRemotable)
  private
    FUniqueID: Integer;
    FEventDate: WideString;
    FPeriodBegin: WideString;
    FPeriodEnd: WideString;
    FDescr: WideString;
    FInstances: Integer;
    FItemType: Integer;
    FItemValue: Double;
    FItemTotal: Double;
  published
    property UniqueID: Integer read FUniqueID write FUniqueID;
    property EventDate: WideString read FEventDate write FEventDate;
    property PeriodBegin: WideString read FPeriodBegin write FPeriodBegin;
    property PeriodEnd: WideString read FPeriodEnd write FPeriodEnd;
    property Descr: WideString read FDescr write FDescr;
    property Instances: Integer read FInstances write FInstances;
    property ItemType: Integer read FItemType write FItemType;
    property ItemValue: Double read FItemValue write FItemValue;
    property ItemTotal: Double read FItemTotal write FItemTotal;
  end;

  TAeBillingItemAry = array of TAeBillingItem;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeShiftLineDetail = class(TRemotable)
  private
    FDate: WideString;
    FTransactionCount: Integer;
    FFirstPunch: WideString;
    FLastPunch: WideString;
    FSchStart: WideString;
    FSchEnd: WideString;
    FExceptionDescr: WideString;
    FDop: Integer;
    FDow: Integer;
    FDayOfWeek: TDayOfWeekEnum;
    FShiftNum: Integer;
    FShiftStyle: Integer;
    FShiftSch: Integer;
    FHours: Integer;
    FHoursHund: Double;
    FGrossHours: Integer;
    FGrossHoursHund: Double;
    FSchHours: Integer;
    FSchHoursHund: Double;
    FPosIndex: Integer;
    FSchStyle: Integer;
    FSchType: TSchTypeEnum;
    FSchRate: Double;
  published
    property Date: WideString read FDate write FDate;
    property TransactionCount: Integer read FTransactionCount write FTransactionCount;
    property FirstPunch: WideString read FFirstPunch write FFirstPunch;
    property LastPunch: WideString read FLastPunch write FLastPunch;
    property SchStart: WideString read FSchStart write FSchStart;
    property SchEnd: WideString read FSchEnd write FSchEnd;
    property ExceptionDescr: WideString read FExceptionDescr write FExceptionDescr;
    property Dop: Integer read FDop write FDop;
    property Dow: Integer read FDow write FDow;
    property DayOfWeek: TDayOfWeekEnum read FDayOfWeek write FDayOfWeek;
    property ShiftNum: Integer read FShiftNum write FShiftNum;
    property ShiftStyle: Integer read FShiftStyle write FShiftStyle;
    property ShiftSch: Integer read FShiftSch write FShiftSch;
    property Hours: Integer read FHours write FHours;
    property HoursHund: Double read FHoursHund write FHoursHund;
    property GrossHours: Integer read FGrossHours write FGrossHours;
    property GrossHoursHund: Double read FGrossHoursHund write FGrossHoursHund;
    property SchHours: Integer read FSchHours write FSchHours;
    property SchHoursHund: Double read FSchHoursHund write FSchHoursHund;
    property PosIndex: Integer read FPosIndex write FPosIndex;
    property SchStyle: Integer read FSchStyle write FSchStyle;
    property SchType: TSchTypeEnum read FSchType write FSchType;
    property SchRate: Double read FSchRate write FSchRate;
  end;

  TAeShiftLineDetailAry = array of TAeShiftLineDetail;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeShiftTransaction = class(TRemotable)
  private
    FTimeStamp: WideString;
    FStation: Integer;
    FTransType: Integer;
    FShiftPosIndex: Integer;
  published
    property TimeStamp: WideString read FTimeStamp write FTimeStamp;
    property Station: Integer read FStation write FStation;
    property TransType: Integer read FTransType write FTransType;
    property ShiftPosIndex: Integer read FShiftPosIndex write FShiftPosIndex;
  end;

  TAeShiftTransactionAry = array of TAeShiftTransaction;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeShiftSchedule = class(TRemotable)
  private
    FSchDate: WideString;
    FSchStartTime: WideString;
    FSchEndTime: WideString;
    FSchHours: Integer;
    FSchRate: Double;
    FSchHoursHund: Double;
    FSchType: TSchTypeEnum;
    FSchStyle: Integer;
    FSchPattID: Integer;
    FBenefitID: Integer;
    FPayDesID: Integer;
    FSchWG1: Integer;
    FSchWG2: Integer;
    FSchWG3: Integer;
    FSchWG4: Integer;
    FSchWG5: Integer;
    FSchWG6: Integer;
    FSchWG7: Integer;
    FSchWGDescr: WideString;
    FShiftPosIndex: Integer;
  published
    property SchDate: WideString read FSchDate write FSchDate;
    property SchStartTime: WideString read FSchStartTime write FSchStartTime;
    property SchEndTime: WideString read FSchEndTime write FSchEndTime;
    property SchHours: Integer read FSchHours write FSchHours;
    property SchRate: Double read FSchRate write FSchRate;
    property SchHoursHund: Double read FSchHoursHund write FSchHoursHund;
    property SchType: TSchTypeEnum read FSchType write FSchType;
    property SchStyle: Integer read FSchStyle write FSchStyle;
    property SchPattID: Integer read FSchPattID write FSchPattID;
    property BenefitID: Integer read FBenefitID write FBenefitID;
    property PayDesID: Integer read FPayDesID write FPayDesID;
    property SchWG1: Integer read FSchWG1 write FSchWG1;
    property SchWG2: Integer read FSchWG2 write FSchWG2;
    property SchWG3: Integer read FSchWG3 write FSchWG3;
    property SchWG4: Integer read FSchWG4 write FSchWG4;
    property SchWG5: Integer read FSchWG5 write FSchWG5;
    property SchWG6: Integer read FSchWG6 write FSchWG6;
    property SchWG7: Integer read FSchWG7 write FSchWG7;
    property SchWGDescr: WideString read FSchWGDescr write FSchWGDescr;
    property ShiftPosIndex: Integer read FShiftPosIndex write FShiftPosIndex;
  end;

  TAeShiftScheduleAry = array of TAeShiftSchedule;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeTimeOffRequestState = class(TRemotable)
  private
    FUniqueID: Integer;
    FStateID: Integer;
    FUsageState: TRTOUsageStateEnum;
    FApprovalID: Integer;
    FDescr: WideString;
    FAbb: WideString;
    FCreated: WideString;
  published
    property UniqueID: Integer read FUniqueID write FUniqueID;
    property StateID: Integer read FStateID write FStateID;
    property UsageState: TRTOUsageStateEnum read FUsageState write FUsageState;
    property ApprovalID: Integer read FApprovalID write FApprovalID;
    property Descr: WideString read FDescr write FDescr;
    property Abb: WideString read FAbb write FAbb;
    property Created: WideString read FCreated write FCreated;
  end;

  TAeTimeOffRequestStatesAry = array of TAeTimeOffRequestState;   { "urn:AeXMLBridgeIntf" }
  TAeTimeOffRequestAry = array of TAeTimeOffRequest;   { "urn:AeXMLBridgeIntf" }
  TAeEmployeeDetail2Ary = array of TAeEmployeeDetail2;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeExchangeStruct = class(TRemotable)
  private
    FRawData: WideString;
    FLineSep: WideString;
    FFieldSep: WideString;
    FItemID: Integer;
  published
    property RawData: WideString read FRawData write FRawData;
    property LineSep: WideString read FLineSep write FLineSep;
    property FieldSep: WideString read FFieldSep write FFieldSep;
    property ItemID: Integer read FItemID write FItemID;
  end;



  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TAeWorkgroupXLate = class(TRemotable)
  private
    FWGExtID: WideString;
    FWGExtDescr: WideString;
    FWG1: Integer;
    FWG2: Integer;
    FWG3: Integer;
    FWG4: Integer;
    FWG5: Integer;
    FWG6: Integer;
    FWG7: Integer;
  published
    property WGExtID: WideString read FWGExtID write FWGExtID;
    property WGExtDescr: WideString read FWGExtDescr write FWGExtDescr;
    property WG1: Integer read FWG1 write FWG1;
    property WG2: Integer read FWG2 write FWG2;
    property WG3: Integer read FWG3 write FWG3;
    property WG4: Integer read FWG4 write FWG4;
    property WG5: Integer read FWG5 write FWG5;
    property WG6: Integer read FWG6 write FWG6;
    property WG7: Integer read FWG7 write FWG7;
  end;

  TAeWorkgroupXLateAry = array of TAeWorkgroupXLate;   { "urn:AeXMLBridgeIntf" }


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf
  // ************************************************************************ //
  TSessionHeader = class(TSOAPHeader)
  private
    FAccount: WideString;
    FPassword: WideString;
    FClientCode: WideString;
  published
    property Account: WideString read FAccount write FAccount;
    property Password: WideString read FPassword write FPassword;
    property ClientCode: WideString read FClientCode write FClientCode;
  end;


  // ************************************************************************ //
  // Namespace : urn:AeXMLBridgeIntf-IAeXMLBridge
  // soapAction: urn:AeXMLBridgeIntf-IAeXMLBridge#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : IAeXMLBridgebinding
  // service   : IAeXMLBridgeservice
  // port      : IAeXMLBridgePort
  // URL       : https://evolutiondev.attendanceondemand.com:8192/cc1exec.aew/soap/IAeXMLBridge
  // ************************************************************************ //
  IAeXMLBridge = interface(IInvokable)
  ['{6BCB3837-D975-032A-9221-70312B2124B8}']
    function  interfaceBuild: Integer; stdcall;
    function  getCompanyName: WideString; stdcall;
    function  getTotalEmployeeCount: Integer; stdcall;
    function  getActiveEmployeesCount: Integer; stdcall;
    function  getInactiveEmployeesCount: Integer; stdcall;
    function  getSiteEnvironment: TAeSiteEnvironment; stdcall;
    function  getBaseClientData: TAeClientData; stdcall;
    procedure setBaseClientData(const AeClientData: TAeClientData); stdcall;
    function  getAccessAccounts: TAeAccessAccountAry; stdcall;
    function  getAccessAccountViaAccountCode(const AccountCode: WideString): TAeAccessAccount; stdcall;
    function  accessAccountCodeExists(const AccountCode: WideString): Boolean; stdcall;
    function  getSystemReportsBasic: TAeClientReportAry; stdcall;
    function  getSystemExportsBasic: TAeClientExportAry; stdcall;
    function  getSystemVendorOpsBasic: TAeClientVendorOpAry; stdcall;
    function  getActiveStatusConditions: TAeBasicDataItemsAry; stdcall;
    function  getInactiveStatusConditions: TAeBasicDataItemsAry; stdcall;
    function  getPayTypes: TAeBasicDataItemsAry; stdcall;
    function  getPayClassesSimple: TAeBasicDataItemsAry; stdcall;
    function  getSchedulePatternsSimple: TAeBasicDataItemsAry; stdcall;
    function  getClockGroupsSimple: TAeBasicDataItemsAry; stdcall;
    function  getHourlyStatusTypesSimple: TAeBasicDataItemsAry; stdcall;
    function  getCorrectiveActionsSimple: TAeBasicDataItemsAry; stdcall;
    function  getStationsSimple: TAeBasicDataItemsAry; stdcall;
    function  getClustersSimple: TAeBasicDataItemsAry; stdcall;
    function  getClusterStationsSimple(const ClusterNum: Integer): TAeBasicDataItemsAry; stdcall;
    function  getClockGroupStationsSimple(const ClockGroupNum: Integer): TAeBasicDataItemsAry; stdcall;
    function  getBenefitsSimple: TAeBasicDataItemsAry; stdcall;
    function  getPayDesignationsSimple: TAeBasicDataItemsAry; stdcall;
    function  getScheduleStylesSimple: TAeBasicDataItemsAry; stdcall;
    function  getUserGroupsSimple: TAeBasicDataItemsAry; stdcall;
    function  getBrowserProfilesSimple: TStringArray; stdcall;
    function  getCurrentBrowserProfileMenuItems: TAeBrowserMenuItemsAry; stdcall;
    function  getHyperQueriesSimple: TStringArray; stdcall;
    function  getPayPeriodClassData(const _PayPeriodClassNum: Integer): TAePayPeriodInfo; stdcall;
    function  getPayPeriodClasses: TAePayPeriodInfoAry; stdcall;
    function  getEmptyEmployeeBasic: TAeEmployeeBasic; stdcall;
    function  getEmptyEmployeeDetail: TAeEmployeeDetail; stdcall;
    function  getEmptyClientExport: TAeClientExport; stdcall;
    function  getEmptyClientReport: TAeClientReport; stdcall;
    function  getEmptyClientVendorOp: TAeClientVendorOp; stdcall;
    function  getEmptyAccessAccount: TAeAccessAccount; stdcall;
    function  getEmployeeBasicByIDNum(const IDNum: WideString): TAeEmployeeBasic; stdcall;
    function  getEmployeeBasicBySSN(const SSN: WideString): TAeEmployeeBasic; stdcall;
    function  getEmployeeBasicByBadge(const Badge: Integer): TAeEmployeeBasic; stdcall;
    function  getEmployeeBasicByFilekey(const Filekey: Integer): TAeEmployeeBasic; stdcall;
    function  getEmployeeDetailByIDNum(const IDNum: WideString): TAeEmployeeDetail; stdcall;
    function  getEmployeeDetailBySSN(const SSN: WideString): TAeEmployeeDetail; stdcall;
    function  getEmployeeDetailByBadge(const Badge: Integer): TAeEmployeeDetail; stdcall;
    function  getEmployeeDetailByFilekey(const Filekey: Integer): TAeEmployeeDetail; stdcall;
    function  extractEmployeeWorkgroupAssignmentByFilekey(const Filekey: Integer): TAeWorkgroupAssignment; stdcall;
    function  extractEmployeeWorkgroupAssignmentByIDNum(const IDNum: WideString): TAeWorkgroupAssignment; stdcall;
    function  extractEmployeeRatePayTypeAssignmentByFilekey(const Filekey: Integer): TAeRatePayTypeAssignment; stdcall;
    function  extractEmployeeRatePayTypeAssignmentByIDNum(const IDNum: WideString): TAeRatePayTypeAssignment; stdcall;
    procedure reassignEmployeeWorkgroup(const AeWorkgroupAssignment: TAeWorkgroupAssignment); stdcall;
    procedure reassignEmployeeRatePayType(const AeRatePayTypeAssignment: TAeRatePayTypeAssignment); stdcall;
    procedure setEmployeeBasic(const AeEmployeeBasic: TAeEmployeeBasic); stdcall;
    procedure setEmployeeDetail(const AeEmployeeDetail: TAeEmployeeDetail); stdcall;
    function  maintainEmployeeBasic(const AeEmployeeBasic: TAeEmployeeBasic; const AddEmpMode: TAddEmpMode; const BadgeManagement: TBadgeManagement): TMaintainEmployeeResult; stdcall;
    function  maintainEmployeeDetail(const AeEmployeeDetail: TAeEmployeeDetail; const AddEmpMode: TAddEmpMode; const BadgeManagement: TBadgeManagement): TMaintainEmployeeResult; stdcall;
    function  maintainEmployeeResultDescr(const MaintainEmployeeResult: TMaintainEmployeeResult): WideString; stdcall;
    function  dateRangeEnumDescr(const DateRangeEnum: TDateRangeEnum): WideString; stdcall;
    function  dateRangeEnumListSimple: TAeBasicDataItemsAry; stdcall;
    function  dateRangeEmpEnumListSimple: TAeBasicDataItemsAry; stdcall;
    function  getActiveEmployeesList: TAeEmployeeBasicAry; stdcall;
    function  getPayrollEmployeesList(const PayPerClass: Integer; const PayPeriodEnum: TPayPeriodEnum): TAeEmployeeBasicAry; stdcall;
    function  getInactiveEmployeesList: TAeEmployeeBasicAry; stdcall;
    function  getAllEmployeesList: TAeEmployeeBasicAry; stdcall;
    function  getEmployeesListBasicFromHyperQuery(const HyperQueryName: WideString): TAeEmployeeBasicAry; stdcall;
    function  getEmployeesListDetailFromHyperQuery(const HyperQueryName: WideString): TAeEmployeeDetailAry; stdcall;
    function  getWorkgroupLevelDetails(const WGLevel: Integer): TAeWorkgroupLevel; stdcall;
    function  getWorkgroups(const WGLevel: Integer; const WGSortingOption: TWGSortingOption): TAeWorkgroupAry; stdcall;
    function  getWorkgroup(const WGLevel: Integer; const WGNum: Integer): TAeWorkgroup; stdcall;
    function  getWorkgroupViaCode(const WGLevel: Integer; const WGCode: WideString): TAeWorkgroup; stdcall;
    function  getWorkgroupViaName(const WGLevel: Integer; const WGName: WideString): TAeWorkgroup; stdcall;
    function  getWorkgroupStruct: TAeWorkgroup; stdcall;
    procedure setWorkgroup(const AeWorkgroup: TAeWorkgroup); stdcall;
    procedure addWorkgroup(const AeWorkgroup: TAeWorkgroup); stdcall;
    function  nextWGNumForWGLevel(const WGLevel: Integer): Integer; stdcall;
    procedure delWorkgroup(const WGLevel: Integer; const WGNum: Integer; const ReplacementWGNum: Integer; const UseValTable: Boolean); stdcall;
    function  extractEmployeePeriodFrameByFilekey(const Filekey: Integer): TAePayPeriodFrame; stdcall;
    function  extractEmployeePeriodFrameByIDNum(const IDNum: WideString): TAePayPeriodFrame; stdcall;
    function  extractEmployeePeriodSummsByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAePayLineAry; stdcall;
    function  extractEmployeePeriodSummsByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAePayLineAry; stdcall;
    function  extractEmployeeDailySummsByFilekey(const Filekey: Integer; const Date: WideString; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAePayLineAry; stdcall;
    function  extractEmployeeDailySummsByIDNum(const IDNum: WideString; const Date: WideString; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAePayLineAry; stdcall;
    function  extractEmployeePeriodShiftsByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAeShiftLineSimpleAry; stdcall;
    function  extractEmployeePeriodShiftsByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAeShiftLineSimpleAry; stdcall;
    function  extractEmployeeTransactionsByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum): TAeTransactionAry; stdcall;
    function  extractEmployeeTransactionsByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum): TAeTransactionAry; stdcall;
    function  extractEmployeeSchedulesByFilekey(const Filekey: Integer; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeScheduleAry; stdcall;
    function  extractEmployeeSchedulesByIDNum(const IDNum: WideString; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeScheduleAry; stdcall;
    function  extractEmployeeEditsByFilekey(const Filekey: Integer; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeEditAry; stdcall;
    function  extractEmployeeEditsByIDNum(const IDNum: WideString; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeEditAry; stdcall;
    function  extractPayPeriodSummaries(const PayPerClass: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum; const NoActivityInclusion: TNoActivityInclusion): TAePayLineAry; stdcall;
    function  executeExportOperation(const AeClientExport: TAeClientExport): WideString; stdcall;
    function  executeExportOperationStrings(const AeClientExport: TAeClientExport): TStringArray; stdcall;
    function  executeReportOperation(const AeClientReport: TAeClientReport): WideString; stdcall;
    function  executeVendorOperation(const AeClientVendorOp: TAeClientVendorOp): TAeClientVendorOpResultsAry; stdcall;
    function  addAccessAccount(const AeAccessAccount: TAeAccessAccount): TAeAccessAccount; stdcall;
    function  removeAccessAccount(const AeAccessAccount: TAeAccessAccount): Boolean; stdcall;
    function  editAccessAccount(const AeAccessAccount: TAeAccessAccount): Boolean; stdcall;
    function  getPayPeriodClosureStats(const PayPeriodEnum: TPayPeriodEnum; const PayPeriodClassNum: Integer; const HyperQueryName: WideString): TAePayPeriodCloseInfo; stdcall;
    function  getPayPeriodClosureEmployees(const PayPeriodEnum: TPayPeriodEnum; const PayPeriodClassNum: Integer; const HyperQueryName: WideString; const CriticalOnly: Boolean; const UserUniqueID: Integer): TAeEmployeeBasicAry; stdcall;
    procedure addAccessAccountWorkgroupAccessRightByAccountCode(const AccountCode: WideString; const AeWorkgroupSet: TAeWorkgroupSet); stdcall;
    procedure removeAccessAccountAllWorkgroupAccessRightsByAccountCode(const AccountCode: WideString); stdcall;
    function  getAccessAccountWorkgroupAccessRightsByAccountCode(const AccountCode: WideString): TAeWorkgroupSetAry; stdcall;
    function  extractRangedTransactionsUsingHyperQuery(const HyperQueryName: WideString; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeEmpTransactionAry; stdcall;
    function  extractRangedSchedulesUsingHyperQuery(const HyperQueryName: WideString; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeScheduleAry; stdcall;
    procedure appendEmployeePunchByIDNum(const IDNum: WideString; const TimeStamp: WideString; const Station: Integer; const TransType: Integer); stdcall;
    procedure appendEmployeePunchByFilekey(const Filekey: Integer; const TimeStamp: WideString; const Station: Integer; const TransType: Integer); stdcall;
    procedure punchEmployeeNowByFilekey(const Filekey: Integer; const Station: Integer; const TransType: Integer); stdcall;
    procedure punchEmployeeNowByIDNum(const IDNum: WideString; const Station: Integer; const TransType: Integer); stdcall;
    procedure appendEmployeeTransferByFilekey(const Filekey: Integer; const TimeStamp: WideString; const AeWorkgroupSet: TAeWorkgroupSet; const Rate: Double; const ReasonCodeID: Integer); stdcall;
    procedure appendEmployeeTransferByIDNum(const IDNum: WideString; const TimeStamp: WideString; const AeWorkgroupSet: TAeWorkgroupSet; const Rate: Double; const ReasonCodeID: Integer); stdcall;
    procedure transferEmployeeNowByFilekey(const Filekey: Integer; const AeWorkgroupSet: TAeWorkgroupSet; const Rate: Double; const ReasonCodeID: Integer); stdcall;
    procedure transferEmployeeNowByIDNum(const IDNum: WideString; const AeWorkgroupSet: TAeWorkgroupSet; const Rate: Double; const ReasonCodeID: Integer); stdcall;
    procedure recomputePendingRequests; stdcall;
    procedure recomputeEmployeeByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum); stdcall;
    procedure recomputeEmployeeByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum); stdcall;
    procedure recomputeEmployeeAccrualsByFilekey(const Filekey: Integer; const FromDate: WideString); stdcall;
    procedure recomputeEmployeeAccrualsByIDNum(const IDNum: WideString; const FromDate: WideString); stdcall;
    function  employeeBenefitBalanceAsOfByFilekey(const Filekey: Integer; const BenefitID: Integer; const SelDate: WideString): Double; stdcall;
    function  employeeBenefitBalanceAsOfByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: WideString): Double; stdcall;
    procedure adjustEmployeeBenefitBalanceByFilekey(const Filekey: Integer; const BenefitID: Integer; const SelDate: WideString; const BenAdjType: TBenAdjTypeEnum; const Amount: Double); stdcall;
    procedure adjustEmployeeBenefitBalanceByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: WideString; const BenAdjType: TBenAdjTypeEnum; const Amount: Double); stdcall;
    procedure removeEmployeeBenefitAdjustmentsByFilekey(const Filekey: Integer; const BenefitID: Integer; const FromDate: WideString; const ToDate: WideString); stdcall;
    procedure removeEmployeeBenefitAdjustmentsByIDNum(const IDNum: WideString; const BenefitID: Integer; const FromDate: WideString; const ToDate: WideString); stdcall;
    function  extractEmployeeBenefitActivityByFilekey(const Filekey: Integer; const BenefitID: Integer; const FromDate: WideString; const ToDate: WideString): TAeEmpBenefitActivityAry; stdcall;
    function  extractEmployeeBenefitActivityByIDNum(const IDNum: WideString; const BenefitID: Integer; const FromDate: WideString; const ToDate: WideString): TAeEmpBenefitActivityAry; stdcall;
    procedure setEmployeeESSPINByFilekey(const Filekey: Integer; const PIN: WideString); stdcall;
    procedure setEmployeeESSPINByIDNum(const IDNum: WideString; const PIN: WideString); stdcall;
    function  getEmployeeESSPINByFilekey(const Filekey: Integer): WideString; stdcall;
    function  getEmployeeESSPINByIDNum(const IDNum: WideString): WideString; stdcall;
    procedure setEmployeeESSEMailByFilekey(const Filekey: Integer; const EMail: WideString); stdcall;
    procedure setEmployeeESSEMailByIDNum(const IDNum: WideString; const EMail: WideString); stdcall;
    function  getEmployeeESSEMailByFilekey(const Filekey: Integer): WideString; stdcall;
    function  getEmployeeESSEMailByIDNum(const IDNum: WideString): WideString; stdcall;
    function  getEmptyRatePayTypeAssignment: TAeRatePayTypeAssignment; stdcall;
    function  getEmployeeRateHistoryByFilekey(const Filekey: Integer; const MinDate: WideString; const MaxDate: WideString): TAeRateAssignmentAry; stdcall;
    function  getEmployeeRateHistoryByIDNum(const IDNum: WideString; const MinDate: WideString; const MaxDate: WideString): TAeRateAssignmentAry; stdcall;
    procedure removeEmployeeRateHistoryByFilekey(const Filekey: Integer; const MinDate: WideString; const MaxDate: WideString); stdcall;
    procedure removeEmployeeRateHistoryByIDNum(const IDNum: WideString; const MinDate: WideString; const MaxDate: WideString); stdcall;
    function  getEmployeeFilekeyByBadge(const BadgeNum: Integer): Integer; stdcall;
    function  loadUsageItemsInRange(const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeUsageItemAry; stdcall;
    function  getAccessAccountViaUniqueID(const UniqueID: Integer): TAeAccessAccount; stdcall;
    function  loadAccessAccountActivitysInRange(const UniqueID: Integer; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeAccessAccountActivityAry; stdcall;
    function  extractTimeClockStations: TAeTimeClockStationAry; stdcall;
    function  loadBillingItemsInRange(const ItemType: Integer; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAeBillingItemAry; stdcall;
    procedure recomputeEmployeeIncidentsByFilekey(const Filekey: Integer; const DateRangeEnum: TDateRangeEnum); stdcall;
    procedure recomputeEmployeeIncidentsByIDNum(const IDNum: WideString; const DateRangeEnum: TDateRangeEnum); stdcall;
    function  dateRangeEnumStartDate(const DateRangeEnum: TDateRangeEnum): WideString; stdcall;
    function  dateRangeEnumEndDate(const DateRangeEnum: TDateRangeEnum): WideString; stdcall;
    function  ClientServerDateTime: WideString; stdcall;
    function  ServerUTCDateTime: WideString; stdcall;
    procedure setWorkgroupLevelLabels(const AeWorkgroupLevel: TAeWorkgroupLevel); stdcall;
    function  extractEmployeePeriodShiftDetailsByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAeShiftLineDetailAry; stdcall;
    function  extractEmployeePeriodShiftDetailsByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAeShiftLineDetailAry; stdcall;
    function  extractEmployeePeriodShiftTransactionsByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum; const ShiftPosIndex: Integer): TAeShiftTransactionAry; stdcall;
    function  extractEmployeePeriodShiftTransactionsByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum; const ShiftPosIndex: Integer): TAeShiftTransactionAry; stdcall;
    function  extractEmployeePeriodShiftSchedulesByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAeShiftScheduleAry; stdcall;
    function  extractEmployeePeriodShiftSchedulesByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAeShiftScheduleAry; stdcall;
    function  extractEmployeeDailySummsInPeriodByFilekey(const Filekey: Integer; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAePayLineAry; stdcall;
    function  extractEmployeeDailySummsInPeriodByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const PayLineStateEnum: TPayLineStateEnum; const CalcedDataTypeEnum: TCalcedDataTypeEnum): TAePayLineAry; stdcall;
    function  getCurrentAccessAccount: TAeAccessAccount; stdcall;
    procedure performTimeCardEdit(const AeEdit: TAeEdit; const RecomputeImmediately: Boolean); stdcall;
    procedure cancelTimeCardEdit(const AeEdit: TAeEdit); stdcall;
    procedure deleteTimeCardEdit(const AeEdit: TAeEdit); stdcall;
    procedure authPeriodTimeCardByFileKey(const FileKey: Integer; const PayPeriodEnum: TPayPeriodEnum; const AuthTimeCardOptionEnum: TAuthTimeCardOptionEnum); stdcall;
    procedure authPeriodTimeCardByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum; const AuthTimeCardOptionEnum: TAuthTimeCardOptionEnum); stdcall;
    procedure unAuthPeriodTimeCardByFileKey(const FileKey: Integer; const PayPeriodEnum: TPayPeriodEnum); stdcall;
    procedure unAuthPeriodTimeCardByIDNum(const IDNum: WideString; const PayPeriodEnum: TPayPeriodEnum); stdcall;
    procedure submitTimeOffRequest(const AeTimeOffRequest: TAeTimeOffRequest); stdcall;
    function  extractTimeOffRequestStates: TAeTimeOffRequestStatesAry; stdcall;
    function  extractLeaveRequestsByState(const StateID: Integer; const ActiveStatusEnum: TActiveStatusEnum; const MinDate: WideString; const MaxDate: WideString): TAeTimeOffRequestAry; stdcall;
    function  extractEmployeeLeaveRequestsByFilekey(const Filekey: Integer; const RTOFilterEnum: TRTOFilterEnum; const StateID: Integer; const MinDate: WideString; const MaxDate: WideString): TAeTimeOffRequestAry; stdcall;
    function  extractEmployeeLeaveRequestsByIDNum(const IDNum: WideString; const RTOFilterEnum: TRTOFilterEnum; const StateID: Integer; const MinDate: WideString; const MaxDate: WideString): TAeTimeOffRequestAry; stdcall;
    procedure removeLeaveRequestEntry(const AeTimeOffRequest: TAeTimeOffRequest); stdcall;
    function  describeLeaveRequestEntry(const UniqueID: Integer): WideString; stdcall;
    procedure approveLeaveRequestEntry(const UniqueID: Integer); stdcall;
    procedure denyLeaveRequestEntry(const UniqueID: Integer); stdcall;
    function  getEmployeesSearchListBasic(const WildCard: WideString; const Phoenetic: Boolean; const MaxRecords: Integer): TAeEmployeeBasicAry; stdcall;
    function  getEmployeesSearchListDetail(const WildCard: WideString; const Phoenetic: Boolean; const MaxRecords: Integer): TAeEmployeeDetailAry; stdcall;
    procedure setEmployeeTZByFilekey(const Filekey: Integer; const Offset: Integer; const ObservesDST: Boolean); stdcall;
    procedure setEmployeeTZByIDNum(const IDNum: WideString; const Offset: Integer; const ObservesDST: Boolean); stdcall;
    function  getEmployeeTZOffsetByFilekey(const Filekey: Integer): Integer; stdcall;
    function  getEmployeeTZOffsetByIDNum(const IDNum: WideString): Integer; stdcall;
    function  getEmployeeTZObservesDSTByFilekey(const Filekey: Integer): Boolean; stdcall;
    function  getEmployeeTZObservesDSTByIDNum(const IDNum: WideString): Boolean; stdcall;
    function  getReasonCodesSimple: TAeBasicDataItemsAry; stdcall;
    function  extractEmployeeSummsByFilekey(const Filekey: Integer; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAePayLineAry; stdcall;
    function  extractEmployeeSummsByIDNum(const IDNum: WideString; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAePayLineAry; stdcall;
    function  extractEmployeeDurationTotalsByFilekey(const Filekey: Integer; const DurationDetailEnum: TDurationDetailEnum; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAePayLineAry; stdcall;
    function  extractEmployeeDurationTotalsByIDNum(const IDNum: WideString; const DurationDetailEnum: TDurationDetailEnum; const DateRangeEnum: TDateRangeEnum; const MinDate: WideString; const MaxDate: WideString): TAePayLineAry; stdcall;
    function  getEmptyEmployeeDetail2: TAeEmployeeDetail2; stdcall;
    function  getEmployeeDetail2ByIDNum(const IDNum: WideString): TAeEmployeeDetail2; stdcall;
    function  getEmployeeDetail2BySSN(const SSN: WideString): TAeEmployeeDetail2; stdcall;
    function  getEmployeeDetail2ByBadge(const Badge: Integer): TAeEmployeeDetail2; stdcall;
    function  getEmployeeDetail2ByFilekey(const Filekey: Integer): TAeEmployeeDetail2; stdcall;
    procedure setEmployeeDetail2(const AeEmployeeDetail2: TAeEmployeeDetail2); stdcall;
    function  maintainEmployeeDetail2(const AeEmployeeDetail2: TAeEmployeeDetail2; const AddEmpMode: TAddEmpMode; const BadgeManagement: TBadgeManagement): TMaintainEmployeeResult; stdcall;
    function  getEmployeesListDetail2FromHyperQuery(const HyperQueryName: WideString): TAeEmployeeDetail2Ary; stdcall;
    function  getEmployeesSearchListDetail2(const WildCard: WideString; const Phoenetic: Boolean; const MaxRecords: Integer): TAeEmployeeDetail2Ary; stdcall;
    function  getManuallySelectedSetupData(const AeExchangeStruct: TAeExchangeStruct): TAeExchangeStruct; stdcall;
    function  setManuallySelectedSetupData(const AeExchangeStruct: TAeExchangeStruct): TAeExchangeStruct; stdcall;
    function  getManuallySelectedEmployeeData(const AeExchangeStruct: TAeExchangeStruct): TAeExchangeStruct; stdcall;
    function  getManuallySelectedCalculatedData(const AeExchangeStruct: TAeExchangeStruct): TAeExchangeStruct; stdcall;
    function  setManuallySelectedEmployeeData(const AeExchangeStruct: TAeExchangeStruct): TAeExchangeStruct; stdcall;
    function  extractWGXlates: TAeWorkgroupXLateAry; stdcall;
    procedure maintainWGXlate(const AeWorkgroupXLate: TAeWorkgroupXLate); stdcall;
    procedure removeWGXlate(const AeWorkgroupXLate: TAeWorkgroupXLate); stdcall;
  end;

function GetIAeXMLBridge(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IAeXMLBridge;


implementation

function GetIAeXMLBridge(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IAeXMLBridge;
const
  defWSDL = 'E:\job\IS\integration\EvoAoD\xml\IAeXMLBridge.wsdl';
  defURL  = 'https://evolutiondev.attendanceondemand.com:8192/cc1exec.aew/soap/IAeXMLBridge';
  defSvc  = 'IAeXMLBridgeservice';
  defPrt  = 'IAeXMLBridgePort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IAeXMLBridge);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(IAeXMLBridge), 'urn:AeXMLBridgeIntf-IAeXMLBridge', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IAeXMLBridge), 'urn:AeXMLBridgeIntf-IAeXMLBridge#%operationName%');
  InvRegistry.RegisterHeaderClass(TypeInfo(IAeXMLBridge), TSessionHeader, 'TSessionHeader', 'urn:AeXMLBridgeIntf');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TBrowserProfileTypeEnum), 'urn:AeXMLBridgeIntf', 'TBrowserProfileTypeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TDateRangeEnum), 'urn:AeXMLBridgeIntf', 'TDateRangeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TDayOfWeekEnum), 'urn:AeXMLBridgeIntf', 'TDayOfWeekEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TDestinationOptionsEnum), 'urn:AeXMLBridgeIntf', 'TDestinationOptionsEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TFileConversionOptionsEnum), 'urn:AeXMLBridgeIntf', 'TFileConversionOptionsEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TFileFormatEnum), 'urn:AeXMLBridgeIntf', 'TFileFormatEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TSchTypeEnum), 'urn:AeXMLBridgeIntf', 'TSchTypeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TRequestTimeOffTypeEnum), 'urn:AeXMLBridgeIntf', 'TRequestTimeOffTypeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TRTOShiftPartTypeEnum), 'urn:AeXMLBridgeIntf', 'TRTOShiftPartTypeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAddEmpMode), 'urn:AeXMLBridgeIntf', 'TAddEmpMode');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TBadgeManagement), 'urn:AeXMLBridgeIntf', 'TBadgeManagement');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TMaintainEmployeeResult), 'urn:AeXMLBridgeIntf', 'TMaintainEmployeeResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TPayPeriodEnum), 'urn:AeXMLBridgeIntf', 'TPayPeriodEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TWGSortingOption), 'urn:AeXMLBridgeIntf', 'TWGSortingOption');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TPayLineStateEnum), 'urn:AeXMLBridgeIntf', 'TPayLineStateEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TCalcedDataTypeEnum), 'urn:AeXMLBridgeIntf', 'TCalcedDataTypeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TNoActivityInclusion), 'urn:AeXMLBridgeIntf', 'TNoActivityInclusion');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TBenAdjTypeEnum), 'urn:AeXMLBridgeIntf', 'TBenAdjTypeEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAuthTimeCardOptionEnum), 'urn:AeXMLBridgeIntf', 'TAuthTimeCardOptionEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TRTOUsageStateEnum), 'urn:AeXMLBridgeIntf', 'TRTOUsageStateEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TActiveStatusEnum), 'urn:AeXMLBridgeIntf', 'TActiveStatusEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TRTOFilterEnum), 'urn:AeXMLBridgeIntf', 'TRTOFilterEnum');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TDurationDetailEnum), 'urn:AeXMLBridgeIntf', 'TDurationDetailEnum');
  RemClassRegistry.RegisterXSClass(TAeSiteEnvironment, 'urn:AeXMLBridgeIntf', 'TAeSiteEnvironment');
  RemClassRegistry.RegisterXSClass(TAeClientData, 'urn:AeXMLBridgeIntf', 'TAeClientData');
  RemClassRegistry.RegisterXSClass(TAeAccessAccount, 'urn:AeXMLBridgeIntf', 'TAeAccessAccount');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeAccessAccountAry), 'urn:AeXMLBridgeIntf', 'TAeAccessAccountAry');
  RemClassRegistry.RegisterXSClass(TAeClientReport, 'urn:AeXMLBridgeIntf', 'TAeClientReport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeClientReportAry), 'urn:AeXMLBridgeIntf', 'TAeClientReportAry');
  RemClassRegistry.RegisterXSClass(TAeClientExport, 'urn:AeXMLBridgeIntf', 'TAeClientExport');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeClientExportAry), 'urn:AeXMLBridgeIntf', 'TAeClientExportAry');
  RemClassRegistry.RegisterXSClass(TAeClientVendorOp, 'urn:AeXMLBridgeIntf', 'TAeClientVendorOp');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeClientVendorOpAry), 'urn:AeXMLBridgeIntf', 'TAeClientVendorOpAry');
  RemClassRegistry.RegisterXSClass(TAeBasicDataItem, 'urn:AeXMLBridgeIntf', 'TAeBasicDataItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeBasicDataItemsAry), 'urn:AeXMLBridgeIntf', 'TAeBasicDataItemsAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TStringArray), 'urn:AeXMLBridgeIntf', 'TStringArray');
  RemClassRegistry.RegisterXSClass(TAeBrowserMenuItem, 'urn:AeXMLBridgeIntf', 'TAeBrowserMenuItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeBrowserMenuItemsAry), 'urn:AeXMLBridgeIntf', 'TAeBrowserMenuItemsAry');
  RemClassRegistry.RegisterXSClass(TAePayPeriodFrame, 'urn:AeXMLBridgeIntf', 'TAePayPeriodFrame');
  RemClassRegistry.RegisterXSClass(TAePayPeriodInfo, 'urn:AeXMLBridgeIntf', 'TAePayPeriodInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAePayPeriodInfoAry), 'urn:AeXMLBridgeIntf', 'TAePayPeriodInfoAry');
  RemClassRegistry.RegisterXSClass(TAeEmployeeBasic, 'urn:AeXMLBridgeIntf', 'TAeEmployeeBasic');
  RemClassRegistry.RegisterXSClass(TAeEmployeeDetail, 'urn:AeXMLBridgeIntf', 'TAeEmployeeDetail');
  RemClassRegistry.RegisterXSClass(TAeEmployeeDetail2, 'urn:AeXMLBridgeIntf', 'TAeEmployeeDetail2');
  RemClassRegistry.RegisterXSClass(TAePayLine, 'urn:AeXMLBridgeIntf', 'TAePayLine');
  RemClassRegistry.RegisterXSClass(TAeShiftLineSimple, 'urn:AeXMLBridgeIntf', 'TAeShiftLineSimple');
  RemClassRegistry.RegisterXSClass(TAeSchedule, 'urn:AeXMLBridgeIntf', 'TAeSchedule');
  RemClassRegistry.RegisterXSClass(TAeEdit, 'urn:AeXMLBridgeIntf', 'TAeEdit');
  RemClassRegistry.RegisterXSClass(TAeEmpTransaction, 'urn:AeXMLBridgeIntf', 'TAeEmpTransaction');
  RemClassRegistry.RegisterXSClass(TAeEmpBenefitActivity, 'urn:AeXMLBridgeIntf', 'TAeEmpBenefitActivity');
  RemClassRegistry.RegisterXSClass(TAeTimeOffRequest, 'urn:AeXMLBridgeIntf', 'TAeTimeOffRequest');
  RemClassRegistry.RegisterXSClass(TAeWorkgroupAssignment, 'urn:AeXMLBridgeIntf', 'TAeWorkgroupAssignment');
  RemClassRegistry.RegisterXSClass(TAeRatePayTypeAssignment, 'urn:AeXMLBridgeIntf', 'TAeRatePayTypeAssignment');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeEmployeeBasicAry), 'urn:AeXMLBridgeIntf', 'TAeEmployeeBasicAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeEmployeeDetailAry), 'urn:AeXMLBridgeIntf', 'TAeEmployeeDetailAry');
  RemClassRegistry.RegisterXSClass(TAeWorkgroupLevel, 'urn:AeXMLBridgeIntf', 'TAeWorkgroupLevel');
  RemClassRegistry.RegisterXSClass(TAeWorkgroup, 'urn:AeXMLBridgeIntf', 'TAeWorkgroup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeWorkgroupAry), 'urn:AeXMLBridgeIntf', 'TAeWorkgroupAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAePayLineAry), 'urn:AeXMLBridgeIntf', 'TAePayLineAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeShiftLineSimpleAry), 'urn:AeXMLBridgeIntf', 'TAeShiftLineSimpleAry');
  RemClassRegistry.RegisterXSClass(TAeTransaction, 'urn:AeXMLBridgeIntf', 'TAeTransaction');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeTransactionAry), 'urn:AeXMLBridgeIntf', 'TAeTransactionAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeScheduleAry), 'urn:AeXMLBridgeIntf', 'TAeScheduleAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeEditAry), 'urn:AeXMLBridgeIntf', 'TAeEditAry');
  RemClassRegistry.RegisterXSClass(TAeClientVendorOpResults, 'urn:AeXMLBridgeIntf', 'TAeClientVendorOpResults');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeClientVendorOpResultsAry), 'urn:AeXMLBridgeIntf', 'TAeClientVendorOpResultsAry');
  RemClassRegistry.RegisterXSClass(TAePayPeriodCloseInfo, 'urn:AeXMLBridgeIntf', 'TAePayPeriodCloseInfo');
  RemClassRegistry.RegisterXSClass(TAeWorkgroupSet, 'urn:AeXMLBridgeIntf', 'TAeWorkgroupSet');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeWorkgroupSetAry), 'urn:AeXMLBridgeIntf', 'TAeWorkgroupSetAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeEmpTransactionAry), 'urn:AeXMLBridgeIntf', 'TAeEmpTransactionAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeEmpBenefitActivityAry), 'urn:AeXMLBridgeIntf', 'TAeEmpBenefitActivityAry');
  RemClassRegistry.RegisterXSClass(TAeRateAssignment, 'urn:AeXMLBridgeIntf', 'TAeRateAssignment');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeRateAssignmentAry), 'urn:AeXMLBridgeIntf', 'TAeRateAssignmentAry');
  RemClassRegistry.RegisterXSClass(TAeUsageItem, 'urn:AeXMLBridgeIntf', 'TAeUsageItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeUsageItemAry), 'urn:AeXMLBridgeIntf', 'TAeUsageItemAry');
  RemClassRegistry.RegisterXSClass(TAeAccessAccountActivity, 'urn:AeXMLBridgeIntf', 'TAeAccessAccountActivity');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeAccessAccountActivityAry), 'urn:AeXMLBridgeIntf', 'TAeAccessAccountActivityAry');
  RemClassRegistry.RegisterXSClass(TAeTimeClockStation, 'urn:AeXMLBridgeIntf', 'TAeTimeClockStation');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeTimeClockStationAry), 'urn:AeXMLBridgeIntf', 'TAeTimeClockStationAry');
  RemClassRegistry.RegisterXSClass(TAeBillingItem, 'urn:AeXMLBridgeIntf', 'TAeBillingItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeBillingItemAry), 'urn:AeXMLBridgeIntf', 'TAeBillingItemAry');
  RemClassRegistry.RegisterXSClass(TAeShiftLineDetail, 'urn:AeXMLBridgeIntf', 'TAeShiftLineDetail');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeShiftLineDetailAry), 'urn:AeXMLBridgeIntf', 'TAeShiftLineDetailAry');
  RemClassRegistry.RegisterXSClass(TAeShiftTransaction, 'urn:AeXMLBridgeIntf', 'TAeShiftTransaction');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeShiftTransactionAry), 'urn:AeXMLBridgeIntf', 'TAeShiftTransactionAry');
  RemClassRegistry.RegisterXSClass(TAeShiftSchedule, 'urn:AeXMLBridgeIntf', 'TAeShiftSchedule');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeShiftScheduleAry), 'urn:AeXMLBridgeIntf', 'TAeShiftScheduleAry');
  RemClassRegistry.RegisterXSClass(TAeTimeOffRequestState, 'urn:AeXMLBridgeIntf', 'TAeTimeOffRequestState');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeTimeOffRequestStatesAry), 'urn:AeXMLBridgeIntf', 'TAeTimeOffRequestStatesAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeTimeOffRequestAry), 'urn:AeXMLBridgeIntf', 'TAeTimeOffRequestAry');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeEmployeeDetail2Ary), 'urn:AeXMLBridgeIntf', 'TAeEmployeeDetail2Ary');
  RemClassRegistry.RegisterXSClass(TAeExchangeStruct, 'urn:AeXMLBridgeIntf', 'TAeExchangeStruct');
  RemClassRegistry.RegisterXSClass(TAeWorkgroupXLate, 'urn:AeXMLBridgeIntf', 'TAeWorkgroupXLate');
  RemClassRegistry.RegisterXSInfo(TypeInfo(TAeWorkgroupXLateAry), 'urn:AeXMLBridgeIntf', 'TAeWorkgroupXLateAry');
  RemClassRegistry.RegisterXSClass(TSessionHeader, 'urn:AeXMLBridgeIntf', 'TSessionHeader');

end. 