inherited ExtAppConnectionParamDlg: TExtAppConnectionParamDlg
  Width = 402
  Height = 82
  inline ExtAppConnectionParamFrame: TExtAppConnectionParamFrm
    Left = 0
    Top = 0
    Width = 402
    Height = 82
    Align = alClient
    TabOrder = 0
    inherited lblUserName: TLabel
      Left = 14
    end
    inherited lblPassword: TLabel
      Left = 14
    end
    inherited lblSite: TLabel
      Left = 14
    end
    inherited PasswordEdit: TPasswordEdit
      Left = 95
      Width = 298
      Anchors = [akLeft, akTop, akRight]
    end
    inherited UserNameEdit: TEdit
      Left = 95
      Width = 298
      Anchors = [akLeft, akTop, akRight]
    end
    inherited SiteEdit: TEdit
      Left = 95
      Width = 298
      Anchors = [akLeft, akTop, akRight]
    end
  end
end
