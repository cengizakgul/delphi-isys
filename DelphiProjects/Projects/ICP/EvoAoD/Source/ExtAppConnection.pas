unit extappconnection;

interface

uses
  extappdecl, gdyCommonLogger;


function CreateExtAppConnection(param: TExtAppConnectionParam; logger: ICommonLogger): IExtAppConnection;

implementation

uses
  tcconnectionbase, evoapiconnectionutils, InvokeRegistry, sysutils,
  rio, gdycommon, gdyRedir, common, XMLDoc, variants, gdystrset, XMLIntf,
  timeclockimport, IAeXMLBridge1, SOAPHTTPTrans, windows, typinfo;

const
  sCtxComponentExtApp = ExtAppName + ' connection';

type
  TExtAppConnection = class (TTCConnectionBase, IExtAppConnection)
  public
    constructor Create(param: TExtAppConnectionParam; logger: ICommonLogger);
  private
    FService: IAeXMLBridge;

    function CheckXML(doc: IXMLDocument): IXMLDocument; override; //unused
    procedure LogMaintainEmployeeResult(r: TMaintainEmployeeResult);

    {IExtAppConnection}
    function  getWorkgroupLevelDetails(const WGLevel: Integer): TAeWorkgroupLevel;
    function  getActiveStatusConditions: TAeBasicDataItemsAry;
    function  getInactiveStatusConditions: TAeBasicDataItemsAry;
    function  getAllEmployeesList: TAeEmployeeBasicAry;
    function  getEmployeeDetail2ByFilekey(const Filekey: Integer): TAeEmployeeDetail2;
    function  getEmployeeDetailByFilekey(const Filekey: Integer): TAeEmployeeDetail;
    procedure AddEmployee(ee: TAeEmployeeDetail; autoBadge: boolean);
    procedure UpdateEmployee(ee: TAeEmployeeDetail; autoBadge: boolean);
    function  getPayClassesSimple: TAeBasicDataItemsAry;
    function  getBaseClientData: TAeClientData;
    function  getWorkgroups(const WGLevel: Integer; const WGSortingOption: TWGSortingOption): TAeWorkgroupAry;
    function  getEmployeeESSEMailByFilekey(const Filekey: Integer): WideString;
    procedure setEmployeeESSEMailByFilekey(const Filekey: Integer; const EMail: WideString);
    procedure setEmployeeESSEMailByIDNum(const IDNum: WideString; const EMail: WideString);
    function  extractEmployeeDailySummsByFilekey(const Filekey: Integer; const Date: TDateTime): TAePayLineAry;
    function  getPayDesignationsSimple: TAeBasicDataItemsAry;
    function  getBenefitsSimple: TAeBasicDataItemsAry;
    procedure adjustEmployeeBenefitBalanceByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: TDateTime; BenAdjType: TBenAdjTypeEnum; const Amount: Double);
    function  extractEmployeeBenefitActivityByFilekey(const Filekey: Integer; const BenefitID: Integer; const FromDate: TDateTime; const ToDate: TDateTime): TAeEmpBenefitActivityAry;
    function  employeeBenefitBalanceAsOfByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: TDateTime): Double;
    function  getHourlyStatusTypesSimple: TAeBasicDataItemsAry;
    function  getClockGroupsSimple: TAeBasicDataItemsAry;
    function  getSchedulePatternsSimple: TAeBasicDataItemsAry;
    function  extractEmployeePeriodFrameByIDNum(const IDNum: WideString): TAePayPeriodFrame;
    function  getHyperQueriesSimple: TStringArray;
    function  getEmployeesListDetail2FromHyperQuery(const HyperQueryName: WideString): TAeEmployeeDetail2Ary;
    function  getEmployeesListBasicFromHyperQuery(const HyperQueryName: WideString): TAeEmployeeBasicAry;
  end;

function CreateExtAppConnection(param: TExtAppConnectionParam; logger: ICommonLogger): IExtAppConnection;
begin
  Result := TExtAppConnection.Create(param, logger);
end;

{ TExtAppConnection }

function TExtAppConnection.CheckXML(doc: IXMLDocument): IXMLDocument;
begin
  Assert(false);
end;

constructor TExtAppConnection.Create(param: TExtAppConnectionParam; logger: ICommonLogger);
var
  soapUrl: string;
begin
  inherited Create(logger);
  FLogger.LogEntry('Initializing');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      LogExtAppConnectionParam(FLogger, param);
      soapUrl := Format('https://%s.attendanceondemand.com:8192/cc1exec.aew/soap/IAeXMLBridge',[trim(param.Site)]);
      FLogger.LogContextItem(ExtAppName + ' service url', soapUrl);

      FService := IAeXMLBridge1.GetIAeXMLBridge(false, soapUrl);
      SetHTTPTimeouts(FService, 30*60*1000 );
      AttachLogger(FService);
      ((FService as IRIOAccess).RIO.WebNode as IHTTPReqResp).GetHTTPReqResp.UserName := param.UserName;
      ((FService as IRIOAccess).RIO.WebNode as IHTTPReqResp).GetHTTPReqResp.Password := param.Password;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getActiveStatusConditions: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getActiveStatusConditions');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getActiveStatusConditions;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getEmployeeDetail2ByFilekey(const Filekey: Integer): TAeEmployeeDetail2;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry('getEmployeeDetail2ByFilekey');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Employee filekey', inttostr(Filekey));
      Result := FService.getEmployeeDetail2ByFilekey(Filekey);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getEmployeeDetailByFilekey(const Filekey: Integer): TAeEmployeeDetail;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry('getEmployeeDetailByFilekey');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Employee filekey', inttostr(Filekey));
      Result := FService.getEmployeeDetailByFilekey(Filekey);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getAllEmployeesList: TAeEmployeeBasicAry;
begin
  FLogger.LogEntry('getAllEmployeesList');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getAllEmployeesList;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getInactiveStatusConditions: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getInactiveStatusConditions');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getInactiveStatusConditions;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getWorkgroupLevelDetails(const WGLevel: Integer): TAeWorkgroupLevel;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry('getWorkgroupLevelDetails');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Workgroup level', sCtxComponentExtApp);
      Assert(WGLevel in [1..7]);
      Result := FService.getWorkgroupLevelDetails(WGLevel);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.LogMaintainEmployeeResult(r: TMaintainEmployeeResult);
begin
  FLogger.LogEntry('maintainEmployeeResultDescr');
  try
    try
      case r of
        merEditOk, merAddOk: ;
        merAddedWithErrors: FLogger.LogWarning( UTF8Encode(FService.maintainEmployeeResultDescr(r)) );
        merBadParam, merDupID, merDupBadge, merCap, merUnknown: raise Exception.Create( FService.maintainEmployeeResultDescr(r) );
      else
        Assert(false);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.AddEmployee(ee: TAeEmployeeDetail; autoBadge: boolean);
var
	bm: TBadgeManagement;
begin
	if autoBadge then
		bm := bmAuto
	else
	  bm := bmManual;
  FLogger.LogEntry( Format('maintainEmployeeDetail(aemAdd, %s)', [getenumname( typeinfo(TBadgeManagement), ord(bm))]) );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      LogMaintainEmployeeResult( FService.maintainEmployeeDetail(ee, aemAdd, bm) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.UpdateEmployee(ee: TAeEmployeeDetail; autoBadge: boolean);
var
	bm: TBadgeManagement;
begin
	if autoBadge then
		bm := bmAuto
	else
	  bm := bmManual;
  FLogger.LogEntry( Format('maintainEmployeeDetail(aemEdit, %s)', [getenumname( typeinfo(TBadgeManagement), ord(bm))]) );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      LogMaintainEmployeeResult( FService.maintainEmployeeDetail(ee, aemEdit, bm) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getPayClassesSimple: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getPayClassesSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getPayClassesSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getBaseClientData: TAeClientData;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry('getBaseClientData');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getBaseClientData;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getWorkgroups(const WGLevel: Integer; const WGSortingOption: TWGSortingOption): TAeWorkgroupAry;
begin
  FLogger.LogEntry('getWorkgroups');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('WGLevel', inttostr(WGLevel));
      Result := FService.getWorkgroups(WGLevel, WGSortingOption);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getEmployeeESSEMailByFilekey(const Filekey: Integer): WideString;
begin
  Result := '';
  FLogger.LogEntry('getEmployeeESSEMailByFilekey');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Employee filekey', inttostr(Filekey));
      Result := FService.getEmployeeESSEMailByFilekey(Filekey);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.setEmployeeESSEMailByFilekey(const Filekey: Integer; const EMail: WideString);
begin
  FLogger.LogEntry('setEmployeeESSEMailByFilekey');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Employee filekey', inttostr(Filekey));
      FService.setEmployeeESSEMailByFilekey(Filekey, EMail);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.setEmployeeESSEMailByIDNum(const IDNum, EMail: WideString);
begin
  FLogger.LogEntry('setEmployeeESSEMailByIDNum');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem(sCtxEECode, IDNum);
      FService.setEmployeeESSEMailByIDNum(IDNum, EMail);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.extractEmployeeDailySummsByFilekey(const Filekey: Integer; const Date: TDateTime): TAePayLineAry;
begin
  FLogger.LogEntry('extractEmployeeDailySummsByFilekey');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Employee filekey', inttostr(Filekey));
      Result := FService.extractEmployeeDailySummsByFilekey(Filekey, DateToStdDate(Date), plsAsSaved, cdtNormal {shouldn't matter});
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getPayDesignationsSimple: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getPayDesignationsSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getPayDesignationsSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getBenefitsSimple: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getBenefitsSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getBenefitsSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.adjustEmployeeBenefitBalanceByIDNum(
  const IDNum: WideString; const BenefitID: Integer;
  const SelDate: TDateTime;
  BenAdjType: TBenAdjTypeEnum; 
  const Amount: Double);
begin
  FLogger.LogEntry('adjustEmployeeBenefitBalanceByIDNum');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem(sCtxEECode, IDNum);

      FService.adjustEmployeeBenefitBalanceByIDNum(IDNum, BenefitID, DateToStdDate(SelDate), BenAdjType, Amount);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.extractEmployeeBenefitActivityByFilekey(const Filekey: Integer;
  const BenefitID: Integer; const FromDate,
  ToDate: TDateTime): TAeEmpBenefitActivityAry;
begin
  FLogger.LogEntry('extractEmployeeBenefitActivityByFilekey');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Employee filekey', inttostr(Filekey));

      Result := FService.extractEmployeeBenefitActivityByFilekey(Filekey, BenefitID, DateToStdDate(FromDate), DateToStdDate(ToDate));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.employeeBenefitBalanceAsOfByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: TDateTime): Double;
begin
  Result := 0; //to make compiler happy
  FLogger.LogEntry('employeeBenefitBalanceAsOfByIDNum');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem(sCtxEECode, IDNum);

      Result := FService.employeeBenefitBalanceAsOfByIDNum(IDNum, BenefitID, DateToStdDate(SelDate));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getHourlyStatusTypesSimple: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getHourlyStatusTypesSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getHourlyStatusTypesSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getClockGroupsSimple: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getClockGroupsSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getClockGroupsSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.extractEmployeePeriodFrameByIDNum(const IDNum: WideString): TAePayPeriodFrame;
begin
  Result := nil;// to make compiler happy
  FLogger.LogEntry('extractEmployeePeriodFrameByIDNum');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.extractEmployeePeriodFrameByIDNum(IDNum);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getSchedulePatternsSimple: TAeBasicDataItemsAry;
begin
  FLogger.LogEntry('getSchedulePatternsSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getSchedulePatternsSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getHyperQueriesSimple: TStringArray;
begin
  Result := nil;
  FLogger.LogEntry('getHyperQueriesSimple');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      Result := FService.getHyperQueriesSimple;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getEmployeesListDetail2FromHyperQuery(
  const HyperQueryName: WideString): TAeEmployeeDetail2Ary;
begin
  FLogger.LogEntry('getEmployeesListDetail2FromHyperQuery');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Hyper Query Name', HyperQueryName);
      Result := FService.getEmployeesListDetail2FromHyperQuery(HyperQueryName);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.getEmployeesListBasicFromHyperQuery(
  const HyperQueryName: WideString): TAeEmployeeBasicAry;
begin
  FLogger.LogEntry('getEmployeesListBasicFromHyperQuery');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
      FLogger.LogContextItem('Hyper Query Name', HyperQueryName);
      Result := FService.getEmployeesListBasicFromHyperQuery(HyperQueryName);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

initialization
  {
  http://www.borlandtalk.com/namespace-in-soapheader-how-to-change-vt112473.html

  This might or might not be related but just in case: there was a bug whereby
  Delphi would not recognize document services. The importer would miss
  emitting the call to register the interface. Something along the lines of:

  InvRegistry.RegisterInvokeOptions(TypeInfo(InterfaceName), ioDocument);

  The results of that bug is that serialization would use Section-5 encoding
  rules... which matches what you're seeing.

  ...the issue of not properly detecting document services should be addressed as of HOTFIX10 of BDS2006.
  }
  InvRegistry.RegisterInvokeOptions(TypeInfo(IAeXMLBridge1.IAeXMLBridge), ioDocument);


end.
