unit ExtAppTasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, common,
  evodata, classes, scheduledCustomTask, extappdecl, timeclockimport,
  extappconnection, evoapiconnectionutils, toaimport;

type
  TExtAppTaskAdapter = class(TTaskAdapterCustomBase)
  private
    function GetExtAppConnectionParam: TExtAppConnectionParam;
  protected
    function GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData; override;
    function DoExportTOA: TTOAStat; override;
    function DoGetTOA: TTOAImportData; override;
  published
    procedure EEExport_Execute;
    function EEExport_Describe: string;
    function TCImport_Describe: string;
    function TOAExport_Describe: string;
    function TOAImport_Describe: string;
  end;


function NewEEExportTask(ClCo: TEvoCompanyDef; EEExportOptions: TEEExportOptions; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
function NewTCImportTask(ClCo: TEvoCompanyDef; TCImportOptions: TTCImportOptions; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
function NewTOAExportTask(ClCo: TEvoCompanyDef; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
function NewTOAImportTask(ClCo: TEvoCompanyDef; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection,
  gdyclasses, gdyCommon, dialogs,
  gdyDialogEngine, controls, kbmMemTable, EEExportDialog, TCImportDialog, extapp, dbdtmapper;


function NewEEExportTask(ClCo: TEvoCompanyDef; EEExportOptions: TEEExportOptions; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
begin
  Result := NewTask('EEExport', 'Employee Export', ClCo);

  SaveEEExportOptions( EEExportOptions, Result.ParamSettings, '' );
  SaveExtAppOptions( LoadExtAppOptions(AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );
  SaveDBDTMapperData( Result.ParamSettings, '', LoadDBDTMapperData(AppSettings, CompanyUniquePath(Result.Company)) );
  Result.ParamSettings.AsString['EEStatusMapping\DataPacket'] := AppSettings.AsString[CompanyUniquePath(Result.Company)+'\EEStatusMapping\DataPacket'];
  Result.ParamSettings.AsString['CustomFieldsMapping\DataPacket'] := AppSettings.AsString[CompanyUniquePath(Result.Company)+'\CustomFieldsMapping\DataPacket'];

  SaveExtAppConnectionParam( LoadExtAppConnectionParam( AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );
  SaveEvoAPIConnectionParam( EvoApiConnParam, Result.ParamSettings, '');
end;

function NewTCImportTask(ClCo: TEvoCompanyDef; TCImportOptions: TTCImportOptions; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
begin
  Result := NewTask('TCImport', 'Timeclock Data Import', ClCo);
  SaveTCImportOptions( TCImportOptions, Result.ParamSettings, '' );

  SaveExtAppConnectionParam( LoadExtAppConnectionParam( AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );
  SaveEvoAPIConnectionParam( EvoApiConnParam, Result.ParamSettings, '');
  SaveDBDTMapperData( Result.ParamSettings, '', LoadDBDTMapperData(AppSettings, CompanyUniquePath(Result.Company)) );
  Result.ParamSettings.AsString['EDCodeMapping\DataPacket'] := AppSettings.AsString[CompanyUniquePath(Result.Company)+'\EDCodeMapping\DataPacket'];
end;

function NewTOAExportTask(ClCo: TEvoCompanyDef; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
begin
  Result := NewTask('TOAExport', 'TOA Export', ClCo);

  SaveExtAppConnectionParam( LoadExtAppConnectionParam( AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );
  SaveEvoAPIConnectionParam( EvoApiConnParam, Result.ParamSettings, '');
  SaveExtAppOptions( LoadExtAppOptions(AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );

  Result.ParamSettings.AsString['TOAMapping\DataPacket'] := AppSettings.AsString[CompanyUniquePath(Result.Company)+'\TOAMapping\DataPacket'];
end;

function NewTOAImportTask(ClCo: TEvoCompanyDef; EvoApiConnParam: TEvoAPIConnectionParam): IScheduledTask;
begin
  Result := NewTask('TOAImport', 'TOA Import', ClCo);

  SaveExtAppConnectionParam( LoadExtAppConnectionParam( AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );
  SaveEvoAPIConnectionParam( EvoApiConnParam, Result.ParamSettings, '');
  SaveExtAppOptions( LoadExtAppOptions(AppSettings, CompanyUniquePath(Result.Company)), Result.ParamSettings, '' );

  Result.ParamSettings.AsString['TOAMapping\DataPacket'] := AppSettings.AsString[CompanyUniquePath(Result.Company)+'\TOAMapping\DataPacket'];
end;

procedure EditEEExportTask(task: IScheduledTask; Owner: TComponent);
var
  dlg: TEEExportDlg;
begin
  dlg := TEEExportDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.EEExportOptionsFrame.Options := LoadEEExportOptions(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveEEExportOptions( dlg.EEExportOptionsFrame.Options, task.ParamSettings, '' );
  finally
    FreeAndNil(dlg);
  end;
end;

procedure EditTCImportTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);
var
  dlg: TTCImportDlg;
begin
  dlg := TTCImportDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.TCImportOptionsFrame.Options := LoadTCImportOptions(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveTCImportOptions(dlg.TCImportOptionsFrame.Options, task.ParamSettings, '');
  finally
    FreeAndNil(dlg);
  end;
end;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);
begin
  if task.Name = 'TCImport' then
    EditTCImportTask(task, Logger, Owner)
  else if task.Name = 'EEExport' then
    EditEEExportTask(task, Owner)
  else if task.Name = 'TOAExport' then
    ShowMessage('TOA Export task has no parameters')
  else if task.Name = 'TOAImport' then
    ShowMessage('TOA Import task has no parameters')
  else
    Assert(false);
end;

{ TExtAppTaskAdapter }

function TExtAppTaskAdapter.EEExport_Describe: string;
begin
  Result := DescribeEEExportOptions( LoadEEExportOptions(FTask.ParamSettings, '') );
end;

procedure TExtAppTaskAdapter.EEExport_Execute;
var
  opname: string;
  EeExportOptions: TEEExportOptions;
  ExtAppOptions: TExtAppOptions;
  EEStatusMatcher: IMatcher;
  DBDTMapper: IDBDTMapper;
  CustomFieldsMatcher: IMatcher;

  procedure DoIt;
  var
    stat: TUpdateEmployeesStat;
    EEData: TEvoEEData;
    analysisResult: TAnalyzeResult;
    updater: TExtAppEmployeeUpdater;
  begin
    EEData := TEvoEEData.Create( GetEvoAPICOnnection, FLogger, FTask.Company);
    try
      updater := TExtAppEmployeeUpdater.Create( CreateExtAppConnection(GetExtAppConnectionParam, FLogger), EeExportOptions, ExtAppOptions, EeStatusMatcher, DBDTMapper, CustomFieldsMatcher, FLogger, EEData );
      try
        analysisResult := updater.Analyze;
        if analysisResult.Actions.Count > 0 then
          FLogger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        stat := updater.Apply(analysisResult.Actions, NullProgressIndicator);
        stat.Errors := stat.Errors + analysisResult.Errors;
      finally
        FreeAndNil(updater);
      end;
      if stat.Errors = 0 then
        FLogger.LogEvent(opname + ' ' + SyncStatToString(stat))
      else
        FLogger.LogWarning(opname + ' ' + SyncStatToString(stat));
    finally
      FreeAndNil(EEData);
    end;
  end;

begin
  opname := Format('%s employee records update', [ExtAppName]);
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      EeExportOptions := LoadEEExportOptions(FTask.ParamSettings, '');
      ExtAppOptions := LoadExtAppOptions(FTask.ParamSettings, '');

      DBDTMapper := CreateDBDTMapper( LoadDBDTMapperData(FTask.ParamSettings, '') );
      EEStatusMatcher := CreateMatcher( EEStatusBinding, FTask.ParamSettings.AsString['EEStatusMapping\DataPacket'] );
      CustomFieldsMatcher := CreateMatcher( CustomFieldsBinding, FTask.ParamSettings.AsString['CustomFieldsMapping\DataPacket'] );

      DoIt;
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppTaskAdapter.GetExtAppConnectionParam: TExtAppConnectionParam;
begin
  Result := LoadExtAppConnectionParam(FTask.ParamSettings, ''{CompanyUniquePath(FTask.Company)});
end;

function TExtAppTaskAdapter.TCImport_Describe: string;
begin
  Result := DescribeTCImportOptions( LoadTCImportOptions(FTask.ParamSettings, '') );
end;

function TExtAppTaskAdapter.GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData;
var
  options: TTCImportOptions;
  EDCodeMatcher: IMatcher;
  DBDTMapper: IDBDTMapper;
begin
  options := LoadTCImportOptions(FTask.ParamSettings, '');
  if batch.HasUserEarningsLines and not options.AllowImportToBatchesWithUserEarningLines then //!! that's ugly that this is here but I need tc import options for this and I don't have them in the base class
    raise Exception.Create( 'This payroll batch already has checks with earnings. You may be doing import second time.' );

  DBDTMapper := CreateDBDTMapper( LoadDBDTMapperData(FTask.ParamSettings, '') );
  EDCodeMatcher := CreateMatcher( EEStatusBinding, FTask.ParamSettings.AsString['EDCodeMapping\DataPacket'] );

  FLogger.LogDebug('DBDT mapping', DBDTMapper.Dump);
  FLogger.LogDebug('EDCode mapping', EDCodeMatcher.Dump);
  Result := extapp.GetTimeClockData( CreateExtAppConnection(GetExtAppConnectionParam, FLogger), options, EDCodeMatcher, DBDTMapper, FTask.Company, FLogger, batch.PayPeriod );
end;

function TExtAppTaskAdapter.DoExportTOA: TTOAStat;
var
  TOAMatcher: IMatcher;
  ExtAppOptions: TExtAppOptions;
begin
  TOAMatcher := CreateMatcher( TOABinding, FTask.ParamSettings.AsString['TOAMapping\DataPacket'] );
  ExtAppOptions := LoadExtAppOptions(FTask.ParamSettings, CompanyUniquePath(FTask.Company));
  Result := extapp.ExportTOA( CreateExtAppConnection(GetExtAppConnectionParam, FLogger), GetEvoAPICOnnection, TOAMatcher, ExtAppOptions, FTask.Company, FLogger, NullProgressIndicator );
end;

function TExtAppTaskAdapter.TOAExport_Describe: string;
begin
  Result := '';
end;

function TExtAppTaskAdapter.TOAImport_Describe: string;
begin
  Result := '';
end;

function TExtAppTaskAdapter.DoGetTOA: TTOAImportData;
var
  TOAMatcher: IMatcher;
  DBDTMapper: IDBDTMapper;
begin
  TOAMatcher := CreateMatcher( TOABinding, FTask.ParamSettings.AsString['TOAMapping\DataPacket'] );
  DBDTMapper := CreateDBDTMapper( LoadDBDTMapperData(FTask.ParamSettings, '') );
  Result := extapp.GetTOAData(CreateExtAppConnection(GetExtAppConnectionParam, FLogger), GetEvoAPICOnnection, TOAMatcher, DBDTMapper, FTask.Company, FLogger, NullProgressIndicator );
end;

end.
