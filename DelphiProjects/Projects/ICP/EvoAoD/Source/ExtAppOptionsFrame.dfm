inherited ExtAppOptionsFrm: TExtAppOptionsFrm
  Width = 318
  Height = 82
  object rgTOA: TRadioGroup
    Left = 0
    Top = 0
    Width = 137
    Height = 82
    Align = alLeft
    Caption = 'TOA transfer direction'
    ItemIndex = 0
    Items.Strings = (
      'Evolution to AoD'
      'AoD to Evolution')
    TabOrder = 0
  end
  object cbTransferTerminatedEEs: TCheckBox
    Left = 144
    Top = 3
    Width = 169
    Height = 17
    Caption = 'Export terminated employees'
    TabOrder = 1
  end
  object cbTransferInactiveTOA: TCheckBox
    Left = 144
    Top = 24
    Width = 153
    Height = 17
    Caption = 'Export inactive TOA'
    TabOrder = 2
  end
  object gbHyperquery: TGroupBox
    Left = 144
    Top = 42
    Width = 174
    Height = 40
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Hyperquery'
    TabOrder = 3
    DesignSize = (
      174
      40)
    object cmbHyperqueries: TComboBox
      Left = 8
      Top = 15
      Width = 160
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = '- not selected -'
      Items.Strings = (
        '- not selected -')
    end
  end
end
