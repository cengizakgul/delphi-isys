unit NotesFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OptionsBaseFrame, ExtCtrls, StdCtrls;

type
  TNotesFrm = class(TOptionsBaseFrm)
    mmNotes: TMemo;
    pnlNotes: TPanel;
  private
    function GetNotes: string;
    procedure SetNotes(const Value: string);
  public
    procedure Clear(enable: boolean);
    property Notes: string read GetNotes write SetNotes;
  end;


implementation

uses
  gdycommon;
{$R *.dfm}

{ TNotesFrame }

procedure TNotesFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    mmNotes.Lines.Text := '';
  finally
    FBlockOnChange := false;
  end;
end;

function TNotesFrm.GetNotes: string;
begin
  Result := mmNotes.Lines.Text;
end;

procedure TNotesFrm.SetNotes(const Value: string);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    mmNotes.Lines.Text := Value;
  finally
    FBlockOnChange := false;
  end;
end;

end.
