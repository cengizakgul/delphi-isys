inherited ExtAppConnectionParamFrm: TExtAppConnectionParamFrm
  Width = 223
  Height = 82
  object lblUserName: TLabel
    Left = 6
    Top = 29
    Width = 48
    Height = 13
    Caption = 'Username'
  end
  object lblPassword: TLabel
    Left = 6
    Top = 53
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object lblSite: TLabel
    Left = 6
    Top = 5
    Width = 40
    Height = 13
    Caption = 'Client ID'
  end
  object lblPasswordComment: TLabel
    Left = 216
    Top = 56
    Width = 3
    Height = 13
  end
  object PasswordEdit: TPasswordEdit
    Left = 87
    Top = 53
    Width = 124
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
  end
  object UserNameEdit: TEdit
    Left = 87
    Top = 28
    Width = 124
    Height = 21
    TabOrder = 1
  end
  object SiteEdit: TEdit
    Left = 87
    Top = 5
    Width = 124
    Height = 21
    TabOrder = 0
  end
end
