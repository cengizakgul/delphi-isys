unit DBDTMappingFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, DBCtrls, ExtCtrls, DBDTCustomMappingFrame;

type
  TDBDTMappingFrm = class(TFrame)
    dsWG: TDataSource;
    GroupBox1: TGroupBox;
    rbNone: TRadioButton;
    rbDirect: TRadioButton;
    rbExpression: TRadioButton;
    dblcDirect: TDBLookupComboBox;
    dsEvoTables: TDataSource;
    DBDTCustomMappingFrame: TDBDTCustomMappingFrm;
    dsExpression: TDataSource;
    rbAdditionalField: TRadioButton;
    dblcAdditionalField: TDBLookupComboBox;
    dsCO_ADDITIONAL_INFO_NAMES: TDataSource;
    pnlTop: TPanel;
    dblcWGIdentityKind: TDBLookupComboBox;
    dsWGIdentityKind: TDataSource;
    Label1: TLabel;
    procedure dsWGDataChange(Sender: TObject; Field: TField);
    procedure rbClick(Sender: TObject);
    procedure WGAutoPost(Sender: TObject);
  private
    FSettingRB: boolean;
  public
    { Public declarations }
    procedure SetMappingDS(wg, expr, evoTables, CO_ADDITIONAL_INFO_NAMES, wgIdentityKind: TDataSet);
  end;

implementation

{$R *.dfm}
uses
  DBDTMapper, gdycommon;

{ TDBDTMappingFrm }

procedure TDBDTMappingFrm.SetMappingDS(wg, expr, evoTables, CO_ADDITIONAL_INFO_NAMES, wgIdentityKind: TDataSet);
begin
  dsWG.DataSet := wg;
  dsEvoTables.DataSet := evoTables;
  dsExpression.DataSet := expr;
  dsCO_ADDITIONAL_INFO_NAMES.DataSet := CO_ADDITIONAL_INFO_NAMES;
  dsWGIdentityKind.DataSet := wgIdentityKind;
  dsWGDataChange(nil, nil);
end;

procedure TDBDTMappingFrm.dsWGDataChange(Sender: TObject; Field: TField);
var
  mk: TMappingKind;
begin
  FSettingRB := true;
  try
    mk := TMappingKind(dsWG.DataSet.FieldByName('TYPE').AsInteger);

    rbNone.Checked := mk = mapNone;

    rbDirect.Checked := mk = mapDirect;
    EnableControl(dblcDirect, rbDirect.Checked);
    if rbDirect.Checked then
      dblcDirect.DataSource := dsWG
    else
      dblcDirect.DataSource := nil;

    rbExpression.Checked := mk = mapExpression;
    EnableControl(DBDTCustomMappingFrame, rbExpression.Checked);
    if rbExpression.Checked then
      DBDTCustomMappingFrame.SetMappingDS(dsExpression.DataSet, dsEvoTables.DataSet)
    else
      DBDTCustomMappingFrame.SetMappingDS(nil, nil);

    rbAdditionalField.Checked := mk = mapAdditionalField;
    EnableControl(dblcAdditionalField, rbAdditionalField.Checked);
    if rbAdditionalField.Checked then
      dblcAdditionalField.DataSource := dsWG
    else
      dblcAdditionalField.DataSource := nil;
  finally
    FSettingRB := false;
  end;
end;

procedure TDBDTMappingFrm.rbClick(Sender: TObject);
begin
  if not FSettingRB then
  begin
    FSettingRB := true;
    try
      dsWG.DataSet.Edit;
      try
        dsWG.DataSet.FieldByName('TYPE').AsInteger := (Sender as TRadioButton).Tag;
        FSettingRB := false;
        dsWG.DataSet.Post;
      except
        dsWG.DataSet.Cancel;
        raise;
      end;
    finally
      FSettingRB := false;
    end;
  end;
end;

procedure TDBDTMappingFrm.WGAutoPost(Sender: TObject);
begin
  if dsWG.DataSet.State = dsEdit then
    dsWG.DataSet.Post;
end;

end.
