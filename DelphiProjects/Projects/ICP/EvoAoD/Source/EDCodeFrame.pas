unit EDCodeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame, ExtAppdecl;

type
  TEDCodeFrm = class(TCustomBinderBaseFrm)
    cdPayDesignations: TClientDataSet;
    cdPayDesignationsCODE: TIntegerField;
    cdPayDesignationsDESCRIPTION: TStringField;
  private
    procedure InitExtAppPayCodes(extConn: IExtAppConnection);
  public
    procedure Init(matchTableContent: string; CL_E_DS: TDataSet; extConn: IExtAppConnection);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, IAeXMLBridge1;

{ TEDCodeFrm }

procedure TEDCodeFrm.Init(matchTableContent: string; CL_E_DS: TDataSet; extConn: IExtAppConnection);
begin
  FBindingKeeper := CreateBindingKeeper( EDCodeBinding, TClientDataSet );
  CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').DisplayLabel := 'E/D Code';
  CL_E_DS.FieldByName('DESCRIPTION').DisplayLabel := 'Description';

  InitExtAppPayCodes(extConn);

  FBindingKeeper.SetTables(CL_E_DS, cdPayDesignations);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TEDCodeFrm.InitExtAppPayCodes(extConn: IExtAppConnection);
var
  i: integer;
  items: TAeBasicDataItemsAry;
begin
  CreateOrEmptyDataSet(cdPayDesignations);

  items := extConn.getPayDesignationsSimple;
  for i := 0 to high(items) do
    if 'Pay Des '+inttostr(items[i].Num) <> items[i].NameLabel then
      Append(cdPayDesignations, 'CODE;DESCRIPTION', [items[i].Num, items[i].NameLabel]);
end;

end.



