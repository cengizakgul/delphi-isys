{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
Unit EvoAoDWebService;

interface

uses SysUtils, Classes, InvokeRegistry, Midas, SOAPMidas, SOAPDm,
		 Types, XSBuiltIns;

type
  TOperation = (opEmployeeExport, opTOAExport, opTOAImport);

  IEvolutionAoDImport = interface(IInvokable)
  ['{7A8D4997-7415-4571-AA6F-D3C9B1CD6C9C}']
    procedure InitiateOperation(const Operation: TOperation; CompanyCode: string); stdcall;

  end;

  IEvolutionAoDImportSDM = interface(IAppServerSOAP)
    ['{647A7671-3D6C-41F0-AA99-EAB4FC7A5F53}']
  end;

  TEvolutionAoDImportSDM = class(TSoapDataModule, IEvolutionAoDImportSDM, IAppServerSOAP, IAppServer, IEvolutionAoDImport)
  private
    procedure InitiateOperation(const Operation: TOperation; CompanyCode: string); stdcall;

  public

  end;

implementation

uses
  gdyclasses, scheduledTask, gdyCommonLogger, gdyLoggerImpl, gdyLogger, common,
  windows, gdyRedir;
{$R *.DFM}


procedure TEvolutionAoDImportSDMCreateInstance(out obj: TObject);
begin
  obj := TEvolutionAoDImportSDM.Create(nil);
end;

{ TEvolutionAoDImportSDM }

function OperationToTaskName(Operation: TOperation): string;
begin
  case Operation of
    opEmployeeExport: Result := 'EEExport';
    opTOAExport: Result := 'TOAExport';
    opTOAImport: Result := 'TOAImport';
  else
    Assert(false);
  end;
end;

procedure LaunchyTaskRunner(Logger: ICOmmonLogger; guid: string);
var
  SI: STARTUPINFO;
  PI: PROCESS_INFORMATION;
  arg: string;
  exitcode: Cardinal;
begin
  logger.LogEntry('Starting task');
  try
    try
      ZeroMemory(@SI, SizeOf(SI));
      ZeroMemory(@PI, SizeOf(PI));
      arg := Format('"%s" %s', [Redirection.GetFilename(sTaskRunnerFileAlias), guid]);
      Win32Check( CreateProcess(nil, PChar(arg), nil, nil, False, 0, nil, nil, SI, PI) );
      CloseHandle(PI.hThread);
      try
        //if there is an error then most likely the process will end immediately
        if WaitForSingleObject(PI.hProcess, 1000) = WAIT_OBJECT_0 then
        begin
          Win32Check( GetExitCodeProcess(PI.hProcess, exitcode) );
          if TScheduledTaskResultStatus(exitcode) = statusSchedulerError then
            raise Exception.Create('Failed to execute task (corrupted task definition?)');
        end;
      finally
        CloseHandle(PI.hProcess);
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

procedure TEvolutionAoDImportSDM.InitiateOperation(const Operation: TOperation; CompanyCode: string);
var
  tasks: IStr;
  logger: ICommonLogger;
begin
  logger := CreateCommonLogger(CreateExceptionLogger(CreateStatefulLogger as ILogger)); //null logger

  tasks := GetTaskGUIDsByNameAndCompany(logger, OperationToTaskName(Operation), CompanyCode );
  if tasks.Count = 0 then
    raise Exception.CreateFmt('<%s> task is not defined for the <%s> company', [OperationToTaskName(Operation), CompanyCode] );
  if tasks.Count > 1 then
    raise Exception.CreateFmt('More than one <%s> task is defined for the <%s> company', [OperationToTaskName(Operation), CompanyCode] );

  LaunchyTaskRunner(logger, tasks.Str[0]);
end;

initialization
   InvRegistry.RegisterInterface(TypeInfo(IEvolutionAoDImport));

   InvRegistry.RegisterInvokableClass(TEvolutionAoDImportSDM, TEvolutionAoDImportSDMCreateInstance);
   InvRegistry.RegisterInterface(TypeInfo(IEvolutionAoDImportSDM));
end.
