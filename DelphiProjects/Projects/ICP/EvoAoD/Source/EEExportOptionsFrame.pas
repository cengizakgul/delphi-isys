unit EEExportOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, extappdecl, OptionsBaseFrame, BadgeExportOptionFrame;

type
  TEEExportOptionsFrm = class(TOptionsBaseFrm)
    cbExportSSN: TCheckBox;
    cbExportRates: TCheckBox;
    cbExportStandardHours: TCheckBox;
    BadgeExportOptionFrame: TBadgeExportOptionFrm;
    rbSalaryAsRate: TRadioButton;
    rbSalaryAsSalary: TRadioButton;
    procedure cbExportRatesClick(Sender: TObject);
  private
    function GetOptions: TEEExportOptions;
    procedure SetOptions(const Value: TEEExportOptions);
    procedure ExportSalarySetEnabled(const Value: Boolean);
  public
    property Options: TEEExportOptions read GetOptions write SetOptions;
    procedure Clear(enable: boolean);
  end;

implementation

{$R *.dfm}

uses
  gdycommon;

{ TEEExportOptionsFrm }

procedure TEEExportOptionsFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    cbExportSSN.Checked := false;
    cbExportRates.Checked := false;
    rbSalaryAsRate.Checked := True;
    rbSalaryAsSalary.Checked := False;
    ExportSalarySetEnabled(false);
    cbExportStandardHours.Checked := false;
    BadgeExportOptionFrame.Clear;
  finally
    FBlockOnChange := false;
  end;
end;

procedure TEEExportOptionsFrm.ExportSalarySetEnabled(const Value: Boolean);
begin
  rbSalaryAsRate.Enabled := Value;
  rbSalaryAsSalary.Enabled := Value;
end;

function TEEExportOptionsFrm.GetOptions: TEEExportOptions;
begin
  Result.ExportSSN := cbExportSSN.Checked;
  Result.ExportRates := cbExportRates.Checked;
  if rbSalaryAsRate.Checked then
    Result.ExportSalary := esSalaryAsRate
  else
    Result.ExportSalary := esSalaryAsSalary;
  Result.ExportStandardHours := cbExportStandardHours.Checked;
  Result.Badge := BadgeExportOptionFrame.Value;
end;

procedure TEEExportOptionsFrm.SetOptions(const Value: TEEExportOptions);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);

    cbExportSSN.Checked := Value.ExportSSN;
    cbExportRates.Checked := Value.ExportRates;
    ExportSalarySetEnabled(cbExportRates.Checked);
    rbSalaryAsRate.Checked := Value.ExportSalary = esSalaryAsRate;
    rbSalaryAsSalary.Checked := Value.ExportSalary = esSalaryAsSalary;
    cbExportStandardHours.Checked := Value.ExportStandardHours;
    BadgeExportOptionFrame.Value := Value.Badge;
  finally
    FBlockOnChange := false;
  end;
end;

procedure TEEExportOptionsFrm.cbExportRatesClick(Sender: TObject);
begin
  ExportSalarySetEnabled(cbExportRates.Checked);
end;

end.
