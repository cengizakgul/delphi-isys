unit ExtAppOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, extappdecl, PasswordEdit, OptionsBaseFrame,
  ExtAppConnection, IAeXMLBridge1;

type
  TExtAppOptionsFrm = class(TOptionsBaseFrm)
    rgTOA: TRadioGroup;
    cbTransferTerminatedEEs: TCheckBox;
    cbTransferInactiveTOA: TCheckBox;
    gbHyperquery: TGroupBox;
    cmbHyperqueries: TComboBox;
  private
    FMultiCompanyMode: boolean;
    FOptions: TExtAppOptions;
    FOptionsSet: boolean;

    function GetOptions: TExtAppOptions;
    procedure SetOptions(const Value: TExtAppOptions);
    procedure DataChanged;
  public
    property Options: TExtAppOptions read GetOptions write SetOptions;
    procedure Clear(enable: boolean);
    procedure SetMultiCompanyMode(multiCompanyMode: boolean);
    procedure LoadHyperqueries(Conn: IExtAppConnection);
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TExtAppOptionsFrm }

procedure TExtAppOptionsFrm.Clear(enable: boolean);
begin
  FMultiCompanyMode := false;
  FOptionsSet := false;
  DataChanged;
end;

function TExtAppOptionsFrm.GetOptions: TExtAppOptions;
begin
  Result.TOAImport := rgTOA.ItemIndex = 1;
  Result.TransferTerminatedEEs := cbTransferTerminatedEEs.Checked;
  Result.TransferInactiveTOA := cbTransferInactiveTOA.Checked;
  Result.Hyperquery := cmbHyperqueries.Text;
end;

procedure TExtAppOptionsFrm.SetMultiCompanyMode(multiCompanyMode: boolean);
begin
  FMultiCompanyMode := multiCompanyMode;
  DataChanged;
end;

procedure TExtAppOptionsFrm.SetOptions(const Value: TExtAppOptions);
var
  i: integer;
begin
  FOptions := Value;
  FOptionsSet := true;
  DataChanged;
  i := cmbHyperqueries.Items.IndexOf( Value.Hyperquery );
  if i > -1 then
    cmbHyperqueries.ItemIndex := i
  else
    cmbHyperqueries.ItemIndex := 0;
end;

procedure TExtAppOptionsFrm.DataChanged;
begin
  FBlockOnChange := true;
  try
//    EnableControlRecursively(Self, FOptionsSet and not FMultiCompanyMode);

    rgTOA.Enabled := FOptionsSet and not FMultiCompanyMode;
    cbTransferTerminatedEEs.Enabled := rgTOA.Enabled;
    cbTransferInactiveTOA.Enabled := rgTOA.Enabled;

    if FMultiCompanyMode then
    begin
      rgTOA.ItemIndex := 1
    end
    else if FOptionsSet then
    begin
      rgTOA.ItemIndex := ord(FOptions.TOAImport);
      cbTransferTerminatedEEs.Checked := FOptions.TransferTerminatedEEs;
      cbTransferInactiveTOA.Checked := FOptions.TransferInactiveTOA;
    end
    else
    begin
      rgTOA.ItemIndex := 0;
      cbTransferTerminatedEEs.Checked := false;
      cbTransferInactiveTOA.Checked := false;
    end
  finally
    FBlockOnChange := false;
  end;
end;

procedure TExtAppOptionsFrm.LoadHyperqueries(Conn: IExtAppConnection);
var
  hq: IAeXMLBridge1.TStringArray;
  i: integer;
begin
  cmbHyperqueries.Clear;
  cmbHyperqueries.Items.Add( NotSelected );

  hq := Conn.getHyperQueriesSimple;
  for i := Low(hq) to High(hq) do
    cmbHyperqueries.Items.Add(hq[i]);
end;

end.
