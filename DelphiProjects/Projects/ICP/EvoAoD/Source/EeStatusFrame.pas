unit EeStatusFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient,
  CustomBinderBaseFrame, ExtAppdecl;

type
  TEeStatusFrm = class(TCustomBinderBaseFrm)
    cdEvoEEStatuses: TClientDataSet;
    cdEvoEEStatusesCODE: TStringField;
    cdEvoEEStatusesNAME: TStringField;
    cdExtAppEEStatuses: TClientDataSet;
    cdExtAppEEStatusesDESCRIPTION: TStringField;
    cdExtAppEEStatusesACTIVE_CODE: TIntegerField;
    cdExtAppEEStatusesCODE: TIntegerField;
    cdExtAppEEStatusesACTIVE: TStringField;
    cdEvoEEStatusesREHIRE_CODE: TStringField;
    cdEvoEEStatusesREHIRE_NAME: TStringField;
  private
    procedure InitEvoEEStatuses;
    procedure InitExtAppEEStatuses(aod: IExtAppConnection);
  public
    procedure Init(matchTableContent: string; aod: IExtAppConnection);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, evConsts, common, IAeXMLBridge1;

{ TEeStatusFrm }

procedure TEeStatusFrm.Init(matchTableContent: string; aod: IExtAppConnection);
var
  newTableContent: string;
begin
  try
    InitExtAppEEStatuses(aod);
    InitEvoEEStatuses;

    FBindingKeeper := CreateBindingKeeper( EEStatusBinding, TClientDataSet );
    FBindingKeeper.SetTables(cdEvoEEStatuses, cdExtAppEEStatuses);
    FBindingKeeper.SetVisualBinder(BinderFrm1);
    newTableContent := matchTableContent;

    if Pos('LEFT_REHIRE_NAME', newTableContent) = 0 then
    begin
      newTableContent := StringReplace(newTableContent,
        '<FIELD attrname="LEFT_NAME" fieldtype="string" WIDTH="40"/>',
        '<FIELD attrname="LEFT_NAME" fieldtype="string" WIDTH="40"/>' +
        '<FIELD attrname="LEFT_REHIRE_NAME" fieldtype="string" WIDTH="15"/>', []);
    end;

    if Pos('<FIELD attrname="LEFT_CODE" fieldtype="string" WIDTH="1"/>', newTableContent) > 0 then
    begin
      newTableContent := StringReplace(newTableContent,
        '<FIELD attrname="LEFT_CODE" fieldtype="string" WIDTH="1"/>',
        '<FIELD attrname="LEFT_CODE" fieldtype="string" WIDTH="2"/>', []);
    end;

    FBindingKeeper.SetMatchTableFromString(newTableContent);
    FBindingKeeper.SetConnected(true);
  except
    UnInit;
    raise;
  end;
end;

procedure TEeStatusFrm.InitExtAppEEStatuses(aod: IExtAppConnection);
var
  i: integer;
  items: TAeBasicDataItemsAry;
begin
  CreateOrEmptyDataSet(cdExtAppEEStatuses);

  items := aod.getActiveStatusConditions;
  for i := 0 to high(items) do
    Append(cdExtAppEEStatuses, 'ACTIVE_CODE;CONDITION_CODE;ACTIVE_DESCRIPTION;CONDITION_DESCRIPTION', [0, items[i].Num, 'Active', items[i].NameLabel]);

  items := aod.getInactiveStatusConditions;
  for i := 0 to high(items) do
    Append(cdExtAppEEStatuses, 'ACTIVE_CODE;CONDITION_CODE;ACTIVE_DESCRIPTION;CONDITION_DESCRIPTION', [1, items[i].Num, 'Terminated', items[i].NameLabel]);
end;

procedure TEeStatusFrm.InitEvoEEStatuses;
var
  lines: IStr;
  i: integer;
begin
  CreateOrEmptyDataSet( cdEvoEEStatuses );
  lines := SplitToLines(EE_TerminationCodeX_ComboChoices);
  for i := 0 to lines.Count-1 do
    with SplitByChar(lines.Str[i], #9) do
    begin
      Append(cdEvoEEStatuses, 'CODE;NAME;REHIRE_NAME', [Str[1] + GROUP_BOX_YES, Str[0], 'Yes']);
      Append(cdEvoEEStatuses, 'CODE;NAME;REHIRE_NAME', [Str[1] + GROUP_BOX_NO, Str[0], 'No']);
      Append(cdEvoEEStatuses, 'CODE;NAME;REHIRE_NAME', [Str[1] + GROUP_BOX_CONDITIONAL, Str[0], 'Conditional']);
    end;
    Append(cdEvoEEStatuses, 'CODE;NAME;REHIRE_NAME', [EE_TERM_X_MISSING, '(Employee is missing)', '']);
end;

end.



