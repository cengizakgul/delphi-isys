unit extappdecl;

interface

uses
  gdystrset, RateImportOptionFrame, isSettings, gdyCommonLogger, IAeXMLBridge1,
  common, gdyBinder;

const
	ExtAppName = 'AoD';
  NotSelected = '- not selected -';
	ExtAppLongName = 'Attendance on Demand';

  IntegrationServiceName = 'EvoAoDWebService';
  IntegrationServiceDisplayName = 'EvoAoD Web Service';

type
  TExtAppConnectionParam = record
    UserName: string;
    Password: string;
    Site: string;
  end;

  TExtAppOptions = record
    TOAImport: boolean;
    TransferTerminatedEEs: boolean; //export
    TransferInactiveTOA: boolean;
    Hyperquery: string;
  end;

  TBadgeExportOption = (badgeFromEmployeeCode, badgeFromBadgeID, badgeFromTimeclockNumber, badgeNone);

  TExportSalaryOption = (esSalaryAsRate, esSalaryAsSalary);

  TEEExportOptions = record
    ExportSSN: boolean;
    ExportRates: boolean;
    ExportSalary: TExportSalaryOption;
    ExportStandardHours: boolean;
    Badge: TBadgeExportOption;
  end;

  TTCImportOptions = record
    Summarize: boolean;
    RateImport: TRateImportOption;
    AllowImportToBatchesWithUserEarningLines: boolean;
  end;

  IExtAppConnection = interface
['{D7C79692-3820-4BD4-A15E-687EAB896897}']
    function  getWorkgroupLevelDetails(const WGLevel: Integer): TAeWorkgroupLevel;
    function  getActiveStatusConditions: TAeBasicDataItemsAry;
    function  getInactiveStatusConditions: TAeBasicDataItemsAry;
    function  getAllEmployeesList: TAeEmployeeBasicAry;
    function  getEmployeeDetail2ByFilekey(const Filekey: Integer): TAeEmployeeDetail2;
    function  getEmployeeDetailByFilekey(const Filekey: Integer): TAeEmployeeDetail;
    procedure AddEmployee(ee: TAeEmployeeDetail; autoBadge: boolean);
    procedure UpdateEmployee(ee: TAeEmployeeDetail; autoBadge: boolean);
    function  getPayClassesSimple: TAeBasicDataItemsAry;
    function  getBaseClientData: TAeClientData;
    function  getWorkgroups(const WGLevel: Integer; const WGSortingOption: TWGSortingOption): TAeWorkgroupAry;
    function  getEmployeeESSEMailByFilekey(const Filekey: Integer): WideString;
    procedure setEmployeeESSEMailByFilekey(const Filekey: Integer; const EMail: WideString);
    procedure setEmployeeESSEMailByIDNum(const IDNum: WideString; const EMail: WideString);
    function  extractEmployeeDailySummsByFilekey(const Filekey: Integer; const Date: TDateTime): TAePayLineAry;
    function  getPayDesignationsSimple: TAeBasicDataItemsAry;
    function  getBenefitsSimple: TAeBasicDataItemsAry;
    procedure adjustEmployeeBenefitBalanceByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: TDateTime; BenAdjType: TBenAdjTypeEnum; const Amount: Double);
    function  extractEmployeeBenefitActivityByFilekey(const Filekey: Integer; const BenefitID: Integer; const FromDate: TDateTime; const ToDate: TDateTime): TAeEmpBenefitActivityAry;
    function  employeeBenefitBalanceAsOfByIDNum(const IDNum: WideString; const BenefitID: Integer; const SelDate: TDateTime): Double;
    function  getHourlyStatusTypesSimple: TAeBasicDataItemsAry;
    function  getClockGroupsSimple: TAeBasicDataItemsAry;
    function  extractEmployeePeriodFrameByIDNum(const IDNum: WideString): TAePayPeriodFrame;
    function  getSchedulePatternsSimple: TAeBasicDataItemsAry;
    function  getHyperQueriesSimple: TStringArray;
    function  getEmployeesListDetail2FromHyperQuery(const HyperQueryName: WideString): TAeEmployeeDetail2Ary;
    function  getEmployeesListBasicFromHyperQuery(const HyperQueryName: WideString): TAeEmployeeBasicAry;
  end;

function LoadEEExportOptions(conf: IisSettings; root: string): TEEExportOptions;
procedure SaveEEExportOptions( const options: TEEExportOptions; conf: IisSettings; root: string);
procedure LogEEExportOptions(Logger: ICommonLogger; options: TEEExportOptions);
function DescribeEEExportOptions(options: TEEExportOptions): string;

function LoadTCImportOptions(conf: IisSettings; root: string): TTCImportOptions;
procedure SaveTCImportOptions( const options: TTCImportOptions; conf: IisSettings; root: string);
procedure LogTCImportOptions(Logger: ICommonLogger; options: TTCImportOptions);
function DescribeTCImportOptions(options: TTCImportOptions): string;

function LoadExtAppConnectionParam(conf: IisSettings; root: string): TExtAppConnectionParam;
procedure SaveExtAppConnectionParam(const param: TExtAppConnectionParam; conf: IisSettings; root: string);
procedure LogExtAppConnectionParam(Logger: ICommonLogger; const param: TExtAppConnectionParam);
function AreExtAppConnectionParamTheSame(one, another: TExtAppConnectionParam): boolean; //ignores SavePassword;

function LoadExtAppOptions(conf: IisSettings; root: string): TExtAppOptions;
procedure SaveExtAppOptions(const options: TExtAppOptions; conf: IisSettings; root: string);
procedure LogExtAppOptions(Logger: ICommonLogger; const options: TExtAppOptions);

const
  EE_TERM_X_REHIRE_CONDITIONAL = '?';
  EE_TERM_X_REHIRE_NO = '!';
  EE_TERM_X_MISSING = '.';

  function EE_TerminationCodeX_ComboChoices: string;

const
  EEStatusBinding: TBindingDesc =
    ( Name: 'EEStatusesBinding';
      Caption: 'Mapped employee statuses';
      LeftDesc: ( Caption: 'Evolution employee statuses'; Unique: true; KeyFields: 'CODE'; ListFields: 'NAME;REHIRE_NAME');
      RightDesc: ( Caption: 'AoD employee statuses'; Unique: false; KeyFields: 'ACTIVE_CODE;CONDITION_CODE'; ListFields: 'ACTIVE_DESCRIPTION;CONDITION_DESCRIPTION');
      HideKeyFields: true;
    );

  EDCodeBinding: TBindingDesc =
    ( Name: 'EDCodeBinding';
      Caption: 'Evolution client E/D codes mapped to AoD pay designations';
      LeftDesc: ( Caption: 'Evolution client E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION'); //CL_E_DS
      RightDesc: ( Caption: 'AoD pay designations'; Unique: true; KeyFields: 'CODE'; ListFields: 'DESCRIPTION');
      HideKeyFields: true;
    );

  TOABinding: TBindingDesc =
    ( Name: 'TOABinding';
      Caption: 'Evolution TOA types mapped to AoD benefits';
      LeftDesc: ( Caption: 'Evolution TOA types'; Unique: true; KeyFields: 'DESCRIPTION'; ListFields: 'DESCRIPTION'); 
      RightDesc: ( Caption: 'AoD benefits'; Unique: false; KeyFields: 'CODE'; ListFields: 'DESCRIPTION');
      HideKeyFields: true;
    );
   
  CustomFieldsBinding: TBindingDesc =
    ( Name: 'CustomFieldsBinding';
      Caption: 'Mapped custom fields';
      LeftDesc: ( Caption: 'Evolution additional fields'; Unique: true; KeyFields: 'NAME'; ListFields: 'NAME');
      RightDesc: ( Caption: 'AoD fields'; Unique: true; KeyFields: 'CODE'; ListFields: 'NAME');
      HideKeyFields: true;
    );

  sExtAppStaticCustomField1Code = 'FN_StaticCustomField1';
  sExtAppStaticCustomField2Code = 'FN_StaticCustomField2';
  sExtAppStaticCustomField3Code = 'FN_StaticCustomField3';
  sExtAppStaticCustomField4Code = 'FN_StaticCustomField4';
  sExtAppStaticCustomField5Code = 'FN_StaticCustomField5';
  sExtAppStaticCustomField6Code = 'FN_StaticCustomField6';
  sExtAppPayClassCode = 'FN_PayClass';
  sExtAppHourlyStatusCode = 'FN_HourlyStatus';
  sExtAppClockGroupCode = 'FN_ClockGroup';
  sExtAppSchedulePatternCode = 'FN_SchedulePattern';

const
  sCurrenPayRate = 'Current pay rate';
  sActiveStatusConditionID = 'Active status condition ID';
  sActiveStatus = 'Active status';
  sInactiveStatusConditionID = 'Inactive status condition ID';
  sHourlyStatus = 'Hourly status';
  sPayClass = 'Pay class';
  sPayType = 'Pay type';

implementation

uses
  sysutils, gdycommon, gdyClasses, gdyCrypt, comboChoices;

const
  cKey='Form.Button';

procedure LogBadgeExportOption(logger: ICommonLogger; option: TBadgeExportOption);
begin
  case option of
    badgeFromEmployeeCode: logger.LogContextItem('Badge export', 'Employee code' );
    badgeFromBadgeID: logger.LogContextItem('Badge export', 'Badge ID');
    badgeFromTimeclockNumber: logger.LogContextItem('Badge export', 'Timeclock number');
    badgeNone: logger.LogContextItem('Badge export', 'None');
  else
    Assert(false);
  end;
end;

function DescribeBadgeExportOption(option: TBadgeExportOption): string;
begin
  Result := 'Badge export: ';
  case option of
    badgeFromEmployeeCode: Result := Result + 'Employee code';
    badgeFromBadgeID: Result := Result + 'Badge ID';
    badgeFromTimeclockNumber: Result := Result + 'Timeclock number';
    badgeNone: Result := Result + 'None';
  else
    Assert(false);
  end;
end;
  
function LoadEEExportOptions(conf: IisSettings; root: string): TEEExportOptions;
begin
  root := root + IIF(root='','','\') + 'EEExportOptions\';
  Result.ExportSSN := conf.AsBoolean[root+'ExportSSN'];
  Result.ExportRates := conf.AsBoolean[root+'ExportRates'];
  Result.ExportSalary := TExportSalaryOption(conf.AsInteger[root+'ExportSalary']);
  Result.ExportStandardHours := conf.AsBoolean[root+'ExportStandardHours'];
  Result.Badge := TBadgeExportOption(conf.AsInteger[root+'BadgeExport']);
  if (Result.Badge < low(TBadgeExportOption)) or (Result.Badge > high(TBadgeExportOption)) then
    Result.Badge := badgeFromEmployeeCode;
end;

procedure LogEEExportOptions(Logger: ICommonLogger; options: TEEExportOptions);
begin
  logger.LogContextItem( 'Export SSN', BoolToStr(options.ExportSSN, true) );
  logger.LogContextItem( 'Export rates', BoolToStr(options.ExportRates, true) );
  if options.ExportRates then
  begin
    if options.ExportSalary = esSalaryAsRate then
      logger.LogContextItem( 'Export salary as', 'Rate Amount')
    else
      logger.LogContextItem( 'Export salary as', 'Salary Amount');
  end;
  logger.LogContextItem( 'Export standard hours', BoolToStr(options.ExportStandardHours, true) );
  LogBadgeExportOption(logger, options.Badge);
end;

function DescribeEEExportOptions(options: TEEExportOptions): string;
begin
  Result := '';
  Result := Result + 'Export SSN: ' + BoolToStr(options.ExportSSN, true) + #13#10;
  Result := Result + 'Export rates: ' + BoolToStr(options.ExportRates, true) + #13#10;
  if options.ExportRates then
  begin
    if options.ExportSalary = esSalaryAsRate then
      Result := Result + 'Export salary as: Rate Amount'#13#10
    else
      Result := Result + 'Export salary as: Salary Amount'#13#10;
  end;
  Result := Result + 'Export standard hours: ' + BoolToStr(options.ExportStandardHours, true) + #13#10;
  Result := Result + DescribeBadgeExportOption(options.Badge) + #13#10;
end;

procedure SaveEEExportOptions( const options: TEEExportOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EEExportOptions\';
  conf.AsBoolean[root+'ExportSSN'] := options.ExportSSN;
  conf.AsBoolean[root+'ExportRates'] := options.ExportRates;
  conf.AsInteger[root+'ExportSalary'] := Integer(options.ExportSalary);
  conf.AsBoolean[root+'ExportStandardHours'] := options.ExportStandardHours;
  conf.AsInteger[root+'BadgeExport'] := ord(options.Badge);
end;

function LoadTCImportOptions(conf: IisSettings; root: string): TTCImportOptions;
begin
  root := root + IIF(root='','','\') + 'TCImportOptions\';

  Result.Summarize := conf.AsBoolean[root+'Summarize'];

  Result.RateImport := TRateImportOption(conf.AsInteger[root+'RateImport']);
  if (Result.RateImport < low(TRateImportOption)) or (Result.RateImport > high(TRateImportOption)) then
    Result.RateImport := rateUseExternal;
  Result.AllowImportToBatchesWithUserEarningLines := conf.AsBoolean[root+'AllowImportToBatchesWithUserEarningLines'];
end;

procedure SaveTCImportOptions( const options: TTCImportOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'TCImportOptions\';
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsInteger[root+'RateImport'] := ord(options.RateImport);
  conf.AsBoolean[root+'AllowImportToBatchesWithUserEarningLines'] := options.AllowImportToBatchesWithUserEarningLines;
end;

function DescribeTCImportOptions(options: TTCImportOptions): string;
begin
  Result := '';
  Result := Result + 'Summarize: ' + BoolToStr(options.Summarize, true) + #13#10;
  Result := Result + DescribeRateImportOption(options.RateImport, ExtAppName) + #13#10;
  Result := Result + 'Allow import to batches that already have checks with earnings: ' + BoolToStr(options.AllowImportToBatchesWithUserEarningLines, true) + #13#10;
end;

procedure LogTCImportOptions(Logger: ICommonLogger; options: TTCImportOptions);
begin
  logger.LogContextItem( 'Summarize', BoolToStr(options.Summarize, true) );
  LogRateImportOption(logger, options.RateImport, ExtAppName);
  logger.LogContextItem( 'Allow import to batches that already have checks with earnings', BoolToStr(options.AllowImportToBatchesWithUserEarningLines, true) );
end;

function LoadExtAppConnectionParam(conf: IisSettings; root: string): TExtAppConnectionParam;
begin
  root := root + IIF(root='','','\') + ExtAppName + 'Connection\';
  Result.UserName := conf.AsString[root+'UserName'];
  Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  Result.Site := conf.AsString[root+'Site'];
end;

procedure SaveExtAppConnectionParam(const param: TExtAppConnectionParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + ExtAppName + 'Connection\';
  conf.AsString[root+'Username'] := param.Username;
  conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey));
  conf.AsString[root+'Site'] := param.Site;
end;

procedure LogExtAppConnectionParam(Logger: ICommonLogger; const param: TExtAppConnectionParam);
begin
  logger.LogContextItem( 'Client ID', param.Site );
  logger.LogContextItem( 'User name', param.UserName );
end;

function AreExtAppConnectionParamTheSame(one, another: TExtAppConnectionParam): boolean;
begin
  Result := (trim(one.UserName) = trim(another.UserName)) and (trim(one.Password) = trim(another.Password)) and (trim(one.Site) = trim(another.Site));
end;

function LoadExtAppOptions(conf: IisSettings; root: string): TExtAppOptions;
begin
  root := root + IIF(root='','','\') + ExtAppName + 'Options\';
  Result.TOAImport := conf.AsBoolean[root+'TOAImport'];
  Result.TransferTerminatedEEs := conf.AsBoolean[root+'TransferTerminatedEEs'];
  Result.TransferInactiveTOA := conf.AsBoolean[root+'TransferInactiveTOA'];
  Result.Hyperquery := conf.AsString[root+'HyperQuery'];
end;

procedure SaveExtAppOptions(const options: TExtAppOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + ExtAppName + 'Options\';
  conf.AsBoolean[root+'TOAImport'] := options.TOAImport;
  conf.AsBoolean[root+'TransferTerminatedEEs'] := options.TransferTerminatedEEs;
  conf.AsBoolean[root+'TransferInactiveTOA'] := options.TransferInactiveTOA;
  conf.AsString[root+'HyperQuery'] := options.Hyperquery;
end;

procedure LogExtAppOptions(Logger: ICommonLogger; const options: TExtAppOptions);
begin
  logger.LogContextItem( 'TOA transfer', IIF(options.TOAImport, 'AoD to Evolution', 'Evolution to AoD') );
  logger.LogContextItem( 'Export terminated employees', BoolToStr(options.TransferTerminatedEEs, true) );
  logger.LogContextItem( 'Transfer inactive TOA', BoolToStr(options.TransferInactiveTOA, true) );
  logger.LogContextItem( 'Hyperquery', options.Hyperquery );
end;

function EE_TerminationCodeX_ComboChoices: string;
begin
  Result := EE_TerminationCode_ComboChoices
   {+ #13 +
    '(Any status, rehire is conditional)'#9 + EE_TERM_X_REHIRE_CONDITIONAL + #13 +
    '(Any status, not eligible for rehire)'#9 + EE_TERM_X_REHIRE_NO + #13 +
    '(Employee is missing)'#9 + EE_TERM_X_MISSING;
    }
end;

end.
