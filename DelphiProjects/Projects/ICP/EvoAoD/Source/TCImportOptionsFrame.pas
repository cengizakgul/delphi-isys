unit TCImportOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, 
  RateImportOptionFrame, OptionsBaseFrame,
  extappdecl;

type
  TTCImportOptionsFrm = class(TOptionsBaseFrm)
    RateImportOptionFrame: TRateImportOptionFrm;
    cbBringInPunchDetails: TCheckBox;
    cbAllowImport: TCheckBox;
  private
    function GetOptions: TTCImportOptions;
    procedure SetOptions(const Value: TTCImportOptions);
  public
    constructor Create( Owner: TComponent ); override;
    property Options: TTCImportOptions read GetOptions write SetOptions;
    procedure Clear(enable: boolean);
  end;

implementation

{$R *.dfm}

uses
  gdycommon;

{ TTCImportOptionsFrm }

procedure TTCImportOptionsFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    cbBringInPunchDetails.Checked := false;
    RateImportOptionFrame.Clear;
    cbAllowImport.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

constructor TTCImportOptionsFrm.Create(Owner: TComponent);
begin
  inherited;
  Clear(false);
end;

function TTCImportOptionsFrm.GetOptions: TTCImportOptions;
begin
  Result.Summarize := not cbBringInPunchDetails.Checked;
  Result.RateImport := RateImportOptionFrame.Value;
  Result.AllowImportToBatchesWithUserEarningLines := cbAllowImport.Checked;
end;

procedure TTCImportOptionsFrm.SetOptions(const Value: TTCImportOptions);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    cbBringInPunchDetails.Checked := not Value.Summarize;
    RateImportOptionFrame.Value := Value.RateImport;
    cbAllowImport.Checked := Value.AllowImportToBatchesWithUserEarningLines;
  finally
    FBlockOnChange := false;
  end;
end;

end.
