unit WorkgroupFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, common, DB, DBClient, DBDTMapper,
  extappdecl, CustomBinderBaseFrame, BinderFrame, ExtCtrls, StdCtrls,
  Grids, Wwdbigrd, Wwdbgrid, dbcomp, DBDTMappingFrame;

type
  TWorkgroupFrm = class(TFrame)
    cdEvoTables: TClientDataSet;
    cdEvoTablesNAME: TStringField;
    cdEvoTablesLEVEL: TStringField;
    pnlWGList: TPanel;
    dgWGList: TReDBGrid;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Panel1: TPanel;
    Bevel3: TBevel;
    pnlRight: TPanel;
    DBDTMappingFrame: TDBDTMappingFrm;
  private
    FOnDBDTMappingChanged: TDBDTMappingChangedEvent;
    FDBDTMappingEditor: IDBDTMappingEditor;
    procedure SetOnDBDTMappingChanged(const Value: TDBDTMappingChangedEvent);
  public
    procedure Init(serialized: TDBDTMapperData; evo: TEvoDBDTInfo; CO_ADDITIONAL_INFO_NAMES: TDataSet; aod: IExtAppConnection);
    function CanGetDBDTMapper: boolean;
    function DBDTMapper: IDBDTMapper;
    function AsData: TDBDTMapperData;
    procedure Uninit;
    property OnDBDTMappingChanged: TDBDTMappingChangedEvent read FOnDBDTMappingChanged write SetOnDBDTMappingChanged;
  end;

implementation

{$R *.dfm}

uses
  evodata, IAeXMLBridge1, gdycommon;

{ TWorkgroupFrm }

function TWorkgroupFrm.CanGetDBDTMapper: boolean;
begin
  Result := FDBDTMappingEditor <> nil;
end;

procedure TWorkgroupFrm.Uninit;
begin
  EnableControlRecursively(Self, false);
  FDBDTMappingEditor := nil;
end;

procedure TWorkgroupFrm.Init(serialized: TDBDTMapperData; evo: TEvoDBDTInfo; CO_ADDITIONAL_INFO_NAMES: TDataSet; aod: IExtAppConnection);
begin
  try
    dgWGList.Selected.Clear;
    AddSelected(dgWGList.Selected, 'NAME', 40, 'Workgroup', true);
    AddSelected(dgWGList.Selected, 'SUMMARY', 10, 'Mapping', true);

    EnableControlRecursively(Self, true);

    FDBDTMappingEditor := CreateDBDTMappingEditor(serialized, evo, aod);
    dgWGList.DataSource := FDBDTMappingEditor.WG;
    DBDTMappingFrame.SetMappingDS(FDBDTMappingEditor.WG.DataSet, FDBDTMappingEditor.Expression.DataSet, FDBDTMappingEditor.EvoTables.DataSet, CO_ADDITIONAL_INFO_NAMES, FDBDTMappingEditor.WGIdentityKind.DataSet);
    FDBDTMappingEditor.SetOnDBDTMappingChanged(FOnDBDTMappingChanged);
  except
    Uninit;
    raise;
  end;
end;

function TWorkgroupFrm.DBDTMapper: IDBDTMapper;
begin
  Assert(CanGetDBDTMapper);
  Result := CreateDBDTMapper(FDBDTMappingEditor.AsData);
end;

function TWorkgroupFrm.AsData: TDBDTMapperData;
begin
  Assert(CanGetDBDTMapper);
  Result := FDBDTMappingEditor.AsData;
end;

procedure TWorkgroupFrm.SetOnDBDTMappingChanged(const Value: TDBDTMappingChangedEvent);
begin
  FOnDBDTMappingChanged := Value;
  if FDBDTMappingEditor <> nil then
    FDBDTMappingEditor.SetOnDBDTMappingChanged(Value);
end;

end.
