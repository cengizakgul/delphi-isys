object DBDTMappingFrm: TDBDTMappingFrm
  Left = 0
  Top = 0
  Width = 646
  Height = 449
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 41
    Width = 646
    Height = 408
    Align = alClient
    Caption = 'Mapping'
    TabOrder = 1
    object rbNone: TRadioButton
      Left = 8
      Top = 16
      Width = 73
      Height = 17
      Caption = 'None'
      TabOrder = 0
      OnClick = rbClick
    end
    object rbDirect: TRadioButton
      Tag = 1
      Left = 8
      Top = 40
      Width = 73
      Height = 17
      Caption = 'Direct'
      TabOrder = 1
      OnClick = rbClick
    end
    object rbExpression: TRadioButton
      Tag = 2
      Left = 8
      Top = 97
      Width = 73
      Height = 17
      Caption = 'Custom'
      TabOrder = 3
      OnClick = rbClick
    end
    object dblcDirect: TDBLookupComboBox
      Left = 27
      Top = 64
      Width = 145
      Height = 21
      DataField = 'DIRECT'
      DataSource = dsWG
      KeyField = 'TABLE'
      ListField = 'NAME'
      ListSource = dsEvoTables
      TabOrder = 2
      OnCloseUp = WGAutoPost
      OnExit = WGAutoPost
    end
    inline DBDTCustomMappingFrame: TDBDTCustomMappingFrm
      Left = 27
      Top = 121
      Width = 360
      Height = 181
      TabOrder = 4
    end
    object rbAdditionalField: TRadioButton
      Tag = 3
      Left = 8
      Top = 304
      Width = 129
      Height = 17
      Caption = 'Additional Field'
      TabOrder = 5
      OnClick = rbClick
    end
    object dblcAdditionalField: TDBLookupComboBox
      Left = 27
      Top = 328
      Width = 360
      Height = 21
      DataField = 'CO_ADD_INFO_NAMES_NAME'
      DataSource = dsWG
      KeyField = 'NAME'
      ListField = 'NAME'
      ListSource = dsCO_ADDITIONAL_INFO_NAMES
      TabOrder = 6
      OnCloseUp = WGAutoPost
      OnExit = WGAutoPost
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 114
      Height = 13
      Caption = 'Identify workgroup by its'
    end
    object dblcWGIdentityKind: TDBLookupComboBox
      Left = 131
      Top = 12
      Width = 145
      Height = 21
      DataField = 'ID_KIND'
      DataSource = dsWG
      KeyField = 'ID_KIND'
      ListField = 'NAME'
      ListSource = dsWGIdentityKind
      TabOrder = 0
    end
  end
  object dsWG: TDataSource
    OnDataChange = dsWGDataChange
    Left = 240
    Top = 24
  end
  object dsEvoTables: TDataSource
    AutoEdit = False
    Left = 200
    Top = 24
  end
  object dsExpression: TDataSource
    Left = 272
    Top = 24
  end
  object dsCO_ADDITIONAL_INFO_NAMES: TDataSource
    Left = 288
    Top = 320
  end
  object dsWGIdentityKind: TDataSource
    AutoEdit = False
    Left = 336
    Top = 16
  end
end
