unit DBDTMappingTextDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, DB, DBCtrls, StdCtrls, Mask;

type
  TDBDTMappingTextDlg = class(TDialogBase)
    dsExpression: TDataSource;
    dbedStart: TDBEdit;
    Label2: TLabel;
  private
    { Private declarations }
  public
    procedure SetMappingDS(expr: TDataSet);
  end;

implementation

{$R *.dfm}

{ TDBDTMappingTextDlg }

procedure TDBDTMappingTextDlg.SetMappingDS(expr: TDataSet);
begin
  dsExpression.DataSet := expr;
end;

end.
