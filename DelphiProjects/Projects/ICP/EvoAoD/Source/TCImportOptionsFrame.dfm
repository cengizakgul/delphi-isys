inherited TCImportOptionsFrm: TTCImportOptionsFrm
  Width = 628
  Height = 104
  inline RateImportOptionFrame: TRateImportOptionFrm
    Left = 8
    Top = 3
    Width = 255
    Height = 79
    TabOrder = 0
    inherited RadioGroup1: TRadioGroup
      Left = -1
      Width = 247
      Items.Strings = (
        'Use AoD rates'
        'Use Evolution D/B/D/T rate override'
        'Use Evolution employee pay rates')
    end
  end
  object cbBringInPunchDetails: TCheckBox
    Left = 274
    Top = 11
    Width = 151
    Height = 17
    Caption = 'Bring in punch details'
    TabOrder = 1
  end
  object cbAllowImport: TCheckBox
    Left = 274
    Top = 27
    Width = 351
    Height = 39
    Caption = 'Allow import to batches that already have checks with earnings'
    TabOrder = 2
    WordWrap = True
  end
end
