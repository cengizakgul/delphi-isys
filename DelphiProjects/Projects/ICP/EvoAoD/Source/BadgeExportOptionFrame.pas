unit BadgeExportOptionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, gdyCommonLogger, extappdecl;

type
  TBadgeExportOptionFrm = class(TFrame)
    RadioGroup1: TRadioGroup;
  private
    procedure SetValue(const Value: TBadgeExportOption);
    function GetValue: TBadgeExportOption;
  public
    property Value: TBadgeExportOption read GetValue write SetValue;
    procedure Clear;
  end;

implementation

{$R *.dfm}

{ TBadgeExportOptionFrm }

procedure TBadgeExportOptionFrm.Clear;
begin
  RadioGroup1.ItemIndex := -1;
end;

function TBadgeExportOptionFrm.GetValue: TBadgeExportOption;
begin
  Assert(RadioGroup1.ItemIndex >= integer(low(TBadgeExportOption)));
  Assert(RadioGroup1.ItemIndex <= integer(high(TBadgeExportOption)));
  Result := TBadgeExportOption(RadioGroup1.ItemIndex);
end;

procedure TBadgeExportOptionFrm.SetValue(const Value: TBadgeExportOption);
begin
  RadioGroup1.ItemIndex := ord(Value);
end;

end.
