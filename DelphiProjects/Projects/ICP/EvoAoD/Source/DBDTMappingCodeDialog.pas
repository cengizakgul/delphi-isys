unit DBDTMappingCodeDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, DB, DBCtrls, StdCtrls, Mask;

type
  TDBDTMappingCodeDlg = class(TDialogBase)
    dsExpression: TDataSource;
    dsEvoTables: TDataSource;
    dblcCode: TDBLookupComboBox;
    Label1: TLabel;
    dbedStart: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    dbedEnd: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
  private
    { Private declarations }

  public
    procedure SetMappingDS(expr, evoTables: TDataSet);
    function CanClose: boolean; override;
  end;

implementation

{$R *.dfm}

{ TDBDTMappingCodeDlg }

function TDBDTMappingCodeDlg.CanClose: boolean;
var
  start, endpos: string;
  noStart, noEnd: boolean;
  startVal, endVal: integer;
begin
  start := trim(dbedStart.Text);
  endpos := trim(dbedEnd.Text);
  noStart := start = '';
  noEnd := endpos = '';
  startVal := StrToIntDef(start, 0);
  endVal := StrToIntDef(endpos, 0);

  Result := (noStart or (startVal>0)) and (noEnd or (endVal>0)) and (noStart or noEnd or (endVal>startVal));
end;

procedure TDBDTMappingCodeDlg.SetMappingDS(expr, evoTables: TDataSet);
begin
  dsExpression.DataSet := expr;
  dsEvoTables.DataSet := evoTables;
end;

end.
