object ServiceControlFrm: TServiceControlFrm
  Left = 0
  Top = 0
  Width = 733
  Height = 253
  TabOrder = 0
  object lblStatus: TLabel
    Left = 136
    Top = 24
    Width = 41
    Height = 13
    Caption = '          '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 16
    Top = 184
    Width = 61
    Height = 13
    Caption = 'Service URL'
  end
  object Label2: TLabel
    Left = 16
    Top = 216
    Width = 57
    Height = 13
    Caption = 'WSDL URL'
  end
  object btnActivate: TButton
    Left = 16
    Top = 56
    Width = 105
    Height = 25
    Action = actActivateService
    TabOrder = 1
  end
  object btnDeactivate: TButton
    Left = 16
    Top = 96
    Width = 105
    Height = 25
    Action = actDeactivateService
    TabOrder = 2
  end
  object btnUpdateStatus: TButton
    Left = 16
    Top = 16
    Width = 105
    Height = 25
    Action = actUpdateStatus
    TabOrder = 0
  end
  object Button1: TButton
    Left = 16
    Top = 136
    Width = 105
    Height = 25
    Action = actConfiguration
    TabOrder = 3
  end
  object edServiceURL: TEdit
    Left = 88
    Top = 181
    Width = 449
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 4
  end
  object edWSDLURL: TEdit
    Left = 88
    Top = 213
    Width = 449
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 5
  end
  object ActionList1: TActionList
    Left = 208
    Top = 96
    object actActivateService: TAction
      Caption = 'Activate service'
      OnExecute = actActivateServiceExecute
    end
    object actDeactivateService: TAction
      Caption = 'Deactivate service'
      OnExecute = actDeactivateServiceExecute
    end
    object actUpdateStatus: TAction
      Caption = 'Update status'
      OnExecute = actUpdateStatusExecute
    end
    object actConfiguration: TAction
      Caption = 'Configure service'
      OnExecute = actConfigurationExecute
    end
  end
end
