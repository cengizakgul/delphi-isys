object WorkgroupFrm: TWorkgroupFrm
  Left = 0
  Top = 0
  Width = 856
  Height = 554
  TabOrder = 0
  object Bevel3: TBevel
    Left = 0
    Top = 0
    Width = 856
    Height = 4
    Align = alTop
    Shape = bsSpacer
  end
  object Panel1: TPanel
    Left = 0
    Top = 4
    Width = 856
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Bevel1: TBevel
      Left = 361
      Top = 0
      Width = 8
      Height = 550
      Align = alLeft
      Shape = bsSpacer
    end
    object pnlWGList: TPanel
      Left = 0
      Top = 0
      Width = 361
      Height = 550
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel2: TBevel
        Left = 0
        Top = 0
        Width = 361
        Height = 5
        Align = alTop
        Shape = bsSpacer
      end
      object dgWGList: TReDBGrid
        Left = 0
        Top = 5
        Width = 361
        Height = 545
        DisableThemesInTitle = False
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        ReadOnly = True
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        RecordCountHintEnabled = False
        FilterDlg = False
      end
    end
    object pnlRight: TPanel
      Left = 369
      Top = 0
      Width = 487
      Height = 550
      Align = alClient
      BevelOuter = bvNone
      Caption = 'pnlRight'
      TabOrder = 1
      inline DBDTMappingFrame: TDBDTMappingFrm
        Left = 0
        Top = 0
        Width = 487
        Height = 550
        Align = alClient
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 487
          Height = 509
        end
        inherited pnlTop: TPanel
          Width = 487
        end
      end
    end
  end
  object cdEvoTables: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 392
    Top = 264
    object cdEvoTablesLEVEL: TStringField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
      Size = 40
    end
    object cdEvoTablesNAME: TStringField
      DisplayLabel = 'Evolution table'
      FieldName = 'LEVEL'
      Size = 40
    end
  end
end
