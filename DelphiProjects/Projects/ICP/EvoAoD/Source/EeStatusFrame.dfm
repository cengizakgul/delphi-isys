inherited EeStatusFrm: TEeStatusFrm
  Width = 598
  Height = 513
  inherited BinderFrm1: TBinderFrm
    Width = 598
    Height = 513
    inherited evSplitter2: TSplitter
      Top = 249
      Width = 598
    end
    inherited pnltop: TPanel
      Width = 598
      Height = 249
      inherited evSplitter1: TSplitter
        Height = 249
      end
      inherited pnlTopLeft: TPanel
        Height = 249
        inherited dgLeft: TReDBGrid
          Height = 224
        end
      end
      inherited pnlTopRight: TPanel
        Width = 328
        Height = 249
        inherited evPanel4: TPanel
          Width = 328
        end
        inherited dgRight: TReDBGrid
          Width = 328
          Height = 224
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 254
      Width = 598
      inherited pnlbottom: TPanel
        Width = 598
        inherited evPanel5: TPanel
          Width = 598
        end
        inherited dgBottom: TReDBGrid
          Width = 598
        end
      end
      inherited pnlMiddle: TPanel
        Width = 598
      end
    end
  end
  object cdEvoEEStatuses: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 288
    Top = 240
    object cdEvoEEStatusesCODE: TStringField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
      Size = 2
    end
    object cdEvoEEStatusesNAME: TStringField
      DisplayLabel = 'Name'
      DisplayWidth = 50
      FieldName = 'NAME'
      Size = 40
    end
    object cdEvoEEStatusesREHIRE_CODE: TStringField
      FieldName = 'REHIRE_CODE'
      Size = 1
    end
    object cdEvoEEStatusesREHIRE_NAME: TStringField
      DisplayLabel = 'Rehire name'
      DisplayWidth = 11
      FieldName = 'REHIRE_NAME'
      Size = 15
    end
  end
  object cdExtAppEEStatuses: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdExtAppEEStatusesACTIVE_CODE: TIntegerField
      FieldName = 'ACTIVE_CODE'
    end
    object cdExtAppEEStatusesCODE: TIntegerField
      FieldName = 'CONDITION_CODE'
    end
    object cdExtAppEEStatusesACTIVE: TStringField
      DisplayLabel = 'Active Status'
      FieldName = 'ACTIVE_DESCRIPTION'
      Size = 10
    end
    object cdExtAppEEStatusesDESCRIPTION: TStringField
      DisplayLabel = 'Condition'
      DisplayWidth = 20
      FieldName = 'CONDITION_DESCRIPTION'
      Size = 40
    end
  end
end
