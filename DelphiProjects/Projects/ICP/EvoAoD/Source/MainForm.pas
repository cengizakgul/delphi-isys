unit MainForm;
                                                    
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoTCImportMainNewForm, ActnList, EvolutionCompanySelectorFrame,
  StdCtrls, Buttons, ComCtrls, EvolutionPrPrBatchFrame, ExtCtrls,
  EvoAPIConnectionParamFrame, common,
  timeclockimport, evoapiconnectionutils, evodata,
  EEExportOptionsFrame, OptionsBaseFrame, TCImportOptionsFrame,
  SchedulerFrame, scheduledTask, SmtpConfigFrame, PlannedActionConfirmationForm,
  ExtAppParamFrame, CustomBinderBaseFrame, WorkgroupFrame, extappdecl,
  EeStatusFrame, passwordCache, EDCodeFrame, TOAFrame, toaimport,
  ServiceControlFrame, ExtAppOptionsFrame, CustomFieldsFrame, 
  NotesFrame;

type
  TMainFm = class(TEvoTCImportMainNewFm)
    TCImportOptionsFrame: TTCImportOptionsFrm;
    BitBtn1: TBitBtn;
    tbshEEExport: TTabSheet;
    Panel2: TPanel;
    BitBtn5: TBitBtn;
    EEExportOptionsFrame: TEEExportOptionsFrm;
    tbshScheduler: TTabSheet;
    tbshSchedulerSettings: TTabSheet;
    SchedulerFrame: TSchedulerFrm;
    SmtpConfigFrame: TSmtpConfigFrm;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    tbshTOA: TTabSheet;
    pcCompany: TPageControl;
    tbshWorkgroup: TTabSheet;
    tbshEEStatus: TTabSheet;
    tbshTOAType: TTabSheet;
    WorkgroupFrame: TWorkgroupFrm;
    EeStatusFrame: TEeStatusFrm;
    tbshEDCode: TTabSheet;
    EDCodeFrame: TEDCodeFrm;
    TOAFrame: TTOAFrm;
    actTOAExport: TAction;
    actScheduleTOAExport: TAction;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    actTOAImport: TAction;
    actScheduleTOAImport: TAction;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    tbshWebService: TTabSheet;
    ServiceControlFrame: TServiceControlFrm;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    ExtAppFrame: TExtAppConnectionParamFrm;
    ExtAppOptionsFrame: TExtAppOptionsFrm;
    tbshCustomFields: TTabSheet;
    CustomFieldsFrame: TCustomFieldsFrm;
    WorkgroupNotesFrame: TNotesFrm;
    CustomFieldsNotesFrame: TNotesFrm;
    procedure RunActionExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunEmployeeExportUpdate(Sender: TObject);
    procedure RunEmployeeExportExecute(Sender: TObject);

    procedure actScheduleEEExportExecute(Sender: TObject);
    procedure actScheduleTCImportUpdate(Sender: TObject);
    procedure actScheduleTCImportExecute(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure actTOAExportUpdate(Sender: TObject);
    procedure actScheduleTOAExportExecute(Sender: TObject);
    procedure actTOAExportExecute(Sender: TObject);
    procedure actTOAImportExecute(Sender: TObject);
    procedure actScheduleTOAImportExecute(Sender: TObject);
    procedure tbshWebServiceShow(Sender: TObject);
    procedure actTOAImportUpdate(Sender: TObject);
  private
    FPasswordCache: TPasswordCache;

    function GetExtAppConnectionParam: TExtAppConnectionParam;

    procedure CheckThatCompanyNameMatches_EEExport(opname: string);
    procedure CheckThatCompanyNameMatches_TCImport(opname: string);

    function GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
  	function UpdateEmployees: TUpdateEmployeesStat;
    function DoExportTOA: TTOAStat;
    function DoGetTOAData: TTOAImportData;

    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);

    procedure HandleExtAppConnectionSettingsChangedByUser(Sender: TObject);
    procedure HandleExtOptionsChangedByUser(Sender: TObject);
    procedure HandleEEExportOptionsChangedByUser(Sender: TObject);
    procedure HandleTCImportOptionsChangedByUser(Sender: TObject);

    procedure UnInitPerCompanySettings;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);

    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleSmtpConfigChange(Sender: TObject);
    procedure HandleDBDTMappingChanged;
    procedure HandleEEStatusMappingChanged;
    procedure HandleEDCodeMappingChanged;
    procedure HandleTOAMappingChanged;
    procedure HandleCustomFieldsMappingChanged;

    procedure HandleWorkgroupNotesChangedByUser(Sender: TObject);
    procedure HandleCustomFieldsNotesChangedByUser(Sender: TObject);

    procedure ForceSavePasswords;
    procedure MultiCompanyModeMayBeChanged;
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  gdyGlobalWaitIndicator,
  userActionHelpers, gdyDialogEngine,  gdyMessageDialogFrame,
  EvoAPIClientMainForm, gdyclasses, extapptasks,
  ExtAppConnection, extapp, ExtAppParamDialog, gdycommon, dbdtmapper;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  FPasswordCache := TPasswordCache.Create;
  inherited;

  FEvoData.AddDS(CL_E_DSDesc);
  FEvoData.AddDS(CO_TIME_OFF_ACCRUALDesc);
  FEvoData.AddDS(CUSTOM_DBDTDesc);
  FEvoData.AddDS(CO_ADDITIONAL_INFO_NAMESDesc);

  UnInitPerCompanySettings;

  ExtAppFrame.OnChangeByUser := HandleExtAppConnectionSettingsChangedByUser;
  ExtAppOptionsFrame.OnChangeByUser := HandleExtOptionsChangedByUser;
  EEExportOptionsFrame.OnChangeByUser := HandleEEExportOptionsChangedByUser;
  TCImportOptionsFrame.OnChangeByUser := HandleTCImportOptionsChangedByUser;

  FEvoData.Advise(HandleCompanyChanged);

  try
    SchedulerFrame.Configure(Logger, TExtAppTaskAdapter.Create(False), HandleEditParameters);
    SchedulerFrame.actRunAs.Visible := True;
  except
    Logger.StopException;
  end;

  SmtpConfigFrame.Config := LoadSmtpConfig(FSettings, '');
  SmtpConfigFrame.OnChangeByUser := HandleSmtpConfigChange;

  if EvoFrame.IsValid then
    PageControl1.ActivePageIndex := 2
  else
    PageControl1.ActivePageIndex := 0;
  WorkgroupFrame.OnDBDTMappingChanged := HandleDBDTMappingChanged;
  EeStatusFrame.OnMatchTableChanged := HandleEEStatusMappingChanged;
  EDCodeFrame.OnMatchTableChanged := HandleEDCodeMappingChanged;
  TOAFrame.OnMatchTableChanged := HandleTOAMappingChanged;
  CustomFieldsFrame.OnMatchTableChanged := HandleCustomFieldsMappingChanged;
  pcCompany.ActivePageIndex := 0;

  WorkgroupNotesFrame.OnChangeByUser := HandleWorkgroupNotesChangedByUser;
  CustomFieldsNotesFrame.OnChangeByUser := HandleCustomFieldsNotesChangedByUser;

  ServiceControlFrame.Logger := Logger;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FPasswordCache);
  inherited;
end;

procedure TMainFm.HandleDBDTMappingChanged;
begin
  SaveDBDTMapperData(FSettings, CompanyUniquePath(FEvoData.GetClCo), WorkgroupFrame.AsData);
  MultiCompanyModeMayBeChanged;
end;

procedure TMainFm.HandleEEStatusMappingChanged;
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\EEStatusMapping\DataPacket'] := EeStatusFrame.SaveToString;
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Repaint;
  Result := extapp.GetTimeClockData( CreateExtAppConnection(GetExtAppConnectionParam, Logger), TCImportOptionsFrame.Options, EDCodeFrame.Matcher, WorkgroupFrame.DBDTMapper, FEvoData.GetClCo, Logger, period );
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      CheckThatCompanyNameMatches_TCImport('import');
      RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData, FEvoData.Connection, TCImportOptionsFrame.Options.AllowImportToBatchesWithUserEarningLines);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := ExtAppFrame.IsValid and FEvoData.CanGetPrBatch and EDCodeFrame.CanCreateMatcher and WorkgroupFrame.CanGetDBDTMapper and CustomFieldsFrame.CanCreateMatcher;
end;

function TMainFm.UpdateEmployees: TUpdateEmployeesStat;
var
  EEData: TEvoEEData;
  updater: TExtAppEmployeeUpdater;
  analysisResult: TAnalyzeResult;
begin
  EEData := FEvoData.CreateEEData;
  try
    updater := TExtAppEmployeeUpdater.Create(CreateExtAppConnection(GetExtAppConnectionParam, Logger), EEExportOptionsFrame.Options, ExtAppOptionsFrame.Options, EeStatusFrame.Matcher, WorkgroupFrame.DBDTMapper, CustomFieldsFrame.Matcher, Logger, EEData );
    try
      WaitIndicator.StartWait('Preparing');
      try
        analysisResult := updater.Analyze;
        if analysisResult.Actions.Count > 0 then
          Logger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        if not ConfirmActions(Self, analysisResult.Actions.ToText) then
          raise Exception.Create('User cancelled operation');
      finally
        WaitIndicator.EndWait;
      end;
      Result := updater.Apply(analysisResult.Actions, Self as IProgressIndicator);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      FreeAndNil(updater);
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.RunEmployeeExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
//      CheckThatCompanyNameMatches_EEExport('export');
      userActionHelpers.RunEmployeeExport(Logger, UpdateEmployees, FEvoData.GetClCo, ExtAppName);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunEmployeeExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := True; //ExtAppFrame.IsValid and FEvoData.CanGetClCo and WorkgroupFrame.CanGetDBDTMapper and EeStatusFrame.CanCreateMatcher;
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UnInitPerCompanySettings;
  Repaint;
  if new.HasValue then
  begin
    LoadPerCompanySettings(new.Value);
    Repaint;
  end
end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  try
    ExtAppOptionsFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    ExtAppFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    EEExportOptionsFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    TCImportOptionsFrame.Clear(false)
  except
    Logger.StopException;
  end;
  try
    WorkgroupFrame.Uninit;
  except
    Logger.StopException;
  end;
  try
    MultiCompanyModeMayBeChanged;
  except
    Logger.StopException;
  end;
  try
    EeStatusFrame.UnInit;
  except
    Logger.StopException;
  end;
  try
    EDCodeFrame.UnInit;
  except
    Logger.StopException;
  end;
  try
    TOAFrame.UnInit;
  except
    Logger.StopException;
  end;
  try
    CustomFieldsFrame.UnInit;
  except
    Logger.StopException;
  end;
  try
    CustomFieldsNotesFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    WorkgroupNotesFrame.Clear(false);
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
var
  ExtAppParam: TExtAppConnectionParam;
  conn: IExtAppConnection;
begin
  try
    ExtAppOptionsFrame.Clear(true);
  except
    Logger.StopException;
  end;
  try
    ExtAppFrame.Clear(true);
    ExtAppFrame.Param := LoadExtAppConnectionParam(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;
  try
    EEExportOptionsFrame.Clear(true);
    EEExportOptionsFrame.Options := LoadEEExportOptions(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;
  try
    TCImportOptionsFrame.Clear(true);
    TCImportOptionsFrame.Options := LoadTCImportOptions(FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
  Repaint;
  if ExtAppFrame.IsValid then
  begin
    try
      ExtAppParam := GetExtAppConnectionParam;
      WaitIndicator.StartWait('Getting AoD data');
      try
        conn := CreateExtAppConnection(ExtAppParam, Logger);
        try
          ExtAppOptionsFrame.LoadHyperqueries( conn );
          ExtAppOptionsFrame.Options := LoadExtAppOptions(FSettings, CompanyUniquePath(Value));
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          WorkgroupFrame.Init( LoadDBDTMapperData(FSettings, CompanyUniquePath(Value)), GetEvoDBDTInfo(Logger, FEvoData), FEvoData.DS['CO_ADDITIONAL_INFO_NAMES'], conn );
          FPasswordCache.Add(ExtAppName, ExtAppParam.Site, ExtAppParam.UserName, ExtAppParam.Password); //after first successfull call to ExtApp
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          MultiCompanyModeMayBeChanged;
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          EeStatusFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\EEStatusMapping\DataPacket'], conn );
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          if FSettings.GetChildNodes(ClientUniquePath(Value)).IndexOf('EDCodeMapping') <> -1 then
            EDCodeFrame.Init( FSettings.AsString[ClientUniquePath(Value)+'\EDCodeMapping\DataPacket'], FEvoData.DS[CL_E_DSDesc.Name], conn )
          else
            EDCodeFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\EDCodeMapping\DataPacket'], FEvoData.DS[CL_E_DSDesc.Name], conn );
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          TOAFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\TOAMapping\DataPacket'], FEvoData.DS[CO_TIME_OFF_ACCRUALDesc.Name], conn );
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          CustomFieldsFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\CustomFieldsMapping\DataPacket'], FEvoData.DS[CO_ADDITIONAL_INFO_NAMESDesc.Name] );
        except
          Application.HandleException(ExceptObject as Exception);
          Logger.StopException;
        end;
        try
          WorkgroupNotesFrame.Clear(true);
          WorkgroupNotesFrame.Notes := FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\Notes\Workgroups'];
        except
          Logger.StopException;
        end;
        try
          CustomFieldsNotesFrame.Clear(true);
          CustomFieldsNotesFrame.Notes := FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\Notes\CustomFields'];
        except
          Logger.StopException;
        end;
      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.StopException;
    end;
  end
  else
    PageControl1.ActivePage := tbshCompanySettings;
end;

procedure TMainFm.HandleExtAppConnectionSettingsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveExtAppConnectionParam( ExtAppFrame.Param, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleEEExportOptionsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveEEExportOptions( EEExportOptionsFrame.Options, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleTCImportOptionsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveTCImportOptions( TCImportOptionsFrame.Options, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  extappTasks.EditTask(task, Logger, Self);
end;

procedure TMainFm.HandleSmtpConfigChange(Sender: TObject);
begin
  SaveSmtpConfig(SmtpConfigFrame.Config, FSettings, '');
end;

procedure TMainFm.ForceSavePasswords;
var
  ExtAppParam: TExtAppConnectionParam;
begin
  EvoFrame.ForceSavePassword;

  ExtAppParam := GetExtAppConnectionParam;
  if trim(ExtAppFrame.Param.Password) = '' then
  begin
    ExtAppFrame.Param := ExtAppParam;
    HandleExtAppConnectionSettingsChangedByUser(nil);
  end;
end;

procedure TMainFm.actScheduleEEExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      CheckThatCompanyNameMatches_EEExport('schedule exporting of');
      ForceSavePasswords;

      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewEEExportTask(FEvoData.GetClCo, EEExportOptionsFrame.Options, EvoFrame.Param) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleTCImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo and ExtAppFrame.IsValid and EDCodeFrame.CanCreateMatcher and WorkgroupFrame.CanGetDBDTMapper; //don't need payrolls and batches
end;

procedure TMainFm.actScheduleTCImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      CheckThatCompanyNameMatches_TCImport('schedule importing of');
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTCImportTask(FEvoData.GetClCo, TCImportOptionsFrame.Options, EvoFrame.Param) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.CheckThatCompanyNameMatches_EEExport(opname: string);
var
  company: TEvoCompanyDef;
begin
  company := FEvoData.GetClCo;
{
  if not AnsiSameText( trim(FSiteName), trim(company.CUSTOM_COMPANY_NUMBER) ) and
     not AnsiSameText( trim(FSiteName), trim(company.Name) ) then
    if MessageDlg( Format('You are about to %s employee information from %s (%s) company into %s company. Do you want to proceed?',
      [opname, trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER), trim(FSiteName)]), mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Abort;
      }
end;

procedure TMainFm.CheckThatCompanyNameMatches_TCImport(opname: string);
var
  company: TEvoCompanyDef;
begin
  company := FEvoData.GetClCo;
  {
  if not AnsiSameText( trim(FSiteName), trim(company.CUSTOM_COMPANY_NUMBER) ) and
     not AnsiSameText( trim(FSiteName), trim(company.Name) ) then
    if MessageDlg( Format('You are about to %s timeclock information from "%s" company into "%s" (%s) company. Do you want to proceed?',
                      [opname, trim(FSiteName), trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER)]), mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Abort;
      }
end;

procedure TMainFm.PageControl1Change(Sender: TObject);
begin
{
  if (PageControl1.TabIndex = 3) and SwipeClockFrame.IsValid and not TCImportOptionsFrame.IsInitializedWithParams(SwipeClockFrame.Param) then
  begin
    UnInitPerCompanySettings;
    if FEvoData.CanGetClCo then
      LoadPerCompanySettings(FEvoData.GetClCo);
  end;
  }
end;

function TMainFm.GetExtAppConnectionParam: TExtAppConnectionParam;
var
  dlg: TExtAppConnectionParamDlg;
begin
  Result := ExtAppFrame.Param;

  if Result.Password = '' then
    Result.Password := FPasswordCache.Get(ExtAppName, Result.Site, Result.UserName);

  if Result.Password = '' then
  begin
    dlg := TExtAppConnectionParamDlg.Create(nil);
    try
      dlg.Caption := ExtAppLongName;
      dlg.Param := Result;
      with DialogEngine(dlg, Owner) do
        if ShowModal = mrOk then
          Result := dlg.Param;
      Repaint;
    finally
      FreeAndNil(dlg);
    end;
  end;

  if Result.Password = '' then
    raise Exception.Create(ExtAppLongName + ' password is not specified');
end;

procedure TMainFm.HandleEDCodeMappingChanged;
begin
  FSettings.AsString[ClientUniquePath(FEvoData.GetClCo) + '\EDCodeMapping\DataPacket'] := EDCodeFrame.SaveToString;
end;

procedure TMainFm.HandleTOAMappingChanged;
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\TOAMapping\DataPacket'] := TOAFrame.SaveToString;
end;

procedure TMainFm.actTOAExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := ExtAppFrame.IsValid and FEvoData.CanGetClCo and TOAFrame.CanCreateMatcher and not ExtAppOptionsFrame.Options.TOAImport;
end;

procedure TMainFm.actTOAImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := ExtAppFrame.IsValid and FEvoData.CanGetClCo and TOAFrame.CanCreateMatcher and WorkgroupFrame.CanGetDBDTMapper and ExtAppOptionsFrame.Options.TOAImport;
end;

procedure TMainFm.actScheduleTOAExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
//      CheckThatCompanyNameMatches_EEExport('schedule exporting of');
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTOAExportTask(FEvoData.GetClCo, EvoFrame.Param) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actTOAExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
//      CheckThatCompanyNameMatches_EEExport('export');
      userActionHelpers.RunTOAExport(Logger, DoExportTOA, FEvoData.GetClCo, ExtAppName);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TMainFm.DoExportTOA: TTOAStat;
begin
  Result := extapp.ExportTOA( CreateExtAppConnection(GetExtAppConnectionParam, Logger), FEvoData.Connection, TOAFrame.Matcher, ExtAppOptionsFrame.Options, FEvoData.GetClCo, Logger, Self as IProgressIndicator );
end;

procedure TMainFm.actTOAImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
//      CheckThatCompanyNameMatches_EEExport('export');
      userActionHelpers.RunTOAImport(Logger, DoGetTOAData, FEvoData.Connection, FEvoData.GetClCo, Self, ExtAppName);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TMainFm.DoGetTOAData: TTOAImportData;
begin
  Result := extapp.GetTOAData(CreateExtAppConnection(GetExtAppConnectionParam, Logger), FEvoData.Connection, TOAFrame.Matcher, WorkgroupFrame.DBDTMapper, FEvoData.GetClCo, Logger, Self as IProgressIndicator);
end;

procedure TMainFm.actScheduleTOAImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
//      CheckThatCompanyNameMatches_EEExport('schedule exporting of');
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTOAImportTask(FEvoData.GetClCo, EvoFrame.Param) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.tbshWebServiceShow(Sender: TObject);
begin
  Logger.LogEntry(Format('Tab "%s" is shown', [(Sender as TTabSheet).Caption]));
  try
    try
      ServiceControlFrame.UpdateStatus;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleExtOptionsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveExtAppOptions( ExtAppOptionsFrame.Options, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.MultiCompanyModeMayBeChanged;
begin
  ExtAppOptionsFrame.SetMultiCompanyMode( WorkgroupFrame.CanGetDBDTMapper and not VarIsNull(WorkgroupFrame.DBDTMapper.GetCompanyWorkgroupLevel) );
end;

procedure TMainFm.HandleCustomFieldsMappingChanged;
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\CustomFieldsMapping\DataPacket'] := CustomFieldsFrame.SaveToString;
end;

procedure TMainFm.HandleWorkgroupNotesChangedByUser(Sender: TObject);
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\Notes\Workgroups'] := WorkgroupNotesFrame.Notes;
end;

procedure TMainFm.HandleCustomFieldsNotesChangedByUser(Sender: TObject);
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\Notes\CustomFields'] := CustomFieldsNotesFrame.Notes;
end;

end.
