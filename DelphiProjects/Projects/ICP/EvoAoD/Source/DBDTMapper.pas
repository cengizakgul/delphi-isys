unit DBDTMapper;

interface

uses
  {kbmMemTable, }evconsts, variants, common, extappdecl, issettings, db;

type
  TCDBDTCodes = array [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] of Variant;
  TWGIdentities = array [1..7] of string;
  TWGIdentityKind = (wgCode, wgName, wgNumber);

  TEvoCodes = record
    CDBDT: TCDBDTCodes;
    Job: Variant;
    Position: Variant;
    Shift: Variant;
  end;

  TDBDTMapperData = record
    WGTable: string;
    ExpressionTable: string;
    LegacyTable: string;
  end;

  TWGValue = record
    IdentityKind: TWGIdentityKind;
    Value: variant;
    AdditionalFieldName: string; //Valid when Value is Unassigned
  end;

  IDBDTMapper = interface
['{9095B4B0-4237-4075-AD4E-4482AB3F807B}']
    function Dump: string; //for debug log
    function CalcWorkgroup(wglevel: integer; codes: TEvoCodes): TWGValue;
    function CalcTable(table: string; wgIdentities: TWGIdentities): Variant;
    function GetCompanyWorkgroupLevel: Variant;
    function GetWorkgroupIdentityKind(wglevel: integer): TWGIdentityKind;
  end;

  TDBDTMappingChangedEvent = procedure of object;

  IDBDTMappingEditor = interface
['{D3D4B3E8-5157-4CFB-B9B9-4C11CBC9749F}']
    function AsData: TDBDTMapperData;
    function WG: TDataSource;
    function Expression: TDataSource;
    function EvoTables: TDataSource;
    function WGIdentityKind: TDataSource;
    procedure SetOnDBDTMappingChanged(Value: TDBDTMappingChangedEvent);
  end;

  TMappingKind = (mapNone, mapDirect, mapExpression, mapAdditionalField);
  TPartKind = (partLiteral, partTable);

function LoadDBDTMapperData(settings: IIsSettings; root: string): TDBDTMapperData;
procedure SaveDBDTMapperData(settings: IIsSettings; root: string; data: TDBDTMapperData);

function CreateDBDTMapper(serialized: TDBDTMapperData): IDBDTMapper;
function CreateDBDTMappingEditor(serialized: TDBDTMapperData; evo: TEvoDBDTInfo; aod: IExtAppConnection): IDBDTMappingEditor;

implementation

uses
  gdydbcommonnew, gdycommon, sysutils, IAeXMLBridge1, classes, dbclient, evodata, gdyStrSet;

type
  TDBDTMapper = class(TInterfacedObject, IDBDTMapper)
  private
    FWG: TClientDataSet;
    FWGDS: TDataSource;
    FExpression: TClientDataSet;
    FExpressionDS: TDataSource;
    FOnDBDTMappingChanged: TDBDTMappingChangedEvent;
    procedure CreateWGDataSet;
    procedure CreateExpressionDataSet;
    procedure HandleWGNewRecord(DataSet: TDataSet);
    procedure HandleWGBeforePost(DataSet: TDataSet);
    procedure HandleExpressionBeforePost(DataSet: TDataSet);
    procedure HandleAfterPost(DataSet: TDataSet);
    procedure HandleExpressionAfterInsert(DataSet: TDataSet);
    procedure LoadLegacy(legacy: string);
  public
    constructor Create(serialized: TDBDTMapperData);
    destructor Destroy; override;
  private
  {IDBDTMapper}
    function Dump: string;
    function CalcWorkgroup(wglevel: integer; codes: TEvoCodes): TWGValue;
    function GetCompanyWorkgroupLevel: Variant;
    function CalcTable(table: string; wgIdentities: TWGIdentities): Variant;
    function GetWorkgroupIdentityKind(wglevel: integer): TWGIdentityKind;
  protected
  {de facto IDBDTMappingEditor}
    function AsData: TDBDTMapperData;
    function WG: TDataSource;
    function Expression: TDataSource;
    procedure SetOnDBDTMappingChanged(Value: TDBDTMappingChangedEvent);
  end;

  TDBDTMappingEditor = class(TDBDTMapper, IDBDTMappingEditor)
  private
    FMaxWGLevel: integer;
    FEvoTables: TClientDataSet;
    FEvoTablesDS: TDataSource;
    FWGIdentityKind: TClientDataSet;
    FWGIdentityKindDS: TDataSource;
    procedure SetWG(aod: IExtAppConnection);
    procedure CreateEvoTablesDS(dbdtlevel: char);
    procedure CreateWGIdentityKindDS;
    procedure HandleFilterDBDT(DataSet: TDataSet; var Accept: Boolean);
  public
    constructor Create(serialized: TDBDTMapperData; evo: TEvoDBDTInfo; aod: IExtAppConnection);
    destructor Destroy; override;
  private
  {IDBDTMappingEditor}
    //almost all are inherited
    function EvoTables: TDataSource;
    function WGIdentityKind: TDataSource;
  end;

function CreateDBDTMapper(serialized: TDBDTMapperData): IDBDTMapper;
begin
  Result := TDBDTMapper.Create(serialized);
end;

function CreateDBDTMappingEditor(serialized: TDBDTMapperData; evo: TEvoDBDTInfo; aod: IExtAppConnection): IDBDTMappingEditor;
begin
  Result := TDBDTMappingEditor.Create(serialized, evo, aod);
end;

function LoadDBDTMapperData(settings: IIsSettings; root: string): TDBDTMapperData;
begin
  Result.LegacyTable := Settings.AsString[root + IIF(root='','','\') + 'WorkgroupMapping\DataPacket'];

  root := root + IIF(root='','','\') + 'DBDTMapping\';
  Result.WGTable := Settings.AsString[root+'WGTable\DataPacket'];
  Result.ExpressionTable := Settings.AsString[root+'ExpressionTable\DataPacket'];
end;

procedure SaveDBDTMapperData(settings: IIsSettings; root: string; data: TDBDTMapperData);
begin
  root := root + IIF(root='','','\') + 'DBDTMapping\';
  Settings.AsString[root+'WGTable\DataPacket'] := data.WGTable;
  Settings.AsString[root+'ExpressionTable\DataPacket'] := data.ExpressionTable;
end;

{ TDBDTMapper }

function TDBDTMapper.Dump: string;
//var
//  unfilteredExpression: TClientDataSet;
begin
  Result := 'DBDT Mapping'#13#10'WGTable:'#13#10+DataSetToCsv(FWG)+#13#10'ExpressionTable:'#13#10+DataSetToCsv(FExpression);
(*  unfilteredExpression := TClientDataSet.Create(nil);
  try
    unfilteredExpression.CloneCursor(FExpression,true, true);
//    unfilteredExpression.MasterSource := nil;
    Result := 'DBDT Mapping'#13#10'WGTable:'#13#10+DataSetToCsv(FWG)+#13#10'ExpressionTable:'#13#10+DataSetToCsv(FExpression);
  finally
    FreeANdNil(unfilteredExpression);
  end;*)
end;

function TDBDTMapper.CalcWorkgroup(wglevel: integer; codes: TEvoCodes): TWGValue;
  function EvoValueByTable(t: string): Variant;
  var
    i: char;
  begin
    for i := Low(CDBDTMetadata) to high(CDBDTMetadata) do
      if CDBDTMetadata[i].Table = t then
      begin
        Result := codes.cdbdt[i];
        Exit;
      end;
    if t = 'CO_JOBS' then
      Result := codes.job
    else if t = 'CO_HR_POSITIONS' then
      Result := codes.position
    else if t = 'CO_SHIFTS' then
      Result := codes.shift
    else
      Assert(false);
  end;

  function CalcExpression: Variant;
  var
    start, len: integer;
  begin
    FExpression.First;
    if FExpression.Eof then
      Result := Null
    else
      Result := '';

    while not FExpression.Eof do
    begin
      case TPartKind(FExpression.FieldByName('TYPE').AsInteger) of
        partLiteral: Result := Result + FExpression.FieldByName('LITERAL').AsString;
        partTable:
        begin
          if FExpression.FieldByName('START').IsNull then
            start := 1
          else
            start := FExpression['START'];

          if FExpression.FieldByName('END').IsNull then
            len := 99999
          else
            len := FExpression['END']-start+1;

          Result := Result + Copy(VarToStr(EvoValueByTable(FExpression['TABLE'])), start, len);
        end
      else
        Assert(false);
      end;
      FExpression.Next;
    end;
  end;

begin
  Result.Value := Null;
  Result.AdditionalFieldName := '';
  if FWG.Locate('LEVEL', wglevel, []) then
  begin
    Result.IdentityKind := TWGIdentityKind(FWG.FieldByName('ID_KIND').AsInteger);
    case TMappingKind(FWG.FieldByName('TYPE').AsInteger) of
      mapNone: ;
      mapDirect: Result.Value := EvoValueByTable(FWG['DIRECT']);
      mapExpression: Result.Value := CalcExpression;
      mapAdditionalField:
      begin
        Result.Value := Unassigned;
        Result.AdditionalFieldName := VarToStr(FWG['CO_ADD_INFO_NAMES_NAME']);
      end
    else
      Assert(false);
    end;
  end;
end;

function TDBDTMapper.GetCompanyWorkgroupLevel: Variant;
begin
  Result := FWG.Lookup('TYPE;DIRECT', VarArrayOf([ord(mapDirect), 'CO']), 'LEVEL');
end;

function DataSetToString(ds: TClientDataSet): string;
var
  ss: TStringStream;
begin
  ss := TStringStream.Create('');
  try
    ds.SaveToStream(ss, dfXML);
    Result := ss.DataString;
  finally
    FreeAndNil(ss);
  end;
end;

function TDBDTMapper.AsData: TDBDTMapperData;
begin
  FWG.MergeChangeLog;
  Result.WGTable := DataSetToString(FWG);
  FExpression.MergeChangeLog;
  Result.ExpressionTable := DataSetToString(FExpression);;
end;

procedure TDBDTMapper.CreateWGDataSet;
begin
  FWG := TClientDataSet.Create(nil);
  CreateIntegerField( FWG, 'LEVEL', 'Level').Visible := false;;
  CreateStringField( FWG, 'NAME', 'Workgroup', 40);
  CreateIntegerField( FWG, 'TYPE', 'Type').Visible := false;
  CreateStringField( FWG, 'DIRECT', 'Mapped to', 15).Visible := false;;
  CreateStringField( FWG, 'SUMMARY', 'Type', 10);
  CreateStringField( FWG, 'CO_ADD_INFO_NAMES_NAME', 'Additional Field', 40);
  CreateIntegerField( FWG, 'ID_KIND', 'Identity').Visible := false;
  FWG.CreateDataSet;
  FWG.OnNewRecord := HandleWGNewRecord;
end;

function StringToDataSet(s: string): TClientDataSet;
var
  ss: TStringStream;
begin
  ss := TStringStream.Create(s);
  try
    Result :=  TClientDataSet.Create(nil);
    try
      Result.LoadFromStream(ss);
    except
      FreeAndNil(Result);
    end;
  finally
    FreeAndNil(ss);
  end;
end;

procedure TDBDTMapper.LoadLegacy(legacy: string);
var
  ds: TDataSet;
begin
  try
    ds := StringToDataSet(legacy);
    try
      ds.First;
      while not ds.Eof do
      begin
        if FWG.Locate('LEVEL', ds['RIGHT_CODE'],[]) then
        begin
          FWG.Edit;
          try
            FWG['TYPE'] := Ord(mapDirect);
            FWG['DIRECT'] := ds['LEFT_CODE'];
            FWG.Post;
          except
            FWG.Cancel;
            raise;
          end;
        end;
        ds.Next;
      end;
    finally
      FreeAndNil(ds);
    end
  except
    //FLogger.StopException
  end;
end;

constructor TDBDTMapper.Create(serialized: TDBDTMapperData);
var
  i: integer;
begin
  CreateWGDataSet;
  if serialized.WGTable <> '' then
    LoadDataFromString(FWG, serialized.WGTable)
  else
  begin
    for i := 1 to 7 do
      Append(FWG, 'LEVEL;NAME;TYPE;DIRECT', [i, 'Workgroup '+IntToStr(i), ord(mapNone), 'CO']);
  end;    

  FWG.BeforePost := HandleWGBeforePost;
  FWG.AfterPost := HandleAfterPost;

  if (serialized.WGTable = '') and (serialized.LegacyTable <> '') then
    LoadLegacy(serialized.LegacyTable);

  FWGDS := TDataSource.Create(nil);
  FWGDS.DataSet := FWG;

  CreateExpressionDataSet;
  LoadDataFromString(FExpression , serialized.ExpressionTable);
  
  FExpression.IndexDefs.Add('LEVEL_ORDER', 'LEVEL;ORDER', []);
  FExpression.IndexName := 'LEVEL_ORDER';

  with FExpression.Aggregates.Add do
  begin
    AggregateName := 'MAXORDER';
    Expression := 'max(ORDER)';
//    IndexName := 'LEVEL_ORDER';
//    GroupingLevel := 1;
    Active := true;
  end;
  FExpression.AggregatesActive := true;

  FExpression.BeforePost := HandleExpressionBeforePost;
  FExpression.AfterPost := HandleAfterPost;
  FExpression.AfterInsert := HandleExpressionAfterInsert;
  FExpressionDS := TDataSource.Create(nil);
  FExpressionDS.DataSet := FExpression;

  FExpression.MasterSource := FWGDS;
  FExpression.MasterFields := 'LEVEL';
  FWG.DisableControls;
  FWG.EnableControls;
end;

destructor TDBDTMapper.Destroy;
begin
  FreeAndNil(FWGDS);
  FreeAndNil(FWG);
  FreeAndNil(FExpressionDS);
  FreeAndNil(FExpression);
end;

function TDBDTMapper.WG: TDataSource;
begin
  Result := FWGDS;
end;

function EvoNameByTable(t: string): string;
var
  i: char;
begin
  for i := Low(CDBDTMetadata) to high(CDBDTMetadata) do
    if CDBDTMetadata[i].Table = t then
    begin
      Result := CDBDTMetadata[i].Name;
      Exit;
    end;
  if t = 'CO_JOBS' then
    Result := 'Job'
  else if t = 'CO_HR_POSITIONS' then
    Result := 'Position'
  else if t = 'CO_SHIFTS' then
    Result := 'Shift'
  else
    Assert(false);
end;

procedure TDBDTMapper.HandleWGBeforePost(DataSet: TDataSet);
begin
  if FWG.FieldByName('TYPE').AsInteger = ord(mapNone) then
    FWG['SUMMARY'] := ''
  else if FWG.FieldByName('TYPE').AsInteger = ord(mapDirect) then
    FWG['SUMMARY'] := EvoNameByTable(FWG['DIRECT'])
  else if FWG.FieldByName('TYPE').AsInteger = ord(mapExpression) then
    FWG['SUMMARY'] := 'Custom'
  else if FWG.FieldByName('TYPE').AsInteger = ord(mapAdditionalField) then
    FWG['SUMMARY'] := FWG['CO_ADD_INFO_NAMES_NAME']
  else
    Assert(faLse);
end;

function TDBDTMapper.Expression: TDataSource;
begin
  Result := FExpressionDS;
end;

procedure TDBDTMapper.SetOnDBDTMappingChanged(Value: TDBDTMappingChangedEvent);
begin
  FOnDBDTMappingChanged := Value;
end;

procedure TDBDTMapper.HandleAfterPost(DataSet: TDataSet);
begin
  if assigned(FOnDBDTMappingChanged) then
    FOnDBDTMappingChanged;
end;

procedure TDBDTMapper.CreateExpressionDataSet;
begin
  FExpression := TClientDataSet.Create(nil);
  CreateIntegerField( FExpression, 'LEVEL', 'Level');
  CreateIntegerField( FExpression, 'ORDER', 'order');
  CreateIntegerField( FExpression, 'TYPE', 'type');
  CreateStringField( FExpression, 'LITERAL', 'Literal', 40);
  CreateStringField( FExpression, 'TABLE', 'Tablr', 15);
  CreateIntegerField( FExpression, 'START', 'Start');
  CreateIntegerField( FExpression, 'END', 'End');
  CreateStringField( FExpression, 'SUMMARY', 'Parts', 80);

  FExpression.CreateDataSet;
end;

procedure TDBDTMapper.HandleExpressionBeforePost(DataSet: TDataSet);
var
  s: string;
begin
  if FExpression.FieldByName('TYPE').AsInteger = ord(partLiteral) then
    s := 'Text "'+FExpression.FieldByName('LITERAL').AsString+'"'
  else if FExpression.FieldByName('TYPE').AsInteger = ord(partTable) then
  begin
    s := EvoNameByTable(FExpression['TABLE']) + ' code';
    if not FExpression.FieldByName('START').IsNull and FExpression.FieldByName('END').IsNull then
      s := s + Format(' from position %d to the end', [FExpression.FieldByName('START').AsInteger])
    else if not FExpression.FieldByName('START').IsNull and not FExpression.FieldByName('END').IsNull then
      s := s + Format(' from position %d to position %d', [FExpression.FieldByName('START').AsInteger, FExpression.FieldByName('END').AsInteger])
    else if FExpression.FieldByName('START').IsNull and not FExpression.FieldByName('END').IsNull then
      s := s + Format(' from the start to position %d', [FExpression.FieldByName('END').AsInteger]);
  end
  else
    Assert(false);
  FExpression['SUMMARY'] := {FExpression.FieldByName('ORDER').AsString + ', '+ }s;
end;

procedure TDBDTMapper.HandleExpressionAfterInsert(DataSet: TDataSet);
var
  v: Variant;
begin
  FExpression['LEVEL'] := FWG['LEVEL'];
  v := FExpression.Aggregates.Find('MAXORDER').Value;
  if VarIsNull(v) then
    FExpression['ORDER'] := 0
  else
    FExpression['ORDER'] := v + 1;
end;

//might support trivial custom mappings in the future
function TDBDTMapper.CalcTable(table: string; wgIdentities: TWGIdentities): Variant;
var
  wglevel: Variant;
begin
  wglevel := FWG.Lookup('TYPE;DIRECT', VarArrayOf([ord(mapDirect), table]), 'LEVEL');
  if not VarIsNull(wglevel) then
    Result := wgIdentities[Integer(wglevel)]
  else
    Result := Null;
end;

procedure TDBDTMapper.HandleWGNewRecord(DataSet: TDataSet);
begin
  DataSet['ID_KIND'] := Ord(wgCode);
end;

function TDBDTMapper.GetWorkgroupIdentityKind( wglevel: integer): TWGIdentityKind;
var
  v: Variant;
begin
  v := FWG.Lookup('LEVEL', wglevel, 'ID_KIND');
  if varIsNull(v) then //>maxWGLevel
    Result := wgCode
  else
    Result := TWGIdentityKind(Integer(v));
end;

{ TDBDTMappingEditor }

procedure TDBDTMappingEditor.SetWG(aod: IExtAppConnection);
var
  i: integer;
  wg: TAeWorkgroupLevel;
begin
  FMaxWGLevel := aod.getBaseClientData.WorkgroupLevels;
  for i := 1 to FMaxWGLevel do
  begin
    wg := aod.getWorkgroupLevelDetails(i);
    if not FWG.Locate('LEVEL', i, []) then
      Assert(false);
    FWG.Edit;
    try
      FWG['NAME'] := wg.WGLevelPluralName;
      FWG.Post;
    except
      FWG.Cancel;
      raise;
    end;
  end;
  FWG.OnFilterRecord := HandleFilterDBDT;
  FWG.Filtered := true;
end;

procedure TDBDTMappingEditor.HandleFilterDBDT(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := Dataset.FieldByName('LEVEL').AsInteger <= FMaxWGLevel;
end;

constructor TDBDTMappingEditor.Create(serialized: TDBDTMapperData; evo: TEvoDBDTInfo; aod: IExtAppConnection);
begin
  inherited Create(serialized);
  SetWG(aod);
  CreateEvoTablesDS(evo.DBDTLevel);
  CreateWGIdentityKindDS;
end;

function TDBDTMappingEditor.EvoTables: TDataSource;
begin
  Result := FEvoTablesDS;
end;

procedure TDBDTMappingEditor.CreateEvoTablesDS(dbdtlevel: char);
  procedure AddTable(t: string);
  begin
    Append(FEvoTables, 'TABLE;NAME', [t, EvoNameByTable(t)]);
  end;
var
  i: char;
begin
  FEvoTables := TClientDataSet.Create(nil);
  CreateStringField(FEvoTables, 'NAME', 'Name', 40);
  CreateStringField(FEvoTables, 'TABLE', 'Table', 15);

  FEvoTables.CreateDataSet;

  for i := CLIENT_LEVEL_COMPANY to dbdtlevel do
    Append(FEvoTables, 'TABLE;NAME', [CDBDTMetadata[i].Table, CDBDTMetadata[i].Name]);
  AddTable('CO_JOBS');
  AddTable('CO_HR_POSITIONS');
  AddTable('CO_SHIFTS');

  FEvoTablesDS := TDataSource.Create(nil);
  FEvoTablesDS.DataSet := FEvoTables;
end;

destructor TDBDTMappingEditor.Destroy;
begin
  inherited;
  FreeAndNil(FEvoTables);
  FreeAndNil(FEvoTablesDS);
  FreeAndNil(FWGIdentityKind);
  FreeAndNil(FWGIdentityKindDS);
end;

procedure TDBDTMappingEditor.CreateWGIdentityKindDS;
begin
  FWGIdentityKind := TClientDataSet.Create(nil);
  CreateIntegerField(FWGIdentityKind, 'ID_KIND', 'Identity');
  CreateStringField(FWGIdentityKind, 'NAME', 'Name', 10);

  FWGIdentityKind.CreateDataSet;
  Append(FWGIdentityKind, 'ID_KIND;NAME', [ord(wgCode), 'Code']);
  Append(FWGIdentityKind, 'ID_KIND;NAME', [ord(wgName), 'Name']);
  Append(FWGIdentityKind, 'ID_KIND;NAME', [ord(wgNumber), 'Number']);

  FWGIdentityKindDS := TDataSource.Create(nil);
  FWGIdentityKindDS.DataSet := FWGIdentityKind;
end;

function TDBDTMappingEditor.WGIdentityKind: TDataSource;
begin
  Result := FWGIdentityKindDS;
end;

end.

