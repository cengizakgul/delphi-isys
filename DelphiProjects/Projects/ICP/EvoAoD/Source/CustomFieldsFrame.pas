unit CustomFieldsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient,
  CustomBinderBaseFrame, ExtAppdecl;

type
  TCustomFieldsFrm = class(TCustomBinderBaseFrm)
    cdExtAppFields: TClientDataSet;
    cdExtAppFieldsCODE: TStringField;
    cdExtAppFieldsNAME: TStringField;
  private
    procedure InitExtAppFields;
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; CO_ADDITIONAL_INFO_NAMES: TDataSet);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, evConsts, common, IAeXMLBridge1;

{ TCustomFieldsFrm }

constructor TCustomFieldsFrm.Create(Owner: TComponent);
begin
  inherited;
  InitExtAppFields;
end;

procedure TCustomFieldsFrm.Init(matchTableContent: string; CO_ADDITIONAL_INFO_NAMES: TDataSet);
begin
  try
    InitExtAppFields;
    if not CO_ADDITIONAL_INFO_NAMES.Locate('Name', 'Standard Hours', []) then
    begin
      CO_ADDITIONAL_INFO_NAMES.Append;
      CO_ADDITIONAL_INFO_NAMES.FieldByName('Name').AsString := 'Standard Hours';
      CO_ADDITIONAL_INFO_NAMES.Post;
      CO_ADDITIONAL_INFO_NAMES.FieldByName('NAME').DisplayLabel := 'Name';
    end;
    FBindingKeeper := CreateBindingKeeper( CustomFieldsBinding, TClientDataSet );
    FBindingKeeper.SetTables(CO_ADDITIONAL_INFO_NAMES, cdExtAppFields);
    FBindingKeeper.SetVisualBinder(BinderFrm1);
    FBindingKeeper.SetMatchTableFromString(matchTableContent);
    FBindingKeeper.SetConnected(true);
  except
    UnInit;
    raise;
  end;
end;

procedure TCustomFieldsFrm.InitExtAppFields;
begin
  CreateOrEmptyDataSet( cdExtAppFields );
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppStaticCustomField1Code, 'Static custom field 1']);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppStaticCustomField2Code, 'Static custom field 2']);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppStaticCustomField3Code, 'Static custom field 3']);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppStaticCustomField4Code, 'Static custom field 4']);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppStaticCustomField5Code, 'Static custom field 5']);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppStaticCustomField6Code, 'Static custom field 6']);

  Append(cdExtAppFields, 'CODE;NAME', [sExtAppPayClassCode, sPayClass]);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppHourlyStatusCode, sHourlyStatus]);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppClockGroupCode, 'Clock group']);
  Append(cdExtAppFields, 'CODE;NAME', [sExtAppSchedulePatternCode, 'Schedule pattern']);
end;

end.



