#define APPBASE "EvoAoD"
#define APPNAME "Evolution AoD Import"

[Files]
Source: ..\Resources\toa_map.xml; DestDir: {app}; Flags: ignoreversion
Source: ..\..\..\..\bin\ICP\{#APPBASE}\EvoAoDService.exe; DestDir: {app}; Flags: ignoreversion
Source: EvoAoDService.red; DestDir: {app}; Flags: ignoreversion

#include <..\..\Common\Installer\common.iss>
 
