unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, InvokeRegistry, Rio, SOAPHTTPClient, IEvolutionAoDImport1;

type
  TForm1 = class(TForm)
    Button1: TButton;
    cbOperation: TComboBox;
    edURL: TEdit;
    lblURL: TLabel;
    Label3: TLabel;
    edCompany: TEdit;
    lblCompany: TLabel;
    mmLog: TMemo;
    Log: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  typinfo;
{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  try
    if trim(edURL.Text) = '' then
      raise Exception.CreateFmt('%s is not entered',[lblURL.Caption]);
    if trim(edCompany.Text) = '' then
      raise Exception.CreateFmt('%s is not entered',[lblCompany.Caption]);

    GetIEvolutionAoDImport(false, trim(edURL.Text)).InitiateOperation(TOperation(cbOperation.ItemIndex), trim(edCompany.Text));

    mmLog.Lines.Add(Format('InitiateOperation(%s, %s) succeeded',
      [GetEnumName(TypeInfo(TOperation), cbOperation.ItemIndex),
      trim(edCompany.Text)]));
  except
    on E: Exception do
    begin
      mmLog.Lines.Add(E.Message);
      raise;
    end;
  end;
end;

end.
