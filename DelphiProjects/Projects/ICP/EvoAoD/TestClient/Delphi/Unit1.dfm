object Form1: TForm1
  Left = 746
  Top = 390
  Width = 427
  Height = 403
  Caption = 'Evolution AoD Import Test Client'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    419
    369)
  PixelsPerInch = 96
  TextHeight = 13
  object lblURL: TLabel
    Left = 16
    Top = 8
    Width = 85
    Height = 13
    Caption = 'Web service URL'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Operation'
  end
  object lblCompany: TLabel
    Left = 216
    Top = 56
    Width = 71
    Height = 13
    Caption = 'Company code'
  end
  object Log: TLabel
    Left = 16
    Top = 152
    Width = 18
    Height = 13
    Caption = 'Log'
  end
  object Button1: TButton
    Left = 16
    Top = 112
    Width = 113
    Height = 25
    Caption = 'Initiate operation'
    TabOrder = 3
    OnClick = Button1Click
  end
  object cbOperation: TComboBox
    Left = 16
    Top = 72
    Width = 185
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 1
    Text = 'Employee export (Evolution->AoD)'
    Items.Strings = (
      'Employee export (Evolution->AoD)'
      'TOA export (Evolution->AoD)'
      'TOA import (AoD->Evolution)')
  end
  object edURL: TEdit
    Left = 14
    Top = 24
    Width = 391
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'http://localhost:1088/soap/IEvolutionAoDImport'
  end
  object edCompany: TEdit
    Left = 214
    Top = 72
    Width = 191
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
  end
  object mmLog: TMemo
    Left = 16
    Top = 168
    Width = 389
    Height = 189
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 4
  end
end
