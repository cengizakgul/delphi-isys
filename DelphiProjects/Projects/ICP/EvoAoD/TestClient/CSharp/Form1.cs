﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace EvoAoDTestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EvoAoDSvc.IEvolutionAoDImportservice svc = new EvoAoDTestClient.EvoAoDSvc.IEvolutionAoDImportservice();
            svc.Url = "http://localhost:1024/soap/IEvolutionAoDImport";
            svc.InitiateOperation(EvoAoDTestClient.EvoAoDSvc.TOperation.opTimeclockImport, "50022");
        }
    }
}
