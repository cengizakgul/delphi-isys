@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoAoD
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoAoD.exe
cd ..\..\..\Projects\ICP\EvoAoD\Installer\Projects

SET TEMPPATH=..\..\..\..\..\Tmp

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoAoD.msi 

"%pWiXDir%\candle.exe" .\EvoAoD.wxs -wx -out %TEMPPATH%\EvoAoD.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -var var.Path -out %TEMPPATH%\EvoAoDRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvoAoDRwQueries.wxs -wx -dPath="..\..\Resources\Queries" -out %TEMPPATH%\EvoAoDRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir "..\..\..\Common\HTML Templates" -gg -sfrag -srd -cg HTMLTemplatesGroup -dr HTMLTEMPLATESDIR -var var.Path -out %TEMPPATH%\EvoAoDHTMLTemplates.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvoAoDHTMLTemplates.wxs -wx -out %TEMPPATH%\EvoAoDHTMLTemplates.wixobj -dPath="..\..\..\Common\HTML Templates"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" %TEMPPATH%\EvoAoD.wixobj %TEMPPATH%\EvoAoDRwQueries.wixobj %TEMPPATH%\EvoAoDHTMLTemplates.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoAoD.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoAoD.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoAoD_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoAoD_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoAoD.msi EvoAoD_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
