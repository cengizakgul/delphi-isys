unit JobsToStatesFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient,
  CustomBinderBaseFrame;

type
  TJobsToStatesFrm = class(TCustomBinderBaseFrm)
  public
    procedure Init(matchTableContent: string; CO_JOBS: TDataSet; CO_STATES: TDataSet);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, gdydbcommon, StatesModifier;

{ TJobsToStatesFrm }

procedure TJobsToStatesFrm.Init(matchTableContent: string; CO_JOBS: TDataSet; CO_STATES: TDataSet);
begin
  FBindingKeeper := CreateBindingKeeper( JobsToStatesBinding, TClientDataSet );
  CO_JOBS.FieldByName('DESCRIPTION').DisplayLabel := 'Job code';
  CO_JOBS.FieldByName('TRUE_DESCRIPTION').DisplayLabel := 'Description';
  CO_STATES.FieldByName('STATE').DisplayLabel := 'State';
  CO_STATES.FieldByName('NAME').DisplayLabel := 'Name';
  FBindingKeeper.SetTables(CO_JOBS, CO_STATES);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

end.



