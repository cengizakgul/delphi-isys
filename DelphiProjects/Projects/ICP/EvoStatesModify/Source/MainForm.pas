unit MainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame, 
  EvolutionPrPrBatchFrame, OptionsBaseFrame, SchedulerFrame,
  SmtpConfigFrame, CustomBinderBaseFrame, JobsToStatesFrame,
  EvolutionPrFrame;

type
  TMainFm = class(TEvoAPIClientMainFm)
    pnlBottom: TPanel;
    ConnectToEvo: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshCompanySettings: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    JobsToStatesFrame: TJobsToStatesFrm;
    PrFrame: TEvolutionPrFrm;
    actModifyStates: TAction;
    BitBtn1: TBitBtn;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure actModifyStatesUpdate(Sender: TObject);
    procedure actModifyStatesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure HandleJobsToStatesMappingChanged;
    procedure AutoConnect;
    procedure DoModifyStates;
  protected
    FEvoData: TEvoData;
  protected
    procedure UnInitPerCompanySettings;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;


implementation

{$R *.dfm}
uses
  isSettings, EEUtilityLoggerViewFrame, dialogs, statesModifier,
  gdyGlobalWaitIndicator, gdyDeferredCall;

procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      FEvoData.Connect(EvoFrame.Param);
      ConnectToEvo.Caption := 'Refresh data';
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEEUtilityLoggerViewFrm;
  inherited;
  ConnectToEvo.Caption := 'Get data';

  FEvoData := TEvoData.Create(Logger);
  CompanyFrame.Init(FEvoData);

  UnInitPerCompanySettings;
  FEvoData.Advise(HandleCompanyChanged);

//////////////////

  JobsToStatesFrame.OnMatchTableChanged := HandleJobsToStatesMappingChanged;

  FEvoData.AddDS(TMP_PRDesc);
  FEvoData.AddDS(CO_JOBSDesc);
  FEvoData.AddDS(CO_STATESDesc);

  if EvoFrame.IsValid then
    PageControl1.TabIndex := TabSheet2.TabIndex
  else
    PageControl1.TabIndex := TabSheet3.TabIndex;
  PrFrame.Init(FEvoData);
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UnInitPerCompanySettings;
  Repaint;
  if new.HasValue then
  begin
    LoadPerCompanySettings(new.Value);
    Repaint;
  end
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  try
    JobsToStatesFrame.Init( FSettings.AsString[CompanyUniquePath(Value)+'\JobsToStatesMapping\DataPacket'], FEvoData.DS[CO_JOBSDesc.Name], FEvoData.DS[CO_STATESDesc.Name] );
  except
    Application.HandleException(ExceptObject as Exception);
    Logger.StopException;
  end;
end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  try
    JobsToStatesFrame.UnInit;
  except
    Application.HandleException(ExceptObject as Exception);
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleJobsToStatesMappingChanged;
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\JobsToStatesMapping\DataPacket'] := JobsToStatesFrame.SaveToString;
end;

procedure TMainFm.actModifyStatesUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetPr and JobsToStatesFrame.CanCreateMatcher;
end;

procedure TMainFm.DoModifyStates;
var
  stat: TStatesModifierStat;
  opname: string;
begin
  opname := 'States modification';
  Logger.LogEntry(opname);
  try
    try
      LogEvoPayrollDef(Logger, FEvoData.GetPr);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait(opname);
      try
        with TStatesModifier.Create(FEvoData.Connection, FEvoData.GetPr, JobsToStatesFrame.Matcher, Logger) do
        try
          stat := ModifyStates;
        finally
          Free;
        end;
      finally
        WaitIndicator.EndWait;
      end;

      if stat.Errors = 0 then
      begin
        Logger.LogEvent( opname + ' ' + StatesModifierStatToString(stat) );
        ShowMessage( opname + ' ' + StatesModifierStatToString(stat) );
      end
      else
      begin
        Logger.LogWarning(opname + ' ' + StatesModifierStatToString(stat));
        MessageDlg(opname + ' ' + StatesModifierStatToString(stat), mtError, [mbOk], 0);
        PageControl1.TabIndex := tbshLog.TabIndex;
      end
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
      PageControl1.TabIndex := tbshLog.TabIndex;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actModifyStatesExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      if MessageDlg( 'Modify states?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        DoModifyStates;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.AutoConnect;
begin
  Logger.LogEntry('Application opened');
  try
    try
      Repaint;
      if EvoFrame.IsValid then
      begin
        FEvoData.Connect(EvoFrame.Param);
        ConnectToEvo.Caption := 'Refresh data';
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.FormShow(Sender: TObject);
begin
  DeferredCall(AutoConnect);
end;

end.
