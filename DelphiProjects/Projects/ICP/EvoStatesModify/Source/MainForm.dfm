inherited MainFm: TMainFm
  Left = 458
  Top = 296
  Caption = 'MainFm'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 41
    Height = 452
    ActivePage = TabSheet2
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 776
        Align = alTop
        inherited GroupBox1: TGroupBox
          Width = 776
          inherited cbSavePassword: TCheckBox
            Width = 106
          end
        end
      end
    end
    object tbshCompanySettings: TTabSheet [1]
      Caption = 'Specify states'
      ImageIndex = 3
      inline JobsToStatesFrame: TJobsToStatesFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 424
        Align = alClient
        TabOrder = 0
        inherited BinderFrm1: TBinderFrm
          Width = 776
          Height = 424
          inherited evSplitter2: TSplitter
            Top = 160
            Width = 776
          end
          inherited pnltop: TPanel
            Width = 776
            Height = 160
            inherited evSplitter1: TSplitter
              Height = 164
            end
            inherited pnlTopLeft: TPanel
              Height = 164
              inherited dgLeft: TReDBGrid
                Height = 139
              end
            end
            inherited pnlTopRight: TPanel
              Width = 514
              Height = 164
              inherited evPanel4: TPanel
                Width = 514
              end
              inherited dgRight: TReDBGrid
                Width = 514
                Height = 139
              end
            end
          end
          inherited evPanel1: TPanel
            Top = 165
            Width = 776
            inherited pnlbottom: TPanel
              Width = 784
              inherited evPanel5: TPanel
                Width = 784
              end
              inherited dgBottom: TReDBGrid
                Width = 784
              end
            end
            inherited pnlMiddle: TPanel
              Width = 784
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Modify states'
      object pnlBottom: TPanel
        Left = 0
        Top = 383
        Width = 776
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 89
          Height = 25
          Action = actModifyStates
          Caption = 'Modify states'
          TabOrder = 0
        end
      end
      inline PrFrame: TEvolutionPrFrm
        Left = 0
        Top = 0
        Width = 776
        Height = 383
        Align = alClient
        TabOrder = 1
        inherited PayrollFrame: TEvolutionDsFrm
          Width = 776
          Height = 383
          inherited Panel1: TPanel
            Width = 776
            Alignment = taLeftJustify
            Caption = ' Payrolls'
          end
          inherited dgGrid: TReDBGrid
            Width = 776
            Height = 358
          end
        end
      end
    end
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 97
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 8
        Top = 8
        Width = 81
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution and XXX data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 97
      Top = 0
      Width = 687
      Height = 41
      Align = alClient
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object ConnectToEvo: TAction
      Caption = 'Get Evolution and XXX data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object actModifyStates: TAction
      Caption = 'Modify states'
      OnExecute = actModifyStatesExecute
      OnUpdate = actModifyStatesUpdate
    end
  end
end
