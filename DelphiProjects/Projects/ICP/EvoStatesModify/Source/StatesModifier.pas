unit StatesModifier;

interface

uses
	gdyBinder, common, evoapiconnection, gdyCommonLogger, kbmMemTable;

const
  JobsToStatesBinding: TBindingDesc =
    ( Name: 'JobsToStatesBinding';
      Caption: 'Jobs and states';
      LeftDesc: ( Caption: 'Jobs'; Unique: true; KeyFields: 'DESCRIPTION'; ListFields: 'DESCRIPTION;TRUE_DESCRIPTION');
      RightDesc: ( Caption: 'States'; Unique: false; KeyFields: 'STATE'; ListFields: 'STATE;NAME');
      HideKeyFields: true;
    );

type
  TStatesModifierStat = record
    Errors: integer;
    Modified: integer;
  end;

  TStatesModifier = class
  private
    FConn: IEvoAPIConnection;
    FPr: TEvoPayrollDef;
    FJobsToStates: IMatcher;
    FLogger: ICommonLogger;

    FEEStates: TkbmCustomMemTable;
    FPrCheckLines: TkbmCustomMemTable;
  public
    constructor Create(conn: IEvoAPIConnection; Pr: TEvoPayrollDef; JobsToStates: IMatcher; Logger: ICommonLogger);
    destructor Destroy; override;
    function ModifyStates: TStatesModifierStat;
  end;

function StatesModifierStatToString(const stat: TStatesModifierStat): string;

implementation

uses
  sysutils, evoApiConnectionUtils, gdyGlobalWaitIndicator, variants,
  XmlRpcTypes, gdycommon;

{ TStatesModifier }

constructor TStatesModifier.Create(conn: IEvoAPIConnection;
  Pr: TEvoPayrollDef; JobsToStates: IMatcher; Logger: ICommonLogger);
begin
  FConn := conn;
  FPr := Pr;
  FJobsToStates := JobsToStates;
  FLogger := Logger;

  FEEStates := ExecCoQuery(FConn, FLogger, FPr.Company, 'CUSTOM_EE_STATES.rwq', 'Getting employee states from Evolution' );
  FPrCheckLines := ExecPrQuery(FConn, FLogger, FPr, 'CUSTOM_PR_CHECK_LINES.rwq', 'Getting check lines from Evolution' );
end;

destructor TStatesModifier.Destroy;
begin
  FreeAndNil(FEEStates);
  FreeAndNil(FPrCheckLines);
  inherited;
end;

function StatesModifierStatToString(const stat: TStatesModifierStat): string;
begin
  if stat.Errors = 0 then
  begin
    if stat.Modified = 0  then
      Result := 'completed: all states were already correct'
    else
      Result := Format('completed: %d check %s updated', [stat.Modified, Plural('line', stat.Modified)]);
  end
  else
  begin
    if stat.Modified = 0  then
      Result := Format('failed: %d %s occured', [stat.Errors, Plural('error', stat.Errors)])
    else
      Result := Format('partially completed: %d %s occured, %d check %s updated',
        [stat.Errors, Plural('error', stat.Errors), stat.Modified, Plural('line', stat.Modified)]);
  end
end;

function TStatesModifier.ModifyStates: TStatesModifierStat;
var
  pr_check_lines_nbr: string;
  job: string;
  stateAbbr: Variant;
  ee_states_nbr: Variant;

  changes: IRpcStruct;
begin
  Result.Errors := 0;
  Result.Modified := 0;
  FLogger.LogEntry('Check lines analysis');
  try
    try
      changes := TRpcStruct.Create;

      FPrCheckLines.First;
      while not FPrCheckLines.eof do
      begin
        FLogger.LogEntry('Analyzing check line');
        try
          try

            FLogger.LogContextItem(sCtxEECode, FPrCheckLines.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString );
            FLogger.LogContextItem('Batch#', FPrCheckLines.FieldByName('PR_BATCH_NBR').AsString );
            pr_check_lines_nbr := FPrCheckLines.FieldByName('PR_CHECK_LINES_NBR').AsString;
            FLogger.LogContextItem('Internal check line#', pr_check_lines_nbr );

            job := FPrCheckLines.FieldByName('JOB_CODE').AsString;
            stateAbbr := FJobsToStates.RightMatch(job);
            if VarIsNull(stateAbbr) then
              raise Exception.CreateFmt('Job <%s> doesn''t have a state specified',[job]);
            ee_states_nbr := FEEStates.Lookup('EE_NBR;STATE', VarArrayOf( [FPrCheckLines['EE_NBR'], stateAbbr] ), 'EE_STATES_NBR');
            if VarIsNull(ee_states_nbr) then
              raise Exception.CreateFmt('Employee doesn''t have %s state set up', [stateAbbr]);
            if ee_states_nbr <> FPrCheckLines['EE_STATES_NBR'] then
            begin
              changes.AddItem('PR_CHECK_LINES__EE_STATES_NBR__'+pr_check_lines_nbr, Integer(ee_states_nbr) );
              Result.Modified := Result.Modified + 1;
            end;  
          except
            Result.Errors := Result.Errors + 1;
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
        FPrCheckLines.Next;
      end;

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;

  FLogger.LogEntry('Check lines modification');
  try
    try

      if changes.Count > 0 then
      begin
        changes.AddItem('PR_CHECK_LINES', Format('PR_NBR=%d', [FPr.Pr.PrNbr]));
        WaitIndicator.StartWait('Modifying check lines');
        try
          FConn.postTableChanges(FPr.Company.ClNbr, changes);
        finally
          WaitIndicator.EndWait;
        end;
      end

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

end.
