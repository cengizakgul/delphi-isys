@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoStatesModify
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoStatesModify.exe
cd ..\..\..\Projects\ICP\EvoStatesModify\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoStatesModify.msi 

"%pWiXDir%\candle.exe" EvoStatesModify.wxs -wx -out ..\..\..\..\..\Tmp\EvoStatesModify.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoStatesModifyRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoStatesModifyRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoStatesModifyRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoStatesModify.wixobj ..\..\..\..\..\Tmp\EvoStatesModifyRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoStatesModify.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoStatesModify.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoStatesModify_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoStatesModify_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoStatesModify.msi EvoStatesModify_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
