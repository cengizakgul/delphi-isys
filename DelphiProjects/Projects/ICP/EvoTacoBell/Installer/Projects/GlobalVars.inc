<Include>

<!-- Global Variables -->
<?define softwareName = "EvoUtils" ?>
<?define productManufacturer = "ISystems, LLC." ?>
<?define productManufacturerAbbr = "iSystems" ?>
<?define defaultVolume = "C:\Program Files\" ?>
<?define resourceFileDir = "..\..\..\..\Evolution\Installer\Resources\" ?>
<?define htmlTemplatesDir = "..\..\..\..\Common\HTML Templates\" ?>
<?define customActionDLL = $(var.resourceFileDir)isMsiCustomAction.dll ?>

</Include>
