@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoTacoBell
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoTacoBell.exe
cd ..\..\..\Projects\ICP\EvoTacoBell\Installer\Projects

SET TEMPPATH=..\..\..\..\..\Tmp

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoTacoBell.msi 

"%pWiXDir%\candle.exe" .\EvoTacoBell.wxs -wx -out %TEMPPATH%\EvoTacoBell.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -var var.Path -out %TEMPPATH%\EvoTacoBellRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvoTacoBellRwQueries.wxs -wx -dPath="..\..\Resources\Queries" -out %TEMPPATH%\EvoTacoBellRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir "..\..\..\Common\HTML Templates" -gg -sfrag -srd -cg HTMLTemplatesGroup -dr HTMLTEMPLATESDIR -var var.Path -out %TEMPPATH%\EvoTacoBellHTMLTemplates.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvoTacoBellHTMLTemplates.wxs -wx -out %TEMPPATH%\EvoTacoBellHTMLTemplates.wixobj -dPath="..\..\..\Common\HTML Templates"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" %TEMPPATH%\EvoTacoBell.wixobj %TEMPPATH%\EvoTacoBellRwQueries.wixobj %TEMPPATH%\EvoTacoBellHTMLTemplates.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoTacoBell.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoTacoBell.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoTacoBell_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoTacoBell_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoTacoBell.msi EvoTacoBell_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
