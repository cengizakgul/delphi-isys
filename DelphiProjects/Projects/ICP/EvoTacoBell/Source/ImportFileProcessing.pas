unit ImportFileProcessing;

interface

uses
  classes, gdycommonlogger, isSettings, common, SysUtils, EvoData, gdyBinder,
  kbmMemTable, DB;

// Find files in selected direcroties,
// Extract certain records
// Return EvoX Employee Import or TCImport file

type
  TEeName = record
    FirstName: string;
    LastName: string;
    MI: string;
  end;

  TImportFileProc = class
  private
    FConf: IisSettings;
    FLogger: ICommonLogger;
    FDirList: TStrings;
    FDivList: TStrings;
    FColumns: TStrings;
    FImportFile: TStrings;
    FEvoCo: TEvoCompanyDef;
    FAssignedDiv: string;
    FEvoEECodes: TkbmCustomMemTable;

    procedure GetDirectoryList;
    procedure ProcessFiles;
    function ParseEmployeeName(const aEeName: string): TEeName;
    procedure findFilesForPeriod(var fNames: TStrings; dat_b, dat_e: TDatetime);
  protected
    function GetEvoSSN(const aTacoBellSSN: string): string;
    function GetEeCodeBySSN(const aSSN: string): string;
    procedure GetFileNames(var fNames: TStrings); virtual; abstract;
    procedure DoBeforeProcessFiles; virtual;
    procedure DoProcessRecord; virtual; abstract;
    function ValidRecordType(const aRecType: string): boolean; virtual; abstract;
    procedure InsertFileHeader; virtual;
  public
    constructor Create(EvoCo: TEvoCompanyDef; logger: ICommonLogger; conf: IisSettings; EvoEECodes: TkbmCustomMemTable);
    destructor Destroy; override;

    function GetImportData: string;
  end;

  TNewHireImportFileProc = class(TImportFileProc)
  private
    FMaxEeCode: string;
    FMaxEeCodeInt: integer;
    FEeNumber: integer;
    FSSNList: TStrings;

    function GetNextEmployeeCode(const aSSN: string): string;
    function MapFedMaritalStatus(const aValue: string): string;
    function MapStateMaritalStatus(const aValue: string): string;
  protected
    procedure GetFileNames(var fNames: TStrings); override;
    procedure DoProcessRecord; override;
    procedure DoBeforeProcessFiles; override;
    function ValidRecordType(const aRecType: string): boolean; override;
    procedure InsertFileHeader; override;
  public
    constructor Create(EvoCo: TEvoCompanyDef; logger: ICommonLogger; conf: IisSettings; EvoEECodes: TkbmCustomMemTable;
      const aMaxEeCode: string);
    destructor Destroy; override;
  end;

  TTimeClockImportFileProc = class(TImportFileProc)
  private
    FDat_b: TDatetime;
    FDat_e: TDatetime;
    FEDCodeMatcher: IMatcher;
    procedure AddTCLine(const aEeCode, Dv, Br, Dp, Tm: string; const aHours: string; const aEDCode: string);
  protected
    procedure GetFileNames(var fNames: TStrings); override;
    procedure DoProcessRecord; override;
    function ValidRecordType(const aRecType: string): boolean; override;
  public
    constructor Create(EvoCo: TEvoCompanyDef; logger: ICommonLogger; conf: IisSettings; EvoEECodes: TkbmCustomMemTable; dat_b, dat_e: TDatetime;
      EDCodeMatcher: IMatcher);
  end;

implementation

uses
  TacoBellCommon, gdystrset, StrUtils, Variants;
  
{ TImportFileProc }

constructor TImportFileProc.Create(EvoCo: TEvoCompanyDef; logger: ICommonLogger; conf: IisSettings; EvoEECodes: TkbmCustomMemTable);
begin
  FLogger := logger;
  FConf := conf;
  FEvoCo := EvoCo;

  FDirList := TStringList.Create;
  FDivList := TStringList.Create;
  FColumns := TStringList.Create;
  FImportFile := TStringList.Create;
  FEvoEECodes := EvoEECodes;
end;

destructor TImportFileProc.Destroy;
begin
  FreeAndNil(FDirList);
  FreeAndNil(FDivList);
  FreeAndNil(FColumns);
  FreeAndNil(FImportFile);
  inherited;
end;

procedure TImportFileProc.DoBeforeProcessFiles;
begin
  //
end;

procedure TImportFileProc.findFilesForPeriod(var fNames: TStrings; dat_b,
  dat_e: TDatetime);
begin
  if Assigned(fNames) then
  begin
    fNames.Clear;
    while Trunc(dat_b) <= Trunc(dat_e) do
    begin
      fNames.Add( FormatDateTime('YYYYMMDD', dat_b) + '.000' );
      dat_b := dat_b + 1;
    end;
    FLogger.LogDebug('File Names', fNames.Text);
  end;
end;

procedure TImportFileProc.GetDirectoryList;
var
  lDirList: TStrings;
  i: integer;
begin
  FDirList.Clear;
  FDivList.Clear;

  lDirList := TStringList.Create;
  FLogger.LogEntry('Get Directory List');
  try
    lDirList.Text := LoadDirListSettings(FConf, CompanyUniquePath(FEvoCo));
    for i := 0 to lDirList.Count - 1 do
    begin
      if Pos(sDirDivDelimiter, lDirList[i]) > 0 then
      begin
        FDirList.Add(Copy(lDirList[i], 1, Pos(sDirDivDelimiter, lDirList[i]) - 1));
        FDivList.Add(Copy(lDirList[i], Pos(sDirDivDelimiter, lDirList[i]) + Length(sDirDivDelimiter), Length(lDirList[i])));
      end
      else begin
        FDirList.Add(lDirList[i]);
        FDivList.Add('');
      end;
      FLogger.LogDebug('Dir #'+IntToStr(i+1), lDirList[i]);
    end;
  finally
    FreeAndNil(lDirList);
    FLogger.LogExit;
  end;
end;

function TImportFileProc.GetImportData: string;
begin
  FLogger.LogEvent('Files processing started: ' + DateTimeToStr(Now));
  try
    FLogger.LogEvent('Analyse import data...');
    FImportFile.Clear;
    GetDirectoryList;
    DoBeforeProcessFiles;
    ProcessFiles;
    if FImportFile.Count > 0 then
    begin
      InsertFileHeader;
      Result := FImportFile.Text;
    end  
    else
      Result := '';
  finally
    FLogger.LogEvent('Files processing finished: ' + DateTimeToStr(Now), Result);
  end;
end;

procedure TImportFileProc.InsertFileHeader;
begin
  //
end;

function TImportFileProc.ParseEmployeeName(const aEeName: string): TEeName;
begin
  if Pos(',', aEeName) <= 0 then
    Result.LastName := aEeName
  else begin
    Result.LastName := Copy(aEeName, 1, Pos(',', aEeName) - 1);
    Result.FirstName := Copy(aEeName, Pos(',', aEeName) + 1, Length(aEeName));
    Result.MI := '';

    if Pos(' ', Result.FirstName) > 0 then
    begin
      Result.MI := Copy(Result.FirstName, Pos(' ', Result.FirstName) + 1, 1);
      Result.FirstName := Copy(Result.FirstName, 1, Pos(' ', Result.FirstName) - 1);
    end;
  end;
  Result.LastName := Trim(Result.LastName);
  Result.FirstName := Trim(Result.FirstName);
  Result.MI := Trim(Result.MI);
end;

procedure TImportFileProc.ProcessFiles;
var
  i, j, k, z: integer;
  fNames, fContent: TStrings;
  d, f: string;
begin
  z := 0;
  d := '';
  f := '';
  FLogger.LogEntry('Process Files');
  fNames := TStringList.Create;
  fContent := TStringList.Create;
  try
    GetFileNames(fNames);
    for i := 0 to FDirList.Count - 1 do
    begin
      FAssignedDiv := FDivList[i];
      for j := 0 to fNames.Count - 1 do
      begin
        f := FDirList[i] + '\' + fNames[j];
        if FileExists( f ) then
        begin
          FLogger.LogDebug('Process file ' + f);
          Inc(z);
          d := d + f + #10#13;

          try
            fContent.LoadFromFile(f);
          except
            FLogger.PassthroughException;
          end;

          FColumns.Clear;

          // the first record is file header
          for k := 1 to fContent.Count - 1 do
          try
            FColumns.CommaText := fContent[k];
            if (FColumns.Count < 3) then
              raise Exception.CreateFmt('Can not define Record Type from the record: ''%s'' ',[fContent[k]]);

            if ValidRecordType(FColumns[2]) then
            begin
              FLogger.LogDebug('File: "'+f+'", Record No: ' + IntToStr(k), fContent[k]);
              DoProcessRecord;
            end;
          except
            on E: Exception do
              FLogger.LogError('Process Files error! File: '+f+'", Record No: ' + IntToStr(k), 'Error: ' + e.Message + ', Record: ' + fContent[k]);
          end;
        end;
      end;
    end;  
    FLogger.LogEvent(IntToStr(z) + ' files have been processed', d);
    FLogger.LogEvent(IntToStr( FImportFile.Count ) + ' records have been found', d);
  finally
    FreeAndNil(fNames);
    FreeAndNil(fContent);
    FLogger.LogExit;
  end;
end;

function TImportFileProc.GetEvoSSN(const aTacoBellSSN: string): string;
begin
  Result := Copy(aTacoBellSSN, 1, 3) + '-' + Copy(aTacoBellSSN, 4, 2) + '-' + Copy(aTacoBellSSN, 6, 4);
end;

{ TNewHireImportFileProc }

procedure TNewHireImportFileProc.DoBeforeProcessFiles;
begin
  inherited;
  FEeNumber := 0;
//  FMaxEeCode := FEvoData.DS['EE_NUMBERS'].FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString;
  if not TryStrToInt(FMaxEeCode, FMaxEeCodeInt) then
    FMaxEeCodeInt := 0;
end;

procedure TNewHireImportFileProc.DoProcessRecord;
var
  line, lssn: string;
  EeName: TEeName;
begin
  FEeNumber := FEeNumber + 1;
  line := '';
  Assert(FColumns.Count = 33, 'HI1 (New Hire) record should contain 33 columns');

  lssn := Trim(FColumns[6]);
  EeName := ParseEmployeeName(FColumns[8]);

  if Trim(FColumns[12]) = '' then
    raise Exception.Create('State is required!');

  if FSSNList.IndexOf(lssn) < 0 then
  begin
    // prepare data for RunEvoXImport
    line := '"' + Trim(FEvoCo.CUSTOM_COMPANY_NUMBER) + '","' +  // CUSTOM_COMPANY_NUMBER
      lssn + '","' +                                            // SSN
      EeName.LastName + '","' +                                 // LAST_NAME
      EeName.FirstName + '","' +                                // FIRST_NAME
      EeName.MI + '","' +                                       // MIDDLE_INITIAL
      GetNextEmployeeCode(Trim(FColumns[6])) + '","' +          // EE_CODE
      FAssignedDiv + '","' +                                    // DIVISION
      Trim(FColumns[32]) + '","' +                              // BRANCH
      Trim(FColumns[9]) + '","' +                               // DEPARTMENT
      Trim(FColumns[10]) + '","' +                              // ADDRESS
      Trim(FColumns[11]) + '","' +                              // CITY
      Trim(FColumns[12]) + '","' +                              // STATE
      Trim(FColumns[13]) + '","' +                              // ZIP_CODE
      Trim(FColumns[14]) + '","' +                              // GENDER
      Copy(FColumns[19], 2, 5) + '","' +                        // PAY_RATE
      Trim(FColumns[15]) + '","' +                              // BIRTH_DATE
      MapFedMaritalStatus(Trim(FColumns[21])) + '","' +         // FEDERAL_MARITAL_STATUS
      Trim(FColumns[12]) + '","' +                              // EE STATE
      MapStateMaritalStatus(Trim(FColumns[12])) + '","' +       // STATE_MARITAL_STATUS
      Trim(FColumns[5]) + '"';                                  // CURRENT_HIRE_DATE

    FImportFile.Add(line);
    FSSNList.Add(lssn);
  end;  
end;

procedure TNewHireImportFileProc.InsertFileHeader;
begin
  FImportFile.Insert(0, 'CUSTOM_COMPANY_NUMBER,SSN,LAST_NAME,FIRST_NAME,MIDDLE_INITIAL,EE_CODE,DIVISION,BRANCH,DEPARTMENT,ADDRESS,CITY,STATE,ZIP_CODE,GENDER,PAY_RATE,BIRTH_DATE,FEDERAL_MARITAL_STATUS,EE_STATE,STATE_MARITAL_STATUS,CURRENT_HIRE_DATE');
end;

procedure TNewHireImportFileProc.GetFileNames(var fNames: TStrings);
var
  dat_b, dat_e: TDatetime;
begin
  if Assigned(fNames) then
  begin
    GetDateRangeSettingsDates(FConf, CompanyUniquePath(FEvoCo), dat_b, dat_e);
    findFilesForPeriod(fNames, dat_b, dat_e);
  end;
end;

function TNewHireImportFileProc.GetNextEmployeeCode(const aSSN: string): string;
begin
  Result := '';
  if FEvoEECodes.Locate('SSN', GetEvoSSN(aSSN), []) then
    Result := FEvoEECodes.FieldByName('Custom_Employee_Number').AsString
  else begin
    if FMaxEeCodeInt > 0 then
      Result := RightStr( IntToStr(FMaxEeCodeInt + FEeNumber), 9 )
    else
      Result := RightStr( FMaxEeCode + IntToStr(FEeNumber), 9 );
  end;   
end;

function TNewHireImportFileProc.MapFedMaritalStatus(
  const aValue: string): string;
begin
  Result := aValue;
  if (Result <> 'S') and (Result <> 'M') then
    Result := 'M';
end;

function TNewHireImportFileProc.MapStateMaritalStatus(
  const aValue: string): string;
begin
  if aValue = 'AZ' then
    Result := '01'
  else
    Result := 'M';  
end;

function TNewHireImportFileProc.ValidRecordType(
  const aRecType: string): boolean;
begin
  Result := UpperCase(aRecType) = 'HI1';
end;

constructor TNewHireImportFileProc.Create(EvoCo: TEvoCompanyDef;
  logger: ICommonLogger; conf: IisSettings; EvoEECodes: TkbmCustomMemTable;
  const aMaxEeCode: string);
begin
  FSSNList := TStringList.Create;
  inherited Create(EvoCo, logger, conf, EvoEECodes);
  FMaxEeCode := aMaxEeCode;
end;

destructor TNewHireImportFileProc.Destroy;
begin
  FreeAndNil(FSSNList);

  inherited;
end;

{ TTimeClockImportFileProc }

function TImportFileProc.GetEeCodeBySSN(const aSSN: string): string;
begin
  Result := '';
  if FEvoEECodes.Locate('SSN', aSSN, []) then
    Result := FEvoEECodes.FieldByName('Custom_Employee_Number').AsString
  else
    raise Exception.CreateFmt('Employee with SSN: %s was not found', [aSSN]);
end;

procedure TTimeClockImportFileProc.DoProcessRecord;
var
  EeCode, Dv, Br, Dp, Tm: string;

  procedure AddHours(const aHours: string; aFieldType: THoursFieldType);
  var
    EdCode: string;
    h: Double;
  begin
    if TryStrToFloat(aHours, h) then
      if h > 0.01 then
      begin
        EdCode := VarToStr( FEDCodeMatcher.RightMatch(aFieldType) );
        if EDCode <> '' then
          AddTCLine(EeCode, Dv, Br, Dp, Tm, aHours, EdCode)
        else
          raise Exception.CreateFmt('TacoBell field ''%s'' isn''t mapped to an Evolution Client E/D Code.',[GetHoursFieldName(aFieldType)]);
      end;
  end;

  function GetColumnByIndex(aIdx: integer): string;
  begin
    if FColumns.Count >= (aIdx + 1) then
      Result := FColumns[aIdx]
    else
      Result := '0';
  end;
begin
  Assert(FColumns.Count >= 6, 'Hours records (TM1) should contain 20 columns');

  // prepare data for RunEvoXImport
  EeCode := GetEeCodeBySSN( GetEvoSSN(FColumns[5]) );
  Dv := FEvoEECodes.FieldByName('Division').AsString;
  Br := FEvoEECodes.FieldByName('Branch').AsString;
  Dp := FEvoEECodes.FieldByName('Department').AsString;
  Tm := FEvoEECodes.FieldByName('Team').AsString;

  AddHours(GetColumnByIndex(9), eh1RegularHours);
  AddHours(GetColumnByIndex(10), eh1StudentHours);
  AddHours(GetColumnByIndex(11), eh1OvertimeHours);
  AddHours(GetColumnByIndex(12), eh1VacationHours);
  AddHours(GetColumnByIndex(16), eh1CanadaSickHours);
  AddHours(GetColumnByIndex(17), eh1CanadaHolidayHours);
  AddHours(GetColumnByIndex(18), eh1DoubletimeHours);
end;

procedure TTimeClockImportFileProc.GetFileNames(var fNames: TStrings);
begin
  if Assigned(fNames) then
    findFilesForPeriod(fNames, FDat_b, FDat_e);
end;

function TTimeClockImportFileProc.ValidRecordType(
  const aRecType: string): boolean;
begin
  Result := (UpperCase(aRecType) = 'TM1');
end;

constructor TTimeClockImportFileProc.Create(EvoCo: TEvoCompanyDef;
  logger: ICommonLogger; conf: IisSettings; EvoEECodes: TkbmCustomMemTable; dat_b, dat_e: TDatetime;
  EDCodeMatcher: IMatcher);
begin
  inherited Create(EvoCo, logger, conf, EvoEECodes);
  FDat_b := dat_b;
  FDat_e := dat_e;
  FEDCodeMatcher := EDCodeMatcher;
end;

procedure TTimeClockImportFileProc.AddTCLine(const aEeCode, Dv, Br, Dp, Tm: string; const aHours,
  aEDCode: string);
var
  line: string;
begin
  line := '';

  // prepare data for RunEvoXImport
  line := '"' + aEeCode + '"' +                        // CUSTOM_EMPLOYEE_NUMBER
    ',""' +                                            // FirstName  LastName
    ',"' + Dp + '"' +                                  // Department
    ',""' +                                            // Job
    ',""' +                                            // Shift
    ',"' + Copy(aEDCode, 1, 1) + '"' +                 // ED_CODE (1st char)
    ',"' + Copy(aEDCode, 2, 10) + '"' +                // ED_CODE (other part)
    ',""' +                                            // Rate
    ',"' + aHours + '"' +                              // Hours
    ',""' +                                            // YY Punch Date
    ',""' +                                            // MM Punch Date
    ',""' +                                            // DD Punch Date
    ',""' +                                            // HH
    ',""' +                                            // MM
    ',""' +                                            // Amount
    ',""' +                                            // Seq Number
    ',"' + Dv + '"' +                                  // Division
    ',"' + Br + '"' +                                  // Branch
    ',""' +                                            // State
    ',""' +                                            // Local
    ',""' +                                            // Blank Fill
    ',""' +                                            // Deduction 3 Hours
    ',""' +                                            // Deduction 3 Amount
    ',""' +                                            // SSN
    ',""' +                                            // WC Code
    ',""' +                                            // Rate Number
    ',""' +                                            // Punch In
    ',""' +                                            // Punch Out
    ',"' + Tm + '"' +                                  // Team
    ',""';                                             // Comment

  FImportFile.Add(line);
end;

end.
