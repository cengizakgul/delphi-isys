unit TacoBellTasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, common, TacoBellActions,
  evodata, classes, scheduledCustomTask, timeclockimport;

type
  TTacoBellTaskAdapter = class(TTaskAdapterCustomBase)
  protected
    function GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData; override;
  published
    procedure EEImport_Execute;
    function EEImport_Describe: string;
    function TCImport_Describe: string;
  end;


function NewEEImportTask(ClCo: TEvoCompanyDef; const DateRange: string; dat_b, dat_e: TDatetime): IScheduledTask;
function NewTCImportTask(ClCo: TEvoCompanyDef): IScheduledTask;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection, TacoBellCommon,
  evoapiconnectionutils, gdyclasses, gdyCommon, DateRangeDialog, Dialogs,
  gdyDialogEngine, controls, kbmMemTable;


function NewEEImportTask(ClCo: TEvoCompanyDef; const DateRange: string; dat_b, dat_e: TDatetime): IScheduledTask;
begin
  Result := NewTask('EEImport', 'New Hires Import', ClCo);
  SaveDateRangeSettings(DateRange, dat_b, dat_e, Result.ParamSettings, '');
end;

function NewTCImportTask(ClCo: TEvoCompanyDef): IScheduledTask;
begin
  Result := NewTask('TCImport', 'Timeclock Data Import', ClCo);
end;

procedure EditEEImportTask(task: IScheduledTask; Owner: TComponent);
var
  dlg: TDateRangeDlg;
begin
  dlg := TDateRangeDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.DateRange.AsString := LoadDateRangeSettings(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveDateRangeSettings( dlg.DateRange.AsString, dlg.DateRange.Dat_b, dlg.DateRange.Dat_e, task.ParamSettings, '' );
  finally
    FreeAndNil(dlg);
  end;
end;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);
begin
  if task.Name = 'TCImport' then
    ShowMessage('TC Import task has no parameters')
  else if task.Name = 'EEImport' then
    EditEEImportTask(task, Owner)
  else
    Assert(false);
end;

{ TTacoBellTaskAdapter }

function TTacoBellTaskAdapter.TCImport_Describe: string;
begin
  Result := 'Timeclock data import';
end;

function TTacoBellTaskAdapter.GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData;
var
  EDCodeMatcher: IMatcher;
begin
  EDCodeMatcher := CreateMatcher( EdCodeBinding, LoadEDCodeMapping(FSettings, CompanyUniquePath( FTask.Company )) );

  Result := TacoBellActions.GetTimeClockData(batch.Company, batch.PayPeriod, EDCodeMatcher, GetEvoAPICOnnection, FLogger, GetSettings);
end;

function TTacoBellTaskAdapter.EEImport_Describe: string;
begin
  Result := LoadDateRangeSettings(FTask.ParamSettings, '');
end;

procedure TTacoBellTaskAdapter.EEImport_Execute;
begin
  DoEmployeeImport(FTask.Company, GetEvoAPICOnnection, FLogger, GetSettings, nil );
end;

end.
