unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoTCImportMainNewForm, ActnList, EvolutionCompanySelectorFrame,
  StdCtrls, Buttons, ComCtrls, EvolutionPrPrBatchFrame, ExtCtrls,
  EvoAPIConnectionParamFrame, common,
  timeclockimport, evoapiconnectionutils, evodata,
  OptionsBaseFrame,
  SchedulerFrame, scheduledTask, SmtpConfigFrame, PlannedActionConfirmationForm,
  DirListFrm, DateRangeFrame, CustomBinderBaseFrame, EDCodeFrame;

type
  TMainFm = class(TEvoTCImportMainNewFm)
    BitBtn1: TBitBtn;
    tbshEEExport: TTabSheet;
    Panel2: TPanel;
    BitBtn5: TBitBtn;
    tbshScheduler: TTabSheet;
    tbshSchedulerSettings: TTabSheet;
    SchedulerFrame: TSchedulerFrm;
    SmtpConfigFrame: TSmtpConfigFrm;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    lblGlueStatus: TLabel;
    gbImportPeriod: TGroupBox;
    DateRangeFrm: TfrmDateRange;
    pnlCoSettingsLeft: TPanel;
    DirListFrame: TDirListFrame;
    gbEdMapping: TGroupBox;
    EDCodeFrm: TEDCodeFrm;
    splLeft: TSplitter;
    procedure RunActionExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunEmployeeExportUpdate(Sender: TObject);
    procedure RunEmployeeExportExecute(Sender: TObject);

    procedure actScheduleEEExportExecute(Sender: TObject);
    procedure actScheduleTCImportUpdate(Sender: TObject);
    procedure actScheduleTCImportExecute(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleCanEditParameters(var can: boolean);

    procedure HandleDirListChangedByUser(Sender: TObject);
    procedure HandleDateRangeChangedByUser(Sender: TObject);

    procedure UnInitPerCompanySettings(const OldValue: TEvoCompanyDef);
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);

    procedure HandleSmtpConfigChange(Sender: TObject);

    function GetTimeClockData(period: TPayPeriod;
      company: TEvoCompanyDef): TTimeClockImportData;
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  TacoBellCommon, gdyGlobalWaitIndicator, TacoBellActions, TacoBellTasks,
  userActionHelpers, gdyDialogEngine,  gdyMessageDialogFrame,
  EvoAPIClientMainForm, gdyclasses, wwdblook;

{ TMainNewFm }

constructor TMainFm.Create(Owner: TComponent);
const
  CO_DIVISIONDesc: TEvoDSDesc = (Name: 'CO_DIVISION'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
begin
  inherited;
  CompanyFrame.ShowBothNumberAndName;
  FEvoData.AddDS(CO_E_D_CODESDesc);
  FEvoData.AddDS(CO_DIVISIONDesc);

  try
    DirListFrame.Clear(false);
    DateRangeFrm.Clear(false);
  except
    Logger.StopException;
  end;

  //now after initialization attach handlers
  DirListFrame.OnChangeByUser := HandleDirListChangedByUser;
  DateRangeFrm.OnChangeByUser := HandleDateRangeChangedByUser;

  FEvoData.Advise(HandleCompanyChanged);

  try
    SchedulerFrame.Configure(Logger, TTacoBellTaskAdapter.Create, HandleEditParameters);
    SchedulerFrame.OnCanEditParameters := HandleCanEditParameters;
  except
    Logger.StopException;
  end;

  SmtpConfigFrame.Config := LoadSmtpConfig(FSettings, '');
  SmtpConfigFrame.OnChangeByUser := HandleSmtpConfigChange;

  if EvoFrame.IsValid then
    PageControl1.ActivePageIndex := 1
  else
    PageControl1.ActivePageIndex := 0;
end;

destructor TMainFm.Destroy;
begin
  inherited;
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData, FEvoData.Connection, false);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EDCodeFrm.CanCreateMatcher and FEvoData.CanGetPrBatch and (DirListFrame.lblDirList.Items.Count > 0);
end;

type
  TAskResult = record
    Ok: boolean;
    NeverAskAgain: boolean;
  end;

function Ask(Owner: TComponent; msg: string): TAskResult;
var
  dlg: TMessageDialog;
begin
  dlg := TMessageDialog.Create(Owner);
  try
    dlg.MessageText := msg;
    dlg.Caption := Application.Title;
    with DialogEngine( dlg, Owner ) do
    begin
      NeverShowAgainOption := true;
      Result.Ok := ShowModal = mrOk;
      Result.NeverAskAgain := NeverShowAgain;
    end
  finally
    FreeAndNil(dlg);
  end;
end;

procedure TMainFm.RunEmployeeExportExecute(Sender: TObject);
begin
  HandleDateRangeChangedByUser(Sender);
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      DoEmployeeImport( FEvoData.GetClCo, FEvoData.Connection, Logger, FSettings, Self );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunEmployeeExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (DirListFrame.lblDirList.Items.Count > 0) and FEvoData.CanGetClCo;
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UnInitPerCompanySettings(old.Value);
  Repaint;
  if new.HasValue then
  begin
    LoadPerCompanySettings(new.Value);
    Repaint;
  end
  else
    EDCodeFrm.UnInit;
end;

procedure TMainFm.UnInitPerCompanySettings(const OldValue: TEvoCompanyDef);
begin
  try
    DirListFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    DateRangeFrm.Clear(false);
  except
    Logger.StopException;
  end;
  try
    if EDCodeFrm.CanCreateMatcher then
      SaveEDCodeMapping(EDCodeFrm.SaveToString, FSettings, CompanyUniquePath(OldValue));
  except
    Logger.StopException;
  end;
end;


procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  try
    DirListFrame.Init( FEvoData.DS['CO_DIVISION'], LoadDirListSettings(FSettings, CompanyUniquePath(Value)) );
  except
    Logger.StopException;
  end;

  try
    DateRangeFrm.Clear(True);
    DateRangeFrm.AsString := LoadDateRangeSettings(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;

  EDCodeFrm.Init( LoadEDCodeMapping(FSettings, CompanyUniquePath(Value)), FEvoData.DS[CO_E_D_CODESDesc.Name] );

  {  if SwipeClockFrame.IsValid then
  begin
    try
      ctx := GetTCImportOptionsContext(SwipeClockFrame.Param, logger);
      FSiteName := ctx.SiteName;
      TCImportOptionsFrame.Clear(true);
      case ctx.DatabaseType of
        dbClassic: lblGlueStatus.Caption := 'SwipeClock Classic';
        dbGlue: lblGlueStatus.Caption := 'SwipeClock Glue';
      else
        Assert(false);
      end;
      TCImportOptionsFrame.Init( LoadTCImportOptions(FSettings, CompanyUniquePath(FEvoData.GetClCo), ctx.DatabaseType), ctx, FEvoData.CloneCompanyTable(Value, 'CO_E_D_CODES'){transfers ownership!} {);}
{    except
      on E: Exception do
      begin
        TCImportOptionsFrame.Clear(false);
        lblGlueStatus.Caption := '';
        Logger.StopException;
        Application.HandleException(E);
      end
    end;
  end
  else
    PageControl1.ActivePage := tbshCompanySettings;}
end;

procedure TMainFm.HandleSmtpConfigChange(Sender: TObject);
begin
  SaveSmtpConfig(SmtpConfigFrame.Config, FSettings, '');
end;

procedure TMainFm.actScheduleEEExportExecute(Sender: TObject);
begin
  HandleDateRangeChangedByUser(Sender);
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewEEImportTask(FEvoData.GetClCo, DateRangeFrm.AsString, DateRangeFrm.Dat_b, DateRangeFrm.Dat_e) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleTCImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo;
end;

procedure TMainFm.actScheduleTCImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      EvoFrame.ForceSavePassword;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTCImportTask(FEvoData.GetClCo) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.TabIndex = 3) then
  begin
    UnInitPerCompanySettings(FEvoData.GetClCo);
    if FEvoData.CanGetClCo then
      LoadPerCompanySettings(FEvoData.GetClCo);
  end;
end;

procedure TMainFm.HandleDateRangeChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveDateRangeSettings( DateRangeFrm.AsString, DateRangeFrm.Dat_b, DateRangeFrm.Dat_e, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleDirListChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveDirListSettings( DirListFrame.AsString, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

function TMainFm.GetTimeClockData(period: TPayPeriod;
  company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result := TacoBellActions.GetTimeClockData(company, period, EDCodeFrm.Matcher, FEvoData.Connection, Logger, FSettings );
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  TacoBellTasks.EditTask(task, Logger, Self);
end;

procedure TMainFm.HandleCanEditParameters(var can: boolean);
begin
  can := FEvoData.CanGetClCo;
end;

procedure TMainFm.FormShow(Sender: TObject);
begin
  inherited;
  DirListFrame.Width := MainFm.Width div 2;
  DirListFrame.gbDirList.Align := alClient;
  DirListFrame.lblDirList.Height := DirListFrame.gbDirList.Height - 142;
end;

end.
