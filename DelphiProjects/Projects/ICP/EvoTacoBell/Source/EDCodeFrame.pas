unit EDCodeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame;

type
  TEDCodeFrm = class(TCustomBinderBaseFrm)
    cdTacoFieldNames: TClientDataSet;
    cdTacoFieldNamesCODE: TIntegerField;
    cdTacoFieldNamesDESCRIPTION: TStringField;
  private
    procedure InitTacoFieldNames;
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(matchTableContent: string; CO_E_D_CODES: TDataSet);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, TacoBellCommon;

{ TEeStatusFrm }

constructor TEDCodeFrm.Create(Owner: TComponent);
begin
  inherited;
  InitTacoFieldNames;
end;

procedure TEDCodeFrm.Init(matchTableContent: string; CO_E_D_CODES: TDataSet);
begin
  FBindingKeeper := CreateBindingKeeper( EdCodeBinding, TClientDataSet );
  CO_E_D_CODES.FieldByName('CUSTOM_E_D_CODE_NUMBER').DisplayLabel := 'E/D Code';
  CO_E_D_CODES.FieldByName('DESCRIPTION').DisplayLabel := 'Description';
  FBindingKeeper.SetTables(cdTacoFieldNames, CO_E_D_CODES);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TEDCodeFrm.InitTacoFieldNames;
  procedure AddRec(fieldType: THoursFieldType);
  begin
    Append(cdTacoFieldNames, 'CODE;DESCRIPTION', [fieldType, GetHoursFieldName(fieldType)]);
  end;
begin
  CreateOrEmptyDataSet( cdTacoFieldNames );
  AddRec(eh1RegularHours);
  AddRec(eh1StudentHours);
  AddRec(eh1OvertimeHours);
  AddRec(eh1VacationHours);
  AddRec(eh1CanadaSickHours);
  AddRec(eh1CanadaHolidayHours);
  AddRec(eh1DoubletimeHours);
end;

end.



