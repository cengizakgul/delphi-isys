program EvoTacoBell;

uses
 {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  common,
  MidasLib,
  scheduledTask,
  gdyRedir,
  jclfileutils,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  timeclockimport in '..\..\common\timeclockimport.pas',
  evoapiconnectionutils in '..\..\common\evoapiconnectionutils.pas',
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  tcconnectionbase in '..\..\common\tcconnectionbase.pas',
  multipleChoiceFrame in '..\..\common\multipleChoiceFrame.pas' {MultipleChoiceFrm: TFrame},
  gdyDialogHolderBaseForm in '..\..\common\gdycommon\dialogs\gdyDialogHolderBaseForm.pas' {DialogHolderBase},
  gdyDialogHolderForm in '..\..\common\gdycommon\dialogs\gdyDialogHolderForm.pas' {DialogHolder},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  gdyMessageDialogFrame in '..\..\common\gdycommon\dialogs\gdyMessageDialogFrame.pas' {MessageDialog: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvoTCImportMainNewForm in '..\..\common\EvoTCImportMainNewForm.pas' {EvoTCImportMainNewFm},
  MainForm in 'MainForm.pas' {MainFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  scheduledCustomTask in '..\..\common\scheduledCustomTask.pas',
  PlannedActions in '..\..\common\PlannedActions.pas',
  PlannedActionConfirmationForm in '..\..\common\PlannedActionConfirmationForm.pas' {PlannedActionConfirmationFm},
  CodesFilterFrame in '..\..\common\CodesFilterFrame.pas' {CodesFilterFrm: TFrame},
  DBDTFilterFrame in '..\..\common\DBDTFilterFrame.pas' {DBDTFilterFrm: TFrame},
  DirListFrm in 'DirListFrm.pas' {DirListFrame: TFrame},
  TacoBellCommon in 'TacoBellCommon.pas',
  DateRangeFrame in 'DateRangeFrame.pas' {frmDateRange: TFrame},
  ImportFileProcessing in 'ImportFileProcessing.pas',
  TacoBellTasks in 'TacoBellTasks.pas',
  TacoBellActions in 'TacoBellActions.pas',
  DateRangeDialog in 'DateRangeDialog.pas' {DateRangeDlg: TFrame},
  BinderFrame in '..\..\Common\BinderFrame.pas' {BinderFrm: TFrame},
  CustomBinderBaseFrame in '..\..\Common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  EDCodeFrame in 'EDCodeFrame.pas' {EDCodeFrm: TFrame};

{$R *.res}
                                                                                            
begin
//{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  LicenseKey := 'E8E883E1380E44F296DD94CA00E92306'; //EvoTacoBell
//  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution TacoBell Import';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
    Application.Run;
  end
  else
  begin
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TTacoBellTaskAdapter.Create));
  end
end.
