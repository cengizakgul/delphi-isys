inherited frmDateRange: TfrmDateRange
  Width = 407
  Height = 62
  object lblFrom: TLabel
    Left = 2
    Top = 42
    Width = 23
    Height = 13
    Caption = 'From'
  end
  object lblTo: TLabel
    Left = 216
    Top = 42
    Width = 13
    Height = 13
    Caption = 'To'
  end
  object lblPastDays: TLabel
    Left = 184
    Top = 16
    Width = 73
    Height = 13
    Caption = 'Amount of days'
    Visible = False
  end
  object edDatB: TDateTimePicker
    Left = 34
    Top = 38
    Width = 86
    Height = 21
    Date = 40831.993553240740000000
    Time = 40831.993553240740000000
    TabOrder = 0
  end
  object edDatE: TDateTimePicker
    Left = 233
    Top = 38
    Width = 86
    Height = 21
    Date = 40831.993588865740000000
    Time = 40831.993588865740000000
    TabOrder = 2
  end
  object edTimeB: TDateTimePicker
    Left = 120
    Top = 38
    Width = 87
    Height = 21
    Date = 40831.995126770830000000
    Time = 40831.995126770830000000
    Kind = dtkTime
    TabOrder = 1
  end
  object edTimeE: TDateTimePicker
    Left = 319
    Top = 38
    Width = 87
    Height = 21
    Date = 40831.995126770830000000
    Time = 40831.995126770830000000
    Kind = dtkTime
    TabOrder = 3
  end
  object cbPeriod: TComboBox
    Left = 8
    Top = 8
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    OnChange = cbPeriodChange
    Items.Strings = (
      'Custom Period'
      'The past ## days'
      'The Current Week'
      'The Last Week'
      'The Current Month'
      'The Last Month'
      'The Current Year')
  end
  object sePastDays: TSpinEdit
    Left = 271
    Top = 10
    Width = 46
    Height = 22
    MaxValue = 365
    MinValue = 1
    TabOrder = 5
    Value = 1
    Visible = False
    OnChange = sePastDaysChange
  end
end
