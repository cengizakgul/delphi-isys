inherited DirListFrame: TDirListFrame
  Width = 576
  Height = 315
  object gbDirList: TGroupBox
    Left = 0
    Top = 0
    Width = 576
    Height = 315
    Align = alClient
    Caption = 'Data Directories List'
    TabOrder = 0
    DesignSize = (
      576
      315)
    object lblDir: TLabel
      Left = 14
      Top = 27
      Width = 68
      Height = 13
      Caption = 'Data Directory'
    end
    object lblDivision: TLabel
      Left = 372
      Top = 26
      Width = 105
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Evo Division (optional)'
    end
    object sbtnSelectDir: TSpeedButton
      Left = 333
      Top = 42
      Width = 21
      Height = 22
      Action = OpenDir
      Anchors = [akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object bvlTopLine: TBevel
      Left = 14
      Top = 72
      Width = 549
      Height = 44
      Anchors = [akLeft, akTop, akRight]
      Shape = bsFrame
    end
    object edDataDirectory: TEdit
      Left = 14
      Top = 43
      Width = 317
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ReadOnly = True
      TabOrder = 0
    end
    object cmbDivision: TComboBox
      Left = 372
      Top = 42
      Width = 193
      Height = 21
      Style = csDropDownList
      Anchors = [akTop, akRight]
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = '-not selected-'
      Items.Strings = (
        '-not selected-')
    end
    object lblDirList: TListBox
      Left = 14
      Top = 127
      Width = 550
      Height = 173
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 2
    end
    object btnAdd: TBitBtn
      Left = 390
      Top = 81
      Width = 76
      Height = 25
      Action = AddDir
      Caption = 'Add'
      TabOrder = 3
    end
    object btnRemove: TBitBtn
      Left = 473
      Top = 81
      Width = 75
      Height = 25
      Action = RemoveDir
      Caption = 'Remove'
      TabOrder = 4
    end
  end
  object Actions: TActionList
    Left = 118
    Top = 10
    object OpenDir: TAction
      Caption = '...'
      OnExecute = OpenDirExecute
    end
    object AddDir: TAction
      Caption = 'Add'
      OnExecute = AddDirExecute
      OnUpdate = AddDirUpdate
    end
    object RemoveDir: TAction
      Caption = 'Remove'
      OnExecute = RemoveDirExecute
      OnUpdate = RemoveDirUpdate
    end
  end
end
