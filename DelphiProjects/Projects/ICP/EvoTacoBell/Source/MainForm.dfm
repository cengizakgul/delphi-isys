inherited MainFm: TMainFm
  Left = 252
  Top = 135
  Width = 1002
  Caption = 'MainFm'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 986
    ActivePage = tbshCompanySettings
    OnChange = PageControl1Change
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 1029
        inherited GroupBox1: TGroupBox
          Width = 1029
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      object splLeft: TSplitter
        Left = 441
        Top = 0
        Width = 4
        Height = 424
      end
      object pnlCoSettingsLeft: TPanel
        Left = 0
        Top = 0
        Width = 441
        Height = 424
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        inline DirListFrame: TDirListFrame
          Left = 0
          Top = 0
          Width = 441
          Height = 424
          Align = alClient
          TabOrder = 0
          inherited gbDirList: TGroupBox
            Width = 441
            Height = 424
            inherited lblDivision: TLabel
              Left = 234
            end
            inherited sbtnSelectDir: TSpeedButton
              Left = 199
            end
            inherited bvlTopLine: TBevel
              Width = 413
            end
            inherited edDataDirectory: TEdit
              Width = 182
            end
            inherited cmbDivision: TComboBox
              Left = 234
            end
            inherited lblDirList: TListBox
              Width = 413
              Height = 282
            end
            inherited btnAdd: TBitBtn
              Left = 251
              Top = 82
              Anchors = [akTop, akRight]
            end
            inherited btnRemove: TBitBtn
              Left = 334
              Top = 82
              Anchors = [akTop, akRight]
            end
          end
        end
      end
      object gbEdMapping: TGroupBox
        Left = 445
        Top = 0
        Width = 533
        Height = 424
        Align = alClient
        Caption = 'Taco Hours to Evo E/D codes mapping'
        TabOrder = 1
        inline EDCodeFrm: TEDCodeFrm
          Left = 2
          Top = 15
          Width = 529
          Height = 407
          Align = alClient
          TabOrder = 0
          inherited BinderFrm1: TBinderFrm
            Width = 529
            Height = 407
            inherited evSplitter2: TSplitter
              Top = 202
              Width = 529
            end
            inherited pnltop: TPanel
              Width = 529
              Height = 202
              inherited evSplitter1: TSplitter
                Left = 230
                Height = 202
              end
              inherited pnlTopLeft: TPanel
                Width = 230
                Height = 202
                inherited evPanel3: TPanel
                  Width = 230
                end
                inherited dgLeft: TReDBGrid
                  Width = 230
                  Height = 177
                end
              end
              inherited pnlTopRight: TPanel
                Left = 235
                Width = 294
                Height = 202
                inherited evPanel4: TPanel
                  Width = 294
                end
                inherited dgRight: TReDBGrid
                  Width = 294
                  Height = 177
                end
              end
            end
            inherited evPanel1: TPanel
              Top = 207
              Width = 529
              Height = 200
              inherited pnlbottom: TPanel
                Width = 529
                Height = 159
                inherited evPanel5: TPanel
                  Width = 529
                  Height = 23
                end
                inherited dgBottom: TReDBGrid
                  Top = 23
                  Width = 529
                  Height = 136
                end
              end
              inherited pnlMiddle: TPanel
                Width = 529
              end
            end
          end
        end
      end
    end
    object tbshEEExport: TTabSheet [2]
      Caption = 'Employee import'
      ImageIndex = 4
      object Panel2: TPanel
        Left = 0
        Top = 121
        Width = 978
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn5: TBitBtn
          Left = 17
          Top = 17
          Width = 137
          Height = 25
          Action = RunEmployeeExport
          Caption = 'Run New Hires Import'
          TabOrder = 0
        end
        object BitBtn2: TBitBtn
          Left = 166
          Top = 17
          Width = 89
          Height = 25
          Action = actScheduleEEExport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      object gbImportPeriod: TGroupBox
        Left = 0
        Top = 0
        Width = 978
        Height = 121
        Align = alTop
        Caption = 'Import New Hires For'
        TabOrder = 1
        inline DateRangeFrm: TfrmDateRange
          Left = 2
          Top = 15
          Width = 974
          Height = 104
          Align = alClient
          TabOrder = 0
          inherited lblFrom: TLabel
            Left = 15
            Top = 39
          end
          inherited lblTo: TLabel
            Left = 160
            Top = 39
          end
          inherited lblPastDays: TLabel
            Left = 308
            Top = 12
          end
          inherited edDatB: TDateTimePicker
            Left = 15
            Top = 55
            Width = 135
          end
          inherited edDatE: TDateTimePicker
            Left = 160
            Top = 55
            Width = 135
          end
          inherited edTimeB: TDateTimePicker
            Left = 488
            Top = 46
            Visible = False
          end
          inherited edTimeE: TDateTimePicker
            Left = 583
            Top = 46
            Visible = False
          end
          inherited cbPeriod: TComboBox
            Left = 15
            Top = 9
            Width = 280
          end
          inherited sePastDays: TSpinEdit
            Left = 387
            Top = 8
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Timeclock data import'
      inherited pnlBottom: TPanel
        Top = 386
        Width = 978
        Height = 38
        object lblGlueStatus: TLabel
          Left = 240
          Top = 16
          Width = 5
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 97
          Height = 25
          Action = RunAction
          Caption = 'Run import'
          TabOrder = 0
        end
        object BitBtn3: TBitBtn
          Left = 120
          Top = 8
          Width = 97
          Height = 25
          Action = actScheduleTCImport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      inherited pnlPayrollBatch: TPanel
        Width = 978
        Height = 386
        inherited PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Width = 978
          Height = 386
          inherited Splitter1: TSplitter
            Height = 386
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 386
            inherited dgGrid: TReDBGrid
              Height = 361
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 659
            Height = 386
            inherited Panel1: TPanel
              Width = 659
            end
            inherited dgGrid: TReDBGrid
              Width = 659
              Height = 361
            end
          end
        end
      end
    end
    object tbshScheduler: TTabSheet [4]
      Caption = 'Scheduled tasks'
      ImageIndex = 5
      inline SchedulerFrame: TSchedulerFrm
        Left = 0
        Top = 0
        Width = 978
        Height = 424
        Align = alClient
        TabOrder = 0
        inherited pnlTasksControl: TPanel
          Width = 978
        end
        inherited PageControl2: TPageControl
          Width = 978
          Height = 383
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 129
              Width = 970
            end
            inherited pnlTasks: TPanel
              Width = 970
              Height = 129
              inherited dgTasks: TReDBGrid
                Width = 1004
                Height = 88
              end
              inherited Panel1: TPanel
                Top = 88
                Width = 1004
              end
            end
            inherited pnlDetails: TPanel
              Top = 136
              Width = 970
              inherited pcTaskDetails: TPageControl
                Width = 970
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 829
                  end
                  inherited dgResults: TReDBGrid
                    Width = 829
                  end
                end
              end
            end
          end
        end
      end
    end
    object tbshSchedulerSettings: TTabSheet [5]
      Caption = 'Scheduler settings'
      ImageIndex = 6
      inline SmtpConfigFrame: TSmtpConfigFrm
        Left = 0
        Top = 0
        Width = 1060
        Height = 180
        Align = alTop
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 1060
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Width = 986
  end
  inherited pnlCompany: TPanel
    Width = 986
    inherited Panel1: TPanel
      Width = 105
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 105
      Width = 881
    end
  end
  inherited ActionList1: TActionList
    Left = 100
    Top = 8
    inherited RunAction: TAction
      Caption = 'Run import'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    inherited RunEmployeeExport: TAction
      Caption = 'Run New Hires Import'
      OnExecute = RunEmployeeExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
    object actScheduleTCImport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleTCImportExecute
      OnUpdate = actScheduleTCImportUpdate
    end
    object actScheduleEEExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEEExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
  end
end
