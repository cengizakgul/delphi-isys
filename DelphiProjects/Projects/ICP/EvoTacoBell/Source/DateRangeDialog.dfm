inherited DateRangeDlg: TDateRangeDlg
  Width = 444
  Height = 122
  object gbImportPeriod: TGroupBox
    Left = 0
    Top = 0
    Width = 444
    Height = 121
    Align = alTop
    Caption = 'Import New Hires For'
    TabOrder = 0
    inline DateRange: TfrmDateRange
      Left = 2
      Top = 15
      Width = 440
      Height = 104
      Align = alClient
      TabOrder = 0
      inherited lblFrom: TLabel
        Left = 15
        Top = 39
      end
      inherited lblTo: TLabel
        Left = 160
        Top = 39
      end
      inherited lblPastDays: TLabel
        Left = 308
        Top = 12
      end
      inherited edDatB: TDateTimePicker
        Left = 15
        Top = 55
        Width = 135
      end
      inherited edDatE: TDateTimePicker
        Left = 160
        Top = 55
        Width = 135
      end
      inherited edTimeB: TDateTimePicker
        Left = 488
        Top = 46
        Visible = False
      end
      inherited edTimeE: TDateTimePicker
        Left = 583
        Top = 46
        Visible = False
      end
      inherited cbPeriod: TComboBox
        Left = 15
        Top = 9
        Width = 280
      end
      inherited sePastDays: TSpinEdit
        Left = 387
        Top = 8
      end
    end
  end
end
