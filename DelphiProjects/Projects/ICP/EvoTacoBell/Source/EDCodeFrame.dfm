inherited EDCodeFrm: TEDCodeFrm
  Width = 598
  Height = 514
  inherited BinderFrm1: TBinderFrm
    Width = 598
    Height = 514
    inherited evSplitter2: TSplitter
      Top = 250
      Width = 598
    end
    inherited pnltop: TPanel
      Width = 598
      Height = 250
      inherited evSplitter1: TSplitter
        Height = 250
      end
      inherited pnlTopLeft: TPanel
        Height = 250
        inherited dgLeft: TReDBGrid
          Height = 225
        end
      end
      inherited pnlTopRight: TPanel
        Width = 328
        Height = 250
        inherited evPanel4: TPanel
          Width = 328
        end
        inherited dgRight: TReDBGrid
          Width = 328
          Height = 225
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 255
      Width = 598
      inherited pnlbottom: TPanel
        Width = 598
        inherited evPanel5: TPanel
          Width = 598
        end
        inherited dgBottom: TReDBGrid
          Width = 598
        end
      end
      inherited pnlMiddle: TPanel
        Width = 598
      end
    end
  end
  object cdTacoFieldNames: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdTacoFieldNamesCODE: TIntegerField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
    end
    object cdTacoFieldNamesDESCRIPTION: TStringField
      DisplayLabel = 'Tacobell Field Name'
      DisplayWidth = 20
      FieldName = 'DESCRIPTION'
      Size = 40
    end
  end
end
