unit TacoBellCommon;

interface

uses
  gdystrset, RateImportOptionFrame, isSettings, gdyCommonLogger, gdyBinder;

const
  sRoot = 'EvoTacoBell';
  sDateRange = 'DateRange';
  sDirList = 'DirList';
  sEDCodeMapping = 'EDCodeMapping\DataPacket';
  sDirDivDelimiter = '; Evo Division:';

  EdCodeBinding: TBindingDesc =
    ( Name: 'EDCodeBinding';
      Caption: 'TacoBell field names mapped to Evolution E/D Codes';
      LeftDesc: ( Caption: 'TacoBell field names'; Unique: true; KeyFields: 'CODE'; ListFields: 'DESCRIPTION');
      RightDesc: ( Caption: 'Evolution company E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION');
      HideKeyFields: true;
    );

type
  THoursFieldType = (
    eh1SSN = 6,
    eh1RegularHours = 10,
    eh1StudentHours = 11,
    eh1OvertimeHours = 12,
    eh1VacationHours = 13,
    eh1StudentRate = 14,
    eh1RegularRate = 15,
    eh1CanadaSickHours = 17,
    eh1CanadaHolidayHours = 18,
    eh1DoubletimeHours = 19);

function GetHoursFieldName(fieldType: THoursFieldType): string;

function LoadDirListSettings(conf: IisSettings; root: string): string;
procedure SaveDirListSettings(const param: string; conf: IisSettings; root: string);

procedure SaveEDCodeMapping(const param: string; conf: IisSettings; root: string);
function LoadEDCodeMapping(conf: IisSettings; root: string): string;

function LoadDateRangeSettings(conf: IisSettings; root: string): string;
procedure GetDateRangeSettingsDates(conf: IisSettings; root: string; var dat_b, dat_e: TDatetime);
procedure SaveDateRangeSettings(const param: string; dat_b, dat_e: TDatetime; conf: IisSettings; root: string);

implementation

uses
  sysutils, gdycommon, gdyClasses, common, gdyCrypt;

const
  cKey='Form.Button';

function GetHoursFieldName(fieldType: THoursFieldType): string;
begin
  case fieldType of
    eh1SSN:                 Result := 'SSN';
    eh1RegularHours:        Result := 'Regular Hours';
    eh1StudentHours:        Result := 'Student Hours';
    eh1OvertimeHours:       Result := 'Overtime Hours';
    eh1VacationHours:       Result := 'Vacation Hours';
    eh1StudentRate:         Result := 'Student Rate';
    eh1RegularRate:         Result := 'Regular Rate';
    eh1CanadaSickHours:     Result := 'Canada Sick Hours';
    eh1CanadaHolidayHours:  Result := 'Canada Holiday Hours';
    eh1DoubletimeHours:     Result := 'Doubletime Hours';
  else
    Assert(false);
  end;
end;

function LoadDirListSettings(conf: IisSettings; root: string): string;
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  Result := conf.AsString[root+sDirList];
end;

function LoadEDCodeMapping(conf: IisSettings; root: string): string;
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  Result := conf.AsString[root+sEDCodeMapping];
end;

procedure SaveDirListSettings(const param: string; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  conf.AsString[root+sDirList] := param;
end;

procedure SaveEDCodeMapping(const param: string; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  conf.AsString[root+sEDCodeMapping] := param;
end;

function LoadDateRangeSettings(conf: IisSettings; root: string): string;
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  Result := conf.AsString[root+sDateRange];
end;

procedure GetDateRangeSettingsDates(conf: IisSettings; root: string; var dat_b, dat_e: TDatetime);
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  dat_b := conf.AsDateTime[root+sDateRange+'_dat_b'];
  dat_e := conf.AsDateTime[root+sDateRange+'_dat_e'];
end;

procedure SaveDateRangeSettings(const param: string; dat_b, dat_e: TDatetime; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + sRoot + '\';
  conf.AsString[root+sDateRange] := param;
  conf.AsDateTime[root+sDateRange+'_dat_b'] := dat_b;
  conf.AsDateTime[root+sDateRange+'_dat_e'] := dat_e;
end;

end.


