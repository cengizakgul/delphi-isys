unit DirListFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Buttons, StdCtrls, ActnList, ExtCtrls, DB, OptionsBaseFrame;

type
  TDirListFrame = class(TOptionsBaseFrm)
    gbDirList: TGroupBox;
    lblDir: TLabel;
    lblDivision: TLabel;
    edDataDirectory: TEdit;
    sbtnSelectDir: TSpeedButton;
    Actions: TActionList;
    cmbDivision: TComboBox;
    lblDirList: TListBox;
    bvlTopLine: TBevel;
    btnAdd: TBitBtn;
    btnRemove: TBitBtn;
    OpenDir: TAction;
    AddDir: TAction;
    RemoveDir: TAction;
    procedure OpenDirExecute(Sender: TObject);
    procedure AddDirExecute(Sender: TObject);
    procedure RemoveDirExecute(Sender: TObject);
    procedure RemoveDirUpdate(Sender: TObject);
    procedure AddDirUpdate(Sender: TObject);
  private
    FRecentDirectory: string;

    procedure SetAsString(const aValue: string);
    function GetAsString: string;

    procedure ClearValues;
  public
    procedure Init(aDivisions: TDataset; const aSettings: string);
    procedure Clear(enable: boolean);

    property AsString: string read GetAsString write SetAsString;
  end;

implementation

uses ShlObj, gdycommon, TacoBellCommon;

{$R *.dfm}

function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam: LPARAM; lpData: LPARAM): Integer; stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) then
    SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);
  BrowseCallbackProc:= 0;
end;

function GetFolderDialog(Handle: Integer; Caption: string; var strFolder: string): Boolean;
const
  BIF_STATUSTEXT           = $0004;
  BIF_NEWDIALOGSTYLE       = $0040;
  BIF_RETURNONLYFSDIRS     = $0080;
  BIF_SHAREABLE            = $0100;
  BIF_USENEWUI             = BIF_EDITBOX or BIF_NEWDIALOGSTYLE;

var
  BrowseInfo: TBrowseInfo;
  ItemIDList: PItemIDList;
  JtemIDList: PItemIDList;
  Path: PAnsiChar;
begin
  Result:= False;
  Path:= StrAlloc(MAX_PATH);
  SHGetSpecialFolderLocation(Handle, CSIDL_DRIVES, JtemIDList);
  with BrowseInfo do
  begin
    hwndOwner:= GetActiveWindow;
    pidlRoot:= JtemIDList;
    SHGetSpecialFolderLocation(hwndOwner, CSIDL_DRIVES, JtemIDList);

    { ������� �������� ���������� �������� }
    pszDisplayName:= StrAlloc(MAX_PATH);

    { ��������� �������� ������� ������ ����� }
    lpszTitle:= PChar(Caption); // '������� ����� �� Delphi (������)';
    { �����, �������������� ������� }
    lpfn:= @BrowseCallbackProc;
    { �������������� ����������, ������� ������� ������� � �������� ����� (callback) }
    lParam:= LongInt(PChar(strFolder));
  end;

  ItemIDList:= SHBrowseForFolder(BrowseInfo);

  if (ItemIDList <> nil) then
    if SHGetPathFromIDList(ItemIDList, Path) then
    begin
      strFolder:= Path;
      Result:= True;
    end;
end;

function TDirListFrame.GetAsString: string;
begin
  Result := lblDirList.Items.Text;
end;

procedure TDirListFrame.Init(aDivisions: TDataset;
  const aSettings: string);
begin
  Clear(True);

  if not aDivisions.Active then
    aDivisions.Open;

  aDivisions.First;
  while not aDivisions.Eof do
  begin
    cmbDivision.Items.Add( Trim(aDivisions.FieldByName('CUSTOM_DiViSION_NUMBER').AsString) );
    aDivisions.Next;
  end;

  AsString := aSettings;
end;

procedure TDirListFrame.OpenDirExecute(Sender: TObject);
begin
  if GetFolderDialog(Application.Handle, 'Select Import Data Directory', FRecentDirectory) then
    edDataDirectory.Text := FRecentDirectory;
end;

procedure TDirListFrame.SetAsString(const aValue: string);
begin
  lblDirList.Items.Text := aValue;
end;

procedure TDirListFrame.AddDirExecute(Sender: TObject);
var
  i, k: integer;
begin
  k := -1;
  for i := 0 to lblDirList.Items.Count - 1 do
    if Pos(sDirDivDelimiter, lblDirList.Items[i]) > 0 then
    begin
      if FRecentDirectory = Copy(lblDirList.Items[i], 1, Pos(sDirDivDelimiter, lblDirList.Items[i]) - 1) then
        k := i;
    end
    else begin
      if FRecentDirectory = lblDirList.Items[i] then
        k := i;
    end;

  if k > -1 then
  begin
    // the directory has already been added
    if MessageDlg('The directory has already been added!' + #10#13 + 'Update Evo Division mapped value?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      if cmbDivision.ItemIndex > 0 then
        lblDirList.Items[k] := FRecentDirectory + sDirDivDelimiter + cmbDivision.Text
      else
        lblDirList.Items[k] := FRecentDirectory;
    end;
  end
  else begin
    if cmbDivision.ItemIndex > 0 then
      lblDirList.Items.Add(FRecentDirectory + sDirDivDelimiter + cmbDivision.Text)
    else
      lblDirList.Items.Add(FRecentDirectory);
  end;

  edDataDirectory.Text := '';
  cmbDivision.ItemIndex := 0;
end;

procedure TDirListFrame.RemoveDirExecute(Sender: TObject);
begin
  if lblDirList.ItemIndex > -1 then
    lblDirList.Items.Delete( lblDirList.ItemIndex ); 
end;

procedure TDirListFrame.RemoveDirUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := lblDirList.ItemIndex > -1;
end;

procedure TDirListFrame.AddDirUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := edDataDirectory.Text <> '';
end;

procedure TDirListFrame.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    ClearValues;
    EnableControlRecursively(Self, enable);
  finally
    FBlockOnChange := false;
  end;
end;

procedure TDirListFrame.ClearValues;
begin
  FRecentDirectory := GetCurrentDir;
  edDataDirectory.Text := '';
  lblDirList.Clear;
  cmbDivision.Clear;
  cmbDivision.Items.Add('-not selected-');
  cmbDivision.ItemIndex := 0;
end;

end.
