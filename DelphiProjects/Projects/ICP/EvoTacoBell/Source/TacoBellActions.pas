unit TacoBellActions;

interface

uses
  classes, gdycommonlogger, isSettings, common, SysUtils, EvoData, ImportFileProcessing,
  evoapiconnection, timeclockimport, gdyBinder;

  procedure DoEmployeeImport(EvoCo: TEvoCompanyDef; EvoAPI: IEvoAPIConnection; logger: ICommonLogger; conf: IisSettings; Owner: TComponent);
  function GetTimeClockData(company: TEvoCompanyDef; period: TPayPeriod; EDMatcher: IMatcher; EvoAPI: IEvoAPIConnection; logger: ICommonLogger; conf: IisSettings): TTimeClockImportData;

implementation

uses
  EvoWaitForm, gdyGlobalWaitIndicator, gdyRedir, gdycommon, evoapiconnectionutils, XmlRpcTypes,
  kbmMemTable, DB, dialogs;

function GetMaxEeCode(EvoAPI: IEvoAPIConnection; EvoCo: TEvoCompanyDef): string;
var
  EeCodes: TkbmCustomMemTable;
  params: IRpcStruct;
begin
  params := TRpcStruct.Create;
  params.AddItem( 'CoNbr', EvoCo.CoNbr );
  try
    EvoAPI.OpenClient( EvoCo.ClNbr );
    EeCodes := EvoAPI.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'EE_NUMBERS.rwq', params);
//    EeCodes.SortFields := 'CUSTOM_EMPLOYEE_NUMBER';
    EeCodes.First;
    Result := Trim(EeCodes.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
  finally
    FreeAndNil( EeCodes );
  end;
end;

function GetTimeClockData(company: TEvoCompanyDef; period: TPayPeriod; EDMatcher: IMatcher; EvoAPI: IEvoAPIConnection; logger: ICommonLogger; conf: IisSettings): TTimeClockImportData;
var
  ImportFileProc: TTimeClockImportFileProc;
  EeCodes: TkbmCustomMemTable;
  params: IRpcStruct;
//  sl: TStrings;
begin
  logger.LogEntry('Getting timeclock data');
  try
    try
      params := TRpcStruct.Create;
      params.AddItem( 'CoNbr', company.CoNbr );
      try
        EvoAPI.OpenClient( company.ClNbr );
        EeCodes := EvoAPI.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'SSN_EE_CODE.rwq', params);
        ImportFileProc := TTimeClockImportFileProc.Create(Company, logger, conf, EeCodes, Period.PeriodBegin, Period.PeriodEnd, EDMatcher);
        try
          Result.Text := ImportFileProc.GetImportData;
{          sl := TStringList.Create;
          try
            sl.LoadFromFile('C:\ICP\Testing\EvoTacoBell\TestImport_11075_pr11-4-2014.txt');
            Result.Text := sl.Text;
          finally
            sl.Free;
          end;}
          Result.APIOptions.LookupOption := tciLookupByNnumber;
          Result.APIOptions.DBDTOption := tciDBDTSmart;
          Result.APIOptions.FourDigitYear := False;
          Result.APIOptions.FileFormat := tciFileFormatCommaDelimited;
          Result.APIOptions.AutoImportJobCodes := True;
          Result.APIOptions.UseEmployeePayRates := True;
        finally
          FreeAndNil(ImportFileProc);
        end;
      finally
        FreeAndNil( EeCodes );
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

procedure DoEmployeeImport(EvoCo: TEvoCompanyDef; EvoAPI: IEvoAPIConnection; logger: ICommonLogger; conf: IisSettings; Owner: TComponent);
var
  ImportFileProc: TNewHireImportFileProc;
  files: TEvoXImportInputFiles;
  res: IEvoXImportResultsWrapper;
  showGUI: boolean;
  EeCodes: TkbmCustomMemTable;
  params: IRpcStruct;
begin
  showGUI := Assigned(Owner);
  Logger.LogEntry('Import New Hires from TacoBell');
  try
    try
      params := TRpcStruct.Create;
      params.AddItem( 'CoNbr', EvoCo.CoNbr );
      SetLength(files, 1);
      files[0].Filename := 'importFile.txt';

      if showGUI then
        WaitIndicator.StartWait('Analyse TacoBell data files');
      try
        EvoAPI.OpenClient( EvoCo.ClNbr );
        EeCodes := EvoAPI.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'SSN_EE_CODE.rwq', params);

        ImportFileProc := TNewHireImportFileProc.Create(EvoCo, logger, conf, EeCodes, GetMaxEeCode(EvoAPI, EvoCo));
        try
          files[0].Filedata := ImportFileProc.GetImportData;
        finally
          FreeAndNil(ImportFileProc);
          FreeAndNil(EeCodes);
        end;

        if files[0].Filedata = '' then
        begin
          Logger.LogWarning('No "New Hires" records found');
          ShowMessage('No "New Hires" records found');
        end
        else begin
          if showGUI then
          begin
            WaitIndicator.EndWait;
            WaitIndicator.StartWait('Import data to Evolution');
          end;

          if showGUI then
            res := CreateGUIEvoXImportExecutor(EvoAPI, Owner, logger).RunEvoXImport(files, FileToString(Redirection.GetFilename(sEvoXMapFileAlias)), 'New Hires')
          else
            res := CreateSilentEvoXImportExecutor(EvoAPI).RunEvoXImport(files, FileToString(Redirection.GetFilename(sEvoXMapFileAlias)), 'New Hires');

          if res <> nil then
          begin
            Assert(res.Count = 1);
            if showGUI then
              ShowMessage(LogEvoXImportResults(Logger, res.Result[0], 'Employee records'))
            else
              LogEvoXImportResults(Logger, res.Result[0], 'Employee records');
          end
          else
            Logger.LogWarningFmt('Employee records import failed', []);
        end;
      finally
        if showGUI then
          WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.
