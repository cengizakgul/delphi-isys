@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoM3.msi 
"%pWiXDir%\candle.exe" .\EvoM3.wxs -wx -out ..\..\..\..\..\Tmp\EvoM3.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoM3.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoM3.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoM3.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoM3%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoM3%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoM3.msi EvoM3%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
