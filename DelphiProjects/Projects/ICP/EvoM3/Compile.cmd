@ECHO OFF
REM Parameters: 
REM    1. Version(optional)
REM    2. Flags: (R)elease or (D)ebug (optional)

SET pAppVersion=%1
IF "%pAppVersion%" == "" GOTO compile

SET pCompileOptions=-DPRODUCTION_LICENCE
IF "%2" == "R" SET pCompileOptions=%pCompileOptions% -DFINAL_RELEASE

:compile

IF NOT EXIST ..\..\..\Bin\ICP\EvoM3 mkdir ..\..\..\Bin\ICP\EvoM3
IF NOT EXIST ..\..\..\Bin\ICP\EvoM3\Queries mkdir ..\..\..\Bin\ICP\EvoM3\Queries
IF NOT EXIST "..\..\..\Bin\ICP\EvoM3\HTML Templates" mkdir "..\..\..\Bin\ICP\EvoM3\HTML Templates"
IF NOT EXIST "..\..\..\Bin\ICP\EvoM3\HTML Templates\Log_files" mkdir "..\..\..\Bin\ICP\EvoM3\HTML Templates\Log_files"

ECHO *Compiling
..\..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion% "%pCompileOptions%"

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
REM XCOPY /Y /Q .\Source\*.mes ..\..\..\Bin\ICP\EvoM3\*.* > NUL
FOR %%a IN (..\..\..\Bin\ICP\EvoM3\*.exe) DO ..\..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoM3\*.exe) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoM3\*.dll) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\EvoM3\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\EvoM3\*.mes
REM FOR %%a IN (..\..\..\Bin\ICP\EvoM3\*.map) DO DEL /F /Q ..\..\..\Bin\ICP\EvoM3\*.map

GOTO end

:error
IF "%pAppVersion%" == "" PAUSE
EXIT 1

:end
IF "%pAppVersion%" == "" PAUSE
