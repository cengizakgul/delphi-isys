inherited OverrideEmailDlg: TOverrideEmailDlg
  Width = 442
  Height = 67
  object gbOverrideEmail: TGroupBox
    Left = 0
    Top = 0
    Width = 442
    Height = 67
    Align = alClient
    TabOrder = 0
    object Label6: TLabel
      Left = 9
      Top = 16
      Width = 56
      Height = 13
      Caption = 'To: address'
    end
    object edAddressTo: TEdit
      Left = 10
      Top = 34
      Width = 423
      Height = 21
      TabOrder = 0
    end
  end
end
