unit Mapping;

interface

uses DB, SysUtils, EvoAPIConnection;

type
  TMapField = record
    Name: string;    // Field Name (corresponds to the column Name on the M3 Import report)
    T: TFieldType;   // Field Type
    S: integer;      // Size
  end;

  // the fields from M3 Import report in the order these appear on the report
  TM3ReportFields = (rfOrganizationCode, rfSiteCode, rfHomeDepartment, rfHomeJob, rfHomeHourlyRate,
    rfEmployeeCode, rfFirstName, rfMiddleInitial, rfGender, rfDateOfBirth, rfClockNo,
    rfDateOfHire, rfEffectiveDate, rfAddress, rfAddress2, rfCity, rfState, rfCountry,
    rfPostalCode, rfEmployeeStatus, rfEmployeeType, rfFullTimePartTime, rfAllocation,
    rfHrlySlryNonExemptFluc1, rfSSN, rfPayGroup, rfReviewDate, rfPhoneNumber, rfTerminationCode,
    rfTerminationDate, rfJobCode2, rfDepartment2, rfJobRate2, rfBranch2, rfJobCode3, rfDepartment3, rfJobRate3,
    rfBranch3, rfJobCode4, rfDepartment4, rfJobRate4, rfBranch4, rfJobCode5, rfDepartment5, rfJobRate5,
    rfBranch5, rfJobCode6, rfDepartment6, rfJobRate6, rfBranch6, rfJobCode7, rfDepartment7, rfJobRate7,
    rfBranch7);

const
  Map: array[Ord(rfOrganizationCode)..Ord(rfBranch7)] of TMapField =
    (
      (Name: 'OrganizationCode'; T: ftString; S: 32), //Organization
      (Name: 'SiteCode'; T: ftString; S: 32), //Home Site
      (Name: 'HomeDepartment'; T: ftString; S: 32), //Home Department
      (Name: 'HomeJob'; T: ftString; S: 32), //Home Job
      (Name: 'HomeHourlyRate'; T: ftCurrency; S: 0), //Home Hourly Rate
      (Name: 'EmployeeCode'; T: ftString; S: 32), //EE Code
      (Name: 'FirstName'; T: ftString; S: 64), //First/Full Name
      (Name: 'MiddleInitial'; T: ftString; S: 1), //Middle Initial
      (Name: 'Gender'; T: ftString; S: 1), //Gender
      (Name: 'DateOfBirth'; T: ftDate; S: 0), //Date of Birth
      (Name: 'ClockNo'; T: ftString; S: 32), //Clock No
      (Name: 'DateOfHire'; T: ftDate; S: 0), //Date of Hire
      (Name: 'EffectiveDate'; T: ftDate; S: 0), //Effective Date
      (Name: 'Address'; T: ftString; S: 128), //Address
      (Name: 'Address2'; T: ftString; S: 128), //Address 2
      (Name: 'City'; T: ftString; S: 32), //City
      (Name: 'State'; T: ftString; S: 2), //State
      (Name: 'Country'; T: ftString; S: 32), //Country
      (Name: 'PostalCode'; T: ftString; S: 32), //Postal Code
      (Name: 'EmployeeStatus'; T: ftString; S: 32), //Employee Status
      (Name: 'EmployeeType'; T: ftString; S: 32), //Employee Type
      (Name: 'FullTimePartTime'; T: ftString; S: 32), //FT/PT
      (Name: 'Allocation'; T: ftString; S: 32), //Allocation
      (Name: 'HrlySlryNonExemptFluc1'; T: ftString; S: 32), //Hourly/Salary/Non-Exempt/Fluctuating
      (Name: 'SSN'; T: ftString; S: 32), //SSN
      (Name: 'PayGroup'; T: ftString; S: 32), //Pay Group
      (Name: 'ReviewDate'; T: ftDate; S: 0), //Review Date
      (Name: 'PhoneNumber'; T: ftString; S: 32), //PhoneNumber
      (Name: 'TerminationCode'; T: ftString; S: 32), //Termination Code
      (Name: 'TerminationDate'; T: ftDate; S: 0), //Termination Date
      (Name: 'JobCode2'; T: ftString; S: 32), //Job Code 2
      (Name: 'Department2'; T: ftString; S: 32), //Department 2
      (Name: 'JobRate2'; T: ftCurrency; S: 0), //Job Rate 2
      (Name: 'Branch2'; T: ftString; S: 32), //SiteCode2
      (Name: 'JobCode3'; T: ftString; S: 32), //Job Code 3
      (Name: 'Department3'; T: ftString; S: 32), //Department 3
      (Name: 'JobRate3'; T: ftCurrency; S: 0), //Job Rate 3
      (Name: 'Branch3'; T: ftString; S: 32), //SiteCode3
      (Name: 'JobCode4'; T: ftString; S: 32), //Job Code 4
      (Name: 'Department4'; T: ftString; S: 32), //Department 4
      (Name: 'JobRate4'; T: ftCurrency; S: 0), //Job Rate 4
      (Name: 'Branch4'; T: ftString; S: 32), //SiteCode4
      (Name: 'JobCode5'; T: ftString; S: 32), //Job Code 5
      (Name: 'Department5'; T: ftString; S: 32), //Department 5
      (Name: 'JobRate5'; T: ftCurrency; S: 0), //Job Rate 5
      (Name: 'Branch5'; T: ftString; S: 32), //SiteCode5
      (Name: 'JobCode6'; T: ftString; S: 32), //Job Code 6
      (Name: 'Department6'; T: ftString; S: 32), //Department 6
      (Name: 'JobRate6'; T: ftCurrency; S: 0), //Job Rate 6
      (Name: 'Branch6'; T: ftString; S: 32), //SiteCode6
      (Name: 'JobCode7'; T: ftString; S: 32), //Job Code 7
      (Name: 'Department7'; T: ftString; S: 32), //Department 7
      (Name: 'JobRate7'; T: ftCurrency; S: 0), //Job Rate 7
      (Name: 'Branch7'; T: ftString; S: 32)  //SiteCode7
    );

implementation

end.
