unit SP_ConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OptionsBaseFrame, StdCtrls, Buttons, PasswordEdit,
  issettings, ActnList, gdyCommonLogger;

type
  TSP_ConnectionParam = record
    URL: string;
    Username: string;
    Password: string;
    SavePassword: boolean;
  end;

  TSP_ConnectionParamFrm = class(TOptionsBaseFrm)
    gbSP_Server: TGroupBox;
    lblAddress: TLabel;
    lblUser: TLabel;
    lblPassword: TLabel;
    edURL: TEdit;
    edSP_Username: TEdit;
    edSP_Password: TPasswordEdit;
    cbSP_SavePassword: TCheckBox;
  private
    function GetParam: TSP_ConnectionParam;
    procedure SetParam(const Value: TSP_ConnectionParam);
  public
    property Param: TSP_ConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
    procedure Check;
    procedure ForceSavePassword;
  end;

procedure SaveSP_ConnectionParam(param: TSP_ConnectionParam; conf: IisSettings; root: string );
function LoadSP_ConnectionParam(conf: IisSettings; root: string): TSP_ConnectionParam;

implementation

{$R *.dfm}

uses
  cswriter, printers, gdyclasses, strutils, gdycommon, gdyRedir, gdyCrypt,
  gdyUtils, gdyGlobalWaitIndicator;

const
  cKey='D1eg0';

procedure SaveSP_ConnectionParam(param: TSP_ConnectionParam; conf: IisSettings; root: string );
begin
  root := root + IIF(root='','','\') + 'SP_Connection\';
  conf.AsString[root+'URL'] := param.URL;
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
end;

function LoadSP_ConnectionParam(conf: IisSettings; root: string): TSP_ConnectionParam;
begin
  root := root + IIF(root='','','\') + 'SP_Connection\';

  Result.URL := conf.AsString[root+'URL'];
  Result.Username := conf.AsString[root+'Username'];
  if conf.GetValueNames(WithoutTrailingSlash(root)).IndexOf('Password') <> -1 then
    Result.Password := conf.AsString[root+'Password']
  else
    Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
end;

{ TSP_ConnectionParamFrm }

procedure TSP_ConnectionParamFrm.Check;
var
  err: string;
  procedure AddErr(s: string);
  begin
    if err <> '' then
      err := err + ',';
    err := err + #13#10 + s;
  end;
begin
  err := '';
  if trim(edURL.Text) = '' then
    AddErr(lblAddress.Caption + ' is not specified');
  if trim(edSP_Username.Text) = '' then
    AddErr(lblUser.Caption + ' is not specified');
  if trim(edSP_Password.Password) = '' then
    AddErr(lblPassword.Caption + ' is not specified');
  if err <> '' then
    raise Exception.Create('Invalid Web Service connection settings:'+err);
end;

procedure TSP_ConnectionParamFrm.ForceSavePassword;
begin
  cbSP_SavePassword.Checked := true;
end;

function TSP_ConnectionParamFrm.GetParam: TSP_ConnectionParam;
begin
  Result.URL := Trim(edURL.Text);
  Result.Username := Trim(edSP_Username.Text);
  Result.Password := edSP_Password.Password;
  Result.SavePassword := cbSP_SavePassword.Checked;
end;

function TSP_ConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(edURL.Text) <> '');// and
//            (trim(edSP_Username.Text) <> '') and
//            (edSP_Password.Password <> '');
end;

procedure TSP_ConnectionParamFrm.SetParam(
  const Value: TSP_ConnectionParam);
begin
  edURL.Text := Value.URL;
  edSP_Username.Text := Value.Username;
  edSP_Password.Password := Value.Password;
  cbSP_SavePassword.Checked := Value.SavePassword;
end;

end.
