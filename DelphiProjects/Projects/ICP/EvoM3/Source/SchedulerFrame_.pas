unit SchedulerFrame_;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, scheduler, scheduledTask, gdyCommonLogger, DB, Grids, Wwdbigrd,
  Wwdbgrid, dbcomp, ActnList, ExtCtrls, StdCtrls, Buttons, ImgList,
  ComCtrls, DBCtrls, isSettings;

type
  TEditParametersEvent = procedure(task: IScheduledTask) of object;
  TCanEditParametersEvent = procedure(var can: boolean) of object;
  TSaveAllSettingsEvent = procedure of object;

  TSchedulerFrm_ = class(TFrame)
    dsTasks: TDataSource;
    pnlTasksControl: TPanel;
    ActionList1: TActionList;
    actRefresh: TAction;
    BitBtn1: TBitBtn;
    actDelete: TAction;
    ImageList1: TImageList;
    actSchedule: TAction;
    actRunNow: TAction;
    dsResults: TDataSource;
    actShowLog: TAction;
    actSendDebugLog: TAction;
    actEditParams: TAction;
    PageControl2: TPageControl;
    tbshTasks: TTabSheet;
    tbshAllResults: TTabSheet;
    pnlTasks: TPanel;
    dgTasks: TReDBGrid;
    Panel1: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    pnlDetails: TPanel;
    pcTaskDetails: TPageControl;
    tbshParameters: TTabSheet;
    Panel3: TPanel;
    BitBtn7: TBitBtn;
    DBMemo1: TDBMemo;
    Panel4: TPanel;
    Panel5: TPanel;
    DBText1: TDBText;
    Label1: TLabel;
    Panel6: TPanel;
    DBText2: TDBText;
    Label2: TLabel;
    Panel7: TPanel;
    DBText3: TDBText;
    Label3: TLabel;
    Panel8: TPanel;
    DBText4: TDBText;
    Label4: TLabel;
    tbshResults: TTabSheet;
    Panel2: TPanel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    dgResults: TReDBGrid;
    Splitter1: TSplitter;
    Panel9: TPanel;
    dgAllResults: TReDBGrid;
    dsAllResults: TDataSource;
    actShowLog2: TAction;
    actSendDebugLog2: TAction;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    actGotoTask: TAction;
    BitBtn10: TBitBtn;
    actOpenFolder: TAction;
    BitBtn11: TBitBtn;
    actOpenFolder2: TAction;
    BitBtn12: TBitBtn;
    actEnableSelected: TAction;
    BitBtn13: TBitBtn;
    actDisableSelected: TAction;
    actEnableAll: TAction;
    actDisableAll: TAction;
    BitBtn14: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    btnOverrideEmail: TBitBtn;
    actOverrideEmail: TAction;
    procedure actRefreshExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actRefreshUpdate(Sender: TObject);
    procedure SingleTaskActionUpdate(Sender: TObject);
    procedure actScheduleExecute(Sender: TObject);
    procedure actRunNowExecute(Sender: TObject);
    procedure ResultsActionUpdate(Sender: TObject);
    procedure actShowLogExecute(Sender: TObject);
    procedure actSendDebugLogExecute(Sender: TObject);
    procedure actEditParamsExecute(Sender: TObject);
    procedure actEditParamsUpdate(Sender: TObject);
    procedure AllResultsActionUpdate(Sender: TObject);
    procedure actShowLog2Execute(Sender: TObject);
    procedure actSendDebugLog2Execute(Sender: TObject);
    procedure actGotoTaskExecute(Sender: TObject);
    procedure dgTasksDblClick(Sender: TObject);
    procedure dgAllResultsDblClick(Sender: TObject);
    procedure dgResultsDblClick(Sender: TObject);
    procedure actOpenFolderExecute(Sender: TObject);
    procedure actOpenFolder2Execute(Sender: TObject);
    procedure MultipleTaskActionUpdate(Sender: TObject);
    procedure actEnableSelectedExecute(Sender: TObject);
    procedure actDisableSelectedExecute(Sender: TObject);
    procedure actEnableAllExecute(Sender: TObject);
    procedure actDisableAllExecute(Sender: TObject);
    procedure actOverrideEmailExecute(Sender: TObject);
    procedure actOverrideEmailUpdate(Sender: TObject);
  private
    FScheduler: TScheduler;
    FLogger: ICommonLogger;
    FOnEditParameters: TEditParametersEvent;
    FReasonWhyWeDontHaveScheduler: string;

    procedure ShowUserLog(ds: TDataSet);
    procedure SendDebugLog(ds: TDataSet);
    procedure OpenFolderWithDebugLog(ds: TDataSet);
    function DisableTask(ds: TDataSet): boolean;
    function EnableTask(ds: TDataSet): boolean;
  protected
    function GetScheduler: TScheduler;
  public
    OnSaveAllSettings: TSaveAllSettingsEvent;
    OnCanEditParameters: TCanEditParametersEvent;
    constructor Create( Owner: TComponent ); override;
    procedure AddTask(task: IScheduledTask);
    procedure Configure(logger: ICommonLogger; ta: ITaskAdapter; EditParametersHandler: TEditParametersEvent);
  end;

implementation

{$R *.dfm}

uses
  common, gdyUtils, gdyMail, OverrideEmailDialog, gdyDialogEngine;

{ TSchedulerFrm }

function LoadOverrideEmail(conf: IisSettings): string;
begin
  Result := conf.AsString[sOverrideEmail];
end;

procedure SaveOverrideEmail(conf: IisSettings; const aValue: string);
begin
  conf.AsString[sOverrideEmail] := aValue;
end;

procedure TSchedulerFrm_.AddTask(task: IScheduledTask);
begin
  if FScheduler = nil then
    raise Exception.CreateFmt('Scheduler is not initialized because an error happened. The error was: %s',[FReasonWhyWeDontHaveScheduler]);
  if assigned(OnSaveAllSettings) then
    OnSaveAllSettings;
  FScheduler.AddTask(task);
  FScheduler.ShowScheduleForCurrentTask;
end;

procedure TSchedulerFrm_.Configure(logger: ICommonLogger; ta: ITaskAdapter; EditParametersHandler: TEditParametersEvent);
begin
  FLogger := logger;
  FOnEditParameters := EditParametersHandler;

  dgTasks.Selected.Clear;
  AddSelected(dgTasks.Selected, 'NAME', 20, 'Name', true);
  AddSelected(dgTasks.Selected, 'CUSTOM_COMPANY_NUMBER', 12, 'Company code', true);
  AddSelected(dgTasks.Selected, 'COMPANY_NAME', 30, 'Company name', true);
  AddSelected(dgTasks.Selected, 'CUSTOM_CLIENT_NUMBER', 12, 'Client code', true);
  AddSelected(dgTasks.Selected, 'CLIENT_NAME', 30, 'Client name', true);
  AddSelected(dgTasks.Selected, 'LAST_RUN_TIME', 20, 'Last run time', true);
  AddSelected(dgTasks.Selected, 'LAST_RUN_STATUS', 20, 'Last run status', true);
  AddSelected(dgTasks.Selected, 'ENABLED', 1, 'Enabled', true);

  dgResults.Selected.Clear;
  AddSelected(dgResults.Selected, 'WHEN', 20, 'Run time', true);
  AddSelected(dgResults.Selected, 'STATUS', 20, 'Status', true);
  AddSelected(dgResults.Selected, 'MESSAGE', 30, 'Info', true);

  dgAllResults.Selected.Clear;
  AddSelected(dgAllResults.Selected, 'NAME', 20, 'Name', true);
  AddSelected(dgAllResults.Selected, 'WHEN', 20, 'Run time', true);
  AddSelected(dgAllResults.Selected, 'STATUS', 20, 'Status', true);
  AddSelected(dgAllResults.Selected, 'CUSTOM_COMPANY_NUMBER', 12, 'Company code', true);
  AddSelected(dgAllResults.Selected, 'COMPANY_NAME', 30, 'Company name', true);
  AddSelected(dgAllResults.Selected, 'CUSTOM_CLIENT_NUMBER', 12, 'Client code', true);
  AddSelected(dgAllResults.Selected, 'CLIENT_NAME', 30, 'Client name', true);
  AddSelected(dgAllResults.Selected, 'MESSAGE', 30, 'Info', true);

  try
    FScheduler := TScheduler.Create(Self, logger, ta);
  except
    on E:Exception do
    begin
      FReasonWhyWeDontHaveScheduler := E.Message;
      raise;
    end
  end;
  dsTasks.DataSet := FScheduler.TasksDataSet;
  dsResults.DataSet := FScheduler.ResultsDataSet;
  dsAllResults.DataSet := FScheduler.AllResultsDataSet;
end;

procedure TSchedulerFrm_.actRefreshExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      FScheduler.Refresh;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.SingleTaskActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0)
       and ((dgTasks.SelectedList = nil) or (dgTasks.SelectedList.Count <= 1));
end;

procedure TSchedulerFrm_.actDeleteExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      if not FScheduler.IsCurrentTaskRunning  then
      begin
        if MessageDlg('Delete task?', mtConfirmation, [mbYes, mbNo], 0{, [mbNo]}) = mrYes then
          FScheduler.DeleteCurrentTask;
      end
      else
        ShowMessage('Cannot delete task while it is running');
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actRefreshUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler);
end;

procedure TSchedulerFrm_.actScheduleExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      FScheduler.ShowScheduleForCurrentTask;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actRunNowExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      if not FScheduler.IsCurrentTaskRunning  then
      begin
        if MessageDlg('Run task?', mtConfirmation, [mbYes, mbNo], 0{, [mbNo]}) = mrYes then
        begin
          if assigned(OnSaveAllSettings) then
            OnSaveAllSettings;
          FScheduler.RunCurrentTask;
          ShowMessage('The task is running now. Press the "Refresh" button a minute later to see the result');
        end
      end
      else
        ShowMessage('Task is already running now')
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.ResultsActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsResults.DataSet) and (dsResults.DataSet.RecordCount > 0);
end;

procedure TSchedulerFrm_.actShowLogExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in task''s results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      ShowUserLog( dsResults.DataSet );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actSendDebugLogExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in task''s results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      SendDebugLog(dsResults.Dataset);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actEditParamsExecute(Sender: TObject);
var
  task: IScheduledTask;
begin
  Assert(assigned(FOnEditParameters));

  task := LoadTask(FScheduler.TasksDataSet['GUID']);
  FOnEditParameters(task);
  FScheduler.TaskParametersEdited(task);
end;

procedure TSchedulerFrm_.actEditParamsUpdate(Sender: TObject);
var
  can: boolean;
begin
  can := true;
  if assigned(OnCanEditParameters) then
    OnCanEditParameters(can);

  (Sender as TCustomAction).Enabled := can and
      assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0);
end;

procedure TSchedulerFrm_.AllResultsActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsAllResults.DataSet) and (dsAllResults.DataSet.RecordCount > 0);
end;

procedure TSchedulerFrm_.actShowLog2Execute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item of all results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      ShowUserLog( dsAllResults.DataSet );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actSendDebugLog2Execute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item of all results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      SendDebugLog(dsAllResults.DataSet);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

constructor TSchedulerFrm_.Create(Owner: TComponent);
begin
  inherited;
  PageControl2.ActivePage := tbshTasks;
  pcTaskDetails.ActivePage := tbshParameters;
end;

procedure TSchedulerFrm_.ShowUserLog(ds: TDataSet);
var
  fn: string;
begin
  fn := LoadTask(ds['GUID']).GetTaskResultFiles(ds['FOLDER']).UserHtmlLogFilename;
  if FileExists(fn) then //to not expose location to user
    OpenDoc(Application.MainForm, fn)
  else
    raise Exception.Create('Log file doesn''t exist');
end;

procedure TSchedulerFrm_.SendDebugLog(ds: TDataSet);
var
  fn: string;
begin
  fn := LoadTask(ds['GUID']).GetTaskResultFiles(ds['FOLDER']).LogArchiveFileName;
  if FileExists(fn) then //to not expose location to user
    SimpleMailTo( GetISystemsEMail, 'Debug log', '', fn )
  else
    raise Exception.Create('Debug log file doesn''t exist');
end;

procedure TSchedulerFrm_.OpenFolderWithDebugLog(ds: TDataSet);
var
  fn: string;
begin
  fn := LoadTask(ds['GUID']).GetTaskResultFiles(ds['FOLDER']).LogArchiveFileName;
  if FileExists(fn) then //to not expose location to user
    OpenInExplorer(fn)
  else
    raise Exception.Create('Debug log file doesn''t exist');
end;

procedure TSchedulerFrm_.actGotoTaskExecute(Sender: TObject);
begin
  Assert(dsResults.DataSet <> nil);
  if not dsTasks.DataSet.Locate('GUID', dsAllResults.DataSet['GUID'], []) then
    Assert(false);
  if not dsResults.DataSet.Locate('FOLDER', dsAllResults.DataSet['FOLDER'], []) then
    Assert(false);
  PageControl2.ActivePage := tbshTasks;
  pcTaskDetails.ActivePage := tbshParameters;
end;

procedure TSchedulerFrm_.dgTasksDblClick(Sender: TObject);
begin
  actSchedule.Execute;
end;

procedure TSchedulerFrm_.dgAllResultsDblClick(Sender: TObject);
begin
  actShowLog2.Execute;
end;

procedure TSchedulerFrm_.dgResultsDblClick(Sender: TObject);
begin
  actShowLog.Execute;
end;

procedure TSchedulerFrm_.actOpenFolderExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in task''s results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      OpenFolderWithDebugLog(dsResults.DataSet);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actOpenFolder2Execute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in all results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      OpenFolderWithDebugLog(dsAllResults.DataSet);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.MultipleTaskActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0);
end;

function TSchedulerFrm_.DisableTask(ds: TDataSet): boolean;
begin
  FScheduler.EnableCurrentTask(false);
  Result := true;
end;

function TSchedulerFrm_.EnableTask(ds: TDataSet): boolean;
begin
  FScheduler.EnableCurrentTask(true);
  Result := true;
end;

procedure TSchedulerFrm_.actEnableSelectedExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForSelected(dgTasks, EnableTask, [fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty]);
      finally
        FScheduler.Refresh;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actDisableSelectedExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForSelected(dgTasks, DisableTask, [fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty]);
      finally
        FScheduler.Refresh;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure ForAll(ds: TDataSet; handler: TOnSelected);
var
  savePos: TBookmark;
begin
  if assigned( ds ) then
  begin
    savePos := ds.GetBookmark;
    try
      try
        ds.DisableControls;
        try
          ds.First;
          while not ds.Eof do
          begin
            handler(ds);
            ds.Next;
          end;
        finally
          ds.EnableControls;
        end;
      finally
        if ds.BookmarkValid( savePos ) then
          ds.GotoBookmark( savePos );
      end;
    finally
      ds.FreeBookmark( savePos );
    end;
  end
end;

procedure TSchedulerFrm_.actEnableAllExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForAll(dsTasks.DataSet, EnableTask);
      finally
        FScheduler.Refresh;
      end;
  except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm_.actDisableAllExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForAll(dsTasks.DataSet, DisableTask);
      finally
        FScheduler.Refresh;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSchedulerFrm_.GetScheduler: TScheduler;
begin
  if FScheduler = nil then
    raise Exception.CreateFmt('Scheduler is not initialized because an error happened. The error was: %s',[FReasonWhyWeDontHaveScheduler]);
  Result := FScheduler;  
end;

procedure TSchedulerFrm_.actOverrideEmailExecute(Sender: TObject);
var
  task: IScheduledTask;
  dlg: TOverrideEmailDlg;
begin
  task := LoadTask(FScheduler.TasksDataSet['GUID']);
  dlg := TOverrideEmailDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Override Email';
    dlg.edAddressTo.Text := LoadOverrideEmail(task.ParamSettings);
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveOverrideEmail(task.ParamSettings, dlg.edAddressTo.Text);
  finally
    FreeAndNil(dlg);
  end;
end;

procedure TSchedulerFrm_.actOverrideEmailUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled :=
      assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0);
end;

end.
