program EvoM3;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  common,
  evoapiconnection,
  jclfileutils,
  gdyredir,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionClCoFrame in '..\..\common\EvolutionClCoFrame.pas' {EvolutionClCoFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  sevenzip in '..\..\common\gdycommon\sevenzip\sevenzip.pas',
  EvoAPIClientNewMainForm in '..\..\common\EvoAPIClientNewMainForm.pas' {EvoAPIClientNewMainFm: TEvoAPIClientMainFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvoWaitForm in '..\..\common\EvoWaitForm.pas' {EvoWaitFm},
  scheduledTask in '..\..\common\scheduledTask.pas',
  ImportProcessing in 'ImportProcessing.pas',
  MainForm in 'MainForm.pas' {MainFm: TEvoAPIClientNewMainFm},
  M3_Tasks in 'M3_Tasks.pas',
  EeHolder in 'EeHolder.pas',
  SecurePayrollDataService in 'SecurePayrollDataService.pas',
  M3Report in 'M3Report.pas',
  SP_ConnectionParamFrame in 'SP_ConnectionParamFrame.pas' {SP_ConnectionParamFrm: TFrame},
  ReportParamFrame in 'ReportParamFrame.pas' {ReportParamFrm: TFrame},
  Mapping in 'Mapping.pas',
  WebServiceConnection in 'WebServiceConnection.pas',
  SchedulerFrame_ in 'SchedulerFrame_.pas' {SchedulerFrm_: TFrame};

{$R *.res}
begin
//  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}

//  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License
  LicenseKey := 'AA63A5BE6C28492BA233E887A04F10C1'; // EvoM3 License
  ConfigVersion := 4;

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'EvoM3';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
    Application.Run;
  end
  else
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TM3_TaskAdapter.Create, True, True, True, True));
end.
