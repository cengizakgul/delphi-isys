inherited MainFm: TMainFm
  Left = 402
  Top = 106
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = TabSheet3
    inherited TabSheet3: TTabSheet
      Caption = 'Settings'
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        inherited GroupBox1: TGroupBox
          Caption = 'Evolution Connection'
        end
      end
      object pnlSpace: TPanel
        Left = 0
        Top = 106
        Width = 776
        Height = 5
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
      inline SP_Frame: TSP_ConnectionParamFrm
        Left = 0
        Top = 111
        Width = 776
        Height = 106
        Align = alTop
        TabOrder = 2
        inherited gbSP_Server: TGroupBox
          Width = 776
          Height = 100
          Align = alTop
          inherited edURL: TEdit
            Left = 143
            Width = 378
          end
          inherited edSP_Username: TEdit
            Left = 143
            Width = 124
          end
          inherited edSP_Password: TPasswordEdit
            Left = 143
            Width = 124
          end
          inherited cbSP_SavePassword: TCheckBox
            Left = 271
          end
        end
      end
      inline ReportFrame: TReportParamFrm
        Left = 0
        Top = 217
        Width = 776
        Height = 80
        Align = alTop
        TabOrder = 3
        inherited gbSP_Server: TGroupBox
          Width = 776
          inherited lblReportNbr: TLabel
            Left = 16
          end
          inherited lblLevel: TLabel
            Left = 16
            Width = 162
            Caption = 'Level                                  Bureau'
          end
          inherited lblLastSync: TLabel
            Left = 288
          end
          inherited edReportnbr: TEdit
            Left = 143
            Width = 125
          end
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      TabVisible = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 89
        Align = alTop
        Caption = 'EvoPayrollPlans connection'
        TabOrder = 0
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Import Emplyees'
      inherited pnlBottom: TPanel
        Top = 423
        Height = 1
      end
      object gbJustFrame: TGroupBox
        Left = 0
        Top = 0
        Width = 776
        Height = 423
        Align = alClient
        TabOrder = 1
        object btnUploadEligibilityfile: TBitBtn
          Left = 23
          Top = 26
          Width = 256
          Height = 28
          Action = actImportEmployees
          Caption = 'Import Employees'
          TabOrder = 0
        end
      end
    end
    inherited tbshScheduler: TTabSheet
      inherited SchedulerFrame: TSchedulerFrm
        Left = 216
        Top = 120
        Width = 560
        Height = 304
        Align = alNone
        Visible = False
        inherited pnlTasksControl: TPanel
          Width = 560
        end
        inherited PageControl2: TPageControl
          Width = 560
          Height = 263
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 9
              Width = 552
            end
            inherited pnlTasks: TPanel
              Width = 552
              Height = 9
              inherited dgTasks: TReDBGrid
                Width = 552
              end
              inherited Panel1: TPanel
                Top = -32
                Width = 552
              end
            end
            inherited pnlDetails: TPanel
              Top = 16
              Width = 552
              inherited pcTaskDetails: TPageControl
                Width = 552
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 377
                  end
                  inherited dgResults: TReDBGrid
                    Width = 377
                  end
                end
              end
            end
          end
        end
      end
      inline SchedulerFrm_: TSchedulerFrm_
        Left = 0
        Top = 0
        Width = 776
        Height = 424
        Align = alClient
        TabOrder = 1
        inherited pnlTasksControl: TPanel
          Width = 776
        end
        inherited PageControl2: TPageControl
          Width = 776
          Height = 383
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 129
              Width = 768
            end
            inherited pnlTasks: TPanel
              Width = 768
              Height = 129
              inherited dgTasks: TReDBGrid
                Width = 768
                Height = 88
              end
              inherited Panel1: TPanel
                Top = 88
                Width = 768
              end
            end
            inherited pnlDetails: TPanel
              Top = 136
              Width = 768
              inherited pcTaskDetails: TPageControl
                Width = 768
                inherited tbshParameters: TTabSheet
                  inherited Panel3: TPanel
                    Left = 610
                  end
                  inherited DBMemo1: TDBMemo
                    Width = 610
                  end
                  inherited Panel4: TPanel
                    Width = 760
                  end
                end
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 593
                  end
                  inherited dgResults: TReDBGrid
                    Width = 593
                  end
                end
              end
            end
          end
        end
      end
    end
    inherited tbshSchedulerSettings: TTabSheet
      inherited SmtpConfigFrame: TSmtpConfigFrm
        Width = 776
        inherited GroupBox1: TGroupBox
          Width = 776
        end
      end
      object BitBtn7: TBitBtn
        Left = 16
        Top = 199
        Width = 97
        Height = 28
        Action = actScheduleImportEmployees
        Caption = 'Create task'
        TabOrder = 1
      end
    end
  end
  inherited pnlCompany: TPanel
    inherited Panel1: TPanel
      Width = 99
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 99
      Width = 685
    end
  end
  inherited ActionList1: TActionList
    Left = 516
    Top = 88
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    object actImportEmployees: TAction
      Caption = 'Import Employees'
      OnExecute = actImportEmployeesExecute
      OnUpdate = actImportEmployeesUpdate
    end
    object actScheduleImportEmployees: TAction
      Caption = 'Create task'
      OnExecute = actScheduleImportEmployeesExecute
      OnUpdate = actImportEmployeesUpdate
    end
  end
end
