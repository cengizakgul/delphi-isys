inherited ReportParamFrm: TReportParamFrm
  Width = 515
  Height = 80
  object gbSP_Server: TGroupBox
    Left = 0
    Top = 0
    Width = 515
    Height = 80
    Align = alClient
    Caption = 'Evolution M3 Import Report'
    TabOrder = 0
    object lblReportNbr: TLabel
      Left = 11
      Top = 52
      Width = 37
      Height = 13
      Caption = 'Number'
      Transparent = False
    end
    object lblLevel: TLabel
      Left = 11
      Top = 26
      Width = 126
      Height = 13
      Caption = 'Level                      Bureau'
      Transparent = False
    end
    object lblLastSync: TLabel
      Left = 256
      Top = 52
      Width = 77
      Height = 13
      Caption = '(Last sync: N/A)'
      OnDblClick = lblLastSyncDblClick
    end
    object edReportnbr: TEdit
      Left = 103
      Top = 47
      Width = 114
      Height = 21
      TabOrder = 0
    end
  end
end
