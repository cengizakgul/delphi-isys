unit M3Report;

interface

uses DB, kbmMemTable, gdyCommonLogger, SysUtils, EvoAPIConnection,
  Classes, Types, common;

const
  sSpaceReplace = '*-*';  

type
  TM3Report = class
  private
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    FReportResult: TkbmCustomMemTable;
    FReport: TEvoReportDef;
    FShowGUI: boolean;

    procedure PrepareDataset;
    procedure ProcessRecord(rec: TStrings);
    procedure LoadDataFromString(const ASCII_Result: string);
  public
    constructor Create(logger: ICommonLogger; EvoConn: IEvoAPIConnection; const aReportLevel: string;
      aReportNbr: integer; aLastSync: TDatetime; company: TEvoCompanyDef; ShowGUI: boolean);
    destructor Destroy; override;
    function RunReport: boolean;

    property ReportResult: TkbmCustomMemTable read FReportResult;
  end;

implementation

uses TypInfo, gdyRedir, DateUtils, XmlRpcTypes, Mapping, gdyGlobalWaitIndicator;

{ TM3Report }

constructor TM3Report.Create(logger: ICommonLogger; EvoConn: IEvoAPIConnection; const aReportLevel: string;
  aReportNbr: integer; aLastSync: TDatetime; company: TEvoCompanyDef; ShowGUI: boolean);
var
  Params: IRpcStruct;
  Clients, Companies: IRpcArray;
begin
  FLogger := logger;
  FEvoAPI := EvoConn;
  FShowGUI := ShowGUI;

  FReport.Level := aReportLevel;
  FReport.Number := aReportNbr;

  Params := TRpcStruct.Create;
  Clients := TRpcArray.Create;
  Companies := TRpcArray.Create;

  Clients.AddItem(company.ClNbr);
  Params.AddItem('Clients', Clients);

  Companies.AddItem(company.CoNbr);
  Params.AddItem('Companies', Companies);

  Params.AddItemDateTime('LastSync', aLastSync);

  FReport.Params := Params;
end;

destructor TM3Report.Destroy;
begin
  FreeAndNil( FReportResult );
  inherited;
end;

procedure TM3Report.LoadDataFromString(const ASCII_Result: string);
var
  sFile, Rec: TStrings;
  i: integer;
begin
  if Trim(ASCII_Result) = '' then
    FLogger.LogEvent('Report is empty')
  else begin
    sFile := TStringList.Create;
    Rec := TStringList.Create;
    try
      PrepareDataset;
      FReportResult.Open;

      sFile.Text := ASCII_Result;

//      sFile.LoadFromFile('c:\ICP\EvoM3\XMLReport.txt');

      // the first row contains column headers
      // data starts from the second row
      for i := 1 to sFile.Count - 1 do
      begin
        Rec.CommaText := StringReplace(sFile[i], ' ', sSpaceReplace, [rfReplaceAll, rfIgnoreCase]);
        ProcessRecord( Rec );
      end;
    finally
      sFile.Free;
      Rec.Free;
    end;

    if FReportResult.State in [dsEdit, dsInsert] then
      FReportResult.Post;
  end;
end;

procedure TM3Report.PrepareDataset;
var
  i: integer;
begin
  FReportResult := TkbmCustomMemTable.Create(nil);

  for i := Low(Map) to High(Map) do
  case Map[i].T of
    ftString: CreateStringField( FReportResult, Map[i].Name, Map[i].Name, Map[i].S );
    ftCurrency: CreateFloatField( FReportResult, Map[i].Name, Map[i].Name );
    ftDate: CreateDateTimeField( FReportResult, Map[i].Name, Map[i].Name );
    ftInteger: CreateIntegerField( FReportResult, Map[i].Name, Map[i].Name );
  end;
end;

procedure TM3Report.ProcessRecord(rec: TStrings);
var
  i, vInt: integer;
  vYear, vMonth, vDay: integer;
  s: string;
  vFloat: Double;
begin
  FReportResult.Append;

  for i := 0 to Rec.Count - 1 do
  begin
    s := StringReplace(Trim(rec[i]), sSpaceReplace, ' ', [rfReplaceAll, rfIgnoreCase]);
    case FReportResult.Fields[i].DataType of
    ftString:
      FReportResult.Fields[i].AsString := s;
    ftInteger:
      begin
        if TryStrToInt(s, vInt) then
          FReportResult.Fields[i].AsInteger := vInt;
      end;
    ftDateTime, ftDate:
      begin // date on the M3 Import report has format "YYYYMMDD"
        if TryStrToInt( Copy(s, 1, 4), vYear) and TryStrToInt( Copy(s, 5, 2), vMonth) and TryStrToInt( Copy(s, 7, 2), vDay) then
          FReportResult.Fields[i].AsDateTime := EncodeDate(vYear, vMonth, vDay);
      end;
    ftFloat:
      begin
        if TryStrToFloat(s, vFloat) then
          FReportResult.Fields[i].AsFloat := vFloat;
      end;
    end;
  end;

  FReportResult.Post;
end;

function TM3Report.RunReport: boolean;
var
  TaskId, ASCII_res: string;
  k: integer; 
begin
  FLogger.LogEvent('Run M3 Import report');
  if FShowGUI then
    WaitIndicator.StartWait('Run M3 Import report');

  try
    TaskId := FEvoAPI.AddReportTask(FReport);
    FLogger.LogDebug('Run report', 'Task ID = ' + TaskId);

    // 15 min waiting for result
    k := 0;
    repeat
      if FShowGUI then
        WaitIndicator.StartWait('Please wait for the results');
      try
        Sleep(10000);
      finally
        if FShowGUI then
          WaitIndicator.EndWait;
      end;

      k := k + 1;
      Result := FEvoAPI.IsTaskFinished(TaskId);
    until Result or (k = 90);

    if Result then
    begin
      // get the report result
      if FShowGUI then
        WaitIndicator.StartWait('Get the report result');
      try
        ASCII_res := FEvoAPI.GetASCIIReport(TaskId);
        FLogger.LogDebug('Get the report result', ASCII_res);
      finally
        if FShowGUI then
          WaitIndicator.EndWait;
      end;

      if FShowGUI then
        WaitIndicator.StartWait('Load data from the report');
      try
        // load the result to Dataset
        LoadDataFromString(ASCII_res);
        FLogger.LogDebug('Load the result to dataset', 'Records loaded = ' + IntToStr(FReportResult.RecordCount));
      finally
        if FShowGUI then
          WaitIndicator.EndWait;
      end;
    end
    else
      FLogger.LogEvent('Waitng report result limit is 15 min. The report has not been finished in that time.');
  finally
    if FShowGUI then
      WaitIndicator.EndWait;
  end;
end;

end.
