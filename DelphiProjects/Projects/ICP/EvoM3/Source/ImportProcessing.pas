unit ImportProcessing;

interface

uses
  Evconsts, evodata, Classes, XmlRpcTypes, SP_ConnectionParamFrame, gdyLoggerImpl,
  gdycommonlogger, PlannedActions, EeHolder, M3Report, ReportParamFrame,
  evoapiconnectionutils, gdyClasses, common, kbmMemTable, EvoAPIConnection;

type
  TImportProcessing = class
  private
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    FSP_Param: TSP_ConnectionParam;
    FReportParam: TReportParam;
    FShowGUI: boolean;
  public
    constructor Create(logger: ICommonLogger; EvoConn: IEvoAPIConnection; SP_Param: TSP_ConnectionParam;
      ReportParam: TReportParam; ShowGUI: boolean);

    function ImportEmployees(company: TEvoCompanyDef): boolean;
  end;

  function DoImportEmployees(Logger: ICommonLogger; StatefulLogger: IStatefulLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection;
    SP_Connection: TSP_ConnectionParam; var RepParam: TReportParam; showGUI: boolean): boolean;

implementation

uses
  sysutils, variants, gdycommon, WebServiceConnection, smtpConfigFrame,
  EvoWaitForm, gdyRedir, gdyGlobalWaitIndicator, userActionHelpers,
  XmlRpcCommon, PlannedActionConfirmationForm, forms, dateutils, DB,
  gdyLogWriters;

procedure SendNotificationEmail(cap, msg: string);
var
  cfg: TSmtpConfig;
begin
  try
    cfg := LoadSmtpConfig(AppSettings, '');
    sendmail( cfg,
       Format('%s - %s', [Application.Title, cap]),
       Format('This message was automatically sent by %s.'#13#10'%s', [Application.Title, msg]), '' );
  except

  end;
end;

function DoImportEmployees(Logger: ICommonLogger; StatefulLogger: IStatefulLogger; company: TEvoCompanyDef;
  evoConn: IEvoAPIConnection; SP_Connection: TSP_ConnectionParam; var RepParam: TReportParam;
  showGUI: boolean): boolean;
var
  ImportEe: TImportProcessing;
  lSync: TDateTime;

  FMailUserLog: TStringStream;
  FMailUserLogOutputWriter: ILoggerEventSink;
begin
  Result := False;
  lSync := now;

  if Assigned(StatefulLogger) then
  begin
    FMailUserLog := TStringStream.Create('');
    FMailUserLogOutputWriter := TPlainTextUserLogWriter.Create( TStreamLogOutput.Create(FMailUserLog) );
    StatefulLogger.Advise(FMailUserLogOutputWriter);
  end;

  Logger.LogEntry('Import Employees');
  try
    try
      ImportEe := TImportProcessing.Create( Logger, evoConn, SP_Connection, RepParam, ShowGUI );
      try
        Result := ImportEe.ImportEmployees( company );
      finally
        FreeAndNil(ImportEe)
      end;
      RepParam.LastSync := lSync;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;

    if Assigned(StatefulLogger) then
    begin
      SendNotificationEmail(
        Format('Import Employees (Cl# %s, Co# %s)', [company.CUSTOM_CLIENT_NUMBER, Company.CUSTOM_COMPANY_NUMBER]),
        FMailUserLog.DataString
      );

      StatefulLogger.UnAdvise(FMailUserLogOutputWriter);
      FMailUserLogOutputWriter := nil;
      FreeAndNil(FMailUserLog);
    end;  
  end;
end;

{ TImportProcessing }

constructor TImportProcessing.Create(logger: ICommonLogger; EvoConn: IEvoAPIConnection;
  SP_Param: TSP_ConnectionParam; ReportParam: TReportParam; ShowGUI: boolean);
begin
  FLogger := logger;
  FEvoAPI := EvoConn;
  FSP_Param := SP_Param;
  FReportParam := ReportParam;
  FShowGUI := ShowGUI;
end;

function TImportProcessing.ImportEmployees(company: TEvoCompanyDef): boolean;
var
  WebService: IWebServiceConnection;
begin
  Result := False;
  with TM3Report.Create( FLogger, FEvoAPI, 'B', FReportParam.ReportNbr, FReportParam.LastSync, company, FShowGUI ) do
  if RunReport then
  begin
    if FShowGUI then
      WaitIndicator.StartWait('Sending data to RightTime');
    try
      WebService := CreateWebServiceConnection(FLogger, FSP_Param, FShowGUI);

      Result := WebService.ImportEmployees(ReportResult);
      if not Result then
        FLogger.LogError('Some submitted items FAILED to import')
      else
        FLogger.LogEvent('Submitted items successfully imported');  
    finally
      if FShowGUI then
        WaitIndicator.EndWait;
    end;
  end
end;

end.

