unit ReportParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OptionsBaseFrame, StdCtrls, Buttons, PasswordEdit,
  issettings, ActnList, gdyCommonLogger;

type
  TReportParam = record
    ReportNbr: integer;
    LastSync: TDateTime;
  end;

  TReportParamFrm = class(TOptionsBaseFrm)
    gbSP_Server: TGroupBox;
    lblReportNbr: TLabel;
    edReportnbr: TEdit;
    lblLevel: TLabel;
    lblLastSync: TLabel;
    procedure lblLastSyncDblClick(Sender: TObject);
  private
    FLastSync: TDateTime;
    function GetParam: TReportParam;
    procedure SetParam(const Value: TReportParam);
    procedure SetLastSyncDate(aValue: TDateTime);
  public
    property Param: TReportParam read GetParam write SetParam;
    function IsValid: boolean;
    procedure Check;
  end;

procedure SaveReportParam(param: TReportParam; conf: IisSettings; root: string);
function LoadReportParam(conf: IisSettings; root: string): TReportParam;

implementation

{$R *.dfm}

uses
  cswriter, printers, gdyclasses, strutils, gdycommon, gdyRedir, gdyCrypt,
  gdyUtils, gdyGlobalWaitIndicator;

const
  cKey='D1eg0';

procedure SaveReportParam(param: TReportParam; conf: IisSettings; root: string );
begin
  root := root + IIF(root='','','\') + 'Report\';
  conf.AsInteger[root+'ReportNbr'] := param.ReportNbr;
  conf.AsDateTime[root+'LastSync'] := param.LastSync;
end;

function LoadReportParam(conf: IisSettings; root: string): TReportParam;
begin
  root := root + IIF(root='','','\') + 'Report\';

  Result.ReportNbr := conf.AsInteger[root+'ReportNbr'];
  Result.LastSync := conf.AsDateTime[root+'LastSync'];
end;

{ TReportParamFrm }

procedure TReportParamFrm.Check;
var
  err: string;
  procedure AddErr(s: string);
  begin
    if err <> '' then
      err := err + ',';
    err := err + #13#10 + s;
  end;
begin
  err := '';
  if trim(edReportnbr.Text) = '' then
    AddErr(lblReportNbr.Caption + ' is not specified');
  if err <> '' then
    raise Exception.Create('Invalid M3 Import Report settings:'+err);
end;

function TReportParamFrm.GetParam: TReportParam;
var
  Nbr: integer;
begin
  if TryStrToInt(Trim(edReportnbr.Text), Nbr) then
    Result.ReportNbr := Nbr
  else
    Result.ReportNbr := 1;

  if FLastSync > 100 then
    Result.LastSync := FLastSync
  else
    Result.LastSync := 0;
end;

function TReportParamFrm.IsValid: boolean;
var
  Nbr: integer;
begin
  Result := TryStrToInt(trim(edReportnbr.Text), Nbr);
  Result := Result and (Nbr > 0);
end;

procedure TReportParamFrm.SetParam(
  const Value: TReportParam);
begin
  edReportnbr.Text := IntToStr(Value.ReportNbr);
  SetLastSyncDate( Value.LastSync );
end;

procedure TReportParamFrm.lblLastSyncDblClick(Sender: TObject);
begin
  inherited;
  if MessageDlg('Clear Last Sync Date?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    SetLastSyncDate(0);
end;

procedure TReportParamFrm.SetLastSyncDate(aValue: TDateTime);
begin
  FLastSync := aValue;

  if FLastSync > 100 then
    lblLastSync.Caption := '(Last Sync: ' + DateTimeToStr(FLastSync) + ')'
  else
    lblLastSync.Caption := '(Last Sync: N/A)';
end;

end.
