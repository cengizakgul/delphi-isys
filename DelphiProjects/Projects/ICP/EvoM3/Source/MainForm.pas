unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientNewMainForm, ActnList,
  EvolutionCompanySelectorFrame, StdCtrls, Buttons, ComCtrls,
  SmtpConfigFrame, SchedulerFrame, ExtCtrls, OptionsBaseFrame,
  EvoAPIConnectionParamFrame, scheduledTask,
  evoapiconnectionutils, common, SP_ConnectionParamFrame, M3_Tasks,
  ReportParamFrame, SchedulerFrame_;

type
  TMainFm = class(TEvoAPIClientNewMainFm)
    GroupBox1: TGroupBox;
    actImportEmployees: TAction;
    actScheduleImportEmployees: TAction;
    pnlSpace: TPanel;
    gbJustFrame: TGroupBox;
    btnUploadEligibilityfile: TBitBtn;
    SP_Frame: TSP_ConnectionParamFrm;
    ReportFrame: TReportParamFrm;
    SchedulerFrm_: TSchedulerFrm_;
    BitBtn7: TBitBtn;
    procedure actImportEmployeesExecute(Sender: TObject);
    procedure actScheduleImportEmployeesExecute(Sender: TObject);
    procedure actImportEmployeesUpdate(Sender: TObject);
  private
    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleSP_ConnectionParamChangedByUser(Sender: TObject);
    procedure HandleReportParamChangedByUser(Sender: TObject);
    procedure HandleEvoFrameChangedByUser(Sender: TObject);

    procedure ForceSavePasswords;
    procedure SetSbUserOverrideEmail(aTask: IScheduledTask);
    procedure ShowSchedulerTabs(const aUserName: string);
  protected
    procedure UnInitPerCompanySettings; override;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef); override;
  public
    constructor Create( Owner: TComponent ); override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  ImportProcessing, userActionHelpers, XmlRpcTypes, kbmMemTable,
  evodata, PlannedActionConfirmationForm, gdyGlobalWaitIndicator,
  gdyClasses, EvoWaitForm, EvoAPIClientMainForm, gdyDialogEngine,
  gdyRedir;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
  try
    SchedulerFrm_.Configure(Logger, TM3_TaskAdapter.Create, HandleEditParameters);
  except
    Logger.StopException;
  end;

  SP_Frame.Param := LoadSP_ConnectionParam( FSettings, '' );
  SP_Frame.OnChangeByUser := HandleSP_ConnectionParamChangedByUser;
  EvoFrame.OnChangeByUser := HandleEvoFrameChangedByUser;

  ReportFrame.Param := LoadReportParam( FSettings, '' );
  ReportFrame.OnChangeByUser := HandleReportParamChangedByUser;

  if EvoFrame.IsValid and SP_Frame.IsValid and ReportFrame.IsValid then
    PageControl1.ActivePageIndex := 2
  else
    PageControl1.ActivePageIndex := 0;

  ShowSchedulerTabs(EvoFrame.UserNameEdit.Text);  
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  M3_Tasks.EditTask(task, Logger, Self);
end;

procedure TMainFm.actImportEmployeesExecute(Sender: TObject);
var
  Res: boolean;
  Param: TReportParam;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
//    Res := True;
    try
      WaitIndicator.StartWait('Import process started');
      try
        Param := ReportFrame.Param;
        Res := DoImportEmployees(Logger, StatefulLogger, FEvoData.GetClCo, FEvoData.Connection, SP_Frame.Param, Param, true);
        ReportFrame.Param := Param;
        HandleReportParamChangedByUser(Sender);
      finally
        WaitIndicator.EndWait;
      end;
    except
      on e: exception do
      begin
        Logger.LogError(e.Message);
        Res := False;
      end;
    end;
    if not Res then
    begin
      ShowMessage('Some submitted items FAILED to import. See LOG and correct FAILED items.');
      PageControl1.ActivePage := tbshLog;
    end
    else
      ShowMessage('Submitted items successfully imported. See LOG for details');
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.ForceSavePasswords;
begin
  EvoFrame.ForceSavePassword;
  SP_Frame.ForceSavePassword;
end;

procedure TMainFm.actScheduleImportEmployeesExecute(Sender: TObject);
var
  Task: IScheduledTask;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      Task := NewImportEmployeesTask(FEvoData.GetClCo);
      SetSbUserOverrideEmail(Task);
      SchedulerFrm_.AddTask(Task);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;


procedure TMainFm.HandleSP_ConnectionParamChangedByUser(Sender: TObject);
begin
  try
    SaveSP_ConnectionParam( SP_Frame.Param, FSettings, '');
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  inherited;

end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  inherited;

end;

procedure TMainFm.HandleReportParamChangedByUser(Sender: TObject);
begin
  try
    SaveReportParam( ReportFrame.Param, FSettings, '');
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.actImportEmployeesUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and FEvoData.CanGetClCo and SP_Frame.IsValid and ReportFrame.IsValid;
end;

procedure TMainFm.SetSbUserOverrideEmail(aTask: IScheduledTask);
var
  Param: IRpcStruct;
  SbUser: TkbmCustomMemTable;
begin
  WaitIndicator.StartWait('Getting SB User "'+EvoFrame.Param.Username+'" email');
  try
    param := TRpcStruct.Create;
    Param.AddItem( 'UserID', EvoFrame.Param.Username );

    SbUser := FEvoData.Connection.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'qSbUserEmail.rwq', Param);
    if Assigned(SbUser) and (SbUser.RecordCount > 0) then
    begin
      aTask.ParamSettings.AsString[sOverrideEmail] := SbUser.FieldByName('Email_Address').AsString;
      FreeAndNil( SbUser );
    end;
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TMainFm.ShowSchedulerTabs(const aUserName: string);
begin
  tbshScheduler.TabVisible := (UpperCase(Copy(aUserName, 1, 5)) = 'INOVA');
  tbshSchedulerSettings.TabVisible := tbshScheduler.TabVisible;
end;

procedure TMainFm.HandleEvoFrameChangedByUser(Sender: TObject);
begin
  try
    SaveEvoAPIConnectionParam( EvoFrame.Param, FSettings, '' );
    ShowSchedulerTabs(EvoFrame.UserNameEdit.Text);
  except
    Logger.StopException;
  end;
end;

end.
