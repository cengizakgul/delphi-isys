inherited SP_ConnectionParamFrm: TSP_ConnectionParamFrm
  Width = 515
  Height = 104
  object gbSP_Server: TGroupBox
    Left = 0
    Top = 0
    Width = 515
    Height = 104
    Align = alClient
    Caption = 'Web Service Connection Settings'
    TabOrder = 0
    object lblAddress: TLabel
      Left = 16
      Top = 26
      Width = 22
      Height = 13
      Caption = 'URL'
    end
    object lblUser: TLabel
      Left = 16
      Top = 51
      Width = 48
      Height = 13
      Caption = 'Username'
    end
    object lblPassword: TLabel
      Left = 16
      Top = 75
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object edURL: TEdit
      Left = 113
      Top = 22
      Width = 278
      Height = 21
      TabOrder = 0
    end
    object edSP_Username: TEdit
      Left = 113
      Top = 47
      Width = 278
      Height = 21
      TabOrder = 1
    end
    object edSP_Password: TPasswordEdit
      Left = 113
      Top = 72
      Width = 278
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object cbSP_SavePassword: TCheckBox
      Left = 392
      Top = 74
      Width = 100
      Height = 17
      Caption = 'Save password'
      TabOrder = 3
    end
  end
end
