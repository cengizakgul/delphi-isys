// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : https://qaservice.m3righttime.com/SecurePayrollDataService.asmx?wsdl
//  >Import : https://qaservice.m3righttime.com/SecurePayrollDataService.asmx?wsdl:0
// Encoding : utf-8
// Version  : 1.0
// (11/16/2015 1:27:00 PM - - $Rev: 10138 $)
// ************************************************************************ //

unit SecurePayrollDataService;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]

  ExportDetails        = class;                 { "http://m3as.com/SPDS"[GblCplx] }
  Employee             = class;                 { "http://m3as.com/SPDS"[GblCplx] }
  ImportDetails        = class;                 { "http://m3as.com/SPDS"[GblCplx] }

  ArrayOfExportDetails = array of ExportDetails;   { "http://m3as.com/SPDS"[GblCplx] }


  // ************************************************************************ //
  // XML       : ExportDetails, global, <complexType>
  // Namespace : http://m3as.com/SPDS
  // ************************************************************************ //
  ExportDetails = class(TRemotable)
  private
    FRecordIDField: WideString;
    FRecordIDField_Specified: boolean;
    FEmployeeCode: WideString;
    FEmployeeCode_Specified: boolean;
    FEmployeeLNChar: WideString;
    FEmployeeLNChar_Specified: boolean;
    FHomeSiteCode: WideString;
    FHomeSiteCode_Specified: boolean;
    FHomeDept: WideString;
    FHomeDept_Specified: boolean;
    FHomeJob: WideString;
    FHomeJob_Specified: boolean;
    FEEStatus: WideString;
    FEEStatus_Specified: boolean;
    FSalaryOrHourly: WideString;
    FSalaryOrHourly_Specified: boolean;
    FWorkedSiteCode: WideString;
    FWorkedSiteCode_Specified: boolean;
    FWorkedDeptCode: WideString;
    FWorkedDeptCode_Specified: boolean;
    FWorkedJobCode: WideString;
    FWorkedJobCode_Specified: boolean;
    FDaysWorked: Integer;
    FRegHours: TXSDecimal;
    FRegRate: TXSDecimal;
    FOTHours: TXSDecimal;
    FOTRate: TXSDecimal;
    FOTFactor: TXSDecimal;
    FDTHours: TXSDecimal;
    FDTRate: TXSDecimal;
    FDTFactor: TXSDecimal;
    FEarningsCode: WideString;
    FEarningsCode_Specified: boolean;
    FEarningsHours: TXSDecimal;
    FEarningsCodeRate: TXSDecimal;
    FEarningsCodeAmount: TXSDecimal;
    FCustom1: WideString;
    FCustom1_Specified: boolean;
    FCustom2: WideString;
    FCustom2_Specified: boolean;
    FCustom3: WideString;
    FCustom3_Specified: boolean;
    FCustom4: WideString;
    FCustom4_Specified: boolean;
    FCustom5: WideString;
    FCustom5_Specified: boolean;
    FCustom6: WideString;
    FCustom6_Specified: boolean;
    FCustom7: WideString;
    FCustom7_Specified: boolean;
    FCustom8: WideString;
    FCustom8_Specified: boolean;
    FCustom9: WideString;
    FCustom9_Specified: boolean;
    FCustom10: WideString;
    FCustom10_Specified: boolean;
    FCustom11: WideString;
    FCustom11_Specified: boolean;
    FCustom12: WideString;
    FCustom12_Specified: boolean;
    FCustom13: WideString;
    FCustom13_Specified: boolean;
    FCustom14: WideString;
    FCustom14_Specified: boolean;
    FCustom15: WideString;
    FCustom15_Specified: boolean;
    procedure SetRecordIDField(Index: Integer; const AWideString: WideString);
    function  RecordIDField_Specified(Index: Integer): boolean;
    procedure SetEmployeeCode(Index: Integer; const AWideString: WideString);
    function  EmployeeCode_Specified(Index: Integer): boolean;
    procedure SetEmployeeLNChar(Index: Integer; const AWideString: WideString);
    function  EmployeeLNChar_Specified(Index: Integer): boolean;
    procedure SetHomeSiteCode(Index: Integer; const AWideString: WideString);
    function  HomeSiteCode_Specified(Index: Integer): boolean;
    procedure SetHomeDept(Index: Integer; const AWideString: WideString);
    function  HomeDept_Specified(Index: Integer): boolean;
    procedure SetHomeJob(Index: Integer; const AWideString: WideString);
    function  HomeJob_Specified(Index: Integer): boolean;
    procedure SetEEStatus(Index: Integer; const AWideString: WideString);
    function  EEStatus_Specified(Index: Integer): boolean;
    procedure SetSalaryOrHourly(Index: Integer; const AWideString: WideString);
    function  SalaryOrHourly_Specified(Index: Integer): boolean;
    procedure SetWorkedSiteCode(Index: Integer; const AWideString: WideString);
    function  WorkedSiteCode_Specified(Index: Integer): boolean;
    procedure SetWorkedDeptCode(Index: Integer; const AWideString: WideString);
    function  WorkedDeptCode_Specified(Index: Integer): boolean;
    procedure SetWorkedJobCode(Index: Integer; const AWideString: WideString);
    function  WorkedJobCode_Specified(Index: Integer): boolean;
    procedure SetEarningsCode(Index: Integer; const AWideString: WideString);
    function  EarningsCode_Specified(Index: Integer): boolean;
    procedure SetCustom1(Index: Integer; const AWideString: WideString);
    function  Custom1_Specified(Index: Integer): boolean;
    procedure SetCustom2(Index: Integer; const AWideString: WideString);
    function  Custom2_Specified(Index: Integer): boolean;
    procedure SetCustom3(Index: Integer; const AWideString: WideString);
    function  Custom3_Specified(Index: Integer): boolean;
    procedure SetCustom4(Index: Integer; const AWideString: WideString);
    function  Custom4_Specified(Index: Integer): boolean;
    procedure SetCustom5(Index: Integer; const AWideString: WideString);
    function  Custom5_Specified(Index: Integer): boolean;
    procedure SetCustom6(Index: Integer; const AWideString: WideString);
    function  Custom6_Specified(Index: Integer): boolean;
    procedure SetCustom7(Index: Integer; const AWideString: WideString);
    function  Custom7_Specified(Index: Integer): boolean;
    procedure SetCustom8(Index: Integer; const AWideString: WideString);
    function  Custom8_Specified(Index: Integer): boolean;
    procedure SetCustom9(Index: Integer; const AWideString: WideString);
    function  Custom9_Specified(Index: Integer): boolean;
    procedure SetCustom10(Index: Integer; const AWideString: WideString);
    function  Custom10_Specified(Index: Integer): boolean;
    procedure SetCustom11(Index: Integer; const AWideString: WideString);
    function  Custom11_Specified(Index: Integer): boolean;
    procedure SetCustom12(Index: Integer; const AWideString: WideString);
    function  Custom12_Specified(Index: Integer): boolean;
    procedure SetCustom13(Index: Integer; const AWideString: WideString);
    function  Custom13_Specified(Index: Integer): boolean;
    procedure SetCustom14(Index: Integer; const AWideString: WideString);
    function  Custom14_Specified(Index: Integer): boolean;
    procedure SetCustom15(Index: Integer; const AWideString: WideString);
    function  Custom15_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property RecordIDField:      WideString  Index (IS_OPTN) read FRecordIDField write SetRecordIDField stored RecordIDField_Specified;
    property EmployeeCode:       WideString  Index (IS_OPTN) read FEmployeeCode write SetEmployeeCode stored EmployeeCode_Specified;
    property EmployeeLNChar:     WideString  Index (IS_OPTN) read FEmployeeLNChar write SetEmployeeLNChar stored EmployeeLNChar_Specified;
    property HomeSiteCode:       WideString  Index (IS_OPTN) read FHomeSiteCode write SetHomeSiteCode stored HomeSiteCode_Specified;
    property HomeDept:           WideString  Index (IS_OPTN) read FHomeDept write SetHomeDept stored HomeDept_Specified;
    property HomeJob:            WideString  Index (IS_OPTN) read FHomeJob write SetHomeJob stored HomeJob_Specified;
    property EEStatus:           WideString  Index (IS_OPTN) read FEEStatus write SetEEStatus stored EEStatus_Specified;
    property SalaryOrHourly:     WideString  Index (IS_OPTN) read FSalaryOrHourly write SetSalaryOrHourly stored SalaryOrHourly_Specified;
    property WorkedSiteCode:     WideString  Index (IS_OPTN) read FWorkedSiteCode write SetWorkedSiteCode stored WorkedSiteCode_Specified;
    property WorkedDeptCode:     WideString  Index (IS_OPTN) read FWorkedDeptCode write SetWorkedDeptCode stored WorkedDeptCode_Specified;
    property WorkedJobCode:      WideString  Index (IS_OPTN) read FWorkedJobCode write SetWorkedJobCode stored WorkedJobCode_Specified;
    property DaysWorked:         Integer     read FDaysWorked write FDaysWorked;
    property RegHours:           TXSDecimal  read FRegHours write FRegHours;
    property RegRate:            TXSDecimal  read FRegRate write FRegRate;
    property OTHours:            TXSDecimal  read FOTHours write FOTHours;
    property OTRate:             TXSDecimal  read FOTRate write FOTRate;
    property OTFactor:           TXSDecimal  read FOTFactor write FOTFactor;
    property DTHours:            TXSDecimal  read FDTHours write FDTHours;
    property DTRate:             TXSDecimal  read FDTRate write FDTRate;
    property DTFactor:           TXSDecimal  read FDTFactor write FDTFactor;
    property EarningsCode:       WideString  Index (IS_OPTN) read FEarningsCode write SetEarningsCode stored EarningsCode_Specified;
    property EarningsHours:      TXSDecimal  read FEarningsHours write FEarningsHours;
    property EarningsCodeRate:   TXSDecimal  read FEarningsCodeRate write FEarningsCodeRate;
    property EarningsCodeAmount: TXSDecimal  read FEarningsCodeAmount write FEarningsCodeAmount;
    property Custom1:            WideString  Index (IS_OPTN) read FCustom1 write SetCustom1 stored Custom1_Specified;
    property Custom2:            WideString  Index (IS_OPTN) read FCustom2 write SetCustom2 stored Custom2_Specified;
    property Custom3:            WideString  Index (IS_OPTN) read FCustom3 write SetCustom3 stored Custom3_Specified;
    property Custom4:            WideString  Index (IS_OPTN) read FCustom4 write SetCustom4 stored Custom4_Specified;
    property Custom5:            WideString  Index (IS_OPTN) read FCustom5 write SetCustom5 stored Custom5_Specified;
    property Custom6:            WideString  Index (IS_OPTN) read FCustom6 write SetCustom6 stored Custom6_Specified;
    property Custom7:            WideString  Index (IS_OPTN) read FCustom7 write SetCustom7 stored Custom7_Specified;
    property Custom8:            WideString  Index (IS_OPTN) read FCustom8 write SetCustom8 stored Custom8_Specified;
    property Custom9:            WideString  Index (IS_OPTN) read FCustom9 write SetCustom9 stored Custom9_Specified;
    property Custom10:           WideString  Index (IS_OPTN) read FCustom10 write SetCustom10 stored Custom10_Specified;
    property Custom11:           WideString  Index (IS_OPTN) read FCustom11 write SetCustom11 stored Custom11_Specified;
    property Custom12:           WideString  Index (IS_OPTN) read FCustom12 write SetCustom12 stored Custom12_Specified;
    property Custom13:           WideString  Index (IS_OPTN) read FCustom13 write SetCustom13 stored Custom13_Specified;
    property Custom14:           WideString  Index (IS_OPTN) read FCustom14 write SetCustom14 stored Custom14_Specified;
    property Custom15:           WideString  Index (IS_OPTN) read FCustom15 write SetCustom15 stored Custom15_Specified;
  end;

  ArrayOfEmployee = array of Employee;          { "http://m3as.com/SPDS"[GblCplx] }


  // ************************************************************************ //
  // XML       : Employee, global, <complexType>
  // Namespace : http://m3as.com/SPDS
  // ************************************************************************ //
  Employee = class(TRemotable)
  private
    FEmployeeCode: WideString;
    FEmployeeCode_Specified: boolean;
    FHomeHourlyRate: TXSDecimal;
    FFirstName: WideString;
    FFirstName_Specified: boolean;
    FLastName: WideString;
    FLastName_Specified: boolean;
    FMiddleInitial: WideString;
    FMiddleInitial_Specified: boolean;
    FGender: WideString;
    FGender_Specified: boolean;
    FDateOfBirth: WideString;
    FDateOfBirth_Specified: boolean;
    FClockNo: WideString;
    FClockNo_Specified: boolean;
    FDateOfHire: WideString;
    FDateOfHire_Specified: boolean;
    FEffectiveDate: WideString;
    FEffectiveDate_Specified: boolean;
    FAddress: WideString;
    FAddress_Specified: boolean;
    FAddress2: WideString;
    FAddress2_Specified: boolean;
    FCity: WideString;
    FCity_Specified: boolean;
    FState: WideString;
    FState_Specified: boolean;
    FCountry: WideString;
    FCountry_Specified: boolean;
    FPostalCode: WideString;
    FPostalCode_Specified: boolean;
    FEmployeeStatus: WideString;
    FEmployeeStatus_Specified: boolean;
    FEmployeeType: WideString;
    FEmployeeType_Specified: boolean;
    FFullTimePartTime: WideString;
    FFullTimePartTime_Specified: boolean;
    FHrlySlryNonExemptFluc1: WideString;
    FHrlySlryNonExemptFluc1_Specified: boolean;
    FSSN: WideString;
    FSSN_Specified: boolean;
    FPayGroup: WideString;
    FPayGroup_Specified: boolean;
    FReviewDate: WideString;
    FReviewDate_Specified: boolean;
    FPhoneNumber: WideString;
    FPhoneNumber_Specified: boolean;
    FTerminationCode: WideString;
    FTerminationCode_Specified: boolean;
    FTerminationDate: WideString;
    FTerminationDate_Specified: boolean;
    FHomeDepartment: WideString;
    FHomeDepartment_Specified: boolean;
    FHomeJob: WideString;
    FHomeJob_Specified: boolean;
    FSiteCode2: WideString;
    FSiteCode2_Specified: boolean;
    FJobCode2: WideString;
    FJobCode2_Specified: boolean;
    FDepartment2: WideString;
    FDepartment2_Specified: boolean;
    FSiteCode3: WideString;
    FSiteCode3_Specified: boolean;
    FJobRate2: TXSDecimal;
    FJobCode3: WideString;
    FJobCode3_Specified: boolean;
    FDepartment3: WideString;
    FDepartment3_Specified: boolean;
    FJobRate3: TXSDecimal;
    FSiteCode4: WideString;
    FSiteCode4_Specified: boolean;
    FJobCode4: WideString;
    FJobCode4_Specified: boolean;
    FDepartment4: WideString;
    FDepartment4_Specified: boolean;
    FJobRate4: TXSDecimal;
    FSiteCode5: WideString;
    FSiteCode5_Specified: boolean;
    FJobCode5: WideString;
    FJobCode5_Specified: boolean;
    FDepartment5: WideString;
    FDepartment5_Specified: boolean;
    FJobRate5: TXSDecimal;
    FSiteCode6: WideString;
    FSiteCode6_Specified: boolean;
    FJobCode6: WideString;
    FJobCode6_Specified: boolean;
    FDepartment6: WideString;
    FDepartment6_Specified: boolean;
    FJobRate6: TXSDecimal;
    FSiteCode7: WideString;
    FSiteCode7_Specified: boolean;
    FJobCode7: WideString;
    FJobCode7_Specified: boolean;
    FDepartment7: WideString;
    FDepartment7_Specified: boolean;
    FJobRate7: TXSDecimal;
    FSiteCode8: WideString;
    FSiteCode8_Specified: boolean;
    FJobCode8: WideString;
    FJobCode8_Specified: boolean;
    FDepartment8: WideString;
    FDepartment8_Specified: boolean;
    FJobRate8: TXSDecimal;
    FSiteCode9: WideString;
    FSiteCode9_Specified: boolean;
    FJobCode9: WideString;
    FJobCode9_Specified: boolean;
    FDepartment9: WideString;
    FDepartment9_Specified: boolean;
    FJobRate9: TXSDecimal;
    FSiteCode10: WideString;
    FSiteCode10_Specified: boolean;
    FJobCode10: WideString;
    FJobCode10_Specified: boolean;
    FDepartment10: WideString;
    FDepartment10_Specified: boolean;
    FJobRate10: TXSDecimal;
    FSiteCode11: WideString;
    FSiteCode11_Specified: boolean;
    FJobCode11: WideString;
    FJobCode11_Specified: boolean;
    FDepartment11: WideString;
    FDepartment11_Specified: boolean;
    FJobRate11: TXSDecimal;
    FSiteCode12: WideString;
    FSiteCode12_Specified: boolean;
    FJobCode12: WideString;
    FJobCode12_Specified: boolean;
    FDepartment12: WideString;
    FDepartment12_Specified: boolean;
    FJobRate12: TXSDecimal;
    procedure SetEmployeeCode(Index: Integer; const AWideString: WideString);
    function  EmployeeCode_Specified(Index: Integer): boolean;
    procedure SetFirstName(Index: Integer; const AWideString: WideString);
    function  FirstName_Specified(Index: Integer): boolean;
    procedure SetLastName(Index: Integer; const AWideString: WideString);
    function  LastName_Specified(Index: Integer): boolean;
    procedure SetMiddleInitial(Index: Integer; const AWideString: WideString);
    function  MiddleInitial_Specified(Index: Integer): boolean;
    procedure SetGender(Index: Integer; const AWideString: WideString);
    function  Gender_Specified(Index: Integer): boolean;
    procedure SetDateOfBirth(Index: Integer; const AWideString: WideString);
    function  DateOfBirth_Specified(Index: Integer): boolean;
    procedure SetClockNo(Index: Integer; const AWideString: WideString);
    function  ClockNo_Specified(Index: Integer): boolean;
    procedure SetDateOfHire(Index: Integer; const AWideString: WideString);
    function  DateOfHire_Specified(Index: Integer): boolean;
    procedure SetEffectiveDate(Index: Integer; const AWideString: WideString);
    function  EffectiveDate_Specified(Index: Integer): boolean;
    procedure SetAddress(Index: Integer; const AWideString: WideString);
    function  Address_Specified(Index: Integer): boolean;
    procedure SetAddress2(Index: Integer; const AWideString: WideString);
    function  Address2_Specified(Index: Integer): boolean;
    procedure SetCity(Index: Integer; const AWideString: WideString);
    function  City_Specified(Index: Integer): boolean;
    procedure SetState(Index: Integer; const AWideString: WideString);
    function  State_Specified(Index: Integer): boolean;
    procedure SetCountry(Index: Integer; const AWideString: WideString);
    function  Country_Specified(Index: Integer): boolean;
    procedure SetPostalCode(Index: Integer; const AWideString: WideString);
    function  PostalCode_Specified(Index: Integer): boolean;
    procedure SetEmployeeStatus(Index: Integer; const AWideString: WideString);
    function  EmployeeStatus_Specified(Index: Integer): boolean;
    procedure SetEmployeeType(Index: Integer; const AWideString: WideString);
    function  EmployeeType_Specified(Index: Integer): boolean;
    procedure SetFullTimePartTime(Index: Integer; const AWideString: WideString);
    function  FullTimePartTime_Specified(Index: Integer): boolean;
    procedure SetHrlySlryNonExemptFluc1(Index: Integer; const AWideString: WideString);
    function  HrlySlryNonExemptFluc1_Specified(Index: Integer): boolean;
    procedure SetSSN(Index: Integer; const AWideString: WideString);
    function  SSN_Specified(Index: Integer): boolean;
    procedure SetPayGroup(Index: Integer; const AWideString: WideString);
    function  PayGroup_Specified(Index: Integer): boolean;
    procedure SetReviewDate(Index: Integer; const AWideString: WideString);
    function  ReviewDate_Specified(Index: Integer): boolean;
    procedure SetPhoneNumber(Index: Integer; const AWideString: WideString);
    function  PhoneNumber_Specified(Index: Integer): boolean;
    procedure SetTerminationCode(Index: Integer; const AWideString: WideString);
    function  TerminationCode_Specified(Index: Integer): boolean;
    procedure SetTerminationDate(Index: Integer; const AWideString: WideString);
    function  TerminationDate_Specified(Index: Integer): boolean;
    procedure SetHomeDepartment(Index: Integer; const AWideString: WideString);
    function  HomeDepartment_Specified(Index: Integer): boolean;
    procedure SetHomeJob(Index: Integer; const AWideString: WideString);
    function  HomeJob_Specified(Index: Integer): boolean;
    procedure SetSiteCode2(Index: Integer; const AWideString: WideString);
    function  SiteCode2_Specified(Index: Integer): boolean;
    procedure SetJobCode2(Index: Integer; const AWideString: WideString);
    function  JobCode2_Specified(Index: Integer): boolean;
    procedure SetDepartment2(Index: Integer; const AWideString: WideString);
    function  Department2_Specified(Index: Integer): boolean;
    procedure SetSiteCode3(Index: Integer; const AWideString: WideString);
    function  SiteCode3_Specified(Index: Integer): boolean;
    procedure SetJobCode3(Index: Integer; const AWideString: WideString);
    function  JobCode3_Specified(Index: Integer): boolean;
    procedure SetDepartment3(Index: Integer; const AWideString: WideString);
    function  Department3_Specified(Index: Integer): boolean;
    procedure SetSiteCode4(Index: Integer; const AWideString: WideString);
    function  SiteCode4_Specified(Index: Integer): boolean;
    procedure SetJobCode4(Index: Integer; const AWideString: WideString);
    function  JobCode4_Specified(Index: Integer): boolean;
    procedure SetDepartment4(Index: Integer; const AWideString: WideString);
    function  Department4_Specified(Index: Integer): boolean;
    procedure SetSiteCode5(Index: Integer; const AWideString: WideString);
    function  SiteCode5_Specified(Index: Integer): boolean;
    procedure SetJobCode5(Index: Integer; const AWideString: WideString);
    function  JobCode5_Specified(Index: Integer): boolean;
    procedure SetDepartment5(Index: Integer; const AWideString: WideString);
    function  Department5_Specified(Index: Integer): boolean;
    procedure SetSiteCode6(Index: Integer; const AWideString: WideString);
    function  SiteCode6_Specified(Index: Integer): boolean;
    procedure SetJobCode6(Index: Integer; const AWideString: WideString);
    function  JobCode6_Specified(Index: Integer): boolean;
    procedure SetDepartment6(Index: Integer; const AWideString: WideString);
    function  Department6_Specified(Index: Integer): boolean;
    procedure SetSiteCode7(Index: Integer; const AWideString: WideString);
    function  SiteCode7_Specified(Index: Integer): boolean;
    procedure SetJobCode7(Index: Integer; const AWideString: WideString);
    function  JobCode7_Specified(Index: Integer): boolean;
    procedure SetDepartment7(Index: Integer; const AWideString: WideString);
    function  Department7_Specified(Index: Integer): boolean;
    procedure SetSiteCode8(Index: Integer; const AWideString: WideString);
    function  SiteCode8_Specified(Index: Integer): boolean;
    procedure SetJobCode8(Index: Integer; const AWideString: WideString);
    function  JobCode8_Specified(Index: Integer): boolean;
    procedure SetDepartment8(Index: Integer; const AWideString: WideString);
    function  Department8_Specified(Index: Integer): boolean;
    procedure SetSiteCode9(Index: Integer; const AWideString: WideString);
    function  SiteCode9_Specified(Index: Integer): boolean;
    procedure SetJobCode9(Index: Integer; const AWideString: WideString);
    function  JobCode9_Specified(Index: Integer): boolean;
    procedure SetDepartment9(Index: Integer; const AWideString: WideString);
    function  Department9_Specified(Index: Integer): boolean;
    procedure SetSiteCode10(Index: Integer; const AWideString: WideString);
    function  SiteCode10_Specified(Index: Integer): boolean;
    procedure SetJobCode10(Index: Integer; const AWideString: WideString);
    function  JobCode10_Specified(Index: Integer): boolean;
    procedure SetDepartment10(Index: Integer; const AWideString: WideString);
    function  Department10_Specified(Index: Integer): boolean;
    procedure SetSiteCode11(Index: Integer; const AWideString: WideString);
    function  SiteCode11_Specified(Index: Integer): boolean;
    procedure SetJobCode11(Index: Integer; const AWideString: WideString);
    function  JobCode11_Specified(Index: Integer): boolean;
    procedure SetDepartment11(Index: Integer; const AWideString: WideString);
    function  Department11_Specified(Index: Integer): boolean;
    procedure SetSiteCode12(Index: Integer; const AWideString: WideString);
    function  SiteCode12_Specified(Index: Integer): boolean;
    procedure SetJobCode12(Index: Integer; const AWideString: WideString);
    function  JobCode12_Specified(Index: Integer): boolean;
    procedure SetDepartment12(Index: Integer; const AWideString: WideString);
    function  Department12_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property EmployeeCode:           WideString  Index (IS_OPTN) read FEmployeeCode write SetEmployeeCode stored EmployeeCode_Specified;
    property HomeHourlyRate:         TXSDecimal  read FHomeHourlyRate write FHomeHourlyRate;
    property FirstName:              WideString  Index (IS_OPTN) read FFirstName write SetFirstName stored FirstName_Specified;
    property LastName:               WideString  Index (IS_OPTN) read FLastName write SetLastName stored LastName_Specified;
    property MiddleInitial:          WideString  Index (IS_OPTN) read FMiddleInitial write SetMiddleInitial stored MiddleInitial_Specified;
    property Gender:                 WideString  Index (IS_OPTN) read FGender write SetGender stored Gender_Specified;
    property DateOfBirth:            WideString  Index (IS_OPTN) read FDateOfBirth write SetDateOfBirth stored DateOfBirth_Specified;
    property ClockNo:                WideString  Index (IS_OPTN) read FClockNo write SetClockNo stored ClockNo_Specified;
    property DateOfHire:             WideString  Index (IS_OPTN) read FDateOfHire write SetDateOfHire stored DateOfHire_Specified;
    property EffectiveDate:          WideString  Index (IS_OPTN) read FEffectiveDate write SetEffectiveDate stored EffectiveDate_Specified;
    property Address:                WideString  Index (IS_OPTN) read FAddress write SetAddress stored Address_Specified;
    property Address2:               WideString  Index (IS_OPTN) read FAddress2 write SetAddress2 stored Address2_Specified;
    property City:                   WideString  Index (IS_OPTN) read FCity write SetCity stored City_Specified;
    property State:                  WideString  Index (IS_OPTN) read FState write SetState stored State_Specified;
    property Country:                WideString  Index (IS_OPTN) read FCountry write SetCountry stored Country_Specified;
    property PostalCode:             WideString  Index (IS_OPTN) read FPostalCode write SetPostalCode stored PostalCode_Specified;
    property EmployeeStatus:         WideString  Index (IS_OPTN) read FEmployeeStatus write SetEmployeeStatus stored EmployeeStatus_Specified;
    property EmployeeType:           WideString  Index (IS_OPTN) read FEmployeeType write SetEmployeeType stored EmployeeType_Specified;
    property FullTimePartTime:       WideString  Index (IS_OPTN) read FFullTimePartTime write SetFullTimePartTime stored FullTimePartTime_Specified;
    property HrlySlryNonExemptFluc1: WideString  Index (IS_OPTN) read FHrlySlryNonExemptFluc1 write SetHrlySlryNonExemptFluc1 stored HrlySlryNonExemptFluc1_Specified;
    property SSN:                    WideString  Index (IS_OPTN) read FSSN write SetSSN stored SSN_Specified;
    property PayGroup:               WideString  Index (IS_OPTN) read FPayGroup write SetPayGroup stored PayGroup_Specified;
    property ReviewDate:             WideString  Index (IS_OPTN) read FReviewDate write SetReviewDate stored ReviewDate_Specified;
    property PhoneNumber:            WideString  Index (IS_OPTN) read FPhoneNumber write SetPhoneNumber stored PhoneNumber_Specified;
    property TerminationCode:        WideString  Index (IS_OPTN) read FTerminationCode write SetTerminationCode stored TerminationCode_Specified;
    property TerminationDate:        WideString  Index (IS_OPTN) read FTerminationDate write SetTerminationDate stored TerminationDate_Specified;
    property HomeDepartment:         WideString  Index (IS_OPTN) read FHomeDepartment write SetHomeDepartment stored HomeDepartment_Specified;
    property HomeJob:                WideString  Index (IS_OPTN) read FHomeJob write SetHomeJob stored HomeJob_Specified;
    property SiteCode2:              WideString  Index (IS_OPTN) read FSiteCode2 write SetSiteCode2 stored SiteCode2_Specified;
    property JobCode2:               WideString  Index (IS_OPTN) read FJobCode2 write SetJobCode2 stored JobCode2_Specified;
    property Department2:            WideString  Index (IS_OPTN) read FDepartment2 write SetDepartment2 stored Department2_Specified;
    property SiteCode3:              WideString  Index (IS_OPTN) read FSiteCode3 write SetSiteCode3 stored SiteCode3_Specified;
    property JobRate2:               TXSDecimal  Index (IS_NLBL) read FJobRate2 write FJobRate2;
    property JobCode3:               WideString  Index (IS_OPTN) read FJobCode3 write SetJobCode3 stored JobCode3_Specified;
    property Department3:            WideString  Index (IS_OPTN) read FDepartment3 write SetDepartment3 stored Department3_Specified;
    property JobRate3:               TXSDecimal  Index (IS_NLBL) read FJobRate3 write FJobRate3;
    property SiteCode4:              WideString  Index (IS_OPTN) read FSiteCode4 write SetSiteCode4 stored SiteCode4_Specified;
    property JobCode4:               WideString  Index (IS_OPTN) read FJobCode4 write SetJobCode4 stored JobCode4_Specified;
    property Department4:            WideString  Index (IS_OPTN) read FDepartment4 write SetDepartment4 stored Department4_Specified;
    property JobRate4:               TXSDecimal  Index (IS_NLBL) read FJobRate4 write FJobRate4;
    property SiteCode5:              WideString  Index (IS_OPTN) read FSiteCode5 write SetSiteCode5 stored SiteCode5_Specified;
    property JobCode5:               WideString  Index (IS_OPTN) read FJobCode5 write SetJobCode5 stored JobCode5_Specified;
    property Department5:            WideString  Index (IS_OPTN) read FDepartment5 write SetDepartment5 stored Department5_Specified;
    property JobRate5:               TXSDecimal  Index (IS_NLBL) read FJobRate5 write FJobRate5;
    property SiteCode6:              WideString  Index (IS_OPTN) read FSiteCode6 write SetSiteCode6 stored SiteCode6_Specified;
    property JobCode6:               WideString  Index (IS_OPTN) read FJobCode6 write SetJobCode6 stored JobCode6_Specified;
    property Department6:            WideString  Index (IS_OPTN) read FDepartment6 write SetDepartment6 stored Department6_Specified;
    property JobRate6:               TXSDecimal  Index (IS_NLBL) read FJobRate6 write FJobRate6;
    property SiteCode7:              WideString  Index (IS_OPTN) read FSiteCode7 write SetSiteCode7 stored SiteCode7_Specified;
    property JobCode7:               WideString  Index (IS_OPTN) read FJobCode7 write SetJobCode7 stored JobCode7_Specified;
    property Department7:            WideString  Index (IS_OPTN) read FDepartment7 write SetDepartment7 stored Department7_Specified;
    property JobRate7:               TXSDecimal  Index (IS_NLBL) read FJobRate7 write FJobRate7;
    property SiteCode8:              WideString  Index (IS_OPTN) read FSiteCode8 write SetSiteCode8 stored SiteCode8_Specified;
    property JobCode8:               WideString  Index (IS_OPTN) read FJobCode8 write SetJobCode8 stored JobCode8_Specified;
    property Department8:            WideString  Index (IS_OPTN) read FDepartment8 write SetDepartment8 stored Department8_Specified;
    property JobRate8:               TXSDecimal  Index (IS_NLBL) read FJobRate8 write FJobRate8;
    property SiteCode9:              WideString  Index (IS_OPTN) read FSiteCode9 write SetSiteCode9 stored SiteCode9_Specified;
    property JobCode9:               WideString  Index (IS_OPTN) read FJobCode9 write SetJobCode9 stored JobCode9_Specified;
    property Department9:            WideString  Index (IS_OPTN) read FDepartment9 write SetDepartment9 stored Department9_Specified;
    property JobRate9:               TXSDecimal  Index (IS_NLBL) read FJobRate9 write FJobRate9;
    property SiteCode10:             WideString  Index (IS_OPTN) read FSiteCode10 write SetSiteCode10 stored SiteCode10_Specified;
    property JobCode10:              WideString  Index (IS_OPTN) read FJobCode10 write SetJobCode10 stored JobCode10_Specified;
    property Department10:           WideString  Index (IS_OPTN) read FDepartment10 write SetDepartment10 stored Department10_Specified;
    property JobRate10:              TXSDecimal  Index (IS_NLBL) read FJobRate10 write FJobRate10;
    property SiteCode11:             WideString  Index (IS_OPTN) read FSiteCode11 write SetSiteCode11 stored SiteCode11_Specified;
    property JobCode11:              WideString  Index (IS_OPTN) read FJobCode11 write SetJobCode11 stored JobCode11_Specified;
    property Department11:           WideString  Index (IS_OPTN) read FDepartment11 write SetDepartment11 stored Department11_Specified;
    property JobRate11:              TXSDecimal  Index (IS_NLBL) read FJobRate11 write FJobRate11;
    property SiteCode12:             WideString  Index (IS_OPTN) read FSiteCode12 write SetSiteCode12 stored SiteCode12_Specified;
    property JobCode12:              WideString  Index (IS_OPTN) read FJobCode12 write SetJobCode12 stored JobCode12_Specified;
    property Department12:           WideString  Index (IS_OPTN) read FDepartment12 write SetDepartment12 stored Department12_Specified;
    property JobRate12:              TXSDecimal  Index (IS_NLBL) read FJobRate12 write FJobRate12;
  end;

  ArrayOfImportDetails = array of ImportDetails;   { "http://m3as.com/SPDS"[GblCplx] }


  // ************************************************************************ //
  // XML       : ImportDetails, global, <complexType>
  // Namespace : http://m3as.com/SPDS
  // ************************************************************************ //
  ImportDetails = class(TRemotable)
  private
    FOrganization: WideString;
    FOrganization_Specified: boolean;
    FHomeSite: WideString;
    FHomeSite_Specified: boolean;
    FEmployeeCode: WideString;
    FEmployeeCode_Specified: boolean;
    FFirstName: WideString;
    FFirstName_Specified: boolean;
    FLastName: WideString;
    FLastName_Specified: boolean;
    FStatus: WideString;
    FStatus_Specified: boolean;
    FDetail: WideString;
    FDetail_Specified: boolean;
    procedure SetOrganization(Index: Integer; const AWideString: WideString);
    function  Organization_Specified(Index: Integer): boolean;
    procedure SetHomeSite(Index: Integer; const AWideString: WideString);
    function  HomeSite_Specified(Index: Integer): boolean;
    procedure SetEmployeeCode(Index: Integer; const AWideString: WideString);
    function  EmployeeCode_Specified(Index: Integer): boolean;
    procedure SetFirstName(Index: Integer; const AWideString: WideString);
    function  FirstName_Specified(Index: Integer): boolean;
    procedure SetLastName(Index: Integer; const AWideString: WideString);
    function  LastName_Specified(Index: Integer): boolean;
    procedure SetStatus(Index: Integer; const AWideString: WideString);
    function  Status_Specified(Index: Integer): boolean;
    procedure SetDetail(Index: Integer; const AWideString: WideString);
    function  Detail_Specified(Index: Integer): boolean;
  published
    property Organization: WideString  Index (IS_OPTN) read FOrganization write SetOrganization stored Organization_Specified;
    property HomeSite:     WideString  Index (IS_OPTN) read FHomeSite write SetHomeSite stored HomeSite_Specified;
    property EmployeeCode: WideString  Index (IS_OPTN) read FEmployeeCode write SetEmployeeCode stored EmployeeCode_Specified;
    property FirstName:    WideString  Index (IS_OPTN) read FFirstName write SetFirstName stored FirstName_Specified;
    property LastName:     WideString  Index (IS_OPTN) read FLastName write SetLastName stored LastName_Specified;
    property Status:       WideString  Index (IS_OPTN) read FStatus write SetStatus stored Status_Specified;
    property Detail:       WideString  Index (IS_OPTN) read FDetail write SetDetail stored Detail_Specified;
  end;


  // ************************************************************************ //
  // Namespace : http://m3as.com/SPDS
  // soapAction: http://m3as.com/SPDS/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : SecurePayrollDataService1Soap
  // service   : SecurePayrollDataService1
  // port      : SecurePayrollDataService1Soap
  // URL       : https://qaservice.m3righttime.com/SecurePayrollDataService.asmx
  // ************************************************************************ //
  SecurePayrollDataService1Soap = interface(IInvokable)
  ['{6DD87AEB-5CC5-6C79-7875-7C8A395152F4}']
    function  ExportEarnings(const username: WideString; const password: WideString; const organizationCode: WideString; const siteCode: WideString; const payGroupCode: WideString; const startDate: TXSDateTime
                             ): ArrayOfExportDetails; stdcall;
    function  ImportEmployees(const username: WideString; const password: WideString; const organizationCode: WideString; const siteCode: WideString; const employees: ArrayOfEmployee): ArrayOfImportDetails; stdcall;
  end;

function GetSecurePayrollDataService1Soap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): SecurePayrollDataService1Soap;


implementation
  uses SysUtils;

function GetSecurePayrollDataService1Soap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): SecurePayrollDataService1Soap;
const
  defWSDL = 'https://qaservice.m3righttime.com/SecurePayrollDataService.asmx?wsdl';
  defURL  = 'https://qaservice.m3righttime.com/SecurePayrollDataService.asmx';
  defSvc  = 'SecurePayrollDataService1';
  defPrt  = 'SecurePayrollDataService1Soap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as SecurePayrollDataService1Soap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor ExportDetails.Destroy;
begin
  FreeAndNil(FRegHours);
  FreeAndNil(FRegRate);
  FreeAndNil(FOTHours);
  FreeAndNil(FOTRate);
  FreeAndNil(FOTFactor);
  FreeAndNil(FDTHours);
  FreeAndNil(FDTRate);
  FreeAndNil(FDTFactor);
  FreeAndNil(FEarningsHours);
  FreeAndNil(FEarningsCodeRate);
  FreeAndNil(FEarningsCodeAmount);
  inherited Destroy;
end;

procedure ExportDetails.SetRecordIDField(Index: Integer; const AWideString: WideString);
begin
  FRecordIDField := AWideString;
  FRecordIDField_Specified := True;
end;

function ExportDetails.RecordIDField_Specified(Index: Integer): boolean;
begin
  Result := FRecordIDField_Specified;
end;

procedure ExportDetails.SetEmployeeCode(Index: Integer; const AWideString: WideString);
begin
  FEmployeeCode := AWideString;
  FEmployeeCode_Specified := True;
end;

function ExportDetails.EmployeeCode_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeCode_Specified;
end;

procedure ExportDetails.SetEmployeeLNChar(Index: Integer; const AWideString: WideString);
begin
  FEmployeeLNChar := AWideString;
  FEmployeeLNChar_Specified := True;
end;

function ExportDetails.EmployeeLNChar_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeLNChar_Specified;
end;

procedure ExportDetails.SetHomeSiteCode(Index: Integer; const AWideString: WideString);
begin
  FHomeSiteCode := AWideString;
  FHomeSiteCode_Specified := True;
end;

function ExportDetails.HomeSiteCode_Specified(Index: Integer): boolean;
begin
  Result := FHomeSiteCode_Specified;
end;

procedure ExportDetails.SetHomeDept(Index: Integer; const AWideString: WideString);
begin
  FHomeDept := AWideString;
  FHomeDept_Specified := True;
end;

function ExportDetails.HomeDept_Specified(Index: Integer): boolean;
begin
  Result := FHomeDept_Specified;
end;

procedure ExportDetails.SetHomeJob(Index: Integer; const AWideString: WideString);
begin
  FHomeJob := AWideString;
  FHomeJob_Specified := True;
end;

function ExportDetails.HomeJob_Specified(Index: Integer): boolean;
begin
  Result := FHomeJob_Specified;
end;

procedure ExportDetails.SetEEStatus(Index: Integer; const AWideString: WideString);
begin
  FEEStatus := AWideString;
  FEEStatus_Specified := True;
end;

function ExportDetails.EEStatus_Specified(Index: Integer): boolean;
begin
  Result := FEEStatus_Specified;
end;

procedure ExportDetails.SetSalaryOrHourly(Index: Integer; const AWideString: WideString);
begin
  FSalaryOrHourly := AWideString;
  FSalaryOrHourly_Specified := True;
end;

function ExportDetails.SalaryOrHourly_Specified(Index: Integer): boolean;
begin
  Result := FSalaryOrHourly_Specified;
end;

procedure ExportDetails.SetWorkedSiteCode(Index: Integer; const AWideString: WideString);
begin
  FWorkedSiteCode := AWideString;
  FWorkedSiteCode_Specified := True;
end;

function ExportDetails.WorkedSiteCode_Specified(Index: Integer): boolean;
begin
  Result := FWorkedSiteCode_Specified;
end;

procedure ExportDetails.SetWorkedDeptCode(Index: Integer; const AWideString: WideString);
begin
  FWorkedDeptCode := AWideString;
  FWorkedDeptCode_Specified := True;
end;

function ExportDetails.WorkedDeptCode_Specified(Index: Integer): boolean;
begin
  Result := FWorkedDeptCode_Specified;
end;

procedure ExportDetails.SetWorkedJobCode(Index: Integer; const AWideString: WideString);
begin
  FWorkedJobCode := AWideString;
  FWorkedJobCode_Specified := True;
end;

function ExportDetails.WorkedJobCode_Specified(Index: Integer): boolean;
begin
  Result := FWorkedJobCode_Specified;
end;

procedure ExportDetails.SetEarningsCode(Index: Integer; const AWideString: WideString);
begin
  FEarningsCode := AWideString;
  FEarningsCode_Specified := True;
end;

function ExportDetails.EarningsCode_Specified(Index: Integer): boolean;
begin
  Result := FEarningsCode_Specified;
end;

procedure ExportDetails.SetCustom1(Index: Integer; const AWideString: WideString);
begin
  FCustom1 := AWideString;
  FCustom1_Specified := True;
end;

function ExportDetails.Custom1_Specified(Index: Integer): boolean;
begin
  Result := FCustom1_Specified;
end;

procedure ExportDetails.SetCustom2(Index: Integer; const AWideString: WideString);
begin
  FCustom2 := AWideString;
  FCustom2_Specified := True;
end;

function ExportDetails.Custom2_Specified(Index: Integer): boolean;
begin
  Result := FCustom2_Specified;
end;

procedure ExportDetails.SetCustom3(Index: Integer; const AWideString: WideString);
begin
  FCustom3 := AWideString;
  FCustom3_Specified := True;
end;

function ExportDetails.Custom3_Specified(Index: Integer): boolean;
begin
  Result := FCustom3_Specified;
end;

procedure ExportDetails.SetCustom4(Index: Integer; const AWideString: WideString);
begin
  FCustom4 := AWideString;
  FCustom4_Specified := True;
end;

function ExportDetails.Custom4_Specified(Index: Integer): boolean;
begin
  Result := FCustom4_Specified;
end;

procedure ExportDetails.SetCustom5(Index: Integer; const AWideString: WideString);
begin
  FCustom5 := AWideString;
  FCustom5_Specified := True;
end;

function ExportDetails.Custom5_Specified(Index: Integer): boolean;
begin
  Result := FCustom5_Specified;
end;

procedure ExportDetails.SetCustom6(Index: Integer; const AWideString: WideString);
begin
  FCustom6 := AWideString;
  FCustom6_Specified := True;
end;

function ExportDetails.Custom6_Specified(Index: Integer): boolean;
begin
  Result := FCustom6_Specified;
end;

procedure ExportDetails.SetCustom7(Index: Integer; const AWideString: WideString);
begin
  FCustom7 := AWideString;
  FCustom7_Specified := True;
end;

function ExportDetails.Custom7_Specified(Index: Integer): boolean;
begin
  Result := FCustom7_Specified;
end;

procedure ExportDetails.SetCustom8(Index: Integer; const AWideString: WideString);
begin
  FCustom8 := AWideString;
  FCustom8_Specified := True;
end;

function ExportDetails.Custom8_Specified(Index: Integer): boolean;
begin
  Result := FCustom8_Specified;
end;

procedure ExportDetails.SetCustom9(Index: Integer; const AWideString: WideString);
begin
  FCustom9 := AWideString;
  FCustom9_Specified := True;
end;

function ExportDetails.Custom9_Specified(Index: Integer): boolean;
begin
  Result := FCustom9_Specified;
end;

procedure ExportDetails.SetCustom10(Index: Integer; const AWideString: WideString);
begin
  FCustom10 := AWideString;
  FCustom10_Specified := True;
end;

function ExportDetails.Custom10_Specified(Index: Integer): boolean;
begin
  Result := FCustom10_Specified;
end;

procedure ExportDetails.SetCustom11(Index: Integer; const AWideString: WideString);
begin
  FCustom11 := AWideString;
  FCustom11_Specified := True;
end;

function ExportDetails.Custom11_Specified(Index: Integer): boolean;
begin
  Result := FCustom11_Specified;
end;

procedure ExportDetails.SetCustom12(Index: Integer; const AWideString: WideString);
begin
  FCustom12 := AWideString;
  FCustom12_Specified := True;
end;

function ExportDetails.Custom12_Specified(Index: Integer): boolean;
begin
  Result := FCustom12_Specified;
end;

procedure ExportDetails.SetCustom13(Index: Integer; const AWideString: WideString);
begin
  FCustom13 := AWideString;
  FCustom13_Specified := True;
end;

function ExportDetails.Custom13_Specified(Index: Integer): boolean;
begin
  Result := FCustom13_Specified;
end;

procedure ExportDetails.SetCustom14(Index: Integer; const AWideString: WideString);
begin
  FCustom14 := AWideString;
  FCustom14_Specified := True;
end;

function ExportDetails.Custom14_Specified(Index: Integer): boolean;
begin
  Result := FCustom14_Specified;
end;

procedure ExportDetails.SetCustom15(Index: Integer; const AWideString: WideString);
begin
  FCustom15 := AWideString;
  FCustom15_Specified := True;
end;

function ExportDetails.Custom15_Specified(Index: Integer): boolean;
begin
  Result := FCustom15_Specified;
end;

destructor Employee.Destroy;
begin
  FreeAndNil(FHomeHourlyRate);
  FreeAndNil(FJobRate2);
  FreeAndNil(FJobRate3);
  FreeAndNil(FJobRate4);
  FreeAndNil(FJobRate5);
  FreeAndNil(FJobRate6);
  FreeAndNil(FJobRate7);
  FreeAndNil(FJobRate8);
  FreeAndNil(FJobRate9);
  FreeAndNil(FJobRate10);
  FreeAndNil(FJobRate11);
  FreeAndNil(FJobRate12);
  inherited Destroy;
end;

procedure Employee.SetEmployeeCode(Index: Integer; const AWideString: WideString);
begin
  FEmployeeCode := AWideString;
  FEmployeeCode_Specified := True;
end;

function Employee.EmployeeCode_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeCode_Specified;
end;

procedure Employee.SetFirstName(Index: Integer; const AWideString: WideString);
begin
  FFirstName := AWideString;
  FFirstName_Specified := True;
end;

function Employee.FirstName_Specified(Index: Integer): boolean;
begin
  Result := FFirstName_Specified;
end;

procedure Employee.SetLastName(Index: Integer; const AWideString: WideString);
begin
  FLastName := AWideString;
  FLastName_Specified := True;
end;

function Employee.LastName_Specified(Index: Integer): boolean;
begin
  Result := FLastName_Specified;
end;

procedure Employee.SetMiddleInitial(Index: Integer; const AWideString: WideString);
begin
  FMiddleInitial := AWideString;
  FMiddleInitial_Specified := True;
end;

function Employee.MiddleInitial_Specified(Index: Integer): boolean;
begin
  Result := FMiddleInitial_Specified;
end;

procedure Employee.SetGender(Index: Integer; const AWideString: WideString);
begin
  FGender := AWideString;
  FGender_Specified := True;
end;

function Employee.Gender_Specified(Index: Integer): boolean;
begin
  Result := FGender_Specified;
end;

procedure Employee.SetDateOfBirth(Index: Integer; const AWideString: WideString);
begin
  FDateOfBirth := AWideString;
  FDateOfBirth_Specified := True;
end;

function Employee.DateOfBirth_Specified(Index: Integer): boolean;
begin
  Result := FDateOfBirth_Specified;
end;

procedure Employee.SetClockNo(Index: Integer; const AWideString: WideString);
begin
  FClockNo := AWideString;
  FClockNo_Specified := True;
end;

function Employee.ClockNo_Specified(Index: Integer): boolean;
begin
  Result := FClockNo_Specified;
end;

procedure Employee.SetDateOfHire(Index: Integer; const AWideString: WideString);
begin
  FDateOfHire := AWideString;
  FDateOfHire_Specified := True;
end;

function Employee.DateOfHire_Specified(Index: Integer): boolean;
begin
  Result := FDateOfHire_Specified;
end;

procedure Employee.SetEffectiveDate(Index: Integer; const AWideString: WideString);
begin
  FEffectiveDate := AWideString;
  FEffectiveDate_Specified := True;
end;

function Employee.EffectiveDate_Specified(Index: Integer): boolean;
begin
  Result := FEffectiveDate_Specified;
end;

procedure Employee.SetAddress(Index: Integer; const AWideString: WideString);
begin
  FAddress := AWideString;
  FAddress_Specified := True;
end;

function Employee.Address_Specified(Index: Integer): boolean;
begin
  Result := FAddress_Specified;
end;

procedure Employee.SetAddress2(Index: Integer; const AWideString: WideString);
begin
  FAddress2 := AWideString;
  FAddress2_Specified := True;
end;

function Employee.Address2_Specified(Index: Integer): boolean;
begin
  Result := FAddress2_Specified;
end;

procedure Employee.SetCity(Index: Integer; const AWideString: WideString);
begin
  FCity := AWideString;
  FCity_Specified := True;
end;

function Employee.City_Specified(Index: Integer): boolean;
begin
  Result := FCity_Specified;
end;

procedure Employee.SetState(Index: Integer; const AWideString: WideString);
begin
  FState := AWideString;
  FState_Specified := True;
end;

function Employee.State_Specified(Index: Integer): boolean;
begin
  Result := FState_Specified;
end;

procedure Employee.SetCountry(Index: Integer; const AWideString: WideString);
begin
  FCountry := AWideString;
  FCountry_Specified := True;
end;

function Employee.Country_Specified(Index: Integer): boolean;
begin
  Result := FCountry_Specified;
end;

procedure Employee.SetPostalCode(Index: Integer; const AWideString: WideString);
begin
  FPostalCode := AWideString;
  FPostalCode_Specified := True;
end;

function Employee.PostalCode_Specified(Index: Integer): boolean;
begin
  Result := FPostalCode_Specified;
end;

procedure Employee.SetEmployeeStatus(Index: Integer; const AWideString: WideString);
begin
  FEmployeeStatus := AWideString;
  FEmployeeStatus_Specified := True;
end;

function Employee.EmployeeStatus_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeStatus_Specified;
end;

procedure Employee.SetEmployeeType(Index: Integer; const AWideString: WideString);
begin
  FEmployeeType := AWideString;
  FEmployeeType_Specified := True;
end;

function Employee.EmployeeType_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeType_Specified;
end;

procedure Employee.SetFullTimePartTime(Index: Integer; const AWideString: WideString);
begin
  FFullTimePartTime := AWideString;
  FFullTimePartTime_Specified := True;
end;

function Employee.FullTimePartTime_Specified(Index: Integer): boolean;
begin
  Result := FFullTimePartTime_Specified;
end;

procedure Employee.SetHrlySlryNonExemptFluc1(Index: Integer; const AWideString: WideString);
begin
  FHrlySlryNonExemptFluc1 := AWideString;
  FHrlySlryNonExemptFluc1_Specified := True;
end;

function Employee.HrlySlryNonExemptFluc1_Specified(Index: Integer): boolean;
begin
  Result := FHrlySlryNonExemptFluc1_Specified;
end;

procedure Employee.SetSSN(Index: Integer; const AWideString: WideString);
begin
  FSSN := AWideString;
  FSSN_Specified := True;
end;

function Employee.SSN_Specified(Index: Integer): boolean;
begin
  Result := FSSN_Specified;
end;

procedure Employee.SetPayGroup(Index: Integer; const AWideString: WideString);
begin
  FPayGroup := AWideString;
  FPayGroup_Specified := True;
end;

function Employee.PayGroup_Specified(Index: Integer): boolean;
begin
  Result := FPayGroup_Specified;
end;

procedure Employee.SetReviewDate(Index: Integer; const AWideString: WideString);
begin
  FReviewDate := AWideString;
  FReviewDate_Specified := True;
end;

function Employee.ReviewDate_Specified(Index: Integer): boolean;
begin
  Result := FReviewDate_Specified;
end;

procedure Employee.SetPhoneNumber(Index: Integer; const AWideString: WideString);
begin
  FPhoneNumber := AWideString;
  FPhoneNumber_Specified := True;
end;

function Employee.PhoneNumber_Specified(Index: Integer): boolean;
begin
  Result := FPhoneNumber_Specified;
end;

procedure Employee.SetTerminationCode(Index: Integer; const AWideString: WideString);
begin
  FTerminationCode := AWideString;
  FTerminationCode_Specified := True;
end;

function Employee.TerminationCode_Specified(Index: Integer): boolean;
begin
  Result := FTerminationCode_Specified;
end;

procedure Employee.SetTerminationDate(Index: Integer; const AWideString: WideString);
begin
  FTerminationDate := AWideString;
  FTerminationDate_Specified := True;
end;

function Employee.TerminationDate_Specified(Index: Integer): boolean;
begin
  Result := FTerminationDate_Specified;
end;

procedure Employee.SetHomeDepartment(Index: Integer; const AWideString: WideString);
begin
  FHomeDepartment := AWideString;
  FHomeDepartment_Specified := True;
end;

function Employee.HomeDepartment_Specified(Index: Integer): boolean;
begin
  Result := FHomeDepartment_Specified;
end;

procedure Employee.SetHomeJob(Index: Integer; const AWideString: WideString);
begin
  FHomeJob := AWideString;
  FHomeJob_Specified := True;
end;

function Employee.HomeJob_Specified(Index: Integer): boolean;
begin
  Result := FHomeJob_Specified;
end;

procedure Employee.SetSiteCode2(Index: Integer; const AWideString: WideString);
begin
  FSiteCode2 := AWideString;
  FSiteCode2_Specified := True;
end;

function Employee.SiteCode2_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode2_Specified;
end;

procedure Employee.SetJobCode2(Index: Integer; const AWideString: WideString);
begin
  FJobCode2 := AWideString;
  FJobCode2_Specified := True;
end;

function Employee.JobCode2_Specified(Index: Integer): boolean;
begin
  Result := FJobCode2_Specified;
end;

procedure Employee.SetDepartment2(Index: Integer; const AWideString: WideString);
begin
  FDepartment2 := AWideString;
  FDepartment2_Specified := True;
end;

function Employee.Department2_Specified(Index: Integer): boolean;
begin
  Result := FDepartment2_Specified;
end;

procedure Employee.SetSiteCode3(Index: Integer; const AWideString: WideString);
begin
  FSiteCode3 := AWideString;
  FSiteCode3_Specified := True;
end;

function Employee.SiteCode3_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode3_Specified;
end;

procedure Employee.SetJobCode3(Index: Integer; const AWideString: WideString);
begin
  FJobCode3 := AWideString;
  FJobCode3_Specified := True;
end;

function Employee.JobCode3_Specified(Index: Integer): boolean;
begin
  Result := FJobCode3_Specified;
end;

procedure Employee.SetDepartment3(Index: Integer; const AWideString: WideString);
begin
  FDepartment3 := AWideString;
  FDepartment3_Specified := True;
end;

function Employee.Department3_Specified(Index: Integer): boolean;
begin
  Result := FDepartment3_Specified;
end;

procedure Employee.SetSiteCode4(Index: Integer; const AWideString: WideString);
begin
  FSiteCode4 := AWideString;
  FSiteCode4_Specified := True;
end;

function Employee.SiteCode4_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode4_Specified;
end;

procedure Employee.SetJobCode4(Index: Integer; const AWideString: WideString);
begin
  FJobCode4 := AWideString;
  FJobCode4_Specified := True;
end;

function Employee.JobCode4_Specified(Index: Integer): boolean;
begin
  Result := FJobCode4_Specified;
end;

procedure Employee.SetDepartment4(Index: Integer; const AWideString: WideString);
begin
  FDepartment4 := AWideString;
  FDepartment4_Specified := True;
end;

function Employee.Department4_Specified(Index: Integer): boolean;
begin
  Result := FDepartment4_Specified;
end;

procedure Employee.SetSiteCode5(Index: Integer; const AWideString: WideString);
begin
  FSiteCode5 := AWideString;
  FSiteCode5_Specified := True;
end;

function Employee.SiteCode5_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode5_Specified;
end;

procedure Employee.SetJobCode5(Index: Integer; const AWideString: WideString);
begin
  FJobCode5 := AWideString;
  FJobCode5_Specified := True;
end;

function Employee.JobCode5_Specified(Index: Integer): boolean;
begin
  Result := FJobCode5_Specified;
end;

procedure Employee.SetDepartment5(Index: Integer; const AWideString: WideString);
begin
  FDepartment5 := AWideString;
  FDepartment5_Specified := True;
end;

function Employee.Department5_Specified(Index: Integer): boolean;
begin
  Result := FDepartment5_Specified;
end;

procedure Employee.SetSiteCode6(Index: Integer; const AWideString: WideString);
begin
  FSiteCode6 := AWideString;
  FSiteCode6_Specified := True;
end;

function Employee.SiteCode6_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode6_Specified;
end;

procedure Employee.SetJobCode6(Index: Integer; const AWideString: WideString);
begin
  FJobCode6 := AWideString;
  FJobCode6_Specified := True;
end;

function Employee.JobCode6_Specified(Index: Integer): boolean;
begin
  Result := FJobCode6_Specified;
end;

procedure Employee.SetDepartment6(Index: Integer; const AWideString: WideString);
begin
  FDepartment6 := AWideString;
  FDepartment6_Specified := True;
end;

function Employee.Department6_Specified(Index: Integer): boolean;
begin
  Result := FDepartment6_Specified;
end;

procedure Employee.SetSiteCode7(Index: Integer; const AWideString: WideString);
begin
  FSiteCode7 := AWideString;
  FSiteCode7_Specified := True;
end;

function Employee.SiteCode7_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode7_Specified;
end;

procedure Employee.SetJobCode7(Index: Integer; const AWideString: WideString);
begin
  FJobCode7 := AWideString;
  FJobCode7_Specified := True;
end;

function Employee.JobCode7_Specified(Index: Integer): boolean;
begin
  Result := FJobCode7_Specified;
end;

procedure Employee.SetDepartment7(Index: Integer; const AWideString: WideString);
begin
  FDepartment7 := AWideString;
  FDepartment7_Specified := True;
end;

function Employee.Department7_Specified(Index: Integer): boolean;
begin
  Result := FDepartment7_Specified;
end;

procedure Employee.SetSiteCode8(Index: Integer; const AWideString: WideString);
begin
  FSiteCode8 := AWideString;
  FSiteCode8_Specified := True;
end;

function Employee.SiteCode8_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode8_Specified;
end;

procedure Employee.SetJobCode8(Index: Integer; const AWideString: WideString);
begin
  FJobCode8 := AWideString;
  FJobCode8_Specified := True;
end;

function Employee.JobCode8_Specified(Index: Integer): boolean;
begin
  Result := FJobCode8_Specified;
end;

procedure Employee.SetDepartment8(Index: Integer; const AWideString: WideString);
begin
  FDepartment8 := AWideString;
  FDepartment8_Specified := True;
end;

function Employee.Department8_Specified(Index: Integer): boolean;
begin
  Result := FDepartment8_Specified;
end;

procedure Employee.SetSiteCode9(Index: Integer; const AWideString: WideString);
begin
  FSiteCode9 := AWideString;
  FSiteCode9_Specified := True;
end;

function Employee.SiteCode9_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode9_Specified;
end;

procedure Employee.SetJobCode9(Index: Integer; const AWideString: WideString);
begin
  FJobCode9 := AWideString;
  FJobCode9_Specified := True;
end;

function Employee.JobCode9_Specified(Index: Integer): boolean;
begin
  Result := FJobCode9_Specified;
end;

procedure Employee.SetDepartment9(Index: Integer; const AWideString: WideString);
begin
  FDepartment9 := AWideString;
  FDepartment9_Specified := True;
end;

function Employee.Department9_Specified(Index: Integer): boolean;
begin
  Result := FDepartment9_Specified;
end;

procedure Employee.SetSiteCode10(Index: Integer; const AWideString: WideString);
begin
  FSiteCode10 := AWideString;
  FSiteCode10_Specified := True;
end;

function Employee.SiteCode10_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode10_Specified;
end;

procedure Employee.SetJobCode10(Index: Integer; const AWideString: WideString);
begin
  FJobCode10 := AWideString;
  FJobCode10_Specified := True;
end;

function Employee.JobCode10_Specified(Index: Integer): boolean;
begin
  Result := FJobCode10_Specified;
end;

procedure Employee.SetDepartment10(Index: Integer; const AWideString: WideString);
begin
  FDepartment10 := AWideString;
  FDepartment10_Specified := True;
end;

function Employee.Department10_Specified(Index: Integer): boolean;
begin
  Result := FDepartment10_Specified;
end;

procedure Employee.SetSiteCode11(Index: Integer; const AWideString: WideString);
begin
  FSiteCode11 := AWideString;
  FSiteCode11_Specified := True;
end;

function Employee.SiteCode11_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode11_Specified;
end;

procedure Employee.SetJobCode11(Index: Integer; const AWideString: WideString);
begin
  FJobCode11 := AWideString;
  FJobCode11_Specified := True;
end;

function Employee.JobCode11_Specified(Index: Integer): boolean;
begin
  Result := FJobCode11_Specified;
end;

procedure Employee.SetDepartment11(Index: Integer; const AWideString: WideString);
begin
  FDepartment11 := AWideString;
  FDepartment11_Specified := True;
end;

function Employee.Department11_Specified(Index: Integer): boolean;
begin
  Result := FDepartment11_Specified;
end;

procedure Employee.SetSiteCode12(Index: Integer; const AWideString: WideString);
begin
  FSiteCode12 := AWideString;
  FSiteCode12_Specified := True;
end;

function Employee.SiteCode12_Specified(Index: Integer): boolean;
begin
  Result := FSiteCode12_Specified;
end;

procedure Employee.SetJobCode12(Index: Integer; const AWideString: WideString);
begin
  FJobCode12 := AWideString;
  FJobCode12_Specified := True;
end;

function Employee.JobCode12_Specified(Index: Integer): boolean;
begin
  Result := FJobCode12_Specified;
end;

procedure Employee.SetDepartment12(Index: Integer; const AWideString: WideString);
begin
  FDepartment12 := AWideString;
  FDepartment12_Specified := True;
end;

function Employee.Department12_Specified(Index: Integer): boolean;
begin
  Result := FDepartment12_Specified;
end;

procedure ImportDetails.SetOrganization(Index: Integer; const AWideString: WideString);
begin
  FOrganization := AWideString;
  FOrganization_Specified := True;
end;

function ImportDetails.Organization_Specified(Index: Integer): boolean;
begin
  Result := FOrganization_Specified;
end;

procedure ImportDetails.SetHomeSite(Index: Integer; const AWideString: WideString);
begin
  FHomeSite := AWideString;
  FHomeSite_Specified := True;
end;

function ImportDetails.HomeSite_Specified(Index: Integer): boolean;
begin
  Result := FHomeSite_Specified;
end;

procedure ImportDetails.SetEmployeeCode(Index: Integer; const AWideString: WideString);
begin
  FEmployeeCode := AWideString;
  FEmployeeCode_Specified := True;
end;

function ImportDetails.EmployeeCode_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeCode_Specified;
end;

procedure ImportDetails.SetFirstName(Index: Integer; const AWideString: WideString);
begin
  FFirstName := AWideString;
  FFirstName_Specified := True;
end;

function ImportDetails.FirstName_Specified(Index: Integer): boolean;
begin
  Result := FFirstName_Specified;
end;

procedure ImportDetails.SetLastName(Index: Integer; const AWideString: WideString);
begin
  FLastName := AWideString;
  FLastName_Specified := True;
end;

function ImportDetails.LastName_Specified(Index: Integer): boolean;
begin
  Result := FLastName_Specified;
end;

procedure ImportDetails.SetStatus(Index: Integer; const AWideString: WideString);
begin
  FStatus := AWideString;
  FStatus_Specified := True;
end;

function ImportDetails.Status_Specified(Index: Integer): boolean;
begin
  Result := FStatus_Specified;
end;

procedure ImportDetails.SetDetail(Index: Integer; const AWideString: WideString);
begin
  FDetail := AWideString;
  FDetail_Specified := True;
end;

function ImportDetails.Detail_Specified(Index: Integer): boolean;
begin
  Result := FDetail_Specified;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(SecurePayrollDataService1Soap), 'http://m3as.com/SPDS', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(SecurePayrollDataService1Soap), 'http://m3as.com/SPDS/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(SecurePayrollDataService1Soap), ioDocument);
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfExportDetails), 'http://m3as.com/SPDS', 'ArrayOfExportDetails');
  RemClassRegistry.RegisterXSClass(ExportDetails, 'http://m3as.com/SPDS', 'ExportDetails');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployee), 'http://m3as.com/SPDS', 'ArrayOfEmployee');
  RemClassRegistry.RegisterXSClass(Employee, 'http://m3as.com/SPDS', 'Employee');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfImportDetails), 'http://m3as.com/SPDS', 'ArrayOfImportDetails');
  RemClassRegistry.RegisterXSClass(ImportDetails, 'http://m3as.com/SPDS', 'ImportDetails');

end.