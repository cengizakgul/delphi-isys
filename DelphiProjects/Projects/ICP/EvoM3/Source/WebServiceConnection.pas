unit WebServiceConnection;

interface

uses gdyCommonLogger, xmlintf, SP_ConnectionParamFrame, StrUtils, XmlRpcCommon,
  classes, sysutils, IdHTTP, IdSSLOpenSSL, idintercept, gdycommon, Types,
  gdyRedir, common, xmldoc, variants, SecurePayrollDataService, Rio, SOAPHTTPClient,
  InvokeRegistry, SOAPConn, DB, kbmMemTable, XSBuiltIns, WinInet, FMTBcd;

type
  IWebServiceConnection = interface
    function ImportEmployees(aData: TkbmCustomMemTable): boolean;
  end;

  TWebServiceConnection = class(TInterfacedObject, IWebServiceConnection)
  private
    FService: SecurePayrollDataService1Soap;
    FLogger: ICommonLogger;
    FParam: TSP_ConnectionParam;
    FShowGUI: boolean;

    procedure BeginConnection;
    procedure EndConnection;

    function EncodePassword(const Pwd: string): string;
  public
    constructor Create(logger: ICommonLogger; param: TSP_ConnectionParam; ShowGUI: boolean);
    destructor Destroy; override;
    {IWebServiceConnection}
    function ImportEmployees(aData: TkbmCustomMemTable): boolean; virtual;
  end;

  TStableWebServiceConnection = class(TWebServiceConnection)
    {IWebServiceConnection}
    function ImportEmployees(aData: TkbmCustomMemTable): boolean; override;
  end;

function CreateWebServiceConnection(logger: ICommonLogger; param: TSP_ConnectionParam; ShowGUI: boolean): IWebServiceConnection;

implementation

uses
  gdyGlobalWaitIndicator, DateUtils, Mapping;

const
  sCtxComponentHRISLink = 'Secure Payroll Web Service connection';

function CreateWebServiceConnection(logger: ICommonLogger; param: TSP_ConnectionParam; ShowGUI: boolean): IWebServiceConnection;
begin
  Result := TStableWebServiceConnection.Create(logger, param, ShowGUI);
end;

procedure TWebServiceConnection.BeginConnection;
begin
  FService := GetSecurePayrollDataService1Soap( false, FParam.URL );
end;

constructor TWebServiceConnection.Create(logger: ICommonLogger;
  param: TSP_ConnectionParam; ShowGUI: boolean);
begin
  FLogger := logger;
  FParam := param;
  FShowGUI := ShowGUI;
end;

destructor TWebServiceConnection.Destroy;
begin
  inherited;
end;

function TWebServiceConnection.EncodePassword(const Pwd: string): string;
const
  Key = 'sH7uf4gp9';

  function XORIt(const S: string; const Key: string): string;
  var
    I: Byte;
  begin
    Result := '';
    for I := 1 to Length(S) do
    begin
      Result := Result + Char(Ord(S[I]) xor Ord(Key[((I-1) mod Length(Key)) + 1]));
{      exp := exp + 'Res[' + IntToStr(I) + '] = Password[' + IntToStr(I) + '] xor Key[' + IntToStr(((I-1) mod Length(Key)) + 1) + '], Char: "' + S[I] + '" xor "' + Key[((I-1) mod Length(Key)) + 1] +
        '" = ' + UrlEncode(Char(Ord(S[I]) xor Ord(Key[((I-1) mod Length(Key)) + 1]))) + ', Bin: ' +
        DEC2BIN(Ord(S[I])) + ' xor ' + DEC2BIN(Ord(Key[((I-1) mod Length(Key)) + 1])) + #10#13;}
    end;
  end;
begin
{  sl := TStringList.create;
  try
    sl.Add('Test ');
    sl.Add('Password: ' +  pwd);
    sl.Add('Key: ' + Key);
    sl.Add('-----');
    sl.Add('XORed password: ->');
    sl.Add(XORIt(Pwd, Key));
    sl.Add('<-');
    sl.Add(exp);
    sl.Add('-----');
    sl.Add('URL Encoded XORed password: ->');
    sl.Add(URLEncode( XORIt(Pwd, Key) ));
    sl.Add('<-');
    sl.Add('-----');
    sl.SaveToFile('C:\ICP\Testing\EvoM3_password_encoding.txt');
  finally;
    sl.Free;
  end;}
  Result := URLEncode( XORIt(Pwd, Key) );
end;

procedure TWebServiceConnection.EndConnection;
begin
  FService := nil;
end;

function TWebServiceConnection.ImportEmployees(aData: TkbmCustomMemTable): boolean;
var
  ImportEeDetails: SecurePayrollDataService.ArrayOfImportDetails;
  Employees: SecurePayrollDataService.ArrayOfEmployee;
  Ee: SecurePayrollDataService.Employee;
  OrgCode, SiteCode, FullName: string;
  i: integer;

  procedure InitNewEeBatch;
  begin
    SetLength(Employees, 0);
    OrgCode := aData.FieldByName(Map[Ord(rfOrganizationCode)].Name).AsString;
    SiteCode := aData.FieldByName(Map[Ord(rfSiteCode)].Name).AsString;
  end;

  procedure RunImportEmployee;
  var
    TimeOut: integer;
    z: integer;
  begin
    if Length(Employees) > 0 then
    begin
      if FShowGUI then
        WaitIndicator.StartWait('Import Employees (Organization Code: ' + OrgCode + ', Site Code: ' + SiteCode + ', Employee Count: ' + IntToStr(Length(Employees)) + ')');

      BeginConnection;
      try
        TimeOut := 1800000; // 30 min
        InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
        InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
        InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

        FLogger.LogEvent('Import Employees (Organization Code: ' + OrgCode + ', Site Code: ' + SiteCode + ', Employee Count: ' + IntToStr(Length(Employees)) + ')');
        try
          ImportEeDetails := FService.ImportEmployees( FParam.Username, EncodePassword( FParam.Password ), OrgCode, SiteCode, Employees );
        except
          on e: exception do
          begin
            FLogger.LogError(e.Message);
            Result := False;
          end;
        end;
      finally
        if Length(ImportEeDetails) = 0 then
          FLogger.LogEvent('No Employees have been submitted')
        else if Length(ImportEeDetails) = 1 then
          FLogger.LogEvent('1 Employee has been submitted')
        else
          FLogger.LogEvent(IntToStr(Length(ImportEeDetails)) + ' Employees have been submitted');

        for z := Low(ImportEeDetails) to High(ImportEeDetails) do
        begin
          Result := Result and not (UpperCase(ImportEeDetails[z].Status) = 'FAILED');
          FLogger.LogEvent(
            'Organization: ' + ImportEeDetails[z].Organization + ', Home Site: ' + ImportEeDetails[z].HomeSite +
            ', Employee: ' + ImportEeDetails[z].FirstName + ' ' + ImportEeDetails[z].LastName +
            ', Status: ' + ImportEeDetails[z].Status,
            ImportEeDetails[z].Detail
          );
        end;

        EndConnection;

        if FShowGUI then
          WaitIndicator.EndWait;
      end;
    end;
  end;
  function ConvertDate(aDate: TDatetime): string;
  begin
    Result := FormatDateTime('yyyy-mm-dd', aDate) + 'T' + FormatDateTime('HH:NN:SS', aDate)
  end;
begin
  Result := True;
  if aData.Active and (aData.RecordCount > 0) then
  begin
    aData.SortOn('OrganizationCode;SiteCode', []);
    aData.First;

    InitNewEeBatch;

    while not aData.Eof do
    begin
      if (OrgCode <> aData.FieldByName(Map[Ord(rfOrganizationCode)].Name).AsString) or
        (SiteCode <> aData.FieldByName(Map[Ord(rfSiteCode)].Name).AsString) then
      begin
        RunImportEmployee;
        InitNewEeBatch;
      end;

      if FShowGUI then
        WaitIndicator.StartWait('Prepare data for Employee #' + aData.FieldByName(Map[Ord(rfEmployeeCode)].Name).AsString);

      try
        Ee := Employee.Create;

        if aData.FieldByName(Map[Ord(rfAddress)].Name).AsString <> '' then
          Ee.Address := aData.FieldByName(Map[Ord(rfAddress)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfAddress2)].Name).AsString <> '' then
          Ee.Address2 := aData.FieldByName(Map[Ord(rfAddress2)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfCity)].Name).AsString <> '' then
          Ee.City := aData.FieldByName(Map[Ord(rfCity)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfClockNo)].Name).AsString <> '' then
          Ee.ClockNo := aData.FieldByName(Map[Ord(rfClockNo)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfCountry)].Name).AsString <> '' then
          Ee.Country := aData.FieldByName(Map[Ord(rfCountry)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfDateOfBirth)].Name).AsDateTime > 1 then
          Ee.DateOfBirth := ConvertDate( aData.FieldByName(Map[Ord(rfDateOfBirth)].Name).AsDateTime );
        if aData.FieldByName(Map[Ord(rfDateOfHire)].Name).AsDateTime > 1 then
          Ee.DateOfHire := ConvertDate( aData.FieldByName(Map[Ord(rfDateOfHire)].Name).AsDateTime );
        if aData.FieldByName(Map[Ord(rfDepartment2)].Name).AsString <> '' then
          Ee.Department2 := aData.FieldByName(Map[Ord(rfDepartment2)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfDepartment3)].Name).AsString <> '' then
          Ee.Department3 := aData.FieldByName(Map[Ord(rfDepartment3)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfEffectiveDate)].Name).AsDateTime > 1 then
          Ee.EffectiveDate := ConvertDate( aData.FieldByName(Map[Ord(rfEffectiveDate)].Name).AsDateTime );
        if aData.FieldByName(Map[Ord(rfEmployeeCode)].Name).AsString <> '' then
          Ee.EmployeeCode := aData.FieldByName(Map[Ord(rfEmployeeCode)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfEmployeeStatus)].Name).AsString <> '' then
          Ee.EmployeeStatus := aData.FieldByName(Map[Ord(rfEmployeeStatus)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfEmployeeType)].Name).AsString <> '' then
          Ee.EmployeeType := aData.FieldByName(Map[Ord(rfEmployeeType)].Name).AsString;

        // there is Ee Full Name in the First Name column, Format = <LastName> + "|" + <FirstName>
        FullName := aData.FieldByName(Map[Ord(rfFirstName)].Name).AsString;
        i := Pos('|', FullName);
        if i > 0 then
        begin
          Ee.FirstName := FullName;//Copy(FullName, i + 1, Length(FullName));
          Ee.LastName := Copy(FullName, 1, i - 1);
        end
        else begin
          Ee.FirstName := FullName;
        end;

        if aData.FieldByName(Map[Ord(rfFullTimePartTime)].Name).AsString <> '' then
          Ee.FullTimePartTime := aData.FieldByName(Map[Ord(rfFullTimePartTime)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfGender)].Name).AsString <> '' then
          Ee.Gender := aData.FieldByName(Map[Ord(rfGender)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfHomeDepartment)].Name).AsString <> '' then
          Ee.HomeDepartment := aData.FieldByName(Map[Ord(rfHomeDepartment)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfHomeHourlyRate)].Name).AsFloat <> 0 then
        begin
          Ee.HomeHourlyRate := TXSDecimal.Create;
          Ee.HomeHourlyRate.AsBcd := aData.FieldByName(Map[Ord(rfHomeHourlyRate)].Name).AsBCD;
        end;
        if aData.FieldByName(Map[Ord(rfHomeJob)].Name).AsString <> '' then
          Ee.HomeJob := aData.FieldByName(Map[Ord(rfHomeJob)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfHrlySlryNonExemptFluc1)].Name).AsString <> '' then
          Ee.HrlySlryNonExemptFluc1 := aData.FieldByName(Map[Ord(rfHrlySlryNonExemptFluc1)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobCode2)].Name).AsString <> '' then
          Ee.JobCode2 := aData.FieldByName(Map[Ord(rfJobCode2)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobCode3)].Name).AsString <> '' then
          Ee.JobCode3 := aData.FieldByName(Map[Ord(rfJobCode3)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobRate2)].Name).AsFloat <> 0 then
        begin
          Ee.JobRate2 := TXSDecimal.Create;
          Ee.JobRate2.AsBcd := aData.FieldByName(Map[Ord(rfJobRate2)].Name).AsBCD;
        end;
        if aData.FieldByName(Map[Ord(rfJobRate3)].Name).AsFloat <> 0 then
        begin
          Ee.JobRate3 := TXSDecimal.Create;
          Ee.JobRate3.AsBcd := aData.FieldByName(Map[Ord(rfJobRate3)].Name).AsBCD;
        end;
        if aData.FieldByName(Map[Ord(rfMiddleInitial)].Name).AsString <> '' then
          Ee.MiddleInitial := aData.FieldByName(Map[Ord(rfMiddleInitial)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfPayGroup)].Name).AsString <> '' then
          Ee.PayGroup := aData.FieldByName(Map[Ord(rfPayGroup)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfPhoneNumber)].Name).AsString <> '' then
          Ee.PhoneNumber := aData.FieldByName(Map[Ord(rfPhoneNumber)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfPostalCode)].Name).AsString <> '' then
          Ee.PostalCode := aData.FieldByName(Map[Ord(rfPostalCode)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfReviewDate)].Name).AsDateTime > 1 then
          Ee.ReviewDate := ConvertDate( aData.FieldByName(Map[Ord(rfReviewDate)].Name).AsDateTime );
        if aData.FieldByName(Map[Ord(rfSSN)].Name).AsString <> '' then
          Ee.SSN := aData.FieldByName(Map[Ord(rfSSN)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfState)].Name).AsString <> '' then
          Ee.State := aData.FieldByName(Map[Ord(rfState)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfTerminationCode)].Name).AsString <> '' then
        begin
          Ee.TerminationCode := aData.FieldByName(Map[Ord(rfTerminationCode)].Name).AsString;
          Ee.TerminationDate := ConvertDate( aData.FieldByName(Map[Ord(rfTerminationDate)].Name).AsDatetime );
        end;
        if aData.FieldByName(Map[Ord(rfJobCode4)].Name).AsString <> '' then
          Ee.JobCode4 := aData.FieldByName(Map[Ord(rfJobCode4)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfDepartment4)].Name).AsString <> '' then
          Ee.Department4 := aData.FieldByName(Map[Ord(rfDepartment4)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobRate4)].Name).AsFloat <> 0 then
        begin
          Ee.JobRate4 := TXSDecimal.Create;
          Ee.JobRate4.AsBcd := aData.FieldByName(Map[Ord(rfJobRate4)].Name).AsBCD;
        end;

        if aData.FieldByName(Map[Ord(rfJobCode5)].Name).AsString <> '' then
          Ee.JobCode5 := aData.FieldByName(Map[Ord(rfJobCode5)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfDepartment5)].Name).AsString <> '' then
          Ee.Department5 := aData.FieldByName(Map[Ord(rfDepartment5)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobRate5)].Name).AsFloat <> 0 then
        begin
          Ee.JobRate5 := TXSDecimal.Create;
          Ee.JobRate5.AsBcd := aData.FieldByName(Map[Ord(rfJobRate5)].Name).AsBCD;
        end;

        if aData.FieldByName(Map[Ord(rfJobCode6)].Name).AsString <> '' then
          Ee.JobCode6 := aData.FieldByName(Map[Ord(rfJobCode6)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfDepartment6)].Name).AsString <> '' then
          Ee.Department6 := aData.FieldByName(Map[Ord(rfDepartment6)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobRate6)].Name).AsFloat <> 0 then
        begin
          Ee.JobRate6 := TXSDecimal.Create;
          Ee.JobRate6.AsBcd := aData.FieldByName(Map[Ord(rfJobRate6)].Name).AsBCD;
        end;

        if aData.FieldByName(Map[Ord(rfJobCode7)].Name).AsString <> '' then
          Ee.JobCode7 := aData.FieldByName(Map[Ord(rfJobCode7)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfDepartment7)].Name).AsString <> '' then
          Ee.Department7 := aData.FieldByName(Map[Ord(rfDepartment7)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfJobRate7)].Name).AsFloat <> 0 then
        begin
          Ee.JobRate7 := TXSDecimal.Create;
          Ee.JobRate7.AsBcd := aData.FieldByName(Map[Ord(rfJobRate7)].Name).AsBCD;
        end;

        if aData.FieldByName(Map[Ord(rfBranch2)].Name).AsString <> '' then
          Ee.SiteCode2 := aData.FieldByName(Map[Ord(rfBranch2)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfBranch3)].Name).AsString <> '' then
          Ee.SiteCode3 := aData.FieldByName(Map[Ord(rfBranch3)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfBranch4)].Name).AsString <> '' then
          Ee.SiteCode4 := aData.FieldByName(Map[Ord(rfBranch4)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfBranch5)].Name).AsString <> '' then
          Ee.SiteCode5 := aData.FieldByName(Map[Ord(rfBranch5)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfBranch6)].Name).AsString <> '' then
          Ee.SiteCode6 := aData.FieldByName(Map[Ord(rfBranch6)].Name).AsString;
        if aData.FieldByName(Map[Ord(rfBranch7)].Name).AsString <> '' then
          Ee.SiteCode7 := aData.FieldByName(Map[Ord(rfBranch7)].Name).AsString;
      finally
        if FShowGUI then
          WaitIndicator.EndWait;
      end;

      // add ee to employees array
      SetLength( Employees, Length(Employees) + 1);
      Employees[High(Employees)] := Ee;

      aData.Next;
    end;

    RunImportEmployee;
  end;

{  if not Result then
    raise Exception.Create('Import failed!');}
end;

{ TStableWebServiceConnection }

function TStableWebServiceConnection.ImportEmployees(aData: TkbmCustomMemTable): boolean;
begin
  Result := False;
  try
    Result := inherited ImportEmployees(aData);
  except
    on e: Exception do
      if Pos('re-authenticate a new session', e.Message) > 0 then
      begin
        Result := inherited ImportEmployees(aData);
      end
      else
        FLogger.PassthroughException;
  end;
end;

end.
