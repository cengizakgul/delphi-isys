unit M3_Tasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, common,
  evodata, classes, scheduledCustomTask,
  SP_ConnectionParamFrame, evoapiconnectionutils, ImportProcessing,
  ReportParamFrame;

type
  TM3_TaskAdapter = class(TTaskAdapterCustomBase)
  private
    function GetSP_ConnectionParam: TSP_ConnectionParam;
  published
    procedure ImportEmployees_Execute;
    function ImportEmployees_Describe: string;
  end;


function NewImportEmployeesTask(ClCo: TEvoCompanyDef): IScheduledTask;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection,
  gdyclasses, gdyCommon, dialogs, SchedulerFrame_,
  gdyDialogEngine, controls, kbmMemTable, userActionHelpers;

function NewImportEmployeesTask(ClCo: TEvoCompanyDef): IScheduledTask;
begin
  Result := NewTask('ImportEmployees', 'Import Employees', ClCo);
end;

{
procedure EditEEExportTask(task: IScheduledTask; Owner: TComponent);
var
  dlg: TEEExportDlg;
begin
  dlg := TEEExportDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.EEExportOptionsFrame.Options := LoadEEExportOptions(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveEEExportOptions( dlg.EEExportOptionsFrame.Options, task.ParamSettings, '' );
  finally
    FreeAndNil(dlg);
  end;
end;
}

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);
begin
  if task.Name = 'ImportEmployees' then
    ShowMessage('Import Employees task has no parameters')
  else
    Assert(false);
end;

{ TM3_TaskAdapter }

function TM3_TaskAdapter.GetSP_ConnectionParam: TSP_ConnectionParam;
begin
  Result := LoadSP_ConnectionParam(FSettings, '');
end;

function TM3_TaskAdapter.ImportEmployees_Describe: string;
begin
  Result := '';
end;

procedure TM3_TaskAdapter.ImportEmployees_Execute;
var
  Param: TReportParam;
begin
  Param := LoadReportParam(FSettings, '');

  ImportProcessing.DoImportEmployees(FLogger, nil, FTask.Company,
    GetEvoAPICOnnection, GetSP_ConnectionParam, Param, false);

  SaveReportParam(Param, FSettings, ''); // <-- store updated LastSync
end;

end.
