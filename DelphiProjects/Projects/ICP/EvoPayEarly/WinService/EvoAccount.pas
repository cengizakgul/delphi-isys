unit EvoAccount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Mask, DBCtrls, StdCtrls, PasswordEdit;

type
  TfrmEvoAccount = class(TForm)
    gbSettings: TGroupBox;
    lblAPIServer: TLabel;
    lblUserName: TLabel;
    lblPassword: TLabel;
    btnSave: TButton;
    btnCancel: TButton;
    dsEvoAccounts: TDataSource;
    edApiAdapter: TDBEdit;
    edUsername: TDBEdit;
    edPassword: TDBEdit;
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure OpenEvoAccount(aDS: TDataset);

implementation

{$R *.dfm}

procedure OpenEvoAccount(aDS: TDataset);
begin
  with TfrmEvoAccount.Create(nil) do
  try
    dsEvoAccounts.DataSet := aDS;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TfrmEvoAccount.btnCancelClick(Sender: TObject);
begin
  if Assigned(dsEvoAccounts.DataSet) and (dsEvoAccounts.DataSet.State in [dsEdit, dsInsert]) then
    dsEvoAccounts.DataSet.Cancel;
end;

procedure TfrmEvoAccount.btnSaveClick(Sender: TObject);
begin
  if Assigned(dsEvoAccounts.DataSet) and (dsEvoAccounts.DataSet.State in [dsEdit, dsInsert]) then
  try
    dsEvoAccounts.DataSet.Post;
  except
    dsEvoAccounts.DataSet.Cancel;
  end;
end;

end.
