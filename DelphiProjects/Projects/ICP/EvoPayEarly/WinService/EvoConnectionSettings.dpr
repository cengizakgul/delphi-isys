program EvoConnectionSettings;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  ConnectionDialog in 'ConnectionDialog.pas' {frmSettings},
  EvoPayEarlyConstAndProc in '..\Source\EvoPayEarlyConstAndProc.pas',
  EvoAccount in 'EvoAccount.pas' {frmEvoAccount};

{$R ..\Source\EvoElastic.RES}
//  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}

begin
  Application.Initialize;
  Application.CreateForm(TfrmSettings, frmSettings);
  Application.Run;
end.
