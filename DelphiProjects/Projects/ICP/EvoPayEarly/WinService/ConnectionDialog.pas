unit ConnectionDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, PasswordEdit, common, Grids, Wwdbigrd, Wwdbgrid,
  dbcomp, ActnList, DB, kbmMemTable;

type
  TfrmSettings = class(TForm)
    gbSettings: TGroupBox;
    btnSave: TButton;
    Button1: TButton;
    grEvoAccounts: TReDBGrid;
    btnNew: TButton;
    btnEdit: TButton;
    btnDelete: TButton;
    lblTitle: TLabel;
    Actions: TActionList;
    acNew: TAction;
    acEdit: TAction;
    acDelete: TAction;
    dsEvoUsers: TDataSource;
    procedure btnSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure acNewUpdate(Sender: TObject);
    procedure acEditUpdate(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acNewExecute(Sender: TObject);
    procedure grEvoAccountsDblClick(Sender: TObject);
    procedure grEvoAccountsKeyPress(Sender: TObject; var Key: Char);
  private
    FEvoAccounts: TkbmCustomMemTable;
    procedure CreateDataset;
  end;

var
  frmSettings: TfrmSettings;

implementation

{$R *.dfm}

uses EvoPayEarlyConstAndProc, EvoAccount;

{ TfrmSettings }

procedure TfrmSettings.btnSaveClick(Sender: TObject);
begin
  SaveEvoAccounts(FEvoAccounts);
  FreeAndNil(FEvoAccounts);
  Close;
end;

procedure TfrmSettings.FormShow(Sender: TObject);
begin
  CreateDataset;
  LoadEvoAccounts(FEvoAccounts);
end;

procedure TfrmSettings.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmSettings.CreateDataset;
begin
  FEvoAccounts := CreateEvoAccountsDataset;

  dsEvoUsers.DataSet := FEvoAccounts;
  grEvoAccounts.Selected.Add('ApiAdapter' + #9 + '25' + #9 + 'Evo API Adapter');
  grEvoAccounts.Selected.Add('Username' + #9 + '25' + #9 + 'Evo Username');
end;

procedure TfrmSettings.acNewUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(dsEvoUsers.DataSet) and dsEvoUsers.DataSet.Active;
end;

procedure TfrmSettings.acEditUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(dsEvoUsers.DataSet) and dsEvoUsers.DataSet.Active and
    (dsEvoUsers.DataSet.RecordCount > 0);
end;

procedure TfrmSettings.acDeleteExecute(Sender: TObject);
begin
  dsEvoUsers.DataSet.Delete;
end;

procedure TfrmSettings.acEditExecute(Sender: TObject);
begin
  dsEvoUsers.DataSet.Edit;
  OpenEvoAccount( dsEvoUsers.DataSet );
end;

procedure TfrmSettings.acNewExecute(Sender: TObject);
begin
  dsEvoUsers.DataSet.Append;
  OpenEvoAccount( dsEvoUsers.DataSet );
end;

procedure TfrmSettings.grEvoAccountsDblClick(Sender: TObject);
begin
  acEdit.Execute;
end;

procedure TfrmSettings.grEvoAccountsKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    acEdit.Execute;
end;

end.
