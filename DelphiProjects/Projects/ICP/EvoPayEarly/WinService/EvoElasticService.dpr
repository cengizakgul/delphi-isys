program EvoElasticService;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  SvcMgr,
  main in 'main.pas' {EvoElastic: TService},
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  XmlRpcServer in '..\Source\XmlRpcServer.pas',
  EvoPayEarlySvcServerMod in 'EvoPayEarlySvcServerMod.pas';

{$R ..\Source\EvoElastic.RES}
//  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}

begin
  LicenseKey := 'B9C39C8BA053485C98499909772470EE'; //EvoPayEarly

  Application.Initialize;
  Application.CreateForm(TEvoElastic, EvoElastic);
  Application.Run;
end.
