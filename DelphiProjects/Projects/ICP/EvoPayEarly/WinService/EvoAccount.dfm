object frmEvoAccount: TfrmEvoAccount
  Left = 595
  Top = 284
  BorderStyle = bsDialog
  Caption = 'Evo Account'
  ClientHeight = 146
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbSettings: TGroupBox
    Left = 6
    Top = 2
    Width = 361
    Height = 100
    TabOrder = 0
    object lblAPIServer: TLabel
      Left = 11
      Top = 17
      Width = 96
      Height = 13
      Caption = 'API adapter address'
    end
    object lblUserName: TLabel
      Left = 11
      Top = 43
      Width = 93
      Height = 13
      Caption = 'Evolution username'
    end
    object lblPassword: TLabel
      Left = 11
      Top = 69
      Width = 92
      Height = 13
      Caption = 'Evolution password'
    end
    object edApiAdapter: TDBEdit
      Left = 115
      Top = 14
      Width = 233
      Height = 21
      DataField = 'ApiAdapter'
      DataSource = dsEvoAccounts
      TabOrder = 0
    end
    object edUsername: TDBEdit
      Left = 115
      Top = 40
      Width = 233
      Height = 21
      DataField = 'Username'
      DataSource = dsEvoAccounts
      TabOrder = 1
    end
    object edPassword: TDBEdit
      Left = 115
      Top = 66
      Width = 233
      Height = 21
      DataField = 'Password'
      DataSource = dsEvoAccounts
      PasswordChar = '*'
      TabOrder = 2
    end
  end
  object btnSave: TButton
    Left = 190
    Top = 111
    Width = 83
    Height = 25
    Caption = 'Save'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnSaveClick
  end
  object btnCancel: TButton
    Left = 278
    Top = 111
    Width = 83
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelClick
  end
  object dsEvoAccounts: TDataSource
    Left = 278
    Top = 18
  end
end
