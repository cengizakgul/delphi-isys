object frmSettings: TfrmSettings
  Left = 303
  Top = 133
  Width = 497
  Height = 300
  BorderIcons = [biSystemMenu]
  Caption = 'Evo Connection Settings'
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    481
    262)
  PixelsPerInch = 96
  TextHeight = 13
  object gbSettings: TGroupBox
    Left = 6
    Top = 2
    Width = 469
    Height = 220
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    DesignSize = (
      469
      220)
    object lblTitle: TLabel
      Left = 9
      Top = 15
      Width = 92
      Height = 13
      Caption = 'Evolution Accounts'
    end
    object grEvoAccounts: TReDBGrid
      Left = 8
      Top = 32
      Width = 367
      Height = 175
      DisableThemesInTitle = False
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsEvoUsers
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      OnDblClick = grEvoAccountsDblClick
      OnKeyPress = grEvoAccountsKeyPress
    end
    object btnNew: TButton
      Left = 382
      Top = 32
      Width = 79
      Height = 25
      Action = acNew
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object btnEdit: TButton
      Left = 382
      Top = 64
      Width = 79
      Height = 25
      Action = acEdit
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
    object btnDelete: TButton
      Left = 382
      Top = 96
      Width = 79
      Height = 25
      Action = acDelete
      Anchors = [akTop, akRight]
      TabOrder = 3
    end
  end
  object btnSave: TButton
    Left = 303
    Top = 229
    Width = 79
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save'
    TabOrder = 1
    OnClick = btnSaveClick
  end
  object Button1: TButton
    Left = 388
    Top = 229
    Width = 79
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Actions: TActionList
    Left = 310
    Top = 138
    object acNew: TAction
      Caption = 'New'
      OnExecute = acNewExecute
      OnUpdate = acNewUpdate
    end
    object acEdit: TAction
      Caption = 'Edit'
      OnExecute = acEditExecute
      OnUpdate = acEditUpdate
    end
    object acDelete: TAction
      Caption = 'Delete'
      OnExecute = acDeleteExecute
      OnUpdate = acEditUpdate
    end
  end
  object dsEvoUsers: TDataSource
    Left = 142
    Top = 66
  end
end
