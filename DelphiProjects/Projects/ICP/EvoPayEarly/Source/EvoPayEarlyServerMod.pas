unit EvoPayEarlyServerMod;

interface

uses
  XmlRpcServer, XmlRpcTypes, Classes, SysUtils, EvoPayEarlyConstAndProc, Dialogs,
  kbmMemTable, DB, gdyCommonLogger, evoApiconnection, common, SyncObjs;

type
  WrongSessionException = class(Exception);
  WrongLicenseException = class(Exception);
  APIException = class(Exception);

  TevRPCMethod = procedure (const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String) of object;
  PTevRPCMethod = ^TevRPCMethod;

  TEvoPayEarlyXmlRPCServer = class
  private
    FRpcServer: TRpcServer;
    FMethods: TStringList;
    FLogger: ICommonLogger;
    FEvoAPIConnectionParam: TEvoAPIConnectionParam;
    FMessenger: IMessenger;
    FEvoAPI: IEvoAPIConnection;

    procedure RegisterMethods;
    procedure RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);

    procedure getEmployeeInfo(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure createDirectDeposit(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure getEmployeeACHInfo(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure getCompanyList(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);

    procedure SetDDDefault(var aFields: IRpcStruct);
    procedure SetSchedDefaultED(var aFields: IRpcStruct; const EdCode: string);
    procedure SetSchedDefault(var aFields: IRpcStruct);

    function GetKeyID: string;
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function ApplyRecordChange(var aFields: IRpcStruct; const aTableName, aType: string; aKey: integer): integer;
    function FindCompany(var aCompany: TEvoCompanyDef): boolean;

    function GetActive: boolean;
    procedure SetActive(AValue: boolean);
    procedure SetEvoConnectionParam(aValue: TEvoAPIConnectionParam);
  public
    constructor Create(logger: ICommonLogger; Messenger: IMessenger);
    destructor Destroy; override;

    property Active: boolean read GetActive write SetActive;
    property EvoConnectionParam: TEvoAPIConnectionParam read FEvoAPIConnectionParam write SetEvoConnectionParam;
  end;

implementation

uses gdyRedir, gdyUtils, StrUtils, Variants, DateUtils;

procedure APICheckCondition(const ACondition: Boolean;
  AExceptClass: ExceptClass; const aMessage: string = '');
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      raise Exception.Create('APICheckCondition: exception class should be assigned!')
    else begin
      if Trim(aMessage) <> '' then
        raise AExceptClass.Create('APIException: ' + aMessage)
      else
        raise AExceptClass.Create('APIException');
    end;
  end;
end;

{ TEvoPayEarlyXmlRPCServer }

function TEvoPayEarlyXmlRPCServer.ApplyRecordChange(var aFields: IRpcStruct;
  const aTableName, aType: string; aKey: integer): integer;
var
  Changes: IRpcArray;
  RowChange, EvoResult: IRpcStruct;
begin
  Result := -1;
  if aFields.Count > 0 then
  begin
    Result := aKey;

    Changes := TRpcArray.Create;
    RowChange := TRpcStruct.Create;
    EvoResult := TRpcStruct.Create;

    RowChange.AddItem('T', aType);
    RowChange.AddItem('D', aTableName);
    if aType = 'U' then
      RowChange.AddItem('K', aKey)
    else
      aFields.AddItem(aTableName + '_NBR', -1);

    RowChange.AddItem('F', aFields);

    Changes.AddItem( RowChange );
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );

    if Result = -1 then
      if EvoResult.KeyExists('-1') then
        if not TryStrToInt(EvoResult.Keys['-1'].AsString, Result) then
          raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
  end;
end;

constructor TEvoPayEarlyXmlRPCServer.Create(logger: ICommonLogger; Messenger: IMessenger);
begin
  FLogger := logger;
  FMessenger := Messenger;

  FMethods := TStringList.Create;
  FMethods.Duplicates := dupError;
  FMethods.CaseSensitive := False;
  FMethods.Sorted := True;

  FRpcServer := TRpcServer.Create( logger );

  RegisterMethods;
end;

destructor TEvoPayEarlyXmlRPCServer.Destroy;
begin
  FRpcServer.Active := False;

  FreeAndNil(FRpcServer);
  FreeAndNil(FMethods);
  inherited;
end;

function TEvoPayEarlyXmlRPCServer.FindCompany(
  var aCompany: TEvoCompanyDef): boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := False;
  aCompany.ClNbr := -1;
  aCompany.CoNbr := -1;

  param := TRpcStruct.Create;
  Param.AddItem( 'CoNumber', aCompany.Custom_Company_Number );
  FClCoData :=  RunQuery('TmpCoQuery.rwq', Param);
  try
    if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
    begin
      aCompany.ClNbr := FClCoData.FindField('CL_NBR').AsInteger;
      aCompany.CoNbr := FClCoData.FindField('CO_NBR').AsInteger;
      Result := True;
    end;
  finally
    FClCoData.Indexes.Clear;
    FreeAndNil( FClCoData );
    Param := nil;
  end;
end;

function TEvoPayEarlyXmlRPCServer.GetActive: boolean;
begin
  Result := Assigned(FRpcServer) and FRpcServer.Active;
end;

procedure TEvoPayEarlyXmlRPCServer.RegisterMethods;

   procedure RegisterMethod(const ASignature, AHelp: String; const AHandler: TevRPCMethod);
   var
     RpcMethodHandler: TRpcMethodHandler;
     s: String;
   begin
     s := ASignature;
     RpcMethodHandler := TRpcMethodHandler.Create;
     RpcMethodHandler.Name := GetNextStrValue(s, '(');
     RpcMethodHandler.Method := RPCHandler;
     RpcMethodHandler.Signature := ASignature;
     RpcMethodHandler.Help := AHelp;
     FRpcServer.RegisterMethodHandler(RpcMethodHandler);

     FMethods.AddObject(RpcMethodHandler.Name, @AHandler);
   end;

begin
  RegisterMethod('getEmployeeInfo(string Key, string ClientNumber, string CompanyNumber, string EeFirstName, string EeLastName, string Last5SSN)',
                 'Return: struct',
                 getEmployeeInfo);

  RegisterMethod('createDirectDeposit(string Key, string ClientNumber, string CompanyNumber, string EeFirstName, string EeLastName, string Last5SSN, string AccountNumber, string RoutingNumber, double Amount, datetime StartDate, double Goal)',
                 'Return: struct',
                 createDirectDeposit);

  RegisterMethod('getEmployeeACHInfo(string Key, string ClientNumber, string CompanyNumber, datetime PayDate, int RunNumber)',
                 'Return: struct',
                 getEmployeeAchInfo);

  RegisterMethod('getCompanyList(string Key)',
                 'Return: array',
                 getCompanyList);
end;

procedure TEvoPayEarlyXmlRPCServer.RPCHandler(Thread: TRpcThread;
  const MethodName: string; List: TList; Return: TRpcReturn);
var
  i: Integer;
  Proc: TMethod;
begin
  try
    i := FMethods.IndexOf(MethodName);
    APICheckCondition(i <> -1, APIException, 'Unregistered method "' + MethodName + '"');

    Proc.Data := Self;
    Proc.Code := FMethods.Objects[i];
    TevRPCMethod(Proc)(List, Return, LowerCase(MethodName));
  except
    on E: APIException do
    begin
      Return.SetError(800, E.Message);
    end;

    on E: WrongSessionException do
    begin
      Return.SetError(810, 'Wrong session');
    end;

    on E: WrongLicenseException do
    begin
      Return.SetError(800, 'Wrong license key');
    end;

    on E: Exception do
    begin
      Return.SetError(500, E.Message);
    end;
  end;
end;

function TEvoPayEarlyXmlRPCServer.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

procedure TEvoPayEarlyXmlRPCServer.SetActive(AValue: boolean);
begin
  if FRpcServer.Active <> AValue then
  begin
    if AValue then
    begin
      FMessenger.ClearMessages;

      try
        FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);
        FMessenger.AddMessage('Evo Connection established');

        FLogger.LogEntry( 'Starting to EvoElastic XML-RPC Server' );
        try
          try
            FRpcServer.ListenPort := 9944;
            FRpcServer.EnableIntrospect := True;

            // Set security connection
            FRpcServer.SSLEnable := True;
            FRpcServer.SSLCertFile := Redirection.GetFilename(sCertificateFileAlias);
            FRpcServer.SSLKeyFile := Redirection.GetFilename(sKeyCertificateFileAlias);
            FRpcServer.SSLRootCertFile := Redirection.GetFilename(sRootCertificateFileAlias);

            FRpcServer.Active := True;
            FMessenger.AddMessage('EvoElastic Server started. Listen port 9944...');
          except
            on e: Exception do
            begin
              FMessenger.AddMessage('EvoElastic Server start error: ' + e.Message);
              FLogger.LogError(e.Message);
            end;
          end;
        finally
          FLogger.LogExit;
        end;

      except
        FMessenger.AddMessage('Evo Connection failed!', True);
      end;
    end
    else
      FRpcServer.Active := False;
  end;
end;

procedure TEvoPayEarlyXmlRPCServer.SetEvoConnectionParam(
  aValue: TEvoAPIConnectionParam);
begin
  Active := False;

  FLogger.LogEntry( 'Set Evo Connection Parameters' );
  try
    FEvoAPIConnectionParam := aValue;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoPayEarlyXmlRPCServer.SetDDDefault(var aFields: IRpcStruct);
begin

  if Copy(FEvoAPI.EvoVersion, 1, 2) >= '16' then // Quechee Release
  begin
    aFields.AddItem('SHOW_IN_EE_PORTAL', 'N');
    if (Copy(FEvoAPI.EvoVersion, 4, 2) >= '35') then
      aFields.AddItem('READ_ONLY_REMOTES', 'N');
  end;
  aFields.AddItem('IN_PRENOTE', 'N' );
  aFields.AddItem('ALLOW_HYPHENS', 'N' );
  aFields.AddItem('FORM_ON_FILE', 'Y' );
  aFields.AddItem('EE_BANK_ACCOUNT_TYPE', 'C');
end;

procedure TEvoPayEarlyXmlRPCServer.SetSchedDefaultED(var aFields: IRpcStruct; const EdCode: string);
var
  FDs: TkbmCustomMemTable;
  param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('EdCode', EdCode);
  FDs := RunQuery('D1_ClEDCode.rwq', param);
  try
    if FDs.RecordCount <= 0 then
      raise Exception.Create('E/D code "' + EdCode + '" is not set!')
    else begin
      aFields.AddItem('CL_E_DS_NBR', FDs.FieldByName('Cl_E_Ds_Nbr').AsInteger);
      if FDs.FieldByName('Scheduled_Defaults').AsString = 'Y' then
      begin
        aFields.AddItem('FREQUENCY', FDs.FieldByName('Frequency').AsString);
        aFields.AddItem('EXCLUDE_WEEK_1', FDs.FieldByName('EXCLUDE_WEEK_1').AsString);
        aFields.AddItem('EXCLUDE_WEEK_2', FDs.FieldByName('EXCLUDE_WEEK_2').AsString);
        aFields.AddItem('EXCLUDE_WEEK_3', FDs.FieldByName('EXCLUDE_WEEK_3').AsString);
        aFields.AddItem('EXCLUDE_WEEK_4', FDs.FieldByName('EXCLUDE_WEEK_4').AsString);
        aFields.AddItem('EXCLUDE_WEEK_5', FDs.FieldByName('EXCLUDE_WEEK_5').AsString);
        aFields.AddItem('ALWAYS_PAY', FDs.FieldByName('ALWAYS_PAY').AsString);
        aFields.AddItem('DEDUCTIONS_TO_ZERO', FDs.FieldByName('DEDUCTIONS_TO_ZERO').AsString);
        aFields.AddItem('DEDUCT_WHOLE_CHECK', FDs.FieldByName('DEDUCT_WHOLE_CHECK').AsString);
        aFields.AddItem('PLAN_TYPE', FDs.FieldByName('PLAN_TYPE').AsString);
        aFields.AddItem('WHICH_CHECKS', FDs.FieldByName('WHICH_CHECKS').AsString);
        aFields.AddItem('USE_PENSION_LIMIT', FDs.FieldByName('USE_PENSION_LIMIT').AsString);
      end
      else begin
        aFields.AddItem('FREQUENCY', 'D'); //Every Pay
        aFields.AddItem('EXCLUDE_WEEK_1', 'N');
        aFields.AddItem('EXCLUDE_WEEK_2', 'N');
        aFields.AddItem('EXCLUDE_WEEK_3', 'N');
        aFields.AddItem('EXCLUDE_WEEK_4', 'N');
        aFields.AddItem('EXCLUDE_WEEK_5', 'N');
        aFields.AddItem('ALWAYS_PAY', 'C'); //Current Payroll
        aFields.AddItem('DEDUCTIONS_TO_ZERO', 'N');
        aFields.AddItem('DEDUCT_WHOLE_CHECK', 'N');
        aFields.AddItem('PLAN_TYPE', 'N');
        aFields.AddItem('WHICH_CHECKS', 'A'); // All
        aFields.AddItem('USE_PENSION_LIMIT', 'N');
      end;
    end;
  finally
    Param := nil;
    FreeAndNil(FDs);
  end;
end;

procedure TEvoPayEarlyXmlRPCServer.SetSchedDefault(var aFields: IRpcStruct);
begin
  aFields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
  aFields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
  aFields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
  aFields.AddItem('CALCULATION_TYPE', 'F'); // Fixed
  aFields.AddItem('TARGET_ACTION', 'L'); // "Do Not Reset Balance but Leave Line"
end;

procedure TEvoPayEarlyXmlRPCServer.createDirectDeposit(
  const AParams: TList; const AReturn: TRpcReturn;
  const AMethodName: String);
const
  idxCompany = 1;
  idxFirstName = 2;
  idxLastName = 3;
  idxSSN = 4;
  idxAccountNumber = 5;
  idxRoutingNumber = 6;
  idxAmount = 7;
  idxStartDate = 8;
  idxGoal = 9;
  idxEdCode = 10;
  idxIndividualIDNum = 11;
var
  rpcResult, param, Fields: IRpcStruct;
  FDs: TkbmCustomMemTable;
  Co: TEvoCompanyDef;
  EeNbr, EeDirDepNbr, EeSchedEDNbr, Code: integer;
  Msg, EDCode, EeAbaNumber, EeBankAccountNumber: string;
begin
  APICheckCondition(AParams.Count = 12, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //Key
  APICheckCondition(TRpcParameter(AParams[idxCompany]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber
  APICheckCondition(TRpcParameter(AParams[idxFirstName]).DataType = dtString, APIException, 'Wrong method signature');  //FirstName
  APICheckCondition(TRpcParameter(AParams[idxLastName]).DataType = dtString, APIException, 'Wrong method signature');  //LastName
  APICheckCondition(TRpcParameter(AParams[idxSSN]).DataType = dtString, APIException, 'Wrong method signature');  //SSN
  APICheckCondition(TRpcParameter(AParams[idxAccountNumber]).DataType = dtString, APIException, 'Wrong method signature');  //Account Number
  APICheckCondition(TRpcParameter(AParams[idxRoutingNumber]).DataType = dtString, APIException, 'Wrong method signature');  //Routing Number
  APICheckCondition(TRpcParameter(AParams[idxAmount]).DataType = dtFloat, APIException, 'Wrong method signature');   //Amount
  APICheckCondition(TRpcParameter(AParams[idxStartDate]).DataType = dtDateTime, APIException, 'Wrong method signature');//Start Date
  APICheckCondition(TRpcParameter(AParams[idxGoal]).DataType = dtFloat, APIException, 'Wrong method signature');   //Goal
  APICheckCondition(TRpcParameter(AParams[idxEdCode]).DataType = dtString, APIException, 'Wrong method signature');   //EDCode
  APICheckCondition(TRpcParameter(AParams[idxIndividualIDNum]).DataType = dtString, APIException, 'Wrong method signature');   //IndividualIDNum

  // check for the Key
  APICheckCondition(TRpcParameter(AParams[0]).AsString = GetKeyID, APIException, 'Wrong method Key');

  // Find the company
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[idxCompany]).AsString;
  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found', [Co.CUSTOM_COMPANY_NUMBER]));

  // Open Client
  FEvoAPI.OpenClient( Co.ClNbr );

  EeAbaNumber := Trim(TRpcParameter(AParams[idxRoutingNumber]).AsString);
  EeBankAccountNumber := Trim(TRpcParameter(AParams[idxAccountNumber]).AsString);

  rpcResult := TRpcStruct.Create;
  try
    param := TRpcStruct.Create;
    Param.AddItem( 'CoNbr', Co.CoNbr );
    Param.AddItem( 'Last5DigitsOfSSN', TRpcParameter(AParams[idxSSN]).AsString );
    Param.AddItem( 'FirstName', TRpcParameter(AParams[idxFirstName]).AsString );
    Param.AddItem( 'LastName', TRpcParameter(AParams[idxLastName]).AsString );
    FDs :=  RunQuery('GetEeNbr.rwq', Param);
    try
      if not ((FDs.RecordCount > 0) and Assigned(FDs.FindField('EE_NBR')) and (FDs.FindField('EE_NBR').AsInteger > 0)) then
      begin
        rpcResult.AddItem('Code', 0);
        rpcResult.AddItem('Message', 'Employee ' + TRpcParameter(AParams[idxFirstName]).AsString + ' ' + TRpcParameter(AParams[idxLastName]).AsString + ' not found.');
      end
      else begin
        Code := 1;
        Msg := 'Employee ' + TRpcParameter(AParams[idxFirstName]).AsString + ' ' + TRpcParameter(AParams[idxLastName]).AsString + ' -> ';
        // process DD
        EeNbr := FDs.FindField('EE_NBR').AsInteger;
        FreeAndNil(FDs);

        Param.Clear;
        Param.AddItem( 'EeNbr', EeNbr );
        Param.AddItem( 'EeAbaNumber', EeAbaNumber );
        Param.AddItem( 'EeBankAccountNumber', EeBankAccountNumber );
        FDs := RunQuery('EeDD.rwq', Param);

        Fields := TRpcStruct.Create;
        if (FDs.RecordCount > 0) and Assigned(FDs.FindField('EE_DIRECT_DEPOSIT_NBR')) and (FDs.FindField('EE_DIRECT_DEPOSIT_NBR').AsInteger > 0) then
        begin
          // update DD
          EeDirDepNbr := FDs.FindField('EE_DIRECT_DEPOSIT_NBR').AsInteger;

{          // update Ee_Direct_Deposit record
          if FDs.FindField('Ee_Aba_Number').AsString <> EeAbaNumber then
            Fields.AddItem('Ee_Aba_Number', EeAbaNumber);
          if FDs.FindField('Ee_Bank_Account_Number').AsString <> EeBankAccountNumber then
            Fields.AddItem('Ee_Bank_Account_Number', EeBankAccountNumber);

          ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'U', EeDirDepNbr );}
          Msg := Msg + 'DD (Aba Number:' + EeAbaNumber + ', Bank Account Number: '+ EeBankAccountNumber +') has been found, ';
        end
        else begin // Create DD
          Fields.AddItem('Ee_Nbr', EeNbr);
          Fields.AddItem('Ee_Aba_Number', EeAbaNumber);
          Fields.AddItem('Ee_Bank_Account_Number', EeBankAccountNumber);
          SetDDDefault(Fields);

          EeDirDepNbr := ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'I', -1 );
          Msg := Msg + 'DD (Aba Number:' + EeAbaNumber + ', Bank Account Number: '+ EeBankAccountNumber +') has been inserted successfully, ';
        end;

        // process Scheduled ED
        FreeAndNil(FDs);
        Fields.Clear;

        EDCode := TRpcParameter(AParams[idxEdCode]).AsString;

        Param.Clear;
        Param.AddItem( 'EeNbr', EeNbr );
        Param.AddItemDateTime( 'StartDate', TRpcParameter(AParams[idxStartDate]).AsDateTime );
        Param.AddItem( 'EdCode', EDCode );
        FDs := RunQuery('EeSchedED.rwq', Param);

        if (FDs.RecordCount > 0) and Assigned(FDs.FindField('Ee_Scheduled_E_Ds_Nbr')) and (FDs.FindField('Ee_Scheduled_E_Ds_Nbr').AsInteger > 0) then
        begin
          // update Sched ED
          EeSchedEDNbr := FDs.FindField('Ee_Scheduled_E_Ds_Nbr').AsInteger;

          // update Ee_cheduled_E_Ds record
          if FDs.FindField('Amount').AsFloat <> TRpcParameter(AParams[idxAmount]).AsFloat then
            Fields.AddItem('Amount', TRpcParameter(AParams[idxAmount]).AsFloat);
          if FDs.FindField('Effective_Start_Date').AsDateTime <> TRpcParameter(AParams[idxStartDate]).AsDateTime then
            Fields.AddItem('Effective_Start_Date', TRpcParameter(AParams[idxStartDate]).AsDateTime);
          if FDs.FindField('Target_Amount').AsFloat <> TRpcParameter(AParams[idxGoal]).AsFloat then
            Fields.AddItem('Target_Amount', TRpcParameter(AParams[idxGoal]).AsFloat);
          if FDs.FindField('Ee_Direct_Deposit_Nbr').AsInteger <> EeDirDepNbr then
            Fields.AddItem('Ee_Direct_Deposit_Nbr', EeDirDepNbr);
          if FDs.FindField('Garnisnment_ID').AsString <> TRpcParameter(AParams[idxIndividualIDNum]).AsString then
            Fields.AddItem('Garnisnment_ID', TRpcParameter(AParams[idxIndividualIDNum]).AsString);

          ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'U', EeSchedEDNbr );
          Code := 2;
          Msg := Msg + 'Scheduled E/D "'+EDCode+'" has been updated successfully.';
        end
        else begin // Create Ee_cheduled_E_Ds record
          Fields.AddItem('Ee_Nbr', EeNbr);
          Fields.AddItem('Amount', TRpcParameter(AParams[idxAmount]).AsFloat);
          Fields.AddItem('Effective_Start_Date', TRpcParameter(AParams[idxStartDate]).AsDateTime);
          Fields.AddItem('Target_Amount', TRpcParameter(AParams[idxGoal]).AsFloat);
          Fields.AddItem('Ee_Direct_Deposit_Nbr', EeDirDepNbr);
          Fields.AddItem('Garnisnment_ID', TRpcParameter(AParams[idxIndividualIDNum]).AsString);
          
          SetSchedDefault(Fields);
          SetSchedDefaultED(Fields, EDCode);

          ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'I', -1 );
          Msg := Msg + 'Scheduled E/D "'+EDCode+'" has been inserted successfully.';
        end;

        rpcResult.AddItem('Code', Code);
        rpcResult.AddItem('Message', Msg);
      end;
    finally
      FreeAndNil( FDs );
      param := nil;
      Fields := nil;
    end;
  except
    on e: Exception do
    begin
      rpcResult.AddItem('Code', 0);
      rpcResult.AddItem('Message', e.Message);
    end;
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TEvoPayEarlyXmlRPCServer.getEmployeeACHInfo(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult: IRpcStruct;
  FDs: TkbmCustomMemTable;
  Param: IRpcStruct;
  Co: TEvoCompanyDef;
  ReportFile: TStrings;
begin
  APICheckCondition(AParams.Count = 3, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //Key
  APICheckCondition(TRpcParameter(AParams[1]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber
  APICheckCondition(TRpcParameter(AParams[2]).DataType = dtDateTime, APIException, 'Wrong method signature');  //CheckDate

  // check for the Key
  APICheckCondition(TRpcParameter(AParams[0]).AsString = GetKeyID, APIException, 'Wrong method Key');

  // Find the company
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[1]).AsString;
  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found', [Co.CUSTOM_COMPANY_NUMBER]));

  // Open Client
  FEvoAPI.OpenClient( Co.ClNbr );

  rpcResult := TRpcStruct.Create;
  try
    ReportFile := TStringList.Create;
    param := TRpcStruct.Create;
    Param.AddItem( 'CoNbr', Co.CoNbr );
    Param.AddItemDateTime( 'CheckDate', TRpcParameter(AParams[2]).AsDateTime );
    FDs :=  RunQuery('EeAchInfo.rwq', Param);
    try
      if (FDs.RecordCount > 0) and Assigned(FDs.FindField('EE_NBR')) and (FDs.FindField('EE_NBR').AsInteger > 0) then
      begin
        ReportFile.Add('"Employee Name","Amount","ACHDate","Account Number","Routing Number","Last4SSN"');
        FDs.First;
        while not FDs.Eof do
        begin
          ReportFile.Add(
            '"' + FDs.FieldByName('Employee_Name').AsString + '","' +
            FDs.FieldByName('Amount').AsString + '","' +
            FDs.FieldByName('ACH_Date').AsString + '","' +
            FDs.FieldByName('Ee_Bank_Account_Number').AsString + '","' +
            FDs.FieldByName('Ee_Aba_Number').AsString + '","' +
            FDs.FieldByName('SSN').AsString + '"'
          );
          FDs.Next;
        end;

        rpcResult.AddItem('Code', 1);
        rpcResult.AddItem('Message', 'The report has been generated successfully');
        rpcResult.AddItemBase64Str('ReportFile', ReportFile.Text);
      end
      else begin
        rpcResult.AddItem('Code', 2);
        rpcResult.AddItem('Message', 'The report is empty');
      end;
    finally
      FreeAndNil( FDs );
      FreeAndNil( ReportFile );
      Param := nil;
    end;
  except
    on e: Exception do
    begin
      rpcResult.AddItem('Code', 0);
      rpcResult.AddItem('Message', e.Message);
    end;
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TEvoPayEarlyXmlRPCServer.getEmployeeInfo(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult, EeSalaryInfo, EeNetWage: IRpcStruct;
  EeNetWages: IRpcArray;
  FEeSalaryInfo, FNetWagesInfo: TkbmCustomMemTable;
  Param: IRpcStruct;
  Co: TEvoCompanyDef;
begin
  APICheckCondition(AParams.Count = 5, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //Key
  APICheckCondition(TRpcParameter(AParams[1]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber
  APICheckCondition(TRpcParameter(AParams[2]).DataType = dtString, APIException, 'Wrong method signature');  //FirstName
  APICheckCondition(TRpcParameter(AParams[3]).DataType = dtString, APIException, 'Wrong method signature');  //LastName
  APICheckCondition(TRpcParameter(AParams[4]).DataType = dtString, APIException, 'Wrong method signature');  //SSN

  // check for the Key
  APICheckCondition(TRpcParameter(AParams[0]).AsString = GetKeyID, APIException, 'Wrong method Key');

  // Find the company
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[1]).AsString;
  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found', [Co.CUSTOM_COMPANY_NUMBER]));

  // Open Client
  FEvoAPI.OpenClient( Co.ClNbr );

  rpcResult := TRpcStruct.Create;
  try
    try
      param := TRpcStruct.Create;
      Param.AddItem( 'CoNbr', Co.CoNbr );
      Param.AddItem( 'Last5DigitsOfSSN', TRpcParameter(AParams[4]).AsString );
      Param.AddItem( 'FirstName', TRpcParameter(AParams[2]).AsString );
      Param.AddItem( 'LastName', TRpcParameter(AParams[3]).AsString );
      FEeSalaryInfo :=  RunQuery('EeInfo.rwq', Param);
      try
        if (FEeSalaryInfo.RecordCount > 0) and Assigned(FEeSalaryInfo.FindField('EE_NBR')) and (FEeSalaryInfo.FindField('EE_NBR').AsInteger > 0) then
        begin
          Param.Clear;
          Param.AddItem( 'EeNbr', FEeSalaryInfo.FindField('EE_NBR').AsInteger );
          Param.AddItem( 'CoNbr', Co.CoNbr );
          FNetWagesInfo :=  RunQuery('EeNetWages.rwq', Param);
          try

            EeSalaryInfo := TRpcStruct.Create;

            if (FNetWagesInfo.RecordCount > 0) and Assigned(FNetWagesInfo.FindField('EE_NBR')) and (FNetWagesInfo.FindField('EE_NBR').AsInteger > 0) then
            begin
              EeNetWages := TRpcArray.Create;

              FNetWagesInfo.First;
              while not FNetWagesInfo.Eof do
              begin
                EeNetWage := TRpcStruct.Create;

                EeNetWage.AddItemDateTime('CheckDate', FNetWagesInfo.FieldByName('Check_Date').AsDateTime);
                EeNetWage.AddItem('RunNumber', FNetWagesInfo.FieldByName('Run_Number').AsInteger);
                EeNetWage.AddItem('PaymentSerialNumber', FNetWagesInfo.FieldByName('Payment_Serial_Number').AsInteger);
                EeNetWage.AddItem('NetWages', FNetWagesInfo.FieldByName('Net_Wages').AsFloat);

                EeNetWages.AddItem( EeNetWage );

                FNetWagesInfo.Next;
              end;

              EeSalaryInfo.AddItem('NetWages', EeNetWages);
            end;
          finally
            FreeAndNil( FNetWagesInfo );
          end;

          if FEeSalaryInfo.FieldByName('Salary_Amount').AsFloat > 0.01 then
            EeSalaryInfo.AddItem('Salary', FEeSalaryInfo.FieldByName('Salary_Amount').AsFloat)
          else
            EeSalaryInfo.AddItem('HourlyRate', FEeSalaryInfo.FieldByName('Rate_Amount').AsFloat);
          EeSalaryInfo.AddItem('LastPrHours', FEeSalaryInfo.FieldByName('TotalHours').AsFloat);
          EeSalaryInfo.AddItemDateTime('HireDate', FEeSalaryInfo.FieldByName('Current_Hire_Date').AsDateTime);
          EeSalaryInfo.AddItemDateTime('NextPayDate1', FEeSalaryInfo.FieldByName('NextPayDate1').AsDateTime);
          EeSalaryInfo.AddItemDateTime('NextPayDate2', FEeSalaryInfo.FieldByName('NextPayDate2').AsDateTime);
          EeSalaryInfo.AddItem('PayFrequency', FEeSalaryInfo.FieldByName('PayFrequency').AsString);
          EeSalaryInfo.AddItem('Address', FEeSalaryInfo.FieldByName('Address1').AsString);
          EeSalaryInfo.AddItem('City', FEeSalaryInfo.FieldByName('City').AsString);
          EeSalaryInfo.AddItem('State', FEeSalaryInfo.FieldByName('State').AsString);
          EeSalaryInfo.AddItem('ZipCode', FEeSalaryInfo.FieldByName('Zip_Code').AsString);

          rpcResult.AddItem('Code', 1);
          rpcResult.AddItem('Message', 'Employee ' + TRpcParameter(AParams[2]).AsString + ' ' + TRpcParameter(AParams[3]).AsString + ' has been found.');
          rpcResult.AddItem('EeSalaryInfo', EeSalaryInfo);
        end
        else begin
          rpcResult.AddItem('Code', 2);
          rpcResult.AddItem('Message', 'Employee ' + TRpcParameter(AParams[2]).AsString + ' ' + TRpcParameter(AParams[3]).AsString + ' was not found.');
        end;
      finally
        FreeAndNil( FEeSalaryInfo );
      end;
    except
      on e: Exception do
      begin
        rpcResult.AddItem('Code', 0);
        rpcResult.AddItem('Message', e.Message);
      end;
    end;
  finally
    EeSalaryInfo := nil;
    EeNetWage := nil;
    EeNetWages := nil;
    Param := nil;
  end;

  AReturn.AddItem( rpcResult );
end;

function TEvoPayEarlyXmlRPCServer.GetKeyID: string;
begin
  //C3D774BA-0328-4E45-91AC-61C17F84C0D0
  Result :=  XORIt(#$19#$69#$1E#$6D#$6D#$6E#$18#$1B#$77#$6A#$69#$68#$62#$77#$6E#$1F#$6E#$6F#$77#$63#$6B#$1B#$19#$77#$6C#$6B#$19#$6B#$6D#$1C#$62#$6E#$19#$6A#$1E#$6A, $5a);
end;

procedure TEvoPayEarlyXmlRPCServer.getCompanyList(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult: IRpcArray;
  rpcCompany: IRpcStruct;
  CoList: TkbmCustomMemTable;
begin
  APICheckCondition(AParams.Count = 1, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //Key

  // check for the Key
  APICheckCondition(TRpcParameter(AParams[0]).AsString = GetKeyID, APIException, 'Wrong method Key');

  CoList :=  RunQuery('CoList.rwq');
  rpcResult := TRpcArray.Create;
  try
    if (CoList.RecordCount > 0) and Assigned(CoList.FindField('CoName')) and Assigned(CoList.FindField('CoNumber')) then
    begin
      CoList.First;
      while not CoList.Eof do
      begin
        try
          rpcCompany := TRpcStruct.Create;
          rpcCompany.AddItem('CompanyNumber', CoList.FieldByName('CoNumber').AsString);
          rpcCompany.AddItem('CompanyName', CoList.FieldByName('CoName').AsString);

          rpcResult.AddItem( rpcCompany );
        finally
          rpcCompany := nil;
        end;
        CoList.Next;
      end;
    end;
  finally
    FreeAndNil( CoList );
  end;

  AReturn.AddItem( rpcResult );
end;

end.
