unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ShellAPI, ActnList, Menus, ImgList, isSettings, common, Log,
  ExtCtrls, StdCtrls, Buttons, EvoPayEarlyConstAndProc, EvoPayEarlyServerMod;

const
  WM_MYTRAYNOTIFY = WM_USER + 123;

  cGreenIcon = 0;
  cBlueIcon = 1;
  cRedIcon = 2;
  cYellowIcon = 3;

type
  TfrmMain = class(TForm, IMessenger)
    PopupMenu: TPopupMenu;
    Actions: TActionList;
    StartServer: TAction;
    Start1: TMenuItem;
    Status: TAction;
    Status1: TMenuItem;
    Settings: TAction;
    Settings1: TMenuItem;
    Exit: TAction;
    N1: TMenuItem;
    Exit1: TMenuItem;
    Images: TImageList;
    ViewLog: TAction;
    ViewLog1: TMenuItem;
    pnlMain: TPanel;
    mmMessages: TMemo;
    btnStart: TBitBtn;
    btnSetting: TBitBtn;
    btnLog: TBitBtn;
    procedure StartServerExecute(Sender: TObject);
    procedure StatusExecute(Sender: TObject);
    procedure SettingsExecute(Sender: TObject);
    procedure ExitExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ViewLogExecute(Sender: TObject);
    procedure StartServerUpdate(Sender: TObject);
  private
    TrayIconData: TNotifyIconData;
    FCanClose: boolean;
    FSettings: IisSettings;
    FXmlRpcSrv: TEvoPayEarlyXmlRPCServer;

    procedure AddTrayIcon;
    procedure DeleteTrayIcon;
    procedure SetIcon(aIndex: integer);

    procedure AddMessage(const aMsg: string; IsError: boolean = false);
    procedure ClearMessages;

    procedure RunServer;
  public
    procedure TrayMessage(var Msg: TMessage); message WM_MYTRAYNOTIFY;

    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmMain: TfrmMain;
  frmLogger: TfrmLogger;

implementation

{$R *.dfm}

uses EvoConnectionDlg, gdyDeferredCall;

procedure TfrmMain.TrayMessage(var Msg: TMessage);
var
  p : TPoint;
begin
  case Msg.lParam of
    WM_LBUTTONDOWN:
      Status.Execute;
    WM_RBUTTONDOWN:
    begin
      SetForegroundWindow(Handle);
      GetCursorPos(p);
      PopUpMenu.Popup(p.x, p.y);
      PostMessage(Handle, WM_NULL, 0, 0);
    end;
  end;
end;

procedure TfrmMain.StartServerExecute(Sender: TObject);
begin
  if assigned(FXmlRpcSrv) then
  try
    SetIcon( cYellowIcon );

    if FXmlRpcSrv.Active then
    begin
      ClearMessages;
      AddMessage('Restart server...');
    end;
    FXmlRpcSrv.Active := False;

    FXmlRpcSrv.Active := True;
  finally
    if FXmlRpcSrv.Active then
      SetIcon( cGreenIcon )
    else
      SetIcon( cRedIcon );
  end
end;

procedure TfrmMain.StatusExecute(Sender: TObject);
begin
  Self.Show;
end;

procedure TfrmMain.SettingsExecute(Sender: TObject);
begin

  if TfrmEvoConnectionDlg.ShowDialog( frmLogger.Logger ) = mrOk then
  begin
    AddMessage('Restart server...');
    FXmlRpcSrv.EvoConnectionParam := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn );
    RunServer;
  end  
end;

procedure TfrmMain.ExitExecute(Sender: TObject);
begin
  FCanClose := True;
  Close;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := FCanClose;
  if not CanClose then
    Hide;
end;

procedure TfrmMain.AddTrayIcon;
var
  lIcon: TIcon;
begin
  lIcon := TIcon.Create;
  try
    with TrayIconData do
    begin
      cbSize := SizeOf(TrayIconData);
      Wnd := Handle;
      uID := 0;
      uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
      uCallbackMessage := WM_MYTRAYNOTIFY;
      Images.GetIcon(cRedIcon, lIcon);
      hIcon := lIcon.Handle;
      StrPCopy(szTip, Application.Title);
    end;
    FCanClose := False;

    Shell_NotifyIcon(NIM_ADD, @TrayIconData);
  finally
    FreeAndNil( lIcon );
  end;
end;

procedure TfrmMain.DeleteTrayIcon;
begin
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMain.SetIcon(aIndex: integer);
var
  Icon: TIcon;
begin
  Icon:=TIcon.Create;
  try
    Images.GetIcon(aIndex, Icon);
    TrayIconData.hIcon := Icon.Handle;
    Shell_NotifyIcon(NIM_Modify, @TrayIconData);
  finally
    Icon.Free;
  end;
end;

procedure TfrmMain.ViewLogExecute(Sender: TObject);
begin
  frmLogger.ShowModal;
end;

constructor TfrmMain.Create(Owner: TComponent);
begin
  frmLogger := TfrmLogger.Create( Application );
  inherited;
  Application.Title := 'EvoElastic (' + GetVersion + ')';
  Self.Caption := Application.Title;
  AddTrayIcon;

  FSettings := AppSettings;
  LoadFormSize(Self, FSettings);
  FXmlRpcSrv := TEvoPayEarlyXmlRPCServer.Create(frmLogger.Logger, Self);
  FXmlRpcSrv.EvoConnectionParam := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn );

  DeferredCall( RunServer );
end;                                     

destructor TfrmMain.Destroy;
begin
  DeleteTrayIcon;
  SaveFormSize(Self, FSettings);
  inherited;
  FreeAndNil( FXmlRpcSrv );
  FreeAndNil( frmLogger );
end;

procedure TfrmMain.AddMessage(const aMsg: string; IsError: boolean = false);
begin
  if IsError then
    frmLogger.Logger.LogError( aMsg )
  else
    frmLogger.Logger.LogEvent( aMsg );

  mmMessages.Lines.Add( aMsg );
end;

procedure TfrmMain.ClearMessages;
begin
  mmMessages.Lines.Clear;
end;

procedure TfrmMain.StartServerUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(FXmlRpcSrv);

  if Assigned(FXmlRpcSrv) then
  begin
    if FXmlRpcSrv.Active then
      (Sender as TCustomAction).Caption := 'Restart'
    else
      (Sender as TCustomAction).Caption := 'Start';
  end;
end;

procedure TfrmMain.RunServer;
begin
  StartServerExecute( Self );
end;

end.
