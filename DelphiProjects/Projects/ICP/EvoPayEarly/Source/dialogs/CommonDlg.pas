unit CommonDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, EvoPayEarlyConstAndProc, isSettings,
  gdyCommonLogger;

type
  TfrmDialog = class(TForm)
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    procedure btnOkClick(Sender: TObject);
  protected
    FSettings: IisSettings;
    FLogger: ICommonLogger;
    procedure DoOK; virtual;
    procedure BeforeShowDialog; virtual;
  public
    class function ShowDialog(aLogger: ICommonLogger): TModalResult;
  end;

implementation

{$R *.dfm}

uses common;

procedure TfrmDialog.BeforeShowDialog;
begin
  //
end;

procedure TfrmDialog.DoOK;
begin
  ModalResult := mrOk;
end;

class function TfrmDialog.ShowDialog(aLogger: ICommonLogger): TModalResult;
var
  frm: TfrmDialog;
begin
  frm := Self.Create(nil);
  with frm do
  try
    FSettings := AppSettings;
    FLogger := aLogger;
    LoadFormSize( frm, FSettings );
    BeforeShowDialog;
    Result := ShowModal;
    SaveFormSize( frm, FSettings );
  finally
    FreeAndNil( frm );
  end;
end;

procedure TfrmDialog.btnOkClick(Sender: TObject);
begin
  DoOK;
end;

end.
