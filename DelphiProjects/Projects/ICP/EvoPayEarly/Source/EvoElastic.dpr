program EvoElastic;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  main in 'main.pas' {frmMain},
  Log in 'Log.pas' {frmLogger},
  EvoPayEarlyConstAndProc in 'EvoPayEarlyConstAndProc.pas',
  CommonDlg in 'dialogs\CommonDlg.pas' {frmDialog},
  EvoConnectionDlg in 'dialogs\EvoConnectionDlg.pas' {frmEvoConnectionDlg},
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  EvoPayEarlyServerMod in 'EvoPayEarlyServerMod.pas',
  XmlRpcServer in 'XmlRpcServer.pas';

{$R *.res}

begin
  LicenseKey := 'B9C39C8BA053485C98499909772470EE'; //EvoPayEarly

  Application.Initialize;
  Application.ShowMainForm := False;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
