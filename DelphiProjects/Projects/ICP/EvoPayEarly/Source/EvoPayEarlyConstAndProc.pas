unit EvoPayEarlyConstAndProc;

interface

uses
  Forms, EvoAPIConnectionParamFrame, common, isSettings, gdyCommonLogger,
  sysutils, Windows, Classes, XmlRpcTypes, EvStreamUtils, DB, isBasicUtils,
  kbmMemTable;

const
  sMainForm = 'MainForm';
  sEvoConn = 'EvoConn';

type
  IMessenger = interface
    procedure AddMessage(const aMsg: string; IsError: boolean = false);
    procedure ClearMessages;
  end;

  TFrameClass = class of TFrame;
  TEvoAPIConnectionParamFrmClass = class of TEvoAPIConnectionParamFrm;

  TMapField = record
    N: string;         //Name RpcStruct member name
    EvoT: string;      //Evo Table
    EvoF: string;      //Evo Field
    T: TDataType;      //Field Type
    M: boolean;        //Mandatory
    H: boolean;        //Handler
  end;

const
  ClPersonStartIdx = 1;
  ClPersonEndIdx = 12;
  EeStartIdx = 13;
  EeEndIdx = 38;
  EeStatesStartIdx = 39;
  EeStatesEndIdx = 49;
  EeRatesStartIdx = 50;
  EeRatesEndIdx = 56;
  EeLocalsStartIdx = 57;
  EeLocalsEndIdx = 58;
  EeDDStartIdx = 59;
  EeDDEndIdx = 61;
  EeSchedStartIdx = 62;
  EeSchedEndIdx = 65;
  PieceWorkStartIdx = 66;
  PieceWorkEndIdx = 68;

  Mapping: array[1..68] of TMapField =
    (
    // Employee
      (N: 'LastName'; EvoT: 'CL_PERSON'; EvoF: 'LAST_NAME'; T: dtString; M: True; H: False), //1
      (N: 'FirstName'; EvoT: 'CL_PERSON'; EvoF: 'FIRST_NAME'; T: dtString; M: True; H: False),
      (N: 'MiddleInitial'; EvoT: 'CL_PERSON'; EvoF: 'MIDDLE_INITIAL'; T: dtString; M: False; H: False),
      (N: 'TaxPayerID'; EvoT: 'CL_PERSON'; EvoF: 'SOCIAL_SECURITY_NUMBER'; T: dtString; M: True; H: False),
      (N: 'Address1'; EvoT: 'CL_PERSON'; EvoF: 'ADDRESS1'; T: dtString; M: True; H: False),
      (N: 'City'; EvoT: 'CL_PERSON'; EvoF: 'CITY'; T: dtString; M: True; H: False),
      (N: 'State'; EvoT: 'CL_PERSON'; EvoF: 'STATE'; T: dtString; M: True; H: False),
      (N: 'ZipCode'; EvoT: 'CL_PERSON'; EvoF: 'ZIP_CODE'; T: dtString; M: True; H: False),
      (N: 'EmployeeType'; EvoT: 'CL_PERSON'; EvoF: 'EIN_OR_SOCIAL_SECURITY_NUMBER'; T: dtString; M: True; H: False),
      (N: 'Gender'; EvoT: 'CL_PERSON'; EvoF: 'GENDER'; T: dtString; M: True; H: False),
      (N: 'Race'; EvoT: 'CL_PERSON'; EvoF: 'ETHNICITY'; T: dtString; M: True; H: False),
      (N: 'BirthDate'; EvoT: 'CL_PERSON'; EvoF: 'BIRTH_DATE'; T: dtDateTime; M: False; H: False), //12

      (N: 'EeCode'; EvoT: 'EE'; EvoF: 'CUSTOM_EMPLOYEE_NUMBER'; T: dtString; M: True; H: False), //13
      (N: 'AnnualPayPeriods'; EvoT: 'EE'; EvoF: 'PAY_FREQUENCY'; T: dtString; M: True; H: False),
      (N: 'DeferredComp'; EvoT: 'EE'; EvoF: 'W2_DEFERRED_COMP'; T: dtString; M: True; H: False),
      (N: 'Pension'; EvoT: 'EE'; EvoF: 'W2_PENSION'; T: dtString; M: True; H: False),
      (N: 'FICA'; EvoT: 'EE'; EvoF: 'EXEMPT_EMPLOYEE_OASDI'; T: dtString; M: True; H: False),
      (N: 'FICA'; EvoT: 'EE'; EvoF: 'EXEMPT_EMPLOYER_OASDI'; T: dtString; M: True; H: False),
      (N: 'Medicare'; EvoT: 'EE'; EvoF: 'EXEMPT_EMPLOYEE_MEDICARE'; T: dtString; M: True; H: False),
      (N: 'Medicare'; EvoT: 'EE'; EvoF: 'EXEMPT_EMPLOYER_MEDICARE'; T: dtString; M: True; H: False),
      (N: 'FUTA'; EvoT: 'EE'; EvoF: 'EXEMPT_EMPLOYER_FUI'; T: dtString; M: True; H: False),
      (N: 'FedExemptions'; EvoT: 'EE'; EvoF: 'NUMBER_OF_DEPENDENTS'; T: dtInteger; M: True; H: False),
      (N: 'FedMaritalStatus'; EvoT: 'EE'; EvoF: 'FEDERAL_MARITAL_STATUS'; T: dtString; M: True; H: False),
      (N: 'FedTaxStatus'; EvoT: 'EE'; EvoF: 'EXEMPT_EXCLUDE_EE_FED'; T: dtString; M: True; H: False),
      (N: 'OverrideFedTaxType'; EvoT: 'EE'; EvoF: 'OVERRIDE_FED_TAX_TYPE'; T: dtString; M: True; H: False),
      (N: 'OverrideFedTaxValue'; EvoT: 'EE'; EvoF: 'OVERRIDE_FED_TAX_VALUE'; T: dtFloat; M: False; H: False),
      (N: 'EIC'; EvoT: 'EE'; EvoF: 'EIC'; T: dtString; M: False; H: False),
      (N: 'EmploymentType'; EvoT: 'EE'; EvoF: 'COMPANY_OR_INDIVIDUAL_NAME'; T: dtString; M: True; H: False),
      (N: 'TermDate'; EvoT: 'EE'; EvoF: 'CURRENT_TERMINATION_DATE'; T: dtDateTime; M: False; H: False),
      (N: 'HireDate'; EvoT: 'EE'; EvoF: 'CURRENT_HIRE_DATE'; T: dtDateTime; M: True; H: False),
      (N: 'PayChangeDate'; EvoT: 'EE'; EvoF: 'NEXT_RAISE_DATE'; T: dtDateTime; M: False; H: False),
      (N: 'PayLineStdHours'; EvoT: 'EE'; EvoF: 'STANDARD_HOURS'; T: dtFloat; M: False; H: False),
      (N: 'JobNumber'; EvoT: 'EE'; EvoF: 'CO_JOBS_NBR'; T: dtString; M: False; H: True),
      (N: 'WC_Code'; EvoT: 'EE'; EvoF: 'CO_WORKERS_COMP_NBR'; T: dtString; M: False; H: True),
      (N: 'PayrollState'; EvoT: 'EE'; EvoF: 'HOME_TAX_EE_STATES_NBR'; T: dtString; M: False; H: True),
      (N: 'HomeLocation'; EvoT: 'EE'; EvoF: 'CO_DIVISION_NBR'; T: dtString; M: False; H: True),
      (N: 'HomeBranch'; EvoT: 'EE'; EvoF: 'CO_BRANCH_NBR'; T: dtString; M: False; H: True),
      (N: 'HomeDepartment'; EvoT: 'EE'; EvoF: 'CO_DEPARTMENT_NBR'; T: dtString; M: False; H: True), //38

    // EeStates
      (N: 'State'; EvoT: 'EE_STATES'; EvoF: 'CO_STATES_NBR'; T: dtString; M: True; H: True), //39
      (N: 'SUI'; EvoT: 'EE_STATES'; EvoF: 'EE_SUI_EXEMPT_EXCLUDE'; T: dtString; M: True; H: False),
      (N: 'SUI'; EvoT: 'EE_STATES'; EvoF: 'ER_SUI_EXEMPT_EXCLUDE'; T: dtString; M: True; H: False),
      (N: 'SDI'; EvoT: 'EE_STATES'; EvoF: 'EE_SDI_EXEMPT_EXCLUDE'; T: dtString; M: True; H: False),
      (N: 'SDI'; EvoT: 'EE_STATES'; EvoF: 'ER_SDI_EXEMPT_EXCLUDE'; T: dtString; M: True; H: False),
      (N: 'StateExemptions'; EvoT: 'EE_STATES'; EvoF: 'STATE_NUMBER_WITHHOLDING_ALLOW'; T: dtInteger; M: False; H: False),
      (N: 'StateMaritalStatus'; EvoT: 'EE_STATES'; EvoF: 'STATE_MARITAL_STATUS'; T: dtString; M: True; H: False),
      (N: 'StateMaritalStatus'; EvoT: 'EE_STATES'; EvoF: 'IMPORTED_MARITAL_STATUS'; T: dtString; M: True; H: False),
      (N: 'StateTaxStatus'; EvoT: 'EE_STATES'; EvoF: 'STATE_EXEMPT_EXCLUDE'; T: dtString; M: True; H: False),
      (N: 'OverrideStateTaxType'; EvoT: 'EE_STATES'; EvoF: 'OVERRIDE_STATE_TAX_TYPE'; T: dtString; M: False; H: False),
      (N: 'UnemploymentTaxState'; EvoT: 'EE_STATES'; EvoF: 'SUI_APPLY_CO_STATES_NBR'; T: dtString; M: False; H: True), //49

    // EeRates
      (N: 'Position'; EvoT: 'EE_RATES'; EvoF: 'CO_HR_POSITIONS_NBR'; T: dtString; M: False; H: True), //50
      (N: 'RateNumber'; EvoT: 'EE_RATES'; EvoF: 'RATE_NUMBER'; T: dtInteger; M: True; H: False),
      (N: 'PrimaryRate'; EvoT: 'EE_RATES'; EvoF: 'PRIMARY_RATE'; T: dtString; M: True; H: False),
      (N: 'RateAmount'; EvoT: 'EE_RATES'; EvoF: 'RATE_AMOUNT'; T: dtFloat; M: True; H: False),
      (N: 'Location'; EvoT: 'EE_RATES'; EvoF: 'CO_DIVISION_NBR'; T: dtString; M: False; H: True),
      (N: 'Branch'; EvoT: 'EE_RATES'; EvoF: 'CO_BRANCH_NBR'; T: dtString; M: False; H: True),
      (N: 'Department'; EvoT: 'EE_RATES'; EvoF: 'CO_DEPARTMENT_NBR'; T: dtString; M: False; H: True), //56

    // EeLocals
      (N: 'LocalTaxLocality'; EvoT: 'EE_LOCALS'; EvoF: 'CO_LOCAL_TAX_NBR'; T: dtString; M: True; H: True), //57
      (N: 'LocalTax'; EvoT: 'EE_LOCALS'; EvoF: 'EXEMPT_EXCLUDE'; T: dtString; M: True; H: False), //58

    // Direct Deposits
      (N: 'AccountTransitNumber'; EvoT: 'EE_DIRECT_DEPOSIT'; EvoF: 'EE_ABA_NUMBER'; T: dtString; M: True; H: False), //59
      (N: 'AccountNumber'; EvoT: 'EE_DIRECT_DEPOSIT'; EvoF: 'EE_BANK_ACCOUNT_NUMBER'; T: dtString; M: True; H: False),
      (N: 'AccountType'; EvoT: 'EE_DIRECT_DEPOSIT'; EvoF: 'EE_BANK_ACCOUNT_TYPE'; T: dtString; M: True; H: False), //61

    // Scheduled E/Ds
      (N: 'EDCode'; EvoT: 'EE_SCHEDULED_E_DS'; EvoF: 'CL_E_DS_NBR'; T: dtString; M: True; H: True), //62
      (N: 'Amount'; EvoT: 'EE_SCHEDULED_E_DS'; EvoF: 'AMOUNT'; T: dtFloat; M: True; H: True),
      (N: 'GoalAmount'; EvoT: 'EE_SCHEDULED_E_DS'; EvoF: 'TARGET_AMOUNT'; T: dtFloat; M: False; H: False),
      (N: 'GoalType'; EvoT: 'EE_SCHEDULED_E_DS'; EvoF: 'TARGET_ACTION'; T: dtString; M: True; H: False), //65

    // Piece Work
      (N: 'ItemName'; EvoT: 'EE_PIECE_WORK'; EvoF: 'CL_PIECES_NBR'; T: dtString; M: True; H: True), //66
      (N: 'Number'; EvoT: 'EE_PIECE_WORK'; EvoF: 'RATE_QUANTITY'; T: dtFloat; M: True; H: False),
      (N: 'Amount'; EvoT: 'EE_PIECE_WORK'; EvoF: 'RATE_AMOUNT'; T: dtFloat; M: True; H: False) //68
    );

procedure SaveFormSize(aForm: TForm; aSettings: IisSettings);
procedure LoadFormSize(aForm: TForm; aSettings: IisSettings);

function IntConvertNull(MyValue: Variant; Replacement: integer): integer;
function ConvertNull(MyValue, Replacement: Variant): Variant;
function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
function GetNextStrValue(var AStr: string; const ADelim: String = ','): String;

function GetEvoAccountsAsString: string;
procedure SetEvoAccountsAsString(const aStr: string);

procedure LoadEvoAccounts(aDS: TDataset);
procedure SaveEvoAccounts(aDS: TDataset);
function CreateEvoAccountsDataset: TkbmCustomMemTable;

implementation

uses gdyCommon, gdyCrypt, Variants, DateUtils, gdyRedir;

const
  //!! encrypt it?
  cKey='LaIslaBonita';

procedure LoadEvoAccounts(aDS: TDataset);
var
  sl: TStrings;
  i: integer;
  s: string;
begin
  if not aDS.Active then
    aDS.Open;

  sl := TStringList.Create;
  try
    sl.Text := GetEvoAccountsAsString;
    for i := 0 to (sl.Count - 1) do
    begin
      s := sl[i];

      aDS.Append;
      aDS.FieldByName('ApiAdapter').AsString := GetNextStrValue(s, #9);
      aDS.FieldByName('Username').AsString := GetNextStrValue(s, #9);
      aDS.FieldByName('Password').AsString := Decrypt(HexToBin(s), cKey);
      aDS.Post;
    end;
  finally
    FreeAndNil(sl);
  end;
end;

procedure SaveEvoAccounts(aDS: TDataset);
var
  sl: TStrings;
begin
  if not aDS.Active then
    aDS.Open;

  sl := TStringList.Create;
  try
    aDS.First;
    while not aDS.Eof do
    begin
      sl.Add(
        aDS.FieldByName('ApiAdapter').AsString + #9 +
        aDS.FieldByName('Username').AsString + #9 +
        BinToHex(Encrypt(aDS.FieldByName('Password').AsString, cKey))
      );
      aDS.Next;
    end;

    SetEvoAccountsAsString( sl.Text );
  finally
    FreeAndNil(sl);
  end;
end;

function CreateEvoAccountsDataset: TkbmCustomMemTable;
begin
  Result := TkbmCustomMemTable.Create(nil);
  CreateStringField(Result, 'ApiAdapter', 'Evo API Adapter', 128);
  CreateStringField(Result, 'Username', 'Evo Username', 128);
  CreateStringField(Result, 'Password', 'Password', 128);
end;

procedure SetEvoAccountsAsString(const aStr: string);
var
  conf: IisSettings;
  keypath: string;
begin
  keypath := 'SYSTEM\CurrentControlSet\Services\EvoElastic';
  conf := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, keypath);
  conf.AsString['EvoAccounts'] := aStr;
end;

function GetEvoAccountsAsString: string;
var
  conf: IisSettings;
  keypath: string;
begin
  keypath := 'SYSTEM\CurrentControlSet\Services\EvoElastic';
  conf := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, keypath);
  Result := conf.AsString['EvoAccounts'];
end;

function GetNextStrValue(var AStr: string; const ADelim: String = ','): String;
var
  j: Integer;
begin
  j := Pos(ADelim, AStr);
  if j = 0 then
  begin
    Result := AStr;
    AStr := '';
  end

  else
  begin
    Result := Copy(AStr, 1, j - 1);
    Delete(AStr, 1, j - 1 + Length(ADelim));
  end;
end;

function IntConvertNull(MyValue: Variant; Replacement: integer): integer;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function ConvertNull(MyValue, Replacement: Variant): Variant;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := Copy(MyString, 1, MyLength);
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

procedure SaveFormSize(aForm: TForm; aSettings: IisSettings);
var
  root: string;
begin
  root := aForm.Name;
  try
    if aForm.WindowState = wsMaximized then
      aSettings.AsInteger[root+'Maximized'] := 1
    else begin
      aSettings.AsInteger[root+'Maximized'] := 0;
      aSettings.AsInteger[root+'Top'] := aForm.Top;
      aSettings.AsInteger[root+'Left'] := aForm.Left;
      aSettings.AsInteger[root+'Width'] := aForm.Width;
      aSettings.AsInteger[root+'Height'] := aForm.Height;
    end;
  except
  end;
end;

procedure LoadFormSize(aForm: TForm; aSettings: IisSettings);
var
  root: string;
begin
  root := aForm.Name;
  try
    aForm.Top := aSettings.AsInteger[root+'Top'];
    aForm.Left := aSettings.AsInteger[root+'Left'];
    aForm.Width := aSettings.AsInteger[root+'Width'];
    aForm.Height := aSettings.AsInteger[root+'Height'];
    if aSettings.AsInteger[root+'Maximized'] = 1 then
      aForm.WindowState := wsMaximized;
  except
  end;
end;

end.
