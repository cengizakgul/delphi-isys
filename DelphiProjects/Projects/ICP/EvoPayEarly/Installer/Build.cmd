@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoElastic.msi 
"%pWiXDir%\candle.exe" .\EvoElastic.wxs -wx -out ..\..\..\..\..\Tmp\EvoElastic.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoElastic.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoElastic.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoElastic.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoElastic%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoElastic%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoElastic.msi EvoElastic%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvoElasticService.msi 
"%pWiXDir%\candle.exe" .\EvoElasticService.wxs -wx -out ..\..\..\..\..\Tmp\EvoElasticService.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoElasticService.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoElasticService.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoElasticService.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoElasticService%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoElasticService%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoElasticService.msi EvoElasticService%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1