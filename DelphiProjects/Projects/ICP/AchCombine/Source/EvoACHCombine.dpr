program EvoACHCombine;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  main in 'main.pas' {frmMain},
  about in 'about.pas' {frmAbout},
  license in 'license.pas' {frmLicense},
  wait in 'wait.pas' {frmWait};

{$R *.res}

begin
  Application.Initialize;
  if CheckLicense then
  begin
    Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
  end;
end.
