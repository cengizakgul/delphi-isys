unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, ComCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TfrmMain = class(TForm)
    Actions: TActionList;
    mmMemu: TMainMenu;
    miFile: TMenuItem;
    miHelp: TMenuItem;
    Exit: TAction;
    About: TAction;
    miExit: TMenuItem;
    miAbout: TMenuItem;
    sbStatus: TStatusBar;
    Combine: TAction;
    gbFiles: TGroupBox;
    lblFiles: TLabel;
    lbFiles: TListBox;
    lblOutPut: TLabel;
    edOutputFile: TEdit;
    sbtnOutputFile: TSpeedButton;
    lblTotalReport: TLabel;
    mmTotals: TMemo;
    pnlCombine: TPanel;
    btnCombine: TButton;
    btnAdd: TBitBtn;
    btnUp: TBitBtn;
    btnDown: TBitBtn;
    btnRemove: TBitBtn;
    Add: TAction;
    Up: TAction;
    Down: TAction;
    Remove: TAction;
    OutputFile: TAction;
    odAchFile: TOpenDialog;
    sdAchFile: TSaveDialog;
    procedure ExitExecute(Sender: TObject);
    procedure OutputFileExecute(Sender: TObject);
    procedure AddExecute(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure RemoveExecute(Sender: TObject);
    procedure UpExecute(Sender: TObject);
    procedure UpUpdate(Sender: TObject);
    procedure DownUpdate(Sender: TObject);
    procedure DownExecute(Sender: TObject);
    procedure CombineUpdate(Sender: TObject);
    procedure CombineExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure AboutExecute(Sender: TObject);
  private
    { Private declarations }
  public
    procedure CombineAch;
  end;

var
  frmMain: TfrmMain;

implementation

uses IniFiles, Math, About;

function GetIniFileName: string;
begin
  Result := Application.ExeName;
  Result := Copy(Result, 1, Length(Result) - 3) + 'ini';
end;

function ZeroFillInt64ToStr(n: Int64; len: Integer): string;
begin
  // 123,7 = "0000123"
  Result := IntToStr(n);
  if Length(Result) > len then
    Delete(Result, 1, Length(Result) - len);
  while Length(Result) < len do
    Result := '0' + Result;
end;

procedure IncHashTotal(aABA: string; var aTotal: Int64);
var
  iAba: Int64;
begin
  // make sure only first 8 digits are passed into total (9th is a check-digit)
  if Length(Trim(aABA)) = 9 then
    iAba := StrToInt64(Copy(aABA, 1, 8))
  else
    iAba := StrToInt64(aABA);

  aTotal := aTotal + iAba;
end;

{$R *.dfm}

procedure TfrmMain.ExitExecute(Sender: TObject);
begin
  frmMain.Close;
end;

procedure TfrmMain.OutputFileExecute(Sender: TObject);
begin
  if sdAchFile.Execute then
    edOutputFile.Text := sdAchFile.FileName;
end;

procedure TfrmMain.AddExecute(Sender: TObject);
var i: integer;
begin
  if odAchFile.Execute then
  for i := 0 to odAchFile.Files.Count - 1 do
    if lbFiles.Items.IndexOf( odAchFile.Files[i] ) < 0 then
      lbFiles.Items.Add( odAchFile.Files[i] );
end;

procedure TfrmMain.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbFiles.Items.Count > 0) and (lbFiles.ItemIndex >= 0);
end;

procedure TfrmMain.RemoveExecute(Sender: TObject);
begin
  lbFiles.Items.Delete( lbFiles.ItemIndex );
end;

procedure TfrmMain.UpExecute(Sender: TObject);
begin
  lbFiles.Items.Exchange( lbFiles.ItemIndex, lbFiles.ItemIndex - 1 );
end;

procedure TfrmMain.UpUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbFiles.Items.Count > 0) and (lbFiles.ItemIndex >= 1);
end;

procedure TfrmMain.DownUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbFiles.Items.Count > 0) and (lbFiles.ItemIndex >= 0) and (lbFiles.ItemIndex < (lbFiles.Items.Count - 1));
end;

procedure TfrmMain.DownExecute(Sender: TObject);
begin
  lbFiles.Items.Exchange( lbFiles.ItemIndex, lbFiles.ItemIndex + 1 );
end;

procedure TfrmMain.CombineUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbFiles.Items.Count > 1) and (edOutputFile.Text <> '');
end;

procedure TfrmMain.CombineExecute(Sender: TObject);
begin
  CombineAch;
end;

procedure TfrmMain.CombineAch;
var
  ACH, CurrentFile: TStringList;
  i, j, k, z, DetailRecords, BatchRecords: integer;
  Hash, Db, Cr: Int64;
  s, H, F: string;
begin
  mmTotals.Lines.Clear;
  mmTotals.Lines.Add( 'ACHcombine Control Totals Report' );
  mmTotals.Lines.Add( '--------------------------------' );
  mmTotals.Lines.Add( DateTimeToStr( Now ) );

  H := '';
  F := '';

  ACH := TStringList.Create;
  CurrentFile := TStringList.Create;
  try
    try
      DetailRecords := 0;
      BatchRecords := 0;
      Hash := 0;
      DB := 0;
      CR := 0;
      z := 0;
      for i := 0 to lbFiles.Items.Count - 1 do
      begin
        CurrentFile.LoadFromFile( lbFiles.Items[i] );

        j := 0;
        if CurrentFile.Count > 0 then
        repeat
          s := CurrentFile[j];
          if Copy(S, 1, 5) = '$$ADD' then
          begin
            if (i = 0) then
              H := s;
            j := j + 1;
            s := CurrentFile[j];
          end;

          if Length( s ) <> 94 then
            raise Exception.Create( 'This doesn''t appear to be an ACH file:' + #10#13 + lbFiles.Items[i] + #10#13 + 'Program will end.' );

          // check the record type and which file we're processing
          // if first file, keep the header.
          // if last file, then write a new footer

          if (s[1] = '5') or (s[1] = '8') then
          begin
            if (s[1] = '5') then
              BatchRecords := BatchRecords + 1;
            s := Copy(s, 1, 87) + ZeroFillInt64ToStr(BatchRecords, 7);
          end
          else if (s[1] = '9') then
          begin
            DetailRecords := DetailRecords + StrToInt(Copy(s, 14, 8));
            IncHashTotal(Copy(s, 22, 10), Hash);
            Db := StrToInt64(Copy(s, 32, 12)) + Db;
            Cr := StrToInt64(Copy(s, 44, 12)) + Cr;
          end;

          if ((s[1] <> '1') or (i = 0)) and (s[1] <> '9') then
            ACH.Add( s )
          else if (s[1] = '9') and (i = (lbFiles.Items.Count - 1)) then
          begin
            s := '9' + ZeroFillInt64ToStr(BatchRecords, 6)
              + ZeroFillInt64ToStr((ACH.Count + 10) div 10, 6) + ZeroFillInt64ToStr(DetailRecords, 8)
              + ZeroFillInt64ToStr(Hash, 10) + ZeroFillInt64ToStr(Db, 12)
              + ZeroFillInt64ToStr(Cr, 12) + StringOfChar(' ', 39);

            ACH.Add(s);
            z := ACH.Count; // processed records;

            if ACH.Count mod 10 > 0 then
              for k := 1 to 10 - (ACH.Count mod 10) do
                ACH.Add( StringOfChar('9', 94) );

            j := CurrentFile.Count - 1;
            s := CurrentFile[j];
            if Copy(s, 1, 5) = '$$END' then
              F := s;
          end;
          j := j + 1;
        until (j = CurrentFile.Count) or (s[1] = '9');
      end;

      mmTotals.Lines.Add( IntToStr(lbFiles.Items.Count) + ' files processed.' );
      mmTotals.Lines.Add( edOutputFile.Text );
      mmTotals.Lines.Add( 'has been created with ' + IntToStr(z) + ' detail records.' );
      mmTotals.Lines.Add( 'Batch Count ' + IntToStr(BatchRecords) );
      mmTotals.Lines.Add( 'Block Count ' + IntToStr(Ach.Count div 10) );
      mmTotals.Lines.Add( 'Entry Count ' + IntToStr(DetailRecords) );
      mmTotals.Lines.Add( 'Entry Hash ' + IntToStr(Hash) );
      mmTotals.Lines.Add( 'Total Debits: $' + FloatToStr(Db/100) );
      mmTotals.Lines.Add( 'Total Credits: $' + FloatToStr(Cr/100) );

      if F <> '' then ACH.Add(F);
      if H <> '' then ACH.Insert(0, H);
      ACH.SaveToFile( edOutputFile.Text );

      s := ExtractFilePath( edOutputFile.Text ) + '\ACHcontrolTotals.txt';
      mmTotals.Lines.SaveToFile( s );

      mmTotals.Lines.Add( 'These control totals can be found in:' );
      mmTotals.Lines.Add( s );
    except
      on e:Exception do
        ShowMessage('Error: ' + e.Message);
    end;
  finally
    ACH.Free;
    CurrentFile.Free;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  ini: TIniFile;
  fName: string;
begin
  fName := GetIniFileName;
  if FileExists(fName) then
  begin
    ini := TIniFile.Create(fName);
    try
      Self.Top := ini.ReadInteger('MainForm', 'Top', Self.Top);
      Self.Left := ini.ReadInteger('MainForm', 'Left', Self.Left);
      Self.Width := Max(Self.Constraints.MinWidth, ini.ReadInteger('MainForm', 'Width', Self.Width));
      Self.Height := Max(Self.Constraints.MaxHeight, ini.ReadInteger('MainForm', 'Height', Self.Height));
    finally
      ini.Free;
    end;
  end;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ini: TIniFile;
  fName: string;
begin
  fName := GetIniFileName;
  ini := TIniFile.Create(fName);
  try
    ini.WriteInteger('MainForm', 'Top', Self.Top);
    ini.WriteInteger('MainForm', 'Left', Self.Left);
    ini.WriteInteger('MainForm', 'Width', Self.Width);
    ini.WriteInteger('MainForm', 'Height', Self.Height);
  finally
    ini.Free;
  end;
end;

procedure TfrmMain.AboutExecute(Sender: TObject);
var
  frmAbout: TfrmAbout;
begin
  frmAbout := TfrmAbout.Create( Application );
  try
    frmAbout.lblVersion.Caption := 'Version: ' + GetVersion;
    frmAbout.ShowModal;
  finally
    frmAbout.Free;
  end;
end;

end.
