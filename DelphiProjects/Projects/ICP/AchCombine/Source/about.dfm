object frmAbout: TfrmAbout
  Left = 343
  Top = 155
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 239
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TButton
    Left = 160
    Top = 202
    Width = 97
    Height = 25
    Caption = 'Ok'
    TabOrder = 0
    OnClick = btnOkClick
  end
  object pnlAbout: TPanel
    Left = 8
    Top = 8
    Width = 409
    Height = 185
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Image1: TImage
      Left = 16
      Top = 16
      Width = 32
      Height = 32
      AutoSize = True
      Picture.Data = {
        055449636F6E0000010001002020100000000000E80200001600000028000000
        2000000040000000010004000000000080020000000000000000000000000000
        0000000000000000000080000080000000808000800000008000800080800000
        80808000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
        FFFFFF0000000000004444444444440000000000000004444444444444444444
        4440000000444444444444444444444444444400044444444444444444444444
        4444444004444444444444444444444444444440044444444444444444444444
        4444444044444444444444444444444444444444009000090000090000090909
        0000000009990099900099900000000000000000909099090909090900000000
        0000000000900009002000002000222002000002009000090020000020020002
        0200000200900009002000002020000002000002009000090002222200200000
        0222222200900009000220220020000002000002009000090000202000020002
        0200000200900009000002000000222002000002009000090000000000000000
        0000000000900200000200022200200000200000009002000002002000202000
        0020000000900200000202000000200000200000009000222220020000002222
        2220000000900022022002000000200000200000009000020200002000202000
        0020000000000000200000022200200000200000200000200022200200000200
        0000000020000020020002020000020000000000200000202000000200000200
        0000000002222200200000022222220000000000022022002000000200000200
        0000000000202000020002020000020000000000000200000022200200000200
        00000000FFC003FFF800001FC000000380000001800000018000000100000000
        DEFBEAFF8C71FFFF52AAFFFFDEDF71BEDEDF6EBEDEDF5FBEDEE0DF80DEE4DFBE
        DEF5EEBEDEFBF1BEDEFFFFFFDBEE37DFDBEDD7DFDBEBF7DFDC1BF01FDC9BF7DF
        DEBDD7DFFF7E37DF7DC6FBFF7DBAFBFF7D7EFBFF837E03FF937EFBFFD7BAFBFF
        EFC6FBFF}
    end
    object lblACHName: TLabel
      Left = 72
      Top = 16
      Width = 231
      Height = 24
      Caption = 'Evo ACH Combine Utility'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblVersion: TLabel
      Left = 72
      Top = 40
      Width = 71
      Height = 13
      Caption = 'Version 3.1.0.0'
    end
    object lblAbout: TLabel
      Left = 71
      Top = 63
      Width = 320
      Height = 91
      Caption = 
        'The purpose of this application is to combine the contents of 2 ' +
        'or more ACH files. The routine assumes all files are for the sam' +
        'e Bank.  After the user selects 2 or more files to combine, the ' +
        'application will take the Header (type 1) record from the first ' +
        'file, append all non-trailer (type 9) records, and accumulate al' +
        'l totals necessary to create a NEW trailer (type 9) record for t' +
        'he combined, ACH file that is output from the routine.'
      WordWrap = True
    end
  end
end
