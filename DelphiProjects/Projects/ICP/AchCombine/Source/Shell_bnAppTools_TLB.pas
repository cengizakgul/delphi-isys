unit Shell_bnAppTools_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 7/25/2011 1:23:04 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\ICP\AchCombine\Source\Shell_bnAppTools.tlb (1)
// LIBID: {868137D6-5517-4FBC-B466-7966279ADA54}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v2.0 mscorlib, (C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\mscorlib.tlb)
// Errors:
//   Error creating palette bitmap of (TLicenseTools) : No Server registered for this CoClass
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, mscorlib_TLB, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  Shell_bnAppToolsMajorVersion = 1;
  Shell_bnAppToolsMinorVersion = 0;

  LIBID_Shell_bnAppTools: TGUID = '{868137D6-5517-4FBC-B466-7966279ADA54}';

  IID_ILicenseTools: TGUID = '{F27920AF-ABC0-348B-A0F3-4657E089C2F4}';
  CLASS_LicenseTools: TGUID = '{E2BA0768-AB27-3719-B1C9-48819EF58641}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ILicenseTools = interface;
  ILicenseToolsDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  LicenseTools = ILicenseTools;


// *********************************************************************//
// Interface: ILicenseTools
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F27920AF-ABC0-348B-A0F3-4657E089C2F4}
// *********************************************************************//
  ILicenseTools = interface(IDispatch)
    ['{F27920AF-ABC0-348B-A0F3-4657E089C2F4}']
    function GetModifier: WideString; safecall;
    function GetProductCode: WideString; safecall;
    function GetLicenseNumber(const ProductCode: WideString; const Modifier: WideString): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  ILicenseToolsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F27920AF-ABC0-348B-A0F3-4657E089C2F4}
// *********************************************************************//
  ILicenseToolsDisp = dispinterface
    ['{F27920AF-ABC0-348B-A0F3-4657E089C2F4}']
    function GetModifier: WideString; dispid 1610743808;
    function GetProductCode: WideString; dispid 1610743809;
    function GetLicenseNumber(const ProductCode: WideString; const Modifier: WideString): WideString; dispid 1610743810;
  end;

// *********************************************************************//
// The Class CoLicenseTools provides a Create and CreateRemote method to          
// create instances of the default interface ILicenseTools exposed by              
// the CoClass LicenseTools. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoLicenseTools = class
    class function Create: ILicenseTools;
    class function CreateRemote(const MachineName: string): ILicenseTools;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TLicenseTools
// Help String      : 
// Default Interface: ILicenseTools
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TLicenseToolsProperties= class;
{$ENDIF}
  TLicenseTools = class(TOleServer)
  private
    FIntf:        ILicenseTools;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TLicenseToolsProperties;
    function      GetServerProperties: TLicenseToolsProperties;
{$ENDIF}
    function      GetDefaultInterface: ILicenseTools;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ILicenseTools);
    procedure Disconnect; override;
    function GetModifier: WideString;
    function GetProductCode: WideString;
    function GetLicenseNumber(const ProductCode: WideString; const Modifier: WideString): WideString;
    property DefaultInterface: ILicenseTools read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TLicenseToolsProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TLicenseTools
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TLicenseToolsProperties = class(TPersistent)
  private
    FServer:    TLicenseTools;
    function    GetDefaultInterface: ILicenseTools;
    constructor Create(AServer: TLicenseTools);
  protected
  public
    property DefaultInterface: ILicenseTools read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoLicenseTools.Create: ILicenseTools;
begin
  Result := CreateComObject(CLASS_LicenseTools) as ILicenseTools;
end;

class function CoLicenseTools.CreateRemote(const MachineName: string): ILicenseTools;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_LicenseTools) as ILicenseTools;
end;

procedure TLicenseTools.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{E2BA0768-AB27-3719-B1C9-48819EF58641}';
    IntfIID:   '{F27920AF-ABC0-348B-A0F3-4657E089C2F4}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TLicenseTools.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ILicenseTools;
  end;
end;

procedure TLicenseTools.ConnectTo(svrIntf: ILicenseTools);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TLicenseTools.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TLicenseTools.GetDefaultInterface: ILicenseTools;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TLicenseTools.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TLicenseToolsProperties.Create(Self);
{$ENDIF}
end;

destructor TLicenseTools.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TLicenseTools.GetServerProperties: TLicenseToolsProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TLicenseTools.GetModifier: WideString;
begin
  Result := DefaultInterface.GetModifier;
end;

function TLicenseTools.GetProductCode: WideString;
begin
  Result := DefaultInterface.GetProductCode;
end;

function TLicenseTools.GetLicenseNumber(const ProductCode: WideString; const Modifier: WideString): WideString;
begin
  Result := DefaultInterface.GetLicenseNumber(ProductCode, Modifier);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TLicenseToolsProperties.Create(AServer: TLicenseTools);
begin
  inherited Create;
  FServer := AServer;
end;

function TLicenseToolsProperties.GetDefaultInterface: ILicenseTools;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TLicenseTools]);
end;

end.
