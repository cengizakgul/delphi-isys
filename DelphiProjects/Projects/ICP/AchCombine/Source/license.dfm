object frmLicense: TfrmLicense
  Left = 894
  Top = 276
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Evo ACH Combine Licensing'
  ClientHeight = 249
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnExit: TButton
    Left = 168
    Top = 216
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Exit'
    ModalResult = 2
    TabOrder = 2
  end
  object btnRegister: TButton
    Left = 88
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Register'
    Default = True
    TabOrder = 1
    OnClick = btnRegisterClick
  end
  object pnlRegister: TPanel
    Left = 6
    Top = 8
    Width = 315
    Height = 201
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object lblInfo1: TLabel
      Left = 13
      Top = 9
      Width = 279
      Height = 26
      Caption = 
        'This utility must be registered before use.  Please call iSystem' +
        's Support for Licensing information.  (802) 655-8347'
      WordWrap = True
    end
    object lblModifier: TLabel
      Left = 32
      Top = 96
      Width = 40
      Height = 13
      Caption = 'Modifier:'
    end
    object lblLicenseNo: TLabel
      Left = 32
      Top = 138
      Width = 50
      Height = 13
      Caption = 'License #:'
    end
    object lblProductCode: TLabel
      Left = 32
      Top = 54
      Width = 65
      Height = 13
      Caption = 'Product Code'
    end
    object edModifier: TEdit
      Left = 32
      Top = 112
      Width = 240
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
    end
    object edLicenseNo: TEdit
      Left = 32
      Top = 154
      Width = 240
      Height = 21
      TabOrder = 2
    end
    object edProductCode: TEdit
      Left = 32
      Top = 70
      Width = 240
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
  end
end
