object frmMain: TfrmMain
  Left = 291
  Top = 188
  Width = 803
  Height = 483
  Caption = 'Evo ACH Combine Utility'
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 700
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mmMemu
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    795
    429)
  PixelsPerInch = 96
  TextHeight = 13
  object lblTotalReport: TLabel
    Left = 435
    Top = 79
    Width = 169
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'ACH Combine Control Totals Report'
  end
  object sbStatus: TStatusBar
    Left = 0
    Top = 410
    Width = 795
    Height = 19
    Panels = <>
  end
  object gbFiles: TGroupBox
    Left = 3
    Top = 0
    Width = 418
    Height = 408
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    DesignSize = (
      418
      408)
    object lblFiles: TLabel
      Left = 12
      Top = 16
      Width = 101
      Height = 13
      Caption = 'Select 2 or more files:'
    end
    object lblOutPut: TLabel
      Left = 11
      Top = 347
      Width = 103
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Name the Output File:'
    end
    object sbtnOutputFile: TSpeedButton
      Left = 347
      Top = 363
      Width = 22
      Height = 21
      Action = OutputFile
      Anchors = [akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbFiles: TListBox
      Left = 12
      Top = 32
      Width = 330
      Height = 280
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
    end
    object edOutputFile: TEdit
      Left = 11
      Top = 363
      Width = 330
      Height = 21
      Anchors = [akLeft, akRight, akBottom]
      ReadOnly = True
      TabOrder = 5
    end
    object btnAdd: TBitBtn
      Left = 352
      Top = 32
      Width = 55
      Height = 25
      Action = Add
      Anchors = [akTop, akRight]
      Caption = 'Add'
      TabOrder = 1
    end
    object btnUp: TBitBtn
      Left = 352
      Top = 64
      Width = 55
      Height = 25
      Action = Up
      Anchors = [akTop, akRight]
      Caption = 'Up'
      TabOrder = 2
    end
    object btnDown: TBitBtn
      Left = 352
      Top = 96
      Width = 55
      Height = 25
      Action = Down
      Anchors = [akTop, akRight]
      Caption = 'Down'
      TabOrder = 3
    end
    object btnRemove: TBitBtn
      Left = 352
      Top = 128
      Width = 55
      Height = 25
      Action = Remove
      Anchors = [akTop, akRight]
      Caption = 'Remove'
      TabOrder = 4
    end
  end
  object mmTotals: TMemo
    Left = 433
    Top = 96
    Width = 353
    Height = 308
    Anchors = [akTop, akRight, akBottom]
    Color = clBtnFace
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object pnlCombine: TPanel
    Left = 433
    Top = 5
    Width = 353
    Height = 68
    Anchors = [akTop, akRight]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      353
      68)
    object btnCombine: TButton
      Left = 101
      Top = 18
      Width = 145
      Height = 33
      Action = Combine
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
  end
  object Actions: TActionList
    Left = 96
    Top = 104
    object Exit: TAction
      Caption = 'Exit'
      OnExecute = ExitExecute
    end
    object About: TAction
      Caption = 'About Evo ACH Combine'
      OnExecute = AboutExecute
    end
    object Combine: TAction
      Caption = 'Combine Selected Files'
      OnExecute = CombineExecute
      OnUpdate = CombineUpdate
    end
    object Add: TAction
      Caption = 'Add'
      Hint = 'Add file to the list'
      OnExecute = AddExecute
    end
    object Up: TAction
      Caption = 'Up'
      Hint = 'Move file up'
      OnExecute = UpExecute
      OnUpdate = UpUpdate
    end
    object Down: TAction
      Caption = 'Down'
      Hint = 'Move file down'
      OnExecute = DownExecute
      OnUpdate = DownUpdate
    end
    object Remove: TAction
      Caption = 'Remove'
      Hint = 'Remove file from the list'
      OnExecute = RemoveExecute
      OnUpdate = RemoveUpdate
    end
    object OutputFile: TAction
      Caption = '...'
      Hint = 'Select Output File'
      OnExecute = OutputFileExecute
    end
  end
  object mmMemu: TMainMenu
    Left = 96
    Top = 64
    object miFile: TMenuItem
      Caption = 'File'
      object miExit: TMenuItem
        Action = Exit
      end
    end
    object miHelp: TMenuItem
      Caption = 'Help'
      object miAbout: TMenuItem
        Action = About
      end
    end
  end
  object odAchFile: TOpenDialog
    DefaultExt = '*.ach'
    Filter = 'Ach files|*.ach|Text files|*.txt|All files|*.*'
    Options = [ofAllowMultiSelect, ofEnableSizing]
    Left = 96
    Top = 141
  end
  object sdAchFile: TSaveDialog
    DefaultExt = '*.ach'
    Filter = 'Ach files|*.ach|Text files|*.txt|All files|*.*'
    Left = 96
    Top = 177
  end
end
