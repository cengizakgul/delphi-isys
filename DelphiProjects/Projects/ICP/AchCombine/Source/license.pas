unit license;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, isBasicUtils,
  Dialogs, StdCtrls, ExtCtrls, ComObj, WinTypes, WinProcs, wait, OnGuard;

const
  LicenseOptionMaxCount = 16;
  cKey: TKey = ($F2, $D0, $DD, $FE, $13, $3E, $C2, $C4, $13, $19, $22, $1C, $04, $5D, $BA, $84);

type
  TGetLicenseNumber = function (const aProductCode: string; const aModifier: string): string; stdcall;

  TfrmLicense = class(TForm)
    btnExit: TButton;
    btnRegister: TButton;
    pnlRegister: TPanel;
    lblInfo1: TLabel;
    lblModifier: TLabel;
    edModifier: TEdit;
    lblLicenseNo: TLabel;
    edLicenseNo: TEdit;
    lblProductCode: TLabel;
    edProductCode: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure btnRegisterClick(Sender: TObject);
  private
    FModifier: Cardinal;
  public
    function GetLicenseNumber: string;
    function GetModifier: string;
    function GetProductCode: string;
  end;

  function CheckLicense: boolean;
  function GetLicenseKey(ProductCode, Modifier: integer): string;

implementation

{$R *.dfm}

function GetLicenseKey(ProductCode, Modifier: integer): string;
var
  Work: TCode;
  K: TKey;
  D1: TDateTime;
  SN: integer;
begin
  K := CKey;
  ApplyModifierToKeyPrim(Modifier, K, SizeOf(K));

  D1 := 72686;

  SN := (ProductCode shl LicenseOptionMaxCount);
  InitSerialNumberCode(K, SN, D1, Work);
  Result := BufferToHex(Work, SizeOf(Work));
end;

function CheckLicense: boolean;
var
  frmLicense: TfrmLicense;
  sl: TStrings;
  s: string;
  regNow: boolean;
begin
  Result := False;
  RegNow := True;
  frmLicense := TfrmLicense.Create(Application);
  sl := TStringList.Create;
  try
    with frmLicense do
    begin
      s := Application.ExeName;
      s := Copy(s, 1, Length(s) - 4) + '.lic';
      if FileExists( s ) then
      begin
        // verify the license
        sl.LoadFromFile( s );
        if sl.Count > 0 then
        begin
          edLicenseNo.Text := sl[0];
          regNow := edLicenseNo.Text <> GetLicenseNumber;
          Result := not regNow;
          if regNow then
            if MessageDlg('The license number is not correct.' + #10#13 + 'Would you like to register now?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
              regNow := False;
        end;
      end
      else
        if MessageDlg('The program is not registered.' + #10#13 + 'Would you like to register now?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
          regNow := False;

      if regNow then
      begin
        Result := ShowModal = mrOk;
        if Result then
        begin
          sl.CommaText := edLicenseNo.Text;
          sl.SaveToFile( s );
        end;
      end;
    end;
  finally
    sl.Free;
    frmLicense.Free;
  end;
end;

{ TfrmLicense }

function TfrmLicense.GetLicenseNumber: string;
var
  ProdCodeValue, ModifierValue: integer;
begin
  Result := '';
  {if} TryStrToInt(edProductCode.Text, ProdCodeValue); {and} TryStrToInt(edModifier.Text, ModifierValue); {then}
    Result := GetLicenseKey(ProdCodeValue, ModifierValue);
end;

function TfrmLicense.GetModifier: string;
begin
  FModifier := Cardinal(CreateMachineID([midUser, midSystem{, midNetwork}, midDrives]));
  Result := IntToStr( FModifier );
end;

function TfrmLicense.GetProductCode: string;
begin
  Result := IntToStr(FModifier shl 10);
end;

procedure TfrmLicense.FormCreate(Sender: TObject);
begin
  StartWait('Load license library... Check for the license...');
  try
    Application.ProcessMessages;
    edModifier.Text := GetModifier;
    edProductCode.Text := GetProductCode;
  finally
    StopWait;
  end;
end;

procedure TfrmLicense.btnRegisterClick(Sender: TObject);
begin
  if edLicenseNo.Text = GetLicenseNumber then
    ModalResult := mrOk
  else
    ShowMessage('The license number is not valid. Please try again.');
end;

end.
