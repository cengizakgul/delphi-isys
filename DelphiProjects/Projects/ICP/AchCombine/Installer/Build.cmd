@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoAchCombine.msi 
"%pWiXDir%\candle.exe" .\EvoAchCombine.wxs -wx -out ..\..\..\..\..\Tmp\EvoAchCombine.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoAchCombine.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombine.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombine.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombine%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombine%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombine.msi EvoAchCombine%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvoAchCombineKeyGen.msi 
"%pWiXDir%\candle.exe" .\EvoAchCombineKeyGen.wxs -wx -out ..\..\..\..\..\Tmp\EvoAchCombineKeyGen.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoAchCombineKeyGen.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombineKeyGen.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombineKeyGen.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombineKeyGen%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombineKeyGen%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoAchCombineKeyGen.msi EvoAchCombineKeyGen%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
