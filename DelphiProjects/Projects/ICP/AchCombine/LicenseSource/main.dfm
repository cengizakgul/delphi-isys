object frmMain: TfrmMain
  Left = 575
  Top = 315
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Evo Ach Combine License Key Generator'
  ClientHeight = 204
  ClientWidth = 275
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlRegister: TPanel
    Left = 6
    Top = 8
    Width = 262
    Height = 190
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object lblModifier: TLabel
      Left = 8
      Top = 50
      Width = 40
      Height = 13
      Caption = 'Modifier:'
    end
    object lblLicenseNo: TLabel
      Left = 8
      Top = 142
      Width = 50
      Height = 13
      Caption = 'License #:'
    end
    object lblProductCode: TLabel
      Left = 8
      Top = 8
      Width = 65
      Height = 13
      Caption = 'Product Code'
    end
    object edModifier: TEdit
      Left = 8
      Top = 66
      Width = 240
      Height = 21
      TabOrder = 1
    end
    object edLicenseNo: TEdit
      Left = 8
      Top = 158
      Width = 240
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
    end
    object edProductCode: TEdit
      Left = 8
      Top = 24
      Width = 240
      Height = 21
      TabOrder = 0
    end
    object btnGetLicense: TButton
      Left = 70
      Top = 104
      Width = 116
      Height = 25
      Caption = 'Get License Number'
      TabOrder = 2
      OnClick = btnGetLicenseClick
    end
  end
end
