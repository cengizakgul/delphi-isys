unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComObj, license;

type
  TfrmMain = class(TForm)
    pnlRegister: TPanel;
    lblModifier: TLabel;
    lblLicenseNo: TLabel;
    lblProductCode: TLabel;
    edModifier: TEdit;
    edLicenseNo: TEdit;
    edProductCode: TEdit;
    btnGetLicense: TButton;
    procedure btnGetLicenseClick(Sender: TObject);
  private
    { Private declarations }
  public
    function GetLicenseNumber: string;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

{ TfrmMain }

function TfrmMain.GetLicenseNumber: string;
var
  ProdCodeValue, ModifierValue: integer;
begin
  Result := '';
  TryStrToInt(edProductCode.Text, ProdCodeValue);
  TryStrToInt(edModifier.Text, ModifierValue);

  Result := GetLicenseKey(ProdCodeValue, ModifierValue);
end;

procedure TfrmMain.btnGetLicenseClick(Sender: TObject);
begin
  edLicenseNo.Text := GetLicenseNumber;
end;

end.
