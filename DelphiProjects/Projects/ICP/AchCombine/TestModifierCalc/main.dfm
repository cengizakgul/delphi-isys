object Form1: TForm1
  Left = 479
  Top = 279
  BorderStyle = bsDialog
  Caption = 'Test Modifier Calculation'
  ClientHeight = 204
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    504
    204)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 491
    Height = 186
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 32
      Width = 37
      Height = 13
      Caption = 'Modifier'
    end
    object Label2: TLabel
      Left = 119
      Top = 9
      Width = 34
      Height = 13
      Caption = 'Current'
    end
    object Label3: TLabel
      Left = 302
      Top = 10
      Width = 31
      Height = 13
      Caption = 'Stored'
    end
    object Label4: TLabel
      Left = 20
      Top = 80
      Width = 68
      Height = 13
      Caption = 'Modifier (User)'
    end
    object Label5: TLabel
      Left = 20
      Top = 106
      Width = 80
      Height = 13
      Caption = 'Modifier (System)'
    end
    object Label6: TLabel
      Left = 20
      Top = 132
      Width = 89
      Height = 13
      Caption = 'Modifier (NetWork)'
    end
    object Label7: TLabel
      Left = 20
      Top = 158
      Width = 76
      Height = 13
      Caption = 'Modifier (Drives)'
    end
    object edModifier: TEdit
      Left = 119
      Top = 27
      Width = 170
      Height = 21
      Color = clWhite
      ReadOnly = True
      TabOrder = 0
    end
    object edModifier_stored: TEdit
      Left = 301
      Top = 27
      Width = 170
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
    end
    object edM_User: TEdit
      Left = 119
      Top = 75
      Width = 170
      Height = 21
      Color = clWhite
      ReadOnly = True
      TabOrder = 2
    end
    object edM_User_Stored: TEdit
      Left = 301
      Top = 75
      Width = 170
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
    end
    object edM_System: TEdit
      Left = 119
      Top = 101
      Width = 170
      Height = 21
      Color = clWhite
      ReadOnly = True
      TabOrder = 4
    end
    object edM_System_stored: TEdit
      Left = 301
      Top = 101
      Width = 170
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 5
    end
    object edM_Network: TEdit
      Left = 119
      Top = 127
      Width = 170
      Height = 21
      Color = clWhite
      ReadOnly = True
      TabOrder = 6
    end
    object edM_Network_stored: TEdit
      Left = 301
      Top = 127
      Width = 170
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 7
    end
    object edM_Drives: TEdit
      Left = 119
      Top = 153
      Width = 170
      Height = 21
      Color = clWhite
      ReadOnly = True
      TabOrder = 8
    end
    object edM_Drives_stored: TEdit
      Left = 301
      Top = 153
      Width = 170
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 9
    end
  end
end
