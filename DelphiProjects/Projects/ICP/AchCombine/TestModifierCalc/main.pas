unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OnGuard, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edModifier: TEdit;
    edModifier_stored: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edM_User: TEdit;
    edM_User_Stored: TEdit;
    Label5: TLabel;
    edM_System: TEdit;
    edM_System_stored: TEdit;
    Label6: TLabel;
    edM_Network: TEdit;
    edM_Network_stored: TEdit;
    Label7: TLabel;
    edM_Drives: TEdit;
    edM_Drives_stored: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    function GetModifier: string;
    function GetModifier_User: string;
    function GetModifier_Systems: string;
    function GetModifier_Network: string;
    function GetModifier_Drivers: string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses iniFiles;

{ TForm1 }

function TForm1.GetModifier: string;
var
  lModifier: Cardinal;
begin
  lModifier := Cardinal(CreateMachineID([midUser, midSystem, midNetwork, midDrives]));
  Result := IntToStr( lModifier );
end;

function TForm1.GetModifier_Drivers: string;
var
  lModifier: Cardinal;
begin
  lModifier := Cardinal(CreateMachineID([midDrives]));
  Result := IntToStr( lModifier );
end;

function TForm1.GetModifier_Network: string;
var
  lModifier: Cardinal;
begin
  lModifier := Cardinal(CreateMachineID([midNetwork]));
  Result := IntToStr( lModifier );
end;

function TForm1.GetModifier_Systems: string;
var
  lModifier: Cardinal;
begin
  lModifier := Cardinal(CreateMachineID([midSystem]));
  Result := IntToStr( lModifier );
end;

function TForm1.GetModifier_User: string;
var
  lModifier: Cardinal;
begin
  lModifier := Cardinal(CreateMachineID([midUser]));
  Result := IntToStr( lModifier );
end;

procedure TForm1.FormShow(Sender: TObject);
var f: TIniFile;
begin
  f := TIniFile.Create( Copy(Application.ExeName, 1, Length(Application.ExeName) - 4) + '.ini' );
  try
    edModifier_stored.Text := f.ReadString( 'Modifier', 'All', '' );
    edM_User_Stored.Text := f.ReadString( 'Modifier', 'User', '' );
    edM_System_stored.Text := f.ReadString( 'Modifier', 'System', '' );
    edM_Network_stored.Text := f.ReadString( 'Modifier', 'Network', '' );
    edM_Drives_stored.Text := f.ReadString( 'Modifier', 'Drives', '' );
  finally
    f.Free;
  end;

  edModifier.Text := GetModifier;
  edM_User.Text := GetModifier_User;
  edM_System.Text := GetModifier_Systems;
  edM_Network.Text := GetModifier_Network;
  edM_Drives.Text := GetModifier_Drivers;

  if (edModifier_stored.Text <> '') and (edModifier.Text <> edModifier_stored.Text) then
    edModifier.Color := clRed;
  if (edM_User_Stored.Text <> '') and (edM_User.Text <> edM_User_Stored.Text) then
    edM_User.Color := clRed;
  if (edM_System_stored.Text <> '') and (edM_System.Text <> edM_System_stored.Text) then
    edM_System.Color := clRed;
  if (edM_Network_stored.Text <> '') and (edM_Network.Text <> edM_Network_stored.Text) then
    edM_Network.Color := clRed;
  if (edM_Drives_stored.Text <> '') and (edM_Drives.Text <> edM_Drives_stored.Text) then
    edM_Drives.Color := clRed;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var f: TIniFile;
begin
  f := TIniFile.Create( Copy(Application.ExeName, 1, Length(Application.ExeName) - 4) + '.ini' );
  try
    f.WriteString( 'Modifier', 'All', edModifier.Text );
    f.WriteString( 'Modifier', 'User', edM_User.Text );
    f.WriteString( 'Modifier', 'System', edM_System.Text );
    f.WriteString( 'Modifier', 'Network', edM_Network.Text );
    f.WriteString( 'Modifier', 'Drives', edM_Drives.Text );
  finally
    f.Free;
  end;
end;

end.
