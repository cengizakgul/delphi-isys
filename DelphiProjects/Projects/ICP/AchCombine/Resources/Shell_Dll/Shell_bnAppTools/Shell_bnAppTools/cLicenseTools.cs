using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using bnAppTools;

public interface ILicenseTools
{
    string GetModifier();
    string GetProductCode();
    string GetLicenseNumber(string ProductCode, string Modifier);
}

[ClassInterface(ClassInterfaceType.None)]
public class LicenseTools: ILicenseTools
{
    public string GetModifier()
    {    
        return bnLicensing.getModifier();
    }

    public string GetProductCode()
    {
        return bnLicensing.getProductCode();
    }

    public string GetLicenseNumber(string ProductCode, string Modifier)
    {
        return bnLicensing.getLicenseNumber(ProductCode, Modifier);
    }
}
