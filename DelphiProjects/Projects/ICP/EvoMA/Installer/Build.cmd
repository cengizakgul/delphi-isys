@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoMA
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoMA.exe
cd ..\..\..\Projects\ICP\EvoMA\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoMA.msi 

"%pWiXDir%\candle.exe" .\EvoMA.wxs -wx -out ..\..\..\..\..\Tmp\EvoMA.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoMARwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoMARwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoMARwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoMA.wixobj ..\..\..\..\..\Tmp\EvoMARwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoMA.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoMA.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoMA_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoMA_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoMA.msi EvoMA_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
