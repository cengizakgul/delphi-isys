inherited MainFm: TMainFm
  Left = 458
  Top = 296
  Caption = 'MainFm'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 41
    Height = 456
    ActivePage = TabSheet2
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 784
        Align = alTop
        inherited GroupBox1: TGroupBox
          Width = 784
          inherited cbSavePassword: TCheckBox
            Width = 106
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Modify'
      object pnlBottom: TPanel
        Left = 0
        Top = 387
        Width = 784
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 57
          Height = 25
          Action = actModifyStates
          Caption = 'Modify'
          TabOrder = 0
        end
      end
      inline PrFrame: TEvolutionPrFrm
        Left = 0
        Top = 0
        Width = 784
        Height = 387
        Align = alClient
        TabOrder = 1
        inherited PayrollFrame: TEvolutionDsFrm
          Width = 784
          Height = 387
          inherited Panel1: TPanel
            Width = 784
            Alignment = taLeftJustify
            Caption = ' Payrolls'
          end
          inherited dgGrid: TReDBGrid
            Width = 784
            Height = 362
          end
        end
      end
    end
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 97
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 8
        Top = 8
        Width = 81
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution and XXX data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 97
      Top = 0
      Width = 695
      Height = 41
      Align = alClient
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object ConnectToEvo: TAction
      Caption = 'Get Evolution and XXX data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object actModifyStates: TAction
      Caption = 'Modify'
      OnExecute = actModifyStatesExecute
      OnUpdate = actModifyStatesUpdate
    end
  end
end
