program EvoMA;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  common,
  MidasLib,
  evoapiconnection,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionClCoFrame in '..\..\common\EvolutionClCoFrame.pas' {EvolutionClCoFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EEUtilityLoggerViewFrame in '..\..\common\EEUtilityLoggerViewFrame.pas' {EEUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm: TEvoAPIClientMainFm},
  StatesModifier in 'StatesModifier.pas',
  EvolutionPrFrame in '..\..\common\EvolutionPrFrame.pas' {EvolutionPrFrm: TFrame};

{$R *.res}

begin
  LicenseKey := '{5E465AFC-D0A1-424E-B21B-4351ECD6BFFF}'; //evo.legacy.*

  ConfigVersion := 1;
{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution MA';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
