unit StatesModifier;

interface

uses
	common, evoapiconnection, gdyCommonLogger, kbmMemTable, evodata, evConsts;

type
  TDBDTRecordIDs = array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of integer;

  TStatesModifierStat = record
    Errors: integer;
    Modified: integer;
  end;

  TStatesModifier = class
  private
    FConn: IEvoAPIConnection;
    FPr: TEvoPayrollDef;
    FDBDT: TEvoDBDTs;
    FLogger: ICommonLogger;

    FEEStates: TkbmCustomMemTable;
    FPrCheckLines: TkbmCustomMemTable;
    function GetDBDTForMA: TDBDTRecordIDs;
  public
    constructor Create(conn: IEvoAPIConnection; Pr: TEvoPayrollDef; Logger: ICommonLogger);
    destructor Destroy; override;
    function ModifyStates: TStatesModifierStat;
  end;

function StatesModifierStatToString(const stat: TStatesModifierStat): string;

implementation

uses
  sysutils, evoApiConnectionUtils, gdyGlobalWaitIndicator, variants,
  XmlRpcTypes, gdycommon;

{ TStatesModifier }

constructor TStatesModifier.Create(conn: IEvoAPIConnection;
  Pr: TEvoPayrollDef; Logger: ICommonLogger);
begin
  FConn := conn;
  FPr := Pr;
  FLogger := Logger;

  FEEStates := ExecCoQuery(FConn, FLogger, FPr.Company, 'CUSTOM_EE_STATES.rwq', 'Getting employee states from Evolution' );
  FPrCheckLines := ExecPrQuery(FConn, FLogger, FPr, 'CUSTOM_PR_CHECK_LINES.rwq', 'Getting check lines from Evolution' );
  FDBDT := TEvoDBDTs.Create(FConn, FPr.Company, FLogger);
end;

destructor TStatesModifier.Destroy;
begin
  FreeAndNil(FDBDT);
  FreeAndNil(FEEStates);
  FreeAndNil(FPrCheckLines);
  inherited;
end;

function StatesModifierStatToString(const stat: TStatesModifierStat): string;
begin
  if stat.Errors = 0 then
  begin
    if stat.Modified = 0  then
      Result := 'completed: all check lines were already correct'
    else
      Result := Format('completed: %d check %s updated', [stat.Modified, Plural('line', stat.Modified)]);
  end
  else
  begin
    if stat.Modified = 0  then
      Result := Format('failed: %d %s occured', [stat.Errors, Plural('error', stat.Errors)])
    else
      Result := Format('partially completed: %d %s occured, %d check %s updated',
        [stat.Errors, Plural('error', stat.Errors), stat.Modified, Plural('line', stat.Modified)]);
  end
end;

function TStatesModifier.GetDBDTForMA: TDBDTRecordIDs;
var
  bFound: boolean;
begin
  bFound := false;
  FDBDT.CO_DIVISION.First;
  while not FDBDT.CO_DIVISION.Eof do
  begin
    FDBDT.CO_BRANCH.First;
    while not FDBDT.CO_BRANCH.Eof do
    begin
      FDBDT.CO_DEPARTMENT.First;
      while not FDBDT.CO_DEPARTMENT.Eof do
      begin
        FDBDT.CO_TEAM.First;
        while not FDBDT.CO_TEAM.Eof do
        begin
          if trim(FDBDT.CO_TEAM.FieldByName('CUSTOM_TEAM_NUMBER').AsString) = 'MA' then
          begin
            if bFound then
              raise Exception.Create('Found more than one team with code "MA"');
            Result[CLIENT_LEVEL_DIVISION] := FDBDT.CO_DIVISION['CO_DIVISION_NBR'];
            Result[CLIENT_LEVEL_BRANCH] := FDBDT.CO_BRANCH['CO_BRANCH_NBR'];
            Result[CLIENT_LEVEL_DEPT] := FDBDT.CO_DEPARTMENT['CO_DEPARTMENT_NBR'];
            Result[CLIENT_LEVEL_TEAM] := FDBDT.CO_TEAM['CO_TEAM_NBR'];
            bFound := true;
          end;

          FDBDT.CO_TEAM.Next;
        end;
        FDBDT.CO_DEPARTMENT.Next;
      end;
      FDBDT.CO_BRANCH.Next;
    end;
    FDBDT.CO_DIVISION.Next;
  end;
  if not bFound then
    raise Exception.Create('Could not find a team with code "MA"');
end;

function TStatesModifier.ModifyStates: TStatesModifierStat;
var
  pr_check_lines_nbr: string;
  ee_states_nbr: Variant;
  dbdt: TDBDTRecordIDs;
  changes: IRpcStruct;
begin
  Result.Errors := 0;
  Result.Modified := 0;
  FLogger.LogEntry('Check lines analysis');
  try
    try
      changes := TRpcStruct.Create;

      dbdt := GetDBDTForMA;

      FPrCheckLines.First;
      while not FPrCheckLines.eof do
      begin
        FLogger.LogEntry('Analyzing check line');
        try
          try

            FLogger.LogContextItem(sCtxEECode, FPrCheckLines.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString );
            FLogger.LogContextItem('Batch#', FPrCheckLines.FieldByName('PR_BATCH_NBR').AsString );
            pr_check_lines_nbr := FPrCheckLines.FieldByName('PR_CHECK_LINES_NBR').AsString;
            FLogger.LogContextItem('Internal check line#', pr_check_lines_nbr );

            if FPrCheckLines.FieldByName('JOB_CODE').AsString = 'MA' then
            begin
              ee_states_nbr := FEEStates.Lookup('EE_NBR;STATE', VarArrayOf( [FPrCheckLines['EE_NBR'], 'MA'] ), 'EE_STATES_NBR');
              if VarIsNull(ee_states_nbr) then
                raise Exception.Create('Employee doesn''t have MA state set up');
              if (ee_states_nbr <> FPrCheckLines['EE_STATES_NBR']) or
                 (ee_states_nbr <> FPrCheckLines['EE_SUI_STATES_NBR']) or
                 (FPrCheckLines.FieldByName('CO_DIVISION_NBR').AsInteger <> dbdt[CLIENT_LEVEL_DIVISION]) or
                 (FPrCheckLines.FieldByName('CO_BRANCH_NBR').AsInteger <> dbdt[CLIENT_LEVEL_BRANCH]) or
                 (FPrCheckLines.FieldByName('CO_DEPARTMENT_NBR').AsInteger <> dbdt[CLIENT_LEVEL_DEPT]) or
                 (FPrCheckLines.FieldByName('CO_TEAM_NBR').AsInteger <> dbdt[CLIENT_LEVEL_TEAM]) then
              begin
                changes.AddItem('PR_CHECK_LINES__EE_STATES_NBR__'+pr_check_lines_nbr, Integer(ee_states_nbr) );
                changes.AddItem('PR_CHECK_LINES__EE_SUI_STATES_NBR__'+pr_check_lines_nbr, Integer(ee_states_nbr) );
                changes.AddItem('PR_CHECK_LINES__CO_DIVISION_NBR__'+pr_check_lines_nbr, dbdt[CLIENT_LEVEL_DIVISION] );
                changes.AddItem('PR_CHECK_LINES__CO_BRANCH_NBR__'+pr_check_lines_nbr, dbdt[CLIENT_LEVEL_BRANCH] );
                changes.AddItem('PR_CHECK_LINES__CO_DEPARTMENT_NBR__'+pr_check_lines_nbr, dbdt[CLIENT_LEVEL_DEPT] );
                changes.AddItem('PR_CHECK_LINES__CO_TEAM_NBR__'+pr_check_lines_nbr, dbdt[CLIENT_LEVEL_TEAM] );
                inc(Result.Modified);
              end;
            end;
          except
            inc(Result.Errors);
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
        FPrCheckLines.Next;
      end;

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;

  FLogger.LogEntry('Check lines modification');
  try
    try

      if changes.Count > 0 then
      begin
        changes.AddItem('PR_CHECK_LINES', Format('PR_NBR=%d', [FPr.Pr.PrNbr]));
        WaitIndicator.StartWait('Modifying check lines');
        try
          FConn.postTableChanges(FPr.Company.ClNbr, changes);
        finally
          WaitIndicator.EndWait;
        end;
      end

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

end.
