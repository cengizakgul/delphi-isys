object frmSettings: TfrmSettings
  Left = 409
  Top = 242
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Evo Connection Settings'
  ClientHeight = 153
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbSettings: TGroupBox
    Left = 6
    Top = 2
    Width = 361
    Height = 111
    TabOrder = 0
    object lblAPIServer: TLabel
      Left = 16
      Top = 26
      Width = 96
      Height = 13
      Caption = 'API adapter address'
    end
    object lblUserName: TLabel
      Left = 16
      Top = 52
      Width = 93
      Height = 13
      Caption = 'Evolution username'
    end
    object lblPassword: TLabel
      Left = 16
      Top = 76
      Width = 92
      Height = 13
      Caption = 'Evolution password'
    end
    object APIServerEdit: TEdit
      Left = 123
      Top = 22
      Width = 124
      Height = 21
      TabOrder = 0
    end
    object UserNameEdit: TEdit
      Left = 123
      Top = 48
      Width = 124
      Height = 21
      TabOrder = 1
    end
    object PasswordEdit: TPasswordEdit
      Left = 123
      Top = 73
      Width = 124
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object cbSavePassword: TCheckBox
      Left = 254
      Top = 75
      Width = 100
      Height = 17
      Caption = 'Save password'
      TabOrder = 3
    end
  end
  object btnSave: TButton
    Left = 97
    Top = 120
    Width = 83
    Height = 25
    Caption = 'Save'
    Default = True
    TabOrder = 1
    OnClick = btnSaveClick
  end
  object Button1: TButton
    Left = 185
    Top = 120
    Width = 83
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Button1Click
  end
end
