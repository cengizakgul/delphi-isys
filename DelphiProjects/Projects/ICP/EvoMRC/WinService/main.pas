unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  ConstAndProc, XmlRpcServerMod, common, gdycommonlogger,
  isSettings, Winsvc;

type
  TEvoMRC = class(TService, ICommonLogger, IMessenger)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    FXmlRpcServer: TEvoMRCXmlRPCServer;

    {gdyLogWriters.TFileLogOutput}
    FLog: Text;
    FFileOpened: boolean;
    procedure FileNeeded;
    procedure CloseFileTemporarily;
    procedure LogMessage(const s: string);

    procedure RunXmlRpcServer;
    function GetVersionFromVersionTxt: string;

    {IMessenger}
    procedure AddMessage(const aMsg: string; IsError: boolean = false);
    procedure ClearMessages;

    {ICommonLogger}
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call

    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string; d: string = '' );
    procedure LogEventFmt( s: string; const args: array of const; d: string = '' );
    procedure LogWarning( s: string; d: string = '' );
    procedure LogWarningFmt( s: string; const args: array of const; d: string = '' );
    procedure LogError( s: string; d: string = '' );
    procedure LogErrorFmt( s: string; const args: array of const; d: string = '' );
    procedure LogDebug( s: string; fulltext: string = '' );

    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  EvoMRC: TEvoMRC;

implementation

uses EvoMRCCommon, gdyRedir, registry, inifiles;

{$R *.DFM}

const
  cKey='Form.Button';

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  EvoMRC.Controller(CtrlCode);
end;

procedure TEvoMRC.AddMessage(const aMsg: string; IsError: boolean);
begin
  if IsError then
    LogError( aMsg )
  else
    LogEvent( aMsg );
end;

procedure TEvoMRC.ClearMessages;
begin

end;

procedure TEvoMRC.CloseFileTemporarily;
begin
  if FFileOpened then
  try
    system.Flush( FLog );
    system.CloseFile( FLog );
    FFileOpened := false;
  except
    //do nothing
  end;
end;

procedure TEvoMRC.FileNeeded;
var
  fn: string;
begin
  if not FFileOpened then
    try
      fn := Redirection.GetFilename(sLogFileAlias);
      ForceDirectories( ExtractFilePath(fn) );
      system.assignfile( FLog, fn );
//      if FileExists(fn) then
 //       system.Append( FLog )
//      else
        system.Rewrite( FLog );
      FFileOpened := true;  
    except
      //do nothing
    end;
end;

function TEvoMRC.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

function TEvoMRC.GetVersionFromVersionTxt: string;
begin
  Result := GetVersion;
end;

procedure TEvoMRC.LogContextItem(tag, value: string);
begin
  LogMessage(tag + ' = ' + value);
end;

procedure TEvoMRC.LogDebug(s, fulltext: string);
begin
  LogEvent(s, fulltext);
end;

procedure TEvoMRC.LogEntry(blockname: string);
begin
  LogMessage(blockname + ' -> (');
end;

procedure TEvoMRC.LogError(s, d: string);
begin
  LogEvent( 'Error: ' + s, d );
end;

procedure TEvoMRC.LogErrorFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( 'Error: ' + Format( s, args ), d );
end;

procedure TEvoMRC.LogEvent(s, d: string);
begin
  if d <> '' then
    LogMessage(s + ', details: ' + d)
  else
    LogMessage(s);
end;

procedure TEvoMRC.LogEventFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( Format( s, args ), d );
end;

procedure TEvoMRC.LogExit;
begin
  LogMessage(') <-');
end;

procedure TEvoMRC.LogMessage(const s: string);
begin
  try
    FileNeeded;
    system.Writeln( FLog, s );
    system.Flush( FLog );
  except
    //do nothing
  end;
end;

procedure TEvoMRC.LogWarning(s, d: string);
begin
  LogEvent('Warning! ' + s, d);
end;

procedure TEvoMRC.LogWarningFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( 'Warning! ' + Format( s, args ), d );
end;

procedure TEvoMRC.PassthroughException;
var
  LException: Exception;
begin
  LException := AcquireExceptionObject;

  if (LException <> nil) and (LException is Exception) then
  begin
    LogMessage('Exception: ' + (LException as Exception).Message );
    raise LException at ExceptAddr;
  end;
end;

procedure TEvoMRC.PassthroughExceptionAndWarnFmt(s: string;
  const args: array of const);
var
  LException: Exception;
begin
  LException := AcquireExceptionObject;

  if (LException <> nil) and (LException is Exception) then
  begin
    LogMessage('Exception: ' + (LException as Exception).Message );
    raise LException at ExceptAddr;
  end;
end;

procedure TEvoMRC.RunXmlRpcServer;
begin
  FXmlRpcServer.Active := True;
  LogMessage('Evo MRC XML RPC Server started...');
  LogMessage('Version ' + GetVersionFromVersionTxt);
end;

procedure TEvoMRC.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  FXmlRpcServer := TEvoMRCXmlRPCServer.Create(Self, Self);
  FXmlRpcServer.EvoConnectionParam := LoadEvoConnectionParam;

  RunXmlRpcServer;

  Started := FXmlRpcServer.Active;
end;

procedure TEvoMRC.ServiceStop(Sender: TService;
  var Stopped: Boolean);
begin
  FXmlRpcServer.Active := False;
  FreeAndNil(FXmlRpcServer);
  CloseFileTemporarily;
end;

procedure TEvoMRC.StopException;
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
end;

procedure TEvoMRC.StopExceptionAndWarnFmt(s: string;
  const args: array of const);
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
end;


//DeleteNTService(ServiceName: String):boolean;
// ServiceName - ��� ������� ����������� ��������
//���������:
// true - ���� ��������� ��������� �������
// false - ���� ���� ������. ����� ���������� call �� GetLastError �����
// ��������������� �� �������� ������

function DeleteNTService(ServiceName: string): boolean;
var
  hServiceToDelete, hSCMgr: SC_HANDLE;
  ss: _SERVICE_STATUS;
begin
  Result := false;
  hSCMgr := OpenSCManager(nil, nil, SC_MANAGER_CREATE_SERVICE);
  if (hSCMgr <> 0) then
  try
    hServiceToDelete := OpenService(hSCMgr, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if hServiceToDelete <> 0 then
    begin
      QueryServiceStatus( hServiceToDelete, SS );
      if ss.dwCurrentState = SERVICE_RUNNING then
        ControlService(hServiceToDelete, SERVICE_CONTROL_STOP, SS);
      DeleteService(hServiceToDelete);
    end;
  finally
    CloseServiceHandle(hSCMgr);
  end;
end;

end.
