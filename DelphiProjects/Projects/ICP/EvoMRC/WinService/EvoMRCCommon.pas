unit EvoMRCCommon;

interface

uses XmlRpcTypes, Classes, SysUtils, common, isSettings, Windows,
  gdyCrypt, gdyCommon;

  function BuildSQL(aTableName: string; aFields: IRpcStruct): string;
  function LoadEvoConnectionParam: TEvoAPIConnectionParam;
  procedure SaveEvoConnectionParam(aParam: TEvoAPIConnectionParam);

implementation

const
  //!! encrypt it?
  cKey='Form.Button';

function LoadEvoConnectionParam: TEvoAPIConnectionParam;
var
  conf: IisSettings;
  root: string;
  keypath: string;
begin
  keypath := 'SYSTEM\CurrentControlSet\Services\EvoMRC';
  conf := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, keypath);
  root := '\Parameters\';
  Result.APIServerAddress := conf.AsString[root+'APIServerAddress'];
  Result.APIServerPort := 9943;
  Result.Username := conf.AsString[root+'Username'];

  if conf.GetValueNames(root).IndexOf('Password') <> -1 then
    Result.Password := conf.AsString[root+'Password']
  else
    Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);

  Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
end;

procedure SaveEvoConnectionParam(aParam: TEvoAPIConnectionParam);
var
  conf: IisSettings;
  root: string;
  keypath: string;
begin
  keypath := 'SYSTEM\CurrentControlSet\Services\EvoMRC';
  conf := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, keypath);
  root := '\Parameters\';

  conf.AsString[root+'APIServerAddress'] := aParam.APIServerAddress;
  conf.AsInteger[root+'APIServerPort'] := 9943;
  conf.AsString[root+'Username'] := aParam.Username;

  conf.DeleteValue(root+'Password');
  if aParam.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt( aParam.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := aParam.SavePassword;
end;

function GetFieldCondition(aFieldName: string; aField: IRpcStructItem): string;
begin
  Result := '';
  case aField.DataType of
  dtInteger:
    Result := aFieldName + '=' + IntToStr(aField.AsInteger);
  dtString:
    Result := aFieldName + '=''' + aField.AsString + '''';
  dtDateTime:
    Result := aFieldName + ' = cast('''+ FormatDateTime('yyyy-mm-dd hh:nn:ss.zzzz', aField.AsDateTime) + ''' as timestamp)';
  dtFloat:
    Result := aFieldName + '=' + FloatToStr(aField.AsFloat);
  {add the other types}
  else
    Result := '1 <> 1';
  end;
  Result := '(' + Result + ')';
end;

function BuildSQL(aTableName: string; aFields: IRpcStruct): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to aFields.Count - 1 do
    Result := Result + ' and ' + GetFieldCondition(aFields.KeyList[i], aFields.Items[i]);

  if Result <> '' then
    Result := ' where ' + Copy(Result, 6, Length(Result));// + ' and Active_Record = ''C''';

  Result := 'select ' + aTableName + '_NBR from ' + aTableName + Result;
end;

end.
