unit ConnectionDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, PasswordEdit, common;

type
  TfrmSettings = class(TForm)
    gbSettings: TGroupBox;
    lblAPIServer: TLabel;
    APIServerEdit: TEdit;
    lblUserName: TLabel;
    UserNameEdit: TEdit;
    PasswordEdit: TPasswordEdit;
    lblPassword: TLabel;
    cbSavePassword: TCheckBox;
    btnSave: TButton;
    Button1: TButton;
    procedure btnSaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FEvoConnectionParam: TEvoAPIConnectionParam;
  public
    procedure LoadEvoAPIConnectionParam;
    procedure SaveEvoAPIConnectionParam;
  end;

var
  frmSettings: TfrmSettings;

implementation

{$R *.dfm}

uses EvoMRCCommon;

const
  cKey='Form.Button';

{ TfrmSettings }

procedure TfrmSettings.LoadEvoAPIConnectionParam;
begin
  FEvoConnectionParam := LoadEvoConnectionParam;

  APIServerEdit.Text := FEvoConnectionParam.APIServerAddress;
  UserNameEdit.Text := FEvoConnectionParam.Username;
  PasswordEdit.Text := FEvoConnectionParam.Password;
  cbSavePassword.Checked := FEvoConnectionParam.SavePassword;
end;

procedure TfrmSettings.SaveEvoAPIConnectionParam;
begin
  FEvoConnectionParam.APIServerAddress := APIServerEdit.Text;
  FEvoConnectionParam.APIServerPort := 9943;
  FEvoConnectionParam.Username := UserNameEdit.Text;
  FEvoConnectionParam.Password := PasswordEdit.Text;
  FEvoConnectionParam.SavePassword := cbSavePassword.Checked;

  SaveEvoConnectionParam( FEvoConnectionParam );
end;

procedure TfrmSettings.btnSaveClick(Sender: TObject);
begin
  SaveEvoAPIConnectionParam;

  WinExec( 'sc stop EvoMRC', SW_HIDE );
  WinExec( 'sc start EvoMRC', SW_HIDE );

  Close;
end;

procedure TfrmSettings.FormShow(Sender: TObject);
begin
  LoadEvoAPIConnectionParam;

  if ParamCount > 0 then
  begin
    if (Trim(APIServerEdit.Text) = '') and (Trim(UserNameEdit.Text) = '') and
      (Trim(PasswordEdit.Text) = '') then
      WinExec( 'EvoMRCService.exe /install', SW_HIDE )
    else
      WinExec( 'sc start EvoMRC', SW_HIDE );
    Close;
  end;
end;

procedure TfrmSettings.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
