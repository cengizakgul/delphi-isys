program EvoConnectionSettings;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  ConnectionDialog in 'ConnectionDialog.pas' {frmSettings},
  EvoMRCCommon in 'EvoMRCCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmSettings, frmSettings);
  Application.Run;
end.
