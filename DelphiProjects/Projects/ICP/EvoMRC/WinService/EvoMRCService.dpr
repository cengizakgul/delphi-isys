program EvoMRCService;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  SvcMgr,
  main in 'main.pas' {EvoMRC: TService},
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  EvoMRCCommon in 'EvoMRCCommon.pas',
  XmlRpcServer in '..\source\XmlRpcServer.pas',
  XmlRpcServerMod in '..\source\XmlRpcServerMod.pas';

{$R ..\Source\EvoMRC.RES}

begin
  LicenseKey := '4FD12EE4AFF0433191159E7B43709FD5'; //EvoMRC

  Application.Initialize;
  Application.CreateForm(TEvoMRC, EvoMRC);
  Application.Run;
end.
