unit XmlRpcServerMod;

interface

uses
  XmlRpcServer, XmlRpcTypes, Classes, SysUtils, ConstAndProc, Dialogs,
  kbmMemTable, DB, gdyCommonLogger, evoApiconnection, common, EeHolder, SyncObjs;

type
  WrongSessionException = class(Exception);
  WrongLicenseException = class(Exception);
  APIException = class(Exception);

  TevRPCMethod = procedure (const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String) of object;
  PTevRPCMethod = ^TevRPCMethod;

  TEvoMRCXmlRPCServer = class
  private
    FRpcServer: TRpcServer;
    FMethods: TStringList;
    FLogger: ICommonLogger;
    FEvoAPIConnectionParam: TEvoAPIConnectionParam;
    FMessenger: IMessenger;
    FEvoAPI: IEvoAPIConnection;

    procedure RegisterMethods;
    procedure RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);

    procedure getDBDTs(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);

    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function FindCompany(var aCompany: TEvoCompanyDef): boolean;

    function GetActive: boolean;
    procedure SetActive(AValue: boolean);
    procedure SetEvoConnectionParam(aValue: TEvoAPIConnectionParam);
  public
    constructor Create(logger: ICommonLogger; Messenger: IMessenger);
    destructor Destroy; override;

    property Active: boolean read GetActive write SetActive;
    property EvoConnectionParam: TEvoAPIConnectionParam read FEvoAPIConnectionParam write SetEvoConnectionParam;
  end;

implementation

uses gdyRedir, gdyUtils, StrUtils, Variants, DateUtils;

procedure APICheckCondition(const ACondition: Boolean;
  AExceptClass: ExceptClass; const aMessage: string = '');
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      raise Exception.Create('APICheckCondition: exception class should be assigned!')
    else begin
      if Trim(aMessage) <> '' then
        raise AExceptClass.Create('APIException: ' + aMessage)
      else
        raise AExceptClass.Create('APIException');
    end;
  end;
end;

{ TEvoMRCXmlRPCServer }

constructor TEvoMRCXmlRPCServer.Create(logger: ICommonLogger; Messenger: IMessenger);
begin
  FLogger := logger;
  FMessenger := Messenger;

  FMethods := TStringList.Create;
  FMethods.Duplicates := dupError;
  FMethods.CaseSensitive := False;
  FMethods.Sorted := True;

  FRpcServer := TRpcServer.Create( logger );

  RegisterMethods;
end;

destructor TEvoMRCXmlRPCServer.Destroy;
begin
  FRpcServer.Active := False;

  FreeAndNil(FRpcServer);
  FreeAndNil(FMethods);
  inherited;
end;

function TEvoMRCXmlRPCServer.FindCompany(
  var aCompany: TEvoCompanyDef): boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := False;
  aCompany.ClNbr := -1;
  aCompany.CoNbr := -1;

  param := TRpcStruct.Create;
  Param.AddItem( 'CoNumber', aCompany.Custom_Company_Number );
  FClCoData :=  RunQuery('TmpCoQuery.rwq', Param);
  try
    if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
    begin
      aCompany.ClNbr := FClCoData.FindField('CL_NBR').AsInteger;
      aCompany.CoNbr := FClCoData.FindField('CO_NBR').AsInteger;
      Result := True;
    end;
  finally
    FClCoData.Indexes.Clear;
    FreeAndNil( FClCoData );
    Param := nil;
  end;
end;

function TEvoMRCXmlRPCServer.GetActive: boolean;
begin
  Result := Assigned(FRpcServer) and FRpcServer.Active;
end;

procedure TEvoMRCXmlRPCServer.RegisterMethods;

   procedure RegisterMethod(const ASignature, AHelp: String; const AHandler: TevRPCMethod);
   var
     RpcMethodHandler: TRpcMethodHandler;
     s: String;
   begin
     s := ASignature;
     RpcMethodHandler := TRpcMethodHandler.Create;
     RpcMethodHandler.Name := GetNextStrValue(s, '(');
     RpcMethodHandler.Method := RPCHandler;
     RpcMethodHandler.Signature := ASignature;
     RpcMethodHandler.Help := AHelp;
     FRpcServer.RegisterMethodHandler(RpcMethodHandler);

     FMethods.AddObject(RpcMethodHandler.Name, @AHandler);
   end;

begin
  RegisterMethod('getDBDTs(string CompanyNumber)',
                 'Return: struct',
                 getDBDTs);
end;

procedure TEvoMRCXmlRPCServer.RPCHandler(Thread: TRpcThread;
  const MethodName: string; List: TList; Return: TRpcReturn);
var
  i: Integer;
  Proc: TMethod;
begin
  try
    i := FMethods.IndexOf(MethodName);
    APICheckCondition(i <> -1, APIException, 'Unregistered method "' + MethodName + '"');

    Proc.Data := Self;
    Proc.Code := FMethods.Objects[i];
    TevRPCMethod(Proc)(List, Return, LowerCase(MethodName));
    
  except
    on E: APIException do
    begin
      Return.SetError(800, E.Message);
    end;

    on E: WrongSessionException do
    begin
      Return.SetError(810, 'Wrong session');
    end;

    on E: WrongLicenseException do
    begin
      Return.SetError(800, 'Wrong license key');
    end;

    on E: Exception do
    begin
      Return.SetError(500, E.Message);
    end;
  end;
end;

function TEvoMRCXmlRPCServer.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

procedure TEvoMRCXmlRPCServer.SetActive(AValue: boolean);
begin
  if FRpcServer.Active <> AValue then
  begin
    if AValue then
    begin
      FMessenger.ClearMessages;

      try
        FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);
        FMessenger.AddMessage('Evo Connection established');

        FLogger.LogEntry( 'Starting to EvoMRC XML-RPC Server' );
        try
          try
            FRpcServer.ListenPort := 9944;
            FRpcServer.EnableIntrospect := True;

            // Set security  connection
{            FRpcServer.SSLEnable := True;
            FRpcServer.SSLCertFile := Redirection.GetFilename(sCertificateFileAlias);
            FRpcServer.SSLKeyFile := Redirection.GetFilename(sKeyCertificateFileAlias);
            FRpcServer.SSLRootCertFile := Redirection.GetFilename(sRootCertificateFileAlias);}

            FRpcServer.Active := True;
            FMessenger.AddMessage('EvoMRC Server started. Listen port 9944...');
          except
            on e: Exception do
            begin
              FMessenger.AddMessage('EvoMRC Server start error: ' + e.Message);
              FLogger.LogError(e.Message);
            end;
          end;
        finally
          FLogger.LogExit;
        end;

      except
        FMessenger.AddMessage('Evo Connection failed!', True);
      end;
    end
    else
      FRpcServer.Active := False;
  end;
end;

procedure TEvoMRCXmlRPCServer.SetEvoConnectionParam(
  aValue: TEvoAPIConnectionParam);
begin
  Active := False;

  FLogger.LogEntry( 'Set Evo Connection Parameters' );
  try
    FEvoAPIConnectionParam := aValue;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoMRCXmlRPCServer.getDBDTs(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult, CompanyDBDT, Division, Branch, Department, Team, param: IRpcStruct;
  Divisions, Branches, Departments, Teams: IRpcArray;
  dCo, dCoDivision, dCoBranch, dCoDepartment, dCoTeam: TkbmCustomMemTable;
  Co: TEvoCompanyDef;

  procedure FillValues(var aStruct: IRpcStruct; const Prefix: string; aDS: TkbmCustomMemTable);
  begin
    aStruct := TRpcStruct.Create;
    //aStruct.Clear;
    aStruct.AddItem(Prefix + 'Name', Trim(aDS.FieldByName(Prefix + 'Name').AsString));
    aStruct.AddItem(Prefix + 'Nbr', aDS.FieldByName(Prefix + 'Nbr').AsInteger);
    aStruct.AddItem(Prefix + 'Number', Trim(aDS.FieldByName(Prefix + 'Number').AsString));
  end;

  procedure FilterDetailDS(aDetailDS, aMasterDS: TkbmCustomMemTable; const Prefix: string);
  begin
    aDetailDS.Filtered := False;
    aDetailDS.Filter := Prefix + 'Nbr='+aMasterDS.FieldByName(Prefix + 'Nbr').AsString;
    aDetailDS.Filtered := True;
    aDetailDS.First;
  end;

  procedure AddArrayItem(var MasterArray: IRpcArray; var MasterStruct: IRpcStruct; DetailArray: IRpcArray; const sArrayItemName: string);
  begin
    if DetailArray <> nil then
      if DetailArray.Count > 0 then
        MasterStruct.AddItem(sArrayItemName, DetailArray);

    MasterArray.AddItem( MasterStruct );
  end;

begin
  APICheckCondition(AParams.Count = 1, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber

  // Find the company
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[0]).AsString;
  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found', [Co.CUSTOM_COMPANY_NUMBER]));

  // Open Client
  FEvoAPI.OpenClient( Co.ClNbr );

  rpcResult := TRpcStruct.Create;
  try
    try
      param := TRpcStruct.Create;
      CompanyDBDT := TRpcStruct.Create;

      Param.AddItem( 'CoNbr', Co.CoNbr );
      dCo :=  RunQuery('CoQuery.rwq', Param);
      dCoDivision :=  RunQuery('CoDivisionQuery.rwq', Param);
      dCoBranch :=  RunQuery('CoBranchQuery.rwq', Param);
      dCoDepartment :=  RunQuery('CoDepartmentQuery.rwq', Param);
      dCoTeam :=  RunQuery('CoTeamQuery.rwq', Param);
      try
        CompanyDBDT.AddItem('CompanyName', dCo.FieldByName('CompanyName').AsString);
        CompanyDBDT.AddItem('CoNbr', Co.CoNbr);
        CompanyDBDT.AddItem('ClNbr', Co.ClNbr);
        CompanyDBDT.AddItem('DBDTLevel', dCo.FieldByName('DBDTLevel').AsString);

        dCoDivision.First;
        Division := TRpcStruct.Create;
        Divisions := TRpcArray.Create;
        while not dCoDivision.Eof do
        begin
          FillValues(Division, 'Div', dCoDivision);
          FilterDetailDS(dCoBranch, dCoDivision, 'Div');
          Branches := TRpcArray.Create;

          while not dCoBranch.Eof do
          begin
            FillValues(Branch, 'Br', dCoBranch);
            FilterDetailDS(dCoDepartment, dCoBranch, 'Br');
            Departments := TRpcArray.Create;

            while not dCoDepartment.Eof do
            begin
              FillValues(Department, 'Dep', dCoDepartment);
              FilterDetailDS(dCoTeam, dCoDepartment, 'Dep');
              Teams := TRpcArray.Create;

              while not dCoTeam.Eof do
              begin
                FillValues(Team, 'Tm', dCoTeam);

                AddArrayItem(Teams, Team, nil, '');
                dCoTeam.Next;
              end;

              AddArrayItem(Departments, Department, Teams, 'Teams');
              dCoDepartment.Next;
            end;

            AddArrayItem(Branches, Branch, Departments, 'Departments');
            dCoBranch.Next;
          end;

          AddArrayItem(Divisions, Division, Branches, 'Branches');
          dCoDivision.Next;
        end;

        CompanyDBDT.AddItem('Divisions', Divisions);

        rpcResult.AddItem('Code', 1);
        rpcResult.AddItem('Message', 'Company #' + Co.CUSTOM_COMPANY_NUMBER + ' DBDT structure');
        rpcResult.AddItem('CompanyDBDT', CompanyDBDT);
      finally
        FreeAndNil( dCo );
        FreeAndNil( dCoDivision );
        FreeAndNil( dCoBranch );
        FreeAndNil( dCoDepartment );
        FreeAndNil( dCoTeam );
      end;
    except
      on e: Exception do
      begin
        rpcResult.AddItem('Code', 0);
        rpcResult.AddItem('Message', e.Message);
      end;
    end;
  finally
    Division := nil;
    Divisions := nil;
    Branch := nil;
    Branches := nil;
    Department := nil;
    Departments := nil;
    Team := nil;
    Teams := nil;
    CompanyDBDT := nil;
    Param := nil;
  end;

  AReturn.AddItem( rpcResult );
end;

end.
