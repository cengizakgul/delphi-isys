program EvoMRC;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  main in 'main.pas' {frmMain},
  Log in 'Log.pas' {frmLogger},
  ConstAndProc in 'ConstAndProc.pas',
  CommonDlg in 'dialogs\CommonDlg.pas' {frmDialog},
  EvoConnectionDlg in 'dialogs\EvoConnectionDlg.pas' {frmEvoConnectionDlg},
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  XmlRpcServerMod in 'XmlRpcServerMod.pas',
  XmlRpcServer in 'XmlRpcServer.pas';

{$R *.res}

begin
  LicenseKey := '4FD12EE4AFF0433191159E7B43709FD5'; //EvoMRC

  Application.Initialize;
  Application.ShowMainForm := False;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
