@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoMRC.msi 
"%pWiXDir%\candle.exe" .\EvoMRC.wxs -wx -out ..\..\..\..\..\Tmp\EvoMRC.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoMRC.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoMRC.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoMRC.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoMRC%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoMRC%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoMRC.msi EvoMRC%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvoMRCService.msi 
"%pWiXDir%\candle.exe" .\EvoMRCService.wxs -wx -out ..\..\..\..\..\Tmp\EvoMRCService.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoMRCService.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoMRCService.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoMRCService.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoMRCService%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoMRCService%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoMRCService.msi EvoMRCService%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
