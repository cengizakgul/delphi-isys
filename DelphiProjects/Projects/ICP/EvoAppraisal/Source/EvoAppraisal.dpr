program EvoAppraisal; 

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  common,
  evoapiconnection,
  jclfileutils,
  gdyredir,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionClCoFrame in '..\..\common\EvolutionClCoFrame.pas' {EvolutionClCoFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  sevenzip in '..\..\common\gdycommon\sevenzip\sevenzip.pas',
  EvoAPIClientNewMainForm in '..\..\common\EvoAPIClientNewMainForm.pas' {EvoAPIClientNewMainFm: TEvoAPIClientMainFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvoWaitForm in '..\..\common\EvoWaitForm.pas' {EvoWaitFm},
  scheduledTask in '..\..\common\scheduledTask.pas',
  MainForm in 'MainForm.pas' {MainFm: TEvoAPIClientNewMainFm},
  EeHolder in 'EeHolder.pas',
  AppraisalProcessing in 'AppraisalProcessing.pas',
  TCImportFileBuilder in 'TCImportFileBuilder.pas',
  FileOpenFrame in '..\..\Common\FileOpenFrame.pas' {FileOpenFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\Common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame};

{$R *.res}
begin
//  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License
  LicenseKey := '8428945913E14DDC83DF84382391F861'; // EvoApprasial License

  ConfigVersion := 4;

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'EvoAppraisal';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
