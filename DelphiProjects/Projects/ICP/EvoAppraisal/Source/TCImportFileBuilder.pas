unit TCImportFileBuilder;

interface

uses
  XLSFile, XLSRects, SysUtils, kbmMemTable, DB, common;

type
  TImportFileBuilder = class
  private
    FFileName: string;
    FTcImportFile: string;
    FErrorMsg: string;
    FData: TkbmCustomMemTable;
    FRegularEDCode: string;
  protected
    procedure AddRow(const RefNbr, FileNbr, Address: string; Fee: double);
    procedure ProcessXlsFile;
    procedure BuildTCImportFile;
    procedure CreateDataset;
  public
    constructor Create(const aFileName: string; const RegularEDCode: string);
    function getErrorMessage: string;
    function getTCImportFile: string;
    property ExportData: TkbmCustomMemTable read FData;
  end;

implementation

uses Variants;

{ TImportFileBuilder }

procedure TImportFileBuilder.ProcessXlsFile;
const
  // the data are on the first page
  // the 1st column is Ref# (EE.Custom_Employee_Number),
  // the 3rd column is File#
  // the 4th column is Address
  // the 5th column is Fee
  idxSheet = 0;
  idxRefNbr = 0;
  idxFileNbr = 2;
  idxAddress = 3;
  idxFee = 4;
var
  xf: TXLSFile;
  i: integer;
  Rect: TRangeRect;
  RefNbr, FileNbr, Address: string;
  Fee: double;
begin
  xf:= TXLSFile.Create;
  try
    xf.OpenFile(FFileName);

    if not xf.Workbook.Sheets[idxSheet].GetUsedRect(Rect) then
      raise Exception.Create('Sheet is empty')
    else
      //check if the first row contains column titles
      if TryStrToFloat( Trim(VarToStr( xf.Workbook.Sheets[0].Cells[0, idxFee].Value )), Fee) then
      begin
        RefNbr := Trim(VarToStr( xf.Workbook.Sheets[0].Cells[0, idxRefNbr].Value ));
        if RefNbr = '' then
          raise Exception.Create('The row (' + IntToStr(0) + ') has empty Ref.# value.');

        FileNbr := Trim(VarToStr( xf.Workbook.Sheets[0].Cells[0, idxFileNbr].Value ));
        if FileNbr = '' then
          raise Exception.Create('The row (' + IntToStr(0) + ') has empty File Number value.');

        Address := Trim(VarToStr( xf.Workbook.Sheets[0].Cells[0, idxAddress].Value ));
        if Address = '' then
          raise Exception.Create('The row (' + IntToStr(0) + ') has empty Address value.');

        AddRow(RefNbr, FileNbr, Address, Fee);
      end;
      // else the first row contains column titles

      for i := 1 to Rect.RowTo do
      begin
        RefNbr := Trim(VarToStr( xf.Workbook.Sheets[0].Cells[i, idxRefNbr].Value ));
        if RefNbr = '' then
          raise Exception.Create('The row (' + IntToStr(i) + ') has empty Ref.# value.');

        FileNbr := Trim(VarToStr( xf.Workbook.Sheets[0].Cells[i, idxFileNbr].Value ));
        if FileNbr = '' then
          raise Exception.Create('The row (' + IntToStr(i) + ') has empty File Number value.');

        Address := Trim(VarToStr( xf.Workbook.Sheets[0].Cells[i, idxAddress].Value ));
        if Address = '' then
          raise Exception.Create('The row (' + IntToStr(i) + ') has empty Address value.');

        if not TryStrToFloat( Trim(VarToStr( xf.Workbook.Sheets[0].Cells[i, idxFee].Value )), Fee) then
          raise Exception.Create('The row (' + IntToStr(i) + ') has incorrect Fee value: ' + Trim(VarToStr( xf.Workbook.Sheets[0].Cells[i, idxFee].Value )));

        AddRow(RefNbr, FileNbr, Address, Fee);
      end;
  finally
    FreeAndNil(xf);
  end;
end;

constructor TImportFileBuilder.Create(const aFileName: string; const RegularEDCode: string);
begin
  FFileName := aFileName;
  FTcImportFile := '';
  FErrorMsg := '';
  FRegularEDCode := RegularEDCode;
  try
    if Trim(RegularEDCode) = '' then
      FErrorMsg := 'Regular E/D code is not set'
    else begin
      CreateDataset;
      ProcessXlsFile;
      BuildTCImportFile;
    end;
  except
    on E:Exception do
      FErrorMsg := E.Message;
  end;
end;

function TImportFileBuilder.getErrorMessage: string;
begin
  Result := FErrorMsg;
end;

function TImportFileBuilder.getTCImportFile: string;
begin
  Result := FTcImportFile;
end;

procedure TImportFileBuilder.BuildTCImportFile;
begin
  FData.First;
  while not FData.Eof do
  begin
    //EeCode,Name,OverrideDept,Job,Shift,D/E,EarnCode
    FTcImportFile := FTcImportFile + FData.FieldByName('EeCode').AsString + ',,,,,'+Copy(FRegularEDCode,1,1)+',' + Copy(FRegularEDCode,2,2) + ',';

    //Rate,Hours,
    FTcImportFile := FTcImportFile + ',,';

    //(LineItemDate)Year,Month,Day,Hour,Minute
    FTcImportFile := FTcImportFile + ',,,,,';

    //Amount,SeqNumber,OverrideDivision,OverrideBranch,OverrideState,OverrideLocal,BlankFill,Deduction3Hours,Deduction3Amount,SSN,W/C,RateNumber
    FTcImportFile := FTcImportFile + FData.FieldByName('Amount').AsString + ',,,,,,,,,,,,';

    //PunchIn(Yyyymmddhhnnss)
    FTcImportFile := FTcImportFile + ',';

    //PunchOut(Yyyymmddhhnnss),OverrideTeam,CheckComments,
    FTcImportFile := FTcImportFile + ',,"' + FData.FieldByName('Comments').AsString + '",';

    //(LineItemEndDate)Year,Month,Day
    FTcImportFile := FTcImportFile + ',,';

    FData.Next;
    if not FData.Eof then
      FTcImportFile := FTcImportFile + #13 + #10;
  end;
end;

procedure TImportFileBuilder.AddRow(const RefNbr, FileNbr, Address: string;
  Fee: double);
begin
  Assert(FData.Active);
  if FData.Locate('EeCode', RefNbr, []) then
  begin
    FData.Edit;
    FData.FieldByName('Comments').AsString := FData.FieldByName('Comments').AsString + '|' + FileNbr + ' ' + Address + ' ' + FormatFloat('0.00', Fee);
    FData.FieldByName('Amount').AsFloat := FData.FieldByName('Amount').AsFloat + Fee;
  end
  else begin
    FData.Append;
    FData.FieldByName('EeCode').AsString := RefNbr;
    FData.FieldByName('Comments').AsString := FileNbr + ' ' + Address + ' ' + FormatFloat('0.00', Fee);
    FData.FieldByName('Amount').AsFloat := Fee;
  end;
  FData.Post;
end;

procedure TImportFileBuilder.CreateDataset;
begin
  // EeCode - varchar(32)
  // Comments - varchar(8084)
  // Amount - float
  FData := TkbmCustomMemTable.Create(nil);
  CreateStringField(FData, 'EeCode', 'Ee Code', 32);
  CreateStringField(FData, 'Comments', 'Comments', 8084);
  CreateFloatField(FData, 'Amount', 'Amount');
  FData.Open;
end;

end.
