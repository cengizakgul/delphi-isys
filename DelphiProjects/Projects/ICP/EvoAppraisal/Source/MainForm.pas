unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientNewMainForm, ActnList,
  EvolutionCompanySelectorFrame, StdCtrls, Buttons, ComCtrls,
  SmtpConfigFrame, SchedulerFrame, ExtCtrls, OptionsBaseFrame,
  EvoAPIConnectionParamFrame, scheduledTask,
  evoapiconnectionutils, common, Grids, Wwdbigrd, Wwdbgrid, dbcomp,
  FileOpenFrame, DB, EvolutionPrPrBatchFrame;

type
  TMainFm = class(TEvoAPIClientNewMainFm)
    GroupBox1: TGroupBox;
    pnlSpace: TPanel;
    gbJustFrame: TGroupBox;
    lblPrBatch: TLabel;
    bvl1: TBevel;
    FileOpenFrm: TFileOpenFrm;
    bvl2: TBevel;
    lbl3: TLabel;
    BitBtn1: TBitBtn;
    actRunExport: TAction;
    EvolutionPrPrBatchFrm: TEvolutionPrPrBatchFrm;
    procedure actNeedExtAppAndCoUpdate(Sender: TObject);
    procedure actRunExportUpdate(Sender: TObject);
    procedure actRunExportExecute(Sender: TObject);
  protected
    procedure UnInitPerCompanySettings; override;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef); override;
  public
    constructor Create( Owner: TComponent ); override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  AppraisalProcessing, userActionHelpers,
  evodata, PlannedActionConfirmationForm, gdyGlobalWaitIndicator,
  gdyClasses, EvoWaitForm, EvoAPIClientMainForm, gdyDialogEngine;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;

  if EvoFrame.IsValid then
    PageControl1.ActivePageIndex := 2
  else
    PageControl1.ActivePageIndex := 0;

  FEvoData.AddDS(TMP_PRDesc);
  FEvoData.AddDS(PR_BATCHDesc);
  FEvoData.AddDS(CL_E_DSDesc);
  EvolutionPrPrBatchFrm.Init( FEvoData );
  FileOpenFrm.FileOpen1.Dialog.Filter := 'Excel-files (*.xls)|*.xls'; 

  Caption := Application.Title + ' ' + GetVersion;
end;

procedure TMainFm.actNeedExtAppAndCoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and FEvoData.CanGetClCo;
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  inherited;
end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  inherited;

end;

procedure TMainFm.actRunExportUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := FileExists(FileOpenFrm.edtFile.Text) and FEvoData.CanGetPrBatch;
end;

procedure TMainFm.actRunExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      RunTCImport(Logger, FEvoData.GetPrBatch, FEvoData.Connection, FileOpenFrm.edtFile.Text,
        'C01'{FEvoData.DS['CL_E_DS'].FieldByName('Custom_E_D_Code_Number').AsString});
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.
