inherited MainFm: TMainFm
  Left = 376
  Top = 139
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabSheet3: TTabSheet
      object pnlSpace: TPanel
        Left = 0
        Top = 106
        Width = 784
        Height = 5
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
    inherited tbshCompanySettings: TTabSheet
      TabVisible = False
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 89
        Align = alTop
        Caption = 'EvoPayrollPlans connection'
        TabOrder = 0
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Export data to Evolution'
      inherited pnlBottom: TPanel
        Top = 427
        Height = 1
      end
      object gbJustFrame: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 427
        Align = alClient
        TabOrder = 1
        DesignSize = (
          784
          427)
        object lblPrBatch: TLabel
          Left = 16
          Top = 19
          Width = 189
          Height = 13
          Caption = '1. Select Payroll/Batch to export data to'
        end
        object bvl1: TBevel
          Left = 16
          Top = 313
          Width = 753
          Height = 9
          Anchors = [akLeft, akRight, akBottom]
          Shape = bsTopLine
        end
        object bvl2: TBevel
          Left = 16
          Top = 366
          Width = 753
          Height = 9
          Anchors = [akLeft, akRight, akBottom]
          Shape = bsTopLine
        end
        object lbl3: TLabel
          Left = 16
          Top = 382
          Width = 9
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = '3.'
        end
        inline FileOpenFrm: TFileOpenFrm
          Left = 9
          Top = 317
          Width = 765
          Height = 41
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 0
          inherited Panel1: TPanel
            Width = 765
            inherited Label1: TLabel
              Width = 87
              Caption = '2. Select Data File'
            end
            inherited BitBtn1: TBitBtn
              Left = 685
            end
            inherited edtFile: TEdit
              Left = 105
              Width = 573
            end
          end
          inherited ActionList1: TActionList
            inherited FileOpen1: TFileOpen
              Hint = 'Open|Opens an Excel file'
            end
          end
        end
        object BitBtn1: TBitBtn
          Left = 37
          Top = 375
          Width = 155
          Height = 27
          Action = actRunExport
          Anchors = [akLeft, akBottom]
          Caption = 'Run Export data to Evolution'
          TabOrder = 1
        end
        inline EvolutionPrPrBatchFrm: TEvolutionPrPrBatchFrm
          Left = 17
          Top = 34
          Width = 753
          Height = 249
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 2
          inherited Splitter1: TSplitter
            Height = 249
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 249
            inherited dgGrid: TReDBGrid
              Height = 224
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 434
            Height = 249
            inherited Panel1: TPanel
              Width = 434
            end
            inherited dgGrid: TReDBGrid
              Width = 434
              Height = 224
            end
          end
        end
      end
    end
    inherited tbshScheduler: TTabSheet
      TabVisible = False
      inherited SchedulerFrame: TSchedulerFrm
        inherited PageControl2: TPageControl
          inherited tbshTasks: TTabSheet
            inherited pnlDetails: TPanel
              inherited pcTaskDetails: TPageControl
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 601
                  end
                  inherited dgResults: TReDBGrid
                    Width = 601
                  end
                end
              end
            end
          end
        end
      end
    end
    inherited tbshSchedulerSettings: TTabSheet
      TabVisible = False
    end
  end
  inherited pnlCompany: TPanel
    inherited Panel1: TPanel
      Width = 99
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 99
      Width = 693
    end
  end
  inherited ActionList1: TActionList
    Left = 516
    Top = 88
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    object actRunExport: TAction
      Caption = 'Run Export data to Evolution'
      OnExecute = actRunExportExecute
      OnUpdate = actRunExportUpdate
    end
  end
end
