unit AppraisalProcessing;

interface

uses
  Evconsts, evodata, Classes, XmlRpcTypes,
  gdycommonlogger, PlannedActions, EeHolder,
  evoapiconnectionutils, gdyClasses, common, kbmMemTable, EvoAPIConnection;

type
  TImportProcessing = class
  private
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    FPrBatch: TEvoPayrollBatchDef;
    FRegularEDCode: string;

    procedure ApplyChangePacket(aChanges: IRpcArray);
    function getExistingChecks(aPrBatchNbr: integer): TkbmCustomMemTable;
    function BuildCheckCommentsChangePacket(ExistingChecks, Comments: TkbmCustomMemTable): IRpcArray;

    procedure TCImport(const aData: string);
    procedure UpdateCheckComments(aData: TkbmCustomMemTable);
  public
    constructor Create(logger: ICommonLogger; PrBatch: TEvoPayrollBatchDef; EvoConn: IEvoAPIConnection;
      const RegularEDCode: string);
    destructor Destroy; override;

    procedure ImportFromFile(const aFileName: string);
  end;

  procedure RunTCImport(Logger: ICommonLogger; PrBatch: TEvoPayrollBatchDef; evoConn: IEvoAPIConnection;
    const aDataFile, RegularEDCode: string);

implementation

uses
  sysutils, variants, gdycommon, TCImportFileBuilder,
  EvoWaitForm, gdyRedir, gdyGlobalWaitIndicator, userActionHelpers,
  XmlRpcCommon, PlannedActionConfirmationForm, forms, dateutils, DB;

procedure RunTCImport(Logger: ICommonLogger; PrBatch: TEvoPayrollBatchDef;
  evoConn: IEvoAPIConnection; const aDataFile, RegularEDCode: string);
begin
  with TImportProcessing.Create(Logger, PrBatch, evoConn, RegularEDCode) do
  try
    ImportFromFile(aDataFile);
  finally
    Free;
  end;
end;

{ TImportProcessing }

procedure TImportProcessing.ApplyChangePacket(aChanges: IRpcArray);
begin
  if aChanges.Count > 0 then
  try
    FEvoAPI.applyDataChangePacket( aChanges );
  except
    on e: Exception do begin
      FLogger.LogError('Apply Data Change Packet error!', e.Message);
    end;
  end;
end;

function TImportProcessing.BuildCheckCommentsChangePacket(
  ExistingChecks, Comments: TkbmCustomMemTable): IRpcArray;
var
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
  NewClBlobNbr: integer;
begin
  Assert(ExistingChecks.Active);
  Assert(Comments.Active);

  // ExistingChecks fields:
  // Pr_Check_Nbr - integer
  // EeCode - varchar(32)
  // Notes_Nbr - integer (reference to CL_BLOB.CL_BLOB_NBR)
  // -----
  // Comments fields:
  // EeCode - varchar(32)
  // Comments - varchar(8084)
  // Amount - float
  NewClBlobNbr := 0;
  Result := TRpcArray.Create;
  ExistingChecks.First;
  while not ExistingChecks.Eof do
  begin
    if Comments.Locate('EeCode', ExistingChecks.FieldByName('EeCode').AsString, []) then
    begin
      Fields := TRpcStruct.Create;
      RowChange := TRpcStruct.Create;

      Fields.AddItem('DATA', Comments.FieldByName('Comments').AsString);

      if ExistingChecks.FieldByName('Notes_Nbr').IsNull then
      begin
        // check exists but comments does not -> need to create ones
        NewClBlobNbr := NewClBlobNbr - 1;
        Fields.AddItem('CL_BLOB_NBR', NewClBlobNbr);

        RowChange.AddItem('T', 'I');
        RowChange.AddItem('F', Fields);
        RowChange.AddItem('D', 'CL_BLOB');

        Result.AddItem( RowChange );

        // update Pr_Check.Notes_Nbr with a newly created check comments blob key value
        Fields := TRpcStruct.Create;
        RowChange := TRpcStruct.Create;

        Fields.AddItem('NOTES_NBR', NewClBlobNbr);

        RowChange.AddItem('T', 'U');
        RowChange.AddItem('K', ExistingChecks.FieldByName('Pr_Check_Nbr').AsInteger);
        RowChange.AddItem('F', Fields);
        RowChange.AddItem('D', 'PR_CHECK');

        Result.AddItem( RowChange );
      end
      else begin
        // check exists and comments exists, so just update comments
        RowChange.AddItem('T', 'U');
        RowChange.AddItem('K', ExistingChecks.FieldByName('Notes_Nbr').AsInteger);
        RowChange.AddItem('F', Fields);
        RowChange.AddItem('D', 'CL_BLOB');

        Result.AddItem( RowChange );
      end;
    end;
    ExistingChecks.Next;
  end;
end;

constructor TImportProcessing.Create(logger: ICommonLogger; PrBatch: TEvoPayrollBatchDef;
  EvoConn: IEvoAPIConnection; const RegularEDCode: string);
begin
  FLogger := logger;
  FEvoAPI := EvoConn;
  FPrBatch := PrBatch;
  FRegularEDCode := RegularEDCode;
end;

destructor TImportProcessing.Destroy;
begin
  inherited;
end;

function TImportProcessing.getExistingChecks(
  aPrBatchNbr: integer): TkbmCustomMemTable;
var
  param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('PrBatchNbr', aPrBatchNbr);
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'qExistingChecks.rwq', Param);
end;

procedure TImportProcessing.ImportFromFile(const aFileName: string);
var
  DataFileBuilder: TImportFileBuilder;
begin
  DataFileBuilder := TImportFileBuilder.Create(aFileName, FRegularEDCode);
  WaitIndicator.StartWait('Export data to Evolution');
  try
    if DataFileBuilder.getErrorMessage <> '' then
      FLogger.LogError('Process Data File Error', DataFileBuilder.getErrorMessage)
    else begin
      TCImport(DataFileBuilder.getTCImportFile);
      UpdateCheckComments(DataFileBuilder.ExportData);
    end;
  finally
    WaitIndicator.EndWait;
    FreeAndNil(DataFileBuilder);
  end;
end;

procedure TImportProcessing.TCImport(const aData: string);
var
  ImportOptions, ret: IRpcStruct;
begin
  ImportOptions := TRpcStruct.Create;
  try
    ImportOptions.AddItem( 'LookupOption', 1 );  // lookup by Custom EE Number
    ImportOptions.AddItem( 'DBDTOption', 1 );  // DBDT Smart
    ImportOptions.AddItem( 'FourDigitYear', False );
    ImportOptions.AddItem( 'FileFormat', 1 ); // File Format CommaDelimited
    ImportOptions.AddItem( 'AutoImportJobCodes', False );
    ImportOptions.AddItem( 'UseEmployeePayRates', False );
    try
      ret := FEvoAPI.SB_TCImport(FPrBatch.Company.CoNbr, FPrBatch.Pr.PrNbr, aData, ImportOptions, FPrBatch.PrBatchNbr);

      FLogger.LogEvent('TC Import is done', 'Rejected Records = ' + IntToStr(ret.Keys['RejectedRecords'].AsInteger));
      if ret.Keys['RejectedRecords'].AsInteger > 0 then
        FLogger.LogWarning('TC Import Log File', ret.Keys['LogFile'].AsBase64Str);
    except
      FLogger.PassthroughException;
    end;
  finally
    ImportOptions := nil;
    ret := nil;
  end;
end;

procedure TImportProcessing.UpdateCheckComments(aData: TkbmCustomMemTable);
begin
  // The fact is TC Import fill PR_CHECK.check_comments field when it create a new check only
  // otherwise check_comments is not changed, that's why this procedure will update check comments
  // in existing checks via ApplyDataChangePacket
  // -----
  // aData fields:
  // EeCode - varchar(32)
  // Comments - varchar(8084)
  // Amount - float
  WaitIndicator.StartWait('Update Check Comments');
  try
    try
      FEvoAPI.OpenClient( FPrBatch.Company.ClNbr );
      ApplyChangePacket( BuildCheckCommentsChangePacket( getExistingChecks(FPrBatch.PrBatchNbr), aData) );
    except
      FLogger.PassthroughException;
    end;
  finally
    WaitIndicator.EndWait;
  end;
end;

end.

