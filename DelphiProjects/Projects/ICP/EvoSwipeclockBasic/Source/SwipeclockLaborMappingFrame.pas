unit SwipeclockLaborMappingFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, swipeclockdecl, StdCtrls, SwipeclockLaborMappingItemFrame;

type
  TSwipeclockLaborMappingFrm = class(TFrame)
    GroupBox1: TGroupBox;
    RateCodeFieldFrame: TSwipeclockLaborMappingItemFrm;
    BranchFieldFrame: TSwipeclockLaborMappingItemFrm;
    DivisionFieldFrame: TSwipeclockLaborMappingItemFrm;
    ShiftFieldFrame: TSwipeclockLaborMappingItemFrm;
    JobFieldFrame: TSwipeclockLaborMappingItemFrm;
    departmentFieldFrame: TSwipeclockLaborMappingItemFrm;
  private
    function GetLaborMapping: TSwipeClockLaborMapping;
    procedure SetLaborMapping(const Value: TSwipeClockLaborMapping);
  public
    property LaborMapping: TSwipeClockLaborMapping read GetLaborMapping write SetLaborMapping;
    procedure SetFieldDefinitions(x,y,z: string);
  end;

implementation

{$R *.dfm}

{ TSwipeclockLaborMappingFrm }

function TSwipeclockLaborMappingFrm.GetLaborMapping: TSwipeClockLaborMapping;
begin
  Result.DepartmentField := DepartmentFieldFrame.SwField;
  Result.JobField := JobFieldFrame.SwField;
  Result.ShiftField := ShiftFieldFrame.SwField;
  Result.DivisionField := DivisionFieldFrame.SwField;
  Result.BranchField := BranchFieldFrame.SwField;
  Result.RateCodeField := RateCodeFieldFrame.SwField;
end;

procedure TSwipeclockLaborMappingFrm.SetFieldDefinitions(x, y, z: string);
begin
  DepartmentFieldFrame.SetFieldDefinitions(x, y, z);
  JobFieldFrame.SetFieldDefinitions(x, y, z);
  ShiftFieldFrame.SetFieldDefinitions(x, y, z);
  DivisionFieldFrame.SetFieldDefinitions(x, y, z);
  BranchFieldFrame.SetFieldDefinitions(x, y, z);
  RateCodeFieldFrame.SetFieldDefinitions(x, y, z);
end;

procedure TSwipeclockLaborMappingFrm.SetLaborMapping(const Value: TSwipeClockLaborMapping);
begin
  DepartmentFieldFrame.SwField := Value.DepartmentField;
  JobFieldFrame.SwField := Value.JobField;
  ShiftFieldFrame.SwField := Value.ShiftField;
  DivisionFieldFrame.SwField := Value.DivisionField;
  BranchFieldFrame.SwField := Value.BranchField;
  RateCodeFieldFrame.SwField := Value.RateCodeField;
end;

end.
