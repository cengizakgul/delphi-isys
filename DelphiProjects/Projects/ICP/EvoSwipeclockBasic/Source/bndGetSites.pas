
{***********************************************************************}
{                                                                       }
{                           XML Data Binding                            }
{                                                                       }
{         Generated on: 5/16/2009 2:36:48 PM                            }
{       Generated from: E:\job\IS\integration\swipeclock\GetSites.xsd   }
{                                                                       }
{***********************************************************************}

unit bndGetSites;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLNewDataSet = interface;
  IXMLSites = interface;

{ IXMLNewDataSet }

  IXMLNewDataSet = interface(IXMLNodeCollection)
    ['{26EA1252-B352-4D7C-BEBA-E7C5BF47D4FE}']
    { Property Accessors }
    function Get_Sites(Index: Integer): IXMLSites;
    { Methods & Properties }
    function Add: IXMLSites;
    function Insert(const Index: Integer): IXMLSites;
    property Sites[Index: Integer]: IXMLSites read Get_Sites; default;
  end;

{ IXMLSites }

  IXMLSites = interface(IXMLNode)
    ['{16293BA1-F38F-4714-B036-59F65CEC0ED6}']
    { Property Accessors }
    function Get_Site: Integer;
    function Get_SiteName: WideString;
    procedure Set_Site(Value: Integer);
    procedure Set_SiteName(Value: WideString);
    { Methods & Properties }
    property Site: Integer read Get_Site write Set_Site;
    property SiteName: WideString read Get_SiteName write Set_SiteName;
  end;

{ Forward Decls }

  TXMLNewDataSet = class;
  TXMLSites = class;

{ TXMLNewDataSet }

  TXMLNewDataSet = class(TXMLNodeCollection, IXMLNewDataSet)
  protected
    { IXMLNewDataSet }
    function Get_Sites(Index: Integer): IXMLSites;
    function Add: IXMLSites;
    function Insert(const Index: Integer): IXMLSites;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSites }

  TXMLSites = class(TXMLNode, IXMLSites)
  protected
    { IXMLSites }
    function Get_Site: Integer;
    function Get_SiteName: WideString;
    procedure Set_Site(Value: Integer);
    procedure Set_SiteName(Value: WideString);
  end;

{ Global Functions }

function GetNewDataSet(Doc: IXMLDocument): IXMLNewDataSet;
function LoadNewDataSet(const FileName: WideString): IXMLNewDataSet;
function NewNewDataSet: IXMLNewDataSet;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetNewDataSet(Doc: IXMLDocument): IXMLNewDataSet;
begin
  Result := Doc.GetDocBinding('NewDataSet', TXMLNewDataSet, TargetNamespace) as IXMLNewDataSet;
end;

function LoadNewDataSet(const FileName: WideString): IXMLNewDataSet;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('NewDataSet', TXMLNewDataSet, TargetNamespace) as IXMLNewDataSet;
end;

function NewNewDataSet: IXMLNewDataSet;
begin
  Result := NewXMLDocument.GetDocBinding('NewDataSet', TXMLNewDataSet, TargetNamespace) as IXMLNewDataSet;
end;

{ TXMLNewDataSet }

procedure TXMLNewDataSet.AfterConstruction;
begin
  RegisterChildNode('Sites', TXMLSites);
  ItemTag := 'Sites';
  ItemInterface := IXMLSites;
  inherited;
end;

function TXMLNewDataSet.Get_Sites(Index: Integer): IXMLSites;
begin
  Result := List[Index] as IXMLSites;
end;

function TXMLNewDataSet.Add: IXMLSites;
begin
  Result := AddItem(-1) as IXMLSites;
end;

function TXMLNewDataSet.Insert(const Index: Integer): IXMLSites;
begin
  Result := AddItem(Index) as IXMLSites;
end;

{ TXMLSites }

function TXMLSites.Get_Site: Integer;
begin
  Result := ChildNodes['Site'].NodeValue;
end;

procedure TXMLSites.Set_Site(Value: Integer);
begin
  ChildNodes['Site'].NodeValue := Value;
end;

function TXMLSites.Get_SiteName: WideString;
begin
  Result := ChildNodes['SiteName'].Text;
end;

procedure TXMLSites.Set_SiteName(Value: WideString);
begin
  ChildNodes['SiteName'].NodeValue := Value;
end;

end.