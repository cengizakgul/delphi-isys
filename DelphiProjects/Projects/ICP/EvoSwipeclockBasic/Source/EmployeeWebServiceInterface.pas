// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\job\IS\integration\swipeclock\EmployeeWebServiceInterface.wsdl
// Encoding : UTF-8
// Version  : 1.0
// (5/7/2009 11:00:59 PM - 1.33.2.5)
// ************************************************************************ //

unit EmployeeWebServiceInterface;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:float           - "http://www.w3.org/2001/XMLSchema"
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"

  AuthHeader           = class;                 { "https://mc2cs.com/scci"[H] }

  GetEmployeesResult =  type WideString;      { "https://mc2cs.com/scci" }


  // ************************************************************************ //
  // Namespace : https://mc2cs.com/scci
  // ************************************************************************ //
  AuthHeader = class(TSOAPHeader)
  private
    FuserName: WideString;
    Fpassword: WideString;
    Fsite: WideString;
  published
    property userName: WideString read FuserName write FuserName;
    property password: WideString read Fpassword write Fpassword;
    property site: WideString read Fsite write Fsite;
  end;

  GetEmployeeResult =  type WideString;      { "https://mc2cs.com/scci" }
  GetSitesResult  =  type WideString;      { "https://mc2cs.com/scci" }

  // ************************************************************************ //
  // Namespace : https://mc2cs.com/scci
  // soapAction: https://mc2cs.com/scci/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : EmployeeWebServiceInterfaceSoap
  // service   : EmployeeWebServiceInterface
  // port      : EmployeeWebServiceInterfaceSoap
  // URL       : http://www.swipeclock.com/scci/xml/EmployeeWebServiceInterface.asmx
  // ************************************************************************ //
  EmployeeWebServiceInterfaceSoap = interface(IInvokable)
  ['{A4D12EEE-C8B4-E01C-600D-C92AFD58F485}']
    function  GetEmployees: GetEmployeesResult; stdcall;
    function  GetEmployee(const employee: WideString): GetEmployeeResult; stdcall;
    function  GetEmployeeCard(const site: Integer; const employeeCode: WideString; const period: WideString; const password: WideString): WideString; stdcall;
    procedure AddEmployee(const employeeCode: WideString; const lastName: WideString; const firstName: WideString; const middleName: WideString; const designation: WideString; const title: WideString; const ssn: WideString; const dept: WideString; const location: WideString; const supervisor: WideString; 
                          const startDate: WideString; const lunch: Integer; const lunchHours: Single; const payRate0: TXSDecimal; const payRate1: TXSDecimal; const payRate2: TXSDecimal; const payRate3: TXSDecimal; const cardnum1: WideString; const cardNum2: WideString; 
                          const cardNum3: WideString; const password: WideString; const options: WideString; const home1: WideString; const home2: WideString; const home3: WideString; const schedule: WideString; const exportBlock: Integer); stdcall;
    procedure AddEmployeeRequired(const lastName: WideString; const firstName: WideString); stdcall;
    procedure RemoveEmployee(const employeeCode: WideString; const delDate: WideString); stdcall;
    procedure UpdateEmployee(const employeeCode: WideString; const employee: Integer; const lastName: WideString; const firstName: WideString; const middleName: WideString; const designation: WideString; const title: WideString; const ssn: WideString; const dept: WideString; const location: WideString; 
                             const supervisor: WideString; const startDate: WideString; const lunch: Integer; const lunchHours: Single; const payRate0: TXSDecimal; const payRate1: TXSDecimal; const payRate2: TXSDecimal; const payRate3: TXSDecimal; const cardnum1: WideString; 
                             const cardNum2: WideString; const cardNum3: WideString; const password: WideString; const options: WideString; const home1: WideString; const home2: WideString; const home3: WideString; const schedule: WideString; const exportBlock: Integer
                             ); stdcall;
    function  GetSiteCards(const startDate: WideString; const endDate: WideString): WideString; stdcall;
    procedure UpdateEmployeeTime(const punchID: Integer; const employee: Integer; const inPunch: WideString; const outPunch: WideString; const punchDate: WideString; const iData: WideString; const jData: WideString; const kData: WideString; const xData: WideString; const yData: WideString; 
                                 const zData: WideString; const hours: WideString; const lunch: Integer; const payrate: WideString; const category: WideString; const pay: WideString; const payType: WideString); stdcall;
    function  GetSites(const login: WideString): GetSitesResult; stdcall;
  end;

function GetEmployeeWebServiceInterfaceSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): EmployeeWebServiceInterfaceSoap;


implementation

function GetEmployeeWebServiceInterfaceSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): EmployeeWebServiceInterfaceSoap;
const
  defWSDL = 'E:\job\IS\integration\swipeclock\EmployeeWebServiceInterface.wsdl';
  defURL  = 'http://www.swipeclock.com/scci/xml/EmployeeWebServiceInterface.asmx';
  defSvc  = 'EmployeeWebServiceInterface';
  defPrt  = 'EmployeeWebServiceInterfaceSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as EmployeeWebServiceInterfaceSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(EmployeeWebServiceInterfaceSoap), 'https://mc2cs.com/scci', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(EmployeeWebServiceInterfaceSoap), 'https://mc2cs.com/scci/%operationName%');
  InvRegistry.RegisterHeaderClass(TypeInfo(EmployeeWebServiceInterfaceSoap), AuthHeader, 'AuthHeader', ''); //https://mc2cs.com/scci

  RemClassRegistry.RegisterXSInfo(TypeInfo(GetEmployeesResult), 'https://mc2cs.com/scci', 'GetEmployeesResult');
  RemClassRegistry.RegisterXSClass(AuthHeader, 'https://mc2cs.com/scci', 'AuthHeader');
  RemClassRegistry.RegisterXSInfo(TypeInfo(GetEmployeeResult), 'https://mc2cs.com/scci', 'GetEmployeeResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(GetSitesResult), 'https://mc2cs.com/scci', 'GetSitesResult');

end.
