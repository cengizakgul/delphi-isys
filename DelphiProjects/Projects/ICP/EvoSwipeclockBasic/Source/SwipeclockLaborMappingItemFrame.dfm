object SwipeclockLaborMappingItemFrm: TSwipeclockLaborMappingItemFrm
  Left = 0
  Top = 0
  Width = 248
  Height = 35
  TabOrder = 0
  object Panel1: TPanel
    Left = 97
    Top = 0
    Width = 151
    Height = 35
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      151
      35)
    object cbFields: TComboBox
      Left = 0
      Top = 8
      Width = 145
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = 'Nothing'
      Items.Strings = (
        'Nothing'
        'X'
        'Y'
        'Z'
        'D Home department'
        'L Home location'
        'S Home supervisor')
    end
  end
  object pnlFieldName: TPanel
    Left = 0
    Top = 0
    Width = 97
    Height = 35
    Align = alLeft
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'xdxx'
    TabOrder = 1
  end
end
