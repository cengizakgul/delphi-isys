unit swipeclockeefilterframe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, swipeclockdecl, StdCtrls, evoapiconnection, gdycommon, 
  ExtCtrls, multipleChoiceFrame, common, swipeclockconnection;

type
  TSwipeClockEEFilterFrm = class(TFrame)
    gbEmployeeFilter: TGroupBox;
    pnlRadioButtons: TPanel;
    rbNoFiltering: TRadioButton;
    rbBranch: TRadioButton;
    rbDepartment: TRadioButton;
    ChoiceFrame: TMultipleChoiceFrm;
    Bevel1: TBevel;
    procedure rbFilterClick(Sender: TObject);
  private
    FEEFilter: TSwipeClockEEFilter;
    FBranchCodes: TStringList;
    FDepartmentCodes: TStringList;
    procedure ChoiceFrameChange(Sender: TObject);

    procedure SetEEFilter(const Value: TSwipeClockEEFilter);
    function GetEEFilter: TSwipeClockEEFilter;
    function GetFilterKind: TSwipeClockEEFilterKind;
    procedure DataUpdated;
    procedure UpdateData;
  public
    property EEFilter: TSwipeClockEEFilter read GetEEFilter write SetEEFilter;

    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    function IsValid: boolean;
    procedure SetBranchAndDepartmentCodes(ees: IXMLGetEmployeesDataSet);
    function GetEEFilterForSaving: TSwipeClockEEFilter; //w/o UpdateData
  end;

implementation

uses
  gdyRedir, evoapiconnectionutils, kbmMemTable, EvConsts, gdyGlobalWaitIndicator;

{$R *.dfm}

{ TSwipeClockEEFilterFrm }

function TSwipeClockEEFilterFrm.GetFilterKind: TSwipeClockEEFilterKind;
begin
  if rbNoFiltering.Checked then
    Result := fltNone
  else if rbBranch.Checked then
    Result := fltBranch
  else if rbDepartment.Checked then
    Result := fltDepartment
  else
  begin
    Result := fltNone; //to make compiler happy
    Assert(false);
  end
end;

function TSwipeClockEEFilterFrm.IsValid: boolean;
begin
  Result := (GetFilterKind = fltNone) or (ChoiceFrame.HasSelection);
end;

function TSwipeClockEEFilterFrm.GetEEFilter: TSwipeClockEEFilter;
begin
  UpdateData;
  Result := FEEFilter;
end;

procedure TSwipeClockEEFilterFrm.SetEEFilter(const Value: TSwipeClockEEFilter);
begin
  FEEFilter := Value;
  DataUpdated;
end;

procedure TSwipeClockEEFilterFrm.SetBranchAndDepartmentCodes(ees: IXMLGetEmployeesDataSet);
var
  i: integer;
begin
  FDepartmentCodes.Clear;
  FBranchCodes.Clear;
  if ees <> nil then
  begin
    for i := 0 to ees.Count-1 do
    begin
      if trim(ees[i].Department) <> '' then
        FDepartmentCodes.Add(AnsiUpperCase(trim(ees[i].Department)));
      if trim(ees[i].Location) <> '' then
        FBranchCodes.Add(AnsiUpperCase(trim(ees[i].Location)));
    end;
  end;  
  DataUpdated;
end;

procedure TSwipeClockEEFilterFrm.UpdateData;
begin
  FEEFilter.Kind := GetFilterKind;
  case FEEFilter.Kind of
    fltNone: ;
    fltBranch: FEEFilter.BranchCodes := ChoiceFrame.GetSelected;
    fltDepartment: FEEFilter.DepartmentCodes := ChoiceFrame.GetSelected;
  else
    assert(false);
  end;
end;

procedure TSwipeClockEEFilterFrm.DataUpdated;
//var
//  ok: boolean;
begin
//  ok := (FBranchCodes.Count > 0) or (FDepartmentCodes.Count > 0)
  case FEEFilter.Kind of
    fltNone:
      begin
        rbNoFiltering.Checked := true;
        ChoiceFrame.ClearAndDisable;
      end;
    fltBranch:
      begin
        rbBranch.Checked := true;
        ChoiceFrame.Setup(FBranchCodes, FEEFilter.BranchCodes, 'branch');
      end;
    fltDepartment:
      begin
        rbDepartment.Checked := true;
        ChoiceFrame.Setup(FDepartmentCodes, FEEFilter.DepartmentCodes, 'department');
      end
  else
    assert(false);
  end;
//  EnableControl( rbNoFiltering, ok);
//  EnableControl( rbBranch, FBranchCodes.Count > 0);
//  EnableControl( rbDepartment, FDepartmentCodes.Count > 0);
end;

procedure TSwipeClockEEFilterFrm.rbFilterClick(Sender: TObject);
begin
  FEEFilter.Kind := GetFilterKind;
  DataUpdated;
end;

procedure TSwipeClockEEFilterFrm.ChoiceFrameChange(Sender: TObject);
begin
  UpdateData;
end;

constructor TSwipeClockEEFilterFrm.Create(Owner: TComponent);
begin
  FBranchCodes := TStringList.Create;
  FBranchCodes.Sorted := true;
  FBranchCodes.Duplicates := dupIgnore;

  FDepartmentCodes := TStringList.Create;
  FDepartmentCodes.Sorted := true;
  FDepartmentCodes.Duplicates := dupIgnore;

  inherited;
  ChoiceFrame.OnChange := ChoiceFrameChange;
end;

destructor TSwipeClockEEFilterFrm.Destroy;
begin
  inherited;
  FreeAndNil(FBranchCodes);
  FreeAndNil(FDepartmentCodes);
end;

function TSwipeClockEEFilterFrm.GetEEFilterForSaving: TSwipeClockEEFilter;
begin
  Result := FEEFilter;
end;

end.

