unit swipeclock;

interface

uses
  swipeclockdecl, gdycommonlogger, timeclockimport{TPayPeriod},
  classes, evodata, common, evoapiconnectionutils;

function UpdateEmployees(const param: TSwipeClockConnectionParam; const options: TSwipeClockOptions; logger: ICommonLogger; EEData: TEvoEEData): TUpdateEmployeesStat;

function GetTimeClockData(const param: TSwipeClockConnectionParam; const options: TSwipeClockOptions; logger: ICommonLogger; period: TPayPeriod): TTimeClockImportData;

procedure GetSites(const param: TSwipeClockConnectionParam; logger: ICommonLogger; list: TStrings);

implementation

uses
  bndGetEmployees,
  sysutils, math, bndGetSiteCards,
  gdystrset, variants, gdyclasses, swipeclockconnection,
  RateImportOptionFrame;

procedure ValidateSwipeClockEECodes(swipeEEs: IXMLGetEmployeesDataSet; logger: ICommonLogger);
var
  i: integer;
  j: integer;
begin
  logger.LogEntry('Validating TimeWorks employee codes');
  try
    try
      for i := 0 to swipeEEs.Count-1 do
        if swipeEEs[i].ChildNodes.FindNode('EmployeeCode') = nil then
          logger.LogWarningFmt('Employee %s %s doesn''t have Employee Code',[swipeEEs[i].FirstName, swipeEEs[i].LastName]);

      for i := 0 to swipeEEs.Count-1 do
        for j := i+1 to swipeEEs.Count-1 do
          if (swipeEEs[i].ChildNodes.FindNode('EmployeeCode') <> nil) and
             (swipeEEs[j].ChildNodes.FindNode('EmployeeCode') <> nil) and
             (trim(swipeEEs[i].EmployeeCode) = trim(swipeEEs[j].EmployeeCode)) then
            logger.LogWarningFmt('Employees %s %s and %s %s have the same Employee Code (%s)',
             [swipeEEs[i].FirstName, swipeEEs[i].LastName, swipeEEs[j].FirstName, swipeEEs[j].LastName, swipeEEs[i].EmployeeCode]);
    except
      logger.StopException;
    end;
  finally
    logger.LogExit;
  end;
end;

function UpdateEmployees(const param: TSwipeClockConnectionParam; const options: TSwipeClockOptions; logger: ICommonLogger; EEData: TEvoEEData): TUpdateEmployeesStat;
var
  conn: TSwipeClockConnection;
  swipeEEs: IXMLGetEmployeesDataSet;
  i: integer;


  function IsInSwipeClock(code: string): boolean;
  var
    i:integer;
  begin
    Result := false;
    for i := 0 to swipeEEs.Count-1 do
      if trim(swipeEEs[i].EmployeeCode) = trim(code) then
      begin
        Result := true;
        Exit;
      end;
  end;

  function AreDifferent(swipeEE: IXMLEmployees; EEData: TEvoEEData): boolean;
    function diffIfNotEmpty(evov: Variant; sc: string): boolean;
    var
      evo: string;
    begin
      evo := trim(VarToStr(evov));
      sc := trim(sc);
      Result := (options.AllowClearing or (evo <> '')) and (evo <> sc);
    end;
  var
    rates: TRateArray;
    ssn: string;
  begin
    Assert( trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(swipeEE.EmployeeCode) );
    rates := EEData.GetEERatesArray;
    ssn := trim(StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll]));
    Result :=
          (EEData.EEs.FieldByName('LAST_NAME').AsString <> swipeEE.LastName) or
          (EEData.EEs.FieldByName('FIRST_NAME').AsString <> swipeEE.FirstName) or
          (EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString <> swipeEE.MiddleName) or
          options.ExportSSN and (ssn <> swipeEE.SSN) or  //SSN in SC has to be without dashes
          (DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime) <> swipeEE.StartDate) or
          options.ExportRates and (
            (abs(rates[0] - StrToFloat(swipeEE.PayRate0)) > eps) or
            (abs(rates[1] - StrToFloat(swipeEE.PayRate1)) > eps) or
            (abs(rates[2] - StrToFloat(swipeEE.PayRate2)) > eps) or
            (abs(rates[3] - StrToFloat(swipeEE.PayRate3)) > eps)
          ) or
          options.ExportBranch and (trim(EEData.EEs.FieldByName('CUSTOM_BRANCH_NUMBER').AsString) <> trim(swipeEE.Location)) or
          options.ExportDepartment and (trim(EEData.EEs.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString) <> trim(swipeEE.Department)) or
          diffIfNotEmpty( EEData.EEs['TIME_CLOCK_NUMBER'], swipeEE.CardNumber1 ) or
          diffIfNotEmpty( EEData.GetEEAdditionalField(SC_PASSWORD), swipeEE.Password ) or
          diffIfNotEmpty( EEData.GetEEAdditionalField(SC_LOGIN_ID), swipeEE.CardNumber2 ) or
          diffIfNotEmpty( EEData.GetEEAdditionalField(SC_SUPERVISOR), swipeEE.Supervisor );
  end;

begin
  Result := BuildEmptySyncStat;
  logger.LogEntry('Updating TimeWorks employee records');
  try
    try
      conn := TSwipeClockConnection.Create(param, logger);
      try
        swipeEEs := conn.GetEmployees;
        ValidateSwipeClockEECodes(swipeEEs, logger);
        for i := 0 to swipeEEs.Count-1 do
        begin
          logger.LogEntry('Processing TimeWorks employee record');
          try
            try
              logger.LogContextItem('First Name', swipeEEs[i].FirstName);
              logger.LogContextItem('Last Name', swipeEEs[i].LastName);
              logger.LogContextItem(sCtxEECode, swipeEEs[i].EmployeeCode);

              if EEData.LocateEE(swipeEEs[i].EmployeeCode) then
              begin
                if AreDifferent(swipeEEs[i], EEData) then
                begin
                  conn.UpdateEmployee(swipeEEs[i], EEData, options);
                  inc(Result.Modified);
                end;
              end
              else
              begin
                conn.RemoveEmployee( trim(swipeEEs[i].EmployeeCode) );
                inc(Result.Deleted);
              end;
            except
              inc(Result.Errors);
              logger.StopException;
            end;
          finally
            logger.LogExit;
          end
        end;
        EEData.EEs.First;
        while not EEData.EEs.Eof do
        begin
          logger.LogEntry('Processing Evolution employee record');
          try
            try
              logger.LogContextItem(sCtxEECode, EEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
              if not IsInSwipeClock( EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'] ) then
              begin
                conn.AddEmployee(EEData, Options);
                inc(Result.Created);
              end;
            except
              inc(Result.Errors);
              logger.StopException;
            end;
          finally
            logger.LogExit;
          end;
          EEData.EEs.Next;
        end;
      finally
        FreeAndNil(conn);
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

type
  TPayCodeTranslator = class
  public
    constructor Create(categories: string; codes: string; logger: ICommonLogger);
    function Translate(cat: string): string;
  private
    FCategories: IStr;
    FCodes: IStr;
    FLogger: ICommonLogger;
  end;

function ConvertToEvoTC(logger: ICommonLogger; x: IXMLGetSiteCards; const options: TSwipeClockOptions; blockedEEs: TStringSet): TTimeClockRecs;
var
  i: integer;
  j: integer;
  cards: IXMLCardsType;
  card: IXMLCardType;
  punch: IXMLPunchType;
  cur: PTimeClockRec;
  typ: string;
  payCodes: TPayCodeTranslator;

  procedure AddPunchTimesIfNeeded;
  begin
    if typ = 'Times' then
    begin
      if trim(punch.InDT) = 'missing' then
        raise Exception.Create('There are missed punches still on record');
      if trim(punch.OutDT) = 'missing' then
        raise Exception.Create('There are missed punches still on record');
      cur.PunchIn := StdDateTimeToDateTime(punch.InDT);
      cur.PunchOut := StdDateTimeToDateTime(punch.OutDT);
    end;
  end;

  procedure AddMappedFields;
    function GetMappedField(f: string): string;
    begin
      Assert( (Length(f) = 0) or ((Length(f) = 1) and (Pos(f, SwipeClockFieldCodes)>0) ) );
      if Length(f) = 1 then
        case f[1] of
          'X': Result := punch.X;
          'Y': Result := punch.Y;
          'Z': Result := punch.Z;
          'D': Result := card.Employee.HomeDepartment;
          'L': Result := card.Employee.HomeLocation;
          'S': Result := card.Employee.HomeSupervisor;
        else
          Assert(false);
        end;
    end;
  begin
    cur.Department := GetMappedField(options.LaborMapping.DepartmentField);
    cur.Job := GetMappedField(options.LaborMapping.JobField);
    cur.Shift := GetMappedField(options.LaborMapping.ShiftField);
    cur.Division := GetMappedField(options.LaborMapping.DivisionField);
    cur.Branch := GetMappedField(options.LaborMapping.BranchField);
    cur.RateNumber := GetMappedField(options.LaborMapping.RateCodeField);
  end;

  function FilterAllows: boolean;
  begin
    case Options.EEFilter.Kind of
      fltNone: Result := true;
      fltBranch: Result := InSet( AnsiUpperCase(trim(card.Employee.HomeLocation)), Options.EEFilter.BranchCodes);
      fltDepartment: Result := InSet( AnsiUpperCase(trim(card.Employee.HomeDepartment)),Options.EEFilter.DepartmentCodes);
    else
      Result := false; //to make compiler happy
      Assert(false);
    end;
  end;

  procedure AddRec;
  begin
    SetLength(Result, Length(Result)+1 ); //initialized with zeroes
    cur := @Result[high(Result)];
    cur.CUSTOM_EMPLOYEE_NUMBER := card.Employee.EmployeeCode;
    cur.FirstName := card.Employee.FirstName;
    cur.LastName := card.Employee.LastName;
    cur.SSN := card.Employee.SSN;
    cur.Rate := Null; // looks like Unassigned is Ok...
  end;

begin
  SetLength(Result, 0);
  cards := x.CardReport.Cards;
  logger.LogEntry('Converting TimeWorks cards to Evolution format');
  try
    try
      payCodes := TPayCodeTranslator.Create(x.CardReport.Site.PayCodeCategories, x.CardReport.Site.PayCodes, logger);
      try
        for i := 0 to cards.Count-1 do
        begin
          logger.LogEntry('Converting TimeWorks card to Evolution format');
          try
            try
              card := cards.Card[i];

              if not FilterAllows then
                continue;

              if (card.Employee.ChildNodes.FindNode('EmployeeCode') = nil) or (trim(card.Employee.EmployeeCode) = '') then
                raise Exception.Create('Employee doesn''t have Employee Code');

              logger.LogContextItem(sCtxEECode, card.Employee.EmployeeCode);

              if InSet(trim(card.Employee.EmployeeCode), blockedEEs) then
              begin
                logger.LogDebug('Employee export is blocked');
                continue;
              end;

              for j := 0 to card.Punches.Count-1 do
              begin
                punch := card.Punches[j];
                AddRec;

                cur.EDCode := payCodes.Translate(punch.Category);
                cur.PunchDate := StdDateToDate(punch.PunchDate);
                //!! ?? punch.ApplicablePayRate;
                typ := trim(punch.Type_);
                if typ = 'Times' then
                begin
                  cur.Hours := punch.NonOTHours;
                  if options.RateImport = rateUseExternal then
                    cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate; 
                  cur.Amount := Null;
                  AddPunchTimesIfNeeded;
                end
                else if typ = 'Hours' then
                begin
                  cur.Hours := punch.NonOTHours;
                  if options.RateImport = rateUseExternal then
                    cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate; 
                  cur.Amount := Null;
                end
                else if typ = 'Pay' then
                begin
                  cur.Hours := Null;
                  cur.Rate := Null;
                  cur.Amount := punch.Pay;
                end
                else
                  raise Exception.CreateFmt('Unexpected punch type: <%s>', [typ]);
                AddMappedFields;

                if InSet(typ, ['Times','Hours']) then
                begin
                  if punch.OT1Hours > eps then
                  begin
                    AddRec;
                    cur.EDCode := payCodes.Translate(punch.OT1Category);
                    cur.PunchDate := StdDateToDate(punch.PunchDate);
                    cur.Hours := punch.OT1Hours;
                    if options.RateImport = rateUseExternal then
                      cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate; // OT1PayRate
                    cur.Amount := Null;
                    AddPunchTimesIfNeeded;
                    AddMappedFields;
                  end;
                  if punch.OT2Hours > eps then
                  begin
                    AddRec;
                    cur.EDCode := payCodes.Translate(punch.OT2Category);
                    cur.PunchDate := StdDateToDate(punch.PunchDate);
                    cur.Hours := punch.OT2Hours;
                    if options.RateImport = rateUseExternal then
                      cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate; //OT2PayRate
                    cur.Amount := Null;
                    AddPunchTimesIfNeeded;
                    AddMappedFields;
                  end;
                end;
              end
            except
              logger.PassthroughException;
            end;
          finally
            logger.LogExit;
          end;
        end;
      finally
        FreeAndNil(payCodes);
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;


function GetBlockedEmployees(logger: ICommonLogger; swipeEEs: IXMLGetEmployeesDataSet): TStringSet;
var
  i: integer;
begin
  Result := nil;
  logger.LogEntry('Making list of TimeWorks employees with blocked export');
  try
    try
      for i := 0 to swipeEEs.Count-1 do
      begin
        logger.LogEntry('Inspecting TimeWorks employee record');
        try
          try
            logger.LogContextItem('First Name', swipeEEs[i].FirstName);
            logger.LogContextItem('Last Name', swipeEEs[i].LastName);
            logger.LogContextItem(sCtxEECode, swipeEEs[i].EmployeeCode);

            if (swipeEEs[i].ChildNodes.FindNode('ExportBlock') <> nil) and swipeEEs[i].ExportBlock then //the same logic as in TSwipeClockConnection.UpdateEmployee
              SetInclude(Result, trim(swipeEEs[i].EmployeeCode));
          except
            logger.StopException;
          end;
        finally
          logger.LogExit;
        end
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

function GetTimeClockData(const param: TSwipeClockConnectionParam; const options: TSwipeClockOptions; logger: ICommonLogger; period: TPayPeriod): TTimeClockImportData;
var
  recs: TTimeClockRecs;
begin
  SetLength(recs, 0);
  logger.LogEntry('Getting timeclock data');
  try
    try
      logger.LogContextItem('Employee Filter', EEFilterToStr(options.EEFilter) );
      if options.EEFilter.Kind <> fltNone then
        logger.LogEventFmt('Using employee filter: %s', [EEFilterToStr(options.EEFilter)] );
      with TSwipeClockConnection.Create(param, logger) do
      try
        recs := ConvertToEvoTC( logger, GetSiteCards(period), options, GetBlockedEmployees(logger, GetEmployees) );
        if options.Summarize then
          recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
        Result := BuildTimeClockImportData(recs, options.RateImport = rateUseEvoEE, false, logger); //false if rateUseExternal; it was this way before and everybody was happy...
      finally
        Free;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

procedure GetSites(const param: TSwipeClockConnectionParam; logger: ICommonLogger; list: TStrings);
var
  sites: IXMLGetSites;
  i: integer;
begin
  list.Clear;
  logger.LogEntry('Getting TimeWorks site list');
  try
    try
      with TSwipeClockConnection.Create(param, logger) do
      try
        sites := GetSites();
        for i := 0 to sites.Count-1 do
          list.AddObject(sites[i].SiteName, Pointer(sites[i].Site) );
      finally
        Free;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

{ TPayCodeTranslator }

constructor TPayCodeTranslator.Create(categories: string; codes: string; logger: ICommonLogger);
begin
  FLogger := logger;
  FCategories := SplitByChar(trim(categories), '|');
  FCodes := SplitByChar(trim(codes), '|');
  FLogger.LogContextItem('Pay Code Categories', categories);
  FLogger.LogContextItem('Pay Codes', codes);
end;

function TPayCodeTranslator.Translate(cat: string): string;
var
  i: integer;
begin
  FLogger.LogEntry('Translating TimeWorks pay code category into pay code');
  try
    try
      FLogger.LogContextItem('Pay code category', cat);
      cat := trim(cat);
      for i := 0 to FCategories.Count-1 do
        if trim(FCategories.Str[i]) = cat then
        begin
          Result := trim(FCodes.Str[i]);
          exit;
        end;
      raise Exception.CreateFmt('Cannot translate TimeWorks pay code category <%s>'#13#10'Use TimeWorks''s Accountant Menu - Pay Code Translation page to specify an E/D code for this pay code category', [cat]);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

end.
