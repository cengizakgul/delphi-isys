program EvoSwipeclockBasic;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  common,
  MidasLib,
  jclfileutils,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EmployeeWebServiceInterface in 'EmployeeWebServiceInterface.pas',
  swipeclock in 'swipeclock.pas',
  bndGetEmployees in 'bndGetEmployees.pas',
  SwipeClockConnectionParamFrame in 'SwipeClockConnectionParamFrame.pas' {SwipeClockConnectionParamFrm: TFrame},
  timeclockimport in '..\..\common\timeclockimport.pas',
  bndGetSites in 'bndGetSites.pas',
  bndGetSiteCards in 'bndGetSiteCards.pas',
  EvolutionSinglePayrollFrame in '..\..\common\EvolutionSinglePayrollFrame.pas' {EvolutionSinglePayrollFrm: TFrame},
  evoapiconnectionutils in '..\..\common\evoapiconnectionutils.pas',
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvoTCImportMainForm in '..\..\common\EvoTCImportMainForm.pas' {EvoTCImportMainFm: TEvoAPIClientMainFm},
  MainForm in 'MainForm.pas' {MainFm},
  swipeclockdecl in 'swipeclockdecl.pas',
  tcconnectionbase in '..\..\common\tcconnectionbase.pas',
  swipeclockoptionsframe in 'swipeclockoptionsframe.pas' {SwipeclockOptionsFrm: TFrame},
  swipeclockeefilterframe in 'swipeclockeefilterframe.pas' {SwipeClockEEFilterFrm: TFrame},
  SwipeclockConnection in 'SwipeclockConnection.pas',
  multipleChoiceFrame in '..\..\common\multipleChoiceFrame.pas' {MultipleChoiceFrm: TFrame},
  SwipeclockLaborMappingFrame in 'SwipeclockLaborMappingFrame.pas' {SwipeclockLaborMappingFrm: TFrame},
  SwipeclockLaborMappingItemFrame in 'SwipeclockLaborMappingItemFrame.pas' {SwipeclockLaborMappingItemFrm: TFrame},
  gdyDialogHolderBaseForm in '..\..\common\gdycommon\dialogs\gdyDialogHolderBaseForm.pas' {DialogHolderBase},
  gdyDialogHolderForm in '..\..\common\gdycommon\dialogs\gdyDialogHolderForm.pas' {DialogHolder},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  gdyMessageDialogFrame in '..\..\common\gdycommon\dialogs\gdyMessageDialogFrame.pas' {MessageDialog: TFrame},
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame};

{$R *.res}

begin
  LicenseKey := '7BD84037E877497282AAE249214E5889'; //Andrei's

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution TimeWorks Import';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
