inherited MainFm: TMainFm
  Left = 190
  Top = 4
  Width = 903
  Height = 784
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 887
    Height = 727
    ActivePage = TabSheet2
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Left = 7
        Top = 6
        Width = 458
        inherited GroupBox1: TGroupBox
          Width = 458
        end
      end
      inline SwipeClockFrame: TSwipeClockConnectionParamFrm
        Left = 480
        Top = 6
        Width = 314
        Height = 106
        TabOrder = 1
        inherited GroupBox1: TGroupBox
          Width = 314
          Height = 106
          Caption = 'Swipeclock'
          inherited Label4: TLabel
            Top = 23
          end
          inherited Label7: TLabel
            Top = 47
          end
          inherited Label3: TLabel
            Top = 71
          end
          inherited SiteEdit: TEdit
            Left = 83
            Top = 71
          end
          inherited PasswordEdit: TPasswordEdit
            Left = 83
            Top = 47
          end
          inherited UserNameEdit: TEdit
            Left = 83
            Top = 22
          end
          inherited cbSavePassword: TCheckBox
            Left = 210
            Top = 49
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited Splitter1: TSplitter
        Top = 273
        Width = 879
        Height = 8
      end
      inherited pnlTop: TPanel
        Width = 879
        inherited BitBtn4: TBitBtn
          Width = 201
        end
      end
      inherited EvolutionSinglePayrollFrame: TEvolutionSinglePayrollFrm
        Width = 879
        Height = 232
        inherited Splitter1: TSplitter
          Height = 232
        end
        inherited EvoCompany: TEvolutionCompanyFrm
          Height = 232
          inherited Splitter1: TSplitter
            Height = 232
          end
          inherited frmCL: TEvolutionDsFrm
            Height = 232
            inherited dgGrid: TReDBGrid
              Height = 207
            end
          end
          inherited frmCO: TEvolutionDsFrm
            Height = 232
            inherited dgGrid: TReDBGrid
              Height = 207
            end
          end
        end
        inherited frmPR: TEvolutionDsFrm
          Width = 355
          Height = 232
          inherited Panel1: TPanel
            Width = 355
          end
          inherited dgGrid: TReDBGrid
            Width = 355
            Height = 207
          end
        end
      end
      inherited pnlBottom: TPanel
        Top = 281
        Width = 879
        Height = 418
        Constraints.MinHeight = 418
        inline OptionsFrame: TSwipeclockOptionsFrm
          Left = 0
          Top = 0
          Width = 879
          Height = 377
          Align = alClient
          TabOrder = 0
          inherited Splitter1: TSplitter
            Height = 377
          end
          inherited Bevel5: TBevel
            Left = 871
            Height = 377
          end
          inherited Bevel6: TBevel
            Height = 377
          end
          inherited GroupBox1: TGroupBox
            Height = 377
          end
          inherited GroupBox2: TGroupBox
            Width = 655
            Height = 377
            inherited Bevel1: TBevel
              Height = 352
            end
            inherited Bevel2: TBevel
              Top = 367
              Width = 651
            end
            inherited Bevel3: TBevel
              Left = 645
              Height = 352
            end
            inherited Bevel4: TBevel
              Left = 384
              Height = 352
            end
            inherited EEFilterFrame: TSwipeClockEEFilterFrm
              Width = 375
              Height = 352
              inherited gbEmployeeFilter: TGroupBox
                Width = 375
                Height = 352
                inherited Bevel1: TBevel
                  Width = 371
                end
                inherited pnlRadioButtons: TPanel
                  Width = 371
                end
                inherited ChoiceFrame: TMultipleChoiceFrm
                  Width = 371
                  Height = 302
                  inherited pnlAvailable: TPanel
                    Height = 302
                    inherited lbAvailable: TListBox
                      Height = 277
                    end
                  end
                  inherited pnlButtons: TPanel
                    Height = 302
                  end
                  inherited pnlSelected: TPanel
                    Width = 146
                    Height = 302
                    inherited pnlSelectedHeader: TPanel
                      Width = 146
                    end
                    inherited lbSelected: TListBox
                      Width = 146
                      Height = 277
                    end
                  end
                end
              end
            end
            inherited Panel1: TPanel
              Left = 392
              Width = 253
              Height = 352
              inherited LaborMappingFrame: TSwipeclockLaborMappingFrm
                Width = 253
                inherited GroupBox1: TGroupBox
                  Width = 253
                  inherited RateCodeFieldFrame: TSwipeclockLaborMappingItemFrm
                    Width = 249
                    inherited Panel1: TPanel
                      Width = 152
                    end
                  end
                  inherited BranchFieldFrame: TSwipeclockLaborMappingItemFrm
                    Width = 249
                    inherited Panel1: TPanel
                      Width = 152
                    end
                  end
                  inherited DivisionFieldFrame: TSwipeclockLaborMappingItemFrm
                    Width = 249
                    inherited Panel1: TPanel
                      Width = 152
                    end
                  end
                  inherited ShiftFieldFrame: TSwipeclockLaborMappingItemFrm
                    Width = 249
                    inherited Panel1: TPanel
                      Width = 152
                    end
                  end
                  inherited JobFieldFrame: TSwipeclockLaborMappingItemFrm
                    Width = 249
                    inherited Panel1: TPanel
                      Width = 152
                    end
                  end
                  inherited departmentFieldFrame: TSwipeclockLaborMappingItemFrm
                    Width = 249
                    inherited Panel1: TPanel
                      Width = 152
                    end
                  end
                end
              end
              inherited RateImportOptionFrame: TRateImportOptionFrm
                Left = 0
                inherited RadioGroup1: TRadioGroup
                  Left = 0
                  Width = 252
                  Items.Strings = (
                    'Use TimeWorks rates'
                    'Use Evolution D/B/D/T rate override'
                    'Use Evolution employee pay rates')
                end
              end
            end
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 377
          Width = 879
          Height = 41
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            879
            41)
          object BitBtn1: TBitBtn
            Left = 8
            Top = 8
            Width = 321
            Height = 25
            Action = RunEmployeeExport
            Caption = 'Update SwipeClock Employee Records for Selected Company'
            TabOrder = 0
          end
          object BitBtn2: TBitBtn
            Left = 745
            Top = 8
            Width = 137
            Height = 25
            Action = RunAction
            Anchors = [akTop, akRight]
            Caption = 'Import Timeclock Data'
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 727
    Width = 887
  end
  inherited ActionList1: TActionList
    inherited RunAction: TAction
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited RunEmployeeExport: TAction
      Caption = 'Update SwipeClock Employee Records for Selected Company'
      OnExecute = RunEmployeeExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
  end
end
