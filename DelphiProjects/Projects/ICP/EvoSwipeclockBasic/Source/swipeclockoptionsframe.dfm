object SwipeclockOptionsFrm: TSwipeclockOptionsFrm
  Left = 0
  Top = 0
  Width = 947
  Height = 418
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 201
    Top = 0
    Width = 15
    Height = 418
  end
  object Bevel5: TBevel
    Left = 939
    Top = 0
    Width = 8
    Height = 418
    Align = alRight
    Shape = bsSpacer
  end
  object Bevel6: TBevel
    Left = 0
    Top = 0
    Width = 8
    Height = 418
    Align = alLeft
    Shape = bsSpacer
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 0
    Width = 193
    Height = 418
    Align = alLeft
    Caption = 'Employee Export'
    Constraints.MinHeight = 130
    Constraints.MinWidth = 170
    TabOrder = 0
    object Label1: TLabel
      Left = 25
      Top = 135
      Width = 141
      Height = 26
      Caption = 'Supervisors, Web Passwords and Login IDs'
      WordWrap = True
    end
    object cbExportSSN: TCheckBox
      Left = 8
      Top = 24
      Width = 97
      Height = 17
      Caption = 'Export SSN'
      TabOrder = 0
    end
    object cbExportRates: TCheckBox
      Left = 8
      Top = 48
      Width = 129
      Height = 17
      Caption = 'Export Pay Rates'
      TabOrder = 1
    end
    object cbExportDepartment: TCheckBox
      Left = 8
      Top = 96
      Width = 121
      Height = 17
      Caption = 'Export Department'
      TabOrder = 3
    end
    object cbExportBranch: TCheckBox
      Left = 8
      Top = 72
      Width = 137
      Height = 17
      Caption = 'Export Branch (location)'
      TabOrder = 2
    end
    object cbAllowClearing: TCheckBox
      Left = 8
      Top = 120
      Width = 177
      Height = 17
      Caption = 'Allow clearing of Card Numbers, '
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 216
    Top = 0
    Width = 723
    Height = 418
    Align = alClient
    Caption = 'Timeclock Import'
    TabOrder = 1
    object Bevel1: TBevel
      Left = 2
      Top = 15
      Width = 7
      Height = 393
      Align = alLeft
      Shape = bsSpacer
    end
    object Bevel2: TBevel
      Left = 2
      Top = 408
      Width = 719
      Height = 8
      Align = alBottom
      Shape = bsSpacer
    end
    object Bevel3: TBevel
      Left = 713
      Top = 15
      Width = 8
      Height = 393
      Align = alRight
      Shape = bsSpacer
    end
    object Bevel4: TBevel
      Left = 456
      Top = 15
      Width = 8
      Height = 393
      Align = alRight
      Shape = bsSpacer
    end
    inline EEFilterFrame: TSwipeClockEEFilterFrm
      Left = 9
      Top = 15
      Width = 447
      Height = 393
      Align = alClient
      TabOrder = 0
      inherited gbEmployeeFilter: TGroupBox
        Width = 447
        Height = 393
        inherited Bevel1: TBevel
          Width = 443
        end
        inherited pnlRadioButtons: TPanel
          Width = 443
        end
        inherited ChoiceFrame: TMultipleChoiceFrm
          Width = 443
          Height = 343
          inherited pnlAvailable: TPanel
            Height = 343
            inherited lbAvailable: TListBox
              Height = 318
            end
          end
          inherited pnlButtons: TPanel
            Height = 343
          end
          inherited pnlSelected: TPanel
            Width = 218
            Height = 343
            inherited pnlSelectedHeader: TPanel
              Width = 218
            end
            inherited lbSelected: TListBox
              Width = 218
              Height = 318
            end
          end
        end
      end
    end
    object Panel1: TPanel
      Left = 464
      Top = 15
      Width = 249
      Height = 393
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object cbSummarize: TCheckBox
        Left = 2
        Top = 323
        Width = 81
        Height = 17
        Caption = 'Summarize'
        TabOrder = 1
      end
      inline LaborMappingFrame: TSwipeclockLaborMappingFrm
        Left = 0
        Top = 0
        Width = 249
        Height = 230
        Align = alTop
        Constraints.MaxHeight = 230
        Constraints.MinWidth = 200
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 249
          inherited RateCodeFieldFrame: TSwipeclockLaborMappingItemFrm
            Width = 245
            inherited Panel1: TPanel
              Width = 148
              inherited cbFields: TComboBox
                Width = 142
              end
            end
          end
          inherited BranchFieldFrame: TSwipeclockLaborMappingItemFrm
            Width = 245
            inherited Panel1: TPanel
              Width = 148
              inherited cbFields: TComboBox
                Width = 142
              end
            end
          end
          inherited DivisionFieldFrame: TSwipeclockLaborMappingItemFrm
            Width = 245
            inherited Panel1: TPanel
              Width = 148
              inherited cbFields: TComboBox
                Width = 142
              end
            end
          end
          inherited ShiftFieldFrame: TSwipeclockLaborMappingItemFrm
            Width = 245
            inherited Panel1: TPanel
              Width = 148
              inherited cbFields: TComboBox
                Width = 142
              end
            end
          end
          inherited JobFieldFrame: TSwipeclockLaborMappingItemFrm
            Width = 245
            inherited Panel1: TPanel
              Width = 148
              inherited cbFields: TComboBox
                Width = 142
              end
            end
          end
          inherited departmentFieldFrame: TSwipeclockLaborMappingItemFrm
            Width = 245
            inherited Panel1: TPanel
              Width = 148
              inherited cbFields: TComboBox
                Width = 142
              end
            end
          end
        end
      end
      inline RateImportOptionFrame: TRateImportOptionFrm
        Left = 3
        Top = 236
        Width = 255
        Height = 79
        TabOrder = 2
        inherited RadioGroup1: TRadioGroup
          Left = -1
          Width = 247
        end
      end
    end
  end
end
