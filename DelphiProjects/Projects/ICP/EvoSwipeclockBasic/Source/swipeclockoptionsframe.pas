unit swipeclockoptionsframe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, swipeclockdecl, ExtCtrls, swipeclockeefilterframe,
  SwipeclockLaborMappingFrame, RateImportOptionFrame;

type
  TSwipeclockOptionsFrm = class(TFrame)
    GroupBox1: TGroupBox;
    cbExportSSN: TCheckBox;
    cbExportRates: TCheckBox;
    cbExportDepartment: TCheckBox;
    cbExportBranch: TCheckBox;
    GroupBox2: TGroupBox;
    Splitter1: TSplitter;
    EEFilterFrame: TSwipeClockEEFilterFrm;
    Panel1: TPanel;
    cbSummarize: TCheckBox;
    Bevel1: TBevel;
    Bevel2: TBevel;
    LaborMappingFrame: TSwipeclockLaborMappingFrm;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    cbAllowClearing: TCheckBox;
    Label1: TLabel;
    RateImportOptionFrame: TRateImportOptionFrm;
  private
    procedure SetOptions(const Value: TSwipeClockOptions);
    function GetOptions: TSwipeClockOptions;
    function GetOptionsHelper: TSwipeClockOptions;
  public
    property Options: TSwipeClockOptions read GetOptions write SetOptions;
    function IsValid: boolean;
    function GetOptionsForSaving: TSwipeClockOptions;
  end;

implementation

{$R *.dfm}

{ TSwipeclockOptionsFrm }

function TSwipeclockOptionsFrm.GetOptions: TSwipeClockOptions;
begin
  Result := GetOptionsHelper;
  Result.EEFilter := EEFilterFrame.EEFilter;
end;

function TSwipeclockOptionsFrm.GetOptionsForSaving: TSwipeClockOptions;
begin
  Result := GetOptionsHelper;
  Result.EEFilter := EEFilterFrame.GetEEFilterForSaving;
end;

function TSwipeclockOptionsFrm.GetOptionsHelper: TSwipeClockOptions;
begin
  Result.ExportSSN := cbExportSSN.Checked;
  Result.ExportRates := cbExportRates.Checked;
  Result.ExportBranch := cbExportBranch.Checked;
  Result.ExportDepartment := cbExportDepartment.Checked;
  Result.AllowClearing := cbAllowClearing.Checked;
  Result.Summarize := cbSummarize.Checked;
  Result.RateImport := RateImportOptionFrame.Value;
  Result.LaborMapping := LaborMappingFrame.LaborMapping;
end;

function TSwipeclockOptionsFrm.IsValid: boolean;
begin
  Result := EEFilterFrame.IsValid;
end;

procedure TSwipeclockOptionsFrm.SetOptions(const Value: TSwipeClockOptions);
begin
  cbExportSSN.Checked := Value.ExportSSN;
  cbExportRates.Checked := Value.ExportRates;
  cbExportBranch.Checked := Value.ExportBranch;
  cbExportDepartment.Checked := Value.ExportDepartment;
  cbAllowClearing.Checked := Value.AllowClearing;
  EEFilterFrame.EEFilter := Value.EEFilter;
  cbSummarize.Checked := Value.Summarize;
  RateImportOptionFrame.Value := Value.RateImport;
  LaborMappingFrame.LaborMapping := Value.LaborMapping;
end;

end.
