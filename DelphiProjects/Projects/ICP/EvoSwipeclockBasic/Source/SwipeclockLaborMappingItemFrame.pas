unit SwipeclockLaborMappingItemFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, swipeclockdecl;

type
  TSwipeclockLaborMappingItemFrm = class(TFrame)
    Panel1: TPanel;
    pnlFieldName: TPanel;
    cbFields: TComboBox;
  private
    procedure SetSwField(s: string);
    function GetSwField: string;
  public
    property SwField: string read GetSwField write SetSwField;
    procedure SetFieldDefinitions(x,y,z: string);
  end;

implementation

{$R *.dfm}

{ TSwipeclockLaborMappingItemFrm }

function TSwipeclockLaborMappingItemFrm.GetSwField: string;
begin
  Assert(cbFields.ItemIndex>=0);
  Result := trim((' '+SwipeClockFieldCodes)[cbFields.ItemIndex+1]);
end;

procedure TSwipeclockLaborMappingItemFrm.SetFieldDefinitions(x, y, z: string);
var
  idx: integer;
begin
  idx := cbFields.ItemIndex;
  cbFields.Items[1] := 'X '+trim(x);
  cbFields.Items[2] := 'Y '+trim(y);
  cbFields.Items[3] := 'Z '+trim(z);
  cbFields.ItemIndex := idx;
end;

procedure TSwipeclockLaborMappingItemFrm.SetSwField(s: string);
begin
  s := trim(s);
  if Length(s) = 1 then
    cbFields.ItemIndex := Pos(copy(s,1,1), SwipeClockFieldCodes)
  else
    cbFields.ItemIndex := 0;
end;

end.
