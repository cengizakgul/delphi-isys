unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoTCImportMainForm, ActnList, ComCtrls, gdyCommonLoggerView,
  Buttons, StdCtrls, ExtCtrls, EvoAPIConnectionParamFrame,
  EvolutionCompanyFrame, SwipeClockConnectionParamFrame,
  evoapiconnection, evoapiconnectionutils, timeclockimport,
  EvolutionSinglePayrollFrame, swipeclockoptionsframe, common,
  OptionsBaseFrame;

type
  TMainFm = class(TEvoTCImportMainFm)
    SwipeClockFrame: TSwipeClockConnectionParamFrm;
    OptionsFrame: TSwipeclockOptionsFrm;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunEmployeeExportExecute(Sender: TObject);
    procedure RunEmployeeExportUpdate(Sender: TObject);
  private
    FSiteName: string;
    FNeverWarnOnAllowClearing: boolean;
    function GetTimeClockData(period: TPayPeriod;company: TEvoCompanyDef): TTimeClockImportData;
  	function UpdateEmployees: TUpdateEmployeesStat;
  protected
  	function CanConnectToTCApp: boolean; override;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

uses
  swipeclock, kbmMemTable, gdyRedir, XmlRpcTypes,
  gdycommon, waitform, isSettings, swipeclockdecl,
  isbaseclasses, gdystrset, gdyclasses, SwipeclockConnection, gdyDialogEngine,
  gdyMessageDialogFrame, gdyGlobalWaitIndicator, userActionHelpers, evodata,
  RateImportOptionFrame, gdyCrypt;

const
  cKey='Form.Button';

{$R *.dfm}

function LoadOptions(conf: IisSettings; root: string): TSwipeClockOptions;
var
  i: integer;
begin
  root := root + '\';
  Result.ExportSSN := conf.AsBoolean[root+'ExportSSN'];
  Result.ExportRates := conf.AsBoolean[root+'ExportRates'];
  Result.ExportBranch := conf.AsBoolean[root+'ExportBranch'];
  Result.ExportDepartment := conf.AsBoolean[root+'ExportDepartment'];
  Result.AllowClearing := conf.AsBoolean[root+'AllowClearing'];

  Result.EEFilter.Kind := TSwipeClockEEFilterKind(conf.AsInteger[root+'EEFilter\Kind']);
  if (Result.EEFilter.Kind < low(TSwipeClockEEFilterKind)) or (Result.EEFilter.Kind > high(TSwipeClockEEFilterKind)) then
    Result.EEFilter.Kind := fltNone;
  with CsvAsStr(conf.AsString[root+'EEFilter\BranchCodes']) do
    for i := 0 to Count-1 do
      SetInclude(Result.EEFilter.BranchCodes, Str[i]);
  with CsvAsStr(conf.AsString[root+'EEFilter\DepartmentCodes']) do
    for i := 0 to Count-1 do
      SetInclude(Result.EEFilter.DepartmentCodes, Str[i]);

  Result.Summarize := conf.AsBoolean[root+'Summarize'];

  Result.RateImport := TRateImportOption(conf.AsInteger[root+'RateImport']);
  if (Result.RateImport < low(TRateImportOption)) or (Result.RateImport > high(TRateImportOption)) then
    Result.RateImport := rateUseExternal;

  //!!copy-paste
  Result.LaborMapping.DepartmentField := Copy(conf.AsString[root+'LaborMapping\DepartmentField'], 1, 1);
  if Pos(Result.LaborMapping.DepartmentField, SwipeClockFieldCodes) = 0 then
    Result.LaborMapping.DepartmentField := '';

  Result.LaborMapping.JobField := Copy(conf.AsString[root+'LaborMapping\JobField'], 1, 1);
  if Pos(Result.LaborMapping.JobField, SwipeClockFieldCodes) = 0 then
    Result.LaborMapping.JobField := '';

  Result.LaborMapping.ShiftField := Copy(conf.AsString[root+'LaborMapping\ShiftField'], 1, 1);
  if Pos(Result.LaborMapping.ShiftField, SwipeClockFieldCodes) = 0 then
    Result.LaborMapping.ShiftField := '';

  Result.LaborMapping.DivisionField := Copy(conf.AsString[root+'LaborMapping\DivisionField'], 1, 1);
  if Pos(Result.LaborMapping.DivisionField, SwipeClockFieldCodes) = 0 then
    Result.LaborMapping.DivisionField := '';

  Result.LaborMapping.BranchField := Copy(conf.AsString[root+'LaborMapping\BranchField'], 1, 1);
  if Pos(Result.LaborMapping.BranchField, SwipeClockFieldCodes) = 0 then
    Result.LaborMapping.BranchField := '';

  Result.LaborMapping.RateCodeField := Copy(conf.AsString[root+'LaborMapping\RateCodeField'], 1, 1);
  if Pos(Result.LaborMapping.RateCodeField, SwipeClockFieldCodes) = 0 then
    Result.LaborMapping.RateCodeField := '';
end;

procedure SaveOptions( const options: TSwipeClockOptions; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsBoolean[root+'ExportSSN'] := options.ExportSSN;
  conf.AsBoolean[root+'ExportRates'] := options.ExportRates;
  conf.AsBoolean[root+'ExportBranch'] := options.ExportBranch;
  conf.AsBoolean[root+'ExportDepartment'] := options.ExportDepartment;
  conf.AsBoolean[root+'AllowClearing'] := options.AllowClearing;
  conf.AsInteger[root+'EEFilter\Kind'] := ord(options.EEFilter.Kind);
  conf.AsString[root+'EEFilter\BranchCodes'] := StringSetToCsv(options.EEFilter.BranchCodes);
  conf.AsString[root+'EEFilter\DepartmentCodes'] := StringSetToCsv(options.EEFilter.DepartmentCodes);
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsInteger[root+'RateImport'] := ord(options.RateImport);

  conf.AsString[root+'LaborMapping\DepartmentField'] := options.LaborMapping.DepartmentField;
  conf.AsString[root+'LaborMapping\JobField'] := options.LaborMapping.JobField;
  conf.AsString[root+'LaborMapping\ShiftField'] := options.LaborMapping.ShiftField;
  conf.AsString[root+'LaborMapping\DivisionField'] := options.LaborMapping.DivisionField;
  conf.AsString[root+'LaborMapping\BranchField'] := options.LaborMapping.BranchField;
  conf.AsString[root+'LaborMapping\RateCodeField'] := options.LaborMapping.RateCodeField;
end;

function LoadConnectionSettings(conf: IisSettings; root: string): TSwipeClockConnectionParam;
begin
  root := root + '\';
  Result.UserName := conf.AsString[root+'UserName'];
  if conf.GetValueNames(root).IndexOf('Password') <> -1 then
    Result.Password := conf.AsString[root+'Password']
  else
    Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  Result.Site := conf.AsString[root+'Site'];
end;

procedure SaveConnectionSettings(const param: TSwipeClockConnectionParam; conf: IisSettings; root: string);
begin
  root := root + '\';
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;

  conf.AsString[root+'Site'] := param.Site;
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
	TCAppName := 'TimeWorks';
  try
    SwipeClockFrame.Param := LoadConnectionSettings( FSettings, 'SwipeClockConnection');
  except
    Logger.StopException;
  end;
  try
    OptionsFrame.Options := LoadOptions( FSettings, 'SwipeClockOptions');
  except
    Logger.StopException;
  end;

  try
    FNeverWarnOnAllowClearing := FSettings.AsBoolean['GUIOptions\NeverWarnOnAllowClearing'];
  except
    Logger.StopException;
  end;
end;

destructor TMainFm.Destroy;
begin
  try
    SaveConnectionSettings( SwipeClockFrame.Param, FSettings, 'SwipeClockConnection');
  except
    Logger.StopException;
  end;
  try
    SaveOptions( OptionsFrame.GetOptionsForSaving, FSettings, 'SwipeClockOptions');
  except
    Logger.StopException;
  end;
  try
    FSettings.AsBoolean['GUIOptions\NeverWarnOnAllowClearing'] := FNeverWarnOnAllowClearing;
  except
    Logger.StopException;
  end;
  inherited;
end;

function TMainFm.CanConnectToTCApp: boolean;
begin
	Result := SwipeClockFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
var
  conn: TSwipeClockConnection;
  dummy: TPayPeriod;
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      inherited;
      logger.LogEntry('Getting data from TimeWorks');
      try
        try
          WaitIndicator.StartWait('Getting data from TimeWorks');
          try
            conn := TSwipeClockConnection.Create(SwipeClockFrame.Param, logger);
            try
              dummy.PeriodBegin := Now;
              dummy.PeriodEnd := Now;
              with conn.GetSiteCards(dummy).CardReport.Site do
              begin
                FSiteName := SiteName;
                OptionsFrame.LaborMappingFrame.SetFieldDefinitions( Xdefinition, Ydefinition, Zdefinition);
              end;
              OptionsFrame.EEFilterFrame.SetBranchAndDepartmentCodes( conn.GetEmployees );
            finally
              FreeAndNil(conn);
            end;
          finally
            WaitIndicator.EndWait;
          end;
        except
          logger.PassthroughException;
        end;
      finally
        logger.LogExit;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result := swipeclock.GetTimeClockData( SwipeClockFrame.Param, OptionsFrame.Options, Logger, period );
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
var
  company: TEvoCompanyDef;
  msg: string;
  batch: TEvoPayrollBatchDef;
  conn: IEvoAPIConnection;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      company := EvolutionSinglePayrollFrame.GetPayrollDef.Company;
      msg := Format('You are about to import timeclock information from "%s" company into "%s" (%s) company. Do you want to proceed?',
                      [trim(FSiteName), trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER)] );

      if AnsiSameText( trim(FSiteName), trim(company.CUSTOM_COMPANY_NUMBER) ) or
         AnsiSameText( trim(FSiteName), trim(company.Name) ) or
         (MessageDlg( msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
      begin
        conn := CreateEvoAPIConnection(EvoFrame.Param, logger);
        batch := GetBatchInfo(conn, logger, EvolutionSinglePayrollFrame.GetPayrollDef);
        RunTimeClockImport(Logger, batch, GetTimeClockData, conn, false);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and CanConnectToTCApp and
     EvolutionSinglePayrollFrame.CanGetPayrollDef and OptionsFrame.IsValid;
end;

type
  TAskResult = record
    Ok: boolean;
    NeverAskAgain: boolean;
  end;

function Ask(Owner: TComponent; msg: string): TAskResult;
var
  dlg: TMessageDialog;
begin
  dlg := TMessageDialog.Create(Owner);
  try
    dlg.MessageText := msg;
    dlg.Caption := Application.Title;
    with DialogEngine( dlg, Owner ) do
    begin
      NeverShowAgainOption := true;
      Result.Ok := ShowModal = mrOk;
      Result.NeverAskAgain := NeverShowAgain;
    end
  finally
    FreeAndNil(dlg);
  end;
end;

function TMainFm.UpdateEmployees: TUpdateEmployeesStat;
var
  EEData: TEvoEEData;
  conn: IEvoAPIConnection;
begin
  conn := CreateEvoAPIConnection(EvoFrame.Param, logger);
  EEData := TEvoEEData.Create(conn, Logger, EvolutionSinglePayrollFrame.EvoCompany.GetClCo);
  try
  	Result := swipeclock.UpdateEmployees( SwipeClockFrame.Param, OptionsFrame.Options, Logger, EEData );
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.RunEmployeeExportExecute(Sender: TObject);
var
  company: TEvoCompanyDef;
  r: TAskResult;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      company := EvolutionSinglePayrollFrame.EvoCompany.GetClCo;
      if not AnsiSameText( trim(FSiteName), trim(company.CUSTOM_COMPANY_NUMBER) ) and
         not AnsiSameText( trim(FSiteName), trim(company.Name) ) then
        if MessageDlg( Format('You are about to export employee information from %s (%s) company into %s company. Do you want to proceed?',
          [trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER), trim(FSiteName)]), mtConfirmation, [mbYes, mbNo], 0) = mrNo then
          Abort;

      if OptionsFrame.Options.AllowClearing and not FNeverWarnOnAllowClearing then
      begin
        r := Ask(Self, 'Clearing of Card Numbers, Web Passwords and Supervisors is allowed. Proceed?');
        if not r.Ok then
          Abort;
        FNeverWarnOnAllowClearing := r.NeverAskAgain;
      end;

      userActionHelpers.RunEmployeeExport(Logger, UpdateEmployees, EvolutionSinglePayrollFrame.EvoCompany.GetClCo, TCAppName);

      WaitIndicator.StartWait('Refreshing SwipeClock location and department lists');
      try
        with TSwipeClockConnection.Create(SwipeClockFrame.Param, logger) do
        try
          OptionsFrame.EEFilterFrame.SetBranchAndDepartmentCodes( This.GetEmployees );
        finally
          Free;
        end;
      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunEmployeeExportUpdate(Sender: TObject);
begin
//only employee filter can be invalid and it is not needed for EE update
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and CanConnectToTCApp and
                  EvolutionSinglePayrollFrame.EvoCompany.CanGetClCo;
end;

end.
