object SwipeclockLaborMappingFrm: TSwipeclockLaborMappingFrm
  Left = 0
  Top = 0
  Width = 265
  Height = 230
  Constraints.MaxHeight = 230
  Constraints.MinWidth = 200
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 265
    Height = 230
    Align = alClient
    Caption = 'Labor Mapping'
    TabOrder = 0
    inline RateCodeFieldFrame: TSwipeclockLaborMappingItemFrm
      Left = 2
      Top = 190
      Width = 261
      Height = 35
      Align = alTop
      TabOrder = 5
      inherited Panel1: TPanel
        Width = 164
        inherited cbFields: TComboBox
          Width = 158
        end
      end
      inherited pnlFieldName: TPanel
        Caption = '  Rate Code Field: '
      end
    end
    inline BranchFieldFrame: TSwipeclockLaborMappingItemFrm
      Left = 2
      Top = 50
      Width = 261
      Height = 35
      Align = alTop
      TabOrder = 1
      inherited Panel1: TPanel
        Width = 164
        inherited cbFields: TComboBox
          Width = 158
        end
      end
      inherited pnlFieldName: TPanel
        Caption = '  Branch Field: '
      end
    end
    inline DivisionFieldFrame: TSwipeclockLaborMappingItemFrm
      Left = 2
      Top = 15
      Width = 261
      Height = 35
      Align = alTop
      TabOrder = 0
      inherited Panel1: TPanel
        Width = 164
        inherited cbFields: TComboBox
          Width = 158
        end
      end
      inherited pnlFieldName: TPanel
        Caption = '  Division Field: '
      end
    end
    inline ShiftFieldFrame: TSwipeclockLaborMappingItemFrm
      Left = 2
      Top = 155
      Width = 261
      Height = 35
      Align = alTop
      TabOrder = 4
      inherited Panel1: TPanel
        Width = 164
        inherited cbFields: TComboBox
          Width = 158
        end
      end
      inherited pnlFieldName: TPanel
        Caption = '  Shift Field: '
      end
    end
    inline JobFieldFrame: TSwipeclockLaborMappingItemFrm
      Left = 2
      Top = 120
      Width = 261
      Height = 35
      Align = alTop
      TabOrder = 3
      inherited Panel1: TPanel
        Width = 164
        inherited cbFields: TComboBox
          Width = 158
        end
      end
      inherited pnlFieldName: TPanel
        Caption = '  Job Field: '
      end
    end
    inline departmentFieldFrame: TSwipeclockLaborMappingItemFrm
      Left = 2
      Top = 85
      Width = 261
      Height = 35
      Align = alTop
      TabOrder = 2
      inherited Panel1: TPanel
        Width = 164
        inherited cbFields: TComboBox
          Width = 158
        end
      end
      inherited pnlFieldName: TPanel
        Caption = '  Department Field: '
      end
    end
  end
end
