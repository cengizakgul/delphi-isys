object SwipeClockEEFilterFrm: TSwipeClockEEFilterFrm
  Left = 0
  Top = 0
  Width = 467
  Height = 281
  TabOrder = 0
  object gbEmployeeFilter: TGroupBox
    Left = 0
    Top = 0
    Width = 467
    Height = 281
    Align = alClient
    Caption = 'Employee Filter'
    TabOrder = 0
    object Bevel1: TBevel
      Left = 2
      Top = 41
      Width = 463
      Height = 7
      Align = alTop
      Shape = bsTopLine
    end
    object pnlRadioButtons: TPanel
      Left = 2
      Top = 15
      Width = 463
      Height = 26
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object rbNoFiltering: TRadioButton
        Left = 8
        Top = 3
        Width = 81
        Height = 17
        Caption = 'No filtering'
        TabOrder = 0
        OnClick = rbFilterClick
      end
      object rbBranch: TRadioButton
        Left = 96
        Top = 3
        Width = 65
        Height = 17
        Caption = 'Branch'
        TabOrder = 1
        OnClick = rbFilterClick
      end
      object rbDepartment: TRadioButton
        Left = 176
        Top = 3
        Width = 81
        Height = 17
        Caption = 'Department'
        TabOrder = 2
        OnClick = rbFilterClick
      end
    end
    inline ChoiceFrame: TMultipleChoiceFrm
      Left = 2
      Top = 48
      Width = 463
      Height = 231
      Align = alClient
      TabOrder = 1
      inherited pnlAvailable: TPanel
        Height = 231
        inherited lbAvailable: TListBox
          Height = 206
        end
      end
      inherited pnlButtons: TPanel
        Height = 231
      end
      inherited pnlSelected: TPanel
        Width = 238
        Height = 231
        inherited pnlSelectedHeader: TPanel
          Width = 238
        end
        inherited lbSelected: TListBox
          Width = 238
          Height = 206
        end
      end
    end
  end
end
