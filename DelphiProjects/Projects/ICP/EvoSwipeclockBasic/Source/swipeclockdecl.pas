unit swipeclockdecl;

interface

uses
  gdystrset, RateImportOptionFrame;

type
  TSwipeClockConnectionParam = record
    UserName: string;
    Password: string;
    Site: string;
    SavePassword: boolean;
  end;

  TSwipeClockEEFilterKind = (fltNone, fltBranch, fltDepartment);

  TSwipeClockEEFilter = record
    Kind: TSwipeClockEEFilterKind;
    BranchCodes: TStringSet;
    DepartmentCodes: TStringSet;
  end;

  TSwipeClockLaborMapping = record
    DepartmentField: string; //XYZDLS
    JobField: string;
    ShiftField: string;
    DivisionField: string;
    BranchField: string;
    RateCodeField: string;
  end;

  TSwipeClockOptions = record
    ExportSSN: boolean;
    ExportRates: boolean;
    ExportBranch: boolean;
    ExportDepartment: boolean;
    AllowClearing: boolean;
    EEFilter: TSwipeClockEEFilter;
    Summarize: boolean;
    RateImport: TRateImportOption;
    LaborMapping: TSwipeClockLaborMapping;
  end;

const
  SwipeClockFieldCodes: string = 'XYZDLS';
  SC_PASSWORD: string = 'SC_PASSWORD';
  SC_LOGIN_ID: string = 'SC_LOGIN_ID';
  SC_SUPERVISOR: string = 'SC_SUPERVISOR';

function EEFilterToStr(EEFilter: TSwipeClockEEFilter): string;

implementation

uses
  sysutils;
  
function EEFilterToStr(EEFilter: TSwipeClockEEFilter): string;
begin
  case EEFilter.Kind of
    fltNone: Result := 'No';
    fltBranch: Result := Format('Employee Branches: %s',[SetToStr(EEFilter.BranchCodes,', ')]);
    fltDepartment: Result := Format('Employee Departments: %s',[SetToStr(EEFilter.DepartmentCodes,', ')]);
  else
    Assert(false);
    Result := 'Invalid';
  end;
end;

end.
