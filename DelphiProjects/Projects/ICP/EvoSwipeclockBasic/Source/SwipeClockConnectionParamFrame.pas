unit SwipeClockConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, swipeclockdecl, PasswordEdit;

type
  TSwipeClockConnectionParamFrm = class(TFrame)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    SiteEdit: TEdit;
    PasswordEdit: TPasswordEdit;
    UserNameEdit: TEdit;
    cbSavePassword: TCheckBox;
  private
    function GetParam: TSwipeClockConnectionParam;
    procedure SetParam(const Value: TSwipeClockConnectionParam);
  public
    property Param: TSwipeClockConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
  end;

implementation

{$R *.dfm}

{ TSwipeClockConnectionParamFrm }

function TSwipeClockConnectionParamFrm.GetParam: TSwipeClockConnectionParam;
begin
  Result.Username := UserNameEdit.Text;
  Result.Password := PasswordEdit.Password;
  Result.Site := SiteEdit.Text;
  Result.SavePassword := cbSavePassword.Checked;
end;

function TSwipeClockConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(UserNameEdit.Text) <> '') and
            (trim(PasswordEdit.Password) <> '') and
            (trim(SiteEdit.Text) <> '');
end;

procedure TSwipeClockConnectionParamFrm.SetParam(const Value: TSwipeClockConnectionParam);
begin
  UserNameEdit.Text := Value.Username;
  PasswordEdit.Password := Value.Password;
  SiteEdit.Text := Value.Site;
  cbSavePassword.Checked := Value.SavePassword;
end;

end.
