unit SwipeclockConnection;

interface

uses
  EmployeeWebServiceInterface, gdycommonlogger, bndGetEmployees,
  bndGetSiteCards, bndGetSites, tcconnectionbase, swipeclockdecl,
  timeclockimport, evodata, common, xmlintf;

type
  IXMLGetSites = bndGetSites.IXMLNewDataSet;
  IXMLGetEmployeesDataSet = bndGetEmployees.IXMLNewDataSet;
  IXMLGetSiteCards = bndGetSiteCards.IXMLResponseType;

  TSwipeClockConnection = class(TTCConnectionBase)
  public
    constructor Create(const param: TSwipeClockConnectionParam; logger: ICommonLogger);

    function GetSiteCards(period: TPayPeriod): IXMLGetSiteCards;
    function GetSites: IXMLGetSites;

    function GetEmployees: IXMLGetEmployeesDataSet;
    procedure RemoveEmployee(code: widestring);
    procedure AddEmployee(EEData: TEvoEEData; const options: TSwipeClockOptions);
    procedure UpdateEmployee(swipeEE: IXMLEmployees; EEData: TEvoEEData; const options: TSwipeClockOptions);
    function This: TSwipeClockConnection;
  protected
    function CheckXML(doc: IXMLDocument): IXMLDocument; override;
    function ToXMLDoc(ws: widestring; name: string): IXMLDocument; override;
  private
    FParam: TSwipeClockConnectionParam;

    FAuthHeader: AuthHeader;
    FService: EmployeeWebServiceInterfaceSoap;

    procedure BeginConnection;
    procedure EndConnection;
    function GetAndCheckWebPassword(EEData: TEvoEEData): string;
  end;

function DateToSwipeClockDate(dt: TDateTime): string;

implementation


uses
  sysutils, gdyGlobalWaitIndicator, variants, InvokeRegistry, gdycommon, XSBuiltIns;

const
  sCtxComponentSwipeclock = 'TimeWorks connection';

function DateToSwipeClockDate(dt: TDateTime): string;
begin
  Result := FormatDateTime('yyyy/mm/dd', dt);
end;


{ TSwipeClockConnection }

function TSwipeClockConnection.CheckXML(doc: IXMLDocument): IXMLDocument;
begin
  if doc.DocumentElement.NodeName = 'error' then
    raise Exception.CreateFmt('TimeWorks error (%d): %s',
      [Integer(doc.DocumentElement.ChildNodes['number'].NodeValue),
       String(doc.DocumentElement.ChildNodes['description'].NodeValue)
      ]);
  Result := doc;
end;

constructor TSwipeClockConnection.Create(
  const param: TSwipeClockConnectionParam; logger: ICommonLogger);
begin
  inherited Create(logger);
  FParam := param;
end;

function TSwipeClockConnection.GetEmployees: IXMLGetEmployeesDataSet;
begin
  FLogger.LogEntry('TimeWorks: Get Employees');
  try
    try
      BeginConnection;
      try
        Result := bndGetEmployees.GetNewDataSet( ToXMLDoc(FService.GetEmployees, 'GetEmployees') );
        Result.OwnerDocument.Options := Result.OwnerDocument.Options + [doNodeAutoCreate]; //some nodes may be missing
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.BeginConnection;
begin
  FLogger.LogContextItem(sCtxComponent, sCtxComponentSwipeclock);

  FService := GetEmployeeWebServiceInterfaceSoap( true, 'http://www.swipeclock.com/scci/xml/EmployeeWebServiceInterface.asmx?WSDL');
  SetHTTPTimeouts(FService, 30*60*1000);
  AttachLogger(FService);

  FAuthHeader := AuthHeader.Create;
  FAuthHeader.userName := FParam.UserName;
  FAuthHeader.password := FParam.Password;
  FAuthHeader.site := FParam.Site;
  (FService as ISOAPHeaders).send(FAuthHeader);
end;

procedure TSwipeClockConnection.EndConnection;
begin
  try
    FService := nil;
  finally
    FreeAndNil(FAuthHeader);
  end;
end;

function TSwipeClockConnection.GetSiteCards(period: TPayPeriod): IXMLGetSiteCards;
begin
  FLogger.LogEntry('TimeWorks: Get Site Cards');
  try
    try
      BeginConnection;
      try
        Result := bndGetSiteCards.Getresponse( ToXMLDoc(
            FService.GetSiteCards(DateToSwipeClockDate(period.PeriodBegin), DateToSwipeClockDate(period.PeriodEnd)),
            'GetSiteCards'
            ) );
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.RemoveEmployee(code: widestring);
begin
  FLogger.LogEntry('TimeWorks: Remove Employee');
  try
    try
      FLogger.LogContextItem(sCtxEECode, code);
      BeginConnection;
      try
        FService.RemoveEmployee(code, DateToSwipeClockDate(now));
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

type
  TPayRates = class
  public
    RateDef: TXSDecimal;
    Rate1: TXSDecimal;
    Rate2: TXSDecimal;
    Rate3: TXSDecimal;

    constructor Create(rates: TRateArray); overload;
    constructor Create(r0, r1, r2, r3: string); overload;
    destructor Destroy; override;
  end;

constructor TPayRates.Create(rates: TRateArray);
begin
  RateDef := TXSDecimal.Create();
  Rate1 := TXSDecimal.Create();
  Rate2 := TXSDecimal.Create();
  Rate3 := TXSDecimal.Create();
  RateDef.DecimalString := FloatToStr( rates[0] );
  Rate1.DecimalString := FloatToStr( rates[1] );
  Rate2.DecimalString := FloatToStr( rates[2] );
  Rate3.DecimalString := FloatToStr( rates[3] );
end;

constructor TPayRates.Create(r0, r1, r2, r3: string);
begin
  RateDef := TXSDecimal.Create();
  Rate1 := TXSDecimal.Create();
  Rate2 := TXSDecimal.Create();
  Rate3 := TXSDecimal.Create();
  RateDef.DecimalString := r0;
  Rate1.DecimalString := r1;
  Rate2.DecimalString := r2;
  Rate3.DecimalString := r3;
end;

destructor TPayRates.Destroy;
begin
  FreeAndNil(RateDef);
  FreeAndNil(Rate1);
  FreeAndNil(Rate2);
  FreeAndNil(Rate3);
end;

function TSwipeClockConnection.GetAndCheckWebPassword(EEData: TEvoEEData): string;
begin
  Result := VarToStr(EEData.GetEEAdditionalField(SC_PASSWORD));
  if (Result <> '') and (Length(Result) <> 8) then
    FLogger.LogWarning('Web password must be at least 8 characters');
end;

procedure TSwipeClockConnection.AddEmployee(EEData: TEvoEEData; const options: TSwipeClockOptions);
var
  rates: TPayRates;
begin
  FLogger.LogEntry('TimeWorks: Add Employee');
  try
    try
      BeginConnection;
      try
        if options.ExportRates then
          rates := TPayRates.Create( EEData.GetEERatesArray )
        else
          rates := TPayRates.Create( '0.0', '0.0', '0.0', '0.0');
        try
          FService.AddEmployee(
            trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), //Employee Code
            EEData.EEs.FieldByName('LAST_NAME').AsString,
            EEData.EEs.FieldByName('FIRST_NAME').AsString,
            EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString,
            '', //designation
            '', //title
            IIF( options.ExportSSN, trim(StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll])) , ''), //ssn
            IIF( options.ExportDepartment, trim(EEData.EEs.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString), ''), //dept
            IIF( options.ExportBranch, trim(EEData.EEs.FieldByName('CUSTOM_BRANCH_NUMBER').AsString), ''), //location
            VarToStr(EEData.GetEEAdditionalField(SC_SUPERVISOR)), //supervisor
            DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime), //? //start date
            0, //autolunch, minute
            0, //autolunch after Hours
            rates.RateDef, //payRate0 (default)
            rates.Rate1, //payRate1
            rates.Rate2, //payRate2
            rates.Rate3, //payRate3
            EEData.EEs.FieldByName('TIME_CLOCK_NUMBER').AsString, //card num1
            VarToStr(EEData.GetEEAdditionalField(SC_LOGIN_ID)), //card num2
            '', //card num3
            GetAndCheckWebPassword(EEData), //web password
            '', //options
            '', //?? //home1
            '', //?? //home2
            '', //?? //home3
            '', //schedule
            0 //exportBlock
            );
        finally
          FreeAndNil(rates);
        end;
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.UpdateEmployee(swipeEE: IXMLEmployees; EEData: TEvoEEData; const options: TSwipeClockOptions);
  function EvoIfNotEmpty(evov: Variant; sc: string): string;
  var
    evo: string;
  begin
    evo := VarToStr(evov);
    if options.AllowClearing or (trim(evo) <> '') then
      Result := evo
    else
      Result := sc;
  end;
var
  rates: TPayRates;
  exportBlock: boolean;
begin
  FLogger.LogEntry('TimeWorks: Update Employee');
  try
    try
      BeginConnection;
      try
        if swipeEE.ChildNodes.FindNode('ExportBlock') <> nil then //make sure AreDifferent doesn't touch this field! and nothing else does
          exportBlock := swipeEE.ExportBlock
        else
          exportBlock := false;
        if options.ExportRates then
          rates := TPayRates.Create( EEData.GetEERatesArray )
        else
          rates := TPayRates.Create( swipeEE.PayRate0, swipeEE.PayRate1, swipeEE.PayRate2, swipeEE.PayRate3 );
        try
          FService.UpdateEmployee(
            trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), //Employee Code
            swipeEE.Employee, // swipe clock Id, I guess
            EEData.EEs.FieldByName('LAST_NAME').AsString,
            EEData.EEs.FieldByName('FIRST_NAME').AsString,
            EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString,
            swipeEE.Designation, //designation
            swipeEE.Title, //title
            IIF(options.ExportSSN, trim(StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll])), swipeEE.SSN),
            IIF(options.ExportDepartment, trim(EEData.EEs.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString), swipeEE.Department), //dept
            IIF(options.ExportBranch, trim(EEData.EEs.FieldByName('CUSTOM_BRANCH_NUMBER').AsString), swipeEE.Location), //location
            EvoIfNotEmpty(EEData.GetEEAdditionalField(SC_SUPERVISOR), swipeEE.Supervisor), //supervisor
            DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime), //? //start date
            swipeEE.LunchMinutes, //autolunch, minute
            swipeEE.AutoLunchHours, //autolunch after Hours
            rates.RateDef, //payRate0 (default)
            rates.Rate1, //payRate1
            rates.Rate2, //payRate2
            rates.Rate3, //payRate3
            EvoIfNotEmpty(EEData.EEs['TIME_CLOCK_NUMBER'], swipeEE.CardNumber1), //card num1
            EvoIfNotEmpty(EEData.GetEEAdditionalField(SC_LOGIN_ID), swipeEE.CardNumber2), //card num2
            swipeEE.CardNumber3, //card num3
            EvoIfNotEmpty(GetAndCheckWebPassword(EEData), swipeEE.Password), //web password
            '', //options //!!no swipeEE.options!
            '', //?? //home1
            '', //?? //home2
            '', //?? //home3
            swipeEE.Schedule, //schedule //node can be missing
            ord(exportBlock) //exportBlock
            );
        finally
          FreeAndNil(rates);
        end;
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockConnection.GetSites: IXMLGetSites;
begin
  FLogger.LogEntry('TimeWorks: Get Sites');
  try
    try
      BeginConnection;
      try
        Result := bndGetSites.GetNewDataSet( ToXMLDoc(FService.GetSites(FParam.UserName), 'GetSites') );
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockConnection.ToXMLDoc(ws: widestring; name: string): IXMLDocument;
begin
  Result := inherited ToXMLDoc(ws, name);
  if Result.DocumentElement.ChildNodes.FindNode('schema') <> nil then
    Result.DocumentElement.ChildNodes.Delete('schema');
end;

function TSwipeClockConnection.This: TSwipeClockConnection;
begin
  Result := Self;
end;

initialization
  {
  http://www.borlandtalk.com/namespace-in-soapheader-how-to-change-vt112473.html

  This might or might not be related but just in case: there was a bug whereby
  Delphi would not recognize document services. The importer would miss
  emitting the call to register the interface. Something along the lines of:

  InvRegistry.RegisterInvokeOptions(TypeInfo(InterfaceName), ioDocument);

  The results of that bug is that serialization would use Section-5 encoding
  rules... which matches what you're seeing.

  ...the issue of not properly detecting document services should be addressed as of HOTFIX10 of BDS2006.
  }
  InvRegistry.RegisterInvokeOptions(TypeInfo(EmployeeWebServiceInterfaceSoap), ioDocument);

end.
