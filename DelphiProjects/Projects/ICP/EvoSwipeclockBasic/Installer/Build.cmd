@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoSwipeclockBasic
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoSwipeclockBasic.exe
cd ..\..\..\Projects\ICP\EvoSwipeclockBasic\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoTimeWorkskBasic.msi 

"%pWiXDir%\candle.exe" .\EvoSwipeclockBasic.wxs -wx -out ..\..\..\..\..\Tmp\EvoSwipeclockBasic.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoSwipeclockBasicRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoSwipeclockBasicRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoSwipeclockBasicRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoSwipeclockBasic.wixobj ..\..\..\..\..\Tmp\EvoSwipeclockBasicRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoSwipeclockBasic.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoSwipeclockBasic.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoTimeWorksBasic_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoTimeWorksBasic_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoSwipeclockBasic.msi EvoTimeWorksBasic_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
