unit EvoScheduledEDHolder;

interface

uses DB, kbmMemTable, gdyCommonLogger, SysUtils, EvoAPIConnection,
  csreader, Classes, Types, Status;

type
  FieldDescr = record
    C: string;     // Caption
    M: boolean;    // Mandatory Field
    T: TFieldType; // Type
    S: integer;    // Size
    N: string;     // Name
    Tb: string;    // Table Name
  end;

const
  idxCoNumber = 1;
  idxEeCode = 2;
  idxEdCode = 3;
  idxCalcMethod = 4;
  idxAmount = 5;
  idxPercent = 6;
  idxFrequency = 7;
  idxStartDate = 8;
  idxEndDate = 9;
  idxWeek1 = 10;
  idxWeek2 = 11;
  idxWeek3 = 12;
  idxWeek4 = 13;
  idxWeek5 = 14;

  FieldsToImport: array[1..14] of FieldDescr =
  (
    (C: 'Custom Company Number'; M: True; T: ftString; S: 20; N: 'CUSTOM_COMPANY_NUMBER'; Tb: 'CO'),
    (C: 'Ee Code'; M: True; T: ftString; S: 10; N: 'CUSTOM_EMPLOYEE_NUMBER'; Tb: 'EE'),
    (C: 'Ed Code'; M: True; T: ftString; S: 10; N: 'CUSTOM_E_D_CODE_NUMBER'; Tb: 'CL_E_DS'),
    (C: 'CalcMethod'; M: False; T: ftString; S: 1; N: 'CALCULATION_TYPE'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Amount'; M: False; T: ftFloat; S: 0; N: 'AMOUNT'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Percentage'; M: False; T: ftFloat; S: 0; N: 'PERCENTAGE'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Frequency'; M: False; T: ftString; S: 1; N: 'FREQUENCY'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Effective Start Date'; M: False; T: ftDateTime; S: 0; N: 'EFFECTIVE_START_DATE'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Effective End Date'; M: False; T: ftDateTime; S: 0; N: 'EFFECTIVE_END_DATE'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Exclude Week 1'; M: False; T: ftString; S: 1; N: 'EXCLUDE_WEEK_1'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Exclude Week 2'; M: False; T: ftString; S: 1; N: 'EXCLUDE_WEEK_2'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Exclude Week 3'; M: False; T: ftString; S: 1; N: 'EXCLUDE_WEEK_3'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Exclude Week 4'; M: False; T: ftString; S: 1; N: 'EXCLUDE_WEEK_4'; Tb: 'EE_SCHEDULED_E_DS'),
    (C: 'Exclude Week 5'; M: False; T: ftString; S: 1; N: 'EXCLUDE_WEEK_5'; Tb: 'EE_SCHEDULED_E_DS')
  );

type
  TEvoScheduledEDHolder = class
  private
    FProcessedRecord: integer;
    FScheduledEDData: TkbmCustomMemTable;
    FLogger: ICommonLogger;
    FStatus: IStatusProgress;

    procedure CreateFields;

    function GetIsReady: boolean;
    function GetRecordCount: integer;

    procedure ProcessRecord( rec: TStrings );
    procedure LoadDataFromFile(const aFileName: string);

    function GetCompanyNbr: string;
    function GetEmployeeCode: string;
    function GetEDCode: string;
    function GetCalcMethod: string;
    function GetAmount: double;
    function GetPercentage: double;
    function GetFrequency: string;
    function GetStartDate: TDatetime;
    function GetEndDate: TDatetime;
    function GetWeek1: string;
    function GetWeek2: string;
    function GetWeek3: string;
    function GetWeek4: string;
    function GetWeek5: string;
  public
    constructor Create(logger: ICommonLogger; const aFileName: string; Status: IStatusProgress);
    destructor Destroy; override;

    procedure First;
    function Eof: boolean;
    procedure Next;

    property IsReady: boolean read GetIsReady;
    property RecordCount: integer read GetRecordCount;
    property ProcessedRecordCount: integer read FProcessedRecord;

    property CompanyNbr: string read GetCompanyNbr;
    property EmployeeCode: string read GetEmployeeCode;
    property EDCode: string read GetEDCode;
    property CalcMethod: string read GetCalcMethod;
    property Amount: double read GetAmount;
    property Percentage: double read GetPercentage;
    property Frequency: string read GetFrequency;
    property StartDate: TDatetime read GetStartDate;
    property EndDate: TDatetime read GetEndDate;
    property Week1: string read GetWeek1;
    property Week2: string read GetWeek2;
    property Week3: string read GetWeek3;
    property Week4: string read GetWeek4;
    property Week5: string read GetWeek5;
  end;

implementation

uses common, TypInfo, EvoSchedEDImportConstAndProc, gdyRedir,
  DateUtils;

{ TEvoScheduledEDHolder }

constructor TEvoScheduledEDHolder.Create(logger: ICommonLogger; const aFileName: string; Status: IStatusProgress);
begin
  FLogger := logger;
  FStatus := Status;
  FScheduledEDData := TkbmCustomMemTable.Create(nil);

  CreateFields;

  LoadDataFromFile( aFileName );
end;

procedure TEvoScheduledEDHolder.CreateFields;

  procedure CreateMapFields(MapArray: Array of FieldDescr; DS: TkbmCustomMemTable);
  var
    i: integer;
  begin
    for i := Low(MapArray) to High(MapArray) do
    if MapArray[i].N <> '' then
    case MapArray[i].T of
      ftInteger:
        CreateIntegerField( DS, MapArray[i].N, MapArray[i].C );
      ftString:
        CreateStringField( DS, MapArray[i].N, MapArray[i].C, MapArray[i].S );
      ftDateTime:
        CreateDateTimeField( DS, MapArray[i].N, MapArray[i].C );
      ftFloat:
        CreateFloatField( DS, MapArray[i].N, MapArray[i].C );
      ftBoolean:
        CreateBooleanField( DS, MapArray[i].N, MapArray[i].C );
    end;
  end;

begin
  CreateMapFields( FieldsToImport, FScheduledEDData );
end;

destructor TEvoScheduledEDHolder.Destroy;
begin
  FreeAndNil( FScheduledEDData );
  inherited;
end;

function TEvoScheduledEDHolder.Eof: boolean;
begin
  Result := FScheduledEDData.Eof;
end;

procedure TEvoScheduledEDHolder.First;
begin
  FScheduledEDData.First;
end;

function TEvoScheduledEDHolder.GetAmount: double;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxAmount].N).AsFloat;
end;

function TEvoScheduledEDHolder.GetCalcMethod: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxCalcMethod].N).AsString;
end;

function TEvoScheduledEDHolder.GetCompanyNbr: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxCoNumber].N).AsString;
end;

function TEvoScheduledEDHolder.GetEDCode: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxEdCode].N).AsString;
end;

function TEvoScheduledEDHolder.GetEmployeeCode: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxEeCode].N).AsString;
end;

function TEvoScheduledEDHolder.GetEndDate: TDatetime;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxEndDate].N).AsDateTime;
end;

function TEvoScheduledEDHolder.GetFrequency: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxFrequency].N).AsString;
end;

function TEvoScheduledEDHolder.GetIsReady: boolean;
begin
  Result := Assigned(FScheduledEDData) and (FScheduledEDData.Active);
end;

function TEvoScheduledEDHolder.GetPercentage: double;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxPercent].N).AsFloat;
end;

function TEvoScheduledEDHolder.GetRecordCount: integer;
begin
  Result := -1;
  if GetIsReady then
    Result := FScheduledEDData.RecordCount;
end;

function TEvoScheduledEDHolder.GetStartDate: TDatetime;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxStartDate].N).AsDateTime;
end;

function TEvoScheduledEDHolder.GetWeek1: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxWeek1].N).AsString;
end;

function TEvoScheduledEDHolder.GetWeek2: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxWeek2].N).AsString;
end;

function TEvoScheduledEDHolder.GetWeek3: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxWeek3].N).AsString;
end;

function TEvoScheduledEDHolder.GetWeek4: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxWeek4].N).AsString;
end;

function TEvoScheduledEDHolder.GetWeek5: string;
begin
  Result := FScheduledEDData.FieldByName(FieldsToImport[idxWeek5].N).AsString;
end;

procedure TEvoScheduledEDHolder.LoadDataFromFile(const aFileName: string);
const
  BatchSize = $FF;
var
  f: TfileStream;
  s: array[0..BatchSize] of char;
  i, j, rest: Dword;
  Text: String;
  sFile: TStrings;
  Rec: TStrings;
begin
  FProcessedRecord := 0;
  if Assigned(  FScheduledEDData ) then
  try
    FLogger.LogEvent('Load data from file');
    FScheduledEDData.Close;
    FScheduledEDData.Open;

//    EachRecord(aFileName, ProcessRecord, True);

    f := TfileStream.Create(aFileName, fmOpenRead);
    try
      i := f.Size div BatchSize;
      rest := f.Size mod BatchSize;
      FStatus.SetProgressCount( i );
      FStatus.SetMessage('', 'Load Scheduled E/Ds from file', '');
      for j := 1 to i do
      begin
        f.Read(s, BatchSize);
        Text := Text + s;
        FStatus.IncProgress;
      end;
      if rest > 0 then
      begin
        f.Read(s, rest);
        Text := Text + Copy(s, 1, rest);
      end;
    finally
      FStatus.ClearProgress;
      f.Free;
    end;

    sFile := TStringList.Create;
    Rec := TStringList.Create;
    try
      sFile.Text := Text;
      for i := 0 to sFile.Count - 1 do
      begin
        Rec.CommaText := sFile[i];
        ProcessRecord( Rec );
      end;
    finally
      Rec.Free;
      sFile.Free;
    end;

    if FScheduledEDData.State in [dsEdit, dsInsert] then
      FScheduledEDData.Post;

    FLogger.LogEvent('Load data from csv file is done successfully');
  except
    on e: Exception do
      FLogger.LogError('Error when loading data from file', e.Message);    
  end;
end;

procedure TEvoScheduledEDHolder.Next;
begin
  FScheduledEDData.Next;
end;

procedure TEvoScheduledEDHolder.ProcessRecord(rec: TStrings);
var
  curValue: Currency;
  datValue: TDatetime;

  function GetExcludeWeekFieldValue(aWeekNbr: integer; const aImportedValue: string): string;
  begin
    Result := Copy(aImportedValue, aWeekNbr, 1);
    if Result = '' then
      Result := 'Y';
  end;

  procedure SetFieldValue(aIdx: integer);
  begin
    if FieldsToImport[aIdx].T = ftString then
      FScheduledEDData.FieldByName( FieldsToImport[aIdx].N ).AsString := Trim(rec[6])
    else if FieldsToImport[aIdx].T = ftFloat then
    begin
      if TryStrToCurr(Trim(rec[6]), curValue) then
        FScheduledEDData.FieldByName( FieldsToImport[aIdx].N ).AsFloat := curValue
      else
        raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. "'+ FieldsToImport[aIdx].C +'" should contain a currency value.');
    end
    else if FieldsToImport[aIdx].T = ftDateTime then
    begin
      if TryStrToDateTime(Trim(rec[6]), datValue) then
        FScheduledEDData.FieldByName( FieldsToImport[aIdx].N ).AsFloat := datValue
      else
        raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. "'+ FieldsToImport[aIdx].C +'" should contain a datatime value.');
    end
  end;
begin
  FProcessedRecord := FProcessedRecord + 1;

  if rec.Count <> 7  then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. There should be 7 columns.')
  else if (Trim(rec[0]) = '') then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. Company Code can''t be empty.')
  else if (Trim(rec[1]) = '') then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. Employee Code can''t be empty.')
  else if (Trim(rec[3]) = '') then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. E/D Code can''t be empty.')
  else begin
    try
      // ED TYPE value of the 5th field means begin of the record
      // so post dataset if necessary and add a new record
      if UpperCase( Trim(rec[4]) ) = 'ED TYPE' then
      begin
        if FScheduledEDData.State in [dsEdit, dsInsert] then
          FScheduledEDData.Post;
        FScheduledEDData.Append;
        FScheduledEDData.FieldByName( FieldsToImport[idxCoNumber].N ).AsString := Trim(rec[0]);
        FScheduledEDData.FieldByName( FieldsToImport[idxEeCode].N ).AsString := Trim(rec[1]);
        FScheduledEDData.FieldByName( FieldsToImport[idxEdCode].N ).AsString := Trim(rec[3]);
      end
      else begin
        // fill field values
        if UpperCase( Trim(rec[4]) ) = 'ED METHOD' then
          SetFieldValue( idxCalcMethod )
        else if UpperCase( Trim(rec[4]) ) = 'ED AMOUNT' then
          SetFieldValue( idxAmount )
        else if UpperCase( Trim(rec[4]) ) = 'ED PERCENT RATE' then
          SetFieldValue( idxPercent )
        else if UpperCase( Trim(rec[4]) ) = 'ED FREQ CODE' then
          SetFieldValue( idxFrequency )
        else if UpperCase( Trim(rec[4]) ) = 'ED START DATE' then
          SetFieldValue( idxStartDate )
        else if UpperCase( Trim(rec[4]) ) = 'ED END DATE' then
          SetFieldValue( idxEndDate )
        else if UpperCase( Trim(rec[4]) ) = 'ED ACTIVE WEEK' then
        begin
          FScheduledEDData.FieldByName( FieldsToImport[idxWeek1].N ).AsString := GetExcludeWeekFieldValue(1, Trim(rec[6]));
          FScheduledEDData.FieldByName( FieldsToImport[idxWeek2].N ).AsString := GetExcludeWeekFieldValue(2, Trim(rec[6]));
          FScheduledEDData.FieldByName( FieldsToImport[idxWeek3].N ).AsString := GetExcludeWeekFieldValue(3, Trim(rec[6]));
          FScheduledEDData.FieldByName( FieldsToImport[idxWeek4].N ).AsString := GetExcludeWeekFieldValue(4, Trim(rec[6]));
          FScheduledEDData.FieldByName( FieldsToImport[idxWeek5].N ).AsString := GetExcludeWeekFieldValue(5, Trim(rec[6]));
        end;
      end;
    except
      on E: Exception do
      begin
        if FScheduledEDData.State in [dsEdit, dsInsert] then
          FScheduledEDData.Cancel;
        FLogger.LogError(E.Message);
      end;
    end;
  end;
end;

end.
