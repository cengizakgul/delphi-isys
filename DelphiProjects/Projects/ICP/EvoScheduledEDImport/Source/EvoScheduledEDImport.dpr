program EvoScheduledEDImport;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  EvoAPIConnection,
  main in 'main.pas' {frmMain},
  EvoSchedEDImportConstAndProc in 'EvoSchedEDImportConstAndProc.pas',
  CommonDlg in 'dialogs\CommonDlg.pas' {frmDialog},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame},
  EvoConnectionDlg in 'dialogs\EvoConnectionDlg.pas' {frmEvoConnectionDlg},
  EvoScheduledEDImportProcessing in 'EvoScheduledEDImportProcessing.pas',
  Log in 'Log.pas' {frmLogger},
  Status in 'Status.pas' {frmStatus: TFrame},
  EvoScheduledEDHolder in 'EvoScheduledEDHolder.pas',
  DateRangeFrame in 'DateRangeFrame.pas' {frmDateRange: TFrame},
  About in 'About.pas' {frmAbout},
  Scheduler in 'Scheduler.pas' {frmScheduler},
  TaskParameters in 'TaskParameters.pas' {frmTaskParameters},
  SimpleScheduler in 'SimpleScheduler.pas' {frmSimpleScheduler: TFrame},
  wait in 'wait.pas' {frmWait},
  EeHolder in 'EeHolder.pas',
  FileOpenLeftBtnFrame in '..\..\common\FileOpenLeftBtnFrame.pas' {FileOpenLeftBtnFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  SMTPConfigDlg in 'SMTPConfigDlg.pas' {frmSmtpConfig};

{$R *.res}

begin
  LicenseKey := '3BF7BA482B114156B387859D47357D37'; // EvoScheduledEDImport License

  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
