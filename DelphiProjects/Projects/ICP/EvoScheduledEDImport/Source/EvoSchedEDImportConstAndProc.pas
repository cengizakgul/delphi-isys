unit EvoSchedEDImportConstAndProc;

interface

uses
  Forms, EvoAPIConnectionParamFrame, common, log, isSettings, gdyCommonLogger,
  sysutils, evodata, DateRangeFrame, Windows, Classes, fmtBcd, IsSMTPClient,
  SHFolder;

const
  sMainForm = 'MainForm';
  sInfinityConn = 'InfinityConn';
  sEvoConn = 'EvoConn';
  sSmtpConfig = 'EvoConn';

  SY_STATESDesc: TEvoDSDesc = (Name: 'SY_STATES');
  SY_STATE_MARITAL_STATUSDesc: TEvoDSDesc = (Name: 'SY_STATE_MARITAL_STATUS');

  cKey='Form.Button';


type
  TFrameClass = class of TFrame;
  TEvoAPIConnectionParamFrmClass = class of TEvoAPIConnectionParamFrm;

  TInfinityHRConnectionParam = record
    UserName: string;
    Password: string;
    SavePassword: boolean;
    URL: string;
  end;

  EInfinityHRError = class(Exception);

  TEvoCompany = record
    CL_NBR: integer;
    CO_NBR: integer;
    Custom_Company_Number: string
  end;

  TTaskParam = record
    FileName: string;
    SendEmail: boolean;
    AttachLog: boolean;
    AttachDevLog: boolean;
  end;

  TUploadFile = record
    EmployeeID: string;
    PayPeriod_StartDate: TDatetime;
    PayPeriod_EndDate: TDatetime;
    PayDate: TDateTime;
    CheckNumber: string;
    DepositAmount: TBcd;
    FileName: string;
    PayStubXml: string;
  end;

  TUploadFiles = array of TUploadFile;

var
  frmLogger: TfrmLogger;

procedure SaveFormSize(aForm: TForm; aSettings: IisSettings);
procedure LoadFormSize(aForm: TForm; aSettings: IisSettings);

procedure SaveDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);
procedure LoadDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);

function LoadInfinityHRConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TInfinityHRConnectionParam;
procedure SaveInfinityHRConnectionParam( const param: TInfinityHRConnectionParam; conf: IisSettings; root: string);

function ConvertNull(MyValue, Replacement: Variant): Variant;
function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;

function GetNextRunTime(aTime: TSystemTime): string;

function GetTaskParam(const aGuid: string): TTaskParam;
procedure SetTaskParam(const aGuid: string; aTaskParam: TTaskParam);
function IntConvertNull(MyValue: Variant; Replacement: integer): integer;
function GetVersion: string;

function TasksDataFolder: string;

implementation

uses gdyCommon, gdyCrypt, Variants, DateUtils, gdyRedir;

function GetVersion: string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function GetSpecialFolderPath(folder : integer) : string;
 const
   SHGFP_TYPE_CURRENT = 0;
 var
   path: array [0..MAX_PATH] of char;
begin
 if SUCCEEDED(SHGetFolderPath(0,folder,0,SHGFP_TYPE_CURRENT,@path[0])) then
   Result := path
 else
   Result := '';
end;

function TasksDataFolder: string;

  procedure CheckForDir(const dir: string);
  begin
    if not DirectoryExists(dir) then
      if not CreateDir( dir ) then
        raise Exception.Create('Cannot create ' + dir);
  end;

begin
  Result := GetSpecialFolderPath( CSIDL_COMMON_APPDATA ) + '\iSystems';
  CheckForDir( Result );

  Result := Result + '\EvoScheduledEDImport';
  CheckForDir( Result );

  Result := Result + '\Tasks';
  CheckForDir( Result );
end;

function IntConvertNull(MyValue: Variant; Replacement: integer): integer;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function GetTaskParam(const aGuid: string): TTaskParam;
var
  sl: TStrings;
  fname: string;
begin
  // defaults
  Result.FileName := '';

  fname := TasksDataFolder + '\' + aGuid +'\Parameters.txt';
  if FileExists( fname ) then
  begin
    sl := TStringList.Create;
    try
      sl.LoadFromFile( fname );
      if sl.Count = 4 then
      begin
        Result.FileName := sl[0];
        Result.SendEmail := StrToBoolDef(sl[1], False);
        Result.AttachLog := StrToBoolDef(sl[2], False);
        Result.AttachDevLog := StrToBoolDef(sl[3], False);
      end;
    finally
      sl.Free;
    end;
  end;
end;

procedure SetTaskParam(const aGuid: string; aTaskParam: TTaskParam);
var
  sl: TStrings;
  fname, dir: string;
begin
  dir := TasksDataFolder;
  if not DirectoryExists(dir) then
    if not CreateDir( dir ) then
      raise Exception.Create('Cannot create ' + dir);

  dir := dir + '\' + aGuid;
  if not DirectoryExists(dir) then
    if not CreateDir( dir ) then
      raise Exception.Create('Cannot create ' + dir);

  fname := dir +'\Parameters.txt';

  sl := TStringList.Create;
  try
    sl.Add(aTaskParam.FileName);
    sl.Add(BoolToStr(aTaskParam.SendEmail, True));
    sl.Add(BoolToStr(aTaskParam.AttachLog, False));
    sl.Add(BoolToStr(aTaskParam.AttachDevLog, False));

    sl.SaveToFile( fname );
  finally
    sl.Free;
  end;
end;

function GetNextRunTime(aTime: TSystemTime): string;
begin
  if aTime.wYear >= YearOf(Now) then
    Result := Format('Next run at %s on %d/%d/%d', [TimeToStr( EncodeTime(aTime.wHour, aTime.wMinute, aTime.wSecond, 0)), aTime.wMonth, aTime.wDay, aTime.wYear])
  else
    Result := 'Done';
end;

function ConvertNull(MyValue, Replacement: Variant): Variant;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := Copy(MyString, 1, MyLength);
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

function LoadInfinityHRConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TInfinityHRConnectionParam;
begin
  root := root + IIF(root='','','\') + 'InfinityHRConnection\';
  try
    Logger.LogDebug('XXX: key='+root+'URL');
    Result.URL := conf.AsString[root+'URL'];
    Logger.LogDebug('XXX: val='+Result.URL);
  except
    Logger.StopException;
  end;
  try
    Result.Username := conf.AsString[root+'Username'];
  except
    Logger.StopException;
  end;
  try
    if conf.GetValueNames(root).IndexOf('Password') <> -1 then
      Result.Password := conf.AsString[root+'Password']
    else
      Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  except
    Logger.StopException;
  end;
  try
    Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  except
    Logger.StopException;
  end;
end;

procedure SaveInfinityHRConnectionParam( const param: TInfinityHRConnectionParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'InfinityHRConnection\';
  conf.AsString[root+'URL'] := param.URL;
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
end;

procedure SaveFormSize(aForm: TForm; aSettings: IisSettings);
var
  root: string;
begin
  root := aForm.Name;
  try
    if aForm.WindowState = wsMaximized then
      aSettings.AsInteger[root+'Maximized'] := 1
    else begin
      aSettings.AsInteger[root+'Maximized'] := 0;
      aSettings.AsInteger[root+'Top'] := aForm.Top;
      aSettings.AsInteger[root+'Left'] := aForm.Left;
//      aSettings.AsInteger[root+'Width'] := aForm.Width;
//      aSettings.AsInteger[root+'Height'] := aForm.Height;
    end;
  except
  end;
end;

procedure LoadFormSize(aForm: TForm; aSettings: IisSettings);
var
  root: string;
begin
  root := aForm.Name;
  try
    aForm.Top := aSettings.AsInteger[root+'Top'];
    aForm.Left := aSettings.AsInteger[root+'Left'];
//    aForm.Width := aSettings.AsInteger[root+'Width'];
//    aForm.Height := aSettings.AsInteger[root+'Height'];
    if aSettings.AsInteger[root+'Maximized'] = 1 then
      aForm.WindowState := wsMaximized;
  except
  end;
end;

procedure SaveDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);
var
  root: string;
begin
  root := aDateRange.Name;
  try
    aSettings.AsString[root+'Period'] := aDateRange.PeriodType;
    aSettings.AsDateTime[root+'Dat_b'] := aDateRange.Dat_b;
    aSettings.AsDateTime[root+'Dat_e'] := aDateRange.Dat_e;
  except
  end;
end;

procedure LoadDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);
var
  root: string;
begin
  root := aDateRange.Name;
  try
    aDateRange.PeriodType := aSettings.AsString[root+'Period'];
    aDateRange.Dat_b := aSettings.AsDateTime[root+'Dat_b'];
    aDateRange.Dat_e := aSettings.AsDateTime[root+'Dat_e'];
    if YearOf( aDateRange.Dat_b ) < 1979 then
      aDateRange.SetDefaults;
  except
  end;
end;

end.
