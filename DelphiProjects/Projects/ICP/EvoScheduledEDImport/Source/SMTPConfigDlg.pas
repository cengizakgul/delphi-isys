unit SMTPConfigDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, OptionsBaseFrame, SmtpConfigFrame;

type
  TfrmSmtpConfig = class(TfrmDialog)
    SmtpConfigFrm: TSmtpConfigFrm;
  protected
    procedure DoOK; override;
    procedure BeforeShowDialog; override;
  end;

var
  frmSmtpConfig: TfrmSmtpConfig;

implementation

{$R *.dfm}

uses EvoSchedEDImportConstAndProc;

{ TfrmDialog1 }

procedure TfrmSmtpConfig.BeforeShowDialog;
begin
  SmtpConfigFrm.Config := LoadSmtpConfig( FSettings, sSmtpConfig )
end;

procedure TfrmSmtpConfig.DoOK;
begin
  SaveSmtpConfig( SmtpConfigFrm.Config, FSettings, sSmtpConfig );
end;

end.
