unit InfinityHRConnection;

interface

uses gdyCommonLogger, xmlintf, EvoInfinityHRConstAndProc, EvoInfinityMapping,
  classes, sysutils, IdHTTP, IdSSLOpenSSL, idintercept, gdycommon, Types,
  gdyRedir, common, xmldoc, variants, PayrollSync, Rio, SOAPHTTPClient,
  InvokeRegistry, SOAPConn, DB, kbmMemTable, XSBuiltIns, WinInet, FMTBcd,
  Status;

type
  IInfinityHRConnection = interface
    procedure GetEmployeesByDateRange(dat_b, dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger);
    procedure UpsertEmployeePayStubs(aFiles: TUploadFiles; FLogger: ICommonLogger);
    procedure UpsertEmployeePaystubData(aFiles: TUploadFiles; FLogger: ICommonLogger; FStatus: IStatusProgress; var SuccessfulUpload: integer);
  end;

  TInfinityHRConnection = class(TInterfacedObject, IInfinityHRConnection)
  public
    constructor Create(logger: ICommonLogger; param: TInfinityHRConnectionParam);
    destructor Destroy; override;
  private
    FService: PayrollSyncSoap;
    FLogger: ICommonLogger;
    FParam: TInfinityHRConnectionParam;
    FSession: string;

    procedure Login;

    procedure BeginConnection;
    procedure EndConnection;

    {IInfinityHRConnection}
    procedure GetEmployeesByDateRange(dat_b, dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger);
    procedure UpsertEmployeePayStubs(aFiles: TUploadFiles; FLogger: ICommonLogger);
    procedure UpsertEmployeePaystubData(aFiles: TUploadFiles; FLogger: ICommonLogger; FStatus: IStatusProgress; var SuccessfulUpload: integer);
  end;

function CreateInfinityHRConnection(logger: ICommonLogger; param: TInfinityHRConnectionParam): IInfinityHRConnection;

implementation

uses
  gdyGlobalWaitIndicator, DateUtils, DelphiNotesUTC;

const
  sCtxComponentHRISLink = 'InfintyHR connection';

function CreateInfinityHRConnection(logger: ICommonLogger; param: TInfinityHRConnectionParam): IInfinityHRConnection;
begin
  Result := TInfinityHRConnection.Create(logger, param);
end;

procedure TInfinityHRConnection.BeginConnection;
begin
  WaitIndicator.StartWait('Connecting to Web Service...');
  FService := GetPayrollSyncSoap( true, 'https://www.infinityhr.com/test/services/PayrollSync.asmx?wsdl');
end;

constructor TInfinityHRConnection.Create(logger: ICommonLogger;
  param: TInfinityHRConnectionParam);
begin
  FLogger := logger;
  FParam := param;
  Login;
end;

destructor TInfinityHRConnection.Destroy;
begin
  inherited;
end;

procedure TInfinityHRConnection.EndConnection;
begin
  try
    FService := nil;
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TInfinityHRConnection.GetEmployeesByDateRange(dat_b,
  dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger);
var
  EeReq: EmployeeRequest;
  BeginDate, EndDate: TXSDateTime;
  EeResp: EmployeeData;
  TimeOut: integer;
begin
  FLogger.LogEntry('InfinityHR GetEmployeesByDateRange');
  EeReq := EmployeeRequest.Create;
  BeginDate := TXSDateTime.Create;
  EndDate := TXSDateTime.Create;
  try
    EeReq.VendorID := FParam.UserName;
    EeReq.SessionID := FSession;

    BeginDate.AsUTCDateTime := LocalTimeToUTC(dat_b);
    EndDate.AsUTCDateTime := LocalTimeToUTC(dat_e);
    try
      FLogger.LogContextItem('Begin Date', DateTimeToStr(BeginDate.AsUTCDateTime));
      FLogger.LogContextItem('End Date', DateTimeToStr(EndDate.AsUTCDateTime));

      BeginConnection;
      try
        TimeOut := 1800000; // 30 min
        InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
        InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
        InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

        if dat_e <= Today then
          EeResp := FService.GetEmployeesByDateRange( EeReq, BeginDate, EndDate )
        else
          EeResp := FService.GetEmployeesByEffectiveDate( EeReq, BeginDate );
      finally
        EndConnection;
      end;
      EeData.LoadFromEeXML( EeResp );
    except
      FLogger.PassthroughException;
    end;
  finally
    EeReq.Free;
    BeginDate.Free;
    EndDate.Free;
    FLogger.LogExit;
  end;
end;

procedure TInfinityHRConnection.Login;
var
  Req: AuthRequest;
  Resp: AuthResponse;
begin
  FLogger.LogEntry('InfinityHR authenticate');
  Req := AuthRequest.Create;
  try
    Req.VendorID := FParam.UserName;
    Req.VendorPwd := FParam.Password;

    FLogger.LogContextItem('InfinityHR User Name', FParam.UserName);
    try
      BeginConnection;
      try
        Resp := FService.Authenticate( Req );
      finally
        EndConnection;
      end;
      FSession := Resp.SessionID;
    except
      FLogger.PassthroughException;
    end;
  finally
    Req.Free;
    FLogger.LogExit;
  end;
end;

function GetTagValue(var sSource: string; const sTag: string; aCut: boolean = False): string;
var
  Idx_b, Idx_e: integer;
begin
  Result := '';

  Idx_b := Pos('<' + sTag + '>', sSource);
  Idx_e := Pos('</' + sTag + '>', sSource);
  if (Idx_b > 0) and (Idx_b < Idx_e) then
  begin
    Result := Copy(sSource, Idx_b + Length(sTag) + 2, Idx_e - Idx_b - 2 - Length(sTag));
    if aCut then
      sSource := Copy(sSource, 1, Idx_b - 1) + Copy(sSource, Idx_e + Length(sTag) + 3, length(sSource));
  end;
end;

function FillPsSummary(var xmlText: string): Paystub_Summary;
var
  s: string;
  d: TDateTime;
  bcd: Tbcd;
begin
  Result := Paystub_Summary.Create;

  Result.PayPeriod_Start := TXSDateTime.Create;
  Result.PayPeriod_End := TXSDateTime.Create;
  Result.PayDate := TXSDateTime.Create;
  Result.DepositAmount := TXSDecimal.Create;
  Result.DepositAmountYTD := TXSDecimal.Create;

  s := GetTagValue(xmlText, 'periodBegin');
  if (s <> '') and TryStrToDate(s, d) then
    Result.PayPeriod_Start.AsUTCDateTime := LocalTimeToUTC( d );

  s := GetTagValue(xmlText, 'periodEnd');
  if (s <> '') and TryStrToDate(s, d) then
    Result.PayPeriod_End.AsUTCDateTime := LocalTimeToUTC( d );

  s := GetTagValue(xmlText, 'checkDate');
  if (s <> '') and TryStrToDate(s, d) then
    Result.PayDate.AsUTCDateTime := LocalTimeToUTC( d );

  s := GetTagValue(xmlText, 'checkAmountCurrent');
  if (s <> '') and TryStrToBcd(s, bcd) then
    Result.DepositAmount.AsBcd := bcd;

  s := GetTagValue(xmlText, 'checkAmountYTD');
  if (s <> '') and TryStrToBcd(s, bcd) then
    Result.DepositAmountYTD.AsBcd := bcd;

  Result.DepositType := 'C';
  Result.CheckNumber := GetTagValue(xmlText, 'checkNumber');
  Result.DepositAccountType := 'Checking';

  //???
  Result.DepositAccountNumber := '0';
  Result.Indicator := 'ON';
end;

function FillDetail(var xmlText: string): ArrayOfPaystub_Detail;
var
  s, lines, line: string;
  bcd: Tbcd;
  psDetail: Paystub_Detail;

  checkDate: TDateTime;
  CheckNumber: string;
begin
  SetLength( Result, 0 );
  s := GetTagValue(xmlText, 'checkDate');
  if not ((s <> '') and TryStrToDate(s, checkDate)) then
    checkDate := 0;
  CheckNumber := GetTagValue(xmlText, 'checkNumber');

  lines := GetTagValue(xmlText, 'lines');

  line := GetTagValue(lines, 'checkLine', True);
  while line <> '' do
  begin
    s := GetTagValue(line, 'earnDescription');
    if s <> '' then
    begin
      // add earning detail
      psDetail := Paystub_Detail.Create;
      psDetail.PayDate := TXSDateTime.Create;
      psDetail.PayDate.AsUTCDateTime := LocalTimeToUTC( checkDate );
      psDetail.CheckNumber := CheckNumber;
      psDetail.RecordType := 'E';
      psDetail.ItemDescription := s;

      s := GetTagValue(line, 'earnRate');
      if (s <> '') and TryStrToBcd(s, bcd) then
      begin
        psDetail.Rate := TXSDecimal.Create;
        psDetail.Rate.AsBcd := bcd;
      end;

      s := GetTagValue(line, 'earnHours');
      if (s <> '') and TryStrToBcd(s, bcd) then
      begin
        psDetail.Hours := TXSDecimal.Create;
        psDetail.Hours.AsBcd := bcd;
      end;

      s := GetTagValue(line, 'earnCurrent');
      if (s <> '') and TryStrToBcd(s, bcd) then
      begin
        psDetail.Amount := TXSDecimal.Create;
        psDetail.Amount.AsBcd := bcd;
      end;

      s := GetTagValue(line, 'earnYTD');
      if (s <> '') and TryStrToBcd(s, bcd) then
      begin
        psDetail.AmountYTD := TXSDecimal.Create;
        psDetail.AmountYTD.AsBcd := bcd;
      end;

      SetLength( Result, Length(Result) + 1);
      Result[ High(Result) ] := psDetail;
    end;

    s := GetTagValue(line, 'dedDescription');
    if s <> '' then
    begin
      // add deduction detail
      psDetail := Paystub_Detail.Create;
      psDetail.PayDate := TXSDateTime.Create;
      psDetail.PayDate.AsUTCDateTime := LocalTimeToUTC( checkDate );
      psDetail.CheckNumber := CheckNumber;
      psDetail.RecordType := 'D';
      psDetail.ItemDescription := s;

      s := GetTagValue(line, 'dedCurrent');
      if (s <> '') and TryStrToBcd(s, bcd) then
      begin
        psDetail.Amount := TXSDecimal.Create;
        psDetail.Amount.AsBcd := bcd;
      end;

      s := GetTagValue(line, 'dedYTD');
      if (s <> '') and TryStrToBcd(s, bcd) then
      begin
        psDetail.AmountYTD := TXSDecimal.Create;
        psDetail.AmountYTD.AsBcd := bcd;
      end;

      SetLength( Result, Length(Result) + 1);
      Result[ High(Result) ] := psDetail;
    end;

    line := GetTagValue(lines, 'checkLine',  True);
  end;
end;

procedure TInfinityHRConnection.UpsertEmployeePaystubData(aFiles: TUploadFiles; FLogger: ICommonLogger; FStatus: IStatusProgress; var SuccessfulUpload: integer);
var
  i: integer;
  EeID: string;
  psReq: PaystubDataRequest;
  psData: PaystubData;
  ApsData: ArrayOfPaystubData;
  TimeOut: integer;

  procedure UploadPaystubs;
  begin
    // send paystubs
    BeginConnection;
    try
      psReq.VendorID := FParam.UserName;
      psReq.SessionID := FSession;
      psReq.EmployeeID := EeID;
      psReq.Paystub_Data := ApsData;

      TimeOut := 1800000; // 30 min
      InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

      try
        FService.UpsertEmployeePaystubData( psReq );
        SuccessfulUpload := SuccessfulUpload + Length(ApsData);
      except
        on e: Exception do
          FLogger.LogWarning('Error while upserting checks for "' + EeID + '" Employee ID', e.Message);
      end;
    finally
      EndConnection;
    end;
  end;

begin
  SuccessfulUpload := 0;
  EeID := '';
  psReq := PaystubDataRequest.Create;
  try
    for i := Low(aFiles) to High(aFiles) do
    begin
      if EeID <> aFiles[i].EmployeeID then
      begin
        if EeID <> '' then
          UploadPaystubs;

        // prepare param for the next ee paystubs batch
        EeID := aFiles[i].EmployeeID;
        SetLength(ApsData, 0);
        FStatus.SetDetailMessage('Process paystubs of Employee ID: ' + EeID);
      end;

      psData := PaystubData.Create;

      psData.PayStub_Summary := FillPsSummary( aFiles[i].PayStubXml );
//      psData.PayStub_Detail := FillDetail( aFiles[i].PayStubXml );

      SetLength( ApsData, Length(ApsData) + 1);
      ApsData[High(ApsData)] := psData;
    end;
    UploadPaystubs;
  finally
    psReq.Free;
  end;
end;

procedure TInfinityHRConnection.UpsertEmployeePayStubs(
  aFiles: TUploadFiles; FLogger: ICommonLogger);
var
  i: integer;
  EeID: string;
  FileReq: PaystubRequest;
  sFile: PaystubFile;
  APaystubFiles: ArrayOfPaystubFile;
  TimeOut: integer;
  FS: TFileStream;
  fContent: TByteDynArray;

  procedure UploadPaystubs;
  begin
    // send paystubs
    BeginConnection;
    try
      FileReq.VendorID := FParam.UserName;
      FileReq.SessionID := FSession;
      FileReq.EmployeeID := EeID;
      FileReq.Paystub_Files := APaystubFiles;

      TimeOut := 1800000; // 30 min
      InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

      FService.UpsertEmployeePaystubs( FileReq );

    finally
      EndConnection;
    end;
  end;
begin
  EeID := '';
  FileReq := PaystubRequest.Create;
  try
    for i := Low(aFiles) to High(aFiles) do
    begin
      if EeID <> aFiles[i].EmployeeID then
      begin
        if EeID <> '' then
          UploadPaystubs;

        // prepare param for the next ee paystubs batch
        EeID := aFiles[i].EmployeeID;
        SetLength(APaystubFiles, 0);
      end;

      sFile := PaystubFile.Create;
      sFile.PayPeriod_StartDate := TXSDateTime.Create;
      sFile.PayPeriod_StartDate.AsUTCDateTime := LocalTimeToUTC( aFiles[i].PayPeriod_StartDate );

      sFile.PayPeriod_EndDate := TXSDateTime.Create;
      sFile.PayPeriod_EndDate.AsUTCDateTime := LocalTimeToUTC( aFiles[i].PayPeriod_EndDate );

      sFile.PayDate := TXSDateTime.Create;
      sFile.PayDate.AsUTCDateTime := LocalTimeToUTC( aFiles[i].PayDate );

      sFile.CheckNumber := aFiles[i].CheckNumber;

      sFile.DepositAmount := TXSDecimal.Create;
      sFile.DepositAmount.AsBcd := aFiles[i].DepositAmount;

      sFile.FileName := aFiles[i].FileName;
      sFile.FileExtension := 'pdf';

      FS := TFileStream.Create( aFiles[i].FileName, fmOpenRead);
      FS.Position := 0;

      SetLength( fContent, FS.Size);
      FS.ReadBuffer( fContent, SizeOf(fContent) );

      sFile.FileLength := FS.Size;
      sFile.FileContent := fContent;

      SetLength( APaystubFiles, Length(APaystubFiles) + 1);
      APaystubFiles[High(APaystubFiles)] := sFile;
    end;
  finally
    FileReq.Free;
  end;
  UploadPaystubs;
end;

end.
