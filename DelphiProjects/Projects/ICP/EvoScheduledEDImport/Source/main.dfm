object frmMain: TfrmMain
  Left = 312
  Top = 209
  BorderStyle = bsSingle
  Caption = 'Evo Scheduled ED Import'
  ClientHeight = 215
  ClientWidth = 649
  Color = clBtnFace
  Constraints.MaxHeight = 300
  Constraints.MinHeight = 250
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  ShowHint = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 196
    Width = 649
    Height = 19
    AutoHint = True
    Panels = <
      item
        Width = 450
      end
      item
        Width = 50
      end>
  end
  object pnlMain: TPanel
    Left = 9
    Top = 7
    Width = 632
    Height = 64
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    inline FileOpen: TFileOpenLeftBtnFrm
      Left = 6
      Top = 13
      Width = 503
      Height = 45
      TabOrder = 0
      inherited BitBtn1: TBitBtn
        Width = 81
        Hint = 'Open|Opens an import data file'
      end
      inherited edtFile: TEdit
        Left = 97
        Width = 401
      end
      inherited ActionList1: TActionList
        inherited FileOpen1: TFileOpen
          Dialog.Filter = 'import files|*.txt;*.csv'
        end
      end
    end
    object btnRunImport: TBitBtn
      Left = 530
      Top = 18
      Width = 90
      Height = 30
      Action = RunImport
      Caption = 'Run Import'
      TabOrder = 1
    end
  end
  inline frmStatus: TfrmStatus
    Left = 9
    Top = 77
    Width = 632
    Height = 113
    TabOrder = 2
    inherited gbStatus: TGroupBox
      Width = 632
      DesignSize = (
        632
        113)
      inherited pnlHolder: TPanel
        Width = 357
        inherited lblMessage: TLabel
          Width = 345
          Height = 28
          Font.Height = -9
          Font.Style = [fsBold]
        end
        inherited lblDetailMessage: TLabel
          Top = 38
          Width = 345
          Height = 30
        end
        inherited pbProgress: TProgressBar
          Width = 342
        end
      end
      inherited mmMessage: TMemo
        Left = 8
        Width = 255
      end
    end
  end
  object MainMenu: TMainMenu
    Left = 101
    Top = 56
    object miFile: TMenuItem
      Caption = 'File'
      object miOpenLog: TMenuItem
        Action = ShowLog
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object miExit: TMenuItem
        Action = Exit
      end
    end
    object miSettings: TMenuItem
      Caption = 'Settings'
      object miEvoConnection: TMenuItem
        Action = EvoConnection
      end
      object EmailConfiguration1: TMenuItem
        Action = EmailConfig
      end
      object miScheduler: TMenuItem
        Action = Scheduler
      end
    end
    object miHelp: TMenuItem
      Caption = 'Help'
      object miAbout: TMenuItem
        Action = About
      end
    end
  end
  object Actions: TActionList
    Left = 39
    object Exit: TAction
      Caption = 'Exit'
      OnExecute = ExitExecute
    end
    object EvoConnection: TAction
      Caption = 'Evo Connection'
      Hint = 'Setup Evo connection parameters'
      OnExecute = EvoConnectionExecute
    end
    object ShowLog: TAction
      Caption = 'Open Log'
      OnExecute = ShowLogExecute
    end
    object About: TAction
      Caption = 'About'
      OnExecute = AboutExecute
    end
    object Scheduler: TAction
      Caption = 'Scheduler'
      Hint = 'Open task scheduler dialog'
      OnExecute = SchedulerExecute
    end
    object RunImport: TAction
      Caption = 'Run Import'
      Hint = 'Import Scheduled E/Ds from the selected file to the Evo'
      OnExecute = RunImportExecute
      OnUpdate = RunImportUpdate
    end
    object EmailConfig: TAction
      Caption = 'Email Configuration'
      Hint = 'Setup email configuration'
      OnExecute = EmailConfigExecute
    end
  end
end
