object frmWait: TfrmWait
  Left = 499
  Top = 339
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Processing'
  ClientHeight = 65
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblMessage: TLabel
    Left = 8
    Top = 8
    Width = 337
    Height = 49
    Alignment = taCenter
    AutoSize = False
    Caption = 'lblMessage'
    Layout = tlCenter
    WordWrap = True
  end
  object Bevel1: TBevel
    Left = 4
    Top = 5
    Width = 343
    Height = 56
    Shape = bsFrame
  end
end
