unit EvoScheduledEDImportProcessing;

interface

uses common, EvoSchedEDImportConstAndProc, gdycommonlogger,
  DB, kbmMemTable, evoApiconnection, XmlRpcTypes, EeHolder,
  status, classes, ISZippingRoutines, EvoScheduledEDHolder;

type
  TStatisticAmounts = record
    EdProcessed: integer;
    EdUpdated: integer;
    EdInserted: integer;
    EdErrors: integer;
    EdWarnings: integer;
  end;

  TImportProcessing = class
  private
    FLogger: ICommonLogger;
    FStatus: IStatusProgress;
    FEvoAPIConnectionParam: TEvoAPIConnectionParam;
    FCurrentCompany: TEvoCompany;

    FEvoAPI: IEvoAPIConnection;

    FStat: TStatisticAmounts;
    FImportData: TEvoScheduledEDHolder;
    EvoCoData: TEvoCoData;

    procedure InitImport;
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;

    function CheckForCompany: boolean;
    function BuildPacket(var aChanges: IRpcArray; aPacketSize: integer = 0): boolean;
    procedure ApplyChangePacket(aChanges: IRpcArray);

    procedure SetSchedDefaultED(var aFields: IRpcStruct; EvoCo: TEvoCoData);
    procedure SetSchedDefault(var aFields: IRpcStruct);

    procedure PostScheduledEDsToEvo;
  public
    constructor Create(logger: ICommonLogger; EvoConn: TEvoAPIConnectionParam; Status: IStatusProgress);
    destructor Destroy; override;

    procedure ImportScheduledEDs(const aFileName: string);
    procedure EvoToInfinity;
  end;

implementation

uses SysUtils, gdyRedir, Variants;

{ TImportProcessing }

constructor TImportProcessing.Create(logger: ICommonLogger; EvoConn: TEvoAPIConnectionParam; Status: IStatusProgress);
begin
  FLogger := logger;
  FStatus := Status;
  FEvoAPIConnectionParam := EvoConn;
end;

destructor TImportProcessing.Destroy;
begin
  FreeAndNil( FImportData );

  inherited;
end;

procedure TImportProcessing.EvoToInfinity;
begin
  //
end;

function TImportProcessing.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

procedure TImportProcessing.InitImport;
begin
  FStat.EdProcessed := 0;
  FStat.EdUpdated := 0;
  FStat.EdInserted := 0;
  FStat.EdErrors := 0;
  FStat.EdWarnings := 0;
  FCurrentCompany.CL_NBR := -1;
  FCurrentCompany.CO_NBR := -1;
  FCurrentCompany.Custom_Company_Number := '';
end;

function TImportProcessing.CheckForCompany: boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
  ReOpenCo, ReOpenCl: boolean;

  procedure ProcessCompanyID(const aCompanyID: string);
  begin
    FCurrentCompany.Custom_Company_Number := aCompanyID;
    FCurrentCompany.CO_NBR := -1;

    FStatus.SetDetailMessage('Open company: ' + FCurrentCompany.Custom_Company_Number);
    param := TRpcStruct.Create;
    Param.AddItem( 'CoNumber', FCurrentCompany.Custom_Company_Number );
    FClCoData := RunQuery('TmpCoQuery.rwq', Param);
    try
      if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
      begin
        FCurrentCompany.CO_NBR := FClCoData.FindField('CO_NBR').AsInteger;
        if FCurrentCompany.CL_NBR <> FClCoData.FindField('CL_NBR').AsInteger then
        begin
          FCurrentCompany.CL_NBR := FClCoData.FindField('CL_NBR').AsInteger;
          ReOpenCl := True;
        end;
        ReOpenCo := True;
      end
      else begin
        FLogger.LogWarning('Company "' + FCurrentCompany.Custom_Company_Number + '" has not been found!');
        FStat.EdWarnings := FStat.EdWarnings + 1;
      end;
    finally
      FreeAndNil( FClCoData );
      Result := FCurrentCompany.CO_NBR > -1;
    end;
  end;
begin
  ReOpenCo := False;
  ReOpenCl := False;
  Result := FCurrentCompany.CL_NBR > -1;

  try
    if (FCurrentCompany.Custom_Company_Number <> FImportData.CompanyNbr) then
      ProcessCompanyID(FImportData.CompanyNbr {+ 'S'});

    if ReOpenCl then
    begin
      FStatus.SetDetailMessage('Open client database: CL_' + IntToStr(FCurrentCompany.CL_NBR));
      FEvoAPI.OpenClient(FCurrentCompany.CL_NBR);

      FStatus.SetDetailMessage('Load client data');
      EvoCoData.OpenClientTables;
    end;

    if ReOpenCo then
    begin
      FStatus.SetDetailMessage('Load company data');
      EvoCoData.OpenCompanyTables( FCurrentCompany.CO_NBR );
    end;
  except
    on e: Exception do
    begin
      FLogger.LogError('Find company error: ' + e.Message);
      FCurrentCompany.CL_NBR := -1;
      FCurrentCompany.CO_NBR := -1;
      FCurrentCompany.Custom_Company_Number := '';
      Result := False;
      FStat.EdErrors := FStat.EdErrors + 1;
    end;
  end;
end;

procedure TImportProcessing.ImportScheduledEDs(const aFileName: string);
begin
  FStatus.Clear;

  FStatus.SetMessage('Import Scheduled E/Ds ('+DateTimeToStr(now)+')', 'Connect to Evolution', '');
  FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);

  FImportData := TEvoScheduledEDHolder.Create(FLogger, aFileName, FStatus);

  FStatus.SetMessage('', 'Load data from Evolution', '');
  EvoCoData := TEvoCoData.Create( FLogger, FEvoAPI );

  try
    if FImportData.IsReady and (FImportData.RecordCount > 0) then
    begin
      FStatus.SetMessage('Import file: ' + aFileName, '', '');
      FStatus.SetMessage('Import file record loaded: ' + IntToStr(FImportData.ProcessedRecordCount), '', '');
      FStatus.SetMessage('Scheduled E/D record count: ' + IntToStr(FImportData.RecordCount), '', '');

      // Post Scheduled EDs
      PostScheduledEDsToEvo;

      FStatus.SetMessage('Processed: ' + IntToStr(FStat.EdProcessed) + ' (Inserted: '+IntToStr(FStat.EdInserted)+', Updated: '+IntToStr(FStat.EdUpdated)+', Errors: ' + IntToStr(FStat.EdErrors)+', Warnings: ' + IntToStr(FStat.EdWarnings)+')', '', '');
    end
    else
      FStatus.SetMessage('No Scheduled E/D data loaded', '', '');
  finally
    FStatus.SetMessage('Ready to Start', '', '');
    FStatus.ClearProgress;
  end;
end;

procedure TImportProcessing.PostScheduledEDsToEvo;
var
  Changes: IRpcArray;
begin
  InitImport;
  Changes := TRpcArray.Create;
  FImportData.First;
  FStatus.SetProgressCount( FImportData.RecordCount );
  try
    while not BuildPacket(Changes, 100) do
      ApplyChangePacket(Changes);

    ApplyChangePacket(Changes);
  finally
    FStatus.ClearProgress;
    Changes := nil;
  end;
end;

function TImportProcessing.BuildPacket(var aChanges: IRpcArray;
  aPacketSize: integer): boolean;
var
  lRecords, lPacketSize, Ee_Nbr, Cl_E_DS_Nbr, Ee_Scheduled_E_Ds_Nbr: integer;
  RowChange, Fields: IRpcStruct;

  function InvertYNValue(const aValue: string): string;
  begin
    if aValue = 'N' then
      Result := 'Y'
    else
      Result := 'N';
  end;
begin
  aChanges.Clear;
  lRecords := 0;
  if (aPacketSize = 0) or (aPacketSize > FImportData.RecordCount) then
    lPacketSize := FImportData.RecordCount
  else
    lPacketSize := aPacketSize;

  FStatus.SetMessage('','Build Change Packet','');

  while (not FImportData.Eof) and (lRecords < lPacketSize) do
  begin
    try
      RowChange := TRpcStruct.Create;
      Fields := TRpcStruct.Create;

      if CheckForCompany then
      begin
        FStatus.SetDetailMessage( 'Employee: ' + FImportData.EmployeeCode + ', E/D Code: ' + FImportData.EDCode );

        // Get Employee Number
        Ee_Nbr := EvoCoData.GetEmployeeNbr( FImportData.EmployeeCode );
        if Ee_Nbr < 0 then
        begin
          FLogger.LogWarning('Employee: ' + FImportData.EmployeeCode + ' is not found.');
          FStat.EdWarnings := FStat.EdWarnings + 1;
        end
        else begin
          // Get ED Code
          Cl_E_DS_Nbr := EvoCoData.GetClEDsNbr( FImportData.EDCode );
          if Cl_E_DS_Nbr < 0 then
          begin
            FLogger.LogWarning('ED Code: ' + FImportData.EDCode + ' is not found.');
            FStat.EdWarnings := FStat.EdWarnings + 1;
          end
          else begin

            Fields.AddItem('CALCULATION_TYPE', FImportData.CalcMethod);

            if FImportData.CalcMethod = 'F' then
            begin
              Fields.AddItem('AMOUNT', FImportData.Amount);
              Fields.AddItem('PERCENTAGE', '#NULL#');
            end
            else begin
              Fields.AddItem('PERCENTAGE', FImportData.Percentage);
              Fields.AddItem('AMOUNT', '#NULL#');
            end;

            Fields.AddItem('FREQUENCY', FImportData.Frequency);
            Fields.AddItemDateTime('EFFECTIVE_START_DATE', FImportData.StartDate);
            if Abs(FImportData.EndDate) <= 0.1 then
              Fields.AddItem('EFFECTIVE_END_DATE', '#NULL#')
            else
              Fields.AddItemDateTime('EFFECTIVE_END_DATE', FImportData.EndDate);

            Fields.AddItem('EXCLUDE_WEEK_1', InvertYNValue(FImportData.Week1));
            Fields.AddItem('EXCLUDE_WEEK_2', InvertYNValue(FImportData.Week2));
            Fields.AddItem('EXCLUDE_WEEK_3', InvertYNValue(FImportData.Week3));
            Fields.AddItem('EXCLUDE_WEEK_4', InvertYNValue(FImportData.Week4));
            Fields.AddItem('EXCLUDE_WEEK_5', InvertYNValue(FImportData.Week5));

            // Try to find Scheduled E/D by Ee_Nbr and Cl_E_Ds_Nbr
            Ee_Scheduled_E_Ds_Nbr := EvoCoData.GetScheduledEDNbr(Ee_Nbr, Cl_E_DS_Nbr, FImportData.StartDate, FImportData.EndDate);

            if Ee_Scheduled_E_Ds_Nbr < 0 then
            begin // insert
              Fields.AddItem('EE_NBR', Ee_Nbr);
              Fields.AddItem('CL_E_DS_NBR', Cl_E_DS_Nbr);

              SetSchedDefault(Fields);
              SetSchedDefaultED(Fields, EvoCoData);

              RowChange.AddItem('T', 'I');
              FStat.EdInserted := FStat.EdInserted + 1;
            end
            else begin //update
              RowChange.AddItem('T', 'U');
              RowChange.AddItem('K', Ee_Scheduled_E_Ds_Nbr);
              FStat.EdUpdated := FStat.EdUpdated + 1;
            end;

            RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
            RowChange.AddItem('F', Fields);

            if Fields.Count > 0 then
              aChanges.AddItem( RowChange );
          end;
        end;
      end;
    except
      on e: Exception do
      begin
        FLogger.LogError('Build packet error!', e.Message);
        FStat.EdErrors := FStat.EdErrors + 1;
      end;
    end;
    FImportData.Next;
    lRecords := lRecords + 1;
    FStat.EdProcessed := FStat.EdProcessed + 1;
    FStatus.IncProgress;
  end;
  Result := FImportData.Eof;
end;

procedure TImportProcessing.ApplyChangePacket(aChanges: IRpcArray);
begin
  if aChanges.Count > 0 then
  try
    FStatus.SetMessage('','Apply Change Packet ('+IntToStr(aChanges.Count)+' items)','');
    FEvoAPI.applyDataChangePacket( aChanges );
  except
    on e: Exception do begin
      FLogger.LogError('Apply Data Change Packet error!', e.Message);
      FStat.EdErrors := FStat.EdErrors + aChanges.Count;
    end;
  end;
end;

procedure TImportProcessing.SetSchedDefault(var aFields: IRpcStruct);
begin
  aFields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
  aFields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
  aFields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
end;

procedure TImportProcessing.SetSchedDefaultED(var aFields: IRpcStruct;
  EvoCo: TEvoCoData);
begin
  if not aFields.KeyExists('CALCULATION_TYPE') then
    aFields.AddItem('CALCULATION_TYPE', EvoCo.CALCULATION_TYPE);

  if not aFields.KeyExists('FREQUENCY') then
    aFields.AddItem('FREQUENCY', EvoCo.FREQUENCY);

  if not aFields.KeyExists('EXCLUDE_WEEK_1') then
    aFields.AddItem('EXCLUDE_WEEK_1', EvoCo.EXCLUDE_WEEK_1);
  if not aFields.KeyExists('EXCLUDE_WEEK_2') then
    aFields.AddItem('EXCLUDE_WEEK_2', EvoCo.EXCLUDE_WEEK_2);
  if not aFields.KeyExists('EXCLUDE_WEEK_3') then
    aFields.AddItem('EXCLUDE_WEEK_3', EvoCo.EXCLUDE_WEEK_3);
  if not aFields.KeyExists('EXCLUDE_WEEK_4') then
    aFields.AddItem('EXCLUDE_WEEK_4', EvoCo.EXCLUDE_WEEK_4);
  if not aFields.KeyExists('EXCLUDE_WEEK_5') then
    aFields.AddItem('EXCLUDE_WEEK_5', EvoCo.EXCLUDE_WEEK_5);

  aFields.AddItem('ALWAYS_PAY', EvoCo.ALWAYS_PAY);
  aFields.AddItem('DEDUCTIONS_TO_ZERO', EvoCo.DEDUCTIONS_TO_ZERO);
  aFields.AddItem('DEDUCT_WHOLE_CHECK', EvoCo.DEDUCT_WHOLE_CHECK);

  if EvoCo.CL_E_D_GROUPS_NBR > 0 then
    aFields.AddItem('CL_E_D_GROUPS_NBR', EvoCo.CL_E_D_GROUPS_NBR);
  if EvoCo.THRESHOLD_E_D_GROUPS_NBR > 0 then
    aFields.AddItem('THRESHOLD_E_D_GROUPS_NBR', EvoCo.THRESHOLD_E_D_GROUPS_NBR);

  if not aFields.KeyExists('EFFECTIVE_START_DATE') then
    aFields.AddItemDateTime('EFFECTIVE_START_DATE', EvoCo.EFFECTIVE_START_DATE);

  aFields.AddItem('PLAN_TYPE', EvoCo.PLAN_TYPE);
  aFields.AddItem('WHICH_CHECKS', EvoCo.WHICH_CHECKS);
  aFields.AddItem('USE_PENSION_LIMIT', EvoCo.USE_PENSION_LIMIT);
end;

end.
