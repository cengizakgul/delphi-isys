unit Scheduler;

interface
                                   
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, EvoSchedEDImportConstAndProc, common,
  jclTask, gdycommonlogger, ActnList, ComObj;

type
  TfrmScheduler = class(TForm)
    pnlMain: TPanel;
    lbTasks: TListBox;
    btnAdd: TBitBtn;
    btnDelete: TBitBtn;
    btnClose: TBitBtn;
    Actions: TActionList;
    Remove: TAction;
    Add: TAction;
    BitBtn1: TBitBtn;
    Edit: TAction;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RemoveExecute(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure AddUpdate(Sender: TObject);
    procedure AddExecute(Sender: TObject);
    procedure EditUpdate(Sender: TObject);
    procedure EditExecute(Sender: TObject);
    procedure lbTasksDblClick(Sender: TObject);
  private
    FLogger: ICommonLogger;
    FSysScheduler: TJclTaskSchedule;
    FTaskParameters: TStrings;

    procedure InitScheduler;
    procedure RefreshTaskList;
    function GetTaskDescription(aTask: TJclScheduledTask): string;
    function GetTask(aParameter: string): TJclScheduledTask;

    procedure RemoveTask;
  public
    { Public declarations }
  end;

  procedure ShowScheduler(Logger: ICommonLogger);

implementation

{$R *.dfm}

uses TaskParameters;

procedure ShowScheduler(Logger: ICommonLogger);
begin
  with TfrmScheduler.Create( Application ) do
  try
    FTaskParameters := TStringList.Create;
    FLogger := Logger;
    InitScheduler;
    RefreshTaskList;

    ShowModal;
  finally
    FreeAndNil(FTaskParameters);
    Free;
  end;
end;

procedure TfrmScheduler.FormDestroy(Sender: TObject);
begin
  FreeAndNil( FSysScheduler );
  SaveFormSize( Self, AppSettings );
end;

procedure TfrmScheduler.FormShow(Sender: TObject);
begin
  LoadFormSize( Self, AppSettings );
end;

procedure TfrmScheduler.InitScheduler;
begin
  FLogger.LogEntry('Initializing scheduler');
  try
    try
      if not TJclTaskSchedule.IsRunning then
        TJclTaskSchedule.Start;
      FSysScheduler := TJclTaskSchedule.Create;

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TfrmScheduler.RefreshTaskList;
var
  i: integer;
begin
  lbTasks.Clear;
  FTaskParameters.Clear;
  FSysScheduler.Refresh;
  for i := 0 to FSysScheduler.TaskCount - 1 do
  if (WideCompareText(FSysScheduler.Tasks[i].ApplicationName, GetModuleName(0)) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
  begin
    lbTasks.Items.Add( GetTaskDescription( FSysScheduler.Tasks[i] ) );
    FTaskParameters.Add( FSysScheduler.Tasks[i].Parameters )
  end;
end;

procedure TfrmScheduler.RemoveExecute(Sender: TObject);
begin
  RemoveTask;
end;

procedure TfrmScheduler.RemoveTask;
var
  i, k: integer;
  parameters, dir: string;
  ArIdx: array of integer;

  procedure RemoveFile(const aFileName: string);
  begin
    if FileExists( aFileName ) then
      if not DeleteFile(aFileName) then
        raise Exception.Create('Cannot delete file: ' + aFileName);
  end;

begin
  parameters := '';
  i := lbTasks.ItemIndex;
  if i > -1 then
  begin
    if i < FTaskParameters.Count then
    begin
      parameters := FTaskParameters[i];

      FSysScheduler.Refresh;
      for k := 0 to FSysScheduler.TaskCount - 1 do
      if (WideCompareText(FSysScheduler.Tasks[k].ApplicationName, GetModuleName(0)) = 0) and
        (WideCompareText(FSysScheduler.Tasks[k].Parameters, parameters) = 0) and
        (WideCompareText(FSysScheduler.Tasks[k].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
      begin
        SetLength(ArIdx, Length(ArIdx) + 1);
        ArIdx[ High(ArIdx) ] := k;
      end;

      if Length(ArIdx) > 0 then
      begin
        FLogger.LogEntry('Remove scheduled task');
        try
          try
            for k := Low(arIdx) to High(arIdx) do
              FSysScheduler.Delete( ArIdx[k] );

          except
            FLogger.PassthroughException;
          end;
        finally
          FLogger.LogExit;
        end;
      end;

      FTaskParameters.Delete( i );
    end;
    lbTasks.Items.Delete( i );

    // remove task directory
    dir := TasksDataFolder + '\' + parameters;

    RemoveFile(dir + '\Parameters.txt');
    RemoveFile(dir + '\debug.log');
    RemoveFile(dir + '\user.log');
 
    if DirectoryExists(dir) then
      if not RemoveDir(dir) then
        raise Exception.Create('Cannot delete ' + dir);
  end;
end;

procedure TfrmScheduler.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := lbTasks.ItemIndex > -1;
end;

procedure TfrmScheduler.AddUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(FSysScheduler);
end;

procedure TfrmScheduler.AddExecute(Sender: TObject);
begin
  if EditTask(CreateClassID, FLogger, FSysScheduler) then
    RefreshTaskList;
end;

procedure TfrmScheduler.EditUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbTasks.ItemIndex > -1) and Assigned(FSysScheduler);
end;

procedure TfrmScheduler.EditExecute(Sender: TObject);
begin
  if EditTask(FTaskParameters[lbTasks.ItemIndex], FLogger, FSysScheduler, GetTask( FTaskParameters[lbTasks.ItemIndex] )) then
    RefreshTaskList;
end;

function TfrmScheduler.GetTask(aParameter: string): TJclScheduledTask;
var
  i: integer;
begin
  Result := nil;

  for i := 0 to FSysScheduler.TaskCount-1 do
  if (WideCompareText(FSysScheduler.Tasks[i].ApplicationName, GetModuleName(0)) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Parameters, aParameter) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
  begin
    Result := FSysScheduler.Tasks[i];
    Break;
  end;
end;

function TfrmScheduler.GetTaskDescription(aTask: TJclScheduledTask): string;
begin
  Result := 'Import from file: ' + ExtractFileName(GetTaskParam( aTask.Parameters ).FileName) + ', ' + GetNextRunTime(aTask.NextRunTime);
end;

procedure TfrmScheduler.lbTasksDblClick(Sender: TObject);
begin
  EditExecute(Sender);
end;

end.
