inherited frmSmtpConfig: TfrmSmtpConfig
  Left = 362
  Top = 198
  Caption = 'Email Configuration'
  ClientHeight = 213
  ClientWidth = 604
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOk: TBitBtn
    Left = 440
    Top = 178
  end
  inherited btnCancel: TBitBtn
    Left = 522
    Top = 178
  end
  inline SmtpConfigFrm: TSmtpConfigFrm
    Left = 6
    Top = 7
    Width = 589
    Height = 158
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    inherited GroupBox1: TGroupBox
      Width = 589
      Height = 158
      inherited Label6: TLabel
        Top = 68
      end
      inherited Label8: TLabel
        Top = 23
      end
      inherited edAddressTo: TEdit
        Top = 84
        Width = 217
      end
      inherited edAddressFrom: TEdit
        Top = 39
        Width = 218
      end
      inherited btnSendTest: TBitBtn
        Top = 118
      end
      inherited gbSMTP: TGroupBox
        Top = 22
        Height = 119
      end
      inherited cbSendEmail: TCheckBox
        Visible = False
      end
    end
  end
end
