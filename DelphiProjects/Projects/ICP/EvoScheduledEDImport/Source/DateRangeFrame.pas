unit DateRangeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TfrmDateRange = class(TFrame)
    edDatB: TDateTimePicker;
    edDatE: TDateTimePicker;
    lblFrom: TLabel;
    lblTo: TLabel;
    edTimeB: TDateTimePicker;
    edTimeE: TDateTimePicker;
    cbPeriod: TComboBox;
    procedure cbPeriodChange(Sender: TObject);
  private
    procedure EnableDates(aValue: boolean);
    procedure SetPeriodType(const aValue: string);
    function GetPeriodType: string;
    procedure SetDatB(aValue: TDateTime);
    function GetDatB: TDateTime;
    procedure SetDatE(aValue: TDateTime);
    function GetDatE: TDateTime;
    function GetAsString: string;
    procedure SetAsString(const aValue: string);
  public
    procedure SetDefaults;
    procedure Loaded; override;

    property AsString: string read GetAsString write SetAsString;

    property PeriodType: string read GetPeriodType write SetPeriodType;
    property Dat_b: TDateTime read GetDatB write SetDatB;
    property Dat_e: TDateTime read GetDatE write SetDatE;
  end;

implementation

uses DateUtils;

{$R *.dfm}

{ TfrmDateRange }

function TfrmDateRange.GetDatB: TDateTime;
begin
  Result := DateOf(edDatB.DateTime) + TimeOf(edTimeB.DateTime);
end;

function TfrmDateRange.GetDatE: TDateTime;
begin
  Result := DateOf(edDatE.DateTime) + TimeOf(edTimeE.DateTime);
end;

procedure TfrmDateRange.Loaded;
begin
  inherited;
  SetDefaults;
end;

procedure TfrmDateRange.SetDatB(aValue: TDateTime);
begin
  edDatB.DateTime := aValue;
  edTimeB.DateTime := aValue;
end;

procedure TfrmDateRange.SetDatE(aValue: TDateTime);
begin
  edDatE.DateTime := aValue;
  edTimeE.DateTime := aValue;
end;

procedure TfrmDateRange.SetDefaults;
begin
  cbPeriod.ItemIndex := 0;
  Dat_e := EndOfTheMonth( Now );
  Dat_b := StartOfTheMonth( Now );
end;

procedure TfrmDateRange.cbPeriodChange(Sender: TObject);
begin
  case cbPeriod.ItemIndex of
    0: begin //Custom Period
      EnableDates( True );
      SetDefaults;
    end;
    1: begin //The Current Week
      EnableDates( False );
      Dat_b := StartOfTheWeek( Now );
      Dat_e := EndOfTheWeek( Now );
    end;
    2: begin //The Last Week
      EnableDates( False );
      Dat_b := StartOfTheWeek( StartOfTheWeek(Now) - 1 );
      Dat_e := EndOfTheWeek( StartOfTheWeek(Now) - 1 );
    end;
    3: begin //The Current Month
      EnableDates( False );
      Dat_b := StartOfTheMonth( Now );
      Dat_e := EndOfTheMonth( Now );
    end;
    4: begin //The Last Month
      EnableDates( False );
      Dat_b := StartOfTheMonth( StartOfTheMonth(Now) - 1 );
      Dat_e := EndOfTheMonth( StartOfTheMonth(Now) - 1 );
    end;
    5: begin //The Current Year
      EnableDates( False );
      Dat_b := StartOfTheYear( Now );
      Dat_e := EndOfTheYear( Now );
    end;
  end;
end;

function TfrmDateRange.GetPeriodType: string;
begin
  Result := cbPeriod.Text;
end;

procedure TfrmDateRange.SetPeriodType(const aValue: string);
begin
  if cbPeriod.Items.IndexOf( aValue ) > -1 then
    cbPeriod.ItemIndex := cbPeriod.Items.IndexOf( aValue )
  else
    cbPeriod.ItemIndex := 0; //Custom Period

  cbPeriodChange( Self );  
end;

procedure TfrmDateRange.EnableDates(aValue: boolean);
begin
  edDatB.Enabled := aValue;
  edDatE.Enabled := aValue;
  edTimeB.Enabled := aValue;
  edTimeE.Enabled := aValue;
end;

function TfrmDateRange.GetAsString: string;
begin
  Result := cbPeriod.Text;

  if cbPeriod.ItemIndex = 0 then
    Result := Result + ': ' + DateToStr( Dat_b ) + ' - ' + DateToStr( Dat_e );
end;

procedure TfrmDateRange.SetAsString(const aValue: string);
var
  BeginDate, EndDate: TDateTime;
  s: string;
begin
  SetDefaults;

  s := aValue;
  if Pos(':', s) > 0 then
    s := Copy(s, 1, Pos(':', s) - 1 );

  PeriodType := s;

  if (cbPeriod.ItemIndex = 0) and (Pos(':', aValue) > 0) then
  begin
    s := Copy(aValue, Pos(':', aValue) + 1, length(aValue) );
    if Pos(' - ', s) > 0  then
      if TryStrToDate( Trim(Copy(s, 1, Pos(' - ', s) - 1 )), BeginDate) and
        TryStrToDate(Trim(Copy(s, Pos(' - ', s) + 3, length(s) )), EndDate) then
      begin
        Dat_b := BeginDate;
        Dat_e := EndDate;
      end;
  end;
end;

end.
