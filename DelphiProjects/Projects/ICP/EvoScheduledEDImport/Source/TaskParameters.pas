unit TaskParameters;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, DateRangeFrame, EvoSchedEDImportConstAndProc,
  common, SimpleScheduler, gdyCommonLogger, jclTask, FileOpenLeftBtnFrame,
  OptionsBaseFrame, SmtpConfigFrame;

type
  TfrmTaskParameters = class(TfrmDialog)
    Scheduler: TfrmSimpleScheduler;
    gbFileName: TGroupBox;
    FileOpen: TFileOpenLeftBtnFrm;
    gbReport: TGroupBox;
    chbSendEmail: TCheckBox;
    chbAttachUserLog: TCheckBox;
    chbAttachDevLog: TCheckBox;
  private
    FParameter: string;
  protected
    procedure DoOK; override;
  end;

  function EditTask(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;

implementation

{$R *.dfm}

uses gdyCrypt, gdyCommon;

function EditTask(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;
begin
  with TfrmTaskParameters.Create( Application ) do
  try
    FParameter := Parameter;
    with GetTaskParam( FParameter ) do
    begin
      FileOpen.Filename := FileName;
      chbSendEmail.Checked := SendEmail;
      chbAttachUserLog.Checked := AttachLog;
      chbAttachDevLog.Checked := AttachDevLog;
    end;
    Scheduler.Init(Parameter, Logger, SysScheduler, Task);

    Result := ShowModal = mrOk;
  finally
    Free;
  end;
end;

{ TfrmTaskParameters }

procedure TfrmTaskParameters.DoOK;
var
  TaskParam: TTaskParam;
begin
  inherited;

  if Scheduler.Save then
  begin
    // create/update task directory
    TaskParam.FileName := FileOpen.Filename;

    TaskParam.SendEmail := chbSendEmail.Checked;
    TaskParam.AttachLog := chbAttachUserLog.Checked;
    TaskParam.AttachDevLog := chbAttachDevLog.Checked;

    SetTaskParam( FParameter, TaskParam );
    ModalResult := mrOk;
  end
  else begin
    ShowMessage('Please, schedule the task');
    ModalResult := mrNone;
  end;
end;

end.
