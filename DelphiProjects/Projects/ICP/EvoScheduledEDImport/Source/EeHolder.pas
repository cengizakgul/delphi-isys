unit EeHolder;

interface

uses DB, kbmMemTable, gdyCommonLogger, SysUtils, EvoData, Common,
  EvoAPIConnection, XmlRpcTypes, wait;

type
  TEvoCoData = class
  private
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    FClEDs: TkbmCustomMemTable;
    FEEs: TkbmCustomMemTable;
    FSchedEDs: TkbmCustomMemTable;

    FCALCULATION_TYPE: string;
    FFREQUENCY: string;
    FEXCLUDE_WEEK_1: string;
    FEXCLUDE_WEEK_2: string;
    FEXCLUDE_WEEK_3: string;
    FEXCLUDE_WEEK_4: string;
    FEXCLUDE_WEEK_5: string;
    FALWAYS_PAY: string;
    FDEDUCTIONS_TO_ZERO: string;
    FDEDUCT_WHOLE_CHECK: string;
    FEFFECTIVE_START_DATE: TDateTime;
    FPLAN_TYPE: string;
    FWHICH_CHECKS: string;
    FUSE_PENSION_LIMIT: string;
    FTHRESHOLD_E_D_GROUPS_NBR: integer;
    FCL_E_D_GROUPS_NBR: integer;

    FRecentEeCode: string;
    FRecentEeNbr: integer;
    FCoNbr: integer;
  public
    constructor Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection);
    destructor Destroy; override;

    function GetEmployeeNbr(const EeCode: string): integer;
    function GetClEDsNbr(const EdCode: string): integer;
    function GetScheduledEDNbr(aEeNbr, aClEDsNbr: integer; aStartDate, aEndDate: TDateTime): integer;

    procedure OpenCompanyTables(CoNbr: integer);
    procedure OpenClientTables;

    property CALCULATION_TYPE: string read FCALCULATION_TYPE;
    property FREQUENCY: string read FFREQUENCY;
    property EXCLUDE_WEEK_1: string read FEXCLUDE_WEEK_1;
    property EXCLUDE_WEEK_2: string read FEXCLUDE_WEEK_2;
    property EXCLUDE_WEEK_3: string read FEXCLUDE_WEEK_3;
    property EXCLUDE_WEEK_4: string read FEXCLUDE_WEEK_4;
    property EXCLUDE_WEEK_5: string read FEXCLUDE_WEEK_5;
    property ALWAYS_PAY: string read FALWAYS_PAY;
    property DEDUCTIONS_TO_ZERO: string read FDEDUCTIONS_TO_ZERO;
    property DEDUCT_WHOLE_CHECK: string read FDEDUCT_WHOLE_CHECK;
    property EFFECTIVE_START_DATE: TDateTime read FEFFECTIVE_START_DATE;
    property PLAN_TYPE: string read FPLAN_TYPE;
    property WHICH_CHECKS: string read FWHICH_CHECKS;
    property USE_PENSION_LIMIT: string read FUSE_PENSION_LIMIT;
    property THRESHOLD_E_D_GROUPS_NBR: integer read FTHRESHOLD_E_D_GROUPS_NBR;
    property CL_E_D_GROUPS_NBR: integer read FCL_E_D_GROUPS_NBR;
  end;

implementation

uses gdyRedir, EvoSchedEDImportConstAndProc, Variants;

{ TEvoCoData }

constructor TEvoCoData.Create(Logger: ICommonLogger;
  EvoAPI: IEvoAPIConnection);
begin
  FLogger := Logger;
  FEvoAPI := EvoAPI;
end;

destructor TEvoCoData.Destroy;
begin
  FreeAndNil( FClEDs );
  FreeAndNil( FEEs );
  FreeAndNil( FSchedEDs );

  inherited;
end;

procedure TEvoCoData.OpenCompanyTables(CoNbr: integer);
begin
  FCoNbr := CoNbr;
end;

procedure TEvoCoData.OpenClientTables;
begin
  if not Assigned(FClEDs) then
  begin
    FClEDs := TkbmCustomMemTable.Create(nil);
    CreateIntegerField(FClEDs, 'Cl_E_Ds_Nbr', 'Cl_E_Ds_Nbr');
    CreateStringField(FClEDs, 'Custom_E_D_Code_Number', 'Custom_E_D_Code_Number', 20);
    CreateStringField(FClEDs, 'Scheduled_Defaults', 'Scheduled_Defaults', 1);
    CreateStringField(FClEDs, 'Sd_Always_Pay', 'Sd_Always_Pay', 1);
    CreateStringField(FClEDs, 'Sd_Calculation_Method', 'Sd_Calculation_Method', 1);
    CreateStringField(FClEDs, 'Sd_Deduct_Whole_Check', 'Sd_Deduct_Whole_Check', 1);
    CreateStringField(FClEDs, 'Sd_Deductions_To_Zero', 'Sd_Deductions_To_Zero', 1);
    CreateDateTimeField(FClEDs, 'Sd_Effective_Start_Date', 'Sd_Effective_Start_Date');
    CreateStringField(FClEDs, 'Sd_Exclude_Week_1', 'Sd_Exclude_Week_1', 1);
    CreateStringField(FClEDs, 'Sd_Exclude_Week_2', 'Sd_Exclude_Week_2', 1);
    CreateStringField(FClEDs, 'Sd_Exclude_Week_3', 'Sd_Exclude_Week_3', 1);
    CreateStringField(FClEDs, 'Sd_Exclude_Week_4', 'Sd_Exclude_Week_4', 1);
    CreateStringField(FClEDs, 'Sd_Exclude_Week_5', 'Sd_Exclude_Week_5', 1);
    CreateStringField(FClEDs, 'Sd_Frequency', 'Sd_Frequency', 1);
    CreateStringField(FClEDs, 'Sd_Plan_Type', 'Sd_Plan_Type', 1);
    CreateStringField(FClEDs, 'Sd_Use_Pension_Limit', 'Sd_Use_Pension_Limit', 1);
    CreateStringField(FClEDs, 'Sd_Which_Checks', 'Sd_Which_Checks', 1);
    CreateIntegerField(FClEDs, 'Sd_Threshold_E_D_Groups_Nbr', 'Sd_Threshold_E_D_Groups_Nbr');
    CreateIntegerField(FClEDs, 'Cl_E_D_Groups_Nbr', 'Cl_E_D_Groups_Nbr');
  end
  else
    FClEDs.Close;

  FClEDs.Open;
end;

function TEvoCoData.GetEmployeeNbr(const EeCode: string): integer;
var
  Param: IRpcStruct;
  lDS: TkbmCustomMemTable;
begin
  if FRecentEeCode = EeCode then
    Result := FRecentEeNbr
  else begin
    param := TRpcStruct.Create;
    Param.AddItem('CoNbr', FCoNbr);
    Param.AddItem('EeCode', EeCode);

    lDS := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'GetEmployeeNbr.rwq', Param);
    try
      if lDS.RecordCount <= 0 then
        Result := -1
      else begin
        Result := lDS.FieldByName('EE_NBR').AsInteger;

        FRecentEeCode := EeCode;
        FRecentEeNbr := Result;

        if Assigned(FSchedEDs) then FreeAndNil(FSchedEDs);

        param := TRpcStruct.Create;
        Param.AddItem('CoNbr', FCoNbr);
        Param.AddItem('EeNbr', Result);

        FSchedEDs := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'GetSchedEDs_pro.rwq', Param);
      end;  
    finally
      FreeAndNil( lDS );
    end;
  end;
end;

function TEvoCoData.GetClEDsNbr(const EdCode: string): integer;
var
  lClEDs: TkbmCustomMemTable;
  Param: IRpcStruct;

  procedure SetResult;
  begin
    Result := FClEDs.FieldByName('CL_E_DS_NBR').AsInteger;

    if FClEDs.FieldByName('Scheduled_Defaults').AsString = 'Y' then
    begin
      FCALCULATION_TYPE := FClEDs.FieldByName('Sd_Calculation_Method').AsString;
      FFREQUENCY := FClEDs.FieldByName('Sd_Frequency').AsString;
      FEXCLUDE_WEEK_1 := FClEDs.FieldByName('Sd_Exclude_Week_1').AsString;
      FEXCLUDE_WEEK_2 := FClEDs.FieldByName('Sd_Exclude_Week_2').AsString;
      FEXCLUDE_WEEK_3 := FClEDs.FieldByName('Sd_Exclude_Week_3').AsString;
      FEXCLUDE_WEEK_4 := FClEDs.FieldByName('Sd_Exclude_Week_4').AsString;
      FEXCLUDE_WEEK_5 := FClEDs.FieldByName('Sd_Exclude_Week_5').AsString;
      FALWAYS_PAY := FClEDs.FieldByName('Sd_Always_Pay').AsString;
      FDEDUCTIONS_TO_ZERO := FClEDs.FieldByName('Sd_Deductions_To_Zero').AsString;
      FDEDUCT_WHOLE_CHECK := FClEDs.FieldByName('Sd_Deduct_Whole_Check').AsString;
      FTHRESHOLD_E_D_GROUPS_NBR := FClEDs.FieldByName('Sd_Threshold_E_D_Groups_Nbr').AsInteger;
      FCL_E_D_GROUPS_NBR := FClEDs.FieldByName('Cl_E_D_Groups_Nbr').AsInteger;

      if FClEDs.FieldByName('Sd_Effective_Start_Date').IsNull then
        FEFFECTIVE_START_DATE := now
      else
        FEFFECTIVE_START_DATE := FClEDs.FieldByName('Sd_Effective_Start_Date').AsDateTime;

      FPLAN_TYPE := FClEDs.FieldByName('Sd_Plan_Type').AsString;
      FWHICH_CHECKS := FClEDs.FieldByName('Sd_Which_Checks').AsString;
      FUSE_PENSION_LIMIT := FClEDs.FieldByName('Sd_Use_Pension_Limit').AsString;
    end
    else begin
      FCALCULATION_TYPE := 'F';
      FFREQUENCY := 'D';
      FEXCLUDE_WEEK_1 := 'N';
      FEXCLUDE_WEEK_2 := 'N';
      FEXCLUDE_WEEK_3 := 'N';
      FEXCLUDE_WEEK_4 := 'N';
      FEXCLUDE_WEEK_5 := 'N';
      FALWAYS_PAY := 'Y';
      FDEDUCTIONS_TO_ZERO := 'Y';
      FDEDUCT_WHOLE_CHECK := 'Y';
      FEFFECTIVE_START_DATE := now;
      FPLAN_TYPE := 'N';
      FWHICH_CHECKS := 'A';
      FUSE_PENSION_LIMIT := 'Y';
      FTHRESHOLD_E_D_GROUPS_NBR := 0;
      FCL_E_D_GROUPS_NBR := 0;
    end;
  end;

  procedure AppendFCLEDs;
  var
    i: integer;
  begin
    FClEDs.Append;
    try
      for i := 0 to lClEDs.FieldCount - 1 do
        FClEDs.FindField( lClEDs.Fields[i].FieldName ).Value := lClEDs.Fields[i].Value;
        
      FClEDs.Post;
    except
      FClEDs.Cancel;
    end;
  end;
begin
  Result := -1;

  if not FClEDs.Locate('CUSTOM_E_D_CODE_NUMBER', EdCode, []) then
  begin
    param := TRpcStruct.Create;
    Param.AddItem('CoNbr', FCoNbr);
    Param.AddItem('EdCode', EdCode);

    lClEDs := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'GetClEDs_pro.rwq', Param);
    try
      if lClEDs.RecordCount > 0 then
      begin
        AppendFCLEDs;
        SetResult;
      end;
    finally
      FreeAndNil( lClEDs );
    end;
  end
  else
    SetResult;
end;

function TEvoCoData.GetScheduledEDNbr(aEeNbr, aClEDsNbr: integer;
  aStartDate, aEndDate: TDateTime): integer;
begin
  Result := -1;
  if Assigned( FSchedEDs ) and FSchedEDs.Active then
  begin
    FSchedEDs.Filter := 'EE_NBR=' + IntToStr(aEeNbr) + ' and CL_E_DS_NBR=' + IntToStr(aClEDsNbr);
    FSchedEDs.Filtered := True;

    if FSchedEDs.RecordCount > 0 then
    begin
      FSchedEDs.First; // sorted by EffectiveStartDate, EffectiveEndDate
      while (not FSchedEDs.Eof) and (Result = -1) do
      begin
        if FSchedEDs.FieldByName('Effective_Start_Date').AsDateTime > aStartDate then
          Result := FSchedEDs.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger;
//
        FSchedEDs.Next;
      end;
      if Result = -1 then
        Result := FSchedEDs.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger;
    end;
  end;
end;

end.
