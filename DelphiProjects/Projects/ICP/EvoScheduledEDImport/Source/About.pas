unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TfrmAbout = class(TForm)
    btnOk: TButton;
    pnlMain: TPanel;
    lblName: TLabel;
    lblVersion: TLabel;
    lblFirm: TLabel;
    Image1: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure ShowAbout;

implementation            

uses DateUtils, EvoSchedEDImportConstAndProc;

{$R *.dfm}

procedure ShowAbout;
begin
  with TfrmAbout.Create( Application ) do
  try
    lblVersion.Caption := 'Version: ' + GetVersion;
    ShowModal;
  finally
    Free;
  end;
end;

end.
 