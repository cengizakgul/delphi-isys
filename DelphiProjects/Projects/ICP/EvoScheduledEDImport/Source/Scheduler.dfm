object frmScheduler: TfrmScheduler
  Left = 416
  Top = 200
  Width = 432
  Height = 307
  Caption = 'Scheduled Task List'
  Color = clBtnFace
  Constraints.MinHeight = 250
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    424
    273)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 8
    Top = 8
    Width = 403
    Height = 220
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      403
      220)
    object lbTasks: TListBox
      Left = 8
      Top = 8
      Width = 388
      Height = 204
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
      OnDblClick = lbTasksDblClick
    end
  end
  object btnAdd: TBitBtn
    Left = 8
    Top = 234
    Width = 65
    Height = 25
    Action = Add
    Anchors = [akLeft, akBottom]
    Caption = 'Add'
    TabOrder = 1
  end
  object btnDelete: TBitBtn
    Left = 148
    Top = 234
    Width = 65
    Height = 25
    Action = Remove
    Anchors = [akLeft, akBottom]
    Caption = 'Remove'
    TabOrder = 2
  end
  object btnClose: TBitBtn
    Left = 345
    Top = 234
    Width = 65
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 3
  end
  object BitBtn1: TBitBtn
    Left = 78
    Top = 234
    Width = 65
    Height = 25
    Action = Edit
    Anchors = [akLeft, akBottom]
    Caption = 'Edit'
    TabOrder = 4
  end
  object Actions: TActionList
    Left = 32
    Top = 32
    object Remove: TAction
      Caption = 'Remove'
      OnExecute = RemoveExecute
      OnUpdate = RemoveUpdate
    end
    object Add: TAction
      Caption = 'Add'
      OnExecute = AddExecute
      OnUpdate = AddUpdate
    end
    object Edit: TAction
      Caption = 'Edit'
      OnExecute = EditExecute
      OnUpdate = EditUpdate
    end
  end
end
