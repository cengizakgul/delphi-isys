unit CommonDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, EvoSchedEDImportConstAndProc, isSettings;

type
  TfrmDialog = class(TForm)
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    procedure btnOkClick(Sender: TObject);
  protected
    FSettings: IisSettings;
    procedure DoOK; virtual;
    procedure BeforeShowDialog; virtual;
  public
    class function ShowDialog: TModalResult;
  end;

implementation

{$R *.dfm}

uses common;

procedure TfrmDialog.BeforeShowDialog;
begin
  //
end;

procedure TfrmDialog.DoOK;
begin
  ModalResult := mrOk;
end;

class function TfrmDialog.ShowDialog: TModalResult;
var
  frm: TfrmDialog;
begin
  frm := Self.Create(nil);
  with frm do
  try
    FSettings := AppSettings;
    BeforeShowDialog;
    Result := ShowModal;
  finally
    FreeAndNil( frm );
  end;
end;

procedure TfrmDialog.btnOkClick(Sender: TObject);
begin
  DoOK;
end;

end.
