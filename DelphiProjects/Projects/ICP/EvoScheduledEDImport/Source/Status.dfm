object frmStatus: TfrmStatus
  Left = 0
  Top = 0
  Width = 501
  Height = 113
  TabOrder = 0
  object gbStatus: TGroupBox
    Left = 0
    Top = 0
    Width = 501
    Height = 113
    Align = alClient
    Caption = 'Status'
    TabOrder = 0
    DesignSize = (
      501
      113)
    object pnlHolder: TPanel
      Left = 268
      Top = 16
      Width = 225
      Height = 89
      Anchors = [akTop, akRight, akBottom]
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
      DesignSize = (
        225
        89)
      object lblMessage: TLabel
        Left = 6
        Top = 8
        Width = 213
        Height = 20
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'lblMessage'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object lblDetailMessage: TLabel
        Left = 6
        Top = 32
        Width = 213
        Height = 39
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoSize = False
        Caption = 'lblMessage'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object pbProgress: TProgressBar
        Left = 7
        Top = 70
        Width = 210
        Height = 12
        Anchors = [akLeft, akRight, akBottom]
        TabOrder = 0
      end
    end
    object mmMessage: TMemo
      Left = 6
      Top = 16
      Width = 257
      Height = 89
      Anchors = [akLeft, akTop, akRight, akBottom]
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
end
