unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ActnList, Menus, common, isSettings, StdCtrls,
  EvoSchedEDImportConstAndProc, ExtCtrls, Status, xmldom, XMLIntf, msxmldom,
  XMLDoc, DateRangeFrame, dbcomp, Grids, Wwdbigrd, Wwdbgrid, SmtpConfigFrame,
  ISBasicClasses, InvokeRegistry, Buttons, FileOpenLeftBtnFrame;

type
  TfrmMain = class(TForm)
    StatusBar: TStatusBar;
    MainMenu: TMainMenu;
    miFile: TMenuItem;
    miHelp: TMenuItem;
    miExit: TMenuItem;
    Actions: TActionList;
    Exit: TAction;
    miSettings: TMenuItem;
    miEvoConnection: TMenuItem;
    miScheduler: TMenuItem;
    miAbout: TMenuItem;
    EvoConnection: TAction;
    ShowLog: TAction;
    miOpenLog: TMenuItem;
    N1: TMenuItem;
    pnlMain: TPanel;
    frmStatus: TfrmStatus;
    About: TAction;
    Scheduler: TAction;
    FileOpen: TFileOpenLeftBtnFrm;
    btnRunImport: TBitBtn;
    RunImport: TAction;
    EmailConfig: TAction;
    EmailConfiguration1: TMenuItem;
    procedure ExitExecute(Sender: TObject);
    procedure EvoConnectionExecute(Sender: TObject);
    procedure ShowLogExecute(Sender: TObject);
    procedure InfinityToEvoExecute(Sender: TObject);
    procedure AboutExecute(Sender: TObject);
    procedure SchedulerExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RunImportUpdate(Sender: TObject);
    procedure RunImportExecute(Sender: TObject);
    procedure EmailConfigExecute(Sender: TObject);
  private
    FSettings: IisSettings;
    FEvoConn: TEvoAPIConnectionParam;

    procedure CheckForScheduledRun;

    function CheckConnectionParameters: boolean;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses log, EvoConnectionDlg, EvoScheduledEDImportProcessing, Math,
  gdyDeferredCall, About, Scheduler, SMTPConfigDlg;

{ TfrmMain }

procedure TfrmMain.ExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.EvoConnectionExecute(Sender: TObject);
begin
  if TfrmEvoConnectionDlg.ShowDialog = mrOk then
    FEvoConn := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn );
end;

procedure TfrmMain.ShowLogExecute(Sender: TObject);
begin
  frmLogger.ShowModal;
end;

destructor TfrmMain.Destroy;
begin
  SaveFormSize(Self, FSettings);
  inherited;
  FreeAndNil(frmLogger);
end;

constructor TfrmMain.Create(Owner: TComponent);
begin
  frmLogger := TfrmLogger.Create( Application );

  inherited;
  FSettings := AppSettings;
  LoadFormSize(Self, FSettings);

  FEvoConn := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn );

  frmStatus.SetLogger( frmLogger.Logger );
  frmStatus.Clear;
  frmStatus.SetMessage('Ready to Start','','');
end;

procedure TfrmMain.InfinityToEvoExecute(Sender: TObject);
begin
  if CheckConnectionParameters then
  with TImportProcessing.Create(frmLogger.Logger, FEvoConn, frmStatus) do
  try
    ImportScheduledEDs('FileName');
  finally
    Free;
  end
end;

procedure TfrmMain.AboutExecute(Sender: TObject);
begin
  ShowAbout;
end;

procedure TfrmMain.SchedulerExecute(Sender: TObject);
begin
  ShowScheduler( frmLogger.Logger );
end;

procedure TfrmMain.CheckForScheduledRun;
var
  Param: TTaskParam;
  SmtpConfig: TSmtpConfig;
  fn: string;
begin
  if (ParamCount > 0) then
  try
    Param := GetTaskParam( ParamStr(1) );

    FileOpen.Filename := Param.FileName;

    Self.WindowState := wsMinimized;
    Self.Hide;

    RunImportExecute( Self );

    frmStatus.mmMessage.Lines.Delete( frmStatus.mmMessage.Lines.Count - 1 );

    if Param.SendEmail then
    try
      SmtpConfig := LoadSmtpConfig( FSettings, sSmtpConfig );
      if Param.AttachLog or Param.AttachDevLog then
      begin
        fn := frmLogger.PackLogFiles( ExtractFileDir(Application.ExeName) + '\To Send\', Param.AttachLog, Param.AttachDevLog );
        try
          SendMail( SmtpConfig, 'EvoScheduledEDImport report', frmStatus.mmMessage.Lines.Text, fn );
        finally
          DeleteFile( fn );
        end;
      end
      else
        SendMail( SmtpConfig, 'EvoScheduledEDImport report', frmStatus.mmMessage.Lines.Text, '' );
    except

    end;
  finally
    Close;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  DeferredCall( CheckForScheduledRun );
end;

function TfrmMain.CheckConnectionParameters: boolean;
begin
  Result := False;
  if (Trim(FEvoConn.APIServerAddress) = '') or (Trim(FEvoConn.Username) = '') or (Trim(FEvoConn.Password) = '') then
    frmStatus.SetMessage('Evo Connection settings were not set up properly', ' ', ' ')
  else
    Result := True;
end;

procedure TfrmMain.RunImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FileExists( FileOpen.Filename );
end;

procedure TfrmMain.RunImportExecute(Sender: TObject);
begin
  with TImportProcessing.Create( frmLogger.Logger, FEvoConn, frmStatus) do
  try
    ImportScheduledEDs( FileOpen.Filename );
  finally
    Free;
  end;
end;

procedure TfrmMain.EmailConfigExecute(Sender: TObject);
begin
  TfrmSmtpConfig.ShowDialog;
end;

end.
