inherited frmTaskParameters: TfrmTaskParameters
  Left = 363
  Top = 173
  Caption = 'Task'
  ClientHeight = 238
  ClientWidth = 487
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOk: TBitBtn
    Left = 323
    Top = 203
    ModalResult = 0
  end
  inherited btnCancel: TBitBtn
    Left = 405
    Top = 203
  end
  inline Scheduler: TfrmSimpleScheduler
    Left = 6
    Top = 72
    Width = 476
    Height = 61
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    inherited gbSchedule: TGroupBox
      Width = 476
      Height = 61
      inherited lblScheduleInfo: TLabel
        Left = 102
        Top = 21
        Width = 366
        Height = 24
      end
      inherited Bevel1: TBevel
        Left = 99
        Top = 17
        Width = 371
        Height = 32
        Shape = bsLeftLine
      end
      inherited btnSchedule: TBitBtn
        Left = 10
        Top = 21
        Width = 82
        Caption = 'Schedule Task'
      end
    end
  end
  object gbFileName: TGroupBox
    Left = 6
    Top = 7
    Width = 476
    Height = 61
    Anchors = [akLeft, akTop, akRight]
    Caption = 'File to Import'
    TabOrder = 3
    DesignSize = (
      476
      61)
    inline FileOpen: TFileOpenLeftBtnFrm
      Left = 4
      Top = 14
      Width = 467
      Height = 37
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      inherited BitBtn1: TBitBtn
        Left = 6
        Width = 83
      end
      inherited edtFile: TEdit
        Left = 97
        Width = 365
      end
      inherited ActionList1: TActionList
        inherited FileOpen1: TFileOpen
          Dialog.Filter = 'import files|*.txt;*.csv'
          Hint = 'Open|Opens an import data file'
        end
      end
    end
  end
  object gbReport: TGroupBox
    Left = 6
    Top = 138
    Width = 476
    Height = 53
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'Report'
    TabOrder = 4
    object chbSendEmail: TCheckBox
      Left = 11
      Top = 22
      Width = 162
      Height = 17
      Caption = 'Send the report when done'
      TabOrder = 0
    end
    object chbAttachUserLog: TCheckBox
      Left = 216
      Top = 22
      Width = 97
      Height = 17
      Caption = 'Attach User log'
      TabOrder = 1
    end
    object chbAttachDevLog: TCheckBox
      Left = 338
      Top = 22
      Width = 125
      Height = 17
      Caption = 'Attach debug log'
      TabOrder = 2
    end
  end
end
