@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoScheduledEDImport.msi 
"%pWiXDir%\candle.exe" .\EvoScheduledEDImport.wxs -wx -out ..\..\..\..\..\Tmp\EvoScheduledEDImport.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoScheduledEDImport.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoScheduledEDImport.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoScheduledEDImport.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoScheduledEDImport%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoScheduledEDImport%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoScheduledEDImport.msi EvoScheduledEDImport%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
