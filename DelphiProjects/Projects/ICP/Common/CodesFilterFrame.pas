unit CodesFilterFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, CheckLst, gdyClasses, gdycommon, gdystrset,
  eefilter, OptionsBaseFrame;

type
  TCodesFilterFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    cbUse: TCheckBox;
    lbStatuses: TCheckListBox;
    procedure cbUseClick(Sender: TObject);
    procedure lbStatusesClickCheck(Sender: TObject);
  private
    FLines: IStr;
    function GetCheckedCodes: TStringSet;
    procedure SetCheckedCodes(codes: TStringSet);
    function IsInitialized: boolean;
    function GetCodesFilter: TCodesFilter;
    procedure SetCodesFilter(const Value: TCodesFilter);
  public
    property CodesFilter: TCodesFilter read GetCodesFilter write SetCodesFilter;

    constructor Create( Owner: TComponent ); override;
    procedure Configure(comboChoices: string; caption: string);

    procedure Clear(enable: boolean);
    procedure Check;
  end;

implementation

uses
  common;

{$R *.dfm}

{ TCodesFilterFrm }

procedure TCodesFilterFrm.cbUseClick(Sender: TObject);
var
  i: integer;
begin
  if not cbUse.Checked then
    for i := 0 to lbStatuses.Count-1 do
      lbStatuses.Checked[i] := false;
//  EnableControl(lbStatuses, cbUse.Checked);
end;

procedure TCodesFilterFrm.Configure(comboChoices: string; caption: string);
var
  i: integer;
begin
  FLines := SplitToLines(comboChoices);
  lbStatuses.Clear;
  for i := 0 to FLines.Count-1 do
    with SplitByChar(FLines.Str[i], #9) do
      lbStatuses.Items.Add(Str[0]);
  GroupBox1.Caption := caption;
end;

function TCodesFilterFrm.GetCheckedCodes: TStringSet;
var
  i: integer;
begin
  SetLength(Result, 0);
  for i := 0 to lbStatuses.Count-1 do
    if lbStatuses.Checked[i] then
      with SplitByChar(FLines.Str[i], #9) do
        SetInclude(Result, Str[1]);
end;

procedure TCodesFilterFrm.SetCheckedCodes(codes: TStringSet);
var
  i: integer;
begin
  for i := 0 to lbStatuses.Count-1 do
    with SplitByChar(FLines.Str[i], #9) do
      lbStatuses.Checked[i] := InSet(Str[1], codes);
end;

function TCodesFilterFrm.IsInitialized: boolean;
begin
  Result := cbUse.Enabled;
end;

constructor TCodesFilterFrm.Create(Owner: TComponent);
begin
  inherited;
  Clear(false);
end;

procedure TCodesFilterFrm.lbStatusesClickCheck(Sender: TObject);
begin
  cbUse.Checked := true;
end;

procedure TCodesFilterFrm.Check;
var
  filter: TCodesFilter;
begin
  filter := CodesFilter;
  if filter.Use and (Length(filter.Codes) = 0) then
    raise Exception.CreateFmt('%s filter is enabled but no %s selected', [GroupBox1.Caption, Plural(AnsiLowerCase(GroupBox1.Caption))]);
end;

procedure TCodesFilterFrm.Clear(enable: boolean);
var
  i: integer;
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    for i := 0 to lbStatuses.Count-1 do
      lbStatuses.Checked[i] := false;
    cbUse.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

function TCodesFilterFrm.GetCodesFilter: TCodesFilter;
begin
  Assert(lbStatuses.Items.Count > 0); //Configure was called
  Assert(IsInitialized);

  Result.Use := cbUse.Checked;
  Result.Codes := GetCheckedCodes;
end;

procedure TCodesFilterFrm.SetCodesFilter(const Value: TCodesFilter);
begin
  Assert(lbStatuses.Items.Count > 0); //Configure was called

  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);

    cbUse.Checked := Value.Use;
    if Value.Use then
      SetCheckedCodes(Value.Codes)
    else
      SetCheckedCodes(nil);
  finally
    FBlockOnChange := false;
  end;
end;

end.
