object EvolutionClCoFrm: TEvolutionClCoFrm
  Left = 0
  Top = 0
  Width = 634
  Height = 377
  TabOrder = 0
  OnResize = FrameResize
  object Splitter1: TSplitter
    Left = 316
    Top = 0
    Height = 377
  end
  inline ClientFrame: TEvolutionDsFrm
    Left = 0
    Top = 0
    Width = 316
    Height = 377
    Align = alLeft
    TabOrder = 0
    inherited Panel1: TPanel
      Caption = 'Clients'
    end
    inherited dgGrid: TReDBGrid
      Height = 352
    end
  end
  inline CompanyFrame: TEvolutionDsFrm
    Left = 319
    Top = 0
    Width = 315
    Height = 377
    Align = alClient
    TabOrder = 1
    inherited Panel1: TPanel
      Width = 315
      Caption = 'Companies'
    end
    inherited dgGrid: TReDBGrid
      Width = 315
      Height = 352
    end
  end
end
