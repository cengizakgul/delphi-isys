object CustomBinderBaseFrm: TCustomBinderBaseFrm
  Left = 0
  Top = 0
  Width = 655
  Height = 460
  TabOrder = 0
  inline BinderFrm1: TBinderFrm
    Left = 0
    Top = 0
    Width = 655
    Height = 460
    Align = alClient
    TabOrder = 0
    inherited evSplitter2: TSplitter
      Top = 196
      Width = 655
    end
    inherited pnltop: TPanel
      Width = 655
      Height = 196
      inherited evSplitter1: TSplitter
        Height = 196
      end
      inherited pnlTopLeft: TPanel
        Height = 196
        inherited dgLeft: TReDBGrid
          Height = 171
        end
      end
      inherited pnlTopRight: TPanel
        Width = 385
        Height = 196
        inherited evPanel4: TPanel
          Width = 385
        end
        inherited dgRight: TReDBGrid
          Width = 385
          Height = 171
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 201
      Width = 655
      inherited pnlbottom: TPanel
        Width = 655
        inherited evPanel5: TPanel
          Width = 655
        end
        inherited dgBottom: TReDBGrid
          Width = 655
        end
      end
      inherited pnlMiddle: TPanel
        Width = 655
      end
    end
  end
end
