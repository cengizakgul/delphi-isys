unit XmlToDataSetUnit;

interface

uses sysutils, classes, db, kbmMemTable, XmlRpcTypes;

function StructToDataSet(const AdsXml: IRpcStruct): TkbmCustomMemTable;

implementation

{**************************************************
  Convert XML-RPC struct to Delphi DataSet
**************************************************}
function StructToDataSet(const AdsXml: IRpcStruct): TkbmCustomMemTable;
var
  i, j: Integer;
  BlobData: TMemoryStream;

//  dd: TddsDataDictionary;
//  ddField: TddsField;
//  ddTable:  TDataDictTable;
  sTableName: string;
  sFieldName: string;
  FieldType: TFieldType;
  fType: Integer;
  fieldSize: Integer;
  fieldRequired: boolean;
  sColNbr: string;
  s: String;

  dsField: TField;
  rowXml: IRpcStruct;
  colXml: IRpcStruct;
  metaXml: IRpcStruct;
  valXml: IRpcStruct;
  dsXml: IRpcStruct;
begin
  dsXML := AdsXML;
  Result := TkbmCustomMemTable.Create(nil);
  try
    Result.AttachMaxCount := 4; //just like TISkbmDataSet
//    if( (dsXml.KeyList.Strings[0] = 'meta') and (dsXml.KeyList.Strings[1] = 'val') ) then

    if ( (dsXml.KeyList.Strings[0] = 'DATASET') and
        (dsXml.Items[0].AsStruct.KeyList.Strings[0] = 'meta') and
        (dsXml.Items[0].AsStruct.KeyList.Strings[1] = 'val') ) then
    begin
dsXml := dsXml.Items[0].AsStruct;
      // get Data Dictionary from the storage
      //dd := ddList.GetDictionary(HandleToConnection(Handle).Version);
      // get meta info and create DS
      metaXml := dsXml.Items[0].AsStruct;
      for i := 0 to metaXML.Count-1 do
      begin
        colXml := metaXml.Items[i].AsStruct;
        sTableName := '';
        sFieldName := '';
        fType := 0;
        fieldSize := 0;
        fieldRequired := false;

        for j := 0 to colXml.Count-1 do
        begin
          if colXml.KeyList.Strings[j] = 'tb' then
            sTableName :=colXml.Items[j].AsString
          else if colXml.KeyList.Strings[j] = 'nm' then
            sFieldName :=colXml.Items[j].AsString
          else if colXml.KeyList.Strings[j] = 'tp' then
            fType := colXml.Items[j].AsInteger
          else if colXml.KeyList.Strings[j] = 'sz' then
            fieldSize :=colXml.Items[j].AsInteger
          else if colXml.KeyList.Strings[j] = 'nl' then
            fieldRequired := colXml.Items[j].AsInteger = 0;
        end; // for - field info

        if (sTableName = '') or (sFieldName = '') then
        begin
          // error: incorrect meta info
          Result.Free;
          Result := nil;
          Exit;
        end;

          FieldType := ftUnknown; // ???
          case fType of
            2004: FieldType := ftBlob;
            12:   FieldType := ftString;
            16:   FieldType := ftBoolean;
            4:    FieldType := ftInteger;
            2:    FieldType := ftFloat;
            91:   FieldType := ftDate;
            93:   FieldType := ftDateTime;  // not TimeStamp !
          end;
            //ddTable := dd.Tables.TableByName(sTableName);
            //ddField := TddsField(ddTable.Fields.FieldByName(sFieldName));
          Result.FieldDefs.Add(sFieldName, FieldType, fieldSize, fieldRequired );
            //break;

      end; // for - colimns

      Result.Active := true;
      // get values and fill out DS
      if dsXml.Items[1].isStruct then // if non-empty DS
      begin
        rowXML := dsXml.Items[1].AsStruct;
        for i := 0 to rowXML.Count-1 do   // rows
        begin
          valXml := rowXml.Items[i].AsStruct;
          Result.Append;
          for j := 0 to valXml.Count-1 do  // columns
          begin
            sColNbr := valXml.KeyList.Strings[j];
            sFieldName := metaXml.Keys[sColNbr].AsStruct.Keys['nm'].AsString;
            //ddField := TddsField(ddTable.Fields.FieldByName(sFieldName));
            //ddField.FieldType;
            //val := valXml.Items[j].AsString; //???

            //Result.Fields[j].Value :=
            dsField := Result.FieldByName(sFieldName);
                if not Assigned(valXml.Items[j]) then
                  dsField.Clear
                else
                  case dsField.DataType of
                    ftString:
                      dsField.Value := valXml.Items[j].AsString;
                    ftBoolean:
                      dsField.Value := valXml.Items[j].AsBoolean;
                    ftDateTime, ftDate, ftTimeStamp:
                      if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                        dsField.Clear
                      else
                        dsField.Value := valXml.Items[j].AsDateTime;
                    ftFloat, ftCurrency:
                      begin
                        if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                          dsField.Clear
                        else
                          dsField.Value := valXml.Items[j].AsFloat;
                      end;
                    ftInteger:
                      begin
                        if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                          dsField.Clear
                        else
                          dsField.Value := valXml.Items[j].AsInteger;
                      end;
                    ftBlob, ftMemo:
                    begin
                      if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                        dsField.Clear
                      else begin
                        BlobData := TMemoryStream.Create;
                        try
                          s := valXml.Items[j].AsBase64Str;
                          BlobData.WriteBuffer(s[1], Length(s));
                          BlobData.Position := 0;
                          TBlobField(dsField).LoadFromStream(BlobData);
                        finally
                          FreeAndNil(BlobData);
                        end;
                      end;
                    end

                  end; // case

          //        ds.Post;
          end; // for - columns
          Result.Post;
        end; // for - rows
      end; // if non-empty DS

    end
    else
    begin
      //error: incorrect dataset structure
      Result.Free;
      Result := nil;
      Exit;
    end;
  except
    Result.Free;
    raise;
  end;
end;

end.
