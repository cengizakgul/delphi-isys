object RateImportOptionFrm: TRateImportOptionFrm
  Left = 0
  Top = 0
  Width = 206
  Height = 79
  TabOrder = 0
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 0
    Width = 201
    Height = 79
    Caption = 'Pay rates'
    ItemIndex = 0
    Items.Strings = (
      'Use Swipeclock rates'
      'Use Evolution D/B/D/T rate override'
      'Use Evolution employee pay rates')
    TabOrder = 0
  end
end
