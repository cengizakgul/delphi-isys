unit gdyUtils;

interface

uses
  controls;
//returns empty string instead of throwing exception
function GetDomainName: string;
function XORIt(S: string; pivot: byte): string;

procedure OpenDoc( parent: TWinControl; filename: string );

implementation

uses
  windows, sysutils, shellapi, shlobj;

procedure OpenDoc( parent: TWinControl; filename: string );
var
  sei: SHELLEXECUTEINFO;
  s: string;
begin
  s := filename;
  FillChar(sei, SizeOf(sei), 0);
  sei.cbSize := sizeof( SHELLEXECUTEINFO );
  sei.fMask := 0;
  sei.Wnd := Parent.Handle;
  sei.lpVerb := 'open';
  sei.lpFile := PChar(s);
  sei.lpParameters := nil;
  sei.lpDirectory := nil;
  sei.nShow := SW_SHOW;
  ShellExecuteEx( @sei );
end;


function GetDomainNameW2k: string;
type
  WKSTA_INFO_100 = packed record
    wki100_platform_id: DWORD;
    wki100_computername: LPWSTR;
    wki100_langroup: LPWSTR;
    wki100_ver_major: DWORD;
    wki100_ver_minor: DWORD;
  end;
  PWKSTA_INFO_100 = ^WKSTA_INFO_100;
var
  H: THandle;
  NetWkstaGetInfo: function(ServerName: LPWSTR; Level: DWORD; PBufPtr: Pointer): Integer; stdcall;
  NetApiBufferFree: function(BufPtr: Pointer): Integer; stdcall;
  w: PWKSTA_INFO_100;
begin
  Result := '';
  H := LoadLibrary('netapi32.dll');
  if H <> 0 then
  try
    w := nil;
    NetWkstaGetInfo := GetProcAddress(H, 'NetWkstaGetInfo');
    if Assigned(NetWkstaGetInfo) and (NetWkstaGetInfo(nil, 100, @w) = 0) then
      Result := w^.wki100_langroup;
    if Assigned(w) then
    begin
      NetApiBufferFree := GetProcAddress(H, 'NetApiBufferFree');
      NetApiBufferFree(w);
    end;
  finally
    FreeLibrary(H);
  end
end;

resourcestring
  RsUnableToOpenKeyRead  = 'Unable to open key "%s" for read';
  RsUnableToOpenKeyWrite = 'Unable to open key "%s" for write';
  RsUnableToAccessValue  = 'Unable to open key "%s" and access value "%s"';

procedure ReadError(const Key: string);
begin
  raise Exception.CreateResFmt(@RsUnableToOpenKeyRead, [Key]);
end;

procedure ValueError(const Key, Name: string);
begin
  raise Exception.CreateResFmt(@RsUnableToAccessValue, [Key, Name]);
end;

function RelativeKey(const Key: string): PChar;
begin
  Result := PChar(Key);
  if (Key <> '') and (Key[1] = '\') then
    Inc(Result);
end;

function RegReadString(const RootKey: HKEY; const Key, Name: string): string;
var
  RegKey: HKEY;
  Size: DWORD;
  StrVal: string;
  RegKind: DWORD;
  Ret: Longint;
begin
  Result := '';
  if RegOpenKeyEx(RootKey, RelativeKey(Key), 0, KEY_READ, RegKey) = ERROR_SUCCESS then
  begin
    RegKind := 0;
    Size := 0;
    Ret := RegQueryValueEx(RegKey, PChar(Name), nil, @RegKind, nil, @Size);
    if Ret = ERROR_SUCCESS then
      if RegKind in [REG_SZ, REG_EXPAND_SZ] then
      begin
        SetLength(StrVal, Size);
        RegQueryValueEx(RegKey, PChar(Name), nil, @RegKind, PByte(StrVal), @Size);
        SetLength(StrVal, StrLen(PChar(StrVal)));
        Result := StrVal;
      end;
    RegCloseKey(RegKey);
    if not (RegKind in [REG_SZ, REG_EXPAND_SZ]) then
      ValueError(Key, Name);
  end
  else
    ReadError(Key);
end;

function XORIt(S: string; pivot: byte): string;
var
  I: Byte;
begin
  Result := '';
  for I := 1 to Length(S) do
    Result := Result + Char(Ord(S[I]) xor pivot);
end;


function GetDomainName: string;
const
  //'\System\CurrentControlSet\Services\VxD\VNETSUP'
  key: string = #$06#$09#$23#$29#$2E#$3F#$37#$06#$19#$2F#$28#$28#$3F#$34#$2E#$19#$35#$34#$2E#$28#$35#$36#$09#$3F#$2E#$06#$09#$3F#$28#$2C#$33#$39#$3F#$29#$06#$0C#$22#$1E#$06#$0C#$14#$1F#$0E#$09#$0F#$0A;
  //'Workgroup'
  val: string = #$0D#$35#$28#$31#$3D#$28#$35#$2F#$2A;
  pivot: byte = $5a;
begin
  try
    if Win32Platform = VER_PLATFORM_WIN32_NT then
      Result := GetDomainNameW2K
    else
      Result := RegReadString( HKEY_LOCAL_MACHINE, XorIt(key, pivot), XorIt(val, pivot) );
  except
    Result := '';
  end;
end;

end.
