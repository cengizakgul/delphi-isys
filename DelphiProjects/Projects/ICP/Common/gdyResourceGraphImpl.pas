unit gdyResourceGraphImpl;
{$INCLUDE gdy.inc}

interface

uses
  gdyResourceManager, gdyResourceGraph, gdystrset;

type
  TResourceDependencyGraph = class(TInterfacedObject, IResourceDependencyGraph)
  private
    FDepDefs: array of TDependencyDef;
    FResources: TStringSet;
    function CreateIter( aName: string ): IResourceIter;
  protected
    {IResourceDependencyGraph}
    function ResourceByName( aName: string ): IResourceIter; //throws ERMNotFound
    function ResourceCount: integer;
    function Resource( Index: integer ): IResourceIter;
  public
    constructor Create( {const aResources: array of string; }const aDepDefs: array of TDependencyDef );
    destructor Destroy; override;
  end;

implementation


type
  TResourceDependencyIter = class(TInterfacedObject, IResourceIter)
  private
    FGraph: IResourceDependencyGraph;
    FGraphInstance: TResourceDependencyGraph;
    FName: string;
    FChilds: array of integer;
    FParents: array of integer;
  protected
    constructor Create( aGraphInst: TResourceDependencyGraph; aName: string );
    destructor Destroy; override;
    {IResourceIter}
    function ChildCount: integer;
    function ChildIter( Index: integer ): IResourceIter; //throws EOutOfRange or somewhat similar
    function Child( Index: integer ): TDependencyDef; //throws EOutOfRange or somewhat similar
    function ParentCount: integer;
    function ParentIter( Index: integer ): IResourceIter; //throws EOutOfRange or somewhat similar
    function Parent( Index: integer ): TDependencyDef; //throws EOutOfRange or somewhat similar
    function Name: string;
  public
  end;

{ TResourceDependencyIter }

constructor TResourceDependencyIter.Create(
  aGraphInst: TResourceDependencyGraph; aName: string);
var
  i: integer;
begin
  FGraphInstance := aGraphInst;
  FGraph := aGraphInst as IResourceDependencyGraph;
  FName := aName;

  for i := 0 to Length(FGraphInstance.FDepDefs)-1 do
  begin
    if FGraphInstance.FDepDefs[i].Name = FName then
    begin
      SetLength( FParents, Length(FParents)+1 );
      FParents[High(FParents)] := i;
    end;
    if FGraphInstance.FDepDefs[i].ParentName = FName then
    begin
      SetLength( FChilds, Length(FChilds)+1 );
      FChilds[High(FChilds)] := i;
    end;
  end
end;

function TResourceDependencyIter.Child(Index: integer): TDependencyDef;
begin
  if (Index < 0)  or (Index > Length(FChilds) - 1) then
    raise ERMInvalidIndex.CreateFmt( 'Child #%d for <$s> doesn''t exist', [Index, FName]);
  Result := FGraphInstance.FDepDefs[FChilds[Index]];
end;

function TResourceDependencyIter.ChildCount: integer;
begin
  Result := Length(FChilds);
end;

function TResourceDependencyIter.ChildIter(
  Index: integer): IResourceIter;
begin
  if (Index < 0)  or (Index > Length(FChilds) - 1) then
    raise ERMInvalidIndex.CreateFmt( 'Child #%d for <%s> doesn''t exist', [Index, FName]);
  Result := FGraphInstance.CreateIter(FGraphInstance.FDepDefs[FChilds[Index]].Name);
end;

function TResourceDependencyIter.Name: string;
begin
  Result := FName;
end;

function TResourceDependencyIter.Parent(Index: integer): TDependencyDef;
begin
  if (Index < 0)  or (Index > Length(FParents) - 1) then
    raise ERMInvalidIndex.CreateFmt( 'Parent #%d for <$s> doesn''t exist', [Index, FName]);
  Result := FGraphInstance.FDepDefs[FParents[Index]];
end;

function TResourceDependencyIter.ParentCount: integer;
begin
  Result := Length(FParents);
end;

function TResourceDependencyIter.ParentIter(
  Index: integer): IResourceIter;
begin
  if (Index < 0)  or (Index > Length(FParents) - 1) then
    raise ERMInvalidIndex.CreateFmt( 'Parent #%d for <$s> doesn''t exist', [Index, FName]);
  Result := FGraphInstance.CreateIter(FGraphInstance.FDepDefs[FParents[Index]].ParentName);
end;

destructor TResourceDependencyIter.Destroy;
begin
  inherited;
end;

{ TResourceDependencyGraph }

constructor TResourceDependencyGraph.Create({const aResources: array of string; }const aDepDefs: array of TDependencyDef);
var
  i: integer;
begin
  SetLength(FDepDefs, Length(aDepDefs));
  for i := low(FDepDefs) to high(FDepDefs) do
  begin
    FDepDefs[i] := aDepDefs[i];
    SetInclude( FResources, FDepDefs[i].Name );
    SetInclude( FResources, FDepDefs[i].ParentName );
  end;
  if InSet('',FResources) then
    raise ERMInvalidResourceName.Create('Resource name cannot be empty string');
//  SetAssign( FResources, aResources )
end;

function TResourceDependencyGraph.CreateIter(
  aName: string): IResourceIter;
begin
  Result := TResourceDependencyIter.Create( Self, aName ) as IResourceIter;
end;

destructor TResourceDependencyGraph.Destroy;
begin
  inherited;
end;

function TResourceDependencyGraph.Resource(
  Index: integer): IResourceIter;
begin
  if (Index < 0)  or (Index > ResourceCount - 1) then
    raise ERMInvalidIndex.CreateFmt( 'Resource #%d doesn''t exist', [Index]);
  Result := CreateIter(FResources[Index]);
end;

function TResourceDependencyGraph.ResourceByName( aName: string ): IResourceIter;
begin
  if not InSet( aName, FResources ) then
    raise ERMNotFound.CreateFmt( 'Resource <%s> not found', [aName]);
  Result := CreateIter( aName );
end;

function TResourceDependencyGraph.ResourceCount: integer;
begin
  Result := Length(FResources);
end;

end.
