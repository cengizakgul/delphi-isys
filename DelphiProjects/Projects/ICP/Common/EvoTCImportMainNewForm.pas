unit EvoTCImportMainNewForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame, 
  EvolutionPrPrBatchFrame, OptionsBaseFrame;

type
  TEvoTCImportMainNewFm = class(TEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    pnlBottom: TPanel;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshCompanySettings: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    pnlPayrollBatch: TPanel;
    PayrollBatchFrame: TEvolutionPrPrBatchFrm;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
  private
  protected
    FEvoData: TEvoData;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}
uses
  isSettings, EeUtilityLoggerViewFrame, dialogs;

procedure TEvoTCImportMainNewFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TEvoTCImportMainNewFm.ConnectToEvoExecute(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      FEvoData.Connect(EvoFrame.Param);
      ConnectToEvo.Caption := 'Refresh data';
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

constructor TEvoTCImportMainNewFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEeUtilityLoggerViewFrm;
  inherited;
  ConnectToEvo.Caption := 'Get data';

  FEvoData := TEvoData.Create(Logger);
  FEvoData.AddDS(TMP_PRDesc);
  FEvoData.AddDS(PR_BATCHDesc);
  CompanyFrame.Init(FEvoData);
  PayrollBatchFrame.Init(FEvoData);
end;

destructor TEvoTCImportMainNewFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

end.
