unit EvolutionMultiplePayrollFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvolutionDSFrame, ExtCtrls, EvolutionCompanyFrame, kbmMemTable,
  evoapiconnection, gdycommonlogger, common, db;

type
  TEvolutionMultiplePayrollFrm = class(TFrame)
    EvoCompany: TEvolutionCompanyFrm;
    Splitter1: TSplitter;
    frmPR: TEvolutionDsFrm;
  private
    FPR: TkbmCustomMemTable;
    FTemp: TPrDefs;
    function GetLogger: ICommonLogger;
    procedure SetLogger(const Value: ICommonLogger);
    function ForPr(ds: TDataSet): boolean;
  public
    procedure FillTables(conn: IEvoAPIConnection; flt: TEvoPayrollFilter);
    procedure ClearTables;
    function CanGetPayrollsDef: boolean;
    function GetPayrollsDef: TEvoPayrollsDef;

    property Logger: ICommonLogger read GetLogger write SetLogger;
  end;

implementation

uses
  gdyRedir, XmlRpcTypes, BinderFrame, dbComp;

{$R *.dfm}

{ TEvolutionMultiplePayrollFrm }

function TEvolutionMultiplePayrollFrm.CanGetPayrollsDef: boolean;
begin
  Result := EvoCompany.CanGetClCo and assigned(FPR) and (FPR.RecordCount > 0);
end;

procedure TEvolutionMultiplePayrollFrm.ClearTables;
begin
  FreeAndNil(FPR);
  EvoCompany.ClearTables;
end;

procedure TEvolutionMultiplePayrollFrm.FillTables(conn: IEvoAPIConnection; flt: TEvoPayrollFilter);
var
  params: IRpcStruct;
begin
  ClearTables;
  EvoCompany.FillTables(conn);

  params := TRpcStruct.Create;
  params.AddItem( 'CHECK_DATE_FROM', DateToStr(flt.FromDate) ); //it works even without DateToStr
  params.AddItem( 'CHECK_DATE_TO', DateToStr(flt.ToDate) );
  FPR := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'qPrList.rwq', params);

  FPR.FieldByName('CHECK_DATE').DisplayLabel := 'Check Date';
  FPR.FieldByName('RUN_NUMBER').DisplayLabel := 'Run Number';
  FPR.FieldByName('PAYROLL_TYPE').DisplayLabel := 'Type';
  FPR.FieldByName('STATUS').DisplayLabel := 'Status';
  FPR.FieldByName('PR_NBR').DisplayLabel := 'Internal Pr#';
  FPR.FieldByName('CL_NBR').Visible := false;
  FPR.FieldByName('CO_NBR').Visible := false;

  frmPR.ds.DataSet := FPR;

  FPR.MasterFields := 'CL_NBR;CO_NBR';
  FPR.DetailFields := 'CL_NBR;CO_NBR';
  FPR.MasterSource := EvoCompany.frmCO.ds;

  FPR.First;
end;

function TEvolutionMultiplePayrollFrm.GetLogger: ICommonLogger;
begin
  Result := EvoCompany.Logger;
end;

function TEvolutionMultiplePayrollFrm.ForPr(ds: TDataSet): boolean;
begin
  SetLength(FTemp, Length(FTemp)+1);
  FTemp[High(FTemp)].CheckDate := ds.FieldByName('CHECK_DATE').AsDateTime;
  FTemp[High(FTemp)].Run := ds['RUN_NUMBER'];
  FTemp[High(FTemp)].PrNbr := ds['PR_NBR'];
  Result := true;
end;

function TEvolutionMultiplePayrollFrm.GetPayrollsDef: TEvoPayrollsDef;
begin
  Assert(CanGetPayrollsDef);
  Result.Company := EvoCompany.GetClCo;
  SetLength(FTemp, 0);
  ForSelected(frmPr.dgGrid, ForPr,[fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty] );
  Result.PrNbrs := FTemp;
end;

procedure TEvolutionMultiplePayrollFrm.SetLogger(const Value: ICommonLogger);
begin
  EvoCompany.Logger := Value;
end;

end.
