inherited EvoAPIConnectionParamFrm: TEvoAPIConnectionParamFrm
  Width = 390
  Height = 106
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 390
    Height = 106
    Align = alClient
    Caption = 'Evolution'
    TabOrder = 0
    object lblAPIServer: TLabel
      Left = 16
      Top = 24
      Width = 96
      Height = 13
      Caption = 'API adapter address'
    end
    object lblUserName: TLabel
      Left = 16
      Top = 48
      Width = 93
      Height = 13
      Caption = 'Evolution username'
    end
    object lblPassword: TLabel
      Left = 16
      Top = 72
      Width = 92
      Height = 13
      Caption = 'Evolution password'
    end
    object SpeedButton1: TSpeedButton
      Left = 273
      Top = 22
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333F797F3333333333F737373FF333333BFB999BFB
        33333337737773773F3333BFBF797FBFB33333733337333373F33BFBFBFBFBFB
        FB3337F33333F33337F33FBFBFB9BFBFBF3337333337F333373FFBFBFBF97BFB
        FBF37F333337FF33337FBFBFBFB99FBFBFB37F3333377FF3337FFBFBFBFB99FB
        FBF37F33333377FF337FBFBF77BF799FBFB37F333FF3377F337FFBFB99FB799B
        FBF373F377F3377F33733FBF997F799FBF3337F377FFF77337F33BFBF99999FB
        FB33373F37777733373333BFBF999FBFB3333373FF77733F7333333BFBFBFBFB
        3333333773FFFF77333333333FBFBF3333333333377777333333}
      NumGlyphs = 2
      OnClick = SpeedButton1Click
    end
    object APIServerEdit: TEdit
      Left = 143
      Top = 22
      Width = 124
      Height = 21
      TabOrder = 0
    end
    object PasswordEdit: TPasswordEdit
      Left = 143
      Top = 72
      Width = 124
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object UserNameEdit: TEdit
      Left = 143
      Top = 47
      Width = 124
      Height = 21
      TabOrder = 1
    end
    object cbSavePassword: TCheckBox
      Left = 271
      Top = 74
      Width = 100
      Height = 17
      Caption = 'Save password'
      TabOrder = 3
    end
  end
end
