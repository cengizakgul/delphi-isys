unit tcconnectionbase;

interface

uses
  gdyCommonLogger, xmlintf, InvokeRegistry, classes, rio, SOAPHTTPTrans;

type
  TTCConnectionBase = class(TInterfacedObject)
  public
    constructor Create(logger: ICommonLogger);
  protected
    FLogger: ICommonLogger;

    function ToXMLDoc(ws: widestring; name: string): IXMLDocument; virtual;
    procedure AttachLogger(intf: IInvokable);
    procedure SetHTTPTimeouts(intf: IInvokable; timeout: integer);

  protected
    function CheckXML(doc: IXMLDocument): IXMLDocument; virtual; abstract;
  private
    FTimeout: integer;
    procedure HandleBeforeExecute(const MethodName: string; var SOAPRequest: InvString);
    procedure HandleAfterExecute(const MethodName: string; SOAPResponse: TStream);
    procedure HandleBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
  end;

implementation

uses
  common, gdyRedir, xmldoc, gdycommon, sysutils, SOAPHTTPClient, wininet;

function TTCConnectionBase.ToXMLDoc(ws: widestring; name: string): IXMLDocument;
begin
  FLogger.LogDebug(name + ' result', UTF8Encode(ws));
{$ifndef FINAL_RELEASE}
  StringToFile( UTF8Encode(ws), Redirection.GetDirectory(sDumpDirAlias) + name + '.xml');
{$endif}
  if trim(ws) <> '' then
    Result := CheckXML( LoadXMLData(ws) )
  else
  begin
//    FLogger.LogWarningFmt('Got empty xml document from %s', [name]);
    Result := CheckXML( NewXMLDocument );
  end;
  Result.Options := Result.Options - [doNodeAutoCreate] + [doNodeAutoIndent];
end;

procedure TTCConnectionBase.HandleAfterExecute(const MethodName: string; SOAPResponse: TStream);
begin
  try //I use try..except here because of StreamToString which (in theory) can throw an exception
    SOAPResponse.Position := 0;
    FLogger.LogDebug( MethodName + ' response', StreamToString(SOAPResponse) ); //!!encoding
{$ifndef FINAL_RELEASE}
    SOAPResponse.Position := 0;
    StreamToFile(SOAPResponse, Redirection.GetDirectory(sDumpDirAlias) + MethodName + '-response.xml');
{$endif}
    SOAPResponse.Position := 0;
  except
    FLogger.StopException;
    SOAPResponse.Position := 0;
  end;
end;

procedure TTCConnectionBase.HandleBeforeExecute(const MethodName: string; var SOAPRequest: InvString);
begin
  FLogger.LogDebug( MethodName + ' request', UTF8Encode(SOAPRequest) );
{$ifndef FINAL_RELEASE}
  StringToFile( UTF8Encode(SOAPRequest), Redirection.GetDirectory(sDumpDirAlias) + MethodName + '-request.xml' );
{$endif}
  Assert(FTimeOut <> -1);
  InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@FTimeOut), SizeOf(FTimeOut));
  InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@FTimeOut), SizeOf(FTimeOut));
  InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@FTimeOut), SizeOf(FTimeOut));
end;

constructor TTCConnectionBase.Create(logger: ICommonLogger);
begin
  Flogger := logger;
  FTimeOut := -1;
end;

procedure TTCConnectionBase.AttachLogger(intf: IInvokable);
begin
  (intf as IRIOAccess).RIO.OnBeforeExecute := HandleBeforeExecute;
  (intf as IRIOAccess).RIO.OnAfterExecute := HandleAfterExecute;
end;

procedure TTCConnectionBase.SetHTTPTimeouts(intf: IInvokable; timeout: integer);
begin
  //http://shenoyatwork.blogspot.com/2006/11/setting-timeouts-in-delphi-soap.html
  FTimeout := timeout;
  ((intf as IRIOAccess).RIO as THTTPRIO).HTTPWebNode.OnBeforePost := HandleBeforePost;
end;

procedure TTCConnectionBase.HandleBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
begin
  Assert(FTimeOut <> -1);
  //I don't check the result codes because these call sometimes return an error while supposedly working allright
  InternetSetOption(Data, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@FTimeOut), SizeOf(FTimeOut));
  InternetSetOption(Data, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@FTimeOut), SizeOf(FTimeOut));
end;

end.
