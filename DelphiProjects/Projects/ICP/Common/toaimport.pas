unit toaimport;

interface

uses
  evoapiconnection, evoapiconnectionutils {TOAStart}, common {TEvoCompanyDef},
  gdyCommonLogger, classes;

type
  TTOADataRec = record
    Accrued: double;
    Used: double;
    TypeDescription: string;
  end;

  TTOADataRecs = array of TTOADataRec;

  TEETOADataRec = record
    EECode: string;
    Data: TTOADataRecs;
  end;

  TEETOADataRecs = array of TEETOADataRec;

  TTOAImportData = record
    Recs: TEETOADataRecs;
    Errors: integer;
  end;

function ImportTOA(recs: TEETOADataRecs; company: TEvoCompanyDef; evox: IEvoXImportExecutor; Logger: ICommonLogger): TTOAStat;

implementation

uses
  sysutils, EvoWaitForm, gdycommon, gdyredir;

function BuildImportFile(recs: TEETOADataRecs; company: TEvoCompanyDef): TEvoXImportInputFiles;
var
  i, j: integer;
  cur: TTOADataRec;
begin
  SetLength(Result, 1);
  Result[0].Filename := 'TOAFromAnIntegrationApp.csv';
  Result[0].Filedata := '"CUSTOM_COMPANY_NUMBER", "CUSTOM_EMPLOYEE_NUMBER", "TOA_TYPE", "CURRENT_ACCRUED", "CURRENT_USED"'#13#10;
  for i := 0 to high(recs) do
    for j := 0 to high(recs[i].Data) do
    begin
      cur := recs[i].Data[j];
      Result[0].Filedata := Result[0].Filedata + Format('"%s","%s","%s","%f","%f"'#13#10, [trim(company.CUSTOM_COMPANY_NUMBER), trim(recs[i].EECode), cur.TypeDescription, cur.Accrued, cur.Used]);
    end;
end;

function ImportTOA(recs: TEETOADataRecs; company: TEvoCompanyDef; evox: IEvoXImportExecutor; Logger: ICommonLogger): TTOAStat;
var
  res: IEvoXImportResultsWrapper;
begin
  Result := BuildEmptyTOAStat;
  Logger.LogEntry('Importing TOA records into Evolution');
  try
    try
      res := evox.RunEvoXImport(BuildImportFile(recs, company), FileToString(Redirection.GetFilename(sTOAEvoXMapFileAlias)), 'TOA' );
      Assert(res.Count = 1);
      Result.Updated := res.Result[0].ImportedRowsCount;
      Result.Errors := res.Result[0].ErrorCount;
      LogEvoXImportMessages(Logger, res.Result[0].Messages);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.
