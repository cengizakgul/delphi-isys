unit UtilityLoggerViewFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyCommonLoggerView, gdyLoggerRichView, ExtCtrls, common,
  comctrls, gdyLoggerImpl, gdySendMailLoggerView;

type
  TUtilityLoggerViewFrm = class(TSendMailLoggerViewFrame)
  private
    procedure HandleMessageLogged(item: TListItem; aLogState: TLogState);
  public
    constructor Create( Owner: TComponent ); override;
  end;

implementation

{$R *.dfm}

{ TUtilityLoggerViewFrm }

constructor TUtilityLoggerViewFrm.Create(Owner: TComponent);
begin
  inherited;
  LoggerRichView.OnMessageLogged := HandleMessageLogged;
end;

procedure TUtilityLoggerViewFrm.HandleMessageLogged(item: TListItem; aLogState: TLogState);
begin
  item.Caption := MakePrefixedMessage(aLogState, sCtxComponent);
end;

initialization

  RegisterClass(TUtilityLoggerViewFrm);

end.
