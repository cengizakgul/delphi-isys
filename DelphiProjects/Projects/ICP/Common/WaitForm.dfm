object WaitFm: TWaitFm
  Left = 464
  Top = 427
  Width = 300
  Height = 90
  BorderIcons = []
  Caption = 'WaitFm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblMessage: TPanel
    Left = 0
    Top = 0
    Width = 292
    Height = 56
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Please wait'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object Timer1: TTimer
    Interval = 1
    OnTimer = Timer1Timer
    Left = 56
    Top = 32
  end
end
