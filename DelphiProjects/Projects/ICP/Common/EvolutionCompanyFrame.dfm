object EvolutionCompanyFrm: TEvolutionCompanyFrm
  Left = 0
  Top = 0
  Width = 636
  Height = 350
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 337
    Top = 0
    Height = 350
  end
  inline frmCL: TEvolutionDsFrm
    Left = 0
    Top = 0
    Width = 337
    Height = 350
    Align = alLeft
    TabOrder = 0
    inherited Panel1: TPanel
      Width = 337
      Caption = 'Clients'
    end
    inherited dgGrid: TReDBGrid
      Width = 337
      Height = 325
    end
  end
  inline frmCO: TEvolutionDsFrm
    Left = 340
    Top = 0
    Width = 296
    Height = 350
    Align = alClient
    TabOrder = 1
    inherited Panel1: TPanel
      Width = 296
      Caption = 'Companies'
    end
    inherited dgGrid: TReDBGrid
      Width = 296
      Height = 325
    end
  end
end
