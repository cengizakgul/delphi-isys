object EvoPayrollFilterFrm: TEvoPayrollFilterFrm
  Left = 0
  Top = 0
  Width = 314
  Height = 60
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 313
    Height = 60
    Caption = 'Check Date Period'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 28
      Width = 23
      Height = 13
      Caption = 'From'
    end
    object Label2: TLabel
      Left = 168
      Top = 28
      Width = 13
      Height = 13
      Caption = 'To'
    end
    object dtFrom: TDateTimePicker
      Left = 48
      Top = 24
      Width = 105
      Height = 21
      Date = 40010.865560509250000000
      Time = 40010.865560509250000000
      TabOrder = 0
    end
    object dtTo: TDateTimePicker
      Left = 192
      Top = 24
      Width = 105
      Height = 21
      Date = 40010.865560509250000000
      Time = 40010.865560509250000000
      TabOrder = 1
    end
  end
end
