{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

unit scheduledTask;

interface

uses
  isSettings, gdyCommonLogger, classes, gdyClasses, common;

type
  TScheduledTaskFiles = record
    StatusFileName: string;
    UserHtmlLogFilename: string;
    DebugTxtLogFilename: string; //only need this because the log archive is password protected and I am too lazy to enter password each time
    LogArchiveFileName: string;
  end;

  TScheduledTaskResultStatus = (statusOk = 0, statusWarning, statusError, statusSchedulerError, statusRunning = 256, statusInvalid = 257);

  TScheduledTaskResultStatusRec = record
    Code: TScheduledTaskResultStatus;
    Message: string;
  end;

  TScheduledTaskResult = record
    Folder: string;
    When: TDateTime;
    Status: TScheduledTaskResultStatusRec;
  end;

  TScheduledTaskResults = array of TScheduledTaskResult;

  IScheduledTask = interface
['{171FCEF0-5EF2-4555-90F9-08FC4BB36558}']
    function ParamSettings: IisSettings;
    function Name: string;
    function UserFriendlyName: string;
    function GUID: string;
    function Company: TEvoCompanyDef;
    function NewSession: TScheduledTaskFiles;
    function GetTaskResults: TScheduledTaskResults;
    function GetTaskResultFiles(folder: string): TScheduledTaskFiles;
    function Dump: string;
  end;

  ITaskAdapter = interface
['{B55CB798-E47E-47F3-B914-5C46DA8CBA32}']
    procedure Execute(logger: ICommonLogger; task: IScheduledTask);
    function Describe(logger: ICommonLogger; task: IScheduledTask): string;
  end;

  TTaskAdapterBase = class(TInterfacedObject, ITaskAdapter)
  protected
    FLogger: ICommonLogger;
    FTask: IScheduledTask;
  private
    function GetMethod(aMethodName: string; out method: TMethod): boolean;
    procedure ResolveMethod(aMethodName: string; out method: TMethod);
    {ITaskAdapter}
    procedure Execute(logger: ICommonLogger; task: IScheduledTask);
    function Describe(logger: ICommonLogger; task: IScheduledTask): string;
  end;


function ExecuteScheduledTask(guid: string; exec: ITaskAdapter; emailWarnings: boolean = true; overrideEmail: boolean = false; userLogByMail: boolean = false; emailAlways: boolean = false): TScheduledTaskResultStatus;

function NewTask(name: string; aUserFriendlyName: string; company: TEvoCompanyDef): IScheduledTask;
function LoadTask(guid: string): IScheduledTask;
procedure DeleteTask(guid: string);

function GetTaskGUIDs(logger: ICommonLogger): IStr;
function GetTaskGUIDsByNameAndCompany(logger: ICommonLogger; name: string; companyCode: string): IStr;

function StatusToString(status: TScheduledTaskResultStatus): string;

implementation

uses
  windows, gdyRedir, sysutils, comobj, gdyLoggerImpl, gdyCommon, gdyLogger,
  gdyLogWriters, NewSevenZip, smtpConfigFrame, forms, jclFileUtils;

type
  TTaskLoggerKeeper = class
  private
    FStatefulLogger: IStatefulLogger;
    FLogger: ICommonLogger;
    Fses: TScheduledTaskFiles;

    FUserLog: TStringStream;
    FUserLogOutputWriter: ILoggerEventSink;

    FMailUserLog: TStringStream;
    FMailUserLogOutputWriter: ILoggerEventSink;

    FDebugLogOutput: IFileLogOutput;
    FDebugLogOutputWriter: ILoggerEventSink;

    FStatWriter: ILoggerEventSink;
    function SaveArchiveTo(ses: TScheduledTaskFiles): string;
  public
    constructor Create(logheader: string; ses: TScheduledTaskFiles);
    destructor Destroy; override;
    procedure Stop;
    procedure Save;
    function LogStat: ILogStat;
    property Logger: ICommonLogger read FLogger;
  end;

procedure SendErrorNotificationEmail(cap, msg, overrideEmail: string; emailAlways: boolean = False);
var
  cfg: TSmtpConfig;
begin
  try
    cfg := LoadSmtpConfig(AppSettings, '');
    if overrideEmail <> '' then
      cfg.AddressTo := overrideEmail;
    if cfg.Use or emailAlways then
      sendmail( cfg,
         Format('%s - %s', [Application.Title, cap]),
         Format('This message was automatically sent by %s.'#13#10'%s', [Application.Title, msg]), '' );
  except
    //!! what can I do?
  end;
end;

{ TTaskAdapterBase }

type
  TTaskMethod = procedure of object;
  TTaskStringFunction = function: string of object;

function TTaskAdapterBase.Describe(logger: ICommonLogger;
  task: IScheduledTask): string;
var
  m: TTaskStringFunction;
begin
  FLogger := logger;
  FTask := task;
  FLogger.LogEntry( Format('Describing task "%s"', [FTask.UserFriendlyName]) );
  try
    try
      FLogger.LogDebug( Format('Task name: %s, guid: %s', [FTask.Name, FTask.GUID]) );
      ResolveMethod(FTask.Name + '_Describe', TMethod(m));
      Result := m;
    except
      FLogger.PassthroughException;
    end
  finally
    FLogger.LogExit;
  end

end;

procedure TTaskAdapterBase.Execute(logger: ICommonLogger; task: IScheduledTask);
var
  m: TTaskMethod;
begin
  FLogger := logger;
  FTask := task;
  FLogger.LogEntry( Format('Executing task "%s"', [FTask.UserFriendlyName]) );
  try
    try
      FLogger.LogDebug( Format('Task name: %s, guid: %s', [FTask.Name, FTask.GUID]) );
      ResolveMethod(FTask.Name + '_Execute', TMethod(m));
      m;
    except
      FLogger.PassthroughException;
    end
  finally
    FLogger.LogExit;
  end
end;

function TTaskAdapterBase.GetMethod(aMethodName: string;
  out method: TMethod): boolean;
begin
  //getmethodprop
  method.Data := Self;
  method.Code := MethodAddress( aMethodName );
  Result := assigned( method.Code );
end;

procedure TTaskAdapterBase.ResolveMethod(aMethodName: string; out method: TMethod);
begin
  if not GetMethod( aMethodName, method ) then
    raise Exception.CreateFmt( 'Cannot find method <%s> in <%s>',[aMethodName, ClassName]);
end;

type
  TScheduledTask = class(TInterfacedObject, IScheduledTask)
  private
    FRootSettings: IisSettings;
    FParamSettings: IisSettings;
    FName: string;
    FUserFriendlyName: string;
    FGUID: string;
    FCompany: TEvoCompanyDef;

    FStreamForLocking: TStream;

    function TaskDir: string;
    function TaskIni: string;
    {IScheduledTask}
    function ParamSettings: IisSettings;
    function Name: string;
    function UserFriendlyName: string;
    function GUID: string;
    function Company: TEvoCompanyDef;
    function NewSession: TScheduledTaskFiles;
    function GetTaskResults: TScheduledTaskResults;
    function GetTaskResultFiles(folder: string): TScheduledTaskFiles;
    function Dump: string;

    class function TasksDir: string;
    class function TaskDirByGUID(guid: string): string;
    class function TaskImmutableIniByGUID(guid: string): IisSettings;
  public
    constructor CreateNew(aname: string; aUserFriendlyName: string; aCompany: TEvoCompanyDef);
    constructor CreateFromGUID(guid: string; denyWrite: boolean);
    destructor Destroy; override;
  end;

function NewTask(name: string; aUserFriendlyName: string; company: TEvoCompanyDef): IScheduledTask;
begin
  Result := TScheduledTask.CreateNew(name, aUserFriendlyName, company);
end;

function LoadTask(guid: string): IScheduledTask;
begin
  Result := TScheduledTask.CreateFromGUID(guid, false);
end;

procedure DeleteTask(guid: string);
begin
  JclFileUtils.DelTree( TScheduledTask.TaskDirByGUID(guid) );
end;

{ TScheduledTask }

constructor TScheduledTask.CreateFromGUID(guid: string; denyWrite: boolean);
begin
  FGUID := guid;
  if not DirectoryExists(TaskDir) then
    raise Exception.CreateFmt('Folder %s doesn''t exist', [TaskDir]);
  if not FileExists(TaskIni) then
    raise Exception.CreateFmt('File %s doesn''t exist', [TaskIni]);

  if denyWrite then
    FStreamForLocking := TFileStream.Create(TaskIni, fmOpenRead or fmShareDenyWrite); //don't want anybody to write while the task is executing

  FRootSettings := TisSettingsINI.Create(TaskIni, 'General');
  if FRootSettings.AsInteger['Version'] < 2 then
    raise Exception.CreateFmt('Task format version %d is not supported anymore', [FRootSettings.AsInteger['Version']]);
  if FRootSettings.AsInteger['Version'] > 2 then
    raise Exception.CreateFmt('Task format version %d is unknown', [FRootSettings.AsInteger['Version']]);

  FName := FRootSettings.AsString['Name'];
  if trim(FName) = '' then
    raise Exception.CreateFmt('Error in %s, task name is empty', [TaskIni]);
  FUserFriendlyName := FRootSettings.AsString['UserFriendlyName'];
  if trim(FUserFriendlyName) = '' then
    raise Exception.CreateFmt('Error in %s, user-friendly task name is empty', [TaskIni]);

  FCompany := LoadEvoCompanyDef(FRootSettings, '');
  FParamSettings := TisSettingsINI.Create(TaskIni, 'Params' );
end;

constructor TScheduledTask.CreateNew(aname: string; aUserFriendlyName: string; aCompany: TEvoCompanyDef);
var
  ImmutableIni: IisSettings;
begin
  FName := aname;
  FUserFriendlyName := aUserFriendlyName;
  FCompany := aCompany;
  FGUID := CreateClassID;
  ForceDirectories( WithTrailingSlash(TaskDir) + 'Results' );
  FRootSettings := TisSettingsINI.Create(TaskIni, 'General');
  FRootSettings.AsString['Name'] := FName;
  FRootSettings.AsString['UserFriendlyName'] := FUserFriendlyName;
  SaveEvoCompanyDef(FCompany, FRootSettings, '');
  FRootSettings.AsInteger['Version'] := 2;

  FParamSettings := TisSettingsINI.Create(TaskIni, 'Params' );

  ImmutableIni := TaskImmutableIniByGUID(GUID);
  ImmutableIni.AsString['Name'] := FName;
  SaveEvoCompanyDef(FCompany, ImmutableIni, '');
end;

function TScheduledTask.TaskDir: string;
begin
  Result := TaskDirByGUID(FGUID);
end;

function TScheduledTask.TaskIni: string;
begin
  Result := TaskDir + '\task.ini';
end;

function TScheduledTask.GUID: string;
begin
  Result := FGUID;
end;

function TScheduledTask.Name: string;
begin
  Result := FName;
end;

function TScheduledTask.ParamSettings: IisSettings;
begin
  Result := FParamSettings;
end;

function FolderToDateTime(s: string): TDateTime;
var
  fs: TFormatSettings;
begin
//  GetLocaleFormatSettings( ,fs);
  fs.DateSeparator := '-';
  fs.ShortDateFormat := 'yyyy-mm-dd';
  Result := StrToDate(copy(s, 1, 10), fs) +
            EncodeTime( strtoint(copy(s, 12, 2)), strtoint(copy(s, 14, 2)), StrToIntDef(copy(s, 16, 2), 0), 0);
end;

function TScheduledTask.NewSession: TScheduledTaskFiles;
var
  dir: string;
begin
  dir := CreateSomeDirectory( TaskDir + '\Results\' + FormatDateTime('yyyy-mm-dd-hhnnss', Now));
  Result := GetTaskResultFiles( ExtractFileName(dir) ); // get timestamp with optional (x) at the end
end;

function TScheduledTask.UserFriendlyName: string;
begin
  Result := FUserFriendlyName;
end;

class function TScheduledTask.TasksDir: string;
begin
  Result := Redirection.GetDirectory(sSchedulerDataDirAlias) + 'Tasks';
end;

class function TScheduledTask.TaskDirByGUID(guid: string): string;
begin
  Result := TScheduledTask.TasksDir + '\' + guid;
end;


{ TTaskLoggerKeeper }

//like TCommonLoggerKeeper.Create
constructor TTaskLoggerKeeper.Create(logheader: string; ses: TScheduledTaskFiles);
begin
  FMailUserLog := TStringStream.Create('');
  FUserLog := TStringStream.Create('');
  Fses := ses;

  FStatefulLogger := CreateStatefulLogger;
  FLogger := CreateCommonLogger( CreateExceptionLogger( FStatefulLogger as ILogger ) );

  FUserLogOutputWriter := THtmlUserLogWriter.Create(TStreamLogOutput.Create(FUserLog), Redirection.GetDirectory(sHtmlTemplatesDirAlias), logheader );
  FStatefulLogger.Advise(FUserLogOutputWriter);

  FMailUserLogOutputWriter := TPlainTextUserLogWriter.Create( TStreamLogOutput.Create(FMailUserLog) );
  FStatefulLogger.Advise(FMailUserLogOutputWriter);

  FDebugLogOutput := TFileLogOutput.Create(Fses.DebugTxtLogFilename);
  FDebugLogOutputWriter := TPlainTextDevLogWriter.Create( FDebugLogOutput as ILogOutput );
  FStatefulLogger.Advise(FDebugLogOutputWriter);

  FStatWriter := TStatWriter.Create;
  FStatefulLogger.Advise(FStatWriter);
end;

destructor TTaskLoggerKeeper.Destroy;
begin
  Assert(FUserLogOutputWriter = nil);
  Assert(FMailUserLogOutputWriter = nil);
  Assert(FDebugLogOutputWriter = nil);
  FreeAndNil(FUserLog);
  FreeAndNil(FMailUserLog);
//  FreeAndNil(FDebugLog);
  inherited;
end;

procedure TTaskLoggerKeeper.Stop;
begin
  Logger.logDebug('Stopping to write to user log');
  FStatefulLogger.UnAdvise(FUserLogOutputWriter);
  FStatefulLogger.UnAdvise(FMailUserLogOutputWriter);
  FUserLogOutputWriter := nil;
  FMailUserLogOutputWriter := nil;
  Logger.logDebug('Stopped to write to user log');

  Logger.logDebug('Stopping to write to debug log');
  FStatefulLogger.UnAdvise(FDebugLogOutputWriter);
  FDebugLogOutputWriter := nil;
//  Logger.logDebug('Stopped to write to debug log'); //goes nowhere

  FStatefulLogger.UnAdvise(FStatWriter);
end;

procedure TTaskLoggerKeeper.Save;
begin
  Assert(FUserLogOutputWriter = nil);
  Assert(FDebugLogOutputWriter = nil);

  SaveHtmlLogFileTo(FUserLog.DataString, Fses.UserHtmlLogFilename);

//{$ifndef FINAL_RELEASE}
//  StringToFile(FDebugLog.DataString, Fses.DebugTxtLogFilename);
//{$endif}
  SaveArchiveTo(Fses);
end;

function TTaskLoggerKeeper.SaveArchiveTo(ses: TScheduledTaskFiles): string;
var
  Arch: I7zOutArchive;
  imgdir: string;
  destimgdir: string;
  htmllogfn: string;
begin
  Arch := CreateOutArchive(CLSID_CFormat7z, Redirection.GetFilename(sSevenZipDllAlias));
  SetCompressionLevel(Arch, 5);
  Arch.SetPassword(GetISystemsPassword);

  FDebugLogOutput.CloseFileTemporarily;
  Arch.AddFile(Fses.DebugTxtLogFilename, ExtractFileName(Fses.DebugTxtLogFilename) );
//  Arch.AddStream(FDebugLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, ExtractFileName(ses.DebugTxtLogFilename), false, false);

  htmllogfn := ExtractFileName(ses.UserHtmlLogFilename);
  Arch.AddStream(FUserLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, htmllogfn, false, false);

  imgdir := Redirection.GetDirectory(sHtmlTemplatesDirAlias) + 'Log_files';
  Assert(AnsiSameText(ChangeFileExt(ExtractFileName(htmllogfn), ''), 'Log'));
  destimgdir := ChangeFileExt(htmllogfn, '') + '_files';

  Arch.AddFile(imgdir + '\error.png', destimgdir + '\error.png');
  Arch.AddFile(imgdir + '\warning.png', destimgdir + '\warning.png');
  Arch.AddFile(imgdir + '\info.png', destimgdir + '\info.png');

  ForceDirectories(ExtractFilePath(ses.LogArchiveFileName));
  Arch.SaveToFile(ses.LogArchiveFileName);
end;

function TTaskLoggerKeeper.LogStat: ILogStat;
begin
  Result := FStatWriter as ILogStat;
end;

function GetAllSubDirs(folder: string): IStr;
var
  SR: TSearchRec;
  isFound: Boolean;
begin
  Result := EmptyIStr;
  isFound := FindFirst( WithTrailingSlash(folder) + '*.*', faDirectory, SR) = 0;
  try
    while IsFound do
    begin
      if DirectoryExists(WithTrailingSlash(folder) + SR.Name) and (copy(SR.Name,1,1) <> '.') then
        Result.Add(SR.Name); //guid here
      IsFound := FindNext(SR) = 0;
    end;
  finally
    FindClose(SR);
  end;
end;

function GetTaskGUIDs(logger: ICommonLogger): IStr;
var
  folder: string;
begin
  Logger.LogEntry('Enumerating subfolders in application''s task folder');
  try
    try
      folder := TScheduledTask.TasksDir;
      Logger.LogContextItem('Tasks folder', folder);
      Result := GetAllSubDirs(folder);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function GetTaskGUIDsByNameAndCompany(logger: ICommonLogger; name: string; companyCode: string): IStr;
var
  allTasks: IStr;
  i: integer;
  immutableIni: IisSettings;
begin
  Logger.LogEntry('Looking for a task by name and company');
  try
    try
      Logger.LogContextItem('Task name', name);
      Logger.LogContextItem('Company#', companyCode);
      Result := EmptyIStr;
      allTasks := GetTaskGUIDs(logger);
      for i := 0 to allTasks.Count-1 do
      begin
        immutableIni := TScheduledTask.TaskImmutableIniByGUID(allTasks.Str[i]);
        if (name = immutableIni.AsString['Name']) and (LoadEvoCompanyDef(immutableIni,'').CUSTOM_COMPANY_NUMBER = companyCode) then
        begin
          Result.Add(allTasks.Str[i]);
        end;
      end
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function ReadStatusFromFile(fn: string): TScheduledTaskResultStatusRec;
var
  status: IStr;
begin
  Result.Message := '';
  try
    status := SplitToLines( FileToString(fn) );
    if (status.Count > 0) and IsInteger(status.Str[0]) and (strtoint(status.Str[0]) in [ord(statusOk) .. ord(statusSchedulerError)]) then
    begin
      Result.Code := TScheduledTaskResultStatus(strtoint(status.Str[0]));
      if status.Count > 1 then //support old status files that have only status code
        Result.Message := status.Str[1];
    end
    else
    begin
      Result.Code := statusInvalid; // need logger here!!!
      Result.Message := Format('Unexpected task result file format (%s)', [fn]);
    end
  except
    if GetLastError = ERROR_SHARING_VIOLATION then
      Result.Code := statusRunning
    else
    begin
      Result.Code := statusInvalid;  // need logger here!!!
      Result.Message := Format('Cannot read task result file: %s (%s)', [SysErrorMessage(GetLastError), fn]);
    end
  end;
end;

function TScheduledTask.GetTaskResults: TScheduledTaskResults;
var
  folders: IStr;
  i: integer;
begin
  folders := GetAllSubDirs(TaskDir + '\Results');
  SetLength(Result, folders.Count);
  for i := 0 to folders.Count-1 do
  begin
    Result[i].Folder := folders.Str[i];
    Result[i].When := FolderToDateTime(folders.Str[i]);
    Result[i].Status := ReadStatusFromFile( GetTaskResultFiles(folders.Str[i]).StatusFileName );
  end;
end;

function TScheduledTask.Company: TEvoCompanyDef;
begin
  Result := FCompany;
end;

function TScheduledTask.GetTaskResultFiles(folder: string): TScheduledTaskFiles;
var
  dir: string;
  base: string;
begin
  dir := TaskDir + '\Results\' + folder;
  Result.UserHtmlLogFilename := dir + '\User\Log.html';
  Result.DebugTxtLogFilename := dir + '\task-debug.log';
  base := ChangeFileExt(GetAppFilename,'');
  Result.LogArchiveFileName := Format('%s\To Send\%s-task-debug-log-%s.7z', [dir, base, folder]);
  Result.StatusFileName := dir + '\status';
end;

function StatusToString(status: TScheduledTaskResultStatus): string;
begin
  case status of
    statusOk: Result := 'OK';
    statusWarning: Result := 'Warning';
    statusError: Result := 'Error';
    statusSchedulerError: Result := 'Scheduler error';
    statusRunning: Result := 'Running';
    statusInvalid: Result := 'Invalid';
  else
    Assert(false);
  end;
end;

destructor TScheduledTask.Destroy;
begin
  FreeAndNil(FStreamForLocking);
  inherited;
end;

procedure SendTaskProblemNotificationEmail(task: IScheduledTask; problem, overrideEmail, msg: string; emailAlways: boolean = False);
begin
  if msg = '' then
    SendErrorNotificationEmail(Format('%s task %s (Cl# %s, Co# %s)', [task.UserFriendlyName, problem, task.Company.CUSTOM_CLIENT_NUMBER, task.Company.CUSTOM_COMPANY_NUMBER]),
      Format('%s task %s. Client: %s (%s), company: %s (%s).', [task.UserFriendlyName, problem, task.Company.CUSTOM_CLIENT_NUMBER, task.Company.ClientName, task.Company.CUSTOM_COMPANY_NUMBER, task.Company.Name])
      + #13#10#13#10 + 'The details are available in the task''s log. To access it, go to the Scheduled Tasks-Tasks tab, select the task, then select a record in the Results grid with appropriate timestamp and use "Show log" button. '+'Alternatively, the grid on the Scheduled Tasks-All Results tab can be used in a similar way.',
      overrideEmail,
      emailAlways
    )
  else
    SendErrorNotificationEmail(Format('%s task %s (Cl# %s, Co# %s)', [task.UserFriendlyName, problem, task.Company.CUSTOM_CLIENT_NUMBER, task.Company.CUSTOM_COMPANY_NUMBER]),
      Format('%s task %s. Client: %s (%s), company: %s (%s).', [task.UserFriendlyName, problem, task.Company.CUSTOM_CLIENT_NUMBER, task.Company.ClientName, task.Company.CUSTOM_COMPANY_NUMBER, task.Company.Name])
      + #13#10#13#10 + msg,
      overrideEmail,
      emailAlways
    )
end;

function ExecuteScheduledTask(guid: string; exec: ITaskAdapter;
  emailWarnings: boolean; overrideEmail: boolean; userLogByMail: boolean; emailAlways: boolean): TScheduledTaskResultStatus;
var
  istr: TFileStream;

  procedure SetStatus(status: TScheduledTaskResultStatus; msg: string = '');
  var
    s: string;
  begin
    Result := status;
    s := inttostr(ord(status)) + #13#10 + msg + #13#10;
    istr.Write(pchar(s)^, Length(s));
  end;

var
  task: IScheduledTask;
  logKeeper: TTaskLoggerKeeper;
  ses: TScheduledTaskFiles;

  function GetOverrideEmail: string;
  begin
    if overrideEmail then
      Result := Trim(task.ParamSettings.AsString[sOverrideEmail])
    else
      Result := '';
  end;

begin
  logKeeper := nil;
  Result := statusSchedulerError;
  try
    try
      task := TScheduledTask.CreateFromGUID(guid, true);
      ses := task.NewSession;
      //!! race condition here, the task result folder is already created, but the status file doesn't exist yet; this looks like invalid result
      istr := TFileStream.Create( ses.StatusFileName, fmCreate or fmShareExclusive );
      try
        try
          logKeeper := TTaskLoggerKeeper.Create(task.UserFriendlyName + ' log', ses);
          try
					  logKeeper.Logger.LogDebug( Format('Caption: %s %s %s', [Application.Title, VersionFixedFileInfoString(GetAppFilename, vfFull), FeatureSetName] ) );
            try
              exec.Execute(logKeeper.Logger, task);
            except
              logKeeper.Logger.StopException;
            end
          finally
            logKeeper.Stop;
            logKeeper.Save;
          end;
          if logKeeper.LogStat.Errors > 0 then
          begin
            SetStatus(statusError);
            SendTaskProblemNotificationEmail(task, 'FAILED', GetOverrideEmail, IIF(userLogByMail, logKeeper.FMailUserLog.DataString, ''), emailAlways);
          end
          else if logKeeper.LogStat.Warnings > 0 then
          begin
            SetStatus(statusWarning);
            if emailWarnings or emailAlways then
              SendTaskProblemNotificationEmail(task, 'partially succeeded', GetOverrideEmail, IIF(userLogByMail, logKeeper.FMailUserLog.DataString, ''), emailAlways);
          end
          else begin
            SetStatus(statusOk);
            if emailAlways then
              SendTaskProblemNotificationEmail(task, 'Successful', GetOverrideEmail, IIF(userLogByMail, logKeeper.FMailUserLog.DataString, ''), emailAlways);
          end;
        except
		      on E: Exception do
    		  begin
	          SetStatus(statusSchedulerError, E.Message);
	          raise;
		      end;
        end;
      finally
        FreeAndNil(istr);
      end;
    except
      on E: Exception do
      begin
        SendErrorNotificationEmail('unknown task failed', 'Unexpected error:'#13#10 + E.Message, GetOverrideEmail, emailAlways);
      end;
    end;
  finally
    FreeAndNil(logKeeper);
  end;
end;

class function TScheduledTask.TaskImmutableIniByGUID(guid: string): IisSettings;
begin
  Result := TisSettingsINI.Create( TaskDirByGUID(guid) + '\immutable.ini', 'General');
end;

function TScheduledTask.Dump: string;
begin
  try
    Result := FileToString(TaskIni);
  except
    //!! no logger here
    on E: Exception do
      Result := Format('Failed to read "%s": %s', [TaskIni, E.Message])
  end;
end;

end.

