{$INCLUDE gdy.inc}

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

unit gdycommon;

interface

uses
  sysutils, classes, contnrs, windows, controls, forms,
  menus, buttons, graphics, actnlist, stdctrls;

const
  endl: string = #13+#10;

type
  TIntegerArray = array of integer;
  TStringArray = array of string;

function GetAppDir: string;
function GetAppFilename: string;
function MySelectDirectory(const Caption: string;
  var Directory: string; const Root: WideString; parent: TwinControl; allowCreate: boolean): Boolean;

function FileToComponent(const FileName: string): TComponent;
procedure ComponentToFile(Component: TComponent; const FileName: string);

procedure StringToFile( s: string; fname: string );
function  FileToString( fname: string ): string;

function ComponentToString(Component: TComponent): string;
function StringToComponent(Value: string): TComponent;

procedure Log(const Fmt: string; const Args: array of const); overload;
procedure Log(const msg: string); overload;
procedure Log(const filename: string; const msg: string); overload;
procedure MB( const msg: string ); overload;
procedure MB( const msg: string; const Args: array of const); overload;

{$ifdef GDYCOMMON}
procedure ODS(const Fmt: string; const Args: array of const); overload;
procedure ODS(const msg: string); overload;
{$endif}

function IsCardinal( s: string ): boolean;
function IsInteger( s: string ): boolean;
function IsFloat( s: string ): boolean;

procedure SetTag( menu: TMenuItem; value: integer );
procedure DeleteWithTag( menu: TMenuItem; value: integer );


procedure FreeObjects( list: TStrings ); overload;
procedure FreeObjects( stack: TObjectStack ); overload;
procedure FreeObjects( list: TObjectList ); overload;
procedure MoveItem( list: TStrings; idx: integer; newIdx: integer);overload;
procedure DeleteItem( lb: TListBox; idx: integer );
procedure MoveItem( lbFrom: TListBox; lbTo: TListBox);overload;

function PopupMenuPos( btn: TControl; isLeft: boolean = false; istop: boolean = false): TPoint;

function WaitForKey( aMsgToWait: cardinal ): boolean;

function VersionInfoAsString: string;


function RandomInteger(iLow, iHigh: Integer): Integer;
function RandomString(iLength: Integer): String;


function BinToHex(s: string): string;
function HexToBin(s: string): string;


//type
//  TPluralFormsArray = array [0..3] of string;
function Plural( n: integer; forms: array of string ): string; overload;
function Plural( s: string ): string; overload;//not complete --doesnt handle -y
function Plural( s: string; count: integer ): string; overload;

procedure StreamToFile( aStream: TStream;  filename: string );
procedure FileToStream( filename: string; aStream: TStream );

function StreamToString(aStream: TStream): string;

function GetOwningForm( frame: TComponent ): TCustomForm;

const
//  EXTENDED_NAME_FORMAT = (
    NameUnknown = 0;
    NameFullyQualifiedDN = 1;
    NameSamCompatible = 2;
    NameDisplay = 3;
    NameUniqueId = 6;
    NameCanonical = 7;
    NameUserPrincipal = 8;
    NameCanonicalEx = 9;
    NameServicePrincipal = 10;

function GetUserName(format: integer): widestring;

function WithoutTrailingSlash( const s: string): string;
function WithTrailingSlash( const s: string): string;

procedure EnableControl(control: TComponent; enable: boolean);
procedure EnableControlRecursively(control: TComponent; enable: boolean);

procedure CheckColor(const control: TControl; const grayed: Boolean);

function EnsureHasMenuItem( Self: TComponent; pm: TPopupMenu; actionInstance: TCustomAction ): TPopupMenu;

//
function GetWindowsTempFolder: string;
function FileGetBasedTempName( const filename: string ): string;

procedure DrawProgressIndicator( aCanvas: TCanvas; aRect: TRect; aOperationName: string; aCurrent, aTotal: integer );
procedure RedrawIfNeeded;
function IndexOfText( sl: TStrings; w: string ): integer;
function IndexOfNameText( sl: TStrings; w: string ): integer;

function getForm( className: string ): TForm;
function CastToMethod( code: pointer; data: pointer = nil ): TMethod;
function PtInControls( pt: TPoint; const cc: array of TControl ): boolean;

function CreateSortedStringList( text: string ): TStringList;
function ExtractValue( s: string ): string;

procedure HeapCheck;

function VarIsEqual( const v1: Variant; const v2: Variant ): boolean;

function NowAsStr: string;
function IIF(const Condition: Boolean; const TruePart, FalsePart: Variant): Variant; overload;
function IIF(const Condition: Boolean; const TruePart, FalsePart: string): string; overload;

function Join( const s: array of string; sep: string ): string;

function GetTempDir: string;
function GetPrivateTempDir: string;
function GetTempFileName(const Prefix: string): string;
function GetSpecialFolderLocation(const FolderID: Integer): string;

function VarArrayConcat( va: array of Variant): Variant;

const
  GoldenSection = 0.61803398875;
  officeEmail =  'uvwater@yaroslavl.ru';


implementation

uses shellapi, shlobj, activex, messages, typinfo,
  comctrls{$ifdef D6_UP}, variants{$endif}, filectrl, dbctrls;

{$WARNINGS OFF}
procedure HeapCheck;
begin
  if GetHeapStatus.HeapErrorCode <> 0 then
    DebugBreak;
end;
{$WARNINGS ON}


function IsCardinal( s: string ): boolean;
var
  V, Code: integer;
begin
  Val( s, v, code);
  Result := (code = 0) and (v >= 0);
end;

{$HINTS OFF}
function IsInteger( s: string ): boolean;
var
  V, Code: integer;
begin
  Val( s, v, code);
  Result := code = 0;
end;

function IsFloat( s: string ): boolean;
var
  V: extended;
begin
  Result := TextToFloat(PChar(S), V, fvExtended);
end;
{$HINTS ON}

function GetAppDir: string;
begin
  Result := ExtractFilePath(ParamStr(0)); 
end;

function GetAppFilename: string;
begin
  Result := ExtractFileName(ParamStr(0)); 
end;

function PathFromIDList( pidl: PItemIDList): string;
var
  Buffer: pchar;
  ShellMalloc: IMalloc;
begin
  Result := '';
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  try
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      ShGetPathFromIDList( pidl, Buffer );
      SetString( Result, Buffer, strlen(Buffer) );
    finally
      ShellMalloc.Free(Buffer);
    end;
  finally
  end;
end;

function MyBrowseCallBackProc(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
var
  s: string;
begin
  try
    if uMsg = BFFM_INITIALIZED then
      SendMessage( Wnd, BFFM_SETSELECTION, 1, lpData )
    else if uMsg = BFFM_SELCHANGED then
    begin
      SetString( s, pchar(lpdata), strlen(pchar(lpdata)) );
      SetLength( s, LastDelimiter('\:',s)-1 );
//      ODS( '%s %s',[PathFromIDList(PItemIDList(lParam)), s]);
      SendMessage( Wnd, BFFM_ENABLEOK, 0, ord( PathFromIDList(PItemIDList(lParam)) <> s )  );
    end;
  except
  end;
  Result := 0;
end;

function MySelectDirectory(  const Caption: string;  var Directory: string; const Root: WideString; parent: TwinControl; allowCreate: boolean ): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  ItemIDList, RootItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;
  initDir: string;
begin
  Result := False;
  initDir := Directory;
  Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(parent.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := parent.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_VALIDATE;
        if allowCreate then
          ulFlags := ulFlags or $40 ;// or BIF_EDITBOX;//
        lParam := integer(pchar(initDir));
        lpfn := @MyBrowseCallBackProc;
      end;
      WindowList := DisableTaskWindows(0);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;

function FileToComponent(const FileName: string): TComponent;
var
  FileStream: TFileStream;
  BinStream: TMemoryStream;
begin
  FileStream := TFileStream.Create( FileName, fmOpenRead or fmShareDenyWrite );
  try
    BinStream := TMemoryStream.Create;
    try
      ObjectTextToBinary(FileStream, BinStream);
      BinStream.Seek(0, soFromBeginning);
      Result := BinStream.ReadComponent(nil);
    finally
      BinStream.Free;
    end;
  finally
    FileStream.Free;
  end;
end;

procedure ComponentToFile(Component: TComponent; const FileName: string);
var
  BinStream:TMemoryStream;
  FileStream: TFileStream;
begin
  if Assigned( Component ) then
  begin
    BinStream := TMemoryStream.Create;
    try
      FileStream := TFileStream.Create( FileName, fmCreate or fmShareExclusive );
      try
        BinStream.WriteComponent(Component);
        BinStream.Seek(0, soFromBeginning);
  //      FileStream.Seek(0, soFromBeginning);
        ObjectBinaryToText(BinStream, FileStream);
      finally
        FileStream.Free;
      end;
    finally
      BinStream.Free
    end;
  end;
end;

procedure StringToFile( s: string; fname: string );
var
  ostr: TFileStream;
begin
  ostr := TFileStream.Create( fname, fmCreate or fmShareExclusive );
  try
    ostr.Write( pchar(s)^, length(s) );
  finally
    FreeAndNil( ostr );
  end;
end;


function  FileToString( fname: string ): string;
var
  istr: TFileStream;
begin
  Result := '';
  istr := TFileStream.Create( fname, fmOpenRead or fmShareDenyWrite	);
  try
    SetLength( Result, istr.Size );
    istr.Read( pchar(Result)^, Length( Result ) );
  finally
    FreeAndNil( istr );
  end;

end;

function ComponentToString(Component: TComponent): string;
var
  BinStream:TMemoryStream;
  StrStream: TStringStream;
  s: string;
begin
  BinStream := TMemoryStream.Create;
  try
    StrStream := TStringStream.Create(s);
    try
      BinStream.WriteComponent(Component);
      BinStream.Seek(0, soFromBeginning);
      ObjectBinaryToText(BinStream, StrStream);
      StrStream.Seek(0, soFromBeginning);
      Result:= StrStream.DataString;
    finally
      StrStream.Free;

    end;
  finally
    BinStream.Free
  end;
end;

function StringToComponent(Value: string): TComponent;
var
  StrStream:TStringStream;
  BinStream: TMemoryStream;
begin
  StrStream := TStringStream.Create(Value);
  try
    BinStream := TMemoryStream.Create;
    try
      ObjectTextToBinary(StrStream, BinStream);
      BinStream.Seek(0, soFromBeginning);
      Result := BinStream.ReadComponent(nil);

    finally
      BinStream.Free;
    end;
  finally
    StrStream.Free;
  end;
end;

procedure MB( const msg: string );
begin
  MessageBox(0,pchar(msg),'MB',0);
end;

procedure MB( const msg: string; const Args: array of const);
begin
  MB( Format( msg, Args ) );
end;

procedure Log(const Fmt: string; const Args: array of const); overload;
begin
  Log( Format( Fmt, Args ) );
end;

procedure Log(const msg: string); overload;
begin
 Log( ChangeFileExt(ParamStr(0),'.log'), msg );
end;

procedure Log(const filename: string; const msg: string); overload;
var
 log: THANDLE;
 written: DWORD;
begin
 log := CreateFile( pchar(filename),
                    GENERIC_WRITE,0{no share},
                    nil{secatt},
                    OPEN_ALWAYS,
                    FILE_ATTRIBUTE_NORMAL or FILE_FLAG_WRITE_THROUGH,0);
 SetFilePointer(log,0,nil,FILE_END);
 WriteFile(log,pchar(msg)^,length(msg), written,nil);
 CloseHandle(log);
end;

{$ifdef GDYCOMMON}
procedure ODS(const Fmt: string; const Args: array of const); overload;
begin
  ODS( Format( Fmt, Args ) );
end;

procedure ODS(const msg: string); overload;
var
  s: string;
begin
  s := msg + #13 + #10;
  OutputDebugString(pchar(s));
end;
{$endif}

procedure SetTag( menu: TMenuItem; value: integer );
var
  i: integer;
begin
  for i := 0 to menu.Count - 1 do
    menu.Items[i].Tag := maxint;
end;

procedure DeleteWithTag( menu: TMenuItem; value: integer );
var
  i: integer;
begin
  i := 0;
  while i < menu.Count do
    if menu.Items[i].Tag = maxint then
      menu.Delete(i)
    else
      inc(i);
end;

procedure FreeObjects( list: TStrings );
var
  i: integer;
begin
  if assigned( list )  then
    for i := 0 to list.count-1 do
    begin
      list.Objects[i].Free;
      list.Objects[i] := nil;
    end
end;

procedure FreeObjects( list: TObjectList );
var
  i: integer;
begin
  if assigned( list )  then
    for i := 0 to list.count-1 do
    begin
      list[i].Free;
      list[i] := nil;
    end
end;

procedure FreeObjects( stack: TObjectStack );
begin
  if stack <> nil then
    while stack.Count > 0 do
      stack.Pop.Free;
end;

procedure MoveItem( list: TStrings; idx: integer; newIdx: integer);
var
  s: string;
  obj: TObject;
begin
  Assert( (idx >=0) and (idx < list.Count) );
  Assert( (newIdx >=0) and (newIdx < list.Count) );
  s := list.Strings[idx];
  obj := list.Objects[idx];
  list.Delete(idx);
  list.InsertObject(newIdx, s, obj);
end;

procedure DeleteItem( lb: TListBox; idx: integer );
begin
  lb.Items.Delete(idx);
  if idx >= lb.Items.Count then
    idx := idx-1;
  lb.ItemIndex := idx;
end;

procedure MoveItem( lbFrom: TListBox; lbTo: TListBox);
var
  idx: integer;
  insIdx: integer;
begin
  idx := lbFrom.ItemIndex;
  insIdx := lbTo.ItemIndex;
  if insIdx = -1 then
    insIdx := 0;

  lbTo.Items.InsertObject(insIdx, lbFrom.Items[idx], lbFrom.Items.Objects[idx]);
  lbTo.ItemIndex := insIdx;

  DeleteItem(lbFrom, idx);
end;

function PopupMenuPos( btn: TControl; isLeft: boolean; istop: boolean  ): TPoint;
var
  p: TPoint;
begin
  if istop then
    p.y := btn.Top
  else
    p.y := btn.Top + btn.Height;
  if isleft then
    p.X := btn.Left
  else
    p.X := btn.Left + btn.Width;

  Result := btn.Parent.ClientToScreen( p );
  if isleft then
  begin
    if (Result.x < 0 ) then
      p.x := btn.Left + btn.Width;
  end
  else
  begin
    Result := btn.Parent.ClientToScreen( p );
    if (Result.x >= Screen.Width) then
      p.x := btn.Left;
  end;
  if istop then
  begin
    if (Result.y < 0 ) then
      p.y := btn.Top + btn.Height;
  end
  else
  begin
    if (Result.y >= Screen.Height) then
      p.y := btn.Top;
  end;
  Result := btn.Parent.ClientToScreen( p );

end;

function WaitForKey( aMsgToWait: cardinal ): boolean;
var
  msg: TMsg;
begin
  Result := false;
  while PeekMessage( msg,0,WM_KEYFIRST, WM_KEYLAST, PM_REMOVE ) do
    if ( msg.message = aMsgToWait ) and (msg.wParam = VK_ESCAPE) then
      Result := true;
end;

function VersionInfoAsString: string;
var
  FileName: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
begin
  Result := '';
  FileName := ParamStr(0);
  InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
        if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
          Result := Format( '%d.%d.%d ������ %d', [
          (FI.dwFileVersionMS shr 16) and $ffff, FI.dwFileVersionMS and $ffff,
          (FI.dwFileVersionLS shr 16) and $ffff, FI.dwFileVersionLS and $ffff ]);
    finally
      FreeMem(VerBuf);
    end;
  end;
end;

function RandomInteger(iLow, iHigh: Integer): Integer;
begin
  result := Trunc(Random(iHigh - iLow)) + iLow;
end;

function RandomString(iLength: Integer): String;
begin
  result := '';
  while Length(result) < iLength do
    result := result + IntToStr(RandomInteger(0, High(Integer)));
  if Length(result) > iLength then
    result := Copy(result, 1, iLength);
end;

function BinToHex(s: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to Length( S ) do
    Result := Result + IntToHex( Ord( S[i] ), 2 );
end;

function HexToBin(s: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to Length( S ) do
  begin
    if ((i mod 2) = 1) then
      Result := Result + Chr( StrToInt( '0x' + Copy( S, i, 2 )));
  end;
end;

function Plural( s: string ): string; 
const
  es_suffixes: array [0..4] of string = (
    'o','s','ch','sh','x'
  );
var
  i: integer;
begin
  Assert(Length(s)>0);
  for i := low(es_suffixes) to high(es_suffixes) do
    if copy( s, Length(s)-Length(es_suffixes[i])+1, Length(es_suffixes[i]) ) = es_suffixes[i] then
    begin
      Result := s + 'es';
      exit;
    end;
  if s[Length(s)] = 'y' then
    Result := copy(s,1,Length(s)-1) + 'ies'
  else
    Result := s + 's';
end;

function Plural( s: string; count: integer ): string; overload;
begin
  if count = 1 then
    Result := s
  else
    Result := Plural( s );
end;

function StreamToString(aStream: TStream): string;
begin
  with TStringStream.Create('') do
  try
    CopyFrom( aStream, 0 );
    Result := DataString;
  finally
    Free;
  end;
end;

procedure StreamToFile( aStream: TStream;  filename: string );
begin
  with TFileStream.Create( FileName, fmCreate ) do
  try
    CopyFrom( aStream, 0 );
  finally
    Free;
  end;
end;

procedure FileToStream( filename: string; aStream: TStream );
var
  fileStream: TFileStream;
begin
  fileStream := TFileStream.Create( FileName, fmOpenRead );
  try
    aStream.CopyFrom( fileStream, 0 );
  finally
    FreeAndNil( fileStream );
  end;
end;

function GetOwningForm( frame: TComponent ): TCustomForm;
var
  r: TComponent;
begin
  r := frame;
  while ( r <> nil ) and not (r is TCustomForm) do
    r := r.Owner;
  Result := r as TCustomForm;
end;


function GetUserName(format: integer): widestring;
var
 GetUserNameExW: function ( NameFormat: dword; lpBuffer: PWChar; var nSize: DWORD): BOOL; stdcall;

const
  UNLEN = 2048; // ;-)
var
  uName: array[0..UNLEN] of wchar;
  len: DWORD;
  hSecur32: THandle;
begin
  Result := '';
  hSecur32 := LoadLibrary( 'secur32.dll' );
  if hSecur32 <> 0 then
  begin
    GetUserNameExW := GetProcAddress( hSecur32, 'GetUserNameExW');
    if assigned(GetUserNameExW) then
    begin
      len := Length(uName);
      if GetUserNameExW(format, uName, len) then
        SetString(Result, uName, len); //different from GetUserNameExA - len DOESN"T include trailing zero!!! 
    end;
  end;

  if Result = '' then
  begin
    len := Length(uName);
    if Windows.GetUserNameW(uName, len) then
      SetString(Result, uName, len-1);
  end;
end;

function WithoutTrailingSlash( const s: string): string;
begin
  Result := s;
  if Length(Result) > 0 then
    if AnsiLastChar(Result) = '\' then
      SetLength( Result, Length(Result)-1 );
end;

function WithTrailingSlash( const s: string): string;
begin
  Result := s;
  if Length(Result) > 0 then
    if AnsiLastChar(Result) <> '\' then
      Result := Result + '\';
end;

procedure EnableControlRecursively(control: TComponent; enable: boolean);
  procedure DoItRecursively(ac: TComponent);
  var
    i: integer;
  begin
    for i := 0 to ac.ComponentCount-1 do
      DoItRecursively( ac.Components[i] );
    EnableControl(ac, enable);
  end;
begin
  DoItRecursively(control);
end;

procedure EnableControl(control: TComponent; enable: boolean);
begin
  if not (control is TControl) then //TDataSource has Enabled property
    Exit;

  if IsPublishedProp( control, 'ReadOnly' ) then
    if enable then
      SetEnumProp( control, 'ReadOnly', 'false' )
    else
      SetEnumProp( control, 'ReadOnly', 'true' );
//  else //!!let's wait and see what I have broken by commenting this
    if IsPublishedProp( control, 'Enabled' ) then
      if enable then
        SetEnumProp( control, 'Enabled', 'true' )
      else
        SetEnumProp( control, 'Enabled', 'false' );

  if IsPublishedProp( control, 'ParentColor' ) and IsPublishedProp( control, 'Color' ) and
    ((control is TRadioButton) or
     (control is TCustomListBox) or
     (control is TCustomEdit) or
     (control is TCustomComboBox) or
     (control is TCustomTreeView) or
     (control is TCustomGroupBox) or
     (control is TDBLookupControl)
     ) then
    if enable then
    begin
      SetEnumProp( control, 'ParentColor', 'false' );
      if (control is TRadioButton) or (control is TCustomGroupBox) then
        SetOrdProp( control, 'Color', clBtnFace )
      else
        SetOrdProp( control, 'Color', clWindow );
    end
    else
    begin
      SetEnumProp( control, 'ParentColor', 'true' );
    end;
end;


procedure CheckColor(const control: TControl; const grayed: Boolean);
begin
  if IsPublishedProp( control, 'ParentColor' ) and IsPublishedProp( control, 'Color' ) then
    if not grayed then
    begin
      SetEnumProp( control, 'ParentColor', 'false' );
      SetOrdProp( control, 'Color', clWindow );
    end
    else
    begin
      SetEnumProp( control, 'ParentColor', 'true' );
    end;
end;

function EnsureHasMenuItem( Self: TComponent; pm: TPopupMenu; actionInstance: TCustomAction ): TPopupMenu;//gdy22
var
  mi: TMenuItem;
  i: integer;
const
  sPopupMenuName = 'DynamicPopupMenu';
begin
  Result := pm;
  if not Assigned( Result ) then begin
    Result := Self.FindComponent( sPopupMenuName ) as TPopupMenu;
    if not Assigned( Result ) then begin
      Result := TPopupMenu.Create(Self);
      Result.Name := sPopupMenuName;
    end;
  end;

  mi := nil;
  for i := 0 to Result.Items.Count-1 do
    if Result.Items[i].Action = actionInstance then
    begin
      mi := Result.Items[i];
      break;
    end;

  if not assigned( mi ) then begin
    mi := TMenuItem.Create(Self);
    mi.Action := actionInstance;
    Result.Items.Add(mi);
  end;
end;



// from jcl
procedure StrResetLength(var S: AnsiString);
begin
  SetLength(S, StrLen(PChar(S)));
end;

function GetWindowsTempFolder: string;
var
  Required: Cardinal;
begin
  Result := '';
  Required := GetTempPath(0, nil);
  if Required <> 0 then
  begin
    SetLength(Result, Required);
    GetTempPath(Required, PChar(Result));
    StrResetLength(Result);
    Result := WithoutTrailingSlash(Result);
  end;
end;

function FileGetBasedTempName( const filename: string ): string;
var
  TempPath: string;
  R: Cardinal;
  sfx: integer;
  ext: string;
  fn: string;
begin
  Result := '';
  R := GetTempPath(0, nil);
  SetLength(TempPath, R-1); //R = Length+1(for #0)
  R := GetTempPath(R, PChar(TempPath));
  if R <> 0 then
  begin
    Result := TempPath + ExtractFileName( filename );
    sfx := 0;
    ext := ExtractFileExt( filename );
    fn := ExtractFileName( filename );
    while FileExists( Result ) do
    begin
      Result := TempPath + copy( fn, 1, length(fn)-length(ext) )+ '_'+inttostr(sfx)+ext;
      inc(sfx);
    end
  end
  else
{$IFNDEF VER130}
{$WARN SYMBOL_DEPRECATED OFF}
{$ENDIF}
    RaiseLastWin32Error;
{$IFNDEF VER130}
{$WARN SYMBOL_DEPRECATED ON}
{$ENDIF}
end;

procedure DrawProgressIndicator( aCanvas: TCanvas; aRect: TRect; aOperationName: string; aCurrent, aTotal: integer );
var
  oldBrushColor, oldFontColor: TColor;
  done: integer;
  percent: integer;
  r: TRect;
  s: string;
begin
  with aCanvas do
  begin
    if aTotal <> 0 then
    begin
      done := (aRect.right - aRect.Left)*aCurrent div aTotal;
      percent := aCurrent * 100 div aTotal;
    end
    else
    begin
      done := 0;
      percent := 0;
    end;
    s := Format(aOperationName,[aCurrent, aTotal, percent]);
//      s := FOperationName;
    r.Top := aRect.Top;
    r.Bottom := aRect.Bottom;

    oldBrushColor := Brush.Color;
    oldFontColor := Font.Color;

    Brush.Color := clActiveCaption;
    Font.Color := clCaptionText;
    r.Left := aRect.Left;
    r.Right := aRect.Left + done;
    TextRect(r,2,2, s); //!!hardcoded indent
    Brush.Color := oldBrushColor;
    Font.Color := oldFontColor;
    r.Left := aRect.Left+done+1;
    r.Right := aRect.Right;
    TextRect(r,2,2, s); //!!hardcoded indent
  end;
end;

procedure RedrawIfNeeded;
var
  Msg: TMsg;
begin
//  OldOnException := Application.OnException;
//  Application.OnException := DummyException;
    while PeekMessage(Msg, 0, WM_PAINT, WM_PAINT, PM_REMOVE) do
      DispatchMessage(Msg);
//  end;
end;

function IndexOfText( sl: TStrings; w: string ): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to sl.count-1 do
    if AnsiSameText( trim(sl[i]), trim(w) ) then
    begin
      Result := i;
      break;
    end;
end;

function IndexOfNameText( sl: TStrings; w: string ): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to sl.count-1 do
    if AnsiSameText( trim(sl.Names[i]), trim(w) ) then
    begin
      Result := i;
      break;
    end;
end;

function PluralIndex( n: integer ): integer;
begin
//n%100 / 10 ==1 ? 2 : n%10==1 ? 0: (n+9)%10 > 3 ? 2:1
  if ((n mod 100) div 10) = 1 then
    Result := 2
  else if (n mod 10) = 1 then
    Result := 0
  else if (( n + 9 ) mod 10) > 3 then
    Result := 2
  else
    Result := 1;
end;

function Plural( n: integer; forms: array of string ): string;
begin
  Result := forms[PluralIndex(n)];
end;

function GetForm( className: string ): TForm;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to screen.FormCount-1 do
    if screen.Forms[i].ClassName = className then
    begin
      Result := screen.forms[i];
      break;
    end;
end;

function CastToMethod( code, data: pointer ): TMethod;
begin
  Result.Code := code;
  Result.Data := data;
end;

function PtInControls( pt: TPoint; const cc: array of TControl ): boolean;
var
  i: integer;
begin
  Result := false;
  for i := low(cc) to high(cc) do
    if PtInRect( cc[i].BoundsRect, pt ) then
    begin
      Result := true;
      break;
    end;
end;

function CreateSortedStringList( text: string ): TStringList;
begin
  Result := TStringList.Create;
  Result.Sorted := true;
  Result.Duplicates := dupIgnore;
  Result.Text := text;
end;

function ExtractValue( s: string ): string;
var
  p: integer;
begin
  p := Pos( '=', s );
  Result := Copy( s, p + 1, Length(s) - p );
end;

function VarIsEqual( const v1: Variant; const v2: Variant ): boolean;
begin
  if VarIsEmpty(v1) or VarIsEmpty(v2) or VarIsNull(v1) or VarIsNull(v2) then
    Result := VarIsEmpty(v1) and VarIsEmpty(v2) or VarIsNull(v1) and VarIsNull(v2)
  else
    Result := v1 = v2;
end;

function NowAsStr: string;
begin
{$ifdef RUSSIAN}
  Result := FormatDateTime( 'yyyy-mm-dd (hh-nn-ss)', Now );
{$else}
  Result := FormatDateTime( 'mm-dd-yyyy (hh-nn-ss)', Now );
{$endif}
end;

function IIF(const Condition: Boolean; const TruePart, FalsePart: Variant): Variant;
begin
  if Condition then
    Result := TruePart
  else
    Result := FalsePart;
end;

function IIF(const Condition: Boolean; const TruePart, FalsePart: string): string;
begin
  if Condition then
    Result := TruePart
  else
    Result := FalsePart;
end;

function Join( const s: array of string; sep: string ): string;
var
  i: integer;
begin
  Result := '';
  for i := low(s) to high(s) do
  begin
    if Result <> '' then
      Result := Result + sep;
    Result := Result + s[i];
  end
end;

function GetTempDir: string;
begin;
  SetLength(Result, 255);
  SetLength(Result, GetTempPath(255, (PChar(Result))));
end;

function GetPrivateTempDir: string;
begin
  Result := GetTempDir +
    ChangeFileExt(GetAppFilename, IntToHex(GetCurrentProcessId, 8)) +
    '\';
  if not DirectoryExists(Result) then
    CreateDir(Result);
end;

function GetTempFileName(const Prefix: string): string;
var
  TempPath, TempFile: string;
  R: Cardinal;
begin
  Result := '';
  TempPath := GetTempDir;
  if TempPath <> '' then
  begin
    SetLength(TempFile, MAX_PATH);
    R := windows.GetTempFileName(PChar(TempPath), PChar(Prefix), 0, PChar(TempFile));
    if R <> 0 then
    begin
      SetLength(TempFile, StrLen(PChar(TempFile)));
      Result := TempFile;
    end;
  end;
end;

function PidlToPath(IdList: PItemIdList): string;
begin
  SetLength(Result, MAX_PATH);
  if SHGetPathFromIdList(IdList, PChar(Result)) then
    StrResetLength(Result)
  else
    Result := '';
end;

function PidlFree(var IdList: PItemIdList): Boolean;
var
  Malloc: IMalloc;
begin
  Result := False;
  if IdList = nil then
    Result := True
  else
  begin
    if Succeeded(SHGetMalloc(Malloc)) and (Malloc.DidAlloc(IdList) > 0) then
    begin
      Malloc.Free(IdList);
      IdList := nil;
      Result := True;
    end;
  end;
end;

function GetSpecialFolderLocation(const FolderID: Integer): string;
var
  FolderPidl: PItemIdList;
begin
  if Succeeded(SHGetSpecialFolderLocation(0, FolderID, FolderPidl)) then
  begin
    Result := PidlToPath(FolderPidl);
    PidlFree(FolderPidl);
  end
  else
    Result := '';
end;

function VarArrayConcat( va: array of Variant): Variant;
var
  len, i, cur, j: integer;
begin
  len := 0;
  for i := low(va) to high(va) do
    if VarIsArray( va[i] ) then
      inc( len, VarArrayHighBound(va[i], 1) - VarArrayLowBound(va[i], 1) + 1 )
    else
      inc( len );
  Result := VarArrayCreate([0, len-1], varVariant);
  cur := 0;
  for i := low(va) to high(va) do
    if VarIsArray( va[i] ) then
      for j := VarArrayLowBound(va[i], 1) to VarArrayHighBound(va[i], 1) do
      begin
        Result[cur] := va[i][j];
        inc(cur);
      end
    else
    begin
      Result[cur] := va[i];
      inc(cur);
    end;
  Assert( cur = len );
end;

end.





