unit gdyCommonLoggerView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyLoggerImpl, StdCtrls, ExtCtrls,
  ComCtrls, gdyLogWriters, gdyLoggerRichView, gdyLogger, gdycommonlogger;

type
  TCommonLoggerViewFrame = class(TFrame )
    pnlLog: TPanel;
    LoggerRichView: TLoggerRichViewFrame;
  private
  protected
    FLoggerKeeper: TCommonLoggerKeeper;
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    property LoggerKeeper: TCommonLoggerKeeper read FLoggerKeeper;
  end;

implementation

uses
  typinfo, gdycommon;

{$R *.dfm}

{ TImportLoggerViewFrame }

constructor TCommonLoggerViewFrame.Create(Owner: TComponent);
begin
  inherited;
  FLoggerKeeper := TCommonLoggerKeeper.Create( LoggerRichView as ILoggerEventSink );
end;

destructor TCommonLoggerViewFrame.Destroy;
begin
  FreeAndNil( FLoggerKeeper );
  inherited;
end;

end.
