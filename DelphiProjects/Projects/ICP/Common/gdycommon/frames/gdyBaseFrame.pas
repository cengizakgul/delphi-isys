unit gdyBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TBaseFrame = class(TFrame)
  private
    { Private declarations }
  protected
    procedure SetParent(AParent: TWinControl); override;
  public
    { Public declarations }
  end;

implementation

uses
  actnlist;
{$R *.dfm}

{ TFixedVCLFrame }

type
  TCustomFormCheatCast = class(TCustomForm);

procedure TBaseFrame.SetParent(AParent: TWinControl);
var
  parentForm: TCustomFormCheatCast;

  procedure AddActionList(ActionList: TCustomActionList);
  begin
    if parentForm.FActionLists = nil then
      parentForm.FActionLists := TList.Create;
    parentForm.FActionLists.Add(ActionList);
  end;

  procedure RemoveActionList(ActionList: TCustomActionList);
  begin
    if (parentForm <> nil) and (parentForm.FActionLists <> nil) then
      parentForm.FActionLists.Remove(ActionList);
  end;

  procedure UpdateActionLists( aFrame: TCustomFrame; Operation: TOperation);
  var
    I: Integer;
    Component: TComponent;
  begin
    for I := 0 to aFrame.ComponentCount - 1 do
    begin
      Component := aFrame.Components[I];
      if Component is TCustomActionList then
        case Operation of
          opInsert: AddActionList( TCustomActionList(Component));
          opRemove: RemoveActionList( TCustomActionList(Component));
        end
      else if Component is TCustomFrame then
        UpdateActionLists( Component as TCustomFrame, Operation )
    end;
  end;

  procedure UpdateActionListsOnChildFrames(Operation: TOperation);
  var
    I: Integer;
  begin
    for I := 0 to ControlCount - 1 do
      if Controls[i] is TCustomFrame then
        UpdateActionLists( Controls[i] as TCustomFrame, Operation )
  end;
begin
  parentForm := TCustomFormCheatCast(GetParentForm(Self));
  if parentForm <> nil then
    UpdateActionListsOnChildFrames(opRemove);
  inherited;
  parentForm := TCustomFormCheatCast(GetParentForm(Self));
  if parentForm <> nil then
    UpdateActionListsOnChildFrames(opInsert);
end;

end.
