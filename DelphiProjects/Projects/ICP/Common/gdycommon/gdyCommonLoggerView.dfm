object CommonLoggerViewFrame: TCommonLoggerViewFrame
  Left = 0
  Top = 0
  Width = 700
  Height = 579
  TabOrder = 0
  object pnlLog: TPanel
    Left = 0
    Top = 0
    Width = 700
    Height = 579
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    inline LoggerRichView: TLoggerRichViewFrame
      Left = 0
      Top = 0
      Width = 700
      Height = 579
      Align = alClient
      TabOrder = 0
      inherited pnlUserView: TPanel
        Width = 700
        Height = 579
        inherited evSplitter1: TSplitter
          Width = 700
        end
        inherited pnlTopPart: TPanel
          Width = 700
          inherited lvMessages: TListView
            Width = 700
          end
        end
        inherited pnlBottom: TPanel
          Width = 700
          Height = 341
          inherited evSplitter2: TSplitter
            Left = 348
            Height = 341
          end
          inherited pnlLeft: TPanel
            Width = 348
            Height = 341
            inherited mmDetails: TMemo
              Width = 348
              Height = 316
            end
            inherited evPanel1: TPanel
              Width = 348
            end
          end
          inherited pnlRight: TPanel
            Left = 353
            Height = 341
            inherited mmContext: TMemo
              Height = 316
            end
          end
        end
      end
    end
  end
end
