{$include gdy.inc}
unit gdyLoggerRichView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, gdyLoggerImpl, StdCtrls, ComCtrls, ExtCtrls,
  ImgList;

type
  TMessageLoggedEvent = procedure(item: TListItem; aLogState: TLogState) of object;

  TLoggerRichViewFrame = class(TFrame, ILoggerEventSink)
    pnlUserView: TPanel;
    evSplitter1: TSplitter;
    pnlTopPart: TPanel;
    lvMessages: TListView;
    pnlBottom: TPanel;
    evSplitter2: TSplitter;
    pnlLeft: TPanel;
    mmDetails: TMemo;
    evPanel1: TPanel;
    pnlRight: TPanel;
    evPanel2: TPanel;
    mmContext: TMemo;
    lblDetails: TLabel;
    lblContext: TLabel;
    evImageList1: TImageList;
    procedure lvMessagesChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
  private
    FOnMessageLogged: TMessageLoggedEvent;
    {ILoggerEventSink}
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  protected
    procedure Loaded; override;
  public
//    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    property OnMessageLogged: TMessageLoggedEvent read FOnMessageLogged write FOnMessageLogged;
    function MessagesCount: integer;
  end;

implementation

uses
  gdyLogWriters, gdyLogger;

{$R *.dfm}

{ TImportLoggerRichViewFrame }

procedure TLoggerRichViewFrame.BlockEntered(aLogState: TLogState);
begin
end;

procedure TLoggerRichViewFrame.BlockLeaving(aLogState: TLogState);
begin
end;

procedure TLoggerRichViewFrame.ContextItemLogged(aLogState: TLogState);
begin
end;

procedure TLoggerRichViewFrame.MessageLogged(aLogState: TLogState);
var
  item: TListItem;
begin
//  TIdAntiFreezeBase.DoProcess(False);
  if not (aLogState.Last.Messages.Last.Kind in [lmDebug]) then
  try
    item := lvMessages.Items.Add;
    item.Data := aLogState.Clone;
    case aLogState.Last.Messages.Last.Kind of
      lmEvent: item.ImageIndex := 2;
      lmWarning: item.ImageIndex := 1;
      lmError, lmException: item.ImageIndex := 0;
    else
      item.ImageIndex := -1;
    end;
    if assigned(FOnMessageLogged) then
      FOnMessageLogged(item, aLogState)
    else
      item.Caption := aLogState.Last.Messages.Last.Text;
  finally    
    Repaint;
  end;
end;

procedure TLoggerRichViewFrame.lvMessagesChange(Sender: TObject; Item: TListItem; Change: TItemChange);
var
  i: integer;
  j: integer;
  ls: TLogState;
  needendl: boolean;
begin
  if Change = ctstate then
  begin
    mmDetails.Lines.Clear;
    mmContext.Lines.Clear;
    if lvMessages.Selected <> nil then
    begin
      ls := TLogState(lvMessages.Selected.Data);
      if assigned(ls) then
      begin
        mmDetails.Lines.Add( ls.Last.Messages.Last.Text );
        mmDetails.Lines.Add( '' );
        mmDetails.Lines.Add( ls.Last.Messages.Last.Details );
        needendl := false;
        for i := 0 to ls.BlockStack.Count-1 do
        begin
          if needendl then
            mmContext.Lines.Add('');
          mmContext.Lines.Add( ls.BlockStack[i].BlockName + ':');
          for j := 0 to ls.BlockStack[i].Context.Count-1 do
          begin
            mmContext.Lines.Add( '  ' + ls.BlockStack[i].Context[j].ContextTag + ': ' + FormatCtxVal(ls.BlockStack[i].Context[j].ContextValue) );
            needendl := true;
          end
        end;
        mmDetails.ScrollBy(-100000,-100000); //!!wtf
{        mmDetails.SelStart := 0;
        mmContext.SelStart := 0;}
//        mmDetails.Lines.Add( 'Dump:' );
//        mmDetails.Lines.Add( ls.Dump );
      end;
    end;
  end;
end;

destructor TLoggerRichViewFrame.Destroy;
var
  i: integer;
begin
  for i := 0 to lvMessages.Items.Count-1 do
  begin
    TLogState(lvMessages.Items[i].Data).Free;
    lvMessages.Items[i].Data := nil;
  end;
  inherited;
end;

procedure TLoggerRichViewFrame.Loaded;
begin
{$ifdef RUSSIAN}
  lblDetails.Caption := '��������� ���������';
  lblContext.Caption := '��������';
  lvMessages.Columns[0].Caption := '������� ���������';
{$endif}
end;

function TLoggerRichViewFrame.MessagesCount: integer;
begin
  Result := lvMessages.Items.Count;
end;


end.
