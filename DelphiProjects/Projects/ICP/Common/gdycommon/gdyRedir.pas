unit gdyRedir;

interface

uses
  inifiles, classes, sysutils;
type

(*

Predefined macros:

AppDir
The directory where executable file is located.

WorkDir
The current directory.

TempDir
Windows temporary directory.

ThisDir
The directory where the redirection file is located.

AppDataDir
The file system directory that serves as a common repository for application-specific data.
A typical path is C:\Documents and Settings\username\Application Data.

CommonAppDataDir
The file system directory that contains application data for all users.
A typical path is C:\Documents and Settings\All Users\Application Data.
This folder is used for application data that is not user specific.
For example, an application can store a spell-check dictionary,
a database of clip art, or a log file in the CSIDL_COMMON_APPDATA folder.
This information will not roam and is available to anyone using the computer.

LocalAppDataDir
The file system directory that serves as a data repository for local (nonroaming) applications.
A typical path is C:\Documents and Settings\username\Local Settings\Application Data.

The directory names DOESN'T have trailing backslash, macro expansion is case INsensitive.
*)

  IRedirection = interface
['{28D30809-069C-438E-8560-31208F6EBDC2}']
//both functions raise ERedirectionNotFound if corresponding alias was not found in .red file (or another source)
    function GetDirectory( alias: string ): string; //ended by backslash
    function GetFilename( alias: string ): string;
    procedure GetFileSet( sl: TStrings );
    procedure Dump( sl: TStrings );
  end;

  IRedirectionEditor = interface
['{A96918EF-1831-4918-9645-5BD4A930203D}']
    procedure SetDirectory( alias: string; value: string );
    procedure SetFilename( alias: string; value: string );
    procedure EnsureExists;
  end;

  ERedirectionError = class(Exception);
  ERedirectionFileNotFound = class(ERedirectionError);
  ERedirectionNotFound = class(ERedirectionError);
  ERedirectionInvalid = class(ERedirectionError);
    ERedirectionDuplicateDirectory = class(ERedirectionInvalid);
    ERedirectionDuplicateFileName = class(ERedirectionInvalid);
    ERedirectionSyntaxError = class(ERedirectionInvalid);

  TGenericRedirection = class( TInterfacedObject, IRedirection )
  private
    FDirectories: TStringList;
    FFilenames: TStringList;
    procedure CheckValid;
    function Substitute( alias: string; initialalias: string ): string;
  protected
    function ExpandPredefinedAlias( alias: string; var expanded: string ): boolean; virtual;
  public
    constructor Create( aSrc: TCustomIniFile );
    destructor Destroy; override;
    function GetDirectory( alias: string ): string;
    function GetFilename( alias: string ): string;
    procedure GetFileset( sl: TStrings ); virtual;
    procedure Dump( sl: TStrings );
  end;

  TRedirectionFile = class( TGenericRedirection )
  private
    FFileName: string;
  protected
    function ExpandPredefinedAlias( alias: string; var expanded: string ): boolean; override;
  public
    constructor Create;
    constructor CreateFromFile( iniFileName: string);
    procedure GetFileset( sl: TStrings ); override;
  end;

  TRedirectionFileEditor = class( TInterfacedObject, IRedirectionEditor )
  private
    FFileName: string;
  public
    constructor Create( iniFileName: string );
  {IRedirectionEditor}
    procedure SetDirectory( alias: string; value: string );
    procedure SetFilename( alias: string; value: string );
    procedure EnsureExists;
  end;

function Redirection: IRedirection; overload; //never use this function in library code
function Redirection( aFileName: string ): IRedirection; overload;
function RedirectionEditor: IRedirectionEditor; overload; //never use this function in library code
function RedirectionEditor( aFileName: string ): IRedirectionEditor; overload;

//never use these functions in library code
procedure EnsureAppRedirectionExists( dirdefaults, filenamedefaults: string );
function AppRedirectionFileName: string;

const
  sDirectories = 'Directories';
  sFilenames = 'Filenames';

implementation

uses
  gdycommon, dialogs, forms, shfolder;


threadvar
  FWorkDir: string; //wo trailing slash


{ TGenericRedirection }

constructor TGenericRedirection.Create( aSrc: TCustomIniFile);
begin
  FDirectories := TStringList.Create;
  FFilenames := TStringList.Create;
  aSrc.ReadSectionValues( sDirectories, FDirectories );
  aSrc.ReadSectionValues( sFilenames, FFilenames );
  CheckValid;  
end;

destructor TGenericRedirection.Destroy;
begin
  inherited;
  FreeAndNil( FDirectories );
  FreeAndNil( FFilenames );
end;

function TGenericRedirection.GetDirectory(alias: string): string;
var
  l: integer;
begin
  if FDirectories.IndexOfName( alias ) = -1 then
    raise ERedirectionNotFound.CreateFmt('Cannot find redirection for directory alias <%s>',[alias]);
  Result := trim(Substitute(FDirectories.Values[alias],alias));
  l := Length(Result);
  if (l > 0) and ( Result[l] <> '\') then
    Result := Result + '\';
end;

procedure TGenericRedirection.Dump(sl: TStrings);
var
  i: integer;
begin
  sl.Add( '[Directories]' );
  for i := 0 to FDirectories.Count-1 do
  try
    sl.Add( FDirectories.Names[i] +'='+GetDirectory(FDirectories.Names[i]) );
  except
    on e:exception do sl.Add( FDirectories.Names[i] +'=������ '+e.Message+'' );
  end;
  sl.Add( '[Filenames]' );
  for i := 0 to FFilenames.Count-1 do
  try
    sl.Add( FFilenames.Names[i] +'='+GetFilename(FFilenames.Names[i]) );
  except
    on e:exception do sl.Add( FFilenames.Names[i] +'=������ '+e.Message+'' );
  end;
end;

function TGenericRedirection.GetFilename(alias: string): string;
begin
  if FFilenames.IndexOfName( alias ) = -1 then
    raise ERedirectionNotFound.CreateFmt('Cannot find redirection for file alias <%s>',[alias]);
  Result := trim(Substitute(FFilenames.Values[alias],alias));
end;

procedure TGenericRedirection.GetFileset(sl: TStrings);
var
  i: integer;
begin
  for i := 0 to FFilenames.Count-1 do
  try
    sl.Add( GetFilename(FFilenames.Names[i]) );
  except
  end;
end;

function TGenericRedirection.ExpandPredefinedAlias( alias: string; var expanded: string ): boolean;
begin
  Result := true;
  expanded := '';
  if AnsiSameText( alias, 'WorkDir' ) then
    expanded := FWorkDir
  else if AnsiSameText( alias, 'AppDir' ) then
    expanded := GetAppDir
  else if AnsiSameText( alias, 'TempDir' ) then
    expanded := GetWindowsTempFolder
  else if AnsiSameText( alias, 'AppDataDir' ) then
    expanded := GetSpecialFolderLocation(CSIDL_APPDATA )
  else if AnsiSameText( alias, 'LocalAppDataDir' ) then
    expanded := GetSpecialFolderLocation(CSIDL_LOCAL_APPDATA )
  else if AnsiSameText( alias, 'CommonAppDataDir' ) then
    expanded := GetSpecialFolderLocation(CSIDL_COMMON_APPDATA )
  else
    Result := false;

  if expanded <> '' then
    expanded := WithoutTrailingSlash(expanded);
end;

function TGenericRedirection.Substitute( alias: string; initialalias: string ): string;

  function OneSubst( a: string; aliaspath: string ): string;
  var
    i: integer;
    state: ( ssIdle, ssSpecialChar, ssInMacro );
    macro: string;
    c:  char;

    function Expand( m: string ): string;
    var
      j: integer;
      name: string;
      found: boolean;
    begin
      if AnsiPos( AnsiLowerCase('%'+m+'%'), AnsiLowerCase(aliaspath) ) <> 0 then
        raise ERedirectionSyntaxError.Createfmt( 'Redirection: circular reference encountered: <%s,%s>',[StringReplace( StringReplace(aliaspath,'%%',',',[rfReplaceAll]),'%','',[rfReplaceAll]),m]);
      found := ExpandPredefinedAlias( m, Result );
      if not found then
        for j := 0 to FDirectories.Count - 1 do
        begin
          name := FDirectories.Names[j];
          if AnsiSameText( m, name ) then
          begin
            Result := trim(FDirectories.Values[name]);
            Result := OneSubst( Result, aliaspath + '%' + m + '%' );
            found := true;
            break;
          end;
        end;

      if not found then
        raise ERedirectionSyntaxError.CreateFmt('Redirection: syntax error: Cannot expand <%%%s%%>',[m]);
    end;
  begin
    Result := '';
    state := ssIdle;
    for i := 1 to length(a) do
    begin
      c := a[i];
      case state of
        ssIdle: if c = '%' then
                  state := ssSpecialChar
                else
                  Result := Result + c;
        ssSpecialChar: if c = '%' then
                       begin
                         Result := Result + '%';
                         state := ssIdle;
                       end
                       else
                       begin
                         macro := c;
                         state := ssInMacro;
                       end;
        ssInMacro: if c = '%' then
                   begin
                     Result := Result + Expand( macro );
                     state := ssIdle;
                   end
                   else
                     macro := macro + c;
      end;
    end;
    if not (State in [ssIdle]) then
      raise ERedirectionSyntaxError.CreateFmt( 'Redirection: syntax error while parsing <%s>.'+#13+#10+'Stopped after <%s>',[a, Result]);
  end;
begin
  if AnsiPos( '%', initialalias ) <> 0 then
    raise ERedirectionSyntaxError.CreateFmt( 'Redirection: syntax error: %% in alias name: <%s>',[initialalias] );
  Result := OneSubst( alias, '%'+initialalias+'%' );
end;

function DefaultRedirectionFileName: string;
begin
  Result := FWorkDir +'\'+ ChangeFileExt( GetAppFileName, '.red' );
end;

function SearchRedirection(modulefn: string): string;
var
  r1, r2: string;
begin
  r1 := FWorkDir +'\'+ ChangeFileExt( ExtractFileName(modulefn), '.red' );
  Result := r1;
  if not FileExists( Result ) then
  begin
    r2 := ChangeFileExt( modulefn, '.red' );
    Result := r2;
  end;
  if not FileExists( Result ) then
    if r1 = r2 then
      raise ERedirectionFileNotFound.CreateFmt( 'Cannot find redirection file for %s (<%s>)',[ExtractFileName(modulefn),r1])
    else
      raise ERedirectionFileNotFound.CreateFmt( 'Cannot find redirection file for %s (<%s> nor <%s>)',[ExtractFileName(modulefn),r1,r2]);
end;

function ModuleRedirectionFileName: string;
begin
  Result := SearchRedirection(ParamStr(0)); //!!! GetModuleName(HInstance)
end;

//!! reimplement using SearchRedirection
function AppRedirectionFileName: string;
var
  r1, r2: string;
begin
  r1 := DefaultRedirectionFileName;
  Result := r1;
  if not FileExists( Result ) then
  begin
    r2 := GetAppDir + ChangeFileExt( GetAppFileName, '.red' );
    Result := r2;
  end;
  if not FileExists( Result ) then
    if r1 = r2 then
      raise ERedirectionFileNotFound.CreateFmt( 'Cannot find redirection file for %s (<%s>)',[GetAppFileName,r1])
    else
      raise ERedirectionFileNotFound.CreateFmt( 'Cannot find redirection file for %s (<%s> nor <%s>)',[GetAppFileName,r1,r2]);
end;

procedure EnsureAppRedirectionExists( dirdefaults, filenamedefaults: string );
var
  sl: TStringList;
  i: integer;
begin
  try
    AppRedirectionFileName;
  except
    on ERedirectionFileNotFound do
      with RedirectionEditor( DefaultRedirectionFileName ) do
        EnsureExists;
  end;
  sl := TStringList.Create;
  try
    sl.Text := dirdefaults;
    for i := 0 to sl.Count-1 do
      try
        Redirection.GetDirectory( sl.Names[i] );
      except
        on ERedirectionNotFound do
          with RedirectionEditor do
            SetDirectory( sl.Names[i], sl.Values[ sl.Names[i] ] );
      end;
    sl.Text := filenamedefaults;
    for i := 0 to sl.Count-1 do
      try
        Redirection.GetFilename( sl.Names[i] );
      except
        on ERedirectionNotFound do
          with RedirectionEditor do
            SetFilename( sl.Names[i], sl.Values[ sl.Names[i] ] );
      end;
  finally
    FreeAndNil( sl );
  end;
end;

resourcestring
  sDuplicateDirectory = '<%s> and <%s>';
  sDuplicateFileName = '<%s> and <%s>';

procedure TGenericRedirection.CheckValid;
var
  i: integer;
  j: integer;
begin
  for i := 0 to FDirectories.Count - 1 do
    for j := i + 1 to FDirectories.Count - 1 do
      if FDirectories.Names[i] = FDirectories.Names[j] then
        raise ERedirectionDuplicateDirectory.CreateFmt( sDuplicateDirectory, [FDirectories[i], FDirectories[j]] );
  for i := 0 to FFilenames.Count - 1 do
    for j := i + 1 to FFilenames.Count - 1 do
      if FFilenames.Names[i] = FFilenames.Names[j] then
        raise ERedirectionDuplicateFilename.CreateFmt( sDuplicateFilename, [FFilenames[i], FFilenames[j]] );
end;

{ TRedirectionFile }


constructor TRedirectionFile.Create;
begin
  CreateFromFile( ModuleRedirectionFileName );
end;

constructor TRedirectionFile.CreateFromFile(iniFileName: string);
var
  iniFile: TCustomIniFile;
begin
  if not FileExists( iniFileName ) then
    raise ERedirectionFileNotFound.CreateFmt( 'Cannot find redirection file <%s>',[iniFileName]);
  FFileName := iniFileName;
  iniFile := TIniFile.Create( FFileName );
  try
    inherited Create( iniFile );
  finally
    FreeAndNil( iniFile );
  end;
end;

function TRedirectionFile.ExpandPredefinedAlias(alias: string;
  var expanded: string): boolean;
begin
  Result := inherited ExpandPredefinedAlias( alias, expanded );
  if not Result then
    if AnsiSameText( alias, 'ThisDir' ) then
    begin
      expanded := WithoutTrailingSlash( ExtractFilePath( FFileName ) );
      Result := true;
    end;
end;

procedure TRedirectionFile.GetFileset(sl: TStrings);
begin
  sl.Add( FFileName );
  inherited;
end;

function Redirection: IRedirection;
begin
  Result := TRedirectionFile.Create as IRedirection;
end;

function Redirection( aFileName: string ): IRedirection;
begin
  Result := TRedirectionFile.CreateFromFile( aFileName ) as IRedirection;
end;

(*
procedure CheckRedirection;
begin
  Redirection;
end;
*)

function RedirectionEditor: IRedirectionEditor; overload;
begin
  Result := RedirectionEditor(ModuleRedirectionFileName);
end;

function RedirectionEditor( aFileName: string ): IRedirectionEditor; overload;
begin
  Result := TRedirectionFileEditor.Create(aFileName) as IRedirectionEditor;
end;

{ TRedirectioonFileEditor }

constructor TRedirectionFileEditor.Create(iniFileName: string);
begin
  FFileName := iniFileName;
end;

procedure TRedirectionFileEditor.SetDirectory(alias, value: string);
begin
  with TIniFile.Create( FFileName ) do
  try
    WriteString( sDirectories, alias, value );
  finally
    Free;
  end;
end;

procedure TRedirectionFileEditor.SetFilename(alias, value: string);
begin
  with TIniFile.Create( FFileName ) do
  try
    WriteString( sFilenames, alias, value );
  finally
    Free;
  end;
end;

procedure TRedirectionFileEditor.EnsureExists;
begin
  if not FileExists( FFileName ) then
    with TstringList.Create do
    try
      Text := '[Directories]' + endl + '[Filenames]' + endl;
      SaveToFile( FFilename );
    finally
      Free;
    end;
end;

initialization
  FWorkDir := GetCurrentDir;


end.
