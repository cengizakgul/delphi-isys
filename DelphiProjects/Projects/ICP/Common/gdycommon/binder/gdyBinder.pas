{$include gdy.inc}
unit gdyBinder;

interface
uses
  db, dbclient;

type
  TBoundTableDesc = record
    Caption: string;
    Unique: boolean;
    KeyFields: string;
    ListFields: string;
  end;

  TBindingDesc = record
    Name: string;
    Caption: string;
    LeftDesc: TBoundTableDesc;
    RightDesc: TBoundTableDesc;
    ExtraFields: string;
    IndicatorField: string;
    HideKeyFields: boolean;
  end;

  TMappingDescs = array of TBindingDesc;

  IMatcher = interface
['{EEA91D3A-CB3D-48CA-ADF6-0EB7430F3156}']
    function BindingName: string;
    function RightMatch( aLeftKeyValues: Variant ): Variant; //may return VarArray or single Variant
    function LeftMatch( aRightKeyValues: Variant ): Variant; //may return VarArray or single Variant
    function ExtraDataByLeftKey( aLeftKeyValues: Variant; aExtraFieldNames: string ): Variant; //may return VarArray or single Variant
    function ExtraDataByRightKey( aRightKeyValues: Variant; aExtraFieldNames: string ): Variant; //may return VarArray or single Variant
    function Dump: string;
  end;

  IMatchTableKeeper = interface
['{CA0EE84A-8684-4FD3-8185-B75F226EBA34}']
    function MatchTable: TDataSet;
    procedure Save( aFileName: string );
    function SaveToString: string;
    function IsChanged: boolean;
    function CreateMatcher: IMatcher;
  end;

  IBinder = interface
    ['{07FBA2D0-6590-42EB-99C9-82A8CFEA2E7F}']
    function LeftTable: TDataSet;
    function RightTable: TDataSet;
    function MatchTable: TDataSet;
    function BindingDesc: TBindingDesc;

    function LeftKeyValues: Variant;
    function RightKeyValues: Variant;
    procedure AddMatch( aLeftKeys, aRightKeys: Variant );
    procedure RemoveMatch;
    procedure RemoveAllMatches;
  end;

  IAutoMapper = interface
    ['{0ABBCD3C-EE8A-4DE1-8A00-0D89DD153ADF}']
    function MapToRightKeys( aLeftTable: TDataSet; bd: TBindingDesc; aRightTable: TDataSet ): Variant;
  end;

  IVisualBinder = interface
    ['{559C65A5-A9BB-47BC-B1AA-DAFDFE563C29}']
    procedure ActivateBinder( aBinder: IBinder; aAutoMapper: IAutoMapper );
    function  GetBinder: IBinder;
  end;

  IMatchersList = interface
    ['{F6DFBACD-99E8-460B-B710-3DB67374E600}']
    procedure Add( aMatch: IMatcher );
    procedure Remove( aMatch: IMatcher );
    function Get( aBindingName: string ): IMatcher;
  end;

  IBindingKeeper = interface
    ['{EDB7707D-08C5-4EE6-9706-1993BC694375}']
    function BindingName: string;
    procedure SetTables( aLeftTable, aRightTable: TDataSet );
    procedure SetVisualBinder( aVisualBinder: IVisualBinder );
    procedure SetConnected( Value: boolean );
    procedure SetMatchTableFromString( content: string );
    procedure SetAutoMapper( aAutoMapper: IAutoMapper );

    function SaveMatchTableToString: string;
    function IsMatchTableChanged: boolean;
    procedure CancelMatchTable;

    function CanCreateMatcher: boolean;
    function CreateMatcher: IMatcher;
  end;

  IBindingKeepersList = interface
    ['{AA358609-9296-4DC9-B8DB-C6F3780CC089}']
    procedure Add( aBindingKeeper: IBindingKeeper );
    procedure Remove( aBindingKeeper: IBindingKeeper );
    function Get( aBindingName: string ): IBindingKeeper;
    function GetByIndex( i: integer ): IBindingKeeper;
    function Count: integer;
  end;

const
  sLeftTablePfx = 'LEFT_';
  sRightTablePfx = 'RIGHT_';
  sMatchedFieldName = 'MATCHED';

type
  TCDSBase = TClientDataSet;
  TCDSBaseClass = class of TCDSBase;

function CreateBindingKeeper( bd: TBindingDesc; cdsClass: TCDSBaseClass ): IBindingKeeper;
function ComposeBindingFilename( aBaseFileName: string; aBindingName: string ): string;
function NullAutoMapper: IAutoMapper;

implementation
uses
  sysutils, gdyBinderKeeperImpl{$ifdef D6_UP}, variants{$endif};

function ComposeBindingFilename( aBaseFileName: string; aBindingName: string ): string;
begin
  Result := ChangeFileExt( aBaseFileName, '' )+'_'+aBindingName+'.xml';
end;

function CreateBindingKeeper( bd: TBindingDesc; cdsClass: TCDSBaseClass ): IBindingKeeper;
begin
  Result := TBindingKeeper.Create( bd, cdsclass ) as IBindingKeeper;
end;

type
  TNullAutoMapper = class(TInterfacedObject, IAutoMapper )
  private
    {IAutoMapper}
    function MapToRightKeys( aLeftTable: TDataSet; bd: TBindingDesc; aRightTable: TDataSet ): Variant;
  end;

function NullAutoMapper: IAutoMapper;
begin
  Result := TNullAutoMapper.Create;
end;

{ TNullAutoMapper }

function TNullAutoMapper.MapToRightKeys( aLeftTable: TDataSet; bd: TBindingDesc; aRightTable: TDataSet ): Variant;
begin
  Result := Null;
end;

{
function GetBindingDescByName( descs: array of TBindingDesc; name: string ): TBindingDesc;
var
  i: integer;
  bFound: boolean;
begin
  bFound := false;
  for i := 0 to high(descs) do
    if descs[i].Name = name then
    begin
      Result := descs[i];
      bFound := true;
      break;
    end;
  if not bFound then
    raise Exception.CreateFmt('Internal configuration error: mapping description is not found for <%s>', [name]);
end;
}

end.


