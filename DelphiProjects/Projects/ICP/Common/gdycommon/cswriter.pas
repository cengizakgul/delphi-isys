{$include gdy.inc}
unit cswriter;

interface

uses
  classes, db;

type


  TCSWriter = class(TObject)
  private
    FOutStream: TStream;
    FAutoDelete: boolean;
    FBeginOfLine: boolean;
    FSeparator: string;
    FPreprocessing: boolean;

    procedure WriteBuffer( buffer: string );
  public
    constructor Create( ostream: TStream; autodel: boolean ); overload;
    constructor Create( filename: string ); overload;
    destructor  Destroy; override;

    procedure   Write( field: TField ); overload;
    procedure   Write( field: string ); overload;
    procedure   WriteRow( fields: array of string ); 

    procedure   WriteEndOfRow;
    property    Separator: string read FSeparator write FSeparator;
    property    Preprocessing:boolean read FPreprocessing write FPreprocessing;
end;

procedure CollectionToCSV( fname: string; col: TCollection );

implementation

uses
  sysutils{$ifdef D6_UP}, variants{$endif}, typinfo;


procedure FreeAndNilProperties(AObject: TObject);
var
  I, Count: Integer;
  PropInfo: PPropInfo;
  TempList: PPropList;
  LObject: TObject;
begin
  Count := GetPropList(AObject, TempList);
  if Count > 0 then
  try
    for I := 0 to Count - 1 do
    begin
      PropInfo := TempList^[I];
      if (PropInfo^.PropType^.Kind = tkClass) and
         Assigned(PropInfo^.GetProc) and
         Assigned(PropInfo^.SetProc) then
      begin
        LObject := GetObjectProp(AObject, PropInfo);
        if LObject <> nil then
        begin
          SetObjectProp(AObject, PropInfo, nil);
          LObject.Free;
        end;
      end;
    end;
  finally
    FreeMem(TempList);
  end;
end;

procedure CollectionToCSV( fname: string; col: TCollection );
var
  i: integer;
  j: integer;
  propCount: Integer;
  TempList: PPropList;
  wr: TCSWriter;
begin
  propcount := GetPropList(col.ItemClass.ClassInfo, TempList);
  if PropCount > 0 then
  try
    wr := TCSWriter.Create( fname );
    try
      for j := 0 to propCount - 1 do
        wr.Write( TempList^[j].Name );
      wr.WriteEndOfRow;
      for i := 0 to col.Count-1 do
      begin
        for j := 0 to propCount - 1 do
          wr.Write( GetPropValue( col.Items[i], TempList^[j].Name, True) );
        wr.WriteEndOfRow;
      end;
    finally
      FreeAndNil( wr ); 
    end;
  finally
    FreeMem(TempList);
  end;
end;

{ TCSWriter }

constructor TCSWriter.Create(ostream: TStream; autodel: boolean);
begin
  FOutStream := ostream;
  FAutoDelete := autodel;
  FBeginOfLine := true;
  FSeparator := ',';
  FPreprocessing := true;
end;

constructor TCSWriter.Create(filename: string);
var
  fstr: TFileStream;
begin
  fstr := TFileStream.Create( filename, fmCreate or fmShareExclusive );
  Create( fstr, true );
end;

destructor TCSWriter.Destroy;
begin
  if FAutoDelete then
    FOutStream.Free;
end;

procedure TCSWriter.WriteBuffer( buffer: string );
var
  temp: string;
  function HaveSpecialChars( s: string ):boolean;
  var
    i:integer;
  begin
    Result := true;
    for i := 1 to length( s ) do
      if s[i] in [#1..' ','"',','] then
        exit;
    Result := false;
  end;
begin
  temp := StringReplace( buffer, '"', '""' , [rfReplaceAll] );;
  temp := StringReplace( temp, #13+#10, ' ' , [rfReplaceAll] );
  if FPreprocessing and HaveSpecialChars(temp) or (Trim(temp) = '') then
    temp := '"'+temp+'"';
 // if FPreprocessing then
 //   temp := StringReplace( temp, ',', ' ' , [rfReplaceAll] );

  FOutStream.Write( pchar(temp)^, length(temp) );
end;

procedure TCSWriter.Write(field: TField);
var
  saveShortDateFormat: string;
begin
  saveShortDateFormat := ShortDateFormat;
  ShortDateFormat := 'mm/dd/yyyy';

  Write( field.AsString );

  ShortDateFormat := saveShortDateFormat;
end;

procedure TCSWriter.WriteEndOfRow;
var
  s:string;
begin
  FBeginOfLine := true;
  s := #13 + #10;
  FOutStream.Write( pchar(s)^, length(s) );
end;

procedure TCSWriter.Write(field: string);
begin
  if not FBeginOfLine then
    FOutStream.Write( pchar(FSeparator)^, length(FSeparator) );
   //   FOutStream.Write( pchar(#13 + #10)^, length(FSeparator) );
  FBeginOfLine := false;
  WriteBuffer( field );
end;



procedure TCSWriter.WriteRow(fields: array of string);
var
  i: integer;
begin
  for i := low(fields) to high(fields) do
    Write( fields[i] );
  WriteEndOfRow;
end;

end.

