unit gdycommonlogger;

interface

uses
  gdylogger, gdyloggerimpl, gdylogwriters;

type
  ICommonLogger = interface
['{5ECA5C05-2875-4A2B-987E-243E0AB3E810}']
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call

    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string; d: string = '' );
    procedure LogEventFmt( s: string; const args: array of const; d: string = '' );
    procedure LogWarning( s: string; d: string = '' );
    procedure LogWarningFmt( s: string; const args: array of const; d: string = '' );
    procedure LogError( s: string; d: string = '' );
    procedure LogErrorFmt( s: string; const args: array of const; d: string = '' );
    procedure LogDebug( s: string; fulltext: string = '' );

    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
  end;

  TCommonLoggerKeeper = class
  private
    FStatefulLogger: IStatefulLogger;
    FLogger: ICommonLogger;

    FGivenLogWriter: ILoggerEventSink;

    FDevLogOutput: IFileLogOutput;
    FDevLogWriter: ILoggerEventSink;

    FUserLogOutput: IFileLogOutput;
    FUserLogWriter: ILoggerEventSink;
  public
    constructor Create( aSink: ILoggerEventSink );
    destructor Destroy; override;

    function Logger: ICommonLogger;
    property StatefulLogger: IStatefulLogger read FStatefulLogger;

    procedure StartDevLoggingToFile( filename: string );
    procedure StopDevLoggingToFile;
    function DevLogFileName: string;
    procedure CloseDevLogFileTemporarily;

    procedure StartLoggingToFile( filename: string );
    procedure StopLoggingToFile;
    function UserLogFileName: string;
    procedure CloseUserLogFileTemporarily;

  end;

const
  endl2 = gdylogger.endl2;

function CreateCommonLogger( aLogger: IExceptionLogger ): ICommonLogger;

type
	TLoggingProc = procedure( aLogger: ICommonLogger );

procedure WithConsoleLogger( logname: string; p: TLoggingProc );

implementation

uses
  sysutils;

type
  TCommonLogger = class( TInterfacedObject, ICommonLogger)
  private
    FLogger: IExceptionLogger;
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call

    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string; d: string = '' );
    procedure LogEventFmt( s: string; const args: array of const; d: string = '' );
    procedure LogWarning( s: string; d: string );
    procedure LogWarningFmt( s: string; const args: array of const; d: string );
    procedure LogError( s: string; d: string = '' );
    procedure LogErrorFmt( s: string; const args: array of const; d: string = '' );
    procedure LogDebug( s: string; fulltext: string = '' );

    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
  public
    constructor Create( aLogger: IExceptionLogger );
  end;


function CreateCommonLogger( aLogger: IExceptionLogger ): ICommonLogger;
begin
  Result := TCommonLogger.Create( aLogger ) as ICommonLogger;
end;

{ TCommonLogger }

constructor TCommonLogger.Create(aLogger: IExceptionLogger);
begin
  FLogger := aLogger;
end;

procedure TCommonLogger.PassthroughException;
begin
  FLogger.PassthroughException;
end;

procedure TCommonLogger.StopException;
begin
  FLogger.StopException;
end;

procedure TCommonLogger.PassthroughExceptionAndWarnFmt(s: string; const args: array of const);
begin
  FLogger.PassthroughExceptionAndLogMessage( lmWarning, Format( s, args), '' ); //look at LogWarning impl
end;

procedure TCommonLogger.StopExceptionAndWarnFmt(s: string; const args: array of const);
begin
  FLogger.StopExceptionAndLogMessage( lmWarning, Format( s, args), '' ); //look at LogWarning impl
end;

procedure TCommonLogger.LogContextItem(tag, value: string);
begin
  FLogger.LogContextItem( tag, value );
end;

procedure TCommonLogger.LogDebug(s, fulltext: string);
begin
  FLogger.LogMessage( lmDebug, s, fulltext );
end;

procedure TCommonLogger.LogEntry(blockname: string);
begin
  FLogger.LogEntry( blockname );
end;

procedure TCommonLogger.LogError(s: string; d: string);
begin
  FLogger.LogMessage( lmError, s, d );
end;

procedure TCommonLogger.LogErrorFmt(s: string; const Args: array of const; d: string);
begin
  LogError( Format( s, args ), d );
end;


procedure TCommonLogger.LogEvent(s: string; d: string = '' );
begin
  FLogger.LogMessage( lmEvent, s, d );
end;

procedure TCommonLogger.LogEventFmt(s: string;
  const args: array of const; d: string = '' );
begin
  LogEvent( Format( s, args ), d );
end;

procedure TCommonLogger.LogExit;
begin
  FLogger.LogExit;
end;

procedure TCommonLogger.LogWarning(s: string; d: string );
begin
  FLogger.LogMessage( lmWarning, s, d );
end;

procedure TCommonLogger.LogWarningFmt(s: string; const args: array of const; d: string);
begin
  LogWarning( Format( s, args ), d );
end;

{TCommonLoggerKeeper}

constructor TCommonLoggerKeeper.Create( aSink: ILoggerEventSink );
begin
  FGivenLogWriter := aSink;
  FStatefulLogger := CreateStatefulLogger;
  if assigned( FGivenLogWriter ) then
    FStatefulLogger.Advise( FGivenLogWriter );
  FLogger := CreateCommonLogger( CreateExceptionLogger( FStatefulLogger as ILogger ) );
end;

destructor TCommonLoggerKeeper.Destroy;
begin
  FLogger.LogDebug( 'Stopping to write developer log to file' );
  FStatefulLogger.UnAdvise( FDevLogWriter );
  FDevLogWriter := nil;
  FLogger.LogDebug( 'Stopped to write developer log to file' );
  StopLoggingToFile;
  if assigned( FGivenLogWriter ) then
    FStatefulLogger.UnAdvise( FGivenLogWriter );
  inherited;
end;

function TCommonLoggerKeeper.Logger: ICommonLogger;
begin
  Result := FLogger;
end;

procedure TCommonLoggerKeeper.StartDevLoggingToFile(filename: string);
begin
  FLogger.LogDebug( 'Starting to write developer''s log to file <' + filename + '>' );
  FStatefulLogger.UnAdvise( FDevLogWriter );

  FDevLogOutput := TFileLogOutput.Create(filename);
  FDevLogWriter := TPlainTextDevLogWriter.Create(FDevLogOutput as ILogOutput);
  FStatefulLogger.Advise( FDevLogWriter );

  FLogger.LogDebug( 'Started to write developer''s log to file <' + filename + '>' );
  FLogger.LogDebug( 'System Information', MadExceptSystemInfo);
end;

procedure TCommonLoggerKeeper.StopDevLoggingToFile;
begin
  FLogger.LogDebug( 'Stopping to write developer''s log to file' );
  FStatefulLogger.UnAdvise( FDevLogWriter );
  FDevLogWriter := nil;
  FDevLogOutput := nil;
  FLogger.LogDebug( 'Stopped to write developer''s log to file' );
end;

procedure TCommonLoggerKeeper.StartLoggingToFile(filename: string);
begin
  FLogger.LogDebug( 'Starting to write user log to file <' + filename + '>' );
  FStatefulLogger.UnAdvise( FUserLogWriter );

  FUserLogOutput := TFileLogOutput.Create(filename);
  FUserLogWriter := TPlainTextUserLogWriter.Create( FUserLogOutput as ILogOutput);
  FStatefulLogger.Advise( FUserLogWriter );

  FLogger.LogDebug( 'Started to write user log to file <' + filename + '>' );
end;

procedure TCommonLoggerKeeper.StopLoggingToFile;
begin
  FLogger.LogDebug( 'Stopping to write user log to file' );
  FStatefulLogger.UnAdvise( FUserLogWriter );
  FUserLogWriter := nil;
  FUserLogOutput := nil;
  FLogger.LogDebug( 'Stopped to write user log to file' );
end;

procedure WithConsoleLogger( logname: string; p: TLoggingProc );
var
  lk: TCommonLoggerKeeper;
begin
  try
    lk := TCommonLoggerKeeper.Create( nil );
    try
      lk.StartLoggingToFile( logname );
      try
	    p( lk.Logger );
      finally
        lk.StopLoggingToFile;
      end;
    finally
      FreeAndNil( lk );
    end;
  except
    SysUtils.ShowException(ExceptObject, ExceptAddr);
  end;
end;

function TCommonLoggerKeeper.DevLogFileName: string;
begin
  if FDevLogOutput <> nil then
    Result := FDevLogOutput.Filename
  else
    Result := '';
end;

function TCommonLoggerKeeper.UserLogFileName: string;
begin
  if FUserLogOutput <> nil then
    Result := FUserLogOutput.Filename
  else
    Result := '';
end;

procedure TCommonLoggerKeeper.CloseDevLogFileTemporarily;
begin
  if FDevLogOutput <> nil then
    FDevLogOutput.CloseFileTemporarily;
end;

procedure TCommonLoggerKeeper.CloseUserLogFileTemporarily;
begin
  if FUserLogOutput <> nil then
    FUserLogOutput.CloseFileTemporarily;
end;

end.
