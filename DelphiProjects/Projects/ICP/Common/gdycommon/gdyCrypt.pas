unit gdyCrypt;


interface


function Encrypt( data: string; key: string ): string;
function Decrypt( data: string; key: string ): string;

function EncryptBinaryString( data: string; key: string ): string;
function DecryptBinaryString( data: string; key: string ): string;

implementation

uses
  elaes, gdycommon, math, classes, sysutils;

function EncryptBinaryString( data: string; key: string ): string;
var
  source: TStringStream;
  dest: TStringStream;
  size: integer;
  aesKey: TAESKey128;
  i: integer;
begin
  for i := 0 to High(aesKey) do
    aesKey[i] := 0;
  for i := 0 to Min(Length(key)-1, High(aesKey)) do
    aesKey[i] := byte(key[i+1]);

  Source := TStringStream.Create('');
  try
    Size := Length(data);
  	Source.WriteBuffer(Size, SizeOf(Size));
  	Source.WriteString(data);
    Dest := TStringStream.Create( '' );
    try
      EncryptAESStreamECB( Source, 0, aesKey, Dest );
      Result := Dest.DataString;
    finally
      FreeAndNil( Dest );
    end;
  finally
    FreeAndNil( Source );
  end;
end;

function DecryptBinaryString( data: string; key: string ): string;
var
  Source: TStringStream;
  Dest: TStringStream;
  size: integer;
  aesKey: TAESKey128;
  i: integer;
begin
  for i := 0 to High(aesKey) do
    aesKey[i] := 0;
  for i := 0 to Min(Length(key)-1, High(aesKey)) do
    aesKey[i] := byte(key[i+1]);

  Source := TStringStream.Create( data );
  try
    Dest := TStringStream.Create( '' );
    try
      DecryptAESStreamECB(Source, 0, aesKey, Dest);
      dest.Position := 0;
      dest.ReadBuffer(size, SizeOf(size));
      Result := Dest.ReadString(size);
    finally
      FreeAndNil( Dest );
    end;
  finally
    FreeAndNil( Source );
  end;
end;

function EncryptPadding( data: string; key: string ): string;
var
  source: TStringStream;
  dest: TStringStream;
  aesKey: TAESKey128;
  i: integer;
begin
  for i := 0 to High(aesKey) do
    aesKey[i] := 0;
  for i := 0 to Min(Length(key)-1, High(aesKey)) do
    aesKey[i] := byte(key[i+1]);

  Source := TStringStream.Create( data );
  try
    Dest := TStringStream.Create( '' );
    try
      EncryptAESStreamECB( Source, 0, aesKey, Dest );
      Result := Dest.DataString;
    finally
      FreeAndNil( Dest );
    end;
  finally
    FreeAndNil( Source );
  end;
end;

function DecryptPadding( data: string; key: string ): string;
var
  Source: TStringStream;
  Dest: TStringStream;
  aesKey: TAESKey128;
  i: integer;
begin
  for i := 0 to High(aesKey) do
    aesKey[i] := 0;
  for i := 0 to Min(Length(key)-1, High(aesKey)) do
    aesKey[i] := byte(key[i+1]);

  Source := TStringStream.Create( data );
  try
    Dest := TStringStream.Create( '' );
    try
      DecryptAESStreamECB(Source, 0, aesKey, Dest);
      Result := Dest.DataString;
    finally
      FreeAndNil( Dest );
    end;
  finally
    FreeAndNil( Source );
  end;
end;

function Decrypt( data: string; key: string ): string;
var
  padded: string;
begin
  padded := DecryptPadding(data, key);
  SetString(Result, PChar(padded), strlen(PChar(padded)));
end;

function Encrypt( data: string; key: string ): string;
begin
  Result := EncryptPadding(data, key);
end;


end.