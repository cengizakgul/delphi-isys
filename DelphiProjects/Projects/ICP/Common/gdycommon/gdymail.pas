unit gdymail;

interface

procedure SimpleMailTo( recipient: string; subject, body: string ); overload;
procedure SimpleMailTo( recipient: string; subject, body, attachment: string ); overload;


implementation

uses
  sendmail, classes, sysutils;
  
procedure SimpleMailTo( recipient: string; subject, body: string );
begin
  SimpleMailTo( recipient, subject, body, '' );
end;

procedure SimpleMailTo( recipient: string; subject, body, attachment: string );
var
  recipients, attachments: TStringList;
begin
  recipients := nil;
  attachments := nil;
  try
    if trim( recipient ) <> '' then
    begin
      recipients := TStringList.Create;
      recipients.Text := recipient;
    end;
    if trim( attachment ) <> '' then
    begin
      attachments := TStringList.Create;
      attachments.Text := attachment;
    end;
    MailTo( recipients, nil, nil, attachments, subject, body, true );
  finally
    FreeAndNil( attachments );
    FreeAndNil( recipients );
  end;
end;

end.
