unit gdyClasses;
{$INCLUDE gdy.inc}
interface

uses
  controls, classes, comctrls, windows;

type
  IUsageLock = interface
    ['{E7184E40-066E-450E-964C-FCDDFE2680FD}']
    procedure Lock;
    procedure Unlock;
    function  IsLocked: boolean;
  end;

  IWaitIndicator = interface
    procedure StartWait(op: string);
    procedure EndWait;
  end;

  IStringMapping = interface
    function CreateMapping( s: string ): integer;
    function GetMapping( tag: integer ): string;
  end;

  IProgressIndicator = interface
['{3DD66A5F-EB4B-4D6F-BDF2-B57966721E8B}']
    procedure StartWait( name: string; total: integer );
    procedure Step( step: integer );
    procedure EndWait;
  end;

  TUsageLock = class( TInterfacedObject, IUsageLock )
  private
    FLockCount: cardinal;
  public
    procedure Lock;
    procedure Unlock;
    function  IsLocked: boolean;
    procedure Reset;
    property LockCount: cardinal read FLockCount write FLockCount;
  end;

  THourglassWaitIndicator = class( TInterfacedObject, IWaitIndicator )
  private
    FOldScreenCursor: TCursor;
    FCursorChangeCount: integer;
  public
    procedure StartWait(op: string);
    procedure EndWait;
  end;

{$ifdef GDYCOMMON}
  TStatusBarProgressIndicator = class( TInterfacedObject, IProgressIndicator )
  private
    FStatusBar: TStatusBar;
    FTotal, FCurrent: integer;
    FCaption: string;
    FOldDrawPanelHandler: TDrawPanelEvent;
    procedure UpdateWait;
    procedure StatusBarDrawPanel(
        StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);

  public
    constructor Create( sb: TStatusBar );
    procedure StartWait( name: string; total: integer );
    procedure Step( step: integer );
    procedure EndWait;
  end;

  TWaitProgressIndicator = class( TInterfacedObject, IProgressIndicator )
  private
    FProgressindicator: IProgressIndicator;
    FWaitIndicator: IWaitIndicator;
  public
    constructor Create(pi: IProgressIndicator; wi: IWaitIndicator);
    procedure StartWait( name: string; total: integer );
    procedure Step( step: integer );
    procedure EndWait;
  end;

{$endif}

  TNullProgressIndicator = class( TInterfacedObject, IProgressIndicator )
  public
    procedure StartWait( name: string; total: integer );
    procedure Step( step: integer );
    procedure EndWait;
  end;

  function NullProgressIndicator: IProgressIndicator;

type
  TCombinedWaitIndicator = class( TInterfacedObject, IWaitIndicator )
  private
    FWaitIndicator1: IWaitIndicator;
    FWaitIndicator2: IWaitIndicator;
  public
    constructor Create(wi1: IWaitIndicator; wi2: IWaitIndicator);
    procedure StartWait( op: string);
    procedure EndWait;
  end;

type
  TStringMapping = class( TInterfacedObject, IStringMapping )
  private
    FMapping: TStringList;
  public
    constructor Create;
    destructor Destroy; override;

    {IStringMapping}
    function CreateMapping( s: string ): integer;
    function GetMapping( tag: integer ): string;
  end;

  TDummyUnknownImpl = class( TObject, IUnknown )
  public
    // mimics TComponent implementation of IUnknown
    function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

  TNexusEvent = procedure(Sender: TObject; AComponent: TComponent) of object;

  TNexus = class(TComponent)
  private
    FOnFreeNotify: TNexusEvent;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    property OnFreeNotify: TNexusEvent read FOnFreeNotify write FOnFreeNotify;
  end;

  IStr = interface
['{378231A5-8033-43A6-83F3-A6FBFAA7899F}']
    procedure SetCSV(text: string);
    procedure SetStdCSV(text: string);
    procedure SetCharSeparatedText(text: string; sep: char);
    procedure SetText(text: string);

    procedure Add(s: string);
    procedure TrimAll;

    procedure Delete(i: integer);
    function Count: integer;
    function GetStr(Idx: integer): string;
    function AsTStrings: TStrings;
    function Clone: IStr;
    property Str[Idx: integer]: string read GetStr;
  end;

function CsvAsStr( text: string ): IStr;
function StdCsvAsStr( text: string ): IStr;
function SplitByChar(text: string; sep: char): IStr;
function SplitToLines( text: string ): IStr;
function EmptyIStr: IStr;

type

  TKeyPairList = class
  private
    FList: Variant;
    procedure IncLength;
    function GetLeftKey(i: integer): Variant;
    function GetRightKey(i: integer): Variant;
  public
//    constructor Create;
//    destructor Destroy; override;
    procedure AddPair( left, right: Variant );
    function Count: integer;
    property LeftKey[i: integer]: Variant read GetLeftKey;
    property RightKey[i: integer]: Variant read GetRightKey;
  end;



implementation

uses
  sysutils, 
  forms 
  {$ifdef GDYCOMMON}, gdycommon{$ENDIF}
  {$ifdef D6_UP}, variants{$endif};

{ TUsageLock }

function TUsageLock.IsLocked: boolean;
begin
  Result := FLockCount > 0;
end;

procedure TUsageLock.Lock;
begin
  inc( FLockCount );
end;

procedure TUsageLock.Reset;
begin
  LockCount := 0;
end;

procedure TUsageLock.Unlock;
begin
  if FLockCount > 0 then
    dec( FLockCount )
  else
    raise Exception.Create( 'Unlock called for non-locked object' );
end;


{ TWaitIndicator }

procedure THourglassWaitIndicator.StartWait(op: string);
begin
  inc( FCursorChangeCount );
  if FCursorChangeCount = 1 then
  begin
    FOldScreenCursor := Screen.Cursor;
    Screen.Cursor := crHourGlass;
  end;
end;

procedure THourglassWaitIndicator.EndWait;
begin
  if FCursorChangeCount > 0 then
  begin
    dec( FCursorChangeCount );
    if FCursorChangeCount = 0 then
      Screen.Cursor := FOldScreenCursor;
  end;
end;


{ TStringMapping }

constructor TStringMapping.Create;
begin
  FMapping := TStringList.Create;
end;

function TStringMapping.CreateMapping(s: string): integer;
begin
  Result := FMapping.IndexOf( s );
  if Result = -1 then
    Result := FMapping.Add( s );
end;

destructor TStringMapping.Destroy;
begin
  FreeAndNil( FMapping );
  inherited;
end;

function TStringMapping.GetMapping(tag: integer): string;
begin
  Result := FMapping[tag];
end;

function TDummyUnknownImpl.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;
end;

function TDummyUnknownImpl._AddRef: Integer;
begin
  Result := -1   // -1 indicates no reference counting is taking place
end;

function TDummyUnknownImpl._Release: Integer;
begin
  Result := -1   // -1 indicates no reference counting is taking place
end;

{$ifdef GDYCOMMON}

{ TStatusBarProgressIndicator }

constructor TStatusBarProgressIndicator.Create(sb: TStatusBar);
begin
  FStatusBar := sb;
  FStatusBar.DoubleBuffered := true;
end;

procedure TStatusBarProgressIndicator.EndWait;
begin
  try
    FCaption := '';
    FCurrent := 0;
    FTotal := 0;
    FStatusBar.Repaint;
    FStatusBar.OnDrawPanel := FOldDrawPanelHandler;
  except
  end;
end;

procedure TStatusBarProgressIndicator.StartWait(name: string;
  total: integer);
begin
  try
    FCaption := name;
    FCurrent := 0;
    FTotal := total;
    UpdateWait;
    FOldDrawPanelHandler := FStatusBar.OnDrawPanel;
    FStatusBar.OnDrawPanel := StatusBarDrawPanel;
  except
  end;
end;

procedure TStatusBarProgressIndicator.StatusBarDrawPanel(
  StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
begin
  try
    if Panel.Index = 0 then
      DrawProgressIndicator( StatusBar.Canvas, Rect, FCaption, FCurrent, FTotal );
    if assigned(FOldDrawPanelHandler) then
      FOldDrawPanelHandler( StatusBar, Panel, Rect )
  except
  end;
end;

procedure TStatusBarProgressIndicator.Step(step: integer);
begin
  try
    FCurrent := step;
    UpdateWait;
  except
  end;
end;

procedure TStatusBarProgressIndicator.UpdateWait;
begin
  FStatusBar.Repaint;
  RedrawIfNeeded;
end;

{TNullProgressIndicator}



{$endif}

procedure TNullProgressIndicator.Step(step: integer);
begin
end;

procedure TNullProgressIndicator.EndWait;
begin
end;

procedure TNullProgressIndicator.StartWait(name: string; total: integer);
begin
end;

function NullProgressIndicator: IProgressIndicator;	
begin
	Result := TNullProgressIndicator.Create;
end;

{ TNexus }

procedure TNexus.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  if (Operation = opRemove) and Assigned(FOnFreeNotify) then
    FOnFreeNotify(Self, AComponent);
  inherited Notification(AComponent, Operation);
end;

type
  TStr = class (TInterfacedObject, IStr)
  private
    FSL: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetCSV(text: string);
    procedure SetStdCSV(text: string);
    procedure SetCharSeparatedText( text: string; sep: char );
    procedure SetText( text: string );

    procedure Add(s: string);
    procedure TrimAll;

    function Count: integer;
    function GetStr(Idx: integer): string;
    procedure Delete(Idx: integer);
    function AsTStrings: TStrings;
    function Clone: IStr;
    property Str[Idx: integer]: string read GetStr;
  end;


{ TStr }

function TStr.Count: integer;
begin
  Result := FSL.Count;
end;

constructor TStr.Create;
begin
  FSL := TStringList.Create;
end;

destructor TStr.Destroy;
begin
  FreeAndNil( FSL );
  inherited;
end;

function TStr.GetStr(Idx: integer): string;
begin
  Result := FSL[idx];
end;

{$ifdef D6_UP}

procedure _SetDelimitedTextStd(strings: TStrings; const Value: string; aDelim: char);
var
  P, P1: PChar;
  S: string;
begin
  strings.BeginUpdate;
  try
    strings.Clear;
    P := PChar(Value);
    while P^ <> #0 do
    begin
      if P^ = strings.QuoteChar then
        S := AnsiExtractQuotedStr(P, strings.QuoteChar)
      else
      begin
        P1 := P;
        while (P^ <> #0) and (P^ <> aDelim) do
          P := CharNext(P);
        SetString(S, P1, P - P1);
      end;
      strings.Add(S);
      if P^ = aDelim then
      begin
        P := CharNext(P);
        if P^ = #0 then
          strings.Add('');
      end;
    end;
  finally
    strings.EndUpdate;
  end;
end;

procedure _SetDelimitedText(strings: TStrings; const Value: string; aDelim: char);
const
  sSpace: set of char = [#1..' '];
var
  P, P1: PChar;
  S: string;
begin
  strings.BeginUpdate;
  try
    strings.Clear;
    P := PChar(Value);
    while P^ in sSpace do
      P := CharNext(P);
    while P^ <> #0 do
    begin
      if P^ = strings.QuoteChar then
        S := AnsiExtractQuotedStr(P, strings.QuoteChar)
      else
      begin
        P1 := P;
        while not (P^ in sSpace) and (P^ <> #0) and (P^ <> aDelim) do
          P := CharNext(P);
        SetString(S, P1, P - P1);
      end;
      strings.Add(S);
      while P^ in sSpace do
        P := CharNext(P);
      if P^ = aDelim then
      begin
        repeat
          P := CharNext(P);
        until not (P^ in sSpace);
        if P^ = #0 then
          strings.Add('');
      end
      else if P^ <> #0 then
        raise Exception.Create('Invalid delimited text');
    end;
  finally
    strings.EndUpdate;
  end;
end;

{$else}
procedure _SetDelimitedText( strings: TStrings; const Value: string; aDelim: char );
const
  sSpace: set of char = [#1..' '];
var
  P, P1: PChar;
  S: string;
begin
  strings.BeginUpdate;
  try
    strings.Clear;
    P := PChar(Value);
    while P^ in sSpace do
      P := CharNext(P);
    while P^ <> #0 do
    begin
      if P^ = '"' then
        S := AnsiExtractQuotedStr(P, '"')
      else
      begin
        P1 := P;
        while not (P^ in sSpace) and (P^ <> #0) and (P^ <> aDelim) do
          P := CharNext(P);
        SetString(S, P1, P - P1);
      end;
      strings.Add(S);
      while P^ in sSpace do
        P := CharNext(P);
      if P^ = aDelim then
      begin
        repeat
          P := CharNext(P);
        until not (P^ in sSpace);
        if P^ = #0 then
          strings.Add('');
      end;
    end;
  finally
    strings.EndUpdate;
  end;
end;
{$endif}

procedure TStr.SetCSV(text: string);
begin
  _SetDelimitedText( FSL, text, ',' );
end;

procedure TStr.SetStdCSV(text: string);
begin
  _SetDelimitedTextStd( FSL, text, ',' );
end;

procedure _SetCharSeparatedText( strings: TStrings; x: char; const Value: string);
var
  P, P1: PChar;
  S: string;
begin
  strings.BeginUpdate;
  try
    strings.Clear;
    P := PChar(Value);
    while P^ <> #0 do
    begin
      P1 := P;
      while (P^ <> x) and (P^ <> #0) do
        P := CharNext(P);
      SetString(S, P1, P - P1);
      strings.Add(S);
      if P^ = x then
      begin
        P := CharNext(P);
        if P^ = #0 then
          strings.Add('');
      end;
    end;
  finally
    strings.EndUpdate;
  end;
end;

procedure TStr.SetCharSeparatedText(text: string; sep: char);
begin
  _SetCharSeparatedText( FSL, sep, text );
end;

procedure TStr.SetText(text: string);
begin
  FSL.Text := text;
end;

function CsvAsStr(text: string): IStr;
begin
  Result := TStr.Create as IStr;
  Result.SetCSV(text);
end;

function StdCsvAsStr(text: string): IStr;
begin
  Result := TStr.Create as IStr;
  Result.SetStdCSV(text);
end;

function SplitByChar(text: string; sep: char): IStr;
begin
  Result := TStr.Create as IStr;
  Result.SetCharSeparatedText(text, sep);
end;

function SplitToLines( text: string ): IStr;
begin
  Result := TStr.Create as IStr;
  Result.SetText(text);
end;

function EmptyIStr: IStr;
begin
  Result := TStr.Create as IStr;
end;

procedure TStr.Delete(Idx: integer);
begin
  FSL.Delete(idx);
end;

procedure TStr.Add(s: string);
begin
  FSL.Add(s);
end;

procedure TStr.TrimAll;
var
  i: integer;
begin
  for i := 0 to FSL.Count-1 do
    FSL[i] := trim(FSL[i]);
end;

function TStr.AsTStrings: TStrings;
begin
  Result := FSL;
end;

function TStr.Clone: IStr;
var
  r: TStr;
begin
  r := TStr.Create;
  r.FSL.Assign(FSL);
  Result := r;
end;

{ TKeyPairList }

procedure TKeyPairList.AddPair(left, right: Variant);
var
  v: Variant;
begin
  IncLength;
  v := VarArrayCreate( [0,1], varVariant );
  v[0] := left;
  v[1] := right;
  FList[VarArrayHighBound(FList, 1)] := v;
end;

function TKeyPairList.Count: integer;
begin
  if VarIsNull( FList ) then
    Result := 0
  else
    Result := VarArrayHighBound(FList,1) - VarArrayLowBound(FList,1) + 1;
end;

function TKeyPairList.GetLeftKey(i: integer): Variant;
begin
  Result := FList[i][0];
end;

function TKeyPairList.GetRightKey(i: integer): Variant;
begin
  Result := FList[i][1];
end;

procedure TKeyPairList.IncLength;
begin
  if VarIsEmpty(FList) then
    FList := VarArrayCreate( [0,0], varVariant )
  else
    VarArrayRedim( FList, VarArrayHighBound(FList,1) + 1 );
end;

{$ifdef GDYCOMMON}
	{.$R 'gdyimages.res' 'gdyimages.rc'}
{$endif}

{ TWaitProgressIndicator }

constructor TWaitProgressIndicator.Create(pi: IProgressIndicator;
  wi: IWaitIndicator);
begin
  FProgressindicator := pi;
  FWaitIndicator := wi;
end;

procedure TWaitProgressIndicator.EndWait;
begin
  FProgressindicator.EndWait;
  FWaitIndicator.EndWait;
end;

procedure TWaitProgressIndicator.StartWait(name: string; total: integer);
begin
  FProgressindicator.StartWait(name, total);
  FWaitIndicator.StartWait(name);
end;

procedure TWaitProgressIndicator.Step(step: integer);
begin
  FProgressindicator.Step(step);
end;

{ TCombinedWaitIndicator }

constructor TCombinedWaitIndicator.Create(wi1, wi2: IWaitIndicator);
begin
  FWaitIndicator1 := wi1;
  FWaitIndicator2 := wi2;
end;

procedure TCombinedWaitIndicator.EndWait;
begin
  try
    FWaitIndicator1.EndWait;
  finally
    FWaitIndicator2.EndWait;
  end;
end;

procedure TCombinedWaitIndicator.StartWait(op: string);
begin
  FWaitIndicator1.StartWait(op);
  try
    FWaitIndicator2.StartWait(op);
  except
    FWaitIndicator1.EndWait;
    raise;
  end;
end;

end.


