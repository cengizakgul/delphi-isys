unit gdyWaitForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, gdyClasses, StdCtrls;

type
  TWaitIndicatorForm = class(TForm)
    lblMessage: TLabel;
  private
    procedure SetOperationName(const Value: string);
  public
    constructor Create(Owner: TComponent); override;
    property OperationName: string write SetOperationName;
  end;

  TLargeWaitIndicator = class( TInterfacedObject, IWaitIndicator )
  private
    FForm: TWaitIndicatorForm;
    FOwner: TComponent;
    FOperations: array of string;
    procedure AssertInvariant;
  public
    constructor Create(Owner: TComponent); reintroduce;
    destructor Destroy; override;
    procedure StartWait(op: string);
    procedure EndWait;
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TWaitFm }

constructor TWaitIndicatorForm.Create(Owner: TComponent);
begin
  inherited;
  Caption := Application.Title + ' - Please wait';
end;

procedure TWaitIndicatorForm.SetOperationName(const Value: string);
begin
  lblMessage.Caption := Value;
end;

{ TLargeWaitIndicator }

procedure TLargeWaitIndicator.AssertInvariant;
begin
  Assert( not ((Length(FOperations) = 0) xor (FForm = nil)) );
end;

constructor TLargeWaitIndicator.Create(Owner: TComponent);
begin
  FOwner := Owner;
end;

destructor TLargeWaitIndicator.Destroy;
begin
  Assert(FForm = nil);
  inherited;
end;

procedure TLargeWaitIndicator.EndWait;
begin
  AssertInvariant;
  Assert(Length(FOperations) > 0);
  SetLength(FOperations, Length(FOperations)-1);
  if Length(FOperations) = 0 then
    FreeAndNil(FForm)
  else
  begin
    FForm.OperationName := Join(FOperations, #13#10);;
    FForm.Repaint;
  end;
  AssertInvariant;
end;

procedure TLargeWaitIndicator.StartWait(op: string);
begin
  AssertInvariant;
  SetLength(FOperations, Length(FOperations)+1);
  FOperations[high(FOperations)] := op;
  if Length(FOperations) = 1 then
  begin
    FForm := TWaitIndicatorForm.Create(FOwner);
    FForm.OperationName := Join(FOperations, #13#10);
    if Application.MainForm.Visible then  //for tray applications with hidden main window
      FForm.Show;
  end
  else
  begin
    FForm.OperationName := Join(FOperations, #13#10);;
  end;
  FForm.Repaint;
  AssertInvariant;
end;

end.


