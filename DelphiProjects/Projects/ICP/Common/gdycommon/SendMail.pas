unit SendMail;

interface

uses
  Classes, MAPI, SysUtils, Forms;

procedure MailTo(Recipients, CC, BCC, AttachedFiles: TStringList; Subject, Body: string; bShowDlg: Boolean = True);

implementation

procedure MailTo(Recipients, CC, BCC, AttachedFiles: TStringList; Subject, Body: string; bShowDlg: Boolean = True);
var
  Recips: packed array of MAPIREcipDesc;

  procedure AddRecipients (RecipClass: cardinal; S: TStringList);
  var i: integer;
  begin
    if S<>nil then
    begin
      Setlength (REcips, length(Recips)+S.Count);
      for i:=0 to S.Count-1 do
        with Recips[length(Recips)-S.Count+i] do
        begin
          ulReserved := 0;
          ulRecipClass := REcipClass;
          lpszName := StrNew(PChar(S[i]));
          lpszAddress := StrNew(PChar('SMTP:'+S[i]));
          ulEIDSize := 0;
          lpEntryID := nil;
        end
    end
  end;

var
  MapiMsg: MapiMessage;
  i: integer;
  Orig: MAPIRecipdesc;
  AttFiles: packed array of MapiFileDesc;
  flFlags: Cardinal;
begin
  FillChar (MapiMsg, sizeof(MapiMSG), 0);
  MapiMsg.lpszSubject := PChar (Subject);
  MapiMSg.lpszNoteText := PChar (Body);
  AddRecipients (MAPI_TO, Recipients);
  AddRecipients (MAPI_CC, CC);
  AddRecipients (MAPI_BCC, BCC);
  try
    MapiMsg.nRecipCount := length(Recips);
    MapiMsg.lpRecips := pointer(Recips);
    Fillchar (Orig, sizeof(Orig), 0);
    MapiMsg.lpOriginator := @Orig;
    if AttachedFiles<>nil then
    begin
      MapiMsg.nFileCount := AttachedFiles.Count;
      SetLength (AttFiles, AttachedFiles.Count);
      for i:=0 to AttachedFiles.Count-1 do
        with AttFiles[i] do
        begin
          ulReserved := 0;
          flFlags := 0;
          nPosition := $FFFFFFFF;
          lpszPathName := PChar(AttachedFiles[i]);
          lpszFileName := nil;
          lpFileType := nil;
        end;
      MapiMsg.lpFiles := pointer(AttFiles);
    end;
    if bShowDlg then
      flFlags := MAPI_DIALOG or MAPI_LOGON_UI or MAPI_NEW_SESSION or MAPI_USE_DEFAULT
    else
      flFlags := MAPI_LOGON_UI or MAPI_NEW_SESSION;
    case MAPISendMail (0, Application.Handle, MapiMsg, flFlags, 0)of
      SUCCESS_SUCCESS: ;
      MAPI_E_AMBIGUOUS_RECIPIENT: raise Exception.Create ('A recipient matched more than one of the recipient descriptor structures and MAPI_DIALOG was not set. No message was sent.');
      MAPI_E_ATTACHMENT_NOT_FOUND: raise Exception.Create ('The specified attachment was not found. No message was sent.');
      MAPI_E_ATTACHMENT_OPEN_FAILURE: raise Exception.Create ('The specified attachment could not be open; no message was sent.');
      MAPI_E_BAD_RECIPTYPE: raise Exception.Create ('The type of a recipient was not MAPI_TO, MAPI_CC, or MAPI_BCC. No message was sent.');
      MAPI_E_FAILURE: raise Exception.Create ('One or more unspecified errors occurred; no message was sent.');
      MAPI_E_INSUFFICIENT_MEMORY: raise Exception.Create ('There was insufficient memory to proceed. No message was sent.');
      MAPI_E_LOGIN_FAILURE: raise Exception.Create ('There was no default logon, and the user failed to log on successfully when the logon dialog box was displayed. No message was sent.');
      MAPI_E_TEXT_TOO_LARGE: raise Exception.Create ('The text in the message was too large to sent; the message was not sent.');
      MAPI_E_TOO_MANY_FILES: raise Exception.Create ('There were too many file attachments; no message was sent.');
      MAPI_E_TOO_MANY_RECIPIENTS: raise Exception.Create ('There were too many recipients; no message was sent.');
      MAPI_E_UNKNOWN_RECIPIENT: raise Exception.Create ('A recipient did not appear in the address list; no message was sent.');
      MAPI_E_USER_ABORT: raise Exception.Create ('The user canceled one of the dialog boxes; no message was sent.');
    else
      RaiseLastOSError;
    end;
  finally
    for i:=0 to length(Recips)-1 do
    begin
      StrDispose(Recips[i].lpszName);
      StrDispose(Recips[i].lpszAddress);
    end
  end
end;

end.

