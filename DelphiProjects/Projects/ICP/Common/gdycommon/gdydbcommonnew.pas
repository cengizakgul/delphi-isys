unit gdyDbCommonNew;

interface

uses
	db, dbclient;

function DataSetToCsv(ds: TDataSet): string;

procedure LoadCDSFromString(cds: TClientDataSet; s: string);

procedure LoadDataFromString(cds: TClientDataSet; s: string);

implementation

uses
	cswriter, classes, sysutils;

function DataSetToCsv(ds: TDataSet): string;
var
  wr: TCSWriter;
  ss: TStringStream;
  bmk: TBookmark;
  i: integer;
begin
  ss := TStringStream.Create('');
  try
    wr := TCSWriter.Create( ss, false );
    try
      ds.DisableControls;
      try
        bmk := ds.GetBookMark;
        try
          try
            ds.First;
            for i := 0 to ds.FieldCount-1 do
              wr.Write(ds.Fields[i].FieldName);
            wr.WriteEndOfRow;
            while not ds.Eof do
            begin
              for i := 0 to ds.FieldCount-1 do
                wr.Write(ds.Fields[i]);
              wr.WriteEndOfRow;
              ds.Next;
            end;
          finally
            ds.GotoBookmark( bmk );
          end;
        finally
          ds.FreeBookmark( bmk );
        end;
      finally
        ds.EnableControls;
      end;
    finally
      FreeAndNil( wr );
    end;
    Result := ss.DataString;
  finally
    FreeAndNil(ss);
  end;
end;

procedure LoadCDSFromString(cds: TClientDataSet; s: string);
var
  ss: TStringStream;
begin
  ss := TStringStream.Create(s);
  try
    cds.LoadFromStream(ss);
  finally
    FreeAndNil(ss);
  end;
  cds.MergeChangeLog;
end;

procedure LoadDataFromString(cds: TClientDataSet; s: string);
var
  tmp: TClientDataSet;
  fieldNames: string;
  i: integer;
  field: TField;
begin
  if s = '' then
    exit;
  tmp := TClientDataSet.Create(nil);
  try
    LoadCDSFromString(tmp, s);

    //intersection
    fieldNames := '';
    for i := 0 to tmp.FieldList.Count-1 do
    begin
      field := cds.FindField(tmp.FieldList[i].FieldName);
      if (field <> nil) and
         ((field.DataType = tmp.FieldList[i].DataType) or
          (field.DataType = ftWideString) and (tmp.FieldList[i].DataType = ftString)) then
      begin
        if fieldNames <> '' then
          fieldNames := fieldNames + ';';
        fieldNames := fieldNames + tmp.FieldList[i].FieldName;
      end;
    end;

    tmp.First;
    while not tmp.Eof do
    begin
      cds.Append;
      try
        cds[fieldNames] := tmp[fieldNames];
        cds.Post;
      except
        cds.Cancel;
        raise;
      end;
      tmp.Next;
    end;
  finally
    FreeAndNil(tmp);
  end;
  cds.MergeChangeLog;
end;

end.
