unit gdyGlobalWaitIndicator;

interface

uses
  gdyClasses;

function WaitIndicator: IWaitIndicator;
procedure SetWaitIndicator(wi: IWaitIndicator);

implementation

uses
  sysutils;

type
  IWaitIndicatorProxy = interface
['{2DCF7B49-04D2-44EC-A23F-3F1A4B347A70}']
    procedure SetWaitIndicator(wi: IWaitIndicator);
  end;

  TWaitIndicatorProxy = class(TInterfacedObject, IWaitIndicator, IWaitIndicatorProxy)
  private
    FWI: IWaitIndicator;
  public
    constructor Create;
    procedure SetWaitIndicator(wi: IWaitIndicator);
    procedure StartWait(op: string);
    procedure EndWait;
  end;

threadvar
  GWaitIndicator: IWaitIndicator;

function WaitIndicator: IWaitIndicator;
begin
  Result := GWaitIndicator;
end;

procedure SetWaitIndicator(wi: IWaitIndicator);
begin
  if wi = nil then
    (GWaitIndicator as IWaitIndicatorProxy).SetWaitIndicator(THourglassWaitIndicator.Create)
  else
    (GWaitIndicator as IWaitIndicatorProxy).SetWaitIndicator(wi);
end;

{ TWaitIndicatorProxy }

constructor TWaitIndicatorProxy.Create;
begin
  FWI := THourglassWaitIndicator.Create;
end;

procedure TWaitIndicatorProxy.EndWait;
begin
  FWI.EndWait;
end;

procedure TWaitIndicatorProxy.SetWaitIndicator(wi: IWaitIndicator);
begin
  FWI := wi;
end;

procedure TWaitIndicatorProxy.StartWait(op: string);
begin
  FWI.StartWait(op);
end;

initialization
  GWaitIndicator := TWaitIndicatorProxy.Create;

finalization
  GWaitIndicator := nil;
  
end.
