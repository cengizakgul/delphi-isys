unit gdyLogWriters;

interface

uses
  classes, gdyLoggerImpl;

type
  TLoggerEventSink = class(TInterfacedObject)
  protected
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  end;

  ILogOutput = interface
['{4D78D2D1-60F9-4819-B939-F8B72275FBE7}']
    procedure Put( s: string );
  end;

  IFileLogOutput = interface
['{84067F28-EB63-4AF2-920C-998B0568317A}']
    function Filename: string;
    procedure CloseFileTemporarily;
  end;

  TFileLogOutput = class ( TInterfacedObject, ILogOutput, IFileLogOutput)
  private
    FLog: Text;
    FFileOpened: boolean;
    FFilename: string;
    procedure FileNeeded;
    {ILogOutput}
    procedure Put( s: string );
    {IFileLogOutput}
    procedure CloseFileTemporarily;
    function Filename: string;
  public
    constructor Create( filename: string );
    destructor Destroy; override;
  end;

  TStreamLogOutput = class ( TInterfacedObject, ILogOutput)
  private
    FStream: TStream;
    {ILogOutput}
    procedure Put( s: string );
  public
    constructor Create(s: TStream);
  end;

  TStringsLogOutput = class ( TInterfacedObject, ILogOutput)
  private
    FStrings: TStrings;
    {ILogOutput}
    procedure Put( s: string );
  public
    constructor Create( aStrings: TStrings );
  end;

  TPlainTextLogWriterBase = class ( TLoggerEventSink )
  protected
    FOut: ILogOutput;
  public
    constructor Create( aOut: ILogOutput );
  end;

  TPlainTextDevLogWriter = class ( TPlainTextLogWriterBase, ILoggerEventSink )
  private
  	FLastWriteTime: TDateTime;
    procedure put( aLogState: TLogState; s: string );
    {ILoggerEventSink}
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  public
    constructor Create( aOut: ILogOutput );
  end;

  TPlainTextUserLogWriter = class ( TPlainTextLogWriterBase, ILoggerEventSink )
  private
    function GenUserLogEntry( ls: TLogState ): string;
    {ILoggerEventSink}
    procedure MessageLogged( aLogState: TLogState );
  public
  end;

  THtmlUserLogWriter = class(TPlainTextLogWriterBase, ILoggerEventSink)
  private
    FFooter: string;
    FEntryTemplate: string;
    FDetailsTemplate: string;
    FContextTemplate: string;
    FContextBlockTemplate: string;
    FContextItemTemplate: string;

    {ILoggerEventSink}
    procedure MessageLogged( aLogState: TLogState );
    function GenUserLogEntry(ls: TLogState): string;
  public
    constructor Create(aOut: ILogOutput; dir: string; name: string);
    destructor Destroy; override;
  end;

  ILogStat = interface
  ['{15C7A498-2176-4AB7-892B-30C687649AB4}']
    function Errors: integer;
    function Warnings: integer;
  end;

  TStatWriter = class(TLoggerEventSink, ILoggerEventSink, ILogStat)
  private
    FErrors: integer;
    FWarnings: integer;

    {ILoggerEventSink}
    procedure MessageLogged( aLogState: TLogState );

    {ILogStat}
    function Errors: integer;
    function Warnings: integer;
  public
  end;

function FormatCtxVal( s: string ): string;

implementation

uses
  typinfo, sysutils, gdyLogger, idstrings{StrHtmlEncode}, gdycommon, dateutils;

function FormatCtxVal( s: string ): string;
begin
  if Pos( #13, s ) > 0 then
    Result := #13#10 + Indent( s, 1 )
  else
    Result := s;
end;


{ TWriterBase }

procedure TLoggerEventSink.BlockEntered(aLogState: TLogState);
begin
//do nothing
end;

procedure TLoggerEventSink.BlockLeaving(aLogState: TLogState);
begin
//do nothing
end;

procedure TLoggerEventSink.ContextItemLogged(aLogState: TLogState);
begin
//do nothing
end;

procedure TLoggerEventSink.MessageLogged(aLogState: TLogState);
begin
//do nothing
end;

{ TFileLogOutput }

constructor TFileLogOutput.Create(filename: string);
begin
  FFilename := filename;
  FileNeeded;
end;

procedure TFileLogOutput.FileNeeded;
begin
  if not FFileOpened then
    try
      ForceDirectories( ExtractFilePath(FFilename) );
      system.assignfile( FLog, FFilename );
      if FileExists(FFilename) then
        system.Append( FLog )
      else
        system.Rewrite( FLog );
      FFileOpened := true;  
    except
      //do nothing
    end;
end;


destructor TFileLogOutput.Destroy;
begin
  inherited;
  CloseFileTemporarily;
end;

function TFileLogOutput.Filename: string;
begin
  Result := FFilename;
end;

procedure TFileLogOutput.CloseFileTemporarily;
begin
  if FFileOpened then
  try
    system.Flush( FLog );
    system.CloseFile( FLog );
    FFileOpened := false;
  except
    //do nothing
  end;
end;

procedure TFileLogOutput.put(s: string);
begin
  try
    FileNeeded;
    system.Writeln( FLog, s );
    system.Flush( FLog );
  except
    //do nothing
  end;
end;

{ TPlainTextDevLogWriter }

constructor TPlainTextDevLogWriter.Create(aOut: ILogOutput);
begin
  inherited;
  FLastWriteTime := Now;
end;

procedure TPlainTextDevLogWriter.BlockEntered(aLogState: TLogState);
begin
  put( aLogState, 'enter ' + aLogState.Last.BlockName );
end;

procedure TPlainTextDevLogWriter.BlockLeaving(aLogState: TLogState);
begin
  put( aLogState, 'leave ' + aLogState.Last.BlockName );
end;

procedure TPlainTextDevLogWriter.ContextItemLogged(aLogState: TLogState);
begin
  put( aLogState, aLogState.Last.Context.Last.RenderAsString  );
end;

procedure TPlainTextDevLogWriter.MessageLogged(aLogState: TLogState);
begin
  put( aLogState, aLogState.Last.Messages.Last.RenderAsString);
end;

procedure TPlainTextDevLogWriter.put(aLogState: TLogState; s: string);
var
  ts, pfx: string;

  function PrefixEachLine: string;
  var
    sl: TStrings;
    i: integer;
    lts: string;
  begin
    Result := s;
    if Result <> '' then
    begin
      //StringReplace is too slow on low-end hardware
      sl := TStringList.Create;
      lts := StringOfChar(' ', Length(ts));
      try
        sl.Text := Result;
        for i := 0 to sl.Count - 1 do
          sl[i] := lts + pfx + sl[i];
        Result := sl.Text;
      finally
        FreeAndNil(sl);
      end;
      Result := ts + pfx + Result;
    end;
  end;
begin
  ts := Format('%9.3f sec | ', [SecondSpan(Now, FLastWriteTime)]);
  pfx := StringOfChar(' ', (aLogState.BlockStack.Count-1)*4);

  FOut.Put( PrefixEachLine );
  FLastWriteTime := Now;
end;

{ TPlainTextUserLogWriter }

function TPlainTextUserLogWriter.GenUserLogEntry( ls: TLogState ): string;
  function DumpLogBlock( lb: TLogBlock ): string;
  var
    i: integer;
  begin
    Result := '';
    if lb.Context.Count > 0 then
    begin
      Result := lb.BlockName + #13#10;
      for i := 0 to lb.Context.Count-1 do
        Result := Result + lb.Context[i].ContextTag + ': ' + FormatCtxVal(lb.Context[i].ContextValue) + #13#10;
    end;
  end;
var
  i: integer;
  ctx: string;
begin
  with ls.Last.Messages.Last do
  begin
    Result := KindAsStr + ': ' + Text + #13#10;
    if Details <> '' then
      Result := Result + Details + #13#10;
  end;
  ctx := '';
  for i := 0 to ls.BlockStack.Count-1 do
    ctx := ctx + DumpLogBlock( ls.BlockStack[i] );
  if ctx <> '' then
    Result := Result + 'Context:' + #13#10 + ctx;
end;

procedure TPlainTextUserLogWriter.MessageLogged(aLogState: TLogState);
begin
  if not (aLogState.Last.Messages.Last.Kind in [lmDebug]) then
    FOut.Put( GenUserLogEntry( aLogState ) );
end;

{ TPlainTextLogWriterBase }

constructor TPlainTextLogWriterBase.Create(aOut: ILogOutput);
begin
  FOut := aOut;
end;

{ TStringsLogOutput }

constructor TStringsLogOutput.Create(aStrings: TStrings);
begin
  FStrings := aStrings;
end;

procedure TStringsLogOutput.Put(s: string);
begin
  FStrings.Add( s );
end;

{ TStreamLogOutput }

constructor TStreamLogOutput.Create(s: TStream);
begin
  FStream := s;
end;

procedure TStreamLogOutput.Put(s: string);
const
  crlf: string = #13#10;
begin
  FStream.WriteBuffer(PChar(s)^, Length(s));
  FStream.WriteBuffer(PChar(crlf)^, Length(crlf));
end;

{ THtmlUserLogWriter }

constructor THtmlUserLogWriter.Create(aOut: ILogOutput; dir: string; name: string);
var
  header: string;
begin
  inherited Create(aOut);
  FEntryTemplate := FileToString(dir + 'entry.html');
  FDetailsTemplate := FileToString(dir + 'details.html');
  FContextTemplate := FileToString(dir + 'context.html');
  FContextBlockTemplate := FileToString(dir + 'context_block.html');
  FContextItemTemplate := FileToString(dir + 'context_item.html');
  FFooter := FileToString(dir + 'footer.html');

  header := FileToString(dir + 'header.html');
  header := StringReplace(header, '%LOGNAME%', name, [rfReplaceAll]);
  FOut.Put( header );
end;

destructor THtmlUserLogWriter.Destroy;
begin
  FOut.Put(FFooter);
  inherited;
end;

function THtmlUserLogWriter.GenUserLogEntry( ls: TLogState ): string;
  function prepare(s: string): string;
  begin
    Result := StringReplace(StrHtmlEncode(s), #13#10,'<br/>', [rfReplaceAll]);
  end;

  function DumpLogBlock( lb: TLogBlock ): string;
  var
    i: integer;
    items: string;
    item: string;
  begin
    Result := '';
    if lb.Context.Count > 0 then
    begin
      Result := StringReplace(FContextBlockTemplate, '%CONTEXT_BLOCK%', prepare(lb.BlockName), [rfReplaceAll]);
      items := '';
      for i := 0 to lb.Context.Count-1 do
      begin
        item := StringReplace(FContextItemTemplate, '%CONTEXT_TAG%', prepare(lb.Context[i].ContextTag), [rfReplaceAll]);
        item := StringReplace(item, '%CONTEXT_VALUE%', prepare( FormatCtxVal(lb.Context[i].ContextValue) ), [rfReplaceAll]);
        items := items + item;
      end;
      Result := StringReplace(Result, '%CONTEXT_ITEMS%', items, [rfReplaceAll]);
    end;
  end;

  function KindToImageName(Kind: TLogMessageKind): string;
  begin
    case Kind of
      lmException,
      lmError: Result := 'error';
      lmWarning: Result := 'warning';
      lmEvent: Result := 'info';
    else
      //lmDebug
      Assert(false);
    end;
  end;

var
  i: integer;
  det: string;
  ctx: string;
begin
  Result := FEntryTemplate;
  with ls.Last.Messages.Last do
  begin
    Result := StringReplace(Result, '%KIND%', KindToImageName(Kind), [rfReplaceAll]);
    Result := StringReplace(Result, '%MESSAGE%', prepare(Text), [rfReplaceAll]);
    if Details <> '' then
      det := StringReplace(FDetailsTemplate, '%DETAILS%', prepare(Details), [rfReplaceAll])
    else
      det := '';
    Result := StringReplace(Result, '%DETAILS%', det, [rfReplaceAll]);
  end;
  ctx := '';
  for i := 0 to ls.BlockStack.Count-1 do
    ctx := ctx + DumpLogBlock( ls.BlockStack[i] );

  if ctx <> '' then
    ctx := StringReplace(FContextTemplate, '%CONTEXT%', ctx, [rfReplaceAll]);
  Result := StringReplace(Result, '%CONTEXT%', ctx, [rfReplaceAll]);
end;

procedure THtmlUserLogWriter.MessageLogged(aLogState: TLogState);
begin
  if not (aLogState.Last.Messages.Last.Kind in [lmDebug]) then
    FOut.Put( GenUserLogEntry(aLogState) );
end;

{ TStatWriter }

function TStatWriter.Errors: integer;
begin
  Result := FErrors;
end;

procedure TStatWriter.MessageLogged(aLogState: TLogState);
begin
  case aLogState.Last.Messages.Last.Kind of
    lmException, lmError: inc(FErrors);
    lmWarning: inc(FWarnings);
    lmEvent, lmDebug: ;
  else
    Assert(false);
  end

end;

function TStatWriter.Warnings: integer;
begin
  Result := FWarnings;
end;

end.
