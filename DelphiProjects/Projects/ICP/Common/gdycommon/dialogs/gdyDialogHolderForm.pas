unit gdyDialogHolderForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  gdyDialogHolderBaseForm, ExtCtrls, ActnList, ImgList, StdCtrls, Buttons;

type
  TDialogHolder = class(TDialogHolderBase)
    ActionList1: TActionList;
    OK: TAction;
    Cancel: TAction;
    ImageList1: TImageList;
    cbNeverShow: TCheckBox;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    procedure OKUpdate(Sender: TObject);
    procedure CancelUpdate(Sender: TObject);
    procedure OKExecute(Sender: TObject);
  private
    { Private declarations }
    procedure SetNeverShowAgainOption( Value: boolean );
  protected
    procedure DoCloseRequested; override;
  public
    { Public declarations }
    constructor Create( Owner: TComponent ); override;
    function NeverShowAgain: boolean;
    property NeverShowAgainOption: boolean write SetNeverShowAgainOption;

  end;


implementation

{$R *.DFM}

procedure TDialogHolder.OKUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := assigned( HoldedDialog ) and HoldedDialog.CanClose;
end;

procedure TDialogHolder.CancelUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := assigned( HoldedDialog );
end;

constructor TDialogHolder.Create(Owner: TComponent);
begin
  inherited;
{$ifdef RUSSIAN}
  cbNeverShow.Caption := '������ �� ����������';
{$endif}
  NeverShowAgainOption := false;
end;

function TDialogHolder.NeverShowAgain: boolean;
begin
  Result := cbNeverShow.Checked;
end;

procedure TDialogHolder.SetNeverShowAgainOption(Value: boolean);
begin
  cbNeverShow.Visible := Value;
  pnlControl.Constraints.MinWidth := 200 + 250*ord(not Value);
end;

procedure TDialogHolder.DoCloseRequested;
begin
  OK.Execute;
end;

procedure TDialogHolder.OKExecute(Sender: TObject);
begin
  if HoldedDialog.CheckBeforeClose then
    ModalResult := mrOk;
end;

end.
