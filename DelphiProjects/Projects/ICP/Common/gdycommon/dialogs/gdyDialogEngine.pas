unit gdyDialogEngine;

interface
uses classes,forms, controls, gdyDialogHolderBaseForm, gdyDialogEngineCommon;


type
  IDialogEngine = interface
['{13899D91-28BA-48A2-A5F6-FB2D81D4C6B2}']
    function ShowModal: TModalResult;
    function NeverShowAgain: boolean;
    procedure SetHolderClass( aHolderClass: TDialogHolderBaseClass );
    property HolderClass: TDialogHolderBaseClass write SetHolderClass;
    procedure SetNeverShowAgainOption( Value: boolean );
    property NeverShowAgainOption: boolean write SetNeverShowAgainOption;
  end;


function DialogEngine( dlg: TFrame; Owner: TComponent ): IDialogEngine; overload;
function DialogEngine( dlgClass: TFrameClass; Owner: TComponent ): IDialogEngine; overload;

procedure SetDialogHolderClass( aHolderClass: TDialogHolderBaseClass );

///// legacy ////
function ShowDialog( dlg: TFrame; Owner: TComponent ): TModalResult; overload;
function ShowDialog( dlgClass: TFrameClass; Owner: TComponent ): TModalResult; overload;
function ShowDialog( dlg: TFrame; Owner: TComponent; aholderClass: TDialogHolderBaseClass ): TModalResult; overload;
function ShowDialog( dlgClass: TFrameClass; Owner: TComponent; aholderClass: TDialogHolderBaseClass ): TModalResult; overload;

type
  TBeforeShowDialogEvent = procedure ( dlg: TFrame ) of object;

var
  BeforeShowDialog: TBeforeShowDialogEvent;

implementation

uses gdyDialogHolderForm, sysutils;

type

  TShowDialog = class( TInterfacedObject, IDialogEngine )
  private
    FDialog: TFrame;
    FDialogClass: TFrameClass;
    FOwner: TComponent;
    FHolderClass: TDialogHolderBaseClass;
    FNeverShowAgainOption: boolean;
    FNeverShowAgainResult: boolean;
    procedure SetHolderClass( aHolderClass: TDialogHolderBaseClass );
    procedure SetNeverShowAgainOption( Value: boolean );
  public
    constructor Create( dlg: TFrame; Owner: TComponent ); overload;
    constructor Create( dlgClass: TFrameClass; Owner: TComponent ); overload;
//    destructor Destroy; override;

    {IDialogEngine}
    property HolderClass: TDialogHolderBaseClass write SetHolderClass;
    function ShowModal: TModalResult;
    function NeverShowAgain: boolean;
    property NeverShowAgainOption: boolean read FNeverShowAgainOption write SetNeverShowAgainOption;
  end;


function DialogEngine( dlg: TFrame; Owner: TComponent ): IDialogEngine;
begin
  Result := TShowDialog.Create( dlg, Owner ) as IDialogEngine;
end;

function DialogEngine( dlgClass: TFrameClass; Owner: TComponent ): IDialogEngine;
begin
  Result := TShowDialog.Create( dlgClass, Owner ) as IDialogEngine;
end;


{ TShowDialog }

var
  GHolderClass: TDialogHolderBaseClass;


constructor TShowDialog.Create(dlg: TFrame; Owner: TComponent);
begin
  FHolderClass := GHolderClass;
  FDialog := dlg;
  FOwner := Owner;
end;

constructor TShowDialog.Create(dlgClass: TFrameClass; Owner: TComponent);
begin
  FHolderClass := TDialogHolder;
  FDialogClass := dlgClass;
  FOwner := Owner;
end;

function TShowDialog.NeverShowAgain: boolean;
begin
  Result := FNeverShowAgainResult;
end;

procedure TShowDialog.SetHolderClass(aHolderClass: TDialogHolderBaseClass);
begin
  FHolderClass := aHolderClass;
end;

procedure TShowDialog.SetNeverShowAgainOption(Value: boolean);
begin
  FNeverShowAgainOption := Value;
end;

function TShowDialog.ShowModal: TModalResult;
var
  shouldFreeDialog: boolean;
  oldParent: TWinControl;
  holder: TDialogHolderBase;
begin
  if FDialog = nil then
  begin
    shouldFreeDialog := true;
    FDialog := FDialogClass.Create( nil );
  end
  else
    shouldFreeDialog := false;
  try
    if assigned(BeforeShowDialog) then
      BeforeShowDialog( FDialog );
    holder := FHolderClass.Create( FOwner );
    try
      if holder is TDialogHolder then
        (holder as TDialogHolder).NeverShowAgainOption := FNeverShowAgainOption;
      oldParent := FDialog.Parent;
      try
        holder.PlaceDialog( FDialog, true );
        Result := holder.ShowModal;
        FNeverShowAgainResult := (holder is TDialogHolder) and (holder as TDialogHolder).NeverShowAgain;
      finally
        FDialog.Parent := oldParent;
      end;
    finally
      FreeAndNil( holder );
    end;
  finally
    if shouldFreeDialog then
      FreeAndNil( FDialog );
  end;
end;

///// legacy ////
function ShowDialog( dlgClass: TFrameClass; Owner: TComponent; aholderClass: TDialogHolderBaseClass ): TModalResult;
begin
  with DialogEngine( dlgClass, Owner ) do
  begin
    HolderClass := aHolderClass;
    Result := ShowModal;
  end;
end;

function ShowDialog( dlg: TFrame; Owner: TComponent; aHolderClass: TDialogHolderBaseClass ): TModalResult;
begin
  with DialogEngine( dlg, Owner ) do
  begin
    HolderClass := aHolderClass;
    Result := ShowModal;
  end;
end;

function ShowDialog( dlgClass: TFrameClass; Owner: TComponent ): TModalResult;
begin
  Result := DialogEngine( dlgClass, Owner ).ShowModal;
end;

function ShowDialog( dlg: TFrame; Owner: TComponent ): TModalResult;
begin
  Result := DialogEngine( dlg, Owner ).ShowModal;
end;

procedure SetDialogHolderClass( aHolderClass: TDialogHolderBaseClass );
begin
  GHolderClass := aHolderClass;
end;

initialization
  GHolderClass := TDialogHolder;

end.

