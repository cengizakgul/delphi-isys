unit gdyDialogEngineCommon;

interface
uses
  forms, controls;

type
  TFrameClass = class of TFrame;

  IHolderEventSink = interface
['{534FD496-27CA-446B-8DE5-4C2BF2FF6BC5}']
    procedure CloseRequested;
  end;

  IBasicDialog = interface
['{D6C52E32-D19B-473E-90E2-982D48BB2DEC}']
    function GetCaption: string;
    procedure SetCaption( Value: string );

    function CanClose: boolean;
    procedure AfterShow;

    property Caption: string read GetCaption write SetCaption;

    function MainControl: TWinControl;
    procedure Advise( hes: IHolderEventSink ); //one slot; to unadvise call Advise(nil)

    function CheckBeforeClose: boolean;
  end;

  IRecordEditDialog = interface
['{ED4F41AB-C9A6-4A92-874B-928A17497140}']
    procedure Post;
    procedure Cancel;
  end;

  ITableEditDialog = interface
['{74ACD938-A9EA-41E1-9D82-CB29E5473106}']
    procedure New;
    procedure View;
    procedure Change;
    procedure Delete;
    procedure Close;
    procedure Cancel;

    function HasRecord: boolean;
    function CanNew: boolean;
    function ReadOnly: boolean;
  end;

//  IDBNavigatedDialog = interface
  

implementation


end.
