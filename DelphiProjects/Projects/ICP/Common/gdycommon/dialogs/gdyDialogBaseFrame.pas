unit gdyDialogBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, gdyDialogEngineCommon;

type
  TDialogBase = class(TFrame, IBasicDialog)
  private
    { Private declarations }
    FCaption: string;
    function GetCaption: string;
    procedure SetCaption( Value: string );
  protected
    FEventSink: IHolderEventSink;
  public
    { Public declarations }
    function CanClose: boolean; virtual;
    procedure AfterShow; virtual;
    property Caption: string read GetCaption write SetCaption;
    function MainControl: TWinControl; virtual;
    procedure Advise( hes: IHolderEventSink ); //one slot; to unadvise call Advise(nil)
    function CheckBeforeClose: boolean; virtual;
  end;

  TDialogBaseClass  = class of TDialogBase;
  
implementation

uses
  stdctrls;

{$R *.DFM}

{ TDialogBase }

procedure TDialogBase.Advise(hes: IHolderEventSink);
begin
  FEventSink := hes;
end;

procedure TDialogBase.AfterShow;
begin

end;

function TDialogBase.CanClose: boolean;
begin
  Result := true;
end;

function TDialogBase.CheckBeforeClose: boolean;
begin
  Result := true;
end;

function TDialogBase.GetCaption: string;
begin
  Result := FCaption;
end;

type
  TFakeCustomEdit = class( TCustomEdit )
  end;
function TDialogBase.MainControl: TWinControl;
var
  i: integer;
  c: TWinControl;
begin
  Result := nil;
  for i := 0 to ComponentCount-1 do
    if Components[i] is TWincontrol then
    begin
      c := Components[i] as TWincontrol;
      if c.CanFocus and ( not (c is TEdit) or not TFakeCustomEdit(c).ReadOnly) then
      begin
        Result := c;
        break;
      end;
    end;
end;

procedure TDialogBase.SetCaption(Value: string);
begin
  FCaption := Value;
end;

end.




