unit gdyDialogHolderBaseForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, gdydialogEngineCommon;

type
  TDialogHolderBase = class(TForm, IHolderEventSink)
    pnlStaticInfo: TPanel;
    pnlPlace: TPanel;
    pnlControl: TPanel;
    Bevel1: TBevel;
    Label1: TLabel;
    pnlHolder: TPanel;
    pnlBevelHolder: TPanel;
    Bevel2: TBevel;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FHoldedDialog: TFrame;
    FBorder: TSize;
    procedure ShrinkAround(dlgWidth, dlgHeight: integer );
    function GetHoldedDialog: IBasicDialog;
  protected
    procedure DoSetCaption( cap: string ); virtual;
    procedure AfterDialogShow; virtual;
    property HoldedDialogFrame: TFrame read FHoldedDialog;
    property HoldedDialog: IBasicDialog read GetHoldedDialog;
    function CalcFormSize( dlgWidth, dlgHeight: integer ): TSize;
    procedure DoCloseRequested; virtual;
  public
    { Public declarations }
    constructor Create( Owner: TComponent ); override;
    procedure CloseRequested;


    procedure PlaceDialog( dlg: TFrame; aAutoSize: boolean ); virtual;
    property Border: TSize read FBorder write FBorder;
    function ShowModal: integer; override;

  end;

  TDialogHolderBaseClass = class of TDialogHolderBase;


implementation

{$R *.DFM}

{ TDialogHolderBase }


procedure TDialogHolderBase.ShrinkAround( dlgWidth, dlgHeight: integer );
begin
  with CalcFormSize( dlgWidth, dlgHeight ) do
  begin
    Width := cx;
    Height := cy;
  end;
end;


procedure TDialogHolderBase.PlaceDialog(dlg: TFrame; aAutoSize: boolean );
begin
  if FHoldedDialog <> nil then
  begin
    FHoldedDialog.Parent := nil;
    FHoldedDialog := nil;
  end;
  if dlg <> nil then
  begin
    if aAutoSize then
      ShrinkAround( dlg.width, dlg.Height );
    pnlHolder.Left := FBorder.cx;
    pnlHolder.Top := FBorder.cy;
    pnlHolder.Width := pnlPlace.Width - 2*FBorder.cx;
    pnlHolder.Height := pnlPlace.Height - 2*FBorder.cy;

    dlg.Align := alClient;
    dlg.Parent := pnlHolder;
    Constraints.MinHeight := Height;
    Constraints.MinWidth := Width;
    FHoldedDialog := dlg;
    DoSetCaption( HoldedDialog.Caption );
    dlg.Show;
  end
end;

procedure TDialogHolderBase.FormShow(Sender: TObject);
begin
  if assigned(HoldedDialog) then
    AfterDialogShow;
end;

procedure TDialogHolderBase.FormActivate(Sender: TObject);
begin
  Realign;
end;

procedure TDialogHolderBase.DoSetCaption(cap: string);
begin
  Caption := cap;
end;

procedure TDialogHolderBase.AfterDialogShow;
var
  ctrl: TWinControl;
begin
  HoldedDialog.AfterShow;
  ctrl := HoldedDialog.MainControl;
  if (ctrl <> nil) and ctrl.CanFocus then
    ctrl.SetFocus;
end;

constructor TDialogHolderBase.Create(Owner: TComponent);
begin
  inherited;
  FBorder.cx := 6;
  FBorder.cy := 6;
end;

function TDialogHolderBase.CalcFormSize(dlgWidth,
  dlgHeight: integer): TSize;
var
  xExcess, yExcess: integer;
begin
  xExcess := Width - pnlPlace.Width;
  yExcess := Height - pnlPlace.Height;
  Result.cx := dlgWidth + FBorder.cx*2 + xExcess;
  Result.cy := dlgHeight + FBorder.cy*2 + yExcess;
end;

function TDialogHolderBase.GetHoldedDialog: IBasicDialog;
begin
{$IFNDEF VER130}
  Result := FHoldedDialog as IBasicDialog;
{$ELSE}
  Result := nil;
  FHoldedDialog.GetInterface( IBasicDialog, Result );
{$ENDIF}
end;

procedure TDialogHolderBase.CloseRequested;
begin
  DoCloseRequested;
end;

procedure TDialogHolderBase.DoCloseRequested;
begin
//do nothing
end;

function TDialogHolderBase.ShowModal: integer;
begin
  HoldedDialog.Advise( Self );
  try
    Result := inherited showModal;
  finally
    HoldedDialog.Advise( nil );
  end;
end;

end.
