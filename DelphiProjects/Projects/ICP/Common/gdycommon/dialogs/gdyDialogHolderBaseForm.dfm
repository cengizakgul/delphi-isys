object DialogHolderBase: TDialogHolderBase
  Left = 464
  Top = 342
  ActiveControl = pnlPlace
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'DialogHolderBase'
  ClientHeight = 281
  ClientWidth = 520
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  Scaled = True
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlStaticInfo: TPanel
    Left = 0
    Top = 0
    Width = 520
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Color = clBtnHighlight
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 520
      Height = 41
      Align = alClient
      Shape = bsBottomLine
    end
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
  end
  object pnlPlace: TPanel
    Left = 0
    Top = 41
    Width = 520
    Height = 190
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlHolder: TPanel
      Left = 64
      Top = 56
      Width = 185
      Height = 32
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object pnlControl: TPanel
    Left = 0
    Top = 240
    Width = 520
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
  end
  object pnlBevelHolder: TPanel
    Left = 0
    Top = 231
    Width = 520
    Height = 9
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Bevel2: TBevel
      Left = 8
      Top = -3
      Width = 505
      Height = 9
      Anchors = [akLeft, akTop, akRight]
      Shape = bsBottomLine
    end
  end
end
