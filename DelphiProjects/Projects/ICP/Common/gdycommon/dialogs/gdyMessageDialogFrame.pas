unit gdyMessageDialogFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, StdCtrls;

type
  TMessageDialog = class(TDialogBase)
    lblMessage: TLabel;
  private
    function GetMessageText: string;
    procedure SetMessageText(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    property MessageText: string read GetMessageText write SetMessageText;
  end;


implementation

{$R *.dfm}

{ TMessageDialog }

function TMessageDialog.GetMessageText: string;
begin
  Result := lblMessage.Caption;
end;

procedure TMessageDialog.SetMessageText(const Value: string);
begin
  lblMessage.Caption := Value;
end;

end.
