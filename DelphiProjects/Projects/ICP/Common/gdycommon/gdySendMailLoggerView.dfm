inherited SendMailLoggerViewFrame: TSendMailLoggerViewFrame
  Height = 592
  inherited pnlLog: TPanel
    Height = 560
    inherited LoggerRichView: TLoggerRichViewFrame
      Height = 560
      inherited pnlUserView: TPanel
        Height = 560
        inherited pnlBottom: TPanel
          Height = 322
          inherited evSplitter2: TSplitter
            Height = 322
          end
          inherited pnlLeft: TPanel
            Height = 322
            inherited mmDetails: TMemo
              Height = 297
            end
          end
          inherited pnlRight: TPanel
            Height = 322
            inherited mmContext: TMemo
              Height = 297
            end
          end
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 560
    Width = 700
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 166
      Top = 3
      Width = 163
      Height = 25
      Caption = 'Send debug log by e-mail...'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 342
      Top = 3
      Width = 179
      Height = 25
      Caption = 'Pack debug log and open folder'
      TabOrder = 1
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 6
      Top = 3
      Width = 147
      Height = 25
      Action = actSaveUserLog
      Caption = 'Save log to file and open'
      TabOrder = 2
    end
  end
  object ActionList1: TActionList
    Left = 144
    Top = 88
    object actSaveUserLog: TAction
      Caption = 'Save log to file and open'
      OnExecute = actSaveUserLogExecute
      OnUpdate = actSaveUserLogUpdate
    end
  end
  object sdUserLog: TSaveDialog
    DefaultExt = '.txt'
    Filter = '*.txt|Text files (*.txt)|*.*|All files'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save log as...'
    Left = 336
    Top = 280
  end
end
