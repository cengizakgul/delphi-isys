{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

unit gdySendMailLoggerView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyCommonLoggerView, gdyLoggerRichView, ExtCtrls, StdCtrls,
  Buttons, gdyLoggerImpl, NewSevenZip, ActnList;

type
  TSendMailLoggerViewFrame = class(TCommonLoggerViewFrame)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ActionList1: TActionList;
    actSaveUserLog: TAction;
    BitBtn3: TBitBtn;
    sdUserLog: TSaveDialog;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure actSaveUserLogUpdate(Sender: TObject);
    procedure actSaveUserLogExecute(Sender: TObject);
  private
    FDebugLog: TMemoryStream;
    FUserLog: TMemoryStream;
    FDebugSink: ILoggerEventSink;
    FUserSink: ILoggerEventSink;
    procedure AddLogsToArch(Arch: I7zOutArchive);
    function SaveArchiveToInternal(folder: string; dt: TDateTime): string;
  public
    Password: string;
    EMail: string;
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
    function SaveArchiveTo(folder: string): string;
    procedure AddUserLogToArch(Arch: I7zOutArchive);
  end;

  TSendMailLoggerViewFrameClass = class of TSendMailLoggerViewFrame;

implementation

{$R *.dfm}
uses
  gdymail, gdyRedir, common, gdyLogWriters, gdyGlobalWaitIndicator, gdycommon,gdyUtils;

procedure TSendMailLoggerViewFrame.AddLogsToArch(Arch: I7zOutArchive);
var
  sl: TStrings;
  i: integer;
begin
  if LoggerKeeper.UserLogFileName <> '' then
  begin
    LoggerKeeper.CloseUserLogFileTemporarily;
    Arch.AddFile(LoggerKeeper.UserLogFileName, ExtractFileName(LoggerKeeper.UserLogFileName) );
  end;
  Arch.AddStream(FUserLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, 'session-user.log', false, false);

  if LoggerKeeper.DevLogFileName <> '' then
  begin
    LoggerKeeper.CloseDevLogFileTemporarily;
    sl := TStringList.Create;
    try
      GetDebugLogFiles(sl);
      for i := 0 to sl.Count - 1 do
        Arch.AddFile(sl[i], ExtractFileName(sl[i]));
    finally
      sl.Free;
    end;
  end;
  Arch.AddStream(FDebugLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, 'session-debug.log', false, false);
//  Arch.SetProgressCallback();
end;

function TSendMailLoggerViewFrame.SaveArchiveTo(folder: string): string;
begin
  Result := SaveArchiveToInternal(folder, Now);
end;

function TSendMailLoggerViewFrame.SaveArchiveToInternal(folder: string; dt: TDateTime): string;
var
  Arch: I7zOutArchive;
  base: string;
begin
  WaitIndicator.StartWait('Packing log files');
  try
    Assert(Password <> '');

    Arch := CreateOutArchive(CLSID_CFormat7z, Redirection.GetFilename(sSevenZipDllAlias));
    SetCompressionLevel(Arch, 5);
    Arch.SetPassword(Password);
    AddLogsToArch(Arch);
    base := ChangeFileExt(GetAppFilename,'') + '-debug-log-';
    Result := WithTrailingSlash(folder) + base + FormatDateTime('yyyy-mm-dd-hhnn', dt) + '.7z';
    ForceDirectories(folder);
    Arch.SaveToFile(Result);
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TSendMailLoggerViewFrame.BitBtn1Click(Sender: TObject);
var
  fn: string;
  dt: TDateTime;
begin
  LoggerKeeper.Logger.LogEntry( Format('User clicked "%s"', [(Sender as TBitBtn).Caption] ));
  try
    try
      dt := Now;
      fn := SaveArchiveToInternal(Redirection.GetDirectory(sLogArchiveDirAlias), dt);
      try
        SimpleMailTo(EMail, Format('Debug log - %s, %s', [Application.Title, FormatDateTime('yyyy.mm.dd hh:nn',dt)]), '', fn);
      finally
        DeleteFile(fn);
      end;
    except
      LoggerKeeper.Logger.PassthroughException;
    end;
  finally
    LoggerKeeper.Logger.LogExit;
  end;
end;

procedure TSendMailLoggerViewFrame.BitBtn2Click(Sender: TObject);
begin
  LoggerKeeper.Logger.LogEntry( Format('User clicked "%s"', [(Sender as TBitBtn).Caption] ));
  try
    try
      OpenInExplorer( SaveArchiveTo( Redirection.GetDirectory(sLogArchiveDirAlias)) );
    except
      LoggerKeeper.Logger.PassthroughException;
    end;
  finally
    LoggerKeeper.Logger.LogExit;
  end;
end;

constructor TSendMailLoggerViewFrame.Create(Owner: TComponent);
begin
  inherited;
  FDebugLog := TMemoryStream.Create;
  FDebugSink := TPlainTextDevLogWriter.Create(TStreamLogOutput.Create(FDebugLog));
  LoggerKeeper.StatefulLogger.Advise(FDebugSink);

  FUserLog := TMemoryStream.Create;
  FUserSink := TPlainTextUserLogWriter.Create(TStreamLogOutput.Create(FUserLog));
  LoggerKeeper.StatefulLogger.Advise(FUserSink);
end;

destructor TSendMailLoggerViewFrame.Destroy;
begin
  LoggerKeeper.StatefulLogger.UnAdvise(FDebugSink);
  LoggerKeeper.StatefulLogger.UnAdvise(FUserSink);
  FDebugSink := nil;
  FUserSink := nil;
  FreeAndNil( FDebugLog );
  FreeAndNil( FUserLog );
  inherited;
end;


procedure TSendMailLoggerViewFrame.actSaveUserLogUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := LoggerRichView.MessagesCount > 0;
end;

procedure TSendMailLoggerViewFrame.actSaveUserLogExecute(Sender: TObject);
begin
  sdUserLog.FileName := ChangeFileExt(GetAppFilename,'') + '-user-log-' + FormatDateTime('yyyy-mm-dd-hhnn', Now) + '.txt';
  if sdUserLog.Execute then
  begin
    StringToFile(StreamToString(FUserLog), sdUserLog.FileName);
    OpenDoc(Application.MainForm, sdUserLog.FileName);
  end;
end;

procedure TSendMailLoggerViewFrame.AddUserLogToArch(Arch: I7zOutArchive);
begin
  Arch.AddStream(FUserLog, soReference, faArchive, CurrentFileTime, CurrentFileTime, 'task-user.log', false, false);
end;

end.
