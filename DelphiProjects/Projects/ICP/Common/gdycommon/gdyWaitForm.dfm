object WaitIndicatorForm: TWaitIndicatorForm
  Left = 464
  Top = 427
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'WaitIndicatorForm'
  ClientHeight = 56
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblMessage: TLabel
    Left = 0
    Top = 0
    Width = 363
    Height = 56
    Align = alClient
    Alignment = taCenter
    Caption = 'Please wait'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Layout = tlCenter
    WordWrap = True
  end
end
