unit gdyLoggerImpl;

interface

uses
  classes, sysutils, contnrs, gdylogger;

type

  TLogMessage = class (TCollectionItem)
  private
    FKind: TLogMessageKind;
    FText: string;
    FDetails: string;
    FMadStack: string;
  public
    procedure Assign(Source: TPersistent); override;
    property Kind: TLogMessageKind read FKind write FKind;
    property Text: string read FText write FText;
    property Details: string read FDetails write FDetails;
    property MadStack: string read FMadStack write FMadStack;
    function KindAsStr: string;
    function RenderAsString: string;
  end;

  TLogMessages = class (TCollection)
    function GetItem(Index: Integer): TLogMessage;
    procedure SetItem(Index: Integer; const Value: TLogMessage);
  public
    constructor Create;
    function Add: TLogMessage;
    property Items[Index: Integer]: TLogMessage read GetItem write SetItem; default;
    function Last: TLogMessage;
  end;

  TLogBlockContextItem = class (TCollectionItem)
  private
    FContextValue: string;
    FContextTag: string;
  public
    procedure Assign(Source: TPersistent); override;
    property ContextTag: string read FContextTag write FContextTag;
    property ContextValue: string read FContextValue write FContextValue;
    function RenderAsString: string;
  end;

  TLogBlockContex = class (TCollection)
  private
    function GetItem(Index: Integer): TLogBlockContextItem;
    procedure SetItem(Index: Integer; const Value: TLogBlockContextItem);
  public
    constructor Create;
    function Add: TLogBlockContextItem;
    property Items[Index: Integer]: TLogBlockContextItem read GetItem write SetItem; default;
    function Last: TLogBlockContextItem;
  end;

  TLogBlock = class (TCollectionItem)
  private
    FBlockName: string;
    FContext: TLogBlockContex;
    FMessages: TLogMessages;
    procedure SetContext(const Value: TLogBlockContex);
    procedure SetMassages(const Value: TLogMessages);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property BlockName: string read FBlockName write FBlockName;
    property Context: TLogBlockContex read FContext write SetContext;
    property Messages: TLogMessages read FMessages write SetMassages;
  end;

  TLogBlocks = class (TCollection)
  private
    function GetItem(Index: Integer): TLogBlock;
    procedure SetItem(Index: Integer; const Value: TLogBlock);
  public
    constructor Create;
    function Add: TLogBlock;
    property Items[Index: Integer]: TLogBlock read GetItem write SetItem; default;
  end;

  TLogState = class (TPersistent)
  private
    FBlockStack: TLogBlocks;
  public
    constructor Create;
    procedure Assign(Source: TPersistent); override;
    destructor Destroy; override;
    function Last: TLogBlock;
    function IsValid: boolean;
    procedure AddBlock( ablockname: string );
    procedure RemoveTopBlock;
    function Dump: string;
    property BlockStack: TLogBlocks read FBlockStack;

    function Clone: TLogState;
  end;

  ILoggerEventSink = interface
['{4448A3FD-20EF-40E6-A402-F6209D2C2FD1}']
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  end;

  //implementors of IStatefulLogger must implement ILogger
  IStatefulLogger = interface
    procedure Advise( aSink: ILoggerEventSink );
    procedure UnAdvise( aSink: ILoggerEventSink );
  end;

function CreateStatefulLogger: IStatefulLogger;
function Indent( s: string; level: integer ): string;

implementation

uses
  gdycommon, typinfo, forms;

function Indent( s: string; level: integer ): string;
  function PrefixEachLine( s: string; pfx: string ): string;
  begin
    Result := s;
    if Result <> '' then
      Result := pfx + StringReplace( Result, #10, #10 + pfx, [rfReplaceAll] );
  end;
begin
  Result := PrefixEachLine( s, StringOfChar(' ',level*4) );
end;


type
  TLoggerEventSinks = class (TInterfacedObject, ILoggerEventSink )
  private
    FList: TInterfaceList;
    {ILoggerEventSink}
    procedure BlockEntered( aLogState: TLogState );
    procedure BlockLeaving( aLogState: TLogState );
    procedure ContextItemLogged( aLogState: TLogState );
    procedure MessageLogged( aLogState: TLogState );
  public
    constructor Create;
    destructor Destroy; override;
    procedure Advise( aSink: ILoggerEventSink );
    procedure UnAdvise( aSink: ILoggerEventSink );
  end;

  TStatefulLogger = class (TInterfacedObject, IStatefulLogger, ILogger)
  private
    FState: TLogState;
    FSinks: TLoggerEventSinks;
  protected
    {ILogger}
    procedure LogEntry( blockname: string );
    procedure LogExit;
    procedure LogContextItem( tag: string; value: string );
    procedure LogMessage( aKind: TLogMessageKind; aText: string; aDetails: string; aMadStack: string = '' );

    procedure Advise( aSink: ILoggerEventSink );
    procedure UnAdvise( aSink: ILoggerEventSink );
  public
    constructor Create;
    destructor Destroy; override;
  end;

function CreateStatefulLogger: IStatefulLogger;
begin
  Result := TStatefulLogger.Create;
end;

{ TLogger }

constructor TStatefulLogger.Create;
begin
  FSinks := TLoggerEventSinks.Create;
  FState := TLogState.Create;

  //������, ��� ����� ����� ����� ���� ������� logmessage, ��� ���������������� LogEntry
  {$ifdef RUSSIAN}
  LogEntry( '���������� ' + Application.Title ); //creates some context
  {$else}
  LogEntry( Application.Title + ' application' ); //creates some context
  {$endif}
end;

destructor TStatefulLogger.Destroy;
begin
  LogExit;
  inherited;
  FreeAndNil( FState );
  FreeAndNil( FSinks );
end;

procedure TStatefulLogger.LogEntry(blockname: string);
begin
  FState.AddBlock( blockname );
  FSinks.BlockEntered( FState );
end;

procedure TStatefulLogger.LogContextItem(tag, value: string);
begin
  if FState.IsValid then
  begin
    with FState.Last.Context.Add do
    begin
      ContextTag := tag;
      ContextValue := value;
    end;
    FSinks.ContextItemLogged( FState );
  end
end;

procedure TStatefulLogger.LogMessage(aKind: TLogMessageKind; aText: string; aDetails: string; aMadStack: string = '');
begin
  if FState.IsValid then
  begin
    with FState.Last.Messages.Add do
    begin
      Kind := aKind;
      Text := aText;
      Details := aDetails;
      MadStack := aMadStack; 
    end;
    FSinks.MessageLogged( FState );
  end
end;

procedure TStatefulLogger.LogExit;
begin
  if FState.IsValid then
  begin
    FSinks.BlockLeaving( FState );
    FState.RemoveTopBlock;
  end;
end;

{ TBlockContext }

procedure TLogBlock.Assign(Source: TPersistent);
begin
  if Source is TLogBlock then
  begin
    BlockName := TLogBlock(Source).BlockName;
    Context :=  TLogBlock(Source).Context;
    Messages := TLogBlock(Source).Messages;
  end
  else
    inherited;
end;

constructor TLogBlock.Create(Collection: TCollection);
begin
  FContext := TLogBlockContex.Create;
  FMessages := TLogMessages.Create;
  inherited;
end;

destructor TLogBlock.Destroy;
begin
  inherited;
  FreeAndNil( FMessages );
  FreeAndNil( FContext );
end;

procedure TStatefulLogger.Advise(aSink: ILoggerEventSink);
begin
  FSinks.Advise( aSink );
end;

procedure TStatefulLogger.UnAdvise(aSink: ILoggerEventSink);
begin
  FSinks.UnAdvise( aSink );
end;

{ TLogBlockContextItem }

procedure TLogBlockContextItem.Assign(Source: TPersistent);
begin
  if Source is TLogBlockContextItem then
  begin
    FContextValue := TLogBlockContextItem(Source).FContextValue;
    FContextTag := TLogBlockContextItem(Source).FContextTag;
  end
  else
    inherited;
end;

function TLogBlockContextItem.RenderAsString: string;
begin
  Result := ContextTag + ' = ' + ContextValue;
end;

{ TLogBlockContex }

function TLogBlockContex.Add: TLogBlockContextItem;
begin
  Result := inherited Add as TLogBlockContextItem;
end;

constructor TLogBlockContex.Create;
begin
  inherited Create( TLogBlockContextItem );
end;

function TLogBlockContex.GetItem(Index: Integer): TLogBlockContextItem;
begin
  Result := inherited Items[ Index ] as TLogBlockContextItem;
end;

function TLogBlockContex.Last: TLogBlockContextItem;
begin
  Result := Items[Count-1];
end;

procedure TLogBlockContex.SetItem(Index: Integer; const Value: TLogBlockContextItem);
begin
  Items[Index] := Value;
end;

{ TLogMessage }

procedure TLogMessage.Assign(Source: TPersistent);
begin
  if Source is TLogMessage then
  begin
    FKind := TLogMessage(Source).FKind;
    FText := TLogMessage(Source).FText;
    FDetails := TLogMessage(Source).FDetails;
  end
  else
    inherited;
end;

function TLogMessage.KindAsStr: string;
begin
  Result := getenumname( typeinfo(TLogMessageKind), ord(Kind));
  Result := copy( Result, 3, Length(Result)-2 );
end;

function TLogMessage.RenderAsString: string;
begin
  Result := KindAsStr + ': ' + Text + #13#10 + Details;
  if MadStack <> '' then
    Result := Result + #10#13 + MadStack;
end;

{ TLogMessages }

function TLogMessages.Add: TLogMessage;
begin
  Result := inherited Add as TLogMessage;
end;

constructor TLogMessages.Create;
begin
  inherited Create( TLogMessage );
end;

function TLogMessages.GetItem(Index: Integer): TLogMessage;
begin
  Result := inherited Items[ Index ] as TLogMessage;
end;

function TLogMessages.Last: TLogMessage;
begin
  Result := Items[count-1];
end;

procedure TLogMessages.SetItem(Index: Integer; const Value: TLogMessage);
begin
  Items[Index] := Value;
end;

{ TLogContextStack }

procedure TLogState.AddBlock(aBlockname: string);
begin
  with FBlockStack.Add do
  begin
    BlockName := aBlockname;
  end
end;

procedure TLogState.Assign(Source: TPersistent);
begin
  if Source is TLogState then
  begin
    FBlockStack.Assign( TLogState(Source).FBlockStack );
  end
  else
    inherited;
end;

constructor TLogState.Create;
begin
  FBlockStack := TLogBlocks.Create;
end;

destructor TLogState.Destroy;
begin
  inherited;
  FreeAndNil( FBlockStack );
end;

function TLogState.Dump: string;
  function DumpLogBlock( lb: TLogBlock ): string;
  var
    i: integer;
  begin
    Result := lb.BlockName + endl;
    for i := 0 to lb.Context.Count-1 do
      Result := Result + lb.Context[i].ContextTag + ' = ' + lb.Context[i].ContextValue + endl;
    for i := 0 to lb.Messages.Count-1 do
      Result := Result + getenumname( typeinfo(TLogMessageKind), ord(lb.Messages[i].Kind)) + ': ' + lb.Messages[i].Text + endl;
  end;
var
  i: integer;
begin
  Result := '';
  for i := 0 to FBlockStack.Count-1 do
    Result := Result + Indent( DumpLogBlock( FBlockStack[i] ), i );
end;

function TLogState.IsValid: boolean;
begin
  Assert( FBlockStack.Count > 0 ); 
  Result := FBlockStack.Count > 0;
end;

procedure TLogState.RemoveTopBlock;
begin
  Assert( IsValid );
  FBlockStack.Delete( FBlockStack.Count-1 );
end;

function TLogState.Last: TLogBlock;
begin
  Assert( IsValid );
  Result := FBlockStack[FBlockStack.Count-1];
end;

procedure TLogBlock.SetContext(const Value: TLogBlockContex);
begin
  FContext.Assign( Value );
end;

procedure TLogBlock.SetMassages(const Value: TLogMessages);
begin
  FMessages.Assign( Value );
end;

{ TLogBlocks }

function TLogBlocks.Add: TLogBlock;
begin
  Result := inherited Add as TLogBlock;
end;

constructor TLogBlocks.Create;
begin
  inherited Create( TLogBlock );
end;

function TLogBlocks.GetItem(Index: Integer): TLogBlock;
begin
  Result := inherited Items[ Index ] as TLogBlock;
end;

procedure TLogBlocks.SetItem(Index: Integer; const Value: TLogBlock);
begin
  Items[Index] := Value;
end;

{ TLoggerEventSinks }

procedure TLoggerEventSinks.Advise(aSink: ILoggerEventSink);
begin
  if assigned(aSink) then
    if FList.IndexOf(aSink) < 0 then
      FList.Add(aSink);
end;

procedure TLoggerEventSinks.BlockEntered(aLogState: TLogState);
var
  i: integer;
begin
  try
    for i := 0 to FList.Count-1 do
      (FList[i] as ILoggerEventSink).BlockEntered( aLogState );
  except
  end;
end;

procedure TLoggerEventSinks.BlockLeaving(aLogState: TLogState);
var
  i: integer;
begin
  try
    for i := 0 to FList.Count-1 do
      (FList[i] as ILoggerEventSink).BlockLeaving( aLogState );
  except
  end;
end;

procedure TLoggerEventSinks.ContextItemLogged(aLogState: TLogState);
var
  i: integer;
begin
  try
    for i := 0 to FList.Count-1 do
      (FList[i] as ILoggerEventSink).ContextItemLogged( aLogState );
  except
  end;
end;

procedure TLoggerEventSinks.MessageLogged(aLogState: TLogState);
var
  i: integer;
begin
  try
    for i := 0 to FList.Count-1 do
      (FList[i] as ILoggerEventSink).MessageLogged( aLogState );
  except
  end;
end;

constructor TLoggerEventSinks.Create;
begin
  FList := TInterfaceList.Create;
end;

destructor TLoggerEventSinks.Destroy;
begin
  Assert( not assigned(FList) or (FList.Count = 0) );
  inherited;
  FreeAndNil( FList );
end;

procedure TLoggerEventSinks.UnAdvise(aSink: ILoggerEventSink);
begin
  if assigned( aSink ) then
    FList.Remove( aSink );
end;


function TLogState.Clone: TLogState;
begin
  Result := TLogState.Create;
  Result.Assign( Self );
end;

end.
