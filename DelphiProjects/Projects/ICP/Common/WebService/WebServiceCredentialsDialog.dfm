inherited WebServiceCredentialsDlg: TWebServiceCredentialsDlg
  Width = 367
  Height = 111
  object lblWindowsAccount: TLabel
    Left = 8
    Top = 16
    Width = 86
    Height = 13
    Caption = 'Windows account'
  end
  object lblPassword: TLabel
    Left = 8
    Top = 48
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object Label3: TLabel
    Left = 0
    Top = 85
    Width = 367
    Height = 26
    Align = alBottom
    Alignment = taCenter
    Caption = 
      'The web service can run only the tasks that have been created un' +
      'der this account'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object edAccount: TEdit
    Left = 104
    Top = 12
    Width = 257
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
  end
  object edPassword: TEdit
    Left = 104
    Top = 40
    Width = 257
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    PasswordChar = '*'
    TabOrder = 1
  end
end
