unit webserviceCommon;

interface

uses
  isSettings;

type
  TWebServiceConfiguration = record
    Port: Integer;
  end;

  TWebServiceCredentials = record
    Account: string;
    Password: string;
  end;

procedure SaveWebServiceConfiguration(const conf: TWebServiceConfiguration; settings: IisSettings; root: string);
function LoadWebServiceConfiguration(settings: IisSettings; root: string): TWebServiceConfiguration;

implementation

uses
  gdycommon;

procedure SaveWebServiceConfiguration(const conf: TWebServiceConfiguration; settings: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'WebServiceConfiguration\';
  settings.AsInteger[root + 'Port'] := conf.Port;
end;

function LoadWebServiceConfiguration(settings: IisSettings; root: string): TWebServiceConfiguration;
begin
  root := root + IIF(root='','','\') + 'WebServiceConfiguration\';
  Result.Port := settings.AsInteger[root + 'Port'];
  if Result.Port = 0 then
    Result.Port := 1088; 
end;


end.
