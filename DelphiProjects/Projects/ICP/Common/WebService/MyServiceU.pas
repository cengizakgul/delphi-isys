unit MyServiceU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  IdHTTPWebBrokerBridge, webserviceCommon;

type
  TMyService = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    FWebBrokerBridge: TIdHTTPWebBrokerBridge;
  public
    function GetServiceController: TServiceController; override;
  end;

var
  MyService: TMyService;

implementation

{$R *.DFM}

uses
  MyWebModuleU, common;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  MyService.Controller(CtrlCode);
end;

function TMyService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TMyService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  // Create server.
  FWebBrokerBridge := TIdHTTPWebBrokerBridge.Create(Self);

  // Register web module class.
  FWebBrokerBridge.RegisterWebModuleClass(TMyWebModule);

  // Set default port.
  FWebBrokerBridge.DefaultPort := LoadWebServiceConfiguration(AppSettings, '').Port;

  // Start server.
  FWebBrokerBridge.Active := True;
end;

procedure TMyService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  // Stop server.
  FWebBrokerBridge.Active := False;

  // Free server component.
  FreeAndNil(FWebBrokerBridge);
end;

end.
