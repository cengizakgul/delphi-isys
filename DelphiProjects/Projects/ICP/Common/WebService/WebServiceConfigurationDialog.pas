unit WebServiceConfigurationDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, StdCtrls, webServiceCommon;

type
  TWebServiceConfigurationDlg = class(TDialogBase)
    edtPort: TEdit;
    lblPort: TLabel;
  private
    function GetConfiguration: TWebServiceConfiguration;
    procedure SetConfiguration(const Value: TWebServiceConfiguration);
  public
    constructor Create( Owner: TComponent ); override;
    function CanClose: boolean; override;
    property Configuration: TWebServiceConfiguration read GetConfiguration write SetConfiguration;
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TWebServiceConfigurationDlg }

function TWebServiceConfigurationDlg.CanClose: boolean;
begin
  Result := IsInteger(edtPort.Text);
end;

constructor TWebServiceConfigurationDlg.Create(Owner: TComponent);
begin
  inherited;
  Caption := 'Web service configuration';
end;

function TWebServiceConfigurationDlg.GetConfiguration: TWebServiceConfiguration;
begin
  Result.Port := StrToInt(edtPort.Text);
end;

procedure TWebServiceConfigurationDlg.SetConfiguration(const Value: TWebServiceConfiguration);
begin
  edtPort.Text := IntToStr(Value.Port);
end;

end.
