unit WebServiceCredentialsDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, StdCtrls, webserviceCommon;

type
  TWebServiceCredentialsDlg = class(TDialogBase)
    lblWindowsAccount: TLabel;
    edAccount: TEdit;
    lblPassword: TLabel;
    edPassword: TEdit;
    Label3: TLabel;
  private
    function GetCredentials: TWebServiceCredentials;
    procedure SetCredentials(const Value: TWebServiceCredentials);
    { Private declarations }
  public
    constructor Create( Owner: TComponent ); override;
    function MainControl: TWinControl; override;
    property Credentials: TWebServiceCredentials read GetCredentials write SetCredentials ;
  end;


implementation

{$R *.dfm}

{ TWebServiceCredentialsDlg }

constructor TWebServiceCredentialsDlg.Create(Owner: TComponent);
begin
  inherited;
  Caption := 'Web service installation';
end;

function TWebServiceCredentialsDlg.GetCredentials: TWebServiceCredentials;
begin
  Result.Account := edAccount.Text;
  Result.Password := edPassword.Text;
end;

function TWebServiceCredentialsDlg.MainControl: TWinControl;
begin
  Result := edPassword;
end;

procedure TWebServiceCredentialsDlg.SetCredentials(const Value: TWebServiceCredentials);
begin
  edAccount.Text := Value.Account;
  edPassword.Text := Value.Password;
end;


end.
