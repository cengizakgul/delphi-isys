unit EvoAPIConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, evoapiconnection, common, passwordedit, OptionsBaseFrame,
  Buttons;

type
  TEvoAPIConnectionParamFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    lblAPIServer: TLabel;
    lblUserName: TLabel;
    lblPassword: TLabel;
    APIServerEdit: TEdit;
    PasswordEdit: TPasswordEdit;
    UserNameEdit: TEdit;
    cbSavePassword: TCheckBox;
    SpeedButton1: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
  private
    function GetParam: TEvoAPIConnectionParam;
    procedure SetParam(const Value: TEvoAPIConnectionParam);
  public
    property Param: TEvoAPIConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
    procedure Check;
    procedure ForceSavePassword;
  end;

implementation

uses
  gdycommon, gdyClasses;

{$R *.dfm}

{ TEvoAPIConnectionParamFrm }


procedure TEvoAPIConnectionParamFrm.Check;
var
  err: string;
  procedure AddErr(s: string);
  begin
    if err <> '' then
      err := err + ',';
    err := err + #13#10 + s;
  end;
begin
  err := '';
  if trim(Param.APIServerAddress) = '' then
    AddErr(lblAPIServer.Caption + ' is not specified')
  else
    if not (SplitByChar(trim(APIServerEdit.Text), ':').Count in [1,2]) then
      AddErr(lblAPIServer.Caption + ' is invalid');
  if trim(UserNameEdit.Text) = '' then
    AddErr(lblUserName.Caption + ' is not specified');
  if PasswordEdit.Password = '' then
    AddErr(lblPassword.Caption + ' is not specified');
  if err <> '' then
    raise Exception.Create('Invalid Evolution connection settings:'+err);
end;

procedure TEvoAPIConnectionParamFrm.ForceSavePassword;
begin
  cbSavePassword.Checked := true;
end;

function TEvoAPIConnectionParamFrm.GetParam: TEvoAPIConnectionParam;
begin
  with SplitByChar(trim(APIServerEdit.Text), ':') do
    if Count = 2 then
    begin
      Result.APIServerAddress := Str[0];
      Result.APIServerPort := strtoint(Str[1]);
    end
    else
    begin
      Result.APIServerAddress := trim(APIServerEdit.Text);
      Result.APIServerPort := 9943;
    end;
  Result.Username := trim(UserNameEdit.Text);
  Result.Password := PasswordEdit.Password;
  Result.SavePassword := cbSavePassword.Checked;
end;

function TEvoAPIConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(APIServerEdit.Text) <> '') and
            (SplitByChar(trim(APIServerEdit.Text), ':').Count in [1,2]) and
            (trim(UserNameEdit.Text) <> '') and
            (PasswordEdit.Password <> '');
end;

procedure TEvoAPIConnectionParamFrm.SetParam( const Value: TEvoAPIConnectionParam);
begin
  APIServerEdit.Text := Value.APIServerAddress;
  if Value.APIServerPort <> 9943 then
    APIServerEdit.Text := APIServerEdit.Text + ':' + inttostr(Value.APIServerPort);
  UserNameEdit.Text := Value.Username;
  PasswordEdit.Password := Value.Password;
  cbSavePassword.Checked := Value.SavePassword;
end;

const
  msg =
'Please contact your service bureau to obtain this address.'#13#10 +
#13#10+
'Service Bureau: The Evolution API service is configured in the Evolution Management Console. '+
'It requires public access via corporate firewalls and a public DNS record in the same fashion that the Remote Relay is configured. '+
'For more detailed information, see the API service description in the Evolution Technical Administration guide.'#13#10 +
#13#10+
'Socket Errors: Be sure to check the following in the event of a socket error'#13#10 +
'1.       Is the API server name correct? Check with your Service Bureau.'#13#10 +
'2.       Is a local Windows or other firewall present on this workstation that might be blocking outbound traffic to your API server using TCP port 9943'#13#10 +
'3.       Is a network firewall in place that might be blocking this traffic? Check with your IT staff.'#13#10 +
#13#10+
'Servername and TCP port override:'#13#10 +
'1.       The default API server uses TCP port 9943.'#13#10 +
'2.       It is possible to run the API service using an alternative port'#13#10 +
'3.       Your Service Bureau will advise you if the API service is running under a custom TCP port'#13#10 +
'4.       You can specify the custom TCP port in the API server field using the syntax below'#13#10 +
'         api.serviceburea.com:<TCPPORT>'#13#10 +
'         like api.servicebureau.com:9944'#13#10;

procedure TEvoAPIConnectionParamFrm.SpeedButton1Click(Sender: TObject);
begin
  ShowMessage(msg);
end;

end.
