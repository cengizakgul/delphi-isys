unit FileOpenFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, StdCtrls, ActnList, StdActns, Buttons;

type
  TFileSelectedEvent = procedure (filename: string) of object;

  TFileOpenFrm = class(TFrame)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    edtFile: TEdit;
    Label1: TLabel;
    procedure FileOpen1Accept(Sender: TObject);
  private
    FOnFileSelected: TFileSelectedEvent;
    function GetFilename: string;
    procedure SetFilename(const Value: string);
  public
    function IsValid: boolean;
    property Filename: string read GetFilename write SetFilename;
    property OnFileSelected: TFileSelectedEvent read FOnFileSelected write FOnFileSelected;
  end;

implementation

{$R *.dfm}

{ TFileOpenFrm }

procedure TFileOpenFrm.FileOpen1Accept(Sender: TObject);
begin
  if assigned(FOnFileSelected) then
    FOnFileSelected((Sender as TFileOpen).Dialog.FileName);
  edtFile.Text := (Sender as TFileOpen).Dialog.FileName;
end;

function TFileOpenFrm.GetFilename: string;
begin
  Result := trim(edtFile.Text);
end;

function TFileOpenFrm.IsValid: boolean;
begin
  Result := Filename <> '';
end;

procedure TFileOpenFrm.SetFilename(const Value: string);
begin
  edtFile.Text := Value;
  FileOpen1.Dialog.FileName := Value;
end;

end.
