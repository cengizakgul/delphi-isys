unit dbdtDS;

interface

uses
  kbmMemTable, evodata, common, db, classes, gdycommon{TStringArray};

type
  TDBDTTable = class(TComponent)
  private
    FLevel: char;
    FDBDT: TkbmCustomMemTable;
    FKeyFields: string;

    procedure CreateDS;
    procedure HandleFilterDBDT(DataSet: TDataSet; var Accept: Boolean);
    procedure SetLevel(const Value: char);
    function GenKeyFields: string;
  public
    constructor Create(CUSTOM_DBDT: TDataSet); reintroduce;
    function LocateByKeys(vals: TStringArray): boolean;
    function KeyValues: TStringArray;
    property Level: char read FLevel write SetLevel;
    property KeyFields: string read FKeyFields;
    property DS: TkbmCustomMemTable read FDBDT;
  end;

implementation

uses
  evConsts, variants, windows, sysutils;

{ TDBDTTable }

constructor TDBDTTable.Create(CUSTOM_DBDT: TDataSet);
var
  lst: TStringList;
  fieldNames: string;
begin
  inherited Create(nil);
  CreateDS;

  FDBDT.Open;

  lst := TStringList.Create;
  try
    FDBDT.GetFieldNames(lst);
    lst.Delete( lst.IndexOf('FILTER') );
    lst.Delimiter := ';';
    fieldNames := lst.DelimitedText;
  finally
    FreeAndNil(lst);
  end;

  CUSTOM_DBDT.First;
  while not CUSTOM_DBDT.Eof do
  begin
    FDBDT.Append;
    try
      FDBDT.FieldValues[fieldNames] := CUSTOM_DBDT.FieldValues[fieldNames];
      FDBDT['FILTER'] := false;
      FDBDT.Post;
    except
      FDBDT.Cancel;
      raise;
    end;
    CUSTOM_DBDT.Next;
  end;

  FLevel := CLIENT_LEVEL_COMPANY;
  FDBDT.OnFilterRecord := HandleFilterDBDT;
  FDBDT.Filtered := true;
end;

procedure TDBDTTable.HandleFilterDBDT(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (FLevel > CLIENT_LEVEL_COMPANY) and Dataset.FieldByName('FILTER').AsBoolean;
end;

function TDBDTTable.GenKeyFields: string;
var
  level: char;
begin
  Result := '';
  for level := CLIENT_LEVEL_DIVISION to FLevel do
  begin
    if Result <> '' then
      Result := Result + ';';
    Result := Result + CDBDTMetadata[level].CodeField;
  end
end;

procedure TDBDTTable.SetLevel(const Value: char);
var
  nFields: integer;
  function AreDifferent(v1, v2: Variant): boolean;
  var
    i: integer;
  begin
    if VarIsArray(v1) then
    begin
      for i := 0 to nFields-1 do
        if not VarIsEqual(v1[i], v2[i]) then
        begin
          Result := true;
          exit;
        end;
      Result := false;
    end
    else
      Result := not VarIsEqual(v1, v2);
  end;
var
  keyVals: Variant;
  newKeyVals: Variant;
  lev: char;
  keyflds: string;
begin
  Assert( Value in [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] );
  FLevel := Value;
  FKeyFields := GenKeyFields;
  FDBDT.Filtered := false;
  if FLevel > CLIENT_LEVEL_COMPANY then
  begin
    nFields := Ord(Value) - Ord(CLIENT_LEVEL_COMPANY); 
    keyVals := Unassigned;

    // copied from TISKbmMemDataSet.SetIndexFieldNames
    keyflds := FKeyFields;
    //just like TISkbmDataSet
    if (FDBDT.Indexes.GetByFieldNames(keyflds) = nil) then
      FDBDT.AddIndex('IND_'+ StringReplace(keyflds, ';', '', [rfReplaceAll]), keyflds, []);
    FDBDT.IndexFieldNames := keyflds;

    FDBDT.Filtered := false;
    FDBDT.First;
    while not FDBDT.Eof do
    begin
      newKeyVals := FDBDT.FieldValues[keyflds];
      FDBDT.Edit;
      try
        FDBDT['FILTER'] := VarIsEmpty(keyVals) or AreDifferent(keyVals, newKeyVals);
        FDBDT.Post;
      except
        FDBDT.Cancel;
        raise;
      end;
      keyVals := newKeyVals;
      FDBDT.Next;
    end;
  end;
  FDBDT.Filtered := true;
  for lev := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
  begin
    FDBDT.FieldByName(CDBDTMetadata[lev].CodeField).Visible := lev <= FLevel;
    FDBDT.FieldByName(CDBDTMetadata[lev].NameField).Visible := lev <= FLevel;
  end;
end;

function TDBDTTable.KeyValues: TStringArray;
var
  level: char;
begin
  SetLength(Result, Integer(FLevel) - Integer(CLIENT_LEVEL_COMPANY) );
  for level := CLIENT_LEVEL_DIVISION to FLevel do
    Result[Integer(level)-Integer(CLIENT_LEVEL_DIVISION)] := FDBDT.FieldByName(CDBDTMetadata[level].CodeField).AsString;
end;


function StringArrayToVar(ss: TStringArray): Variant;
var
  vv: array of Variant;
  i: integer;
begin
  if length(ss) = 1 then
    Result := ss[0]
  else
  begin
    SetLength(vv, Length(ss));
    for i := 0 to high(ss) do
      vv[i] := ss[i];
    Result := VarArrayOf(vv);
  end;
end;

function TDBDTTable.LocateByKeys(vals: TStringArray): boolean;
begin
  Result := FDBDT.Locate(KeyFields, StringArrayToVar(vals), []);
end;

procedure TDBDTTable.CreateDS;
var
  level: char;
begin
  FDBDT := TkbmCustomMemTable.Create(Self);

  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
  begin
    CreateIntegerField( FDBDT, 'CO_' + UpperCase(CDBDTMetadata[level].Name) + '_NBR', CDBDTMetadata[level].Name +' internal code').Visible := false;;
    CreateStringField( FDBDT, CDBDTMetadata[level].CodeField, CDBDTMetadata[level].Name +' code', 20);
    CreateStringField( FDBDT, CDBDTMetadata[level].NameField, CDBDTMetadata[level].Name +' name', 40);
    CreateStringField( FDBDT, CDBDTMetadata[level].PrintField, 'Print ' + AnsiLowerCase(CDBDTMetadata[level].Name), 1).Visible := false;
  end;
  with TBooleanField.Create(FDBDT) do
  begin
    FieldName := 'FILTER';
    DisplayLabel := 'Filter';
    DataSet := FDBDT;
    Visible := false;
  end;
end;

end.
