inherited CodesFilterFrm: TCodesFilterFrm
  Width = 240
  Height = 279
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 240
    Height = 279
    Align = alClient
    Caption = 'xxx'
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 236
      Height = 26
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object cbUse: TCheckBox
        Left = 6
        Top = 0
        Width = 145
        Height = 17
        Caption = 'Filter'
        TabOrder = 0
        OnClick = cbUseClick
      end
    end
    object lbStatuses: TCheckListBox
      Left = 2
      Top = 41
      Width = 236
      Height = 236
      OnClickCheck = lbStatusesClickCheck
      Align = alClient
      ItemHeight = 13
      TabOrder = 1
    end
  end
end
