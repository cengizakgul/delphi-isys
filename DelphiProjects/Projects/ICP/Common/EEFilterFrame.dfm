inherited EeFilterFrm: TEeFilterFrm
  Width = 530
  Height = 509
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 530
    Height = 509
    Align = alClient
    Caption = 'Employee filter'
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 239
      Height = 492
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      inline EEStatusFilterFrame: TCodesFilterFrm
        Left = 0
        Top = 0
        Width = 239
        Height = 279
        Align = alClient
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 239
          inherited Panel1: TPanel
            Width = 235
          end
          inherited lbStatuses: TCheckListBox
            Width = 235
          end
        end
      end
      inline EEPositionStatusFilterFrame: TCodesFilterFrm
        Left = 0
        Top = 279
        Width = 239
        Height = 151
        Align = alBottom
        TabOrder = 1
        inherited GroupBox1: TGroupBox
          Width = 239
          Height = 151
          inherited Panel1: TPanel
            Width = 235
          end
          inherited lbStatuses: TCheckListBox
            Width = 235
            Height = 108
          end
        end
      end
      inline EESalaryFilterFrame: TEESalaryFilterFrm
        Left = 0
        Top = 430
        Width = 239
        Height = 62
        Align = alBottom
        TabOrder = 2
        inherited GroupBox1: TGroupBox
          Width = 239
          inherited Panel1: TPanel
            Width = 235
          end
        end
      end
    end
    inline DBDTFilterFrame: TDBDTFilterFrm
      Left = 241
      Top = 15
      Width = 287
      Height = 492
      Align = alClient
      TabOrder = 1
      inherited GroupBox1: TGroupBox
        Width = 287
        Height = 492
        inherited Panel1: TPanel
          Width = 283
        end
        inherited dgDBDT: TReDBCheckGrid
          Width = 283
          Height = 441
        end
      end
    end
  end
end
