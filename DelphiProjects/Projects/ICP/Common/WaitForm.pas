unit WaitForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TJobEvent = procedure of object;
  TJobEvent1 = procedure(arg: Variant) of object;

  TWaitFm = class(TForm)
    lblMessage: TPanel;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    FJob: TJobEvent;
    FJob1: TJobEvent1;
    FArg: Variant;
  end;

function ExecuteWithWaitForm( AOwner: TComponent; ex: TJobEvent; op:  string): boolean; overload; deprecated;
function ExecuteWithWaitForm( AOwner: TComponent; ex: TJobEvent1; arg: Variant; op:  string): boolean; overload; deprecated;

implementation



{$R *.dfm}
function ExecuteWithWaitForm( AOwner: TComponent; ex: TJobEvent; op:  string): boolean;
begin
  with TWaitFm.Create(AOwner) do
  try
    FJob := ex;
    Caption := op;
    Result := ShowModal = mrYes;
  finally
    Free;
  end;
end;

function ExecuteWithWaitForm( AOwner: TComponent; ex: TJobEvent1; arg: Variant; op:  string): boolean;
begin
  with TWaitFm.Create(AOwner) do
  try
    FJob1 := ex;
    FArg := arg;
    Caption := op;
    Result := ShowModal = mrYes;
  finally
    Free;
  end;
end;

procedure TWaitFm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := false;
  try
    if assigned(FJob) then
      FJob
    else if assigned(FJob1) then
      FJob1(FArg)
    else
      Assert(false);
    ModalResult := mrYes;
  except
    ModalResult := mrNo;
    raise;
  end;
end;

end.


