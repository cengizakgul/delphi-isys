unit EEFilter;

interface

uses
  gdystrset, isSettings, gdycommon, db, gdyCommonLogger;

type
  TCodesFilter = record
    Codes: TStringSet;
    Use: boolean;
  end;

  TEESalaryFilter = record
    Salaried: boolean;
    Use: boolean;
  end;

  TDBDTFilterItems = array of TStringArray;

  TDBDTFilter = record
    Level: char;
    Items: TDBDTFilterItems;
    Use: boolean;
  end;

  TEEFilter = record
    EEStatusFilter: TCodesFilter;
    EEPositionStatusFilter: TCodesFilter;
    EESalaryFilter: TEESalaryFilter;
    DBDTFilter: TDBDTFilter;
  end;

function AcceptedByFilter(Logger: ICommonLogger; EEs: TDataSet; filter: TEEFilter): boolean;

function LoadEEFilter(conf: IisSettings; root: string): TEEFilter;
procedure SaveEEFilter( const filter: TEEFilter; conf: IisSettings; root: string);

procedure LogEEFilter(Logger: ICommonLogger; const filter: TEEFilter);
function DescribeEEFilter(const filter: TEEFilter): string;

implementation

uses
  isBaseClasses, common, gdyClasses, evConsts, sysutils, evodata, combochoices;

function LoadEESalaryFilter(conf: IisSettings; root: string): TEESalaryFilter;
begin
  root := root + IIF(root='','','\') + 'EESalaryFilter\';
  Result.Use := conf.AsBoolean[root+'Use'];
  Result.Salaried := conf.AsBoolean[root+'Salaried'];
end;

procedure SaveEESalaryFilter(const filter: TEESalaryFilter; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EESalaryFilter\';
  conf.AsBoolean[root+'Use'] := filter.Use;
  conf.AsBoolean[root+'Salaried'] := filter.Salaried;
end;

function LoadCodesFilter(conf: IisSettings; root: string): TCodesFilter;
var
  i: integer;
begin
  root := root + IIF(root='','','\') + 'CodesFilter\';
  Result.Use := conf.AsBoolean[root+'Use'];
  SetLength(Result.Codes, 0);
  with CsvAsStr(conf.AsString[root+'Codes']) do
    for i := 0 to Count-1 do
      SetInclude(Result.Codes, Str[i]);
end;

procedure SaveCodesFilter(const filter: TCodesFilter; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'CodesFilter\';
  conf.AsBoolean[root+'Use'] := filter.Use;
  conf.AsString[root+'Codes'] := StringSetToCsv(filter.Codes);
end;

function LoadDBDTFilter(conf: IisSettings; root: string): TDBDTFilter;
var
  i, j: integer;
  s: string;
  items: IisStringListRO;
  codes: IisStringListRO;
begin
  root := root + IIF(root='','','\') + 'DBDTFilter\';
  Result.Use := conf.AsBoolean[root+'Use'];
  s := conf.AsString[root+'Level'];
  try
    if (Length(s) = 1) and (s[1] in [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM]) then
      Result.Level := s[1]
    else
      Abort;

    items := conf.GetChildNodes(root + 'DBDTCodes');
    SetLength(Result.Items, items.Count);
    for i := 0 to items.Count-1 do
    begin
      codes := conf.GetValueNames(root + Format('DBDTCodes\%.4d', [i]) );
      if codes.Count <> (Ord(Result.Level) - Ord(CLIENT_LEVEL_COMPANY)) then
        Abort;
      SetLength(Result.Items[i], codes.Count);
      for j := 0 to codes.Count-1 do
      begin
        if codes.IndexOf(inttostr(j)) = -1 then
          Abort;
        Result.Items[i][j] := conf.AsString[root + Format('DBDTCodes\%.4d\%d', [i, j])];
      end
    end;
  except
    on EAbort do
    begin
      Result.Level := CLIENT_LEVEL_DIVISION;
      SetLength(Result.Items, 0);
      Result.Use := false;
    end;
  end;
end;

procedure SaveDBDTFilter(const filter: TDBDTFilter; conf: IisSettings; root: string);
var
  i: integer;
  j: integer;
begin
  root := root + IIF(root='','','\') + 'DBDTFilter\';
  conf.AsBoolean[root+'Use'] := filter.Use;
  conf.AsString[root+'Level'] := filter.Level;
  conf.DeleteNode(root + 'DBDTCodes');
  for i := 0 to high(filter.Items) do
    for j := 0 to high(filter.Items[i]) do
      conf.AsString[root + Format('DBDTCodes\%.4d\%d', [i, j])] := filter.Items[i][j];
end;

function LoadEEFilter(conf: IisSettings; root: string): TEEFilter;
begin
  root := root + IIF(root='','','\') + 'EEFilter';
  Result.EEStatusFilter := LoadCodesFilter(conf, root + '\EEStatus'); //because codes filter is generic
  Result.EEPositionStatusFilter := LoadCodesFilter(conf, root + '\EEPositionStatus'); //because codes filter is generic
  Result.EESalaryFilter := LoadEESalaryFilter(conf, root);
  Result.DBDTFilter := LoadDBDTFilter(conf, root);
end;

procedure SaveEEFilter( const filter: TEEFilter; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EEFilter';
  SaveCodesFilter(filter.EEStatusFilter, conf, root + '\EEStatus'); //because codes filter is generic
  SaveCodesFilter(filter.EEPositionStatusFilter, conf, root + '\EEPositionStatus'); //because codes filter is generic
  SaveEESalaryFilter(filter.EESalaryFilter, conf, root);
  SaveDBDTFilter(filter.DBDTFilter, conf, root);
end;

function GetDBDTCodes(EEs: TDataSet): TStringArray;
var
  level: char;
  fn: string;
begin
  SetLength(Result, 0);
  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
  begin
    fn := CDBDTMetadata[level].CodeField;
//    if EEs.FieldByName(fn).IsNull then
//      break;

    SetLength(Result, Length(Result)+1);
    Result[high(Result)] := EEs.FieldByName(fn).AsString;
  end
end;


function AcceptedByDBDTFilter(EEs: TDataSet; filter: TDBDTFilter): boolean;
var
  codes: TStringArray;
  i: integer;

  function CodesMatch(item: TStringArray): boolean;
  var
    j: integer;
  begin
    Assert( Length(item) <= Length(codes) );
    Result := true;
    for j := 0 to high(item) do
      if (codes[j] <> '') and (item[j] <> codes[j]) then  //!! ee's codes are blank for hidden departments and filter's codes are always non-blank
      begin
        Result := false;
        break;
      end;
  end;
begin
  Result := not filter.Use;
  if not Result then
  begin
    codes := GetDBDTCodes(EEs);
    Assert( filter.Level in [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM]);
    for i := 0 to high(filter.items) do
    begin
      Assert( Ord(filter.Level) - Ord(CLIENT_LEVEL_COMPANY) = Length(filter.Items[i]) );
      if CodesMatch(filter.Items[i]) then
      begin
        Result := true;
        break;
      end;
    end;
  end;
end;

function AcceptedByFilter(Logger: ICommonLogger; EEs: TDataSet; filter: TEEFilter): boolean;
begin
  Result := (not filter.EEStatusFilter.Use or InSet( EEs.FieldByName('CURRENT_TERMINATION_CODE').AsString, filter.EEStatusFilter.Codes))
            and
            (not filter.EEPositionStatusFilter.Use or InSet( EEs.FieldByName('POSITION_STATUS').AsString, filter.EEPositionStatusFilter.Codes))
            and
            (not filter.EESalaryFilter.Use or ((EEs.FieldByName('SALARY_AMOUNT').AsFloat > 0) = filter.EESalaryFilter.Salaried) )
            and
            AcceptedByDBDTFilter(EEs, filter.DBDTFilter);
  Logger.LogDebug( Format('%s accepted by filter: %s', [EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, boolToStr(Result, true)]) );
end;

function StrSetOfCodesToNames(codes: TStringSet; def: string): TStringSet;
var
  i: integer;
begin
  SetLength(Result, Length(codes));
  for i := 0 to high(codes) do
    Result[i] := GetEvoNameByCode(codes[i], def);
end;

function DescribeCodesFilter(const filter: TCodesFilter; caption: string; def: string): string;
begin
  Result := '';
  if filter.Use then
    Result := Format('%s filter: %s'#13#10,[caption, SetToStr(StrSetOfCodesToNames(filter.Codes, def), '; ')] );
end;

procedure LogCodesFilter(Logger: ICommonLogger; const filter: TCodesFilter; caption: string; def: string);
begin
  if filter.Use then
    Logger.LogContextItem(Format('%s filter - codes',[caption]), SetToStr( StrSetOfCodesToNames(filter.Codes, def), '; ' ) );
end;

procedure LogEESalaryFilter(Logger: ICommonLogger; const filter: TEESalaryFilter);
begin
  if filter.Use then
    Logger.LogContextItem('Salary filter - salaried?', BoolToStr(filter.Salaried, true) );
end;

function DescribeEESalaryFilter(const filter: TEESalaryFilter): string;
begin
  Result := '';
  if filter.Use then
    Result := 'Salary filter: ' + IIF(filter.Salaried, 'salaried', 'hourly') + #13#10;
end;

procedure LogDBDTFilter(Logger: ICommonLogger; const filter: TDBDTFilter);
var
  i: integer;
begin
  if filter.Use then
  begin
    Assert( filter.Level in [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM]);
    Logger.LogContextItem('D/B/D/T filter - level', CDBDTMetadata[filter.level].Name );
    for i := 0 to high(filter.items) do
      Logger.LogContextItem( Format('D/B/D/T filter - item %d', [i]), Join(filter.items[i], '; ') );
  end
end;

function DescribeDBDTFilter(const filter: TDBDTFilter): string;
var
  i: integer;
begin
  Result := '';
  if filter.Use then
  begin
    Assert( filter.Level in [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM]);
    Result := 'D/B/D/T filter: '#13#10'Level: ' + CDBDTMetadata[filter.level].Name + #13#10'Codes:'#13#10;
    for i := 0 to high(filter.items) do
      Result := Result + Join(filter.items[i], '; ') + #13#10
  end
end;

procedure LogEEFilter(Logger: ICommonLogger; const filter: TEEFilter);
begin
  LogCodesFilter(Logger, filter.EEStatusFilter, 'Employee status', EE_TerminationCode_ComboChoices);
  LogCodesFilter(Logger, filter.EEPositionStatusFilter, 'Employee position status', PositionStatus_ComboChoices);
  LogEESalaryFilter(Logger, filter.EESalaryFilter);
  LogDBDTFilter(Logger, filter.DBDTFilter);
end;

function DescribeEEFilter(const filter: TEEFilter): string;
begin
  Result := '';
  Result := Result + DescribeCodesFilter(filter.EEStatusFilter, 'Employee status', EE_TerminationCode_ComboChoices);
  Result := Result + DescribeCodesFilter(filter.EEPositionStatusFilter, 'Employee position status', PositionStatus_ComboChoices);
  Result := Result + DescribeEESalaryFilter(filter.EESalaryFilter);
  Result := Result + DescribeDBDTFilter(filter.DBDTFilter);
end;

end.
