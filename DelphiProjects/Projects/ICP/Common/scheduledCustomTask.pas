unit scheduledCustomTask;

interface

uses
  isSettings, scheduledTask, evoapiconnection, common, timeclockimport,
  evoapiconnectionutils, toaimport, kbmMemTable, evodata;

type
  TTaskAdapterCustomBase = class(TTaskAdapterBase)
  private
    FEvoAPIConnection: IEvoAPIConnection;
  protected
    FSettings: IisSettings;
    FUseAppSettings: boolean;
    function GetSettings: IisSettings;

    function GetEvoAPICOnnection: IEvoAPIConnection;
    function AutoSelectPrBatch: TEvoPayrollBatchDef;
    function GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData; virtual; abstract;
    function DoExportTOA: TTOAStat; virtual; abstract;
    function DoGetTOA: TTOAImportData; virtual; abstract;
  public
    constructor Create(UseAppSettings: boolean = True);
  published
    procedure TCImport_Execute;
    procedure TOAExport_Execute;
    procedure TOAImport_Execute;
  end;

implementation

uses
  sysutils, gdycommon, gdyClasses;

function TTaskAdapterCustomBase.GetEvoAPICOnnection: IEvoAPIConnection;
begin
  if FEvoAPIConnection = nil then
    FEvoAPIConnection := evoapiconnection.CreateEvoAPIConnection( LoadEvoAPIConnectionParam(FLogger, GetSettings, ''), FLogger);
  Result := FEvoAPIConnection;
end;

constructor TTaskAdapterCustomBase.Create(UseAppSettings: boolean = True);
begin
  FUseAppSettings := UseAppSettings;
  if UseAppSettings then
    FSettings := AppSettings;
end;

function TTaskAdapterCustomBase.AutoSelectPrBatch: TEvoPayrollBatchDef;
var
  EvoData: TEvoData;
begin
  EvoData := TEvoData.Create(FLogger);
  try
    EvoData.AddDS(TMP_PRDesc);
    EvoData.AddDS(PR_BATCHDesc);                                 
    EvoData.Connect( LoadEvoAPIConnectionParam(FLogger, GetSettings, '') );

    EvoData.SetClCo(FTask.Company);

    if EvoData.DS['TMP_PR'].RecordCount < 1 then
      raise Exception.Create('No payrolls with status "W"');
    if EvoData.DS['TMP_PR'].RecordCount > 1 then
      raise Exception.Create('More than one payroll with status "W". Cannot select automatically.');

    if EvoData.DS['PR_BATCH'].RecordCount < 1 then
      raise Exception.Create('No batches in payroll');
    if EvoData.DS['PR_BATCH'].RecordCount > 1 then
      raise Exception.Create('More than one batch in payroll. Cannot select automatically.');

    Result := EvoData.GetPrBatch;
  finally
    FreeAndNil(EvoData);
  end;
end;

procedure TTaskAdapterCustomBase.TCImport_Execute;
var
  opname: string;
  batch: TEvoPayrollBatchDef;
  tc: TTimeClockImportData;
  res: TTCImportResult;
  nAccepted: integer;
  msg: string;
begin
  opname := 'Timeclock data import';
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      batch := AutoSelectPrBatch;
      LogEvoPayrollBatchDef(FLogger, batch, false); //don't log company

      if not batch.HasRegularChecks then
        raise Exception.Create('This payroll batch doesn''t have auto-created checks');

      Assert( EvoCompanyDefsAreEqual(FTask.Company, batch.Company) );
      tc := GetTimeClockData(batch);

      if SplitToLines(tc.Text).Count = 0 then
        raise Exception.Create('There is no timeclock data for the chosen period');
      res := GetEvoAPIConnection.TCImport(batch, tc);

      nAccepted := SplitToLines(tc.Text).Count - res.RejectedRecords;
      if res.RejectedRecords > 0 then
      begin
        if nAccepted <> 0 then
          msg := Format('%s partially completed: %d check line%s rejected, %d %s accepted.',
                  [opname, res.RejectedRecords, IIF(res.RejectedRecords>1,'s were',' was'),
                   nAccepted, IIF(nAccepted>1,'were','was')] )
        else
          msg := Format('%s failed: all check lines were rejected.', [opname]);
        FLogger.LogWarning(msg + ' See details.', res.LogFile);
      end
      else
      begin
        FLogger.LogEventFmt('%s completed', [opname], Format('%d check line%s accepted.',
                  [nAccepted, IIF(nAccepted>1,'s were',' was')] ) + #13#10#13#10 + res.LogFile);
      end;

    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TTaskAdapterCustomBase.TOAExport_Execute;
var
  stat: TTOAStat;
  opname: string;
begin
  opname := 'TOA export from Evolution';
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      stat := DoExportTOA;

      if stat.Errors = 0 then
        FLogger.LogEvent( opname + ' ' + TOAStatToString(stat) )
      else
        FLogger.LogWarning(opname + ' ' + TOAStatToString(stat));
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TTaskAdapterCustomBase.TOAImport_Execute;
var
  data: TTOAImportData;
  stat: TTOAStat;
  opname: string;
begin
  opname := 'TOA import into Evolution';
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      data := DoGetTOA;
      stat := toaimport.ImportTOA(data.Recs, FTask.Company, CreateSilentEvoXImportExecutor(GetEvoAPICOnnection), FLogger);
      stat.Errors := stat.Errors + data.Errors;

      if stat.Errors = 0 then
        FLogger.LogEvent( opname + ' ' + TOAStatToString(stat) )
      else
        FLogger.LogWarning(opname + ' ' + TOAStatToString(stat));
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TTaskAdapterCustomBase.GetSettings: IisSettings;
begin
  if FUseAppSettings then
    Result := FSettings
  else
    Result := FTask.ParamSettings;  
end;

end.
