unit PlannedActionConfirmationForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TPlannedActionConfirmationFm = class(TForm)
    Panel1: TPanel;
    bbCancel: TBitBtn;
    bbOk: TBitBtn;
    Panel2: TPanel;
    mmActions: TMemo;
    procedure FormActivate(Sender: TObject);
  private
  public
  end;

function ConfirmActions(Owner: TComponent; actions: string): boolean;

implementation

{$R *.dfm}

function ConfirmActions(Owner: TComponent; actions: string): boolean;
begin
  if trim(actions) <> ''  then
    with TPlannedActionConfirmationFm.Create(Owner) do
    try
      Caption := Application.Title;
      mmActions.Lines.Text := actions;
      Result := ShowModal = mrOk;
    finally
      Free;
    end
  else
    Result := true;
end;

procedure TPlannedActionConfirmationFm.FormActivate(Sender: TObject);
begin
  SetForegroundWindow(Handle);
end;

end.
