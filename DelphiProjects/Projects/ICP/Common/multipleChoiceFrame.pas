unit multipleChoiceFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ActnList, Buttons, gdystrset;

type
  TMultipleChoiceFrm = class(TFrame)
    pnlAvailable: TPanel;
    pnlButtons: TPanel;
    pnlSelected: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ActionList1: TActionList;
    actAdd: TAction;
    actRemove: TAction;
    pnlAvailableHeader: TPanel;
    pnlSelectedHeader: TPanel;
    lbAvailable: TListBox;
    lbSelected: TListBox;
    procedure actAddExecute(Sender: TObject);
    procedure actAddUpdate(Sender: TObject);
    procedure actRemoveExecute(Sender: TObject);
    procedure actRemoveUpdate(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure lbAvailableDblClick(Sender: TObject);
    procedure lbSelectedDblClick(Sender: TObject);
    procedure lbKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FOnChange: TNotifyEvent;
  public
    property OnChange: TNotifyEvent read FOnChange write FOnChange;

    procedure Setup(all: TStrings; selected: TStringSet; itemName: string);
    procedure ClearAndDisable;

    function GetSelected: TStringSet;
    function HasSelection: boolean;
  end;

implementation

uses
  gdycommon;
{$R *.dfm}

procedure MoveItems(lbSrc, lbDest: TListBox);
var
  i: integer;
begin
  i := 0;
  while i < lbSrc.Count do
    if lbSrc.Selected[i] then
    begin
      lbDest.Items.AddObject(lbSrc.Items[i], lbSrc.Items.Objects[i]);
      lbSrc.Items.Delete(i);
    end
    else
      i := i + 1;
end;

procedure TMultipleChoiceFrm.actAddExecute(Sender: TObject);
begin
  MoveItems(lbAvailable, lbSelected);
  if assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TMultipleChoiceFrm.actAddUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := lbAvailable.SelCount > 0;
end;

procedure TMultipleChoiceFrm.actRemoveExecute(Sender: TObject);
begin
  MoveItems(lbSelected, lbAvailable);
  if assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TMultipleChoiceFrm.actRemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := lbSelected.SelCount > 0;
end;

function TMultipleChoiceFrm.GetSelected: TStringSet;
var
  i: integer;
begin
  for i := 0 to lbSelected.Count-1 do
    SetInclude(Result, trim(lbSelected.Items[i]));
end;

procedure TMultipleChoiceFrm.Setup(all: TStrings; selected: TStringSet; itemName: string);
var
  i: integer;
begin
  lbAvailable.Items.Clear;
  lbSelected.Items.Clear;
  pnlAvailableHeader.Caption := Format('Available %s', [Plural(itemName)]);
  pnlSelectedHeader.Caption := Format('Selected %s', [Plural(itemName)]);
  EnableControl(lbAvailable, true);
  EnableControl(lbSelected, true);
  for i := 0 to all.Count-1 do
    if InSet(trim(all[i]), selected) then
      lbSelected.Items.AddObject(all[i], all.Objects[i])
    else
      lbAvailable.Items.AddObject(all[i], all.Objects[i]);
  if lbAvailable.Items.Count > 0 then
    lbAvailable.ItemIndex := 0;
  if lbSelected.Items.Count > 0 then
    lbSelected.ItemIndex := 0;
end;

procedure TMultipleChoiceFrm.ClearAndDisable;
begin
  lbAvailable.Items.Clear;
  lbSelected.Items.Clear;
  pnlAvailableHeader.Caption := '';
  pnlSelectedHeader.Caption := '';
  EnableControl(lbAvailable, false);
  EnableControl(lbSelected, false);
end;

procedure TMultipleChoiceFrm.FrameResize(Sender: TObject);
begin
  pnlAvailable.Width := (Width - pnlButtons.Width) div 2;
end;

function TMultipleChoiceFrm.HasSelection: boolean;
begin
  Result := lbSelected.Count > 0;
end;

procedure TMultipleChoiceFrm.lbAvailableDblClick(Sender: TObject);
begin
  MoveItems(lbAvailable, lbSelected);
  if assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TMultipleChoiceFrm.lbSelectedDblClick(Sender: TObject);
begin
  MoveItems(lbSelected, lbAvailable);
  if assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TMultipleChoiceFrm.lbKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Char(Key) = 'A') and (Shift = [ssCtrl]) then
    (Sender as TListBox).SelectAll;
end;

end.
