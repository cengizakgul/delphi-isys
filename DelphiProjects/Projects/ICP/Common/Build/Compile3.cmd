@ECHO OFF

IF "%1" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. App name (like EvoXYZ)
ECHO    2. Version(optional)
ECHO    3. Flags: (R)elease or (D)ebug (optional)
ECHO    Current folder must be app folder (like DelphiProjects\Projects\ICP\EvoXYZ)
GOTO very_end

:start

SET pAppName=%1
SET pAppVersion=%2
IF "%pAppVersion%" == "" GOTO compile

IF "%3" == "R" SET pCompileOptions= -DFINAL_RELEASE

:compile
ECHO *Cleaning Bin
FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.*) DO DEL /F /Q /S ..\..\..\Bin\ICP\%pAppName%\*.* > nul

call ..\Common\Build\CompileOpenCommon.cmd %pAppName%

ECHO *Compiling
..\..\..\Common\OpenProject\isOpenProject.exe Compile.ops %pAppVersion% "%pCompileOptions%"

IF ERRORLEVEL 1 GOTO error

ECHO *Patching binaries
FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.exe) DO ..\..\..\Common\External\Utils\StripReloc.exe /B %%a > NUL

REM XCOPY /Y /Q .\Source\*.mes ..\..\..\Bin\ICP\%pAppName%\*.* > NUL
FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\%pAppName%\*.mes

FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.exe) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL
REM FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.dll) DO ..\..\..\Common\External\MadExcept\madExceptPatch.exe %%a > NUL

REM FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.mes) DO DEL /F /Q ..\..\..\Bin\ICP\%pAppName%\*.mes
FOR %%a IN (..\..\..\Bin\ICP\%pAppName%\*.map) DO DEL /F /Q ..\..\..\Bin\ICP\%pAppName%\*.map

GOTO end

:error
IF "%pAppVersion%" == "" PAUSE
EXIT 1

:end
IF "%pAppVersion%" == "" PAUSE

:very_end