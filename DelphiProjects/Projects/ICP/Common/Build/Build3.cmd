@ECHO OFF

IF "%1" NEQ "" GOTO start
IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. Application Name (like EvoXYZ)
ECHO    2. Application Version
ECHO    3. DevTools folder (optional)
ECHO    Current folder must be app folder (like DelphiProjects\Projects\ICP\EvoXYZ)
GOTO end

:start

SET pAppName=%1
SET pAppVersion=%2
IF "%pAppVersion%" NEQ ""  SET pAppVersion=%pAppVersion:"=%
SET pDevToolsDir=%3
IF "%pDevToolsDir%" NEQ "" SET pDevToolsDir=%pDevToolsDir:"=%

CALL ..\Common\Build\Compile3.cmd %pAppName% "%pAppVersion%" R

IF "%pDevToolsDir%" == "" GOTO end

ECHO *Building release installer
CALL "%pDevToolsDir%"\InnoSetup\ISCC.exe  /q /dNAME_SUFFIX=Setup .\Setup\setup.iss

CALL ..\Common\Build\Compile3.cmd %pAppName% "%pAppVersion%" 

ECHO *Building debug installer
CALL "%pDevToolsDir%"\InnoSetup\ISCC.exe  /q /dNAME_SUFFIX=Setup-WithDebugLog .\Setup\setup.iss

:end
