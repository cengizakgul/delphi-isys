@ECHO OFF

IF "%1" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. App name (like EvoXYZ)
ECHO    Current folder must be app folder (like DelphiProjects\Projects\ICP\EvoXYZ)
GOTO end

:start

SET pAppName=%1

ECHO *Cleaning Tmp
FOR %%a IN (..\..\..\Tmp\*.*) DO DEL /F /Q ..\..\..\Tmp\*.*

ECHO *Copying stuff to Bin
mkdir ..\..\..\Bin\ICP\%pAppName%

REM OpenSSL
copy /Y /B "..\..\..\Common\External\OpenSSL\libeay32.dll" "..\..\..\Bin\ICP\%pAppName%" >nul
copy /Y /B "..\..\..\Common\External\OpenSSL\ssleay32.dll" "..\..\..\Bin\ICP\%pAppName%" >nul
copy /Y /B "..\..\..\Common\External\Misc\msvcr90.dll" "..\..\..\Bin\ICP\%pAppName%" >nul
copy /Y /B "..\..\..\Common\External\Misc\Microsoft.VC90.CRT.manifest" "..\..\..\Bin\ICP\%pAppName%" >nul
copy /Y /B "..\..\..\Common\External\7Zip\New\7za.dll" "..\..\..\Bin\ICP\%pAppName%" >nul

REM Local stuff
copy /Y /B ".\Resources\*.red" "..\..\..\Bin\ICP\%pAppName%" 


:end
