unit EvoAPIClientNewMainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  common, EvoAPIConnectionParamFrame, evodata,
  EvolutionCompanySelectorFrame, 
  EvolutionPrPrBatchFrame, OptionsBaseFrame, SchedulerFrame,
  SmtpConfigFrame;

type
  TEvoAPIClientNewMainFm = class(TEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    pnlBottom: TPanel;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    pnlCompany: TPanel;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    tbshCompanySettings: TTabSheet;
    CompanyFrame: TEvolutionCompanySelectorFrm;
    tbshScheduler: TTabSheet;
    tbshSchedulerSettings: TTabSheet;
    SchedulerFrame: TSchedulerFrm;
    SmtpConfigFrame: TSmtpConfigFrm;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
  private
    procedure HandleSmtpConfigChange(Sender: TObject);
    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
  protected
    FEvoData: TEvoData;
  protected
    procedure UnInitPerCompanySettings; virtual; abstract;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef); virtual; abstract;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}
uses
  isSettings, EeUtilityLoggerViewFrame, dialogs;

procedure TEvoAPIClientNewMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TEvoAPIClientNewMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      FEvoData.Connect(EvoFrame.Param);
      ConnectToEvo.Caption := 'Refresh data';
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

constructor TEvoAPIClientNewMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEeUtilityLoggerViewFrm;
  inherited;
  ConnectToEvo.Caption := 'Get data';

  FEvoData := TEvoData.Create(Logger);
  CompanyFrame.Init(FEvoData);

  UnInitPerCompanySettings;
  FEvoData.Advise(HandleCompanyChanged);

  SmtpConfigFrame.Config := LoadSmtpConfig(FSettings, '');
  SmtpConfigFrame.OnChangeByUser := HandleSmtpConfigChange;
end;

destructor TEvoAPIClientNewMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

procedure TEvoAPIClientNewMainFm.HandleSmtpConfigChange(Sender: TObject);
begin
  SaveSmtpConfig(SmtpConfigFrame.Config, FSettings, '');
end;

procedure TEvoAPIClientNewMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UnInitPerCompanySettings;
  Repaint;
  if new.HasValue then
  begin
    LoadPerCompanySettings(new.Value);
    Repaint;
  end
end;

end.
