inherited DBDTFilterFrm: TDBDTFilterFrm
  Width = 358
  Height = 414
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 358
    Height = 414
    Align = alClient
    Caption = 'D/B/D/T'
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 354
      Height = 34
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 118
        Top = 3
        Width = 70
        Height = 13
        Caption = 'D/B/D/T level'
      end
      object cbLevel: TComboBox
        Left = 200
        Top = 0
        Width = 97
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnSelect = cbLevelSelect
      end
      object cbUse: TCheckBox
        Left = 6
        Top = 2
        Width = 75
        Height = 17
        Caption = 'Filter'
        TabOrder = 1
        OnClick = cbUseClick
      end
    end
    object dgDBDT: TReDBCheckGrid
      Left = 2
      Top = 49
      Width = 354
      Height = 363
      DisableThemesInTitle = False
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsDBDT
      MultiSelectOptions = [msoShiftSelect]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect]
      ReadOnly = True
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
    end
  end
  object dsDBDT: TDataSource
    Left = 192
    Top = 136
  end
end
