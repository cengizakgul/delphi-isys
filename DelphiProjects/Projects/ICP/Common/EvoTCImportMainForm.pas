unit EvoTCImportMainForm;

interface

uses
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ActnList, EvoAPIClientMainForm,
  Classes, Controls, Forms, SysUtils, evoapiconnection,
  EvolutionSinglePayrollFrame, common, EvoAPIConnectionParamFrame,
  OptionsBaseFrame;

type
  TEvoTCImportMainFm = class(TEvoAPIClientMainFm)
    RunEmployeeExport: TAction;
    pnlTop: TPanel;
    BitBtn4: TBitBtn;
    EvolutionSinglePayrollFrame: TEvolutionSinglePayrollFrm;
    pnlBottom: TPanel;
    Splitter1: TSplitter;
    ConnectToEvo: TAction;
    RunAction: TAction;
    ActionList1: TActionList;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
  private
    FTCAppName: string;
    procedure SetTCAppName(const Value: string);
  protected
    property TCAppName: string read FTCAppName write SetTCAppName;
  	function CanConnectToTCApp: boolean; virtual; abstract;
  public
    constructor Create(Owner: TComponent); override;
  end;

implementation

{$R *.dfm}
uses
  isSettings, gdyGlobalWaitIndicator, EeUtilityLoggerViewFrame;

procedure TEvoTCImportMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and CanConnectToTCApp;
end;

procedure TEvoTCImportMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  WaitIndicator.StartWait('Getting payroll list from Evolution');
  try
    EvolutionSinglePayrollFrame.FillTables(CreateEvoAPIConnection(EvoFrame.Param, Logger));
  finally
    WaitIndicator.EndWait;
  end;
  ConnectToEvo.Caption := Format('Refresh Evolution and %s data', [TCAppName]);
end;

procedure TEvoTCImportMainFm.SetTCAppName(const Value: string);
begin
  FTCAppName := Value;
  ConnectToEvo.Caption := Format('Get Evolution and %s data', [TCAppName]);
end;

constructor TEvoTCImportMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEeUtilityLoggerViewFrm;
  inherited;
end;

end.
