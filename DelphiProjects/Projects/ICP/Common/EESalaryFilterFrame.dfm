inherited EESalaryFilterFrm: TEESalaryFilterFrm
  Width = 173
  Height = 62
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 173
    Height = 62
    Align = alClient
    Caption = 'Salary/hourly'
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 169
      Height = 26
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object cbUse: TCheckBox
        Left = 7
        Top = 0
        Width = 145
        Height = 17
        Caption = 'Filter'
        TabOrder = 0
        OnClick = cbUseClick
      end
    end
    object rbSalary: TRadioButton
      Left = 8
      Top = 40
      Width = 65
      Height = 17
      Caption = 'Salary'
      TabOrder = 1
      OnClick = rbClick
    end
    object rbHourly: TRadioButton
      Left = 72
      Top = 40
      Width = 113
      Height = 17
      Caption = 'Hourly'
      TabOrder = 2
      OnClick = rbClick
    end
  end
end
