unit EvolutionSinglePayrollFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, EvolutionDSFrame, ExtCtrls, EvolutionCompanyFrame, kbmMemTable,
  evoapiconnection, gdycommonlogger, common;

type
  TEvolutionSinglePayrollFrm = class(TFrame)
    EvoCompany: TEvolutionCompanyFrm;
    Splitter1: TSplitter;
    frmPR: TEvolutionDsFrm;
  private
    FPR: TkbmCustomMemTable;
    function GetLogger: ICommonLogger;
    procedure SetLogger(const Value: ICommonLogger);
  public
    procedure FillTables(conn: IEvoAPIConnection);
    procedure ClearTables;
    function CanGetPayrollDef: boolean;
    function GetPayrollDef: TEvoPayrollDef;

    property Logger: ICommonLogger read GetLogger write SetLogger;
  end;

implementation

uses
  gdyRedir;

{$R *.dfm}

{ TEvolutionSinglePayrollFrm }

function TEvolutionSinglePayrollFrm.CanGetPayrollDef: boolean;
begin
  Result := EvoCompany.CanGetClCo and assigned(FPR) and not FPR.Eof;
end;

procedure TEvolutionSinglePayrollFrm.ClearTables;
begin
  FreeAndNil(FPR);
  EvoCompany.ClearTables;
end;

procedure TEvolutionSinglePayrollFrm.FillTables(conn: IEvoAPIConnection);
begin
  ClearTables;
  EvoCompany.FillTables(conn);          

  FPR := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'qPrList.rwq');

  FPR.FieldByName('CHECK_DATE').DisplayLabel := 'Check Date';
  FPR.FieldByName('RUN_NUMBER').DisplayLabel := 'Run Number';
  FPR.FieldByName('PAYROLL_TYPE').DisplayLabel := 'Type';
  FPR.FieldByName('STATUS').DisplayLabel := 'Status';
  FPR.FieldByName('PR_NBR').DisplayLabel := 'Internal Pr#';
  FPR.FieldByName('CL_NBR').Visible := false;
  FPR.FieldByName('CO_NBR').Visible := false;

  frmPR.ds.DataSet := FPR;

  FPR.MasterFields := 'CL_NBR;CO_NBR';
  FPR.DetailFields := 'CL_NBR;CO_NBR';
  FPR.MasterSource := EvoCompany.frmCO.ds;
  
  FPR.First;
end;

function TEvolutionSinglePayrollFrm.GetLogger: ICommonLogger;
begin
  Result := EvoCompany.Logger;
end;

function TEvolutionSinglePayrollFrm.GetPayrollDef: TEvoPayrollDef;
begin
  Result.Company := EvoCompany.GetClCo;
  Result.Pr.PrNbr := FPR['PR_NBR'];
  Result.Pr.Run := FPR['RUN_NUMBER'];
  Result.Pr.CheckDate := FPR.FieldByName('CHECK_DATE').AsDateTime;
end;

procedure TEvolutionSinglePayrollFrm.SetLogger(const Value: ICommonLogger);
begin
  EvoCompany.Logger := Value;
end;

end.
