unit comboChoices;

interface

uses
  db, evoapiconnection, evoapiconnectionutils;

var
  EE_TerminationCode_ComboChoices: string;
  PositionStatus_ComboChoices: string;
  Ethnicity_ComboChoices: string;
  PayrollStatus_ComboChoices: string;

procedure InitComboChoices(conn: IEvoAPIConnection);

implementation

uses
  sysutils, gdyRedir, common, evconsts;


function GetComboChoices(codes: TDataSet; tableName, fieldname: string): string;
begin
  Result := '';
  codes.Filter := Format('TABLE_NAME=''%s'' and FIELD_NAME=''%s''', [tableName, fieldname]);
  codes.Filtered := true;
  try
    codes.First;
    while not codes.Eof do
    begin
      if Result <> '' then
        Result := Result + #13;
      Result := Result + codes['NAME'] + #9 + codes['CODE'];
      codes.Next;
    end;
  finally
    codes.Filtered := false;
  end;
end;

procedure InitComboChoices(conn: IEvoAPIConnection);
var
  V_FIELD_CONSTANTS: TDataSet;
begin
  if not FileExists(Redirection.GetDirectory(sQueryDirAlias) + 'V_FIELD_CONSTANTS.rwq') then
    Exit;

  V_FIELD_CONSTANTS := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'V_FIELD_CONSTANTS.rwq');
  try
    EE_TerminationCode_ComboChoices := GetComboChoices(V_FIELD_CONSTANTS, 'EE', 'CURRENT_TERMINATION_CODE');
    PositionStatus_ComboChoices := GetComboChoices(V_FIELD_CONSTANTS, 'EE', 'POSITION_STATUS');
    Ethnicity_ComboChoices := GetComboChoices(V_FIELD_CONSTANTS, 'CL_PERSON', 'ETHNICITY');
    PayrollStatus_ComboChoices := GetComboChoices(V_FIELD_CONSTANTS, 'PR', 'STATUS');
  finally
    FreeAndNil(V_FIELD_CONSTANTS);
  end;
end;


initialization

//need them before we have connection to Evo (describing scheduled task that has ee filter among its parameters)  
  EE_TerminationCode_ComboChoices :=
    'Active' + #9 + EE_TERM_ACTIVE + #13 +
    'Involuntary Layoff' + #9 + EE_TERM_INV_LAYOFF + #13 +
    'Involuntary Termination Due to Misconduct' + #9 + EE_TERM_INV_MISCONDUCT + #13 +
    'Termination Due to Layoff' + #9 + EE_TERM_LAYOFF + #13 +
    'Release Without Prejudice' + #9 + EE_TERM_RELEASE + #13 +
    'Voluntary Resignation' + #9 + EE_TERM_RESIGN + #13 +
    'Termination Due to Retirement' + #9 + EE_TERM_RETIREMENT + #13 +
    'Termination Due to Transfer' + #9 + EE_TERM_TRANSFER + #13 +
    'Military Leave' + #9 + EE_TERM_MILITARY_LEAVE + #13 +
    'Leave of Absence' + #9 + EE_TERM_LEAVE + #13 +
    'Seasonal' + #9 + EE_TERM_SEASONAL + #13 +
    'Termination Due to Death' + #9 + EE_TERM_DEATH + #13 +
    'Suspended' + #9 + EE_TERM_SUSPENDED + #13 +
    'Terminated' + #9 + EE_TERM_TERMINATED + #13 +
    'FMLA' + #9 + EE_TERM_FMLA;

  PositionStatus_ComboChoices :=
    'N/A' + #9 + POSITION_STATUS_NA + #13 +
    'Full Time' + #9 + POSITION_STATUS_FULL + #13 +
    'Full Time Temp' + #9 + POSITION_STATUS_FULL_TEMP + #13 +
    'Part Time' + #9 + POSITION_STATUS_PART + #13 +
    'Part Time temp' + #9 + POSITION_STATUS_PART_TEMP + #13 +
    'Half Time' + #9 + POSITION_STATUS_HALF_TIME + #13 +
    'Seasonal' + #9 + POSITION_STATUS_SEASONAL + #13 +
    'Student' + #9 + POSITION_STATUS_STUDENT + #13 +
    '1099' + #9 + POSITION_STATUS_1099;

  Ethnicity_ComboChoices :=
    'Asian' + #9 + ETHNIC_ASIAN + #13 +
    'American Indian or Alaska Native' + #9 + ETHNIC_INDIAN + #13 +
    'Black or African American' + #9 + ETHNIC_AFRICAN + #13 +
    'White' + #9 + ETHNIC_CAUCASIAN + #13 +
    'Hispanic or Latino' + #9 + ETHNIC_HISPANIC + #13 +
    'Native Hawaiian or other Pacific Islander' + #9 + ETHNIC_PACIFIC + #13 +
    'Two or More Races' + #9 + ETHNIC_MANY + #13 +
    'Other' + #9 + ETHNIC_OTHER + #13 +
    'Not Applicable' + #9 + ETHNIC_NOT_APPLICATIVE;

  PayrollStatus_ComboChoices :=
    'Pending' + #9 + PAYROLL_STATUS_PENDING + #13 +
    'Processed' + #9 + PAYROLL_STATUS_PROCESSED + #13 +
    'Completed' + #9 + PAYROLL_STATUS_COMPLETED + #13 +
    'Voided' + #9 + PAYROLL_STATUS_VOIDED + #13 +
    'Deleted' + #9 + PAYROLL_STATUS_DELETED + #13 +
    'Hold' + #9 + PAYROLL_STATUS_HOLD + #13 +
    'SB Review' + #9 + PAYROLL_STATUS_SB_REVIEW + #13 +
    'Backdated' + #9 + PAYROLL_STATUS_BACKDATED + #13 +
    'Required Mgr. Approval' + #9 + PAYROLL_STATUS_REQUIRED_MGR_APPROVAL + #13 +
    'Processing' + #9 + PAYROLL_STATUS_PROCESSING + #13 +
    'Pre-Processing' + #9 + PAYROLL_STATUS_PREPROCESSING;


end.
