unit SchedulerFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, scheduler, scheduledTask, gdyCommonLogger, DB, Grids, Wwdbigrd,
  Wwdbgrid, dbcomp, ActnList, ExtCtrls, StdCtrls, Buttons, ImgList,
  ComCtrls, DBCtrls;

type
  TEditParametersEvent = procedure(task: IScheduledTask) of object;
  TCanEditParametersEvent = procedure(var can: boolean) of object;
  TSaveAllSettingsEvent = procedure of object;

  TSchedulerFrm = class(TFrame)
    dsTasks: TDataSource;
    pnlTasksControl: TPanel;
    ActionList1: TActionList;
    actRefresh: TAction;
    BitBtn1: TBitBtn;
    actDelete: TAction;
    ImageList1: TImageList;
    actSchedule: TAction;
    actRunNow: TAction;
    dsResults: TDataSource;
    actShowLog: TAction;
    actSendDebugLog: TAction;
    actEditParams: TAction;
    PageControl2: TPageControl;
    tbshTasks: TTabSheet;
    tbshAllResults: TTabSheet;
    pnlTasks: TPanel;
    dgTasks: TReDBGrid;
    Panel1: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    pnlDetails: TPanel;
    pcTaskDetails: TPageControl;
    tbshParameters: TTabSheet;
    Panel3: TPanel;
    BitBtn7: TBitBtn;
    DBMemo1: TDBMemo;
    Panel4: TPanel;
    Panel5: TPanel;
    DBText1: TDBText;
    Label1: TLabel;
    Panel6: TPanel;
    DBText2: TDBText;
    Label2: TLabel;
    Panel7: TPanel;
    DBText3: TDBText;
    Label3: TLabel;
    Panel8: TPanel;
    DBText4: TDBText;
    Label4: TLabel;
    tbshResults: TTabSheet;
    Panel2: TPanel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    dgResults: TReDBGrid;
    Splitter1: TSplitter;
    Panel9: TPanel;
    dgAllResults: TReDBGrid;
    dsAllResults: TDataSource;
    actShowLog2: TAction;
    actSendDebugLog2: TAction;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    actGotoTask: TAction;
    BitBtn10: TBitBtn;
    actOpenFolder: TAction;
    BitBtn11: TBitBtn;
    actOpenFolder2: TAction;
    BitBtn12: TBitBtn;
    actEnableSelected: TAction;
    BitBtn13: TBitBtn;
    actDisableSelected: TAction;
    actEnableAll: TAction;
    actDisableAll: TAction;
    BitBtn14: TBitBtn;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    actRunAs: TAction;
    BitBtn17: TBitBtn;
    procedure actRefreshExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actRefreshUpdate(Sender: TObject);
    procedure SingleTaskActionUpdate(Sender: TObject);
    procedure actScheduleExecute(Sender: TObject);
    procedure actRunNowExecute(Sender: TObject);
    procedure ResultsActionUpdate(Sender: TObject);
    procedure actShowLogExecute(Sender: TObject);
    procedure actSendDebugLogExecute(Sender: TObject);
    procedure actEditParamsExecute(Sender: TObject);
    procedure actEditParamsUpdate(Sender: TObject);
    procedure AllResultsActionUpdate(Sender: TObject);
    procedure actShowLog2Execute(Sender: TObject);
    procedure actSendDebugLog2Execute(Sender: TObject);
    procedure actGotoTaskExecute(Sender: TObject);
    procedure dgTasksDblClick(Sender: TObject);
    procedure dgAllResultsDblClick(Sender: TObject);
    procedure dgResultsDblClick(Sender: TObject);
    procedure actOpenFolderExecute(Sender: TObject);
    procedure actOpenFolder2Execute(Sender: TObject);
    procedure MultipleTaskActionUpdate(Sender: TObject);
    procedure actEnableSelectedExecute(Sender: TObject);
    procedure actDisableSelectedExecute(Sender: TObject);
    procedure actEnableAllExecute(Sender: TObject);
    procedure actDisableAllExecute(Sender: TObject);
    procedure actRunAsExecute(Sender: TObject);
  private
    FScheduler: TScheduler;
    FLogger: ICommonLogger;
    FOnEditParameters: TEditParametersEvent;
    FReasonWhyWeDontHaveScheduler: string;

    procedure ShowUserLog(ds: TDataSet);
    procedure SendDebugLog(ds: TDataSet);
    procedure OpenFolderWithDebugLog(ds: TDataSet);
    function DisableTask(ds: TDataSet): boolean;
    function EnableTask(ds: TDataSet): boolean;
  public
    OnSaveAllSettings: TSaveAllSettingsEvent;
    OnCanEditParameters: TCanEditParametersEvent;
    constructor Create( Owner: TComponent ); override;
    procedure AddTask(task: IScheduledTask);
    procedure Configure(logger: ICommonLogger; ta: ITaskAdapter; EditParametersHandler: TEditParametersEvent);
  end;

implementation

{$R *.dfm}

uses
  common, gdyUtils, gdyMail;

{ TSchedulerFrm }

procedure TSchedulerFrm.AddTask(task: IScheduledTask);
begin
  if FScheduler = nil then
    raise Exception.CreateFmt('Scheduler is not initialized because an error happened. The error was: %s',[FReasonWhyWeDontHaveScheduler]);
  if assigned(OnSaveAllSettings) then
    OnSaveAllSettings;
  FScheduler.AddTask(task);
  FScheduler.ShowScheduleForCurrentTask;
end;

procedure TSchedulerFrm.Configure(logger: ICommonLogger; ta: ITaskAdapter; EditParametersHandler: TEditParametersEvent);
begin
  FLogger := logger;
  FOnEditParameters := EditParametersHandler;

  dgTasks.Selected.Clear;
  AddSelected(dgTasks.Selected, 'NAME', 20, 'Name', true);
  AddSelected(dgTasks.Selected, 'CUSTOM_COMPANY_NUMBER', 12, 'Company code', true);
  AddSelected(dgTasks.Selected, 'COMPANY_NAME', 30, 'Company name', true);
  AddSelected(dgTasks.Selected, 'CUSTOM_CLIENT_NUMBER', 12, 'Client code', true);
  AddSelected(dgTasks.Selected, 'CLIENT_NAME', 30, 'Client name', true);
  AddSelected(dgTasks.Selected, 'LAST_RUN_TIME', 20, 'Last run time', true);
  AddSelected(dgTasks.Selected, 'LAST_RUN_STATUS', 20, 'Last run status', true);
  AddSelected(dgTasks.Selected, 'ENABLED', 1, 'Enabled', true);

  dgResults.Selected.Clear;
  AddSelected(dgResults.Selected, 'WHEN', 20, 'Run time', true);
  AddSelected(dgResults.Selected, 'STATUS', 20, 'Status', true);
  AddSelected(dgResults.Selected, 'MESSAGE', 30, 'Info', true);

  dgAllResults.Selected.Clear;
  AddSelected(dgAllResults.Selected, 'NAME', 20, 'Name', true);
  AddSelected(dgAllResults.Selected, 'WHEN', 20, 'Run time', true);
  AddSelected(dgAllResults.Selected, 'STATUS', 20, 'Status', true);
  AddSelected(dgAllResults.Selected, 'CUSTOM_COMPANY_NUMBER', 12, 'Company code', true);
  AddSelected(dgAllResults.Selected, 'COMPANY_NAME', 30, 'Company name', true);
  AddSelected(dgAllResults.Selected, 'CUSTOM_CLIENT_NUMBER', 12, 'Client code', true);
  AddSelected(dgAllResults.Selected, 'CLIENT_NAME', 30, 'Client name', true);
  AddSelected(dgAllResults.Selected, 'MESSAGE', 30, 'Info', true);

  try
    FScheduler := TScheduler.Create(Self, logger, ta);
  except
    on E:Exception do
    begin
      FReasonWhyWeDontHaveScheduler := E.Message;
      raise;
    end
  end;
  dsTasks.DataSet := FScheduler.TasksDataSet;
  dsResults.DataSet := FScheduler.ResultsDataSet;
  dsAllResults.DataSet := FScheduler.AllResultsDataSet;
end;

procedure TSchedulerFrm.actRefreshExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      FScheduler.Refresh;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.SingleTaskActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0)
       and ((dgTasks.SelectedList = nil) or (dgTasks.SelectedList.Count <= 1));
end;

procedure TSchedulerFrm.actDeleteExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      if not FScheduler.IsCurrentTaskRunning  then
      begin
        if MessageDlg('Delete task?', mtConfirmation, [mbYes, mbNo], 0{, [mbNo]}) = mrYes then
          FScheduler.DeleteCurrentTask;
      end
      else
        ShowMessage('Cannot delete task while it is running');
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actRefreshUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler);
end;

procedure TSchedulerFrm.actScheduleExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      FScheduler.ShowScheduleForCurrentTask;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actRunNowExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      if not FScheduler.IsCurrentTaskRunning  then
      begin
        if MessageDlg('Run task?', mtConfirmation, [mbYes, mbNo], 0{, [mbNo]}) = mrYes then
        begin
          if assigned(OnSaveAllSettings) then
            OnSaveAllSettings;
          FScheduler.RunCurrentTask;
          ShowMessage('The task is running now. Press the "Refresh" button a minute later to see the result');
        end
      end
      else
        ShowMessage('Task is already running now')
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.ResultsActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsResults.DataSet) and (dsResults.DataSet.RecordCount > 0);
end;

procedure TSchedulerFrm.actShowLogExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in task''s results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      ShowUserLog( dsResults.DataSet );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actSendDebugLogExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in task''s results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      SendDebugLog(dsResults.Dataset);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actEditParamsExecute(Sender: TObject);
var
  task: IScheduledTask;
begin
  Assert(assigned(FOnEditParameters));

  task := LoadTask(FScheduler.TasksDataSet['GUID']);
  FOnEditParameters(task);
  FScheduler.TaskParametersEdited(task);
end;

procedure TSchedulerFrm.actEditParamsUpdate(Sender: TObject);
var
  can: boolean;
begin
  can := true;
  if assigned(OnCanEditParameters) then
    OnCanEditParameters(can);

  (Sender as TCustomAction).Enabled := can and
      assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0);
end;

procedure TSchedulerFrm.AllResultsActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsAllResults.DataSet) and (dsAllResults.DataSet.RecordCount > 0);
end;

procedure TSchedulerFrm.actShowLog2Execute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item of all results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      ShowUserLog( dsAllResults.DataSet );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actSendDebugLog2Execute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item of all results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      SendDebugLog(dsAllResults.DataSet);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

constructor TSchedulerFrm.Create(Owner: TComponent);
begin
  inherited;
  PageControl2.ActivePage := tbshTasks;
  pcTaskDetails.ActivePage := tbshParameters;
end;

procedure TSchedulerFrm.ShowUserLog(ds: TDataSet);
var
  fn: string;
begin
  fn := LoadTask(ds['GUID']).GetTaskResultFiles(ds['FOLDER']).UserHtmlLogFilename;
  if FileExists(fn) then //to not expose location to user
    OpenDoc(Application.MainForm, fn)
  else
    raise Exception.Create('Log file doesn''t exist');
end;

procedure TSchedulerFrm.SendDebugLog(ds: TDataSet);
var
  fn: string;
begin
  fn := LoadTask(ds['GUID']).GetTaskResultFiles(ds['FOLDER']).LogArchiveFileName;
  if FileExists(fn) then //to not expose location to user
    SimpleMailTo( GetISystemsEMail, 'Debug log', '', fn )
  else
    raise Exception.Create('Debug log file doesn''t exist');
end;

procedure TSchedulerFrm.OpenFolderWithDebugLog(ds: TDataSet);
var
  fn: string;
begin
  fn := LoadTask(ds['GUID']).GetTaskResultFiles(ds['FOLDER']).LogArchiveFileName;
  if FileExists(fn) then //to not expose location to user
    OpenInExplorer(fn)
  else
    raise Exception.Create('Debug log file doesn''t exist');
end;

procedure TSchedulerFrm.actGotoTaskExecute(Sender: TObject);
begin
  Assert(dsResults.DataSet <> nil);
  if not dsTasks.DataSet.Locate('GUID', dsAllResults.DataSet['GUID'], []) then
    Assert(false);
  if not dsResults.DataSet.Locate('FOLDER', dsAllResults.DataSet['FOLDER'], []) then
    Assert(false);
  PageControl2.ActivePage := tbshTasks;
  pcTaskDetails.ActivePage := tbshParameters;
end;

procedure TSchedulerFrm.dgTasksDblClick(Sender: TObject);
begin
  actSchedule.Execute;
end;

procedure TSchedulerFrm.dgAllResultsDblClick(Sender: TObject);
begin
  actShowLog2.Execute;
end;

procedure TSchedulerFrm.dgResultsDblClick(Sender: TObject);
begin
  actShowLog.Execute;
end;

procedure TSchedulerFrm.actOpenFolderExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in task''s results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      OpenFolderWithDebugLog(dsResults.DataSet);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actOpenFolder2Execute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s" for an item in all results grid', [(Sender as TCustomAction).Caption] ));
  try
    try
      OpenFolderWithDebugLog(dsAllResults.DataSet);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.MultipleTaskActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(FScheduler) and assigned(dsTasks.DataSet) and (dsTasks.DataSet.RecordCount > 0);
end;

function TSchedulerFrm.DisableTask(ds: TDataSet): boolean;
begin
  FScheduler.EnableCurrentTask(false);
  Result := true;
end;

function TSchedulerFrm.EnableTask(ds: TDataSet): boolean;
begin
  FScheduler.EnableCurrentTask(true);
  Result := true;
end;

procedure TSchedulerFrm.actEnableSelectedExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForSelected(dgTasks, EnableTask, [fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty]);
      finally
        FScheduler.Refresh;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actDisableSelectedExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForSelected(dgTasks, DisableTask, [fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty]);
      finally
        FScheduler.Refresh;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure ForAll(ds: TDataSet; handler: TOnSelected);
var
  savePos: TBookmark;
begin
  if assigned( ds ) then
  begin
    savePos := ds.GetBookmark;
    try
      try
        ds.DisableControls;
        try
          ds.First;
          while not ds.Eof do
          begin
            handler(ds);
            ds.Next;
          end;
        finally
          ds.EnableControls;
        end;
      finally
        if ds.BookmarkValid( savePos ) then
          ds.GotoBookmark( savePos );
      end;
    finally
      ds.FreeBookmark( savePos );
    end;
  end
end;

procedure TSchedulerFrm.actEnableAllExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForAll(dsTasks.DataSet, EnableTask);
      finally
        FScheduler.Refresh;
      end;
  except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actDisableAllExecute(Sender: TObject);
begin
  FLogger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption] ));
  try
    try
      try
        ForAll(dsTasks.DataSet, DisableTask);
      finally
        FScheduler.Refresh;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSchedulerFrm.actRunAsExecute(Sender: TObject);
var
  AForm: TForm;
  AUserEdit: TEdit;
  APassEdit: TEdit;
  ForAllEdit: TCheckBox;
  SavePos: TBookmark;

begin
  AForm := TForm.Create(Application);
  try
    AForm.Caption := 'Run as';
    AForm.Width := 300;
    AForm.Height := 200;
    AForm.Position := poMainFormCenter;

    with TLabel.Create(AForm) do
    begin
      Parent := AForm;
      SetBounds(16, 8, AForm.ClientWidth - 32, 13);
      Caption := 'Account';
    end;

    AUserEdit := TEdit.Create(AForm);
    AUserEdit.Parent := AForm;
    AUserEdit.SetBounds(16, 23, AForm.ClientWidth - 32, 21);
    AUserEdit.Text := FScheduler.CurrentTaskAccount;

    with TLabel.Create(AForm) do
    begin
      Parent := AForm;
      SetBounds(16, 52, AForm.ClientWidth - 32, 13);
      Caption := 'Password';
    end;

    APassEdit := TEdit.Create(AForm);
    APassEdit.Parent := AForm;
    APassEdit.PasswordChar := '*';
    APassEdit.SetBounds(16, 67, AForm.ClientWidth - 32, 21);

    ForAllEdit := TCheckBox.Create(AForm);
    ForAllEdit.Parent := AForm;
    ForAllEdit.SetBounds(16, 96, AForm.ClientWidth - 32, 21);
    ForAllEdit.Caption := 'Apply for all tasks';

    with TButton.Create(AForm) do
    begin
      Parent := AForm;
      SetBounds(32, 135, 110, 21);
      Caption := 'Ok';
      ModalResult := mrOk;
    end;

    with TButton.Create(AForm) do
    begin
      Parent := AForm;
      SetBounds(156, 135, 110, 21);
      Caption := 'Cancel';
      ModalResult := mrCancel;
    end;

    if AForm.ShowModal = mrOk then
    begin
      if ForAllEdit.Checked then
      begin
        savePos := dsTasks.DataSet.GetBookmark;
        try
          try
            dsTasks.DataSet.DisableControls;
            try
              dsTasks.DataSet.First;
              while not dsTasks.DataSet.Eof do
              begin
                FScheduler.CurrentTaskSetAccountInfo(AUserEdit.Text, APassEdit.Text);
                dsTasks.DataSet.Next;
              end;
            finally
              dsTasks.DataSet.EnableControls;
            end;
          finally
            if dsTasks.DataSet.BookmarkValid( savePos ) then
              dsTasks.DataSet.GotoBookmark( savePos );
          end;
        finally
          dsTasks.DataSet.FreeBookmark( savePos );
        end;
      end else
      begin
        FScheduler.CurrentTaskSetAccountInfo(AUserEdit.Text, APassEdit.Text);
      end;
    end;
  finally
    AForm.Free;
  end;
end;

end.


