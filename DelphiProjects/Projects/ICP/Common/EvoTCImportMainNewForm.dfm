inherited EvoTCImportMainNewFm: TEvoTCImportMainNewFm
  Left = 215
  Top = 170
  Caption = 'EvoTCImportMainNewFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 41
    Height = 456
    ActivePage = TabSheet2
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 784
        Align = alTop
        inherited GroupBox1: TGroupBox
          Width = 784
          inherited cbSavePassword: TCheckBox
            Width = 338
            Caption = 'Save password (mandatory for scheduling tasks)'
          end
        end
      end
    end
    object tbshCompanySettings: TTabSheet [1]
      Caption = 'Company settings'
      ImageIndex = 3
    end
    inherited TabSheet2: TTabSheet
      object pnlBottom: TPanel
        Left = 0
        Top = 387
        Width = 784
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
      end
      object pnlPayrollBatch: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 387
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        inline PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Left = 0
          Top = 0
          Width = 784
          Height = 387
          Align = alClient
          TabOrder = 0
          inherited Splitter1: TSplitter
            Height = 387
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 387
            inherited dgGrid: TReDBGrid
              Height = 362
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 465
            Height = 387
            inherited Panel1: TPanel
              Width = 465
            end
            inherited dgGrid: TReDBGrid
              Width = 465
              Height = 362
            end
          end
        end
      end
    end
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 177
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 8
        Top = 8
        Width = 153
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution and XXX data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 177
      Top = 0
      Width = 615
      Height = 41
      Align = alClient
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object RunAction: TAction
      Caption = 'Import timeclock data'
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution and XXX data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object RunEmployeeExport: TAction
      Caption = 'Update XYZ Employee Records for Selected Company'
    end
  end
end
