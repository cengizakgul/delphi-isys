unit FileOpenLeftBtnFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, StdCtrls, ActnList, StdActns, Buttons;

type
  TFileSelectedEvent = procedure (filename: string) of object;

  TFileOpenLeftBtnFrm = class(TFrame)
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    BitBtn1: TBitBtn;
    edtFile: TEdit;
    procedure FileOpen1Accept(Sender: TObject);
  private
    FOnFileSelected: TFileSelectedEvent;
    function GetFilename: string;
    procedure SetFilename(const Value: string);
  public
    function IsValid: boolean;
    property Filename: string read GetFilename write SetFilename;
    property OnFileSelected: TFileSelectedEvent read FOnFileSelected write FOnFileSelected;

  end;

implementation

{$R *.dfm}

{ TFileOpenLeftBtnFrm }

procedure TFileOpenLeftBtnFrm.FileOpen1Accept(Sender: TObject);
begin
  if assigned(FOnFileSelected) then
    FOnFileSelected((Sender as TFileOpen).Dialog.FileName);
  edtFile.Text := (Sender as TFileOpen).Dialog.FileName;
end;

function TFileOpenLeftBtnFrm.GetFilename: string;
begin
  Result := trim(edtFile.Text);
end;

function TFileOpenLeftBtnFrm.IsValid: boolean;
begin
  Result := Filename <> '';
end;

procedure TFileOpenLeftBtnFrm.SetFilename(const Value: string);
begin
  edtFile.Text := Value;
  FileOpen1.Dialog.FileName := Value;
end;

end.
