unit DirOpenFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ActnList, StdActns, Buttons, gdyCommonLogger;

type
  TDirChangedEvent = procedure (dirname: string) of object;

  TDirOpenFrm = class(TFrame)
    Panel1: TPanel;
    edtFile: TEdit;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
  private
    FOnDirChanged: TDirChangedEvent;
    function GetFolderName: string;
    procedure SetFolderName(const Value: string);
  public
    Logger: ICommonLogger;
    FolderRole: string;
    function IsValid: boolean;
    property FolderName: string read GetFolderName write SetFolderName;
    property OnDirChanged: TDirChangedEvent read FOnDirChanged write FOnDirChanged;
  end;

implementation

{$R *.dfm}
uses
  gdycommon;

{ TDirOpenFrm }

function TDirOpenFrm.GetFolderName: string;
begin
  Result := trim(edtFile.Text);
end;

function TDirOpenFrm.IsValid: boolean;
begin
  Result := FolderName <> '';
end;

procedure TDirOpenFrm.SetFolderName(const Value: string);
begin
  edtFile.Text := Value;
end;

procedure TDirOpenFrm.BitBtn1Click(Sender: TObject);
var
  dir: string;
begin
  Logger.LogEntry(Format('User clicked "Open Folder" button for "%s"',[Label1.Caption]));
  try
    try
      Logger.LogDebug('Current folder name is <' + edtFile.Text + '>');

      dir := edtFile.Text;
      if MySelectDirectory('Select folder', dir, '', Self, true) then
      begin
        edtFile.Text := dir;
        Logger.LogDebug('User selected folder <' + edtFile.Text + '>');
        if assigned(FOnDirChanged) then
          FOnDirChanged(dir);
      end
      else
        Logger.LogDebug('User didn''t select anyting');

    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TDirOpenFrm.Panel1Resize(Sender: TObject);
begin
  edtFile.Width := Panel1.Width - 16;
  edtFile.Left := 8;
end;

end.
