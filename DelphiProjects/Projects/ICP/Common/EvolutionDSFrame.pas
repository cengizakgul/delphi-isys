unit EvolutionDSFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ExtCtrls, Wwdbigrd, Wwdbgrid, dbcomp, evodata,
  common;

type
  TEvolutionDsFrm = class(TFrame)
    Panel1: TPanel;
    ds: TDataSource;
    dgGrid: TReDBGrid;
  public
    procedure Init(evoData: TEvoData; dsname: string);
  private
    FDSName: string;
    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure UpdateControls(evoData: TEvoData);
  end;

implementation

{$R *.dfm}

uses
  gdyCommon;

{ TEvolutionDsFrm }

procedure TEvolutionDsFrm.HandleCompanyChanged(evoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UpdateControls(evoData);
end;

procedure TEvolutionDsFrm.Init(evoData: TEvoData; dsname: string);
begin
  FDSName := dsname;
  evoData.Advise(HandleCompanyChanged);
  UpdateControls(evoData);
end;

procedure TEvolutionDsFrm.UpdateControls(evoData: TEvoData);
begin
  if evoData.Connected then
  begin
    if ds.DataSet <> evoData.DS[FDSName] then
      ds.DataSet := evoData.DS[FDSName];
  end
  else
  begin
    ds.DataSet := nil;
  end;
  EnableControl(dgGrid, evoData.Connected);
end;

end.
