unit EvoData;

interface

uses
  evoapiconnection, gdycommonlogger, kbmMemTable, timeclockimport, db, common,
  xmlrpctypes, evConsts, evoapiconnectionutils, classes, eefilter;

type
  TRateArray = array [0..3] of Double;

  TRateItemTWP = record
    rate: Double;
    dep: string;
    effectiveDate: TDateTime;
  end;
  TRateArrayTWP = array [0..9] of TRateItemTWP;

  TEeAdditionalField = record
    IsSetup: boolean;
    Value: variant;
  end;

  TEvoEEData = class
  private
    function GetStr(s: string): string;
    function IsSet(s: string): boolean;
  public
    constructor Create(conn: IEvoAPIConnection; logger: ICommonLogger; aCompany: TEvoCompanyDef);
    destructor Destroy; override;
    function GetEERatesArray: TRateArray;
    function GetEERatesArrayTWP: TRateArrayTWP;
    function GetPrimaryRate: Double;
    function GetEEAdditionalField(name: string): Variant;
    function GetEEAdditionalFieldVal(name: string): TEeAdditionalField; // looks to Info_Value first, returns a record with IsSetup flag that shows if the Additional field setup
    function GetEEDBDTCustomNumbers: string;
    function GetEETerminationDate: Variant;
    function LocateEE(code: string): boolean;
  public
    EEs: TkbmCustomMemTable;
    Company: TEvoCompanyDef;
  private
    FConn: IEvoAPIConnection;
    FLogger: ICommonLogger;
    FEEsDs: TDataSource;
    FEERates: TkbmCustomMemTable;
    FEEAdditionalFields: TkbmCustomMemTable;
    FEETermDates: TkbmCustomMemTable;
    function EERates: TkbmCustomMemTable;
    function EEAdditionalFields: TkbmCustomMemTable;
    function EETermDates: TkbmCustomMemTable;
  end;

  TEvoCDBDTMetadata = record
    Table: string;
    Name: string;
    CodeField: string;
    NameField: string;
    PrintField: string;
  end;

const
  //as CUSTOM_DBDT returns, Table is only used as a code for level
  CDBDTMetadata: array [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] of TEvoCDBDTMetadata =
  (
   (Table: 'CO'; Name: 'Company'; CodeField: 'CUSTOM_COMPANY_NUMBER'; NameField: 'COMPANY_NAME'; PrintField: ''),
   (Table: 'CO_DIVISION'; Name: 'Division'; CodeField: 'CUSTOM_DIVISION_NUMBER'; NameField: 'DIVISION_NAME'; PrintField: 'PRINT_DIVISION'),
   (Table: 'CO_BRANCH'; Name: 'Branch'; CodeField: 'CUSTOM_BRANCH_NUMBER'; NameField: 'BRANCH_NAME'; PrintField: 'PRINT_BRANCH'),
   (Table: 'CO_DEPARTMENT'; Name: 'Department'; CodeField: 'CUSTOM_DEPARTMENT_NUMBER'; NameField: 'DEPARTMENT_NAME'; PrintField: 'PRINT_DEPARTMENT'),
   (Table: 'CO_TEAM'; Name: 'Team'; CodeField: 'CUSTOM_TEAM_NUMBER'; NameField: 'TEAM_NAME'; PrintField: 'PRINT_TEAM')
  );

type
  TEvoDBDTs = class
  private
    FDataSets: array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of TkbmCustomMemTable;
    FDataSources: array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of TDataSource;
    FLogger: ICommonLogger;
    function GetDS(level: char): TkbmCustomMemTable;
    function GetDS_Int(level: integer): TkbmCustomMemTable;
  public
    constructor Create(conn: IEvoApiConnection; company: TEvoCompanyDef; logger: ICommonLogger);
    destructor Destroy; override;
    property DS[level: char]: TkbmCustomMemTable read GetDS; default;
    property CO_DIVISION: TkbmCustomMemTable index integer(CLIENT_LEVEL_DIVISION) read GetDS_Int;
    property CO_BRANCH: TkbmCustomMemTable index integer(CLIENT_LEVEL_BRANCH) read GetDS_Int;
    property CO_DEPARTMENT: TkbmCustomMemTable index integer(CLIENT_LEVEL_DEPT) read GetDS_Int;
    property CO_TEAM: TkbmCustomMemTable index integer(CLIENT_LEVEL_TEAM) read GetDS_Int;
  end;

  TEvoDSDesc = record
    Name: string;
    ClientDB: boolean;
    MasterName: string;
    LinkFields: string;
  end;

const
  TMP_PRDesc: TEvoDSDesc = (Name: 'TMP_PR'; ClientDb: false; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
  PR_BATCHDesc: TEvoDSDesc = (Name: 'PR_BATCH'; ClientDb: true; MasterName: 'TMP_PR'; LinkFields: 'CL_NBR;PR_NBR');
  CL_E_DSDesc: TEvoDSDesc = (Name: 'CL_E_DS'; ClientDb: true; MasterName: 'TMP_CL'; LinkFields: 'CL_NBR');
  CO_E_D_CODESDesc: TEvoDSDesc = (Name: 'CO_E_D_CODES'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
  CO_TIME_OFF_ACCRUALDesc: TEvoDSDesc = (Name: 'CO_TIME_OFF_ACCRUAL'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
  CO_JOBSDesc: TEvoDSDesc = (Name: 'CO_JOBS'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
  CUSTOM_DBDTDesc: TEvoDSDesc = (Name: 'CUSTOM_DBDT'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
  CO_STATESDesc: TEvoDSDesc = (Name: 'CO_STATES'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');
  CO_ADDITIONAL_INFO_NAMESDesc: TEvoDSDesc = (Name: 'CO_ADDITIONAL_INFO_NAMES'; ClientDb: true; MasterName: 'TMP_CO'; LinkFields: 'CL_NBR;CO_NBR');

type
  TEvoDS = record
    Desc: TEvoDSDesc;
    DataSet: TkbmCustomMemTable;
    DataSource: TDataSource;
  end;

  TEvoData = class;
  TCompanyChangedEvent = procedure(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef) of object;

  TEvoData = class(TComponent)
  private
    function GetDS(name: string): TkbmCustomMemTable;
    function GetDataSource(name: string): TDataSource;
  public
    constructor Create(logger: ICommonLogger); reintroduce;
    destructor Destroy; override;

    procedure AddDS(desc: TEvoDSDesc);

    procedure Connect(param: TEvoAPIConnectionParam);
    procedure Disconnect;

    function Connected: boolean;

    function CanGetClCo: boolean;
    function GetClCo: TEvoCompanyDef;
    procedure SetClCo( company: TEvoCompanyDef);

    function CanGetPrBatch: boolean;
    function GetPrBatch: TEvoPayrollBatchDef;

    function CanGetPr: boolean;
    function GetPr: TEvoPayrollDef;

    procedure Advise(ev: TCompanyChangedEvent);
    procedure Unadvise(ev: TCompanyChangedEvent);

    function CreateEEData: TEvoEEData;
    function Connection: IEvoAPIConnection;

    procedure DisableClCoEvents;
    procedure EnableClCoEvents;

    procedure ClientDataRequired(clnbr: integer);

    function CloneCompanyTable(Company: TEvoCompanyDef; tableName: string): TkbmMemTable;

    property DS[name: string]: TkbmCustomMemTable read GetDS;
    property DataSource[name: string]: TDataSource read GetDataSource;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  private
    FLogger: ICommonLogger;
    FCompanyChangedEvents: array of TCompanyChangedEvent;
    FConn: IEvoAPIConnection;

    FDS: array of TEvoDS;

    FClientDataFetched: array of integer;

    FLastCompany: TNullableEvoCompanyDef; //used to track changes
    FLastUsedCompany: TNullableEvoCompanyDef; //used to restore company after refresh

    procedure ClearTables;
    procedure CLdsDataChange(Sender: TObject; Field: TField);
    procedure COdsDataChange(Sender: TObject; Field: TField);
    procedure FireCompanyChanged;
    function FindEvent(ev: TCompanyChangedEvent): integer;

    function IsClientDataFetched(clnbr: integer): boolean;
    procedure SetClientDataFetched(clnbr: integer);

    function GetEvoDSIdx(name: string): integer;
    function GetNullableClCo: TNullableEvoCompanyDef;
  end;

function GetEvoDBDTInfo(logger: ICommonLogger; EvoData: TEvoData): TEvoDBDTInfo; overload;
function GetEvoDBDTInfo(logger: ICommonLogger; conn: IEvoAPIConnection; company: TEvoCompanyDef): TEvoDBDTInfo; overload;

function GetEvoDBDTLevel(CUSTOM_DBDT: TDataSet): char;

implementation

uses
  gdyGlobalWaitIndicator, sysutils, variants, gdyRedir, gdycommon, gdystrset;

{ TEvoEEData }

constructor TEvoEEData.Create(conn: IEvoAPIConnection; logger: ICommonLogger; aCompany: TEvoCompanyDef);
begin
  Company := aCompany;
  FConn := conn;
  FLogger := logger;
  EEs := ExecCoQuery(conn, Logger, Company, 'qEEExport.rwq', 'Getting list of employees from Evolution');
  FEEsDs := TDataSource.Create(nil);
  FEEsDs.DataSet := EEs;
end;

destructor TEvoEEData.Destroy;
begin
  FreeAndNil(FEEAdditionalFields);
  FreeAndNil(FEERates);
  FreeAndNil(FEEsDs);
  FreeAndNil(FEETermDates);
  FreeAndNil(EEs);
  inherited;
end;

function TEvoEEData.EEAdditionalFields: TkbmCustomMemTable;
begin
  if FEEAdditionalFields = nil then
  begin
    FEEAdditionalFields := ExecCoQuery(FConn, FLogger, Company, 'qEEAdditionalFieldsExport.rwq', 'Getting employee additional fields from Evolution');
    FEEAdditionalFields.MasterSource := FEEsDs;
    FEEAdditionalFields.MasterFields := 'EE_NBR';
    FEEAdditionalFields.DetailFields := 'EE_NBR';
    EEs.DisableControls;
    EEs.EnableControls;
  end;
  Result := FEEAdditionalFields;
end;

function TEvoEEData.EERates: TkbmCustomMemTable;
begin
  if FEERates = nil then
  begin
    FEERates := ExecCoQuery(FConn, FLogger, Company, 'qEERateExport.rwq', 'Getting employee rates from Evolution');
    FEERates.MasterSource := FEEsDs;
    FEERates.MasterFields := 'EE_NBR';
    FEERates.DetailFields := 'EE_NBR';
    EEs.DisableControls;
    EEs.EnableControls;
  end;
  Result := FEERates;
end;

function TEvoEEData.EETermDates: TkbmCustomMemTable;
begin
  if FEETermDates = nil then
  begin
    FEETermDates := ExecCoQuery(FConn, FLogger, Company, 'qEeTerminationDate.rwq', 'Getting employee termination dates from Evolution');
    FEETermDates.MasterSource := FEEsDs;
    FEETermDates.MasterFields := 'EE_NBR';
    FEETermDates.DetailFields := 'EE_NBR';
    EEs.DisableControls;
    EEs.EnableControls;
  end;
  Result := FEETermDates;
end;

function TEvoEEData.GetEEAdditionalField(name: string): Variant;
var
  val: Variant;
begin
  Result := Null;
  name := trim(name);
  if name = 'Standard Hours' then
  begin
    Result := EEs.FieldByName('STANDARD_HOURS').AsFloat;
    Exit;
  end;
  EEAdditionalFields.First;
  while not EEAdditionalFields.Eof do
  begin
    if trim(EEAdditionalFields.FieldByName('NAME').AsString) = name then
    begin
      val := EEAdditionalFields['INFO_STRING'];
      if trim(VarToStr(val)) = '' then
        val := EEAdditionalFields['INFO_VALUE'];
      if trim(VarToStr(val)) = '' then
        val := EEAdditionalFields['INFO_AMOUNT'];
      if trim(VarToStr(val)) = '' then
        val := EEAdditionalFields['INFO_DATE'];

      if Ord(IsSet('INFO_STRING')) + Ord(IsSet('INFO_VALUE')) + Ord(IsSet('INFO_AMOUNT')) + Ord(IsSet('INFO_DATE')) > 1 then
        FLogger.LogWarningFmt('There is more than one value specified for the additional field ''%s'' (''%s'', ''%s'', ''%s'', ''%s''), exporting ''%s''', [name, GetStr('INFO_STRING'), GetStr('INFO_VALUE'),GetStr('INFO_AMOUNT'),GetStr('INFO_DATE'), VarToStr(val)] );

      if VarIsNull(Result) then
        Result := val
      else
        if trim(VarToStr(Result)) <> trim(VarToStr(val)) then
          raise Exception.CreateFmt('There is more than one entry for the additional field ''%s'' (''%s'' and ''%s'')',[name, VarToStr(Result), VarToStr(val)] );
    end;
    EEAdditionalFields.Next;
  end;
end;

function TEvoEEData.GetEEAdditionalFieldVal(
  name: string): TEeAdditionalField;
var
  val: Variant;
begin
  Result.Value := Null;
  Result.IsSetup := False;
  name := trim(name);
  if name = 'Standard Hours' then
  begin
    Result.Value := EEs.FieldByName('STANDARD_HOURS').AsFloat;
    Exit;
  end;
  EEAdditionalFields.First;
  while not EEAdditionalFields.Eof do
  begin
    if trim(EEAdditionalFields.FieldByName('NAME').AsString) = name then
    begin
      val := EEAdditionalFields['INFO_STRING'];
      if trim(VarToStr(val)) = '' then
        val := EEAdditionalFields['INFO_VALUE'];
      if trim(VarToStr(val)) = '' then
        val := EEAdditionalFields['INFO_AMOUNT'];
      if trim(VarToStr(val)) = '' then
        val := EEAdditionalFields['INFO_DATE'];

      if Ord(IsSet('INFO_STRING')) + Ord(IsSet('INFO_VALUE')) + Ord(IsSet('INFO_AMOUNT')) + Ord(IsSet('INFO_DATE')) > 1 then
        FLogger.LogWarningFmt('There is more than one value specified for the additional field ''%s'' (''%s'', ''%s'', ''%s'', ''%s''), exporting ''%s''', [name, GetStr('INFO_STRING'), GetStr('INFO_VALUE'),GetStr('INFO_AMOUNT'),GetStr('INFO_DATE'), VarToStr(val)] );

      if VarIsNull(Result.Value) then
      begin
        Result.Value := val;
        Result.IsSetup := True;
      end
      else
        if trim(VarToStr(Result.Value)) <> trim(VarToStr(val)) then
          raise Exception.CreateFmt('There is more than one entry for the additional field ''%s'' (''%s'' and ''%s'')',[name, VarToStr(Result.Value), VarToStr(val)] );
    end;
    EEAdditionalFields.Next;
  end;
end;

function TEvoEEData.GetEEDBDTCustomNumbers: string;
  procedure Add(fn: string);
  begin
    if not EEs.FieldByName(fn).IsNull then
      if Result <> '' then
        Result := Result + '|' + trim(EEs[fn])
      else
        Result := trim(EEs[fn]);
  end;
begin
  Result := '';
  Add('CUSTOM_DIVISION_NUMBER');
  Add('CUSTOM_BRANCH_NUMBER');
  Add('CUSTOM_DEPARTMENT_NUMBER');
  Add('CUSTOM_TEAM_NUMBER');
end;

function TEvoEEData.GetEERatesArray: TRateArray;
var
  i: integer;
begin
  i := low(Result);
  EERates.First;
  while not EERates.Eof and (i <= high(Result) ) do
  begin
    Result[i] := EERates.FieldByName('Rate_Amount').AsFloat; //null gets converted to '0'
    i := i + 1;
    EERates.Next;
  end;
  while i <= high(Result) do
  begin
    Result[i] := 0;
    i := i + 1;
  end
end;

function TEvoEEData.GetEERatesArrayTWP: TRateArrayTWP;
var
  i: integer;
begin
  i := low(Result);
  EERates.First;
  while not EERates.Eof and (i <= high(Result) ) do
  begin
    Result[i].rate := EERates.FieldByName('Rate_Amount').AsFloat; //null gets converted to '0'
    Result[i].dep := EERates.FieldByName('Custom_Department_Number').AsString; //null gets converted to empty string
    Result[i].effectiveDate := EERates.FieldByName('Effective_Date').AsDateTime; //null gets converted to '0'
    i := i + 1;
    EERates.Next;
  end;
  while i <= high(Result) do
  begin
    Result[i].rate := 0;
    Result[i].dep := '';
    Result[i].effectiveDate := 0;
    i := i + 1;
  end
end;

function TEvoEEData.GetEETerminationDate: Variant;
begin
  if EEs.FieldByName('CURRENT_TERMINATION_DATE').IsNull then
  begin
    if EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE then
      Result := Null
    else
    begin
      EETermDates.First;
      if not EETermDates.Eof then
        Result := EETermDates['TerminationDate']
      else
        Result := Null; //must be bug in the query
    end;
  end
  else
    Result := EEs['CURRENT_TERMINATION_DATE'];
end;

function TEvoEEData.GetPrimaryRate: Double;
begin
  Result := 0;
  EERates.First;
  if not EERates.Eof then
    Result := EERates.FieldByName('Rate_Amount').AsFloat; //null gets converted to '0'
end;

function TEvoEEData.GetStr(s: string): string;
begin
  Result := VarToStr(EEAdditionalFields[s]);
end;

function TEvoEEData.IsSet(s: string): boolean;
begin
  Result := trim(GetStr(s)) <> '';
end;

function TEvoEEData.LocateEE(code: string): boolean;
begin
  Result := false;
  code := trim(code);
  EEs.DisableControls;
  try
    EEs.First;
    while not EEs.Eof do
    begin
      if trim(EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = code then
      begin
        Result := true;
        Exit;
      end;
      EEs.Next;
    end;
  finally
    EEs.EnableControls;
  end;
end;

function AppendData(data, cldata: TkbmCustomMemTable): TkbmCustomMemTable;
var
  fieldnames: string;
begin
  if data = nil then
    Result := cldata
  else
  try
    Result := data;
    cldata.First;
    cldata.FieldList.Delimiter := ';';
    fieldnames := cldata.FieldList.DelimitedText;
    while not cldata.Eof do
    begin
      data.Append;
      try
        data.FieldValues[fieldnames] := cldata.FieldValues[fieldnames];
      except
        data.Cancel;
        raise;
      end;
      data.Post;
      cldata.Next;
    end
  finally
    FreeAndNil(cldata);
  end;
end;

{ TEvoData }

procedure TEvoData.AddDS(desc: TEvoDSDesc);
begin
  Assert(FConn = nil); //disconnected
  SetLength(FDS, Length(FDS)+1);
  FDS[high(FDS)].Desc := desc;
end;

constructor TEvoData.Create(logger: ICommonLogger);
const
  CLDesc: TEvoDSDesc = (Name: 'TMP_CL'; ClientDb: false );
  CODesc: TEvoDSDesc = (Name: 'TMP_CO'; ClientDb: false; MasterName: 'TMP_CL'; LinkFields: 'CL_NBR');
begin
  FLogger := logger;

  AddDS(CLDesc);
  AddDS(CODesc);
  inherited Create(nil);
end;

destructor TEvoData.Destroy;
var
  i: integer;
begin
  ClearTables;
  for i := high(FDS) downto low(FDS) do
    FreeAndNil(FDS[i].DataSet);
  inherited;
end;

procedure TEvoData.ClearTables;
var
  i: integer;
begin
  FConn := nil; //after this CanGetClCo will return false
  FireCompanyChanged; //here! for IP components

  SetLength(FClientDataFetched, 0);

  for i := high(FDS) downto low(FDS) do
  begin
    if FDS[i].DataSource <> nil then
    begin
      FDS[i].DataSource.DataSet := nil;
      FDS[i].DataSource.OnDataChange := nil;
    end;
    if FDS[i].DataSet <> nil then
      FDS[i].DataSet.EmptyTable;
  end;
end;

function TEvoData.GetEvoDSIdx(name: string): integer;
var
  i: integer;
begin
  for i := low(FDS) to high(FDS) do
    if FDS[i].Desc.Name = name then
    begin
      Result := i;
      Exit;
    end;
  Result := -1; //to make compiler happy; we don't turn Assert off
  Assert(false);
end;

procedure TEvoData.Connect(param: TEvoAPIConnectionParam);
  procedure DoIt(var ds: TEvoDS);
  var
    mi: integer;
  begin
    ds.DataSet := AppendData( ds.DataSet, FConn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + ds.Desc.Name + '.rwq') );
    if ds.Desc.MasterName <> '' then
    begin
      mi := GetEvoDSIdx(ds.Desc.MasterName);
      ds.DataSet.MasterSource := FDS[mi].DataSource;
      Assert(ds.Desc.LinkFields <> '');
      ds.DataSet.MasterFields := ds.Desc.LinkFields;
      ds.DataSet.DetailFields := ds.Desc.LinkFields;
    end;
    ds.DataSet.First;

    if ds.DataSource = nil then
      ds.DataSource := TDataSource.Create(Self);
  end;

var
  i: integer;
begin
  Disconnect;

  WaitIndicator.StartWait('Getting data from Evolution');
  try
    FConn := CreateEvoAPIConnection(param, FLogger);
    try
      for i := low(FDS) to high(FDS) do
        if not FDS[i].Desc.ClientDB then
          DoIt(FDS[i]);


      for i := low(FDS) to high(FDS) do
        if not FDS[i].Desc.ClientDB then
          FDS[i].DataSource.DataSet := FDS[i].DataSet;

      if FLastUsedCompany.HasValue then
      begin
        FDS[GetEvoDSIdx('TMP_CL')].DataSet.Locate('CL_NBR', FLastUsedCompany.Value.ClNbr, []);
        FDS[GetEvoDSIdx('TMP_CO')].DataSet.Locate('CO_NBR', FLastUsedCompany.Value.CoNbr, []);
      end;

      FDS[GetEvoDSIdx('TMP_CL')].DataSource.OnDataChange := CLdsDataChange;
      FDS[GetEvoDSIdx('TMP_CO')].DataSource.OnDataChange := COdsDataChange;

      CLdsDataChange(FDS[GetEvoDSIdx('TMP_CL')].DataSource, nil);
      COdsDataChange(FDS[GetEvoDSIdx('TMP_CO')].DataSource, nil);
    except
      ClearTables;
      raise;
    end;
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TEvoData.Disconnect;
begin
  ClearTables;
end;

procedure TEvoData.FireCompanyChanged;
var
  oldCompany: TNullableEvoCompanyDef;
  i: integer;
begin
  if Length(FCompanyChangedEvents) > 0 then
  begin
    oldCompany := FLastCompany;
    FLastCompany := GetNullableClCo;

    if not NullableEvoCompanyDefsAreEqual(oldCompany, FLastCompany) or
      (Connected and (DS['TMP_CL'].RecordCount > 0) and (DS['TMP_CO'].RecordCount = 0)) then
    begin
      for i := 0 to high(FCompanyChangedEvents) do
        FCompanyChangedEvents[i](Self, oldCompany, FLastCompany);
    end;

    //do it after firing event, if an exception is raised then don't remember this company
    if FLastCompany.HasValue then
      FLastUsedCompany := FLastCompany;
  end;
end;

procedure TEvoData.COdsDataChange(Sender: TObject; Field: TField);
begin
  //COdsDataChange is called before CLdsDataChange and I need client data in CompanyChanged handlers
  if DS['TMP_CO'].RecordCount <> 0 then
    ClientDataRequired( DS['TMP_CO']['CL_NBR'] );
{  else begin
    FLastCompany.HasValue := True;
    FLastCompany.Value.ClNbr := -1;
  end;}
  FireCompanyChanged;
end;

procedure TEvoData.ClientDataRequired(clnbr: integer);
  function HasPerClientDS: boolean;
  var
    i: integer;
  begin
    Result := faLse;
    for i := low(FDS) to high(FDS) do
      if FDS[i].Desc.ClientDB then
      begin
        Result := true;
        break;
      end;
  end;

  procedure DoIt(var ds: TEvoDS);
  var
    mi: integer;
  begin
    ds.DataSet := AppendData( ds.DataSet, ExecClQuery(FConn, FLogger, clnbr, ds.Desc.Name + '.rwq', 'Fetching '+ds.Desc.Name) );

    //!! copy-paste from TEvoData.Connect;
    if ds.Desc.MasterName <> '' then
    begin
      mi := GetEvoDSIdx(ds.Desc.MasterName);
      ds.DataSet.MasterSource := FDS[mi].DataSource;
      Assert(ds.Desc.LinkFields <> '');
      ds.DataSet.MasterFields := ds.Desc.LinkFields;
      ds.DataSet.DetailFields := ds.Desc.LinkFields;

      ds.DataSet.MasterSource.Enabled := false;
      ds.DataSet.MasterSource.Enabled := true;
    end;
    ds.DataSet.First;

    if ds.DataSource = nil then
      ds.DataSource := TDataSource.Create(Self);
    ds.DataSource.DataSet := ds.DataSet;
  end;
var
  i: integer;
begin
  if HasPerClientDS and not IsClientDataFetched(clnbr) then
  begin
    SetClientDataFetched(clnbr); //!! set it here - WaitIndicator.StartWait may cause reentrancy!!!
    DisableClCoEvents;  // for the 'ds.DataSet.MasterSource.Enabled := true;' above not to trigger events
    try
      WaitIndicator.StartWait('Getting client data from Evolution');
      try
        for i := low(FDS) to high(FDS) do
          if FDS[i].Desc.ClientDB then
            DoIt(FDS[i]);
      finally
        WaitIndicator.EndWait;
      end;
    finally
      EnableClCoEvents;
    end;
  end;
end;

procedure TEvoData.CLdsDataChange(Sender: TObject; Field: TField);
begin
  if DS['TMP_CL'].RecordCount <> 0 then
    ClientDataRequired( DS['TMP_CL']['CL_NBR'] );
end;

procedure TEvoData.Advise(ev: TCompanyChangedEvent);
begin
  Assert(FindEvent(ev) = -1);
  Assert( TObject(TMethod(ev).Data) is TComponent );
  (TObject(TMethod(ev).Data) as TComponent).FreeNotification(Self);
  SetLength(FCompanyChangedEvents, Length(FCompanyChangedEvents)+1);
  FCompanyChangedEvents[high(FCompanyChangedEvents)] := ev;
end;

procedure TEvoData.Unadvise(ev: TCompanyChangedEvent);
var
  p: integer;
  i: integer;
begin
  p := FindEvent(ev);
  Assert(p <> -1);
  for i := p+1 to high(FCompanyChangedEvents) do
    FCompanyChangedEvents[i-1] := FCompanyChangedEvents[i];
  SetLength(FCompanyChangedEvents, Length(FCompanyChangedEvents)-1);
end;

function TEvoData.FindEvent(ev: TCompanyChangedEvent): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to high(FCompanyChangedEvents) do
    if (TMethod(FCompanyChangedEvents[i]).Code = TMethod(ev).Code) and
       (TMethod(FCompanyChangedEvents[i]).Data = TMethod(ev).Data) then
    begin
      Result := i;
      break;
    end;
end;

function TEvoData.IsClientDataFetched(clnbr: integer): boolean;
var
  i: integer;
begin
  Result := false;
  for i := 0 to high(FClientDataFetched) do
    if FClientDataFetched[i] = clnbr then
    begin
      Result := true;
      exit;
    end
end;

procedure TEvoData.SetClientDataFetched(clnbr: integer);
begin
  Assert(not IsClientDataFetched(clnbr));
  SetLength(FClientDataFetched, Length(FClientDataFetched)+1);
  FClientDataFetched[high(FClientDataFetched)] := clnbr;
end;

function TEvoData.Connected: boolean;
begin
  Result := assigned(FConn)
            and (FDS[GetEvoDsIdx('TMP_CL')].DataSet <> nil)
            and (FDS[GetEvoDsIdx('TMP_CO')].DataSet <> nil);
end;

function TEvoData.CanGetClCo: boolean;
begin
  Result := Connected and (DS['TMP_CL'].RecordCount > 0) and (DS['TMP_CO'].RecordCount > 0);
end;

function TEvoData.GetClCo: TEvoCompanyDef;
begin
  Assert(CanGetClCo);
  Result.ClNbr := DS['TMP_CO']['CL_NBR'];
  Result.CoNbr := DS['TMP_CO']['CO_NBR'];
  Result.CUSTOM_COMPANY_NUMBER := DS['TMP_CO']['CUSTOM_COMPANY_NUMBER'];
  Result.Name := DS['TMP_CO']['Name'];
  Result.CUSTOM_CLIENT_NUMBER := DS['TMP_CL']['CUSTOM_CLIENT_NUMBER'];
  Result.ClientName := DS['TMP_CL']['Name'];
end;

procedure TEvoData.SetClCo(company: TEvoCompanyDef);
begin
  Assert(Connected);
  if not DS['TMP_CL'].Locate('CL_NBR', company.ClNbr, []) then
    raise Exception.CreateFmt('Client with internal number %d not found', [company.ClNbr]);
  if not DS['TMP_CO'].Locate('CO_NBR', company.CoNbr, []) then
    raise Exception.CreateFmt('Company with internal number %d not found', [company.CoNbr]);
end;

procedure TEvoData.Notification(AComponent: TComponent; Operation: TOperation);
var
  i: integer;
begin
  inherited;
  if Operation = opRemove then
    for i := 0 to high(FCompanyChangedEvents) do
      if TMethod(FCompanyChangedEvents[i]).Data = AComponent then
      begin
        Unadvise(FCompanyChangedEvents[i]);
        exit;
      end;
end;

function TEvoData.CreateEEData: TEvoEEData;
begin
  Assert( CanGetClCo );
  Result := TEvoEEData.Create(FConn, FLogger, GetClCo);
end;

function TEvoData.Connection: IEvoAPIConnection;
begin
  Assert(FConn <> nil);
  Result := FConn;
end;

function TEvoData.CanGetPr: boolean;
begin
  Result := CanGetClCo and (FDS[GetEvoDsIdx('TMP_PR')].DataSet <> nil) and (DS['TMP_PR'].RecordCount > 0);
end;

function TEvoData.GetPr: TEvoPayrollDef;
begin
  Assert(CanGetPr);

  Result.Company := GetClCo;

  Result.Pr.PrNbr := DS['TMP_PR']['PR_NBR'];
  Result.Pr.Run := DS['TMP_PR']['RUN_NUMBER'];
  Result.Pr.CheckDate := DS['TMP_PR'].FieldByName('CHECK_DATE').AsDateTime;
end;

function TEvoData.CanGetPrBatch: boolean;
begin
  Result := CanGetClCo and (FDS[GetEvoDsIdx('PR_BATCH')].DataSet <> nil) and (DS['PR_BATCH'].RecordCount > 0);
end;

function TEvoData.GetPrBatch: TEvoPayrollBatchDef;
begin
  Assert(CanGetPrBatch);

  Result.Company := GetClCo;
  Result.Pr := GetPr.Pr;

  Result.PrBatchNbr := DS['PR_BATCH']['PR_BATCH_NBR'];
  Result.PayPeriod.PeriodBegin := DS['PR_BATCH'].FieldByName('PERIOD_BEGIN_DATE').AsDateTime;
  Result.PayPeriod.PeriodEnd := DS['PR_BATCH'].FieldByName('PERIOD_END_DATE').AsDateTime;
  Result.HasRegularChecks := DS['PR_BATCH'].FieldByName('ChecksCount').AsInteger > 0;
  Result.HasUserEarningsLines := DS['PR_BATCH'].FieldByName('EarningsCount').AsInteger > 0;
end;

function TEvoData.GetDS(name: string): TkbmCustomMemTable;
begin
  Result := FDS[GetEvoDsIdx(name)].DataSet;
  Assert(Result <> nil, 'Dataset "' + name + '" is not found');
end;

procedure TEvoData.DisableClCoEvents;
begin
  Assert( assigned(FDS[GetEvoDSIdx('TMP_CL')].DataSource.OnDataChange) );
  Assert( assigned(FDS[GetEvoDSIdx('TMP_CO')].DataSource.OnDataChange) );
  FDS[GetEvoDSIdx('TMP_CL')].DataSource.OnDataChange := nil;
  FDS[GetEvoDSIdx('TMP_CO')].DataSource.OnDataChange := nil;
end;

procedure TEvoData.EnableClCoEvents;
begin
  Assert( not assigned(FDS[GetEvoDSIdx('TMP_CL')].DataSource.OnDataChange) );
  Assert( not assigned(FDS[GetEvoDSIdx('TMP_CO')].DataSource.OnDataChange) );
  FDS[GetEvoDSIdx('TMP_CL')].DataSource.OnDataChange := CLdsDataChange;
  FDS[GetEvoDSIdx('TMP_CO')].DataSource.OnDataChange := COdsDataChange;
  CLdsDataChange(FDS[GetEvoDSIdx('TMP_CL')].DataSource, nil);
  COdsDataChange(FDS[GetEvoDSIdx('TMP_CO')].DataSource, nil);
end;

function TEvoData.CloneCompanyTable(Company: TEvoCompanyDef; tableName: string): TkbmMemTable;
begin
  ClientDataRequired(Company.ClNbr);

  Result := TkbmMemTable.Create(nil);
  try
    Result.AttachedTo := DS[tableName];
    Result.Filter := Format('CL_NBR=%d and CO_NBR=%d', [Company.ClNbr, Company.CoNbr]);
    Result.Filtered := true;
    Result.Open;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function DoGetEvoDBDTInfo(logger: ICommonLogger; CUSTOM_DBDT: TDataSet; company: TEvoCompanyDef): TEvoDBDTInfo;
var
  DBDT: TDataSet;

  procedure GetLevel(level: char);
  var
    hasHidden: boolean;
    hasVisible: boolean;
    fn: string;
    nm: string;
  begin
    fn := CDBDTMetadata[level].PrintField;
    hasHidden := false;
    hasVisible := false;
    DBDT.First;
    while not DBDT.eof do
    begin
      if not DBDT.FieldByName(fn).IsNull then
        if DBDT.FieldByName(fn).AsString[1] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
          hasVisible := true
        else
          hasHidden := true;
      DBDT.Next;
    end;
    nm := AnsiLowerCase( Plural(CDBDTMetadata[level].Name) );
    if (hasVisible or hasHidden) and (level>Result.DBDTLevel) then
      logger.LogWarningFmt('Evolution D/B/D/T Level is set to ''%s'' but some %s are defined',[ CDBDTMetadata[Result.DBDTLevel].Name, nm ]);

    Result.Levels[level].Enabled := false;
    if level>Result.DBDTLevel then
      Result.Levels[level].Reason := Format('Company D/B/D/T Level is set to ''%s''', [CDBDTMetadata[Result.DBDTLevel].Name])
    else if hasHidden and not hasVisible then //mixed may be OK
      Result.Levels[level].Reason := Format('All Evolution %s have Display Option set to ''Hide''', [nm])
    else
      Result.Levels[level].Enabled := true;
    Result.Levels[level].Name := CDBDTMetadata[level].Name;
  end;

var
  temp: string;
  level: char;
begin
  Logger.LogEntry('Analyzing Evolution D/B/D/T structure');
  try
    try
      DBDT := CUSTOM_DBDT;
      LogEvoCompanyDef(Logger, company);

      Assert(not DBDT.Filtered);
      DBDT.First;
      if not DBDT.Eof then
      begin
        temp := DBDT['DBDT_LEVEL'];
        Assert(Length(temp)=1);
        Assert(temp[1] in [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM]);
        Result.DBDTLevel := temp[1];

        for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
          GetLevel(level);
      end
      else
      begin
        Result.DBDTLevel := CLIENT_LEVEL_COMPANY;
        for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
        begin
          Result.Levels[level].Name := CDBDTMetadata[level].Name;
          Result.Levels[level].Enabled := false;
          Result.Levels[level].Reason := 'No D/B/D/Ts found';
        end
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function GetEvoDBDTInfo(logger: ICommonLogger; EvoDaTA: TEvoData): TEvoDBDTInfo;
begin
  Result := DoGetEvoDBDTInfo(logger, EvoData.DS['CUSTOM_DBDT'], EvoData.GetClCo);
end;

function GetEvoDBDTInfo(logger: ICommonLogger; conn: IEvoAPIConnection; company: TEvoCompanyDef): TEvoDBDTInfo;
var
  CUSTOM_DBDT: TDataSet;
begin
  CUSTOM_DBDT := ExecCoQuery( conn, logger, Company, 'CUSTOM_DBDT.rwq', 'Getting D/B/D/T info from Evolution');
  try
    Result := DoGetEvoDBDTInfo(logger, CUSTOM_DBDT, Company);
  finally
    FreeAndNil(CUSTOM_DBDT);
  end;
end;

function GetEvoDBDTLevel(CUSTOM_DBDT: TDataSet): char;
begin
  Result := CLIENT_LEVEL_COMPANY;
  CUSTOM_DBDT.First;
  if not CUSTOM_DBDT.Eof then
  begin
    Assert( Length(CUSTOM_DBDT.FieldByName('DBDT_LEVEL').AsString) = 1 );
    Result := CUSTOM_DBDT.FieldByName('DBDT_LEVEL').AsString[1];
    Assert( Result in [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] );
  end;
end;

function TEvoData.GetNullableClCo: TNullableEvoCompanyDef;
begin
  Result.HasValue := CanGetClCo;
  if Result.HasValue then
    Result.Value := GetClCo;
end;


function TEvoData.GetDataSource(name: string): TDataSource;
begin
  Result := FDS[GetEvoDsIdx(name)].DataSource;
  Assert(Result <> nil);
end;

{ TEvoDBDTs }

constructor TEvoDBDTs.Create(conn: IEvoApiConnection; company: TEvoCompanyDef; logger: ICommonLogger);
var
  level: char;
begin
  FLogger := Logger;

  Logger.LogEntry('Getting Evolution DBDTs');
  try
    try
      LogEvoCompanyDef(Logger, company);

      for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
      begin
        FDataSets[level] := ExecClQuery(conn, Logger, company.ClNbr, CDBDTMetadata[level].Table + '.rwq', Format('Getting %s from Evolution',[Plural(AnsiLowerCase(CDBDTMetadata[level].Name))] ) );
        FDataSources[level] := TDataSource.Create(nil);
        FDataSources[level].DataSet := FDataSets[level];
        if level > CLIENT_LEVEL_DIVISION then
        begin
          FDataSets[level].MasterSource := FDataSources[Pred(level)];
          FDataSets[level].MasterFields := CDBDTMetadata[Pred(level)].Table + '_NBR';
          FDataSets[level].DetailFields := CDBDTMetadata[Pred(level)].Table + '_NBR';
        end;
      end;
      FDataSets[CLIENT_LEVEL_DIVISION].Filter := 'CO_NBR=' + inttostr(company.CoNbr);
      FDataSets[CLIENT_LEVEL_DIVISION].Filtered := true;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;

end;

destructor TEvoDBDTs.Destroy;
var
  level: char;
begin
  for level := CLIENT_LEVEL_TEAM downto CLIENT_LEVEL_DIVISION do
  begin
    FreeAndNil(FDataSources[level]);
    FreeAndNil(FDataSets[level]);
  end;
end;

function TEvoDBDTs.GetDS(level: char): TkbmCustomMemTable;
begin
  Result := FDataSets[level];
end;

function TEvoDBDTs.GetDS_Int(level: integer): TkbmCustomMemTable;
begin
  Result := DS[char(level)];
end;

end.
