inherited SmtpConfigFrm: TSmtpConfigFrm
  Width = 833
  Height = 180
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 833
    Height = 180
    Align = alClient
    Caption = 'Email configuration'
    TabOrder = 0
    object Label6: TLabel
      Left = 352
      Top = 92
      Width = 56
      Height = 13
      Caption = 'To: address'
    end
    object Label8: TLabel
      Left = 352
      Top = 47
      Width = 66
      Height = 13
      Caption = 'From: address'
    end
    object edAddressTo: TEdit
      Left = 352
      Top = 108
      Width = 317
      Height = 21
      TabOrder = 3
    end
    object edAddressFrom: TEdit
      Left = 352
      Top = 63
      Width = 317
      Height = 21
      TabOrder = 2
    end
    object btnSendTest: TBitBtn
      Left = 352
      Top = 144
      Width = 83
      Height = 22
      Caption = 'Test'
      TabOrder = 4
      OnClick = btnSendTestClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD6666666800
        08DDDD8888888DFFFDDDD666666660BBB0DDD88888888D888FDD666EEEEE70BB
        B0DD888DDDDDDD888FDD66EDDDDDDD0008DD88DDDDDDDDFFFDDD66DD666660BB
        B0DD88DD88888D888FDD66D6666660BBB08D88D888888D888FDD66D66E66760B
        BB0888D88D88D8D888FD66D66D66D660BBB088D88D88D88D888F66D66D60006D
        0BB088D88D8DFFFDD88F66D66660BB000BB088D8888D88FFF88F66DE6660BBBB
        BBB088DD888D8888888D66DDEE7D0BBBBB0D88DDDDDDD88888DD666DDDDD6000
        00DD888DDDDD8DDDDDDDE6666666667DDDDDD888888888DDDDDDDE6666666EDD
        DDDDDD8888888DDDDDDDDDEEEEEEEDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
    end
    object gbSMTP: TGroupBox
      Left = 16
      Top = 46
      Width = 315
      Height = 123
      Caption = 'SMTP'
      TabOrder = 1
      object Label4: TLabel
        Left = 16
        Top = 20
        Width = 31
        Height = 13
        Caption = 'Server'
      end
      object Label2: TLabel
        Left = 257
        Top = 20
        Width = 19
        Height = 13
        Caption = 'Port'
      end
      object Label5: TLabel
        Left = 16
        Top = 71
        Width = 68
        Height = 13
        Caption = 'User (optional)'
      end
      object Label1: TLabel
        Left = 178
        Top = 71
        Width = 92
        Height = 13
        Caption = 'Password (optional)'
      end
      object edSMTPServer: TEdit
        Left = 16
        Top = 36
        Width = 226
        Height = 21
        TabOrder = 0
      end
      object edSMTPPort: TEdit
        Left = 257
        Top = 36
        Width = 41
        Height = 21
        TabOrder = 1
      end
      object edSMTPUser: TEdit
        Left = 16
        Top = 87
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object edSMTPPassword: TPasswordEdit
        Left = 178
        Top = 87
        Width = 121
        Height = 21
        PasswordChar = '*'
        TabOrder = 3
      end
    end
    object cbSendEmail: TCheckBox
      Left = 16
      Top = 21
      Width = 193
      Height = 17
      Caption = 'Send email when error happens'
      TabOrder = 0
    end
  end
end
