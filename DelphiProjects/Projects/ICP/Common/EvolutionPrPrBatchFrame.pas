unit EvolutionPrPrBatchFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, EvolutionDSFrame, evodata;

type
  TEvolutionPrPrBatchFrm = class(TFrame)
    PayrollFrame: TEvolutionDsFrm;
    Splitter1: TSplitter;
    BatchFrame: TEvolutionDsFrm;
    procedure FrameResize(Sender: TObject);
  private
  public
    procedure Init(evoData: TEvoData);
  end;

implementation

uses
  common;
{$R *.dfm}

{ TEvolutionPayrollBatchFrm }

procedure TEvolutionPrPrBatchFrm.Init(evoData: TEvoData);
begin
  PayrollFrame.dgGrid.Selected.Clear;
  AddSelected(PayrollFrame.dgGrid.Selected, 'CHECK_DATE', 0, 'Check Date', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'RUN_NUMBER', 0, 'Run Number', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'PAYROLL_TYPE', 0, 'Type', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'STATUS', 0, 'Status', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'PR_NBR', 0, 'Internal Pr#', true);

  PayrollFrame.Init(evoData, 'TMP_PR');

  BatchFrame.dgGrid.Selected.Clear;
  AddSelected(BatchFrame.dgGrid.Selected, 'PERIOD_BEGIN_DATE', 0, 'Period Begin Date', true);
  AddSelected(BatchFrame.dgGrid.Selected, 'PERIOD_END_DATE', 0, 'Period End Date', true);
  AddSelected(BatchFrame.dgGrid.Selected, 'PR_BATCH_NBR', 0, 'Batch', true);

  BatchFrame.Init(evoData, 'PR_BATCH');
end;

procedure TEvolutionPrPrBatchFrm.FrameResize(Sender: TObject);
begin
  PayrollFrame.Width := (Width - Splitter1.Width) div 2;
end;

end.
