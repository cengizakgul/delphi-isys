unit gdyResourceGraph;
{$INCLUDE gdy.inc}

interface

uses
  gdyResourceManager;
  
type
  IResourceIter = interface
['{C0B44EB2-E54F-42AA-ADD9-1298B68A31FC}']
    function ChildCount: integer;
    function ChildIter( Index: integer ): IResourceIter; //throws EOutOfRange or somewhat similar
    function Child( Index: integer ): TDependencyDef; //throws EOutOfRange or somewhat similar
    function ParentCount: integer;
    function ParentIter( Index: integer ): IResourceIter; //throws EOutOfRange or somewhat similar
    function Parent( Index: integer ): TDependencyDef; //throws EOutOfRange or somewhat similar
    function Name: string;
  end;

  IResourceDependencyGraph = interface
['{D776D46E-2560-4CF6-A0FC-69EE247176F9}']
    function ResourceByName( aResourceName: string ): IResourceIter; //throws ERMNotFound
    function ResourceCount: integer;
    function Resource( Index: integer ): IResourceIter;
  end;

  TOnResourceEvent = function( aName: string ): boolean of object;

  TGraphNavigator = class(TObject)
  private
    FGraph: IResourceDependencyGraph;
    function ForChildsRInternal( aRes: IResourceIter; handler: TOnResourceEvent ): boolean;
    function ForParentsRInternal(aRes: IResourceIter; handler: TOnResourceEvent): boolean;
    function ForParentsInternal( aRes: IResourceIter; handler: TOnResourceEvent ): boolean;
//    function ForChildsInternal( aRes: IResourceIter; handler: TOnResourceEvent ): boolean;
    procedure ForRoots( handler: TOnResourceEvent );
  public
    constructor Create( rdg: IResourceDependencyGraph );
    procedure ForParents( aName: string; handler: TOnResourceEvent ); //for resource and its parents
    procedure ForParentsR( aName: string; handler: TOnResourceEvent ); //for resource and its parents
    procedure ForChildsR( aName: string; handler: TOnResourceEvent ); //for resource and its childs
//    procedure ForChilds( aName: string; handler: TOnResourceEvent ); //for resource and its childs
    procedure ForEach( handler: TOnResourceEvent );
    procedure ForRootsChildsR( handler: TOnResourceEvent );
  end;

implementation

uses
  gdystrset;

type
  TGraphValidator = class
  private
    FNavigator: TGraphNavigator;
    FAllRes: TStringSet;
    FRoots: TStringSet;
    FVisited: TStringSet;
    function HandleRoot( aName: string ): boolean;
    function HandleEach( aName: string ): boolean;
    procedure CheckAcycle;
  public
    constructor Create( aNavigator: TGraphNavigator );
    destructor Destroy; override;
  end;

{ TGraphValidator }

procedure TGraphValidator.CheckAcycle;
  procedure CheckChild( aRes: IResourceIter; const aparents: array of string );
    function CycleStr: string;
    var
      i: integer;
      incycle: boolean;
    begin
      Result := '';
      incycle := false;
      for i := 0 to Length(aparents)-1 do
        if (aparents[i] = aRes.Name) or incycle then
        begin
          incycle := true;
          if Result <> '' then
            Result := Result + ', ';
          Result := Result + aparents[i];
        end;
    end;
  var
    i: integer;
    visited: TStringSet;
  begin
    if InSet( aRes.Name, aparents ) then
      raise ERMCycleFound.CreateFmt( 'Cycle found (parent to child direction): %s, %s',[CycleStr, aRes.Name]);
    SetAssign( visited, aparents );
    SetInclude( visited, aRes.Name);
    SetInclude( FVisited, aRes.Name);
    for i := 0 to aRes.ChildCount-1 do
      CheckChild( aRes.ChildIter(i),  visited );
  end;
var
  i: integer;
begin
  FNavigator.ForEach( HandleEach );

  FNavigator.ForRoots( HandleRoot );
  for i := 0 to Length(FRoots)-1 do
    CheckChild( FNavigator.FGraph.ResourceByName(FRoots[i]), [] );
  if not SetEqual( FAllRes, FVisited ) then
    raise ERMCycleFound.CreateFmt( 'Cycle found (parent to child direction) in the following resources: <%s>',[ SetToStr(SetDiff(FAllRes, FVisited), ', ')]);

end;

constructor TGraphValidator.Create( aNavigator: TGraphNavigator );
begin
  FNavigator := aNavigator;
  checkAcycle;
end;

destructor TGraphValidator.Destroy;
begin
  inherited;
end;

function TGraphValidator.HandleEach(aName: string): boolean;
begin
  SetInclude( FAllRes, aName );
  Result := true;
end;

function TGraphValidator.HandleRoot(aName: string): boolean;
begin
  SetInclude( FRoots, aName );
  Result := true;
end;

{ TGraphNavigator }

constructor TGraphNavigator.Create(rdg: IResourceDependencyGraph);
begin
  FGraph := rdg;
  TGraphValidator.Create( Self ).Free;
end;

{
procedure TGraphNavigator.ForChilds(aName: string;
  handler: TOnResourceEvent);
begin
  ForChildsInternal( FGraph.ResourceByName(aName), handler );
end;

function TGraphNavigator.ForChildsInternal(aRes: IResourceIter;
  handler: TOnResourceEvent): boolean;
var
  i: integer;
begin
  Result := handler( aRes.Name );
  if Result then
    for i := 0 to aRes.ChildCount-1 do
      if not ForChildsInternal( aRes.ChildIter(i), handler ) then
      begin
        Result := false;
        break;
      end;
end;
}

procedure TGraphNavigator.ForChildsR(aName: string; handler: TOnResourceEvent);
begin
  ForChildsRInternal( FGraph.ResourceByName(aName), handler );
end;

function TGraphNavigator.ForChildsRInternal(
  aRes: IResourceIter; handler: TOnResourceEvent): boolean;
var
  i: integer;
begin
  Result := true;
  for i := 0 to aRes.ChildCount-1 do
    if not ForChildsRInternal( aRes.ChildIter(i), handler ) then
    begin
      Result := false;
      break;
    end;
  if Result then
    Result := handler( aRes.Name );
end;

procedure TGraphNavigator.ForParentsR(aName: string;
  handler: TOnResourceEvent);
begin
  ForParentsRInternal( FGraph.ResourceByName(aName), handler );
end;

function TGraphNavigator.ForParentsRInternal(
  aRes: IResourceIter; handler: TOnResourceEvent): boolean;
var
  i: integer;
begin
  Result := true;
  for i := 0 to aRes.ParentCount-1 do
    if not ForParentsRInternal( aRes.ParentIter(i), handler ) then
    begin
      Result := false;
      break;
    end;
  if Result then
    Result := handler( aRes.Name );
end;

procedure TGraphNavigator.ForEach(handler: TOnResourceEvent);
var
  i: integer;
begin
  for i := 0 to FGraph.ResourceCount-1 do
    if not handler( FGraph.Resource(i).Name ) then
      break;
end;

procedure TGraphNavigator.ForRoots(handler: TOnResourceEvent);
var
  i: integer;
  res: IResourceIter;
begin
  for i := 0 to FGraph.ResourceCount-1 do
  begin
    res := FGraph.Resource(i);
    if res.ParentCount = 0 then
      if not handler( res.Name ) then
        break;
  end
end;

procedure TGraphNavigator.ForParents(aName: string;
  handler: TOnResourceEvent);
begin
  ForParentsInternal( FGraph.ResourceByName(aName), handler );
end;

function TGraphNavigator.ForParentsInternal(aRes: IResourceIter;
  handler: TOnResourceEvent): boolean;
var
  i: integer;
begin
  Result := handler( aRes.Name );
  if Result then
    for i := 0 to aRes.ParentCount-1 do
      if not ForParentsInternal( aRes.ParentIter(i), handler ) then
      begin
        Result := false;
        break;
      end;
end;

procedure TGraphNavigator.ForRootsChildsR(handler: TOnResourceEvent);
var
  i: integer;
  res: IResourceIter;
begin
  for i := 0 to FGraph.ResourceCount-1 do
  begin
    res := FGraph.Resource(i);
    if res.ParentCount = 0 then
      if not ForChildsRInternal( res, handler ) then
        break;
  end
end;


end.
