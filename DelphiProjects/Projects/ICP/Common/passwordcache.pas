unit passwordcache;

interface

uses
  classes;

type
  TPasswordCache = class
  private
    FCache: TStringList;
    function MakeKey(app, service, username: string): string;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Add(app, service, username, password: string);
    function Get(app, service, username: string): string;
  end;

implementation

uses
  sysutils;

{ TPasswordCache }

procedure TPasswordCache.Add(app, service, username, password: string);
begin
  FCache.Values[MakeKey(app, service, username)] := password;
end;

constructor TPasswordCache.Create;
begin
  FCache := TStringList.Create;
end;

destructor TPasswordCache.Destroy;
begin
  FreeAndNil(FCache);
  inherited;
end;

function TPasswordCache.Get(app, service, username: string): string;
begin
  Result := FCache.Values[MakeKey(app, service, username)];
end;

function TPasswordCache.MakeKey(app, service, username: string): string;
begin
  Result := Format('%s|%s|%s', [app, service, username]);
end;

end.
