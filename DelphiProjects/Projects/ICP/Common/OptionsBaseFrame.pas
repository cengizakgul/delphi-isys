unit OptionsBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TOptionsBaseFrm = class(TFrame)
  private
    procedure CMChanged(var Message: TMessage); message CM_CHANGED;
  protected  
    FBlockOnChange: boolean;
    procedure DoChangeByUser;
  public
    OnChangeByUser: TNotifyEvent;
  end;

implementation

{$R *.dfm}

{ TOptionsBaseFrm }

procedure TOptionsBaseFrm.CMChanged(var Message: TMessage);
begin
  DoChangeByUser;
  inherited;
end;

procedure TOptionsBaseFrm.DoChangeByUser;
begin
  if assigned(OnChangeByUser) and not FBlockOnChange then
    OnChangeByUser(Self);
end;

end.
