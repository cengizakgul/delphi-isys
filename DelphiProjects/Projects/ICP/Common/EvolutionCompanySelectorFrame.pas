unit EvolutionCompanySelectorFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, kbmMemTable, evoapiconnection, common,
  gdycommonlogger, gdycommon, db, evodata, StdCtrls, wwdblook;

type
  TEvolutionCompanySelectorFrm = class(TFrame)
    dblcCL: TwwDBLookupCombo;
    dblcCO: TwwDBLookupCombo;
    Label1: TLabel;
    Label2: TLabel;
    procedure dblcBeforeDropDown(Sender: TObject);
    procedure dblcCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  public
    constructor Create(Owner: TComponent); override;
    procedure ShowBothNumberAndName;
    procedure Init(d: TEvoData);
  private
    FEvoData: TEvoData;
    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure UpdateControls(evoData: TEvoData);
  end;

implementation

{$R *.dfm}


{ TEvolutionCompanySelectorFrm }

constructor TEvolutionCompanySelectorFrm.Create(Owner: TComponent);
begin
  inherited;
  AddSelected(dblcCL.Selected, 'NAME', 40, 'Name', true);
  AddSelected(dblcCL.Selected, 'CUSTOM_CLIENT_NUMBER', 12, 'Number', true);
  AddSelected(dblcCO.Selected, 'NAME', 40, 'Name', true);
  AddSelected(dblcCO.Selected, 'CUSTOM_COMPANY_NUMBER', 12, 'Number', true);

  EnableControl(dblcCL, false);
  EnableControl(dblcCO, false);
  dblcCL.Enabled := false;
  dblcCO.Enabled := false;
end;

procedure TEvolutionCompanySelectorFrm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UpdateControls(EvoData);
end;

procedure TEvolutionCompanySelectorFrm.Init(d: TEvoData);
begin
  Assert(d <> nil);
  d.Advise(HandleCompanyChanged);
  UpdateControls(d);
end;

procedure TEvolutionCompanySelectorFrm.UpdateControls(evoData: TEvoData);
type
  dsgetter = function: TDataSet of object;
  procedure doit(cb: TwwDBLookupCombo; dsname: string);
  begin
    if (evoData <> nil) and evoData.Connected then
    begin
      //we are called back when a user changes company
      if cb.LookupTable <> evoData.DS[dsname] then
      begin
        cb.Navigator := false;
        cb.LookupTable := evoData.DS[dsname];
        cb.Navigator := true;
        if not cb.LookupTable.Eof then
          cb.LookupValue := cb.LookupTable[cb.LookupField];
      end
    end
    else
    begin
      cb.LookupTable := nil;
      cb.Clear;
    end;
    EnableControl(cb, (evoData <> nil) and evoData.Connected);
    cb.Enabled := (evoData <> nil) and evoData.Connected;
  end;
begin
  FEvoData := evodata;
  //TEvoData calls CompanyChanged notification from its destructor with evoData.Connected = false
  //so comboboxes will be disabled and their event handlers won't be called
  doit(dblcCL, 'TMP_CL');
  doit(dblcCO, 'TMP_CO');
  Repaint;
end;

procedure TEvolutionCompanySelectorFrm.dblcBeforeDropDown(
  Sender: TObject);
begin
  Assert( assigned(FEvoData) );
  FEvoData.DisableClCoEvents;
end;

procedure TEvolutionCompanySelectorFrm.dblcCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  Assert( assigned(FEvoData) );
  FEvoData.EnableClCoEvents;
end;

procedure TEvolutionCompanySelectorFrm.ShowBothNumberAndName;
begin
  dblcCL.Selected.Clear;
  AddSelected(dblcCL.Selected, 'CUSTOM_CLIENT_NUMBER_AND_NAME', 60, 'Number and name', true);
  dblcCO.Selected.Clear;
  AddSelected(dblcCO.Selected, 'CUSTOM_COMPANY_NUMBER_AND_NAME', 60, 'Number and name', true);
end;

end.

