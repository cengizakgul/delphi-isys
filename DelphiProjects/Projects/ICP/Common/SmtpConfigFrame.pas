unit SmtpConfigFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PasswordEdit, StdCtrls, Buttons, issettings, gdyCommonLogger, OptionsBaseFrame;

type
  TSmtpConfig = record
    Server: string;
    UserName: string;
    Password: string;
    Port: integer;
    AddressFrom: string;
    AddressTo: string;
    Use: boolean;
  end;

  TSmtpConfigFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    edAddressTo: TEdit;
    edAddressFrom: TEdit;
    btnSendTest: TBitBtn;
    gbSMTP: TGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    edSMTPServer: TEdit;
    edSMTPPort: TEdit;
    edSMTPUser: TEdit;
    edSMTPPassword: TPasswordEdit;
    cbSendEmail: TCheckBox;
    procedure btnSendTestClick(Sender: TObject);
  private
    procedure SetConfig(Value: TSmtpConfig);
    function GetConfig: TSmtpConfig;

  public
    constructor Create(Owner: TComponent); override;
    procedure Check;
    property Config: TSmtpConfig read GetConfig write SetConfig;
  end;

procedure SaveSmtpConfig(param: TSmtpConfig; conf: IisSettings; root: string );
function LoadSmtpConfig(conf: IisSettings; root: string): TSmtpConfig;

function SmtpConfigToStr(param: TSmtpConfig): string;
procedure LogSmtpConfig(Logger: ICommonLogger; param: TSmtpConfig);

procedure sendmail(SMTPConfig: TSMTPConfig; asubject, abody, aattachment: string);

implementation

{$R *.dfm}

uses
  IsSMTPClient, gdyGlobalWaitIndicator, gdyCrypt, gdycommon;

const
  cKey='Form.Button';
  
{ TSmtpConfigFrm }

procedure LogSmtpConfig(Logger: ICommonLogger; param: TSmtpConfig);
begin
  Logger.LogContextItem('SMTP server', param.Server);
  Logger.LogContextItem('Port', IntToStr(param.port));
  Logger.LogContextItem('User', param.UserName);
  Logger.LogContextItem('Password', IIF( trim(param.Password) = '', '<not used>', '<used>') );
  Logger.LogContextItem('From: address', param.AddressFrom);
  Logger.LogContextItem('To: address', param.AddressTo);
  Logger.LogContextItem('Send email notification', BoolToStr(param.Use, true));
end;

function SmtpConfigToStr(param: TSmtpConfig): string;
begin
  Result :=
    Format('SMTP server: %s'#13#10, [param.Server]) +
    Format('Port: %d'#13#10, [param.port]) +
    Format('User: %s'#13#10, [param.UserName]) +
    Format('Password: %s#13#10', [IIF( trim(param.Password) = '', 'not used', 'used') ]) +
    Format('From: address: %s'#13#10, [param.AddressFrom]) +
    Format('To: address: %s'#13#10, [param.AddressTo]) +
    Format('Send email notification: %s'#13#10, [BoolToStr(param.Use, true)] );
end;

procedure SaveSmtpConfig(param: TSmtpConfig; conf: IisSettings; root: string );
begin
  root := root + IIF(root='','','\') + 'SmtpConfig\';
  conf.AsString[root+'Server'] := param.Server;
  conf.AsString[root+'UserName'] := param.UserName;
  conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey));
  conf.AsInteger[root+'Port'] := param.Port;
  conf.AsString[root+'AddressFrom'] := param.AddressFrom;
  conf.AsString[root+'AddressTo'] := param.AddressTo;
  conf.AsBoolean[root+'Use'] := param.Use;
end;

function LoadSmtpConfig(conf: IisSettings; root: string): TSmtpConfig;
begin
  root := root + IIF(root='','','\') + 'SmtpConfig\';
  Result.Server := conf.AsString[root+'Server'];
  Result.UserName := conf.AsString[root+'UserName'];
  Result.Password := Decrypt(HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  if conf.GetValueNames(WithoutTrailingSlash(root)).IndexOf('Port') <> -1 then
    Result.Port := conf.AsInteger[root+'Port']
  else
    Result.Port := 25;
  Result.AddressFrom := conf.AsString[root+'AddressFrom'];
  Result.AddressTo := conf.AsString[root+'AddressTo'];
  Result.Use := conf.AsBoolean[root+'Use'];
end;

procedure TSmtpConfigFrm.Check;
begin
  //!!
//  if
end;

constructor TSmtpConfigFrm.Create(Owner: TComponent);
begin
  inherited;
  edSMTPPort.Text := '25';
end;

function TSmtpConfigFrm.GetConfig: TSmtpConfig;
begin
  Result.Server := trim(edSMTPServer.Text);
  Result.Port := StrToIntDef(edSMTPPort.Text, 25);
  Result.UserName := trim(edSMTPUser.Text);
  Result.Password := trim(edSMTPPassword.Password);
  Result.AddressFrom := trim(edAddressFrom.Text);
  Result.AddressTo := trim(edAddressTo.Text);
  Result.Use := cbSendEmail.Checked;
end;

procedure TSmtpConfigFrm.SetConfig(Value: TSmtpConfig);
begin
  FBlockOnChange := true;
  try
//    EnableControlRecursively(Self, true);   //always enabled
    edSMTPServer.Text := Value.Server;
    edSMTPPort.Text := IntToStr(Value.Port);
    edSMTPUser.Text := Value.UserName;
    edSMTPPassword.Password := Value.Password;
    edAddressFrom.Text := Value.AddressFrom;
    edAddressTo.Text := Value.AddressTo;
    cbSendEmail.Checked := Value.Use;
  finally
    FBlockOnChange := false;
  end;
end;

procedure sendmail(SMTPConfig: TSMTPConfig; asubject, abody, aattachment: string);
var
  SMTP: TIsSMTPClient;
begin
  SMTP := TIsSMTPClient.Create(SMTPConfig.Server, SMTPConfig.Port, SMTPConfig.UserName, SMTPConfig.Password, atAutoSelect);
  try
    if aattachment <> '' then
      SMTP.SendQuickAttach(SMTPConfig.AddressFrom, SMTPConfig.AddressTo, asubject, abody, aattachment)
    else
      SMTP.SendQuickMessage(SMTPConfig.AddressFrom, SMTPConfig.AddressTo, asubject, abody);
  finally
    FreeAndNil(SMTP);
  end;
end;

procedure TSmtpConfigFrm.btnSendTestClick(Sender: TObject);
begin
  WaitIndicator.StartWait('Sending test message');
  try
    sendmail(Config, Format('Test message from %s',[Application.Title]),
                   Format('%s email test successful!',[Application.Title]), '' );
  finally
    WaitIndicator.EndWait;
  end;
end;

end.
