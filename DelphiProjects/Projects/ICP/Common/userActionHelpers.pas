unit userActionHelpers;

interface

uses
  gdyCommonLogger, common, evoapiconnectionutils, evoapiconnection,
  timeclockimport, evodata, toaimport, classes;

type
  TGetTimeClockDataProc = function (period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData of object;
	TUpdateEmployeesProc = function: TUpdateEmployeesStat of object;
	TTOAProc = function: TTOAStat of object;
	TGetTOAProc = function: TTOAImportData of object;

procedure RunTimeClockImport(Logger: ICommonLogger; batch: TEvoPayrollBatchDef; proc: TGetTimeClockDataProc; conn: IEvoAPIConnection; AllowImportToBatchesWithUserEarningLines: boolean);
procedure RunEmployeeExport(Logger: ICommonLogger; proc: TUpdateEmployeesProc; company: TEvoCompanyDef; TCAppName: string);
procedure RunEmployeeImport(Logger: ICommonLogger; proc: TUpdateEmployeesProc; company: TEvoCompanyDef);
procedure RunTOAExport(Logger: ICommonLogger; proc: TTOAProc; company: TEvoCompanyDef; TCAppName: string);
procedure RunTOAImport(Logger: ICommonLogger; proc: TGetTOAProc; conn: IEvoAPIConnection; company: TEvoCompanyDef; Owner: TComponent; TCAppName: string);

procedure ReportSyncStat(Logger: ICommonLogger; stat: TSyncStat; opname: string; showGUI: boolean);

implementation

uses
  sysutils, gdyCommon, gdyGlobalWaitIndicator, dialogs, controls, EvoWaitForm, gdyClasses;

procedure RunTimeClockImport(Logger: ICommonLogger; batch: TEvoPayrollBatchDef; proc: TGetTimeClockDataProc; conn: IEvoAPIConnection; AllowImportToBatchesWithUserEarningLines: boolean);
var
  tc: TTimeClockImportData;
  res: TTCImportResult;
  nAccepted: integer;
  opname: string;
  msg: string;
begin
  opname := 'Timeclock import';
  Logger.LogEntry(opname);
  try
    try
      LogEvoPayrollBatchDef(Logger, batch);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait(opname);
      try
        if not batch.HasRegularChecks then
          raise Exception.Create('This payroll batch doesn''t have auto-created checks');
        if batch.HasUserEarningsLines and not AllowImportToBatchesWithUserEarningLines then
          if MessageDlg( 'This payroll batch already has checks with earnings. You may be doing import second time. Continue?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
            Abort;
        tc := proc(batch.PayPeriod, batch.Company);
        if SplitToLines(tc.Text).Count = 0 then
          raise Exception.Create('There is no timeclock data for the chosen period');
        res := conn.TCImport(batch, tc);
      finally
        WaitIndicator.EndWait;
      end;

      nAccepted := SplitToLines(tc.Text).Count - res.RejectedRecords;
      if res.RejectedRecords > 0 then
      begin
        if nAccepted <> 0 then
          msg := Format('%s partially completed: %d check line%s rejected, %d %s accepted.',
                  [opname, res.RejectedRecords, IIF(res.RejectedRecords>1,'s were',' was'),
                   nAccepted, IIF(nAccepted>1,'were','was')] )
        else
          msg := Format('%s failed: all check lines were rejected.', [opname]);
        Logger.LogWarning(msg + ' See details.', res.LogFile);
        MessageDlg(msg, mtError, [mbOk], 0);
      end
      else
      begin
        Logger.LogEventFmt('%s completed', [opname], Format('%d check line%s accepted.',
                  [nAccepted, IIF(nAccepted>1,'s were',' was')] ) + #13#10#13#10 + res.LogFile);
        ShowMessage('Timeclock import completed');
      end;
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure ReportSyncStat(Logger: ICommonLogger; stat: TSyncStat; opname: string; showGUI: boolean);
begin
  if stat.Errors = 0 then
  begin
    Logger.LogEvent( opname + ' ' + SyncStatToString(stat) );
    if showGUI then
      ShowMessage( opname + ' completed');
  end
  else
  begin
    Logger.LogWarning(opname + ' ' + SyncStatToString(stat));
    if showGUI then
      MessageDlg(opname + ' ' + SyncStatToString(stat), mtError, [mbOk], 0);
  end
end;

procedure RunEmployeeExport(Logger: ICommonLogger; proc: TUpdateEmployeesProc; company: TEvoCompanyDef; TCAppName: string);
var
  stat: TUpdateEmployeesStat;
  opname: string;
begin
  opname := Format('%s employee records update', [TCAppName]);
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait(opname);
      try
        stat := proc;
      finally
        WaitIndicator.EndWait;
      end;
      ReportSyncStat(Logger, stat, opname, true);
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure RunEmployeeImport(Logger: ICommonLogger; proc: TUpdateEmployeesProc; company: TEvoCompanyDef);
var
  stat: TUpdateEmployeesStat;
  opname: string;
begin
  opname := 'Evolution employee records update';
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait(opname);
      try
        stat := proc;
      finally
        WaitIndicator.EndWait;
      end;

      ReportSyncStat(Logger, stat, opname, true);
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure RunTOAExport(Logger: ICommonLogger; proc: TTOAProc; company: TEvoCompanyDef; TCAppName: string);
var
  stat: TTOAStat;
  opname: string;
begin
  opname := Format('TOA export (Evolution to %s)', [TCAppName]);
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait(opname);
      try
        stat := proc;
      finally
        WaitIndicator.EndWait;
      end;

      if stat.Errors = 0 then
      begin
        Logger.LogEvent( opname + ' ' + TOAStatToString(stat) );
        ShowMessage( opname + ' completed');
      end
      else
      begin
        Logger.LogWarning(opname + ' ' + TOAStatToString(stat));
        MessageDlg(opname + ' ' + TOAStatToString(stat), mtError, [mbOk], 0);
      end
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure RunTOAImport(Logger: ICommonLogger; proc: TGetTOAProc; conn: IEvoAPIConnection; company: TEvoCompanyDef; Owner: TComponent; TCAppName: string);
var
  data: TTOAImportData;
  stat: TTOAStat;
  opname: string;
begin
  opname := Format('TOA import (%s to Evolution)', [TCAppName]);
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait(opname);
      try
        data := proc;
      finally
        WaitIndicator.EndWait;
      end;
      stat := toaimport.ImportTOA(data.Recs, company, CreateGUIEvoXImportExecutor(conn, Owner, Logger), Logger); //shows its own wait form with the Cancel button
      stat.Errors := stat.Errors + data.Errors;

      if stat.Errors = 0 then
      begin
        Logger.LogEvent( opname + ' ' + TOAStatToString(stat) );
        ShowMessage( opname + ' completed');
      end
      else
      begin
        Logger.LogWarning(opname + ' ' + TOAStatToString(stat));
        MessageDlg(opname + ' ' + TOAStatToString(stat), mtError, [mbOk], 0);
      end
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

end.
