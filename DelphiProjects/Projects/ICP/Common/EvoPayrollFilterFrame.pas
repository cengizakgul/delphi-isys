unit EvoPayrollFilterFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, common, StdCtrls, ComCtrls;

type
  TEvoPayrollFilterFrm = class(TFrame)
    GroupBox1: TGroupBox;
    dtFrom: TDateTimePicker;
    dtTo: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
  private
    function GetFilter: TEvoPayrollFilter;
    procedure SetFilter(const Value: TEvoPayrollFilter);
  public
    constructor Create(Owner: TComponent); override;
    property Filter: TEvoPayrollFilter read GetFilter write SetFilter;
  end;

implementation

uses
  dateutils;
{$R *.dfm}

{ TEvoPayrollFilterFrm }

constructor TEvoPayrollFilterFrm.Create(Owner: TComponent);
begin
  inherited;
  dtFrom.Date := StartOfTheYear(Now);
  dtTo.Date := EndOfTheYear(Now);
end;

function TEvoPayrollFilterFrm.GetFilter: TEvoPayrollFilter;
begin
  Result.FromDate := dtFrom.Date;
  Result.ToDate := dtTo.Date;
end;

procedure TEvoPayrollFilterFrm.SetFilter(const Value: TEvoPayrollFilter);
begin
  dtFrom.Date := Value.FromDate;
  dtTo.Date := Value.ToDate;
end;

end.
