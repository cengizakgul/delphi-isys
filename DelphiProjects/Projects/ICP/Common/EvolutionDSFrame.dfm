object EvolutionDsFrm: TEvolutionDsFrm
  Left = 0
  Top = 0
  Width = 316
  Height = 199
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 316
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Dataset name'
    TabOrder = 0
  end
  object dgGrid: TReDBGrid
    Left = 0
    Top = 25
    Width = 316
    Height = 174
    DisableThemesInTitle = False
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = ds
    KeyOptions = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgWordWrap]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
  end
  object ds: TDataSource
    AutoEdit = False
    Left = 152
    Top = 96
  end
end
