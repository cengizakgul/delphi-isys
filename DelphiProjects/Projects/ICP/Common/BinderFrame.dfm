object BinderFrm: TBinderFrm
  Left = 0
  Top = 0
  Width = 561
  Height = 498
  TabOrder = 0
  OnResize = FrameResize
  object evSplitter2: TSplitter
    Left = 0
    Top = 234
    Width = 561
    Height = 5
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
  end
  object pnltop: TPanel
    Left = 0
    Top = 0
    Width = 561
    Height = 234
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object evSplitter1: TSplitter
      Left = 265
      Top = 0
      Width = 5
      Height = 234
      Beveled = True
    end
    object pnlTopLeft: TPanel
      Left = 0
      Top = 0
      Width = 265
      Height = 234
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object evPanel3: TPanel
        Left = 0
        Top = 0
        Width = 265
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lblLeft: TLabel
          Left = 0
          Top = 8
          Width = 3
          Height = 13
        end
      end
      object dgLeft: TReDBGrid
        Left = 0
        Top = 25
        Width = 265
        Height = 209
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TBinderFrame\dgLeft'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = dgLeftRowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsLeft
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnMouseDown = dgLeftMouseDown
        PaintOptions.AlternatingRowColor = clCream
        FilterDlg = False
      end
    end
    object pnlTopRight: TPanel
      Left = 270
      Top = 0
      Width = 291
      Height = 234
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object evPanel4: TPanel
        Left = 0
        Top = 0
        Width = 291
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lblRight: TLabel
          Left = 0
          Top = 8
          Width = 3
          Height = 13
        end
      end
      object dgRight: TReDBGrid
        Left = 0
        Top = 25
        Width = 291
        Height = 209
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TBinderFrame\dgRight'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsRight
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        FilterDlg = False
      end
    end
  end
  object evPanel1: TPanel
    Left = 0
    Top = 239
    Width = 561
    Height = 259
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
    object pnlbottom: TPanel
      Left = 0
      Top = 41
      Width = 561
      Height = 218
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object evPanel5: TPanel
        Left = 0
        Top = 0
        Width = 561
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lblBottom: TLabel
          Left = 0
          Top = 8
          Width = 3
          Height = 13
        end
      end
      object dgBottom: TReDBGrid
        Left = 0
        Top = 25
        Width = 561
        Height = 193
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TBinderFrame\dgBottom'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsBottom
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = dgBottomDblClick
        PaintOptions.AlternatingRowColor = clCream
        FilterDlg = False
      end
    end
    object pnlMiddle: TPanel
      Left = 0
      Top = 0
      Width = 561
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Constraints.MaxHeight = 41
      TabOrder = 0
      OnResize = pnlMiddleResize
      object evLabel1: TLabel
        Left = 400
        Top = 8
        Width = 3
        Height = 13
      end
      object pnlControl: TPanel
        Left = 0
        Top = 0
        Width = 337
        Height = 41
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object evBitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 91
          Height = 25
          Action = AddMatch
          Caption = 'Add'
          TabOrder = 0
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
            DDDDDDDDDDFDDDDDDDDDDDDDD868DDDDDDDDDDDDDF8FDDDDDDDDDDDD86E68DDD
            DDDDDDDDF888FDDDDDDDDDD86EEE68DDDDDDDDDF88888FDDDDDDDD86EEEEE68D
            DDDDDDF8888888FDDDDDD86EEEEEEE68DDDDDF888888888FDDDD86EEEEEEEEE6
            8DDDF88888888888FDDD6666EEEEE6666DDD8888888888888DDDDDD6EEEEE6DD
            DDDDDDD8888888DDDDDDDDD6E6666688888DDDD8888888FFFFFFDDD6E6000000
            0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E60FFFFF
            FFF0DDD8888DDDDDDDD8DDD6EE0000000000DDD8888888888888DDD6EEEEE6DD
            DDDDDDD8888888DDDDDDDDD6666666DDDDDDDDD8888888DDDDDD}
          NumGlyphs = 2
        end
        object evBitBtn2: TBitBtn
          Left = 112
          Top = 8
          Width = 91
          Height = 25
          Action = RemoveMatch
          Caption = 'Remove'
          TabOrder = 1
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
            DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
            888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
            FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6EE000000
            0000DDD8888888888888DDD6EEEEE6DDDDDDDDD8888888DDDDDD8886EEEEE688
            8DDDFFF8888888FFFDDD6666EEEEE6666DDD8888888888888DDDD6EEEEEEEEE6
            DDDDD88888888888DDDDDD6EEEEEEE6DDDDDDD888888888DDDDDDDD6EEEEE6DD
            DDDDDDD8888888DDDDDDDDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
            DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
          NumGlyphs = 2
        end
        object evBitBtn3: TBitBtn
          Left = 214
          Top = 8
          Width = 91
          Height = 25
          Action = RemoveAllMatches
          Caption = 'Remove All'
          TabOrder = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
            DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
            888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
            FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E6000000
            0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD88886E60FFFFF
            FFF0FFF8888DDDDDDDD86666E600000000008888888888888888D6EEE60FFFFF
            FFF0D888888DDDDDDDD8DD6EE60FFFFFFFF0DD88888DDDDDDDD8DDD6EE000000
            0000DDD8888888888888DDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
            DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
          NumGlyphs = 2
        end
      end
    end
  end
  object evActionList1: TActionList
    Left = 352
    Top = 247
    object AddMatch: TAction
      Caption = 'Add'
      ImageIndex = 4
      OnExecute = AddMatchExecute
      OnUpdate = AddMatchUpdate
    end
    object RemoveMatch: TAction
      Caption = 'Remove'
      ImageIndex = 2
      OnExecute = RemoveMatchExecute
      OnUpdate = RemoveMatchUpdate
    end
    object RemoveAllMatches: TAction
      Caption = 'Remove All'
      ImageIndex = 0
      OnExecute = RemoveAllMatchesExecute
      OnUpdate = RemoveMatchUpdate
    end
  end
  object dsLeft: TDataSource
    OnDataChange = dsLeftDataChange
    Left = 16
    Top = 128
  end
  object dsRight: TDataSource
    Left = 390
    Top = 112
  end
  object dsBottom: TDataSource
    OnDataChange = dsBottomDataChange
    Left = 224
    Top = 369
  end
end
