unit EvoWaitForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, evoapiconnection, gdyCommonLogger, ExtCtrls, common;

type
  TEvoWaitFm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    bbCancel: TBitBtn;
    Timer1: TTimer;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure Panel2Resize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  protected
    procedure HandleTaskFailed;
    procedure HandleTaskReady(r: Variant);
  public
    FLogger: ICommonLogger;
    FConnection: IEvoAPIConnection;
    FTask: TEvoTask;
    FResult: Variant;

    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;

    procedure SetText(t: string);
  end;

{
Modal result meaning:
mrYes - got valid response from Evo
mrCancel - user cancelled
mrNo - error happened
}
function CreateGUIEvoXImportExecutor(conn: IEvoAPIConnection; Owner: TComponent; logger: ICommonLogger): IEvoXImportExecutor;

implementation

{$R *.dfm}

destructor TEvoWaitFm.Destroy;
begin
  FreeAndNil(FTask);
  inherited;
end;

procedure TEvoWaitFm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not assigned(FTask) or (MessageDlg('Cancel?', mtConfirmation, [mbYes, mbNo], 0) = mrYes);
end;

procedure TEvoWaitFm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := false;
  try
    FTask.Start;
  except
    HandleTaskFailed;
    raise;
  end;
end;

constructor TEvoWaitFm.Create(Owner: TComponent);
begin
  inherited;
  Caption := Application.Title;
  FResult := Unassigned;
end;

procedure TEvoWaitFm.SetText(t: string);
begin
  Panel1.Caption := t;
end;

procedure TEvoWaitFm.Panel2Resize(Sender: TObject);
begin
  bbCancel.Left := (Panel2.Width - bbCancel.Width) div 2;
  bbCancel.Top := (Panel2.Height - bbCancel.Height) div 2;
end;

procedure TEvoWaitFm.HandleTaskFailed;
begin
  FreeAndNil(FTask); //it's OK, we are called by DeferredCall function or from our timer
  FResult := Null;
  ModalResult := mrNo;
end;

procedure TEvoWaitFm.HandleTaskReady(r: Variant);
begin
  FreeAndNil(FTask); //it's OK, we are called by DeferredCall function
  FResult := r;
  ModalResult := mrYes;
end;

procedure TEvoWaitFm.FormActivate(Sender: TObject);
begin
  Visible := Application.MainForm.Visible; //for tray applications with hidden main window
end;

type
  TGUIEvoXImportExecutor = class(TInterfacedObject, IEvoXImportExecutor)
  private
    FConn: IEvoAPIConnection;
    FOwner: TComponent;
    FLogger: ICommonLogger;
  public
    constructor Create(conn: IEvoAPIConnection; Owner: TComponent; logger: ICommonLogger);
    {IEvoXImportExecutor}
    function RunEvoXImport(inputs: TEvoXImportInputFiles; mapfileContent: string; objname: string): IEvoXImportResultsWrapper;
  end;

{ TGUIEvoXImportExecutor }
constructor TGUIEvoXImportExecutor.Create(conn: IEvoAPIConnection; Owner: TComponent; logger: ICommonLogger);
begin
  FConn := conn;
  FOwner := Owner;
  FLogger := logger;
end;

function TGUIEvoXImportExecutor.RunEvoXImport(inputs: TEvoXImportInputFiles; mapfileContent: string; objname: string): IEvoXImportResultsWrapper;
var
  fm: TEvoWaitFm;
begin
  Result := nil;
  fm := TEvoWaitFm.Create(FOwner);
  try
    fm.FConnection := FConn;
    fm.FLogger := FLogger;
    fm.SetText(Format('Please wait while Evolution imports %s records',[AnsiLowerCase(objname)]));
    fm.FTask := TEvoImportTask.Create(FConn, inputs, mapfileContent, fm.HandleTaskReady, fm.HandleTaskFailed, FLogger);
    case fm.ShowModal of
      mrYes:
        begin
          Assert(VarIsType(fm.FResult, varUnknown));
          Result := IInterface(fm.FResult) as IEvoXImportResultsWrapper;
        end;
      mrCancel: FLogger.LogWarning('User cancelled operation');
      mrNo: ;     //already logged an error
    else
      Assert(false);
    end
  finally
    FreeAndNil(fm);
  end;
  if Result = nil then
    Abort;
end;

function CreateGUIEvoXImportExecutor(conn: IEvoAPIConnection; Owner: TComponent; logger: ICommonLogger): IEvoXImportExecutor;
begin
  Result := TGUIEvoXImportExecutor.Create(conn, Owner, logger);
end;


end.
