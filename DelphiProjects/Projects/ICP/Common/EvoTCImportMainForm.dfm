inherited EvoTCImportMainFm: TEvoTCImportMainFm
  Left = 165
  Top = 165
  Caption = 'EvoTCImportMainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabSheet2: TTabSheet
      object Splitter1: TSplitter
        Left = 0
        Top = 421
        Width = 776
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object pnlTop: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn4: TBitBtn
          Left = 8
          Top = 8
          Width = 161
          Height = 25
          Action = ConnectToEvo
          Caption = 'Get Evolution and XXX data'
          TabOrder = 0
        end
      end
      inline EvolutionSinglePayrollFrame: TEvolutionSinglePayrollFrm
        Left = 0
        Top = 41
        Width = 776
        Height = 380
        Align = alClient
        TabOrder = 1
        inherited Splitter1: TSplitter
          Left = 521
          Height = 380
        end
        inherited EvoCompany: TEvolutionCompanyFrm
          Width = 521
          Height = 380
          inherited Splitter1: TSplitter
            Left = 249
            Height = 380
          end
          inherited frmCL: TEvolutionDsFrm
            Width = 249
            Height = 380
            inherited Panel1: TPanel
              Width = 249
            end
            inherited dgGrid: TReDBGrid
              Width = 249
              Height = 355
            end
          end
          inherited frmCO: TEvolutionDsFrm
            Left = 252
            Width = 269
            Height = 380
            inherited Panel1: TPanel
              Width = 269
            end
            inherited dgGrid: TReDBGrid
              Width = 269
              Height = 355
            end
          end
        end
        inherited frmPR: TEvolutionDsFrm
          Left = 524
          Width = 252
          Height = 380
          inherited Panel1: TPanel
            Width = 252
          end
          inherited dgGrid: TReDBGrid
            Width = 252
            Height = 355
          end
        end
      end
      object pnlBottom: TPanel
        Left = 0
        Top = 424
        Width = 776
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object RunAction: TAction
      Caption = 'Import timeclock data'
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution and XXX data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object RunEmployeeExport: TAction
      Caption = 'Update XYZ Employee Records for Selected Company'
    end
  end
end
