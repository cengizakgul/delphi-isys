object EvoAPIClientMainFm: TEvoAPIClientMainFm
  Left = 960
  Top = 185
  Width = 800
  Height = 550
  Caption = 'Evolution Blah-Blah-Blah'
  Color = clBtnFace
  Constraints.MinHeight = 550
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 792
    Height = 497
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    object TabSheet3: TTabSheet
      Caption = 'Connection settings'
      ImageIndex = 2
      inline EvoFrame: TEvoAPIConnectionParamFrm
        Left = 0
        Top = 0
        Width = 390
        Height = 106
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Import/Export'
      ImageIndex = 1
    end
    object tbshLog: TTabSheet
      Caption = 'Log'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 497
    Width = 792
    Height = 19
    Panels = <
      item
        Style = psOwnerDraw
        Width = -1
      end>
  end
end
