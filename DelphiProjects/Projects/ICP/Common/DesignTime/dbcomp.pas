unit dbcomp;

interface

uses
  classes, windows, Wwdbgrid, messages, controls, dbclient, graphics, db, wwdbigrd,
  types, grids, wwlocate, wwdotdot, wwdblook, wwdbcomb, stdctrls,
  dbctrls, wwDBDateTimePicker, wwradiogroup, menus, buttons;

type
  TGetSortFieldEvent = procedure( var aVisibleFieldNAme: string ) of object;

  TReDBGrid = class(TwwDBGrid)
  private
    FLocateDlg: Boolean;

    {functionality switchers}
    FSorting: Boolean;
    FRecordCountHintEnabled: boolean;

    {work data}
    FLastClickShift: TShiftState;
    FLastClickX: integer;
    FLastClickY: integer;

//locate dialog stuff
    FFieldNameComboBoxChange: TNotifyEvent;
    FFirstButtonClick: TNotifyEvent;
    FNextButtonClick: TNotifyEvent;

//filter stuff
    FilterBmp, FilteredBmp: TBitmap;
    FFilterDlg: Boolean;
    FUseCaseInsensitiveFilterExpression: Boolean;


    FOnGetSortField: TGetSortFieldEvent;
    procedure InitDialog(Dialog: TwwLocateDlg);
    procedure CloseDlgQuery(Sender: TObject; var CanClose: Boolean);

    procedure CMHintShow(var Message: TMessage); message CM_HINTSHOW;
    procedure SetSorting(const Value: Boolean);
    class function CreateTitleImageList: TImageList;
    procedure FieldNameComboBoxChange(Sender: TObject);
    procedure FirstButtonClick(Sender: TObject);
    procedure NextButtonClick(Sender: TObject);
    function  GetSortField(aVisibleFieldName: string): string;

    procedure SetFilterDlg(const Value: Boolean);
    procedure IndicatorButtonClick(Sender: TObject);
    procedure UpdateIndicatorButton;
    function  IsFiltered: Boolean;

  protected
    procedure DataChanged; override;

    procedure ExecLocateDlg;
    procedure DoTitleButtonClick(AFieldName: string); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;  X, Y: Integer); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoCalcTitleAttributes(AFieldName: string; AFont: TFont; ABrush: TBrush;
	     var FTitleAlignment: TAlignment); override;
    procedure DoCalcTitleImage(Sender: TObject; Field: TField;
         var TitleImageAttributes: TwwTitleImageAttributes); override;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;

    function LastClickWasOnData: boolean;
  published

    property LocateDlg: Boolean read FLocateDlg write FLocateDlg default True;
    property Sorting: Boolean read FSorting write SetSorting default True;
    property RecordCountHintEnabled: boolean read FRecordCountHintEnabled write FRecordCountHintEnabled default true;
    property OnGetSortField: TGetSortFieldEvent read FOnGetSortField write FOnGetSortField;

    property FilterDlg: Boolean read FFilterDlg write SetFilterDlg default True;
  {property overrides}
    property TitleButtons default True;
  end;

  TFixedVCLDBLookupComboBox = class(TDBLookupComboBox)
  protected
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  end;

  TreLabel = class(TLabel)
  private
    FInvalidate: Boolean;
  protected
    procedure Paint; override;
    function GetLabelText: string; override;
    procedure AdjustBounds; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Invalidate; override;
  end;

  TwwReverseSelectionRecordEvent =
     procedure (Grid: TwwDBGrid;  var Accept: boolean) of object;

  TReDBCheckGrid = class(TreDBGrid)
  private
    FSelIndictorButton: TSpeedButton;
    FSelPopupMenu: TPopupMenu;
    FPrevOnFilter: TFilterRecordEvent;
    FOldFiltered: Boolean;
    FFilterSetup: Boolean;
    FShowSelectedOnly: Boolean;
    FSelRecs: TList;
    FFltRecNo: Integer;
    FGrayMode: Boolean;
    FOnReverseSelectionRecords : TwwReverseSelectionRecordEvent;

    procedure FDropMenuDown(Sender: TObject);
    procedure FSelectAll(Sender: TObject);
    procedure FUnSelectAll(Sender: TObject);
    procedure FReverseSelection(Sender: TObject);
    procedure FShowSelection(Sender: TObject);
    procedure FilterSelection(DataSet: TDataSet; var Accept: Boolean);
    procedure ConvertBookMarksIntoRecNo(ARunFilting: Boolean);
    function  GetMultiselection: Boolean;
    procedure SetMultiselection(const Value: Boolean);
    procedure SetGrayMode(const Value: Boolean);

  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Scroll(Distance: Integer); override;
    function  HighlightCell(DataCol, DataRow: Integer; const Value: string; AState: TGridDrawState): Boolean; override;
    procedure DrawSelCheckBox(ARect: TRect; ACol, ARow: integer; val: boolean);
//    function  MakeInfoHint: string; override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure SelectAll; reintroduce; virtual;
    procedure UnSelectAll; override;
    procedure ReverseSelection; virtual;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    property  MultiSelection: Boolean read GetMultiselection write SetMultiselection;
    property  GrayMode: Boolean read FGrayMode write SetGrayMode;
  published
    property OnMultiSelectReverseRecords : TwwReverseSelectionRecordEvent read FOnReverseSelectionRecords write FOnReverseSelectionRecords;

  end;

type
//To continue enumeration, the callback function must return true;
//to stop enumeration, it must return false
  TOnSelected = function( ds: TDataSet ): boolean of object;
  TOnSelectedProc = function( ds: TDataSet; par: TObject ): boolean;
  TfsOption = (fsSavePosition, fsDisableControls, fsForCurrentIfListIsEmpty);
  TfsOptions = set of TfsOption;
// return false if enumeration was aborted by callback
// if (grid.selectionlist.count = 0) and (fsForCurrentIfListIsEmpty in anOptions) then calls callback for current record

function ForSelected( grid: TreDbGrid; selproc: TOnSelectedProc; par: TObject; anOptions: TfsOptions = [] ): boolean; overload;
function ForSelected( grid: TreDbGrid; selevent: TOnSelected; anOptions: TfsOptions = []  ): boolean; overload;

implementation
uses
  dialogs, gdyCommon, sysutils, forms{for TCMHintShow}, gdydbcommon,
  typinfo, imglist, variants, SFilterDialog, kbmMemTable;

// All these resources exist in isBasicClasses.res (Aleksey)
// If you need to use dbcomp.res - rename IDs
 {$R *.res }

function ShadeColor(InColor : TColor; ShadingDepth: Integer) : TColor;
begin
 Result := ColorToRGB(InColor);
 Result :=
  ((Result mod 256) * ShadingDepth div 6) +
  ((Result div 256 mod 256) * ShadingDepth div 6) * 256 +
  ((Result div 256 div 256 mod 256) * ShadingDepth div 6) * 256 * 256;
end;

{ TReDBGrid }

var //even not threadvar
  FReDBGrid_TitleImageList: TImageList;

procedure TReDBGrid.CMHintShow(var Message: TMessage);
resourcestring
  sHintFormat = 'Record Count: %d';
begin
  if RecordCountHintEnabled and ActiveDS( DataSource ) then
    if trim( TCMHintShow(Message).HintInfo^.HintStr ) <> '' then
      TCMHintShow(Message).HintInfo^.HintStr := Format( shintformat, [DataSource.DataSet.RecordCount] ) + '; '+TCMHintShow(Message).HintInfo^.HintStr
    else
      TCMHintShow(Message).HintInfo^.HintStr := Format( shintformat, [DataSource.DataSet.RecordCount] );

  inherited;
end;

constructor TReDBGrid.Create(AOwner: TComponent);
begin
  inherited;
  FLocateDlg := true;
  FSorting := true;
  FRecordCountHintEnabled := true;

  {overriden properties}
  TitleButtons := true;

  FilterBmp := TBitmap.Create;
  FilteredBmp := TBitmap.Create;
  FilterBmp.LoadFromResourceName(HInstance, 'FILTER_RE');
  FilteredBmp.LoadFromResourceName(HInstance, 'FILTERED_RE');
  FilterDlg := True;
end;

procedure TReDBGrid.DataChanged;
var
  i: integer;
  affected: boolean;
begin
//IP bugfix
  if AssignedDS(DataSource) then //deletes bookmarks in closed DS too
  begin
//    if not DataSource.DataSet.Active then
//      ODS('TReDBGrid.DataChanged: dsassigned: ds is not active ');
    affected := false;
    for i := SelectedList.Count-1 downto 0 do
      if not DataSource.DataSet.BookmarkValid( SelectedList[i]) then
      begin
        DataSource.DataSet.FreeBookmark( SelectedList[i] );
        SelectedList.Delete( i );
        affected := true;
      end;
    if affected then
      Invalidate;
  end;
  inherited;
end;

destructor TReDBGrid.Destroy;
begin
  inherited;
  FreeAndNil(FilterBmp);
  FreeAndNil(FilteredBmp);
end;

procedure TReDBGrid.DoTitleButtonClick(AFieldName: string);
begin
  if Sorting and assignedDS(DataSource) and IsSortableDS(DataSource.DataSet) then
    ResortRequested( DataSource.DataSet, GetSortField(AFieldName), ssCtrl in FLastClickShift );
  inherited;
end;

procedure TReDBGrid.ExecLocateDlg;
var
  i: Integer;
  Field: TField;
  LocateDlg: TwwLocateDialog;
  a: array of boolean;
begin
  LocateDlg := TwwLocateDialog.Create(Self);
  try
    if ActiveDS(DataSource) then
    begin
      LocateDlg.FieldSelection := fsVisibleFields;
      LocateDlg.DataSource := DataSource;
      if Selected.Count > 0 then
      begin
        DataSource.DataSet.DisableControls;
        try
          SetLength(a, DataSource.DataSet.FieldCount);
          for i := 0 to Pred(DataSource.DataSet.FieldCount) do
          begin
            a[i] := DataSource.DataSet.Fields[i].Visible;
            DataSource.DataSet.Fields[i].Visible := False;
          end;
          for i := 0 to Pred(Selected.Count) do
          begin
            Field := DataSource.DataSet.FieldByName(Copy(Selected[i], 1, Pred(Pos(#9, Selected[i]))));
            if Assigned(Field) then
            begin
              if Columns[i].DisplayLabel <> '' then
                Field.DisplayLabel := Columns[i].DisplayLabel;
              Field.Visible := True;
            end;
          end;
        finally
          DataSource.DataSet.EnableControls;
        end;
      end;

      if GetActiveField <> nil then
        LocateDlg.SearchField := GetActiveField.FieldName
      else
        LocateDlg.SearchField := '';
      LocateDlg.OnInitDialog := InitDialog;
      try
        LocateDlg.Execute;
      finally
        if Selected.Count > 0 then
        begin
          DataSource.DataSet.DisableControls;
          try
            for i := 0 to Pred(DataSource.DataSet.FieldCount) do
              DataSource.DataSet.Fields[i].Visible := a[i];
          finally
            DataSource.DataSet.EnableControls;
          end;
        end;
      end;
    end;
  finally
    LocateDlg.Free;
  end;
end;

procedure TReDBGrid.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if FLocateDlg and (Char(Key) = 'F') and (Shift = [ssCtrl]) then
    ExecLocateDlg;
  if FFilterDlg and Assigned(IndicatorButton) and (Char(Key) = 'R') and (Shift = [ssCtrl]) then
    IndicatorButton.Click;
  inherited;
end;

procedure TReDBGrid.SetSorting(const Value: Boolean);
begin
  FSorting := Value;
  if Value then
    TitleButtons := True;
end;

procedure TReDBGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  FLastClickShift := Shift;
  FLastClickX := X;
  FLastClickY := Y;
  inherited;
end;

procedure TReDBGrid.DoCalcTitleAttributes(AFieldName: string; AFont: TFont;
  ABrush: TBrush; var FTitleAlignment: TAlignment);
var
  sortOnFields, sortDescOnFields: string;
  realSortFieldName: string;
begin
//  ODS( 'DoCalcTitleAttributes');
  if Sorting and AssignedDS(DataSource) and IsSortableDS(DataSource.DataSet) then
  begin
    realSortFieldName := GetSortField(AFieldName);
    if not CanSortOnField( DataSource.DataSet.FieldByName(realSortFieldName) ) then
      ABrush.Color := ShadeColor(ABrush.Color, 5)
    else
    begin
      GetCurrentSorting( DataSource.DataSet, sortOnFields, sortDescOnFields );
      if Pos(';' + realSortFieldName + ';', ';' + sortOnFields +';'+ sortDescOnFields + ';') <> 0 then
      begin
        AFont.Color := clBlue;
        ABrush.Color := clYellow;
      end;
    end;
  end;
  inherited;
end;

procedure TReDBGrid.DoCalcTitleImage(Sender: TObject; Field: TField;
  var TitleImageAttributes: TwwTitleImageAttributes);
var
  sortOnFields, sortDescOnFields: string;
  realSortFieldName: string;
begin
  if Sorting and AssignedDS(DataSource) and IsSortableDS(DataSource.DataSet) then
  begin
    realSortFieldName := GetSortField(Field.FieldName);
    TitleImageAttributes.Alignment := taRightJustify;
    GetCurrentSorting( DataSource.DataSet, sortOnFields, sortDescOnFields );
    if Pos( ';' + realSortFieldName + ';', ';' + sortDescOnFields + ';') <> 0 then
      TitleImageAttributes.ImageIndex := 0
    else if Pos( ';' + realSortFieldName + ';', ';' + sortOnFields + ';') <> 0 then
      TitleImageAttributes.ImageIndex := 1
    else
      TitleImageAttributes.ImageIndex := -1;
  end;
  inherited;

end;

class function TReDBGrid.CreateTitleImageList: TImageList;
begin
  Result := TImageList.Create(nil);
  with Result do
  begin
    Height := 8;
    Width := 8;
    GetInstRes(HInstance, rtBitmap, 'SORT_UP_RE', 8, [lrTransparent], clWhite);
    GetInstRes(HInstance, rtBitmap, 'SORT_DOWN_RE', 8, [lrTransparent], clWhite);
  end;
end;

procedure TReDBGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);
var
  t: TImageList;
begin
  if Sorting and AssignedDS(DataSource) and IsSortableDS(DataSource.DataSet) then
  begin
    t := TitleImageList;
    TitleImageList := FReDBGrid_TitleImageList;
    try
      inherited;
    finally
      TitleImageList := t;
    end;
  end
  else
    inherited;
end;


procedure TReDBGrid.InitDialog(Dialog: TwwLocateDlg);
begin
  Dialog.OnCloseQuery := CloseDlgQuery;
  FFieldNameComboBoxChange := Dialog.FieldNameComboBox.OnChange;
  Dialog.FieldNameComboBox.OnChange := FieldNameComboBoxChange;
  FFirstButtonClick := Dialog.FirstButton.OnClick;
  Dialog.FirstButton.OnClick := FirstButtonClick;
  FNextButtonClick := Dialog.NextButton.OnClick;
  Dialog.NextButton.OnClick := NextButtonClick;
end;

procedure TreDBGrid.FieldNameComboBoxChange(Sender: TObject);
var
  s: string;
begin
  s := TwwLocateDlg(TComboBox(Sender).Owner).SearchValue.Text;
  try
    if Assigned(FFieldNameComboBoxChange) then
      FFieldNameComboBoxChange(Sender);
  finally
    TwwLocateDlg(TComboBox(Sender).Owner).SearchValue.Text := s;
  end;
end;

procedure TreDBGrid.FirstButtonClick(Sender: TObject);
begin
  if Assigned(FFirstButtonClick) then
    FFirstButtonClick(Sender);
  if TwwLocateDlg(TButton(Sender).Owner).ModalResult = mrOK then
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= False;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= True;
  end
  else
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= True;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= False;
  end;
end;

procedure TreDBGrid.NextButtonClick(Sender: TObject);
begin
  if Assigned(FNextButtonClick) then
    FNextButtonClick(Sender);
  if TwwLocateDlg(TButton(Sender).Owner).ModalResult = mrOK then
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= False;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= True;
  end
  else
  begin
    TwwLocateDlg(TButton(Sender).Owner).FirstButton.Default:= True;
    TwwLocateDlg(TButton(Sender).Owner).NextButton.Default:= False;
  end;
end;

procedure TReDBGrid.CloseDlgQuery(Sender: TObject; var CanClose: Boolean);
begin
  with Sender as TForm do
    if ModalResult = mrOK then
    begin
      CanClose := False;
      ModalResult := mrNone;
    end;
end;

function TReDBGrid.LastClickWasOnData: boolean;
begin
  with MouseCoord( FLastClickX, FLastClickY ) do
    Result := (x <> 0) and (y<>0);
end;

function TReDBGrid.GetSortField(aVisibleFieldName: string): string;
begin
  Result := aVisibleFieldName;
  if Assigned(FOnGetSortField) then
    FOnGetSortField( Result );
end;

procedure TReDBGrid.SetFilterDlg(const Value: Boolean);
begin
//  if FFilterDlg <> Value then
  begin
    FFilterDlg := Value;
    if not (csDesigning in ComponentState) then
    begin
      if Value and (dgIndicator in Options) then
      begin
        IndicatorButton := TwwIButton.Create(Self);
        UpdateIndicatorButton;
        IndicatorButton.Parent := Self;
        IndicatorButton.Width := Succ(IndicatorButtonWidth);
        IndicatorButton.Height := Succ(RowHeights[0]);
        IndicatorButton.OnClick := IndicatorButtonClick;
      end
      else
      begin
        IndicatorButton.Free;
        IndicatorButton := nil;
      end;
      Invalidate;
    end;
  end;
end;

procedure TReDBGrid.IndicatorButtonClick(Sender: TObject);
var
  TempDialog: TFilterDialog;
  TempBookmark: TBookMark;
  s, s1: string;
  t: TStringList;
  i: Integer;
begin
  inherited;
  if not ActiveDS(DataSource) then
    exit;

  if DataSource.DataSet.State in [dsEdit, dsInsert] then
  begin
    MessageDlg('You have to Post or Cancel your changes first', mtError, [mbOk], 0);
    Exit;
  end;

  if not DataSource.DataSet.Filtered then
    S := ''
  else
    S := DataSource.DataSet.Filter;

  if DataSource.DataSet is TkbmCustomMemTable then  //just like TISkbmDataSet
    s := StringReplace(StringReplace(s, '*''', '%''',  [rfReplaceAll]), '''*', '''%',  [rfReplaceAll]);

  TempDialog := TFilterDialog.Create(Self);
  try
    if Selected.Count = 0 then
    begin
      t := TStringList.Create;
      try
        for i := 0 to Pred(DataSource.DataSet.FieldCount) do
          if DataSource.DataSet.Fields[i].Visible then
            t.Add(DataSource.DataSet.Fields[i].FieldName + #9#9 + DataSource.DataSet.Fields[i].DisplayName);
        TempDialog.Initialize(S, t, DataSource.DataSet);
      finally
        t.Free;
      end;
    end
    else
      TempDialog.Initialize(S, Selected, DataSource.DataSet);

    if TempDialog.ShowModal = mrOK then
    begin
      UnselectAll; //filter change messes with bookmarks
      TempBookmark := DataSource.DataSet.GetBookmark;
      try
        DataSource.DataSet.FilterOptions := DataSource.DataSet.FilterOptions + [foCaseInsensitive];

        s1 := TempDialog.Filter(FUseCaseInsensitiveFilterExpression);
        if DataSource.DataSet is TkbmCustomMemTable then //just like TISkbmDataSet
          s1 := StringReplace(StringReplace(s1, '%''', '*''', [rfReplaceAll]), '''%', '''*', [rfReplaceAll]);
        DataSource.DataSet.Filter := s1;

        DataSource.DataSet.Filtered := False;
        DataSource.DataSet.Filtered := (Length(DataSource.DataSet.Filter) > 0);

        try
          DataSource.DataSet.GotoBookmark(TempBookmark);
        except
          on EDatabaseError do
            DataSource.DataSet.First
          else
            raise;
        end;
//          IndicatorButton.Down := DataSet.Filtered;
        UpdateIndicatorButton;
      finally
        DataSource.DataSet.FreeBookmark(TempBookmark);
      end;
    end;
  finally
    TempDialog.Free;
  end;
end;

function TReDBGrid.IsFiltered: Boolean;
begin
  Result :=  ActiveDS(DataSource) and DataSource.DataSet.Filtered and (trim(DataSource.DataSet.Filter) <> '');
end;

procedure TReDBGrid.UpdateIndicatorButton;
begin
  if Assigned(IndicatorButton) then
    if IsFiltered then
      IndicatorButton.Glyph.Assign(FilteredBmp)
    else
      IndicatorButton.Glyph.Assign(FilterBmp);
end;

{ TreLabel }

procedure TreLabel.AdjustBounds;
begin
  if FInvalidate and not ((Copy(Caption, 1, 1) = '~') and (Font.Color <> clMaroon) and  (Font.Style <> [fsBold])) then
    inherited;
end;

constructor TreLabel.Create(AOwner: TComponent);
begin
  FInvalidate := True;
  inherited;
end;

function TreLabel.GetLabelText: string;
begin
  if Copy(Caption, 1, 1) = '~' then
    Result := Copy(Caption, 2, Length(Caption))
  else
    Result := inherited GetLabelText;
end;

procedure TreLabel.Invalidate;
begin
  if FInvalidate then
    inherited;
end;

procedure TreLabel.Paint;
var
  c: TColor;
  s: TFontStyles;
begin
  if Copy(Caption, 1, 1) = '~' then
  begin
    FInvalidate := False;
    c := Font.Color;
    s := Font.Style;
    Font.Color := clMaroon;
    Font.Style := [fsBold];
    try
      inherited;
    finally
      Font.Color := c;
      Font.Style := s;
      FInvalidate := True;
    end;
  end
  else
    inherited;
end;

procedure TFixedVCLDBLookupComboBox.WMKeyDown(var Message: TWMKeyDown);
begin
  if (NullValueKey <> 0) and CanModify and (NullValueKey = ShortCut(Message.CharCode,
     KeyDataToShiftState(Message.KeyData))) then
  begin
    if assigned(DataLink) and assigned(Field) then
    begin
      DataLink.Edit;
      Field.Clear;
    end
    else
    begin
      KeyValue := Null;
      Click;
    end;
    Message.CharCode := 0;
  end;
  inherited;
end;

{ TReDBCheckGrid }

procedure TReDBCheckGrid.ConvertBookMarksIntoRecNo(ARunFilting: Boolean);
var
  i: Integer;
  BM: TBookmark;
  flt: Boolean;
begin
  FSelRecs := TList.Create;

  try
    FSelRecs.Count := SelectedList.Count;
    BM := DataLink.DataSet.GetBookmark;
    DataLink.DataSet.DisableControls;
    try
      Flt := DataLink.DataSet.Filtered;
      DataLink.DataSet.Filtered := False;
      for i := 0 to SelectedList.Count - 1 do
      begin
        DataLink.DataSet.GotoBookmark(SelectedList[i]);
        FSelRecs[i] := Pointer(DataLink.DataSet.RecNo);
      end;

      FFltRecNo := 0;
      if ARunFilting then
        DataLink.DataSet.Filtered := True
      else
        DataLink.DataSet.Filtered := Flt;

      if DataLink.DataSet.BookmarkValid(BM) then
        DataLink.DataSet.GotoBookmark(BM);

    finally
      DataLink.DataSet.EnableControls;
      DataLink.DataSet.FreeBookmark(BM);
    end;

  finally
    FSelRecs.Free;
    FSelRecs := nil;
  end;  
end;

const
  cIndColWidth = 26;

constructor TReDBCheckGrid.Create(AOwner: TComponent);
var
  MI: TMenuItem;
begin
  inherited;

  FSelPopupMenu := TPopupMenu.Create(Self);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'Select All';
  MI.OnClick := FSelectAll;
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'UnSelect All';
  MI.OnClick := FUnSelectAll;
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'Reverse Selection';
  MI.OnClick := FReverseSelection;
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := '-';
  FSelPopupMenu.Items.Add(MI);
  MI := TMenuItem.Create(FSelPopupMenu);
  MI.Caption := 'Show Selected Only';
  MI.OnClick := FShowSelection;
  MI.Enabled := False;
  FSelPopupMenu.Items.Add(MI);

  if not (csDesigning in ComponentState) then
    IndicatorButton.Width := cIndColWidth div 2;

  FSelIndictorButton := TSpeedButton.Create(Self);
  FSelIndictorButton.Visible := False;
  FSelIndictorButton.Parent := Self;
  FSelIndictorButton.OnClick := FDropMenuDown;
  FSelIndictorButton.Glyph.LoadFromResourceName(HInstance, 'SELECT_RE');
  FSelIndictorButton.Margin := 0;

  FFilterSetup := False;
  FShowSelectedOnly := False;

  Options := Options - [dgEditing];
  MultiSelectOptions := MultiSelectOptions - [msoAutoUnSelect] + [msoShiftSelect];
  ReadOnly := True;

  MultiSelection := True;
  FGrayMode := False;
end;


procedure TReDBCheckGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);
var
  DF, Sel: Boolean;
  R, R2: TRect;
  OldActive: Integer;
begin
  if FGrayMode and (SelectedList.Count > 0) then
  begin
    FGrayMode := False;
    invalidate;
  end;

  if (gdFixed in AState) and (ARow > 0) and (dgMultiSelect in Options) then
  begin
    R := ARect;
    R.Top := R.Top + 3;
    R.Bottom := R.Bottom - 3;
    R.Right := R.Right - 3;
    R.Left := R.Right - (R.Bottom - R.Top);
    R2 := ARect;
    R2.Right := R.Left;

    Canvas.Brush.Color := TitleColor;
    Canvas.FillRect(ARect);

    DF :=  DefaultDrawing;
    try
      DefaultDrawing := False;
      inherited DrawCell(ACol, ARow, R2, AState);
    finally
      DefaultDrawing := DF;
    end;

    if DataLink.Active then
    begin
      OldActive := DataLink.ActiveRecord;
      try
        DataLink.ActiveRecord := ARow - 1;
        if FShowSelectedOnly then
          Sel := not DataLink.DataSet.IsEmpty
        else
          Sel := IsSelectedRecord;
      finally
        DataLink.ActiveRecord := OldActive;
      end;
    end
    else
      Sel := False;

    DrawSelCheckBox(R, ACol, ARow, Sel);

    if DefaultDrawing then
      Draw3DLines(ARect, ACol, ARow, AState);
  end

  else
    inherited;
end;


procedure TReDBCheckGrid.DrawSelCheckBox(ARect: TRect; ACol, ARow: integer; val: boolean);
begin
  Canvas.Pen.Width:= 1;
  if FGrayMode then
    Canvas.Brush.Color := $00E2E6E7
  else
    Canvas.Brush.Color := clWindow;

  { Draw checkbox frame }
  Canvas.FillRect(ARect);
  Canvas.Pen.Color:= clBlack;
  Canvas.MoveTo(ARect.right-1, ARect.Top);
  Canvas.LineTo(ARect.left, ARect.Top);
  Canvas.LineTo(ARect.left, ARect.Bottom+1);

  Canvas.Pen.Color:= clGrayText;
  Canvas.MoveTo(ARect.left+1, ARect.Bottom);
  Canvas.LineTo(ARect.right, ARect.Bottom);
  Canvas.LineTo(ARect.right, ARect.Top-1);

  Canvas.Pen.Color:= clWhite;
  Canvas.MoveTo(ARect.left, ARect.Bottom+1);
  Canvas.LineTo(ARect.right+1, ARect.Bottom+1);
  Canvas.LineTo(ARect.right+1, ARect.Top-1);

  Canvas.Pen.Color:= clGray;
  Canvas.MoveTo(ARect.right, ARect.Top-1);
  Canvas.LineTo(ARect.left-1, ARect.Top-1);
  Canvas.LineTo(ARect.left-1, ARect.Bottom+1);

  if val or FGrayMode then
  begin
    if FGrayMode then
      Canvas.Pen.Color:= clGrayText
    else
      Canvas.Pen.Color:= clBlue;

   { Draw checkbox lines }

    Canvas.Pen.Width:=1;
    Canvas.MoveTo(ARect.Left+2,ARect.Top+8 div 2);
    Canvas.LineTo(ARect.Left+2,ARect.Bottom-3);
    Canvas.MoveTo(ARect.Left+3,ARect.Top+10 div 2);
    Canvas.LineTo(ARect.Left+3,ARect.Bottom-2);
    Canvas.MoveTo(ARect.Left+4,ARect.Top+12 div 2);
    Canvas.LineTo(ARect.Left+4,ARect.Bottom-1);
    Canvas.MoveTo(ARect.Left+5,ARect.Top+10 div 2);
    Canvas.LineTo(ARect.Left+5,ARect.Bottom-2);
    Canvas.MoveTo(ARect.Left+6,ARect.Top+8 div 2);
    Canvas.LineTo(ARect.Left+6,ARect.Bottom-3);
    Canvas.MoveTo(ARect.Left+7,ARect.Top+6 div 2);
    Canvas.LineTo(ARect.Left+7,ARect.Bottom-4);
    Canvas.MoveTo(ARect.Left+8,ARect.Top+4 div 2);
    Canvas.LineTo(ARect.Left+8,ARect.Bottom-5);
  end;

  if Canvas<>Canvas then begin
    Canvas.CopyMode := cmSrcCopy;
    InflateRect(ARect, 1, 1);
    ARect.right:= ARect.right + 1;
    ARect.bottom:= ARect.bottom + 1;
    Canvas.CopyRect(ARect, Canvas, ARect);
  end
end;


procedure TReDBCheckGrid.FDropMenuDown(Sender: TObject);
var
  P: TPoint;
begin
  if DataLink.Active then
  begin
    P := FSelIndictorButton.ClientToScreen(Point(0, FSelIndictorButton.Height));
    FSelPopupMenu.Popup(P.X, P.Y);
  end;
end;

procedure TReDBCheckGrid.FilterSelection(DataSet: TDataSet; var Accept: Boolean);
begin
  Inc(FFltRecNo);

  if Assigned(FPrevOnFilter) then
    FPrevOnFilter(DataSet, Accept);

  if Accept and FShowSelectedOnly and Assigned(FSelRecs) then
  begin
    Accept := FSelRecs.IndexOf(Pointer(FFltRecNo)) <> -1;
    if FFltRecNo = DataSet.RecordCount then
      FFltRecNo := 0;
  end;
end;

procedure TReDBCheckGrid.FReverseSelection(Sender: TObject);
begin
  ReverseSelection;
end;


procedure TReDBCheckGrid.ReverseSelection;
var
  Sel: TList;
  saveBK, BM: TBookmark;
  i: integer;
  fl: Boolean;
  Accept : boolean;
begin
   If Assigned(FOnReverseSelectionRecords) then
   begin
      Accept:= True;
      FOnReverseSelectionRecords(Self, Accept);
      if not Accept then exit;
   end;

   Sel := TList.Create;
   try
     with DataSource.Dataset do
     begin
       for i:= 0 to SelectedList.Count-1 do
         Sel.Add(SelectedList.Items[i]);

       SelectedList.Clear;

       saveBK := GetBookmark;
       DisableControls;
       First;
       while (not Eof) do
       begin
         BM := GetBookmark;
         fl := False;
         for i := 0 to Sel.Count - 1 do
           if CompareBookmarks(BM, Sel[i]) = 0 then
           begin
             fl := True;
             FreeBookmark(Sel[i]);
             Sel.Delete(i);
             break;
           end;

           if not fl then
             SelectedList.Add(BM);

         Next;
       end;

       for i := 0 to Sel.Count - 1 do
         FreeBookmark(Sel[i]);

       GotoBookmark(saveBK);
       Freebookmark(saveBK);
       EnableControls;
     end;

   finally
     Sel.Free;
   end;
end;

procedure TReDBCheckGrid.FSelectAll(Sender: TObject);
begin
  SelectAll;
end;

procedure TReDBCheckGrid.FShowSelection(Sender: TObject);
begin
  TMenuItem(Sender).Checked := not TMenuItem(Sender).Checked;
  FShowSelectedOnly := TMenuItem(Sender).Checked;

  if FShowSelectedOnly and not FFilterSetup then
  begin
    DataLink.DataSet.DisableControls;
    try
      FPrevOnFilter := DataLink.DataSet.OnFilterRecord;
      FOldFiltered := DataLink.DataSet.Filtered;
      DataLink.DataSet.OnFilterRecord := FilterSelection;
      ConvertBookMarksIntoRecNo(True);
      FFilterSetup := True;
    finally
      DataLink.DataSet.EnableControls;
    end;
  end

  else if FFilterSetup then
  begin
    DataLink.DataSet.DisableControls;
    try
      DataLink.DataSet.OnFilterRecord := FPrevOnFilter;
      DataLink.DataSet.Filtered := False;
      DataLink.DataSet.Filtered := FOldFiltered;
      FFilterSetup := False;
    finally
      DataLink.DataSet.EnableControls;
    end;
  end;
end;

procedure TReDBCheckGrid.FUnSelectAll(Sender: TObject);
begin
  UnSelectAll;
end;

function TReDBCheckGrid.GetMultiselection: Boolean;
begin
  Result := dgMultiSelect in Options;
end;

function TReDBCheckGrid.HighlightCell(DataCol, DataRow: Integer; const Value: string; AState: TGridDrawState): Boolean;
begin
  Result := (gdSelected in AState) and ((dgAlwaysShowSelection in Options) or  Focused);
end;


procedure TReDBCheckGrid.KeyDown(var Key: Word; Shift: TShiftState);
var
  R: TRect;
begin
  if (Key = VK_SPACE) and not (dgEditing in Options) then
  begin
    R := CellRect(Col, Row);
    MouseDown(mbLeft, Shift+[ssCtrl], R.Left + 1, R.Top + 1);
  end;

  inherited;
end;

{
const
  sRecSelectedHintFormat = 'Selected Records: %d';

function TReDBCheckGrid.MakeInfoHint: string;
var
  h: String;
begin
  Result := inherited MakeInfoHint;

  if MultiSelection and
     RecordCountHintEnabled and  Assigned(DataSource) and
     Assigned(DataSource.DataSet) and DataSource.DataSet.Active then
  begin
    h := Format(sRecSelectedHintFormat, [SelectedList.Count]);

    Result := Trim(Result);
    if Length(Result) > 0 then
      Result := Result + #13 + h
    else
      Result := h;
  end;
end;
}

procedure TReDBCheckGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  GC: TGridCoord;
  RunInh: Boolean;
begin
  RunInh := True;

  if Button = mbLeft then
  begin
    GC := MouseCoord(X, Y);
    if (GC.X = 0) and (GC.Y > 0) then
      if not (ssCtrl in Shift) then
      begin
        if not (ssShift in Shift) then
          Shift := Shift + [ssCtrl];
        inherited MouseDown(Button, Shift, IndicatorButtonWidth + 1, Y);
        RunInh := False;
      end;
  end;

  if RunInh then
    inherited;
end;


procedure TReDBCheckGrid.Scroll(Distance: Integer);
{ar
  NewRect: TRect;
  RowHeight: Integer;}
begin
{ if Distance <> 0 then
  begin
    NewRect := BoxRect(0, Row, 0, Row);
    InvalidateRect(Handle, @NewRect, False);

    if Abs(Distance) <= VisibleRowCount then
    begin
      NewRect := BoxRect(0, 1, 0, 1000);
      RowHeight := DefaultRowHeight;
      if dgRowLines in Options then Inc(RowHeight);
      ScrollWindowEx(Handle, 0, -RowHeight * Distance, @NewRect, @NewRect,
        0, nil, SW_Invalidate);
    end;
  end;
 }
  inherited;
end;


procedure TReDBCheckGrid.SelectAll;
begin
  FGrayMode := False;
  inherited SelectAll;
end;

procedure TReDBCheckGrid.SetGrayMode(const Value: Boolean);
begin
  if Value then
    UnSelectAll;
  FGrayMode := Value;    
  Invalidate;
end;

procedure TReDBCheckGrid.SetMultiselection(const Value: Boolean);
begin
  if Value then
  begin
    Options := Options + [dgMultiSelect];
    if not (csDesigning in ComponentState) then
    begin
      IndicatorButton.Width := cIndColWidth div 2;
      FSelIndictorButton.Width := IndicatorButton.Width;
      FSelIndictorButton.Height := IndicatorButton.Height;
      FSelIndictorButton.Top := IndicatorButton.Top;
      FSelIndictorButton.Left := IndicatorButton.Width;
      FSelIndictorButton.Visible := True;
    end;
    IndicatorButtonWidth := cIndColWidth;
    LayoutChanged;
  end
  else
  begin
    Options := Options - [dgMultiSelect];
    FSelIndictorButton.Visible := False;
    if not (csDesigning in ComponentState) then
      IndicatorButtonWidth := IndicatorButton.Width - 1;
    if SelectedList.Count > 0 then
      UnselectAll;
    LayoutChanged;
  end;
end;


procedure TReDBCheckGrid.UnSelectAll;
begin
  FGrayMode := False;
  try
    inherited;  //To prevent poping of exception SParentRequired when it trys to draw something on destruction
  except
  end;
end;

function ForSelectedInternal( ds: TDataSet; list: TList; selproc: TOnSelectedProc; selEvent: TOnSelected; par: TObject; anOptions: TfsOptions ): boolean;

  function CallProcBack: boolean;
  begin
    if assigned( selproc ) then
      Result := selproc( ds, par )
    else if assigned( selevent ) then
      Result := selevent( ds )
    else
      Result := true;
  end;

  function DoForSelected: boolean;
  var
    curPos: TBookmark;
    i: integer;
  begin
    Result := true;
    ds.First;
    while not ds.Eof do
    begin
      curPos := ds.GetBookmark;
      try
        for i := 0 to list.Count-1 do
          if ds.CompareBookmarks( curPos, list[i] ) = 0 then
          begin
            Result := Result and CallProcBack;
            if not Result then
              Exit;
          end;
      finally
        ds.FreeBookmark( curPos );
      end;
      ds.Next;
    end;
  end;

  function HandleEmptyListAndDoForSelected: boolean;
  begin
    if assigned( list ) and (list.Count > 0) then
      Result := DoForSelected
    else if fsForCurrentIfListIsEmpty in anOptions then
      Result := CallProcBack
    else
      Result := true;
  end;

  function DisableControlsAndDoForSelected: boolean;
  begin
    if fsDisableControls in anOptions then
    begin
      ds.DisableControls;
      try
        Result := HandleEmptyListAndDoForSelected;
      finally
        ds.EnableControls;
      end;
    end
    else
      Result := HandleEmptyListAndDoForSelected;
  end;
var
  savePos: TBookmark;
begin
  if  assigned( ds ) and
     ( assigned( selproc ) or assigned( selevent ) ) then
  begin
    if fsSavePosition in anOptions then
    begin
      savePos := ds.GetBookmark;
      try
        try
          Result := DisableControlsAndDoForSelected;
        finally
          if ds.BookmarkValid( savePos ) then
            ds.GotoBookmark( savePos );
        end;
      finally
        ds.FreeBookmark( savePos );
      end;
    end
    else
      Result := DisableControlsAndDoForSelected;
  end
  else
    Result := true; //it wasn't aborted by callback
end;

function ForSelectedInGrid( grid: TreDbGrid; selproc: TOnSelectedProc; selEvent: TOnSelected; par: TObject; anOptions: TfsOptions  ): boolean;
begin
  if assigned( grid ) and assigned( grid.DataSource ) then
    Result := ForSelectedInternal( grid.DataSource.DataSet, grid.SelectedList, selproc, selevent, par, anOptions )
  else
    Result := true; //it wasn't aborted by callback
end;

function ForSelected( grid: TreDbGrid; selproc: TOnSelectedProc; par: TObject; anOptions: TfsOptions ): boolean;
begin
  Result := ForSelectedInGrid( grid, selproc, nil, par, anOptions );
end;

function ForSelected( grid: TreDbGrid; selevent: TOnSelected; anOptions: TfsOptions ): boolean;
begin
  Result := ForSelectedInGrid( grid, nil, selevent, nil, anOptions );
end;


initialization
  FReDBGrid_TitleImageList := TReDBGrid.CreateTitleImageList;

finalization
  FreeAndNil( FReDBGrid_TitleImageList );

end.

