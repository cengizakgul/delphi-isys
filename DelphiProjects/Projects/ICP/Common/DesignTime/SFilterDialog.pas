// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SFilterDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, ExtCtrls, Db, StrUtils;

type
  TFilterDialog = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label2: TLabel;
    StringGrid1: TStringGrid;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Label1: TLabel;
    Label3: TLabel;
    cbExact: TCheckBox;
    cbNot: TCheckBox;
    procedure StringGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    CurrentFilter: string;
    CurrentFieldName: TStringList;
    DataSet: TDataSet;
    function GetFieldName(const i: Integer): string;
    function GetCaption(const i: Integer): string;
  public
    { Public declarations }
    procedure Initialize(var aCurrentFilter: string; aCurrentFieldName: TStrings; aDataSet: TDataSet);
    function Filter(const ACaseInsensitiveExpression: Boolean = False): string;
  end;

var
  FilterDialog: TFilterDialog;

implementation

{$R *.DFM}

procedure TFilterDialog.Initialize(var aCurrentFilter: string; aCurrentFieldName: TStrings; aDataSet: TDataSet);
var
  I, J: integer;
  S, N, E: string;
  X, XL, L: integer;
begin
  CurrentFilter := aCurrentFilter;
  CurrentFieldName.Assign(aCurrentFieldName);
  DataSet := aDataSet;

  StringGrid1.RowCount := 1;

  for I := 0 to CurrentFieldName.Count - 1 do
  begin
    StringGrid1.RowCount := Succ(i);
    StringGrid1.Cells[0, I] := GetCaption(I);
  end;

  S := CurrentFilter;

  cbExact.Checked := False;
  cbNot.Checked := False;
  if Copy(S, 1, 8) = '1=1 AND ' then
  begin
    S := Copy(S, 9, Length(S));
    cbExact.Checked := True;
  end
  else if Copy(S, 1, 8) = '0=0 AND ' then
    S := Copy(S, 9, Length(S));

  while Length(S) > 0 do
  begin
    if AnsiCompareText(LeftStr(S, 4), 'not ') = 0 then
    begin
      cbNot.Checked := True;
      S := RightStr(S, Length(S) - 4);
    end;

    X := Pos('=', S);
    XL := Pos('like', S);

    if (X = 0) and (XL = 0) then
      break;

    L := 1;
    if (X = 0) or (XL <> 0) and (XL < X) then
    begin
      X := XL;
      L := 4;
    end;

    N := Trim(Copy(S, 1, X - 1));

    if Copy(N, 1, 6) = 'Upper(' then
      N := Copy(N, 7, Length(N) - 7);

    S := Copy(S, X + L, 9999);

    X := Pos(' AND ', S);
    if (X = 0) then
    begin
      E := Trim(S);
      S := '';
    end
    else
    begin
      E := Trim(Copy(S, 1, X - 1));
      S := Copy(S, X + 5, 9999);
    end;

    if E[1] = '''' then
      E := Copy(E, 2, 999);
    if E[1] = '%' then
      E := Copy(E, 2, 999);

    J := Length(E);
    if J > 0 then
      if E[J] = '''' then
      begin
        E := Copy(E, 1, J - 1);
        J := J - 1;
      end;

    if J > 0 then
      if E[J] = '%' then
      begin
        E := Copy(E, 1, J - 1);
//                J := J - 1 ;
      end;

    for J := 0 to CurrentFieldName.Count - 1 do
    begin
      if GetFieldName(J) = N then
        StringGrid1.Cells[1, J] := E;
    end;
  end;
end;

procedure TFilterDialog.StringGrid1KeyPress(Sender: TObject;
  var Key: Char);
var
  S: string;
begin
  S := Key;
  S := AnsiUpperCase(S);
  Key := S[1];
end;

function TFilterDialog.Filter(const ACaseInsensitiveExpression: Boolean = False): string;
var
  I: integer;
  Fld, Expr: String;
begin
  result := '';

  for I := 0 to CurrentFieldName.Count - 1 do
    if Length(StringGrid1.Cells[1, I]) > 0 then
    begin
      if not (result = '') then
        result := result + ' AND ';

      if cbNot.Checked then
        result := result + 'not ';
                                        
      Fld := GetFieldName(I);
      Expr := StringGrid1.Cells[1, I];

      if DataSet.FieldByName(GetFieldName(I)).DataType = ftString then
      begin
        if ACaseInsensitiveExpression then
        begin
          Fld := 'Upper(' + Fld +')';
          Expr := AnsiUpperCase(Expr);
        end;

        if cbExact.Checked then
          result := result + Fld + ' like ''' + Expr + ''''
        else
          result := result + Fld + ' like ''%' + Expr + '%'''
      end

      else
        result := result + Fld + ' = ''' + Expr + '''';
    end;

  if result <> '' then
    if cbExact.Checked then
      result := '1=1 AND ' + result
    else
      result := '0=0 AND ' + result;
end;

procedure TFilterDialog.BitBtn3Click(Sender: TObject);
var
  I: integer;
begin
  for I := 0 to StringGrid1.RowCount - 1 do
    StringGrid1.Cells[1, I] := '';
end;

function TFilterDialog.GetCaption(const i: Integer): string;
var
  j, k: Integer;
begin
  j := 1;
  while CurrentFieldName[i][j] <> #9 do
    Inc(j);
  Inc(j);
  while CurrentFieldName[i][j] <> #9 do
    Inc(j);
  Inc(j);
  k := j;
  while (j <= Length(CurrentFieldName[i])) and (CurrentFieldName[i][j] <> #9) do
    Inc(j);
  Result := Copy(CurrentFieldName[i], k, j - k);
end;

function TFilterDialog.GetFieldName(const i: Integer): string;
var
  j: Integer;
begin
  j := 1;
  while CurrentFieldName[i][j] <> #9 do
    Inc(j);
  Result := Copy(CurrentFieldName[i], 1, j - 1);
end;

procedure TFilterDialog.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    BitBtn1.Click;
    Key := 0;
  end
  else
    if Key = VK_ESCAPE then
  begin
    BitBtn2.Click;
    Key := 0;
  end;
end;

procedure TFilterDialog.FormCreate(Sender: TObject);
begin
  CurrentFieldName := TStringList.Create;
end;

procedure TFilterDialog.FormDestroy(Sender: TObject);
begin
  CurrentFieldName.Free;
end;

end.
