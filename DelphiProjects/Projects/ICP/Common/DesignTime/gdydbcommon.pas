{$INCLUDE gdy.inc}
unit gdydbcommon;

interface
uses
  db, dbclient, classes, dbtables, kbmMemTable;


function ActiveDS( ds: TDataSource ): boolean; overload;
function ActiveDS( ds: TDataSet ): boolean; overload;
function AssignedDS( ds: TDataSource ): boolean;
function NotEmptyDS( ds: TDataSource ): boolean;
function IsTouchedDS( ds: TDataSource ): boolean;

{$IFDEF D6_UP}
const
  sUserSortIndexName = 'SORT';

function CanSortOnField(f: TField): boolean;
function IsSortableDS(ds: TDataSet): boolean;

procedure GetCurrentSorting( ds: TDataSet; var aSortOnFields: string; var aSortDescOnFields: string); overload;
procedure ResortRequested( ds: TDataSet; aFieldName: string; incremental: boolean );
procedure SetCurrentSorting( ds: TDataSet;  aSortOnFields, aSortDescOnFields: string);
{$ENDIF}

function GetInteger( dbname, command: string ): integer;

procedure EnableDBControl( control: TComponent; enable: boolean );

{$IFDEF D6_UP}
function CloneDS( cds: TCustomClientDataSet ): TClientDataSet;
{$ENDIF}

type
  TTableBookmark = record
    fieldName: string;
    fieldValue: integer;
    restore: boolean;
  end;

function CloseDS( p: TDataSet; fieldname: string ): TTableBookmark;
procedure OpenDS( p: TDataSet; bmk: TTableBookmark );
procedure RefreshDS( p: TDataSet; fieldname: string );

procedure AddFields( table: TDataSet; fieldlist: string; aDatabaseName, aTableName: string ); overload;
procedure AddFields( table: TTable; fieldlist: string ); overload;

function CreateClientDataset(const ds: TDataset; const Owner: TComponent): TClientDataset;

procedure InitDBASE( adb: TDatabase; path: string);
procedure SetLangdriver( aSession: TSession; ld: string );
procedure SetDriverParam( aSession: TSession; drv, param, val: string );

procedure EnsureHasIndex( table: TTable; afieldNames: string ); overload;
procedure EnsureHasIndex( aDatabaseName, aTableName, afieldNames: string ); overload;
procedure DeleteAllIndexes( table: TTable);


type
  TSpecElem = record
    inname: string;
    outname: string;
  end;

type
  TCustomExportEvent = function( ds: TDataSet; outname: string ): string of object;

{$ifdef D6_UP}
procedure ExportDSToCSV( ds: TDataSet; fname: string; const spec: array of TSpecElem; cee: TCustomExportEvent =nil);
procedure SaveDSToCSV( ds: TDataSet; fname: string );
{$endif}

implementation

uses
  sysutils, gdycommon, typinfo, dbctrls, graphics, provider, csreader{$ifdef D6_UP}, cswriter{$endif};

function ActiveDS( ds: TDataSource ): boolean;
begin
  Result :=  assigned(ds) and assigned(ds.DataSet) and ds.DataSet.Active;
end;

function ActiveDS( ds: TDataSet ): boolean;
begin
  Result :=  Assigned(ds) and ds.Active;
end;

function AssignedDS( ds: TDataSource ): boolean;
begin
  Result := assigned(ds) and assigned(ds.DataSet);
end;

{$IFDEF D6_UP}

function IsSortableDS(ds: TDataSet): boolean;
begin
  Result := (ds is TCustomClientDataSet) or (ds is TkbmCustomMemTable)
end;

function CanSortOnField( f: TField ): boolean;
begin
  Result := not (f is TBlobField) {and (f.FieldKind in [fkData, fkInternalCalc])};
end;

type
  TCheatCustomClientDataSet = class(TCustomClientDataSet);

procedure GetCurrentSortingCDS( ds: TCustomClientDataSet; var aSortOnFields: string; var aSortDescOnFields: string); 
var
  i: integer;
begin
  with TCheatCustomClientDataSet(ds) do
  begin
    IndexDefs.Update;
//      for i := 0 to  IndexDefs.Count -1 do
//        ODS( '%d <%s> <%s> <%s>',[i,IndexDefs[i].Name, IndexDefs[i].Fields, IndexDefs[i].DescFields ]);
    i := IndexDefs.IndexOf( IndexName );
    if i <> -1 then
    begin
      aSortOnFields := IndexDefs[i].Fields;
      if ixDescending in IndexDefs[i].Options then
        aSortDescOnFields := IndexDefs[i].Fields  // assume that DescFields is '' and DescFields doen't mean reverse order fields
      else
        aSortDescOnFields := IndexDefs[i].DescFields;
    end
    else if trim(IndexFieldNames) <> '' then
    begin
      aSortOnFields := IndexFieldNames;
      aSortDescOnFields := '';
    end
    else
    begin
      aSortOnFields := '';
      aSortDescOnFields := '';
    end;
    //ODS('Get sorting order: <%s> <%s>',[aSortOnFields,aSortDescOnFields]);
  end;
end;

procedure GetCurrentSortingKbm( ds: TkbmCustomMemTable; var aSortOnFields: string; var aSortDescOnFields: string);
var
  i: integer;
begin
  with ds do
  begin
    IndexDefs.Update;
//      for i := 0 to  IndexDefs.Count -1 do
//        ODS( '%d <%s> <%s> <%s>',[i,IndexDefs[i].Name, IndexDefs[i].Fields, IndexDefs[i].DescFields ]);
    i := IndexDefs.IndexOf( IndexName );
    if i <> -1 then
    begin
      aSortOnFields := IndexDefs[i].Fields;
      if ixDescending in IndexDefs[i].Options then
        aSortDescOnFields := IndexDefs[i].Fields  // assume that DescFields is '' and DescFields doen't mean reverse order fields
      else
        aSortDescOnFields := IndexDefs[i].DescFields;
    end
    else if trim(IndexFieldNames) <> '' then
    begin
      aSortOnFields := IndexFieldNames;
      aSortDescOnFields := '';
    end
    else
    begin
      aSortOnFields := '';
      aSortDescOnFields := '';
    end;
    //ODS('Get sorting order: <%s> <%s>',[aSortOnFields,aSortDescOnFields]);
  end;
end;

procedure GetCurrentSorting( ds: TDataSet; var aSortOnFields: string; var aSortDescOnFields: string);
begin
  if ds is TCustomClientDataSet then
    GetCurrentSortingCDS(ds as TCustomClientDataSet, aSortOnFields, aSortDescOnFields)
  else if ds is TkbmCustomMemTable then
    GetCurrentSortingKbm(ds as TkbmCustomMemTable, aSortOnFields, aSortDescOnFields)
  else
    Assert(false);
end;

procedure DeleteSubStrFrom(const Substr: string; var Str: string);
var
  i: Integer;
begin
  i := Pos(';' + Substr + ';', ';' + Str + ';');
  if i <> 0 then
  begin
    Str := ';' + Str + ';';
    Str := Copy(Str, 1, i - 1) + ';' + Copy(Str, i + Length(Substr) + 2, 255);
    if (Str <> '') and (Str[1] = ';') then
      Delete(Str, 1, 1);
    if (Str <> '') and (Str[Length(Str)] = ';') then
      Delete(Str, Length(Str), 1);
  end;
end;

procedure SetCurrentSortingKbm( ds: TkbmCustomMemTable;  aSortOnFields, aSortDescOnFields: string);
var
  id: TIndexDef;
begin
  with ds do
  begin
    if trim(MasterFields) <> '' then //temporary
    begin
      DeleteSubStrFrom( MasterFields, aSortOnFields );
      DeleteSubStrFrom( MasterFields, aSortDescOnFields );
      if trim(aSortOnFields) <> '' then
        aSortOnFields := MasterFields + ';' + aSortOnFields
      else
        aSortOnFields := MasterFields;
    end;
    IndexDefs.Update;
    if IndexDefs.IndexOf( sUserSortIndexName ) <> -1 then
      DeleteIndex( sUserSortIndexName );

    if trim(aSortOnFields) <> '' then
    begin
      id := TIndexDef.Create(IndexDefs);
      try
        id.Name := sUserSortIndexName;
        id.Fields := aSortOnFields;
        id.DescFields := aSortDescOnFields;
        id.Options := [ixCaseInsensitive];
        Indexes.Add(id);
      except
        id.Free;
        raise;
      end;
      IndexName := sUserSortIndexName;
    end
    else
    begin
      IndexName := '';
      IndexFieldNames := '';
    end;
    //ODS('Set sorting order: <%s> <%s>',[aSortOnFields,aSortDescOnFields]);
  end;
end;

procedure SetCurrentSortingCDS( ds: TCustomClientDataSet;  aSortOnFields, aSortDescOnFields: string);
begin
  with TCheatCustomClientDataSet(ds) do
  begin
    if trim(MasterFields) <> '' then //temporary
    begin
      DeleteSubStrFrom( MasterFields, aSortOnFields );
      DeleteSubStrFrom( MasterFields, aSortDescOnFields );
      if trim(aSortOnFields) <> '' then
        aSortOnFields := MasterFields + ';' + aSortOnFields
      else
        aSortOnFields := MasterFields;
    end;
    IndexDefs.Update;
    if IndexDefs.IndexOf( sUserSortIndexName ) <> -1 then
      DeleteIndex( sUserSortIndexName );

    if trim(aSortOnFields) <> '' then
    begin
      AddIndex(sUserSortIndexName, aSortOnFields, [ixCaseInsensitive], aSortDescOnFields);
      IndexName := sUserSortIndexName;
    end
    else
    begin
      IndexName := '';
      IndexFieldNames := '';
    end;
    //ODS('Set sorting order: <%s> <%s>',[aSortOnFields,aSortDescOnFields]);
  end;
end;

procedure SetCurrentSorting( ds: TDataSet;  aSortOnFields, aSortDescOnFields: string);
begin
  if ds is TCustomClientDataSet then
    SetCurrentSortingCDS(ds as TCustomClientDataSet, aSortOnFields, aSortDescOnFields)
  else if ds is TkbmCustomMemTable then
    SetCurrentSortingKbm(ds as TkbmCustomMemTable, aSortOnFields, aSortDescOnFields)
  else
    Assert(false);
end;

procedure ResortRequested( ds: TDataSet; aFieldName: string; incremental: boolean );
type
  TFieldSortStatus = ( fsFirst, fsNone = fsFirst, fsAsc, fsDesc, fsLast = fsDesc );
var
  f: TField;
  fss: TFieldSortStatus;
  sortOnFields: string;
  sortDescOnFields: string;


  function CheckSubStr(const Substr, Str: string): Boolean;
  begin
    Result := Pos(';' + Substr + ';', ';' + Str + ';') <> 0;
  end;

  procedure AddSubStrTo(const Substr: string; var Str: string);
  begin
    if not CheckSubStr(Substr, Str) then
    begin
      if Str <> '' then
        Str := Str + ';';
      Str := Str + Substr;
    end;
  end;

  function CalcFieldSortStatus( aFieldName, aSortOnFields, aSortDescOnFields: string ): TFieldSortStatus;
  begin
    if CheckSubStr( aFieldName, aSortDescOnFields ) then
      Result := fsDesc
    else if CheckSubStr( aFieldName, aSortOnFields ) then
      Result := fsAsc
    else
      Result := fsNone;
  end;

  function NextFSS( fss: TFieldSortStatus ): TFieldSortStatus;
  begin
    if fss = fsLast then
      Result := fsFirst
    else
      Result := succ(fss);
  end;

  procedure ChangeSorting( afieldName: string; fss: TFieldSortStatus; var aSortOnFields: string; var aSortDescOnFields: string );
  begin
    case fss of
      fsNone: begin
                DeleteSubStrFrom( afieldName, aSortOnFields );
                DeleteSubStrFrom( afieldName, aSortDescOnFields );
              end;
      fsAsc:  begin
                AddSubStrTo( afieldName, aSortOnFields );
                DeleteSubStrFrom( afieldName, aSortDescOnFields );
              end;
      fsDesc: begin
                AddSubStrTo( afieldName, aSortOnFields );
                AddSubStrTo( afieldName, aSortDescOnFields );
              end;
    end;
  end;

begin
  f := ds.FieldByName( aFieldName );
  if CanSortOnField( f ) then
  begin
    ds.DisableControls;
    try
      GetCurrentSorting( ds, sortOnFields, sortDescOnFields );
      fss := CalcFieldSortStatus( afieldName, sortOnFields, sortDescOnFields );
      if {(fss = fsNone) and }not incremental then
      begin
        if CheckSubStr(aFieldName, sortOnFields) then
          sortOnFields := aFieldName
        else
          sortOnFields := '';
        if CheckSubStr(AFieldName, sortDescOnFields) then
          sortDescOnFields := aFieldName
        else
          sortDescOnFields := ''
      end;
      fss := NextFSS(fss);
      ChangeSorting( afieldName, fss, sortOnFields, sortDescOnFields );
      SetCurrentSorting( ds, sortOnFields, sortDescOnFields );
    finally
      ds.EnableControls;
    end;
  end;
end;
{$ENDIF}


function GetInteger( dbname, command: string ): integer;
begin
  with TQuery.Create( nil ) do
  try
    SQL.Text := command;
    DatabaseName := dbname;
    Open;
    try
      Result := Fields[0].AsInteger;
    finally
      Close;
    end;
  finally
    Free;
  end;
end;

procedure EnableDBControl( control: TComponent; enable: boolean );
begin
  EnableControl( control, enable );
  if IsPublishedProp( control, 'ParentColor' ) and IsPublishedProp( control, 'Color' ) and (control is TDBLookupControl) then
    if enable then
    begin
      SetEnumProp( control, 'ParentColor', 'false' );
      SetOrdProp( control, 'Color', clWindow );
    end
    else
    begin
      SetEnumProp( control, 'ParentColor', 'true' );
    end;
end;

{$IFDEF D6_UP}
function CloneDS( cds: TCustomClientDataSet ): TClientDataSet;
var
  aField: TField;
  i: integer;
type
  TFieldClass = class of TField;
begin
  Result := TClientDataSet.Create(nil);
  try
    for i := 0 to cds.FieldCount - 1 do
    begin
      aField := TFieldClass(cds.Fields[I].ClassType).Create(Result);
//      ODS( aField.ClassName );
      with aField do
      begin
        Visible := cds.Fields[I].Visible;
        Size := cds.Fields[I].Size;
        FieldName := cds.Fields[I].FieldName;
        Required := cds.Fields[I].Required;
        DataSet := Result;
        DisplayLabel := cds.Fields[I].DisplayLabel;
        FieldKind := cds.Fields[I].FieldKind;
        KeyFields := cds.Fields[I].KeyFields;
        LookupDataSet := cds.Fields[I].LookupDataSet;
        LookupKeyFields := cds.Fields[I].LookupKeyFields;
        LookupResultField := cds.Fields[I].LookupResultField;
      end;
      if aField is TBCDField then
        TBCDField(aField).Precision := TBCDField(cds.Fields[I]).Precision;
      if cds.Fields[I].DataType = ftFixedChar then
        TStringField(aField).FixedChar := True;
    end;
    Result.CreateDataSet;
    Result.Data := cds.Data;
  except
    FreeAndNil(Result);
    raise;
  end;
end;
{$ENDIF}

function NotEmptyDS( ds: TDataSource ): boolean;
begin
  Result := ActiveDS( ds ) and not( ds.DataSet.Eof and ds.DataSet.Bof );
end;

function IsTouchedDS( ds: TDataSource ): boolean;
begin
  Result := ActiveDS( ds ) and ( ds.DataSet.State in [dsEdit, dsInsert] );
end;

function CloseDS( p: TDataSet; fieldname: string ): TTableBookmark;
begin
  Result.restore := p.Active and not (p.Bof and p.Eof);
  Result.fieldName := fieldname;
  if Result.restore then
    Result.fieldValue := p[fieldname];
  p.Close;
end;

procedure OpenDS( p: TDataSet; bmk: TTableBookmark );
begin
  p.DisableControls;
  try
    p.Open;
    if bmk.restore then
      p.Locate( bmk.fieldName, bmk.fieldValue,[]);
  finally
    p.EnableControls;
  end;
end;

procedure RefreshDS( p: TDataSet; fieldname: string );
var
  bmk: TTableBookmark;
begin
  p.DisableControls;
  try
    bmk := CloseDS( p, fieldname );
    OpenDS( p, bmk );
  finally
    p.EnableControls;
  end;
end;

procedure AddFields( table: TTable; fieldlist: string );
begin
  AddFields( table, fieldlist, table.DatabaseName, table.TableName );
end;

procedure AddFields( table: TDataSet; fieldlist: string; aDatabaseName, aTableName: string );
var
  sl: TStringList;
  fieldCount: integer;
  hasField: array of boolean;
  hasntAny: boolean;
  tableActive: boolean;
  i: integer;
  s: string;
  p: integer;
  sqltext: string;
begin
  tableActive := table.Active;
  try
    table.Open;
    sl := TStringList.Create;
    try
      sl.Text := fieldlist;
      fieldCount := sl.Count;
      SetLength( hasField, fieldCount );
      hasntAny := false;
      for i := 0 to fieldCount-1 do
      begin
        s := trim(sl[i]);
        p := Pos( ' ', s );
        hasField[i] := table.FindField(copy( s, 1, p-1)) <> nil;
        hasntAny := hasntAny or not hasField[i];
      end;
      if hasntAny then
      begin
        table.Close;
        sqltext := '';
        for i := 0 to fieldCount-1 do
          if not hasField[i] then
          begin
            s := trim(sl[i]);
            p := Pos( ' ', s );
            if sqltext <> '' then
              sqltext := sqltext + ',';
            sqltext := sqltext + Format(' add %s %s',[copy( s, 1, p-1), copy( s, p+1, length(s)-p )]);
          end;
        sqltext := 'alter table "'+aTableName+'"' + sqltext;
        with TQuery.Create(nil) do
        try
          DatabaseName := aDatabaseName;
          SQL.Text := sqltext;
          ExecSQL;
        finally
          Free;
        end;
      end;
    finally
      FreeAndNil( sl );
    end;
  finally
    table.Active := tableActive;
  end;
end;


function CreateClientDataset(const ds: TDataset; const Owner: TComponent): TClientDataset;
var
  p: TProvider;
begin
  Result := TClientDataSet.Create(Owner);
  p := TProvider.Create(nil);
  try
    ds.First;
    p.Dataset := ds;
    Result.Data := p.Data;
  finally
    p.Dataset := nil;
    p.Free;
  end;
end;

procedure InitDBASE( adb: TDatabase; path: string);
begin
  adb.DriverName := 'STANDARD';
  adb.Params.Clear;
  adb.Params.Values['ENABLE BCD'] := 'FALSE';
  adb.Params.Values['DEFAULT DRIVER'] := 'DBASE';
  adb.Params.Values['PATH'] := path;
  adb.Open;
end;

procedure SetDriverParam( aSession: TSession; drv, param, val: string );
var
  drvparam: TStringList;
begin
  aSession.ConfigMode := cmSession;
  try
    drvparam := TStringList.create;
    try
      drvparam.Add( param + '=' + val );
      aSession.ModifyDriver( drv, drvparam );
    finally
      FreeAndNil(drvparam);
    end;
  finally
    aSession.ConfigMode := cmAll;
  end;
end;


procedure SetLangdriver( aSession: TSession; ld: string );
begin
  SetDriverParam( aSession, 'DBASE', 'LANGDRIVER', ld );
end;

procedure EnsureHasIndex( aDatabaseName, aTableName, afieldNames: string );
var
  table: TTable;
begin
  table := TTable.Create(nil);
  try
    table.DatabaseName := aDatabaseName;
    table.TableName := aTableName;
    EnsureHasIndex( table, afieldNames );
  finally
    table.Free;
  end;
end;

procedure EnsureHasIndex( table: TTable; afieldNames: string );
begin
  table.IndexDefs.Update;
  if table.IndexDefs.GetIndexForFields(afieldNames, false) = nil then
    table.AddIndex( stringReplace(trim(afieldNames),';','_', [rfReplaceAll]), afieldNames,[]);
  table.IndexName := table.IndexDefs.FindIndexForFields( afieldNames ).Name;
end;

procedure DeleteAllIndexes( table: TTable);
var
  i: integer;
begin
  table.Close;
  table.Exclusive := true;
  table.IndexFiles.Add( ChangeFileExt(table.TableName,'.mdx') );
  table.IndexDefs.Update;
//  table.Open;
  for i := 0 to table.IndexDefs.count-1 do
    table.DeleteIndex( table.IndexDefs[i].Name );
end;


{$ifdef D6_UP}
procedure ExportDSToCSV( ds: TDataSet; fname: string; const spec: array of TSpecElem; cee: TCustomExportEvent );
var
  wr: TCSWriter;
  i: integer;
  bWasActive: boolean;
begin
  with ds do
    begin
      bWasActive := Active;
      if not Active then
        Open;
      try
        wr := TCSWriter.Create( fname );
        try
          for i := low(spec) to high(spec) do
            wr.Write( spec[i].outname );
          wr.WriteEndOfRow;
          First;
          while not Eof do
          begin
            for i := low(spec) to high(spec) do
              if spec[i].inname <> '' then
                wr.Write( FieldByName( spec[i].inname ) )
              else if assigned(cee) then
                wr.Write( cee( ds, spec[i].outname) )
              else
                wr.Write( '' );
            wr.WriteEndOfRow;
            Next;
          end
        finally
          FreeAndNil( wr );
        end
      finally
	      Active := bWasActive;
      end
    end
end;

procedure SaveDSToCSV( ds: TDataSet; fname: string );
var
  wr: TCSWriter;
  i: integer;
  bWasActive: boolean;
begin
  with ds do
  begin
    bWasActive := Active;
    if not Active then
      Open;
    try
  //      writeln( RecordCount );
      wr := TCSWriter.Create( fname );
      try
        for i := 0 to ds.FieldCount-1 do
          wr.Write( ds.Fields[i].FieldName );
        wr.WriteEndOfRow;
        First;
        while not Eof do
        begin
          for i := 0 to ds.FieldCount-1 do
            wr.Write( ds.Fields[i] );
          wr.WriteEndOfRow;
          Next;
        end
      finally
        FreeAndNil( wr );
      end
    finally
      Active := bWasActive;
    end;
  end
end;
{$endif}



end.
