unit reg;

interface

procedure Register;

implementation

uses
  dbcomp, classes, passwordedit;

procedure Register;
begin
  RegisterComponents('ICP',
  [
  TreDBGrid,
  TreLabel,
  TFixedVCLDBLookupComboBox,
  TPasswordEdit,
  TReDBCheckGrid
  ]);
end;

end.
