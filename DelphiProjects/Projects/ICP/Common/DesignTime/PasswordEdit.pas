unit PasswordEdit;

interface

uses
  stdctrls;

type
  TPasswordEdit = class(TEdit) //somewhat secure
  private
    FPassword: string;
    function GetPassword: string;
    procedure SetPassword(const Value: string);
  public
    // never use Text property
    property Password: string read GetPassword write SetPassword;
    property PasswordChar default '*';
  end;

implementation

{ TPasswordEdit }

function TPasswordEdit.GetPassword: string;
begin
  if Text = StringOfChar(' ', Length(FPassword)) then
    Result:= FPassword
  else
    Result := Text;
end;

procedure TPasswordEdit.SetPassword(const Value: string);
begin
  FPassword := Value;
  Text := StringOfChar(' ', Length(FPassword));
end;

end.
