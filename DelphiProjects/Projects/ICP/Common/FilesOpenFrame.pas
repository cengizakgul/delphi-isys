unit FilesOpenFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, StdCtrls, ActnList, StdActns, Buttons;

type
  TFilesSelectedEvent = procedure (files: TStrings) of object;

  TFilesOpenFrm = class(TFrame)
    Panel1: TPanel;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    mmFiles: TMemo;
    Panel3: TPanel;
    procedure FileOpen1Accept(Sender: TObject);
  private
    FOnFilesSelected: TFilesSelectedEvent;
  public
    property OnFilesSelected: TFilesSelectedEvent read FOnFilesSelected write FOnFilesSelected;
  end;

implementation

{$R *.dfm}

{ TFilesOpenFrm }

procedure TFilesOpenFrm.FileOpen1Accept(Sender: TObject);
begin
  if assigned(FOnFilesSelected) then
    FOnFilesSelected((Sender as TFileOpen).Dialog.Files);
  mmFiles.Lines.Assign( (Sender as TFileOpen).Dialog.Files );
end;

end.
