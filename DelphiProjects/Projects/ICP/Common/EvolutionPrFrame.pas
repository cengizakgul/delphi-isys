unit EvolutionPrFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, EvolutionDSFrame, evodata;

type
  TEvolutionPrFrm = class(TFrame)
    PayrollFrame: TEvolutionDsFrm;
  public
    procedure Init(evoData: TEvoData);
  end;

implementation

uses
  common;
{$R *.dfm}

{ TEvolutionPrFrm }

procedure TEvolutionPrFrm.Init(evoData: TEvoData);
begin
  PayrollFrame.dgGrid.Selected.Clear;
  AddSelected(PayrollFrame.dgGrid.Selected, 'CHECK_DATE', 0, 'Check Date', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'RUN_NUMBER', 0, 'Run Number', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'PAYROLL_TYPE', 0, 'Type', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'STATUS', 0, 'Status', true);
  AddSelected(PayrollFrame.dgGrid.Selected, 'PR_NBR', 0, 'Internal Pr#', true);

  PayrollFrame.Init(evoData, 'TMP_PR');
end;

end.
