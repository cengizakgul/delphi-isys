unit timeclockimport;

interface

uses
  gdycommonlogger;

type
  TTimeClockRec = record              //see ConsolidateTimeClockRecs
    CUSTOM_EMPLOYEE_NUMBER: string;   //grouping
    FirstName: string;                //non essential
    LastName: string;                 //non essential
    SSN: string;                      //non essential
    EDCode: string;                   //grouping
    Rate: Variant;                    //tricky grouping
    Hours: Variant;                   //tricky grouping and summing
    Amount: Variant;                  //tricky grouping and summing
    PunchDate: Variant; //TDateTime;  //non essential
    PunchIn: Variant; //TDateTime;    //non essential
    PunchOut: Variant; //TDateTime;   //non essential
    Division: string;                 //grouping
    Branch: string;                   //grouping
    Department: string;               //grouping
    Team: string;                     //grouping
    Comment: string;                  //non essential, but preserved in summarize mode provided it's the same for the grouped records
    Job: string;                      //grouping
    Shift: string;                    //grouping
    RateNumber: string;               //grouping
    WCCode: string;                   //grouping
  end;

  PTimeClockRec = ^TTimeClockRec;

  TTimeClockRecs = array of TTimeClockRec;

  TTCImportLookupOption = (tciLookupByName = 0, tciLookupByNnumber = 1, tciLookupBySSN = 2);
  TTCImportDBDTOption = (tciDBDTFull = 0, tciDBDTSmart = 1);
  TTCImportFileFormatOption = (tciFileFormatFixed = 0, tciFileFormatCommaDelimited = 1);

  TTCImportAPIOptions = record
    LookupOption: TTCImportLookupOption;
    DBDTOption: TTCImportDBDTOption;
    FourDigitYear: boolean;
    FileFormat: TTCImportFileFormatOption;
    AutoImportJobCodes: boolean;
    UseEmployeePayRates: boolean;
  end;

  TTimeClockImportData = record
    Text: string;
    APIOptions: TTCImportAPIOptions;
  end;

function ConsolidateTimeClockRecs(recs: TTimeClockRecs; logger: ICommonLogger): TTimeClockRecs;
function BuildTimeClockImportData(recs: TTimeClockRecs; useEmployeePayRates: boolean; autoImportJobCodes: boolean; logger: ICommonLogger): TTimeClockImportData;
procedure LogTCImportAPIOptions(logger: ICommonLogger; options: TTCImportAPIOptions);

const
  eps: double = 1e-10; //why not

implementation

uses
  cswriter, classes, sysutils, gdycommon, variants, dateutils, StrUtils,
  evoapiconnection, common;

function DateTimeToEvoDateTime(v: Variant): string;
begin
  if VarIsNull(v) or VarIsEmpty(v) then
    Result := ''
  else
  begin
    Result := FormatDateTime('yyyymmddhhnnss', v);
  end
end;

function TimeClockRecToText(const rec: TTimeClockRec; logger: ICommonLogger): string;
var
  wr: TCSWriter;
  ss: TStringStream;
  rate: Variant;

  procedure Write(val: string);
  begin
    wr.Write( trim(val) );
  end;

begin
  logger.LogEntry('Generating Evolution TC Import file record');
  try
    try
      logger.LogContextItem(sCtxEECode, rec.CUSTOM_EMPLOYEE_NUMBER);

      rate := rec.Rate;

      if VarIsNull(rate) or VarIsEmpty(rate) then
        rate := 0;

      ss := TStringStream.Create('');
      try
        wr := TCSWriter.Create( ss, false );
        try
          Write( rec.CUSTOM_EMPLOYEE_NUMBER );
          Write( trim(rec.FirstName) + ' ' + trim(rec.LastName) );
          Write( rec.Department ); //Override Dept
          Write( rec.Job ); //Job
          Write( rec.Shift ); //Shift

          Write( copy(trim(rec.EDCode),1,1) );  //D
          Write( copy(trim(rec.EDCode),2,9999) );
          Write( VarToStr(rate) );
          Write( VarToStr(rec.Hours) );
          if not VarIsNull(rec.PunchDate) and not VarIsEmpty(rec.PunchDate) then
          begin
            Write( FormatDateTime('yy', rec.PunchDate) );
            Write( FormatDateTime('mm', rec.PunchDate) );
            Write( FormatDateTime('dd', rec.PunchDate) );
          end
          else
          begin
            Write( '' );
            Write( '' );
            Write( '' );
          end;

          Write( '' ); //hour
          Write( '' ); //minute
          Write( VarToStr(rec.Amount) );
          Write( '' ); //seq number
          Write( rec.Division ); //Override Division
          Write( rec.Branch ); //Override branch
          Write( '' ); //Override state
          Write( '' ); //Override local
          Write( '' ); //blank fill
          Write( '' ); //Deduction 3 hours
          Write( '' ); //Deduction 3 amount
          Write( '' ); //SSN  //      Write( trim(rec.SSN) );
          Write( rec.WCCode ); //Workers� Compensation
          Write( rec.RateNumber ); //Rate Number
          Write( DateTimeToEvoDateTime(rec.PunchIn) ); //Punch In
          Write( DateTimeToEvoDateTime(rec.PunchOut) ); //Punch Out
          Write( rec.Team ); //Override Team
          Write( rec.Comment ); //Check Comments
          wr.WriteEndOfRow();
{
fixed length and csv formats are different!
RE 62022
The comma-delimited version of the timeclock import has been changed so that the SSN will remain in position 24, while the Team has been moved to the last position of 29.
}
        finally
          FreeAndNil( wr );
        end;
        Result := ss.DataString;
      finally
        FreeAndNil( ss );
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

function TimeClockRecsToText(recs: TTimeClockRecs; logger: ICommonLogger): string;
var
  i: integer;
begin
  Result := '';
  logger.LogEntry('Generating Evolution TC Import file');
  try
    try
      for i := 0 to High(recs) do
      begin
        Result := Result + TimeClockRecToText(recs[i], logger);
      end
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

function AreGroupable(const one: TTimeClockRec; const another: TTimeClockRec): boolean;
  function VarsAreGroupable(const v1: Variant; const v2: Variant): boolean;
  var
    e1, e2: boolean;
  begin
    e1 := VarIsEmpty(v1) or VarIsNull(v1);
    e2 := VarIsEmpty(v2) or VarIsNull(v2);
    Result := not (e1 xor e2);
  end;
  function RatesAreGroupable(const v1: Variant; const v2: Variant): boolean;
  var
    e1, e2: boolean;
  begin
    e1 := VarIsEmpty(v1) or VarIsNull(v1);
    e2 := VarIsEmpty(v2) or VarIsNull(v2);
    Result := (e1 and e2) or (not e1 and not e2 and (abs(v1-v2) < eps));
  end;
begin
  Result :=
    (one.CUSTOM_EMPLOYEE_NUMBER = another.CUSTOM_EMPLOYEE_NUMBER) and
    (one.EDCode = another.EDCode) and
    RatesAreGroupable(one.Rate, another.Rate) and
    VarsAreGroupable(one.Hours, another.Hours) and
    VarsAreGroupable(one.Amount, another.Amount) and
    (one.Division = another.Division) and
    (one.Branch = another.Branch) and
    (one.Department = another.Department) and
    (one.Team = another.Team) and
    (one.Job = another.Job) and
    (one.Shift = another.Shift) and
    (one.RateNumber = another.RateNumber) and
    (one.WCCode = another.WCCode);
end;

//!! O(n^2)
function ConsolidateTimeClockRecs(recs: TTimeClockRecs; logger: ICommonLogger): TTimeClockRecs;
  function AddVars(const v1: Variant; const v2: Variant): Variant;
  begin
    if not(VarIsNull(v1) or VarIsEmpty(v1) or VarIsNull(v2) or VarIsEmpty(v2)) then // then both are neither empty nor null and we can sum them safely
      Result := v1 + v2
    else //both are empty or null
      Result := Null;
  end;

var
  used: integer;
  i, j: integer;
  found: boolean;
begin
  used := 0;
  SetLength(Result, Length(recs));
  for i := 0 to high(recs) do
  begin
    found := false;
    for j := 0 to used-1 do
    begin
      if AreGroupable(recs[i], Result[j]) then
      begin
        found := true;
        Result[j].Hours := AddVars(Result[j].Hours, recs[i].Hours);
        Result[j].Amount := AddVars(Result[j].Amount, recs[i].Amount);
      end
    end;
    if not found then
    begin
      Result[used] := recs[i];

      //clear non essential fields
      Result[used].FirstName := '';
      Result[used].LastName := '';
      Result[used].SSN := '';
      Result[used].PunchDate := Null;
      Result[used].PunchIn := Null;
      Result[used].PunchOut := Null;
//      Result[used].Comment := '';  //to pass through comments like 'Imported by EvoXXX'

      used := used + 1;
    end;
  end;
  SetLength(Result, used);
end;

function BuildTimeClockImportData(recs: TTimeClockRecs; useEmployeePayRates: boolean; autoImportJobCodes: boolean; logger: ICommonLogger): TTimeClockImportData;
begin
  Result.Text := TimeClockRecsToText(recs, logger);
  Result.APIOptions.LookupOption := tciLookupByNnumber;
  Result.APIOptions.DBDTOption := tciDBDTSmart;
  Result.APIOptions.FourDigitYear := false;
  Result.APIOptions.FileFormat := tciFileFormatCommaDelimited;
  Result.APIOptions.AutoImportJobCodes := autoImportJobCodes;
  Result.APIOptions.UseEmployeePayRates := useEmployeePayRates;       //UseEmployeePayRates must be false if DBDTs are not supplied
end;

procedure LogTCImportAPIOptions(logger: ICommonLogger; options: TTCImportAPIOptions);
const
  LookupOptionNames: array [TTCImportLookupOption] of string = ('Name', 'Custom #', 'SSN');
  DBDTOptionNames: array [TTCImportDBDTOption] of string = ('Full', 'Partial');
  FileFormatOptionNames: array [TTCImportFileFormatOption] of string = ('Fixed positions', 'Comma delimited');
begin
  logger.LogContextItem( 'Look up employee by', LookupOptionNames[options.LookupOption] );
  logger.LogContextItem( 'DBDT match', DBDTOptionNames[options.DBDTOption] );
  logger.LogContextItem( 'File format', FileFormatOptionNames[options.FileFormat] );
  logger.LogContextItem( 'Use four digits for year', BoolToStr(options.FourDigitYear, true) );
  logger.LogContextItem( 'Auto import job codes', BoolToStr(options.AutoImportJobCodes, true) );
  logger.LogContextItem( 'Use employee pay rates', BoolToStr(options.UseEmployeePayRates, true) );
end;

end.
