object EvolutionCompanySelectorFrm: TEvolutionCompanySelectorFrm
  Left = 0
  Top = 0
  Width = 651
  Height = 43
  TabOrder = 0
  object Label1: TLabel
    Left = 8
    Top = 17
    Width = 72
    Height = 13
    Caption = 'Evolution client'
  end
  object Label2: TLabel
    Left = 352
    Top = 17
    Width = 44
    Height = 13
    Caption = 'Company'
  end
  object dblcCL: TwwDBLookupCombo
    Left = 88
    Top = 13
    Width = 249
    Height = 21
    DropDownAlignment = taLeftJustify
    LookupField = 'CL_NBR'
    Style = csDropDownList
    Navigator = True
    TabOrder = 0
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnBeforeDropDown = dblcBeforeDropDown
    OnCloseUp = dblcCloseUp
  end
  object dblcCO: TwwDBLookupCombo
    Left = 400
    Top = 13
    Width = 233
    Height = 21
    DropDownAlignment = taLeftJustify
    LookupField = 'CO_NBR'
    Style = csDropDownList
    Navigator = True
    TabOrder = 1
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnBeforeDropDown = dblcBeforeDropDown
    OnCloseUp = dblcCloseUp
  end
end
