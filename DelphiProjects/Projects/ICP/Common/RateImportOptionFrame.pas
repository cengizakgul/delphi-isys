unit RateImportOptionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, gdyCommonLogger;

type
  TRateImportOption = (rateUseExternal, rateUseEvo, rateUseEvoEE);

  TRateImportOptionFrm = class(TFrame)
    RadioGroup1: TRadioGroup;
  private
    procedure SetValue(const Value: TRateImportOption);
    function GetValue: TRateImportOption;
  public
    property Value: TRateImportOption read GetValue write SetValue;
    procedure Clear;
  end;

procedure LogRateImportOption(logger: ICommonLogger; option: TRateImportOption; extapp: string);
function DescribeRateImportOption(option: TRateImportOption; extapp: string): string;

implementation

{$R *.dfm}

procedure LogRateImportOption(logger: ICommonLogger; option: TRateImportOption; extapp: string);
begin
  case option of
    rateUseExternal: logger.LogContextItem('Rate Import', Format('Use %s rates',[extapp]) );
    rateUseEvo: logger.LogContextItem('Rate Import', 'Use Evolution D/B/D/T rate override');
    rateUseEvoEE: logger.LogContextItem('Rate Import', 'Use Evolution employee pay rates');
  else
    Assert(false);
  end;
end;

function DescribeRateImportOption(option: TRateImportOption; extapp: string): string;
begin
  Result := 'Rate Import: ';
  case option of
    rateUseExternal: Result := Result + Format('Use %s rates',[extapp]);
    rateUseEvo: Result := Result + 'Use Evolution D/B/D/T rate override';
    rateUseEvoEE: Result := Result + 'Use Evolution employee pay rates';
  else
    Assert(false);
  end;
end;

{ TRateImportOptionFrm }

procedure TRateImportOptionFrm.Clear;
begin
  RadioGroup1.ItemIndex := -1;
end;

function TRateImportOptionFrm.GetValue: TRateImportOption;
begin
  Assert(RadioGroup1.ItemIndex >= integer(low(TRateImportOption)));
  Assert(RadioGroup1.ItemIndex <= integer(high(TRateImportOption)));
  Result := TRateImportOption(RadioGroup1.ItemIndex);
end;

procedure TRateImportOptionFrm.SetValue(const Value: TRateImportOption);
begin
  RadioGroup1.ItemIndex := ord(Value);
end;

end.
