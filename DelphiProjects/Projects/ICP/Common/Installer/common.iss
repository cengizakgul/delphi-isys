#ifndef VERSION
#define VERSION GetFileVersion(SourcePath + "\..\..\..\..\bin\ICP\"+APPBASE+"\"+APPBASE+".exe")
#define APPVERNAME APPNAME +" "+VERSION
#endif

[Setup]
AppVerName={#APPVERNAME}
AppName={#APPNAME}
#ifndef APPDIRNAME
DefaultDirName={pf}\iSystems\{#APPNAME}
#else
DefaultDirName={pf}\iSystems\{#APPDIRNAME}
#endif
DefaultGroupName=iSystems\{#APPNAME}
UninstallDisplayIcon={app}{#APPBASE}.exe
OutputDir=..\..\..\..\bin\ICP\Installers
OutputBaseFilename={#APPBASE}-{#VERSION}-{#NAME_SUFFIX}

Compression=lzma/ultra64
InternalCompressLevel=ultra64
SolidCompression=true

AppPublisher=iSystems LLC
AppCopyright=iSystems LLC, 2013
LicenseFile=..\..\..\Evolution\Installer\Resources\EULA.rtf

[Messages]
LicenseAccepted=I &accept the terms in the License Agreement
LicenseNotAccepted=I &do not accept the terms in the License Agreement

[Tasks]
Name: desktopicon; Description: Create a desktop icon; MinVersion: 4,4

[Files]

Source: ..\..\..\..\bin\ICP\{#APPBASE}\{#APPBASE}.exe; DestDir: {app}; Flags: ignoreversion
Source: {#APPBASE}.red; DestDir: {app}; Flags: ignoreversion
#ifndef NO_QUERIES
Source: ..\Resources\queries\*.rwq; DestDir: {app}\queries; Flags: ignoreversion
#endif

[Files]

Source: ..\..\..\..\Common\Misc\SSL\cert.pem; DestDir: {app}; Flags: ignoreversion
Source: ..\..\..\..\Common\External\OpenSSL\libeay32.dll; DestDir: {app}; Flags: ignoreversion
Source: ..\..\..\..\Common\External\OpenSSL\ssleay32.dll; DestDir: {app}; Flags: ignoreversion
Source: ..\..\..\..\Common\External\Misc\msvcr90.dll; DestDir: {app}; Flags: ignoreversion
Source: ..\..\..\..\Common\External\Misc\Microsoft.VC90.CRT.manifest; DestDir: {app}; Flags: ignoreversion		
Source: ..\..\..\..\Common\External\7Zip\New\7za.dll; DestDir: {app}; Flags: ignoreversion
#ifndef NO_HTML_TEMPLATES
Source: ..\..\..\..\Projects\ICP\Common\HTML Templates\*.*; DestDir: {app}\HTML Templates; Flags: ignoreversion
Source: ..\..\..\..\Projects\ICP\Common\HTML Templates\Log_files\*.*; DestDir: {app}\HTML Templates\Log_files; Flags: ignoreversion
#endif
[Icons]
Name: {group}\{#APPNAME}; Filename: {app}\{#APPBASE}.exe; WorkingDir: {app}
Name: {userdesktop}\{#APPNAME}; Filename: {app}\{#APPBASE}.exe; WorkingDir: {app}; MinVersion: 4,4; Tasks: desktopicon
 
