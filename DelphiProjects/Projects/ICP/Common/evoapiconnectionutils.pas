unit evoapiconnectionutils;

interface

uses
  evoapiconnection, gdycommonlogger, kbmMemTable, timeclockimport, db, common,
  xmlrpctypes, evConsts;

//EvoQ Stuff
function BuildPayrollsReportParams(payrolls: TEvoPayrollsDef): IRPCStruct;

//TC Import stuff

type
  TSyncStat = record
    Created: integer;
    Modified: integer;
    Deleted: integer;
    Errors: integer;
  end;

  TUpdateEmployeesStat = TSyncStat;

  TTOAStat = record
    Updated: integer;
    Errors: integer;
  end;


function SyncStatToString(const stat: TSyncStat): string;
function BuildEmptySyncStat: TSyncStat;
function AddSyncStats(const stat1: TSyncStat; const stat2: TSyncStat): TSyncStat;

function TOAStatToString(const stat: TTOAStat): string;
function BuildEmptyTOAStat: TTOAStat;

function ExecQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; queryFile: string; opName: string): TkbmCustomMemTable;
function ExecClQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; clnbr: integer; queryFile: string; opName: string): TkbmCustomMemTable;
function ExecCoQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; Company: TEvoCompanyDef; queryFile: string; opName: string): TkbmCustomMemTable;
function ExecPrQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; Pr: TEvoPayrollDef; queryFile: string; opName: string): TkbmCustomMemTable;

function LogEvoXImportResults(logger: ICommonLogger; r: TEvoXImportResult; objname: string): string; //deprecated;
function LogEvoXImportMessages(logger: ICommonLogger; messages: string): string;

function GetBatchInfo(conn: IEvoAPIConnection; Logger: ICommonLogger; Payroll: TEvoPayrollDef): TEvoPayrollBatchDef;

implementation

uses
  sysutils, gdyRedir, gdycommon, variants, gdyGlobalWaitIndicator, gdyclasses, strutils;

function ExecQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; queryFile: string; opName: string): TkbmCustomMemTable;
begin
  Result := nil; //to make compiler happy
  Logger.LogEntry(opName);
  try
    try
      WaitIndicator.StartWait(opName);
      try
        Result := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + queryFile, nil);
      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function ExecClQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; clnbr: integer; queryFile: string; opName: string): TkbmCustomMemTable;
var
  params: IRpcStruct;
begin
  Result := nil; //to make compiler happy
  Logger.LogEntry(opName);
  try
    try
      WaitIndicator.StartWait(opName);
      try

        Logger.LogContextItem('Internal Cl#', inttostr(ClNbr) );
        conn.OpenClient( ClNbr );
        params := TRpcStruct.Create;
        params.AddItem( 'CL_NBR', ClNbr ); //some queries use this parameter to return column CL_NBR; this column is father used in client data cache (TEvoData)
        Result := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + queryFile, params);

      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function ExecCoQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; Company: TEvoCompanyDef; queryFile: string; opName: string): TkbmCustomMemTable;
var
  params: IRpcStruct;
begin
  Result := nil; //to make compiler happy
  Logger.LogEntry(opName);
  try
    try
      WaitIndicator.StartWait(opName);
      try

        LogEvoCompanyDef( Logger, Company );
        conn.OpenClient( Company.ClNbr );
        params := TRpcStruct.Create;
        params.AddItem( 'CO_NBR', Company.CoNbr );
        Result := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + queryFile, params);

      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function ExecPrQuery(conn: IEvoAPIConnection; Logger: ICommonLogger; Pr: TEvoPayrollDef; queryFile: string; opName: string): TkbmCustomMemTable;
var
  params: IRpcStruct;
begin
  Result := nil; //to make compiler happy
  Logger.LogEntry(opName);
  try
    try
      WaitIndicator.StartWait(opName);
      try
        LogEvoPayrollDef(Logger, Pr);
        conn.OpenClient(Pr.Company.ClNbr);
        params := TRpcStruct.Create;
        params.AddItem( 'PR_NBR', Pr.Pr.PrNbr );
        Result := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + queryFile, params);
      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function BuildEmptySyncStat: TSyncStat;
begin
  Result.Modified := 0;
  Result.Deleted := 0;
  Result.Created := 0;
  Result.Errors := 0;
end;

function AddSyncStats(const stat1: TSyncStat; const stat2: TSyncStat): TSyncStat;
begin
  Result.Modified := stat1.Modified + stat2.Modified;
  Result.Deleted := stat1.Deleted + stat2.Deleted;
  Result.Created := stat1.Created + stat2.Created;
  Result.Errors := stat1.Errors + stat2.Errors;
end;

function SyncStatToString(const stat: TSyncStat): string;
  procedure Log(n: integer; act: string);
  begin
    if n > 0 then
    begin
      if Result <> '' then
        Result := Result + ', ';
      Result := Result + Format('%d %s', [n, act]);
    end;
  end;
begin
  Result := '';
  Log(stat.Errors, Plural('error',stat.Errors) );
  Log(stat.Created, 'created');
  Log(stat.Modified, 'modified');
  Log(stat.Deleted, 'deleted');

  if stat.Errors = 0 then
  begin
    if stat.Created + stat.Modified + stat.Deleted = 0 then
      Result := 'completed: all records were already up to date'
    else
      Result := 'completed: ' + Result;
  end
  else
  begin
    if stat.Created + stat.Modified + stat.Deleted = 0 then
      Result := 'failed: ' + Result
    else
      Result := 'partially completed: ' + Result;
  end
end;

function TOAStatToString(const stat: TTOAStat): string;
begin
  if stat.Errors = 0 then
  begin
    Result := Format('completed: %d %s updated', [stat.Updated, Plural('record', stat.Updated)]);
  end
  else
  begin
    if stat.Updated = 0  then
      Result := Format('failed: %d %s occured', [stat.Errors, Plural('error', stat.Errors)])
    else
      Result := Format('partially completed: %d %s occured, %d %s updated',
        [stat.Errors, Plural('error', stat.Errors), stat.Updated, Plural('record', stat.Updated)]);
  end
end;

function BuildEmptyTOAStat: TTOAStat;
begin
  Result.Updated := 0;
  Result.Errors := 0;
end;

function BuildPayrollsReportParams(payrolls: TEvoPayrollsDef): IRPCStruct;
var
  ClientsParameter, CompaniesParameter, PayrollsParameter: IRpcArray;
  i: integer;
begin
  Result := TRPCStruct.Create;

  ClientsParameter := TRPCArray.Create;
  ClientsParameter.AddItem(payrolls.Company.ClNbr); // internal client nbr: CL_NBR field
  Result.AddItem('Clients', ClientsParameter);

  CompaniesParameter := TRPCArray.Create;
  CompaniesParameter.AddItem(payrolls.Company.CoNbr); // internal company nbr: CO_NBR field
  Result.AddItem('Companies', CompaniesParameter);
  Result.AddItem('Company', payrolls.Company.CoNbr);

  PayrollsParameter := TRPCArray.Create;
  for i := 0 to high(payrolls.PrNbrs) do
    PayrollsParameter.AddItem(payrolls.PrNbrs[i].PrNbr);  // internal payroll number: PR_NBR
  Result.AddItem('Payrolls', PayrollsParameter);
end;

function LogEvoXImportResults(logger: ICommonLogger; r: TEvoXImportResult; objname: string): string;
var
  details: string;
begin
  Result := '';
  details := Format('Imported %d out of %d records',[r.ImportedRowsCount, r.InputRowsCount]);
  if r.ImportedRowsCount = r.InputRowsCount then
  begin
    if trim(r.Messages) <> '' then
    begin
      details := details + #13#10#13#10 + r.Messages;
      Logger.LogWarningFmt('%s import succeeded with warnings', [objname], details);
      Result := 'Import succeeded with warnings';
    end
    else
    begin
      Logger.LogEventFmt('%s import succeeded.', [objname], details);
      Result := 'Import succeeded';
    end
  end
  else
  begin
    details := details + #13#10#13#10 + r.Messages;
    if r.ImportedRowsCount > 0 then
    begin
      Logger.LogWarningFmt('%s import succeeded partially.', [objname], details);
      Result := 'Import succeeded partially';
    end
    else
    begin
      Logger.LogWarningFmt('%s import failed.', [objname], 'Nothing was imported.'#13#10#13#10 + r.Messages);
      Result := 'Import failed';
    end;
  end
end;

function LogEvoXImportMessages(logger: ICommonLogger; messages: string): string;
var
  list: IStr;
  msg: string;
  i: integer;
begin
  logger.LogDebug('EvoX result messages', messages);

  list := SplitToLines(messages);
  for i := 0 to list.Count-1 do
  begin
    msg := list.Str[i];
    if Pos('Error:', msg) = 1 then
      logger.LogError( 'EvoX import: ' + Trim(Copy(msg, Pos(':', msg)+1, 99999)) )
    else if Pos('Warning:', msg) = 1 then
      logger.LogWarning( 'EvoX import: ' + Trim(Copy(msg, Pos(':', msg)+1, 99999)) )
    else if Pos('Info:', msg) = 1 then
      logger.LogEvent( 'EvoX import: ' + Trim(Copy(msg, Pos(':', msg)+1, 99999)) )
    else if trim(msg) <> '' then
      logger.LogWarning( 'EvoX import (not parsed): ' + msg );
  end;
end;

function GetBatchInfo(conn: IEvoAPIConnection; Logger: ICommonLogger; Payroll: TEvoPayrollDef): TEvoPayrollBatchDef;
var
  params: IRpcStruct;
  PrBatches: TkbmCustomMemTable;
begin
  Logger.LogEntry( 'Getting pay period for payroll with single batch' );
  try
    try
      LogEvoPayrollDef(Logger, payroll);

      Result.Company := Payroll.Company;
      Result.Pr := Payroll.Pr;

      conn.OpenClient( Payroll.Company.ClNbr );

      params := TRpcStruct.Create;
      params.AddItem( 'PR_NBR', Payroll.Pr.PrNbr );
      PrBatches := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'qPrBatch.rwq', params);
      try
        if PrBatches.RecordCount = 0 then
          raise Exception.Create('This payroll doesn''t have a batch');
        if PrBatches.RecordCount > 1 then
          raise Exception.Create('This payroll has more than one batch');
        Result.PayPeriod.PeriodBegin := PrBatches.FieldByName('PERIOD_BEGIN_DATE').AsDateTime;
        Result.PayPeriod.PeriodEnd := PrBatches.FieldByName('PERIOD_END_DATE').AsDateTime;
        Result.HasRegularChecks := PrBatches.FieldByName('ChecksCount').AsInteger > 0;
        Result.HasUserEarningsLines := PrBatches.FieldByName('EarningsCount').AsInteger > 0;
      finally
        FreeAndNil(PrBatches);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.
