unit DBDTFilterFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evConsts, StdCtrls, evodata, ExtCtrls, Grids,
  Wwdbigrd, Wwdbgrid, dbcomp, DB, dbdtds, eefilter, common, OptionsBaseFrame;

type
  TDBDTFilterFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    cbLevel: TComboBox;
    cbUse: TCheckBox;
    dgDBDT: TreDBCheckGrid;
    dsDBDT: TDataSource;
    procedure cbLevelSelect(Sender: TObject);
    procedure cbUseClick(Sender: TObject);
  private
    FDBDT: TDBDTTable;
    FFilters: array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of TDBDTFilterItems;
    procedure InitLevelCombo(maxlevel: char);
    procedure InitForCurrentLevel;
    function SelectionToFilterItems: TDBDTFilterItems;
    procedure FilterItemsToSelection(items: TDBDTFilterItems);
    function IsInitialized: boolean;
    procedure HandleMultiSelectRecord(Grid: TwwDBGrid; Selecting: boolean; var Accept: boolean);
    procedure HandleMultiSelectAllRecords(Grid: TwwDBGrid; Selecting: boolean; var Accept: boolean);
    procedure HandleReverseSelectionRecord(Grid: TwwDBGrid;  var Accept: boolean);
  public
    constructor Create( Owner: TComponent ); override;
    procedure Init(CUSTOM_DBDT: TDataSet; Filter: TDBDTFilter);
    procedure ClearAndDisable;

    function GetFilter: TDBDTFilter;
    procedure Check;
  end;

implementation

{$R *.dfm}

uses
  gdycommon, gdyClasses, gdystrset, gdyDeferredCall;

{ TDBDTFilterFrm }

procedure TDBDTFilterFrm.Init(CUSTOM_DBDT: TDataSet; Filter: TDBDTFilter);
var
  maxlevel: char;
  level: char;
begin
  ClearAndDisable;

  FBlockOnChange := true;
  try

    FDBDT := TDBDTTable.Create(CUSTOM_DBDT);
    try
      maxlevel := GetEvoDBDTLevel(CUSTOM_DBDT);
      InitLevelCombo(maxlevel);

      for level := low(FFilters) to high(FFilters) do
        SetLength( FFilters[level], 0 );
      level := maxlevel;
      if Filter.Use and (Filter.Level <= maxlevel) then
      begin
        level := Filter.Level;
        FFilters[level] := Filter.Items;
      end;

      cbLevel.ItemIndex := Ord(level) - Ord(CLIENT_LEVEL_DIVISION);

      EnableControlRecursively(Self, true);
      cbUse.Checked := Filter.Use;
      cbUseClick(nil);
    except
      ClearAndDisable;
      raise;
    end

  finally
    FBlockOnChange := false;
  end;
end;

procedure TDBDTFilterFrm.ClearAndDisable;
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, false);
    if IsInitialized then
    begin
      cbUse.Checked := false;
      cbUseClick(nil);
      FreeAndNil(FDBDT);
    end;
  finally
    FBlockOnChange := false;
  end;
end;

procedure TDBDTFilterFrm.InitLevelCombo(maxlevel: char);
var
  i: char;
begin
  cbLevel.Items.Clear;
  for i := CLIENT_LEVEL_DIVISION to maxlevel do
    cbLevel.Items.Add( AnsiLowerCase(CDBDTMetadata[i].Name) );
  EnableControl(cbLevel, true);
end;

procedure TDBDTFilterFrm.cbLevelSelect(Sender: TObject);
begin
  cbUse.Checked := true;
  InitForCurrentLevel;
end;

function TDBDTFilterFrm.SelectionToFilterItems: TDBDTFilterItems;
var
  pos: TBookmark;
  i: integer;
begin
  SetLength(Result, dgDBDT.SelectedList.Count);
  FDBDT.DS.DisableControls;
  try
    pos := FDBDT.DS.GetBookmark;
    try
      for i := 0 to dgDBDT.SelectedList.Count-1 do
      begin
        FDBDT.DS.GotoBookmark( dgDBDT.SelectedList[i] );
        Result[i] := FDBDT.KeyValues;
      end;
    finally
      FDBDT.DS.GotoBookmark(pos);
      FDBDT.DS.FreeBookmark(pos);
    end;
  finally
    FDBDT.DS.EnableControls;
  end;
end;

procedure TDBDTFilterFrm.FilterItemsToSelection(items: TDBDTFilterItems);
var
  pos: TBookmark;
  i: integer;
begin
  FDBDT.DS.DisableControls;
  try
    dgDBDT.UnSelectAll;
    pos := FDBDT.DS.GetBookmark;
    try
      for i := 0 to high(items) do
      begin
        if FDBDT.LocateByKeys(items[i]) then
          dgDBDT.SelectedList.Add(FDBDT.DS.GetBookmark);
      end;
    finally
      FDBDT.DS.GotoBookmark(pos);
      FDBDT.DS.FreeBookmark(pos);
    end;
  finally
    FDBDT.DS.EnableControls;
  end;
end;

procedure TDBDTFilterFrm.InitForCurrentLevel;
begin
  if FDBDT.Level <> CLIENT_LEVEL_COMPANY then
    FFilters[FDBDT.Level] := SelectionToFilterItems;

  dgDBDT.UnSelectAll;
  dsDBDT.DataSet := nil;

  FDBDT.Level := Char( Ord(CLIENT_LEVEL_DIVISION) + cbLevel.ItemIndex );

  if FDBDT.Level <> CLIENT_LEVEL_COMPANY then
  begin
    dsDBDT.DataSet := FDBDT.DS;
    FilterItemsToSelection( FFilters[FDBDT.Level] );
  end;
  EnableControl(dgDBDT, FDBDT.Level <> CLIENT_LEVEL_COMPANY);
end;

procedure TDBDTFilterFrm.cbUseClick(Sender: TObject);
begin
  if not cbUse.Checked then
    cbLevel.ItemIndex := -1;
//  EnableControl(cbLevel, cbUse.Checked);
  InitForCurrentLevel;
end;

constructor TDBDTFilterFrm.Create(Owner: TComponent);
begin
  inherited;
  EnableControlRecursively(Self, false);
  dgDBDT.OnMultiSelectRecord := HandleMultiSelectRecord;
  dgDBDT.OnMultiSelectAllRecords := HandleMultiSelectAllRecords;
  dgDBDT.OnMultiSelectReverseRecords := HandleReverseSelectionRecord;
end;

function TDBDTFilterFrm.GetFilter: TDBDTFilter;
begin
  Assert(IsInitialized);
  Result.Use := cbUse.Checked;
  Result.Level := Char(Ord(CLIENT_LEVEL_DIVISION) + cbLevel.ItemIndex);
  Result.Items := SelectionToFilterItems;
end;

function TDBDTFilterFrm.IsInitialized: boolean;
begin
  Result := FDBDT <> nil;
end;

procedure TDBDTFilterFrm.Check;
var
  filter: TDBDTFilter;
begin
  filter := GetFilter;
  if filter.Use and (Length(filter.Items) = 0) then
    raise Exception.Create('D/B/D/T filter is enabled but no items are selected');
end;

procedure TDBDTFilterFrm.HandleMultiSelectRecord(Grid: TwwDBGrid; Selecting: boolean; var Accept: boolean);
begin
  DeferredCall(Changed);
  //DeferredCall is used because this event is called before the selection gets changed
  //Changed is used to fire TOptionsBaseFrm.OnChangeByUser event
end;

procedure TDBDTFilterFrm.HandleMultiSelectAllRecords(Grid: TwwDBGrid; Selecting: boolean; var Accept: boolean);
begin
  DeferredCall(Changed);
end;

procedure TDBDTFilterFrm.HandleReverseSelectionRecord(Grid: TwwDBGrid; var Accept: boolean);
begin
  DeferredCall(Changed);
end;

end.
