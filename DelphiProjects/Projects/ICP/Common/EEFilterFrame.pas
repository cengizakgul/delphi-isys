unit EEFilterFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodesFilterFrame, StdCtrls, ExtCtrls, EESalaryFilterFrame,
  DBDTFilterFrame, isSettings, db, evodata, eefilter, OptionsBaseFrame;

type
  TEeFilterFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    EEStatusFilterFrame: TCodesFilterFrm;
    EEPositionStatusFilterFrame: TCodesFilterFrm;
    EESalaryFilterFrame: TEESalaryFilterFrm;
    DBDTFilterFrame: TDBDTFilterFrm;
  public
    procedure Init(CUSTOM_DBDT: TDataSet; Filter: TEEFilter);
    procedure ClearAndDisable;
    function GetFilter: TEEFilter;
    procedure Check;
  end;

implementation

{$R *.dfm}

uses
  common, gdycommon, comboChoices;

{ TEeFilterFrm }

procedure TEeFilterFrm.Check;
begin
  EEStatusFilterFrame.Check;
  EEPositionStatusFilterFrame.Check;
  EESalaryFilterFrame.Check;
  DBDTFilterFrame.Check;
end;

function TEeFilterFrm.GetFilter: TEEFilter;
begin
  Result.EEStatusFilter := EEStatusFilterFrame.CodesFilter;
  Result.EEPositionStatusFilter := EEPositionStatusFilterFrame.CodesFilter;
  Result.EESalaryFilter := EESalaryFilterFrame.EESalaryFilter;
  Result.DBDTFilter := DBDTFilterFrame.GetFilter;
end;

procedure TEeFilterFrm.Init(CUSTOM_DBDT: TDataSet; Filter: TEEFilter);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);

    EEStatusFilterFrame.Configure(EE_TerminationCode_ComboChoices, 'Status');
    EEPositionStatusFilterFrame.Configure(PositionStatus_ComboChoices, 'Position status');

    EEStatusFilterFrame.CodesFilter := Filter.EEStatusFilter;
    try
      EEPositionStatusFilterFrame.CodesFilter := Filter.EEPositionStatusFilter;
      try
        EESalaryFilterFrame.EESalaryFilter := Filter.EESalaryFilter;
        try
          DBDTFilterFrame.Init(CUSTOM_DBDT, Filter.DBDTFilter);
        except
          EESalaryFilterFrame.Clear(false);
          raise;
        end
      except
        EEPositionStatusFilterFrame.Clear(false);
        raise;
      end;
    except
      EEStatusFilterFrame.Clear(false);
      raise;
    end;

  finally
    FBlockOnChange := false;
  end;
end;

procedure TEeFilterFrm.ClearAndDisable;
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, false);

    EEStatusFilterFrame.Clear(false);
    EEPositionStatusFilterFrame.Clear(false);
    EESalaryFilterFrame.Clear(false);
    DBDTFilterFrame.ClearAndDisable;
  finally
    FBlockOnChange := false;
  end;
end;

end.
