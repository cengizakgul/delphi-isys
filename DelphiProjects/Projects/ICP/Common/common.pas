{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

unit common;

interface

uses
  gdystrset, classes, gdyCommonLogger, db, gdyLoggerImpl, DBClient, evConsts,
  isSettings;

type
  TEvoCompanyDef = record
    ClNbr, CoNbr: integer;
    CUSTOM_COMPANY_NUMBER: string;
    Name: string;
    CUSTOM_CLIENT_NUMBER: string;
    ClientName: string;
  end;

  TNullableEvoCompanyDef = record
    HasValue: boolean;
    Value: TEvoCompanyDef;
  end;

  TPrDef = record
    CheckDate: TDateTime;
    Run: integer;
    PrNbr: integer;
  end;

  TPrDefs = array of TPrDef;

  TPayPeriod = record
    PeriodBegin: TDateTime;
    PeriodEnd: TDateTime;
  end;

  TEvoPayrollBatchDef = record
    Company: TEvoCompanyDef;
    Pr: TPrDef;

    PrBatchNbr: integer;
    PayPeriod: TPayPeriod;
    HasRegularChecks: boolean;
    HasUserEarningsLines: boolean;
  end;

  TEvoPayrollDef = record
    Company: TEvoCompanyDef;
    Pr: TPrDef;
  end;

  TEvoPayrollsDef = record
    Company: TEvoCompanyDef;
    PrNbrs: TPrDefs;
  end;

  THierarchyLevelInfo = record
    Name: string;
    Enabled: boolean;
    Reason: string; //if disabled
  end;

  TEvoDBDTInfo = record
    DBDTLevel: char;
    Levels: array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of THierarchyLevelInfo;
  end;

const
  CLIENT_LEVEL_X_JOB = 'J';

type

  TDateRange = record
    FromDate: TDateTime;
    ToDate: TDateTime;
  end;

  TEvoPayrollFilter = type TDateRange;

  TEvoAPIConnectionParam = record
    APIServerAddress: string;
    APIServerPort: integer;
    Username: string;
    Password: string;
    SavePassword: boolean;
  end;

  TTCImportResult = record
    RejectedRecords: integer;
    LogFile: string;
  end;

const
  sLogFileAlias = 'Log';
{$ifndef FINAL_RELEASE}
  sDebugLogFileAlias = 'DebugLog';
{$endif}
  sCertificateFileAlias = 'Certificate';
  sQueryDirAlias = 'Queries';
  sLogArchiveDirAlias = 'LogArchives';
  sSettingsDirAlias = 'Settings';
  sSevenZipDllAlias = 'SevenZipDll';
  sEvoXMapFileAlias = 'EvoXMap';
  sRatesXMapFileAlias = 'RateEvoXMap';
  sSchedEDsXMapFileAlias = 'SchedEDsEvoXMap';
  sTOAEvoXMapFileAlias = 'TOAEvoXMap';
  sHtmlTemplatesDirAlias = 'HTMLTemplates';
  sSchedulerDataDirAlias = 'SchedulerData';
  sTaskRunnerFileAlias = 'TaskRunner';
  sWebServiceFileAlias = 'WebService';
  sConfigFileAlias = 'Config';
  sRootCertificateFileAlias = 'RootCertificate';
  sKeyCertificateFileAlias = 'KeyCertificate';
  sOverrideEmail = 'OverrideEmail';

{$ifndef FINAL_RELEASE}
  sDumpDirAlias = 'DumpDir';
{$endif}

const
  sCtxEECode = 'Employee Code';
  sCtxComponent = 'Component';

type
  ILogRecorder = interface
  ['{1E72D91D-7C68-42BD-BF16-C063EA1F8C6D}']
    procedure Start(html: boolean);
    function Stop: string;
  end;

function DateToStdDate(d: TDateTime): string;
function StdDateToDate(s: string): TDateTime;
function StdDateTimeToDateTime(s: string): TDateTime;
function StringSetToCsv(strs: TStringSet): string;
function StringArrayToCsv(strs: array of string): string;
procedure PrintString(s: string);


procedure LogEvoCompanyDef(Logger: ICommonLogger; company: TEvoCompanyDef);
procedure SaveEvoCompanyDef( const clco: TEvoCompanyDef; conf: IisSettings; root: string);
function LoadEvoCompanyDef(conf: IisSettings; root: string): TEvoCompanyDef;
function EvoCompanyDefsAreEqual(const one: TEvoCompanyDef; const another: TEvoCompanyDef): boolean;

procedure LogNullableEvoCompanyDef(Logger: ICommonLogger; company: TNullableEvoCompanyDef);
function NullableEvoCompanyDefsAreEqual(const one: TNullableEvoCompanyDef; const another: TNullableEvoCompanyDef): boolean;
function NullableEvoCompanyDefsClientsAreEqual(const one: TNullableEvoCompanyDef; const another: TNullableEvoCompanyDef): boolean;

procedure LogEvoPayrollDef(Logger: ICommonLogger; payroll: TEvoPayrollDef);
procedure LogEvoPayrollsDef(Logger: ICommonLogger; payrolls: TEvoPayrollsDef);
procedure LogEvoPayrollBatchDef(Logger: ICommonLogger; batch: TEvoPayrollBatchDef; logCompany: boolean = true);

function MakeASCIIReportReallyEmpty(report: string): string;

function CreateStringField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string; aSize: integer): TStringField;
function CreateIntegerField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TIntegerField;
function CreateDateTimeField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TDateTimeField;
function CreateFloatField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TFloatField;
function CreateBooleanField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TBooleanField;

function ExtractUniqueContextItem(aLogState: TLogState; ctxtag: string): Variant;
function MakePrefixedMessage(aLogState: TLogState; ctxtag: string): string;

procedure CreateOrEmptyDataSet(cds: TClientDataSet);
procedure Append( ds: TDataSet; aFields: string; vals: array of Variant );


function GetEvoNameByCode(code: string; def: string): string;

procedure AddSelected( aSelected: TStrings; aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean); overload;
procedure AddSelected(aSelected: TStrings; f: TField); overload;

function CompanyUniquePath(const company: TEvoCompanyDef): string;
function ClientUniquePath(const company: TEvoCompanyDef): string;

function VarHasValue(v: Variant): boolean;

function TrimLeadingZeroes(s: string): string;
function MajorVersionAsString: string;
function CreateSomeDirectory(basedir: string): string;
procedure SaveHtmlLogFileTo(htmllog: string; fn: string);
function GetISystemsPassword: string;
function GetISystemsEMail: string;

procedure GetDebugLogFiles(fList: TStrings);

function LoadEvoAPIConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TEvoAPIConnectionParam;
procedure SaveEvoAPIConnectionParam( const param: TEvoAPIConnectionParam; conf: IisSettings; root: string);

var
  ConfigVersion: integer;
  FeatureSetName: string; //'Advanced' or 'Basic' or ''

function AppSettings: IisSettings;
//assumes that product versions in file version info are the same
function SiblingAppSettings(filename: string): IisSettings; //!! ugly - assumes that FeatureSet is the same

procedure OpenInExplorer(fn: string);

function GetVersion: string;

implementation

uses
  sysutils, cswriter, dialogs, printers, variants, gdyclasses, strutils,
  windows, gdycommon, gdyRedir, gdyCrypt, gdyUtils;

procedure GetDebugLogFiles(fList: TStrings);
var
  Dir: string;
  r: TSearchRec;
begin
  fList.Clear;
  Dir := GetCurrentDir;
  if FindFirst(Dir + '\debug_*.log', faAnyFile, r) = 0 then
  repeat
    fList.Add(r.Name);
  until FindNext(r) <> 0;
end;

procedure PrintString(s: string);
var
  MyFile: TextFile;
begin
  with TPrintDialog.Create(nil) do
  try
    if Execute then
    begin
      AssignPrn(MyFile);
      Rewrite(MyFile);
      Writeln(MyFile, s);
      System.CloseFile(MyFile);
    end;
  finally
    Free;
  end;
end;

function DateToStdDate(d: TDateTime): string;
begin
  Result := FormatDateTime('yyyy-mm-dd', d);
end;

function StdDateToDate(s: string): TDateTime;
var
  fs: TFormatSettings;
begin
//  GetLocaleFormatSettings( ,fs);
  fs.DateSeparator := '-';
  fs.ShortDateFormat := 'yyyy-mm-dd';
  Result := StrToDate(s, fs);
end;

function GetVersion: string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function StdDateTimeToDateTime(s: string): TDateTime;
var
  dt, tm: string;
  p: integer;
begin
  s := trim(s);
  p := Pos('T', s);
  if p <> 0 then //swipeclock returns only date for midnight
  begin               
    dt := copy(s, 1, p-1);
    tm := copy(s, p+1, length(s)-p);
    Result := StdDateToDate(dt);
    if tm <> '' then
      Result := Result + StrToTime(tm);
  end
  else
    Result := StdDateToDate(s);
end;
//copy-pasted to StringArrayToCsv
function StringSetToCsv(strs: TStringSet): string;
var
  wr: TCSWriter;
  ss: TStringStream;
  i: integer;
begin
  ss := TStringStream.Create('');
  try
    wr := TCSWriter.Create( ss, false );
    try
      for i := 0 to high(strs) do
        wr.Write(strs[i]);
    finally
      FreeAndNil( wr );
    end;
    Result := ss.DataString;
  finally
    FreeAndNil(ss);
  end;
end;

//!! copy-paste from StringSetToCsv
function StringArrayToCsv(strs: array of string): string;
var
  wr: TCSWriter;
  ss: TStringStream;
  i: integer;
begin
  ss := TStringStream.Create('');
  try
    wr := TCSWriter.Create( ss, false );
    try
      for i := 0 to high(strs) do
        wr.Write(strs[i]);
    finally
      FreeAndNil( wr );
    end;
    Result := ss.DataString;
  finally
    FreeAndNil(ss);
  end;
end;

function MakePrInfo(prNbrs: array of TPrDef; glue: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(prNbrs) do
  begin
    if Result <> '' then
      Result := Result + glue;
    Result := Result + inttostr(prNbrs[i].PrNbr);
  end;
end;

function MakePrInfo2(prNbrs: array of TPrDef; glue: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(prNbrs) do
  begin
    if Result <> '' then
      Result := Result + glue;
    Result := Result + Format('%s (run #%d)',[datetostr(prNbrs[i].CheckDate), prNbrs[i].Run]);
  end;
end;

procedure LogEvoCompanyDef(Logger: ICommonLogger; company: TEvoCompanyDef);
begin
  Logger.LogContextItem('Client#', company.CUSTOM_CLIENT_NUMBER );
  Logger.LogContextItem('Client', company.ClientName );
  Logger.LogContextItem('Company#', company.CUSTOM_COMPANY_NUMBER );
  Logger.LogContextItem('Company', company.Name );
  Logger.LogContextItem('Internal Client#', inttostr(company.ClNbr) );
  Logger.LogContextItem('Internal Company#', inttostr(company.CoNbr) );
end;

procedure LogEvoPayrollDef(Logger: ICommonLogger; payroll: TEvoPayrollDef);
begin
  LogEvoCompanyDef(Logger, payroll.Company);
  Logger.LogContextItem('Pr', Format('%s (run #%d)',[datetostr(payroll.Pr.CheckDate), payroll.Pr.Run]) );
  Logger.LogContextItem('Internal Pr#', inttostr(payroll.Pr.PrNbr) );
end;

procedure LogEvoPayrollsDef(Logger: ICommonLogger; payrolls: TEvoPayrollsDef);
begin
  LogEvoCompanyDef(Logger, payrolls.Company);
  Logger.LogContextItem('Pr', MakePrInfo2(payrolls.PrNbrs, ', ') );
  Logger.LogContextItem('Internal Pr#', MakePrInfo(payrolls.PrNbrs, ', ') );
end;

procedure LogEvoPayrollBatchDef(Logger: ICommonLogger; batch: TEvoPayrollBatchDef; logCompany: boolean);
begin
	if logCompany then
	  LogEvoCompanyDef(Logger, batch.Company);
  Logger.LogContextItem('Pr', Format('%s (run #%d)',[datetostr(batch.Pr.CheckDate), batch.Pr.Run]) );
  Logger.LogContextItem('Internal Pr#', inttostr(batch.Pr.PrNbr) );
  Logger.LogContextItem('Internal Batch#', inttostr(batch.PrBatchNbr) );
end;

function MakeASCIIReportReallyEmpty(report: string): string;
begin
  if copy(report, 1, 4) = 'TPF0' then
    Result := ''
  else
    Result := report;
end;

//from sRemotePayrollUtils
function CreateStringField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string; aSize: integer): TStringField;
var
  TS: TStringField;
begin
  TS := TStringField.Create(aDataSet);
  with TS do
  begin
    FieldName := aFieldName;
    Size := aSize;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;
  result := TS;
end;

function CreateIntegerField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TIntegerField;
var
  TI: TIntegerField;
begin
  TI := TIntegerField.Create(aDataSet);
  with TI do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TI;
end;

function CreateDateTimeField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TDateTimeField;
var
  TS: TDateTimeField;
begin
  TS := TDateTimeField.Create(aDataSet);
  with TS do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;
  result := TS;
end;

function CreateFloatField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TFloatField;
var
  TF: TFloatField;
begin
  TF := TFloatField.Create(aDataSet);
  with TF do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TF;
end;

function CreateBooleanField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TBooleanField;
var
  TF: TBooleanField;
begin
  TF := TBooleanField.Create(aDataSet);
  with TF do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TF;
end;

//Result.CommaTextFormat := TkbmStreamFormat.Create(Result);
//FLogger.LogDebug('Dataset', Result.CommaText);

function ExtractUniqueContextItem(aLogState: TLogState; ctxtag: string): Variant;
var
  i, j: integer;
  block: TLogBlock;
begin
  Result := Unassigned;
  for i := aLogState.BlockStack.Count-1 downto 0 do
  begin
    block := aLogState.BlockStack[i];
    for j := block.Context.Count-1 downto 0 do
      if block.Context[j].ContextTag = ctxtag then
      begin
        Assert( VarIsEmpty(Result) or (trim(Result) = trim(block.Context[j].ContextValue)) );
        Result := block.Context[j].ContextValue;
      end;
  end;
end;

function MakePrefixedMessage(aLogState: TLogState; ctxtag: string): string;
var
  val: Variant;
begin
  try
    val := ExtractUniqueContextItem(aLogState, ctxtag)
  except
    //don't want to risk losing log message
  end;
  if trim(VarToStr(val)) <> '' then
    Result := VarToStr(val) + ': ' + aLogState.Last.Messages.Last.Text
  else
    Result := aLogState.Last.Messages.Last.Text;
end;

procedure CreateOrEmptyDataSet(cds: TClientDataSet);
begin
  if not cds.Active then
    cds.CreateDataSet
  else
    cds.EmptyDataSet;
end;

procedure Append( ds: TDataSet; aFields: string; vals: array of Variant );
begin
  ds.Append;
  try
    if Length(vals) > 1 then
      ds[aFields] := VarArrayOf(vals)
    else
      ds[aFields] := vals[0];
    ds.Post;
  except
    ds.Cancel;
    raise;
  end;
end;

function GetEvoNameByCode(code: string; def: string): string;
var
  lines: IStr;
  i: integer;
begin
  Result := '???';
  lines := SplitToLines(def);
  for i := 0 to lines.Count-1 do
    with SplitByChar(lines.Str[i], #9) do
      if Str[1] = code then
      begin
        Result := Str[0];
        Exit;
      end;
//  raise Exception.CreateFmt('Cannot find name for code "%s"', [code]); 
// commented because this function may be called before establishing connection to Evo, in that case it would use hardcocded codes 
end;

function BuildSelected( aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean): string;
begin
  Result := aFieldName+#9+IntToStr(aWidth)+#9+aCaption+#9;
  if bRO then
    Result := Result + 'T'
  else
    Result := Result + 'F';
end;

procedure AddSelected( aSelected: TStrings; aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean); overload;
begin
  aSelected.Add( BuildSelected(aFieldName, aWidth, aCaption, bRO) );
end;

procedure AddSelected(aSelected: TStrings; f: TField); overload;
begin
  aSelected.Add( BuildSelected(f.FieldName, f.DisplayWidth, f.DisplayName, f.ReadOnly) );
end;

function CompanyUniquePath(const company: TEvoCompanyDef): string;
begin
  Result := ClientUniquePath(company) + '\PerCompanySettings\' + Inttostr(company.CoNbr);
end;

function ClientUniquePath(const company: TEvoCompanyDef): string;
begin
  Result := 'PerClientSettings\' + Inttostr(company.ClNbr);
end;

function VarHasValue(v: Variant): boolean;
begin
  Result := not VarIsNull(v) and not VarIsEmpty(v);
end;

function TrimLeadingZeroes(s: string): string;
begin
  Result := s;
  while (Length(Result) > 0) and (Result[1] = '0') do
    Result := RightStr(Result, Length(Result)-1);
end;

function MajorVersionAsString: string;
var
  FileName: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
begin
  Result := '';
  FileName := ParamStr(0);
  InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
        if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
        begin
          Assert(FI.dwProductVersionMS = FI.dwFileVersionMS);
          Result := inttostr( (FI.dwProductVersionMS shr 16) and $ffff);
        end;
    finally
      FreeMem(VerBuf);
    end;
  end;
  Assert(Result <> '');
end;

function CreateSomeDirectory(basedir: string): string;
var
  exists: boolean;
  i: integer;
begin
  Result := basedir;
  exists := DirectoryExists(Result);
  i := 1;
  while exists and (i < 100) do
  begin
    Result := Format('%s (%d)', [basedir, i]);
    exists := DirectoryExists(Result);
    i := i + 1;
  end;
  Win32Check( ForceDirectories(Result) );
end;

procedure SaveHtmlLogFileTo(htmllog: string; fn: string);
var
  imgdir: string;
  destimgdir: string;
begin
  Win32Check(ForceDirectories(ExtractFileDir(fn)));
  StringToFile(htmllog, fn);
  Assert(ChangeFileExt(ExtractFileName(fn), '') = 'Log');
  destimgdir := ChangeFileExt(fn, '') + '_files';
  Win32Check(CreateDirectory(pchar(destimgdir), nil));
  imgdir := Redirection.GetDirectory(sHtmlTemplatesDirAlias) + 'Log_files'; 
  Win32Check(CopyFile(pchar(imgdir + '\error.png'), pchar(destimgdir + '\error.png'), true) );
  Win32Check(CopyFile(pchar(imgdir + '\warning.png'), pchar(destimgdir + '\warning.png'), true) );
  Win32Check(CopyFile(pchar(imgdir + '\info.png'), pchar(destimgdir + '\info.png'), true) );
end;

function GetISystemsPassword: string;
begin
  Result :=  XORIt(#$32#$1E#$7B#$31#$37#$62#$7F#$15#$13, $5a);        //'hD!km8%OI';
end;

function GetISystemsEMail: string;
begin
//{$ifdef PRODUCTION_LICENCE}
  Result := XORIt(#$3B#$34#$3E#$28#$3F#$33#$1A#$33#$29#$23#$29#$2E#$3F#$37#$29#$36#$36#$39#$74#$39#$35#$37, $5a); //'andrei@isystemsllc.com';
//{$else}
//  Result := 'gerasimov@gmail.com';
//{$endif}
end;

const
  //!! encrypt it?
  cKey='Form.Button';

function LoadEvoAPIConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TEvoAPIConnectionParam;
begin
  root := root + IIF(root='','','\') + 'EvolutionConnection\';
  try
    Logger.LogDebug('XXX: key='+root+'APIServerAddress');
    Result.APIServerAddress := conf.AsString[root+'APIServerAddress'];
    Logger.LogDebug('XXX: val='+Result.APIServerAddress);
  except
    Logger.StopException;
  end;
  try
    Result.APIServerPort := conf.GetValue(root+'APIServerPort', 9943);
  except
    Logger.StopException;
  end;
  try
    Result.Username := conf.AsString[root+'Username'];
  except
    Logger.StopException;
  end;
  try
    if conf.GetValueNames(WithoutTrailingSlash(root)).IndexOf('Password') <> -1 then
      Result.Password := conf.AsString[root+'Password']
    else
      Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  except
    Logger.StopException;
  end;
  try
    Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  except
    Logger.StopException;
  end;
end;

procedure SaveEvoAPIConnectionParam( const param: TEvoAPIConnectionParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EvolutionConnection\';
  conf.AsString[root+'APIServerAddress'] := param.APIServerAddress;
  conf.AsInteger[root+'APIServerPort'] := param.APIServerPort;
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
end;

function AppSettings: IisSettings;
var
  keypath: string;
begin
  keypath := 'Software\iSystems\' + ChangeFileExt(GetAppFilename, '');
  if ConfigVersion > 0 then
    keypath := keypath + '\' + IntToStr(ConfigVersion);
  if FeatureSetName <> '' then
    keypath := keypath + '\' + FeatureSetName;
  Result := TisSettingsRegistry.Create(HKEY_CURRENT_USER, keypath);
end;

function SiblingAppSettings(filename: string): IisSettings;
var
  keypath: string;
begin
  keypath := 'Software\iSystems\' + ChangeFileExt(ExtractFileName(filename), '');
  if ConfigVersion > 0 then
    keypath := keypath + '\' + IntToStr(ConfigVersion);
  if FeatureSetName <> '' then
    keypath := keypath + '\' + FeatureSetName;
  Result := TisSettingsRegistry.Create(HKEY_CURRENT_USER, keypath);
end;

procedure SaveEvoCompanyDef( const clco: TEvoCompanyDef; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EvoCompany\';
  conf.AsInteger[root + 'CL_NBR'] := ClCo.ClNbr;
  conf.AsInteger[root + 'CO_NBR'] := ClCo.CoNbr;
  conf.AsString[root + 'CUSTOM_COMPANY_NUMBER'] := ClCo.CUSTOM_COMPANY_NUMBER;
  conf.AsString[root + 'Name'] := ClCo.Name;
  conf.AsString[root + 'CUSTOM_CLIENT_NUMBER'] := ClCo.CUSTOM_CLIENT_NUMBER;
  conf.AsString[root + 'ClientName'] := ClCo.ClientName;
end;

function LoadEvoCompanyDef(conf: IisSettings; root: string): TEvoCompanyDef;
begin
  root := root + IIF(root='','','\') + 'EvoCompany\';
  Result.ClNbr := conf.AsInteger[root + 'CL_NBR'];
  Result.CoNbr := conf.AsInteger[root + 'CO_NBR'];
  Result.CUSTOM_COMPANY_NUMBER := conf.AsString[root + 'CUSTOM_COMPANY_NUMBER'];
  Result.Name := conf.AsString[root + 'Name'];
  Result.CUSTOM_CLIENT_NUMBER := conf.AsString[root + 'CUSTOM_CLIENT_NUMBER'];
  Result.ClientName := conf.AsString[root + 'ClientName'];

  if Result.ClNbr <= 0 then
    raise Exception.CreateFmt('Invalid internal client number: %d', [Result.ClNbr]);
  if Result.CoNbr <= 0 then
    raise Exception.CreateFmt('Invalid internal company number: %d', [Result.CoNbr]);
  if trim(Result.CUSTOM_CLIENT_NUMBER) = '' then
    raise Exception.CreateFmt('Empty client code', [Result.CUSTOM_COMPANY_NUMBER]);
  if trim(Result.CUSTOM_COMPANY_NUMBER) = '' then
    raise Exception.CreateFmt('Empty company code', [Result.CUSTOM_COMPANY_NUMBER]);
end;

function EvoCompanyDefsAreEqual(const one: TEvoCompanyDef; const another: TEvoCompanyDef): boolean;
begin
  Result := (one.ClNbr = another.ClNbr) and (one.CoNbr = another.CoNbr);
end;

function NullableEvoCompanyDefsAreEqual(const one: TNullableEvoCompanyDef; const another: TNullableEvoCompanyDef): boolean;
begin
  Result := (one.HasValue = another.HasValue) and (not one.HasValue or EvoCompanyDefsAreEqual(one.Value, another.Value) );
end;

function NullableEvoCompanyDefsClientsAreEqual(const one: TNullableEvoCompanyDef; const another: TNullableEvoCompanyDef): boolean;
begin
  Result := (one.HasValue = another.HasValue) and (not one.HasValue or (one.Value.ClNbr = another.Value.ClNbr));
end;
procedure LogNullableEvoCompanyDef(Logger: ICommonLogger; company: TNullableEvoCompanyDef);
begin
  Logger.LogContextItem('Company selected', BoolToStr(company.HasValue, true));
  if company.HasValue then
    LogEvoCompanyDef(Logger, company.Value);
end;

procedure OpenInExplorer(fn: string);
var
  SI: STARTUPINFO;
  PI: PROCESS_INFORMATION;
  arg: string;
begin
  arg := Format('explorer.exe /select, "%s"', [fn]);
  ZeroMemory(@SI, SizeOf(SI));
  ZeroMemory(@PI, SizeOf(PI));
  Win32Check( CreateProcess(nil, PChar(arg), nil, nil, False, 0, nil, nil, SI, PI) );
  CloseHandle(PI.hThread);
  CloseHandle(PI.hProcess);
end;

end.
