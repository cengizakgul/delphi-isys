unit EvolutionCompanyFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, EvolutionDSFrame, kbmMemTable, evoapiconnection, common,
  gdycommonlogger, gdycommon, db, evodata;

type
  TEvolutionCompanyFrm = class(TFrame)
    frmCL: TEvolutionDsFrm;
    Splitter1: TSplitter;
    frmCO: TEvolutionDsFrm;
  public
    procedure FillTables(conn: IEvoAPIConnection);
    procedure ClearTables;
    function CanGetClCo: boolean;
    function GetClCo: TEvoCompanyDef;
    function GetClientList: TIntegerArray;
  public
    Logger: ICommonLogger;
  private
    FCL: TkbmCustomMemTable;
    FCO: TkbmCustomMemTable;
  end;

implementation

{$R *.dfm}

uses
  gdyclasses, gdyRedir;

{ TEvolutionPayrollFrm }

function TEvolutionCompanyFrm.CanGetClCo: boolean;
begin
  Result := assigned(FCL) and assigned(FCO) and not FCL.Eof and not FCO.Eof;
end;

procedure TEvolutionCompanyFrm.ClearTables;
begin
  FreeAndNil(FCL);
  FreeAndNil(FCO);
end;

procedure TEvolutionCompanyFrm.FillTables(conn: IEvoAPIConnection);
begin
  ClearTables;
  (Application.MainForm as IProgressIndicator).StartWait('Getting company list', 2);
  try
    FCL := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'qClList.rwq');
    (Application.MainForm as IProgressIndicator).Step(1);
    FCO := conn.RunQuery( Redirection.GetDirectory(sQueryDirAlias) + 'qCoList.rwq');
    (Application.MainForm as IProgressIndicator).Step(2);
  finally
    (Application.MainForm as IProgressIndicator).EndWait;
  end;

  FCL.FieldByName('CUSTOM_CLIENT_NUMBER').DisplayLabel := 'Number';
  FCL.FieldByName('NAME').DisplayLabel := 'Name';
  FCL.FieldByName('CL_NBR').DisplayLabel := 'Internal Cl#';

  FCO.FieldByName('CUSTOM_COMPANY_NUMBER').DisplayLabel := 'Number';
  FCO.FieldByName('NAME').DisplayLabel := 'Name';
  FCO.FieldByName('CO_NBR').DisplayLabel := 'Internal Co#';
  FCO.FieldByName('CL_NBR').Visible := false;

  frmCL.ds.DataSet := FCL;

  FCO.MasterSource := frmCL.ds;
  FCO.MasterFields := 'CL_NBR';
  FCO.DetailFields := 'CL_NBR';

  frmCO.ds.DataSet := FCO;

  FCL.First;
  FCO.First;
end;

function TEvolutionCompanyFrm.GetClCo: TEvoCompanyDef;
begin
  Assert(CanGetClCo);
  Result.ClNbr := FCO['CL_NBR'];
  Result.CoNbr := FCO['CO_NBR'];
  Result.CUSTOM_COMPANY_NUMBER := FCO['CUSTOM_COMPANY_NUMBER'];
  Result.Name := FCO['Name'];
  //!! this frame is deprecated anyway
  Result.CUSTOM_CLIENT_NUMBER := '<unavailable>';
  Result.ClientName := '<unavailable>';
end;

function TEvolutionCompanyFrm.GetClientList: TIntegerArray;
var
  bmk: TBookmark;
  i: integer;
begin
  Assert( assigned(FCL) );
  SetLength(Result, FCL.RecordCount);
  FCL.DisableControls;
  try
    bmk := FCL.GetBookMark;
    try
      try
        i := 0;
        FCL.First;
        while not FCL.Eof do
        begin
          Result[i] := FCL['CL_NBR'];
          inc(i);
          FCL.Next;
        end;
      finally
        FCL.GotoBookmark( bmk );
      end;
    finally
      FCL.FreeBookmark( bmk );
    end;
  finally
    FCL.EnableControls;
  end;
end;

end.

