inherited EvoAPIClientNewMainFm: TEvoAPIClientNewMainFm
  Left = 458
  Top = 296
  Caption = 'EvoAPIClientNewMainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Top = 41
    Height = 456
    ActivePage = TabSheet2
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 784
        Align = alTop
        inherited GroupBox1: TGroupBox
          Width = 784
          inherited cbSavePassword: TCheckBox
            Width = 338
            Caption = 'Save password (mandatory for scheduling tasks)'
          end
        end
      end
    end
    object tbshCompanySettings: TTabSheet [1]
      Caption = 'Company settings'
      ImageIndex = 3
    end
    inherited TabSheet2: TTabSheet
      object pnlBottom: TPanel
        Left = 0
        Top = 387
        Width = 784
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
    object tbshScheduler: TTabSheet [3]
      Caption = 'Scheduled tasks'
      ImageIndex = 4
      inline SchedulerFrame: TSchedulerFrm
        Left = 0
        Top = 0
        Width = 784
        Height = 428
        Align = alClient
        TabOrder = 0
        inherited pnlTasksControl: TPanel
          Width = 784
        end
        inherited PageControl2: TPageControl
          Width = 784
          Height = 387
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 133
              Width = 776
            end
            inherited pnlTasks: TPanel
              Width = 776
              Height = 133
              inherited dgTasks: TReDBGrid
                Width = 776
                Height = 92
              end
              inherited Panel1: TPanel
                Top = 92
                Width = 776
              end
            end
            inherited pnlDetails: TPanel
              Top = 140
              Width = 776
              inherited pcTaskDetails: TPageControl
                Width = 776
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 618
                  end
                  inherited dgResults: TReDBGrid
                    Width = 618
                  end
                end
              end
            end
          end
        end
      end
    end
    object tbshSchedulerSettings: TTabSheet [4]
      Caption = 'Scheduler settings'
      ImageIndex = 5
      inline SmtpConfigFrame: TSmtpConfigFrm
        Left = 0
        Top = 0
        Width = 784
        Height = 180
        Align = alTop
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 784
        end
      end
    end
  end
  object pnlCompany: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 97
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn4: TBitBtn
        Left = 8
        Top = 8
        Width = 73
        Height = 25
        Action = ConnectToEvo
        Caption = 'Get Evolution and XXX data'
        TabOrder = 0
      end
    end
    inline CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 97
      Top = 0
      Width = 695
      Height = 41
      Align = alClient
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 316
    Top = 592
    object RunAction: TAction
      Caption = 'Import timeclock data'
    end
    object ConnectToEvo: TAction
      Caption = 'Get Evolution and XXX data'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object RunEmployeeExport: TAction
      Caption = 'Update XYZ Employee Records for Selected Company'
    end
  end
end
