unit EeUtilityLoggerViewFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdySendMailLoggerView, gdyLoggerRichView, ExtCtrls, common,
  comctrls, gdyLoggerImpl, StdCtrls, Buttons, ActnList;

type
  TEeUtilityLoggerViewFrm = class(TSendMailLoggerViewFrame)
  private
    procedure HandleMessageLogged(item: TListItem; aLogState: TLogState);
  public
    constructor Create( Owner: TComponent ); override;
  end;

implementation

{$R *.dfm}

{ TEeUtilityLoggerViewFrm }

constructor TEeUtilityLoggerViewFrm.Create(Owner: TComponent);
begin
  inherited;
  LoggerRichView.OnMessageLogged := HandleMessageLogged;
end;

procedure TEeUtilityLoggerViewFrm.HandleMessageLogged(item: TListItem; aLogState: TLogState);
begin
  item.Caption := '';
  item.SubItems.Add('');
  item.SubItems.Add( MakePrefixedMessage(aLogState, sCtxComponent) );
  try
    item.SubItems[0] := ExtractUniqueContextItem(aLogState, sCtxEECode)
  except
    item.SubItems[0] := '?';
    raise;
  end;
end;

end.
