unit EESalaryFilterFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, EEFilter, OptionsBaseFrame;

type
  TEESalaryFilterFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    cbUse: TCheckBox;
    rbSalary: TRadioButton;
    rbHourly: TRadioButton;
    procedure cbUseClick(Sender: TObject);
    procedure rbClick(Sender: TObject);
  private
    function IsInitialized: boolean;

    function GetEESalaryFilter: TEESalaryFilter;
    procedure SetEESalaryFilter(const Value: TEESalaryFilter);
  public
    property EESalaryFilter: TEESalaryFilter read GetEESalaryFilter write SetEESalaryFilter;

    constructor Create( Owner: TComponent ); override;
    procedure Clear(enable: boolean);
    procedure Check;
  end;


implementation

uses
  gdycommon;

{$R *.dfm}

{ TEESalaryFilterFrm }

constructor TEESalaryFilterFrm.Create(Owner: TComponent);
begin
  inherited;
  Clear(false);
end;

procedure TEESalaryFilterFrm.cbUseClick(Sender: TObject);
begin
  if not cbUse.Checked then
  begin
    rbSalary.Checked := false;
    rbHourly.Checked := false;
  end;
end;

function TEESalaryFilterFrm.IsInitialized: boolean;
begin
  Result := cbUse.Enabled;
end;

procedure TEESalaryFilterFrm.rbClick(Sender: TObject);
begin
  cbUse.Checked := true;
end;

procedure TEESalaryFilterFrm.Check;
begin
  Assert(IsInitialized);
  if cbUse.Checked and not rbSalary.Checked and not rbHourly.Checked then
    raise Exception.Create('Salary filter is enabled but neither Hourly nor Salary is selected');
end;

procedure TEESalaryFilterFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    rbSalary.Checked := false;
    rbHourly.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

function TEESalaryFilterFrm.GetEESalaryFilter: TEESalaryFilter;
begin
  Assert(IsInitialized);
  Result.Use := cbUse.Checked;
  Result.Salaried := rbSalary.Checked;
end;

procedure TEESalaryFilterFrm.SetEESalaryFilter(const Value: TEESalaryFilter);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);

    cbUse.Checked := Value.Use;
    if Value.Use then
    begin
      rbSalary.Checked := Value.Salaried;
      rbHourly.Checked := not Value.Salaried;
    end
    else
    begin
      rbSalary.Checked := false;
      rbHourly.Checked := false;
    end;
  finally
    FBlockOnChange := false;
  end;
end;

end.
