unit CustomBinderBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyBinder, BinderFrame;

type
  TCustomBinderBaseFrm = class(TFrame)
    BinderFrm1: TBinderFrm;
  private
    function GetOnMatchTableChanged: TMatchTableChangedEvent;
    procedure SetOnMatchTableChanged(const Value: TMatchTableChangedEvent);
  protected
    FBindingKeeper: IBindingKeeper;
  public
    function Matcher: IMatcher;
    function CanCreateMatcher: boolean;
    procedure UnInit;
    function SaveToString: string;
    function CanSave: boolean;
    property OnMatchTableChanged: TMatchTableChangedEvent read GetOnMatchTableChanged write SetOnMatchTableChanged;
  end;

implementation

{$R *.dfm}

function TCustomBinderBaseFrm.Matcher: IMatcher;
begin
  Assert(FBindingKeeper <> nil);
  Result := FBindingKeeper.CreateMatcher;
end;

function TCustomBinderBaseFrm.CanCreateMatcher: boolean;
begin
  Result := (FBindingKeeper <> nil) and FBindingKeeper.CanCreateMatcher;
end;

procedure TCustomBinderBaseFrm.UnInit;
begin
  if FBindingKeeper <> nil then
  begin
    FBindingKeeper.SetTables(nil, nil); //I don't need this if binding keeper gets actually destroyed here
    FBindingKeeper.SetVisualBinder(nil);
    FBindingKeeper := nil;
  end
end;

function TCustomBinderBaseFrm.SaveToString: string;
begin
  Assert(assigned(FBindingKeeper));
  Result := FBindingKeeper.SaveMatchTableToString;
end;


function TCustomBinderBaseFrm.CanSave: boolean;
begin
  Result := assigned(FBindingKeeper);
end;

function TCustomBinderBaseFrm.GetOnMatchTableChanged: TMatchTableChangedEvent;
begin
  Result := BinderFrm1.OnMatchTableChanged;
end;

procedure TCustomBinderBaseFrm.SetOnMatchTableChanged(const Value: TMatchTableChangedEvent);
begin
  BinderFrm1.OnMatchTableChanged := Value;
end;

end.
