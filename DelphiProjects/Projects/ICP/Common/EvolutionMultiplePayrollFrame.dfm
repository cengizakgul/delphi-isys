object EvolutionMultiplePayrollFrm: TEvolutionMultiplePayrollFrm
  Left = 0
  Top = 0
  Width = 881
  Height = 417
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 636
    Top = 0
    Height = 417
  end
  inline EvoCompany: TEvolutionCompanyFrm
    Left = 0
    Top = 0
    Width = 636
    Height = 417
    Align = alLeft
    TabOrder = 0
    inherited Splitter1: TSplitter
      Height = 417
    end
    inherited frmCL: TEvolutionDsFrm
      Height = 417
      inherited dgGrid: TReDBGrid
        Height = 392
      end
    end
    inherited frmCO: TEvolutionDsFrm
      Height = 417
      inherited dgGrid: TReDBGrid
        Height = 392
      end
    end
  end
  inline frmPR: TEvolutionDsFrm
    Left = 639
    Top = 0
    Width = 242
    Height = 417
    Align = alClient
    TabOrder = 1
    inherited Panel1: TPanel
      Width = 242
      Caption = 'Payrolls'
    end
    inherited dgGrid: TReDBGrid
      Width = 242
      Height = 392
      MultiSelectOptions = [msoAutoUnselect, msoShiftSelect]
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgMultiSelect]
    end
  end
end
