unit EvolutionClCoFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, EvolutionDSFrame, evodata;

type
  TEvolutionClCoFrm = class(TFrame)
    ClientFrame: TEvolutionDsFrm;
    Splitter1: TSplitter;
    CompanyFrame: TEvolutionDsFrm;
    procedure FrameResize(Sender: TObject);
  private
  public
    procedure Init(evoData: TEvoData);
  end;

implementation

{$R *.dfm}

uses
  common;
{ TEvolutionClCoFrm }

procedure TEvolutionClCoFrm.Init(evoData: TEvoData);
begin
  ClientFrame.dgGrid.Selected.Clear;
  AddSelected(ClientFrame.dgGrid.Selected, 'CUSTOM_CLIENT_NUMBER', 0, 'Number', true);
  AddSelected(ClientFrame.dgGrid.Selected, 'NAME', 0, 'Name', true);
  AddSelected(ClientFrame.dgGrid.Selected, 'CL_NBR', 0, 'Internal Cl#', true);
  ClientFrame.Init(evoData, 'TMP_CL');

  CompanyFrame.dgGrid.Selected.Clear;
  AddSelected(CompanyFrame.dgGrid.Selected, 'CUSTOM_COMPANY_NUMBER', 0, 'Number', true);
  AddSelected(CompanyFrame.dgGrid.Selected, 'NAME', 0, 'Name', true);
  AddSelected(CompanyFrame.dgGrid.Selected, 'CO_NBR', 0, 'Internal Co#', true);
  CompanyFrame.Init(evoData, 'TMP_CO');
end;

procedure TEvolutionClCoFrm.FrameResize(Sender: TObject);
begin
  ClientFrame.Width := (Width - Splitter1.Width) div 2;
end;

end.
