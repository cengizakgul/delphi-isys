unit evoapiconnection;

interface

uses
   XmlRpcClient, XmlRpcCommon, XmlRpcTypes, kbmMemTable, ExtCtrls,
   gdycommonlogger, timeclockimport, common, sysutils;

type
  TEvoReportDef = record
    Params: IRpcStruct;
    Level: string;
    Number: integer;
  end;

  TEvoPrReportDef = record
    PrNbr: integer;
    CheckDate: TDatetime;
    PayrollChecks: string;
  end;

  EEvoAPIError = class(Exception)
  public
    Code: integer;
    EvoMessage: string;
  public
    constructor Create(acode: integer; msg: string);
  end;

  TEvoXImportInputFile = record
    Filename: string;
    Filedata: string;
  end;

  TEvoXImportInputFiles = array of TEvoXImportInputFile;

  TEvoXImportResult = record
    InputRowsCount: integer;
    ImportedRowsCount: integer;

    Messages: string;

    ErrorCount: integer;
    WarningCount: integer;
    InfoCount: integer;
  end;

  TEvoXImportResults = array of TEvoXImportResult;

  IEvoXImportResultsWrapper = interface
['{57E80909-60D8-454F-B1F5-FCF7001420AD}']
    function GetResult(i: integer): TEvoXImportResult;
    function Count: integer;
    property Result[i: integer]: TEvoXImportResult read GetResult;
  end;

  IEvoAPIConnection = interface
  ['{F3FFE4B6-E6E8-42E5-9F1D-41BE43DD914F}']
    procedure OpenClient(cl_nbr: integer);
    function RunQuery(queryfile: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function OpenSQL(SQLText: string; ClientID: integer; ColumnMap: string): TkbmCustomMemTable;

    function AddReportTask(reportDef: TEvoReportDef): string;
    function AddPayrollReportTask(PrReportDef: TEvoPrReportDef): string;
    function IsTaskFinished(taskId: string): boolean;
    procedure DeleteTask(taskId: string);
    function GetASCIIReport(taskId: string): string;
    function TCImport(batch: TEvoPayrollBatchDef; tc: TTimeClockImportData): TTCImportResult;
    function GetPDFReport(taskId: string): string;
    function GetW2List(EeNbr: integer): TkbmCustomMemTable;
    function GetW2(CoTaxReturnRunsNbr: integer): string;

    function postTableChanges(clNbr: integer; changes: IRpcStruct): IRpcStruct;
    function AddEvoXImportTask(inputs: TEvoXImportInputFiles; mapfile: string): string;
    function GetEvoXImportResults(taskId: string): TEvoXImportResults;

    function createNewClientDB: integer;
    function applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
    function SB_CreateCoCalendar(CoNbr: integer; CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: string): IRpcArray;
    function SB_TCImport(CoNbr, PrNbr: integer; const ImportData: string; ImportOptions: IRpcStruct; PrBatchNbr: integer): IRpcStruct;
    function TaskQueue_RunCreatePayroll(aParams: IRpcStruct): string;
    procedure SB_autoEnlistEDsForNewEE(CoNbr, EeNbr: integer);
    function EvoVersion: string;
  end;

  TEvoTask = class
  public
    constructor Create(conn: IEvoAPIConnection; logger: ICommonLogger);
    destructor Destroy; override;
    procedure Start; virtual;
  private
    FLogger: ICommonLogger;
    FConnection: IEvoAPIConnection;
    FTaskId: string;
    FTimer: TTimer;
    procedure HandleTimer(Sender: TObject);
    procedure DoHandleTimer; virtual; abstract;
  end;

  TReportReadyEvent = procedure(report: Variant) of object;
  TReportFailedEvent = procedure of object;

  TEvoReportTask = class (TEvoTask)
  public
    constructor Create(conn: IEvoAPIConnection; reportDef: TEvoReportDef; readyHandler: TReportReadyEvent; failedHandler: TReportFailedEvent; logger: ICommonLogger);
    procedure Start; override;
  private
    FOnReportReady: TReportReadyEvent;
    FOnReportFailed: TReportFailedEvent;
    FReportDef: TEvoReportDef;
    procedure DoHandleTimer; override;
  end;

  TEvoXImportReadyEvent = procedure(results: Variant) of object;
  TEvoXImportFailedEvent = procedure of object;

  TEvoImportTask = class (TEvoTask)
  public
    constructor Create(conn: IEvoAPIConnection; inputs: TEvoXImportInputFiles; mapfile: string; readyHandler: TEvoXImportReadyEvent; failedHandler: TEvoXImportFailedEvent; logger: ICommonLogger);
    procedure Start; override;
  private
    FOnEvoXImportReady: TEvoXImportReadyEvent;
    FOnEvoXImportFailed: TEvoXImportFailedEvent;
    FInputs: TEvoXImportInputFiles;
    FMapfile: string;
    procedure DoHandleTimer; override;
  end;

  IEvoXImportExecutor = interface
    function RunEvoXImport(inputs: TEvoXImportInputFiles; mapfileContent: string; objname: string): IEvoXImportResultsWrapper;
  end;

var
  LicenseKey: string;

function CreateEvoAPIConnection(const param: TEvoAPIConnectionParam; logger: ICommonLogger): IEvoAPIConnection;
function CreateSilentEvoXImportExecutor(conn: IEvoAPIConnection): IEvoXImportExecutor;

implementation

uses
  gdycommon, XmlToDataSetUnit, messages,
  gdyRedir, gdyDeferredCall, gdyglobalwaitindicator, dateutils,
  gdydbcommonnew, gdyclasses, comboChoices, IdSSLOpenSSL, IdSSLOpenSSLHeaders;

const
  cEvoPollingIntervalMS = 15000; //30000

type
  TEvoAPIConnection = class (TInterfacedObject, IEvoAPIConnection)
  public
    constructor Create(const param: TEvoAPIConnectionParam; logger: ICommonLogger);
    destructor Destroy; override;
  private
    FRpcFunction: IRpcFunction;
    FRpcCaller: TRpcCaller;
    FSessionId: string;
    FVersion: string;
    FLogger: ICommonLogger;
    function Call: IRPCResult;

    {IEvoAPIConnection}
    procedure OpenClient(cl_nbr: integer);
    function RunQuery(queryfile: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function OpenSQL(SQLText: string; ClientID: integer; ColumnMap: string): TkbmCustomMemTable;

    function AddReportTask(reportDef: TEvoReportDef): string;
    function AddPayrollReportTask(PrReportDef: TEvoPrReportDef): string;
    function IsTaskFinished(taskId: string): boolean;
    procedure DeleteTask(taskId: string);
    function GetASCIIReport(taskId: string): string;
    function TCImport(batch: TEvoPayrollBatchDef; tc: TTimeClockImportData): TTCImportResult;
    function GetPDFReport(taskId: string): string;
    function GetW2List(EeNbr: integer): TkbmCustomMemTable;
    function GetW2(CoTaxReturnRunsNbr: integer): string;

    function postTableChanges(clNbr: integer; changes: IRpcStruct): IRpcStruct;
    function AddEvoXImportTask(inputs: TEvoXImportInputFiles; mapfile: string): string;
    function GetEvoXImportResults(taskId: string): TEvoXImportResults;

    function createNewClientDB: integer;
    function applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
    function SB_CreateCoCalendar(CoNbr: integer; CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: string): IRpcArray;
    function SB_TCImport(CoNbr, PrNbr: integer; const ImportData: string; ImportOptions: IRpcStruct; PrBatchNbr: integer): IRpcStruct;
    function TaskQueue_RunCreatePayroll(aParams: IRpcStruct): string;
    procedure SB_autoEnlistEDsForNewEE(CoNbr, EeNbr: integer);
    function EvoVersion: string;
  end;

  TStableEvoAPIConnection = class (TInterfacedObject, IEvoAPIConnection)
  public
    constructor Create(param: TEvoAPIConnectionParam; logger: ICommonLogger);
  private
    FParam: TEvoAPIConnectionParam;
    FLogger: ICommonLogger;
    FConn: IEvoAPIConnection;
    FRetryCount: integer;

    procedure ReconnectOrRaise;
    procedure BeforeCall;

    {IEvoAPIConnection}
    procedure OpenClient(cl_nbr: integer);
    function RunQuery(queryfile: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function OpenSQL(SQLText: string; ClientID: integer; ColumnMap: string): TkbmCustomMemTable;

    function AddReportTask(reportDef: TEvoReportDef): string;
    function AddPayrollReportTask(PrReportDef: TEvoPrReportDef): string;
    function IsTaskFinished(taskId: string): boolean;
    procedure DeleteTask(taskId: string);
    function GetASCIIReport(taskId: string): string;
    function TCImport(batch: TEvoPayrollBatchDef; tc: TTimeClockImportData): TTCImportResult;
    function GetPDFReport(taskId: string): string;
    function GetW2List(EeNbr: integer): TkbmCustomMemTable;
    function GetW2(CoTaxReturnRunsNbr: integer): string;

    function postTableChanges(clNbr: integer; changes: IRpcStruct): IRpcStruct;
    function AddEvoXImportTask(inputs: TEvoXImportInputFiles; mapfile: string): string;
    function GetEvoXImportResults(taskId: string): TEvoXImportResults;

    function createNewClientDB: integer;
    function applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
    function SB_CreateCoCalendar(CoNbr: integer; CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: string): IRpcArray;
    function SB_TCImport(CoNbr, PrNbr: integer; const ImportData: string; ImportOptions: IRpcStruct; PrBatchNbr: integer): IRpcStruct;
    function TaskQueue_RunCreatePayroll(aParams: IRpcStruct): string;
    procedure SB_autoEnlistEDsForNewEE(CoNbr, EeNbr: integer);
    function EvoVersion: string;
  end;


function CreateEvoAPIConnection(const param: TEvoAPIConnectionParam; logger: ICommonLogger): IEvoAPIConnection;
begin
  Result := TStableEvoAPIConnection.Create(param, logger);
  InitComboChoices(Result); //!! ugly hack
end;

const
  sCtxComponentEvolution = 'Evolution connection';
{ TEvoAPIConnection }

constructor TEvoAPIConnection.Create(const param: TEvoAPIConnectionParam; logger: ICommonLogger);
var
  RpcResult: IRpcResult;
  username, domain: string;
begin
  FLogger := logger;
  FLogger.LogEntry( 'Connecting to Evolution' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FLogger.LogContextItem('APIServerAddress', param.APIServerAddress );
      FLogger.LogContextItem('APIServerPort', inttostr(param.APIServerPort) );
//      FLogger.LogContextItem('APILicenseKey', param.APILicenseKey );
      FLogger.LogContextItem('Username', param.Username );
//      FLogger.LogContextItem('Password', param.Password );
      with SplitByChar(param.Username, '@') do
        if Count = 2 then
        begin
          username := Str[0];
          domain := Str[1];
        end
        else
        begin
          username := param.Username;
          domain := '';
        end;
      {
      FLogger.LogEntry( 'Workaround' );
      try
        try
          //!!TODO OpenSSL library loads only on the second attempt
          TIdSSLContext.Create.Free;
        except
          on E:Exception do
            FLogger.LogDebug(E.Message + ' '+ WhichFailedToLoad);
        end;
      finally
        FLogger.LogExit;
      end;
      }
      FRpcCaller := TRpcCaller.Create;
      try
        FRpcCaller.SSLEnable := true;
        FRpcCaller.SSLCertFile := Redirection.GetFilename(sCertificateFileAlias);
        FRpcCaller.HostName := param.APIServerAddress;
        FRpcCaller.HostPort := param.APIServerPort;

        FRpcFunction := TRpcFunction.Create;

        FRpcFunction.ObjectMethod := 'evo.sb.connect';
        FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
        FRpcFunction.AddItem(LicenseKey); // license key
        FRpcFunction.AddItem(''); // Evo server - unused
        FRpcFunction.AddItem(username); // user name
        FRpcFunction.AddItem(param.Password); // password
        if domain <> '' then
          FRpcFunction.AddItem(domain); // evoDomain - optional

        RpcResult := Call;
        FSessionId := RpcResult.AsStruct.Items[RpcResult.AsStruct.IndexOf('SESSION')].AsString;
        FVersion := RpcResult.AsStruct.Items[RpcResult.AsStruct.IndexOf('VER')].AsString;
      except
        FreeAndNil(FRpcCaller);
        raise;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

destructor TEvoAPIConnection.Destroy;
begin
  try
    if FSessionId <> '' then
    begin
//!! this dtor may be called from finally block and by calling LogEntry erase info that exception was already reported causing double reporting
      FRpcFunction.Clear;
      FRpcFunction.ObjectMethod := 'evo.disconnect';
      FRpcFunction.AddItem(FSessionId);
      try
        Call;
      except
        on E: EEvoAPIError do
          if (E.Code <> 810) or (E.EvoMessage <> 'Wrong session') then
            raise;
      end;
    end;
  finally
    FreeAndNil(FRpcCaller);
  end;
  inherited;
end;

function TEvoAPIConnection.RunQuery(queryfile: string; params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry( 'Evolution query' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
//      FLogger.LogContextItem('Session Id', FSessionId );
      FLogger.LogContextItem('Query file', queryfile );
      FRpcFunction.ObjectMethod := 'evo.runQBQuery';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItemBase64StrFromFile(queryfile);
      FRpcFunction.AddItem(''); //'Cl_Nbr=TMP_CL, Custom_Client_Number=TMP_CL, Name=TMP_CL'
      if params <> nil then //optional
        FRpcFunction.AddItem(params);
      Result := StructToDataSet( Call.AsStruct );
      FLogger.LogDebug('Dataset:', DataSetToCsv(Result));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.AddReportTask(reportDef: TEvoReportDef): string;
begin
  FLogger.LogEntry( 'Starting Evolution report generation' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FLogger.LogContextItem('Report', reportDef.Level + IntToStr(reportDef.Number) );

      // adding report to task queue
      FRpcFunction.ObjectMethod := 'evo.taskQueue.runReport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(reportDef.Number); // report number without report's level
      FRpcFunction.addItem(reportDef.Params);
      FRpcFunction.AddItem(reportDef.Level);

      Result := Call.AsString;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.IsTaskFinished(taskId: String): boolean;
begin
  Result := false; //to make compiler happy

  FLogger.LogEntry( 'Querying status of an Evolution task' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
//      FLogger.LogContextItem('Session Id', FSessionId );
      FLogger.LogContextItem('Task Id', taskId );
      Assert(taskId <> '');
      FRpcFunction.ObjectMethod := 'evo.taskQueue.isTaskFinished';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(taskId);
      Result := Call.AsBoolean;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.GetASCIIReport(taskId: string): string;
var
  RpcResult: IRpcResult;
  ResultType: String;
begin
  Assert(taskId <> '');
  FLogger.LogEntry( 'Getting Evolution report' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
//      FLogger.LogContextItem('Session Id', FSessionId );
      FLogger.LogContextItem('Task Id', taskId );
      FRpcFunction.ObjectMethod := 'evo.taskQueue.getReport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(taskId);
      RpcResult := Call;
      ResultType := RpcResult.AsStruct.Items[RpcResult.AsStruct.IndexOf('Type')].AsString;

      Result := RpcResult.AsStruct.Items[RpcResult.AsStruct.IndexOf('Report')].AsBase64Str;

      //!!remove task
      if ResultType = 'ERROR' then
      begin
        raise Exception.CreateFmt('Report finished with errors: %s', [Result]);
      end
      else if ResultType <> 'ASCII' then
      begin
        raise Exception.CreateFmt('Unexpected report type: %s', [ResultType]);
      end;
      FLogger.LogDebug('Evo report', Result);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoAPIConnection.DeleteTask(taskId: string);
begin
  Assert(taskId <> '');
  FLogger.LogEntry( 'Deleting an Evolution task' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
//      FLogger.LogContextItem('Session Id', FSessionId );
      FLogger.LogContextItem('Task Id', taskId );
      FRpcFunction.ObjectMethod := 'evo.taskQueue.deleteTask';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(taskId);
      Call;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoAPIConnection.OpenClient(cl_nbr: integer);
begin
  FLogger.LogEntry( 'Opening Evolution client database' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FLogger.LogContextItem('Internal Cl#', inttostr(cl_nbr) );
      FRpcFunction.ObjectMethod := 'evo.sb.openClient';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );

      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(cl_nbr);
      Call;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.TCImport(batch: TEvoPayrollBatchDef; tc: TTimeClockImportData): TTCImportResult;
var
  opt: IRpcStruct;
  ret: IRpcStruct;
begin
  FLogger.LogEntry( 'Executing TC Import' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      LogEvoPayrollBatchDef(FLogger, batch);

      OpenClient(batch.Company.ClNbr);

      FRpcFunction.Clear;
      FRpcFunction.ObjectMethod := 'evo.sb.TCImport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      LogTCImportAPIOptions(FLogger, tc.APIOptions);
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(batch.Company.CoNbr);
      FRpcFunction.AddItem(batch.Pr.PrNbr);

      FRpcFunction.AddItemBase64Str(tc.Text);
      FLogger.LogDebug( 'tc import file', tc.Text);
{$ifndef FINAL_RELEASE}
      StringToFile(tc.Text, Redirection.GetDirectory(sDumpDirAlias) + 'tcimport.txt');
{$endif}
//      FRpcFunction.AddItemBase64Str( FileToString(GetAppDir + 'tcimport.txt') );

      opt := TRpcStruct.Create;
      opt.AddItem('LookupOption', ord(tc.APIOptions.LookupOption) ); //(integer): 0 - By Name, 1 - By Number, 2- By SSN;
      opt.AddItem('DBDTOption', ord(tc.APIOptions.DBDTOption) ); //(integer): 0 - Full, 1 - Smart;
      opt.AddItem('FourDigitYear', tc.APIOptions.FourDigitYear);
      opt.AddItem('FileFormat', ord(tc.APIOptions.FileFormat) ); //(integer): 0 - Fixed,  1 - CommaDelimited;
      opt.AddItem('AutoImportJobCodes', tc.APIOptions.AutoImportJobCodes);
      opt.AddItem('UseEmployeePayRates', tc.APIOptions.UseEmployeePayRates);
      //UseEmployeePayRates must be false if DBDTs are not supplied

      FRpcFunction.AddItem(opt);

      FRpcFunction.AddItem(batch.PrBatchNbr);

      ret := Call.AsStruct;
      Result.RejectedRecords := ret.Keys['RejectedRecords'].AsInteger;
      Result.LogFile := ret.Keys['LogFile'].AsBase64Str;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.Call: IRPCResult;
  function RpcCheck(const res: IRPCResult): IRPCResult;
  begin
    if res = nil then
      raise Exception.Create('Unexpected error: got nil IRPCResult'); //this should never happen anyway...
    if res.IsError then
      raise EEvoAPIError.Create(res.ErrorCode, res.ErrorMsg);
    Result := res;
  end;
var
  res: IRpcResult;
  startTime: TDateTime;
begin
  FRpcCaller.SentLog := '';
  FRpcCaller.ReceivedLog := '';
  startTime := Now;
  FLogger.LogDebug(Format('Call time: %s', [DateTimeToStr(startTime)]) );
  res := FRpcCaller.Execute(FRpcFunction);
  FLogger.LogDebug(Format('Time elapsed: %.3f sec', [SecondSpan(Now, startTime)]) );
//{$ifndef FINAL_RELEASE}
  if FRpcFunction.ObjectMethod <> 'evo.runQBQuery' then
  begin
    FLogger.LogDebug('SentLog', FRpcCaller.SentLog);
    FLogger.LogDebug('ReceivedLog', FRpcCaller.ReceivedLog);
  end;
//{$endif}
  Result := RpcCheck(res);
end;

function TEvoAPIConnection.postTableChanges(clNbr: integer; changes: IRpcStruct): IRpcStruct;
begin
  FLogger.LogEntry( 'Evolution postTableChanges' );
  try
    try
      OpenClient(clNbr);
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FRpcFunction.ObjectMethod := 'evo.legacy.postTableChanges';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(changes);
      Result := Call.AsStruct;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.AddEvoXImportTask(inputs: TEvoXImportInputFiles; mapfile: string): string;
var
  files: IRpcArray;
  afile: IRpcStruct;
  ret: IRpcStruct;
  i: integer;
begin
  FLogger.LogEntry( 'Starting Evolution EvoX import task' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FLogger.LogDebug('Map file', mapfile);

      // adding import to the task queue
      FRpcFunction.ObjectMethod := 'evo.taskQueue.evoXImport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);

      files := TRpcArray.Create;
      for i := 0 to high(inputs) do
      begin
        FLogger.LogDebug( Format('File data #%d (%s)', [i, inputs[i].Filename]), inputs[i].Filedata );
        afile := TRpcStruct.Create;
        afile.AddItem('fileName', inputs[i].Filename);
        afile.AddItemBase64Str('fileData', inputs[i].Filedata);
        files.AddItem(afile);
      end;
      FRpcFunction.AddItem(files);
      FRpcFunction.AddItemBase64Str(mapfile);
      ret := Call.AsStruct;
      if ret.Keys['taskNbr'].AsString = '-1' then
        raise Exception.CreateFmt('Failed to create EvoX import task: %s', [ret.Keys['errorLog'].AsString])
      else
        Result := ret.Keys['taskNbr'].AsString;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.GetEvoXImportResults(taskId: string): TEvoXImportResults;
var
  ret: IRpcArray;
  i: integer;
begin
  Assert(taskId <> '');
  FLogger.LogEntry( 'Getting Evolution EvoX import results' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
//      FLogger.LogContextItem('Session Id', FSessionId );
      FLogger.LogContextItem('Task Id', taskId );
      FRpcFunction.ObjectMethod := 'evo.taskQueue.getEvoXImportResults';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(taskId);
      ret := Call.AsArray;
      SetLength(Result, ret.Count);
      for i := 0 to high(Result) do
      begin
        Result[i].InputRowsCount := ret.Items[i].AsStruct.Keys['inputRowsAmount'].AsInteger;
        Result[i].ImportedRowsCount := ret.Items[i].AsStruct.Keys['importedRowsAmount'].AsInteger;
        if ret.Items[i].AsStruct.KeyExists('errorsList') then //if errorsList's value is an empty base64 string then it isn't put in the RpcStruct
          Result[i].Messages := ret.Items[i].AsStruct.Keys['errorsList'].AsBase64Str
        else
          Result[i].Messages := '';
        Result[i].ErrorCount := ret.Items[i].AsStruct.Keys['realErrorsAmount'].AsInteger;
        Result[i].WarningCount := ret.Items[i].AsStruct.Keys['warningsAmount'].AsInteger;
        Result[i].InfoCount := ret.Items[i].AsStruct.Keys['realErrorsAmount'].AsInteger;
      end;
      if Length(Result) = 0 then
        raise Exception.Create('EvoX import returned no results at all. The import probably has failed');
//      Assert(Length(Result) > 0);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.createNewClientDB: integer;
begin
  FLogger.LogEntry( 'Evolution createNewClientDB' );
  try
    FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
    FRpcFunction.Clear;
    FRpcFunction.ObjectMethod := 'evo.createNewClientDB';
    FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
    FRpcFunction.AddItem(FSessionId);
    Result := Call.AsInteger;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
begin
  FLogger.LogEntry( 'Evolution applyDataChangePacket' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FRpcFunction.ObjectMethod := 'evo.applyDataChangePacket';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(changes);

{      if aAsOF <> 0 then
        FRpcFunction.AddItemDateTime( aAsOF );}

      Result := Call.AsStruct;
      FLogger.LogDebug('SentLog', FRpcCaller.SentLog);
      FLogger.LogDebug('ReceivedLog', FRpcCaller.ReceivedLog);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.SB_TCImport(CoNbr, PrNbr: integer;
  const ImportData: string; ImportOptions: IRpcStruct; PrBatchNbr: integer): IRpcStruct;
begin
  FLogger.LogEntry( 'Evolution Time Clock Import' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FRpcFunction.ObjectMethod := 'evo.sb.TCImport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );

      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(CoNbr);
      FRpcFunction.AddItem(PrNbr);
      FRpcFunction.AddItemBase64Str(ImportData);
      FRpcFunction.AddItem(ImportOptions);
      FRpcFunction.AddItem(PrBatchNbr);

      Result := Call.AsStruct;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.TaskQueue_RunCreatePayroll(
  aParams: IRpcStruct): string;
begin
  FLogger.LogEntry( 'Evolution Create Payroll' );
  try
    FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
    FRpcFunction.Clear;
    FRpcFunction.ObjectMethod := 'evo.taskQueue.runCreatePayroll';
    FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
    FRpcFunction.AddItem(FSessionId);
    FRpcFunction.AddItem(aParams);
    Result := Call.AsString;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.SB_CreateCoCalendar(CoNbr: integer;
  CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate,
  MoveDeliveryDate: string): IRpcArray;
begin
  FLogger.LogEntry( 'Evolution createCoCalendar' );
  try
    FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
    FRpcFunction.Clear;
    FRpcFunction.ObjectMethod := 'evo.sb.createCoCalendar';
    FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
    FRpcFunction.AddItem(FSessionId);
    FRpcFunction.AddItem(CoNbr);
    FRpcFunction.AddItem(CoSettings);
    FRpcFunction.AddItem(BasedOn);
    FRpcFunction.AddItem(MoveCheckDate);
    FRpcFunction.AddItem(MoveCallInDate);
    FRpcFunction.AddItem(MoveDeliveryDate);
    Result := Call.AsArray;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.OpenSQL(SQLText: string; ClientID: integer;
  ColumnMap: string): TkbmCustomMemTable;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry( 'Evolution Open SQL Query' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FLogger.LogContextItem('Query text', SQLText );
      FRpcFunction.ObjectMethod := 'evo.legacy.openSQL';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(SQLText);
      FRpcFunction.AddItem(ColumnMap); 
      FRpcFunction.AddItem(ClientID);
      Result := StructToDataSet( Call.AsStruct );
      FLogger.LogDebug('Dataset:', DataSetToCsv(Result));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.AddPayrollReportTask(
  PrReportDef: TEvoPrReportDef): string;
begin
  FLogger.LogEntry( 'Starting Evolution checkstock report generation' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FLogger.LogContextItem('Pr_Check_Nbr list', PrReportDef.PayrollChecks);

      // adding report to task queue
      FRpcFunction.ObjectMethod := 'evo.taskQueue.runPayrollReport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(PrReportDef.PrNbr);
      FRpcFunction.AddItemDateTime(PrReportDef.CheckDate);
      FRpcFunction.AddItem(PrReportDef.PayrollChecks);

      Result := Call.AsString;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.GetPDFReport(taskId: string): string;
var
  RpcResult: IRpcResult;
begin
  Assert(taskId <> '');
  FLogger.LogEntry( 'Getting Evolution pdf report' );
  try
    try
      FRpcFunction.Clear;
      FLogger.LogContextItem('Task Id', taskId );
      FRpcFunction.ObjectMethod := 'evo.taskQueue.getPDFReport';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(taskId);
      RpcResult := Call;
      if RpcResult.IsBase64 then
        Result := RpcResult.AsBase64Str
      else
        raise EXmlRpcError.Create('The report is blank');
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.GetW2List(EeNbr: integer): TkbmCustomMemTable;
begin
  Result := nil;
  FLogger.LogEntry( 'Getting employee W2 report list' );
  try
    try
      FRpcFunction.Clear;
      FLogger.LogContextItem('Ee Nbr', IntToStr(EeNbr) );
      FRpcFunction.ObjectMethod := 'evo.ee.getW2List';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(EeNbr);
      Result := StructToDataSet( Call.AsStruct );
      FLogger.LogDebug('Dataset:', DataSetToCsv(Result));

      // returns dataset:
      // CO_TAX_RETURN_RUNS_NBR, DataType: ftInteger
      // NAME, DataType: ftString; Size: 60
      // Year, DataType: ftInteger
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.GetW2(
  CoTaxReturnRunsNbr: integer): string;
begin
  FLogger.LogEntry( 'Getting employee W2 pdf report' );
  try
    try
      FRpcFunction.Clear;
      FLogger.LogContextItem('CoTaxReturnRunsNbr', IntToStr(CoTaxReturnRunsNbr) );
      FRpcFunction.ObjectMethod := 'evo.ee.getW2';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );
      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(CoTaxReturnRunsNbr);
      Result := Call.AsBase64Str;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoAPIConnection.SB_autoEnlistEDsForNewEE(CoNbr,
  EeNbr: integer);
begin
  FLogger.LogEntry( 'Evolution Auto Enlist Scheduled E/Ds Codes' );
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentEvolution);
      FRpcFunction.Clear;
      FRpcFunction.ObjectMethod := 'evo.sb.autoEnlistEDsForNewEE';
      FLogger.LogContextItem('API method name', FRpcFunction.ObjectMethod );

      FRpcFunction.AddItem(FSessionId);
      FRpcFunction.AddItem(CoNbr);
      FRpcFunction.AddItem(EeNbr);

      Call.AsStruct;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TEvoAPIConnection.EvoVersion: string;
begin
  Result := FVersion;
end;

{ TEvoReportTask }
constructor TEvoReportTask.Create(conn: IEvoAPIConnection;
  reportDef: TEvoReportDef; readyHandler: TReportReadyEvent; failedHandler: TReportFailedEvent; logger: ICommonLogger);
begin
  Assert(assigned(readyHandler));
  Assert(assigned(failedHandler));
  FReportDef := reportDef;
  FOnReportReady := readyHandler;
  FOnReportFailed := failedHandler;
  inherited Create(conn, logger);
end;

procedure TEvoReportTask.DoHandleTimer;
var
  report: string;
begin
  FLogger.LogEntry( 'Fetching the Evolution report if it is ready' );
  try
    try
      Assert(FTaskId <> '');
      FTimer.Enabled := false;
      try
        if FConnection.IsTaskFinished(FTaskId) then
        begin
          report := FConnection.GetASCIIReport(FTaskId);
{$ifndef FINAL_RELEASE}
          StringToFile(report, Redirection.GetDirectory(sDumpDirAlias) + Format('%s%d.txt', [FReportDef.Level, FReportDef.Number]) );
{$endif}
          DeferredCall(FOnReportReady, report);
        end
        else
          FTimer.Enabled := true; //don't count time spent in the request
      except
        DeferredCall(FOnReportFailed);
        raise;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoReportTask.Start;
begin
  FTaskId := FConnection.AddReportTask(FReportDef);
  inherited;
end;

{ TEvoAPIError }

constructor EEvoAPIError.Create(acode: integer; msg: string);
var
  explanation: string;
begin
  Code := acode;
  EvoMessage := msg;
  explanation := '';
  if (acode = 800) and SameText(msg, 'Wrong API Key') then
    explanation := 'You do not have a valid license to use this product. Please, contact your payroll provider.';
  CreateFmt('Evolution server returned error: (%d) %s. %s', [Code, EvoMessage, explanation]);
end;

{ TStableEvoAPIConnection }

constructor TStableEvoAPIConnection.Create(param: TEvoAPIConnectionParam;
  logger: ICommonLogger);
begin
  FParam := param;
  FLogger := logger;
  FConn := TEvoAPIConnection.Create(FParam, FLogger);
end;

procedure TStableEvoAPIConnection.BeforeCall;
begin
  FRetryCount := 0;
end;

procedure TStableEvoAPIConnection.ReconnectOrRaise;
begin
  Assert( ExceptObject <> nil );
  if (FRetryCount = 0) and (ExceptObject is EEvoAPIError) and
     ((ExceptObject as EEvoAPIError).Code = 810) and ((ExceptObject as EEvoAPIError).EvoMessage = 'Wrong session') then
  begin
    Flogger.LogEvent('Reconnecting to Evolution');
    FRetryCount := FRetryCount + 1;
    try
      FConn := nil;
    except
      //nobody cares
    end;
    FConn := TEvoAPIConnection.Create(FParam, FLogger);
    FLogger.LogEvent('Reconnected to Evolution');
  end
  else
    Flogger.PassthroughException;
end;

function TStableEvoAPIConnection.AddReportTask(reportDef: TEvoReportDef): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.AddReportTask(reportDef);
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableEvoAPIConnection.DeleteTask(taskId: string);
begin
  BeforeCall;
  while true do
  try
    FConn.DeleteTask(taskId);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.GetASCIIReport(taskId: string): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetASCIIReport(taskId);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.IsTaskFinished(taskId: string): boolean;
begin
  Result := false; //to make compiler happy
  BeforeCall;
  while true do
  try
    Result := FConn.IsTaskFinished(taskId);
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableEvoAPIConnection.OpenClient(cl_nbr: integer);
begin
  BeforeCall;
  while true do
  try
    FConn.OpenClient(cl_nbr);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.postTableChanges(clNbr: integer; changes: IRpcStruct): IRpcStruct;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.postTableChanges(clNbr, changes);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.RunQuery(queryfile: string; params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := nil; //to make compiler happy
  BeforeCall;
  while true do
  try
    Result := FConn.RunQuery(queryfile, params);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.TCImport(batch: TEvoPayrollBatchDef; tc: TTimeClockImportData): TTCImportResult;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.TCImport(batch, tc);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.AddEvoXImportTask(inputs: TEvoXImportInputFiles; mapfile: string): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.AddEvoXImportTask(inputs, mapfile);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.GetEvoXImportResults(
  taskId: string): TEvoXImportResults;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetEvoXImportResults(taskId);
    break;
  except
    ReconnectOrRaise;
  end;
end;


function TStableEvoAPIConnection.createNewClientDB: integer;
begin
  Result := -1;
  BeforeCall;
  while true do
  try
    Result := FConn.createNewClientDB;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.applyDataChangePacket(changes, aAsOF);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.SB_TCImport(CoNbr, PrNbr: integer;
  const ImportData: string; ImportOptions: IRpcStruct; PrBatchNbr: integer): IRpcStruct;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.SB_TCImport(CoNbr, PrNbr, ImportData, ImportOptions, PrBatchNbr);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.SB_CreateCoCalendar(CoNbr: integer;
  CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate,
  MoveDeliveryDate: string): IRpcArray;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.SB_CreateCoCalendar(CoNbr, CoSettings, BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.TaskQueue_RunCreatePayroll(
  aParams: IRpcStruct): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.TaskQueue_RunCreatePayroll(aParams);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.OpenSQL(SQLText: string;
  ClientID: integer; ColumnMap: string): TkbmCustomMemTable;
begin
  Result := nil; //to make compiler happy
  BeforeCall;
  while true do
  try
    Result := FConn.OpenSQL(SQLText, ClientID, ColumnMap);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.AddPayrollReportTask(
  PrReportDef: TEvoPrReportDef): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.AddPayrollReportTask(PrReportDef);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.GetPDFReport(taskId: string): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetPDFReport(taskId);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.GetW2List(
  EeNbr: integer): TkbmCustomMemTable;
begin
  Result := nil;
  BeforeCall;
  while true do
  try
    Result := FConn.GetW2List(EeNbr);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.GetW2(
  CoTaxReturnRunsNbr: integer): string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetW2(CoTaxReturnRunsNbr);
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableEvoAPIConnection.SB_autoEnlistEDsForNewEE(CoNbr,
  EeNbr: integer);
begin
  BeforeCall;
  while true do
  try
    FConn.SB_autoEnlistEDsForNewEE(CoNbr, EeNbr);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableEvoAPIConnection.EvoVersion: string;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.EvoVersion;
    break;
  except
    ReconnectOrRaise;
  end;
end;

{ TEvoTask }

constructor TEvoTask.Create(conn: IEvoAPIConnection;
  logger: ICommonLogger);
begin
  FLogger := logger;
  FTimer := TTimer.Create(nil);
  FTimer.Enabled := false;
  FTimer.OnTimer := HandleTimer;
  FTimer.Interval := cEvoPollingIntervalMS;
  FConnection := conn;
end;

destructor TEvoTask.Destroy;
begin
  FreeAndNil(FTimer);
  if FTaskId <> '' then
  try
    FConnection.DeleteTask(FTaskId);
  except
    FLogger.StopException;
  end;
  inherited;
end;

procedure TEvoTask.HandleTimer(Sender: TObject);
begin
  DoHandleTimer;
end;


type
  TEvoXImportResultsWrapper = class(TInterfacedObject, IEvoXImportResultsWrapper)
  public
    constructor Create(r: TEvoXImportResults);
  private
    FResults: TEvoXImportResults;
    {IEvoXImportResultsWrapper}
    function GetResult(i: integer): TEvoXImportResult;
    function Count: integer;
  end;

procedure TEvoTask.Start;
begin
  FTimer.Enabled := true;
end;

{ TEvoXImportResultsWrapper }

function TEvoXImportResultsWrapper.Count: integer;
begin
  Result := Length(FResults);
end;

constructor TEvoXImportResultsWrapper.Create(r: TEvoXImportResults);
begin
  FResults := Copy(r);
end;

function TEvoXImportResultsWrapper.GetResult(i: integer): TEvoXImportResult;
begin
  Result := FResults[i];
end;

{ TEvoImportTask }

constructor TEvoImportTask.Create(conn: IEvoAPIConnection;
  inputs: TEvoXImportInputFiles; mapfile: string;
  readyHandler: TEvoXImportReadyEvent;
  failedHandler: TEvoXImportFailedEvent; logger: ICommonLogger);
begin
  Assert(assigned(readyHandler));
  Assert(assigned(failedHandler));
  FOnEvoXImportReady := readyHandler;
  FOnEvoXImportFailed := failedHandler;
  FInputs := Copy(inputs);
  FMapfile := mapfile;
  inherited Create(conn, logger);
end;

procedure TEvoImportTask.DoHandleTimer;
var
  p: IEvoXImportResultsWrapper;
begin
  FLogger.LogEntry( 'Fetching EvoX import results if they are ready' );
  try
    try
      FTimer.Enabled := false;
      try
        if FConnection.IsTaskFinished(FTaskId) then
        begin
          p := TEvoXImportResultsWrapper.Create( FConnection.GetEvoXImportResults(FTaskId) );
          DeferredCall(FOnEvoXImportReady, p);
        end
        else
          FTimer.Enabled := true; //don't count time spent in the request
      except
        DeferredCall(FOnEvoXImportFailed);
        raise;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoImportTask.Start;
begin
  FTaskId := FConnection.AddEvoXImportTask(FInputs, FMapfile);
  inherited;
end;

procedure WaitForTask(conn: IEvoAPIConnection; taskID: string);
var
  timePassedSec: integer;
begin
  timePassedSec := 0;
  try
    while timePassedSec < 60*60*8 do //eight hours
    begin
      Sleep(cEvoPollingIntervalMS);
      timePassedSec := timePassedSec + cEvoPollingIntervalMS div 1000;
      if conn.IsTaskFinished(taskID) then
        break;
    end;
  except
    conn.DeleteTask(taskID);
    raise;
  end;
end;

type
  TSilentEvoXImportExecutor = class(TInterfacedObject, IEvoXImportExecutor)
  private
    FConn: IEvoAPIConnection;
  public
    constructor Create(conn: IEvoAPIConnection);
    {IEvoXImportExecutor}
    function RunEvoXImport(inputs: TEvoXImportInputFiles; mapfileContent: string; objname: string): IEvoXImportResultsWrapper;
  end;


{ TSilentEvoXImportExecutor }

constructor TSilentEvoXImportExecutor.Create(conn: IEvoAPIConnection);
begin
  FConn := conn;
end;

function TSilentEvoXImportExecutor.RunEvoXImport(inputs: TEvoXImportInputFiles; mapfileContent: string; objname: string): IEvoXImportResultsWrapper;
var
  taskID: string;
begin
  taskID := FConn.AddEvoXImportTask(inputs, mapfileContent);
  WaitForTask(FConn, taskID);
  Result := TEvoXImportResultsWrapper.Create( FConn.GetEvoXImportResults(taskID) );
end;

function CreateSilentEvoXImportExecutor(conn: IEvoAPIConnection): IEvoXImportExecutor;
begin
  Result := TSilentEvoXImportExecutor.Create(conn);
end;

end.


