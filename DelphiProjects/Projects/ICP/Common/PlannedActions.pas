unit PlannedActions;

interface

uses
  contnrs, gdyclasses, gdycommonlogger;

type
  TSyncAction = (euaCreate, euaUpdate, euaDelete);
  TSyncActionSet = set of TSyncAction;

  TChangedFieldRec = record
    FieldName: string;
    SourceValue: string; //Evo
    TargetValue: string; //ExtApp
  end;

  TChangedFieldRecs = array of TChangedFieldRec;

  TSyncActionInfo = class
  public
    Code: string;
    Action: TSyncAction;
    ChangedFields: TChangedFieldRecs;

    Tag: Integer;

    NewVersion: string;

    Context: IStr;
  public
    constructor Create;
  end;

  TSyncActionInfos = array of TSyncActionInfo;

  ISyncActionsRO = interface
['{4C6F1A2A-B5F6-474C-A0ED-3EB0F1D182A6}']
    function Get(i: integer): TSyncActionInfo;
    function Count: integer;
    function ToText: string;
  end;

  ISyncActions = interface(ISyncActionsRO)
['{D5E91535-9FE6-4256-8F0E-10911013A4F9}']
    function Add(Code: string; Action: TSyncAction; ChangedFields: TChangedFieldRecs; tag: Integer = -1; newVersion: string = ''): TSyncActionInfo;
  end;

  TSyncActionGroup = record
    ParentCode: string;
    Actions: ISyncActionsRO;
  end;

  TSyncActionGroups = array of TSyncActionGroup;

function CreateSyncActions(TargetApp: string; objName: string; deletedVerb: string = ''): ISyncActions; {like 'Employee'}{like 'deleted'}

//first value is from an external app, second one is from evo
procedure AddChangedFields(var ChangedFields: TChangedFieldRecs; TargetS, SourceS, fieldName: string);
procedure InvertChangedFields(var ChangedFields: TChangedFieldRecs);

function ExistsChangedField(const ChangedFields: TChangedFieldRecs; name: string): boolean;

procedure CheckIfChanged(var ChangedFields: TChangedFieldRecs; v1, v2: Variant; fieldName: string);
procedure CheckIfSSNChanged(var ChangedFields: TChangedFieldRecs; s1, s2: string; fieldName: string);
procedure CheckIfFloatChanged(var ChangedFields: TChangedFieldRecs; s1, s2: string; fieldName: string; eps: extended = 1e-10); overload;
procedure CheckIfFloatChanged(var ChangedFields: TChangedFieldRecs; f1, f2: Extended; fieldName: string; eps: extended = 1e-10); overload;
procedure CheckIfFloatChanged(var ChangedFields: TChangedFieldRecs; v1, v2: Variant; fieldName: string; eps: extended = 1e-10); overload;

function SelectByActionType(actions: ISyncActionsRO; acts: TSyncActionSet): TSyncActionInfos; overload;
function SelectByActionType(const groups: TSyncActionGroups; acts: TSyncActionSet): TSyncActionInfos; overload;

function SyncActionGroupsToText(const groups: TSyncActionGroups; parentName: string): string;

procedure LogContext(logger: ICommonLogger; context: IStr);

const
  SyncActionNames: array [TSyncAction] of string = ('Create', 'Update', 'Delete');

implementation

uses
  sysutils, variants, gdyCommon, common;

procedure AddChangedFields(var ChangedFields: TChangedFieldRecs; TargetS, SourceS, fieldName: string);
begin
  SetLength(ChangedFields, Length(ChangedFields)+1);
  ChangedFields[high(ChangedFields)].FieldName := fieldName;
  ChangedFields[high(ChangedFields)].TargetValue := TargetS;
  ChangedFields[high(ChangedFields)].SourceValue := SourceS;
end;

procedure CheckIfChanged(var ChangedFields: TChangedFieldRecs; v1, v2: Variant; fieldName: string);
var
  diff: boolean;
begin
  diff := trim(VarToStr(v1)) <> trim(VarToStr(v2));
  if diff then
    AddChangedFields(ChangedFields, VarToStr(v1), VarToStr(v2), fieldName)
end;

procedure CheckIfSSNChanged(var ChangedFields: TChangedFieldRecs; s1, s2: string; fieldName: string);
var
  x1, x2: string;
  diff: boolean;
begin
  x1 := StringReplace(s1, '-', '', [rfReplaceAll]);
  x2 := StringReplace(s2, '-', '', [rfReplaceAll]);
  diff := trim(x1) <> trim(x2);
  if diff then
    AddChangedFields(ChangedFields, s1, s2, fieldName)
end;

procedure CheckIfFloatChanged(var ChangedFields: TChangedFieldRecs; s1, s2: string; fieldName: string; eps: extended);
var
  diff: boolean;
begin
  diff := abs(StrToFloat(s1) - StrToFloat(s2)) > eps;
  if diff then
    AddChangedFields(ChangedFields, s1, s2, fieldName)
end;

procedure CheckIfFloatChanged(var ChangedFields: TChangedFieldRecs; f1, f2: Extended; fieldName: string; eps: extended);
var
  diff: boolean;
begin
  diff := abs(f1 - f2) > eps;
  if diff then
    AddChangedFields(ChangedFields, FloatToStr(f1), FloatToStr(f2), fieldName)
end;

procedure CheckIfFloatChanged(var ChangedFields: TChangedFieldRecs; v1, v2: Variant; fieldName: string; eps: extended);
var
  diff: boolean;
begin
  if not VarHasValue(v1) and not VarHasValue(v2) then
    diff := false
  else if not VarHasValue(v1) or not VarHasValue(v2) then
    diff := true
  else
    diff := abs(Extended(v1) - Extended(v2)) > eps;
  if diff then
    AddChangedFields(ChangedFields, VarToStr(v1), VarToStr(v2), fieldName);
end;

type
  TSyncActions = class(TInterfacedObject, ISyncActions)
  private
    FPlannedActions: TObjectList;
    FTargetApp: string;
    FObjName: string;
    FDeletedVerb: string;
    {ISyncActions}
    function Get(i: integer): TSyncActionInfo;
    function Count: integer;
    function ToText: string;
    function Add(Code: string; Action: TSyncAction; ChangedFields: TChangedFieldRecs; tag: Integer = -1; newVersion: string = ''): TSyncActionInfo;
  public
    constructor Create(TargetApp: string; objName: string; deletedVerb: string); //singular, first letter capitalized
    destructor Destroy; override;
  end;

{ TSyncActions }

function TSyncActions.Add(Code: string; Action: TSyncAction; ChangedFields: TChangedFieldRecs; tag: Integer = -1; newVersion: string = ''): TSyncActionInfo;
begin
  Result := TSyncActionInfo.Create;
  FPlannedActions.Add(Result);
  Result.Code := trim(Code);
  Result.Action := Action;
  Result.ChangedFields := ChangedFields;
  Result.Tag := tag;
  Result.NewVersion := newVersion;
end;

function TSyncActions.Count: integer;
begin
  Result := FPlannedActions.Count;
end;

constructor TSyncActions.Create(TargetApp: string; objName: string; deletedVerb: string);
begin
  FTargetApp := TargetApp;
  FPlannedActions := TObjectList.Create(true);
  FPlannedActions.Capacity := 1000; //why not
  FObjName := objName;
  if deletedVerb <> '' then
    FDeletedVerb := deletedVerb
  else
    FDeletedVerb := 'deleted';
end;

destructor TSyncActions.Destroy;
begin
  FreeAndNil(FPlannedActions);
  inherited;
end;

function TSyncActions.Get(i: integer): TSyncActionInfo;
begin
  Result := FPlannedActions[i] as TSyncActionInfo;
end;

function ChangedFieldRecsToDetailedString(changedFields: TChangedFieldRecs): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(changedFields) do
  begin
    if Result <> '' then
      Result := Result + #13#10;
    Result := Result + Format( '%s, old value: "%s", new value: "%s"', [changedFields[i].FieldName,changedFields[i].TargetValue, changedFields[i].SourceValue]);
  end
end;

function TSyncActions.ToText: string;
  function GetListOfEECodesForAction(act: TSyncAction): string;
  var
    i: integer;
  begin
    Result := '';
    for i := 0 to Count-1 do
      if Get(i).Action = act then
      begin
        if Result <> '' then
          Result := Result + ', ';
        Result := Result + Get(i).Code;
      end;
  end;
var
  i: integer;
  s: string;
begin
  Result := '';
  s := GetListOfEECodesForAction(euaCreate);
  if s <> '' then
    Result := Result + Format('%s records will be CREATED for the following %s: %s'#13#10#13#10#13#10, [FTargetApp, Plural(AnsiLowerCase(FObjName)), s]);

  s := '';
  for i := 0 to Count-1 do
    if Get(i).Action = euaUpdate then
    begin
      if s <> '' then
        s := s + #13#10#13#10;
      s := s +  Format('%s # %s:'#13#10'%s', [FObjName, Get(i).Code, ChangedFieldRecsToDetailedString(Get(i).ChangedFields)]);
    end;
  if s <> '' then
    Result := Result + Format('%s records will be UPDATED for the following %s: '#13#10#13#10'%s'#13#10#13#10#13#10, [FTargetApp, Plural(AnsiLowerCase(FObjName)), s]);
  s := GetListOfEECodesForAction(euaDelete);
  if s <> '' then
    Result := Result + Format('%s records will be %s for the following %s: %s'#13#10#13#10#13#10, [FTargetApp, AnsiUpperCase(FDeletedVerb), Plural(AnsiLowerCase(FObjName)), s]);
end;

function CreateSyncActions(TargetApp: string; objName: string; deletedVerb: string): ISyncActions;
begin
  Result := TSyncActions.Create(TargetApp, objName, deletedVerb);
end;

procedure InvertChangedFields(var ChangedFields: TChangedFieldRecs);
var
  temp: string;
  i: integer;
begin
  for i := 0 to high(ChangedFields) do
  begin
    temp := ChangedFields[i].SourceValue;
    ChangedFields[i].SourceValue := ChangedFields[i].TargetValue;
    ChangedFields[i].TargetValue := temp;
  end;
end;

function SelectByActionType(actions: ISyncActionsRO; acts: TSyncActionSet): TSyncActionInfos;
var
  i: integer;
begin
  SetLength(Result, 0);
  for i := 0 to actions.Count-1 do
    if actions.Get(i).Action in acts then
    begin
      SetLength(Result, Length(Result)+1);
      Result[high(Result)] := actions.Get(i);
    end;
end;

function SyncActionGroupsToText(const groups: TSyncActionGroups; parentName: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(groups) do
    if groups[i].Actions.Count > 0 then
      Result := Result + Format( '%s # %s:'#13#10'%s', [parentName, groups[i].ParentCode, groups[i].Actions.ToText]);
end;

function SelectByActionType(const groups: TSyncActionGroups; acts: TSyncActionSet): TSyncActionInfos;
var
  cnt, used: integer;
  i, j: integer;
begin
  cnt := 0;
  for i := 0 to high(groups) do
    cnt := cnt + groups[i].Actions.Count;

  SetLength(Result, cnt);
  used := 0;
  for i := 0 to high(groups) do
    for j := 0 to groups[i].Actions.Count-1 do
      if groups[i].Actions.Get(j).Action in acts then
      begin
        Result[used] := groups[i].Actions.Get(j);
        inc(used);
      end;
  SetLength(Result, used);
end;

function ExistsChangedField(const ChangedFields: TChangedFieldRecs; name: string): boolean;
var
  i: integer;
begin
  Result := false;
  for i := 0 to high(ChangedFields) do
    if ChangedFields[i].FieldName = name then
    begin
      Result := true;
      exit;
    end;
end;

{ TSyncActionInfo }

constructor TSyncActionInfo.Create;
begin
  Context := EmptyIStr;
end;

procedure LogContext(logger: ICommonLogger; context: IStr);
var
  j: integer;
begin
  for j := 0 to Context.Count-1 do
    with SplitByChar(Context.Str[j],'=') do
      logger.LogContextItem( Str[0], Str[1] );
end;

end.
