unit scheduler;

interface

uses
  isSettings, gdyCommonLogger, jclTask, classes, scheduledTask,
  kbmMemTable, db;

type
  TScheduler = class (TComponent)
  private
    FSysScheduler: TJclTaskSchedule;
    FLogger: ICommonLogger;

    FTasks: TkbmMemTable;
    FTasksPrivate: TkbmMemTable;
    FTasksDS: TDataSource;

    FResults: TkbmMemTable;
    FAllResults: TkbmMemTable;

    FTaskAdapter: ITaskAdapter;

    procedure CreateDS;
    procedure HandleAfterInsert(DataSet: TDataSet);
    procedure HandleBeforeDelete(DataSet: TDataSet);
    procedure AddTaskToDS(task: IScheduledTask);
    function AddTaskToSysScheduler(guid: string): TJclScheduledTask;
    function TaskNameByGUID(guid: string): string;
    function FindOrCreateSysTaskByGUID(guid: string): TJclScheduledTask;
    function TryFindSysTaskByGUID(guid: string): TJclScheduledTask;
    procedure LogSysTask(sysTask: TJclScheduledTask);
    procedure LogSysTasks;
  public
    constructor Create(Owner: TComponent; Logger: ICommonLogger; ta: ITaskAdapter); reintroduce;
    destructor Destroy; override;

    procedure AddTask(task: IScheduledTask);
    procedure TaskParametersEdited(task: IScheduledTask);
    procedure DeleteCurrentTask;
    procedure ShowScheduleForCurrentTask;
    procedure RunCurrentTask;
    procedure EnableCurrentTask(enable: boolean);
    function IsCurrentTaskRunning: boolean;
    procedure Refresh;
    function CurrentTaskAccount: string;
    procedure CurrentTaskSetAccountInfo(const Account, Password: string);

    function TasksDataSet: TDataSet;
    function ResultsDataSet: TDataSet;
    function AllResultsDataSet: TDataSet;
  end;

implementation

uses
  gdyRedir, common, sysutils, gdyCommon, forms, gdydbcommonnew,
  Variants, gdyClasses, comobj, gdydbcommon, typinfo;

{ TScheduler }

function TScheduler.AddTaskToSysScheduler(guid: string): TJclScheduledTask;
var
  uname: widestring;
begin
  Result := nil; //to make compiler happy
  FLogger.LogEntry('Adding new task to system scheduler');
  try
    try
      Result := FSysScheduler.Add( TaskNameByGUID(guid));
      Result.ApplicationName := ParamStr(0);
      Result.Parameters := guid;
      Result.Comment := Format('Please use "%s" to manage this task.', [Application.Title]);

      uname := GetUserName(NameSamCompatible);
      Result.Flags := Result.Flags + [tfRunOnlyIfLoggedOn]; //!! because password isn't specified
      OleCheck(Result.ScheduledWorkItem.SetAccountInformation(PWideChar(uname), nil));
      Result.Save;
      Result.Refresh;
      LogSysTask(Result);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TScheduler.AddTask(task: IScheduledTask);
begin
  AddTaskToDS(task);
  AddTaskToSysScheduler(task.GUID);
end;

constructor TScheduler.Create(Owner: TComponent; Logger: ICommonLogger; ta: ITaskAdapter);
begin
  inherited Create(nil);
  FLogger := Logger;
  FTaskAdapter := ta;

  FLogger.LogEntry('Initializing scheduler');
  try
    try
      if not TJclTaskSchedule.IsRunning then
        TJclTaskSchedule.Start;
      FSysScheduler := TJclTaskSchedule.Create;

      CreateDS;
      Refresh;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure CreateStringLookupField(ds: TDataSet; keyFieldName: string; protoField: TStringField);
begin
  with CreateStringField( ds, protoField.FieldName, protoField.DisplayLabel, protoField.DataSize) do
  begin
    FieldKind := fkLookup;
    LookupDataSet := protoField.DataSet;
    LookupKeyFields := keyFieldName;
    LookupResultField := protoField.FieldName;
    KeyFields := keyFieldName;
  end;
end;

procedure TScheduler.CreateDS;
begin
  FTasks := TkbmMemTable.Create(Self);
  FTasks.AttachMaxCount := 4; //!! like IskbmDataSet
  CreateStringField( FTasks, 'GUID', 'Internal ID', 38);
  CreateStringField( FTasks, 'NAME', 'Name', 256);

  CreateStringField( FTasks, 'CUSTOM_COMPANY_NUMBER', 'Company code', 12);
  CreateStringField( FTasks, 'COMPANY_NAME', 'Company name', 40);
  CreateStringField( FTasks, 'CUSTOM_CLIENT_NUMBER', 'Client code', 12);
  CreateStringField( FTasks, 'CLIENT_NAME', 'Client name', 40);
  CreateStringField( FTasks, 'SUMMARY', 'Summary', 4000);
  CreateDateTimeField( FTasks, 'LAST_RUN_TIME', 'Last run time');
  CreateStringField( FTasks, 'LAST_RUN_STATUS', 'Last run status', 256);
  with TBooleanField.Create(FTasks) do
  begin
    FieldName := 'ENABLED';
    DisplayLabel := 'Enabled';
    DataSet := FTasks;
  end;

  FTasks.AfterInsert := HandleAfterInsert;
  FTasks.BeforeDelete := HandleBeforeDelete;

  FTasksDS := TDataSource.Create(Self);
  FTasksDS.DataSet := FTasks;

  FTasksPrivate := TkbmMemTable.Create(Self);
  FTasksPrivate.AttachedTo := FTasks;

  FResults := TkbmMemTable.Create(Self);
  FResults.AttachMaxCount := 4; //!! like IskbmDataSet

  CreateStringField( FResults, 'GUID', 'Internal ID', 38);
  CreateStringField( FResults, 'FOLDER', 'Folder', 256);
  CreateDateTimeField( FResults, 'WHEN', 'When');
  CreateStringField( FResults, 'STATUS', 'Status', 256);
  CreateStringField( FResults, 'MESSAGE', 'Info', 256);

  CreateStringLookupField(FResults, 'GUID', FTasksPrivate.FieldByName('NAME') as TStringField);
  CreateStringLookupField(FResults, 'GUID', FTasksPrivate.FieldByName('CUSTOM_COMPANY_NUMBER') as TStringField);
  CreateStringLookupField(FResults, 'GUID', FTasksPrivate.FieldByName('COMPANY_NAME') as TStringField);
  CreateStringLookupField(FResults, 'GUID', FTasksPrivate.FieldByName('CUSTOM_CLIENT_NUMBER') as TStringField);
  CreateStringLookupField(FResults, 'GUID', FTasksPrivate.FieldByName('CLIENT_NAME') as TStringField);

  FResults.MasterSource := FTasksDS;
  FResults.MasterFields := 'GUID';
  FResults.DetailFields := 'GUID';

  FAllResults := TkbmMemTable.Create(Self);
  FAllResults.AttachedTo := FResults;

  FTasks.Open;
  FTasksPrivate.Open;
  FResults.Open;
  FAllResults.Open;

  SetCurrentSorting(FAllResults, 'WHEN', 'WHEN'); //desc
end;

destructor TScheduler.Destroy;
begin
  FreeAndNil(FSysScheduler);
  inherited;
end;

procedure TScheduler.HandleAfterInsert(DataSet: TDataSet);
begin

end;

procedure TScheduler.Refresh;
var
  savedguid: Variant;
  savedResultFolder: Variant;
  taskguids: IStr;
  i: integer;
  t: TJclScheduledTask;
  task: IScheduledTask;
begin
  FLogger.LogEntry('Making list of tasks');
  try
    try
      FLogger.LogEntry('Enumerating system scheduler''s tasks');
      try
        try
          FSysScheduler.Refresh;
          LogSysTasks;
        except
          FLogger.PassthroughException;
        end;
      finally
        FLogger.LogExit;
      end;

      savedguid := Null;
      savedResultFolder := Null;
      if FTasks.RecordCount > 0 then
      begin
        savedguid := FTasks['GUID'];
        if FResults.RecordCount > 0 then
          savedResultFolder := FResults['FOLDER']
      end;

      FResults.EmptyTable;
      FTasks.EmptyTable;

      taskguids := GetTaskGUIDs(FLogger);

      for i := 0 to taskguids.Count-1 do
      begin
        FLogger.LogEntry('Adding task to list');
        try
          try
            FLogger.LogContextItem('Internal task ID', taskguids.Str[i]);
            task := LoadTask(taskguids.Str[i]);
            FLogger.LogDebug('App task dump', task.Dump);
            AddTaskToDS(task);

            t := TryFindSysTaskByGUID(taskguids.Str[i]);
            if t <> nil then
            begin
              FTasks.Edit;
              try
                FTasks['ENABLED'] := not (tfDisabled in t.Flags);
                FTasks.Post;
              except
                FTasks.Cancel;
                raise;
              end;
            end;

          except
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
      end;

      if not VarIsNull(savedguid) then
      begin
        FTasks.Locate('GUID', savedguid, []);
        if not VarIsNull(savedResultFolder) then
          FResults.Locate('FOLDER', savedResultFolder, []);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TScheduler.AddTaskToDS(task: IScheduledTask);
var
  results: TScheduledTaskResults;
  i: integer;
  lastRunTime: TDateTime;
  lastRunStatus: TScheduledTaskResultStatus;
begin
  FTasks.Append;
  try
    FTasks['GUID'] := task.GUID;

    FTasks['NAME'] := task.UserFriendlyName;
    FTasks['CUSTOM_CLIENT_NUMBER'] := task.Company.CUSTOM_CLIENT_NUMBER;
    FTasks['CUSTOM_COMPANY_NUMBER'] := task.Company.CUSTOM_COMPANY_NUMBER;
    FTasks['COMPANY_NAME'] := task.Company.Name;
    FTasks['CLIENT_NAME'] := task.Company.ClientName;
    FTasks['SUMMARY'] := FTaskAdapter.Describe(FLogger, task);

//    FTasks['LAST_RUN_TIME'] := Null;
    FTasks['LAST_RUN_STATUS'] := 'Never run';
    FTasks.Post;
  except
    FTasks.Cancel;
  end;

  lastRunTime := 0;
  lastRunStatus := statusInvalid; //to make compiler happy

  results := task.GetTaskResults;
  for i := 0 to high(results) do
  begin
    FResults.Append;
    try
      FResults['GUID'] := task.GUID;
      FResults['FOLDER'] := results[i].Folder;
      FResults['WHEN'] := results[i].When;
      FResults['STATUS'] := StatusToString(results[i].Status.Code);
      FResults['MESSAGE'] := results[i].Status.Message;
      FResults.Post;
    except
      FResults.Cancel;
    end;
    if results[i].When >= lastRunTime then
    begin
      lastRunTime := results[i].When;
      lastRunStatus := results[i].Status.Code;
    end;
  end;

  if Length(results) > 0 then
  begin
    FTasks.Edit;
    try
      FTasks['LAST_RUN_TIME'] := lastRunTime;
      FTasks['LAST_RUN_STATUS'] := StatusToString(lastRunStatus);
      FTasks.Post;
    except
      FTasks.Cancel;
    end;
  end;

end;

procedure TScheduler.HandleBeforeDelete(DataSet: TDataSet);
begin
  FSysScheduler.Refresh;
  FSysScheduler.Remove( TaskNameByGUID(DataSet['GUID']) );
  DeleteTask(DataSet['GUID']);
end;

procedure TScheduler.DeleteCurrentTask;
begin
  FResults.First;
  while not FResults.Eof do
    FResults.Delete;
  FTasks.Delete;
end;

function TScheduler.TaskNameByGUID(guid: string): string;
begin
  Result := ChangeFileExt(GetAppFilename, '') + ' - ' + guid + '.job';
end;

procedure TScheduler.ShowScheduleForCurrentTask;
var
  ATask: TJclScheduledTask;
begin
  FLogger.LogEntry('Showing schedule dialog');
  try
    try
      ATask := FindOrCreateSysTaskByGUID(FTasks['GUID']);
      if ATask.ShowPage([ppSchedule], Application.Title, Application.MainForm.Handle) then
      begin
        Flogger.LogDebug('Saved');
        ATask.Save;
      end
      else
        Flogger.LogDebug('Not saved');
    except
      FLogger.PassthroughException
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TScheduler.LogSysTask(sysTask: TJclScheduledTask);
begin
  FLogger.LogEntry('Logging system task');
  try
    try
      FLogger.LogDebug('TaskName: ' + UTF8Encode(sysTask.TaskName));
      FLogger.LogDebug('AccountName: ' + UTF8Encode(sysTask.AccountName));
      FLogger.LogDebug('Creator: ' + UTF8Encode(sysTask.Creator));
      FLogger.LogDebug('ExitCode: ' + IntToStr(sysTask.ExitCode));
      FLogger.LogDebug('Status: ' + GetEnumName(TypeInfo(TJclScheduledTaskStatus), ord(sysTask.Status)));
      if sysTask.MostRecentRunTime.wYear <> 0 then
        FLogger.LogDebug('MostRecentRunTime: ' + DateTimeToStr(SystemTimeToDateTime(sysTask.MostRecentRunTime)))
      else
        FLogger.LogDebug('MostRecentRunTime: 0');
      if sysTask.NextRunTime.wYear <> 0 then
        FLogger.LogDebug('NextRunTime: ' + DateTimeToStr(SystemTimeToDateTime(sysTask.NextRunTime)))
      else
        FLogger.LogDebug('NextRunTime: 0');
    except
      FLogger.StopException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TScheduler.LogSysTasks;
var
  i: integer;
begin
  for i := 0 to FSysScheduler.TaskCount-1 do
    LogSysTask(FSysScheduler.Tasks[i]);
end;

function TScheduler.TryFindSysTaskByGUID(guid: string): TJclScheduledTask;
var
  i: integer;
  tname: string;
begin
  Result := nil;
  FLogger.LogEntry('Looking up system task by internal id');
  try
    try
      FLogger.LogContextItem('guid', guid);
      tname := TaskNameByGUID(guid);
      FLogger.LogContextItem('task name', tname);
      for i := 0 to FSysScheduler.TaskCount-1 do
        if WideCompareText(FSysScheduler.Tasks[i].TaskName, tname) = 0 then
        begin
          Result := FSysScheduler.Tasks[i];
          FLogger.LogDebug('Found.');
          LogSysTask(Result);
          exit;
        end;
       FLogger.LogDebug('Not found');
    except
      FLogger.PassthroughException
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TScheduler.FindOrCreateSysTaskByGUID(guid: string): TJclScheduledTask;
begin
  Result := TryFindSysTaskByGUID(guid);
  if Result = nil then
  begin
    FLogger.LogWarningFmt('Cannot find system task with name %s. Creating new one.', [TaskNameByGUID(guid)]);
    Result := AddTaskToSysScheduler(guid);
  end;
end;

function TScheduler.ResultsDataSet: TDataSet;
begin
  Result := FResults;
end;

function TScheduler.TasksDataSet: TDataSet;
begin
  Result := FTasks;
end;

procedure TScheduler.RunCurrentTask;
var
  t: TJclScheduledTask;
begin
  FSysScheduler.Refresh;
  t := FindOrCreateSysTaskByGUID(FTasks['GUID']);
  t.Run;
end;

function TScheduler.IsCurrentTaskRunning: boolean;
var
  t: TJclScheduledTask;
begin
  FSysScheduler.Refresh;
  t := FindOrCreateSysTaskByGUID(FTasks['GUID']);
  Result := t.Status = tsRunning;
end;

procedure TScheduler.TaskParametersEdited(task: IScheduledTask);
begin
  if FTasks.Locate('GUID', task.GUID, []) then
  begin
    FTasks.Edit;
    try
      FTasks['SUMMARY'] := FTaskAdapter.Describe(FLogger, task);
      FTasks.Post;
    except
      FTasks.Cancel;
    end
  end
  else
    Assert(false);
end;

function TScheduler.AllResultsDataSet: TDataSet;
begin
  Result := FAllResults;
end;

procedure TScheduler.EnableCurrentTask(enable: boolean);
var
  t: TJclScheduledTask;
begin
  FSysScheduler.Refresh;
  t := FindOrCreateSysTaskByGUID(FTasks['GUID']);

  // it does not catch that Flags has been changes, so let update Comment as well
  if enable then
  begin
    t.Flags := t.Flags - [tfDisabled];
    t.Comment := Format('Please use "%s" to manage this task.', [Application.Title]);
  end
  else begin
    t.Flags := t.Flags + [tfDisabled];
    t.Comment := Format('Please use "%s" to manage this task. Task is disabled.', [Application.Title]);
  end;

  t.Save;
end;

function TScheduler.CurrentTaskAccount: string;
begin
  Result := FindOrCreateSysTaskByGUID(FTasks['GUID']).AccountName;
end;

procedure TScheduler.CurrentTaskSetAccountInfo(const Account, Password: string);
var
  t: TJclScheduledTask;
  A, P: WideString;
begin
  A := Account;
  P := Password;
  t := FindOrCreateSysTaskByGUID(FTasks['GUID']);
  if Password <> '' then
  begin
    t.Flags := t.Flags - [tfRunOnlyIfLoggedOn];
    OleCheck(t.ScheduledWorkItem.SetAccountInformation(PWideChar(A), PWideChar(P)));
  end else
  begin
    t.Flags := t.Flags + [tfRunOnlyIfLoggedOn];
    OleCheck(t.ScheduledWorkItem.SetAccountInformation(PWideChar(A), nil));
  end;

  t.Save;
end;

end.


