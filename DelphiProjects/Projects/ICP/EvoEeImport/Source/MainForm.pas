unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientMainForm, ComCtrls, OptionsBaseFrame, evodata,
  EvoAPIConnectionParamFrame, CSVFileFrame, ExtCtrls, ActnList, StdCtrls,
  Buttons, EeImport;

const
  SY_STATESDesc: TEvoDSDesc = (Name: 'SY_STATES');
  SY_STATE_MARITAL_STATUSDesc: TEvoDSDesc = (Name: 'SY_STATE_MARITAL_STATUS');

type
  TMainFm = class(TEvoAPIClientMainFm)
    pnlButtons: TPanel;
    CSVFile: TCSVFileFrm;
    Actions: TActionList;
    ConnectToEvo: TAction;                        
    btnConnect: TBitBtn;
    btnImport: TBitBtn;
    ImportEmployees: TAction;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImportEmployeesExecute(Sender: TObject);
    procedure ImportEmployeesUpdate(Sender: TObject);

  protected
    FEvoData: TEvoData;
    FEEImport: TEEImport;

    procedure ConnectEvoData;
    procedure AutoConnect;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}
uses EEUtilityLoggerViewFrame, gdyDeferredCall, kbmMemTable, InitStackTracer;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEEUtilityLoggerViewFrm;
  inherited;
  FEvoData := TEvoData.Create(Logger);
  FEvoData.AddDS(SY_STATESDesc);
  FEvoData.AddDS(SY_STATE_MARITAL_STATUSDesc);

  if EvoFrame.IsValid then
    PageControl1.TabIndex := TabSheet2.TabIndex
  else
    PageControl1.TabIndex := TabSheet3.TabIndex;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FEvoData);
  inherited;
end;

procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      ConnectEvoData;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.AutoConnect;
begin
  Logger.LogEntry('Application opened');
  try
    try
      Repaint;
      if EvoFrame.IsValid then
        ConnectEvoData;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.FormShow(Sender: TObject);
begin
  DeferredCall(AutoConnect);
end;

procedure TMainFm.ConnectEvoData;
begin
  FEvoData.Connect(EvoFrame.Param);
  if FEvoData.Connected then
  begin
    ConnectToEvo.Caption := 'Refresh data';
    FEEImport := TEEImport.Create(FEvoData.Connection, FEvoData, Logger, ProgressIndicator, EvoFrame.Param);
    CSVFile.Init(FEEImport.EeData, Logger);
  end;
end;

procedure TMainFm.ImportEmployeesExecute(Sender: TObject);
begin
  FEEImport.ImportEmployees;
end;

procedure TMainFm.ImportEmployeesUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(FEvoData) and FEvoData.Connected and Assigned(FEEImport) and
    (FEEImport.EeData.Active) and (FEEImport.EeData.RecordCount > 0);
end;

end.
