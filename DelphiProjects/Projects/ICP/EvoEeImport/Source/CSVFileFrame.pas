unit CSVFileFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileOpenLeftBtnFrame, Grids, Wwdbigrd, Wwdbgrid,
  dbcomp, csreader, DB, gdyCommonLogger, EeImport;

type
  TCSVFileFrm = class(TFrame)
    CSVFileFrame: TFileOpenLeftBtnFrm;
    dbgData: TReDBGrid;
    dsCSVData: TDataSource;
  private
    FLogger: ICommonLogger;

    function CheckTermCode(const aTermCode: string): string;
    function CheckPayFrequency(const aPayFrequency: string): string;
    function CheckFederalMaritalStatus(const aValue: string): string;
    function CheckErOASDIExempt(const aValue: string): string;

    procedure FileSelected(filename: string);
    procedure ProcessRecord( rec: TStrings );
  public
    procedure Init(aDS: TDataset; Logger: ICommonLogger);
  end;

implementation

{$R *.dfm}

function PadStringLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := MyString;
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

{ TCSVFileFrm }

procedure TCSVFileFrm.FileSelected(filename: string);
begin
  FLogger.LogEntry('Load data from csv file');
  if Assigned(dsCSVData.DataSet) then
  begin
    dsCSVData.DataSet.Close;
    dsCSVData.DataSet.Open;

    EachRecord(filename, ProcessRecord, True);
  end;
end;

procedure TCSVFileFrm.Init(aDS: TDataset; Logger: ICommonLogger);
begin
  FLogger := Logger;
  CSVFileFrame.OnFileSelected := FileSelected;
  dsCSVData.DataSet := aDS;
end;

function TCSVFileFrm.CheckTermCode(const aTermCode: string): string;
begin
  Result := Trim(aTermCode);
  if (Result <> '') and  not (Result[1] in ['A', 'I', 'C', 'L', 'F', 'R', 'E', 'T', 'Y', 'V', 'S', 'D', 'P', 'M', 'Z', 'N']) then
    raise Exception.Create('Wrong Termination Code value: "'+aTermCode+'". Should be one of the following: "A", "I", "C", "L", "F", "R", "E", "T", "Y", "V", "S", "D", "P", "M", "Z", "N"');
end;

function TCSVFileFrm.CheckPayFrequency(const aPayFrequency: string): string;
begin
  Result := Trim(aPayFrequency);
  if (Result <> '') and (not (Result[1] in ['D', 'W', 'B', 'S', 'M', 'Q'])) then
    raise Exception.Create('Wrong Pay Frequency value: "'+aPayFrequency+'". Should be one of the following: "D", "W", "B", "S", "M", "Q"');
end;

function TCSVFileFrm.CheckFederalMaritalStatus(const aValue: string): string;
begin
  Result := Trim(aValue);
  if (Result <> '') and not (Result[1] in ['S', 'M']) then
    raise Exception.Create('Wrong Federal Marital Status value: "'+aValue+'". Should be one of the following: "S", "M"');
end;

procedure TCSVFileFrm.ProcessRecord(rec: TStrings);
var
  i, intValue: integer;
  curValue: Currency;
  datValue: TDatetime;
  bPost: boolean;
begin
  if rec.Count < (High( FieldsToImport ) - Low( FieldsToImport ) + 1) then
    FLogger.LogError('The record "' + rec.CommaText + '" has not been processed. There should be ' + IntToStr(High( FieldsToImport ) - Low( FieldsToImport ) + 1) + ' columns.')
  else
  begin
    bPost := True;
    dsCSVData.DataSet.Append;
    for i := 0 to rec.Count - 1 do
    try
      if FieldsToImport[i + 1].M and (Trim(rec[i]) = '') then
        raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. The column "' + FieldsToImport[i + 1].C + '" is mandatory.')
      else if (i + 1) = idxEeCode then
        dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsString := PadStringLeft( Trim(Copy(Trim(rec[i]), 1, FieldsToImport[i + 1].S)), ' ', 9 )
      else if (i + 1) = idxTermCode then
        dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsString := CheckTermCode(Copy(Trim(rec[i]), 1, FieldsToImport[i + 1].S))
      else if (i + 1) = idxFederalMaritalStatus then
        dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsString := CheckFederalMaritalStatus(Copy(Trim(rec[i]), 1, FieldsToImport[i + 1].S))
      else if (i + 1) = idxPayFrequency then
        dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsString := CheckPayFrequency(Copy(Trim(rec[i]), 1, FieldsToImport[i + 1].S))
      else if (i + 1) = idxErOASDIExempt then
        dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsString := CheckErOASDIExempt(Copy(Trim(rec[i]), 1, FieldsToImport[i + 1].S))
      else if FieldsToImport[i + 1].T = ftInteger then
      begin
        if TryStrToInt(Trim(rec[i]), intValue) then
          dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsInteger := intValue
        else
          raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. The column "' + FieldsToImport[i + 1].C + '" should contain an integer value.');
      end
      else if FieldsToImport[i + 1].T = ftCurrency then
      begin
        if TryStrToCurr(Trim(rec[i]), curValue) then
          dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsCurrency := curValue
        else
          raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. The column "' + FieldsToImport[i + 1].C + '" should contain a currency value.');
      end
      else if FieldsToImport[i + 1].T = ftDateTime then
      begin
        if TryStrToDateTime(Trim(rec[i]), datValue) then
          dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsDatetime := datValue
        else
          raise Exception.Create('The record "' + rec.CommaText + '" has not been processed. The column "' + FieldsToImport[i + 1].C + '" should contain a datetime value.');
      end
      else
        dsCSVData.DataSet.FieldByName(FieldsToImport[i + 1].N).AsString := Trim(Copy(Trim(rec[i]), 1, FieldsToImport[i + 1].S));
    except
      on E: Exception do
      begin
        bPost := False;
        FLogger.LogError(E.Message);
      end;
    end;
    if bPost then
      dsCSVData.DataSet.Post
    else
      dsCSVData.DataSet.Cancel;
  end;
end;

function TCSVFileFrm.CheckErOASDIExempt(const aValue: string): string;
begin
  Result := Trim(aValue);
  if Result = '' then
    Result := 'N';
  if (Result <> '') and (not (Result[1] in ['Y', 'N'])) then
    raise Exception.Create('Wrong Er OASDI Exempt value: "'+aValue+'". Should be one of the following: "Y", "N"');
end;

end.
