program EvoEeImport;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TFrame},
  sevenzip in '..\..\common\gdycommon\sevenzip\sevenzip.pas',
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  MainForm in 'MainForm.pas' {MainFm},
  CSVFileFrame in 'CSVFileFrame.pas' {CSVFileFrm: TFrame},
  FileOpenLeftBtnFrame in '..\..\common\FileOpenLeftBtnFrame.pas' {FileOpenLeftBtnFrm: TFrame},
  EeImport in 'EeImport.pas',
  csreader in 'csreader.pas';

{$R *.res}

begin
  LicenseKey := 'EF34EB4164834C4A94CF10FCE89AE3D3'; //advanced evo adi
  //LicenseKey := '{5E465AFC-D0A1-424E-B21B-4351ECD6BFFF}'; //evo.legacy.*

  Application.Initialize;
  Application.Title := 'Evolution Employee Import';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
