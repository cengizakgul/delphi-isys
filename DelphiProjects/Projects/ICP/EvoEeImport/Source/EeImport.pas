unit EeImport;

interface

uses
  common, evoapiconnection, gdyCommonLogger, kbmMemTable, DB, evodata,
  XmlRpcTypes, evoapiconnectionutils, Classes, gdyClasses, Forms;

type
  FieldDescr = record
    C: string;     // Caption
    M: boolean;    // Mandatory Field
    T: TFieldType; // Type
    S: integer;    // Size
    N: string;     // Name
    Tb: string;    // Table Name
  end;

const
  idxCoNumber = 1;
  idxSSN = 2;
  idxEeCode = 3;
  idxDepartment = 4;
  idxNotUsed1 = 5;
  idxNotUsed2 = 6;
  idxLastName = 7;
  idxFirstName = 8;
  idxMI = 9;
  idxAddress1 = 10;
  idxCity = 11;
  idxZipCode = 12;
  idxState = 13; // idxHomeState = 19;  idxSuiState = 23; idxSdiState = 24; idxStateTaxCode = 25;
  idxCompanyHomeState = 13;
  idxHireDate = 14;
  idxTermCode = 15;
  idxPayFrequency = 16;
  idxDivision = 17; // idxRateNumber = 26;
  idxBranch = 17;
  idxRateAmount = 18;
  idxFederalMaritalStatus = 19;
  idxNumberOfDependents = 20;
  idxStateMaritalStatus = 21;
  idxStateDep = 22;
  idxNotUsed3 = 23;
  idxErOASDIExempt = 24;

  EeChunkSize = 100;

  FieldsToImport: array[1..24] of FieldDescr =
  (
    (C: 'Custom Company Number'; M: True; T: ftString; S: 20; N: 'CUSTOM_COMPANY_NUMBER'; Tb: 'CO'),
    (C: 'SSN'; M: True; T: ftString; S: 11; N: 'SOCIAL_SECURITY_NUMBER'; Tb: 'CL_PERSON'),
    (C: 'Ee Code'; M: True; T: ftString; S: 10; N: 'CUSTOM_EMPLOYEE_NUMBER'; Tb: 'EE'),
    (C: 'Department'; M: False; T: ftString; S: 20; N: 'CO_DEPARTMENT_NBR'; Tb: 'EE'),
    (C: '<Not used>'; M: False; T: ftString; S: 20; N: 'XXX1'; Tb: 'XXX'),
    (C: '<Not used>'; M: False; T: ftString; S: 20; N: 'XXX2'; Tb: 'XXX'),
    (C: 'Last Name'; M: True; T: ftString; S: 30; N: 'LAST_NAME'; Tb: 'CL_PERSON'),
    (C: 'First Name'; M: True; T: ftString; S: 20; N: 'FIRST_NAME'; Tb: 'CL_PERSON'),
    (C: 'MI'; M: False; T: ftString; S: 1; N: 'MIDDLE_INITIAL'; Tb: 'CL_PERSON'),
    (C: 'Address1'; M: True; T: ftString; S: 30; N: 'ADDRESS1'; Tb: 'CL_PERSON'),
    (C: 'City'; M: True; T: ftString; S: 20; N: 'CITY'; Tb: 'CL_PERSON'),
    (C: 'Zip Code'; M: True; T: ftString; S: 10; N: 'ZIP_CODE'; Tb: 'CL_PERSON'),
    (C: 'State'; M: True; T: ftString; S: 2; N: 'STATE'; Tb: 'CL_PERSON'),
    (C: 'Current Hire Date'; M: False; T: ftDateTime; S: 0; N: 'CURRENT_HIRE_DATE'; Tb: 'EE'),
    (C: 'Current Status Code'; M: True; T: ftString; S: 1; N: 'CURRENT_TERMINATION_CODE'; Tb: 'EE'),
    (C: 'Pay Frequency'; M: True; T: ftString; S: 1; N: 'PAY_FREQUENCY'; Tb: 'EE'),
    (C: 'Division'; M: False; T: ftString; S: 20; N: 'CO_DIVISION_NBR'; Tb: 'EE'),
    (C: 'Rate Amount #01'; M: False; T: ftCurrency; S: 0; N: 'RATE_AMOUNT'; Tb: 'EE_RATES'),
    (C: 'Federal Marital Status'; M: True; T: ftString; S: 1; N: 'FEDERAL_MARITAL_STATUS'; Tb: 'EE'),
    (C: 'Federal Dependents'; M: True; T: ftInteger; S: 0; N: 'NUMBER_OF_DEPENDENTS'; Tb: 'EE'),
    (C: 'State Marital Status'; M: True; T: ftString; S: 2; N: 'STATE_MARITAL_STATUS'; Tb: 'EE_STATES'),
    (C: 'State Dep'; M: False; T: ftInteger; S: 0; N: 'STATE_NBR_WITHHOLD_ALLOW'; Tb: 'EE_STATES'),
    (C: '<Not used>'; M: False; T: ftString; S: 20; N: 'XXX3'; Tb: 'XXX'),
    (C: 'ER OASDI Exempt'; M: False; T: ftString; S: 1; N: 'EXEMPT_EMPLOYER_OASDI'; Tb: 'EE')
  );

//    (C: 'Branch'; M: False; T: ftString; S: 20; N: 'CO_BRANCH_NBR'; Tb: 'EE'), //the same as Division (17)
//    (C: 'Home State'; M: True; T: ftString; S: 0; N: 'HOME_TAX_EE_STATES_NBR'; Tb: 'EE'), // the same as State (13)
//    (C: 'Company Home State'; M: False; T: ftString; S: 2; N: 'CO_STATES_NBR'; Tb: 'EE_STATES'), // the same as State (13)
//    (C: 'Rate Number #01'; M: False; T: ftInteger; S: 0; N: 'RATE_NUMBER'; Tb: 'EE_RATES'), //the same as Division (17)
//    (C: 'SUI State'; M: False; T: ftString; S: 2; N: 'SUI_APPLY_CO_STATES_NBR'; Tb: 'EE_STATES'), // the same as State (13)
//    (C: 'SDI State'; M: False; T: ftString; S: 2; N: 'SDI_APPLY_CO_STATES_NBR'; Tb: 'EE_STATES'), // the same as State (13)
//    (C: 'State Tax Code'; M: False; T: ftString; S: 20; N: 'EE_TAX_CODE'; Tb: 'EE_STATES'), // the same as State (13)

type
  TEEImport = class
  private
    FCoDef: TEvoCompanyDef;
    FData: TkbmCustomMemTable;
    FConn: IEvoAPIConnection;
    FLogger: ICommonLogger;
    FProgressIndicator: IProgressIndicator;
    FEvoData: TEvoData;
    FCoStates: TkbmCustomMemTable;
    FEEData: TkbmCustomMemTable;
    FEEStates: TkbmCustomMemTable;
    FEERates: TkbmCustomMemTable;
    FClPerson: TkbmCustomMemTable;
    FDBDT: TkbmCustomMemTable;

    FEeCodeListFilters: TStrings;
    FSSNListFilters: TStrings;

    FStateMaritalStatus: string;
    FSyStateMaritalStatusNbr: integer;
    Fparam: TEvoAPIConnectionParam;

    function ApplyRecordChange(aFields: IRpcStruct; const aTableName, aType: string; aKey: integer): integer;

    procedure UpdateEeCodeListFilters;

    procedure FillEEDefaults(aFields: IRpcStruct);
    procedure FillEEStatesDefaults(aFields: IRpcStruct);
    procedure FillClPersonDefaults(aFields: IRpcStruct);

    function GetCoStateNbr(const aState: string): integer;
    function LocateDBDT(const CDivNbr, CBrNbr, CDepNbr: string): boolean;
    function LocateSyStateMaritalStatusNbr: boolean;

    procedure AddToUpdate(aDS:TkbmCustomMemTable; aMap: IRpcStruct; aFieldIndex: integer);
    function UpdateClPerson(aClPersonNbr: integer): integer;

    function RunQuery(const aFileName: string; params: IRpcStruct): TkbmCustomMemTable;

    procedure CreateEvoEE;
    procedure UpdateEvoEE;
  public
    constructor Create(Conn: IEvoAPIConnection; EvoData: TEvoData; Logger: ICommonLogger; PI: IProgressIndicator; param: TEvoAPIConnectionParam);
    destructor Destroy; override;

    procedure CreateFields;
    procedure ImportEmployees;
    property EeData: TkbmCustomMemTable read FData;
  end;

implementation

uses SysUtils, Variants, DateUtils, gdyRedir;

function ConvertNull(MyValue, Replacement: Variant): Variant;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function CreateCurrencyField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TCurrencyField;
var
  TF: TCurrencyField;
begin
  TF := TCurrencyField.Create(aDataSet);
  with TF do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;
  result := TF;
end;

{ TEEImport }

function TEEImport.GetCoStateNbr(const aState: string): integer;
begin
  if FCoStates.Locate('State', aState, []) then
    Result := FCoStates.FieldByName('CO_STATES_NBR').AsInteger
  else begin
    raise Exception.Create('Company ' + FCoDef.CUSTOM_COMPANY_NUMBER + ' doesn''t have ' + aState + ' state set up');
  end;
end;

constructor TEEImport.Create(Conn: IEvoAPIConnection; EvoData: TEvoData; Logger: ICommonLogger; PI: IProgressIndicator; param: TEvoAPIConnectionParam);
begin
  FConn := Conn;
  FLogger := Logger;
  FData := TkbmCustomMemTable.Create(nil);
  CreateFields;
  FEvoData := EvoData;
  FEeCodeListFilters := TStringList.Create;
  FSSNListFilters := TStringList.Create;
  FProgressIndicator := PI;
  Fparam := param;
end;

procedure TEEImport.CreateFields;
var i: integer;
begin
  for i := Low(FieldsToImport) to High(FieldsToImport) do
  begin
    if FieldsToImport[i].T = ftString then
      CreateStringField(FData, FieldsToImport[i].N, FieldsToImport[i].C, FieldsToImport[i].S)
    else if FieldsToImport[i].T = ftInteger then
      CreateIntegerField(FData, FieldsToImport[i].N, FieldsToImport[i].C)
    else if FieldsToImport[i].T = ftCurrency then
      CreateCurrencyField(FData, FieldsToImport[i].N, FieldsToImport[i].C)
    else if FieldsToImport[i].T = ftDateTime then
      CreateDateTimeField(FData, FieldsToImport[i].N, FieldsToImport[i].C);
  end;
end;

destructor TEEImport.Destroy;
begin
  FreeAndNil( FData );
  FreeAndNil( FEEData );
  FreeAndNil( FCoStates );
  FreeAndNil( FEEStates );
  FreeAndNil( FEERates );
  FreeAndNil( FEeCodeListFilters );
  FreeAndNil( FSSNListFilters );
  FreeAndNil( FClPerson );
  FreeAndNil( FDBDT );

  inherited;
end;

procedure TEEImport.ImportEmployees;
var
  EeCode: string;
  i, j, idxEeCodeList: integer;

  procedure OpenNextCompany;
  var
    ClCoNbrs: TkbmCustomMemTable;
  begin
    FLogger.LogEntry('Open Company');
    try
      FLogger.LogContextItem('Custom Company Number', FCoDef.CUSTOM_COMPANY_NUMBER);

//      FCoDef.ClNbr := ConvertNull( FEvoData.DS['TMP_CO'].Lookup('CUSTOM_COMPANY_NUMBER', FCoDef.CUSTOM_COMPANY_NUMBER, 'CL_NBR'), 0);
//      FCoDef.CoNbr := ConvertNull( FEvoData.DS['TMP_CO'].Lookup('CUSTOM_COMPANY_NUMBER', FCoDef.CUSTOM_COMPANY_NUMBER, 'CO_NBR'), 0);
      ClCoNbrs := FConn.OpenSQL(
        'SELECT t1.CL_NBR, t1.CO_NBR FROM TMP_CO t1 '+
        'where t1.CUSTOM_COMPANY_NUMBER=''' + FCoDef.CUSTOM_COMPANY_NUMBER + '''',
        0,
        '' );
//      if FEvoData.DS['TMP_CO'].Locate('CUSTOM_COMPANY_NUMBER', FCoDef.CUSTOM_COMPANY_NUMBER, []) then
      if ClCoNbrs.FieldByName('CL_NBR').AsInteger > 0 then
        try
          FCoDef.ClNbr := ClCoNbrs.FieldByName('CL_NBR').AsInteger;
          FCoDef.CoNbr := ClCoNbrs.FieldByName('CO_NBR').AsInteger;
          FEvoData.SetClCo( FCoDef );

          FDBDT := ExecCoQuery( FConn, FLogger, FCoDef, 'DBDT.rwq', 'Getting D/B/D/Ts from Evolution');
          FCoStates := ExecCoQuery( FConn, FLogger, FCoDef, 'CoStates.rwq', 'Getting company states from Evolution');

          i := EeChunkSize;
        except
          FLogger.StopException;
        end
      else
        FLogger.LogWarning('Company #' + FCoDef.CUSTOM_COMPANY_NUMBER +
          ' is not found! Please, check if such company exists in "' + Fparam.APIServerAddress + '" ' +
          'and user "' + Fparam.Username + '" does have rights to access that company.');
    finally
      FLogger.LogExit;
      if Assigned(ClCoNbrs) then
        FreeAndNil(ClCoNbrs);
    end;
  end;

  procedure OpenEeDatasets(idxEeCodeList: integer);
  var
    Param: IRpcStruct;
  begin
    FLogger.LogEntry('Open EE datasets');
    try
      FLogger.LogContextItem('Custom Company Number', FCoDef.CUSTOM_COMPANY_NUMBER);
      try
        param := TRpcStruct.Create;
        param.AddItem( 'CO_NBR', FCoDef.CoNbr );
        param.AddItem( 'EeCodeList', FEeCodeListFilters[idxEeCodeList] );

        FEeData := RunQuery( 'EmployeesFull.rwq', param );
        FEEStates := RunQuery( 'EeStates.rwq', param );
        FEERates := RunQuery( 'EeRates.rwq', param );

        param.Clear;
        param.AddItem( 'SSNList', FSSNListFilters[idxEeCodeList] );
        FClPerson := RunQuery( 'ClPersons.rwq', param );
      except
        FLogger.StopException;
      end;
    finally
      FLogger.LogExit;
    end;
  end;
begin
  if (FData.Active) and (FData.RecordCount > 0) then
  begin
    UpdateEeCodeListFilters; //this is needed to select the only ees that are going to be imported (if they are exists in client db)
    FData.First;
    i := 0;
    j := 0;
    idxEeCodeList := 0;
    FCoDef.CUSTOM_COMPANY_NUMBER := '';
    FProgressIndicator.StartWait('Import Employees', FData.RecordCount);
    while not FData.Eof do
    begin
      FProgressIndicator.Step(j);
      Application.ProcessMessages;
      
      if FCoDef.CUSTOM_COMPANY_NUMBER <> FData.FieldByName(FieldsToImport[idxCoNumber].N).AsString then
      begin
        FCoDef.CUSTOM_COMPANY_NUMBER := FData.FieldByName(FieldsToImport[idxCoNumber].N).AsString;
        OpenNextCompany;
      end;

      if (FCoDef.ClNbr > 0) and (i = EeChunkSize) then
      begin
        OpenEeDatasets(idxEeCodeList);
        Inc(idxEeCodeList);
        i := 0;
      end;

      if (FCoDef.ClNbr > 0) and Assigned(FEEData) then
      begin
        FLogger.LogEntry('Processing employee record');
        try
          try
            EeCode := FData.FieldByName(FieldsToImport[idxEeCode].N).AsString;
            FLogger.LogContextItem(sCtxEECode, EeCode);
            FLogger.LogContextItem('First Name', FData.FieldByName(FieldsToImport[idxFirstName].N).AsString);
            FLogger.LogContextItem('Last Name', FData.FieldByName(FieldsToImport[idxLastName].N).AsString);
            FLogger.LogContextItem('SSN', FData.FieldByName(FieldsToImport[idxSSN].N).AsString);

            if FEEData.Locate('CUSTOM_EMPLOYEE_NUMBER', EeCode, []) then
              UpdateEvoEE
            else
              CreateEvoEE;
          except
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
      end;
      FData.Next;
      Inc(i);
      Inc(j);
    end;
    FProgressIndicator.EndWait;
  end;
end;

procedure TEEImport.UpdateEeCodeListFilters;
var
  i: integer;
  CoNumber: string;
  EeCodes: TStrings;
  SSNList: TStrings;
begin
  FData.SortFields := FieldsToImport[idxCoNumber].N;
  FData.First;
  i := 0;
  CoNumber := FData.FieldByName(FieldsToImport[idxCoNumber].N).AsString;
  EeCodes := TStringList.Create;
  SSNList := TStringList.Create;
  try
    while not FData.Eof do
    begin
      if CoNumber <> FData.FieldByName(FieldsToImport[idxCoNumber].N).AsString then
      begin
        CoNumber := FData.FieldByName(FieldsToImport[idxCoNumber].N).AsString;
        i := EeChunkSize;
      end;

      if i = EeChunkSize then
      begin
        FEeCodeListFilters.Add( EeCodes.CommaText );
        FSSNListFilters.Add( SSNList.CommaText );
        EeCodes.Clear;
        SSNList.Clear;
        i := 0;
      end;

      EeCodes.Add( FData.FieldByName(FieldsToImport[idxEeCode].N).AsString );
      SSNList.Add( FData.FieldByName(FieldsToImport[idxSSN].N).AsString );

      Inc(i);
      FData.Next;
    end;
    if EeCodes.Count > 0 then
    begin
      FEeCodeListFilters.Add( EeCodes.CommaText );
      FSSNListFilters.Add( SSNList.CommaText );
    end;
  finally
    FreeAndNil( EeCodes );
    FreeAndNil( SSNList );
  end;
end;

function TEEImport.UpdateClPerson(aClPersonNbr: integer): integer;
var
  systatenbr: Variant;
  clp: string;
  FieldList: IRpcStruct;
  ChangeType: string;
begin
  FieldList := TRpcStruct.Create;

  systatenbr := FEvoData.DS['SY_STATES'].Lookup('STATE', trim(FData.FieldByName(FieldsToImport[idxCompanyHomeState].N).AsString), 'SY_STATES_NBR');
  if VarIsNull(systatenbr) then
    raise Exception.Create('Missing State');

  Result := aClPersonNbr;
  if (aClPersonNbr = -1) and
    FClPerson.Locate('SOCIAL_SECURITY_NUMBER', FData.FieldByName(FieldsToImport[idxSSN].N).AsString, []) then
    Result := FClPerson.FieldByName('CL_PERSON_NBR').AsInteger
  else
    FClPerson.Locate('CL_PERSON_NBR', aClPersonNbr, []);

  if Result = -1 then
  begin
    ChangeType := 'I';
    FieldList.AddItem('CL_PERSON_NBR', -1 );
    FieldList.AddItem('LAST_NAME', FData.FieldByName(FieldsToImport[idxLastName].N).AsString );
    FieldList.AddItem('FIRST_NAME', FData.FieldByName(FieldsToImport[idxFirstName].N).AsString );
    FieldList.AddItem('MIDDLE_INITIAL', FData.FieldByName(FieldsToImport[idxMI].N).AsString );
    FieldList.AddItem('SOCIAL_SECURITY_NUMBER', FData.FieldByName(FieldsToImport[idxSSN].N).AsString );
    FieldList.AddItem('ADDRESS1', FData.FieldByName(FieldsToImport[idxAddress1].N).AsString );
    FieldList.AddItem('CITY', FData.FieldByName(FieldsToImport[idxCity].N).AsString );
    FieldList.AddItem('STATE', FData.FieldByName(FieldsToImport[idxState].N).AsString );
    FieldList.AddItem('ZIP_CODE', FData.FieldByName(FieldsToImport[idxZipCode].N).AsString );
    FieldList.AddItem('RESIDENTIAL_STATE_NBR', Integer(systatenbr) );
  end
  else begin
    ChangeType := 'U';
    clp := IntToStr( Result );

    AddToUpdate(FClPerson, FieldList, idxLastName);
    AddToUpdate(FClPerson, FieldList, idxFirstName);
    AddToUpdate(FClPerson, FieldList, idxMI);

    if (FClPerson.FieldByName(FieldsToImport[idxSSN].N).AsString <> '000-00-0000') and //SSN edit restricted
      (FClPerson.FieldByName(FieldsToImport[idxSSN].N).AsString <> FData.FieldByName(FieldsToImport[idxSSN].N).AsString) then
      FieldList.AddItem( FieldsToImport[idxSSN].N, FData.FieldByName(FieldsToImport[idxSSN].N).AsString );

    AddToUpdate(FClPerson, FieldList, idxAddress1);
    AddToUpdate(FClPerson, FieldList, idxCity);
    AddToUpdate(FClPerson, FieldList, idxState);
    AddToUpdate(FClPerson, FieldList, idxZipCode);
  end;

  Result := ApplyRecordChange( FieldList, 'CL_PERSON', ChangeType, Result );
end;

function TEEImport.LocateDBDT(const CDivNbr, CBrNbr,
  CDepNbr: string): boolean;
begin
  Result := False;
  if (FDBDT <> nil) and FDBDT.Active then
  begin
    if (CDivNbr <> '') and (CBrNbr <> '') and (CDepNbr <> '') then
      Result := FDBDT.Locate('Custom_Division_Number;Custom_Branch_Number;Custom_Department_Number', VarArrayOf([CDivNbr,CBrNbr,CDepNbr]), [])
    else if (CDivNbr <> '') and (CBrNbr <> '') then
      Result := FDBDT.Locate('Custom_Division_Number;Custom_Branch_Number', VarArrayOf([CDivNbr,CBrNbr]), [])
    else if (CDivNbr <> '') then
      Result := FDBDT.Locate('Custom_Division_Number', CDivNbr, []);
  end;    
end;

procedure TEeImport.CreateEvoEE;
var
  ClPersonNbr, EeNbr, EeStatesNbr, CoStatesNbr, RateNbr: integer;
  FieldList: IRpcStruct;
begin
  if not LocateSyStateMaritalStatusNbr then
    raise Exception.Create('There is no State Marital Status "' + FStateMaritalStatus + '" set up for the state "' + FData.FieldByName(FieldsToImport[idxState].N).AsString + '"');

  FieldList := TRpcStruct.Create;

  FieldList.AddItem('EE_NBR', -1);
  FieldList.AddItem('CO_NBR', FCoDef.CoNbr );
  FieldList.AddItemDateTime('CURRENT_HIRE_DATE', FData.FieldByName(FieldsToImport[idxHireDate].N).AsDateTime );
  FieldList.AddItem('CUSTOM_EMPLOYEE_NUMBER', FData.FieldByName(FieldsToImport[idxEeCode].N).AsString );

  if not LocateDBDT( FData.FieldByName( FieldsToImport[idxDivision].N ).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString, FData.FieldByName(FieldsToImport[idxDepartment].N).AsString ) then
  begin
    FLogger.LogWarningFmt('Division: (%s), Branch: (%s), Department: (%s) is not found', [ FData.FieldByName(FieldsToImport[idxDivision].N).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString, FData.FieldByName(FieldsToImport[idxDepartment].N).AsString ] );
    if not LocateDBDT( FData.FieldByName( FieldsToImport[idxDivision].N ).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString, '' ) then
    begin
      FLogger.LogWarningFmt('Division: (%s), Branch: (%s) is not found', [ FData.FieldByName(FieldsToImport[idxDivision].N).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString ] );
      if not LocateDBDT( FData.FieldByName( FieldsToImport[idxDivision].N ).AsString, '', '' ) then
        FLogger.LogWarningFmt('Division: (%s) is not found', [ FData.FieldByName(FieldsToImport[idxDivision].N).AsString ] )
      else
        FieldList.AddItem('CO_DIVISION_NBR', FDBDT.FieldByName('CO_DIVISION_NBR').AsInteger );
    end
    else begin
      FieldList.AddItem('CO_DIVISION_NBR', FDBDT.FieldByName('CO_DIVISION_NBR').AsInteger );
      FieldList.AddItem('CO_BRANCH_NBR', FDBDT.FieldByName('CO_BRANCH_NBR').AsInteger );
    end;
  end
  else begin
    FieldList.AddItem('CO_DIVISION_NBR', FDBDT.FieldByName('CO_DIVISION_NBR').AsInteger );
    FieldList.AddItem('CO_BRANCH_NBR', FDBDT.FieldByName('CO_BRANCH_NBR').AsInteger );
    FieldList.AddItem('CO_DEPARTMENT_NBR', FDBDT.FieldByName('CO_DEPARTMENT_NBR').AsInteger );
  end;

  FieldList.AddItem('PAY_FREQUENCY', FData.FieldByName(FieldsToImport[idxPayFrequency].N).AsString );
  FieldList.AddItem('CURRENT_TERMINATION_CODE', FData.FieldByName(FieldsToImport[idxTermCode].N).AsString );
  FieldList.AddItem('FEDERAL_MARITAL_STATUS', FData.FieldByName(FieldsToImport[idxFederalMaritalStatus].N).AsString );
  FieldList.AddItem('NUMBER_OF_DEPENDENTS', FData.FieldByName(FieldsToImport[idxNumberOfDependents].N).AsInteger );
  FieldList.AddItem('EXEMPT_EMPLOYER_OASDI', FData.FieldByName(FieldsToImport[idxErOASDIExempt].N).AsString );

  ClPersonNbr := UpdateClPerson(-1);

  FieldList.AddItem('CL_PERSON_NBR', ClPersonNbr );

  EeNbr := ApplyRecordChange( FieldList, 'EE', 'I', -1 );


  CoStatesNbr := GetCoStateNbr(FData.FieldByName(FieldsToImport[idxState].N).AsString);

  FieldList.Clear;
  FieldList.AddItem('EE_STATES_NBR', -1);
  FieldList.AddItem('EE_NBR', EeNbr );
  FieldList.AddItem('SUI_APPLY_CO_STATES_NBR', CoStatesNbr );
  FieldList.AddItem('SDI_APPLY_CO_STATES_NBR', CoStatesNbr );
  FieldList.AddItem('CO_STATES_NBR', CoStatesNbr );
  FieldList.AddItem('STATE_NUMBER_WITHHOLDING_ALLOW', FData.FieldByName(FieldsToImport[idxStateDep].N).AsInteger );
  FieldList.AddItem('EE_TAX_CODE', FData.FieldByName(FieldsToImport[idxState].N).AsString );
  FieldList.AddItem('SY_STATE_MARITAL_STATUS_NBR', FSyStateMaritalStatusNbr );
  FieldList.AddItem('STATE_MARITAL_STATUS', FStateMaritalStatus );
  FieldList.AddItem('IMPORTED_MARITAL_STATUS', FData.FieldByName(FieldsToImport[idxFederalMaritalStatus].N).AsString );
  FieldList.AddItem('STATE_EXEMPT_EXCLUDE', 'I' );
  FieldList.AddItem('EE_SDI_EXEMPT_EXCLUDE', 'I' );
  FieldList.AddItem('EE_SUI_EXEMPT_EXCLUDE', 'I' );
  FieldList.AddItem('ER_SDI_EXEMPT_EXCLUDE', 'I' );
  FieldList.AddItem('ER_SUI_EXEMPT_EXCLUDE', 'I' );

  EeStatesNbr := ApplyRecordChange( FieldList, 'EE_STATES', 'I', -1 );

  FieldList.Clear;
  FieldList.AddItem('HOME_TAX_EE_STATES_NBR', EeStatesNbr );
  EeNbr := ApplyRecordChange( FieldList, 'EE', 'U', EeNbr );

  if not TryStrToInt( Trim(FData.FieldByName( FieldsToImport[idxDivision].N ).AsString), RateNbr ) then
    RateNbr := 1;

  FieldList.Clear;
  FieldList.AddItem('EE_RATES_NBR', -1);
  FieldList.AddItem('EE_NBR', EeNbr);
  FieldList.AddItem('RATE_NUMBER', RateNbr);
  FieldList.AddItem('RATE_AMOUNT', FData.FieldByName(FieldsToImport[idxRateAmount].N).AsCurrency);
  FieldList.AddItem('PRIMARY_RATE', 'Y');
  ApplyRecordChange( FieldList, 'EE_RATES', 'I', -1 );
end;

procedure TEeImport.UpdateEvoEE;
var
  iEeStatesNbr, iEeNbr, iCoStateNbr, iRateNbr: integer;
  FieldList: IRpcStruct;
begin
  if not LocateSyStateMaritalStatusNbr then
    raise Exception.Create('There is no State Marital Status "' + FStateMaritalStatus + '" set up for the state "' + FData.FieldByName(FieldsToImport[idxCompanyHomeState].N).AsString + '"');

  iCoStateNbr := GetCoStateNbr(FData.FieldByName(FieldsToImport[idxState].N).AsString);

  UpdateClPerson( FEEData.FieldByName('CL_PERSON_NBR').AsInteger );
  iEeNbr := FEEData.FieldByName('EE_NBR').AsInteger;

  FieldList := TRpcStruct.Create;

  FieldList.AddItem( 'SUI_APPLY_CO_STATES_NBR', iCoStateNbr );
  Fieldlist.AddItem( 'SDI_APPLY_CO_STATES_NBR', iCoStateNbr );
  Fieldlist.AddItem( 'STATE_NUMBER_WITHHOLDING_ALLOW', FData.FieldByName(FieldsToImport[idxStateDep].N).AsInteger );
  Fieldlist.AddItem( 'EE_TAX_CODE', FData.FieldByName(FieldsToImport[idxState].N).AsString );
  Fieldlist.AddItem( 'SY_STATE_MARITAL_STATUS_NBR', FSyStateMaritalStatusNbr );
  Fieldlist.AddItem( 'STATE_MARITAL_STATUS', FStateMaritalStatus );
  FieldList.AddItem('IMPORTED_MARITAL_STATUS', FData.FieldByName(FieldsToImport[idxFederalMaritalStatus].N).AsString );

  if FEEStates.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([iEeNbr, iCoStateNbr]), []) then
  begin
    iEeStatesNbr := FEEStates.FieldByName('EE_STATES_NBR').AsInteger;
    // If Home State is found in Ee States
    // update existing Ee State and set it as home state (EE.HOME_TAX_EE_STATES_NBR)

    ApplyRecordChange( FieldList, 'EE_STATES', 'U', iEeStatesNbr );
  end
  else begin
    // If Home State is not found in Ee States for this EE
    // create a new one and set it as Home state (EE.HOME_TAX_EE_STATES_NBR)
    Fieldlist.AddItem( 'EE_STATES_NBR', -1 );
    Fieldlist.AddItem( 'EE_NBR', iEeNbr );
    Fieldlist.AddItem( 'CO_STATES_NBR', iCoStateNbr );

    iEeStatesNbr := ApplyRecordChange( FieldList, 'EE_STATES', 'I', -1 );
  end;


  FieldList.Clear;
  if not TryStrToInt( Trim(FData.FieldByName( FieldsToImport[idxDivision].N ).AsString), iRateNbr ) then
    iRateNbr := 1;

  if FEERates.Locate('EE_NBR;Rate_Number', VarArrayOf([iEeNbr, iRateNbr]), []) then
  begin
    AddToUpdate(FEERates, FieldList, idxRateAmount);
    ApplyRecordChange( FieldList, 'EE_RATES', 'U', FEERates.FieldByName('EE_RATES_NBR').AsInteger );
  end
  else begin
    FieldList.AddItem( 'EE_RATES_NBR', -1 );
    FieldList.AddItem( 'EE_NBR', iEeNbr );
    FieldList.AddItem( 'RATE_NUMBER', iRateNbr );
    FieldList.AddItem( 'PRIMARY_RATE', 'Y' );
    FieldList.AddItem( 'RATE_AMOUNT', FData.FieldByName(FieldsToImport[idxRateAmount].N).AsCurrency );
    ApplyRecordChange( FieldList, 'EE_RATES', 'I', -1 );
  end;

  FieldList.Clear;
  AddToUpdate(FEEData, FieldList, idxHireDate);

  if not LocateDBDT( FData.FieldByName( FieldsToImport[idxDivision].N ).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString, FData.FieldByName(FieldsToImport[idxDepartment].N).AsString ) then
  begin
    FLogger.LogWarningFmt('Division: (%s), Branch: (%s), Department: (%s) is not found', [ FData.FieldByName(FieldsToImport[idxDivision].N).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString, FData.FieldByName(FieldsToImport[idxDepartment].N).AsString ] );
    if not LocateDBDT( FData.FieldByName( FieldsToImport[idxDivision].N ).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString, '' ) then
    begin
      FLogger.LogWarningFmt('Division: (%s), Branch: (%s) is not found', [ FData.FieldByName(FieldsToImport[idxDivision].N).AsString, FData.FieldByName(FieldsToImport[idxBranch].N).AsString ] );
      if not LocateDBDT( FData.FieldByName( FieldsToImport[idxDivision].N ).AsString, '', '' ) then
        FLogger.LogWarningFmt('Division: (%s) is not found', [ FData.FieldByName(FieldsToImport[idxDivision].N).AsString ] )
      else
        FieldList.AddItem('CO_DIVISION_NBR', FDBDT.FieldByName('CO_DIVISION_NBR').AsInteger );
    end
    else begin
      FieldList.AddItem('CO_DIVISION_NBR', FDBDT.FieldByName('CO_DIVISION_NBR').AsInteger );
      FieldList.AddItem('CO_BRANCH_NBR', FDBDT.FieldByName('CO_BRANCH_NBR').AsInteger );
    end;
  end
  else begin
    FieldList.AddItem('CO_DIVISION_NBR', FDBDT.FieldByName('CO_DIVISION_NBR').AsInteger );
    FieldList.AddItem('CO_BRANCH_NBR', FDBDT.FieldByName('CO_BRANCH_NBR').AsInteger );
    FieldList.AddItem('CO_DEPARTMENT_NBR', FDBDT.FieldByName('CO_DEPARTMENT_NBR').AsInteger );
  end;

  AddToUpdate(FEEData, FieldList, idxPayFrequency);
  AddToUpdate(FEEData, FieldList, idxTermCode);
  AddToUpdate(FEEData, FieldList, idxFederalMaritalStatus);
  AddToUpdate(FEEData, FieldList, idxNumberOfDependents);
  AddToUpdate(FEEData, FieldList, idxErOASDIExempt);

  FieldList.AddItem( 'HOME_TAX_EE_STATES_NBR', iEeStatesNbr );

  ApplyRecordChange( FieldList, 'EE', 'U', iEeNbr );
end;


function TEEImport.LocateSyStateMaritalStatusNbr: boolean;
begin
  FStateMaritalStatus := FData.FieldByName(FieldsToImport[idxStateMaritalStatus].N).AsString;
  FSyStateMaritalStatusNbr := ConvertNull( FEvoData.DS['SY_STATE_MARITAL_STATUS'].Lookup('STATE;STATUS_TYPE', VarArrayOf([FData.FieldByName(FieldsToImport[idxCompanyHomeState].N).AsString, FStateMaritalStatus]), 'SY_STATE_MARITAL_STAT_NBR'), 0);
  Result := FSyStateMaritalStatusNbr <> 0; 
end;

procedure TEEImport.AddToUpdate(aDS: TkbmCustomMemTable; aMap: IRpcStruct;
  aFieldIndex: integer);
begin
  if (FieldsToImport[aFieldIndex].T = ftString) and
    (aDS.FieldByName(FieldsToImport[aFieldIndex].N).AsString <> FData.FieldByName(FieldsToImport[aFieldIndex].N).AsString) then
    aMap.AddItem( FieldsToImport[aFieldIndex].N, FData.FieldByName(FieldsToImport[aFieldIndex].N).AsString )
  else if (FieldsToImport[aFieldIndex].T = ftInteger) and
    (aDS.FieldByName(FieldsToImport[aFieldIndex].N).AsInteger <> FData.FieldByName(FieldsToImport[aFieldIndex].N).AsInteger) then
    aMap.AddItem( FieldsToImport[aFieldIndex].N, FData.FieldByName(FieldsToImport[aFieldIndex].N).AsInteger )
  else if (FieldsToImport[aFieldIndex].T = ftCurrency) and
    (aDS.FieldByName(FieldsToImport[aFieldIndex].N).AsFloat <> FData.FieldByName(FieldsToImport[aFieldIndex].N).AsFloat) then
    aMap.AddItem( FieldsToImport[aFieldIndex].N, FData.FieldByName(FieldsToImport[aFieldIndex].N).AsFloat )
  else if (FieldsToImport[aFieldIndex].T = ftDatetime) and
    (aDS.FieldByName(FieldsToImport[aFieldIndex].N).AsDatetime <> FData.FieldByName(FieldsToImport[aFieldIndex].N).AsDateTime) then
    aMap.AddItemDateTime( FieldsToImport[aFieldIndex].N, FData.FieldByName(FieldsToImport[aFieldIndex].N).AsDateTime );
end;

function TEEImport.ApplyRecordChange(aFields: IRpcStruct; const aTableName,
  aType: string; aKey: integer): integer;
var
  Changes: IRpcArray;
  RowChange, EvoResult: IRpcStruct;
begin
  Result := aKey;
  
  Changes := TRpcArray.Create;
  RowChange := TRpcStruct.Create;
  EvoResult := TRpcStruct.Create;

  RowChange.AddItem('T', aType);
  RowChange.AddItem('D', aTableName);
  if aType = 'U' then
    RowChange.AddItem('K', aKey);


  if aFields.Count > 0 then
  begin
    if aType = 'I' then
    begin
      if aTableName = 'EE' then
        FillEEDefaults( aFields )
      else if aTableName = 'CL_PERSON' then
        FillClPersonDefaults( aFields )
      else if aTableName = 'EE_STATES' then
        FillEEStatesDefaults( aFields );
    end;    

    RowChange.AddItem('F', aFields);

    Changes.AddItem( RowChange );
    EvoResult := FConn.applyDataChangePacket( Changes );

    if Result = -1 then
      if EvoResult.KeyExists('-1') then
        if not TryStrToInt(EvoResult.Keys['-1'].AsString, Result) then
          raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
  end;
end;

procedure TEEImport.FillEEDefaults(aFields: IRpcStruct);
begin
  // set default EE fields
  aFields.AddItem('NEW_HIRE_REPORT_SENT', 'P' );
  aFields.AddItem('POSITION_STATUS', 'N' );
  aFields.AddItem('TIPPED_DIRECTLY', 'N' );
  aFields.AddItem('DISTRIBUTE_TAXES', 'B' );
  aFields.AddItem('FLSA_EXEMPT', 'N' );
  aFields.AddItem('W2_TYPE', 'F' );
  aFields.AddItem('W2_PENSION', 'N' );
  aFields.AddItem('W2_DEFERRED_COMP', 'N' );
  aFields.AddItem('W2_DECEASED', 'N' );
  aFields.AddItem('W2_STATUTORY_EMPLOYEE', 'N' );
  aFields.AddItem('W2_LEGAL_REP', 'N' );
  aFields.AddItem('EXEMPT_EMPLOYEE_OASDI', 'N' );
  aFields.AddItem('EXEMPT_EMPLOYEE_MEDICARE', 'N' );
  aFields.AddItem('EXEMPT_EMPLOYER_OASDI', 'N' );
  aFields.AddItem('EXEMPT_EMPLOYER_MEDICARE', 'N' );
  aFields.AddItem('EXEMPT_EMPLOYER_FUI', 'N' );
  aFields.AddItem('OVERRIDE_FED_TAX_TYPE', 'N' );
  aFields.AddItem('BASE_RETURNS_ON_THIS_EE', 'N' );
  aFields.AddItem('COMPANY_OR_INDIVIDUAL_NAME', 'I' );
  aFields.AddItem('TAX_AMT_DETERMINED_1099R', 'N' );
  aFields.AddItem('TOTAL_DISTRIBUTION_1099R', 'N' );
  aFields.AddItem('PENSION_PLAN_1099R', 'N' );
  aFields.AddItem('MAKEUP_FICA_ON_CLEANUP_PR', 'N' );
  aFields.AddItem('GENERATE_SECOND_CHECK', 'N' );
  aFields.AddItem('HIGHLY_COMPENSATED', 'N' );
  aFields.AddItem('CORPORATE_OFFICER', 'N' );
  aFields.AddItem('ELIGIBLE_FOR_REHIRE', 'N' );
  aFields.AddItem('SELFSERVE_ENABLED', 'N' );
  aFields.AddItem('WC_WAGE_LIMIT_FREQUENCY', 'P' );
  aFields.AddItem('HEALTHCARE_COVERAGE', 'N' );
  aFields.AddItem('EE_ENABLED', 'Y' );
  aFields.AddItem('AUTO_UPDATE_RATES', 'Y' );
  aFields.AddItem('PRINT_VOUCHER', 'Y' );
  aFields.AddItem('BENEFITS_ENABLED', 'Y' );
  aFields.AddItem('TIME_OFF_ENABLED', 'Y' );
  aFields.AddItem('EXISTING_PATIENT', 'N' );
  aFields.AddItem('DEPENDENT_BENEFITS_AVAILABLE', 'N' );
  aFields.AddItem('LAST_QUAL_BENEFIT_EVENT', 'Z' );
  aFields.AddItem('W2_FORM_ON_FILE', 'N' );
  aFields.AddItem('W2', 'A' );

  aFields.AddItem('EXEMPT_EXCLUDE_EE_FED', 'I' );
  aFields.AddItem('GOV_GARNISH_PRIOR_CHILD_SUPPT', 'N');
  aFields.AddItem('EIC', 'N');
  aFields.AddItem('NEXT_PAY_FREQUENCY', 'W' ); //Weekly
  aFields.AddItem('ACA_STATUS', 'N' ); //N/A

  if (Copy(FConn.EvoVersion, 1, 2) >= '16') then // New EE Fields in Quechee Release
  begin
    if (Copy(FConn.EvoVersion, 4, 2) >= '24') then
    begin
      aFields.AddItem('ACA_POLICY_ORIGIN', 'B');
      aFields.AddItem('ENABLE_ANALYTICS', 'Y');
      aFields.AddItem('BENEFITS_ELIGIBLE', 'N');
    end;

    if (Copy(FConn.EvoVersion, 4, 2) >= '35') then
    begin
      aFields.AddItem('DIRECT_DEPOSIT_ENABLED', 'N' );
      aFields.AddItem('ACA_FORM_ON_FILE', 'N');
      aFields.AddItem('ACA_TYPE', 'N');
      aFields.AddItem('BASE_ACA_ON_THIS_EE', 'Y');
      aFields.AddItem('COMMENSURATE_WAGE', 'N');
    end;
  end;
end;

procedure TEEImport.FillClPersonDefaults(aFields: IRpcStruct);
begin
  // set default CL_PERSON fields
  aFields.AddItem('EIN_OR_SOCIAL_SECURITY_NUMBER', 'S' );
  aFields.AddItem('GENDER', 'M' );
  aFields.AddItem('ETHNICITY', 'P' );
  aFields.AddItem('SMOKER', 'N' );
  aFields.AddItem('VETERAN', 'A' );
  aFields.AddItem('VIETNAM_VETERAN', 'A' );
  aFields.AddItem('DISABLED_VETERAN', 'A' );
  aFields.AddItem('MILITARY_RESERVE', 'A' );
  aFields.AddItem('I9_ON_FILE', 'N' );
  aFields.AddItem('VISA_TYPE', 'N' );
  aFields.AddItem('RELIABLE_CAR', 'Y' );
  aFields.AddItem('SERVICE_MEDAL_VETERAN', 'A' );
  aFields.AddItem('OTHER_PROTECTED_VETERAN', 'A' );
  aFields.AddItem('NATIVE_LANGUAGE', 'E' );
end;

procedure TEEImport.FillEEStatesDefaults(aFields: IRpcStruct);
begin
  aFields.AddItem('STATE_EXEMPT_EXCLUDE', 'E' );
  aFields.AddItem('OVERRIDE_STATE_TAX_TYPE', 'N' );
  aFields.AddItem('EE_SDI_EXEMPT_EXCLUDE', 'E' );
  aFields.AddItem('ER_SDI_EXEMPT_EXCLUDE', 'E' );
  aFields.AddItem('EE_SUI_EXEMPT_EXCLUDE', 'E' );
  aFields.AddItem('ER_SUI_EXEMPT_EXCLUDE', 'E' );
  aFields.AddItem('RECIPROCAL_METHOD', 'N' );
  aFields.AddItem('CALCULATE_TAXABLE_WAGES_1099', 'N' );
  aFields.AddItem('SALARY_TYPE', 'N' );
end;

function TEEImport.RunQuery(const aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FConn.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

end.
