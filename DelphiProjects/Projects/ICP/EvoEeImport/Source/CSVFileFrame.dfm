object CSVFileFrm: TCSVFileFrm
  Left = 0
  Top = 0
  Width = 673
  Height = 352
  TabOrder = 0
  inline CSVFileFrame: TFileOpenLeftBtnFrm
    Left = 0
    Top = 0
    Width = 673
    Height = 45
    Align = alTop
    TabOrder = 0
    inherited edtFile: TEdit
      Width = 546
    end
    inherited ActionList1: TActionList
      inherited FileOpen1: TFileOpen
        Dialog.Filter = 'csv-files (*.csv)|*.csv'
        Hint = 'Open|Opens an csv-file'
      end
    end
  end
  object dbgData: TReDBGrid
    Left = 0
    Top = 45
    Width = 673
    Height = 307
    DisableThemesInTitle = False
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsCSVData
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
  end
  object dsCSVData: TDataSource
    Left = 112
    Top = 72
  end
end
