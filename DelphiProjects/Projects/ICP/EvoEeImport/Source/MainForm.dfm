inherited MainFm: TMainFm
  Left = 440
  Top = 127
  Caption = 'MainFm'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabSheet3: TTabSheet
      object btnConnect: TBitBtn
        Left = 268
        Top = 144
        Width = 121
        Height = 25
        Action = ConnectToEvo
        Caption = 'Connect to Evo'
        TabOrder = 1
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Import from file'
      object pnlButtons: TPanel
        Left = 0
        Top = 428
        Width = 784
        Height = 41
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object btnImport: TBitBtn
          Left = 7
          Top = 8
          Width = 114
          Height = 25
          Action = ImportEmployees
          Caption = 'Import Employees'
          TabOrder = 0
        end
      end
      inline CSVFile: TCSVFileFrm
        Left = 0
        Top = 0
        Width = 784
        Height = 428
        Align = alClient
        TabOrder = 1
        inherited CSVFileFrame: TFileOpenLeftBtnFrm
          Width = 784
          inherited edtFile: TEdit
            Width = 657
          end
        end
        inherited dbgData: TReDBGrid
          Width = 784
          Height = 383
        end
      end
    end
  end
  object Actions: TActionList
    Left = 476
    Top = 48
    object ConnectToEvo: TAction
      Caption = 'Connect to Evo'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object ImportEmployees: TAction
      Caption = 'Import Employees'
      OnExecute = ImportEmployeesExecute
      OnUpdate = ImportEmployeesUpdate
    end
  end
end
