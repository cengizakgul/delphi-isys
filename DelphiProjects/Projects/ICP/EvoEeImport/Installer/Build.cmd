@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoEeImport.msi 
"%pWiXDir%\candle.exe" .\EvoEeImport.wxs -wx -out ..\..\..\..\..\Tmp\EvoEeImport.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoEeImport.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoEeImport.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoEeImport.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoEeImport%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoEeImport%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoEeImport.msi EvoEeImport%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
