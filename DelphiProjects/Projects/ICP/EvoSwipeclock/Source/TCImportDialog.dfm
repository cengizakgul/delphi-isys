inherited TCImportDlg: TTCImportDlg
  Width = 857
  Height = 307
  inline TCImportOptionsFrame: TTCImportOptionsFrm
    Left = 0
    Top = 0
    Width = 857
    Height = 307
    Align = alClient
    TabOrder = 0
    inherited Bevel4: TBevel
      Height = 307
    end
    inherited EEFilterFrame: TSwipeClockEEFilterFrm
      Width = 335
      Height = 307
      inherited gbEmployeeFilter: TGroupBox
        Width = 335
        Height = 307
        inherited Bevel1: TBevel
          Width = 331
        end
        inherited pnlRadioButtons: TPanel
          Width = 331
        end
        inherited ChoiceFrame: TMultipleChoiceFrm
          Width = 331
          Height = 257
          inherited pnlAvailable: TPanel
            Height = 257
            inherited lbAvailable: TListBox
              Height = 232
            end
          end
          inherited pnlButtons: TPanel
            Height = 257
          end
          inherited pnlSelected: TPanel
            Width = 106
            Height = 257
            inherited pnlSelectedHeader: TPanel
              Width = 106
            end
            inherited lbSelected: TListBox
              Width = 106
              Height = 232
            end
          end
        end
      end
    end
    inherited Panel1: TPanel
      Height = 307
    end
  end
end
