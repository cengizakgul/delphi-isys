inherited EEExportDlg: TEEExportDlg
  Width = 829
  Height = 686
  inline EEExportOptionsFrame: TEEExportOptionsFrm
    Left = 0
    Top = 630
    Width = 829
    Height = 56
    Align = alBottom
    TabOrder = 0
  end
  inline EEFilterFrame: TSwipeclockEEExportFilterFrm
    Left = 0
    Top = 0
    Width = 829
    Height = 630
    Align = alClient
    TabOrder = 1
    inherited GroupBox1: TGroupBox
      Width = 829
      Height = 630
      inherited Panel1: TPanel
        Height = 613
        inherited EEPositionStatusFilterFrame: TCodesFilterFrm
          Height = 551
          inherited GroupBox1: TGroupBox
            Height = 551
            inherited lbStatuses: TCheckListBox
              Height = 508
            end
          end
        end
        inherited EESalaryFilterFrame: TEESalaryFilterFrm
          Top = 551
        end
      end
      inherited DBDTFilterFrame: TDBDTFilterFrm
        Width = 586
        Height = 613
        inherited GroupBox1: TGroupBox
          Width = 586
          Height = 613
          inherited Panel1: TPanel
            Width = 582
          end
          inherited dgDBDT: TreDBCheckGrid
            Width = 582
            Height = 562
          end
        end
      end
    end
  end
end
