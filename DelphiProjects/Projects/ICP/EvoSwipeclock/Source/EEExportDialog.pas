unit EEExportDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, EEExportOptionsFrame, OptionsBaseFrame,
  EEFilterFrame, SwipeclockEEExportFilterFrame;

type
  TEEExportDlg = class(TDialogBase)
    EEExportOptionsFrame:TEEExportOptionsFrm;
    EEFilterFrame: TSwipeclockEEExportFilterFrm;
  public  
    function CheckBeforeClose: boolean; override;
  end;


implementation

{$R *.dfm}

{ TEEExportDlg }

function TEEExportDlg.CheckBeforeClose: boolean;
begin
  EeFilterFrame.Check;
  Result := true;
end;

end.
