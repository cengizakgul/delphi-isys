unit SwipeclockNumericMappingFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, db, swipeclockdecl, kbmMemTable, common, DBClient,
  StdCtrls, gdyClasses, wwdblook, Grids, Wwdbigrd, Wwdbgrid, dbcomp;

type
  TSwipeclockNumericMappingFrm = class(TFrame)
    dsrcEDCodes: TDataSource;
    dsrcCO_E_D_CODES: TDataSource;
    gbNumericMapping: TGroupBox;
    dgNM: TReDBGrid;
    dbcbED: TwwDBLookupCombo;
  private
    FNumericVars: IStr;
    cdEDCODES: TClientDataSet;
    function GetNumericMapping: TSwipeClockNumericMapping;
    procedure SetNumericMapping(nm: TSwipeClockNumericMapping);
  public
    destructor Destroy; override;
    property NumericMapping: TSwipeClockNumericMapping read GetNumericMapping write SetNumericMapping;
    procedure Init(CO_E_D_CODESClone: TkbmMemTable {takes ownership!}; numericVars: IStr);
    procedure Clear;
  end;

implementation

{$R *.dfm}

function EmptyNumericMapping: TSwipeClockNumericMapping;
begin
  Result.SerializedVarToEDCode := '';
end;

{ TSwipeclockNumericMappingFrm }


destructor TSwipeclockNumericMappingFrm.Destroy;
begin
  if dsrcCO_E_D_CODES.DataSet <> nil then
  begin
    dsrcCO_E_D_CODES.DataSet.Free;
    dsrcCO_E_D_CODES.DataSet := nil;
  end;
  inherited;
end;

procedure TSwipeclockNumericMappingFrm.Clear;
begin
  if (cdEDCODES <> nil) and (dsrcCO_E_D_CODES.DataSet <> nil) then
    NumericMapping := EmptyNumericMapping;
  if dsrcCO_E_D_CODES.DataSet <> nil then
  begin
    dsrcCO_E_D_CODES.DataSet.Free;
    dsrcCO_E_D_CODES.DataSet := nil;
  end;
end;

procedure TSwipeclockNumericMappingFrm.Init(CO_E_D_CODESClone: TkbmMemTable{takes ownership!}; numericVars: IStr);
begin
  FNumericVars := numericVars;

  Assert(dsrcCO_E_D_CODES.DataSet = nil);
  dsrcCO_E_D_CODES.DataSet := CO_E_D_CODESClone;

  FreeAndNil(cdEDCODES);
  cdEDCODES := TClientDataSet.Create(Self);

  with CreateStringField( cdEDCODES, 'VARIABLE', 'Variable', 100) do
  begin
    Visible := false;
  end;
  with CreateStringField( cdEDCODES, 'VARCAPTION', 'Prompt', 100) do
  begin
    DisplayWidth := 15;
  end;
  with CreateStringField( cdEDCODES, 'EDCODE', 'E/D code', CO_E_D_CODESClone.FieldByName('Custom_E_D_Code_Number').DataSize) do
  begin
    Visible := false;
  end;
  with CreateStringField( cdEDCODES, 'EDNAME', 'E/D', CO_E_D_CODESClone.FieldByName('CODE_AND_DESCRIPTION').DataSize) do
  begin
    FieldKind := fkLookup;
    LookupDataSet := CO_E_D_CODESClone;
    LookupKeyFields := 'Custom_E_D_Code_Number';
    LookupResultField := 'CODE_AND_DESCRIPTION';
    KeyFields := 'EDCODE';
    DisplayWidth := 15;
  end;
  cdEDCODES.CreateDataSet;
  dsrcEDCodes.DataSet := cdEDCODES;

  dbcbED.LookupTable := CO_E_D_CODESClone;
  dbcbED.LookupField := 'Custom_E_D_Code_Number';
  dbcbED.Selected.Clear;
  AddSelected(dbcbED.Selected, CO_E_D_CODESClone.FieldByName('CODE_AND_DESCRIPTION') );

  dgNM.SetControlType('EDNAME', fctCustom, 'dbcbED');

end;

function TSwipeclockNumericMappingFrm.GetNumericMapping: TSwipeClockNumericMapping;
var
  temp: IStr;
  bmk: TBookmark;
begin
  Assert(dsrcCO_E_D_CODES.DataSet <> nil);

  temp := EmptyIStr;
  cdEDCODES.DisableControls;
  try
    bmk := cdEDCODES.GetBookmark;
    try
      cdEDCODES.First;
      while not cdEDCODES.Eof do
      begin
        if not VarIsNull(cdEDCODES['EDCODE']) then
          temp.Add( cdEDCODES['VARIABLE'] + '=' + cdEDCODES['EDCODE']);
        cdEDCODES.Next;
      end;
    finally
      cdEDCODES.GotoBookmark(bmk);
      cdEDCODES.Freebookmark(bmk);
    end;
  finally
    cdEDCODES.EnableControls;
  end;
  Result.SerializedVarToEDCode := temp.AsTStrings.Text;
end;

procedure TSwipeclockNumericMappingFrm.SetNumericMapping(nm: TSwipeClockNumericMapping);
var
  temp: IStr;
  i: integer;
begin
  Assert( (cdEDCODES <> nil) and (dsrcCO_E_D_CODES.DataSet <> nil) );

  cdEDCODES.First;
  while not cdEDCODES.Eof do
    cdEDCODES.Delete;

  temp := SplitToLines(nm.SerializedVarToEDCode);
  for i := 0 to FNumericVars.Count-1 do
    Append(cdEDCODES, 'VARIABLE;VARCAPTION;EDCODE', [FNumericVars.AsTStrings.Names[i], FNumericVars.AsTStrings.Values[FNumericVars.AsTStrings.Names[i]], temp.AsTStrings.Values[FNumericVars.AsTStrings.Names[i]]]);
end;

end.
