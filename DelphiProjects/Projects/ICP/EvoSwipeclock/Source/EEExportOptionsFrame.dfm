inherited EEExportOptionsFrm: TEEExportOptionsFrm
  Width = 812
  Height = 60
  object cbExportSSN: TCheckBox
    Left = 8
    Top = 9
    Width = 97
    Height = 17
    Caption = 'Export SSN'
    TabOrder = 0
  end
  object cbExportRates: TCheckBox
    Left = 8
    Top = 31
    Width = 129
    Height = 17
    Caption = 'Export pay rates'
    TabOrder = 1
  end
  object cbExportDepartment: TCheckBox
    Left = 128
    Top = 31
    Width = 121
    Height = 17
    Caption = 'Export department'
    TabOrder = 2
  end
  object cbExportBranch: TCheckBox
    Left = 128
    Top = 9
    Width = 137
    Height = 17
    Caption = 'Export branch (location)'
    TabOrder = 3
  end
  object gbAllowClearing: TGroupBox
    Left = 280
    Top = 5
    Width = 521
    Height = 44
    Caption = 'Allow clearing of '
    TabOrder = 4
    object cbAllowClearingOfCardNumber1: TCheckBox
      Left = 16
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Card number 1'
      TabOrder = 0
    end
    object cbAllowClearingOfSupervisor: TCheckBox
      Left = 128
      Top = 16
      Width = 81
      Height = 17
      Caption = 'Supervisor'
      TabOrder = 1
    end
    object cbAllowClearingOfWebPassword: TCheckBox
      Left = 224
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Web password'
      TabOrder = 2
    end
    object cbAllowClearingOfWebLogin: TCheckBox
      Left = 344
      Top = 16
      Width = 161
      Height = 17
      Caption = 'Web login ID (card number 2)'
      TabOrder = 3
    end
  end
  object gbClockNumbers: TGroupBox
    Left = 280
    Top = 5
    Width = 521
    Height = 44
    Caption = 'Sync to TWP ClockNumbers'
    TabOrder = 5
    object chbTimeClockNumber: TCheckBox
      Left = 16
      Top = 16
      Width = 116
      Height = 17
      Caption = 'Time Clock Number'
      TabOrder = 0
    end
    object chbEeAddInfo: TCheckBox
      Left = 148
      Top = 16
      Width = 155
      Height = 17
      Caption = 'EE Additional Info Fields'
      TabOrder = 1
    end
    object chbEeCode: TCheckBox
      Left = 307
      Top = 16
      Width = 73
      Height = 17
      Caption = 'EE Code'
      TabOrder = 2
    end
  end
end
