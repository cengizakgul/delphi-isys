unit SwipeclockConnection;

interface

uses
  EmployeeWebServiceInterface, gdycommonlogger, bndGetEmployees,
  bndGetSiteCards, bndGetSites, tcconnectionbase, swipeclockdecl,
  timeclockimport, evodata, xmlintf, common, EmployeeWebInterface, api12;

type
  IXMLGetSites = bndGetSites.IXMLNewDataSet;
  IXMLGetEmployeesDataSet = bndGetEmployees.IXMLNewDataSet;
  IXMLGetSiteCards = bndGetSiteCards.IXMLResponseType;

  TSwipeClockConnection = class(TTCConnectionBase)
  public
    constructor Create(const param: TSwipeClockConnectionParam; logger: ICommonLogger);

    function GetSiteCards(period: TPayPeriod): IXMLGetSiteCards;
    function GetSites: IXMLGetSites;

    function GetEmployees: IXMLGetEmployeesDataSet;
    procedure RemoveEmployee(code: widestring; termDate: TDateTime);
    procedure AddEmployee(EEData: TEvoEEData; const options: TEEExportOptions; const ExtAppSettings: TExtAppSettings);
    procedure UpdateEmployee(swipeEE: IXMLEmployees; EEData: TEvoEEData; const options: TEEExportOptions); //consolidated
    procedure UpdateEmployeeFields(EEData: TEvoEEData; const options: TEEExportOptions); //not consolidated
    procedure UpdateEmployeeFields_api12(SendPass: boolean; EEData: TEvoEEData; const options: TEEExportOptions; AddIfNotFound: boolean); //not consolidated
    function This: TSwipeClockConnection;

    function GetSiteRuleSet: GetSiteRuleSetApiResult;
    function GetUnfinalizedPayroll(period: TPayPeriod): IXMLDocument;
  protected
    function CheckXML(doc: IXMLDocument): IXMLDocument; override;
    function ToXMLDoc(ws: widestring; name: string): IXMLDocument; override;
  private
    FParam: TSwipeClockConnectionParam;

    FAuthHeader: AuthHeader;
    FService: EmployeeWebServiceInterface.EmployeeWebServiceInterfaceSoap;
    FService2: EmployeeWebInterface.EmployeeWebInterfaceSoap;
    FService3: api12.API12Soap;
    FSession3: string;
    procedure CheclAPI12Result(res: api12.ApiResult; fn: string);
    procedure CheckAPI2Result(res: widestring; nm: string);

    procedure BeginConnection;
    procedure EndConnection;
    procedure BeginConnection2;
    procedure EndConnection2;
    procedure BeginConnection3;
    procedure EndConnection3;

    function GetAndCheckWebPassword(EEData: TEvoEEData): string;
  end;

function DateToSwipeClockDate(dt: TDateTime): string;
function SwipeClockDateToDate(s: string): TDateTime;

function GetCardNumber1(EEData: TEvoEEData): string;
function GetTWPClockNumbers(EEData: TEvoEEData; options: TEEExportOptions): string;

implementation


uses
  sysutils, gdyGlobalWaitIndicator, variants, InvokeRegistry, gdycommon,
  XSBuiltIns, gdyRedir, xmlDoc, Classes, DateUtils{, EvConsts};

const
  sCtxComponentSwipeclock = 'TimeWorks connection';

function GetCardNumber1(EEData: TEvoEEData): string;
begin
  Result := trim(VarToStr(EEData.EEs['TIME_CLOCK_NUMBER']));
  if Result = '' then
    Result := trim(VarToStr(EEData.EEs['CUSTOM_EMPLOYEE_NUMBER']));
end;

function GetTWPClockNumbers(EEData: TEvoEEData; options: TEEExportOptions): string;
var
  AddF: TEeAdditionalField;
  sl: TStringList;

  procedure AddTWPLogin(const fieldName: string);
  begin
    AddF := EEData.GetEEAdditionalFieldVal(fieldName);
    if AddF.IsSetup and (Trim(VarToStr(AddF.Value)) <> '') then
    begin
      if sl.IndexOf(Trim(VarToStr(AddF.Value))) < 0 then
        sl.Add( Trim(VarToStr(AddF.Value)) );
    end;
  end;
begin
  Result := '';
  sl := TStringList.Create;
  try
    if options.UseTimeClockNumber and (trim(VarToStr(EEData.EEs['TIME_CLOCK_NUMBER'])) <> '') then
      sl.Add( trim(VarToStr(EEData.EEs['TIME_CLOCK_NUMBER'])) );

    if options.UseEeAddInfo then
    begin
      AddTWPLogin( TWP_LOGIN );
      AddTWPLogin( TWP_LOGIN1 );
      AddTWPLogin( TWP_LOGIN2 );
    end;

    if options.UseEeCode then
    begin
      if sl.IndexOf( trim(VarToStr(EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'])) ) < 0 then
        sl.Add( trim(VarToStr(EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'])) );
    end;

    if sl.Count > 0 then
    begin
      sl.Sorted := True;
      Result := sl.CommaText;
    end;  
  finally
    FreeAndNil(sl);
  end
end;

function DateToSwipeClockDate(dt: TDateTime): string;
begin
  Result := FormatDateTime('yyyy/mm/dd', dt);
end;

function SwipeClockDateToDate(s: string): TDateTime;
var
  fs: TFormatSettings;
begin
//  GetLocaleFormatSettings( ,fs);
  fs.DateSeparator := '/';
  fs.ShortDateFormat := 'yyyy/mm/dd';
  Result := StrToDate(s, fs);
end;

{ TSwipeClockConnection }

function TSwipeClockConnection.CheckXML(doc: IXMLDocument): IXMLDocument;
var
	nbr: string;
	desc: string;
begin
  if doc.DocumentElement.NodeName = 'error' then
  begin
    if doc.DocumentElement.ChildNodes['number'] <> nil then
      nbr := doc.DocumentElement.ChildNodes['number'].NodeValue
    else
      nbr := '?';
    if doc.DocumentElement.ChildNodes['description'] <> nil then
      desc := doc.DocumentElement.ChildNodes['description'].NodeValue
    else
      desc := '?';
    raise Exception.CreateFmt('TimeWorks error (%s): %s', [nbr, desc]);
  end;
  Result := doc;
end;

constructor TSwipeClockConnection.Create(const param: TSwipeClockConnectionParam; logger: ICommonLogger);
begin
  inherited Create(logger);
  FParam := param;
end;

function TSwipeClockConnection.GetEmployees: IXMLGetEmployeesDataSet;
begin
  FLogger.LogEntry('TimeWorks: Get Employees');
  try
    try
      BeginConnection;
      try
        Result := bndGetEmployees.GetNewDataSet( ToXMLDoc(FService.GetEmployees, 'GetEmployees') );
        Result.OwnerDocument.Options := Result.OwnerDocument.Options + [doNodeAutoCreate]; //some nodes may be missing
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.BeginConnection;
begin
  FLogger.LogContextItem(sCtxComponent, sCtxComponentSwipeclock);

  FService := GetEmployeeWebServiceInterfaceSoap( true, 'https://www.swipeclock.com/scci/xml/EmployeeWebServiceInterface.asmx?WSDL');
  FLogger.LogDebug('got FService');
  SetHTTPTimeouts(FService, 30*60*1000);
  AttachLogger(FService);
  FLogger.LogDebug('attached logger');

  FAuthHeader := AuthHeader.Create;
  FAuthHeader.userName := FParam.UserName;
  FAuthHeader.password := FParam.Password;
  FAuthHeader.site := FParam.Site;
  (FService as ISOAPHeaders).send(FAuthHeader);
  FLogger.LogDebug('set auth');
end;

procedure TSwipeClockConnection.EndConnection;
begin
  try
    FService := nil;
  finally
    FreeAndNil(FAuthHeader);
  end;
end;

procedure TSwipeClockConnection.BeginConnection2;
begin
  FLogger.LogContextItem(sCtxComponent, sCtxComponentSwipeclock);

  FService2 := EmployeeWebInterface.GetEmployeeWebInterfaceSoap( false {wrong port number in the wsdl}, 'https://www.payrollservers.us/scci/xml/EmployeeWebInterface.asmx');
  SetHTTPTimeouts(FService2, 30*60*1000);
  AttachLogger(FService2);

  FAuthHeader := AuthHeader.Create;
  FAuthHeader.userName := FParam.UserName;
  FAuthHeader.password := FParam.Password;
  FAuthHeader.site := FParam.Site;
  (FService2 as ISOAPHeaders).send(FAuthHeader);
end;

procedure TSwipeClockConnection.EndConnection2;
begin
  try
    FService2 := nil;
  finally
    FreeAndNil(FAuthHeader);
  end;
end;

procedure TSwipeClockConnection.CheclAPI12Result(res: api12.ApiResult; fn: string);
var
  i, j: integer;
  FailureText: string;
begin
  if not res.Success then
  begin
    if res.Comments <> '' then
      raise Exception.CreateFmt('TimeWorks API %s failed: %s (%s).', [fn, res.FailureReason, res.Comments])
    else if (res is UpdateEmployeeFieldsApiResult) then
    begin
      FailureText := 'Failure Reason: ' + res.FailureReason;
      with (res as UpdateEmployeeFieldsApiResult) do
      for i := Low(ValidationFailuresList) to High(ValidationFailuresList) do
      begin
        FailureText := FailureText + #10#13;
        FailureText := FailureText + 'Failure ID: ' + ValidationFailuresList[i].Identifier;

        for j := Low(ValidationFailuresList[i].FieldValidationFailures) to High(ValidationFailuresList[i].FieldValidationFailures) do
          FailureText := FailureText + #10#13 + '  Field: ' + ValidationFailuresList[i].FieldValidationFailures[j].field + ', Message: ' + ValidationFailuresList[i].FieldValidationFailures[j].message;
      end;
      raise Exception.CreateFmt('TimeWorks API %s failed: %s.', [fn, FailureText]);
    end
    else
      raise Exception.CreateFmt('TimeWorks API %s failed: %s.', [fn, res.FailureReason]);
  end
  else
  begin
    if res.Comments <> '' then
      FLogger.LogEventFmt('TimeWorks API %s: %s', [fn, res.Comments]);
  end;
end;

procedure TSwipeClockConnection.BeginConnection3;
var
  res: CreateSessionResult;
begin
  FLogger.LogContextItem(sCtxComponent, sCtxComponentSwipeclock);
  FService3 := api12.GetAPI12Soap(false, 'https://payrollservers.us/pg/api12.asmx');
  SetHTTPTimeouts(FService3, 30*60*1000);
  AttachLogger(FService3);
  try
    FLogger.LogEntry('TimeWorks: CreateSessionSelectClient');
    try
      try
        res := FService3.CreateSessionSelectClient(FParam.UserName, FParam.Password, '', 'MC2ID', FParam.Site);
        try
          CheclAPI12Result(res, 'CreateSessionSelectClient');
          FSession3 := res.SessionID;
        finally
          FreeAndNil(res);
        end;
      except
        FLogger.PassthroughException;
      end;
    finally
      FLogger.LogExit;
    end;
  except
    EndConnection3;
    raise;
  end;
end;

procedure TSwipeClockConnection.EndConnection3;
begin
  FSession3 := '';
  FService3 := nil;
end;

function TSwipeClockConnection.GetSiteRuleSet: GetSiteRuleSetApiResult;
begin
  FLogger.LogEntry('TimeWorks: GetSiteRuleSet');
  try
    try
      BeginConnection3;
      try
        Result := FService3.GetSiteRuleSet(FSession3);
        CheclAPI12Result(Result, 'GetSiteRuleSet');
      finally
        EndConnection3;
      end;
    except
      Result := nil; //to make compiler happy
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockConnection.GetUnfinalizedPayroll(period: TPayPeriod): IXMLDocument;
var
  res: GetUnfinalizedPayrollResult;
  periodBegin, periodEnd: TXSDateTime;
begin
  FLogger.LogEntry('SwipeClock: GetUnfinalizedPayroll');
  try
    try
      BeginConnection3;
      try
        periodBegin := TXSDateTime.Create;
        periodEnd := TXSDateTime.Create;
        try
          periodBegin.AsUTCDateTime := period.PeriodBegin;
          periodEnd.AsUTCDateTime := period.PeriodEnd;
          FLogger.LogContextItem('Period begin', DateToStr(periodBegin.AsUTCDateTime));
          FLogger.LogContextItem('Period end', DateToStr(periodEnd.AsUTCDateTime));
          res := FService3.GetUnfinalizedPayroll(FSession3, periodBegin, periodEnd);
          try
            CheclAPI12Result(res, 'GetUnfinalizedPayroll');

            FLogger.LogDebug('GetUnfinalizedPayroll result', UTF8Encode(res.UnfinalizedPayrollDocument));
          {$ifndef FINAL_RELEASE}
            StringToFile( UTF8Encode(res.UnfinalizedPayrollDocument), Redirection.GetDirectory(sDumpDirAlias) + 'GetUnfinalizedPayroll.xml');
          {$endif}

            Result := LoadXMLData(res.UnfinalizedPayrollDocument);
            Result.Options := Result.Options - [doNodeAutoCreate] + [doNodeAutoIndent];
          finally
            FreeAndNil(res);
          end;
        finally
          FreeAndNil(periodBegin);
          FreeAndNil(periodEnd);
        end;
      finally
        EndConnection3;
      end;
    except
      Result := nil; //to make compiler happy
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockConnection.GetSiteCards(period: TPayPeriod): IXMLGetSiteCards;
var
	periodBegin, periodEnd, res: WideString;
	doc: IXMLDocument;
begin
  FLogger.LogEntry('TimeWorks: Get Site Cards');
  try
    try
      BeginConnection;
      try
        FLogger.LogDebug('about to call GetSiteCards');
        Assert(FService <> nil);
        periodBegin := DateToSwipeClockDate(period.PeriodBegin);
        FLogger.LogContextItem('Period begin', UTF8Encode(periodBegin));
        periodEnd := DateToSwipeClockDate(period.PeriodEnd);
        FLogger.LogContextItem('Period end', UTF8Encode(periodEnd));
        FLogger.LogDebug('now calling GetSiteCards');
        res := FService.GetSiteCards(periodBegin, periodEnd);
        FLogger.LogDebug('now calling ToXMLDoc');
        doc := ToXMLDoc(res,'GetSiteCards');
        FLogger.LogDebug('now calling Getresponce');
        Result := bndGetSiteCards.Getresponse(doc);
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.RemoveEmployee(code: widestring; termDate: TDateTime);
begin
  FLogger.LogEntry('TimeWorks: Remove Employee');
  try
    try
      FLogger.LogContextItem(sCtxEECode, code);
      FLogger.LogContextItem('Termination date', DateToSwipeClockDate(termDate));
      BeginConnection;
      try
        FService.RemoveEmployee(code, DateToSwipeClockDate(termDate));
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

type
  TPayRates = class
  public
    RateDef: TXSDecimal;
    Rate1: TXSDecimal;
    Rate2: TXSDecimal;
    Rate3: TXSDecimal;

    constructor Create(rates: TRateArray); overload;
    constructor Create(r0, r1, r2, r3: string); overload;
    destructor Destroy; override;
  end;

constructor TPayRates.Create(rates: TRateArray);
begin
  RateDef := TXSDecimal.Create();
  Rate1 := TXSDecimal.Create();
  Rate2 := TXSDecimal.Create();
  Rate3 := TXSDecimal.Create();
  RateDef.DecimalString := FloatToStr( rates[0] );
  Rate1.DecimalString := FloatToStr( rates[1] );
  Rate2.DecimalString := FloatToStr( rates[2] );
  Rate3.DecimalString := FloatToStr( rates[3] );
end;

constructor TPayRates.Create(r0, r1, r2, r3: string);
begin
  RateDef := TXSDecimal.Create();
  Rate1 := TXSDecimal.Create();
  Rate2 := TXSDecimal.Create();
  Rate3 := TXSDecimal.Create();
  RateDef.DecimalString := r0;
  Rate1.DecimalString := r1;
  Rate2.DecimalString := r2;
  Rate3.DecimalString := r3;
end;

destructor TPayRates.Destroy;
begin
  FreeAndNil(RateDef);
  FreeAndNil(Rate1);
  FreeAndNil(Rate2);
  FreeAndNil(Rate3);
end;

function TSwipeClockConnection.GetAndCheckWebPassword(EEData: TEvoEEData): string;
begin
  Result := VarToStr(EEData.GetEEAdditionalField(SC_PASSWORD));
  if (Result <> '') and (Length(Result) <> 8) then
    FLogger.LogWarning('Web password must be at least 8 characters');
end;

procedure TSwipeClockConnection.AddEmployee(EEData: TEvoEEData; const options: TEEExportOptions; const ExtAppSettings: TExtAppSettings);
var
  rates: TPayRates;
  loc: string;
begin
  FLogger.LogEntry('TimeWorks: Add Employee');
  try
    try
      BeginConnection;
      try
        if options.ExportRates then
          rates := TPayRates.Create( EEData.GetEERatesArray )
        else
          rates := TPayRates.Create( '0.0', '0.0', '0.0', '0.0');
        if ExtAppSettings.ConsolidatedDatabase then
          loc := trim(EEData.Company.CUSTOM_COMPANY_NUMBER)
        else
          loc := IIF( options.ExportBranch, trim(EEData.EEs.FieldByName('CUSTOM_BRANCH_NUMBER').AsString), '');
        try
          FService.AddEmployee(
            trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), //Employee Code
            EEData.EEs.FieldByName('LAST_NAME').AsString,
            EEData.EEs.FieldByName('FIRST_NAME').AsString,
            EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString,
            '', //designation
            '', //title
            IIF( options.ExportSSN, trim(StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll])) , ''), //ssn
            IIF( options.ExportDepartment, trim(EEData.EEs.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString), ''), //dept
            loc, //location
            VarToStr(EEData.GetEEAdditionalField(SC_SUPERVISOR)), //supervisor
            DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime), //? //start date
            0, //autolunch, minute
            0, //autolunch after Hours
            rates.RateDef, //payRate0 (default)
            rates.Rate1, //payRate1
            rates.Rate2, //payRate2
            rates.Rate3, //payRate3
            GetCardNumber1(EEData), //card num1
            VarToStr(EEData.GetEEAdditionalField(SC_LOGIN_ID)), //card num2
            '', //card num3
            GetAndCheckWebPassword(EEData), //web password
            '', //options
            '', //?? //home1
            '', //?? //home2
            '', //?? //home3
            '', //schedule
            0 //exportBlock
            );
        finally
          FreeAndNil(rates);
        end;
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.UpdateEmployee(swipeEE: IXMLEmployees; EEData: TEvoEEData; const options: TEEExportOptions);
  function EvoIfNotEmpty(evov: Variant; sc: string; allowClearing: boolean): string;
  var
    evo: string;
  begin
    evo := VarToStr(evov);
    if allowClearing or (trim(evo) <> '') then
      Result := evo
    else
      Result := sc;
  end;
var
  rates: TPayRates;
  exportBlock: boolean;
begin
  FLogger.LogEntry('TimeWorks: Update Employee');
  try
    try
      BeginConnection;
      try
        if swipeEE.ChildNodes.FindNode('ExportBlock') <> nil then //make sure AreDifferent doesn't touch this field! and nothing else does
          exportBlock := swipeEE.ExportBlock
        else
          exportBlock := false;
        if options.ExportRates then
          rates := TPayRates.Create( EEData.GetEERatesArray )
        else
          rates := TPayRates.Create( swipeEE.PayRate0, swipeEE.PayRate1, swipeEE.PayRate2, swipeEE.PayRate3 );
        try
          FService.UpdateEmployee(
            trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), //Employee Code
            swipeEE.Employee, // swipe clock Id, I guess
            EEData.EEs.FieldByName('LAST_NAME').AsString,
            EEData.EEs.FieldByName('FIRST_NAME').AsString,
            EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString,
            swipeEE.Designation, //designation
            swipeEE.Title, //title
            IIF(options.ExportSSN, trim(StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll])), swipeEE.SSN),
            IIF(options.ExportDepartment, trim(EEData.EEs.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString), swipeEE.Department), //dept
            trim(EEData.Company.CUSTOM_COMPANY_NUMBER), //location      //because consolidated
            EvoIfNotEmpty(EEData.GetEEAdditionalField(SC_SUPERVISOR), swipeEE.Supervisor, options.AllowClearingOfSupervisor), //supervisor
            DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime), //? //start date
            swipeEE.LunchMinutes, //autolunch, minute
            swipeEE.AutoLunchHours, //autolunch after Hours
            rates.RateDef, //payRate0 (default)
            rates.Rate1, //payRate1
            rates.Rate2, //payRate2
            rates.Rate3, //payRate3
            EvoIfNotEmpty(GetCardNumber1(EEData), swipeEE.CardNumber1, options.AllowClearingOfCardNumber1), //card num1
            EvoIfNotEmpty(EEData.GetEEAdditionalField(SC_LOGIN_ID), swipeEE.CardNumber2, options.AllowClearingOfWebLogin), //card num2
            swipeEE.CardNumber3, //card num3
            EvoIfNotEmpty(GetAndCheckWebPassword(EEData), swipeEE.Password, options.AllowClearingOfWebPassword), //web password
            '', //options //!!no swipeEE.options!
            '', //?? //home1
            '', //?? //home2
            '', //?? //home3
            swipeEE.Schedule, //schedule //node can be missing
            ord(exportBlock) //exportBlock
            );
        finally
          FreeAndNil(rates);
        end;
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TSwipeClockConnection.CheckAPI2Result(res: widestring; nm: string);
var
  doc: IXmlDocument;
  msg: string;
  desc: string;
begin
  FLogger.LogDebug(Format('%s result', [nm, UTF8Encode(res)]));
{$ifndef FINAL_RELEASE}
  StringToFile( UTF8Encode(res), Redirection.GetDirectory(sDumpDirAlias) + nm +'.xml');
{$endif}
  doc := LoadXMLData(res);
  doc.Options := doc.Options - [doNodeAutoCreate] + [doNodeAutoIndent];
  msg := trim(VarToStr(doc.DocumentElement.ChildValues['message']));
  desc := VarToStr(doc.DocumentElement.ChildValues['description']);
  if SameText(msg, 'Failure') then
    raise Exception.CreateFmt('TimeWorks error: %s', [desc])
  else if not SameText(msg,'Success') then
    FLogger.LogWarningFmt('TimeWorks API returned an unexpected result: message = <%s>, description = <%s>', [msg, desc]);
end;

procedure TSwipeClockConnection.UpdateEmployeeFields(EEData: TEvoEEData; const options: TEEExportOptions);
var
//  rates: TRateArray;
  fields: EmployeeWebInterface.ArrayOfString; // array of WideString

  procedure AddField(fn: string; val: Variant);
  begin
    SetLength(fields, Length(fields)+1);
    fields[high(fields)] := Format('%s=%s', [fn, trim(VarToStr(val))]);
  end;

  procedure AddFieldIfNotEmpty(fn: string; val: Variant; allowClearing: boolean);
  begin
    if (trim(VarToStr(val)) <> '') or allowClearing then
      AddField(fn, val);
  end;

var
  res: widestring;
begin
  FLogger.LogEntry('TimeWorks: Update Employee Fields');
  try
    try
      BeginConnection2;
      try
        SetLength(fields, 0);

        if options.ExportSSN then
          AddField('SSN', StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll]) );
        if options.ExportDepartment then
          AddField('Department', EEData.EEs['CUSTOM_DEPARTMENT_NUMBER'] );

        if options.ExportBranch then //because not consolidated
          AddField('Location', EEData.EEs['CUSTOM_BRANCH_NUMBER'] );
        AddField('StartDate', DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime) );
        AddField('EndDate', '' );

{        if options.ExportRates then
        begin
          rates := EEData.GetEERatesArray;
          AddField('PAYRATE0', FloatToStr(rates[0].rate) );
          AddField('PayRate1', FloatToStr(rates[1].rate) );
          AddField('PayRate2', FloatToStr(rates[2].rate) );
          AddField('PayRate3', FloatToStr(rates[3].rate) );
        end;
        AddFieldIfNotEmpty('CardNumber1', GetCardNumber1(EEData), options.AllowClearingOfCardNumber1 );
        AddFieldIfNotEmpty('CardNumber2', EEData.GetEEAdditionalField(SC_LOGIN_ID), options.AllowClearingOfWebLogin );
        AddFieldIfNotEmpty('Password', GetAndCheckWebPassword(EEData), options.AllowClearingOfWebPassword );
        AddField('PHONE', EEData.EEs['PHONE']);
        AddField('EMAIL', EEData.EEs['EMAIL']);}

        res := FService2.UpdateEmployeeFields(false,
            trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString),
            EEData.EEs.FieldByName('LAST_NAME').AsString,
            EEData.EEs.FieldByName('FIRST_NAME').AsString,
            EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString,
            fields);
        CheckAPI2Result(res, 'UpdateEmployeeFields');
      finally
        EndConnection2;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockConnection.GetSites: IXMLGetSites;
begin
  FLogger.LogEntry('TimeWorks: Get Sites');
  try
    try
      BeginConnection;
      try
        Result := bndGetSites.GetNewDataSet( ToXMLDoc(FService.GetSites(FParam.UserName), 'GetSites') );
      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockConnection.ToXMLDoc(ws: widestring; name: string): IXMLDocument;
begin
  Result := inherited ToXMLDoc(ws, name);
  if (Result <> nil) and (Result.DocumentElement <> nil) and (Result.DocumentElement.ChildNodes <> nil) and
     (Result.DocumentElement.ChildNodes.FindNode('schema') <> nil) then
    Result.DocumentElement.ChildNodes.Delete('schema');
end;

function TSwipeClockConnection.This: TSwipeClockConnection;
begin
  Result := Self;
end;

procedure TSwipeClockConnection.UpdateEmployeeFields_api12(SendPass: boolean;
  EEData: TEvoEEData; const options: TEEExportOptions; AddIfNotFound: boolean);
var
  Employees: ArrayOfEmployeeFieldsToUpdate;
  rates: TRateArrayTWP;
  fields: ArrayOfFieldToUpdate;

  procedure AddFieldAsOf(fn: string; val: Variant; effDate: TDatetime);
  var
    k: integer;
  begin
    SetLength(fields, Length(fields)+1);
    k := Length(fields)-1;
    fields[k] := FieldToUpdate.Create;
    fields[k].FieldName := fn;
    fields[k].Value := VarToStr(val);
    fields[k].EffectiveDate := DateToSwipeClockDate(effDate);
  end;

  procedure AddField(fn: string; val: Variant);
  begin
    AddFieldAsOf(fn, val, Now)
  end;

  function AddFieldIfNotEmpty(fn: string; val: Variant; allowClearing: boolean = False): boolean; overload;
  begin
    Result := False;
    if (trim(VarToStr(val)) <> '') or allowClearing then
    begin
      AddField(fn, val);
      Result := True;
    end;
  end;

  function AddFieldIfNotEmpty(fn: string; val: TEeAdditionalField; allowClearing: boolean = False): boolean; overload;
  begin
    Result := False;
    if val.IsSetup and ((trim(VarToStr(val.Value)) <> '') or allowClearing) then
    begin
      AddField(fn, val.Value);
      Result := True;
    end;
  end;

  procedure AddDepartmentField(const fieldName, twpName: string; r: TRateItemTWP);
  begin
    if options.ExportRates and (r.dep <> '') then
      AddFieldIfNotEmpty(fieldName, r.dep)
    else
      AddFieldIfNotEmpty(fieldName, EEData.GetEEAdditionalFieldVal(twpName));
  end;

var
  res: UpdateEmployeeFieldsApiResult;
begin
  FLogger.LogEntry('TimeWorks: Update Employee Fields (API12)');
  try
    FLogger.LogContextItem('AddIfNotFound', BoolToStr(AddIfNotFound));
    try
      BeginConnection3;
      try
        SetLength(fields, 0);

        AddField('EmployeeCode', trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString));
        AddField('FirstName', EEData.EEs.FieldByName('FIRST_NAME').AsString);
        AddField('LastName', EEData.EEs.FieldByName('LAST_NAME').AsString);
        AddField('MiddleName', EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString);

        if options.ExportSSN then
          AddField('SSN', StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll]) );

        if options.ExportBranch then //because not consolidated
          AddField('Location', EEData.EEs['CUSTOM_BRANCH_NUMBER'] );
        AddField('StartDate', DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime) );
        if not EEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').IsNull and (EEData.EEs['CURRENT_TERMINATION_CODE'] <> 'A'{EE_TERM_ACTIVE}) then
          AddField('EndDate', DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime) )
        else
          AddField('EndDate', '' );

        AddFieldIfNotEmpty('Supervisor', EEData.GetEEAdditionalFieldVal(TWP_SUPERVISOR), options.AllowClearingOfSupervisor );

        if options.ExportRates then
        begin
          rates := EEData.GetEERatesArrayTWP;
          AddFieldAsOf( 'PAYRATE0', FloatToStr(rates[0].rate), rates[0].effectiveDate);
          AddFieldAsOf( 'PayRate1', FloatToStr(rates[1].rate), rates[1].effectiveDate);
          AddFieldAsOf( 'PayRate2', FloatToStr(rates[2].rate), rates[2].effectiveDate);
          AddFieldAsOf( 'PayRate3', FloatToStr(rates[3].rate), rates[3].effectiveDate);
          AddFieldAsOf( 'PayRate4', FloatToStr(rates[4].rate), rates[4].effectiveDate);
          AddFieldAsOf( 'PayRate5', FloatToStr(rates[5].rate), rates[5].effectiveDate);
          AddFieldAsOf( 'PayRate6', FloatToStr(rates[6].rate), rates[6].effectiveDate);
          AddFieldAsOf( 'PayRate7', FloatToStr(rates[7].rate), rates[7].effectiveDate);
          AddFieldAsOf( 'PayRate8', FloatToStr(rates[8].rate), rates[8].effectiveDate);
          AddFieldAsOf( 'PayRate9', FloatToStr(rates[9].rate), rates[9].effectiveDate);

          if options.ExportDepartment then
          begin
            if rates[0].Dep <> '' then
              AddField('Department', rates[0].Dep )
            else
              AddFieldIfNotEmpty('Department', EEData.EEs['CUSTOM_DEPARTMENT_NUMBER'] );
            AddDepartmentField('DEPARTMENT1', TWP_DEPARTMENT_1, rates[1]);
            AddDepartmentField('DEPARTMENT2', TWP_DEPARTMENT_2, rates[2]);
            AddDepartmentField('DEPARTMENT3', TWP_DEPARTMENT_3, rates[3]);
            AddDepartmentField('DEPARTMENT4', TWP_DEPARTMENT_4, rates[4]);
            AddDepartmentField('DEPARTMENT5', TWP_DEPARTMENT_5, rates[5]);
            AddDepartmentField('DEPARTMENT6', TWP_DEPARTMENT_6, rates[6]);
            AddDepartmentField('DEPARTMENT7', TWP_DEPARTMENT_7, rates[7]);
            AddDepartmentField('DEPARTMENT8', TWP_DEPARTMENT_8, rates[8]);
            AddDepartmentField('DEPARTMENT9', TWP_DEPARTMENT_9, rates[9]);
          end;
        end
        else if options.ExportDepartment then
        begin
          AddFieldIfNotEmpty('Department', EEData.EEs['CUSTOM_DEPARTMENT_NUMBER'] );
          AddFieldIfNotEmpty('DEPARTMENT1', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_1));
          AddFieldIfNotEmpty('DEPARTMENT2', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_2));
          AddFieldIfNotEmpty('DEPARTMENT3', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_3));
          AddFieldIfNotEmpty('DEPARTMENT4', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_4));
          AddFieldIfNotEmpty('DEPARTMENT5', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_5));
          AddFieldIfNotEmpty('DEPARTMENT6', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_6));
          AddFieldIfNotEmpty('DEPARTMENT7', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_7));
          AddFieldIfNotEmpty('DEPARTMENT8', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_8));
          AddFieldIfNotEmpty('DEPARTMENT9', EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_9));
        end;

        AddFieldIfNotEmpty('ClockNumberReplace', GetTWPClockNumbers(EEData, options));

        AddFieldIfNotEmpty('HOME1', EEData.GetEEAdditionalFieldVal(TWP_HOME_1));
        AddFieldIfNotEmpty('HOME2', EEData.GetEEAdditionalFieldVal(TWP_HOME_2));
        AddFieldIfNotEmpty('HOME3', EEData.GetEEAdditionalFieldVal(TWP_HOME_3));
        AddFieldIfNotEmpty('HOME4', EEData.GetEEAdditionalFieldVal(TWP_HOME_4));
        AddFieldIfNotEmpty('HOME5', EEData.GetEEAdditionalFieldVal(TWP_HOME_5));
        AddFieldIfNotEmpty('HOME6', EEData.GetEEAdditionalFieldVal(TWP_HOME_6));
        AddFieldIfNotEmpty('HOME7', EEData.GetEEAdditionalFieldVal(TWP_HOME_7));
        AddFieldIfNotEmpty('HOME8', EEData.GetEEAdditionalFieldVal(TWP_HOME_8));
        AddFieldIfNotEmpty('HOME9', EEData.GetEEAdditionalFieldVal(TWP_HOME_9));

        AddFieldIfNotEmpty('LOCATION1', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_1));
        AddFieldIfNotEmpty('LOCATION2', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_2));
        AddFieldIfNotEmpty('LOCATION3', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_3));
        AddFieldIfNotEmpty('LOCATION4', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_4));
        AddFieldIfNotEmpty('LOCATION5', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_5));
        AddFieldIfNotEmpty('LOCATION6', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_6));
        AddFieldIfNotEmpty('LOCATION7', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_7));
        AddFieldIfNotEmpty('LOCATION8', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_8));
        AddFieldIfNotEmpty('LOCATION9', EEData.GetEEAdditionalFieldVal(TWP_LOCATION_9));

        AddFieldIfNotEmpty('GEODATAENABLED', EEData.GetEEAdditionalFieldVal(TWP_GEODATAENABLED));
        AddFieldIfNotEmpty('WEBCLOCKENABLED', EEData.GetEEAdditionalFieldVal(TWP_WEB_CLOCK_ENABLED));
        AddFieldIfNotEmpty('MOBILEPUNCHENABLED', EEData.GetEEAdditionalFieldVal(TWP_MOBILE_PUNCH_ENABLED));
        AddFieldIfNotEmpty('MOBILEENABLED', EEData.GetEEAdditionalFieldVal(TWP_MOBILE_ENABLED));
        AddFieldIfNotEmpty('LOGINS', EEData.GetEEAdditionalFieldVal(TWP_LOGINS));
        AddFieldIfNotEmpty('ACCRUALFACTOR', EEData.GetEEAdditionalFieldVal(TWP_ACCRUAL_FACTOR));

        if SendPass then // initial create of TWP Self-Serve Password
          AddFieldIfNotEmpty('PASSWORD', EEData.GetEEAdditionalFieldVal(TWP_ESS_PASSWORD));

        AddFieldIfNotEmpty('PHONE', EEData.EEs['PHONE']);
        if not AddFieldIfNotEmpty('EMAIL', EEData.EEs['EMAIL_PRIME']) then
          AddFieldIfNotEmpty('EMAIL', EEData.EEs['EMAIL']);                                      

        SetLength(Employees, 1);
        Employees[0] := EmployeeFieldsToUpdate.Create;
        Employees[0].Identifier := trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        Employees[0].OptionalSecondaryIdentifier := '';;
        Employees[0].FieldsToUpdate := fields;

        res := FService3.UpdateEmployeeFields(FSession3,
          'EvoTimeWorks',
          AddIfNotFound,
          EMPLOYEECODE,
          Employees,
          NOTUSED);
        try
          CheclAPI12Result(res, 'UpdateEmployeeFields');
          FLogger.LogDebug('UpdateEmployeeFields FailureReason',  res.FailureReason);
          FLogger.LogDebug('UpdateEmployeeFields Comments',  res.Comments);
        finally
          FreeAndNil(res);
        end;
      finally
        EndConnection3;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

initialization
  {
  http://www.borlandtalk.com/namespace-in-soapheader-how-to-change-vt112473.html

  This might or might not be related but just in case: there was a bug whereby
  Delphi would not recognize document services. The importer would miss
  emitting the call to register the interface. Something along the lines of:

  InvRegistry.RegisterInvokeOptions(TypeInfo(InterfaceName), ioDocument);

  The results of that bug is that serialization would use Section-5 encoding
  rules... which matches what you're seeing.

  ...the issue of not properly detecting document services should be addressed as of HOTFIX10 of BDS2006.
  }
  InvRegistry.RegisterInvokeOptions(TypeInfo(EmployeeWebServiceInterfaceSoap), ioDocument);
  InvRegistry.RegisterInvokeOptions(TypeInfo(EmployeeWebInterfaceSoap), ioDocument);
  InvRegistry.RegisterInvokeOptions(TypeInfo(API12Soap), ioDocument);

end.
