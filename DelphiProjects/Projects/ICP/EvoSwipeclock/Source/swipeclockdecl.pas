unit swipeclockdecl;

interface

uses
  gdystrset, RateImportOptionFrame, isSettings, gdyCommonLogger;

type
  TSwipeClockConnectionParam = record
    UserName: string;
    Password: string;
    Site: string;
    SavePassword: boolean;
  end;

  TSwipeClockEEFilterKind = (fltNone, fltBranch, fltDepartment);

  TSwipeClockEEFilter = record
    Kind: TSwipeClockEEFilterKind;
    BranchCodes: TStringSet;
    DepartmentCodes: TStringSet;
  end;

  TExtAppDatabaseType = (dbClassic, dbGlue);

  TSwipeClockLaborMapping = record
    DepartmentField: string; //XYZDLS
    JobField: string;
    ShiftField: string;
    DivisionField: string;
    BranchField: string;
    RateCodeField: string;
    TeamField: string;
    WorkCompField: string;
    DatabaseType: TExtAppDatabaseType;
  end;

  TSwipeClockNumericMapping = record
    SerializedVarToEDCode: string;
  end;

  TEEExportOptions = record
    ExportSSN: boolean;
    ExportRates: boolean;
    ExportBranch: boolean;
    ExportDepartment: boolean;
    AllowClearingOfCardNumber1: boolean;
    AllowClearingOfSupervisor: boolean;
    AllowClearingOfWebPassword: boolean;
    AllowClearingOfWebLogin: boolean;
    UseTimeClockNumber: boolean;
    UseEeAddInfo: boolean;
    UseEeCode: boolean;
  end;

  TTCImportOptions = record
    EEFilter: TSwipeClockEEFilter;
    Summarize: boolean;
    RateImport: TRateImportOption;
    LaborMapping: TSwipeClockLaborMapping;
    AllowImportToBatchesWithUserEarningLines: boolean; //!!
    NumericMapping: TSwipeClockNumericMapping;
    AutoImportJobCodes: boolean;
  end;

  TExtAppSettings = record
    ConsolidatedDatabase: boolean;
  end;

function LoadEEExportOptions(conf: IisSettings; root: string): TEEExportOptions;
procedure SaveEEExportOptions( const options: TEEExportOptions; conf: IisSettings; root: string);
procedure LogEEExportOptions(Logger: ICommonLogger; options: TEEExportOptions);
function DescribeEEExportOptions(options: TEEExportOptions): string;

function LoadTCImportOptions(conf: IisSettings; root: string; databaseType: TExtAppDatabaseType): TTCImportOptions;
procedure SaveTCImportOptions( const options: TTCImportOptions; conf: IisSettings; root: string);
procedure LogTCImportOptions(Logger: ICommonLogger; options: TTCImportOptions);
function DescribeTCImportOptions(options: TTCImportOptions): string;

function LoadConnectionSettings(conf: IisSettings; root: string): TSwipeClockConnectionParam;
procedure SaveConnectionSettings(const param: TSwipeClockConnectionParam; conf: IisSettings; root: string);
function AreConnectionSettingsTheSame(one, another: TSwipeClockConnectionParam): boolean; //ignores SavePassword;

function LoadExtAppSettings(conf: IisSettings; root: string): TExtAppSettings;
procedure SaveExtAppSettings( const param: TExtAppSettings; conf: IisSettings; root: string);
procedure LogExtAppSettings(Logger: ICommonLogger; const param: TExtAppSettings);

const
  SwipeClockFieldCodes: string = 'XYZDLS';
  SCHomeLocationFieldCode: string = 'H-76E2AFDA-2600-4643-B51F-85981947633D';
  SCHomeDepartmentFieldCode: string = 'D-76E2AFDA-2600-4643-B51F-85981947633D';
  SCHomeSupervisorFieldCode: string = 'S-76E2AFDA-2600-4643-B51F-85981947633D';

  SC_PASSWORD: string = 'SC_PASSWORD';
  SC_LOGIN_ID: string = 'SC_LOGIN_ID';
  SC_SUPERVISOR: string = 'SC_SUPERVISOR';

  TWP_SUPERVISOR: string = 'TWP_SUPERVISOR';
  TWP_HOME_1: string = 'TWP_HOME1';
  TWP_HOME_2: string = 'TWP_HOME2';
  TWP_HOME_3: string = 'TWP_HOME3';
  TWP_HOME_4: string = 'TWP_HOME4';
  TWP_HOME_5: string = 'TWP_HOME5';
  TWP_HOME_6: string = 'TWP_HOME6';
  TWP_HOME_7: string = 'TWP_HOME7';
  TWP_HOME_8: string = 'TWP_HOME8';
  TWP_HOME_9: string = 'TWP_HOME9';
  TWP_DEPARTMENT_1: string = 'TWP_DEPARTMENT1';
  TWP_DEPARTMENT_2: string = 'TWP_DEPARTMENT2';
  TWP_DEPARTMENT_3: string = 'TWP_DEPARTMENT3';
  TWP_DEPARTMENT_4: string = 'TWP_DEPARTMENT4';
  TWP_DEPARTMENT_5: string = 'TWP_DEPARTMENT5';
  TWP_DEPARTMENT_6: string = 'TWP_DEPARTMENT6';
  TWP_DEPARTMENT_7: string = 'TWP_DEPARTMENT7';
  TWP_DEPARTMENT_8: string = 'TWP_DEPARTMENT8';
  TWP_DEPARTMENT_9: string = 'TWP_DEPARTMENT9';
  TWP_LOCATION_1: string = 'TWP_LOCATION1';
  TWP_LOCATION_2: string = 'TWP_LOCATION2';
  TWP_LOCATION_3: string = 'TWP_LOCATION3';
  TWP_LOCATION_4: string = 'TWP_LOCATION4';
  TWP_LOCATION_5: string = 'TWP_LOCATION5';
  TWP_LOCATION_6: string = 'TWP_LOCATION6';
  TWP_LOCATION_7: string = 'TWP_LOCATION7';
  TWP_LOCATION_8: string = 'TWP_LOCATION8';
  TWP_LOCATION_9: string = 'TWP_LOCATION9';
  TWP_PHONE: string = 'TWP_PHONE';
  TWP_EMAIL: string = 'TWP_EMAIL';
  TWP_WEB_CLOCK_ENABLED: string = 'TWP_WEBCLOCKENABLED';
  TWP_MOBILE_PUNCH_ENABLED: string = 'TWP_MOBILEPUNCHENABLED';
  TWP_MOBILE_ENABLED: string = 'TWP_MOBILEENABLED';
  TWP_LOGINS: string = 'TWP_LOGINS';
  TWP_ESS_PASSWORD: string = 'TWP_ESSPASSWORD';
  TWP_ACCRUAL_FACTOR: string = 'TWP_ACCRUALFACTOR';
  TWP_GEODATAENABLED: string = 'TWP_GPSMOBILE';
  TWP_LOGIN = 'TWP_LOGIN';
  TWP_LOGIN1 = 'TWP_LOGIN1';
  TWP_LOGIN2 = 'TWP_LOGIN2';

function EEFilterToStr(EEFilter: TSwipeClockEEFilter): string;

implementation

uses
  sysutils, gdycommon, gdyClasses, common, gdyCrypt;

function EEFilterToStr(EEFilter: TSwipeClockEEFilter): string;
begin
  case EEFilter.Kind of
    fltNone: Result := 'No filter';
    fltBranch: Result := Format('Branches: %s',[SetToStr(EEFilter.BranchCodes,', ')]);
    fltDepartment: Result := Format('Departments: %s',[SetToStr(EEFilter.DepartmentCodes,', ')]);
  else
    Assert(false);
    Result := 'Invalid';
  end;
end;

const
  cKey='Form.Button';

function LoadEEExportOptions(conf: IisSettings; root: string): TEEExportOptions;
begin
  root := root + IIF(root='','','\') + 'EEExportOptions\';
  Result.ExportSSN := conf.AsBoolean[root+'ExportSSN'];
  Result.ExportRates := conf.AsBoolean[root+'ExportRates'];
  Result.ExportBranch := conf.AsBoolean[root+'ExportBranch'];
  Result.ExportDepartment := conf.AsBoolean[root+'ExportDepartment'];
  Result.UseTimeClockNumber := conf.AsBoolean[root+'UseTimeClockNumber'];
  Result.UseEeAddInfo := conf.AsBoolean[root+'UseEeAddInfo'];
  Result.UseEeCode := conf.AsBoolean[root+'UseEeCode'];
  if conf.GetValueNames(WithoutTrailingSlash(root)).IndexOf('AllowClearingOfCardNumber1') <> -1 then
  begin
    Result.AllowClearingOfCardNumber1 := conf.AsBoolean[root+'AllowClearingOfCardNumber1'];
    Result.AllowClearingOfSupervisor := conf.AsBoolean[root+'AllowClearingOfSupervisor'];
    Result.AllowClearingOfWebPassword := conf.AsBoolean[root+'AllowClearingOfWebPassword'];
    Result.AllowClearingOfWebLogin := conf.AsBoolean[root+'AllowClearingOfWebLogin'];
  end
  else
  begin
    Result.AllowClearingOfCardNumber1 := conf.AsBoolean[root+'AllowClearing'];
    Result.AllowClearingOfSupervisor := conf.AsBoolean[root+'AllowClearing'];
    Result.AllowClearingOfWebPassword := conf.AsBoolean[root+'AllowClearing'];
    Result.AllowClearingOfWebLogin := conf.AsBoolean[root+'AllowClearing'];
  end;

end;

procedure LogEEExportOptions(Logger: ICommonLogger; options: TEEExportOptions);
begin
  logger.LogContextItem( 'Export SSN', BoolToStr(options.ExportSSN, true) );
  logger.LogContextItem( 'Export rates', BoolToStr(options.ExportRates, true) );
  logger.LogContextItem( 'Export branch', BoolToStr(options.ExportBranch, true) );
  logger.LogContextItem( 'Export department', BoolToStr(options.ExportDepartment, true) );
  logger.LogContextItem( 'Allow clearing of card number 1', BoolToStr(options.AllowClearingOfCardNumber1, true) );
  logger.LogContextItem( 'Allow clearing of supervisor', BoolToStr(options.AllowClearingOfSupervisor, true) );
  logger.LogContextItem( 'Allow clearing of web password', BoolToStr(options.AllowClearingOfWebPassword, true) );
  logger.LogContextItem( 'Allow clearing of web login ID', BoolToStr(options.AllowClearingOfWebLogin, true) );
  logger.LogContextItem( 'Use Employee Number as a Login', BoolToStr(options.UseEeCode, true) );
  logger.LogContextItem( 'Use Employee Additional Info fields as a Login', BoolToStr(options.UseEeAddInfo, true) );
  logger.LogContextItem( 'Use Employee Time Clock Number as a Login', BoolToStr(options.UseTimeClockNumber, true) );
end;

function DescribeEEExportOptions(options: TEEExportOptions): string;
begin
  Result := '';
  Result := Result + 'Export SSN: ' + BoolToStr(options.ExportSSN, true) + #13#10;
  Result := Result + 'Export rates: ' + BoolToStr(options.ExportRates, true) + #13#10;
  Result := Result + 'Export branch: ' + BoolToStr(options.ExportBranch, true) + #13#10;
  Result := Result + 'Export department: ' + BoolToStr(options.ExportDepartment, true) + #13#10;
  Result := Result + 'Use Employee Number as a Login: ' + BoolToStr(options.UseEeCode, true) + #13#10;
  Result := Result + 'Use Employee Additional Info fields as a Login: ' + BoolToStr(options.UseEeAddInfo, true) + #13#10;
  Result := Result + 'Use Employee Time Clock Number as a Login: ' + BoolToStr(options.UseTimeClockNumber, true) + #13#10;
//  Result := Result + 'Consolidated database: ' + BoolToStr(options.ConsolidatedDatabase, true) + #13#10;
  Result := Result + 'Allow clearing of card number 1: ' + BoolToStr(options.AllowClearingOfCardNumber1, true) + #13#10;
  Result := Result + 'Allow clearing of supervisor: ' + BoolToStr(options.AllowClearingOfSupervisor, true) + #13#10;
  Result := Result + 'Allow clearing of web password: ' + BoolToStr(options.AllowClearingOfWebPassword, true) + #13#10;
  Result := Result + 'Allow clearing of web login ID: ' + BoolToStr(options.AllowClearingOfWebLogin, true) + #13#10;
end;

procedure SaveEEExportOptions( const options: TEEExportOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EEExportOptions\';
  conf.AsBoolean[root+'ExportSSN'] := options.ExportSSN;
  conf.AsBoolean[root+'ExportRates'] := options.ExportRates;
  conf.AsBoolean[root+'ExportBranch'] := options.ExportBranch;
  conf.AsBoolean[root+'ExportDepartment'] := options.ExportDepartment;
  conf.AsBoolean[root+'UseEeCode'] := options.UseEeCode;
  conf.AsBoolean[root+'UseEeAddInfo'] := options.UseEeAddInfo;
  conf.AsBoolean[root+'UseTimeClockNumber'] := options.UseTimeClockNumber;

  conf.AsBoolean[root+'AllowClearingOfCardNumber1'] := options.AllowClearingOfCardNumber1;
  conf.AsBoolean[root+'AllowClearingOfSupervisor'] := options.AllowClearingOfSupervisor;
  conf.AsBoolean[root+'AllowClearingOfWebPassword'] := options.AllowClearingOfWebPassword;
  conf.AsBoolean[root+'AllowClearingOfWebLogin'] := options.AllowClearingOfWebLogin;
  conf.DeleteValue(root+'AllowClearing');
end;

function LoadTCImportOptions(conf: IisSettings; root: string; databaseType: TExtAppDatabaseType): TTCImportOptions;
var
  i: integer;
  laborMappingPath: string;
  temp: IStr;
begin
  root := root + IIF(root='','','\') + 'SwipeClockOptions\';

  Result.EEFilter.Kind := TSwipeClockEEFilterKind(conf.AsInteger[root+'EEFilter\Kind']);
  if (Result.EEFilter.Kind < low(TSwipeClockEEFilterKind)) or (Result.EEFilter.Kind > high(TSwipeClockEEFilterKind)) then
    Result.EEFilter.Kind := fltNone;
  with CsvAsStr(conf.AsString[root+'EEFilter\BranchCodes']) do
    for i := 0 to Count-1 do
      SetInclude(Result.EEFilter.BranchCodes, Str[i]);
  with CsvAsStr(conf.AsString[root+'EEFilter\DepartmentCodes']) do
    for i := 0 to Count-1 do
      SetInclude(Result.EEFilter.DepartmentCodes, Str[i]);

  Result.Summarize := conf.AsBoolean[root+'Summarize'];

  Result.RateImport := TRateImportOption(conf.AsInteger[root+'RateImport']);
  if (Result.RateImport < low(TRateImportOption)) or (Result.RateImport > high(TRateImportOption)) then
    Result.RateImport := rateUseExternal;

  Result.LaborMapping.DatabaseType := databaseType;
  case Result.LaborMapping.DatabaseType of
    dbClassic:
    begin
      laborMappingPath := root+'LaborMapping\';

      Result.LaborMapping.DepartmentField := Copy(conf.AsString[laborMappingPath+'DepartmentField'], 1, 1);
      Result.LaborMapping.JobField := Copy(conf.AsString[laborMappingPath+'JobField'], 1, 1);
      Result.LaborMapping.ShiftField := Copy(conf.AsString[laborMappingPath+'ShiftField'], 1, 1);
      Result.LaborMapping.DivisionField := Copy(conf.AsString[laborMappingPath+'DivisionField'], 1, 1);
      Result.LaborMapping.BranchField := Copy(conf.AsString[laborMappingPath+'BranchField'], 1, 1);
      Result.LaborMapping.RateCodeField := Copy(conf.AsString[laborMappingPath+'RateCodeField'], 1, 1);
      Result.LaborMapping.WorkCompField := Copy(conf.AsString[laborMappingPath+'WorkCompField'], 1, 1);
      Result.LaborMapping.TeamField := Copy(conf.AsString[laborMappingPath+'TeamField'], 1, 1);

      if Pos(Result.LaborMapping.DepartmentField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.DepartmentField := '';
      if Pos(Result.LaborMapping.JobField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.JobField := '';
      if Pos(Result.LaborMapping.ShiftField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.ShiftField := '';
      if Pos(Result.LaborMapping.DivisionField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.DivisionField := '';
      if Pos(Result.LaborMapping.BranchField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.BranchField := '';
      if Pos(Result.LaborMapping.RateCodeField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.RateCodeField := '';
      if Pos(Result.LaborMapping.WorkCompField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.WorkCompField := '';
      if Pos(Result.LaborMapping.TeamField, SwipeClockFieldCodes) = 0 then
        Result.LaborMapping.TeamField := '';

      if conf.GetValueNames(root+'\Classic').IndexOf('NumericMapping') <> -1 then
        Result.NumericMapping.SerializedVarToEDCode := conf.AsString[root+'Classic\NumericMapping']
      else
      begin
        temp := EmptyIStr;
        if conf.AsString[root+'NumericMapping\IEdCode'] <> '' then
          temp.Add('I=' + conf.AsString[root+'NumericMapping\IEdCode']);
        if conf.AsString[root+'NumericMapping\JEdCode'] <> '' then
          temp.Add('J=' + conf.AsString[root+'NumericMapping\JEdCode']);
        if conf.AsString[root+'NumericMapping\KEdCode'] <> '' then
          temp.Add('K=' + conf.AsString[root+'NumericMapping\KEdCode']);
        Result.NumericMapping.SerializedVarToEDCode := temp.AsTStrings.Text;
      end
    end;
    dbGlue:
    begin
      laborMappingPath := root+'Glue\LaborMapping\';

      Result.LaborMapping.DepartmentField := conf.AsString[laborMappingPath+'DepartmentField'];
      Result.LaborMapping.JobField := conf.AsString[laborMappingPath+'JobField'];
      Result.LaborMapping.ShiftField := conf.AsString[laborMappingPath+'ShiftField'];
      Result.LaborMapping.DivisionField := conf.AsString[laborMappingPath+'DivisionField'];
      Result.LaborMapping.BranchField := conf.AsString[laborMappingPath+'BranchField'];
      Result.LaborMapping.RateCodeField := conf.AsString[laborMappingPath+'RateCodeField'];
      Result.LaborMapping.WorkCompField := conf.AsString[laborMappingPath+'WorkCompField'];
      Result.LaborMapping.TeamField := conf.AsString[laborMappingPath+'TeamField'];

      Result.NumericMapping.SerializedVarToEDCode := conf.AsString[root+'Glue\NumericMapping'];
    end;
  else
    Assert(false);
  end;

  Result.AllowImportToBatchesWithUserEarningLines := conf.AsBoolean[root+'AllowImportToBatchesWithUserEarningLines'];
  Result.AutoImportJobCodes := conf.AsBoolean[root+'AutoImportJobCodes'];
end;

procedure SaveTCImportOptions( const options: TTCImportOptions; conf: IisSettings; root: string);
var
  laborMappingPath: string;
begin
  root := root + IIF(root='','','\') + 'SwipeClockOptions\';
  conf.AsInteger[root+'EEFilter\Kind'] := ord(options.EEFilter.Kind);
  conf.AsString[root+'EEFilter\BranchCodes'] := StringSetToCsv(options.EEFilter.BranchCodes);
  conf.AsString[root+'EEFilter\DepartmentCodes'] := StringSetToCsv(options.EEFilter.DepartmentCodes);
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsInteger[root+'RateImport'] := ord(options.RateImport);

  case options.LaborMapping.DatabaseType of
    dbClassic:
    begin
      conf.AsString[root+'Classic\NumericMapping'] := options.NumericMapping.SerializedVarToEDCode;
      laborMappingPath := root+'LaborMapping\';
    end;
    dbGlue:
    begin
      conf.AsString[root+'Glue\NumericMapping'] := options.NumericMapping.SerializedVarToEDCode;
      laborMappingPath := root+'Glue\LaborMapping\';
    end;
  else
    Assert(false);
  end;

  conf.AsString[laborMappingPath+'DepartmentField'] := options.LaborMapping.DepartmentField;
  conf.AsString[laborMappingPath+'JobField'] := options.LaborMapping.JobField;
  conf.AsString[laborMappingPath+'ShiftField'] := options.LaborMapping.ShiftField;
  conf.AsString[laborMappingPath+'DivisionField'] := options.LaborMapping.DivisionField;
  conf.AsString[laborMappingPath+'BranchField'] := options.LaborMapping.BranchField;
  conf.AsString[laborMappingPath+'RateCodeField'] := options.LaborMapping.RateCodeField;
  conf.AsString[laborMappingPath+'WorkCompField'] := options.LaborMapping.WorkCompField;
  conf.AsString[laborMappingPath+'TeamField'] := options.LaborMapping.TeamField;

  conf.AsBoolean[root+'AllowImportToBatchesWithUserEarningLines'] := options.AllowImportToBatchesWithUserEarningLines;
  conf.AsBoolean[root+'AutoImportJobCodes'] := options.AutoImportJobCodes;
end;

function ConvEEFieldsToReadable(fn: string): string;
begin
  if fn = SCHomeLocationFieldCode then
    Result := '<Home Location>'
  else if fn = SCHomeDepartmentFieldCode then
    Result := '<Home Department>'
  else if fn = SCHomeSupervisorFieldCode then
    Result := '<Home Supervisor>'
  else
    Result := fn;
end;

function DescribeLaborMapping(lm: TSwipeClockLaborMapping): string;
begin
  Result := '';
  if lm.DivisionField <> '' then
    Result := Result + '  Division field: ' + ConvEEFieldsToReadable(lm.DivisionField) + #13#10;
  if lm.BranchField <> '' then
    Result := Result + '  Branch field: ' + ConvEEFieldsToReadable(lm.BranchField) + #13#10;
  if lm.DepartmentField <> '' then
    Result := Result + '  Department field: ' + ConvEEFieldsToReadable(lm.DepartmentField) + #13#10;
  if lm.TeamField <> '' then
    Result := Result + '  Team field: ' + ConvEEFieldsToReadable(lm.TeamField) + #13#10;
  if lm.JobField <> '' then
    Result := Result + '  Job field: ' + ConvEEFieldsToReadable(lm.JobField) + #13#10;
  if lm.ShiftField <> '' then
    Result := Result + '  Shift field: ' + ConvEEFieldsToReadable(lm.ShiftField) + #13#10;
  if lm.RateCodeField <> '' then
    Result := Result + '  Rate code field: ' + ConvEEFieldsToReadable(lm.RateCodeField) + #13#10;
  if lm.WorkCompField <> '' then
    Result := Result + '  Workers''s compensation code field: ' + ConvEEFieldsToReadable(lm.WorkCompField) + #13#10;

  if Result <> '' then
    Result := 'Labor mapping:'#13#10 + Result;
end;

function DescribeNumericMapping(nm: TSwipeClockNumericMapping): string;
var
  temp: IStr;
  i: integer;
begin
  Result := '';
  temp := SplitToLines(nm.SerializedVarToEDCode);
  for i := 0 to temp.Count-1 do
    Result := Result + '  ' + temp.AsTStrings.Names[i] + ' E/D Code: ' + temp.AsTStrings.Values[temp.AsTStrings.Names[i]] + #13#10;

  if Result <> '' then
    Result := 'Numeric mapping:'#13#10 + Result;
end;

function DescribeTCImportOptions(options: TTCImportOptions): string;
begin
  Result := '';
  Result := Result + 'Summarize: ' + BoolToStr(options.Summarize, true) + #13#10;
  Result := Result + DescribeRateImportOption(options.RateImport, 'TimeWorks') + #13#10;
  if options.EEFilter.Kind <> fltNone then
    Result := Result + 'Employee filter: ' + EEFilterToStr(options.EEFilter) + #13#10;
  Result := Result + DescribeLaborMapping(options.LaborMapping);
  Result := Result + DescribeNumericMapping(options.NumericMapping);
  Result := Result + 'Allow import to batches that already have checks with earnings: ' + BoolToStr(options.AllowImportToBatchesWithUserEarningLines, true) + #13#10;
  Result := Result + 'Auto import job codes: ' + BoolToStr(options.AutoImportJobCodes, true) + #13#10;
end;

procedure LogLaborMapping(Logger: ICommonLogger; lm: TSwipeClockLaborMapping);
begin
  logger.LogContextItem('Labor mapping - division field', ConvEEFieldsToReadable(lm.DivisionField));
  logger.LogContextItem('Labor mapping - branch field', ConvEEFieldsToReadable(lm.BranchField));
  logger.LogContextItem('Labor mapping - department field', ConvEEFieldsToReadable(lm.DepartmentField));
  logger.LogContextItem('Labor mapping - team field', ConvEEFieldsToReadable(lm.TeamField));
  logger.LogContextItem('Labor mapping - job field', ConvEEFieldsToReadable(lm.JobField));
  logger.LogContextItem('Labor mapping - shift field', ConvEEFieldsToReadable(lm.ShiftField));
  logger.LogContextItem('Labor mapping - race code field', ConvEEFieldsToReadable(lm.RateCodeField));
  logger.LogContextItem('Labor mapping - worker''s compensation code field', ConvEEFieldsToReadable(lm.WorkCompField));
end;

procedure LogNumericMapping(Logger: ICommonLogger; nm: TSwipeClockNumericMapping);
var
  temp: IStr;
  i: integer;
begin
  temp := SplitToLines(nm.SerializedVarToEDCode);
  for i := 0 to temp.Count-1 do
    logger.LogContextItem('Numeric mapping - '+temp.AsTStrings.Names[i]+' E/D Code', temp.AsTStrings.Values[temp.AsTStrings.Names[i]]);
end;

procedure LogTCImportOptions(Logger: ICommonLogger; options: TTCImportOptions);
begin
  logger.LogContextItem( 'Summarize', BoolToStr(options.Summarize, true) );
  LogRateImportOption(logger, options.RateImport, 'TimeWorks');
  logger.LogContextItem('Employee filter', EEFilterToStr(options.EEFilter) );
  LogLaborMapping(Logger, options.LaborMapping);
  LogNumericMapping(Logger, options.NumericMapping);
  logger.LogContextItem( 'Allow import to batches that already have checks with earnings', BoolToStr(options.AllowImportToBatchesWithUserEarningLines, true) );
  logger.LogContextItem( 'Auto import job codes', BoolToStr(options.AutoImportJobCodes, true) );
end;

function LoadConnectionSettings(conf: IisSettings; root: string): TSwipeClockConnectionParam;
begin
  root := root + IIF(root='','','\') + 'SwipeClockConnection\';
  Result.UserName := conf.AsString[root+'UserName'];
  if conf.GetValueNames(WithoutTrailingSlash(root)).IndexOf('Password') <> -1 then
    Result.Password := conf.AsString[root+'Password']
  else
    Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  Result.Site := conf.AsString[root+'Site'];
end;

procedure SaveConnectionSettings(const param: TSwipeClockConnectionParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'SwipeClockConnection\';
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;

  conf.AsString[root+'Site'] := param.Site;
end;

function AreConnectionSettingsTheSame(one, another: TSwipeClockConnectionParam): boolean;
begin
  Result := (trim(one.UserName) = trim(another.UserName)) and (trim(one.Password) = trim(another.Password)) and (trim(one.Site) = trim(another.Site));
end;

function LoadExtAppSettings(conf: IisSettings; root: string): TExtAppSettings;
begin
  root := root + IIF(root='','','\') + 'SwipeClockCompanySettings\';
  Result.ConsolidatedDatabase := conf.AsBoolean[root+'ConsolidatedDatabase'];
end;

function ExtAppDatabaseTypeToStr(dbt: TExtAppDatabaseType): string;
begin
  case dbt of
    dbClassic: Result := 'Classic';
    dbGlue: Result := 'Glue';
  else
    Assert(false);
  end;
end;

procedure SaveExtAppSettings( const param: TExtAppSettings; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'SwipeClockCompanySettings\';
  conf.AsBoolean[root+'ConsolidatedDatabase'] := param.ConsolidatedDatabase;
end;

procedure LogExtAppSettings(Logger: ICommonLogger; const param: TExtAppSettings);
begin
  logger.LogContextItem( 'Consolidated database', BoolToStr(param.ConsolidatedDatabase, true) );
end;

end.


