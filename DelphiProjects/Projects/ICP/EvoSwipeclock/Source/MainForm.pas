unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoTCImportMainNewForm, ActnList, EvolutionCompanySelectorFrame,
  StdCtrls, Buttons, ComCtrls, EvolutionPrPrBatchFrame, ExtCtrls,
  EvoAPIConnectionParamFrame, SwipeClockConnectionParamFrame, common,
  timeclockimport, evoapiconnectionutils, evodata,
  EEExportOptionsFrame, OptionsBaseFrame, TCImportOptionsFrame,
  SchedulerFrame, scheduledTask, SmtpConfigFrame, PlannedActionConfirmationForm,
  EEFilterFrame, SwipeclockEEExportFilterFrame, ExtAppSettingsFrame;

type
  TMainFm = class(TEvoTCImportMainNewFm)
    TCImportOptionsFrame: TTCImportOptionsFrm;
    BitBtn1: TBitBtn;
    SwipeClockFrame: TSwipeClockConnectionParamFrm;
    tbshEEExport: TTabSheet;
    Panel2: TPanel;
    BitBtn5: TBitBtn;
    EEExportOptionsFrame: TEEExportOptionsFrm;
    tbshScheduler: TTabSheet;
    tbshSchedulerSettings: TTabSheet;
    SchedulerFrame: TSchedulerFrm;
    SmtpConfigFrame: TSmtpConfigFrm;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    EEFilterFrame: TSwipeclockEEExportFilterFrm;
    ExtAppSettingsFrame: TExtAppSettingsFrm;
    lblGlueStatus: TLabel;
    procedure RunActionExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunEmployeeExportUpdate(Sender: TObject);
    procedure RunEmployeeExportExecute(Sender: TObject);

    procedure actScheduleEEExportExecute(Sender: TObject);
    procedure actScheduleTCImportUpdate(Sender: TObject);
    procedure actScheduleTCImportExecute(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    FSiteName: string;
    FNeverWarnOnAllowClearing: boolean;
    procedure CheckThatCompanyNameMatches_EEExport(opname: string);
    procedure CheckThatCompanyNameMatches_TCImport(opname: string);
    procedure CheckThatClearingIsOk;
    function GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
  	function UpdateEmployees: TUpdateEmployeesStat;
    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);

    procedure HandleSwipeClockConnectionSettingsChangedByUser(Sender: TObject);
    procedure HandleEEExportOptionsChangedByUser(Sender: TObject);
    procedure HandleTCImportOptionsChangedByUser(Sender: TObject);
    procedure HandleEEFilterChangedByUser(Sender: TObject);
    procedure HandleExtAppSettingsChangedByUser(Sender: TObject);

    procedure UnInitPerCompanySettings;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);

    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleCanEditParameters(var can: boolean);
    procedure HandleSmtpConfigChange(Sender: TObject);
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  swipeclockdecl, swipeclock, swipeclockConnection, gdyGlobalWaitIndicator,
  userActionHelpers, gdyDialogEngine,  gdyMessageDialogFrame,
  EvoAPIClientMainForm, SwipeclockTasks, gdyclasses, eefilter, 
  wwdblook;

{ TMainNewFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
  CompanyFrame.ShowBothNumberAndName;
  FEvoData.AddDS(CUSTOM_DBDTDesc);
  FEvoData.AddDS(CO_E_D_CODESDesc);

  try
    FNeverWarnOnAllowClearing := FSettings.AsBoolean['GUIOptions\NeverWarnOnAllowClearing'];
  except
    Logger.StopException;
  end;

  UnInitPerCompanySettings;

  //now after initialization attach handlers
  SwipeClockFrame.OnChangeByUser := HandleSwipeClockConnectionSettingsChangedByUser;
  EEExportOptionsFrame.OnChangeByUser := HandleEEExportOptionsChangedByUser;
  TCImportOptionsFrame.OnChangeByUser := HandleTCImportOptionsChangedByUser;
  EeFilterFrame.OnChangeByUser := HandleEEFilterChangedByUser;
  ExtAppSettingsFrame.OnChangeByUser := HandleExtAppSettingsChangedByUser;

  FEvoData.Advise(HandleCompanyChanged);

  try
    SchedulerFrame.Configure(Logger, TSwipeClockTaskAdapter.Create, HandleEditParameters);
    SchedulerFrame.OnCanEditParameters := HandleCanEditParameters;
  except
    Logger.StopException;
  end;

  SmtpConfigFrame.Config := LoadSmtpConfig(FSettings, '');
  SmtpConfigFrame.OnChangeByUser := HandleSmtpConfigChange;

  if EvoFrame.IsValid then
    PageControl1.ActivePageIndex := 2
  else
    PageControl1.ActivePageIndex := 0;
end;

destructor TMainFm.Destroy;
begin
  try
    FSettings.AsBoolean['GUIOptions\NeverWarnOnAllowClearing'] := FNeverWarnOnAllowClearing;
  except
    Logger.StopException;
  end;
  inherited;
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result := swipeclock.GetTimeClockData( SwipeClockFrame.Param, TCImportOptionsFrame.Options, Logger, company, period, ExtAppSettingsFrame.Settings );
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      LogTCImportOptions(logger, TCImportOptionsFrame.Options);
      CheckThatCompanyNameMatches_TCImport('import');
      RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData, FEvoData.Connection, TCImportOptionsFrame.Options.AllowImportToBatchesWithUserEarningLines);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := SwipeClockFrame.IsValid and FEvoData.CanGetPrBatch and TCImportOptionsFrame.IsValid;
end;

type
  TAskResult = record
    Ok: boolean;
    NeverAskAgain: boolean;
  end;

function Ask(Owner: TComponent; msg: string): TAskResult;
var
  dlg: TMessageDialog;
begin
  dlg := TMessageDialog.Create(Owner);
  try
    dlg.MessageText := msg;
    dlg.Caption := Application.Title;
    with DialogEngine( dlg, Owner ) do
    begin
      NeverShowAgainOption := true;
      Result.Ok := ShowModal = mrOk;
      Result.NeverAskAgain := NeverShowAgain;
    end
  finally
    FreeAndNil(dlg);
  end;
end;

function TMainFm.UpdateEmployees: TUpdateEmployeesStat;
var
  EEData: TEvoEEData;
  updater: TSwipeClockEmployeeUpdater;
  analysisResult: TAnalyzeResult;
begin
  EEData := FEvoData.CreateEEData;
  try
    updater := TSwipeClockEmployeeUpdater.Create(SwipeClockFrame.Param, EEExportOptionsFrame.Options, Logger, EEFilterFrame.GetFilter, EEData, ExtAppSettingsFrame.Settings );
    try
      WaitIndicator.StartWait('Preparing');
      try
        analysisResult := updater.Analyze;
        if analysisResult.Actions.Count > 0 then
          Logger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        if not ConfirmActions(Self, analysisResult.Actions.ToText) then
          raise Exception.Create('User cancelled operation');
      finally
        WaitIndicator.EndWait;
      end;
      Result := updater.Apply(analysisResult.Actions, Self as IProgressIndicator);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      FreeAndNil(updater);
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.RunEmployeeExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      CheckThatCompanyNameMatches_EEExport('export');
      CheckThatClearingIsOk;
      EEFilterFrame.Check;

      userActionHelpers.RunEmployeeExport(Logger, UpdateEmployees, FEvoData.GetClCo, 'TimeWorks&Plus');

      WaitIndicator.StartWait('Refreshing TimeWorks&Plus location and department lists');
      try
        with TSwipeClockConnection.Create(SwipeClockFrame.Param, logger) do
        try
          TCImportOptionsFrame.UpdateBranchAndDepartmentCodes( This.GetEmployees );
        finally
          Free;
        end;
      finally
        WaitIndicator.EndWait;
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunEmployeeExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := SwipeClockFrame.IsValid and FEvoData.CanGetClCo;
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  UnInitPerCompanySettings;
  Repaint;
  if new.HasValue then
  begin
    LoadPerCompanySettings(new.Value);
    Repaint;
  end;
end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  try
    SwipeClockFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    EEExportOptionsFrame.Clear(false);
  except
    Logger.StopException;
  end;
  try
    TCImportOptionsFrame.Clear(false);
    lblGlueStatus.Caption := '';
  except
    Logger.StopException;
  end;
  try
    EeFilterFrame.ClearAndDisable;
  except
    Logger.StopException;
  end;
  try
    ExtAppSettingsFrame.Clear(false);
  except
    Logger.StopException;
  end;

  FSiteName := '???';
end;


procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
var
  ctx: TTCImportOptionsContext;
begin
  try
    SwipeClockFrame.Clear(true);
    SwipeClockFrame.Param := LoadConnectionSettings(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;
  try
    EEExportOptionsFrame.Clear(true);
    EEExportOptionsFrame.Options := LoadEEExportOptions(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;

  try
    EeFilterFrame.Init( FEvoData.DS['CUSTOM_DBDT'], LoadEEFilter(FSettings, CompanyUniquePath(Value)) );
  except
    Logger.StopException;
  end;

  try
    ExtAppSettingsFrame.Clear(true);
    ExtAppSettingsFrame.Settings := LoadExtAppSettings(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;

  if SwipeClockFrame.IsValid then
  begin
    try
      ctx := GetTCImportOptionsContext(SwipeClockFrame.Param, logger);
      FSiteName := ctx.SiteName;
      TCImportOptionsFrame.Clear(true);
      case ctx.DatabaseType of
        dbClassic: begin
          lblGlueStatus.Caption := 'TimeWorks';
          EEExportOptionsFrame.EnableAllowClearingOptions(True);
        end;
        dbGlue: begin
          lblGlueStatus.Caption := 'TimeWorks Plus';
          EEExportOptionsFrame.EnableAllowClearingOptions(False);
        end;
      else
        Assert(false);
      end;
      TCImportOptionsFrame.Init( LoadTCImportOptions(FSettings, CompanyUniquePath(FEvoData.GetClCo), ctx.DatabaseType), ctx, FEvoData.CloneCompanyTable(Value, 'CO_E_D_CODES'){transfers ownership!} );
    except
      on E: Exception do
      begin
        TCImportOptionsFrame.Clear(false);
        lblGlueStatus.Caption := '';
        Logger.StopException;
        Application.HandleException(E);
      end
    end;
  end
  else
    PageControl1.ActivePage := tbshCompanySettings;
end;

procedure TMainFm.HandleSwipeClockConnectionSettingsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveConnectionSettings( SwipeClockFrame.Param, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleEEExportOptionsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveEEExportOptions( EEExportOptionsFrame.Options, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleTCImportOptionsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveTCImportOptions( TCImportOptionsFrame.Options, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  SwipeclockTasks.EditTask(task, Logger, FEvoData, Self);
end;

procedure TMainFm.HandleSmtpConfigChange(Sender: TObject);
begin
  SaveSmtpConfig(SmtpConfigFrame.Config, FSettings, '');
end;

procedure TMainFm.actScheduleEEExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      CheckThatCompanyNameMatches_EEExport('schedule exporting of');
      CheckThatClearingIsOk;
      EEFilterFrame.Check;

      EvoFrame.ForceSavePassword;
      SwipeClockFrame.ForceSavePassword;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewEEExportTask(FEvoData.GetClCo, EeFilterFrame.GetFilter, EEExportOptionsFrame.Options) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleTCImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo and SwipeClockFrame.IsValid and TCImportOptionsFrame.IsValid; //don't need payrolls and batches
end;

procedure TMainFm.actScheduleTCImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      CheckThatCompanyNameMatches_TCImport('schedule importing of');
      EvoFrame.ForceSavePassword;
      SwipeClockFrame.ForceSavePassword;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTCImportTask(FEvoData.GetClCo, TCImportOptionsFrame.Options) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.CheckThatCompanyNameMatches_EEExport(opname: string);
var
  company: TEvoCompanyDef;
begin
  company := FEvoData.GetClCo;
  if not AnsiSameText( trim(FSiteName), trim(company.CUSTOM_COMPANY_NUMBER) ) and
     not AnsiSameText( trim(FSiteName), trim(company.Name) ) then
    if MessageDlg( Format('You are about to %s employee information from %s (%s) company into %s company. Do you want to proceed?',
      [opname, trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER), trim(FSiteName)]), mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Abort;
end;

procedure TMainFm.CheckThatCompanyNameMatches_TCImport(opname: string);
var
  company: TEvoCompanyDef;
begin
  company := FEvoData.GetClCo;
  if not AnsiSameText( trim(FSiteName), trim(company.CUSTOM_COMPANY_NUMBER) ) and
     not AnsiSameText( trim(FSiteName), trim(company.Name) ) then
    if MessageDlg( Format('You are about to %s timeclock information from "%s" company into "%s" (%s) company. Do you want to proceed?',
                      [opname, trim(FSiteName), trim(company.Name), trim(company.CUSTOM_COMPANY_NUMBER)]), mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Abort;
end;

procedure TMainFm.CheckThatClearingIsOk;
var
  r: TAskResult;
begin
  if (EEExportOptionsFrame.Options.AllowClearingOfCardNumber1 or
      EEExportOptionsFrame.Options.AllowClearingOfSupervisor or
      EEExportOptionsFrame.Options.AllowClearingOfWebPassword or
      EEExportOptionsFrame.Options.AllowClearingOfWebLogin)
      and not FNeverWarnOnAllowClearing then
  begin
    r := Ask(Self, 'Clearing of some TimeWorks fields is allowed. Proceed?');
    Repaint;
    if not r.Ok then
      Abort;
    FNeverWarnOnAllowClearing := r.NeverAskAgain;
  end;
end;

procedure TMainFm.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.TabIndex = 3) and SwipeClockFrame.IsValid and not TCImportOptionsFrame.IsInitializedWithParams(SwipeClockFrame.Param) then
  begin
    UnInitPerCompanySettings;
    if FEvoData.CanGetClCo then
      LoadPerCompanySettings(FEvoData.GetClCo);
  end;
end;

procedure TMainFm.HandleEEFilterChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveEEFilter( EeFilterFrame.GetFilter, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleCanEditParameters(var can: boolean);
begin
  can := FEvoData.Connected;
end;

procedure TMainFm.HandleExtAppSettingsChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveExtAppSettings( ExtAppSettingsFrame.Settings, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

end.
