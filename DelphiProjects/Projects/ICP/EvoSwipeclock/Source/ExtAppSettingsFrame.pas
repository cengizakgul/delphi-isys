unit ExtAppSettingsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, swipeclockdecl, PasswordEdit, OptionsBaseFrame;

type
  TExtAppSettingsFrm = class(TOptionsBaseFrm)
    cbConsolidatedDatabase: TCheckBox;
  private
    function GetSettings: TExtAppSettings;
    procedure SetSettings(const Value: TExtAppSettings);
  public
    property Settings: TExtAppSettings read GetSettings write SetSettings;
    procedure Clear(enable: boolean);
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TExtAppSettingsFrm }

procedure TExtAppSettingsFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    cbConsolidatedDatabase.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

function TExtAppSettingsFrm.GetSettings: TExtAppSettings;
begin
  Result.ConsolidatedDatabase := cbConsolidatedDatabase.Checked;
end;

procedure TExtAppSettingsFrm.SetSettings(const Value: TExtAppSettings);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    cbConsolidatedDatabase.Checked := Value.ConsolidatedDatabase;
  finally
    FBlockOnChange := false;
  end;
end;

end.
