
{********************************************************************************************************}
{                                                                                                        }
{                                            XML Data Binding                                            }
{                                                                                                        }
{         Generated on: 2/7/2013 2:54:22 PM                                                              }
{       Generated from: E:\job\IS\integration-release\EvoSwipeclock\2.4\EvoSwipeclock\GetSiteCards.xml   }
{   Settings stored in: E:\job\IS\integration-release\EvoSwipeclock\2.4\EvoSwipeclock\GetSiteCards.xdb   }
{                                                                                                        }
{********************************************************************************************************}

unit bndGetSiteCards;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLResponseType = interface;
  IXMLCardReportType = interface;
  IXMLSiteType = interface;
  IXMLCardsType = interface;
  IXMLCardType = interface;
  IXMLEmployeeType = interface;
  IXMLPunchesType = interface;
  IXMLPunchType = interface;

{ IXMLResponseType }

  IXMLResponseType = interface(IXMLNode)
    ['{43551482-F371-47EB-949D-62017A9C5612}']
    { Property Accessors }
    function Get_CardReport: IXMLCardReportType;
    { Methods & Properties }
    property CardReport: IXMLCardReportType read Get_CardReport;
  end;

{ IXMLCardReportType }

  IXMLCardReportType = interface(IXMLNode)
    ['{E9E88CCB-977D-42B1-8D18-8F549AC54DA4}']
    { Property Accessors }
    function Get_BeginDate: WideString;
    function Get_EndDate: WideString;
    function Get_Site: IXMLSiteType;
    function Get_Cards: IXMLCardsType;
    procedure Set_BeginDate(Value: WideString);
    procedure Set_EndDate(Value: WideString);
    { Methods & Properties }
    property BeginDate: WideString read Get_BeginDate write Set_BeginDate;
    property EndDate: WideString read Get_EndDate write Set_EndDate;
    property Site: IXMLSiteType read Get_Site;
    property Cards: IXMLCardsType read Get_Cards;
  end;

{ IXMLSiteType }

  IXMLSiteType = interface(IXMLNode)
    ['{8F74442A-F744-46C7-8A11-16ED6D9952E6}']
    { Property Accessors }
    function Get_SiteNumber: Integer;
    function Get_ClientTag: WideString;
    function Get_SiteName: WideString;
    function Get_UseMinuteRounding: Integer;
    function Get_Idefinition: WideString;
    function Get_Jdefinition: WideString;
    function Get_Kdefinition: WideString;
    function Get_Xdefinition: WideString;
    function Get_Ydefinition: WideString;
    function Get_Zdefinition: WideString;
    function Get_PayCodes: WideString;
    function Get_PayCodeCategories: WideString;
    function Get_DateTimeUTC: WideString;
    function Get_DateTimeSiteLocal: WideString;
    procedure Set_SiteNumber(Value: Integer);
    procedure Set_ClientTag(Value: WideString);
    procedure Set_SiteName(Value: WideString);
    procedure Set_UseMinuteRounding(Value: Integer);
    procedure Set_Idefinition(Value: WideString);
    procedure Set_Jdefinition(Value: WideString);
    procedure Set_Kdefinition(Value: WideString);
    procedure Set_Xdefinition(Value: WideString);
    procedure Set_Ydefinition(Value: WideString);
    procedure Set_Zdefinition(Value: WideString);
    procedure Set_PayCodes(Value: WideString);
    procedure Set_PayCodeCategories(Value: WideString);
    procedure Set_DateTimeUTC(Value: WideString);
    procedure Set_DateTimeSiteLocal(Value: WideString);
    { Methods & Properties }
    property SiteNumber: Integer read Get_SiteNumber write Set_SiteNumber;
    property ClientTag: WideString read Get_ClientTag write Set_ClientTag;
    property SiteName: WideString read Get_SiteName write Set_SiteName;
    property UseMinuteRounding: Integer read Get_UseMinuteRounding write Set_UseMinuteRounding;
    property Idefinition: WideString read Get_Idefinition write Set_Idefinition;
    property Jdefinition: WideString read Get_Jdefinition write Set_Jdefinition;
    property Kdefinition: WideString read Get_Kdefinition write Set_Kdefinition;
    property Xdefinition: WideString read Get_Xdefinition write Set_Xdefinition;
    property Ydefinition: WideString read Get_Ydefinition write Set_Ydefinition;
    property Zdefinition: WideString read Get_Zdefinition write Set_Zdefinition;
    property PayCodes: WideString read Get_PayCodes write Set_PayCodes;
    property PayCodeCategories: WideString read Get_PayCodeCategories write Set_PayCodeCategories;
    property DateTimeUTC: WideString read Get_DateTimeUTC write Set_DateTimeUTC;
    property DateTimeSiteLocal: WideString read Get_DateTimeSiteLocal write Set_DateTimeSiteLocal;
  end;

{ IXMLCardsType }

  IXMLCardsType = interface(IXMLNodeCollection)
    ['{E7CC72AE-733D-4D0B-9901-2D6A93B71478}']
    { Property Accessors }
    function Get_Card(Index: Integer): IXMLCardType;
    { Methods & Properties }
    function Add: IXMLCardType;
    function Insert(const Index: Integer): IXMLCardType;
    property Card[Index: Integer]: IXMLCardType read Get_Card; default;
  end;

{ IXMLCardType }

  IXMLCardType = interface(IXMLNode)
    ['{F7D002E3-CDA7-47B3-AFF1-35C19FBA30B9}']
    { Property Accessors }
    function Get_Employee: IXMLEmployeeType;
    function Get_Punches: IXMLPunchesType;
    { Methods & Properties }
    property Employee: IXMLEmployeeType read Get_Employee;
    property Punches: IXMLPunchesType read Get_Punches;
  end;

{ IXMLEmployeeType }

  IXMLEmployeeType = interface(IXMLNode)
    ['{B6819B28-833D-427D-89DE-51770D1BA18A}']
    { Property Accessors }
    function Get_FullName: WideString;
    function Get_FirstName: WideString;
    function Get_LastName: WideString;
    function Get_SSN: WideString;
    function Get_EmployeeCode: WideString;
    function Get_HomeDepartment: WideString;
    function Get_HomeLocation: WideString;
    function Get_HomeSupervisor: WideString;
    function Get_CardNumber1: WideString;
    function Get_CardNumber2: WideString;
    function Get_CardNumber3: WideString;
    function Get_Home1: WideString;
    function Get_Home2: WideString;
    function Get_Home3: WideString;
    function Get_PayRate0: WideString;
    function Get_PayRate1: WideString;
    function Get_PayRate2: WideString;
    function Get_PayRate3: WideString;
    procedure Set_FullName(Value: WideString);
    procedure Set_FirstName(Value: WideString);
    procedure Set_LastName(Value: WideString);
    procedure Set_SSN(Value: WideString);
    procedure Set_EmployeeCode(Value: WideString);
    procedure Set_HomeDepartment(Value: WideString);
    procedure Set_HomeLocation(Value: WideString);
    procedure Set_HomeSupervisor(Value: WideString);
    procedure Set_CardNumber1(Value: WideString);
    procedure Set_CardNumber2(Value: WideString);
    procedure Set_CardNumber3(Value: WideString);
    procedure Set_Home1(Value: WideString);
    procedure Set_Home2(Value: WideString);
    procedure Set_Home3(Value: WideString);
    procedure Set_PayRate0(Value: WideString);
    procedure Set_PayRate1(Value: WideString);
    procedure Set_PayRate2(Value: WideString);
    procedure Set_PayRate3(Value: WideString);
    { Methods & Properties }
    property FullName: WideString read Get_FullName write Set_FullName;
    property FirstName: WideString read Get_FirstName write Set_FirstName;
    property LastName: WideString read Get_LastName write Set_LastName;
    property SSN: WideString read Get_SSN write Set_SSN;
    property EmployeeCode: WideString read Get_EmployeeCode write Set_EmployeeCode;
    property HomeDepartment: WideString read Get_HomeDepartment write Set_HomeDepartment;
    property HomeLocation: WideString read Get_HomeLocation write Set_HomeLocation;
    property HomeSupervisor: WideString read Get_HomeSupervisor write Set_HomeSupervisor;
    property CardNumber1: WideString read Get_CardNumber1 write Set_CardNumber1;
    property CardNumber2: WideString read Get_CardNumber2 write Set_CardNumber2;
    property CardNumber3: WideString read Get_CardNumber3 write Set_CardNumber3;
    property Home1: WideString read Get_Home1 write Set_Home1;
    property Home2: WideString read Get_Home2 write Set_Home2;
    property Home3: WideString read Get_Home3 write Set_Home3;
    property PayRate0: WideString read Get_PayRate0 write Set_PayRate0;
    property PayRate1: WideString read Get_PayRate1 write Set_PayRate1;
    property PayRate2: WideString read Get_PayRate2 write Set_PayRate2;
    property PayRate3: WideString read Get_PayRate3 write Set_PayRate3;
  end;

{ IXMLPunchesType }

  IXMLPunchesType = interface(IXMLNodeCollection)
    ['{32412C5C-B735-4AA5-B88D-91689DA81936}']
    { Property Accessors }
    function Get_Punch(Index: Integer): IXMLPunchType;
    { Methods & Properties }
    function Add: IXMLPunchType;
    function Insert(const Index: Integer): IXMLPunchType;
    property Punch[Index: Integer]: IXMLPunchType read Get_Punch; default;
  end;

{ IXMLPunchType }

  IXMLPunchType = interface(IXMLNode)
    ['{DD523305-31AF-4739-8A37-4E6B28875186}']
    { Property Accessors }
    function Get_Id: Integer;
    function Get_PunchDate: WideString;
    function Get_Type_: WideString;
    function Get_InDT: WideString;
    function Get_InUnrounded: WideString;
    function Get_OutDT: WideString;
    function Get_OutUnrounded: WideString;
    function Get_LunchMinutes: Integer;
    function Get_Hours: Double;
    function Get_NonOTHours: Double;
    function Get_OT1Hours: Double;
    function Get_OT1Category: WideString;
    function Get_OT1PayRate: Double;
    function Get_OT2Hours: Double;
    function Get_OT2Category: WideString;
    function Get_OT2PayRate: Double;
    function Get_PayRate: Double;
    function Get_ApplicablePayRate: Double;
    function Get_Category: WideString;
    function Get_I: Double;
    function Get_J: Double;
    function Get_K: Double;
    function Get_X: WideString;
    function Get_Y: WideString;
    function Get_Z: WideString;
    function Get_Pay: Double;
    procedure Set_Id(Value: Integer);
    procedure Set_PunchDate(Value: WideString);
    procedure Set_Type_(Value: WideString);
    procedure Set_InDT(Value: WideString);
    procedure Set_InUnrounded(Value: WideString);
    procedure Set_OutDT(Value: WideString);
    procedure Set_OutUnrounded(Value: WideString);
    procedure Set_LunchMinutes(Value: Integer);
    procedure Set_Hours(Value: Double);
    procedure Set_NonOTHours(Value: Double);
    procedure Set_OT1Hours(Value: Double);
    procedure Set_OT1Category(Value: WideString);
    procedure Set_OT1PayRate(Value: Double);
    procedure Set_OT2Hours(Value: Double);
    procedure Set_OT2Category(Value: WideString);
    procedure Set_OT2PayRate(Value: Double);
    procedure Set_PayRate(Value: Double);
    procedure Set_ApplicablePayRate(Value: Double);
    procedure Set_Category(Value: WideString);
    procedure Set_I(Value: Double);
    procedure Set_J(Value: Double);
    procedure Set_K(Value: Double);
    procedure Set_X(Value: WideString);
    procedure Set_Y(Value: WideString);
    procedure Set_Z(Value: WideString);
    procedure Set_Pay(Value: Double);
    { Methods & Properties }
    property Id: Integer read Get_Id write Set_Id;
    property PunchDate: WideString read Get_PunchDate write Set_PunchDate;
    property Type_: WideString read Get_Type_ write Set_Type_;
    property InDT: WideString read Get_InDT write Set_InDT;
    property InUnrounded: WideString read Get_InUnrounded write Set_InUnrounded;
    property OutDT: WideString read Get_OutDT write Set_OutDT;
    property OutUnrounded: WideString read Get_OutUnrounded write Set_OutUnrounded;
    property LunchMinutes: Integer read Get_LunchMinutes write Set_LunchMinutes;
    property Hours: Double read Get_Hours write Set_Hours;
    property NonOTHours: Double read Get_NonOTHours write Set_NonOTHours;
    property OT1Hours: Double read Get_OT1Hours write Set_OT1Hours;
    property OT1Category: WideString read Get_OT1Category write Set_OT1Category;
    property OT1PayRate: Double read Get_OT1PayRate write Set_OT1PayRate;
    property OT2Hours: Double read Get_OT2Hours write Set_OT2Hours;
    property OT2Category: WideString read Get_OT2Category write Set_OT2Category;
    property OT2PayRate: Double read Get_OT2PayRate write Set_OT2PayRate;
    property PayRate: Double read Get_PayRate write Set_PayRate;
    property ApplicablePayRate: Double read Get_ApplicablePayRate write Set_ApplicablePayRate;
    property Category: WideString read Get_Category write Set_Category;
    property I: Double read Get_I write Set_I;
    property J: Double read Get_J write Set_J;
    property K: Double read Get_K write Set_K;
    property X: WideString read Get_X write Set_X;
    property Y: WideString read Get_Y write Set_Y;
    property Z: WideString read Get_Z write Set_Z;
    property Pay: Double read Get_Pay write Set_Pay;
  end;

{ Forward Decls }

  TXMLResponseType = class;
  TXMLCardReportType = class;
  TXMLSiteType = class;
  TXMLCardsType = class;
  TXMLCardType = class;
  TXMLEmployeeType = class;
  TXMLPunchesType = class;
  TXMLPunchType = class;

{ TXMLResponseType }

  TXMLResponseType = class(TXMLNode, IXMLResponseType)
  protected
    { IXMLResponseType }
    function Get_CardReport: IXMLCardReportType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCardReportType }

  TXMLCardReportType = class(TXMLNode, IXMLCardReportType)
  protected
    { IXMLCardReportType }
    function Get_BeginDate: WideString;
    function Get_EndDate: WideString;
    function Get_Site: IXMLSiteType;
    function Get_Cards: IXMLCardsType;
    procedure Set_BeginDate(Value: WideString);
    procedure Set_EndDate(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSiteType }

  TXMLSiteType = class(TXMLNode, IXMLSiteType)
  protected
    { IXMLSiteType }
    function Get_SiteNumber: Integer;
    function Get_ClientTag: WideString;
    function Get_SiteName: WideString;
    function Get_UseMinuteRounding: Integer;
    function Get_Idefinition: WideString;
    function Get_Jdefinition: WideString;
    function Get_Kdefinition: WideString;
    function Get_Xdefinition: WideString;
    function Get_Ydefinition: WideString;
    function Get_Zdefinition: WideString;
    function Get_PayCodes: WideString;
    function Get_PayCodeCategories: WideString;
    function Get_DateTimeUTC: WideString;
    function Get_DateTimeSiteLocal: WideString;
    procedure Set_SiteNumber(Value: Integer);
    procedure Set_ClientTag(Value: WideString);
    procedure Set_SiteName(Value: WideString);
    procedure Set_UseMinuteRounding(Value: Integer);
    procedure Set_Idefinition(Value: WideString);
    procedure Set_Jdefinition(Value: WideString);
    procedure Set_Kdefinition(Value: WideString);
    procedure Set_Xdefinition(Value: WideString);
    procedure Set_Ydefinition(Value: WideString);
    procedure Set_Zdefinition(Value: WideString);
    procedure Set_PayCodes(Value: WideString);
    procedure Set_PayCodeCategories(Value: WideString);
    procedure Set_DateTimeUTC(Value: WideString);
    procedure Set_DateTimeSiteLocal(Value: WideString);
  end;

{ TXMLCardsType }

  TXMLCardsType = class(TXMLNodeCollection, IXMLCardsType)
  protected
    { IXMLCardsType }
    function Get_Card(Index: Integer): IXMLCardType;
    function Add: IXMLCardType;
    function Insert(const Index: Integer): IXMLCardType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCardType }

  TXMLCardType = class(TXMLNode, IXMLCardType)
  protected
    { IXMLCardType }
    function Get_Employee: IXMLEmployeeType;
    function Get_Punches: IXMLPunchesType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmployeeType }

  TXMLEmployeeType = class(TXMLNode, IXMLEmployeeType)
  protected
    { IXMLEmployeeType }
    function Get_FullName: WideString;
    function Get_FirstName: WideString;
    function Get_LastName: WideString;
    function Get_SSN: WideString;
    function Get_EmployeeCode: WideString;
    function Get_HomeDepartment: WideString;
    function Get_HomeLocation: WideString;
    function Get_HomeSupervisor: WideString;
    function Get_CardNumber1: WideString;
    function Get_CardNumber2: WideString;
    function Get_CardNumber3: WideString;
    function Get_Home1: WideString;
    function Get_Home2: WideString;
    function Get_Home3: WideString;
    function Get_PayRate0: WideString;
    function Get_PayRate1: WideString;
    function Get_PayRate2: WideString;
    function Get_PayRate3: WideString;
    procedure Set_FullName(Value: WideString);
    procedure Set_FirstName(Value: WideString);
    procedure Set_LastName(Value: WideString);
    procedure Set_SSN(Value: WideString);
    procedure Set_EmployeeCode(Value: WideString);
    procedure Set_HomeDepartment(Value: WideString);
    procedure Set_HomeLocation(Value: WideString);
    procedure Set_HomeSupervisor(Value: WideString);
    procedure Set_CardNumber1(Value: WideString);
    procedure Set_CardNumber2(Value: WideString);
    procedure Set_CardNumber3(Value: WideString);
    procedure Set_Home1(Value: WideString);
    procedure Set_Home2(Value: WideString);
    procedure Set_Home3(Value: WideString);
    procedure Set_PayRate0(Value: WideString);
    procedure Set_PayRate1(Value: WideString);
    procedure Set_PayRate2(Value: WideString);
    procedure Set_PayRate3(Value: WideString);
  end;

{ TXMLPunchesType }

  TXMLPunchesType = class(TXMLNodeCollection, IXMLPunchesType)
  protected
    { IXMLPunchesType }
    function Get_Punch(Index: Integer): IXMLPunchType;
    function Add: IXMLPunchType;
    function Insert(const Index: Integer): IXMLPunchType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPunchType }

  TXMLPunchType = class(TXMLNode, IXMLPunchType)
  protected
    { IXMLPunchType }
    function Get_Id: Integer;
    function Get_PunchDate: WideString;
    function Get_Type_: WideString;
    function Get_InDT: WideString;
    function Get_InUnrounded: WideString;
    function Get_OutDT: WideString;
    function Get_OutUnrounded: WideString;
    function Get_LunchMinutes: Integer;
    function Get_Hours: Double;
    function Get_NonOTHours: Double;
    function Get_OT1Hours: Double;
    function Get_OT1Category: WideString;
    function Get_OT1PayRate: Double;
    function Get_OT2Hours: Double;
    function Get_OT2Category: WideString;
    function Get_OT2PayRate: Double;
    function Get_PayRate: Double;
    function Get_ApplicablePayRate: Double;
    function Get_Category: WideString;
    function Get_I: Double;
    function Get_J: Double;
    function Get_K: Double;
    function Get_X: WideString;
    function Get_Y: WideString;
    function Get_Z: WideString;
    function Get_Pay: Double;
    procedure Set_Id(Value: Integer);
    procedure Set_PunchDate(Value: WideString);
    procedure Set_Type_(Value: WideString);
    procedure Set_InDT(Value: WideString);
    procedure Set_InUnrounded(Value: WideString);
    procedure Set_OutDT(Value: WideString);
    procedure Set_OutUnrounded(Value: WideString);
    procedure Set_LunchMinutes(Value: Integer);
    procedure Set_Hours(Value: Double);
    procedure Set_NonOTHours(Value: Double);
    procedure Set_OT1Hours(Value: Double);
    procedure Set_OT1Category(Value: WideString);
    procedure Set_OT1PayRate(Value: Double);
    procedure Set_OT2Hours(Value: Double);
    procedure Set_OT2Category(Value: WideString);
    procedure Set_OT2PayRate(Value: Double);
    procedure Set_PayRate(Value: Double);
    procedure Set_ApplicablePayRate(Value: Double);
    procedure Set_Category(Value: WideString);
    procedure Set_I(Value: Double);
    procedure Set_J(Value: Double);
    procedure Set_K(Value: Double);
    procedure Set_X(Value: WideString);
    procedure Set_Y(Value: WideString);
    procedure Set_Z(Value: WideString);
    procedure Set_Pay(Value: Double);
  end;

{ Global Functions }

function Getresponse(Doc: IXMLDocument): IXMLResponseType;
function Loadresponse(const FileName: WideString): IXMLResponseType;
function Newresponse: IXMLResponseType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getresponse(Doc: IXMLDocument): IXMLResponseType;
begin
  Result := Doc.GetDocBinding('response', TXMLResponseType, TargetNamespace) as IXMLResponseType;
end;

function Loadresponse(const FileName: WideString): IXMLResponseType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('response', TXMLResponseType, TargetNamespace) as IXMLResponseType;
end;

function Newresponse: IXMLResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('response', TXMLResponseType, TargetNamespace) as IXMLResponseType;
end;

{ TXMLResponseType }

procedure TXMLResponseType.AfterConstruction;
begin
  RegisterChildNode('CardReport', TXMLCardReportType);
  inherited;
end;

function TXMLResponseType.Get_CardReport: IXMLCardReportType;
begin
  Result := ChildNodes['CardReport'] as IXMLCardReportType;
end;

{ TXMLCardReportType }

procedure TXMLCardReportType.AfterConstruction;
begin
  RegisterChildNode('Site', TXMLSiteType);
  RegisterChildNode('Cards', TXMLCardsType);
  inherited;
end;

function TXMLCardReportType.Get_BeginDate: WideString;
begin
  Result := ChildNodes['BeginDate'].Text;
end;

procedure TXMLCardReportType.Set_BeginDate(Value: WideString);
begin
  ChildNodes['BeginDate'].NodeValue := Value;
end;

function TXMLCardReportType.Get_EndDate: WideString;
begin
  Result := ChildNodes['EndDate'].Text;
end;

procedure TXMLCardReportType.Set_EndDate(Value: WideString);
begin
  ChildNodes['EndDate'].NodeValue := Value;
end;

function TXMLCardReportType.Get_Site: IXMLSiteType;
begin
  Result := ChildNodes['Site'] as IXMLSiteType;
end;

function TXMLCardReportType.Get_Cards: IXMLCardsType;
begin
  Result := ChildNodes['Cards'] as IXMLCardsType;
end;

{ TXMLSiteType }

function TXMLSiteType.Get_SiteNumber: Integer;
begin
  Result := ChildNodes['SiteNumber'].NodeValue;
end;

procedure TXMLSiteType.Set_SiteNumber(Value: Integer);
begin
  ChildNodes['SiteNumber'].NodeValue := Value;
end;

function TXMLSiteType.Get_ClientTag: WideString;
begin
  Result := ChildNodes['ClientTag'].Text;
end;

procedure TXMLSiteType.Set_ClientTag(Value: WideString);
begin
  ChildNodes['ClientTag'].NodeValue := Value;
end;

function TXMLSiteType.Get_SiteName: WideString;
begin
  Result := ChildNodes['SiteName'].Text;
end;

procedure TXMLSiteType.Set_SiteName(Value: WideString);
begin
  ChildNodes['SiteName'].NodeValue := Value;
end;

function TXMLSiteType.Get_UseMinuteRounding: Integer;
begin
  Result := ChildNodes['UseMinuteRounding'].NodeValue;
end;

procedure TXMLSiteType.Set_UseMinuteRounding(Value: Integer);
begin
  ChildNodes['UseMinuteRounding'].NodeValue := Value;
end;

function TXMLSiteType.Get_Idefinition: WideString;
begin
  Result := ChildNodes['Idefinition'].Text;
end;

procedure TXMLSiteType.Set_Idefinition(Value: WideString);
begin
  ChildNodes['Idefinition'].NodeValue := Value;
end;

function TXMLSiteType.Get_Jdefinition: WideString;
begin
  Result := ChildNodes['Jdefinition'].Text;
end;

procedure TXMLSiteType.Set_Jdefinition(Value: WideString);
begin
  ChildNodes['Jdefinition'].NodeValue := Value;
end;

function TXMLSiteType.Get_Kdefinition: WideString;
begin
  Result := ChildNodes['Kdefinition'].Text;
end;

procedure TXMLSiteType.Set_Kdefinition(Value: WideString);
begin
  ChildNodes['Kdefinition'].NodeValue := Value;
end;

function TXMLSiteType.Get_Xdefinition: WideString;
begin
  Result := ChildNodes['Xdefinition'].Text;
end;

procedure TXMLSiteType.Set_Xdefinition(Value: WideString);
begin
  ChildNodes['Xdefinition'].NodeValue := Value;
end;

function TXMLSiteType.Get_Ydefinition: WideString;
begin
  Result := ChildNodes['Ydefinition'].Text;
end;

procedure TXMLSiteType.Set_Ydefinition(Value: WideString);
begin
  ChildNodes['Ydefinition'].NodeValue := Value;
end;

function TXMLSiteType.Get_Zdefinition: WideString;
begin
  Result := ChildNodes['Zdefinition'].Text;
end;

procedure TXMLSiteType.Set_Zdefinition(Value: WideString);
begin
  ChildNodes['Zdefinition'].NodeValue := Value;
end;

function TXMLSiteType.Get_PayCodes: WideString;
begin
  Result := ChildNodes['PayCodes'].Text;
end;

procedure TXMLSiteType.Set_PayCodes(Value: WideString);
begin
  ChildNodes['PayCodes'].NodeValue := Value;
end;

function TXMLSiteType.Get_PayCodeCategories: WideString;
begin
  Result := ChildNodes['PayCodeCategories'].Text;
end;

procedure TXMLSiteType.Set_PayCodeCategories(Value: WideString);
begin
  ChildNodes['PayCodeCategories'].NodeValue := Value;
end;

function TXMLSiteType.Get_DateTimeUTC: WideString;
begin
  Result := ChildNodes['DateTimeUTC'].Text;
end;

procedure TXMLSiteType.Set_DateTimeUTC(Value: WideString);
begin
  ChildNodes['DateTimeUTC'].NodeValue := Value;
end;

function TXMLSiteType.Get_DateTimeSiteLocal: WideString;
begin
  Result := ChildNodes['DateTimeSiteLocal'].Text;
end;

procedure TXMLSiteType.Set_DateTimeSiteLocal(Value: WideString);
begin
  ChildNodes['DateTimeSiteLocal'].NodeValue := Value;
end;

{ TXMLCardsType }

procedure TXMLCardsType.AfterConstruction;
begin
  RegisterChildNode('Card', TXMLCardType);
  ItemTag := 'Card';
  ItemInterface := IXMLCardType;
  inherited;
end;

function TXMLCardsType.Get_Card(Index: Integer): IXMLCardType;
begin
  Result := List[Index] as IXMLCardType;
end;

function TXMLCardsType.Add: IXMLCardType;
begin
  Result := AddItem(-1) as IXMLCardType;
end;

function TXMLCardsType.Insert(const Index: Integer): IXMLCardType;
begin
  Result := AddItem(Index) as IXMLCardType;
end;

{ TXMLCardType }

procedure TXMLCardType.AfterConstruction;
begin
  RegisterChildNode('Employee', TXMLEmployeeType);
  RegisterChildNode('Punches', TXMLPunchesType);
  inherited;
end;

function TXMLCardType.Get_Employee: IXMLEmployeeType;
begin
  Result := ChildNodes['Employee'] as IXMLEmployeeType;
end;

function TXMLCardType.Get_Punches: IXMLPunchesType;
begin
  Result := ChildNodes['Punches'] as IXMLPunchesType;
end;

{ TXMLEmployeeType }

function TXMLEmployeeType.Get_FullName: WideString;
begin
  Result := ChildNodes['FullName'].Text;
end;

procedure TXMLEmployeeType.Set_FullName(Value: WideString);
begin
  ChildNodes['FullName'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_FirstName: WideString;
begin
  Result := ChildNodes['FirstName'].Text;
end;

procedure TXMLEmployeeType.Set_FirstName(Value: WideString);
begin
  ChildNodes['FirstName'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_LastName: WideString;
begin
  Result := ChildNodes['LastName'].Text;
end;

procedure TXMLEmployeeType.Set_LastName(Value: WideString);
begin
  ChildNodes['LastName'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_SSN: WideString;
begin
  Result := ChildNodes['SSN'].Text;
end;

procedure TXMLEmployeeType.Set_SSN(Value: WideString);
begin
  ChildNodes['SSN'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_EmployeeCode: WideString;
begin
  Result := ChildNodes['EmployeeCode'].Text;
end;

procedure TXMLEmployeeType.Set_EmployeeCode(Value: WideString);
begin
  ChildNodes['EmployeeCode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_HomeDepartment: WideString;
begin
  Result := ChildNodes['HomeDepartment'].Text;
end;

procedure TXMLEmployeeType.Set_HomeDepartment(Value: WideString);
begin
  ChildNodes['HomeDepartment'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_HomeLocation: WideString;
begin
  Result := ChildNodes['HomeLocation'].Text;
end;

procedure TXMLEmployeeType.Set_HomeLocation(Value: WideString);
begin
  ChildNodes['HomeLocation'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_HomeSupervisor: WideString;
begin
  Result := ChildNodes['HomeSupervisor'].Text;
end;

procedure TXMLEmployeeType.Set_HomeSupervisor(Value: WideString);
begin
  ChildNodes['HomeSupervisor'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_CardNumber1: WideString;
begin
  Result := ChildNodes['CardNumber1'].Text;
end;

procedure TXMLEmployeeType.Set_CardNumber1(Value: WideString);
begin
  ChildNodes['CardNumber1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_CardNumber2: WideString;
begin
  Result := ChildNodes['CardNumber2'].Text;
end;

procedure TXMLEmployeeType.Set_CardNumber2(Value: WideString);
begin
  ChildNodes['CardNumber2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_CardNumber3: WideString;
begin
  Result := ChildNodes['CardNumber3'].Text;
end;

procedure TXMLEmployeeType.Set_CardNumber3(Value: WideString);
begin
  ChildNodes['CardNumber3'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Home1: WideString;
begin
  Result := ChildNodes['Home1'].Text;
end;

procedure TXMLEmployeeType.Set_Home1(Value: WideString);
begin
  ChildNodes['Home1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Home2: WideString;
begin
  Result := ChildNodes['Home2'].Text;
end;

procedure TXMLEmployeeType.Set_Home2(Value: WideString);
begin
  ChildNodes['Home2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Home3: WideString;
begin
  Result := ChildNodes['Home3'].Text;
end;

procedure TXMLEmployeeType.Set_Home3(Value: WideString);
begin
  ChildNodes['Home3'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_PayRate0: WideString;
begin
  Result := ChildNodes['PayRate0'].Text;
end;

procedure TXMLEmployeeType.Set_PayRate0(Value: WideString);
begin
  ChildNodes['PayRate0'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_PayRate1: WideString;
begin
  Result := ChildNodes['PayRate1'].Text;
end;

procedure TXMLEmployeeType.Set_PayRate1(Value: WideString);
begin
  ChildNodes['PayRate1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_PayRate2: WideString;
begin
  Result := ChildNodes['PayRate2'].Text;
end;

procedure TXMLEmployeeType.Set_PayRate2(Value: WideString);
begin
  ChildNodes['PayRate2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_PayRate3: WideString;
begin
  Result := ChildNodes['PayRate3'].Text;
end;

procedure TXMLEmployeeType.Set_PayRate3(Value: WideString);
begin
  ChildNodes['PayRate3'].NodeValue := Value;
end;

{ TXMLPunchesType }

procedure TXMLPunchesType.AfterConstruction;
begin
  RegisterChildNode('Punch', TXMLPunchType);
  ItemTag := 'Punch';
  ItemInterface := IXMLPunchType;
  inherited;
end;

function TXMLPunchesType.Get_Punch(Index: Integer): IXMLPunchType;
begin
  Result := List[Index] as IXMLPunchType;
end;

function TXMLPunchesType.Add: IXMLPunchType;
begin
  Result := AddItem(-1) as IXMLPunchType;
end;

function TXMLPunchesType.Insert(const Index: Integer): IXMLPunchType;
begin
  Result := AddItem(Index) as IXMLPunchType;
end;

{ TXMLPunchType }

function TXMLPunchType.Get_Id: Integer;
begin
  Result := ChildNodes['id'].NodeValue;
end;

procedure TXMLPunchType.Set_Id(Value: Integer);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLPunchType.Get_PunchDate: WideString;
begin
  Result := ChildNodes['PunchDate'].Text;
end;

procedure TXMLPunchType.Set_PunchDate(Value: WideString);
begin
  ChildNodes['PunchDate'].NodeValue := Value;
end;

function TXMLPunchType.Get_Type_: WideString;
begin
  Result := ChildNodes['Type'].Text;
end;

procedure TXMLPunchType.Set_Type_(Value: WideString);
begin
  ChildNodes['Type'].NodeValue := Value;
end;

function TXMLPunchType.Get_InDT: WideString;
begin
  Result := ChildNodes['InDT'].Text;
end;

procedure TXMLPunchType.Set_InDT(Value: WideString);
begin
  ChildNodes['InDT'].NodeValue := Value;
end;

function TXMLPunchType.Get_InUnrounded: WideString;
begin
  Result := ChildNodes['InUnrounded'].Text;
end;

procedure TXMLPunchType.Set_InUnrounded(Value: WideString);
begin
  ChildNodes['InUnrounded'].NodeValue := Value;
end;

function TXMLPunchType.Get_OutDT: WideString;
begin
  Result := ChildNodes['OutDT'].Text;
end;

procedure TXMLPunchType.Set_OutDT(Value: WideString);
begin
  ChildNodes['OutDT'].NodeValue := Value;
end;

function TXMLPunchType.Get_OutUnrounded: WideString;
begin
  Result := ChildNodes['OutUnrounded'].Text;
end;

procedure TXMLPunchType.Set_OutUnrounded(Value: WideString);
begin
  ChildNodes['OutUnrounded'].NodeValue := Value;
end;

function TXMLPunchType.Get_LunchMinutes: Integer;
begin
  Result := ChildNodes['LunchMinutes'].NodeValue;
end;

procedure TXMLPunchType.Set_LunchMinutes(Value: Integer);
begin
  ChildNodes['LunchMinutes'].NodeValue := Value;
end;

function TXMLPunchType.Get_Hours: Double;
begin
  Result := ChildNodes['Hours'].NodeValue;
end;

procedure TXMLPunchType.Set_Hours(Value: Double);
begin
  ChildNodes['Hours'].NodeValue := Value;
end;

function TXMLPunchType.Get_NonOTHours: Double;
begin
  Result := ChildNodes['NonOTHours'].NodeValue;
end;

procedure TXMLPunchType.Set_NonOTHours(Value: Double);
begin
  ChildNodes['NonOTHours'].NodeValue := Value;
end;

function TXMLPunchType.Get_OT1Hours: Double;
begin
  Result := ChildNodes['OT1Hours'].NodeValue;
end;

procedure TXMLPunchType.Set_OT1Hours(Value: Double);
begin
  ChildNodes['OT1Hours'].NodeValue := Value;
end;

function TXMLPunchType.Get_OT1Category: WideString;
begin
  Result := ChildNodes['OT1Category'].Text;
end;

procedure TXMLPunchType.Set_OT1Category(Value: WideString);
begin
  ChildNodes['OT1Category'].NodeValue := Value;
end;

function TXMLPunchType.Get_OT1PayRate: Double;
begin
  Result := ChildNodes['OT1PayRate'].NodeValue;
end;

procedure TXMLPunchType.Set_OT1PayRate(Value: Double);
begin
  ChildNodes['OT1PayRate'].NodeValue := Value;
end;

function TXMLPunchType.Get_OT2Hours: Double;
begin
  Result := ChildNodes['OT2Hours'].NodeValue;
end;

procedure TXMLPunchType.Set_OT2Hours(Value: Double);
begin
  ChildNodes['OT2Hours'].NodeValue := Value;
end;

function TXMLPunchType.Get_OT2Category: WideString;
begin
  Result := ChildNodes['OT2Category'].Text;
end;

procedure TXMLPunchType.Set_OT2Category(Value: WideString);
begin
  ChildNodes['OT2Category'].NodeValue := Value;
end;

function TXMLPunchType.Get_OT2PayRate: Double;
begin
  Result := ChildNodes['OT2PayRate'].NodeValue;
end;

procedure TXMLPunchType.Set_OT2PayRate(Value: Double);
begin
  ChildNodes['OT2PayRate'].NodeValue := Value;
end;

function TXMLPunchType.Get_PayRate: Double;
begin
  Result := ChildNodes['PayRate'].NodeValue;
end;

procedure TXMLPunchType.Set_PayRate(Value: Double);
begin
  ChildNodes['PayRate'].NodeValue := Value;
end;

function TXMLPunchType.Get_ApplicablePayRate: Double;
begin
  Result := ChildNodes['ApplicablePayRate'].NodeValue;
end;

procedure TXMLPunchType.Set_ApplicablePayRate(Value: Double);
begin
  ChildNodes['ApplicablePayRate'].NodeValue := Value;
end;

function TXMLPunchType.Get_Category: WideString;
begin
  Result := ChildNodes['Category'].Text;
end;

procedure TXMLPunchType.Set_Category(Value: WideString);
begin
  ChildNodes['Category'].NodeValue := Value;
end;

function TXMLPunchType.Get_I: Double;
begin
  Result := ChildNodes['I'].NodeValue;
end;

procedure TXMLPunchType.Set_I(Value: Double);
begin
  ChildNodes['I'].NodeValue := Value;
end;

function TXMLPunchType.Get_J: Double;
begin
  Result := ChildNodes['J'].NodeValue;
end;

procedure TXMLPunchType.Set_J(Value: Double);
begin
  ChildNodes['J'].NodeValue := Value;
end;

function TXMLPunchType.Get_K: Double;
begin
  Result := ChildNodes['K'].NodeValue;
end;

procedure TXMLPunchType.Set_K(Value: Double);
begin
  ChildNodes['K'].NodeValue := Value;
end;

function TXMLPunchType.Get_X: WideString;
begin
  Result := ChildNodes['X'].Text;
end;

procedure TXMLPunchType.Set_X(Value: WideString);
begin
  ChildNodes['X'].NodeValue := Value;
end;

function TXMLPunchType.Get_Y: WideString;
begin
  Result := ChildNodes['Y'].Text;
end;

procedure TXMLPunchType.Set_Y(Value: WideString);
begin
  ChildNodes['Y'].NodeValue := Value;
end;

function TXMLPunchType.Get_Z: WideString;
begin
  Result := ChildNodes['Z'].Text;
end;

procedure TXMLPunchType.Set_Z(Value: WideString);
begin
  ChildNodes['Z'].NodeValue := Value;
end;

function TXMLPunchType.Get_Pay: Double;
begin
  Result := ChildNodes['Pay'].NodeValue;
end;

procedure TXMLPunchType.Set_Pay(Value: Double);
begin
  ChildNodes['Pay'].NodeValue := Value;
end;

end.