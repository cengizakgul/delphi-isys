unit EEExportOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, swipeclockdecl, OptionsBaseFrame;

type
  TEEExportOptionsFrm = class(TOptionsBaseFrm)
    cbExportSSN: TCheckBox;
    cbExportRates: TCheckBox;
    cbExportDepartment: TCheckBox;
    cbExportBranch: TCheckBox;
    gbAllowClearing: TGroupBox;
    cbAllowClearingOfCardNumber1: TCheckBox;
    cbAllowClearingOfSupervisor: TCheckBox;
    cbAllowClearingOfWebPassword: TCheckBox;
    cbAllowClearingOfWebLogin: TCheckBox;
    gbClockNumbers: TGroupBox;
    chbTimeClockNumber: TCheckBox;
    chbEeAddInfo: TCheckBox;
    chbEeCode: TCheckBox;
  private
    function GetOptions: TEEExportOptions;
    procedure SetOptions(const Value: TEEExportOptions);
  public
    property Options: TEEExportOptions read GetOptions write SetOptions;
    procedure Clear(enable: boolean);
    procedure EnableAllowClearingOptions(enable: boolean);
  end;

implementation

{$R *.dfm}

uses
  gdycommon;

{ TEEExportOptionsFrm }

procedure TEEExportOptionsFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    cbExportSSN.Checked := false;
    cbExportRates.Checked := false;
    cbExportBranch.Checked := false;
    cbExportDepartment.Checked := false;
    cbAllowClearingOfCardNumber1.Checked := false;
    cbAllowClearingOfSupervisor.Checked := false;
    cbAllowClearingOfWebPassword.Checked := false;
    cbAllowClearingOfWebLogin.Checked := false;
    chbTimeClockNumber.Checked := false;
    chbEeAddInfo.Checked := false;
    chbEeCode.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

procedure TEEExportOptionsFrm.EnableAllowClearingOptions(enable: boolean);
begin
  FBlockOnChange := true;
  try
    gbAllowClearing.Visible := enable;
    gbClockNumbers.Visible := not enable;

    if not enable then
    begin
      cbAllowClearingOfCardNumber1.Checked := false;
      cbAllowClearingOfSupervisor.Checked := false;
      cbAllowClearingOfWebPassword.Checked := false;
      cbAllowClearingOfWebLogin.Checked := false;
    end
    else begin
      chbTimeClockNumber.Checked := false;
      chbEeAddInfo.Checked := false;
      chbEeCode.Checked := false;
    end;  
  finally
    FBlockOnChange := false;
  end;
end;

function TEEExportOptionsFrm.GetOptions: TEEExportOptions;
begin
  Result.ExportSSN := cbExportSSN.Checked;
  Result.ExportRates := cbExportRates.Checked;
  Result.ExportBranch := cbExportBranch.Checked;
  Result.ExportDepartment := cbExportDepartment.Checked;
  Result.AllowClearingOfCardNumber1 := cbAllowClearingOfCardNumber1.Checked;
  Result.AllowClearingOfSupervisor := cbAllowClearingOfSupervisor.Checked;
  Result.AllowClearingOfWebPassword := cbAllowClearingOfWebPassword.Checked;
  Result.AllowClearingOfWebLogin := cbAllowClearingOfWebLogin.Checked;
  Result.UseEeCode := chbEeCode.Checked;
  Result.UseEeAddInfo := chbEeAddInfo.Checked;
  Result.UseTimeClockNumber := chbTimeClockNumber.Checked;
end;

procedure TEEExportOptionsFrm.SetOptions(const Value: TEEExportOptions);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);

    cbExportSSN.Checked := Value.ExportSSN;
    cbExportRates.Checked := Value.ExportRates;
    cbExportBranch.Checked := Value.ExportBranch;
    cbExportDepartment.Checked := Value.ExportDepartment;
    cbAllowClearingOfCardNumber1.Checked := Value.AllowClearingOfCardNumber1;
    cbAllowClearingOfSupervisor.Checked := Value.AllowClearingOfSupervisor;
    cbAllowClearingOfWebPassword.Checked := Value.AllowClearingOfWebPassword;
    cbAllowClearingOfWebLogin.Checked := Value.AllowClearingOfWebLogin;
    chbTimeClockNumber.Checked := Value.UseTimeClockNumber;
    chbEeAddInfo.Checked := Value.UseEeAddInfo;
    chbEeCode.Checked := Value.UseEeCode;
  finally
    FBlockOnChange := false;
  end;
end;

end.
