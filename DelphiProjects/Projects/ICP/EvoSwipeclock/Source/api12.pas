// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\IS\Accurev\Dev1\DelphiProjects\Projects\ICP\EvoSwipeclock\Source\api12.wsdl
// Encoding : utf-8
// Version  : 1.0
// (8/19/2013 1:42:31 PM - 1.33.2.5)
// ************************************************************************ //

unit api12;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:Employees       - "http://www.w3.org/2001/XMLSchema"
  // !:long            - "http://www.w3.org/2001/XMLSchema"
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"
  // !:string          - "http://mc2cs.com/"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"

  ValueBagItem         = class;                 { "http://mc2cs.com/" }
  ValueBag             = class;                 { "http://mc2cs.com/"[A] }
  Date                 = class;                 { "http://mc2cs.com/" }
  ApiResult            = class;                 { "http://mc2cs.com/" }
  GetUnfinalizedPayrollResult = class;          { "http://mc2cs.com/" }
  GetFinalizedPayrollResult = class;            { "http://mc2cs.com/" }
  ListOfFinalizedPayrollsResult = class;        { "http://mc2cs.com/" }
  EEIDSubmitResult     = class;                 { "http://mc2cs.com/" }
  CreateSessionResult  = class;                 { "http://mc2cs.com/" }
  StringApiResult      = class;                 { "http://mc2cs.com/" }
  EmployeeSelectorCustomCriterion = class;      { "http://mc2cs.com/" }
  EmployeeSelectorState = class;                { "http://mc2cs.com/" }
  RuleDefinition       = class;                 { "http://mc2cs.com/" }
  RuleSet              = class;                 { "http://mc2cs.com/" }
  GetSiteRuleSetApiResult = class;              { "http://mc2cs.com/" }
  FieldToUpdate        = class;                 { "http://mc2cs.com/" }
  EmployeeFieldsToUpdate = class;               { "http://mc2cs.com/" }
  ValidationFailure    = class;                 { "http://mc2cs.com/" }
  EmployeeUpdateFailures = class;               { "http://mc2cs.com/" }
  UpdateEmployeeFieldsApiResult = class;        { "http://mc2cs.com/" }
  GetActivityFileApiResult = class;             { "http://mc2cs.com/" }

  { "http://mc2cs.com/" }
  Selections = (NoEmployees, AllEmployees, SpecificEmployees, GroupOfEmployees, CustomCriteria, FilterScript, AllActiveEmployees);

  { "http://mc2cs.com/" }
  EmployeeIdentifierField = (EMPLOYEECODE, UNIQUEID);

  { "http://mc2cs.com/" }
  SecondaryIdentifierField = (NOTUSED, DEPARTMENT, LOCATION, SUPERVISOR);

  SubmissionResults =  type WideString;      { "http://mc2cs.com/" }
  PayPeriods      =  type WideString;      { "http://mc2cs.com/" }
  FinalizedPayrollDocument =  type WideString;      { "http://mc2cs.com/" }
  UnfinalizedPayrollDocument =  type WideString;      { "http://mc2cs.com/" }
  XmlConfiguration =  type WideString;      { "http://mc2cs.com/" }
  Employees = type WideString;

  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  ValueBagItem = class(TRemotable)
  private
    Fname: WideString;
    Ftype_: WideString;
  published
    property name: WideString read Fname write Fname stored AS_ATTRIBUTE;
    property type_: WideString read Ftype_ write Ftype_ stored AS_ATTRIBUTE;
  end;

  Item       = array of ValueBagItem;           { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // Serializtn: [xoInlineArrays]
  // ************************************************************************ //
  ValueBag = class(TRemotable)
  private
    FItem: Item;
  public
    constructor Create; override;
    destructor Destroy; override;
    function   GetValueBagItemArray(Index: Integer): ValueBagItem;
    function   GetValueBagItemArrayLength: Integer;
    property   ValueBagItemArray[Index: Integer]: ValueBagItem read GetValueBagItemArray; default;
    property   Len: Integer read GetValueBagItemArrayLength;
  published
    property Item: Item read FItem write FItem;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  Date = class(TRemotable)
  private
  published
  end;

  ArrayOfLong = array of Int64;                 { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  ApiResult = class(TRemotable)
  private
    FComments: WideString;
    FFailureReason: WideString;
    FSuccess: Boolean;
  published
    property Comments: WideString read FComments write FComments;
    property FailureReason: WideString read FFailureReason write FFailureReason;
    property Success: Boolean read FSuccess write FSuccess;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  GetUnfinalizedPayrollResult = class(ApiResult)
  private
    FUnfinalizedPayrollDocument: UnfinalizedPayrollDocument;
  published
    property UnfinalizedPayrollDocument: UnfinalizedPayrollDocument read FUnfinalizedPayrollDocument write FUnfinalizedPayrollDocument;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  GetFinalizedPayrollResult = class(ApiResult)
  private
    FFinalizedPayrollDocument: FinalizedPayrollDocument;
  published
    property FinalizedPayrollDocument: FinalizedPayrollDocument read FFinalizedPayrollDocument write FFinalizedPayrollDocument;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  ListOfFinalizedPayrollsResult = class(ApiResult)
  private
    FPayPeriods: PayPeriods;
  published
    property PayPeriods: PayPeriods read FPayPeriods write FPayPeriods;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  EEIDSubmitResult = class(ApiResult)
  private
    FSubmissionResults: SubmissionResults;
  published
    property SubmissionResults: SubmissionResults read FSubmissionResults write FSubmissionResults;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  CreateSessionResult = class(ApiResult)
  private
    FSessionID: WideString;
  published
    property SessionID: WideString read FSessionID write FSessionID;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  StringApiResult = class(ApiResult)
  private
    FStringResult: WideString;
  published
    property StringResult: WideString read FStringResult write FStringResult;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  EmployeeSelectorCustomCriterion = class(TRemotable)
  private
    FFriendlyName: WideString;
    FJoiner: WideString;
    FOperand: WideString;
    FOperator: WideString;
    FVariableName: WideString;
  published
    property FriendlyName: WideString read FFriendlyName write FFriendlyName stored AS_ATTRIBUTE;
    property Joiner: WideString read FJoiner write FJoiner stored AS_ATTRIBUTE;
    property Operand: WideString read FOperand write FOperand stored AS_ATTRIBUTE;
    property Operator: WideString read FOperator write FOperator stored AS_ATTRIBUTE;
    property VariableName: WideString read FVariableName write FVariableName stored AS_ATTRIBUTE;
  end;

  ArrayOfEmployeeSelectorCustomCriterion = array of EmployeeSelectorCustomCriterion;   { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  EmployeeSelectorState = class(TRemotable)
  private
    FCustomCriteria: ArrayOfEmployeeSelectorCustomCriterion;
    FEmployees: ArrayOfLong;
    FFilterScript: WideString;
    FGroupName: WideString;
    FSelection: Selections;
    FExcludeActiveEmployees: Boolean;
    FExcludeInactiveEmployees: Boolean;
  public
    destructor Destroy; override;
  published
    property CustomCriteria: ArrayOfEmployeeSelectorCustomCriterion read FCustomCriteria write FCustomCriteria;
    property Employees: ArrayOfLong read FEmployees write FEmployees;
    property FilterScript: WideString read FFilterScript write FFilterScript;
    property GroupName: WideString read FGroupName write FGroupName;
    property Selection: Selections read FSelection write FSelection;
    property ExcludeActiveEmployees: Boolean read FExcludeActiveEmployees write FExcludeActiveEmployees stored AS_ATTRIBUTE;
    property ExcludeInactiveEmployees: Boolean read FExcludeInactiveEmployees write FExcludeInactiveEmployees stored AS_ATTRIBUTE;
  end;


{$WARNINGS OFF} //ClassName causes "Redeclaration of 'ClassName' hides a member in the base class"
  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  RuleDefinition = class(TRemotable)
  private
    FConfiguration: XmlConfiguration;
    FEmpSelectorConfiguration: EmployeeSelectorState;
    FEnableOnOrAfterDate: Date;
    FXmlConfiguration: XmlConfiguration;
    FDescription: WideString;
    FClassName: WideString;
    FClassRevision: Integer;
  public
    destructor Destroy; override;
  published
    property Configuration: XmlConfiguration read FConfiguration write FConfiguration;
    property EmpSelectorConfiguration: EmployeeSelectorState read FEmpSelectorConfiguration write FEmpSelectorConfiguration;
    property EnableOnOrAfterDate: Date read FEnableOnOrAfterDate write FEnableOnOrAfterDate;
    property XmlConfiguration: XmlConfiguration read FXmlConfiguration write FXmlConfiguration;
    property Description: WideString read FDescription write FDescription;
    property ClassName: WideString read FClassName write FClassName stored AS_ATTRIBUTE;
    property ClassRevision: Integer read FClassRevision write FClassRevision stored AS_ATTRIBUTE;
  end;
{$WARNINGS ON}

  ArrayOfRuleDefinition = array of RuleDefinition;   { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  RuleSet = class(TRemotable)
  private
    FRuleDefinitions: ArrayOfRuleDefinition;
  public
    destructor Destroy; override;
  published
    property RuleDefinitions: ArrayOfRuleDefinition read FRuleDefinitions write FRuleDefinitions;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  GetSiteRuleSetApiResult = class(ApiResult)
  private
    FActiveRuleSet: RuleSet;
    FActiveRuleSetLastModifiedUTC: TXSDateTime;
    FActiveRuleSetRevisionNumber: Int64;
    FGlueStatus: WideString;
    FSiteName: WideString;
    FSiteNumber: Integer;
  public
    destructor Destroy; override;
  published
    property ActiveRuleSet: RuleSet read FActiveRuleSet write FActiveRuleSet;
    property ActiveRuleSetLastModifiedUTC: TXSDateTime read FActiveRuleSetLastModifiedUTC write FActiveRuleSetLastModifiedUTC;
    property ActiveRuleSetRevisionNumber: Int64 read FActiveRuleSetRevisionNumber write FActiveRuleSetRevisionNumber;
    property GlueStatus: WideString read FGlueStatus write FGlueStatus;
    property SiteName: WideString read FSiteName write FSiteName;
    property SiteNumber: Integer read FSiteNumber write FSiteNumber;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  FieldToUpdate = class(TRemotable)
  private
    FEffectiveDate: WideString;
    FFieldName: WideString;
    FValue: WideString;
  published
    property EffectiveDate: WideString read FEffectiveDate write FEffectiveDate;
    property FieldName: WideString read FFieldName write FFieldName;
    property Value: WideString read FValue write FValue;
  end;

  ArrayOfFieldToUpdate = array of FieldToUpdate;   { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  EmployeeFieldsToUpdate = class(TRemotable)
  private
    FFieldsToUpdate: ArrayOfFieldToUpdate;
    FIdentifier: WideString;
    FOptionalSecondaryIdentifier: WideString;
  public
    destructor Destroy; override;
  published
    property FieldsToUpdate: ArrayOfFieldToUpdate read FFieldsToUpdate write FFieldsToUpdate;
    property Identifier: WideString read FIdentifier write FIdentifier;
    property OptionalSecondaryIdentifier: WideString read FOptionalSecondaryIdentifier write FOptionalSecondaryIdentifier;
  end;

  ArrayOfEmployeeFieldsToUpdate = array of EmployeeFieldsToUpdate;   { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  ValidationFailure = class(TRemotable)
  private
    Ffield: WideString;
    Fmessage: WideString;
  published
    property field: WideString read Ffield write Ffield;
    property message: WideString read Fmessage write Fmessage;
  end;

  ArrayOfValidationFailure = array of ValidationFailure;   { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  EmployeeUpdateFailures = class(TRemotable)
  private
    FFieldValidationFailures: ArrayOfValidationFailure;
    FIdentifier: WideString;
  public
    destructor Destroy; override;
  published
    property FieldValidationFailures: ArrayOfValidationFailure read FFieldValidationFailures write FFieldValidationFailures;
    property Identifier: WideString read FIdentifier write FIdentifier;
  end;

  ArrayOfEmployeeUpdateFailures = array of EmployeeUpdateFailures;   { "http://mc2cs.com/" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  UpdateEmployeeFieldsApiResult = class(ApiResult)
  private
    FValidationFailuresList: ArrayOfEmployeeUpdateFailures;
  public
    destructor Destroy; override;
  published
    property ValidationFailuresList: ArrayOfEmployeeUpdateFailures read FValidationFailuresList write FValidationFailuresList;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // ************************************************************************ //
  GetActivityFileApiResult = class(ApiResult)
  private
    FFormatBinaryFile: TByteDynArray;
    FFormatText: WideString;
    FMimeType: WideString;
    FSiteName: WideString;
    FSiteNumber: Integer;
  published
    property FormatBinaryFile: TByteDynArray read FFormatBinaryFile write FFormatBinaryFile;
    property FormatText: WideString read FFormatText write FFormatText;
    property MimeType: WideString read FMimeType write FMimeType;
    property SiteName: WideString read FSiteName write FSiteName;
    property SiteNumber: Integer read FSiteNumber write FSiteNumber;
  end;


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/
  // soapAction: http://mc2cs.com/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : API12Soap
  // service   : API12
  // port      : API12Soap
  // URL       : https://www.payrollservers.us:8003/pg/api12.asmx
  // ************************************************************************ //
  API12Soap = interface(IInvokable)
  ['{0DFA2C69-55B4-A076-64B2-B44DA02E6756}']
    function  CreateSession(const login: WideString; const password: WideString; const secondFactor: WideString): CreateSessionResult; stdcall;
    function  CreateSessionSelectClient(const login: WideString; const password: WideString; const secondFactor: WideString; const matchfield: WideString; const clientID: WideString): CreateSessionResult; stdcall;
    function  EEIDSubmit(const sessionID: WideString; const SiteNumber: Integer; const ApplicationID: WideString; const Employees: Employees): EEIDSubmitResult; stdcall;
    function  GetListOfFinalizedPayrolls(const sessionID: WideString): ListOfFinalizedPayrollsResult; stdcall;
    function  GetFinalizedPayroll(const sessionID: WideString; const FinalizedPayPeriodRecordNumber: Int64): GetFinalizedPayrollResult; stdcall;
    function  GetUnfinalizedPayroll(const sessionID: WideString; const BeginDate: TXSDateTime; const EndDate: TXSDateTime): GetUnfinalizedPayrollResult; stdcall;
    function  GetOneTimeCredential(const sessionID: WideString; const targetLogin: WideString): StringApiResult; stdcall;
    function  GetOneTimeCredentialESS(const sessionID: WideString; const targetLogin: WideString): StringApiResult; stdcall;
    function  GetSiteRuleSet(const sessionid: WideString): GetSiteRuleSetApiResult; stdcall;
    function  UpdateEmployeeFields(const sessionID: WideString; const ApplicationID: WideString; const AddIfNotFound: Boolean; const IdentityField: EmployeeIdentifierField; const Employees: ArrayOfEmployeeFieldsToUpdate; const OptionalSecondaryIdentityField: SecondaryIdentifierField): UpdateEmployeeFieldsApiResult; stdcall;
    function  GetActivityFile(const sessionID: WideString; const BeginDate: WideString; const EndDate: WideString; const FormatName: WideString): GetActivityFileApiResult; stdcall;
  end;

function GetAPI12Soap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): API12Soap;


implementation

function GetAPI12Soap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): API12Soap;
const
  defWSDL = 'E:\IS\Accurev\Dev1\DelphiProjects\Projects\ICP\EvoSwipeclock\Source\api12.wsdl';
  defURL  = 'https://www.payrollservers.us:8003/pg/api12.asmx';
  defSvc  = 'API12';
  defPrt  = 'API12Soap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as API12Soap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


constructor ValueBag.Create;
begin
  inherited Create;
  FSerializationOptions := [xoInlineArrays];
end;

destructor ValueBag.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FItem)-1 do
    if Assigned(FItem[I]) then
      FItem[I].Free;
  SetLength(FItem, 0);
  inherited Destroy;
end;

function ValueBag.GetValueBagItemArray(Index: Integer): ValueBagItem;
begin
  Result := FItem[Index];
end;

function ValueBag.GetValueBagItemArrayLength: Integer;
begin
  if Assigned(FItem) then
    Result := Length(FItem)
  else
  Result := 0;
end;

destructor EmployeeSelectorState.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FCustomCriteria)-1 do
    if Assigned(FCustomCriteria[I]) then
      FCustomCriteria[I].Free;
  SetLength(FCustomCriteria, 0);
  inherited Destroy;
end;

destructor RuleDefinition.Destroy;
begin
  if Assigned(FEmpSelectorConfiguration) then
    FEmpSelectorConfiguration.Free;
  if Assigned(FEnableOnOrAfterDate) then
    FEnableOnOrAfterDate.Free;
  inherited Destroy;
end;

destructor RuleSet.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FRuleDefinitions)-1 do
    if Assigned(FRuleDefinitions[I]) then
      FRuleDefinitions[I].Free;
  SetLength(FRuleDefinitions, 0);
  inherited Destroy;
end;

destructor GetSiteRuleSetApiResult.Destroy;
begin
  if Assigned(FActiveRuleSet) then
    FActiveRuleSet.Free;
  if Assigned(FActiveRuleSetLastModifiedUTC) then
    FActiveRuleSetLastModifiedUTC.Free;
  inherited Destroy;
end;

destructor EmployeeFieldsToUpdate.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FFieldsToUpdate)-1 do
    if Assigned(FFieldsToUpdate[I]) then
      FFieldsToUpdate[I].Free;
  SetLength(FFieldsToUpdate, 0);
  inherited Destroy;
end;

destructor EmployeeUpdateFailures.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FFieldValidationFailures)-1 do
    if Assigned(FFieldValidationFailures[I]) then
      FFieldValidationFailures[I].Free;
  SetLength(FFieldValidationFailures, 0);
  inherited Destroy;
end;

destructor UpdateEmployeeFieldsApiResult.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FValidationFailuresList)-1 do
    if Assigned(FValidationFailuresList[I]) then
      FValidationFailuresList[I].Free;
  SetLength(FValidationFailuresList, 0);
  inherited Destroy;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(API12Soap), 'http://mc2cs.com/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(API12Soap), 'http://mc2cs.com/%operationName%');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Selections), 'http://mc2cs.com/', 'Selections');
  RemClassRegistry.RegisterXSInfo(TypeInfo(EmployeeIdentifierField), 'http://mc2cs.com/', 'EmployeeIdentifierField');
  RemClassRegistry.RegisterXSInfo(TypeInfo(SecondaryIdentifierField), 'http://mc2cs.com/', 'SecondaryIdentifierField');
  RemClassRegistry.RegisterXSInfo(TypeInfo(SubmissionResults), 'http://mc2cs.com/', 'SubmissionResults');
  RemClassRegistry.RegisterXSInfo(TypeInfo(PayPeriods), 'http://mc2cs.com/', 'PayPeriods');
  RemClassRegistry.RegisterXSInfo(TypeInfo(FinalizedPayrollDocument), 'http://mc2cs.com/', 'FinalizedPayrollDocument');
  RemClassRegistry.RegisterXSInfo(TypeInfo(UnfinalizedPayrollDocument), 'http://mc2cs.com/', 'UnfinalizedPayrollDocument');
  RemClassRegistry.RegisterXSInfo(TypeInfo(XmlConfiguration), 'http://mc2cs.com/', 'XmlConfiguration');
  RemClassRegistry.RegisterXSClass(ValueBagItem, 'http://mc2cs.com/', 'ValueBagItem');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(ValueBagItem), 'type_', 'type');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Item), 'http://mc2cs.com/', 'Item');
  RemClassRegistry.RegisterXSClass(ValueBag, 'http://mc2cs.com/', 'ValueBag');
  RemClassRegistry.RegisterSerializeOptions(ValueBag, [xoInlineArrays]);
  RemClassRegistry.RegisterXSClass(Date, 'http://mc2cs.com/', 'Date');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfLong), 'http://mc2cs.com/', 'ArrayOfLong');
  RemClassRegistry.RegisterXSClass(ApiResult, 'http://mc2cs.com/', 'ApiResult');
  RemClassRegistry.RegisterXSClass(GetUnfinalizedPayrollResult, 'http://mc2cs.com/', 'GetUnfinalizedPayrollResult');
  RemClassRegistry.RegisterXSClass(GetFinalizedPayrollResult, 'http://mc2cs.com/', 'GetFinalizedPayrollResult');
  RemClassRegistry.RegisterXSClass(ListOfFinalizedPayrollsResult, 'http://mc2cs.com/', 'ListOfFinalizedPayrollsResult');
  RemClassRegistry.RegisterXSClass(EEIDSubmitResult, 'http://mc2cs.com/', 'EEIDSubmitResult');
  RemClassRegistry.RegisterXSClass(CreateSessionResult, 'http://mc2cs.com/', 'CreateSessionResult');
  RemClassRegistry.RegisterXSClass(StringApiResult, 'http://mc2cs.com/', 'StringApiResult');
  RemClassRegistry.RegisterXSClass(EmployeeSelectorCustomCriterion, 'http://mc2cs.com/', 'EmployeeSelectorCustomCriterion');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeSelectorCustomCriterion), 'http://mc2cs.com/', 'ArrayOfEmployeeSelectorCustomCriterion');
  RemClassRegistry.RegisterXSClass(EmployeeSelectorState, 'http://mc2cs.com/', 'EmployeeSelectorState');
  RemClassRegistry.RegisterXSClass(RuleDefinition, 'http://mc2cs.com/', 'RuleDefinition');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfRuleDefinition), 'http://mc2cs.com/', 'ArrayOfRuleDefinition');
  RemClassRegistry.RegisterXSClass(RuleSet, 'http://mc2cs.com/', 'RuleSet');
  RemClassRegistry.RegisterXSClass(GetSiteRuleSetApiResult, 'http://mc2cs.com/', 'GetSiteRuleSetApiResult');
  RemClassRegistry.RegisterXSClass(FieldToUpdate, 'http://mc2cs.com/', 'FieldToUpdate');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfFieldToUpdate), 'http://mc2cs.com/', 'ArrayOfFieldToUpdate');
  RemClassRegistry.RegisterXSClass(EmployeeFieldsToUpdate, 'http://mc2cs.com/', 'EmployeeFieldsToUpdate');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeFieldsToUpdate), 'http://mc2cs.com/', 'ArrayOfEmployeeFieldsToUpdate');
  RemClassRegistry.RegisterXSClass(ValidationFailure, 'http://mc2cs.com/', 'ValidationFailure');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfValidationFailure), 'http://mc2cs.com/', 'ArrayOfValidationFailure');
  RemClassRegistry.RegisterXSClass(EmployeeUpdateFailures, 'http://mc2cs.com/', 'EmployeeUpdateFailures');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeUpdateFailures), 'http://mc2cs.com/', 'ArrayOfEmployeeUpdateFailures');
  RemClassRegistry.RegisterXSClass(UpdateEmployeeFieldsApiResult, 'http://mc2cs.com/', 'UpdateEmployeeFieldsApiResult');
  RemClassRegistry.RegisterXSClass(GetActivityFileApiResult, 'http://mc2cs.com/', 'GetActivityFileApiResult');

end.
