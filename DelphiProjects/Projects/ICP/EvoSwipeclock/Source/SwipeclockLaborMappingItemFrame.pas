unit SwipeclockLaborMappingItemFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, swipeclockdecl, gdyClasses;

type
  TSwipeclockLaborMappingItemFrm = class(TFrame)
    Panel1: TPanel;
    pnlFieldName: TPanel;
    cbFields: TComboBox;
  private
    FLaborVars: IStr;
    procedure SetSwField(s: string);
    function GetSwField: string;
  public
    constructor Create(Owner: TComponent); override;
    property SwField: string read GetSwField write SetSwField;
    procedure SetFieldDefinitions(laborVars: IStr);
  end;

implementation

{$R *.dfm}

{ TSwipeclockLaborMappingItemFrm }

constructor TSwipeclockLaborMappingItemFrm.Create(Owner: TComponent);
begin
  inherited;
  FLaborVars := EmptyIStr;
  FLaborVars.Add('=Nothing');
  cbFields.ItemIndex := 0;
end;

function TSwipeclockLaborMappingItemFrm.GetSwField: string;
begin
  Assert(cbFields.ItemIndex>=0);
  Result := FLaborVars.AsTStrings.Names[cbFields.ItemIndex];
end;

procedure TSwipeclockLaborMappingItemFrm.SetSwField(s: string);
begin
  cbFields.ItemIndex := FLaborVars.AsTStrings.IndexOfName(trim(s));
  if cbFields.ItemIndex = -1 then
    cbFields.ItemIndex := 0;
end;

procedure TSwipeclockLaborMappingItemFrm.SetFieldDefinitions(laborVars: IStr);
var
  nm: string;
  i: integer;
begin
  nm := SwField;
  FLaborVars := laborVars;
  cbFields.Items.Clear;
  for i := 0 to laborVars.Count-1 do
    cbFields.Items.Add(laborVars.AsTStrings.Values[laborVars.AsTStrings.Names[i]]);
  SwField := nm;
end;

end.
