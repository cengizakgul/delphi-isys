unit SwipeclockLaborMappingFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, swipeclockdecl, StdCtrls, SwipeclockLaborMappingItemFrame, gdyClasses;

type
  TSwipeclockLaborMappingFrm = class(TFrame)
    GroupBox1: TGroupBox;
    RateCodeFieldFrame: TSwipeclockLaborMappingItemFrm;
    BranchFieldFrame: TSwipeclockLaborMappingItemFrm;
    DivisionFieldFrame: TSwipeclockLaborMappingItemFrm;
    ShiftFieldFrame: TSwipeclockLaborMappingItemFrm;
    JobFieldFrame: TSwipeclockLaborMappingItemFrm;
    departmentFieldFrame: TSwipeclockLaborMappingItemFrm;
    WCCodeFieldFrame: TSwipeclockLaborMappingItemFrm;
    TeamCodeFieldFrame: TSwipeclockLaborMappingItemFrm;
  private
    FDatabaseType: TExtAppDatabaseType;
    function GetLaborMapping: TSwipeClockLaborMapping;
    procedure SetLaborMapping(const Value: TSwipeClockLaborMapping);
  public
    property LaborMapping: TSwipeClockLaborMapping read GetLaborMapping write SetLaborMapping;
    procedure SetFieldDefinitions(laborVars: IStr);
    procedure Clear;
  end;

implementation

{$R *.dfm}

{ TSwipeclockLaborMappingFrm }

procedure TSwipeclockLaborMappingFrm.Clear;
begin
  DepartmentFieldFrame.SwField := '';
  JobFieldFrame.SwField := '';
  ShiftFieldFrame.SwField := '';
  DivisionFieldFrame.SwField := '';
  BranchFieldFrame.SwField := '';
  RateCodeFieldFrame.SwField := '';
  TeamCodeFieldFrame.SwField := '';
  WCCodeFieldFrame.SwField := '';
end;

function TSwipeclockLaborMappingFrm.GetLaborMapping: TSwipeClockLaborMapping;
begin
  Result.DatabaseType := FDatabaseType;
  Result.DepartmentField := DepartmentFieldFrame.SwField;
  Result.JobField := JobFieldFrame.SwField;
  Result.ShiftField := ShiftFieldFrame.SwField;
  Result.DivisionField := DivisionFieldFrame.SwField;
  Result.BranchField := BranchFieldFrame.SwField;
  Result.RateCodeField := RateCodeFieldFrame.SwField;
  Result.TeamField := TeamCodeFieldFrame.SwField;
  Result.WorkCompField := WCCodeFieldFrame.SwField;
end;

procedure TSwipeclockLaborMappingFrm.SetFieldDefinitions(laborVars: IStr);
begin
  DepartmentFieldFrame.SetFieldDefinitions(laborVars);
  JobFieldFrame.SetFieldDefinitions(laborVars);
  ShiftFieldFrame.SetFieldDefinitions(laborVars);
  DivisionFieldFrame.SetFieldDefinitions(laborVars);
  BranchFieldFrame.SetFieldDefinitions(laborVars);
  RateCodeFieldFrame.SetFieldDefinitions(laborVars);
  TeamCodeFieldFrame.SetFieldDefinitions(laborVars);
  WCCodeFieldFrame.SetFieldDefinitions(laborVars);
end;

procedure TSwipeclockLaborMappingFrm.SetLaborMapping(const Value: TSwipeClockLaborMapping);
begin
  FDatabaseType := Value.DatabaseType;
  DepartmentFieldFrame.SwField := Value.DepartmentField;
  JobFieldFrame.SwField := Value.JobField;
  ShiftFieldFrame.SwField := Value.ShiftField;
  DivisionFieldFrame.SwField := Value.DivisionField;
  BranchFieldFrame.SwField := Value.BranchField;
  RateCodeFieldFrame.SwField := Value.RateCodeField;
  TeamCodeFieldFrame.SwField := Value.TeamField;
  WCCodeFieldFrame.SwField := Value.WorkCompField;
end;

end.
