unit SwipeClockConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, swipeclockdecl, PasswordEdit, OptionsBaseFrame;

type
  TSwipeClockConnectionParamFrm = class(TOptionsBaseFrm)
    GroupBox1: TGroupBox;
    lblUserName: TLabel;
    lblPassword: TLabel;
    PasswordEdit: TPasswordEdit;
    UserNameEdit: TEdit;
    cbSavePassword: TCheckBox;
    lblSite: TLabel;
    SiteEdit: TEdit;
  private
    function GetParam: TSwipeClockConnectionParam;
    procedure SetParam(const Value: TSwipeClockConnectionParam);
  public
    property Param: TSwipeClockConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
    procedure Check;
    procedure Clear(enable: boolean);
    procedure ForceSavePassword;
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TSwipeClockConnectionParamFrm }

procedure TSwipeClockConnectionParamFrm.Check;
var
  err: string;
  procedure AddErr(s: string);
  begin
    if err <> '' then
      err := err + ',';
    err := err + #13#10 + s;
  end;
begin
  err := '';
  if trim(Param.Site) = '' then
    AddErr(lblSite.Caption + ' is not specified');
  if trim(Param.UserName) = '' then
    AddErr(lblUserName.Caption + ' is not specified');
  if Param.Password = '' then
    AddErr(lblPassword.Caption + ' is not specified');
  if err <> '' then
    raise Exception.Create('Invalid TimeWorks connection settings:'+err);
end;

procedure TSwipeClockConnectionParamFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    UserNameEdit.Text := '';
    PasswordEdit.Password := '';
    SiteEdit.Text := '';
    cbSavePassword.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

procedure TSwipeClockConnectionParamFrm.ForceSavePassword;
begin
  cbSavePassword.Checked := true;
end;

function TSwipeClockConnectionParamFrm.GetParam: TSwipeClockConnectionParam;
begin
  Result.Username := UserNameEdit.Text;
  Result.Password := PasswordEdit.Password;
  Result.Site := SiteEdit.Text;
  Result.SavePassword := cbSavePassword.Checked;
end;

function TSwipeClockConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(UserNameEdit.Text) <> '') and
            (PasswordEdit.Password <> '') and
            (trim(SiteEdit.Text) <> '');
end;

procedure TSwipeClockConnectionParamFrm.SetParam(const Value: TSwipeClockConnectionParam);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    UserNameEdit.Text := Value.Username;
    PasswordEdit.Password := Value.Password;
    SiteEdit.Text := Value.Site;
    cbSavePassword.Checked := Value.SavePassword;
  finally
    FBlockOnChange := false;
  end;
end;

end.
