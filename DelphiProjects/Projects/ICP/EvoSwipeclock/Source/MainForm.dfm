inherited MainFm: TMainFm
  Left = 364
  Top = 70
  Width = 1084
  Height = 698
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 1068
    Height = 600
    OnChange = PageControl1Change
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 1060
        inherited GroupBox1: TGroupBox
          Width = 1060
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      inline SwipeClockFrame: TSwipeClockConnectionParamFrm
        Left = 0
        Top = 0
        Width = 1060
        Height = 105
        Align = alTop
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 1060
          Caption = 'TimeWorks connection'
          inherited cbSavePassword: TCheckBox
            Width = 427
            Caption = 'Save password (mandatory for scheduling tasks)'
          end
        end
      end
      inline ExtAppSettingsFrame: TExtAppSettingsFrm
        Left = 0
        Top = 112
        Width = 377
        Height = 73
        TabOrder = 1
      end
    end
    object tbshEEExport: TTabSheet [2]
      Caption = 'Employee export'
      ImageIndex = 4
      object Panel2: TPanel
        Left = 0
        Top = 534
        Width = 1060
        Height = 38
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn5: TBitBtn
          Left = 8
          Top = 8
          Width = 97
          Height = 25
          Action = RunEmployeeExport
          Caption = 'Run export'
          TabOrder = 0
        end
        object BitBtn2: TBitBtn
          Left = 120
          Top = 8
          Width = 97
          Height = 25
          Action = actScheduleEEExport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      inline EEExportOptionsFrame: TEEExportOptionsFrm
        Left = 0
        Top = 477
        Width = 1060
        Height = 57
        Align = alBottom
        TabOrder = 1
        inherited gbAllowClearing: TGroupBox
          Left = 288
        end
      end
      inline EEFilterFrame: TSwipeclockEEExportFilterFrm
        Left = 0
        Top = 0
        Width = 1060
        Height = 477
        Align = alClient
        TabOrder = 2
        inherited GroupBox1: TGroupBox
          Width = 1060
          Height = 477
          inherited Panel1: TPanel
            Height = 460
            inherited EEPositionStatusFilterFrame: TCodesFilterFrm
              Height = 398
              inherited GroupBox1: TGroupBox
                Height = 398
                inherited lbStatuses: TCheckListBox
                  Height = 355
                end
              end
            end
            inherited EESalaryFilterFrame: TEESalaryFilterFrm
              Top = 398
            end
          end
          inherited DBDTFilterFrame: TDBDTFilterFrm
            Width = 817
            Height = 460
            inherited GroupBox1: TGroupBox
              Width = 817
              Height = 460
              inherited Panel1: TPanel
                Width = 813
              end
              inherited dgDBDT: TReDBCheckGrid
                Width = 813
                Height = 409
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Timeclock data import'
      inherited pnlBottom: TPanel
        Top = 534
        Width = 1060
        Height = 38
        object lblGlueStatus: TLabel
          Left = 240
          Top = 16
          Width = 5
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 97
          Height = 25
          Action = RunAction
          Caption = 'Run import'
          TabOrder = 0
        end
        object BitBtn3: TBitBtn
          Left = 120
          Top = 8
          Width = 97
          Height = 25
          Action = actScheduleTCImport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      inherited pnlPayrollBatch: TPanel
        Width = 1060
        Height = 534
        inherited PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Width = 1060
          Height = 230
          inherited Splitter1: TSplitter
            Height = 230
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 230
            inherited dgGrid: TReDBGrid
              Height = 205
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 741
            Height = 230
            inherited Panel1: TPanel
              Width = 741
            end
            inherited dgGrid: TReDBGrid
              Width = 741
              Height = 205
            end
          end
        end
        inline TCImportOptionsFrame: TTCImportOptionsFrm
          Left = 0
          Top = 230
          Width = 1060
          Height = 304
          Align = alBottom
          TabOrder = 1
          inherited EEFilterFrame: TSwipeClockEEFilterFrm
            Width = 538
            inherited gbEmployeeFilter: TGroupBox
              Width = 538
              inherited Bevel1: TBevel
                Width = 534
              end
              inherited pnlRadioButtons: TPanel
                Width = 534
              end
              inherited ChoiceFrame: TMultipleChoiceFrm
                Width = 534
                inherited pnlSelected: TPanel
                  Width = 309
                  inherited pnlSelectedHeader: TPanel
                    Width = 309
                  end
                  inherited lbSelected: TListBox
                    Width = 309
                  end
                end
              end
            end
          end
        end
      end
    end
    object tbshScheduler: TTabSheet [4]
      Caption = 'Scheduled tasks'
      ImageIndex = 5
      inline SchedulerFrame: TSchedulerFrm
        Left = 0
        Top = 0
        Width = 1060
        Height = 572
        Align = alClient
        TabOrder = 0
        inherited pnlTasksControl: TPanel
          Width = 1060
        end
        inherited PageControl2: TPageControl
          Width = 1060
          Height = 531
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 277
              Width = 1052
            end
            inherited pnlTasks: TPanel
              Width = 1052
              Height = 277
              inherited dgTasks: TReDBGrid
                Width = 776
                Height = 142
              end
              inherited Panel1: TPanel
                Top = 142
                Width = 776
              end
            end
            inherited pnlDetails: TPanel
              Top = 284
              Width = 1052
              inherited pcTaskDetails: TPageControl
                Width = 1052
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 618
                  end
                  inherited dgResults: TReDBGrid
                    Width = 618
                  end
                end
              end
            end
          end
        end
      end
    end
    object tbshSchedulerSettings: TTabSheet [5]
      Caption = 'Scheduler settings'
      ImageIndex = 6
      inline SmtpConfigFrame: TSmtpConfigFrm
        Left = 0
        Top = 0
        Width = 784
        Height = 180
        Align = alTop
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 784
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 641
    Width = 1068
  end
  inherited pnlCompany: TPanel
    Width = 1068
    inherited Panel1: TPanel
      Width = 105
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 105
      Width = 963
    end
  end
  inherited ActionList1: TActionList
    Left = 324
    Top = 576
    inherited RunAction: TAction
      Caption = 'Run import'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    inherited RunEmployeeExport: TAction
      Caption = 'Run export'
      OnExecute = RunEmployeeExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
    object actScheduleTCImport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleTCImportExecute
      OnUpdate = actScheduleTCImportUpdate
    end
    object actScheduleEEExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEEExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
  end
end
