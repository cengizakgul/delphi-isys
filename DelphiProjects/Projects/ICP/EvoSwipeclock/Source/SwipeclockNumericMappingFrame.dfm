object SwipeclockNumericMappingFrm: TSwipeclockNumericMappingFrm
  Left = 0
  Top = 0
  Width = 271
  Height = 123
  TabOrder = 0
  object gbNumericMapping: TGroupBox
    Left = 0
    Top = 0
    Width = 271
    Height = 123
    Align = alClient
    Caption = 'Numeric mapping'
    TabOrder = 0
    object dgNM: TReDBGrid
      Left = 2
      Top = 15
      Width = 267
      Height = 106
      DisableThemesInTitle = False
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsrcEDCodes
      KeyOptions = []
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      Sorting = False
      FilterDlg = False
    end
    object dbcbED: TwwDBLookupCombo
      Left = 102
      Top = 54
      Width = 121
      Height = 21
      DropDownAlignment = taLeftJustify
      DataField = 'EDCODE'
      DataSource = dsrcEDCodes
      TabOrder = 1
      AutoDropDown = False
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
    end
  end
  object dsrcEDCodes: TDataSource
    Left = 32
    Top = 8
  end
  object dsrcCO_E_D_CODES: TDataSource
    Left = 24
    Top = 72
  end
end
