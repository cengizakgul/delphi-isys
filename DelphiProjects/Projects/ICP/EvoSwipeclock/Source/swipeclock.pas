unit swipeclock;

interface

uses
  swipeclockdecl, gdycommonlogger, timeclockimport{TPayPeriod},
  classes, evodata, evoapiconnectionutils, swipeclockconnection, common,
  PlannedActions, gdyClasses, eefilter, kbmMemTable;

type
  TAnalyzeResult = record
    Errors: integer;
    Actions: ISyncActionsRO;
  end;

  TSwipeClockEmployeeUpdater = class
  private
    FConn: TSwipeClockConnection;
    FLogger: ICOmmonLogger;
    FOptions: TEEExportOptions;
    FEEFilter: TEEFilter;
    FEEData: TEvoEEData;
    FExtAppSettings: TExtAppSettings;
    FSwipeEEs: IXMLGetEmployeesDataSet;
    FSwipeCustomFields: TkbmCustomMemTable;
    FEEHadInitialPassword: TStrings;

    function ShouldSendPassword(EeCode: string): boolean;
    procedure CreateSwipeCustomFieldsDS;
    procedure LoadSwipeCustomFields;

    procedure ValidateSwipeClockEECodes(SwipeEEs: IXMLGetEmployeesDataSet);
    procedure RemoveSwipeclockEEsFromOtherCompanies(SwipeEEs: IXMLGetEmployeesDataSet);
  public
    constructor Create(const param: TSwipeClockConnectionParam; const options: TEEExportOptions; logger: ICommonLogger; EEFilter: TEEFilter; EEData: TEvoEEData; const ExtAppSettings: TExtAppSettings);
    destructor Destroy; override;
    function Analyze: TAnalyzeResult;
    function Apply(actions: ISyncActionsRO; pi: IProgressIndicator): TUpdateEmployeesStat;
  end;


function GetTimeClockData(const param: TSwipeClockConnectionParam; const options: TTCImportOptions; logger: ICommonLogger; company: TEvoCompanyDef; period: TPayPeriod; const ExtAppSettings: TExtAppSettings): TTimeClockImportData;

//procedure GetSites(const param: TSwipeClockConnectionParam; logger: ICommonLogger; list: TStrings);

type
  TTCImportOptionsContext = record
    Param: TSwipeClockConnectionParam;
    SiteName: string; //!! has nothing to do with tc options
    DatabaseType: TExtAppDatabaseType;

    NumericVars: IStr;
    LaborVars: IStr;
    ees: IXMLGetEmployeesDataSet;
  end;

function GetTCImportOptionsContext(const param: TSwipeClockConnectionParam; logger: ICommonLogger): TTCImportOptionsContext;
function GetDatabaseType(const param: TSwipeClockConnectionParam; logger: ICommonLogger): TExtAppDatabaseType;

implementation

uses
  bndGetEmployees,
  sysutils, math, bndGetSiteCards,
  gdystrset, variants, dateutils,
  RateImportOptionFrame, gdyGlobalWaitIndicator, DB, evConsts, xmlintf,
  api12, xmldoc;

function FindRule(rules: GetSiteRuleSetApiResult; nm: string): RuleDefinition;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to high(rules.ActiveRuleSet.RuleDefinitions) do
    if AnsiSameText(trim(rules.ActiveRuleSet.RuleDefinitions[i].ClassName), trim(nm)) then
    begin
      Result := rules.ActiveRuleSet.RuleDefinitions[i];
      exit;
    end;
end;

function GetRule(rules: GetSiteRuleSetApiResult; nm: string): RuleDefinition;
begin
  Result := FindRule(rules, nm);
  if Result = nil then
    raise Exception.CreateFmt('Cannot find the definition of the <%s> rule.'#13#10'Use TimeWorks''s Accountant Menu - Processing Rules page to setup this rule.', [nm]);
end;

type
  TClassicPayCodeTranslator = class
  public
    constructor Create(categories: string; codes: string; logger: ICommonLogger);
    function Translate(cat: string): string;
  private
    FCategories: IStr;
    FCodes: IStr;
    FLogger: ICommonLogger;
  end;

function GetBlockedEmployees(logger: ICommonLogger; swipeEEs: IXMLGetEmployeesDataSet): TStringSet;
var
  i: integer;
begin
  Result := nil;
  logger.LogEntry('Making list of TimeWorks employees with blocked export');
  try
    try
      for i := 0 to swipeEEs.Count-1 do
      begin
        logger.LogEntry('Inspecting TimeWorks employee record');
        try
          try
            logger.LogContextItem('First Name', swipeEEs[i].FirstName);
            logger.LogContextItem('Last Name', swipeEEs[i].LastName);
            logger.LogContextItem(sCtxEECode, swipeEEs[i].EmployeeCode);

            if (swipeEEs[i].ChildNodes.FindNode('ExportBlock') <> nil) and swipeEEs[i].ExportBlock then //the same logic as in TSwipeClockConnection.UpdateEmployee
              SetInclude(Result, trim(swipeEEs[i].EmployeeCode));
          except
            logger.StopException;
          end;
        finally
          logger.LogExit;
        end
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

function ConvertClassicToEvoTC(logger: ICommonLogger; x: IXMLGetSiteCards; company: TEvoCompanyDef; const options: TTCImportOptions; blockedEEs: TStringSet; const ExtAppSettings: TExtAppSettings): TTimeClockRecs;
var
  used: integer;
  i: integer;
  j: integer;
  cards: IXMLCardsType;
  card: IXMLCardType;
  punch: IXMLPunchType;
  cur: PTimeClockRec;
  typ: string;
  payCodes: TClassicPayCodeTranslator;

  procedure AddPunchTimesIfNeeded;
  begin
    if typ = 'Times' then
    begin
      if trim(punch.InDT) = 'missing' then
        raise Exception.Create('There are missed punches still on record');
      if trim(punch.OutDT) = 'missing' then
        raise Exception.Create('There are missed punches still on record');
      cur.PunchIn := StdDateTimeToDateTime(punch.InDT);
      cur.PunchOut := StdDateTimeToDateTime(punch.OutDT);
    end;
  end;

  procedure AddMappedFields;
    function GetMappedField(f: string): string;
    begin
      Assert( (Length(f) = 0) or ((Length(f) = 1) and (Pos(f, SwipeClockFieldCodes)>0) ) );
      if Length(f) = 1 then
        case f[1] of
          'X': Result := punch.X;
          'Y': Result := punch.Y;
          'Z': Result := punch.Z;
          'D': Result := card.Employee.HomeDepartment;
          'L': Result := card.Employee.HomeLocation;
          'S': Result := card.Employee.HomeSupervisor;
        else
          Assert(false);
        end;
    end;
  begin
    cur.Department := GetMappedField(options.LaborMapping.DepartmentField);
    cur.Job := GetMappedField(options.LaborMapping.JobField);
    cur.Shift := GetMappedField(options.LaborMapping.ShiftField);
    cur.Division := GetMappedField(options.LaborMapping.DivisionField);
    cur.Branch := GetMappedField(options.LaborMapping.BranchField);
    cur.RateNumber := GetMappedField(options.LaborMapping.RateCodeField);
    cur.Team := GetMappedField(options.LaborMapping.TeamField);
    cur.WCCode := GetMappedField(options.LaborMapping.WorkCompField);
  end;

  function FilterAllows: boolean;
  begin
    case Options.EEFilter.Kind of
      fltNone: Result := true;
      fltBranch: Result := InSet( AnsiUpperCase(trim(card.Employee.HomeLocation)), Options.EEFilter.BranchCodes);
      fltDepartment: Result := InSet( AnsiUpperCase(trim(card.Employee.HomeDepartment)),Options.EEFilter.DepartmentCodes);
    else
      Result := false; //to make compiler happy
      Assert(false);
    end;
  end;

  procedure AddRec;
  begin
    used := used + 1;
    if used > Length(Result) then
    begin
      logger.LogDebug('Reallocating...');
      SetLength(Result, Length(Result) + 10000 ); //initialized with zeroes
      logger.LogDebug('Reallocated');
    end;
    cur := @Result[used-1];

    cur.CUSTOM_EMPLOYEE_NUMBER := card.Employee.EmployeeCode;
    cur.FirstName := card.Employee.FirstName;
    cur.LastName := card.Employee.LastName;
    cur.SSN := card.Employee.SSN;
    cur.Rate := Null; // looks like Unassigned is Ok...
  end;

  procedure AddNumeric(EdCode: string; num: double);
  begin
    if trim(EdCode) <> '' then
    begin
      AddRec;
      cur.EDCode := EdCode;
      cur.PunchDate := StdDateToDate(punch.PunchDate);
      cur.Amount := num;
      AddMappedFields;
      cur.RateNumber := ''; //because we import Amount here
    end;
  end;

var
  numMap: IStr;
begin
  used := 0;
  SetLength(Result, 0);
  try
    cards := x.CardReport.Cards;
    logger.LogEntry('Converting TimeWorks cards to Evolution format');
    try
      try
        payCodes := TClassicPayCodeTranslator.Create(x.CardReport.Site.PayCodeCategories, x.CardReport.Site.PayCodes, logger);
        try
          for i := 0 to cards.Count-1 do
          begin
            logger.LogEntry('Converting TimeWorks card to Evolution format');
            try
              try
                card := cards.Card[i];

                if not FilterAllows then
                  continue;

                if (card.Employee.ChildNodes.FindNode('EmployeeCode') = nil) or (trim(card.Employee.EmployeeCode) = '') then
                  raise Exception.Create('Employee doesn''t have Employee Code');

                logger.LogContextItem(sCtxEECode, card.Employee.EmployeeCode);

                if InSet(trim(card.Employee.EmployeeCode), blockedEEs) then
                begin
                  logger.LogDebug('Employee export is blocked');
                  continue;
                end;

                if ExtAppSettings.ConsolidatedDatabase and not SameText(trim(card.Employee.HomeLocation), trim(company.CUSTOM_COMPANY_NUMBER)) then
                begin
                  logger.LogDebug('Employee belongs to other company');
                  continue;
                end;

                for j := 0 to card.Punches.Count-1 do
                begin
                  punch := card.Punches[j];
                  AddRec;

                  cur.EDCode := payCodes.Translate(punch.Category);
                  cur.PunchDate := StdDateToDate(punch.PunchDate);
                  //!! ?? punch.ApplicablePayRate;
                  typ := trim(punch.Type_);
                  if typ = 'Times' then
                  begin
                    cur.Hours := punch.NonOTHours;
                    if options.RateImport = rateUseExternal then
                      cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate;
                    cur.Amount := Null;
                    AddPunchTimesIfNeeded;
                  end
                  else if typ = 'Hours' then
                  begin
                    cur.Hours := punch.NonOTHours;
                    if options.RateImport = rateUseExternal then
                      cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate;
                    cur.Amount := Null;
                  end
                  else if typ = 'Pay' then
                  begin
                    cur.Hours := Null;
                    cur.Rate := Null;
                    cur.Amount := punch.Pay;
                  end
                  else
                    raise Exception.CreateFmt('Unexpected punch type: <%s>', [typ]);
                  AddMappedFields;

                  if InSet(typ, ['Times','Hours']) then
                  begin
                    if punch.OT1Hours > eps then
                    begin
                      AddRec;
                      cur.EDCode := payCodes.Translate(punch.OT1Category);
                      cur.PunchDate := StdDateToDate(punch.PunchDate);
                      cur.Hours := punch.OT1Hours;
                      if options.RateImport = rateUseExternal then
                        cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate; // OT1PayRate
                      cur.Amount := Null;
                      AddPunchTimesIfNeeded;
                      AddMappedFields;
                    end;
                    if punch.OT2Hours > eps then
                    begin
                      AddRec;
                      cur.EDCode := payCodes.Translate(punch.OT2Category);
                      cur.PunchDate := StdDateToDate(punch.PunchDate);
                      cur.Hours := punch.OT2Hours;
                      if options.RateImport = rateUseExternal then
                        cur.Rate := punch.ApplicablePayRate; //because of blended rates //PayRate; //OT2PayRate
                      cur.Amount := Null;
                      AddPunchTimesIfNeeded;
                      AddMappedFields;
                    end;
                  end;

                  numMap := SplitToLines(options.NumericMapping.SerializedVarToEDCode);
                  if numMap.AsTStrings.IndexOfName('I') <> -1 then
                    AddNumeric(numMap.AsTStrings.Values['I'], punch.I);
                  if numMap.AsTStrings.IndexOfName('J') <> -1 then
                    AddNumeric(numMap.AsTStrings.Values['J'], punch.j);
                  if numMap.AsTStrings.IndexOfName('K') <> -1 then
                    AddNumeric(numMap.AsTStrings.Values['K'], punch.K);
                end
              except
                logger.PassthroughException;
              end;
            finally
              logger.LogExit;
            end;
          end;
        finally
          FreeAndNil(payCodes);
        end;
      except
        logger.PassthroughException;
      end;
    finally
      logger.LogExit;
    end;
  finally
    SetLength(Result, used);
  end;
end;

function SameStr(v: OleVariant; s: widestring): boolean;
begin
  Result := AnsiSameText(trim(VarToStr(v)), trim(s));
end;

function HasVar(vars: IXmlNode; nm: string): boolean;
var
  item: IXmlNode;
  i: integer;
begin
  Result := false;
  for i := 0 to vars.ChildNodes.Count-1 do
  begin
    item := vars.ChildNodes.Get(i);
    if SameStr(item.Attributes['name'], nm) then
    begin
      Result := true;
      exit;
    end;
  end
end;

function GetVar(vars: IXmlNode; nm: string): string;
var
  item: IXmlNode;
  i: integer;
begin
  Result := '';
  for i := 0 to vars.ChildNodes.Count-1 do
  begin
    item := vars.ChildNodes.Get(i);
    if SameStr(item.Attributes['name'], nm) then
    begin
      Result := trim(VarToStr(item.NodeValue));
      exit;
    end;
  end
end;

type
  TGlueCategoryRec = record
    Base: string;
    OT1: string;
    OT2: string;
  end;

  TGlueCategoryRecs = array of TGlueCategoryRec;
  TGluePayCodeTranslator = class
  public
    constructor Create(rules: GetSiteRuleSetApiResult; logger: ICommonLogger);
    function Translate(cat: string): string;
    function BaseCategory(cat: string): TGlueCategoryRec;
  private
    FDefaultPayCode: string;
    FCategoryToPayCode: IStr;
    FCategories: TGlueCategoryRecs;
    FLogger: ICommonLogger;
  end;

{ TGluePayCodeTranslator }

function ValueBagToXml(conf: widestring): IXmlDocument;
begin
  Result := LoadXMLData(widestring('<whatever>')+conf+'</whatever>');
  Result.Options := Result.Options - [doNodeAutoCreate] + [doNodeAutoIndent];
end;

constructor TGluePayCodeTranslator.Create(rules: GetSiteRuleSetApiResult; logger: ICommonLogger);

  procedure LoadTranslation(trRule: RuleDefinition);
  var
    tr: IXmlDocument;
    i: integer;
    table: string;
  begin
    FLogger.LogEntry('Loading TimeWorks Plus pay code translation');
    try
      try
        logger.LogDebug('Translation:', Utf8Encode(trRule.Configuration));
        tr := ValueBagToXml(trRule.Configuration);
        table := '';
        for i := 0 to tr.DocumentElement.ChildNodes.Count-1 do
        begin
          if SameStr(tr.DocumentElement.ChildNodes.Get(i).Attributes['name'], 'table') then
            table := trim(VarToStr(tr.DocumentElement.ChildNodes.Get(i).NodeValue))
          else if SameStr(tr.DocumentElement.ChildNodes.Get(i).Attributes['name'], 'default') then
            FDefaultPayCode := trim(VarToStr(tr.DocumentElement.ChildNodes.Get(i).NodeValue));
        end;
        FLogger.LogContextItem('Pay code translation table', table);
        FLogger.LogContextItem('Pay code translation default code', FDefaultPayCode);
        FCategoryToPayCode := SplitToLines(table);
        FLogger.LogContextItem('Pay code translation table as strings', FCategoryToPayCode.AsTStrings.Text);
      except
        FLogger.PassthroughException;
      end;
    finally
      FLogger.LogExit;
    end;
  end;

  procedure LoadCategories(catsRule: RuleDefinition);
  var
    cats: IXmlDocument;
    i: integer;
  begin
    FLogger.LogEntry('Loading TimeWorks Plus punch categories');
    try
      try
        if catsRule = nil then
        begin
          logger.LogDebug('No PunchCategories found');
          exit;
        end;
        logger.LogDebug('PunchCategories:',Utf8Encode(catsRule.XMLConfiguration));
        cats := LoadXMLData(catsRule.XMLConfiguration);
        cats.Options := cats.Options - [doNodeAutoCreate] + [doNodeAutoIndent];

        SetLength(FCategories, cats.DocumentElement.ChildNodes.Count);
        for i := 0 to cats.DocumentElement.ChildNodes.Count-1 do
        begin
          FCategories[i].Base := trim(VarToStr(cats.DocumentElement.ChildNodes.Get(i).Attributes['BaseCategory']));
          FCategories[i].OT1 := trim(VarToStr(cats.DocumentElement.ChildNodes.Get(i).Attributes['OT1Name']));
          FCategories[i].OT2 := trim(VarToStr(cats.DocumentElement.ChildNodes.Get(i).Attributes['OT2Name']));
          logger.LogDebug(Format('Base=<%s>, OT1=<%s>, OT2=<%s>', [FCategories[i].Base, FCategories[i].OT1, FCategories[i].OT2]));
        end;
      except
        FLogger.PassthroughException;
      end;
    finally
      FLogger.LogExit;
    end;
  end;

begin
  FLogger := logger;
  LoadTranslation(GetRule(rules, 'PayCodeTranslation'));
  LoadCategories(FindRule(rules, 'PunchCategories'));
end;

function TGluePayCodeTranslator.Translate(cat: string): string;
begin
  FLogger.LogEntry('Translating TimeWorks Plus pay code category into pay code');
  try
    try
      FLogger.LogContextItem('Pay code category', cat);
      cat := trim(cat);
      if FCategoryToPayCode.AsTStrings.IndexOfName(cat) <> -1 then
        Result := trim(FCategoryToPayCode.AsTStrings.Values[cat])
      else
        Result := trim(FDefaultPayCode);
      if Result = '' then
        raise Exception.CreateFmt('Cannot translate TimeWorks Plus pay code category <%s>'#13#10'Use TimeWorks''s Accountant Menu - Processing Rules - PayCodeTranslation Rule page to specify an E/D code for this pay code category', [cat])
      else
        FLogger.LogDebug(Format('Category=<%s> => ED=<%s>', [cat, Result]));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TGluePayCodeTranslator.BaseCategory(cat: string): TGlueCategoryRec;
var
  i: integer;
begin
  FLogger.LogEntry('Finding TimeWorks Plus punch category definition');
  try
    try
      FLogger.LogContextItem('Punch category', cat);
      cat := trim(cat);
      Result.Base := cat;
      Result.OT1 := 'Overtime';
      Result.OT2 := 'Doubletime';
      for i := 0 to high(FCategories) do
        if AnsiSameText(FCategories[i].Base, cat) then
        begin
          Result := FCategories[i];
          FLogger.LogDebug(Format('Found: Base=<%s>, OT1=<%s>, OT2=<%s>', [Result.Base, Result.OT1, Result.OT2]));
          exit;
        end;
      FLogger.LogDebug(Format('Default: Base=<%s>, OT1=<%s>, OT2=<%s>', [Result.Base, Result.OT1, Result.OT2]));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function ConvertGlueToEvoTC(logger: ICommonLogger; x: IXMLDocument; rules: GetSiteRuleSetApiResult; company: TEvoCompanyDef; const options: TTCImportOptions; const ExtAppSettings: TExtAppSettings; period: TPayPeriod): TTimeClockRecs;

  function GetEEVar(ee: IXmlNode; nm: string): string;
  var
    i: integer;
    states: IXmlNode;
    st: IXmlNode;
    valdt: TDateTime;
    dt: TDateTime;
  begin
    Result := '';
    dt := 0;
    states := ee.ChildNodes['States'];
    for i := 0 to states.ChildNodes.Count-1 do
    begin
      st := states.ChildNodes.Get(i);
      if HasVar(st.ChildNodes['Variables'], nm) then
      begin
        if SameStr(st.ChildValues['EffectiveDate'], '') then
          valdt := 0
        else
          valdt := StdDateToDate(trim(VarToStr(st.ChildValues['EffectiveDate'])));
        if (valdt >= dt) and (valdt <= period.PeriodEnd) then
        begin
          dt := valdt;
          Result := GetVar(st.childNodes['Variables'], nm);
        end;
      end;
    end;
  end;

var
  used: integer;
  cards: IXMLNode;
  card: IXMLNode;
  ee: IXmlNode;
  EECode: string;
  lines: IXMLNode;
  line: IXMLNode;
  cur: PTimeClockRec;
  payCodes: TGluePayCodeTranslator;
  punchDate: TDateTime;

  procedure AddPunchTimesIfNeeded;
  begin
    if SameStr(line.Attributes['InIsMissing'], 'true') or SameStr(line.Attributes['OutIsMissing'], 'true') then
      raise Exception.Create('There are missed punches still on record');
    if not SameStr(line.ChildValues['InDT'], '') then
      cur.PunchIn := StdDateTimeToDateTime(VarToStr(line.ChildValues['InDT']));
    if not SameStr(line.ChildValues['OutDT'], '') then
      cur.PunchOut := StdDateTimeToDateTime(VarToStr(line.ChildValues['OutDT']));
  end;

  procedure AddMappedFields;
    function GetMappedField(f: string): string;
    begin
      f := trim(f);
      if f = SCHomeDepartmentFieldCode then
        Result := GetEEVar(ee, 'Department')
      else if f = SCHomeLocationFieldCode then
        Result := GetEEVar(ee, 'Location')
      else if f = SCHomeSupervisorFieldCode then
        Result := GetEEVar(ee, 'Supervisor')
      else
        Result := GetVar(line.ChildNodes['Variables'], f);
    end;
  begin
    cur.Department := GetMappedField(options.LaborMapping.DepartmentField);
    cur.Job := GetMappedField(options.LaborMapping.JobField);
    cur.Shift := GetMappedField(options.LaborMapping.ShiftField);
    cur.Division := GetMappedField(options.LaborMapping.DivisionField);
    cur.Branch := GetMappedField(options.LaborMapping.BranchField);
    cur.RateNumber := GetMappedField(options.LaborMapping.RateCodeField);
    cur.Team := GetMappedField(options.LaborMapping.TeamField);
    cur.WCCode := GetMappedField(options.LaborMapping.WorkCompField);
  end;

  function FilterAllows: boolean;
  begin
    case Options.EEFilter.Kind of
      fltNone: Result := true;
      fltBranch: Result := InSet( AnsiUpperCase(GetEEVar(ee, 'Location')), Options.EEFilter.BranchCodes);
      fltDepartment: Result := InSet( AnsiUpperCase(GetEEVar(ee, 'Department')), Options.EEFilter.DepartmentCodes);
    else
      Result := false; //to make compiler happy
      Assert(false);
    end;
  end;

  procedure AddRec;
  begin
    used := used + 1;
    if used > Length(Result) then
    begin
      logger.LogDebug('Reallocating...');
      SetLength(Result, Length(Result) + 10000 ); //initialized with zeroes
      logger.LogDebug('Reallocated');
    end;
    cur := @Result[used-1];

    cur.CUSTOM_EMPLOYEE_NUMBER := EECode;
    cur.FirstName := trim(VarToStr(ee.Attributes['FirstName']));
    cur.LastName := trim(VarToStr(ee.Attributes['LastName']));
  end;

  procedure AddHours(category: string; secAttrName: string);
  var
    rateStr: string;
  begin
    if StrToFloat(line.Attributes[secAttrName]) > 0 then
    begin
      AddRec;
      cur.EDCode := payCodes.Translate(category);
      cur.PunchDate := punchDate;
      cur.Hours := StrToFloat(line.Attributes[secAttrName]) / 3600;
      if options.RateImport = rateUseExternal then
      begin
        rateStr := GetVar(line.ChildNodes['Variables'], 'PAYRATE');
        if rateStr <> '' then
          cur.Rate := StrToFloat(rateStr);
      end;
      AddPunchTimesIfNeeded;
      AddMappedFields;
    end;
  end;

  procedure AddAmount(EdCode: string; numStr: string);
  var
    num: double;
  begin
    if trim(numStr) <> '' then
    begin
      num := StrToFloat(trim(numStr));
      if num > 0 then
      begin
        AddRec;
        cur.EDCode := EdCode;
        cur.PunchDate := punchDate;
        cur.Amount := num;
        AddMappedFields;
      end;
    end;
  end;

var
  numMap: IStr;
  i: integer;
  j: integer;
  k: integer;
  cat: string;
begin
  used := 0;
  SetLength(Result, 0);
  try
    if x.DocumentElement = nil then
      exit;
    cards := x.DocumentElement.ChildNodes['TimeCards'];
    if cards = nil then
      exit;
    logger.LogEntry('Converting TimeWorks Plus cards to Evolution format');
    try
      try
        payCodes := TGluePayCodeTranslator.Create(rules, logger);
        try
          for i := 0 to cards.ChildNodes.Count-1 do
          begin
            logger.LogEntry('Converting TimeWorks Plus card to Evolution format');
            try
              try
                card := cards.ChildNodes.Get(i);
                ee := card.ChildNodes['Employee'];

                EECode := trim(VarToStr(ee.Attributes['EmployeeCode']));
                logger.LogContextItem(sCtxEECode, EECode);

                if not FilterAllows then
                begin
                  logger.LogDebug('Employee doesn''t pass filter');
                  continue;
                end;

                if (VarToStr(ee.ChildValues['EndDate']) <> '') and (StdDateToDate(ee.ChildValues['EndDate']) < period.PeriodBegin) then
                begin
                  logger.LogDebug('Employee end date is before period begin date');
                  continue;
                end;

                if (VarToStr(ee.ChildValues['BeginDate']) <> '') and (StdDateToDate(ee.ChildValues['BeginDate']) > period.PeriodEnd) then
                begin
                  logger.LogDebug('Employee begin date is after period end date');
                  continue;
                end;

                if SameStr(ee.Attributes['ExportBlock'], 'true') then
                begin
                  logger.LogDebug('Employee export is blocked');
                  continue;
                end;

                if ExtAppSettings.ConsolidatedDatabase and not AnsiSameText(GetEEVar(ee, 'Location'), trim(company.CUSTOM_COMPANY_NUMBER)) then
                begin
                  logger.LogDebug('Employee belongs to other company');
                  continue;
                end;

                if EECode = '' then
                  raise Exception.Create('Employee doesn''t have Employee Code');

                lines := card.ChildNodes['Lines'];
                for j := 0 to lines.ChildNodes.Count-1 do
                begin
                  line := lines.ChildNodes.Get(j);
                  if SameStr(line.Attributes['IsDeleted'], 'true') then
                    continue;
                  if not SameStr(line.Attributes['TimeCardLineKind'], 'Normal') then
                    continue;

                  if line.ChildNodes.FindNode('ReportingDate') <> nil then
                    punchDate := StdDateToDate(VarToStr(line.ChildValues['ReportingDate']))
                  else
                    punchDate := StdDateToDate(VarToStr(line.ChildValues['PostDate']));
                  Assert((punchDate >= period.PeriodBegin) and (punchDate <= period.PeriodEnd));
                  cat := trim(VarToStr(line.Attributes['BaseCategory']));
                  if cat = '' then
                    cat := 'Regular';
                  logger.LogDebug(Format('cat=<%s>', [cat]));
                  AddHours(cat, 'NonOTSeconds');
                  AddHours(payCodes.BaseCategory(cat).OT1, 'OT1Seconds');
                  AddHours(payCodes.BaseCategory(cat).OT2, 'OT2Seconds');

                  AddAmount(payCodes.Translate(cat), GetVar(line.ChildNodes['Variables'], 'AMOUNT'));
                  numMap := SplitToLines(options.NumericMapping.SerializedVarToEDCode);
                  for k := 0 to numMap.Count-1 do
                    AddAmount(numMap.AsTStrings.Values[numMap.AsTStrings.Names[k]], GetVar(line.ChildNodes['Variables'], numMap.AsTStrings.Names[k]));
                end
              except
                logger.PassthroughException;
              end;
            finally
              logger.LogExit;
            end;
          end;
        finally
          FreeAndNil(payCodes);
        end;
      except
        logger.PassthroughException;
      end;
    finally
      logger.LogExit;
    end;
  finally
    SetLength(Result, used);
  end;
end;

function GlueStatusToDatabaseType(glueStatus: widestring): TExtAppDatabaseType;
begin
  if SameStr(glueStatus, 'Not') or SameStr(glueStatus, 'Testing') or SameStr(glueStatus, 'Migrating') then
    Result := dbClassic
  else if SameStr(glueStatus, 'Native') or SameStr(glueStatus, 'Migrated') then
    Result := dbGlue
  else
    raise Exception.CreateFmt('Unknown GlueStatus: <%s>',[glueStatus]);
end;

function GetTimeClockData(const param: TSwipeClockConnectionParam; const options: TTCImportOptions; logger: ICommonLogger; company: TEvoCompanyDef; period: TPayPeriod; const ExtAppSettings: TExtAppSettings): TTimeClockImportData;
var
  recs: TTimeClockRecs;
  rules: GetSiteRuleSetApiResult;
begin
  logger.LogEntry('Getting timeclock data');
  try
    try
      LogTCImportOptions(logger, options);
      if options.EEFilter.Kind <> fltNone then
        logger.LogEventFmt('Using employee filter: %s', [EEFilterToStr(options.EEFilter)] );
      with TSwipeClockConnection.Create(param, logger) do
      try
        case GlueStatusToDatabaseType(GetSiteRuleSet.GlueStatus) of
          dbClassic:
          begin
            recs := ConvertClassicToEvoTC(logger, GetSiteCards(period), company, options, GetBlockedEmployees(logger, GetEmployees), ExtAppSettings);
          end;
          dbGlue:
          begin
            rules := GetSiteRuleSet;
            try
              recs := ConvertGlueToEvoTC(logger, GetUnfinalizedPayroll(period), rules, company, options, ExtAppSettings, period);
            finally
              FreeAndNil(rules);
            end;
          end;
        else
          Assert(false);
        end;
        if options.Summarize then
          recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
        Result := BuildTimeClockImportData(recs, options.RateImport = rateUseEvoEE, options.AutoImportJobCodes, logger); //false if rateUseExternal; it was this way before and everybody was happy...
      finally
        Free;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;
{
procedure GetSites(const param: TSwipeClockConnectionParam; logger: ICommonLogger; list: TStrings);
var
  sites: IXMLGetSites;
  i: integer;
begin
  list.Clear;
  logger.LogEntry('Getting Swipeclock site list');
  try
    try
      with TSwipeClockConnection.Create(param, logger) do
      try
        sites := GetSites();
        for i := 0 to sites.Count-1 do
          list.AddObject(sites[i].SiteName, Pointer(sites[i].Site) );
      finally
        Free;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;
}
{ TClassicPayCodeTranslator }

constructor TClassicPayCodeTranslator.Create(categories: string; codes: string; logger: ICommonLogger);
begin
  FLogger := logger;
  FCategories := SplitByChar(trim(categories), '|');
  FCodes := SplitByChar(trim(codes), '|');
  FLogger.LogContextItem('Pay Code Categories', categories);
  FLogger.LogContextItem('Pay Codes', codes);
end;

function TClassicPayCodeTranslator.Translate(cat: string): string;
var
  i: integer;
begin
  FLogger.LogEntry('Translating TimeWorks pay code category into pay code');
  try
    try
      FLogger.LogContextItem('Pay code category', cat);
      cat := trim(cat);
      for i := 0 to FCategories.Count-1 do
        if trim(FCategories.Str[i]) = cat then
        begin
          Result := trim(FCodes.Str[i]);
          exit;
        end;

      if FCodes.Count = FCategories.Count+1 then //then the last code is for everything else
      begin
        Result := trim(FCodes.Str[FCodes.Count-1]);
        exit;
      end;

      raise Exception.CreateFmt('Cannot translate TimeWorks pay code category <%s>'#13#10'Use TimeWorks''s Accountant Menu - Pay Code Translation page to specify an E/D code for this pay code category', [cat]);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function GetDatabaseType(const param: TSwipeClockConnectionParam; logger: ICommonLogger): TExtAppDatabaseType;
var
  conn: TSwipeClockConnection;
  rules: GetSiteRuleSetApiResult;
begin
  Result := dbClassic; //to make compiler happy
  logger.LogEntry('Getting TimeWorks database type');
  try
    try
      WaitIndicator.StartWait('Getting data from TimeWorks');
      try
        conn := TSwipeClockConnection.Create(param, logger);
        try
          rules := conn.GetSiteRuleSet;
          try
            Result := GlueStatusToDatabaseType(rules.GlueStatus);
          finally
            FreeAndNil(rules);
          end;
        finally
          FreeAndNil(conn);
        end;
      finally
        WaitIndicator.EndWait;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

function GetTCImportOptionsContext(const param: TSwipeClockConnectionParam; logger: ICommonLogger): TTCImportOptionsContext;

  procedure LoadPrompts(promptRule: RuleDefinition; laborVars: IStr; numericVars: IStr);
  var
    prompts: IXmlDocument;
    i: integer;
  begin
    logger.LogEntry('Loading TimeWorks Plus prompts');
    try
      try
        if promptRule = nil then
        begin
          logger.LogDebug('No ClockPrompts found');
          exit;
        end;
        prompts := LoadXMLData(promptRule.XMLConfiguration);
        prompts.Options := prompts.Options - [doNodeAutoCreate] + [doNodeAutoIndent];
        for i := 0 to prompts.DocumentElement.ChildNodes.Count-1 do
          if InSet(trim(VarToStr(prompts.DocumentElement.ChildNodes.Get(i).ChildValues['PromptAssignment'])), ['Labor','X','Y','Z']) then
            laborVars.Add(trim(VarToStr(prompts.DocumentElement.ChildNodes.Get(i).ChildValues['VariableName'])) + '=' + trim(VarToStr(prompts.DocumentElement.ChildNodes.Get(i).ChildValues['HeaderName'])))
          else if InSet(trim(VarToStr(prompts.DocumentElement.ChildNodes.Get(i).ChildValues['PromptAssignment'])), ['Numeric','I','J','K']) then
            numericVars.Add(trim(VarToStr(prompts.DocumentElement.ChildNodes.Get(i).ChildValues['VariableName'])) + '=' + trim(VarToStr(prompts.DocumentElement.ChildNodes.Get(i).ChildValues['HeaderName'])));
      except
        logger.PassthroughException;
      end;
    finally
      logger.LogExit;
    end;
  end;

var
  conn: TSwipeClockConnection;
  dummy: TPayPeriod;
  rules: GetSiteRuleSetApiResult;
begin
  logger.LogEntry('Getting data from TimeWorks');
  try
    try
      WaitIndicator.StartWait('Getting data from TimeWorks');
      try
        Result.Param := param;
        conn := TSwipeClockConnection.Create(param, logger);
        try
          Result.LaborVars := EmptyIStr;
          Result.NumericVars := EmptyIStr;
          rules := conn.GetSiteRuleSet;
          try
            Result.DatabaseType := GlueStatusToDatabaseType(rules.GlueStatus);
            case Result.DatabaseType of
              dbClassic:
              begin
                dummy.PeriodBegin := Now;
                dummy.PeriodEnd := Now;
                with conn.GetSiteCards(dummy).CardReport.Site do
                begin
                  Result.SiteName := SiteName;
                  Result.LaborVars.Add('=Nothing');
                  Result.LaborVars.Add('X=X '+Xdefinition);
                  Result.LaborVars.Add('Y=Y '+Ydefinition);
                  Result.LaborVars.Add('Z=Z '+Zdefinition);
                  Result.LaborVars.Add('D=D Home Department');
                  Result.LaborVars.Add('L=L Home Location');
                  Result.LaborVars.Add('S=S Home Supervisor');

                  Result.NumericVars.Add('I=I '+Idefinition);
                  Result.NumericVars.Add('J=J '+Jdefinition);
                  Result.NumericVars.Add('K=K '+Kdefinition);
                end;
              end;
              dbGlue:
              begin
                Result.SiteName := rules.SiteName;
                Result.LaborVars.Add('=Nothing');
                LoadPrompts(FindRule(rules, 'ClockPrompts'), Result.LaborVars, Result.NumericVars);
                Result.LaborVars.Add(SCHomeDepartmentFieldCode + '=Home Department');
                Result.LaborVars.Add(SCHomeLocationFieldCode + '=Home Location');
                Result.LaborVars.Add(SCHomeSupervisorFieldCode + '=Home Supervisor');
              end;
            else
              Assert(false);
            end;
          finally
            FreeAndNil(rules);
          end;
          Result.ees := conn.GetEmployees;
        finally
          FreeAndNil(conn);
        end;
      finally
        WaitIndicator.EndWait;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;


{ TSwipeClockEmployeeUpdater }

procedure TSwipeClockEmployeeUpdater.ValidateSwipeClockEECodes(SwipeEEs: IXMLGetEmployeesDataSet);
var
  i: integer;
  j: integer;
begin
  FLogger.LogEntry('Validating TimeWorks employee codes');
  try
    try
      Assert(SwipeEEs <> nil);
      for i := SwipeEEs.Count-1 downto 0 do
        if SwipeEEs[i].ChildNodes.FindNode('EmployeeCode') = nil then
        begin
          FLogger.LogWarningFmt('TimeWorks employee %s %s doesn''t have Employee Code.',[SwipeEEs[i].FirstName, SwipeEEs[i].LastName]);
          SwipeEEs.Delete(i);
        end;

      for i := 0 to SwipeEEs.Count-1 do
        if trim(SwipeEEs[i].EndDate) = '' then
          for j := i+1 to SwipeEEs.Count-1 do
            if trim(SwipeEEs[j].EndDate) = '' then
              if (SwipeEEs[i].ChildNodes.FindNode('EmployeeCode') <> nil) and
                 (SwipeEEs[j].ChildNodes.FindNode('EmployeeCode') <> nil) and
                 (trim(SwipeEEs[i].EmployeeCode) = trim(SwipeEEs[j].EmployeeCode)) then
                FLogger.LogWarningFmt('TimeWorks employees %s %s and %s %s have the same Employee Code (%s)',
                 [SwipeEEs[i].FirstName, SwipeEEs[i].LastName, SwipeEEs[j].FirstName, SwipeEEs[j].LastName, SwipeEEs[i].EmployeeCode]);
    except
      FLogger.StopException;
    end;
  finally
    FLogger.LogExit;
  end;
end;
           
function IsInSwipeClock(SwipeEEs: IXMLGetEmployeesDataSet; code: string): boolean;
var
  i:integer;
begin
  Assert(SwipeEEs <> nil);
  Result := false;
  for i := 0 to SwipeEEs.Count-1 do
    if trim(SwipeEEs[i].EmployeeCode) = trim(code) then
    begin
      Result := true;
      Exit;
    end;
end;

procedure TSwipeClockEmployeeUpdater.RemoveSwipeclockEEsFromOtherCompanies(SwipeEEs: IXMLGetEmployeesDataSet);
var
  i: integer;
begin
  FLogger.LogEntry('Removing Swipeclock employees from other companies from the list');
  try
    try
      if FExtAppSettings.ConsolidatedDatabase then
      begin
        Assert(SwipeEEs <> nil);
        for i := SwipeEEs.Count-1 downto 0 do
          if not SameText(trim(SwipeEEs[i].Location), trim(FEEData.Company.CUSTOM_COMPANY_NUMBER)) then
            SwipeEEs.Delete(i);
      end
    except
      FLogger.StopException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockEmployeeUpdater.Analyze: TAnalyzeResult;

  procedure CheckIfChangedVal(var ChangedFields: TChangedFieldRecs; v1: Variant; v2: TEeAdditionalField; fieldName: string);
  var
    diff: boolean;
  begin
    diff := trim(VarToStr(v1)) <> trim(VarToStr(v2.Value));
    if v2.IsSetup and diff then
      AddChangedFields(ChangedFields, VarToStr(v1), VarToStr(v2.Value), fieldName)
  end;

  procedure CheckIfNotEmptyAndChanged(var ChangedFields: TChangedFieldRecs; sc: string; evov: Variant; fieldName: string; allowClearing: boolean);
  var
    evo: string;
  begin
    evo := trim(VarToStr(evov));
    sc := trim(sc);
    if allowClearing or (evo <> '') then
      CheckIfChanged(ChangedFields, sc, evo, fieldname);
  end;

  function GetEeRateDep(rateDep: TRateItemTWP; eeAddField: TEeAdditionalField): TEeAdditionalField;
  begin
    Result.IsSetup := eeAddField.IsSetup;
    Result.Value := eeAddField.Value;

    if (rateDep.dep <> '') then
    begin
      Result.IsSetup := True;
      Result.Value := rateDep.dep;
    end;
  end;

  function CompareFields(swipeEE: IXMLEmployees; EEData: TEvoEEData): TChangedFieldRecs;
  var
    rates: TRateArray;
    ratesTWP: TRateArrayTWP;
    ClockNumbers: string;
  begin
    Assert( trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(swipeEE.EmployeeCode) );

    SetLength(Result, 0);

//    CheckIfChanged(Result, swipeEE.EmployeeCode, EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'], 'Employee Code');
    CheckIfChanged(Result, swipeEE.LastName, EEData.EEs['LAST_NAME'], 'Last name');
    CheckIfChanged(Result, swipeEE.FirstName, EEData.EEs['FIRST_NAME'], 'First name');
    CheckIfChanged(Result, swipeEE.MiddleName, EEData.EEs['MIDDLE_INITIAL'], 'Middle initial');
    if FOptions.ExportSSN then
      CheckIfChanged(Result, swipeEE.SSN, StringReplace(EEData.EEs.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, '-', '', [rfReplaceAll]), 'SSN'); //SSN in SC has to be without dashes
    CheckIfChanged(Result, swipeEE.StartDate, DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_HIRE_DATE').AsDateTime), 'Start date');
    if not FExtAppSettings.ConsolidatedDatabase then //can't update EndDate
    begin
      if not EEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').IsNull and (EEData.EEs['CURRENT_TERMINATION_CODE'] <> EE_TERM_ACTIVE) then
        CheckIfChanged(Result, swipeEE.EndDate, DateToSwipeClockDate(EEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime), 'End date')
      else
        CheckIfChanged(Result, swipeEE.EndDate, '', 'End date'); //make believe that Evolution's end date is always '', because Swipeclock's End Date determinates employee status
    end;
    if FExtAppSettings.ConsolidatedDatabase then
      CheckIfChanged(Result, swipeEE.Location, EEData.Company.CUSTOM_COMPANY_NUMBER, 'Location (company)')
    else
    begin
      if FOptions.ExportBranch then
        CheckIfChanged(Result, swipeEE.Location, Trim(EEData.EEs['CUSTOM_BRANCH_NUMBER']), 'Location (branch)');
    end;

    if FExtAppSettings.ConsolidatedDatabase then
    begin
      if FOptions.ExportRates then
      begin
        rates := EEData.GetEERatesArray;
        CheckIfFloatChanged(Result, StrToFloat(swipeEE.PayRate0), rates[0], 'Default payrate');
        CheckIfFloatChanged(Result, StrToFloat(swipeEE.PayRate1), rates[1], 'Payrate 1');
        CheckIfFloatChanged(Result, StrToFloat(swipeEE.PayRate2), rates[2], 'Payrate 2');
        CheckIfFloatChanged(Result, StrToFloat(swipeEE.PayRate3), rates[3], 'Payrate 3');
      end;

      if FOptions.ExportDepartment then
        CheckIfChanged(Result, swipeEE.Department, Trim(EEData.EEs['CUSTOM_DEPARTMENT_NUMBER']), 'Department');

      CheckIfNotEmptyAndChanged(Result, swipeEE.CardNumber1, GetCardNumber1(EEData), 'Card number 1', FOptions.AllowClearingOfCardNumber1 );
      CheckIfNotEmptyAndChanged(Result, swipeEE.Password, EEData.GetEEAdditionalField(SC_PASSWORD), 'Password', FOptions.AllowClearingOfWebPassword);
      CheckIfNotEmptyAndChanged(Result, swipeEE.CardNumber2, EEData.GetEEAdditionalField(SC_LOGIN_ID), 'Card number 2 (web login)', FOptions.AllowClearingOfWebLogin);
      CheckIfNotEmptyAndChanged(Result, swipeEE.Supervisor, EEData.GetEEAdditionalField(SC_SUPERVISOR), 'Supervisor', FOptions.AllowClearingOfSupervisor);
    end
    else begin
      CheckIfNotEmptyAndChanged(Result, swipeEE.Supervisor, EEData.GetEEAdditionalField(TWP_SUPERVISOR), 'Supervisor', FOptions.AllowClearingOfSupervisor);

      if FSwipeCustomFields.Locate('EeCode', trim(swipeEE.EmployeeCode), []) then
      begin
        if FOptions.ExportRates then
        begin
          ratesTWP := EEData.GetEERatesArrayTWP;

          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate0').AsFloat, ratesTWP[0].rate, 'Default payrate');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate1').AsFloat, ratesTWP[1].rate, 'Payrate1');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate2').AsFloat, ratesTWP[2].rate, 'Payrate2');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate3').AsFloat, ratesTWP[3].rate, 'Payrate3');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate4').AsFloat, ratesTWP[4].rate, 'Payrate4');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate5').AsFloat, ratesTWP[5].rate, 'Payrate5');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate6').AsFloat, ratesTWP[6].rate, 'Payrate6');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate7').AsFloat, ratesTWP[7].rate, 'Payrate7');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate8').AsFloat, ratesTWP[8].rate, 'Payrate8');
          CheckIfFloatChanged(Result, FSwipeCustomFields.FieldByName('PayRate9').AsFloat, ratesTWP[9].rate, 'Payrate9');

          if FOptions.ExportDepartment then
          begin
            if ratesTWP[0].Dep <> '' then
              CheckIfChanged(Result, FSwipeCustomFields.FieldByName('Department').AsString, ratesTWP[0].dep, 'Department')
            else
              CheckIfChanged(Result, FSwipeCustomFields.FieldByName('Department').AsString, Trim(EEData.EEs['CUSTOM_DEPARTMENT_NUMBER']), 'Department');

            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department1').AsString, GetEeRateDep(ratesTWP[1], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_1)), 'Department 1');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department2').AsString, GetEeRateDep(ratesTWP[2], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_2)), 'Department 2');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department3').AsString, GetEeRateDep(ratesTWP[3], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_3)), 'Department 3');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department4').AsString, GetEeRateDep(ratesTWP[4], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_4)), 'Department 4');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department5').AsString, GetEeRateDep(ratesTWP[5], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_5)), 'Department 5');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department6').AsString, GetEeRateDep(ratesTWP[6], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_6)), 'Department 6');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department7').AsString, GetEeRateDep(ratesTWP[7], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_7)), 'Department 7');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department8').AsString, GetEeRateDep(ratesTWP[8], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_8)), 'Department 8');
            CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department9').AsString, GetEeRateDep(ratesTWP[9], EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_9)), 'Department 9');
          end;
        end
        else if FOptions.ExportDepartment then begin
          CheckIfChanged(Result, FSwipeCustomFields.FieldByName('Department').AsString, Trim(EEData.EEs['CUSTOM_DEPARTMENT_NUMBER']), 'Department');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department1').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_1), 'Department 1');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department2').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_2), 'Department 2');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department3').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_3), 'Department 3');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department4').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_4), 'Department 4');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department5').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_5), 'Department 5');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department6').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_6), 'Department 6');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department7').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_7), 'Department 7');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department8').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_8), 'Department 8');
          CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Department9').AsString, EEData.GetEEAdditionalFieldVal(TWP_DEPARTMENT_9), 'Department 9');
        end;

        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home1').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_1), 'Home 1');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home2').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_2), 'Home 2');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home3').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_3), 'Home 3');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home4').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_4), 'Home 4');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home5').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_5), 'Home 5');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home6').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_6), 'Home 6');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home7').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_7), 'Home 7');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home8').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_8), 'Home 8');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Home9').AsString, EEData.GetEEAdditionalFieldVal(TWP_HOME_9), 'Home 9');

        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location1').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 1');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location2').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 2');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location3').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 3');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location4').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 4');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location5').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 5');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location6').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 6');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location7').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 7');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location8').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 8');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('Location9').AsString, EEData.GetEEAdditionalFieldVal(TWP_Location_1), 'Location 9');

        if Trim(VarToStr(EEData.EEs['EMAIL_PRIME'])) <> '' then
          CheckIfChanged(Result, FSwipeCustomFields.FieldByName('Email').AsString, EEData.EEs['EMAIL_PRIME'], 'Email')
        else
          CheckIfChanged(Result, FSwipeCustomFields.FieldByName('Email').AsString, EEData.EEs['EMAIL'], 'Email');
        CheckIfChanged(Result, FSwipeCustomFields.FieldByName('Phone').AsString, EEData.EEs['PHONE'], 'Phone');

        ClockNumbers := GetTWPClockNumbers( EEData, FOptions );
        if ClockNumbers <> '' then
          CheckIfChanged(Result, FSwipeCustomFields.FieldByName('ClockNumbers').AsString, ClockNumbers, 'ClockNumbers');

        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('WebClockEnabled').AsBoolean, EEData.GetEEAdditionalFieldVal(TWP_WEB_CLOCK_ENABLED), 'Web Clock Enabled');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('MobileEnabled').AsBoolean, EEData.GetEEAdditionalFieldVal(TWP_MOBILE_ENABLED), 'Mobile Enabled');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('MobilePunchEnabled').AsBoolean, EEData.GetEEAdditionalFieldVal(TWP_MOBILE_PUNCH_ENABLED), 'Mobile Punch Enabled');
        CheckIfChangedVal(Result, FSwipeCustomFields.FieldByName('GeoDataEnabled').AsBoolean, EEData.GetEEAdditionalFieldVal(TWP_GEODATAENABLED), 'Geo Data Enabled (Gps Mobile)');
      end;
    end;
  end;
var
  i: integer;
  changedFields: TChangedFieldRecs;
  processed: TStringSet;
begin
  Result.Errors := 0;
  Result.Actions := CreateSyncActions('TimeWorks', 'Employee', 'marked as terminated');
  FLogger.LogEntry('Analyzing employee records');
  try
    try
      LogExtAppSettings(FLogger, FExtAppSettings);
      LogEEExportOptions(FLogger, FOptions);
      LogEEFilter(FLogger, FEEFilter);

      processed := nil;
      FSwipeEEs := FConn.GetEmployees;
      RemoveSwipeclockEEsFromOtherCompanies(FSwipeEEs);
      ValidateSwipeClockEECodes(FSwipeEEs);

      if not FExtAppSettings.ConsolidatedDatabase then
        LoadSwipeCustomFields;

      //!! I am afraid the separate handling of terminated and active employees isn't very useful since UpdateEmployeeFields chooses which record to update in a mistical way
      //But at least it first compares records which are visible to the user and don't do anything when they match their Evolution counterparts exactly
      for i := 0 to FSwipeEEs.Count-1 do
        if trim(FSwipeEEs[i].EndDate) = '' then
        begin
          FLogger.LogEntry('Analyzing TimeWorks employee record (active)');
          try
            try
              FLogger.LogContextItem('First Name', FSwipeEEs[i].FirstName);
              FLogger.LogContextItem('Last Name', FSwipeEEs[i].LastName);
              FLogger.LogContextItem(sCtxEECode, FSwipeEEs[i].EmployeeCode);

              SetInclude(processed, trim(FSwipeEEs[i].EmployeeCode) );
              if FEEData.LocateEE(FSwipeEEs[i].EmployeeCode) then
              begin
                if AcceptedByFilter(FLogger, FEEData.EEs, FEEFilter) then
                begin
                  if (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) then
                  begin
                    changedFields := CompareFields(FSwipeEEs[i], FEEData);
                    if Length(changedFields) > 0 then
                      (Result.Actions as ISyncActions).Add(FSwipeEEs[i].EmployeeCode, euaUpdate, changedFields, i);
                  end
                  else
                  begin
                    if not FExtAppSettings.ConsolidatedDatabase then //can't delete the right record if an employee works in two companies
                      (Result.Actions as ISyncActions).Add(FSwipeEEs[i].EmployeeCode, euaDelete, nil); //will set end date
                  end
                end
              end
              else
              begin
                {
                if an employee record isn't in FEEData it could be because it isn't visible to the user of EvoSwipeclock for security reasons.
                So, don't delete it
                --
                The below client is having an issue with the advanced swipeclock api.
                The client has 2 locations miami and ny. The individual below is an admin in ny with no access to miami.
                I have her restricted through security.
                The issue we are having is when she goes to export employees the api is wanting to terminate all the employees in miami.
                }
              end
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end
        end;

      for i := 0 to FSwipeEEs.Count-1 do
        if (trim(FSwipeEEs[i].EndDate) <> '') and not InSet(trim(FSwipeEEs[i].EmployeeCode), processed) then
        begin
          FLogger.LogEntry('Analyzing TimeWorks employee record (inactive)');
          try
            try
              FLogger.LogContextItem('First Name', FSwipeEEs[i].FirstName);
              FLogger.LogContextItem('Last Name', FSwipeEEs[i].LastName);
              FLogger.LogContextItem(sCtxEECode, FSwipeEEs[i].EmployeeCode);

              SetInclude(processed, trim(FSwipeEEs[i].EmployeeCode) );

              if FEEData.LocateEE(FSwipeEEs[i].EmployeeCode) then
              begin
                if  (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) then
                begin
                  if AcceptedByFilter(FLogger, FEEData.EEs, FEEFilter) then
                  begin
                    changedFields := CompareFields(FSwipeEEs[i], FEEData);
                    if Length(changedFields) > 0 then
                      (Result.Actions as ISyncActions).Add(FSwipeEEs[i].EmployeeCode, euaUpdate, changedFields, i);
                  end
                end
                else begin
                  if Trim(VarToStr(FSwipeEEs[i].EndDate)) <> Trim(VarToStr(DateToSwipeClockDate(FEEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime))) then
                  begin
                    SetLength(ChangedFields, 1);
                    ChangedFields[high(ChangedFields)].FieldName := 'End date';
                    ChangedFields[high(ChangedFields)].TargetValue := VarToStr(FSwipeEEs[i].EndDate);
                    ChangedFields[high(ChangedFields)].SourceValue := VarToStr(DateToSwipeClockDate(FEEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime));

                    if not FExtAppSettings.ConsolidatedDatabase then //can't delete the right record if an employee works in two companies
                      (Result.Actions as ISyncActions).Add(FSwipeEEs[i].EmployeeCode, euaUpdate, changedFields); //will set end date
                  end;
                end;
              end;
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end
        end;

      FEEData.EEs.First;
      while not FEEData.EEs.Eof do
      begin
        FLogger.LogEntry('Analyzing Evolution employee record');
        try
          try
            FLogger.LogContextItem(sCtxEECode, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
            if (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) and
               AcceptedByFilter(FLogger, FEEData.EEs, FEEFilter) and
               not IsInSwipeClock( FSwipeEEs, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER'] ) then
              (Result.Actions as ISyncActions).Add( FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER'], euaCreate, nil);
          except
            inc(Result.Errors);
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
        FEEData.EEs.Next;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockEmployeeUpdater.Apply(actions: ISyncActionsRO; pi: IProgressIndicator): TUpdateEmployeesStat;
var
  i: integer;
  termDate: TDateTime;
begin
  Result := BuildEmptySyncStat;
  FLogger.LogEntry('Synchronizing TimeWorks employee records');
  try
    try
      LogExtAppSettings(FLogger, FExtAppSettings);
      LogEEExportOptions(FLogger, FOptions);
      LogEEFilter(FLogger, FEEFilter);
      pi.StartWait('Synchronizing TimeWorks employee records', actions.Count);
      try
        for i := 0 to actions.Count-1 do
        begin
          FLogger.LogEntry('Processing employee');
          try
            try
              FLogger.LogContextItem(sCtxEECode, actions.Get(i).Code);
              FLogger.LogContextItem('Action', SyncActionNames[actions.Get(i).Action]);
              case actions.Get(i).Action of
                euaCreate:
                  begin
                    if not FEEData.LocateEE(actions.Get(i).Code) then
                      Assert(false);
                    if FExtAppSettings.ConsolidatedDatabase then
                      FConn.AddEmployee(FEEData, FOptions, FExtAppSettings)
                    else
                      FConn.UpdateEmployeeFields_api12(ShouldSendPassword( Trim(actions.Get(i).Code) ), FEEData, FOptions, True);

                    inc(Result.Created);
                  end;
                euaUpdate:
                  begin
                    if not FEEData.LocateEE(actions.Get(i).Code) then
                      Assert(false);
                    if FExtAppSettings.ConsolidatedDatabase then
                    begin
                      Assert(actions.Get(i).Tag <> -1);
                      FConn.UpdateEmployee(FSwipeEEs[actions.Get(i).Tag], FEEData, FOptions);
                    end
                    else
                      FConn.UpdateEmployeeFields_api12(False, FEEData, FOptions, False);
                    inc(Result.Modified);
                  end;
                euaDelete:
                  begin
                    Assert(not FExtAppSettings.ConsolidatedDatabase);
                    termDate := Now;
                    if FEEData.LocateEE(actions.Get(i).Code) and not FEEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').IsNull then
                      termDate := FEEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime;
                    FConn.RemoveEmployee(actions.Get(i).Code, termDate);
                    inc(Result.Deleted)
                  end;
              else
                Assert(false);
              end;
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          pi.Step(i+1);
        end;
      finally
        pi.EndWait;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

constructor TSwipeClockEmployeeUpdater.Create(
  const param: TSwipeClockConnectionParam; const options: TEEExportOptions;
  logger: ICommonLogger; EEFilter: TEEFilter; EEData: TEvoEEData; const ExtAppSettings: TExtAppSettings);
begin
  FLogger := logger;
  FConn := TSwipeClockConnection.Create(param, logger);
  FOptions := options;
  FEEFilter := EEFilter;
  FEEData := EEData;
  FExtAppSettings := ExtAppSettings;
  FEEHadInitialPassword := TStringlist.Create;

  CreateSwipeCustomFieldsDS;
end;

destructor TSwipeClockEmployeeUpdater.Destroy;
begin
  FreeAndNil(FConn);
  FSwipeCustomFields.Close;
  FreeAndNil( FSwipeCustomFields );
  FreeAndNil( FEEHadInitialPassword );
  inherited;
end;

procedure PutGlueCustomFieldToDS(logger: ICommonLogger; x: IXMLDocument; period: TPayPeriod; var SwipeCustomFields: TkbmCustomMemTable);

  function GetEEVar(ee: IXmlNode; nm: string): string;
  var
    i: integer;
    states: IXmlNode;
    st: IXmlNode;
    valdt: TDateTime;
    dt: TDateTime;
  begin
    Result := '';
    dt := 0;
    states := ee.ChildNodes['States'];
    for i := 0 to states.ChildNodes.Count-1 do
    begin
      st := states.ChildNodes.Get(i);
      if HasVar(st.ChildNodes['Variables'], nm) then
      begin
        if SameStr(st.ChildValues['EffectiveDate'], '') then
          valdt := 0
        else
          valdt := StdDateToDate(trim(VarToStr(st.ChildValues['EffectiveDate'])));
        if (valdt >= dt) and (valdt <= period.PeriodEnd) then
        begin
          dt := valdt;
          Result := GetVar(st.childNodes['Variables'], nm);
        end;
      end;
    end;
  end;

  function GetClockNumbers(aClockNumbers: IXmlNode): string;
  var
    i: integer;
    node: IXmlNode;
    sl: TStringList;
  begin
    Result := '';
    sl := TStringList.Create;
    try
      for i := 0 to aClockNumbers.ChildNodes.Count-1 do
      begin
        node := aClockNumbers.ChildNodes.Get(i);
        sl.Add(VarToStr(node.NodeValue));
      end;
      if sl.Count > 0 then
      begin
        sl.Sorted := True;
        Result := sl.CommaText;
      end;
    finally
      FreeAndNil(sl);
    end;
  end;

var
  cards: IXMLNode;
  card: IXMLNode;
  ee: IXmlNode;
  MobileEnabled: IXmlNode;
  MobilePunchEnabled: IXmlNode;
  GeoDataEnabled: IXmlNode;
  Email: IXmlNode;
  Phone: IXmlNode;
  EECode: string;
  ClockNumbers: IXmlNode;
  i: integer;

  function GetRate(const fieldName: string): double;
  var
    r: double;
  begin
    if TryStrToFloat(GetEEVar(ee, fieldName), r) then
      Result := r
    else
      Result := 0;
  end;

begin
  if x.DocumentElement = nil then
    exit;
  cards := x.DocumentElement.ChildNodes['TimeCards'];
  if cards = nil then
    exit;
  logger.LogEntry('Getting custom fields from TWP time cards report (GetUnfinalizedPayroll)');
  try
    try
      for i := 0 to cards.ChildNodes.Count-1 do
      begin
        logger.LogEntry('Converting TimeWorks Plus card to Evolution format');
        try
          try
            card := cards.ChildNodes.Get(i);
            ee := card.ChildNodes['Employee'];
            MobileEnabled := ee.ChildNodes['MobileEnabled'];
            MobilePunchEnabled := ee.ChildNodes['MobilePunchEnabled'];
            GeoDataEnabled := ee.ChildNodes['GeoDataEnabled'];
            Email := ee.ChildNodes['Email'];
            ClockNumbers := ee.ChildNodes['ClockNumbers'];
            Phone := ee.ChildNodes['Phone'];
            GeoDataEnabled := ee.ChildNodes['GeoDataEnabled'];

            EECode := trim(VarToStr(ee.Attributes['EmployeeCode']));
            logger.LogContextItem(sCtxEECode, EECode);

            if (VarToStr(ee.ChildValues['EndDate']) <> '') and (StdDateToDate(ee.ChildValues['EndDate']) < period.PeriodBegin) then
            begin
              logger.LogDebug('Employee end date is before period begin date');
              continue;
            end;

            if (VarToStr(ee.ChildValues['BeginDate']) <> '') and (StdDateToDate(ee.ChildValues['BeginDate']) > period.PeriodEnd) then
            begin
              logger.LogDebug('Employee begin date is after period end date');
              continue;
            end;

            if (EECode <> '') and not SwipeCustomFields.Locate('EeCode', EECode, []) then
            try
              SwipeCustomFields.Append;
              SwipeCustomFields.FieldByName('EeCode').AsString := EECode;
              SwipeCustomFields.FieldByName('WebClockEnabled').AsBoolean := StrToBool( trim(VarToStr(ee.Attributes['WebClockEnabled'])) );
              SwipeCustomFields.FieldByName('MobileEnabled').AsBoolean := StrToBool(Trim(VarToStr(MobileEnabled.NodeValue)));
              SwipeCustomFields.FieldByName('MobilePunchEnabled').AsBoolean := StrToBool(Trim(VarToStr(MobilePunchEnabled.NodeValue)));
              SwipeCustomFields.FieldByName('GeoDataEnabled').AsBoolean := StrToBool(Trim(VarToStr(GeoDataEnabled.NodeValue)));
              SwipeCustomFields.FieldByName('Email').AsString := Trim(VarToStr(Email.NodeValue));
              SwipeCustomFields.FieldByName('Phone').AsString := Trim(VarToStr(Phone.NodeValue));
              try
                SwipeCustomFields.FieldByName('ClockNumbers').AsString := GetClockNumbers(ClockNumbers);
              except
                SwipeCustomFields.FieldByName('ClockNumbers').AsString := '';
              end;

              SwipeCustomFields.FieldByName('Home1').AsString := GetEEVar(ee, 'Home1');
              SwipeCustomFields.FieldByName('Home2').AsString := GetEEVar(ee, 'Home2');
              SwipeCustomFields.FieldByName('Home3').AsString := GetEEVar(ee, 'Home3');
              SwipeCustomFields.FieldByName('Home4').AsString := GetEEVar(ee, 'Home4');
              SwipeCustomFields.FieldByName('Home5').AsString := GetEEVar(ee, 'Home5');
              SwipeCustomFields.FieldByName('Home6').AsString := GetEEVar(ee, 'Home6');
              SwipeCustomFields.FieldByName('Home7').AsString := GetEEVar(ee, 'Home7');
              SwipeCustomFields.FieldByName('Home8').AsString := GetEEVar(ee, 'Home8');
              SwipeCustomFields.FieldByName('Home9').AsString := GetEEVar(ee, 'Home9');

              SwipeCustomFields.FieldByName('Department').AsString := GetEEVar(ee, 'Department');
              SwipeCustomFields.FieldByName('Department1').AsString := GetEEVar(ee, 'Department1');
              SwipeCustomFields.FieldByName('Department2').AsString := GetEEVar(ee, 'Department2');
              SwipeCustomFields.FieldByName('Department3').AsString := GetEEVar(ee, 'Department3');
              SwipeCustomFields.FieldByName('Department4').AsString := GetEEVar(ee, 'Department4');
              SwipeCustomFields.FieldByName('Department5').AsString := GetEEVar(ee, 'Department5');
              SwipeCustomFields.FieldByName('Department6').AsString := GetEEVar(ee, 'Department6');
              SwipeCustomFields.FieldByName('Department7').AsString := GetEEVar(ee, 'Department7');
              SwipeCustomFields.FieldByName('Department8').AsString := GetEEVar(ee, 'Department8');
              SwipeCustomFields.FieldByName('Department9').AsString := GetEEVar(ee, 'Department9');

              SwipeCustomFields.FieldByName('Location1').AsString := GetEEVar(ee, 'Location1');
              SwipeCustomFields.FieldByName('Location2').AsString := GetEEVar(ee, 'Location2');
              SwipeCustomFields.FieldByName('Location3').AsString := GetEEVar(ee, 'Location3');
              SwipeCustomFields.FieldByName('Location4').AsString := GetEEVar(ee, 'Location4');
              SwipeCustomFields.FieldByName('Location5').AsString := GetEEVar(ee, 'Location5');
              SwipeCustomFields.FieldByName('Location6').AsString := GetEEVar(ee, 'Location6');
              SwipeCustomFields.FieldByName('Location7').AsString := GetEEVar(ee, 'Location7');
              SwipeCustomFields.FieldByName('Location8').AsString := GetEEVar(ee, 'Location8');
              SwipeCustomFields.FieldByName('Location9').AsString := GetEEVar(ee, 'Location9');

              SwipeCustomFields.FieldByName('PayRate0').AsFloat := GetRate('PayRate0');
              SwipeCustomFields.FieldByName('PayRate1').AsFloat := GetRate('PayRate1');
              SwipeCustomFields.FieldByName('PayRate2').AsFloat := GetRate('PayRate2');
              SwipeCustomFields.FieldByName('PayRate3').AsFloat := GetRate('PayRate3');
              SwipeCustomFields.FieldByName('PayRate4').AsFloat := GetRate('PayRate4');
              SwipeCustomFields.FieldByName('PayRate5').AsFloat := GetRate('PayRate5');
              SwipeCustomFields.FieldByName('PayRate6').AsFloat := GetRate('PayRate6');
              SwipeCustomFields.FieldByName('PayRate7').AsFloat := GetRate('PayRate7');
              SwipeCustomFields.FieldByName('PayRate8').AsFloat := GetRate('PayRate8');
              SwipeCustomFields.FieldByName('PayRate9').AsFloat := GetRate('PayRate9');

              SwipeCustomFields.Post;
            except
              SwipeCustomFields.Cancel;
            end;
          except
            logger.PassthroughException;
          end;
        finally
          logger.LogExit;
        end;
      end;
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

procedure TSwipeClockEmployeeUpdater.LoadSwipeCustomFields;
var
  period: TPayPeriod;
begin
  period.PeriodBegin := Today - 1;
  period.PeriodEnd := Today;

  PutGlueCustomFieldToDS(FLogger, FConn.GetUnfinalizedPayroll(period), period, FSwipeCustomFields);
end;

procedure TSwipeClockEmployeeUpdater.CreateSwipeCustomFieldsDS;
begin
  FSwipeCustomFields := TkbmCustomMemTable.Create(nil);
  CreateStringField( FSwipeCustomFields, 'EeCode', 'EeCode', 32 );

  CreateStringField( FSwipeCustomFields, 'Email', 'Email', 32 );
  CreateBooleanField( FSwipeCustomFields, 'WEBCLOCKENABLED', 'WEBCLOCKENABLED' );
  CreateBooleanField( FSwipeCustomFields, 'MOBILEPUNCHENABLED', 'MOBILEPUNCHENABLED' );
  CreateBooleanField( FSwipeCustomFields, 'MOBILEENABLED', 'MOBILEENABLED' );
  CreateStringField( FSwipeCustomFields, 'ClockNumbers', 'ClockNumbers', 256 );

  CreateStringField( FSwipeCustomFields, 'LOGINS', 'LOGINS', 32 );
  CreateStringField( FSwipeCustomFields, 'ESSPASSWORD', 'ESSPASSWORD', 32 );
  CreateStringField( FSwipeCustomFields, 'ACCRUALFACTOR', 'ACCRUALFACTOR', 32 );
  CreateBooleanField( FSwipeCustomFields, 'GeoDataEnabled', 'GeoDataEnabled' );
  CreateStringField( FSwipeCustomFields, 'Phone', 'Phone', 32 );

  CreateFloatField( FSwipeCustomFields, 'PayRate0', 'PayRate0' );
  CreateFloatField( FSwipeCustomFields, 'PayRate1', 'PayRate1' );
  CreateFloatField( FSwipeCustomFields, 'PayRate2', 'PayRate2' );
  CreateFloatField( FSwipeCustomFields, 'PayRate3', 'PayRate3' );
  CreateFloatField( FSwipeCustomFields, 'PayRate4', 'PayRate4' );
  CreateFloatField( FSwipeCustomFields, 'PayRate5', 'PayRate5' );
  CreateFloatField( FSwipeCustomFields, 'PayRate6', 'PayRate6' );
  CreateFloatField( FSwipeCustomFields, 'PayRate7', 'PayRate7' );
  CreateFloatField( FSwipeCustomFields, 'PayRate8', 'PayRate8' );
  CreateFloatField( FSwipeCustomFields, 'PayRate9', 'PayRate9' );

  CreateStringField( FSwipeCustomFields, 'Home1', 'Home1', 32 );
  CreateStringField( FSwipeCustomFields, 'Home2', 'Home2', 32 );
  CreateStringField( FSwipeCustomFields, 'Home3', 'Home3', 32 );
  CreateStringField( FSwipeCustomFields, 'Home4', 'Home4', 32 );
  CreateStringField( FSwipeCustomFields, 'Home5', 'Home5', 32 );
  CreateStringField( FSwipeCustomFields, 'Home6', 'Home6', 32 );
  CreateStringField( FSwipeCustomFields, 'Home7', 'Home7', 32 );
  CreateStringField( FSwipeCustomFields, 'Home8', 'Home8', 32 );
  CreateStringField( FSwipeCustomFields, 'Home9', 'Home9', 32 );

  CreateStringField( FSwipeCustomFields, 'Department', 'Department', 32 );
  CreateStringField( FSwipeCustomFields, 'Department1', 'Department1', 32 );
  CreateStringField( FSwipeCustomFields, 'Department2', 'Department2', 32 );
  CreateStringField( FSwipeCustomFields, 'Department3', 'Department3', 32 );
  CreateStringField( FSwipeCustomFields, 'Department4', 'Department4', 32 );
  CreateStringField( FSwipeCustomFields, 'Department5', 'Department5', 32 );
  CreateStringField( FSwipeCustomFields, 'Department6', 'Department6', 32 );
  CreateStringField( FSwipeCustomFields, 'Department7', 'Department7', 32 );
  CreateStringField( FSwipeCustomFields, 'Department8', 'Department8', 32 );
  CreateStringField( FSwipeCustomFields, 'Department9', 'Department9', 32 );

  CreateStringField( FSwipeCustomFields, 'Location1', 'Location1', 32 );
  CreateStringField( FSwipeCustomFields, 'Location2', 'Location2', 32 );
  CreateStringField( FSwipeCustomFields, 'Location3', 'Location3', 32 );
  CreateStringField( FSwipeCustomFields, 'Location4', 'Location4', 32 );
  CreateStringField( FSwipeCustomFields, 'Location5', 'Location5', 32 );
  CreateStringField( FSwipeCustomFields, 'Location6', 'Location6', 32 );
  CreateStringField( FSwipeCustomFields, 'Location7', 'Location7', 32 );
  CreateStringField( FSwipeCustomFields, 'Location8', 'Location8', 32 );
  CreateStringField( FSwipeCustomFields, 'Location9', 'Location9', 32 );

  FSwipeCustomFields.Open;
end;

function TSwipeClockEmployeeUpdater.ShouldSendPassword(
  EeCode: string): boolean;
begin
  Result := FEEHadInitialPassword.IndexOf(EeCode) < 0;
  if Result then
    FEEHadInitialPassword.Add(EeCode);
end;

end.
