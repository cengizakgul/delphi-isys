// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\job\IS\integration-release\EvoSwipeclock\2.2\EvoSwipeclock\EmployeeWebInterface.wsdl
// Encoding : utf-8
// Version  : 1.0
// (11/11/2010 10:52:09 PM - 1.33.2.5)
// ************************************************************************ //

unit EmployeeWebInterface;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:float           - "http://www.w3.org/2001/XMLSchema"
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"

  AuthHeader           = class;                 { "https://mc2cs.com/scci"[H] }



  // ************************************************************************ //
  // Namespace : https://mc2cs.com/scci
  // ************************************************************************ //
  AuthHeader = class(TSOAPHeader)
  private
    FuserName: WideString;
    Fpassword: WideString;
    Fsite: WideString;
  published
    property userName: WideString read FuserName write FuserName;
    property password: WideString read Fpassword write Fpassword;
    property site: WideString read Fsite write Fsite;
  end;

  ArrayOfString = array of WideString;          { "https://mc2cs.com/scci" }

  // ************************************************************************ //
  // Namespace : https://mc2cs.com/scci
  // soapAction: https://mc2cs.com/scci/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : EmployeeWebInterfaceSoap
  // service   : EmployeeWebInterface
  // port      : EmployeeWebInterfaceSoap
  // URL       : http://www.payrollservers.us:8002/scci/xml/EmployeeWebInterface.asmx
  // ************************************************************************ //
  EmployeeWebInterfaceSoap = interface(IInvokable)
  ['{69FAA31D-17A5-35A0-4044-582F83FEB7CD}']
    function  GetEmployees: WideString; stdcall;
    function  GetEmployees2(const ExcludeTerminated: Boolean): WideString; stdcall;
    function  GetEmployeesByFilter(const filterBy: WideString; const filter: WideString): WideString; stdcall;
    function  GetEmployeesByFilter2(const filterBy: WideString; const filter: WideString; const ExcludeTerminated: Boolean): WideString; stdcall;
    function  GetEmployeesOverload(const doesNothing: WideString): WideString; stdcall;
    function  GetEmployee(const employee: WideString): WideString; stdcall;
    function  GetEmployeeCard(const employeeCode: WideString; const period: WideString): WideString; stdcall;
    function  AddEmployee(const employeeCode: WideString; const lastName: WideString; const firstName: WideString; const middleName: WideString; const designation: WideString; const title: WideString; const ssn: WideString; const dept: WideString; const location: WideString; const supervisor: WideString; 
                          const startDate: WideString; const endDate: WideString; const lunch: Integer; const lunchHours: Single; const payRate0: TXSDecimal; const payRate1: TXSDecimal; const payRate2: TXSDecimal; const payRate3: TXSDecimal; const cardnum1: WideString; 
                          const cardNum2: WideString; const cardNum3: WideString; const password: WideString; const options: WideString; const home1: WideString; const home2: WideString; const home3: WideString; const schedule: WideString; const exportBlock: Integer
                          ): WideString; stdcall;
    function  AddEmployeeRequired(const lastName: WideString; const firstName: WideString): WideString; stdcall;
    function  RemoveEmployee(const employeeCode: WideString; const delDate: WideString): WideString; stdcall;
    function  GetOneTimeCredential(const login: WideString): WideString; stdcall;
    function  GetPayPeriod(const theDate: WideString): WideString; stdcall;
    function  UpdateSiteSetting(const FieldToUpdate: WideString; const NewValue: WideString): WideString; stdcall;
    function  UpdateEmployeeFields(const AddIfNotFound: Boolean; const employeeCode: WideString; const lastName: WideString; const firstName: WideString; const middleName: WideString; const FieldsToUpdate: ArrayOfString): WideString; stdcall;
    function  UpdateEmployee(const employeeCode: WideString; const employee: Integer; const lastName: WideString; const firstName: WideString; const middleName: WideString; const designation: WideString; const title: WideString; const ssn: WideString; const dept: WideString; const location: WideString; 
                             const supervisor: WideString; const startDate: WideString; const endDate: WideString; const lunch: Integer; const lunchHours: Single; const payRate0: TXSDecimal; const payRate1: TXSDecimal; const payRate2: TXSDecimal; const payRate3: TXSDecimal; 
                             const cardnum1: WideString; const cardNum2: WideString; const cardNum3: WideString; const password: WideString; const options: WideString; const home1: WideString; const home2: WideString; const home3: WideString; const schedule: WideString; 
                             const exportBlock: Integer): WideString; stdcall;
    function  GetSiteInfo: WideString; stdcall;
    function  GetSiteCards(const startDate: WideString; const endDate: WideString): WideString; stdcall;
    function  GetSiteCardsFiltered(const startDate: WideString; const endDate: WideString; const eeListMatchField: WideString; const eeList: WideString): WideString; stdcall;
    function  GetActivityFile(const fileName: WideString; const startDate: WideString; const endDate: WideString): WideString; stdcall;
    function  GetActivityFileLaborMapped(const fileName: WideString; const startDate: WideString; const endDate: WideString; const laborMapping: WideString): WideString; stdcall;
    function  UpdateEmployeeTime(const punchID: Integer; const employee: Integer; const inPunch: WideString; const outPunch: WideString; const punchDate: WideString; const iData: WideString; const jData: WideString; const kData: WideString; const xData: WideString; const yData: WideString; 
                                 const zData: WideString; const hours: WideString; const lunch: Integer; const payrate: WideString; const category: WideString; const pay: WideString; const payType: WideString): WideString; stdcall;
    function  UpdateEmployeeTimeUTC(const punchID: Integer; const employee: Integer; const inPunch: WideString; const outPunch: WideString; const punchDate: WideString; const iData: WideString; const jData: WideString; const kData: WideString; const xData: WideString; const yData: WideString; 
                                    const zData: WideString; const hours: WideString; const lunch: Integer; const payrate: WideString; const category: WideString; const pay: WideString; const payType: WideString): WideString; stdcall;
    function  GetPunchInfo(const punchID: Integer): WideString; stdcall;
    function  DeleteEmployeePunch(const punchID: Integer): WideString; stdcall;
    function  GetSites(const login: WideString): WideString; stdcall;
    function  GetActiveEmployeeCount(const year: WideString; const month: WideString): WideString; stdcall;
  end;

function GetEmployeeWebInterfaceSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): EmployeeWebInterfaceSoap;


implementation

function GetEmployeeWebInterfaceSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): EmployeeWebInterfaceSoap;
const
  defWSDL = 'E:\job\IS\integration-release\EvoSwipeclock\2.2\EvoSwipeclock\EmployeeWebInterface.wsdl';
  defURL  = 'http://www.payrollservers.us:8002/scci/xml/EmployeeWebInterface.asmx';
  defSvc  = 'EmployeeWebInterface';
  defPrt  = 'EmployeeWebInterfaceSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as EmployeeWebInterfaceSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(EmployeeWebInterfaceSoap), 'https://mc2cs.com/scci', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(EmployeeWebInterfaceSoap), 'https://mc2cs.com/scci/%operationName%');
  InvRegistry.RegisterHeaderClass(TypeInfo(EmployeeWebInterfaceSoap), AuthHeader, 'AuthHeader', '');
  RemClassRegistry.RegisterXSClass(AuthHeader, 'https://mc2cs.com/scci', 'AuthHeader');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfString), 'https://mc2cs.com/scci', 'ArrayOfString');

end. 