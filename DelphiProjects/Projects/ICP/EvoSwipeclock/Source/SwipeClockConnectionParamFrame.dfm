inherited SwipeClockConnectionParamFrm: TSwipeClockConnectionParamFrm
  Width = 385
  Height = 105
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 385
    Height = 105
    Align = alClient
    Caption = 'TimeWorks'
    TabOrder = 0
    object lblUserName: TLabel
      Left = 14
      Top = 45
      Width = 48
      Height = 13
      Caption = 'Username'
    end
    object lblPassword: TLabel
      Left = 14
      Top = 69
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object lblSite: TLabel
      Left = 14
      Top = 21
      Width = 36
      Height = 13
      Caption = 'Client #'
    end
    object PasswordEdit: TPasswordEdit
      Left = 143
      Top = 69
      Width = 124
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object UserNameEdit: TEdit
      Left = 143
      Top = 44
      Width = 124
      Height = 21
      TabOrder = 1
    end
    object cbSavePassword: TCheckBox
      Left = 270
      Top = 71
      Width = 97
      Height = 17
      Caption = 'Save password'
      TabOrder = 3
    end
    object SiteEdit: TEdit
      Left = 143
      Top = 21
      Width = 124
      Height = 21
      TabOrder = 0
    end
  end
end
