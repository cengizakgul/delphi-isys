unit TCImportOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, swipeclockdecl, ExtCtrls, swipeclockeefilterframe,
  SwipeclockLaborMappingFrame, RateImportOptionFrame, OptionsBaseFrame,
  swipeclock, swipeclockconnection, kbmMemTable, SwipeclockNumericMappingFrame, db;

type
  TTCImportOptionsFrm = class(TOptionsBaseFrm)
    EEFilterFrame: TSwipeClockEEFilterFrm;
    Panel1: TPanel;
    cbSummarize: TCheckBox;
    LaborMappingFrame: TSwipeclockLaborMappingFrm;
    Bevel4: TBevel;
    RateImportOptionFrame: TRateImportOptionFrm;
    cbAllowImport: TCheckBox;
    NumericMappingFrame: TSwipeclockNumericMappingFrm;
    cbAutoImportJobCodes: TCheckBox;
  private
    FParam: TSwipeClockConnectionParam;
    function GetOptions: TTCImportOptions;
  public
    constructor Create( Owner: TComponent ); override;

    property Options: TTCImportOptions read GetOptions;
    function IsValid: boolean;
    procedure Clear(enable: boolean);

    procedure Init(const Value: TTCImportOptions; ctx: TTCImportOptionsContext; CO_E_D_CODESClone: TkbmMemTable {takes ownership});
    procedure UpdateBranchAndDepartmentCodes(ees: IXMLGetEmployeesDataSet);
    function IsInitializedWithParams(Param: TSwipeClockConnectionParam): boolean;

  end;

implementation

{$R *.dfm}

uses
  gdycommon;

{ TTCImportOptionsFrm }

procedure TTCImportOptionsFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    cbSummarize.Checked := false;
    RateImportOptionFrame.Clear;
    LaborMappingFrame.Clear;
    NumericMappingFrame.Clear;
    EEFilterFrame.Clear;
    cbAllowImport.Checked := false;
    cbAutoImportJobCodes.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

constructor TTCImportOptionsFrm.Create(Owner: TComponent);
begin
  FBlockOnChange := true;
  try
    inherited;
    Clear(false);
  finally
    FBlockOnChange := false;
  end;
end;

function TTCImportOptionsFrm.GetOptions: TTCImportOptions;
begin
  FBlockOnChange := true;
  try
    Result.Summarize := cbSummarize.Checked;
    Result.RateImport := RateImportOptionFrame.Value;
    Result.LaborMapping := LaborMappingFrame.LaborMapping;
    Result.EEFilter := EEFilterFrame.EEFilter;
    Result.AllowImportToBatchesWithUserEarningLines := cbAllowImport.Checked;
    Result.NumericMapping := NumericMappingFrame.NumericMapping;
    Result.AutoImportJobCodes := cbAutoImportJobCodes.Checked;
  finally
    FBlockOnChange := false;
  end;
end;

procedure TTCImportOptionsFrm.Init(const Value: TTCImportOptions; ctx: TTCImportOptionsContext; CO_E_D_CODESClone: TkbmMemTable);
begin
  FBlockOnChange := true;
  try
    try
      FParam := ctx.Param;
      EnableControlRecursively(Self, true);

      EEFilterFrame.EEFilter := Value.EEFilter;
      cbSummarize.Checked := Value.Summarize;
      RateImportOptionFrame.Value := Value.RateImport;
      LaborMappingFrame.SetFieldDefinitions(ctx.LaborVars);
      LaborMappingFrame.LaborMapping := Value.LaborMapping;
      cbAllowImport.Checked := Value.AllowImportToBatchesWithUserEarningLines;
      cbAutoImportJobCodes.Checked := Value.AutoImportJobCodes;
      EEFilterFrame.SetBranchAndDepartmentCodes(ctx.ees);
    except
      FreeAndNil(CO_E_D_CODESClone);
      raise;
    end;
    NumericMappingFrame.Init(CO_E_D_CODESClone, ctx.NumericVars);
    NumericMappingFrame.NumericMapping := Value.NumericMapping;
  finally
    FBlockOnChange := false;
  end;
end;

function TTCImportOptionsFrm.IsInitializedWithParams(Param: TSwipeClockConnectionParam): boolean;
begin
  Result := Enabled and AreConnectionSettingsTheSame(FParam, Param);
end;

function TTCImportOptionsFrm.IsValid: boolean;
begin
  Result := EEFilterFrame.IsValid;
end;

procedure TTCImportOptionsFrm.UpdateBranchAndDepartmentCodes(
  ees: IXMLGetEmployeesDataSet);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    EEFilterFrame.SetBranchAndDepartmentCodes(ees);
  finally
    FBlockOnChange := false;
  end;
end;

end.
