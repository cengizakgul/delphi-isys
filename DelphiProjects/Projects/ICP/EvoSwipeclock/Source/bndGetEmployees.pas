
{******************************************************************************}
{                                                                              }
{                               XML Data Binding                               }
{                                                                              }
{         Generated on: 3/16/2010 1:01:32 PM                                   }
{       Generated from: E:\job\IS\integration\EvoSwipeclock\GetEmployees.xsd   }
{                                                                              }
{******************************************************************************}

unit bndGetEmployees;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLNewDataSet = interface;
  IXMLEmployees = interface;

{ IXMLNewDataSet }

  IXMLNewDataSet = interface(IXMLNodeCollection)
    ['{3B7CD6EA-412D-4C18-83D8-BCE536D4DC19}']
    { Property Accessors }
    function Get_Employees(Index: Integer): IXMLEmployees;
    { Methods & Properties }
    function Add: IXMLEmployees;
    function Insert(const Index: Integer): IXMLEmployees;
    property Employees[Index: Integer]: IXMLEmployees read Get_Employees; default;
  end;

{ IXMLEmployees }

  IXMLEmployees = interface(IXMLNode)
    ['{E52045C6-C6D6-426C-8998-7151233360A1}']
    { Property Accessors }
    function Get_Employee: Integer;
    function Get_EmployeeCode: WideString;
    function Get_FirstName: WideString;
    function Get_LastName: WideString;
    function Get_MiddleName: WideString;
    function Get_Designation: WideString;
    function Get_Title: WideString;
    function Get_SSN: WideString;
    function Get_Department: WideString;
    function Get_Location: WideString;
    function Get_Supervisor: WideString;
    function Get_StartDate: WideString;
    function Get_EndDate: WideString;
    function Get_LunchMinutes: Integer;
    function Get_AutoLunchHours: Double;
    function Get_PayRate0: WideString;
    function Get_AlternatePayRates: WideString;
    function Get_PayRate1: WideString;
    function Get_PayRate2: WideString;
    function Get_PayRate3: WideString;
    function Get_CardNumber1: WideString;
    function Get_CardNumber2: WideString;
    function Get_CardNumber3: WideString;
    function Get_Password: WideString;
    function Get_Home1: WideString;
    function Get_Home2: WideString;
    function Get_Home3: WideString;
    function Get_Schedule: WideString;
    function Get_ExportBlock: Boolean;
    function Get_Options: WideString;
    procedure Set_Employee(Value: Integer);
    procedure Set_EmployeeCode(Value: WideString);
    procedure Set_FirstName(Value: WideString);
    procedure Set_LastName(Value: WideString);
    procedure Set_MiddleName(Value: WideString);
    procedure Set_Designation(Value: WideString);
    procedure Set_Title(Value: WideString);
    procedure Set_SSN(Value: WideString);
    procedure Set_Department(Value: WideString);
    procedure Set_Location(Value: WideString);
    procedure Set_Supervisor(Value: WideString);
    procedure Set_StartDate(Value: WideString);
    procedure Set_EndDate(Value: WideString);
    procedure Set_LunchMinutes(Value: Integer);
    procedure Set_AutoLunchHours(Value: Double);
    procedure Set_PayRate0(Value: WideString);
    procedure Set_AlternatePayRates(Value: WideString);
    procedure Set_PayRate1(Value: WideString);
    procedure Set_PayRate2(Value: WideString);
    procedure Set_PayRate3(Value: WideString);
    procedure Set_CardNumber1(Value: WideString);
    procedure Set_CardNumber2(Value: WideString);
    procedure Set_CardNumber3(Value: WideString);
    procedure Set_Password(Value: WideString);
    procedure Set_Home1(Value: WideString);
    procedure Set_Home2(Value: WideString);
    procedure Set_Home3(Value: WideString);
    procedure Set_Schedule(Value: WideString);
    procedure Set_ExportBlock(Value: Boolean);
    procedure Set_Options(Value: WideString);
    { Methods & Properties }
    property Employee: Integer read Get_Employee write Set_Employee;
    property EmployeeCode: WideString read Get_EmployeeCode write Set_EmployeeCode;
    property FirstName: WideString read Get_FirstName write Set_FirstName;
    property LastName: WideString read Get_LastName write Set_LastName;
    property MiddleName: WideString read Get_MiddleName write Set_MiddleName;
    property Designation: WideString read Get_Designation write Set_Designation;
    property Title: WideString read Get_Title write Set_Title;
    property SSN: WideString read Get_SSN write Set_SSN;
    property Department: WideString read Get_Department write Set_Department;
    property Location: WideString read Get_Location write Set_Location;
    property Supervisor: WideString read Get_Supervisor write Set_Supervisor;
    property StartDate: WideString read Get_StartDate write Set_StartDate;
    property EndDate: WideString read Get_EndDate write Set_EndDate;
    property LunchMinutes: Integer read Get_LunchMinutes write Set_LunchMinutes;
    property AutoLunchHours: Double read Get_AutoLunchHours write Set_AutoLunchHours;
    property PayRate0: WideString read Get_PayRate0 write Set_PayRate0;
    property AlternatePayRates: WideString read Get_AlternatePayRates write Set_AlternatePayRates;
    property PayRate1: WideString read Get_PayRate1 write Set_PayRate1;
    property PayRate2: WideString read Get_PayRate2 write Set_PayRate2;
    property PayRate3: WideString read Get_PayRate3 write Set_PayRate3;
    property CardNumber1: WideString read Get_CardNumber1 write Set_CardNumber1;
    property CardNumber2: WideString read Get_CardNumber2 write Set_CardNumber2;
    property CardNumber3: WideString read Get_CardNumber3 write Set_CardNumber3;
    property Password: WideString read Get_Password write Set_Password;
    property Home1: WideString read Get_Home1 write Set_Home1;
    property Home2: WideString read Get_Home2 write Set_Home2;
    property Home3: WideString read Get_Home3 write Set_Home3;
    property Schedule: WideString read Get_Schedule write Set_Schedule;
    property ExportBlock: Boolean read Get_ExportBlock write Set_ExportBlock;
    property Options: WideString read Get_Options write Set_Options;
  end;

{ Forward Decls }

  TXMLNewDataSet = class;
  TXMLEmployees = class;

{ TXMLNewDataSet }

  TXMLNewDataSet = class(TXMLNodeCollection, IXMLNewDataSet)
  protected
    { IXMLNewDataSet }
    function Get_Employees(Index: Integer): IXMLEmployees;
    function Add: IXMLEmployees;
    function Insert(const Index: Integer): IXMLEmployees;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmployees }

  TXMLEmployees = class(TXMLNode, IXMLEmployees)
  protected
    { IXMLEmployees }
    function Get_Employee: Integer;
    function Get_EmployeeCode: WideString;
    function Get_FirstName: WideString;
    function Get_LastName: WideString;
    function Get_MiddleName: WideString;
    function Get_Designation: WideString;
    function Get_Title: WideString;
    function Get_SSN: WideString;
    function Get_Department: WideString;
    function Get_Location: WideString;
    function Get_Supervisor: WideString;
    function Get_StartDate: WideString;
    function Get_EndDate: WideString;
    function Get_LunchMinutes: Integer;
    function Get_AutoLunchHours: Double;
    function Get_PayRate0: WideString;
    function Get_AlternatePayRates: WideString;
    function Get_PayRate1: WideString;
    function Get_PayRate2: WideString;
    function Get_PayRate3: WideString;
    function Get_CardNumber1: WideString;
    function Get_CardNumber2: WideString;
    function Get_CardNumber3: WideString;
    function Get_Password: WideString;
    function Get_Home1: WideString;
    function Get_Home2: WideString;
    function Get_Home3: WideString;
    function Get_Schedule: WideString;
    function Get_ExportBlock: Boolean;
    function Get_Options: WideString;
    procedure Set_Employee(Value: Integer);
    procedure Set_EmployeeCode(Value: WideString);
    procedure Set_FirstName(Value: WideString);
    procedure Set_LastName(Value: WideString);
    procedure Set_MiddleName(Value: WideString);
    procedure Set_Designation(Value: WideString);
    procedure Set_Title(Value: WideString);
    procedure Set_SSN(Value: WideString);
    procedure Set_Department(Value: WideString);
    procedure Set_Location(Value: WideString);
    procedure Set_Supervisor(Value: WideString);
    procedure Set_StartDate(Value: WideString);
    procedure Set_EndDate(Value: WideString);
    procedure Set_LunchMinutes(Value: Integer);
    procedure Set_AutoLunchHours(Value: Double);
    procedure Set_PayRate0(Value: WideString);
    procedure Set_AlternatePayRates(Value: WideString);
    procedure Set_PayRate1(Value: WideString);
    procedure Set_PayRate2(Value: WideString);
    procedure Set_PayRate3(Value: WideString);
    procedure Set_CardNumber1(Value: WideString);
    procedure Set_CardNumber2(Value: WideString);
    procedure Set_CardNumber3(Value: WideString);
    procedure Set_Password(Value: WideString);
    procedure Set_Home1(Value: WideString);
    procedure Set_Home2(Value: WideString);
    procedure Set_Home3(Value: WideString);
    procedure Set_Schedule(Value: WideString);
    procedure Set_ExportBlock(Value: Boolean);
    procedure Set_Options(Value: WideString);
  end;

{ Global Functions }

function GetNewDataSet(Doc: IXMLDocument): IXMLNewDataSet;
function LoadNewDataSet(const FileName: WideString): IXMLNewDataSet;
function NewNewDataSet: IXMLNewDataSet;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetNewDataSet(Doc: IXMLDocument): IXMLNewDataSet;
begin
  Result := Doc.GetDocBinding('NewDataSet', TXMLNewDataSet, TargetNamespace) as IXMLNewDataSet;
end;

function LoadNewDataSet(const FileName: WideString): IXMLNewDataSet;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('NewDataSet', TXMLNewDataSet, TargetNamespace) as IXMLNewDataSet;
end;

function NewNewDataSet: IXMLNewDataSet;
begin
  Result := NewXMLDocument.GetDocBinding('NewDataSet', TXMLNewDataSet, TargetNamespace) as IXMLNewDataSet;
end;

{ TXMLNewDataSet }

procedure TXMLNewDataSet.AfterConstruction;
begin
  RegisterChildNode('Employees', TXMLEmployees);
  ItemTag := 'Employees';
  ItemInterface := IXMLEmployees;
  inherited;
end;

function TXMLNewDataSet.Get_Employees(Index: Integer): IXMLEmployees;
begin
  Result := List[Index] as IXMLEmployees;
end;

function TXMLNewDataSet.Add: IXMLEmployees;
begin
  Result := AddItem(-1) as IXMLEmployees;
end;

function TXMLNewDataSet.Insert(const Index: Integer): IXMLEmployees;
begin
  Result := AddItem(Index) as IXMLEmployees;
end;

{ TXMLEmployees }

function TXMLEmployees.Get_Employee: Integer;
begin
  Result := ChildNodes['Employee'].NodeValue;
end;

procedure TXMLEmployees.Set_Employee(Value: Integer);
begin
  ChildNodes['Employee'].NodeValue := Value;
end;

function TXMLEmployees.Get_EmployeeCode: WideString;
begin
  Result := ChildNodes['EmployeeCode'].Text;
end;

procedure TXMLEmployees.Set_EmployeeCode(Value: WideString);
begin
  ChildNodes['EmployeeCode'].NodeValue := Value;
end;

function TXMLEmployees.Get_FirstName: WideString;
begin
  Result := ChildNodes['FirstName'].Text;
end;

procedure TXMLEmployees.Set_FirstName(Value: WideString);
begin
  ChildNodes['FirstName'].NodeValue := Value;
end;

function TXMLEmployees.Get_LastName: WideString;
begin
  Result := ChildNodes['LastName'].Text;
end;

procedure TXMLEmployees.Set_LastName(Value: WideString);
begin
  ChildNodes['LastName'].NodeValue := Value;
end;

function TXMLEmployees.Get_MiddleName: WideString;
begin
  Result := ChildNodes['MiddleName'].Text;
end;

procedure TXMLEmployees.Set_MiddleName(Value: WideString);
begin
  ChildNodes['MiddleName'].NodeValue := Value;
end;

function TXMLEmployees.Get_Designation: WideString;
begin
  Result := ChildNodes['Designation'].Text;
end;

procedure TXMLEmployees.Set_Designation(Value: WideString);
begin
  ChildNodes['Designation'].NodeValue := Value;
end;

function TXMLEmployees.Get_Title: WideString;
begin
  Result := ChildNodes['Title'].Text;
end;

procedure TXMLEmployees.Set_Title(Value: WideString);
begin
  ChildNodes['Title'].NodeValue := Value;
end;

function TXMLEmployees.Get_SSN: WideString;
begin
  Result := ChildNodes['SSN'].Text;
end;

procedure TXMLEmployees.Set_SSN(Value: WideString);
begin
  ChildNodes['SSN'].NodeValue := Value;
end;

function TXMLEmployees.Get_Department: WideString;
begin
  Result := ChildNodes['Department'].Text;
end;

procedure TXMLEmployees.Set_Department(Value: WideString);
begin
  ChildNodes['Department'].NodeValue := Value;
end;

function TXMLEmployees.Get_Location: WideString;
begin
  Result := ChildNodes['Location'].Text;
end;

procedure TXMLEmployees.Set_Location(Value: WideString);
begin
  ChildNodes['Location'].NodeValue := Value;
end;

function TXMLEmployees.Get_Supervisor: WideString;
begin
  Result := ChildNodes['Supervisor'].Text;
end;

procedure TXMLEmployees.Set_Supervisor(Value: WideString);
begin
  ChildNodes['Supervisor'].NodeValue := Value;
end;

function TXMLEmployees.Get_StartDate: WideString;
begin
  Result := ChildNodes['StartDate'].Text;
end;

procedure TXMLEmployees.Set_StartDate(Value: WideString);
begin
  ChildNodes['StartDate'].NodeValue := Value;
end;

function TXMLEmployees.Get_EndDate: WideString;
begin
  Result := ChildNodes['EndDate'].Text;
end;

procedure TXMLEmployees.Set_EndDate(Value: WideString);
begin
  ChildNodes['EndDate'].NodeValue := Value;
end;

function TXMLEmployees.Get_LunchMinutes: Integer;
begin
  Result := ChildNodes['LunchMinutes'].NodeValue;
end;

procedure TXMLEmployees.Set_LunchMinutes(Value: Integer);
begin
  ChildNodes['LunchMinutes'].NodeValue := Value;
end;

function TXMLEmployees.Get_AutoLunchHours: Double;
begin
  Result := ChildNodes['AutoLunchHours'].NodeValue;
end;

procedure TXMLEmployees.Set_AutoLunchHours(Value: Double);
begin
  ChildNodes['AutoLunchHours'].NodeValue := Value;
end;

function TXMLEmployees.Get_PayRate0: WideString;
begin
  Result := ChildNodes['PayRate0'].Text;
end;

procedure TXMLEmployees.Set_PayRate0(Value: WideString);
begin
  ChildNodes['PayRate0'].NodeValue := Value;
end;

function TXMLEmployees.Get_AlternatePayRates: WideString;
begin
  Result := ChildNodes['AlternatePayRates'].Text;
end;

procedure TXMLEmployees.Set_AlternatePayRates(Value: WideString);
begin
  ChildNodes['AlternatePayRates'].NodeValue := Value;
end;

function TXMLEmployees.Get_PayRate1: WideString;
begin
  Result := ChildNodes['PayRate1'].Text;
end;

procedure TXMLEmployees.Set_PayRate1(Value: WideString);
begin
  ChildNodes['PayRate1'].NodeValue := Value;
end;

function TXMLEmployees.Get_PayRate2: WideString;
begin
  Result := ChildNodes['PayRate2'].Text;
end;

procedure TXMLEmployees.Set_PayRate2(Value: WideString);
begin
  ChildNodes['PayRate2'].NodeValue := Value;
end;

function TXMLEmployees.Get_PayRate3: WideString;
begin
  Result := ChildNodes['PayRate3'].Text;
end;

procedure TXMLEmployees.Set_PayRate3(Value: WideString);
begin
  ChildNodes['PayRate3'].NodeValue := Value;
end;

function TXMLEmployees.Get_CardNumber1: WideString;
begin
  Result := ChildNodes['CardNumber1'].Text;
end;

procedure TXMLEmployees.Set_CardNumber1(Value: WideString);
begin
  ChildNodes['CardNumber1'].NodeValue := Value;
end;

function TXMLEmployees.Get_CardNumber2: WideString;
begin
  Result := ChildNodes['CardNumber2'].Text;
end;

procedure TXMLEmployees.Set_CardNumber2(Value: WideString);
begin
  ChildNodes['CardNumber2'].NodeValue := Value;
end;

function TXMLEmployees.Get_CardNumber3: WideString;
begin
  Result := ChildNodes['CardNumber3'].Text;
end;

procedure TXMLEmployees.Set_CardNumber3(Value: WideString);
begin
  ChildNodes['CardNumber3'].NodeValue := Value;
end;

function TXMLEmployees.Get_Password: WideString;
begin
  Result := ChildNodes['Password'].Text;
end;

procedure TXMLEmployees.Set_Password(Value: WideString);
begin
  ChildNodes['Password'].NodeValue := Value;
end;

function TXMLEmployees.Get_Home1: WideString;
begin
  Result := ChildNodes['Home1'].Text;
end;

procedure TXMLEmployees.Set_Home1(Value: WideString);
begin
  ChildNodes['Home1'].NodeValue := Value;
end;

function TXMLEmployees.Get_Home2: WideString;
begin
  Result := ChildNodes['Home2'].Text;
end;

procedure TXMLEmployees.Set_Home2(Value: WideString);
begin
  ChildNodes['Home2'].NodeValue := Value;
end;

function TXMLEmployees.Get_Home3: WideString;
begin
  Result := ChildNodes['Home3'].Text;
end;

procedure TXMLEmployees.Set_Home3(Value: WideString);
begin
  ChildNodes['Home3'].NodeValue := Value;
end;

function TXMLEmployees.Get_Schedule: WideString;
begin
  Result := ChildNodes['Schedule'].Text;
end;

procedure TXMLEmployees.Set_Schedule(Value: WideString);
begin
  ChildNodes['Schedule'].NodeValue := Value;
end;

function TXMLEmployees.Get_ExportBlock: Boolean;
begin
  Result := ChildNodes['ExportBlock'].NodeValue;
end;

procedure TXMLEmployees.Set_ExportBlock(Value: Boolean);
begin
  ChildNodes['ExportBlock'].NodeValue := Value;
end;

function TXMLEmployees.Get_Options: WideString;
begin
  Result := ChildNodes['Options'].Text;
end;

procedure TXMLEmployees.Set_Options(Value: WideString);
begin
  ChildNodes['Options'].NodeValue := Value;
end;

end.