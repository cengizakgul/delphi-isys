unit SwipeclockTasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, common,
  evodata, classes, scheduledCustomTask, swipeclockdecl, timeclockimport, eefilter;

type
  TSwipeClockTaskAdapter = class(TTaskAdapterCustomBase)
  private
    function GetSwipeclockConnectionParam: TSwipeClockConnectionParam;
    function GetExtAppSettings: TExtAppSettings;
  protected
    function GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData; override;
  published
    procedure EEExport_Execute;
    function EEExport_Describe: string;
    function TCImport_Describe: string;
  end;


function NewEEExportTask(ClCo: TEvoCompanyDef; eefilter: TEEFilter; EEExportOptions: TEEExportOptions): IScheduledTask;
function NewTCImportTask(ClCo: TEvoCompanyDef; TCImportOptions: TTCImportOptions): IScheduledTask;


procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; EvoData: TEvoData; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection,
  evoapiconnectionutils, gdyclasses, gdyCommon,
  gdyDialogEngine, controls, kbmMemTable, swipeclock, EEExportDialog, TCImportDialog;


function NewEEExportTask(ClCo: TEvoCompanyDef; eefilter: TEEFilter; EEExportOptions: TEEExportOptions): IScheduledTask;
begin
  Result := NewTask('EEExport', 'Employee Export', ClCo); //TSwipeClockTaskAdapter.EEExport
  SaveEEFilter(eefilter, Result.ParamSettings, '');
  SaveEEExportOptions( EEExportOptions, Result.ParamSettings, '' );
end;

function NewTCImportTask(ClCo: TEvoCompanyDef; TCImportOptions: TTCImportOptions): IScheduledTask;
begin
  Result := NewTask('TCImport', 'Timeclock Data Import', ClCo); //TADITaskExecutor.TCImport
  SaveTCImportOptions( TCImportOptions, Result.ParamSettings, '' );
end;

procedure EditEEExportTask(task: IScheduledTask; EvoData: TEvoData; Owner: TComponent);
var
  dlg: TEEExportDlg;
  CUSTOM_DBDT: TkbmMemTable;
begin
  CUSTOM_DBDT := EvoData.CloneCompanyTable(task.Company, 'CUSTOM_DBDT');
  try
    dlg := TEEExportDlg.Create(nil);
    try
      dlg.Caption := task.UserFriendlyName + ' Task Parameters';
      dlg.EeFilterFrame.Init( CUSTOM_DBDT, LoadEEFilter(task.ParamSettings, '') );
      dlg.EEExportOptionsFrame.Options := LoadEEExportOptions(task.ParamSettings, '');
      with DialogEngine(dlg, Owner) do
        if ShowModal = mrOk then
        begin
          SaveEEFilter( dlg.EeFilterFrame.GetFilter, task.ParamSettings, '' );
          SaveEEExportOptions( dlg.EEExportOptionsFrame.Options, task.ParamSettings, '' );
        end
    finally
      FreeAndNil(dlg);
    end;
  finally
    FreeAndNil(CUSTOM_DBDT);
  end;
end;

procedure EditTCImportTask(task: IScheduledTask; EvoData: TEvoData; Logger: ICommonLogger; Owner: TComponent);
var
  options: TTCImportOptions;
  dlg: TTCImportDlg;
  ctx: TTCImportOptionsContext;
begin
  dlg := TTCImportDlg.Create(nil);                                   
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    ctx := GetTCImportOptionsContext(LoadConnectionSettings(AppSettings, CompanyUniquePath(task.Company)), logger);
    options := LoadTCImportOptions(task.ParamSettings, '', ctx.DatabaseType);
    dlg.TCImportOptionsFrame.Init(options, ctx, EvoData.CloneCompanyTable(task.Company, 'CO_E_D_CODES'){transfers ownership!} );
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveTCImportOptions(dlg.TCImportOptionsFrame.Options, task.ParamSettings, '');
  finally
    FreeAndNil(dlg);
  end;
end;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; EvoData: TEvoData; Owner: TComponent);
begin
  if task.Name = 'TCImport' then
    EditTCImportTask(task, EvoData, Logger, Owner)
  else if task.Name = 'EEExport' then
    EditEEExportTask(task, EvoData, Owner)
  else
    Assert(false);
end;

{ TSwopeClockTaskAdapter }

function TSwipeClockTaskAdapter.EEExport_Describe: string;
begin
  Result := DescribeEEExportOptions( LoadEEExportOptions(FTask.ParamSettings, '') );
  if Result <> '' then
    Result := Result + #13#10;
  Result := Result + DescribeEEFilter( LoadEEFilter(FTask.ParamSettings, '') );
end;

procedure TSwipeClockTaskAdapter.EEExport_Execute;
var
  opname: string;
  EEFilter: TEEFilter;
  EeExportOptions: TEEExportOptions;

  procedure DoIt;
  var
    stat: TUpdateEmployeesStat;
    EEData: TEvoEEData;
    analysisResult: TAnalyzeResult;
    updater: TSwipeClockEmployeeUpdater;
  begin
    EEData := TEvoEEData.Create( GetEvoAPICOnnection, FLogger, FTask.Company);
    try
      updater := TSwipeClockEmployeeUpdater.Create(GetSwipeclockConnectionParam, EeExportOptions, FLogger, EeFilter, EEData, GetExtAppSettings );
      try
        analysisResult := updater.Analyze;
        if analysisResult.Actions.Count > 0 then
          FLogger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );

        stat := updater.Apply(analysisResult.Actions, NullProgressIndicator);
        stat.Errors := stat.Errors + analysisResult.Errors;
      finally
        FreeAndNil(updater);
      end;
      if stat.Errors = 0 then
        FLogger.LogEvent(opname + ' ' + SyncStatToString(stat))
      else
        FLogger.LogWarning(opname + ' ' + SyncStatToString(stat));
    finally
      FreeAndNil(EEData);
    end;
  end;

begin
  opname := Format('%s employee records update', ['TimeWorks']);
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      EeExportOptions := LoadEEExportOptions(FTask.ParamSettings, '');
      EEFilter := LoadEEFilter(FTask.ParamSettings, '');

      DoIt;
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TSwipeClockTaskAdapter.GetSwipeclockConnectionParam: TSwipeClockConnectionParam;
begin
  Result := LoadConnectionSettings(FSettings, CompanyUniquePath(FTask.Company));
end;

function TSwipeClockTaskAdapter.TCImport_Describe: string;
var
  options: TTCImportOptions;
begin
  options := LoadTCImportOptions(FTask.ParamSettings, '', GetDatabaseType(LoadConnectionSettings(AppSettings, CompanyUniquePath(FTask.Company)), FLogger) );
  Result := DescribeTCImportOptions(options);
end;

function TSwipeClockTaskAdapter.GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData;
var
  options: TTCImportOptions;
begin
  options := LoadTCImportOptions(FTask.ParamSettings, '', GetDatabaseType(LoadConnectionSettings(AppSettings, CompanyUniquePath(FTask.Company)), FLogger) );
  if batch.HasUserEarningsLines and not options.AllowImportToBatchesWithUserEarningLines then //!! that's ugly that this is here but I need tc import options for this and I don't have them in the base class
    raise Exception.Create( 'This payroll batch already has checks with earnings. You may be doing import second time.' );
  Result := swipeclock.GetTimeClockData(GetSwipeclockConnectionParam, options, FLogger, FTask.Company, batch.PayPeriod, GetExtAppSettings);
end;

function TSwipeClockTaskAdapter.GetExtAppSettings: TExtAppSettings;
begin
  Result := LoadExtAppSettings(FSettings, CompanyUniquePath(FTask.Company));
end;

end.
