program EvoSwipeclock;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  common,
  MidasLib,
  scheduledTask,
  gdyRedir,
  jclfileutils,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EmployeeWebServiceInterface in 'EmployeeWebServiceInterface.pas',
  swipeclock in 'swipeclock.pas',
  SwipeClockConnectionParamFrame in 'SwipeClockConnectionParamFrame.pas' {SwipeClockConnectionParamFrm: TOptionsBaseFrm},
  timeclockimport in '..\..\common\timeclockimport.pas',
  bndGetSites in 'bndGetSites.pas',
  bndGetSiteCards in 'bndGetSiteCards.pas',
  evoapiconnectionutils in '..\..\common\evoapiconnectionutils.pas',
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  swipeclockdecl in 'swipeclockdecl.pas',
  tcconnectionbase in '..\..\common\tcconnectionbase.pas',
  TCImportOptionsFrame in 'TCImportOptionsFrame.pas' {TCImportOptionsFrm: TOptionsBaseFrm},
  swipeclockeefilterframe in 'swipeclockeefilterframe.pas' {SwipeClockEEFilterFrm: TFrame},
  SwipeclockConnection in 'SwipeclockConnection.pas',
  multipleChoiceFrame in '..\..\common\multipleChoiceFrame.pas' {MultipleChoiceFrm: TFrame},
  SwipeclockLaborMappingFrame in 'SwipeclockLaborMappingFrame.pas' {SwipeclockLaborMappingFrm: TFrame},
  SwipeclockLaborMappingItemFrame in 'SwipeclockLaborMappingItemFrame.pas' {SwipeclockLaborMappingItemFrm: TFrame},
  gdyDialogHolderBaseForm in '..\..\common\gdycommon\dialogs\gdyDialogHolderBaseForm.pas' {DialogHolderBase},
  gdyDialogHolderForm in '..\..\common\gdycommon\dialogs\gdyDialogHolderForm.pas' {DialogHolder},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  gdyMessageDialogFrame in '..\..\common\gdycommon\dialogs\gdyMessageDialogFrame.pas' {MessageDialog: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvoTCImportMainNewForm in '..\..\common\EvoTCImportMainNewForm.pas' {EvoTCImportMainNewFm},
  MainForm in 'MainForm.pas' {MainFm},
  EEExportOptionsFrame in 'EEExportOptionsFrame.pas' {EEExportOptionsFrm: TOptionsBaseFrm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  SwipeclockTasks in 'SwipeclockTasks.pas',
  scheduledCustomTask in '..\..\common\scheduledCustomTask.pas',
  EEExportDialog in 'EEExportDialog.pas' {EEExportDlg: TFrame},
  TCImportDialog in 'TCImportDialog.pas' {TCImportDlg: TFrame},
  PlannedActions in '..\..\common\PlannedActions.pas',
  PlannedActionConfirmationForm in '..\..\common\PlannedActionConfirmationForm.pas' {PlannedActionConfirmationFm},
  bndGetEmployees in 'bndGetEmployees.pas',
  CodesFilterFrame in '..\..\common\CodesFilterFrame.pas' {CodesFilterFrm: TFrame},
  EESalaryFilterFrame in '..\..\common\EESalaryFilterFrame.pas' {EESalaryFilterFrm: TFrame},
  DBDTFilterFrame in '..\..\common\DBDTFilterFrame.pas' {DBDTFilterFrm: TFrame},
  EEFilter in '..\..\common\EEFilter.pas',
  EEFilterFrame in '..\..\common\EEFilterFrame.pas' {EeFilterFrm: TFrame},
  SwipeclockEEExportFilterFrame in 'SwipeclockEEExportFilterFrame.pas' {SwipeclockEEExportFilterFrm: TFrame},
  SwipeclockNumericMappingFrame in 'SwipeclockNumericMappingFrame.pas' {SwipeclockNumericMappingFrm: TFrame},
  EmployeeWebInterface in 'EmployeeWebInterface.pas',
  ExtAppSettingsFrame in 'ExtAppSettingsFrame.pas' {ExtAppSettingsFrm: TFrame},
  api12 in 'api12.pas',
  scheduler in 'scheduler.pas';

{$R *.res}

begin
  LicenseKey := '3BF7BA482B114156B387859D47357D37'; //advanced swipeclock
//{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}

  ConfigVersion := 2;
  FeatureSetName := 'Advanced';

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}
 
  Application.Initialize;
  Application.Title := 'Evolution TimeWorks&Plus Import';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
  Application.Run;
  end
  else
  begin
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TSwipeClockTaskAdapter.Create));
  end
end.
