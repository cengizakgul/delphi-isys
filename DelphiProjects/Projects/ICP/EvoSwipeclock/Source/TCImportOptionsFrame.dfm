inherited TCImportOptionsFrm: TTCImportOptionsFrm
  Width = 939
  Height = 304
  object Bevel4: TBevel
    Left = 514
    Top = 0
    Width = 8
    Height = 304
    Align = alLeft
    Shape = bsSpacer
  end
  inline EEFilterFrame: TSwipeClockEEFilterFrm
    Left = 522
    Top = 0
    Width = 417
    Height = 304
    Align = alClient
    TabOrder = 1
    inherited gbEmployeeFilter: TGroupBox
      Width = 417
      Height = 304
      inherited Bevel1: TBevel
        Width = 413
      end
      inherited pnlRadioButtons: TPanel
        Width = 413
      end
      inherited ChoiceFrame: TMultipleChoiceFrm
        Width = 413
        Height = 254
        inherited pnlAvailable: TPanel
          Height = 254
          inherited lbAvailable: TListBox
            Height = 229
          end
        end
        inherited pnlButtons: TPanel
          Height = 254
        end
        inherited pnlSelected: TPanel
          Width = 188
          Height = 254
          inherited pnlSelectedHeader: TPanel
            Width = 188
          end
          inherited lbSelected: TListBox
            Width = 188
            Height = 229
          end
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 304
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object cbSummarize: TCheckBox
      Left = 262
      Top = 234
      Width = 81
      Height = 17
      Caption = 'Summarize'
      TabOrder = 3
    end
    inline LaborMappingFrame: TSwipeclockLaborMappingFrm
      Left = 0
      Top = 0
      Width = 257
      Height = 300
      Constraints.MaxHeight = 300
      Constraints.MinWidth = 200
      TabOrder = 0
      inherited GroupBox1: TGroupBox
        Width = 257
        inherited RateCodeFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited BranchFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited DivisionFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited ShiftFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited JobFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited departmentFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited WCCodeFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
        inherited TeamCodeFieldFrame: TSwipeclockLaborMappingItemFrm
          Width = 253
          inherited Panel1: TPanel
            Width = 156
            inherited cbFields: TComboBox
              Width = 150
            end
          end
        end
      end
    end
    inline RateImportOptionFrame: TRateImportOptionFrm
      Left = 261
      Top = 131
      Width = 255
      Height = 98
      TabOrder = 2
      inherited RadioGroup1: TRadioGroup
        Left = 1
        Width = 252
        Height = 98
      end
    end
    object cbAllowImport: TCheckBox
      Left = 262
      Top = 271
      Width = 243
      Height = 31
      Caption = 'Allow import to batches that already have checks with earnings'
      TabOrder = 5
      WordWrap = True
    end
    inline NumericMappingFrame: TSwipeclockNumericMappingFrm
      Left = 261
      Top = 0
      Width = 253
      Height = 124
      TabOrder = 1
      inherited gbNumericMapping: TGroupBox
        Width = 253
        Height = 124
        inherited dgNM: TReDBGrid
          Width = 249
          Height = 107
        end
      end
      inherited dsrcEDCodes: TDataSource
        Left = 40
      end
      inherited dsrcCO_E_D_CODES: TDataSource
        Left = 32
      end
    end
    object cbAutoImportJobCodes: TCheckBox
      Left = 262
      Top = 255
      Width = 147
      Height = 17
      Caption = 'Auto import job codes'
      TabOrder = 4
    end
  end
end
