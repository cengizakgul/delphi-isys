@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoSwipeclock
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoSwipeclock.exe
cd ..\..\..\Projects\ICP\EvoSwipeclock\Installer\Projects

SET TEMPPATH=..\..\..\..\..\Tmp

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoTimeWorks.msi 

"%pWiXDir%\candle.exe" .\EvoSwipeclock.wxs -wx -out %TEMPPATH%\EvoSwipeclock.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -var var.Path -out %TEMPPATH%\EvoSwipeclockRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvoSwipeclockRwQueries.wxs -wx -dPath="..\..\Resources\Queries" -out %TEMPPATH%\EvoSwipeclockRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir "..\..\..\Common\HTML Templates" -gg -sfrag -srd -cg HTMLTemplatesGroup -dr HTMLTEMPLATESDIR -var var.Path -out %TEMPPATH%\EvoSwipeclockHTMLTemplates.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" %TEMPPATH%\EvoSwipeclockHTMLTemplates.wxs -wx -out %TEMPPATH%\EvoSwipeclockHTMLTemplates.wixobj -dPath="..\..\..\Common\HTML Templates"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" %TEMPPATH%\EvoSwipeclock.wixobj %TEMPPATH%\EvoSwipeclockRwQueries.wixobj %TEMPPATH%\EvoSwipeclockHTMLTemplates.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoSwipeclock.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoSwipeclock.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoTimeWorks_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoTimeWorks_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoSwipeclock.msi EvoTimeWorks_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
