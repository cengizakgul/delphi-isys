@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

ECHO Creating EvoWorkersChoice.msi 
"%pWiXDir%\candle.exe" .\EvoWorkersChoice.wxs -wx -out ..\..\..\..\..\Tmp\EvoWorkersChoice.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoWorkersChoice.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoice.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoice.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoice%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoice%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoice.msi EvoWorkersChoice%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvoWorkersChoiceService.msi 
"%pWiXDir%\candle.exe" .\EvoWorkersChoiceService.wxs -wx -out ..\..\..\..\..\Tmp\EvoWorkersChoiceService.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoWorkersChoiceService.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoiceService.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoiceService.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoiceService%pFilePrefix%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoiceService%pFilePrefix%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoWorkersChoiceService.msi EvoWorkersChoiceService%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1
