program EvoConnectionSettings;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  ConnectionDialog in 'ConnectionDialog.pas' {frmSettings},
  EvoWorkersChoiceCommon in '..\Source\EvoWorkersChoiceCommon.pas';

{$R ..\Source\EvoWorkersChoice.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfrmSettings, frmSettings);
  Application.Run;
end.
