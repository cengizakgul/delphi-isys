unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  EvoWorkersChoiceProc, WorkersChoiceXmlRpcServerMod, common, gdycommonlogger,
  isSettings, Winsvc;

type
  TEvoWorkersChoice = class(TService, ICommonLogger)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
  private
    EvoConnectionParam: TEvoAPIConnectionParam;
    FEvoWorkersChoiceProc: TEvoWorkersChoiceProc;
    FWorkersChoiceXmlRpcServer: TWorkersChoiceXmlRPCServer;

    {gdyLogWriters.TFileLogOutput}
    FLog: Text;
    FFileOpened: boolean;
    procedure FileNeeded;
    procedure CloseFileTemporarily;
    procedure LogMessage(const s: string);

    procedure RunWorkersChoiceXmlRpcServer;
    procedure AutoConnect;

    {ICommonLogger}
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call

    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string; d: string = '' );
    procedure LogEventFmt( s: string; const args: array of const; d: string = '' );
    procedure LogWarning( s: string; d: string = '' );
    procedure LogWarningFmt( s: string; const args: array of const; d: string = '' );
    procedure LogError( s: string; d: string = '' );
    procedure LogErrorFmt( s: string; const args: array of const; d: string = '' );
    procedure LogDebug( s: string; fulltext: string = '' );

    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  EvoWorkersChoice: TEvoWorkersChoice;

implementation

uses EvoWorkersChoiceCommon, gdyRedir, registry;

{$R *.DFM}

const
  cKey='Form.Button';

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  EvoWorkersChoice.Controller(CtrlCode);
end;

procedure TEvoWorkersChoice.AutoConnect;
begin
  if Assigned( FEvoWorkersChoiceProc ) then
  try
    LogMessage('Connecting to Evo...');
    FEvoWorkersChoiceProc.Connect(EvoConnectionParam);
  except
    //
  end;
end;

procedure TEvoWorkersChoice.CloseFileTemporarily;
begin
  if FFileOpened then
  try
    system.Flush( FLog );
    system.CloseFile( FLog );
    FFileOpened := false;
  except
    //do nothing
  end;
end;

procedure TEvoWorkersChoice.FileNeeded;
var
  fn: string;
begin
  if not FFileOpened then
    try
      fn := Redirection.GetFilename(sLogFileAlias);
      ForceDirectories( ExtractFilePath(fn) );
      system.assignfile( FLog, fn );
//      if FileExists(fn) then
 //       system.Append( FLog )
//      else
        system.Rewrite( FLog );
      FFileOpened := true;  
    except
      //do nothing
    end;
end;

function TEvoWorkersChoice.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TEvoWorkersChoice.LogContextItem(tag, value: string);
begin
  LogMessage(tag + ' = ' + value);
end;

procedure TEvoWorkersChoice.LogDebug(s, fulltext: string);
begin
  LogEvent(s, fulltext);
end;

procedure TEvoWorkersChoice.LogEntry(blockname: string);
begin
  LogMessage(blockname + ' -> (');
end;

procedure TEvoWorkersChoice.LogError(s, d: string);
begin
  LogEvent( 'Error: ' + s, d );
end;

procedure TEvoWorkersChoice.LogErrorFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( 'Error: ' + Format( s, args ), d );
end;

procedure TEvoWorkersChoice.LogEvent(s, d: string);
begin
  if d <> '' then
    LogMessage(s + ', details: ' + d)
  else
    LogMessage(s);
end;

procedure TEvoWorkersChoice.LogEventFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( Format( s, args ), d );
end;

procedure TEvoWorkersChoice.LogExit;
begin
  LogMessage(') <-');
end;

procedure TEvoWorkersChoice.LogMessage(const s: string);
begin
  try
    FileNeeded;
    system.Writeln( FLog, s );
    system.Flush( FLog );
  except
    //do nothing
  end;
end;

procedure TEvoWorkersChoice.LogWarning(s, d: string);
begin
  LogEvent('Warning! ' + s, d);
end;

procedure TEvoWorkersChoice.LogWarningFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( 'Warning! ' + Format( s, args ), d );
end;

procedure TEvoWorkersChoice.PassthroughException;
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
  begin
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
    raise Exception.Create((ExceptObject as Exception).Message);
  end;
end;

procedure TEvoWorkersChoice.PassthroughExceptionAndWarnFmt(s: string;
  const args: array of const);
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
  begin
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
    raise Exception.Create((ExceptObject as Exception).Message);
  end;
end;

procedure TEvoWorkersChoice.RunWorkersChoiceXmlRpcServer;
begin
  FWorkersChoiceXmlRpcServer.EvoWorkersChoiceProc := FEvoWorkersChoiceProc;
  FWorkersChoiceXmlRpcServer.Active := True;
  LogMessage('XML RPC Server started...');
end;

procedure TEvoWorkersChoice.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  EvoConnectionParam := LoadEvoConnectionParam;

  FWorkersChoiceXmlRpcServer := TWorkersChoiceXmlRPCServer.Create;
  FEvoWorkersChoiceProc := TEvoWorkersChoiceProc.Create( Self );

  RunWorkersChoiceXmlRpcServer;
  AutoConnect;

  Started := FWorkersChoiceXmlRpcServer.Active and FEvoWorkersChoiceProc.Connected;
end;

procedure TEvoWorkersChoice.ServiceStop(Sender: TService;
  var Stopped: Boolean);
begin
  FWorkersChoiceXmlRpcServer.Active := False;
  FreeAndNil(FWorkersChoiceXmlRpcServer);
  CloseFileTemporarily;
end;

procedure TEvoWorkersChoice.StopException;
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
end;

procedure TEvoWorkersChoice.StopExceptionAndWarnFmt(s: string;
  const args: array of const);
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
end;


//DeleteNTService(ServiceName: String):boolean;
// ServiceName - ��� ������� ����������� ��������
//���������:
// true - ���� ��������� ��������� �������
// false - ���� ���� ������. ����� ���������� call �� GetLastError �����
// ��������������� �� �������� ������

function DeleteNTService(ServiceName: string): boolean;
var
  hServiceToDelete, hSCMgr: SC_HANDLE;
  ss: _SERVICE_STATUS;
begin
  Result := false;
  hSCMgr := OpenSCManager(nil, nil, SC_MANAGER_CREATE_SERVICE);
  if (hSCMgr <> 0) then
  try
    hServiceToDelete := OpenService(hSCMgr, PChar(ServiceName), SERVICE_ALL_ACCESS);
    if hServiceToDelete <> 0 then
    begin
      QueryServiceStatus( hServiceToDelete, SS );
      if ss.dwCurrentState = SERVICE_RUNNING then
        ControlService(hServiceToDelete, SERVICE_CONTROL_STOP, SS);
      DeleteService(hServiceToDelete);
    end;
  finally
    CloseServiceHandle(hSCMgr);
  end;
end;

end.
