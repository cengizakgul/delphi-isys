program EvoWorkersChoiceService;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  SvcMgr,
  main in 'main.pas' {EvoWorkersChoice: TService},
  EvoWorkersChoiceCommon in '..\Source\EvoWorkersChoiceCommon.pas',
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  XmlRpcServer in '..\Source\XmlRpcServer.pas';

{$R ..\Source\EvoWorkersChoice.RES}

begin
  LicenseKey := '2FD79336BB844142AB861AB1AAECF2DA'; // EvoWorkersChoice License

  Application.Initialize;
  Application.CreateForm(TEvoWorkersChoice, EvoWorkersChoice);
  Application.Run;
end.
