IdSSLOpenSSLHeaders.pas 

function Load: boolean;

// the functions below are not loaded and that causes "Could not load SSL" error
...
    (@IdSslCtxSetInfoCallback<>nil) and
    (@IdSslX509StoreCtxGetAppData<>nil) and
    (@IdSslSessionGetId<>nil) and
    (@IdSslSessionGetIdCtx<>nil) and
    (@IdSslCtxGetVersion<>nil) and
    (@IdSslCtxSetOptions<>nil) and

    (@IdSslX509GetNotBefore<>nil) and
    (@IdSslX509GetNotAfter<>nil) and

    (@iddes_set_odd_parity <>nil) and
    (@iddes_set_key<>nil) and
    (@iddes_ecb_encrypt<>nil) and
...
