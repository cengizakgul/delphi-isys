unit EvoWorkersChoiceProc;

interface

uses
  common, evoapiconnection, gdycommonlogger, kbmMemTable, DB,
  XmlRpcTypes, evoapiconnectionutils;

type
  IEvoWorkersChoiceProc = interface
    function applyDataChangePacket(aChanges: IRpcArray): IRpcStruct;
    procedure OpenClient(cl_nbr: integer);
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function Connected: boolean;
  end;

  TEvoWorkersChoiceProc = class(TInterfacedObject, IEvoWorkersChoiceProc)
  private
    FConn: IEvoAPIConnection;
    FLogger: ICommonLogger;
  public
    function applyDataChangePacket(aChanges: IRpcArray): IRpcStruct;
    procedure OpenClient(cl_nbr: integer);
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;

    function Connected: boolean;
    function Connection: IEvoAPIConnection;
    procedure Connect(param: TEvoAPIConnectionParam);
    procedure Disconnect;

    constructor Create(Logger: ICommonLogger);
    destructor Destroy; override;
  end;

implementation

uses SysUtils, Variants, DateUtils, gdyRedir, gdyGlobalWaitIndicator;

{ TEvoWorkersChoiceProc }

function TEvoWorkersChoiceProc.applyDataChangePacket(
  aChanges: IRpcArray): IRpcStruct;
begin
  Result := nil;
  if aChanges.Count = 0 then
    FLogger.LogWarning('Data change packet should not be empty.')
  else begin
    Result := FConn.applyDataChangePacket( aChanges );
  end;
end;

procedure TEvoWorkersChoiceProc.Connect(param: TEvoAPIConnectionParam);
begin
  Disconnect;

//  WaitIndicator.StartWait('Connecting to Evolution');
  try
    FConn := CreateEvoAPIConnection(param, FLogger);
  except
    Disconnect;
//    WaitIndicator.EndWait;
  end;
end;

function TEvoWorkersChoiceProc.Connected: boolean;
begin
  Result := assigned(FConn);
end;

function TEvoWorkersChoiceProc.Connection: IEvoAPIConnection;
begin
  Assert(FConn <> nil);
  Result := FConn;
end;

constructor TEvoWorkersChoiceProc.Create(Logger: ICommonLogger);
begin
  FLogger := Logger;
end;

destructor TEvoWorkersChoiceProc.Destroy;
begin
  FConn := nil;
  FLogger := nil;
  inherited;
end;

procedure TEvoWorkersChoiceProc.Disconnect;
begin
  FConn := nil;
end;

procedure TEvoWorkersChoiceProc.OpenClient(cl_nbr: integer);
begin
  FConn.OpenClient(cl_nbr);
end;

function TEvoWorkersChoiceProc.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FConn.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

end.
