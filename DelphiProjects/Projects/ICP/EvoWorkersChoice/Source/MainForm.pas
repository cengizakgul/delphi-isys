unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientMainForm, ComCtrls, OptionsBaseFrame, evodata,
  EvoAPIConnectionParamFrame, ExtCtrls, ActnList, StdCtrls,
  Buttons, EvoWorkersChoiceProc, WorkersChoiceXmlRpcServerMod, XmlRpcTypes;

type
  TMainFm = class(TEvoAPIClientMainFm)
    pnlMain: TPanel;
    Actions: TActionList;
    ConnectToEvo: TAction;
    btnConnect: TBitBtn;
    btnCreateNewClient: TBitBtn;
    createNewClientDB: TAction;
    lblNewClient: TLabel;
    pnlGetRecordNbr: TPanel;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    edTableName: TEdit;
    Label2: TLabel;
    edF1Name: TEdit;
    edF1Value: TEdit;
    Label3: TLabel;
    edF2Name: TEdit;
    edF2Value: TEdit;
    Label4: TLabel;
    edF3Name: TEdit;
    edF3Value: TEdit;
    GetRecordNbr: TAction;
    lblGetRecordNbrResult: TLabel;
    procedure ConnectToEvoUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    FEvoWorkersChoiceProc: TEvoWorkersChoiceProc;
    FWorkersChoiceXmlRpcServer: TWorkersChoiceXmlRPCServer;

    procedure RunWorkersChoiceXmlRpcServer;
    procedure AutoConnect;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses EEUtilityLoggerViewFrame, gdyDeferredCall;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  FLoggerFrameClass := TEEUtilityLoggerViewFrm;
  inherited;
  FWorkersChoiceXmlRpcServer := TWorkersChoiceXmlRPCServer.Create;
  FEvoWorkersChoiceProc := TEvoWorkersChoiceProc.Create(Logger);

  FWorkersChoiceXmlRpcServer.EvoWorkersChoiceProc := FEvoWorkersChoiceProc;

  if EvoFrame.IsValid then
    PageControl1.TabIndex := TabSheet2.TabIndex
  else
    PageControl1.TabIndex := TabSheet3.TabIndex;
end;

destructor TMainFm.Destroy;
begin
  FWorkersChoiceXmlRpcServer.Active := False;
  FreeAndNil(FWorkersChoiceXmlRpcServer);

  inherited;
end;

procedure TMainFm.ConnectToEvoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and (Assigned(FEvoWorkersChoiceProc) and not FEvoWorkersChoiceProc.Connected);
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  Logger.LogEntry( Format('User clicked "%s"', [(Sender as TCustomAction).Caption]) );
  try
    try
      FEvoWorkersChoiceProc.Connect(EvoFrame.Param);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunWorkersChoiceXmlRpcServer;
begin
  FWorkersChoiceXmlRpcServer.EvoWorkersChoiceProc := FEvoWorkersChoiceProc;
  FWorkersChoiceXmlRpcServer.Active := True;
end;

procedure TMainFm.FormShow(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePage := TabSheet3;

  DeferredCall( RunWorkersChoiceXmlRpcServer );
  DeferredCall( AutoConnect );
end;

procedure TMainFm.AutoConnect;
begin
  Logger.LogEntry( 'Auto Connect' );
  try
    if Assigned( FEvoWorkersChoiceProc ) then
    try
      FEvoWorkersChoiceProc.Connect(EvoFrame.Param);
      btnConnect.Caption := 'Reconnect to Evo';
    except
      btnConnect.Caption := 'Connect to Evo';
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.
