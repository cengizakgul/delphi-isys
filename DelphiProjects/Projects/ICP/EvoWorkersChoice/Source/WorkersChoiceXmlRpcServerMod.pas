unit WorkersChoiceXmlRpcServerMod;

interface

uses
  XmlRpcServer, XmlRpcTypes, Classes, SysUtils, EvoWorkersChoiceProc, Dialogs,
  kbmMemTable, DB;

const
  sCompanyCode = 'CompanyCode';
  sFirstName = 'FirstName';
  sLastName = 'LastName';
  sLast5DigitsOfSSN = 'Last5DigitsOfSSN';
  sEDCode = 'EDCode';
  sAmount = 'Amount';
  sStartDate = 'StartDate';
  sGoal = 'Goal';
  sClientNbr = 'ClientNbr';
  sCompanyNbr = 'CompanyNbr';
  sAnnualIncome = 'AnnualIncome';
  sHireDate = 'HireDate';

type
  WrongSignatureException = class(Exception);
  WrongSessionException = class(Exception);
  WrongLicenseException = class(Exception);
  WrongReturnTypeException = class(Exception);
  WrongReportLevelException = class(Exception);
  WrongPackageNumberException = class(Exception);
  WrongAPIKeyException = class(Exception);

  UnsupportedParamTypeException = class(Exception);
  UnsupportedOutputParamTypeException = class(Exception);
  UnsupportedFunctionException = class(Exception);

  NotAllowedEEException = class(Exception);
  TableNameEmptyException = class(Exception);
  NotSSUserException = class(Exception);

  QueueTaskNotFoundException = class(Exception);
  QueueTaskNotFinishedException = class(Exception);
  CannotConvertNbrToIdException = class(Exception);
  CannotConvertIdToNbrException = class(Exception);

  UnknownParamTypeException = class(Exception);
  ResultWrongStructureException = class(Exception);
  ResultIsEmptyException = class(Exception);
  EmptyDatasetException = class(Exception);

  UnknownCoStorageTag = class(Exception);
  CannotGetDataDueTimeout = class(Exception);
  UnknownEEChangeRequestType = class(Exception);

  LegacyCallsNotAllowed = class(Exception);
  ESSNotEnabledException = class(Exception);
  SecurityCallsNotAllowed = class(Exception);

  WrongParametersGetRecordNbrException = class(Exception);
  UnsupportedTableNameException = class(Exception);
  APIException = class(Exception);


  TevRPCMethod = procedure (const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String) of object;
  PTevRPCMethod = ^TevRPCMethod;

  TWorkersChoiceXmlRPCServer = class
  private
    FRpcServer: TRpcServer;
    FMethods: TStringList;
    FEvoWorkersChoiceProc: IEvoWorkersChoiceProc;

    procedure GetClNbrByCompany(const aCustomCompanyNumber: string; var ClNbr: integer; var CoNbr: integer);
    function GetEeNbrBySSN(const aLast5DigitsOfSSN: string; aCoNbr: integer): integer;
    function GetSchedEdNbr(EeNbr: integer; const aEDCode: string): integer;
    function GetEdNbr(const aEDCode: string): integer;
    function GetDate(const aStrDate: string): TDatetime;

    procedure RegisterMethods;
    procedure RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);

    procedure VerifyStructMember(aStruct: IRpcStruct; const aName: string; aDataType: TDataType; Mandatory: boolean = True);

    procedure IsEeEligible(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure ImportScheduledED(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);

    procedure APICheckCondition(const ACondition: Boolean; AExceptClass: ExceptClass; const aMessage: string = '');

    function GetActive: boolean;
    procedure SetActive(AValue: boolean);
  public
    constructor Create;
    destructor Destroy; override;

    property Active: boolean read GetActive write SetActive;
    property EvoWorkersChoiceProc: IEvoWorkersChoiceProc read FEvoWorkersChoiceProc write FEvoWorkersChoiceProc;
  end;

implementation

uses gdyRedir, common, EvoWorkersChoiceCommon;

procedure CheckCondition(const ACondition: Boolean; const AErrMessage: String; AExceptClass: ExceptClass = nil; AHelpContext: Integer = 0);
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      AExceptClass := Exception;

    if AHelpContext = 0 then
      raise AExceptClass.Create(AErrMessage)
    else
      raise AExceptClass.CreateHelp(AErrMessage, AHelpContext);
  end;
end;

function GetNextStrValue(var AStr: string; const ADelim: String = ','): String;
var
  j: Integer;
begin
  j := Pos(ADelim, AStr);
  if j = 0 then
  begin
    Result := AStr;
    AStr := '';
  end

  else
  begin
    Result := Copy(AStr, 1, j - 1);
    Delete(AStr, 1, j - 1 + Length(ADelim));
  end;
end;

{ TWorkersChoiceXmlRPCServer }

procedure TWorkersChoiceXmlRPCServer.APICheckCondition(const ACondition: Boolean;
  AExceptClass: ExceptClass; const aMessage: string = '');
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      raise Exception.Create('APICheckCondition: exception class should be assigned!')
    else begin
      if Trim(aMessage) <> '' then
        raise AExceptClass.Create('APIException: ' + aMessage)
      else
        raise AExceptClass.Create('APIException');
    end;
  end;
end;

constructor TWorkersChoiceXmlRPCServer.Create;
begin
  inherited Create;

  FMethods := TStringList.Create;
  FMethods.Duplicates := dupError;
  FMethods.CaseSensitive := False;
  FMethods.Sorted := True;

  FRpcServer := TRpcServer.Create();

  RegisterMethods;
end;

destructor TWorkersChoiceXmlRPCServer.Destroy;
begin
  FEvoWorkersChoiceProc := nil;
  FreeAndNil(FRpcServer);
  FreeAndNil(FMethods);
  inherited;
end;

function TWorkersChoiceXmlRPCServer.GetActive: boolean;
begin
  Result := FRpcServer.Active;
end;

procedure TWorkersChoiceXmlRPCServer.GetClNbrByCompany(const aCustomCompanyNumber: string; var ClNbr: integer; var CoNbr: integer);
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  ClNbr := 0;
  CoNbr := 0;
  param := TRpcStruct.Create;
  Param.AddItem( 'CoNumber', Trim(aCustomCompanyNumber) );
  FClCoData :=  FEvoWorkersChoiceProc.RunQuery('TmpCoQuery.rwq', Param);
  try
    if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
    begin
      ClNbr := FClCoData.FindField('CL_NBR').AsInteger;
      CoNbr := FClCoData.FindField('CO_NBR').AsInteger;
    end;
  finally
    FreeAndNil( FClCoData );
  end;
end;

function TWorkersChoiceXmlRPCServer.GetDate(
  const aStrDate: string): TDatetime; // the format should be yyyymmdd
var
  Y, M, D: integer;
begin
  Result := 0;
  if TryStrToInt( Copy(aStrDate, 1, 4), Y ) then
    if TryStrToInt( Copy(aStrDate, 5, 2), M ) then
      if TryStrToInt( Copy(aStrDate, 7, 2), D ) then
        TryEncodeDate(Y, M, D, Result);
end;

function TWorkersChoiceXmlRPCServer.GetEdNbr(
  const aEDCode: string): integer;
var
  FEdData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := 0;
  param := TRpcStruct.Create;
  Param.AddItem( 'EDCode', aEDCode );
  FEdData :=  FEvoWorkersChoiceProc.RunQuery('EdQuery.rwq', Param);
  try
    if (FEdData.RecordCount > 0) and Assigned(FEdData.FindField('CL_E_DS_NBR')) then
      Result := FEdData.FindField('CL_E_DS_NBR').AsInteger;
  finally
    FreeAndNil( FEdData );
  end;
end;

function TWorkersChoiceXmlRPCServer.GetEeNbrBySSN(const aLast5DigitsOfSSN: string; aCoNbr: integer): integer;
var
  FEeData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := 0;
  param := TRpcStruct.Create;
  Param.AddItem( 'Last5DigitsOfSSN', aLast5DigitsOfSSN );
  Param.AddItem( 'CoNbr', aCoNbr );
  FEeData :=  FEvoWorkersChoiceProc.RunQuery('EeQuery.rwq', Param);
  try
    if (FEeData.RecordCount > 0) and Assigned(FEeData.FindField('EE_NBR')) then
      Result := FEeData.FindField('EE_NBR').AsInteger;
  finally
    FreeAndNil( FEeData );
  end;
end;

function TWorkersChoiceXmlRPCServer.GetSchedEdNbr(EeNbr: integer;
  const aEDCode: string): integer;
var
  FSchedEdData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := 0;
  param := TRpcStruct.Create;
  Param.AddItem( 'EeNbr', EeNbr );
  Param.AddItem( 'EDCode', aEDCode );
  FSchedEdData :=  FEvoWorkersChoiceProc.RunQuery('SchedEdQuery.rwq', Param);
  try
    if (FSchedEdData.RecordCount > 0) and Assigned(FSchedEdData.FindField('EE_SCHEDULED_E_DS_NBR')) then
      Result := FSchedEdData.FindField('EE_SCHEDULED_E_DS_NBR').AsInteger;
  finally
    FreeAndNil( FSchedEdData );
  end;
end;

procedure TWorkersChoiceXmlRPCServer.ImportScheduledED(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcStruct, RowChange, flist, rpcResult, rpcEvoResult: IRpcStruct;
  ClNbr, CoNbr, EeNbr, SchedEdNbr, EDNbr: integer;
  Changes: IRpcArray;
  s: string;
  StartDate: TDatetime;
begin
  // ImportScheduledED(struct ScheduledEDInfo)
  // ScheduledEDInfo has the following fields:
  // 1. "CompanyCode" - string, Custom Company Number
  // 2. "FirstName" - string, Employee First Name
  // 3. "LastName" - string, Employee Last Name
  // 4. "Last5DigitsOfSSN" - string, the last 5 digits of employee SSN
  // 5. "EDCode" - string, custom E/D code number
  // 6. "Amount" - double, deducted amount
  // 7. "StartDate" - string, effective start date in format yyyymmdd
  // 8. "Goal" - double, Scheduled E/D Target Amount
  // 9. "ClientNbr" - integer, client internal number
  // 10. "CompanyNbr" - integer, company internal number
  // Return: struct where the members are ReturnCode of integer and ReturnString of string

  rpcResult := TRpcStruct.Create;
  try
    APICheckCondition(FEvoWorkersChoiceProc.Connected, APIException, 'EvoWorkersChoice is not connected to Evo!');
    APICheckCondition(AParams.Count = 1, WrongSignatureException);
    APICheckCondition(TRpcParameter(AParams[0]).DataType = dtStruct, WrongSignatureException);

    rpcStruct := TRpcParameter(AParams[0]).AsStruct;

    VerifyStructMember(rpcStruct, sCompanyCode, dtString);
    VerifyStructMember(rpcStruct, sFirstName, dtString);
    VerifyStructMember(rpcStruct, sLastName, dtString);
    VerifyStructMember(rpcStruct, sLast5DigitsOfSSN, dtString);
    VerifyStructMember(rpcStruct, sEDCode, dtString);
    VerifyStructMember(rpcStruct, sAmount, dtFloat);
    VerifyStructMember(rpcStruct, sStartDate, dtString);
    VerifyStructMember(rpcStruct, sGoal, dtFloat);

    if rpcStruct.KeyExists(sClientNbr) then
    begin
      ClNbr := rpcStruct.Keys[sClientNbr].AsInteger;
      CoNbr := rpcStruct.Keys[sCompanyNbr].AsInteger;
    end
    else begin
      s := rpcStruct.Keys[sCompanyCode].AsString;
      GetClNbrByCompany( s, ClNbr, CoNbr );
      APICheckCondition((ClNbr > 0) and (CoNbr > 0), APIException, 'Company #' + s + ' is not found');
    end;

    FEvoWorkersChoiceProc.OpenClient( ClNbr );

    StartDate := GetDate( rpcStruct.Keys[sStartDate].AsString );
    APICheckCondition(StartDate > 0, APIException, 'Start Date "' + rpcStruct.Keys[sStartDate].AsString + '" is incorrect. Should be correct date in format YYYYMMDD');

    s := rpcStruct.Keys[sLast5DigitsOfSSN].AsString;
    s := 'XXX-X' + Copy(s, 1, 1) + '-' + Copy(s, 2, 4);
    EeNbr := GetEeNbrBySSN( rpcStruct.Keys[sLast5DigitsOfSSN].AsString, CoNbr );
    APICheckCondition(EeNbr > 0, APIException, 'Employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + ' (SSN: ' + s + ')" is not found');

    EDNbr := GetEdNbr( rpcStruct.Keys[sEDCode].AsString);
    APICheckCondition(EDNbr > 0, APIException, 'Client E/D Code "' + rpcStruct.Keys[sEDCode].AsString + '" is not found');

    SchedEdNbr := GetSchedEdNbr( EeNbr, rpcStruct.Keys[sEDCode].AsString);
    if SchedEdNbr > 0 then
      s := 'U'  // change type "update"
    else
      s := 'I'; // change type "insert"

    Changes := TRpcArray.Create;
    RowChange := TRpcStruct.Create;
    flist := TRpcStruct.Create;
    rpcEvoResult := TRpcStruct.Create;

    flist.AddItem('EE_NBR', EeNbr);
    flist.AddItem('CL_E_DS_NBR', EDNbr);
    flist.AddItem('PLAN_TYPE', 'N');
    flist.AddItem('FREQUENCY', 'P');
    flist.AddItem('EXCLUDE_WEEK_1', 'N');
    flist.AddItem('EXCLUDE_WEEK_2', 'N');
    flist.AddItem('EXCLUDE_WEEK_3', 'N');
    flist.AddItem('EXCLUDE_WEEK_4', 'N');
    flist.AddItem('EXCLUDE_WEEK_5', 'N');
    flist.AddItemDateTime('EFFECTIVE_START_DATE', StartDate);
    flist.AddItem('WHICH_CHECKS', 'A');
    flist.AddItem('DEDUCT_WHOLE_CHECK', 'N');
    flist.AddItem('ALWAYS_PAY', 'N');
    flist.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
    flist.AddItem('CAP_STATE_TAX_CREDIT', 'N');
    flist.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
    flist.AddItem('DEDUCTIONS_TO_ZERO', 'N');
    flist.AddItem('USE_PENSION_LIMIT', 'N');
    flist.AddItem('AMOUNT', rpcStruct.Keys[sAmount].AsFloat);
    flist.AddItem('TARGET_AMOUNT', rpcStruct.Keys[sGoal].AsFloat);
    flist.AddItem('CALCULATION_TYPE', 'F');

    RowChange.AddItem('T', s);
    RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
    if s = 'U' then
      RowChange.AddItem('K', SchedEdNbr)
    else
      flist.AddItem('EE_SCHEDULED_E_DS_NBR', -1);

    RowChange.AddItem('F', flist);
    Changes.AddItem( RowChange );

    rpcEvoResult := FEvoWorkersChoiceProc.applyDataChangePacket( Changes );

    rpcResult.AddItem('returnCode', 1);
    if rpcEvoResult.KeyExists(IntToStr(-1)) then
      rpcResult.AddItem('returnString', 'Scheduled ED "' + rpcStruct.Keys[sEDCode].AsString + '" for the employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + '" has been added.')
    else
      rpcResult.AddItem('returnString', 'Scheduled ED "' + rpcStruct.Keys[sEDCode].AsString + '" for the employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + '" has been updated.');

  except
    on e: Exception do
    begin
      rpcResult.AddItem('returnCode', 0);
      rpcResult.AddItem('returnString', e.Message);
    end;
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TWorkersChoiceXmlRPCServer.IsEeEligible(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcStruct, rpcResult: IRpcStruct;
  ClNbr, CoNbr, EeNbr: integer;
  s: string;
  HireDate: TDatetime;
  FEeData: TkbmCustomMemTable;
  Param: IRpcStruct;
  EeAnnualIncome: double;
begin
  // IsEeEligible (struct EeInfo)
  // EeInfo has the following fields:
  // 1. "CompanyCode" - string, Custom Company Number
  // 2. "FirstName" - string, Employee First Name
  // 3. "LastName" - string, Employee Last Name
  // 4. "Last5DigitsOfSSN" - string, the last 5 digits of employee SSN
  // 5. "AnnualIncome" - double, Ee Gross for the current year
  // 6. "HireDate" - string, Current Hire Date in format yyyymmdd
  // 7. "ClientNbr" - integer, client internal number
  // 8. "CompanyNbr" - integer, company internal number
  // Return: struct where the members are ReturnCode of integer and ReturnString of string

  rpcResult := TRpcStruct.Create;
  try
    APICheckCondition(FEvoWorkersChoiceProc.Connected, APIException, 'EvoWorkersChoice is not connected to Evo!');
    APICheckCondition(AParams.Count = 1, WrongSignatureException);
    APICheckCondition(TRpcParameter(AParams[0]).DataType = dtStruct, WrongSignatureException);

    rpcStruct := TRpcParameter(AParams[0]).AsStruct;

    VerifyStructMember(rpcStruct, sCompanyCode, dtString);
    VerifyStructMember(rpcStruct, sFirstName, dtString);
    VerifyStructMember(rpcStruct, sLastName, dtString);
    VerifyStructMember(rpcStruct, sLast5DigitsOfSSN, dtString);
    VerifyStructMember(rpcStruct, sAnnualIncome, dtFloat);
    VerifyStructMember(rpcStruct, sHireDate, dtString);

    if rpcStruct.KeyExists(sClientNbr) then
    begin
      ClNbr := rpcStruct.Keys[sClientNbr].AsInteger;
      CoNbr := rpcStruct.Keys[sCompanyNbr].AsInteger;
    end
    else begin
      s := rpcStruct.Keys[sCompanyCode].AsString;
      GetClNbrByCompany( s, ClNbr, CoNbr );
      APICheckCondition((ClNbr > 0) and (CoNbr > 0), APIException, 'Company #' + s + ' is not found');
    end;

    FEvoWorkersChoiceProc.OpenClient( ClNbr );

    HireDate := GetDate( rpcStruct.Keys[sHireDate].AsString );
    APICheckCondition(HireDate > 0, APIException, 'Hire Date "' + rpcStruct.Keys[sHireDate].AsString + '" is incorrect. Should be correct date in format YYYYMMDD');

    s := rpcStruct.Keys[sLast5DigitsOfSSN].AsString;
    s := 'XXX-X' + Copy(s, 1, 1) + '-' + Copy(s, 2, 4);
    EeNbr := GetEeNbrBySSN( rpcStruct.Keys[sLast5DigitsOfSSN].AsString, CoNbr );
    APICheckCondition(EeNbr > 0, APIException, 'Employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + ' (SSN: ' + s + ')" is not found');

    param := TRpcStruct.Create;
    Param.AddItem( 'EeNbr', EeNbr );
    FEeData :=  FEvoWorkersChoiceProc.RunQuery('EeInfo.rwq', Param); // returns Current_Hire_Date and EE_Annual_Income
    try
      APICheckCondition(FEeData.RecordCount > 0, APIException, 'The EeInfo query result is empty.');
      APICheckCondition( Assigned(FEeData.FindField('Current_Hire_Date')), APIException, 'The EeInfo query has no Current_Hire_Date field');
      //APICheckCondition( Assigned(FEeData.FindField('EE_Annual_Income')), APIException, 'The EeInfo query has no EE_Annual_Income field');

      // calculate Ee Annual Income
      EeAnnualIncome := FEeData.FindField('Salary_Amount').AsFloat;
      if EeAnnualIncome < 0.01 then
      begin
        EeAnnualIncome := FEeData.FindField('Rate_Amount').AsFloat;
        APICheckCondition(EeAnnualIncome >= 0.01, APIException, 'Primary Rate should be set.');
        if FEeData.FindField('Standard_Hours').AsFloat >= 0.01 then
          EeAnnualIncome := EeAnnualIncome * FEeData.FindField('Standard_Hours').AsFloat
        else begin // supposed hours worked
          if FEeData.FindField('Pay_Frequency').AsString = 'W' then // weekly
            EeAnnualIncome := EeAnnualIncome * 40
          else if FEeData.FindField('Pay_Frequency').AsString = 'D' then //Daily
            EeAnnualIncome := EeAnnualIncome * 8
          else if FEeData.FindField('Pay_Frequency').AsString = 'B' then //Bi-Weekly
            EeAnnualIncome := EeAnnualIncome * 80
          else if FEeData.FindField('Pay_Frequency').AsString = 'S' then //Semi-Monthly
            EeAnnualIncome := EeAnnualIncome * 86.67
          else if FEeData.FindField('Pay_Frequency').AsString = 'M' then //Monthly
            EeAnnualIncome := EeAnnualIncome * 173.33
          else if FEeData.FindField('Pay_Frequency').AsString = 'Q' then //Quarterly
            EeAnnualIncome := EeAnnualIncome * 520
          else //Annualy
            EeAnnualIncome := EeAnnualIncome * 2080;
        end;
      end;

      if FEeData.FindField('Pay_Frequency').AsString = 'W' then // weekly
        EeAnnualIncome := EeAnnualIncome * 52
      else if FEeData.FindField('Pay_Frequency').AsString = 'D' then //Daily
        EeAnnualIncome := EeAnnualIncome * 365
      else if FEeData.FindField('Pay_Frequency').AsString = 'B' then //Bi-Weekly
        EeAnnualIncome := EeAnnualIncome * 26
      else if FEeData.FindField('Pay_Frequency').AsString = 'S' then //Semi-Monthly
        EeAnnualIncome := EeAnnualIncome * 24
      else if FEeData.FindField('Pay_Frequency').AsString = 'M' then //Monthly
        EeAnnualIncome := EeAnnualIncome * 12
      else if FEeData.FindField('Pay_Frequency').AsString = 'Q' then //Quarterly
        EeAnnualIncome := EeAnnualIncome * 4;

      if EeAnnualIncome >= rpcStruct.Keys[sAnnualIncome].AsFloat then
      begin
        if FormatDateTime('yyyymmdd', FEeData.FindField('Current_Hire_Date').AsDateTime) <=  rpcStruct.Keys[sHireDate].AsString then
        begin
          rpcResult.AddItem('returnCode', 1);
          rpcResult.AddItem('returnString', 'Employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + '" is eligible)');
        end
        else begin
          rpcResult.AddItem('returnCode', 0);
          rpcResult.AddItem('returnString', 'Employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + '" is not eligible (Hire Date = '+FEeData.FindField('Current_Hire_Date').AsString+')');
        end;
      end
      else begin
        rpcResult.AddItem('returnCode', 0);
        rpcResult.AddItem('returnString', 'Employee "' + rpcStruct.Keys[sFirstName].AsString + ' ' + rpcStruct.Keys[sLastName].AsString + '" is not eligible (Annual Income = '+FloatToStr(EeAnnualIncome)+')');
      end
    finally
      FreeAndNil( FEeData );
    end;

  except
    on e: Exception do
    begin
      rpcResult.AddItem('returnCode', 0);
      rpcResult.AddItem('returnString', e.Message);
    end;
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TWorkersChoiceXmlRPCServer.RegisterMethods;

   procedure RegisterMethod(const ASignature, AHelp: String; const AHandler: TevRPCMethod);
   var
     RpcMethodHandler: TRpcMethodHandler;
     s: String;
   begin
     s := ASignature;
     RpcMethodHandler := TRpcMethodHandler.Create;
     RpcMethodHandler.Name := GetNextStrValue(s, '(');
     RpcMethodHandler.Method := RPCHandler;
     RpcMethodHandler.Signature := ASignature;
     RpcMethodHandler.Help := AHelp;
     FRpcServer.RegisterMethodHandler(RpcMethodHandler);

     FMethods.AddObject(RpcMethodHandler.Name, @AHandler);
   end;

begin
  RegisterMethod('isEeEligible(struct EeInformation)',
                 'Return: struct',
                 IsEeEligible);

  RegisterMethod('importScheduledED(struct EDCodeInformation)',
                 'Return: struct',
                 ImportScheduledED);
end;

procedure TWorkersChoiceXmlRPCServer.RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);
var
  i: Integer;
  Proc: TMethod;
begin
  try
    i := FMethods.IndexOf(MethodName);
    CheckCondition(i <> -1, 'Unregistered method "' + MethodName + '"');

    Proc.Data := Self;
    Proc.Code := FMethods.Objects[i];
    TevRPCMethod(Proc)(List, Return, LowerCase(MethodName));
  except
    on E: APIException do
    begin
      Return.SetError(800, E.Message);
    end;

    on E: UnsupportedTableNameException do
    begin
      Return.SetError(800, 'Unsupported Table');
    end;

    on E: WrongParametersGetRecordNbrException do
    begin
      Return.SetError(800, 'Wrong struct parameter for GetRecordNbr method');
    end;

    on E: WrongSignatureException do
    begin
      Return.SetError(800, 'Wrong method signature');
    end;

    on E: WrongSessionException do
    begin
      Return.SetError(810, 'Wrong session');
    end;

    on E: WrongLicenseException do
    begin
      Return.SetError(800, 'Wrong license key');
    end;

    on E: WrongAPIKeyException do
    begin
      Return.SetError(800, 'Wrong API Key');
    end;

    on E: NotAllowedEEException do
    begin
      Return.SetError(810, 'Operation is not allowed in EE mode');
    end;

    on E: TableNameEmptyException do
    begin
      Return.SetError(800, 'No table name');
    end;

    on E: QueueTaskNotFoundException do
    begin
      Return.SetError(800, 'Task not found in queue');
    end;

    on E: QueueTaskNotFinishedException do
    begin
      Return.SetError(800, 'Task not finished yet');
    end;

    on E: NotSSUserException do
    begin
      Return.SetError(800, 'This user is not registered as a SelfServe user');
    end;

    on E: WrongReportLevelException do
    begin
      Return.SetError(810, 'Wrong report level');
    end;

    on E: UnsupportedParamTypeException do
    begin
      Return.SetError(811, 'dtArray and dtNone are unsupported parameter types');
    end;

    on E: UnknownParamTypeException do
    begin
      Return.SetError(811, 'Unsupported parameter type');
    end;

    on E: ResultWrongStructureException do
    begin
      Return.SetError(810, 'Result has wrong structure');
    end;

    on E: WrongReturnTypeException do
    begin
      Return.SetError(510, 'Wrong return type');
    end;

    on E: WrongPackageNumberException do
    begin
      Return.SetError(810, 'Package number should be 4 (REMOTABLE_CALC_PACKAGE)');
    end;

    on E: UnsupportedFunctionException do
    begin
      Return.SetError(810, 'Unsupported function');
    end;

    on E: ResultIsEmptyException do
    begin
      Return.SetError(810, 'Result is empty');
    end;

    on E: UnsupportedOutputParamTypeException do
    begin
      Return.SetError(810, 'Unsupported output parameter type');
    end;

    on E: EmptyDatasetException do
    begin
      Return.SetError(810, 'Dataset is empty');
    end;

    on E: CannotConvertNbrToIdException do
    begin
      Return.SetError(810, 'Cannot find TaskId for given TaskNbr');
    end;

    on E: CannotConvertIdToNbrException do
    begin
      Return.SetError(810, 'Cannot find TaskNbr for given TaskId');
    end;

    on E: LegacyCallsNotAllowed do
    begin
      Return.SetError(810, 'You have no rights to do legacy calls');
    end;

    on E: SecurityCallsNotAllowed do
    begin
      Return.SetError(810, 'You have no rights to do security calls');
    end;

    on E: UnknownCoStorageTag do
    begin
      Return.SetError(800, 'Unknown Tag');
    end;

    on E: CannotGetDataDueTimeout do
    begin
      Return.SetError(810, 'Program was not able to read data because table is locked');
    end;

    on E: UnknownEEChangeRequestType do
    begin
      Return.SetError(800, 'Unknown type of employee change request');
    end;

    on E: ESSNotEnabledException do
    begin
      Return.SetError(800, 'SelfServe is not enabled for this employee');
    end;

    on E: Exception do
    begin
      Return.SetError(500, E.Message);
    end;
  end;
end;

procedure TWorkersChoiceXmlRPCServer.SetActive(AValue: boolean);
begin
  if FRpcServer.Active <> AValue then
  begin
    if AValue then
    begin
      FRpcServer.ListenPort := 9944;
      FRpcServer.EnableIntrospect := True;

      FRpcServer.SSLEnable := True;
      FRpcServer.SSLCertFile := Redirection.GetFilename(sCertificateFileAlias);
      FRpcServer.SSLKeyFile := Redirection.GetFilename(sKeyCertificateFileAlias);
      FRpcServer.SSLRootCertFile := Redirection.GetFilename(sRootCertificateFileAlias);

      FRpcServer.Active := True;
    end
    else
      FRpcServer.Active := False;
  end;
end;

procedure TWorkersChoiceXmlRPCServer.VerifyStructMember(aStruct: IRpcStruct;
  const aName: string; aDataType: TDataType; Mandatory: boolean = True);
begin
  if Mandatory then
    APICheckCondition(aStruct.KeyExists(aName), APIException, aName + ' is required');

  if aStruct.KeyExists(aName) then
    APICheckCondition(aStruct.Keys[aName].DataType = aDataType, APIException, aName + ' has a wrong type');
end;

end.
