program EvoWorkersChoice;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TFrame},
  sevenzip in '..\..\common\gdycommon\sevenzip\sevenzip.pas',
  MainForm in 'MainForm.pas' {MainFm},
  WorkersChoiceXmlRpcServerMod in 'WorkersChoiceXmlRpcServerMod.pas',
  EvoWorkersChoiceCommon in 'EvoWorkersChoiceCommon.pas',
  EvoWorkersChoiceProc in 'EvoWorkersChoiceProc.pas',
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  XmlRpcServer in 'XmlRpcServer.pas';

{$R *.res}

begin
  LicenseKey := '2FD79336BB844142AB861AB1AAECF2DA'; // EvoWorkersChoice License

  Application.Initialize;
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
