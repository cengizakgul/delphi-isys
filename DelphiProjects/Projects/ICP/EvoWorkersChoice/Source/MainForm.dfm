inherited MainFm: TMainFm
  Left = 390
  Top = 183
  Caption = 'MainFm'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    inherited TabSheet3: TTabSheet
      object btnConnect: TBitBtn
        Left = 268
        Top = 144
        Width = 121
        Height = 25
        Action = ConnectToEvo
        Caption = 'Connect to Evo'
        TabOrder = 1
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = '<Debugging>'
      TabVisible = False
      object pnlMain: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 41
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object lblNewClient: TLabel
          Left = 208
          Top = 14
          Width = 3
          Height = 13
        end
        object btnCreateNewClient: TBitBtn
          Left = 8
          Top = 8
          Width = 129
          Height = 25
          Action = createNewClientDB
          Caption = 'Create a New Client DB'
          TabOrder = 0
        end
      end
      object pnlGetRecordNbr: TPanel
        Left = 0
        Top = 48
        Width = 784
        Height = 178
        Anchors = [akLeft, akTop, akRight]
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 14
          Width = 58
          Height = 13
          Caption = 'Table Name'
        end
        object Label2: TLabel
          Left = 12
          Top = 54
          Width = 31
          Height = 13
          Caption = 'Field 1'
        end
        object Label3: TLabel
          Left = 12
          Top = 78
          Width = 31
          Height = 13
          Caption = 'Field 2'
        end
        object Label4: TLabel
          Left = 12
          Top = 102
          Width = 31
          Height = 13
          Caption = 'Field 3'
        end
        object lblGetRecordNbrResult: TLabel
          Left = 168
          Top = 152
          Width = 3
          Height = 13
        end
        object BitBtn2: TBitBtn
          Left = 8
          Top = 144
          Width = 129
          Height = 25
          Action = GetRecordNbr
          Caption = 'GetRecordNbr'
          TabOrder = 0
        end
        object edTableName: TEdit
          Left = 78
          Top = 10
          Width = 121
          Height = 21
          TabOrder = 1
        end
        object edF1Name: TEdit
          Left = 78
          Top = 50
          Width = 121
          Height = 21
          TabOrder = 2
        end
        object edF1Value: TEdit
          Left = 206
          Top = 50
          Width = 121
          Height = 21
          TabOrder = 3
        end
        object edF2Name: TEdit
          Left = 78
          Top = 74
          Width = 121
          Height = 21
          TabOrder = 4
        end
        object edF2Value: TEdit
          Left = 206
          Top = 74
          Width = 121
          Height = 21
          TabOrder = 5
        end
        object edF3Name: TEdit
          Left = 78
          Top = 98
          Width = 121
          Height = 21
          TabOrder = 6
        end
        object edF3Value: TEdit
          Left = 206
          Top = 98
          Width = 121
          Height = 21
          TabOrder = 7
        end
      end
    end
  end
  object Actions: TActionList
    Left = 476
    Top = 48
    object ConnectToEvo: TAction
      Caption = 'Connect to Evo'
      OnExecute = ConnectToEvoExecute
      OnUpdate = ConnectToEvoUpdate
    end
    object createNewClientDB: TAction
      Caption = 'Create a New Client DB'
    end
    object GetRecordNbr: TAction
      Caption = 'GetRecordNbr'
    end
  end
end
