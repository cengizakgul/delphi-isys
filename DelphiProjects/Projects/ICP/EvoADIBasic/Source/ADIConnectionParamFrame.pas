unit ADIConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, adidecl, PasswordEdit;

type
  TADIConnectionParamFrm = class(TFrame)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    PasswordEdit: TPasswordEdit;
    UserNameEdit: TEdit;
    cbSavePassword: TCheckBox;
    ClientUrlEdit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
  private
    function GetParam: TADIConnectionParam;
    procedure SetParam(const Value: TADIConnectionParam);
  public
    property Param: TADIConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
  end;

implementation

{$R *.dfm}

{ TADIClockConnectionParamFrm }

function TADIConnectionParamFrm.GetParam: TADIConnectionParam;
begin
  Result.Username := UserNameEdit.Text;
  Result.Password := PasswordEdit.Password;
  Result.SavePassword := cbSavePassword.Checked;
  Result.ClientURL := ClientUrlEdit.Text;
end;

function TADIConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(UserNameEdit.Text) <> '') and
            (trim(PasswordEdit.Password) <> '') and
            (trim(ClientUrlEdit.Text) <> '');
end;

procedure TADIConnectionParamFrm.SetParam(const Value: TADIConnectionParam);
begin
  UserNameEdit.Text := Value.Username;
  PasswordEdit.Password := Value.Password;
  cbSavePassword.Checked := Value.SavePassword;
  ClientUrlEdit.Text := Value.ClientURL;
end;

end.
