inherited MainFm: TMainFm
  Left = 275
  Top = 198
  Width = 995
  Height = 672
  Caption = 'MainFm'
  Constraints.MinHeight = 520
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 979
    Height = 615
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Top = 6
        Width = 373
        Height = 161
        inherited GroupBox1: TGroupBox
          Width = 373
          Height = 161
        end
      end
      inline ADIFrame: TADIConnectionParamFrm
        Left = 390
        Top = 6
        Width = 387
        Height = 161
        TabOrder = 1
        inherited GroupBox1: TGroupBox
          Width = 387
          Height = 161
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited Splitter1: TSplitter
        Top = 314
        Width = 971
      end
      inherited pnlTop: TPanel
        Width = 971
        inherited BitBtn4: TBitBtn
          Width = 177
          Caption = 'Get Evolution and ADI data'
        end
      end
      inherited EvolutionSinglePayrollFrame: TEvolutionSinglePayrollFrm
        Width = 971
        Height = 273
        inherited Splitter1: TSplitter
          Height = 273
        end
        inherited EvoCompany: TEvolutionCompanyFrm
          Height = 273
          inherited Splitter1: TSplitter
            Height = 273
          end
          inherited frmCL: TEvolutionDsFrm
            Height = 273
            inherited dgGrid: TReDBGrid
              Height = 248
            end
          end
          inherited frmCO: TEvolutionDsFrm
            Height = 273
            inherited dgGrid: TReDBGrid
              Height = 248
            end
          end
        end
        inherited frmPR: TEvolutionDsFrm
          Width = 447
          Height = 273
          inherited Panel1: TPanel
            Width = 447
          end
          inherited dgGrid: TReDBGrid
            Width = 447
            Height = 248
          end
        end
      end
      inherited pnlBottom: TPanel
        Top = 317
        Width = 971
        Height = 270
        inline OptionsFrame: TADIOptionsFrm
          Left = 0
          Top = 0
          Width = 971
          Height = 229
          Align = alClient
          Constraints.MinWidth = 750
          TabOrder = 0
          inherited Splitter1: TSplitter
            Left = 784
            Height = 229
          end
          inherited Bevel5: TBevel
            Left = 963
            Height = 229
          end
          inherited Bevel6: TBevel
            Height = 229
          end
          inherited GroupBox2: TGroupBox
            Width = 776
            Height = 229
            inherited FieldsToExportFrame: TFieldsToExportFrm
              Width = 772
              Height = 162
              inherited clbFields: TCheckListBox
                Width = 772
                Height = 132
              end
              inherited Panel1: TPanel
                Width = 772
              end
            end
            inherited Panel1: TPanel
              Width = 772
            end
          end
          inherited GroupBox1: TGroupBox
            Left = 799
            Width = 164
            Height = 229
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 229
          Width = 971
          Height = 41
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn1: TBitBtn
            Left = 16
            Top = 8
            Width = 289
            Height = 25
            Action = RunEmployeeExport
            Caption = 'Update ADI Employee Records for Selected Company'
            TabOrder = 0
          end
          object BitBtn2: TBitBtn
            Left = 352
            Top = 8
            Width = 177
            Height = 25
            Action = RunAction
            Caption = 'Import timeclock data'
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 615
    Width = 979
  end
  inherited ActionList1: TActionList
    Left = 332
    Top = 512
    inherited RunAction: TAction
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited RunEmployeeExport: TAction
      Caption = 'Update ADI Employee Records for Selected Company'
      OnExecute = RunEmployeeExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
  end
end
