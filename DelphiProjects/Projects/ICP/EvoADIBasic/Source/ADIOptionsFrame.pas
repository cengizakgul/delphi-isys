unit ADIOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, adidecl, StdCtrls, ExtCtrls, bndRetrieveAllPayGroups,
  FieldsToExportFrame, OptionsBaseFrame;

type
  TADIOptionsFrm = class(TFrame)
    GroupBox2: TGroupBox;
    Splitter1: TSplitter;
    GroupBox1: TGroupBox;
    Bevel5: TBevel;
    cbSummarize: TCheckBox;
    Bevel6: TBevel;
    cbTransferTempHierachy: TCheckBox;
    cbTransferTempRate: TCheckBox;
    FieldsToExportFrame: TFieldsToExportFrm;
    Panel1: TPanel;
    Label1: TLabel;
    cbPayGroup: TComboBox;
    cbAllowDeletingOfEE: TCheckBox;
    procedure lbPayGroupsClick(Sender: TObject);
    procedure cbSummarizeClick(Sender: TObject);
    procedure cbAllowDeletingOfEEClick(Sender: TObject);
    procedure cbTransferTempHierachyClick(Sender: TObject);
    procedure cbTransferTempRateClick(Sender: TObject);
  private
    FPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
    FOptions: TADIOptions;
    procedure SetOptions(const Value: TADIOptions);
    procedure SetPayGroups(const Value: bndRetrieveAllPayGroups.IXMLPaygroupsType);
    procedure DataUpdated;
    procedure FieldsToExportChangedByUser(Sender: TObject);
  public
    constructor Create( Owner: TComponent ); override;
    property PayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType write SetPayGroups;
    property Options: TADIOptions read FOptions write SetOptions;
    function IsValid: boolean;
  end;

implementation

uses XMLIntf;

{$R *.dfm}

function TADIOptionsFrm.IsValid: boolean;
begin
  Result := cbPayGroup.ItemIndex <> -1;
end;

procedure TADIOptionsFrm.SetOptions(const Value: TADIOptions);
begin
  FOptions := Value;
  DataUpdated;
end;

procedure TADIOptionsFrm.SetPayGroups( const Value: bndRetrieveAllPayGroups.IXMLPaygroupsType);
var
  i: integer;
begin
  FPayGroups := Value;
  cbPayGroup.Items.Clear;
  for i := 0 to FPayGroups.Count-1 do
    cbPayGroup.Items.Add( trim(FPayGroups[i].Name) );
  DataUpdated;
end;

procedure TADIOptionsFrm.DataUpdated;
var
  i: integer;
begin
  cbPayGroup.ItemIndex := -1;
  if FPayGroups <> nil then
    for i := 0 to FPayGroups.Count-1 do
      if trim(FPayGroups[i].Paygroupid) = trim(FOptions.DefaultPayGroupCode) then
      begin
        cbPayGroup.ItemIndex := i;
      end;
  if cbPayGroup.ItemIndex = -1 then
    if cbPayGroup.Items.Count > 0 then
    begin
      cbPayGroup.ItemIndex := 0;
      lbPayGroupsClick(nil);
    end;
  FieldsToExportFrame.FieldsToExport := FOptions.FieldsToExport;
  cbSummarize.Checked := FOptions.Summarize;
  cbAllowDeletingOfEE.Checked := FOptions.AllowDeletionOfEE;
  cbTransferTempHierachy.Checked := FOptions.TransferTempHierarchy;
  cbTransferTempRate.Checked := FOptions.TransferTempRate;
end;

procedure TADIOptionsFrm.lbPayGroupsClick(Sender: TObject);
begin
  if cbPayGroup.ItemIndex <> -1 then
    FOptions.DefaultPayGroupCode := trim(FPayGroups[cbPayGroup.ItemIndex].Paygroupid);
end;

procedure TADIOptionsFrm.cbSummarizeClick(Sender: TObject);
begin
  FOptions.Summarize := cbSummarize.Checked;
end;

procedure TADIOptionsFrm.cbAllowDeletingOfEEClick(Sender: TObject);
begin
  FOptions.AllowDeletionOfEE := cbAllowDeletingOfEE.Checked;
end;

procedure TADIOptionsFrm.cbTransferTempHierachyClick(Sender: TObject);
begin
  FOptions.TransferTempHierarchy := cbTransferTempHierachy.Checked;
end;

procedure TADIOptionsFrm.cbTransferTempRateClick(Sender: TObject);
begin
  FOptions.TransferTempRate := cbTransferTempRate.Checked;
end;

constructor TADIOptionsFrm.Create(Owner: TComponent);
begin
  inherited;
  FieldsToExportFrame.OnChangeByUser := FieldsToExportChangedByUser;
end;

procedure TADIOptionsFrm.FieldsToExportChangedByUser(Sender: TObject);
begin
  FOptions.FieldsToExport := FieldsToExportFrame.FieldsToExport;
end;

end.
