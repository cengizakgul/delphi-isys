object ADIConnectionParamFrm: TADIConnectionParamFrm
  Left = 0
  Top = 0
  Width = 382
  Height = 169
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 382
    Height = 169
    Align = alClient
    Caption = 'ADI'
    TabOrder = 0
    object Label4: TLabel
      Left = 14
      Top = 23
      Width = 48
      Height = 13
      Caption = 'Username'
    end
    object Label7: TLabel
      Left = 14
      Top = 48
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object Label1: TLabel
      Left = 14
      Top = 74
      Width = 51
      Height = 13
      Caption = 'Client URL'
    end
    object Label2: TLabel
      Left = 82
      Top = 98
      Width = 171
      Height = 13
      Caption = '(like https://clientname.aditime.com)'
    end
    object Label3: TLabel
      Left = 15
      Top = 116
      Width = 362
      Height = 39
      Caption = 
        'User must be a member of a role that has full API access (open A' +
        'DI web UI, go to Configurations - Administration - Security Role' +
        ', click on a role, go to Configuration - Administration tab, all' +
        'ow full API Access)'
      WordWrap = True
    end
    object PasswordEdit: TPasswordEdit
      Left = 81
      Top = 47
      Width = 192
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object UserNameEdit: TEdit
      Left = 81
      Top = 22
      Width = 192
      Height = 21
      TabOrder = 0
    end
    object cbSavePassword: TCheckBox
      Left = 281
      Top = 49
      Width = 97
      Height = 17
      Caption = 'Save password'
      TabOrder = 2
    end
    object ClientUrlEdit: TEdit
      Left = 81
      Top = 72
      Width = 192
      Height = 21
      TabOrder = 3
    end
  end
end
