object SVSelectionFm: TSVSelectionFm
  Left = 281
  Top = 178
  Width = 674
  Height = 589
  Caption = 'Select ADI Supervisors'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 510
    Width = 658
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      658
      41)
    object bbCancel: TBitBtn
      Left = 583
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 1
      Kind = bkCancel
    end
    object bbOk: TBitBtn
      Left = 495
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
  end
  inline SVSelectionFrame: TSVSelectionFrm
    Left = 0
    Top = 0
    Width = 658
    Height = 510
    Align = alClient
    TabOrder = 1
    inherited dgEE: TReDBGrid
      Width = 658
      Height = 510
    end
  end
end
