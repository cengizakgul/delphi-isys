unit adidecl;

interface

uses
  sysutils, xmlintf,
  bndRetrieveAllEmployees, bndRequestPayrollData, bndRetrieveAllPayTypes,
  bndUpdateEmployee, bndRetrieveSettings, bndRetrieveAllEmployeescodes,
  bndRetrieveAllPayGroups, bndRetrieveAllUsers, timeclockimport,
  gdyCommonLogger, issettings, common;

type
  TADIConnectionParam = record
    UserName: string;
    Password: string;
    SavePassword: boolean;
    ClientURL: string;
  end;

  TFieldExportInfo = record
    UserFriendlyName: string;
    ConfigKey: string;
  end;

  TFieldToExport = (
    fteLastName, fteFirstName, fteMiddleInitial, fteShortName, fteInitials, fteSSN, fteRates, fteCompany, fteLocationADI,
    fteDivisionADI, fteDepartmentADI, ftePositionADI, fteSupervisor, fteBadgeID_ClockID, fteAddressline1,
    fteAddressline2, fteCity, fteZipCode, fteState, ftePhone, fteEmailAddress, fteHireDate, fteBirthDate,
    fteReviewDate, fteSalaried );

  TFieldsToExport = set of TFieldToExport;

  TADIOptions = record
    FieldsToExport: TFieldsToExport;
    DefaultPayGroupCode: string;
    Summarize: boolean;
    AllowDeletionOfEE: boolean;
    TransferTempHierarchy: boolean;
    TransferTempRate: boolean;
  end;

  EADIError = class(Exception)
  public
    Code: string;
  public
    constructor Create(acode, descr: Variant; details: string);
  end;

  EADIEmptyResponseError = class(Exception);

  TADILevelRec = record
    Name: string;
    Data: IXMLDocument;
    DisabledReason: string;
  end;

  TADILevel = (adiCompany = 1, adiLocation = 2, adiDivision = 3, adiDepartment = 4, adiPosition = 5);

  IADIConnection = interface
['{CF449340-445C-471B-91E1-9EFEB7ABC654}']
    function RetrieveAllUsers: bndRetrieveAllUsers.IXMLUsersType;
    function GetGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
    function RetrieveAllPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
    function RetrieveAllCodes(codetype: integer): IXMLDocument;
    function RetrieveAllLevels(level: TADILevel): TADILevelRec;
    function RetrieveAllEmployees: bndRetrieveAllEmployees.IXMLEmployeesType;
    function RetrieveAllPayTypes: IXMLPaytypesType;
    function RetrieveSettings: IXMLSettingsType;

    function RequestPayrollData(period: TPayPeriod): bndRequestPayrollData.IXMLResponseType;

    procedure UpdateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
    procedure DeleteEmployee(code: widestring);
    procedure CreateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
  end;

const
  FieldExportInfos: array [TFieldToExport] of TFieldExportInfo =
  (
    ( UserFriendlyName: 'Last name'; ConfigKey: 'Last name'),
    ( UserFriendlyName: 'First name'; ConfigKey: 'First name'),
    ( UserFriendlyName: 'Middle initial'; ConfigKey: 'Middle initial'),
    ( UserFriendlyName: 'Short name'; ConfigKey: 'Short name'),
    ( UserFriendlyName: 'Initials'; ConfigKey: 'Initials'),
    ( UserFriendlyName: 'SSN'; ConfigKey: 'SSN'),
    ( UserFriendlyName: 'Rates'; ConfigKey: 'Rates'),
    ( UserFriendlyName: 'Company'; ConfigKey: 'Company'),
    ( UserFriendlyName: 'Location (ADI)'; ConfigKey: 'Location (ADI)'),
    ( UserFriendlyName: 'Division (ADI)'; ConfigKey: 'Division (ADI)'),
    ( UserFriendlyName: 'Department (ADI)'; ConfigKey: 'Department (ADI)'),
    ( UserFriendlyName: 'Position (ADI)'; ConfigKey: 'Position (ADI)'),
    ( UserFriendlyName: 'Supervisor'; ConfigKey: 'Supervisor'),
    ( UserFriendlyName: 'Badge ID/Clock ID #'; ConfigKey: 'Badge ID - Clock ID'),
    ( UserFriendlyName: 'Address line 1'; ConfigKey: 'Address line 1'),
    ( UserFriendlyName: 'Address line 2'; ConfigKey: 'Address line 2'),
    ( UserFriendlyName: 'City'; ConfigKey: 'City'),
    ( UserFriendlyName: 'Zip code'; ConfigKey: 'Zip code'),
    ( UserFriendlyName: 'State'; ConfigKey: 'State'),
    ( UserFriendlyName: 'Phone'; ConfigKey: 'Phone'),
    ( UserFriendlyName: 'E-mail address'; ConfigKey: 'E-mail address'),
    ( UserFriendlyName: 'Hire date'; ConfigKey: 'Hire date'),
    ( UserFriendlyName: 'Birth date'; ConfigKey: 'Birth date'),
    ( UserFriendlyName: 'Review date'; ConfigKey: 'Review date'),
    ( UserFriendlyName: 'Salaried'; ConfigKey: 'Salaried')
  );

procedure LogADIOptions(Logger: ICommonLogger; options: TADIOptions);
function LoadFieldsToExport(conf: IisSettings; root: string): TFieldsToExport;
procedure SaveFieldsToExport(const fieldsToExport: TFieldsToExport; conf: IisSettings; root: string);

implementation

uses
  variants, gdycommon;
{ EADIError }

constructor EADIError.Create(acode, descr: Variant; details: string);
begin
  Code := trim(VarToStr(acode));
  CreateFmt('ADI returned an error (%s): %s%s', [Code, VarToStr(descr), details]);
end;

procedure LogADIOptions(Logger: ICommonLogger; options: TADIOptions);
var
  i: TFieldToExport;
begin
  logger.LogContextItem( 'Default pay group code', options.DefaultPayGroupCode );
  logger.LogContextItem( 'Summarize', BoolToStr(options.Summarize, true) );
  logger.LogContextItem( 'Allow deletion of ADI employee records', BoolToStr(options.AllowDeletionOfEE, true) );
  for i := low(TFieldToExport) to high(TFieldToExport) do
    logger.LogContextItem( 'Export '+FieldExportInfos[i].UserFriendlyName, BoolToStr(i in options.FieldsToExport, true) );
end;

function LoadFieldsToExport(conf: IisSettings; root: string): TFieldsToExport;
var
  i: TFieldToExport;
begin
  Result := [];
  if conf.GetChildNodes(WithoutTrailingSlash(root)).IndexOf('FieldsToExport') <> -1 then
  begin
    root := root + IIF(root='','','\') + 'FieldsToExport\';
    for i := low(TFieldToExport) to high(TFieldToExport) do
      if conf.AsBoolean[root+FieldExportInfos[i].ConfigKey] then
        Include(Result, i);
  end
  else
  begin
    for i := low(TFieldToExport) to high(TFieldToExport) do
      Include(Result, i);

    root := root + IIF(root='','','\');
    if not conf.AsBoolean[root+'ExportSSN'] then
      Exclude(Result, fteSSN);
    if not conf.AsBoolean[root+'ExportRates'] then
      Exclude(Result, fteRates);
  end
end;

procedure SaveFieldsToExport(const fieldsToExport: TFieldsToExport; conf: IisSettings; root: string);
var
  i: TFieldToExport;
begin
  root := root + IIF(root='','','\') + 'FieldsToExport\';

  for i := low(TFieldToExport) to high(TFieldToExport) do
    conf.AsBoolean[root+FieldExportInfos[i].ConfigKey] := i in fieldsToExport;
end;


end.
