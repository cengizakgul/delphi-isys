unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoTCImportMainForm, ActnList, ComCtrls, gdyCommonLoggerView,
  Buttons, StdCtrls, ExtCtrls, isSettings, EvoAPIConnectionParamFrame,
  EvolutionCompanyFrame, evoapiconnection, timeclockimport, evoapiconnectionutils,
  EvolutionSinglePayrollFrame, ADIConnectionParamFrame,
  ADIOptionsFrame, common, gdySendMailLoggerView, OptionsBaseFrame;

type
  TMainFm = class(TEvoTCImportMainFm)
    ADIFrame: TADIConnectionParamFrm;
    OptionsFrame: TADIOptionsFrm;                          
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure RunEmployeeExportExecute(Sender: TObject);
    procedure RunEmployeeExportUpdate(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunActionExecute(Sender: TObject);
  private
    procedure ConnectToADIJob;
  protected
  	function CanConnectToTCApp: boolean; override;
  	function UpdateEmployees: TUpdateEmployeesStat;
    function GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

uses
  kbmMemTable, gdyRedir, XmlRpcTypes,
  gdycommon, aditime, adidecl, adiconnection, SVSelectionForm, gdyclasses, gdyCrypt,
  ConfirmationForm, waitform, evodata, userActionHelpers; //

{$R *.dfm}

const
  cKey='Form.Button';

function LoadSettings(conf: IisSettings; root: string): TADIConnectionParam;
begin
  Result.UserName := conf.AsString[root+'UserName'];
  if conf.GetValueNames('').IndexOf(root+'Password') <> -1 then //work around bug here - no slash
    Result.Password := conf.AsString[root+'Password']
  else
    Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  Result.ClientURL := conf.AsString[root+'ClientURL'];
end;

procedure SaveSettings( const param: TADIConnectionParam; conf: IisSettings; root: string);
begin
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
  conf.AsString[root+'ClientURL'] := param.ClientURL;
end;

function LoadADIOptions(conf: IisSettings; root: string): TADIOptions;
begin
  root := root + '\';
  Result.FieldsToExport := LoadFieldsToExport(conf, root);
  Result.DefaultPayGroupCode := conf.AsString[root+'DefaultPayGroupCode'];
  Result.Summarize := conf.AsBoolean[root+'Summarize'];
  Result.AllowDeletionOfEE := conf.AsBoolean[root+'AllowDeletionOfEE'];
  Result.TransferTempHierarchy := conf.AsBoolean[root+'TransferTempHierarchy'];
  Result.TransferTempRate := conf.AsBoolean[root+'TransferTempRate'];
end;

procedure SaveADIOptions( const options: TADIOptions; conf: IisSettings; root: string);
begin
  root := root + '\';
  SaveFieldsToExport(options.FieldsToExport, conf, root);
  conf.AsString[root+'DefaultPayGroupCode'] := options.DefaultPayGroupCode;
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsBoolean[root+'AllowDeletionOfEE'] := options.AllowDeletionOfEE;
  conf.AsBoolean[root+'TransferTempHierarchy'] := options.TransferTempHierarchy;
  conf.AsBoolean[root+'TransferTempRate'] := options.TransferTempRate;
end;

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
	TCAppName := 'ADI';
  try
    ADIFrame.Param := LoadSettings( FSettings, 'ADIConnection');
  except
    Logger.StopException;
  end;
  try
    OptionsFrame.Options := LoadADIOptions( FSettings, 'ADIOptions');
  except
    Logger.StopException;
  end;
end;

destructor TMainFm.Destroy;
begin
  try
    SaveSettings( ADIFrame.Param, FSettings, 'ADIConnection');
  except
    Logger.StopException;
  end;
  try
    SaveADIOptions( OptionsFrame.Options, FSettings, 'ADIOptions');
  except
    Logger.StopException;
  end;
  inherited;
end;

function TMainFm.CanConnectToTCApp: boolean;
begin
	Result := ADIFrame.IsValid;
end;

function TMainFm.UpdateEmployees: TUpdateEmployeesStat;
var
  conn: IEvoAPIConnection;
  company: TEvoCompanyDef;
  EEData: TEvoEEData;
  analysisResult: TAnalyzeResult;
  sv: TkbmCustomMemTable;
begin
  company := EvolutionSinglePayrollFrame.EvoCompany.GetClCo;
  conn := CreateEvoAPIConnection(EvoFrame.Param, logger);
  EEData := TEvoEEData.Create(conn, Logger, company);
  try
    with TADIEmployeeUpdater.Create(ADIFrame.Param, OptionsFrame.Options, EEData, Logger) do
    try
      try
        CheckHierarchyMatch( GetEvoDBDTInfo(Logger, conn, EEData.Company) );
      except
        Logger.StopException;
      end;
      analysisResult := Analyze;
      if Length(analysisResult.Actions) > 0 then
        Logger.LogEvent( 'Planned actions. See details.', EEUpdateActionRecsToString(analysisResult.Actions) );
      if not ConfirmActions(Self, analysisResult.Actions) then
        raise Exception.Create('User cancelled operation');

      if analysisResult.ADIHasSupervisors then
      begin
        sv := CreateSVDataSet;
        try
          if not SelectSV(Self, analysisResult.Actions, EEData, sv) then
            raise Exception.Create('User cancelled operation');
        finally
          FreeAndNil(sv);
        end;
      end;
      Result := Apply(analysisResult.Actions, Self as IProgressIndicator);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      Free;
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result := aditime.GetTimeClockData( Logger, ADIFrame.Param, OptionsFrame.Options, company, period );
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  inherited;
  ExecuteWithWaitForm(Self, ConnectToADIJob, 'Getting Pay Group list from ADI');
end;

procedure TMainFm.ConnectToADIJob;
begin
  OptionsFrame.PayGroups := CreateADIConnection(ADIFrame.Param, logger).RetrieveAllPayGroups;
end;

procedure TMainFm.RunEmployeeExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      userActionHelpers.RunEmployeeExport(Logger, UpdateEmployees, EvolutionSinglePayrollFrame.EvoCompany.GetClCo, TCAppName);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.RunEmployeeExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and CanConnectToTCApp and
      EvolutionSinglePayrollFrame.CanGetPayrollDef and OptionsFrame.IsValid;
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := EvoFrame.IsValid and CanConnectToTCApp and
                  EvolutionSinglePayrollFrame.EvoCompany.CanGetClCo;
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
var
  company: TEvoCompanyDef;
  batch: TEvoPayrollBatchDef;
  conn: IEvoAPIConnection;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      company := EvolutionSinglePayrollFrame.GetPayrollDef.Company;
      conn := CreateEvoAPIConnection(EvoFrame.Param, logger);
      batch := GetBatchInfo(conn, logger, EvolutionSinglePayrollFrame.GetPayrollDef);
      RunTimeClockImport(Logger, batch, GetTimeClockData, conn, false);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.



