object ADIOptionsFrm: TADIOptionsFrm
  Left = 0
  Top = 0
  Width = 750
  Height = 270
  Align = alLeft
  Constraints.MinWidth = 750
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 566
    Top = 0
    Width = 15
    Height = 270
    Align = alRight
  end
  object Bevel5: TBevel
    Left = 742
    Top = 0
    Width = 8
    Height = 270
    Align = alRight
    Shape = bsSpacer
  end
  object Bevel6: TBevel
    Left = 0
    Top = 0
    Width = 8
    Height = 270
    Align = alLeft
    Shape = bsSpacer
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 0
    Width = 558
    Height = 270
    Align = alClient
    Caption = 'Employee export'
    Constraints.MinWidth = 313
    TabOrder = 0
    inline FieldsToExportFrame: TFieldsToExportFrm
      Left = 2
      Top = 65
      Width = 554
      Height = 203
      Align = alClient
      TabOrder = 1
      inherited clbFields: TCheckListBox
        Width = 554
        Height = 173
      end
      inherited Panel1: TPanel
        Width = 554
        inherited Label1: TLabel
          Width = 110
          Caption = 'Optional fields to export'
        end
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 554
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 3
        Width = 221
        Height = 13
        Caption = 'Default pay group for newly created employees'
      end
      object cbPayGroup: TComboBox
        Left = 8
        Top = 24
        Width = 297
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
      end
      object cbAllowDeletingOfEE: TCheckBox
        Left = 336
        Top = 25
        Width = 281
        Height = 17
        Caption = 'Allow deletion of ADI employee records'
        TabOrder = 1
        OnClick = cbAllowDeletingOfEEClick
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 581
    Top = 0
    Width = 161
    Height = 270
    Align = alRight
    Caption = 'Timeclock import'
    Constraints.MinWidth = 105
    TabOrder = 1
    object cbSummarize: TCheckBox
      Left = 9
      Top = 72
      Width = 81
      Height = 17
      Caption = 'Summarize'
      TabOrder = 2
      OnClick = cbSummarizeClick
    end
    object cbTransferTempHierachy: TCheckBox
      Left = 9
      Top = 48
      Width = 144
      Height = 17
      Caption = 'Transfer temp D/B/D/T'
      TabOrder = 1
      OnClick = cbTransferTempHierachyClick
    end
    object cbTransferTempRate: TCheckBox
      Left = 9
      Top = 24
      Width = 144
      Height = 17
      Caption = 'Transfer temp rate'
      TabOrder = 0
      OnClick = cbTransferTempRateClick
    end
  end
end
