unit SVSelectionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs,
  kbmMemTable, aditime, Grids, DBGrids, DB, evodata,
  Wwdbigrd, Wwdbgrid, dbcomp, StdCtrls, wwdblook, gdystrset, evoapiconnectionutils;

type
  TSVSelectionFrm = class(TFrame)
    dsSV: TDataSource;
    dsEE: TDataSource;
    dgEE: TReDBGrid;
    dbcbUserID: TwwDBLookupCombo;
  private
    FEE: TkbmMemTable;
  public
    destructor Destroy; override;
    procedure Init(actions: TEEUpdateActionRecs; EEData: TEvoEEData; sv: TkbmCustomMemTable);
    procedure GetSVs(actions: TEEUpdateActionRecs);
  end;

implementation

{$R *.dfm}

uses
  common;

{ TSVSelectionFrm }

destructor TSVSelectionFrm.Destroy;
begin
  FreeAndNil(FEE);
  inherited;
end;

procedure TSVSelectionFrm.Init(actions: TEEUpdateActionRecs; EEData: TEvoEEData; sv: TkbmCustomMemTable);
var
  i: integer;
begin
  dsSV.DataSet := sv;
  FEE := TkbmMemTable.Create(nil);

  CreateStringField( FEE, 'CUSTOM_EMPLOYEE_NUMBER', 'EE Code', 10);
  with CreateStringField( FEE, 'NAME', 'Name', 53) do
  begin
    DisplayWidth := 20;
  end;
  with CreateStringField( FEE, 'DBDT_CUSTOM_NUMBERS', 'D/B/D/T Numbers', 80) do
  begin
    DisplayWidth := 30;
  end;
  with CreateStringField( FEE, 'USER_ID', 'Supervisor''s User Id', SV.FieldByName('USER_ID').DataSize) do
  begin
    Visible := false;
  end;
  with CreateStringField( FEE, 'USER_NAME', 'Supervisor', SV.FieldByName('USER_NAME').DataSize) do
  begin
    FieldKind := fkLookup;
    LookupDataSet := sv;
    LookupKeyFields := 'USER_ID';
    LookupResultField := 'USER_NAME';
    KeyFields := 'USER_ID';
  end;
  FEE.Open;

  for i := 0 to high(actions) do
  begin
    if (actions[i].Action = euaCreate) and not actions[i].HasEvoSV then
    begin
      Assert(EEData.LocateEE(actions[i].Code));

      FEE.Append;
      try
        FEE['CUSTOM_EMPLOYEE_NUMBER'] := EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'];
        FEE['NAME'] := trim(trim(EEData.EEs.FieldByName('LAST_NAME').AsString) + ' ' + trim(EEData.EEs.FieldByName('FIRST_NAME').AsString) + ' ' + trim(EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString));
        FEE['DBDT_CUSTOM_NUMBERS'] := EEData.GetEEDBDTCustomNumbers;
        FEE['USER_ID'] := '';
        FEE.Post;
      except
        FEE.Cancel;
        raise
      end;
    end;
  end;

  FEE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').ReadOnly := true;
  FEE.FieldByName('NAME').ReadOnly := true;
  FEE.FieldByName('DBDT_CUSTOM_NUMBERS').ReadOnly := true;

  dbcbUserID.LookupTable := SV;
  dbcbUserID.LookupField := 'USER_ID';
  dbcbUserID.Selected.Clear;
  AddSelected(dbcbUserID.Selected, SV.FieldByName('USER_NAME') );
  AddSelected(dbcbUserID.Selected, SV.FieldByName('USER_ID') );

  dgEE.SetControlType('USER_NAME', fctCustom, 'dbcbUserID');

  FEE.AddIndex('SORT', 'CUSTOM_EMPLOYEE_NUMBER', [ixCaseInsensitive]);
  FEE.IndexName := 'SORT';
  FEE.IndexDefs.Update;

  dsEE.DataSet := FEE;
end;

procedure TSVSelectionFrm.GetSVs(actions: TEEUpdateActionRecs);
var
  i: integer;
begin
  if FEE.State = dsEdit then
    FEE.Post;
  Assert(FEE.State = dsBrowse);
  FEE.Filtered := false; //user filter may be set by grid
//  FEE.UserFiltered := false; //if it were TISkbmDataSet
  for i := 0 to high(actions) do
  begin
    if (actions[i].Action = euaCreate) and not actions[i].HasEvoSV then
    begin
      actions[i].SVUserID := '';
      FEE.First;
      while not FEE.Eof do
      begin
        if trim(FEE['CUSTOM_EMPLOYEE_NUMBER']) = trim(actions[i].code) then
        begin
          actions[i].SVUserID := FEE.FieldByName('USER_ID').AsString;
          break;
        end;
        FEE.Next;
      end;
      Assert(not FEE.Eof);
    end
  end;
end;

end.
