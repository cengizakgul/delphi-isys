unit ConfirmationForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls,
  kbmMemTable, evoapiconnectionutils, aditime, db;

type
  TConfirmationFm = class(TForm)
    Panel1: TPanel;
    bbCancel: TBitBtn;
    bbOk: TBitBtn;
    Panel2: TPanel;
    mmActions: TMemo;
    procedure FormActivate(Sender: TObject);
  private
  public
  end;

function ConfirmActions(Owner: TComponent; actions: TEEUpdateActionRecs): boolean;

implementation

{$R *.dfm}

function ConfirmActions(Owner: TComponent; actions: TEEUpdateActionRecs): boolean;
begin
  if Length(actions) > 0  then
    with TConfirmationFm.Create(Owner) do
    try
      Caption := Application.Title;
      mmActions.Lines.Text := EEUpdateActionRecsToString(actions);
      Result := ShowModal = mrOk;
    finally
      Free;
    end
  else
    Result := true;
end;

procedure TConfirmationFm.FormActivate(Sender: TObject);
begin
  SetForegroundWindow(Handle);
end;

end.
