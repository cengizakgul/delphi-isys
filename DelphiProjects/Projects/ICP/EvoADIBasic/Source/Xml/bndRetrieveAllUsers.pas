
{****************************************************************************}
{                                                                            }
{                              XML Data Binding                              }
{                                                                            }
{         Generated on: 6/19/2009 7:42:15 PM                                 }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllUsers.xml   }
{                                                                            }
{****************************************************************************}

unit bndRetrieveAllUsers;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLUsersType = interface;
  IXMLUserType = interface;
  IXMLSupervisortypeType = interface;
  IXMLIsactiveType = interface;
  IXMLIswebactiveType = interface;
  IXMLIsemployeeenabledType = interface;
  IXMLIswebenabledType = interface;
  IXMLAccessByEmpAssignType = interface;
  IXMLAssignablelevelType = interface;
  IXMLAccesslevelType = interface;
  IXMLLevelType = interface;
  IXMLRolesType = interface;
  IXMLRoleType = interface;
  IXMLEmployeeroleType = interface;
  IXMLLoginpermittedonadiwebdeactivatedType = interface;

{ IXMLUsersType }

  IXMLUsersType = interface(IXMLNodeCollection)
    ['{98FCF65C-0971-4B60-9F22-66DB3251F7AB}']
    { Property Accessors }
    function Get_User(Index: Integer): IXMLUserType;
    { Methods & Properties }
    function Add: IXMLUserType;
    function Insert(const Index: Integer): IXMLUserType;
    property User[Index: Integer]: IXMLUserType read Get_User; default;
  end;

{ IXMLUserType }

  IXMLUserType = interface(IXMLNode)
    ['{281AA654-192B-4896-A65C-362F8ACD4373}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get__id: Integer;
    function Get_Userid: WideString;
    function Get_Username: WideString;
    function Get_Loginid: WideString;
    function Get_Emailrecipients: WideString;
    function Get_Empusertype: WideString;
    function Get_Supervisortype: IXMLSupervisortypeType;
    function Get_Isactive: IXMLIsactiveType;
    function Get_Iswebactive: IXMLIswebactiveType;
    function Get_Isemployeeenabled: IXMLIsemployeeenabledType;
    function Get_Iswebenabled: IXMLIswebenabledType;
    function Get_Supervisorid: WideString;
    function Get_Timeclocknbr: Integer;
    function Get_Supervisor: WideString;
    function Get_AccessByEmpAssign: IXMLAccessByEmpAssignType;
    function Get_Assignablelevel: IXMLAssignablelevelType;
    function Get_Accesslevel: IXMLAccesslevelType;
    function Get_Level: IXMLLevelType;
    function Get_Roles: IXMLRolesType;
    procedure Set_Id(Value: WideString);
    procedure Set__id(Value: Integer);
    procedure Set_Userid(Value: WideString);
    procedure Set_Username(Value: WideString);
    procedure Set_Loginid(Value: WideString);
    procedure Set_Emailrecipients(Value: WideString);
    procedure Set_Empusertype(Value: WideString);
    procedure Set_Supervisorid(Value: WideString);
    procedure Set_Timeclocknbr(Value: Integer);
    procedure Set_Supervisor(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property _id: Integer read Get__id write Set__id;
    property Userid: WideString read Get_Userid write Set_Userid;
    property Username: WideString read Get_Username write Set_Username;
    property Loginid: WideString read Get_Loginid write Set_Loginid;
    property Emailrecipients: WideString read Get_Emailrecipients write Set_Emailrecipients;
    property Empusertype: WideString read Get_Empusertype write Set_Empusertype;
    property Supervisortype: IXMLSupervisortypeType read Get_Supervisortype;
    property Isactive: IXMLIsactiveType read Get_Isactive;
    property Iswebactive: IXMLIswebactiveType read Get_Iswebactive;
    property Isemployeeenabled: IXMLIsemployeeenabledType read Get_Isemployeeenabled;
    property Iswebenabled: IXMLIswebenabledType read Get_Iswebenabled;
    property Supervisorid: WideString read Get_Supervisorid write Set_Supervisorid;
    property Timeclocknbr: Integer read Get_Timeclocknbr write Set_Timeclocknbr;
    property Supervisor: WideString read Get_Supervisor write Set_Supervisor;
    property AccessByEmpAssign: IXMLAccessByEmpAssignType read Get_AccessByEmpAssign;
    property Assignablelevel: IXMLAssignablelevelType read Get_Assignablelevel;
    property Accesslevel: IXMLAccesslevelType read Get_Accesslevel;
    property Level: IXMLLevelType read Get_Level;
    property Roles: IXMLRolesType read Get_Roles;
  end;

{ IXMLSupervisortypeType }

  IXMLSupervisortypeType = interface(IXMLNode)
    ['{E1450923-6A82-494E-AA6F-52FFC2B1B9B9}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIsactiveType }

  IXMLIsactiveType = interface(IXMLNode)
    ['{B636D9C9-24D5-4129-8239-03E19225B951}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIswebactiveType }

  IXMLIswebactiveType = interface(IXMLNode)
    ['{86C6E5AC-2F95-4F50-A7E6-3BF46B371DEC}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIsemployeeenabledType }

  IXMLIsemployeeenabledType = interface(IXMLNode)
    ['{56B30390-CFA8-4980-8186-A9193B15FC25}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIswebenabledType }

  IXMLIswebenabledType = interface(IXMLNode)
    ['{45F593B1-24AF-49D6-9FF2-4D725816E893}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAccessByEmpAssignType }

  IXMLAccessByEmpAssignType = interface(IXMLNode)
    ['{D2D036B1-E8EC-453D-879C-83D23ECB23E0}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAssignablelevelType }

  IXMLAssignablelevelType = interface(IXMLNode)
    ['{0FBEAF57-5B53-409C-96F5-36A30E9CE63A}']
    { Property Accessors }
    function Get_Type_: Integer;
    procedure Set_Type_(Value: Integer);
    { Methods & Properties }
    property Type_: Integer read Get_Type_ write Set_Type_;
  end;

{ IXMLAccesslevelType }

  IXMLAccesslevelType = interface(IXMLNode)
    ['{50B75511-81B0-4591-B183-AC2F77E09624}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLLevelType }

  IXMLLevelType = interface(IXMLNode)
    ['{3264FDBE-818A-4767-A790-F9665CC1EE5F}']
    { Property Accessors }
    function Get_Type_: WideString;
    procedure Set_Type_(Value: WideString);
    { Methods & Properties }
    property Type_: WideString read Get_Type_ write Set_Type_;
  end;

{ IXMLRolesType }

  IXMLRolesType = interface(IXMLNode)
    ['{DA6B4B0E-75B6-4E68-B1BE-40AA2C422EFB}']
    { Property Accessors }
    function Get_Role: IXMLRoleType;
    { Methods & Properties }
    property Role: IXMLRoleType read Get_Role;
  end;

{ IXMLRoleType }

  IXMLRoleType = interface(IXMLNode)
    ['{E62D0C63-D253-42B7-9D8D-DE7BAF73DD8D}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Rolename: WideString;
    function Get_Roledescription: WideString;
    function Get_Employeerole: IXMLEmployeeroleType;
    function Get_Loginpermittedonadiwebdeactivated: IXMLLoginpermittedonadiwebdeactivatedType;
    procedure Set_Id(Value: WideString);
    procedure Set_Rolename(Value: WideString);
    procedure Set_Roledescription(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Rolename: WideString read Get_Rolename write Set_Rolename;
    property Roledescription: WideString read Get_Roledescription write Set_Roledescription;
    property Employeerole: IXMLEmployeeroleType read Get_Employeerole;
    property Loginpermittedonadiwebdeactivated: IXMLLoginpermittedonadiwebdeactivatedType read Get_Loginpermittedonadiwebdeactivated;
  end;

{ IXMLEmployeeroleType }

  IXMLEmployeeroleType = interface(IXMLNode)
    ['{360368C5-D71B-45FB-AE8F-9E8505EFA7FC}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLLoginpermittedonadiwebdeactivatedType }

  IXMLLoginpermittedonadiwebdeactivatedType = interface(IXMLNode)
    ['{C5F03F06-DDAC-4DBB-AFD3-82DFE067AE73}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ Forward Decls }

  TXMLUsersType = class;
  TXMLUserType = class;
  TXMLSupervisortypeType = class;
  TXMLIsactiveType = class;
  TXMLIswebactiveType = class;
  TXMLIsemployeeenabledType = class;
  TXMLIswebenabledType = class;
  TXMLAccessByEmpAssignType = class;
  TXMLAssignablelevelType = class;
  TXMLAccesslevelType = class;
  TXMLLevelType = class;
  TXMLRolesType = class;
  TXMLRoleType = class;
  TXMLEmployeeroleType = class;
  TXMLLoginpermittedonadiwebdeactivatedType = class;

{ TXMLUsersType }

  TXMLUsersType = class(TXMLNodeCollection, IXMLUsersType)
  protected
    { IXMLUsersType }
    function Get_User(Index: Integer): IXMLUserType;
    function Add: IXMLUserType;
    function Insert(const Index: Integer): IXMLUserType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLUserType }

  TXMLUserType = class(TXMLNode, IXMLUserType)
  protected
    { IXMLUserType }
    function Get_Id: WideString;
    function Get__id: Integer;
    function Get_Userid: WideString;
    function Get_Username: WideString;
    function Get_Loginid: WideString;
    function Get_Emailrecipients: WideString;
    function Get_Empusertype: WideString;
    function Get_Supervisortype: IXMLSupervisortypeType;
    function Get_Isactive: IXMLIsactiveType;
    function Get_Iswebactive: IXMLIswebactiveType;
    function Get_Isemployeeenabled: IXMLIsemployeeenabledType;
    function Get_Iswebenabled: IXMLIswebenabledType;
    function Get_Supervisorid: WideString;
    function Get_Timeclocknbr: Integer;
    function Get_Supervisor: WideString;
    function Get_AccessByEmpAssign: IXMLAccessByEmpAssignType;
    function Get_Assignablelevel: IXMLAssignablelevelType;
    function Get_Accesslevel: IXMLAccesslevelType;
    function Get_Level: IXMLLevelType;
    function Get_Roles: IXMLRolesType;
    procedure Set_Id(Value: WideString);
    procedure Set__id(Value: Integer);
    procedure Set_Userid(Value: WideString);
    procedure Set_Username(Value: WideString);
    procedure Set_Loginid(Value: WideString);
    procedure Set_Emailrecipients(Value: WideString);
    procedure Set_Empusertype(Value: WideString);
    procedure Set_Supervisorid(Value: WideString);
    procedure Set_Timeclocknbr(Value: Integer);
    procedure Set_Supervisor(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSupervisortypeType }

  TXMLSupervisortypeType = class(TXMLNode, IXMLSupervisortypeType)
  protected
    { IXMLSupervisortypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIsactiveType }

  TXMLIsactiveType = class(TXMLNode, IXMLIsactiveType)
  protected
    { IXMLIsactiveType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIswebactiveType }

  TXMLIswebactiveType = class(TXMLNode, IXMLIswebactiveType)
  protected
    { IXMLIswebactiveType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIsemployeeenabledType }

  TXMLIsemployeeenabledType = class(TXMLNode, IXMLIsemployeeenabledType)
  protected
    { IXMLIsemployeeenabledType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIswebenabledType }

  TXMLIswebenabledType = class(TXMLNode, IXMLIswebenabledType)
  protected
    { IXMLIswebenabledType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAccessByEmpAssignType }

  TXMLAccessByEmpAssignType = class(TXMLNode, IXMLAccessByEmpAssignType)
  protected
    { IXMLAccessByEmpAssignType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAssignablelevelType }

  TXMLAssignablelevelType = class(TXMLNode, IXMLAssignablelevelType)
  protected
    { IXMLAssignablelevelType }
    function Get_Type_: Integer;
    procedure Set_Type_(Value: Integer);
  end;

{ TXMLAccesslevelType }

  TXMLAccesslevelType = class(TXMLNode, IXMLAccesslevelType)
  protected
    { IXMLAccesslevelType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLLevelType }

  TXMLLevelType = class(TXMLNode, IXMLLevelType)
  protected
    { IXMLLevelType }
    function Get_Type_: WideString;
    procedure Set_Type_(Value: WideString);
  end;

{ TXMLRolesType }

  TXMLRolesType = class(TXMLNode, IXMLRolesType)
  protected
    { IXMLRolesType }
    function Get_Role: IXMLRoleType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRoleType }

  TXMLRoleType = class(TXMLNode, IXMLRoleType)
  protected
    { IXMLRoleType }
    function Get_Id: WideString;
    function Get_Rolename: WideString;
    function Get_Roledescription: WideString;
    function Get_Employeerole: IXMLEmployeeroleType;
    function Get_Loginpermittedonadiwebdeactivated: IXMLLoginpermittedonadiwebdeactivatedType;
    procedure Set_Id(Value: WideString);
    procedure Set_Rolename(Value: WideString);
    procedure Set_Roledescription(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmployeeroleType }

  TXMLEmployeeroleType = class(TXMLNode, IXMLEmployeeroleType)
  protected
    { IXMLEmployeeroleType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLLoginpermittedonadiwebdeactivatedType }

  TXMLLoginpermittedonadiwebdeactivatedType = class(TXMLNode, IXMLLoginpermittedonadiwebdeactivatedType)
  protected
    { IXMLLoginpermittedonadiwebdeactivatedType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ Global Functions }

function Getusers(Doc: IXMLDocument): IXMLUsersType;
function Loadusers(const FileName: WideString): IXMLUsersType;
function Newusers: IXMLUsersType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getusers(Doc: IXMLDocument): IXMLUsersType;
begin
  Result := Doc.GetDocBinding('users', TXMLUsersType, TargetNamespace) as IXMLUsersType;
end;

function Loadusers(const FileName: WideString): IXMLUsersType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('users', TXMLUsersType, TargetNamespace) as IXMLUsersType;
end;

function Newusers: IXMLUsersType;
begin
  Result := NewXMLDocument.GetDocBinding('users', TXMLUsersType, TargetNamespace) as IXMLUsersType;
end;

{ TXMLUsersType }

procedure TXMLUsersType.AfterConstruction;
begin
  RegisterChildNode('user', TXMLUserType);
  ItemTag := 'user';
  ItemInterface := IXMLUserType;
  inherited;
end;

function TXMLUsersType.Get_User(Index: Integer): IXMLUserType;
begin
  Result := List[Index] as IXMLUserType;
end;

function TXMLUsersType.Add: IXMLUserType;
begin
  Result := AddItem(-1) as IXMLUserType;
end;

function TXMLUsersType.Insert(const Index: Integer): IXMLUserType;
begin
  Result := AddItem(Index) as IXMLUserType;
end;

{ TXMLUserType }

procedure TXMLUserType.AfterConstruction;
begin
  RegisterChildNode('supervisortype', TXMLSupervisortypeType);
  RegisterChildNode('isactive', TXMLIsactiveType);
  RegisterChildNode('iswebactive', TXMLIswebactiveType);
  RegisterChildNode('isemployeeenabled', TXMLIsemployeeenabledType);
  RegisterChildNode('iswebenabled', TXMLIswebenabledType);
  RegisterChildNode('accessByEmpAssign', TXMLAccessByEmpAssignType);
  RegisterChildNode('assignablelevel', TXMLAssignablelevelType);
  RegisterChildNode('accesslevel', TXMLAccesslevelType);
  RegisterChildNode('level', TXMLLevelType);
  RegisterChildNode('roles', TXMLRolesType);
  inherited;
end;

function TXMLUserType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLUserType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLUserType.Get__id: Integer;
begin
  Result := ChildNodes['_id'].NodeValue;
end;

procedure TXMLUserType.Set__id(Value: Integer);
begin
  ChildNodes['_id'].NodeValue := Value;
end;

function TXMLUserType.Get_Userid: WideString;
begin
  Result := ChildNodes['userid'].Text;
end;

procedure TXMLUserType.Set_Userid(Value: WideString);
begin
  ChildNodes['userid'].NodeValue := Value;
end;

function TXMLUserType.Get_Username: WideString;
begin
  Result := ChildNodes['username'].Text;
end;

procedure TXMLUserType.Set_Username(Value: WideString);
begin
  ChildNodes['username'].NodeValue := Value;
end;

function TXMLUserType.Get_Loginid: WideString;
begin
  Result := ChildNodes['loginid'].Text;
end;

procedure TXMLUserType.Set_Loginid(Value: WideString);
begin
  ChildNodes['loginid'].NodeValue := Value;
end;

function TXMLUserType.Get_Emailrecipients: WideString;
begin
  Result := ChildNodes['emailrecipients'].Text;
end;

procedure TXMLUserType.Set_Emailrecipients(Value: WideString);
begin
  ChildNodes['emailrecipients'].NodeValue := Value;
end;

function TXMLUserType.Get_Empusertype: WideString;
begin
  Result := ChildNodes['empusertype'].Text;
end;

procedure TXMLUserType.Set_Empusertype(Value: WideString);
begin
  ChildNodes['empusertype'].NodeValue := Value;
end;

function TXMLUserType.Get_Supervisortype: IXMLSupervisortypeType;
begin
  Result := ChildNodes['supervisortype'] as IXMLSupervisortypeType;
end;

function TXMLUserType.Get_Isactive: IXMLIsactiveType;
begin
  Result := ChildNodes['isactive'] as IXMLIsactiveType;
end;

function TXMLUserType.Get_Iswebactive: IXMLIswebactiveType;
begin
  Result := ChildNodes['iswebactive'] as IXMLIswebactiveType;
end;

function TXMLUserType.Get_Isemployeeenabled: IXMLIsemployeeenabledType;
begin
  Result := ChildNodes['isemployeeenabled'] as IXMLIsemployeeenabledType;
end;

function TXMLUserType.Get_Iswebenabled: IXMLIswebenabledType;
begin
  Result := ChildNodes['iswebenabled'] as IXMLIswebenabledType;
end;

function TXMLUserType.Get_Supervisorid: WideString;
begin
  Result := ChildNodes['supervisorid'].Text;
end;

procedure TXMLUserType.Set_Supervisorid(Value: WideString);
begin
  ChildNodes['supervisorid'].NodeValue := Value;
end;

function TXMLUserType.Get_Timeclocknbr: Integer;
begin
  Result := ChildNodes['timeclocknbr'].NodeValue;
end;

procedure TXMLUserType.Set_Timeclocknbr(Value: Integer);
begin
  ChildNodes['timeclocknbr'].NodeValue := Value;
end;

function TXMLUserType.Get_Supervisor: WideString;
begin
  Result := ChildNodes['supervisor'].Text;
end;

procedure TXMLUserType.Set_Supervisor(Value: WideString);
begin
  ChildNodes['supervisor'].NodeValue := Value;
end;

function TXMLUserType.Get_AccessByEmpAssign: IXMLAccessByEmpAssignType;
begin
  Result := ChildNodes['accessByEmpAssign'] as IXMLAccessByEmpAssignType;
end;

function TXMLUserType.Get_Assignablelevel: IXMLAssignablelevelType;
begin
  Result := ChildNodes['assignablelevel'] as IXMLAssignablelevelType;
end;

function TXMLUserType.Get_Accesslevel: IXMLAccesslevelType;
begin
  Result := ChildNodes['accesslevel'] as IXMLAccesslevelType;
end;

function TXMLUserType.Get_Level: IXMLLevelType;
begin
  Result := ChildNodes['level'] as IXMLLevelType;
end;

function TXMLUserType.Get_Roles: IXMLRolesType;
begin
  Result := ChildNodes['roles'] as IXMLRolesType;
end;

{ TXMLSupervisortypeType }

function TXMLSupervisortypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLSupervisortypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIsactiveType }

function TXMLIsactiveType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsactiveType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIswebactiveType }

function TXMLIswebactiveType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIswebactiveType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIsemployeeenabledType }

function TXMLIsemployeeenabledType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsemployeeenabledType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIswebenabledType }

function TXMLIswebenabledType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIswebenabledType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAccessByEmpAssignType }

function TXMLAccessByEmpAssignType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAccessByEmpAssignType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAssignablelevelType }

function TXMLAssignablelevelType.Get_Type_: Integer;
begin
  Result := AttributeNodes['type'].NodeValue;
end;

procedure TXMLAssignablelevelType.Set_Type_(Value: Integer);
begin
  SetAttribute('type', Value);
end;

{ TXMLAccesslevelType }

function TXMLAccesslevelType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAccesslevelType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLLevelType }

function TXMLLevelType.Get_Type_: WideString;
begin
  Result := AttributeNodes['type'].Text;
end;

procedure TXMLLevelType.Set_Type_(Value: WideString);
begin
  SetAttribute('type', Value);
end;

{ TXMLRolesType }

procedure TXMLRolesType.AfterConstruction;
begin
  RegisterChildNode('role', TXMLRoleType);
  inherited;
end;

function TXMLRolesType.Get_Role: IXMLRoleType;
begin
  Result := ChildNodes['role'] as IXMLRoleType;
end;

{ TXMLRoleType }

procedure TXMLRoleType.AfterConstruction;
begin
  RegisterChildNode('employeerole', TXMLEmployeeroleType);
  RegisterChildNode('loginpermittedonadiwebdeactivated', TXMLLoginpermittedonadiwebdeactivatedType);
  inherited;
end;

function TXMLRoleType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLRoleType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLRoleType.Get_Rolename: WideString;
begin
  Result := ChildNodes['rolename'].Text;
end;

procedure TXMLRoleType.Set_Rolename(Value: WideString);
begin
  ChildNodes['rolename'].NodeValue := Value;
end;

function TXMLRoleType.Get_Roledescription: WideString;
begin
  Result := ChildNodes['roledescription'].Text;
end;

procedure TXMLRoleType.Set_Roledescription(Value: WideString);
begin
  ChildNodes['roledescription'].NodeValue := Value;
end;

function TXMLRoleType.Get_Employeerole: IXMLEmployeeroleType;
begin
  Result := ChildNodes['employeerole'] as IXMLEmployeeroleType;
end;

function TXMLRoleType.Get_Loginpermittedonadiwebdeactivated: IXMLLoginpermittedonadiwebdeactivatedType;
begin
  Result := ChildNodes['loginpermittedonadiwebdeactivated'] as IXMLLoginpermittedonadiwebdeactivatedType;
end;

{ TXMLEmployeeroleType }

function TXMLEmployeeroleType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLEmployeeroleType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLLoginpermittedonadiwebdeactivatedType }

function TXMLLoginpermittedonadiwebdeactivatedType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLLoginpermittedonadiwebdeactivatedType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

end.