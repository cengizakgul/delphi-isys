
{******************************************************************************************}
{                                                                                          }
{                                     XML Data Binding                                     }
{                                                                                          }
{         Generated on: 7/14/2009 5:38:39 PM                                               }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllCodes\Output_Code 7.xml   }
{                                                                                          }
{******************************************************************************************}

unit bndStateMasters;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLStatemastersType = interface;
  IXMLStatemasterType = interface;
  IXMLIstaxcodeenabledType = interface;

{ IXMLStatemastersType }

  IXMLStatemastersType = interface(IXMLNodeCollection)
    ['{7F68D26A-65B5-447A-9980-4E1F5077E615}']
    { Property Accessors }
    function Get_Morerecords: Integer;
    function Get_Statemaster(Index: Integer): IXMLStatemasterType;
    procedure Set_Morerecords(Value: Integer);
    { Methods & Properties }
    function Add: IXMLStatemasterType;
    function Insert(const Index: Integer): IXMLStatemasterType;
    property Morerecords: Integer read Get_Morerecords write Set_Morerecords;
    property Statemaster[Index: Integer]: IXMLStatemasterType read Get_Statemaster; default;
  end;

{ IXMLStatemasterType }

  IXMLStatemasterType = interface(IXMLNode)
    ['{835ABCDD-736C-4639-BEA1-96C2A6BE40CD}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Statetaxcode: WideString;
    function Get_Abbreviation: WideString;
    function Get_Istaxcodeenabled: IXMLIstaxcodeenabledType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Statetaxcode(Value: WideString);
    procedure Set_Abbreviation(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
    property Statetaxcode: WideString read Get_Statetaxcode write Set_Statetaxcode;
    property Abbreviation: WideString read Get_Abbreviation write Set_Abbreviation;
    property Istaxcodeenabled: IXMLIstaxcodeenabledType read Get_Istaxcodeenabled;
  end;

{ IXMLIstaxcodeenabledType }

  IXMLIstaxcodeenabledType = interface(IXMLNode)
    ['{3BB89365-72ED-43F5-BC0C-C1126F61A6F4}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ Forward Decls }

  TXMLStatemastersType = class;
  TXMLStatemasterType = class;
  TXMLIstaxcodeenabledType = class;

{ TXMLStatemastersType }

  TXMLStatemastersType = class(TXMLNodeCollection, IXMLStatemastersType)
  protected
    { IXMLStatemastersType }
    function Get_Morerecords: Integer;
    function Get_Statemaster(Index: Integer): IXMLStatemasterType;
    procedure Set_Morerecords(Value: Integer);
    function Add: IXMLStatemasterType;
    function Insert(const Index: Integer): IXMLStatemasterType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLStatemasterType }

  TXMLStatemasterType = class(TXMLNode, IXMLStatemasterType)
  protected
    { IXMLStatemasterType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Statetaxcode: WideString;
    function Get_Abbreviation: WideString;
    function Get_Istaxcodeenabled: IXMLIstaxcodeenabledType;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Statetaxcode(Value: WideString);
    procedure Set_Abbreviation(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIstaxcodeenabledType }

  TXMLIstaxcodeenabledType = class(TXMLNode, IXMLIstaxcodeenabledType)
  protected
    { IXMLIstaxcodeenabledType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ Global Functions }

function Getstatemasters(Doc: IXMLDocument): IXMLStatemastersType;
function Loadstatemasters(const FileName: WideString): IXMLStatemastersType;
function Newstatemasters: IXMLStatemastersType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getstatemasters(Doc: IXMLDocument): IXMLStatemastersType;
begin
  Result := Doc.GetDocBinding('statemasters', TXMLStatemastersType, TargetNamespace) as IXMLStatemastersType;
end;

function Loadstatemasters(const FileName: WideString): IXMLStatemastersType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('statemasters', TXMLStatemastersType, TargetNamespace) as IXMLStatemastersType;
end;

function Newstatemasters: IXMLStatemastersType;
begin
  Result := NewXMLDocument.GetDocBinding('statemasters', TXMLStatemastersType, TargetNamespace) as IXMLStatemastersType;
end;

{ TXMLStatemastersType }

procedure TXMLStatemastersType.AfterConstruction;
begin
  RegisterChildNode('statemaster', TXMLStatemasterType);
  ItemTag := 'statemaster';
  ItemInterface := IXMLStatemasterType;
  inherited;
end;

function TXMLStatemastersType.Get_Morerecords: Integer;
begin
  Result := AttributeNodes['morerecords'].NodeValue;
end;

procedure TXMLStatemastersType.Set_Morerecords(Value: Integer);
begin
  SetAttribute('morerecords', Value);
end;

function TXMLStatemastersType.Get_Statemaster(Index: Integer): IXMLStatemasterType;
begin
  Result := List[Index] as IXMLStatemasterType;
end;

function TXMLStatemastersType.Add: IXMLStatemasterType;
begin
  Result := AddItem(-1) as IXMLStatemasterType;
end;

function TXMLStatemastersType.Insert(const Index: Integer): IXMLStatemasterType;
begin
  Result := AddItem(Index) as IXMLStatemasterType;
end;

{ TXMLStatemasterType }

procedure TXMLStatemasterType.AfterConstruction;
begin
  RegisterChildNode('istaxcodeenabled', TXMLIstaxcodeenabledType);
  inherited;
end;

function TXMLStatemasterType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLStatemasterType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLStatemasterType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLStatemasterType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

function TXMLStatemasterType.Get_Statetaxcode: WideString;
begin
  Result := ChildNodes['statetaxcode'].Text;
end;

procedure TXMLStatemasterType.Set_Statetaxcode(Value: WideString);
begin
  ChildNodes['statetaxcode'].NodeValue := Value;
end;

function TXMLStatemasterType.Get_Abbreviation: WideString;
begin
  Result := ChildNodes['abbreviation'].Text;
end;

procedure TXMLStatemasterType.Set_Abbreviation(Value: WideString);
begin
  ChildNodes['abbreviation'].NodeValue := Value;
end;

function TXMLStatemasterType.Get_Istaxcodeenabled: IXMLIstaxcodeenabledType;
begin
  Result := ChildNodes['istaxcodeenabled'] as IXMLIstaxcodeenabledType;
end;

{ TXMLIstaxcodeenabledType }

function TXMLIstaxcodeenabledType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIstaxcodeenabledType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

end.