
{***********************************************************************************}
{                                                                                   }
{                                 XML Data Binding                                  }
{                                                                                   }
{         Generated on: 6/5/2009 5:10:52 PM                                         }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveSettings\Output.xml   }
{                                                                                   }
{***********************************************************************************}

unit bndRetrieveSettings;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLSettingsType = interface;

{ IXMLSettingsType }

  IXMLSettingsType = interface(IXMLNode)
    ['{9B0BD213-BD28-48CA-9DFD-1F7E5BD79AC2}']
    { Property Accessors }
    function Get_Isaccrualsenabled: Integer;
    function Get_Ispointsenabled: Integer;
    function Get_Isleaverequestenabled: Integer;
    function Get_Isdatacollectionterminalenabled: Integer;
    function Get_Enablesupervisor: Integer;
    procedure Set_Isaccrualsenabled(Value: Integer);
    procedure Set_Ispointsenabled(Value: Integer);
    procedure Set_Isleaverequestenabled(Value: Integer);
    procedure Set_Isdatacollectionterminalenabled(Value: Integer);
    procedure Set_Enablesupervisor(Value: Integer);
    { Methods & Properties }
    property Isaccrualsenabled: Integer read Get_Isaccrualsenabled write Set_Isaccrualsenabled;
    property Ispointsenabled: Integer read Get_Ispointsenabled write Set_Ispointsenabled;
    property Isleaverequestenabled: Integer read Get_Isleaverequestenabled write Set_Isleaverequestenabled;
    property Isdatacollectionterminalenabled: Integer read Get_Isdatacollectionterminalenabled write Set_Isdatacollectionterminalenabled;
    property Enablesupervisor: Integer read Get_Enablesupervisor write Set_Enablesupervisor;
  end;

{ Forward Decls }

  TXMLSettingsType = class;

{ TXMLSettingsType }

  TXMLSettingsType = class(TXMLNode, IXMLSettingsType)
  protected
    { IXMLSettingsType }
    function Get_Isaccrualsenabled: Integer;
    function Get_Ispointsenabled: Integer;
    function Get_Isleaverequestenabled: Integer;
    function Get_Isdatacollectionterminalenabled: Integer;
    function Get_Enablesupervisor: Integer;
    procedure Set_Isaccrualsenabled(Value: Integer);
    procedure Set_Ispointsenabled(Value: Integer);
    procedure Set_Isleaverequestenabled(Value: Integer);
    procedure Set_Isdatacollectionterminalenabled(Value: Integer);
    procedure Set_Enablesupervisor(Value: Integer);
  end;

{ Global Functions }

function Getsettings(Doc: IXMLDocument): IXMLSettingsType;
function Loadsettings(const FileName: WideString): IXMLSettingsType;
function Newsettings: IXMLSettingsType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getsettings(Doc: IXMLDocument): IXMLSettingsType;
begin
  Result := Doc.GetDocBinding('settings', TXMLSettingsType, TargetNamespace) as IXMLSettingsType;
end;

function Loadsettings(const FileName: WideString): IXMLSettingsType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('settings', TXMLSettingsType, TargetNamespace) as IXMLSettingsType;
end;

function Newsettings: IXMLSettingsType;
begin
  Result := NewXMLDocument.GetDocBinding('settings', TXMLSettingsType, TargetNamespace) as IXMLSettingsType;
end;

{ TXMLSettingsType }

function TXMLSettingsType.Get_Isaccrualsenabled: Integer;
begin
  Result := ChildNodes['isaccrualsenabled'].NodeValue;
end;

procedure TXMLSettingsType.Set_Isaccrualsenabled(Value: Integer);
begin
  ChildNodes['isaccrualsenabled'].NodeValue := Value;
end;

function TXMLSettingsType.Get_Ispointsenabled: Integer;
begin
  Result := ChildNodes['ispointsenabled'].NodeValue;
end;

procedure TXMLSettingsType.Set_Ispointsenabled(Value: Integer);
begin
  ChildNodes['ispointsenabled'].NodeValue := Value;
end;

function TXMLSettingsType.Get_Isleaverequestenabled: Integer;
begin
  Result := ChildNodes['isleaverequestenabled'].NodeValue;
end;

procedure TXMLSettingsType.Set_Isleaverequestenabled(Value: Integer);
begin
  ChildNodes['isleaverequestenabled'].NodeValue := Value;
end;

function TXMLSettingsType.Get_Isdatacollectionterminalenabled: Integer;
begin
  Result := ChildNodes['isdatacollectionterminalenabled'].NodeValue;
end;

procedure TXMLSettingsType.Set_Isdatacollectionterminalenabled(Value: Integer);
begin
  ChildNodes['isdatacollectionterminalenabled'].NodeValue := Value;
end;

function TXMLSettingsType.Get_Enablesupervisor: Integer;
begin
  Result := ChildNodes['enablesupervisor'].NodeValue;
end;

procedure TXMLSettingsType.Set_Enablesupervisor(Value: Integer);
begin
  ChildNodes['enablesupervisor'].NodeValue := Value;
end;

end.