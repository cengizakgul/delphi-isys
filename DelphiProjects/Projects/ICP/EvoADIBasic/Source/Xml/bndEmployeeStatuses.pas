
{******************************************************************************************}
{                                                                                          }
{                                     XML Data Binding                                     }
{                                                                                          }
{         Generated on: 6/5/2009 1:57:57 PM                                                }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllCodes\Output_Code 6.xml   }
{                                                                                          }
{******************************************************************************************}

unit bndEmployeeStatuses;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLEmployeestatusesType = interface;
  IXMLEmployeestatusType = interface;
  IXMLStatusclassType = interface;

{ IXMLEmployeestatusesType }

  IXMLEmployeestatusesType = interface(IXMLNodeCollection)
    ['{54B0DCC7-DA77-47DB-BF69-517AC37B7B48}']
    { Property Accessors }
    function Get_Morerecords: Integer;
    function Get_Employeestatus(Index: Integer): IXMLEmployeestatusType;
    procedure Set_Morerecords(Value: Integer);
    { Methods & Properties }
    function Add: IXMLEmployeestatusType;
    function Insert(const Index: Integer): IXMLEmployeestatusType;
    property Morerecords: Integer read Get_Morerecords write Set_Morerecords;
    property Employeestatus[Index: Integer]: IXMLEmployeestatusType read Get_Employeestatus; default;
  end;

{ IXMLEmployeestatusType }

  IXMLEmployeestatusType = interface(IXMLNode)
    ['{AEC41E0F-84FF-4BF7-9290-901ECB752BB6}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Code: WideString;
    function Get_Description: WideString;
    function Get_Statusclass: IXMLStatusclassType;
    procedure Set_Id(Value: WideString);
    procedure Set_Code(Value: WideString);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Code: WideString read Get_Code write Set_Code;
    property Description: WideString read Get_Description write Set_Description;
    property Statusclass: IXMLStatusclassType read Get_Statusclass;
  end;

{ IXMLStatusclassType }

  IXMLStatusclassType = interface(IXMLNode)
    ['{1CA09FB0-3C29-4E08-AFF5-4EBEB607737C}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ Forward Decls }

  TXMLEmployeestatusesType = class;
  TXMLEmployeestatusType = class;
  TXMLStatusclassType = class;

{ TXMLEmployeestatusesType }

  TXMLEmployeestatusesType = class(TXMLNodeCollection, IXMLEmployeestatusesType)
  protected
    { IXMLEmployeestatusesType }
    function Get_Morerecords: Integer;
    function Get_Employeestatus(Index: Integer): IXMLEmployeestatusType;
    procedure Set_Morerecords(Value: Integer);
    function Add: IXMLEmployeestatusType;
    function Insert(const Index: Integer): IXMLEmployeestatusType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmployeestatusType }

  TXMLEmployeestatusType = class(TXMLNode, IXMLEmployeestatusType)
  protected
    { IXMLEmployeestatusType }
    function Get_Id: WideString;
    function Get_Code: WideString;
    function Get_Description: WideString;
    function Get_Statusclass: IXMLStatusclassType;
    procedure Set_Id(Value: WideString);
    procedure Set_Code(Value: WideString);
    procedure Set_Description(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLStatusclassType }

  TXMLStatusclassType = class(TXMLNode, IXMLStatusclassType)
  protected
    { IXMLStatusclassType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ Global Functions }

function Getemployeestatuses(Doc: IXMLDocument): IXMLEmployeestatusesType;
function Loademployeestatuses(const FileName: WideString): IXMLEmployeestatusesType;
function Newemployeestatuses: IXMLEmployeestatusesType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getemployeestatuses(Doc: IXMLDocument): IXMLEmployeestatusesType;
begin
  Result := Doc.GetDocBinding('employeestatuses', TXMLEmployeestatusesType, TargetNamespace) as IXMLEmployeestatusesType;
end;

function Loademployeestatuses(const FileName: WideString): IXMLEmployeestatusesType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('employeestatuses', TXMLEmployeestatusesType, TargetNamespace) as IXMLEmployeestatusesType;
end;

function Newemployeestatuses: IXMLEmployeestatusesType;
begin
  Result := NewXMLDocument.GetDocBinding('employeestatuses', TXMLEmployeestatusesType, TargetNamespace) as IXMLEmployeestatusesType;
end;

{ TXMLEmployeestatusesType }

procedure TXMLEmployeestatusesType.AfterConstruction;
begin
  RegisterChildNode('employeestatus', TXMLEmployeestatusType);
  ItemTag := 'employeestatus';
  ItemInterface := IXMLEmployeestatusType;
  inherited;
end;

function TXMLEmployeestatusesType.Get_Morerecords: Integer;
begin
  Result := AttributeNodes['morerecords'].NodeValue;
end;

procedure TXMLEmployeestatusesType.Set_Morerecords(Value: Integer);
begin
  SetAttribute('morerecords', Value);
end;

function TXMLEmployeestatusesType.Get_Employeestatus(Index: Integer): IXMLEmployeestatusType;
begin
  Result := List[Index] as IXMLEmployeestatusType;
end;

function TXMLEmployeestatusesType.Add: IXMLEmployeestatusType;
begin
  Result := AddItem(-1) as IXMLEmployeestatusType;
end;

function TXMLEmployeestatusesType.Insert(const Index: Integer): IXMLEmployeestatusType;
begin
  Result := AddItem(Index) as IXMLEmployeestatusType;
end;

{ TXMLEmployeestatusType }

procedure TXMLEmployeestatusType.AfterConstruction;
begin
  RegisterChildNode('statusclass', TXMLStatusclassType);
  inherited;
end;

function TXMLEmployeestatusType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLEmployeestatusType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLEmployeestatusType.Get_Code: WideString;
begin
  Result := ChildNodes['code'].Text;
end;

procedure TXMLEmployeestatusType.Set_Code(Value: WideString);
begin
  ChildNodes['code'].NodeValue := Value;
end;

function TXMLEmployeestatusType.Get_Description: WideString;
begin
  Result := ChildNodes['description'].Text;
end;

procedure TXMLEmployeestatusType.Set_Description(Value: WideString);
begin
  ChildNodes['description'].NodeValue := Value;
end;

function TXMLEmployeestatusType.Get_Statusclass: IXMLStatusclassType;
begin
  Result := ChildNodes['statusclass'] as IXMLStatusclassType;
end;

{ TXMLStatusclassType }

function TXMLStatusclassType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLStatusclassType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

end.