
{***************************************************************************************}
{                                                                                       }
{                                   XML Data Binding                                    }
{                                                                                       }
{         Generated on: 7/14/2009 3:14:41 PM                                            }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllEmployees\Output.xml   }
{   Settings stored in: E:\job\IS\integration\ADI\xml\RetrieveAllEmployees\Output.xdb   }
{                                                                                       }
{***************************************************************************************}

unit bndRetrieveAllEmployees;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLEmployeesType = interface;
  IXMLEmployeeType = interface;
  IXMLAccrualgroupType = interface;
  IXMLAlternateschedulemodeType = interface;
  IXMLCitycountymaritalstatusType = interface;
  IXMLCitycountytaxmethodType = interface;
  IXMLEmployeestatusType = interface;
  IXMLEmployeetypeType = interface;
  IXMLFamilymemberbirthdateType = interface;
  IXMLFamilymembergenderType = interface;
  IXMLGenderType = interface;
  IXMLHiredateType = interface;
  IXMLIsfulltimestudentType = interface;
  IXMLLocalhiredateType = interface;
  IXMLPaygroupType = interface;
  IXMLPointsgroupType = interface;
  IXMLStateType = interface;
  IXMLRolesType = interface;
  IXMLSchedulemodeType = interface;
  IXMLSenioritydateType = interface;
  IXMLShiftType = interface;
  IXMLStatemaritalstatusType = interface;
  IXMLStatetaxmethodType = interface;
  IXMLFederalmaritalstatusType = interface;
  IXMLFederaltaxmethodType = interface;
  IXMLSupervisorType = interface;
  IXMLVisaexpirationType = interface;
  IXMLLevelType = interface;
  IXMLBirthdateType = interface;
  IXMLCitizenshipType = interface;
  IXMLDateofchangeType = interface;
  IXMLRaceType = interface;
  IXMLReviewdateType = interface;
  IXMLTerminationdateType = interface;

{ IXMLEmployeesType }

  IXMLEmployeesType = interface(IXMLNodeCollection)
    ['{F4F1AAA6-BBC6-445B-AC86-5AD660F7FA5B}']
    { Property Accessors }
    function Get_Employee(Index: Integer): IXMLEmployeeType;
    { Methods & Properties }
    function Add: IXMLEmployeeType;
    function Insert(const Index: Integer): IXMLEmployeeType;
    property Employee[Index: Integer]: IXMLEmployeeType read Get_Employee; default;
  end;

{ IXMLEmployeeType }

  IXMLEmployeeType = interface(IXMLNode)
    ['{7BAF63A6-C675-424C-97EF-39FFBD9DE1D7}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Accrualgroup: IXMLAccrualgroupType;
    function Get_Addressline1: WideString;
    function Get_Addressline2: WideString;
    function Get_Agecertificatenumber: WideString;
    function Get_Alternaterate1: WideString;
    function Get_Alternaterate2: WideString;
    function Get_Alternateschedulemode: IXMLAlternateschedulemodeType;
    function Get_Alternativecontactphone: WideString;
    function Get_City: WideString;
    function Get_Citycountyadditionalwithholdingamount: Integer;
    function Get_Citycountyexemptions: Integer;
    function Get_Citycountymaritalstatus: IXMLCitycountymaritalstatusType;
    function Get_Citycountytaxmethod: IXMLCitycountytaxmethodType;
    function Get_Education: WideString;
    function Get_Emailaddress: WideString;
    function Get_Emergencycontactname: WideString;
    function Get_Employeeidnumber: WideString;
    function Get_Employeestatus: IXMLEmployeestatusType;
    function Get_Statusclass: Integer;
    function Get_Employeetype: IXMLEmployeetypeType;
    function Get_Familymemberbirthdate: IXMLFamilymemberbirthdateType;
    function Get_Familymembergender: IXMLFamilymembergenderType;
    function Get_Familymembername: WideString;
    function Get_Familymemberrelation: WideString;
    function Get_Familymemberssn: WideString;
    function Get_Firstname: WideString;
    function Get_Formername: WideString;
    function Get_Gender: IXMLGenderType;
    function Get_Hiredate: IXMLHiredateType;
    function Get_Initials: WideString;
    function Get_Isautopunchtoschedule: Integer;
    function Get_Isfulltimestudent: IXMLIsfulltimestudentType;
    function Get_Ismarried: Integer;
    function Get_Isnopay: Integer;
    function Get_Isoverrideholidayhours: Integer;
    function Get_Ispaidholidays: Integer;
    function Get_Isrehireable: Integer;
    function Get_Issalaried: Integer;
    function Get_Isunionmember: Integer;
    function Get_Iswebenabled: Integer;
    function Get_Lastname: WideString;
    function Get_Localhiredate: IXMLLocalhiredateType;
    function Get_Loginid: WideString;
    function Get_Lunchbreakminutes: Integer;
    function Get_Middleinitial: WideString;
    function Get_Overriddenholidayhours: Integer;
    function Get_Paygroup: IXMLPaygroupType;
    function Get_Phone: WideString;
    function Get_Phonetype: WideString;
    function Get_Pointsgroup: IXMLPointsgroupType;
    function Get_Primarylanguage: WideString;
    function Get_Primaryrate: WideString;
    function Get_State: IXMLStateType;
    function Get_Roles: IXMLRolesType;
    function Get_Salary: WideString;
    function Get_Scheduledworkhoursperday: Integer;
    function Get_Schedulemode: IXMLSchedulemodeType;
    function Get_Secondarylanguage: WideString;
    function Get_Senioritydate: IXMLSenioritydateType;
    function Get_Shift: IXMLShiftType;
    function Get_Shortname: WideString;
    function Get_Ssn: WideString;
    function Get_Stateadditionalwithholdingamount: Integer;
    function Get_Stateexemptions: Integer;
    function Get_Statemaritalstatus: IXMLStatemaritalstatusType;
    function Get_Statetaxmethod: IXMLStatetaxmethodType;
    function Get_Federaladditionalwithholdingamount: Integer;
    function Get_Federalexemptions: Integer;
    function Get_Federalmaritalstatus: IXMLFederalmaritalstatusType;
    function Get_Federaltaxmethod: IXMLFederaltaxmethodType;
    function Get_Supervisor: IXMLSupervisorType;
    function Get_Timeclocknumber: string;
    function Get_Unionlocal: WideString;
    function Get_Unionname: WideString;
    function Get_Visaexpiration: IXMLVisaexpirationType;
    function Get_Visanumber: WideString;
    function Get_Visatype: WideString;
    function Get_Workpermitnumber: WideString;
    function Get_Zipcode: string;
    function Get_Notificationlevel: Integer;
    function Get_Hierarchydetails: WideString;
    function Get_Level: IXMLLevelType;
    function Get_Company: WideString;
    function Get_Location: WideString;
    function Get_Division: WideString;
    function Get_Department: WideString;
    function Get_Position: WideString;
    function Get_Emergencycontactaddresscity: WideString;
    function Get_Emergencycontactaddresscountry: WideString;
    function Get_Emergencycontactaddressline1: WideString;
    function Get_Emergencycontactaddressline2: WideString;
    function Get_Emergencycontactaddressphone: Integer;
    function Get_Emergencycontactaddresszipcode: WideString;
    function Get_Birthdate: IXMLBirthdateType;
    function Get_Citizenship: IXMLCitizenshipType;
    function Get_Dateofchange: IXMLDateofchangeType;
    function Get_Race: IXMLRaceType;
    function Get_Reviewdate: IXMLReviewdateType;
    function Get_Terminationdate: IXMLTerminationdateType;
    procedure Set_Id(Value: WideString);
    procedure Set_Addressline1(Value: WideString);
    procedure Set_Addressline2(Value: WideString);
    procedure Set_Agecertificatenumber(Value: WideString);
    procedure Set_Alternaterate1(Value: WideString);
    procedure Set_Alternaterate2(Value: WideString);
    procedure Set_Alternativecontactphone(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_Citycountyadditionalwithholdingamount(Value: Integer);
    procedure Set_Citycountyexemptions(Value: Integer);
    procedure Set_Education(Value: WideString);
    procedure Set_Emailaddress(Value: WideString);
    procedure Set_Emergencycontactname(Value: WideString);
    procedure Set_Employeeidnumber(Value: WideString);
    procedure Set_Statusclass(Value: Integer);
    procedure Set_Familymembername(Value: WideString);
    procedure Set_Familymemberrelation(Value: WideString);
    procedure Set_Familymemberssn(Value: WideString);
    procedure Set_Firstname(Value: WideString);
    procedure Set_Formername(Value: WideString);
    procedure Set_Initials(Value: WideString);
    procedure Set_Isautopunchtoschedule(Value: Integer);
    procedure Set_Ismarried(Value: Integer);
    procedure Set_Isnopay(Value: Integer);
    procedure Set_Isoverrideholidayhours(Value: Integer);
    procedure Set_Ispaidholidays(Value: Integer);
    procedure Set_Isrehireable(Value: Integer);
    procedure Set_Issalaried(Value: Integer);
    procedure Set_Isunionmember(Value: Integer);
    procedure Set_Iswebenabled(Value: Integer);
    procedure Set_Lastname(Value: WideString);
    procedure Set_Loginid(Value: WideString);
    procedure Set_Lunchbreakminutes(Value: Integer);
    procedure Set_Middleinitial(Value: WideString);
    procedure Set_Overriddenholidayhours(Value: Integer);
    procedure Set_Phone(Value: WideString);
    procedure Set_Phonetype(Value: WideString);
    procedure Set_Primarylanguage(Value: WideString);
    procedure Set_Primaryrate(Value: WideString);
    procedure Set_Salary(Value: WideString);
    procedure Set_Scheduledworkhoursperday(Value: Integer);
    procedure Set_Secondarylanguage(Value: WideString);
    procedure Set_Shortname(Value: WideString);
    procedure Set_Ssn(Value: WideString);
    procedure Set_Stateadditionalwithholdingamount(Value: Integer);
    procedure Set_Stateexemptions(Value: Integer);
    procedure Set_Federaladditionalwithholdingamount(Value: Integer);
    procedure Set_Federalexemptions(Value: Integer);
    procedure Set_Timeclocknumber(Value: string);
    procedure Set_Unionlocal(Value: WideString);
    procedure Set_Unionname(Value: WideString);
    procedure Set_Visanumber(Value: WideString);
    procedure Set_Visatype(Value: WideString);
    procedure Set_Workpermitnumber(Value: WideString);
    procedure Set_Zipcode(Value: string);
    procedure Set_Notificationlevel(Value: Integer);
    procedure Set_Hierarchydetails(Value: WideString);
    procedure Set_Company(Value: WideString);
    procedure Set_Location(Value: WideString);
    procedure Set_Division(Value: WideString);
    procedure Set_Department(Value: WideString);
    procedure Set_Position(Value: WideString);
    procedure Set_Emergencycontactaddresscity(Value: WideString);
    procedure Set_Emergencycontactaddresscountry(Value: WideString);
    procedure Set_Emergencycontactaddressline1(Value: WideString);
    procedure Set_Emergencycontactaddressline2(Value: WideString);
    procedure Set_Emergencycontactaddressphone(Value: Integer);
    procedure Set_Emergencycontactaddresszipcode(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Accrualgroup: IXMLAccrualgroupType read Get_Accrualgroup;
    property Addressline1: WideString read Get_Addressline1 write Set_Addressline1;
    property Addressline2: WideString read Get_Addressline2 write Set_Addressline2;
    property Agecertificatenumber: WideString read Get_Agecertificatenumber write Set_Agecertificatenumber;
    property Alternaterate1: WideString read Get_Alternaterate1 write Set_Alternaterate1;
    property Alternaterate2: WideString read Get_Alternaterate2 write Set_Alternaterate2;
    property Alternateschedulemode: IXMLAlternateschedulemodeType read Get_Alternateschedulemode;
    property Alternativecontactphone: WideString read Get_Alternativecontactphone write Set_Alternativecontactphone;
    property City: WideString read Get_City write Set_City;
    property Citycountyadditionalwithholdingamount: Integer read Get_Citycountyadditionalwithholdingamount write Set_Citycountyadditionalwithholdingamount;
    property Citycountyexemptions: Integer read Get_Citycountyexemptions write Set_Citycountyexemptions;
    property Citycountymaritalstatus: IXMLCitycountymaritalstatusType read Get_Citycountymaritalstatus;
    property Citycountytaxmethod: IXMLCitycountytaxmethodType read Get_Citycountytaxmethod;
    property Education: WideString read Get_Education write Set_Education;
    property Emailaddress: WideString read Get_Emailaddress write Set_Emailaddress;
    property Emergencycontactname: WideString read Get_Emergencycontactname write Set_Emergencycontactname;
    property Employeeidnumber: WideString read Get_Employeeidnumber write Set_Employeeidnumber;
    property Employeestatus: IXMLEmployeestatusType read Get_Employeestatus;
    property Statusclass: Integer read Get_Statusclass write Set_Statusclass;
    property Employeetype: IXMLEmployeetypeType read Get_Employeetype;
    property Familymemberbirthdate: IXMLFamilymemberbirthdateType read Get_Familymemberbirthdate;
    property Familymembergender: IXMLFamilymembergenderType read Get_Familymembergender;
    property Familymembername: WideString read Get_Familymembername write Set_Familymembername;
    property Familymemberrelation: WideString read Get_Familymemberrelation write Set_Familymemberrelation;
    property Familymemberssn: WideString read Get_Familymemberssn write Set_Familymemberssn;
    property Firstname: WideString read Get_Firstname write Set_Firstname;
    property Formername: WideString read Get_Formername write Set_Formername;
    property Gender: IXMLGenderType read Get_Gender;
    property Hiredate: IXMLHiredateType read Get_Hiredate;
    property Initials: WideString read Get_Initials write Set_Initials;
    property Isautopunchtoschedule: Integer read Get_Isautopunchtoschedule write Set_Isautopunchtoschedule;
    property Isfulltimestudent: IXMLIsfulltimestudentType read Get_Isfulltimestudent;
    property Ismarried: Integer read Get_Ismarried write Set_Ismarried;
    property Isnopay: Integer read Get_Isnopay write Set_Isnopay;
    property Isoverrideholidayhours: Integer read Get_Isoverrideholidayhours write Set_Isoverrideholidayhours;
    property Ispaidholidays: Integer read Get_Ispaidholidays write Set_Ispaidholidays;
    property Isrehireable: Integer read Get_Isrehireable write Set_Isrehireable;
    property Issalaried: Integer read Get_Issalaried write Set_Issalaried;
    property Isunionmember: Integer read Get_Isunionmember write Set_Isunionmember;
    property Iswebenabled: Integer read Get_Iswebenabled write Set_Iswebenabled;
    property Lastname: WideString read Get_Lastname write Set_Lastname;
    property Localhiredate: IXMLLocalhiredateType read Get_Localhiredate;
    property Loginid: WideString read Get_Loginid write Set_Loginid;
    property Lunchbreakminutes: Integer read Get_Lunchbreakminutes write Set_Lunchbreakminutes;
    property Middleinitial: WideString read Get_Middleinitial write Set_Middleinitial;
    property Overriddenholidayhours: Integer read Get_Overriddenholidayhours write Set_Overriddenholidayhours;
    property Paygroup: IXMLPaygroupType read Get_Paygroup;
    property Phone: WideString read Get_Phone write Set_Phone;
    property Phonetype: WideString read Get_Phonetype write Set_Phonetype;
    property Pointsgroup: IXMLPointsgroupType read Get_Pointsgroup;
    property Primarylanguage: WideString read Get_Primarylanguage write Set_Primarylanguage;
    property Primaryrate: WideString read Get_Primaryrate write Set_Primaryrate;
    property State: IXMLStateType read Get_State;
    property Roles: IXMLRolesType read Get_Roles;
    property Salary: WideString read Get_Salary write Set_Salary;
    property Scheduledworkhoursperday: Integer read Get_Scheduledworkhoursperday write Set_Scheduledworkhoursperday;
    property Schedulemode: IXMLSchedulemodeType read Get_Schedulemode;
    property Secondarylanguage: WideString read Get_Secondarylanguage write Set_Secondarylanguage;
    property Senioritydate: IXMLSenioritydateType read Get_Senioritydate;
    property Shift: IXMLShiftType read Get_Shift;
    property Shortname: WideString read Get_Shortname write Set_Shortname;
    property Ssn: WideString read Get_Ssn write Set_Ssn;
    property Stateadditionalwithholdingamount: Integer read Get_Stateadditionalwithholdingamount write Set_Stateadditionalwithholdingamount;
    property Stateexemptions: Integer read Get_Stateexemptions write Set_Stateexemptions;
    property Statemaritalstatus: IXMLStatemaritalstatusType read Get_Statemaritalstatus;
    property Statetaxmethod: IXMLStatetaxmethodType read Get_Statetaxmethod;
    property Federaladditionalwithholdingamount: Integer read Get_Federaladditionalwithholdingamount write Set_Federaladditionalwithholdingamount;
    property Federalexemptions: Integer read Get_Federalexemptions write Set_Federalexemptions;
    property Federalmaritalstatus: IXMLFederalmaritalstatusType read Get_Federalmaritalstatus;
    property Federaltaxmethod: IXMLFederaltaxmethodType read Get_Federaltaxmethod;
    property Supervisor: IXMLSupervisorType read Get_Supervisor;
    property Timeclocknumber: string read Get_Timeclocknumber write Set_Timeclocknumber;
    property Unionlocal: WideString read Get_Unionlocal write Set_Unionlocal;
    property Unionname: WideString read Get_Unionname write Set_Unionname;
    property Visaexpiration: IXMLVisaexpirationType read Get_Visaexpiration;
    property Visanumber: WideString read Get_Visanumber write Set_Visanumber;
    property Visatype: WideString read Get_Visatype write Set_Visatype;
    property Workpermitnumber: WideString read Get_Workpermitnumber write Set_Workpermitnumber;
    property Zipcode: string read Get_Zipcode write Set_Zipcode;
    property Notificationlevel: Integer read Get_Notificationlevel write Set_Notificationlevel;
    property Hierarchydetails: WideString read Get_Hierarchydetails write Set_Hierarchydetails;
    property Level: IXMLLevelType read Get_Level;
    property Company: WideString read Get_Company write Set_Company;
    property Location: WideString read Get_Location write Set_Location;
    property Division: WideString read Get_Division write Set_Division;
    property Department: WideString read Get_Department write Set_Department;
    property Position: WideString read Get_Position write Set_Position;
    property Emergencycontactaddresscity: WideString read Get_Emergencycontactaddresscity write Set_Emergencycontactaddresscity;
    property Emergencycontactaddresscountry: WideString read Get_Emergencycontactaddresscountry write Set_Emergencycontactaddresscountry;
    property Emergencycontactaddressline1: WideString read Get_Emergencycontactaddressline1 write Set_Emergencycontactaddressline1;
    property Emergencycontactaddressline2: WideString read Get_Emergencycontactaddressline2 write Set_Emergencycontactaddressline2;
    property Emergencycontactaddressphone: Integer read Get_Emergencycontactaddressphone write Set_Emergencycontactaddressphone;
    property Emergencycontactaddresszipcode: WideString read Get_Emergencycontactaddresszipcode write Set_Emergencycontactaddresszipcode;
    property Birthdate: IXMLBirthdateType read Get_Birthdate;
    property Citizenship: IXMLCitizenshipType read Get_Citizenship;
    property Dateofchange: IXMLDateofchangeType read Get_Dateofchange;
    property Race: IXMLRaceType read Get_Race;
    property Reviewdate: IXMLReviewdateType read Get_Reviewdate;
    property Terminationdate: IXMLTerminationdateType read Get_Terminationdate;
  end;

{ IXMLAccrualgroupType }

  IXMLAccrualgroupType = interface(IXMLNode)
    ['{A96426EA-6AA3-46DF-A288-9BD06BF8F2FE}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAlternateschedulemodeType }

  IXMLAlternateschedulemodeType = interface(IXMLNode)
    ['{7EA3A9B1-F96A-462D-92A1-C526D41FA1F3}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCitycountymaritalstatusType }

  IXMLCitycountymaritalstatusType = interface(IXMLNode)
    ['{8E1D24B8-8ADF-4E88-9CC8-F9EB72F64E6F}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCitycountytaxmethodType }

  IXMLCitycountytaxmethodType = interface(IXMLNode)
    ['{14F0CEAB-849F-46DE-8263-4A4E8235B27F}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLEmployeestatusType }

  IXMLEmployeestatusType = interface(IXMLNode)
    ['{21AE6276-08C5-45B5-BC83-A792A04BB014}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLEmployeetypeType }

  IXMLEmployeetypeType = interface(IXMLNode)
    ['{A3E85301-CBAB-4050-90D2-DA4CBE625109}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLFamilymemberbirthdateType }

  IXMLFamilymemberbirthdateType = interface(IXMLNode)
    ['{BAC2DB40-2676-4BB7-909F-E6ABF631407C}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLFamilymembergenderType }

  IXMLFamilymembergenderType = interface(IXMLNode)
    ['{6A633E4E-B7E7-4F77-86BE-833155219822}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLGenderType }

  IXMLGenderType = interface(IXMLNode)
    ['{B7DF600A-36CE-452A-A947-3161FB97A057}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLHiredateType }

  IXMLHiredateType = interface(IXMLNode)
    ['{5543E8B3-0102-48E9-A2B1-0C900DFF60A8}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLIsfulltimestudentType }

  IXMLIsfulltimestudentType = interface(IXMLNode)
    ['{799737B2-EED6-4455-BA43-01C8ABBBB7A9}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLLocalhiredateType }

  IXMLLocalhiredateType = interface(IXMLNode)
    ['{33C28242-0CD2-4955-A34F-75BB9F2CC6F3}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLPaygroupType }

  IXMLPaygroupType = interface(IXMLNode)
    ['{6CBACD0D-570D-41F9-8289-9464C32CB0DD}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPointsgroupType }

  IXMLPointsgroupType = interface(IXMLNode)
    ['{4A6B303A-B93F-42FF-AFC0-D755601EFD62}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLStateType }

  IXMLStateType = interface(IXMLNode)
    ['{8D534595-7662-45A6-99FA-10B6DDEAFFAA}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLRolesType }

  IXMLRolesType = interface(IXMLNode)
    ['{4595DBD0-FD90-4B7C-9475-B35ECC2CC46B}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLSchedulemodeType }

  IXMLSchedulemodeType = interface(IXMLNode)
    ['{0D31612A-1185-4C91-97EE-B1CB27107D02}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLSenioritydateType }

  IXMLSenioritydateType = interface(IXMLNode)
    ['{E62ABD33-B575-499A-8436-7AA0AB12B8DE}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLShiftType }

  IXMLShiftType = interface(IXMLNode)
    ['{ACF29EA9-698B-4771-86D3-5EF602706137}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLStatemaritalstatusType }

  IXMLStatemaritalstatusType = interface(IXMLNode)
    ['{EBA57D9D-6320-4C77-BF74-31F14C353FC1}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLStatetaxmethodType }

  IXMLStatetaxmethodType = interface(IXMLNode)
    ['{25963CE3-1646-4053-9911-0BE91A41F8BA}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLFederalmaritalstatusType }

  IXMLFederalmaritalstatusType = interface(IXMLNode)
    ['{A7E3C0C9-0E8E-4D2E-9121-89527A745F25}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLFederaltaxmethodType }

  IXMLFederaltaxmethodType = interface(IXMLNode)
    ['{7C91B661-FF24-44EA-9D82-430D2FCA9F36}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLSupervisorType }

  IXMLSupervisorType = interface(IXMLNode)
    ['{18B8F6C4-5309-47D6-9AE6-311FE320C387}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLVisaexpirationType }

  IXMLVisaexpirationType = interface(IXMLNode)
    ['{F4704C03-D922-48EF-8CC3-E4B480D4F96E}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLLevelType }

  IXMLLevelType = interface(IXMLNode)
    ['{4B903490-65B2-4D2C-9140-E0C86F1218C7}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLBirthdateType }

  IXMLBirthdateType = interface(IXMLNode)
    ['{C52A4DE9-5F53-4836-A3B0-E666B3860B44}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLCitizenshipType }

  IXMLCitizenshipType = interface(IXMLNode)
    ['{06F97940-827C-410A-982F-E39726D0F2BE}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLDateofchangeType }

  IXMLDateofchangeType = interface(IXMLNode)
    ['{CC546ED0-19A3-48F3-93AC-40731F409E32}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLRaceType }

  IXMLRaceType = interface(IXMLNode)
    ['{5240E4D4-750B-4779-B986-F476E86B1232}']
    { Property Accessors }
    function Get_Name: Integer;
    procedure Set_Name(Value: Integer);
    { Methods & Properties }
    property Name: Integer read Get_Name write Set_Name;
  end;

{ IXMLReviewdateType }

  IXMLReviewdateType = interface(IXMLNode)
    ['{3000901F-CE46-4759-8B88-D6FE0A4BF875}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLTerminationdateType }

  IXMLTerminationdateType = interface(IXMLNode)
    ['{1BBEF51A-12DF-4727-9AFC-A76DD1EA574D}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ Forward Decls }

  TXMLEmployeesType = class;
  TXMLEmployeeType = class;
  TXMLAccrualgroupType = class;
  TXMLAlternateschedulemodeType = class;
  TXMLCitycountymaritalstatusType = class;
  TXMLCitycountytaxmethodType = class;
  TXMLEmployeestatusType = class;
  TXMLEmployeetypeType = class;
  TXMLFamilymemberbirthdateType = class;
  TXMLFamilymembergenderType = class;
  TXMLGenderType = class;
  TXMLHiredateType = class;
  TXMLIsfulltimestudentType = class;
  TXMLLocalhiredateType = class;
  TXMLPaygroupType = class;
  TXMLPointsgroupType = class;
  TXMLStateType = class;
  TXMLRolesType = class;
  TXMLSchedulemodeType = class;
  TXMLSenioritydateType = class;
  TXMLShiftType = class;
  TXMLStatemaritalstatusType = class;
  TXMLStatetaxmethodType = class;
  TXMLFederalmaritalstatusType = class;
  TXMLFederaltaxmethodType = class;
  TXMLSupervisorType = class;
  TXMLVisaexpirationType = class;
  TXMLLevelType = class;
  TXMLBirthdateType = class;
  TXMLCitizenshipType = class;
  TXMLDateofchangeType = class;
  TXMLRaceType = class;
  TXMLReviewdateType = class;
  TXMLTerminationdateType = class;

{ TXMLEmployeesType }

  TXMLEmployeesType = class(TXMLNodeCollection, IXMLEmployeesType)
  protected
    { IXMLEmployeesType }
    function Get_Employee(Index: Integer): IXMLEmployeeType;
    function Add: IXMLEmployeeType;
    function Insert(const Index: Integer): IXMLEmployeeType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmployeeType }

  TXMLEmployeeType = class(TXMLNode, IXMLEmployeeType)
  protected
    { IXMLEmployeeType }
    function Get_Id: WideString;
    function Get_Accrualgroup: IXMLAccrualgroupType;
    function Get_Addressline1: WideString;
    function Get_Addressline2: WideString;
    function Get_Agecertificatenumber: WideString;
    function Get_Alternaterate1: WideString;
    function Get_Alternaterate2: WideString;
    function Get_Alternateschedulemode: IXMLAlternateschedulemodeType;
    function Get_Alternativecontactphone: WideString;
    function Get_City: WideString;
    function Get_Citycountyadditionalwithholdingamount: Integer;
    function Get_Citycountyexemptions: Integer;
    function Get_Citycountymaritalstatus: IXMLCitycountymaritalstatusType;
    function Get_Citycountytaxmethod: IXMLCitycountytaxmethodType;
    function Get_Education: WideString;
    function Get_Emailaddress: WideString;
    function Get_Emergencycontactname: WideString;
    function Get_Employeeidnumber: WideString;
    function Get_Employeestatus: IXMLEmployeestatusType;
    function Get_Statusclass: Integer;
    function Get_Employeetype: IXMLEmployeetypeType;
    function Get_Familymemberbirthdate: IXMLFamilymemberbirthdateType;
    function Get_Familymembergender: IXMLFamilymembergenderType;
    function Get_Familymembername: WideString;
    function Get_Familymemberrelation: WideString;
    function Get_Familymemberssn: WideString;
    function Get_Firstname: WideString;
    function Get_Formername: WideString;
    function Get_Gender: IXMLGenderType;
    function Get_Hiredate: IXMLHiredateType;
    function Get_Initials: WideString;
    function Get_Isautopunchtoschedule: Integer;
    function Get_Isfulltimestudent: IXMLIsfulltimestudentType;
    function Get_Ismarried: Integer;
    function Get_Isnopay: Integer;
    function Get_Isoverrideholidayhours: Integer;
    function Get_Ispaidholidays: Integer;
    function Get_Isrehireable: Integer;
    function Get_Issalaried: Integer;
    function Get_Isunionmember: Integer;
    function Get_Iswebenabled: Integer;
    function Get_Lastname: WideString;
    function Get_Localhiredate: IXMLLocalhiredateType;
    function Get_Loginid: WideString;
    function Get_Lunchbreakminutes: Integer;
    function Get_Middleinitial: WideString;
    function Get_Overriddenholidayhours: Integer;
    function Get_Paygroup: IXMLPaygroupType;
    function Get_Phone: WideString;
    function Get_Phonetype: WideString;
    function Get_Pointsgroup: IXMLPointsgroupType;
    function Get_Primarylanguage: WideString;
    function Get_Primaryrate: WideString;
    function Get_State: IXMLStateType;
    function Get_Roles: IXMLRolesType;
    function Get_Salary: WideString;
    function Get_Scheduledworkhoursperday: Integer;
    function Get_Schedulemode: IXMLSchedulemodeType;
    function Get_Secondarylanguage: WideString;
    function Get_Senioritydate: IXMLSenioritydateType;
    function Get_Shift: IXMLShiftType;
    function Get_Shortname: WideString;
    function Get_Ssn: WideString;
    function Get_Stateadditionalwithholdingamount: Integer;
    function Get_Stateexemptions: Integer;
    function Get_Statemaritalstatus: IXMLStatemaritalstatusType;
    function Get_Statetaxmethod: IXMLStatetaxmethodType;
    function Get_Federaladditionalwithholdingamount: Integer;
    function Get_Federalexemptions: Integer;
    function Get_Federalmaritalstatus: IXMLFederalmaritalstatusType;
    function Get_Federaltaxmethod: IXMLFederaltaxmethodType;
    function Get_Supervisor: IXMLSupervisorType;
    function Get_Timeclocknumber: string;
    function Get_Unionlocal: WideString;
    function Get_Unionname: WideString;
    function Get_Visaexpiration: IXMLVisaexpirationType;
    function Get_Visanumber: WideString;
    function Get_Visatype: WideString;
    function Get_Workpermitnumber: WideString;
    function Get_Zipcode: string;
    function Get_Notificationlevel: Integer;
    function Get_Hierarchydetails: WideString;
    function Get_Level: IXMLLevelType;
    function Get_Company: WideString;
    function Get_Location: WideString;
    function Get_Division: WideString;
    function Get_Department: WideString;
    function Get_Position: WideString;
    function Get_Emergencycontactaddresscity: WideString;
    function Get_Emergencycontactaddresscountry: WideString;
    function Get_Emergencycontactaddressline1: WideString;
    function Get_Emergencycontactaddressline2: WideString;
    function Get_Emergencycontactaddressphone: Integer;
    function Get_Emergencycontactaddresszipcode: WideString;
    function Get_Birthdate: IXMLBirthdateType;
    function Get_Citizenship: IXMLCitizenshipType;
    function Get_Dateofchange: IXMLDateofchangeType;
    function Get_Race: IXMLRaceType;
    function Get_Reviewdate: IXMLReviewdateType;
    function Get_Terminationdate: IXMLTerminationdateType;
    procedure Set_Id(Value: WideString);
    procedure Set_Addressline1(Value: WideString);
    procedure Set_Addressline2(Value: WideString);
    procedure Set_Agecertificatenumber(Value: WideString);
    procedure Set_Alternaterate1(Value: WideString);
    procedure Set_Alternaterate2(Value: WideString);
    procedure Set_Alternativecontactphone(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_Citycountyadditionalwithholdingamount(Value: Integer);
    procedure Set_Citycountyexemptions(Value: Integer);
    procedure Set_Education(Value: WideString);
    procedure Set_Emailaddress(Value: WideString);
    procedure Set_Emergencycontactname(Value: WideString);
    procedure Set_Employeeidnumber(Value: WideString);
    procedure Set_Statusclass(Value: Integer);
    procedure Set_Familymembername(Value: WideString);
    procedure Set_Familymemberrelation(Value: WideString);
    procedure Set_Familymemberssn(Value: WideString);
    procedure Set_Firstname(Value: WideString);
    procedure Set_Formername(Value: WideString);
    procedure Set_Initials(Value: WideString);
    procedure Set_Isautopunchtoschedule(Value: Integer);
    procedure Set_Ismarried(Value: Integer);
    procedure Set_Isnopay(Value: Integer);
    procedure Set_Isoverrideholidayhours(Value: Integer);
    procedure Set_Ispaidholidays(Value: Integer);
    procedure Set_Isrehireable(Value: Integer);
    procedure Set_Issalaried(Value: Integer);
    procedure Set_Isunionmember(Value: Integer);
    procedure Set_Iswebenabled(Value: Integer);
    procedure Set_Lastname(Value: WideString);
    procedure Set_Loginid(Value: WideString);
    procedure Set_Lunchbreakminutes(Value: Integer);
    procedure Set_Middleinitial(Value: WideString);
    procedure Set_Overriddenholidayhours(Value: Integer);
    procedure Set_Phone(Value: WideString);
    procedure Set_Phonetype(Value: WideString);
    procedure Set_Primarylanguage(Value: WideString);
    procedure Set_Primaryrate(Value: WideString);
    procedure Set_Salary(Value: WideString);
    procedure Set_Scheduledworkhoursperday(Value: Integer);
    procedure Set_Secondarylanguage(Value: WideString);
    procedure Set_Shortname(Value: WideString);
    procedure Set_Ssn(Value: WideString);
    procedure Set_Stateadditionalwithholdingamount(Value: Integer);
    procedure Set_Stateexemptions(Value: Integer);
    procedure Set_Federaladditionalwithholdingamount(Value: Integer);
    procedure Set_Federalexemptions(Value: Integer);
    procedure Set_Timeclocknumber(Value: string);
    procedure Set_Unionlocal(Value: WideString);
    procedure Set_Unionname(Value: WideString);
    procedure Set_Visanumber(Value: WideString);
    procedure Set_Visatype(Value: WideString);
    procedure Set_Workpermitnumber(Value: WideString);
    procedure Set_Zipcode(Value: string);
    procedure Set_Notificationlevel(Value: Integer);
    procedure Set_Hierarchydetails(Value: WideString);
    procedure Set_Company(Value: WideString);
    procedure Set_Location(Value: WideString);
    procedure Set_Division(Value: WideString);
    procedure Set_Department(Value: WideString);
    procedure Set_Position(Value: WideString);
    procedure Set_Emergencycontactaddresscity(Value: WideString);
    procedure Set_Emergencycontactaddresscountry(Value: WideString);
    procedure Set_Emergencycontactaddressline1(Value: WideString);
    procedure Set_Emergencycontactaddressline2(Value: WideString);
    procedure Set_Emergencycontactaddressphone(Value: Integer);
    procedure Set_Emergencycontactaddresszipcode(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAccrualgroupType }

  TXMLAccrualgroupType = class(TXMLNode, IXMLAccrualgroupType)
  protected
    { IXMLAccrualgroupType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAlternateschedulemodeType }

  TXMLAlternateschedulemodeType = class(TXMLNode, IXMLAlternateschedulemodeType)
  protected
    { IXMLAlternateschedulemodeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCitycountymaritalstatusType }

  TXMLCitycountymaritalstatusType = class(TXMLNode, IXMLCitycountymaritalstatusType)
  protected
    { IXMLCitycountymaritalstatusType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCitycountytaxmethodType }

  TXMLCitycountytaxmethodType = class(TXMLNode, IXMLCitycountytaxmethodType)
  protected
    { IXMLCitycountytaxmethodType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLEmployeestatusType }

  TXMLEmployeestatusType = class(TXMLNode, IXMLEmployeestatusType)
  protected
    { IXMLEmployeestatusType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLEmployeetypeType }

  TXMLEmployeetypeType = class(TXMLNode, IXMLEmployeetypeType)
  protected
    { IXMLEmployeetypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLFamilymemberbirthdateType }

  TXMLFamilymemberbirthdateType = class(TXMLNode, IXMLFamilymemberbirthdateType)
  protected
    { IXMLFamilymemberbirthdateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLFamilymembergenderType }

  TXMLFamilymembergenderType = class(TXMLNode, IXMLFamilymembergenderType)
  protected
    { IXMLFamilymembergenderType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLGenderType }

  TXMLGenderType = class(TXMLNode, IXMLGenderType)
  protected
    { IXMLGenderType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLHiredateType }

  TXMLHiredateType = class(TXMLNode, IXMLHiredateType)
  protected
    { IXMLHiredateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLIsfulltimestudentType }

  TXMLIsfulltimestudentType = class(TXMLNode, IXMLIsfulltimestudentType)
  protected
    { IXMLIsfulltimestudentType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLLocalhiredateType }

  TXMLLocalhiredateType = class(TXMLNode, IXMLLocalhiredateType)
  protected
    { IXMLLocalhiredateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLPaygroupType }

  TXMLPaygroupType = class(TXMLNode, IXMLPaygroupType)
  protected
    { IXMLPaygroupType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPointsgroupType }

  TXMLPointsgroupType = class(TXMLNode, IXMLPointsgroupType)
  protected
    { IXMLPointsgroupType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLStateType }

  TXMLStateType = class(TXMLNode, IXMLStateType)
  protected
    { IXMLStateType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLRolesType }

  TXMLRolesType = class(TXMLNode, IXMLRolesType)
  protected
    { IXMLRolesType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLSchedulemodeType }

  TXMLSchedulemodeType = class(TXMLNode, IXMLSchedulemodeType)
  protected
    { IXMLSchedulemodeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLSenioritydateType }

  TXMLSenioritydateType = class(TXMLNode, IXMLSenioritydateType)
  protected
    { IXMLSenioritydateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLShiftType }

  TXMLShiftType = class(TXMLNode, IXMLShiftType)
  protected
    { IXMLShiftType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLStatemaritalstatusType }

  TXMLStatemaritalstatusType = class(TXMLNode, IXMLStatemaritalstatusType)
  protected
    { IXMLStatemaritalstatusType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLStatetaxmethodType }

  TXMLStatetaxmethodType = class(TXMLNode, IXMLStatetaxmethodType)
  protected
    { IXMLStatetaxmethodType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLFederalmaritalstatusType }

  TXMLFederalmaritalstatusType = class(TXMLNode, IXMLFederalmaritalstatusType)
  protected
    { IXMLFederalmaritalstatusType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLFederaltaxmethodType }

  TXMLFederaltaxmethodType = class(TXMLNode, IXMLFederaltaxmethodType)
  protected
    { IXMLFederaltaxmethodType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLSupervisorType }

  TXMLSupervisorType = class(TXMLNode, IXMLSupervisorType)
  protected
    { IXMLSupervisorType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLVisaexpirationType }

  TXMLVisaexpirationType = class(TXMLNode, IXMLVisaexpirationType)
  protected
    { IXMLVisaexpirationType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLLevelType }

  TXMLLevelType = class(TXMLNode, IXMLLevelType)
  protected
    { IXMLLevelType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLBirthdateType }

  TXMLBirthdateType = class(TXMLNode, IXMLBirthdateType)
  protected
    { IXMLBirthdateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLCitizenshipType }

  TXMLCitizenshipType = class(TXMLNode, IXMLCitizenshipType)
  protected
    { IXMLCitizenshipType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLDateofchangeType }

  TXMLDateofchangeType = class(TXMLNode, IXMLDateofchangeType)
  protected
    { IXMLDateofchangeType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLRaceType }

  TXMLRaceType = class(TXMLNode, IXMLRaceType)
  protected
    { IXMLRaceType }
    function Get_Name: Integer;
    procedure Set_Name(Value: Integer);
  end;

{ TXMLReviewdateType }

  TXMLReviewdateType = class(TXMLNode, IXMLReviewdateType)
  protected
    { IXMLReviewdateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLTerminationdateType }

  TXMLTerminationdateType = class(TXMLNode, IXMLTerminationdateType)
  protected
    { IXMLTerminationdateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ Global Functions }

function Getemployees(Doc: IXMLDocument): IXMLEmployeesType;
function Loademployees(const FileName: WideString): IXMLEmployeesType;
function Newemployees: IXMLEmployeesType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getemployees(Doc: IXMLDocument): IXMLEmployeesType;
begin
  Result := Doc.GetDocBinding('employees', TXMLEmployeesType, TargetNamespace) as IXMLEmployeesType;
end;

function Loademployees(const FileName: WideString): IXMLEmployeesType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('employees', TXMLEmployeesType, TargetNamespace) as IXMLEmployeesType;
end;

function Newemployees: IXMLEmployeesType;
begin
  Result := NewXMLDocument.GetDocBinding('employees', TXMLEmployeesType, TargetNamespace) as IXMLEmployeesType;
end;

{ TXMLEmployeesType }

procedure TXMLEmployeesType.AfterConstruction;
begin
  RegisterChildNode('employee', TXMLEmployeeType);
  ItemTag := 'employee';
  ItemInterface := IXMLEmployeeType;
  inherited;
end;

function TXMLEmployeesType.Get_Employee(Index: Integer): IXMLEmployeeType;
begin
  Result := List[Index] as IXMLEmployeeType;
end;

function TXMLEmployeesType.Add: IXMLEmployeeType;
begin
  Result := AddItem(-1) as IXMLEmployeeType;
end;

function TXMLEmployeesType.Insert(const Index: Integer): IXMLEmployeeType;
begin
  Result := AddItem(Index) as IXMLEmployeeType;
end;

{ TXMLEmployeeType }

procedure TXMLEmployeeType.AfterConstruction;
begin
  RegisterChildNode('accrualgroup', TXMLAccrualgroupType);
  RegisterChildNode('alternateschedulemode', TXMLAlternateschedulemodeType);
  RegisterChildNode('citycountymaritalstatus', TXMLCitycountymaritalstatusType);
  RegisterChildNode('citycountytaxmethod', TXMLCitycountytaxmethodType);
  RegisterChildNode('employeestatus', TXMLEmployeestatusType);
  RegisterChildNode('employeetype', TXMLEmployeetypeType);
  RegisterChildNode('familymemberbirthdate', TXMLFamilymemberbirthdateType);
  RegisterChildNode('familymembergender', TXMLFamilymembergenderType);
  RegisterChildNode('gender', TXMLGenderType);
  RegisterChildNode('hiredate', TXMLHiredateType);
  RegisterChildNode('isfulltimestudent', TXMLIsfulltimestudentType);
  RegisterChildNode('localhiredate', TXMLLocalhiredateType);
  RegisterChildNode('paygroup', TXMLPaygroupType);
  RegisterChildNode('pointsgroup', TXMLPointsgroupType);
  RegisterChildNode('state', TXMLStateType);
  RegisterChildNode('roles', TXMLRolesType);
  RegisterChildNode('schedulemode', TXMLSchedulemodeType);
  RegisterChildNode('senioritydate', TXMLSenioritydateType);
  RegisterChildNode('shift', TXMLShiftType);
  RegisterChildNode('statemaritalstatus', TXMLStatemaritalstatusType);
  RegisterChildNode('statetaxmethod', TXMLStatetaxmethodType);
  RegisterChildNode('federalmaritalstatus', TXMLFederalmaritalstatusType);
  RegisterChildNode('federaltaxmethod', TXMLFederaltaxmethodType);
  RegisterChildNode('supervisor', TXMLSupervisorType);
  RegisterChildNode('visaexpiration', TXMLVisaexpirationType);
  RegisterChildNode('level', TXMLLevelType);
  RegisterChildNode('birthdate', TXMLBirthdateType);
  RegisterChildNode('citizenship', TXMLCitizenshipType);
  RegisterChildNode('dateofchange', TXMLDateofchangeType);
  RegisterChildNode('race', TXMLRaceType);
  RegisterChildNode('reviewdate', TXMLReviewdateType);
  RegisterChildNode('terminationdate', TXMLTerminationdateType);
  inherited;
end;

function TXMLEmployeeType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLEmployeeType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Accrualgroup: IXMLAccrualgroupType;
begin
  Result := ChildNodes['accrualgroup'] as IXMLAccrualgroupType;
end;

function TXMLEmployeeType.Get_Addressline1: WideString;
begin
  Result := ChildNodes['addressline1'].Text;
end;

procedure TXMLEmployeeType.Set_Addressline1(Value: WideString);
begin
  ChildNodes['addressline1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Addressline2: WideString;
begin
  Result := ChildNodes['addressline2'].Text;
end;

procedure TXMLEmployeeType.Set_Addressline2(Value: WideString);
begin
  ChildNodes['addressline2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Agecertificatenumber: WideString;
begin
  Result := ChildNodes['agecertificatenumber'].Text;
end;

procedure TXMLEmployeeType.Set_Agecertificatenumber(Value: WideString);
begin
  ChildNodes['agecertificatenumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternaterate1: WideString;
begin
  Result := ChildNodes['alternaterate1'].Text;
end;

procedure TXMLEmployeeType.Set_Alternaterate1(Value: WideString);
begin
  ChildNodes['alternaterate1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternaterate2: WideString;
begin
  Result := ChildNodes['alternaterate2'].Text;
end;

procedure TXMLEmployeeType.Set_Alternaterate2(Value: WideString);
begin
  ChildNodes['alternaterate2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternateschedulemode: IXMLAlternateschedulemodeType;
begin
  Result := ChildNodes['alternateschedulemode'] as IXMLAlternateschedulemodeType;
end;

function TXMLEmployeeType.Get_Alternativecontactphone: WideString;
begin
  Result := ChildNodes['alternativecontactphone'].Text;
end;

procedure TXMLEmployeeType.Set_Alternativecontactphone(Value: WideString);
begin
  ChildNodes['alternativecontactphone'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_City: WideString;
begin
  Result := ChildNodes['city'].Text;
end;

procedure TXMLEmployeeType.Set_City(Value: WideString);
begin
  ChildNodes['city'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountyadditionalwithholdingamount: Integer;
begin
  Result := ChildNodes['citycountyadditionalwithholdingamount'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Citycountyadditionalwithholdingamount(Value: Integer);
begin
  ChildNodes['citycountyadditionalwithholdingamount'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountyexemptions: Integer;
begin
  Result := ChildNodes['citycountyexemptions'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Citycountyexemptions(Value: Integer);
begin
  ChildNodes['citycountyexemptions'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountymaritalstatus: IXMLCitycountymaritalstatusType;
begin
  Result := ChildNodes['citycountymaritalstatus'] as IXMLCitycountymaritalstatusType;
end;

function TXMLEmployeeType.Get_Citycountytaxmethod: IXMLCitycountytaxmethodType;
begin
  Result := ChildNodes['citycountytaxmethod'] as IXMLCitycountytaxmethodType;
end;

function TXMLEmployeeType.Get_Education: WideString;
begin
  Result := ChildNodes['education'].Text;
end;

procedure TXMLEmployeeType.Set_Education(Value: WideString);
begin
  ChildNodes['education'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emailaddress: WideString;
begin
  Result := ChildNodes['emailaddress'].Text;
end;

procedure TXMLEmployeeType.Set_Emailaddress(Value: WideString);
begin
  ChildNodes['emailaddress'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactname: WideString;
begin
  Result := ChildNodes['emergencycontactname'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactname(Value: WideString);
begin
  ChildNodes['emergencycontactname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeeidnumber: WideString;
begin
  Result := ChildNodes['employeeidnumber'].Text;
end;

procedure TXMLEmployeeType.Set_Employeeidnumber(Value: WideString);
begin
  ChildNodes['employeeidnumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeestatus: IXMLEmployeestatusType;
begin
  Result := ChildNodes['employeestatus'] as IXMLEmployeestatusType;
end;

function TXMLEmployeeType.Get_Statusclass: Integer;
begin
  Result := ChildNodes['statusclass'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Statusclass(Value: Integer);
begin
  ChildNodes['statusclass'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeetype: IXMLEmployeetypeType;
begin
  Result := ChildNodes['employeetype'] as IXMLEmployeetypeType;
end;

function TXMLEmployeeType.Get_Familymemberbirthdate: IXMLFamilymemberbirthdateType;
begin
  Result := ChildNodes['familymemberbirthdate'] as IXMLFamilymemberbirthdateType;
end;

function TXMLEmployeeType.Get_Familymembergender: IXMLFamilymembergenderType;
begin
  Result := ChildNodes['familymembergender'] as IXMLFamilymembergenderType;
end;

function TXMLEmployeeType.Get_Familymembername: WideString;
begin
  Result := ChildNodes['familymembername'].Text;
end;

procedure TXMLEmployeeType.Set_Familymembername(Value: WideString);
begin
  ChildNodes['familymembername'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymemberrelation: WideString;
begin
  Result := ChildNodes['familymemberrelation'].Text;
end;

procedure TXMLEmployeeType.Set_Familymemberrelation(Value: WideString);
begin
  ChildNodes['familymemberrelation'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymemberssn: WideString;
begin
  Result := ChildNodes['familymemberssn'].Text;
end;

procedure TXMLEmployeeType.Set_Familymemberssn(Value: WideString);
begin
  ChildNodes['familymemberssn'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Firstname: WideString;
begin
  Result := ChildNodes['firstname'].Text;
end;

procedure TXMLEmployeeType.Set_Firstname(Value: WideString);
begin
  ChildNodes['firstname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Formername: WideString;
begin
  Result := ChildNodes['formername'].Text;
end;

procedure TXMLEmployeeType.Set_Formername(Value: WideString);
begin
  ChildNodes['formername'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Gender: IXMLGenderType;
begin
  Result := ChildNodes['gender'] as IXMLGenderType;
end;

function TXMLEmployeeType.Get_Hiredate: IXMLHiredateType;
begin
  Result := ChildNodes['hiredate'] as IXMLHiredateType;
end;

function TXMLEmployeeType.Get_Initials: WideString;
begin
  Result := ChildNodes['initials'].Text;
end;

procedure TXMLEmployeeType.Set_Initials(Value: WideString);
begin
  ChildNodes['initials'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isautopunchtoschedule: Integer;
begin
  Result := ChildNodes['isautopunchtoschedule'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isautopunchtoschedule(Value: Integer);
begin
  ChildNodes['isautopunchtoschedule'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isfulltimestudent: IXMLIsfulltimestudentType;
begin
  Result := ChildNodes['isfulltimestudent'] as IXMLIsfulltimestudentType;
end;

function TXMLEmployeeType.Get_Ismarried: Integer;
begin
  Result := ChildNodes['ismarried'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Ismarried(Value: Integer);
begin
  ChildNodes['ismarried'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isnopay: Integer;
begin
  Result := ChildNodes['isnopay'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isnopay(Value: Integer);
begin
  ChildNodes['isnopay'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isoverrideholidayhours: Integer;
begin
  Result := ChildNodes['isoverrideholidayhours'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isoverrideholidayhours(Value: Integer);
begin
  ChildNodes['isoverrideholidayhours'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Ispaidholidays: Integer;
begin
  Result := ChildNodes['ispaidholidays'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Ispaidholidays(Value: Integer);
begin
  ChildNodes['ispaidholidays'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isrehireable: Integer;
begin
  Result := ChildNodes['isrehireable'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isrehireable(Value: Integer);
begin
  ChildNodes['isrehireable'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Issalaried: Integer;
begin
  Result := ChildNodes['issalaried'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Issalaried(Value: Integer);
begin
  ChildNodes['issalaried'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isunionmember: Integer;
begin
  Result := ChildNodes['isunionmember'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isunionmember(Value: Integer);
begin
  ChildNodes['isunionmember'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Iswebenabled: Integer;
begin
  Result := ChildNodes['iswebenabled'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Iswebenabled(Value: Integer);
begin
  ChildNodes['iswebenabled'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Lastname: WideString;
begin
  Result := ChildNodes['lastname'].Text;
end;

procedure TXMLEmployeeType.Set_Lastname(Value: WideString);
begin
  ChildNodes['lastname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Localhiredate: IXMLLocalhiredateType;
begin
  Result := ChildNodes['localhiredate'] as IXMLLocalhiredateType;
end;

function TXMLEmployeeType.Get_Loginid: WideString;
begin
  Result := ChildNodes['loginid'].Text;
end;

procedure TXMLEmployeeType.Set_Loginid(Value: WideString);
begin
  ChildNodes['loginid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Lunchbreakminutes: Integer;
begin
  Result := ChildNodes['lunchbreakminutes'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Lunchbreakminutes(Value: Integer);
begin
  ChildNodes['lunchbreakminutes'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Middleinitial: WideString;
begin
  Result := ChildNodes['middleinitial'].Text;
end;

procedure TXMLEmployeeType.Set_Middleinitial(Value: WideString);
begin
  ChildNodes['middleinitial'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Overriddenholidayhours: Integer;
begin
  Result := ChildNodes['overriddenholidayhours'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Overriddenholidayhours(Value: Integer);
begin
  ChildNodes['overriddenholidayhours'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Paygroup: IXMLPaygroupType;
begin
  Result := ChildNodes['paygroup'] as IXMLPaygroupType;
end;

function TXMLEmployeeType.Get_Phone: WideString;
begin
  Result := ChildNodes['phone'].Text;
end;

procedure TXMLEmployeeType.Set_Phone(Value: WideString);
begin
  ChildNodes['phone'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Phonetype: WideString;
begin
  Result := ChildNodes['phonetype'].Text;
end;

procedure TXMLEmployeeType.Set_Phonetype(Value: WideString);
begin
  ChildNodes['phonetype'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Pointsgroup: IXMLPointsgroupType;
begin
  Result := ChildNodes['pointsgroup'] as IXMLPointsgroupType;
end;

function TXMLEmployeeType.Get_Primarylanguage: WideString;
begin
  Result := ChildNodes['primarylanguage'].Text;
end;

procedure TXMLEmployeeType.Set_Primarylanguage(Value: WideString);
begin
  ChildNodes['primarylanguage'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Primaryrate: WideString;
begin
  Result := ChildNodes['primaryrate'].Text;
end;

procedure TXMLEmployeeType.Set_Primaryrate(Value: WideString);
begin
  ChildNodes['primaryrate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_State: IXMLStateType;
begin
  Result := ChildNodes['state'] as IXMLStateType;
end;

function TXMLEmployeeType.Get_Roles: IXMLRolesType;
begin
  Result := ChildNodes['roles'] as IXMLRolesType;
end;

function TXMLEmployeeType.Get_Salary: WideString;
begin
  Result := ChildNodes['salary'].Text;
end;

procedure TXMLEmployeeType.Set_Salary(Value: WideString);
begin
  ChildNodes['salary'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Scheduledworkhoursperday: Integer;
begin
  Result := ChildNodes['scheduledworkhoursperday'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Scheduledworkhoursperday(Value: Integer);
begin
  ChildNodes['scheduledworkhoursperday'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Schedulemode: IXMLSchedulemodeType;
begin
  Result := ChildNodes['schedulemode'] as IXMLSchedulemodeType;
end;

function TXMLEmployeeType.Get_Secondarylanguage: WideString;
begin
  Result := ChildNodes['secondarylanguage'].Text;
end;

procedure TXMLEmployeeType.Set_Secondarylanguage(Value: WideString);
begin
  ChildNodes['secondarylanguage'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Senioritydate: IXMLSenioritydateType;
begin
  Result := ChildNodes['senioritydate'] as IXMLSenioritydateType;
end;

function TXMLEmployeeType.Get_Shift: IXMLShiftType;
begin
  Result := ChildNodes['shift'] as IXMLShiftType;
end;

function TXMLEmployeeType.Get_Shortname: WideString;
begin
  Result := ChildNodes['shortname'].Text;
end;

procedure TXMLEmployeeType.Set_Shortname(Value: WideString);
begin
  ChildNodes['shortname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Ssn: WideString;
begin
  Result := ChildNodes['ssn'].Text;
end;

procedure TXMLEmployeeType.Set_Ssn(Value: WideString);
begin
  ChildNodes['ssn'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Stateadditionalwithholdingamount: Integer;
begin
  Result := ChildNodes['stateadditionalwithholdingamount'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Stateadditionalwithholdingamount(Value: Integer);
begin
  ChildNodes['stateadditionalwithholdingamount'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Stateexemptions: Integer;
begin
  Result := ChildNodes['stateexemptions'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Stateexemptions(Value: Integer);
begin
  ChildNodes['stateexemptions'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Statemaritalstatus: IXMLStatemaritalstatusType;
begin
  Result := ChildNodes['statemaritalstatus'] as IXMLStatemaritalstatusType;
end;

function TXMLEmployeeType.Get_Statetaxmethod: IXMLStatetaxmethodType;
begin
  Result := ChildNodes['statetaxmethod'] as IXMLStatetaxmethodType;
end;

function TXMLEmployeeType.Get_Federaladditionalwithholdingamount: Integer;
begin
  Result := ChildNodes['federaladditionalwithholdingamount'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Federaladditionalwithholdingamount(Value: Integer);
begin
  ChildNodes['federaladditionalwithholdingamount'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Federalexemptions: Integer;
begin
  Result := ChildNodes['federalexemptions'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Federalexemptions(Value: Integer);
begin
  ChildNodes['federalexemptions'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Federalmaritalstatus: IXMLFederalmaritalstatusType;
begin
  Result := ChildNodes['federalmaritalstatus'] as IXMLFederalmaritalstatusType;
end;

function TXMLEmployeeType.Get_Federaltaxmethod: IXMLFederaltaxmethodType;
begin
  Result := ChildNodes['federaltaxmethod'] as IXMLFederaltaxmethodType;
end;

function TXMLEmployeeType.Get_Supervisor: IXMLSupervisorType;
begin
  Result := ChildNodes['supervisor'] as IXMLSupervisorType;
end;

function TXMLEmployeeType.Get_Timeclocknumber: string;
begin
  Result := ChildNodes['timeclocknumber'].Text;
end;

procedure TXMLEmployeeType.Set_Timeclocknumber(Value: string);
begin
  ChildNodes['timeclocknumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Unionlocal: WideString;
begin
  Result := ChildNodes['unionlocal'].Text;
end;

procedure TXMLEmployeeType.Set_Unionlocal(Value: WideString);
begin
  ChildNodes['unionlocal'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Unionname: WideString;
begin
  Result := ChildNodes['unionname'].Text;
end;

procedure TXMLEmployeeType.Set_Unionname(Value: WideString);
begin
  ChildNodes['unionname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Visaexpiration: IXMLVisaexpirationType;
begin
  Result := ChildNodes['visaexpiration'] as IXMLVisaexpirationType;
end;

function TXMLEmployeeType.Get_Visanumber: WideString;
begin
  Result := ChildNodes['visanumber'].Text;
end;

procedure TXMLEmployeeType.Set_Visanumber(Value: WideString);
begin
  ChildNodes['visanumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Visatype: WideString;
begin
  Result := ChildNodes['visatype'].Text;
end;

procedure TXMLEmployeeType.Set_Visatype(Value: WideString);
begin
  ChildNodes['visatype'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Workpermitnumber: WideString;
begin
  Result := ChildNodes['workpermitnumber'].Text;
end;

procedure TXMLEmployeeType.Set_Workpermitnumber(Value: WideString);
begin
  ChildNodes['workpermitnumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Zipcode: string;
begin
  Result := ChildNodes['zipcode'].Text;
end;

procedure TXMLEmployeeType.Set_Zipcode(Value: string);
begin
  ChildNodes['zipcode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Notificationlevel: Integer;
begin
  Result := ChildNodes['notificationlevel'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Notificationlevel(Value: Integer);
begin
  ChildNodes['notificationlevel'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Hierarchydetails: WideString;
begin
  Result := ChildNodes['hierarchydetails'].Text;
end;

procedure TXMLEmployeeType.Set_Hierarchydetails(Value: WideString);
begin
  ChildNodes['hierarchydetails'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Level: IXMLLevelType;
begin
  Result := ChildNodes['level'] as IXMLLevelType;
end;

function TXMLEmployeeType.Get_Company: WideString;
begin
  Result := ChildNodes['company'].Text;
end;

procedure TXMLEmployeeType.Set_Company(Value: WideString);
begin
  ChildNodes['company'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Location: WideString;
begin
  Result := ChildNodes['location'].Text;
end;

procedure TXMLEmployeeType.Set_Location(Value: WideString);
begin
  ChildNodes['location'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Division: WideString;
begin
  Result := ChildNodes['division'].Text;
end;

procedure TXMLEmployeeType.Set_Division(Value: WideString);
begin
  ChildNodes['division'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Department: WideString;
begin
  Result := ChildNodes['department'].Text;
end;

procedure TXMLEmployeeType.Set_Department(Value: WideString);
begin
  ChildNodes['department'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Position: WideString;
begin
  Result := ChildNodes['position'].Text;
end;

procedure TXMLEmployeeType.Set_Position(Value: WideString);
begin
  ChildNodes['position'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddresscity: WideString;
begin
  Result := ChildNodes['emergencycontactaddresscity'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddresscity(Value: WideString);
begin
  ChildNodes['emergencycontactaddresscity'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddresscountry: WideString;
begin
  Result := ChildNodes['emergencycontactaddresscountry'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddresscountry(Value: WideString);
begin
  ChildNodes['emergencycontactaddresscountry'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddressline1: WideString;
begin
  Result := ChildNodes['emergencycontactaddressline1'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddressline1(Value: WideString);
begin
  ChildNodes['emergencycontactaddressline1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddressline2: WideString;
begin
  Result := ChildNodes['emergencycontactaddressline2'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddressline2(Value: WideString);
begin
  ChildNodes['emergencycontactaddressline2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddressphone: Integer;
begin
  Result := ChildNodes['emergencycontactaddressphone'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddressphone(Value: Integer);
begin
  ChildNodes['emergencycontactaddressphone'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddresszipcode: WideString;
begin
  Result := ChildNodes['emergencycontactaddresszipcode'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddresszipcode(Value: WideString);
begin
  ChildNodes['emergencycontactaddresszipcode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Birthdate: IXMLBirthdateType;
begin
  Result := ChildNodes['birthdate'] as IXMLBirthdateType;
end;

function TXMLEmployeeType.Get_Citizenship: IXMLCitizenshipType;
begin
  Result := ChildNodes['citizenship'] as IXMLCitizenshipType;
end;

function TXMLEmployeeType.Get_Dateofchange: IXMLDateofchangeType;
begin
  Result := ChildNodes['dateofchange'] as IXMLDateofchangeType;
end;

function TXMLEmployeeType.Get_Race: IXMLRaceType;
begin
  Result := ChildNodes['race'] as IXMLRaceType;
end;

function TXMLEmployeeType.Get_Reviewdate: IXMLReviewdateType;
begin
  Result := ChildNodes['reviewdate'] as IXMLReviewdateType;
end;

function TXMLEmployeeType.Get_Terminationdate: IXMLTerminationdateType;
begin
  Result := ChildNodes['terminationdate'] as IXMLTerminationdateType;
end;

{ TXMLAccrualgroupType }

function TXMLAccrualgroupType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAccrualgroupType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAlternateschedulemodeType }

function TXMLAlternateschedulemodeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAlternateschedulemodeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCitycountymaritalstatusType }

function TXMLCitycountymaritalstatusType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCitycountymaritalstatusType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCitycountytaxmethodType }

function TXMLCitycountytaxmethodType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCitycountytaxmethodType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLEmployeestatusType }

function TXMLEmployeestatusType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLEmployeestatusType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLEmployeetypeType }

function TXMLEmployeetypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLEmployeetypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLFamilymemberbirthdateType }

function TXMLFamilymemberbirthdateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLFamilymemberbirthdateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLFamilymemberbirthdateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLFamilymemberbirthdateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLFamilymembergenderType }

function TXMLFamilymembergenderType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLFamilymembergenderType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLGenderType }

function TXMLGenderType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLGenderType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLHiredateType }

function TXMLHiredateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLHiredateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLHiredateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLHiredateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLIsfulltimestudentType }

function TXMLIsfulltimestudentType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsfulltimestudentType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLLocalhiredateType }

function TXMLLocalhiredateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLLocalhiredateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLLocalhiredateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLLocalhiredateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLPaygroupType }

function TXMLPaygroupType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPaygroupType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPointsgroupType }

function TXMLPointsgroupType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPointsgroupType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLStateType }

function TXMLStateType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLStateType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLRolesType }

function TXMLRolesType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLRolesType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLSchedulemodeType }

function TXMLSchedulemodeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLSchedulemodeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLSenioritydateType }

function TXMLSenioritydateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLSenioritydateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLSenioritydateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLSenioritydateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLShiftType }

function TXMLShiftType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLShiftType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLStatemaritalstatusType }

function TXMLStatemaritalstatusType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLStatemaritalstatusType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLStatetaxmethodType }

function TXMLStatetaxmethodType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLStatetaxmethodType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLFederalmaritalstatusType }

function TXMLFederalmaritalstatusType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLFederalmaritalstatusType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLFederaltaxmethodType }

function TXMLFederaltaxmethodType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLFederaltaxmethodType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLSupervisorType }

function TXMLSupervisorType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLSupervisorType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLVisaexpirationType }

function TXMLVisaexpirationType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLVisaexpirationType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLVisaexpirationType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLVisaexpirationType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLLevelType }

function TXMLLevelType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLLevelType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLBirthdateType }

function TXMLBirthdateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLBirthdateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLBirthdateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLBirthdateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLCitizenshipType }

function TXMLCitizenshipType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCitizenshipType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLDateofchangeType }

function TXMLDateofchangeType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLDateofchangeType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLDateofchangeType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLDateofchangeType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLRaceType }

function TXMLRaceType.Get_Name: Integer;
begin
  Result := AttributeNodes['name'].NodeValue;
end;

procedure TXMLRaceType.Set_Name(Value: Integer);
begin
  SetAttribute('name', Value);
end;

{ TXMLReviewdateType }

function TXMLReviewdateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLReviewdateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLReviewdateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLReviewdateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLTerminationdateType }

function TXMLTerminationdateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLTerminationdateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLTerminationdateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLTerminationdateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

end.