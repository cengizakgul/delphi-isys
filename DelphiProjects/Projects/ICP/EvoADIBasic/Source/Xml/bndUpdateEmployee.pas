
{*********************************************************************************************************}
{                                                                                                         }
{                                            XML Data Binding                                             }
{                                                                                                         }
{         Generated on: 4/9/2010 7:37:12 PM                                                               }
{       Generated from: E:\job\IS\integration-release\EvoADI\1.2\ADI\xml\UpdateEmployee\EmployeeXML.xml   }
{                                                                                                         }
{*********************************************************************************************************}

unit bndUpdateEmployee;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLEmployeeType = interface;
  IXMLIntegerList = interface;

{ IXMLEmployeeType }

  IXMLEmployeeType = interface(IXMLNode)
    ['{2C1E9EBE-4220-4E61-8EF1-5AA4BBAD8478}']
    { Property Accessors }
    function Get_Lastname: WideString;
    function Get_Firstname: WideString;
    function Get_Ssn: WideString;
    function Get_Employeeidnumber: WideString;
    function Get_Middleinitial: WideString;
    function Get_Timeclocknumber: WideString;
    function Get_Shortname: WideString;
    function Get_Iswebenabled: Integer;
    function Get_Initials: WideString;
    function Get_Loginid: WideString;
    function Get_Roles: WideString;
    function Get_Employeepassword: WideString;
    function Get_Paygroup: WideString;
    function Get_Accrualgroup: WideString;
    function Get_Pointsgroup: WideString;
    function Get_Supervisor: WideString;
    function Get_Schedulemode: Integer;
    function Get_Masterschedule: WideString;
    function Get_Rotationschedule: WideString;
    function Get_Alternateschedulemode: Integer;
    function Get_Alternatemasterschedule: WideString;
    function Get_Alternaterotationschedule: WideString;
    function Get_Zipcode: WideString;
    function Get_Addressline1: WideString;
    function Get_Addressline2: WideString;
    function Get_Phone: WideString;
    function Get_City: WideString;
    function Get_Emailaddress: WideString;
    function Get_State: WideString;
    function Get_Ispaidholidays: Integer;
    function Get_Isrehireable: Integer;
    function Get_Isnopay: Integer;
    function Get_Issalaried: Integer;
    function Get_Salary: WideString;
    function Get_Primaryrate: WideString;
    function Get_Isautopunchtoschedule: Integer;
    function Get_Alternaterate1: WideString;
    function Get_Alternaterate2: WideString;
    function Get_Employeestatus: WideString;
    function Get_Hiredate: WideString;
    function Get_Birthdate: WideString;
    function Get_Citizenship: WideString;
    function Get_Agecertificatenumber: IXMLIntegerList;
    function Get_Visanumber: Integer;
    function Get_Workpermitnumber: Integer;
    function Get_Visaexpiration: WideString;
    function Get_Isfulltimestudent: Integer;
    function Get_Visatype: Integer;
    function Get_Primarylanguage: WideString;
    function Get_Secondarylanguage: WideString;
    function Get_Senioritydate: WideString;
    function Get_Formername: WideString;
    function Get_Familymembername: WideString;
    function Get_Familymemberbirthdate: WideString;
    function Get_Familymemberssn: Integer;
    function Get_Familymemberrelation: WideString;
    function Get_Gender: Integer;
    function Get_Familymembergender: Integer;
    function Get_Education: WideString;
    function Get_Isunionmember: Integer;
    function Get_Race: Integer;
    function Get_Unionname: WideString;
    function Get_Unionlocal: WideString;
    function Get_Emergencycontactname: WideString;
    function Get_Alternativecontactphone: Integer;
    function Get_Emergencycontactaddressline1: WideString;
    function Get_Emergencycontactaddressline2: WideString;
    function Get_Emergencycontactaddresscountry: WideString;
    function Get_Emergencycontactaddresszipcode: Integer;
    function Get_Emergencycontactaddresscity: WideString;
    function Get_Emergencycontactaddressphone: Integer;
    function Get_Phonetype: WideString;
    function Get_Employeetype: Integer;
    function Get_Ismarried: Integer;
    function Get_Isoverrideholidayhours: Integer;
    function Get_Overriddenholidayhours: Integer;
    function Get_Dateofchange: WideString;
    function Get_Lunchbreakminutes: Integer;
    function Get_Scheduledworkhoursperday: Integer;
    function Get_Federaltaxmethod: Integer;
    function Get_Federalexemptions: Integer;
    function Get_Statetaxmethod: Integer;
    function Get_Stateexemptions: Integer;
    function Get_Stateadditionalwithholdingamount: Integer;
    function Get_Federalmaritalstatus: Integer;
    function Get_Statemaritalstatus: Integer;
    function Get_Citycounty: WideString;
    function Get_Shift: WideString;
    function Get_Localhiredate: WideString;
    function Get_Reviewdate: WideString;
    function Get_Citycountytaxmethod: Integer;
    function Get_Citycountyexemptions: Integer;
    function Get_Citycountyadditionalwithholdingamount: Integer;
    function Get_Federaladditionalwithholdingamount: Integer;
    function Get_Citycountymaritalstatus: Integer;
    function Get_Terminationdate: WideString;
    function Get_Terminationreason: WideString;
    function Get_Companyid: WideString;
    function Get_Locationid: WideString;
    function Get_Divisionid: WideString;
    function Get_Departmentid: WideString;
    function Get_Positionid: WideString;
    function Get_Canbecreated: Integer;
    procedure Set_Lastname(Value: WideString);
    procedure Set_Firstname(Value: WideString);
    procedure Set_Ssn(Value: WideString);
    procedure Set_Employeeidnumber(Value: WideString);
    procedure Set_Middleinitial(Value: WideString);
    procedure Set_Timeclocknumber(Value: WideString);
    procedure Set_Shortname(Value: WideString);
    procedure Set_Iswebenabled(Value: Integer);
    procedure Set_Initials(Value: WideString);
    procedure Set_Loginid(Value: WideString);
    procedure Set_Roles(Value: WideString);
    procedure Set_Employeepassword(Value: WideString);
    procedure Set_Paygroup(Value: WideString);
    procedure Set_Accrualgroup(Value: WideString);
    procedure Set_Pointsgroup(Value: WideString);
    procedure Set_Supervisor(Value: WideString);
    procedure Set_Schedulemode(Value: Integer);
    procedure Set_Masterschedule(Value: WideString);
    procedure Set_Rotationschedule(Value: WideString);
    procedure Set_Alternateschedulemode(Value: Integer);
    procedure Set_Alternatemasterschedule(Value: WideString);
    procedure Set_Alternaterotationschedule(Value: WideString);
    procedure Set_Zipcode(Value: WideString);
    procedure Set_Addressline1(Value: WideString);
    procedure Set_Addressline2(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_Emailaddress(Value: WideString);
    procedure Set_State(Value: WideString);
    procedure Set_Ispaidholidays(Value: Integer);
    procedure Set_Isrehireable(Value: Integer);
    procedure Set_Isnopay(Value: Integer);
    procedure Set_Issalaried(Value: Integer);
    procedure Set_Salary(Value: WideString);
    procedure Set_Primaryrate(Value: WideString);
    procedure Set_Isautopunchtoschedule(Value: Integer);
    procedure Set_Alternaterate1(Value: WideString);
    procedure Set_Alternaterate2(Value: WideString);
    procedure Set_Employeestatus(Value: WideString);
    procedure Set_Hiredate(Value: WideString);
    procedure Set_Birthdate(Value: WideString);
    procedure Set_Citizenship(Value: WideString);
    procedure Set_Visanumber(Value: Integer);
    procedure Set_Workpermitnumber(Value: Integer);
    procedure Set_Visaexpiration(Value: WideString);
    procedure Set_Isfulltimestudent(Value: Integer);
    procedure Set_Visatype(Value: Integer);
    procedure Set_Primarylanguage(Value: WideString);
    procedure Set_Secondarylanguage(Value: WideString);
    procedure Set_Senioritydate(Value: WideString);
    procedure Set_Formername(Value: WideString);
    procedure Set_Familymembername(Value: WideString);
    procedure Set_Familymemberbirthdate(Value: WideString);
    procedure Set_Familymemberssn(Value: Integer);
    procedure Set_Familymemberrelation(Value: WideString);
    procedure Set_Gender(Value: Integer);
    procedure Set_Familymembergender(Value: Integer);
    procedure Set_Education(Value: WideString);
    procedure Set_Isunionmember(Value: Integer);
    procedure Set_Race(Value: Integer);
    procedure Set_Unionname(Value: WideString);
    procedure Set_Unionlocal(Value: WideString);
    procedure Set_Emergencycontactname(Value: WideString);
    procedure Set_Alternativecontactphone(Value: Integer);
    procedure Set_Emergencycontactaddressline1(Value: WideString);
    procedure Set_Emergencycontactaddressline2(Value: WideString);
    procedure Set_Emergencycontactaddresscountry(Value: WideString);
    procedure Set_Emergencycontactaddresszipcode(Value: Integer);
    procedure Set_Emergencycontactaddresscity(Value: WideString);
    procedure Set_Emergencycontactaddressphone(Value: Integer);
    procedure Set_Phonetype(Value: WideString);
    procedure Set_Employeetype(Value: Integer);
    procedure Set_Ismarried(Value: Integer);
    procedure Set_Isoverrideholidayhours(Value: Integer);
    procedure Set_Overriddenholidayhours(Value: Integer);
    procedure Set_Dateofchange(Value: WideString);
    procedure Set_Lunchbreakminutes(Value: Integer);
    procedure Set_Scheduledworkhoursperday(Value: Integer);
    procedure Set_Federaltaxmethod(Value: Integer);
    procedure Set_Federalexemptions(Value: Integer);
    procedure Set_Statetaxmethod(Value: Integer);
    procedure Set_Stateexemptions(Value: Integer);
    procedure Set_Stateadditionalwithholdingamount(Value: Integer);
    procedure Set_Federalmaritalstatus(Value: Integer);
    procedure Set_Statemaritalstatus(Value: Integer);
    procedure Set_Citycounty(Value: WideString);
    procedure Set_Shift(Value: WideString);
    procedure Set_Localhiredate(Value: WideString);
    procedure Set_Reviewdate(Value: WideString);
    procedure Set_Citycountytaxmethod(Value: Integer);
    procedure Set_Citycountyexemptions(Value: Integer);
    procedure Set_Citycountyadditionalwithholdingamount(Value: Integer);
    procedure Set_Federaladditionalwithholdingamount(Value: Integer);
    procedure Set_Citycountymaritalstatus(Value: Integer);
    procedure Set_Terminationdate(Value: WideString);
    procedure Set_Terminationreason(Value: WideString);
    procedure Set_Companyid(Value: WideString);
    procedure Set_Locationid(Value: WideString);
    procedure Set_Divisionid(Value: WideString);
    procedure Set_Departmentid(Value: WideString);
    procedure Set_Positionid(Value: WideString);
    procedure Set_Canbecreated(Value: Integer);
    { Methods & Properties }
    property Lastname: WideString read Get_Lastname write Set_Lastname;
    property Firstname: WideString read Get_Firstname write Set_Firstname;
    property Ssn: WideString read Get_Ssn write Set_Ssn;
    property Employeeidnumber: WideString read Get_Employeeidnumber write Set_Employeeidnumber;
    property Middleinitial: WideString read Get_Middleinitial write Set_Middleinitial;
    property Timeclocknumber: WideString read Get_Timeclocknumber write Set_Timeclocknumber;
    property Shortname: WideString read Get_Shortname write Set_Shortname;
    property Iswebenabled: Integer read Get_Iswebenabled write Set_Iswebenabled;
    property Initials: WideString read Get_Initials write Set_Initials;
    property Loginid: WideString read Get_Loginid write Set_Loginid;
    property Roles: WideString read Get_Roles write Set_Roles;
    property Employeepassword: WideString read Get_Employeepassword write Set_Employeepassword;
    property Paygroup: WideString read Get_Paygroup write Set_Paygroup;
    property Accrualgroup: WideString read Get_Accrualgroup write Set_Accrualgroup;
    property Pointsgroup: WideString read Get_Pointsgroup write Set_Pointsgroup;
    property Supervisor: WideString read Get_Supervisor write Set_Supervisor;
    property Schedulemode: Integer read Get_Schedulemode write Set_Schedulemode;
    property Masterschedule: WideString read Get_Masterschedule write Set_Masterschedule;
    property Rotationschedule: WideString read Get_Rotationschedule write Set_Rotationschedule;
    property Alternateschedulemode: Integer read Get_Alternateschedulemode write Set_Alternateschedulemode;
    property Alternatemasterschedule: WideString read Get_Alternatemasterschedule write Set_Alternatemasterschedule;
    property Alternaterotationschedule: WideString read Get_Alternaterotationschedule write Set_Alternaterotationschedule;
    property Zipcode: WideString read Get_Zipcode write Set_Zipcode;
    property Addressline1: WideString read Get_Addressline1 write Set_Addressline1;
    property Addressline2: WideString read Get_Addressline2 write Set_Addressline2;
    property Phone: WideString read Get_Phone write Set_Phone;
    property City: WideString read Get_City write Set_City;
    property Emailaddress: WideString read Get_Emailaddress write Set_Emailaddress;
    property State: WideString read Get_State write Set_State;
    property Ispaidholidays: Integer read Get_Ispaidholidays write Set_Ispaidholidays;
    property Isrehireable: Integer read Get_Isrehireable write Set_Isrehireable;
    property Isnopay: Integer read Get_Isnopay write Set_Isnopay;
    property Issalaried: Integer read Get_Issalaried write Set_Issalaried;
    property Salary: WideString read Get_Salary write Set_Salary;
    property Primaryrate: WideString read Get_Primaryrate write Set_Primaryrate;
    property Isautopunchtoschedule: Integer read Get_Isautopunchtoschedule write Set_Isautopunchtoschedule;
    property Alternaterate1: WideString read Get_Alternaterate1 write Set_Alternaterate1;
    property Alternaterate2: WideString read Get_Alternaterate2 write Set_Alternaterate2;
    property Employeestatus: WideString read Get_Employeestatus write Set_Employeestatus;
    property Hiredate: WideString read Get_Hiredate write Set_Hiredate;
    property Birthdate: WideString read Get_Birthdate write Set_Birthdate;
    property Citizenship: WideString read Get_Citizenship write Set_Citizenship;
    property Agecertificatenumber: IXMLIntegerList read Get_Agecertificatenumber;
    property Visanumber: Integer read Get_Visanumber write Set_Visanumber;
    property Workpermitnumber: Integer read Get_Workpermitnumber write Set_Workpermitnumber;
    property Visaexpiration: WideString read Get_Visaexpiration write Set_Visaexpiration;
    property Isfulltimestudent: Integer read Get_Isfulltimestudent write Set_Isfulltimestudent;
    property Visatype: Integer read Get_Visatype write Set_Visatype;
    property Primarylanguage: WideString read Get_Primarylanguage write Set_Primarylanguage;
    property Secondarylanguage: WideString read Get_Secondarylanguage write Set_Secondarylanguage;
    property Senioritydate: WideString read Get_Senioritydate write Set_Senioritydate;
    property Formername: WideString read Get_Formername write Set_Formername;
    property Familymembername: WideString read Get_Familymembername write Set_Familymembername;
    property Familymemberbirthdate: WideString read Get_Familymemberbirthdate write Set_Familymemberbirthdate;
    property Familymemberssn: Integer read Get_Familymemberssn write Set_Familymemberssn;
    property Familymemberrelation: WideString read Get_Familymemberrelation write Set_Familymemberrelation;
    property Gender: Integer read Get_Gender write Set_Gender;
    property Familymembergender: Integer read Get_Familymembergender write Set_Familymembergender;
    property Education: WideString read Get_Education write Set_Education;
    property Isunionmember: Integer read Get_Isunionmember write Set_Isunionmember;
    property Race: Integer read Get_Race write Set_Race;
    property Unionname: WideString read Get_Unionname write Set_Unionname;
    property Unionlocal: WideString read Get_Unionlocal write Set_Unionlocal;
    property Emergencycontactname: WideString read Get_Emergencycontactname write Set_Emergencycontactname;
    property Alternativecontactphone: Integer read Get_Alternativecontactphone write Set_Alternativecontactphone;
    property Emergencycontactaddressline1: WideString read Get_Emergencycontactaddressline1 write Set_Emergencycontactaddressline1;
    property Emergencycontactaddressline2: WideString read Get_Emergencycontactaddressline2 write Set_Emergencycontactaddressline2;
    property Emergencycontactaddresscountry: WideString read Get_Emergencycontactaddresscountry write Set_Emergencycontactaddresscountry;
    property Emergencycontactaddresszipcode: Integer read Get_Emergencycontactaddresszipcode write Set_Emergencycontactaddresszipcode;
    property Emergencycontactaddresscity: WideString read Get_Emergencycontactaddresscity write Set_Emergencycontactaddresscity;
    property Emergencycontactaddressphone: Integer read Get_Emergencycontactaddressphone write Set_Emergencycontactaddressphone;
    property Phonetype: WideString read Get_Phonetype write Set_Phonetype;
    property Employeetype: Integer read Get_Employeetype write Set_Employeetype;
    property Ismarried: Integer read Get_Ismarried write Set_Ismarried;
    property Isoverrideholidayhours: Integer read Get_Isoverrideholidayhours write Set_Isoverrideholidayhours;
    property Overriddenholidayhours: Integer read Get_Overriddenholidayhours write Set_Overriddenholidayhours;
    property Dateofchange: WideString read Get_Dateofchange write Set_Dateofchange;
    property Lunchbreakminutes: Integer read Get_Lunchbreakminutes write Set_Lunchbreakminutes;
    property Scheduledworkhoursperday: Integer read Get_Scheduledworkhoursperday write Set_Scheduledworkhoursperday;
    property Federaltaxmethod: Integer read Get_Federaltaxmethod write Set_Federaltaxmethod;
    property Federalexemptions: Integer read Get_Federalexemptions write Set_Federalexemptions;
    property Statetaxmethod: Integer read Get_Statetaxmethod write Set_Statetaxmethod;
    property Stateexemptions: Integer read Get_Stateexemptions write Set_Stateexemptions;
    property Stateadditionalwithholdingamount: Integer read Get_Stateadditionalwithholdingamount write Set_Stateadditionalwithholdingamount;
    property Federalmaritalstatus: Integer read Get_Federalmaritalstatus write Set_Federalmaritalstatus;
    property Statemaritalstatus: Integer read Get_Statemaritalstatus write Set_Statemaritalstatus;
    property Citycounty: WideString read Get_Citycounty write Set_Citycounty;
    property Shift: WideString read Get_Shift write Set_Shift;
    property Localhiredate: WideString read Get_Localhiredate write Set_Localhiredate;
    property Reviewdate: WideString read Get_Reviewdate write Set_Reviewdate;
    property Citycountytaxmethod: Integer read Get_Citycountytaxmethod write Set_Citycountytaxmethod;
    property Citycountyexemptions: Integer read Get_Citycountyexemptions write Set_Citycountyexemptions;
    property Citycountyadditionalwithholdingamount: Integer read Get_Citycountyadditionalwithholdingamount write Set_Citycountyadditionalwithholdingamount;
    property Federaladditionalwithholdingamount: Integer read Get_Federaladditionalwithholdingamount write Set_Federaladditionalwithholdingamount;
    property Citycountymaritalstatus: Integer read Get_Citycountymaritalstatus write Set_Citycountymaritalstatus;
    property Terminationdate: WideString read Get_Terminationdate write Set_Terminationdate;
    property Terminationreason: WideString read Get_Terminationreason write Set_Terminationreason;
    property Companyid: WideString read Get_Companyid write Set_Companyid;
    property Locationid: WideString read Get_Locationid write Set_Locationid;
    property Divisionid: WideString read Get_Divisionid write Set_Divisionid;
    property Departmentid: WideString read Get_Departmentid write Set_Departmentid;
    property Positionid: WideString read Get_Positionid write Set_Positionid;
    property Canbecreated: Integer read Get_Canbecreated write Set_Canbecreated;
  end;

{ IXMLIntegerList }

  IXMLIntegerList = interface(IXMLNodeCollection)
    ['{6C9C799C-A996-4A48-B8EB-4F5C0D14D1F0}']
    { Methods & Properties }
    function Add(const Value: Integer): IXMLNode;
    function Insert(const Index: Integer; const Value: Integer): IXMLNode;
    function Get_Item(Index: Integer): Integer;
    property Items[Index: Integer]: Integer read Get_Item; default;
  end;

{ Forward Decls }

  TXMLEmployeeType = class;
  TXMLIntegerList = class;

{ TXMLEmployeeType }

  TXMLEmployeeType = class(TXMLNode, IXMLEmployeeType)
  private
    FAgecertificatenumber: IXMLIntegerList;
  protected
    { IXMLEmployeeType }
    function Get_Lastname: WideString;
    function Get_Firstname: WideString;
    function Get_Ssn: WideString;
    function Get_Employeeidnumber: WideString;
    function Get_Middleinitial: WideString;
    function Get_Timeclocknumber: WideString;
    function Get_Shortname: WideString;
    function Get_Iswebenabled: Integer;
    function Get_Initials: WideString;
    function Get_Loginid: WideString;
    function Get_Roles: WideString;
    function Get_Employeepassword: WideString;
    function Get_Paygroup: WideString;
    function Get_Accrualgroup: WideString;
    function Get_Pointsgroup: WideString;
    function Get_Supervisor: WideString;
    function Get_Schedulemode: Integer;
    function Get_Masterschedule: WideString;
    function Get_Rotationschedule: WideString;
    function Get_Alternateschedulemode: Integer;
    function Get_Alternatemasterschedule: WideString;
    function Get_Alternaterotationschedule: WideString;
    function Get_Zipcode: WideString;
    function Get_Addressline1: WideString;
    function Get_Addressline2: WideString;
    function Get_Phone: WideString;
    function Get_City: WideString;
    function Get_Emailaddress: WideString;
    function Get_State: WideString;
    function Get_Ispaidholidays: Integer;
    function Get_Isrehireable: Integer;
    function Get_Isnopay: Integer;
    function Get_Issalaried: Integer;
    function Get_Salary: WideString;
    function Get_Primaryrate: WideString;
    function Get_Isautopunchtoschedule: Integer;
    function Get_Alternaterate1: WideString;
    function Get_Alternaterate2: WideString;
    function Get_Employeestatus: WideString;
    function Get_Hiredate: WideString;
    function Get_Birthdate: WideString;
    function Get_Citizenship: WideString;
    function Get_Agecertificatenumber: IXMLIntegerList;
    function Get_Visanumber: Integer;
    function Get_Workpermitnumber: Integer;
    function Get_Visaexpiration: WideString;
    function Get_Isfulltimestudent: Integer;
    function Get_Visatype: Integer;
    function Get_Primarylanguage: WideString;
    function Get_Secondarylanguage: WideString;
    function Get_Senioritydate: WideString;
    function Get_Formername: WideString;
    function Get_Familymembername: WideString;
    function Get_Familymemberbirthdate: WideString;
    function Get_Familymemberssn: Integer;
    function Get_Familymemberrelation: WideString;
    function Get_Gender: Integer;
    function Get_Familymembergender: Integer;
    function Get_Education: WideString;
    function Get_Isunionmember: Integer;
    function Get_Race: Integer;
    function Get_Unionname: WideString;
    function Get_Unionlocal: WideString;
    function Get_Emergencycontactname: WideString;
    function Get_Alternativecontactphone: Integer;
    function Get_Emergencycontactaddressline1: WideString;
    function Get_Emergencycontactaddressline2: WideString;
    function Get_Emergencycontactaddresscountry: WideString;
    function Get_Emergencycontactaddresszipcode: Integer;
    function Get_Emergencycontactaddresscity: WideString;
    function Get_Emergencycontactaddressphone: Integer;
    function Get_Phonetype: WideString;
    function Get_Employeetype: Integer;
    function Get_Ismarried: Integer;
    function Get_Isoverrideholidayhours: Integer;
    function Get_Overriddenholidayhours: Integer;
    function Get_Dateofchange: WideString;
    function Get_Lunchbreakminutes: Integer;
    function Get_Scheduledworkhoursperday: Integer;
    function Get_Federaltaxmethod: Integer;
    function Get_Federalexemptions: Integer;
    function Get_Statetaxmethod: Integer;
    function Get_Stateexemptions: Integer;
    function Get_Stateadditionalwithholdingamount: Integer;
    function Get_Federalmaritalstatus: Integer;
    function Get_Statemaritalstatus: Integer;
    function Get_Citycounty: WideString;
    function Get_Shift: WideString;
    function Get_Localhiredate: WideString;
    function Get_Reviewdate: WideString;
    function Get_Citycountytaxmethod: Integer;
    function Get_Citycountyexemptions: Integer;
    function Get_Citycountyadditionalwithholdingamount: Integer;
    function Get_Federaladditionalwithholdingamount: Integer;
    function Get_Citycountymaritalstatus: Integer;
    function Get_Terminationdate: WideString;
    function Get_Terminationreason: WideString;
    function Get_Companyid: WideString;
    function Get_Locationid: WideString;
    function Get_Divisionid: WideString;
    function Get_Departmentid: WideString;
    function Get_Positionid: WideString;
    function Get_Canbecreated: Integer;
    procedure Set_Lastname(Value: WideString);
    procedure Set_Firstname(Value: WideString);
    procedure Set_Ssn(Value: WideString);
    procedure Set_Employeeidnumber(Value: WideString);
    procedure Set_Middleinitial(Value: WideString);
    procedure Set_Timeclocknumber(Value: WideString);
    procedure Set_Shortname(Value: WideString);
    procedure Set_Iswebenabled(Value: Integer);
    procedure Set_Initials(Value: WideString);
    procedure Set_Loginid(Value: WideString);
    procedure Set_Roles(Value: WideString);
    procedure Set_Employeepassword(Value: WideString);
    procedure Set_Paygroup(Value: WideString);
    procedure Set_Accrualgroup(Value: WideString);
    procedure Set_Pointsgroup(Value: WideString);
    procedure Set_Supervisor(Value: WideString);
    procedure Set_Schedulemode(Value: Integer);
    procedure Set_Masterschedule(Value: WideString);
    procedure Set_Rotationschedule(Value: WideString);
    procedure Set_Alternateschedulemode(Value: Integer);
    procedure Set_Alternatemasterschedule(Value: WideString);
    procedure Set_Alternaterotationschedule(Value: WideString);
    procedure Set_Zipcode(Value: WideString);
    procedure Set_Addressline1(Value: WideString);
    procedure Set_Addressline2(Value: WideString);
    procedure Set_Phone(Value: WideString);
    procedure Set_City(Value: WideString);
    procedure Set_Emailaddress(Value: WideString);
    procedure Set_State(Value: WideString);
    procedure Set_Ispaidholidays(Value: Integer);
    procedure Set_Isrehireable(Value: Integer);
    procedure Set_Isnopay(Value: Integer);
    procedure Set_Issalaried(Value: Integer);
    procedure Set_Salary(Value: WideString);
    procedure Set_Primaryrate(Value: WideString);
    procedure Set_Isautopunchtoschedule(Value: Integer);
    procedure Set_Alternaterate1(Value: WideString);
    procedure Set_Alternaterate2(Value: WideString);
    procedure Set_Employeestatus(Value: WideString);
    procedure Set_Hiredate(Value: WideString);
    procedure Set_Birthdate(Value: WideString);
    procedure Set_Citizenship(Value: WideString);
    procedure Set_Visanumber(Value: Integer);
    procedure Set_Workpermitnumber(Value: Integer);
    procedure Set_Visaexpiration(Value: WideString);
    procedure Set_Isfulltimestudent(Value: Integer);
    procedure Set_Visatype(Value: Integer);
    procedure Set_Primarylanguage(Value: WideString);
    procedure Set_Secondarylanguage(Value: WideString);
    procedure Set_Senioritydate(Value: WideString);
    procedure Set_Formername(Value: WideString);
    procedure Set_Familymembername(Value: WideString);
    procedure Set_Familymemberbirthdate(Value: WideString);
    procedure Set_Familymemberssn(Value: Integer);
    procedure Set_Familymemberrelation(Value: WideString);
    procedure Set_Gender(Value: Integer);
    procedure Set_Familymembergender(Value: Integer);
    procedure Set_Education(Value: WideString);
    procedure Set_Isunionmember(Value: Integer);
    procedure Set_Race(Value: Integer);
    procedure Set_Unionname(Value: WideString);
    procedure Set_Unionlocal(Value: WideString);
    procedure Set_Emergencycontactname(Value: WideString);
    procedure Set_Alternativecontactphone(Value: Integer);
    procedure Set_Emergencycontactaddressline1(Value: WideString);
    procedure Set_Emergencycontactaddressline2(Value: WideString);
    procedure Set_Emergencycontactaddresscountry(Value: WideString);
    procedure Set_Emergencycontactaddresszipcode(Value: Integer);
    procedure Set_Emergencycontactaddresscity(Value: WideString);
    procedure Set_Emergencycontactaddressphone(Value: Integer);
    procedure Set_Phonetype(Value: WideString);
    procedure Set_Employeetype(Value: Integer);
    procedure Set_Ismarried(Value: Integer);
    procedure Set_Isoverrideholidayhours(Value: Integer);
    procedure Set_Overriddenholidayhours(Value: Integer);
    procedure Set_Dateofchange(Value: WideString);
    procedure Set_Lunchbreakminutes(Value: Integer);
    procedure Set_Scheduledworkhoursperday(Value: Integer);
    procedure Set_Federaltaxmethod(Value: Integer);
    procedure Set_Federalexemptions(Value: Integer);
    procedure Set_Statetaxmethod(Value: Integer);
    procedure Set_Stateexemptions(Value: Integer);
    procedure Set_Stateadditionalwithholdingamount(Value: Integer);
    procedure Set_Federalmaritalstatus(Value: Integer);
    procedure Set_Statemaritalstatus(Value: Integer);
    procedure Set_Citycounty(Value: WideString);
    procedure Set_Shift(Value: WideString);
    procedure Set_Localhiredate(Value: WideString);
    procedure Set_Reviewdate(Value: WideString);
    procedure Set_Citycountytaxmethod(Value: Integer);
    procedure Set_Citycountyexemptions(Value: Integer);
    procedure Set_Citycountyadditionalwithholdingamount(Value: Integer);
    procedure Set_Federaladditionalwithholdingamount(Value: Integer);
    procedure Set_Citycountymaritalstatus(Value: Integer);
    procedure Set_Terminationdate(Value: WideString);
    procedure Set_Terminationreason(Value: WideString);
    procedure Set_Companyid(Value: WideString);
    procedure Set_Locationid(Value: WideString);
    procedure Set_Divisionid(Value: WideString);
    procedure Set_Departmentid(Value: WideString);
    procedure Set_Positionid(Value: WideString);
    procedure Set_Canbecreated(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIntegerList }

  TXMLIntegerList = class(TXMLNodeCollection, IXMLIntegerList)
  protected
    { IXMLIntegerList }
    function Add(const Value: Integer): IXMLNode;
    function Insert(const Index: Integer; const Value: Integer): IXMLNode;
    function Get_Item(Index: Integer): Integer;
  end;

{ Global Functions }

function Getemployee(Doc: IXMLDocument): IXMLEmployeeType;
function Loademployee(const FileName: WideString): IXMLEmployeeType;
function Newemployee: IXMLEmployeeType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getemployee(Doc: IXMLDocument): IXMLEmployeeType;
begin
  Result := Doc.GetDocBinding('employee', TXMLEmployeeType, TargetNamespace) as IXMLEmployeeType;
end;

function Loademployee(const FileName: WideString): IXMLEmployeeType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('employee', TXMLEmployeeType, TargetNamespace) as IXMLEmployeeType;
end;

function Newemployee: IXMLEmployeeType;
begin
  Result := NewXMLDocument.GetDocBinding('employee', TXMLEmployeeType, TargetNamespace) as IXMLEmployeeType;
end;

{ TXMLEmployeeType }

procedure TXMLEmployeeType.AfterConstruction;
begin
  FAgecertificatenumber := CreateCollection(TXMLIntegerList, IXMLNode, 'agecertificatenumber') as IXMLIntegerList;
  inherited;
end;

function TXMLEmployeeType.Get_Lastname: WideString;
begin
  Result := ChildNodes['lastname'].Text;
end;

procedure TXMLEmployeeType.Set_Lastname(Value: WideString);
begin
  ChildNodes['lastname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Firstname: WideString;
begin
  Result := ChildNodes['firstname'].Text;
end;

procedure TXMLEmployeeType.Set_Firstname(Value: WideString);
begin
  ChildNodes['firstname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Ssn: WideString;
begin
  Result := ChildNodes['ssn'].Text;
end;

procedure TXMLEmployeeType.Set_Ssn(Value: WideString);
begin
  ChildNodes['ssn'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeeidnumber: WideString;
begin
  Result := ChildNodes['employeeidnumber'].Text;
end;

procedure TXMLEmployeeType.Set_Employeeidnumber(Value: WideString);
begin
  ChildNodes['employeeidnumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Middleinitial: WideString;
begin
  Result := ChildNodes['middleinitial'].Text;
end;

procedure TXMLEmployeeType.Set_Middleinitial(Value: WideString);
begin
  ChildNodes['middleinitial'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Timeclocknumber: WideString;
begin
  Result := ChildNodes['timeclocknumber'].Text;
end;

procedure TXMLEmployeeType.Set_Timeclocknumber(Value: WideString);
begin
  ChildNodes['timeclocknumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Shortname: WideString;
begin
  Result := ChildNodes['shortname'].Text;
end;

procedure TXMLEmployeeType.Set_Shortname(Value: WideString);
begin
  ChildNodes['shortname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Iswebenabled: Integer;
begin
  Result := ChildNodes['iswebenabled'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Iswebenabled(Value: Integer);
begin
  ChildNodes['iswebenabled'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Initials: WideString;
begin
  Result := ChildNodes['initials'].Text;
end;

procedure TXMLEmployeeType.Set_Initials(Value: WideString);
begin
  ChildNodes['initials'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Loginid: WideString;
begin
  Result := ChildNodes['loginid'].Text;
end;

procedure TXMLEmployeeType.Set_Loginid(Value: WideString);
begin
  ChildNodes['loginid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Roles: WideString;
begin
  Result := ChildNodes['roles'].Text;
end;

procedure TXMLEmployeeType.Set_Roles(Value: WideString);
begin
  ChildNodes['roles'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeepassword: WideString;
begin
  Result := ChildNodes['employeepassword'].Text;
end;

procedure TXMLEmployeeType.Set_Employeepassword(Value: WideString);
begin
  ChildNodes['employeepassword'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Paygroup: WideString;
begin
  Result := ChildNodes['paygroup'].Text;
end;

procedure TXMLEmployeeType.Set_Paygroup(Value: WideString);
begin
  ChildNodes['paygroup'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Accrualgroup: WideString;
begin
  Result := ChildNodes['accrualgroup'].Text;
end;

procedure TXMLEmployeeType.Set_Accrualgroup(Value: WideString);
begin
  ChildNodes['accrualgroup'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Pointsgroup: WideString;
begin
  Result := ChildNodes['pointsgroup'].Text;
end;

procedure TXMLEmployeeType.Set_Pointsgroup(Value: WideString);
begin
  ChildNodes['pointsgroup'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Supervisor: WideString;
begin
  Result := ChildNodes['supervisor'].Text;
end;

procedure TXMLEmployeeType.Set_Supervisor(Value: WideString);
begin
  ChildNodes['supervisor'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Schedulemode: Integer;
begin
  Result := ChildNodes['schedulemode'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Schedulemode(Value: Integer);
begin
  ChildNodes['schedulemode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Masterschedule: WideString;
begin
  Result := ChildNodes['masterschedule'].Text;
end;

procedure TXMLEmployeeType.Set_Masterschedule(Value: WideString);
begin
  ChildNodes['masterschedule'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Rotationschedule: WideString;
begin
  Result := ChildNodes['rotationschedule'].Text;
end;

procedure TXMLEmployeeType.Set_Rotationschedule(Value: WideString);
begin
  ChildNodes['rotationschedule'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternateschedulemode: Integer;
begin
  Result := ChildNodes['alternateschedulemode'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Alternateschedulemode(Value: Integer);
begin
  ChildNodes['alternateschedulemode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternatemasterschedule: WideString;
begin
  Result := ChildNodes['alternatemasterschedule'].Text;
end;

procedure TXMLEmployeeType.Set_Alternatemasterschedule(Value: WideString);
begin
  ChildNodes['alternatemasterschedule'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternaterotationschedule: WideString;
begin
  Result := ChildNodes['alternaterotationschedule'].Text;
end;

procedure TXMLEmployeeType.Set_Alternaterotationschedule(Value: WideString);
begin
  ChildNodes['alternaterotationschedule'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Zipcode: WideString;
begin
  Result := ChildNodes['zipcode'].Text;
end;

procedure TXMLEmployeeType.Set_Zipcode(Value: WideString);
begin
  ChildNodes['zipcode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Addressline1: WideString;
begin
  Result := ChildNodes['addressline1'].Text;
end;

procedure TXMLEmployeeType.Set_Addressline1(Value: WideString);
begin
  ChildNodes['addressline1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Addressline2: WideString;
begin
  Result := ChildNodes['addressline2'].Text;
end;

procedure TXMLEmployeeType.Set_Addressline2(Value: WideString);
begin
  ChildNodes['addressline2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Phone: WideString;
begin
  Result := ChildNodes['phone'].Text;
end;

procedure TXMLEmployeeType.Set_Phone(Value: WideString);
begin
  ChildNodes['phone'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_City: WideString;
begin
  Result := ChildNodes['city'].Text;
end;

procedure TXMLEmployeeType.Set_City(Value: WideString);
begin
  ChildNodes['city'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emailaddress: WideString;
begin
  Result := ChildNodes['emailaddress'].Text;
end;

procedure TXMLEmployeeType.Set_Emailaddress(Value: WideString);
begin
  ChildNodes['emailaddress'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_State: WideString;
begin
  Result := ChildNodes['state'].Text;
end;

procedure TXMLEmployeeType.Set_State(Value: WideString);
begin
  ChildNodes['state'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Ispaidholidays: Integer;
begin
  Result := ChildNodes['ispaidholidays'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Ispaidholidays(Value: Integer);
begin
  ChildNodes['ispaidholidays'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isrehireable: Integer;
begin
  Result := ChildNodes['isrehireable'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isrehireable(Value: Integer);
begin
  ChildNodes['isrehireable'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isnopay: Integer;
begin
  Result := ChildNodes['isnopay'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isnopay(Value: Integer);
begin
  ChildNodes['isnopay'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Issalaried: Integer;
begin
  Result := ChildNodes['issalaried'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Issalaried(Value: Integer);
begin
  ChildNodes['issalaried'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Salary: WideString;
begin
  Result := ChildNodes['salary'].Text;
end;

procedure TXMLEmployeeType.Set_Salary(Value: WideString);
begin
  ChildNodes['salary'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Primaryrate: WideString;
begin
  Result := ChildNodes['primaryrate'].Text;
end;

procedure TXMLEmployeeType.Set_Primaryrate(Value: WideString);
begin
  ChildNodes['primaryrate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isautopunchtoschedule: Integer;
begin
  Result := ChildNodes['isautopunchtoschedule'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isautopunchtoschedule(Value: Integer);
begin
  ChildNodes['isautopunchtoschedule'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternaterate1: WideString;
begin
  Result := ChildNodes['alternaterate1'].Text;
end;

procedure TXMLEmployeeType.Set_Alternaterate1(Value: WideString);
begin
  ChildNodes['alternaterate1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternaterate2: WideString;
begin
  Result := ChildNodes['alternaterate2'].Text;
end;

procedure TXMLEmployeeType.Set_Alternaterate2(Value: WideString);
begin
  ChildNodes['alternaterate2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeestatus: WideString;
begin
  Result := ChildNodes['employeestatus'].Text;
end;

procedure TXMLEmployeeType.Set_Employeestatus(Value: WideString);
begin
  ChildNodes['employeestatus'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Hiredate: WideString;
begin
  Result := ChildNodes['hiredate'].Text;
end;

procedure TXMLEmployeeType.Set_Hiredate(Value: WideString);
begin
  ChildNodes['hiredate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Birthdate: WideString;
begin
  Result := ChildNodes['birthdate'].Text;
end;

procedure TXMLEmployeeType.Set_Birthdate(Value: WideString);
begin
  ChildNodes['birthdate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citizenship: WideString;
begin
  Result := ChildNodes['citizenship'].Text;
end;

procedure TXMLEmployeeType.Set_Citizenship(Value: WideString);
begin
  ChildNodes['citizenship'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Agecertificatenumber: IXMLIntegerList;
begin
  Result := FAgecertificatenumber;
end;

function TXMLEmployeeType.Get_Visanumber: Integer;
begin
  Result := ChildNodes['visanumber'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Visanumber(Value: Integer);
begin
  ChildNodes['visanumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Workpermitnumber: Integer;
begin
  Result := ChildNodes['workpermitnumber'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Workpermitnumber(Value: Integer);
begin
  ChildNodes['workpermitnumber'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Visaexpiration: WideString;
begin
  Result := ChildNodes['visaexpiration'].Text;
end;

procedure TXMLEmployeeType.Set_Visaexpiration(Value: WideString);
begin
  ChildNodes['visaexpiration'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isfulltimestudent: Integer;
begin
  Result := ChildNodes['isfulltimestudent'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isfulltimestudent(Value: Integer);
begin
  ChildNodes['isfulltimestudent'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Visatype: Integer;
begin
  Result := ChildNodes['visatype'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Visatype(Value: Integer);
begin
  ChildNodes['visatype'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Primarylanguage: WideString;
begin
  Result := ChildNodes['primarylanguage'].Text;
end;

procedure TXMLEmployeeType.Set_Primarylanguage(Value: WideString);
begin
  ChildNodes['primarylanguage'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Secondarylanguage: WideString;
begin
  Result := ChildNodes['secondarylanguage'].Text;
end;

procedure TXMLEmployeeType.Set_Secondarylanguage(Value: WideString);
begin
  ChildNodes['secondarylanguage'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Senioritydate: WideString;
begin
  Result := ChildNodes['senioritydate'].Text;
end;

procedure TXMLEmployeeType.Set_Senioritydate(Value: WideString);
begin
  ChildNodes['senioritydate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Formername: WideString;
begin
  Result := ChildNodes['formername'].Text;
end;

procedure TXMLEmployeeType.Set_Formername(Value: WideString);
begin
  ChildNodes['formername'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymembername: WideString;
begin
  Result := ChildNodes['familymembername'].Text;
end;

procedure TXMLEmployeeType.Set_Familymembername(Value: WideString);
begin
  ChildNodes['familymembername'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymemberbirthdate: WideString;
begin
  Result := ChildNodes['familymemberbirthdate'].Text;
end;

procedure TXMLEmployeeType.Set_Familymemberbirthdate(Value: WideString);
begin
  ChildNodes['familymemberbirthdate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymemberssn: Integer;
begin
  Result := ChildNodes['familymemberssn'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Familymemberssn(Value: Integer);
begin
  ChildNodes['familymemberssn'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymemberrelation: WideString;
begin
  Result := ChildNodes['familymemberrelation'].Text;
end;

procedure TXMLEmployeeType.Set_Familymemberrelation(Value: WideString);
begin
  ChildNodes['familymemberrelation'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Gender: Integer;
begin
  Result := ChildNodes['gender'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Gender(Value: Integer);
begin
  ChildNodes['gender'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Familymembergender: Integer;
begin
  Result := ChildNodes['familymembergender'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Familymembergender(Value: Integer);
begin
  ChildNodes['familymembergender'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Education: WideString;
begin
  Result := ChildNodes['education'].Text;
end;

procedure TXMLEmployeeType.Set_Education(Value: WideString);
begin
  ChildNodes['education'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isunionmember: Integer;
begin
  Result := ChildNodes['isunionmember'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isunionmember(Value: Integer);
begin
  ChildNodes['isunionmember'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Race: Integer;
begin
  Result := ChildNodes['race'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Race(Value: Integer);
begin
  ChildNodes['race'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Unionname: WideString;
begin
  Result := ChildNodes['unionname'].Text;
end;

procedure TXMLEmployeeType.Set_Unionname(Value: WideString);
begin
  ChildNodes['unionname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Unionlocal: WideString;
begin
  Result := ChildNodes['unionlocal'].Text;
end;

procedure TXMLEmployeeType.Set_Unionlocal(Value: WideString);
begin
  ChildNodes['unionlocal'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactname: WideString;
begin
  Result := ChildNodes['emergencycontactname'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactname(Value: WideString);
begin
  ChildNodes['emergencycontactname'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Alternativecontactphone: Integer;
begin
  Result := ChildNodes['alternativecontactphone'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Alternativecontactphone(Value: Integer);
begin
  ChildNodes['alternativecontactphone'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddressline1: WideString;
begin
  Result := ChildNodes['emergencycontactaddressline1'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddressline1(Value: WideString);
begin
  ChildNodes['emergencycontactaddressline1'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddressline2: WideString;
begin
  Result := ChildNodes['emergencycontactaddressline2'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddressline2(Value: WideString);
begin
  ChildNodes['emergencycontactaddressline2'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddresscountry: WideString;
begin
  Result := ChildNodes['emergencycontactaddresscountry'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddresscountry(Value: WideString);
begin
  ChildNodes['emergencycontactaddresscountry'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddresszipcode: Integer;
begin
  Result := ChildNodes['emergencycontactaddresszipcode'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddresszipcode(Value: Integer);
begin
  ChildNodes['emergencycontactaddresszipcode'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddresscity: WideString;
begin
  Result := ChildNodes['emergencycontactaddresscity'].Text;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddresscity(Value: WideString);
begin
  ChildNodes['emergencycontactaddresscity'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Emergencycontactaddressphone: Integer;
begin
  Result := ChildNodes['emergencycontactaddressphone'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Emergencycontactaddressphone(Value: Integer);
begin
  ChildNodes['emergencycontactaddressphone'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Phonetype: WideString;
begin
  Result := ChildNodes['phonetype'].Text;
end;

procedure TXMLEmployeeType.Set_Phonetype(Value: WideString);
begin
  ChildNodes['phonetype'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Employeetype: Integer;
begin
  Result := ChildNodes['employeetype'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Employeetype(Value: Integer);
begin
  ChildNodes['employeetype'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Ismarried: Integer;
begin
  Result := ChildNodes['ismarried'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Ismarried(Value: Integer);
begin
  ChildNodes['ismarried'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Isoverrideholidayhours: Integer;
begin
  Result := ChildNodes['isoverrideholidayhours'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Isoverrideholidayhours(Value: Integer);
begin
  ChildNodes['isoverrideholidayhours'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Overriddenholidayhours: Integer;
begin
  Result := ChildNodes['overriddenholidayhours'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Overriddenholidayhours(Value: Integer);
begin
  ChildNodes['overriddenholidayhours'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Dateofchange: WideString;
begin
  Result := ChildNodes['dateofchange'].Text;
end;

procedure TXMLEmployeeType.Set_Dateofchange(Value: WideString);
begin
  ChildNodes['dateofchange'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Lunchbreakminutes: Integer;
begin
  Result := ChildNodes['lunchbreakminutes'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Lunchbreakminutes(Value: Integer);
begin
  ChildNodes['lunchbreakminutes'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Scheduledworkhoursperday: Integer;
begin
  Result := ChildNodes['scheduledworkhoursperday'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Scheduledworkhoursperday(Value: Integer);
begin
  ChildNodes['scheduledworkhoursperday'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Federaltaxmethod: Integer;
begin
  Result := ChildNodes['federaltaxmethod'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Federaltaxmethod(Value: Integer);
begin
  ChildNodes['federaltaxmethod'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Federalexemptions: Integer;
begin
  Result := ChildNodes['federalexemptions'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Federalexemptions(Value: Integer);
begin
  ChildNodes['federalexemptions'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Statetaxmethod: Integer;
begin
  Result := ChildNodes['statetaxmethod'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Statetaxmethod(Value: Integer);
begin
  ChildNodes['statetaxmethod'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Stateexemptions: Integer;
begin
  Result := ChildNodes['stateexemptions'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Stateexemptions(Value: Integer);
begin
  ChildNodes['stateexemptions'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Stateadditionalwithholdingamount: Integer;
begin
  Result := ChildNodes['stateadditionalwithholdingamount'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Stateadditionalwithholdingamount(Value: Integer);
begin
  ChildNodes['stateadditionalwithholdingamount'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Federalmaritalstatus: Integer;
begin
  Result := ChildNodes['federalmaritalstatus'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Federalmaritalstatus(Value: Integer);
begin
  ChildNodes['federalmaritalstatus'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Statemaritalstatus: Integer;
begin
  Result := ChildNodes['statemaritalstatus'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Statemaritalstatus(Value: Integer);
begin
  ChildNodes['statemaritalstatus'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycounty: WideString;
begin
  Result := ChildNodes['citycounty'].Text;
end;

procedure TXMLEmployeeType.Set_Citycounty(Value: WideString);
begin
  ChildNodes['citycounty'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Shift: WideString;
begin
  Result := ChildNodes['shift'].Text;
end;

procedure TXMLEmployeeType.Set_Shift(Value: WideString);
begin
  ChildNodes['shift'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Localhiredate: WideString;
begin
  Result := ChildNodes['localhiredate'].Text;
end;

procedure TXMLEmployeeType.Set_Localhiredate(Value: WideString);
begin
  ChildNodes['localhiredate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Reviewdate: WideString;
begin
  Result := ChildNodes['reviewdate'].Text;
end;

procedure TXMLEmployeeType.Set_Reviewdate(Value: WideString);
begin
  ChildNodes['reviewdate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountytaxmethod: Integer;
begin
  Result := ChildNodes['citycountytaxmethod'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Citycountytaxmethod(Value: Integer);
begin
  ChildNodes['citycountytaxmethod'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountyexemptions: Integer;
begin
  Result := ChildNodes['citycountyexemptions'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Citycountyexemptions(Value: Integer);
begin
  ChildNodes['citycountyexemptions'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountyadditionalwithholdingamount: Integer;
begin
  Result := ChildNodes['citycountyadditionalwithholdingamount'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Citycountyadditionalwithholdingamount(Value: Integer);
begin
  ChildNodes['citycountyadditionalwithholdingamount'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Federaladditionalwithholdingamount: Integer;
begin
  Result := ChildNodes['federaladditionalwithholdingamount'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Federaladditionalwithholdingamount(Value: Integer);
begin
  ChildNodes['federaladditionalwithholdingamount'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Citycountymaritalstatus: Integer;
begin
  Result := ChildNodes['citycountymaritalstatus'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Citycountymaritalstatus(Value: Integer);
begin
  ChildNodes['citycountymaritalstatus'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Terminationdate: WideString;
begin
  Result := ChildNodes['terminationdate'].Text;
end;

procedure TXMLEmployeeType.Set_Terminationdate(Value: WideString);
begin
  ChildNodes['terminationdate'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Terminationreason: WideString;
begin
  Result := ChildNodes['terminationreason'].Text;
end;

procedure TXMLEmployeeType.Set_Terminationreason(Value: WideString);
begin
  ChildNodes['terminationreason'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Companyid: WideString;
begin
  Result := ChildNodes['companyid'].Text;
end;

procedure TXMLEmployeeType.Set_Companyid(Value: WideString);
begin
  ChildNodes['companyid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Locationid: WideString;
begin
  Result := ChildNodes['locationid'].Text;
end;

procedure TXMLEmployeeType.Set_Locationid(Value: WideString);
begin
  ChildNodes['locationid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Divisionid: WideString;
begin
  Result := ChildNodes['divisionid'].Text;
end;

procedure TXMLEmployeeType.Set_Divisionid(Value: WideString);
begin
  ChildNodes['divisionid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Departmentid: WideString;
begin
  Result := ChildNodes['departmentid'].Text;
end;

procedure TXMLEmployeeType.Set_Departmentid(Value: WideString);
begin
  ChildNodes['departmentid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Positionid: WideString;
begin
  Result := ChildNodes['positionid'].Text;
end;

procedure TXMLEmployeeType.Set_Positionid(Value: WideString);
begin
  ChildNodes['positionid'].NodeValue := Value;
end;

function TXMLEmployeeType.Get_Canbecreated: Integer;
begin
  Result := ChildNodes['canbecreated'].NodeValue;
end;

procedure TXMLEmployeeType.Set_Canbecreated(Value: Integer);
begin
  ChildNodes['canbecreated'].NodeValue := Value;
end;

{ TXMLIntegerList }

function TXMLIntegerList.Add(const Value: Integer): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLIntegerList.Insert(const Index: Integer; const Value: Integer): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;
function TXMLIntegerList.Get_Item(Index: Integer): Integer;
begin
  Result := List[Index].NodeValue;
end;

end.