// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\job\IS\integration\ADI\api.wsdl
// Encoding : UTF-8
// Version  : 1.0
// (5/25/2009 1:51:39 AM - 1.33.2.5)
// ************************************************************************ //

unit api;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : http://www.aditime.com/webservices
  // soapAction: http://www.aditime.com/webservices/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : APISoap
  // service   : API
  // port      : APISoap
  // URL       : https://demo.aditime.com/evoapi/api/api.asmx
  // ************************************************************************ //
  APISoap = interface(IInvokable)
  ['{5E80C71F-5200-4FA3-EF4E-04C24065868C}']
    function  Login(const LoginID: WideString; const Password: WideString): WideString; stdcall;
    function  AuthenticateUser(const LoginID: WideString; const Password: WideString): WideString; stdcall;
    function  Logout(const SessionID: WideString): WideString; stdcall;
    function  CreateEmployee(const EmployeeXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  UpdateEmployee(const EmployeeIDXML: WideString; const EmployeeXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveEmployee(const EmployeeIDXML: WideString; const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllEmployees(const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  DeleteEmployee(const EmployeeIDXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveEmployeeMessages(const EmployeeIDXML: WideString; const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllLevels(const LevelType: WideString; const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateHierarchy(const LevelType: WideString; const LevelXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  UpdateHierarchy(const LevelType: WideString; const LevelIDXML: WideString; const LevelXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllPayGroups(const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllCodes(const Type_: WideString; const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllPayCalendar(const Year: WideString; const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateTimeEventPunch(const EmployeeIDXML: WideString; const PunchXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateTimePunch(const EmployeeIDXML: WideString; const PunchXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveTimePunch(const EmployeeIDXML: WideString; const PunchDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RequestPayrollData(const RequestXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveTimePunchByPeriod(const EmployeeIDXML: WideString; const PunchDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveTimePunchesByJobCodes(const JobIDsXML: WideString; const EmployeeIDsXML: WideString; const PunchStartDate: WideString; const PunchEndDate: WideString; const DetailsRequired: WideString; const SessionID: WideString): WideString; stdcall;
    function  DeleteTimePunch(const EmployeeIDXML: WideString; const PunchTime: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateMiscPay(const EmployeeIDXML: WideString; const MiscPayXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateTimeEventMiscPay(const EmployeeIDXML: WideString; const MiscPayXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveMiscPay(const EmployeeIDXML: WideString; const MiscPayDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveMiscPayByPeriod(const EmployeeIDXML: WideString; const MiscPayDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  DeleteMiscPay(const MiscPayKey: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveTimePunchesByHierarchy(const HierarchyXML: WideString; const PunchStartDate: WideString; const PunchEndDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllPayTypes(const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllUsers(const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateUser(const UserXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllMasterSchedules(const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllEmployeeSchedules(const PayPeriodDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllEmployeeSchedulesByDateRange(const StartDate: WideString; const EndDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveEmployeeSchedule(const EmployeeIDXML: WideString; const StartDate: WideString; const EndDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllEmployeeRoles(const SessionID: WideString): WideString; stdcall;
    function  CreateJob(const JobXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  UpdateJob(const JobIDXml: WideString; const JobXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveHolidaysAndSpecialDaysByDateRange(const StartDate: WideString; const EndDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllTimeCardUDF(const SessionID: WideString): WideString; stdcall;
    function  RetrieveSettings(const FieldSetXML: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveAllEmployeeLeaveRequest(const StartDate: WideString; const EndDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveEmployeeLeaveRequest(const EmployeeIDXML: WideString; const StartDate: WideString; const EndDate: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveBellSchedule(const BellScheduleName: WideString; const SessionID: WideString): WideString; stdcall;
    function  ResetClockBioTemplate(const EmployeeClockId: WideString; const ClockType: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveClockUsers(const ClockSiteId: WideString; const ClockId: WideString; const SessionID: WideString): WideString; stdcall;
    function  RetrieveClockBioTemplates(const ClockSiteId: WideString; const ClockId: WideString; const SessionID: WideString): WideString; stdcall;
    function  CreateClockBioTemplate(const EmployeeClockId: WideString; const ClockType: WideString; const TemplateData: WideString; const TemplateDate: WideString; const SessionID: WideString): WideString; stdcall;
  end;

function GetAPISoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): APISoap;


implementation

function GetAPISoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): APISoap;
const
  defWSDL = 'E:\job\IS\integration\ADI\api.wsdl';
  defURL  = 'https://demo.aditime.com/evoapi/api/api.asmx';
  defSvc  = 'API';
  defPrt  = 'APISoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as APISoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(APISoap), 'http://www.aditime.com/webservices', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(APISoap), 'http://www.aditime.com/webservices/%operationName%');
  InvRegistry.RegisterExternalParamName(TypeInfo(APISoap), 'RetrieveAllCodes', 'Type_', 'Type');

end. 