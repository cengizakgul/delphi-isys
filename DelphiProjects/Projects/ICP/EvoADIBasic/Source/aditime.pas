unit aditime;

interface

uses
  gdyCommonLogger, timeclockimport, evoapiconnectionutils,
  evoapiconnection, adidecl, adiconnection, gdystrset,
  bndUpdateEmployee, bndRetrieveAllUsers, bndEmployeeStatuses,
  bndRetrieveSettings, bndRetrieveAllEmployees, bndRequestPayrollData,
  bndRetrieveAllPayTypes, bndRetrieveAllEmployeescodes, bndStateMasters,
  kbmMemTable, common, gdyClasses, evodata;

type
  TADIDictionaries = class
  public
    constructor Create(conn: IADIConnection; logger: ICommonLogger);
    function DefEEStatus: string;
    function TermEEStatus: string;
    function DefAccrualGroup: string;
    function DefPointsGroup: string;
    function GetStateAbbrByNullableGUID(v: Variant): string;
//    function GetEEStatusesForClass(statusclass: string): TStringSet;
  private
    FConn: IADIConnection;
    FLogger: ICommonLogger;

    FDefEEStatus: Variant;
    FTermEEStatus: Variant;
    FDefAccrualGroup: Variant;
    FDefPointsGroup: Variant;

    FStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
    FGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
    FStates: bndStateMasters.IXMLStatemastersType;
    function States: bndStateMasters.IXMLStatemastersType;

    function GuessDefaultAccrualGroup: variant;
    function GuessDefaultPointsGroup: variant;
    function GuessDefEmployeeStatus: Variant;
    function GuessTermEmployeeStatus: Variant;
  end;

  TADIHierarchy = class
  public
    constructor Create(conn: IADIConnection; logger: ICommonLogger);
    procedure FilterEEsByCompany(company: TEvoCompanyDef; adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType);
    function GetGUIDByCode(level: TADILevel; code: string): string;
    function GetCodeByGUID(level: TADILevel; guid: string): string;
    function LevelInfo(level: TADILevel): THierarchyLevelInfo;
  private
  	FLogger: ICommonLogger;
    FLevels: array [TADILevel] of TADILevelRec;
  end;

  TEEUpdateAction = (euaCreate, euaUpdate, euaDelete);

  TChangedFieldRec = record
    FieldName: string;
    EvoValue: string;
    ADIValue: string;
  end;

  TChangedFieldRecs = array of TChangedFieldRec;

  TEEUpdateActionRec = record
    Action: TEEUpdateAction;
    Code: string;
    ChangedFields: TChangedFieldRecs;

    //the fields below are meaningful only for Create action
    //and useful only when ADI supervisors are enabled
    HasEvoSV: boolean;
    //if HasEvoSV is false then (after Analyze()) SVUserID may, or may not, contain hint for a user
    //and (when this record is passed to Apply()) SVUserID may, or may not,
    //contain userid of an ADI supervisor chosen by a user
    SVUserID: string;
  end;

  TEEUpdateActionRecs = array of TEEUpdateActionRec;

  TAnalyzeResult = record
    Errors: integer;
    Actions: TEEUpdateActionRecs;
    ADIHasSupervisors: boolean;
  end;

  TADIEmployeeUpdater = class
  public
    constructor Create(const param: TADIConnectionParam; const adiOptions: TADIOptions; EEData: TEvoEEData; logger: ICommonLogger);
    destructor Destroy; override;

    procedure CheckHierarchyMatch(evoDBDT: TEvoDBDTInfo);
    function Analyze: TAnalyzeResult;
    function CreateSVDataSet: TkbmCustomMemTable;
    function Apply(recs: TEEUpdateActionRecs; pi: IProgressIndicator): TUpdateEmployeesStat;

  private
    function BuildEmployee(creating: boolean): bndUpdateEmployee.IXMLEmployeeType;
    function DeleteEmployee(code: widestring): boolean;
    procedure AddDefaultValues(adiEE: bndUpdateEmployee.IXMLEmployeeType; userChosenSV: string );
    procedure CreateEmployee(userChosenSV: string);
    function SuggestRandomSVUserId: string;
    function ADIHasSupervisors: boolean;
    function MakeFakeSSN(eecode: string): string;
  private
    FLogger: ICommonLogger;
    FADIOptions: TADIOptions;
    FEEData: TEvoEEData;
    FConn: IADIConnection;
    FDict: TADIDictionaries;
    FInactiveAdiEEs: TStringSet; //IDs

  private
    FADISettings: bndRetrieveSettings.IXMLSettingsType;
    FUsers: bndRetrieveAllUsers.IXMLUsersType;
    FHierarchy: TADIHierarchy;
    function ADISettings: bndRetrieveSettings.IXMLSettingsType;
    function Users: bndRetrieveAllUsers.IXMLUsersType;
    function Hierarchy: TADIHierarchy;
  end;

function GetTimeClockData(logger: ICommonLogger; const param: TADIConnectionParam; const options: TADIOptions; company: TEvoCompanyDef; period: TPayPeriod): TTimeClockImportData;

function EEUpdateActionRecsToString(recs: TEEUpdateActionRecs): string;

implementation

uses
  gdycommon, gdyRedir, XMLIntf,
  sysutils, gdyGlobalWaitIndicator, variants,
  EvConsts, db, StrUtils;

function IsSV(user: bndRetrieveAllUsers.IXMLUserType): boolean;
begin
  //User Type check removed by Penn Gaines's request
  Result := {((user.Supervisortype.NodeValue = 2) or (user.Supervisortype.NodeValue = 3))
    and} (user.Isactive.NodeValue = 1);
end;

function FindSVUserIdByEvoName(logger: ICommonLogger; users: bndRetrieveAllUsers.IXMLUsersType; evoSupervisorName: string): string;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
begin
  logger.LogEntry('Finding ADI supervisor''s user ID by Evolution supervisor name');
  try
    try
      logger.LogContextItem('Evolution supervisor name', evoSupervisorName);
      for i := 0 to users.Count-1 do
      begin
        user := users[i];
        logger.LogEntry('Checking user');
        try
          try
            logger.LogContextItem('user.Username', UTF8Encode(user.Username) );
            logger.LogContextItem('user.Supervisortype', VarToStr(user.Supervisortype.NodeValue) );
            logger.LogContextItem('user.Isactive', VarToStr(user.Isactive.NodeValue) );
            if IsSV(user) then
            begin
              if AnsiSameText(trim(user.Username), trim(evoSupervisorName)) then
              begin
                Result := user.Userid; //not supervisorid!
                logger.LogDebug('Found!');
                Exit;
              end
              else
                logger.LogDebug('Name doesn''t match');
            end
            else
              logger.LogDebug('This is not an active supervisor or manager');
          except
            logger.PassthroughException;
          end;
        finally
          logger.LogExit;
        end;
      end;

      raise Exception.CreateFmt('Cannot find ADI supervisor with user name <%s>', [evoSupervisorName]);
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

function GetSVCodeByGuid(users: bndRetrieveAllUsers.IXMLUsersType; guid: string): string;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
begin
  Result := '';
  guid := trim(guid);
  for i := 0 to users.Count-1 do
  begin
    user := users[i];
    if AnsiSameText(trim(user.Id), guid) then
    begin
      Result := user.Userid; //not supervisorid!
      Exit;
    end
  end;
  raise Exception.CreateFmt('Cannot find ADI supervisor by internal id (%s)', [guid]);
end;

{ TADIConnectionHierachy }

constructor TADIHierarchy.Create(conn: IADIConnection; logger: ICommonLogger);
var
  i: TADILevel;
begin
  FLogger := logger;
  for i := low(TADILevel) to high(TADILevel) do
    FLevels[i] := conn.RetrieveAllLevels(i);
end;

procedure TADIHierarchy.FilterEEsByCompany(company: TEvoCompanyDef; adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType);
var
  i: integer;
  companyGuid: string;
begin
  FLogger.LogEntry('Filtering employees by company');
  try
    try
      LogEvoCompanyDef(FLogger, company);
      companyGuid := GetGUIDByCode(adiCompany, company.CUSTOM_COMPANY_NUMBER);
      FLogger.LogContextItem('ADI company''s GUID', companyGuid);
      i := 0;
      while i < adiEEs.Count do
        if AnsiSameText(trim(adiEEs[i].Company), companyGuid) then
          i := i + 1
        else
          adiEEs.Delete(i);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIHierarchy.GetGUIDByCode(level: TADILevel; code: string): string;
var
  i: integer;
  lev: IXMLNodeList;
begin
  Result := '';
  code := trim(code);
  if FLevels[level].Data = nil then
    raise Exception.CreateFmt('ADI %s were not retrieved. ADI response was %s', [Plural(FLevels[level].Name), FLevels[level].DisabledReason]);
  if FLevels[level].Data.DocumentElement <> nil then //ADI sometimes returns empty documents instead of empty top-level tag
  begin
    lev := FLevels[level].Data.DocumentElement.ChildNodes;
    for i := 0 to lev.Count-1 do
    begin
      if trim(lev[i].ChildValues[FLevels[level].Name + 'id']) = code then
      begin
        Result := AnsiLowerCase(trim( lev[i].ChildValues['id'] ));
        exit;
      end
    end;
  end;
  raise Exception.CreateFmt('Cannot find ADI %s with code <%s>', [FLevels[level].Name, code]);
end;

function TADIHierarchy.GetCodeByGUID(level: TADILevel; guid: string): string;
var
  i: integer;
  lev: IXMLNodeList;
begin
  Result := '';
  guid := trim(guid);
  if FLevels[level].Data = nil then
    raise Exception.CreateFmt('ADI %s were not retrieved. ADI response was <%s>', [Plural(FLevels[level].Name), FLevels[level].DisabledReason]);
  if FLevels[level].Data.DocumentElement <> nil then //ADI sometimes returns empty documents instead of empty top-level tag
  begin
    lev := FLevels[level].Data.DocumentElement.ChildNodes;
    for i := 0 to lev.Count-1 do
    begin
      if AnsiSameText(trim(lev[i].ChildValues['id']), guid) then
      begin
        Result := trim( lev[i].ChildValues[FLevels[level].Name+'id'] );
        exit;
      end
    end;
  end;
  raise Exception.CreateFmt('Cannot find ADI %s with id <%s>', [FLevels[level].Name, guid]);
end;

function TADIHierarchy.LevelInfo(level: TADILevel): THierarchyLevelInfo;
begin
  Result.Name := FLevels[level].Name;
  Result.Enabled := FLevels[level].Data <> nil;
  Result.Reason := FLevels[level].DisabledReason;
end;

function GetTimeClockData(logger: ICommonLogger; const param: TADIConnectionParam; const options: TADIOptions; company: TEvoCompanyDef; period: TPayPeriod): TTimeClockImportData;

  function ConvertToEvoTC(data: bndRequestPayrollData.IXMLResponseType; paytypes: IXMLPaytypesType ): TTimeClockRecs;
    function PayTypeByGUID(guid: string; code: string): IXMLPaytypeType;
    var
      i: integer;
    begin
      guid := trim(guid);
      for i := 0 to paytypes.Count-1 do
        if AnsiSameText(trim(paytypes[i].Id), guid) then
        begin
          Result := paytypes[i];
          exit;
        end;
      raise Exception.CreateFmt('Cannot find ADI pay type <%s> with id <%s> in the list of ADI pay types',[code, guid]);
    end;
  var
    i: integer;
    segments: IXMLPayrollsegmentsType;
    segment: IXMLPayrollsegmentType;
    cur: PTimeClockRec;
  begin
    SetLength(Result, 0);
    segments := data.Payrollsegments;
    for i := 0 to segments.Count-1 do
    begin
      logger.LogEntry('Converting ADI payroll segment to Evolution format');
      try
        try
          segment := segments[i];
          logger.LogContextItem('Payroll segment key', segment.payrollsegmentkey);
          if (segment.ChildNodes.FindNode('companykey') <> nil) and (trim(VarToStr(segment.companykey.Id)) <> trim(company.CUSTOM_COMPANY_NUMBER)) then
            continue;
          logger.LogContextItem('Report date', segment.Reportdate);
          logger.LogContextItem(sCtxEECode, segment.employeekey.id);

          SetLength(Result, Length(Result)+1 );  //new element is initialized with zeros
          cur := @Result[high(Result)];
          cur.CUSTOM_EMPLOYEE_NUMBER := trim(segment.Employeekey.Id);

          if VarHasValue(segment.ChildValues['hours']) and (abs(segment.Hours) > eps) then
          begin
            cur.Hours := segment.Hours;
            cur.EDCode := PayTypeByGUID(segment.paytypekey.NodeValue, segment.paytypekey.id).hourscode;
            if options.TransferTempRate then
              cur.Rate := segment.Basepayrate;
          end
          else
          begin
            cur.Amount := segment.Amount;
            cur.EDCode := PayTypeByGUID(segment.paytypekey.NodeValue, segment.paytypekey.id).Dollarscode
          end;

          cur.PunchDate := StrToDate(segment.Reportdate);
          if trim(VarToStr(segment.startdatetime.NodeValue)) <> '' then
            cur.PunchIn := StdDateTimeToDateTime(segment.startdatetime.NodeValue);
          if trim(VarToStr(segment.Enddatetime.NodeValue)) <> '' then
            cur.PunchOut := StdDateTimeToDateTime(segment.Enddatetime.NodeValue);

          if options.TransferTempHierarchy then
          begin
            if segment.ChildNodes.FindNode('locationkey') <> nil then
              cur.Division := VarToStr(segment.Locationkey.Id);
            if segment.ChildNodes.FindNode('divisionkey') <> nil then
              cur.Branch := VarToStr(segment.Divisionkey.Id);
            if segment.ChildNodes.FindNode('departmentkey') <> nil then
              cur.Department := VarToStr(segment.Departmentkey.Id);
            if segment.ChildNodes.FindNode('positionkey') <> nil then
              cur.Team := VarToStr(segment.Positionkey.Id);
          end;

          cur.Comment := VarToStr(segment.Comments);
        except
          logger.StopException;
        end;
      finally
        logger.LogExit;
      end;
    end
  end;

var
  conn: IADIConnection;
  recs: TTimeClockRecs;
begin
  SetLength(recs, 0);
  logger.LogEntry('Getting timeclock data');
  try
    try
      conn := CreateADIConnection(param, logger);
      recs := ConvertToEvoTC(conn.RequestPayrollData(period), conn.RetrieveAllPayTypes);
      if options.Summarize then
        recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
      Result := BuildTimeClockImportData(recs, false, false, logger);
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

{ TADIDictionaries }

constructor TADIDictionaries.Create(conn: IADIConnection; logger: ICommonLogger);
begin
  FConn := conn;
  FLogger := logger;
end;

function TADIDictionaries.GuessDefEmployeeStatus: Variant;
var
  i: integer;
  descr: string;
begin
  Result := Null;
  if FStatuses = nil then
    FStatuses := bndEmployeeStatuses.Getemployeestatuses( FConn.RetrieveAllCodes(6) );

  for i := 0 to FStatuses.Count-1 do
    if AnsiSameText(trim(FStatuses[i].Statusclass.Name), 'Active') and
       AnsiSameText(trim(FStatuses[i].Description), 'Active') then
      begin
        Result := trim(FStatuses[i].Code);
        Exit; //no warnings, no exceptions
      end;
  if VarIsNull(Result) then
  begin
    for i := 0 to FStatuses.Count-1 do
      if AnsiSameText(trim(FStatuses[i].Statusclass.Name), 'Active') then
        begin
          Result := trim(FStatuses[i].Code);
          descr := FStatuses[i].Description;
          break;
        end;
  end;
  if VarIsNull(Result) then
    if FStatuses.Count > 0 then
    begin
      Result := trim(FStatuses[0].Code);
      descr := FStatuses[0].Description;
    end;
  if VarIsNull(Result) then
    raise Exception.Create('No employee status codes defined in ADI')
  else
    FLogger.LogWarningFmt('Using <%s> as default employee status', [descr]);
end;

function TADIDictionaries.GuessTermEmployeeStatus: Variant;
var
  i: integer;
begin
  Result := Null;
  if FStatuses = nil then
    FStatuses := bndEmployeeStatuses.Getemployeestatuses( FConn.RetrieveAllCodes(6) );
  for i := 0 to FStatuses.Count-1 do
    if AnsiSameText(trim(FStatuses[i].Statusclass.Name), 'Terminated') and AnsiSameText(trim(FStatuses[i].Description), 'Terminated') then
      Result := trim(FStatuses[i].Code);
  if VarIsNull(Result) then
  begin
    for i := 0 to FStatuses.Count-1 do
      if AnsiSameText(trim(FStatuses[i].Statusclass.Name), 'Terminated') then
        begin
          Result := trim(FStatuses[i].Code);
          FLogger.LogWarningFmt('Using <%s> as deleted employee status', [FStatuses[i].Description]);
        end;
  end;
  if VarIsNull(Result) then
    raise Exception.Create('No employee status codes with class <Terminated> defined in ADI');
end;

function TADIDictionaries.GuessDefaultAccrualGroup: variant;
var
  accrualGroups: bndRetrieveAllEmployeescodes.IXMLAccrualgroupsType;
  i: integer;
begin
  Result := 'STD';  //cannot get any other id because id field actually contains guid here
  if FGroups = nil then
    FGroups := FConn.GetGroups;
  accrualGroups := FGroups.Accrualgroups;
  for i := 0 to accrualGroups.Count-1 do
    if AnsiSameText(trim(accrualGroups[i].Name), 'Standard Accruals') or
       AnsiSameText(trim(accrualGroups[i].Name), 'Standard Accrual') then //id is guid here so use name
      exit; //no warnings
  FLogger.LogWarning('Cannot find ADI accrual group named "Standard Accruals" or "Standard Accrual". Using ID "STD" as default accrual group ID anyway');
end;

function TADIDictionaries.GuessDefaultPointsGroup: variant;
var
  pointsGroups: bndRetrieveAllEmployeescodes.IXMLPointsgroupType;
  i: integer;
begin
  if FGroups = nil then
    FGroups := FConn.GetGroups;
  pointsGroups := FGroups.Pointsgroup;
  for i := 0 to pointsGroups.Count-1 do
    if AnsiSameText(trim(pointsGroups[i].Id), 'NO POINTS') then //id is custom ID here!
    begin
      Result := trim(pointsGroups[i].Id);
      exit; //no warnings
    end;
  if pointsGroups.Count > 0 then
  begin
    Result := pointsGroups[0].Id;
    FLogger.LogWarningFmt('Using <%s> as default points group', [pointsGroups[0].Name]);
  end
  else
    raise Exception.Create('No points groups defined in ADI')
end;

function TADIDictionaries.DefEEStatus: string;
begin
  if VarIsEmpty(FDefEEStatus) then
    FDefEEStatus := GuessDefEmployeeStatus;
  Result := FDefEEStatus;
end;

function TADIDictionaries.DefAccrualGroup: string;
begin
  if VarIsEmpty(FDefAccrualGroup) then
    FDefAccrualGroup := GuessDefaultAccrualGroup;
  Result := FDefAccrualGroup;
end;

function TADIDictionaries.DefPointsGroup: string;
begin
  if VarIsEmpty(FDefPointsGroup) then
    FDefPointsGroup := GuessDefaultPointsGroup;
  Result := FDefPointsGroup;
end;

function TADIDictionaries.TermEEStatus: string;
begin
  if VarIsEmpty(FTermEEStatus) then
    FTermEEStatus := GuessTermEmployeeStatus;
  Result := FTermEEStatus;
end;
 {
function TADIDictionaries.GetEEStatusesForClass(statusclass: string): TStringSet;
var
  i: integer;
begin
  if FStatuses = nil then
    FStatuses := bndEmployeeStatuses.Getemployeestatuses( FConn.RetrieveAllCodes(6) );
  for i := 0 to FStatuses.Count-1 do
    if AnsiSameText(trim(FStatuses[i].Statusclass.Name), statusclass) then
      SetInclude( Result, trim(FStatuses[i].Code) );
end;
}

function TADIDictionaries.GetStateAbbrByNullableGUID(v: Variant): string;
var
  i: integer;
  guid: string;
begin
  guid := trim(VarToStr(v));
  if guid = '' then
  begin
    Result := '';
    exit;
  end;
  for i := 0 to States.Count-1 do
    if AnsiSameText(trim(States[i].id), guid) then
      begin
        Result := trim(States[i].Abbreviation);
        Exit;
      end;
  raise Exception.CreateFmt('Cannot find ADI state master record by internal id <%s>', [guid]);
end;

function TADIDictionaries.States: bndStateMasters.IXMLStatemastersType;
begin
  if FStates = nil then
    FStates := bndStateMasters.Getstatemasters( FConn.RetrieveAllCodes(7) );
  Result := FStates;
end;

{ TADIEmployeeUpdater }

constructor TADIEmployeeUpdater.Create(const param: TADIConnectionParam;
  const adiOptions: TADIOptions; EEData: TEvoEEData;
  logger: ICommonLogger);
begin
  FLogger := logger;
  FADIOptions := adiOptions;
  FEEData := EEData;
  FConn := CreateADIConnection(param, FLogger);
  FDict := TADIDictionaries.Create(FConn, FLogger);
end;

function TADIEmployeeUpdater.ADISettings: bndRetrieveSettings.IXMLSettingsType;
begin
  if FADISettings = nil then
  begin
    FADISettings := FConn.RetrieveSettings;
  end;
  Result := FADISettings;
end;

function TADIEmployeeUpdater.Users: bndRetrieveAllUsers.IXMLUsersType;
begin
  if FUsers = nil then
  begin
    Assert(FConn<>nil);
    FUsers := FConn.RetrieveAllUsers;
  end;
  Result := FUsers;
end;

function ComposeShortName(last, first, mi: string): string;
begin
  // was firstname + ' ' + lastname;
  Result := last + ', ' + first;
  if trim(mi) <> '' then
    Result := Result + ' ' + mi + '.';
end;

function ComposeInitials(last, first, mi: string): string;
begin
  Result := copy(first, 1, 1) + copy(mi, 1, 1) + copy(last, 1, 1);
end;

function TADIEmployeeUpdater.BuildEmployee(creating: boolean): bndUpdateEmployee.IXMLEmployeeType;
  function NullableDateToStdDate(d: Variant): string;
  begin
    if not VarIsNull(d) then
      Result := DateToStdDate(d)
    else
      Result := '';
  end;
var
  rates: TRateArray;
  firstname, lastname, mi: string;
  i: integer;
begin
  Result := bndUpdateEmployee.Newemployee;
  //doNodeAutoCreate is On by default, AreDifferent may add empty nodes to the ee xml
  // so call this function again when creating or updating employee

  lastname := trim(FEEData.EEs['LAST_NAME']);
  firstname := trim(FEEData.EEs['FIRST_NAME']);
  mi := trim(FEEData.EEs.FieldByName('MIDDLE_INITIAL').AsString); //optional in Evo

  if (fteLastName in FADIOptions.FieldsToExport) or creating then
  Result.Lastname := lastname;
  if (fteFirstName in FADIOptions.FieldsToExport) or creating then
  Result.Firstname := firstname;
  if (fteMiddleInitial in FADIOptions.FieldsToExport) or creating then
    Result.Middleinitial := mi;
  if (fteShortName in FADIOptions.FieldsToExport) or creating then
    Result.Shortname := ComposeShortName(lastname, firstname, mi);
  if (fteInitials in FADIOptions.FieldsToExport) or creating then
  Result.Initials := ComposeInitials(lastname, firstname, mi);

  if fteSSN in FADIOptions.FieldsToExport then //if it is off then never export
    Result.Ssn := FEEData.EEs['SOCIAL_SECURITY_NUMBER']
  else if creating then
    Result.Ssn := MakeFakeSSN(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);

  Result.Employeeidnumber := FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER'];
  if fteRates in FADIOptions.FieldsToExport then //if they are off then never export
  begin
    rates := FEEData.GetEERatesArray;
    Result.Primaryrate := FloatToStr(rates[0]);
    Result.Alternaterate1 := FloatToStr(rates[1]);
    Result.Alternaterate2 := FloatToStr(rates[2]);
  end;

  if (fteCompany in FADIOptions.FieldsToExport) or creating then
  if Hierarchy.LevelInfo(adiCompany).Enabled then
    Result.Companyid := FEEData.Company.CUSTOM_COMPANY_NUMBER;

  if (fteLocationADI in FADIOptions.FieldsToExport) or creating then
  if not VarIsNull(FEEData.EEs['CUSTOM_DIVISION_NUMBER'])then
    Result.Locationid := FEEData.EEs['CUSTOM_DIVISION_NUMBER'];

  if (fteDivisionADI in FADIOptions.FieldsToExport) or creating then
  if not VarIsNull(FEEData.EEs['CUSTOM_BRANCH_NUMBER']) then
    Result.Divisionid := FEEData.EEs['CUSTOM_BRANCH_NUMBER'];

  if (fteDepartmentADI in FADIOptions.FieldsToExport) or creating then
  if not VarIsNull(FEEData.EEs['CUSTOM_DEPARTMENT_NUMBER']) then
    Result.Departmentid := FEEData.EEs['CUSTOM_DEPARTMENT_NUMBER'];

  if (ftePositionADI in FADIOptions.FieldsToExport) or creating then
  if not VarIsNull(FEEData.EEs['CUSTOM_TEAM_NUMBER']) then
    Result.Positionid := FEEData.EEs['CUSTOM_TEAM_NUMBER'];

  //we can't clear ADI supervisor field so if this Evo employee doesn't have supervisor then
  //we assume that we either don't have HR enabled or just don't care about supervisors
  if (fteSupervisor in FADIOptions.FieldsToExport) or creating then
    if ADIHasSupervisors and (trim(FEEData.EEs.FieldByName('supervisor_name').AsString)<>'') then
      Result.Supervisor := FindSVUserIdByEvoName(FLogger, Users, FEEData.EEs.FieldByName('supervisor_name').AsString);

  //we can't clear this field in ADI
  if (fteBadgeID_ClockID in FADIOptions.FieldsToExport) or creating then
    if trim(FEEData.EEs.FieldByName('BADGE_ID').AsString) <> '' then
      Result.Timeclocknumber := FEEData.EEs['BADGE_ID'];

  if (fteAddressline1 in FADIOptions.FieldsToExport) or creating then
    Result.Addressline1 := FEEData.EEs['ADDRESS1'];
  if (fteAddressline2 in FADIOptions.FieldsToExport) or creating then
    Result.Addressline2 := FEEData.EEs.FieldByName('ADDRESS2').AsString; //optional in Evo
  if (fteCity in FADIOptions.FieldsToExport) or creating then
    Result.City := FEEData.EEs['CITY'];
  if (fteState in FADIOptions.FieldsToExport) or creating then
    Result.State := FEEData.EEs['STATE'];
  if (fteZipCode in FADIOptions.FieldsToExport) or creating then
    Result.Zipcode:= FEEData.EEs.FieldByName('ZIP_CODE').AsString; //let ADI deal with non-integer values

  if (ftePhone in FADIOptions.FieldsToExport) or creating then
    Result.Phone := FEEData.EEs.FieldByName('PHONE1').AsString; //optional in Evo
  if (fteEmailAddress in FADIOptions.FieldsToExport) or creating then
    Result.Emailaddress := FEEData.EEs.FieldByName('E_MAIL_ADDRESS').AsString; //optional in Evo
  if (fteHireDate in FADIOptions.FieldsToExport) or creating then
    Result.Hiredate := DateToStdDate(FEEData.EEs['CURRENT_HIRE_DATE']);
  if (fteBirthDate in FADIOptions.FieldsToExport) or creating then
    Result.Birthdate := NullableDateToStdDate(FEEData.EEs['BIRTH_DATE']);
  if (fteReviewDate in FADIOptions.FieldsToExport) or creating then
    Result.Reviewdate := NullableDateToStdDate(FEEData.EEs['REVIEW_DATE']);

  if (fteSalaried in FADIOptions.FieldsToExport) or creating then
    Result.Issalaried := ord(FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat > 0);
//  Result.Salary := FloatToStr(FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat);

  for i := 0 to Result.childNodes.Count-1 do
    if not VarIsNull(Result.childNodes[i].NodeValue) then //all assignments are done via typed wrapper so this can't happen, but still...
      Result.childNodes[i].NodeValue := trim(Result.childNodes[i].NodeValue);
end;

function TADIEmployeeUpdater.DeleteEmployee(code: widestring): boolean;
  procedure ChangeStatus;
  var
    ee: bndUpdateEmployee.IXMLEmployeeType;
    dt: string;
  begin
    if FEEData.LocateEE(code) and not FEEData.EEs.FieldByName('CURRENT_TERMINATION_DATE').IsNull then
      dt := DateToStdDate(FEEData.EEs['CURRENT_TERMINATION_DATE'])
    else
      dt := DateToStdDate(Now);
    ee := bndUpdateEmployee.Newemployee;
    ee.Employeeidnumber := code;
    ee.Employeestatus := FDict.TermEEStatus;
    ee.Terminationdate := dt;
    FConn.UpdateEmployee(ee);
    Result := false;
  end;
begin
  if FADIOptions.AllowDeletionOfEE then
  try
    FConn.DeleteEmployee( code );
    Result := true;
  except
    on E: Exception do
    begin
      FLogger.LogWarningFmt('Cannot delete ADI employee <%s>, changing status instead', [code], e.Message);
      ChangeStatus;
    end
  end
  else
  begin
    FLogger.LogWarningFmt('Not allowed to delete ADI employee <%s>, changing status instead', [code]);
    ChangeStatus;
  end;
end;

procedure TADIEmployeeUpdater.AddDefaultValues( adiEE: bndUpdateEmployee.IXMLEmployeeType; userChosenSV: string );
begin
  adiEE.Employeestatus := FDict.DefEEStatus;

  if ADISettings.Isaccrualsenabled <> 0 then
    adiEE.Accrualgroup := FDict.DefAccrualGroup;

  if ADISettings.Ispointsenabled <> 0 then
    adiEE.Pointsgroup := FDict.DefPointsGroup; //FLogger.LogWarning('Points are not supported');

  adiEE.Paygroup := FADIOptions.DefaultPayGroupCode;

  if ADIHasSupervisors and (adiEE.ChildNodes.FindNode('supervisor') = nil) then
  begin
    //this Evo EE don't have supervisor and we have asked the user to choose one
    if trim(userChosenSV) <> '' then
      adiEE.Supervisor := userChosenSV;
  end;
end;

function TADIEmployeeUpdater.MakeFakeSSN(eecode: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to length(eecode) do
    if eecode[i] in ['0'..'9'] then
      Result := Result + copy(eecode, i, 1);
  if Length(Result) > 9 then
    Result := RightStr(Result, 9)
  else if Length(Result) < 9 then
    Result := StringOfChar('0', 9 - Length(Result)) + Result;
  Result := Format( '%s-%s-%s', [copy(Result,1, 3), copy(Result,4, 2), copy(Result,6, 4)] );
end;

procedure TADIEmployeeUpdater.CreateEmployee(userChosenSV: string);
var
  adiEE: bndUpdateEmployee.IXMLEmployeeType;
begin
  if not InSet(trim(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']), FInactiveAdiEEs) then
  begin
    adiEE := BuildEmployee(true);
    AddDefaultValues(adiEE, userChosenSV);
    FConn.CreateEmployee(adiEE);
  end
  else
  begin
    adiEE := BuildEmployee(false);
    adiEE.Employeestatus := FDict.DefEEStatus;
    FConn.UpdateEmployee( adiEE );
  end;
end;

function TADIEmployeeUpdater.SuggestRandomSVUserId: string;
var
  EeHierarchyGuids: TStringSet;

  procedure AddLevelGuid(level: TADILevel; fn: string);
  begin
    if not VarIsNull(FEEData.EEs[fn]) then
      SetInclude( EeHierarchyGuids, Hierarchy.GetGUIDByCode(level, FEEData.EEs[fn]) );
  end;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
  j: integer;
begin
  if hierarchy.LevelInfo(adiCompany).Enabled then
    SetInclude( EeHierarchyGuids, hierarchy.GetGUIDByCode(adiCompany, FEEData.Company.CUSTOM_COMPANY_NUMBER) );
  AddLevelGuid( adiLocation, 'CUSTOM_DIVISION_NUMBER' );
  AddLevelGuid( adiDivision, 'CUSTOM_BRANCH_NUMBER' );
  AddLevelGuid( adiDepartment, 'CUSTOM_DEPARTMENT_NUMBER' );
  AddLevelGuid( adiPosition, 'CUSTOM_TEAM_NUMBER' );

  for i := 0 to users.Count-1 do
  begin
    user := users[i];
    if IsSV(user) then
    begin
      if user.ChildNodes.FindNode('assignablelevel') <> nil then
      begin
        with SplitByChar(VarToStr(user.Assignablelevel.NodeValue), ';') do
          for j := 0 to Count-1 do
            if InSet(trim(Str[j]), EeHierarchyGuids) then
            begin
              Result := user.Userid; //not supervisorid!
              Exit;
            end;
      end
    end
  end;
end;

function ChangedFieldRecsToBriefString(changedFields: TChangedFieldRecs): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(changedFields) do
  begin
    if Result <> '' then
      Result := Result + ', ';
    Result := Result + changedFields[i].FieldName;
  end
end;

function ChangedFieldRecsToDetailedString(changedFields: TChangedFieldRecs): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(changedFields) do
  begin
    if Result <> '' then
      Result := Result + #13#10;
    Result := Result + Format( '%s, old value: "%s", new value: "%s"', [changedFields[i].FieldName,changedFields[i].ADIValue, changedFields[i].EvoValue]);
  end
end;


function EEUpdateActionRecsToString(recs: TEEUpdateActionRecs): string;
  function GetListOfEECodesForAction(act: TEEUpdateAction): string;
  var
    i: integer;
  begin
    Result := '';
    for i := 0 to high(recs) do
      if recs[i].Action = act then
      begin
        if Result <> '' then
          Result := Result + ', ';
        Result := Result + recs[i].Code;
      end;
  end;
var
  i: integer;
  s: string;
begin
  Result := '';
  s := GetListOfEECodesForAction(euaCreate);
  if s <> '' then
    Result := Result + 'ADI records will be CREATED for the following employees: ' + s + #13#10#13#10#13#10;

  s := '';
  for i := 0 to high(recs) do
    if recs[i].Action = euaUpdate then
    begin
      if s <> '' then
        s := s + #13#10#13#10;
      s := s + 'Employee # ' + recs[i].Code + ':'#13#10 + ChangedFieldRecsToDetailedString(recs[i].ChangedFields);
    end;
  if s <> '' then
    Result := Result + 'ADI records will be UPDATED for the following employees: '#13#10#13#10 + s + #13#10#13#10#13#10;

  s := GetListOfEECodesForAction(euaDelete);
  if s <> '' then
    Result := Result + 'ADI records will be DELETED for the following employees: ' + s + #13#10#13#10#13#10;
end;

function TADIEmployeeUpdater.Analyze: TAnalyzeResult;
var
  adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType;

  function IsEEInADI(code: string): boolean;
  var
    i:integer;
  begin
    Result := false;
    code := trim(code);
    for i := 0 to adiEEs.Count-1 do
      if trim(adiEEs[i].Employeeidnumber) = code then
      begin
        Result := true;
        Exit;
      end;
  end;

  //expects that adiEE's doNodeAutoCreate is On and adds empty nodes to adiEE
  function CompareFields(adiEE: bndRetrieveAllEmployees.IXMLEmployeeType): TChangedFieldRecs;
  var
    evoEE: bndUpdateEmployee.IXMLEmployeeType;

    procedure AddChangedFields(adiS, evoS, fieldName: string);
    begin
      SetLength(Result, Length(Result)+1);
      Result[high(Result)].FieldName := fieldName;
      Result[high(Result)].ADIValue := adiS;
      Result[high(Result)].EvoValue := evoS;
    end;

    procedure compare(v1, v2: Variant; fte: TFieldToExport);
    begin
      if fte in FADIOptions.FieldsToExport then
        if trim(VarToStr(v1)) <> trim(VarToStr(v2)) then
          AddChangedFields(VarToStr(v1), VarToStr(v2), FieldExportInfos[fte].UserFriendlyName)
    end;
    procedure compareFloat(s1, s2: string; fieldName: string);
    var
      diff: boolean;
    begin
      diff := abs(StrToFloat(s1) - StrToFloat(s2)) > 0.01; //ADI rounds to 2 digits
      if diff then
        AddChangedFields(s1, s2, fieldName)
    end;
    procedure compareSSN(s1, s2: string; fieldName: string);
    var
      x1, x2: string;
      diff: boolean;
    begin
      x1 := StringReplace(s1, '-', '', [rfReplaceAll]);
      x2 := StringReplace(s2, '-', '', [rfReplaceAll]);
      diff := trim(x1) <> trim(x2);
      if diff then
        AddChangedFields(s1, s2, fieldName)
    end;
    function GetAdiEELevelCode(level: TADILevel): string;
    var
      fn: string;
    begin
      fn := ADILevelNames[level];
      if adiEE.ChildNodes.FindNode(fn) <> nil then
        Result := Hierarchy.GetCodeByGUID(level, adiEE[fn])
      else
        Result := '';
    end;
    procedure compareDateTimeAndDate(v1, v2: Variant; fte: TFieldToExport);
    var
      d1, d2: string;
      e1, e2: boolean;
      diff: boolean;
      s1, s2: string;
    begin
      if not (fte in FADIOptions.FieldsToExport) then
        exit;

      d1 := trim(VarToStr(v1));
      if d1 = '0001-01-01T00:00:00' then
        d1 := '';
      d2 := trim(VarToStr(v2));
      e1 := d1 = '';
      e2 := d2 = '';
      diff := (e1 and e2) or (not e1 and not e2 and (StdDateTimeToDateTime(d1) = StdDateToDate(d2)) );
      diff := not diff;
      if diff then
      begin
        if d1 <> '' then
          s1 := DateTimeToStr(StdDateTimeToDateTime(d1))
        else
          s1 := '';
        if d2 <> '' then
          s2 := DateTimeToStr(StdDateToDate(d2))
        else
          s2 := '';
        AddChangedFields(s1, s2, FieldExportInfos[fte].UserFriendlyName);
      end
    end;
  begin
    Assert( trim(FEEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(adiEE.Employeeidnumber) );

    evoEE := BuildEmployee(false);

    SetLength(Result, 0);

    compare(adiEE.Lastname, EvoEE.Lastname, fteLastName);
    compare(adiEE.Firstname, EvoEE.Firstname, fteFirstName);
    compare(adiEE.Middleinitial, EvoEE.Middleinitial, fteMiddleInitial);
    compare(adiEE.Shortname, EvoEE.Shortname, fteShortName);
    if fteSSN in FADIOptions.FieldsToExport  then
      compareSSN(adiEE.Ssn, EvoEE.Ssn, 'SSN');
    if fteRates in FADIOptions.FieldsToExport then
    begin
      compareFloat(adiEE.Primaryrate, EvoEE.Primaryrate, 'Primary rate');
      compareFloat(adiEE.Alternaterate1, EvoEE.Alternaterate1, 'Alternate rate 1');
      compareFloat(adiEE.Alternaterate2, EvoEE.Alternaterate2, 'Alternate rate 2');
    end;

    if EvoEE.ChildNodes.FindNode('companyid') <> nil then
      compare(GetAdiEELevelCode(adiCompany), EvoEE.Companyid, fteCompany);
    if EvoEE.ChildNodes.FindNode('locationid') <> nil then
      compare(GetAdiEELevelCode(adiLocation), EvoEE.Locationid, fteLocationADI);
    if EvoEE.ChildNodes.FindNode('divisionid') <> nil then
      compare(GetAdiEELevelCode(adiDivision), EvoEE.Divisionid, fteDivisionADI);
    if EvoEE.ChildNodes.FindNode('departmentid') <> nil then
      compare(GetAdiEELevelCode(adiDepartment), EvoEE.Departmentid, fteDepartmentADI);
    if EvoEE.ChildNodes.FindNode('positionid') <> nil then
      compare(GetAdiEELevelCode(adiPosition), EvoEE.Positionid, ftePositionADI);

    //we can't clear ADI supervisor field so if an Evo employee doesn't have supervisor then
    //we assume that we either don't have HR enabled or just don't care about supervisors
    if ADIHasSupervisors and (EvoEE.ChildNodes.FindNode('supervisor') <> nil) then
      compare(GetSVCodeByGuid(Users, adiEE.Supervisor.NodeValue), EvoEE.Supervisor, fteSupervisor); //!! adiEE.Supervisor.name is superuserId

    //we can't clear this field so if it's empty in Evo then just don't touch ADI data
    if (EvoEE.ChildNodes.FindNode('timeclocknumber') <> nil) then
      compare(adiEE.Timeclocknumber, EvoEE.Timeclocknumber, fteBadgeID_ClockID);

    compare(adiEE.Addressline1, EvoEE.Addressline1, fteAddressline1);
    compare(adiEE.Addressline2, EvoEE.Addressline2, fteAddressline2);
    compare(adiEE.City, EvoEE.City, fteCity);
    compare(adiEE.Zipcode, EvoEE.Zipcode, fteZipCode);
    compare(FDict.GetStateAbbrByNullableGUID(adiEE.State.NodeValue), EvoEE.State, fteState);
    compare(adiEE.Phone, EvoEE.Phone, ftePhone);
    compare(adiEE.Emailaddress, EvoEE.Emailaddress, fteEmailAddress);

    compareDateTimeAndDate(adiEE.Hiredate.NodeValue, EvoEE.Hiredate, fteHireDate);
    compareDateTimeAndDate(adiEE.Birthdate.NodeValue, EvoEE.Birthdate, fteBirthDate);
    compareDateTimeAndDate(adiEE.Reviewdate.NodeValue, EvoEE.Reviewdate, fteReviewDate);

    compare(adiEE.ChildValues['issalaried'], EvoEE.ChildValues['issalaried'], fteSalaried);
//    compareFloat(adiEE.Salary, EvoEE.Salary, 'Salary');
//    if Length(Result) > 0 then
//      FLogger.LogEvent('Changed fields: ' + ChangedFieldRecsToBriefString(Result), ChangedFieldRecsToDetailedString(Result));
  end;

  procedure FilterOutNotActiveEEs;
  var
    i: integer;
  begin
    SetLength(FInactiveAdiEEs, 0);
    i := 0;
    while i < adiEEs.Count do
      if adiEEs[i].statusclass = 1 {Active} then
        i := i + 1
      else
      begin
        SetInclude(FInactiveAdiEEs, trim(adiEEs[i].Employeeidnumber) );
        adiEEs.Delete(i);
      end;
  end;
var
  i: integer;
  used: integer;
  changedFields: TChangedFieldRecs;
begin
  Result.Errors := 0;
  used := 0;
  FLogger.LogEntry('Analyzing employee records');
  try
    try
      LogADIOptions(FLogger, FADIOptions);
      adiEEs := FConn.RetrieveAllEmployees;
      FLogger.LogDebug( Format('ADI total employee count: %d',[adiEEs.Count]) );

      adiEEs.OwnerDocument.Options := adiEEs.OwnerDocument.Options + [doNodeAutoCreate]; //for AreDifferent
      FLogger.LogDebug( Format('ADI total employee count (1): %d',[adiEEs.Count]) );

      if hierarchy.LevelInfo(adiCompany).Enabled then
        hierarchy.FilterEEsByCompany(FEEData.Company, adiEEs);
      FLogger.LogDebug( Format('ADI company employee count: %d',[adiEEs.Count]) );

      FilterOutNotActiveEEs;
      FLogger.LogDebug( Format('ADI active company employee count: %d',[adiEEs.Count]) );

      SetLength(Result.Actions, adiEEs.Count + FEEData.EEs.RecordCount);

      for i := 0 to adiEEs.Count-1 do
      begin
        FLogger.LogEntry('Analyzing ADI employee record');
        try
          try
            FLogger.LogContextItem(sCtxEECode, adiEEs[i].Employeeidnumber);
            if FEEData.LocateEE(adiEEs[i].Employeeidnumber) and (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) then
            begin
              changedFields := CompareFields(adiEEs[i]);
              if Length(changedFields) > 0 then
              begin
                Result.Actions[used].Code := trim(adiEEs[i].Employeeidnumber);
                Result.Actions[used].Action := euaUpdate;
                Result.Actions[used].ChangedFields := changedFields;
                used := used + 1;
              end;
            end
            else
            begin
              Result.Actions[used].Code := trim(adiEEs[i].Employeeidnumber);
              Result.Actions[used].Action := euaDelete;
              used := used + 1;
            end
          except
            Result.Errors := Result.Errors + 1;
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end
      end;
      FEEData.EEs.First;
      while not FEEData.EEs.Eof do
      begin
        FLogger.LogEntry('Analyzing Evolution employee record');
        try
          try
            FLogger.LogContextItem(sCtxEECode, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
            if (FEEData.EEs['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) and not IsEEInADI( FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER'] ) then
            begin
              Result.Actions[used].Code := trim(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
              Result.Actions[used].Action := euaCreate;
              Result.Actions[used].HasEvoSV := trim(FEEData.EEs.FieldByName('supervisor_name').AsString) <> '';
              if ADIHasSupervisors and not Result.Actions[used].HasEvoSV then
                Result.Actions[used].SVUserID := SuggestRandomSVUserId;
              used := used + 1;
            end;
          except
            Result.Errors := Result.Errors + 1;
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
        FEEData.EEs.Next;
      end;
      SetLength(Result.Actions, used);
      Result.ADIHasSupervisors := ADIHasSupervisors;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

destructor TADIEmployeeUpdater.Destroy;
begin
  FreeAndNil(FDict);
  inherited;
end;

const
  EEUpdateActionNames: array [TEEUpdateAction] of string = ('Create', 'Update', 'Delete');

function TADIEmployeeUpdater.Apply(recs: TEEUpdateActionRecs; pi: IProgressIndicator): TUpdateEmployeesStat;
var
  i: integer;
begin
  Result := BuildEmptySyncStat;
  FLogger.LogEntry('Updating ADI employee records');
  try
    try
      LogADIOptions(FLogger, FADIOptions);
      pi.StartWait('Updating ADI employee records', Length(recs));
      try
        for i := 0 to high(recs) do
        begin
          FLogger.LogEntry('Processing employee');
          try
            try
              FLogger.LogContextItem(sCtxEECode, recs[i].Code);
              FLogger.LogContextItem('Action', EEUpdateActionNames[recs[i].Action]);
              case recs[i].Action of
                euaCreate:
                  begin
                    Assert( FEEData.LocateEE(recs[i].Code) );
                    CreateEmployee(recs[i].SVUserID); //SVUserID is ignored <=> HasEvoSV
                    inc(Result.Created);
                  end;
                euaUpdate:
                  begin
                    Assert( FEEData.LocateEE(recs[i].Code) );
                    FConn.UpdateEmployee( BuildEmployee(false) );
                    inc(Result.Modified);
                  end;
                euaDelete:
                  begin
                    if DeleteEmployee( recs[i].Code ) then
                      inc(Result.Deleted)
                    else
                      inc(Result.Modified);
                  end;
              else
                Assert(false);
              end;
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          pi.Step(i+1);
        end;
        //!!for debug only  -- remove it!
        //to put ee records into log
        try
          FLogger.LogDebug('After update:');
          FConn.RetrieveAllEmployees;
        except
          FLogger.StopException;
        end;
      finally
        pi.EndWait;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIEmployeeUpdater.ADIHasSupervisors: boolean;
begin
  Result := ADISettings.enablesupervisor = 1;
end;

function TADIEmployeeUpdater.CreateSVDataSet: TkbmCustomMemTable;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
begin
  Result := TkbmMemTable.Create(nil);
  try
    CreateStringField( Result, 'USER_ID', 'User Id', 10);
    CreateStringField( Result, 'USER_NAME', 'User Name', 30);
    Result.Open;
    Result.Append;
    try
      Result['USER_ID'] := '';
      Result['USER_NAME'] := '(No supervisor selected)';
      Result.Post;
    except
      Result.Cancel;
      raise;
    end;
    for i := 0 to Users.Count-1 do
    begin
      user := Users[i];
      if IsSV(user) then
      begin
        Result.Append;
        try
          Result['USER_ID'] := user.Userid;
          Result['USER_NAME'] := user.Username;
          Result.Post;
        except
          Result.Cancel;
          raise;
        end;
      end
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

procedure TADIEmployeeUpdater.CheckHierarchyMatch(evoDBDT: TEvoDBDTInfo);
var
  errorsFound: boolean;

  procedure LogError(const fmt: string; const args: array of const; d: string);
  begin
    Flogger.LogErrorFmt(fmt, args, d);
    errorsFound := true;
  end;

  procedure Check(evolevel: THierarchyLevelInfo; adilevel: THierarchyLevelInfo);
  var
    details: string;
  begin
  //!! fix wording
    details := 'These levels are on the same, well, level in ADI and Evolution hierarchy structures.'#13#10;
    if not adilevel.Enabled then
      details := details + #13#10'The reason why the ADI level is disabled:'#13#10 + adilevel.Reason;
    if not evolevel.Enabled then
      details := details + #13#10'The reason why the Evolution level is disabled:'#13#10 + evolevel.Reason;

    if adiLevel.Enabled <> evoLevel.Enabled then
    begin
      LogError('ADI ''%s'' level is %s but Evolution ''%s'' level is %s. See Details pane.',
      [adiLevel.Name, IIF(adiLevel.Enabled, 'enabled', 'disabled'),
       evoLevel.Name, IIF(evoLevel.Enabled, 'enabled', 'disabled') ],
      details);
    end
  end;

begin
  FLogger.LogEntry('Checking if Evolution D/B/D/T structure and ADI hierarchy match');
  try
    try
//      if not Hierarchy.LevelInfo(adiCompany).Enabled then
//        LogError('ADI ''Company'' level must be On', [], 'The reason why the ADI level is disabled:'#13#10 + Hierarchy.LevelInfo(adiCompany).Reason);

      Check( evoDBDT.Levels[CLIENT_LEVEL_DIVISION], Hierarchy.LevelInfo(adiLocation) );
      Check( evoDBDT.Levels[CLIENT_LEVEL_BRANCH], Hierarchy.LevelInfo(adiDivision) );
      Check( evoDBDT.Levels[CLIENT_LEVEL_DEPT], Hierarchy.LevelInfo(adiDepartment) );
      Check( evoDBDT.Levels[CLIENT_LEVEL_TEAM], Hierarchy.LevelInfo(adiPosition) );
      if errorsFound then
        raise Exception.Create('Evolution D/B/D/T structure and ADI hierarchy don''t match');
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIEmployeeUpdater.Hierarchy: TADIHierarchy;
begin
  if FHierarchy = nil then
    FHierarchy := TADIHierarchy.Create(FConn, FLogger);
  Result := FHierarchy;
end;

end.




