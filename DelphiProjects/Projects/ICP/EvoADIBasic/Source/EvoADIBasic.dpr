program EvoADIBasic;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  timeclockimport in '..\..\common\timeclockimport.pas',
  EvolutionSinglePayrollFrame in '..\..\common\EvolutionSinglePayrollFrame.pas' {EvolutionSinglePayrollFrm: TFrame},
  EvoTCImportMainForm in '..\..\common\EvoTCImportMainForm.pas' {EvoTCImportMainFm: TEvoAPIClientMainFm},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  FieldsToExportFrame in 'FieldsToExportFrame.pas' {FieldsToExportFrm: TFrame},
  ADIOptionsFrame in 'ADIOptionsFrame.pas' {ADIOptionsFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm},
  api in 'api.pas',
  aditime in 'aditime.pas',
  ADIConnectionParamFrame in 'ADIConnectionParamFrame.pas' {ADIConnectionParamFrm: TFrame},
  bndRetrieveAllPayTypes in 'xml\bndRetrieveAllPayTypes.pas',
  adidecl in 'adidecl.pas',
  adiconnection in 'adiconnection.pas',
  bndEmployeeStatuses in 'xml\bndEmployeeStatuses.pas',
  bndRetrieveSettings in 'xml\bndRetrieveSettings.pas',
  bndRequestPayrollData in 'xml\bndRequestPayrollData.pas',
  bndRetrieveAllEmployeescodes in 'xml\bndRetrieveAllEmployeescodes.pas',
  bndRetrieveAllPayGroups in 'xml\bndRetrieveAllPayGroups.pas',
  bndRetrieveAllUsers in 'xml\bndRetrieveAllUsers.pas',
  SVSelectionFrame in 'SVSelectionFrame.pas' {SVSelectionFrm: TFrame},
  SVSelectionForm in 'SVSelectionForm.pas' {SVSelectionFm},
  bndRetrieveAllEmployees in 'xml\bndRetrieveAllEmployees.pas',
  bndUpdateEmployee in 'xml\bndUpdateEmployee.pas',
  bndStateMasters in 'xml\bndStateMasters.pas',
  ConfirmationForm in 'ConfirmationForm.pas' {ConfirmationFm};

{$R *.res}

begin
  LicenseKey := 'F0B388DD829946C2B907C3A7E9E532C2'; //Andrei's

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution ADI Import';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
