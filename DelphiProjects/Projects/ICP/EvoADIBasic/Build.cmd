@ECHO OFF

IF "%1" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. Application Version
ECHO    2. DevTools folder (optional)
GOTO end

:start

SET pAppVersion=%1
IF "%pAppVersion%" NEQ ""  SET pAppVersion=%pAppVersion:"=%
SET pDevToolsDir=%2
IF "%pDevToolsDir%" NEQ "" SET pDevToolsDir=%pDevToolsDir:"=%

FOR %%a IN (..\..\..\Bin\ICP\EvoADIBasic\*.*) DO DEL /F /Q /S ..\..\..\Bin\ICP\EvoADIBasic\*.* > nul

CALL Compile.cmd "%pAppVersion%" R

IF "%pDevToolsDir%" == "" GOTO end

ECHO *Building release installer
CD .\Installer
CALL .\Build.cmd "%pDevToolsDir%\WiX" %pAppVersion%

:end
