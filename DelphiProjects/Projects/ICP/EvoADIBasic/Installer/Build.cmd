@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoADIBasic
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoADIBasic.exe
cd ..\..\..\Projects\ICP\EvoADIBasic\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoADIBasic.msi 

"%pWiXDir%\candle.exe" .\EvoADIBasic.wxs -wx -out ..\..\..\..\..\Tmp\EvoADIBasic.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoADIBasicRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoADIBasicRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoADIBasicRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoADIBasic.wixobj ..\..\..\..\..\Tmp\EvoADIBasicRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoADIBasic.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoADIBasic.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoADIBasic_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoADIBasic_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoADIBasic.msi EvoADIBasic_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
