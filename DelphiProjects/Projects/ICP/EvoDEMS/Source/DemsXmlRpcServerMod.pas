unit DemsXmlRpcServerMod;

interface

uses
  XmlRpcServer, XmlRpcTypes, Classes, SysUtils, EvoDemsProc, Dialogs,
  kbmMemTable, DB, EvoDemsDBTables, SyncObjs, DIMime, gdyCommonLogger,
  evoapiconnection, evoapiconnectionutils, common;

type
  WrongSignatureException = class(Exception);
  WrongSessionException = class(Exception);
  WrongLicenseException = class(Exception);
  WrongReturnTypeException = class(Exception);
  WrongReportLevelException = class(Exception);
  WrongPackageNumberException = class(Exception);
  WrongAPIKeyException = class(Exception);

  UnsupportedParamTypeException = class(Exception);
  UnsupportedOutputParamTypeException = class(Exception);
  UnsupportedFunctionException = class(Exception);

  NotAllowedEEException = class(Exception);
  TableNameEmptyException = class(Exception);
  NotSSUserException = class(Exception);

  QueueTaskNotFoundException = class(Exception);
  QueueTaskNotFinishedException = class(Exception);
  CannotConvertNbrToIdException = class(Exception);
  CannotConvertIdToNbrException = class(Exception);

  UnknownParamTypeException = class(Exception);
  ResultWrongStructureException = class(Exception);
  ResultIsEmptyException = class(Exception);
  EmptyDatasetException = class(Exception);

  UnknownCoStorageTag = class(Exception);
  CannotGetDataDueTimeout = class(Exception);
  UnknownEEChangeRequestType = class(Exception);

  LegacyCallsNotAllowed = class(Exception);
  ESSNotEnabledException = class(Exception);
  SecurityCallsNotAllowed = class(Exception);

  WrongParametersGetRecordNbrException = class(Exception);
  UnsupportedTableNameException = class(Exception);
  APIException = class(Exception);


  TevRPCMethod = procedure (const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String) of object;
  PTevRPCMethod = ^TevRPCMethod;

  TDEMSXmlRPCServer = class
  private
    FLogger: ICommonLogger;
//    FCS: TCriticalSection;
    FRpcServer: TRpcServer;
    FMethods: TStringList;
//    FEvoDemsProc: IEvoDemsProc;
    FEvoDemsDBTables: IEvoDemsDBTables;
    FEvoConnection: TEvoAPIConnectionParam;
    FEvoAPI: IEvoAPIConnection;

    // for frequently used queries
    FSyStates: TkbmCustomMemTable;

    //for debugging ticket #103104
    procedure GetClBankAccounts;

    procedure RegisterMethods;
    procedure RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);

    procedure testGetSystemTimeMethod(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure VerifyStructMember(aStruct: IRpcStruct; const aName: string; aDataType: TDataType; Mandatory: boolean = True);

    procedure GetRecordNbr(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure ProcessRecord(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure CreateCalendar(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure CreatePayroll(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);

    procedure APICheckCondition(const ACondition: Boolean; AExceptClass: ExceptClass; const aMessage: string = '');

    procedure OpenClient(clNbr: integer);
    function SB_CreateCoCalendar(CoNbr: integer; CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: string): IRpcArray;
    function TaskQueue_RunCreatePayroll(aParams: IRpcStruct): string;
    function OpenSQL(SQLText: string; cl_nbr: integer; MapFields: string): TkbmCustomMemTable;
    function createNewClientDB: integer;
    function applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;

    function GetActive: boolean;
    procedure SetActive(AValue: boolean);
    function ConnectToEvo: boolean;
  public
    constructor Create(Logger: ICommonLogger);
    destructor Destroy; override;

    property EvoConnection: TEvoAPIConnectionParam read FEvoConnection write FEvoConnection;
    property Active: boolean read GetActive write SetActive;
//    property EvoDemsProc: IEvoDemsProc read FEvoDemsProc write FEvoDemsProc;
    property EvoDemsDBTables: IEvoDemsDBTables read FEvoDemsDBTables write FEvoDemsDBTables;
  end;

implementation

uses gdyRedir, EvoDemsCommon, StrUtils;

const
  MaxRetryCount = 2;

procedure CheckCondition(const ACondition: Boolean; const AErrMessage: String; AExceptClass: ExceptClass = nil; AHelpContext: Integer = 0);
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      AExceptClass := Exception;

    if AHelpContext = 0 then
      raise AExceptClass.Create(AErrMessage)
    else
      raise AExceptClass.CreateHelp(AErrMessage, AHelpContext);
  end;
end;

function GetNextStrValue(var AStr: string; const ADelim: String = ','): String;
var
  j: Integer;
begin
  j := Pos(ADelim, AStr);
  if j = 0 then
  begin
    Result := AStr;
    AStr := '';
  end

  else
  begin
    Result := Copy(AStr, 1, j - 1);
    Delete(AStr, 1, j - 1 + Length(ADelim));
  end;
end;

{ TDEMSXmlRPCServer }

procedure TDEMSXmlRPCServer.APICheckCondition(const ACondition: Boolean;
  AExceptClass: ExceptClass; const aMessage: string = '');
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      raise Exception.Create('APICheckCondition: exception class should be assigned!')
    else begin
      if Trim(aMessage) <> '' then
        raise AExceptClass.Create('APIException: ' + aMessage)
      else
        raise AExceptClass.Create('APIException');
    end;
  end;
end;

function TDEMSXmlRPCServer.applyDataChangePacket(changes: IRpcArray;
  aAsOF: TDateTime): IRpcStruct;
var
  RetryCount: integer;
  errMessage: string;
begin
  RetryCount := 0;
  errMessage := '';
  Result := nil;
  while RetryCount < MaxRetryCount do
  try
    if not Assigned(FEvoAPI) then
      ConnectToEvo;

    if Assigned(FEvoAPI) then
      Result := FEvoAPI.applyDataChangePacket( changes, aAsOF )
    else
      errMessage := 'Please check if Evo Server "' + FEvoConnection.APIServerAddress + '" is available!';

    Break;
  except
    on e: Exception do
    begin
      FEvoAPI := nil;
      RetryCount := RetryCount + 1;
      errMessage := e.Message;
    end;
  end;

  if errMessage <> '' then
    FLogger.LogError('Can not call Evo API applyDataChangePacket method', errMessage);
end;

function TDEMSXmlRPCServer.ConnectToEvo: boolean;
begin
  Result := False;
  try
    FEvoAPI := CreateEvoAPIConnection(FEvoConnection, FLogger);
    Result := True;
    FLogger.LogEvent('Successfully connected to Evo Server ' + FEvoConnection.APIServerAddress);
  except
    on E: Exception do begin
      FLogger.LogError('Can not connect to Evolution', e.Message);
      FEvoAPI := nil;
    end;
  end;
end;

constructor TDEMSXmlRPCServer.Create(Logger: ICommonLogger);
begin
  inherited Create;

  FLogger := Logger;
//  FCS := TCriticalSection.Create;

  FMethods := TStringList.Create;
  FMethods.Duplicates := dupError;
  FMethods.CaseSensitive := False;
  FMethods.Sorted := True;

  FRpcServer := TRpcServer.Create;
  FEvoAPI := nil;

  RegisterMethods;
end;

procedure TDEMSXmlRPCServer.CreateCalendar(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcStruct, rpcFrequencies, rpcFreq, rpcFrequenciesEvo, rpcFreqEvo, rpcDemsResult: IRpcStruct;
  cl_nbr, co_nbr, i: integer;
  sFreq, CalcDay: string;
  EvoResult: IRpcArray;
begin
  //CreateCalendar(struct CalendarParams)
  //Return: struct
  // Return_Code - integer, -1 - error, 0 - successful
  // Return_Message - string

  APICheckCondition(AParams.Count = 1, WrongSignatureException);
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtStruct, WrongSignatureException);

  rpcStruct := TRpcParameter(AParams[0]).AsStruct;

  VerifyStructMember(rpcStruct, 'CL_NBR', dtInteger);
  VerifyStructMember(rpcStruct, 'CO_NBR', dtInteger);
  VerifyStructMember(rpcStruct, 'CalculateDay', dtString);

  CalcDay := rpcStruct.Keys['CalculateDay'].AsString;
  if CalcDay = 'R' then // in case of Calendar Date the following parameters are mandatory
  begin
    VerifyStructMember(rpcStruct, 'MoveCheckDate', dtString);
    VerifyStructMember(rpcStruct, 'MoveCallInDate', dtString);
    VerifyStructMember(rpcStruct, 'MoveDeliveryDate', dtString);
    rpcFrequencies := rpcStruct.Items[6].AsStruct;
  end
  else
    rpcFrequencies := rpcStruct.Items[3].AsStruct;

  VerifyStructMember(rpcStruct, 'NumberOfMonths', dtInteger);

//  rpcStruct.KeyList.CommaText;
//  VerifyStructMember(rpcStruct, 'Frequencies', dtStruct);

  // Frequencies is a struct of structs. Each member should have a name of W (Weekly), B (Bi-Weekly),
  // S (Semi-Monthly), M (Monthly), Q (Quarterly).

  APICheckCondition(rpcFrequencies.Count >= 1, APIException, '"Frequencies" should not be empty');

  cl_nbr := rpcStruct.Keys['CL_NBR'].AsInteger;
  OpenClient( cl_nbr );
  co_nbr := rpcStruct.Keys['CO_NBR'].AsInteger;

  rpcFrequenciesEvo := TRpcStruct.Create;
  rpcFreqEvo := TRpcStruct.Create;
  rpcDemsResult := TRpcStruct.Create;

  for i := 0 to rpcFrequencies.Count - 1 do
  begin
    rpcFreq := rpcFrequencies.Items[i].AsStruct;//rpcFrequencies.Keys[sFreq].AsStruct;
    sFreq := rpcFreq.Keys['Frequency'].AsString;//'W';//rpcFrequencies.KeyList[i];

    VerifyStructMember(rpcFreq, 'DaysBeforeCheckDate', dtInteger);
    VerifyStructMember(rpcFreq, 'DaysAfterCallInDate', dtInteger);
    VerifyStructMember(rpcFreq, 'CallInTime', dtString);
    VerifyStructMember(rpcFreq, 'DeliveryTime', dtString);
    VerifyStructMember(rpcFreq, 'InitialCheckDate', dtDateTime);
    VerifyStructMember(rpcFreq, 'PeriodBeginDate', dtDateTime);
    VerifyStructMember(rpcFreq, 'PeriodEndDate', dtDateTime);

    rpcFreqEvo.AddItem('NUMBER_OF_DAYS_PRIOR', rpcFreq.Keys['DaysBeforeCheckDate'].AsInteger);
    rpcFreqEvo.AddItem('CALL_IN_TIME', rpcFreq.Keys['CallInTime'].AsString);
    rpcFreqEvo.AddItem('NUMBER_OF_DAYS_AFTER', rpcFreq.Keys['DaysAfterCallInDate'].AsInteger);
    rpcFreqEvo.AddItem('DELIVERY_TIME', rpcFreq.Keys['DeliveryTime'].AsString);
    rpcFreqEvo.AddItemDateTime('LAST_REAL_SCHEDULED_CHECK_DATE', rpcFreq.Keys['InitialCheckDate'].AsDateTime);
    rpcFreqEvo.AddItemDateTime('PERIOD_BEGIN_DATE', rpcFreq.Keys['PeriodBeginDate'].AsDateTime);
    rpcFreqEvo.AddItemDateTime('PERIOD_END_DATE', rpcFreq.Keys['PeriodEndDate'].AsDateTime);
    rpcFreqEvo.AddItem('NUMBER_OF_MONTHS', rpcStruct.Keys['NumberOfMonths'].AsInteger);
    rpcFreqEvo.AddItem('CheckEndOfMonth', False);
    rpcFreqEvo.AddItem('PeriodEndOfMonth', False);


    if sFreq = 'S' then // additional field for Semi-Monthly frequency
    begin
      VerifyStructMember(rpcFreq, 'DaysBeforeCheckDate2', dtInteger);
      VerifyStructMember(rpcFreq, 'DaysAfterCallInDate2', dtInteger);
      VerifyStructMember(rpcFreq, 'CallInTime2', dtString);
      VerifyStructMember(rpcFreq, 'DeliveryTime2', dtString);
      VerifyStructMember(rpcFreq, 'InitialCheckDate2', dtDateTime);
      VerifyStructMember(rpcFreq, 'PeriodBeginDate2', dtDateTime);
      VerifyStructMember(rpcFreq, 'PeriodEndDate2', dtDateTime);

      rpcFreqEvo.AddItem('NUMBER_OF_DAYS_PRIOR2', rpcFreq.Keys['DaysBeforeCheckDate2'].AsInteger);
      rpcFreqEvo.AddItem('CALL_IN_TIME2', rpcFreq.Keys['CallInTime2'].AsString);
      rpcFreqEvo.AddItem('NUMBER_OF_DAYS_AFTER2', rpcFreq.Keys['DaysAfterCallInDate2'].AsInteger);
      rpcFreqEvo.AddItem('DELIVERY_TIME2', rpcFreq.Keys['DeliveryTime2'].AsString);
      rpcFreqEvo.AddItemDateTime('LAST_REAL_SCHED_CHECK_DATE2', rpcFreq.Keys['InitialCheckDate2'].AsDateTime);
      rpcFreqEvo.AddItemDateTime('PERIOD_BEGIN_DATE2', rpcFreq.Keys['PeriodBeginDate2'].AsDateTime);
      rpcFreqEvo.AddItemDateTime('PERIOD_END_DATE2', rpcFreq.Keys['PeriodEndDate2'].AsDateTime);
      rpcFreqEvo.AddItem('CheckEndOfMonth2', False);
      rpcFreqEvo.AddItem('PeriodEndOfMonth2', False);
    end;

    rpcFrequenciesEvo.AddItem(sFreq, rpcFreqEvo);
  end;

  try
    EvoResult := SB_CreateCoCalendar( co_nbr, rpcFrequenciesEvo, CalcDay,
      rpcStruct.Keys['MoveCheckDate'].AsString, rpcStruct.Keys['MoveCallInDate'].AsString, rpcStruct.Keys['MoveDeliveryDate'].AsString );

    sFreq := '';
    for i := 0 to EvoResult.Count - 1 do
      sFreq := sFreq + EvoResult[i].AsString + #10#13;

    if sFreq <> '' then
    begin
      rpcDemsResult.AddItem('Code', -1);
      rpcDemsResult.AddItem('Message', sFreq);
    end
    else
      rpcDemsResult.AddItem('Code', 0);

  finally
    AReturn.AddItem( rpcDemsResult );
  end;
end;

function TDEMSXmlRPCServer.createNewClientDB: integer;
var
  RetryCount: integer;
  errMessage: string;
begin
  RetryCount := 0;
  errMessage := '';
  Result := -1;
  while RetryCount < MaxRetryCount do
  try
    if not Assigned(FEvoAPI) then
      ConnectToEvo;

    if Assigned(FEvoAPI) then
      Result := FEvoAPI.createNewClientDB
    else
      errMessage := 'Please check if Evo Server "' + FEvoConnection.APIServerAddress + '" is available!';
    Break;
  except
    on e: Exception do
    begin
      FEvoAPI := nil;
      RetryCount := RetryCount + 1;
      errMessage := e.Message;
    end;
  end;

  if errMessage <> '' then
    FLogger.LogError('Can not call Evo API createNewClientDB method', errMessage);
end;

procedure TDEMSXmlRPCServer.CreatePayroll(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcStruct, rpcDemsResult: IRpcStruct;
  i: integer;
  EvoResult: string;

  procedure ConvertToBoolean(const aKey: string);
  var
    b: boolean;
  begin
    b := false;
    if rpcStruct.KeyExists(aKey) then
    try
      b := rpcStruct.Keys[aKey].AsInteger = 0;
    finally
      rpcStruct.Delete(aKey);
      rpcStruct.AddItem(aKey, b);
    end;
  end;

  procedure ChangeKeyName(const aKey, aNewKey: string);
  var
    v: integer;
  begin
    v := rpcStruct.Keys[aKey].AsInteger;
    rpcStruct.Delete(aKey);
    rpcStruct.AddItem(aNewKey, v);
  end;

  procedure FixEeList;
  var
    EeList: string;
  begin
    EeList := rpcStruct.Keys['EeList'].AsString;
    if EeList <> '' then
    begin
      rpcStruct.Delete('EeList');
      rpcStruct.AddItem('EeList', EeList + ',');
    end;
  end;

begin
  //CreatePayroll(struct PayrollParams)
  //Return: struct
  // Return_Code - integer, -1 - error, 0 - successful
  // Return_Message - string

  APICheckCondition(AParams.Count = 1, WrongSignatureException);
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtStruct, WrongSignatureException);

  rpcStruct := TRpcParameter(AParams[0]).AsStruct;

  VerifyStructMember(rpcStruct, 'CL_NBR', dtInteger);
  VerifyStructMember(rpcStruct, 'CO_NBR', dtInteger);
  VerifyStructMember(rpcStruct, 'SalaryOrHourly', dtInteger);
  VerifyStructMember(rpcStruct, 'CheckDateReplace', dtInteger);
  VerifyStructMember(rpcStruct, 'CheckDate', dtDateTime, False);
  VerifyStructMember(rpcStruct, 'BatchFreq', dtString);
  VerifyStructMember(rpcStruct, 'PeriodBeginDate', dtDateTime);
  VerifyStructMember(rpcStruct, 'PeriodEndDate', dtDateTime);
  VerifyStructMember(rpcStruct, 'EeList', dtString);
  VerifyStructMember(rpcStruct, 'BlockTaxDeposits', dtInteger);
  VerifyStructMember(rpcStruct, 'BlockChecks', dtInteger);
  VerifyStructMember(rpcStruct, 'BlockTimeOff', dtInteger);
  VerifyStructMember(rpcStruct, 'BlockAgencies', dtInteger);
  VerifyStructMember(rpcStruct, 'Block401K', dtInteger);
  VerifyStructMember(rpcStruct, 'BlockACH', dtInteger);
  VerifyStructMember(rpcStruct, 'BlockBilling', dtInteger);
  VerifyStructMember(rpcStruct, 'TcImportSourceFile', dtBase64, False);
  VerifyStructMember(rpcStruct, 'TcLookupOption', dtInteger, False);
  VerifyStructMember(rpcStruct, 'TcDBDTOption', dtInteger, False);
  VerifyStructMember(rpcStruct, 'TcFileFormat', dtInteger, False);
  VerifyStructMember(rpcStruct, 'TcFourDigitYear', dtInteger, False);
  VerifyStructMember(rpcStruct, 'TcAutoImportJobCodes', dtInteger, False);

//  cl_nbr := rpcStruct.Keys['CL_NBR'].AsInteger;
//  FEvoDemsProc.OpenClient( cl_nbr );

  rpcDemsResult := TRpcStruct.Create;
  try
    ConvertToBoolean('TcFourDigitYear');
    ConvertToBoolean('TcAutoImportJobCodes');

    ChangeKeyName('CL_NBR', 'CLNbr');
    ChangeKeyName('CO_NBR', 'CONbr');

    FixEeList;

    EvoResult := TaskQueue_RunCreatePayroll( rpcStruct );

    if not TryStrToInt(EvoResult, i) then
      rpcDemsResult.AddItem('Code', -1)
    else
      rpcDemsResult.AddItem('Code', 0);

    rpcDemsResult.AddItem('Message', EvoResult);

  finally
    AReturn.AddItem( rpcDemsResult );
  end;
end;

destructor TDEMSXmlRPCServer.Destroy;
begin
  SetActive( False );

  FEvoDemsDBTables := nil;
  inherited;

  if Assigned(FSyStates) then
    FreeAndNil(FSyStates);

  FreeAndNil(FRpcServer);
  FreeAndNil(FMethods);
//  FreeAndNil(FCS);
end;

function TDEMSXmlRPCServer.GetActive: boolean;
begin
  Result := FRpcServer.Active;
end;

procedure TDEMSXmlRPCServer.GetClBankAccounts;
var
  SQL: string;
begin
  SQL := 'SELECT t1.Cl_Bank_Account_Nbr, t1.Sb_Banks_Nbr, t1.Custom_Bank_Account_Number, ' +
         '  t1.Effective_Date, t1.Effective_Until ' +
         'FROM Cl_Bank_Account t1 where {AsOfNow<t1>} ';

  OpenSQL( SQL, 0, '' );
end;

procedure TDEMSXmlRPCServer.GetRecordNbr(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcStruct: IRpcStruct;
  rNbr: integer;
  TableName, SQL: string;
  Params: IRpcStruct;
  lQuery: TkbmCustomMemTable;
  cl_nbr: integer;
begin
  //GetRecordNbr(struct TableFields)
  //Return: integer - internal record number

  rNbr := -1;

  APICheckCondition(AParams.Count = 1, WrongSignatureException);
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtStruct, WrongSignatureException);

  rpcStruct := TRpcParameter(AParams[0]).AsStruct;
  FLogger.LogDebug('GetRecordNbr input parameters, time:' + DateTimeToStr(now), rpcStruct.GetAsXML);

  APICheckCondition(rpcStruct.Count = 2, WrongParametersGetRecordNbrException);
  APICheckCondition(rpcStruct.Items[0].DataType = dtString, WrongParametersGetRecordNbrException); //'Wrong table name parameter type for GetRecordNbr. Should be <string>.

  TableName := rpcStruct.Items[0].AsString;
  Params := rpcStruct.Items[1].AsStruct;

  if (TableName = 'SY_STATES') and (Params.KeyExists('STATE')) then
  begin
    if not Assigned( FSyStates ) then
    begin
      SQL := 'select s.SY_STATES_NBR, s.STATE from SY_STATES s where {AsOfNow<s>}';
      FSyStates := OpenSQL( SQL, 0, '' );
    end;

    if Assigned(FSyStates) and (FSyStates.RecordCount > 0) then
      rNbr := FSyStates.Lookup('STATE', Params.Keys['STATE'].AsString, 'SY_STATES_NBR');
  end;

  if rNbr = -1 then
  begin
    if Params.KeyExists('CL_NBR') then
    begin
      cl_nbr := Params.Keys['CL_NBR'].AsInteger;
      Params.Delete('CL_NBR');
    end
    else
      cl_nbr := 0;

    SQL := BuildSQL(TableName, Params);

  {  FCS.Acquire;
    try}
      if cl_nbr > 0 then
        OpenClient( cl_nbr );
      lQuery := OpenSQL( SQL, cl_nbr, TableName + '_NBR=' + TableName );
      try
        if (lQuery.RecordCount > 0) and Assigned(lQuery.FindField(TableName + '_NBR')) then
          rNbr := lQuery.FieldByName(TableName + '_NBR').AsInteger;
      finally
        FreeAndNil(lQuery);
      end;
  {  finally
      FCS.Release;
    end;}
  end;

  AReturn.AddItem( rNbr );
end;

procedure TDEMSXmlRPCServer.OpenClient(clNbr: integer);
var
  RetryCount: integer;
  errMessage: string;
begin
  RetryCount := 0;
  errMessage := '';
  while RetryCount < MaxRetryCount do
  try
    if not Assigned(FEvoAPI) then
      ConnectToEvo;

    if Assigned(FEvoAPI) then
      FEvoAPI.OpenClient( clNbr )
    else
      errMessage := 'Please check if Evo Server "' + FEvoConnection.APIServerAddress + '" is available!';

    Break;
  except
    on e: Exception do
    begin
      FEvoAPI := nil;
      RetryCount := RetryCount + 1;
      errMessage := e.Message;
    end;
  end;

  if errMessage <> '' then
    FLogger.LogError('Can not call Evo API OpenClient method', errMessage);
end;

function TDEMSXmlRPCServer.OpenSQL(SQLText: string; cl_nbr: integer;
  MapFields: string): TkbmCustomMemTable;
var
  RetryCount: integer;
  errMessage: string;
begin
  RetryCount := 0;
  errMessage := '';
  Result := nil;
  while RetryCount < MaxRetryCount do
  try
    if not Assigned(FEvoAPI) then
      ConnectToEvo;

    if Assigned(FEvoAPI) then
      Result := FEvoAPI.OpenSQL( SQLText, cl_nbr, MapFields )
    else
      errMessage := 'Please check if Evo Server "' + FEvoConnection.APIServerAddress + '" is available!';
    Break;
  except
    on e: Exception do
    begin
      FEvoAPI := nil;
      RetryCount := RetryCount + 1;
      errMessage := e.Message;
    end;
  end;

  if errMessage <> '' then
    FLogger.LogError('Can not call Evo API OpenSQL method', errMessage);
end;

procedure TDEMSXmlRPCServer.ProcessRecord(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcArray: IRpcArray;
  rpcStruct, rpcRowChange, rpcEvoResult, rpcDemsResult: IRpcStruct;
  Changes: IRpcArray;
  i, pk: integer;
  RecKeyList: TStrings;
  rk, tn, changetype: string;
  flist: IRpcStruct;
  RecentOpenedClient: integer;
  aAsOf: TDatetime;

begin
  // ProcessRecord(array of struct RecordChanges [, datetime AsOf])
  // RecordChanges has the following fields:
  // 1. "RecordKey" - string, ID of the record
  // 2. "TableName" - string, name of db table
  // 3. "Fields" - struct, fieldnames and values
  // Return: struct where the name is string ID from the "RecordKey" and the value is PK value

  APICheckCondition(AParams.Count >= 1, WrongSignatureException);
  RecentOpenedClient := 0;

  rpcEvoResult := TRpcStruct.Create;
  Changes := TRpcArray.Create;
  RecKeyList := TStringList.Create;
  try
    APICheckCondition(TRpcParameter(AParams[0]).DataType = dtArray, WrongSignatureException);

    if AParams.Count = 2 then
    begin
      APICheckCondition(TRpcParameter(AParams[1]).DataType = dtDateTime, WrongSignatureException);
      aAsOf := TRpcParameter(AParams[1]).AsDateTime;
    end
    else
      aAsOf := 0;

    rpcArray := TRpcParameter(AParams[0]).AsArray;
    FLogger.LogDebug('ProcessRecord input parameters, time:' + DateTimeToStr(now), rpcArray.GetAsXML);
    for i := 0 to rpcArray.Count - 1 do
      if rpcArray.Items[i].IsStruct then
      begin
        rpcStruct := rpcArray.Items[i].AsStruct;

        // check if the row change struct has all required keys
        // otherwise it won't be passed further
        if rpcStruct.KeyExists('RecordKey') and rpcStruct.KeyExists('TableName') {and rpcStruct.KeyExists('Fields')} then
        begin
          // adapt row change struct
          rk := rpcStruct.Keys['RecordKey'].AsString;
          tn := rpcStruct.Keys['TableName'].AsString;

          APICheckCondition( FEvoDemsDBTables.TableExists( tn ), UnsupportedTableNameException);

          flist := rpcStruct.Items[2].AsStruct;

          // primary key must exists
          if flist.KeyExists(tn + '_NBR') then
          begin
            rpcRowChange := TRpcStruct.Create;
            changetype := 'U'; //update
            pk := flist.Keys[tn + '_NBR'].AsInteger;

            // open Client db
            if flist.KeyExists('CL_NBR') then
            begin
              if (flist.Keys['CL_NBR'].AsInteger > 0) and (flist.Keys['CL_NBR'].AsInteger <> RecentOpenedClient) then
              begin
                RecentOpenedClient := flist.Keys['CL_NBR'].AsInteger;
              end;
              if tn <> 'CL' then
                flist.Delete('CL_NBR');
            end;

            // ticket #103748, remove CO_NBR from CL table modification requests
            if (tn = 'CL') and flist.KeyExists('CO_NBR') then
            begin
              flist.Delete('CO_NBR');
              FLogger.LogWarning('Do not use CO_NBR in ProcessRecord request for CL table! There is no such field in CL table.');
            end;

            if pk < 0 then // insert record
            begin
              if tn <> 'CL' then
              begin
                pk := -1 - RecKeyList.Count;
                flist.Keys[tn + '_NBR'].AsInteger := pk;
                changetype := 'I'; //insert
              end
              else begin
                // create a new client db
                pk := createNewClientDB;
                flist.Keys[tn + '_NBR'].AsInteger := pk;

                RecentOpenedClient := pk;

                GetClBankAccounts;
              end;
            end;

            if (tn = 'CL_BANK_ACCOUNT') then
              GetClBankAccounts;

            if (tn = 'CO_ENLIST_GROUPS') then
            begin
              if flist.KeyExists('MEDIA_TYPE') then
              begin
                if flist.Keys['MEDIA_TYPE'].AsString = '' then
                  flist.Keys['MEDIA_TYPE'].AsString := ' ';
              end
              else
                flist.AddItem('MEDIA_TYPE', ' ');
            end;

            if (tn = 'EE') and (flist.KeyExists('CUSTOM_EMPLOYEE_NUMBER')) then
              flist.Keys['CUSTOM_EMPLOYEE_NUMBER'].AsString := PadLeft(flist.Keys['CUSTOM_EMPLOYEE_NUMBER'].AsString, ' ', 9);

            if (tn = 'CO_DIVISION') and (flist.KeyExists('CUSTOM_DIVISION_NUMBER')) then
              flist.Keys['CUSTOM_DIVISION_NUMBER'].AsString := PadLeft(flist.Keys['CUSTOM_DIVISION_NUMBER'].AsString, ' ', 20);
            if (tn = 'CO_BRANCH') and (flist.KeyExists('CUSTOM_BRANCH_NUMBER')) then
              flist.Keys['CUSTOM_BRANCH_NUMBER'].AsString := PadLeft(flist.Keys['CUSTOM_BRANCH_NUMBER'].AsString, ' ', 20);
            if (tn = 'CO_DEPARTMENT') and (flist.KeyExists('CUSTOM_DEPARTMENT_NUMBER')) then
              flist.Keys['CUSTOM_DEPARTMENT_NUMBER'].AsString := PadLeft(flist.Keys['CUSTOM_DEPARTMENT_NUMBER'].AsString, ' ', 20);
            if (tn = 'CO_TEAM') and (flist.KeyExists('CUSTOM_TEAM_NUMBER')) then
              flist.Keys['CUSTOM_TEAM_NUMBER'].AsString := PadLeft(flist.Keys['CUSTOM_TEAM_NUMBER'].AsString, ' ', 20);

            RecKeyList.Add( rk + '=' + IntToStr(pk) );


            rpcRowChange.AddItem('T', changetype);
            rpcRowChange.AddItem('D', tn);
            if changetype = 'U' then
              rpcRowChange.AddItem('K', pk);
            rpcRowChange.AddItem('F', flist);
            Changes.AddItem( rpcRowChange );
          end;
        end;
      end;

    if Changes.Count > 0 then
    begin
      if RecentOpenedClient > 0 then
        OpenClient( RecentOpenedClient );

      rpcEvoResult := applyDataChangePacket( Changes, aAsOf );

      rpcDemsResult := TRpcStruct.Create;
      for i := 0 to RecKeyList.Count - 1 do
      begin
        pk := -1 - i;
        rk := RecKeyList.Values[ RecKeyList.Names[i] ];  // primary key value (negative for inserts)

        if Assigned(rpcEvoResult) and not TryStrToInt( rk, pk ) or (pk < 0) then
          if rpcEvoResult.KeyExists(IntToStr(pk)) then
          begin
            if rpcEvoResult.Keys[IntToStr(pk)].IsString then
            begin
              if not TryStrToInt(rpcEvoResult.Keys[IntToStr(pk)].AsString, pk) then
                raise Exception.Create('Can not convert key value "'+rpcEvoResult.Keys[IntToStr(pk)].AsString+'" to an integer type');
            end
            else if rpcEvoResult.Keys[IntToStr(pk)].IsInteger then
              pk := rpcEvoResult.Keys[IntToStr(pk)].AsInteger
            else
              raise Exception.Create('Unknown type of key value');
          end;
        rpcDemsResult.AddItem( RecKeyList.Names[i], pk );
      end;
      AReturn.AddItem( rpcDemsResult );
    end;
  finally
    FreeAndNil( RecKeyList );
  end;
end;

procedure TDEMSXmlRPCServer.RegisterMethods;

   procedure RegisterMethod(const ASignature, AHelp: String; const AHandler: TevRPCMethod);
   var
     RpcMethodHandler: TRpcMethodHandler;
     s: String;
   begin
     s := ASignature;
     RpcMethodHandler := TRpcMethodHandler.Create;
     RpcMethodHandler.Name := GetNextStrValue(s, '(');
     RpcMethodHandler.Method := RPCHandler;
     RpcMethodHandler.Signature := ASignature;
     RpcMethodHandler.Help := AHelp;
     FRpcServer.RegisterMethodHandler(RpcMethodHandler);

     FMethods.AddObject(RpcMethodHandler.Name, @AHandler);
   end;

begin
  RegisterMethod('test.getSystemTime()',
                 'Return: datetime - current system time',
                 testGetSystemTimeMethod);

  RegisterMethod('getRecordNbr(struct TableFields)',
                 'Return: integer - internal record number',
                 GetRecordNbr);

  RegisterMethod('processRecord(array of struct RecordChanges)',
                 'Return: struct',
                 ProcessRecord);

  RegisterMethod('createCalendar(struct CalendarParams)',
                 'Return: struct',
                 CreateCalendar);

  RegisterMethod('createPayroll(struct CreatePayrollParams)',
                 'Return: struct',
                 CreatePayroll);
end;

procedure TDEMSXmlRPCServer.RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);
var
  i: Integer;
  Proc: TMethod;
begin
  try
    i := FMethods.IndexOf(MethodName);
    CheckCondition(i <> -1, 'Unregistered method "' + MethodName + '"');

    Proc.Data := Self;
    Proc.Code := FMethods.Objects[i];
    TevRPCMethod(Proc)(List, Return, LowerCase(MethodName));
  except
    on E: EEvoAPIError do
    begin
      Return.SetError(E.Code, E.Message);
    end;

    on E: APIException do
    begin
      Return.SetError(800, E.Message);
    end;

    on E: UnsupportedTableNameException do
    begin
      Return.SetError(800, 'Unsupported Table');
    end;

    on E: WrongParametersGetRecordNbrException do
    begin
      Return.SetError(800, 'Wrong struct parameter for GetRecordNbr method');
    end;

    on E: WrongSignatureException do
    begin
      Return.SetError(800, 'Wrong method signature');
    end;

    on E: WrongSessionException do
    begin
      Return.SetError(810, 'Wrong session');
    end;

    on E: WrongLicenseException do
    begin
      Return.SetError(800, 'Wrong license key');
    end;

    on E: WrongAPIKeyException do
    begin
      Return.SetError(800, 'Wrong API Key');
    end;

    on E: NotAllowedEEException do
    begin
      Return.SetError(810, 'Operation is not allowed in EE mode');
    end;

    on E: TableNameEmptyException do
    begin
      Return.SetError(800, 'No table name');
    end;

    on E: QueueTaskNotFoundException do
    begin
      Return.SetError(800, 'Task not found in queue');
    end;

    on E: QueueTaskNotFinishedException do
    begin
      Return.SetError(800, 'Task not finished yet');
    end;

    on E: NotSSUserException do
    begin
      Return.SetError(800, 'This user is not registered as a SelfServe user');
    end;

    on E: WrongReportLevelException do
    begin
      Return.SetError(810, 'Wrong report level');
    end;

    on E: UnsupportedParamTypeException do
    begin
      Return.SetError(811, 'dtArray and dtNone are unsupported parameter types');
    end;

    on E: UnknownParamTypeException do
    begin
      Return.SetError(811, 'Unsupported parameter type');
    end;

    on E: ResultWrongStructureException do
    begin
      Return.SetError(810, 'Result has wrong structure');
    end;

    on E: WrongReturnTypeException do
    begin
      Return.SetError(510, 'Wrong return type');
    end;

    on E: WrongPackageNumberException do
    begin
      Return.SetError(810, 'Package number should be 4 (REMOTABLE_CALC_PACKAGE)');
    end;

    on E: UnsupportedFunctionException do
    begin
      Return.SetError(810, 'Unsupported function');
    end;

    on E: ResultIsEmptyException do
    begin
      Return.SetError(810, 'Result is empty');
    end;

    on E: UnsupportedOutputParamTypeException do
    begin
      Return.SetError(810, 'Unsupported output parameter type');
    end;

    on E: EmptyDatasetException do
    begin
      Return.SetError(810, 'Dataset is empty');
    end;

    on E: CannotConvertNbrToIdException do
    begin
      Return.SetError(810, 'Cannot find TaskId for given TaskNbr');
    end;

    on E: CannotConvertIdToNbrException do
    begin
      Return.SetError(810, 'Cannot find TaskNbr for given TaskId');
    end;

    on E: LegacyCallsNotAllowed do
    begin
      Return.SetError(810, 'You have no rights to do legacy calls');
    end;

    on E: SecurityCallsNotAllowed do
    begin
      Return.SetError(810, 'You have no rights to do security calls');
    end;

    on E: UnknownCoStorageTag do
    begin
      Return.SetError(800, 'Unknown Tag');
    end;

    on E: CannotGetDataDueTimeout do
    begin
      Return.SetError(810, 'Program was not able to read data because table is locked');
    end;

    on E: UnknownEEChangeRequestType do
    begin
      Return.SetError(800, 'Unknown type of employee change request');
    end;

    on E: ESSNotEnabledException do
    begin
      Return.SetError(800, 'SelfServe is not enabled for this employee');
    end;

    on E: Exception do
    begin
      Return.SetError(500, E.Message);
    end;
  end;
end;

function TDEMSXmlRPCServer.SB_CreateCoCalendar(CoNbr: integer;
  CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate,
  MoveDeliveryDate: string): IRpcArray;
var
  RetryCount: integer;
  errMessage: string;
begin
  RetryCount := 0;
  errMessage := '';
  Result := nil;
  while RetryCount < MaxRetryCount do
  try
    if not Assigned(FEvoAPI) then
      ConnectToEvo;

    if Assigned(FEvoAPI) then
      Result := FEvoAPI.SB_CreateCoCalendar( CoNbr, CoSettings, BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate )
    else
      errMessage := 'Please check if Evo Server "' + FEvoConnection.APIServerAddress + '" is available!';
    Break;
  except
    on e: Exception do
    begin
      FEvoAPI := nil;
      RetryCount := RetryCount + 1;
      errMessage := e.Message;
    end;
  end;

  if errMessage <> '' then
    FLogger.LogError('Can not call Evo API SB_CreateCoCalendar method', errMessage);
end;

procedure TDEMSXmlRPCServer.SetActive(AValue: boolean);
begin
  if FRpcServer.Active <> AValue then
  begin
    if AValue then
    begin
      if ConnectToEvo then
      begin
        FRpcServer.ListenPort := 9944;
        FRpcServer.EnableIntrospect := True;
        FRpcServer.Active := True;
      end;  
    end
    else
      FRpcServer.Active := False;
  end;
end;

function TDEMSXmlRPCServer.TaskQueue_RunCreatePayroll(
  aParams: IRpcStruct): string;
var
  RetryCount: integer;
  errMessage: string;
begin
  RetryCount := 0;
  errMessage := '';
  Result := '';
  while RetryCount < MaxRetryCount do
  try
    if not Assigned(FEvoAPI) then
      ConnectToEvo;

    if Assigned(FEvoAPI) then
      Result := FEvoAPI.TaskQueue_RunCreatePayroll( aParams )
    else
      errMessage := 'Please check if Evo Server "' + FEvoConnection.APIServerAddress + '" is available!';
    Break;
  except
    on e: Exception do
    begin
      FEvoAPI := nil;
      RetryCount := RetryCount + 1;
      errMessage := e.Message;
    end;
  end;

  if errMessage <> '' then
    FLogger.LogError('Can not call Evo API TaskQueue_RunCreatePayroll method', errMessage);
end;

procedure TDEMSXmlRPCServer.testGetSystemTimeMethod(
  const AParams: TList; const AReturn: TRpcReturn;
  const AMethodName: String);
begin
// getSystemTime()
// Return: datetime - current system time

  APICheckCondition(AParams.Count = 0, WrongSignatureException);
  AReturn.AddItem( DateToStr(Now) );
end;

procedure TDEMSXmlRPCServer.VerifyStructMember(aStruct: IRpcStruct;
  const aName: string; aDataType: TDataType; Mandatory: boolean = True);
begin
  if Mandatory then
    APICheckCondition(aStruct.KeyExists(aName), APIException, aName + ' is required');

  if aStruct.KeyExists(aName) then
    APICheckCondition(aStruct.Keys[aName].DataType = aDataType, APIException, aName + ' has a wrong type');
end;

end.
