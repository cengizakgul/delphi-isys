unit EvoDemsDBTables;

interface

uses
  common, kbmMemTable, DB, XmlRpcTypes, Classes, SysUtils;

type
  IEvoDemsDBTables = interface
    function TableExists(const aTable: string): boolean;
//    function VerifyRecord(const aTable: string; aFields: IRpcStruct): boolean;
  end;

  TEvoDemsDBTables = class(TInterfacedObject, IEvoDemsDBTables)
  private
    FDBTables: TkbmCustomMemTable;

    procedure CreateFields;
    procedure AddTableToDBTables(aID: integer; const aName: string);
  public
    function TableExists(const aTable: string): boolean;
//    function VerifyRecord(const aTable: string; aFields: IRpcStruct): boolean;

    constructor Create;
    destructor Destroy; override;

    procedure Init;
  end;

implementation


//from sRemotePayrollUtils
function CreateStringField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string; aSize: integer): TStringField;
var
  TS: TStringField;
begin
  TS := TStringField.Create(aDataSet);
  with TS do
  begin
    FieldName := aFieldName;
    Size := aSize;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;
  result := TS;
end;

function CreateIntegerField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TIntegerField;
var
  TI: TIntegerField;
begin
  TI := TIntegerField.Create(aDataSet);
  with TI do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TI;
end;

{ TEvoDemsDBTables }

constructor TEvoDemsDBTables.Create;
begin
  FDBTables := TkbmCustomMemTable.Create(nil);
  CreateFields;
  FDBTables.Open;
  Init;
end;

procedure TEvoDemsDBTables.CreateFields;
begin
  CreateIntegerField(FDBTables, 'ID', 'ID');
  CreateStringField(FDBTables, 'TableName', 'Evo Table', 50);
  FDBTables.AddIndex('idxTableName', 'TableName', [ixUnique]);
end;

destructor TEvoDemsDBTables.Destroy;
begin
  if FDBTables.Active then
    FDBTables.Close;

  FreeAndNil( FDBTables );

  inherited;
end;

function TEvoDemsDBTables.TableExists(const aTable: string): boolean;
begin
  Result := FDBTables.Locate('TableName', UpperCase(aTable), []);
end;

procedure TEvoDemsDBTables.Init;
begin
  AddTableToDBTables(1, 'SB_ACCOUNTANT');
  AddTableToDBTables(2, 'SB_BANKS');
  AddTableToDBTables(3, 'CL');
  AddTableToDBTables(4, 'CL_BANK_ACCOUNT');
  AddTableToDBTables(5, 'CL_DELIVERY_METHOD');
  AddTableToDBTables(6, 'CL_DELIVERY_GROUP');
  AddTableToDBTables(7, 'CL_E_DS');
  AddTableToDBTables(8, 'CL_E_D_GROUPS');
  AddTableToDBTables(9, 'CL_E_D_GROUP_CODES');
  AddTableToDBTables(10, 'CL_BILLING');
  AddTableToDBTables(11, 'CL_E_D_LOCAL_EXMPT_EXCLD');
  AddTableToDBTables(12, 'CL_E_D_STATE_EXMPT_EXCLD');
  AddTableToDBTables(13, 'CL_PERSON');
  AddTableToDBTables(14, 'CO');
  AddTableToDBTables(15, 'CO_E_D_CODES');
  AddTableToDBTables(16, 'CO_LOCAL_TAX');
  AddTableToDBTables(17, 'CO_PHONE');
  AddTableToDBTables(18, 'CO_STATES');
  AddTableToDBTables(19, 'CO_SUI');
  AddTableToDBTables(20, 'EE');
  AddTableToDBTables(21, 'EE_DIRECT_DEPOSIT');
  AddTableToDBTables(22, 'EE_LOCALS');
  AddTableToDBTables(23, 'EE_RATES');
  AddTableToDBTables(24, 'EE_SCHEDULED_E_DS');
  AddTableToDBTables(25, 'EE_STATES');
  AddTableToDBTables(26, 'CO_AUTO_ENLIST_RETURNS');
  AddTableToDBTables(27, 'CO_ENLIST_GROUPS');
  AddTableToDBTables(28, 'CO_REPORTS');
  AddTableToDBTables(29, 'CO_DIVISION');
  AddTableToDBTables(30, 'CO_BRANCH');
  AddTableToDBTables(31, 'CO_DEPARTMENT');
  AddTableToDBTables(32, 'CO_TEAM');
  AddTableToDBTables(33, 'CL_E_D_STATE_EXMPT_EXCLD');
  AddTableToDBTables(34, 'CL_E_D_LOCAL_EXMPT_EXCLD');
  AddTableToDBTables(35, 'CO_TEAM_PR_BATCH_DEFLT_ED');
  AddTableToDBTables(36, 'CL_AGENCY');
  AddTableToDBTables(37, 'CO_BILLING_HISTORY');
end;

procedure TEvoDemsDBTables.AddTableToDBTables(aID: integer;
  const aName: string);
begin
  FDBTables.Append;
  FDBTables.FieldByName('ID').AsInteger := aID;
  FDBTables.FieldByName('TableName').AsString := aName;

  try
    FDBTables.Post;
  except
    FDBTables.Cancel;
  end;
end;

end.
