unit EvoDemsCommon;

interface

uses XmlRpcTypes, Classes, SysUtils;

  function BuildSQL(aTableName: string; aFields: IRpcStruct): string;
  function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;

implementation

function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := Copy(MyString, 1, MyLength);
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

function GetFieldCondition(aFieldName: string; aField: IRpcStructItem): string;
begin
  Result := '';
  case aField.DataType of
  dtInteger:
    Result := aFieldName + '=' + IntToStr(aField.AsInteger);
  dtString: begin
    if aFieldName = 'CUSTOM_EMPLOYEE_NUMBER' then
      Result := aFieldName + '=' + QuotedStr(PadLeft(aField.AsString, ' ', 9))
    else if (aFieldName = 'CUSTOM_DIVISION_NUMBER') or (aFieldName = 'CUSTOM_BRUNCH_NUMBER') or
      (aFieldName = 'CUSTOM_DEPARTMENT_NUMBER') or (aFieldName = 'CUSTOM_TEAM_NUMBER') then
      Result := aFieldName + '=' + QuotedStr(PadLeft(aField.AsString, ' ', 20))
    else
      Result := aFieldName + '=' + QuotedStr(aField.AsString);
  end;
  dtDateTime:
    Result := aFieldName + ' = cast('''+ FormatDateTime('yyyy-mm-dd hh:nn:ss.zzzz', aField.AsDateTime) + ''' as timestamp)';
  dtFloat:
    Result := aFieldName + '=' + FloatToStr(aField.AsFloat);
  {add the other types}
  else
    Result := '1 <> 1';
  end;
  Result := '(' + Result + ')';
end;

function BuildSQL(aTableName: string; aFields: IRpcStruct): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to aFields.Count - 1 do
    Result := Result + ' and ' + GetFieldCondition(aFields.KeyList[i], aFields.Items[i]);

  if Result <> '' then
    Result := ' where ' + Copy(Result, 6, Length(Result))
  else // if there are no parameters to build where clause
    Result := ' where (1=1)';

  if (UpperCase(aTableName) <> 'SY_CHANGE_LOG') and (UpperCase(Copy(aTableName, 1, 3)) <> 'TMP') then
    Result := Result + ' and {AsOfNow<'+aTableName+'>}';

  Result := 'select ' + aTableName + '_NBR from ' + aTableName + Result;
end;

end.
