unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientMainForm, ComCtrls, OptionsBaseFrame, evodata,
  EvoAPIConnectionParamFrame, ExtCtrls, ActnList, StdCtrls, EvoDemsDBTables,
  Buttons, EvoDEMSProc, DemsXmlRpcServerMod, XmlRpcTypes;

type
  TMainFm = class(TEvoAPIClientMainFm)
    pnlMain: TPanel;
    Actions: TActionList;
    ConnectToEvo: TAction;
    btnConnect: TBitBtn;
    btnCreateNewClient: TBitBtn;
    createNewClientDB: TAction;
    lblNewClient: TLabel;
    pnlGetRecordNbr: TPanel;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    edTableName: TEdit;
    Label2: TLabel;
    edF1Name: TEdit;
    edF1Value: TEdit;
    Label3: TLabel;
    edF2Name: TEdit;
    edF2Value: TEdit;
    Label4: TLabel;
    edF3Name: TEdit;
    edF3Value: TEdit;
    GetRecordNbr: TAction;
    lblGetRecordNbrResult: TLabel;
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure createNewClientDBUpdate(Sender: TObject);
    procedure createNewClientDBExecute(Sender: TObject);
    procedure GetRecordNbrExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    FEvoDemsProc: TEvoDemsProc;
    FEvoDemsDBTables: TEvoDemsDBTables;
    FDemsXmlRpcServer: TDEMSXmlRPCServer;

    procedure AutoConnect;
    procedure RunDemsXmlRpcServer;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}
uses EEUtilityLoggerViewFrame, gdyDeferredCall, gdyRedir, gdyGlobalWaitIndicator;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;

  FDemsXmlRpcServer := TDEMSXmlRPCServer.Create(Logger);
  FEvoDemsProc := TEvoDemsProc.Create(Logger);
  FEvoDemsDBTables := TEvoDemsDBTables.Create;

//  FDemsXmlRpcServer.EvoDemsProc := FEvoDemsProc;
  FDemsXmlRpcServer.EvoDemsDBTables := FEvoDemsDBTables;

  if EvoFrame.IsValid then
    PageControl1.TabIndex := TabSheet2.TabIndex
  else
    PageControl1.TabIndex := TabSheet3.TabIndex;
end;

destructor TMainFm.Destroy;
begin
  SaveLogArchiveTo( Redirection.GetDirectory('MyAppData') );
  FreeAndNil(FDemsXmlRpcServer);

  inherited;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  AutoConnect;
end;

procedure TMainFm.createNewClientDBUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := Assigned(FEvoDemsProc) and (FEvoDemsProc.Connected);
end;

procedure TMainFm.createNewClientDBExecute(Sender: TObject);
begin
  inherited;
  try
    lblNewClient.Caption := 'Done! New Client internal number is "' + IntToStr(FEvoDemsProc.createNewClientDB) + '"';
  except
    on E: Exception do
      lblNewClient.Caption := e.Message;
  end;
end;

procedure TMainFm.RunDemsXmlRpcServer;
begin
  if FDemsXmlRpcServer.Active then
    FDemsXmlRpcServer.Active := False;
    
  FDemsXmlRpcServer.EvoConnection := EvoFrame.Param;
  FDemsXmlRpcServer.Active := True;
end;

procedure TMainFm.GetRecordNbrExecute(Sender: TObject);
var
  Params: IRpcStruct;
  Fields: IRpcStruct;
begin
  Params := TRpcStruct.Create;
  Fields := TRpcStruct.Create;

  if (Trim(edF1Name.Text) <> '') then
    Fields.AddItem(edF1Name.Text, edF1Value.Text);
  if (Trim(edF2Name.Text) <> '') then
    Fields.AddItem(edF2Name.Text, edF2Value.Text);
  if (Trim(edF3Name.Text) <> '') then
    Fields.AddItem(edF3Name.Text, edF3Value.Text);

  Params.AddItem('TableName', edTableName.Text);
  Params.AddItem('Fields', Fields);
  try
    lblGetRecordNbrResult.Caption := 'Done! The record nbr is "' + IntToStr(FEvoDemsProc.GetRecordNbr(Params)) + '"';
  except
    on E: Exception do
      lblNewClient.Caption := e.Message;
  end;
end;

procedure TMainFm.FormShow(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePage := TabSheet3;

//  DeferredCall( RunDemsXmlRpcServer );
//  DeferredCall( AutoConnect );
  AutoConnect;
end;

procedure TMainFm.AutoConnect;
begin
  WaitIndicator.StartWait('Connecting to Evolution');
  Logger.LogEntry( 'Auto Connect' );
  try
    if Assigned( FEvoDemsProc ) then
    try
      RunDemsXmlRpcServer;

      btnConnect.Caption := 'Reconnect to Evo';
    except
      btnConnect.Caption := 'Connect to Evo';
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
    WaitIndicator.EndWait;
  end;
end;

end.
