unit EvoDemsProc;

interface

uses
  common, evoapiconnection, gdyCommonLogger, kbmMemTable, DB, evodata,
  XmlRpcTypes, evoapiconnectionutils;

type
  IEvoDemsProc = interface
    function GetRecordNbr(aTableFields: IRpcStruct): integer;
    function createNewClientDB: integer;
    function applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
    function SB_CreateCoCalendar(CoNbr: integer; CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: string): IRpcArray;
    function TaskQueue_RunCreatePayroll(aParams: IRpcStruct): string;
    procedure OpenClient(cl_nbr: integer);
    function OpenSQL(SQLText: string; cl_nbr: integer; MapFields: string): TkbmCustomMemTable;
  end;

  TEvoDemsProc = class(TInterfacedObject, IEvoDemsProc)
  private
    FConn: IEvoAPIConnection;
    FLogger: ICommonLogger;
  public
    function createNewClientDB: integer;
    function GetRecordNbr(aTableFields: IRpcStruct): integer;
    function applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
    function SB_CreateCoCalendar(CoNbr: integer; CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: string): IRpcArray;
    function TaskQueue_RunCreatePayroll(aParams: IRpcStruct): string;
    procedure OpenClient(cl_nbr: integer);
    function OpenSQL(SQLText: string; cl_nbr: integer; MapFields: string): TkbmCustomMemTable;

    function Connected: boolean;
    function Connection: IEvoAPIConnection;
    procedure Connect(param: TEvoAPIConnectionParam);
    procedure Disconnect;

    constructor Create(Logger: ICommonLogger);
    destructor Destroy; override;
  end;

implementation

uses SysUtils, Variants, DateUtils, gdyRedir, gdyGlobalWaitIndicator;

{ TEvoDemsProc }

function TEvoDemsProc.applyDataChangePacket(changes: IRpcArray; aAsOF: TDateTime = 0): IRpcStruct;
begin
  Result := nil;
  if Changes.Count = 0 then
    FLogger.LogWarning('Data change packet should not be empty.')
  else begin
    Result := FConn.applyDataChangePacket( Changes, 0{aAsOF} );
  end;
end;

procedure TEvoDemsProc.Connect(param: TEvoAPIConnectionParam);
begin
  Disconnect;

  WaitIndicator.StartWait('Connecting to Evolution');
  try
    FConn := CreateEvoAPIConnection(param, FLogger);
  finally
    WaitIndicator.EndWait;
  end;
end;

function TEvoDemsProc.Connected: boolean;
begin
  Result := assigned(FConn);
end;

function TEvoDemsProc.Connection: IEvoAPIConnection;
begin
  Assert(FConn <> nil);
  Result := FConn;
end;

constructor TEvoDemsProc.Create(Logger: ICommonLogger);
begin
  FLogger := Logger;
end;

destructor TEvoDemsProc.Destroy;
begin
  FConn := nil;
  FLogger := nil;
  inherited;
end;

procedure TEvoDemsProc.Disconnect;
begin
  FConn := nil;
end;

function TEvoDemsProc.GetRecordNbr(aTableFields: IRpcStruct): integer;
var
  TableName: string;
  Params: IRpcStruct;
  lQuery: TkbmCustomMemTable;
begin
  Result := -1;
  if (aTableFields.Count <> 2) then
    FLogger.LogWarning('Wrong parameters structure for GetRecordNbr.')
  else if aTableFields.Items[0].DataType <> dtString then
    FLogger.LogWarning('Wrong table name parameter type for GetRecordNbr. Should be <string>.')
  else begin
    TableName := Redirection.GetDirectory(sQueryDirAlias) + aTableFields.Items[0].AsString + '.rwq';
    if not FileExists( TableName ) then
      FLogger.LogWarning('The QB query "' + TableName + '" has not been found!')
    else begin
      Params := aTableFields.Items[1].AsStruct;
      if Params.Count <= 0 then
        FLogger.LogWarning('There are no field names and values for GetRecordNbr.')
      else begin
        lQuery := FConn.RunQuery( TableName, Params);
        try
          if (lQuery.RecordCount > 0) and Assigned(lQuery.FindField('KEY')) then
            Result := lQuery.FieldByName('KEY').AsInteger;
        finally
          FreeAndNil(lQuery);
        end;
      end;
    end;
  end;
end;

function TEvoDemsProc.createNewClientDB: integer;
begin
  Result := FConn.createNewClientDB;
end;

procedure TEvoDemsProc.OpenClient(cl_nbr: integer);
begin
  FConn.OpenClient(cl_nbr);
end;

function TEvoDemsProc.OpenSQL(SQLText: string; cl_nbr: integer;
  MapFields: string): TkbmCustomMemTable;
begin
  Result := FConn.OpenSQL(SQLText, cl_nbr, MapFields);
end;

function TEvoDemsProc.SB_CreateCoCalendar(CoNbr: integer;
  CoSettings: IRpcStruct; const BasedOn, MoveCheckDate, MoveCallInDate,
  MoveDeliveryDate: string): IRpcArray;
begin
  Result := FConn.SB_CreateCoCalendar( CoNbr, CoSettings, BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate );
end;

function TEvoDemsProc.TaskQueue_RunCreatePayroll(
  aParams: IRpcStruct): string;
begin
  Result := FConn.TaskQueue_RunCreatePayroll( aParams );
end;

end.
