program EvoDems;
                                                   
uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TFrame},
  sevenzip in '..\..\common\gdycommon\sevenzip\sevenzip.pas',
  MainForm in 'MainForm.pas' {MainFm},
  EvoDemsProc in 'EvoDemsProc.pas',
  DemsXmlRpcServerMod in 'DemsXmlRpcServerMod.pas',
  EvoDemsDBTables in 'EvoDemsDBTables.pas',
  EvoDemsCommon in 'EvoDemsCommon.pas',
  evoapiconnection in '..\..\common\evoAPIConnection.pas';

{$R *.res}

begin
  LicenseKey := '14742D293AF449349C0776F42D25F8C4'; // EvoDEMS License
//  LicenseKey := '{5E465AFC-D0A1-424E-B21B-4351ECD6BFFF}'; //evo.legacy.*

  Application.Initialize;
  Application.Title := 'DEMS Evo Server';
  Application.CreateForm(TMainFm, MainFm);
  Application.Run;
end.
