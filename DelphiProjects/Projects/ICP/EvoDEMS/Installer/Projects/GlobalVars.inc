<Include>

<!-- Global Variables -->
<?define softwareName = "EvoUtils" ?>
<?define productManufacturer = "ISystems, LLC." ?>
<?define productManufacturerAbbr = "iSystems" ?>
<?define defaultVolume = "C:\Program Files\" ?>
<?define sourceFileDir = "..\..\..\..\..\Bin\ICP\EvoDEMS\" ?>
<?define resourceFileDir = "..\..\..\..\Evolution\Installer\Resources\" ?>
<?define customActionDLL = $(var.resourceFileDir)isMsiCustomAction.dll ?>

</Include>
