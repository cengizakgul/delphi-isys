[Directories]
Queries=%AppDir%\queries

MyLocalAppData=%AppDir%
MyAppData=%AppDir%

LogArchives=%MyLocalAppData%\To Send

;not in production version
DumpDir=%AppDir%\log

[Filenames]
Certificate=%AppDir%\cert.pem
RootCertificate=%AppDir%\root.pem
KeyCertificate=%AppDir%\key.pem

SevenZipDll=%AppDir%\7za.dll

Log=%MyAppData%\user.log

;used only by beta version
DebugLog=%MyLocalAppData%\debug.log
