#define APPBASE "EvoADI"
#define APPNAME "Evolution ADI Import Advanced"
#define VERSION GetFileVersion(SourcePath + "\..\..\..\..\bin\ICP\"+APPBASE+"\"+APPBASE+".exe")
#define APPVERNAME "Evolution ADI Import "+VERSION+" Advanced"

#include <..\..\Common\Installer\common.iss>
