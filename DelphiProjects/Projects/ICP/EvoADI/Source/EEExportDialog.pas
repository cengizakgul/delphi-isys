unit EEExportDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, ExtCtrls, EEFilterFrame, ADIEEOptionsFrame,
  OptionsBaseFrame;

type
  TEEExportDlg = class(TDialogBase)
    EEExportOptionsFrame: TADIEEOptionsFrm;
    EeFilterFrame: TEeFilterFrm;
  private
  public
    constructor Create( Owner: TComponent ); override;
    function CheckBeforeClose: boolean; override;
  end;


implementation

{$R *.dfm}

{ TEEExportDlg }

function TEEExportDlg.CheckBeforeClose: boolean;
begin
  EeFilterFrame.Check;
  Result := true;
end;

constructor TEEExportDlg.Create(Owner: TComponent);
begin
  inherited;
  Caption := 'Employee Export Options';
end;

end.
