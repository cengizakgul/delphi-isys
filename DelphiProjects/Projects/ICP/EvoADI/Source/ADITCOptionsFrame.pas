unit ADITCOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, adidecl, StdCtrls, ExtCtrls, bndRetrieveAllPayGroups,
  RateImportOptionFrame;

type
  TADITCOptionsFrm = class(TFrame)
    RateImportOptionFrame: TRateImportOptionFrm;
    cbTransferTempHierachy: TCheckBox;
    cbSummarize: TCheckBox;
    cbAllowImport: TCheckBox;
    procedure cbSummarizeClick(Sender: TObject);
    procedure cbTransferTempHierachyClick(Sender: TObject);
    procedure RateImportOptionFrm1RadioGroup1Click(Sender: TObject);
    procedure cbAllowImportClick(Sender: TObject);
  private
    FOptions: TADITCOptions;
    procedure SetOptions(const Value: TADITCOptions);
    procedure DataUpdated;
    procedure RateImportOptionChanged;
  public
    property Options: TADITCOptions read FOptions write SetOptions;
  end;

implementation

uses XMLIntf;

{$R *.dfm}

procedure TADITCOptionsFrm.SetOptions(const Value: TADITCOptions);
begin
  FOptions := Value;
  RateImportOptionChanged;//calls DataUpdated
end;

procedure TADITCOptionsFrm.DataUpdated;
begin
  cbSummarize.Checked := FOptions.Summarize;
  cbTransferTempHierachy.Checked := FOptions.TransferTempHierarchy;
  RateImportOptionFrame.Value := FOptions.RateImport;
  cbAllowImport.Checked := FOptions.AllowImportToBatchesWithUserEarningLines;

  cbTransferTempHierachy.Enabled := FOptions.RateImport <> rateUseEvoEE;
end;

procedure TADITCOptionsFrm.cbSummarizeClick(Sender: TObject);
begin
  FOptions.Summarize := cbSummarize.Checked;
end;

procedure TADITCOptionsFrm.cbTransferTempHierachyClick(Sender: TObject);
begin
  FOptions.TransferTempHierarchy := cbTransferTempHierachy.Checked;
end;

procedure TADITCOptionsFrm.RateImportOptionChanged;
begin
  if FOptions.RateImport = rateUseEvoEE then
    FOptions.TransferTempHierarchy := true;
  DataUpdated;
end;

procedure TADITCOptionsFrm.RateImportOptionFrm1RadioGroup1Click(
  Sender: TObject);
begin
  FOptions.RateImport := RateImportOptionFrame.Value;
  RateImportOptionChanged;
end;

procedure TADITCOptionsFrm.cbAllowImportClick(Sender: TObject);
begin
  FOptions.AllowImportToBatchesWithUserEarningLines := cbAllowImport.Checked;
end;

end.
