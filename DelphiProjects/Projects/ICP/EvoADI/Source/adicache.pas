unit adicache;

interface

uses
  adiconnection, adidecl, kbmMemTable, issettings, db, gdyCommonLogger, classes;

type
  TADICache = class (TComponent)
  private
    FLogger: ICommonLogger;
    FParamUsed: TADIConnectionParam;
    FConnection: IADIConnection;
  public
    function GetConnection(param: TADIConnectionParam): IADIConnection;
  end;

  TADIConnections = class(TComponent) //inherits TComponent only for owning TADICache objects
  private
    FLogger: ICommonLogger;
    FConns: TkbmMemTable;
    FConns2: TkbmMemTable;
    FConnsPrivateView: TkbmMemTable;
//    FConnsDS: TDataSource;

    function GetCache(ds: TDataSet): TADICache;
    procedure SetCache(ds: TDataSet; const Value: TADICache);

    function GetConnection(guid: string): IADIConnection;
    procedure HandleAfterInsert(DataSet: TDataSet);
    procedure HandleBeforeDelete(DataSet: TDataSet);

    procedure AddConnection(guid: string; param: TADIConnectionParam);
    //    procedure HandleDataChange(Sender: TObject; Field: TField);
  public
    constructor Create(Logger: ICommonLogger); reintroduce;
    destructor Destroy; override;
    procedure Load(conf: IisSettings);
    procedure Save(conf: IisSettings);
    function HasCompleteParams(guid: string): boolean;
    procedure ForceSavePassword(guid: string);

    property DS: TkbmMemTable read FConns; //OK to be filtered by user
    property DS2: TkbmMemTable read FConns2; //OK to be filtered by user

    property Connection[guid: string]: IADIConnection read GetConnection; default;
  end;

function ADIConnectionUniquePath(guid: string): string;

implementation

uses
  common, sysutils, comobj, isbaseclasses, variants;

{ TADIConnections }

constructor TADIConnections.Create(Logger: ICommonLogger);
begin
  inherited Create(nil);
  FLogger := Logger;
  
  FConns := TkbmMemTable.Create(nil);
  CreateStringField( FConns, 'GUID', 'Internal ID', 38);
  CreateStringField( FConns, 'USER_NAME', 'User name', 256);
  CreateStringField( FConns, 'PASSWORD', 'Password', 256);
  CreateStringField( FConns, 'NEW_PASSWORD', 'Password', 256);
  with TBooleanField.Create(FConns) do
  begin
    FieldName := 'SAVE_PASSWORD';
    DisplayLabel := 'Save password';
    DataSet := FConns;
  end;
  CreateStringField( FConns, 'CLIENT_URL', 'Client URL', 1024);
  CreateIntegerField(FConns, 'CACHE_PTR', 'CACHE_PTR').Visible := false;


//  FConnsDS := TDataSource.Create(FConns);
//  FConnsDS.DataSet := FConns;
//  FConnsDS.OnDataChange := HandleDataChange;
  FConnsPrivateView := TkbmMemTable.Create(FConns);
  FConnsPrivateView.AttachedTo := FConns;

  FConns2 := TkbmMemTable.Create(FConns);
  FConns2.AttachedTo := FConns;

  FConns.AfterInsert := HandleAfterInsert;
  FConns.BeforeDelete := HandleBeforeDelete;
  FConns2.AfterInsert := HandleAfterInsert;
  FConns2.BeforeDelete := HandleBeforeDelete;
  FConnsPrivateView.AfterInsert := HandleAfterInsert;
  FConnsPrivateView.BeforeDelete := HandleBeforeDelete;

  FConns.Open;
  FConnsPrivateView.Open;
  FConns2.Open;
end;

procedure TADIConnections.HandleAfterInsert(DataSet: TDataSet);
begin
  SetCache(DataSet, TADICache.Create(Self));
  GetCache(DataSet).FLogger := FLogger;

  DataSet['SAVE_PASSWORD'] := false;
  DataSet['GUID'] := CreateClassID;
end;

procedure TADIConnections.HandleBeforeDelete(DataSet: TDataSet);
begin
  GetCache(DataSet).Free;
end;
{
procedure TADIConnections.HandleDataChange(Sender: TObject; Field: TField);
begin

end;
}

destructor TADIConnections.Destroy;
begin
  FreeAndNil(FConns); //cache objects are owned by this object
  inherited;
end;

function MakeParams(ds: TDataSet): TADIConnectionParam;
begin
  Result.ClientURL := ds.FieldByName('CLIENT_URL').AsString;
  Result.UserName := ds.FieldByName('USER_NAME').AsString;
  if ds.FieldByName('NEW_PASSWORD').AsString = StringOfChar(' ', Length(ds.FieldByName('PASSWORD').AsString) ) then
    Result.Password := ds.FieldByName('PASSWORD').AsString
  else
    Result.Password := ds.FieldByName('NEW_PASSWORD').AsString;
  Result.SavePassword := ds.FieldByName('SAVE_PASSWORD').AsBoolean;
end;

function TADIConnections.GetConnection(guid: string): IADIConnection;
begin
  guid := trim(guid);
  Assert(guid <> '');
  if FConnsPrivateView.Locate('GUID', guid, []) then
    Result := GetCache(FConnsPrivateView).GetConnection( MakeParams(FConnsPrivateView) )
  else
    Assert(false);
end;

function TADIConnections.HasCompleteParams(guid: string): boolean;
begin
  guid := trim(guid);
  Assert(guid <> '');
  if FConnsPrivateView.Locate('GUID', guid, []) then
    Result := IsCompleteADIConnectionParam( MakeParams(FConnsPrivateView) )
  else
  begin
    Assert(false);
    Result := false; //to make compiler happy
  end;
end;

procedure TADIConnections.AddConnection(guid: string; param: TADIConnectionParam);
begin
  FConns.Append;
  try
    FConns.FieldByName('GUID').AsString := guid;
    FConns.FieldByName('USER_NAME').AsString := param.UserName;
    FConns.FieldByName('PASSWORD').AsString := param.Password;
    FConns.FieldByName('NEW_PASSWORD').AsString := StringOfChar(' ', Length(FConns.FieldByName('PASSWORD').AsString) );
    FConns.FieldByName('SAVE_PASSWORD').AsBoolean := param.SavePassword;
    FConns.FieldByName('CLIENT_URL').AsString := param.ClientURL;
    FConns.Post;
  except
    FConns.Cancel; //cache objects are owned by this object, no memory leaks
    raise;
  end;
end;

function ADIConnectionUniquePath(guid: string): string;
begin
  Assert(guid <> '');
  Assert(trim(guid) = guid);
  Result := 'ADIConnections\' + guid;
end;

procedure TADIConnections.Load(conf: IisSettings);
var
  i: integer;
  childs: IisStringListRO;
  root: string;
begin
  root := 'ADIConnections';
  childs := conf.GetChildNodes(root);
  FConns.EmptyTable;
  root := root + '\';
  for i := 0 to childs.Count-1 do
    try
      AddConnection( childs[i], LoadADIConnectionParam(FLogger, conf, root + childs[i] +'\ADIConnection') );
    except
      FLogger.StopException;
    end;
end;

procedure TADIConnections.Save(conf: IisSettings);
var
  i: integer;
  childs: IisStringListRO;
  root: string;
begin
  root := 'ADIConnections';
  childs := conf.GetChildNodes(root);
  root := root + '\';

  for i := 0 to childs.Count-1 do
    if VarIsNull(FConnsPrivateView.Lookup('GUID', childs[i], 'GUID')) then
      conf.DeleteNode(root + childs[i]);

  FConnsPrivateView.First;
  while not FConnsPrivateView.Eof do
  begin
    try
      SaveADIConnectionParam(FLogger, MakeParams(FConnsPrivateView), conf, root + FConnsPrivateView['GUID'] + '\ADIConnection');
    except
      FLogger.StopException;
    end;
    FConnsPrivateView.Next;
  end;
end;

function TADIConnections.GetCache(ds: TDataSet): TADICache;
begin
  Assert( ds.FieldByName('CACHE_PTR').AsInteger <> 0);
  Result := Pointer(ds.FieldByName('CACHE_PTR').AsInteger);
end;

procedure TADIConnections.SetCache(ds: TDataSet; const Value: TADICache);
begin
  ds.FieldByName('CACHE_PTR').AsInteger := Integer(Pointer(Value));
end;

procedure TADIConnections.ForceSavePassword(guid: string);
begin
  guid := trim(guid);
  Assert(guid <> '');

  if not FConnsPrivateView.Locate('GUID', guid, []) then
    Assert(false);

  FConnsPrivateView.Edit;
  try
    FConnsPrivateView['SAVE_PASSWORD'] := true;
    FConnsPrivateView.Post;   //the grid connected to DS gets refreshed, don't know why though
  except
    FConnsPrivateView.Cancel;
    raise;
  end;
end;

{ TADICache }

function TADICache.GetConnection(param: TADIConnectionParam): IADIConnection;
begin
  Assert(FLogger <> nil);
  if FConnection = nil then
  begin
    FConnection := CreateADIConnection(param, FLogger);
    FParamUsed := param;
  end
  else
    Assert( IsSameADIConnectionParam(param, FParamUsed) );
  Result := FConnection;
end;

end.
