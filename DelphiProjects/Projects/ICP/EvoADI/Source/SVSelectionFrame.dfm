object SVSelectionFrm: TSVSelectionFrm
  Left = 0
  Top = 0
  Width = 475
  Height = 406
  TabOrder = 0
  object dgEE: TReDBGrid
    Left = 0
    Top = 0
    Width = 475
    Height = 406
    DisableThemesInTitle = False
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsEE
    KeyOptions = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
  end
  object dbcbUserID: TwwDBLookupCombo
    Left = 192
    Top = 192
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    DataField = 'USER_NAME'
    DataSource = dsEE
    TabOrder = 1
    AutoDropDown = False
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = True
    OnBeforeDropDown = dbcbUserIDBeforeDropDown
    OnCloseUp = dbcbUserIDCloseUp
  end
  object dsSV: TDataSource
    AutoEdit = False
    Left = 48
    Top = 32
  end
  object dsEE: TDataSource
    Left = 56
    Top = 96
  end
end
