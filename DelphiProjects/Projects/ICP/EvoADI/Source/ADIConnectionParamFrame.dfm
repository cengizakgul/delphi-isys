object ADIConnectionParamFrm: TADIConnectionParamFrm
  Left = 0
  Top = 0
  Width = 751
  Height = 315
  TabOrder = 0
  object pnlControls: TPanel
    Left = 0
    Top = 192
    Width = 751
    Height = 123
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 751
      Height = 123
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 9
        Top = 12
        Width = 51
        Height = 13
        Caption = 'Client URL'
      end
      object Label4: TLabel
        Left = 9
        Top = 34
        Width = 51
        Height = 13
        Caption = 'User name'
      end
      object Label7: TLabel
        Left = 9
        Top = 59
        Width = 46
        Height = 13
        Caption = 'Password'
      end
      object Label2: TLabel
        Left = 547
        Top = 11
        Width = 171
        Height = 13
        Caption = '(like https://clientname.aditime.com)'
      end
      object Label3: TLabel
        Left = 10
        Top = 85
        Width = 672
        Height = 26
        Caption = 
          'User must be a member of a role that has full API access. To gra' +
          'nt API access open ADI web UI,'#13'go to Configurations - Administra' +
          'tion - Security Role page, click on a role then go to Configurat' +
          'ion - Administration tab and allow full API Access.'
        WordWrap = True
      end
      object dbClientURL: TDBEdit
        Left = 76
        Top = 9
        Width = 464
        Height = 21
        DataField = 'CLIENT_URL'
        DataSource = dsConn
        TabOrder = 0
      end
      object dbUsername: TDBEdit
        Left = 76
        Top = 33
        Width = 152
        Height = 21
        DataField = 'USER_NAME'
        DataSource = dsConn
        TabOrder = 1
      end
      object dbPassword: TDBEdit
        Left = 76
        Top = 57
        Width = 152
        Height = 21
        DataField = 'NEW_PASSWORD'
        DataSource = dsConn
        PasswordChar = '*'
        TabOrder = 2
      end
      object dbSavePassword: TDBCheckBox
        Left = 243
        Top = 58
        Width = 262
        Height = 17
        Caption = 'Save password (mandatory for scheduling tasks)'
        DataField = 'SAVE_PASSWORD'
        DataSource = dsConn
        TabOrder = 3
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
    end
  end
  object pnlGrid: TPanel
    Left = 0
    Top = 0
    Width = 751
    Height = 192
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dbGrid: TReDBGrid
      Left = 0
      Top = 41
      Width = 751
      Height = 151
      DisableThemesInTitle = False
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsConn
      KeyOptions = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
      ReadOnly = True
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 751
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn1: TBitBtn
        Left = 8
        Top = 8
        Width = 70
        Height = 25
        Action = actInsert
        Caption = 'New'
        TabOrder = 0
        Glyph.Data = {
          76020000424D7602000000000000760000002800000040000000100000000100
          0400000000000002000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330008
          8333333333333333333333333333333333333333333333333333333333322008
          83333333333FF33333333333333FF33333333333333FF3333333333333AA2008
          833333333388F333333333333388F333333333333388F3333333333338AA2008
          888833333388F333333333333388F333333333333388F3333333333388AA2008
          888833333388F333333333333388F333333333333388F3333333333000AA2000
          000033333388F333333333333388F333333333333388F3333333330000AA2000
          000033333388F333333333333388F333333333333388F3333333322222AA2222
          22203FFFFF88FFFFFFF33FFFFF88FFFFFFF33FFFFF88FFFFFFF3AAAAAAAAAAAA
          AA2388888888888888F388888888888888F388888888888888F3AAAAAAAAAAAA
          AA33888888888888883388888888888888338888888888888833333333AA2008
          833333333388F333333333333388F333333333333388F3333333333333AA2008
          333333333388F333333333333388F333333333333388F3333333333333AA2003
          333333333388F333333333333388F333333333333388F3333333333333AA2033
          333333333388F333333333333388F333333333333388F3333333333333AA2333
          333333333388F333333333333388F333333333333388F3333333333333AA3333
          3333333333883333333333333388333333333333338833333333}
        NumGlyphs = 4
      end
      object BitBtn2: TBitBtn
        Left = 96
        Top = 8
        Width = 70
        Height = 25
        Action = actDelete
        Caption = 'Delete'
        TabOrder = 1
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333337777777
          7777333377777777777733300000000000003300000000000000311111111111
          1113999999999999993399999999999999333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
      end
    end
  end
  object dsConn: TDataSource
    OnDataChange = dsConnDataChange
    Left = 592
    Top = 152
  end
  object ActionList2: TActionList
    Left = 552
    Top = 40
    object actInsert: TAction
      Caption = 'New'
      OnExecute = actInsertExecute
      OnUpdate = actInsertUpdate
    end
    object actDelete: TAction
      Caption = 'Delete'
      OnExecute = actDeleteExecute
      OnUpdate = actDeleteUpdate
    end
  end
end
