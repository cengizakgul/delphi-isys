inherited EthnicityFrm: TEthnicityFrm
  Width = 598
  Height = 513
  inherited BinderFrm1: TBinderFrm
    Width = 598
    Height = 513
    inherited evSplitter2: TSplitter
      Top = 249
      Width = 598
    end
    inherited pnltop: TPanel
      Width = 598
      Height = 249
      inherited evSplitter1: TSplitter
        Height = 249
      end
      inherited pnlTopLeft: TPanel
        Height = 249
        inherited dgLeft: TReDBGrid
          Height = 224
        end
      end
      inherited pnlTopRight: TPanel
        Width = 328
        Height = 249
        inherited evPanel4: TPanel
          Width = 328
        end
        inherited dgRight: TReDBGrid
          Width = 328
          Height = 224
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 254
      Width = 598
      inherited pnlbottom: TPanel
        Width = 598
        inherited evPanel5: TPanel
          Width = 598
        end
        inherited dgBottom: TReDBGrid
          Width = 598
        end
      end
      inherited pnlMiddle: TPanel
        Width = 598
      end
    end
  end
  object cdEvoEthnicities: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 288
    Top = 240
    object cdEvoEthnicitiesCODE: TStringField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
      Size = 1
    end
    object cdEvoEthnicitiesNAME: TStringField
      DisplayLabel = 'Ethnicity'
      FieldName = 'NAME'
      Size = 60
    end
  end
  object cdADIRaces: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 288
    object cdADIRacesCODE: TStringField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
      Size = 1
    end
    object cdADIRacesDESCRIPTION: TStringField
      DisplayLabel = 'Race'
      DisplayWidth = 20
      FieldName = 'DESCRIPTION'
      Size = 40
    end
  end
end
