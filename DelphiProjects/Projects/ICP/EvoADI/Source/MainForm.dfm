inherited MainFm: TMainFm
  Left = 535
  Top = 138
  Width = 810
  Height = 620
  Caption = 'MainFm'
  Constraints.MinHeight = 620
  Constraints.MinWidth = 810
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 794
    Height = 522
    ActivePage = tbshEE
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 786
        inherited GroupBox1: TGroupBox
          Caption = 'Evolution connection'
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 106
        Width = 786
        Height = 388
        Align = alClient
        Caption = 'ADI connections'
        TabOrder = 1
        inline ADIFrame: TADIConnectionParamFrm
          Left = 2
          Top = 15
          Width = 782
          Height = 371
          Align = alClient
          TabOrder = 0
          inherited pnlControls: TPanel
            Top = 248
            Width = 782
            inherited Panel5: TPanel
              Width = 780
            end
          end
          inherited pnlGrid: TPanel
            Width = 782
            Height = 248
            inherited dbGrid: TReDBGrid
              Width = 780
              Height = 86
            end
            inherited Panel6: TPanel
              Width = 780
            end
          end
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 786
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        inline PerCompanyADIClientFrame: TPerCompanyADIClientFrm
          Left = 0
          Top = 0
          Width = 786
          Height = 33
          Align = alClient
          TabOrder = 0
          inherited Panel1: TPanel
            Height = 33
          end
          inherited Panel2: TPanel
            Width = 689
            Height = 33
            inherited DBLookupComboBox1: TDBLookupComboBox
              Top = 7
              Width = 688
            end
          end
          inherited dsConn2: TDataSource
            Left = 340
            Top = 8
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 33
        Width = 786
        Height = 461
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object pcMappings: TPageControl
          Left = 0
          Top = 0
          Width = 786
          Height = 461
          ActivePage = tbshHierarchy
          Align = alClient
          TabOrder = 0
          object tbshHierarchy: TTabSheet
            Caption = 'Hierarchy level mapping'
            ImageIndex = 1
            inline DBDTFrame: TDBDTFrm
              Left = 0
              Top = 0
              Width = 778
              Height = 433
              Align = alClient
              TabOrder = 0
              inherited BinderFrm1: TBinderFrm
                Width = 778
                Height = 433
                inherited evSplitter2: TSplitter
                  Top = 169
                  Width = 778
                end
                inherited pnltop: TPanel
                  Width = 778
                  Height = 169
                  inherited evSplitter1: TSplitter
                    Left = 169
                    Height = 169
                  end
                  inherited pnlTopLeft: TPanel
                    Width = 169
                    Height = 169
                    inherited evPanel3: TPanel
                      Width = 169
                    end
                    inherited dgLeft: TReDBGrid
                      Width = 169
                      Height = 144
                    end
                  end
                  inherited pnlTopRight: TPanel
                    Left = 174
                    Width = 594
                    Height = 169
                    inherited evPanel4: TPanel
                      Width = 594
                    end
                    inherited dgRight: TReDBGrid
                      Width = 594
                      Height = 144
                    end
                  end
                end
                inherited evPanel1: TPanel
                  Top = 174
                  Width = 778
                  inherited pnlbottom: TPanel
                    Width = 768
                    inherited evPanel5: TPanel
                      Width = 768
                    end
                    inherited dgBottom: TReDBGrid
                      Width = 768
                    end
                  end
                  inherited pnlMiddle: TPanel
                    Width = 768
                  end
                end
              end
            end
          end
          object tbshEEStatus: TTabSheet
            Caption = 'Employee status mapping'
            inline EeStatusFrame: TEeStatusFrm
              Left = 0
              Top = 0
              Width = 778
              Height = 433
              Align = alClient
              TabOrder = 0
              inherited BinderFrm1: TBinderFrm
                Width = 778
                Height = 433
                inherited evSplitter2: TSplitter
                  Top = 168
                  Width = 778
                end
                inherited pnltop: TPanel
                  Width = 778
                  Height = 168
                  inherited evSplitter1: TSplitter
                    Left = 185
                    Height = 102
                  end
                  inherited pnlTopLeft: TPanel
                    Width = 185
                    Height = 102
                    inherited evPanel3: TPanel
                      Width = 185
                    end
                    inherited dgLeft: TReDBGrid
                      Width = 185
                      Height = 77
                    end
                  end
                  inherited pnlTopRight: TPanel
                    Left = 190
                    Width = 586
                    Height = 102
                    inherited evPanel4: TPanel
                      Width = 586
                    end
                    inherited dgRight: TReDBGrid
                      Width = 586
                      Height = 77
                    end
                  end
                end
                inherited evPanel1: TPanel
                  Top = 173
                  Width = 778
                  Height = 260
                  inherited pnlbottom: TPanel
                    Width = 776
                    Height = 219
                    inherited evPanel5: TPanel
                      Width = 776
                    end
                    inherited dgBottom: TReDBGrid
                      Width = 776
                      Height = 194
                    end
                  end
                  inherited pnlMiddle: TPanel
                    Width = 776
                  end
                end
              end
            end
          end
          object tbshEthnicity: TTabSheet
            Caption = 'Ethnicity mapping'
            ImageIndex = 2
            inline EthnicityFrame: TEthnicityFrm
              Left = 0
              Top = 0
              Width = 778
              Height = 433
              Align = alClient
              TabOrder = 0
              inherited BinderFrm1: TBinderFrm
                Width = 778
                Height = 433
                inherited evSplitter2: TSplitter
                  Top = 169
                  Width = 778
                end
                inherited pnltop: TPanel
                  Width = 778
                  Height = 169
                  inherited evSplitter1: TSplitter
                    Height = 103
                  end
                  inherited pnlTopLeft: TPanel
                    Height = 103
                    inherited dgLeft: TReDBGrid
                      Height = 78
                    end
                  end
                  inherited pnlTopRight: TPanel
                    Width = 506
                    Height = 103
                    inherited evPanel4: TPanel
                      Width = 506
                    end
                    inherited dgRight: TReDBGrid
                      Width = 506
                      Height = 78
                    end
                  end
                end
                inherited evPanel1: TPanel
                  Top = 174
                  Width = 778
                  inherited pnlbottom: TPanel
                    Width = 776
                    inherited evPanel5: TPanel
                      Width = 776
                    end
                    inherited dgBottom: TReDBGrid
                      Width = 776
                    end
                  end
                  inherited pnlMiddle: TPanel
                    Width = 776
                  end
                end
              end
            end
          end
          object tbshAccrualTypes: TTabSheet
            Caption = 'Accrual type mapping'
            ImageIndex = 3
            inline TOAFrame: TTOAFrm
              Left = 0
              Top = 0
              Width = 778
              Height = 433
              Align = alClient
              TabOrder = 0
              inherited BinderFrm1: TBinderFrm
                Width = 778
                Height = 433
                inherited evSplitter2: TSplitter
                  Top = 169
                  Width = 778
                end
                inherited pnltop: TPanel
                  Width = 778
                  Height = 169
                  inherited evSplitter1: TSplitter
                    Height = 103
                  end
                  inherited pnlTopLeft: TPanel
                    Height = 103
                    inherited dgLeft: TReDBGrid
                      Height = 78
                    end
                  end
                  inherited pnlTopRight: TPanel
                    Width = 506
                    Height = 103
                    inherited evPanel4: TPanel
                      Width = 506
                    end
                    inherited dgRight: TReDBGrid
                      Width = 506
                      Height = 78
                    end
                  end
                end
                inherited evPanel1: TPanel
                  Top = 174
                  Width = 778
                  inherited pnlbottom: TPanel
                    Width = 776
                    inherited evPanel5: TPanel
                      Width = 776
                    end
                    inherited dgBottom: TReDBGrid
                      Width = 776
                    end
                  end
                  inherited pnlMiddle: TPanel
                    Width = 776
                  end
                end
              end
            end
          end
        end
      end
    end
    object tbshEE: TTabSheet [2]
      Caption = 'Employee export'
      ImageIndex = 4
      inline EEOptionsFrame: TADIEEOptionsFrm
        Left = 0
        Top = 324
        Width = 786
        Height = 129
        Align = alBottom
        TabOrder = 0
        inherited FieldsToExportFrame: TFieldsToExportFrm
          Width = 473
          Height = 129
          inherited clbFields: TCheckListBox
            Top = 23
            Width = 473
            Columns = 4
          end
          inherited Panel1: TPanel
            Width = 473
            Height = 23
          end
        end
        inherited Panel1: TPanel
          Left = 473
          Height = 129
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 453
        Width = 786
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object lblEEExportTaskParamInstruction: TLabel
          Left = 216
          Top = 16
          Width = 488
          Height = 13
          Caption = 
            'To change tasks parameters go to Scheduled Tasks tab, select a t' +
            'ask and click Edit Parameters button'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object BitBtn5: TBitBtn
          Left = 8
          Top = 8
          Width = 89
          Height = 25
          Action = RunEmployeeExport
          Caption = 'Run export'
          TabOrder = 0
        end
        object BitBtn1: TBitBtn
          Left = 112
          Top = 8
          Width = 81
          Height = 25
          Action = actScheduleEEExport
          Caption = 'Create task'
          TabOrder = 1
        end
      end
      inline EeFilterFrame: TEeFilterFrm
        Left = 0
        Top = 0
        Width = 786
        Height = 324
        Align = alClient
        TabOrder = 2
        inherited GroupBox1: TGroupBox
          Width = 786
          Height = 324
          inherited Panel1: TPanel
            Height = 307
            inherited EEStatusFilterFrame: TCodesFilterFrm
              Height = 94
              inherited GroupBox1: TGroupBox
                Height = 94
                inherited lbStatuses: TCheckListBox
                  Height = 51
                end
              end
            end
            inherited EEPositionStatusFilterFrame: TCodesFilterFrm
              Top = 94
            end
            inherited EESalaryFilterFrame: TEESalaryFilterFrm
              Top = 245
            end
          end
          inherited DBDTFilterFrame: TDBDTFilterFrm
            Width = 543
            Height = 307
            inherited GroupBox1: TGroupBox
              Width = 543
              Height = 307
              inherited Panel1: TPanel
                Width = 539
              end
              inherited dgDBDT: TReDBCheckGrid
                Width = 539
                Height = 256
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Timeclock data import'
      inherited pnlBottom: TPanel
        Top = 370
        Width = 786
        Height = 124
        inline TCOptionsFrame: TADITCOptionsFrm
          Left = 0
          Top = 0
          Width = 786
          Height = 83
          Align = alClient
          TabOrder = 0
        end
        object Panel2: TPanel
          Left = 0
          Top = 83
          Width = 786
          Height = 41
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object lblTCImportTaskInstruction: TLabel
            Left = 216
            Top = 16
            Width = 488
            Height = 13
            Caption = 
              'To change tasks parameters go to Scheduled Tasks tab, select a t' +
              'ask and click Edit Parameters button'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object BitBtn2: TBitBtn
            Left = 8
            Top = 8
            Width = 89
            Height = 25
            Action = RunAction
            Caption = 'Run import'
            TabOrder = 0
          end
          object BitBtn3: TBitBtn
            Left = 112
            Top = 8
            Width = 81
            Height = 25
            Action = actScheduleTCImport
            Caption = 'Create task'
            TabOrder = 1
          end
        end
      end
      inherited pnlPayrollBatch: TPanel
        Width = 786
        Height = 370
        inherited PayrollBatchFrame: TEvolutionPrPrBatchFrm
          Width = 786
          Height = 370
          inherited Splitter1: TSplitter
            Height = 370
          end
          inherited PayrollFrame: TEvolutionDsFrm
            Height = 370
            inherited dgGrid: TReDBGrid
              Height = 345
            end
          end
          inherited BatchFrame: TEvolutionDsFrm
            Width = 467
            Height = 370
            inherited Panel1: TPanel
              Width = 467
            end
            inherited dgGrid: TReDBGrid
              Width = 467
              Height = 345
            end
          end
        end
      end
    end
    object tbshTOAExport: TTabSheet [4]
      Caption = 'TOA export'
      ImageIndex = 7
      object BitBtn6: TBitBtn
        Left = 8
        Top = 16
        Width = 97
        Height = 25
        Action = actTOAExport
        Caption = 'Run export'
        TabOrder = 0
      end
      object BitBtn7: TBitBtn
        Left = 120
        Top = 16
        Width = 121
        Height = 25
        Action = actScheduleTOAExport
        Caption = 'Create task'
        TabOrder = 1
      end
    end
    object tbshScheduler: TTabSheet [5]
      Caption = 'Scheduled tasks'
      ImageIndex = 5
      inline SchedulerFrame: TSchedulerFrm
        Left = 0
        Top = 0
        Width = 786
        Height = 494
        Align = alClient
        TabOrder = 0
        inherited pnlTasksControl: TPanel
          Width = 786
        end
        inherited PageControl2: TPageControl
          Width = 786
          Height = 453
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Top = 199
              Width = 778
            end
            inherited pnlTasks: TPanel
              Width = 778
              Height = 199
              inherited dgTasks: TReDBGrid
                Width = 784
                Height = 124
              end
              inherited Panel1: TPanel
                Top = 165
                Width = 784
              end
            end
            inherited pnlDetails: TPanel
              Top = 206
              Width = 778
              inherited pcTaskDetails: TPageControl
                Width = 778
                inherited tbshParameters: TTabSheet
                  inherited Panel3: TPanel
                    Left = 626
                  end
                  inherited DBMemo1: TDBMemo
                    Width = 626
                  end
                  inherited Panel4: TPanel
                    Width = 776
                  end
                end
              end
            end
          end
        end
        inherited ImageList1: TImageList
          Bitmap = {
            494C010106000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
            0000000000003600000028000000400000003000000001002000000000000030
            0000000000000000000000000000000000003697792100C30B4908938E3A9BFE
            AFE9DA05C199E6D9D0442179BACBB1E8BDF09B10D1B1751DE6B75DCBCB775A67
            86E5B6FEA712F6635A9B32793AAF50F862166CD6376B51028277DDE8F84E9955
            9F679216F3848A7599900F5E0990AF07DFDD1F36CF22E17FDE3FF1E4FB80EA6D
            D928BF7D9571A7145B9D5C3F0BBA8A95EE0F16156C80CF7913185F7CEC575279
            7AD5D62CA3D14431319332FC8C83B01F7D52650829F8A0724B676E2B2C34D77F
            158370FB46BD61D4069FE2499E09E5826ACDC3017AC29168558425031FDE8F69
            266829CF237147198B86FBCDC224759826E63F74CB28FEEA2271358A96DE87F0
            8D4B0F8263858E6CEFE134C5C685C33D7EDD4807114BFE63C21860F6C696D746
            AB35CAA3759F03D786EF67B7A42028C423A67BC0E62D9CC8387F88063B090A46
            F2C4EA4A1797D746ED4F9CF85A90DDCE552F3F36C06D1983B8F9818E21901B44
            9BDEE46B0E2E6C83401CB22E486FE9DEC5B60740445C16063F8836FF995D114F
            6C5C057A1FB3D44637CE0BFBEB649608BBB611FB509716747C73E7FF7A8AC466
            BF5993C01EE73BB0578E309E3AE44AE5D41F3BBFEC8E280FFC51932C45A54E46
            8CD1F627AA90B62721489AD9CBC72AB133B705F7A57B18702AF8AE4E0C219B34
            E6B2CF4A2138476BE29C0CC9F044961A10501BA6E3E8AAC1D6DE9DB251B725B0
            83472550B9054847A69EE54D3C0331FF4A749B397F6381086F8600CDB170854F
            4E8120BC4DB42E4071E901EA6D490671BA7741D37970E2EB973E419597F84B6D
            2C8FAB03562453E746C64C13C8A1A6415986D0F1FAB605BBA3CE45A3433B419C
            C3BD15FBD42DEE79B2112C62FDB7C65E39F65D74E24F40E5583E6EBADE674CEA
            1ECE3954D5F4CFF7C786C8B16E66A762B8929351791D54D56BCF1CFB7BA5C668
            C1277BDBB0524F5BA23F690569883F5BF8DF50C3743866E8723359195022A235
            0BB0CD977C1F759EE364481DADC8462B395CC93F097A407ECE599C428E558F68
            F6D19BD55F5FED5E53DC0ABBEB2688432DF9AA811916C6C6F0E6A7EA38724C19
            AB790543BCF3C52CDB1017AA44EB9DAB6CBE099F0F17AD4EBBB967D6B2289C42
            081FD300AA4557EE65864524BE1F695F82F0364B987F3C9217544704E8993288
            26EFC37039ADEBD71167165D120FDFD93383EB0CBEA6F0155049FD00403C623A
            FCC60F40D37B1BDCFF3995BB91F2F446CF1A424BC78855D98F70C6338485AEC9
            54D28C611EF1614EE14D6F4C3CE2CE3516878683E11ED47DE1AC967A1AAFE4B7
            EFDAB82C8439B92AA966AAFD4EDF9D9BB1B791652F4B3595870B611D24C639AB
            4D13452058BC3A3102E87668988F3EB80A354F08B42B98E0C22D86E8147101B2
            B2801ECD9387AC453C591352829875DF769549A0FEDA1A5EC7F8155EFE35FC37
            171C139FBDFDEB83CA745889D85D248093BB1ED54179BCFBA8813F9BCE89856D
            5E67A8A42BEAF7A29E8E55B8F793D526BDCBF0D497D1FC68CB9428296FE6977F
            4CA57166B67C1574394B6CBCFD9600B5017226332D683CEA06912B8C5CCE9464
            7EA3282961E1632264CCC2E74EF0A7502D95BD4C2D5117D6022F9E2D0631E844
            19E46F5C1F9C400F2553B523E0DBA541A0EF4711253C113173E87806B2DD470E
            5DF3AD2DC7CFAC5E5275CC184A78AA1B7AAF069C88839615C4C182F5D76366B5
            C569E36B905E0153193BCF32A7A10486038825330F417879A45D07EDBA72B6F8
            A49D0F4DCB6385CEB07F19239FD7091EE9DE858E4B29CEF159916AC163C365FB
            9A80A5B26E9761FC72A4F6B007CD140B05085F4AEB046835A591E9AF5FA5227B
            AB4F37F6379111FE8B9E4E252F00036E819200FE540AB9BB3ACC53DEB1EF851E
            C93FD9E67FC7D73FBC7B9A9BA00D90D45D0F5AC2C26B0DD7229A46B0444FE121
            397F55A72B630DE64DB64F6754D0BA9B95D0BA0BF79B0B1AF8EDB5015A3B16F2
            6C557AA1051B2527DB65EF2AD9B029B884D638C240400A6FBAD3B92EDAEA63F6
            5E58A14F866B21557FD0B56D7B120F279257761D94284CD1E445BC19624604DE
            8675028BEA3C67EA5502F985958731C92526AE5E88BDFB145240283D56551250
            0531A33488A075B4E958752E1021DDEE02E1F7F6CC9A184F02D5F114DDF025F8
            FFE830353F9AAD5C7F489CD962C16E67E0B595ACA2884C945358F63232031869
            EC156F6AC92AC3FF39E528CBA2423D6EB6F53CF5090A0A3E1FA9FEE212FD3DBE
            EC9091C2256B77F969237B8973DB0FFF11046197872E69D9E2DB6C9529E2A4E7
            9B2CCF78C928521C72C561EC3E025BD3ED98F3DA7F3E0277B1EA735B39098A9B
            02F8924DF056EBCF9D261F47C9B57E6D163B64F5A1493510986F858FB25000FD
            3306BEE6D516D02F8D057E59A8658644982AD430BFCB5726CEDB540CE22D2C9C
            AFA5037AF4A65086481477AAE9F4AB633F37A9F3592A36DBC9E6F9927D208A1F
            1FCB80B433ACFF3B60376875E9CF312396187AF56B574D1F283AECA7CD142E07
            E956553651F555575A6AB488CA6B9155BD11C3A0CB18D59BE688D93E1763829F
            A80408505A80CDECFD00BE0EF9E23CD183CB9034C559118B0E3B00D0BCDB56D4
            4407F7ED3B6F862E604136FF24C8B658F02CC2638D522DC28F2352D088E436C9
            83D565EFD251B345B4B36AC40D5981C3286833A0EF8F8947F5B433DC1E29B090
            B0C380E180A127B3169065BF0B90733F9FC1A00CF1E6846EFF9F02D5B98A413D
            A394C1C171FA11D5ABACA3022E824B7C2A41A1C1870C28431E8640241A52D998
            7AFB872A6A2F9269A43856675F0EEF9DFF06CF87A7D1698507DA6EEC066C588C
            88948F371390DC7520BE1E14E8B4272AFBC6236FD961ECDE69328BF69E36EDC6
            185880D2AE858F2942E326806556F184CDE044140B13011F21AFBF491688DFAE
            BC77410F6390220B7310B5DA120764883D62BBD991AF6D7FB66E645ECC8188CE
            A9CE11E99D793D3B04B634343BD702DC54E24A92BD15A1910CCFE48CBB3D9849
            9638F42074FCCA50ED230D181D8C3FD1119DA617218FA88410436DB49BF37050
            866E320D9641DFF06F6358699990C023AAD7BB2359859BF517226F85E40ECC2D
            9378920C1F8905D87B8C1E3EC84B351FA20F1BD68D3E90212F21C5232E0CB779
            1FD42B4A53D82F38ED043E895057CF648200F7F86B68FF9ABB128858380C3A8D
            2CA71FDA5FE497F4481CCDFB9BF03304C561D489EA3B96DFC435987F7791BADE
            503CA8E7F55679D184972F757B11EF0974511AA33FF95991339BA502AC3310DA
            C751D5C46C127599BD59AF7F5E06CE2CDF496EB9024DE7374AE9E32E0F2980D1
            58018C8756ED8404D63F9BEE8E471B0DA894E6F74401C50CA9B25ECEE9E78436
            6FDD857CB896B1D2F21917AF49F0FB4B51DAF3304CE9F71351B3FACF681F23E4
            7A97A9BB6419EE9C971423DC540063CC87D159E00F88DE3F462723724641B288
            2C3933609946A2C77D7E0CBD61A5CCC816B7EB0AD29AA1281451B1F6562D6528
            CAE9F12D51BB16CB10EC4F71837011005755A00451E1F95CEC239D54ACB45710
            46E4D366CCDA05A3EDDC5B0F2E7DBB140CD7EE67D0B9EA468C2EFB76BB3A97F1
            6997A9E0A28939002ADAF10E72C77DE97FF7FE6C6240C1969BAE8BADEB90E518
            9E93854D46A2A4A71C4645DDE61D4E071D5FB231D9FE62FCBE467ED043F2A177
            1632CB7C9D4C304434FD0160E86B3D7D88454F76070912E38F8962F4C9DE0A08
            259135418AA6BD7B92F617214050EA558A368714B3D2915294D0AAD69BD20FCD
            CF7E084B50E25FA5F8FC5A9B62758D9133A30D2FB9D1852F76CBEB15BFF6ED8F
            E43711E52011A14344AD6CEADE69F9216D92073D4CA2F73F718D49B553F10356
            CC50896804544EC9063EC2F037632159B9FFB7558F2AC1B9077ECCAC3561987F
            D5E8B41D40E5DD6F48D68FF0ECB989F7DD6B78F55DD8A53CE4A72B9B92896A6B
            68B2A3563F895624DA4D12231B9020BB6D702B31B21580375CEB72ACE5F088A9
            DEA0B5D387001911E850F4099354CDF070A63D0459F3945B75AC7E7CA3E9CBE7
            0537A1DA6C73873416E718E70B69752AC2157FC8889B1075469527331187ED9D
            3E2284D507C388658D231608388601DE4C2758F51B5B6E91DEC20B84F632900E
            FB01B981DF0A72D5E0EF3AA85B43D6EEEF61AFE31B1825B8AAA4877BD17DBC9C
            FBBCCEA6548D443CE09BD654305E14AEAE94CAA0B112AA34C85E42A68657F9A0
            403D29503C8E58D7A6265548ED4DDC91D406124466A5AF16ED98FC9856D09C0F
            A77486B24502F788BE8C8AC9B2DF6800BBF5CFCFFBBBA536D6115EE37A0C210B
            FE72A5E342565AABB892B1DE05911D5F0B3B36550E93E5C1B2365E978B70CAF5
            DED25036A1DF8FDEE7AF7051242955C29517E2B0E12AA7E24145F41835569B95
            A5061B5CC0C247DE4F6187AFC8CDE7E0AD30F6C5382044A63A7B9084ADB95E42
            AC68934585E3E9C04D8D21155A6B0A41EBD3998F2321182D134B1F3E923A2F7D
            99850A37B1F18F4E98B2461DEBF429EB720290507D2BFDB2E26381C2554FCEE8
            883E041C647091EF94631FE8FC0671934A4DF294BD4746ED8AE68795D2846E61
            95EE7D46B4D0B997C40F70B51DDE9EDF4D3E2293E4DA28450F0BF79B5F9468ED
            37A7C5D11FD0DDEF6AD92D098CBF2B11A860325F21EFF9B307C87B0B5A0BDA69
            17221C291BA86BE9AFA7A3BD1E6773F69291D5E9B09D9C7782C7151841E7E30E
            E140D976009B1942C9D20CC940166A1431D60EC96004C2016E78609A38A205EC
            2ACF054EF9A297C2A20F667592F3FD6E1DFCC5CD33DD1D1E9599733694014560
            0E9B4573DA28577392E5FFD02223F15BA6F98ACD0857AC8F41ACAC964F3DB7CE
            33A58D372C3A4A2605923DDE623E5ADFA37A82063980A8DE782BC81C5B5444E4
            743E10930ED32724C04B7308575C1DF25C10AC2FDFCED79F74E8E3E6244122F8
            3FCD3700508B9F325FB988E03843BE2AB0A2380B067C7B406DA62B3E4A6FD066
            F5299F4CFAB50367B4593EA1061A87FE331DE2A8D6118F1BB8BB72D41CE240E6
            DE14A5B006AE5E24B6EB6D2F5DE689923B43B03C794DB38D05A87FD2077A9F4A
            6AB9950391ADBC5E55A17363B1AD9C9718105C55153A477324F36EFA3AF38A27
            AF2AF9E7C7795195173D615A3A395D41A77788C9CFBE5785FACCFFA52F13B3AE
            5CCB9FE1F589A6F2AE6B41C5C93FB12FE204C203FD2774B50BC2192799208B6C
            2AD76AD235003D6A2FEB7F5856EF5641BFF05F3C878C1FFD87C53C9E5AF1CBF9
            EBFEC145FAE565A948EA37CC7C160D55394AA1F19777646E6FF1667406B58013
            4E71E70CD71C7DABE19FBC5D30C406652601B78A0BBCF8ABEFBAD90432347804
            E531070AA1DE2928539419D216B47FFC270592E47F81915F5B4EADD974C73BD2
            E5B68C1BE2DA5FAC24CBEED926FBC7B57CC2FC78AC6E0BCDA47EAD2DB0DF3799
            7397D373A4E3172A2AF35049B931552B7C9FDFC16EAF0DC094E2389DC051C40A
            E3C41325807A29E7C0B444C88CD61327FD3D660DB0AEA6D0419B17491F034913
            F051AD6E163A29513FD5A6F9575275A83FC87D33CDB70B0B03581FEEEC093CA6
            E6C432940BE8D82FF4A989DF2050A620B3DE9FA822329C15769FE0A2B657DA7E
            DB6341B14E6943E3C602C19E0D957356B322EC3B3F75BC12388DB0CD73B7262F
            259D3145C9CDB610E195DE1D09B5162C9B7787024BAE64A2819AFB75E7552F4F
            99E252E56D9D731C575BC2384D84ACF1A40573907BAD6D61052ABCF6E5773D29
            49A8EF44744BD0A7A6911785B33E02024A4DF86943F394B3B32D1E984F1F27E7
            2573528C11E66EA627CD04FF67881763BD9FA274F2FA347D86860A1887623F00
            E249C3A0BCA94C22A86674B7FE8C01C10D9E4D23C7B660EC3C6952DA74C72E83
            1915D72DE444DF34262EC755FE2611FB9FD7014CE51DD5F88CA64C4EAC5B5D9F
            A53B1C852AF9814B71A3C7605EA35FDFC5072E82F8D147CEF9869AFEEB5A06A9
            C79A136698C52C1EE80BFB4A4DB5EBE30E8F924A83D689862DDA54B1AB9CBE9A
            B7DDD187467B2FDEE58C9F0FB03CBEC01FDD845D2FB0DDB699494DE0F1089D0D
            8D2CAB67C13659A9B99901BE449F193F651FC898B13DB722697A4D624AEA3AD3
            8F6B5100A016D59E98507473BC8F76071CD9EE6A8E0C9B142FBBC990BAD92F51
            262C0FFBFC9B601DF8746F4CBEFA93EF98E7D43ED69FCDC4DF2BE698D97372F2
            CC3527AE8C2D07E69B9E2E8550051209C31AB3FC1068DA6B2403D60B511F5962
            989DE24F97282A12631D0A851960357DF6F7A954653B347E58342B5EBE7C04B7
            6A510B71D7285352062C6989EEF0412113F61D3646F7130DADB41A0EBCACB368
            FD45E9757CE3EE65F1767E15B4E203490B706458570BFBA8AA3142A8ECC03287
            36B63D209D36925925070B3CE775742C758AA44E8025DC20D0538970E8B14737
            C2D9DB810D3DA9CD48F1269891CF451F0FBBFDAE1FEC54972185F6F59598A3D6
            EBDFC398E98D779808C10A37E7F76045D0A1C7DF96B058A5866AAE4D7A73A2D2
            ADFE9D633180F83700CF043B2AF11DDD8CC29D38ADB80328613C19EC4AA0BCFE
            7F27EBC2CFA6340D21A0609A5F0E6A73750D4958ED4AD1EDB4324EA68770B59E
            89E217F29ED3034B363DFE51EC296D973600958B59A2856102A7319F83C3811F
            EA80B87DE3D86BC9888B2651146432090CD8ABFC0138E728B98F971B1AD4E26A
            9293C3CC91BB0B601191E44AFB2F8606293EEC5483C41F729D706E9AAE15CBB1
            5DD612F491153EF84E32600DFA7CE6673C7BE51EDC75B51C1B329DFBD368456C
            49E64D716FBD27D95282108BC20D889E98DD1CE47E452EF5EC5227D6915AC03C
            7F69947D65117704131D6A388AD3D00798F0B03E40CA31F155A1A6D99ACC7646
            AF63E409ACCC4EF34828018762DF1BDA21A9372493C94B379D2B936EA9861CCB
            A61C49936087F88651AD9E21C276B5E54724EFE349239DA6E1DE737856D15D4F
            14CB7852D807BBD41AB0791FC8CBE0E9CC2A72701ACA9436097AD115C817D1A7
            0FF05D045499F2C4FC95BC214A5F44EEC2AD742A233D66C4BCFDCECEDD84E625
            059DF2C5242B8BFD4EA6E6DFAD97C514EF79B788117C70F00EA826F8F9DB1784
            C12DB3EB26E26DA6BC334851B4E25223BFB19E08E88C018903DBFDFE02ED8EA0
            CB5BC453D440C26589B821F8ACE5E2DFD46C8D361237987E7BDA6D899A68BEE2
            85A9812FA20401CECCF9F1B086C9D9D9083133884F3E0D32F542967E570E6991
            7E739224A68EE7FD32BE2A498EE5BEB28EEE3AB8C70143EBED5E7D70760397CF
            8341026794E0599E2572C8FCCA46E7F8CEF2C144082B05DFF2EEA54339223CC3
            363A33ECD2C9E545A59F46E88AD60A33114ECD66522BA832DBF98A3900C24EF6
            0F2109309775F05B39098559260236D58A035C5A4DF8248A6F828E2D16643DA6
            35FE9FBA961364AFDEC73B91EB9FF7A1CB53EA71A778A7AE8238562DCC80BCD1
            76E47DFD30C9730CA0744089DBA4DB31CF912990514231C44EB2D23BAC0CA01B
            EC50D79C4854845E325038E302FB4E094BF557DB4396373CDFA9FD9847F2CD11
            85E68B1D262A19AB19D24D5DC9B0B2913AF35D9F3B57BB18694971827041C99A
            DDFCC83E60B755634DEAF5A9CD46DEA978F2D15256AECFFED1771532B78D773E
            483FF540F9A6317BD9D0F9D86AB40DCFEDD19E6A592CF7F990B3CAA73D1B232E
            89D8DF8A1F30B5F7F05D17FF51C0E42A6DB3486F3BBFD2B7AC442EB6B9D4C30E
            0D60B0DE1CBC5044823732AF58D1009920789A003397A03E7F2B827BF0532758
            C46CCBB049DF3E07A9DD4F9818147B0432DA133B3BD5C4E35AB4B9A53F64A000
            3BC63F6596BCE5A269AD8CBAB7D2EA692935E187700A7A9CCC57B9768D9DE627
            FF8589D123984347A0F8280D36772ABC5E598CA2E8DAA248C1D3072FAD2BCB5F
            D60D86AA0F992BF371AF99E9E85F0DBE52A4A2DF0246E341496B4FB8069C0D94
            E58F0395519DF8CA5024CA113CBAF754AAF573827D5047709B28EA3B6F323D90
            D272E3E62F2735D9E0175286375AA112F3F06189753B645626F47AA755E59066
            AFC4D3ABDABFAFD0F0A2179F9CC3C38BA9AF7F30F77DA3B5492FF7B74BD12BFC
            157A25C9DF8DC19996D9CE97BD635E8F0C3CDD5EB7256DC6F1804B27EFC759CB
            C98743C0A72380FE4DA024FD4BAFABA9BE478194F3E37F6721734CF39F99AD9B
            BEAF4630E69D1D2E060CE017F4401B29A457CE69FEEBA40733177FD1E79B96F8
            EE10E73481BD896AAE813E2F9670B2CC8C690214161C470E9FDDF24DB2BFC87F
            374C26CA77B5989D4C46E8770D832C7BAEFE9C4CAC2A6D274283CBF3E3A7E41F
            D983EB87263728583E628E047BEB37746E169FA0208413C3DB93DC0E154194E1
            0D030F6921F0C4AB7ECA16F80C4DDADCAC48F64081DAFBC7328C85FD7086CBE0
            69F8D10747E2C789A6440588B67FF6F15BE7C825BBF56EAD84FC5A88B1898798
            C4AFC54877C9D264E5AE6753D3AFA1DE357FB25AC6681BFBDF55A9A85EBA9177
            88A31860B3DF7BB7FEF930C4FEBDB44DB48D0F37BB6FA5C29D85B248EC314C6B
            2D9871A4CA1658A0155C5B1246FD30A21FBC5F1EB6E8D7BAF33C6B3B99826C73
            F87EE99DBC951721ACCB6F2A3F04F54B07934528F2408CA548482CAB1466329D
            9F57198BA46A75D9A6AE10C6798D9ABD7AA5A3B9EDBDCFED06AD7C4E3CBCA264
            8D0CAB132E512FF5431ED7C1C8DD9CDD24BEF0B8E5CB75A4D0B2B47F912C8E9E
            873ED73A8238514A233917778D59E2BF630D65B01C7815D6C028D2CF7AA6DA46
            BCBE1D0621C3A026D349734349FEE10498A989D00B470FAA67A5557BE160BA6F
            4C60BBB7013AEEAE23AF03FB8C64BDDDFB03DF7986E9C7FBB1474464FAE09659
            A662733AD418456FAC00741FC6FD13D4DD3C62691F00A4E0B1CC2D3AE53E064A
            53CE87FB08E892C8333E379D5CC85EF30F2D811075C5003396B73DC55F4B058E
            9F40A71FD1EC8FEB80104788796A682D9C30817B2E982171CDC19DB71D0FEE0F
            5FEC3D4E79440B99D48A507A0FB2CA0B6ED13CDE5C3AFAF89886A29653F58368
            A1C340D0EA3C65E79ECC03F1B8F346BCB6772482AE834C3184E298526FAE06D5
            128CF321373B55237F18FB651092CF40B56A908A2BC9A37C62FA27549D97C752
            A3CF3F856D9391466B7A3F408FE43BB5FF817E6E06C70E011AD445C6107E92D9
            AE930F43A84811670410E81CD437D973586F7D4B2C331C7C27FA6356FC525780
            C86B4B447D3E63E3D7BAA78CA0B1AD93B126CAED5B1C2913648C53CF35AE2184
            4B752B474F9AE606E2C8BC69C0A1306658082CC987A94D43520ADB7DD9413231
            1F4065BC8AB9E344B3DD4AD753823ABA314761BC3E3D42F21FB7822819639FB9
            E2E9B6968BF23AC6992FFD0ABEDB716C37D0BE04F41AAC1BF137AB6DDB412F21
            E41F1DBAD75C315B102E44923EFDCCE1329867F4F5F35DC6B0CB0DB19DE1C0B4
            05B6418FD9F844A1F8D0316F1A2625618E341375A823029C8F4F97C2570A1E62
            50A05BAE71CDDDC3DF3269AD849E450B88597D94F2B351ADE957B8299ECEBB37
            095655D54FDEC7768C7926B10D53CE8327D269CF94C113FA7D2671856F22DAB2
            CBE68634DBE4438B8561590219D503D3C33D33B9D64A004963C3FF9ACE7F5A59
            3A78D9F74ABBAF13DD1CC1BDE9495CF9EA1498BE31900DE7493DED19C1395E6E
            0B3441F9F88CABB2D746268C45C71B955ABA305A11B432E6B1292C9F7A98120B
            E1D84947749C11A0838BF9A0FB03417BCF131A6AF69A65404635BDFE7F834C06
            5EF01933A2455C2CC4989AA808947287708B6F865805F9150815A8B5B6553BCB
            058ADA561DA5208079505412E95380B4A9B51AF3FE3863877989F733CD99456D
            ECCDE116D770C92D290E003267495E4C5CC587657EA50BF0964DAFF29893A768
            13946216BFBCD6101D489BC805C97FAF2E9217313C10D1B5E81F04E9F723DE5E
            46B0D7936DA869965951147BBBB1AB5217E9E4A4066BE23B025F23ADB48BC9A8
            FD318E94CA3586A6DA3025DA2897E7B7D8BB61D24768A556E22EAF36A9494B38
            C4DD404265122461651291EE9568F4CE1CC553AC387115CF05FDA1E7B775D8AC
            A4D28F69B49933856263D1E66E83E46CBF410A52A9D927572DE76249AB85294A
            6905E68DD31C1FE8B2F5831E97823CEDB5DE5D1C0D08F4BE968394333F365816
            EA37714AADCF4EE5B9D61A4A486CE4F81DC6E77C7F38BB558559A99FB0C2E8FD
            3AED57B41F3CCBE967454D479CCFA13FBDED62902BF9D6C673308DF56CED8962
            187CD58C03A5916F61892D46C9FC4326C446561F834455F8E71316EEFF463E6E
            DA51D7F553DFF06DB42260F585FE08615FB2963A903E2784E0F2891651988D94
            75374B44B2E9C9C3B78AB1EC2DCD98E88807D0FC272150C4A954380E7C549227
            9D9541BF0C4AD972102EA1100E8B2A4A82D108818FCAC76CD3A8450D292C4B91
            4FF5C31D7A00695E54B9FE9AF3F64DEE0A19CC703421332DA533E1993C82C530
            ED29183EBB49AFCC5494CDEE4915EF08C2323FEC35C2FF6E81EE4BB0469785E8
            7C56070440CF638F109CFAD64FAE5AC6AC9172E726A506CBB6BC39D19D56A074
            B9145749AC3045EE557F5A4916C5DA53401BAA58589D52B632F3908859F715F4
            DEA3F94F68EC3D43E55999E8EE75B50122E208EE899255D096EAB968853D2FAE
            8853DFE6BA8E2F69391C525D597A5A0C3F758EB23B0F3341C25E51919E710676
            540CA3B8FDF9E84B9F6820C5DEE5E08BA82248D69DFCF8273653E870A66DF6DA
            1B1B6DD32BC18F53E0166E605A20553D6773DB9F52B31D3D78ECC2FC544FA046
            30BD0193ED8C2DC736E784A9860B7BD598E3D3C1A27BB153A0ED4EC9B3F99181
            97031576225A85C74C772FC4AF494C6BC0CDFE91273D3D968530AFB58A044CCD
            667CD13FC7EDA736E53098A1F188D14BFAF941B387F9534D02674C32DCAF236B
            E79C9D8486D52568F143689916AF06DCAD81B0D070E83CC74C4528E5862AD42F
            77716F2526EFDC1FBFD6E426654BA95BA3D53F7804D3390A80123EFCF4C8A7A2
            D1FB63E86E01676D5F01E2F79A282FA75D38FAA72FD3CB521FF72A78DCA5C970
            CE246EF9535515E4BCB6A3216A6F210968E035F9090C71BA65C9A92EC99AECCF
            655C1734D45935E331D3BB7E4CFBCAD95C92F02459C1902A09A29CDD4E869F5C
            47C6F9B0BB4D6C66E3F07291D7A8D813BD4B4D9D49E81CC2567F42861354825A
            CCA74796DD252A0BCB7366D6F7793860B2ED2D1412C42AEA2143028E3001CE9D
            47EA10D1CA3E7A80702B57A76CC6005229043B43E46F5B6C22EC748037D36969
            E322ADF28B9C5A71E9E7CF955C7179CF55378892B6319AD7654CB404DAFF847F
            2A73A2D6BAFDECA3EA9CD4080662AA28A9F0E823C328ED2EF81EF80F5D60AA78
            1D5FE636C2CBB5F400C8EBD13296007EA14A2A0DD7FA83B30194427DED75EC6C
            80B58B5B554E03BBAA14DCE9B3FAFD3FAE7EA9CBCDA2D15F6B6872790C918209
            3DB7796BC1D0A70C5F8C5B4E36B23FFD29F426A26347E4E00F09C25FDC4573A6
            B99297ED6D7E72BD8CDDE6726929C6AD09B1E567598141B7C7CCE8573A368FDA
            680FF16A69F5D1695423D890BA8476273AB839AE1143F7354BBA9E31094E9F72
            77EB2B4693C8C59742BF119FA8E80F2A29089804AB5BBF40A70676C8BD1F8C48
            C3FE4E3785252B780A2085191BB3E13D3B18B50A2DF2C8A604C610BDDD913F56
            2709F7316D94FEAEA49477F69E076BF7C70871D361767D717C1B6A06D851FC3F
            E749B6C51AE785A463502959833E4EE7DC40578955859090BDF68247498F6C84
            ACF6401B34F94790E98136CD5D8FB2605262C0FFA76E28F0AA1F8D8C6797FFC9
            1AC808F3FF0CA32E68D15B0FCA492A032D0151D87EC8224793EF32752FF5AC5F
            1726CBFD5B8D5A7785CF2F6E32358472DC0F546CBB0EF84D976B6B43DFD352ED
            53D0A3850A4EB612DBAF496B79D3E1D9B5553B6D867C5FD67EB9BD66C00182F6
            C53BDFA40CBC7305354B0443940F10263A11DC74D2F6510EE8453760887B5D13
            ED17B02278D54B46408536F801915E2D60B96B4927385115BEF1AE40383D3734
            B6EF07AEC9762070387C75270910B18A071357E49C7C05EC0A4E55262E5D03D2
            4C4FD376E2EC26209FFA129B501B14E510029E949CF954BA68D5091262DB6A26
            F70879759F5CB3F79BCE970F2BA6BF4036EEEC3ABCB30BC20DAEBFE644FE6F19
            3C3AB67D5DBF0D60C77F53287A86EBEF5E250EB725CA6F8DE79C72549AD232A5
            79F1594567772B02089DD8685BE3883F09936515B3D0A0E67FD73C07ABD50456
            10ACF537D5C291C8C9979A486C649D711902F1712F7936B3D11735295E679EE9
            64DD5DEA155180DC942350B608D38C362A891111F258D004BA5BF6034EDEE4BF
            17A678C67B03455E9261918208C37ECE790C7849935DD4B823969CDAF2380E8E
            4D330386C0A5E2C5417A6E377C0F75AFA29A3E0FCD7CFF2C050F6A73774CAC1C
            F38ABE5CC2052E2DDA1B4AC1CA9D466B7E3E7F43AFED52A2EF48C2A8D50D070D
            4A368146247CDFD14B72C1BD294563771D2306B184430787EB9ABFF4B131513B
            F3997753D629C8BBF604F9A715BAC3F22A8B41E426BA43E86209BB9E07FA0FC1
            E61C993E72E7C33B6371F2A908B653F797C9FA871DA29D17A3EBD6E121A7FFB8
            34B7883CFAC4BB65C1C7F6B2CA3ED0A1AA95E287AF7B006D990DFA4AFCC057D3
            0515C263BA9BC469E0A654D84BA6C6B54AA7AA28C62E9FAEC6AE401F99496526
            9E75698D262FC755E05AFB0FF10971C02F2478B9567E43CFFC323778FC9FE240
            79B7FDA3220E96D1DE9296DEE9E14058AC47B0B20EC441EF2551088CB84852BE
            8612FD00651B70CF128D0716A05939DF88F645216DEF0BA2EA5097319EAFF802
            5426F3F5447933E39DD8A57EECF30C8EB57079646EBD68BF81FF5A498281D21E
            1FD431119092CBC55B2A7ED8117184F7292F0F0B287B67E4EEBCF817DE67CC16
            0F4772F440137F2354B3C5881811668D2DD0E067395953CF29E6C37E6ED4A518
            2DF39A9CB0942AC3F079936D822604AF56C296ED1373B2401B7D1C4D14B4A9DB
            47BADC911E790DAB85ABE521B5D87CDE8FCF7E7FB730B0EEF38747237300FD98
            ABB22C3DAFFB6F856E504B48CB06F3BAEC2F32F095FF07703B8F68AD9DAE468C
            6F905ACAEE11A0008C50B4A99943A7FFB924F1F9F63A0FF15D8331B8A31BEAFB
            BB721E6B507AB950E4E70596C63E96BD99925D11217B5FEC2B74026A16749D15
            C9C4AC1096E178410FC018923CE3351321750DFBEF26DB26BFEFFA9DA084BFC9
            D76EAF3CFE0037B714FF46A7EEDB4A3F9DCD607E1F3810C38085BC4C72631BFD
            C283C82D2099693A26FED5B7C80D7576090A3A63305528FD57F598C4BFC2E964
            C270A6A599BC8561514F3261B7C38901E175E162134818D43FFBA33EF53114E2
            9ADF2722DB68B60C1DCFC4E8C6B38928A99F5DFA9ACD32878A2B5BE132F24DE4
            1E85B715CF66F8CEE8CAED08B9D0436885CB4CF64ABC77F97FECA240D870AAC2
            51CD4573B679957E254F1A5E9233ABC8D0D1561230349D8609E2F09695622A9E
            7ED3D385B221CFEAB1D6BFE98700D559CAC1327685517A1E110EBAE8C2AFC135
            21441BF52855B7CA593BC49B3BBA4480D9BD4C3F891CE9371473B3FAD918F362
            9E652075763A0BA40F18F1FCA507EFD472B50AB6117B3B367E5651022CE95580
            938D6785B9224A1CA2EC2F4F222D610667C330C6BE039F472CF9EB564BE66D8D
            D621DB4E2F11377C279BFF2D6FC80C5DDE9EF53899C8569571EAAE8D15EE289E
            A36C354D413079B2215A1973905B3B0F1DD4B7DAEFA8FF2A7976073EBE8809D4
            BDA73BCDF1F669D0A1DE5D7881DD0FA72EB03178347B6FFF3999BCEFD05F932C
            5EC1B9286E14EF4CB999BE39E68A3A2A9BD564C0F1DA642205F3A54F3B5FC6CB
            9FF1B7A2ADECE4C7B0BC09B3B24C0E60AB6DBD9611B106799980CE53AC58782C
            B9A057BEE29F86145F97959BBD4D0345198C74466315C4120030D6B30D8905A9
            2E485513D0557E6CB5BEF18FA97EC327011394972B52E67C60C0C8A58A0FA62F
            DC96F18B960A63A2967A436EFF56A5E9108B4C05D194ABD470199145EF8DEAAB
            717A70BBF4C815AA40E3F4852C59AC95877BA9E4A40D4643C206F88CBC15C56A
            68961E010FC61F4B18E9AABD663E2AD948A9D1EB765B58403D90346E115D7F1F
            1D7FB2BDD2D8CA4701C8E8B67970C4CF4F444EF40F116B4DA27C3808998EA76E
            0F9ED3D5466A19ADA078F1AE001D07566A545C991B11073281380436B7854839
            C420A77750CB1E5112D4BE0E48EB1F5F692F38A5AF2EF9F4BF11314E10C4C25D
            93E1D910446ACBECBC29799A6432001C8FC51204B147010C9BEDEFE453A644AB
            C79D1F6D989D60CCD7E063C7EB2E48AA43F80ACD70E2F1B76F09BB761E803695
            21F0BC786F0959419F1D2DC89377FA407A7F9C173F1A6F393A73F86AC93E82B6
            61D47E9618A473DAD21D326E76D361DDB68FD4B2D3E89138A9B6A8C742462B07
            543EA5CDF74DD860B020B4E20E3B66F5284ABC156F06B160D4FF8E7032D92084
            5BBA9DD433460DC35AA93F9D57813FF71E90633D5BAA9127F7583E40C88890DC
            FB2F470532EE8D9BD9EE4CECCBE8B7B2948F88B858B882C7315CACDA37E81665
            7B1F418E13C557CEF3A6DE05A847CE63088AA65E499994104C0F785BB8B8114F
            805B67D8FA7B10F98539958639A99252C34AD4B4D202C18D7A6CCFC3DAF40AD9
            913B9C8782A7013EC04B9572C5CDBCB283339873CE63BF1EBFB90B8C60E20A40
            57A612FDEB4CB6620462649F5DED0E35FE7BE97B46C26D2CC257EAC1DC2F1AB2
            96C53F463F8F3B13A69F86EC627D74585647635438558A826655A6E6642364C3
            E438F5B456855B23FE5A5BDDD16D003FB0495D0230D6CA94E7AFD5B7731D9FC6
            E74BAFD06C9AD3F9DD8C9794F3F92512F550812D420AFC5E24C74935DFA55629
            4EB4F12589B651038E6CBEAD4BDB109AF848D5BFE02FCBF71F5E770409AE1DEA
            685E95455B008B4A3F2DA4873D58F6CD94E0A2EF0870D70544ABC95251417869
            8D8FC0FF3566EC82E9F692BECC7BC04BB11C0A2225F7C3FE4E40C1D74D4423E2
            FD919F17CA0B5BA9B144F415A38C3B9780C97E60DE1CE38C5530DA76904B5D7C
            1F978527EF0147EB76384F3E5B52719DF23543ADE5DA20A0C7236358B3B228BF
            F9A6E0419890C03674545F04A658FD51633B3320DC08CCAEEF469BA20CA4482E
            EC294B472CA0A583C61701B56CFB7946BB66605D71D9A0931D5954195AAC6ABE
            A568A64FA3FFB0CD557265082BD67F85BADA3D79670BD4522FA5F23C06623C20
            CA78596F8AF6217506021C88B0BE4A4F0DA63F33D72ADB14909F8B67468E9B1B
            7C1028FF9F6E6B76D05CACF589FFC55E0935F134E3704804B93BAFDAD2429E8F
            2A31E5A021E9841198444427B7BED4DC28EEC5173F44C9251F58519660E03710
            D9BA7B8607CE032C914F0A465E4407A479F235D4C33D20A4F371A975758C2FCC
            CF5D4FF34902D50AE3CD1F655B3408B4C194AE693E71FE4315D56CE3A9A03D8C
            8C66FE5447C1EA26490E6A13952022D14AD4B0767202B001591D241F4782493D
            388377610A43339273A09698DE43DD9A953ED54694EA036596DA07A17DACFB94
            F3045DF3F010528DAF5F378F2E5EDEE8BAE68982AB2BA2F6C48C527D3338B543
            671BFEA118091A05D14F6E9D38AFB9C08ECD150D3EBA3BB7D612F8B17CD5DD0D
            031EA78F6060EB40D6FA56AC5BD73A712937E4134CDF1DFFA26EE365D130193C
            E3F6C356376F374D723A705EE619B1B44753CA630E218E00F94E6FA99824BDF2
            8C9B9801278DF92A38D2A172483516A986469AB5352B8AD71BDFB5704FB55490
            666A0EF7D89A5B2D2CC9CA815272398F9DD5F41CC919F34D51289E435E842E5E
            9E567907927A87D7C00CE1917BB58CA3082652B0C783F688159842EA74907C1F
            F01043C1A390D77CD4915CB58A2C1AC4111333713019213EF99970D4DE941E2A
            209272F0E00F918E3A7E632C46015E6D1F67C31436F0938E99D996C5A760F743
            EB8057431E749792CBC2A4DF6BEE751A290FCF105621D77595FDE21D583FB570
            BEE76DF6BE54401422D4203BAAABBEF1615503BF50F22EC6F17E88282F940BAA
            1B35A5F544A150E998379BE1DF1D31652283445F74D47517AFF473DE858F132E
            3FA32194F4E09BA7E5395AE01FB193A79AEFA0939C5B7BBC15A5514E9B841A1B
            AF871137DD2563167E5996508BEF80694477CFEA7B11CAF6E086ABC4D5753330
            A85D9F3491A8C32A5AD76BDEF773F51042D9476D72796F43364C3CAC4B58E618
            FC0DB98C1FC3C5037AE7A62A2E28610AD8584987BAD2A639807D2634CE17E316
            8C66A615DA8D7562EB0C8E0E2B70B7BCDBE7BD06A955D1C8A9B482357CED4D64
            DB3B5904808B31604F7A4DFD523EC7BDD7F394E1DEC0A327DFCDE4DFFCB2A212
            56C51FC11B17DBA65E11B7ED1FE8A6FE3726CE44AE3A4322AB02925A65813997
            E2693DE86AD5B6E2B7EED5DB5213EDA712263F2284FF1B9D1CD025E66AF5068E
            24748330240FEF466E84CBFB910D2F39994D950F7806EF0E6E1602A7D9DF1147
            8F0E2B894AF76F59D60B18A782410C6488B949F98DF841F7DE6D3ED78C9A85D3
            AFF9E35FDC4019E1467B2683172FE81C3698B2B8440926A3A57F66C8D22BFE6C
            2D77CBFEBF25296867201834DEF1A46906A6FCB00B4567795C21EED9471826F6
            AC10FF92E7E5D62C2CD6CD27F68CA3C61E18A113C17000A68BE71DDC5036DFDC
            14570EC5212899B6C1070A9D14FC0CBAEE98AFE549D9497BCEBA64F8D6B48B7B
            EE872246D7C1CBD9727905230C7ECC6C8FD993D2C57DA44A215FC85229918501
            B48CF828E50CCDED4E6318D55C020BA123A860A5BF4A172E0F9083D893688E67
            1AAFE7EDB19046C1770E620F80F41154A2C13598FB5BFA40C7A9066CF26000AC
            3E1D5F1097D5DB0AB263D4CEC5FE6EEF75F05EC83F9DA124A42E50CA2D9001E0
            0E32F148B2FA4EEBC2CE89F9E38D44192BB1A7222C6B371E949B5BDCF98D4D00
            7D29262878C9F37E18A59F2A1CD15CFD1B5AAAB91F4256761D1935EB1AC755A9
            1ECFBB1F742207757351DB0C9357E4B2367EFC6618F2576B29426B56C6D5D071
            F9492144CE15691930A294CB643B874966A29F9351C35F4B8FC324DB39679515
            01A71C41CCCCDEE7879ACB6220C132228E873E75E5E6F4FF58EB8BB89EACE614
            B8FBE54D2A1679FD5C3A65285A64B28A01C377AB016860B9344719D885A1E0B1
            D0BB13A33E328BB07CFCA80F51E7004639C0CA80C0FA13702F4F8184A191E541
            B5D05FAEE439BA2EC320A4DC6081106BFC57424D3E000000000000003E000000
            2800000040000000300000000100010000000000800100000000000000000000
            000000000000000000000000FFFFFF0000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000FFFFFFFF00000000FFF9FFFF00000000
            EFF10000000000008FE100000000000087C3000000000000C187000000000000
            C10F000000000000F01F000000000000F83F000000000000F81F000000000000
            F00F000000000000C187000000000000C3E300010000000087F300FF00000000
            0FFF81FF000000001FFFFFFF00000000FFFFFFFFFFFFFFFFE03FE03FFFFFFFF9
            C01FD01FFFFFEFF1800FB80FFFFF8FE1800FB80FF80087C380FFBFFFF000C187
            80BFBEBFE000C10F801FBF1FC000F01F800FBE0F8001F83F8007BC070003F81F
            8003B8030003F00F8001B001FFFFC1878007BC0FFFFFC3E3C00FD80FFFFF87F3
            E01FE01FFFFF0FFFFFFFFFFFFFFF1FFF00000000000000000000000000000000
            000000000000}
        end
      end
    end
    object tbshSchedulerSettings: TTabSheet [6]
      Caption = 'Scheduler settings'
      ImageIndex = 6
      inline SmtpConfigFrame: TSmtpConfigFrm
        Left = 0
        Top = 0
        Width = 784
        Height = 180
        Align = alTop
        TabOrder = 0
        inherited GroupBox1: TGroupBox
          Width = 784
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 563
    Width = 794
  end
  inherited pnlCompany: TPanel
    Width = 794
    inherited Panel1: TPanel
      Width = 185
      inherited BitBtn4: TBitBtn
        Width = 169
        Caption = 'Get Evolution and ADI data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 185
      Width = 609
      inherited Label2: TLabel
        Left = 316
      end
      inherited dblcCL: TwwDBLookupCombo
        Width = 217
      end
      inherited dblcCO: TwwDBLookupCombo
        Left = 368
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 372
    Top = 200
    inherited RunAction: TAction
      Caption = 'Run import'
      OnExecute = RunActionExecute
      OnUpdate = RunActionUpdate
    end
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    inherited RunEmployeeExport: TAction
      Caption = 'Run export'
      OnExecute = RunEmployeeExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
    object actRunEmployeeImport: TAction
      Caption = 'ADI -> Evolution'
      OnExecute = actRunEmployeeImportExecute
      OnUpdate = actRunEmployeeImportUpdate
    end
    object actScheduleTCImport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleTCImportExecute
      OnUpdate = actScheduleTCImportUpdate
    end
    object actScheduleEEExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEEExportExecute
      OnUpdate = RunEmployeeExportUpdate
    end
    object actTOAExport: TAction
      Caption = 'Run export'
      OnExecute = actTOAExportExecute
      OnUpdate = actTOAExportUpdate
    end
    object actScheduleTOAExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleTOAExportExecute
      OnUpdate = actTOAExportUpdate
    end
  end
end
