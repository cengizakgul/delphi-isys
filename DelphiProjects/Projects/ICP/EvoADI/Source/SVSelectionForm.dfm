object SVSelectionFm: TSVSelectionFm
  Left = 329
  Top = 194
  Width = 667
  Height = 582
  Caption = 'Select ADI Supervisors'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 507
    Width = 659
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      659
      41)
    object bbCancel: TBitBtn
      Left = 576
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 1
      Kind = bkCancel
    end
    object bbOk: TBitBtn
      Left = 488
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
  end
  inline SVSelectionFrame: TSVSelectionFrm
    Left = 0
    Top = 0
    Width = 659
    Height = 507
    Align = alClient
    TabOrder = 1
    inherited dgEE: TReDBGrid
      Width = 659
      Height = 507
    end
  end
end
