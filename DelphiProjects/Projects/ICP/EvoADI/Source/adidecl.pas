unit adidecl;

interface

uses
  sysutils, xmlintf, common,
  bndRetrieveAllEmployees, bndRequestPayrollData, bndRetrieveAllPayTypes,
  bndUpdateEmployee, bndRetrieveSettings, bndRetrieveAllEmployeescodes,
  bndRetrieveAllPayGroups, bndRetrieveAllUsers, timeclockimport, gdyCommonLogger,
  bndEmployeeStatuses, issettings, RateImportOptionFrame, gdyBinder,
  bndRetrieveAllAccrualTypes, bndRetrieveAllPayCalendar;

const
  ExtAppName = 'ADI';

type
  TADIConnectionParam = record
    UserName: string;
    Password: string;
    SavePassword: boolean;
    ClientURL: string;
  end;

  TFieldExportInfo = record
    UserFriendlyName: string;
    ConfigKey: string;
  end;

  TFieldToExport = (
    fteLastName, fteFirstName, fteMiddleInitial, fteShortName, fteInitials, fteSSN, fteStatus, fteRates, fteCompany, fteLocationADI,
    fteDivisionADI, fteDepartmentADI, ftePositionADI, fteSupervisor, fteBadgeID_ClockID, fteAddressline1,
    fteAddressline2, fteCity, fteZipCode, fteState, ftePhone, fteEmailAddress, fteHireDate, fteBirthDate,
    fteReviewDate, fteSalaried, fteGender, fteRace);

  TFieldsToExport = set of TFieldToExport;

  TADIEEOptions = record
    FieldsToExport: TFieldsToExport;
    DefaultPayGroupCode: string;
    AllowDeletionOfEE: boolean;
  end;

  TADITCOptions = record
    Summarize: boolean;
    TransferTempHierarchy: boolean;
    RateImport: TRateImportOption;
    AllowImportToBatchesWithUserEarningLines: boolean;
  end;

  EADIError = class(Exception)
  public
    Code: string;
  public
    constructor Create(acode, descr: Variant; details: string);
  end;

  EADIEmptyResponseError = class(Exception);

  TADILevelRec = record
    Name: string;
    Data: IXMLDocument;
    DisabledReason: string;
  end;

  TADILevel = (adiCompany = 1, adiLocation = 2, adiDivision = 3, adiDepartment = 4, adiPosition = 5);

  IADIConnection = interface
['{CF449340-445C-471B-91E1-9EFEB7ABC654}']
    function RetrieveAllUsers: bndRetrieveAllUsers.IXMLUsersType;
    function GetGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
    function RetrieveAllPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
    function RetrieveAllCodes(codetype: integer): IXMLDocument;
    function GetEmployeeStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
    function RetrieveAllLevels(level: TADILevel): TADILevelRec;
    function RetrieveAllEmployees: bndRetrieveAllEmployees.IXMLEmployeesType;
    function RetrieveAllPayTypes: IXMLPaytypesType;
    function RetrieveSettings: IXMLSettingsType;
    function RetrieveAllAccrualTypes: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType;
    function RetrieveAllPayCalendar(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;

    function RequestPayrollData(period: TPayPeriod): bndRequestPayrollData.IXMLResponseType;

    procedure UpdateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
    procedure DeleteEmployee(code: widestring);
    procedure CreateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);

    procedure UpdateAccrualBalance(ee: bndRetrieveAllEmployees.IXMLEmployeeType; accrualtypeGUID: string; balance: double; date: TDateTime);
  end;

const
  FieldExportInfos: array [TFieldToExport] of TFieldExportInfo =
  (
    ( UserFriendlyName: 'Last name'; ConfigKey: 'Last name'),
    ( UserFriendlyName: 'First name'; ConfigKey: 'First name'),
    ( UserFriendlyName: 'Middle initial'; ConfigKey: 'Middle initial'),
    ( UserFriendlyName: 'Short name'; ConfigKey: 'Short name'),
    ( UserFriendlyName: 'Initials'; ConfigKey: 'Initials'),
    ( UserFriendlyName: 'SSN'; ConfigKey: 'SSN'),
    ( UserFriendlyName: 'Status'; ConfigKey: 'Status'),
    ( UserFriendlyName: 'Rates'; ConfigKey: 'Rates'),
    ( UserFriendlyName: 'Company'; ConfigKey: 'Company'),
    ( UserFriendlyName: 'Location (ADI)'; ConfigKey: 'Location (ADI)'),
    ( UserFriendlyName: 'Division (ADI)'; ConfigKey: 'Division (ADI)'),
    ( UserFriendlyName: 'Department (ADI)'; ConfigKey: 'Department (ADI)'),
    ( UserFriendlyName: 'Position (ADI)'; ConfigKey: 'Position (ADI)'),
    ( UserFriendlyName: 'Supervisor'; ConfigKey: 'Supervisor'),
    ( UserFriendlyName: 'Badge ID/Clock ID #'; ConfigKey: 'Badge ID - Clock ID'),
    ( UserFriendlyName: 'Address line 1'; ConfigKey: 'Address line 1'),
    ( UserFriendlyName: 'Address line 2'; ConfigKey: 'Address line 2'),
    ( UserFriendlyName: 'City'; ConfigKey: 'City'),
    ( UserFriendlyName: 'Zip code'; ConfigKey: 'Zip code'),
    ( UserFriendlyName: 'State'; ConfigKey: 'State'),
    ( UserFriendlyName: 'Phone'; ConfigKey: 'Phone'),
    ( UserFriendlyName: 'E-mail address'; ConfigKey: 'E-mail address'),
    ( UserFriendlyName: 'Hire date'; ConfigKey: 'Hire date'),
    ( UserFriendlyName: 'Birth date'; ConfigKey: 'Birth date'),
    ( UserFriendlyName: 'Review date'; ConfigKey: 'Review date'),
    ( UserFriendlyName: 'Salaried'; ConfigKey: 'Salaried'),
    ( UserFriendlyName: 'Gender'; ConfigKey: 'Gender'),
    ( UserFriendlyName: 'Race'; ConfigKey: 'Race')
  );

function LoadFieldsToExport(conf: IisSettings; root: string): TFieldsToExport;
procedure SaveFieldsToExport(const fieldsToExport: TFieldsToExport; conf: IisSettings; root: string);

function LoadADIEEOptions(conf: IisSettings; root: string): TADIEEOptions;
procedure SaveADIEEOptions( const options: TADIEEOptions; conf: IisSettings; root: string);
procedure LogADIEEOptions(Logger: ICommonLogger; options: TADIEEOptions);
function DescribeADIEEOptions(options: TADIEEOptions): string;

procedure LogADIConnectionParam(Logger: ICommonLogger; const param: TADIConnectionParam);
function LoadADIConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TADIConnectionParam;
procedure SaveADIConnectionParam(Logger: ICommonLogger; const param: TADIConnectionParam; conf: IisSettings; root: string);

procedure LogADITCOptions(Logger: ICommonLogger; options: TADITCOptions);
function DescribeADITCOptions(options: TADITCOptions): string;
function LoadADITCOptions(conf: IisSettings; root: string): TADITCOptions;
procedure SaveADITCOptions( const options: TADITCOptions; conf: IisSettings; root: string);

function IsSameADIConnectionParam(one, another: TADIConnectionParam): boolean;
function IsCompleteADIConnectionParam(param: TADIConnectionParam): boolean;

const
  DBDTBinding: TBindingDesc =
    ( Name: 'DBDTBinding';
      Caption: 'Mapped levels';
      LeftDesc: ( Caption: 'Evolution D/B/D/T levels'; Unique: false; KeyFields: 'CODE'; ListFields: 'LEVEL');
      RightDesc: ( Caption: 'ADI hierarchy levels'; Unique: true; KeyFields: 'CODE'; ListFields: 'LEVEL');
      HideKeyFields: true
    );

  EEStatusBinding: TBindingDesc =
    ( Name: 'EEStatusesBinding';
      Caption: 'Mapped employee statuses';
      LeftDesc: ( Caption: 'Evolution employee statuses'; Unique: true; KeyFields: 'CODE'; ListFields: 'NAME');
      RightDesc: ( Caption: 'ADI employee statuses'; Unique: false; KeyFields: 'CODE'; ListFields: 'DESCRIPTION;STATUSCLASS')
    );

  EthnicityBinding: TBindingDesc =
    ( Name: 'EthnicityBinding';
      Caption: 'Mapped ethnicities and races';
      LeftDesc: ( Caption: 'Evolution ethnicities'; Unique: true; KeyFields: 'CODE'; ListFields: 'NAME');
      RightDesc: ( Caption: 'ADI races'; Unique: false; KeyFields: 'CODE'; ListFields: 'DESCRIPTION')
    );

  TOABinding: TBindingDesc =
    ( Name: 'TOABinding';
      Caption: 'Mapped TOA types';
      LeftDesc: ( Caption: 'Evolution TOA types'; Unique: true; KeyFields: 'DESCRIPTION'; ListFields: 'DESCRIPTION'); 
      RightDesc: ( Caption: 'ADI TOA types'; Unique: false; KeyFields: 'GUID'; ListFields: 'CODE;DESCRIPTION');
      HideKeyFields: true;
    );

implementation

uses
  variants, gdycrypt, gdycommon;

const
  cKey='Form.Button';

{ EADIError }

constructor EADIError.Create(acode, descr: Variant; details: string);
begin
  Code := trim(VarToStr(acode));
  CreateFmt('ADI error (%s): %s%s', [Code, VarToStr(descr), details]);
end;

procedure LogADIEEOptions(Logger: ICommonLogger; options: TADIEEOptions);
var
  i: TFieldToExport;
begin
  logger.LogContextItem( 'Default pay group code', options.DefaultPayGroupCode );
  logger.LogContextItem( 'Allow deletion of ADI employee records', BoolToStr(options.AllowDeletionOfEE, true) );
  for i := low(TFieldToExport) to high(TFieldToExport) do
    logger.LogContextItem( 'Export '+FieldExportInfos[i].UserFriendlyName, BoolToStr(i in options.FieldsToExport, true) );
end;

function DescribeADIEEOptions(options: TADIEEOptions): string;
var
  i: TFieldToExport;
begin
  Result := '';
  Result := Result + 'Default pay group code: ' + options.DefaultPayGroupCode + #13#10;
  Result := Result + 'Allow deletion of ADI employee records: ' + BoolToStr(options.AllowDeletionOfEE, true) + #13#10;
  for i := low(TFieldToExport) to high(TFieldToExport) do
    Result := Result + 'Export '+FieldExportInfos[i].UserFriendlyName+ ': ' + BoolToStr(i in options.FieldsToExport, true) + #13#10;
end;

procedure LogADITCOptions(Logger: ICommonLogger; options: TADITCOptions);
begin
  logger.LogContextItem( 'Summarize', BoolToStr(options.Summarize, true) );
  logger.LogContextItem( 'Transfer temp hierarchy', BoolToStr(options.TransferTempHierarchy, true) );
  LogRateImportOption(logger, options.RateImport, 'ADI');
  logger.LogContextItem( 'Allow import to batches that already have checks with earnings', BoolToStr(options.AllowImportToBatchesWithUserEarningLines, true) );
end;

function DescribeADITCOptions(options: TADITCOptions): string;
begin
  Result := '';
  Result := Result + 'Summarize: ' + BoolToStr(options.Summarize, true) + #13#10;
  Result := Result + 'Transfer temp hierarchy: ' + BoolToStr(options.TransferTempHierarchy, true) + #13#10;
  Result := Result + DescribeRateImportOption(options.RateImport, 'ADI') + #13#10;
  Result := Result + 'Allow import to batches that already have checks with earnings: ' + BoolToStr(options.AllowImportToBatchesWithUserEarningLines, true);
end;

procedure LogADIConnectionParam(Logger: ICommonLogger; const param: TADIConnectionParam);
begin
  Logger.LogContextItem('ADI client URL', param.ClientURL);
  Logger.LogContextItem('ADI user name', param.UserName);
end;

function LoadADIConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TADIConnectionParam;
begin
  Logger.LogEntry('Loading ADI connection');
  try
    try
      root := root + IIF(root='','','\');
//      Logger.LogContextItem('Key', root);
      Result.UserName := conf.AsString[root+'UserName'];
      if conf.GetValueNames(root).IndexOf('Password') <> -1 then
        Result.Password := conf.AsString[root+'Password']
      else
        Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
      Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
      Result.ClientURL := conf.AsString[root+'ClientURL'];
    except
      Logger.PassthroughException;
    end
  finally
    Logger.LogExit;
  end;
end;

procedure SaveADIConnectionParam(Logger: ICommonLogger; const param: TADIConnectionParam; conf: IisSettings; root: string);
begin
  Logger.LogEntry('Saving ADI connection');
  try
    try
      LogADIConnectionParam(Logger, param);
      root := root + IIF(root='','','\');
      conf.AsString[root+'Username'] := param.Username;
      conf.DeleteValue(root+'Password');
      if param.SavePassword then
        conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
      else
        conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
      conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
      conf.AsString[root+'ClientURL'] := param.ClientURL;
    except
      Logger.PassthroughException;
    end
  finally
    Logger.LogExit;
  end;
end;

function LoadFieldsToExport(conf: IisSettings; root: string): TFieldsToExport;
var
  i: TFieldToExport;
begin
  Result := [];
  root := WithoutTrailingSlash(root);
  if conf.GetChildNodes(root).IndexOf('FieldsToExport') <> -1 then
  begin
    root := root + IIF(root='','','\') + 'FieldsToExport\';
    for i := low(TFieldToExport) to high(TFieldToExport) do
      if conf.AsBoolean[root+FieldExportInfos[i].ConfigKey] then
        Include(Result, i);
  end
  else
  begin
    for i := low(TFieldToExport) to high(TFieldToExport) do
      Include(Result, i);

    root := root + IIF(root='','','\');
    if not conf.AsBoolean[root+'ExportSSN'] then
      Exclude(Result, fteSSN);
    if not conf.AsBoolean[root+'ExportRates'] then
      Exclude(Result, fteRates);
  end
end;

procedure SaveFieldsToExport(const fieldsToExport: TFieldsToExport; conf: IisSettings; root: string);
var
  i: TFieldToExport;
begin
  root := WithoutTrailingSlash(root);
  root := root + IIF(root='','','\') + 'FieldsToExport\';

  for i := low(TFieldToExport) to high(TFieldToExport) do
    conf.AsBoolean[root+FieldExportInfos[i].ConfigKey] := i in fieldsToExport;
end;

function LoadADIEEOptions(conf: IisSettings; root: string): TADIEEOptions;
begin
  root := root + IIF(root='','','\') + 'EmployeeExportOptions\';
  Result.FieldsToExport := LoadFieldsToExport(conf, root);
  Result.DefaultPayGroupCode := conf.AsString[root+'DefaultPayGroupCode'];
  Result.AllowDeletionOfEE := conf.AsBoolean[root+'AllowDeletionOfEE'];
end;

procedure SaveADIEEOptions( const options: TADIEEOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'EmployeeExportOptions\';
  SaveFieldsToExport(options.FieldsToExport, conf, root);
  conf.AsString[root+'DefaultPayGroupCode'] := options.DefaultPayGroupCode;
  conf.AsBoolean[root+'AllowDeletionOfEE'] := options.AllowDeletionOfEE;
end;

function LoadADITCOptions(conf: IisSettings; root: string): TADITCOptions;
begin
  root := root + IIF(root='','','\') + 'TCImportOptions\';
  Result.Summarize := conf.AsBoolean[root+'Summarize'];
  Result.TransferTempHierarchy := conf.AsBoolean[root+'TransferTempHierarchy'];
  Result.RateImport := TRateImportOption(conf.AsInteger[root+'RateImport']);
  if (Result.RateImport < low(TRateImportOption)) or (Result.RateImport > high(TRateImportOption)) then
    Result.RateImport := rateUseExternal;
  Result.AllowImportToBatchesWithUserEarningLines := conf.AsBoolean[root+'AllowImportToBatchesWithUserEarningLines'];
end;

procedure SaveADITCOptions( const options: TADITCOptions; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'TCImportOptions\';
  conf.AsBoolean[root+'Summarize'] := options.Summarize;
  conf.AsBoolean[root+'TransferTempHierarchy'] := options.TransferTempHierarchy;
  conf.AsInteger[root+'RateImport'] := ord(options.RateImport);
  conf.AsBoolean[root+'AllowImportToBatchesWithUserEarningLines'] := options.AllowImportToBatchesWithUserEarningLines;
end;

function IsSameADIConnectionParam(one, another: TADIConnectionParam): boolean;
begin
  Result := (trim(one.UserName) = trim(another.UserName)) and (trim(one.Password) = trim(another.Password)) and
    (trim(one.ClientURL) = trim(another.ClientURL));
end;

function IsCompleteADIConnectionParam(param: TADIConnectionParam): boolean;
begin
  Result := (trim(param.UserName) <> '') and (trim(param.Password) <> '') and (trim(param.ClientURL) <> '');
end;

end.
