object ADIEEOptionsFrm: TADIEEOptionsFrm
  Left = 0
  Top = 0
  Width = 807
  Height = 130
  TabOrder = 0
  inline FieldsToExportFrame: TFieldsToExportFrm
    Left = 0
    Top = 0
    Width = 494
    Height = 130
    Align = alClient
    TabOrder = 0
    inherited clbFields: TCheckListBox
      Top = 24
      Width = 494
      Height = 106
    end
    inherited Panel1: TPanel
      Width = 494
      Height = 24
      inherited Label1: TLabel
        Top = 5
      end
    end
  end
  object Panel1: TPanel
    Left = 494
    Top = 0
    Width = 313
    Height = 130
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 11
      Top = 5
      Width = 221
      Height = 13
      Caption = 'Default pay group for newly created employees'
    end
    object Label2: TLabel
      Left = 8
      Top = 96
      Width = 297
      Height = 19
      Caption = 'Evolution supervisor names should match ADI user names'
      WordWrap = True
    end
    object cbPayGroup: TComboBox
      Left = 9
      Top = 24
      Width = 297
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbPayGroupsClick
    end
    object cbAllowDeletingOfEE: TCheckBox
      Left = 9
      Top = 54
      Width = 245
      Height = 17
      Caption = 'Allow deletion of ADI employee records'
      TabOrder = 1
      OnClick = cbAllowDeletingOfEEClick
    end
  end
end
