unit aditasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, eefilter, adidecl, common,
  evodata, adicache, classes, scheduledCustomTask, timeclockimport,
  toaimport, evoapiconnectionutils;

type
  TADITaskAdapter = class(TTaskAdapterCustomBase)
  private
    function CreateExtAppConnection: IADIConnection;
    function ADIClientGUID: string;
  protected
    function GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData; override;
    function DoExportTOA: TTOAStat; override;
  published
    procedure EEExport_Execute;
    function EEExport_Describe: string;
    function TCImport_Describe: string;
    function TOAExport_Describe: string;
  end;

function NewEEExportTask(ClCo: TEvoCompanyDef; eefilter: TEEFilter; EEExportOptions: TADIEEOptions): IScheduledTask;
function NewTCImportTask(ClCo: TEvoCompanyDef; TCImportOptions: TADITCOptions): IScheduledTask;
function NewTOAExportTask(ClCo: TEvoCompanyDef): IScheduledTask;

procedure EditTask(task: IScheduledTask; EvoData: TEvoData; adiConns: TADIConnections; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection,
  aditime, gdyclasses, gdyCommon,
  TCImportDialog, gdyDialogEngine, EEExportDialog, controls, kbmMemTable, dialogs;

function NewEEExportTask(ClCo: TEvoCompanyDef; eefilter: TEEFilter; EEExportOptions: TADIEEOptions): IScheduledTask;
begin
  Result := NewTask('EEExport', 'Employee Export', ClCo); //TADITaskExecutor.EEExport
  SaveEEFilter(eefilter, Result.ParamSettings, '');
  SaveADIEEOptions( EEExportOptions, Result.ParamSettings, '' );
end;

function NewTCImportTask(ClCo: TEvoCompanyDef; TCImportOptions: TADITCOptions): IScheduledTask;
begin
  Result := NewTask('TCImport', 'Timeclock Data Import', ClCo); //TADITaskExecutor.TCImport
  SaveADITCOptions( TCImportOptions, Result.ParamSettings, '' );
end;

function NewTOAExportTask(ClCo: TEvoCompanyDef): IScheduledTask;
begin
  Result := NewTask('TOAExport', 'TOA Export', ClCo);
end;

function ADIClientGUID(task: IScheduledTask): string;
begin
  Result := AppSettings.AsString[CompanyUniquePath(task.Company)+'\ADIClientGUID'];
  if trim(Result) = '' then
    raise Exception.Create('ADI connection is not specified');
end;

procedure EditEEExportTask(task: IScheduledTask; EvoData: TEvoData; adiConns: TADIConnections; Owner: TComponent);
var
  dlg: TEEExportDlg;
  CUSTOM_DBDT: TkbmMemTable;
begin
  EvoData.ClientDataRequired(task.Company.ClNbr);

  CUSTOM_DBDT := TkbmMemTable.Create(nil);
  try
    CUSTOM_DBDT.AttachedTo := EvoData.DS['CUSTOM_DBDT'];
    CUSTOM_DBDT.Filter := Format('CL_NBR=%d and CO_NBR=%d', [task.Company.ClNbr, task.Company.CoNbr]);
    CUSTOM_DBDT.Filtered := true;
    CUSTOM_DBDT.Open;

    dlg := TEEExportDlg.Create(nil);
    try
      dlg.Caption := task.UserFriendlyName + ' Task Parameters';
      dlg.EeFilterFrame.Init( CUSTOM_DBDT, LoadEEFilter(task.ParamSettings, '') );
      dlg.EEExportOptionsFrame.PayGroups := adiConns[ADIClientGUID(task)].RetrieveAllPayGroups;
      dlg.EEExportOptionsFrame.Options := LoadADIEEOptions(task.ParamSettings, '');
      with DialogEngine(dlg, Owner) do
        if ShowModal = mrOk then
        begin
          SaveEEFilter( dlg.EeFilterFrame.GetFilter, task.ParamSettings, '' );
          SaveADIEEOptions( dlg.EEExportOptionsFrame.Options, task.ParamSettings, '' );
        end;
    finally
      FreeAndNil(dlg);
    end;
  finally
    FreeAndNil(CUSTOM_DBDT);
  end;
end;

procedure EditTCImportTask(task: IScheduledTask; Owner: TComponent);
var
  dlg: TTCImportDlg;
begin
  dlg := TTCImportDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.TCImportOptionsFrame.Options := LoadADITCOptions(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveADITCOptions( dlg.TCImportOptionsFrame.Options, task.ParamSettings, '' );
  finally
    FreeAndNil(dlg);
  end;
end;

procedure EditTask(task: IScheduledTask; EvoData: TEvoData; adiConns: TADIConnections; Owner: TComponent);
begin
  if task.Name = 'TCImport' then
    EditTCImportTask(task, Owner)
  else if task.Name = 'EEExport' then
    EditEEExportTask(task, EvoData, adiConns, Owner)
  else if task.Name = 'TOAExport' then
    ShowMessage('TOA Export task has no parameters')
  else
    Assert(false);
end;

{ TADITaskExecutor }

function TADITaskAdapter.EEExport_Describe: string;
begin
  Result := DescribeADIEEOptions( LoadADIEEOptions(FTask.ParamSettings, '') );
  if Result <> '' then
    Result := Result + #13#10;
  Result := Result + DescribeEEFilter( LoadEEFilter(FTask.ParamSettings, '') );
end;

procedure TADITaskAdapter.EEExport_Execute;
var
  opname: string;

  EEFilter: TEEFilter;
  EeExportOptions: TADIEEOptions;
  EEStatusMatcher: IMatcher;
  DBDTMatcher: IMatcher;
  EthnicityMatcher: IMatcher;

  procedure DoIt;
  var
    stat: TUpdateEmployeesStat;
    EEData: TEvoEEData;
    analysisResult: TAnalyzeResult;
    updater: TADIEmployeeUpdater;
  begin
    EEData := TEvoEEData.Create( GetEvoAPICOnnection, FLogger, FTask.Company);
    try

      updater := TADIEmployeeUpdater.Create( CreateExtAppConnection, EeExportOptions, EeFilter, EEData, DBDTMatcher, EEStatusMatcher, EthnicityMatcher, FLogger);
      try
        analysisResult := updater.Analyze;
        if Length(analysisResult.Actions) > 0 then
          FLogger.LogEvent( 'Planned actions. See details.', EEUpdateActionRecsToString(analysisResult.Actions) );

        stat := updater.Apply(analysisResult.Actions, NullProgressIndicator);
        stat.Errors := stat.Errors + analysisResult.Errors;
      finally
        FreeAndNil(updater);
      end;

      if stat.Errors = 0 then
        FLogger.LogEvent(opname + ' ' + SyncStatToString(stat))
      else
        FLogger.LogWarning(opname + ' ' + SyncStatToString(stat));

    finally
      FreeAndNil(EEData);
    end;
  end;

begin
  opname := Format('%s employee records update', ['ADI']);
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      EEFilter := LoadEEFilter(FTask.ParamSettings, '');
      EeExportOptions := LoadADIEEOptions(FTask.ParamSettings, '');
      DBDTMatcher := CreateMatcher( DBDTBinding, FSettings.AsString[CompanyUniquePath(FTask.Company)+'\DBDTMapping\DataPacket'] );
      EEStatusMatcher := CreateMatcher( EEStatusBinding, FSettings.AsString[ADIConnectionUniquePath(ADICLientGUID)+'\EEStatusMapping\DataPacket'] );
      EthnicityMatcher := CreateMatcher( EthnicityBinding, FSettings.AsString[ADIConnectionUniquePath(ADICLientGUID)+'\EthnicityMapping\DataPacket'] );

      DoIt;
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADITaskAdapter.ADIClientGUID: string;
begin
  Result := aditasks.ADIClientGUID(FTask);
end;

function TADITaskAdapter.CreateExtAppConnection: IADIConnection;
begin
  with TADIConnections.Create(FLogger) do
  try
    Load(FSettings);
    Result := Connection[ADICLientGUID];
  finally
    Free;
  end;
end;

function TADITaskAdapter.TCImport_Describe: string;
begin
  Result := DescribeADITCOptions( LoadADITCOptions(FTask.ParamSettings, '') );
end;

function TADITaskAdapter.GetTimeClockData(batch: TEvoPayrollBatchDef): TTimeClockImportData;
var
  options: TADITCOptions;
  DBDTMatcher: IMatcher;
begin
  options := LoadADITCOptions(FTask.ParamSettings, '');
  DBDTMatcher := CreateMatcher( DBDTBinding, FSettings.AsString[CompanyUniquePath(batch.Company)+'\DBDTMapping\DataPacket'] );

  if batch.HasUserEarningsLines and not options.AllowImportToBatchesWithUserEarningLines then
    raise Exception.Create( 'This payroll batch already has checks with earnings. You may be doing import second time.' );

  Result := aditime.GetTimeClockData(FLogger, CreateExtAppConnection, options, batch.Company, batch.PayPeriod, DBDTMatcher);
end;

function TADITaskAdapter.TOAExport_Describe: string;
begin
  Result := '';
end;

function TADITaskAdapter.DoExportTOA: TTOAStat;
var
  TOAMatcher: IMatcher;
begin
  TOAMatcher := CreateMatcher( TOABinding, FSettings.AsString[CompanyUniquePath(FTask.Company)+'\TOAMapping\DataPacket'] );
  Result := aditime.ExportTOA( CreateExtAppConnection, GetEvoAPICOnnection, TOAMatcher, FTask.Company, FLogger, NullProgressIndicator );
end;


end.
