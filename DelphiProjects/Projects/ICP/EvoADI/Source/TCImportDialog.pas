unit TCImportDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, ADITCOptionsFrame;

type
  TTCImportDlg = class(TDialogBase)
    TCImportOptionsFrame: TADITCOptionsFrm;
  private
    { Private declarations }
  public
    constructor Create( Owner: TComponent ); override;
  end;


implementation

{$R *.dfm}

{ TTCImportDlg }

constructor TTCImportDlg.Create(Owner: TComponent);
begin
  inherited;
  Caption := 'Timeclock Data Import Parameters';
end;

end.
