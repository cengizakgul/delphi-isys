inherited DBDTFrm: TDBDTFrm
  Width = 754
  Height = 554
  inherited BinderFrm1: TBinderFrm
    Width = 754
    Height = 554
    inherited evSplitter2: TSplitter
      Top = 290
      Width = 754
    end
    inherited pnltop: TPanel
      Width = 754
      Height = 290
      inherited evSplitter1: TSplitter
        Height = 290
      end
      inherited pnlTopLeft: TPanel
        Height = 290
        inherited dgLeft: TReDBGrid
          Height = 265
        end
      end
      inherited pnlTopRight: TPanel
        Width = 484
        Height = 290
        inherited evPanel4: TPanel
          Width = 484
        end
        inherited dgRight: TReDBGrid
          Width = 484
          Height = 265
        end
      end
    end
    inherited evPanel1: TPanel
      Top = 295
      Width = 754
      inherited pnlbottom: TPanel
        Width = 754
        inherited evPanel5: TPanel
          Width = 754
        end
        inherited dgBottom: TReDBGrid
          Width = 754
        end
      end
      inherited pnlMiddle: TPanel
        Width = 754
      end
    end
  end
  object cdEvoDBDT: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 392
    Top = 264
    object cdEvoDBDTLEVEL: TStringField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
      Size = 1
    end
    object cdEvoDBDTNAME: TStringField
      DisplayLabel = 'Evolution level'
      FieldName = 'LEVEL'
      Size = 15
    end
  end
  object cdADIHierarchy: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 408
    Top = 224
    object cdADIHierarchyNUMBER: TIntegerField
      DisplayLabel = 'Code'
      FieldName = 'CODE'
    end
    object cdADIHierarchyLEVEL: TStringField
      DisplayLabel = 'ADI level'
      FieldName = 'LEVEL'
      Size = 15
    end
  end
end
