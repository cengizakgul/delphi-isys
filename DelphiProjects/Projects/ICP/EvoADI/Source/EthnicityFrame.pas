unit EthnicityFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, bndRaceCodes,
  CustomBinderBaseFrame;

type
  TEthnicityFrm = class(TCustomBinderBaseFrm)
    cdEvoEthnicities: TClientDataSet;
    cdEvoEthnicitiesCODE: TStringField;
    cdEvoEthnicitiesNAME: TStringField;
    cdADIRaces: TClientDataSet;
    cdADIRacesCODE: TStringField;
    cdADIRacesDESCRIPTION: TStringField;
  private
    procedure InitEvoEthnicities;
    procedure InitADIRaces(races: bndRaceCodes.IXMLRacecodesType);
  public
    procedure Init(matchTableContent: string; races: bndRaceCodes.IXMLRacecodesType);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, evConsts, common, adidecl, comboChoices;

{ TEthnicityFrm }

procedure TEthnicityFrm.Init(matchTableContent: string; races: bndRaceCodes.IXMLRacecodesType);
begin
  try
    InitADIRaces(races);
    InitEvoEthnicities;

    FBindingKeeper := CreateBindingKeeper( EthnicityBinding, TClientDataSet );
    FBindingKeeper.SetTables(cdEvoEthnicities, cdADIRaces);
    FBindingKeeper.SetVisualBinder(BinderFrm1);
    FBindingKeeper.SetMatchTableFromString(matchTableContent);
    FBindingKeeper.SetConnected(true);
  except
    Uninit;
    raise;
  end
end;

procedure TEthnicityFrm.InitADIRaces(races: bndRaceCodes.IXMLRacecodesType);
var
  i: integer;
begin
  CreateOrEmptyDataSet( cdADIRaces );
  for i := 0 to races.Count-1 do
    Append(cdADIRaces, 'CODE;DESCRIPTION', [races[i].Code, races[i].Description])
end;

procedure TEthnicityFrm.InitEvoEthnicities;
var
  lines: IStr;
  i: integer;
begin
  CreateOrEmptyDataSet( cdEvoEthnicities );
  lines := SplitToLines(Ethnicity_ComboChoices);
  for i := 0 to lines.Count-1 do
    with SplitByChar(lines.Str[i], #9) do
      if Str[1] <> ETHNIC_NOT_APPLICATIVE then
        Append(cdEvoEthnicities, 'CODE;NAME', [Str[1], Str[0]])
end;

end.



