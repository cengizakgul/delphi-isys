object ADITCOptionsFrm: TADITCOptionsFrm
  Left = 0
  Top = 0
  Width = 607
  Height = 266
  Align = alLeft
  TabOrder = 0
  inline RateImportOptionFrame: TRateImportOptionFrm
    Left = 1
    Top = 4
    Width = 232
    Height = 79
    TabOrder = 0
    inherited RadioGroup1: TRadioGroup
      Left = 6
      Top = -2
      Width = 219
      Items.Strings = (
        'Use ADI rates'
        'Use Evolution D/B/D/T rate override'
        'Use Evolution employee pay rates')
      OnClick = RateImportOptionFrm1RadioGroup1Click
    end
  end
  object cbTransferTempHierachy: TCheckBox
    Left = 239
    Top = 10
    Width = 144
    Height = 17
    Caption = 'Transfer temp D/B/D/T'
    TabOrder = 1
    OnClick = cbTransferTempHierachyClick
  end
  object cbSummarize: TCheckBox
    Left = 239
    Top = 34
    Width = 81
    Height = 17
    Caption = 'Summarize'
    TabOrder = 2
    OnClick = cbSummarizeClick
  end
  object cbAllowImport: TCheckBox
    Left = 239
    Top = 58
    Width = 322
    Height = 17
    Caption = 'Allow import to batches that already have checks with earnings'
    TabOrder = 3
    OnClick = cbAllowImportClick
  end
end
