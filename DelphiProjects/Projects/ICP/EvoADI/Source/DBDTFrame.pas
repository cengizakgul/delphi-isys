unit DBDTFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, BinderFrame, common, gdyBinder, DB, DBClient,
  aditime, CustomBinderBaseFrame;

type
  TDBDTFrm = class(TCustomBinderBaseFrm)
    cdEvoDBDT: TClientDataSet;
    cdEvoDBDTNAME: TStringField;
    cdEvoDBDTLEVEL: TStringField;
    cdADIHierarchy: TClientDataSet;
    cdADIHierarchyNUMBER: TIntegerField;
    cdADIHierarchyLEVEL: TStringField;
  public
    procedure Init(matchTableContent: string; evo: TEvoDBDTInfo; adi: IADIHierarchy);
  end;

implementation

{$R *.dfm}

uses
  adidecl;

{ TDBDTFrm }

procedure TDBDTFrm.Init(matchTableContent: string; evo: TEvoDBDTInfo; adi: IADIHierarchy);
var
  i: char;
  j: TADILevel;
begin
  try
    CreateOrEmptyDataSet(cdEvoDBDT);
    for i := low(evo.Levels) to high(evo.Levels) do
      if evo.Levels[i].Enabled then
        Append(cdEvoDBDT, 'CODE;LEVEL', [i, evo.Levels[i].Name]);
    Append(cdEvoDBDT, 'CODE;LEVEL', [CLIENT_LEVEL_X_JOB, 'Job']);

    CreateOrEmptyDataSet(cdADIHierarchy);
    for j := adiLocation to high(TADILevel) do
      if adi.LevelInfo(j).Enabled then
        Append(cdADIHierarchy, 'CODE;LEVEL', [ord(j), adi.LevelInfo(j).Name]);

    FBindingKeeper := CreateBindingKeeper( DBDTBinding, TClientDataSet );
    FBindingKeeper.SetTables(cdEvoDBDT, cdADIHierarchy);
    FBindingKeeper.SetVisualBinder(BinderFrm1);
    FBindingKeeper.SetMatchTableFromString(matchTableContent);
    FBindingKeeper.SetConnected(true);
  except
    Uninit;
    raise;
  end;
end;

end.
