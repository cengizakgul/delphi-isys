object PerCompanyADIClientFrm: TPerCompanyADIClientFrm
  Left = 0
  Top = 0
  Width = 406
  Height = 29
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 97
    Height = 29
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'ADI connection'
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 97
    Top = 0
    Width = 309
    Height = 29
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      309
      29)
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 3
      Top = 4
      Width = 300
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DataField = 'GUID'
      DataSource = dsPerCompany
      KeyField = 'GUID'
      ListField = 'CLIENT_URL'
      ListSource = dsConn2
      TabOrder = 0
    end
  end
  object cdPerCompany: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 248
    object cdPerCompanyGUID: TStringField
      FieldName = 'GUID'
      Size = 38
    end
  end
  object dsConn2: TDataSource
    OnDataChange = dsConn2DataChange
    Left = 252
    Top = 32
  end
  object dsPerCompany: TDataSource
    DataSet = cdPerCompany
    Left = 336
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = cdPerCompany
    OnDataChange = DataSource1DataChange
    Left = 96
    Top = 8
  end
end
