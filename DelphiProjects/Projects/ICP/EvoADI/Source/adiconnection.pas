unit adiconnection;

interface

uses
  adidecl, gdyCommonLogger;

const
  //must be lowercase! they are tag names also
  ADILevelNames: array[TADILevel] of string = ('company', 'location', 'division', 'department', 'position');

function CreateADIConnection(param: TADIConnectionParam; logger: ICommonLogger): IADIConnection;

implementation

uses
  api, tcconnectionbase, evoapiconnectionutils, InvokeRegistry, sysutils,
  rio, gdycommon, gdyRedir, common, XMLDoc, variants, gdystrset, XMLIntf,
  bndRetrieveAllEmployees, bndRequestPayrollData, bndRetrieveAllPayTypes,
  bndUpdateEmployee, bndRetrieveSettings, bndRetrieveAllEmployeescodes,
  bndRetrieveAllPayGroups, bndRetrieveAllUsers, timeclockimport,
  bndEmployeeStatuses, bndRetrieveAllAccrualTypes, bndRetrieveAllPayCalendar;

type
  TADIConnection = class (TTCConnectionBase, IADIConnection)
  public
    constructor Create(param: TADIConnectionParam; logger: ICommonLogger);
    destructor Destroy; override;
  private
    FConn: APISoap;
    FSessionId: string;
    procedure Login(param: TADIConnectionParam);
    procedure Logout;

    function IdXMLByEECode(eecode: widestring): widestring;

    {IADIConnection}
    function RetrieveAllUsers: bndRetrieveAllUsers.IXMLUsersType;
    function GetGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
    function RetrieveAllPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
    function RetrieveAllCodes(codetype: integer): IXMLDocument;
    function GetEmployeeStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
    function RetrieveAllLevels(level: TADILevel): TADILevelRec;
    function RetrieveAllEmployees: bndRetrieveAllEmployees.IXMLEmployeesType;
    function RetrieveAllPayTypes: IXMLPaytypesType;
    function RetrieveSettings: IXMLSettingsType;
    function RetrieveAllAccrualTypes: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType;
    function RetrieveAllPayCalendar(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;

    function RequestPayrollData(period: TPayPeriod): bndRequestPayrollData.IXMLResponseType;

    procedure UpdateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
    procedure DeleteEmployee(code: widestring);
    procedure CreateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
    procedure UpdateAccrualBalance(ee: bndRetrieveAllEmployees.IXMLEmployeeType; accrualtypeGUID: string; balance: double; date: TDateTime);
  protected
    function CheckXML(doc: IXMLDocument): IXMLDocument; override;
  end;

  TStableADIConnection = class (TInterfacedObject, IADIConnection)
  public
    constructor Create(param: TADIConnectionParam; logger: ICommonLogger);

  private
    FADIConnectionParam: TADIConnectionParam;
    FLogger: ICommonLogger;
    FConn: IADIConnection;
    FRetryCount: integer;

    procedure ReconnectOrRaise;
    procedure BeforeCall;
    {IADIConnection}
    function RetrieveAllUsers: bndRetrieveAllUsers.IXMLUsersType;
    function GetGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
    function RetrieveAllPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
    function RetrieveAllCodes(codetype: integer): IXMLDocument;
    function GetEmployeeStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
    function RetrieveAllLevels(level: TADILevel): TADILevelRec;
    function RetrieveAllEmployees: bndRetrieveAllEmployees.IXMLEmployeesType;
    function RetrieveAllPayTypes: IXMLPaytypesType;
    function RetrieveSettings: IXMLSettingsType;
    function RetrieveAllAccrualTypes: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType;
    function RetrieveAllPayCalendar(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;

    function RequestPayrollData(period: TPayPeriod): bndRequestPayrollData.IXMLResponseType;

    procedure UpdateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
    procedure DeleteEmployee(code: widestring);
    procedure CreateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
    procedure UpdateAccrualBalance(ee: bndRetrieveAllEmployees.IXMLEmployeeType; accrualtypeGUID: string; balance: double; date: TDateTime);
  end;

const
  sCtxComponentADI = 'ADI connection';

{ TADIConnection }

function TADIConnection.CheckXML(doc: IXMLDocument): IXMLDocument;
  function BuildDetails(node: IXMLNode): string;
  var
    i: integer;
  begin
    for i := 0 to node.ChildNodes.Count-1 do
      Result := Result + #13#10 + node.ChildNodes[i].NodeName + ': ' + VarToStr(node.ChildNodes[i].NodeValue);
    // cannot use errordetails.ChildValues[i] because it always treats index as string
  end;

var
  error: IXMLNode;
  warningxml: IXMLNode;
  details: string;
  i: integer;
  j: integer;
  warned: boolean;
begin
  Result := doc;

  if doc.DocumentElement = nil then
    raise EADIEmptyResponseError.Create('ADI returned nothing');

  //multiple <warningxml> or <warning>? handle both cases...
  for i := 0 to doc.DocumentElement.ChildNodes.Count-1 do
    if doc.DocumentElement.ChildNodes[i].NodeName = 'warningxml' then
    begin
      warningxml := doc.DocumentElement.ChildNodes[i];
      warned := false;
      for j := 0 to warningxml.ChildNodes.Count-1 do
        if warningxml.ChildNodes[j].NodeName = 'warning' then
        begin
          FLogger.LogWarningFmt('ADI warning: %s', [VarToStr(warningxml.ChildNodes[j].ChildValues['description'])] );
          warned := true;
        end;
      if not warned then
        FLogger.LogWarning('Unknown ADI warning (no <warning> tag)');
    end;

  if doc.DocumentElement.ChildNodes.FindNode('errorxml') <> nil then
  begin
    error := doc.DocumentElement.ChildNodes['errorxml'].ChildNodes.FindNode('error');
    if error <> nil then
    begin
      if error.ChildNodes.FindNode('errordetails') <> nil then
        details := BuildDetails( error.ChildNodes['errordetails'] );
      raise EADIError.Create(error.ChildValues['code'], error.ChildValues['description'], details);
    end
    else
      raise Exception.CreateFmt('Unknown ADI error (no <error> tag)', []);
  end
end;

constructor TADIConnection.Create(param: TADIConnectionParam;
  logger: ICommonLogger);
begin
  inherited Create(logger);
  Login(param);
end;

destructor TADIConnection.Destroy;
begin
//!! this dtor may be called from finally block and by calling LogEntry erase info that exception was already reported causing double reporting
  if FSessionId <> '' then
  try
    Logout;
  except
    FLogger.StopException;
  end;
  inherited;
end;

procedure TADIConnection.Login(param: TADIConnectionParam);
var
  ADIWSDL: string;
begin
  FLogger.LogEntry('ADI AuthenticateUser');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      ADIWSDL := Format('%s/api/api.asmx?WSDL',[trim(param.ClientURL)]);
//      ADIWSDL := 'https://demo.aditime.com/evoapi/api/api.asmx?WSDL';
      LogADIConnectionParam(FLogger, param);
      FLogger.LogContextItem('ADI service WSDL url', ADIWSDL);

      FConn := api.GetAPISoap(true, ADIWSDL);
      AttachLogger(FConn);
      SetHTTPTimeouts(FConn, 30*60*1000 );
      FSessionId := ToXMLDoc( FConn.AuthenticateUser(param.UserName, param.Password), 'AuthenticateUser' ).DocumentElement.ChildNodes['login'].ChildValues['sessionid'];
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TADIConnection.Logout;
begin
//!! temporary solution: this function is called from finally block and
// LogEntry sets FExceptAddr to Null erasing information that exception was already reported

//  FLogger.LogEntry('ADI logout');
//  try
//    try
//      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      ToXMLDoc( FConn.Logout(FSessionId), 'Logout' );
//    except
//      FLogger.PassthroughException;
//    end;
//  finally
//    FLogger.LogExit;
//  end;
end;

function TADIConnection.RetrieveAllLevels(level: TADILevel): TADILevelRec;
begin
  FLogger.LogEntry('ADI RetrieveAllLevels');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Result.Name := ADILevelNames[level];
      Result.Data := nil;
      Result.DisabledReason := '';
      try
        Result.Data := ToXMLDoc( FConn.RetrieveAllLevels( inttostr(ord(level)), '', FSessionId), Format('RetrieveAllLevels(%s)',[ADILevelNames[level]]) );
      except
        on E: EADIError do
        begin
          if E.Code = '9000009' then //Hierarchy Disabled
          begin
            Result.DisabledReason := E.Message;
          end
          else
            raise;
        end;
        on EADIEmptyResponseError do //only RetrieveAllLevels is expected to return empty xml documents
          Result.Data := NewXMLDocument;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RetrieveAllEmployees: bndRetrieveAllEmployees.IXMLEmployeesType;
var
  fs: string;
begin
  FLogger.LogEntry('ADI RetrieveAllEmployees');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      //fs := '<fieldset><field>id</field><field>ssn</field><field>codes</field></fieldset>';
      fs := ''; //all fields but without codes
      Result := bndRetrieveAllEmployees.Getemployees( ToXMLDoc( FConn.RetrieveAllEmployees(fs, FSessionId), 'RetrieveAllEmployees' ) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RequestPayrollData(period: TPayPeriod): bndRequestPayrollData.IXMLResponseType;
var
  reqxml: string;
begin
  FLogger.LogEntry('ADI RequestPayrollData');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      reqxml := Format('<request type = ''PayrollSegment''><filters><filter type = ''PayrollSegment''><startdate>%s</startdate><enddate>%s</enddate>	</filter></filters></request>',
           [FormatDateTime('mm/dd/yy', period.PeriodBegin), FormatDateTime('mm/dd/yy', period.PeriodEnd)]);
      Result := bndRequestPayrollData.Getresponse( ToXMLDoc( FConn.RequestPayrollData(reqxml, FSessionId), 'RequestPayrollData' ) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RetrieveAllPayTypes: IXMLPaytypesType;
begin
  FLogger.LogEntry('ADI RetrieveAllPayTypes');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Result := Getpaytypes( ToXMLDoc( FConn.RetrieveAllPayTypes(''{fieldset}, FSessionId), 'RetrieveAllPayTypes' ) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TADIConnection.UpdateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
begin
  FLogger.LogEntry('ADI UpdateEmployee');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      ToXMLDoc( FConn.UpdateEmployee(IdXMLByEECode(ee.Employeeidnumber), ee.XML, FSessionId), 'UpdateEmployee' );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TADIConnection.DeleteEmployee(code: widestring);
begin
  FLogger.LogEntry('ADI DeleteEmployee');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      ToXMLDoc( FConn.DeleteEmployee(IdXMLByEECode(code), FSessionId), 'DeleteEmployee' );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TADIConnection.CreateEmployee(ee: bndUpdateEmployee.IXMLEmployeeType);
var
  i: integer;
begin
  FLogger.LogEntry('ADI CreateEmployee');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
{$if true}
      //these fields can't be passed to CreateEmployee
      Assert(ee.ChildNodes.FindNode('Canbecreated') = nil);
//      Assert(ee.ChildNodes.FindNode('agecertificatenumber') = nil);

      //I cannot pass empty fields to CreateEmployee but I have to pass them to UpdateEmployee
      //when I need to clear those field in ADI.
      i := 0;
      while i < ee.ChildNodes.Count-1 do
        if trim(VarToStr(ee.ChildNodes[i].NodeValue)) = '' then
          ee.ChildNodes.Delete(i)
        else
          inc(i);

      ToXMLDoc( FConn.CreateEmployee(ee.XML, FSessionId), 'CreateEmployee' );
{$else}

      //always fails with an error (9000035 Session Currently Active.)
      ee.Canbecreated := 1;
      ToXMLDoc( FConn.UpdateEmployee(IdXMLByEECode(ee.Employeeidnumber), ee.XML, FSessionId), 'UpdateEmployee(canbecreated)' );
{$ifend}
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RetrieveAllCodes(codetype: integer): IXMLDocument;
begin
  FLogger.LogEntry('ADI RetrieveAllCodes');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Assert( (codetype >= 1) and (codetype <= 8) );
      Result := ToXMLDoc( FConn.RetrieveAllCodes( inttostr(codetype), '', FSessionId), Format('RetrieveAllCodes%d',[codetype]) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RetrieveSettings: IXMLSettingsType;
begin
  FLogger.LogEntry('ADI RetrieveSettings');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Result := bndRetrieveSettings.Getsettings( ToXMLDoc( FConn.RetrieveSettings('', FSessionId), 'RetrieveSettings' ) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.GetGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
var
  fs: string;
begin
  FLogger.LogEntry('ADI RetrieveAllEmployees (only codes)');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      fs := '<fieldset><field>codes</field></fieldset>';
//      fs := '<fieldset><field>id</field><field>ssn</field><field>codes</field></fieldset>';
      Result := bndRetrieveAllEmployeescodes.Getemployees( ToXMLDoc( FConn.RetrieveAllEmployees(fs, FSessionId), 'RetrieveAllEmployees-codes' ) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RetrieveAllPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
begin
  FLogger.LogEntry('ADI RetrieveAllPayGroups');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Result := bndRetrieveAllPayGroups.Getpaygroups(ToXMLDoc( FConn.RetrieveAllPayGroups('', FSessionId), 'RetrieveAllPayGroups' ));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.RetrieveAllUsers: bndRetrieveAllUsers.IXMLUsersType;
begin
  FLogger.LogEntry('ADI RetrieveAllUsers');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Result := bndRetrieveAllUsers.Getusers( ToXMLDoc( FConn.RetrieveAllUsers( '', FSessionId), 'RetrieveAllUsers' ) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.GetEmployeeStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
begin
  Result := bndEmployeeStatuses.Getemployeestatuses( RetrieveAllCodes(6) );
end;

function CreateADIConnection(param: TADIConnectionParam; logger: ICommonLogger): IADIConnection;
begin
  Result := TStableADIConnection.Create(param, logger);
end;

function TADIConnection.RetrieveAllAccrualTypes: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType;
begin
  FLogger.LogEntry('ADI RetrieveAllAccrualTypes');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      Result := bndRetrieveAllAccrualTypes.Getaccrualtypes( ToXMLDoc( FConn.RetrieveAllAccrualTypes(FSessionId,''), 'RetrieveAllAccrualTypes') );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TADIConnection.UpdateAccrualBalance(ee: bndRetrieveAllEmployees.IXMLEmployeeType; accrualtypeGUID: string; balance: double; date: TDateTime);
var
  accrualXML: string;
begin
  FLogger.LogEntry('ADI UpdateAccrualBalance');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      Assert(FSessionId <> '');
      accrualXML :=
        '<accrual><activity>4</activity>' +
        Format('<accruedamount>%f</accruedamount><recorddate>%s</recorddate><comment>Imported from Evolution by EvoADI</comment><accrualtypekey>%s</accrualtypekey><employeekey>%s</employeekey>',[balance, DateToStdDate(date), accrualtypeGUID, ee.Id]) +
        '<actiontype>AccrualTransaction</actiontype><isearned>true</isearned></accrual>';
      ToXMLDoc( FConn.UpdateAccrualBalance(FSessionId, IdXMLByEECode(ee.Employeeidnumber), accrualXML), 'UpdateAccrualBalance');
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIConnection.IdXMLByEECode(eecode: widestring): widestring;
begin
  Result := Format('<employee><employeeid>%s</employeeid></employee>', [eecode]);
end;

function TADIConnection.RetrieveAllPayCalendar(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;
begin
  FLogger.LogEntry('ADI RetrieveAllPayCalendar');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentADI);
      FLogger.LogContextItem('Year', IntToStr(year));
      Assert(FSessionId <> '');
      Result := bndRetrieveAllPayCalendar.GetpayCalendars( ToXMLDoc( FConn.RetrieveAllPayCalendar(inttostr(year), '', FSessionId), Format('RetrieveAllPayCalendar-%d',[year])) );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

{ TStableADIConnection }

constructor TStableADIConnection.Create(param: TADIConnectionParam; logger: ICommonLogger);
begin
  FADIConnectionParam := param;
  FLogger := logger;
  FConn := TADIConnection.Create(FADIConnectionParam, FLogger);
end;

procedure TStableADIConnection.ReconnectOrRaise;
begin
  Assert( ExceptObject <> nil );
  if (FRetryCount = 0) and (ExceptObject is EADIError) and InSet((ExceptObject as EADIError).Code, ['9000032', '9000033', '9000034', '9000035']) then
  begin
    Flogger.LogEvent('Reconnecting to ADI');
    FRetryCount := FRetryCount + 1;
    FConn := TADIConnection.Create(FADIConnectionParam, FLogger);
    FLogger.LogEvent('Reconnected to ADI');
  end
  else
    Flogger.PassthroughException;
end;

procedure TStableADIConnection.BeforeCall;
begin
  FRetryCount := 0;
end;

procedure TStableADIConnection.CreateEmployee(ee: IXMLEmployeeType);
begin
  BeforeCall;
  while true do
  try
    FConn.CreateEmployee(ee);
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableADIConnection.DeleteEmployee(code: widestring);
begin
  BeforeCall;
  while true do
  try
    FConn.DeleteEmployee(code);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.GetGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetGroups;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RequestPayrollData(
  period: TPayPeriod): bndRequestPayrollData.IXMLResponseType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RequestPayrollData(period);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllCodes(codetype: integer): IXMLDocument;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllCodes(codetype);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllEmployees: bndRetrieveAllEmployees.IXMLEmployeesType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllEmployees;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllLevels(level: TADILevel): TADILevelRec;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllLevels(level);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllPayGroups;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllPayTypes: IXMLPaytypesType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllPayTypes;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllUsers: bndRetrieveAllUsers.IXMLUsersType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllUsers;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveSettings: IXMLSettingsType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveSettings;
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableADIConnection.UpdateEmployee(ee: IXMLEmployeeType);
begin
  BeforeCall;
  while true do
  try
    FConn.UpdateEmployee(ee);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.GetEmployeeStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetEmployeeStatuses;
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllAccrualTypes: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllAccrualTypes;
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableADIConnection.UpdateAccrualBalance(ee: bndRetrieveAllEmployees.IXMLEmployeeType; accrualtypeGUID: string; balance: double; date: TDateTime);
begin
  BeforeCall;
  while true do
  try
    FConn.UpdateAccrualBalance(ee, accrualtypeGUID, balance, date);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableADIConnection.RetrieveAllPayCalendar(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.RetrieveAllPayCalendar(year);
    break;
  except
    ReconnectOrRaise;
  end;
end;

initialization
  {
  http://www.borlandtalk.com/namespace-in-soapheader-how-to-change-vt112473.html

  This might or might not be related but just in case: there was a bug whereby
  Delphi would not recognize document services. The importer would miss
  emitting the call to register the interface. Something along the lines of:

  InvRegistry.RegisterInvokeOptions(TypeInfo(InterfaceName), ioDocument);

  The results of that bug is that serialization would use Section-5 encoding
  rules... which matches what you're seeing.

  ...the issue of not properly detecting document services should be addressed as of HOTFIX10 of BDS2006.
  }
  InvRegistry.RegisterInvokeOptions(TypeInfo(api.APISoap), ioDocument);


end.
