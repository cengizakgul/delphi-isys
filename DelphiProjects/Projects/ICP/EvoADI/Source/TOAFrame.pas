unit TOAFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, 
  CustomBinderBaseFrame, adidecl, bndRetrieveAllAccrualTypes;

type
  TTOAFrm = class(TCustomBinderBaseFrm)
    cdExtAppTOA: TClientDataSet;
    cdExtAppTOADESCRIPTION: TStringField;
    cdExtAppTOACODE: TStringField;
    cdExtAppTOAGUID: TStringField;
  private
    procedure InitExtAppTOATypes(extAppTOA: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType);
  public
    procedure Init(matchTableContent: string; CO_TIME_OFF_ACCRUAL: TDataSet; extAppTOA: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, common, XMLIntf;

{ TTOAFrm }

procedure TTOAFrm.Init(matchTableContent: string; CO_TIME_OFF_ACCRUAL: TDataSet; extAppTOA: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType);
begin
  FBindingKeeper := CreateBindingKeeper( TOABinding, TClientDataSet );
  CO_TIME_OFF_ACCRUAL.FieldByName('DESCRIPTION').DisplayLabel := 'Evolution TOA type';

  InitExtAppTOATypes(extAppTOA);

  FBindingKeeper.SetTables(CO_TIME_OFF_ACCRUAL, cdExtAppTOA);
  FBindingKeeper.SetVisualBinder(BinderFrm1);
  FBindingKeeper.SetMatchTableFromString(matchTableContent);
  FBindingKeeper.SetConnected(true);
end;

procedure TTOAFrm.InitExtAppTOATypes(extAppTOA: bndRetrieveAllAccrualTypes.IXMLAccrualtypesType);
var
  i: integer;
begin
  CreateOrEmptyDataSet(cdExtAppTOA);

  for i := 0 to extAppTOA.Count-1 do
    if extAppTOA[i].Isactive.NodeValue = 1 then //SameText(extAppTOA[i].Isactive.Name, 'Active') should work too
      Append(cdExtAppTOA, 'GUID;CODE;DESCRIPTION', [extAppTOA[i].Id, extAppTOA[i].Accrualtypeid, extAppTOA[i].Name]);
end;

end.



