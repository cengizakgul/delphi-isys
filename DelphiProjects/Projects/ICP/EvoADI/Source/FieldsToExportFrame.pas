unit FieldsToExportFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, adidecl, OptionsBaseFrame, StdCtrls, ExtCtrls, CheckLst;

type
  TFieldsToExportFrm = class(TOptionsBaseFrm)
    clbFields: TCheckListBox;
    Panel1: TPanel;
    Label1: TLabel;
  private
    procedure SetFieldsToExport(const Value: TFieldsToExport);
    function GetFieldsToExport: TFieldsToExport;
  public
    constructor Create( Owner: TComponent ); override;
    property FieldsToExport: TFieldsToExport read GetFieldsToExport write SetFieldsToExport;
  end;

implementation

{$R *.dfm}

{ TFieldsToExportFrm }

constructor TFieldsToExportFrm.Create(Owner: TComponent);
var
  f: TFieldToExport;
begin
  inherited;
  clbFields.Clear;
  for f := low(TFieldToExport) to high(TFieldToExport) do
    clbFields.Items.AddObject(FieldExportInfos[f].UserFriendlyName, Pointer(ord(f)));
end;

function TFieldsToExportFrm.GetFieldsToExport: TFieldsToExport;
var
  i: integer;
begin
  Result := [];
  for i := 0 to clbFields.Items.Count-1 do
    if clbFields.Checked[i] then
      Include(Result, TFieldToExport(Integer(Pointer(clbFields.Items.Objects[i]))) );
end;

procedure TFieldsToExportFrm.SetFieldsToExport(const Value: TFieldsToExport);
var
  i: integer;
begin
  for i := 0 to clbFields.Items.Count-1 do
    clbFields.Checked[i] := TFieldToExport(Integer(Pointer(clbFields.Items.Objects[i]))) in Value;
end;

end.
