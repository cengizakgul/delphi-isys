unit ADIEEOptionsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, adidecl, StdCtrls, ExtCtrls, bndRetrieveAllPayGroups,
  OptionsBaseFrame, FieldsToExportFrame;

type
  TADIEEOptionsFrm = class(TFrame)
    FieldsToExportFrame: TFieldsToExportFrm;
    Panel1: TPanel;
    Label1: TLabel;
    cbPayGroup: TComboBox;
    cbAllowDeletingOfEE: TCheckBox;
    Label2: TLabel;
    procedure lbPayGroupsClick(Sender: TObject);
    procedure cbAllowDeletingOfEEClick(Sender: TObject);
  private
    FPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
    FOptions: TADIEEOptions;
    procedure SetOptions(const Value: TADIEEOptions);
    procedure SetPayGroups(const Value: bndRetrieveAllPayGroups.IXMLPaygroupsType);
    procedure DataUpdated;
    procedure FieldsToExportChangedByUser(Sender: TObject);
  public
    constructor Create( Owner: TComponent ); override;
    property PayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType write SetPayGroups;
    property Options: TADIEEOptions read FOptions write SetOptions;
    function IsValid: boolean;
  end;

implementation

uses XMLIntf;

{$R *.dfm}

function TADIEEOptionsFrm.IsValid: boolean;
begin
  Result := cbPayGroup.ItemIndex <> -1;
end;

procedure TADIEEOptionsFrm.SetOptions(const Value: TADIEEOptions);
begin
  FOptions := Value;
  DataUpdated;
end;

procedure TADIEEOptionsFrm.SetPayGroups( const Value: bndRetrieveAllPayGroups.IXMLPaygroupsType);
var
  i: integer;
begin
  FPayGroups := Value;
  cbPayGroup.Items.Clear;
  if FPayGroups <> nil then
  begin
    for i := 0 to FPayGroups.Count-1 do
      cbPayGroup.Items.Add( trim(FPayGroups[i].Name) );
  end;    
  DataUpdated;
end;

procedure TADIEEOptionsFrm.DataUpdated;
var
  i: integer;
begin
  cbPayGroup.ItemIndex := -1;
  if FPayGroups <> nil then
    for i := 0 to FPayGroups.Count-1 do
      if trim(FPayGroups[i].Paygroupid) = trim(FOptions.DefaultPayGroupCode) then
      begin
        cbPayGroup.ItemIndex := i;
      end;
  if cbPayGroup.ItemIndex = -1 then
    if cbPayGroup.Items.Count > 0 then
    begin
      cbPayGroup.ItemIndex := 0;
      lbPayGroupsClick(nil);
    end;
  FieldsToExportFrame.FieldsToExport := FOptions.FieldsToExport;
  cbAllowDeletingOfEE.Checked := FOptions.AllowDeletionOfEE;
end;

procedure TADIEEOptionsFrm.lbPayGroupsClick(Sender: TObject);
begin
  if cbPayGroup.ItemIndex <> -1 then
    FOptions.DefaultPayGroupCode := trim(FPayGroups[cbPayGroup.ItemIndex].Paygroupid);
end;

procedure TADIEEOptionsFrm.cbAllowDeletingOfEEClick(Sender: TObject);
begin
  FOptions.AllowDeletionOfEE := cbAllowDeletingOfEE.Checked;
end;

constructor TADIEEOptionsFrm.Create(Owner: TComponent);
begin
  inherited;
  FieldsToExportFrame.OnChangeByUser := FieldsToExportChangedByUser;
end;

procedure TADIEEOptionsFrm.FieldsToExportChangedByUser(Sender: TObject);
begin                        
  FOptions.FieldsToExport := FieldsToExportFrame.FieldsToExport;
end;

end.
