unit SVSelectionForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SVSelectionFrame, StdCtrls, Buttons, ExtCtrls,
  kbmMemTable, aditime, db, evodata, gdycommonlogger;

type
  TSVSelectionFm = class(TForm)
    Panel1: TPanel;
    bbCancel: TBitBtn;
    bbOk: TBitBtn;
    SVSelectionFrame: TSVSelectionFrm;
    procedure FormActivate(Sender: TObject);
  private
  public
  end;

function SelectSV(Owner: TComponent; actions: TEEUpdateActionRecs; EEData: TEvoEEData; sv: TkbmCustomMemTable; logger: ICommonLogger): boolean;

implementation

{$R *.dfm}

function SelectSV(Owner: TComponent; actions: TEEUpdateActionRecs; EEData: TEvoEEData; sv: TkbmCustomMemTable; logger: ICommonLogger): boolean;
var
  i: integer;
  hasSV: boolean;
begin
  hasSV := true;
  for i := 0 to high(actions) do
    if (actions[i].Action = euaCreate) and not actions[i].HasEvoSV then
    begin
      hasSV := false;
      break;
    end;
  if not hasSV then
    with TSVSelectionFm.Create(Owner) do
    try
      SVSelectionFrame.Init(actions, EEData, sv, logger);
      Result := ShowModal = mrOk;
      if Result then
        SVSelectionFrame.GetSVs(actions);
    finally
      Free;
    end
  else
    Result := true;

end;

procedure TSVSelectionFm.FormActivate(Sender: TObject);
begin
  SetForegroundWindow(Handle);
end;

end.
