unit MainForm;

interface

uses                   
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoTCImportMainNewForm, ActnList, ComCtrls, gdyCommonLoggerView,
  Buttons, StdCtrls, ExtCtrls, isSettings, EvoAPIConnectionParamFrame,
  evoapiconnection, timeclockimport, evoapiconnectionutils,
  ADIConnectionParamFrame,
  common, EEStatusFrame, DBDTFrame, CustomBinderBaseFrame,
  EvolutionCompanySelectorFrame, evodata,
  EvolutionPrPrBatchFrame, adicache, wwdblook, DB, PerCompanyADIClientFrame,
  ADITCOptionsFrame, ADIEEOptionsFrame, EEFilterFrame, SchedulerFrame,
  SmtpConfigFrame, DBDTFilterFrame, scheduledTask, EthnicityFrame,
  OptionsBaseFrame, TOAFrame;

type
  TMainFm = class(TEvoTCImportMainNewFm)
    TCOptionsFrame: TADITCOptionsFrm;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox3: TGroupBox;
    ADIFrame: TADIConnectionParamFrm;
    PerCompanyADIClientFrame: TPerCompanyADIClientFrm;
    tbshEE: TTabSheet;
    EEOptionsFrame: TADIEEOptionsFrm;
    Panel5: TPanel;
    BitBtn5: TBitBtn;
    actRunEmployeeImport: TAction;
    tbshScheduler: TTabSheet;
    EeFilterFrame: TEeFilterFrm;
    actScheduleEEExport: TAction;
    BitBtn1: TBitBtn;
    SchedulerFrame: TSchedulerFrm;
    actScheduleTCImport: TAction;
    BitBtn3: TBitBtn;
    tbshSchedulerSettings: TTabSheet;
    SmtpConfigFrame: TSmtpConfigFrm;
    pcMappings: TPageControl;
    tbshEEStatus: TTabSheet;
    tbshHierarchy: TTabSheet;
    tbshEthnicity: TTabSheet;
    EeStatusFrame: TEeStatusFrm;
    DBDTFrame: TDBDTFrm;
    EthnicityFrame: TEthnicityFrm;
    tbshAccrualTypes: TTabSheet;
    TOAFrame: TTOAFrm;
    tbshTOAExport: TTabSheet;
    actTOAExport: TAction;
    actScheduleTOAExport: TAction;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    lblEEExportTaskParamInstruction: TLabel;
    lblTCImportTaskInstruction: TLabel;
    procedure RunActionExecute(Sender: TObject);
    procedure RunActionUpdate(Sender: TObject);
    procedure RunEmployeeExportExecute(Sender: TObject);
    procedure RunEmployeeExportUpdate(Sender: TObject);
    procedure actRunEmployeeImportUpdate(Sender: TObject);
    procedure actRunEmployeeImportExecute(Sender: TObject);
    procedure actScheduleEEExportExecute(Sender: TObject);
    procedure actScheduleTCImportExecute(Sender: TObject);
    procedure actScheduleTCImportUpdate(Sender: TObject);
    procedure ConnectToEvoExecute(Sender: TObject);
    procedure actScheduleTOAExportExecute(Sender: TObject);
    procedure actTOAExportExecute(Sender: TObject);
    procedure actTOAExportUpdate(Sender: TObject);
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  protected
  	function CanConnectToTCApp: boolean;
  private
    FADIConnections: TADIConnections;
    function GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
  	function UpdateEmployees: TUpdateEmployeesStat;
    function DoExportTOA: TTOAStat;

    procedure HandleSaveAllSettings;
    procedure HandleDBDTMappingChanged;
    procedure HandleEEStatusMappingChanged;
    procedure HandleEthnicityMappingChanged;
    procedure HandleTOAMappingChanged;
    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleCanEditParameters(var can: boolean);
    procedure HandleSmtpConfigChange(Sender: TObject);
    procedure HandleEEFilterChangedByUser(Sender: TObject);

    procedure SaveGlobalSettings;
    procedure SavePerCompanySettings(const oldValue: TEvoCompanyDef);
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef);

    procedure HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
    procedure HandleADIClientChanged(oldguid, guid: string);
    procedure ReInitDBDTFrame;
    procedure ReInitTOAFrame;
  end;

var
  MainFm: TMainFm;

implementation

uses
  kbmMemTable, gdyRedir, XmlRpcTypes,
  gdycommon, waitform, aditime, adidecl, adiconnection,
  SVSelectionForm, gdyclasses, gdyGlobalWaitIndicator, userActionHelpers,
  EvoAPIClientMainForm, PlannedActionConfirmationForm, EvoWaitForm, EEFilter,
  typinfo, aditasks, bndRaceCodes, xmldoc;

{$R *.dfm}

(*
X -> Y = X depends on Y

ADI, ADI ref <- OptionsFrame -> EvoCompany
ADI, ADI ref <- EeStatusFrame
ADI, ADI ref <- EthnicityFrame
ADI, ADI ref <- DBDTFrame -> EvoCompany
ADI, ADI ref <- TOAFrame -> EvoCompany
ADI ref -> EvoCompany
EEFilter -> EvoCompany
*)

constructor TMainFm.Create(Owner: TComponent);
begin
  inherited;
  FEvoData.AddDS(CO_TIME_OFF_ACCRUALDesc);
  FEvoData.AddDS(CUSTOM_DBDTDesc);

  FADIConnections := TADIConnections.Create(Logger);
  try
    FADIConnections.Load(FSettings);
  except
    Logger.StopException;
  end;
  ADIFrame.Init(FADIConnections);

  //now after initialization attach handlers
  FEvoData.Advise(HandleCompanyChanged);
  PerCompanyADIClientFrame.OnADIClientChanged := HandleADIClientChanged;

  try
    SchedulerFrame.Configure(Logger, TADITaskAdapter.Create, HandleEditParameters);
    SchedulerFrame.OnSaveAllSettings := HandleSaveAllSettings;
    SchedulerFrame.OnCanEditParameters := HandleCanEditParameters;
  except
    Logger.StopException;
  end;

  SmtpConfigFrame.Config := LoadSmtpConfig(FSettings, '');
  SmtpConfigFrame.OnChangeByUser := HandleSmtpConfigChange;

  EeFilterFrame.OnChangeByUser := HandleEEFilterChangedByUser;

  DBDTFrame.OnMatchTableChanged := HandleDBDTMappingChanged;
  EeStatusFrame.OnMatchTableChanged := HandleEEStatusMappingChanged;
  EthnicityFrame.OnMatchTableChanged := HandleEthnicityMappingChanged;
  TOAFrame.OnMatchTableChanged := HandleTOAMappingChanged;

  pcMappings.ActivePageIndex := 0;
end;

destructor TMainFm.Destroy;
begin
  try
    FADIConnections.Save(FSettings);
  except
    Logger.StopException;
  end;
  inherited;
  FreeAndNil(FADIConnections);
end;

function TMainFm.CanConnectToTCApp: boolean;
begin
	Result := (PerCompanyADIClientFrame.GUID <> '') and FADIConnections.HasCompleteParams(PerCompanyADIClientFrame.GUID);
end;

function TMainFm.GetTimeClockData(period: TPayPeriod; company: TEvoCompanyDef): TTimeClockImportData;
begin
  Result := aditime.GetTimeClockData( Logger, FADIConnections[PerCompanyADIClientFrame.GUID], TCOptionsFrame.Options, company, period, DBDTFrame.Matcher );
end;

procedure TMainFm.RunActionExecute(Sender: TObject);
begin
  RunTimeClockImport(Logger, FEvoData.GetPrBatch, GetTimeClockData, FEvoData.Connection, TCOptionsFrame.Options.AllowImportToBatchesWithUserEarningLines);
end;

procedure TMainFm.RunActionUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetPrBatch and CanConnectToTCApp;
end;

function TMainFm.UpdateEmployees: TUpdateEmployeesStat;
var
  EEData: TEvoEEData;
  analysisResult: TAnalyzeResult;
  sv: TkbmCustomMemTable;
begin
  EeFilterFrame.Check;

  EEData := FEvoData.CreateEEData;
  try
    with TADIEmployeeUpdater.Create( FADIConnections[PerCompanyADIClientFrame.GUID], EEOptionsFrame.Options, EeFilterFrame.GetFilter, EEData, DBDTFrame.Matcher, EEStatusFrame.Matcher, EthnicityFrame.Matcher, Logger) do
    try
      WaitIndicator.StartWait('Preparing');
      try
        analysisResult := Analyze;
        if Length(analysisResult.Actions) > 0 then
          Logger.LogEvent( 'Planned actions. See details.', EEUpdateActionRecsToString(analysisResult.Actions) );
        if not ConfirmActions(Self, EEUpdateActionRecsToString(analysisResult.Actions)) then
          raise Exception.Create('User cancelled operation');

        if analysisResult.ADIHasSupervisors then
        begin
          sv := CreateSVDataSet;
          try
            if not SelectSV(Self, analysisResult.Actions, EEData, sv, Logger) then
              raise Exception.Create('User cancelled operation');
          finally
            FreeAndNil(sv);
          end;
        end;
      finally
        WaitIndicator.EndWait;
      end;
      Result := Apply(analysisResult.Actions, Self as IProgressIndicator);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      Free;
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.RunEmployeeExportExecute(Sender: TObject);
begin
  userActionHelpers.RunEmployeeExport(Logger, UpdateEmployees, FEvoData.GetClCo, 'ADI');
end;

procedure TMainFm.RunEmployeeExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo and CanConnectToTCApp and EEOptionsFrame.IsValid;
end;

procedure TMainFm.actRunEmployeeImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo;
//  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo and CanConnectToTCApp and EEOptionsFrame.IsValid;
end;

procedure TMainFm.actRunEmployeeImportExecute(Sender: TObject);
var
  files: TEvoXImportInputFiles;
begin
  SetLength(files, 1);
  files[0].Filename := 'filename here';
  files[0].Filedata := 'CUSTOM_COMPANY_NUMBER,SSN,CUSTOM_EMPLOYEE_NUMBER,LAST_NAME,FIRST_NAME'#13#10 +
//                       '5002,0010-010-0001,1001,1002Bugs!,1002Bunny!';
                       '5002,001-01-0001,1001,1001Bugs!,1001Bunny!';
//  RunEvoXImport(Self, Logger, FEvoData.Connection, files, FileToString(Redirection.GetFilename(sEvoXMapFileAlias)) );
end;

procedure TMainFm.HandleEEFilterChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveEEFilter( EeFilterFrame.GetFilter, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleCompanyChanged(EvoData: TEvoData; old: TNullableEvoCompanyDef; new: TNullableEvoCompanyDef);
begin
  if old.HasValue then
    SavePerCompanySettings(old.Value);

  if new.HasValue then
    LoadPerCompanySettings(new.Value)
  else
  begin
    PerCompanyADIClientFrame.Uninit;
    DBDTFrame.UnInit;
    TOAFrame.UnInit;
    try
      EeFilterFrame.ClearAndDisable;
    except
      Logger.StopException;
    end;
  end
end;

procedure TMainFm.SavePerCompanySettings(const oldValue: TEvoCompanyDef);
begin
  try
    SaveADIEEOptions( EEOptionsFrame.Options, FSettings, CompanyUniquePath(oldValue) ); //new convention
  except
    Logger.StopException;
  end;
  try
    SaveADITCOptions( TCOptionsFrame.Options, FSettings, CompanyUniquePath(oldValue)); //new convention
  except
    Logger.StopException;
  end;
  //EeFilterFrame is now saved when it is changed
  //DBDTFrame is now saved when it is changed
  //ADIClientGUID is now saved when it is changed
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  try
  	EeFilterFrame.ClearAndDisable;
    EeFilterFrame.Init( FEvoData.DS['CUSTOM_DBDT'], LoadEEFilter(FSettings, CompanyUniquePath(Value)) );
  except
    Logger.StopException;
  end;

  try
    EEOptionsFrame.Options := LoadADIEEOptions( FSettings, CompanyUniquePath(Value)); //new convention
  except
    Logger.StopException;
  end;
  try
    TCOptionsFrame.Options := LoadADITCOptions( FSettings, CompanyUniquePath(Value)); //new convention
  except
    Logger.StopException;
  end;

  try
    PerCompanyADIClientFrame.Init( FADIConnections.DS2 );
    PerCompanyADIClientFrame.GUID := FSettings.AsString[CompanyUniquePath(Value)+'\ADIClientGUID'];
  except
    Logger.StopException;
    PerCompanyADIClientFrame.GUID := '';
  end;
  ReInitDBDTFrame;
  ReInitTOAFrame;
end;

procedure TMainFm.ReInitDBDTFrame;
begin
  if (PerCompanyADIClientFrame.GUID <> '') and FEvoData.CanGetClCo then
  begin
    WaitIndicator.StartWait('Initializing D/B/D/T Mapping');
    try
      try
        DBDTFrame.Init( FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo)+'\DBDTMapping\DataPacket'],
                        GetEvoDBDTInfo(Logger, FEvoData),
                        TADIHierarchy.Create(FADIConnections[PerCompanyADIClientFrame.GUID], Logger)
                        );
      except
        Logger.StopException;
        DBDTFrame.UnInit //if an error happens before calling Init
      end;
    finally
      WaitIndicator.EndWait;
    end;
  end
  else
    DBDTFrame.UnInit;
end;

//!! copy-pasted from ReInitDBDTFrame;
procedure TMainFm.ReInitTOAFrame;
begin
  if (PerCompanyADIClientFrame.GUID <> '') and FEvoData.CanGetClCo then
  begin
    WaitIndicator.StartWait('Initializing TOA mapping');
    try
      try
        TOAFrame.Init( FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo)+'\TOAMapping\DataPacket'],
                        FEvoData.DS[CO_TIME_OFF_ACCRUALDesc.Name],
                        FADIConnections[PerCompanyADIClientFrame.GUID].RetrieveAllAccrualTypes
                        );
      except
        Logger.StopException;
        try
          TOAFrame.Init( '', FEvoData.DS[CO_TIME_OFF_ACCRUALDesc.Name],
                        FADIConnections[PerCompanyADIClientFrame.GUID].RetrieveAllAccrualTypes
                          );
        except
          Logger.StopException;
          TOAFrame.UnInit //if an error happens before calling Init
        end;
      end;
    finally
      WaitIndicator.EndWait;
    end;
  end
  else
    TOAFrame.UnInit;
end;

procedure TMainFm.HandleADIClientChanged(oldguid, guid: string);
begin
  Assert( guid = PerCompanyADIClientFrame.GUID );
  Assert( guid <> oldguid );

  //EEStatusFrame and EhnicityFrame are saved when mapping is changed

  if GUID <> '' then
  begin
    //!! kludge
    //to get exception shown to the user if connection params are invalid
    try
      FADIConnections[GUID];
    except
      on E: Exception do
      begin
        Application.HandleException(E);
        Logger.StopException;
      end
    end;

    WaitIndicator.StartWait('Getting pay group list from ADI');
    try
      try
        EEOptionsFrame.PayGroups := FADIConnections[GUID].RetrieveAllPayGroups;
      except
        Logger.StopException;
        EEOptionsFrame.PayGroups := nil;
      end;
    finally
      WaitIndicator.EndWait;
    end;

    WaitIndicator.StartWait('Getting employee status list from ADI');
    try
      try
        EeStatusFrame.Init( FSettings.AsString[ADIConnectionUniquePath(GUID)+'\EEStatusMapping\DataPacket'], FADIConnections[GUID].GetEmployeeStatuses );
      except
        Logger.StopException;
        EeStatusFrame.UnInit; //if an error happens before calling Init
      end;
    finally
      WaitIndicator.EndWait;
    end;

    WaitIndicator.StartWait('Getting race list from ADI');
    try
      try
        EthnicityFrame.Init( FSettings.AsString[ADIConnectionUniquePath(GUID)+'\EthnicityMapping\DataPacket'], bndRaceCodes.Getracecodes(FADIConnections[GUID].RetrieveAllCodes(5)) );
      except
        Logger.StopException;
        EthnicityFrame.UnInit; //if an error happens before calling Init
      end;
    finally
      WaitIndicator.EndWait;
    end;
  end
  else
  begin
    EEOptionsFrame.PayGroups := nil;
    EeStatusFrame.UnInit;
    EthnicityFrame.UnInit;
  end;

  if FEvoData.CanGetClCo then
    try
      FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo)+'\ADIClientGUID'] := PerCompanyADIClientFrame.GUID;
    except
      Logger.StopException;
    end;

  //will reload in ReInitDBDTFrame
  if DBDTFrame.CanSave then
    FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\DBDTMapping\DataPacket'] := DBDTFrame.SaveToString;
  ReInitDBDTFrame;

  if TOAFrame.CanSave then
    FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\TOAMapping\DataPacket'] := TOAFrame.SaveToString;
  ReInitTOAFrame;
end;

procedure TMainFm.actScheduleEEExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      EeFilterFrame.Check;
      EvoFrame.ForceSavePassword;
      FADIConnections.ForceSavePassword(PerCompanyADIClientFrame.GUID);
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewEEExportTask(FEvoData.GetClCo, EeFilterFrame.GetFilter, EEOptionsFrame.Options) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleSmtpConfigChange(Sender: TObject);
begin
  SaveSmtpConfig(SmtpConfigFrame.Config, FSettings, '');
end;

procedure TMainFm.actScheduleTCImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      EvoFrame.ForceSavePassword;
      FADIConnections.ForceSavePassword(PerCompanyADIClientFrame.GUID);
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTCImportTask(FEvoData.GetClCo, TCOptionsFrame.Options) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

//can be called when a user pressed the "Run now" button
procedure TMainFm.HandleSaveAllSettings;
begin
  if FEvoData.CanGetClCo then
    SavePerCompanySettings(FEvoData.GetClCo);

  try
    if (PerCompanyADIClientFrame.GUID <> '') and EeStatusFrame.CanSave then
      FSettings.AsString[ADIConnectionUniquePath(PerCompanyADIClientFrame.GUID)+'\EEStatusMapping\DataPacket'] := EeStatusFrame.SaveToString;
  except
    Logger.StopException;
  end;

  SaveGlobalSettings;
end;

procedure TMainFm.actScheduleTCImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo and CanConnectToTCApp; //don't need payrolls and batches
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  aditasks.EditTask(task, FEvoData, FADIConnections, Self);
end;

procedure TMainFm.HandleCanEditParameters(var can: boolean);
begin
  can := FEvoData.CanGetClCo and CanConnectToTCApp;
end;

procedure TMainFm.HandleDBDTMappingChanged;
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\DBDTMapping\DataPacket'] := DBDTFrame.SaveToString;
end;

procedure TMainFm.HandleEEStatusMappingChanged;
begin
  FSettings.AsString[ADIConnectionUniquePath(PerCompanyADIClientFrame.GUID)+'\EEStatusMapping\DataPacket'] := EeStatusFrame.SaveToString;
end;

procedure TMainFm.ConnectToEvoExecute(Sender: TObject);
begin
  inherited;
  SaveGlobalSettings;
end;

procedure TMainFm.SaveGlobalSettings;
begin
  try
    FADIConnections.Save(FSettings);
  except
    Logger.StopException;
  end;

  try
    SaveEvoAPIConnectionParam( EvoFrame.Param, FSettings, '' );
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleEthnicityMappingChanged;
begin
  FSettings.AsString[ADIConnectionUniquePath(PerCompanyADIClientFrame.GUID)+'\EthnicityMapping\DataPacket'] := EthnicityFrame.SaveToString;
end;

procedure TMainFm.HandleTOAMappingChanged;
begin
  FSettings.AsString[CompanyUniquePath(FEvoData.GetClCo) + '\TOAMapping\DataPacket'] := TOAFrame.SaveToString;
end;

procedure TMainFm.actTOAExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FEvoData.CanGetClCo and CanConnectToTCApp and TOAFrame.CanCreateMatcher;
end;

procedure TMainFm.actScheduleTOAExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      EvoFrame.ForceSavePassword;
      FADIConnections.ForceSavePassword(PerCompanyADIClientFrame.GUID);
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewTOAExportTask(FEvoData.GetClCo) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actTOAExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      userActionHelpers.RunTOAExport(Logger, DoExportTOA, FEvoData.GetClCo, ExtAppName);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TMainFm.DoExportTOA: TTOAStat;
begin
  Result := aditime.ExportTOA( FADIConnections[PerCompanyADIClientFrame.GUID], FEvoData.Connection, TOAFrame.Matcher, FEvoData.GetClCo, Logger, Self as IProgressIndicator );
end;

end.

