unit ADIConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, adidecl, Grids, Wwdbigrd, Wwdbgrid, dbcomp,
  DBCtrls, Mask, CustomBinderBaseFrame, EeStatusFrame, DB, adicache,
  ActnList, Buttons;

type
  TADIConnectionParamFrm = class(TFrame)
    pnlControls: TPanel;
    pnlGrid: TPanel;
    dbGrid: TReDBGrid;
    Panel5: TPanel;
    Label1: TLabel;
    dbClientURL: TDBEdit;
    Label4: TLabel;
    dbUsername: TDBEdit;
    Label7: TLabel;
    dbPassword: TDBEdit;
    dbSavePassword: TDBCheckBox;
    Label2: TLabel;
    Label3: TLabel;
    dsConn: TDataSource;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    ActionList2: TActionList;
    actInsert: TAction;
    BitBtn2: TBitBtn;
    actDelete: TAction;
    procedure actInsertUpdate(Sender: TObject);
    procedure actInsertExecute(Sender: TObject);
    procedure dsConnDataChange(Sender: TObject; Field: TField);
    procedure actDeleteUpdate(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
  private
    procedure PostIfNeeded;
  public
    procedure Init(conns: TADIConnections);
  end;

implementation

{$R *.dfm}

uses
  common, gdyDeferredCall;

{ TADIConnectionParamFrm }


procedure TADIConnectionParamFrm.Init(conns: TADIConnections);
begin
  dbGrid.Selected.Clear;
  AddSelected(dbGrid.Selected, 'CLIENT_URL', 80, 'Client URL', true);
  AddSelected(dbGrid.Selected, 'USER_NAME', 20, 'User name', true);
  AddSelected(dbGrid.Selected, 'SAVE_PASSWORD', 0, 'Password saved', true);
  dsConn.DataSet := conns.DS;
end;

procedure TADIConnectionParamFrm.actInsertUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := dsConn.DataSet.State = dsBrowse;
end;

procedure TADIConnectionParamFrm.actInsertExecute(Sender: TObject);
begin
  dsConn.DataSet.Append;
  Application.MainForm.ActiveControl := dbClientURL;
end;

procedure TADIConnectionParamFrm.dsConnDataChange(Sender: TObject;
  Field: TField);
begin
  if dsConn.State in [dsEdit, dsInsert] then
    DeferredCall(PostIfNeeded);
end;

procedure TADIConnectionParamFrm.PostIfNeeded;
begin
  if dsConn.State in [dsEdit, dsInsert] then
    dsConn.DataSet.Post;
end;

procedure TADIConnectionParamFrm.actDeleteUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (dsConn.DataSet.State = dsBrowse) and (dsConn.DataSet.RecordCount>0);
end;

procedure TADIConnectionParamFrm.actDeleteExecute(Sender: TObject);
begin
  if MessageDlg('Delete connection?', mtConfirmation, [mbYes, mbNo], 0{, [mbNo]}) = mrYes then
    dsConn.DataSet.Delete;
end;

end.
