inherited EEExportDlg: TEEExportDlg
  Width = 853
  Height = 614
  inline EEExportOptionsFrame: TADIEEOptionsFrm
    Left = 0
    Top = 488
    Width = 853
    Height = 126
    Align = alBottom
    TabOrder = 0
    inherited FieldsToExportFrame: TFieldsToExportFrm
      Width = 540
      Height = 126
      inherited clbFields: TCheckListBox
        Width = 540
        Height = 102
      end
      inherited Panel1: TPanel
        Width = 540
      end
    end
    inherited Panel1: TPanel
      Left = 540
      Height = 126
    end
  end
  inline EeFilterFrame: TEeFilterFrm
    Left = 0
    Top = 0
    Width = 853
    Height = 488
    Align = alClient
    TabOrder = 1
    inherited GroupBox1: TGroupBox
      Width = 853
      Height = 488
      inherited Panel1: TPanel
        Height = 471
        inherited EEStatusFilterFrame: TCodesFilterFrm
          Height = 258
          inherited GroupBox1: TGroupBox
            Height = 258
            inherited lbStatuses: TCheckListBox
              Height = 215
            end
          end
        end
        inherited EEPositionStatusFilterFrame: TCodesFilterFrm
          Top = 258
        end
        inherited EESalaryFilterFrame: TEESalaryFilterFrm
          Top = 409
        end
      end
      inherited DBDTFilterFrame: TDBDTFilterFrm
        Width = 610
        Height = 471
        inherited GroupBox1: TGroupBox
          Width = 610
          Height = 471
          inherited Panel1: TPanel
            Width = 606
          end
          inherited dgDBDT: TISDBCheckGrid
            Width = 606
            Height = 420
          end
        end
      end
    end
  end
end
