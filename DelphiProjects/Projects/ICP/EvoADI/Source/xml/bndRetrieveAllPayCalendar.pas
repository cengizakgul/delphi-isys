
{***************************************************************************************************************}
{                                                                                                               }
{                                               XML Data Binding                                                }
{                                                                                                               }
{         Generated on: 3/8/2012 12:49:44 AM                                                                    }
{       Generated from: E:\job\IS\integration-release\EvoADI\2.2\EvoADI\xml\RetrieveAllPayCalendar\Output.xml   }
{                                                                                                               }
{***************************************************************************************************************}

unit bndRetrieveAllPayCalendar;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLPayCalendarsType = interface;
  IXMLPaycalendarType = interface;
  IXMLPaycalendartypeType = interface;
  IXMLPayweekdayType = interface;
  IXMLPaycalendarstartdayType = interface;
  IXMLYearstartType = interface;
  IXMLIssundayType = interface;
  IXMLIsmondayType = interface;
  IXMLIstuesdayType = interface;
  IXMLIswednesdayType = interface;
  IXMLIsthursdayType = interface;
  IXMLIsfridayType = interface;
  IXMLIssaturdayType = interface;
  IXMLPaycalendarperiodsType = interface;
  IXMLPeriodType = interface;
  IXMLStartdateType = interface;
  IXMLEnddateType = interface;
  IXMLPaydateType = interface;

{ IXMLPayCalendarsType }

  IXMLPayCalendarsType = interface(IXMLNodeCollection)
    ['{DED0B708-6CC7-43F0-ADDF-A5525E48BF87}']
    { Property Accessors }
    function Get_Paycalendar(Index: Integer): IXMLPaycalendarType;
    { Methods & Properties }
    function Add: IXMLPaycalendarType;
    function Insert(const Index: Integer): IXMLPaycalendarType;
    property Paycalendar[Index: Integer]: IXMLPaycalendarType read Get_Paycalendar; default;
  end;

{ IXMLPaycalendarType }

  IXMLPaycalendarType = interface(IXMLNode)
    ['{0E28EE77-4EBA-47A2-9A8E-4157DF768A12}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Paycalendarid: WideString;
    function Get_Description: WideString;
    function Get_Periodtwostart: Integer;
    function Get_Paycalendartype: IXMLPaycalendartypeType;
    function Get_Payweekday: IXMLPayweekdayType;
    function Get_Paycalendarstartday: IXMLPaycalendarstartdayType;
    function Get_Yearstart: IXMLYearstartType;
    function Get_Issunday: IXMLIssundayType;
    function Get_Ismonday: IXMLIsmondayType;
    function Get_Istuesday: IXMLIstuesdayType;
    function Get_Iswednesday: IXMLIswednesdayType;
    function Get_Isthursday: IXMLIsthursdayType;
    function Get_Isfriday: IXMLIsfridayType;
    function Get_Issaturday: IXMLIssaturdayType;
    function Get_Offsetdays: Integer;
    function Get_Paycalendarperiods: IXMLPaycalendarperiodsType;
    procedure Set_Id(Value: WideString);
    procedure Set_Paycalendarid(Value: WideString);
    procedure Set_Description(Value: WideString);
    procedure Set_Periodtwostart(Value: Integer);
    procedure Set_Offsetdays(Value: Integer);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Paycalendarid: WideString read Get_Paycalendarid write Set_Paycalendarid;
    property Description: WideString read Get_Description write Set_Description;
    property Periodtwostart: Integer read Get_Periodtwostart write Set_Periodtwostart;
    property Paycalendartype: IXMLPaycalendartypeType read Get_Paycalendartype;
    property Payweekday: IXMLPayweekdayType read Get_Payweekday;
    property Paycalendarstartday: IXMLPaycalendarstartdayType read Get_Paycalendarstartday;
    property Yearstart: IXMLYearstartType read Get_Yearstart;
    property Issunday: IXMLIssundayType read Get_Issunday;
    property Ismonday: IXMLIsmondayType read Get_Ismonday;
    property Istuesday: IXMLIstuesdayType read Get_Istuesday;
    property Iswednesday: IXMLIswednesdayType read Get_Iswednesday;
    property Isthursday: IXMLIsthursdayType read Get_Isthursday;
    property Isfriday: IXMLIsfridayType read Get_Isfriday;
    property Issaturday: IXMLIssaturdayType read Get_Issaturday;
    property Offsetdays: Integer read Get_Offsetdays write Set_Offsetdays;
    property Paycalendarperiods: IXMLPaycalendarperiodsType read Get_Paycalendarperiods;
  end;

{ IXMLPaycalendartypeType }

  IXMLPaycalendartypeType = interface(IXMLNode)
    ['{81A099C7-11EC-4E46-97D0-9FDF78F1F0CB}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPayweekdayType }

  IXMLPayweekdayType = interface(IXMLNode)
    ['{C34B3F16-7235-46FD-8555-D99C06CFDED8}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPaycalendarstartdayType }

  IXMLPaycalendarstartdayType = interface(IXMLNode)
    ['{5308ED19-BC93-45B7-B8A3-4DE575510BEB}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLYearstartType }

  IXMLYearstartType = interface(IXMLNode)
    ['{9E957959-5F55-4238-BD91-429919563F88}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLIssundayType }

  IXMLIssundayType = interface(IXMLNode)
    ['{58EF5CDB-7370-43C3-8A2D-63D6175A8DFD}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIsmondayType }

  IXMLIsmondayType = interface(IXMLNode)
    ['{5FB25B4E-EA2C-41B5-9EBB-0B96643E8AEC}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIstuesdayType }

  IXMLIstuesdayType = interface(IXMLNode)
    ['{C4986815-F58A-45E7-A106-86B85411C90C}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIswednesdayType }

  IXMLIswednesdayType = interface(IXMLNode)
    ['{7AF6A669-B46C-4487-A8E5-9915B821B97C}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIsthursdayType }

  IXMLIsthursdayType = interface(IXMLNode)
    ['{74389ABD-3450-4BE2-B80A-DB0B8779B561}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIsfridayType }

  IXMLIsfridayType = interface(IXMLNode)
    ['{65BF7240-80AE-43DE-BF87-F58E568A70BB}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLIssaturdayType }

  IXMLIssaturdayType = interface(IXMLNode)
    ['{82D24A3E-3BB5-4DE2-8D97-0F7BC0AE1777}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPaycalendarperiodsType }

  IXMLPaycalendarperiodsType = interface(IXMLNodeCollection)
    ['{989891B3-353B-4766-B7D7-576C03EBF445}']
    { Property Accessors }
    function Get_Year: Integer;
    function Get_Period(Index: Integer): IXMLPeriodType;
    procedure Set_Year(Value: Integer);
    { Methods & Properties }
    function Add: IXMLPeriodType;
    function Insert(const Index: Integer): IXMLPeriodType;
    property Year: Integer read Get_Year write Set_Year;
    property Period[Index: Integer]: IXMLPeriodType read Get_Period; default;
  end;

{ IXMLPeriodType }

  IXMLPeriodType = interface(IXMLNode)
    ['{90650754-E898-4784-BE80-6492FBC5DEB4}']
    { Property Accessors }
    function Get_Startdate: IXMLStartdateType;
    function Get_Enddate: IXMLEnddateType;
    function Get_Paydate: IXMLPaydateType;
    { Methods & Properties }
    property Startdate: IXMLStartdateType read Get_Startdate;
    property Enddate: IXMLEnddateType read Get_Enddate;
    property Paydate: IXMLPaydateType read Get_Paydate;
  end;

{ IXMLStartdateType }

  IXMLStartdateType = interface(IXMLNode)
    ['{D414448B-CB68-4FE4-AF7B-D0822BD5BCB6}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLEnddateType }

  IXMLEnddateType = interface(IXMLNode)
    ['{590B68FC-43E0-4FBB-9184-24DF9D78287C}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLPaydateType }

  IXMLPaydateType = interface(IXMLNode)
    ['{D673F3C3-F1BF-496F-B50D-F1B39B8F0DD6}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ Forward Decls }

  TXMLPayCalendarsType = class;
  TXMLPaycalendarType = class;
  TXMLPaycalendartypeType = class;
  TXMLPayweekdayType = class;
  TXMLPaycalendarstartdayType = class;
  TXMLYearstartType = class;
  TXMLIssundayType = class;
  TXMLIsmondayType = class;
  TXMLIstuesdayType = class;
  TXMLIswednesdayType = class;
  TXMLIsthursdayType = class;
  TXMLIsfridayType = class;
  TXMLIssaturdayType = class;
  TXMLPaycalendarperiodsType = class;
  TXMLPeriodType = class;
  TXMLStartdateType = class;
  TXMLEnddateType = class;
  TXMLPaydateType = class;

{ TXMLPayCalendarsType }

  TXMLPayCalendarsType = class(TXMLNodeCollection, IXMLPayCalendarsType)
  protected
    { IXMLPayCalendarsType }
    function Get_Paycalendar(Index: Integer): IXMLPaycalendarType;
    function Add: IXMLPaycalendarType;
    function Insert(const Index: Integer): IXMLPaycalendarType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPaycalendarType }

  TXMLPaycalendarType = class(TXMLNode, IXMLPaycalendarType)
  protected
    { IXMLPaycalendarType }
    function Get_Id: WideString;
    function Get_Paycalendarid: WideString;
    function Get_Description: WideString;
    function Get_Periodtwostart: Integer;
    function Get_Paycalendartype: IXMLPaycalendartypeType;
    function Get_Payweekday: IXMLPayweekdayType;
    function Get_Paycalendarstartday: IXMLPaycalendarstartdayType;
    function Get_Yearstart: IXMLYearstartType;
    function Get_Issunday: IXMLIssundayType;
    function Get_Ismonday: IXMLIsmondayType;
    function Get_Istuesday: IXMLIstuesdayType;
    function Get_Iswednesday: IXMLIswednesdayType;
    function Get_Isthursday: IXMLIsthursdayType;
    function Get_Isfriday: IXMLIsfridayType;
    function Get_Issaturday: IXMLIssaturdayType;
    function Get_Offsetdays: Integer;
    function Get_Paycalendarperiods: IXMLPaycalendarperiodsType;
    procedure Set_Id(Value: WideString);
    procedure Set_Paycalendarid(Value: WideString);
    procedure Set_Description(Value: WideString);
    procedure Set_Periodtwostart(Value: Integer);
    procedure Set_Offsetdays(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPaycalendartypeType }

  TXMLPaycalendartypeType = class(TXMLNode, IXMLPaycalendartypeType)
  protected
    { IXMLPaycalendartypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPayweekdayType }

  TXMLPayweekdayType = class(TXMLNode, IXMLPayweekdayType)
  protected
    { IXMLPayweekdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPaycalendarstartdayType }

  TXMLPaycalendarstartdayType = class(TXMLNode, IXMLPaycalendarstartdayType)
  protected
    { IXMLPaycalendarstartdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLYearstartType }

  TXMLYearstartType = class(TXMLNode, IXMLYearstartType)
  protected
    { IXMLYearstartType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLIssundayType }

  TXMLIssundayType = class(TXMLNode, IXMLIssundayType)
  protected
    { IXMLIssundayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIsmondayType }

  TXMLIsmondayType = class(TXMLNode, IXMLIsmondayType)
  protected
    { IXMLIsmondayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIstuesdayType }

  TXMLIstuesdayType = class(TXMLNode, IXMLIstuesdayType)
  protected
    { IXMLIstuesdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIswednesdayType }

  TXMLIswednesdayType = class(TXMLNode, IXMLIswednesdayType)
  protected
    { IXMLIswednesdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIsthursdayType }

  TXMLIsthursdayType = class(TXMLNode, IXMLIsthursdayType)
  protected
    { IXMLIsthursdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIsfridayType }

  TXMLIsfridayType = class(TXMLNode, IXMLIsfridayType)
  protected
    { IXMLIsfridayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLIssaturdayType }

  TXMLIssaturdayType = class(TXMLNode, IXMLIssaturdayType)
  protected
    { IXMLIssaturdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPaycalendarperiodsType }

  TXMLPaycalendarperiodsType = class(TXMLNodeCollection, IXMLPaycalendarperiodsType)
  protected
    { IXMLPaycalendarperiodsType }
    function Get_Year: Integer;
    function Get_Period(Index: Integer): IXMLPeriodType;
    procedure Set_Year(Value: Integer);
    function Add: IXMLPeriodType;
    function Insert(const Index: Integer): IXMLPeriodType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPeriodType }

  TXMLPeriodType = class(TXMLNode, IXMLPeriodType)
  protected
    { IXMLPeriodType }
    function Get_Startdate: IXMLStartdateType;
    function Get_Enddate: IXMLEnddateType;
    function Get_Paydate: IXMLPaydateType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLStartdateType }

  TXMLStartdateType = class(TXMLNode, IXMLStartdateType)
  protected
    { IXMLStartdateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLEnddateType }

  TXMLEnddateType = class(TXMLNode, IXMLEnddateType)
  protected
    { IXMLEnddateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLPaydateType }

  TXMLPaydateType = class(TXMLNode, IXMLPaydateType)
  protected
    { IXMLPaydateType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ Global Functions }

function GetpayCalendars(Doc: IXMLDocument): IXMLPayCalendarsType;
function LoadpayCalendars(const FileName: WideString): IXMLPayCalendarsType;
function NewpayCalendars: IXMLPayCalendarsType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetpayCalendars(Doc: IXMLDocument): IXMLPayCalendarsType;
begin
  Result := Doc.GetDocBinding('payCalendars', TXMLPayCalendarsType, TargetNamespace) as IXMLPayCalendarsType;
end;

function LoadpayCalendars(const FileName: WideString): IXMLPayCalendarsType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('payCalendars', TXMLPayCalendarsType, TargetNamespace) as IXMLPayCalendarsType;
end;

function NewpayCalendars: IXMLPayCalendarsType;
begin
  Result := NewXMLDocument.GetDocBinding('payCalendars', TXMLPayCalendarsType, TargetNamespace) as IXMLPayCalendarsType;
end;

{ TXMLPayCalendarsType }

procedure TXMLPayCalendarsType.AfterConstruction;
begin
  RegisterChildNode('paycalendar', TXMLPaycalendarType);
  ItemTag := 'paycalendar';
  ItemInterface := IXMLPaycalendarType;
  inherited;
end;

function TXMLPayCalendarsType.Get_Paycalendar(Index: Integer): IXMLPaycalendarType;
begin
  Result := List[Index] as IXMLPaycalendarType;
end;

function TXMLPayCalendarsType.Add: IXMLPaycalendarType;
begin
  Result := AddItem(-1) as IXMLPaycalendarType;
end;

function TXMLPayCalendarsType.Insert(const Index: Integer): IXMLPaycalendarType;
begin
  Result := AddItem(Index) as IXMLPaycalendarType;
end;

{ TXMLPaycalendarType }

procedure TXMLPaycalendarType.AfterConstruction;
begin
  RegisterChildNode('paycalendartype', TXMLPaycalendartypeType);
  RegisterChildNode('payweekday', TXMLPayweekdayType);
  RegisterChildNode('paycalendarstartday', TXMLPaycalendarstartdayType);
  RegisterChildNode('yearstart', TXMLYearstartType);
  RegisterChildNode('issunday', TXMLIssundayType);
  RegisterChildNode('ismonday', TXMLIsmondayType);
  RegisterChildNode('istuesday', TXMLIstuesdayType);
  RegisterChildNode('iswednesday', TXMLIswednesdayType);
  RegisterChildNode('isthursday', TXMLIsthursdayType);
  RegisterChildNode('isfriday', TXMLIsfridayType);
  RegisterChildNode('issaturday', TXMLIssaturdayType);
  RegisterChildNode('paycalendarperiods', TXMLPaycalendarperiodsType);
  inherited;
end;

function TXMLPaycalendarType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLPaycalendarType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLPaycalendarType.Get_Paycalendarid: WideString;
begin
  Result := ChildNodes['paycalendarid'].Text;
end;

procedure TXMLPaycalendarType.Set_Paycalendarid(Value: WideString);
begin
  ChildNodes['paycalendarid'].NodeValue := Value;
end;

function TXMLPaycalendarType.Get_Description: WideString;
begin
  Result := ChildNodes['description'].Text;
end;

procedure TXMLPaycalendarType.Set_Description(Value: WideString);
begin
  ChildNodes['description'].NodeValue := Value;
end;

function TXMLPaycalendarType.Get_Periodtwostart: Integer;
begin
  Result := ChildNodes['periodtwostart'].NodeValue;
end;

procedure TXMLPaycalendarType.Set_Periodtwostart(Value: Integer);
begin
  ChildNodes['periodtwostart'].NodeValue := Value;
end;

function TXMLPaycalendarType.Get_Paycalendartype: IXMLPaycalendartypeType;
begin
  Result := ChildNodes['paycalendartype'] as IXMLPaycalendartypeType;
end;

function TXMLPaycalendarType.Get_Payweekday: IXMLPayweekdayType;
begin
  Result := ChildNodes['payweekday'] as IXMLPayweekdayType;
end;

function TXMLPaycalendarType.Get_Paycalendarstartday: IXMLPaycalendarstartdayType;
begin
  Result := ChildNodes['paycalendarstartday'] as IXMLPaycalendarstartdayType;
end;

function TXMLPaycalendarType.Get_Yearstart: IXMLYearstartType;
begin
  Result := ChildNodes['yearstart'] as IXMLYearstartType;
end;

function TXMLPaycalendarType.Get_Issunday: IXMLIssundayType;
begin
  Result := ChildNodes['issunday'] as IXMLIssundayType;
end;

function TXMLPaycalendarType.Get_Ismonday: IXMLIsmondayType;
begin
  Result := ChildNodes['ismonday'] as IXMLIsmondayType;
end;

function TXMLPaycalendarType.Get_Istuesday: IXMLIstuesdayType;
begin
  Result := ChildNodes['istuesday'] as IXMLIstuesdayType;
end;

function TXMLPaycalendarType.Get_Iswednesday: IXMLIswednesdayType;
begin
  Result := ChildNodes['iswednesday'] as IXMLIswednesdayType;
end;

function TXMLPaycalendarType.Get_Isthursday: IXMLIsthursdayType;
begin
  Result := ChildNodes['isthursday'] as IXMLIsthursdayType;
end;

function TXMLPaycalendarType.Get_Isfriday: IXMLIsfridayType;
begin
  Result := ChildNodes['isfriday'] as IXMLIsfridayType;
end;

function TXMLPaycalendarType.Get_Issaturday: IXMLIssaturdayType;
begin
  Result := ChildNodes['issaturday'] as IXMLIssaturdayType;
end;

function TXMLPaycalendarType.Get_Offsetdays: Integer;
begin
  Result := ChildNodes['offsetdays'].NodeValue;
end;

procedure TXMLPaycalendarType.Set_Offsetdays(Value: Integer);
begin
  ChildNodes['offsetdays'].NodeValue := Value;
end;

function TXMLPaycalendarType.Get_Paycalendarperiods: IXMLPaycalendarperiodsType;
begin
  Result := ChildNodes['paycalendarperiods'] as IXMLPaycalendarperiodsType;
end;

{ TXMLPaycalendartypeType }

function TXMLPaycalendartypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPaycalendartypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPayweekdayType }

function TXMLPayweekdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPayweekdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPaycalendarstartdayType }

function TXMLPaycalendarstartdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPaycalendarstartdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLYearstartType }

function TXMLYearstartType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLYearstartType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLYearstartType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLYearstartType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLIssundayType }

function TXMLIssundayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIssundayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIsmondayType }

function TXMLIsmondayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsmondayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIstuesdayType }

function TXMLIstuesdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIstuesdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIswednesdayType }

function TXMLIswednesdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIswednesdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIsthursdayType }

function TXMLIsthursdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsthursdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIsfridayType }

function TXMLIsfridayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsfridayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLIssaturdayType }

function TXMLIssaturdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIssaturdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPaycalendarperiodsType }

procedure TXMLPaycalendarperiodsType.AfterConstruction;
begin
  RegisterChildNode('period', TXMLPeriodType);
  ItemTag := 'period';
  ItemInterface := IXMLPeriodType;
  inherited;
end;

function TXMLPaycalendarperiodsType.Get_Year: Integer;
begin
  Result := AttributeNodes['year'].NodeValue;
end;

procedure TXMLPaycalendarperiodsType.Set_Year(Value: Integer);
begin
  SetAttribute('year', Value);
end;

function TXMLPaycalendarperiodsType.Get_Period(Index: Integer): IXMLPeriodType;
begin
  Result := List[Index] as IXMLPeriodType;
end;

function TXMLPaycalendarperiodsType.Add: IXMLPeriodType;
begin
  Result := AddItem(-1) as IXMLPeriodType;
end;

function TXMLPaycalendarperiodsType.Insert(const Index: Integer): IXMLPeriodType;
begin
  Result := AddItem(Index) as IXMLPeriodType;
end;

{ TXMLPeriodType }

procedure TXMLPeriodType.AfterConstruction;
begin
  RegisterChildNode('startdate', TXMLStartdateType);
  RegisterChildNode('enddate', TXMLEnddateType);
  RegisterChildNode('paydate', TXMLPaydateType);
  inherited;
end;

function TXMLPeriodType.Get_Startdate: IXMLStartdateType;
begin
  Result := ChildNodes['startdate'] as IXMLStartdateType;
end;

function TXMLPeriodType.Get_Enddate: IXMLEnddateType;
begin
  Result := ChildNodes['enddate'] as IXMLEnddateType;
end;

function TXMLPeriodType.Get_Paydate: IXMLPaydateType;
begin
  Result := ChildNodes['paydate'] as IXMLPaydateType;
end;

{ TXMLStartdateType }

function TXMLStartdateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLStartdateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLStartdateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLStartdateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLEnddateType }

function TXMLEnddateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLEnddateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLEnddateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLEnddateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLPaydateType }

function TXMLPaydateType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLPaydateType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLPaydateType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLPaydateType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

end.