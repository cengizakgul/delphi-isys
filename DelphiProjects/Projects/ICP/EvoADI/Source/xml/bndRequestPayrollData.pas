
{******************************************************************************}
{                                                                              }
{                               XML Data Binding                               }
{                                                                              }
{         Generated on: 8/15/2009 4:44:06 PM                                   }
{       Generated from: E:\job\IS\integration\ADI\xml\RequestPayrollData.xml   }
{   Settings stored in: E:\job\IS\integration\ADI\xml\RequestPayrollData.xdb   }
{                                                                              }
{******************************************************************************}

unit bndRequestPayrollData;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLResponseType = interface;
  IXMLPayrollsegmentsType = interface;
  IXMLPayrollsegmentType = interface;
  IXMLEmployeekeyType = interface;
  IXMLStartdatetimeType = interface;
  IXMLEnddatetimeType = interface;
  IXMLCompanykeyType = interface;
  IXMLLocationkeyType = interface;
  IXMLDivisionkeyType = interface;
  IXMLDepartmentkeyType = interface;
  IXMLPositionkeyType = interface;
  IXMLPaytypekeyType = interface;

{ IXMLResponseType }

  IXMLResponseType = interface(IXMLNode)
    ['{68F80E15-448D-46EE-B70A-0F3A0084C2B9}']
    { Property Accessors }
    function Get_Payrollsegments: IXMLPayrollsegmentsType;
    { Methods & Properties }
    property Payrollsegments: IXMLPayrollsegmentsType read Get_Payrollsegments;
  end;

{ IXMLPayrollsegmentsType }

  IXMLPayrollsegmentsType = interface(IXMLNodeCollection)
    ['{3F205F6D-6AE8-4080-8A6D-44E90FE1A39A}']
    { Property Accessors }
    function Get_Payrollsegment(Index: Integer): IXMLPayrollsegmentType;
    { Methods & Properties }
    function Add: IXMLPayrollsegmentType;
    function Insert(const Index: Integer): IXMLPayrollsegmentType;
    property Payrollsegment[Index: Integer]: IXMLPayrollsegmentType read Get_Payrollsegment; default;
  end;

{ IXMLPayrollsegmentType }

  IXMLPayrollsegmentType = interface(IXMLNode)
    ['{5A086CA2-6AFA-414C-9A26-868B7C6C7D21}']
    { Property Accessors }
    function Get_Reportdate: WideString;
    function Get_Utcdate: WideString;
    function Get_Payrollsegmentkey: WideString;
    function Get_Employeekey: IXMLEmployeekeyType;
    function Get_Startdatetime: IXMLStartdatetimeType;
    function Get_Enddatetime: IXMLEnddatetimeType;
    function Get_Hours: Double;
    function Get_Amount: Double;
    function Get_Payrate: Double;
    function Get_Basepayrate: Double;
    function Get_Companykey: IXMLCompanykeyType;
    function Get_Locationkey: IXMLLocationkeyType;
    function Get_Divisionkey: IXMLDivisionkeyType;
    function Get_Departmentkey: IXMLDepartmentkeyType;
    function Get_Positionkey: IXMLPositionkeyType;
    function Get_Comments: WideString;
    function Get_Paytypekey: IXMLPaytypekeyType;
    procedure Set_Reportdate(Value: WideString);
    procedure Set_Utcdate(Value: WideString);
    procedure Set_Payrollsegmentkey(Value: WideString);
    procedure Set_Hours(Value: Double);
    procedure Set_Amount(Value: Double);
    procedure Set_Payrate(Value: Double);
    procedure Set_Basepayrate(Value: Double);
    procedure Set_Comments(Value: WideString);
    { Methods & Properties }
    property Reportdate: WideString read Get_Reportdate write Set_Reportdate;
    property Utcdate: WideString read Get_Utcdate write Set_Utcdate;
    property Payrollsegmentkey: WideString read Get_Payrollsegmentkey write Set_Payrollsegmentkey;
    property Employeekey: IXMLEmployeekeyType read Get_Employeekey;
    property Startdatetime: IXMLStartdatetimeType read Get_Startdatetime;
    property Enddatetime: IXMLEnddatetimeType read Get_Enddatetime;
    property Hours: Double read Get_Hours write Set_Hours;
    property Amount: Double read Get_Amount write Set_Amount;
    property Payrate: Double read Get_Payrate write Set_Payrate;
    property Basepayrate: Double read Get_Basepayrate write Set_Basepayrate;
    property Companykey: IXMLCompanykeyType read Get_Companykey;
    property Locationkey: IXMLLocationkeyType read Get_Locationkey;
    property Divisionkey: IXMLDivisionkeyType read Get_Divisionkey;
    property Departmentkey: IXMLDepartmentkeyType read Get_Departmentkey;
    property Positionkey: IXMLPositionkeyType read Get_Positionkey;
    property Comments: WideString read Get_Comments write Set_Comments;
    property Paytypekey: IXMLPaytypekeyType read Get_Paytypekey;
  end;

{ IXMLEmployeekeyType }

  IXMLEmployeekeyType = interface(IXMLNode)
    ['{F076306F-78E3-40D9-B46E-EA8CBECFDA9C}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLStartdatetimeType }

  IXMLStartdatetimeType = interface(IXMLNode)
    ['{1DE5F419-DDDF-4E99-8077-BC13F33024E9}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLEnddatetimeType }

  IXMLEnddatetimeType = interface(IXMLNode)
    ['{9768F4C7-9744-4D46-B182-F75C530FEEDB}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLCompanykeyType }

  IXMLCompanykeyType = interface(IXMLNode)
    ['{DCA7F378-D603-4243-8F31-9AD5ACE6D3FD}']
    { Property Accessors }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
  end;

{ IXMLLocationkeyType }

  IXMLLocationkeyType = interface(IXMLNode)
    ['{5C0BBF25-D954-4B24-A835-DE5CC007C20D}']
    { Property Accessors }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
  end;

{ IXMLDivisionkeyType }

  IXMLDivisionkeyType = interface(IXMLNode)
    ['{3DB8004C-E23A-41CF-B3F9-7B501ABE0766}']
    { Property Accessors }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
  end;

{ IXMLDepartmentkeyType }

  IXMLDepartmentkeyType = interface(IXMLNode)
    ['{6CFFE006-76F5-4E7F-99F9-73974583453B}']
    { Property Accessors }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
  end;

{ IXMLPositionkeyType }

  IXMLPositionkeyType = interface(IXMLNode)
    ['{1547B0A6-44C2-424D-86F8-7B75F35ED0C7}']
    { Property Accessors }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
  end;

{ IXMLPaytypekeyType }

  IXMLPaytypekeyType = interface(IXMLNode)
    ['{A1B9B028-801E-4FCA-81B8-FE5BF86C714D}']
    { Property Accessors }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
  end;

{ Forward Decls }

  TXMLResponseType = class;
  TXMLPayrollsegmentsType = class;
  TXMLPayrollsegmentType = class;
  TXMLEmployeekeyType = class;
  TXMLStartdatetimeType = class;
  TXMLEnddatetimeType = class;
  TXMLCompanykeyType = class;
  TXMLLocationkeyType = class;
  TXMLDivisionkeyType = class;
  TXMLDepartmentkeyType = class;
  TXMLPositionkeyType = class;
  TXMLPaytypekeyType = class;

{ TXMLResponseType }

  TXMLResponseType = class(TXMLNode, IXMLResponseType)
  protected
    { IXMLResponseType }
    function Get_Payrollsegments: IXMLPayrollsegmentsType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPayrollsegmentsType }

  TXMLPayrollsegmentsType = class(TXMLNodeCollection, IXMLPayrollsegmentsType)
  protected
    { IXMLPayrollsegmentsType }
    function Get_Payrollsegment(Index: Integer): IXMLPayrollsegmentType;
    function Add: IXMLPayrollsegmentType;
    function Insert(const Index: Integer): IXMLPayrollsegmentType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPayrollsegmentType }

  TXMLPayrollsegmentType = class(TXMLNode, IXMLPayrollsegmentType)
  protected
    { IXMLPayrollsegmentType }
    function Get_Reportdate: WideString;
    function Get_Utcdate: WideString;
    function Get_Payrollsegmentkey: WideString;
    function Get_Employeekey: IXMLEmployeekeyType;
    function Get_Startdatetime: IXMLStartdatetimeType;
    function Get_Enddatetime: IXMLEnddatetimeType;
    function Get_Hours: Double;
    function Get_Amount: Double;
    function Get_Payrate: Double;
    function Get_Basepayrate: Double;
    function Get_Companykey: IXMLCompanykeyType;
    function Get_Locationkey: IXMLLocationkeyType;
    function Get_Divisionkey: IXMLDivisionkeyType;
    function Get_Departmentkey: IXMLDepartmentkeyType;
    function Get_Positionkey: IXMLPositionkeyType;
    function Get_Comments: WideString;
    function Get_Paytypekey: IXMLPaytypekeyType;
    procedure Set_Reportdate(Value: WideString);
    procedure Set_Utcdate(Value: WideString);
    procedure Set_Payrollsegmentkey(Value: WideString);
    procedure Set_Hours(Value: Double);
    procedure Set_Amount(Value: Double);
    procedure Set_Payrate(Value: Double);
    procedure Set_Basepayrate(Value: Double);
    procedure Set_Comments(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmployeekeyType }

  TXMLEmployeekeyType = class(TXMLNode, IXMLEmployeekeyType)
  protected
    { IXMLEmployeekeyType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
  end;

{ TXMLStartdatetimeType }

  TXMLStartdatetimeType = class(TXMLNode, IXMLStartdatetimeType)
  protected
    { IXMLStartdatetimeType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLEnddatetimeType }

  TXMLEnddatetimeType = class(TXMLNode, IXMLEnddatetimeType)
  protected
    { IXMLEnddatetimeType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLCompanykeyType }

  TXMLCompanykeyType = class(TXMLNode, IXMLCompanykeyType)
  protected
    { IXMLCompanykeyType }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
  end;

{ TXMLLocationkeyType }

  TXMLLocationkeyType = class(TXMLNode, IXMLLocationkeyType)
  protected
    { IXMLLocationkeyType }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
  end;

{ TXMLDivisionkeyType }

  TXMLDivisionkeyType = class(TXMLNode, IXMLDivisionkeyType)
  protected
    { IXMLDivisionkeyType }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
  end;

{ TXMLDepartmentkeyType }

  TXMLDepartmentkeyType = class(TXMLNode, IXMLDepartmentkeyType)
  protected
    { IXMLDepartmentkeyType }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
  end;

{ TXMLPositionkeyType }

  TXMLPositionkeyType = class(TXMLNode, IXMLPositionkeyType)
  protected
    { IXMLPositionkeyType }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
  end;

{ TXMLPaytypekeyType }

  TXMLPaytypekeyType = class(TXMLNode, IXMLPaytypekeyType)
  protected
    { IXMLPaytypekeyType }
    function Get_Id: WideString;
    procedure Set_Id(Value: WideString);
  end;

{ Global Functions }

function Getresponse(Doc: IXMLDocument): IXMLResponseType;
function Loadresponse(const FileName: WideString): IXMLResponseType;
function Newresponse: IXMLResponseType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getresponse(Doc: IXMLDocument): IXMLResponseType;
begin
  Result := Doc.GetDocBinding('response', TXMLResponseType, TargetNamespace) as IXMLResponseType;
end;

function Loadresponse(const FileName: WideString): IXMLResponseType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('response', TXMLResponseType, TargetNamespace) as IXMLResponseType;
end;

function Newresponse: IXMLResponseType;
begin
  Result := NewXMLDocument.GetDocBinding('response', TXMLResponseType, TargetNamespace) as IXMLResponseType;
end;

{ TXMLResponseType }

procedure TXMLResponseType.AfterConstruction;
begin
  RegisterChildNode('payrollsegments', TXMLPayrollsegmentsType);
  inherited;
end;

function TXMLResponseType.Get_Payrollsegments: IXMLPayrollsegmentsType;
begin
  Result := ChildNodes['payrollsegments'] as IXMLPayrollsegmentsType;
end;

{ TXMLPayrollsegmentsType }

procedure TXMLPayrollsegmentsType.AfterConstruction;
begin
  RegisterChildNode('payrollsegment', TXMLPayrollsegmentType);
  ItemTag := 'payrollsegment';
  ItemInterface := IXMLPayrollsegmentType;
  inherited;
end;

function TXMLPayrollsegmentsType.Get_Payrollsegment(Index: Integer): IXMLPayrollsegmentType;
begin
  Result := List[Index] as IXMLPayrollsegmentType;
end;

function TXMLPayrollsegmentsType.Add: IXMLPayrollsegmentType;
begin
  Result := AddItem(-1) as IXMLPayrollsegmentType;
end;

function TXMLPayrollsegmentsType.Insert(const Index: Integer): IXMLPayrollsegmentType;
begin
  Result := AddItem(Index) as IXMLPayrollsegmentType;
end;

{ TXMLPayrollsegmentType }

procedure TXMLPayrollsegmentType.AfterConstruction;
begin
  RegisterChildNode('employeekey', TXMLEmployeekeyType);
  RegisterChildNode('startdatetime', TXMLStartdatetimeType);
  RegisterChildNode('enddatetime', TXMLEnddatetimeType);
  RegisterChildNode('companykey', TXMLCompanykeyType);
  RegisterChildNode('locationkey', TXMLLocationkeyType);
  RegisterChildNode('divisionkey', TXMLDivisionkeyType);
  RegisterChildNode('departmentkey', TXMLDepartmentkeyType);
  RegisterChildNode('positionkey', TXMLPositionkeyType);
  RegisterChildNode('paytypekey', TXMLPaytypekeyType);
  inherited;
end;

function TXMLPayrollsegmentType.Get_Reportdate: WideString;
begin
  Result := AttributeNodes['reportdate'].Text;
end;

procedure TXMLPayrollsegmentType.Set_Reportdate(Value: WideString);
begin
  SetAttribute('reportdate', Value);
end;

function TXMLPayrollsegmentType.Get_Utcdate: WideString;
begin
  Result := AttributeNodes['utcdate'].Text;
end;

procedure TXMLPayrollsegmentType.Set_Utcdate(Value: WideString);
begin
  SetAttribute('utcdate', Value);
end;

function TXMLPayrollsegmentType.Get_Payrollsegmentkey: WideString;
begin
  Result := ChildNodes['payrollsegmentkey'].Text;
end;

procedure TXMLPayrollsegmentType.Set_Payrollsegmentkey(Value: WideString);
begin
  ChildNodes['payrollsegmentkey'].NodeValue := Value;
end;

function TXMLPayrollsegmentType.Get_Employeekey: IXMLEmployeekeyType;
begin
  Result := ChildNodes['employeekey'] as IXMLEmployeekeyType;
end;

function TXMLPayrollsegmentType.Get_Startdatetime: IXMLStartdatetimeType;
begin
  Result := ChildNodes['startdatetime'] as IXMLStartdatetimeType;
end;

function TXMLPayrollsegmentType.Get_Enddatetime: IXMLEnddatetimeType;
begin
  Result := ChildNodes['enddatetime'] as IXMLEnddatetimeType;
end;

function TXMLPayrollsegmentType.Get_Hours: Double;
begin
  Result := ChildNodes['hours'].NodeValue;
end;

procedure TXMLPayrollsegmentType.Set_Hours(Value: Double);
begin
  ChildNodes['hours'].NodeValue := Value;
end;

function TXMLPayrollsegmentType.Get_Amount: Double;
begin
  Result := ChildNodes['amount'].NodeValue;
end;

procedure TXMLPayrollsegmentType.Set_Amount(Value: Double);
begin
  ChildNodes['amount'].NodeValue := Value;
end;

function TXMLPayrollsegmentType.Get_Payrate: Double;
begin
  Result := ChildNodes['payrate'].NodeValue;
end;

procedure TXMLPayrollsegmentType.Set_Payrate(Value: Double);
begin
  ChildNodes['payrate'].NodeValue := Value;
end;

function TXMLPayrollsegmentType.Get_Basepayrate: Double;
begin
  Result := ChildNodes['basepayrate'].NodeValue;
end;

procedure TXMLPayrollsegmentType.Set_Basepayrate(Value: Double);
begin
  ChildNodes['basepayrate'].NodeValue := Value;
end;

function TXMLPayrollsegmentType.Get_Companykey: IXMLCompanykeyType;
begin
  Result := ChildNodes['companykey'] as IXMLCompanykeyType;
end;

function TXMLPayrollsegmentType.Get_Locationkey: IXMLLocationkeyType;
begin
  Result := ChildNodes['locationkey'] as IXMLLocationkeyType;
end;

function TXMLPayrollsegmentType.Get_Divisionkey: IXMLDivisionkeyType;
begin
  Result := ChildNodes['divisionkey'] as IXMLDivisionkeyType;
end;

function TXMLPayrollsegmentType.Get_Departmentkey: IXMLDepartmentkeyType;
begin
  Result := ChildNodes['departmentkey'] as IXMLDepartmentkeyType;
end;

function TXMLPayrollsegmentType.Get_Positionkey: IXMLPositionkeyType;
begin
  Result := ChildNodes['positionkey'] as IXMLPositionkeyType;
end;

function TXMLPayrollsegmentType.Get_Comments: WideString;
begin
  Result := ChildNodes['comments'].Text;
end;

procedure TXMLPayrollsegmentType.Set_Comments(Value: WideString);
begin
  ChildNodes['comments'].NodeValue := Value;
end;

function TXMLPayrollsegmentType.Get_Paytypekey: IXMLPaytypekeyType;
begin
  Result := ChildNodes['paytypekey'] as IXMLPaytypekeyType;
end;

{ TXMLEmployeekeyType }

function TXMLEmployeekeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLEmployeekeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

function TXMLEmployeekeyType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLEmployeekeyType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLStartdatetimeType }

function TXMLStartdatetimeType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLStartdatetimeType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLStartdatetimeType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLStartdatetimeType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLEnddatetimeType }

function TXMLEnddatetimeType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLEnddatetimeType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLEnddatetimeType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLEnddatetimeType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLCompanykeyType }

function TXMLCompanykeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLCompanykeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

{ TXMLLocationkeyType }

function TXMLLocationkeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLLocationkeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

{ TXMLDivisionkeyType }

function TXMLDivisionkeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLDivisionkeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

{ TXMLDepartmentkeyType }

function TXMLDepartmentkeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLDepartmentkeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

{ TXMLPositionkeyType }

function TXMLPositionkeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLPositionkeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

{ TXMLPaytypekeyType }

function TXMLPaytypekeyType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

procedure TXMLPaytypekeyType.Set_Id(Value: WideString);
begin
  SetAttribute('id', Value);
end;

end.