
{**************************************************************************************}
{                                                                                      }
{                                   XML Data Binding                                   }
{                                                                                      }
{         Generated on: 5/28/2010 6:36:51 PM                                           }
{       Generated from: E:\job\IS\integration\EvoADI\xml\RetrieveAllAccrualTypes.xml   }
{                                                                                      }
{**************************************************************************************}

unit bndRetrieveAllAccrualTypes;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLAccrualtypesType = interface;
  IXMLAccrualtypeType = interface;
  IXMLIsactiveType = interface;

{ IXMLAccrualtypesType }

  IXMLAccrualtypesType = interface(IXMLNodeCollection)
    ['{BA3E4510-E229-4B27-93F8-729D138229AC}']
    { Property Accessors }
    function Get_Accrualtype(Index: Integer): IXMLAccrualtypeType;
    { Methods & Properties }
    function Add: IXMLAccrualtypeType;
    function Insert(const Index: Integer): IXMLAccrualtypeType;
    property Accrualtype[Index: Integer]: IXMLAccrualtypeType read Get_Accrualtype; default;
  end;

{ IXMLAccrualtypeType }

  IXMLAccrualtypeType = interface(IXMLNode)
    ['{4C55B75A-E192-4B17-A051-3EB8A98EF73C}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Accrualtypeid: WideString;
    function Get_Name: WideString;
    function Get_Isactive: IXMLIsactiveType;
    procedure Set_Id(Value: WideString);
    procedure Set_Accrualtypeid(Value: WideString);
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Accrualtypeid: WideString read Get_Accrualtypeid write Set_Accrualtypeid;
    property Name: WideString read Get_Name write Set_Name;
    property Isactive: IXMLIsactiveType read Get_Isactive;
  end;

{ IXMLIsactiveType }

  IXMLIsactiveType = interface(IXMLNode)
    ['{BFEED448-D8B5-48EF-BE67-4A65E705B956}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ Forward Decls }

  TXMLAccrualtypesType = class;
  TXMLAccrualtypeType = class;
  TXMLIsactiveType = class;

{ TXMLAccrualtypesType }

  TXMLAccrualtypesType = class(TXMLNodeCollection, IXMLAccrualtypesType)
  protected
    { IXMLAccrualtypesType }
    function Get_Accrualtype(Index: Integer): IXMLAccrualtypeType;
    function Add: IXMLAccrualtypeType;
    function Insert(const Index: Integer): IXMLAccrualtypeType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAccrualtypeType }

  TXMLAccrualtypeType = class(TXMLNode, IXMLAccrualtypeType)
  protected
    { IXMLAccrualtypeType }
    function Get_Id: WideString;
    function Get_Accrualtypeid: WideString;
    function Get_Name: WideString;
    function Get_Isactive: IXMLIsactiveType;
    procedure Set_Id(Value: WideString);
    procedure Set_Accrualtypeid(Value: WideString);
    procedure Set_Name(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIsactiveType }

  TXMLIsactiveType = class(TXMLNode, IXMLIsactiveType)
  protected
    { IXMLIsactiveType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ Global Functions }

function Getaccrualtypes(Doc: IXMLDocument): IXMLAccrualtypesType;
function Loadaccrualtypes(const FileName: WideString): IXMLAccrualtypesType;
function Newaccrualtypes: IXMLAccrualtypesType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getaccrualtypes(Doc: IXMLDocument): IXMLAccrualtypesType;
begin
  Result := Doc.GetDocBinding('accrualtypes', TXMLAccrualtypesType, TargetNamespace) as IXMLAccrualtypesType;
end;

function Loadaccrualtypes(const FileName: WideString): IXMLAccrualtypesType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('accrualtypes', TXMLAccrualtypesType, TargetNamespace) as IXMLAccrualtypesType;
end;

function Newaccrualtypes: IXMLAccrualtypesType;
begin
  Result := NewXMLDocument.GetDocBinding('accrualtypes', TXMLAccrualtypesType, TargetNamespace) as IXMLAccrualtypesType;
end;

{ TXMLAccrualtypesType }

procedure TXMLAccrualtypesType.AfterConstruction;
begin
  RegisterChildNode('accrualtype', TXMLAccrualtypeType);
  ItemTag := 'accrualtype';
  ItemInterface := IXMLAccrualtypeType;
  inherited;
end;

function TXMLAccrualtypesType.Get_Accrualtype(Index: Integer): IXMLAccrualtypeType;
begin
  Result := List[Index] as IXMLAccrualtypeType;
end;

function TXMLAccrualtypesType.Add: IXMLAccrualtypeType;
begin
  Result := AddItem(-1) as IXMLAccrualtypeType;
end;

function TXMLAccrualtypesType.Insert(const Index: Integer): IXMLAccrualtypeType;
begin
  Result := AddItem(Index) as IXMLAccrualtypeType;
end;

{ TXMLAccrualtypeType }

procedure TXMLAccrualtypeType.AfterConstruction;
begin
  RegisterChildNode('isactive', TXMLIsactiveType);
  inherited;
end;

function TXMLAccrualtypeType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLAccrualtypeType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLAccrualtypeType.Get_Accrualtypeid: WideString;
begin
  Result := ChildNodes['accrualtypeid'].Text;
end;

procedure TXMLAccrualtypeType.Set_Accrualtypeid(Value: WideString);
begin
  ChildNodes['accrualtypeid'].NodeValue := Value;
end;

function TXMLAccrualtypeType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLAccrualtypeType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

function TXMLAccrualtypeType.Get_Isactive: IXMLIsactiveType;
begin
  Result := ChildNodes['isactive'] as IXMLIsactiveType;
end;

{ TXMLIsactiveType }

function TXMLIsactiveType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsactiveType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

end.