
{*********************************************************************************************}
{                                                                                             }
{                                      XML Data Binding                                       }
{                                                                                             }
{         Generated on: 1/24/2010 6:32:35 PM                                                  }
{       Generated from: E:\job\IS\integration\EvoADI\xml\RetrieveAllCodes\Output_Code 5.xml   }
{                                                                                             }
{*********************************************************************************************}

unit bndRaceCodes;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRacecodesType = interface;
  IXMLRacecodeType = interface;

{ IXMLRacecodesType }

  IXMLRacecodesType = interface(IXMLNodeCollection)
    ['{F53D9506-65E6-4F23-8C69-A873881B0E76}']
    { Property Accessors }
    function Get_Morerecords: Integer;
    function Get_Racecode(Index: Integer): IXMLRacecodeType;
    procedure Set_Morerecords(Value: Integer);
    { Methods & Properties }
    function Add: IXMLRacecodeType;
    function Insert(const Index: Integer): IXMLRacecodeType;
    property Morerecords: Integer read Get_Morerecords write Set_Morerecords;
    property Racecode[Index: Integer]: IXMLRacecodeType read Get_Racecode; default;
  end;

{ IXMLRacecodeType }

  IXMLRacecodeType = interface(IXMLNode)
    ['{A2B1DC15-ABB7-46B4-B095-73A663A9B1E5}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Code: WideString;
    function Get_Description: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Code(Value: WideString);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Code: WideString read Get_Code write Set_Code;
    property Description: WideString read Get_Description write Set_Description;
  end;

{ Forward Decls }

  TXMLRacecodesType = class;
  TXMLRacecodeType = class;

{ TXMLRacecodesType }

  TXMLRacecodesType = class(TXMLNodeCollection, IXMLRacecodesType)
  protected
    { IXMLRacecodesType }
    function Get_Morerecords: Integer;
    function Get_Racecode(Index: Integer): IXMLRacecodeType;
    procedure Set_Morerecords(Value: Integer);
    function Add: IXMLRacecodeType;
    function Insert(const Index: Integer): IXMLRacecodeType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRacecodeType }

  TXMLRacecodeType = class(TXMLNode, IXMLRacecodeType)
  protected
    { IXMLRacecodeType }
    function Get_Id: WideString;
    function Get_Code: WideString;
    function Get_Description: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Code(Value: WideString);
    procedure Set_Description(Value: WideString);
  end;

{ Global Functions }

function Getracecodes(Doc: IXMLDocument): IXMLRacecodesType;
function Loadracecodes(const FileName: WideString): IXMLRacecodesType;
function Newracecodes: IXMLRacecodesType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getracecodes(Doc: IXMLDocument): IXMLRacecodesType;
begin
  Result := Doc.GetDocBinding('racecodes', TXMLRacecodesType, TargetNamespace) as IXMLRacecodesType;
end;

function Loadracecodes(const FileName: WideString): IXMLRacecodesType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('racecodes', TXMLRacecodesType, TargetNamespace) as IXMLRacecodesType;
end;

function Newracecodes: IXMLRacecodesType;
begin
  Result := NewXMLDocument.GetDocBinding('racecodes', TXMLRacecodesType, TargetNamespace) as IXMLRacecodesType;
end;

{ TXMLRacecodesType }

procedure TXMLRacecodesType.AfterConstruction;
begin
  RegisterChildNode('racecode', TXMLRacecodeType);
  ItemTag := 'racecode';
  ItemInterface := IXMLRacecodeType;
  inherited;
end;

function TXMLRacecodesType.Get_Morerecords: Integer;
begin
  Result := AttributeNodes['morerecords'].NodeValue;
end;

procedure TXMLRacecodesType.Set_Morerecords(Value: Integer);
begin
  SetAttribute('morerecords', Value);
end;

function TXMLRacecodesType.Get_Racecode(Index: Integer): IXMLRacecodeType;
begin
  Result := List[Index] as IXMLRacecodeType;
end;

function TXMLRacecodesType.Add: IXMLRacecodeType;
begin
  Result := AddItem(-1) as IXMLRacecodeType;
end;

function TXMLRacecodesType.Insert(const Index: Integer): IXMLRacecodeType;
begin
  Result := AddItem(Index) as IXMLRacecodeType;
end;

{ TXMLRacecodeType }

function TXMLRacecodeType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLRacecodeType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLRacecodeType.Get_Code: WideString;
begin
  Result := ChildNodes['code'].Text;
end;

procedure TXMLRacecodeType.Set_Code(Value: WideString);
begin
  ChildNodes['code'].NodeValue := Value;
end;

function TXMLRacecodeType.Get_Description: WideString;
begin
  Result := ChildNodes['description'].Text;
end;

procedure TXMLRacecodeType.Set_Description(Value: WideString);
begin
  ChildNodes['description'].NodeValue := Value;
end;

end.