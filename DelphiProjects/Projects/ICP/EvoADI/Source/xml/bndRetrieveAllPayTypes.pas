
{**************************************************************************************}
{                                                                                      }
{                                   XML Data Binding                                   }
{                                                                                      }
{         Generated on: 5/28/2009 10:49:52 PM                                          }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllPayTypes\Output.xml   }
{                                                                                      }
{**************************************************************************************}

unit bndRetrieveAllPayTypes;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLPaytypesType = interface;
  IXMLPaytypeType = interface;
  IXMLPayrollclassType = interface;
  IXMLPaidtimeType = interface;
  IXMLConsideredworkedType = interface;
  IXMLAllocatetowardspremiumType = interface;
  IXMLCreditpremiumType = interface;
  IXMLCredittowardsdailytotalType = interface;
  IXMLCreditdailyguaranteeType = interface;
  IXMLCreditweeklyguaranteeType = interface;
  IXMLCreditaccrualType = interface;
  IXMLDisqualifyaccrualType = interface;
  IXMLAdjustandautoloadType = interface;
  IXMLAllocatetowardsovertimeType = interface;
  IXMLCreditweeklyovertimeType = interface;
  IXMLCreditperiodovertimeType = interface;
  IXMLCreditconsecutiveovertimeType = interface;
  IXMLRequirereasonType = interface;
  IXMLAllowonscheduleType = interface;
  IXMLConsideredabsentType = interface;
  IXMLAllowfortimeoffType = interface;
  IXMLNomiscpayontimecardType = interface;
  IXMLDeductpremiumType = interface;
  IXMLAllowaccrualType = interface;
  IXMLPayratemodificationType = interface;
  IXMLAllowregularhoursType = interface;
  IXMLAllowmischoursType = interface;
  IXMLAllowmiscamountType = interface;
  IXMLAttendancecalendarType = interface;
  IXMLTipscodeType = interface;

{ IXMLPaytypesType }

  IXMLPaytypesType = interface(IXMLNodeCollection)
    ['{06A69FF3-4BB4-4F54-9DA4-0AD7C077F35B}']
    { Property Accessors }
    function Get_Morerecords: Integer;
    function Get_Paytype(Index: Integer): IXMLPaytypeType;
    procedure Set_Morerecords(Value: Integer);
    { Methods & Properties }
    function Add: IXMLPaytypeType;
    function Insert(const Index: Integer): IXMLPaytypeType;
    property Morerecords: Integer read Get_Morerecords write Set_Morerecords;
    property Paytype[Index: Integer]: IXMLPaytypeType read Get_Paytype; default;
  end;

{ IXMLPaytypeType }

  IXMLPaytypeType = interface(IXMLNode)
    ['{33032E81-3FB3-4C52-A084-07C958D5AFC1}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Paytypeid: WideString;
    function Get_Paytypename: WideString;
    function Get_Payrollclass: IXMLPayrollclassType;
    function Get_Paidtime: IXMLPaidtimeType;
    function Get_Consideredworked: IXMLConsideredworkedType;
    function Get_Allocatetowardspremium: IXMLAllocatetowardspremiumType;
    function Get_Creditpremium: IXMLCreditpremiumType;
    function Get_Credittowardsdailytotal: IXMLCredittowardsdailytotalType;
    function Get_Creditdailyguarantee: IXMLCreditdailyguaranteeType;
    function Get_Creditweeklyguarantee: IXMLCreditweeklyguaranteeType;
    function Get_Creditaccrual: IXMLCreditaccrualType;
    function Get_Disqualifyaccrual: IXMLDisqualifyaccrualType;
    function Get_Adjustandautoload: IXMLAdjustandautoloadType;
    function Get_Allocatetowardsovertime: IXMLAllocatetowardsovertimeType;
    function Get_Creditweeklyovertime: IXMLCreditweeklyovertimeType;
    function Get_Creditperiodovertime: IXMLCreditperiodovertimeType;
    function Get_Creditconsecutiveovertime: IXMLCreditconsecutiveovertimeType;
    function Get_Requirereason: IXMLRequirereasonType;
    function Get_Allowonschedule: IXMLAllowonscheduleType;
    function Get_Consideredabsent: IXMLConsideredabsentType;
    function Get_Allowfortimeoff: IXMLAllowfortimeoffType;
    function Get_Nomiscpayontimecard: IXMLNomiscpayontimecardType;
    function Get_Deductpremium: IXMLDeductpremiumType;
    function Get_Allowaccrual: IXMLAllowaccrualType;
    function Get_Hourscode: WideString;
    function Get_Dollarscode: WideString;
    function Get_Reportgroup: WideString;
    function Get_Payratemodification: IXMLPayratemodificationType;
    function Get_Fixedamountaddition: Integer;
    function Get_Percentageaddition: Integer;
    function Get_Allowregularhours: IXMLAllowregularhoursType;
    function Get_Allowmischours: IXMLAllowmischoursType;
    function Get_Allowmiscamount: IXMLAllowmiscamountType;
    function Get_Timeclockid: Integer;
    function Get_Attendancecalendar: IXMLAttendancecalendarType;
    function Get_Useabbreviation: WideString;
    function Get_Tipscode: IXMLTipscodeType;
    procedure Set_Id(Value: WideString);
    procedure Set_Paytypeid(Value: WideString);
    procedure Set_Paytypename(Value: WideString);
    procedure Set_Hourscode(Value: WideString);
    procedure Set_Dollarscode(Value: WideString);
    procedure Set_Reportgroup(Value: WideString);
    procedure Set_Fixedamountaddition(Value: Integer);
    procedure Set_Percentageaddition(Value: Integer);
    procedure Set_Timeclockid(Value: Integer);
    procedure Set_Useabbreviation(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Paytypeid: WideString read Get_Paytypeid write Set_Paytypeid;
    property Paytypename: WideString read Get_Paytypename write Set_Paytypename;
    property Payrollclass: IXMLPayrollclassType read Get_Payrollclass;
    property Paidtime: IXMLPaidtimeType read Get_Paidtime;
    property Consideredworked: IXMLConsideredworkedType read Get_Consideredworked;
    property Allocatetowardspremium: IXMLAllocatetowardspremiumType read Get_Allocatetowardspremium;
    property Creditpremium: IXMLCreditpremiumType read Get_Creditpremium;
    property Credittowardsdailytotal: IXMLCredittowardsdailytotalType read Get_Credittowardsdailytotal;
    property Creditdailyguarantee: IXMLCreditdailyguaranteeType read Get_Creditdailyguarantee;
    property Creditweeklyguarantee: IXMLCreditweeklyguaranteeType read Get_Creditweeklyguarantee;
    property Creditaccrual: IXMLCreditaccrualType read Get_Creditaccrual;
    property Disqualifyaccrual: IXMLDisqualifyaccrualType read Get_Disqualifyaccrual;
    property Adjustandautoload: IXMLAdjustandautoloadType read Get_Adjustandautoload;
    property Allocatetowardsovertime: IXMLAllocatetowardsovertimeType read Get_Allocatetowardsovertime;
    property Creditweeklyovertime: IXMLCreditweeklyovertimeType read Get_Creditweeklyovertime;
    property Creditperiodovertime: IXMLCreditperiodovertimeType read Get_Creditperiodovertime;
    property Creditconsecutiveovertime: IXMLCreditconsecutiveovertimeType read Get_Creditconsecutiveovertime;
    property Requirereason: IXMLRequirereasonType read Get_Requirereason;
    property Allowonschedule: IXMLAllowonscheduleType read Get_Allowonschedule;
    property Consideredabsent: IXMLConsideredabsentType read Get_Consideredabsent;
    property Allowfortimeoff: IXMLAllowfortimeoffType read Get_Allowfortimeoff;
    property Nomiscpayontimecard: IXMLNomiscpayontimecardType read Get_Nomiscpayontimecard;
    property Deductpremium: IXMLDeductpremiumType read Get_Deductpremium;
    property Allowaccrual: IXMLAllowaccrualType read Get_Allowaccrual;
    property Hourscode: WideString read Get_Hourscode write Set_Hourscode;
    property Dollarscode: WideString read Get_Dollarscode write Set_Dollarscode;
    property Reportgroup: WideString read Get_Reportgroup write Set_Reportgroup;
    property Payratemodification: IXMLPayratemodificationType read Get_Payratemodification;
    property Fixedamountaddition: Integer read Get_Fixedamountaddition write Set_Fixedamountaddition;
    property Percentageaddition: Integer read Get_Percentageaddition write Set_Percentageaddition;
    property Allowregularhours: IXMLAllowregularhoursType read Get_Allowregularhours;
    property Allowmischours: IXMLAllowmischoursType read Get_Allowmischours;
    property Allowmiscamount: IXMLAllowmiscamountType read Get_Allowmiscamount;
    property Timeclockid: Integer read Get_Timeclockid write Set_Timeclockid;
    property Attendancecalendar: IXMLAttendancecalendarType read Get_Attendancecalendar;
    property Useabbreviation: WideString read Get_Useabbreviation write Set_Useabbreviation;
    property Tipscode: IXMLTipscodeType read Get_Tipscode;
  end;

{ IXMLPayrollclassType }

  IXMLPayrollclassType = interface(IXMLNode)
    ['{19A7C25F-5370-4C9E-9651-6181A6F260A6}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPaidtimeType }

  IXMLPaidtimeType = interface(IXMLNode)
    ['{626CDED8-7C7B-4672-8FF2-E9DE922509B0}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLConsideredworkedType }

  IXMLConsideredworkedType = interface(IXMLNode)
    ['{BB43B0B3-1479-4BDC-91A5-49AF78741197}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllocatetowardspremiumType }

  IXMLAllocatetowardspremiumType = interface(IXMLNode)
    ['{3A6A85F9-54FE-4130-A6AC-A6A378B2BE48}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditpremiumType }

  IXMLCreditpremiumType = interface(IXMLNode)
    ['{2D3F2B25-47E9-4D53-9B95-BA425FCEA2FB}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCredittowardsdailytotalType }

  IXMLCredittowardsdailytotalType = interface(IXMLNode)
    ['{A81207B2-7312-4D3F-98B5-DD800EA24A09}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditdailyguaranteeType }

  IXMLCreditdailyguaranteeType = interface(IXMLNode)
    ['{644B6536-C3ED-43DB-8422-64CAC3E44378}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditweeklyguaranteeType }

  IXMLCreditweeklyguaranteeType = interface(IXMLNode)
    ['{7556D5B7-FF5B-4965-AF25-608B24A0BF4D}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditaccrualType }

  IXMLCreditaccrualType = interface(IXMLNode)
    ['{EEE0893E-AD13-43DF-9944-2B0A27DB0CF8}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLDisqualifyaccrualType }

  IXMLDisqualifyaccrualType = interface(IXMLNode)
    ['{CD4DE72A-0DC1-4D27-ACB7-FFD9EA574D98}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAdjustandautoloadType }

  IXMLAdjustandautoloadType = interface(IXMLNode)
    ['{1352E4B9-20F9-4740-8AC6-C2B57A9F79F8}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllocatetowardsovertimeType }

  IXMLAllocatetowardsovertimeType = interface(IXMLNode)
    ['{D29FD7A2-0ADB-4423-9991-4DAF82E01A8D}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditweeklyovertimeType }

  IXMLCreditweeklyovertimeType = interface(IXMLNode)
    ['{95226053-CD73-4B05-B88C-51017D34881E}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditperiodovertimeType }

  IXMLCreditperiodovertimeType = interface(IXMLNode)
    ['{AB2D7A66-7CDD-46F9-AAEE-B1E6EBCBA838}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLCreditconsecutiveovertimeType }

  IXMLCreditconsecutiveovertimeType = interface(IXMLNode)
    ['{BCB99B27-FA89-43D8-BB5C-63AA7B42067D}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLRequirereasonType }

  IXMLRequirereasonType = interface(IXMLNode)
    ['{671E1CF8-530C-40D5-A911-72F35DD65ECB}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowonscheduleType }

  IXMLAllowonscheduleType = interface(IXMLNode)
    ['{7328FCD5-9265-4700-9490-BE85EB3B08FF}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLConsideredabsentType }

  IXMLConsideredabsentType = interface(IXMLNode)
    ['{0E7711E6-61EF-40E1-B945-FFDCEBF0EB16}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowfortimeoffType }

  IXMLAllowfortimeoffType = interface(IXMLNode)
    ['{857F1698-1E63-4292-9503-DB4D769447FC}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLNomiscpayontimecardType }

  IXMLNomiscpayontimecardType = interface(IXMLNode)
    ['{12908DCF-00BC-4841-B5AF-7D9219AEE09F}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLDeductpremiumType }

  IXMLDeductpremiumType = interface(IXMLNode)
    ['{F4107448-B0AD-4E7B-AFE0-15B36ADE59AD}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowaccrualType }

  IXMLAllowaccrualType = interface(IXMLNode)
    ['{5910BD8E-74A6-49E9-990B-0E1D83D91846}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPayratemodificationType }

  IXMLPayratemodificationType = interface(IXMLNode)
    ['{9E14B48B-BC5B-4795-A517-1348E0118AEC}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowregularhoursType }

  IXMLAllowregularhoursType = interface(IXMLNode)
    ['{50F8F1F7-143C-4E59-B061-722DD6DCC982}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowmischoursType }

  IXMLAllowmischoursType = interface(IXMLNode)
    ['{03A199A7-B85B-4A0F-9C4D-1201CF461E56}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowmiscamountType }

  IXMLAllowmiscamountType = interface(IXMLNode)
    ['{8D28492F-BC20-43E4-B82D-A5206C0173CC}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAttendancecalendarType }

  IXMLAttendancecalendarType = interface(IXMLNode)
    ['{2DE0EA08-2FDB-4E13-9D4C-33FD86468186}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLTipscodeType }

  IXMLTipscodeType = interface(IXMLNode)
    ['{014DFA01-EDE9-45B7-97D1-A2D000EA3762}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ Forward Decls }

  TXMLPaytypesType = class;
  TXMLPaytypeType = class;
  TXMLPayrollclassType = class;
  TXMLPaidtimeType = class;
  TXMLConsideredworkedType = class;
  TXMLAllocatetowardspremiumType = class;
  TXMLCreditpremiumType = class;
  TXMLCredittowardsdailytotalType = class;
  TXMLCreditdailyguaranteeType = class;
  TXMLCreditweeklyguaranteeType = class;
  TXMLCreditaccrualType = class;
  TXMLDisqualifyaccrualType = class;
  TXMLAdjustandautoloadType = class;
  TXMLAllocatetowardsovertimeType = class;
  TXMLCreditweeklyovertimeType = class;
  TXMLCreditperiodovertimeType = class;
  TXMLCreditconsecutiveovertimeType = class;
  TXMLRequirereasonType = class;
  TXMLAllowonscheduleType = class;
  TXMLConsideredabsentType = class;
  TXMLAllowfortimeoffType = class;
  TXMLNomiscpayontimecardType = class;
  TXMLDeductpremiumType = class;
  TXMLAllowaccrualType = class;
  TXMLPayratemodificationType = class;
  TXMLAllowregularhoursType = class;
  TXMLAllowmischoursType = class;
  TXMLAllowmiscamountType = class;
  TXMLAttendancecalendarType = class;
  TXMLTipscodeType = class;

{ TXMLPaytypesType }

  TXMLPaytypesType = class(TXMLNodeCollection, IXMLPaytypesType)
  protected
    { IXMLPaytypesType }
    function Get_Morerecords: Integer;
    function Get_Paytype(Index: Integer): IXMLPaytypeType;
    procedure Set_Morerecords(Value: Integer);
    function Add: IXMLPaytypeType;
    function Insert(const Index: Integer): IXMLPaytypeType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPaytypeType }

  TXMLPaytypeType = class(TXMLNode, IXMLPaytypeType)
  protected
    { IXMLPaytypeType }
    function Get_Id: WideString;
    function Get_Paytypeid: WideString;
    function Get_Paytypename: WideString;
    function Get_Payrollclass: IXMLPayrollclassType;
    function Get_Paidtime: IXMLPaidtimeType;
    function Get_Consideredworked: IXMLConsideredworkedType;
    function Get_Allocatetowardspremium: IXMLAllocatetowardspremiumType;
    function Get_Creditpremium: IXMLCreditpremiumType;
    function Get_Credittowardsdailytotal: IXMLCredittowardsdailytotalType;
    function Get_Creditdailyguarantee: IXMLCreditdailyguaranteeType;
    function Get_Creditweeklyguarantee: IXMLCreditweeklyguaranteeType;
    function Get_Creditaccrual: IXMLCreditaccrualType;
    function Get_Disqualifyaccrual: IXMLDisqualifyaccrualType;
    function Get_Adjustandautoload: IXMLAdjustandautoloadType;
    function Get_Allocatetowardsovertime: IXMLAllocatetowardsovertimeType;
    function Get_Creditweeklyovertime: IXMLCreditweeklyovertimeType;
    function Get_Creditperiodovertime: IXMLCreditperiodovertimeType;
    function Get_Creditconsecutiveovertime: IXMLCreditconsecutiveovertimeType;
    function Get_Requirereason: IXMLRequirereasonType;
    function Get_Allowonschedule: IXMLAllowonscheduleType;
    function Get_Consideredabsent: IXMLConsideredabsentType;
    function Get_Allowfortimeoff: IXMLAllowfortimeoffType;
    function Get_Nomiscpayontimecard: IXMLNomiscpayontimecardType;
    function Get_Deductpremium: IXMLDeductpremiumType;
    function Get_Allowaccrual: IXMLAllowaccrualType;
    function Get_Hourscode: WideString;
    function Get_Dollarscode: WideString;
    function Get_Reportgroup: WideString;
    function Get_Payratemodification: IXMLPayratemodificationType;
    function Get_Fixedamountaddition: Integer;
    function Get_Percentageaddition: Integer;
    function Get_Allowregularhours: IXMLAllowregularhoursType;
    function Get_Allowmischours: IXMLAllowmischoursType;
    function Get_Allowmiscamount: IXMLAllowmiscamountType;
    function Get_Timeclockid: Integer;
    function Get_Attendancecalendar: IXMLAttendancecalendarType;
    function Get_Useabbreviation: WideString;
    function Get_Tipscode: IXMLTipscodeType;
    procedure Set_Id(Value: WideString);
    procedure Set_Paytypeid(Value: WideString);
    procedure Set_Paytypename(Value: WideString);
    procedure Set_Hourscode(Value: WideString);
    procedure Set_Dollarscode(Value: WideString);
    procedure Set_Reportgroup(Value: WideString);
    procedure Set_Fixedamountaddition(Value: Integer);
    procedure Set_Percentageaddition(Value: Integer);
    procedure Set_Timeclockid(Value: Integer);
    procedure Set_Useabbreviation(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPayrollclassType }

  TXMLPayrollclassType = class(TXMLNode, IXMLPayrollclassType)
  protected
    { IXMLPayrollclassType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPaidtimeType }

  TXMLPaidtimeType = class(TXMLNode, IXMLPaidtimeType)
  protected
    { IXMLPaidtimeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLConsideredworkedType }

  TXMLConsideredworkedType = class(TXMLNode, IXMLConsideredworkedType)
  protected
    { IXMLConsideredworkedType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllocatetowardspremiumType }

  TXMLAllocatetowardspremiumType = class(TXMLNode, IXMLAllocatetowardspremiumType)
  protected
    { IXMLAllocatetowardspremiumType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditpremiumType }

  TXMLCreditpremiumType = class(TXMLNode, IXMLCreditpremiumType)
  protected
    { IXMLCreditpremiumType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCredittowardsdailytotalType }

  TXMLCredittowardsdailytotalType = class(TXMLNode, IXMLCredittowardsdailytotalType)
  protected
    { IXMLCredittowardsdailytotalType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditdailyguaranteeType }

  TXMLCreditdailyguaranteeType = class(TXMLNode, IXMLCreditdailyguaranteeType)
  protected
    { IXMLCreditdailyguaranteeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditweeklyguaranteeType }

  TXMLCreditweeklyguaranteeType = class(TXMLNode, IXMLCreditweeklyguaranteeType)
  protected
    { IXMLCreditweeklyguaranteeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditaccrualType }

  TXMLCreditaccrualType = class(TXMLNode, IXMLCreditaccrualType)
  protected
    { IXMLCreditaccrualType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLDisqualifyaccrualType }

  TXMLDisqualifyaccrualType = class(TXMLNode, IXMLDisqualifyaccrualType)
  protected
    { IXMLDisqualifyaccrualType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAdjustandautoloadType }

  TXMLAdjustandautoloadType = class(TXMLNode, IXMLAdjustandautoloadType)
  protected
    { IXMLAdjustandautoloadType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllocatetowardsovertimeType }

  TXMLAllocatetowardsovertimeType = class(TXMLNode, IXMLAllocatetowardsovertimeType)
  protected
    { IXMLAllocatetowardsovertimeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditweeklyovertimeType }

  TXMLCreditweeklyovertimeType = class(TXMLNode, IXMLCreditweeklyovertimeType)
  protected
    { IXMLCreditweeklyovertimeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditperiodovertimeType }

  TXMLCreditperiodovertimeType = class(TXMLNode, IXMLCreditperiodovertimeType)
  protected
    { IXMLCreditperiodovertimeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLCreditconsecutiveovertimeType }

  TXMLCreditconsecutiveovertimeType = class(TXMLNode, IXMLCreditconsecutiveovertimeType)
  protected
    { IXMLCreditconsecutiveovertimeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLRequirereasonType }

  TXMLRequirereasonType = class(TXMLNode, IXMLRequirereasonType)
  protected
    { IXMLRequirereasonType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowonscheduleType }

  TXMLAllowonscheduleType = class(TXMLNode, IXMLAllowonscheduleType)
  protected
    { IXMLAllowonscheduleType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLConsideredabsentType }

  TXMLConsideredabsentType = class(TXMLNode, IXMLConsideredabsentType)
  protected
    { IXMLConsideredabsentType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowfortimeoffType }

  TXMLAllowfortimeoffType = class(TXMLNode, IXMLAllowfortimeoffType)
  protected
    { IXMLAllowfortimeoffType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLNomiscpayontimecardType }

  TXMLNomiscpayontimecardType = class(TXMLNode, IXMLNomiscpayontimecardType)
  protected
    { IXMLNomiscpayontimecardType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLDeductpremiumType }

  TXMLDeductpremiumType = class(TXMLNode, IXMLDeductpremiumType)
  protected
    { IXMLDeductpremiumType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowaccrualType }

  TXMLAllowaccrualType = class(TXMLNode, IXMLAllowaccrualType)
  protected
    { IXMLAllowaccrualType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPayratemodificationType }

  TXMLPayratemodificationType = class(TXMLNode, IXMLPayratemodificationType)
  protected
    { IXMLPayratemodificationType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowregularhoursType }

  TXMLAllowregularhoursType = class(TXMLNode, IXMLAllowregularhoursType)
  protected
    { IXMLAllowregularhoursType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowmischoursType }

  TXMLAllowmischoursType = class(TXMLNode, IXMLAllowmischoursType)
  protected
    { IXMLAllowmischoursType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowmiscamountType }

  TXMLAllowmiscamountType = class(TXMLNode, IXMLAllowmiscamountType)
  protected
    { IXMLAllowmiscamountType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAttendancecalendarType }

  TXMLAttendancecalendarType = class(TXMLNode, IXMLAttendancecalendarType)
  protected
    { IXMLAttendancecalendarType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLTipscodeType }

  TXMLTipscodeType = class(TXMLNode, IXMLTipscodeType)
  protected
    { IXMLTipscodeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ Global Functions }

function Getpaytypes(Doc: IXMLDocument): IXMLPaytypesType;
function Loadpaytypes(const FileName: WideString): IXMLPaytypesType;
function Newpaytypes: IXMLPaytypesType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getpaytypes(Doc: IXMLDocument): IXMLPaytypesType;
begin
  Result := Doc.GetDocBinding('paytypes', TXMLPaytypesType, TargetNamespace) as IXMLPaytypesType;
end;

function Loadpaytypes(const FileName: WideString): IXMLPaytypesType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('paytypes', TXMLPaytypesType, TargetNamespace) as IXMLPaytypesType;
end;

function Newpaytypes: IXMLPaytypesType;
begin
  Result := NewXMLDocument.GetDocBinding('paytypes', TXMLPaytypesType, TargetNamespace) as IXMLPaytypesType;
end;

{ TXMLPaytypesType }

procedure TXMLPaytypesType.AfterConstruction;
begin
  RegisterChildNode('paytype', TXMLPaytypeType);
  ItemTag := 'paytype';
  ItemInterface := IXMLPaytypeType;
  inherited;
end;

function TXMLPaytypesType.Get_Morerecords: Integer;
begin
  Result := AttributeNodes['morerecords'].NodeValue;
end;

procedure TXMLPaytypesType.Set_Morerecords(Value: Integer);
begin
  SetAttribute('morerecords', Value);
end;

function TXMLPaytypesType.Get_Paytype(Index: Integer): IXMLPaytypeType;
begin
  Result := List[Index] as IXMLPaytypeType;
end;

function TXMLPaytypesType.Add: IXMLPaytypeType;
begin
  Result := AddItem(-1) as IXMLPaytypeType;
end;

function TXMLPaytypesType.Insert(const Index: Integer): IXMLPaytypeType;
begin
  Result := AddItem(Index) as IXMLPaytypeType;
end;

{ TXMLPaytypeType }

procedure TXMLPaytypeType.AfterConstruction;
begin
  RegisterChildNode('payrollclass', TXMLPayrollclassType);
  RegisterChildNode('paidtime', TXMLPaidtimeType);
  RegisterChildNode('consideredworked', TXMLConsideredworkedType);
  RegisterChildNode('allocatetowardspremium', TXMLAllocatetowardspremiumType);
  RegisterChildNode('creditpremium', TXMLCreditpremiumType);
  RegisterChildNode('credittowardsdailytotal', TXMLCredittowardsdailytotalType);
  RegisterChildNode('creditdailyguarantee', TXMLCreditdailyguaranteeType);
  RegisterChildNode('creditweeklyguarantee', TXMLCreditweeklyguaranteeType);
  RegisterChildNode('creditaccrual', TXMLCreditaccrualType);
  RegisterChildNode('disqualifyaccrual', TXMLDisqualifyaccrualType);
  RegisterChildNode('adjustandautoload', TXMLAdjustandautoloadType);
  RegisterChildNode('allocatetowardsovertime', TXMLAllocatetowardsovertimeType);
  RegisterChildNode('creditweeklyovertime', TXMLCreditweeklyovertimeType);
  RegisterChildNode('creditperiodovertime', TXMLCreditperiodovertimeType);
  RegisterChildNode('creditconsecutiveovertime', TXMLCreditconsecutiveovertimeType);
  RegisterChildNode('requirereason', TXMLRequirereasonType);
  RegisterChildNode('allowonschedule', TXMLAllowonscheduleType);
  RegisterChildNode('consideredabsent', TXMLConsideredabsentType);
  RegisterChildNode('allowfortimeoff', TXMLAllowfortimeoffType);
  RegisterChildNode('nomiscpayontimecard', TXMLNomiscpayontimecardType);
  RegisterChildNode('deductpremium', TXMLDeductpremiumType);
  RegisterChildNode('allowaccrual', TXMLAllowaccrualType);
  RegisterChildNode('payratemodification', TXMLPayratemodificationType);
  RegisterChildNode('allowregularhours', TXMLAllowregularhoursType);
  RegisterChildNode('allowmischours', TXMLAllowmischoursType);
  RegisterChildNode('allowmiscamount', TXMLAllowmiscamountType);
  RegisterChildNode('attendancecalendar', TXMLAttendancecalendarType);
  RegisterChildNode('tipscode', TXMLTipscodeType);
  inherited;
end;

function TXMLPaytypeType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLPaytypeType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Paytypeid: WideString;
begin
  Result := ChildNodes['paytypeid'].Text;
end;

procedure TXMLPaytypeType.Set_Paytypeid(Value: WideString);
begin
  ChildNodes['paytypeid'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Paytypename: WideString;
begin
  Result := ChildNodes['paytypename'].Text;
end;

procedure TXMLPaytypeType.Set_Paytypename(Value: WideString);
begin
  ChildNodes['paytypename'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Payrollclass: IXMLPayrollclassType;
begin
  Result := ChildNodes['payrollclass'] as IXMLPayrollclassType;
end;

function TXMLPaytypeType.Get_Paidtime: IXMLPaidtimeType;
begin
  Result := ChildNodes['paidtime'] as IXMLPaidtimeType;
end;

function TXMLPaytypeType.Get_Consideredworked: IXMLConsideredworkedType;
begin
  Result := ChildNodes['consideredworked'] as IXMLConsideredworkedType;
end;

function TXMLPaytypeType.Get_Allocatetowardspremium: IXMLAllocatetowardspremiumType;
begin
  Result := ChildNodes['allocatetowardspremium'] as IXMLAllocatetowardspremiumType;
end;

function TXMLPaytypeType.Get_Creditpremium: IXMLCreditpremiumType;
begin
  Result := ChildNodes['creditpremium'] as IXMLCreditpremiumType;
end;

function TXMLPaytypeType.Get_Credittowardsdailytotal: IXMLCredittowardsdailytotalType;
begin
  Result := ChildNodes['credittowardsdailytotal'] as IXMLCredittowardsdailytotalType;
end;

function TXMLPaytypeType.Get_Creditdailyguarantee: IXMLCreditdailyguaranteeType;
begin
  Result := ChildNodes['creditdailyguarantee'] as IXMLCreditdailyguaranteeType;
end;

function TXMLPaytypeType.Get_Creditweeklyguarantee: IXMLCreditweeklyguaranteeType;
begin
  Result := ChildNodes['creditweeklyguarantee'] as IXMLCreditweeklyguaranteeType;
end;

function TXMLPaytypeType.Get_Creditaccrual: IXMLCreditaccrualType;
begin
  Result := ChildNodes['creditaccrual'] as IXMLCreditaccrualType;
end;

function TXMLPaytypeType.Get_Disqualifyaccrual: IXMLDisqualifyaccrualType;
begin
  Result := ChildNodes['disqualifyaccrual'] as IXMLDisqualifyaccrualType;
end;

function TXMLPaytypeType.Get_Adjustandautoload: IXMLAdjustandautoloadType;
begin
  Result := ChildNodes['adjustandautoload'] as IXMLAdjustandautoloadType;
end;

function TXMLPaytypeType.Get_Allocatetowardsovertime: IXMLAllocatetowardsovertimeType;
begin
  Result := ChildNodes['allocatetowardsovertime'] as IXMLAllocatetowardsovertimeType;
end;

function TXMLPaytypeType.Get_Creditweeklyovertime: IXMLCreditweeklyovertimeType;
begin
  Result := ChildNodes['creditweeklyovertime'] as IXMLCreditweeklyovertimeType;
end;

function TXMLPaytypeType.Get_Creditperiodovertime: IXMLCreditperiodovertimeType;
begin
  Result := ChildNodes['creditperiodovertime'] as IXMLCreditperiodovertimeType;
end;

function TXMLPaytypeType.Get_Creditconsecutiveovertime: IXMLCreditconsecutiveovertimeType;
begin
  Result := ChildNodes['creditconsecutiveovertime'] as IXMLCreditconsecutiveovertimeType;
end;

function TXMLPaytypeType.Get_Requirereason: IXMLRequirereasonType;
begin
  Result := ChildNodes['requirereason'] as IXMLRequirereasonType;
end;

function TXMLPaytypeType.Get_Allowonschedule: IXMLAllowonscheduleType;
begin
  Result := ChildNodes['allowonschedule'] as IXMLAllowonscheduleType;
end;

function TXMLPaytypeType.Get_Consideredabsent: IXMLConsideredabsentType;
begin
  Result := ChildNodes['consideredabsent'] as IXMLConsideredabsentType;
end;

function TXMLPaytypeType.Get_Allowfortimeoff: IXMLAllowfortimeoffType;
begin
  Result := ChildNodes['allowfortimeoff'] as IXMLAllowfortimeoffType;
end;

function TXMLPaytypeType.Get_Nomiscpayontimecard: IXMLNomiscpayontimecardType;
begin
  Result := ChildNodes['nomiscpayontimecard'] as IXMLNomiscpayontimecardType;
end;

function TXMLPaytypeType.Get_Deductpremium: IXMLDeductpremiumType;
begin
  Result := ChildNodes['deductpremium'] as IXMLDeductpremiumType;
end;

function TXMLPaytypeType.Get_Allowaccrual: IXMLAllowaccrualType;
begin
  Result := ChildNodes['allowaccrual'] as IXMLAllowaccrualType;
end;

function TXMLPaytypeType.Get_Hourscode: WideString;
begin
  Result := ChildNodes['hourscode'].Text;
end;

procedure TXMLPaytypeType.Set_Hourscode(Value: WideString);
begin
  ChildNodes['hourscode'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Dollarscode: WideString;
begin
  Result := ChildNodes['dollarscode'].Text;
end;

procedure TXMLPaytypeType.Set_Dollarscode(Value: WideString);
begin
  ChildNodes['dollarscode'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Reportgroup: WideString;
begin
  Result := ChildNodes['reportgroup'].Text;
end;

procedure TXMLPaytypeType.Set_Reportgroup(Value: WideString);
begin
  ChildNodes['reportgroup'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Payratemodification: IXMLPayratemodificationType;
begin
  Result := ChildNodes['payratemodification'] as IXMLPayratemodificationType;
end;

function TXMLPaytypeType.Get_Fixedamountaddition: Integer;
begin
  Result := ChildNodes['fixedamountaddition'].NodeValue;
end;

procedure TXMLPaytypeType.Set_Fixedamountaddition(Value: Integer);
begin
  ChildNodes['fixedamountaddition'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Percentageaddition: Integer;
begin
  Result := ChildNodes['percentageaddition'].NodeValue;
end;

procedure TXMLPaytypeType.Set_Percentageaddition(Value: Integer);
begin
  ChildNodes['percentageaddition'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Allowregularhours: IXMLAllowregularhoursType;
begin
  Result := ChildNodes['allowregularhours'] as IXMLAllowregularhoursType;
end;

function TXMLPaytypeType.Get_Allowmischours: IXMLAllowmischoursType;
begin
  Result := ChildNodes['allowmischours'] as IXMLAllowmischoursType;
end;

function TXMLPaytypeType.Get_Allowmiscamount: IXMLAllowmiscamountType;
begin
  Result := ChildNodes['allowmiscamount'] as IXMLAllowmiscamountType;
end;

function TXMLPaytypeType.Get_Timeclockid: Integer;
begin
  Result := ChildNodes['timeclockid'].NodeValue;
end;

procedure TXMLPaytypeType.Set_Timeclockid(Value: Integer);
begin
  ChildNodes['timeclockid'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Attendancecalendar: IXMLAttendancecalendarType;
begin
  Result := ChildNodes['attendancecalendar'] as IXMLAttendancecalendarType;
end;

function TXMLPaytypeType.Get_Useabbreviation: WideString;
begin
  Result := ChildNodes['useabbreviation'].Text;
end;

procedure TXMLPaytypeType.Set_Useabbreviation(Value: WideString);
begin
  ChildNodes['useabbreviation'].NodeValue := Value;
end;

function TXMLPaytypeType.Get_Tipscode: IXMLTipscodeType;
begin
  Result := ChildNodes['tipscode'] as IXMLTipscodeType;
end;

{ TXMLPayrollclassType }

function TXMLPayrollclassType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPayrollclassType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPaidtimeType }

function TXMLPaidtimeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPaidtimeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLConsideredworkedType }

function TXMLConsideredworkedType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLConsideredworkedType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllocatetowardspremiumType }

function TXMLAllocatetowardspremiumType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllocatetowardspremiumType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditpremiumType }

function TXMLCreditpremiumType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditpremiumType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCredittowardsdailytotalType }

function TXMLCredittowardsdailytotalType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCredittowardsdailytotalType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditdailyguaranteeType }

function TXMLCreditdailyguaranteeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditdailyguaranteeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditweeklyguaranteeType }

function TXMLCreditweeklyguaranteeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditweeklyguaranteeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditaccrualType }

function TXMLCreditaccrualType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditaccrualType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLDisqualifyaccrualType }

function TXMLDisqualifyaccrualType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLDisqualifyaccrualType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAdjustandautoloadType }

function TXMLAdjustandautoloadType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAdjustandautoloadType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllocatetowardsovertimeType }

function TXMLAllocatetowardsovertimeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllocatetowardsovertimeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditweeklyovertimeType }

function TXMLCreditweeklyovertimeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditweeklyovertimeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditperiodovertimeType }

function TXMLCreditperiodovertimeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditperiodovertimeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLCreditconsecutiveovertimeType }

function TXMLCreditconsecutiveovertimeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLCreditconsecutiveovertimeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLRequirereasonType }

function TXMLRequirereasonType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLRequirereasonType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowonscheduleType }

function TXMLAllowonscheduleType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowonscheduleType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLConsideredabsentType }

function TXMLConsideredabsentType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLConsideredabsentType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowfortimeoffType }

function TXMLAllowfortimeoffType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowfortimeoffType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLNomiscpayontimecardType }

function TXMLNomiscpayontimecardType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLNomiscpayontimecardType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLDeductpremiumType }

function TXMLDeductpremiumType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLDeductpremiumType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowaccrualType }

function TXMLAllowaccrualType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowaccrualType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPayratemodificationType }

function TXMLPayratemodificationType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPayratemodificationType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowregularhoursType }

function TXMLAllowregularhoursType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowregularhoursType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowmischoursType }

function TXMLAllowmischoursType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowmischoursType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowmiscamountType }

function TXMLAllowmiscamountType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowmiscamountType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAttendancecalendarType }

function TXMLAttendancecalendarType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAttendancecalendarType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLTipscodeType }

function TXMLTipscodeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLTipscodeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

end.