
{***************************************************************************************}
{                                                                                       }
{                                   XML Data Binding                                    }
{                                                                                       }
{         Generated on: 6/9/2009 1:04:29 PM                                             }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllPayGroups\Output.xml   }
{                                                                                       }
{***************************************************************************************}

unit bndRetrieveAllPayGroups;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLPaygroupsType = interface;
  IXMLPaygroupType = interface;
  IXMLIsactiveType = interface;
  IXMLPaycalendarType = interface;
  IXMLDefaultpaytypeType = interface;
  IXMLAutopunchpaytypeType = interface;
  IXMLTimeentrytechniqueType = interface;
  IXMLAllowdefaultpaytypesType = interface;
  IXMLAllowpayapprovalsType = interface;
  IXMLAllowphantomscheduleType = interface;
  IXMLExemptemployeesType = interface;
  IXMLNewdaystarttimeType = interface;
  IXMLScheduledstarttimeType = interface;
  IXMLWeekstartdayType = interface;
  IXMLAbsencetypeType = interface;
  IXMLScheduleddayworkedqualifiertypeType = interface;
  IXMLNonscheduleddayworkedqualifiertypeType = interface;

{ IXMLPaygroupsType }

  IXMLPaygroupsType = interface(IXMLNodeCollection)
    ['{A135B485-9CB2-4550-B047-8DFCE7430178}']
    { Property Accessors }
    function Get_Paygroup(Index: Integer): IXMLPaygroupType;
    { Methods & Properties }
    function Add: IXMLPaygroupType;
    function Insert(const Index: Integer): IXMLPaygroupType;
    property Paygroup[Index: Integer]: IXMLPaygroupType read Get_Paygroup; default;
  end;

{ IXMLPaygroupType }

  IXMLPaygroupType = interface(IXMLNode)
    ['{F93DABFF-4F1F-41CD-9C7C-A0A7E55BBA5D}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Paygroupid: WideString;
    function Get_Name: WideString;
    function Get_Maxpunchsetsfortheday: Integer;
    function Get_Minimumsplithours: Integer;
    function Get_Toomanyhourswarninglimit: Integer;
    function Get_Automaticpunchouthours: WideString;
    function Get_Maxtimebetweenactivity: WideString;
    function Get_Maxworkhoursperday: WideString;
    function Get_Isactive: IXMLIsactiveType;
    function Get_Paycalendar: IXMLPaycalendarType;
    function Get_Defaultpaytype: IXMLDefaultpaytypeType;
    function Get_Autopunchpaytype: IXMLAutopunchpaytypeType;
    function Get_Timeentrytechnique: IXMLTimeentrytechniqueType;
    function Get_Allowdefaultpaytypes: IXMLAllowdefaultpaytypesType;
    function Get_Allowpayapprovals: IXMLAllowpayapprovalsType;
    function Get_Allowphantomschedule: IXMLAllowphantomscheduleType;
    function Get_Exemptemployees: IXMLExemptemployeesType;
    function Get_Newdaystarttime: IXMLNewdaystarttimeType;
    function Get_Scheduledstarttime: IXMLScheduledstarttimeType;
    function Get_Weekstartday: IXMLWeekstartdayType;
    function Get_Absencetype: IXMLAbsencetypeType;
    function Get_Scheduleddayworkedqualifiertype: IXMLScheduleddayworkedqualifiertypeType;
    function Get_Scheduleddayworkhours: WideString;
    function Get_Nonscheduleddayworkedqualifiertype: IXMLNonscheduleddayworkedqualifiertypeType;
    function Get_Nonscheduleddayworkedqualifiervalue: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Paygroupid(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Maxpunchsetsfortheday(Value: Integer);
    procedure Set_Minimumsplithours(Value: Integer);
    procedure Set_Toomanyhourswarninglimit(Value: Integer);
    procedure Set_Automaticpunchouthours(Value: WideString);
    procedure Set_Maxtimebetweenactivity(Value: WideString);
    procedure Set_Maxworkhoursperday(Value: WideString);
    procedure Set_Scheduleddayworkhours(Value: WideString);
    procedure Set_Nonscheduleddayworkedqualifiervalue(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Paygroupid: WideString read Get_Paygroupid write Set_Paygroupid;
    property Name: WideString read Get_Name write Set_Name;
    property Maxpunchsetsfortheday: Integer read Get_Maxpunchsetsfortheday write Set_Maxpunchsetsfortheday;
    property Minimumsplithours: Integer read Get_Minimumsplithours write Set_Minimumsplithours;
    property Toomanyhourswarninglimit: Integer read Get_Toomanyhourswarninglimit write Set_Toomanyhourswarninglimit;
    property Automaticpunchouthours: WideString read Get_Automaticpunchouthours write Set_Automaticpunchouthours;
    property Maxtimebetweenactivity: WideString read Get_Maxtimebetweenactivity write Set_Maxtimebetweenactivity;
    property Maxworkhoursperday: WideString read Get_Maxworkhoursperday write Set_Maxworkhoursperday;
    property Isactive: IXMLIsactiveType read Get_Isactive;
    property Paycalendar: IXMLPaycalendarType read Get_Paycalendar;
    property Defaultpaytype: IXMLDefaultpaytypeType read Get_Defaultpaytype;
    property Autopunchpaytype: IXMLAutopunchpaytypeType read Get_Autopunchpaytype;
    property Timeentrytechnique: IXMLTimeentrytechniqueType read Get_Timeentrytechnique;
    property Allowdefaultpaytypes: IXMLAllowdefaultpaytypesType read Get_Allowdefaultpaytypes;
    property Allowpayapprovals: IXMLAllowpayapprovalsType read Get_Allowpayapprovals;
    property Allowphantomschedule: IXMLAllowphantomscheduleType read Get_Allowphantomschedule;
    property Exemptemployees: IXMLExemptemployeesType read Get_Exemptemployees;
    property Newdaystarttime: IXMLNewdaystarttimeType read Get_Newdaystarttime;
    property Scheduledstarttime: IXMLScheduledstarttimeType read Get_Scheduledstarttime;
    property Weekstartday: IXMLWeekstartdayType read Get_Weekstartday;
    property Absencetype: IXMLAbsencetypeType read Get_Absencetype;
    property Scheduleddayworkedqualifiertype: IXMLScheduleddayworkedqualifiertypeType read Get_Scheduleddayworkedqualifiertype;
    property Scheduleddayworkhours: WideString read Get_Scheduleddayworkhours write Set_Scheduleddayworkhours;
    property Nonscheduleddayworkedqualifiertype: IXMLNonscheduleddayworkedqualifiertypeType read Get_Nonscheduleddayworkedqualifiertype;
    property Nonscheduleddayworkedqualifiervalue: WideString read Get_Nonscheduleddayworkedqualifiervalue write Set_Nonscheduleddayworkedqualifiervalue;
  end;

{ IXMLIsactiveType }

  IXMLIsactiveType = interface(IXMLNode)
    ['{C375943E-C572-4BAB-A28D-52F0E22BDC30}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPaycalendarType }

  IXMLPaycalendarType = interface(IXMLNode)
    ['{B0762EB4-0CB6-4BA1-8895-43F2C810F712}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLDefaultpaytypeType }

  IXMLDefaultpaytypeType = interface(IXMLNode)
    ['{1E1FAE0F-CB8F-4742-B7CD-C05F36275EBD}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAutopunchpaytypeType }

  IXMLAutopunchpaytypeType = interface(IXMLNode)
    ['{40A1E9B6-90EA-47C2-853F-07D39D37738E}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLTimeentrytechniqueType }

  IXMLTimeentrytechniqueType = interface(IXMLNode)
    ['{6DCFFA4E-A9C0-4792-A4F3-1D14D3DD4F90}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowdefaultpaytypesType }

  IXMLAllowdefaultpaytypesType = interface(IXMLNode)
    ['{982688BF-E113-4FB0-AD94-5149D41AC4D4}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowpayapprovalsType }

  IXMLAllowpayapprovalsType = interface(IXMLNode)
    ['{249D705C-32F3-46FA-BE5E-E2DAF12EE1CE}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAllowphantomscheduleType }

  IXMLAllowphantomscheduleType = interface(IXMLNode)
    ['{0C4F6546-E347-4025-ADA1-1D19C237399C}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLExemptemployeesType }

  IXMLExemptemployeesType = interface(IXMLNode)
    ['{8E27B6BE-E65A-4EC5-8F94-948935B0E777}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLNewdaystarttimeType }

  IXMLNewdaystarttimeType = interface(IXMLNode)
    ['{DDC9D63E-D7D4-48D7-A96C-F56F1A877414}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLScheduledstarttimeType }

  IXMLScheduledstarttimeType = interface(IXMLNode)
    ['{2ACC552E-B25B-4810-A343-312BEC470242}']
    { Property Accessors }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
    { Methods & Properties }
    property Date: WideString read Get_Date write Set_Date;
    property Time: WideString read Get_Time write Set_Time;
  end;

{ IXMLWeekstartdayType }

  IXMLWeekstartdayType = interface(IXMLNode)
    ['{1DFEEB72-CADE-4F96-AA99-EE13219021B5}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAbsencetypeType }

  IXMLAbsencetypeType = interface(IXMLNode)
    ['{59C3C23B-33EE-4EED-9121-6D3B708B6ED2}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLScheduleddayworkedqualifiertypeType }

  IXMLScheduleddayworkedqualifiertypeType = interface(IXMLNode)
    ['{87DFDEA3-3BAF-48E0-922E-CFB0D4232DBF}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLNonscheduleddayworkedqualifiertypeType }

  IXMLNonscheduleddayworkedqualifiertypeType = interface(IXMLNode)
    ['{5B9D89CD-C7F6-4EED-9CC2-57E58B4A8CBD}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ Forward Decls }

  TXMLPaygroupsType = class;
  TXMLPaygroupType = class;
  TXMLIsactiveType = class;
  TXMLPaycalendarType = class;
  TXMLDefaultpaytypeType = class;
  TXMLAutopunchpaytypeType = class;
  TXMLTimeentrytechniqueType = class;
  TXMLAllowdefaultpaytypesType = class;
  TXMLAllowpayapprovalsType = class;
  TXMLAllowphantomscheduleType = class;
  TXMLExemptemployeesType = class;
  TXMLNewdaystarttimeType = class;
  TXMLScheduledstarttimeType = class;
  TXMLWeekstartdayType = class;
  TXMLAbsencetypeType = class;
  TXMLScheduleddayworkedqualifiertypeType = class;
  TXMLNonscheduleddayworkedqualifiertypeType = class;

{ TXMLPaygroupsType }

  TXMLPaygroupsType = class(TXMLNodeCollection, IXMLPaygroupsType)
  protected
    { IXMLPaygroupsType }
    function Get_Paygroup(Index: Integer): IXMLPaygroupType;
    function Add: IXMLPaygroupType;
    function Insert(const Index: Integer): IXMLPaygroupType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPaygroupType }

  TXMLPaygroupType = class(TXMLNode, IXMLPaygroupType)
  protected
    { IXMLPaygroupType }
    function Get_Id: WideString;
    function Get_Paygroupid: WideString;
    function Get_Name: WideString;
    function Get_Maxpunchsetsfortheday: Integer;
    function Get_Minimumsplithours: Integer;
    function Get_Toomanyhourswarninglimit: Integer;
    function Get_Automaticpunchouthours: WideString;
    function Get_Maxtimebetweenactivity: WideString;
    function Get_Maxworkhoursperday: WideString;
    function Get_Isactive: IXMLIsactiveType;
    function Get_Paycalendar: IXMLPaycalendarType;
    function Get_Defaultpaytype: IXMLDefaultpaytypeType;
    function Get_Autopunchpaytype: IXMLAutopunchpaytypeType;
    function Get_Timeentrytechnique: IXMLTimeentrytechniqueType;
    function Get_Allowdefaultpaytypes: IXMLAllowdefaultpaytypesType;
    function Get_Allowpayapprovals: IXMLAllowpayapprovalsType;
    function Get_Allowphantomschedule: IXMLAllowphantomscheduleType;
    function Get_Exemptemployees: IXMLExemptemployeesType;
    function Get_Newdaystarttime: IXMLNewdaystarttimeType;
    function Get_Scheduledstarttime: IXMLScheduledstarttimeType;
    function Get_Weekstartday: IXMLWeekstartdayType;
    function Get_Absencetype: IXMLAbsencetypeType;
    function Get_Scheduleddayworkedqualifiertype: IXMLScheduleddayworkedqualifiertypeType;
    function Get_Scheduleddayworkhours: WideString;
    function Get_Nonscheduleddayworkedqualifiertype: IXMLNonscheduleddayworkedqualifiertypeType;
    function Get_Nonscheduleddayworkedqualifiervalue: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Paygroupid(Value: WideString);
    procedure Set_Name(Value: WideString);
    procedure Set_Maxpunchsetsfortheday(Value: Integer);
    procedure Set_Minimumsplithours(Value: Integer);
    procedure Set_Toomanyhourswarninglimit(Value: Integer);
    procedure Set_Automaticpunchouthours(Value: WideString);
    procedure Set_Maxtimebetweenactivity(Value: WideString);
    procedure Set_Maxworkhoursperday(Value: WideString);
    procedure Set_Scheduleddayworkhours(Value: WideString);
    procedure Set_Nonscheduleddayworkedqualifiervalue(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIsactiveType }

  TXMLIsactiveType = class(TXMLNode, IXMLIsactiveType)
  protected
    { IXMLIsactiveType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPaycalendarType }

  TXMLPaycalendarType = class(TXMLNode, IXMLPaycalendarType)
  protected
    { IXMLPaycalendarType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLDefaultpaytypeType }

  TXMLDefaultpaytypeType = class(TXMLNode, IXMLDefaultpaytypeType)
  protected
    { IXMLDefaultpaytypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAutopunchpaytypeType }

  TXMLAutopunchpaytypeType = class(TXMLNode, IXMLAutopunchpaytypeType)
  protected
    { IXMLAutopunchpaytypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLTimeentrytechniqueType }

  TXMLTimeentrytechniqueType = class(TXMLNode, IXMLTimeentrytechniqueType)
  protected
    { IXMLTimeentrytechniqueType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowdefaultpaytypesType }

  TXMLAllowdefaultpaytypesType = class(TXMLNode, IXMLAllowdefaultpaytypesType)
  protected
    { IXMLAllowdefaultpaytypesType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowpayapprovalsType }

  TXMLAllowpayapprovalsType = class(TXMLNode, IXMLAllowpayapprovalsType)
  protected
    { IXMLAllowpayapprovalsType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAllowphantomscheduleType }

  TXMLAllowphantomscheduleType = class(TXMLNode, IXMLAllowphantomscheduleType)
  protected
    { IXMLAllowphantomscheduleType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLExemptemployeesType }

  TXMLExemptemployeesType = class(TXMLNode, IXMLExemptemployeesType)
  protected
    { IXMLExemptemployeesType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLNewdaystarttimeType }

  TXMLNewdaystarttimeType = class(TXMLNode, IXMLNewdaystarttimeType)
  protected
    { IXMLNewdaystarttimeType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLScheduledstarttimeType }

  TXMLScheduledstarttimeType = class(TXMLNode, IXMLScheduledstarttimeType)
  protected
    { IXMLScheduledstarttimeType }
    function Get_Date: WideString;
    function Get_Time: WideString;
    procedure Set_Date(Value: WideString);
    procedure Set_Time(Value: WideString);
  end;

{ TXMLWeekstartdayType }

  TXMLWeekstartdayType = class(TXMLNode, IXMLWeekstartdayType)
  protected
    { IXMLWeekstartdayType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAbsencetypeType }

  TXMLAbsencetypeType = class(TXMLNode, IXMLAbsencetypeType)
  protected
    { IXMLAbsencetypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLScheduleddayworkedqualifiertypeType }

  TXMLScheduleddayworkedqualifiertypeType = class(TXMLNode, IXMLScheduleddayworkedqualifiertypeType)
  protected
    { IXMLScheduleddayworkedqualifiertypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLNonscheduleddayworkedqualifiertypeType }

  TXMLNonscheduleddayworkedqualifiertypeType = class(TXMLNode, IXMLNonscheduleddayworkedqualifiertypeType)
  protected
    { IXMLNonscheduleddayworkedqualifiertypeType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ Global Functions }

function Getpaygroups(Doc: IXMLDocument): IXMLPaygroupsType;
function Loadpaygroups(const FileName: WideString): IXMLPaygroupsType;
function Newpaygroups: IXMLPaygroupsType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getpaygroups(Doc: IXMLDocument): IXMLPaygroupsType;
begin
  Result := Doc.GetDocBinding('paygroups', TXMLPaygroupsType, TargetNamespace) as IXMLPaygroupsType;
end;

function Loadpaygroups(const FileName: WideString): IXMLPaygroupsType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('paygroups', TXMLPaygroupsType, TargetNamespace) as IXMLPaygroupsType;
end;

function Newpaygroups: IXMLPaygroupsType;
begin
  Result := NewXMLDocument.GetDocBinding('paygroups', TXMLPaygroupsType, TargetNamespace) as IXMLPaygroupsType;
end;

{ TXMLPaygroupsType }

procedure TXMLPaygroupsType.AfterConstruction;
begin
  RegisterChildNode('paygroup', TXMLPaygroupType);
  ItemTag := 'paygroup';
  ItemInterface := IXMLPaygroupType;
  inherited;
end;

function TXMLPaygroupsType.Get_Paygroup(Index: Integer): IXMLPaygroupType;
begin
  Result := List[Index] as IXMLPaygroupType;
end;

function TXMLPaygroupsType.Add: IXMLPaygroupType;
begin
  Result := AddItem(-1) as IXMLPaygroupType;
end;

function TXMLPaygroupsType.Insert(const Index: Integer): IXMLPaygroupType;
begin
  Result := AddItem(Index) as IXMLPaygroupType;
end;

{ TXMLPaygroupType }

procedure TXMLPaygroupType.AfterConstruction;
begin
  RegisterChildNode('isactive', TXMLIsactiveType);
  RegisterChildNode('paycalendar', TXMLPaycalendarType);
  RegisterChildNode('defaultpaytype', TXMLDefaultpaytypeType);
  RegisterChildNode('autopunchpaytype', TXMLAutopunchpaytypeType);
  RegisterChildNode('timeentrytechnique', TXMLTimeentrytechniqueType);
  RegisterChildNode('allowdefaultpaytypes', TXMLAllowdefaultpaytypesType);
  RegisterChildNode('allowpayapprovals', TXMLAllowpayapprovalsType);
  RegisterChildNode('allowphantomschedule', TXMLAllowphantomscheduleType);
  RegisterChildNode('exemptemployees', TXMLExemptemployeesType);
  RegisterChildNode('newdaystarttime', TXMLNewdaystarttimeType);
  RegisterChildNode('scheduledstarttime', TXMLScheduledstarttimeType);
  RegisterChildNode('weekstartday', TXMLWeekstartdayType);
  RegisterChildNode('absencetype', TXMLAbsencetypeType);
  RegisterChildNode('scheduleddayworkedqualifiertype', TXMLScheduleddayworkedqualifiertypeType);
  RegisterChildNode('nonscheduleddayworkedqualifiertype', TXMLNonscheduleddayworkedqualifiertypeType);
  inherited;
end;

function TXMLPaygroupType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLPaygroupType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Paygroupid: WideString;
begin
  Result := ChildNodes['paygroupid'].Text;
end;

procedure TXMLPaygroupType.Set_Paygroupid(Value: WideString);
begin
  ChildNodes['paygroupid'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLPaygroupType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Maxpunchsetsfortheday: Integer;
begin
  Result := ChildNodes['maxpunchsetsfortheday'].NodeValue;
end;

procedure TXMLPaygroupType.Set_Maxpunchsetsfortheday(Value: Integer);
begin
  ChildNodes['maxpunchsetsfortheday'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Minimumsplithours: Integer;
begin
  Result := ChildNodes['minimumsplithours'].NodeValue;
end;

procedure TXMLPaygroupType.Set_Minimumsplithours(Value: Integer);
begin
  ChildNodes['minimumsplithours'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Toomanyhourswarninglimit: Integer;
begin
  Result := ChildNodes['toomanyhourswarninglimit'].NodeValue;
end;

procedure TXMLPaygroupType.Set_Toomanyhourswarninglimit(Value: Integer);
begin
  ChildNodes['toomanyhourswarninglimit'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Automaticpunchouthours: WideString;
begin
  Result := ChildNodes['automaticpunchouthours'].Text;
end;

procedure TXMLPaygroupType.Set_Automaticpunchouthours(Value: WideString);
begin
  ChildNodes['automaticpunchouthours'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Maxtimebetweenactivity: WideString;
begin
  Result := ChildNodes['maxtimebetweenactivity'].Text;
end;

procedure TXMLPaygroupType.Set_Maxtimebetweenactivity(Value: WideString);
begin
  ChildNodes['maxtimebetweenactivity'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Maxworkhoursperday: WideString;
begin
  Result := ChildNodes['maxworkhoursperday'].Text;
end;

procedure TXMLPaygroupType.Set_Maxworkhoursperday(Value: WideString);
begin
  ChildNodes['maxworkhoursperday'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Isactive: IXMLIsactiveType;
begin
  Result := ChildNodes['isactive'] as IXMLIsactiveType;
end;

function TXMLPaygroupType.Get_Paycalendar: IXMLPaycalendarType;
begin
  Result := ChildNodes['paycalendar'] as IXMLPaycalendarType;
end;

function TXMLPaygroupType.Get_Defaultpaytype: IXMLDefaultpaytypeType;
begin
  Result := ChildNodes['defaultpaytype'] as IXMLDefaultpaytypeType;
end;

function TXMLPaygroupType.Get_Autopunchpaytype: IXMLAutopunchpaytypeType;
begin
  Result := ChildNodes['autopunchpaytype'] as IXMLAutopunchpaytypeType;
end;

function TXMLPaygroupType.Get_Timeentrytechnique: IXMLTimeentrytechniqueType;
begin
  Result := ChildNodes['timeentrytechnique'] as IXMLTimeentrytechniqueType;
end;

function TXMLPaygroupType.Get_Allowdefaultpaytypes: IXMLAllowdefaultpaytypesType;
begin
  Result := ChildNodes['allowdefaultpaytypes'] as IXMLAllowdefaultpaytypesType;
end;

function TXMLPaygroupType.Get_Allowpayapprovals: IXMLAllowpayapprovalsType;
begin
  Result := ChildNodes['allowpayapprovals'] as IXMLAllowpayapprovalsType;
end;

function TXMLPaygroupType.Get_Allowphantomschedule: IXMLAllowphantomscheduleType;
begin
  Result := ChildNodes['allowphantomschedule'] as IXMLAllowphantomscheduleType;
end;

function TXMLPaygroupType.Get_Exemptemployees: IXMLExemptemployeesType;
begin
  Result := ChildNodes['exemptemployees'] as IXMLExemptemployeesType;
end;

function TXMLPaygroupType.Get_Newdaystarttime: IXMLNewdaystarttimeType;
begin
  Result := ChildNodes['newdaystarttime'] as IXMLNewdaystarttimeType;
end;

function TXMLPaygroupType.Get_Scheduledstarttime: IXMLScheduledstarttimeType;
begin
  Result := ChildNodes['scheduledstarttime'] as IXMLScheduledstarttimeType;
end;

function TXMLPaygroupType.Get_Weekstartday: IXMLWeekstartdayType;
begin
  Result := ChildNodes['weekstartday'] as IXMLWeekstartdayType;
end;

function TXMLPaygroupType.Get_Absencetype: IXMLAbsencetypeType;
begin
  Result := ChildNodes['absencetype'] as IXMLAbsencetypeType;
end;

function TXMLPaygroupType.Get_Scheduleddayworkedqualifiertype: IXMLScheduleddayworkedqualifiertypeType;
begin
  Result := ChildNodes['scheduleddayworkedqualifiertype'] as IXMLScheduleddayworkedqualifiertypeType;
end;

function TXMLPaygroupType.Get_Scheduleddayworkhours: WideString;
begin
  Result := ChildNodes['scheduleddayworkhours'].Text;
end;

procedure TXMLPaygroupType.Set_Scheduleddayworkhours(Value: WideString);
begin
  ChildNodes['scheduleddayworkhours'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Nonscheduleddayworkedqualifiertype: IXMLNonscheduleddayworkedqualifiertypeType;
begin
  Result := ChildNodes['nonscheduleddayworkedqualifiertype'] as IXMLNonscheduleddayworkedqualifiertypeType;
end;

function TXMLPaygroupType.Get_Nonscheduleddayworkedqualifiervalue: WideString;
begin
  Result := ChildNodes['nonscheduleddayworkedqualifiervalue'].Text;
end;

procedure TXMLPaygroupType.Set_Nonscheduleddayworkedqualifiervalue(Value: WideString);
begin
  ChildNodes['nonscheduleddayworkedqualifiervalue'].NodeValue := Value;
end;

{ TXMLIsactiveType }

function TXMLIsactiveType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLIsactiveType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLPaycalendarType }

function TXMLPaycalendarType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLPaycalendarType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLDefaultpaytypeType }

function TXMLDefaultpaytypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLDefaultpaytypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAutopunchpaytypeType }

function TXMLAutopunchpaytypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAutopunchpaytypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLTimeentrytechniqueType }

function TXMLTimeentrytechniqueType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLTimeentrytechniqueType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowdefaultpaytypesType }

function TXMLAllowdefaultpaytypesType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowdefaultpaytypesType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowpayapprovalsType }

function TXMLAllowpayapprovalsType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowpayapprovalsType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAllowphantomscheduleType }

function TXMLAllowphantomscheduleType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAllowphantomscheduleType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLExemptemployeesType }

function TXMLExemptemployeesType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLExemptemployeesType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLNewdaystarttimeType }

function TXMLNewdaystarttimeType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLNewdaystarttimeType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLNewdaystarttimeType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLNewdaystarttimeType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLScheduledstarttimeType }

function TXMLScheduledstarttimeType.Get_Date: WideString;
begin
  Result := AttributeNodes['date'].Text;
end;

procedure TXMLScheduledstarttimeType.Set_Date(Value: WideString);
begin
  SetAttribute('date', Value);
end;

function TXMLScheduledstarttimeType.Get_Time: WideString;
begin
  Result := AttributeNodes['time'].Text;
end;

procedure TXMLScheduledstarttimeType.Set_Time(Value: WideString);
begin
  SetAttribute('time', Value);
end;

{ TXMLWeekstartdayType }

function TXMLWeekstartdayType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLWeekstartdayType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLAbsencetypeType }

function TXMLAbsencetypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLAbsencetypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLScheduleddayworkedqualifiertypeType }

function TXMLScheduleddayworkedqualifiertypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLScheduleddayworkedqualifiertypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLNonscheduleddayworkedqualifiertypeType }

function TXMLNonscheduleddayworkedqualifiertypeType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLNonscheduleddayworkedqualifiertypeType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

end.