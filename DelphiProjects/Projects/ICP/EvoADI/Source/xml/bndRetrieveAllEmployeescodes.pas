
{**************************************************************************************}
{                                                                                      }
{                                   XML Data Binding                                   }
{                                                                                      }
{         Generated on: 6/7/2009 8:00:46 PM                                            }
{       Generated from: E:\job\IS\integration\ADI\xml\RetrieveAllEmployees-codes.xml   }
{                                                                                      }
{**************************************************************************************}

unit bndRetrieveAllEmployeescodes;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLEmployeesType = interface;
  IXMLRolesType = interface;
  IXMLRoleType = interface;
  IXMLPaygroupsType = interface;
  IXMLPaygroupType = interface;
  IXMLAccrualgroupsType = interface;
  IXMLAccrualType = interface;
  IXMLPointsgroupType = interface;
  IXMLPointgroupType = interface;
  IXMLMasterschedulesType = interface;
  IXMLMasterscheduleType = interface;

{ IXMLEmployeesType }

  IXMLEmployeesType = interface(IXMLNode)
    ['{28AFEA21-2F37-4FD6-8173-A2732E3FE84D}']
    { Property Accessors }
    function Get_Roles: IXMLRolesType;
    function Get_Paygroups: IXMLPaygroupsType;
    function Get_Accrualgroups: IXMLAccrualgroupsType;
    function Get_Pointsgroup: IXMLPointsgroupType;
    function Get_Supervisors: WideString;
    function Get_Masterschedules: IXMLMasterschedulesType;
    function Get_Rotationschedules: WideString;
    procedure Set_Supervisors(Value: WideString);
    procedure Set_Rotationschedules(Value: WideString);
    { Methods & Properties }
    property Roles: IXMLRolesType read Get_Roles;
    property Paygroups: IXMLPaygroupsType read Get_Paygroups;
    property Accrualgroups: IXMLAccrualgroupsType read Get_Accrualgroups;
    property Pointsgroup: IXMLPointsgroupType read Get_Pointsgroup;
    property Supervisors: WideString read Get_Supervisors write Set_Supervisors;
    property Masterschedules: IXMLMasterschedulesType read Get_Masterschedules;
    property Rotationschedules: WideString read Get_Rotationschedules write Set_Rotationschedules;
  end;

{ IXMLRolesType }

  IXMLRolesType = interface(IXMLNodeCollection)
    ['{47A4C30D-322B-4522-BD70-3CA9F075EE90}']
    { Property Accessors }
    function Get_Role(Index: Integer): IXMLRoleType;
    { Methods & Properties }
    function Add: IXMLRoleType;
    function Insert(const Index: Integer): IXMLRoleType;
    property Role[Index: Integer]: IXMLRoleType read Get_Role; default;
  end;

{ IXMLRoleType }

  IXMLRoleType = interface(IXMLNode)
    ['{A5D9DFDC-2030-4E40-8204-254F8DB2F2B2}']
    { Property Accessors }
    function Get_Name: WideString;
    function Get_Description: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
    property Description: WideString read Get_Description write Set_Description;
  end;

{ IXMLPaygroupsType }

  IXMLPaygroupsType = interface(IXMLNodeCollection)
    ['{BAF1C490-6330-487C-9D15-87FC9221A213}']
    { Property Accessors }
    function Get_Paygroup(Index: Integer): IXMLPaygroupType;
    { Methods & Properties }
    function Add: IXMLPaygroupType;
    function Insert(const Index: Integer): IXMLPaygroupType;
    property Paygroup[Index: Integer]: IXMLPaygroupType read Get_Paygroup; default;
  end;

{ IXMLPaygroupType }

  IXMLPaygroupType = interface(IXMLNode)
    ['{5F5578F1-B6B7-436D-8818-984D1833AD5A}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLAccrualgroupsType }

  IXMLAccrualgroupsType = interface(IXMLNodeCollection)
    ['{AD03DE7B-4E7B-4FCD-BD88-3E551C9877FA}']
    { Property Accessors }
    function Get_Accrual(Index: Integer): IXMLAccrualType;
    { Methods & Properties }
    function Add: IXMLAccrualType;
    function Insert(const Index: Integer): IXMLAccrualType;
    property Accrual[Index: Integer]: IXMLAccrualType read Get_Accrual; default;
  end;

{ IXMLAccrualType }

  IXMLAccrualType = interface(IXMLNode)
    ['{A6C38CE4-24A3-424C-B995-E48172104B2D}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLPointsgroupType }

  IXMLPointsgroupType = interface(IXMLNodeCollection)
    ['{791B9C11-9D31-47B7-8AD9-DBD709325D94}']
    { Property Accessors }
    function Get_Pointgroup(Index: Integer): IXMLPointgroupType;
    { Methods & Properties }
    function Add: IXMLPointgroupType;
    function Insert(const Index: Integer): IXMLPointgroupType;
    property Pointgroup[Index: Integer]: IXMLPointgroupType read Get_Pointgroup; default;
  end;

{ IXMLPointgroupType }

  IXMLPointgroupType = interface(IXMLNode)
    ['{DC0B11F3-495F-49CE-A4E4-CE825FAD0A92}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLMasterschedulesType }

  IXMLMasterschedulesType = interface(IXMLNodeCollection)
    ['{FE7E125B-9F83-4BB8-AF26-2ACB83371FFF}']
    { Property Accessors }
    function Get_Masterschedule(Index: Integer): IXMLMasterscheduleType;
    { Methods & Properties }
    function Add: IXMLMasterscheduleType;
    function Insert(const Index: Integer): IXMLMasterscheduleType;
    property Masterschedule[Index: Integer]: IXMLMasterscheduleType read Get_Masterschedule; default;
  end;

{ IXMLMasterscheduleType }

  IXMLMasterscheduleType = interface(IXMLNode)
    ['{28437C1F-F4B5-42A1-BC67-EA0BCBB4A01E}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Description: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Description(Value: WideString);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Description: WideString read Get_Description write Set_Description;
  end;

{ Forward Decls }

  TXMLEmployeesType = class;
  TXMLRolesType = class;
  TXMLRoleType = class;
  TXMLPaygroupsType = class;
  TXMLPaygroupType = class;
  TXMLAccrualgroupsType = class;
  TXMLAccrualType = class;
  TXMLPointsgroupType = class;
  TXMLPointgroupType = class;
  TXMLMasterschedulesType = class;
  TXMLMasterscheduleType = class;

{ TXMLEmployeesType }

  TXMLEmployeesType = class(TXMLNode, IXMLEmployeesType)
  protected
    { IXMLEmployeesType }
    function Get_Roles: IXMLRolesType;
    function Get_Paygroups: IXMLPaygroupsType;
    function Get_Accrualgroups: IXMLAccrualgroupsType;
    function Get_Pointsgroup: IXMLPointsgroupType;
    function Get_Supervisors: WideString;
    function Get_Masterschedules: IXMLMasterschedulesType;
    function Get_Rotationschedules: WideString;
    procedure Set_Supervisors(Value: WideString);
    procedure Set_Rotationschedules(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRolesType }

  TXMLRolesType = class(TXMLNodeCollection, IXMLRolesType)
  protected
    { IXMLRolesType }
    function Get_Role(Index: Integer): IXMLRoleType;
    function Add: IXMLRoleType;
    function Insert(const Index: Integer): IXMLRoleType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRoleType }

  TXMLRoleType = class(TXMLNode, IXMLRoleType)
  protected
    { IXMLRoleType }
    function Get_Name: WideString;
    function Get_Description: WideString;
    procedure Set_Name(Value: WideString);
    procedure Set_Description(Value: WideString);
  end;

{ TXMLPaygroupsType }

  TXMLPaygroupsType = class(TXMLNodeCollection, IXMLPaygroupsType)
  protected
    { IXMLPaygroupsType }
    function Get_Paygroup(Index: Integer): IXMLPaygroupType;
    function Add: IXMLPaygroupType;
    function Insert(const Index: Integer): IXMLPaygroupType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPaygroupType }

  TXMLPaygroupType = class(TXMLNode, IXMLPaygroupType)
  protected
    { IXMLPaygroupType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
  end;

{ TXMLAccrualgroupsType }

  TXMLAccrualgroupsType = class(TXMLNodeCollection, IXMLAccrualgroupsType)
  protected
    { IXMLAccrualgroupsType }
    function Get_Accrual(Index: Integer): IXMLAccrualType;
    function Add: IXMLAccrualType;
    function Insert(const Index: Integer): IXMLAccrualType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAccrualType }

  TXMLAccrualType = class(TXMLNode, IXMLAccrualType)
  protected
    { IXMLAccrualType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
  end;

{ TXMLPointsgroupType }

  TXMLPointsgroupType = class(TXMLNodeCollection, IXMLPointsgroupType)
  protected
    { IXMLPointsgroupType }
    function Get_Pointgroup(Index: Integer): IXMLPointgroupType;
    function Add: IXMLPointgroupType;
    function Insert(const Index: Integer): IXMLPointgroupType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPointgroupType }

  TXMLPointgroupType = class(TXMLNode, IXMLPointgroupType)
  protected
    { IXMLPointgroupType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Name(Value: WideString);
  end;

{ TXMLMasterschedulesType }

  TXMLMasterschedulesType = class(TXMLNodeCollection, IXMLMasterschedulesType)
  protected
    { IXMLMasterschedulesType }
    function Get_Masterschedule(Index: Integer): IXMLMasterscheduleType;
    function Add: IXMLMasterscheduleType;
    function Insert(const Index: Integer): IXMLMasterscheduleType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMasterscheduleType }

  TXMLMasterscheduleType = class(TXMLNode, IXMLMasterscheduleType)
  protected
    { IXMLMasterscheduleType }
    function Get_Id: WideString;
    function Get_Description: WideString;
    procedure Set_Id(Value: WideString);
    procedure Set_Description(Value: WideString);
  end;

{ Global Functions }

function Getemployees(Doc: IXMLDocument): IXMLEmployeesType;
function Loademployees(const FileName: WideString): IXMLEmployeesType;
function Newemployees: IXMLEmployeesType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getemployees(Doc: IXMLDocument): IXMLEmployeesType;
begin
  Result := Doc.GetDocBinding('employees', TXMLEmployeesType, TargetNamespace) as IXMLEmployeesType;
end;

function Loademployees(const FileName: WideString): IXMLEmployeesType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('employees', TXMLEmployeesType, TargetNamespace) as IXMLEmployeesType;
end;

function Newemployees: IXMLEmployeesType;
begin
  Result := NewXMLDocument.GetDocBinding('employees', TXMLEmployeesType, TargetNamespace) as IXMLEmployeesType;
end;

{ TXMLEmployeesType }

procedure TXMLEmployeesType.AfterConstruction;
begin
  RegisterChildNode('roles', TXMLRolesType);
  RegisterChildNode('paygroups', TXMLPaygroupsType);
  RegisterChildNode('accrualgroups', TXMLAccrualgroupsType);
  RegisterChildNode('pointsgroup', TXMLPointsgroupType);
  RegisterChildNode('masterschedules', TXMLMasterschedulesType);
  inherited;
end;

function TXMLEmployeesType.Get_Roles: IXMLRolesType;
begin
  Result := ChildNodes['roles'] as IXMLRolesType;
end;

function TXMLEmployeesType.Get_Paygroups: IXMLPaygroupsType;
begin
  Result := ChildNodes['paygroups'] as IXMLPaygroupsType;
end;

function TXMLEmployeesType.Get_Accrualgroups: IXMLAccrualgroupsType;
begin
  Result := ChildNodes['accrualgroups'] as IXMLAccrualgroupsType;
end;

function TXMLEmployeesType.Get_Pointsgroup: IXMLPointsgroupType;
begin
  Result := ChildNodes['pointsgroup'] as IXMLPointsgroupType;
end;

function TXMLEmployeesType.Get_Supervisors: WideString;
begin
  Result := ChildNodes['supervisors'].Text;
end;

procedure TXMLEmployeesType.Set_Supervisors(Value: WideString);
begin
  ChildNodes['supervisors'].NodeValue := Value;
end;

function TXMLEmployeesType.Get_Masterschedules: IXMLMasterschedulesType;
begin
  Result := ChildNodes['masterschedules'] as IXMLMasterschedulesType;
end;

function TXMLEmployeesType.Get_Rotationschedules: WideString;
begin
  Result := ChildNodes['rotationschedules'].Text;
end;

procedure TXMLEmployeesType.Set_Rotationschedules(Value: WideString);
begin
  ChildNodes['rotationschedules'].NodeValue := Value;
end;

{ TXMLRolesType }

procedure TXMLRolesType.AfterConstruction;
begin
  RegisterChildNode('role', TXMLRoleType);
  ItemTag := 'role';
  ItemInterface := IXMLRoleType;
  inherited;
end;

function TXMLRolesType.Get_Role(Index: Integer): IXMLRoleType;
begin
  Result := List[Index] as IXMLRoleType;
end;

function TXMLRolesType.Add: IXMLRoleType;
begin
  Result := AddItem(-1) as IXMLRoleType;
end;

function TXMLRolesType.Insert(const Index: Integer): IXMLRoleType;
begin
  Result := AddItem(Index) as IXMLRoleType;
end;

{ TXMLRoleType }

function TXMLRoleType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLRoleType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

function TXMLRoleType.Get_Description: WideString;
begin
  Result := ChildNodes['description'].Text;
end;

procedure TXMLRoleType.Set_Description(Value: WideString);
begin
  ChildNodes['description'].NodeValue := Value;
end;

{ TXMLPaygroupsType }

procedure TXMLPaygroupsType.AfterConstruction;
begin
  RegisterChildNode('paygroup', TXMLPaygroupType);
  ItemTag := 'paygroup';
  ItemInterface := IXMLPaygroupType;
  inherited;
end;

function TXMLPaygroupsType.Get_Paygroup(Index: Integer): IXMLPaygroupType;
begin
  Result := List[Index] as IXMLPaygroupType;
end;

function TXMLPaygroupsType.Add: IXMLPaygroupType;
begin
  Result := AddItem(-1) as IXMLPaygroupType;
end;

function TXMLPaygroupsType.Insert(const Index: Integer): IXMLPaygroupType;
begin
  Result := AddItem(Index) as IXMLPaygroupType;
end;

{ TXMLPaygroupType }

function TXMLPaygroupType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLPaygroupType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLPaygroupType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLPaygroupType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

{ TXMLAccrualgroupsType }

procedure TXMLAccrualgroupsType.AfterConstruction;
begin
  RegisterChildNode('accrual', TXMLAccrualType);
  ItemTag := 'accrual';
  ItemInterface := IXMLAccrualType;
  inherited;
end;

function TXMLAccrualgroupsType.Get_Accrual(Index: Integer): IXMLAccrualType;
begin
  Result := List[Index] as IXMLAccrualType;
end;

function TXMLAccrualgroupsType.Add: IXMLAccrualType;
begin
  Result := AddItem(-1) as IXMLAccrualType;
end;

function TXMLAccrualgroupsType.Insert(const Index: Integer): IXMLAccrualType;
begin
  Result := AddItem(Index) as IXMLAccrualType;
end;

{ TXMLAccrualType }

function TXMLAccrualType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLAccrualType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLAccrualType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLAccrualType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

{ TXMLPointsgroupType }

procedure TXMLPointsgroupType.AfterConstruction;
begin
  RegisterChildNode('pointgroup', TXMLPointgroupType);
  ItemTag := 'pointgroup';
  ItemInterface := IXMLPointgroupType;
  inherited;
end;

function TXMLPointsgroupType.Get_Pointgroup(Index: Integer): IXMLPointgroupType;
begin
  Result := List[Index] as IXMLPointgroupType;
end;

function TXMLPointsgroupType.Add: IXMLPointgroupType;
begin
  Result := AddItem(-1) as IXMLPointgroupType;
end;

function TXMLPointsgroupType.Insert(const Index: Integer): IXMLPointgroupType;
begin
  Result := AddItem(Index) as IXMLPointgroupType;
end;

{ TXMLPointgroupType }

function TXMLPointgroupType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLPointgroupType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLPointgroupType.Get_Name: WideString;
begin
  Result := ChildNodes['name'].Text;
end;

procedure TXMLPointgroupType.Set_Name(Value: WideString);
begin
  ChildNodes['name'].NodeValue := Value;
end;

{ TXMLMasterschedulesType }

procedure TXMLMasterschedulesType.AfterConstruction;
begin
  RegisterChildNode('masterschedule', TXMLMasterscheduleType);
  ItemTag := 'masterschedule';
  ItemInterface := IXMLMasterscheduleType;
  inherited;
end;

function TXMLMasterschedulesType.Get_Masterschedule(Index: Integer): IXMLMasterscheduleType;
begin
  Result := List[Index] as IXMLMasterscheduleType;
end;

function TXMLMasterschedulesType.Add: IXMLMasterscheduleType;
begin
  Result := AddItem(-1) as IXMLMasterscheduleType;
end;

function TXMLMasterschedulesType.Insert(const Index: Integer): IXMLMasterscheduleType;
begin
  Result := AddItem(Index) as IXMLMasterscheduleType;
end;

{ TXMLMasterscheduleType }

function TXMLMasterscheduleType.Get_Id: WideString;
begin
  Result := ChildNodes['id'].Text;
end;

procedure TXMLMasterscheduleType.Set_Id(Value: WideString);
begin
  ChildNodes['id'].NodeValue := Value;
end;

function TXMLMasterscheduleType.Get_Description: WideString;
begin
  Result := ChildNodes['description'].Text;
end;

procedure TXMLMasterscheduleType.Set_Description(Value: WideString);
begin
  ChildNodes['description'].NodeValue := Value;
end;

end.