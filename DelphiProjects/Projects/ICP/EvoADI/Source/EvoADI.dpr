program EvoADI;

uses                           
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  evoapiconnection,
  common,
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  timeclockimport in '..\..\common\timeclockimport.pas',
  EvoTCImportMainNewForm in '..\..\common\EvoTCImportMainNewForm.pas' {EvoTCImportMainNewFm: TEvoAPIClientMainFm},
  aditime in 'aditime.pas',
  bndRetrieveAllPayTypes in 'xml\bndRetrieveAllPayTypes.pas',
  adidecl in 'adidecl.pas',
  adiconnection in 'adiconnection.pas',
  bndEmployeeStatuses in 'xml\bndEmployeeStatuses.pas',
  bndRetrieveSettings in 'xml\bndRetrieveSettings.pas',
  bndRetrieveAllEmployeescodes in 'xml\bndRetrieveAllEmployeescodes.pas',
  FieldsToExportFrame in 'FieldsToExportFrame.pas' {FieldsToExportFrm: TFrame},
  ADITCOptionsFrame in 'ADITCOptionsFrame.pas' {ADITCOptionsFrm: TFrame},
  bndRetrieveAllPayGroups in 'xml\bndRetrieveAllPayGroups.pas',
  bndRetrieveAllUsers in 'xml\bndRetrieveAllUsers.pas',
  SVSelectionFrame in 'SVSelectionFrame.pas' {SVSelectionFrm: TFrame},
  bndRetrieveAllEmployees in 'xml\bndRetrieveAllEmployees.pas',
  bndStateMasters in 'xml\bndStateMasters.pas',
  CustomBinderBaseFrame in '..\..\common\CustomBinderBaseFrame.pas' {CustomBinderBaseFrm: TFrame},
  BinderFrame in '..\..\common\BinderFrame.pas' {BinderFrm: TFrame},
  EeStatusFrame in 'EeStatusFrame.pas' {EeStatusFrm: TCustomBinderBaseFrm},
  DBDTFrame in 'DBDTFrame.pas' {DBDTFrm: TCustomBinderBaseFrm},
  bndRequestPayrollData in 'xml\bndRequestPayrollData.pas',
  userActionHelpers in '..\..\common\userActionHelpers.pas',
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  EvoData in '..\..\common\EvoData.pas',
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvolutionPrPrBatchFrame in '..\..\common\EvolutionPrPrBatchFrame.pas' {EvolutionPrPrBatchFrm: TFrame},
  MidasLib,
  ADIConnectionParamFrame in 'ADIConnectionParamFrame.pas' {ADIConnectionParamFrm: TFrame},
  MainForm in 'MainForm.pas' {MainFm},
  adicache in 'adicache.pas',
  PerCompanyADIClientFrame in 'PerCompanyADIClientFrame.pas' {PerCompanyADIClientFrm: TFrame},
  ADIEEOptionsFrame in 'ADIEEOptionsFrame.pas' {ADIEEOptionsFrm: TFrame},
  EvoWaitForm in '..\..\common\EvoWaitForm.pas' {EvoWaitFm},
  PlannedActionConfirmationForm in '..\..\common\PlannedActionConfirmationForm.pas' {ConfirmationFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  scheduler in '..\..\common\scheduler.pas',
  aditasks in 'aditasks.pas',
  scheduledTask in '..\..\common\scheduledTask.pas',
  EEFilterFrame in '..\..\common\EEFilterFrame.pas' {EeFilterFrm: TFrame},
  CodesFilterFrame in '..\..\common\CodesFilterFrame.pas' {CodesFilterFrm: TFrame},
  EESalaryFilterFrame in '..\..\common\EESalaryFilterFrame.pas' {EESalaryFilterFrm: TFrame},
  dbdtDS in '..\..\common\dbdtDS.pas',
  DBDTFilterFrame in '..\..\common\DBDTFilterFrame.pas' {DBDTFilterFrm: TFrame},
  EEFilter in '..\..\common\EEFilter.pas',
  RateImportOptionFrame in '..\..\common\RateImportOptionFrame.pas' {RateImportOptionFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  TCImportDialog in 'TCImportDialog.pas' {TCImportDlg: TFrame},
  EEExportDialog in 'EEExportDialog.pas' {EEExportDlg: TFrame},
  bndRaceCodes in 'xml\bndRaceCodes.pas',
  bndUpdateEmployee in 'xml\bndUpdateEmployee.pas',
  EthnicityFrame in 'EthnicityFrame.pas' {EthnicityFrm: TFrame},
  api in 'api.pas',
  bndRetrieveAllAccrualTypes in 'xml\bndRetrieveAllAccrualTypes.pas',
  TOAFrame in 'TOAFrame.pas' {TOAFrm: TFrame},
  bndRetrieveAllPayCalendar in 'xml\bndRetrieveAllPayCalendar.pas';

{$R *.res}

begin
  LicenseKey := 'EF34EB4164834C4A94CF10FCE89AE3D3'; //advanced evo adi

  ConfigVersion := 2;
  FeatureSetName := 'Advanced';

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution ADI Import';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
  Application.Run;
  end
  else
  begin
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TADITaskAdapter.Create));
  end
end.
