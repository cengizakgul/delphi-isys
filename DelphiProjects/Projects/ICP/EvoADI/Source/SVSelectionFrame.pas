unit SVSelectionFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs,
  kbmMemTable, aditime, Grids, DBGrids, DB,
  Wwdbigrd, Wwdbgrid, dbcomp, StdCtrls, wwdblook, gdystrset, evodata, gdycommonlogger;

type
  TSVSelectionFrm = class(TFrame)
    dsSV: TDataSource;
    dsEE: TDataSource;
    dgEE: TReDBGrid;
    dbcbUserID: TwwDBLookupCombo;
    procedure dbcbUserIDBeforeDropDown(Sender: TObject);
    procedure dbcbUserIDCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  private
    FEE: TkbmMemTable;
    FActions: TEEUpdateActionRecs;
    FAssignableSVUserIDs: TStringSet;
    FLogger: ICommonLogger;
    function GetAssignableSVUserIDs(code: string): TStringSet;
    procedure HandleFilterSV(DataSet: TDataSet; var Accept: Boolean);
  public
    destructor Destroy; override;
    procedure Init(actions: TEEUpdateActionRecs; EEData: TEvoEEData; sv: TkbmCustomMemTable; logger: ICommonLogger);
    procedure GetSVs(actions: TEEUpdateActionRecs);
  end;

implementation

{$R *.dfm}

uses
  common, gdydbcommonnew;

{ TSVSelectionFrm }

destructor TSVSelectionFrm.Destroy;
begin
  FreeAndNil(FEE);
  inherited;
end;

procedure TSVSelectionFrm.Init(actions: TEEUpdateActionRecs; EEData: TEvoEEData; sv: TkbmCustomMemTable; logger: ICommonLogger);
var
  i: integer;
  used: integer;
begin
  FLogger := logger;
  dsSV.DataSet := sv;

  FLogger.LogDebug('SVDataSet:', DataSetToCsv(sv));

  FEE := TkbmMemTable.Create(nil);

  CreateStringField( FEE, 'CUSTOM_EMPLOYEE_NUMBER', 'EE Code', 10);
  with CreateStringField( FEE, 'NAME', 'Name', 53) do
  begin
    DisplayWidth := 20;
  end;
  with CreateStringField( FEE, 'DBDT_CUSTOM_NUMBERS', 'D/B/D/T Numbers', 80) do
  begin
    DisplayWidth := 30;
  end;
  with CreateStringField( FEE, 'USER_ID', 'Supervisor''s User Id', SV.FieldByName('USER_ID').DataSize) do
  begin
    Visible := false;
  end;
  with CreateStringField( FEE, 'USER_NAME', 'Supervisor', SV.FieldByName('USER_NAME').DataSize) do
  begin
    FieldKind := fkLookup;
    LookupDataSet := sv;
    LookupKeyFields := 'USER_ID';
    LookupResultField := 'USER_NAME';
    KeyFields := 'USER_ID';
  end;
  FEE.Open;

  SetLength(FActions, Length(actions));
  used := 0;
  for i := 0 to high(actions) do
  begin
    if (actions[i].Action = euaCreate) and not actions[i].HasEvoSV then
    begin
      Assert(EEData.LocateEE(actions[i].Code));
      FActions[used] := actions[i];
      used := used+1;

      FEE.Append;
      try
        FEE['CUSTOM_EMPLOYEE_NUMBER'] := EEData.EEs['CUSTOM_EMPLOYEE_NUMBER'];
        FEE['NAME'] := trim(trim(EEData.EEs.FieldByName('LAST_NAME').AsString) + ' ' + trim(EEData.EEs.FieldByName('FIRST_NAME').AsString) + ' ' + trim(EEData.EEs.FieldByName('MIDDLE_INITIAL').AsString));
        FEE['DBDT_CUSTOM_NUMBERS'] := EEData.GetEEDBDTCustomNumbers;
        FEE['USER_ID'] := Null;
        FEE.Post;
      except
        FEE.Cancel;
        raise
      end;
    end;
  end;
  SetLength(FActions, used);

  FEE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').ReadOnly := true;
  FEE.FieldByName('NAME').ReadOnly := true;
  FEE.FieldByName('DBDT_CUSTOM_NUMBERS').ReadOnly := true;

  dbcbUserID.LookupTable := SV;
  dbcbUserID.LookupField := 'USER_ID';
  dbcbUserID.Selected.Clear;
  AddSelected(dbcbUserID.Selected, SV.FieldByName('USER_NAME') );
  AddSelected(dbcbUserID.Selected, SV.FieldByName('USER_ID') );

  dgEE.SetControlType('USER_NAME', fctCustom, 'dbcbUserID');

  FEE.AddIndex('SORT', 'CUSTOM_EMPLOYEE_NUMBER', [ixCaseInsensitive]);
  FEE.IndexName := 'SORT';
  FEE.IndexDefs.Update;


  dsSV.DataSet.OnFilterRecord := HandleFilterSV;
  dsEE.DataSet := FEE;
end;

procedure TSVSelectionFrm.GetSVs(actions: TEEUpdateActionRecs);
var
  i: integer;
begin
  if FEE.State = dsEdit then
    FEE.Post;
  Assert(FEE.State = dsBrowse);
  FEE.Filtered := false; //user filter may be set by grid
//  FEE.UserFiltered := false; //if it were TISkbmDataSet
  FLogger.LogDebug('GetSVs: EE dataset', DataSetToCsv(FEE));
  for i := 0 to high(actions) do
  begin
    if (actions[i].Action = euaCreate) and not actions[i].HasEvoSV then
    begin
      actions[i].SVUserID := '';
      FEE.First;
      while not FEE.Eof do
      begin
        if trim(FEE['CUSTOM_EMPLOYEE_NUMBER']) = trim(actions[i].code) then
        begin
          actions[i].SVUserID := FEE.FieldByName('USER_ID').AsString;
          break;
        end;
        FEE.Next;
      end;
      Assert(not FEE.Eof);
    end
  end;
end;

function TSVSelectionFrm.GetAssignableSVUserIDs(code: string): TStringSet;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to high(FActions) do
    if trim(FActions[i].Code) = trim(code) then
    begin
      Result := FActions[i].AssignableSVUserIDs;
      exit;
    end;
  Assert(false);
end;

procedure TSVSelectionFrm.dbcbUserIDBeforeDropDown(Sender: TObject);
begin
  Assert(FAssignableSVUserIDs = nil);
  FAssignableSVUserIDs := GetAssignableSVUserIDs(FEE['CUSTOM_EMPLOYEE_NUMBER']);
  dsSV.DataSet.Filtered := true;
  FLogger.LogDebug(Format('Filtered = true, EE = %s, FAssignableSVUserIDs=%s', [VarToStr(FEE['CUSTOM_EMPLOYEE_NUMBER']), SetToStr(FAssignableSVUserIDs,';')]));
end;

procedure TSVSelectionFrm.dbcbUserIDCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  dsSV.DataSet.Filtered := false;
  FAssignableSVUserIDs := nil;
  FLogger.LogDebug(Format('Filtered = false, EE = %s', [VarToStr(FEE['CUSTOM_EMPLOYEE_NUMBER'])]));
end;

procedure TSVSelectionFrm.HandleFilterSV(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := InSet(DataSet['USER_ID'], FAssignableSVUserIDs);
  FLogger.LogDebug(Format('Filter: userid=%s, FAssignableSVUserIDs = %s, accepted=%s', [VarToStr(DataSet['USER_ID']), SetToStr(FAssignableSVUserIDs,';'), BoolToStr(Accept, true)]));
end;

end.
