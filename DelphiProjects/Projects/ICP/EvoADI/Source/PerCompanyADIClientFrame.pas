unit PerCompanyADIClientFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, StdCtrls, DBCtrls, DBClient, ExtCtrls;

type
  TADIClientChangedEvent = procedure(oldguid, guid: string) of object;

  TPerCompanyADIClientFrm = class(TFrame)
    cdPerCompany: TClientDataSet;
    dsConn2: TDataSource;
    cdPerCompanyGUID: TStringField;
    dsPerCompany: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    DBLookupComboBox1: TDBLookupComboBox;
    DataSource1: TDataSource;
    procedure dsConn2DataChange(Sender: TObject; Field: TField);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
  private
    FOldGUID: string;
    FOnADIClientChanged: TADIClientChangedEvent;

    function GetGUID: string;
    procedure SetGUID(const Value: string);
    procedure DoSetGUID(const Value: string);
  public
    constructor Create(Owner: TComponent); override;
    procedure Init(ds: TDataSet);
    procedure Uninit;

    property GUID: string read GetGUID write SetGUID;
    property OnADIClientChanged: TADIClientChangedEvent read FOnADIClientChanged write FOnADIClientChanged;
  end;

implementation

{$R *.dfm}

uses
  gdyCommon;

{ TPerCompanyADIClientFrm }

constructor TPerCompanyADIClientFrm.Create(Owner: TComponent);
begin
  inherited;
  cdPerCompany.CreateDataSet;
  cdPerCompany.Open;
  cdPerCompany.Append;
  cdPerCompany.Post;
  cdPerCompany.First;
  EnableControl(DBLookupComboBox1, false);
end;

procedure TPerCompanyADIClientFrm.Init(ds: TDataSet);
begin
  dsConn2.DataSet := ds;
  EnableControl(DBLookupComboBox1, true);
end;

procedure TPerCompanyADIClientFrm.Uninit;
begin
  dsConn2.DataSet := nil;
  EnableControl(DBLookupComboBox1, false);
end;

procedure TPerCompanyADIClientFrm.DoSetGUID(const Value: string);
begin
  cdPerCompany.Edit;
  try
    cdPerCompanyGUID.Value := Value;
    cdPerCompany.Post;
  except
    cdPerCompany.Cancel;
    raise;
  end;
end;

procedure TPerCompanyADIClientFrm.SetGUID(const Value: string);
begin
  if VarIsNull(dsConn2.DataSet.LookUp('GUID', Value, 'GUID')) then
    DoSetGUID('')
  else
    DoSetGUID(Value);
end;

function TPerCompanyADIClientFrm.GetGUID: string;
begin
  Result := trim(cdPerCompanyGUID.Value);
end;

procedure TPerCompanyADIClientFrm.dsConn2DataChange(Sender: TObject;
  Field: TField);
begin
  if dsConn2.DataSet.State = dsBrowse then
    if VarIsNull(dsConn2.DataSet.LookUp('GUID', cdPerCompanyGUID.Value, 'GUID')) then
      DoSetGUID('');
end;

procedure TPerCompanyADIClientFrm.DataSource1DataChange(Sender: TObject;
  Field: TField);
var
  old: string;
begin
  if FoldGUID <> GUID then
  begin
    old := FOldGUID;
    FOldGUID := GUID;
    if assigned(FOnADIClientChanged) then
      FOnADIClientChanged(old, GUID);
  end;
end;

end.
