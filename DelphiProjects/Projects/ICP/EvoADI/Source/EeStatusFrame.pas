unit EeStatusFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BinderFrame, gdyBinder, DB, DBClient, bndEmployeeStatuses,
  CustomBinderBaseFrame;

type
  TEeStatusFrm = class(TCustomBinderBaseFrm)
    cdEvoEEStatuses: TClientDataSet;
    cdEvoEEStatusesCODE: TStringField;
    cdEvoEEStatusesNAME: TStringField;
    cdADIEEStatuses: TClientDataSet;
    cdADIEEStatusesCODE: TStringField;
    cdADIEEStatusesDESCRIPTION: TStringField;
    cdADIEEStatusesSTATUSCLASS: TStringField;
  private
    procedure InitEvoEEStatuses;
    procedure InitADIEEStatuses(statuses: bndEmployeeStatuses.IXMLEmployeestatusesType);
  public
    procedure Init(matchTableContent: string; statuses: bndEmployeeStatuses.IXMLEmployeestatusesType);
  end;

implementation

{$R *.dfm}
uses
  gdyClasses, evConsts, common, adidecl, comboChoices;

{ TEeStatusFrm }

procedure TEeStatusFrm.Init(matchTableContent: string; statuses: bndEmployeeStatuses.IXMLEmployeestatusesType);
begin
  try
    InitADIEEStatuses(statuses);
    InitEvoEEStatuses;

    FBindingKeeper := CreateBindingKeeper( EEStatusBinding, TClientDataSet );
    FBindingKeeper.SetTables(cdEvoEEStatuses, cdADIEEStatuses);
    FBindingKeeper.SetVisualBinder(BinderFrm1);
    FBindingKeeper.SetMatchTableFromString(matchTableContent);
    FBindingKeeper.SetConnected(true);
  except
    UnInit;
    raise;
  end;
end;

procedure TEeStatusFrm.InitADIEEStatuses(statuses: IXMLEmployeestatusesType);
var
  i: integer;
begin
  CreateOrEmptyDataSet( cdADIEEStatuses );
  for i := 0 to statuses.Count-1 do
    Append(cdADIEEStatuses, 'CODE;DESCRIPTION;STATUSCLASS', [statuses[i].Code, statuses[i].Description, statuses[i].Statusclass.Name])
end;

procedure TEeStatusFrm.InitEvoEEStatuses;
var
  lines: IStr;
  i: integer;
begin
  CreateOrEmptyDataSet( cdEvoEEStatuses );
  lines := SplitToLines(EE_TerminationCode_ComboChoices);
  for i := 0 to lines.Count-1 do
    with SplitByChar(lines.Str[i], #9) do
      Append(cdEvoEEStatuses, 'CODE;NAME', [Str[1], Str[0]])
end;

end.



