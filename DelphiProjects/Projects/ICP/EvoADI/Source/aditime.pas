unit aditime;

interface

uses
  gdyCommonLogger, timeclockimport, evoapiconnectionutils,
  evoapiconnection, adidecl, adiconnection, gdystrset,
  bndUpdateEmployee, bndRetrieveAllUsers, bndEmployeeStatuses,
  bndRetrieveSettings, bndRetrieveAllEmployees, bndRequestPayrollData,
  bndRetrieveAllPayTypes, bndRetrieveAllEmployeescodes, bndStateMasters, bndRaceCodes,
  kbmMemTable, common, gdyClasses, gdyBinder, evodata, eefilter,
  bndRetrieveAllPayCalendar, bndRetrieveAllPayGroups;

type
  TADIDictionaries = class
  public
    constructor Create(conn: IADIConnection; logger: ICommonLogger);
    function DefAccrualGroup: string;
    function DefPointsGroup: string;

    //!! alternatively I could use attributes of employees tags, that would be cleaner
    function GetStateAbbrByNullableGUID(v: Variant): string;
    function GetStatusCodeByGUID(guid: string): string;
    function GetRaceCodeByNullableGUID(v: Variant): string;

    function GetStatusClassByStatusCode(code: string): integer;
    function GetPayPeriodStartDate(guid: string; dt: TDateTime): TDateTime;
  private
    FConn: IADIConnection;
    FLogger: ICommonLogger;

    FDefAccrualGroup: Variant;
    FDefPointsGroup: Variant;

    FStatuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
    FGroups: bndRetrieveAllEmployeescodes.IXMLEmployeesType;
    FStates: bndStateMasters.IXMLStatemastersType;
    FRaces: bndRaceCodes.IXMLRacecodesType;
    FCalendars: array [2000..2099] of bndRetrieveAllPayCalendar.IXMLPayCalendarsType; //100 years is enough for everyone
    FPayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;

    function Statuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
    function States: bndStateMasters.IXMLStatemastersType;
    function Races: bndRaceCodes.IXMLRacecodesType;
    function Calendars(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;
    function PayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;

    function GuessDefaultAccrualGroup: variant;
    function GuessDefaultPointsGroup: variant;

    function GetPayGroupByGUID(guid: string): bndRetrieveAllPayGroups.IXMLPaygroupType;
    function FindPayCalendar(guid: string;
      year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarType;
  end;

  IADIHierarchy = interface
    procedure FilterEEsByCompany(company: TEvoCompanyDef; adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType);
    function GetGUIDByCode(level: TADILevel; code: string): string;
    function GetCodeByGUID(level: TADILevel; guid: string): string;
    function LevelInfo(level: TADILevel): THierarchyLevelInfo;
  end;

  TADIHierarchy = class(TInterfacedObject, IADIHierarchy)
  public
    constructor Create(conn: IADIConnection; logger: ICommonLogger);
  private
  	FLogger: ICommonLogger;
    FLevels: array [TADILevel] of TADILevelRec;
  private
    {IADIHierarchy}
    procedure FilterEEsByCompany(company: TEvoCompanyDef; adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType);
    function GetGUIDByCode(level: TADILevel; code: string): string;
    function GetCodeByGUID(level: TADILevel; guid: string): string;
    function LevelInfo(level: TADILevel): THierarchyLevelInfo;
  end;

  TEEUpdateAction = (euaCreate, euaUpdate, euaDelete);

  TChangedFieldRec = record
    FieldName: string;
    EvoValue: string;
    ADIValue: string;
  end;

  TChangedFieldRecs = array of TChangedFieldRec;

  TEEUpdateActionRec = record
    Action: TEEUpdateAction;
    Code: string;
    ChangedFields: TChangedFieldRecs;

    //the fields below are meaningful only for Create action
    //and useful only when ADI supervisors are enabled
    HasEvoSV: boolean;
    //if HasEvoSV is false then (when this record is passed to Apply()) SVUserID may, or may not,
    //contain userid of an ADI supervisor chosen by a user
    AssignableSVUserIDs: TStringSet;
    SVUserID: string;
  end;

  TEEUpdateActionRecs = array of TEEUpdateActionRec;

  TAnalyzeResult = record
    Errors: integer;
    Actions: TEEUpdateActionRecs;
    ADIHasSupervisors: boolean;
  end;

  TADIEmployeeUpdater = class
  public
    constructor Create(conn: IADIConnection; const adiOptions: TADIEEOptions; EEFilter: TEEFilter; EEData: TEvoEEData; DBDTMatcher, EEStatusMatcher, EthnicityMatcher: IMatcher; logger: ICommonLogger);
    destructor Destroy; override;

    function Analyze: TAnalyzeResult;
    function CreateSVDataSet: TkbmCustomMemTable;
    function Apply(recs: TEEUpdateActionRecs; pi: IProgressIndicator): TUpdateEmployeesStat;

  private
    function BuildEmployee(adiEECode: string; creating: boolean): bndUpdateEmployee.IXMLEmployeeType;
    procedure AddDefaultValues(adiEE: bndUpdateEmployee.IXMLEmployeeType; userChosenSV: string );
    function GetAssignableSVUserIDs: TStringSet;
    function ADIHasSupervisors: boolean;
    function MakeFakeSSN(eecode: string): string;
    function MapEvoEEStatus(st: string): Variant;
  private
    FLogger: ICommonLogger;
    FADIOptions: TADIEEOptions;
    FEEData: TEvoEEData;
    FConn: IADIConnection;
    FDict: TADIDictionaries;
    FDBDTMatcher: IMatcher;
    FEEStatusMatcher: IMatcher;
    FEthnicityMatcher: IMatcher;
    FEEFilter: TEEFilter;

  private
    FADISettings: bndRetrieveSettings.IXMLSettingsType;
    FUsers: bndRetrieveAllUsers.IXMLUsersType;
    FHierarchy: IADIHierarchy;
    function ADISettings: bndRetrieveSettings.IXMLSettingsType;
    function Users: bndRetrieveAllUsers.IXMLUsersType;
    function Hierarchy: IADIHierarchy;
    function MapADILevelToEvoFieldName(adilevel: TADILevel): string;
  end;

function GetTimeClockData(logger: ICommonLogger; conn: IADIConnection; const options: TADITCOptions; company: TEvoCompanyDef; period: TPayPeriod; DBDTMatcher: IMatcher): TTimeClockImportData;

function EEUpdateActionRecsToString(recs: TEEUpdateActionRecs): string;

function ExportTOA(conn: IADIConnection; evoConn: IEvoAPIConnection; TOAMatcher: IMatcher; company: TEvoCompanyDef; logger: ICommonLogger; pi: IProgressIndicator): TTOAStat;

implementation

uses
  gdycommon, gdyRedir, XMLIntf,
  sysutils, gdyGlobalWaitIndicator, variants,
  EvConsts, db, StrUtils, RateImportOptionFrame, combochoices, dateutils, math;

function IsSV(user: bndRetrieveAllUsers.IXMLUserType): boolean;
begin
  //User Type check removed by Penn Gaines's request
  Result := {((user.Supervisortype.NodeValue = 2) or (user.Supervisortype.NodeValue = 3))
    and} (user.Isactive.NodeValue = 1);
end;

function FindSVUserIdByEvoName(users: bndRetrieveAllUsers.IXMLUsersType; evoSupervisorName: string): string;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
begin
  for i := 0 to users.Count-1 do
  begin
    user := users[i];
    if IsSV(user) then
    begin
      if AnsiSameText(trim(user.Username), trim(evoSupervisorName)) then
      begin
        Result := user.Userid; //not supervisorid!
        Exit;
      end
    end
  end;
  raise Exception.CreateFmt('Cannot find ADI supervisor with user name <%s>', [evoSupervisorName]);
end;

function GetSVCodeByNullableGuid(users: bndRetrieveAllUsers.IXMLUsersType; v: Variant): string;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
  guid: string;
begin
  guid := trim(VarToStr(v));
  if guid = '' then
  begin
    Result := '';
    exit;
  end;
  for i := 0 to users.Count-1 do
  begin
    user := users[i];
    if AnsiSameText(trim(user.Id), guid) then
    begin
      Result := user.Userid; //not supervisorid!
      Exit;
    end
  end;
  raise Exception.CreateFmt('Cannot find ADI supervisor by internal id (%s)', [guid]);
end;

{ TADIConnectionHierachy }

constructor TADIHierarchy.Create(conn: IADIConnection; logger: ICommonLogger);
var
  i: TADILevel;
begin
  FLogger := logger;
  for i := low(TADILevel) to high(TADILevel) do
    FLevels[i] := conn.RetrieveAllLevels(i);
end;

procedure TADIHierarchy.FilterEEsByCompany(company: TEvoCompanyDef; adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType);
var
  i: integer;
  companyGuid: string;
begin
  FLogger.LogEntry('Filtering employees by company');
  try
    try
      LogEvoCompanyDef(FLogger, company);
      companyGuid := GetGUIDByCode(adiCompany, company.CUSTOM_COMPANY_NUMBER);
      FLogger.LogContextItem('ADI company''s GUID', companyGuid);
      i := 0;
      while i < adiEEs.Count do
        if AnsiSameText(trim(adiEEs[i].Company), companyGuid) then
          i := i + 1
        else
          adiEEs.Delete(i);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIHierarchy.GetGUIDByCode(level: TADILevel; code: string): string;
var
  i: integer;
  lev: IXMLNodeList;
begin
  Result := '';
  code := trim(code);
  if FLevels[level].Data = nil then
    raise Exception.CreateFmt('ADI %s were not retrieved. ADI response was %s', [Plural(FLevels[level].Name), FLevels[level].DisabledReason]);
  if FLevels[level].Data.DocumentElement <> nil then //ADI sometimes returns empty documents instead of empty top-level tag
  begin
    lev := FLevels[level].Data.DocumentElement.ChildNodes;
    for i := 0 to lev.Count-1 do
    begin
      if trim(lev[i].ChildValues[FLevels[level].Name + 'id']) = code then
      begin
        Result := AnsiLowerCase(trim( lev[i].ChildValues['id'] ));
        exit;
      end
    end;
  end;
  raise Exception.CreateFmt('Cannot find ADI %s with code <%s>', [FLevels[level].Name, code]);
end;

function TADIHierarchy.GetCodeByGUID(level: TADILevel; guid: string): string;
var
  i: integer;
  lev: IXMLNodeList;
begin
  Result := '';
  guid := trim(guid);
  if FLevels[level].Data = nil then
    raise Exception.CreateFmt('ADI %s were not retrieved. ADI response was <%s>', [Plural(FLevels[level].Name), FLevels[level].DisabledReason]);
  if FLevels[level].Data.DocumentElement <> nil then //ADI sometimes returns empty documents instead of empty top-level tag
  begin
    lev := FLevels[level].Data.DocumentElement.ChildNodes;
    for i := 0 to lev.Count-1 do
    begin
      if AnsiSameText(trim(lev[i].ChildValues['id']), guid) then
      begin
        Result := trim( lev[i].ChildValues[FLevels[level].Name+'id'] );
        exit;
      end
    end;
  end;
  raise Exception.CreateFmt('Cannot find ADI %s with id <%s>', [FLevels[level].Name, guid]);
end;

function TADIHierarchy.LevelInfo(level: TADILevel): THierarchyLevelInfo;
begin
  Result.Name := FLevels[level].Name;
  Result.Enabled := FLevels[level].Data <> nil;
  Result.Reason := FLevels[level].DisabledReason;
end;

function GetTimeClockData(logger: ICommonLogger; conn: IADIConnection; const options: TADITCOptions; company: TEvoCompanyDef; period: TPayPeriod; DBDTMatcher: IMatcher): TTimeClockImportData;

  function ConvertToEvoTC(data: bndRequestPayrollData.IXMLResponseType; paytypes: IXMLPaytypesType ): TTimeClockRecs;
    function PayTypeByGUID(guid: string; code: string): IXMLPaytypeType;
    var
      i: integer;
    begin
      guid := trim(guid);
      for i := 0 to paytypes.Count-1 do
        if AnsiSameText(trim(paytypes[i].Id), guid) then
        begin
          Result := paytypes[i];
          exit;
        end;
      raise Exception.CreateFmt('Cannot find ADI pay type <%s> with id <%s> in the list of ADI pay types',[code, guid]);
    end;

    var
      segment: IXMLPayrollsegmentType;
      cur: PTimeClockRec;

    procedure TransferHierarchy(adilevel: TADILevel);
    var
      evolevel: Variant;
      adifn: string;
      val: string;
      sEvoLevel: string;
    begin
      adifn := ADILevelNames[TADILevel(adilevel)] + 'key';
      if segment.ChildNodes.FindNode(adifn) <> nil then
      begin
        evolevel := DBDTMatcher.LeftMatch(adilevel);
        if not VarIsNull(evolevel) then
        begin
          sEvoLevel := VarToStr(evolevel);
          Assert( Length(sEvoLevel) = 1 );
          Assert( ((sEvolevel[1] >= CLIENT_LEVEL_DIVISION) and (sEvoLevel[1] <= CLIENT_LEVEL_TEAM)) or (sEvoLevel = CLIENT_LEVEL_X_JOB));
          val := VarToStr(segment.ChildNodes[adifn].Attributes['id']);
          case sEvoLevel[1] of
            CLIENT_LEVEL_DIVISION: cur.Division := val;
            CLIENT_LEVEL_BRANCH: cur.Branch := val;
            CLIENT_LEVEL_DEPT: cur.Department := val;
            CLIENT_LEVEL_TEAM: cur.Team := val;
            CLIENT_LEVEL_X_JOB: cur.Job := val;
          else
            Assert(false);
          end;
        end;
      end;
    end;

  var
    i: integer;
    used: integer;
    segments: IXMLPayrollsegmentsType;
    pt: IXMLPaytypeType;
  begin
    segments := data.Payrollsegments;
    SetLength(Result, segments.Count); //new elements are initialized with zeros
    used := 0;
    for i := 0 to segments.Count-1 do
    begin
      logger.LogEntry('Converting ADI payroll segment to Evolution format');
      try
        try
          segment := segments[i];
          logger.LogContextItem('Payroll segment key', segment.payrollsegmentkey);
          if (segment.ChildNodes.FindNode('companykey') <> nil) and (trim(VarToStr(segment.companykey.Id)) <> trim(company.CUSTOM_COMPANY_NUMBER)) then
          begin
          	logger.LogDebug('Company mismatch');
            continue;
          end;
          logger.LogContextItem('Report date', segment.Reportdate);
          logger.LogContextItem(sCtxEECode, segment.employeekey.id);

          pt := PayTypeByGUID(segment.paytypekey.NodeValue, segment.paytypekey.id);
          if pt.Paidtime.NodeValue = 0 then
          begin
          	logger.LogDebug('Not paid time');
            continue;
          end;

          used := used + 1;
          Assert( used <= Length(Result) );
          cur := @Result[used-1];
          cur.CUSTOM_EMPLOYEE_NUMBER := trim(segment.Employeekey.Id);

          if VarHasValue(segment.ChildValues['hours']) and (abs(segment.Hours) > eps) then
          begin
            cur.Hours := segment.Hours;
            cur.EDCode := pt.hourscode;
            if options.RateImport = rateUseExternal then
              cur.Rate := segment.Basepayrate;
          end
          else
          begin
            cur.Amount := segment.Amount;
            cur.EDCode := pt.Dollarscode
          end;
          cur.PunchDate := StrToDate(segment.Reportdate);
          if trim(VarToStr(segment.startdatetime.NodeValue)) <> '' then
            cur.PunchIn := StdDateTimeToDateTime(segment.startdatetime.NodeValue);
          if trim(VarToStr(segment.Enddatetime.NodeValue)) <> '' then
            cur.PunchOut := StdDateTimeToDateTime(segment.Enddatetime.NodeValue);

          if options.TransferTempHierarchy then
          begin
            TransferHierarchy(adiLocation);
            TransferHierarchy(adiDivision);
            TransferHierarchy(adiDepartment);
            TransferHierarchy(adiPosition);
          end;
          cur.Comment := VarToStr(segment.Comments);
        except
          logger.PassthroughException;
        end;
      finally
        logger.LogExit;
      end;
    end;
    SetLength(Result, used); 
  end;

var
  recs: TTimeClockRecs;
begin
  logger.LogEntry('Getting timeclock data');
  try
    try
      LogADITCOptions(logger, options);
      Logger.LogDebug('D/B/D/T mapping', DBDTMatcher.Dump);
      recs := ConvertToEvoTC(conn.RequestPayrollData(period), conn.RetrieveAllPayTypes);
      if options.Summarize then
        recs := timeclockimport.ConsolidateTimeClockRecs(recs, logger);
      Result := BuildTimeClockImportData(recs, options.RateImport = rateUseEvoEE, false{AutoImportJobCodes}, logger); //false if rateUseExternal; it was this way before and everybody was happy...
    except
      logger.PassthroughException;
    end;
  finally
    logger.LogExit;
  end;
end;

{ TADIDictionaries }

constructor TADIDictionaries.Create(conn: IADIConnection; logger: ICommonLogger);
begin
  FConn := conn;
  FLogger := logger;
end;

function TADIDictionaries.GuessDefaultAccrualGroup: variant;
var
  accrualGroups: bndRetrieveAllEmployeescodes.IXMLAccrualgroupsType;
  i: integer;
begin
  Result := 'STD';  //cannot get any other id because id field actually contains guid here
  if FGroups = nil then
    FGroups := FConn.GetGroups;
  accrualGroups := FGroups.Accrualgroups;
  for i := 0 to accrualGroups.Count-1 do
    if AnsiSameText(trim(accrualGroups[i].Name), 'Standard Accruals') or
       AnsiSameText(trim(accrualGroups[i].Name), 'Standard Accrual') then //id is guid here so use name
     begin
       Result := trim(accrualGroups[i].Id); //!! to test
       //exit; //no warnings
     end;
//  FLogger.LogWarning('Cannot find ADI accrual group named "Standard Accruals" or "Standard Accrual". Using ID "STD" as default accrual group ID anyway');
end;

function TADIDictionaries.GuessDefaultPointsGroup: variant;
var
  pointsGroups: bndRetrieveAllEmployeescodes.IXMLPointsgroupType;
  i: integer;
begin
  if FGroups = nil then
    FGroups := FConn.GetGroups;
  pointsGroups := FGroups.Pointsgroup;
  for i := 0 to pointsGroups.Count-1 do
    if AnsiSameText(trim(pointsGroups[i].Id), 'NO POINTS') then //id is custom ID here!
    begin
      Result := trim(pointsGroups[i].Id);
      exit; //no warnings
    end;
  if pointsGroups.Count > 0 then
  begin
    Result := pointsGroups[0].Id;
    FLogger.LogWarningFmt('Using <%s> as default points group', [pointsGroups[0].Name]);
  end
  else
    raise Exception.Create('No points groups defined in ADI')
end;

function TADIDictionaries.DefAccrualGroup: string;
begin
  if VarIsEmpty(FDefAccrualGroup) then
    FDefAccrualGroup := GuessDefaultAccrualGroup;
  Result := FDefAccrualGroup;
end;

function TADIDictionaries.DefPointsGroup: string;
begin
  if VarIsEmpty(FDefPointsGroup) then
    FDefPointsGroup := GuessDefaultPointsGroup;
  Result := FDefPointsGroup;
end;

function TADIDictionaries.GetStateAbbrByNullableGUID(v: Variant): string;
var
  i: integer;
  guid: string;
begin
  guid := trim(VarToStr(v));
  if guid = '' then
  begin
    Result := '';
    exit;
  end;
  for i := 0 to States.Count-1 do
    if AnsiSameText(trim(States[i].id), guid) then
      begin
        Result := trim(States[i].Abbreviation);
        Exit;
      end;
  raise Exception.CreateFmt('Cannot find ADI state master record by internal id <%s>', [guid]);
end;

function TADIDictionaries.States: bndStateMasters.IXMLStatemastersType;
begin
  if FStates = nil then
    FStates := bndStateMasters.Getstatemasters( FConn.RetrieveAllCodes(7) );
  Result := FStates;
end;

function TADIDictionaries.GetStatusCodeByGUID(guid: string): string;
var
  i: integer;
begin
  guid := trim(guid);
  for i := 0 to Statuses.Count-1 do
    if AnsiSameText(trim(Statuses[i].Id), guid) then
    begin
      Result := trim(Statuses[i].Code);
      Exit;
    end;
  raise Exception.CreateFmt('Cannot find ADI employee status with internal id <%s>', [guid]);
end;

function TADIDictionaries.GetRaceCodeByNullableGUID(v: Variant): string;
var
  i: integer;
  guid: string;
begin
  guid := trim(VarToStr(v));
  if guid = '' then
  begin
    Result := '';
    exit;
  end;
  for i := 0 to Races.Count-1 do
    if AnsiSameText(trim(Races[i].Id), guid) then
      begin
        Result := trim(Races[i].Code);
        Exit;
      end;
  raise Exception.CreateFmt('Cannot find ADI race code by internal id <%s>', [guid]);
end;

function TADIDictionaries.Races: bndRaceCodes.IXMLRacecodesType;
begin
  if FRaces = nil then
    FRaces := bndRaceCodes.Getracecodes( FConn.RetrieveAllCodes(5) );
  Result := FRaces;
end;

function TADIDictionaries.Statuses: bndEmployeeStatuses.IXMLEmployeestatusesType;
begin
  if FStatuses = nil then
    FStatuses := bndEmployeeStatuses.Getemployeestatuses( FConn.RetrieveAllCodes(6) );
  Result := FStatuses;
end;

function TADIDictionaries.GetStatusClassByStatusCode(code: string): integer;
var
  i: integer;
begin
  code := trim(code);
  for i := 0 to Statuses.Count-1 do
    if trim(Statuses[i].Code) = code then
    begin
      Result := Statuses[i].Statusclass.NodeValue;
      Exit;
    end;
  raise Exception.CreateFmt('Cannot find ADI employee status with ccde <%s>', [code]);
end;

function TADIDictionaries.Calendars(year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarsType;
begin
  if FCalendars[year] = nil then
    FCalendars[year] := FConn.RetrieveAllPayCalendar(year);
  Result := FCalendars[year];
end;

function TADIDictionaries.PayGroups: bndRetrieveAllPayGroups.IXMLPaygroupsType;
begin
  if FPayGroups = nil then
    FPayGroups := FConn.RetrieveAllPayGroups;
  Result := FPayGroups;
end;

function TADIDictionaries.GetPayGroupByGUID(guid: string): bndRetrieveAllPayGroups.IXMLPaygroupType;
var
  i: integer;
begin
  for i := 0 to PayGroups.Count-1 do
    if PayGroups[i].id = guid then
    begin
      Result := PayGroups[i];
      Exit;
    end;
  raise Exception.CreateFmt('Cannot find a pay group with id <%s>', [guid]);
end;

function TADIDictionaries.FindPayCalendar(guid: string; year: integer): bndRetrieveAllPayCalendar.IXMLPayCalendarType;
var
  cals: bndRetrieveAllPayCalendar.IXMLPayCalendarsType;
  i: integer;
begin
  Result := nil;
  cals := Calendars(year);
  for i := 0 to cals.Count-1 do
    if cals[i].Id = guid then
    begin
      Result := cals[i];
      exit;
    end;
//  raise Exception.CreateFmt('Cannot find a pay calendar with id <%s> for %d year', [guid, year]);
end;

function TADIDictionaries.GetPayPeriodStartDate(guid: string; dt: TDateTime): TDateTime;
var
  paygroup: bndRetrieveAllPayGroups.IXMLPaygroupType;
  paycalendarGuid: string;
  calendar: bndRetrieveAllPayCalendar.IXMLPayCalendarType;
  i: integer;
begin
  Result := 0; //to make compiler happy
  FLogger.LogEntry('Getting pay period start date');
  try
    try
      paygroup := GetPayGroupByGUID(guid);
      FLogger.LogContextItem('Pay group code', paygroup.Paygroupid);
      if paygroup.ChildNodes.FindNode('paycalendar') = nil then
        raise Exception.Create('Pay group does not have pay calendar specified');
      FLogger.LogContextItem('Pay calendar name', paygroup.Paycalendar.Name);
      paycalendarGuid := VarToStr(paygroup.Paycalendar.NodeValue);

      calendar := FindPayCalendar(paycalendarGuid, YearOf(dt));
      if calendar <> nil then
        for i := 0 to calendar.paycalendarperiods.Count-1 do
          if (StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Startdate.NodeValue) <= dt) and
             (StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Enddate.NodeValue) >= dt) then
          begin
            Result := StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Startdate.NodeValue);
            Exit;
          end;
      calendar := FindPayCalendar(paycalendarGuid, YearOf(dt)-1); //copy-paste; added -1
      if calendar <> nil then
        for i := 0 to calendar.paycalendarperiods.Count-1 do
          if (StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Startdate.NodeValue) <= dt) and
             (StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Enddate.NodeValue) >= dt) then
          begin
            Result := StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Startdate.NodeValue);
            Exit;
          end;
      calendar := FindPayCalendar(paycalendarGuid, YearOf(dt)+1); //copy-paste; added 1
      if calendar <> nil then
        for i := 0 to calendar.paycalendarperiods.Count-1 do
          if (StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Startdate.NodeValue) <= dt) and
             (StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Enddate.NodeValue) >= dt) then
          begin
            Result := StdDateTimeToDateTime(calendar.paycalendarperiods.Period[i].Startdate.NodeValue);
            Exit;
          end;
      raise Exception.CreateFmt('Cannot find pay period for date %s in %s calendar', [DateToStr(dt), paygroup.Paycalendar.Name]);

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

{ TADIEmployeeUpdater }

constructor TADIEmployeeUpdater.Create(conn: IADIConnection;
  const adiOptions: TADIEEOptions; EEFilter: TEEFilter; EEData: TEvoEEData; DBDTMatcher, EEStatusMatcher, EthnicityMatcher: IMatcher;
  logger: ICommonLogger);
begin
  FLogger := logger;
  FADIOptions := adiOptions;
  FEEData := EEData;
  FConn := conn;
  FDict := TADIDictionaries.Create(FConn, FLogger);
  FDBDTMatcher := DBDTMatcher;
  FEEStatusMatcher := EEStatusMatcher;
  FEthnicityMatcher := EthnicityMatcher;
  FEEFilter := EEFilter;

  LogADIEEOptions(FLogger, FADIOptions);
  LogEEFilter(FLogger, FEEFilter);
  FLogger.LogDebug('D/B/D/T mapping', FDBDTMatcher.Dump);
  FLogger.LogDebug('EEStatus mapping', FEEStatusMatcher.Dump);
  FLogger.LogDebug('Ethnicity mapping', FEthnicityMatcher.Dump);
end;

function TADIEmployeeUpdater.ADISettings: bndRetrieveSettings.IXMLSettingsType;
begin
  if FADISettings = nil then
  begin
    FADISettings := FConn.RetrieveSettings;
  end;
  Result := FADISettings;
end;

function TADIEmployeeUpdater.Users: bndRetrieveAllUsers.IXMLUsersType;
begin
  if FUsers = nil then
  begin
    Assert(FConn<>nil);
    FUsers := FConn.RetrieveAllUsers;
  end;
  Result := FUsers;
end;

function TADIEmployeeUpdater.MapADILevelToEvoFieldName(adilevel: TADILevel): string;
var
  v: Variant;
  evoLevel: string;
begin
  v := FDBDTMatcher.LeftMatch(adilevel);
  if not VarIsNull(v) then
  begin
    evoLevel := VarToStr(v);
    Assert( Length(evoLevel) = 1 );
    Assert( ((evoLevel[1] >= CLIENT_LEVEL_DIVISION) and (evoLevel[1] <= CLIENT_LEVEL_TEAM)) or (evoLevel = CLIENT_LEVEL_X_JOB));
    if evoLevel = CLIENT_LEVEL_X_JOB then
      Result := 'JOB_CODE'
    else
      Result := CDBDTMetadata[evoLevel[1]].CodeField;
  end
  else
    Result := '';
end;

function ComposeShortName(last, first, mi: string): string;
begin
  // was firstname + ' ' + lastname;
  Result := last + ', ' + first;
  if trim(mi) <> '' then
    Result := Result + ' ' + mi + '.';
  Result := Copy(Result, 1, 30);  
end;

function ComposeInitials(last, first, mi: string): string;
begin
  Result := copy(first, 1, 1) + copy(mi, 1, 1) + copy(last, 1, 1);
end;

function TADIEmployeeUpdater.MapEvoEEStatus(st: string): Variant;
begin
  Result := FEEStatusMatcher.RightMatch( st );
  if VarIsNull(Result) then
    raise Exception.CreateFmt('Evolution employee status <%s> is not mapped to ADI employee status', [GetEvoNameByCode(st, EE_TerminationCode_ComboChoices)]);
end;

function TADIEmployeeUpdater.BuildEmployee(adiEECode: string; creating: boolean): bndUpdateEmployee.IXMLEmployeeType;
  function NullableDateToStdDate(d: Variant): string;
  begin
    if not VarIsNull(d) then
      Result := DateToStdDate(d)
    else
      Result := '';
  end;

  procedure SetDBDT(adilevel: TADILevel);
  var
    evofn: string;
  begin
    evofn := MapADILevelToEvoFieldName(adilevel);
    if evofn <> '' then
    begin
      //I doubt I can clear a hierarchy level field and it might be the wrong thing to do because ADI can use a default level element
      if not VarIsNull(FEEData.EEs[evofn]) then
        Result.ChildNodes[ADILevelNames[adilevel]+'id'].NodeValue := FEEData.EEs[evofn];
    end
  end;

  function MapEvoEthnicity(eth: string): Variant;
  begin
    if eth = ETHNIC_NOT_APPLICATIVE then  //this field in Evo is mandatory but user can set it to ETHNIC_NOT_APPLICATIVE, it's like setting it to Null but done deliberately
      Result := Null
    else
    begin
      Result := FEthnicityMatcher.RightMatch( eth );
      if VarIsNull(Result) then
        raise Exception.CreateFmt('Evolution ethnicity <%s> is not mapped to ADI race code', [GetEvoNameByCode(eth, Ethnicity_ComboChoices)]);
    end
  end;
var
  rates: TRateArray;
  firstname, lastname, mi: string;
  i: integer;
  termdt: Variant;
begin
  Assert(trim(adiEECode) = trim(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']));
  Result := bndUpdateEmployee.Newemployee;
  //doNodeAutoCreate is On by default, CompareFields may add empty nodes to the ee xml
  // so call this function again when creating or updating employee

  lastname := trim(FEEData.EEs['LAST_NAME']);
  firstname := trim(FEEData.EEs['FIRST_NAME']);
  mi := trim(FEEData.EEs.FieldByName('MIDDLE_INITIAL').AsString); //optional in Evo

  if (fteLastName in FADIOptions.FieldsToExport) or creating then
    Result.Lastname := lastname;
  if (fteFirstName in FADIOptions.FieldsToExport) or creating then
    Result.Firstname := firstname;
  if (fteMiddleInitial in FADIOptions.FieldsToExport) or creating then
    Result.Middleinitial := mi;
  if (fteShortName in FADIOptions.FieldsToExport) or creating then
    Result.Shortname := ComposeShortName(lastname, firstname, mi);
  if (fteInitials in FADIOptions.FieldsToExport) or creating then
    Result.Initials := ComposeInitials(lastname, firstname, mi);

  if fteSSN in FADIOptions.FieldsToExport then //if it is off then never export
    Result.Ssn := FEEData.EEs['SOCIAL_SECURITY_NUMBER']
  else if creating then
    Result.Ssn := MakeFakeSSN(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);

  Result.Employeeidnumber := adiEECode; //with leading or trailing spaces
  if (fteStatus in FADIOptions.FieldsToExport) or creating then
  begin
    Result.Employeestatus := MapEvoEEStatus( FEEData.EEs['CURRENT_TERMINATION_CODE'] );
    Result.Isrehireable := ord( FEEData.EEs['ELIGIBLE_FOR_REHIRE'] <> GROUP_BOX_NO); //GROUP_BOX_CONDITIONAL
    termdt := FEEData.GetEETerminationDate;
    if not VarIsNull(termdt) then
      Result.Terminationdate := DateToStdDate(termdt);
  end;

  if (fteRates in FADIOptions.FieldsToExport) and (FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat = 0) then //if they are off then never export
  begin
    rates := FEEData.GetEERatesArray;
    Result.Primaryrate := FloatToStr(rates[0]);
    Result.Alternaterate1 := FloatToStr(rates[1]);
    Result.Alternaterate2 := FloatToStr(rates[2]);
  end;

  if (fteCompany in FADIOptions.FieldsToExport) or creating then
    if Hierarchy.LevelInfo(adiCompany).Enabled then
      Result.Companyid := FEEData.Company.CUSTOM_COMPANY_NUMBER;

  if (fteLocationADI in FADIOptions.FieldsToExport) or creating then
    SetDBDT(adiLocation);
  if (fteDivisionADI in FADIOptions.FieldsToExport) or creating then
    SetDBDT(adiDivision);
  if (fteDepartmentADI in FADIOptions.FieldsToExport) or creating then
    SetDBDT(adiDepartment);
  if (ftePositionADI in FADIOptions.FieldsToExport) or creating then
    SetDBDT(adiPosition);

  //we can't clear ADI supervisor field so if this Evo employee doesn't have supervisor then
  //we assume that we either don't have HR enabled or just don't care about supervisors
  if (fteSupervisor in FADIOptions.FieldsToExport) or creating then
    if ADIHasSupervisors and (trim(FEEData.EEs.FieldByName('supervisor_name').AsString)<>'') then
      Result.Supervisor := FindSVUserIdByEvoName(Users, FEEData.EEs.FieldByName('supervisor_name').AsString);

  //we can't clear this field in ADI
  if (fteBadgeID_ClockID in FADIOptions.FieldsToExport) or creating then
    if trim(FEEData.EEs.FieldByName('BADGE_ID').AsString) <> '' then
      Result.Timeclocknumber := FEEData.EEs['BADGE_ID'];

  if (fteAddressline1 in FADIOptions.FieldsToExport) or creating then
    Result.Addressline1 := FEEData.EEs['ADDRESS1'];
  if (fteAddressline2 in FADIOptions.FieldsToExport) or creating then
    Result.Addressline2 := FEEData.EEs.FieldByName('ADDRESS2').AsString; //optional in Evo
  if (fteCity in FADIOptions.FieldsToExport) or creating then
    Result.City := FEEData.EEs['CITY'];
  if (fteState in FADIOptions.FieldsToExport) or creating then
    Result.State := FEEData.EEs['STATE'];
  if (fteZipCode in FADIOptions.FieldsToExport) or creating then
    Result.Zipcode:= FEEData.EEs.FieldByName('ZIP_CODE').AsString; //let ADI deal with non-integer values

  if (ftePhone in FADIOptions.FieldsToExport) or creating then
    Result.Phone := FEEData.EEs.FieldByName('PHONE1').AsString; //optional in Evo
  if (fteEmailAddress in FADIOptions.FieldsToExport) or creating then
    Result.Emailaddress := FEEData.EEs.FieldByName('E_MAIL_ADDRESS').AsString; //optional in Evo
  if (fteHireDate in FADIOptions.FieldsToExport) or creating then
    Result.Hiredate := DateToStdDate(FEEData.EEs['CURRENT_HIRE_DATE']);
  if (fteBirthDate in FADIOptions.FieldsToExport) or creating then
    Result.Birthdate := NullableDateToStdDate(FEEData.EEs['BIRTH_DATE']);
  if (fteReviewDate in FADIOptions.FieldsToExport) or creating then
    Result.Reviewdate := NullableDateToStdDate(FEEData.EEs['REVIEW_DATE']);

  if (fteGender in FADIOptions.FieldsToExport) or creating then
  begin
    if FEEData.EEs['GENDER'] = GROUP_BOX_MALE then
      Result.gender := 1
    else if FEEData.EEs['GENDER'] = GROUP_BOX_FEMALE then
      Result.gender := 2;
    //else it is GROUP_BOX_UNKOWN, don't export it, this field is not mandatory in ADI
  end;

  if (fteRace in FADIOptions.FieldsToExport) or creating  then
    Result.ChildValues['race'] := MapEvoEthnicity(FEEData.EEs['ETHNICITY']); //integer and can be null, other nullable fields are string and null is represented as ''

  if (fteSalaried in FADIOptions.FieldsToExport) or creating then
    Result.Issalaried := ord(FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat > 0);
//  Result.Salary := FloatToStr(FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat);

  for i := 0 to Result.childNodes.Count-1 do
    if not VarIsNull(Result.childNodes[i].NodeValue) then
      Result.childNodes[i].NodeValue := trim(Result.childNodes[i].NodeValue);
end;

procedure TADIEmployeeUpdater.AddDefaultValues( adiEE: bndUpdateEmployee.IXMLEmployeeType; userChosenSV: string );
begin
  if ADISettings.Isaccrualsenabled <> 0 then
    adiEE.Accrualgroup := FDict.DefAccrualGroup;

  if ADISettings.Ispointsenabled <> 0 then
    adiEE.Pointsgroup := FDict.DefPointsGroup; //FLogger.LogWarning('Points are not supported');

  adiEE.Paygroup := FADIOptions.DefaultPayGroupCode;

  if ADIHasSupervisors and (adiEE.ChildNodes.FindNode('supervisor') = nil) then
  begin
    //this Evo EE don't have supervisor and we have asked the user to choose one
    if trim(userChosenSV) <> '' then
    begin
      adiEE.Supervisor := userChosenSV;
      FLogger.LogDebug('Using user-chosen sv:' + userChosenSV);
    end;
  end;
end;

function TADIEmployeeUpdater.MakeFakeSSN(eecode: string): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to length(eecode) do
    if eecode[i] in ['0'..'9'] then
      Result := Result + copy(eecode, i, 1);
  if Length(Result) > 9 then
    Result := RightStr(Result, 9)
  else if Length(Result) < 9 then
    Result := StringOfChar('0', 9 - Length(Result)) + Result;
  Result := Format( '%s-%s-%s', [copy(Result,1, 3), copy(Result,4, 2), copy(Result,6, 4)] );
end;

function TADIEmployeeUpdater.GetAssignableSVUserIDs: TStringSet;
var
  EeHierarchyGuids: TStringSet;

  procedure AddLevelGuid(adiLevel: TADILevel);
  var
    evofn: string;
  begin
    evofn := MapADILevelToEvoFieldName(adilevel);
    if evofn <> '' then
    begin
      if not VarIsNull(FEEData.EEs[evofn]) then
        SetInclude( EeHierarchyGuids, Hierarchy.GetGUIDByCode(adilevel, FEEData.EEs[evofn]) );
    end
  end;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
  j: integer;
begin
  EeHierarchyGuids := nil;
  if hierarchy.LevelInfo(adiCompany).Enabled then
    SetInclude( EeHierarchyGuids, hierarchy.GetGUIDByCode(adiCompany, FEEData.Company.CUSTOM_COMPANY_NUMBER) );

  AddLevelGuid( adiLocation );
  AddLevelGuid( adiDivision );
  AddLevelGuid( adiDepartment );
  AddLevelGuid( adiPosition );

  Result := nil;

  for i := 0 to users.Count-1 do
  begin
    user := users[i];
    if IsSV(user) and (user.ChildNodes.FindNode('assignablelevel') <> nil) then
      with SplitByChar(VarToStr(user.Assignablelevel.NodeValue), ';') do
        for j := 0 to Count-1 do
          if InSet(trim(Str[j]), EeHierarchyGuids) then
            SetInclude(Result, user.Userid); //not supervisorid!
  end;
  FLogger.LogDebug('AssignableSVUserIDs:'+SetToStr(Result,';'));
end;

function ChangedFieldRecsToBriefString(changedFields: TChangedFieldRecs): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(changedFields) do
  begin
    if Result <> '' then
      Result := Result + ', ';
    Result := Result + changedFields[i].FieldName;
  end
end;

function ChangedFieldRecsToDetailedString(changedFields: TChangedFieldRecs): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to high(changedFields) do
  begin
    if Result <> '' then
      Result := Result + #13#10;
    Result := Result + Format( '%s, old value: "%s", new value: "%s"', [changedFields[i].FieldName,changedFields[i].ADIValue, changedFields[i].EvoValue]);
  end
end;


function EEUpdateActionRecsToString(recs: TEEUpdateActionRecs): string;
  function GetListOfEECodesForAction(act: TEEUpdateAction): string;
  var
    i: integer;
  begin
    Result := '';
    for i := 0 to high(recs) do
      if recs[i].Action = act then
      begin
        if Result <> '' then
          Result := Result + ', ';
        Result := Result + recs[i].Code;
      end;
  end;
var
  i: integer;
  s: string;
begin
  Result := '';
  s := GetListOfEECodesForAction(euaCreate);
  if s <> '' then
    Result := Result + 'ADI records will be CREATED for the following employees: ' + s + #13#10#13#10#13#10;

  s := '';
  for i := 0 to high(recs) do
    if recs[i].Action = euaUpdate then
    begin
      if s <> '' then
        s := s + #13#10#13#10;
      s := s + 'Employee # ' + recs[i].Code + ':'#13#10 + ChangedFieldRecsToDetailedString(recs[i].ChangedFields);
    end;
  if s <> '' then
    Result := Result + 'ADI records will be UPDATED for the following employees: '#13#10#13#10 + s + #13#10#13#10#13#10;

  s := GetListOfEECodesForAction(euaDelete);
  if s <> '' then
    Result := Result + 'ADI records will be DELETED for the following employees: ' + s + #13#10#13#10#13#10;
end;

function FindADIEE(adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType; code: string): IXMLEmployeeType;
var
  i:integer;
begin
  Result := nil;
  code := trim(code);
  for i := 0 to adiEEs.Count-1 do
    if trim(adiEEs[i].Employeeidnumber) = code then
    begin
      Result := adiEEs[i];
      Exit;
    end;
end;

function TADIEmployeeUpdater.Analyze: TAnalyzeResult;
var
  adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType;

  //expects that adiEE's doNodeAutoCreate is On and adds empty nodes to adiEE
  function CompareFields(adiEE: bndRetrieveAllEmployees.IXMLEmployeeType): TChangedFieldRecs;
  var
    evoEE: bndUpdateEmployee.IXMLEmployeeType;

    procedure AddChangedFields(adiS, evoS, fieldName: string);
    begin
      SetLength(Result, Length(Result)+1);
      Result[high(Result)].FieldName := fieldName;
      Result[high(Result)].ADIValue := adiS;
      Result[high(Result)].EvoValue := evoS;
    end;

    procedure compare(v1, v2: Variant; fn: string); overload;
    begin
      if trim(VarToStr(v1)) <> trim(VarToStr(v2)) then
        AddChangedFields(VarToStr(v1), VarToStr(v2), fn)
    end;
    procedure compare(v1, v2: Variant; fte: TFieldToExport); overload;
    begin
      if fte in FADIOptions.FieldsToExport then
        compare(v1, v2, FieldExportInfos[fte].UserFriendlyName);
    end;
    procedure compareFloat(s1, s2: string; fieldName: string);
    var
      diff: boolean;
    begin
      diff := abs(StrToFloat(s1) - StrToFloat(s2)) > 0.01; //ADI rounds to 2 digits
      if diff then
        AddChangedFields(s1, s2, fieldName)
    end;
    procedure compareSSN(s1, s2: string; fieldName: string);
    var
      x1, x2: string;
      diff: boolean;
    begin
      x1 := StringReplace(s1, '-', '', [rfReplaceAll]);
      x2 := StringReplace(s2, '-', '', [rfReplaceAll]);
      diff := trim(x1) <> trim(x2);
      if diff then
        AddChangedFields(s1, s2, fieldName)
    end;
    function GetAdiEELevelCode(level: TADILevel): string;
    var
      fn: string;
    begin
      fn := ADILevelNames[level];
      if adiEE.ChildNodes.FindNode(fn) <> nil then
        Result := Hierarchy.GetCodeByGUID(level, adiEE[fn])
      else
        Result := '';
    end;
    procedure compareDateTimeAndDate(v1, v2: Variant; fn: string); overload;
    var
      d1, d2: string;
      e1, e2: boolean;
      diff: boolean;
      s1, s2: string;
    begin
      d1 := trim(VarToStr(v1));
      if d1 = '0001-01-01T00:00:00' then
        d1 := '';
      d2 := trim(VarToStr(v2));
      e1 := d1 = '';
      e2 := d2 = '';
      diff := (e1 and e2) or (not e1 and not e2 and (StdDateTimeToDateTime(d1) = StdDateToDate(d2)) );
      diff := not diff;
      if diff then
      begin
        if d1 <> '' then
          s1 := DateTimeToStr(StdDateTimeToDateTime(d1))
        else
          s1 := '';
        if d2 <> '' then
          s2 := DateTimeToStr(StdDateToDate(d2))
        else
          s2 := '';
        AddChangedFields(s1, s2, fn);
      end
    end;
    procedure compareDateTimeAndDate(v1, v2: Variant; fte: TFieldToExport); overload;
    begin
      if fte in FADIOptions.FieldsToExport then
        compareDateTimeAndDate(v1, v2, FieldExportInfos[fte].UserFriendlyName)
    end;
  begin
    Assert( trim(FEEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(adiEE.Employeeidnumber) );

    evoEE := BuildEmployee(adiEE.Employeeidnumber, false);

    SetLength(Result, 0);

    compare(adiEE.Lastname, EvoEE.Lastname, fteLastName);
    compare(adiEE.Firstname, EvoEE.Firstname, fteFirstName);
    compare(adiEE.Middleinitial, EvoEE.Middleinitial, fteMiddleInitial);
    compare(adiEE.Shortname, EvoEE.Shortname, fteShortName);
    if fteSSN in FADIOptions.FieldsToExport then
      compareSSN(adiEE.Ssn, EvoEE.Ssn, 'SSN');
    if fteStatus in FADIOptions.FieldsToExport then
    begin
      compare(FDict.GetStatusCodeByGUID(adiEE.ChildValues['employeestatus']), EvoEE.ChildValues['employeestatus'], fteStatus);
      //Even if I set these field it won't change unless ee status class is Term or Term pending
      if FDict.GetStatusClassByStatusCode(evoEE.Employeestatus) in [2,4] then // Term, Term pending
      begin
        compare(adiEE.ChildValues['isrehireable'], EvoEE.ChildValues['isrehireable'], 'Rehire OK');
        compareDateTimeAndDate(adiEE.ChildValues['terminationdate'], EvoEE.ChildValues['terminationdate'], 'Termination Date');
      end;
    end;  

    if (fteRates in FADIOptions.FieldsToExport) and (FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat = 0) then
    begin
      compareFloat(adiEE.Primaryrate, EvoEE.Primaryrate, 'Primary rate');
      compareFloat(adiEE.Alternaterate1, EvoEE.Alternaterate1, 'Alternate rate 1');
      compareFloat(adiEE.Alternaterate2, EvoEE.Alternaterate2, 'Alternate rate 2');
    end;

    if EvoEE.ChildNodes.FindNode('companyid') <> nil then
      compare(GetAdiEELevelCode(adiCompany), EvoEE.Companyid, fteCompany);
    if EvoEE.ChildNodes.FindNode('locationid') <> nil then
      compare(GetAdiEELevelCode(adiLocation), EvoEE.Locationid, fteLocationADI);
    if EvoEE.ChildNodes.FindNode('divisionid') <> nil then
      compare(GetAdiEELevelCode(adiDivision), EvoEE.Divisionid, fteDivisionADI);
    if EvoEE.ChildNodes.FindNode('departmentid') <> nil then
      compare(GetAdiEELevelCode(adiDepartment), EvoEE.Departmentid, fteDepartmentADI);
    if EvoEE.ChildNodes.FindNode('positionid') <> nil then
      compare(GetAdiEELevelCode(adiPosition), EvoEE.Positionid, ftePositionADI);

    //we can't clear ADI supervisor field so if an Evo employee doesn't have supervisor then
    //we assume that we either don't have HR enabled or just don't care about supervisors
    if ADIHasSupervisors and (EvoEE.ChildNodes.FindNode('supervisor') <> nil) then
      compare(GetSVCodeByNullableGuid(Users, adiEE.Supervisor.NodeValue), EvoEE.Supervisor, fteSupervisor); //!! adiEE.Supervisor.name is superuserId

    //we can't clear this field so if it's empty in Evo then just don't touch ADI data
    if (EvoEE.ChildNodes.FindNode('timeclocknumber') <> nil) then
      compare(adiEE.Timeclocknumber, EvoEE.Timeclocknumber, fteBadgeID_ClockID);

    compare(adiEE.Addressline1, EvoEE.Addressline1, fteAddressline1);
    compare(adiEE.Addressline2, EvoEE.Addressline2, fteAddressline2);
    compare(adiEE.City, EvoEE.City, fteCity);
    compare(adiEE.Zipcode, EvoEE.Zipcode, fteZipCode);
    compare(FDict.GetStateAbbrByNullableGUID(adiEE.State.NodeValue), EvoEE.State, fteState);
    compare(adiEE.Phone, EvoEE.Phone, ftePhone);
    compare(adiEE.Emailaddress, EvoEE.Emailaddress, fteEmailAddress);

    compareDateTimeAndDate(adiEE.Hiredate.NodeValue, EvoEE.Hiredate, fteHireDate);
    compareDateTimeAndDate(adiEE.Birthdate.NodeValue, EvoEE.Birthdate, fteBirthDate);
    compareDateTimeAndDate(adiEE.Reviewdate.NodeValue, EvoEE.Reviewdate, fteReviewDate);


    //we can't clear this field so if this mandatory field is 'N/A' in Evo then just don't touch ADI data
    //!! or maybe I can, I have to check this
    if EvoEE.ChildNodes.FindNode('gender') <> nil then
      compare(adiEE.Gender.NodeValue, EvoEE.Gender, fteGender);
    compare(FDict.GetRaceCodeByNullableGUID(adiEE.Race.NodeValue), EvoEE.Race, fteRace);

    compare(adiEE.ChildValues['issalaried'], EvoEE.ChildValues['issalaried'], fteSalaried);
//    compareFloat(adiEE.Salary, EvoEE.Salary, 'Salary');
//    if Length(Result) > 0 then
//      FLogger.LogEvent('Changed fields: ' + ChangedFieldRecsToBriefString(Result), ChangedFieldRecsToDetailedString(Result));
  end;

var
  i: integer;
  used: integer;
  changedFields: TChangedFieldRecs;
begin
  Result.Errors := 0;
  used := 0;
  FLogger.LogEntry('Analyzing employee records');
  try
    try
      adiEEs := FConn.RetrieveAllEmployees;
      FLogger.LogDebug( Format('ADI total employee count: %d',[adiEEs.Count]) );

      adiEEs.OwnerDocument.Options := adiEEs.OwnerDocument.Options + [doNodeAutoCreate]; //for CompareFields
      FLogger.LogDebug( Format('ADI total employee count (1): %d',[adiEEs.Count]) );

      if hierarchy.LevelInfo(adiCompany).Enabled then
        hierarchy.FilterEEsByCompany(FEEData.Company, adiEEs);
      FLogger.LogDebug( Format('ADI company employee count: %d',[adiEEs.Count]) );

      SetLength(Result.Actions, adiEEs.Count + FEEData.EEs.RecordCount);

      for i := 0 to adiEEs.Count-1 do
      begin
        FLogger.LogEntry('Analyzing ADI employee record');
        try
          try
            FLogger.LogContextItem(sCtxEECode, adiEEs[i].Employeeidnumber);
            if FEEData.LocateEE(adiEEs[i].Employeeidnumber) then
            begin
              if AcceptedByFilter(FLogger, FEEData.EEs, FEEFilter) then
              begin
                changedFields := CompareFields(adiEEs[i]);
                if Length(changedFields) > 0 then
                begin
                  Result.Actions[used].Code := adiEEs[i].Employeeidnumber;
                  Result.Actions[used].Action := euaUpdate;
                  Result.Actions[used].ChangedFields := changedFields;
                  used := used + 1;
                end;
              end;
            end
            else if FADIOptions.AllowDeletionOfEE then
            begin
              Result.Actions[used].Code := adiEEs[i].Employeeidnumber;
              Result.Actions[used].Action := euaDelete;
              used := used + 1;
            end
            {
            Craig wrote:
            If they are not part of the filtered list, then I would completely ignore updating them at all.
            It is possible that a client could keep track of people in TLM who are not on payroll at all,
            so I would simply skip them and leave them as is.
            }
          except
            Result.Errors := Result.Errors + 1;
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end
      end;
      FEEData.EEs.First;
      while not FEEData.EEs.Eof do
      begin
        FLogger.LogEntry('Analyzing Evolution employee record');
        try
          try
            FLogger.LogContextItem(sCtxEECode, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
            if AcceptedByFilter(FLogger, FEEData.EEs, FEEFilter) then
            begin
              if FindADIEE(adiEEs, FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']) = nil then
              begin
                Result.Actions[used].Code := trim(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
                Result.Actions[used].Action := euaCreate;
                Result.Actions[used].HasEvoSV := trim(FEEData.EEs.FieldByName('supervisor_name').AsString) <> '';
                if ADIHasSupervisors and not Result.Actions[used].HasEvoSV then
                  Result.Actions[used].AssignableSVUserIDs := GetAssignableSVUserIds;
                used := used + 1;
              end;
            end;
          except
            Result.Errors := Result.Errors + 1;
            FLogger.StopException;
          end;
        finally
          FLogger.LogExit;
        end;
        FEEData.EEs.Next;
      end;
      SetLength(Result.Actions, used);
      Result.ADIHasSupervisors := ADIHasSupervisors;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

destructor TADIEmployeeUpdater.Destroy;
begin
  FreeAndNil(FDict);
  inherited;
end;

const
  EEUpdateActionNames: array [TEEUpdateAction] of string = ('Create', 'Update', 'Delete');

function TADIEmployeeUpdater.Apply(recs: TEEUpdateActionRecs; pi: IProgressIndicator): TUpdateEmployeesStat;
var
  i: integer;
  ee: bndUpdateEmployee.IXMLEmployeeType;
begin
  Result := BuildEmptySyncStat;
  FLogger.LogEntry('Synchronizing ADI employee records');
  try
    try
      pi.StartWait('Synchronizing ADI employee records', Length(recs));
      try
        for i := 0 to high(recs) do
        begin
          FLogger.LogEntry('Processing employee');
          try
            try
              FLogger.LogContextItem(sCtxEECode, recs[i].Code);
              FLogger.LogContextItem('Action', EEUpdateActionNames[recs[i].Action]);
              case recs[i].Action of
                euaCreate:
                  begin
                    Assert( FEEData.LocateEE(recs[i].Code) );
                    ee := BuildEmployee(recs[i].Code, true);
                    AddDefaultValues(ee, recs[i].SVUserID); //SVUserID is ignored <=> HasEvoSV
                    FConn.CreateEmployee(ee);
                    inc(Result.Created);
                  end;
                euaUpdate:
                  begin
                    Assert( FEEData.LocateEE(recs[i].Code) );
                    FConn.UpdateEmployee( BuildEmployee(recs[i].Code, false) );
                    inc(Result.Modified);
                  end;
                euaDelete:
                  begin
                    try
                      FConn.DeleteEmployee(recs[i].Code);
                      inc(Result.Deleted)
                    except
                      on E: Exception do
                      begin
                        FLogger.LogWarningFmt('Cannot delete ADI employee <%s>, changing status to "Terminated" instead', [recs[i].Code], e.Message);
                        ee := bndUpdateEmployee.Newemployee;
                        ee.Employeeidnumber := recs[i].Code;
                        ee.Employeestatus := MapEvoEEStatus(EE_TERM_TERMINATED);
                        FConn.UpdateEmployee(ee);
                        inc(Result.Modified);
                      end
                    end;
                  end;
              else
                Assert(false);
              end;
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          pi.Step(i+1);
        end;
        //!!for debug only  -- remove it!
        //to put ee records into log
        try
          FLogger.LogDebug('After update:');
          FConn.RetrieveAllEmployees;
        except
          FLogger.StopException;
        end;
      finally
        pi.EndWait;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TADIEmployeeUpdater.ADIHasSupervisors: boolean;
begin
  Result := ADISettings.enablesupervisor = 1;
end;

function TADIEmployeeUpdater.CreateSVDataSet: TkbmCustomMemTable;
var
  i: integer;
  user: bndRetrieveAllUsers.IXMLUserType;
begin
  Result := TkbmMemTable.Create(nil);
  try
    CreateStringField( Result, 'USER_ID', 'User Id', 10);
    CreateStringField( Result, 'USER_NAME', 'User Name', 30);
    Result.Open;
{    Result.Append;
    try
      Result['USER_ID'] := '';
      Result['USER_NAME'] := '(No supervisor)';
      Result.Post;
    except
      Result.Cancel;
      raise;
    end;     }
    for i := 0 to Users.Count-1 do
    begin
      user := Users[i];
      if IsSV(user) then
      begin
        Result.Append;
        try
          Result['USER_ID'] := user.Userid;
          Result['USER_NAME'] := user.Username;
          Result.Post;
        except
          Result.Cancel;
          raise;
        end;
      end
    end;
  except
    FreeAndNil(Result);
    raise;
  end;
end;

function TADIEmployeeUpdater.Hierarchy: IADIHierarchy;
begin
  if FHierarchy = nil then
    FHierarchy := TADIHierarchy.Create(FConn, FLogger);
  Result := FHierarchy;
end;

function GetADIEE(adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType; code: string): IXMLEmployeeType;
begin
  Result := FindADIEE(adiEEs, code);
  if Result = nil then
    raise Exception.CreateFmt('Cannot find ADI employee record with code <%s>', [code]);
end;

//bndRetrieveAllPayCalendar.IXMLPayCalendarsType
function ExportTOA(conn: IADIConnection; evoConn: IEvoAPIConnection; TOAMatcher: IMatcher; company: TEvoCompanyDef; logger: ICommonLogger; pi: IProgressIndicator): TTOAStat;
var
  eecode: string;
  TOA: TkbmCustomMemTable;
  accrualTypeId: Variant;
  dt: TDateTime;
  accrued, used: double;
  adiEEs: bndRetrieveAllEmployees.IXMLEmployeesType;
  adiEE: bndRetrieveAllEmployees.IXMLEmployeeType;
  dict: TADIDictionaries;

  function GetEEPayGroupGUID: string;
  begin
    if adiEE.ChildNodes.FindNode('paygroup') = nil then
      raise Exception.Create('Employee does not have pay group specified');
    Result := adiEE.Paygroup.NodeValue;
  end;
begin
  Result := BuildEmptyTOAStat;
  Logger.LogEntry(Format('Updating %s TOA records', [ExtAppName]));
  try
    try
      Logger.LogDebug('TOA mapping', TOAMatcher.Dump);

      TOA := ExecCoQuery(evoConn, Logger, Company, 'EE_TIME_OFF_ACCRUAL.rwq', 'Getting TOA records from Evolution');
      try
        pi.StartWait( Format('Updating %s TOA records', [ExtAppName]), TOA.RecordCount);
        try
          dict := TADIDictionaries.Create(conn, logger);
          try
            adiEEs := conn.RetrieveAllEmployees;
            TOA.First;
            while not TOA.Eof do
            begin
              Logger.LogEntry('Processing TOA record');
              try
                try
                  eecode := TOA['CUSTOM_EMPLOYEE_NUMBER'];
                  
                  Logger.LogContextItem(sCtxEECode, eecode);
                  Logger.LogContextItem('Evolution TOA pay type', TOA.FieldByName('DESCRIPTION').AsString);
                  Logger.LogContextItem('Evolution max accrual date', DateToStdDate(TOA.FieldByName('MAX_ACCRUAL_DATE').AsDateTime));
                  Logger.LogContextItem('Evolution max batch period end date', DateToStdDate(TOA.FieldByName('MAX_PERIOD_END_DATE').AsDateTime));
                  Logger.LogContextItem('Evolution accrual record effective date', DateToStdDate(TOA.FieldByName('EFFECTIVE_DATE').AsDateTime));

                  accrued := TOA.FieldByName('REAL_CURRENT_ACCRUED').AsFloat;
                  used := TOA.FieldByName('REAL_CURRENT_USED').AsFloat;

                  dt := TOA.FieldByName('MAX_ACCRUAL_DATE').AsDateTime;
                  if TOA.FieldByName('MAX_PERIOD_END_DATE').AsDateTime <> 0 then
                    dt := Max(dt, TOA.FieldByName('MAX_PERIOD_END_DATE').AsDateTime + 1);
                  if dt = 0 then
                  begin
                    dt := TOA.FieldByName('EFFECTIVE_DATE').AsDateTime;  //we need to take max of these three dates but we first need to get the effective date of the relevant fields, not the whole record
                    if abs(accrued-used) < eps then //make correction only in case of zero balance, let the user get an error from ADI in the other cases
                    begin
                      if (TOA.FieldByName('Current_Termination_Date').AsDateTime > 0) and (dt > TOA.FieldByName('Current_Termination_Date').AsDateTime) then
                        dt := TOA.FieldByName('Current_Termination_Date').AsDateTime;
                    end;
                  end;

                  Logger.LogContextItem('Evolution accrual update effective date', DateToStdDate(dt));

                  adiEE := GetADIEE(adiEEs, eecode);
                  dt := dict.GetPayPeriodStartDate(GetEEPayGroupGUID, dt);

                  Logger.LogContextItem('ADI accrual effective date', DateToStdDate(dt));

                  accrualTypeId := TOAMatcher.RightMatch( TOA.FieldByName('DESCRIPTION').AsString );
                  if VarIsNull(accrualTypeId) then
                    raise Exception.CreateFmt('Evolution TOA pay type <%s> is not mapped to an %s accrual type', [TOA.FieldByName('DESCRIPTION').AsString, ExtAppName]);

                  conn.UpdateAccrualBalance(adiEE, accrualTypeId, accrued-used, dt);

                  inc(Result.Updated);
                except
                  inc(Result.Errors);
                  Logger.StopException;
                end;
              finally
                Logger.LogExit;
              end;
              TOA.Next;
              pi.Step(TOA.RecNo+1);
            end;
          finally
            FreeAndNil(dict);
          end;
        finally
          pi.EndWait;
        end;
      finally
        FreeAndNil(TOA);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

end.




