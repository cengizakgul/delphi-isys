@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoADI

SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoADI.exe
cd ..\..\..\Projects\ICP\EvoADI\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoADI.msi 

"%pWiXDir%\candle.exe" EvoADI.wxs -wx -out ..\..\..\..\..\Tmp\EvoADI.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoADIRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoADIRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoADIRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoADI.wixobj ..\..\..\..\..\Tmp\EvoADIRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoADI.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoADI.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoADI_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoADI_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoADI.msi EvoADI_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
