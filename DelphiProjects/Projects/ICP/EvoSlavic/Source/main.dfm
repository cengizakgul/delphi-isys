inherited frmMain: TfrmMain
  Left = 325
  Top = 157
  Width = 820
  Height = 500
  Caption = 'EvoSlavic'
  Constraints.MinHeight = 500
  Constraints.MinWidth = 820
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 812
    Height = 447
    ActivePage = TabSheet3
    inherited TabSheet3: TTabSheet
      Caption = 'Settings'
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Left = 5
        Top = 3
        inherited GroupBox1: TGroupBox
          Height = 134
          Caption = 'Connection to Evolution'
        end
      end
      object gbFTPServer: TGroupBox
        Left = 5
        Top = 145
        Width = 390
        Height = 264
        Caption = 'Connection to FTP'
        TabOrder = 1
        object lblAddress: TLabel
          Left = 16
          Top = 26
          Width = 94
          Height = 13
          Caption = 'FTP Server address'
        end
        object lblUser: TLabel
          Left = 16
          Top = 51
          Width = 48
          Height = 13
          Caption = 'Username'
        end
        object lblPassword: TLabel
          Left = 16
          Top = 75
          Width = 46
          Height = 13
          Caption = 'Password'
        end
        object lblFolder: TLabel
          Left = 16
          Top = 174
          Width = 29
          Height = 13
          Caption = 'Folder'
        end
        object lblDeferralFile: TLabel
          Left = 16
          Top = 199
          Width = 110
          Height = 13
          Caption = 'Deferral/Enrollment File'
        end
        object lblEeAddressChangeFile: TLabel
          Left = 16
          Top = 224
          Width = 107
          Height = 13
          Caption = 'Ee Address Chage File'
        end
        object lblPortCaption: TLabel
          Left = 16
          Top = 102
          Width = 19
          Height = 13
          Caption = 'Port'
        end
        object edFTPServerAddress: TEdit
          Left = 143
          Top = 22
          Width = 124
          Height = 21
          TabOrder = 0
          OnChange = edFTPServerAddressChange
        end
        object edFtpUsername: TEdit
          Left = 143
          Top = 47
          Width = 124
          Height = 21
          TabOrder = 1
          OnChange = edFTPServerAddressChange
        end
        object edFtpPassword: TPasswordEdit
          Left = 143
          Top = 72
          Width = 124
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
          OnChange = edFTPServerAddressChange
        end
        object cbFtpSavePassword: TCheckBox
          Left = 272
          Top = 74
          Width = 100
          Height = 17
          Caption = 'Save password'
          TabOrder = 3
          OnClick = cbFtpSavePasswordClick
        end
        object edFolder: TEdit
          Left = 143
          Top = 169
          Width = 237
          Height = 21
          TabOrder = 4
          OnChange = edFTPServerAddressChange
        end
        object edDeferralFileName: TEdit
          Left = 143
          Top = 194
          Width = 237
          Height = 21
          TabOrder = 5
          OnChange = edFTPServerAddressChange
        end
        object edEeAddressFileName: TEdit
          Left = 143
          Top = 219
          Width = 237
          Height = 21
          TabOrder = 6
          OnChange = edFTPServerAddressChange
        end
        object btnTestConnection: TBitBtn
          Left = 143
          Top = 131
          Width = 125
          Height = 25
          Action = TestFtpConnection
          Caption = 'Test Connection'
          TabOrder = 7
        end
        object edFTPPort: TEdit
          Left = 143
          Top = 97
          Width = 124
          Height = 21
          TabOrder = 8
          OnChange = edFTPServerAddressChange
        end
        object chbSecureFTP: TCheckBox
          Left = 272
          Top = 25
          Width = 100
          Height = 17
          Caption = 'Secure FTP'
          TabOrder = 9
          OnClick = cbFtpSavePasswordClick
        end
      end
      inline SchedulerFrame: TfrmSimpleScheduler
        Left = 416
        Top = 3
        Width = 382
        Height = 134
        TabOrder = 2
        inherited gbSchedule: TGroupBox
          Width = 382
          Height = 134
          inherited lblScheduleInfo: TLabel
            Width = 353
            Height = 65
          end
          inherited btnSchedule: TBitBtn
            Top = 96
          end
          inherited btnClear: TBitBtn
            Top = 96
          end
        end
      end
      inline SmtpConfigFrm: TSmtpConfigFrm
        Left = 416
        Top = 145
        Width = 382
        Height = 264
        TabOrder = 3
        inherited GroupBox1: TGroupBox
          Width = 382
          Height = 264
          inherited Label6: TLabel
            Left = 16
            Top = 197
          end
          inherited Label8: TLabel
            Left = 16
            Top = 171
          end
          inherited edAddressTo: TEdit
            Left = 95
            Top = 194
            Width = 255
          end
          inherited edAddressFrom: TEdit
            Left = 95
            Top = 168
            Width = 255
          end
          inherited btnSendTest: TBitBtn
            Left = 96
            Top = 225
            Height = 28
          end
          inherited gbSMTP: TGroupBox
            Top = 42
            Width = 335
            Height = 112
            inherited Label4: TLabel
              Left = 10
              Top = 18
            end
            inherited Label2: TLabel
              Left = 250
              Top = 18
            end
            inherited Label5: TLabel
              Left = 9
              Top = 62
            end
            inherited Label1: TLabel
              Left = 171
              Top = 62
            end
            inherited edSMTPServer: TEdit
              Left = 10
              Top = 34
            end
            inherited edSMTPPort: TEdit
              Left = 250
              Top = 34
            end
            inherited edSMTPUser: TEdit
              Left = 9
              Top = 78
            end
            inherited edSMTPPassword: TPasswordEdit
              Left = 171
              Top = 78
            end
          end
          inherited cbSendEmail: TCheckBox
            Top = 19
            Caption = 'Send the report after import is done'
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Import'
      object pnlTop: TPanel
        Left = 0
        Top = 0
        Width = 804
        Height = 77
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        DesignSize = (
          804
          77)
        object btnImport: TButton
          Left = 8
          Top = 12
          Width = 103
          Height = 32
          Action = RunImport
          Default = True
          TabOrder = 0
        end
        object btnLoadData: TButton
          Left = 120
          Top = 14
          Width = 102
          Height = 28
          Action = LoadData
          TabOrder = 1
        end
        inline frmStatus: TfrmStatus
          Left = 364
          Top = 7
          Width = 425
          Height = 62
          TabOrder = 2
          inherited gbStatus: TGroupBox
            Width = 425
            Height = 62
            inherited lblMessage: TLabel
              Width = 406
              Height = 14
            end
            inherited pbProgress: TProgressBar
              Top = 36
              Width = 408
            end
          end
        end
        object gbReport: TGroupBox
          Left = 17
          Top = 59
          Width = 267
          Height = 59
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = 'Report'
          TabOrder = 3
          Visible = False
          DesignSize = (
            267
            59)
          object mmReport: TMemo
            Left = 8
            Top = 16
            Width = 252
            Height = 35
            Anchors = [akLeft, akTop, akRight, akBottom]
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
        object btnShowReport: TButton
          Left = 229
          Top = 14
          Width = 102
          Height = 28
          Action = ShowReport
          TabOrder = 4
        end
      end
      object pcData: TPageControl
        Left = 0
        Top = 77
        Width = 804
        Height = 342
        ActivePage = tsDeferralFile
        Align = alClient
        TabOrder = 1
        object tsDeferralFile: TTabSheet
          Caption = 'Deferral/Enrollment File'
          inline frmDeferralFile: TfrmCSvloader
            Left = 0
            Top = 0
            Width = 796
            Height = 314
            Align = alClient
            TabOrder = 0
            inherited dbgData: TReDBGrid
              Width = 796
              Height = 314
            end
          end
        end
        object tsEeAddress: TTabSheet
          Caption = 'Employee Address File'
          ImageIndex = 1
          inline frmEeAddressFile: TfrmCSvloader
            Left = 0
            Top = 0
            Width = 796
            Height = 314
            Align = alClient
            TabOrder = 0
            inherited dbgData: TReDBGrid
              Width = 796
              Height = 314
            end
          end
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 447
    Width = 812
  end
  object Actions: TActionList
    Left = 388
    Top = 80
    object ConnectToEvo: TAction
      Caption = 'Connect To Evo'
    end
    object TestFtpConnection: TAction
      Caption = 'Test Connection'
      OnExecute = TestFtpConnectionExecute
    end
    object LoadData: TAction
      Caption = 'Load files'
      OnExecute = LoadDataExecute
      OnUpdate = RunImportUpdate
    end
    object RunImport: TAction
      Caption = 'Run Import'
      OnExecute = RunImportExecute
      OnUpdate = RunImportUpdate
    end
    object ShowReport: TAction
      Caption = 'Show Report'
      OnExecute = ShowReportExecute
      OnUpdate = ShowReportUpdate
    end
  end
end
