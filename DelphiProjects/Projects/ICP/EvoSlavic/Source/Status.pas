unit Status;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, StdCtrls;

type
  IStatusProgress = interface
    procedure SetProgressCount(StepCount: integer);
    procedure Clear;
    procedure Hide;
    procedure Show;
    procedure SetMessage(aMsg: string);
    procedure IncProgress(aMsg: string = '');
  end;

  TfrmStatus = class(TFrame, IStatusProgress)
    gbStatus: TGroupBox;
    lblMessage: TLabel;
    pbProgress: TProgressBar;
  public
    procedure SetProgressCount(StepCount: integer);
    procedure Clear;
    procedure Hide;
    procedure Show;
    procedure SetMessage(aMsg: string);
    procedure IncProgress(aMsg: string = '');
  end;

implementation

{$R *.dfm}

procedure TfrmStatus.Clear;
begin
  lblMessage.Caption := '';
  pbProgress.Position := 0;
  pbProgress.Min := 0;
  pbProgress.Max := 0;
  pbProgress.Visible := False;
  Application.ProcessMessages;
end;

procedure TfrmStatus.Hide;
begin
  Self.Visible := False;
end;

procedure TfrmStatus.IncProgress(aMsg: string);
begin
  if aMsg <> '' then
    lblMessage.Caption := aMsg;
  pbProgress.Position := pbProgress.Position + 1;
  Application.ProcessMessages;
end;

procedure TfrmStatus.SetMessage(aMsg: string);
begin
  lblMessage.Caption := aMsg;
  Application.ProcessMessages;
end;

procedure TfrmStatus.SetProgressCount(StepCount: integer);
begin
  lblMessage.Caption := '';
  pbProgress.Position := 0;
  pbProgress.Min := 0;
  pbProgress.Max := StepCount - 1;
  pbProgress.Visible := True;
  Application.ProcessMessages;
end;

procedure TfrmStatus.Show;
begin
  Self.Visible := True;
end;

end.
