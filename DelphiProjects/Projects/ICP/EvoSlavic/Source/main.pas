unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientMainForm, ComCtrls, OptionsBaseFrame, common,
  EvoAPIConnectionParamFrame, StdCtrls, PasswordEdit, ActnList, Buttons,
  ExtCtrls, CSVLoader, ImportProcessing, Status, EvoSlavicProcedures,
  SimpleScheduler, SmtpConfigFrame, SBSimpleSftp;

type
  TfrmMain = class(TEvoAPIClientMainFm, IMainForm)
    gbFTPServer: TGroupBox;
    lblAddress: TLabel;
    lblUser: TLabel;
    lblPassword: TLabel;
    edFTPServerAddress: TEdit;
    edFtpUsername: TEdit;
    edFtpPassword: TPasswordEdit;
    cbFtpSavePassword: TCheckBox;
    lblFolder: TLabel;
    lblDeferralFile: TLabel;
    lblEeAddressChangeFile: TLabel;
    edFolder: TEdit;
    edDeferralFileName: TEdit;
    edEeAddressFileName: TEdit;
    Actions: TActionList;
    btnTestConnection: TBitBtn;
    ConnectToEvo: TAction;
    TestFtpConnection: TAction;
    pnlTop: TPanel;
    pcData: TPageControl;
    tsDeferralFile: TTabSheet;
    tsEeAddress: TTabSheet;
    frmDeferralFile: TfrmCSvloader;
    frmEeAddressFile: TfrmCSvloader;
    btnImport: TButton;
    btnLoadData: TButton;
    LoadData: TAction;
    frmStatus: TfrmStatus;
    gbReport: TGroupBox;
    mmReport: TMemo;
    SchedulerFrame: TfrmSimpleScheduler;
    SmtpConfigFrm: TSmtpConfigFrm;
    RunImport: TAction;
    btnShowReport: TButton;
    ShowReport: TAction;
    lblPortCaption: TLabel;
    edFTPPort: TEdit;
    chbSecureFTP: TCheckBox;
    procedure edFTPServerAddressChange(Sender: TObject);
    procedure cbFtpSavePasswordClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TestFtpConnectionExecute(Sender: TObject);
    procedure LoadDataExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RunImportExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RunImportUpdate(Sender: TObject);
    procedure ShowReportUpdate(Sender: TObject);
    procedure ShowReportExecute(Sender: TObject);
  private
    FImportProcessing: TImportProcessing;
    FScheduledRun: boolean;

    procedure LoadMainFormSize;
    procedure SaveMainFormSize;

    function GetEvoConnectionFrame: TEvoAPIConnectionParamFrm;
    procedure ClearImportReport;
    procedure AddMessageToImportReport(aMsg: string);
    procedure SendImportReportByEmail;
    procedure CloseIfScheduledRun;
    procedure CloseAfterScheduledRun;
    procedure CheckForScheduledRun;

    procedure HandleFTPParamChange;
    procedure HandleSmtpConfigChange(Sender: TObject);

  public
    procedure RunImportTask;

    constructor Create(Owner: TComponent); override;
    procedure Init;
  end;

var
  frmMain: TfrmMain;

implementation

uses gdyDeferredCall, isSettings, iniFiles, gdyRedir;

{$R *.dfm}

{ TfrmMain }

constructor TfrmMain.Create(Owner: TComponent);
begin
  inherited;
  GlobalLogger := Logger;
  with LoadFTPParam(FSettings, '') do
  begin
    edFTPServerAddress.Text := FtpServerAddress;
    edFtpUsername.Text := Username;
    edFtpPassword.Text := Password;
    edFTPPort.Text := IntToStr(Port);
    cbFtpSavePassword.Checked := SavePassword;
    chbSecureFTP.Checked := SecureFTP;
    edFolder.Text := Folder;
  end;

  edDeferralFileName.Text := FormatDateTime('yyyymmdd', Now) + 'd.csv';
  edEeAddressFileName.Text := FormatDateTime('yyyymmdd', Now) + 'a.csv';
  Caption := Application.Title + ' ' + GetVersion;
end;

procedure TfrmMain.HandleFTPParamChange;
var
  ftp_param: TFtpParam;
  lPort: integer;
begin
  try
    if (edFTPPort.Text <> '') and not TryStrToInt(edFTPPort.Text, lPort) then
    begin
      edFTPPort.Text := '21';
      lPort := 21;
    end;
    with ftp_param do
    begin
      FtpServerAddress := edFTPServerAddress.Text;
      Username := edFtpUsername.Text;
      Password := edFtpPassword.Text;
      Port := lPort;
      Folder := edFolder.Text;
      SavePassword := cbFtpSavePassword.Checked;
      SecureFTP := chbSecureFTP.Checked;
      DeferralFile := edDeferralFileName.Text;
      EeAddressFile := edEeAddressFileName.Text;
    end;

    if Assigned( FImportProcessing ) then
      FImportProcessing.FTPParameters := ftp_param;

    SaveFTPParam( ftp_param, FSettings, '' );
  except
    Logger.StopException;
  end;
end;

procedure TfrmMain.Init;
begin
  FImportProcessing := TImportProcessing.Create( Logger, frmEeAddressFile, frmDeferralFile, frmStatus, Self );
  HandleFTPParamChange;
  SchedulerFrame.Init( sRunImport );


  SmtpConfigFrm.Config := LoadSmtpConfig(FSettings, '');
  SmtpConfigFrm.OnChangeByUser := HandleSmtpConfigChange;

  CheckForScheduledRun;
end;

procedure TfrmMain.edFTPServerAddressChange(Sender: TObject);
begin
  inherited;
  HandleFTPParamChange;
end;

procedure TfrmMain.cbFtpSavePasswordClick(Sender: TObject);
begin
  inherited;
  HandleFTPParamChange;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveMainFormSize;
  inherited;
  FImportProcessing.Free;
end;

procedure TfrmMain.TestFtpConnectionExecute(Sender: TObject);
begin
  inherited;
  if FImportProcessing.TestFTPConnection then
    ShowMessage('Connection has been established successfully!')
  else
    ShowMessage('Can not connect to FTP Server. Please, see log for details');
end;

procedure TfrmMain.LoadDataExecute(Sender: TObject);
begin
  inherited;
  FImportProcessing.LoadData;
end;

function TfrmMain.GetEvoConnectionFrame: TEvoAPIConnectionParamFrm;
begin
  Result := EvoFrame;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  inherited;
  DeferredCall(Init);
  LoadMainFormSize;
end;

procedure TfrmMain.HandleSmtpConfigChange(Sender: TObject);
begin
  SaveSmtpConfig(SmtpConfigFrm.Config, FSettings, '');
end;

procedure TfrmMain.LoadMainFormSize;
var
  root: string;
begin
  root := sMainForm;
  try
    Self.Top := FSettings.AsInteger[root+'Top'];
    Self.Left := FSettings.AsInteger[root+'Left'];
    Self.Width := FSettings.AsInteger[root+'Width'];
    Self.Height := FSettings.AsInteger[root+'Height'];
    if FSettings.AsInteger[root+'Maximized'] = 1 then
      Self.WindowState := wsMaximized;
  except
  end;
end;

procedure TfrmMain.SaveMainFormSize;
var
  root: string;
begin
  root := sMainForm;
  try
    if Self.WindowState = wsMaximized then
      FSettings.AsInteger[root+'Maximized'] := 1
    else begin
      FSettings.AsInteger[root+'Maximized'] := 0;
      FSettings.AsInteger[root+'Top'] := Self.Top;
      FSettings.AsInteger[root+'Left'] := Self.Left;
      FSettings.AsInteger[root+'Width'] := Self.Width;
      FSettings.AsInteger[root+'Height'] := Self.Height;
    end;
  except
  end;
end;

procedure TfrmMain.RunImportTask;
begin
  RunImportExecute(Self);
end;

procedure TfrmMain.RunImportExecute(Sender: TObject);
begin
  if Assigned( FImportProcessing ) and not FImportProcessing.IsRunning then
  begin
    mmReport.Lines.Text := '';
    FImportProcessing.RunImport;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  inherited;
  frmStatus.Hide;
end;

procedure TfrmMain.RunImportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned( FImportProcessing ) and not FImportProcessing.IsRunning and not FScheduledRun;
end;

procedure TfrmMain.AddMessageToImportReport(aMsg: string);
begin
  mmReport.Lines.Add( aMsg );
  Application.ProcessMessages;
end;

procedure TfrmMain.ClearImportReport;
begin
  mmReport.Lines.Clear;
  Application.ProcessMessages;
end;

procedure TfrmMain.SendImportReportByEmail;
begin
  if SmtpConfigFrm.Config.Use then
  begin
    frmStatus.SetMessage('Send import report by email');
    sendmail(SmtpConfigFrm.Config, 'EvoSlavic utility: Import Report', mmReport.Lines.Text, '');
  end;
end;

procedure TfrmMain.ShowReportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned( FImportProcessing ) and not FImportProcessing.IsRunning and
    (mmReport.Lines.Text <> '') and not FScheduledRun;
end;

procedure TfrmMain.ShowReportExecute(Sender: TObject);
begin
  inherited;
  ShowMessage(mmReport.Lines.Text);
end;

procedure TfrmMain.CloseIfScheduledRun;
begin
  if FScheduledRun then
    DeferredCall( CloseAfterScheduledRun );
end;

procedure TfrmMain.CloseAfterScheduledRun;
begin
  Close;
end;

procedure TfrmMain.CheckForScheduledRun;
begin
  if (ParamCount > 0) and (ParamStr(1) = sRunImport) then
  begin
    FScheduledRun := True;
    TabSheet3.TabVisible := False;
    tbshLog.TabVisible := False;
    Self.Caption := Self.Caption + ' <Scheduled Run>';
    RunImportTask;
  end
  else
    FScheduledRun := False;
end;

end.
