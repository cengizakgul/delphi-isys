object frmStatus: TfrmStatus
  Left = 0
  Top = 0
  Width = 405
  Height = 83
  TabOrder = 0
  object gbStatus: TGroupBox
    Left = 0
    Top = 0
    Width = 405
    Height = 83
    Align = alClient
    Caption = 'Status'
    TabOrder = 0
    DesignSize = (
      405
      83)
    object lblMessage: TLabel
      Left = 9
      Top = 19
      Width = 386
      Height = 35
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoSize = False
      Caption = 'lblMessage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object pbProgress: TProgressBar
      Left = 8
      Top = 57
      Width = 388
      Height = 17
      Anchors = [akLeft, akRight, akBottom]
      TabOrder = 0
    end
  end
end
