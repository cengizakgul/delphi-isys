unit ImportProcessing;

interface

uses kbmMemTable, common, DB, SysUtils, CSVLoader, gdyCommonLogger, Classes,
  EvoSlavicProcedures, gdyClasses, Status, evoapiconnection, XmlRpcTypes;

const
  sDeferralFileAlias = 'DeferralFile';
  sEeAddressFileAlias = 'EeAddressFile';

type
  TEvoCompany = record
    CL_NBR: integer;
    CO_NBR: integer;
    Custom_Company_Number: string
  end;

  TEvoEeInfo = record
    CL_PERSON_NBR: integer;
    EE_NBR: integer;
    Address: string;
    City: string;
    State: string;
    ZipCode: string;
    SSN: string;
  end;

  TEvoSchedEDInfo = record
    EE_SCHEDULED_E_DS_NBR: integer;
    CL_E_DS_NBR: integer;
    E_D_CODE_TYPE: string;
    Amount: Currency;
    Percentage: Currency;
    Effective_Start_Date: TDatetime;
    Calculation_Type: string;
  end;

  TStatisticAmounts = record
    TotalImportedED: integer;
    EeAddressUpdated: integer;
    EDUpdated: integer;
    EDInserted: integer;
    ErrorCount: integer;
  end;

  TImportProcessing = class
  private
    FEEAddress: TkbmCustomMemTable;
    FDeferral: TkbmCustomMemTable;
    FClEDCode: TkbmCustomMemTable;
    FStatusProgress: IStatusProgress;
    FIsRunning: boolean;

    FFTPParam: TFtpParam;
    FStat: TStatisticAmounts;

    FEEAddressLoader: ICSVLoader;
    FDeferralLoader: ICSVLoader;
    FLogger: ICommonLogger;
    FConn: IEvoAPIConnection;
    FMainForm: IMainForm;

    FDeferralFileName: string;
    FEeAddressFileName: string;

    F401kGrossEDGroupNbr: integer;
    FM3Data: TkbmCustomMemTable;

    procedure GetClientData;
    procedure ClearStatCounts;
    procedure CreateDatasets;
    procedure LoadFilesFromFTP;

    procedure ImportScheduledEDs;
    procedure ImportEEAddress;

    procedure OpenNextCompany(var aCompany: TEvoCompany);
    function FindEeNbrBySSN(const aSSN: string; aCoNbr: integer; var EeInfo: TEvoEeInfo): boolean;
    procedure UpdateEeAddress(EeInfo: TEvoEeInfo);
    function FindEDNbr(EeNbr: integer; const aEDCode: string; var SchedEDInfo: TEvoSchedEDInfo): boolean;
    function FindSchedEdNbr(EeNbr: integer; var SchedEDInfo: TEvoSchedEDInfo): boolean;
    procedure UpdateSchedED(SchedEDInfo: TEvoSchedEDInfo);
    procedure InsertSchedED(EeInfo: TEvoEeInfo; SchedEDInfo: TEvoSchedEDInfo);
    procedure AddM3SchedEDs(EeInfo: TEvoEeInfo);

    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function ApplyRecordChange(aFields: IRpcStruct; const aTableName, aType: string; aKey: integer): integer;

    function GetIsRunning: boolean;
  public
    constructor Create( aLogger: ICommonLogger; aEeAddressLoader: ICSVLoader; aDeferralLoader: ICSVLoader;
      aStatusProgress: IStatusProgress; aMainForm: IMainForm );
    destructor Destroy; override;

    procedure LoadData;
    procedure RunImport;

    function Connected: boolean;
    procedure Connect;
    procedure Disconnect;
    function TestFTPConnection: boolean;

    property FTPParameters: TFtpParam read FFTPParam write FFTPParam;
    property IsRunning: boolean read GetIsRunning;
  end;

implementation

uses IsFtp, gdyRedir, gdyGlobalWaitIndicator, Math, Variants, SBSimpleSftp,
  SBSftpCommon;

function CreateCurrencyField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TCurrencyField;
var
  TF: TCurrencyField;
begin
  TF := TCurrencyField.Create(aDataSet);
  with TF do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;
  result := TF;
end;

{ TImportProcessing }

procedure TImportProcessing.AddM3SchedEDs(EeInfo: TEvoEeInfo);
var
  SchdEDInfo: TEvoSchedEDInfo;
  Fields: IRpcStruct;
begin
  // search for M3 code, add if not set up yet
  if Assigned(FM3Data) and (FM3Data.RecordCount > 0) and (not FM3Data.FieldByName('CL_E_DS_NBR').IsNull) then
  begin
    FM3Data.First;
    while not FM3Data.Eof do
    begin
      SchdEDInfo.CL_E_DS_NBR := FM3Data.FieldByName('CL_E_DS_NBR').AsInteger;
      if not FindSchedEdNbr( EeInfo.EE_NBR, SchdEDInfo ) then
      begin
        if FM3Data.FieldByName('Scheduled_Defaults').AsString <> 'Y' then
          FLogger.LogWarning('Pension Match E/D code "'+FM3Data.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString+'" should have Scheduled Defaults')
        else begin
          Fields := TRpcStruct.Create;
          try
            Fields.AddItem('EE_NBR', EeInfo.EE_NBR);
            Fields.AddItem('CL_E_DS_NBR', SchdEDInfo.CL_E_DS_NBR);
            Fields.AddItemDateTime('EFFECTIVE_START_DATE', FM3Data.FieldByName('EFFECTIVE_START_DATE').AsDateTime);
            Fields.AddItem('AMOUNT', FM3Data.FieldByName('AMOUNT').AsCurrency);
            Fields.AddItem('CALCULATION_TYPE', FM3Data.FieldByName('CALCULATION_METHOD').AsString);
            Fields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
            Fields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
            Fields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
            if not FM3Data.FieldByName('CL_AGENCY_NBR').IsNull then
              Fields.AddItem('CL_Agency_Nbr', FM3Data.FieldByName('CL_AGENCY_NBR').AsInteger);
            Fields.AddItem('PLAN_TYPE', FM3Data.FieldByName('PLAN_TYPE').AsString);
            Fields.AddItem('FREQUENCY', FM3Data.FieldByName('Frequency').AsString);
            Fields.AddItem('EXCLUDE_WEEK_1', FM3Data.FieldByName('EXCLUDE_WEEK_1').AsString);
            Fields.AddItem('EXCLUDE_WEEK_2', FM3Data.FieldByName('EXCLUDE_WEEK_2').AsString);
            Fields.AddItem('EXCLUDE_WEEK_3', FM3Data.FieldByName('EXCLUDE_WEEK_3').AsString);
            Fields.AddItem('EXCLUDE_WEEK_4', FM3Data.FieldByName('EXCLUDE_WEEK_4').AsString);
            Fields.AddItem('EXCLUDE_WEEK_5', FM3Data.FieldByName('EXCLUDE_WEEK_5').AsString);
            Fields.AddItem('WHICH_CHECKS', FM3Data.FieldByName('WHICH_CHECKS').AsString);
            Fields.AddItem('DEDUCT_WHOLE_CHECK', FM3Data.FieldByName('DEDUCT_WHOLE_CHECK').AsString);
            Fields.AddItem('ALWAYS_PAY', FM3Data.FieldByName('ALWAYS_PAY').AsString);
            Fields.AddItem('DEDUCTIONS_TO_ZERO', FM3Data.FieldByName('DEDUCTIONS_TO_ZERO').AsString);
            Fields.AddItem('USE_PENSION_LIMIT', FM3Data.FieldByName('USE_PENSION_LIMIT').AsString);

            FStatusProgress.SetMessage('Insert Pension Match scheduled E/D code...');
            ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'I', -1 );
            FStat.EDInserted := FStat.EDInserted + 1;
          finally
            Fields := nil;
          end;
        end;  
      end;
      FM3Data.Next;
    end;
  end
  else
    FLogger.LogWarning('401k Deduction was set up, but no Pension Match E/D codes was found.');
end;

function TImportProcessing.ApplyRecordChange(aFields: IRpcStruct;
  const aTableName, aType: string; aKey: integer): integer;
var
  Changes: IRpcArray;
  RowChange, EvoResult: IRpcStruct;
begin
  Result := aKey;
  
  Changes := TRpcArray.Create;
  RowChange := TRpcStruct.Create;
  EvoResult := TRpcStruct.Create;

  RowChange.AddItem('T', aType);
  RowChange.AddItem('D', aTableName);
  if aType = 'U' then
    RowChange.AddItem('K', aKey);

  if aFields.Count > 0 then
  begin
    RowChange.AddItem('F', aFields);

    Changes.AddItem( RowChange );
    EvoResult := FConn.applyDataChangePacket( Changes );

    if Result = -1 then
      if EvoResult.KeyExists('-1') then
        if not TryStrToInt(EvoResult.Keys['-1'].AsString, Result) then
          raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
  end;
end;

procedure TImportProcessing.ClearStatCounts;
begin
  FStat.EeAddressUpdated := 0;
  FStat.EDUpdated := 0;
  FStat.EDInserted := 0;
  FStat.ErrorCount := 0;
  FStat.TotalImportedED := 0;
end;

procedure TImportProcessing.Connect;
begin
  Disconnect;
  WaitIndicator.StartWait('Connecting to Evolution');
  FLogger.LogEntry('Connection to Evo');
  try
    if FMainForm.GetEvoConnectionFrame.IsValid then
    try
      FConn := CreateEvoAPIConnection(FMainForm.GetEvoConnectionFrame.Param, FLogger);
      FMainForm.AddMessageToImportReport('Connected to Evo...');
    except
      on E: Exception do
      begin
        FLogger.LogError('Evo Connection error', E.Message);
        FMainForm.AddMessageToImportReport('Evo Connection error: ' + E.Message);
        FConn := nil;
      end;
    end
    else
      FLogger.LogError('Evo Connection settings are invalid');
  finally
    FLogger.LogExit;
    WaitIndicator.EndWait;
  end;
end;

function TImportProcessing.Connected: boolean;
begin
  Result := assigned(FConn);
end;

constructor TImportProcessing.Create( aLogger: ICommonLogger;
  aEeAddressLoader: ICSVLoader; aDeferralLoader: ICSVLoader; aStatusProgress: IStatusProgress;
  aMainForm: IMainForm );
begin
  FLogger := aLogger;
  FEEAddressLoader := aEeAddressLoader;
  FDeferralLoader := aDeferralLoader;
  FStatusProgress := aStatusProgress;
  CreateDatasets;

  FDeferralFileName := Redirection.GetFilename(sDeferralFileAlias);
  FEeAddressFileName := Redirection.GetFilename(sEeAddressFileAlias);

  FMainForm := aMainForm;
  FStatusProgress.Clear;
end;

procedure TImportProcessing.CreateDatasets;
begin
  FEEAddress := TkbmCustomMemTable.Create(nil);
  FDeferral := TkbmCustomMemTable.Create(nil);

  // Ee Address Change File
  CreateStringField(FEEAddress, 'CUSTOM_COMPANY_NUMBER', 'Company', 9);
  CreateStringField(FEEAddress, 'SOCIAL_SECURITY_NUMBER', 'SSN', 9);
  CreateStringField(FEEAddress, 'ADDRESS1', 'Street', 50);
  CreateStringField(FEEAddress, 'CITY', 'City', 50);
  CreateStringField(FEEAddress, 'STATE', 'State', 2);
  CreateStringField(FEEAddress, 'ZIPCODE', 'ZIP', 10);

  // Deferral/Enrollment File
  CreateStringField(FDeferral, 'CUSTOM_COMPANY_NUMBER', 'Company', 9);
  CreateStringField(FDeferral, 'SOCIAL_SECURITY_NUMBER', 'SSN', 9);
  CreateStringField(FDeferral, 'CUSTOM_E_D_CODE_NUMBER', 'E/D Code', 10);
  CreateDateTimeField(FDeferral, 'EFFECTIVE_START_DATE', 'Entry Date');
  CreateStringField(FDeferral, 'CONTRIB_TYPE', 'Contribution Type', 1);  // 0- Amount (Fixed = 'F'), 1- Percent (% of Gross = 'G')
  CreateCurrencyField(FDeferral, 'AMT', 'Amount');
end;

destructor TImportProcessing.Destroy;
begin
  FreeAndNil( FEEAddress );
  FreeAndNil( FDeferral );
  FreeAndNil( FClEDCode );
  if Assigned(FM3Data) then
    FreeAndNil(FM3Data);

  inherited;
end;

procedure TImportProcessing.Disconnect;
begin
  FConn := nil;
end;

function TImportProcessing.FindEDNbr(EeNbr: integer; const aEDCode: string;
  var SchedEDInfo: TEvoSchedEDInfo): boolean;
var
  Param: IRpcStruct;
begin
  Result := False;
  FStatusProgress.SetMessage('Search E/D code: '+aEDCode);
  if Assigned( FClEDCode ) then
    FreeAndNil( FClEDCode );
  param := TRpcStruct.Create;
  Param.AddItem( 'EdCode', aEDCode );
  FClEDCode := RunQuery('EDQuery.rwq', Param);
  try
    if (FClEDCode.RecordCount > 0) and Assigned(FClEDCode.FindField('Cl_E_Ds_Nbr')) then
    begin
      FStatusProgress.SetMessage('E/D code: '+aEDCode+' is found!');
      SchedEDInfo.CL_E_DS_NBR := FClEDCode.FindField('Cl_E_Ds_Nbr').AsInteger;
      SchedEDInfo.E_D_CODE_TYPE := FClEDCode.FindField('E_D_Code_Type').AsString;
      Result := True;
    end
  finally
    Param := nil;
  end;
end;

function TImportProcessing.FindEeNbrBySSN(const aSSN: string;
  aCoNbr: integer; var EeInfo: TEvoEeInfo): boolean;
var
  FEeData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := False;
  FStatusProgress.SetMessage('Search EE by SSN: '+aSSN);

  param := TRpcStruct.Create;
  Param.AddItem( 'SSN', Copy(aSSN, 1, 3) + '-' + Copy(aSSN, 4, 2) + '-' + Copy(aSSN, 6, 4) );
  Param.AddItem( 'CoNbr', aCoNbr );
  FEeData := RunQuery('EeQuery.rwq', Param);

  try
    if (FEeData.RecordCount > 0) and Assigned(FEeData.FindField('EE_NBR')) then
    begin
      EeInfo.EE_NBR := FEeData.FindField('EE_NBR').AsInteger;
      EeInfo.CL_PERSON_NBR := FEeData.FindField('CL_PERSON_NBR').AsInteger;
      EeInfo.Address := FEeData.FindField('ADDRESS1').AsString;
      EeInfo.City := FEeData.FindField('CITY').AsString;
      EeInfo.State := FEeData.FindField('STATE').AsString;
      EeInfo.ZipCode := FEeData.FindField('ZIP_CODE').AsString;
      EeInfo.SSN := aSSN;
      Result := True;
      FStatusProgress.SetMessage('Found!');
    end
    else
      FStatusProgress.SetMessage('Not found!');
  finally
    FreeAndNil( FEeData );
  end;
end;

function TImportProcessing.FindSchedEdNbr(EeNbr: integer;
  var SchedEDInfo: TEvoSchedEDInfo): boolean;
var
  FEeSchedED: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := False;
  FStatusProgress.SetMessage('Search scheduled E/D code...');
  param := TRpcStruct.Create;
  Param.AddItem( 'Cl_E_DS_NBR', SchedEDInfo.CL_E_DS_NBR );
  Param.AddItem( 'EeNbr', EeNbr );
  FEeSchedED := RunQuery('SchedEDQuery.rwq', Param);
  try
    if (FEeSchedED.RecordCount > 0) and Assigned(FEeSchedED.FindField('Ee_Scheduled_E_Ds_Nbr')) then
    begin
      SchedEDInfo.EE_SCHEDULED_E_DS_NBR := FEeSchedED.FindField('Ee_Scheduled_E_Ds_Nbr').AsInteger;
      SchedEDInfo.Amount := FEeSchedED.FindField('AMOUNT').AsFloat;
      SchedEDInfo.Percentage := FEeSchedED.FindField('Percentage').AsFloat;
      SchedEDInfo.Effective_Start_Date := FEeSchedED.FindField('Effective_Start_Date').AsDateTime;
      SchedEDInfo.Calculation_Type := FEeSchedED.FindField('Calculation_Type').AsString;
      Result := True;
    end
  finally
    FStatusProgress.SetMessage('');
    FreeAndNil( FEeSchedED );
  end;
end;

procedure TImportProcessing.GetClientData;
var
  FData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  F401kGrossEDGroupNbr := -1;

  param := TRpcStruct.Create;
  Param.AddItem('pName', '401k Gross' );
  FData := RunQuery('GetClEDGroupByName.rwq', Param);
  try
    if (FData.RecordCount > 0) and Assigned(FData.FindField('GroupNbr')) and (not FData.FindField('GroupNbr').IsNull) then
      F401kGrossEDGroupNbr := FData.FindField('GroupNbr').AsInteger;
  finally
    FreeAndNil( FData );
  end;

  if Assigned(FM3Data) then
    FreeAndNil(FM3Data);
  FM3Data := RunQuery('M3EdQuery.rwq');
end;

function TImportProcessing.GetIsRunning: boolean;
begin
  Result := FIsRunning;
end;

procedure TImportProcessing.ImportEEAddress;
var
  CurrentCompany: TEvoCompany;
  EeInfo: TEvoEeInfo;
begin
  ClearStatCounts;

  FMainForm.AddMessageToImportReport(' ');
  FMainForm.AddMessageToImportReport('Import Employee Address data... (' + FFTPParam.EeAddressFile + ')');
  FLogger.LogEntry('Import Employee Address data');
  try
    if (FEEAddress.Active) and (FEEAddress.RecordCount > 0) then
    begin
      FEEAddress.SortFields := 'CUSTOM_COMPANY_NUMBER;SOCIAL_SECURITY_NUMBER';
      FEEAddress.First;
      CurrentCompany.Custom_Company_Number := '';

      FStatusProgress.SetProgressCount( FEEAddress.RecordCount );
      FStatusProgress.SetMessage('Import data');

      while not FEEAddress.Eof do
      begin
        try
          // check if need openClient
          if CurrentCompany.Custom_Company_Number <> FEEAddress.FieldByName('CUSTOM_COMPANY_NUMBER').AsString then
          begin
            CurrentCompany.Custom_Company_Number := FEEAddress.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
            OpenNextCompany( CurrentCompany );
            FMainForm.AddMessageToImportReport('Import data to the company #' + CurrentCompany.Custom_Company_Number);
          end;

          // get Ee_Nbr, if exists - go next, put error to the log otherwise
          if not FindEeNbrBySSN( FEEAddress.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, CurrentCompany.CO_NBR, EeInfo ) then
            FLogger.LogWarning('Employee with SSN: ' + FEEAddress.FieldByName('SOCIAL_SECURITY_NUMBER').AsString + ' is not found in Company: ' + CurrentCompany.Custom_Company_Number)
          else
            UpdateEeAddress(EeInfo);
        except
          on E: Exception do
          begin
            FStat.ErrorCount := FStat.ErrorCount + 1;
            FLogger.LogError('Error importing data', E.Message);
          end;
        end;
        FStatusProgress.IncProgress('');

        FEEAddress.Next;
      end;
    end
    else
      FLogger.LogEvent('Employee address data file is empty');
  finally
    FLogger.LogExit;
    FMainForm.AddMessageToImportReport('--------------------------------------------------');
    if FEEAddress.RecordCount = 0 then
      FMainForm.AddMessageToImportReport('Ee Address file: No Activity')
    else begin
      FMainForm.AddMessageToImportReport(Format('Employee address updated: %d', [FStat.EeAddressUpdated]));
      FMainForm.AddMessageToImportReport(Format('Errors: %d', [FStat.ErrorCount]));
    end;
  end;
end;

procedure TImportProcessing.ImportScheduledEDs;
var
  CurrentCompany: TEvoCompany;
  EeInfo: TEvoEeInfo;
  SchedEDInfo: TEvoSchedEDInfo;
begin
  ClearStatCounts;

  FMainForm.AddMessageToImportReport(' ');
  FMainForm.AddMessageToImportReport('Import Deferral/Enrollment data... (' + FFTPParam.DeferralFile + ')');
  FLogger.LogEntry('Import Deferral/Enrollment data');
  try
    if (FDeferral.Active) and (FDeferral.RecordCount > 0) then
    begin
      FDeferral.SortFields := 'CUSTOM_COMPANY_NUMBER;SOCIAL_SECURITY_NUMBER';
      FDeferral.First;
      CurrentCompany.Custom_Company_Number := '';
      CurrentCompany.CL_NBR := -1;

      FStatusProgress.SetProgressCount( FDeferral.RecordCount );
      FStatusProgress.SetMessage('Import data');

      while not FDeferral.Eof do
      begin
        FStatusProgress.SetMessage(Format('Import "%s" E/D code', [FDeferral.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString]));
        try
          // check if need openClient

          if CurrentCompany.Custom_Company_Number <> FDeferral.FieldByName('CUSTOM_COMPANY_NUMBER').AsString then
          begin
            CurrentCompany.Custom_Company_Number := FDeferral.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
            OpenNextCompany( CurrentCompany );
            FMainForm.AddMessageToImportReport('Import data to the company #' + CurrentCompany.Custom_Company_Number);
            GetClientData;
          end;

          if CurrentCompany.CL_NBR > -1 then
          begin
            // get Ee_Nbr, if exists - go next, put error to the log otherwise
            if not FindEeNbrBySSN( FDeferral.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, CurrentCompany.CO_NBR, EeInfo ) then
              FLogger.LogWarning('Employee with SSN: ' + FDeferral.FieldByName('SOCIAL_SECURITY_NUMBER').AsString + ' is not found in Company: ' + CurrentCompany.Custom_Company_Number)
            else if not FindEDNbr( EeInfo.EE_NBR, FDeferral.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString, SchedEDInfo ) then
              FLogger.LogWarning('Client E/Ds code: ' + FDeferral.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString + ' is not found')
            else begin
              // get Scheduled E/Ds nbr, if exists - update, insert owerwise
              if FindSchedEdNbr( EeInfo.EE_NBR, SchedEDInfo ) then
                UpdateSchedED( SchedEDInfo )
              else
                InsertSchedED( EeInfo, SchedEDInfo );

              if SchedEDInfo.E_D_CODE_TYPE = 'DA' then // 401k deduction
                AddM3SchedEDs(EeInfo);  
            end;
            FStat.TotalImportedED := FStat.TotalImportedED + 1;
          end
          else
            FLogger.LogWarning('Company: ' + CurrentCompany.Custom_Company_Number + ' is not found!');
        except
          on E: Exception do
          begin
            FStat.ErrorCount := FStat.ErrorCount + 1;
            FLogger.LogError('Error importing data', E.Message);
          end;
        end;
        FStatusProgress.IncProgress('');

        FDeferral.Next;
      end;
    end
    else
      FLogger.LogEvent('Deferral data file is empty');
  finally
    FLogger.LogExit;
    FMainForm.AddMessageToImportReport('--------------------------------------------------');
    if FDeferral.RecordCount = 0 then
      FMainForm.AddMessageToImportReport('Deferral/Enrollment file: No Activity')
    else begin
      FMainForm.AddMessageToImportReport(Format('Total Deferral/Enrollment processed: %d', [FStat.TotalImportedED]));
      FMainForm.AddMessageToImportReport(Format('Scheduled E/D Codes inserted: %d', [FStat.EDInserted]));
      FMainForm.AddMessageToImportReport(Format('Scheduled E/D Codes updated: %d', [FStat.EDUpdated]));
      FMainForm.AddMessageToImportReport(Format('Employee address updated: %d', [FStat.EeAddressUpdated]));
      FMainForm.AddMessageToImportReport(Format('Errors: %d', [FStat.ErrorCount]));
    end;
  end;
end;

procedure TImportProcessing.InsertSchedED(EeInfo: TEvoEeInfo; SchedEDInfo: TEvoSchedEDInfo);
var
  Fields: IRpcStruct;
begin
  // check if Scheduled ED need change
  Fields := TRpcStruct.Create;
  try
    Fields.AddItem('EE_NBR', EeInfo.EE_NBR);
    Fields.AddItem('CL_E_DS_NBR', SchedEDInfo.CL_E_DS_NBR);
    Fields.AddItemDateTime('EFFECTIVE_START_DATE', FDeferral.FieldByName('EFFECTIVE_START_DATE').AsDateTime);

    if FDeferral.FieldByName('CONTRIB_TYPE').AsString = '0' then // "Fixed"
    begin
      Fields.AddItem('AMOUNT', FDeferral.FieldByName('AMT').AsCurrency);
      Fields.AddItem('CALCULATION_TYPE', 'F');
    end
    else if FDeferral.FieldByName('CONTRIB_TYPE').AsString = '1' then // "% of E/D Group Amount"
    begin
      Fields.AddItem('PERCENTAGE', FDeferral.FieldByName('AMT').AsCurrency);
      Fields.AddItem('CALCULATION_TYPE', 'P');
      if F401kGrossEDGroupNbr > -1 then
        Fields.AddItem('CL_E_D_Groups_Nbr', F401kGrossEDGroupNbr);
    end;

    Fields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
    Fields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
    Fields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');

    if not FClEDCode.FieldByName('CL_AGENCY_NBR').IsNull then
      Fields.AddItem('CL_Agency_Nbr', FClEDCode.FieldByName('CL_AGENCY_NBR').AsInteger);

    if FClEDCode.FieldByName('Scheduled_Defaults').AsString = 'Y' then
    begin
      Fields.AddItem('PLAN_TYPE', FClEDCode.FieldByName('PLAN_TYPE').AsString);
      Fields.AddItem('FREQUENCY', FClEDCode.FieldByName('Frequency').AsString);
      Fields.AddItem('EXCLUDE_WEEK_1', FClEDCode.FieldByName('EXCLUDE_WEEK_1').AsString);
      Fields.AddItem('EXCLUDE_WEEK_2', FClEDCode.FieldByName('EXCLUDE_WEEK_2').AsString);
      Fields.AddItem('EXCLUDE_WEEK_3', FClEDCode.FieldByName('EXCLUDE_WEEK_3').AsString);
      Fields.AddItem('EXCLUDE_WEEK_4', FClEDCode.FieldByName('EXCLUDE_WEEK_4').AsString);
      Fields.AddItem('EXCLUDE_WEEK_5', FClEDCode.FieldByName('EXCLUDE_WEEK_5').AsString);
      Fields.AddItem('WHICH_CHECKS', FClEDCode.FieldByName('WHICH_CHECKS').AsString);
      Fields.AddItem('DEDUCT_WHOLE_CHECK', FClEDCode.FieldByName('DEDUCT_WHOLE_CHECK').AsString);
      Fields.AddItem('ALWAYS_PAY', FClEDCode.FieldByName('ALWAYS_PAY').AsString);
      Fields.AddItem('DEDUCTIONS_TO_ZERO', FClEDCode.FieldByName('DEDUCTIONS_TO_ZERO').AsString);
      Fields.AddItem('USE_PENSION_LIMIT', FClEDCode.FieldByName('USE_PENSION_LIMIT').AsString);
    end
    else begin
      Fields.AddItem('PLAN_TYPE', 'N');
      Fields.AddItem('FREQUENCY', 'P');
      Fields.AddItem('EXCLUDE_WEEK_1', 'N');
      Fields.AddItem('EXCLUDE_WEEK_2', 'N');
      Fields.AddItem('EXCLUDE_WEEK_3', 'N');
      Fields.AddItem('EXCLUDE_WEEK_4', 'N');
      Fields.AddItem('EXCLUDE_WEEK_5', 'N');
      Fields.AddItem('WHICH_CHECKS', 'A');
      Fields.AddItem('DEDUCT_WHOLE_CHECK', 'N');
      Fields.AddItem('ALWAYS_PAY', 'N');
      Fields.AddItem('DEDUCTIONS_TO_ZERO', 'N');
      Fields.AddItem('USE_PENSION_LIMIT', 'N');
    end;

    FStatusProgress.SetMessage('Insert scheduled E/D code...');
    ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'I', -1 );
    FStat.EDInserted := FStat.EDInserted + 1;
  finally
    Fields := nil;
  end;
end;

procedure TImportProcessing.LoadData;
begin
  FStatusProgress.SetMessage('Load data');

  // load from FTP
  LoadFilesFromFTP;

  try
    FDeferralLoader.LoadData( FDeferralFileName, FDeferral, FLogger );
  except
    on E: Exception do
      FLogger.LogError('Load Deferral/Enrollment file', E.Message)
  end;

  try
    FEEAddressLoader.LoadData( FEeAddressFileName, FEEAddress, FLogger );
  except
    on E: Exception do
      FLogger.LogError('Load Employee Address file', E.Message)
  end;
end;

procedure TImportProcessing.LoadFilesFromFTP;
var
  MyFtp: TIsFTP;
  MySFtp: TElSimpleSFTPClient;
begin
  WaitIndicator.StartWait('Download files from FTP...');
  FLogger.LogEntry('Download files from FTP...');
  try
    if FFTPParam.SecureFTP then
    begin
      MySFtp := TElSimpleSFTPClient.Create(nil);
      try
        try
          MySFtp.Address := FFTPParam.FtpServerAddress;
          MySFtp.Username := FFTPParam.Username;
          MySFtp.Password := FFTPParam.Password;
          MySFTp.Port := FFTPParam.Port;
          MySFtp.Open;
          FLogger.LogEvent('Connected to SFTP Server: ' + FFTPParam.FtpServerAddress);

          if Trim(FFTPParam.DeferralFile) <> '' then
          try
            if FileExists( FDeferralFileName ) then
              DeleteFile( FDeferralFileName );
            MySFtp.DownloadFile(FFTPParam.Folder + '\' + FFTPParam.DeferralFile, FDeferralFileName);
            FLogger.LogEvent('Downloaded Deferral/Enrolment file: ' + FFTPParam.DeferralFile);
          except
            on E: Exception do
              FLogger.LogError('Deferral/Enrolment file downloading failed', E.Message)
          end
          else
            FLogger.LogEvent('Deferral/Enrolment file is not specified');

          if Trim(FFTPParam.EeAddressFile) <> '' then
          try
            if FileExists( FEeAddressFileName ) then
              DeleteFile( FEeAddressFileName );
            MySFtp.DownloadFile(FFTPParam.Folder + '\' + FFTPParam.EeAddressFile, FEeAddressFileName);
            FLogger.LogEvent('Downloaded Employee Address file: ' + FFTPParam.EeAddressFile);
          except
            on E: Exception do
              FLogger.LogError('Employee Address file downloading failed', E.Message)
          end
          else
            FLogger.LogEvent('Employee Address file is not specified');

          MySFtp.Close;
        except
          on E: Exception do
            FLogger.LogError('Downloading failed', E.Message)
        end;
      finally
        MySFtp.Free;
      end;
    end
    else begin
      MyFtp := TIsFTP.Create;
      try
        try
          MyFtp.Host := FFTPParam.FtpServerAddress;
          MyFtp.User := FFTPParam.Username;
          MyFtp.Password := FFTPParam.Password;
          MyFtp.Port := FFTPParam.Port;
          MyFtp.Connect;
          FLogger.LogEvent('Connected to FTP Server: ' + FFTPParam.FtpServerAddress);

          if Trim(FFTPParam.DeferralFile) <> '' then
          try
            if FileExists( FDeferralFileName ) then
              DeleteFile( FDeferralFileName );
            MyFtp.Get(FDeferralFileName, FFTPParam.Folder + '\' + FFTPParam.DeferralFile, True);
            FLogger.LogEvent('Downloaded Deferral/Enrolment file: ' + FFTPParam.DeferralFile);
          except
            on E: Exception do
              FLogger.LogError('Deferral/Enrolment file downloading failed', E.Message)
          end
          else
            FLogger.LogEvent('Deferral/Enrolment file is not specified');

          if Trim(FFTPParam.EeAddressFile) <> '' then
          try
            if FileExists( FEeAddressFileName ) then
              DeleteFile( FEeAddressFileName );
            MyFtp.Get(FEeAddressFileName, FFTPParam.Folder + '\' + FFTPParam.EeAddressFile, True);
            FLogger.LogEvent('Downloaded Employee Address file: ' + FFTPParam.EeAddressFile);
          except
            on E: Exception do
              FLogger.LogError('Employee Address file downloading failed', E.Message)
          end
          else
            FLogger.LogEvent('Employee Address file is not specified');

          MyFtp.Disconnect;
        except
          on E: Exception do
            FLogger.LogError('Downloading failed', E.Message)
        end;
      finally
        MyFtp.Free;
      end;
    end;
  finally
    FLogger.LogExit;
    WaitIndicator.EndWait;
  end;
end;

procedure TImportProcessing.OpenNextCompany(var aCompany: TEvoCompany);
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem( 'CoNumber', aCompany.Custom_Company_Number );
  FClCoData := RunQuery('TmpCoQuery.rwq', Param);
  try
    if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
    begin
      aCompany.CL_NBR := FClCoData.FindField('CL_NBR').AsInteger;
      aCompany.CO_NBR := FClCoData.FindField('CO_NBR').AsInteger;

      FConn.OpenClient(aCompany.CL_NBR);
    end;
  finally
    FreeAndNil( FClCoData );
  end;
end;

procedure TImportProcessing.RunImport;
begin
  FIsRunning := True;
  FStatusProgress.Clear;
  FStatusProgress.Show;
  try
    LoadData;

    if not FDeferral.Active or ((FDeferral.Active) and (FDeferral.RecordCount = 0)) then
    begin
      FMainForm.AddMessageToImportReport(' ');
      FMainForm.AddMessageToImportReport('Import Deferral/Enrollment data... (' + FFTPParam.DeferralFile + ')');
      FMainForm.AddMessageToImportReport('No Activity');
    end
    else begin
      if not Connected then
        Connect;
      if Connected then
        ImportScheduledEDs;
    end;

    if not FEEAddress.Active or ((FEEAddress.Active) and (FEEAddress.RecordCount = 0)) then
    begin
      FMainForm.AddMessageToImportReport(' ');
      FMainForm.AddMessageToImportReport('Import Employee Address data... (' + FFTPParam.EeAddressFile + ')');
      FMainForm.AddMessageToImportReport('No Activity');
    end
    else begin
      if not Connected then
        Connect;
      if Connected then
        ImportEEAddress;
    end;
  finally
    FMainForm.SendImportReportByEmail;
    FIsRunning := False;
    FStatusProgress.Clear;
    FStatusProgress.Hide;
    FMainForm.CloseIfScheduledRun;
  end;
end;

function TImportProcessing.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FConn.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

function TImportProcessing.TestFTPConnection: boolean;
var
  MyFtp: TIsFTP;
  MySFtp: TElSimpleSFTPClient;
begin
  Result := False;
  WaitIndicator.StartWait('Test FTP connection...');
  FLogger.LogEntry('Test FTP connection...');
  try
    if FFTPParam.SecureFTP then
    begin
      MySFtp := TElSimpleSFTPClient.Create(nil);
      try
        try
          MySFtp.Address := FFTPParam.FtpServerAddress;
          MySFtp.Username := FFTPParam.Username;
          MySFtp.Password := FFTPParam.Password;
          MySFTp.Port := FFTPParam.Port;
          MySFtp.Open;

          Result := True;
          FLogger.LogEvent('Connected to FTP Server: ' + FFTPParam.FtpServerAddress);
          MySFtp.Close;
        except
          on E: Exception do
            FLogger.LogError('Connection failed', E.Message)
        end;
      finally
        MySFtp.Free;
      end;
    end
    else begin
      MyFtp:=TIsFTP.Create;
      try
        try
          MyFtp.Host := FFTPParam.FtpServerAddress;
          MyFtp.User := FFTPParam.Username;
          MyFtp.Password := FFTPParam.Password;
          MyFtp.Connect;
          FLogger.LogEvent('Connected to FTP Server: ' + FFTPParam.FtpServerAddress);
          Result := True;
          MyFtp.Disconnect;
        except
          on E: Exception do
          begin
            FLogger.LogError('Connection failed!', E.Message);
            Result := False;
          end;
        end;
      finally
        MyFTP.Free;
      end;
    end;  
  finally
    FLogger.LogExit;
    WaitIndicator.EndWait;
  end;
end;

procedure TImportProcessing.UpdateEeAddress(EeInfo: TEvoEeInfo);
var
  Fields: IRpcStruct;
begin
  // check if address is changed
  Fields := TRpcStruct.Create;
  try
    if EeInfo.Address <> FEEAddress.FieldByName('ADDRESS1').AsString then
      Fields.AddItem('ADDRESS1', FEEAddress.FieldByName('ADDRESS1').AsString );
    if EeInfo.City <> FEEAddress.FieldByName('CITY').AsString then
      Fields.AddItem('CITY', FEEAddress.FieldByName('CITY').AsString );
    if EeInfo.State <> FEEAddress.FieldByName('STATE').AsString then
      Fields.AddItem('STATE', FEEAddress.FieldByName('STATE').AsString );
    if EeInfo.ZipCode <> FEEAddress.FieldByName('ZIPCODE').AsString then
      Fields.AddItem('ZIP_CODE', FEEAddress.FieldByName('ZIPCODE').AsString );

    if Fields.Count > 0 then
    begin
      FStatusProgress.SetMessage('Update EE Address (SSN: '+EeInfo.SSN+')');
      ApplyRecordChange( Fields, 'CL_PERSON', 'U', EeInfo.CL_PERSON_NBR );
      FStat.EeAddressUpdated := FStat.EeAddressUpdated + 1;
    end;
  finally
    Fields := nil;
  end;
end;

procedure TImportProcessing.UpdateSchedED(SchedEDInfo: TEvoSchedEDInfo);
var
  Fields: IRpcStruct;
begin
  // check if Scheduled ED need change
  Fields := TRpcStruct.Create;
  if SchedEDInfo.Effective_Start_Date <> FDeferral.FieldByName('EFFECTIVE_START_DATE').AsDateTime then
    Fields.AddItemDateTime('EFFECTIVE_START_DATE', FDeferral.FieldByName('EFFECTIVE_START_DATE').AsDateTime);

  if FDeferral.FieldByName('CONTRIB_TYPE').AsString = '0' then //Fixed
  begin
    if SchedEDInfo.Calculation_Type = 'F' then
    begin
      if SchedEDInfo.Amount <> FDeferral.FieldByName('AMT').AsCurrency then
        Fields.AddItem('AMOUNT', FDeferral.FieldByName('AMT').AsCurrency);
    end
    else begin
      Fields.AddItem('CALCULATION_TYPE', 'F');
      Fields.AddItem('AMOUNT', FDeferral.FieldByName('AMT').AsCurrency);
      Fields.AddItem('PERCENTAGE', '#NULL#');
      Fields.AddItem('CL_E_D_Groups_Nbr', '#NULL#');
    end;
  end
  else if FDeferral.FieldByName('CONTRIB_TYPE').AsString = '1' then //% of E/D Group Amount
  begin
    if SchedEDInfo.Calculation_Type = 'P' then
    begin
      if SchedEDInfo.Percentage <> FDeferral.FieldByName('AMT').AsCurrency then
        Fields.AddItem('PERCENTAGE', FDeferral.FieldByName('AMT').AsCurrency);
    end
    else begin
      Fields.AddItem('CALCULATION_TYPE', 'P');
      Fields.AddItem('AMOUNT', '#NULL#');
      Fields.AddItem('PERCENTAGE', FDeferral.FieldByName('AMT').AsCurrency);
      if F401kGrossEDGroupNbr > -1 then
        Fields.AddItem('CL_E_D_Groups_Nbr', F401kGrossEDGroupNbr);
    end;
  end;

  if Fields.Count > 0 then
  begin
    FStatusProgress.SetMessage('Update Scheduled E/D code');
    ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'U', SchedEDInfo.EE_SCHEDULED_E_DS_NBR );
    FStat.EDUpdated := FStat.EDUpdated + 1;
  end;
end;

end.
