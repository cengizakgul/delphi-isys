program EvoSlavic;
                                    
uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  EvoAPIConnection,
  main in 'main.pas' {frmMain},
  EvoSlavicProcedures in 'EvoSlavicProcedures.pas',
  ImportProcessing in 'ImportProcessing.pas',
  CSVLoader in 'CSVLoader.pas' {frmCSvloader: TFrame},
  Status in 'Status.pas' {frmStatus: TFrame},
  SimpleScheduler in 'SimpleScheduler.pas' {frmSimpleScheduler: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  csreader in 'csreader.pas',
  EvoAPIClientMainForm in '..\..\Common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  OptionsBaseFrame in '..\..\Common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  EvoAPIConnectionParamFrame in '..\..\Common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame};

{$R *.res}

begin
  LicenseKey := '2FF03CA07D864A4BA3BC5A1FC6FD6997'; // EvoSlavic License
//  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License
//  LicenseKey := '{5E465AFC-D0A1-424E-B21B-4351ECD6BFFF}'; //evo.legacy.*

  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
