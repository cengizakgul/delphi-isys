object frmSimpleScheduler: TfrmSimpleScheduler
  Left = 0
  Top = 0
  Width = 351
  Height = 132
  TabOrder = 0
  object gbSchedule: TGroupBox
    Left = 0
    Top = 0
    Width = 351
    Height = 132
    Align = alClient
    Caption = 'Scheduler'
    TabOrder = 0
    DesignSize = (
      351
      132)
    object lblScheduleInfo: TLabel
      Left = 16
      Top = 24
      Width = 322
      Height = 63
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoSize = False
      Caption = '<no scheuled "Import">'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      OnDblClick = lblScheduleInfoDblClick
    end
    object btnSchedule: TBitBtn
      Left = 16
      Top = 94
      Width = 75
      Height = 25
      Action = Schedule
      Anchors = [akLeft, akBottom]
      Caption = 'Schedule'
      TabOrder = 0
    end
    object btnClear: TBitBtn
      Left = 96
      Top = 94
      Width = 75
      Height = 25
      Action = Clear
      Anchors = [akLeft, akBottom]
      Caption = 'Clear'
      TabOrder = 1
    end
  end
  object Actions: TActionList
    Left = 216
    Top = 96
    object Schedule: TAction
      Caption = 'Schedule'
      OnExecute = ScheduleExecute
    end
    object Clear: TAction
      Caption = 'Clear'
      OnExecute = ClearExecute
    end
  end
end
