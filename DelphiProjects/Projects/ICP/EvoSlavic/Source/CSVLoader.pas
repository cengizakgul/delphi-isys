unit CSVLoader;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, Grids, Wwdbigrd, Wwdbgrid, dbcomp, csreader, gdyCommonLogger;

type
  ICSVLoader = interface
    function LoadData(const aFileName: string; aDS: TDataset; Logger: ICommonLogger): boolean;
  end;

  TfrmCSvloader = class(TFrame, ICSVLoader)
    dbgData: TReDBGrid;
    dsCSVData: TDataSource;
  private
    FLogger: ICommonLogger;
    procedure ProcessRecord( rec: TStrings );
  public
    function LoadData(const aFileName: string; aDS: TDataset; Logger: ICommonLogger): boolean;
  end;

implementation

uses Math;

{$R *.dfm}

function PadStringLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := MyString;
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

{ TfrmCSvloader }

function TfrmCSvloader.LoadData(const aFileName: string; aDS: TDataset;
  Logger: ICommonLogger): boolean;
begin
  Result := False;
  dsCSVData.DataSet := aDS;
  FLogger := Logger;

  Logger.LogEntry('Load csv file: ' + aFileName);
  try

    if not FileExists( aFileName ) then
      Logger.LogWarning('"' + aFileName + '" is not found!')
    else begin
      if Assigned(dsCSVData.DataSet) then
      begin
        dsCSVData.DataSet.Close;
        dsCSVData.DataSet.Open;

        EachRecord(aFileName, ProcessRecord, True);
      end;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TfrmCSvloader.ProcessRecord(rec: TStrings);
var
  i: integer;
  curValue: Currency;
  datValue: TDatetime;
  bPost: boolean;
  sErrorMsg: string;
begin
  sErrorMsg := 'The record "' + rec.CommaText + '" has not been processed.';
  if UpperCase(Trim( rec.Text )) = 'NO ACTIVITY' then
    exit;
     
  if rec.Count <> dsCSVData.DataSet.FieldCount then
    FLogger.LogError(sErrorMsg, 'There should be ' + IntToStr( dsCSVData.DataSet.FieldCount ) + ' columns.')
  else
  with dsCSVData do
  begin
    bPost := True;
    DataSet.Append;
    for i := 0 to rec.Count - 1 do
    try
      if DataSet.Fields[i] is TStringField then
        DataSet.Fields[i].AsString := rec[i]
      else if DataSet.Fields[i] is TCurrencyField then
      begin
        if TryStrToCurr(rec[i], curValue) then
          DataSet.Fields[i].AsCurrency := curValue
        else
          FLogger.LogError(sErrorMsg, DataSet.Fields[i].FieldName + ' is currency field, but "' + rec[i] + '" is not currency value!');
      end
      else if DataSet.Fields[i] is TDateTimeField then
      begin
        if TryStrToDate(rec[i], datValue) then
          DataSet.Fields[i].AsDateTime := datValue
        else
          FLogger.LogError(sErrorMsg, DataSet.Fields[i].FieldName + ' is date field, but "' + rec[i] + '" is not date value!');
      end;
    except
      on E: Exception do
      begin
        bPost := False;
        FLogger.LogError(sErrorMsg, E.Message);
      end;
    end;
    if bPost then
      DataSet.Post
    else
      DataSet.Cancel;
  end;
end;

end.
