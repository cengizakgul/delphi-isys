unit SimpleScheduler;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ActnList, jclTask, EvoSlavicProcedures, gdycommon,
  comobj;

type
  TfrmSimpleScheduler = class(TFrame)
    gbSchedule: TGroupBox;
    lblScheduleInfo: TLabel;
    btnSchedule: TBitBtn;
    btnClear: TBitBtn;
    Actions: TActionList;
    Schedule: TAction;
    Clear: TAction;
    procedure lblScheduleInfoDblClick(Sender: TObject);
    procedure ScheduleExecute(Sender: TObject);
    procedure ClearExecute(Sender: TObject);
  private
    FSysScheduler: TJclTaskSchedule;
    FParameter: string;

    function GetNextRunTime(aTime: TSystemTime): string;
    procedure EditAndSave(aTask: TJclScheduledTask);
  public
    procedure RefreshTaskInfo;
    procedure ClearAll;
    procedure ScheduleTask;
    procedure Init(const Parameter: string);
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

{ TfrmSimpleScheduler }

destructor TfrmSimpleScheduler.Destroy;
begin
  FreeAndNil( FSysScheduler );
  inherited;
end;

procedure TfrmSimpleScheduler.Init(const Parameter: string);
begin
  FParameter := Parameter;
  GlobalLogger.LogEntry('Initializing scheduler');
  try
    try
      if not TJclTaskSchedule.IsRunning then
        TJclTaskSchedule.Start;
      FSysScheduler := TJclTaskSchedule.Create;

      RefreshTaskInfo;
    except
      GlobalLogger.PassthroughException;
    end;
  finally
    GlobalLogger.LogExit;
  end;
end;

procedure TfrmSimpleScheduler.RefreshTaskInfo;
var
  i: integer;
  s: string;
begin
  s := '';
  lblScheduleInfo.Caption := '<no scheuled "Import">';
  FSysScheduler.Refresh;
  for i := 0 to FSysScheduler.TaskCount - 1 do
  if (WideCompareText(FSysScheduler.Tasks[i].ApplicationName, GetModuleName(0)) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Parameters, FParameter) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
  begin
    if s <> '' then
      s := s + #10#13;
    s := s + GetNextRunTime( FSysScheduler.Tasks[i].NextRunTime );
  end;
  if s <> '' then
    lblScheduleInfo.Caption := s;
end;

function TfrmSimpleScheduler.GetNextRunTime(aTime: TSystemTime): string;
begin
  Result := Format('Next run at %s on %d/%d/%d', [TimeToStr( EncodeTime(aTime.wHour, aTime.wMinute, aTime.wSecond, 0)), aTime.wMonth, aTime.wDay, aTime.wYear]);
end;

procedure TfrmSimpleScheduler.ClearAll;
var
  i: integer;
  ArIdx: array of integer;
begin
  FSysScheduler.Refresh;
  for i := 0 to FSysScheduler.TaskCount - 1 do
  if (WideCompareText(FSysScheduler.Tasks[i].ApplicationName, GetModuleName(0)) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Parameters, FParameter) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
  begin
    SetLength(ArIdx, Length(ArIdx) + 1);
    ArIdx[ High(ArIdx) ] := i;
  end;

  if Length(ArIdx) > 0 then
  begin
    GlobalLogger.LogEntry('Remove scheduled task');
    try
      try
        for i := Low(arIdx) to High(arIdx) do
          FSysScheduler.Delete( ArIdx[i] );

        lblScheduleInfo.Caption := '<no scheuled "Import">';
      except
        GlobalLogger.PassthroughException;
      end;
    finally
      GlobalLogger.LogExit;
    end;
  end;
end;

procedure TfrmSimpleScheduler.ScheduleTask;
var
  ATask: TJclScheduledTask;
  uname: widestring;
  i: integer;
begin
  ATask := nil;

  for i := 0 to FSysScheduler.TaskCount-1 do
  if (WideCompareText(FSysScheduler.Tasks[i].ApplicationName, GetModuleName(0)) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Parameters, FParameter) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
  begin
    ATask := FSysScheduler.Tasks[i];
    Break;
  end;

  if not Assigned( aTask) then
  begin
    atask := FSysScheduler.Add( 'EvoSlavic import task' );
    atask.ApplicationName := GetModuleName(0);
    atask.Parameters := FParameter;
    atask.Comment := Format('Please use "%s" to manage this task.', [Application.Title]);

    uname := GetUserName(NameSamCompatible);
    atask.Flags := atask.Flags + [tfRunOnlyIfLoggedOn]; //!! because password isn't specified

    if (uname = LocalSystemAccount) or (uname = '') then
      OleCheck(atask.ScheduledWorkItem.SetAccountInformation('', nil))
    else
      OleCheck(atask.ScheduledWorkItem.SetAccountInformation(PWideChar(uname), nil));
  end;

  EditAndSave( aTask );
end;

procedure TfrmSimpleScheduler.lblScheduleInfoDblClick(Sender: TObject);
begin
  ScheduleTask;
end;

procedure TfrmSimpleScheduler.ScheduleExecute(Sender: TObject);
begin
  ScheduleTask;
end;

procedure TfrmSimpleScheduler.ClearExecute(Sender: TObject);
begin
  ClearAll;
end;

procedure TfrmSimpleScheduler.EditAndSave(aTask: TJclScheduledTask);
begin
  if Assigned(ATask) and ATask.ShowPage([ppSchedule], Application.Title, Application.MainForm.Handle) then
  begin
    ATask.Save;
    Atask.Refresh;
    lblScheduleInfo.Caption := GetNextRunTime( Atask.NextRunTime );
  end;
end;

end.
