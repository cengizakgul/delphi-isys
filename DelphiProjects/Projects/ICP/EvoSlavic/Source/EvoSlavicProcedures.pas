unit EvoSlavicProcedures;

interface

uses isSettings, gdyCommonLogger, gdyCommon, gdyCrypt, EvoAPIConnectionParamFrame;

const
  sRunImport = 'RunImport';
  sMainForm = 'MainForm\';

type
  IMainForm = interface
    function GetEvoConnectionFrame: TEvoAPIConnectionParamFrm;
    procedure ClearImportReport;
    procedure AddMessageToImportReport(aMsg: string);
    procedure SendImportReportByEmail;
    procedure CloseIfScheduledRun;
  end;

  TFtpParam = record
    FtpServerAddress: string;
    //ServerPort: integer;
    Username: string;
    Password: string;
    SavePassword: boolean;
    SecureFTP: boolean;
    Folder: string;
    DeferralFile: string;
    EeAddressFile: string;
    Port: integer;
  end;

procedure SaveFTPParam( const param: TFtpParam; conf: IisSettings; root: string );
function LoadFTPParam(conf: IisSettings; root: string ): TFtpParam;

var
  GlobalLogger: ICommonLogger;

implementation

const
  cKey='Form.Button';

procedure SaveFTPParam( const param: TFtpParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'FTPConnection\';
  conf.AsString[root+'FTPServerAddress'] := param.FtpServerAddress;
  conf.AsInteger[root+'FTPServerPort'] := param.Port;
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
  conf.AsBoolean[root+'SecureFTP'] := param.SecureFTP;
  conf.AsString[root+'Folder'] := param.Folder;
  conf.AsString[root+'DeferralFile'] := param.DeferralFile;
  conf.AsString[root+'EeAddressFile'] := param.EeAddressFile;
end;

function LoadFTPParam(conf: IisSettings; root: string ): TFtpParam;
begin
  root := root + IIF(root='','','\') + 'FtpConnection\';
  try
    GlobalLogger.LogDebug('XXX: key='+root+'FTPServerAddress');
    Result.FTPServerAddress := conf.AsString[root+'FTPServerAddress'];
    GlobalLogger.LogDebug('XXX: val='+Result.FtpServerAddress);
  except
    GlobalLogger.StopException;
  end;
  try
    Result.Username := conf.AsString[root+'Username'];
  except
    GlobalLogger.StopException;
  end;
  try
    Result.Port := conf.AsInteger[root+'FTPServerPort'];
  except
    GlobalLogger.StopException;
  end;
  try
    if conf.GetValueNames(root).IndexOf('Password') <> -1 then
      Result.Password := conf.AsString[root+'Password']
    else
      Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  except
    GlobalLogger.StopException;
  end;
  try
    Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  except
    GlobalLogger.StopException;
  end;
  try
    Result.SecureFTP := conf.AsBoolean[root+'SecureFTP'];
  except
    GlobalLogger.StopException;
  end;
  try
    Result.Folder := conf.AsString[root+'Folder'];
  except
    GlobalLogger.StopException;
  end;
  try
    Result.DeferralFile := conf.AsString[root+'DeferralFile'];
  except
    GlobalLogger.StopException;
  end;
  try
    Result.EeAddressFile := conf.AsString[root+'EeAddressFile'];
  except
    GlobalLogger.StopException;
  end;
end;

end.
