unit csreader;

interface

uses
  sysutils, Windows, classes;

type

  ECSReaderError = class ( Exception );

  TCSReader = class(TObject)
  private
    FData: TStringList;
    FParsedHeader: TStringList;
    procedure SetCommaText(strings: TStrings; const Value: string);
    procedure GetRawRecord( rec: TStringList; i: integer);
  public
    constructor Create( filename: string );
    destructor  Destroy; override;
    function RecordCount: integer;
    function Fields: TStrings;
    procedure GetHeader(hdr: TStringList);
    procedure GetRecord( rec: TStringList; i: integer);
  end;

type
  TCSRecordEvent = procedure( rec: TStrings ) of object;

procedure EachRecord( fname: string; cb: TCSRecordEvent; NoHeader: boolean = False );
procedure CollectionFromCSV( fname: string; col: TCollection );

implementation

uses
  math, typinfo;


procedure EachRecord( fname: string; cb: TCSRecordEvent; NoHeader: boolean = False );
var
  rd: TCSReader;
  rec: TStringList;
  i: integer;
begin
  rec := TStringList.Create;
  try
    rd := TCSReader.Create( fname );
    try
      if NoHeader then
      begin
        rd.GetHeader( rec );
        cb( rec );
      end;

      for i := 0 to rd.RecordCount-1 do
      begin
        if NoHeader then
          rd.GetRawRecord( rec, i )
        else
          rd.GetRecord( rec, i );
        cb( rec );
      end
    finally
      FreeAndNil( rd );
    end;
  finally
    FreeAndNil( rec );
  end;
end;

procedure CollectionFromCSV( fname: string; col: TCollection );
var
  rd: TCSReader;
  rec: TStringList;
  i: integer;
  item: TCollectionItem;
  j: integer;
//  props: array of PPropInfo;
begin
  rec := TStringList.Create;
  try
    rd := TCSReader.Create( fname );
    try
//      SetLength( props, rd.Fields.Count );
//      for j := 0 to rd.Fields.Count-1 do
//        props[j] := GetPropInfo( col.ItemClass, rd.Fields[j], [tkString, tkLString, tkWString] );
      for i := 0 to rd.RecordCount-1 do
      begin
        rd.GetRawRecord( rec, i );
        item := col.Add;
        for j := 0 to rec.Count-1 do
          SetPropValue( item, rd.Fields[j], rec[j] );
//          SetStrProp( item, props[j], rec[j] );
      end
    finally
      FreeAndNil( rd );
    end;
  finally
    FreeAndNil( rec );
  end;
end;

{ TCSReader }

resourcestring
  sInvalidRecordIndex = 'Record index (%d) out of range 0..%d';
  sInvalidRecord = 'Invalid Record %d: record contains %d fields while header has %d fields';

constructor TCSReader.Create(filename: string);
begin
  FData := nil;
  FParsedHeader := nil;
//----------
  FData := TStringList.Create;
  FData.LoadFromFile( filename );
  while FData.Count > 0 do
    if trim(FData[FData.Count-1]) = '' then
      FData.Delete( FData.Count-1 )
    else
      break;  
  FParsedHeader := TStringList.Create;
  GetHeader( FParsedHeader );
end;

destructor TCSReader.Destroy;
begin
  FreeAndNil( FData );
  FreeAndNil( FParsedHeader );
  inherited;
end;

procedure TCSReader.SetCommaText( strings: TStrings; const Value: string);
var
  P, P1: PChar;
  S: string;
begin
  strings.Clear;
//  strings.CommaText := Value;
  P := PChar(Value);
  while P^ in [#1..' '] do P := CharNext(P);
  while P^ <> #0 do
  begin
    if P^ = '"' then
      S := AnsiExtractQuotedStr(P, '"')
    else
    begin
      P1 := P;
      while (P^ >= ' ') and (P^ <> ',') do P := CharNext(P);
      SetString(S, P1, P - P1);
    end;
    strings.Add(S);
    while P^ in [#1..' '] do P := CharNext(P);
    if P^ = ',' then begin
      repeat
        P := CharNext(P);
      until not (P^ in [#1..' ']);
      if P^ = #0 then
        strings.Add('');
    end;
  end;
end;

procedure TCSReader.GetRawRecord(rec: TStringList; i: integer);
begin
  if ( i < -1 ) or ( i >= RecordCount ) then
    raise ECSReaderError.CreateResFmt( @sInvalidRecordIndex, [i,RecordCount - 1] );
  SetCommaText( rec, FData.Strings[i+1] );
  if FParsedHeader.count <> rec.Count then
    raise ECSReaderError.CreateResFmt( @sInvalidRecord,[i,rec.Count,FParsedHeader.count]);
end;


procedure TCSReader.GetRecord( rec: TStringList; i: integer);
var
  j :integer;
begin
  GetRawRecord( rec, i );
  for j := 0 to rec.Count - 1 do
    rec.Strings[j] := trim(FParsedHeader.Strings[j]) + '='+ trim(rec.Strings[j]);
end;

procedure TCSReader.GetHeader( hdr: TStringList );
begin
  if FData.Count > 0 then
    SetCommaText( hdr, FData.Strings[0] )
  else
    hdr.Clear;
end;

function TCSReader.RecordCount: integer;
begin
  Result := max( FData.Count - 1, 0 );
end;

function TCSReader.Fields: TStrings;
begin
  Result := FParsedHeader;
end;

end.
