object frmCSvloader: TfrmCSvloader
  Left = 0
  Top = 0
  Width = 320
  Height = 240
  TabOrder = 0
  object dbgData: TReDBGrid
    Left = 0
    Top = 0
    Width = 320
    Height = 240
    DisableThemesInTitle = False
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsCSVData
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
  end
  object dsCSVData: TDataSource
    Left = 112
    Top = 72
  end
end
