unit EeHolder;

interface

uses DB, kbmMemTable, gdyCommonLogger, SysUtils, EvoAPIConnection, XmlRpcTypes;

type
  TEeHolder = class
  private
    FCoNbr: integer;
    FClNbr: integer;
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    FEeData: TkbmCustomMemTable;
    FClPersonData: TkbmCustomMemTable;
    FEeStates: TkbmCustomMemTable;
    FRates: TkbmCustomMemTable;
    FEeLocals: TkbmCustomMemTable;
    FEePieceWork: TkbmCustomMemTable;
    FEE_DD: TkbmCustomMemTable;
    FSchedEd: TkbmCustomMemTable;
  public
    constructor Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; ClNbr, CoNbr: integer);
    destructor Destroy; override;

    procedure LoadEmployeesData(const SSNList: string); // SSNList - comma delimited list of SSN (format: DDD-DD-DDDD)

    function FindEeNbrByClPersonNbr(aClPersonNbr: integer): integer;
    function FindClPersonNbrBySSN(const aSSN: string): integer;
    function FindEeStatesNbr(aEeNbr, aCoStatesNbr: integer): integer;
    procedure GetEeHomeStateNbr(const aEeCode, aState: string; var EeNbr: integer; var EeStateNbr: integer);
    function FindeeRatesNbr(aEeNbr, aRateNumber: integer): integer;
    function FindEeLocalNbr(aEeNbr, aCoLocalTaxNbr: integer): integer;
    function FindEePieceWorkNbr(aEeNbr, aClPieceNbr: integer): integer;
    function FindEeDirectDeposit(aEeNbr: integer; const aEeAbaNumber, aEeBankAccountNumber: string): integer;
    function FindEeDirectDeposit2(aEeDirDepNbr: integer): integer;
    function FindEeSchedEDs(aEeNbr, aEeDirDepNbr: integer{; const EDCode: string}): integer;
    function FindEeSchedEDs2(aEeNbr, aClEdsNbr: integer): integer;

    function StringFieldValue(const aTableName, aFieldName: string): string;
    function DateTimeFieldValue(const aTableName, aFieldName: string): TDateTime;
    function IntegerFieldValue(const aTableName, aFieldName: string): integer;
    function DoubleFieldValue(const aTableName, aFieldName: string): Double;
  end;

  TEvoCoData = class
  private
    FCoNbr: integer;
    FClNbr: integer;

    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    FDBDTData: TkbmCustomMemTable;
    FWorkersComp: TkbmCustomMemTable;
    FCoJobs: TkbmCustomMemTable;
    FClEDs: TkbmCustomMemTable;
    FCoStates: TkbmCustomMemTable;
    FSyStatesMaritalStatus: TkbmCustomMemTable;
    FCoLocals: TkbmCustomMemTable;
    FClPieces: TkbmCustomMemTable;

    FDivFound: boolean;
    FDivisionNbr: integer;
    FBrFound: boolean;
    FBranchNbr: integer;
    FDepFound: boolean;
    FDepartmentNbr: integer;
    FTmFound: boolean;
    FTeamNbr: integer;
    FCO_WORKERS_COMP_NBR: integer;

    FCL_E_DS_NBR: integer;
    FCALCULATION_TYPE: string;
    FFREQUENCY: string;
    FEXCLUDE_WEEK_1: string;
    FEXCLUDE_WEEK_2: string;
    FEXCLUDE_WEEK_3: string;
    FEXCLUDE_WEEK_4: string;
    FEXCLUDE_WEEK_5: string;
    FALWAYS_PAY: string;
    FDEDUCTIONS_TO_ZERO: string;
    FDEDUCT_WHOLE_CHECK: string;
    FEFFECTIVE_START_DATE: TDateTime;
    FPLAN_TYPE: string;
    FWHICH_CHECKS: string;
    FUSE_PENSION_LIMIT: string;

    FCO_JOBS_NBR: integer;
    FCO_STATES_NBR: integer;
    FSY_STATE_MARITAL_STATUS: integer;
    FCO_LOCAL_TAX_NBR: integer;
    FCL_PIECES_NBR: integer;

    procedure ClearDBDTProperties;
  public
    constructor Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; ClNbr, CoNbr: integer);
    destructor Destroy; override;

    procedure OpenCompanyTables(const aDivList, aBrList, aDepList, aWcList, aJobList: string);
    procedure OpenClientTables;

    procedure OpenDBDT(const aDivList, aBrList, aDepList: string);
    procedure OpenCoStates;

    procedure LocateDBDTByCustomNumbers(const DivCNum, BrCNum, DepCNum, TmCNum: string);
    function LocateWorkersComp(const WC_CODE: string): boolean;
    function LocateCLEDCode(const EDCode: string): boolean;
    function LocateCoJob(const MiscJobCode: string): boolean;
    function LocateCoState(const State: string): boolean;
    function LocateSyStateMaritalStatus(const State, Status: string): boolean;
    function LocateCoLocalTax(const aLocalTax: string): boolean;
    function LocateCLPiece(const aName: string): boolean;

    property DivisionFound: boolean read FDivFound;
    property BranchFound: boolean read FBrFound;
    property DepartmentFound: boolean read FDepFound;
    property TeamFound: boolean read FTmFound;

    property CO_DIVISION_NBR: integer read FDivisionNbr;
    property CO_BRANCH_NBR: integer read FBranchNbr;
    property CO_DEPARTMENT_NBR: integer read FDepartmentNbr;
    property CO_TEAM_NBR: integer read FTeamNbr;

    property CO_WORKERS_COMP_NBR: integer read FCO_WORKERS_COMP_NBR;

    property CL_E_DS_NBR: integer read FCL_E_DS_NBR;
    property CALCULATION_TYPE: string read FCALCULATION_TYPE;
    property FREQUENCY: string read FFREQUENCY;
    property EXCLUDE_WEEK_1: string read FEXCLUDE_WEEK_1;
    property EXCLUDE_WEEK_2: string read FEXCLUDE_WEEK_2;
    property EXCLUDE_WEEK_3: string read FEXCLUDE_WEEK_3;
    property EXCLUDE_WEEK_4: string read FEXCLUDE_WEEK_4;
    property EXCLUDE_WEEK_5: string read FEXCLUDE_WEEK_5;
    property ALWAYS_PAY: string read FALWAYS_PAY;
    property DEDUCTIONS_TO_ZERO: string read FDEDUCTIONS_TO_ZERO;
    property DEDUCT_WHOLE_CHECK: string read FDEDUCT_WHOLE_CHECK;
    property EFFECTIVE_START_DATE: TDateTime read FEFFECTIVE_START_DATE;
    property PLAN_TYPE: string read FPLAN_TYPE;
    property WHICH_CHECKS: string read FWHICH_CHECKS;
    property USE_PENSION_LIMIT: string read FUSE_PENSION_LIMIT;

    property CO_JOBS_NBR: integer read FCO_JOBS_NBR;
    property CO_NBR: integer read FCoNbr;
    property CO_STATES_NBR: integer read FCO_STATES_NBR;
    property SY_STATE_MARITAL_STATUS: integer read FSY_STATE_MARITAL_STATUS;
    property CO_LOCAL_TAX_NBR: integer read FCO_LOCAL_TAX_NBR;
    property CL_PIECES_NBR: integer read FCL_PIECES_NBR;
  end;

implementation

uses gdyRedir, EvoJBCConstAndProc, common, Variants;

{ TEeHolder }

constructor TEeHolder.Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; ClNbr, CoNbr: integer);
begin
  FLogger := Logger;
  FEvoAPI := EvoAPI;
{  FEeData := TkbmCustomMemTable.Create(nil);
  FClPersonData := TkbmCustomMemTable.Create(nil);
  FEeStates := TkbmCustomMemTable.Create(nil);
  FEeLocals := TkbmCustomMemTable.Create(nil);
  FRates := TkbmCustomMemTable.Create(nil);
  FEePieceWork := TkbmCustomMemTable.Create(nil);
  FEE_DD := TkbmCustomMemTable.Create(nil);
  FSchedEd := TkbmCustomMemTable.Create(nil);}
  FCoNbr := CoNbr;
  FClNbr := ClNbr;
end;

function TEeHolder.DateTimeFieldValue(const aTableName,
  aFieldName: string): TDateTime;
begin
  Result := 0;
  if (aTableName = 'CL_PERSON') and Assigned(FClPersonData) and Assigned(FClPersonData.FindField(aFieldName)) then
    Result := FClPersonData.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE') and Assigned(FEeData) and Assigned(FEeData.FindField(aFieldName)) then
    Result := FEeData.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE_STATES') and Assigned(FEeStates) and Assigned(FEeStates.FindField(aFieldName)) then
    Result := FEeStates.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE_RATES') and Assigned(FRates) and Assigned(FRates.FindField(aFieldName)) then
    Result := FRates.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE_LOCALS') and Assigned(FEeLocals) and Assigned(FEeLocals.FindField(aFieldName)) then
    Result := FEeLocals.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE_DIRECT_DEPOSIT') and Assigned(FEE_DD) and Assigned(FEE_DD.FindField(aFieldName)) then
    Result := FEE_DD.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE_SCHEDULED_E_DS') and Assigned(FSchedEd) and Assigned(FSchedEd.FindField(aFieldName)) then
    Result := FSchedEd.FindField(aFieldName).AsDateTime
  else if (aTableName = 'EE_PIECE_WORK') and Assigned(FEePieceWork) and Assigned(FEePieceWork.FindField(aFieldName)) then
    Result := FEePieceWork.FindField(aFieldName).AsDateTime;
end;

destructor TEeHolder.Destroy;
begin
  FreeAndNil( FEeData );
  FreeAndNil( FClPersonData );
  FreeAndNil( FEeStates );
  FreeAndNil( FRates );
  FreeAndNil( FEE_DD );
  FreeAndNil( FEeLocals );
  FreeAndNil( FEePieceWork );
  FreeAndNil( FSchedEd );

  inherited;
end;

function TEeHolder.DoubleFieldValue(const aTableName,
  aFieldName: string): Double;
begin
  Result := 0;
  if (aTableName = 'CL_PERSON') and Assigned(FClPersonData) and Assigned(FClPersonData.FindField(aFieldName)) then
    Result := FClPersonData.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE') and Assigned(FEeData) and Assigned(FEeData.FindField(aFieldName)) then
    Result := FEeData.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE_STATES') and Assigned(FEeStates) and Assigned(FEeStates.FindField(aFieldName)) then
    Result := FEeStates.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE_RATES') and Assigned(FRates) and Assigned(FRates.FindField(aFieldName)) then
    Result := FRates.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE_LOCALS') and Assigned(FEeLocals) and Assigned(FEeLocals.FindField(aFieldName)) then
    Result := FEeLocals.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE_DIRECT_DEPOSIT') and Assigned(FEE_DD) and Assigned(FEE_DD.FindField(aFieldName)) then
    Result := FEE_DD.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE_SCHEDULED_E_DS') and Assigned(FSchedEd) and Assigned(FSchedEd.FindField(aFieldName)) then
    Result := FSchedEd.FindField(aFieldName).AsFloat
  else if (aTableName = 'EE_PIECE_WORK') and Assigned(FEePieceWork) and Assigned(FEePieceWork.FindField(aFieldName)) then
    Result := FEePieceWork.FindField(aFieldName).AsFloat;
end;

function TEeHolder.FindClPersonNbrBySSN(const aSSN: string): integer;
begin
  Result := -1;
  if Assigned(FClPersonData) and FClPersonData.Active then
    if FClPersonData.Locate('Social_Security_Number', aSSN, []) then
      Result := FClPersonData.FieldByName('CL_PERSON_NBR').AsInteger;
end;

function TEeHolder.FindEeDirectDeposit(aEeNbr: integer;
  const aEeAbaNumber, aEeBankAccountNumber: string): integer;
begin
  Result := -1;
  if Assigned(FEE_DD) and FEE_DD.Active then
    if FEE_DD.Locate('EE_NBR;EE_ABA_NUMBER;EE_BANK_ACCOUNT_NUMBER', VarArrayOf([aEeNbr, aEeAbaNumber, aEeBankAccountNumber]), []) then
      Result := FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger;
end;

function TEeHolder.FindEeDirectDeposit2(aEeDirDepNbr: integer): integer;
begin
  Result := -1;
  if Assigned(FEE_DD) and FEE_DD.Active then
    if FEE_DD.Locate('EE_DIRECT_DEPOSIT_NBR', aEeDirDepNbr, []) then
      Result := FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger;
end;

function TEeHolder.FindEeLocalNbr(aEeNbr,
  aCoLocalTaxNbr: integer): integer;
begin
  Result := -1;
  if Assigned(FEeLocals) and FEeLocals.Active then
    if FEeLocals.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([aEeNbr, aCoLocalTaxNbr]), []) then
      Result := FEeLocals.FieldByName('EE_LOCALS_NBR').AsInteger;
end;

function TEeHolder.FindEeNbrByClPersonNbr(aClPersonNbr: integer): integer;
begin
  Result := -1;
  if aClPersonNbr > -1 then
    if Assigned(FEeData) and FEeData.Active then
      if FEeData.Locate('CL_PERSON_NBR', aClPersonNbr, []) then
        Result := FEeData.FieldByName('EE_NBR').AsInteger;
end;

function TEeHolder.FindEePieceWorkNbr(aEeNbr,
  aClPieceNbr: integer): integer;
begin
  Result := -1;
  if Assigned(FEePieceWork) and FEePieceWork.Active then
    if FEePieceWork.Locate('EE_NBR;CL_PIECES_NBR', VarArrayOf([aEeNbr, aClPieceNbr]), []) then
      Result := FEePieceWork.FieldByName('EE_PIECE_WORK_NBR').AsInteger;
end;

function TEeHolder.FindeeRatesNbr(aEeNbr, aRateNumber: integer): integer;
begin
  Result := -1;
  if Assigned(FRates) and FRates.Active then
    if FRates.Locate('EE_NBR;RATE_NUMBER', VarArrayOf([aEeNbr, aRateNumber]), []) then
      Result := FRates.FieldByName('EE_RATES_NBR').AsInteger;
end;

function TEeHolder.FindEeSchedEDs(aEeNbr, aEeDirDepNbr: integer): integer;
begin
  Result := -1;
  if Assigned(FSchedEd) and FSchedEd.Active then
    if FSchedEd.Locate('EE_NBR;EE_DIRECT_DEPOSIT_NBR', VarArrayOf([aEeNbr, aEeDirDepNbr]), []) then
      Result := FSchedEd.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger;
end;

function TEeHolder.FindEeSchedEDs2(aEeNbr, aClEdsNbr: integer): integer;
begin
  Result := -1;
  if Assigned(FSchedEd) and FSchedEd.Active then
    if FSchedEd.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([aEeNbr, aClEdsNbr]), []) then
      Result := FSchedEd.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger;
end;

function TEeHolder.FindEeStatesNbr(aEeNbr, aCoStatesNbr: integer): integer;
begin
  Result := -1;
  if Assigned(FEeStates) and FEeStates.Active then
    if FEeStates.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([aEeNbr, aCoStatesNbr]), []) then
      Result := FEeStates.FieldByName('EE_STATES_NBR').AsInteger;
end;

procedure TEeHolder.GetEeHomeStateNbr(const aEeCode, aState: string;
  var EeNbr, EeStateNbr: integer);
var
  Param: IRpcStruct;
  FData: TkbmCustomMemTable;
begin
  EeNbr := -1;
  EeStateNbr := -1;
  param := TRpcStruct.Create;
  Param.AddItem('EeCode', aEeCode );
  Param.AddItem('State', aState );
  Param.AddItem('CoNbr', FCoNbr );
  try
    FData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'GetEeStateQuery.rwq', Param);
    if Assigned(FData) and (FData.RecordCount > 0) and Assigned(FData.FindField('EE_STATES_NBR')) then
    begin
      EeNbr := FData.FindField('EE_NBR').AsInteger;
      EeStateNbr := FData.FindField('EE_STATES_NBR').AsInteger;
    end;
  finally
    FreeAndNil( FData );
  end;
end;

function TEeHolder.IntegerFieldValue(const aTableName,
  aFieldName: string): integer;
begin
  Result := 0;
  if (aTableName = 'CL_PERSON') and Assigned(FClPersonData) and Assigned(FClPersonData.FindField(aFieldName)) then
    Result := FClPersonData.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE') and Assigned(FEeData) and Assigned(FEeData.FindField(aFieldName)) then
    Result := FEeData.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE_STATES') and Assigned(FEeStates) and Assigned(FEeStates.FindField(aFieldName)) then
    Result := FEeStates.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE_RATES') and Assigned(FRates) and Assigned(FRates.FindField(aFieldName)) then
    Result := FRates.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE_LOCALS') and Assigned(FEeLocals) and Assigned(FEeLocals.FindField(aFieldName)) then
    Result := FEeLocals.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE_DIRECT_DEPOSIT') and Assigned(FEE_DD) and Assigned(FEE_DD.FindField(aFieldName)) then
    Result := FEE_DD.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE_SCHEDULED_E_DS') and Assigned(FSchedEd) and Assigned(FSchedEd.FindField(aFieldName)) then
    Result := FSchedEd.FindField(aFieldName).AsInteger
  else if (aTableName = 'EE_PIECE_WORK') and Assigned(FEePieceWork) and Assigned(FEePieceWork.FindField(aFieldName)) then
    Result := FEePieceWork.FindField(aFieldName).AsInteger;
end;

procedure TEeHolder.LoadEmployeesData(const SSNList: string);
var
  Param: IRpcStruct;
  NbrList: string;

  function GetFieldValuesList(aDS: TDataset; const aFieldName: string): string;
  begin
    Result := '-1';
    aDs.First;
    while not aDS.Eof do
    begin
      Result := Result + ',' + aDS.FieldByName(aFieldName).AsString;
      aDS.Next;
    end;
  end;
begin
  param := TRpcStruct.Create;
  Param.AddItem( 'SSNList', SSNList );

  if Assigned(FClPersonData) then
    FreeAndNil(FClPersonData);
  FClPersonData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'ClPersonQuery.rwq', Param);

  if Assigned(FClPersonData) and (FClPersonData.RecordCount > 0) and Assigned(FClPersonData.FindField('CL_PERSON_NBR')) then
  begin
    NbrList := GetFieldValuesList(FClPersonData, 'CL_PERSON_NBR');

    Param.Clear;
    Param.AddItem( 'ClPersonNbrList', NbrList );
    Param.AddItem( 'CoNbr', FCoNbr );

    if Assigned(FEeData) then
      FreeAndNil(FEeData);
    FEeData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EeQuery.rwq', Param);

    if Assigned(FEeData) and (FEeData.RecordCount > 0) and Assigned(FEeData.FindField('EE_NBR')) then
    begin
      NbrList := GetFieldValuesList(FEeData, 'EE_NBR');

      Param.Clear;
      Param.AddItem( 'EeNbrList', NbrList );

      if Assigned(FEeStates) then FreeAndNil(FEeStates);
      if Assigned(FRates) then FreeAndNil(FRates);
      if Assigned(FEeLocals) then FreeAndNil(FEeLocals);
      if Assigned(FEePieceWork) then FreeAndNil(FEePieceWork);
      if Assigned(FEE_DD) then FreeAndNil(FEE_DD);
      if Assigned(FSchedEd) then FreeAndNil(FSchedEd);

      FEeStates := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EeStatesQuery.rwq', Param);
      FRates := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EeRatesQuery.rwq', Param);
      FEeLocals := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EeLocalsQuery.rwq', Param);
      FEePieceWork := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EePieceWorkQuery.rwq', Param);
      FEE_DD := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EeDirectDepositQuery.rwq', Param);
      FSchedEd := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'EeSchedEDsQuery.rwq', Param);
    end;
  end;
end;

function TEeHolder.StringFieldValue(const aTableName,
  aFieldName: string): string;
begin
  Result := '';
  if (aTableName = 'CL_PERSON') and Assigned(FClPersonData) and Assigned(FClPersonData.FindField(aFieldName)) then
    Result := FClPersonData.FindField(aFieldName).AsString
  else if (aTableName = 'EE') and Assigned(FEeData) and Assigned(FEeData.FindField(aFieldName)) then
    Result := FEeData.FindField(aFieldName).AsString
  else if (aTableName = 'EE_STATES') and Assigned(FEeStates) and Assigned(FEeStates.FindField(aFieldName)) then
    Result := FEeStates.FindField(aFieldName).AsString
  else if (aTableName = 'EE_RATES') and Assigned(FRates) and Assigned(FRates.FindField(aFieldName)) then
    Result := FRates.FindField(aFieldName).AsString
  else if (aTableName = 'EE_LOCALS') and Assigned(FEeLocals) and Assigned(FEeLocals.FindField(aFieldName)) then
    Result := FEeLocals.FindField(aFieldName).AsString
  else if (aTableName = 'EE_DIRECT_DEPOSIT') and Assigned(FEE_DD) and Assigned(FEE_DD.FindField(aFieldName)) then
    Result := FEE_DD.FindField(aFieldName).AsString
  else if (aTableName = 'EE_SCHEDULED_E_DS') and Assigned(FSchedEd) and Assigned(FSchedEd.FindField(aFieldName)) then
    Result := FSchedEd.FindField(aFieldName).AsString
  else if (aTableName = 'EE_PIECE_WORK') and Assigned(FEePieceWork) and Assigned(FEePieceWork.FindField(aFieldName)) then
    Result := FEePieceWork.FindField(aFieldName).AsString;
end;

{ TEvoCoData }

constructor TEvoCoData.Create(Logger: ICommonLogger;
  EvoAPI: IEvoAPIConnection; ClNbr, CoNbr: integer);
begin
  FLogger := Logger;
  FEvoAPI := EvoAPI;
{  FDBDTData := TkbmCustomMemTable.Create(nil);
  FWorkersComp := TkbmCustomMemTable.Create(nil);
  FCoJobs := TkbmCustomMemTable.Create(nil);
  FClEDs := TkbmCustomMemTable.Create(nil);
  FCoStates := TkbmCustomMemTable.Create(nil);
  FSyStatesMaritalStatus := TkbmCustomMemTable.Create(nil);
  FCoLocals := TkbmCustomMemTable.Create(nil);
  FClPieces := TkbmCustomMemTable.Create(nil);}

  FClNbr := ClNbr;
  FCoNbr := CoNbr;

  ClearDBDTProperties;
end;

destructor TEvoCoData.Destroy;
begin
  FreeAndNil( FWorkersComp );
  FreeAndNil( FCoJobs );
  FreeAndNil( FDBDTData );
  FreeAndNil( FClEDs );
  FreeAndNil( FCoStates );
  FreeAndNil( FSyStatesMaritalStatus );
  FreeAndNil( FCoLocals );
  FreeAndNil( FClPieces );

  inherited;
end;

procedure TEvoCoData.ClearDBDTProperties;
begin
  FDivFound := False;
  FBrFound := False;
  FDepFound := False;
  FTmFound := False;

  FDivisionNbr := -1;
  FBranchNbr := -1;
  FDepartmentNbr := -1;
  FTeamNbr := -1;
end;

procedure TEvoCoData.LocateDBDTByCustomNumbers(const DivCNum, BrCNum,
  DepCNum, TmCNum: string);
begin
  ClearDBDTProperties;

  if (FDBDTData <> nil) and FDBDTData.Active then
  begin
    if (DivCNum <> '') and (BrCNum <> '') and (DepCNum <> '') and (TmCNum <> '') then
    begin
      if FDBDTData.Locate('Custom_Division_Number;Custom_Branch_Number;Custom_Department_Number;Custom_Team_Number', VarArrayOf([Trim(DivCNum),Trim(BrCNum),Trim(DepCNum),Trim(TmCNum)]), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;

        FBrFound := True;
        FBranchNbr := FDBDTData.FieldByName('CO_BRANCH_NBR').AsInteger;

        FDepFound := True;
        FDepartmentNbr := FDBDTData.FieldByName('CO_DEPARTMENT_NBR').AsInteger;

        FTmFound := True;
        FTeamNbr := FDBDTData.FieldByName('CO_TEAM_NBR').AsInteger;
      end;
    end;
    if (DivCNum <> '') and (BrCNum <> '') and (DepCNum <> '') and not FTmFound then
    begin
      if FDBDTData.Locate('Custom_Division_Number;Custom_Branch_Number;Custom_Department_Number', VarArrayOf([Trim(DivCNum),Trim(BrCNum),Trim(DepCNum)]), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;

        FBrFound := True;
        FBranchNbr := FDBDTData.FieldByName('CO_BRANCH_NBR').AsInteger;

        FDepFound := True;
        FDepartmentNbr := FDBDTData.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
      end;
    end;
    if (DivCNum <> '') and (BrCNum <> '') and not FDepFound then
    begin
      if FDBDTData.Locate('Custom_Division_Number;Custom_Branch_Number', VarArrayOf([Trim(DivCNum),Trim(BrCNum)]), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;

        FBrFound := True;
        FBranchNbr := FDBDTData.FieldByName('CO_BRANCH_NBR').AsInteger;
      end;
    end;
    if (DivCNum <> '') and not FBrFound then
    begin
      if FDBDTData.Locate('Custom_Division_Number', Trim(DivCNum), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;
      end;
    end;
  end;
end;

function TEvoCoData.LocateWorkersComp(const WC_CODE: string): boolean;
begin
  Result := False;
  FCO_WORKERS_COMP_NBR := -1;

  if (FWorkersComp <> nil) and FWorkersComp.Active and (Trim(WC_CODE) <> '') then
  begin
    if FWorkersComp.Locate('WORKERS_COMP_CODE', Trim(WC_CODE), []) then
    begin
      Result := True;
      FCO_WORKERS_COMP_NBR := FWorkersComp.FieldByName('Co_Workers_Comp_Nbr').AsInteger;
    end;
  end;
end;

procedure TEvoCoData.OpenCompanyTables(const aDivList, aBrList, aDepList, aWcList, aJobList: string);
var
  Param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('CoNbr', FCoNbr);
  Param.AddItem('DivisionList', aDivList);
  Param.AddItem('BranchList', aBrList);
  Param.AddItem('DepartmentList', aDepList);

  if Assigned(FDBDTData) then FreeAndNil(FDBDTData);
  FDBDTData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'DBDT.rwq', Param);

  Param.Clear;
  Param.AddItem('CoNbr', FCoNbr);
  Param.AddItem('WCList', aWcList);

  if Assigned(FWorkersComp) then FreeAndNil(FWorkersComp);
  FWorkersComp := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'WorkersCompQuery.rwq', Param);

  Param.Clear;
  Param.AddItem('CoNbr', FCoNbr);
  Param.AddItem('JobList', aJobList);

  if Assigned(FCoJobs) then FreeAndNil(FCoJobs);
  FCoJobs := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'CoJobsQuery.rwq', Param);

  Param.Clear;
  Param.AddItem('CoNbr', FCoNbr);

  if Assigned(FCoStates) then FreeAndNil(FCoStates);
  if Assigned(FCoLocals) then FreeAndNil(FCoLocals);
  FCoStates := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'CoStatesQuery.rwq', Param);
  FCoLocals := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'CoLocalsQuery.rwq', Param);
end;

procedure TEvoCoData.OpenClientTables;
begin
  if Assigned(FClEDs) then FreeAndNil(FClEDs);
  if Assigned(FSyStatesMaritalStatus) then FreeAndNil(FSyStatesMaritalStatus);
  if Assigned(FClPieces) then FreeAndNil(FClPieces);

//  FEvoAPI.OpenClient( FClNbr );
  FClEDs := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'ClEDs.rwq');
  FSyStatesMaritalStatus := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'SY_STATE_MARITAL_STATUS.rwq');
  FClPieces := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'ClPiecesQuery.rwq');
end;

function TEvoCoData.LocateCLEDCode(const EDCode: string): boolean;
begin
  Result := False;
  FCL_E_DS_NBR := -1;

  if (FClEDs <> nil) and FClEDs.Active and (Trim(EDCode) <> '') then
  begin
    if FClEDs.Locate('Custom_E_D_Code_Number', Trim(EDCode), []) then
    begin
      Result := True;
      FCL_E_DS_NBR := FClEDs.FieldByName('CL_E_DS_Nbr').AsInteger;
      FCALCULATION_TYPE := FClEDs.FieldByName('Sd_Calculation_Method').AsString;
      FFREQUENCY := FClEDs.FieldByName('Sd_Frequency').AsString;
      FEXCLUDE_WEEK_1 := FClEDs.FieldByName('Sd_Exclude_Week_1').AsString;
      FEXCLUDE_WEEK_2 := FClEDs.FieldByName('Sd_Exclude_Week_2').AsString;
      FEXCLUDE_WEEK_3 := FClEDs.FieldByName('Sd_Exclude_Week_3').AsString;
      FEXCLUDE_WEEK_4 := FClEDs.FieldByName('Sd_Exclude_Week_4').AsString;
      FEXCLUDE_WEEK_5 := FClEDs.FieldByName('Sd_Exclude_Week_5').AsString;
      FALWAYS_PAY := FClEDs.FieldByName('Sd_Always_Pay').AsString;
      FDEDUCTIONS_TO_ZERO := FClEDs.FieldByName('Sd_Deductions_To_Zero').AsString;
      FDEDUCT_WHOLE_CHECK := FClEDs.FieldByName('Sd_Deduct_Whole_Check').AsString;

      if FClEDs.FieldByName('Sd_Effective_Start_Date').IsNull then
        FEFFECTIVE_START_DATE := now
      else
        FEFFECTIVE_START_DATE := FClEDs.FieldByName('Sd_Effective_Start_Date').AsDateTime;

      FPLAN_TYPE := FClEDs.FieldByName('Sd_Plan_Type').AsString;
      FWHICH_CHECKS := FClEDs.FieldByName('Sd_Which_Checks').AsString;
      FUSE_PENSION_LIMIT := FClEDs.FieldByName('Sd_Use_Pension_Limit').AsString;
    end;
  end;
end;

function TEvoCoData.LocateCoJob(const MiscJobCode: string): boolean;
begin
  Result := False;
  FCO_JOBS_NBR := -1;

  if (FCoJobs <> nil) and FCoJobs.Active and (Trim(MiscJobCode) <> '') then
  begin
    if FCoJobs.Locate('MISC_JOB_CODE', Trim(MiscJobCode), []) then
    begin
      Result := True;
      FCO_JOBS_NBR := FCoJobs.FieldByName('Co_Jobs_Nbr').AsInteger;
    end;
  end;
end;

function TEvoCoData.LocateCoState(const State: string): boolean;
begin
  Result := False;
  FCO_STATES_NBR := -1;

  if (FCoStates <> nil) and FCoStates.Active and (Trim(State) <> '') then
  begin
    if FCoStates.Locate('STATE', Trim(State), []) then
    begin
      Result := True;
      FCO_STATES_NBR := FCoStates.FieldByName('Co_States_Nbr').AsInteger;
    end;
  end;
end;

function TEvoCoData.LocateSyStateMaritalStatus(const State,
  Status: string): boolean;
begin
  Result := False;
  if FSyStatesMaritalStatus <> nil then
  begin
    FSY_STATE_MARITAL_STATUS := IntConvertNull(FSyStatesMaritalStatus.Lookup('STATE;STATUS_TYPE', VarArrayOf([State, Status]), 'SY_STATE_MARITAL_STAT_NBR'), -1);
    Result := FSY_STATE_MARITAL_STATUS > -1;
  end;  
end;

function TEvoCoData.LocateCoLocalTax(const aLocalTax: string): boolean;
begin
  Result := False;
  FCO_LOCAL_TAX_NBR := -1;

  if (FCoLocals <> nil) and FCoLocals.Active and (Trim(aLocalTax) <> '') then
  begin
    if FCoLocals.Locate('NAME', Trim(aLocalTax), []) then
    begin
      Result := True;
      FCO_LOCAL_TAX_NBR := FCoLocals.FieldByName('Co_Local_Tax_Nbr').AsInteger;
    end;
  end;
end;

function TEvoCoData.LocateCLPiece(const aName: string): boolean;
begin
  Result := False;
  FCL_PIECES_NBR := -1;

  if (FClPieces <> nil) and FClPieces.Active and (Trim(aName) <> '') then
  begin
    if FClPieces.Locate('Name', Trim(aName), []) then
    begin
      Result := True;
      FCL_PIECES_NBR := FClPieces.FieldByName('CL_PIECES_Nbr').AsInteger;
    end;
  end;
end;

procedure TEvoCoData.OpenDBDT(const aDivList, aBrList, aDepList: string);
var
  Param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('CoNbr', FCoNbr);
  Param.AddItem('DivisionList', aDivList);
  Param.AddItem('BranchList', aBrList);
  Param.AddItem('DepartmentList', aDepList);
  FDBDTData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'DBDT.rwq', Param);
end;

procedure TEvoCoData.OpenCoStates;
var
  Param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('CoNbr', FCoNbr);
  FCoStates := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'CoStatesQuery.rwq', Param);
end;

end.
