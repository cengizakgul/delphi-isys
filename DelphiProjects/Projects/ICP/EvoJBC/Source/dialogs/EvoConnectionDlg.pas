unit EvoConnectionDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, OptionsBaseFrame,
  EvoAPIConnectionParamFrame;

type
  TfrmEvoConnectionDlg = class(TfrmDialog)
    EvoAPIConnectionParamFrm: TEvoAPIConnectionParamFrm;
  protected
    procedure DoOK; override;
    procedure BeforeShowDialog; override;
  end;

implementation

{$R *.dfm}

uses common, EvoJBCConstAndProc;

{ TfrmEvoConnectionDlg }

procedure TfrmEvoConnectionDlg.BeforeShowDialog;
begin
  EvoAPIConnectionParamFrm.Param := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn )
end;

procedure TfrmEvoConnectionDlg.DoOK;
begin
  SaveEvoAPIConnectionParam( EvoAPIConnectionParamFrm.Param, FSettings, sEvoConn );
end;

end.
