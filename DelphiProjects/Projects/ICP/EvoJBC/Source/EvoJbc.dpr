program EvoJbc;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  main in 'main.pas' {frmMain},
  Log in 'Log.pas' {frmLogger},
  EvoJBCConstAndProc in 'EvoJBCConstAndProc.pas',
  CommonDlg in 'dialogs\CommonDlg.pas' {frmDialog},
  EvoConnectionDlg in 'dialogs\EvoConnectionDlg.pas' {frmEvoConnectionDlg},
  evoapiconnection in '..\..\common\evoAPIConnection.pas',
  EvoJBCServerMod in 'EvoJBCServerMod.pas',
  EeHolder in 'EeHolder.pas',
  XmlRpcServer in 'XmlRpcServer.pas';

{$R *.res}

begin
  LicenseKey := '633C8CFEB63F4CE8B2DAA4B7F4B9B869'; //EvoJBC

  Application.Initialize;
  Application.ShowMainForm := False;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
