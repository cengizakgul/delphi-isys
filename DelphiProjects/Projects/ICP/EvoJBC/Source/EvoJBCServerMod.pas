unit EvoJBCServerMod;

interface

uses
  XmlRpcServer, XmlRpcTypes, Classes, SysUtils, EvoJBCConstAndProc, Dialogs,
  kbmMemTable, DB, gdyCommonLogger, evoApiconnection, common, EeHolder, SyncObjs;

type
  WrongSessionException = class(Exception);
  WrongLicenseException = class(Exception);
  APIException = class(Exception);

  TevRPCMethod = procedure (const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String) of object;
  PTevRPCMethod = ^TevRPCMethod;

  TEvoJbcXmlRPCServer = class
  private
    FRpcServer: TRpcServer;
    FMethods: TStringList;
    FLogger: ICommonLogger;
    FEvoAPIConnectionParam: TEvoAPIConnectionParam;
    FMessenger: IMessenger;
    FEvoAPI: IEvoAPIConnection;
    FSyStates: TkbmCustomMemTable;
    FCS: TCriticalSection;

    // containers for employee data posted by loadEmployeeData
    FCL_PERSON_EE: TkbmCustomMemTable;
    FEE_STATES: TkbmCustomMemTable;
    FEE_RATES: TkbmCustomMemTable;
    FEE_LOCALS: TkbmCustomMemTable;
    FEE_PIECE_WORK: TkbmCustomMemTable;
    FEE_DD: TkbmCustomMemTable;

    // the company the employees will be imported to
    FCo: TEvoCompanyDef;
    FedMStatus: string;

    //packet update testing
    FChanges: IRpcArray;
    KeyNbr: integer;
    procedure AddRowChange(const UpdateType, TableName: string; var aKeyNbr: integer; var aFields: IRpcStruct);

    procedure StoreFedMStatus(aFields: IRpcStruct);

    procedure RegisterMethods;
    procedure RPCHandler(Thread: TRpcThread; const MethodName: string; List: TList; Return: TRpcReturn);
    procedure VerifyStructMember(aStruct: IRpcStruct; const aName: string; aDataType: TDataType; Mandatory: boolean = True;
      const aMsg: string = '');

    procedure loadEmployeeData(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure loadPayrollData(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure loadDBDTData(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);

    procedure initializeEeImport(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure loadEeData(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure finalizeEeImport(const AParams: TList; const AReturn: TRpcReturn; const AMethodName : String);
    procedure ClearEeContainers;
    procedure GetFilterValuesLists(var DivList: string; var BrList: string; var DepList: string;
      var WcList: string; var JobList: string; var SSNList: string);
    procedure ApplyClPersonDataChangePacket(EvoEE: TEeHolder);
    procedure ApplyEeDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData);
    procedure ApplyEeStatesDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
    procedure ApplyEeRatesDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
    procedure ApplyEeLocalsDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
    procedure ApplyEeDDDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
    procedure ApplyEePieceWorkDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
    procedure ApplyEeHomeTaxChangePacket;

    procedure SetClPersonDefault(var aFields: IRpcStruct; const State: string);
    procedure SetEeDefault(var aFields: IRpcStruct);
    procedure SetLocalsDefault(var aFields: IRpcStruct);
    procedure SetDDDefault(var aFields: IRpcStruct);
    procedure SetSchedDefaultED(var aFields: IRpcStruct; EvoCo: TEvoCoData);
    procedure SetSchedDefault(var aFields: IRpcStruct);

    procedure UpdateClPerson(aClPersonNbr: integer; EmployeeData: IRpcStruct; EvoEe: TEeHolder; aAsOF: TDateTime = 0);
    procedure CreateClPerson(var aClPersonNbr: integer; EmployeeData: IRpcStruct; EvoEe: TEeHolder; aAsOF: TDateTime = 0);

    procedure UpdateEe(aEeNbr: integer; EmployeeData: IRpcStruct; EvoEe: TEeHolder; EvoCo: TEvoCoData; aAsOF: TDateTime = 0);
    procedure CreateEe(var aEeNbr: integer; aClPersonNbr: integer; EmployeeData: IRpcStruct; EvoEe: TEeHolder; EvoCo: TEvoCoData; aAsOF: TDateTime = 0);

    procedure UpdateEeStates(aEeNbr: integer; EeStates: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string);
    procedure UpdateEeHomeTaxState(aEeNbr, aEeStatesNbr: integer; var aMsg: string);
    procedure UpdateEeRates(aEeNbr: integer; EeRates: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string; aAsOF: TDateTime = 0);
    procedure UpdateEeLocals(aEeNbr: integer; EeLocals: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string; aAsOF: TDateTime = 0);
    procedure UpdateEeDD(aEeNbr: integer; EeDDs: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string; aAsOF: TDateTime = 0);
    procedure UpdatePieceWork(aEeNbr: integer; Pieces: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string; aAsOF: TDateTime = 0);

    function BuildTCLine(PayrollDataLine: IRpcStruct): string;

    procedure SetTableFields(StartIdx, EndIdx: integer;EvoEe: TEeHolder; Data: IRpcStruct; const aTableName: string; var Fields: IRpcStruct; Insert: boolean = False); overload;
    procedure SetTableFields(StartIdx, EndIdx: integer;EvoEe: TEeHolder; Data: TDataset; const aTableName: string; var Fields: IRpcStruct; Insert: boolean = False); overload;
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function ApplyRecordChange(var aFields: IRpcStruct; const aTableName, aType: string; aKey: integer; aAsOF: TDateTime = 0): integer;
    function FindCompany(var aCompany: TEvoCompanyDef): boolean;
    function FindPayroll(aCoNbr: integer; aCheckDate: TDatetime; aRunNumber: integer): integer; // result = -1 means the payroll was not found

    function GetActive: boolean;
    procedure SetActive(AValue: boolean);
    procedure SetEvoConnectionParam(aValue: TEvoAPIConnectionParam);
  public
    constructor Create(logger: ICommonLogger; Messenger: IMessenger);
    destructor Destroy; override;

    property Active: boolean read GetActive write SetActive;
    property EvoConnectionParam: TEvoAPIConnectionParam read FEvoAPIConnectionParam write SetEvoConnectionParam;
  end;

implementation

uses gdyRedir, StrUtils, Variants, DateUtils;

procedure APICheckCondition(const ACondition: Boolean;
  AExceptClass: ExceptClass; const aMessage: string = '');
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      raise Exception.Create('APICheckCondition: exception class should be assigned!')
    else begin
      if Trim(aMessage) <> '' then
        raise AExceptClass.Create('APIException: ' + aMessage)
      else
        raise AExceptClass.Create('APIException');
    end;
  end;
end;

{ TEvoJbcXmlRPCServer }

procedure TEvoJbcXmlRPCServer.SetTableFields(StartIdx, EndIdx: integer; EvoEe: TEeHolder; Data: IRpcStruct; const aTableName: string; var Fields: IRpcStruct; Insert: boolean = False);
var
  i: integer;
begin
  for i := StartIdx to EndIdx do
  if not Mapping[i].H and (Mapping[i].EvoT = aTableName) and (Data.KeyExists(Mapping[i].N)) and (not Fields.KeyExists(Mapping[i].EvoF)) then
  begin
    case Mapping[i].T of
    dtString:
      if Insert or (EvoEe.StringFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.Keys[Mapping[i].N].AsString) then
        Fields.AddItem( Mapping[i].EvoF, Data.Keys[Mapping[i].N].AsString );
    dtDateTime:
      if Insert or (EvoEe.DateTimeFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.Keys[Mapping[i].N].AsDateTime) then
        Fields.AddItemDateTime( Mapping[i].EvoF, Data.Keys[Mapping[i].N].AsDateTime );
    dtInteger:
      if Insert or (EvoEe.IntegerFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.Keys[Mapping[i].N].AsInteger) then
        Fields.AddItem( Mapping[i].EvoF, Data.Keys[Mapping[i].N].AsInteger );
    dtFloat:
      if Insert or (EvoEe.DoubleFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.Keys[Mapping[i].N].AsFloat) then
      begin
        if Abs(Data.Keys[Mapping[i].N].AsFloat) >= 0.01 then
          Fields.AddItem( Mapping[i].EvoF, Data.Keys[Mapping[i].N].AsFloat );
      end;
    end;
  end;
end;

function TEvoJbcXmlRPCServer.ApplyRecordChange(var aFields: IRpcStruct;
  const aTableName, aType: string; aKey: integer; aAsOF: TDateTime = 0): integer;
var
  Changes: IRpcArray;
  RowChange, EvoResult: IRpcStruct;
begin
  Result := -1;
  if aFields.Count > 0 then
  begin
    Result := aKey;

    Changes := TRpcArray.Create;
    RowChange := TRpcStruct.Create;
    EvoResult := TRpcStruct.Create;

    RowChange.AddItem('T', aType);
    RowChange.AddItem('D', aTableName);
    if aType = 'U' then
      RowChange.AddItem('K', aKey)
    else
      aFields.AddItem(aTableName + '_NBR', -1);

    RowChange.AddItem('F', aFields);

    Changes.AddItem( RowChange );
    EvoResult := FEvoAPI.applyDataChangePacket( Changes, aAsOf );

    if Result = -1 then
      if EvoResult.KeyExists('-1') then
        if not TryStrToInt(EvoResult.Keys['-1'].AsString, Result) then
          raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
  end;
end;

function TEvoJbcXmlRPCServer.BuildTCLine(
  PayrollDataLine: IRpcStruct): string;
begin
  Result := '';

  with PayrollDataLine do
  begin
    //EeCode,Name,OverrideDept,Job,Shift,D/E,EarnCode
    Result := Result + Keys['EeCode'].AsString + ',,,,,' + Copy(Keys['EDCode'].AsString,1,1) + ',' + Copy(Keys['EDCode'].AsString,2,2) + ',';

    //Rate,Hours,
    if KeyExists('Rate') then
      Result := Result + FloatToStr(Keys['Rate'].AsFloat);

    Result := Result + ',';

    if KeyExists('Hours') then
      Result := Result + FloatToStr(Keys['Hours'].AsFloat);

    Result := Result + ',';

    //(LineItemDate)Year,Month,Day,Hour,Minute
    if KeyExists('LineItemDate') then
      Result := Result + FormatDateTime('yy', Keys['LineItemDate'].AsDateTime) + ',' +
      FormatDateTime('mm', Keys['LineItemDate'].AsDateTime) + ',' + FormatDateTime('dd', Keys['LineItemDate'].AsDateTime)
    else
      Result := Result + ',,';

    Result := Result + ',,,';

    //Amount,SeqNumber,OverrideDivision,OverrideBranch,OverrideState,OverrideLocal,BlankFill,Deduction3Hours,Deduction3Amount,SSN,W/C,RateNumber
    if KeyExists('Amount') then
      Result := Result + FloatToStr(Keys['Amount'].AsFloat);

    Result := Result + ',,,,,,,,,,,,';

    //PunchIn(Yyyymmddhhnnss)
    if KeyExists('PunchIn') then
      Result := Result + FormatDateTime('yyyymmddhhnnss', Keys['PunchIn'].AsDateTime);

    Result := Result + ',';

    //PunchOut(Yyyymmddhhnnss),OverrideTeam,CheckComments,
    if KeyExists('PunchOut') then
      Result := Result + FormatDateTime('yyyymmddhhnnss', Keys['PunchOut'].AsDateTime);

    Result := Result + ',,,';

    //(LineItemEndDate)Year,Month,Day
    if KeyExists('LineItemEndDate') then
      Result := Result + FormatDateTime('yy', Keys['LineItemEndDate'].AsDateTime) + ',' + FormatDateTime('mm', Keys['LineItemEndDate'].AsDateTime) + ',' + FormatDateTime('dd', Keys['LineItemEndDate'].AsDateTime)
    else
      Result := Result + ',,';
  end;
end;

constructor TEvoJbcXmlRPCServer.Create(logger: ICommonLogger; Messenger: IMessenger);
begin
  FLogger := logger;
  FMessenger := Messenger;

  FCS := TCriticalSection.Create;

  FMethods := TStringList.Create;
  FMethods.Duplicates := dupError;
  FMethods.CaseSensitive := False;
  FMethods.Sorted := True;

  FRpcServer := TRpcServer.Create( logger );

  RegisterMethods;

// FSyStates := TkbmCustomMemTable.Create(nil);
end;

procedure TEvoJbcXmlRPCServer.CreateClPerson(var aClPersonNbr: integer;
  EmployeeData: IRpcStruct; EvoEe: TEeHolder; aAsOF: TDateTime = 0);
var
  Fields: IRpcStruct;
begin
  Fields := TRpcStruct.Create;
  SetTableFields(ClPersonStartIdx, ClPersonEndIdx, EvoEe, EmployeeData, 'CL_PERSON', Fields, True );

  SetClPersonDefault(Fields, EmployeeData.Keys['State'].AsString);
//  aClPersonNbr := ApplyRecordChange( Fields, 'CL_PERSON', 'I', -1, aAsOf );

  AddRowChange('I', 'CL_PERSON', aClPersonNbr, Fields);
end;

procedure TEvoJbcXmlRPCServer.CreateEe(var aEeNbr: integer; aClPersonNbr: integer;
  EmployeeData: IRpcStruct; EvoEe: TEeHolder; EvoCo: TEvoCoData; aAsOF: TDateTime = 0);
var
  Fields: IRpcStruct;
  Dv, Br, Dp: string;
begin
  Fields := TRpcStruct.Create;
  Fields.AddItem('CL_PERSON_NBR', aClPersonNbr);
  Fields.AddItem('CO_NBR', EvoCo.CO_NBR);

  Dp := '';
  if EmployeeData.KeyExists('HomeDepartment') then
    Dp := EmployeeData.Keys['HomeDepartment'].AsString;
  Dv := '';
  if EmployeeData.KeyExists('HomeLocation') then
    Dv := EmployeeData.Keys['HomeLocation'].AsString;
  Br := '';
  if EmployeeData.KeyExists('HomeBranch') then
    Br := EmployeeData.Keys['HomeBranch'].AsString;

  if EmployeeData.KeyExists('WC_Code') and EvoCo.LocateWorkersComp( EmployeeData.Keys['WC_Code'].AsString ) then
    Fields.AddItem('CO_WORKERS_COMP_NBR', EvoCo.CO_WORKERS_COMP_NBR);

  if EmployeeData.KeyExists('JobNumber') and EvoCo.LocateCoJob( EmployeeData.Keys['JobNumber'].AsString ) then
    Fields.AddItem('CO_JOBS_NBR', EvoCo.CO_JOBS_NBR);

  EvoCo.LocateDBDTByCustomNumbers( Dv, Br, Dp, '' );

  if EvoCo.DivisionFound then
    Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR);

  if EvoCo.BranchFound then
    Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR);

  if EvoCo.DepartmentFound then
    Fields.AddItem('CO_DEPARTMENT_NBR', EvoCo.CO_DEPARTMENT_NBR);

  if (EmployeeData.KeyExists('TermDate')) and
    (StartOfTheDay( Today ) >= StartOfTheDay( (EmployeeData.Keys['TermDate'].AsDateTime) )) then
    Fields.AddItem('CURRENT_TERMINATION_CODE', 'M')
  else
    Fields.AddItem('CURRENT_TERMINATION_CODE', 'A');

  if EmployeeData.KeyExists('EmploymentType') then
  begin
    if EmployeeData.Keys['EmploymentType'].AsString = 'I' then
      Fields.AddItem('W2_TYPE', 'F')
    else
      Fields.AddItem('W2_TYPE', 'O');
  end
  else
    Fields.AddItem('W2_TYPE', 'N');

  SetTableFields( EeStartIdx, EeEndIdx, EvoEe, EmployeeData, 'EE', Fields, True );
  SetEeDefault( Fields );

//  aEeNbr := ApplyRecordChange( Fields, 'EE', 'I', -1, aAsOf );
  StoreFedMStatus(Fields);
  AddRowChange('I', 'EE', aEeNbr, Fields);
end;

destructor TEvoJbcXmlRPCServer.Destroy;
begin
  FRpcServer.Active := False;

  FreeAndNil(FRpcServer);
  FreeAndNil(FMethods);
  FreeAndNil(FSyStates);
  FreeAndNil(FCS);
  inherited;
end;

function TEvoJbcXmlRPCServer.FindCompany(
  var aCompany: TEvoCompanyDef): boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := False;
  aCompany.ClNbr := -1;
  aCompany.CoNbr := -1;

  param := TRpcStruct.Create;
  Param.AddItem( 'ClNumber', aCompany.Custom_Client_Number );
  Param.AddItem( 'CoNumber', aCompany.Custom_Company_Number );
  FClCoData :=  RunQuery('TmpCoQuery.rwq', Param);
  try
    if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
    begin
      aCompany.ClNbr := FClCoData.FindField('CL_NBR').AsInteger;
      aCompany.CoNbr := FClCoData.FindField('CO_NBR').AsInteger;
      Result := True;
    end;
  finally
    FClCoData.Indexes.Clear;
    FreeAndNil( FClCoData );
  end;
end;

function TEvoJbcXmlRPCServer.FindPayroll(aCoNbr: integer; aCheckDate: TDatetime;
  aRunNumber: integer): integer;
var
  FPrData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := -1;

  param := TRpcStruct.Create;
  Param.AddItem( 'CoNbr', aCoNbr );
  Param.AddItem( 'CheckDate', aCheckDate );
  Param.AddItem( 'RunNumber', aRunNumber );
  FPrData := RunQuery('FindPrQuery.rwq', Param);
  try
    if (FPrData.RecordCount > 0) and Assigned(FPrData.FindField('PR_NBR')) then
      Result := FPrData.FindField('PR_NBR').AsInteger;
  finally
    FPrData.Indexes.Clear;
    FreeAndNil( FPrData );
  end;
end;

function TEvoJbcXmlRPCServer.GetActive: boolean;
begin
  Result := Assigned(FRpcServer) and FRpcServer.Active;
end;

procedure TEvoJbcXmlRPCServer.loadEmployeeData(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult, EmployeeList, eeSubArray: IRpcArray;
  rpcStruct, EmployeeData, eeSubData: IRpcStruct;

  i, j, ClPersonNbr, EeNbr, EeStatesNbr: integer;
  Co: TEvoCompanyDef;
  Msg: string;
  SSNList, DivList, BrList, DepList, WcList, JobList: string;

  EvoEe: TEeHolder;
  EvoCo: TEvoCoData;

  ResStrings: TStrings;

  function GetEeSubArray(const KeyItemName: string): IRpcArray;
  var
    z: integer;
  begin
    Result := nil;
    for z := 0 to EmployeeData.Count - 1 do
    if EmployeeData.Items[z].IsArray then
    begin
      if (EmployeeData.Items[z].AsArray.Count > 0) and EmployeeData.Items[z].AsArray[0].IsStruct and
        EmployeeData.Items[z].AsArray[0].AsStruct.KeyExists( KeyItemName ) then
      Result := EmployeeData.Items[z].AsArray;
    end;
  end;
begin
  APICheckCondition(AParams.Count >= 3, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //ClientNumber
  APICheckCondition(TRpcParameter(AParams[1]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber
  APICheckCondition(TRpcParameter(AParams[2]).DataType = dtArray, APIException, 'Wrong method signature');   //EmployeeList

  // Find the company
  Co.CUSTOM_CLIENT_NUMBER := TRpcParameter(AParams[0]).AsString;
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[1]).AsString;

  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found in Client "%s"', [Co.CUSTOM_COMPANY_NUMBER, Co.CUSTOM_CLIENT_NUMBER]));

  EmployeeList := TRpcParameter(AParams[2]).AsArray;

  SSNList := '-1';
  DivList := '_';
  BrList := '_';
  DepList := '_';
  WcList := '_';
  JobList := '_';
  ResStrings := TStringList.Create;
  Msg := '';

  // Verify parameter structure
  for i := 0 to EmployeeList.Count - 1 do
  try
    APICheckCondition(EmployeeList[i].IsStruct, APIException, 'Wrong method signature: EmployeeData item ' + IntToStr(i) + ' must be a structure!');
    EmployeeData := EmployeeList[i].AsStruct;

    for j := EeStartIdx to EeEndIdx do
      VerifyStructMember(EmployeeData, Mapping[j].N, Mapping[j].T, Mapping[j].M);

    SSNList := SSNList + ',' + EmployeeData.Keys['TaxPayerID'].AsString ;
    if EmployeeData.KeyExists('HomeLocation') then
      DivList := DivList + ',' + EmployeeData.Keys['HomeLocation'].AsString;
    if EmployeeData.KeyExists('HomeBranch') then
      BrList := BrList + ',' + EmployeeData.Keys['HomeBranch'].AsString;
    if EmployeeData.KeyExists('HomeDepartment') then
      DepList := DepList + ',' + EmployeeData.Keys['HomeDepartment'].AsString;
    if EmployeeData.KeyExists('WC_Code') then
      WcList := WcList + ',' + EmployeeData.Keys['WC_Code'].AsString;
    if EmployeeData.KeyExists('JobNumber') then
      JobList := JobList + ',' + EmployeeData.Keys['JobNumber'].AsString;

    // pull possible values of DBDT from EeRates
    EeSubArray := GetEeSubArray('RateNumber');
    if eeSubArray <> nil then
    for j := 0 to eeSubArray.Count - 1 do
    begin
      eeSubData := eeSubArray[j].AsStruct;

      if eeSubData.KeyExists('Location') and eeSubData.Keys['Location'].IsString then
        DivList := DivList + ',' + eeSubData.Keys['Location'].AsString;
      if eeSubData.KeyExists('Branch') and eeSubData.Keys['Branch'].IsString then
        BrList := BrList + ',' + eeSubData.Keys['Branch'].AsString;
      if eeSubData.KeyExists('Department') and eeSubData.Keys['Department'].IsString then
        DepList := DepList + ',' + eeSubData.Keys['Department'].AsString;
    end;

    ResStrings.Add('Employee ' + EmployeeData.Keys['FirstName'].AsString + ' ' + EmployeeData.Keys['LastName'].AsString + ', ' + EmployeeData.Keys['EeCode'].AsString);
  except
    on E: Exception do
      ResStrings.Add( '0' + e.Message );
  end;

  rpcResult := TRpcArray.Create;

  EvoEe := TEeHolder.Create( FLogger, FEvoAPI, Co.ClNbr, Co.CoNbr );
  EvoCo := TEvoCoData.Create( FLogger, FEvoAPI, Co.ClNbr, Co.CoNbr );
  try
    // Open Client
    FEvoAPI.OpenClient( Co.ClNbr );

    EvoCo.OpenClientTables;
    EvoCo.OpenCompanyTables( DivList, BrList, DepList, WcList, JobList );
    EvoEe.LoadEmployeesData( SSNList );

    //import employees
    for i := 0 to EmployeeList.Count - 1 do
    if Copy(ResStrings[i], 1, 1) <> '0' then
    try
      EmployeeData := EmployeeList[i].AsStruct;
      EmployeeData.Keys['EeCode'].AsString := PadLeft(Trim(EmployeeData.Keys['EeCode'].AsString), ' ', 9);

      FChanges := TRpcArray.Create;
      KeyNbr := 0;

      ClPersonNbr := EvoEe.FindClPersonNbrBySSN( EmployeeData.Keys['TaxPayerID'].AsString );
      if ClPersonNbr > -1 then // update Cl_Person
      begin
        UpdateClPerson(ClPersonNbr, EmployeeData, EvoEe);
        ResStrings[i] := '2' + ResStrings[i] + ' has been updated successfully';
      end
      else begin // insert Cl_Person
        CreateClPerson(ClPersonNbr, EmployeeData, EvoEe);
        ResStrings[i] := '1' + ResStrings[i] + ' has been inserted successfully';
      end;

      EeNbr := EvoEe.FindEeNbrByClPersonNbr(ClPersonNbr);
      if EeNbr > -1 then // update EE
        UpdateEe(EeNbr, EmployeeData, EvoEe, EvoCo)
      else // insert EE
        CreateEe(EeNbr, ClPersonNbr, EmployeeData, EvoEe, EvoCo);

      Msg := '';
      // EeStateList
      UpdateEeStates(EeNbr, GetEeSubArray('State'), EvoEe, EvoCo, Msg);

      // EeRateList
      UpdateEeRates(EeNbr, GetEeSubArray('RateNumber'), EvoEe, EvoCo, Msg);

      // EeLocalList
      UpdateEeLocals(EeNbr, GetEeSubArray('LocalTaxLocality'), EvoEe, EvoCo, Msg);

      // DirectDepositList
      UpdateEeDD(EeNbr, GetEeSubArray('AccountTransitNumber'), EvoEe, EvoCo, Msg);

      // PieceWorkList
      UpdatePieceWork(EeNbr, GetEeSubArray('ItemName'), EvoEe, EvoCo, Msg);

      FEvoAPI.applyDataChangePacket(FChanges);

      if EmployeeData.KeyExists('PayrollState') then
      begin
        EvoEe.GetEeHomeStateNbr(EmployeeData.Keys['EeCode'].AsString, EmployeeData.Keys['PayrollState'].AsString, EeNbr, EeStatesNbr);
        if EeStatesNbr > 0 then
          UpdateEeHomeTaxState(EeNbr, EeStatesNbr, Msg);
      end;

      ResStrings[i] := ResStrings[i] + ' ' + Msg;

      FLogger.LogDebug('Ee Import', ResStrings[i]);
    except
      on E: Exception do
      begin
        ResStrings[i] := '0' + e.Message;
        FLogger.LogDebug('Ee Import Error', e.Message);
      end;
    end;

    for i := 0 to ResStrings.Count - 1 do
    begin
      rpcStruct := TRpcStruct.Create;
      rpcStruct.AddItem('Code', StrToInt(Copy(ResStrings[i],1,1)));
      rpcStruct.AddItem('Message', Copy(ResStrings[i], 2, Length(ResStrings[i])));
      rpcResult.AddItem( rpcStruct );
    end;
  finally
    FreeAndNil( EvoCo );
    FreeAndNil( EvoEE );
    FreeAndNil( ResStrings );
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TEvoJbcXmlRPCServer.loadPayrollData(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult, RpcImportOptions, ret: IRpcStruct;
  PayrollData: IRpcArray;
  i: integer;
  Co: TEvoCompanyDef;
  PrNbr: integer;
  sImportData: string;
begin
  APICheckCondition(AParams.Count = 5, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //ClientNumber
  APICheckCondition(TRpcParameter(AParams[1]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber
  APICheckCondition(TRpcParameter(AParams[2]).DataType = dtDateTime, APIException, 'Wrong method signature');//CheckDate
  APICheckCondition(TRpcParameter(AParams[3]).DataType = dtInteger, APIException, 'Wrong method signature'); //RunNumber
  APICheckCondition(TRpcParameter(AParams[4]).DataType = dtArray, APIException, 'Wrong method signature');   //PayrollData

  // verify and build ImportData
  PayrollData := TRpcParameter(AParams[4]).AsArray;
  sImportData := '';
  for i := 0 to PayrollData.Count - 1 do
  begin
    APICheckCondition(PayrollData[i].IsStruct, APIException, 'Wrong method signature: CheckLineData item ' + IntToStr(i) + ' must be a structure!');

    VerifyStructMember(PayrollData[i].AsStruct, 'EeCode', dtString, True, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'EDCode', dtString, True, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'Rate', dtFloat, False, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'Hours', dtFloat, False, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'Amount', dtFloat, False, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'LineItemDate', dtDateTime, False, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'LineItemEndDate', dtDateTime, False, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'PunchIn', dtDateTime, False, ' CheckLineData item ' + IntToStr(i));
    VerifyStructMember(PayrollData[i].AsStruct, 'PunchOut', dtDateTime, False, ' CheckLineData item ' + IntToStr(i));

    APICheckCondition(Trim(PayrollData[i].AsStruct.Keys['EeCode'].AsString) <> '', APIException, 'CheckLineData item ' + IntToStr(i) + ': EeCode should not be blank!');

    if i > 0 then
      sImportData := sImportData + #13 + #10;

    sImportData := sImportData + BuildTCLine( PayrollData[i].AsStruct );
  end;

  // Find the company
  Co.CUSTOM_CLIENT_NUMBER := TRpcParameter(AParams[0]).AsString;
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[1]).AsString;
  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found in Client "%s"', [Co.CUSTOM_COMPANY_NUMBER, Co.CUSTOM_CLIENT_NUMBER]));

  // Open Client
  FEvoAPI.OpenClient( Co.ClNbr );

  //Find the waiting payroll to load data to
  PrNbr := FindPayroll( Co.CoNbr, TRpcParameter(AParams[2]).AsDateTime, TRpcParameter(AParams[3]).AsInteger );
  APICheckCondition( PrNbr > 0, APIException, Format('No pending payroll %s-%s has been found in Company "%s"',
    [DateToStr(TRpcParameter(AParams[2]).AsDateTime), IntToStr(TRpcParameter(AParams[3]).AsInteger), Co.CUSTOM_COMPANY_NUMBER]));

  // create Import Options parameter
  RpcImportOptions := TRpcStruct.Create;
  RpcImportOptions.AddItem( 'LookupOption', 1 );  // lookup by Custom EE Number
  RpcImportOptions.AddItem( 'DBDTOption', 1 );  // DBDT Smart
  RpcImportOptions.AddItem( 'FourDigitYear', False );
  RpcImportOptions.AddItem( 'FileFormat', 1 ); // File Format CommaDelimited
  RpcImportOptions.AddItem( 'AutoImportJobCodes', False );
  RpcImportOptions.AddItem( 'UseEmployeePayRates', False );

  rpcResult := TRpcStruct.Create;
  try
    FLogger.LogDebug('Import file', sImportData);

    ret := FEvoAPI.SB_TCImport(Co.CoNbr, PrNbr, sImportData, RpcImportOptions, -1);

    FLogger.LogDebug('Import result log', ret.Keys['LogFile'].AsBase64Str);

    rpcResult.AddItem('Code', 1);
    rpcResult.AddItem('Message', 'Import is done');
    rpcResult.AddItem('RejectedRecords', ret.Keys['RejectedRecords'].AsInteger);
    rpcResult.AddItem('LogFile', ret.Keys['LogFile'].AsBase64Str);
  except
    on e: Exception do
    begin
      rpcResult.AddItem('Code', 0);
      rpcResult.AddItem('Message', e.Message);
    end;
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TEvoJbcXmlRPCServer.RegisterMethods;

   procedure RegisterMethod(const ASignature, AHelp: String; const AHandler: TevRPCMethod);
   var
     RpcMethodHandler: TRpcMethodHandler;
     s: String;
   begin
     s := ASignature;
     RpcMethodHandler := TRpcMethodHandler.Create;
     RpcMethodHandler.Name := GetNextStrValue(s, '(');
     RpcMethodHandler.Method := RPCHandler;
     RpcMethodHandler.Signature := ASignature;
     RpcMethodHandler.Help := AHelp;
     FRpcServer.RegisterMethodHandler(RpcMethodHandler);

     FMethods.AddObject(RpcMethodHandler.Name, @AHandler);
   end;

begin
  RegisterMethod('loadEmployeeData(string ClientNumber, string CompanyNumber, array EmployeeList)',
                 'Return: array',
                 loadEmployeeData);

  RegisterMethod('loadPayrollData(string ClientNumber, string CompanyNumber, datetime CheckDate, int RunNumber, array PayrollData)',
                 'Return: struct',
                 loadPayrollData);

  RegisterMethod('loadDBDTData(string ClientNumber, string CompanyNumber, int Level, struct DBDTData)',
                 'Return: struct',
                 loadDBDTData);

  RegisterMethod('loadEeData(array EmployeeList)',
                 'Return: array',
                 loadEeData);

  RegisterMethod('initializeEeImport(string ClientNumber, string CompanyNumber)',
                 'Return: string',
                 initializeEeImport);

  RegisterMethod('finalizeEeImport( no parameters )',
                 'Return: array',
                 finalizeEeImport);
end;

procedure TEvoJbcXmlRPCServer.RPCHandler(Thread: TRpcThread;
  const MethodName: string; List: TList; Return: TRpcReturn);
var
  i: Integer;
  Proc: TMethod;
begin
  try
    i := FMethods.IndexOf(MethodName);
    APICheckCondition(i <> -1, APIException, 'Unregistered method "' + MethodName + '"');

    Proc.Data := Self;
    Proc.Code := FMethods.Objects[i];
    TevRPCMethod(Proc)(List, Return, LowerCase(MethodName));
  except
    on E: APIException do
    begin
      Return.SetError(800, E.Message);
    end;

    on E: WrongSessionException do
    begin
      Return.SetError(810, 'Wrong session');
    end;

    on E: WrongLicenseException do
    begin
      Return.SetError(800, 'Wrong license key');
    end;

    on E: Exception do
    begin
      Return.SetError(500, E.Message);
    end;
  end;
end;

function TEvoJbcXmlRPCServer.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
end;

procedure TEvoJbcXmlRPCServer.SetActive(AValue: boolean);
begin
  if FRpcServer.Active <> AValue then
  begin
    if AValue then
    begin
      FMessenger.ClearMessages;

      try
        FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);
        FMessenger.AddMessage('Evo Connection established');

        FMessenger.AddMessage('Load Evo system data');
        FSyStates := RunQuery('Sy_States.rwq');

        FLogger.LogEntry( 'Starting to EvoJBC XML-RPC Server' );
        try
          try
            FRpcServer.ListenPort := 9944;
            FRpcServer.EnableIntrospect := True;

            // Set security connection
            {FRpcServer.SSLEnable := True;
            FRpcServer.SSLCertFile := Redirection.GetFilename(sCertificateFileAlias);
            FRpcServer.SSLKeyFile := Redirection.GetFilename(sKeyCertificateFileAlias);
            FRpcServer.SSLRootCertFile := Redirection.GetFilename(sRootCertificateFileAlias);}

            FRpcServer.Active := True;
            FMessenger.AddMessage('EvoJBC Server started. Listen port 9944...');
          except
            on e: Exception do
            begin
              FMessenger.AddMessage('EvoJBC Server start error: ' + e.Message);
              FLogger.LogError(e.Message);
            end;
          end;
        finally
          FLogger.LogExit;
        end;

      except
        FMessenger.AddMessage('Evo Connection failed!', True);
      end;
    end
    else
      FRpcServer.Active := False;
  end;
end;

procedure TEvoJbcXmlRPCServer.SetEvoConnectionParam(
  aValue: TEvoAPIConnectionParam);
begin
  Active := False;

  FLogger.LogEntry( 'Set Evo Connection Parameters' );
  try
    FEvoAPIConnectionParam := aValue;
  finally
    FLogger.LogExit;
  end;
end;

procedure TEvoJbcXmlRPCServer.UpdateClPerson(aClPersonNbr: integer;
  EmployeeData: IRpcStruct; EvoEe: TEeHolder; aAsOF: TDateTime = 0);
var
  Fields: IRpcStruct;
begin
  Fields := TRpcStruct.Create;
  SetTableFields( ClPersonStartIdx, ClPersonEndIdx, EvoEe, EmployeeData, 'CL_PERSON', Fields, aAsOf <> 0 );
//  ApplyRecordChange( Fields, 'CL_PERSON', 'U', aClPersonNbr, aAsOf );
  AddRowChange('U', 'CL_PERSON', aClPersonNbr, Fields);
end;

procedure TEvoJbcXmlRPCServer.UpdateEe(aEeNbr: integer;
  EmployeeData: IRpcStruct; EvoEe: TEeHolder; EvoCo: TEvoCoData; aAsOF: TDateTime = 0);
var
  Fields: IRpcStruct;
  Dv, Br, Dp: string;
begin
  Fields := TRpcStruct.Create;

  Dp := '';
  if EmployeeData.KeyExists('HomeDepartment') then
    Dp := EmployeeData.Keys['HomeDepartment'].AsString;
  Dv := '';
  if EmployeeData.KeyExists('HomeLocation') then
    Dv := EmployeeData.Keys['HomeLocation'].AsString;
  Br := '';
  if EmployeeData.KeyExists('HomeBranch') then
    Br := EmployeeData.Keys['HomeBranch'].AsString;

  if EmployeeData.KeyExists('WC_Code') and EvoCo.LocateWorkersComp( EmployeeData.Keys['WC_Code'].AsString ) and (EvoEe.IntegerFieldValue('EE', 'CO_WORKERS_COMP_NBR') <> EvoCo.CO_WORKERS_COMP_NBR) then
    Fields.AddItem('CO_WORKERS_COMP_NBR', EvoCo.CO_WORKERS_COMP_NBR);

  if EmployeeData.KeyExists('JobNumber') and EvoCo.LocateCoJob( EmployeeData.Keys['JobNumber'].AsString ) and (EvoEe.IntegerFieldValue('EE', 'CO_JOBS_NBR') <> EvoCo.CO_JOBS_NBR) then
    Fields.AddItem('CO_JOBS_NBR', EvoCo.CO_JOBS_NBR);

  EvoCo.LocateDBDTByCustomNumbers( Dv, Br, Dp, '' );

  if EvoCo.DivisionFound and (EvoEe.IntegerFieldValue('EE', 'CO_DIVISION_NBR') <> EvoCo.CO_DIVISION_NBR) then
    Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR);

  if EvoCo.BranchFound and (EvoEe.IntegerFieldValue('EE', 'CO_BRANCH_NBR') <> EvoCo.CO_BRANCH_NBR) then
    Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR);

  if EvoCo.DepartmentFound and (EvoEe.IntegerFieldValue('EE', 'CO_DEPARTMENT_NBR') <> EvoCo.CO_DEPARTMENT_NBR) then
    Fields.AddItem('CO_DEPARTMENT_NBR', EvoCo.CO_DEPARTMENT_NBR);

  SetTableFields( EeStartIdx, EeEndIdx, EvoEe, EmployeeData, 'EE', Fields, aAsOf <> 0 );
  if (EmployeeData.KeyExists('TermDate')) and
    (StartOfTheDay( Today ) >= StartOfTheDay( (EmployeeData.Keys['TermDate'].AsDateTime) )) then
    Fields.AddItem('CURRENT_TERMINATION_CODE', 'M')
  else
    Fields.AddItem('CURRENT_TERMINATION_CODE', 'A');

  if EmployeeData.KeyExists('EmploymentType') then
  begin
    if EmployeeData.Keys['EmploymentType'].AsString = 'I' then
      Fields.AddItem('W2_TYPE', 'F')
    else
      Fields.AddItem('W2_TYPE', 'O');
  end
  else
    Fields.AddItem('W2_TYPE', 'N');

//  ApplyRecordChange( Fields, 'EE', 'U', aEeNbr, aAsOf );
  StoreFedMStatus(Fields);

  AddRowChange('U', 'EE', aEeNbr, Fields);
end;

procedure TEvoJbcXmlRPCServer.VerifyStructMember(aStruct: IRpcStruct;
  const aName: string; aDataType: TDataType; Mandatory: boolean = True; const aMsg: string = '');
begin
  if Mandatory then
    APICheckCondition(aStruct.KeyExists(aName), APIException, aName + ' is required.' + aMsg);

  if aStruct.KeyExists(aName) then
    APICheckCondition(aStruct.Keys[aName].DataType = aDataType, APIException, aName + ' has a wrong type.' + aMsg);
end;

procedure TEvoJbcXmlRPCServer.UpdateEeStates(aEeNbr: integer;
  EeStates: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string);
var
  i, k, EeStatesNbr, CoStatesNbr, SuiApplyCoStatesNbr: integer;
  EeState, Fields: IRpcStruct;
begin
  if (EeStates = nil) then
    exit;
  if (EeStates.Count <= 0) then
    exit;

  aMsg := aMsg + ' > Ee States import: ';
  for i := 0 to EeStates.Count - 1 do
  try
    EeState := EeStates[i].AsStruct;
    for k := EeStatesStartIdx to EeStatesEndIdx do
      VerifyStructMember(EeState, Mapping[k].N, Mapping[k].T, Mapping[k].M, ' <- Ee States item ' + IntToStr(i));

    if EvoCo.LocateCoState( EeState.Keys['State'].AsString ) then
    begin
      CoStatesNbr := EvoCo.CO_STATES_NBR;
      SuiApplyCoStatesNbr := -1;
      if EeState.KeyExists('UnemploymentTaxState') then
      begin
        if EvoCo.LocateCoState( EeState.Keys['UnemploymentTaxState'].AsString ) then
          SuiApplyCoStatesNbr := EvoCo.CO_STATES_NBR
        else
          aMsg := aMsg + ' SUI State ' +EeState.Keys['UnemploymentTaxState'].AsString + ' was not found, ';
      end;

      EeStatesNbr := EvoEe.FindEeStatesNbr(aEeNbr, CoStatesNbr);
      Fields := TRpcStruct.Create;
      Fields.AddItem('IMPORTED_MARITAL_STATUS', FedMStatus);
      if EeStatesNbr = -1 then // insert state
      begin
        Fields.AddItem('EE_NBR', aEeNbr );
        Fields.AddItem('CO_STATES_NBR', CoStatesNbr );
        Fields.AddItem('RECIPROCAL_METHOD', 'N' );
        Fields.AddItem('CALCULATE_TAXABLE_WAGES_1099', 'N' );
        Fields.AddItem('SALARY_TYPE', 'N' );
        SetTableFields(EeStatesStartIdx, EeStatesEndIdx, EvoEe, EeState, 'EE_STATES', Fields, True );

        if SuiApplyCoStatesNbr > -1 then
          Fields.AddItem('SUI_APPLY_CO_STATES_NBR', SuiApplyCoStatesNbr);

        Fields.AddItem('SDI_APPLY_CO_STATES_NBR', CoStatesNbr);

        if EvoCo.LocateSyStateMaritalStatus(EeState.Keys['State'].AsString, EeState.Keys['StateMaritalStatus'].AsString) then
          Fields.AddItem('SY_STATE_MARITAL_STATUS_NBR', EvoCo.SY_STATE_MARITAL_STATUS)
        else
          raise Exception.Create('State Marital Status was not found (State: '+EeState.Keys['State'].AsString+' Status: '+EeState.Keys['StateMaritalStatus'].AsString+')');

        AddRowChange('I', 'EE_STATES', EeStatesNbr, Fields);

        aMsg := aMsg + 'Tax State ' + EeState.Keys['State'].AsString + ' inserted successfully, ';
      end
      else begin// update
        SetTableFields(EeStatesStartIdx, EeStatesEndIdx, EvoEe, EeState, 'EE_STATES', Fields);

        if (SuiApplyCoStatesNbr > -1) and (SuiApplyCoStatesNbr <> EvoEe.IntegerFieldValue('EE_STATES', 'SUI_APPLY_CO_STATES_NBR')) then
          Fields.AddItem('SUI_APPLY_CO_STATES_NBR', SuiApplyCoStatesNbr);

        Fields.AddItem('SDI_APPLY_CO_STATES_NBR', CoStatesNbr);

        if EvoCo.LocateSyStateMaritalStatus(EeState.Keys['State'].AsString, EeState.Keys['StateMaritalStatus'].AsString) then
        begin
          if EvoCo.SY_STATE_MARITAL_STATUS <> EvoEe.IntegerFieldValue('EE_STATES', 'SY_STATE_MARITAL_STATUS_NBR') then
            Fields.AddItem('SY_STATE_MARITAL_STATUS_NBR', EvoCo.SY_STATE_MARITAL_STATUS);
        end
        else
          raise Exception.Create('State Marital Status was not found (State: '+EeState.Keys['State'].AsString+' Status: '+EeState.Keys['StateMaritalStatus'].AsString+')');

        AddRowChange('U', 'EE_STATES', EeStatesNbr, Fields);

        aMsg := aMsg + 'Tax State ' + EeState.Keys['State'].AsString + ' updated successfully, ';
      end;
    end
    else
      aMsg := aMsg + 'Tax State ' + EeState.Keys['State'].AsString + ' was not found in Co States table, ';
  except
    on e: Exception do
      aMsg := aMsg + 'Tax State ' + EeState.Keys['State'].AsString + ' import error: <' + e.Message + '>, ';
  end;
  aMsg := Copy(aMsg, 1, Length(aMsg) - 2);
end;

procedure TEvoJbcXmlRPCServer.UpdateEeRates(aEeNbr: integer;
  EeRates: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData;
  var aMsg: string; aAsOF: TDateTime = 0);
var
  i, k, {CoHrPositionNbr,} EeRatesNbr: integer;
  EeRate, Fields: IRpcStruct;
  Dp, Dv, Br: string;
begin
  if (EeRates = nil) then
    exit;
  if (EeRates.Count <= 0) then
    exit;

  aMsg := aMsg + ' > Ee Rates import: ';
  for i := 0 to EeRates.Count - 1 do
  try
    EeRate := EeRates[i].AsStruct;
    for k := EeRatesStartIdx to EeRatesEndIdx do
      VerifyStructMember(EeRate, Mapping[k].N, Mapping[k].T, Mapping[k].M, ' <- Ee Rate item ' + IntToStr(i));

    EeRatesNbr := EvoEe.FindeeRatesNbr(aEeNbr, EeRate.Keys['RateNumber'].AsInteger);
    Fields := TRpcStruct.Create;

    Dp := '';
    if EeRate.KeyExists('Department') then
      Dp := EeRate.Keys['Department'].AsString;
    Dv := '';
    if EeRate.KeyExists('Location') then
      Dv := EeRate.Keys['Location'].AsString;
    Br := '';
    if EeRate.KeyExists('Branch') then
      Br := EeRate.Keys['Branch'].AsString;

    EvoCo.LocateDBDTByCustomNumbers( Dv, Br, Dp, '' );

    if EvoCo.DivisionFound and ((EeRatesNbr = -1) or (EvoEe.IntegerFieldValue('EE_RATES', 'CO_DIVISION_NBR') <> EvoCo.CO_DIVISION_NBR)) then
      Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR);

    if EvoCo.BranchFound and ((EeRatesNbr = -1) or (EvoEe.IntegerFieldValue('EE_RATES', 'CO_BRANCH_NBR') <> EvoCo.CO_BRANCH_NBR)) then
      Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR);

    if EvoCo.DepartmentFound and ((EeRatesNbr = -1) or (EvoEe.IntegerFieldValue('EE_RATES', 'CO_DEPARTMENT_NBR') <> EvoCo.CO_DEPARTMENT_NBR)) then
      Fields.AddItem('CO_DEPARTMENT_NBR', EvoCo.CO_DEPARTMENT_NBR);

    if EeRatesNbr = -1 then // insert rate
    begin
      Fields.AddItem('EE_NBR', aEeNbr );
      SetTableFields(EeRatesStartIdx, EeRatesEndIdx, EvoEe, EeRate, 'EE_RATES', Fields, True );
//      {EeRatesNbr := }ApplyRecordChange( Fields, 'EE_RATES', 'I', -1, aAsOf );
      AddRowChange('I', 'EE_RATES', EeRatesNbr, Fields);
      aMsg := aMsg + 'Rate ' + IntToStr(EeRate.Keys['RateNumber'].AsInteger) + ' inserted successfully, ';
    end
    else begin // update rate
      SetTableFields(EeRatesStartIdx, EeRatesEndIdx, EvoEe, EeRate, 'EE_RATES', Fields, aAsOf <> 0 );
//      ApplyRecordChange( Fields, 'EE_RATES', 'U', EeRatesNbr, aAsOf );
      AddRowChange('U', 'EE_RATES', EeRatesNbr, Fields);
      aMsg := aMsg + 'Rate ' + IntToStr(EeRate.Keys['RateNumber'].AsInteger) + ' updated successfully, ';
    end;
  except
    on e: Exception do
      aMsg := aMsg + 'Rate item ' + IntToStr(i) + ' import error: <' + e.Message + '>, ';
  end;
  aMsg := Copy(aMsg, 1, Length(aMsg) - 2);
end;

procedure TEvoJbcXmlRPCServer.UpdateEeLocals(aEeNbr: integer;
  EeLocals: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData;
  var aMsg: string; aAsOF: TDateTime = 0);
var
  i, k, EeLocalsNbr: integer;
  EeLocal, Fields: IRpcStruct;
begin
  if (EeLocals = nil) then
    exit;
  if (EeLocals.Count <= 0) then
    exit;

  aMsg := aMsg + ' > Ee Locals import: ';
  for i := 0 to EeLocals.Count - 1 do
  try
    EeLocal := EeLocals[i].AsStruct;
    for k := EeLocalsStartIdx to EeLocalsEndIdx do
      VerifyStructMember(EeLocal, Mapping[k].N, Mapping[k].T, Mapping[k].M, ' <- Ee Local item ' + IntToStr(i));

    if not EvoCo.LocateCoLocalTax( EeLocal.Keys['LocalTaxLocality'].AsString ) then
      aMsg := aMsg + EeLocal.Keys['LocalTaxLocality'].AsString + ' was not found in Co Locals table, '
    else begin
      EeLocalsNbr := EvoEe.FindEeLocalNbr(aEeNbr, EvoCo.CO_LOCAL_TAX_NBR);
      Fields := TRpcStruct.Create;
      if EeLocalsNbr = -1 then // insert Ee Local Tax
      begin
        Fields.AddItem('EE_NBR', aEeNbr );
        Fields.AddItem('CO_LOCAL_TAX_NBR', EvoCo.CO_LOCAL_TAX_NBR );

        SetLocalsDefault(Fields);

        SetTableFields(EeLocalsStartIdx, EeLocalsEndIdx, EvoEe, EeLocal, 'EE_LOCALS', Fields, True );
//        {EeLocalsNbr := }ApplyRecordChange( Fields, 'EE_LOCALS', 'I', -1, aAsOf );
        AddRowChange('I', 'EE_LOCALS', EeLocalsNbr, Fields);
        aMsg := aMsg + EeLocal.Keys['LocalTaxLocality'].AsString + ' inserted successfully, ';
      end
      else begin // update Ee Local Tax
        SetTableFields(EeLocalsStartIdx, EeLocalsEndIdx, EvoEe, EeLocal, 'EE_LOCALS', Fields, aAsOf <> 0 );
//        ApplyRecordChange( Fields, 'EE_LOCALS', 'U', EeLocalsNbr, aAsOf );
        AddRowChange('U', 'EE_LOCALS', EeLocalsNbr, Fields);
        aMsg := aMsg + EeLocal.Keys['LocalTaxLocality'].AsString + ' updated successfully, ';
      end;
    end;
  except
    on e: Exception do
      aMsg := aMsg + EeLocal.Keys['LocalTaxLocality'].AsString + ' import error: <' + e.Message + '>, ';
  end;
  aMsg := Copy(aMsg, 1, Length(aMsg) - 2);
end;

procedure TEvoJbcXmlRPCServer.UpdatePieceWork(aEeNbr: integer;
  Pieces: IRpcArray; EvoEe: TEeHolder; EvoCo: TEvoCoData;
  var aMsg: string; aAsOF: TDateTime = 0);
var
  i, k, EePieceWorkNbr: integer;
  PieceWork, Fields: IRpcStruct;
begin
  if (Pieces = nil) then
    exit;
  if (Pieces.Count <= 0) then
    exit;

  aMsg := aMsg + ' > Ee Piece Work import: ';
  for i := 0 to Pieces.Count - 1 do
  try
    PieceWork := Pieces[i].AsStruct;
    for k := PieceWorkStartIdx to PieceWorkEndIdx do
      VerifyStructMember(PieceWork, Mapping[k].N, Mapping[k].T, Mapping[k].M, ' <- Ee Piece Work item ' + IntToStr(i));

    if not EvoCo.LocateCLPiece( PieceWork.Keys['ItemName'].AsString ) then
      aMsg := aMsg + PieceWork.Keys['ItemName'].AsString + ' was not found in Cl Pieces table, '
    else begin
      EePieceWorkNbr := EvoEe.FindEePieceWorkNbr(aEeNbr, EvoCo.CL_PIECES_NBR);
      Fields := TRpcStruct.Create;
      if EePieceWorkNbr = -1 then // insert Ee Piece Work
      begin
        Fields.AddItem('EE_NBR', aEeNbr );
        Fields.AddItem('CL_PIECES_NBR', EvoCo.CL_PIECES_NBR );
        SetTableFields(PieceWorkStartIdx, PieceWorkEndIdx, EvoEe, PieceWork, 'EE_PIECE_WORK', Fields, True );
//        {EePieceWorkNbr := }ApplyRecordChange( Fields, 'EE_PIECE_WORK', 'I', -1, aAsOf );
        AddRowChange('I', 'EE_PIECE_WORK', EePieceWorkNbr, Fields);
        aMsg := aMsg + PieceWork.Keys['ItemName'].AsString + ' inserted successfully, ';
      end
      else begin // update Ee Piece Work
        SetTableFields(PieceWorkStartIdx, PieceWorkEndIdx, EvoEe, PieceWork, 'EE_PIECE_WORK', Fields, aAsOf <> 0 );
//        ApplyRecordChange( Fields, 'EE_PIECE_WORK', 'U', EePieceWorkNbr, aAsOf );
        AddRowChange('U', 'EE_PIECE_WORK', EePieceWorkNbr, Fields);
        aMsg := aMsg + PieceWork.Keys['ItemName'].AsString + ' updated successfully, ';
      end;
    end;
  except
    on e: Exception do
      aMsg := aMsg + PieceWork.Keys['ItemName'].AsString + ' import error: <' + e.Message + '>, ';
  end;
  aMsg := Copy(aMsg, 1, Length(aMsg) - 2);
end;

procedure TEvoJbcXmlRPCServer.UpdateEeDD(aEeNbr: integer; EeDDs: IRpcArray;
  EvoEe: TEeHolder; EvoCo: TEvoCoData; var aMsg: string; aAsOF: TDateTime = 0);
var
  i, k, EeDirDepNbr, EeSchedEDsNbr: integer;
  EeDD, Fields: IRpcStruct;
begin
  if (EeDDs = nil) then
    exit;
  if (EeDDs.Count <= 0) then
    exit;

  aMsg := aMsg + ' > Ee Direct Deposits import: ';
  for i := 0 to EeDDs.Count - 1 do
  try
    EeDD := EeDDs[i].AsStruct;
    for k := EeDDStartIdx to EeDDEndIdx do
      VerifyStructMember(EeDD, Mapping[k].N, Mapping[k].T, Mapping[k].M, ' <- Ee DD item ' + IntToStr(i));

    if not EvoCo.LocateCLEDCode( EeDD.Keys['EDCode'].AsString ) then
      aMsg := {aMsg + }' > Ee Direct Deposits import: E/D "'+EeDD.Keys['EDCode'].AsString+'" was not found in Cl E/Ds table'
    else begin
      FLogger.LogDebug('Ee DD update: start');

      EeSchedEDsNbr := EvoEe.FindEeSchedEDs2(aEeNbr, EvoCo.CL_E_DS_NBR);
      FLogger.LogDebug('Ee DD update: EeSchedEDsNbr = ' + IntToStr(EeSchedEDsNbr));

      if EeSchedEDsNbr > -1 then
        EeDirDepNbr := EvoEe.FindEeDirectDeposit2(EvoEe.IntegerFieldValue('EE_SCHEDULED_E_DS', 'EE_DIRECT_DEPOSIT_NBR'))
      else begin
        // todo: does the DD exist
        EeDirDepNbr := EvoEe.FindEeDirectDeposit(aEeNbr, EeDD.Keys['AccountTransitNumber'].AsString, EeDD.Keys['AccountNumber'].AsString);
      end;
      FLogger.LogDebug('Ee DD update: After search -> EeDirDepNbr = ' + IntToStr(EeDirDepNbr));

      Fields := TRpcStruct.Create;

      if EeDirDepNbr = -1 then // insert Ee Dir Dep
      begin
        Fields.AddItem('EE_NBR', aEeNbr );

        SetDDDefault( Fields );
        SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, EeDD, 'EE_DIRECT_DEPOSIT', Fields, True );
//        EeDirDepNbr := ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'I', -1, aAsOf );
        AddRowChange('I', 'EE_DIRECT_DEPOSIT', EeDirDepNbr, Fields);

        FLogger.LogDebug('Ee DD update: After insert -> EeDirDepNbr = ' + IntToStr(EeDirDepNbr));
      end
      else begin // update Ee Dir Dep
        SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, EeDD, 'EE_DIRECT_DEPOSIT', Fields, aAsOf <> 0 );
//        ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'U', EeDirDepNbr, aAsOf );
        AddRowChange('U', 'EE_DIRECT_DEPOSIT', EeDirDepNbr, Fields);
      end;
      FLogger.LogDebug('Ee DD update: end');

      Fields := TRpcStruct.Create;

      SetSchedDefaultED( Fields, EvoCo );
      if EeSchedEDsNbr = -1 then // insert Ee Sched EDs
      begin
        Fields.AddItem('EE_NBR', aEeNbr );
        Fields.AddItem('EE_DIRECT_DEPOSIT_NBR', EeDirDepNbr );
        SetSchedDefault( Fields );

        Fields.AddItem('CL_E_DS_NBR', EvoCo.CL_E_DS_NBR);

        if EvoCo.CALCULATION_TYPE = 'F' then
          Fields.AddItem('AMOUNT', EeDD.Keys['Amount'].AsFloat)
        else
          Fields.AddItem('PERCENTAGE', EeDD.Keys['Amount'].AsFloat);

        SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, EeDD, 'EE_SCHEDULED_E_DS', Fields, True );
//        ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'I', -1, aAsOf );
        AddRowChange('I', 'EE_SCHEDULED_E_DS', EeSchedEDsNbr, Fields);
      end
      else begin // update Ee Sched EDs
        if EvoCo.CALCULATION_TYPE = 'F' then
        begin
          if EvoEe.DoubleFieldValue('EE_SCHEDULED_E_DS', 'AMOUNT') <> EeDD.Keys['Amount'].AsFloat then
            Fields.AddItem('AMOUNT', EeDD.Keys['Amount'].AsFloat);
        end
        else begin
          if EvoEe.DoubleFieldValue('EE_SCHEDULED_E_DS', 'PERCENTAGE') <> EeDD.Keys['Amount'].AsFloat then
            Fields.AddItem('PERCENTAGE', EeDD.Keys['Amount'].AsFloat);
        end;

        SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, EeDD, 'EE_SCHEDULED_E_DS', Fields, aAsOf <> 0 );
//        ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'U', EeSchedEDsNbr, aAsOf );
        AddRowChange('U', 'EE_SCHEDULED_E_DS', EeSchedEDsNbr, Fields);
      end;
      aMsg := aMsg + 'DD (' + EeDD.Keys['AccountTransitNumber'].AsString + ') imported successfully, ';
    end;
  except
    on e: Exception do
      aMsg := aMsg + 'DD (' + EeDD.Keys['AccountTransitNumber'].AsString + ') import error: <' + e.Message + '>, ';
  end;
  aMsg := Copy(aMsg, 1, Length(aMsg) - 2);
end;

procedure TEvoJbcXmlRPCServer.loadDBDTData(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcDBDT, rpcResult: IRpcStruct;
  Co: TEvoCompanyDef;
  Dv, Br, Dp, Msg: string;
  EvoCo: TEvoCoData;
  Level, keyNbr: integer;
  aAsOf: TDateTime;

  function UpdateDBDT(const aLevel: string; aInsert: boolean; aKey: integer): integer;
  var
    Fields: IRpcStruct;
  begin
    Fields := TRpcStruct.Create;
    if aLevel = 'DIVISION' then
    begin
      Fields.AddItem('CO_NBR', Co.CoNbr );
      Fields.AddItem( 'CUSTOM_DIVISION_NUMBER', PadLeft(Trim(Dv), ' ', 20) );
      Fields.AddItem( 'PRINT_DIV_ADDRESS_ON_CHECKS', rpcDBDT.Keys['PrintAddressOnChecks'].AsString );
    end
    else if aLevel = 'BRANCH' then
    begin
      if not EvoCo.DivisionFound then
        raise Exception.Create('Division "' + Dv + '" was not found in Evo!')
      else
        Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR );
      Fields.AddItem( 'CUSTOM_BRANCH_NUMBER', PadLeft(Trim(Br), ' ', 20) );
      Fields.AddItem( 'PRINT_BRANCH_ADDRESS_ON_CHECK', rpcDBDT.Keys['PrintAddressOnChecks'].AsString );
    end
    else if aLevel = 'DEPARTMENT' then
    begin
      if not EvoCo.DivisionFound then
        raise Exception.Create('Division "' + Dv + '" was not found in Evo!')
      else if not EvoCo.BranchFound then
        raise Exception.Create('Branch "' + Br + '" was not found in Evo!')
      else
        Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR );
      Fields.AddItem( 'CUSTOM_DEPARTMENT_NUMBER', PadLeft(Trim(Dp), ' ', 20) );
      Fields.AddItem( 'PRINT_DEPT_ADDRESS_ON_CHECKS', rpcDBDT.Keys['PrintAddressOnChecks'].AsString );
    end;

    Fields.AddItem('NAME', rpcDBDT.Keys['Name'].AsString );
    Fields.AddItem('CONTACT1', rpcDBDT.Keys['Contact'].AsString );
    Fields.AddItem('PAY_FREQUENCY_HOURLY', rpcDBDT.Keys['PayFrequencyHourly'].AsString );
    Fields.AddItem('PAY_FREQUENCY_SALARY', rpcDBDT.Keys['PayFrequencySalary'].AsString );
    Fields.AddItem('HOME_STATE_TYPE', rpcDBDT.Keys['HomeStateType'].AsString );
    if EvoCo.LocateCoState( rpcDBDT.Keys['HomeState'].AsString ) then
      Fields.AddItem('HOME_CO_STATES_NBR', EvoCo.CO_STATES_NBR )
    else
      raise Exception.Create('State "' + rpcDBDT.Keys['HomeState'].AsString + '" was not found in Evo Compnay States!');

    if aInsert then
    begin
      Result := ApplyRecordChange( Fields, 'CO_' + aLevel, 'I', aKey, aAsOf );
      Msg := Msg + ' has been inserted successfully';
    end
    else begin
      Result := ApplyRecordChange( Fields, 'CO_' + aLevel, 'U', aKey, aAsOf );
      Msg := Msg + ' has been updated successfully';
    end;
  end;
begin
  APICheckCondition(AParams.Count >= 4, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //ClientNumber
  APICheckCondition(TRpcParameter(AParams[1]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber
  APICheckCondition(TRpcParameter(AParams[2]).DataType = dtInteger, APIException, 'Wrong method signature'); //Level
  APICheckCondition(TRpcParameter(AParams[3]).DataType = dtStruct, APIException, 'Wrong method signature');  //DBDT Data

  // Find the company
  Co.CUSTOM_CLIENT_NUMBER := TRpcParameter(AParams[0]).AsString;
  Co.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[1]).AsString;
  APICheckCondition( FindCompany(Co), APIException, Format('Comany "%s" is not found in Client "%s"', [Co.CUSTOM_COMPANY_NUMBER, Co.CUSTOM_CLIENT_NUMBER]));

  // Open Client
  FEvoAPI.OpenClient( Co.ClNbr );

  Level := TRpcParameter(AParams[2]).AsInteger;
  if (Level < 1) or (Level > 3) then
    raise Exception.Create('Parameter "Level" should have one of the values: 1, 2, 3');

  rpcDBDT := TRpcParameter(AParams[3]).AsStruct;

  Dv := '_';
  Br := '_';
  Dp := '_';
  Msg := '';
  KeyNbr := -1;

  if AParams.Count = 5 then
  begin
    APICheckCondition(TRpcParameter(AParams[4]).DataType = dtDateTime, APIException, 'Wrong method signature'); //AsOf date
    aAsOf := TRpcParameter(AParams[4]).AsDateTime;
  end
  else
    aAsOf := 0;

  // Verify parameter structure
  try
    if Level >= 2 then
    begin
      VerifyStructMember(rpcDBDT, 'ParentDivision', dtString, True);
      Dv := rpcDBDT.Keys['ParentDivision'].AsString;
    end;
    if Level = 3 then
    begin
      VerifyStructMember(rpcDBDT, 'ParentBranch', dtString, True);
      Br := rpcDBDT.Keys['ParentBranch'].AsString;
    end;

    VerifyStructMember(rpcDBDT, 'CustomNumber', dtString, True);
    case Level of
      1: begin
        Dv := rpcDBDT.Keys['CustomNumber'].AsString;
        Msg := 'Division "' + Dv + '"';
      end;
      2: begin
        Br := rpcDBDT.Keys['CustomNumber'].AsString;
        Msg := 'Branch "' + Br + '"';
      end;
      3: begin
        Dp := rpcDBDT.Keys['CustomNumber'].AsString;
        Msg := 'Department "' + Dp + '"';
      end;
    end;

    VerifyStructMember(rpcDBDT, 'Name', dtString, True);
    VerifyStructMember(rpcDBDT, 'Contact', dtString, True);
    VerifyStructMember(rpcDBDT, 'PayFrequencyHourly', dtString, True);
    VerifyStructMember(rpcDBDT, 'PayFrequencySalary', dtString, True);
    VerifyStructMember(rpcDBDT, 'PrintAddressOnChecks', dtString, True);
    VerifyStructMember(rpcDBDT, 'HomeStateType', dtString, True);
    VerifyStructMember(rpcDBDT, 'HomeState', dtString, True);

  except
    on E: Exception do
      Msg := '0' + e.Message;
  end;

  rpcResult := TRpcStruct.Create;

  //import DBDT
  if Copy(Msg, 1, 1) <> '0' then
  try
    EvoCo := TEvoCoData.Create( FLogger, FEvoAPI, Co.ClNbr, Co.CoNbr );
    try
      EvoCo.OpenDBDT('_,' + Dv, '_,' + Br, '_,' + Dp);
      EvoCo.OpenCoStates;

      EvoCo.LocateDBDTByCustomNumbers(Dv, Br, Dp, '');
      case Level of
      1: if EvoCo.DivisionFound then
           KeyNbr := UpdateDBDT('DIVISION', False, EvoCo.CO_DIVISION_NBR)
         else
           KeyNbr := UpdateDBDT('DIVISION', True, -1);
      2: if EvoCo.BranchFound then
           KeyNbr := UpdateDBDT('BRANCH', False, EvoCo.CO_BRANCH_NBR)
         else
           KeyNbr := UpdateDBDT('BRANCH', True, -1);
      3: if EvoCo.DepartmentFound then
           KeyNbr := UpdateDBDT('DEPARTMENT', False, EvoCo.CO_DEPARTMENT_NBR)
         else
           KeyNbr := UpdateDBDT('DEPARTMENT', True, -1);
      end;
    finally
      FreeAndNil( EvoCo );
    end;

  except
    on E: Exception do
      Msg := '0' + e.Message;
  end;

  if Copy(Msg,1,1) = '0' then
  begin
    KeyNbr := 0;
    Msg := Copy(Msg, 2, Length(Msg));
  end;

  rpcResult.AddItem('Code', keyNbr);
  rpcResult.AddItem('Message', Msg);

  AReturn.AddItem( rpcResult );
end;

procedure TEvoJbcXmlRPCServer.loadEeData(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult, EmployeeList: IRpcArray;
  rpcStruct, EmployeeData: IRpcStruct;

  i, j, ClPersonCount: integer;

  ResStrings: TStrings;

  function GetEeSubArray(const KeyItemName: string): IRpcArray;
  var
    z: integer;
  begin
    Result := nil;
    for z := 0 to EmployeeData.Count - 1 do
    if EmployeeData.Items[z].IsArray then
    begin
      if (EmployeeData.Items[z].AsArray.Count > 0) and EmployeeData.Items[z].AsArray[0].IsStruct and
        EmployeeData.Items[z].AsArray[0].AsStruct.KeyExists( KeyItemName ) then
      Result := EmployeeData.Items[z].AsArray;
    end;
  end;

  procedure FillFieldValues(StartIdx, EndIdx: integer; DS: TDataset; Data: IRpcStruct);
  var k: integer;
  begin
    for k := StartIdx to EndIdx do
    if (Mapping[k].EvoF <> '') and (Data.KeyExists(Mapping[k].N)) then
    begin
      case Mapping[k].T of
      dtString:
        DS.FieldByName(Mapping[k].EvoF).AsString := Data.Keys[Mapping[k].N].AsString;
      dtDateTime:
        DS.FieldByName(Mapping[k].EvoF).AsDateTime := Data.Keys[Mapping[k].N].AsDateTime;
      dtInteger:
        DS.FieldByName(Mapping[k].EvoF).AsInteger := Data.Keys[Mapping[k].N].AsInteger;
      dtFloat:
        DS.FieldByName(Mapping[k].EvoF).AsFloat := Data.Keys[Mapping[k].N].AsFloat;
      end;
    end;
  end;

  procedure FillSubArrayValues(eeSubArray: IRpcArray; StartIdx, EndIdx: integer; var aDS: TkbmCustomMemTable; aEeNbr: integer;
    aKeyName: string; aKeyName2: string = '');
  var
    eeSubData: IRpcStruct;
    k, n, RecCount: integer;
  begin
    if eeSubArray = nil then
      Exit;

    RecCount := aDS.RecordCount;
    for k := 0  to eeSubArray.Count - 1 do
    begin
      eeSubData := eeSubArray[k].AsStruct;
      for n := StartIdx to EndIdx do
        VerifyStructMember(eeSubData, Mapping[n].N, Mapping[n].T, Mapping[n].M);

      aDS.Append;
      FillFieldValues(StartIdx, EndIdx, aDS, eeSubData);
      aDS.FieldByName('EE_NBR').AsInteger := aEeNbr;
      aDS.FieldByName( aKeyName ).AsInteger := -(k+1 + RecCount);
      if aKeyName2 <> '' then
        aDS.FieldByName( aKeyName2 ).AsInteger := -(k+1 + RecCount);
      aDS.Post;
    end;
  end;
begin
  APICheckCondition( FCo.ClNbr > 0, APIException, 'initializeEEImport should be run before calling loadEeData');
  APICheckCondition( FCL_PERSON_EE <> nil, APIException, 'initializeEEImport should be run before calling loadEeData');
  // store employee data for the furure finalizeEeImport
  APICheckCondition(AParams.Count = 1, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtArray, APIException, 'Wrong method signature');   //EmployeeList

  EmployeeList := TRpcParameter(AParams[0]).AsArray;
  ResStrings := TStringList.Create;

  ClPersonCount := FCL_PERSON_EE.RecordCount;

  // Verify parameter structure
  for i := 0 to EmployeeList.Count - 1 do
  try
    APICheckCondition(EmployeeList[i].IsStruct, APIException, 'Wrong method signature: EmployeeData item ' + IntToStr(i) + ' must be a structure!');
    EmployeeData := EmployeeList[i].AsStruct;

    for j := ClPersonStartIdx to EeEndIdx do
      VerifyStructMember(EmployeeData, Mapping[j].N, Mapping[j].T, Mapping[j].M);

    //store CL_PERSON data
    FCL_PERSON_EE.Append;
    FillFieldValues(ClPersonStartIdx, EeEndIdx, FCL_PERSON_EE, EmployeeData);
    FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsInteger := -(i+1 + ClPersonCount);
    FCL_PERSON_EE.FieldByName('EE_NBR').AsInteger := -(i+1 + ClPersonCount);
    FCL_PERSON_EE.Post;

    //store EE_States data
    FillSubArrayValues(GetEeSubArray('State'), EeStatesStartIdx, EeStatesEndIdx, FEE_STATES, -(i+1 + ClPersonCount), 'EE_STATES_NBR');

    //store EE_Locals data
    FillSubArrayValues(GetEeSubArray('LocalTaxLocality'), EeLocalsStartIdx, EeLocalsEndIdx, FEE_LOCALS, -(i+1 + ClPersonCount), 'EE_LOCALS_NBR');

    //store EE_Rates data
    FillSubArrayValues(GetEeSubArray('RateNumber'), EeRatesStartIdx, EeRatesEndIdx, FEE_RATES, -(i+1 + ClPersonCount), 'EE_RATES_NBR');

    //store EE_DIRECT_DEPOSIT data
    FillSubArrayValues(GetEeSubArray('AccountTransitNumber'), EeDDStartIdx, EeSchedEndIdx, FEE_DD,
      -(i+1 + ClPersonCount), 'EE_DIRECT_DEPOSIT_NBR', 'EE_SCHEDULED_E_DS_NBR');

    //store EE_PIECE_WORK data
    FillSubArrayValues(GetEeSubArray('ItemName'), PieceWorkStartIdx, PieceWorkEndIdx, FEE_PIECE_WORK, -(i+1 + ClPersonCount), 'EE_PIECE_WORK_NBR');

    ResStrings.Add('1Employee ' + EmployeeData.Keys['FirstName'].AsString + ' ' + EmployeeData.Keys['LastName'].AsString + ', ' + EmployeeData.Keys['EeCode'].AsString + ' is ready to import');
  except
    on E: Exception do
      ResStrings.Add( '0' + e.Message );
  end;

  rpcResult := TRpcArray.Create;

  for i := 0 to ResStrings.Count - 1 do
  begin
    rpcStruct := TRpcStruct.Create;
    rpcStruct.AddItem('Code', StrToInt(Copy(ResStrings[i],1,1)));
    rpcStruct.AddItem('Message', Copy(ResStrings[i], 2, Length(ResStrings[i])));
    rpcResult.AddItem( rpcStruct );
  end;

  AReturn.AddItem( rpcResult );
end;

procedure TEvoJbcXmlRPCServer.finalizeEeImport(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);
var
  rpcResult: IRpcStruct;
  Msg: string;

  EvoEe: TEeHolder;
  EvoCo: TEvoCoData;
  SSNList, DivList, BrList, DepList, WcList, JobList: string;
begin
  APICheckCondition( FCo.ClNbr > 0, APIException, 'initializeEEImport should be run before calling finalizeEeImport');
  APICheckCondition( FCL_PERSON_EE <> nil, APIException, 'initializeEEImport should be run before calling finalizeEeImport');
  APICheckCondition( FCL_PERSON_EE.RecordCount > 0, APIException, 'loadEEData should be run before calling finalizeEeImport');

  SSNList := '-1';
  DivList := '_';
  BrList := '_';
  DepList := '_';
  WcList := '_';
  JobList := '_';
  Msg := '';
  
  try
    // post accumulated employee data to the Evo
    EvoEe := TEeHolder.Create( FLogger, FEvoAPI, FCo.ClNbr, FCo.CoNbr );
    EvoCo := TEvoCoData.Create( FLogger, FEvoAPI, FCo.ClNbr, FCo.CoNbr );
    try
      // Open Client
      FEvoAPI.OpenClient( FCo.ClNbr );

      EvoCo.OpenClientTables;

      GetFilterValuesLists(DivList, BrList, DepList, WcList, JobList, SSNList);
      EvoCo.OpenCompanyTables( DivList, BrList, DepList, WcList, JobList );
      EvoEe.LoadEmployeesData( SSNList );

      rpcResult := TRpcStruct.Create;
      try
        ApplyClPersonDataChangePacket(EvoEE);
        ApplyEeDataChangePacket(EvoEe, EvoCo);
        ApplyEeStatesDataChangePacket(EvoEE, EvoCo, Msg);
        ApplyEeRatesDataChangePacket(EvoEE, EvoCo, Msg);
        ApplyEeLocalsDataChangePacket(EvoEE, EvoCo, Msg);
        ApplyEeDDDataChangePacket(EvoEE, EvoCo, Msg);
        ApplyEePieceWorkDataChangePacket(EvoEE, EvoCo, Msg);
        ApplyEeHomeTaxChangePacket;

        if Msg <> '' then
          Msg := IntToStr(FCL_PERSON_EE.RecordCount) + ' ee imported.' + #13 + #10 + Msg
        else
          Msg := IntToStr(FCL_PERSON_EE.RecordCount) + ' ee imported.';

        rpcResult.AddItem('Code', 1);
        rpcResult.AddItem('Message', Msg);
      except
        on e: Exception do
        begin
          rpcResult.AddItem('Code', 0);
          rpcResult.AddItem('Message', e.Message);
        end;
      end;

      AReturn.AddItem( rpcResult );
    finally
      FreeAndNil(EvoEE);
      FreeAndNil(EvoCo);
    end;

  finally
    // clear employee data
    ClearEEContainers;
    FCo.ClNbr := -1;
  end;
end;

procedure TEvoJbcXmlRPCServer.initializeEeImport(const AParams: TList;
  const AReturn: TRpcReturn; const AMethodName: String);

  procedure CreateFields(aStartIdx, aEndIdx: integer; DS: TkbmCustomMemTable);
  var
    i: integer;
  begin
    for i := aStartIdx to aEndIdx do
    if Mapping[i].EvoF <> '' then
    case Mapping[i].T of
      dtInteger:
        CreateIntegerField( DS, Mapping[i].EvoF, Mapping[i].N );
      dtString:
        CreateStringField( DS, Mapping[i].EvoF, Mapping[i].N, 128 );
      dtDateTime:
        CreateDateTimeField( DS, Mapping[i].EvoF, Mapping[i].N );
      dtFloat:
        CreateFloatField( DS, Mapping[i].EvoF, Mapping[i].N );
      dtBoolean:
        CreateBooleanField( DS, Mapping[i].EvoF, Mapping[i].N );
    end;
  end;

begin
  APICheckCondition(AParams.Count = 2, APIException, 'Wrong method signature');
  APICheckCondition(TRpcParameter(AParams[0]).DataType = dtString, APIException, 'Wrong method signature');  //ClientNumber
  APICheckCondition(TRpcParameter(AParams[1]).DataType = dtString, APIException, 'Wrong method signature');  //CompanyNumber

  try
    // Find the company
    FCo.CUSTOM_CLIENT_NUMBER := TRpcParameter(AParams[0]).AsString;
    FCo.CUSTOM_COMPANY_NUMBER := TRpcParameter(AParams[1]).AsString;
    APICheckCondition( FindCompany(FCo), APIException, Format('Comany "%s" is not found in Client "%s"', [FCo.CUSTOM_COMPANY_NUMBER, FCo.CUSTOM_CLIENT_NUMBER]));

    // create container for accumulating Employee Data by loadEmployeeData
    ClearEeContainers;

    FCL_PERSON_EE := TkbmCustomMemTable.Create(nil);
    FEE_STATES := TkbmCustomMemTable.Create(nil);
    FEE_RATES := TkbmCustomMemTable.Create(nil);
    FEE_LOCALS := TkbmCustomMemTable.Create(nil);
    FEE_PIECE_WORK := TkbmCustomMemTable.Create(nil);
    FEE_DD := TkbmCustomMemTable.Create(nil);

    CreateFields( ClPersonStartIdx, EeEndIdx, FCL_PERSON_EE );
    CreateFields( EeStatesStartIdx, EeStatesEndIdx, FEE_STATES );
    CreateFields( EeRatesStartIdx, EeRatesEndIdx, FEE_RATES );
    CreateFields( EeLocalsStartIdx, EeLocalsEndIdx, FEE_LOCALS );
    CreateFields( PieceWorkStartIdx, PieceWorkEndIdx, FEE_PIECE_WORK );
    CreateFields( EeDDStartIdx, EeSchedEndIdx, FEE_DD );

    // key fields
    CreateIntegerField( FCL_PERSON_EE, 'CL_PERSON_NBR', 'CL_PERSON_NBR' );
    CreateIntegerField( FCL_PERSON_EE, 'EE_NBR', 'EE_NBR' );
    CreateIntegerField( FEE_STATES, 'EE_NBR', 'EE_NBR' );
    CreateIntegerField( FEE_STATES, 'EE_STATES_NBR', 'EE_STATES_NBR' );
    CreateIntegerField( FEE_RATES, 'EE_NBR', 'EE_NBR' );
    CreateIntegerField( FEE_RATES, 'EE_RATES_NBR', 'EE_RATES_NBR' );
    CreateIntegerField( FEE_LOCALS, 'EE_NBR', 'EE_NBR' );
    CreateIntegerField( FEE_LOCALS, 'EE_LOCALS_NBR', 'EE_LOCALS_NBR' );
    CreateIntegerField( FEE_PIECE_WORK, 'EE_NBR', 'EE_NBR' );
    CreateIntegerField( FEE_PIECE_WORK, 'EE_PIECE_WORK_NBR', 'EE_PIECE_WORK_NBR' );
    CreateIntegerField( FEE_DD, 'EE_NBR', 'EE_NBR' );
    CreateIntegerField( FEE_DD, 'EE_DIRECT_DEPOSIT_NBR', 'EE_DIRECT_DEPOSIT_NBR' );
    CreateIntegerField( FEE_DD, 'EE_SCHEDULED_E_DS_NBR', 'EE_SCHEDULED_E_DS_NBR' );

    FCL_PERSON_EE.Open;
    FEE_STATES.Open;
    FEE_RATES.Open;
    FEE_LOCALS.Open;
    FEE_PIECE_WORK.Open;
    FEE_DD.Open;

    AReturn.AddItem( 'Done!' );
  except
    on e: Exception do
      AReturn.AddItem('Initialize EE Import error: ' + e.Message  );
  end;
end;

procedure TEvoJbcXmlRPCServer.ClearEeContainers;
  procedure ClearDS(var aDS: TkbmCustomMemTable);
  begin
    if (aDS <> nil) then
    begin
      if aDS.Active then
        aDS.Close;
      FreeAndNil(aDS);
    end;
  end;
begin
  ClearDS( FCL_PERSON_EE );
  ClearDS( FEE_STATES );
  ClearDS( FEE_RATES );
  ClearDS( FEE_LOCALS );
  ClearDS( FEE_PIECE_WORK );
  ClearDS( FEE_DD );
end;

procedure TEvoJbcXmlRPCServer.GetFilterValuesLists(var DivList: string; var BrList: string; var DepList: string;
      var WcList: string; var JobList: string; var SSNList: string);
var
  slSSN, slDiv, slBr, slDep, slWC, slJob: TStrings;

  procedure AddFilterValue(sl: TStrings; aValue: string);
  begin
    if (Trim( aValue ) <> '') and (sl.IndexOf( aValue ) < 0) then
      sl.Add( aValue );
  end;

  procedure fillList(sl: TStrings; var s: string);
  var i: integer;
  begin
    for i := 0 to sl.Count - 1 do
      s := s + ',' + sl[i];
  end;
begin
  SSNList := '-1';
  DivList := '_';
  BrList := '_';
  DepList := '_';
  WcList := '_';
  JobList := '_';

  slSSN := TStringList.Create;
  slDiv := TStringList.Create;
  slBr := TStringList.Create;
  slDep := TStringList.Create;
  slWC := TStringList.Create;
  slJob := TStringList.Create;

  try
    FCL_PERSON_EE.First;
    while not FCL_PERSON_EE.Eof do
    begin
      AddFilterValue(slSSN, FCL_PERSON_EE.FieldByName('SOCIAL_SECURITY_NUMBER').AsString);
      AddFilterValue(slDiv, FCL_PERSON_EE.FieldByName('CO_DIVISION_NBR').AsString);
      AddFilterValue(slBr, FCL_PERSON_EE.FieldByName('CO_BRANCH_NBR').AsString);
      AddFilterValue(slDep, FCL_PERSON_EE.FieldByName('CO_DEPARTMENT_NBR').AsString);
      AddFilterValue(slWC, FCL_PERSON_EE.FieldByName('CO_WORKERS_COMP_NBR').AsString);
      AddFilterValue(slJob, FCL_PERSON_EE.FieldByName('CO_JOBS_NBR').AsString);

      // pull possible values of DBDT from EeRates
      FEE_RATES.Filtered := False;
      FEE_RATES.Filter := 'EE_NBR=' + FCL_PERSON_EE.FieldByName('EE_NBR').AsString;
      FEE_RATES.Filtered := True;

      FEE_RATES.First;
      while not FEE_RATES.Eof do
      begin
        AddFilterValue(slDiv, FEE_RATES.FieldByName('CO_DIVISION_NBR').AsString);
        AddFilterValue(slBr, FEE_RATES.FieldByName('CO_BRANCH_NBR').AsString);
        AddFilterValue(slDep, FEE_RATES.FieldByName('CO_DEPARTMENT_NBR').AsString);

        FEE_RATES.Next;
      end;
      FCL_PERSON_EE.Next;
    end;


    fillList(slSSN, SSNList);
    fillList(slDiv, DivList);
    fillList(slBr, BrList);
    fillList(slDep, DepList);
    fillList(slWC, WcList);
    fillList(slJob, JobList);
  finally
    FreeAndNil(slSSN);
    FreeAndNil(slDiv);
    FreeAndNil(slBr);
    FreeAndNil(slDep);
    FreeAndNil(slWC);
    FreeAndNil(slJob);
  end;
end;

procedure TEvoJbcXmlRPCServer.ApplyClPersonDataChangePacket(EvoEE: TEeHolder);
var
  ClPersonNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;
begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FCL_PERSON_EE.First;
  while not FCL_PERSON_EE.Eof do
  begin
    RowChange := TRpcStruct.Create;
    Fields := TRpcStruct.Create;

    ClPersonNbr := EvoEe.FindClPersonNbrBySSN( FCL_PERSON_EE.FieldByName('SOCIAL_SECURITY_NUMBER').AsString );
    if ClPersonNbr > -1 then // update Cl_Person
    begin
      FCL_PERSON_EE.Edit;
      FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsInteger := ClPersonNbr;
      FCL_PERSON_EE.Post;

      SetTableFields( ClPersonStartIdx, ClPersonEndIdx, EvoEe, FCL_PERSON_EE, 'CL_PERSON', Fields );

      RowChange.AddItem('T', 'U'); // update
      RowChange.AddItem('K', ClPersonNbr)
    end
    else begin // insert Cl_Person
      SetTableFields(ClPersonStartIdx, ClPersonEndIdx, EvoEe, FCL_PERSON_EE, 'CL_PERSON', Fields, True );
      SetClPersonDefault(Fields, FCL_PERSON_EE.FieldByName('State').AsString);

      RowChange.AddItem('T', 'I'); // insert
      Fields.AddItem('CL_PERSON_NBR', FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsInteger);
    end;

    RowChange.AddItem('D', 'CL_PERSON');
    RowChange.AddItem('F', Fields);

    Changes.AddItem( RowChange );

    FCL_PERSON_EE.Next;
  end;

  EvoResult := FEvoAPI.applyDataChangePacket( Changes );

  FCL_PERSON_EE.First;
  while not FCL_PERSON_EE.Eof do
  begin
    if EvoResult.KeyExists( FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsString ) then
    begin
      FCL_PERSON_EE.Edit;
      FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsInteger := EvoResult.Keys[ FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsString ].AsInteger;
      FCL_PERSON_EE.Post;
    end;

    FCL_PERSON_EE.Next;
  end;
end;

procedure TEvoJbcXmlRPCServer.SetTableFields(StartIdx, EndIdx: integer;
  EvoEe: TEeHolder; Data: TDataset; const aTableName: string;
  var Fields: IRpcStruct; Insert: boolean);
var
  i: integer;
begin
  for i := StartIdx to EndIdx do
  if not Mapping[i].H and (Mapping[i].EvoT = aTableName) and Assigned(Data.FindField(Mapping[i].EvoF)) and (not Fields.KeyExists(Mapping[i].EvoF)) then
  begin
    case Mapping[i].T of
    dtString:
      if Insert or (EvoEe.StringFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.FieldByName(Mapping[i].EvoF).AsString) then
        Fields.AddItem( Mapping[i].EvoF, Data.FieldByName(Mapping[i].EvoF).AsString );
    dtDateTime:
      if Insert or (EvoEe.DateTimeFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.FieldByName(Mapping[i].EvoF).AsDateTime) then
        Fields.AddItemDateTime( Mapping[i].EvoF, Data.FieldByName(Mapping[i].EvoF).AsDateTime );
    dtInteger:
      if Insert or (EvoEe.IntegerFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.FieldByName(Mapping[i].EvoF).AsInteger) then
        Fields.AddItem( Mapping[i].EvoF, Data.FieldByName(Mapping[i].EvoF).AsInteger );
    dtFloat:
      if Insert or (EvoEe.DoubleFieldValue(Mapping[i].EvoT, Mapping[i].EvoF) <> Data.FieldByName(Mapping[i].EvoF).AsFloat) then
      begin
        if Abs( Data.FieldByName(Mapping[i].EvoF).AsFloat ) >= 0.01 then
          Fields.AddItem( Mapping[i].EvoF, Data.FieldByName(Mapping[i].EvoF).AsFloat );
      end;
    end;
  end;
end;

procedure TEvoJbcXmlRPCServer.SetClPersonDefault(var aFields: IRpcStruct; const State: string);
begin
  // set Additional CL_PERSON fields
  aFields.AddItem('RESIDENTIAL_STATE_NBR',  IntConvertNull( FSyStates.Lookup('STATE', State, 'SY_STATES_NBR'), 4) );

  // set mandaroty CL_PERSON fields that were not filled yet
  aFields.AddItem('SMOKER', 'N' );
  aFields.AddItem('VETERAN', 'A' );
  aFields.AddItem('VISA_TYPE', 'N' );
  aFields.AddItem('VIETNAM_VETERAN', 'A' );
  aFields.AddItem('DISABLED_VETERAN', 'A' );
  aFields.AddItem('MILITARY_RESERVE', 'A' );
  aFields.AddItem('I9_ON_FILE', 'Y' );
  aFields.AddItem('RELIABLE_CAR', 'Y' );

  // new mandatory fields in Orange
  aFields.AddItem('SERVICE_MEDAL_VETERAN', 'A' );
  aFields.AddItem('OTHER_PROTECTED_VETERAN', 'A' );
  aFields.AddItem('NATIVE_LANGUAGE', 'E' );
end;

procedure TEvoJbcXmlRPCServer.ApplyEeDataChangePacket(EvoEE: TEeHolder; EvoCo: TEvoCoData);
var
  EeNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;

  procedure UpdateEeNbrInSubTable(aDS: TDataset; aOld, aNew: integer);
  begin
    aDS.Filtered := False;
    aDS.Filter := 'EE_NBR=' + IntToStr(aOld);
    aDS.Filtered := True;
    try
      aDS.First;
      while not aDS.Eof do
      begin
        aDS.Edit;
        aDS.FieldByName('EE_NBR').AsInteger := EeNbr;
        aDS.Post;
        aDS.Next;
      end;
    finally
      aDS.Filtered := False;
    end;
  end;

  procedure UpdateEeNbr(aOld, aNew: integer);
  begin
    UpdateEeNbrInSubTable( FEE_STATES, aOld, aNew );
    UpdateEeNbrInSubTable( FEE_RATES, aOld, aNew );
    UpdateEeNbrInSubTable( FEE_LOCALS, aOld, aNew );
    UpdateEeNbrInSubTable( FEE_PIECE_WORK, aOld, aNew );
    UpdateEeNbrInSubTable( FEE_DD, aOld, aNew );

    FCL_PERSON_EE.Edit;
    FCL_PERSON_EE.FieldByName('EE_NBR').AsInteger := EeNbr;
    FCL_PERSON_EE.Post;
  end;
begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FCL_PERSON_EE.First;
  while not FCL_PERSON_EE.Eof do
  begin
    RowChange := TRpcStruct.Create;
    Fields := TRpcStruct.Create;

    if (FCL_PERSON_EE.FieldByName('CURRENT_TERMINATION_DATE').AsFloat > 10) and
      (StartOfTheDay( Today ) >= StartOfTheDay( (FCL_PERSON_EE.FieldByName('CURRENT_TERMINATION_DATE').AsDateTime) )) then
      Fields.AddItem('CURRENT_TERMINATION_CODE', 'M')
    else
      Fields.AddItem('CURRENT_TERMINATION_CODE', 'A');

    if FCL_PERSON_EE.FieldByName('COMPANY_OR_INDIVIDUAL_NAME').AsString = 'I' then
      Fields.AddItem('W2_TYPE', 'F')
    else
      Fields.AddItem('W2_TYPE', 'O');

    EeNbr := EvoEe.FindEeNbrByClPersonNbr( FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsInteger );
    if EeNbr > -1 then // update Ee
    begin
      UpdateEeNbr( FCL_PERSON_EE.FieldByName('EE_NBR').AsInteger, EeNbr );

      if Assigned(FCL_PERSON_EE.FindField('CO_WORKERS_COMP_NBR')) and EvoCo.LocateWorkersComp( FCL_PERSON_EE.FieldByName('CO_WORKERS_COMP_NBR').AsString ) and
        (EvoEe.IntegerFieldValue('EE', 'CO_WORKERS_COMP_NBR') <> EvoCo.CO_WORKERS_COMP_NBR) then
        Fields.AddItem('CO_WORKERS_COMP_NBR', EvoCo.CO_WORKERS_COMP_NBR);

      if Assigned(FCL_PERSON_EE.FindField('CO_JOBS_NBR')) and EvoCo.LocateWorkersComp( FCL_PERSON_EE.FieldByName('CO_JOBS_NBR').AsString ) and
        (EvoEe.IntegerFieldValue('EE', 'CO_JOBS_NBR') <> EvoCo.CO_JOBS_NBR) then
        Fields.AddItem('CO_JOBS_NBR', EvoCo.CO_JOBS_NBR);

      EvoCo.LocateDBDTByCustomNumbers( FCL_PERSON_EE.FieldByName('CO_DIVISION_NBR').AsString,
        FCL_PERSON_EE.FieldByName('CO_BRANCH_NBR').AsString, FCL_PERSON_EE.FieldByName('CO_DEPARTMENT_NBR').AsString, '');

      if EvoCo.DivisionFound and (EvoEe.IntegerFieldValue('EE', 'CO_DIVISION_NBR') <> EvoCo.CO_DIVISION_NBR) then
        Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR);

      if EvoCo.BranchFound and (EvoEe.IntegerFieldValue('EE', 'CO_BRANCH_NBR') <> EvoCo.CO_BRANCH_NBR) then
        Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR);

      if EvoCo.DepartmentFound and (EvoEe.IntegerFieldValue('EE', 'CO_DEPARTMENT_NBR') <> EvoCo.CO_DEPARTMENT_NBR) then
        Fields.AddItem('CO_DEPARTMENT_NBR', EvoCo.CO_DEPARTMENT_NBR);

      SetTableFields( EeStartIdx, EeEndIdx, EvoEe, FCL_PERSON_EE, 'EE', Fields );

      RowChange.AddItem('T', 'U'); // update
      RowChange.AddItem('K', EeNbr)
    end
    else begin // insert Ee
      Fields.AddItem('CO_NBR', EvoCo.CO_NBR);
      Fields.AddItem('CL_PERSON_NBR', FCL_PERSON_EE.FieldByName('CL_PERSON_NBR').AsInteger);
      if Assigned(FCL_PERSON_EE.FindField('CO_WORKERS_COMP_NBR')) and EvoCo.LocateWorkersComp( FCL_PERSON_EE.FieldByName('CO_WORKERS_COMP_NBR').AsString ) then
        Fields.AddItem('CO_WORKERS_COMP_NBR', EvoCo.CO_WORKERS_COMP_NBR);

      if Assigned(FCL_PERSON_EE.FindField('CO_JOBS_NBR')) and EvoCo.LocateWorkersComp( FCL_PERSON_EE.FieldByName('CO_JOBS_NBR').AsString ) then
        Fields.AddItem('CO_JOBS_NBR', EvoCo.CO_JOBS_NBR);

      EvoCo.LocateDBDTByCustomNumbers( FCL_PERSON_EE.FieldByName('CO_DIVISION_NBR').AsString,
        FCL_PERSON_EE.FieldByName('CO_BRANCH_NBR').AsString, FCL_PERSON_EE.FieldByName('CO_DEPARTMENT_NBR').AsString, '');

      if EvoCo.DivisionFound then
        Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR);

      if EvoCo.BranchFound then
        Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR);

      if EvoCo.DepartmentFound then
        Fields.AddItem('CO_DEPARTMENT_NBR', EvoCo.CO_DEPARTMENT_NBR);

      SetTableFields(EeStartIdx, EeEndIdx, EvoEe, FCL_PERSON_EE, 'EE', Fields, True );
      SetEeDefault(Fields);

      RowChange.AddItem('T', 'I'); // insert
      Fields.AddItem('EE_NBR', FCL_PERSON_EE.FieldByName('EE_NBR').AsInteger);
    end;

    RowChange.AddItem('D', 'EE');
    RowChange.AddItem('F', Fields);

    Changes.AddItem( RowChange );

    FCL_PERSON_EE.Next;
  end;

  EvoResult := FEvoAPI.applyDataChangePacket( Changes );

  FCL_PERSON_EE.First;
  while not FCL_PERSON_EE.Eof do
  begin
    if EvoResult.KeyExists( FCL_PERSON_EE.FieldByName('EE_NBR').AsString ) then
    begin
      FCL_PERSON_EE.Edit;
      FCL_PERSON_EE.FieldByName('EE_NBR').AsInteger := EvoResult.Keys[ FCL_PERSON_EE.FieldByName('EE_NBR').AsString ].AsInteger;
      FCL_PERSON_EE.Post;
    end;

    FCL_PERSON_EE.Next;
  end;
end;

procedure TEvoJbcXmlRPCServer.SetEeDefault(var aFields: IRpcStruct);
begin
  // set default EE fields
//  aFields.AddItem('CURRENT_TERMINATION_CODE', 'A' );
  aFields.AddItem('NEW_HIRE_REPORT_SENT', 'P' );
  aFields.AddItem('POSITION_STATUS', 'N' );
  aFields.AddItem('TIPPED_DIRECTLY', 'N' );
  aFields.AddItem('DISTRIBUTE_TAXES', 'B' );
  aFields.AddItem('FLSA_EXEMPT', 'N' );
//  aFields.AddItem('W2_TYPE', 'N' );
  aFields.AddItem('W2_DECEASED', 'N' );
  aFields.AddItem('W2_STATUTORY_EMPLOYEE', 'N' );
  aFields.AddItem('W2_LEGAL_REP', 'N' );
  aFields.AddItem('BASE_RETURNS_ON_THIS_EE', 'N' );
  aFields.AddItem('TAX_AMT_DETERMINED_1099R', 'N' );
  aFields.AddItem('TOTAL_DISTRIBUTION_1099R', 'N' );
  aFields.AddItem('PENSION_PLAN_1099R', 'N' );
  aFields.AddItem('MAKEUP_FICA_ON_CLEANUP_PR', 'N' );
  aFields.AddItem('GENERATE_SECOND_CHECK', 'N' );
  aFields.AddItem('HIGHLY_COMPENSATED', 'N' );
  aFields.AddItem('CORPORATE_OFFICER', 'N' );
  aFields.AddItem('ELIGIBLE_FOR_REHIRE', 'Y' );
  aFields.AddItem('SELFSERVE_ENABLED', 'N' );
  aFields.AddItem('WC_WAGE_LIMIT_FREQUENCY', 'P' );
  aFields.AddItem('HEALTHCARE_COVERAGE', 'N' );
  aFields.AddItem('EE_ENABLED', 'Y' );
  aFields.AddItem('AUTO_UPDATE_RATES', 'Y' );
  aFields.AddItem('PRINT_VOUCHER', 'Y' );

  // new mandatory fields in Orange
  aFields.AddItem('BENEFITS_ENABLED', 'Y' );
  aFields.AddItem('TIME_OFF_ENABLED', 'Y' );
  aFields.AddItem('EXISTING_PATIENT', 'N' );
  aFields.AddItem('DEPENDENT_BENEFITS_AVAILABLE', 'N' );
  aFields.AddItem('LAST_QUAL_BENEFIT_EVENT', 'Z' );
  aFields.AddItem('W2_FORM_ON_FILE', 'N' );
  aFields.AddItem('W2', 'A' );
  aFields.AddItem('GOV_GARNISH_PRIOR_CHILD_SUPPT', 'N' );

  // new mandatory fields in Plymouth
  aFields.AddItem('EIC', 'N' ); //None
  aFields.AddItem('NEXT_PAY_FREQUENCY', 'W' ); //Weekly
  aFields.AddItem('ACA_STATUS', 'N' ); //N/A

  if (Copy(FEvoAPI.EvoVersion, 1, 2) >= '16') then // New EE Fields in Quechee Release
  begin
    if (Copy(FEvoAPI.EvoVersion, 4, 2) >= '24') then
    begin
      aFields.AddItem('ACA_POLICY_ORIGIN', 'B');
      aFields.AddItem('ENABLE_ANALYTICS', 'Y');
      aFields.AddItem('BENEFITS_ELIGIBLE', 'N');
    end;

    if (Copy(FEvoAPI.EvoVersion, 4, 2) >= '35') then
    begin
      aFields.AddItem('DIRECT_DEPOSIT_ENABLED', 'N' );
      aFields.AddItem('ACA_FORM_ON_FILE', 'N');
      aFields.AddItem('ACA_TYPE', 'N');
      aFields.AddItem('BASE_ACA_ON_THIS_EE', 'Y');
      aFields.AddItem('COMMENSURATE_WAGE', 'N');
    end;
  end;
end;

procedure TEvoJbcXmlRPCServer.ApplyEeStatesDataChangePacket(
  EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
var
  EeNbr, EeStatesNbr, CoStatesNbr, SuiApplyCoStatesNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;

begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FEE_STATES.First;
  while not FEE_STATES.Eof do
  begin
    if EvoCo.LocateCoState( FEE_STATES.FieldByName('CO_STATES_NBR').AsString ) then
    try
      RowChange := TRpcStruct.Create;
      Fields := TRpcStruct.Create;

      CoStatesNbr := EvoCo.CO_STATES_NBR;
      SuiApplyCoStatesNbr := -1;
      if FEE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').AsString <> '' then
      begin
        if EvoCo.LocateCoState( FEE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').AsString ) then
          SuiApplyCoStatesNbr := EvoCo.CO_STATES_NBR
        else
          Msg := Msg + #13 + 'SUI State ' + FEE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').AsString + ' was not found';
      end;

      EeNbr := FEE_STATES.FieldByName('EE_NBR').AsInteger;
      EeStatesNbr := EvoEe.FindEeStatesNbr(EeNbr, CoStatesNbr);

      if EeStatesNbr > -1 then // update Ee
      begin
        FEE_STATES.Edit;
        FEE_STATES.FieldByName('EE_STATES_NBR').AsInteger := EeStatesNbr;
        FEE_STATES.Post;

        SetTableFields(EeStatesStartIdx, EeStatesEndIdx, EvoEe, FEE_STATES, 'EE_STATES', Fields );

        if (SuiApplyCoStatesNbr > -1) then //and (SuiApplyCoStatesNbr <> EvoEe.IntegerFieldValue('EE_STATES', 'SUI_APPLY_CO_STATES_NBR')) then
          Fields.AddItem('SUI_APPLY_CO_STATES_NBR', SuiApplyCoStatesNbr);
        Fields.AddItem('SDI_APPLY_CO_STATES_NBR', CoStatesNbr);

        if EvoCo.LocateSyStateMaritalStatus(FEE_STATES.FieldByName('CO_STATES_NBR').AsString, FEE_STATES.FieldByName('STATE_MARITAL_STATUS').AsString) then
        begin
          Fields.AddItem('SY_STATE_MARITAL_STATUS_NBR', EvoCo.SY_STATE_MARITAL_STATUS);

          RowChange.AddItem('T', 'U'); // update
          RowChange.AddItem('K', EeStatesNbr)
        end
        else
          raise Exception.Create('State Marital Status was not found (State: '+FEE_STATES.FieldByName('State').AsString+' Status: '+FEE_STATES.FieldByName('STATE_MARITAL_STATUS').AsString+')');
      end
      else begin // insert Ee State
        Fields.AddItem('EE_NBR', EeNbr );
        Fields.AddItem('CO_STATES_NBR', CoStatesNbr );
        Fields.AddItem('RECIPROCAL_METHOD', 'N' );
        Fields.AddItem('CALCULATE_TAXABLE_WAGES_1099', 'N' );
        Fields.AddItem('SALARY_TYPE', 'N' );
        SetTableFields(EeStatesStartIdx, EeStatesEndIdx, EvoEe, FEE_STATES, 'EE_STATES', Fields, True );

        if SuiApplyCoStatesNbr > -1 then
          Fields.AddItem('SUI_APPLY_CO_STATES_NBR', SuiApplyCoStatesNbr);

        Fields.AddItem('SDI_APPLY_CO_STATES_NBR', CoStatesNbr);

        if EvoCo.LocateSyStateMaritalStatus(FEE_STATES.FieldByName('CO_STATES_NBR').AsString, FEE_STATES.FieldByName('STATE_MARITAL_STATUS').AsString) then
        begin
          Fields.AddItem('SY_STATE_MARITAL_STATUS_NBR', EvoCo.SY_STATE_MARITAL_STATUS);

          RowChange.AddItem('T', 'I'); // insert
          Fields.AddItem('EE_STATES_NBR', FEE_STATES.FieldByName('EE_STATES_NBR').AsInteger);
        end
        else
          raise Exception.Create('State Marital Status was not found (State: '+FEE_STATES.FieldByName('State').AsString+' Status: '+FEE_STATES.FieldByName('STATE_MARITAL_STATUS').AsString+')');
      end;
      RowChange.AddItem('D', 'EE_STATES');
      RowChange.AddItem('F', Fields);

      if Fields.Count > 0 then
        Changes.AddItem( RowChange );
    except
      on E: Exception do
        Msg := Msg + #13 + E.Message;
    end
    else
      Msg := Msg + #13 + 'State ' + FEE_STATES.FieldByName('CO_STATES_NBR').AsString + ' was not found in Co States table';

    FEE_STATES.Next;
  end;

  if Changes.Count > 0 then
  begin
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );

    FEE_STATES.First;
    while not FEE_STATES.Eof do
    begin
      if EvoResult.KeyExists( FEE_STATES.FieldByName('EE_STATES_NBR').AsString ) then
      begin
        FEE_STATES.Edit;
        FEE_STATES.FieldByName('EE_STATES_NBR').AsInteger := EvoResult.Keys[ FEE_STATES.FieldByName('EE_STATES_NBR').AsString ].AsInteger;
        FEE_STATES.Post;
      end;

      FEE_STATES.Next;
    end;
  end;  
end;

procedure TEvoJbcXmlRPCServer.ApplyEeRatesDataChangePacket(
  EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
var
  EeNbr, EeRatesNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;

begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FEE_RATES.First;
  while not FEE_RATES.Eof do
  begin
    RowChange := TRpcStruct.Create;
    Fields := TRpcStruct.Create;
    try
      EeNbr := FEE_RATES.FieldByName('EE_NBR').AsInteger;
      EeRatesNbr := EvoEe.FindeeRatesNbr(EeNbr, FEE_RATES.FieldByName('RATE_NUMBER').AsInteger);

      EvoCo.LocateDBDTByCustomNumbers( FEE_RATES.FieldByName('CO_DIVISION_NBR').AsString,
        FEE_RATES.FieldByName('CO_BRANCH_NBR').AsString, FEE_RATES.FieldByName('CO_DEPARTMENT_NBR').AsString, '');

      if EvoCo.DivisionFound and ((EeRatesNbr = -1) or (EvoEe.IntegerFieldValue('EE_RATES', 'CO_DIVISION_NBR') <> EvoCo.CO_DIVISION_NBR)) then
        Fields.AddItem('CO_DIVISION_NBR', EvoCo.CO_DIVISION_NBR);

      if EvoCo.BranchFound and ((EeRatesNbr = -1) or (EvoEe.IntegerFieldValue('EE_RATES', 'CO_BRANCH_NBR') <> EvoCo.CO_BRANCH_NBR)) then
        Fields.AddItem('CO_BRANCH_NBR', EvoCo.CO_BRANCH_NBR);

      if EvoCo.DepartmentFound and ((EeRatesNbr = -1) or (EvoEe.IntegerFieldValue('EE_RATES', 'CO_DEPARTMENT_NBR') <> EvoCo.CO_DEPARTMENT_NBR)) then
        Fields.AddItem('CO_DEPARTMENT_NBR', EvoCo.CO_DEPARTMENT_NBR);

      if EeRatesNbr = -1 then // insert rate
      begin
        Fields.AddItem('EE_NBR', EeNbr );
        SetTableFields(EeRatesStartIdx, EeRatesEndIdx, EvoEe, FEE_RATES, 'EE_RATES', Fields, True );

        RowChange.AddItem('T', 'I'); // insert
        Fields.AddItem('EE_RATES_NBR', FEE_RATES.FieldByName('EE_RATES_NBR').AsInteger);
      end
      else begin // update rate
        SetTableFields(EeRatesStartIdx, EeRatesEndIdx, EvoEe, FEE_RATES, 'EE_RATES', Fields );

        RowChange.AddItem('T', 'U'); // update
        RowChange.AddItem('K', EeRatesNbr)
      end;

      RowChange.AddItem('D', 'EE_RATES');
      RowChange.AddItem('F', Fields);

      if Fields.Count > 0 then
        Changes.AddItem( RowChange );
    except
      on E: Exception do
        Msg := Msg + #13 + E.Message;
    end;

    FEE_RATES.Next;
  end;

  if Changes.Count > 0 then
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );

{  FEE_RATES.First;
  while not FEE_RATES.Eof do
  begin
    if EvoResult.KeyExists( FEE_RATES.FieldByName('EE_RATES_NBR').AsString ) then
    begin
      FEE_RATES.Edit;
      FEE_RATES.FieldByName('EE_RATES_NBR').AsInteger := EvoResult.Keys[ FEE_RATES.FieldByName('EE_RATES_NBR').AsString ].AsInteger;
      FEE_RATES.Post;
    end;

    FEE_RATES.Next;
  end;}
end;

procedure TEvoJbcXmlRPCServer.SetLocalsDefault(var aFields: IRpcStruct);
begin
  aFields.AddItem('DEDUCT', 'Y' );
  aFields.AddItem('LOCAL_ENABLED', 'Y' );
  aFields.AddItem('OVERRIDE_LOCAL_TAX_TYPE', 'N' );
  aFields.AddItem('INCLUDE_IN_PRETAX', 'Y' );
  aFields.AddItem('WORK_ADDRESS_OVR', 'N' );
end;

procedure TEvoJbcXmlRPCServer.ApplyEeLocalsDataChangePacket(
  EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
var
  EeNbr, EeLocalsNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;

begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FEE_LOCALS.First;
  while not FEE_LOCALS.Eof do
  begin
    try
      if not EvoCo.LocateCoLocalTax( FEE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsString ) then
        Msg := Msg + #13 + FEE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsString + ' was not found in Co Locals table '
      else begin
        RowChange := TRpcStruct.Create;
        Fields := TRpcStruct.Create;
        EeNbr := FEE_LOCALS.FieldByName('EE_NBR').AsInteger;
        EeLocalsNbr := EvoEe.FindEeLocalNbr(EeNbr, EvoCo.CO_LOCAL_TAX_NBR);
        if EeLocalsNbr = -1 then // insert Ee Local Tax
        begin
          Fields.AddItem('EE_NBR', EeNbr );
          Fields.AddItem('CO_LOCAL_TAX_NBR', EvoCo.CO_LOCAL_TAX_NBR );

          SetLocalsDefault(Fields);
          SetTableFields(EeLocalsStartIdx, EeLocalsEndIdx, EvoEe, FEE_LOCALS, 'EE_LOCALS', Fields, True );

          RowChange.AddItem('T', 'I'); // insert
          Fields.AddItem('EE_LOCALS_NBR', FEE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger);
        end
        else begin // update Ee Local Tax
          SetTableFields(EeLocalsStartIdx, EeLocalsEndIdx, EvoEe, FEE_LOCALS, 'EE_LOCALS', Fields );

          RowChange.AddItem('T', 'U'); // update
          RowChange.AddItem('K', EeLocalsNbr)
        end;
        RowChange.AddItem('D', 'EE_LOCALS');
        RowChange.AddItem('F', Fields);

        if Fields.Count > 0 then
          Changes.AddItem( RowChange );
      end;
    except
      on E: Exception do
        Msg := Msg + #13 + E.Message;
    end;

    FEE_LOCALS.Next;
  end;

  if Changes.Count > 0 then
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );
end;

procedure TEvoJbcXmlRPCServer.ApplyEeDDDataChangePacket(EvoEE: TEeHolder;
  EvoCo: TEvoCoData; var Msg: string);
var
  EeNbr, EeDirDepNbr, EeSchedEDsNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;

begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FEE_DD.First;
  while not FEE_DD.Eof do
  begin
    try
      if not EvoCo.LocateCLEDCode( FEE_DD.FieldByName('CL_E_DS_NBR').AsString ) then
        Msg := Msg + #13 +'E/D "' + FEE_DD.FieldByName('CL_E_DS_NBR').AsString+'" was not found in Cl E/Ds table'
      else begin
        RowChange := TRpcStruct.Create;
        Fields := TRpcStruct.Create;
        EeNbr := FEE_DD.FieldByName('EE_NBR').AsInteger;
        EeSchedEDsNbr := EvoEe.FindEeSchedEDs2(EeNbr, EvoCo.CL_E_DS_NBR);
        if EeSchedEDsNbr > -1 then
          EeDirDepNbr := EvoEe.FindEeDirectDeposit2(EvoEe.IntegerFieldValue('EE_SCHEDULED_E_DS', 'EE_DIRECT_DEPOSIT_NBR'))
        else
          EeDirDepNbr := -1;

        if EeDirDepNbr = -1 then // insert Ee Dir Dep
        begin
          Fields.AddItem('EE_NBR', EeNbr );
          SetDDDefault( Fields );
          SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, FEE_DD, 'EE_DIRECT_DEPOSIT', Fields, True );

          RowChange.AddItem('T', 'I'); // insert
          Fields.AddItem('EE_DIRECT_DEPOSIT_NBR', FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger);
        end
        else begin // update Ee Dir Dep
          FEE_DD.Edit;
          FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger := EeDirDepNbr;
          FEE_DD.Post;

          SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, FEE_DD, 'EE_DIRECT_DEPOSIT', Fields );

          RowChange.AddItem('T', 'U'); // update
          RowChange.AddItem('K', EeDirDepNbr)
        end;
        RowChange.AddItem('D', 'EE_DIRECT_DEPOSIT');
        RowChange.AddItem('F', Fields);

        if Fields.Count > 0 then
          Changes.AddItem( RowChange );
      end;
    except
      on E: Exception do
        Msg := Msg + #13 + E.Message;
    end;

    FEE_DD.Next;
  end;

  if Changes.Count > 0 then
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );

  Changes.Clear;

  FEE_DD.First;
  while not FEE_DD.Eof do
  begin
    if Assigned(EvoResult) then
      if EvoResult.KeyExists( FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsString ) then
      begin
        FEE_DD.Edit;
        FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger := EvoResult.Keys[ FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsString ].AsInteger;
        FEE_DD.Post;
      end;

    try
      if EvoCo.LocateCLEDCode( FEE_DD.FieldByName('CL_E_DS_NBR').AsString ) and (FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger > -1) then
      begin
        RowChange := TRpcStruct.Create;
        Fields := TRpcStruct.Create;
        EeNbr := FEE_DD.FieldByName('EE_NBR').AsInteger;
        EeDirDepNbr := FEE_DD.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger;
        EeSchedEDsNbr := EvoEe.FindEeSchedEDs2(EeNbr, EvoCo.CL_E_DS_NBR);

        SetSchedDefaultED(Fields, EvoCo);
        if EeSchedEDsNbr = -1 then // insert Ee Sched EDs
        begin
          Fields.AddItem('EE_NBR', EeNbr );
          Fields.AddItem('EE_DIRECT_DEPOSIT_NBR', EeDirDepNbr );
          SetSchedDefault(Fields);

          Fields.AddItem('CL_E_DS_NBR', EvoCo.CL_E_DS_NBR);

          if EvoCo.CALCULATION_TYPE = 'F' then
            Fields.AddItem('AMOUNT', FEE_DD.FieldByName('Amount').AsFloat)
          else
            Fields.AddItem('PERCENTAGE', FEE_DD.FieldByName('Amount').AsFloat);

          SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, FEE_DD, 'EE_SCHEDULED_E_DS', Fields, True );

          RowChange.AddItem('T', 'I'); // insert
          Fields.AddItem('EE_SCHEDULED_E_DS_NBR', FEE_DD.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);
        end
        else begin // update Ee Sched EDs
          if EvoCo.CALCULATION_TYPE = 'F' then
          begin
            if EvoEe.DoubleFieldValue('EE_SCHEDULED_E_DS', 'AMOUNT') <> FEE_DD.FieldByName('Amount').AsFloat then
              Fields.AddItem('AMOUNT', FEE_DD.FieldByName('Amount').AsFloat);
          end
          else begin
            if EvoEe.DoubleFieldValue('EE_SCHEDULED_E_DS', 'PERCENTAGE') <> FEE_DD.FieldByName('Amount').AsFloat then
              Fields.AddItem('PERCENTAGE', FEE_DD.FieldByName('Amount').AsFloat);
          end;

          SetTableFields(EeDDStartIdx, EeDDEndIdx, EvoEe, FEE_DD, 'EE_SCHEDULED_E_DS', Fields );

          RowChange.AddItem('T', 'U'); // update
          RowChange.AddItem('K', EeSchedEDsNbr);
        end;
      end;

      RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
      RowChange.AddItem('F', Fields);

      if Fields.Count > 0 then
        Changes.AddItem( RowChange );
    except
      on E: Exception do
        Msg := Msg + #13 + E.Message;
    end;

    FEE_DD.Next;
  end;

  if Changes.Count > 0 then
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );
end;

procedure TEvoJbcXmlRPCServer.SetDDDefault(var aFields: IRpcStruct);
begin
  aFields.AddItem('IN_PRENOTE', 'N' );
  aFields.AddItem('ALLOW_HYPHENS', 'N' );
  aFields.AddItem('FORM_ON_FILE', 'Y' );
  if Copy(FEvoAPI.EvoVersion, 1, 2) = '16' then // Quechee Release
  begin
    aFields.AddItem('SHOW_IN_EE_PORTAL', 'N');
    if (Copy(FEvoAPI.EvoVersion, 4, 2) >= '35') then
      aFields.AddItem('READ_ONLY_REMOTES', 'N');
  end;
end;

procedure TEvoJbcXmlRPCServer.SetSchedDefaultED(var aFields: IRpcStruct;
  EvoCo: TEvoCoData);
begin
  aFields.AddItem('CALCULATION_TYPE', EvoCo.CALCULATION_TYPE);
  aFields.AddItem('FREQUENCY', EvoCo.FREQUENCY);
  aFields.AddItem('EXCLUDE_WEEK_1', EvoCo.EXCLUDE_WEEK_1);
  aFields.AddItem('EXCLUDE_WEEK_2', EvoCo.EXCLUDE_WEEK_2);
  aFields.AddItem('EXCLUDE_WEEK_3', EvoCo.EXCLUDE_WEEK_3);
  aFields.AddItem('EXCLUDE_WEEK_4', EvoCo.EXCLUDE_WEEK_4);
  aFields.AddItem('EXCLUDE_WEEK_5', EvoCo.EXCLUDE_WEEK_5);
  aFields.AddItem('ALWAYS_PAY', EvoCo.ALWAYS_PAY);
  aFields.AddItem('DEDUCTIONS_TO_ZERO', EvoCo.DEDUCTIONS_TO_ZERO);
  aFields.AddItem('DEDUCT_WHOLE_CHECK', EvoCo.DEDUCT_WHOLE_CHECK);
  aFields.AddItemDateTime('EFFECTIVE_START_DATE', EvoCo.EFFECTIVE_START_DATE);
  aFields.AddItem('PLAN_TYPE', EvoCo.PLAN_TYPE);
  aFields.AddItem('WHICH_CHECKS', EvoCo.WHICH_CHECKS);
  aFields.AddItem('USE_PENSION_LIMIT', EvoCo.USE_PENSION_LIMIT);
end;

procedure TEvoJbcXmlRPCServer.SetSchedDefault(var aFields: IRpcStruct);
begin
  aFields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
  aFields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
  aFields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
  aFields.AddItem('TARGET_ACTION', 'N');
end;

procedure TEvoJbcXmlRPCServer.ApplyEePieceWorkDataChangePacket(
  EvoEE: TEeHolder; EvoCo: TEvoCoData; var Msg: string);
var
  EeNbr, EePieceWorkNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;

begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FEE_PIECE_WORK.First;
  while not FEE_PIECE_WORK.Eof do
  begin
    try
      if not EvoCo.LocateCLPiece( FEE_PIECE_WORK.FieldByName('CL_PIECES_NBR').AsString ) then
        Msg := Msg + #13 + FEE_PIECE_WORK.FieldByName('CL_PIECES_NBR').AsString + ' was not found in Cl Pieces table, '
      else begin
        RowChange := TRpcStruct.Create;
        Fields := TRpcStruct.Create;

        EeNbr := FEE_PIECE_WORK.FieldByName('EE_NBR').AsInteger;
        EePieceWorkNbr := EvoEe.FindEePieceWorkNbr(EeNbr, EvoCo.CL_PIECES_NBR);

        if EePieceWorkNbr = -1 then // insert Ee Piece Work
        begin
          Fields.AddItem('EE_NBR', EeNbr );
          Fields.AddItem('CL_PIECES_NBR', EvoCo.CL_PIECES_NBR );
          SetTableFields(PieceWorkStartIdx, PieceWorkEndIdx, EvoEe, FEE_PIECE_WORK, 'EE_PIECE_WORK', Fields, True );

          RowChange.AddItem('T', 'I'); // insert
          Fields.AddItem('EE_PIECE_WORK_NBR', FEE_PIECE_WORK.FieldByName('EE_PIECE_WORK_NBR').AsInteger);
        end
        else begin // update Ee Piece Work
          SetTableFields(PieceWorkStartIdx, PieceWorkEndIdx, EvoEe, FEE_PIECE_WORK, 'EE_PIECE_WORK', Fields );

          RowChange.AddItem('T', 'U'); // update
          RowChange.AddItem('K', EePieceWorkNbr)
        end;

        RowChange.AddItem('D', 'EE_PIECE_WORK');
        RowChange.AddItem('F', Fields);

        if Fields.Count > 0 then
          Changes.AddItem( RowChange );
      end;
    except
      on E: Exception do
        Msg := Msg + #13 + E.Message;
    end;

    FEE_PIECE_WORK.Next;
  end;

  if Changes.Count > 0 then
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );
end;

procedure TEvoJbcXmlRPCServer.ApplyEeHomeTaxChangePacket;
var
  EeNbr: integer;
  Fields, RowChange, EvoResult: IRpcStruct;
  Changes: IRpcArray;
  EeHomeTaxState: string;
begin
  Changes := TRpcArray.Create;
  EvoResult := TRpcStruct.Create;

  FCL_PERSON_EE.First;
  while not FCL_PERSON_EE.Eof do
  begin
    EeHomeTaxState := FCL_PERSON_EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsString;
    if EeHomeTaxState <> '' then
    begin
      EeNbr := FCL_PERSON_EE.FieldByName('EE_NBR').AsInteger;
      if FEE_STATES.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([EeNbr, EeHomeTaxState]), []) then
      begin
        RowChange := TRpcStruct.Create;
        Fields := TRpcStruct.Create;
        Fields.AddItem('HOME_TAX_EE_STATES_NBR', FEE_STATES.FieldByName('EE_STATES_NBR').AsInteger );

        RowChange.AddItem('T', 'U'); // update
        RowChange.AddItem('K', EeNbr);

        RowChange.AddItem('D', 'EE');
        RowChange.AddItem('F', Fields);

        if Fields.Count > 0 then
          Changes.AddItem( RowChange );
      end;
    end;
    FCL_PERSON_EE.Next;
  end;

  if Changes.Count > 0 then
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );
end;

procedure TEvoJbcXmlRPCServer.UpdateEeHomeTaxState(aEeNbr, aEeStatesNbr: integer; var aMsg: string);
var
  Fields: IRpcStruct;
begin
  // update Home State
  try
    Fields := TRpcStruct.Create;
    FChanges.Clear;
    Fields.AddItem('HOME_TAX_EE_STATES_NBR', aEeStatesNbr);

    aMsg := aMsg + ' > Set Ee Home Tax State, ';
    AddRowChange('U', 'EE', aEeNbr, Fields);

    FEvoAPI.applyDataChangePacket(FChanges);
  except
    on e: Exception do
      aMsg := aMsg + 'error: <' + e.Message + '>, ';
  end;
  aMsg := Copy(aMsg, 1, Length(aMsg) - 2);
end;

procedure TEvoJbcXmlRPCServer.AddRowChange(const UpdateType,
  TableName: string; var aKeyNbr: integer; var aFields: IRpcStruct);
var
  RowChange: IRpcStruct;
begin
  if aFields.Count > 0 then
  begin
    RowChange := TRpcStruct.Create;
    RowChange.AddItem('T', UpdateType);
    RowChange.AddItem('D', TableName);
    RowChange.AddItem('F', aFields);
    if UpdateType = 'I' then
    begin
      Dec(KeyNbr);
      aKeyNbr := KeyNbr;
      aFields.AddItem(TableName + '_NBR', aKeyNbr);
    end
    else
      RowChange.AddItem('K', aKeyNbr);

    FChanges.AddItem( RowChange );
  end;
end;

procedure TEvoJbcXmlRPCServer.StoreFedMStatus(aFields: IRpcStruct);
begin
  FedMStatus := 'S';
  if aFields.KeyExists('FEDERAL_MARITAL_STATUS') then
    FedMStatus := aFields.Keys['FEDERAL_MARITAL_STATUS'].AsString;
end;

end.
