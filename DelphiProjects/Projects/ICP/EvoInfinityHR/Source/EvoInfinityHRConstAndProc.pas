unit EvoInfinityHRConstAndProc;

interface

uses
  Forms, EvoAPIConnectionParamFrame, common, log, isSettings, gdyCommonLogger,
  sysutils, evodata, DateRangeFrame, Windows, Classes, fmtBcd, EvStreamUtils;

const
  sMainForm = 'MainForm';
  sInfinityConn = 'InfinityConn';
  sEvoConn = 'EvoConn';
  sSmtpConfig = 'EvoInfinityEmailSettings';

  SY_STATESDesc: TEvoDSDesc = (Name: 'SY_STATES');
  SY_STATE_MARITAL_STATUSDesc: TEvoDSDesc = (Name: 'SY_STATE_MARITAL_STATUS');

type
  TFrameClass = class of TFrame;
  TEvoAPIConnectionParamFrmClass = class of TEvoAPIConnectionParamFrm;

  TInfinityHRConnectionParam = record
    UserName: string;
    Password: string;
    SavePassword: boolean;
    Enterprise: boolean;
    URL: string;
  end;

  EInfinityHRError = class(Exception);

  TEvoCompany = record
    CL_NBR: integer;
    CO_NBR: integer;
    Custom_Company_Number: string
  end;

  TTaskParam = record
    Title: string;
    InputParameters: string;
    SendEmail: boolean;
    AttachLog: boolean;
    ElectedED: boolean;
    TransHRates: boolean;
  end;

  TUploadFile = record
    SSN: string;
    PayPeriod_StartDate: TDatetime;
    PayPeriod_EndDate: TDatetime;
    PayDate: TDateTime;
    CheckNumber: string;          
    DepositAmount: TBcd;
    FileName: string;
    FileContent: string;
  end;

  TUploadW2File = record
    SSN: string;
    TaxYear: integer;
    EeCaption: string;
    FileContent: string;
  end;

  TUploadFiles = array of TUploadFile;

var
  frmLogger: TfrmLogger;

procedure SaveFormSize(aForm: TForm; aSettings: IisSettings);
procedure LoadFormSize(aForm: TForm; aSettings: IisSettings);

procedure SaveDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);
procedure LoadDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);

function LoadInfinityHRConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TInfinityHRConnectionParam;
procedure SaveInfinityHRConnectionParam( const param: TInfinityHRConnectionParam; conf: IisSettings; root: string);

function ConvertNull(MyValue, Replacement: Variant): Variant;
function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;

function GetNextRunTime(aTime: TSystemTime): string;

procedure GetDateRangeDatesByString(const s: string; var aDat_b: TDatetime; var aDat_e: TDatetime);

function GetTaskParam(const aGuid: string): TTaskParam;
procedure SetTaskParam(const aGuid: string; aTaskParam: TTaskParam);
function IntConvertNull(MyValue: Variant; Replacement: integer): integer;
function GetVersion: string;

implementation

uses gdyCommon, gdyCrypt, Variants, DateUtils, gdyRedir;

const
  //!! encrypt it?
  cKey='Form.Button';

function GetVersion: string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

procedure GetDateRangeDatesByString(const s: string; var aDat_b: TDatetime; var aDat_e: TDatetime);
begin
  with TfrmDateRange.Create(nil) do
  try
    AsString := s;
    aDat_b := Dat_b;
    aDat_e := Dat_e;
  finally
    Free;
  end;
end;

function IntConvertNull(MyValue: Variant; Replacement: integer): integer;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function GetTaskParam(const aGuid: string): TTaskParam;
var
  sl: TStrings;
  fname: string;
  b: boolean;
begin
  // defaults
  Result.Title := 'InfinityEvo';
  Result.InputParameters := '';

  fname := Redirection.GetDirectory('TaskData') + aGuid +'\Parameters.txt';
  if FileExists( fname ) then
  begin
    sl := TStringList.Create;
    try
      sl.LoadFromFile( fname );
      if sl.Count >= 2 then
      begin
        Result.Title := sl[0];
        Result.InputParameters := sl[1];

        Result.SendEmail := True;
        if sl.Count > 2 then
          if TryStrToBool(sl[2], b) then
            Result.SendEmail := b;

        Result.AttachLog := True;
        if sl.Count > 3 then
          if TryStrToBool(sl[3], b) then
            Result.AttachLog := b;

        Result.ElectedED := True;
        if sl.Count > 4 then
          if TryStrToBool(sl[4], b) then
            Result.ElectedED := b;

        Result.TransHRates := False;
        if sl.Count > 5 then
          if TryStrToBool(sl[5], b) then
            Result.TransHRates := b;
      end;
    finally
      sl.Free;
    end;
  end;
end;

procedure SetTaskParam(const aGuid: string; aTaskParam: TTaskParam);
var
  sl: TStrings;
  fname, dir: string;
begin
  dir := Redirection.GetDirectory('TaskData');
  if not DirectoryExists(dir) then
    if not CreateDir( dir ) then
      raise Exception.Create('Cannot create ' + dir);

  dir := dir + aGuid;
  if not DirectoryExists(dir) then
    if not CreateDir( dir ) then
      raise Exception.Create('Cannot create ' + dir);

  fname := dir +'\Parameters.txt';

  sl := TStringList.Create;
  try
    sl.Add(aTaskParam.Title);
    sl.Add(aTaskParam.InputParameters);
    sl.Add(BoolToStr(aTaskParam.SendEmail));
    sl.Add(BoolToStr(aTaskParam.AttachLog));
    sl.Add(BoolToStr(aTaskParam.ElectedED));
    sl.Add(BoolToStr(aTaskParam.TransHRates));

    sl.SaveToFile( fname );
  finally
    sl.Free;
  end;
end;

function GetNextRunTime(aTime: TSystemTime): string;
begin
  if aTime.wYear >= YearOf(Now) then
    Result := Format('Next run at %s on %d/%d/%d', [TimeToStr( EncodeTime(aTime.wHour, aTime.wMinute, aTime.wSecond, 0)), aTime.wMonth, aTime.wDay, aTime.wYear])
  else
    Result := 'Done';
end;

function ConvertNull(MyValue, Replacement: Variant): Variant;
begin
  if VarIsNull(MyValue) then
    Result := Replacement
  else
    Result := MyValue;
end;

function PadLeft(MyString: string; MyPad: Char; MyLength: Integer): string;
begin
  Result := Copy(MyString, 1, MyLength);
  while Length(Result) < MyLength do
    Result := MyPad + Result;
end;

function LoadInfinityHRConnectionParam(Logger: ICommonLogger; conf: IisSettings; root: string): TInfinityHRConnectionParam;
begin
  root := root + IIF(root='','','\') + 'InfinityHRConnection\';
  try
    Logger.LogDebug('XXX: key='+root+'URL');
    Result.URL := conf.AsString[root+'URL'];
    Logger.LogDebug('XXX: val='+Result.URL);
  except
    Logger.StopException;
  end;
  try
    Result.Username := conf.AsString[root+'Username'];
  except
    Logger.StopException;
  end;
  try
    if conf.GetValueNames(root).IndexOf('Password') <> -1 then
      Result.Password := conf.AsString[root+'Password']
    else
      Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
  except
    Logger.StopException;
  end;
  try
    Result.SavePassword := conf.AsBoolean[root+'SavePassword'];
  except
    Logger.StopException;
  end;
  try
    Result.Enterprise := conf.AsBoolean[root+'Enterprise'];
  except
    Logger.StopException;
  end;
end;

procedure SaveInfinityHRConnectionParam( const param: TInfinityHRConnectionParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'InfinityHRConnection\';
  conf.AsString[root+'URL'] := param.URL;
  conf.AsString[root+'Username'] := param.Username;
  conf.DeleteValue(root+'Password');
  if param.SavePassword then
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey))
  else
    conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt('', cKey));
  conf.AsBoolean[root+'SavePassword'] := param.SavePassword;
  conf.AsBoolean[root+'Enterprise'] := param.Enterprise;
end;

procedure SaveFormSize(aForm: TForm; aSettings: IisSettings);
var
  root: string;
begin
  root := aForm.Name;
  try
    if aForm.WindowState = wsMaximized then
      aSettings.AsInteger[root+'Maximized'] := 1
    else begin
      aSettings.AsInteger[root+'Maximized'] := 0;
      aSettings.AsInteger[root+'Top'] := aForm.Top;
      aSettings.AsInteger[root+'Left'] := aForm.Left;
      aSettings.AsInteger[root+'Width'] := aForm.Width;
      aSettings.AsInteger[root+'Height'] := aForm.Height;
    end;
  except
  end;
end;

procedure LoadFormSize(aForm: TForm; aSettings: IisSettings);
var
  root: string;
begin
  root := aForm.Name;
  try
    aForm.Top := aSettings.AsInteger[root+'Top'];
    aForm.Left := aSettings.AsInteger[root+'Left'];
    aForm.Width := aSettings.AsInteger[root+'Width'];
    aForm.Height := aSettings.AsInteger[root+'Height'];
    if aSettings.AsInteger[root+'Maximized'] = 1 then
      aForm.WindowState := wsMaximized;
  except
  end;
end;

procedure SaveDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);
var
  root: string;
begin
  root := aDateRange.Name;
  try
    aSettings.AsString[root+'Period'] := aDateRange.PeriodType;
    aSettings.AsDateTime[root+'Dat_b'] := aDateRange.Dat_b;
    aSettings.AsDateTime[root+'Dat_e'] := aDateRange.Dat_e;
    aSettings.AsString[root+'StringPeriod'] := aDateRange.AsString;
  except
  end;
end;

procedure LoadDateRange(aDateRange: TfrmDateRange; aSettings: IisSettings);
var
  root: string;
begin
  root := aDateRange.Name;
  try
    if aSettings.AsString[root+'StringPeriod'] <> '' then
      aDateRange.AsString := aSettings.AsString[root+'StringPeriod']
    else begin
      aDateRange.Dat_b := aSettings.AsDateTime[root+'Dat_b'];
      aDateRange.Dat_e := aSettings.AsDateTime[root+'Dat_e'];
      aDateRange.PeriodType := aSettings.AsString[root+'Period'];
      if YearOf( aDateRange.Dat_b ) < 1979 then
        aDateRange.SetDefaults;
    end;    
  except
  end;
end;

end.
