inherited frmSmtpConfig: TfrmSmtpConfig
  Left = 598
  Top = 279
  Caption = 'Email Settings'
  ClientHeight = 201
  ClientWidth = 604
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOk: TBitBtn
    Left = 440
    Top = 166
  end
  inherited btnCancel: TBitBtn
    Left = 522
    Top = 166
  end
  inline SmtpConfigFrm: TSmtpConfigFrm
    Left = 3
    Top = 1
    Width = 597
    Height = 155
    TabOrder = 2
    inherited GroupBox1: TGroupBox
      Width = 597
      Height = 155
      inherited Label6: TLabel
        Top = 62
      end
      inherited Label8: TLabel
        Top = 17
      end
      inherited edAddressTo: TEdit
        Top = 78
        Width = 238
      end
      inherited edAddressFrom: TEdit
        Top = 33
        Width = 238
      end
      inherited btnSendTest: TBitBtn
        Top = 114
      end
      inherited gbSMTP: TGroupBox
        Top = 16
      end
      inherited cbSendEmail: TCheckBox
        Visible = False
      end
    end
  end
end
