unit UploadTaskParameters;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, DateRangeFrame, EvoInfinityHRConstAndProc,
  common, SimpleScheduler, gdyCommonLogger, jclTask, SelectCheckFrm;

type
  TfrmUploadTaskParameters = class(TfrmDialog)
    gbDestination: TGroupBox;
    gbDatePeriod: TGroupBox;
    Scheduler: TfrmSimpleScheduler;
    gbReport: TGroupBox;
    chbSendEmail: TCheckBox;
    chbAttachLog: TCheckBox;
    lblTaskCaption: TLabel;
    SelectCheckFrm: TSelectCheckFrame;
  private
    FParameter: string;
  protected
    procedure DoOK; override;
    procedure BeforeShowDialog; override;
  public  
    constructor Create(AOwner: TComponent); override;
  end;

  function EditUploadTask(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;

implementation

{$R *.dfm}

function EditUploadTask(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;
var
  FSysScheduler: TJclTaskSchedule;
begin
  FSysScheduler := nil;
  with TfrmUploadTaskParameters.Create( Application ) do
  try
    FParameter := Parameter;
    with GetTaskParam( FParameter ) do
    begin
      SelectCheckFrm.InitFrame(frmLogger.Logger, FSettings, sEvoConn, True);
      SelectCheckFrm.AsString := InputParameters;
      chbSendEmail.Checked := SendEmail;
      chbAttachLog.Checked := AttachLog;
    end;
    if Assigned(SysScheduler) then
      Scheduler.Init(Parameter, Logger, SysScheduler, Task)
    else begin
      if not TJclTaskSchedule.IsRunning then
        TJclTaskSchedule.Start;
      FSysScheduler := TJclTaskSchedule.Create;
      Scheduler.Init(Parameter, Logger, FSysScheduler, Task);
    end;

    Result := ShowModal = mrOk;
  finally
    if Assigned(FSysScheduler) then
      FreeAndNil(FSysScheduler);
    Free;
  end;
end;

{ TfrmUploadTaskParameters }

procedure TfrmUploadTaskParameters.BeforeShowDialog;
begin
  inherited;
  SelectCheckFrm.InitFrame(frmLogger.Logger, FSettings, sEvoConn);
end;

constructor TfrmUploadTaskParameters.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FSettings := AppSettings;
end;

procedure TfrmUploadTaskParameters.DoOK;
var
  TaskParam: TTaskParam;
begin
  inherited;

  if Scheduler.Save then
  begin
    // create/update task directory
    TaskParam.Title := 'UploadPaystubs';
    TaskParam.InputParameters := SelectCheckFrm.AsString;
    TaskParam.SendEmail := chbSendEmail.Checked;
    TaskParam.AttachLog := chbAttachLog.Checked;
    TaskParam.ElectedED := False;
    TaskParam.TransHRates := False;
    SetTaskParam( FParameter, TaskParam );
    ModalResult := mrOk;
  end
  else begin
    ShowMessage('Please, schedule the task');
    ModalResult := mrNone;
  end;
end;

end.
