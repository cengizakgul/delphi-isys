unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ActnList, Menus, common, isSettings, StdCtrls,
  EvoInfinityHRConstAndProc, ExtCtrls, Status, xmldom, XMLIntf, msxmldom,
  XMLDoc, DateRangeFrame, dbcomp, Grids, Wwdbigrd, Wwdbgrid,
  ISBasicClasses, {EvoData,} EeHolder, {CheckSelect,} CheckSelectCommon,
  InvokeRegistry, Rio, SOAPHTTPClient, SMTPConfigDlg, SelectCheckFrm,
  Buttons, ImgList, SelectEmployeeFrm;

type
  TfrmMain = class(TForm)
    StatusBar: TStatusBar;
    MainMenu: TMainMenu;
    miFile: TMenuItem;
    miHelp: TMenuItem;
    miExit: TMenuItem;
    Actions: TActionList;
    Exit: TAction;
    miSettings: TMenuItem;
    miEvoConnection: TMenuItem;
    miInfinityHRConnection: TMenuItem;
    miScheduler: TMenuItem;
    miAbout: TMenuItem;
    EvoConnection: TAction;
    ShowLog: TAction;
    miOpenLog: TMenuItem;
    N1: TMenuItem;
    InfinityHRConnection: TAction;
    InfinityToEvo: TAction;
    pnlMain: TPanel;
    frmStatus: TfrmStatus;
    About: TAction;
    Scheduler: TAction;
    pcFunctions: TPageControl;
    tsEeTransfer: TTabSheet;
    pnlButtons: TPanel;
    btnInfinityToEvo: TButton;
    Button2: TButton;
    gbDateRange: TGroupBox;
    DateRange: TfrmDateRange;
    tsUploadPayStub: TTabSheet;
    pnlPayStub: TPanel;
    pnlStartUpload: TPanel;
    btnUpload: TButton;
    Upload: TAction;
    SMTPConfig: TAction;
    miEmailSettings: TMenuItem;
    SelectCheckFrm: TSelectCheckFrame;
    SchedInfinityToEvo: TAction;
    sbSchedInfinityEvo: TSpeedButton;
    sbSchedEvoInfinity: TSpeedButton;
    sbSchedUpload: TSpeedButton;
    SchedUpload: TAction;
    Images: TImageList;
    tsUploadW2: TTabSheet;
    pnlW2: TPanel;
    pnlStartW2Upload: TPanel;
    sbtnSchedUploadW2: TSpeedButton;
    btnUploadW2: TButton;
    SelectEmployeeFrm: TSelecEmployeeFrame;
    UploadW2: TAction;
    SchedUploadW2: TAction;
    gbOptions: TGroupBox;
    chbElectedED: TCheckBox;
    chbTrasferHourlyRates: TCheckBox;
    procedure ExitExecute(Sender: TObject);
    procedure EvoConnectionExecute(Sender: TObject);
    procedure ShowLogExecute(Sender: TObject);
    procedure InfinityHRConnectionExecute(Sender: TObject);
    procedure InfinityToEvoExecute(Sender: TObject);
    procedure AboutExecute(Sender: TObject);
    procedure SchedulerExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UploadUpdate(Sender: TObject);
    procedure UploadExecute(Sender: TObject);
    procedure SMTPConfigExecute(Sender: TObject);
    procedure SchedInfinityToEvoExecute(Sender: TObject);
    procedure SchedUploadExecute(Sender: TObject);
    procedure UploadW2Update(Sender: TObject);
    procedure UploadW2Execute(Sender: TObject);
    procedure SchedUploadW2Execute(Sender: TObject);
    procedure sbtnSchedUploadW2Click(Sender: TObject);
  private
    FSettings: IisSettings;
    FEvoConn: TEvoAPIConnectionParam;
    FInfinityConn: TInfinityHRConnectionParam;
    FIncompleteEeSetup: string;

    procedure CheckForScheduledRun;
    function CheckConnectionParameters: boolean;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses log, EvoConnectionDlg, InfinityHRConnectionDlg, CheckSelect,
  EvoInfinityHRProcessing, Math, gdyDeferredCall, About, Scheduler,
  SmtpConfigFrame, ImportTaskParameters, ComObj, UploadTaskParameters,
  UploadW2TaskParameters, DateUtils;

{ TfrmMain }

procedure TfrmMain.ExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.EvoConnectionExecute(Sender: TObject);
begin
  if TfrmEvoConnectionDlg.ShowDialog = mrOk then
    FEvoConn := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn );
end;

procedure TfrmMain.ShowLogExecute(Sender: TObject);
begin
  frmLogger.ShowModal;
end;

destructor TfrmMain.Destroy;
begin
  FSettings.AsInteger['ActivePage'] := pcFunctions.ActivePageIndex;
  FSettings.AsBoolean['SyncElected'] := chbElectedED.Checked;
  FSettings.AsBoolean['TransHRates'] := chbTrasferHourlyRates.Checked;
  SaveFormSize(Self, FSettings);
  SaveDateRange( DateRange, FSettings );
  inherited;
  FreeAndNil(frmLogger);
end;

constructor TfrmMain.Create(Owner: TComponent);
begin
  frmLogger := TfrmLogger.Create( Application );

  inherited;
  FSettings := AppSettings;
  LoadFormSize(Self, FSettings);
  pcFunctions.ActivePageIndex := FSettings.AsInteger['ActivePage'];
  chbElectedED.Checked := FSettings.GetValue('SyncElected', True);
  chbTrasferHourlyRates.Checked := FSettings.GetValue('TransHRates', False);

  FInfinityConn := LoadInfinityHRConnectionParam( frmLogger.Logger, FSettings, sInfinityConn );
  FEvoConn := LoadEvoAPIConnectionParam( frmLogger.Logger, FSettings, sEvoConn );
  LoadDateRange( DateRange, FSettings );

  SelectCheckFrm.InitFrame(frmLogger.Logger, FSettings, sEvoConn);
  SelectEmployeeFrm.InitFrame(frmLogger.Logger, FSettings, sEvoConn);

  frmStatus.SetLogger( frmLogger.Logger );
  frmStatus.Clear;
  frmStatus.SetMessage('Ready to Start','','');
  frmLogger.Logger.LogDebug('EvoInfnityHR','Version: ' + EvoInfinityHRConstAndProc.GetVersion);
end;

procedure TfrmMain.InfinityHRConnectionExecute(Sender: TObject);
begin
  if TfrmInfinityHRConnectionDlg.ShowDialog = mrOk then
    FInfinityConn := LoadInfinityHRConnectionParam( frmLogger.Logger, FSettings, sInfinityConn );
end;

procedure TfrmMain.InfinityToEvoExecute(Sender: TObject);
begin
  FIncompleteEeSetup := '';
  if CheckConnectionParameters then
  with TImportProcessing.Create(frmLogger.Logger, FEvoConn, FInfinityConn, frmStatus) do
  try
    InfinityToEvo( DateRange.Dat_b, DateRange.Dat_e, chbElectedED.Checked, chbTrasferHourlyRates.Checked );
    FIncompleteEeSetup := IncompleteEeSetupWarnings;
  finally
    Free;
  end
end;

procedure TfrmMain.AboutExecute(Sender: TObject);
begin
  ShowAbout;
end;

procedure TfrmMain.SchedulerExecute(Sender: TObject);
begin
  ShowScheduler( frmLogger.Logger );
end;

procedure TfrmMain.CheckForScheduledRun;
var
  Param: TTaskParam;
  SmtpConfig: TSmtpConfig;
  fn: string;
begin
  if (ParamCount > 0) then
  begin
    Param := GetTaskParam( ParamStr(1) );

    Self.WindowState := wsMinimized;
    Self.Hide;

    if Param.Title = 'InfinityEvo' then
    begin
      DateRange.AsString := Param.InputParameters;
      chbElectedED.Checked := Param.ElectedED;
      chbTrasferHourlyRates.Checked := Param.TransHRates;
      InfinityToEvoExecute( Self );
    end
    else if Param.Title = 'UploadPaystubs' then
    begin //Upload Paystubs
      SelectCheckFrm.AsString := Param.InputParameters;
      UploadExecute( Self );
    end
    else begin // Upload W2
      SelectEmployeeFrm.AsString := Param.InputParameters;
      UploadW2Execute( Self );
    end;

    frmStatus.mmMessage.Lines.Delete( frmStatus.mmMessage.Lines.Count - 1 );
    if Param.SendEmail then
    try
      SmtpConfig := LoadSmtpConfig( FSettings, sSmtpConfig );

      if FIncompleteEeSetup <> '' then
        SendMail( SmtpConfig, 'EvoInfinityHR scheduled task - incomplete setup', FIncompleteEeSetup, '' );

      if Param.AttachLog then
      begin
        fn := frmLogger.PackLogFiles( ExtractFileDir(Application.ExeName) + '\To Send\', True, True );
        try
          SendMail( SmtpConfig, 'EvoInfinityHR report', frmStatus.mmMessage.Lines.Text, fn );
        finally
          DeleteFile( fn );
        end;
      end
      else
        SendMail( SmtpConfig, 'EvoInfinityHR report', frmStatus.mmMessage.Lines.Text, '' );
    except
    end;

    Close;
  end;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  DeferredCall( CheckForScheduledRun );
end;

procedure TfrmMain.UploadUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Length(SelectCheckFrm.CheckList) > 0;
end;

procedure TfrmMain.UploadExecute(Sender: TObject);
begin
  if CheckConnectionParameters then
  with TImportProcessing.Create(frmLogger.Logger, FEvoConn, FInfinityConn, frmStatus) do
  try
    LoadCheckStubs( SelectCheckFrm.CheckList );
  finally
    Free;
  end
end;

function TfrmMain.CheckConnectionParameters: boolean;
begin
  Result := False;
  if (Trim(FEvoConn.APIServerAddress) = '') or (Trim(FEvoConn.Username) = '') or (Trim(FEvoConn.Password) = '') then
    frmStatus.SetMessage('Evo Connection settings were not set up properly', ' ', ' ')
  else if (Trim(FInfinityConn.URL) = '') or (Trim(FInfinityConn.Username) = '') or (Trim(FInfinityConn.Password) = '') then
    frmStatus.SetMessage('InfinityHR Connection settings were not set up properly', ' ', ' ')
  else
    Result := True;
end;

procedure TfrmMain.SMTPConfigExecute(Sender: TObject);
begin
  TfrmSmtpConfig.ShowDialog;
end;

procedure TfrmMain.SchedInfinityToEvoExecute(Sender: TObject);
begin
  EditImportTask(CreateClassID, frmLogger.Logger, nil);
end;

procedure TfrmMain.SchedUploadExecute(Sender: TObject);
begin
  EditUploadTask(CreateClassID, frmLogger.Logger, nil);
end;

procedure TfrmMain.UploadW2Update(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Length(SelectEmployeeFrm.EmployeeList) > 0;
end;

procedure TfrmMain.UploadW2Execute(Sender: TObject);
var
  Year: integer;
begin
  if CheckConnectionParameters then
  with TImportProcessing.Create(frmLogger.Logger, FEvoConn, FInfinityConn, frmStatus) do
  try
    if (ParamCount > 0) then
      LoadEmployeeW2s( SelectEmployeeFrm.EmployeeList, YearOf(Today) - 1 ) // W2 upload for the prior year by default
    else begin
      if MessageDlg('Would you like upload employee W2 for the prior year?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        LoadEmployeeW2s( SelectEmployeeFrm.EmployeeList, YearOf(Today) - 1 )
      else begin
        if TryStrToInt(Trim(InputBox('Select Tax Year', 'Tax Year to retrieve W-2s for', IntToStr(YearOf(Today) - 1))), Year) then
          LoadEmployeeW2s( SelectEmployeeFrm.EmployeeList, Year );
      end;
    end;
  finally
    Free;
  end
end;

procedure TfrmMain.SchedUploadW2Execute(Sender: TObject);
begin
  EditUploadW2Task(CreateClassID, frmLogger.Logger, nil);
end;

procedure TfrmMain.sbtnSchedUploadW2Click(Sender: TObject);
begin
  EditUploadW2Task(CreateClassID, frmLogger.Logger, nil);
end;

end.
