unit DateRangeFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Spin;

type
  TfrmDateRange = class(TFrame)
    edDatB: TDateTimePicker;
    edDatE: TDateTimePicker;
    lblFrom: TLabel;
    lblTo: TLabel;
    edTimeB: TDateTimePicker;
    edTimeE: TDateTimePicker;
    cbPeriod: TComboBox;
    lblPastDays: TLabel;
    sePastDays: TSpinEdit;
    lblForwardDeductions: TLabel;
    seForwardDeductions: TSpinEdit;
    lblForwardDays: TLabel;
    procedure cbPeriodChange(Sender: TObject);
    procedure sePastDaysChange(Sender: TObject);
  private
    procedure EnableDates(aValue: boolean);
    procedure PrepareControls(aIndex: integer);
    procedure SetPeriodType(const aValue: string);
    function GetPeriodType: string;
    procedure SetDatB(aValue: TDateTime);
    function GetDatB: TDateTime;
    procedure SetDatE(aValue: TDateTime);
    function GetDatE: TDateTime;
    function GetAsString: string;
    procedure SetAsString(const aValue: string);
  public
    procedure SetDefaults;
    procedure Loaded; override;

    property AsString: string read GetAsString write SetAsString;

    property PeriodType: string read GetPeriodType write SetPeriodType;
    property Dat_b: TDateTime read GetDatB write SetDatB;
    property Dat_e: TDateTime read GetDatE write SetDatE;
  end;

implementation

uses DateUtils;

{$R *.dfm}

{ TfrmDateRange }

function TfrmDateRange.GetDatB: TDateTime;
begin
  Result := DateOf(edDatB.DateTime) + TimeOf(edTimeB.DateTime);
end;

function TfrmDateRange.GetDatE: TDateTime;
begin
  Result := DateOf(edDatE.DateTime) + TimeOf(edTimeE.DateTime);
end;

procedure TfrmDateRange.Loaded;
begin
  inherited;
  SetDefaults;
end;

procedure TfrmDateRange.SetDatB(aValue: TDateTime);
begin
  edDatB.DateTime := aValue;
  edTimeB.DateTime := aValue;
end;

procedure TfrmDateRange.SetDatE(aValue: TDateTime);
begin
  edDatE.DateTime := aValue;
  edTimeE.DateTime := aValue;
end;

procedure TfrmDateRange.SetDefaults;
begin
  cbPeriod.ItemIndex := 0;
  Dat_e := EndOfTheMonth( Now );
  Dat_b := StartOfTheMonth( Now );
end;

procedure TfrmDateRange.cbPeriodChange(Sender: TObject);
begin
  case cbPeriod.ItemIndex of
    0: begin //Custom Period
      EnableDates( True );
      SetDefaults;
    end;
    1: begin //The past ## days
      Dat_b := Now - sePastDays.Value;
      Dat_e := Now;
    end;
    2: begin //The past ## days and Look forward for deductions syncing
      Dat_b := Now - sePastDays.Value;
      Dat_e := Now + seForwardDeductions.Value;
    end;
    3: begin //The Current Week
      Dat_b := StartOfTheWeek( Now );
      Dat_e := EndOfTheWeek( Now );
    end;
    4: begin //The Last Week
      Dat_b := StartOfTheWeek( StartOfTheWeek(Now) - 1 );
      Dat_e := EndOfTheWeek( StartOfTheWeek(Now) - 1 );
    end;
    5: begin //The Current Month
      Dat_b := StartOfTheMonth( Now );
      Dat_e := EndOfTheMonth( Now );
    end;
    6: begin //The Last Month
      Dat_b := StartOfTheMonth( StartOfTheMonth(Now) - 1 );
      Dat_e := EndOfTheMonth( StartOfTheMonth(Now) - 1 );
    end;
    7: begin //The Current Year
      Dat_b := StartOfTheYear( Now );
      Dat_e := EndOfTheYear( Now );
    end;
  end;
  PrepareControls( cbPeriod.ItemIndex );
end;

function TfrmDateRange.GetPeriodType: string;
begin
  Result := cbPeriod.Text;
end;

procedure TfrmDateRange.SetPeriodType(const aValue: string);
begin
  if cbPeriod.Items.IndexOf( aValue ) > -1 then
    cbPeriod.ItemIndex := cbPeriod.Items.IndexOf( aValue )
  else
    cbPeriod.ItemIndex := 0; //Custom Period

  if cbPeriod.ItemIndex = 1 then
    sePastDays.Value := Trunc(Dat_e - Dat_b);

  if cbPeriod.ItemIndex = 2 then
    sePastDays.Value := Trunc(Dat_e - Dat_b);

  cbPeriodChange( Self );  
end;

procedure TfrmDateRange.EnableDates(aValue: boolean);
begin
  edDatB.Enabled := aValue;
  edDatE.Enabled := aValue;
  edTimeB.Enabled := aValue;
  edTimeE.Enabled := aValue;
end;

function TfrmDateRange.GetAsString: string;
begin
  Result := cbPeriod.Text;

  if cbPeriod.ItemIndex = 0 then
    Result := Result + ': ' + DateToStr( Dat_b ) + ' - ' + DateToStr( Dat_e )
  else if cbPeriod.ItemIndex = 1 then
    Result := 'The past ' + IntToStr(sePastDays.Value) + ' days'
  else if cbPeriod.ItemIndex = 2 then
    Result := 'The past ' + IntToStr(sePastDays.Value) + ' days and look forward for ' + IntToStr(seForwardDeductions.Value) + ' for deductions syncing';
end;

procedure TfrmDateRange.SetAsString(const aValue: string);
var
  BeginDate, EndDate: TDateTime;
  DayAmount: integer;
  s: string;

  function GetNumber(aSource: string; aStartPos: integer): string;
  var
    i: integer;
    z: string;
  begin
    Result := '';
    z := Copy(aSource, aStartPos, Length(aSource));
    i := 0;
    while (i < Length(z)) do
    begin
      if z[i] in ['0'..'9'] then
        Result := Result + z[i]
      else if Result <> '' then // already got number
        z := '';

      Inc(i);
    end;
  end;
begin
  s := aValue;

  if Pos(':', aValue) > 0 then // Custom period
    s := cbPeriod.Items[0];

  if Pos('The past', aValue) = 1 then // The past ## days
    s := cbPeriod.Items[1];

  if Pos('look forward', aValue) > 1 then // The past ## days and look forward for deductions syncing
    s := cbPeriod.Items[2];

  PeriodType := s;

  if (cbPeriod.ItemIndex = 0) then
  begin
    s := Copy(aValue, Pos(':', aValue) + 1, length(aValue) );
    if Pos(' - ', s) > 0  then
    begin
      if TryStrToDate( Trim(Copy(s, 1, Pos(' - ', s) - 1 )), BeginDate) and
        TryStrToDate(Trim(Copy(s, Pos(' - ', s) + 3, length(s) )), EndDate) then
      begin
        Dat_b := BeginDate;
        Dat_e := EndDate;
      end
      else
        SetDefaults;
    end
    else
      SetDefaults;
  end;

  if (cbPeriod.ItemIndex >= 1) then
  begin
    s := GetNumber(aValue, 1);
    if TryStrToInt(s, DayAmount) then
      sePastDays.Value := DayAmount
    else
      SetDefaults;
  end;

  if (cbPeriod.ItemIndex = 2) then
  begin
    s := GetNumber(aValue, Pos('look forward', aValue));
    if TryStrToInt(s, DayAmount) then
      seForwardDeductions.Value := DayAmount
    else
      SetDefaults;
  end;
end;

procedure TfrmDateRange.PrepareControls(aIndex: integer);
  procedure ShowDayAmount(aShow: boolean; IncludeForwardDays: boolean = false);
  begin
    if (lblPastDays.Visible <> aShow) or (lblForwardDeductions.Visible <> IncludeForwardDays) then
    begin
      lblPastDays.Visible := aShow;
      sePastDays.Visible := aShow;
      lblForwardDeductions.Visible := IncludeForwardDays;
      lblForwardDays.Visible := IncludeForwardDays;
      seForwardDeductions.Visible := IncludeForwardDays;
      if aShow then
      begin
        lblPastDays.Left := lblFrom.Left + 3;
        lblPastDays.Top := lblFrom.Top + 10;

        if IncludeForwardDays then
        begin
          lblForwardDeductions.Left := lblPastDays.Left + 175;
          lblForwardDeductions.Top := lblPastDays.Top;
          lblForwardDays.Left := lblPastDays.Left + 315;
          lblForwardDays.Top := lblPastDays.Top;
        end;

        sePastDays.Left := lblFrom.Left + lblPastDays.Width + 15;
        sePastDays.Top := lblFrom.Top + 5;

        if IncludeForwardDays then
        begin
          seForwardDeductions.Left := sePastDays.Left + 155;
          seForwardDeductions.Top := sePastDays.Top;
        end;  

        edDatB.Visible := False;
        edDatE.Visible := False;
        edTimeB.Visible := False;
        edTimeE.Visible := False;
        lblFrom.Visible := False;
        lblTo.Visible := False;
      end
      else begin
        edDatB.Visible := True;
        edDatE.Visible := True;
        edTimeB.Visible := True;
        edTimeE.Visible := True;
        lblFrom.Visible := True;
        lblTo.Visible := True;
      end;
    end;
  end;
begin
  if aIndex = 0 then
    EnableDates(True);

  if aIndex = 1 then
    ShowDayAmount(True, False)
  else if aIndex = 2 then
    ShowDayAmount(True, True)
  else
    ShowDayAmount(False, False);

  if aIndex > 2 then
    EnableDates(False);
end;

procedure TfrmDateRange.sePastDaysChange(Sender: TObject);
begin
  cbPeriodChange(Sender);
end;

end.
