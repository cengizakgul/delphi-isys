unit EvoInfinityMapping;
                                                             
interface

uses DB, kbmMemTable, PayrollSync, gdyCommonLogger, SysUtils, EvoAPIConnection;

type
  TCustomFieldType = (cftInteger, cftBoolean, cftXSBoolean, cftWideStr, cftXSDateTime,
    cftXSDecimal, cftDouble);

  TMapField = record
    InFld: string; // Infinty Field
    EvoFld: string;    // Evo Field Name
    EvoTbl: string;     // Evo Table Name
    Caption: string; // Caption
    T: TCustomFieldType;   // Field Type
    S: integer;       // Size
  end;

const
  EeCodeField = 'EmployeeDataNbr';

  idxEmployeeID = 1;
  idxCompanyID = 2;
  idxVendorCompany_ID = 3;
  idxLastName = 4;
  idxFirstName = 5;
  idxMiddleName = 6;
  idxGender = 7;
  idxTobaccoUser = 8;
  idxEthnicity = 9;
  idxAddress1 = 10;
  idxAddress2 = 11;
  idxCity = 12;
  idxHomePhone = 13;
  idxState = 14;
  idxZip = 15;
  idxSSN = 16;
  idxBirthDate = 17;
  idxCostCenter1 = 18;
  idxCostCenter2 = 19;
  idxCostCenter3 = 20;
  idxCostCenter4 = 21;
  idxDefaultHours = 22;
  idxEmployeeStatus = 23;
  idxHireDate = 24;
  idxOrigHireDate = 25;
  idxPayFrequency = 26;
  idxSalary = 27;
  idxTaxForm = 28;
  idxWorkersCompCode = 29;
  idxTerminationDate = 30;
  idxBaseRate = 31;
  idxHighCompensation = 32;
  idxWorkEmail = 33;
  idxEEOClass = 34;
  idxVisaExpiration = 35;
  idxVisaType = 36;
  idxIsVeteran = 37;
  idxUserDefinedL15 = 38;
  idxEmployeeStatusACA = 39;

  // Employee Info mapping

  Mapping: array[1..39] of TMapField =
    (
      (InFld: 'PayrollEmployeeID'; EvoFld: 'CUSTOM_EMPLOYEE_NUMBER'; EvoTbl: 'EE'; Caption: 'Payroll Employee ID'; T: cftWideStr; S: 20),
      (InFld: 'CompanyID'; EvoFld: 'CUSTOM_COMPANY_NUMBER'; EvoTbl: 'CO'; Caption: 'Company ID'; T: cftInteger; S: 0),
      (InFld: 'VendorCompany_ID'; EvoFld: 'CUSTOM_COMPANY_NUMBER'; EvoTbl: 'CO'; Caption: 'Vendor Company ID'; T: cftWideStr; S: 50),
      (InFld: 'LastName'; EvoFld: 'LAST_NAME'; EvoTbl: 'CL_PERSON'; Caption: 'Last Name'; T: cftWideStr; S: 50),
      (InFld: 'FirstName'; EvoFld: 'FIRST_NAME'; EvoTbl: 'CL_PERSON'; Caption: 'First Name'; T: cftWideStr; S: 50),
      (InFld: 'MiddleName'; EvoFld: 'MIDDLE_INITIAL'; EvoTbl: 'CL_PERSON'; Caption: 'Middle Name'; T: cftWideStr; S: 50),
      (InFld: 'Gender'; EvoFld: 'GENDER'; EvoTbl: 'CL_PERSON'; Caption: 'Gender'; T: cftWideStr; S: 50),
      (InFld: 'TobaccoUser'; EvoFld: 'SMOKER'; EvoTbl: 'CL_PERSON'; Caption: 'Tobacco User'; T: cftBoolean; S: 0),
      (InFld: 'Ethnicity'; EvoFld: 'ETHNICITY'; EvoTbl: 'CL_PERSON'; Caption: 'Ethnicity'; T: cftWideStr; S: 50),
//      (InFld: 'MaritalStatus'; EvoFld: 'FEDERAL_MARITAL_STATUS'; EvoTbl: 'EE'; Caption: 'Marital Status'; T: cftWideStr; S: 50),
      (InFld: 'Address1'; EvoFld: 'ADDRESS1'; EvoTbl: 'CL_PERSON'; Caption: 'Address 1'; T: cftWideStr; S: 100),
      (InFld: 'Address2'; EvoFld: 'ADDRESS2'; EvoTbl: 'CL_PERSON'; Caption: 'Address 2'; T: cftWideStr; S: 100),
      (InFld: 'City'; EvoFld: 'CITY'; EvoTbl: 'CL_PERSON'; Caption: 'City'; T: cftWideStr; S: 50),
      (InFld: 'HomePhone'; EvoFld: 'PHONE1'; EvoTbl: 'CL_PERSON'; Caption: 'Home Phone'; T: cftWideStr; S: 20),
      (InFld: 'State'; EvoFld: 'STATE'; EvoTbl: 'CL_PERSON'; Caption: 'State'; T: cftWideStr; S: 20),
      (InFld: 'Zip'; EvoFld: 'ZIP_CODE'; EvoTbl: 'CL_PERSON'; Caption: 'Zip'; T: cftWideStr; S: 20),
      (InFld: 'SSN'; EvoFld: 'SOCIAL_SECURITY_NUMBER'; EvoTbl: 'CL_PERSON'; Caption: 'SSN'; T: cftWideStr; S: 11),
      (InFld: 'BirthDate'; EvoFld: 'BIRTH_DATE'; EvoTbl: 'CL_PERSON'; Caption: 'Birth Date'; T: cftXSDateTime; S: 0),
      (InFld: 'CostCenter1'; EvoFld: 'NAME'; EvoTbl: 'CO_DIVISION'; Caption: 'Cost Center 1'; T: cftWideStr; S: 50),
      (InFld: 'CostCenter2'; EvoFld: 'NAME'; EvoTbl: 'CO_BRANCH'; Caption: 'Cost Center 2'; T: cftWideStr; S: 50),
      (InFld: 'CostCenter3'; EvoFld: 'NAME'; EvoTbl: 'CO_DEPARTMENT'; Caption: 'Cost Center 3'; T: cftWideStr; S: 50),
      (InFld: 'CostCenter4'; EvoFld: 'NAME'; EvoTbl: 'CO_TEAM'; Caption: 'Cost Center 4'; T: cftWideStr; S: 50),
      (InFld: 'DefaultHours'; EvoFld: 'STANDARD_HOURS'; EvoTbl: 'EE'; Caption: 'Default Hours'; T: cftXSDecimal; S: 0),
      (InFld: 'EmployeeStatus'; EvoFld: 'POSITION_STATUS'; EvoTbl: 'EE_'; Caption: 'Employee Status'; T: cftWideStr; S: 50), // EvoTbl: 'EE_' makes it not to be set into update package automatically (by SetTableFields)
      (InFld: 'HireDate'; EvoFld: 'CURRENT_HIRE_DATE'; EvoTbl: 'EE'; Caption: 'Hire Date'; T: cftXSDateTime; S: 0),
      (InFld: 'OrigHireDate'; EvoFld: 'ORIGINAL_HIRE_DATE'; EvoTbl: 'EE'; Caption: 'Original Hire Date'; T: cftXSDateTime; S: 0),
      (InFld: 'PayFrequency'; EvoFld: 'PAY_FREQUENCY'; EvoTbl: 'EE'; Caption: 'Pay Frequency'; T: cftWideStr; S: 20),
      (InFld: 'Salary'; EvoFld: 'SALARY_AMOUNT'; EvoTbl: 'EE'; Caption: 'Salary'; T: cftXSDecimal; S: 0),
      (InFld: 'TaxForm'; EvoFld: 'W2_TYPE'; EvoTbl: 'EE'; Caption: 'Tax Form'; T: cftWideStr; S: 20),
      (InFld: 'WorkersCompCode'; EvoFld: 'WORKERS_COMP_CODE'; EvoTbl: 'CO_WORKERS_COMP'; Caption: 'Workers Comp Code'; T: cftWideStr; S: 20),
      (InFld: 'TerminationDate'; EvoFld: 'CURRENT_TERMINATION_DATE'; EvoTbl: 'EE'; Caption: 'Termination Date'; T: cftXSDateTime; S: 0),
      (InFld: 'BaseRate'; EvoFld: 'RATE_AMOUNT'; EvoTbl: 'EE_RATES'; Caption: 'Base Rate'; T: cftWideStr; S: 20),
      (InFld: 'HighCompensation'; EvoFld: 'HIGHLY_COMPENSATED'; EvoTbl: 'EE'; Caption: 'High Compensation'; T: cftWideStr; S: 20),
      (InFld: 'WorkEmail'; EvoFld: 'E_MAIL_ADDRESS'; EvoTbl: 'CL_PERSON'; Caption: 'Work Email'; T: cftWideStr; S: 100),
      (InFld: 'EEOClass'; EvoFld: 'SY_HR_EEO_NBR'; EvoTbl: 'EE'; Caption: 'EEO Class'; T: cftWideStr; S: 20),
      (InFld: 'VisaExpiration'; EvoFld: 'VISA_EXPIRATION_DATE'; EvoTbl: 'CL_PERSON'; Caption: 'Visa Expiration Date'; T: cftXSDateTime; S: 0),
      (InFld: 'VisaType'; EvoFld: 'VISA_TYPE'; EvoTbl: 'CL_PERSON'; Caption: 'Visa Type'; T: cftWideStr; S: 20),
      (InFld: 'IsVeteran'; EvoFld: 'VETERAN'; EvoTbl: 'CL_PERSON'; Caption: 'Is Veteran'; T: cftXSBoolean; S: 0),
      (InFld: 'UserDefinedLookup15'; EvoFld: 'CUSTOM_COMPANY_NUMBER'; EvoTbl: 'CO'; Caption: 'User Defined Lookup15'; T: cftWideStr; S: 50),
      (InFld: 'EmployeeStatus_ACA'; EvoFld: 'POSITION_STATUS'; EvoTbl: 'EE_'; Caption: 'Employee Status'; T: cftWideStr; S: 50) // EvoTbl: 'EE_' makes it not to be set into update package automatically (by SetTableFields)
    );

  //Deductions mapping
  idxDecutionCode = 1;
  idxPerPayEmployeeCost = 2;
  idxEffectiveDate = 3;
  idxExpirationDate = 4;
  idxDedEDCode = 5;
  idxPlanCode = 6;
  idxErEDCode = 7;
  idxErAmount = 8;
  idxModifiedDate = 9;
  idxCoverageAmount = 10;

  DeductionsMapping: array[1..10] of TMapField =
    (
      (InFld: 'DeductionCode'; EvoFld: 'CUSTOM_E_D_CODE_NUMBER'; EvoTbl: 'CL_E_DS'; Caption: 'Deduction Code'; T: cftWideStr; S: 20),
      (InFld: 'PerPayEmployeeCost'; EvoFld: 'AMOUNT'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Per Pay Employee Cost'; T: cftXSDecimal; S: 0),
      (InFld: 'EffectiveDate'; EvoFld: 'Effective_Start_Date'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: ''; T: cftXSDateTime; S: 0),
      (InFld: 'ExpirationDate'; EvoFld: 'Effective_End_Date'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Vendor Company ID'; T: cftXSDateTime; S: 0),
      (InFld: 'EdCode'; EvoFld: 'CUSTOM_E_D_CODE_NUMBER'; EvoTbl: 'CL_E_DS'; Caption: 'EdCode'; T: cftWideStr; S: 20),
      (InFld: 'CarrierPlanCode'; EvoFld: 'CUSTOM_E_D_CODE_NUMBER'; EvoTbl: 'CL_E_DS'; Caption: 'CarrierPlanCode'; T: cftWideStr; S: 20),
      (InFld: 'ErdCode'; EvoFld: 'CUSTOM_E_D_CODE_NUMBER'; EvoTbl: 'CL_E_DS'; Caption: 'ErdCode'; T: cftWideStr; S: 20),
      (InFld: 'PerPayEmployerCost'; EvoFld: 'AMOUNT'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Per Pay Employee Cost'; T: cftXSDecimal; S: 0),
      (InFld: 'ModifiedDate'; EvoFld: 'Effective_Start_Date'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: ''; T: cftXSDateTime; S: 0),
      (InFld: 'CoverageAmount'; EvoFld: 'AMOUNT'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Coverage Amount'; T: cftXSDecimal; S: 0)
    );

  //Compensations mapping
  idxRate = 1;
  idxRateEffectiveDate = 2;
  idxRateExpirationDate = 3;

  CompensationMapping: array[1..3] of TMapField =
    (
      (InFld: 'Rate'; EvoFld: 'RATE_AMOUNT'; EvoTbl: 'EE_RATES'; Caption: 'Rate'; T: cftXSDecimal; S: 20),
      (InFld: 'EffectiveDate'; EvoFld: 'Effective_Date'; EvoTbl: 'EE_RATES'; Caption: ''; T: cftXSDateTime; S: 0),
      (InFld: 'ExpirationDate'; EvoFld: 'Effective_Date'; EvoTbl: 'EE_RATES'; Caption: ''; T: cftXSDateTime; S: 0)
//      (InFld: 'RateType'; EvoFld: ''; EvoTbl: ''; Caption: 'RateType'; T: cftWideStr; S: 20)
    );

  //DirectDeposits mapping
  idxDirectDepositTransitNumber = 1;
  idxDirectDepositAccountNumber = 2;
  idxAccountType = 3;
  idxDDEDCode = 4;
  idxDDIsActive = 5;
  idxDepositAmount = 6;
  idxDdExpirationDate = 7;
  idxDepositPercent = 8;
  idxOverridePrenote = 9;
  idxDDEffectiveDate = 10;
  idxDDModifiedDate = 11;

  DirectDepositMapping: array[1..11] of TMapField =
    (
      (InFld: 'DirectDepositTransitNumber'; EvoFld: 'EE_ABA_NUMBER'; EvoTbl: 'EE_DIRECT_DEPOSIT'; Caption: 'Direct Deposit Transit Number'; T: cftWideStr; S: 20),
      (InFld: 'DirectDepositAccountNumber'; EvoFld: 'EE_BANK_ACCOUNT_NUMBER'; EvoTbl: 'EE_DIRECT_DEPOSIT'; Caption: 'Direct Deposit Account Number'; T: cftWideStr; S: 20),
      (InFld: 'AccountType'; EvoFld: 'EE_BANK_ACCOUNT_TYPE'; EvoTbl: 'EE_DIRECT_DEPOSIT'; Caption: 'Account Type'; T: cftWideStr; S: 20),
      (InFld: 'EDCode'; EvoFld: 'EDCode'; EvoTbl: 'EE_DIRECT_DEPOSIT'; Caption: 'EDCode'; T: cftWideStr; S: 20),
      (InFld: 'IsActive'; EvoFld: 'IsActive'; EvoTbl: 'EE_DIRECT_DEPOSIT'; Caption: 'IsActive'; T: cftBoolean; S: 0),
      (InFld: 'DepositAmount'; EvoFld: 'AMOUNT'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'DepositAmount'; T: cftXSDecimal; S: 0),
      (InFld: 'ExpirationDate'; EvoFld: 'EFFECTIVE_END_DATE'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Effective End Date'; T: cftXSDateTime; S: 0),
      (InFld: 'DepositPercent'; EvoFld: 'AMOUNT'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'DepositPercent'; T: cftXSDecimal; S: 0),
      (InFld: 'OverridePreNote'; EvoFld: 'OverridePreNote'; EvoTbl: 'EE_DIRECT_DEPOSIT'; Caption: 'OverridePreNote'; T: cftBoolean; S: 0),
      (InFld: 'EffectiveDate'; EvoFld: 'EFFECTIVE_END_DATE'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Effective End Date'; T: cftXSDateTime; S: 0),
      (InFld: 'ModifiedDate'; EvoFld: 'EFFECTIVE_END_DATE'; EvoTbl: 'EE_SCHEDULED_E_DS'; Caption: 'Effective End Date'; T: cftXSDateTime; S: 0)
    );

  //StateTaxes
  idxIncomeFilingState = 1;
  idxUnemploymentState = 2;
  idxStateAllowances = 3;
  idxFilingStatus = 4;
  idxStateAdditionalWithholding = 5;
  idxIsExempt = 6;
  idxPercentWithholding = 7;
  idxOverrideAmount = 8;
  idxOverridePercent = 9;
  idxCounty = 10;
  idxClaimAmount = 11;
  idxAdditionalPercentWithheld = 12;
  idxStateExpirationDate = 13;
  idxStateStartDate = 14;

  StateTaxMapping: array[1..14] of TMapField =
    (
      (InFld: 'IncomeFilingState'; EvoFld: 'STATE'; EvoTbl: 'EE_STATES'; Caption: 'Ee State'; T: cftWideStr; S: 2),
      (InFld: 'UnemploymentState'; EvoFld: 'SUI_STATE'; EvoTbl: 'EE_STATES'; Caption: 'Ee SUI State'; T: cftWideStr; S: 2),
      (InFld: 'Allowances'; EvoFld: 'State_Nbr_Withhold_Allow'; EvoTbl: 'EE_STATES'; Caption: 'Ee State Tax Allowances'; T: cftInteger; S: 0),
      (InFld: 'FilingStatus'; EvoFld: 'State_Marital_Status'; EvoTbl: 'EE_STATES'; Caption: 'Ee State Tax Marital Status'; T: cftWideStr; S: 20),
      (InFld: 'AdditionalWithholding'; EvoFld: 'OVERRIDE_STATE_TAX_VALUE'; EvoTbl: 'EE_STATES'; Caption: 'Override State Tax Value'; T: cftXSDecimal; S: 0),
      (InFld: 'IsExempt'; EvoFld: 'STATE_EXEMPT_EXCLUDE'; EvoTbl: 'EE_STATES'; Caption: 'Exempt'; T: cftBoolean; S: 0),
      (InFld: 'PercentWithholding'; EvoFld: 'OVERRIDE_STATE_TAX_VALUE'; EvoTbl: 'EE_STATES'; Caption: 'Percent Withholding'; T: cftXSDecimal; S: 0),
      (InFld: 'OverrideAmount'; EvoFld: 'OVERRIDE_STATE_TAX_VALUE'; EvoTbl: 'EE_STATES'; Caption: 'Override Amount'; T: cftXSDecimal; S: 0),
      (InFld: 'OverridePercent'; EvoFld: 'OVERRIDE_STATE_TAX_VALUE'; EvoTbl: 'EE_STATES'; Caption: 'Override Amount'; T: cftXSDecimal; S: 0),
      (InFld: 'County'; EvoFld: 'NOTES'; EvoTbl: 'EE_STATES'; Caption: 'County'; T: cftWideStr; S: 50),
      (InFld: 'ClaimAmount'; EvoFld: 'NOTES'; EvoTbl: 'EE_STATES'; Caption: 'Claim Amount'; T: cftWideStr; S: 20),
      (InFld: 'AdditionalPercentWithheld'; EvoFld: 'OVERRIDE_STATE_TAX_VALUE'; EvoTbl: 'EE_STATES'; Caption: 'Additional Percent Withheld'; T: cftXSDecimal; S: 0),
      (InFld: 'ExpirationDate'; EvoFld: 'ExpirationDate'; EvoTbl: 'EE_STATES'; Caption: 'Expiration Date'; T: cftXSDateTime; S: 0),
      (InFld: 'StartDate'; EvoFld: 'StartDate'; EvoTbl: 'EE_STATES'; Caption: 'Start Date'; T: cftXSDateTime; S: 0)
    );

  //FederalTaxes
  idxFedAllowances = 1;
  idxFedMaritialStatus = 2;
  idxAdditonalWithholding = 3;
  idxExempt = 4;
  idxFedOverrideAmount = 5;
  idxFedOverridePercent = 6;
  idxFedExpirationDate = 7;
  idxFedStartDate = 8;

  FederalTaxMapping: array[1..8] of TMapField =
    (
      (InFld: 'Allowances'; EvoFld: 'Number_Of_Dependents'; EvoTbl: 'EE'; Caption: 'Fed Tax Allowances'; T: cftInteger; S: 0),
      (InFld: 'MaritalStatus'; EvoFld: 'Federal_Marital_Status'; EvoTbl: 'EE'; Caption: 'Fed Tax Marital Status'; T: cftWideStr; S: 20),
      (InFld: 'AdditionalWithholding'; EvoFld: 'OVERRIDE_FED_TAX_VALUE'; EvoTbl: 'EE'; Caption: 'Override Fed Tax Value'; T: cftXSDecimal; S: 0),
      (InFld: 'Exempt'; EvoFld: 'EXEMPT_EXCLUDE_EE_FED'; EvoTbl: 'EE'; Caption: 'Exempt'; T: cftBoolean; S: 0),
      (InFld: 'OverrideAmount'; EvoFld: 'OVERRIDE_FED_TAX_VALUE'; EvoTbl: 'EE'; Caption: 'Override Amount'; T: cftXSDecimal; S: 0),
      (InFld: 'OverridePercent'; EvoFld: 'OVERRIDE_FED_TAX_VALUE'; EvoTbl: 'EE'; Caption: 'Override Amount'; T: cftXSDecimal; S: 0),
      (InFld: 'ExpirationDate'; EvoFld: 'ExpirationDate'; EvoTbl: 'EE'; Caption: 'Expiration Date'; T: cftXSDateTime; S: 0),
      (InFld: 'StartDate'; EvoFld: 'StartDate'; EvoTbl: 'EE'; Caption: 'Start Date'; T: cftXSDateTime; S: 0)
    );

type

  TEvoInfinityMapping = class
  private
    FEeID: integer;
    FEmployeeData: TkbmCustomMemTable;
    FEEsDs: TDataSource;
    FDeductionData: TkbmCustomMemTable;
    FCompensationData: TkbmCustomMemTable;
    FDirectDepositData: TkbmCustomMemTable;
    FFedTaxData: TkbmCustomMemTable;
    FStateTaxData: TkbmCustomMemTable;
    FMissedFields: string;

    FSyStates: TkbmCustomMemTable;
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;

    procedure CreateFields;
    procedure LoadSyStates;

    function GetIsReady: boolean;
    function GetRecordCount: integer;
    function GetVendorCompanyID: string;
    function GetCompanyID: string;
    function GetEeCode: string;
    function GetUserDefinedL15: string;
    function GetState: string;
    function GetSyStateNbr: integer;
    function GetSSN: string;
    function GetGender: string;
    function GetW2Type: string;
    function GetEEOClass: integer;
    function GetSmoker: string;
    function GetEthnicity: string;
    function GetVeteran: string;
    function GetMiddleName: string;
    function GetFirstName: string;
    function GetLastName: string;
    function GetAddress1: string;
    function GetAddress2: string;
    function GetCity: string;
//    function GetDisabled: string;
    function GetVisaType: string;
    function GetPositionStatus: string;
    function GetACAStatus: string;
    function GetPayFrequency: string;
    function GetHighlyCompensated: string;
    function GetEeName: string;
    function GetHomePhone: string;
    function GetEmployeeID: string;
    function GetDirDepBankAccountType: string;
    function GetCurrentTerminationCode: string;
    function GetM_Status: string;
    function GetFM_Status: string;

    function GetStringFieldValue(Index: integer): string;
    function GetIntegerFieldValue(Index: integer): integer;
    function GetDateTimeFieldValue(Index: integer): TDateTime;
    function GetFloatFieldValue(Index: integer): Double;
  public
    procedure LoadFromEeXML(EeXML: EmployeeData);

    constructor Create(logger: ICommonLogger; EvoAPI: IEvoAPIConnection);
    destructor Destroy; override;

    procedure First;
    function Eof: boolean;
    procedure Next;
    function IsEeSetupComplete: boolean;

    property IsReady: boolean read GetIsReady;
    property RecordCount: integer read GetRecordCount;

    property VendorCompany_ID: string read GetVendorCompanyID;
    property Company_ID: string read GetCompanyID;
    property UserDefinedL15: string read GetUserDefinedL15;

    property Social_Security_Number: string read GetSSN;
    property EeCode: string read GetEeCode;
    property EeName: string read GetEeName;
    property DirDepBankAccountType: string read GetDirDepBankAccountType;
    property TerminationCode: string read GetCurrentTerminationCode;
    property PositionStatus: string read GetPositionStatus;
    property ACAStatus: string read GetACAStatus;

    property StringFieldValue[Index: integer]: string read GetStringFieldValue;
    property IntegerFieldValue[Index: integer]: integer read GetIntegerFieldValue;
    property DateTimeFieldValue[Index: integer]: TDateTime read GetDateTimeFieldValue;
    property FloatFieldValue[Index: integer]: Double read GetFloatFieldValue;

    property Deductions: TkbmCustomMemTable read FDeductionData;
    property Compensations: TkbmCustomMemTable read FCompensationData;
    property DirectDeposits: TkbmCustomMemTable read FDirectDepositData;
    property FedTaxes: TkbmCustomMemTable read FFedTaxData;
    property StateTaxes: TkbmCustomMemTable read FStateTaxData;

    property MappedMaritalStatus: string read GetM_Status;
    property MappedFedMaritalStatus: string read GetFM_Status;

    // Evolution Mandatory fields that have not been populated in Infinity
    property MissedFields: string read FMissedFields;
  end;

implementation

uses common, TypInfo, XSBuiltIns, EvoInfinityHRConstAndProc, gdyRedir, FMTBcd,
  DateUtils, Math, Variants;

{ TEvoInfinityMapping }

constructor TEvoInfinityMapping.Create(logger: ICommonLogger; EvoAPI: IEvoAPIConnection);
begin
  FLogger := logger;
  FEeID := 0;
  FEmployeeData := TkbmCustomMemTable.Create(nil);
  FDeductionData := TkbmCustomMemTable.Create(nil);
  FCompensationData := TkbmCustomMemTable.Create(nil);
  FDirectDepositData := TkbmCustomMemTable.Create(nil);
  FFedTaxData := TkbmCustomMemTable.Create(nil);
  FStateTaxData := TkbmCustomMemTable.Create(nil);
  FMissedFields := '';

  FEEsDs := TDataSource.Create(nil);
  FEEsDs.DataSet := FEmployeeData;

  FSyStates := TkbmCustomMemTable.Create(nil);
  FEvoAPI := EvoAPI;
  CreateFields;
  LoadSyStates;
end;

procedure TEvoInfinityMapping.CreateFields;

  procedure CreateMapFields(MapArray: Array of TMapField; DS: TkbmCustomMemTable);
  var
    i: integer;
  begin
    for i := Low(MapArray) to High(MapArray) do
    if MapArray[i].EvoFld <> '' then
    case MapArray[i].T of
      cftInteger:
        CreateIntegerField( DS, MapArray[i].InFld, MapArray[i].Caption );
      cftWideStr:
        CreateStringField( DS, MapArray[i].InFld, MapArray[i].Caption, MapArray[i].S );
      cftXSDateTime:
        CreateDateTimeField( DS, MapArray[i].InFld, MapArray[i].Caption );
      cftDouble, cftXSDecimal:
        CreateFloatField( DS, MapArray[i].InFld, MapArray[i].Caption );
      cftBoolean, cftXSBoolean:
        CreateBooleanField( DS, MapArray[i].InFld, MapArray[i].Caption );
    end;
  end;

begin
  CreateIntegerField( FEmployeeData, EeCodeField, EeCodeField );
  CreateMapFields( Mapping, FEmployeeData );

  CreateIntegerField( FDeductionData, EeCodeField, EeCodeField );
  CreateMapFields( DeductionsMapping, FDeductionData );

  CreateIntegerField( FCompensationData, EeCodeField, EeCodeField );
  CreateStringField( FCompensationData, 'RateType', 'RateType', 20 );
  CreateFloatField( FCompensationData, 'PayPeriodAmount', 'PayPeriodAmount');
  CreateFloatField( FCompensationData, 'HourlyRate', 'HourlyRate');
  CreateMapFields( CompensationMapping, FCompensationData );

  CreateIntegerField( FDirectDepositData, EeCodeField, EeCodeField );
  CreateMapFields( DirectDepositMapping, FDirectDepositData );

  CreateIntegerField( FFedTaxData, EeCodeField, EeCodeField );
  CreateMapFields( FederalTaxMapping, FFedTaxData );

  CreateIntegerField( FStateTaxData, EeCodeField, EeCodeField );
  CreateMapFields( StateTaxMapping, FStateTaxData );
end;

destructor TEvoInfinityMapping.Destroy;
begin
  FreeAndNil( FEmployeeData );
  FreeAndNil( FDeductionData );
  FreeAndNil( FCompensationData );
  FreeAndNil( FDirectDepositData );
  FreeAndNil( FFedTaxData );
  FreeAndNil( FStateTaxData );
  FreeAndNil( FEEsDs );
  FreeAndNil( FSyStates );
  inherited;
end;

function TEvoInfinityMapping.Eof: boolean;
begin
  Result := FEmployeeData.Eof;
end;

procedure TEvoInfinityMapping.First;
begin
  FEmployeeData.First;
  FMissedFields := '';
end;

function TEvoInfinityMapping.GetEthnicity: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxEthnicity].InFld ).AsString));
  if Result = 'ASIAN' then
    Result := 'A'
  else if Result = 'AMERICAN INDIAN OR ALASKA NATIVE' then
    Result := 'I'
  else if Result = 'BLACK OR AFRICAN AMERICAN' then
    Result := 'G'
  else if Result = 'WHITE' then
    Result := 'C'
  else if Result = 'HISPANIC OR LATINO' then
    Result := 'H'
  else if Result = 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER' then
    Result := 'S'
  else if Result = 'TWO OR MORE RACES' then
    Result := 'M'
  else if Result = 'OTHER' then
    Result := 'O'
  else
    Result := 'P';
end;

function TEvoInfinityMapping.GetStringFieldValue(Index: integer): string;
begin
  case Index of
  idxCity:
    Result := GetCity;
  idxAddress2:
    Result := GetAddress2;
  idxAddress1:
    Result := GetAddress1;
  idxFirstName:
    Result := GetFirstName;
  idxLastName:
    Result := GetLastName;
  idxMiddleName:
    Result := GetMiddleName;
  idxGender:
    Result := GetGender;
  idxState:
    Result := GetState;
  idxSSN:
    Result := GetSSN;
  idxEthnicity:
    Result := GetEthnicity;
  idxTobaccoUser:
    Result := GetSmoker;
  idxIsVeteran:
    Result := GetVeteran;
  idxVisaType:
    Result := GetVisaType;
{  idxEmployeeStatus:
    Result := GetPositionStatus;}
  idxPayFrequency:
    Result := GetPayFrequency;
  idxHighCompensation:
    Result := GetHighlyCompensated;
  idxTaxForm:
    Result := GetW2Type;
  idxEmployeeID:
    Result := GetEmployeeID;
  idxHomePhone:
    Result := GetHomePhone;
  else
    Result := FEmployeeData.FieldByName(Mapping[Index].InFld).AsString;
  end;
end;

function TEvoInfinityMapping.GetGender: string;
begin
  Result := Trim(FEmployeeData.FieldByName( Mapping[idxGender].InFld ).AsString);
  if Result <> '' then
    Result := Result[1]
  else
    Result := 'U';
end;

function TEvoInfinityMapping.GetIsReady: boolean;
begin
  Result := (FEmployeeData.Active) and (FEmployeeData.RecordCount > 0);
end;

function TEvoInfinityMapping.GetSmoker: string;
begin
  if FEmployeeData.FieldByName( Mapping[idxTobaccoUser].InFld ).AsBoolean then
    Result := 'Y'
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetSSN: string;
begin
  Result := FEmployeeData.FieldByName( Mapping[idxSSN].InFld ).AsString;
  Result := Copy(Result, 1, 3) + '-' + Copy(Result, 4, 2) + '-' + Copy(Result, 6, 4);
end;

function TEvoInfinityMapping.GetState: string;
begin
  Result := Trim(FEmployeeData.FieldByName(Mapping[idxState].InFld).AsString);
  if Length(Result) > 2 then
  begin
    Result := ConvertNull( FSyStates.Lookup('NAME', Result, 'STATE'), Result);
    if Length(Result) > 2 then
      raise Exception.Create('State "' + Result + '" is not recognized!');
  end;
end;

function TEvoInfinityMapping.GetSyStateNbr: integer;
begin
  if Length(Trim(FEmployeeData.FieldByName(Mapping[idxState].InFld).AsString)) <> 2 then
    Result := ConvertNull( FSyStates.Lookup('NAME', Trim(FEmployeeData.FieldByName(Mapping[idxState].InFld).AsString), 'SY_STATES_NBR'), 4)
  else
    Result := ConvertNull( FSyStates.Lookup('STATE', Trim(FEmployeeData.FieldByName(Mapping[idxState].InFld).AsString), 'SY_STATES_NBR'), 4);
end;

procedure TEvoInfinityMapping.LoadFromEeXML(EeXML: EmployeeData);
var
  i, j: integer;

  procedure FillRecordValues(DS: TkbmCustomMemTable; Map: array of TMapField; XMLObj: TObject);
  var
    Obj: TObject;
    k: integer;
    s: string;
  begin
    try
      for k := Low(Map) to High(Map) do
      if Map[k].EvoFld <> '' then
      begin
        case Map[k].T of
        cftInteger:
          DS.FieldByName( Map[k].InFld ).AsInteger := GetOrdProp(XMLObj, Map[k].InFld);

        cftWideStr:
          DS.FieldByName( Map[k].InFld ).AsString := GetWideStrProp(XMLObj, Map[k].InFld);

        cftBoolean, cftXSBoolean:
        begin
          s := GetPropValue(XMLObj, Map[k].InFld, True);
          if s = 'False' then
            DS.FieldByName( Map[k].InFld ).AsBoolean := False
          else if s = 'True' then
            DS.FieldByName( Map[k].InFld ).AsBoolean := True;
        end;

        cftXSDateTime:
        begin
          Obj := GetObjectProp( XMLObj, Map[k].InFld );
          if Assigned( Obj ) and (Obj is TXSDateTime) then
            DS.FieldByName( Map[k].InFld ).AsDateTime := (Obj as TXSDateTime).AsUTCDateTime;
        end;

        cftXSDecimal:
        begin
          Obj := GetObjectProp( XMLObj, Map[k].InFld );
          if Assigned( Obj ) and (Obj is TXSDecimal) then
            DS.FieldByName( Map[k].InFld ).AsBCD := (Obj as TXSDecimal).AsBcd;
        end;

        cftDouble:
          DS.FieldByName( Map[k].InFld ).AsFloat := GetFloatProp(XMLObj, Map[k].InFld);
        end;
        FLogger.LogDebug(Map[k].InFld + ' = ' + DS.FieldByName( Map[k].InFld ).AsString);
      end;  
    except
      FLogger.PassthroughException;
    end;
  end;
begin
  if not FEmployeeData.Active then
    FEmployeeData.Open;
  if not FDeductionData.Active then
    FDeductionData.Open;
  if not FCompensationData.Active then
    FCompensationData.Open;
  if not FDirectDepositData.Active then
    FDirectDepositData.Open;
  if not FFedTaxData.Active then
    FFedTaxData.Open;
  if not FStateTaxData.Active then
    FStateTaxData.Open;

  FLogger.LogDebug('Start Load Employee Data from Infinity ->');
  for i := Low(EeXML.Employees) to High(EeXML.Employees) do
  try
    Inc(FEeID);
    FLogger.LogDebug('Employee ' + IntToStr(FEeID));
    FEmployeeData.Append;
    FEmployeeData.FieldByName(EeCodeField).AsInteger := FEeID;

    FillRecordValues( FEmployeeData, Mapping, EeXML.Employees[i] );

    FEmployeeData.Post;

    for j := Low(EeXML.Employees[i].Deductions) to High(EeXML.Employees[i].Deductions) do
    try
      FLogger.LogDebug('Employee Deduction ' + IntToStr(j + 1));
      FDeductionData.Append;
      FDeductionData.FieldByName( EeCodeField ).AsInteger := FEeID;

      FillRecordValues( FDeductionData, DeductionsMapping, EeXML.Employees[i].Deductions[j] );

      FDeductionData.Post;
    except
      FDeductionData.Cancel;
    end;

    for j := Low(EeXML.Employees[i].Compensations) to High(EeXML.Employees[i].Compensations) do
    try
      FLogger.LogDebug('Employee Compensation ' + IntToStr(j + 1));
      FCompensationData.Append;
      FCompensationData.FieldByName( EeCodeField ).AsInteger := FEeID;
      FCompensationData.FieldByName( 'RateType' ).AsString := EeXML.Employees[i].Compensations[j].RateType;
      FCompensationData.FieldByName( 'PayPeriodAmount' ).AsBCD := EeXML.Employees[i].Compensations[j].PayPeriodAmount.AsBcd;
      FCompensationData.FieldByName( 'HourlyRate' ).AsBCD := EeXML.Employees[i].Compensations[j].HourlyRate.AsBcd;

      FLogger.LogDebug('RateType = ' + FCompensationData.FieldByName( 'RateType' ).AsString);
      FLogger.LogDebug('PayPeriodAmount = ' + FCompensationData.FieldByName( 'PayPeriodAmount' ).AsString);
      FLogger.LogDebug('HourlyRate = ' + FCompensationData.FieldByName( 'HourlyRate' ).AsString);

      FillRecordValues( FCompensationData, CompensationMapping, EeXML.Employees[i].Compensations[j] );

      FCompensationData.Post;
    except
      FCompensationData.Cancel;
    end;

    for j := Low(EeXML.Employees[i].DirectDeposits) to High(EeXML.Employees[i].DirectDeposits) do
    try
      FLogger.LogDebug('Employee Direct Deposit ' + IntToStr(j + 1));
      FDirectDepositData.Append;
      FDirectDepositData.FieldByName( EeCodeField ).AsInteger := FEeID;

      FillRecordValues( FDirectDepositData, DirectDepositMapping, EeXML.Employees[i].DirectDeposits[j] );

      FDirectDepositData.Post;
    except
      FDirectDepositData.Cancel;
    end;

    for j := Low(EeXML.Employees[i].FederalTax) to High(EeXML.Employees[i].FederalTax) do
    try
      FLogger.LogDebug('Employee Federal Tax ' + IntToStr(j + 1));
      FFedTaxData.Append;
      FFedTaxData.FieldByName( EeCodeField ).AsInteger := FEeID;

      FillRecordValues( FFedTaxData, FederalTaxMapping, EeXML.Employees[i].FederalTax[j] );

      FFedTaxData.Post;
    except
      FFedTaxData.Cancel;
    end;

    for j := Low(EeXML.Employees[i].StateTax) to High(EeXML.Employees[i].StateTax) do
    try
      FLogger.LogDebug('Employee State Tax ' + IntToStr(j + 1));
      FStateTaxData.Append;
      FStateTaxData.FieldByName( EeCodeField ).AsInteger := FEeID;

      FillRecordValues( FStateTaxData, StateTaxMapping, EeXML.Employees[i].StateTax[j] );

      FStateTaxData.Post;
    except
      FStateTaxData.Cancel;
    end;
  except
    FEmployeeData.Cancel;
  end;
  FLogger.LogDebug('<- End Load Employee Data from Infinity');

  FEmployeeData.SortFields := Mapping[idxVendorCompany_ID].InFld;

  FDeductionData.MasterSource := FEEsDs;
  FDeductionData.MasterFields := EeCodeField;
  FDeductionData.DetailFields := EeCodeField;

  if not Assigned(FDeductionData.IndexByName['SORT']) then
    FDeductionData.AddIndex('SORT', 'EffectiveDate;ExpirationDate:DC', [ixCaseInsensitive]);
  FDeductionData.IndexName := 'SORT';
  FDeductionData.IndexDefs.Update;

  FCompensationData.MasterSource := FEEsDs;
  FCompensationData.MasterFields := EeCodeField;
  FCompensationData.DetailFields := EeCodeField;

  FDirectDepositData.MasterSource := FEEsDs;
  FDirectDepositData.MasterFields := EeCodeField;
  FDirectDepositData.DetailFields := EeCodeField;

  FFedTaxData.MasterSource := FEEsDs;
  FFedTaxData.MasterFields := EeCodeField;
  FFedTaxData.DetailFields := EeCodeField;

  FStateTaxData.MasterSource := FEEsDs;
  FStateTaxData.MasterFields := EeCodeField;
  FStateTaxData.DetailFields := EeCodeField;
end;

procedure TEvoInfinityMapping.Next;
begin
  FEmployeeData.Next;
  FMissedFields := '';
end;

function TEvoInfinityMapping.GetIntegerFieldValue(Index: integer): integer;
begin
  if Index = idxState then
    Result := GetSyStateNbr
  else if Index = idxEEOClass then
    Result := GetEEOClass
  else
    Result := FEmployeeData.FieldByName(Mapping[Index].InFld).AsInteger;
end;

function TEvoInfinityMapping.GetDateTimeFieldValue(
  Index: integer): TDateTime;
begin
  Result := FEmployeeData.FieldByName(Mapping[Index].InFld).AsDateTime;
end;

function TEvoInfinityMapping.GetFloatFieldValue(Index: integer): Double;
begin
  Result := FEmployeeData.FieldByName(Mapping[Index].InFld).AsFloat;
end;

function TEvoInfinityMapping.GetVeteran: string;
begin
  if FEmployeeData.FieldByName( Mapping[idxIsVeteran].InFld ).IsNull then
    Result := 'A'
  else if FEmployeeData.FieldByName( Mapping[idxIsVeteran].InFld ).AsBoolean then
    Result := 'Y'
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetVisaType: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxVisaType].InFld ).AsString));
  if Result = 'B1' then
    Result := 'A'
  else if Result = 'F1' then
    Result := 'B'
  else if Result = 'F2' then
    Result := 'C'
  else if Result = 'H1A' then
    Result := 'D'
  else if Result = 'H1B' then
    Result := 'E'
  else if Result = 'H1B RES' then
    Result := 'U'
  else if Result = 'H2A' then
    Result := 'F'
  else if Result = 'H2B' then
    Result := 'G'
  else if Result = 'H3' then
    Result := 'H'
  else if Result = 'H4' then
    Result := 'I'
  else if Result = 'J1' then
    Result := 'J'
  else if Result = 'J2' then
    Result := 'K'
  else if Result = 'L1' then
    Result := 'L'
  else if Result = 'L2' then
    Result := 'M'
  else if Result = 'L1-7' then
    Result := 'T'
  else if Result = 'O1' then
    Result := 'O'
  else if Result = 'R1' then
    Result := 'P'
  else if Result = 'R2' then
    Result := 'Q'
  else if Result = 'TC' then
    Result := 'R'
  else if Result = 'TN' then
    Result := 'S'
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetPositionStatus: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxEmployeeStatus].InFld ).AsString));
  if Result = 'FULL TIME' then
    Result := 'F'
  else if Result = 'FULL TIME TEMP' then
    Result := 'U'
  else if Result = 'PART TIME' then
    Result := 'P'
  else if Result = 'PART TIME TEMP' then
    Result := 'R'
  else if Result = 'HALF TIME' then
    Result := 'H'
  else if Result = 'SEASONAL' then
    Result := 'S'
  else if Result = 'STUDENT' then
    Result := 'T'
  else if Result = '1099' then
    Result := '1'
  else if Result = 'OTHER' then
    Result := 'O'
  else if (Result = 'LEAVE OF ABSENCE') or
          (Result = 'RETIRED') or
          (Result = 'TERMINATED') then
    Result := ''
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetPayFrequency: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxPayFrequency].InFld ).AsString));
  if (Result = '52') or (Result = '53') then //weekly
    Result := 'W'
  else if (Result = '26') or (Result = '22') or (Result = '27') then //bi-weekly
    Result := 'B'
  else if Result = '24' then //semi-monthly
    Result := 'S'
  else if Result = '12' then //monthly
    Result := 'M'
  else if Result = '4' then //quarterly
    Result := 'Q'
  else
    Result := 'D';
end;

function TEvoInfinityMapping.GetHighlyCompensated: string;
begin
  if FEmployeeData.FieldByName( Mapping[idxHighCompensation].InFld ).AsBoolean then
    Result := 'Y'
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetRecordCount: integer;
begin
  if (FEmployeeData.Active) then
    Result := FEmployeeData.RecordCount
  else
    Result := 0;  
end;

procedure TEvoInfinityMapping.LoadSyStates;
begin
  FSyStates := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'Sy_States.rwq');
end;

function TEvoInfinityMapping.GetEeName: string;
begin
  Result := FEmployeeData.FieldByName( Mapping[idxFirstName].InFld ).AsString + ' ' +
    FEmployeeData.FieldByName( Mapping[idxMiddleName].InFld ).AsString + ' ' +
    FEmployeeData.FieldByName( Mapping[idxLastName].InFld ).AsString + ', ' +
    FEmployeeData.FieldByName( Mapping[idxEmployeeID].InFld ).AsString;
end;

function TEvoInfinityMapping.GetW2Type: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxTaxForm].InFld ).AsString));
  if (Result = 'W-2') or (Result = 'W2') then
    Result := 'F'
  else if (Result = '1099') or (Result = '1099M') then
    Result := 'O'
  else if Result = 'W-2 & 1099' then
    Result := 'B'
  else if Result = 'PUERTO RICO' then
    Result := 'P'
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetEmployeeID: string;
begin
  Result := PadLeft(FEmployeeData.FieldByName( Mapping[idxEmployeeID].InFld ).AsString, ' ', 9);
end;

function TEvoInfinityMapping.GetDirDepBankAccountType: string;
begin
  Result := UpperCase(Trim(FDirectDepositData.FieldByName( 'AccountType' ).AsString));
  if Copy(Result, 1, 7) = 'SAVINGS' then
    Result := 'S'
  else if Copy(Result, 1, 8) = 'CHECKING' then
    Result := 'C'
  else
    Result := 'M';
end;

function TEvoInfinityMapping.GetCompanyID: string;
begin
  Result := FEmployeeData.FieldByName(Mapping[idxCompanyID].InFld).AsString
end;

function TEvoInfinityMapping.GetVendorCompanyID: string;
begin
  Result := FEmployeeData.FieldByName(Mapping[idxVendorCompany_ID].InFld).AsString
end;

function TEvoInfinityMapping.GetCurrentTerminationCode: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxEmployeeStatus].InFld ).AsString));
  if (Result = 'FULL TIME') or (Result = 'FULL TIME TEMP') or (Result = 'PART TIME') or
     (Result = 'PART TIME TEMP') or (Result = 'HALF TIME') or (Result = 'SEASONAL') or
     (Result = 'STUDENT') or (Result = '1099') or (Result = 'UNDEFINED') then
    Result := 'A'
  else if (Result = 'LEAVE OF ABSENCE') then
    Result := 'V'
  else if (Result = 'RETIRED') then
    Result := 'E'
  else if (Result = 'TERMINATED') then
    Result := 'M'
  else
    Result := 'N';
end;

function TEvoInfinityMapping.GetHomePhone: string;
var
  i: integer;
  s: string;
begin
  Result := '';
  s := FEmployeeData.FieldByName( Mapping[idxHomePhone].InFld ).AsString;
  if Length(s) >= 10 then
  begin
    i := 1;
    while (i <= Length(s)) or (i <= 10) do
    begin
      if Pos(s[i], '0123456789') > 0 then
        Result := Result + s[i];
      if (Length(Result) = 3) or (Length(Result) = 7) then
        Result := Result + '-';
      Inc(i);
    end;
  end;
end;

{function TEvoInfinityMapping.GetDisabled: string;
begin
  if FEmployeeData.FieldByName( Mapping[idxHandicapped].InFld ).AsBoolean then
    Result := 'Y'
  else
    Result := 'N';
end;}

function TEvoInfinityMapping.GetM_Status: string;
var
  lState: string;
  lClaimAmount: string;
  fClaimAmount: double;
  lCounty: string;
  lAllowances: integer;
begin
  lState := Trim(FStateTaxData.FieldByName('IncomeFilingState').AsString);
  if Length(lState) > 2 then
    lState := ConvertNull( FSyStates.Lookup('NAME', lState, 'STATE'), lState);

  lCounty := UpperCase(Trim(FStateTaxData.FieldByName('County').AsString));
  lAllowances := FStateTaxData.FieldByName('Allowances').AsInteger;  

  lClaimAmount := Trim(FStateTaxData.FieldByName('ClaimAmount').AsString);
  if lClaimAmount = '' then
    lClaimAmount := '0';

  if not TryStrToFloat(lClaimAmount, fClaimAmount) then
    fClaimAmount := 0;

  Result := Trim(UpperCase(FStateTaxData.FieldByName('FilingStatus').AsString));

  if ((lState = 'AK') or (lState = 'FL') or (lState = 'TN') or (lState = 'NH') or (lState = 'NV')
    or (lState = 'SD') or (lState = 'TX') or (lState = 'WA') or (lState = 'WY'))
    and ((Result = '') or (Result = 'N/A')) then
    Result := 'S'

  else if (lState = 'AL') and (Result = 'MS') then
    Result := 'A'

  else if (lState = 'AR') then
  begin
    if Result = 'S' then
      Result := 'A'
    else if Result = 'MS' then
      Result := 'B'
    else if Result = 'MJ' then
      Result := 'C'
    else if Result = 'H' then
      Result := 'D'
    else if Result = '0' then
      Result := 'S0'
    else Result := '';
  end

  else if (lState = 'AZ') and not ((Result = '00') or (Result = '01') or (Result = '02') or (Result = '03')
    or (Result = '04') or (Result = '05') or (Result = '06') or (Result = '07')) then
  begin
    Result := FormatFloat('0.00', FStateTaxData.FieldByName('PercentWithholding').AsFloat);
    if Result = '1.30' then // 1.3 percent
      Result := '01'
    else if Result = '1.80' then // 1.8 percent
      Result := '02'
    else if Result = '2.70' then // 2.7 percent
      Result := '03'
    else if Result = '3.60' then // 3.6 percent
      Result := '04'
    else if Result = '4.20' then // 4.2 percent
      Result := '05'
    else if Result = '5.10' then // 5.1 percent
      Result := '06'
    else if Result = '0.80' then // 0.8 percent
      Result := '07'
    else //Exempt
      Result := '00';
  end

  else if (lState = 'CA') and (Result = 'S') then
    Result := 'SM'
  else if (lState = 'CA') and (Result = 'M') and (lAllowances > 1) then
    Result := 'M2'

  else if ((lState = 'DC') or (lState = 'DE')) and (Result = 'MS') then
    Result := 'A'
  else if ((lState = 'DC') or (lState = 'DE')) and (Result = 'MJ') then
    Result := 'M'

  else if (lState = 'GA') then
  begin
    if (Result = 'S') and (lAllowances = 0) then
      Result := '0A'  //0A - Single-0 Exempt, used to be A1
    else if (Result = 'S') then
      Result := '1A'  //1A - Single-1 Exempt, used to be A
    else if (Result = 'MS') and (lAllowances >= 1) then
      Result := '1D'  //1D - Married Separate-1 Exempt, used to be B3
    else if (Result = 'MS') then
      Result := '0D'  //0D - Married Separate-0 Exempt, used to be B
    else if (Result = 'MJ2') and (lAllowances = 0) then
      Result := '0B'
    else if Result = 'MJ2' then
      Result := '1B'
    else if (Result = 'MJ1') and (lAllowances = 0) then
      Result := '0C'
    else if (Result = 'MJ1') and (lAllowances = 2) then
      Result := '2C'
    else if Result = 'MJ1' then
      Result := '1C'
    else if (Result = 'H') and (lAllowances = 0) then
      Result := '0E'  //0E - Head Of Household-0 Exempt, used to be D1
    else if Result = 'H' then
      Result := '1E'  //1E - Head Of Household-1 Exempt, used to be D
    else Result := '';
  end

  else if (lState = 'IA') then
  begin
    if lAllowances > 1 then
      Result := 'S2'
    else
      Result := 'S';
  end

  else if (lState = 'IN') then
  begin
    if (lAllowances >= 0) and (lAllowances <= 10) then
      Result := IntToStr(lAllowances)
    else
      Result := 'Allowances should be in range 0..10'
  end

  else if (lState = 'KY') then
  begin
    if (Result <> 'M') and (Result <> 'S') then
      Result := 'S';
  end

  else if (lState = 'LA') then
  begin
    if Result = 'S' then
      Result := '0'
    else if Result = 'M01' then
      Result := '1'
    else if Result = 'M' then
      Result := '2'
    else Result := '';
  end

  else if (lState = 'MA') then
  begin
    if Result <> 'H' then
      Result := '';

    if lAllowances >= 2 then
      Result := Result +  '2'
    else if lAllowances >= 1 then
      Result := Result + '1'
    else
      Result := Result + '0';
  end

  else if (lState = 'MD') then
  begin
    if Result = 'NM' then
      Result := '52'
    else if Result = 'M' then // Married
    begin
      if lCounty = 'ALLEGANY' then
        Result := '41'
      else if lCounty = 'ANNE ARUNDEL' then
        Result := '28'
      else if lCounty = 'BALTIMORE CITY' then
        Result := '42'
      else if lCounty = 'BALTIMORE COUNTY' then
        Result := '35'
      else if lCounty = 'CALVERT' then
        Result := '32'
      else if lCounty = 'CAROLINE' then
        Result := '30'
      else if lCounty = 'CARROLL' then
        Result := '43'
      else if lCounty = 'CECIL' then
        Result := '33'
      else if lCounty = 'CHARLES' then
        Result := '39'
      else if lCounty = 'DORCHESTER' then
        Result := '29'
      else if lCounty = 'FREDERICK' then
        Result := '40'
      else if lCounty = 'GARRETT' then
        Result := '31'
      else if lCounty = 'HARFORD' then
        Result := '45'
      else if lCounty = 'HOWARD' then
        Result := '48'
      else if lCounty = 'KENT' then
        Result := '36'
      else if lCounty = 'MONTGOMERY' then
        Result := '49'
      else if lCounty = 'PRINCE GEORGE' then
        Result := '50'
      else if lCounty = 'QUEEN ANNE' then
        Result := '38'
      else if lCounty = 'SOMERSET' then
        Result := '47'
      else if lCounty = 'ST. MARY' then
        Result := '44'
      else if lCounty = 'TALBOT' then
        Result := '27'
      else if lCounty = 'WASHINGTON' then
        Result := '34'
      else if lCounty = 'WICOMICO' then
        Result := '46'
      else if lCounty = 'WORCESTER' then
        Result := '26'
      else
        Result := 'County Not Found ';
    end
    else if Result = 'S' then begin // Single
      if lCounty = 'ALLEGANY' then
        Result := '1'
      else if lCounty = 'ANNE ARUNDEL' then
        Result := '2'
      else if lCounty = 'BALTIMORE CITY' then
        Result := '3'
      else if lCounty = 'BALTIMORE COUNTY' then
        Result := '4'
      else if lCounty = 'CALVERT' then
        Result := '5'
      else if lCounty = 'CAROLINE' then
        Result := '6'
      else if lCounty = 'CARROLL' then
        Result := '7'
      else if lCounty = 'CECIL' then
        Result := '8'
      else if lCounty = 'CHARLES' then
        Result := '24'
      else if lCounty = 'DORCHESTER' then
        Result := '9'
      else if lCounty = 'FREDERICK' then
        Result := '10'
      else if lCounty = 'GARRETT' then
        Result := '11'
      else if lCounty = 'HARFORD' then
        Result := '23'
      else if lCounty = 'HOWARD' then
        Result := '12'
      else if lCounty = 'KENT' then
        Result := '13'
      else if lCounty = 'MONTGOMERY' then
        Result := '14'
      else if lCounty = 'PRINCE GEORGE' then
        Result := '15'
      else if lCounty = 'QUEEN ANNE' then
        Result := '16'
      else if lCounty = 'SOMERSET' then
        Result := '25'
      else if lCounty = 'ST. MARY' then
        Result := '17'
      else if lCounty = 'TALBOT' then
        Result := '18'
      else if lCounty = 'WASHINGTON' then
        Result := '19'
      else if lCounty = 'WICOMICO' then
        Result := '20'
      else if lCounty = 'WORCESTER' then
        Result := '21'
      else
        Result := 'County Not Found ';
    end;
  end

  else if (lState = 'ME') then
  begin
    if (Result = 'MD') or (Result = 'NRS') then
      Result := 'S';
    if Result = 'NRD' then
      Result := 'M';
  end

  else if (lState = 'MI') then
    Result := 'SM'

  else if (lState = 'MN') and ((Result = '') or (Result = 'N/A')) then
    Result := 'MM'

  else if (lState = 'MO') and (Result = 'MS') then
    Result := 'M0'
  else if (lState = 'MO') and (Result = 'M') then
    Result := 'M1'
  else if (lState = 'MO') and (Result = 'H') then
  begin
    if (lClaimAmount = '0') or (lClaimAmount = '1') or (lClaimAmount = '2') or (lClaimAmount = '3') or (lClaimAmount = '4') then
      Result := 'H2';
  end

  else if (lState = 'MS') then
  begin
    if (Result = 'S') and (lClaimAmount = '0.00') then
      Result := 'S0';
    if (Result = 'MJ') then
      Result := 'M';

    if Result = 'MS' then
    begin
      if (fClaimAmount >= 500) and (fClaimAmount <= 12000) then
        Result := 'M' + Chr(Trunc(RoundTo(fClaimAmount / 500, 0)) - 1 + Ord('A'))
      else
        Result := 'ClaimAmount does not fit to mapping ('+ lClaimAmount +'), should be 500..12000 ';
    end;
  end

  else if (lState = 'NC') and (Result = 'H') then
    Result := 'HH'
  else if (lState = 'NC') and (Result = 'M') then
    Result := 'MW'

  else if (lState = 'NJ') then
  begin
    if (Result = 'MS') or (Result = 'S') then
      Result := 'A'
    else if (Result = 'MJ') or (Result = 'HH') or (Result = 'QW') then
      Result := 'B';  
  end

  else if (lState = 'OK') and ((Result = '') or (Result = 'N/A')) then
    Result := 'D'

  else if (lState = 'OR') and (Result = 'H') then
    Result := 'S3'

  else if (lState = 'PR') then
  begin
    if Result = 'MS' then
      Result := 'M'
    else if Result = 'MJ' then
      Result := 'M0' 
    else if Result = 'H' then
      Result := 'MH'
    else if Result = 'MSO' then
      Result := 'MS'
    else if Result = 'S' then
      Result := 'S'
    else Result := '';
  end

  else if ((lState = 'OH') or (lState = 'PA') or (lState = 'VA')) then
    Result := 'SM'

  else if (lState = 'VT') and ((Result = '') or (Result = 'N/A')) then
    Result := 'SM'

  else if (lState = 'WV') and (Result = 'MD') then
    Result := 'MM'

  else if (lState = 'WI') then
  begin
    if (Result = 'M') and (lAllowances = 0) then
      Result := 'M0'
    else if (Result = 'S') and (lAllowances = 0) then
      Result := 'S0';
  end

  else if (lState = 'SC') then
  begin
    if lAllowances >= 1 then
      Result := '1'
    else
      Result := '0';
  end;
end;

function TEvoInfinityMapping.GetEEOClass: integer;
var
  EEOCode: string;
begin
  EEOCode := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxEEOClass].InFld ).AsString));
  if EEOCode = '1' then
    Result := 11 //1.1
  else if (EEOCode <> '') and (EEOCode[1] in ['2'..'9']) then
    Result := StrToInt(EEOCode) + 1
  else
    Result := 1; //0
end;

function TEvoInfinityMapping.GetFM_Status: string;
begin
  Result := UpperCase(FFedTaxData.FieldByName('MaritalStatus').AsString);
  if (Result = 'NRA') or (Pos('MARRIED, BUT WITHHOL', UpperCase(Result)) > 0) then
    Result := 'S'
  else if Result = 'NRM' then
    Result := 'M'
  else
    Result := Copy(Result, 1, 1);    
end;

function TEvoInfinityMapping.GetUserDefinedL15: string;
begin
  Result := FEmployeeData.FieldByName(Mapping[idxUserDefinedL15].InFld).AsString
end;

function TEvoInfinityMapping.GetEeCode: string;
begin
  Result := FEmployeeData.FieldByName(Mapping[idxEmployeeID].InFld).AsString
end;

function TEvoInfinityMapping.GetMiddleName: string;
begin
  Result := Copy(Trim(FEmployeeData.FieldByName( Mapping[idxMiddleName].InFld ).AsString), 1, 1);
end;

function TEvoInfinityMapping.GetFirstName: string;
begin
  Result := Copy(Trim(FEmployeeData.FieldByName( Mapping[idxFirstName].InFld ).AsString), 1, 20);
end;

function TEvoInfinityMapping.GetLastName: string;
begin
  Result := Copy(Trim(FEmployeeData.FieldByName( Mapping[idxLastName].InFld ).AsString), 1, 30);
end;

function TEvoInfinityMapping.GetAddress1: string;
begin
  Result := Copy(Trim(FEmployeeData.FieldByName( Mapping[idxAddress1].InFld ).AsString), 1, 30);
end;

function TEvoInfinityMapping.GetAddress2: string;
begin
  Result := Copy(Trim(FEmployeeData.FieldByName( Mapping[idxAddress2].InFld ).AsString), 1, 30);
end;

function TEvoInfinityMapping.GetCity: string;
begin
  Result := Copy(Trim(FEmployeeData.FieldByName( Mapping[idxCity].InFld ).AsString), 1, 20);
end;


function TEvoInfinityMapping.IsEeSetupComplete: boolean;

  procedure CheckMandatoryField(idx: integer);
  begin
    if (Mapping[idx].T = cftWideStr) and (Trim(FEmployeeData.FieldByName(Mapping[idx].InFld).AsString) = '') then
//      (Mapping[idx].T = cftXSDateTime) and ((FEmployeeData.FieldByName(Mapping[idx].InFld).AsDateTime) < 100) then
    begin
      Result := False;
      FMissedFields := FMissedFields + Mapping[idx].Caption + ', ';
    end;
  end;

begin
  FMissedFields := '';
  Result := True;

  CheckMandatoryField( idxLastname );
  CheckMandatoryField( idxFirstname );
  CheckMandatoryField( idxGender );
  CheckMandatoryField( idxTobaccoUser );
  CheckMandatoryField( idxEthnicity );
  CheckMandatoryField( idxAddress1 );
  CheckMandatoryField( idxCity );
  CheckMandatoryField( idxState );
  CheckMandatoryField( idxZip );
  CheckMandatoryField( idxSSN );
  CheckMandatoryField( idxEmployeeStatus );
  CheckMandatoryField( idxPayFrequency );
  CheckMandatoryField( idxTaxForm );
//  CheckMandatoryField( idxBaseRate );
//  CheckMandatoryField( idxHighCompensation );
//  CheckMandatoryField( idxVisaType );
  CheckMandatoryField( idxIsVeteran );

  if StateTaxes.RecordCount = 0 then
  begin
    FMissedFields := FMissedFields + 'Employee State Taxes, ';
    Result := False;
  end;

  if FedTaxes.RecordCount = 0 then
  begin
    FMissedFields := FMissedFields + 'Employee Federal Taxes, ';
    Result := False;
  end;

  if Compensations.RecordCount = 0 then
  begin
    FMissedFields := FMissedFields + 'Employee Compensations, ';
    Result := False;
  end;

  if not Result and (FMissedFields <> '') then
    FMissedFields := '"' + GetEeName + '", employee setup is incomplete! The following fields must be filled: ' +
      Copy(FMissedFields, 1, Length(FMissedFields) - 2 );
end;

function TEvoInfinityMapping.GetACAStatus: string;
begin
  Result := UpperCase(Trim(FEmployeeData.FieldByName( Mapping[idxEmployeeStatusACA].InFld ).AsString));
  if (Result = 'FULL TIME') or (Result = 'FULL TIME TEMP') then
    Result := 'F'
  else if (Result = 'HALF TIME') or (Result = 'PART TIME') or (Result = 'PART TIME TEMP') then
    Result := 'P'
  else if Result = 'SEASONAL' then
    Result := 'S'
  else if Result = 'SEASONAL <120 DAYS' then
    Result := 'E'
  else if Result = 'VARIABLE' then
    Result := 'V'  
  else
    Result := 'N';
end;

end.
