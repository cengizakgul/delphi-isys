unit InfinityHRConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, PasswordEdit, EvoInfinityHRConstAndProc;

type
  TInfinityHRConnectionParamFrm = class(TFrame)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    SiteEdit: TEdit;
    PasswordEdit: TPasswordEdit;
    UserNameEdit: TEdit;
    cbSavePassword: TCheckBox;
    cbEnterprise: TCheckBox;
  private
    function GetParam: TInfinityHRConnectionParam;
    procedure SetParam(const Value: TInfinityHRConnectionParam);
  public
    property Param: TInfinityHRConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
  end;

implementation

{$R *.dfm}

{ TInfinityHRConnectionParamFrm }

function TInfinityHRConnectionParamFrm.GetParam: TInfinityHRConnectionParam;
begin
  Result.Username := UserNameEdit.Text;
  Result.Password := PasswordEdit.Password;
  Result.URL := SiteEdit.Text;
  Result.SavePassword := cbSavePassword.Checked;
  Result.Enterprise := cbEnterprise.Checked;
end;

function TInfinityHRConnectionParamFrm.IsValid: boolean;
begin
  Result := (trim(UserNameEdit.Text) <> '') and
            (trim(PasswordEdit.Password) <> '') and
            (trim(SiteEdit.Text) <> '');
end;

procedure TInfinityHRConnectionParamFrm.SetParam(const Value: TInfinityHRConnectionParam);
begin
  UserNameEdit.Text := Value.Username;
  PasswordEdit.Password := Value.Password;
  SiteEdit.Text := Value.URL;
  cbSavePassword.Checked := Value.SavePassword;
  cbEnterprise.Checked := Value.Enterprise;
end;

end.
