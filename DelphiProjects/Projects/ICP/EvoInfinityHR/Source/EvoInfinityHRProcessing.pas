unit EvoInfinityHRProcessing;

interface

uses common, EvoInfinityHRConstAndProc, gdycommonlogger, InfinityHRConnection,
  DB, kbmMemTable, evoApiconnection, XmlRpcTypes, EvoInfinityMapping,
  EeHolder, status, CheckSelectCommon, classes, ISZippingRoutines,
  EmployeeSelectCommon;

type
  TStatisticAmounts = record
    EeProcessed: integer;
    EeUpdated: integer;
    EeInserted: integer;
  end;

  TImportProcessing = class
  private
    FLogger: ICommonLogger;
    FStatus: IStatusProgress;
    FInfinityHRConnectionParam: TInfinityHRConnectionParam;
    FEvoAPIConnectionParam: TEvoAPIConnectionParam;
    FCurrentCompany: TEvoCompany;
    FCreatedSchedEDsNbr: TStrings; // contains CL_E_DS_NBR's of created Scheduled E/D's for currently imported employee


    FInfinityHR: IInfinityHRConnection;
    FEvoAPI: IEvoAPIConnection;

    FEmployeeData: TEvoInfinityMapping;
    FEvoEe: TEeHolder;
    EvoCoData: TEvoCoData;
    FElectedED: boolean; // "Sync elected Scheduled E/Ds only" option means sync the only scheduled E/Ds which have Amount <> 0
    FTransHRates: boolean;

    FSSNAccess: boolean;
    FFedMaritalStatus: string; // the current value of EE.FEDERAL_MARITAL_STATUS

    FStat: TStatisticAmounts;
    FCurrentEe: string;
    FUnableToSync: string;

    FDat_b: TDateTime;
    FDat_e: TDateTime;

    procedure InitImport;
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    function ApplyRecordChange(aFields: IRpcStruct; const aTableName, aType: string; aKey: integer): integer;
    function ApplyChangePacket(aChanges: IRpcArray): IRpcStruct;

    procedure PostEmployeeDataToEvo;
    function CheckForCompany: boolean;

    procedure SetOverrideAmount(var Fields: IRpcStruct; aDS: TDataset; const aValueFN, aTypeFN: string);
    function ProcessFederalTaxes(var Fields: IRpcStruct): boolean; // done or not
    procedure SetSchedEdAmountPercent(var Fields: IRpcStruct; Amt, Percent: double);
    function PassBySelPeriod(aEffectiveDate, aExpirationDate, aModifiedDate: TDatetime): boolean; overload;
    function PassBySelPeriod(aEffectiveDate, aExpirationDate: TDatetime): boolean; overload;
    function PassByStartAndExpirationDates(aStartDate, aExpirationDate: TDatetime): boolean;

    procedure SetSchedDefaultED(var aFields: IRpcStruct; ClEDsNbr: integer);
    procedure BuildSetEndDatePacket(var aChanges: IRpcArray; NewEndDate: TDateTime; Key: integer);
    procedure BuildInsertScheduledEDPacket(var aChanges: IRpcArray; Amount, Percent: double;
      Cl_E_ED_Nbr: integer; StartDate, EndDate: TDateTime; var Key: integer; aEeNbr, aDdNbr: integer);
    procedure BuildUpdateScheduledEDPacket(var aChanges: IRpcArray; Amount, Percent: double;
      StartDate, EndDate: TDateTime; Key, aDdNbr: integer);
    procedure ProcessDeduction(var Changes: IRpcArray; const EdCode: string;
      Amount, Percent: double; StartDate, EndDate, ModifiedDate: TDatetime; var Key: integer; aEeNbr: integer; Insert: boolean; aDdNbr: integer = -1);
    function AllowPercentage(const EdCode: string): boolean;

    procedure UpdateEE;
    procedure CreateEE;

    procedure PostScheduledEDs(aEeNbr: integer; Insert: boolean = True);
    procedure PostEeRates(aEeNbr: integer; Insert: boolean = True);
    procedure PostEeDirectDeposits(aEeNbr: integer; Insert: boolean = True);
    procedure PostEeStates(aEeNbr: integer; const HomeState: string; Insert: boolean = True);

    procedure AutoCreateEeLocals(aEeNbr: integer);
    procedure AutoCreateEeWorkShifts(aEeNbr: integer);
    procedure AutoCreateEeTOA(aEeNbr: integer);
    procedure AutoCreateScheduledEDs(aEeNbr: integer);
  public
    constructor Create(logger: ICommonLogger; EvoConn: TEvoAPIConnectionParam; InfinityConn: TInfinityHRConnectionParam; Status: IStatusProgress);
    destructor Destroy; override;

    procedure InfinityToEvo(dat_b, dat_e: TDateTime; ElectedED, TransHRates: boolean);
    function IncompleteEeSetupWarnings: string; 
    procedure EvoToInfinity;
    procedure LoadCheckStubs(SelectedChecks: TSelectedChecks);
    procedure LoadEmployeeW2s(SelectedEmployees: TSelectedEmployees; aTaxYear: integer);
  end;

implementation

uses SysUtils, gdyRedir, Variants, DateUtils, StrUtils;

{ TImportProcessing }

constructor TImportProcessing.Create(logger: ICommonLogger; EvoConn: TEvoAPIConnectionParam;
  InfinityConn: TInfinityHRConnectionParam; Status: IStatusProgress);
begin
  FLogger := logger;
  FStatus := Status;
  FSSNAccess := True;
  FEvoAPIConnectionParam := EvoConn;
  FInfinityHRConnectionParam := InfinityConn;
  FCreatedSchedEDsNbr := TStringList.Create;
end;

destructor TImportProcessing.Destroy;
begin
  FreeAndNil( FEmployeeData );
  FreeAndNil( FEvoEe );
  FreeAndNil( EvoCoData );
  FreeAndNil( FCreatedSchedEDsNbr );

  inherited;
end;

procedure TImportProcessing.EvoToInfinity;
begin
  //
end;

procedure TImportProcessing.InfinityToEvo(dat_b, dat_e: TDateTime; ElectedED, TransHRates: boolean);
begin
  FStatus.Clear;
  FElectedED := ElectedED;
  FTransHRates := TransHRates;
  FDat_b := dat_b;
  FDat_e := dat_e;

    FInfinityHR := CreateInfinityHRConnection( FLogger, FInfinityHRConnectionParam );

  FStatus.SetMessage('Import Employee Data', 'Connect to Evolution', '');
  FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);

  FStatus.SetMessage('', 'Load data from Evolution', '');
  FEmployeeData := TEvoInfinityMapping.Create( FLogger, FEvoAPI );
  FEvoEe := TEeHolder.Create( FLogger, FEvoAPI );
  EvoCoData := TEvoCoData.Create( FLogger, FEvoAPI );

  FStatus.SetMessage('', 'Connect to Infinity HR web service', '');
  FInfinityHR := CreateInfinityHRConnection( FLogger, FInfinityHRConnectionParam );
  try
    // Get Employee Data from Infinity HR Web Service
    FStatus.SetMessage('Load Employee Data from Infinity HR (' + DateTimeToStr(dat_b) + ' - ' + DateTimeToStr(dat_e) + ')', '', '');
    FInfinityHR.GetEmployeesByDateRange(dat_b, dat_e, FEmployeeData, FLogger, FInfinityHRConnectionParam.Enterprise);

    if FEmployeeData.IsReady then
    begin
      FStatus.SetMessage('Employee record count: ' + IntToStr(FEmployeeData.RecordCount), '', '');

      // Post Employee Data to Evo
      PostEmployeeDataToEvo;
    end
    else
      FStatus.SetMessage('No employees loaded', '', '');
  finally
    FStatus.SetMessage('Ready to Start', '', '');
    FInfinityHR := nil;
  end;
end;

function TImportProcessing.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  try
    Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
  except
    on e: exception do
    begin
      FLogger.LogError(aFileName, e.Message);
      raise Exception.Create('Query file: ' + aFileName + '. ' + e.Message);
    end
  end;
end;

procedure TImportProcessing.PostEmployeeDataToEvo;
begin
  FUnableToSync := '';
  if FEmployeeData.IsReady then
  begin
    FStatus.SetMessage('Send Employee Data to Evolution','','');
    InitImport;

    FEmployeeData.First;
    CheckForCompany;
    FStatus.SetProgressCount( FEmployeeData.RecordCount + 1);
    try
      while not FEmployeeData.Eof do
      begin
        try
          FCurrentEe := '(Employee: '+ FEmployeeData.EeName +')';
          if CheckForCompany and FSSNAccess then
          begin
            FCurrentEe := '(Company: '+ FCurrentCompany.Custom_Company_Number +', Employee: '+ FEmployeeData.EeName +')';
            FStatus.SetMessage('', 'Process Employee: ' + FEmployeeData.EeName, 'Search the employee in Evo');

            // get Ee_Nbr, if exists - go next, put error to the log otherwise
            if not FEvoEe.FindEeBySSN( FEmployeeData.Social_Security_Number, FEmployeeData.EeCode, FCurrentCompany.CO_NBR ) then
            begin
              // create a new Employee
              CreateEE;
              if not FEmployeeData.IsEeSetupComplete then
              begin
                FLogger.LogWarning('Incomplete setup! ' + FCurrentEe, FEmployeeData.MissedFields);
                FUnableToSync := FUnableToSync + FEmployeeData.MissedFields + #10#13 + #10#13;
              end;
            end
            else
              // update Eemployee
              UpdateEE;

            FStat.EeProcessed := FStat.EeProcessed + 1;
          end
          else
            FLogger.LogWarning('Employee has not been submitted! ' + FCurrentEe);
        except
          on E: Exception do
          begin
            if (Pos('T_BIU_EE_3', E.Message) > 0) and (Pos('RAISE_DUPLICATE_RECORD', E.Message) > 0) then
              FLogger.LogError('Error importing data!',
                Format('Company %s: EE Code %s doesn''t have SSN of %s. Please fix the mismatch.',
                  [FCurrentCompany.Custom_Company_Number, FEmployeeData.EeCode, FEmployeeData.Social_Security_Number]))
            else
              FLogger.LogError('Error importing data! ' + FCurrentEe, E.Message);
          end;
        end;
        FStatus.IncProgress;
        FEmployeeData.Next;
      end;
    finally
      FStatus.ClearProgress;
    end;  

    FStatus.SetMessage('Done. Processed: ' + IntToStr(FStat.EeProcessed) + ', Added: ' + IntToStr(FStat.EeInserted) + ', Updated: ' + IntToStr(FStat.EeUpdated), '', '');
  end
end;

procedure TImportProcessing.UpdateEE;
var
  Fields: IRpcStruct;
  
  procedure SetTableFields(const aTableName: string);
  var
    i: integer;
  begin
    for i := Low(Mapping) to High(Mapping) do
    if (Mapping[i].EvoTbl = aTableName) then
    begin
      if (i <> idxSSN) and (i <> idxEEOClass) and (i <> idxSalary) then
      case Mapping[i].T of
      cftWideStr, cftBoolean, cftXSBoolean:
        if FEvoEe.FieldByName(Mapping[i].EvoFld).AsString <> FEmployeeData.StringFieldValue[i] then
          Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.StringFieldValue[i] );
      cftInteger:
        if FEvoEe.FieldByName(Mapping[i].EvoFld).AsInteger <> FEmployeeData.IntegerFieldValue[i] then
          Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.IntegerFieldValue[i] );
      cftXSDateTime:
        if (FEmployeeData.DateTimeFieldValue[i] > 0) and (Trunc(FEvoEe.FieldByName(Mapping[i].EvoFld).AsDateTime) <> Trunc(FEmployeeData.DateTimeFieldValue[i])) then
          Fields.AddItemDateTime( Mapping[i].EvoFld, FEmployeeData.DateTimeFieldValue[i] );
      cftXSDecimal, cftDouble:
        if FEvoEe.FieldByName(Mapping[i].EvoFld).AsFloat <> FEmployeeData.FloatFieldValue[i] then
          Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.FloatFieldValue[i] );
      end;
    end;
  end;
begin
  FStatus.SetDetailMessage('Update employee');
  Fields := TRpcStruct.Create;
  // update CL_PERSON fields

  SetTableFields('CL_PERSON');

  // set Additional CL_PERSON fields
  if FEvoEe.FieldByName('RESIDENTIAL_STATE_NBR').AsInteger <> FEmployeeData.IntegerFieldValue[idxState] then
    Fields.AddItem('RESIDENTIAL_STATE_NBR', FEmployeeData.IntegerFieldValue[idxState] );

  // post CL_PERSON changes
  ApplyRecordChange( Fields, 'CL_PERSON', 'U', FEvoEE.CL_PERSON_NBR );

  // update EE fields
  Fields.Clear;

  SetTableFields('EE');

  // set Additional EE fields:

  // DBDT
  EvoCoData.LocateDBDTByCustomNumbers(
    FEmployeeData.StringFieldValue[idxCostCenter1],   // DivCode
    FEmployeeData.StringFieldValue[idxCostCenter2],   // BrCode
    FEmployeeData.StringFieldValue[idxCostCenter3],   // DepCode
    FEmployeeData.StringFieldValue[idxCostCenter4] ); // TmCode

  if EvoCoData.DivisionFound and
    (FEvoEe.FieldByName('CO_DIVISION_NBR').AsInteger <> EvoCoData.CO_DIVISION_NBR) then
      Fields.AddItem( 'CO_DIVISION_NBR', EvoCoData.CO_DIVISION_NBR );

  if EvoCoData.BranchFound and
    (FEvoEe.FieldByName('CO_BRANCH_NBR').AsInteger <> EvoCoData.CO_BRANCH_NBR) then
      Fields.AddItem( 'CO_BRANCH_NBR', EvoCoData.CO_BRANCH_NBR );

  if EvoCoData.DepartmentFound and
    (FEvoEe.FieldByName('CO_DEPARTMENT_NBR').AsInteger <> EvoCoData.CO_DEPARTMENT_NBR) then
      Fields.AddItem( 'CO_DEPARTMENT_NBR', EvoCoData.CO_DEPARTMENT_NBR );

  if EvoCoData.TeamFound and
    (FEvoEe.FieldByName('CO_TEAM_NBR').AsInteger <> EvoCoData.CO_TEAM_NBR) then
      Fields.AddItem( 'CO_TEAM_NBR', EvoCoData.CO_TEAM_NBR );

  // Workers Comp
  if EvoCoData.LocateWorkersComp(FEmployeeData.StringFieldValue[idxWorkersCompCode]) and
    (FEvoEe.FieldByName('CO_WORKERS_COMP_NBR').AsInteger <> EvoCoData.CO_WORKERS_COMP_NBR) then
      Fields.AddItem( 'CO_WORKERS_COMP_NBR', EvoCoData.CO_WORKERS_COMP_NBR );

  if not ProcessFederalTaxes(Fields) then
    FFedMaritalStatus := FEvoEe.FieldByName('FEDERAL_MARITAL_STATUS').AsString;

  Fields.AddItem('CURRENT_TERMINATION_CODE', FEmployeeData.TerminationCode );
  if FEmployeeData.TerminationCode = 'A' then
    // clear Current Termination Date
    Fields.AddItem('CURRENT_TERMINATION_DATE', '#NULL#');

  if FEmployeeData.PositionStatus <> '' then
    Fields.AddItem('POSITION_STATUS', FEmployeeData.PositionStatus);

  Fields.AddItem('ACA_STATUS', FEmployeeData.ACAStatus );

  if FEvoEe.FieldByName('SY_HR_EEO_NBR').AsInteger <> FEmployeeData.IntegerFieldValue[idxEEOClass] then
    Fields.AddItem( 'SY_HR_EEO_NBR', FEmployeeData.IntegerFieldValue[idxEEOClass] );

  // post EE changes
  ApplyRecordChange( Fields, 'EE', 'U', FEvoEe.EE_NBR );

  PostScheduledEDs(FEvoEe.EE_NBR, False);

  PostEeRates(FEvoEe.EE_NBR, False);

  PostEeDirectDeposits(FEvoEe.EE_NBR, False);

  PostEeStates(FEvoEe.EE_NBR, FEmployeeData.StringFieldValue[idxState], False);

  FStat.EeUpdated := FStat.EeUpdated + 1;
end;

function TImportProcessing.ApplyRecordChange(aFields: IRpcStruct;
  const aTableName, aType: string; aKey: integer): integer;
var
  Changes: IRpcArray;
  RowChange, EvoResult: IRpcStruct;
begin
  Result := -1;
  if aFields.Count > 0 then
  begin
    Result := aKey;

    Changes := TRpcArray.Create;
    RowChange := TRpcStruct.Create;
    EvoResult := TRpcStruct.Create;

    RowChange.AddItem('T', aType);
    RowChange.AddItem('D', aTableName);
    if aType = 'U' then
      RowChange.AddItem('K', aKey);

    RowChange.AddItem('F', aFields);

    Changes.AddItem( RowChange );

    FLogger.LogDebug('XML change packet', Changes.GetAsXML);
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );
    FLogger.LogDebug('applyDataChangePacket response', EvoResult.GetAsXML);

    if Result = -1 then
      if EvoResult.KeyExists('-1') then
      begin
        if EvoResult.Keys['-1'].IsInteger then
          Result := EvoResult.Keys['-1'].AsInteger
        else if EvoResult.Keys['-1'].IsString then
        begin
          if not TryStrToInt(EvoResult.Keys['-1'].AsString, Result) then
            raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
        end
        else
          raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
      end;
  end;
end;

procedure TImportProcessing.CreateEE;
var
  Fields: IRpcStruct;
  ClPersonNbr, EeNbr: integer;

  procedure SetTableFields(const aTableName: string);
  var
    i: integer;
  begin
    for i := Low(Mapping) to High(Mapping) do
    if (Mapping[i].EvoTbl = aTableName) then
    case Mapping[i].T of
    cftWideStr, cftBoolean, cftXSBoolean:
      if i = idxEEOClass then
        Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.IntegerFieldValue[i] )
      else
        Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.StringFieldValue[i] );
    cftInteger:
      if i = idxEmployeeID then
        Fields.AddItem( Mapping[i].EvoFld, PadLeft(IntToStr(FEmployeeData.IntegerFieldValue[i]), ' ', 9) )
      else
        Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.IntegerFieldValue[i] );
    cftXSDateTime:
      if (FEmployeeData.DateTimeFieldValue[i] > 0) then
        Fields.AddItemDateTime( Mapping[i].EvoFld, FEmployeeData.DateTimeFieldValue[i] );
    cftXSDecimal, cftDouble:
      if i <> idxSalary then
        Fields.AddItem( Mapping[i].EvoFld, FEmployeeData.FloatFieldValue[i] );
    end;
  end;
begin
  FStatus.SetDetailMessage('Create a new employee');
  Fields := TRpcStruct.Create;

  if Trim(FEmployeeData.StringFieldValue[idxEmployeeID]) = '' then
  begin
    FStatus.SetDetailMessage('Employee ID should have a value!');
    FLogger.LogWarning('Can''t create an employee: PayrollEmployeeID field should have a value!');
    exit;
  end;

  // set mapped CL_PERSON fields
  SetTableFields('CL_PERSON');

  // set Additional CL_PERSON fields
  Fields.AddItem('RESIDENTIAL_STATE_NBR', FEmployeeData.IntegerFieldValue[idxState] );

  // set mandaroty CL_PERSON fields that were not filled yet
  Fields.AddItem('EIN_OR_SOCIAL_SECURITY_NUMBER', 'S' );
  Fields.AddItem('VIETNAM_VETERAN', 'A' );
  Fields.AddItem('DISABLED_VETERAN', 'A' );
  Fields.AddItem('MILITARY_RESERVE', 'A' );
  Fields.AddItem('I9_ON_FILE', 'N' );
  Fields.AddItem('RELIABLE_CAR', 'Y' );

  // new mandatory fields in Orange
  Fields.AddItem('SERVICE_MEDAL_VETERAN', 'A' );
  Fields.AddItem('OTHER_PROTECTED_VETERAN', 'A' );
  Fields.AddItem('NATIVE_LANGUAGE', 'E' );

  // post CL_PERSON changes
  ClPersonNbr := FEvoEe.CL_PERSON_NBR;
  if ClPersonNbr > 0 then
    ApplyRecordChange( Fields, 'CL_PERSON', 'U', ClPersonNbr )
  else begin
    Fields.AddItem('CL_PERSON_NBR', -1);
    ClPersonNbr := ApplyRecordChange( Fields, 'CL_PERSON', 'I', -1 );
  end;

  if ClPersonNbr > -1 then
  begin
    Fields.Clear;

    Fields.AddItem('EE_NBR', -1);
    Fields.AddItem('CL_PERSON_NBR', ClPersonNbr);
    // set mapped EE fields
    SetTableFields('EE');

    // set Additional EE fields:

    // DBDT
    EvoCoData.LocateDBDTByCustomNumbers(
      FEmployeeData.StringFieldValue[idxCostCenter1],   // DivCode
      FEmployeeData.StringFieldValue[idxCostCenter2],   // BrCode
      FEmployeeData.StringFieldValue[idxCostCenter3],   // DepCode
      FEmployeeData.StringFieldValue[idxCostCenter4] ); // TmCode

    if EvoCoData.DivisionFound then
      Fields.AddItem( 'CO_DIVISION_NBR', EvoCoData.CO_DIVISION_NBR );

    if EvoCoData.BranchFound then
      Fields.AddItem( 'CO_BRANCH_NBR', EvoCoData.CO_BRANCH_NBR );

    if EvoCoData.DepartmentFound then
      Fields.AddItem( 'CO_DEPARTMENT_NBR', EvoCoData.CO_DEPARTMENT_NBR );

    if EvoCoData.TeamFound then
      Fields.AddItem( 'CO_TEAM_NBR', EvoCoData.CO_TEAM_NBR );

    // Workers Comp
    if EvoCoData.LocateWorkersComp(FEmployeeData.StringFieldValue[idxWorkersCompCode]) then
      Fields.AddItem( 'CO_WORKERS_COMP_NBR', EvoCoData.CO_WORKERS_COMP_NBR );

    if not ProcessFederalTaxes(Fields) then
    begin
      FFedMaritalStatus := 'S';
      Fields.AddItem('Federal_Marital_Status', 'S');
      Fields.AddItem('NUMBER_OF_DEPENDENTS', 0 );
      Fields.AddItem('OVERRIDE_FED_TAX_TYPE', 'N' );
      Fields.AddItem('EXEMPT_EXCLUDE_EE_FED', 'I' );
    end;

    // set mandaroty EE fields that were not filled yet
    Fields.AddItem('CO_NBR', FCurrentCompany.CO_NBR );
    Fields.AddItem('CURRENT_TERMINATION_CODE', FEmployeeData.TerminationCode );
    if FEmployeeData.PositionStatus = '' then
      Fields.AddItem('POSITION_STATUS', 'N' )
    else
      Fields.AddItem('POSITION_STATUS', FEmployeeData.PositionStatus);

    Fields.AddItem('BENEFITS_ENABLED', EvoCoData.Enable_Benefits );
    Fields.AddItem('TIME_OFF_ENABLED', EvoCoData.Enable_Time_Off );
    Fields.AddItem('SELFSERVE_ENABLED', EvoCoData.Enable_ESS );

    Fields.AddItem('NEW_HIRE_REPORT_SENT', 'P' );
    Fields.AddItem('TIPPED_DIRECTLY', 'N' );
    Fields.AddItem('DISTRIBUTE_TAXES', 'B' );
    Fields.AddItem('FLSA_EXEMPT', 'N' );
    Fields.AddItem('W2_PENSION', 'N' );
    Fields.AddItem('W2_DEFERRED_COMP', 'N' );
    Fields.AddItem('W2_DECEASED', 'N' );
    Fields.AddItem('W2_STATUTORY_EMPLOYEE', 'N' );
    Fields.AddItem('W2_LEGAL_REP', 'N' );
    Fields.AddItem('EXEMPT_EMPLOYEE_OASDI', 'N' );
    Fields.AddItem('EXEMPT_EMPLOYEE_MEDICARE', 'N' );
    Fields.AddItem('EXEMPT_EMPLOYER_OASDI', 'N' );
    Fields.AddItem('EXEMPT_EMPLOYER_MEDICARE', 'N' );
    Fields.AddItem('EXEMPT_EMPLOYER_FUI', 'N' );
    Fields.AddItem('OVERRIDE_FED_TAX_TYPE', 'N' );
    Fields.AddItem('BASE_RETURNS_ON_THIS_EE', 'N' );
    Fields.AddItem('COMPANY_OR_INDIVIDUAL_NAME', 'I' );
    Fields.AddItem('TAX_AMT_DETERMINED_1099R', 'N' );
    Fields.AddItem('TOTAL_DISTRIBUTION_1099R', 'N' );
    Fields.AddItem('PENSION_PLAN_1099R', 'N' );
    Fields.AddItem('MAKEUP_FICA_ON_CLEANUP_PR', 'N' );
    Fields.AddItem('GENERATE_SECOND_CHECK', 'N' );
    Fields.AddItem('HIGHLY_COMPENSATED', 'N' );
    Fields.AddItem('CORPORATE_OFFICER', 'N' );
    Fields.AddItem('ELIGIBLE_FOR_REHIRE', 'Y' );
    Fields.AddItem('WC_WAGE_LIMIT_FREQUENCY', 'P' );
    Fields.AddItem('HEALTHCARE_COVERAGE', 'N' );
    Fields.AddItem('EE_ENABLED', 'Y' );
    Fields.AddItem('AUTO_UPDATE_RATES', 'Y' );
    Fields.AddItem('PRINT_VOUCHER', 'Y' ); 

    Fields.AddItem('EXISTING_PATIENT', 'N' );
    Fields.AddItem('DEPENDENT_BENEFITS_AVAILABLE', 'N' );
    Fields.AddItem('LAST_QUAL_BENEFIT_EVENT', 'Z' );
    Fields.AddItem('W2_FORM_ON_FILE', 'N' );
    Fields.AddItem('W2', 'A' );

    Fields.AddItem('GOV_GARNISH_PRIOR_CHILD_SUPPT', 'N' );

    // new mandatory fields in Plymouth
    Fields.AddItem('EIC', 'N' ); //None
    Fields.AddItem('NEXT_PAY_FREQUENCY', 'W' ); //Weekly
    Fields.AddItem('ACA_STATUS', FEmployeeData.ACAStatus ); //N/A

    if (Copy(FEvoAPI.EvoVersion, 1, 2) >= '16') and (Copy(FEvoAPI.EvoVersion, 4, 2) >= '24') then // New EE Fields in Quechee Release
    begin
      Fields.AddItem('ACA_POLICY_ORIGIN', 'B');
      Fields.AddItem('ENABLE_ANALYTICS', 'Y');
      Fields.AddItem('BENEFITS_ELIGIBLE', 'N');
    end;

    if (Copy(FEvoAPI.EvoVersion, 1, 2) >= '16') and (Copy(FEvoAPI.EvoVersion, 4, 2) >= '35') then // New EE Fields in Quechee Release
    begin
      Fields.AddItem('DIRECT_DEPOSIT_ENABLED', EvoCoData.Enable_DD );
      Fields.AddItem('ACA_FORM_ON_FILE', 'N');
      Fields.AddItem('ACA_TYPE', 'N');
      Fields.AddItem('BASE_ACA_ON_THIS_EE', 'Y');
      Fields.AddItem('COMMENSURATE_WAGE', 'N');
    end;

    // post EE changes
    EeNbr := ApplyRecordChange( Fields, 'EE', 'I', -1 );
    FStat.EeInserted := FStat.EeInserted + 1;

    if EeNbr > -1 then
    begin
      FCreatedSchedEDsNbr.Clear;
      
      // create Scheduled E/Ds
      PostScheduledEDs( EeNbr );

      // create Rates
      PostEeRates( EeNbr );

      // create Direct Deposits
      PostEeDirectDeposits( EeNbr );

      // create States
      PostEeStates( EeNbr, FEmployeeData.StringFieldValue[idxState] );

      AutoCreateEeLocals( EeNbr);
      AutoCreateEeWorkShifts( EeNbr );
      AutoCreateEeTOA( EeNbr );
      AutoCreateScheduledEDs( EeNbr );
    end;
  end;
end;

procedure TImportProcessing.InitImport;
begin
  FStat.EeProcessed := 0;
  FStat.EeUpdated := 0;
  FStat.EeInserted := 0;
  FCurrentCompany.CL_NBR := -1;
  FCurrentCompany.CO_NBR := -1;
  FCurrentCompany.Custom_Company_Number := '';
end;

procedure TImportProcessing.PostScheduledEDs(aEeNbr: integer; Insert: boolean = True);
var
  DeductionCode, resKey: string;
  IhrStartDate, IhrEndDate, ModifiedDate: TDatetime;
  i, k: integer;
  Changes: IRpcArray;
  Res: IRpcStruct;
  Amt, Percentage: double;

begin
  i := 0;
  Changes := TRpcArray.Create;
  try
    FEmployeeData.Deductions.First;
    while not FEmployeeData.Deductions.Eof do
    begin
      DeductionCode := FEmployeeData.Deductions.FieldByName('EDCode').AsString;
      if (Trim(DeductionCode) = '') then
        DeductionCode := FEmployeeData.Deductions.FieldByName('CarrierPlanCode').AsString;

      Amt := FEmployeeData.Deductions.FieldByName('PerPayEmployeeCost').AsCurrency;
      if AllowPercentage(DeductionCode) then
        Percentage := FEmployeeData.Deductions.FieldByName('CoverageAmount').AsCurrency
      else
        Percentage := 0.0;
      IhrStartDate := FEmployeeData.Deductions.FieldByName('EffectiveDate').AsDateTime;
      IhrEndDate := FEmployeeData.Deductions.FieldByName('ExpirationDate').AsDateTime;
      ModifiedDate := FEmployeeData.Deductions.FieldByName('ModifiedDate').AsDateTime;

      ProcessDeduction(Changes, DeductionCode, Amt, Percentage, IhrStartDate, IhrEndDate, ModifiedDate, i, aEeNbr, Insert);

      if (Trim(FEmployeeData.Deductions.FieldByName('ERDCode').AsString) <> '') and
        (FEmployeeData.Deductions.FieldByName('ERDCode').AsString <> DeductionCode) then
      begin
        // process ERDCode - E/D Code for the Employer Cost of Benefits
        DeductionCode := FEmployeeData.Deductions.FieldByName('ERDCode').AsString;
        Amt := FEmployeeData.Deductions.FieldByName('PerPayEmployerCost').AsCurrency;
        ProcessDeduction(Changes, DeductionCode, Amt, 0, IhrStartDate, IhrEndDate, ModifiedDate, i, aEeNbr, Insert);
      end;

      FEmployeeData.Deductions.Next;
    end;

    if Changes.Count > 0 then
    begin
      Res := ApplyChangePacket(Changes);

      FEvoEe.SchedED.Filter := 'Ee_Scheduled_E_Ds_Nbr<0';
      FEvoEe.SchedED.Filtered := True;
      try
        FEvoEe.SchedED.First;
        while not FEvoEe.SchedED.Eof do
        begin
          resKey := FEvoEe.SchedED.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsString;
          if Res.KeyExists(resKey) then
          begin
            if Res.Keys[resKey].IsString then
            begin
              if not TryStrToInt(Res.Keys[resKey].AsString, k) then
                raise Exception.Create('applyDataChangePacket did not return a new internal key value!');
            end
            else if Res.Keys[resKey].IsInteger then
              k := Res.Keys[resKey].AsInteger
            else
              raise Exception.Create('applyDataChangePacket did not return a new internal key value!');

            FEvoEe.SchedED.Edit;
            FEvoEe.SchedED.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsInteger := k;
            FEvoEe.SchedED.Post;
          end;
          FEvoEe.SchedED.Next;
        end;
      finally
        FEvoEe.SchedED.Filtered := False;
        FEvoEe.SchedED.Filter := '';
      end;
    end;  
  finally
    Res := nil;
    Changes := nil;
  end;
end;

procedure TImportProcessing.PostEeDirectDeposits(aEeNbr: integer;
  Insert: boolean);
var
  Fields: IRpcStruct;
  AbaNumber, BankAccountNumber, EDCode: string;
  DoInsert: boolean;
  EeDDNbr: integer;
  EDCodeList: TStrings;
  StartDateList: array of TDateTime;
  EndDateList: array of TDateTime;
  EffEndDate: TDateTime;
  EffStartDate: TDateTime;
  ModifiedDate: TDateTime;
  Changes: IRpcArray;
  i: integer;

  procedure AddDates(aStartDate, aEndDate: TDateTime);
  begin
    SetLength(StartDateList, Length(StartDateList) + 1);
    SetLength(EndDateList, Length(EndDateList) + 1);
    StartDateList[High(StartDateList)] := aStartDate;
    if aEndDate < 100 then // open Expiration Date
      EndDateList[High(EndDateList)] := 9999999
    else
      EndDateList[High(EndDateList)] := aEndDate;
  end;

  function IsDuplicated(const aEDCode: string; aStartDate, aEndDate: TDateTime): boolean;
  var
    k: integer;
    eDate: TDateTime;
  begin
    Result := False;
    k := 0;

    if aEndDate < 100 then
      eDate := 9999999
    else
      eDate := aEndDate;

    while not Result and (k < EDCodeList.Count) do
    begin
      if EDCodeList[k] = EDCode then
      begin
        Assert((k+1) <= Length(StartDateList));
        Assert((k+1) <= Length(EndDateList));
        Result := not ((StartDateList[k + 1] >= eDate) or (EndDateList[k + 1] <= aStartDate));
      end;
      k := k + 1;
    end;
  end;
begin
  EDCodeList := TStringList.Create;
  SetLength(StartDateList, 0);
  SetLength(EndDateList, 0);
  try
    FEmployeeData.DirectDeposits.First;
    while not FEmployeeData.DirectDeposits.Eof do
    begin
      EffEndDate := FEmployeeData.DirectDeposits.FieldByName('ExpirationDate').AsDateTime;
      EffStartDate := FEmployeeData.DirectDeposits.FieldByName('EffectiveDate').AsDateTime;
      ModifiedDate := FEmployeeData.DirectDeposits.FieldByName('ModifiedDate').AsDateTime;
      if  {FEmployeeData.DirectDeposits.FieldByName('IsActive').AsBoolean and}
        PassBySelPeriod( EffStartDate, EffEndDate, ModifiedDate ) then
      begin
        EdCode := FEmployeeData.DirectDeposits.FieldByName('EDCode').AsString;

        if IsDuplicated(EDCode, EffStartDate, EffEndDate) then
        begin
          if (EffEndDate < 100) or (EffEndDate >= Now) then
          begin
            FStatus.SetDetailMessage('Duplicate direct deposit! E/D code "' + EdCode + '", ' + FCurrentEe);
            FLogger.LogWarning('Duplicate direct deposit! E/D code "' + EdCode + '", ' + FCurrentEe);
          end;
          // else just ignore this expired record
        end
        else begin
          if Trim(EdCode) <> '' then
          begin
            EDCodeList.Add( EDCode );
            AddDates( EffStartDate, EffEndDate );
            
            AbaNumber := Trim(FEmployeeData.DirectDeposits.FieldByName('DirectDepositTransitNumber').AsString);
            BankAccountNumber := Trim(FEmployeeData.DirectDeposits.FieldByName('DirectDepositAccountNumber').AsString);

            if not EvoCoData.LocateCLEDCode( EdCode ) then
            begin
              FStatus.SetDetailMessage('E/D code "' + EdCode + '" is not found in Client E/Ds! ' + FCurrentEe);
              FLogger.LogWarning('E/D code "' + EdCode + '" is not found in Client E/Ds!' + FCurrentEe);
            end
            else if (AbaNumber <> '') and (BankAccountNumber <> '') then
            begin
              Fields := TRpcStruct.Create;
              try
                DoInsert := Insert;
                if not DoInsert then
                  DoInsert := not FEvoEe.DirectDeposit.Locate('EE_NBR;EE_Aba_Number;Ee_Bank_Account_Number', VarArrayOf([aEeNbr, AbaNumber, BankAccountNumber]), []);

                if not DoInsert then
                  EeDDNbr := FEvoEe.DirectDeposit.FieldByName('Ee_Direct_Deposit_Nbr').AsInteger
                else
                  EeDDNbr := -1;

                if DoInsert then
                begin
                  FStatus.SetDetailMessage('Add direct deposit "' + AbaNumber + '"');
                  Fields.AddItem('EE_DIRECT_DEPOSIT_NBR', -1);
                  Fields.AddItem('EE_NBR', aEeNbr);
                  Fields.AddItem('EE_ABA_NUMBER', AbaNumber);
                  Fields.AddItem('EE_BANK_ACCOUNT_NUMBER', BankAccountNumber);
                  Fields.AddItem('EE_BANK_ACCOUNT_TYPE', FEmployeeData.DirDepBankAccountType);
                  Fields.AddItem('FORM_ON_FILE', 'N');
                  Fields.AddItem('ALLOW_HYPHENS', 'N');
                  if Copy(FEvoAPI.EvoVersion, 1, 2) >= '16' then // Quechee Release
                  begin
                    Fields.AddItem('SHOW_IN_EE_PORTAL', 'N');
                    if (Copy(FEvoAPI.EvoVersion, 4, 2) >= '35') then
                      Fields.AddItem('READ_ONLY_REMOTES', 'N');
                  end;

                  if FEmployeeData.DirectDeposits.FieldByName('OverridePreNote').AsBoolean then
                    Fields.AddItem('IN_PRENOTE', 'N')
                  else
                    Fields.AddItem('IN_PRENOTE', 'Y');

                  EeDDNbr := ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'I', -1 );
                end
                else begin
                  FStatus.SetDetailMessage('Update direct deposit "' + AbaNumber + '"');
                  Fields.AddItem('EE_BANK_ACCOUNT_TYPE', FEmployeeData.DirDepBankAccountType);

                  ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'U', EeDDNbr );
                end;

                Changes := TRpcArray.Create;
                i := 0;
                FStatus.SetDetailMessage('Update direct deposit in Scheduled E/D "' + EDCode + '"');
                ProcessDeduction(Changes, EDCode, FEmployeeData.DirectDeposits.FieldByName('DepositAmount').AsFloat,
                  FEmployeeData.DirectDeposits.FieldByName('DepositPercent').AsFloat,
                  EffStartDate,
                  EffEndDate,
                  ModifiedDate,
                  i,
                  aEeNbr,
                  Insert,
                  EeDDNbr
                );
                ApplyChangePacket(Changes);
              finally
                Fields := nil;
              end;
            end;
          end;
        end;
      end;
      FEmployeeData.DirectDeposits.Next;
    end;
  finally
    FreeAndNil(EdCodeList);
  end;
end;

procedure TImportProcessing.PostEeRates(aEeNbr: integer; Insert: boolean);
var
  Found: boolean;
  Rate: Double;
  RateType: string;

  Changes: IRpcArray;

  procedure BuildUpdateEESalaryPacket(var aChanges: IRpcArray; Amount: double);
  var
    RowChange: IRpcStruct;
    Fields: IRpcStruct;
  begin
    Fields := TRpcStruct.Create;
    RowChange := TRpcStruct.Create;

    if Abs(Amount) <= 0.01 then
    begin
      FStatus.SetDetailMessage('Clear Employee Salary Amount');
      Fields.AddItem('SALARY_AMOUNT', '#NULL#');
    end
    else begin
      FStatus.SetDetailMessage( 'Set Employee Salary Amount ' + FormatFloat('0.00', Amount));
      Fields.AddItem('SALARY_AMOUNT', Amount);
    end;

    RowChange.AddItem('T', 'U');
    RowChange.AddItem('K', aEeNbr);
    RowChange.AddItem('D', 'EE');
    RowChange.AddItem('F', Fields);

    aChanges.AddItem( RowChange );
  end;

  procedure BuildUpdatePrimaryRatePacket(var aChanges: IRpcArray; Amount: double; aInsert: boolean);
  var
    RowChange: IRpcStruct;
    Fields: IRpcStruct;
  begin
    Fields := TRpcStruct.Create;
    RowChange := TRpcStruct.Create;

    if Abs(Amount) <= 0.01 then
      FStatus.SetDetailMessage('Set Zero Primary Rate')
    else
      FStatus.SetDetailMessage( 'Set Primary Rate Amount ' + FormatFloat('0.00', Amount));

    if aInsert or (FEvoEe.Rates.RecordCount < 1) then
    begin // insert a Primary Rate
      Fields.AddItem('EE_NBR', aEeNbr);
      Fields.AddItem( 'EE_RATES_NBR', -1 );
      Fields.AddItem( 'RATE_NUMBER', 1 );
      Fields.AddItem( 'PRIMARY_RATE', 'Y' );
      Fields.AddItem( 'RATE_AMOUNT', Amount );

      RowChange.AddItem('T', 'I');
    end
    else begin
      FEvoEe.Rates.First;
      if Abs(FEvoEe.Rates.FieldByName('Rate_Amount').AsFloat - Amount) >= 0.0001 then
        Fields.AddItem( 'RATE_AMOUNT', Amount );

      RowChange.AddItem('T', 'U');
      RowChange.AddItem('K', FEvoEe.Rates.FieldByName('Ee_Rates_Nbr').AsInteger);
    end;

    if Fields.Count > 0 then
    begin
      RowChange.AddItem('F', Fields);
      RowChange.AddItem('D', 'EE_RATES');

      aChanges.AddItem( RowChange );
    end;

    if not aInsert and (FEvoEe.Rates.RecordCount > 1) then // there are a few Primary Rates (!!!)
    begin
      FEvoEe.Rates.First;
      FEvoEe.Rates.Next;
      while not FEvoEe.Rates.Eof do
      begin
        RowChange := TRpcStruct.Create;
        Fields := TRpcStruct.Create;

        Fields.AddItem( 'PRIMARY_RATE', 'N' );

        RowChange.AddItem('T', 'U');
        RowChange.AddItem('K', FEvoEe.Rates.FieldByName('Ee_Rates_Nbr').AsInteger);
        RowChange.AddItem('F', Fields);
        RowChange.AddItem('D', 'EE_RATES');
        aChanges.AddItem( RowChange );

        FEvoEe.Rates.Next;
      end;
    end;
  end;

begin
  Found := False;
  FEmployeeData.Compensations.First;
  while not FEmployeeData.Compensations.Eof and not Found do
  begin
    // update only the first record with Effective Date less or equal now
    Found := (FEmployeeData.Compensations.FieldByName('EffectiveDate').AsDateTime <= Now) and
      ( (FEmployeeData.Compensations.FieldByName('ExpirationDate').AsDateTime > Now) //Future Date
        or
        (FEmployeeData.Compensations.FieldByName('ExpirationDate').AsDateTime < 100) //Empty
      );

    if not Found then
      FEmployeeData.Compensations.Next;
  end;

  Changes := TRpcArray.Create;
  try
    if Found then
    begin
      Rate := FEmployeeData.Compensations.FieldByName('Rate').AsFloat;
      RateType := UpperCase( FEmployeeData.Compensations.FieldByName('RateType').AsString );

      if (RateType = 'SALARY') then
      begin
        BuildUpdateEESalaryPacket(Changes, FEmployeeData.Compensations.FieldByName('PayPeriodAmount').AsFloat);
        if FTransHRates then
          BuildUpdatePrimaryRatePacket(Changes, FEmployeeData.Compensations.FieldByName('HourlyRate').AsFloat, Insert)
        else
          BuildUpdatePrimaryRatePacket(Changes, 0, Insert);
      end;

      if (RateType = 'HOURLY') then
      begin
        BuildUpdateEESalaryPacket(Changes, 0);
        BuildUpdatePrimaryRatePacket(Changes, Rate, Insert)
      end;

      FStatus.SetDetailMessage('Apply Employee Rate Changes');
      ApplyChangePacket(Changes);
    end
    else // not found: no Salary/Hourly rate info in Compensations for import
      if Insert then // it's employee insert: put the salary from EmployeeInfo.Salary
      begin
        BuildUpdateEESalaryPacket(Changes, FEmployeeData.FloatFieldValue[idxSalary]);

        ApplyChangePacket(Changes);
      end;
  finally
    Changes := nil;
  end;
end;

function TImportProcessing.CheckForCompany: boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
  ReOpenCo, ReOpenCl: boolean;

  procedure ProcessCompanyID(const aCompanyID: string);
  begin
    Result := (Trim(aCompanyID) <> '') and (FCurrentCompany.Custom_Company_Number = aCompanyID);
    if (Trim(aCompanyID) <> '') and (FCurrentCompany.Custom_Company_Number <> aCompanyID) then
    begin
      FCurrentCompany.Custom_Company_Number := aCompanyID;
      FCurrentCompany.CO_NBR := -1;

      FStatus.SetMessage('', 'Open company: ' + FCurrentCompany.Custom_Company_Number, '');
      param := TRpcStruct.Create;
      Param.AddItem( 'CoNumber', FCurrentCompany.Custom_Company_Number );
      FClCoData := RunQuery('TmpCoQuery.rwq', Param);
      try
        if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
        begin
          FCurrentCompany.CO_NBR := FClCoData.FindField('CO_NBR').AsInteger;
          Result := True;
          if FCurrentCompany.CL_NBR <> FClCoData.FindField('CL_NBR').AsInteger then
          begin
            FCurrentCompany.CL_NBR := FClCoData.FindField('CL_NBR').AsInteger;
            ReOpenCl := True;
          end;
          ReOpenCo := True;
        end
        else
          FLogger.LogWarning('Company "' + FCurrentCompany.Custom_Company_Number + '" has not been found!')
      finally
        FreeAndNil( FClCoData );
      end;
    end;
  end;
begin
  ReOpenCo := False;
  ReOpenCl := False;
  Result := False;
  try
    if (FEmployeeData.VendorCompany_ID = '') and (FEmployeeData.Company_ID = '') and (FEmployeeData.UserDefinedL15 = '') then
      FLogger.LogWarning('All the Company ID fields (User Defined Lookup15, Vendor Company ID, Company ID) are blank!')
    else begin
      ProcessCompanyID(FEmployeeData.UserDefinedL15);
      if not Result then
        ProcessCompanyID(FEmployeeData.VendorCompany_ID);
      if not Result then
        ProcessCompanyID(FEmployeeData.Company_ID {+ 'S'});
    end;    

    if ReOpenCl then
    begin
      FStatus.SetDetailMessage('Open client database: CL_' + IntToStr(FCurrentCompany.CL_NBR));
      FEvoAPI.OpenClient(FCurrentCompany.CL_NBR);

      // check for SSN access
      FStatus.SetDetailMessage('Check for SSN access...');
      FSSNAccess := FEvoEe.CheckForSSNAccess( FCurrentCompany.CO_NBR );
      if not FSSNAccess then
        FStatus.SetMessage('Evo user "' +FEvoAPIConnectionParam.Username + '" does not have an access to Employee SSN!', '', '')
      else begin
        FStatus.SetDetailMessage('Load client data');
        EvoCoData.OpenClientTables;
      end;
      ReOpenCo := True;
    end;
    if ReOpenCo and FSSNAccess then
    begin
      FStatus.SetDetailMessage('Load company data');
      EvoCoData.OpenCompanyTables( FCurrentCompany.CO_NBR );
    end;
    Result := FCurrentCompany.CO_NBR > -1;
  except
    on e: Exception do
    begin
      FLogger.LogError('Find company error: ' + e.Message);
      FCurrentCompany.CL_NBR := -1;
      FCurrentCompany.CO_NBR := -1;
      FCurrentCompany.Custom_Company_Number := '';
      Result := False;
    end;
  end;
end;

procedure TImportProcessing.LoadCheckStubs(
  SelectedChecks: TSelectedChecks);
var
  PdfCheckLoader: TPdfCheckLoader;
  k, i, Total, Uploaded: integer;
  FailedPayStubs: TUploadFiles;
  paystub: TUploadFile;
begin
  FStatus.Clear;
  Total := 0;
  Uploaded := 0;
  SetLength(FailedPayStubs, 0);
  if Length(SelectedChecks) > 0 then
  try
    FStatus.SetMessage('Upload Pay Stubs', 'Upload Pay Stubs', 'Connect to Evolution');
    FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);
    FStatus.SetDetailMessage('Connect to Infinity HR');
    FInfinityHR := CreateInfinityHRConnection( FLogger, FInfinityHRConnectionParam );

    for i := Low(SelectedChecks) to High(SelectedChecks) do
    begin
      // load check stubs in PDF format from Evo
      FStatus.SetMessage('', SelectedChecks[i].Caption, 'Get check data');
      PdfCheckLoader := TPDFCheckLoader.Create(SelectedChecks[i], FEvoAPI, FLogger);
      with PdfCheckLoader do
      try
        k := 0;
        Total := Total + CheckRePrintTasks.RecordCount;
        FStatus.SetDetailMessage('Run checkstub report...');
        RunCheckReprintTasks;
        if CheckRePrintTasks.RecordCount = 0 then
          FStatus.SetMessage(SelectedChecks[i].Caption + ' -> No checks found!', '', '')
        else begin
          FStatus.SetProgressCount(CheckRePrintTasks.RecordCount);
          FStatus.SetMessage('', SelectedChecks[i].Caption, 'Get check data');
          FStatus.SetDetailMessage('Run checkstub report... Waiting for result...');
          repeat
            CheckRePrintTasks.First;
            while not CheckRePrintTasks.Eof do
            begin
              if not IsTaskRun then
              begin
                k := 0;
                DeleteTaskData;
                FLogger.LogDebug('Delete check print task', 'Check #: ' + CheckRePrintTasks.FieldByName('Payment_Serial_Number').AsString);
              end
              else if IsTaskReady then
              try
                k := 0;
                FStatus.SetDetailMessage('Send paystub file to Infinity HR. Check Number: ' + CheckRePrintTasks.FieldByName('Payment_Serial_Number').AsString);
                // upload pay stubs to Infinity HR

                paystub.FileContent := '';
                paystub := GetPaystubData;
                try
                  if paystub.FileContent <> '' then
                  begin
                    FInfinityHR.UpsertEmployeePayStubs(paystub, FLogger, FInfinityHRConnectionParam.Enterprise);
                    Uploaded := Uploaded + 1;
                  end
                  else
                    FLogger.LogWarning('Evo returned empty paystub', 'Employee SSN: ' + paystub.SSN + ', Check #: ' + paystub.CheckNumber);
                except
                  SetLength(FailedPayStubs, Succ(Length(FailedPayStubs)));
                  FailedPayStubs[Length(FailedPayStubs)-1] := paystub;
                end;
                FStatus.IncProgress;
              finally
                DeleteTaskData;
              end;
              CheckRePrintTasks.Next;
            end;
            k := k + 1;
          until (CheckRePrintTasks.RecordCount = 0) or (k = 500);
        end;
      finally
        FreeAndNil(PdfCheckLoader);
        FStatus.ClearProgress;
      end;
    end;

    // the second attempt to upload the paystubs that were failed to upload the first time
    for i := 0 to Length(FailedPayStubs) - 1 do
    try
      FInfinityHR.UpsertEmployeePayStubs(FailedPayStubs[i], FLogger, FInfinityHRConnectionParam.Enterprise);
      Uploaded := Uploaded + 1;
    except
      on E: Exception do
        FLogger.LogError('Error uploading paystub! Check Number: ' + FailedPayStubs[i].CheckNumber, e.Message);
    end;
  finally
    FInfinityHR := nil;
    if Total = 0 then
      FStatus.SetMessage('No paystubs uploaded', '', '')
    else
      FStatus.SetMessage('Successfully uploaded ' + IntToStr(Uploaded) + ' of ' + IntToStr(Total) + ' paystubs', '', '');
    if Uploaded < Total then
      FStatus.SetMessage('Please, see log for errors!', '', '');
    FStatus.SetMessage('Ready to Start', '', '');
  end;
end;

procedure TImportProcessing.PostEeStates(aEeNbr: integer; const HomeState: string; Insert: boolean);
var
  Fields: IRpcStruct;
  DoInsert: boolean;
  CoStatesNbr, SuiApplyCoStatesNbr, HomeTaxEeStatesNbr: integer;
  State, MappedMaritalStatus: string;
  ProcessedStates: TStrings;
begin
  ProcessedStates := TStringList.Create;
  Fields := TRpcStruct.Create;
  HomeTaxEeStatesNbr := -1;
  FEmployeeData.StateTaxes.First;
  try
    while not FEmployeeData.StateTaxes.Eof do
    begin
      if PassByStartAndExpirationDates(FEmployeeData.StateTaxes.FieldByName('StartDate').AsDateTime, FEmployeeData.StateTaxes.FieldByName('ExpirationDate').AsDateTime) then
      begin
        State := FEmployeeData.StateTaxes.FieldByName('IncomeFilingState').AsString;
        if ProcessedStates.IndexOf(State) > -1 then
          FLogger.LogWarning('Tax State ' + State + ' has already been processed! ' + FCurrentEe)
        else
        begin
          MappedMaritalStatus := FEmployeeData.MappedMaritalStatus;
          if not EvoCoData.LocateCoState( State ) then
            FLogger.LogWarning('Tax State ' + State + ' was not found in Co States table ' + FCurrentEe)
          else if not EvoCoData.LocateSyStateMaritalStatus(State, MappedMaritalStatus) then
            FLogger.LogWarning('State Marital Status was not found (State: '+State+', Mapped Status: '+FEmployeeData.MappedMaritalStatus+', Filing Status: '+FEmployeeData.StateTaxes.FieldByName('FilingStatus').AsString+') ' + FCurrentEe)
          else begin
            CoStatesNbr := EvoCoData.CO_STATES_NBR;

            SuiApplyCoStatesNbr := -1;
            if Trim(FEmployeeData.StateTaxes.FieldByName('UnemploymentState').AsString) <> '' then
            begin
              if not EvoCoData.LocateCoState( FEmployeeData.StateTaxes.FieldByName('UnemploymentState').AsString ) then
                FLogger.LogWarning('Sui Tax State ' + FEmployeeData.StateTaxes.FieldByName('UnemploymentState').AsString + ' was not found in Co States table ' + FCurrentEe)
              else
                SuiApplyCoStatesNbr := EvoCoData.CO_STATES_NBR;
            end;

            Fields.Clear;

            Fields.AddItem('EE_NBR', aEeNbr);
            Fields.AddItem('CO_STATES_NBR', CoStatesNbr );
            Fields.AddItem('SDI_APPLY_CO_STATES_NBR', CoStatesNbr);
            if SuiApplyCoStatesNbr > -1 then
              Fields.AddItem('SUI_APPLY_CO_STATES_NBR', SuiApplyCoStatesNbr);
            Fields.AddItem('STATE_MARITAL_STATUS', MappedMaritalStatus);
            if FFedMaritalStatus <> '' then
              Fields.AddItem('IMPORTED_MARITAL_STATUS', FFedMaritalStatus);
            Fields.AddItem('SY_STATE_MARITAL_STATUS_NBR', EvoCoData.SY_STATE_MARITAL_STATUS);
            Fields.AddItem('State_Number_Withholding_Allow', FEmployeeData.StateTaxes.FieldByName('Allowances').AsInteger);

            SetOverrideAmount(Fields, FEmployeeData.StateTaxes, 'OVERRIDE_STATE_TAX_VALUE', 'OVERRIDE_STATE_TAX_TYPE');

            // Employee - States - Overrides (State Exempt or Block)
            if (FEmployeeData.StateTaxes.FieldByName('IsExempt').AsBoolean) or
              ((State = 'KY') and (Trim(UpperCase(FEmployeeData.StateTaxes.FieldByName('FilingStatus').AsString)) = 'E')) then
              Fields.AddItem('STATE_EXEMPT_EXCLUDE', 'X' ) // Block
            else
              Fields.AddItem('STATE_EXEMPT_EXCLUDE', 'I' ); // Include

            DoInsert := Insert;
            if not DoInsert then
              DoInsert := not FEvoEe.States.Locate('CO_STATES_NBR', CoStatesNbr, []);

            if DoInsert then
            begin
              FStatus.SetDetailMessage('Add state "' + State + '"');
              Fields.AddItem('EE_STATES_NBR', -1 );
              Fields.AddItem('RECIPROCAL_METHOD', 'N' );
              Fields.AddItem('CALCULATE_TAXABLE_WAGES_1099', 'N' );
              Fields.AddItem('SALARY_TYPE', 'N' );
              Fields.AddItem('EE_SDI_EXEMPT_EXCLUDE', 'I' );
              Fields.AddItem('EE_SUI_EXEMPT_EXCLUDE', 'I' );
              Fields.AddItem('ER_SDI_EXEMPT_EXCLUDE', 'I' );
              Fields.AddItem('ER_SUI_EXEMPT_EXCLUDE', 'I' );

              if (HomeTaxEeStatesNbr = -1) or (HomeState = State) then
                HomeTaxEeStatesNbr := ApplyRecordChange( Fields, 'EE_STATES', 'I', -1 );
            end
            else begin
              FStatus.SetDetailMessage('Update state "' + State + '"');

              ApplyRecordChange( Fields, 'EE_STATES', 'U', FEvoEe.States.FieldByName('EE_STATES_NBR').AsInteger );

              if (HomeTaxEeStatesNbr = -1) or (HomeState = State) then
                HomeTaxEeStatesNbr := FEvoEe.States.FieldByName('EE_STATES_NBR').AsInteger;
            end;
          end;
          ProcessedStates.Add(State);
        end;
      end;
      FEmployeeData.StateTaxes.Next;
    end;
  finally
    FreeAndNil(ProcessedStates);
  end;

  if HomeTaxEeStatesNbr > -1 then
  begin
    Fields.Clear;
    FStatus.SetDetailMessage('Set employee Home State');
    Fields.AddItem('HOME_TAX_EE_STATES_NBR', HomeTaxEeStatesNbr);
    ApplyRecordChange( Fields, 'EE', 'U', aEeNbr );
  end;
  Fields := nil;
end;

procedure TImportProcessing.SetOverrideAmount(var Fields: IRpcStruct; aDS: TDataset; const aValueFN, aTypeFN: string);
begin
  if Abs(aDS.FieldByName('OverrideAmount').AsFloat) > 0.01 then
  begin
    Fields.AddItem(aValueFN, aDS.FieldByName('OverrideAmount').AsFloat );
    Fields.AddItem(aTypeFN, 'A' );
  end
  else if Abs(aDS.FieldByName('OverridePercent').AsFloat) > 0.01 then
  begin
    Fields.AddItem(aValueFN, aDS.FieldByName('OverridePercent').AsFloat );
    Fields.AddItem(aTypeFN, 'P' );
  end
  else if Abs(aDS.FieldByName('AdditionalWithholding').AsFloat) > 0.01 then
  begin
    Fields.AddItem(aValueFN, aDS.FieldByName('AdditionalWithholding').AsFloat );
    Fields.AddItem(aTypeFN, 'M' );
  end
  else if Assigned(aDS.FindField('AdditionalPercentWithheld')) and (Abs(aDS.FieldByName('AdditionalPercentWithheld').AsFloat) > 0.01) then
  begin
    Fields.AddItem(aValueFN, aDS.FieldByName('AdditionalPercentWithheld').AsFloat );
    Fields.AddItem(aTypeFN, 'E' );
  end
  else begin
    Fields.AddItem(aValueFN, 0 );
    Fields.AddItem(aTypeFN, 'N' );
  end;
end;

function TImportProcessing.ApplyChangePacket(aChanges: IRpcArray): IRpcStruct;
begin
  if aChanges.Count > 0 then
  try
    FLogger.LogDebug('XML change packet', aChanges.GetAsXML);
    Result := FEvoAPI.applyDataChangePacket( aChanges );
    FLogger.LogDebug('applyDataChangePacket response', Result.GetAsXML);
  except
    on e: Exception do begin
      FLogger.LogError('Apply Data Change Packet error!', e.Message);
    end;
  end;
end;

procedure TImportProcessing.LoadEmployeeW2s(
  SelectedEmployees: TSelectedEmployees; aTaxYear: integer);
var
  PdfEeW2Loader: TPdfEmployeeW2Loader;
  i, Total, Uploaded: integer;
  se: TSelectedEmployee;
  EeList: TEvoCoEeList;

  procedure loadSingleEe(Ee: TSelectedEmployee);
  begin
    // load Employee W2 reports in PDF format from Evo
    FStatus.SetMessage('', Ee.Caption, 'Get Employee W2 List');
    PdfEeW2Loader := TPDFEmployeeW2Loader.Create(Ee, FEvoAPI, FLogger, aTaxYear);
    try
      with PdfEeW2Loader do
      if W2List.RecordCount = 0 then
      begin
        FLogger.LogWarning('No W2 for ' + IntToStr(aTaxYear) + ' year. Employee: ' + Ee.Caption);
        FStatus.SetDetailMessage('No W2 for ' + IntToStr(aTaxYear) + ' year. Employee: ' + Ee.Caption);
      end
      else begin
        Total := Total + W2List.RecordCount;
        FStatus.SetProgressCount(W2List.RecordCount);

        FStatus.SetMessage('', Ee.Caption, 'Get W2 files');
        W2List.Last; // upload the most recent W2 for the selected year
{        while not W2List.Eof do
        begin
          if W2List.FieldByName('Year').AsInteger = aTaxYear then
          begin}
            FStatus.SetDetailMessage('Send W2 file to Infinity HR. Year: ' + W2List.FieldByName('Year').AsString);
            // upload W2s to Infinity HR
            try
              FInfinityHR.UpsertEmployeeW2(GetW2File, FLogger, FInfinityHRConnectionParam.Enterprise);
              Uploaded := Uploaded + 1;
            except
              on E: Exception do
                FLogger.LogError('Error uploading W2 file! Employee: ' + Ee.Caption + ' Year: ' + W2List.FieldByName('Year').AsString, e.Message);
            end;
{            FStatus.IncProgress;
          end
          else
            Total := Total - 1;

          W2List.Next;
        end;}
      end;
    finally
      FreeAndNil(PdfEeW2Loader);
      FStatus.ClearProgress;
    end;
  end;
begin
  FStatus.Clear;
  Total := 0;
  Uploaded := 0;
  if Length(SelectedEmployees) > 0 then
  try
    FStatus.SetMessage('Upload Employee W2s for ' + IntToStr(aTaxYear), 'Upload Employee W2s for ' + IntToStr(aTaxYear), 'Connect to Evolution');
    FEvoAPI := CreateEvoAPIConnection(FEvoAPIConnectionParam, FLogger);
    FStatus.SetDetailMessage('Connect to Infinity HR');
    FInfinityHR := CreateInfinityHRConnection( FLogger, FInfinityHRConnectionParam );

    for i := Low(SelectedEmployees) to High(SelectedEmployees) do
    begin
      if SelectedEmployees[i].EE_NBR > 0 then
        loadSingleEe(SelectedEmployees[i])
      else begin
        EeList := TEvoCoEeList.Create(FLogger, FEvoAPI);
        try
          EeList.LoadEmployees(SelectedEmployees[i].CL_NBR, SelectedEmployees[i].CO_NBR);
          EeList.EeList.First;
          while not EeList.EeList.Eof do
          begin
            se.Caption := 'Ee #' + EeList.EeList.FieldByName('EeCode').AsString;
            se.CL_NBR := SelectedEmployees[i].CL_NBR;
            se.CO_NBR := SelectedEmployees[i].CO_NBR;
            se.EE_NBR := EeList.EeList.FieldByName('EE_NBR').AsInteger;
            se.SSN := StringReplace( Trim(EeList.EeList.FieldByName('SSN').AsString), '-', '', [rfReplaceAll]);

            loadSingleEe(se);
            EeList.EeList.Next;
          end;
        finally
          FreeAndNil(EeList);
        end;
      end;
    end;
  finally
    FInfinityHR := nil;
    FStatus.SetMessage('Successfully uploaded ' + IntToStr(Uploaded) + ' of ' + IntToStr(Total) + ' W2 files', '', '');
    if Uploaded < Total then
      FStatus.SetMessage('Please, see log for errors!', '', '');
    FStatus.SetMessage('Ready to Start', '', '');
  end;
end;

function TImportProcessing.ProcessFederalTaxes(
  var Fields: IRpcStruct): boolean;
begin
  Result := False;
  FFedMaritalStatus := '';
  FEmployeeData.FedTaxes.First;
  while not FEmployeeData.FedTaxes.Eof do
  begin
    if PassByStartAndExpirationDates(FEmployeeData.FedTaxes.FieldByName('StartDate').AsDateTime,
      FEmployeeData.FedTaxes.FieldByName('ExpirationDate').AsDateTime) then
    begin
      Fields.AddItem('NUMBER_OF_DEPENDENTS', FEmployeeData.FedTaxes.FieldByName('Allowances').AsInteger );
      FFedMaritalStatus := FEmployeeData.MappedFedMaritalStatus;
      Fields.AddItem('Federal_Marital_Status', FFedMaritalStatus );

      SetOverrideAmount(Fields, FEmployeeData.FedTaxes, 'OVERRIDE_FED_TAX_VALUE', 'OVERRIDE_FED_TAX_TYPE');

      if FEmployeeData.FedTaxes.FieldByName('Exempt').AsBoolean then
        Fields.AddItem('EXEMPT_EXCLUDE_EE_FED', 'X' )
      else
        Fields.AddItem('EXEMPT_EXCLUDE_EE_FED', 'I' );

      Result := True;
      FEmployeeData.FedTaxes.Last;
    end;
    FEmployeeData.FedTaxes.Next;
  end;
end;

procedure TImportProcessing.SetSchedEdAmountPercent(var Fields: IRpcStruct;
  Amt, Percent: double);
begin
  if (Percent > 99) and (Percent < 101) then //Full Amount
  begin
    Fields.AddItem('CALCULATION_TYPE', 'Z'); //None
    Fields.AddItem('DEDUCT_WHOLE_CHECK', 'Y');
    Fields.AddItem('Amount', Amt);
  end
  else if (Abs(Percent) >= 0.01) and (Abs(Amt) < 0.01) then //Percent
  begin
    Fields.AddItem('CALCULATION_TYPE', 'N'); //% of Net
    Fields.AddItem('DEDUCT_WHOLE_CHECK', 'N');
    Fields.AddItem('PERCENTAGE', Percent);
  end
  else begin // if (Abs(Percent) < 0.01) and (Abs(Amt) >= 0.01) then //Flat Amount
    Fields.AddItem('CALCULATION_TYPE', 'F'); //Fixed
    Fields.AddItem('DEDUCT_WHOLE_CHECK', 'N');
    Fields.AddItem('Amount', Amt);
  end;
end;

function TImportProcessing.PassByStartAndExpirationDates(aStartDate,
  aExpirationDate: TDatetime): boolean;
begin
  Result := (aStartDate <= Today) and (aExpirationDate > Today) or (aExpirationDate < 100);
end;

procedure TImportProcessing.AutoCreateEeLocals(aEeNbr: integer);
var
  Changes: IRpcArray;
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
  i: integer;
begin
  if Assigned(EvoCoData.AutoCreateCoLocals) and EvoCoData.AutoCreateCoLocals.Active then
  begin
    Changes := TRpcArray.Create;
    i := 0;
    EvoCoData.AutoCreateCoLocals.First;
    while not EvoCoData.AutoCreateCoLocals.Eof do
    begin
      i := i - 1;
      Fields := TRpcStruct.Create;
      Fields.AddItem('EE_LOCALS_NBR', i);
      Fields.AddItem('EE_NBR', aEeNbr );
      Fields.AddItem('CO_LOCAL_TAX_NBR', EvoCoData.AutoCreateCoLocals.FieldByName('Co_Local_Tax_Nbr').AsInteger );
      Fields.AddItem('DEDUCT', EvoCoData.AutoCreateCoLocals.FieldByName('Deduct').AsString );
      Fields.AddItem('LOCAL_ENABLED', 'Y' );
      Fields.AddItem('OVERRIDE_LOCAL_TAX_TYPE', 'N' );
      Fields.AddItem('INCLUDE_IN_PRETAX', 'Y' );
      Fields.AddItem('WORK_ADDRESS_OVR', 'N' );
      Fields.AddItem('EXEMPT_EXCLUDE', 'I' );

      RowChange := TRpcStruct.Create;
      RowChange.AddItem('T', 'I');
      RowChange.AddItem('D', 'EE_LOCALS');
      RowChange.AddItem('F', Fields);

      Changes.AddItem( RowChange );
      EvoCoData.AutoCreateCoLocals.Next;
    end;
    if Changes.Count > 0 then
    begin
      FStatus.SetDetailMessage('Auto Create Employee Locals');
      ApplyChangePacket(Changes);
    end;
  end;
end;

procedure TImportProcessing.AutoCreateEeWorkShifts(aEeNbr: integer);
var
  Changes: IRpcArray;
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
  i: integer;
begin
  if Assigned(EvoCoData.AutoCreateCoShifts) and EvoCoData.AutoCreateCoShifts.Active then
  begin
    Changes := TRpcArray.Create;
    i := 0;
    EvoCoData.AutoCreateCoShifts.First;
    while not EvoCoData.AutoCreateCoShifts.Eof do
    begin
      i := i - 1;
      Fields := TRpcStruct.Create;
      Fields.AddItem('EE_WORK_SHIFTS_NBR', i);
      Fields.AddItem('EE_NBR', aEeNbr );
      Fields.AddItem('CO_SHIFTS_NBR', EvoCoData.AutoCreateCoShifts.FieldByName('Co_Shifts_Nbr').AsInteger );
      Fields.AddItem('SHIFT_RATE', EvoCoData.AutoCreateCoShifts.FieldByName('DEFAULT_AMOUNT').AsString );
      Fields.AddItem('SHIFT_PERCENTAGE', EvoCoData.AutoCreateCoShifts.FieldByName('DEFAULT_PERCENTAGE').AsString );

      RowChange := TRpcStruct.Create;
      RowChange.AddItem('T', 'I');
      RowChange.AddItem('D', 'EE_WORK_SHIFTS');
      RowChange.AddItem('F', Fields);

      Changes.AddItem( RowChange );
      EvoCoData.AutoCreateCoShifts.Next;
    end;
    if Changes.Count > 0 then
    begin
      FStatus.SetDetailMessage('Auto Create Employee Work Shifts');
      ApplyChangePacket(Changes);
    end;
  end;
end;

procedure TImportProcessing.AutoCreateEeTOA(aEeNbr: integer);
var
  Changes: IRpcArray;
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
  i: integer;
begin
  if Assigned(EvoCoData.AutoCreateCoTOA) and EvoCoData.AutoCreateCoTOA.Active then
  begin
    Changes := TRpcArray.Create;
    i := 0;
    EvoCoData.AutoCreateCoTOA.First;
    while not EvoCoData.AutoCreateCoTOA.Eof do
    begin
      if (Trim(EvoCoData.AutoCreateCoTOA.FieldByName('Auto_Create_For_Statuses').AsString) = '') or
        (Pos(IfThen(FEmployeeData.PositionStatus = '', 'N', FEmployeeData.PositionStatus),
             EvoCoData.AutoCreateCoTOA.FieldByName('Auto_Create_For_Statuses').AsString) > 0) then
      begin
        i := i - 1;
        Fields := TRpcStruct.Create;
        Fields.AddItem('EE_TIME_OFF_ACCRUAL_NBR', i);
        Fields.AddItem('EE_NBR', aEeNbr );
        Fields.AddItem('CO_TIME_OFF_ACCRUAL_NBR', EvoCoData.AutoCreateCoTOA.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger );
        Fields.AddItem('JUST_RESET', 'N');
        Fields.AddItem('CURRENT_ACCRUED', 0);
        Fields.AddItem('CURRENT_USED', 0);
        Fields.AddItem('MARKED_ACCRUED', 0);
        Fields.AddItem('MARKED_USED', 0);
        Fields.AddItem('STATUS', 'Y');
        Fields.AddItem('ROLLOVER_FREQ', 'N');
        Fields.AddItem('ROLLOVER_PAYROLL_OF_MONTH', '4');

        RowChange := TRpcStruct.Create;
        RowChange.AddItem('T', 'I');
        RowChange.AddItem('D', 'EE_TIME_OFF_ACCRUAL');
        RowChange.AddItem('F', Fields);

        Changes.AddItem( RowChange );
      end;
      EvoCoData.AutoCreateCoTOA.Next;
    end;
    if Changes.Count > 0 then
    begin
      FStatus.SetDetailMessage('Auto Create Employee TOA');
      ApplyChangePacket(Changes);
    end;
  end;
end;

procedure TImportProcessing.BuildSetEndDatePacket(var aChanges: IRpcArray;
  NewEndDate: TDateTime; Key: integer);
var
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
begin
  Fields := TRpcStruct.Create;
  RowChange := TRpcStruct.Create;

  Fields.AddItemDateTime('EFFECTIVE_END_DATE', NewEndDate);

  RowChange.AddItem('T', 'U');
  RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
  RowChange.AddItem('K', Key);
  RowChange.AddItem('F', Fields);

  aChanges.AddItem( RowChange );

  // update end date
  FEvoEe.SchedED.Edit;
  FEvoEe.SchedED.FieldByName('Effective_End_Date').AsDatetime := NewEndDate;
  FEvoEe.SchedED.Post;
end;

procedure TImportProcessing.BuildInsertScheduledEDPacket(
  var aChanges: IRpcArray; Amount, Percent: double; Cl_E_ED_Nbr: integer; StartDate,
  EndDate: TDateTime; var Key: integer; aEeNbr, aDdNbr: integer);
var
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
begin
  Key := Key - 1;
  Fields := TRpcStruct.Create;
  RowChange := TRpcStruct.Create;

  FCreatedSchedEDsNbr.Add(IntToStr(Cl_E_ED_Nbr));

  Fields.AddItem( 'EE_SCHEDULED_E_DS_NBR', Key );
  Fields.AddItem('EE_NBR', aEeNbr);
  Fields.AddItem('CL_E_DS_NBR', Cl_E_ED_Nbr);
  Fields.AddItemDateTime('EFFECTIVE_START_DATE', StartDate);
  if EndDate > StartDate then
    Fields.AddItemDateTime('EFFECTIVE_END_DATE', EndDate);

  SetSchedDefaultED(Fields, Cl_E_ED_Nbr);

  if aDdNbr > -1 then
  begin
    Fields.AddItem('Ee_Direct_Deposit_Nbr', aDdNbr);
    SetSchedEdAmountPercent(Fields, Amount, Percent);
  end
  else begin
    {-----ticket 102542-----}
    if Abs(Percent) >= 0.01 then
    begin
      Fields.AddItem('CALCULATION_TYPE', 'P'); //% of E/D Code Group Amount
      Fields.AddItem('PERCENTAGE', Percent);
//      Fields.AddItem('Cl_E_D_GROUPS_NBR', EvoCoData.Cl_E_D_Groups_Nbr);
    end
    {-----ticket 102542-----}
    else begin
      Fields.AddItem('AMOUNT', Abs(Amount));
      Fields.AddItem('CALCULATION_TYPE', 'F');
    end;
    Fields.AddItem('DEDUCT_WHOLE_CHECK', 'N');
  end;

  RowChange.AddItem('T', 'I');
  RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
  RowChange.AddItem('F', Fields);

  aChanges.AddItem( RowChange );

  // add to the FEvoEe.SchedED for possible using by DD update
  FEvoEe.SchedED.Append;
  FEvoEe.SchedED.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsInteger := Key;
  FEvoEe.SchedED.FieldByName('CL_E_Ds_Nbr').AsInteger := Cl_E_ED_Nbr;
  FEvoEe.SchedED.FieldByName('Effective_Start_Date').AsDatetime := StartDate;
  if EndDate > StartDate then
    FEvoEe.SchedED.FieldByName('Effective_End_Date').AsDatetime := EndDate;
  FEvoEe.SchedED.FieldByName('Ee_Nbr').AsInteger := aEeNbr;
  FEvoEe.SchedED.Post;
end;

procedure TImportProcessing.BuildUpdateScheduledEDPacket(
  var aChanges: IRpcArray; Amount, Percent: double; StartDate, EndDate: TDateTime;
  Key, aDdNbr: integer);
var
  RowChange: IRpcStruct;
  Fields: IRpcStruct;
begin
  Fields := TRpcStruct.Create;
  RowChange := TRpcStruct.Create;

  Fields.AddItemDateTime('EFFECTIVE_START_DATE', StartDate);
  if EndDate > StartDate then
    Fields.AddItemDateTime('EFFECTIVE_END_DATE', EndDate);
  if aDdNbr > -1 then
  begin
    Fields.AddItem('Ee_Direct_Deposit_Nbr', aDdNbr);
    SetSchedEdAmountPercent(Fields, Amount, Percent);
  end
  else begin
    {-----ticket 102542-----}
    if Abs(Percent) >= 0.01 then
    begin
      Fields.AddItem('CALCULATION_TYPE', 'P'); //% of E/D Code Group Amount
      Fields.AddItem('PERCENTAGE', Percent);
//      Fields.AddItem('Cl_E_D_GROUPS_NBR', EvoCoData.Cl_E_D_Groups_Nbr);
    end
    {-----ticket 102542-----}
    else begin
      Fields.AddItem('AMOUNT', Abs(Amount));
      Fields.AddItem('CALCULATION_TYPE', 'F');
    end;
  end;

  RowChange.AddItem('T', 'U');
  RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
  RowChange.AddItem('K', Key);
  RowChange.AddItem('F', Fields);

  aChanges.AddItem( RowChange );

  // update start and end dates
  FEvoEe.SchedED.Edit;
  FEvoEe.SchedED.FieldByName('Effective_End_Date').AsDatetime := EndDate;
  FEvoEe.SchedED.FieldByName('Effective_Start_Date').AsDatetime := StartDate;
  FEvoEe.SchedED.Post;
end;

procedure TImportProcessing.ProcessDeduction(var Changes: IRpcArray; const EdCode: string;
  Amount, Percent: double; StartDate, EndDate, ModifiedDate: TDatetime; var Key: integer; aEeNbr: integer; Insert: boolean;
  aDdNbr: integer = -1);
var
  z: integer;
  DoInsert, bIntersect: boolean;
  TmpChanges: IRpcArray;
begin
  if (aDdNbr > -1) or (PassBySelPeriod(StartDate, EndDate) and (Trim(EdCode) <> '') and not (FElectedED and (Abs(Amount) < 0.01))) then
  begin
    if not EvoCoData.LocateCLEDCode( EdCode ) then
    begin
      FStatus.SetDetailMessage('Deduction code "' + EdCode + '" is not found in Client E/Ds! ' + FCurrentEe);
      FLogger.LogWarning('Deduction code "' + EdCode + '" is not found in Client E/Ds!' + FCurrentEe);
    end
    else begin
      FStatus.SetDetailMessage('Deduction code "' + EdCode + '" has been found. ' + FCurrentEe);

      DoInsert := Insert;
      if not DoInsert then
      begin
        DoInsert := not FEvoEe.SchedED.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([aEeNbr, EvoCoData.CL_E_DS_NBR]), []);
      end;

      if DoInsert then
      begin
        FStatus.SetDetailMessage('Add scheduled deduction "' + EdCode + '"');
        BuildInsertScheduledEDPacket(Changes, Amount, Percent, EvoCoData.CL_E_DS_NBR, StartDate, EndDate, Key, aEeNbr, aDdNbr);
      end
      else begin
        FStatus.SetDetailMessage('Update scheduled deduction "' + EdCode + '"');
        FEvoEe.SchedED.Filter := 'CL_E_DS_NBR=' + IntToStr(EvoCoData.CL_E_DS_NBR);
        FEvoEe.SchedED.Filtered := True;
        TmpChanges := TRpcArray.Create;
        try
          bIntersect := False; //IHR period overlaps a current Evo period
          try
            FEvoEe.SchedED.First;
            while not FEvoEe.SchedED.Eof do
            begin
              if (FEvoEe.SchedED.FieldByName('Effective_Start_Date').AsDateTime < StartDate) and
                ((FEvoEe.SchedED.FieldByName('Effective_End_Date').AsDateTime >= StartDate) or
                 (FEvoEe.SchedED.FieldByName('Effective_End_Date').IsNull)) then
              begin // IHR Start Date is in the existing Scheduled E/D period
                if bIntersect then
                  raise Exception.Create('Period for deduction code "' + EdCode + '" overlaps two existing periods!' + FCurrentEe)
                else
                  bIntersect := True;
                if StartDate > Today then // shift Effective End Date in the current Sched E/D code and create a new Sched E/D code
                begin
                  BuildSetEndDatePacket(TmpChanges,
                    StartDate - 1,
                    FEvoEe.SchedED.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsInteger);
                  BuildInsertScheduledEDPacket(TmpChanges,
                    Amount, Percent,
                    EvoCoData.CL_E_DS_NBR,
                    StartDate,
                    EndDate, Key, aEeNbr, aDdNbr);
                end
                else // update the current Sched E/D code
                begin
                  BuildUpdateScheduledEDPacket(TmpChanges,
                    Amount, Percent,
                    StartDate,
                    EndDate,
                    FEvoEe.SchedED.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsInteger, aDdNbr);
                end;
              end
              else if ((FEvoEe.SchedED.FieldByName('Effective_Start_Date').AsDateTime >= StartDate) and
              ((FEvoEe.SchedED.FieldByName('Effective_Start_Date').AsDateTime < EndDate) or
               (EndDate < StartDate))) then
              begin // existing Scheduled E/D Start Date is between IHR Start Date and End Date
                if bIntersect then
                  raise Exception.Create('Period for deduction code "' + EdCode + '" overlaps two existing periods!' + FCurrentEe)
                else
                  bIntersect := True;
                // update the current Sched E/D code
                BuildUpdateScheduledEDPacket(TmpChanges, Amount, Percent, StartDate, EndDate, FEvoEe.SchedED.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsInteger, aDdNbr);
              end;
              FEvoEe.SchedED.Next;
            end;
            if not bIntersect then
              BuildInsertScheduledEDPacket(TmpChanges, Amount, Percent, EvoCoData.CL_E_DS_NBR, StartDate, EndDate, Key, aEeNbr, aDdNbr);
            for z := 0 to TmpChanges.Count-1 do
              Changes.AddItem( TmpChanges.Items[z].AsStruct );
          except
            on E: Exception do
              FLogger.LogError(E.Message);
          end;
        finally
          FEvoEe.SchedED.Filter := '';
          FEvoEe.SchedED.Filtered := False;
          TmpChanges := nil;
        end;
      end;
    end;
  end;
end;

procedure TImportProcessing.SetSchedDefaultED(var aFields: IRpcStruct; ClEDsNbr: integer);
var
  FDs: TkbmCustomMemTable;
  param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('ClEDsNbr', ClEDsNbr);
  FDs := RunQuery('D1_ClEDCode.rwq', param);
  try
    aFields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
    aFields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
    aFields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
    aFields.AddItem('TARGET_ACTION', 'N');

{    if (FDs.RecordCount > 0) and (FDs.FieldByName('Cl_E_D_Groups_Nbr').AsInteger > 0) then
      aFields.AddItem('Cl_E_D_GROUPS_NBR', FDs.FieldByName('Cl_E_D_Groups_Nbr').AsInteger);}

    if (FDs.RecordCount > 0) and (FDs.FieldByName('Scheduled_Defaults').AsString = 'Y') then
    begin
      aFields.AddItem('FREQUENCY', FDs.FieldByName('Frequency').AsString);
      aFields.AddItem('EXCLUDE_WEEK_1', FDs.FieldByName('EXCLUDE_WEEK_1').AsString);
      aFields.AddItem('EXCLUDE_WEEK_2', FDs.FieldByName('EXCLUDE_WEEK_2').AsString);
      aFields.AddItem('EXCLUDE_WEEK_3', FDs.FieldByName('EXCLUDE_WEEK_3').AsString);
      aFields.AddItem('EXCLUDE_WEEK_4', FDs.FieldByName('EXCLUDE_WEEK_4').AsString);
      aFields.AddItem('EXCLUDE_WEEK_5', FDs.FieldByName('EXCLUDE_WEEK_5').AsString);
      aFields.AddItem('ALWAYS_PAY', FDs.FieldByName('ALWAYS_PAY').AsString);
      aFields.AddItem('DEDUCTIONS_TO_ZERO', FDs.FieldByName('DEDUCTIONS_TO_ZERO').AsString);
      aFields.AddItem('PLAN_TYPE', FDs.FieldByName('PLAN_TYPE').AsString);
      aFields.AddItem('WHICH_CHECKS', FDs.FieldByName('WHICH_CHECKS').AsString);
      aFields.AddItem('USE_PENSION_LIMIT', FDs.FieldByName('USE_PENSION_LIMIT').AsString);
      if FDs.FieldByName('DEFAULT_CL_AGENCY_NBR').AsInteger > 0 then
        aFields.AddItem('CL_AGENCY_NBR', FDs.FieldByName('DEFAULT_CL_AGENCY_NBR').AsInteger)
    end
    else begin
      aFields.AddItem('FREQUENCY', 'P');
      aFields.AddItem('EXCLUDE_WEEK_1', 'N');
      aFields.AddItem('EXCLUDE_WEEK_2', 'N');
      aFields.AddItem('EXCLUDE_WEEK_3', 'N');
      aFields.AddItem('EXCLUDE_WEEK_4', 'N');
      aFields.AddItem('EXCLUDE_WEEK_5', 'N');
      aFields.AddItem('ALWAYS_PAY', 'N');
      aFields.AddItem('DEDUCTIONS_TO_ZERO', 'N');
      aFields.AddItem('PLAN_TYPE', 'N');
      aFields.AddItem('WHICH_CHECKS', 'A'); // All
      aFields.AddItem('USE_PENSION_LIMIT', 'N');
    end;
  finally
    Param := nil;
    FreeAndNil(FDs);
  end;
end;

function TImportProcessing.PassBySelPeriod(
  aEffectiveDate, aExpirationDate, aModifiedDate: TDatetime): boolean;
begin
  Result := ((aEffectiveDate >= FDat_b) and (aEffectiveDate <= FDat_e) or
    (aExpirationDate >= FDat_b) and (aExpirationDate <= FDat_e) or
    (aEffectiveDate < FDat_b) and ((aExpirationDate > FDat_e) or (aExpirationDate < 100)))
    or
    ((aModifiedDate >= FDat_b) and (aModifiedDate <= FDat_e));
end;

function TImportProcessing.PassBySelPeriod(
  aEffectiveDate, aExpirationDate: TDatetime): boolean;
begin
  Result := ((aEffectiveDate >= FDat_b) and (aEffectiveDate <= FDat_e) or
    (aExpirationDate >= FDat_b) and (aExpirationDate <= FDat_e) or
    (aEffectiveDate < FDat_b) and ((aExpirationDate > FDat_e) or (aExpirationDate < 100)))
    or
    (aEffectiveDate >= FDat_b);
end;

procedure TImportProcessing.AutoCreateScheduledEDs(aEeNbr: integer);
var
  param: IRpcStruct;
  AutoEDs: TkbmCustomMemTable;
  Changes: IRpcArray;
  Key: integer;

  procedure InsertScheduledED;
  var
    RowChange: IRpcStruct;
    Fields: IRpcStruct;
  begin
    Key := Key - 1;
    Fields := TRpcStruct.Create;
    RowChange := TRpcStruct.Create;

    Fields.AddItem( 'EE_SCHEDULED_E_DS_NBR', Key );
    Fields.AddItem('EE_NBR', aEeNbr);
    Fields.AddItem('CL_E_DS_NBR', AutoEDs.FieldByName('Cl_E_DS_Nbr').AsInteger);

    if AutoEDs.FieldByName('SD_EFFECTIVE_START_DATE').AsFloat > 100 then
      Fields.AddItemDateTime('EFFECTIVE_START_DATE', AutoEDs.FieldByName('SD_EFFECTIVE_START_DATE').AsDateTime)
    else
      Fields.AddItemDateTime('EFFECTIVE_START_DATE', Now);

    Fields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
    Fields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
    Fields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
    Fields.AddItem('TARGET_ACTION', 'N');
    Fields.AddItem('DEDUCT_WHOLE_CHECK', 'N');

    Fields.AddItem('FREQUENCY', AutoEDs.FieldByName('SD_FREQUENCY').AsString);
    Fields.AddItem('EXCLUDE_WEEK_1', AutoEDs.FieldByName('SD_EXCLUDE_WEEK_1').AsString);
    Fields.AddItem('EXCLUDE_WEEK_2', AutoEDs.FieldByName('SD_EXCLUDE_WEEK_2').AsString);
    Fields.AddItem('EXCLUDE_WEEK_3', AutoEDs.FieldByName('SD_EXCLUDE_WEEK_3').AsString);
    Fields.AddItem('EXCLUDE_WEEK_4', AutoEDs.FieldByName('SD_EXCLUDE_WEEK_4').AsString);
    Fields.AddItem('EXCLUDE_WEEK_5', AutoEDs.FieldByName('SD_EXCLUDE_WEEK_5').AsString);

    Fields.AddItem('ALWAYS_PAY', AutoEDs.FieldByName('SD_ALWAYS_PAY').AsString);
    Fields.AddItem('DEDUCTIONS_TO_ZERO', AutoEDs.FieldByName('SD_DEDUCTIONS_TO_ZERO').AsString);
    Fields.AddItem('PLAN_TYPE', AutoEDs.FieldByName('SD_PLAN_TYPE').AsString);
    Fields.AddItem('WHICH_CHECKS', AutoEDs.FieldByName('SD_WHICH_CHECKS').AsString);
    Fields.AddItem('USE_PENSION_LIMIT', AutoEDs.FieldByName('SD_USE_PENSION_LIMIT').AsString);

    if AutoEDs.FieldByName('DEFAULT_CL_AGENCY_NBR').AsInteger > 0 then
      Fields.AddItem('CL_AGENCY_NBR', AutoEDs.FieldByName('DEFAULT_CL_AGENCY_NBR').AsInteger);

    Fields.AddItem('AMOUNT', AutoEDs.FieldByName('SD_AMOUNT').AsFloat);
    Fields.AddItem('PERCENTAGE', AutoEDs.FieldByName('SD_RATE').AsFloat);
    Fields.AddItem('CALCULATION_TYPE', AutoEDs.FieldByName('SD_CALCULATION_METHOD').AsString);

    if not AutoEDs.FieldByName('CL_E_D_GROUPS_NBR').IsNull then
      Fields.AddItem('CL_E_D_GROUPS_NBR', AutoEDs.FieldByName('CL_E_D_GROUPS_NBR').AsInteger);

    if not AutoEDs.FieldByName('SD_PRIORITY_NUMBER').IsNull then
      Fields.AddItem('PRIORITY_NUMBER', AutoEDs.FieldByName('SD_PRIORITY_NUMBER').AsInteger);

    if not AutoEDs.FieldByName('SD_MAX_AVG_AMT_GRP_NBR').IsNull then
      Fields.AddItem('MAX_AVG_AMT_CL_E_D_GROUPS_NBR', AutoEDs.FieldByName('SD_MAX_AVG_AMT_GRP_NBR').AsInteger);

    if not AutoEDs.FieldByName('SD_MAX_AVG_HRS_GRP_NBR').IsNull then
      Fields.AddItem('MAX_AVG_HRS_CL_E_D_GROUPS_NBR', AutoEDs.FieldByName('SD_MAX_AVG_HRS_GRP_NBR').AsInteger);

    if not AutoEDs.FieldByName('SD_MAX_AVERAGE_HOURLY_WAGE_RATE').IsNull then
      Fields.AddItem('MAX_AVERAGE_HOURLY_WAGE_RATE', AutoEDs.FieldByName('SD_MAX_AVERAGE_HOURLY_WAGE_RATE').AsFloat);

    if not AutoEDs.FieldByName('SD_THRESHOLD_E_D_GROUPS_NBR').IsNull then
      Fields.AddItem('THRESHOLD_E_D_GROUPS_NBR', AutoEDs.FieldByName('SD_THRESHOLD_E_D_GROUPS_NBR').AsInteger);

    if not AutoEDs.FieldByName('SD_THRESHOLD_AMOUNT').IsNull then
      Fields.AddItem('THRESHOLD_AMOUNT', AutoEDs.FieldByName('SD_THRESHOLD_AMOUNT').AsFloat);

    RowChange.AddItem('T', 'I');
    RowChange.AddItem('D', 'EE_SCHEDULED_E_DS');
    RowChange.AddItem('F', Fields);

    Changes.AddItem( RowChange );
  end;
begin
//  FEvoAPI.SB_autoEnlistEDsForNewEE(FCurrentCompany.CO_NBR, aEeNbr);

  param := TRpcStruct.Create;
  Param.AddItem('CoNbr', FCurrentCompany.CO_NBR);
  AutoEDs := RunQuery('AutoSchedEdCodes.rwq', param);
  Changes := TRpcArray.Create;
  Key := -1;
  try
    AutoEDs.First;
    while not AutoEDs.Eof do
    begin
      // it should not autocreate if scheduled E/Ds has already been created from EmployeeInfo.Deductions or EmployeeInfo.DirectDeposit
      if FCreatedSchedEDsNbr.IndexOf(AutoEDs.FieldByName('Cl_E_DS_NBR').AsString) < 0 then
      begin
        FCreatedSchedEDsNbr.Add(AutoEDs.FieldByName('Cl_E_DS_NBR').AsString);
        InsertScheduledED;
      end;
      AutoEDs.Next;
    end;

    if Changes.Count > 0 then
      ApplyChangePacket(Changes);
  finally
    Changes := nil;
    param := nil;
    FCreatedSchedEDsNbr.Clear;
    AutoEDs.Close;
    FreeAndNil(AutoEDs);
  end;
end;

function TImportProcessing.IncompleteEeSetupWarnings: string;
begin
  Result := FUnableToSync;
end;

function TImportProcessing.AllowPercentage(const EdCode: string): boolean;
begin
  Result := Pos( EvoCoData.GetEDCodeType(EdCode), ' D! D# D$ D% D( D) D* D@ D\ D2 D3 D4 ' +
   'DA DB DC DD DE DF DG DH DI DK DX M3 M5 M6 ' ) > 0;
end;

end.
