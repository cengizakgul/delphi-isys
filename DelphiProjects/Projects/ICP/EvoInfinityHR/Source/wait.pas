unit wait;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmWait = class(TForm)
    lblMessage: TLabel;
    Bevel1: TBevel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmWait: TfrmWait;

  procedure StartWait(const aMessage: string);
  procedure StopWait;

implementation

{$R *.dfm}

procedure StartWait(const aMessage: string);
begin
  frmWait := TfrmWait.Create( Application );
  if Assigned( frmWait ) then
  begin
    frmWait.lblMessage.Caption := aMessage;
    frmWait.Show;
    Application.ProcessMessages;
  end;
end;

procedure StopWait;
begin
  if Assigned( frmWait ) then
    FreeAndNil( frmWait );
end;

end.
