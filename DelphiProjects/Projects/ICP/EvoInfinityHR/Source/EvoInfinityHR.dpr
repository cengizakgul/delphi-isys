program EvoInfinityHR;

uses
  {$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  //{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  Forms,
  EvoAPIConnection,
  main in 'main.pas' {frmMain},
  EvoInfinityHRConstAndProc in 'EvoInfinityHRConstAndProc.pas',
  CommonDlg in 'dialogs\CommonDlg.pas' {frmDialog},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TFrame},
  EvoConnectionDlg in 'dialogs\EvoConnectionDlg.pas' {frmEvoConnectionDlg},
  EvoInfinityHRProcessing in 'EvoInfinityHRProcessing.pas',
  Log in 'Log.pas' {frmLogger},
  InfinityHRConnectionDlg in 'dialogs\InfinityHRConnectionDlg.pas' {frmInfinityHRConnectionDlg},
  InfinityHRConnectionParamFrame in 'InfinityHRConnectionParamFrame.pas' {InfinityHRConnectionParamFrm: TFrame},
  InfinityHRConnection in 'InfinityHRConnection.pas',
  Status in 'Status.pas' {frmStatus: TFrame},
  PayrollSync in 'PayrollSync.pas',
  EvoInfinityMapping in 'EvoInfinityMapping.pas',
  DateRangeFrame in 'DateRangeFrame.pas' {frmDateRange: TFrame},
  EeHolder in 'EeHolder.pas',
  About in 'About.pas' {frmAbout},
  Scheduler in 'Scheduler.pas' {frmScheduler},
  ImportTaskParameters in 'ImportTaskParameters.pas' {frmImportTaskParameters},
  SimpleScheduler in 'SimpleScheduler.pas' {frmSimpleScheduler: TFrame},
  CheckSelectCommon in 'dialogs\CheckList\CheckSelectCommon.pas',
  wait in 'wait.pas' {frmWait},
  DelphiNotesUTC in 'DelphiNotesUTC.pas',
  UploadW2TaskParameters in 'UploadW2TaskParameters.pas' {frmUploadW2TaskParameters},
  OptionsBaseFrame in '..\..\Common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  SmtpConfigFrame in '..\..\Common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  SelectCheckFrm in 'Dialogs\CheckList\SelectCheckFrm.pas' {SelectCheckFrame: TFrame},
  SelectEmployeeFrm in 'Dialogs\EmployeeList\SelectEmployeeFrm.pas' {SelecEmployeeFrame: TFrame},
  UploadTaskParameters in 'UploadTaskParameters.pas' {frmUploadTaskParameters},
  TypInfo in 'TypInfo.pas';

{$R *.res}

begin
  LicenseKey := 'C7BF4595F6DC40D391BF67BAB7661E7B'; // EvoInfinityHR License

  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
