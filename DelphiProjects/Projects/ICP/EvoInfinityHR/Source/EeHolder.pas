unit EeHolder;

interface

uses DB, kbmMemTable, PayrollSync, gdyCommonLogger, SysUtils, EvoData, Common,
  EvoInfinityMapping, EvoAPIConnection, XmlRpcTypes, wait;

type
  THolder = class
  protected
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
  public
    function RunQuery(aFileName: string; params: IRpcStruct = nil): TkbmCustomMemTable;
    constructor Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection);
  end;

  TEeHolder = class(THolder)
  private
    FEeData: TkbmCustomMemTable;
    FSchedEd: TkbmCustomMemTable;
    FRates: TkbmCustomMemTable;
    FStates: TkbmCustomMemTable;
    FEE_DD: TkbmCustomMemTable;

    function GetFieldValue(Index: integer): variant;
    function GetEENbr: integer;
    function GetCoNbr: integer;
    function GetClPersonNbr: integer;
  public
    destructor Destroy; override;

    function FindEeBySSN(const aSSN, aEeCode: string; aCoNbr: integer): boolean;
    function CheckForSSNAccess(aCoNbr: integer): boolean;

    function FieldByName(const aFieldName: string): TField;
    property FieldValue[Index: integer]: variant read GetFieldValue;
    property EE_NBR: integer read GetEENbr;
    property CL_PERSON_NBR: integer read GetCLPersonNbr;
    property CO_NBR: integer read GetCoNbr;

    property SchedED: TkbmCustomMemTable read FSchedEd;
    property Rates: TkbmCustomMemTable read FRates;
    property DirectDeposit: TkbmCustomMemTable read FEE_DD;
    property States: TkbmCustomMemTable read FStates;
  end;

  TEvoCoList = class(THolder)
  protected
    FCoListData: TkbmCustomMemTable;
  public
    destructor Destroy; override;

    function GetCoList(const aNumber: string; const aName: string): TkbmCustomMemTable;
  end;

  TEvoCoEeList = class(TEvoCoList)
  protected
    FEeListData: TkbmCustomMemTable;
    function GetEeList: TkbmCustomMemTable;
  public
    destructor Destroy; override;
    procedure LoadEmployees; overload;
    procedure LoadEmployees(Cl_Nbr, Co_Nbr: integer); overload;
    property EeList: TkbmCustomMemTable read GetEeList;
  end;

  TEvoCoPrList = class(TEvoCoList)
  protected
    FPrListData: TkbmCustomMemTable;
    FPrCheckListData: TkbmCustomMemTable;
    FPrDs: TDataSource;

    function GetPrList: TkbmCustomMemTable;
    function GetPrCheckList: TkbmCustomMemTable;
  public
    constructor Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection);
    destructor Destroy; override;

    procedure LoadPayrollsChecks(aDat_b, aDat_e: TDatetime);
    property PrList: TkbmCustomMemTable read GetPrList;
    property PrCheckList: TkbmCustomMemTable read GetPrCheckList;
  end;

  TEvoCoData = class(THolder)
  private
    FDBDTData: TkbmCustomMemTable;
    FWorkersComp: TkbmCustomMemTable;
    FClEDs: TkbmCustomMemTable;
    FSyStatesMaritalStatus: TkbmCustomMemTable;
    FCoStates: TkbmCustomMemTable;
    FCoLocals: TkbmCustomMemTable;
    FCoShifts: TkbmCustomMemTable;
    FCoTOA: TkbmCustomMemTable;

    FDivFound: boolean;
    FDivisionNbr: integer;
    FBrFound: boolean;
    FBranchNbr: integer;
    FDepFound: boolean;
    FDepartmentNbr: integer;
    FTmFound: boolean;
    FTeamNbr: integer;
    FCO_WORKERS_COMP_NBR: integer;
    FCL_E_DS_NBR: integer;
    FCl_E_D_Groups_Nbr: integer;
    FCO_STATES_NBR: integer;
    FSY_STATE_MARITAL_STATUS: integer;
    FCO_ENABLE_ESS: string; // N - No, R - Read Only, Y - Full Access
    FCO_ENABLE_TIME_OFF: string; // N - No, R - Read Only, Y - Full Access
    FCO_ENABLE_BENEFITS: string; // N - No, R - Read Only, Y - Full Access
    FCO_ENABLE_DD: string; // N - No, R - Read Only, Y - Full Access

    procedure ClearDBDTProperties;
  public
    constructor Create(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection);
    destructor Destroy; override;

    procedure OpenCompanyTables(CoNbr: integer);
    procedure OpenClientTables;

    procedure LocateDBDTByCustomNumbers(const DivCNum, BrCNum, DepCNum, TmCNum: string);
    function LocateWorkersComp(const WC_CODE: string): boolean;
    function LocateCLEDCode(const EDCode: string): boolean;
    function LocateCoState(const State: string): boolean;
    function LocateSyStateMaritalStatus(const State, Status: string): boolean;
    function GetEDCodeType(const EDCode: string): string;

    property DivisionFound: boolean read FDivFound;
    property BranchFound: boolean read FBrFound;
    property DepartmentFound: boolean read FDepFound;
    property TeamFound: boolean read FTmFound;

    property CO_DIVISION_NBR: integer read FDivisionNbr;
    property CO_BRANCH_NBR: integer read FBranchNbr;
    property CO_DEPARTMENT_NBR: integer read FDepartmentNbr;
    property CO_TEAM_NBR: integer read FTeamNbr;

    property CO_WORKERS_COMP_NBR: integer read FCO_WORKERS_COMP_NBR;
    property CL_E_DS_NBR: integer read FCL_E_DS_NBR;
    property Cl_E_D_Groups_Nbr: integer read FCl_E_D_Groups_Nbr;
    property CO_STATES_NBR: integer read FCO_STATES_NBR;
    property SY_STATE_MARITAL_STATUS: integer read FSY_STATE_MARITAL_STATUS;

    property Enable_ESS: string read FCO_ENABLE_ESS;
    property Enable_Time_Off: string read FCO_ENABLE_TIME_OFF;
    property Enable_Benefits: string read FCO_ENABLE_BENEFITS;
    property Enable_DD: string read FCO_ENABLE_DD;

    property AutoCreateCoLocals: TkbmCustomMemTable read FCoLocals;
    property AutoCreateCoShifts: TkbmCustomMemTable read FCoShifts;
    property AutoCreateCoTOA: TkbmCustomMemTable read FCoTOA;
  end;

implementation

uses gdyRedir, EvoInfinityHRConstAndProc, Variants;

{ TEeHolder }

function TEeHolder.CheckForSSNAccess(aCoNbr: integer): boolean;
begin
  // rough
  if Assigned(FEeData) then
    FreeAndNil(FEeData);

  FEeData := RunQuery('SSNCheck.rwq', nil);
  Result := Assigned(FEeData) and Assigned(FEeData.FindField('PersonCount')) and Assigned(FEeData.FindField('ZeroSSNCount')) and
    ((FEeData.FindField('PersonCount').AsInteger = 0) or
    (FEeData.FindField('ZeroSSNCount').AsInteger < FEeData.FindField('PersonCount').AsInteger));

  if Assigned(FEeData) then
    FreeAndNil(FEeData);
end;

destructor TEeHolder.Destroy;
begin
//  FEeData.Close;
  FreeAndNil( FEeData );
//  FSchedEd.Close;
  FreeAndNil( FSchedEd );
//  FRates.Close;
  FreeAndNil( FRates );
//  FEE_DD.Close;
  FreeAndNil( FEE_DD );
  FreeAndNil( FStates );

  inherited;
end;

function TEeHolder.FieldByName(const aFieldName: string): TField;
begin
  if FEeData.Active and (FEeData.RecordCount > 0) and Assigned(FEeData.FindField( aFieldName )) then
    Result := FEeData.FindField( aFieldName )
  else
    Result := nil;  
end;

function TEeHolder.FindEeBySSN(const aSSN, aEeCode: string; aCoNbr: integer): boolean;
var
  Param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem( 'SSN', aSSN );
  Param.AddItem( 'CoNbr', aCoNbr );

  if Assigned(FEeData) then
    FreeAndNil(FEeData);

  FEeData := RunQuery('EeQuery.rwq', Param);

  // if there are more than one EE record choose the Active one (CURRENT_TERMINATION_CODE = A)
  // add debug log message somethng like: more than 1 EE's found, locate Active successful
  if FEeData.RecordCount > 1 then
  begin
    FLogger.LogDebug('More than one EE found!');
    if FEeData.Locate('EeCode', Trim(aEeCode), []){FEeData.Locate('CURRENT_TERMINATION_CODE', 'A', [])} then
      FLogger.LogDebug('Ee "'+aEeCode+'" selected, EE_NBR = ' + FEeData.FindField('EE_NBR').AsString);
  end;

  Param.Clear;
  Param.AddItem('EeNbr', FEeData.FindField('EE_NBR').AsInteger );

  // SchedEd must be created even for new employees since it's used 
  if Assigned(FSchedEd) then FreeAndNil(FSchedEd);
  FSchedEd := RunQuery('SchedEdQuery.rwq', Param);

  Result := (FEeData.RecordCount > 0) and Assigned(FEeData.FindField('EE_NBR')) and (FEeData.FindField('EE_NBR').AsInteger > 0);
  if Result then
  begin
    FEeData.Locate('EeCode', Trim(aEeCode), []);

    if Assigned(FRates) then FreeAndNil(FRates);
    if Assigned(FEE_DD) then FreeAndNil(FEE_DD);
    if Assigned(FStates) then FreeAndNil(FStates);

    FRates := RunQuery('EeRatesQuery.rwq', Param);
    FEE_DD := RunQuery('EeDirectDepositQuery.rwq', Param);
    FStates := RunQuery('EeStatesQuery.rwq', Param);
  end;
end;

function TEeHolder.GetClPersonNbr: integer;
begin
  if FEeData.Active and (FEeData.RecordCount > 0) and Assigned(FEeData.FindField( 'CL_PERSON_NBR' )) then
    Result := FEeData.FindField( 'CL_PERSON_NBR' ).AsVariant
  else
    Result := -1;
end;

function TEeHolder.GetCoNbr: integer;
begin
  if FEeData.Active and (FEeData.RecordCount > 0) and Assigned(FEeData.FindField( 'CO_NBR' )) then
    Result := FEeData.FindField( 'CO_NBR' ).AsVariant
  else
    Result := -1;
end;

function TEeHolder.GetEENbr: integer;
begin
  if FEeData.Active and (FEeData.RecordCount > 0) and Assigned(FEeData.FindField( 'EE_NBR' )) then
    Result := FEeData.FindField( 'EE_NBR' ).AsVariant
  else
    Result := -1;
end;

function TEeHolder.GetFieldValue(Index: integer): variant;
begin
  if FEeData.Active and (FEeData.RecordCount > 0) and Assigned(FEeData.FindField( Mapping[Index].EvoFld )) then
    Result := FEeData.FindField( Mapping[Index].EvoFld ).AsVariant
  else
    Result := Null;
end;

{ TEvoCoData }

constructor TEvoCoData.Create(Logger: ICommonLogger;
  EvoAPI: IEvoAPIConnection);
begin
  inherited Create(Logger, EvoAPI);
  ClearDBDTProperties;
end;

destructor TEvoCoData.Destroy;
begin
  FreeAndNil( FWorkersComp );
  FreeAndNil( FDBDTData );
  FreeAndNil( FClEDs );
  FreeAndNil( FSyStatesMaritalStatus );
  FreeAndNil( FCoStates );
  FreeAndNil( FCoLocals );
  FreeAndNil( FCoShifts );
  FreeAndNil( FCoTOA );

  inherited;
end;

procedure TEvoCoData.ClearDBDTProperties;
begin
  FDivFound := False;
  FBrFound := False;
  FDepFound := False;
  FTmFound := False;

  FDivisionNbr := -1;
  FBranchNbr := -1;
  FDepartmentNbr := -1;
  FTeamNbr := -1;
end;

procedure TEvoCoData.LocateDBDTByCustomNumbers(const DivCNum, BrCNum,
  DepCNum, TmCNum: string);
begin
  ClearDBDTProperties;

  if (FDBDTData <> nil) and FDBDTData.Active then
  begin
    if (DivCNum <> '') and (BrCNum <> '') and (DepCNum <> '') and (TmCNum <> '') then
    begin
      if FDBDTData.Locate('Custom_Division_Number;Custom_Branch_Number;Custom_Department_Number;Custom_Team_Number', VarArrayOf([Trim(DivCNum),Trim(BrCNum),Trim(DepCNum),Trim(TmCNum)]), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;

        FBrFound := True;
        FBranchNbr := FDBDTData.FieldByName('CO_BRANCH_NBR').AsInteger;

        FDepFound := True;
        FDepartmentNbr := FDBDTData.FieldByName('CO_DEPARTMENT_NBR').AsInteger;

        FTmFound := True;
        FTeamNbr := FDBDTData.FieldByName('CO_TEAM_NBR').AsInteger;
      end;
    end;
    if (DivCNum <> '') and (BrCNum <> '') and (DepCNum <> '') and not FTmFound then
    begin
      if FDBDTData.Locate('Custom_Division_Number;Custom_Branch_Number;Custom_Department_Number', VarArrayOf([Trim(DivCNum),Trim(BrCNum),Trim(DepCNum)]), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;

        FBrFound := True;
        FBranchNbr := FDBDTData.FieldByName('CO_BRANCH_NBR').AsInteger;

        FDepFound := True;
        FDepartmentNbr := FDBDTData.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
      end;
    end;
    if (DivCNum <> '') and (BrCNum <> '') and not FDepFound then
    begin
      if FDBDTData.Locate('Custom_Division_Number;Custom_Branch_Number', VarArrayOf([Trim(DivCNum),Trim(BrCNum)]), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;

        FBrFound := True;
        FBranchNbr := FDBDTData.FieldByName('CO_BRANCH_NBR').AsInteger;
      end;
    end;
    if (DivCNum <> '') and not FBrFound then
    begin
      if FDBDTData.Locate('Custom_Division_Number', Trim(DivCNum), []) then
      begin
        FDivFound := True;
        FDivisionNbr := FDBDTData.FieldByName('CO_DIVISION_NBR').AsInteger;
      end;
    end;
  end;
end;

function TEvoCoData.LocateWorkersComp(const WC_CODE: string): boolean;
begin
  Result := False;
  FCO_WORKERS_COMP_NBR := -1;

  if (FWorkersComp <> nil) and FWorkersComp.Active and (Trim(WC_CODE) <> '') then
  begin
    if FWorkersComp.Locate('WORKERS_COMP_CODE', Trim(WC_CODE), []) then
    begin
      Result := True;
      FCO_WORKERS_COMP_NBR := FWorkersComp.FieldByName('Co_Workers_Comp_Nbr').AsInteger;
    end;
  end;
end;

procedure TEvoCoData.OpenCompanyTables(CoNbr: integer);
var
  Param: IRpcStruct;
  CoInfo: TkbmCustomMemTable;

  function GetEeTableValue(const aOption: string): string;
  begin
    // Co Table: N - No, R - Read Only, Y - Full Access
    // Ee Table: N - No, Y - Read Only, F - Full Access
    Result := aOption;
    if Result = 'R' then
      Result := 'Y'
    else if Result = 'Y' then
      Result := 'F';
  end;
begin
  param := TRpcStruct.Create;
  Param.AddItem('CoNbr', CoNbr);

  if Assigned(FDBDTData) then FreeAndNil(FDBDTData);
  FDBDTData := RunQuery('DBDT.rwq', Param);

  if Assigned(FWorkersComp) then FreeAndNil(FWorkersComp);
  FWorkersComp := RunQuery('WorkersCompQuery.rwq', Param);

  if Assigned(FCoStates) then FreeAndNil(FCoStates);
  FCoStates := RunQuery('CoStates.rwq', Param);

  if Assigned(FCoLocals) then FreeAndNil(FCoLocals);
  FCoLocals := RunQuery('CoLocals.rwq', Param);

  if Assigned(FCoShifts) then FreeAndNil(FCoShifts);
  FCoShifts := RunQuery('CoShifts.rwq', Param);

  if Assigned(FCoTOA) then FreeAndNil(FCoTOA);
  FCoTOA := RunQuery('CoTOA.rwq', Param);

  CoInfo := RunQuery('qCoInfo.rwq', Param);
  try
    if CoInfo.Active and (CoInfo.RecordCount >= 1) then
    begin
      FCO_ENABLE_ESS := GetEeTableValue( CoInfo.FieldByName('ENABLE_ESS').AsString );
      FCO_ENABLE_TIME_OFF := GetEeTableValue( CoInfo.FieldByName('ENABLE_TIME_OFF').AsString );
      FCO_ENABLE_BENEFITS := GetEeTableValue( CoInfo.FieldByName('ENABLE_BENEFITS').AsString );
      FCO_ENABLE_DD := GetEeTableValue( CoInfo.FieldByName('ENABLE_DD').AsString );
    end
    else begin // default values
      FCO_ENABLE_ESS := 'N';
      FCO_ENABLE_TIME_OFF := 'Y';
      FCO_ENABLE_BENEFITS := 'Y';
      FCO_ENABLE_DD := 'N';
    end;
  finally
    FreeAndNil(CoInfo);
  end;
end;

procedure TEvoCoData.OpenClientTables;
begin
  FClEDs := RunQuery('ClEDs.rwq');
  FSyStatesMaritalStatus := RunQuery('SY_STATE_MARITAL_STATUS.rwq');
end;

function TEvoCoData.LocateCLEDCode(const EDCode: string): boolean;
begin
  Result := False;
  FCL_E_DS_NBR := -1;
  FCl_E_D_Groups_Nbr := -1;

  if (FClEDs <> nil) and FClEDs.Active and (Trim(EDCode) <> '') then
  begin
    if FClEDs.Locate('Custom_E_D_Code_Number', Trim(EDCode), []) then
    begin
      Result := True;
      FCL_E_DS_NBR := FClEDs.FieldByName('CL_E_DS_Nbr').AsInteger;
      if not FClEDs.FieldByName('Cl_E_D_Groups_Nbr').IsNull then
        FCl_E_D_Groups_Nbr := FClEDs.FieldByName('Cl_E_D_Groups_Nbr').AsInteger;
    end;
  end;
end;

function TEvoCoData.LocateCoState(const State: string): boolean;
begin
  Result := False;
  FCO_STATES_NBR := -1;

  if (FCoStates <> nil) and FCoStates.Active and (Trim(State) <> '') then
  begin
    if FCoStates.Locate('STATE', Trim(State), []) then
    begin
      Result := True;
      FCO_STATES_NBR := FCoStates.FieldByName('Co_States_Nbr').AsInteger;
    end;
  end;
end;

function TEvoCoData.LocateSyStateMaritalStatus(const State,
  Status: string): boolean;
begin
  Result := False;
  if FSyStatesMaritalStatus <> nil then
  begin
    FSY_STATE_MARITAL_STATUS := IntConvertNull(FSyStatesMaritalStatus.Lookup('STATE;STATUS_TYPE', VarArrayOf([State, Status]), 'SY_STATE_MARITAL_STAT_NBR'), -1);
    Result := FSY_STATE_MARITAL_STATUS > -1;
  end;  
end;

function TEvoCoData.GetEDCodeType(const EDCode: string): string;
begin
  Result := '';
  FCL_E_DS_NBR := -1;

  if (FClEDs <> nil) and FClEDs.Active and (Trim(EDCode) <> '') then
  begin
    if FClEDs.Locate('Custom_E_D_Code_Number', Trim(EDCode), []) then
    begin
      Result := FClEDs.FieldByName('E_D_Code_Type').AsString;
      FCL_E_DS_NBR := FClEDs.FieldByName('CL_E_DS_Nbr').AsInteger;
    end;
  end;
end;

{ TEvoCoPrList }

constructor TEvoCoPrList.Create(Logger: ICommonLogger;
  EvoAPI: IEvoAPIConnection);
begin
  inherited Create(Logger, EvoAPI);
  FPrDs := TDataSource.Create(nil);
end;

destructor TEvoCoPrList.Destroy;
begin
  FPrDs.DataSet := nil;

  if Assigned( FPrListData ) then
    FreeAndNil( FPrListData );
  if Assigned( FPrCheckListData ) then
    FreeAndNil( FPrCheckListData );
  FreeAndNil( FPrDs );
  inherited;
end;

function TEvoCoPrList.GetPrCheckList: TkbmCustomMemTable;
begin
  Result := FPrCheckListData;
end;

function TEvoCoPrList.GetPrList: TkbmCustomMemTable;
begin
  Result := FPrListData;
end;

procedure TEvoCoPrList.LoadPayrollsChecks(aDat_b, aDat_e: TDatetime);
var
  Params: IRpcStruct;
begin
  Params := TRpcStruct.Create;
  StartWait('Load Payrolls/Checks List...');
  try
    FEvoAPI.OpenClient( FCoListData.FieldByName('CL_NBR').AsInteger );
    Params.AddItem('CoNbr', FCoListData.FieldByName('CO_NBR').AsInteger);
    Params.AddItemDateTime('dat_b', aDat_b);
    Params.AddItemDateTime('dat_e', aDat_e);

    if Assigned( FPrCheckListData ) then
      FreeAndNil( FPrCheckListData );
    if Assigned( FPrListData ) then
      FreeAndNil( FPrListData );

    FPrListData := RunQuery('PrList.rwq', Params);
    FPrCheckListData := RunQuery('PrCheckList.rwq', Params);
    FPrDS.DataSet := FPrListData;

    FPrCheckListData.MasterSource := FPrDs;
    FPrCheckListData.MasterFields := 'PR_NBR';
    FPrCheckListData.DetailFields := 'PR_NBR';
  finally
    StopWait;
    Params := nil;
  end
end;

{ TEvoCoEeList }

destructor TEvoCoEeList.Destroy;
begin
  if Assigned( FEeListData ) then
    FreeAndNil( FEeListData );
  inherited;
end;

function TEvoCoEeList.GetEeList: TkbmCustomMemTable;
begin
  Result := FEeListData;
end;

procedure TEvoCoEeList.LoadEmployees;
var
  Params: IRpcStruct;
begin
  Params := TRpcStruct.Create;
  StartWait('Load Employee List...');
  try
    FEvoAPI.OpenClient( FCoListData.FieldByName('CL_NBR').AsInteger );
    Params.AddItem('CoNbr', FCoListData.FieldByName('CO_NBR').AsInteger);

    if Assigned( FEeListData ) then
      FreeAndNil( FEeListData );

    FEeListData := RunQuery('EeList.rwq', Params);
  finally
    StopWait;
    Params := nil;
  end
end;

procedure TEvoCoEeList.LoadEmployees(Cl_Nbr, Co_Nbr: integer);
var
  Params: IRpcStruct;
begin
  Params := TRpcStruct.Create;
  StartWait('Load Employee List...');
  try
    FEvoAPI.OpenClient( Cl_Nbr );
    Params.AddItem('CoNbr', Co_Nbr);

    if Assigned( FEeListData ) then
      FreeAndNil( FEeListData );

    FEeListData := RunQuery('EeList.rwq', Params);
  finally
    StopWait;
    Params := nil;
  end
end;

{ TEvoCoList }

destructor TEvoCoList.Destroy;
begin
  if Assigned( FCoListData ) then
    FreeAndNil( FCoListData );
  inherited;
end;

function TEvoCoList.GetCoList(const aNumber,
  aName: string): TkbmCustomMemTable;
var
  Params: IRpcStruct;
begin
  if Assigned(FCoListData) then
  begin
    FCoListData.Close;
    FreeAndNil(FCoListData);
  end;

  Params := TRpcStruct.Create;
  StartWait('Search for a Company...');
  try
    Params.AddItem('CoNumber', aNumber);
    Params.AddItem('CoName', aName);
    FCoListData := RunQuery('TMP_CO.rwq', Params);
  finally
    StopWait;
    Params := nil;
  end;
  Result := FCoListData;
end;

{ THolder }

constructor THolder.Create(Logger: ICommonLogger;
  EvoAPI: IEvoAPIConnection);
begin
  FLogger := Logger;
  FEvoAPI := EvoAPI;
end;

function THolder.RunQuery(aFileName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  try
    Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + aFileName, params);
  except
    on e: exception do
    begin
      FLogger.LogError(aFileName, e.Message);
      raise Exception.Create('Query file: ' + aFileName + '. ' + e.Message);
    end
  end;
end;

end.
