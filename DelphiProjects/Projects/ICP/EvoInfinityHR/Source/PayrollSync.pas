// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : https://www.infinityhrconnect.com/services/payrollsync.asmx?wsdl
//  >Import : https://www.infinityhrconnect.com/services/payrollsync.asmx?wsdl:0
// Encoding : utf-8
// Version  : 1.0
// (8/25/2015 10:45:42  - - $Rev: 10138 $)
// ************************************************************************ //

unit payrollsync;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:double          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:long            - "http://www.w3.org/2001/XMLSchema"[Gbl]

  AuthRequest          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  AuthResponse         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  EmployeeRequest      = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  CoverageInfo         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  EmployeeInfo         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Deduction            = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Compensation         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  DirectDeposit        = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  StateTax             = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  FederalTax           = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  EmployeeData         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  EmployeeInfoSkinny   = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  FieldInfo            = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PaystubRequest       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PaystubFile          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PaystubDataRequest   = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PaystubData          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Paystub_TimeOff      = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Paystub_Tax          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Paystub_Summary      = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Paystub_Detail       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  TimeOffBalanceRequest = class;                { "http://www.infinity-ss.com/services"[GblCplx] }
  AccrualBalance       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  W2Request            = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  W2File               = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  BenefitRequest       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  BenefitStructure     = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  BenefitStructureItem = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  LookupData           = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  Lookup               = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PayScheduleData      = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PaySchedule          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  CompanyRequest       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  CostCenterData       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  CostCenterInfo       = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  CompanyData          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  CompanyInfo          = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  PayrollServiceException = class;              { "http://www.infinity-ss.com/services"[GblCplx] }
  AuditRequest         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }
  ErrorRequest         = class;                 { "http://www.infinity-ss.com/services"[GblCplx] }

  { "http://www.infinity-ss.com/services"[GblSmpl] }
  FieldChangeType = (Add, Change, Delete, General);



  // ************************************************************************ //
  // XML       : AuthRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AuthRequest = class(TRemotable)
  private
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FVendorPwd: WideString;
    FVendorPwd_Specified: boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetVendorPwd(Index: Integer; const AWideString: WideString);
    function  VendorPwd_Specified(Index: Integer): boolean;
  published
    property VendorID:  WideString  Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property VendorPwd: WideString  Index (IS_OPTN) read FVendorPwd write SetVendorPwd stored VendorPwd_Specified;
  end;



  // ************************************************************************ //
  // XML       : AuthResponse, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AuthResponse = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FIsVendorActive: Boolean;
    FSessionStart: TXSDateTime;
    FSessionEnd: TXSDateTime;
    FSyncDate: TXSDateTime;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property SessionID:      WideString   Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property IsVendorActive: Boolean      read FIsVendorActive write FIsVendorActive;
    property SessionStart:   TXSDateTime  read FSessionStart write FSessionStart;
    property SessionEnd:     TXSDateTime  read FSessionEnd write FSessionEnd;
    property SyncDate:       TXSDateTime  read FSyncDate write FSyncDate;
  end;



  // ************************************************************************ //
  // XML       : EmployeeRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FShowAllDeductions: Boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
  published
    property SessionID:         WideString  Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:          WideString  Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property ShowAllDeductions: Boolean     read FShowAllDeductions write FShowAllDeductions;
  end;

  ArrayOfEmployeeInfo = array of EmployeeInfo;   { "http://www.infinity-ss.com/services"[GblCplx] }
  ArrayOfDeduction = array of Deduction;        { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : CoverageInfo, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CoverageInfo = class(TRemotable)
  private
    FPackage_ID: Integer;
    FBenefit_ID: Integer;
    FPlan_ID: Integer;
    FOption_ID: Integer;
    FPackageName: WideString;
    FPackageName_Specified: boolean;
    FBenefitName: WideString;
    FBenefitName_Specified: boolean;
    FPlanName: WideString;
    FPlanName_Specified: boolean;
    FOptionName: WideString;
    FOptionName_Specified: boolean;
    FPackageEffectiveDate: TXSDateTime;
    FPackageExpirationDate: TXSDateTime;
    FBenefit_EffectiveDate: TXSDateTime;
    FBenefit_ExpirationDate: TXSDateTime;
    FPremiumAmount: TXSDecimal;
    FCreditAmount: TXSDecimal;
    FEmployeeCost: TXSDecimal;
    FHSACredit: TXSDecimal;
    FEmployerCoveragePercent: TXSDecimal;
    FAllEarnings: TXSDecimal;
    FRateOfPay: TXSDecimal;
    FAllEarningsTestResult: TXSDecimal;
    FRateOfPayTestResult: TXSDecimal;
    FIsAffordable_AllEarnings: Boolean;
    FIsAffordable_RateOfPay: Boolean;
    procedure SetPackageName(Index: Integer; const AWideString: WideString);
    function  PackageName_Specified(Index: Integer): boolean;
    procedure SetBenefitName(Index: Integer; const AWideString: WideString);
    function  BenefitName_Specified(Index: Integer): boolean;
    procedure SetPlanName(Index: Integer; const AWideString: WideString);
    function  PlanName_Specified(Index: Integer): boolean;
    procedure SetOptionName(Index: Integer; const AWideString: WideString);
    function  OptionName_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Package_ID:               Integer      read FPackage_ID write FPackage_ID;
    property Benefit_ID:               Integer      read FBenefit_ID write FBenefit_ID;
    property Plan_ID:                  Integer      read FPlan_ID write FPlan_ID;
    property Option_ID:                Integer      read FOption_ID write FOption_ID;
    property PackageName:              WideString   Index (IS_OPTN) read FPackageName write SetPackageName stored PackageName_Specified;
    property BenefitName:              WideString   Index (IS_OPTN) read FBenefitName write SetBenefitName stored BenefitName_Specified;
    property PlanName:                 WideString   Index (IS_OPTN) read FPlanName write SetPlanName stored PlanName_Specified;
    property OptionName:               WideString   Index (IS_OPTN) read FOptionName write SetOptionName stored OptionName_Specified;
    property PackageEffectiveDate:     TXSDateTime  read FPackageEffectiveDate write FPackageEffectiveDate;
    property PackageExpirationDate:    TXSDateTime  read FPackageExpirationDate write FPackageExpirationDate;
    property Benefit_EffectiveDate:    TXSDateTime  read FBenefit_EffectiveDate write FBenefit_EffectiveDate;
    property Benefit_ExpirationDate:   TXSDateTime  read FBenefit_ExpirationDate write FBenefit_ExpirationDate;
    property PremiumAmount:            TXSDecimal   read FPremiumAmount write FPremiumAmount;
    property CreditAmount:             TXSDecimal   read FCreditAmount write FCreditAmount;
    property EmployeeCost:             TXSDecimal   read FEmployeeCost write FEmployeeCost;
    property HSACredit:                TXSDecimal   read FHSACredit write FHSACredit;
    property EmployerCoveragePercent:  TXSDecimal   read FEmployerCoveragePercent write FEmployerCoveragePercent;
    property AllEarnings:              TXSDecimal   read FAllEarnings write FAllEarnings;
    property RateOfPay:                TXSDecimal   read FRateOfPay write FRateOfPay;
    property AllEarningsTestResult:    TXSDecimal   read FAllEarningsTestResult write FAllEarningsTestResult;
    property RateOfPayTestResult:      TXSDecimal   read FRateOfPayTestResult write FRateOfPayTestResult;
    property IsAffordable_AllEarnings: Boolean      read FIsAffordable_AllEarnings write FIsAffordable_AllEarnings;
    property IsAffordable_RateOfPay:   Boolean      read FIsAffordable_RateOfPay write FIsAffordable_RateOfPay;
  end;

  ArrayOfCompensation = array of Compensation;   { "http://www.infinity-ss.com/services"[GblCplx] }
  ArrayOfDirectDeposit = array of DirectDeposit;   { "http://www.infinity-ss.com/services"[GblCplx] }
  ArrayOfStateTax = array of StateTax;          { "http://www.infinity-ss.com/services"[GblCplx] }
  ArrayOfFederalTax = array of FederalTax;      { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : EmployeeInfo, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeInfo = class(TRemotable)
  private
    FInfinityHREmployeeID: Integer;
    FPayrollEmployeeID: WideString;
    FPayrollEmployeeID_Specified: boolean;
    FCompanyID: Integer;
    FIsAdmin: Boolean;
    FVendorCompany_ID: WideString;
    FVendorCompany_ID_Specified: boolean;
    FLastName: WideString;
    FLastName_Specified: boolean;
    FFirstName: WideString;
    FFirstName_Specified: boolean;
    FMiddleName: WideString;
    FMiddleName_Specified: boolean;
    FUserName: WideString;
    FUserName_Specified: boolean;
    FGender: WideString;
    FGender_Specified: boolean;
    FTobaccoUser: Boolean;
    FEthnicity: WideString;
    FEthnicity_Specified: boolean;
    FOccupation: WideString;
    FOccupation_Specified: boolean;
    FMaritalStatus: WideString;
    FMaritalStatus_Specified: boolean;
    FAddress1: WideString;
    FAddress1_Specified: boolean;
    FAddress2: WideString;
    FAddress2_Specified: boolean;
    FCity: WideString;
    FCity_Specified: boolean;
    FHomePhone: WideString;
    FHomePhone_Specified: boolean;
    FState: WideString;
    FState_Specified: boolean;
    FStateCode: WideString;
    FStateCode_Specified: boolean;
    FZip: WideString;
    FZip_Specified: boolean;
    FSSN: WideString;
    FSSN_Specified: boolean;
    FBirthDate: TXSDateTime;
    FAutoPay: Boolean;
    FCostCenter1: WideString;
    FCostCenter1_Specified: boolean;
    FCostCenter2: WideString;
    FCostCenter2_Specified: boolean;
    FCostCenter3: WideString;
    FCostCenter3_Specified: boolean;
    FCostCenter4: WideString;
    FCostCenter4_Specified: boolean;
    FCostCenter5: WideString;
    FCostCenter5_Specified: boolean;
    FDefaultHours: TXSDecimal;
    FEmployeeStatus: WideString;
    FEmployeeStatus_Specified: boolean;
    FEmployeeStatus_ACA: WideString;
    FEmployeeStatus_ACA_Specified: boolean;
    FEmployeeStatus_ACA_Code: WideString;
    FEmployeeStatus_ACA_Code_Specified: boolean;
    FAdditionalHouseholdIncome: TXSDecimal;
    FCountry: WideString;
    FCountry_Specified: boolean;
    FBenefitStatus: WideString;
    FBenefitStatus_Specified: boolean;
    FDepartment: WideString;
    FDepartment_Specified: boolean;
    FHireDate: TXSDateTime;
    FOrigHireDate: TXSDateTime;
    FAlternateServiceDate1: TXSDateTime;
    FAlternateServiceDate2: TXSDateTime;
    FRetirementDate: TXSDateTime;
    FWorkPhone: WideString;
    FWorkPhone_Specified: boolean;
    FWorkFax: WideString;
    FWorkFax_Specified: boolean;
    FMobilePhone: WideString;
    FMobilePhone_Specified: boolean;
    FHoursWorked: Double;
    FPayFrequency: WideString;
    FPayFrequency_Specified: boolean;
    FSalary: TXSDecimal;
    FTaxForm: WideString;
    FTaxForm_Specified: boolean;
    FWorkersCompCode: WideString;
    FWorkersCompCode_Specified: boolean;
    FEmployeeType: WideString;
    FEmployeeType_Specified: boolean;
    FReHireDate: TXSDateTime;
    FTerminationDate: TXSDateTime;
    FJobCode: WideString;
    FJobCode_Specified: boolean;
    FTerminationReason: WideString;
    FTerminationReason_Specified: boolean;
    FFacility1: WideString;
    FFacility1_Specified: boolean;
    FFacility2: WideString;
    FFacility2_Specified: boolean;
    FManager: Boolean;
    FPTOManager: WideString;
    FPTOManager_Specified: boolean;
    FPTOManager_ID: Integer;
    FTAManager_ID: Integer;
    FPAManager: WideString;
    FPAManager_Specified: boolean;
    FPaySchedule_Master: WideString;
    FPaySchedule_Master_Specified: boolean;
    FEmployee_UserDefined1: WideString;
    FEmployee_UserDefined1_Specified: boolean;
    FEmployee_UserDefined2: WideString;
    FEmployee_UserDefined2_Specified: boolean;
    FEmployee_UserDefined3: WideString;
    FEmployee_UserDefined3_Specified: boolean;
    FEmployee_UserDefined4: WideString;
    FEmployee_UserDefined4_Specified: boolean;
    FEmployee_UserDefined5: WideString;
    FEmployee_UserDefined5_Specified: boolean;
    FSchedule: WideString;
    FSchedule_Specified: boolean;
    FPWD: WideString;
    FPWD_Specified: boolean;
    FBaseRate: WideString;
    FBaseRate_Specified: boolean;
    FPension: WideString;
    FPension_Specified: boolean;
    FHighCompensation: WideString;
    FHighCompensation_Specified: boolean;
    FIsOwner: WideString;
    FIsOwner_Specified: boolean;
    FUserDefinedLookup1: WideString;
    FUserDefinedLookup1_Specified: boolean;
    FUserDefinedLookup2: WideString;
    FUserDefinedLookup2_Specified: boolean;
    FUserDefinedLookup3: WideString;
    FUserDefinedLookup3_Specified: boolean;
    FUserDefinedLookup4: WideString;
    FUserDefinedLookup4_Specified: boolean;
    FUserDefinedLookup5: WideString;
    FUserDefinedLookup5_Specified: boolean;
    FUserDefinedLookup6: WideString;
    FUserDefinedLookup6_Specified: boolean;
    FUserDefinedLookup7: WideString;
    FUserDefinedLookup7_Specified: boolean;
    FUserDefinedLookup8: WideString;
    FUserDefinedLookup8_Specified: boolean;
    FUserDefinedLookup9: WideString;
    FUserDefinedLookup9_Specified: boolean;
    FUserDefinedLookup10: WideString;
    FUserDefinedLookup10_Specified: boolean;
    FUserDefinedLookup11: WideString;
    FUserDefinedLookup11_Specified: boolean;
    FUserDefinedLookup12: WideString;
    FUserDefinedLookup12_Specified: boolean;
    FUserDefinedLookup13: WideString;
    FUserDefinedLookup13_Specified: boolean;
    FUserDefinedLookup14: WideString;
    FUserDefinedLookup14_Specified: boolean;
    FUserDefinedLookup15: WideString;
    FUserDefinedLookup15_Specified: boolean;
    FWorkAddress1: WideString;
    FWorkAddress1_Specified: boolean;
    FWorkAddress2: WideString;
    FWorkAddress2_Specified: boolean;
    FWorkEmail: WideString;
    FWorkEmail_Specified: boolean;
    FHomeEmail: WideString;
    FHomeEmail_Specified: boolean;
    FBenefitServiceDate: TXSDateTime;
    FEmpStatusEffDate: TXSDateTime;
    FCompensationCategory: WideString;
    FCompensationCategory_Specified: boolean;
    FHourlyRate: Double;
    FEEOClass: WideString;
    FEEOClass_Specified: boolean;
    FEEOClassDescription: WideString;
    FEEOClassDescription_Specified: boolean;
    FOTExempt: Boolean;
    FVisaExpiration: TXSDateTime;
    FVisaType: WideString;
    FVisaType_Specified: boolean;
    FIsVeteran: Boolean;
    FNickName: WideString;
    FNickName_Specified: boolean;
    FDeductions: ArrayOfDeduction;
    FDeductions_Specified: boolean;
    FACA_LowestCost_Coverage: CoverageInfo;
    FACA_LowestCost_Coverage_Specified: boolean;
    FCompensations: ArrayOfCompensation;
    FCompensations_Specified: boolean;
    FDirectDeposits: ArrayOfDirectDeposit;
    FDirectDeposits_Specified: boolean;
    FStateTax: ArrayOfStateTax;
    FStateTax_Specified: boolean;
    FFederalTax: ArrayOfFederalTax;
    FFederalTax_Specified: boolean;
    FHandicapped: Boolean;
    FFullTimeStudent: Boolean;
    FPartTimeStudent: Boolean;
    FShowAllDeductions: Boolean;
    FPayScheduleName: WideString;
    FPayScheduleName_Specified: boolean;
    procedure SetPayrollEmployeeID(Index: Integer; const AWideString: WideString);
    function  PayrollEmployeeID_Specified(Index: Integer): boolean;
    procedure SetVendorCompany_ID(Index: Integer; const AWideString: WideString);
    function  VendorCompany_ID_Specified(Index: Integer): boolean;
    procedure SetLastName(Index: Integer; const AWideString: WideString);
    function  LastName_Specified(Index: Integer): boolean;
    procedure SetFirstName(Index: Integer; const AWideString: WideString);
    function  FirstName_Specified(Index: Integer): boolean;
    procedure SetMiddleName(Index: Integer; const AWideString: WideString);
    function  MiddleName_Specified(Index: Integer): boolean;
    procedure SetUserName(Index: Integer; const AWideString: WideString);
    function  UserName_Specified(Index: Integer): boolean;
    procedure SetGender(Index: Integer; const AWideString: WideString);
    function  Gender_Specified(Index: Integer): boolean;
    procedure SetEthnicity(Index: Integer; const AWideString: WideString);
    function  Ethnicity_Specified(Index: Integer): boolean;
    procedure SetOccupation(Index: Integer; const AWideString: WideString);
    function  Occupation_Specified(Index: Integer): boolean;
    procedure SetMaritalStatus(Index: Integer; const AWideString: WideString);
    function  MaritalStatus_Specified(Index: Integer): boolean;
    procedure SetAddress1(Index: Integer; const AWideString: WideString);
    function  Address1_Specified(Index: Integer): boolean;
    procedure SetAddress2(Index: Integer; const AWideString: WideString);
    function  Address2_Specified(Index: Integer): boolean;
    procedure SetCity(Index: Integer; const AWideString: WideString);
    function  City_Specified(Index: Integer): boolean;
    procedure SetHomePhone(Index: Integer; const AWideString: WideString);
    function  HomePhone_Specified(Index: Integer): boolean;
    procedure SetState(Index: Integer; const AWideString: WideString);
    function  State_Specified(Index: Integer): boolean;
    procedure SetStateCode(Index: Integer; const AWideString: WideString);
    function  StateCode_Specified(Index: Integer): boolean;
    procedure SetZip(Index: Integer; const AWideString: WideString);
    function  Zip_Specified(Index: Integer): boolean;
    procedure SetSSN(Index: Integer; const AWideString: WideString);
    function  SSN_Specified(Index: Integer): boolean;
    procedure SetCostCenter1(Index: Integer; const AWideString: WideString);
    function  CostCenter1_Specified(Index: Integer): boolean;
    procedure SetCostCenter2(Index: Integer; const AWideString: WideString);
    function  CostCenter2_Specified(Index: Integer): boolean;
    procedure SetCostCenter3(Index: Integer; const AWideString: WideString);
    function  CostCenter3_Specified(Index: Integer): boolean;
    procedure SetCostCenter4(Index: Integer; const AWideString: WideString);
    function  CostCenter4_Specified(Index: Integer): boolean;
    procedure SetCostCenter5(Index: Integer; const AWideString: WideString);
    function  CostCenter5_Specified(Index: Integer): boolean;
    procedure SetEmployeeStatus(Index: Integer; const AWideString: WideString);
    function  EmployeeStatus_Specified(Index: Integer): boolean;
    procedure SetEmployeeStatus_ACA(Index: Integer; const AWideString: WideString);
    function  EmployeeStatus_ACA_Specified(Index: Integer): boolean;
    procedure SetEmployeeStatus_ACA_Code(Index: Integer; const AWideString: WideString);
    function  EmployeeStatus_ACA_Code_Specified(Index: Integer): boolean;
    procedure SetCountry(Index: Integer; const AWideString: WideString);
    function  Country_Specified(Index: Integer): boolean;
    procedure SetBenefitStatus(Index: Integer; const AWideString: WideString);
    function  BenefitStatus_Specified(Index: Integer): boolean;
    procedure SetDepartment(Index: Integer; const AWideString: WideString);
    function  Department_Specified(Index: Integer): boolean;
    procedure SetWorkPhone(Index: Integer; const AWideString: WideString);
    function  WorkPhone_Specified(Index: Integer): boolean;
    procedure SetWorkFax(Index: Integer; const AWideString: WideString);
    function  WorkFax_Specified(Index: Integer): boolean;
    procedure SetMobilePhone(Index: Integer; const AWideString: WideString);
    function  MobilePhone_Specified(Index: Integer): boolean;
    procedure SetPayFrequency(Index: Integer; const AWideString: WideString);
    function  PayFrequency_Specified(Index: Integer): boolean;
    procedure SetTaxForm(Index: Integer; const AWideString: WideString);
    function  TaxForm_Specified(Index: Integer): boolean;
    procedure SetWorkersCompCode(Index: Integer; const AWideString: WideString);
    function  WorkersCompCode_Specified(Index: Integer): boolean;
    procedure SetEmployeeType(Index: Integer; const AWideString: WideString);
    function  EmployeeType_Specified(Index: Integer): boolean;
    procedure SetJobCode(Index: Integer; const AWideString: WideString);
    function  JobCode_Specified(Index: Integer): boolean;
    procedure SetTerminationReason(Index: Integer; const AWideString: WideString);
    function  TerminationReason_Specified(Index: Integer): boolean;
    procedure SetFacility1(Index: Integer; const AWideString: WideString);
    function  Facility1_Specified(Index: Integer): boolean;
    procedure SetFacility2(Index: Integer; const AWideString: WideString);
    function  Facility2_Specified(Index: Integer): boolean;
    procedure SetPTOManager(Index: Integer; const AWideString: WideString);
    function  PTOManager_Specified(Index: Integer): boolean;
    procedure SetPAManager(Index: Integer; const AWideString: WideString);
    function  PAManager_Specified(Index: Integer): boolean;
    procedure SetPaySchedule_Master(Index: Integer; const AWideString: WideString);
    function  PaySchedule_Master_Specified(Index: Integer): boolean;
    procedure SetEmployee_UserDefined1(Index: Integer; const AWideString: WideString);
    function  Employee_UserDefined1_Specified(Index: Integer): boolean;
    procedure SetEmployee_UserDefined2(Index: Integer; const AWideString: WideString);
    function  Employee_UserDefined2_Specified(Index: Integer): boolean;
    procedure SetEmployee_UserDefined3(Index: Integer; const AWideString: WideString);
    function  Employee_UserDefined3_Specified(Index: Integer): boolean;
    procedure SetEmployee_UserDefined4(Index: Integer; const AWideString: WideString);
    function  Employee_UserDefined4_Specified(Index: Integer): boolean;
    procedure SetEmployee_UserDefined5(Index: Integer; const AWideString: WideString);
    function  Employee_UserDefined5_Specified(Index: Integer): boolean;
    procedure SetSchedule(Index: Integer; const AWideString: WideString);
    function  Schedule_Specified(Index: Integer): boolean;
    procedure SetPWD(Index: Integer; const AWideString: WideString);
    function  PWD_Specified(Index: Integer): boolean;
    procedure SetBaseRate(Index: Integer; const AWideString: WideString);
    function  BaseRate_Specified(Index: Integer): boolean;
    procedure SetPension(Index: Integer; const AWideString: WideString);
    function  Pension_Specified(Index: Integer): boolean;
    procedure SetHighCompensation(Index: Integer; const AWideString: WideString);
    function  HighCompensation_Specified(Index: Integer): boolean;
    procedure SetIsOwner(Index: Integer; const AWideString: WideString);
    function  IsOwner_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup1(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup1_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup2(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup2_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup3(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup3_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup4(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup4_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup5(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup5_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup6(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup6_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup7(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup7_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup8(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup8_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup9(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup9_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup10(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup10_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup11(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup11_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup12(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup12_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup13(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup13_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup14(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup14_Specified(Index: Integer): boolean;
    procedure SetUserDefinedLookup15(Index: Integer; const AWideString: WideString);
    function  UserDefinedLookup15_Specified(Index: Integer): boolean;
    procedure SetWorkAddress1(Index: Integer; const AWideString: WideString);
    function  WorkAddress1_Specified(Index: Integer): boolean;
    procedure SetWorkAddress2(Index: Integer; const AWideString: WideString);
    function  WorkAddress2_Specified(Index: Integer): boolean;
    procedure SetWorkEmail(Index: Integer; const AWideString: WideString);
    function  WorkEmail_Specified(Index: Integer): boolean;
    procedure SetHomeEmail(Index: Integer; const AWideString: WideString);
    function  HomeEmail_Specified(Index: Integer): boolean;
    procedure SetCompensationCategory(Index: Integer; const AWideString: WideString);
    function  CompensationCategory_Specified(Index: Integer): boolean;
    procedure SetEEOClass(Index: Integer; const AWideString: WideString);
    function  EEOClass_Specified(Index: Integer): boolean;
    procedure SetEEOClassDescription(Index: Integer; const AWideString: WideString);
    function  EEOClassDescription_Specified(Index: Integer): boolean;
    procedure SetVisaType(Index: Integer; const AWideString: WideString);
    function  VisaType_Specified(Index: Integer): boolean;
    procedure SetNickName(Index: Integer; const AWideString: WideString);
    function  NickName_Specified(Index: Integer): boolean;
    procedure SetDeductions(Index: Integer; const AArrayOfDeduction: ArrayOfDeduction);
    function  Deductions_Specified(Index: Integer): boolean;
    procedure SetACA_LowestCost_Coverage(Index: Integer; const ACoverageInfo: CoverageInfo);
    function  ACA_LowestCost_Coverage_Specified(Index: Integer): boolean;
    procedure SetCompensations(Index: Integer; const AArrayOfCompensation: ArrayOfCompensation);
    function  Compensations_Specified(Index: Integer): boolean;
    procedure SetDirectDeposits(Index: Integer; const AArrayOfDirectDeposit: ArrayOfDirectDeposit);
    function  DirectDeposits_Specified(Index: Integer): boolean;
    procedure SetStateTax(Index: Integer; const AArrayOfStateTax: ArrayOfStateTax);
    function  StateTax_Specified(Index: Integer): boolean;
    procedure SetFederalTax(Index: Integer; const AArrayOfFederalTax: ArrayOfFederalTax);
    function  FederalTax_Specified(Index: Integer): boolean;
    procedure SetPayScheduleName(Index: Integer; const AWideString: WideString);
    function  PayScheduleName_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property InfinityHREmployeeID:      Integer               read FInfinityHREmployeeID write FInfinityHREmployeeID;
    property PayrollEmployeeID:         WideString            Index (IS_OPTN) read FPayrollEmployeeID write SetPayrollEmployeeID stored PayrollEmployeeID_Specified;
    property CompanyID:                 Integer               read FCompanyID write FCompanyID;
    property IsAdmin:                   Boolean               read FIsAdmin write FIsAdmin;
    property VendorCompany_ID:          WideString            Index (IS_OPTN) read FVendorCompany_ID write SetVendorCompany_ID stored VendorCompany_ID_Specified;
    property LastName:                  WideString            Index (IS_OPTN) read FLastName write SetLastName stored LastName_Specified;
    property FirstName:                 WideString            Index (IS_OPTN) read FFirstName write SetFirstName stored FirstName_Specified;
    property MiddleName:                WideString            Index (IS_OPTN) read FMiddleName write SetMiddleName stored MiddleName_Specified;
    property UserName:                  WideString            Index (IS_OPTN) read FUserName write SetUserName stored UserName_Specified;
    property Gender:                    WideString            Index (IS_OPTN) read FGender write SetGender stored Gender_Specified;
    property TobaccoUser:               Boolean               read FTobaccoUser write FTobaccoUser;
    property Ethnicity:                 WideString            Index (IS_OPTN) read FEthnicity write SetEthnicity stored Ethnicity_Specified;
    property Occupation:                WideString            Index (IS_OPTN) read FOccupation write SetOccupation stored Occupation_Specified;
    property MaritalStatus:             WideString            Index (IS_OPTN) read FMaritalStatus write SetMaritalStatus stored MaritalStatus_Specified;
    property Address1:                  WideString            Index (IS_OPTN) read FAddress1 write SetAddress1 stored Address1_Specified;
    property Address2:                  WideString            Index (IS_OPTN) read FAddress2 write SetAddress2 stored Address2_Specified;
    property City:                      WideString            Index (IS_OPTN) read FCity write SetCity stored City_Specified;
    property HomePhone:                 WideString            Index (IS_OPTN) read FHomePhone write SetHomePhone stored HomePhone_Specified;
    property State:                     WideString            Index (IS_OPTN) read FState write SetState stored State_Specified;
    property StateCode:                 WideString            Index (IS_OPTN) read FStateCode write SetStateCode stored StateCode_Specified;
    property Zip:                       WideString            Index (IS_OPTN) read FZip write SetZip stored Zip_Specified;
    property SSN:                       WideString            Index (IS_OPTN) read FSSN write SetSSN stored SSN_Specified;
    property BirthDate:                 TXSDateTime           read FBirthDate write FBirthDate;
    property AutoPay:                   Boolean               read FAutoPay write FAutoPay;
    property CostCenter1:               WideString            Index (IS_OPTN) read FCostCenter1 write SetCostCenter1 stored CostCenter1_Specified;
    property CostCenter2:               WideString            Index (IS_OPTN) read FCostCenter2 write SetCostCenter2 stored CostCenter2_Specified;
    property CostCenter3:               WideString            Index (IS_OPTN) read FCostCenter3 write SetCostCenter3 stored CostCenter3_Specified;
    property CostCenter4:               WideString            Index (IS_OPTN) read FCostCenter4 write SetCostCenter4 stored CostCenter4_Specified;
    property CostCenter5:               WideString            Index (IS_OPTN) read FCostCenter5 write SetCostCenter5 stored CostCenter5_Specified;
    property DefaultHours:              TXSDecimal            read FDefaultHours write FDefaultHours;
    property EmployeeStatus:            WideString            Index (IS_OPTN) read FEmployeeStatus write SetEmployeeStatus stored EmployeeStatus_Specified;
    property EmployeeStatus_ACA:        WideString            Index (IS_OPTN) read FEmployeeStatus_ACA write SetEmployeeStatus_ACA stored EmployeeStatus_ACA_Specified;
    property EmployeeStatus_ACA_Code:   WideString            Index (IS_OPTN) read FEmployeeStatus_ACA_Code write SetEmployeeStatus_ACA_Code stored EmployeeStatus_ACA_Code_Specified;
    property AdditionalHouseholdIncome: TXSDecimal            read FAdditionalHouseholdIncome write FAdditionalHouseholdIncome;
    property Country:                   WideString            Index (IS_OPTN) read FCountry write SetCountry stored Country_Specified;
    property BenefitStatus:             WideString            Index (IS_OPTN) read FBenefitStatus write SetBenefitStatus stored BenefitStatus_Specified;
    property Department:                WideString            Index (IS_OPTN) read FDepartment write SetDepartment stored Department_Specified;
    property HireDate:                  TXSDateTime           read FHireDate write FHireDate;
    property OrigHireDate:              TXSDateTime           read FOrigHireDate write FOrigHireDate;
    property AlternateServiceDate1:     TXSDateTime           read FAlternateServiceDate1 write FAlternateServiceDate1;
    property AlternateServiceDate2:     TXSDateTime           read FAlternateServiceDate2 write FAlternateServiceDate2;
    property RetirementDate:            TXSDateTime           read FRetirementDate write FRetirementDate;
    property WorkPhone:                 WideString            Index (IS_OPTN) read FWorkPhone write SetWorkPhone stored WorkPhone_Specified;
    property WorkFax:                   WideString            Index (IS_OPTN) read FWorkFax write SetWorkFax stored WorkFax_Specified;
    property MobilePhone:               WideString            Index (IS_OPTN) read FMobilePhone write SetMobilePhone stored MobilePhone_Specified;
    property HoursWorked:               Double                read FHoursWorked write FHoursWorked;
    property PayFrequency:              WideString            Index (IS_OPTN) read FPayFrequency write SetPayFrequency stored PayFrequency_Specified;
    property Salary:                    TXSDecimal            read FSalary write FSalary;
    property TaxForm:                   WideString            Index (IS_OPTN) read FTaxForm write SetTaxForm stored TaxForm_Specified;
    property WorkersCompCode:           WideString            Index (IS_OPTN) read FWorkersCompCode write SetWorkersCompCode stored WorkersCompCode_Specified;
    property EmployeeType:              WideString            Index (IS_OPTN) read FEmployeeType write SetEmployeeType stored EmployeeType_Specified;
    property ReHireDate:                TXSDateTime           read FReHireDate write FReHireDate;
    property TerminationDate:           TXSDateTime           read FTerminationDate write FTerminationDate;
    property JobCode:                   WideString            Index (IS_OPTN) read FJobCode write SetJobCode stored JobCode_Specified;
    property TerminationReason:         WideString            Index (IS_OPTN) read FTerminationReason write SetTerminationReason stored TerminationReason_Specified;
    property Facility1:                 WideString            Index (IS_OPTN) read FFacility1 write SetFacility1 stored Facility1_Specified;
    property Facility2:                 WideString            Index (IS_OPTN) read FFacility2 write SetFacility2 stored Facility2_Specified;
    property Manager:                   Boolean               read FManager write FManager;
    property PTOManager:                WideString            Index (IS_OPTN) read FPTOManager write SetPTOManager stored PTOManager_Specified;
    property PTOManager_ID:             Integer               read FPTOManager_ID write FPTOManager_ID;
    property TAManager_ID:              Integer               read FTAManager_ID write FTAManager_ID;
    property PAManager:                 WideString            Index (IS_OPTN) read FPAManager write SetPAManager stored PAManager_Specified;
    property PaySchedule_Master:        WideString            Index (IS_OPTN) read FPaySchedule_Master write SetPaySchedule_Master stored PaySchedule_Master_Specified;
    property Employee_UserDefined1:     WideString            Index (IS_OPTN) read FEmployee_UserDefined1 write SetEmployee_UserDefined1 stored Employee_UserDefined1_Specified;
    property Employee_UserDefined2:     WideString            Index (IS_OPTN) read FEmployee_UserDefined2 write SetEmployee_UserDefined2 stored Employee_UserDefined2_Specified;
    property Employee_UserDefined3:     WideString            Index (IS_OPTN) read FEmployee_UserDefined3 write SetEmployee_UserDefined3 stored Employee_UserDefined3_Specified;
    property Employee_UserDefined4:     WideString            Index (IS_OPTN) read FEmployee_UserDefined4 write SetEmployee_UserDefined4 stored Employee_UserDefined4_Specified;
    property Employee_UserDefined5:     WideString            Index (IS_OPTN) read FEmployee_UserDefined5 write SetEmployee_UserDefined5 stored Employee_UserDefined5_Specified;
    property Schedule:                  WideString            Index (IS_OPTN) read FSchedule write SetSchedule stored Schedule_Specified;
    property PWD:                       WideString            Index (IS_OPTN) read FPWD write SetPWD stored PWD_Specified;
    property BaseRate:                  WideString            Index (IS_OPTN) read FBaseRate write SetBaseRate stored BaseRate_Specified;
    property Pension:                   WideString            Index (IS_OPTN) read FPension write SetPension stored Pension_Specified;
    property HighCompensation:          WideString            Index (IS_OPTN) read FHighCompensation write SetHighCompensation stored HighCompensation_Specified;
    property IsOwner:                   WideString            Index (IS_OPTN) read FIsOwner write SetIsOwner stored IsOwner_Specified;
    property UserDefinedLookup1:        WideString            Index (IS_OPTN) read FUserDefinedLookup1 write SetUserDefinedLookup1 stored UserDefinedLookup1_Specified;
    property UserDefinedLookup2:        WideString            Index (IS_OPTN) read FUserDefinedLookup2 write SetUserDefinedLookup2 stored UserDefinedLookup2_Specified;
    property UserDefinedLookup3:        WideString            Index (IS_OPTN) read FUserDefinedLookup3 write SetUserDefinedLookup3 stored UserDefinedLookup3_Specified;
    property UserDefinedLookup4:        WideString            Index (IS_OPTN) read FUserDefinedLookup4 write SetUserDefinedLookup4 stored UserDefinedLookup4_Specified;
    property UserDefinedLookup5:        WideString            Index (IS_OPTN) read FUserDefinedLookup5 write SetUserDefinedLookup5 stored UserDefinedLookup5_Specified;
    property UserDefinedLookup6:        WideString            Index (IS_OPTN) read FUserDefinedLookup6 write SetUserDefinedLookup6 stored UserDefinedLookup6_Specified;
    property UserDefinedLookup7:        WideString            Index (IS_OPTN) read FUserDefinedLookup7 write SetUserDefinedLookup7 stored UserDefinedLookup7_Specified;
    property UserDefinedLookup8:        WideString            Index (IS_OPTN) read FUserDefinedLookup8 write SetUserDefinedLookup8 stored UserDefinedLookup8_Specified;
    property UserDefinedLookup9:        WideString            Index (IS_OPTN) read FUserDefinedLookup9 write SetUserDefinedLookup9 stored UserDefinedLookup9_Specified;
    property UserDefinedLookup10:       WideString            Index (IS_OPTN) read FUserDefinedLookup10 write SetUserDefinedLookup10 stored UserDefinedLookup10_Specified;
    property UserDefinedLookup11:       WideString            Index (IS_OPTN) read FUserDefinedLookup11 write SetUserDefinedLookup11 stored UserDefinedLookup11_Specified;
    property UserDefinedLookup12:       WideString            Index (IS_OPTN) read FUserDefinedLookup12 write SetUserDefinedLookup12 stored UserDefinedLookup12_Specified;
    property UserDefinedLookup13:       WideString            Index (IS_OPTN) read FUserDefinedLookup13 write SetUserDefinedLookup13 stored UserDefinedLookup13_Specified;
    property UserDefinedLookup14:       WideString            Index (IS_OPTN) read FUserDefinedLookup14 write SetUserDefinedLookup14 stored UserDefinedLookup14_Specified;
    property UserDefinedLookup15:       WideString            Index (IS_OPTN) read FUserDefinedLookup15 write SetUserDefinedLookup15 stored UserDefinedLookup15_Specified;
    property WorkAddress1:              WideString            Index (IS_OPTN) read FWorkAddress1 write SetWorkAddress1 stored WorkAddress1_Specified;
    property WorkAddress2:              WideString            Index (IS_OPTN) read FWorkAddress2 write SetWorkAddress2 stored WorkAddress2_Specified;
    property WorkEmail:                 WideString            Index (IS_OPTN) read FWorkEmail write SetWorkEmail stored WorkEmail_Specified;
    property HomeEmail:                 WideString            Index (IS_OPTN) read FHomeEmail write SetHomeEmail stored HomeEmail_Specified;
    property BenefitServiceDate:        TXSDateTime           read FBenefitServiceDate write FBenefitServiceDate;
    property EmpStatusEffDate:          TXSDateTime           read FEmpStatusEffDate write FEmpStatusEffDate;
    property CompensationCategory:      WideString            Index (IS_OPTN) read FCompensationCategory write SetCompensationCategory stored CompensationCategory_Specified;
    property HourlyRate:                Double                read FHourlyRate write FHourlyRate;
    property EEOClass:                  WideString            Index (IS_OPTN) read FEEOClass write SetEEOClass stored EEOClass_Specified;
    property EEOClassDescription:       WideString            Index (IS_OPTN) read FEEOClassDescription write SetEEOClassDescription stored EEOClassDescription_Specified;
    property OTExempt:                  Boolean               Index (IS_NLBL) read FOTExempt write FOTExempt;
    property VisaExpiration:            TXSDateTime           Index (IS_NLBL) read FVisaExpiration write FVisaExpiration;
    property VisaType:                  WideString            Index (IS_OPTN) read FVisaType write SetVisaType stored VisaType_Specified;
    property IsVeteran:                 Boolean               Index (IS_NLBL) read FIsVeteran write FIsVeteran;
    property NickName:                  WideString            Index (IS_OPTN) read FNickName write SetNickName stored NickName_Specified;
    property Deductions:                ArrayOfDeduction      Index (IS_OPTN) read FDeductions write SetDeductions stored Deductions_Specified;
    property ACA_LowestCost_Coverage:   CoverageInfo          Index (IS_OPTN) read FACA_LowestCost_Coverage write SetACA_LowestCost_Coverage stored ACA_LowestCost_Coverage_Specified;
    property Compensations:             ArrayOfCompensation   Index (IS_OPTN) read FCompensations write SetCompensations stored Compensations_Specified;
    property DirectDeposits:            ArrayOfDirectDeposit  Index (IS_OPTN) read FDirectDeposits write SetDirectDeposits stored DirectDeposits_Specified;
    property StateTax:                  ArrayOfStateTax       Index (IS_OPTN) read FStateTax write SetStateTax stored StateTax_Specified;
    property FederalTax:                ArrayOfFederalTax     Index (IS_OPTN) read FFederalTax write SetFederalTax stored FederalTax_Specified;
    property Handicapped:               Boolean               read FHandicapped write FHandicapped;
    property FullTimeStudent:           Boolean               read FFullTimeStudent write FFullTimeStudent;
    property PartTimeStudent:           Boolean               read FPartTimeStudent write FPartTimeStudent;
    property ShowAllDeductions:         Boolean               read FShowAllDeductions write FShowAllDeductions;
    property PayScheduleName:           WideString            Index (IS_OPTN) read FPayScheduleName write SetPayScheduleName stored PayScheduleName_Specified;
  end;



  // ************************************************************************ //
  // XML       : Deduction, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Deduction = class(TRemotable)
  private
    FID: Integer;
    FEffectiveDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
    FPremiumAmount: TXSDecimal;
    FBenefitType: WideString;
    FBenefitType_Specified: boolean;
    FEmployeeCost: TXSDecimal;
    FEmployerCost: TXSDecimal;
    FCreditAmount: TXSDecimal;
    FCoverageAmount: TXSDecimal;
    FPreTaxDeduction: TXSDecimal;
    FPreTaxDeduction_PerPay: TXSDecimal;
    FPostTaxDeduction_PerPay: TXSDecimal;
    FPostTaxDeduction: TXSDecimal;
    FOptionName: WideString;
    FOptionName_Specified: boolean;
    FPlanID: Integer;
    FPlanName: WideString;
    FPlanName_Specified: boolean;
    FBenefitName: WideString;
    FBenefitName_Specified: boolean;
    FPackageName: WideString;
    FPackageName_Specified: boolean;
    FPerPayEmployeeCost: TXSDecimal;
    FPerPayEmployerCost: TXSDecimal;
    FAnnualPremiumAmount: TXSDecimal;
    FAnnualCreditAmount: TXSDecimal;
    FAnnualEmployeeCost: TXSDecimal;
    FDeductionCode: WideString;
    FDeductionCode_Specified: boolean;
    FDeductionCodeAlt1: WideString;
    FDeductionCodeAlt1_Specified: boolean;
    FDeductionCodeAlt2: WideString;
    FDeductionCodeAlt2_Specified: boolean;
    FCarrierCode: WideString;
    FCarrierCode_Specified: boolean;
    FCarrierPlanCode: WideString;
    FCarrierPlanCode_Specified: boolean;
    FEDCode: WideString;
    FEDCode_Specified: boolean;
    FERDCode: WideString;
    FERDCode_Specified: boolean;
    FDeductionEffectiveDate: TXSDateTime;
    FDeductionExpirationDate: TXSDateTime;
    FBenefitCategory: WideString;
    FBenefitCategory_Specified: boolean;
    FCoverageInputType: WideString;
    FCoverageInputType_Specified: boolean;
    FModifiedDate: TXSDateTime;
    FDeleted: Boolean;
    procedure SetBenefitType(Index: Integer; const AWideString: WideString);
    function  BenefitType_Specified(Index: Integer): boolean;
    procedure SetOptionName(Index: Integer; const AWideString: WideString);
    function  OptionName_Specified(Index: Integer): boolean;
    procedure SetPlanName(Index: Integer; const AWideString: WideString);
    function  PlanName_Specified(Index: Integer): boolean;
    procedure SetBenefitName(Index: Integer; const AWideString: WideString);
    function  BenefitName_Specified(Index: Integer): boolean;
    procedure SetPackageName(Index: Integer; const AWideString: WideString);
    function  PackageName_Specified(Index: Integer): boolean;
    procedure SetDeductionCode(Index: Integer; const AWideString: WideString);
    function  DeductionCode_Specified(Index: Integer): boolean;
    procedure SetDeductionCodeAlt1(Index: Integer; const AWideString: WideString);
    function  DeductionCodeAlt1_Specified(Index: Integer): boolean;
    procedure SetDeductionCodeAlt2(Index: Integer; const AWideString: WideString);
    function  DeductionCodeAlt2_Specified(Index: Integer): boolean;
    procedure SetCarrierCode(Index: Integer; const AWideString: WideString);
    function  CarrierCode_Specified(Index: Integer): boolean;
    procedure SetCarrierPlanCode(Index: Integer; const AWideString: WideString);
    function  CarrierPlanCode_Specified(Index: Integer): boolean;
    procedure SetEDCode(Index: Integer; const AWideString: WideString);
    function  EDCode_Specified(Index: Integer): boolean;
    procedure SetERDCode(Index: Integer; const AWideString: WideString);
    function  ERDCode_Specified(Index: Integer): boolean;
    procedure SetBenefitCategory(Index: Integer; const AWideString: WideString);
    function  BenefitCategory_Specified(Index: Integer): boolean;
    procedure SetCoverageInputType(Index: Integer; const AWideString: WideString);
    function  CoverageInputType_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                      Integer      read FID write FID;
    property EffectiveDate:           TXSDateTime  read FEffectiveDate write FEffectiveDate;
    property ExpirationDate:          TXSDateTime  read FExpirationDate write FExpirationDate;
    property PremiumAmount:           TXSDecimal   read FPremiumAmount write FPremiumAmount;
    property BenefitType:             WideString   Index (IS_OPTN) read FBenefitType write SetBenefitType stored BenefitType_Specified;
    property EmployeeCost:            TXSDecimal   read FEmployeeCost write FEmployeeCost;
    property EmployerCost:            TXSDecimal   read FEmployerCost write FEmployerCost;
    property CreditAmount:            TXSDecimal   read FCreditAmount write FCreditAmount;
    property CoverageAmount:          TXSDecimal   read FCoverageAmount write FCoverageAmount;
    property PreTaxDeduction:         TXSDecimal   read FPreTaxDeduction write FPreTaxDeduction;
    property PreTaxDeduction_PerPay:  TXSDecimal   read FPreTaxDeduction_PerPay write FPreTaxDeduction_PerPay;
    property PostTaxDeduction_PerPay: TXSDecimal   read FPostTaxDeduction_PerPay write FPostTaxDeduction_PerPay;
    property PostTaxDeduction:        TXSDecimal   read FPostTaxDeduction write FPostTaxDeduction;
    property OptionName:              WideString   Index (IS_OPTN) read FOptionName write SetOptionName stored OptionName_Specified;
    property PlanID:                  Integer      read FPlanID write FPlanID;
    property PlanName:                WideString   Index (IS_OPTN) read FPlanName write SetPlanName stored PlanName_Specified;
    property BenefitName:             WideString   Index (IS_OPTN) read FBenefitName write SetBenefitName stored BenefitName_Specified;
    property PackageName:             WideString   Index (IS_OPTN) read FPackageName write SetPackageName stored PackageName_Specified;
    property PerPayEmployeeCost:      TXSDecimal   read FPerPayEmployeeCost write FPerPayEmployeeCost;
    property PerPayEmployerCost:      TXSDecimal   read FPerPayEmployerCost write FPerPayEmployerCost;
    property AnnualPremiumAmount:     TXSDecimal   read FAnnualPremiumAmount write FAnnualPremiumAmount;
    property AnnualCreditAmount:      TXSDecimal   read FAnnualCreditAmount write FAnnualCreditAmount;
    property AnnualEmployeeCost:      TXSDecimal   read FAnnualEmployeeCost write FAnnualEmployeeCost;
    property DeductionCode:           WideString   Index (IS_OPTN) read FDeductionCode write SetDeductionCode stored DeductionCode_Specified;
    property DeductionCodeAlt1:       WideString   Index (IS_OPTN) read FDeductionCodeAlt1 write SetDeductionCodeAlt1 stored DeductionCodeAlt1_Specified;
    property DeductionCodeAlt2:       WideString   Index (IS_OPTN) read FDeductionCodeAlt2 write SetDeductionCodeAlt2 stored DeductionCodeAlt2_Specified;
    property CarrierCode:             WideString   Index (IS_OPTN) read FCarrierCode write SetCarrierCode stored CarrierCode_Specified;
    property CarrierPlanCode:         WideString   Index (IS_OPTN) read FCarrierPlanCode write SetCarrierPlanCode stored CarrierPlanCode_Specified;
    property EDCode:                  WideString   Index (IS_OPTN) read FEDCode write SetEDCode stored EDCode_Specified;
    property ERDCode:                 WideString   Index (IS_OPTN) read FERDCode write SetERDCode stored ERDCode_Specified;
    property DeductionEffectiveDate:  TXSDateTime  read FDeductionEffectiveDate write FDeductionEffectiveDate;
    property DeductionExpirationDate: TXSDateTime  read FDeductionExpirationDate write FDeductionExpirationDate;
    property BenefitCategory:         WideString   Index (IS_OPTN) read FBenefitCategory write SetBenefitCategory stored BenefitCategory_Specified;
    property CoverageInputType:       WideString   Index (IS_OPTN) read FCoverageInputType write SetCoverageInputType stored CoverageInputType_Specified;
    property ModifiedDate:            TXSDateTime  read FModifiedDate write FModifiedDate;
    property Deleted:                 Boolean      read FDeleted write FDeleted;
  end;



  // ************************************************************************ //
  // XML       : Compensation, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Compensation = class(TRemotable)
  private
    FID: Integer;
    FEffectiveDate: TXSDateTime;
    FRate: TXSDecimal;
    FHoursWorked: TXSDecimal;
    FAnnualRate: TXSDecimal;
    FHourlyRate: TXSDecimal;
    FOverTimeRate: TXSDecimal;
    FOtherRate: TXSDecimal;
    FNotes: WideString;
    FNotes_Specified: boolean;
    FLastModifiedDate: TXSDateTime;
    FPayPeriodAmount: TXSDecimal;
    FRateType: WideString;
    FRateType_Specified: boolean;
    FExpirationDate: TXSDateTime;
    FAutoPay: Boolean;
    FAutoPayHours: Double;
    FRateCode: WideString;
    FRateCode_Specified: boolean;
    FDeleted: Boolean;
    procedure SetNotes(Index: Integer; const AWideString: WideString);
    function  Notes_Specified(Index: Integer): boolean;
    procedure SetRateType(Index: Integer; const AWideString: WideString);
    function  RateType_Specified(Index: Integer): boolean;
    procedure SetRateCode(Index: Integer; const AWideString: WideString);
    function  RateCode_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:               Integer      read FID write FID;
    property EffectiveDate:    TXSDateTime  read FEffectiveDate write FEffectiveDate;
    property Rate:             TXSDecimal   read FRate write FRate;
    property HoursWorked:      TXSDecimal   read FHoursWorked write FHoursWorked;
    property AnnualRate:       TXSDecimal   read FAnnualRate write FAnnualRate;
    property HourlyRate:       TXSDecimal   read FHourlyRate write FHourlyRate;
    property OverTimeRate:     TXSDecimal   read FOverTimeRate write FOverTimeRate;
    property OtherRate:        TXSDecimal   read FOtherRate write FOtherRate;
    property Notes:            WideString   Index (IS_OPTN) read FNotes write SetNotes stored Notes_Specified;
    property LastModifiedDate: TXSDateTime  read FLastModifiedDate write FLastModifiedDate;
    property PayPeriodAmount:  TXSDecimal   read FPayPeriodAmount write FPayPeriodAmount;
    property RateType:         WideString   Index (IS_OPTN) read FRateType write SetRateType stored RateType_Specified;
    property ExpirationDate:   TXSDateTime  read FExpirationDate write FExpirationDate;
    property AutoPay:          Boolean      read FAutoPay write FAutoPay;
    property AutoPayHours:     Double       read FAutoPayHours write FAutoPayHours;
    property RateCode:         WideString   Index (IS_OPTN) read FRateCode write SetRateCode stored RateCode_Specified;
    property Deleted:          Boolean      read FDeleted write FDeleted;
  end;



  // ************************************************************************ //
  // XML       : DirectDeposit, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  DirectDeposit = class(TRemotable)
  private
    FID: Integer;
    FDirectDepositTransitNumber: WideString;
    FDirectDepositTransitNumber_Specified: boolean;
    FDirectDepositAccountNumber: WideString;
    FDirectDepositAccountNumber_Specified: boolean;
    FBankName: WideString;
    FBankName_Specified: boolean;
    FAccountType: WideString;
    FAccountType_Specified: boolean;
    FDepositAmount: TXSDecimal;
    FDepositPercent: TXSDecimal;
    FIsActive: Boolean;
    FModifiedDate: TXSDateTime;
    FEDCode: WideString;
    FEDCode_Specified: boolean;
    FEffectiveDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
    FOverridePreNote: Boolean;
    procedure SetDirectDepositTransitNumber(Index: Integer; const AWideString: WideString);
    function  DirectDepositTransitNumber_Specified(Index: Integer): boolean;
    procedure SetDirectDepositAccountNumber(Index: Integer; const AWideString: WideString);
    function  DirectDepositAccountNumber_Specified(Index: Integer): boolean;
    procedure SetBankName(Index: Integer; const AWideString: WideString);
    function  BankName_Specified(Index: Integer): boolean;
    procedure SetAccountType(Index: Integer; const AWideString: WideString);
    function  AccountType_Specified(Index: Integer): boolean;
    procedure SetEDCode(Index: Integer; const AWideString: WideString);
    function  EDCode_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                         Integer      read FID write FID;
    property DirectDepositTransitNumber: WideString   Index (IS_OPTN) read FDirectDepositTransitNumber write SetDirectDepositTransitNumber stored DirectDepositTransitNumber_Specified;
    property DirectDepositAccountNumber: WideString   Index (IS_OPTN) read FDirectDepositAccountNumber write SetDirectDepositAccountNumber stored DirectDepositAccountNumber_Specified;
    property BankName:                   WideString   Index (IS_OPTN) read FBankName write SetBankName stored BankName_Specified;
    property AccountType:                WideString   Index (IS_OPTN) read FAccountType write SetAccountType stored AccountType_Specified;
    property DepositAmount:              TXSDecimal   read FDepositAmount write FDepositAmount;
    property DepositPercent:             TXSDecimal   read FDepositPercent write FDepositPercent;
    property IsActive:                   Boolean      read FIsActive write FIsActive;
    property ModifiedDate:               TXSDateTime  read FModifiedDate write FModifiedDate;
    property EDCode:                     WideString   Index (IS_OPTN) read FEDCode write SetEDCode stored EDCode_Specified;
    property EffectiveDate:              TXSDateTime  read FEffectiveDate write FEffectiveDate;
    property ExpirationDate:             TXSDateTime  read FExpirationDate write FExpirationDate;
    property OverridePreNote:            Boolean      read FOverridePreNote write FOverridePreNote;
  end;



  // ************************************************************************ //
  // XML       : StateTax, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  StateTax = class(TRemotable)
  private
    FID: Integer;
    FTaxYear: Integer;
    FFilingStatus: WideString;
    FFilingStatus_Specified: boolean;
    FIncomeFilingState: WideString;
    FIncomeFilingState_Specified: boolean;
    FUnemploymentState: WideString;
    FUnemploymentState_Specified: boolean;
    FAllowances: Integer;
    FAdditionalWithholding: TXSDecimal;
    FPercentWithholding: TXSDecimal;
    FIsExempt: Boolean;
    FEffectiveDate: TXSDateTime;
    FIsActive: Boolean;
    FOverrideAmount: TXSDecimal;
    FOverridePercent: TXSDecimal;
    FCounty: WideString;
    FCounty_Specified: boolean;
    FClaimAmount: WideString;
    FClaimAmount_Specified: boolean;
    FAdditionalPercentWithheld: TXSDecimal;
    FStartDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
    FModifiedDate: TXSDateTime;
    FDeleted: Boolean;
    procedure SetFilingStatus(Index: Integer; const AWideString: WideString);
    function  FilingStatus_Specified(Index: Integer): boolean;
    procedure SetIncomeFilingState(Index: Integer; const AWideString: WideString);
    function  IncomeFilingState_Specified(Index: Integer): boolean;
    procedure SetUnemploymentState(Index: Integer; const AWideString: WideString);
    function  UnemploymentState_Specified(Index: Integer): boolean;
    procedure SetCounty(Index: Integer; const AWideString: WideString);
    function  County_Specified(Index: Integer): boolean;
    procedure SetClaimAmount(Index: Integer; const AWideString: WideString);
    function  ClaimAmount_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                        Integer      read FID write FID;
    property TaxYear:                   Integer      read FTaxYear write FTaxYear;
    property FilingStatus:              WideString   Index (IS_OPTN) read FFilingStatus write SetFilingStatus stored FilingStatus_Specified;
    property IncomeFilingState:         WideString   Index (IS_OPTN) read FIncomeFilingState write SetIncomeFilingState stored IncomeFilingState_Specified;
    property UnemploymentState:         WideString   Index (IS_OPTN) read FUnemploymentState write SetUnemploymentState stored UnemploymentState_Specified;
    property Allowances:                Integer      read FAllowances write FAllowances;
    property AdditionalWithholding:     TXSDecimal   read FAdditionalWithholding write FAdditionalWithholding;
    property PercentWithholding:        TXSDecimal   read FPercentWithholding write FPercentWithholding;
    property IsExempt:                  Boolean      read FIsExempt write FIsExempt;
    property EffectiveDate:             TXSDateTime  read FEffectiveDate write FEffectiveDate;
    property IsActive:                  Boolean      read FIsActive write FIsActive;
    property OverrideAmount:            TXSDecimal   read FOverrideAmount write FOverrideAmount;
    property OverridePercent:           TXSDecimal   read FOverridePercent write FOverridePercent;
    property County:                    WideString   Index (IS_OPTN) read FCounty write SetCounty stored County_Specified;
    property ClaimAmount:               WideString   Index (IS_OPTN) read FClaimAmount write SetClaimAmount stored ClaimAmount_Specified;
    property AdditionalPercentWithheld: TXSDecimal   read FAdditionalPercentWithheld write FAdditionalPercentWithheld;
    property StartDate:                 TXSDateTime  read FStartDate write FStartDate;
    property ExpirationDate:            TXSDateTime  read FExpirationDate write FExpirationDate;
    property ModifiedDate:              TXSDateTime  read FModifiedDate write FModifiedDate;
    property Deleted:                   Boolean      read FDeleted write FDeleted;
  end;



  // ************************************************************************ //
  // XML       : FederalTax, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  FederalTax = class(TRemotable)
  private
    FID: Integer;
    FTaxYear: Integer;
    FAllowances: Integer;
    FAdditionalWithholding: TXSDecimal;
    FMaritalStatus: WideString;
    FMaritalStatus_Specified: boolean;
    FExempt: Boolean;
    FLastNameDiffers: Boolean;
    FEffectiveDate: TXSDateTime;
    FOverrideAmount: TXSDecimal;
    FOverridePercent: TXSDecimal;
    FStartDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
    FModifiedDate: TXSDateTime;
    FDeleted: Boolean;
    procedure SetMaritalStatus(Index: Integer; const AWideString: WideString);
    function  MaritalStatus_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:                    Integer      read FID write FID;
    property TaxYear:               Integer      read FTaxYear write FTaxYear;
    property Allowances:            Integer      read FAllowances write FAllowances;
    property AdditionalWithholding: TXSDecimal   read FAdditionalWithholding write FAdditionalWithholding;
    property MaritalStatus:         WideString   Index (IS_OPTN) read FMaritalStatus write SetMaritalStatus stored MaritalStatus_Specified;
    property Exempt:                Boolean      read FExempt write FExempt;
    property LastNameDiffers:       Boolean      read FLastNameDiffers write FLastNameDiffers;
    property EffectiveDate:         TXSDateTime  read FEffectiveDate write FEffectiveDate;
    property OverrideAmount:        TXSDecimal   read FOverrideAmount write FOverrideAmount;
    property OverridePercent:       TXSDecimal   read FOverridePercent write FOverridePercent;
    property StartDate:             TXSDateTime  read FStartDate write FStartDate;
    property ExpirationDate:        TXSDateTime  read FExpirationDate write FExpirationDate;
    property ModifiedDate:          TXSDateTime  read FModifiedDate write FModifiedDate;
    property Deleted:               Boolean      read FDeleted write FDeleted;
  end;

  ArrayOfEmployeeInfoSkinny = array of EmployeeInfoSkinny;   { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : EmployeeData, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeData = class(TRemotable)
  private
    FEmployees: ArrayOfEmployeeInfo;
    FEmployees_Specified: boolean;
    FSyncDate: TXSDateTime;
    FEmployeesSkinny: ArrayOfEmployeeInfoSkinny;
    FEmployeesSkinny_Specified: boolean;
    procedure SetEmployees(Index: Integer; const AArrayOfEmployeeInfo: ArrayOfEmployeeInfo);
    function  Employees_Specified(Index: Integer): boolean;
    procedure SetEmployeesSkinny(Index: Integer; const AArrayOfEmployeeInfoSkinny: ArrayOfEmployeeInfoSkinny);
    function  EmployeesSkinny_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Employees:       ArrayOfEmployeeInfo        Index (IS_OPTN) read FEmployees write SetEmployees stored Employees_Specified;
    property SyncDate:        TXSDateTime                read FSyncDate write FSyncDate;
    property EmployeesSkinny: ArrayOfEmployeeInfoSkinny  Index (IS_OPTN) read FEmployeesSkinny write SetEmployeesSkinny stored EmployeesSkinny_Specified;
  end;

  ArrayOfFieldInfo = array of FieldInfo;        { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : EmployeeInfoSkinny, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeInfoSkinny = class(TRemotable)
  private
    FInfinityHREmployeeID: Integer;
    FPayrollEmployeeID: WideString;
    FPayrollEmployeeID_Specified: boolean;
    FEmployeeSSN: WideString;
    FEmployeeSSN_Specified: boolean;
    FFirstName: WideString;
    FFirstName_Specified: boolean;
    FLastName: WideString;
    FLastName_Specified: boolean;
    FDepartment: WideString;
    FDepartment_Specified: boolean;
    FCompanyID: Integer;
    FIsAdmin: Boolean;
    FVendorCompany_ID: WideString;
    FVendorCompany_ID_Specified: boolean;
    FChangeFields: ArrayOfFieldInfo;
    FChangeFields_Specified: boolean;
    FIncludeCompensation: Boolean;
    FIncludeDeductions: Boolean;
    FIncludeDirectDeposit: Boolean;
    FIncludeFederalTax: Boolean;
    FIncludeStateTax: Boolean;
    FDeductions: ArrayOfDeduction;
    FDeductions_Specified: boolean;
    FCompensations: ArrayOfCompensation;
    FCompensations_Specified: boolean;
    FDirectDeposits: ArrayOfDirectDeposit;
    FDirectDeposits_Specified: boolean;
    FStateTax: ArrayOfStateTax;
    FStateTax_Specified: boolean;
    FFederalTax: ArrayOfFederalTax;
    FFederalTax_Specified: boolean;
    procedure SetPayrollEmployeeID(Index: Integer; const AWideString: WideString);
    function  PayrollEmployeeID_Specified(Index: Integer): boolean;
    procedure SetEmployeeSSN(Index: Integer; const AWideString: WideString);
    function  EmployeeSSN_Specified(Index: Integer): boolean;
    procedure SetFirstName(Index: Integer; const AWideString: WideString);
    function  FirstName_Specified(Index: Integer): boolean;
    procedure SetLastName(Index: Integer; const AWideString: WideString);
    function  LastName_Specified(Index: Integer): boolean;
    procedure SetDepartment(Index: Integer; const AWideString: WideString);
    function  Department_Specified(Index: Integer): boolean;
    procedure SetVendorCompany_ID(Index: Integer; const AWideString: WideString);
    function  VendorCompany_ID_Specified(Index: Integer): boolean;
    procedure SetChangeFields(Index: Integer; const AArrayOfFieldInfo: ArrayOfFieldInfo);
    function  ChangeFields_Specified(Index: Integer): boolean;
    procedure SetDeductions(Index: Integer; const AArrayOfDeduction: ArrayOfDeduction);
    function  Deductions_Specified(Index: Integer): boolean;
    procedure SetCompensations(Index: Integer; const AArrayOfCompensation: ArrayOfCompensation);
    function  Compensations_Specified(Index: Integer): boolean;
    procedure SetDirectDeposits(Index: Integer; const AArrayOfDirectDeposit: ArrayOfDirectDeposit);
    function  DirectDeposits_Specified(Index: Integer): boolean;
    procedure SetStateTax(Index: Integer; const AArrayOfStateTax: ArrayOfStateTax);
    function  StateTax_Specified(Index: Integer): boolean;
    procedure SetFederalTax(Index: Integer; const AArrayOfFederalTax: ArrayOfFederalTax);
    function  FederalTax_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property InfinityHREmployeeID: Integer               read FInfinityHREmployeeID write FInfinityHREmployeeID;
    property PayrollEmployeeID:    WideString            Index (IS_OPTN) read FPayrollEmployeeID write SetPayrollEmployeeID stored PayrollEmployeeID_Specified;
    property EmployeeSSN:          WideString            Index (IS_OPTN) read FEmployeeSSN write SetEmployeeSSN stored EmployeeSSN_Specified;
    property FirstName:            WideString            Index (IS_OPTN) read FFirstName write SetFirstName stored FirstName_Specified;
    property LastName:             WideString            Index (IS_OPTN) read FLastName write SetLastName stored LastName_Specified;
    property Department:           WideString            Index (IS_OPTN) read FDepartment write SetDepartment stored Department_Specified;
    property CompanyID:            Integer               read FCompanyID write FCompanyID;
    property IsAdmin:              Boolean               read FIsAdmin write FIsAdmin;
    property VendorCompany_ID:     WideString            Index (IS_OPTN) read FVendorCompany_ID write SetVendorCompany_ID stored VendorCompany_ID_Specified;
    property ChangeFields:         ArrayOfFieldInfo      Index (IS_OPTN) read FChangeFields write SetChangeFields stored ChangeFields_Specified;
    property IncludeCompensation:  Boolean               read FIncludeCompensation write FIncludeCompensation;
    property IncludeDeductions:    Boolean               read FIncludeDeductions write FIncludeDeductions;
    property IncludeDirectDeposit: Boolean               read FIncludeDirectDeposit write FIncludeDirectDeposit;
    property IncludeFederalTax:    Boolean               read FIncludeFederalTax write FIncludeFederalTax;
    property IncludeStateTax:      Boolean               read FIncludeStateTax write FIncludeStateTax;
    property Deductions:           ArrayOfDeduction      Index (IS_OPTN) read FDeductions write SetDeductions stored Deductions_Specified;
    property Compensations:        ArrayOfCompensation   Index (IS_OPTN) read FCompensations write SetCompensations stored Compensations_Specified;
    property DirectDeposits:       ArrayOfDirectDeposit  Index (IS_OPTN) read FDirectDeposits write SetDirectDeposits stored DirectDeposits_Specified;
    property StateTax:             ArrayOfStateTax       Index (IS_OPTN) read FStateTax write SetStateTax stored StateTax_Specified;
    property FederalTax:           ArrayOfFederalTax     Index (IS_OPTN) read FFederalTax write SetFederalTax stored FederalTax_Specified;
  end;



  // ************************************************************************ //
  // XML       : FieldInfo, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  FieldInfo = class(TRemotable)
  private
    FFieldName: WideString;
    FFieldName_Specified: boolean;
    FValueBefore: WideString;
    FValueBefore_Specified: boolean;
    FValueAfter: WideString;
    FValueAfter_Specified: boolean;
    FFieldDataType: WideString;
    FFieldDataType_Specified: boolean;
    FDataCategory_ID: Integer;
    FChangeDate: TXSDateTime;
    FChangeType: FieldChangeType;
    FTable_ID: Integer;
    FRecordInfo: WideString;
    FRecordInfo_Specified: boolean;
    procedure SetFieldName(Index: Integer; const AWideString: WideString);
    function  FieldName_Specified(Index: Integer): boolean;
    procedure SetValueBefore(Index: Integer; const AWideString: WideString);
    function  ValueBefore_Specified(Index: Integer): boolean;
    procedure SetValueAfter(Index: Integer; const AWideString: WideString);
    function  ValueAfter_Specified(Index: Integer): boolean;
    procedure SetFieldDataType(Index: Integer; const AWideString: WideString);
    function  FieldDataType_Specified(Index: Integer): boolean;
    procedure SetRecordInfo(Index: Integer; const AWideString: WideString);
    function  RecordInfo_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property FieldName:       WideString       Index (IS_OPTN) read FFieldName write SetFieldName stored FieldName_Specified;
    property ValueBefore:     WideString       Index (IS_OPTN) read FValueBefore write SetValueBefore stored ValueBefore_Specified;
    property ValueAfter:      WideString       Index (IS_OPTN) read FValueAfter write SetValueAfter stored ValueAfter_Specified;
    property FieldDataType:   WideString       Index (IS_OPTN) read FFieldDataType write SetFieldDataType stored FieldDataType_Specified;
    property DataCategory_ID: Integer          read FDataCategory_ID write FDataCategory_ID;
    property ChangeDate:      TXSDateTime      read FChangeDate write FChangeDate;
    property ChangeType:      FieldChangeType  read FChangeType write FChangeType;
    property Table_ID:        Integer          read FTable_ID write FTable_ID;
    property RecordInfo:      WideString       Index (IS_OPTN) read FRecordInfo write SetRecordInfo stored RecordInfo_Specified;
  end;

  ArrayOfInt = array of Integer;                { "http://www.infinity-ss.com/services"[GblCplx] }
  ArrayOfPaystubFile = array of PaystubFile;    { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : PaystubRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FSSN: WideString;
    FSSN_Specified: boolean;
    FPaystub_Files: ArrayOfPaystubFile;
    FPaystub_Files_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetSSN(Index: Integer; const AWideString: WideString);
    function  SSN_Specified(Index: Integer): boolean;
    procedure SetPaystub_Files(Index: Integer; const AArrayOfPaystubFile: ArrayOfPaystubFile);
    function  Paystub_Files_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property SessionID:     WideString          Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:      WideString          Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property SSN:           WideString          Index (IS_OPTN) read FSSN write SetSSN stored SSN_Specified;
    property Paystub_Files: ArrayOfPaystubFile  Index (IS_OPTN) read FPaystub_Files write SetPaystub_Files stored Paystub_Files_Specified;
  end;



  // ************************************************************************ //
  // XML       : PaystubFile, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubFile = class(TRemotable)
  private
    FPayPeriod_StartDate: TXSDateTime;
    FPayPeriod_EndDate: TXSDateTime;
    FPayDate: TXSDateTime;
    FCheckNumber: WideString;
    FCheckNumber_Specified: boolean;
    FDepositAmount: TXSDecimal;
    FIndicator: WideString;
    FIndicator_Specified: boolean;
    FFileContent: TByteDynArray;
    FFileContent_Specified: boolean;
    FFileName: WideString;
    FFileName_Specified: boolean;
    FFileExtension: WideString;
    FFileExtension_Specified: boolean;
    FFileLength: Double;
    procedure SetCheckNumber(Index: Integer; const AWideString: WideString);
    function  CheckNumber_Specified(Index: Integer): boolean;
    procedure SetIndicator(Index: Integer; const AWideString: WideString);
    function  Indicator_Specified(Index: Integer): boolean;
    procedure SetFileContent(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  FileContent_Specified(Index: Integer): boolean;
    procedure SetFileName(Index: Integer; const AWideString: WideString);
    function  FileName_Specified(Index: Integer): boolean;
    procedure SetFileExtension(Index: Integer; const AWideString: WideString);
    function  FileExtension_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PayPeriod_StartDate: TXSDateTime    read FPayPeriod_StartDate write FPayPeriod_StartDate;
    property PayPeriod_EndDate:   TXSDateTime    read FPayPeriod_EndDate write FPayPeriod_EndDate;
    property PayDate:             TXSDateTime    read FPayDate write FPayDate;
    property CheckNumber:         WideString     Index (IS_OPTN) read FCheckNumber write SetCheckNumber stored CheckNumber_Specified;
    property DepositAmount:       TXSDecimal     read FDepositAmount write FDepositAmount;
    property Indicator:           WideString     Index (IS_OPTN) read FIndicator write SetIndicator stored Indicator_Specified;
    property FileContent:         TByteDynArray  Index (IS_OPTN) read FFileContent write SetFileContent stored FileContent_Specified;
    property FileName:            WideString     Index (IS_OPTN) read FFileName write SetFileName stored FileName_Specified;
    property FileExtension:       WideString     Index (IS_OPTN) read FFileExtension write SetFileExtension stored FileExtension_Specified;
    property FileLength:          Double         read FFileLength write FFileLength;
  end;

  ArrayOfPaystubData = array of PaystubData;    { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : PaystubDataRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubDataRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FSSN: WideString;
    FSSN_Specified: boolean;
    FPaystub_Data: ArrayOfPaystubData;
    FPaystub_Data_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetSSN(Index: Integer; const AWideString: WideString);
    function  SSN_Specified(Index: Integer): boolean;
    procedure SetPaystub_Data(Index: Integer; const AArrayOfPaystubData: ArrayOfPaystubData);
    function  Paystub_Data_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property SessionID:    WideString          Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:     WideString          Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property SSN:          WideString          Index (IS_OPTN) read FSSN write SetSSN stored SSN_Specified;
    property Paystub_Data: ArrayOfPaystubData  Index (IS_OPTN) read FPaystub_Data write SetPaystub_Data stored Paystub_Data_Specified;
  end;

  ArrayOfPaystub_Summary = array of Paystub_Summary;   { "http://www.infinity-ss.com/services"[GblCplx] }
  ArrayOfPaystub_Detail = array of Paystub_Detail;   { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : PaystubData, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubData = class(TRemotable)
  private
    FPayStub_Summary: ArrayOfPaystub_Summary;
    FPayStub_Summary_Specified: boolean;
    FPayStub_Detail: ArrayOfPaystub_Detail;
    FPayStub_Detail_Specified: boolean;
    FPayStub_TimeOff: Paystub_TimeOff;
    FPayStub_TimeOff_Specified: boolean;
    FPayStub_Tax: Paystub_Tax;
    FPayStub_Tax_Specified: boolean;
    procedure SetPayStub_Summary(Index: Integer; const AArrayOfPaystub_Summary: ArrayOfPaystub_Summary);
    function  PayStub_Summary_Specified(Index: Integer): boolean;
    procedure SetPayStub_Detail(Index: Integer; const AArrayOfPaystub_Detail: ArrayOfPaystub_Detail);
    function  PayStub_Detail_Specified(Index: Integer): boolean;
    procedure SetPayStub_TimeOff(Index: Integer; const APaystub_TimeOff: Paystub_TimeOff);
    function  PayStub_TimeOff_Specified(Index: Integer): boolean;
    procedure SetPayStub_Tax(Index: Integer; const APaystub_Tax: Paystub_Tax);
    function  PayStub_Tax_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PayStub_Summary: ArrayOfPaystub_Summary  Index (IS_OPTN) read FPayStub_Summary write SetPayStub_Summary stored PayStub_Summary_Specified;
    property PayStub_Detail:  ArrayOfPaystub_Detail   Index (IS_OPTN) read FPayStub_Detail write SetPayStub_Detail stored PayStub_Detail_Specified;
    property PayStub_TimeOff: Paystub_TimeOff         Index (IS_OPTN) read FPayStub_TimeOff write SetPayStub_TimeOff stored PayStub_TimeOff_Specified;
    property PayStub_Tax:     Paystub_Tax             Index (IS_OPTN) read FPayStub_Tax write SetPayStub_Tax stored PayStub_Tax_Specified;
  end;



  // ************************************************************************ //
  // XML       : Paystub_TimeOff, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_TimeOff = class(TRemotable)
  private
    FPayDate: TXSDateTime;
    FIndicator: WideString;
    FIndicator_Specified: boolean;
    FLeaveType: WideString;
    FLeaveType_Specified: boolean;
    FStartBalance: TXSDecimal;
    FEarned: TXSDecimal;
    FTaken: TXSDecimal;
    FAdjustment: TXSDecimal;
    FEndBalance: TXSDecimal;
    procedure SetIndicator(Index: Integer; const AWideString: WideString);
    function  Indicator_Specified(Index: Integer): boolean;
    procedure SetLeaveType(Index: Integer; const AWideString: WideString);
    function  LeaveType_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PayDate:      TXSDateTime  read FPayDate write FPayDate;
    property Indicator:    WideString   Index (IS_OPTN) read FIndicator write SetIndicator stored Indicator_Specified;
    property LeaveType:    WideString   Index (IS_OPTN) read FLeaveType write SetLeaveType stored LeaveType_Specified;
    property StartBalance: TXSDecimal   read FStartBalance write FStartBalance;
    property Earned:       TXSDecimal   read FEarned write FEarned;
    property Taken:        TXSDecimal   read FTaken write FTaken;
    property Adjustment:   TXSDecimal   read FAdjustment write FAdjustment;
    property EndBalance:   TXSDecimal   read FEndBalance write FEndBalance;
  end;



  // ************************************************************************ //
  // XML       : Paystub_Tax, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_Tax = class(TRemotable)
  private
    FPayDate: TXSDateTime;
    FIndicator: WideString;
    FIndicator_Specified: boolean;
    FFederalFilingStatus: WideString;
    FFederalFilingStatus_Specified: boolean;
    FFederalAllowances: TXSDecimal;
    FFederalAdditionalAllowances: TXSDecimal;
    FFederalAdditionalAmount: TXSDecimal;
    FFilingState: WideString;
    FFilingState_Specified: boolean;
    FUnemploymentFilingState: WideString;
    FUnemploymentFilingState_Specified: boolean;
    FStateTaxFilingStatus: WideString;
    FStateTaxFilingStatus_Specified: boolean;
    FStateAllowances: TXSDecimal;
    FStateAdditionalAllowances: TXSDecimal;
    FStateAdditionalPercentage: TXSDecimal;
    FStateAdditionalAmount: TXSDecimal;
    procedure SetIndicator(Index: Integer; const AWideString: WideString);
    function  Indicator_Specified(Index: Integer): boolean;
    procedure SetFederalFilingStatus(Index: Integer; const AWideString: WideString);
    function  FederalFilingStatus_Specified(Index: Integer): boolean;
    procedure SetFilingState(Index: Integer; const AWideString: WideString);
    function  FilingState_Specified(Index: Integer): boolean;
    procedure SetUnemploymentFilingState(Index: Integer; const AWideString: WideString);
    function  UnemploymentFilingState_Specified(Index: Integer): boolean;
    procedure SetStateTaxFilingStatus(Index: Integer; const AWideString: WideString);
    function  StateTaxFilingStatus_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PayDate:                     TXSDateTime  read FPayDate write FPayDate;
    property Indicator:                   WideString   Index (IS_OPTN) read FIndicator write SetIndicator stored Indicator_Specified;
    property FederalFilingStatus:         WideString   Index (IS_OPTN) read FFederalFilingStatus write SetFederalFilingStatus stored FederalFilingStatus_Specified;
    property FederalAllowances:           TXSDecimal   read FFederalAllowances write FFederalAllowances;
    property FederalAdditionalAllowances: TXSDecimal   read FFederalAdditionalAllowances write FFederalAdditionalAllowances;
    property FederalAdditionalAmount:     TXSDecimal   read FFederalAdditionalAmount write FFederalAdditionalAmount;
    property FilingState:                 WideString   Index (IS_OPTN) read FFilingState write SetFilingState stored FilingState_Specified;
    property UnemploymentFilingState:     WideString   Index (IS_OPTN) read FUnemploymentFilingState write SetUnemploymentFilingState stored UnemploymentFilingState_Specified;
    property StateTaxFilingStatus:        WideString   Index (IS_OPTN) read FStateTaxFilingStatus write SetStateTaxFilingStatus stored StateTaxFilingStatus_Specified;
    property StateAllowances:             TXSDecimal   read FStateAllowances write FStateAllowances;
    property StateAdditionalAllowances:   TXSDecimal   read FStateAdditionalAllowances write FStateAdditionalAllowances;
    property StateAdditionalPercentage:   TXSDecimal   read FStateAdditionalPercentage write FStateAdditionalPercentage;
    property StateAdditionalAmount:       TXSDecimal   read FStateAdditionalAmount write FStateAdditionalAmount;
  end;



  // ************************************************************************ //
  // XML       : Paystub_Summary, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_Summary = class(TRemotable)
  private
    FPayPeriod_Start: TXSDateTime;
    FPayPeriod_End: TXSDateTime;
    FPayDate: TXSDateTime;
    FPayGroup: WideString;
    FPayGroup_Specified: boolean;
    FDepositType: WideString;
    FDepositType_Specified: boolean;
    FCheckNumber: WideString;
    FCheckNumber_Specified: boolean;
    FDepositAccountType: WideString;
    FDepositAccountType_Specified: boolean;
    FDepositAccountNumber: WideString;
    FDepositAccountNumber_Specified: boolean;
    FDepositAmount: TXSDecimal;
    FDepositAmountYTD: TXSDecimal;
    FIndicator: WideString;
    FIndicator_Specified: boolean;
    procedure SetPayGroup(Index: Integer; const AWideString: WideString);
    function  PayGroup_Specified(Index: Integer): boolean;
    procedure SetDepositType(Index: Integer; const AWideString: WideString);
    function  DepositType_Specified(Index: Integer): boolean;
    procedure SetCheckNumber(Index: Integer; const AWideString: WideString);
    function  CheckNumber_Specified(Index: Integer): boolean;
    procedure SetDepositAccountType(Index: Integer; const AWideString: WideString);
    function  DepositAccountType_Specified(Index: Integer): boolean;
    procedure SetDepositAccountNumber(Index: Integer; const AWideString: WideString);
    function  DepositAccountNumber_Specified(Index: Integer): boolean;
    procedure SetIndicator(Index: Integer; const AWideString: WideString);
    function  Indicator_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PayPeriod_Start:      TXSDateTime  read FPayPeriod_Start write FPayPeriod_Start;
    property PayPeriod_End:        TXSDateTime  read FPayPeriod_End write FPayPeriod_End;
    property PayDate:              TXSDateTime  read FPayDate write FPayDate;
    property PayGroup:             WideString   Index (IS_OPTN) read FPayGroup write SetPayGroup stored PayGroup_Specified;
    property DepositType:          WideString   Index (IS_OPTN) read FDepositType write SetDepositType stored DepositType_Specified;
    property CheckNumber:          WideString   Index (IS_OPTN) read FCheckNumber write SetCheckNumber stored CheckNumber_Specified;
    property DepositAccountType:   WideString   Index (IS_OPTN) read FDepositAccountType write SetDepositAccountType stored DepositAccountType_Specified;
    property DepositAccountNumber: WideString   Index (IS_OPTN) read FDepositAccountNumber write SetDepositAccountNumber stored DepositAccountNumber_Specified;
    property DepositAmount:        TXSDecimal   read FDepositAmount write FDepositAmount;
    property DepositAmountYTD:     TXSDecimal   read FDepositAmountYTD write FDepositAmountYTD;
    property Indicator:            WideString   Index (IS_OPTN) read FIndicator write SetIndicator stored Indicator_Specified;
  end;



  // ************************************************************************ //
  // XML       : Paystub_Detail, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_Detail = class(TRemotable)
  private
    FPayDate: TXSDateTime;
    FIndicator: WideString;
    FIndicator_Specified: boolean;
    FRecordType: WideString;
    FRecordType_Specified: boolean;
    FItemDescription: WideString;
    FItemDescription_Specified: boolean;
    FAmount: TXSDecimal;
    FAmountYTD: TXSDecimal;
    FRate: TXSDecimal;
    FHours: TXSDecimal;
    FHoursYTD: TXSDecimal;
    FRecordTypeIndicator: WideString;
    FRecordTypeIndicator_Specified: boolean;
    FCheckNumber: WideString;
    FCheckNumber_Specified: boolean;
    procedure SetIndicator(Index: Integer; const AWideString: WideString);
    function  Indicator_Specified(Index: Integer): boolean;
    procedure SetRecordType(Index: Integer; const AWideString: WideString);
    function  RecordType_Specified(Index: Integer): boolean;
    procedure SetItemDescription(Index: Integer; const AWideString: WideString);
    function  ItemDescription_Specified(Index: Integer): boolean;
    procedure SetRecordTypeIndicator(Index: Integer; const AWideString: WideString);
    function  RecordTypeIndicator_Specified(Index: Integer): boolean;
    procedure SetCheckNumber(Index: Integer; const AWideString: WideString);
    function  CheckNumber_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PayDate:             TXSDateTime  read FPayDate write FPayDate;
    property Indicator:           WideString   Index (IS_OPTN) read FIndicator write SetIndicator stored Indicator_Specified;
    property RecordType:          WideString   Index (IS_OPTN) read FRecordType write SetRecordType stored RecordType_Specified;
    property ItemDescription:     WideString   Index (IS_OPTN) read FItemDescription write SetItemDescription stored ItemDescription_Specified;
    property Amount:              TXSDecimal   read FAmount write FAmount;
    property AmountYTD:           TXSDecimal   read FAmountYTD write FAmountYTD;
    property Rate:                TXSDecimal   read FRate write FRate;
    property Hours:               TXSDecimal   read FHours write FHours;
    property HoursYTD:            TXSDecimal   read FHoursYTD write FHoursYTD;
    property RecordTypeIndicator: WideString   Index (IS_OPTN) read FRecordTypeIndicator write SetRecordTypeIndicator stored RecordTypeIndicator_Specified;
    property CheckNumber:         WideString   Index (IS_OPTN) read FCheckNumber write SetCheckNumber stored CheckNumber_Specified;
  end;

  ArrayOfAccrualBalance = array of AccrualBalance;   { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : TimeOffBalanceRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  TimeOffBalanceRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FTimeOffBalance: ArrayOfAccrualBalance;
    FTimeOffBalance_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetTimeOffBalance(Index: Integer; const AArrayOfAccrualBalance: ArrayOfAccrualBalance);
    function  TimeOffBalance_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property SessionID:      WideString             Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:       WideString             Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property TimeOffBalance: ArrayOfAccrualBalance  Index (IS_OPTN) read FTimeOffBalance write SetTimeOffBalance stored TimeOffBalance_Specified;
  end;



  // ************************************************************************ //
  // XML       : AccrualBalance, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AccrualBalance = class(TRemotable)
  private
    FSSN: WideString;
    FSSN_Specified: boolean;
    FTransactionDate: TXSDateTime;
    FTimeOffType: WideString;
    FTimeOffType_Specified: boolean;
    FTransactionType: WideString;
    FTransactionType_Specified: boolean;
    FBalance: TXSDecimal;
    FComments: WideString;
    FComments_Specified: boolean;
    procedure SetSSN(Index: Integer; const AWideString: WideString);
    function  SSN_Specified(Index: Integer): boolean;
    procedure SetTimeOffType(Index: Integer; const AWideString: WideString);
    function  TimeOffType_Specified(Index: Integer): boolean;
    procedure SetTransactionType(Index: Integer; const AWideString: WideString);
    function  TransactionType_Specified(Index: Integer): boolean;
    procedure SetComments(Index: Integer; const AWideString: WideString);
    function  Comments_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property SSN:             WideString   Index (IS_OPTN) read FSSN write SetSSN stored SSN_Specified;
    property TransactionDate: TXSDateTime  read FTransactionDate write FTransactionDate;
    property TimeOffType:     WideString   Index (IS_OPTN) read FTimeOffType write SetTimeOffType stored TimeOffType_Specified;
    property TransactionType: WideString   Index (IS_OPTN) read FTransactionType write SetTransactionType stored TransactionType_Specified;
    property Balance:         TXSDecimal   read FBalance write FBalance;
    property Comments:        WideString   Index (IS_OPTN) read FComments write SetComments stored Comments_Specified;
  end;

  ArrayOfW2File = array of W2File;              { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : W2Request, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  W2Request = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FInfinityHREmployee_ID: Integer;
    FSSN: WideString;
    FSSN_Specified: boolean;
    FW2_Files: ArrayOfW2File;
    FW2_Files_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetSSN(Index: Integer; const AWideString: WideString);
    function  SSN_Specified(Index: Integer): boolean;
    procedure SetW2_Files(Index: Integer; const AArrayOfW2File: ArrayOfW2File);
    function  W2_Files_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property SessionID:             WideString     Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:              WideString     Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property InfinityHREmployee_ID: Integer        read FInfinityHREmployee_ID write FInfinityHREmployee_ID;
    property SSN:                   WideString     Index (IS_OPTN) read FSSN write SetSSN stored SSN_Specified;
    property W2_Files:              ArrayOfW2File  Index (IS_OPTN) read FW2_Files write SetW2_Files stored W2_Files_Specified;
  end;



  // ************************************************************************ //
  // XML       : W2File, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  W2File = class(TRemotable)
  private
    FNotes: WideString;
    FNotes_Specified: boolean;
    FTaxYear: Integer;
    FFileContent: TByteDynArray;
    FFileContent_Specified: boolean;
    FFileExtension: WideString;
    FFileExtension_Specified: boolean;
    FFileLength: Double;
    procedure SetNotes(Index: Integer; const AWideString: WideString);
    function  Notes_Specified(Index: Integer): boolean;
    procedure SetFileContent(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  FileContent_Specified(Index: Integer): boolean;
    procedure SetFileExtension(Index: Integer; const AWideString: WideString);
    function  FileExtension_Specified(Index: Integer): boolean;
  published
    property Notes:         WideString     Index (IS_OPTN) read FNotes write SetNotes stored Notes_Specified;
    property TaxYear:       Integer        read FTaxYear write FTaxYear;
    property FileContent:   TByteDynArray  Index (IS_OPTN) read FFileContent write SetFileContent stored FileContent_Specified;
    property FileExtension: WideString     Index (IS_OPTN) read FFileExtension write SetFileExtension stored FileExtension_Specified;
    property FileLength:    Double         read FFileLength write FFileLength;
  end;



  // ************************************************************************ //
  // XML       : BenefitRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  BenefitRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
  published
    property SessionID: WideString  Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:  WideString  Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
  end;

  ArrayOfBenefitStructureItem = array of BenefitStructureItem;   { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : BenefitStructure, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  BenefitStructure = class(TRemotable)
  private
    FBenefitStructureItems: ArrayOfBenefitStructureItem;
    FBenefitStructureItems_Specified: boolean;
    procedure SetBenefitStructureItems(Index: Integer; const AArrayOfBenefitStructureItem: ArrayOfBenefitStructureItem);
    function  BenefitStructureItems_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property BenefitStructureItems: ArrayOfBenefitStructureItem  Index (IS_OPTN) read FBenefitStructureItems write SetBenefitStructureItems stored BenefitStructureItems_Specified;
  end;



  // ************************************************************************ //
  // XML       : BenefitStructureItem, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  BenefitStructureItem = class(TRemotable)
  private
    FPackageName: WideString;
    FPackageName_Specified: boolean;
    FPackage_ID: Integer;
    FPackageEffectiveDate: TXSDateTime;
    FPackageExpirationDate: TXSDateTime;
    FBenefit_ID: Integer;
    FBenefitName: WideString;
    FBenefitName_Specified: boolean;
    FBenefitCarrierCode: WideString;
    FBenefitCarrierCode_Specified: boolean;
    FPlan_ID: Integer;
    FPlanName: WideString;
    FPlanName_Specified: boolean;
    FPlanCarrierCode: WideString;
    FPlanCarrierCode_Specified: boolean;
    FOption_ID: Integer;
    FOptionName: WideString;
    FOptionName_Specified: boolean;
    procedure SetPackageName(Index: Integer; const AWideString: WideString);
    function  PackageName_Specified(Index: Integer): boolean;
    procedure SetBenefitName(Index: Integer; const AWideString: WideString);
    function  BenefitName_Specified(Index: Integer): boolean;
    procedure SetBenefitCarrierCode(Index: Integer; const AWideString: WideString);
    function  BenefitCarrierCode_Specified(Index: Integer): boolean;
    procedure SetPlanName(Index: Integer; const AWideString: WideString);
    function  PlanName_Specified(Index: Integer): boolean;
    procedure SetPlanCarrierCode(Index: Integer; const AWideString: WideString);
    function  PlanCarrierCode_Specified(Index: Integer): boolean;
    procedure SetOptionName(Index: Integer; const AWideString: WideString);
    function  OptionName_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PackageName:           WideString   Index (IS_OPTN) read FPackageName write SetPackageName stored PackageName_Specified;
    property Package_ID:            Integer      read FPackage_ID write FPackage_ID;
    property PackageEffectiveDate:  TXSDateTime  read FPackageEffectiveDate write FPackageEffectiveDate;
    property PackageExpirationDate: TXSDateTime  read FPackageExpirationDate write FPackageExpirationDate;
    property Benefit_ID:            Integer      read FBenefit_ID write FBenefit_ID;
    property BenefitName:           WideString   Index (IS_OPTN) read FBenefitName write SetBenefitName stored BenefitName_Specified;
    property BenefitCarrierCode:    WideString   Index (IS_OPTN) read FBenefitCarrierCode write SetBenefitCarrierCode stored BenefitCarrierCode_Specified;
    property Plan_ID:               Integer      read FPlan_ID write FPlan_ID;
    property PlanName:              WideString   Index (IS_OPTN) read FPlanName write SetPlanName stored PlanName_Specified;
    property PlanCarrierCode:       WideString   Index (IS_OPTN) read FPlanCarrierCode write SetPlanCarrierCode stored PlanCarrierCode_Specified;
    property Option_ID:             Integer      read FOption_ID write FOption_ID;
    property OptionName:            WideString   Index (IS_OPTN) read FOptionName write SetOptionName stored OptionName_Specified;
  end;

  ArrayOfLookup = array of Lookup;              { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : LookupData, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  LookupData = class(TRemotable)
  private
    FLookups: ArrayOfLookup;
    FLookups_Specified: boolean;
    procedure SetLookups(Index: Integer; const AArrayOfLookup: ArrayOfLookup);
    function  Lookups_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Lookups: ArrayOfLookup  Index (IS_OPTN) read FLookups write SetLookups stored Lookups_Specified;
  end;



  // ************************************************************************ //
  // XML       : Lookup, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Lookup = class(TRemotable)
  private
    FID: Integer;
    FText: WideString;
    FText_Specified: boolean;
    procedure SetText(Index: Integer; const AWideString: WideString);
    function  Text_Specified(Index: Integer): boolean;
  published
    property ID:   Integer     read FID write FID;
    property Text: WideString  Index (IS_OPTN) read FText write SetText stored Text_Specified;
  end;

  ArrayOfPaySchedule = array of PaySchedule;    { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : PayScheduleData, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PayScheduleData = class(TRemotable)
  private
    FPaySchedules: ArrayOfPaySchedule;
    FPaySchedules_Specified: boolean;
    procedure SetPaySchedules(Index: Integer; const AArrayOfPaySchedule: ArrayOfPaySchedule);
    function  PaySchedules_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property PaySchedules: ArrayOfPaySchedule  Index (IS_OPTN) read FPaySchedules write SetPaySchedules stored PaySchedules_Specified;
  end;



  // ************************************************************************ //
  // XML       : PaySchedule, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaySchedule = class(TRemotable)
  private
    FID: Integer;
    FName_: WideString;
    FName__Specified: boolean;
    FDescription: WideString;
    FDescription_Specified: boolean;
    FPayPeriods: Integer;
    FIsDefaultSchedule: Boolean;
    procedure SetName_(Index: Integer; const AWideString: WideString);
    function  Name__Specified(Index: Integer): boolean;
    procedure SetDescription(Index: Integer; const AWideString: WideString);
    function  Description_Specified(Index: Integer): boolean;
  published
    property ID:                Integer     read FID write FID;
    property Name_:             WideString  Index (IS_OPTN) read FName_ write SetName_ stored Name__Specified;
    property Description:       WideString  Index (IS_OPTN) read FDescription write SetDescription stored Description_Specified;
    property PayPeriods:        Integer     read FPayPeriods write FPayPeriods;
    property IsDefaultSchedule: Boolean     read FIsDefaultSchedule write FIsDefaultSchedule;
  end;



  // ************************************************************************ //
  // XML       : CompanyRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CompanyRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
  published
    property SessionID: WideString  Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:  WideString  Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
  end;

  ArrayOfCostCenterInfo = array of CostCenterInfo;   { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : CostCenterData, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CostCenterData = class(TRemotable)
  private
    FCostCenters: ArrayOfCostCenterInfo;
    FCostCenters_Specified: boolean;
    procedure SetCostCenters(Index: Integer; const AArrayOfCostCenterInfo: ArrayOfCostCenterInfo);
    function  CostCenters_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property CostCenters: ArrayOfCostCenterInfo  Index (IS_OPTN) read FCostCenters write SetCostCenters stored CostCenters_Specified;
  end;



  // ************************************************************************ //
  // XML       : CostCenterInfo, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CostCenterInfo = class(TRemotable)
  private
    FCostCenter_ID: Integer;
    FCostCenterName: WideString;
    FCostCenterName_Specified: boolean;
    FCostCenterDescription: WideString;
    FCostCenterDescription_Specified: boolean;
    FCreatedDate: TXSDateTime;
    FModifiedDate: TXSDateTime;
    FLevel: Integer;
    FLevelName: WideString;
    FLevelName_Specified: boolean;
    FCostCenterParent_ID: Integer;
    FCostCenterParent: CostCenterInfo;
    FCostCenterParent_Specified: boolean;
    procedure SetCostCenterName(Index: Integer; const AWideString: WideString);
    function  CostCenterName_Specified(Index: Integer): boolean;
    procedure SetCostCenterDescription(Index: Integer; const AWideString: WideString);
    function  CostCenterDescription_Specified(Index: Integer): boolean;
    procedure SetLevelName(Index: Integer; const AWideString: WideString);
    function  LevelName_Specified(Index: Integer): boolean;
    procedure SetCostCenterParent(Index: Integer; const ACostCenterInfo: CostCenterInfo);
    function  CostCenterParent_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property CostCenter_ID:         Integer         read FCostCenter_ID write FCostCenter_ID;
    property CostCenterName:        WideString      Index (IS_OPTN) read FCostCenterName write SetCostCenterName stored CostCenterName_Specified;
    property CostCenterDescription: WideString      Index (IS_OPTN) read FCostCenterDescription write SetCostCenterDescription stored CostCenterDescription_Specified;
    property CreatedDate:           TXSDateTime     read FCreatedDate write FCreatedDate;
    property ModifiedDate:          TXSDateTime     read FModifiedDate write FModifiedDate;
    property Level:                 Integer         read FLevel write FLevel;
    property LevelName:             WideString      Index (IS_OPTN) read FLevelName write SetLevelName stored LevelName_Specified;
    property CostCenterParent_ID:   Integer         read FCostCenterParent_ID write FCostCenterParent_ID;
    property CostCenterParent:      CostCenterInfo  Index (IS_OPTN) read FCostCenterParent write SetCostCenterParent stored CostCenterParent_Specified;
  end;

  ArrayOfCompanyInfo = array of CompanyInfo;    { "http://www.infinity-ss.com/services"[GblCplx] }


  // ************************************************************************ //
  // XML       : CompanyData, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CompanyData = class(TRemotable)
  private
    FEnterpriseVendor_ID: WideString;
    FEnterpriseVendor_ID_Specified: boolean;
    FCompanies: ArrayOfCompanyInfo;
    FCompanies_Specified: boolean;
    procedure SetEnterpriseVendor_ID(Index: Integer; const AWideString: WideString);
    function  EnterpriseVendor_ID_Specified(Index: Integer): boolean;
    procedure SetCompanies(Index: Integer; const AArrayOfCompanyInfo: ArrayOfCompanyInfo);
    function  Companies_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property EnterpriseVendor_ID: WideString          Index (IS_OPTN) read FEnterpriseVendor_ID write SetEnterpriseVendor_ID stored EnterpriseVendor_ID_Specified;
    property Companies:           ArrayOfCompanyInfo  Index (IS_OPTN) read FCompanies write SetCompanies stored Companies_Specified;
  end;



  // ************************************************************************ //
  // XML       : CompanyInfo, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CompanyInfo = class(TRemotable)
  private
    FVendor_ID: WideString;
    FVendor_ID_Specified: boolean;
    FInfinityCompany_ID: WideString;
    FInfinityCompany_ID_Specified: boolean;
    FInfinityCompany_Name: WideString;
    FInfinityCompany_Name_Specified: boolean;
    FInfinityCompany_Status: WideString;
    FInfinityCompany_Status_Specified: boolean;
    FInfinityCompany_Type: WideString;
    FInfinityCompany_Type_Specified: boolean;
    FVendorCompany_ID: WideString;
    FVendorCompany_ID_Specified: boolean;
    FVendorCompanyPassword: WideString;
    FVendorCompanyPassword_Specified: boolean;
    FDirectDepositExcluded: Boolean;
    FCompensationExcluded: Boolean;
    FStateTaxExcluded: Boolean;
    FFederalTaxExcluded: Boolean;
    FDeductionsExcluded: Boolean;
    FCostCenterData: CostCenterData;
    FCostCenterData_Specified: boolean;
    procedure SetVendor_ID(Index: Integer; const AWideString: WideString);
    function  Vendor_ID_Specified(Index: Integer): boolean;
    procedure SetInfinityCompany_ID(Index: Integer; const AWideString: WideString);
    function  InfinityCompany_ID_Specified(Index: Integer): boolean;
    procedure SetInfinityCompany_Name(Index: Integer; const AWideString: WideString);
    function  InfinityCompany_Name_Specified(Index: Integer): boolean;
    procedure SetInfinityCompany_Status(Index: Integer; const AWideString: WideString);
    function  InfinityCompany_Status_Specified(Index: Integer): boolean;
    procedure SetInfinityCompany_Type(Index: Integer; const AWideString: WideString);
    function  InfinityCompany_Type_Specified(Index: Integer): boolean;
    procedure SetVendorCompany_ID(Index: Integer; const AWideString: WideString);
    function  VendorCompany_ID_Specified(Index: Integer): boolean;
    procedure SetVendorCompanyPassword(Index: Integer; const AWideString: WideString);
    function  VendorCompanyPassword_Specified(Index: Integer): boolean;
    procedure SetCostCenterData(Index: Integer; const ACostCenterData: CostCenterData);
    function  CostCenterData_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property Vendor_ID:              WideString      Index (IS_OPTN) read FVendor_ID write SetVendor_ID stored Vendor_ID_Specified;
    property InfinityCompany_ID:     WideString      Index (IS_OPTN) read FInfinityCompany_ID write SetInfinityCompany_ID stored InfinityCompany_ID_Specified;
    property InfinityCompany_Name:   WideString      Index (IS_OPTN) read FInfinityCompany_Name write SetInfinityCompany_Name stored InfinityCompany_Name_Specified;
    property InfinityCompany_Status: WideString      Index (IS_OPTN) read FInfinityCompany_Status write SetInfinityCompany_Status stored InfinityCompany_Status_Specified;
    property InfinityCompany_Type:   WideString      Index (IS_OPTN) read FInfinityCompany_Type write SetInfinityCompany_Type stored InfinityCompany_Type_Specified;
    property VendorCompany_ID:       WideString      Index (IS_OPTN) read FVendorCompany_ID write SetVendorCompany_ID stored VendorCompany_ID_Specified;
    property VendorCompanyPassword:  WideString      Index (IS_OPTN) read FVendorCompanyPassword write SetVendorCompanyPassword stored VendorCompanyPassword_Specified;
    property DirectDepositExcluded:  Boolean         read FDirectDepositExcluded write FDirectDepositExcluded;
    property CompensationExcluded:   Boolean         read FCompensationExcluded write FCompensationExcluded;
    property StateTaxExcluded:       Boolean         read FStateTaxExcluded write FStateTaxExcluded;
    property FederalTaxExcluded:     Boolean         read FFederalTaxExcluded write FFederalTaxExcluded;
    property DeductionsExcluded:     Boolean         read FDeductionsExcluded write FDeductionsExcluded;
    property CostCenterData:         CostCenterData  Index (IS_OPTN) read FCostCenterData write SetCostCenterData stored CostCenterData_Specified;
  end;



  // ************************************************************************ //
  // XML       : PayrollServiceException, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PayrollServiceException = class(TRemotable)
  private
    FErrorMessage: WideString;
    FErrorMessage_Specified: boolean;
    FErrorException: WideString;
    FErrorException_Specified: boolean;
    FErrorStackTrace: WideString;
    FErrorStackTrace_Specified: boolean;
    procedure SetErrorMessage(Index: Integer; const AWideString: WideString);
    function  ErrorMessage_Specified(Index: Integer): boolean;
    procedure SetErrorException(Index: Integer; const AWideString: WideString);
    function  ErrorException_Specified(Index: Integer): boolean;
    procedure SetErrorStackTrace(Index: Integer; const AWideString: WideString);
    function  ErrorStackTrace_Specified(Index: Integer): boolean;
  published
    property ErrorMessage:    WideString  Index (IS_OPTN) read FErrorMessage write SetErrorMessage stored ErrorMessage_Specified;
    property ErrorException:  WideString  Index (IS_OPTN) read FErrorException write SetErrorException stored ErrorException_Specified;
    property ErrorStackTrace: WideString  Index (IS_OPTN) read FErrorStackTrace write SetErrorStackTrace stored ErrorStackTrace_Specified;
  end;



  // ************************************************************************ //
  // XML       : AuditRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AuditRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FAuditMessage: WideString;
    FAuditMessage_Specified: boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetAuditMessage(Index: Integer; const AWideString: WideString);
    function  AuditMessage_Specified(Index: Integer): boolean;
  published
    property SessionID:    WideString  Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:     WideString  Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property AuditMessage: WideString  Index (IS_OPTN) read FAuditMessage write SetAuditMessage stored AuditMessage_Specified;
  end;



  // ************************************************************************ //
  // XML       : ErrorRequest, global, <complexType>
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  ErrorRequest = class(TRemotable)
  private
    FErrorMessage: WideString;
    FErrorMessage_Specified: boolean;
    FSessionID: WideString;
    FSessionID_Specified: boolean;
    FVendorID: WideString;
    FVendorID_Specified: boolean;
    FErrorInnerException: WideString;
    FErrorInnerException_Specified: boolean;
    FErrorStackTrace: WideString;
    FErrorStackTrace_Specified: boolean;
    FErrorProcedureName: WideString;
    FErrorProcedureName_Specified: boolean;
    procedure SetErrorMessage(Index: Integer; const AWideString: WideString);
    function  ErrorMessage_Specified(Index: Integer): boolean;
    procedure SetSessionID(Index: Integer; const AWideString: WideString);
    function  SessionID_Specified(Index: Integer): boolean;
    procedure SetVendorID(Index: Integer; const AWideString: WideString);
    function  VendorID_Specified(Index: Integer): boolean;
    procedure SetErrorInnerException(Index: Integer; const AWideString: WideString);
    function  ErrorInnerException_Specified(Index: Integer): boolean;
    procedure SetErrorStackTrace(Index: Integer; const AWideString: WideString);
    function  ErrorStackTrace_Specified(Index: Integer): boolean;
    procedure SetErrorProcedureName(Index: Integer; const AWideString: WideString);
    function  ErrorProcedureName_Specified(Index: Integer): boolean;
  published
    property ErrorMessage:        WideString  Index (IS_OPTN) read FErrorMessage write SetErrorMessage stored ErrorMessage_Specified;
    property SessionID:           WideString  Index (IS_OPTN) read FSessionID write SetSessionID stored SessionID_Specified;
    property VendorID:            WideString  Index (IS_OPTN) read FVendorID write SetVendorID stored VendorID_Specified;
    property ErrorInnerException: WideString  Index (IS_OPTN) read FErrorInnerException write SetErrorInnerException stored ErrorInnerException_Specified;
    property ErrorStackTrace:     WideString  Index (IS_OPTN) read FErrorStackTrace write SetErrorStackTrace stored ErrorStackTrace_Specified;
    property ErrorProcedureName:  WideString  Index (IS_OPTN) read FErrorProcedureName write SetErrorProcedureName stored ErrorProcedureName_Specified;
  end;


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // soapAction: http://www.infinity-ss.com/services/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : PayrollSyncSoap
  // service   : PayrollSync
  // port      : PayrollSyncSoap
  // URL       : https://www.infinityhrconnect.com/services/payrollsync.asmx
  // ************************************************************************ //
  PayrollSyncSoap = interface(IInvokable)
  ['{182DD499-16FD-3404-0686-6834DBB5202E}']
    function  Authenticate(const request: AuthRequest): AuthResponse; stdcall;
    procedure UpdateEmployees(const employeeRequest: EmployeeRequest; const employeeInfos: ArrayOfEmployeeInfo); stdcall;
    procedure MapEmployeesForSSO(const employeeRequest: EmployeeRequest; const employeeInfos: ArrayOfEmployeeInfo); stdcall;
    function  GetEmployeesByEffectiveDate(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime): EmployeeData; stdcall;
    function  GetEmployeesByDateRange(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime; const endDate: TXSDateTime): EmployeeData; stdcall;
    function  GetEmployeesByChangeType(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime; const endDate: TXSDateTime; const DataCategory_ID: Int64; const IncludeAdds: Boolean; const IncludeChanges: Boolean; 
                                       const IncludeDeletes: Boolean): EmployeeData; stdcall;
    function  GetEmployeeChanges(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime; const endDate: TXSDateTime; const DataCategories: ArrayOfInt; const IncludeAdds: Boolean; const IncludeChanges: Boolean; 
                                 const IncludeDeletes: Boolean): EmployeeData; stdcall;
    function  GetEmployeeByHREmployeeID(const employeeRequest: EmployeeRequest; const HREmployee_ID: Integer): EmployeeData; stdcall;
    function  GetEmployeeByEmployeeSSN(const employeeRequest: EmployeeRequest; const Employee_SSN: WideString): EmployeeData; stdcall;
    function  GetEmployeeByUserName(const employeeRequest: EmployeeRequest; const UserName: WideString): EmployeeData; stdcall;
    function  GetEmployeesAll(const employeeRequest: EmployeeRequest): EmployeeData; stdcall;
    function  GetEmployeeIDs_All(const employeeRequest: EmployeeRequest): EmployeeData; stdcall;
    function  GetEmployeeIDs_ByEffectiveDate(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime): EmployeeData; stdcall;
    function  UpsertEmployees(const empRequest: EmployeeRequest; const employeeData: EmployeeData): EmployeeData; stdcall;
    procedure UpsertEmployeePaystubs(const paystubRequest: PaystubRequest); stdcall;
    procedure UpsertEmployeePaystubData(const paystubDataRequest: PaystubDataRequest); stdcall;
    procedure UploadTimeOffBalance(const balanceRequest: TimeOffBalanceRequest); stdcall;
    procedure UploadEmployeeW2(const w2Request: W2Request); stdcall;
    procedure UploadEmployeeW2_BySSN(const w2Request: W2Request); stdcall;
    function  GetCurrentBenefitStructure(const benefitRequest: BenefitRequest): BenefitStructure; stdcall;
    function  GetCurrentBenefitStructureByEffectiveDate(const benefitRequest: BenefitRequest; const EffectiveDate: TXSDateTime): BenefitStructure; stdcall;
    function  GetEmployeeTypes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetPTOTypes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetPTOTransactionTypes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetDepartments(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetPaySchedules(const empRequest: EmployeeRequest; const PaySchedule_Year: Integer): PayScheduleData; stdcall;
    function  GetJobCodes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetCostCenters(const CompanyRequest: CompanyRequest): CostCenterData; stdcall;
    function  GetDataCategories(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetServerTime: TXSDateTime; stdcall;
    function  GetEnterpriseCompanyInfo(const CompanyRequest: CompanyRequest): CompanyData; stdcall;
    function  GetCompanyInfo(const CompanyRequest: CompanyRequest): CompanyData; stdcall;
    procedure LogException(const PayrollServiceException: PayrollServiceException); stdcall;
    procedure LogAudit(const audit: AuditRequest); stdcall;
    procedure LogEvent(const audit: AuditRequest); stdcall;
    procedure LogError(const serviceError: ErrorRequest); stdcall;
  end;

function GetPayrollSyncSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): PayrollSyncSoap;


implementation
  uses SysUtils;

function GetPayrollSyncSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): PayrollSyncSoap;
const
  defWSDL = 'https://www.infinityhrconnect.com/services/payrollsync.asmx?wsdl';
  defURL  = 'https://www.infinityhrconnect.com/services/payrollsync.asmx';
  defSvc  = 'PayrollSync';
  defPrt  = 'PayrollSyncSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as PayrollSyncSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


procedure AuthRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function AuthRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure AuthRequest.SetVendorPwd(Index: Integer; const AWideString: WideString);
begin
  FVendorPwd := AWideString;
  FVendorPwd_Specified := True;
end;

function AuthRequest.VendorPwd_Specified(Index: Integer): boolean;
begin
  Result := FVendorPwd_Specified;
end;

destructor AuthResponse.Destroy;
begin
  FreeAndNil(FSessionStart);
  FreeAndNil(FSessionEnd);
  FreeAndNil(FSyncDate);
  inherited Destroy;
end;

procedure AuthResponse.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function AuthResponse.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure EmployeeRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function EmployeeRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure EmployeeRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function EmployeeRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

destructor CoverageInfo.Destroy;
begin
  FreeAndNil(FPackageEffectiveDate);
  FreeAndNil(FPackageExpirationDate);
  FreeAndNil(FBenefit_EffectiveDate);
  FreeAndNil(FBenefit_ExpirationDate);
  FreeAndNil(FPremiumAmount);
  FreeAndNil(FCreditAmount);
  FreeAndNil(FEmployeeCost);
  FreeAndNil(FHSACredit);
  FreeAndNil(FEmployerCoveragePercent);
  FreeAndNil(FAllEarnings);
  FreeAndNil(FRateOfPay);
  FreeAndNil(FAllEarningsTestResult);
  FreeAndNil(FRateOfPayTestResult);
  inherited Destroy;
end;

procedure CoverageInfo.SetPackageName(Index: Integer; const AWideString: WideString);
begin
  FPackageName := AWideString;
  FPackageName_Specified := True;
end;

function CoverageInfo.PackageName_Specified(Index: Integer): boolean;
begin
  Result := FPackageName_Specified;
end;

procedure CoverageInfo.SetBenefitName(Index: Integer; const AWideString: WideString);
begin
  FBenefitName := AWideString;
  FBenefitName_Specified := True;
end;

function CoverageInfo.BenefitName_Specified(Index: Integer): boolean;
begin
  Result := FBenefitName_Specified;
end;

procedure CoverageInfo.SetPlanName(Index: Integer; const AWideString: WideString);
begin
  FPlanName := AWideString;
  FPlanName_Specified := True;
end;

function CoverageInfo.PlanName_Specified(Index: Integer): boolean;
begin
  Result := FPlanName_Specified;
end;

procedure CoverageInfo.SetOptionName(Index: Integer; const AWideString: WideString);
begin
  FOptionName := AWideString;
  FOptionName_Specified := True;
end;

function CoverageInfo.OptionName_Specified(Index: Integer): boolean;
begin
  Result := FOptionName_Specified;
end;

destructor EmployeeInfo.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FDeductions)-1 do
    FreeAndNil(FDeductions[I]);
  SetLength(FDeductions, 0);
  for I := 0 to Length(FCompensations)-1 do
    FreeAndNil(FCompensations[I]);
  SetLength(FCompensations, 0);
  for I := 0 to Length(FDirectDeposits)-1 do
    FreeAndNil(FDirectDeposits[I]);
  SetLength(FDirectDeposits, 0);
  for I := 0 to Length(FStateTax)-1 do
    FreeAndNil(FStateTax[I]);
  SetLength(FStateTax, 0);
  for I := 0 to Length(FFederalTax)-1 do
    FreeAndNil(FFederalTax[I]);
  SetLength(FFederalTax, 0);
  FreeAndNil(FBirthDate);
  FreeAndNil(FDefaultHours);
  FreeAndNil(FAdditionalHouseholdIncome);
  FreeAndNil(FHireDate);
  FreeAndNil(FOrigHireDate);
  FreeAndNil(FAlternateServiceDate1);
  FreeAndNil(FAlternateServiceDate2);
  FreeAndNil(FRetirementDate);
  FreeAndNil(FSalary);
  FreeAndNil(FReHireDate);
  FreeAndNil(FTerminationDate);
  FreeAndNil(FBenefitServiceDate);
  FreeAndNil(FEmpStatusEffDate);
  FreeAndNil(FVisaExpiration);
  FreeAndNil(FACA_LowestCost_Coverage);
  inherited Destroy;
end;

procedure EmployeeInfo.SetPayrollEmployeeID(Index: Integer; const AWideString: WideString);
begin
  FPayrollEmployeeID := AWideString;
  FPayrollEmployeeID_Specified := True;
end;

function EmployeeInfo.PayrollEmployeeID_Specified(Index: Integer): boolean;
begin
  Result := FPayrollEmployeeID_Specified;
end;

procedure EmployeeInfo.SetVendorCompany_ID(Index: Integer; const AWideString: WideString);
begin
  FVendorCompany_ID := AWideString;
  FVendorCompany_ID_Specified := True;
end;

function EmployeeInfo.VendorCompany_ID_Specified(Index: Integer): boolean;
begin
  Result := FVendorCompany_ID_Specified;
end;

procedure EmployeeInfo.SetLastName(Index: Integer; const AWideString: WideString);
begin
  FLastName := AWideString;
  FLastName_Specified := True;
end;

function EmployeeInfo.LastName_Specified(Index: Integer): boolean;
begin
  Result := FLastName_Specified;
end;

procedure EmployeeInfo.SetFirstName(Index: Integer; const AWideString: WideString);
begin
  FFirstName := AWideString;
  FFirstName_Specified := True;
end;

function EmployeeInfo.FirstName_Specified(Index: Integer): boolean;
begin
  Result := FFirstName_Specified;
end;

procedure EmployeeInfo.SetMiddleName(Index: Integer; const AWideString: WideString);
begin
  FMiddleName := AWideString;
  FMiddleName_Specified := True;
end;

function EmployeeInfo.MiddleName_Specified(Index: Integer): boolean;
begin
  Result := FMiddleName_Specified;
end;

procedure EmployeeInfo.SetUserName(Index: Integer; const AWideString: WideString);
begin
  FUserName := AWideString;
  FUserName_Specified := True;
end;

function EmployeeInfo.UserName_Specified(Index: Integer): boolean;
begin
  Result := FUserName_Specified;
end;

procedure EmployeeInfo.SetGender(Index: Integer; const AWideString: WideString);
begin
  FGender := AWideString;
  FGender_Specified := True;
end;

function EmployeeInfo.Gender_Specified(Index: Integer): boolean;
begin
  Result := FGender_Specified;
end;

procedure EmployeeInfo.SetEthnicity(Index: Integer; const AWideString: WideString);
begin
  FEthnicity := AWideString;
  FEthnicity_Specified := True;
end;

function EmployeeInfo.Ethnicity_Specified(Index: Integer): boolean;
begin
  Result := FEthnicity_Specified;
end;

procedure EmployeeInfo.SetOccupation(Index: Integer; const AWideString: WideString);
begin
  FOccupation := AWideString;
  FOccupation_Specified := True;
end;

function EmployeeInfo.Occupation_Specified(Index: Integer): boolean;
begin
  Result := FOccupation_Specified;
end;

procedure EmployeeInfo.SetMaritalStatus(Index: Integer; const AWideString: WideString);
begin
  FMaritalStatus := AWideString;
  FMaritalStatus_Specified := True;
end;

function EmployeeInfo.MaritalStatus_Specified(Index: Integer): boolean;
begin
  Result := FMaritalStatus_Specified;
end;

procedure EmployeeInfo.SetAddress1(Index: Integer; const AWideString: WideString);
begin
  FAddress1 := AWideString;
  FAddress1_Specified := True;
end;

function EmployeeInfo.Address1_Specified(Index: Integer): boolean;
begin
  Result := FAddress1_Specified;
end;

procedure EmployeeInfo.SetAddress2(Index: Integer; const AWideString: WideString);
begin
  FAddress2 := AWideString;
  FAddress2_Specified := True;
end;

function EmployeeInfo.Address2_Specified(Index: Integer): boolean;
begin
  Result := FAddress2_Specified;
end;

procedure EmployeeInfo.SetCity(Index: Integer; const AWideString: WideString);
begin
  FCity := AWideString;
  FCity_Specified := True;
end;

function EmployeeInfo.City_Specified(Index: Integer): boolean;
begin
  Result := FCity_Specified;
end;

procedure EmployeeInfo.SetHomePhone(Index: Integer; const AWideString: WideString);
begin
  FHomePhone := AWideString;
  FHomePhone_Specified := True;
end;

function EmployeeInfo.HomePhone_Specified(Index: Integer): boolean;
begin
  Result := FHomePhone_Specified;
end;

procedure EmployeeInfo.SetState(Index: Integer; const AWideString: WideString);
begin
  FState := AWideString;
  FState_Specified := True;
end;

function EmployeeInfo.State_Specified(Index: Integer): boolean;
begin
  Result := FState_Specified;
end;

procedure EmployeeInfo.SetStateCode(Index: Integer; const AWideString: WideString);
begin
  FStateCode := AWideString;
  FStateCode_Specified := True;
end;

function EmployeeInfo.StateCode_Specified(Index: Integer): boolean;
begin
  Result := FStateCode_Specified;
end;

procedure EmployeeInfo.SetZip(Index: Integer; const AWideString: WideString);
begin
  FZip := AWideString;
  FZip_Specified := True;
end;

function EmployeeInfo.Zip_Specified(Index: Integer): boolean;
begin
  Result := FZip_Specified;
end;

procedure EmployeeInfo.SetSSN(Index: Integer; const AWideString: WideString);
begin
  FSSN := AWideString;
  FSSN_Specified := True;
end;

function EmployeeInfo.SSN_Specified(Index: Integer): boolean;
begin
  Result := FSSN_Specified;
end;

procedure EmployeeInfo.SetCostCenter1(Index: Integer; const AWideString: WideString);
begin
  FCostCenter1 := AWideString;
  FCostCenter1_Specified := True;
end;

function EmployeeInfo.CostCenter1_Specified(Index: Integer): boolean;
begin
  Result := FCostCenter1_Specified;
end;

procedure EmployeeInfo.SetCostCenter2(Index: Integer; const AWideString: WideString);
begin
  FCostCenter2 := AWideString;
  FCostCenter2_Specified := True;
end;

function EmployeeInfo.CostCenter2_Specified(Index: Integer): boolean;
begin
  Result := FCostCenter2_Specified;
end;

procedure EmployeeInfo.SetCostCenter3(Index: Integer; const AWideString: WideString);
begin
  FCostCenter3 := AWideString;
  FCostCenter3_Specified := True;
end;

function EmployeeInfo.CostCenter3_Specified(Index: Integer): boolean;
begin
  Result := FCostCenter3_Specified;
end;

procedure EmployeeInfo.SetCostCenter4(Index: Integer; const AWideString: WideString);
begin
  FCostCenter4 := AWideString;
  FCostCenter4_Specified := True;
end;

function EmployeeInfo.CostCenter4_Specified(Index: Integer): boolean;
begin
  Result := FCostCenter4_Specified;
end;

procedure EmployeeInfo.SetCostCenter5(Index: Integer; const AWideString: WideString);
begin
  FCostCenter5 := AWideString;
  FCostCenter5_Specified := True;
end;

function EmployeeInfo.CostCenter5_Specified(Index: Integer): boolean;
begin
  Result := FCostCenter5_Specified;
end;

procedure EmployeeInfo.SetEmployeeStatus(Index: Integer; const AWideString: WideString);
begin
  FEmployeeStatus := AWideString;
  FEmployeeStatus_Specified := True;
end;

function EmployeeInfo.EmployeeStatus_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeStatus_Specified;
end;

procedure EmployeeInfo.SetEmployeeStatus_ACA(Index: Integer; const AWideString: WideString);
begin
  FEmployeeStatus_ACA := AWideString;
  FEmployeeStatus_ACA_Specified := True;
end;

function EmployeeInfo.EmployeeStatus_ACA_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeStatus_ACA_Specified;
end;

procedure EmployeeInfo.SetEmployeeStatus_ACA_Code(Index: Integer; const AWideString: WideString);
begin
  FEmployeeStatus_ACA_Code := AWideString;
  FEmployeeStatus_ACA_Code_Specified := True;
end;

function EmployeeInfo.EmployeeStatus_ACA_Code_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeStatus_ACA_Code_Specified;
end;

procedure EmployeeInfo.SetCountry(Index: Integer; const AWideString: WideString);
begin
  FCountry := AWideString;
  FCountry_Specified := True;
end;

function EmployeeInfo.Country_Specified(Index: Integer): boolean;
begin
  Result := FCountry_Specified;
end;

procedure EmployeeInfo.SetBenefitStatus(Index: Integer; const AWideString: WideString);
begin
  FBenefitStatus := AWideString;
  FBenefitStatus_Specified := True;
end;

function EmployeeInfo.BenefitStatus_Specified(Index: Integer): boolean;
begin
  Result := FBenefitStatus_Specified;
end;

procedure EmployeeInfo.SetDepartment(Index: Integer; const AWideString: WideString);
begin
  FDepartment := AWideString;
  FDepartment_Specified := True;
end;

function EmployeeInfo.Department_Specified(Index: Integer): boolean;
begin
  Result := FDepartment_Specified;
end;

procedure EmployeeInfo.SetWorkPhone(Index: Integer; const AWideString: WideString);
begin
  FWorkPhone := AWideString;
  FWorkPhone_Specified := True;
end;

function EmployeeInfo.WorkPhone_Specified(Index: Integer): boolean;
begin
  Result := FWorkPhone_Specified;
end;

procedure EmployeeInfo.SetWorkFax(Index: Integer; const AWideString: WideString);
begin
  FWorkFax := AWideString;
  FWorkFax_Specified := True;
end;

function EmployeeInfo.WorkFax_Specified(Index: Integer): boolean;
begin
  Result := FWorkFax_Specified;
end;

procedure EmployeeInfo.SetMobilePhone(Index: Integer; const AWideString: WideString);
begin
  FMobilePhone := AWideString;
  FMobilePhone_Specified := True;
end;

function EmployeeInfo.MobilePhone_Specified(Index: Integer): boolean;
begin
  Result := FMobilePhone_Specified;
end;

procedure EmployeeInfo.SetPayFrequency(Index: Integer; const AWideString: WideString);
begin
  FPayFrequency := AWideString;
  FPayFrequency_Specified := True;
end;

function EmployeeInfo.PayFrequency_Specified(Index: Integer): boolean;
begin
  Result := FPayFrequency_Specified;
end;

procedure EmployeeInfo.SetTaxForm(Index: Integer; const AWideString: WideString);
begin
  FTaxForm := AWideString;
  FTaxForm_Specified := True;
end;

function EmployeeInfo.TaxForm_Specified(Index: Integer): boolean;
begin
  Result := FTaxForm_Specified;
end;

procedure EmployeeInfo.SetWorkersCompCode(Index: Integer; const AWideString: WideString);
begin
  FWorkersCompCode := AWideString;
  FWorkersCompCode_Specified := True;
end;

function EmployeeInfo.WorkersCompCode_Specified(Index: Integer): boolean;
begin
  Result := FWorkersCompCode_Specified;
end;

procedure EmployeeInfo.SetEmployeeType(Index: Integer; const AWideString: WideString);
begin
  FEmployeeType := AWideString;
  FEmployeeType_Specified := True;
end;

function EmployeeInfo.EmployeeType_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeType_Specified;
end;

procedure EmployeeInfo.SetJobCode(Index: Integer; const AWideString: WideString);
begin
  FJobCode := AWideString;
  FJobCode_Specified := True;
end;

function EmployeeInfo.JobCode_Specified(Index: Integer): boolean;
begin
  Result := FJobCode_Specified;
end;

procedure EmployeeInfo.SetTerminationReason(Index: Integer; const AWideString: WideString);
begin
  FTerminationReason := AWideString;
  FTerminationReason_Specified := True;
end;

function EmployeeInfo.TerminationReason_Specified(Index: Integer): boolean;
begin
  Result := FTerminationReason_Specified;
end;

procedure EmployeeInfo.SetFacility1(Index: Integer; const AWideString: WideString);
begin
  FFacility1 := AWideString;
  FFacility1_Specified := True;
end;

function EmployeeInfo.Facility1_Specified(Index: Integer): boolean;
begin
  Result := FFacility1_Specified;
end;

procedure EmployeeInfo.SetFacility2(Index: Integer; const AWideString: WideString);
begin
  FFacility2 := AWideString;
  FFacility2_Specified := True;
end;

function EmployeeInfo.Facility2_Specified(Index: Integer): boolean;
begin
  Result := FFacility2_Specified;
end;

procedure EmployeeInfo.SetPTOManager(Index: Integer; const AWideString: WideString);
begin
  FPTOManager := AWideString;
  FPTOManager_Specified := True;
end;

function EmployeeInfo.PTOManager_Specified(Index: Integer): boolean;
begin
  Result := FPTOManager_Specified;
end;

procedure EmployeeInfo.SetPAManager(Index: Integer; const AWideString: WideString);
begin
  FPAManager := AWideString;
  FPAManager_Specified := True;
end;

function EmployeeInfo.PAManager_Specified(Index: Integer): boolean;
begin
  Result := FPAManager_Specified;
end;

procedure EmployeeInfo.SetPaySchedule_Master(Index: Integer; const AWideString: WideString);
begin
  FPaySchedule_Master := AWideString;
  FPaySchedule_Master_Specified := True;
end;

function EmployeeInfo.PaySchedule_Master_Specified(Index: Integer): boolean;
begin
  Result := FPaySchedule_Master_Specified;
end;

procedure EmployeeInfo.SetEmployee_UserDefined1(Index: Integer; const AWideString: WideString);
begin
  FEmployee_UserDefined1 := AWideString;
  FEmployee_UserDefined1_Specified := True;
end;

function EmployeeInfo.Employee_UserDefined1_Specified(Index: Integer): boolean;
begin
  Result := FEmployee_UserDefined1_Specified;
end;

procedure EmployeeInfo.SetEmployee_UserDefined2(Index: Integer; const AWideString: WideString);
begin
  FEmployee_UserDefined2 := AWideString;
  FEmployee_UserDefined2_Specified := True;
end;

function EmployeeInfo.Employee_UserDefined2_Specified(Index: Integer): boolean;
begin
  Result := FEmployee_UserDefined2_Specified;
end;

procedure EmployeeInfo.SetEmployee_UserDefined3(Index: Integer; const AWideString: WideString);
begin
  FEmployee_UserDefined3 := AWideString;
  FEmployee_UserDefined3_Specified := True;
end;

function EmployeeInfo.Employee_UserDefined3_Specified(Index: Integer): boolean;
begin
  Result := FEmployee_UserDefined3_Specified;
end;

procedure EmployeeInfo.SetEmployee_UserDefined4(Index: Integer; const AWideString: WideString);
begin
  FEmployee_UserDefined4 := AWideString;
  FEmployee_UserDefined4_Specified := True;
end;

function EmployeeInfo.Employee_UserDefined4_Specified(Index: Integer): boolean;
begin
  Result := FEmployee_UserDefined4_Specified;
end;

procedure EmployeeInfo.SetEmployee_UserDefined5(Index: Integer; const AWideString: WideString);
begin
  FEmployee_UserDefined5 := AWideString;
  FEmployee_UserDefined5_Specified := True;
end;

function EmployeeInfo.Employee_UserDefined5_Specified(Index: Integer): boolean;
begin
  Result := FEmployee_UserDefined5_Specified;
end;

procedure EmployeeInfo.SetSchedule(Index: Integer; const AWideString: WideString);
begin
  FSchedule := AWideString;
  FSchedule_Specified := True;
end;

function EmployeeInfo.Schedule_Specified(Index: Integer): boolean;
begin
  Result := FSchedule_Specified;
end;

procedure EmployeeInfo.SetPWD(Index: Integer; const AWideString: WideString);
begin
  FPWD := AWideString;
  FPWD_Specified := True;
end;

function EmployeeInfo.PWD_Specified(Index: Integer): boolean;
begin
  Result := FPWD_Specified;
end;

procedure EmployeeInfo.SetBaseRate(Index: Integer; const AWideString: WideString);
begin
  FBaseRate := AWideString;
  FBaseRate_Specified := True;
end;

function EmployeeInfo.BaseRate_Specified(Index: Integer): boolean;
begin
  Result := FBaseRate_Specified;
end;

procedure EmployeeInfo.SetPension(Index: Integer; const AWideString: WideString);
begin
  FPension := AWideString;
  FPension_Specified := True;
end;

function EmployeeInfo.Pension_Specified(Index: Integer): boolean;
begin
  Result := FPension_Specified;
end;

procedure EmployeeInfo.SetHighCompensation(Index: Integer; const AWideString: WideString);
begin
  FHighCompensation := AWideString;
  FHighCompensation_Specified := True;
end;

function EmployeeInfo.HighCompensation_Specified(Index: Integer): boolean;
begin
  Result := FHighCompensation_Specified;
end;

procedure EmployeeInfo.SetIsOwner(Index: Integer; const AWideString: WideString);
begin
  FIsOwner := AWideString;
  FIsOwner_Specified := True;
end;

function EmployeeInfo.IsOwner_Specified(Index: Integer): boolean;
begin
  Result := FIsOwner_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup1(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup1 := AWideString;
  FUserDefinedLookup1_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup1_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup1_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup2(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup2 := AWideString;
  FUserDefinedLookup2_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup2_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup2_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup3(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup3 := AWideString;
  FUserDefinedLookup3_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup3_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup3_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup4(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup4 := AWideString;
  FUserDefinedLookup4_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup4_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup4_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup5(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup5 := AWideString;
  FUserDefinedLookup5_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup5_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup5_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup6(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup6 := AWideString;
  FUserDefinedLookup6_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup6_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup6_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup7(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup7 := AWideString;
  FUserDefinedLookup7_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup7_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup7_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup8(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup8 := AWideString;
  FUserDefinedLookup8_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup8_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup8_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup9(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup9 := AWideString;
  FUserDefinedLookup9_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup9_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup9_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup10(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup10 := AWideString;
  FUserDefinedLookup10_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup10_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup10_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup11(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup11 := AWideString;
  FUserDefinedLookup11_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup11_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup11_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup12(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup12 := AWideString;
  FUserDefinedLookup12_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup12_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup12_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup13(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup13 := AWideString;
  FUserDefinedLookup13_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup13_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup13_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup14(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup14 := AWideString;
  FUserDefinedLookup14_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup14_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup14_Specified;
end;

procedure EmployeeInfo.SetUserDefinedLookup15(Index: Integer; const AWideString: WideString);
begin
  FUserDefinedLookup15 := AWideString;
  FUserDefinedLookup15_Specified := True;
end;

function EmployeeInfo.UserDefinedLookup15_Specified(Index: Integer): boolean;
begin
  Result := FUserDefinedLookup15_Specified;
end;

procedure EmployeeInfo.SetWorkAddress1(Index: Integer; const AWideString: WideString);
begin
  FWorkAddress1 := AWideString;
  FWorkAddress1_Specified := True;
end;

function EmployeeInfo.WorkAddress1_Specified(Index: Integer): boolean;
begin
  Result := FWorkAddress1_Specified;
end;

procedure EmployeeInfo.SetWorkAddress2(Index: Integer; const AWideString: WideString);
begin
  FWorkAddress2 := AWideString;
  FWorkAddress2_Specified := True;
end;

function EmployeeInfo.WorkAddress2_Specified(Index: Integer): boolean;
begin
  Result := FWorkAddress2_Specified;
end;

procedure EmployeeInfo.SetWorkEmail(Index: Integer; const AWideString: WideString);
begin
  FWorkEmail := AWideString;
  FWorkEmail_Specified := True;
end;

function EmployeeInfo.WorkEmail_Specified(Index: Integer): boolean;
begin
  Result := FWorkEmail_Specified;
end;

procedure EmployeeInfo.SetHomeEmail(Index: Integer; const AWideString: WideString);
begin
  FHomeEmail := AWideString;
  FHomeEmail_Specified := True;
end;

function EmployeeInfo.HomeEmail_Specified(Index: Integer): boolean;
begin
  Result := FHomeEmail_Specified;
end;

procedure EmployeeInfo.SetCompensationCategory(Index: Integer; const AWideString: WideString);
begin
  FCompensationCategory := AWideString;
  FCompensationCategory_Specified := True;
end;

function EmployeeInfo.CompensationCategory_Specified(Index: Integer): boolean;
begin
  Result := FCompensationCategory_Specified;
end;

procedure EmployeeInfo.SetEEOClass(Index: Integer; const AWideString: WideString);
begin
  FEEOClass := AWideString;
  FEEOClass_Specified := True;
end;

function EmployeeInfo.EEOClass_Specified(Index: Integer): boolean;
begin
  Result := FEEOClass_Specified;
end;

procedure EmployeeInfo.SetEEOClassDescription(Index: Integer; const AWideString: WideString);
begin
  FEEOClassDescription := AWideString;
  FEEOClassDescription_Specified := True;
end;

function EmployeeInfo.EEOClassDescription_Specified(Index: Integer): boolean;
begin
  Result := FEEOClassDescription_Specified;
end;

procedure EmployeeInfo.SetVisaType(Index: Integer; const AWideString: WideString);
begin
  FVisaType := AWideString;
  FVisaType_Specified := True;
end;

function EmployeeInfo.VisaType_Specified(Index: Integer): boolean;
begin
  Result := FVisaType_Specified;
end;

procedure EmployeeInfo.SetNickName(Index: Integer; const AWideString: WideString);
begin
  FNickName := AWideString;
  FNickName_Specified := True;
end;

function EmployeeInfo.NickName_Specified(Index: Integer): boolean;
begin
  Result := FNickName_Specified;
end;

procedure EmployeeInfo.SetDeductions(Index: Integer; const AArrayOfDeduction: ArrayOfDeduction);
begin
  FDeductions := AArrayOfDeduction;
  FDeductions_Specified := True;
end;

function EmployeeInfo.Deductions_Specified(Index: Integer): boolean;
begin
  Result := FDeductions_Specified;
end;

procedure EmployeeInfo.SetACA_LowestCost_Coverage(Index: Integer; const ACoverageInfo: CoverageInfo);
begin
  FACA_LowestCost_Coverage := ACoverageInfo;
  FACA_LowestCost_Coverage_Specified := True;
end;

function EmployeeInfo.ACA_LowestCost_Coverage_Specified(Index: Integer): boolean;
begin
  Result := FACA_LowestCost_Coverage_Specified;
end;

procedure EmployeeInfo.SetCompensations(Index: Integer; const AArrayOfCompensation: ArrayOfCompensation);
begin
  FCompensations := AArrayOfCompensation;
  FCompensations_Specified := True;
end;

function EmployeeInfo.Compensations_Specified(Index: Integer): boolean;
begin
  Result := FCompensations_Specified;
end;

procedure EmployeeInfo.SetDirectDeposits(Index: Integer; const AArrayOfDirectDeposit: ArrayOfDirectDeposit);
begin
  FDirectDeposits := AArrayOfDirectDeposit;
  FDirectDeposits_Specified := True;
end;

function EmployeeInfo.DirectDeposits_Specified(Index: Integer): boolean;
begin
  Result := FDirectDeposits_Specified;
end;

procedure EmployeeInfo.SetStateTax(Index: Integer; const AArrayOfStateTax: ArrayOfStateTax);
begin
  FStateTax := AArrayOfStateTax;
  FStateTax_Specified := True;
end;

function EmployeeInfo.StateTax_Specified(Index: Integer): boolean;
begin
  Result := FStateTax_Specified;
end;

procedure EmployeeInfo.SetFederalTax(Index: Integer; const AArrayOfFederalTax: ArrayOfFederalTax);
begin
  FFederalTax := AArrayOfFederalTax;
  FFederalTax_Specified := True;
end;

function EmployeeInfo.FederalTax_Specified(Index: Integer): boolean;
begin
  Result := FFederalTax_Specified;
end;

procedure EmployeeInfo.SetPayScheduleName(Index: Integer; const AWideString: WideString);
begin
  FPayScheduleName := AWideString;
  FPayScheduleName_Specified := True;
end;

function EmployeeInfo.PayScheduleName_Specified(Index: Integer): boolean;
begin
  Result := FPayScheduleName_Specified;
end;

destructor Deduction.Destroy;
begin
  FreeAndNil(FEffectiveDate);
  FreeAndNil(FExpirationDate);
  FreeAndNil(FPremiumAmount);
  FreeAndNil(FEmployeeCost);
  FreeAndNil(FEmployerCost);
  FreeAndNil(FCreditAmount);
  FreeAndNil(FCoverageAmount);
  FreeAndNil(FPreTaxDeduction);
  FreeAndNil(FPreTaxDeduction_PerPay);
  FreeAndNil(FPostTaxDeduction_PerPay);
  FreeAndNil(FPostTaxDeduction);
  FreeAndNil(FPerPayEmployeeCost);
  FreeAndNil(FPerPayEmployerCost);
  FreeAndNil(FAnnualPremiumAmount);
  FreeAndNil(FAnnualCreditAmount);
  FreeAndNil(FAnnualEmployeeCost);
  FreeAndNil(FDeductionEffectiveDate);
  FreeAndNil(FDeductionExpirationDate);
  FreeAndNil(FModifiedDate);
  inherited Destroy;
end;

procedure Deduction.SetBenefitType(Index: Integer; const AWideString: WideString);
begin
  FBenefitType := AWideString;
  FBenefitType_Specified := True;
end;

function Deduction.BenefitType_Specified(Index: Integer): boolean;
begin
  Result := FBenefitType_Specified;
end;

procedure Deduction.SetOptionName(Index: Integer; const AWideString: WideString);
begin
  FOptionName := AWideString;
  FOptionName_Specified := True;
end;

function Deduction.OptionName_Specified(Index: Integer): boolean;
begin
  Result := FOptionName_Specified;
end;

procedure Deduction.SetPlanName(Index: Integer; const AWideString: WideString);
begin
  FPlanName := AWideString;
  FPlanName_Specified := True;
end;

function Deduction.PlanName_Specified(Index: Integer): boolean;
begin
  Result := FPlanName_Specified;
end;

procedure Deduction.SetBenefitName(Index: Integer; const AWideString: WideString);
begin
  FBenefitName := AWideString;
  FBenefitName_Specified := True;
end;

function Deduction.BenefitName_Specified(Index: Integer): boolean;
begin
  Result := FBenefitName_Specified;
end;

procedure Deduction.SetPackageName(Index: Integer; const AWideString: WideString);
begin
  FPackageName := AWideString;
  FPackageName_Specified := True;
end;

function Deduction.PackageName_Specified(Index: Integer): boolean;
begin
  Result := FPackageName_Specified;
end;

procedure Deduction.SetDeductionCode(Index: Integer; const AWideString: WideString);
begin
  FDeductionCode := AWideString;
  FDeductionCode_Specified := True;
end;

function Deduction.DeductionCode_Specified(Index: Integer): boolean;
begin
  Result := FDeductionCode_Specified;
end;

procedure Deduction.SetDeductionCodeAlt1(Index: Integer; const AWideString: WideString);
begin
  FDeductionCodeAlt1 := AWideString;
  FDeductionCodeAlt1_Specified := True;
end;

function Deduction.DeductionCodeAlt1_Specified(Index: Integer): boolean;
begin
  Result := FDeductionCodeAlt1_Specified;
end;

procedure Deduction.SetDeductionCodeAlt2(Index: Integer; const AWideString: WideString);
begin
  FDeductionCodeAlt2 := AWideString;
  FDeductionCodeAlt2_Specified := True;
end;

function Deduction.DeductionCodeAlt2_Specified(Index: Integer): boolean;
begin
  Result := FDeductionCodeAlt2_Specified;
end;

procedure Deduction.SetCarrierCode(Index: Integer; const AWideString: WideString);
begin
  FCarrierCode := AWideString;
  FCarrierCode_Specified := True;
end;

function Deduction.CarrierCode_Specified(Index: Integer): boolean;
begin
  Result := FCarrierCode_Specified;
end;

procedure Deduction.SetCarrierPlanCode(Index: Integer; const AWideString: WideString);
begin
  FCarrierPlanCode := AWideString;
  FCarrierPlanCode_Specified := True;
end;

function Deduction.CarrierPlanCode_Specified(Index: Integer): boolean;
begin
  Result := FCarrierPlanCode_Specified;
end;

procedure Deduction.SetEDCode(Index: Integer; const AWideString: WideString);
begin
  FEDCode := AWideString;
  FEDCode_Specified := True;
end;

function Deduction.EDCode_Specified(Index: Integer): boolean;
begin
  Result := FEDCode_Specified;
end;

procedure Deduction.SetERDCode(Index: Integer; const AWideString: WideString);
begin
  FERDCode := AWideString;
  FERDCode_Specified := True;
end;

function Deduction.ERDCode_Specified(Index: Integer): boolean;
begin
  Result := FERDCode_Specified;
end;

procedure Deduction.SetBenefitCategory(Index: Integer; const AWideString: WideString);
begin
  FBenefitCategory := AWideString;
  FBenefitCategory_Specified := True;
end;

function Deduction.BenefitCategory_Specified(Index: Integer): boolean;
begin
  Result := FBenefitCategory_Specified;
end;

procedure Deduction.SetCoverageInputType(Index: Integer; const AWideString: WideString);
begin
  FCoverageInputType := AWideString;
  FCoverageInputType_Specified := True;
end;

function Deduction.CoverageInputType_Specified(Index: Integer): boolean;
begin
  Result := FCoverageInputType_Specified;
end;

destructor Compensation.Destroy;
begin
  FreeAndNil(FEffectiveDate);
  FreeAndNil(FRate);
  FreeAndNil(FHoursWorked);
  FreeAndNil(FAnnualRate);
  FreeAndNil(FHourlyRate);
  FreeAndNil(FOverTimeRate);
  FreeAndNil(FOtherRate);
  FreeAndNil(FLastModifiedDate);
  FreeAndNil(FPayPeriodAmount);
  FreeAndNil(FExpirationDate);
  inherited Destroy;
end;

procedure Compensation.SetNotes(Index: Integer; const AWideString: WideString);
begin
  FNotes := AWideString;
  FNotes_Specified := True;
end;

function Compensation.Notes_Specified(Index: Integer): boolean;
begin
  Result := FNotes_Specified;
end;

procedure Compensation.SetRateType(Index: Integer; const AWideString: WideString);
begin
  FRateType := AWideString;
  FRateType_Specified := True;
end;

function Compensation.RateType_Specified(Index: Integer): boolean;
begin
  Result := FRateType_Specified;
end;

procedure Compensation.SetRateCode(Index: Integer; const AWideString: WideString);
begin
  FRateCode := AWideString;
  FRateCode_Specified := True;
end;

function Compensation.RateCode_Specified(Index: Integer): boolean;
begin
  Result := FRateCode_Specified;
end;

destructor DirectDeposit.Destroy;
begin
  FreeAndNil(FDepositAmount);
  FreeAndNil(FDepositPercent);
  FreeAndNil(FModifiedDate);
  FreeAndNil(FEffectiveDate);
  FreeAndNil(FExpirationDate);
  inherited Destroy;
end;

procedure DirectDeposit.SetDirectDepositTransitNumber(Index: Integer; const AWideString: WideString);
begin
  FDirectDepositTransitNumber := AWideString;
  FDirectDepositTransitNumber_Specified := True;
end;

function DirectDeposit.DirectDepositTransitNumber_Specified(Index: Integer): boolean;
begin
  Result := FDirectDepositTransitNumber_Specified;
end;

procedure DirectDeposit.SetDirectDepositAccountNumber(Index: Integer; const AWideString: WideString);
begin
  FDirectDepositAccountNumber := AWideString;
  FDirectDepositAccountNumber_Specified := True;
end;

function DirectDeposit.DirectDepositAccountNumber_Specified(Index: Integer): boolean;
begin
  Result := FDirectDepositAccountNumber_Specified;
end;

procedure DirectDeposit.SetBankName(Index: Integer; const AWideString: WideString);
begin
  FBankName := AWideString;
  FBankName_Specified := True;
end;

function DirectDeposit.BankName_Specified(Index: Integer): boolean;
begin
  Result := FBankName_Specified;
end;

procedure DirectDeposit.SetAccountType(Index: Integer; const AWideString: WideString);
begin
  FAccountType := AWideString;
  FAccountType_Specified := True;
end;

function DirectDeposit.AccountType_Specified(Index: Integer): boolean;
begin
  Result := FAccountType_Specified;
end;

procedure DirectDeposit.SetEDCode(Index: Integer; const AWideString: WideString);
begin
  FEDCode := AWideString;
  FEDCode_Specified := True;
end;

function DirectDeposit.EDCode_Specified(Index: Integer): boolean;
begin
  Result := FEDCode_Specified;
end;

destructor StateTax.Destroy;
begin
  FreeAndNil(FAdditionalWithholding);
  FreeAndNil(FPercentWithholding);
  FreeAndNil(FEffectiveDate);
  FreeAndNil(FOverrideAmount);
  FreeAndNil(FOverridePercent);
  FreeAndNil(FAdditionalPercentWithheld);
  FreeAndNil(FStartDate);
  FreeAndNil(FExpirationDate);
  FreeAndNil(FModifiedDate);
  inherited Destroy;
end;

procedure StateTax.SetFilingStatus(Index: Integer; const AWideString: WideString);
begin
  FFilingStatus := AWideString;
  FFilingStatus_Specified := True;
end;

function StateTax.FilingStatus_Specified(Index: Integer): boolean;
begin
  Result := FFilingStatus_Specified;
end;

procedure StateTax.SetIncomeFilingState(Index: Integer; const AWideString: WideString);
begin
  FIncomeFilingState := AWideString;
  FIncomeFilingState_Specified := True;
end;

function StateTax.IncomeFilingState_Specified(Index: Integer): boolean;
begin
  Result := FIncomeFilingState_Specified;
end;

procedure StateTax.SetUnemploymentState(Index: Integer; const AWideString: WideString);
begin
  FUnemploymentState := AWideString;
  FUnemploymentState_Specified := True;
end;

function StateTax.UnemploymentState_Specified(Index: Integer): boolean;
begin
  Result := FUnemploymentState_Specified;
end;

procedure StateTax.SetCounty(Index: Integer; const AWideString: WideString);
begin
  FCounty := AWideString;
  FCounty_Specified := True;
end;

function StateTax.County_Specified(Index: Integer): boolean;
begin
  Result := FCounty_Specified;
end;

procedure StateTax.SetClaimAmount(Index: Integer; const AWideString: WideString);
begin
  FClaimAmount := AWideString;
  FClaimAmount_Specified := True;
end;

function StateTax.ClaimAmount_Specified(Index: Integer): boolean;
begin
  Result := FClaimAmount_Specified;
end;

destructor FederalTax.Destroy;
begin
  FreeAndNil(FAdditionalWithholding);
  FreeAndNil(FEffectiveDate);
  FreeAndNil(FOverrideAmount);
  FreeAndNil(FOverridePercent);
  FreeAndNil(FStartDate);
  FreeAndNil(FExpirationDate);
  FreeAndNil(FModifiedDate);
  inherited Destroy;
end;

procedure FederalTax.SetMaritalStatus(Index: Integer; const AWideString: WideString);
begin
  FMaritalStatus := AWideString;
  FMaritalStatus_Specified := True;
end;

function FederalTax.MaritalStatus_Specified(Index: Integer): boolean;
begin
  Result := FMaritalStatus_Specified;
end;

destructor EmployeeData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FEmployees)-1 do
    FreeAndNil(FEmployees[I]);
  SetLength(FEmployees, 0);
  for I := 0 to Length(FEmployeesSkinny)-1 do
    FreeAndNil(FEmployeesSkinny[I]);
  SetLength(FEmployeesSkinny, 0);
  FreeAndNil(FSyncDate);
  inherited Destroy;
end;

procedure EmployeeData.SetEmployees(Index: Integer; const AArrayOfEmployeeInfo: ArrayOfEmployeeInfo);
begin
  FEmployees := AArrayOfEmployeeInfo;
  FEmployees_Specified := True;
end;

function EmployeeData.Employees_Specified(Index: Integer): boolean;
begin
  Result := FEmployees_Specified;
end;

procedure EmployeeData.SetEmployeesSkinny(Index: Integer; const AArrayOfEmployeeInfoSkinny: ArrayOfEmployeeInfoSkinny);
begin
  FEmployeesSkinny := AArrayOfEmployeeInfoSkinny;
  FEmployeesSkinny_Specified := True;
end;

function EmployeeData.EmployeesSkinny_Specified(Index: Integer): boolean;
begin
  Result := FEmployeesSkinny_Specified;
end;

destructor EmployeeInfoSkinny.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FChangeFields)-1 do
    FreeAndNil(FChangeFields[I]);
  SetLength(FChangeFields, 0);
  for I := 0 to Length(FDeductions)-1 do
    FreeAndNil(FDeductions[I]);
  SetLength(FDeductions, 0);
  for I := 0 to Length(FCompensations)-1 do
    FreeAndNil(FCompensations[I]);
  SetLength(FCompensations, 0);
  for I := 0 to Length(FDirectDeposits)-1 do
    FreeAndNil(FDirectDeposits[I]);
  SetLength(FDirectDeposits, 0);
  for I := 0 to Length(FStateTax)-1 do
    FreeAndNil(FStateTax[I]);
  SetLength(FStateTax, 0);
  for I := 0 to Length(FFederalTax)-1 do
    FreeAndNil(FFederalTax[I]);
  SetLength(FFederalTax, 0);
  inherited Destroy;
end;

procedure EmployeeInfoSkinny.SetPayrollEmployeeID(Index: Integer; const AWideString: WideString);
begin
  FPayrollEmployeeID := AWideString;
  FPayrollEmployeeID_Specified := True;
end;

function EmployeeInfoSkinny.PayrollEmployeeID_Specified(Index: Integer): boolean;
begin
  Result := FPayrollEmployeeID_Specified;
end;

procedure EmployeeInfoSkinny.SetEmployeeSSN(Index: Integer; const AWideString: WideString);
begin
  FEmployeeSSN := AWideString;
  FEmployeeSSN_Specified := True;
end;

function EmployeeInfoSkinny.EmployeeSSN_Specified(Index: Integer): boolean;
begin
  Result := FEmployeeSSN_Specified;
end;

procedure EmployeeInfoSkinny.SetFirstName(Index: Integer; const AWideString: WideString);
begin
  FFirstName := AWideString;
  FFirstName_Specified := True;
end;

function EmployeeInfoSkinny.FirstName_Specified(Index: Integer): boolean;
begin
  Result := FFirstName_Specified;
end;

procedure EmployeeInfoSkinny.SetLastName(Index: Integer; const AWideString: WideString);
begin
  FLastName := AWideString;
  FLastName_Specified := True;
end;

function EmployeeInfoSkinny.LastName_Specified(Index: Integer): boolean;
begin
  Result := FLastName_Specified;
end;

procedure EmployeeInfoSkinny.SetDepartment(Index: Integer; const AWideString: WideString);
begin
  FDepartment := AWideString;
  FDepartment_Specified := True;
end;

function EmployeeInfoSkinny.Department_Specified(Index: Integer): boolean;
begin
  Result := FDepartment_Specified;
end;

procedure EmployeeInfoSkinny.SetVendorCompany_ID(Index: Integer; const AWideString: WideString);
begin
  FVendorCompany_ID := AWideString;
  FVendorCompany_ID_Specified := True;
end;

function EmployeeInfoSkinny.VendorCompany_ID_Specified(Index: Integer): boolean;
begin
  Result := FVendorCompany_ID_Specified;
end;

procedure EmployeeInfoSkinny.SetChangeFields(Index: Integer; const AArrayOfFieldInfo: ArrayOfFieldInfo);
begin
  FChangeFields := AArrayOfFieldInfo;
  FChangeFields_Specified := True;
end;

function EmployeeInfoSkinny.ChangeFields_Specified(Index: Integer): boolean;
begin
  Result := FChangeFields_Specified;
end;

procedure EmployeeInfoSkinny.SetDeductions(Index: Integer; const AArrayOfDeduction: ArrayOfDeduction);
begin
  FDeductions := AArrayOfDeduction;
  FDeductions_Specified := True;
end;

function EmployeeInfoSkinny.Deductions_Specified(Index: Integer): boolean;
begin
  Result := FDeductions_Specified;
end;

procedure EmployeeInfoSkinny.SetCompensations(Index: Integer; const AArrayOfCompensation: ArrayOfCompensation);
begin
  FCompensations := AArrayOfCompensation;
  FCompensations_Specified := True;
end;

function EmployeeInfoSkinny.Compensations_Specified(Index: Integer): boolean;
begin
  Result := FCompensations_Specified;
end;

procedure EmployeeInfoSkinny.SetDirectDeposits(Index: Integer; const AArrayOfDirectDeposit: ArrayOfDirectDeposit);
begin
  FDirectDeposits := AArrayOfDirectDeposit;
  FDirectDeposits_Specified := True;
end;

function EmployeeInfoSkinny.DirectDeposits_Specified(Index: Integer): boolean;
begin
  Result := FDirectDeposits_Specified;
end;

procedure EmployeeInfoSkinny.SetStateTax(Index: Integer; const AArrayOfStateTax: ArrayOfStateTax);
begin
  FStateTax := AArrayOfStateTax;
  FStateTax_Specified := True;
end;

function EmployeeInfoSkinny.StateTax_Specified(Index: Integer): boolean;
begin
  Result := FStateTax_Specified;
end;

procedure EmployeeInfoSkinny.SetFederalTax(Index: Integer; const AArrayOfFederalTax: ArrayOfFederalTax);
begin
  FFederalTax := AArrayOfFederalTax;
  FFederalTax_Specified := True;
end;

function EmployeeInfoSkinny.FederalTax_Specified(Index: Integer): boolean;
begin
  Result := FFederalTax_Specified;
end;

destructor FieldInfo.Destroy;
begin
  FreeAndNil(FChangeDate);
  inherited Destroy;
end;

procedure FieldInfo.SetFieldName(Index: Integer; const AWideString: WideString);
begin
  FFieldName := AWideString;
  FFieldName_Specified := True;
end;

function FieldInfo.FieldName_Specified(Index: Integer): boolean;
begin
  Result := FFieldName_Specified;
end;

procedure FieldInfo.SetValueBefore(Index: Integer; const AWideString: WideString);
begin
  FValueBefore := AWideString;
  FValueBefore_Specified := True;
end;

function FieldInfo.ValueBefore_Specified(Index: Integer): boolean;
begin
  Result := FValueBefore_Specified;
end;

procedure FieldInfo.SetValueAfter(Index: Integer; const AWideString: WideString);
begin
  FValueAfter := AWideString;
  FValueAfter_Specified := True;
end;

function FieldInfo.ValueAfter_Specified(Index: Integer): boolean;
begin
  Result := FValueAfter_Specified;
end;

procedure FieldInfo.SetFieldDataType(Index: Integer; const AWideString: WideString);
begin
  FFieldDataType := AWideString;
  FFieldDataType_Specified := True;
end;

function FieldInfo.FieldDataType_Specified(Index: Integer): boolean;
begin
  Result := FFieldDataType_Specified;
end;

procedure FieldInfo.SetRecordInfo(Index: Integer; const AWideString: WideString);
begin
  FRecordInfo := AWideString;
  FRecordInfo_Specified := True;
end;

function FieldInfo.RecordInfo_Specified(Index: Integer): boolean;
begin
  Result := FRecordInfo_Specified;
end;

destructor PaystubRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPaystub_Files)-1 do
    FreeAndNil(FPaystub_Files[I]);
  SetLength(FPaystub_Files, 0);
  inherited Destroy;
end;

procedure PaystubRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function PaystubRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure PaystubRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function PaystubRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure PaystubRequest.SetSSN(Index: Integer; const AWideString: WideString);
begin
  FSSN := AWideString;
  FSSN_Specified := True;
end;

function PaystubRequest.SSN_Specified(Index: Integer): boolean;
begin
  Result := FSSN_Specified;
end;

procedure PaystubRequest.SetPaystub_Files(Index: Integer; const AArrayOfPaystubFile: ArrayOfPaystubFile);
begin
  FPaystub_Files := AArrayOfPaystubFile;
  FPaystub_Files_Specified := True;
end;

function PaystubRequest.Paystub_Files_Specified(Index: Integer): boolean;
begin
  Result := FPaystub_Files_Specified;
end;

destructor PaystubFile.Destroy;
begin
  FreeAndNil(FPayPeriod_StartDate);
  FreeAndNil(FPayPeriod_EndDate);
  FreeAndNil(FPayDate);
  FreeAndNil(FDepositAmount);
  inherited Destroy;
end;

procedure PaystubFile.SetCheckNumber(Index: Integer; const AWideString: WideString);
begin
  FCheckNumber := AWideString;
  FCheckNumber_Specified := True;
end;

function PaystubFile.CheckNumber_Specified(Index: Integer): boolean;
begin
  Result := FCheckNumber_Specified;
end;

procedure PaystubFile.SetIndicator(Index: Integer; const AWideString: WideString);
begin
  FIndicator := AWideString;
  FIndicator_Specified := True;
end;

function PaystubFile.Indicator_Specified(Index: Integer): boolean;
begin
  Result := FIndicator_Specified;
end;

procedure PaystubFile.SetFileContent(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FFileContent := ATByteDynArray;
  FFileContent_Specified := True;
end;

function PaystubFile.FileContent_Specified(Index: Integer): boolean;
begin
  Result := FFileContent_Specified;
end;

procedure PaystubFile.SetFileName(Index: Integer; const AWideString: WideString);
begin
  FFileName := AWideString;
  FFileName_Specified := True;
end;

function PaystubFile.FileName_Specified(Index: Integer): boolean;
begin
  Result := FFileName_Specified;
end;

procedure PaystubFile.SetFileExtension(Index: Integer; const AWideString: WideString);
begin
  FFileExtension := AWideString;
  FFileExtension_Specified := True;
end;

function PaystubFile.FileExtension_Specified(Index: Integer): boolean;
begin
  Result := FFileExtension_Specified;
end;

destructor PaystubDataRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPaystub_Data)-1 do
    FreeAndNil(FPaystub_Data[I]);
  SetLength(FPaystub_Data, 0);
  inherited Destroy;
end;

procedure PaystubDataRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function PaystubDataRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure PaystubDataRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function PaystubDataRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure PaystubDataRequest.SetSSN(Index: Integer; const AWideString: WideString);
begin
  FSSN := AWideString;
  FSSN_Specified := True;
end;

function PaystubDataRequest.SSN_Specified(Index: Integer): boolean;
begin
  Result := FSSN_Specified;
end;

procedure PaystubDataRequest.SetPaystub_Data(Index: Integer; const AArrayOfPaystubData: ArrayOfPaystubData);
begin
  FPaystub_Data := AArrayOfPaystubData;
  FPaystub_Data_Specified := True;
end;

function PaystubDataRequest.Paystub_Data_Specified(Index: Integer): boolean;
begin
  Result := FPaystub_Data_Specified;
end;

destructor PaystubData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPayStub_Summary)-1 do
    FreeAndNil(FPayStub_Summary[I]);
  SetLength(FPayStub_Summary, 0);
  for I := 0 to Length(FPayStub_Detail)-1 do
    FreeAndNil(FPayStub_Detail[I]);
  SetLength(FPayStub_Detail, 0);
  FreeAndNil(FPayStub_TimeOff);
  FreeAndNil(FPayStub_Tax);
  inherited Destroy;
end;

procedure PaystubData.SetPayStub_Summary(Index: Integer; const AArrayOfPaystub_Summary: ArrayOfPaystub_Summary);
begin
  FPayStub_Summary := AArrayOfPaystub_Summary;
  FPayStub_Summary_Specified := True;
end;

function PaystubData.PayStub_Summary_Specified(Index: Integer): boolean;
begin
  Result := FPayStub_Summary_Specified;
end;

procedure PaystubData.SetPayStub_Detail(Index: Integer; const AArrayOfPaystub_Detail: ArrayOfPaystub_Detail);
begin
  FPayStub_Detail := AArrayOfPaystub_Detail;
  FPayStub_Detail_Specified := True;
end;

function PaystubData.PayStub_Detail_Specified(Index: Integer): boolean;
begin
  Result := FPayStub_Detail_Specified;
end;

procedure PaystubData.SetPayStub_TimeOff(Index: Integer; const APaystub_TimeOff: Paystub_TimeOff);
begin
  FPayStub_TimeOff := APaystub_TimeOff;
  FPayStub_TimeOff_Specified := True;
end;

function PaystubData.PayStub_TimeOff_Specified(Index: Integer): boolean;
begin
  Result := FPayStub_TimeOff_Specified;
end;

procedure PaystubData.SetPayStub_Tax(Index: Integer; const APaystub_Tax: Paystub_Tax);
begin
  FPayStub_Tax := APaystub_Tax;
  FPayStub_Tax_Specified := True;
end;

function PaystubData.PayStub_Tax_Specified(Index: Integer): boolean;
begin
  Result := FPayStub_Tax_Specified;
end;

destructor Paystub_TimeOff.Destroy;
begin
  FreeAndNil(FPayDate);
  FreeAndNil(FStartBalance);
  FreeAndNil(FEarned);
  FreeAndNil(FTaken);
  FreeAndNil(FAdjustment);
  FreeAndNil(FEndBalance);
  inherited Destroy;
end;

procedure Paystub_TimeOff.SetIndicator(Index: Integer; const AWideString: WideString);
begin
  FIndicator := AWideString;
  FIndicator_Specified := True;
end;

function Paystub_TimeOff.Indicator_Specified(Index: Integer): boolean;
begin
  Result := FIndicator_Specified;
end;

procedure Paystub_TimeOff.SetLeaveType(Index: Integer; const AWideString: WideString);
begin
  FLeaveType := AWideString;
  FLeaveType_Specified := True;
end;

function Paystub_TimeOff.LeaveType_Specified(Index: Integer): boolean;
begin
  Result := FLeaveType_Specified;
end;

destructor Paystub_Tax.Destroy;
begin
  FreeAndNil(FPayDate);
  FreeAndNil(FFederalAllowances);
  FreeAndNil(FFederalAdditionalAllowances);
  FreeAndNil(FFederalAdditionalAmount);
  FreeAndNil(FStateAllowances);
  FreeAndNil(FStateAdditionalAllowances);
  FreeAndNil(FStateAdditionalPercentage);
  FreeAndNil(FStateAdditionalAmount);
  inherited Destroy;
end;

procedure Paystub_Tax.SetIndicator(Index: Integer; const AWideString: WideString);
begin
  FIndicator := AWideString;
  FIndicator_Specified := True;
end;

function Paystub_Tax.Indicator_Specified(Index: Integer): boolean;
begin
  Result := FIndicator_Specified;
end;

procedure Paystub_Tax.SetFederalFilingStatus(Index: Integer; const AWideString: WideString);
begin
  FFederalFilingStatus := AWideString;
  FFederalFilingStatus_Specified := True;
end;

function Paystub_Tax.FederalFilingStatus_Specified(Index: Integer): boolean;
begin
  Result := FFederalFilingStatus_Specified;
end;

procedure Paystub_Tax.SetFilingState(Index: Integer; const AWideString: WideString);
begin
  FFilingState := AWideString;
  FFilingState_Specified := True;
end;

function Paystub_Tax.FilingState_Specified(Index: Integer): boolean;
begin
  Result := FFilingState_Specified;
end;

procedure Paystub_Tax.SetUnemploymentFilingState(Index: Integer; const AWideString: WideString);
begin
  FUnemploymentFilingState := AWideString;
  FUnemploymentFilingState_Specified := True;
end;

function Paystub_Tax.UnemploymentFilingState_Specified(Index: Integer): boolean;
begin
  Result := FUnemploymentFilingState_Specified;
end;

procedure Paystub_Tax.SetStateTaxFilingStatus(Index: Integer; const AWideString: WideString);
begin
  FStateTaxFilingStatus := AWideString;
  FStateTaxFilingStatus_Specified := True;
end;

function Paystub_Tax.StateTaxFilingStatus_Specified(Index: Integer): boolean;
begin
  Result := FStateTaxFilingStatus_Specified;
end;

destructor Paystub_Summary.Destroy;
begin
  FreeAndNil(FPayPeriod_Start);
  FreeAndNil(FPayPeriod_End);
  FreeAndNil(FPayDate);
  FreeAndNil(FDepositAmount);
  FreeAndNil(FDepositAmountYTD);
  inherited Destroy;
end;

procedure Paystub_Summary.SetPayGroup(Index: Integer; const AWideString: WideString);
begin
  FPayGroup := AWideString;
  FPayGroup_Specified := True;
end;

function Paystub_Summary.PayGroup_Specified(Index: Integer): boolean;
begin
  Result := FPayGroup_Specified;
end;

procedure Paystub_Summary.SetDepositType(Index: Integer; const AWideString: WideString);
begin
  FDepositType := AWideString;
  FDepositType_Specified := True;
end;

function Paystub_Summary.DepositType_Specified(Index: Integer): boolean;
begin
  Result := FDepositType_Specified;
end;

procedure Paystub_Summary.SetCheckNumber(Index: Integer; const AWideString: WideString);
begin
  FCheckNumber := AWideString;
  FCheckNumber_Specified := True;
end;

function Paystub_Summary.CheckNumber_Specified(Index: Integer): boolean;
begin
  Result := FCheckNumber_Specified;
end;

procedure Paystub_Summary.SetDepositAccountType(Index: Integer; const AWideString: WideString);
begin
  FDepositAccountType := AWideString;
  FDepositAccountType_Specified := True;
end;

function Paystub_Summary.DepositAccountType_Specified(Index: Integer): boolean;
begin
  Result := FDepositAccountType_Specified;
end;

procedure Paystub_Summary.SetDepositAccountNumber(Index: Integer; const AWideString: WideString);
begin
  FDepositAccountNumber := AWideString;
  FDepositAccountNumber_Specified := True;
end;

function Paystub_Summary.DepositAccountNumber_Specified(Index: Integer): boolean;
begin
  Result := FDepositAccountNumber_Specified;
end;

procedure Paystub_Summary.SetIndicator(Index: Integer; const AWideString: WideString);
begin
  FIndicator := AWideString;
  FIndicator_Specified := True;
end;

function Paystub_Summary.Indicator_Specified(Index: Integer): boolean;
begin
  Result := FIndicator_Specified;
end;

destructor Paystub_Detail.Destroy;
begin
  FreeAndNil(FPayDate);
  FreeAndNil(FAmount);
  FreeAndNil(FAmountYTD);
  FreeAndNil(FRate);
  FreeAndNil(FHours);
  FreeAndNil(FHoursYTD);
  inherited Destroy;
end;

procedure Paystub_Detail.SetIndicator(Index: Integer; const AWideString: WideString);
begin
  FIndicator := AWideString;
  FIndicator_Specified := True;
end;

function Paystub_Detail.Indicator_Specified(Index: Integer): boolean;
begin
  Result := FIndicator_Specified;
end;

procedure Paystub_Detail.SetRecordType(Index: Integer; const AWideString: WideString);
begin
  FRecordType := AWideString;
  FRecordType_Specified := True;
end;

function Paystub_Detail.RecordType_Specified(Index: Integer): boolean;
begin
  Result := FRecordType_Specified;
end;

procedure Paystub_Detail.SetItemDescription(Index: Integer; const AWideString: WideString);
begin
  FItemDescription := AWideString;
  FItemDescription_Specified := True;
end;

function Paystub_Detail.ItemDescription_Specified(Index: Integer): boolean;
begin
  Result := FItemDescription_Specified;
end;

procedure Paystub_Detail.SetRecordTypeIndicator(Index: Integer; const AWideString: WideString);
begin
  FRecordTypeIndicator := AWideString;
  FRecordTypeIndicator_Specified := True;
end;

function Paystub_Detail.RecordTypeIndicator_Specified(Index: Integer): boolean;
begin
  Result := FRecordTypeIndicator_Specified;
end;

procedure Paystub_Detail.SetCheckNumber(Index: Integer; const AWideString: WideString);
begin
  FCheckNumber := AWideString;
  FCheckNumber_Specified := True;
end;

function Paystub_Detail.CheckNumber_Specified(Index: Integer): boolean;
begin
  Result := FCheckNumber_Specified;
end;

destructor TimeOffBalanceRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FTimeOffBalance)-1 do
    FreeAndNil(FTimeOffBalance[I]);
  SetLength(FTimeOffBalance, 0);
  inherited Destroy;
end;

procedure TimeOffBalanceRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function TimeOffBalanceRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure TimeOffBalanceRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function TimeOffBalanceRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure TimeOffBalanceRequest.SetTimeOffBalance(Index: Integer; const AArrayOfAccrualBalance: ArrayOfAccrualBalance);
begin
  FTimeOffBalance := AArrayOfAccrualBalance;
  FTimeOffBalance_Specified := True;
end;

function TimeOffBalanceRequest.TimeOffBalance_Specified(Index: Integer): boolean;
begin
  Result := FTimeOffBalance_Specified;
end;

destructor AccrualBalance.Destroy;
begin
  FreeAndNil(FTransactionDate);
  FreeAndNil(FBalance);
  inherited Destroy;
end;

procedure AccrualBalance.SetSSN(Index: Integer; const AWideString: WideString);
begin
  FSSN := AWideString;
  FSSN_Specified := True;
end;

function AccrualBalance.SSN_Specified(Index: Integer): boolean;
begin
  Result := FSSN_Specified;
end;

procedure AccrualBalance.SetTimeOffType(Index: Integer; const AWideString: WideString);
begin
  FTimeOffType := AWideString;
  FTimeOffType_Specified := True;
end;

function AccrualBalance.TimeOffType_Specified(Index: Integer): boolean;
begin
  Result := FTimeOffType_Specified;
end;

procedure AccrualBalance.SetTransactionType(Index: Integer; const AWideString: WideString);
begin
  FTransactionType := AWideString;
  FTransactionType_Specified := True;
end;

function AccrualBalance.TransactionType_Specified(Index: Integer): boolean;
begin
  Result := FTransactionType_Specified;
end;

procedure AccrualBalance.SetComments(Index: Integer; const AWideString: WideString);
begin
  FComments := AWideString;
  FComments_Specified := True;
end;

function AccrualBalance.Comments_Specified(Index: Integer): boolean;
begin
  Result := FComments_Specified;
end;

destructor W2Request.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FW2_Files)-1 do
    FreeAndNil(FW2_Files[I]);
  SetLength(FW2_Files, 0);
  inherited Destroy;
end;

procedure W2Request.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function W2Request.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure W2Request.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function W2Request.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure W2Request.SetSSN(Index: Integer; const AWideString: WideString);
begin
  FSSN := AWideString;
  FSSN_Specified := True;
end;

function W2Request.SSN_Specified(Index: Integer): boolean;
begin
  Result := FSSN_Specified;
end;

procedure W2Request.SetW2_Files(Index: Integer; const AArrayOfW2File: ArrayOfW2File);
begin
  FW2_Files := AArrayOfW2File;
  FW2_Files_Specified := True;
end;

function W2Request.W2_Files_Specified(Index: Integer): boolean;
begin
  Result := FW2_Files_Specified;
end;

procedure W2File.SetNotes(Index: Integer; const AWideString: WideString);
begin
  FNotes := AWideString;
  FNotes_Specified := True;
end;

function W2File.Notes_Specified(Index: Integer): boolean;
begin
  Result := FNotes_Specified;
end;

procedure W2File.SetFileContent(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FFileContent := ATByteDynArray;
  FFileContent_Specified := True;
end;

function W2File.FileContent_Specified(Index: Integer): boolean;
begin
  Result := FFileContent_Specified;
end;

procedure W2File.SetFileExtension(Index: Integer; const AWideString: WideString);
begin
  FFileExtension := AWideString;
  FFileExtension_Specified := True;
end;

function W2File.FileExtension_Specified(Index: Integer): boolean;
begin
  Result := FFileExtension_Specified;
end;

procedure BenefitRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function BenefitRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure BenefitRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function BenefitRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

destructor BenefitStructure.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FBenefitStructureItems)-1 do
    FreeAndNil(FBenefitStructureItems[I]);
  SetLength(FBenefitStructureItems, 0);
  inherited Destroy;
end;

procedure BenefitStructure.SetBenefitStructureItems(Index: Integer; const AArrayOfBenefitStructureItem: ArrayOfBenefitStructureItem);
begin
  FBenefitStructureItems := AArrayOfBenefitStructureItem;
  FBenefitStructureItems_Specified := True;
end;

function BenefitStructure.BenefitStructureItems_Specified(Index: Integer): boolean;
begin
  Result := FBenefitStructureItems_Specified;
end;

destructor BenefitStructureItem.Destroy;
begin
  FreeAndNil(FPackageEffectiveDate);
  FreeAndNil(FPackageExpirationDate);
  inherited Destroy;
end;

procedure BenefitStructureItem.SetPackageName(Index: Integer; const AWideString: WideString);
begin
  FPackageName := AWideString;
  FPackageName_Specified := True;
end;

function BenefitStructureItem.PackageName_Specified(Index: Integer): boolean;
begin
  Result := FPackageName_Specified;
end;

procedure BenefitStructureItem.SetBenefitName(Index: Integer; const AWideString: WideString);
begin
  FBenefitName := AWideString;
  FBenefitName_Specified := True;
end;

function BenefitStructureItem.BenefitName_Specified(Index: Integer): boolean;
begin
  Result := FBenefitName_Specified;
end;

procedure BenefitStructureItem.SetBenefitCarrierCode(Index: Integer; const AWideString: WideString);
begin
  FBenefitCarrierCode := AWideString;
  FBenefitCarrierCode_Specified := True;
end;

function BenefitStructureItem.BenefitCarrierCode_Specified(Index: Integer): boolean;
begin
  Result := FBenefitCarrierCode_Specified;
end;

procedure BenefitStructureItem.SetPlanName(Index: Integer; const AWideString: WideString);
begin
  FPlanName := AWideString;
  FPlanName_Specified := True;
end;

function BenefitStructureItem.PlanName_Specified(Index: Integer): boolean;
begin
  Result := FPlanName_Specified;
end;

procedure BenefitStructureItem.SetPlanCarrierCode(Index: Integer; const AWideString: WideString);
begin
  FPlanCarrierCode := AWideString;
  FPlanCarrierCode_Specified := True;
end;

function BenefitStructureItem.PlanCarrierCode_Specified(Index: Integer): boolean;
begin
  Result := FPlanCarrierCode_Specified;
end;

procedure BenefitStructureItem.SetOptionName(Index: Integer; const AWideString: WideString);
begin
  FOptionName := AWideString;
  FOptionName_Specified := True;
end;

function BenefitStructureItem.OptionName_Specified(Index: Integer): boolean;
begin
  Result := FOptionName_Specified;
end;

destructor LookupData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FLookups)-1 do
    FreeAndNil(FLookups[I]);
  SetLength(FLookups, 0);
  inherited Destroy;
end;

procedure LookupData.SetLookups(Index: Integer; const AArrayOfLookup: ArrayOfLookup);
begin
  FLookups := AArrayOfLookup;
  FLookups_Specified := True;
end;

function LookupData.Lookups_Specified(Index: Integer): boolean;
begin
  Result := FLookups_Specified;
end;

procedure Lookup.SetText(Index: Integer; const AWideString: WideString);
begin
  FText := AWideString;
  FText_Specified := True;
end;

function Lookup.Text_Specified(Index: Integer): boolean;
begin
  Result := FText_Specified;
end;

destructor PayScheduleData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPaySchedules)-1 do
    FreeAndNil(FPaySchedules[I]);
  SetLength(FPaySchedules, 0);
  inherited Destroy;
end;

procedure PayScheduleData.SetPaySchedules(Index: Integer; const AArrayOfPaySchedule: ArrayOfPaySchedule);
begin
  FPaySchedules := AArrayOfPaySchedule;
  FPaySchedules_Specified := True;
end;

function PayScheduleData.PaySchedules_Specified(Index: Integer): boolean;
begin
  Result := FPaySchedules_Specified;
end;

procedure PaySchedule.SetName_(Index: Integer; const AWideString: WideString);
begin
  FName_ := AWideString;
  FName__Specified := True;
end;

function PaySchedule.Name__Specified(Index: Integer): boolean;
begin
  Result := FName__Specified;
end;

procedure PaySchedule.SetDescription(Index: Integer; const AWideString: WideString);
begin
  FDescription := AWideString;
  FDescription_Specified := True;
end;

function PaySchedule.Description_Specified(Index: Integer): boolean;
begin
  Result := FDescription_Specified;
end;

procedure CompanyRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function CompanyRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure CompanyRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function CompanyRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

destructor CostCenterData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FCostCenters)-1 do
    FreeAndNil(FCostCenters[I]);
  SetLength(FCostCenters, 0);
  inherited Destroy;
end;

procedure CostCenterData.SetCostCenters(Index: Integer; const AArrayOfCostCenterInfo: ArrayOfCostCenterInfo);
begin
  FCostCenters := AArrayOfCostCenterInfo;
  FCostCenters_Specified := True;
end;

function CostCenterData.CostCenters_Specified(Index: Integer): boolean;
begin
  Result := FCostCenters_Specified;
end;

destructor CostCenterInfo.Destroy;
begin
  FreeAndNil(FCreatedDate);
  FreeAndNil(FModifiedDate);
  FreeAndNil(FCostCenterParent);
  inherited Destroy;
end;

procedure CostCenterInfo.SetCostCenterName(Index: Integer; const AWideString: WideString);
begin
  FCostCenterName := AWideString;
  FCostCenterName_Specified := True;
end;

function CostCenterInfo.CostCenterName_Specified(Index: Integer): boolean;
begin
  Result := FCostCenterName_Specified;
end;

procedure CostCenterInfo.SetCostCenterDescription(Index: Integer; const AWideString: WideString);
begin
  FCostCenterDescription := AWideString;
  FCostCenterDescription_Specified := True;
end;

function CostCenterInfo.CostCenterDescription_Specified(Index: Integer): boolean;
begin
  Result := FCostCenterDescription_Specified;
end;

procedure CostCenterInfo.SetLevelName(Index: Integer; const AWideString: WideString);
begin
  FLevelName := AWideString;
  FLevelName_Specified := True;
end;

function CostCenterInfo.LevelName_Specified(Index: Integer): boolean;
begin
  Result := FLevelName_Specified;
end;

procedure CostCenterInfo.SetCostCenterParent(Index: Integer; const ACostCenterInfo: CostCenterInfo);
begin
  FCostCenterParent := ACostCenterInfo;
  FCostCenterParent_Specified := True;
end;

function CostCenterInfo.CostCenterParent_Specified(Index: Integer): boolean;
begin
  Result := FCostCenterParent_Specified;
end;

destructor CompanyData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FCompanies)-1 do
    FreeAndNil(FCompanies[I]);
  SetLength(FCompanies, 0);
  inherited Destroy;
end;

procedure CompanyData.SetEnterpriseVendor_ID(Index: Integer; const AWideString: WideString);
begin
  FEnterpriseVendor_ID := AWideString;
  FEnterpriseVendor_ID_Specified := True;
end;

function CompanyData.EnterpriseVendor_ID_Specified(Index: Integer): boolean;
begin
  Result := FEnterpriseVendor_ID_Specified;
end;

procedure CompanyData.SetCompanies(Index: Integer; const AArrayOfCompanyInfo: ArrayOfCompanyInfo);
begin
  FCompanies := AArrayOfCompanyInfo;
  FCompanies_Specified := True;
end;

function CompanyData.Companies_Specified(Index: Integer): boolean;
begin
  Result := FCompanies_Specified;
end;

destructor CompanyInfo.Destroy;
begin
  FreeAndNil(FCostCenterData);
  inherited Destroy;
end;

procedure CompanyInfo.SetVendor_ID(Index: Integer; const AWideString: WideString);
begin
  FVendor_ID := AWideString;
  FVendor_ID_Specified := True;
end;

function CompanyInfo.Vendor_ID_Specified(Index: Integer): boolean;
begin
  Result := FVendor_ID_Specified;
end;

procedure CompanyInfo.SetInfinityCompany_ID(Index: Integer; const AWideString: WideString);
begin
  FInfinityCompany_ID := AWideString;
  FInfinityCompany_ID_Specified := True;
end;

function CompanyInfo.InfinityCompany_ID_Specified(Index: Integer): boolean;
begin
  Result := FInfinityCompany_ID_Specified;
end;

procedure CompanyInfo.SetInfinityCompany_Name(Index: Integer; const AWideString: WideString);
begin
  FInfinityCompany_Name := AWideString;
  FInfinityCompany_Name_Specified := True;
end;

function CompanyInfo.InfinityCompany_Name_Specified(Index: Integer): boolean;
begin
  Result := FInfinityCompany_Name_Specified;
end;

procedure CompanyInfo.SetInfinityCompany_Status(Index: Integer; const AWideString: WideString);
begin
  FInfinityCompany_Status := AWideString;
  FInfinityCompany_Status_Specified := True;
end;

function CompanyInfo.InfinityCompany_Status_Specified(Index: Integer): boolean;
begin
  Result := FInfinityCompany_Status_Specified;
end;

procedure CompanyInfo.SetInfinityCompany_Type(Index: Integer; const AWideString: WideString);
begin
  FInfinityCompany_Type := AWideString;
  FInfinityCompany_Type_Specified := True;
end;

function CompanyInfo.InfinityCompany_Type_Specified(Index: Integer): boolean;
begin
  Result := FInfinityCompany_Type_Specified;
end;

procedure CompanyInfo.SetVendorCompany_ID(Index: Integer; const AWideString: WideString);
begin
  FVendorCompany_ID := AWideString;
  FVendorCompany_ID_Specified := True;
end;

function CompanyInfo.VendorCompany_ID_Specified(Index: Integer): boolean;
begin
  Result := FVendorCompany_ID_Specified;
end;

procedure CompanyInfo.SetVendorCompanyPassword(Index: Integer; const AWideString: WideString);
begin
  FVendorCompanyPassword := AWideString;
  FVendorCompanyPassword_Specified := True;
end;

function CompanyInfo.VendorCompanyPassword_Specified(Index: Integer): boolean;
begin
  Result := FVendorCompanyPassword_Specified;
end;

procedure CompanyInfo.SetCostCenterData(Index: Integer; const ACostCenterData: CostCenterData);
begin
  FCostCenterData := ACostCenterData;
  FCostCenterData_Specified := True;
end;

function CompanyInfo.CostCenterData_Specified(Index: Integer): boolean;
begin
  Result := FCostCenterData_Specified;
end;

procedure PayrollServiceException.SetErrorMessage(Index: Integer; const AWideString: WideString);
begin
  FErrorMessage := AWideString;
  FErrorMessage_Specified := True;
end;

function PayrollServiceException.ErrorMessage_Specified(Index: Integer): boolean;
begin
  Result := FErrorMessage_Specified;
end;

procedure PayrollServiceException.SetErrorException(Index: Integer; const AWideString: WideString);
begin
  FErrorException := AWideString;
  FErrorException_Specified := True;
end;

function PayrollServiceException.ErrorException_Specified(Index: Integer): boolean;
begin
  Result := FErrorException_Specified;
end;

procedure PayrollServiceException.SetErrorStackTrace(Index: Integer; const AWideString: WideString);
begin
  FErrorStackTrace := AWideString;
  FErrorStackTrace_Specified := True;
end;

function PayrollServiceException.ErrorStackTrace_Specified(Index: Integer): boolean;
begin
  Result := FErrorStackTrace_Specified;
end;

procedure AuditRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function AuditRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure AuditRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function AuditRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure AuditRequest.SetAuditMessage(Index: Integer; const AWideString: WideString);
begin
  FAuditMessage := AWideString;
  FAuditMessage_Specified := True;
end;

function AuditRequest.AuditMessage_Specified(Index: Integer): boolean;
begin
  Result := FAuditMessage_Specified;
end;

procedure ErrorRequest.SetErrorMessage(Index: Integer; const AWideString: WideString);
begin
  FErrorMessage := AWideString;
  FErrorMessage_Specified := True;
end;

function ErrorRequest.ErrorMessage_Specified(Index: Integer): boolean;
begin
  Result := FErrorMessage_Specified;
end;

procedure ErrorRequest.SetSessionID(Index: Integer; const AWideString: WideString);
begin
  FSessionID := AWideString;
  FSessionID_Specified := True;
end;

function ErrorRequest.SessionID_Specified(Index: Integer): boolean;
begin
  Result := FSessionID_Specified;
end;

procedure ErrorRequest.SetVendorID(Index: Integer; const AWideString: WideString);
begin
  FVendorID := AWideString;
  FVendorID_Specified := True;
end;

function ErrorRequest.VendorID_Specified(Index: Integer): boolean;
begin
  Result := FVendorID_Specified;
end;

procedure ErrorRequest.SetErrorInnerException(Index: Integer; const AWideString: WideString);
begin
  FErrorInnerException := AWideString;
  FErrorInnerException_Specified := True;
end;

function ErrorRequest.ErrorInnerException_Specified(Index: Integer): boolean;
begin
  Result := FErrorInnerException_Specified;
end;

procedure ErrorRequest.SetErrorStackTrace(Index: Integer; const AWideString: WideString);
begin
  FErrorStackTrace := AWideString;
  FErrorStackTrace_Specified := True;
end;

function ErrorRequest.ErrorStackTrace_Specified(Index: Integer): boolean;
begin
  Result := FErrorStackTrace_Specified;
end;

procedure ErrorRequest.SetErrorProcedureName(Index: Integer; const AWideString: WideString);
begin
  FErrorProcedureName := AWideString;
  FErrorProcedureName_Specified := True;
end;

function ErrorRequest.ErrorProcedureName_Specified(Index: Integer): boolean;
begin
  Result := FErrorProcedureName_Specified;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(PayrollSyncSoap), 'http://www.infinity-ss.com/services', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(PayrollSyncSoap), 'http://www.infinity-ss.com/services/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(PayrollSyncSoap), ioDocument);
  RemClassRegistry.RegisterXSClass(AuthRequest, 'http://www.infinity-ss.com/services', 'AuthRequest');
  RemClassRegistry.RegisterXSClass(AuthResponse, 'http://www.infinity-ss.com/services', 'AuthResponse');
  RemClassRegistry.RegisterXSClass(EmployeeRequest, 'http://www.infinity-ss.com/services', 'EmployeeRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeInfo), 'http://www.infinity-ss.com/services', 'ArrayOfEmployeeInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfDeduction), 'http://www.infinity-ss.com/services', 'ArrayOfDeduction');
  RemClassRegistry.RegisterXSClass(CoverageInfo, 'http://www.infinity-ss.com/services', 'CoverageInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCompensation), 'http://www.infinity-ss.com/services', 'ArrayOfCompensation');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfDirectDeposit), 'http://www.infinity-ss.com/services', 'ArrayOfDirectDeposit');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfStateTax), 'http://www.infinity-ss.com/services', 'ArrayOfStateTax');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfFederalTax), 'http://www.infinity-ss.com/services', 'ArrayOfFederalTax');
  RemClassRegistry.RegisterXSClass(EmployeeInfo, 'http://www.infinity-ss.com/services', 'EmployeeInfo');
  RemClassRegistry.RegisterXSClass(Deduction, 'http://www.infinity-ss.com/services', 'Deduction');
  RemClassRegistry.RegisterXSClass(Compensation, 'http://www.infinity-ss.com/services', 'Compensation');
  RemClassRegistry.RegisterXSClass(DirectDeposit, 'http://www.infinity-ss.com/services', 'DirectDeposit');
  RemClassRegistry.RegisterXSClass(StateTax, 'http://www.infinity-ss.com/services', 'StateTax');
  RemClassRegistry.RegisterXSClass(FederalTax, 'http://www.infinity-ss.com/services', 'FederalTax');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeInfoSkinny), 'http://www.infinity-ss.com/services', 'ArrayOfEmployeeInfoSkinny');
  RemClassRegistry.RegisterXSClass(EmployeeData, 'http://www.infinity-ss.com/services', 'EmployeeData');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfFieldInfo), 'http://www.infinity-ss.com/services', 'ArrayOfFieldInfo');
  RemClassRegistry.RegisterXSClass(EmployeeInfoSkinny, 'http://www.infinity-ss.com/services', 'EmployeeInfoSkinny');
  RemClassRegistry.RegisterXSInfo(TypeInfo(FieldChangeType), 'http://www.infinity-ss.com/services', 'FieldChangeType');
  RemClassRegistry.RegisterXSClass(FieldInfo, 'http://www.infinity-ss.com/services', 'FieldInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfInt), 'http://www.infinity-ss.com/services', 'ArrayOfInt');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystubFile), 'http://www.infinity-ss.com/services', 'ArrayOfPaystubFile');
  RemClassRegistry.RegisterXSClass(PaystubRequest, 'http://www.infinity-ss.com/services', 'PaystubRequest');
  RemClassRegistry.RegisterXSClass(PaystubFile, 'http://www.infinity-ss.com/services', 'PaystubFile');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystubData), 'http://www.infinity-ss.com/services', 'ArrayOfPaystubData');
  RemClassRegistry.RegisterXSClass(PaystubDataRequest, 'http://www.infinity-ss.com/services', 'PaystubDataRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystub_Summary), 'http://www.infinity-ss.com/services', 'ArrayOfPaystub_Summary');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystub_Detail), 'http://www.infinity-ss.com/services', 'ArrayOfPaystub_Detail');
  RemClassRegistry.RegisterXSClass(PaystubData, 'http://www.infinity-ss.com/services', 'PaystubData');
  RemClassRegistry.RegisterXSClass(Paystub_TimeOff, 'http://www.infinity-ss.com/services', 'Paystub_TimeOff');
  RemClassRegistry.RegisterXSClass(Paystub_Tax, 'http://www.infinity-ss.com/services', 'Paystub_Tax');
  RemClassRegistry.RegisterXSClass(Paystub_Summary, 'http://www.infinity-ss.com/services', 'Paystub_Summary');
  RemClassRegistry.RegisterXSClass(Paystub_Detail, 'http://www.infinity-ss.com/services', 'Paystub_Detail');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfAccrualBalance), 'http://www.infinity-ss.com/services', 'ArrayOfAccrualBalance');
  RemClassRegistry.RegisterXSClass(TimeOffBalanceRequest, 'http://www.infinity-ss.com/services', 'TimeOffBalanceRequest');
  RemClassRegistry.RegisterXSClass(AccrualBalance, 'http://www.infinity-ss.com/services', 'AccrualBalance');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfW2File), 'http://www.infinity-ss.com/services', 'ArrayOfW2File');
  RemClassRegistry.RegisterXSClass(W2Request, 'http://www.infinity-ss.com/services', 'W2Request');
  RemClassRegistry.RegisterXSClass(W2File, 'http://www.infinity-ss.com/services', 'W2File');
  RemClassRegistry.RegisterXSClass(BenefitRequest, 'http://www.infinity-ss.com/services', 'BenefitRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfBenefitStructureItem), 'http://www.infinity-ss.com/services', 'ArrayOfBenefitStructureItem');
  RemClassRegistry.RegisterXSClass(BenefitStructure, 'http://www.infinity-ss.com/services', 'BenefitStructure');
  RemClassRegistry.RegisterXSClass(BenefitStructureItem, 'http://www.infinity-ss.com/services', 'BenefitStructureItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfLookup), 'http://www.infinity-ss.com/services', 'ArrayOfLookup');
  RemClassRegistry.RegisterXSClass(LookupData, 'http://www.infinity-ss.com/services', 'LookupData');
  RemClassRegistry.RegisterXSClass(Lookup, 'http://www.infinity-ss.com/services', 'Lookup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaySchedule), 'http://www.infinity-ss.com/services', 'ArrayOfPaySchedule');
  RemClassRegistry.RegisterXSClass(PayScheduleData, 'http://www.infinity-ss.com/services', 'PayScheduleData');
  RemClassRegistry.RegisterXSClass(PaySchedule, 'http://www.infinity-ss.com/services', 'PaySchedule');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(PaySchedule), 'Name_', 'Name');
  RemClassRegistry.RegisterXSClass(CompanyRequest, 'http://www.infinity-ss.com/services', 'CompanyRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCostCenterInfo), 'http://www.infinity-ss.com/services', 'ArrayOfCostCenterInfo');
  RemClassRegistry.RegisterXSClass(CostCenterData, 'http://www.infinity-ss.com/services', 'CostCenterData');
  RemClassRegistry.RegisterXSClass(CostCenterInfo, 'http://www.infinity-ss.com/services', 'CostCenterInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCompanyInfo), 'http://www.infinity-ss.com/services', 'ArrayOfCompanyInfo');
  RemClassRegistry.RegisterXSClass(CompanyData, 'http://www.infinity-ss.com/services', 'CompanyData');
  RemClassRegistry.RegisterXSClass(CompanyInfo, 'http://www.infinity-ss.com/services', 'CompanyInfo');
  RemClassRegistry.RegisterXSClass(PayrollServiceException, 'http://www.infinity-ss.com/services', 'PayrollServiceException');
  RemClassRegistry.RegisterXSClass(AuditRequest, 'http://www.infinity-ss.com/services', 'AuditRequest');
  RemClassRegistry.RegisterXSClass(ErrorRequest, 'http://www.infinity-ss.com/services', 'ErrorRequest');

{
  InvRegistry.RegisterInterface(TypeInfo(PayrollSyncSoap), 'http://www.infinity-ss.com/services', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(PayrollSyncSoap), 'http://www.infinity-ss.com/services/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(PayrollSyncSoap), ioDocument);
  RemClassRegistry.RegisterXSClass(AuthRequest, 'http://www.infinity-ss.com/services', 'AuthRequest');
  RemClassRegistry.RegisterXSClass(AuthResponse, 'http://www.infinity-ss.com/services', 'AuthResponse');
  RemClassRegistry.RegisterXSClass(EmployeeRequest, 'http://www.infinity-ss.com/services', 'EmployeeRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeInfo), 'http://www.infinity-ss.com/services', 'ArrayOfEmployeeInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfDeduction), 'http://www.infinity-ss.com/services', 'ArrayOfDeduction');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCompensation), 'http://www.infinity-ss.com/services', 'ArrayOfCompensation');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfDirectDeposit), 'http://www.infinity-ss.com/services', 'ArrayOfDirectDeposit');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfStateTax), 'http://www.infinity-ss.com/services', 'ArrayOfStateTax');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfFederalTax), 'http://www.infinity-ss.com/services', 'ArrayOfFederalTax');
  RemClassRegistry.RegisterXSClass(EmployeeInfo, 'http://www.infinity-ss.com/services', 'EmployeeInfo');
  RemClassRegistry.RegisterXSClass(Deduction, 'http://www.infinity-ss.com/services', 'Deduction');
  RemClassRegistry.RegisterXSClass(Compensation, 'http://www.infinity-ss.com/services', 'Compensation');
  RemClassRegistry.RegisterXSClass(DirectDeposit, 'http://www.infinity-ss.com/services', 'DirectDeposit');
  RemClassRegistry.RegisterXSClass(StateTax, 'http://www.infinity-ss.com/services', 'StateTax');
  RemClassRegistry.RegisterXSClass(FederalTax, 'http://www.infinity-ss.com/services', 'FederalTax');
  RemClassRegistry.RegisterXSClass(EmployeeData, 'http://www.infinity-ss.com/services', 'EmployeeData');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystubFile), 'http://www.infinity-ss.com/services', 'ArrayOfPaystubFile');
  RemClassRegistry.RegisterXSClass(PaystubRequest, 'http://www.infinity-ss.com/services', 'PaystubRequest');
  RemClassRegistry.RegisterXSClass(PaystubFile, 'http://www.infinity-ss.com/services', 'PaystubFile');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystubData), 'http://www.infinity-ss.com/services', 'ArrayOfPaystubData');
  RemClassRegistry.RegisterXSClass(PaystubDataRequest, 'http://www.infinity-ss.com/services', 'PaystubDataRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystub_Summary), 'http://www.infinity-ss.com/services', 'ArrayOfPaystub_Summary');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystub_Detail), 'http://www.infinity-ss.com/services', 'ArrayOfPaystub_Detail');
  RemClassRegistry.RegisterXSClass(PaystubData, 'http://www.infinity-ss.com/services', 'PaystubData');
  RemClassRegistry.RegisterXSClass(Paystub_TimeOff, 'http://www.infinity-ss.com/services', 'Paystub_TimeOff');
  RemClassRegistry.RegisterXSClass(Paystub_Tax, 'http://www.infinity-ss.com/services', 'Paystub_Tax');
  RemClassRegistry.RegisterXSClass(Paystub_Summary, 'http://www.infinity-ss.com/services', 'Paystub_Summary');
  RemClassRegistry.RegisterXSClass(Paystub_Detail, 'http://www.infinity-ss.com/services', 'Paystub_Detail');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfAccrualBalance), 'http://www.infinity-ss.com/services', 'ArrayOfAccrualBalance');
  RemClassRegistry.RegisterXSClass(TimeOffBalanceRequest, 'http://www.infinity-ss.com/services', 'TimeOffBalanceRequest');
  RemClassRegistry.RegisterXSClass(AccrualBalance, 'http://www.infinity-ss.com/services', 'AccrualBalance');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfW2File), 'http://www.infinity-ss.com/services', 'ArrayOfW2File');
  RemClassRegistry.RegisterXSClass(W2Request, 'http://www.infinity-ss.com/services', 'W2Request');
  RemClassRegistry.RegisterXSClass(W2File, 'http://www.infinity-ss.com/services', 'W2File');
  RemClassRegistry.RegisterXSClass(BenefitRequest, 'http://www.infinity-ss.com/services', 'BenefitRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfBenefitStructureItem), 'http://www.infinity-ss.com/services', 'ArrayOfBenefitStructureItem');
  RemClassRegistry.RegisterXSClass(BenefitStructure, 'http://www.infinity-ss.com/services', 'BenefitStructure');
  RemClassRegistry.RegisterXSClass(BenefitStructureItem, 'http://www.infinity-ss.com/services', 'BenefitStructureItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfLookup), 'http://www.infinity-ss.com/services', 'ArrayOfLookup');
  RemClassRegistry.RegisterXSClass(LookupData, 'http://www.infinity-ss.com/services', 'LookupData');
  RemClassRegistry.RegisterXSClass(Lookup, 'http://www.infinity-ss.com/services', 'Lookup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaySchedule), 'http://www.infinity-ss.com/services', 'ArrayOfPaySchedule');
  RemClassRegistry.RegisterXSClass(PayScheduleData, 'http://www.infinity-ss.com/services', 'PayScheduleData');
  RemClassRegistry.RegisterXSClass(PaySchedule, 'http://www.infinity-ss.com/services', 'PaySchedule');
  RemClassRegistry.RegisterExternalPropName(TypeInfo(PaySchedule), 'Name_', 'Name');
  RemClassRegistry.RegisterXSClass(CompanyRequest, 'http://www.infinity-ss.com/services', 'CompanyRequest');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCompanyInfo), 'http://www.infinity-ss.com/services', 'ArrayOfCompanyInfo');
  RemClassRegistry.RegisterXSClass(CompanyData, 'http://www.infinity-ss.com/services', 'CompanyData');
  RemClassRegistry.RegisterXSClass(CompanyInfo, 'http://www.infinity-ss.com/services', 'CompanyInfo');
  RemClassRegistry.RegisterXSClass(PayrollServiceException, 'http://www.infinity-ss.com/services', 'PayrollServiceException');
  RemClassRegistry.RegisterXSClass(AuditRequest, 'http://www.infinity-ss.com/services', 'AuditRequest');
  RemClassRegistry.RegisterXSClass(ErrorRequest, 'http://www.infinity-ss.com/services', 'ErrorRequest');
}  
end.