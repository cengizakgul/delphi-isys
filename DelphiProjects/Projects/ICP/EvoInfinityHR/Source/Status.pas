unit Status;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, gdyCommonLogger;

type
  IStatusProgress = interface
    procedure SetProgressCount(StepCount: integer);
    procedure Clear;
    procedure Hide;
    procedure Show;
    procedure SetMessage(const aMsg: string; const aMsg2: string; const aMsg3: string);
    procedure SetDetailMessage(const aMsg3: string);
    procedure IncProgress;
    procedure ClearProgress;
  end;

  TfrmStatus = class(TFrame, IStatusProgress)
    gbStatus: TGroupBox;
    pnlHolder: TPanel;
    lblMessage: TLabel;
    lblDetailMessage: TLabel;
    pbProgress: TProgressBar;
    mmMessage: TMemo;
  private
    FLogger: ICommonLogger;
  public
    procedure SetProgressCount(StepCount: integer);
    procedure Clear;
    procedure Hide;
    procedure Show;
    procedure SetMessage(const aMsg: string; const aMsg2: string; const aMsg3: string);
    procedure SetDetailMessage(const aMsg3: string);
    procedure IncProgress;
    procedure ClearProgress;
    procedure SetLogger(Logger: ICommonLogger);
  end;

implementation

{$R *.dfm}

procedure TfrmStatus.Clear;
begin
  mmMessage.Lines.Clear;
  lblMessage.Caption := '';
  lblDetailMessage.Caption := '';
  pbProgress.Position := 0;
  pbProgress.Min := 0;
  pbProgress.Max := 0;
  pbProgress.Visible := False;
  Application.ProcessMessages;
end;

procedure TfrmStatus.ClearProgress;
begin
  pbProgress.Position := 0;
  pbProgress.Visible := False;
  Application.ProcessMessages;
end;

procedure TfrmStatus.Hide;
begin
  Self.Visible := False;
end;

procedure TfrmStatus.IncProgress;
begin
  pbProgress.Position := pbProgress.Position + 1;
  Application.ProcessMessages;
end;

procedure TfrmStatus.SetDetailMessage(const aMsg3: string);
begin
  if lblDetailMessage.Caption <> aMsg3 then
  begin
    lblDetailMessage.Caption := aMsg3;
    FLogger.LogDebug(aMsg3);
  end;

  Application.ProcessMessages;
end;

procedure TfrmStatus.SetLogger(Logger: ICommonLogger);
begin
  FLogger := Logger;
end;

procedure TfrmStatus.SetMessage(const aMsg: string; const aMsg2: string; const aMsg3: string);
begin
  if aMsg <> '' then
  begin
    FLogger.LogEvent( aMsg );
    FLogger.LogDebug( aMsg );
    mmMessage.Lines.Add(aMsg);
  end;

  if (aMsg2 = '') and (aMsg <> '') then
    lblMessage.Caption := aMsg
  else if lblMessage.Caption <> aMsg2 then
  begin
    lblMessage.Caption := aMsg2;
    FLogger.LogDebug( aMsg2 );
  end;

  if lblDetailMessage.Caption <> aMsg3 then
  begin
    lblDetailMessage.Caption := aMsg3;
    FLogger.LogDebug( aMsg3 );
  end;

  Application.ProcessMessages;
end;

procedure TfrmStatus.SetProgressCount(StepCount: integer);
begin
  lblMessage.Caption := '';
  pbProgress.Position := 0;
  if StepCount > 0 then
  begin
    pbProgress.Min := 0;
    pbProgress.Max := StepCount - 1;
    pbProgress.Visible := True;
  end;
  Application.ProcessMessages;
end;

procedure TfrmStatus.Show;
begin
  Self.Visible := True;
end;

end.
