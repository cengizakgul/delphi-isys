unit CheckSelectByPeriod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, dbcomp,
  DB, evoApiconnection, gdycommonlogger, CheckSelectCommon, DateRangeFrame,
  EeHolder, common, wwdblook, Spin;

type
  TfrmCheckSelectByPeriod = class(TForm)
    pnlMain: TPanel;
    Actions: TActionList;
    pnlResult: TPanel;
    pnlBottom: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    CompanyDS: TDataSource;
    Ok: TAction;
    Cancel: TAction;
    pnlCoList: TPanel;
    lbChecks: TListBox;
    btnNo: TButton;
    Remove: TAction;
    gbCompany: TGroupBox;
    lblCoNumber: TLabel;
    lblCompany: TLabel;
    edCoNumber: TEdit;
    edCoName: TEdit;
    dbgCompanies: TReDBGrid;
    AddPeriod: TAction;
    rgPeriod: TRadioGroup;
    seDays: TSpinEdit;
    btnAdd: TButton;
    procedure CancelExecute(Sender: TObject);
    procedure OkExecute(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure edCoNumberKeyPress(Sender: TObject; var Key: Char);
    procedure RemoveExecute(Sender: TObject);
    procedure AddPeriodUpdate(Sender: TObject);
    procedure AddPeriodExecute(Sender: TObject);
  private
    FCompanies: TEvoCoPrList;
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    SelectedChecks: TSelectedChecks;
  protected
    procedure LoadCompanyList;

    procedure ClearForm;
    procedure Init(aEvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
    procedure GetResult(var Res: TSelectedChecks);
    procedure SetResult(Res: TSelectedChecks);

    function GetPeriodAsString: string;
//    procedure SetPeriodAsString(const aValue: string);
  end;


function OpenCheckSelectByPeriod(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; var Res: TSelectedChecks): boolean;

implementation

uses Math;

{$R *.dfm}

function OpenCheckSelectByPeriod(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; var Res: TSelectedChecks): boolean;
var
  frm: TfrmCheckSelectByPeriod;
begin
  frm := TfrmCheckSelectByPeriod.Create(nil);
  try
    frm.SetResult( Res );
    frm.Init( EvoAPI, Logger );
    Result := frm.ShowModal = mrOk;
    if Result then
      frm.GetResult( Res );
  finally
    frm.ClearForm;
    FreeAndNil(frm);
  end;
end;

{ TfrmCheckSelect }

procedure TfrmCheckSelectByPeriod.GetResult(var Res: TSelectedChecks);
var i: integer;
begin
  SetLength(Res, Length(SelectedChecks));
  for i := low(SelectedChecks) to high(SelectedChecks) do
  begin
    Res[i].Caption := SelectedChecks[i].Caption;
    Res[i].CL_NBR := SelectedChecks[i].CL_NBR;
    Res[i].CO_NBR := SelectedChecks[i].CO_NBR;
    Res[i].PR_NBR := SelectedChecks[i].PR_NBR;
    Res[i].PR_CHECK_NBR := SelectedChecks[i].PR_CHECK_NBR;
    Res[i].Period := SelectedChecks[i].Period;
  end;
end;

procedure TfrmCheckSelectByPeriod.Init(aEvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
begin
  FEvoAPI := aEvoAPI;
  FLogger := Logger;
  FCompanies := TEvoCoPrList.Create(FLogger, FEvoAPI);

  AddSelected(dbgCompanies.Selected, 'CoNumber', 10, 'Number', true);
  AddSelected(dbgCompanies.Selected, 'NAME', 25, 'Name', true);

  dbgCompanies.Enabled := False;
end;

procedure TfrmCheckSelectByPeriod.CancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCheckSelectByPeriod.OkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmCheckSelectByPeriod.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbChecks.Items.Count > 0) and (lbChecks.ItemIndex >= 0);
end;

procedure TfrmCheckSelectByPeriod.ClearForm;
begin
  FLogger := nil;
  CompanyDS.DataSet := nil;

  FreeAndNil(FCompanies); // to do: create this object in main form
end;

procedure TfrmCheckSelectByPeriod.SetResult(Res: TSelectedChecks);
var i: integer;
begin
  lbChecks.Clear;
  SetLength(SelectedChecks, Length(Res));
  for i := low(res) to high(res) do
  begin
    lbChecks.Items.Add( res[i].Caption );

    SelectedChecks[i].Caption := res[i].Caption;
    SelectedChecks[i].CL_NBR := res[i].CL_NBR;
    SelectedChecks[i].CO_NBR := res[i].CO_NBR;
    SelectedChecks[i].PR_NBR := res[i].PR_NBR;
    SelectedChecks[i].PR_CHECK_NBR := res[i].PR_CHECK_NBR;
    SelectedChecks[i].Period := res[i].Period;
  end;
end;

procedure TfrmCheckSelectByPeriod.LoadCompanyList;
begin
  dbgCompanies.Enabled := False;
  CompanyDS.DataSet := nil;
  try
    if (Trim(edCoNumber.Text) <> '') or (Trim(edCoName.Text) <> '') then
      CompanyDS.DataSet := FCompanies.GetCoList( Trim(edCoNumber.Text), Trim(edCoName.Text) )
    else
      ShowMessage('Please, type in either Custom Company Number or Company Name');
  finally
    if assigned(CompanyDS.DataSet) then
    begin
      if CompanyDS.DataSet.RecordCount > 0 then
        dbgCompanies.Enabled := True
      else
        ShowMessage('No Companies with such name or number were found');
    end;    
  end;
end;

procedure TfrmCheckSelectByPeriod.edCoNumberKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    LoadCompanyList;
end;

procedure TfrmCheckSelectByPeriod.RemoveExecute(Sender: TObject);
var
  i: integer;
begin
  for i := lbChecks.ItemIndex to Length(SelectedChecks) - 2 do
  begin
    SelectedChecks[i].CL_NBR := SelectedChecks[i+1].CL_NBR;
    SelectedChecks[i].CO_NBR := SelectedChecks[i+1].CO_NBR;
    SelectedChecks[i].PR_NBR := SelectedChecks[i+1].PR_NBR;
    SelectedChecks[i].PR_CHECK_NBR := SelectedChecks[i+1].PR_CHECK_NBR;

    SelectedChecks[i].Caption := SelectedChecks[i+1].Caption;
  end;

  SetLength( SelectedChecks, Length(SelectedChecks) - 1 );

  lbChecks.Items.Delete( lbChecks.ItemIndex );
end;

procedure TfrmCheckSelectByPeriod.AddPeriodUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0);
end;

procedure TfrmCheckSelectByPeriod.AddPeriodExecute(Sender: TObject);
begin
  SetLength( SelectedChecks, Length(SelectedChecks) + 1 );

  with SelectedChecks[High(SelectedChecks)] do
  begin
    CL_NBR := CompanyDS.DataSet.FieldByName('CL_NBR').AsInteger;
    CO_NBR := CompanyDS.DataSet.FieldByName('CO_NBR').AsInteger;
    PR_NBR := -1;
    PR_CHECK_NBR := -1;
    Period := GetPeriodAsString;

    SelectedChecks[High(SelectedChecks)].Caption := 'Company #' + CompanyDS.DataSet.FieldByName('CONUMBER').AsString +
      ', paystubs for ' + GetPeriodAsString;

    lbChecks.Items.Add( SelectedChecks[High(SelectedChecks)].Caption );
  end;
end;

function TfrmCheckSelectByPeriod.GetPeriodAsString: string;
begin
  case rgPeriod.ItemIndex of
    0: Result := 'The past ' + IntToStr(seDays.Value) + ' days';
    1: Result := 'The Current Week';
    2: Result := 'The Last Week';
    3: Result := 'The Current Month';
  else
    Result := 'The Last Month';
  end;
end;

end.
