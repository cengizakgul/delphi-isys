unit CheckSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, dbcomp,
  DB, evoApiconnection, gdycommonlogger, CheckSelectCommon, DateRangeFrame,
  EeHolder, common, wwdblook;

type
  TfrmCheckSelect = class(TForm)
    pnlMain: TPanel;
    Actions: TActionList;
    pnlResult: TPanel;
    pnlBottom: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    CompanyDS: TDataSource;
    Ok: TAction;
    Cancel: TAction;
    pnlCoList: TPanel;
    pnlChecks: TPanel;
    lbChecks: TListBox;
    GetPayrolls: TAction;
    dbgPayrolls: TReDBGrid;
    dbgChecks: TReDBGrid;
    btnNo: TButton;
    Add: TAction;
    Remove: TAction;
    PayrollDS: TDataSource;
    CheckDS: TDataSource;
    gbCompany: TGroupBox;
    lblCoNumber: TLabel;
    lblCompany: TLabel;
    edCoNumber: TEdit;
    edCoName: TEdit;
    dbgCompanies: TReDBGrid;
    GroupBox1: TGroupBox;
    CheckDateRange: TfrmDateRange;
    bvlTopLine1: TBevel;
    btnCheckByPeriod: TButton;
    btnCheckByPayroll: TButton;
    btnCheck: TButton;
    lblCap1: TLabel;
    lblComment1: TLabel;
    bvlTopLine2: TBevel;
    lblCap2: TLabel;
    lblComments2: TLabel;
    Bevel1: TBevel;
    btnGetPayrolls: TButton;
    lblCap3: TLabel;
    lblComments3: TLabel;
    AddPayroll: TAction;
    AddPeriod: TAction;
    procedure CancelExecute(Sender: TObject);
    procedure OkExecute(Sender: TObject);
    procedure GetPayrollsExecute(Sender: TObject);
    procedure GetPayrollsUpdate(Sender: TObject);
    procedure AddUpdate(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure AddExecute(Sender: TObject);
    procedure dbgChecksDblClick(Sender: TObject);
    procedure edCoNumberKeyPress(Sender: TObject; var Key: Char);
    procedure RemoveExecute(Sender: TObject);
    procedure AddPayrollUpdate(Sender: TObject);
    procedure AddPayrollExecute(Sender: TObject);
    procedure AddPeriodUpdate(Sender: TObject);
    procedure AddPeriodExecute(Sender: TObject);
  private
    FCompanies: TEvoCoPrList;
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    SelectedChecks: TSelectedChecks;

  protected
    procedure LoadCompanyList;

    procedure ClearForm;
    procedure Init(aEvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
    procedure GetResult(var Res: TSelectedChecks);
    procedure SetResult(Res: TSelectedChecks);
  end;


function OpenCheckSelect(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; var Res: TSelectedChecks): boolean;

implementation

uses Math;

{$R *.dfm}

function OpenCheckSelect(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; var Res: TSelectedChecks): boolean;
var
  frmCheckSelect: TfrmCheckSelect;
begin
  frmCheckSelect := TfrmCheckSelect.Create(nil);
  try
    frmCheckSelect.SetResult( Res );
    frmCheckSelect.Init( EvoAPI, Logger );
    Result := frmCheckSelect.ShowModal = mrOk;
    if Result then
      frmCheckSelect.GetResult( Res );
  finally
    frmCheckSelect.ClearForm;
    FreeAndNil(frmCheckSelect);
  end;
end;

{ TfrmCheckSelect }

procedure TfrmCheckSelect.GetResult(var Res: TSelectedChecks);
var i: integer;
begin
  SetLength(Res, Length(SelectedChecks));
  for i := low(SelectedChecks) to high(SelectedChecks) do
  begin
    Res[i].Caption := SelectedChecks[i].Caption;
    Res[i].CL_NBR := SelectedChecks[i].CL_NBR;
    Res[i].CO_NBR := SelectedChecks[i].CO_NBR;
    Res[i].PR_NBR := SelectedChecks[i].PR_NBR;
    Res[i].PR_CHECK_NBR := SelectedChecks[i].PR_CHECK_NBR;
    Res[i].Period := SelectedChecks[i].Period;
  end;
end;

procedure TfrmCheckSelect.Init(aEvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
begin
  FEvoAPI := aEvoAPI;
  FLogger := Logger;
  FCompanies := TEvoCoPrList.Create(FLogger, FEvoAPI);

  AddSelected(dbgCompanies.Selected, 'CoNumber', 10, 'Number', true);
  AddSelected(dbgCompanies.Selected, 'NAME', 25, 'Name', true);

  AddSelected(dbgPayrolls.Selected, 'CHECK_DATE', 12, 'Check_Date', true);
  AddSelected(dbgPayrolls.Selected, 'RUN_NUMBER', 5, 'Run #', true);

  AddSelected(dbgChecks.Selected, 'CHECK', 20, 'Check #', true);
  AddSelected(dbgChecks.Selected, 'EE_CODE', 15, 'Employee Code', true);

  dbgCompanies.Enabled := False;
end;

procedure TfrmCheckSelect.CancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCheckSelect.OkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmCheckSelect.GetPayrollsExecute(Sender: TObject);
begin
  FCompanies.LoadPayrollsChecks(CheckDateRange.Dat_b, CheckDateRange.Dat_e);
  PayrollDS.DataSet := FCompanies.PrList;
  CheckDS.DataSet := FCompanies.PrCheckList;
end;

procedure TfrmCheckSelect.GetPayrollsUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0);
end;

procedure TfrmCheckSelect.AddUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0) and Assigned(PayrollDS.DataSet) and PayrollDS.DataSet.Active and
    (PayrollDS.DataSet.RecordCount > 0) and Assigned(CheckDS.DataSet) and CheckDS.DataSet.Active and
    (CheckDS.DataSet.RecordCount > 0);
end;

procedure TfrmCheckSelect.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbChecks.Items.Count > 0) and (lbChecks.ItemIndex >= 0);
end;

procedure TfrmCheckSelect.ClearForm;
begin
  FLogger := nil;
  CompanyDS.DataSet := nil;
  PayrollDS.DataSet := nil;
  CheckDS.DataSet := nil;

  FreeAndNil(FCompanies); // to do: create this object in main form
end;

procedure TfrmCheckSelect.AddExecute(Sender: TObject);
var
  s: string;
begin
  s := 'Company #' + CompanyDS.DataSet.FieldByName('CONUMBER').AsString +
      ', Pr ' + PayrollDS.DataSet.FieldByName('CHECK_DATE').AsString + '-' + PayrollDS.DataSet.FieldByName('RUN_NUMBER').AsString +
      ', Check #' + CheckDS.DataSet.FieldByName('CHECK').AsString + ', EE #' + CheckDS.DataSet.FieldByName('EE_CODE').AsString;

  if lbChecks.Items.IndexOf( s ) = -1 then
  begin
    SetLength( SelectedChecks, Length(SelectedChecks) + 1 );
    with SelectedChecks[High(SelectedChecks)] do
    begin
      CL_NBR := CompanyDS.DataSet.FieldByName('CL_NBR').AsInteger;
      CO_NBR := CompanyDS.DataSet.FieldByName('CO_NBR').AsInteger;
      PR_NBR := PayrollDS.DataSet.FieldByName('PR_NBR').AsInteger;
      PR_CHECK_NBR := CheckDS.DataSet.FieldByName('PR_CHECK_NBR').AsInteger;
      SelectedChecks[High(SelectedChecks)].Caption := s;
      lbChecks.Items.Add( SelectedChecks[High(SelectedChecks)].Caption );
    end;
  end;  
end;

procedure TfrmCheckSelect.SetResult(Res: TSelectedChecks);
var i: integer;
begin
  lbChecks.Clear;
  SetLength(SelectedChecks, Length(Res));
  for i := low(res) to high(res) do
  begin
    lbChecks.Items.Add( res[i].Caption );

    SelectedChecks[i].Caption := res[i].Caption;
    SelectedChecks[i].CL_NBR := res[i].CL_NBR;
    SelectedChecks[i].CO_NBR := res[i].CO_NBR;
    SelectedChecks[i].PR_NBR := res[i].PR_NBR;
    SelectedChecks[i].PR_CHECK_NBR := res[i].PR_CHECK_NBR;
    SelectedChecks[i].Period := res[i].Period;
  end;
end;

procedure TfrmCheckSelect.dbgChecksDblClick(Sender: TObject);
begin
  AddExecute(Sender);
end;

procedure TfrmCheckSelect.LoadCompanyList;
begin
  dbgCompanies.Enabled := False;
  CompanyDS.DataSet := nil;
  try
    if (Trim(edCoNumber.Text) <> '') or (Trim(edCoName.Text) <> '') then
      CompanyDS.DataSet := FCompanies.GetCoList( Trim(edCoNumber.Text), Trim(edCoName.Text) )
    else
      ShowMessage('Please, type in either Custom Company Number or Company Name');
  finally
    if assigned(CompanyDS.DataSet) then
    begin
      if CompanyDS.DataSet.RecordCount > 0 then
        dbgCompanies.Enabled := True
      else
        ShowMessage('No Companies with such name or number were found');
    end;    
  end;
end;

procedure TfrmCheckSelect.edCoNumberKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    LoadCompanyList;
end;

procedure TfrmCheckSelect.RemoveExecute(Sender: TObject);
var
  i: integer;
begin
  for i := lbChecks.ItemIndex to Length(SelectedChecks) - 2 do
  begin
    SelectedChecks[i].CL_NBR := SelectedChecks[i+1].CL_NBR;
    SelectedChecks[i].CO_NBR := SelectedChecks[i+1].CO_NBR;
    SelectedChecks[i].PR_NBR := SelectedChecks[i+1].PR_NBR;
    SelectedChecks[i].PR_CHECK_NBR := SelectedChecks[i+1].PR_CHECK_NBR;

    SelectedChecks[i].Caption := SelectedChecks[i+1].Caption;
  end;

  SetLength( SelectedChecks, Length(SelectedChecks) - 1 );

  lbChecks.Items.Delete( lbChecks.ItemIndex );
end;

procedure TfrmCheckSelect.AddPayrollUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0) and Assigned(PayrollDS.DataSet) and PayrollDS.DataSet.Active and
    (PayrollDS.DataSet.RecordCount > 0);
end;

procedure TfrmCheckSelect.AddPayrollExecute(Sender: TObject);
begin
  SetLength( SelectedChecks, Length(SelectedChecks) + 1 );

  with SelectedChecks[High(SelectedChecks)] do
  begin
    CL_NBR := CompanyDS.DataSet.FieldByName('CL_NBR').AsInteger;
    CO_NBR := CompanyDS.DataSet.FieldByName('CO_NBR').AsInteger;
    PR_NBR := PayrollDS.DataSet.FieldByName('PR_NBR').AsInteger;
    PR_CHECK_NBR := -1;

    SelectedChecks[High(SelectedChecks)].Caption := 'Company #' + CompanyDS.DataSet.FieldByName('CONUMBER').AsString +
      ', All the checks from Pr ' + PayrollDS.DataSet.FieldByName('CHECK_DATE').AsString + '-' + PayrollDS.DataSet.FieldByName('RUN_NUMBER').AsString;

    lbChecks.Items.Add( SelectedChecks[High(SelectedChecks)].Caption );
  end;
end;

procedure TfrmCheckSelect.AddPeriodUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0);
end;

procedure TfrmCheckSelect.AddPeriodExecute(Sender: TObject);
begin
  SetLength( SelectedChecks, Length(SelectedChecks) + 1 );

  with SelectedChecks[High(SelectedChecks)] do
  begin
    CL_NBR := CompanyDS.DataSet.FieldByName('CL_NBR').AsInteger;
    CO_NBR := CompanyDS.DataSet.FieldByName('CO_NBR').AsInteger;
    PR_NBR := -1;
    PR_CHECK_NBR := -1;
    Period := CheckDateRange.AsString;

    SelectedChecks[High(SelectedChecks)].Caption := 'Company #' + CompanyDS.DataSet.FieldByName('CONUMBER').AsString +
      ', All the checks from the payrolls with Check Date falls in ' + CheckDateRange.AsString; 

    lbChecks.Items.Add( SelectedChecks[High(SelectedChecks)].Caption );
  end;
end;

end.
