unit CheckSelectCommon;

interface

uses
  evoApiconnection, gdycommonlogger, DB, kbmMemTable, EvStreamUtils,
  EvoInfinityHRConstAndProc, XmlRpcTypes, Classes, Common, SysUtils;

type
  TSelectedCheck = record
    Caption: string;
    CL_NBR: integer;
    CO_NBR: integer;
    PR_NBR: integer;
    PR_CHECK_NBR: integer;
    Period: string;
  end;

  TSelectedChecks = array of TSelectedCheck;

  TPDFCheckLoader = class
  private
    FEvoAPI: IEvoAPIConnection;
    FLogger: ICommonLogger;
    FUploadCheckData: TkbmCustomMemTable;
    FSelectedCheck: TSelectedCheck;

    procedure GetCheckData;
    function IsAllowedToPrint: boolean;
  public
    constructor Create(SelectedCheck: TSelectedCheck; EvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
    destructor Destroy; override;

    procedure RunCheckReprintTasks;
    function IsTaskReady: boolean;
    function IsTaskRun: boolean;
    function GetPaystubData: TUploadFile;
    procedure DeleteTaskData;

    property CheckRePrintTasks: TkbmCustomMemTable read FUploadCheckData;
    // FieldList:
    // Pr_Nbr: integer
    // Pr_Check_Nbr: integer
    // Payment_Serial_Number: integer
    // Period_Begin_Date: TDatetime
    // Period_End_Date: TDatetime
    // Check_Date: TDatetime
    // Net_Wages: float
    // Social_Security_Number: string
    // TaskID: string
    // PRINT_VOUCHER: string
    // SHOW_MANUAL_CHECKS_IN_ESS: string
    // PRINT_MANUAL_CHECK_STUBS: string
    // CHECK_TYPE: string
    // EeName: string -> #<Custom_Employee_Number> (First_Name Middle_Initial Last_Name)
  end;

implementation

uses gdyRedir;

{ TPDFCheckLoader }

constructor TPDFCheckLoader.Create(SelectedCheck: TSelectedCheck;
  EvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
begin
  FEvoAPI := EvoAPI;
  FLogger := Logger;
  // load check info (qUploadCheckData)
  FSelectedCheck := SelectedCheck;
  try
    GetCheckData;
  except
    on e: exception do
      Logger.LogError('GetCheckData', e.Message);
  end;
end;

procedure TPDFCheckLoader.DeleteTaskData;
var
  TaskID: string;
begin
  if FUploadCheckData.Active and (FUploadCheckData.RecordCount > 0) then
  begin
    TaskID := FUploadCheckData.FieldByName('TaskID').AsString;
    if Trim(TaskID) <> '' then
      FEvoAPI.DeleteTask( TaskID );
    FUploadCheckData.Delete;
  end;
end;

destructor TPDFCheckLoader.Destroy;
begin
  // delete all tasks
  if FUploadCheckData.Active and (FUploadCheckData.RecordCount > 0) then
  begin
    FUploadCheckData.First;
    while not FUploadCheckData.Eof do
    begin
      if (Trim(FUploadCheckData.FieldByName('TaskID').AsString) <> '') then
        FEvoAPI.DeleteTask( FUploadCheckData.FieldByName('TaskID').AsString );
      FUploadCheckData.Next;
    end;
    FUploadCheckData.Close;
    FreeAndNil(FUploadCheckData);
  end;
  inherited;
end;

procedure TPDFCheckLoader.GetCheckData;
var
  Param: IRpcStruct;
  PrCheckNbrList, PrNbrList: string;
  Dat_b, Dat_e: TDatetime;

  procedure GetPrNbrsToList(var aList: string; dat_b, dat_e: TDateTime; aCoNbr: integer);
  var
    Params: IRpcStruct;
    FData: TkbmCustomMemTable;
  begin
    Params := TRpcStruct.Create;
    Params.AddItem('CoNbr', aCoNbr);
    Params.AddItemDateTime('dat_b', Dat_b);
    Params.AddItemDateTime('dat_e', Dat_e);
    try
      FData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'PrList.rwq', Params);
    except
      on e: exception do
        FLogger.LogError(Redirection.GetDirectory(sQueryDirAlias) + 'PrList.rwq', e.Message);
    end;

    if Assigned(FData) then
    try
      FData.First;
      while not FData.Eof do
      begin
        aList := aList + '"' + FData.FieldByName('PR_NBR').AsString + '",';
        FData.Next;
      end;
      if aList <> '' then
        aList := Copy(aList, 1, Length(aList) - 1);
    finally
      FreeAndNil(FData);
      Params := nil;
    end;
  end;
begin
  PrCheckNbrList := '';
  PrNbrList := '';
  FLogger.LogDebug('Open client database: CL_' + IntToStr(FSelectedCheck.CL_NBR));
  FEvoAPI.OpenClient(FSelectedCheck.CL_NBR);

  if (FSelectedCheck.PR_CHECK_NBR > -1) then // select by check
    PrCheckNbrList := '"' + IntToStr(FSelectedCheck.PR_CHECK_NBR) + '"'
  else if FSelectedCheck.PR_NBR > -1 then // select by payroll
    PrNbrList := '"' + IntToStr(FSelectedCheck.PR_NBR) + '"'
  else begin // by period
    GetDateRangeDatesByString(FSelectedCheck.Period, Dat_b, Dat_e);
    GetPrNbrsToList(PrNbrList, Dat_b, Dat_e, FSelectedCheck.CO_NBR);
  end;

  Param := TRpcStruct.Create;
  Param.AddItem( 'PrCheckNbrList', PrCheckNbrList);
  Param.AddItem( 'PrNbrList', PrNbrList);
  FUploadCheckData := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + 'qUploadCheckData.rwq', Param);
end;

function TPDFCheckLoader.GetPaystubData: TUploadFile;
begin
  if FUploadCheckData.Active and (FUploadCheckData.RecordCount > 0) then
  begin
    FLogger.LogEntry('Prepare paystub PDF file');
    try
      Result.SSN := StringReplace( Trim(FUploadCheckData.FieldByName('SOCIAL_SECURITY_NUMBER').AsString), '-', '', [rfReplaceAll]);
      Result.PayPeriod_StartDate := FUploadCheckData.FieldByName('PERIOD_BEGIN_DATE').AsDateTime;
      Result.PayPeriod_EndDate := FUploadCheckData.FieldByName('PERIOD_END_DATE').AsDateTime;
      Result.PayDate := FUploadCheckData.FieldByName('CHECK_DATE').AsDateTime;
      Result.CheckNumber := FUploadCheckData.FieldByName('Payment_Serial_Number').AsString;
      Result.DepositAmount := FUploadCheckData.FieldByName('Net_Wages').AsBCD;
      Result.FileName := 'checkstub';
      try
        Result.FileContent := FEvoAPI.GetPDFReport(FUploadCheckData.FieldByName('TaskID').AsString);
      except
        Result.FileContent := '';
      end;

      FLogger.LogContextItem('SSN', Result.SSN);
      FLogger.LogContextItem('Check Date', DateToStr(Result.PayDate));
      FLogger.LogContextItem('Check Number', Result.CheckNumber);
      FLogger.LogContextItem('File size', IntToStr(Length(Result.FileContent)) + ' bytes');
    finally
      FLogger.LogExit;
    end;
  end;
end;

function TPDFCheckLoader.IsAllowedToPrint: boolean;
begin
  Result := False;

  with FUploadCheckData do
  if Active and (RecordCount > 0) then
  begin
    if (FieldByName('CHECK_TYPE').AsString = 'M') and (FieldByName('SHOW_MANUAL_CHECKS_IN_ESS').AsString <> 'Y') then
      FLogger.LogWarning('Paystub is not printed! Check Number: ' + FieldByName('Payment_Serial_Number').AsString, 'It''s a manual check. The company option "Manual Checks in ESS" should be "Yes"')

    else if (FieldByName('CHECK_TYPE').AsString = 'M') and (FieldByName('PRINT_MANUAL_CHECK_STUBS').AsString <> 'Y') then
      FLogger.LogWarning('Paystub is not printed! Check Number: ' + FieldByName('Payment_Serial_Number').AsString, 'It''s a manual check. The company option "Print Manual Check Stubs" should be "Yes"')

    else
      Result := True;
  end;
end;

function TPDFCheckLoader.IsTaskReady: boolean;
begin
  Result := FUploadCheckData.Active and (FUploadCheckData.RecordCount > 0) and
    (Trim(FUploadCheckData.FieldByName('TaskID').AsString) <> '') and
    FEvoAPI.IsTaskFinished( FUploadCheckData.FieldByName('TaskID').AsString );
end;

function TPDFCheckLoader.IsTaskRun: boolean;
begin
  Result := FUploadCheckData.Active and (FUploadCheckData.RecordCount > 0) and
    (Trim(FUploadCheckData.FieldByName('TaskID').AsString) <> '');
end;

procedure TPDFCheckLoader.RunCheckReprintTasks;
var
  EvoReportDef: TEvoReportDef;
  Params: IRpcStruct;
  Clients, Companies, PrCheckNbrs: IRpcArray;
begin
  if FUploadCheckData.Active and (FUploadCheckData.RecordCount > 0) then
  begin
    FUploadCheckData.First;
    while not FUploadCheckData.Eof do
    begin
      if IsAllowedToPrint then
      begin
        Params := TRpcStruct.Create;
        Clients := TRpcArray.Create;
        Companies := TRpcArray.Create;
        PrCheckNbrs := TRpcArray.Create;

        Clients.AddItem(FSelectedCheck.CL_NBR);
        Params.AddItem('Clients', Clients);
        Companies.AddItem(FSelectedCheck.CO_NBR);
        Params.AddItem('Companies', Companies);
        PrCheckNbrs.AddItem(FUploadCheckData.FieldByName('Pr_Check_Nbr').AsInteger);
        Params.AddItem('PrCheckNbrs', PrCheckNbrs);

        EvoReportDef.Level := 'S';
        EvoReportDef.Number := 2590; // "Check Stub Detail"
        EvoReportDef.Params := Params; // <- Clear params before assign new ones?

        FUploadCheckData.Edit;
        try
          FUploadCheckData.FieldByName('TaskID').AsString := FEvoAPI.AddReportTask(EvoReportDef);
          FUploadCheckData.Post;
        except
          FUploadCheckData.Cancel;
        end;
      end;

      FUploadCheckData.Next;
    end;
  end;
end;

end.
 