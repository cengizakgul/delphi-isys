unit SelectCheckFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ActnList, ExtCtrls, CheckSelectCommon, gdycommonlogger,
  isSettings, common;

type
  TSelectCheckFrame = class(TFrame)
    pnlButtons: TPanel;
    btnEditCheckList: TButton;
    btnClear: TButton;
    lbChecks: TListBox;
    Actions: TActionList;
    Add: TAction;
    Clear: TAction;
    procedure AddExecute(Sender: TObject);
    procedure ClearExecute(Sender: TObject);
  private
    FLogger: ICommonLogger;
    FSettings: IisSettings;
    FEvoConnStr: string;
    FCheckList: TSelectedChecks;
    FSimpleDialog: boolean;

    procedure SetAsString(const aValue: string);
    function GetAsString: string;
  public
    procedure ShowSelectedChecks;
    procedure InitFrame(aLogger: ICommonLogger; aSettings: IisSettings; sEvoConn: string; aSimpleDialog: boolean = False);

    property CheckList: TSelectedChecks read FCheckList write FCheckList;
    property AsString: string read GetAsString write SetAsString;
  end;

implementation

{$R *.dfm}

uses CheckSelect, CheckSelectByPeriod, EvoAPIConnection;

procedure TSelectCheckFrame.AddExecute(Sender: TObject);
var
  EvoAPI: IEvoAPIConnection;
  b: boolean;
begin
  EvoAPI := CreateEvoAPIConnection( LoadEvoAPIConnectionParam( FLogger, FSettings, FEvoConnStr ), FLogger );

  if FSimpleDialog then
    b := OpenCheckSelectByPeriod( FLogger, EvoAPI, FCheckList )
  else
    b := OpenCheckSelect( FLogger, EvoAPI, FCheckList );

  if b then // load result
    ShowSelectedChecks;
end;

procedure TSelectCheckFrame.ClearExecute(Sender: TObject);
begin
  SetLength( FCheckList, 0 );
  ShowSelectedChecks;
end;

function TSelectCheckFrame.GetAsString: string;
var
  sl: TStrings;
  i: integer;
begin
  sl := TStringList.Create;
  try
    for i := Low(FCheckList) to High(FCheckList) do
    begin
      sl.Add( FCheckList[i].Caption );
      sl.Add( FCheckList[i].Period );
      sl.Add( IntToStr(FCheckList[i].CL_NBR) );
      sl.Add( IntToStr(FCheckList[i].CO_NBR) );
      sl.Add( IntToStr(FCheckList[i].PR_NBR) );
      sl.Add( IntToStr(FCheckList[i].PR_CHECK_NBR) );
    end;
    Result := sl.CommaText;
  finally
    FreeAndNil(sl);
  end;
end;

procedure TSelectCheckFrame.InitFrame(aLogger: ICommonLogger; aSettings: IisSettings;
  sEvoConn: string; aSimpleDialog: boolean = False);
begin
  FSettings := aSettings;
  FLogger := aLogger;
  FEvoConnStr := sEvoConn;
  FSimpleDialog := aSimpleDialog;
end;

procedure TSelectCheckFrame.SetAsString(const aValue: string);
var
  sl: TStrings;
  i: integer;
begin
  sl := TStringList.Create;
  try
    sl.CommaText := aValue;
    SetLength(FCheckList, sl.Count div 6);
    for i := Low(FCheckList) to High(FCheckList) do
    begin
      FCheckList[i].Caption := sl[i*6];
      FCheckList[i].Period := sl[i*6 + 1];
      FCheckList[i].CL_NBR := StrToInt(sl[i*6 + 2]);
      FCheckList[i].CO_NBR := StrToInt(sl[i*6 + 3]);
      FCheckList[i].PR_NBR := StrToInt(sl[i*6 + 4]);
      FCheckList[i].PR_CHECK_NBR := StrToInt(sl[i*6 + 5]);
    end;
  finally
    FreeAndNil(sl);
  end;
  ShowSelectedChecks;
end;

procedure TSelectCheckFrame.ShowSelectedChecks;
var
  i: integer;
begin
  lbChecks.Clear;

  for i := low(FCheckList) to high(FCheckList) do
    lbChecks.Items.Add( FCheckList[i].Caption );
end;

end.
