object SelectCheckFrame: TSelectCheckFrame
  Left = 0
  Top = 0
  Width = 422
  Height = 125
  TabOrder = 0
  object pnlButtons: TPanel
    Left = 369
    Top = 0
    Width = 53
    Height = 125
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    object btnEditCheckList: TButton
      Left = 4
      Top = 2
      Width = 45
      Height = 25
      Action = Add
      TabOrder = 0
    end
    object btnClear: TButton
      Left = 4
      Top = 32
      Width = 45
      Height = 25
      Action = Clear
      TabOrder = 1
    end
  end
  object lbChecks: TListBox
    Left = 0
    Top = 0
    Width = 369
    Height = 125
    Align = alClient
    ItemHeight = 13
    TabOrder = 1
  end
  object Actions: TActionList
    Left = 8
    Top = 8
    object Add: TAction
      Caption = 'Add'
      OnExecute = AddExecute
    end
    object Clear: TAction
      Caption = 'Clear'
      OnExecute = ClearExecute
    end
  end
end
