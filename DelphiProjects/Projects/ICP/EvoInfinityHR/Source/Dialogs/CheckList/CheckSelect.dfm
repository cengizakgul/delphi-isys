object frmCheckSelect: TfrmCheckSelect
  Left = 287
  Top = 94
  BorderStyle = bsDialog
  Caption = 'Select Check '
  ClientHeight = 558
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 805
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlCoList: TPanel
      Left = 0
      Top = 0
      Width = 305
      Height = 403
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        305
        403)
      object gbCompany: TGroupBox
        Left = 5
        Top = 4
        Width = 295
        Height = 229
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Company'
        TabOrder = 0
        DesignSize = (
          295
          229)
        object lblCoNumber: TLabel
          Left = 9
          Top = 19
          Width = 122
          Height = 13
          Caption = 'Custom Company Number'
        end
        object lblCompany: TLabel
          Left = 9
          Top = 66
          Width = 75
          Height = 13
          Caption = 'Company Name'
        end
        object edCoNumber: TEdit
          Left = 10
          Top = 36
          Width = 272
          Height = 21
          Hint = 
            'please, type here Evolution custom company number (whole number ' +
            'or just a part of number)'
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          OnKeyPress = edCoNumberKeyPress
        end
        object edCoName: TEdit
          Left = 10
          Top = 83
          Width = 272
          Height = 21
          Hint = 
            'please, type here Evolution company name (whole name or just a p' +
            'art of name)'
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          OnKeyPress = edCoNumberKeyPress
        end
        object dbgCompanies: TReDBGrid
          Left = 11
          Top = 120
          Width = 271
          Height = 94
          DisableThemesInTitle = False
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataSource = CompanyDS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
          ReadOnly = True
          TabOrder = 2
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
        end
      end
      object GroupBox1: TGroupBox
        Left = 5
        Top = 240
        Width = 295
        Height = 156
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Payroll Check Date Range'
        TabOrder = 1
        DesignSize = (
          295
          156)
        inline CheckDateRange: TfrmDateRange
          Left = 4
          Top = 14
          Width = 285
          Height = 91
          TabOrder = 0
          inherited lblFrom: TLabel
            Left = 29
            Top = 39
          end
          inherited lblTo: TLabel
            Left = 28
            Top = 66
          end
          inherited edDatB: TDateTimePicker
            Left = 61
            Top = 35
          end
          inherited edDatE: TDateTimePicker
            Left = 61
            Top = 62
          end
          inherited edTimeB: TDateTimePicker
            Left = 147
            Top = 35
          end
          inherited edTimeE: TDateTimePicker
            Left = 147
            Top = 62
          end
          inherited cbPeriod: TComboBox
            Left = 3
            Top = 3
            Width = 277
          end
        end
        object btnGetPayrolls: TButton
          Left = 112
          Top = 116
          Width = 77
          Height = 28
          Action = GetPayrolls
          Anchors = [akTop, akRight]
          TabOrder = 1
        end
      end
    end
    object pnlChecks: TPanel
      Left = 305
      Top = 0
      Width = 500
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        500
        403)
      object bvlTopLine1: TBevel
        Left = 3
        Top = 10
        Width = 490
        Height = 15
        Anchors = [akLeft, akTop, akRight]
        Shape = bsTopLine
      end
      object lblCap1: TLabel
        Left = 8
        Top = 20
        Width = 138
        Height = 13
        Caption = 'Select checks by period'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComment1: TLabel
        Left = 8
        Top = 36
        Width = 297
        Height = 28
        AutoSize = False
        Caption = 
          '(All the checks from the payrolls with Check Date fall in select' +
          'ed Payroll Check Date Range)'
        WordWrap = True
      end
      object bvlTopLine2: TBevel
        Left = 3
        Top = 86
        Width = 490
        Height = 15
        Anchors = [akLeft, akTop, akRight]
        Shape = bsTopLine
      end
      object lblCap2: TLabel
        Left = 8
        Top = 95
        Width = 123
        Height = 13
        Caption = 'Select payroll checks'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComments2: TLabel
        Left = 8
        Top = 111
        Width = 297
        Height = 28
        AutoSize = False
        Caption = '(All the checks from selected payroll)'
        WordWrap = True
      end
      object Bevel1: TBevel
        Left = 2
        Top = 142
        Width = 490
        Height = 15
        Anchors = [akLeft, akTop, akRight]
        Shape = bsTopLine
      end
      object lblCap3: TLabel
        Left = 8
        Top = 151
        Width = 144
        Height = 13
        Caption = 'Select a particular check'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComments3: TLabel
        Left = 8
        Top = 167
        Width = 297
        Height = 28
        AutoSize = False
        Caption = '(All the checks from selected payroll)'
        WordWrap = True
      end
      object dbgPayrolls: TReDBGrid
        Left = 3
        Top = 185
        Width = 158
        Height = 210
        DisableThemesInTitle = False
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Anchors = [akLeft, akTop, akBottom]
        DataSource = PayrollDS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        ReadOnly = True
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
      end
      object dbgChecks: TReDBGrid
        Left = 166
        Top = 185
        Width = 200
        Height = 210
        DisableThemesInTitle = False
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataSource = CheckDS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        ReadOnly = True
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = dbgChecksDblClick
      end
      object btnCheckByPeriod: TButton
        Left = 384
        Top = 22
        Width = 97
        Height = 25
        Action = AddPeriod
        TabOrder = 2
      end
      object btnCheckByPayroll: TButton
        Left = 384
        Top = 98
        Width = 97
        Height = 25
        Action = AddPayroll
        TabOrder = 3
      end
      object btnCheck: TButton
        Left = 384
        Top = 185
        Width = 97
        Height = 26
        Action = Add
        TabOrder = 4
      end
    end
  end
  object pnlResult: TPanel
    Left = 0
    Top = 403
    Width = 805
    Height = 114
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      805
      114)
    object lbChecks: TListBox
      Left = 5
      Top = 7
      Width = 732
      Height = 98
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
    end
    object btnNo: TButton
      Left = 744
      Top = 8
      Width = 53
      Height = 29
      Action = Remove
      TabOrder = 1
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 517
    Width = 805
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      805
      41)
    object btnOk: TButton
      Left = 637
      Top = 8
      Width = 75
      Height = 25
      Action = Ok
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 717
      Top = 8
      Width = 75
      Height = 25
      Action = Cancel
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
  end
  object Actions: TActionList
    Left = 128
    Top = 48
    object Ok: TAction
      Caption = 'Ok'
      OnExecute = OkExecute
    end
    object Cancel: TAction
      Caption = 'Cancel'
      OnExecute = CancelExecute
    end
    object GetPayrolls: TAction
      Caption = 'Get Payrolls'
      OnExecute = GetPayrollsExecute
      OnUpdate = GetPayrollsUpdate
    end
    object Add: TAction
      Caption = 'Select Check'
      OnExecute = AddExecute
      OnUpdate = AddUpdate
    end
    object Remove: TAction
      Caption = 'Remove'
      OnExecute = RemoveExecute
      OnUpdate = RemoveUpdate
    end
    object AddPayroll: TAction
      Caption = 'Select By Payroll'
      OnExecute = AddPayrollExecute
      OnUpdate = AddPayrollUpdate
    end
    object AddPeriod: TAction
      Caption = 'Select By Period'
      OnExecute = AddPeriodExecute
      OnUpdate = AddPeriodUpdate
    end
  end
  object CompanyDS: TDataSource
    Left = 96
    Top = 49
  end
  object PayrollDS: TDataSource
    Left = 94
    Top = 89
  end
  object CheckDS: TDataSource
    Left = 102
    Top = 121
  end
end
