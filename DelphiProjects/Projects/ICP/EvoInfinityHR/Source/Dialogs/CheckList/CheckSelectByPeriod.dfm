object frmCheckSelectByPeriod: TfrmCheckSelectByPeriod
  Left = 652
  Top = 22
  BorderStyle = bsDialog
  Caption = 'Select Paystubs By Period'
  ClientHeight = 558
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 321
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlCoList: TPanel
      Left = 0
      Top = 0
      Width = 320
      Height = 403
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        320
        403)
      object gbCompany: TGroupBox
        Left = 5
        Top = 4
        Width = 310
        Height = 229
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Company'
        TabOrder = 0
        DesignSize = (
          310
          229)
        object lblCoNumber: TLabel
          Left = 9
          Top = 19
          Width = 122
          Height = 13
          Caption = 'Custom Company Number'
        end
        object lblCompany: TLabel
          Left = 9
          Top = 66
          Width = 75
          Height = 13
          Caption = 'Company Name'
        end
        object edCoNumber: TEdit
          Left = 10
          Top = 36
          Width = 287
          Height = 21
          Hint = 
            'please, type here Evolution custom company number (whole number ' +
            'or just a part of number)'
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          OnKeyPress = edCoNumberKeyPress
        end
        object edCoName: TEdit
          Left = 10
          Top = 83
          Width = 287
          Height = 21
          Hint = 
            'please, type here Evolution company name (whole name or just a p' +
            'art of name)'
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          OnKeyPress = edCoNumberKeyPress
        end
        object dbgCompanies: TReDBGrid
          Left = 11
          Top = 120
          Width = 286
          Height = 94
          DisableThemesInTitle = False
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataSource = CompanyDS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
          ReadOnly = True
          TabOrder = 2
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
        end
      end
      object rgPeriod: TRadioGroup
        Left = 6
        Top = 240
        Width = 310
        Height = 157
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'Upload all paystubs for the'
        Items.Strings = (
          'past                         days'
          'Current Week'
          'Last Week'
          'Current Month'
          'Last Month')
        TabOrder = 1
      end
      object seDays: TSpinEdit
        Left = 59
        Top = 259
        Width = 64
        Height = 22
        MaxValue = 90000
        MinValue = 1
        TabOrder = 2
        Value = 1
      end
    end
  end
  object pnlResult: TPanel
    Left = 0
    Top = 403
    Width = 321
    Height = 114
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      321
      114)
    object lbChecks: TListBox
      Left = 5
      Top = 39
      Width = 308
      Height = 67
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
    end
    object btnNo: TButton
      Left = 159
      Top = 7
      Width = 74
      Height = 27
      Action = Remove
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object btnAdd: TButton
      Left = 80
      Top = 7
      Width = 74
      Height = 27
      Action = AddPeriod
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 517
    Width = 321
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      321
      41)
    object btnOk: TButton
      Left = 153
      Top = 8
      Width = 75
      Height = 25
      Action = Ok
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 233
      Top = 8
      Width = 75
      Height = 25
      Action = Cancel
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
  end
  object Actions: TActionList
    Left = 128
    Top = 48
    object Ok: TAction
      Caption = 'Ok'
      OnExecute = OkExecute
    end
    object Cancel: TAction
      Caption = 'Cancel'
      OnExecute = CancelExecute
    end
    object Remove: TAction
      Caption = 'Remove'
      OnExecute = RemoveExecute
      OnUpdate = RemoveUpdate
    end
    object AddPeriod: TAction
      Caption = 'Add'
      OnExecute = AddPeriodExecute
      OnUpdate = AddPeriodUpdate
    end
  end
  object CompanyDS: TDataSource
    Left = 96
    Top = 49
  end
end
