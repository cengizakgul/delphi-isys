unit InfinityHRConnectionDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, InfinityHRConnectionParamFrame;

type
  TfrmInfinityHRConnectionDlg = class(TfrmDialog)
    InfinityHRConnectionParamFrm: TInfinityHRConnectionParamFrm;
  protected
    procedure DoOK; override;
    procedure BeforeShowDialog; override;
  end;

implementation

uses Log, EvoInfinityHRConstAndProc;

{$R *.dfm}

procedure TfrmInfinityHRConnectionDlg.BeforeShowDialog;
begin
  InfinityHRConnectionParamFrm.Param := LoadInfinityHRConnectionParam( frmLogger.Logger, FSettings, sInfinityConn )
end;

procedure TfrmInfinityHRConnectionDlg.DoOK;
begin
  SaveInfinityHRConnectionParam(InfinityHRConnectionParamFrm.Param, FSettings, sInfinityConn);
end;

end.
