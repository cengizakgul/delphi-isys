inherited frmInfinityHRConnectionDlg: TfrmInfinityHRConnectionDlg
  Left = 477
  Top = 282
  Caption = 'Infinity HR Connection'
  ClientHeight = 155
  ClientWidth = 423
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOk: TBitBtn
    Left = 259
    Top = 120
  end
  inherited btnCancel: TBitBtn
    Left = 341
    Top = 120
  end
  inline InfinityHRConnectionParamFrm: TInfinityHRConnectionParamFrm
    Left = 4
    Top = 4
    Width = 413
    Height = 109
    TabOrder = 2
    inherited GroupBox1: TGroupBox
      Width = 413
      Height = 109
      inherited SiteEdit: TEdit
        Width = 221
      end
      inherited PasswordEdit: TPasswordEdit
        Width = 221
      end
      inherited UserNameEdit: TEdit
        Width = 221
      end
      inherited cbSavePassword: TCheckBox
        Left = 311
      end
      inherited cbEnterprise: TCheckBox
        Left = 311
      end
    end
  end
end
