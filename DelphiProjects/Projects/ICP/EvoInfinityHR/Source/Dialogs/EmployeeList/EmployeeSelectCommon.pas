unit EmployeeSelectCommon;

interface

uses
  evoApiconnection, gdycommonlogger, DB, kbmMemTable, EvStreamUtils,
  EvoInfinityHRConstAndProc, XmlRpcTypes, Classes, Common, SysUtils;

type
  TSelectedEmployee = record
    Caption: string;
    CL_NBR: integer;
    CO_NBR: integer;
    EE_NBR: integer;
    SSN: string;
  end;

  TSelectedEmployees = array of TSelectedEmployee;

  TPdfEmployeeW2Loader = class
  private
    FEvoAPI: IEvoAPIConnection;
    FLogger: ICommonLogger;
    FW2List: TkbmCustomMemTable;
    FSelectedEe: TSelectedEmployee;
    procedure RunGetW2List(aTaxYear: integer);
  public
    constructor Create(SelectedEmployee: TSelectedEmployee; EvoAPI: IEvoAPIConnection; Logger: ICommonLogger; aTaxYear: integer);
    destructor Destroy; override;

    function GetW2File: TUploadW2File;
    property W2List: TkbmCustomMemTable read FW2List;
    // FieldList:
    // CO_TAX_RETURN_RUNS_NBR: integer
    // NAME: varchar(60)
    // Year: integer
  end;

implementation

{ TPdfEmployeeW2Loader }

constructor TPdfEmployeeW2Loader.Create(
  SelectedEmployee: TSelectedEmployee; EvoAPI: IEvoAPIConnection;
  Logger: ICommonLogger; aTaxYear: integer);
begin
  FEvoAPI := EvoAPI;
  FLogger := Logger;
  // run GetW2List task
  FSelectedEe := SelectedEmployee;
  RunGetW2List(aTaxYear);
end;

destructor TPdfEmployeeW2Loader.Destroy;
begin
  FreeAndNil(FW2List);
  inherited;
end;

function TPdfEmployeeW2Loader.GetW2File: TUploadW2File;
begin
  if FW2List.Active and (FW2List.RecordCount > 0) then
  begin
    FLogger.LogEntry('Prepare W2 PDF file');
    try
      Result.SSN := StringReplace( Trim(FSelectedEe.SSN), '-', '', [rfReplaceAll]);
      Result.EeCaption := FSelectedEe.Caption;
      Result.TaxYear := FW2List.FieldByName('Year').AsInteger;
      Result.FileContent := FEvoAPI.GetW2(-FW2List.FieldByName('CO_TAX_RETURN_RUNS_NBR').AsInteger);

      FLogger.LogContextItem('SSN', Result.SSN);
      FLogger.LogContextItem('Tax Year', IntToStr(Result.TaxYear));
      FLogger.LogContextItem('File size', IntToStr(Length(Result.FileContent)) + ' bytes');
    finally
      FLogger.LogExit;
    end;
  end;
end;

procedure TPdfEmployeeW2Loader.RunGetW2List(aTaxYear: integer);
var
  EeRuns: TkbmCustomMemTable;
begin
  FLogger.LogDebug('Open client database: CL_' + IntToStr(FSelectedEe.CL_NBR));
  FEvoAPI.OpenClient(FSelectedEe.CL_NBR);
  // get dataset:
  // CO_TAX_RETURN_RUNS_NBR, DataType: ftInteger
  // NAME, DataType: ftString; Size: 60
  // Year, DataType: ftInteger
  FW2List := FEvoAPI.GetW2List(FSelectedEe.EE_NBR);

  if FW2List.Active and (FW2List.RecordCount > 0) then
  try
    FW2List.Filter := 'YEAR=' + IntToStr(aTaxYear);
    FW2List.Filtered := True;

    // remove Runs of the other Employee (in case one SSN assigned to a few Employees)
    EeRuns :=  FEvoAPI.OpenSQL(
      'SELECT t1.CO_TAX_RETURN_RUNS_NBR FROM CO_TAX_RETURN_RUNS t1 '+
      'where {AsOfNow<t1>} and t1.EE_NBR=' + IntToStr(FSelectedEe.EE_NBR),
      FSelectedEe.CL_NBR,
      '' );

    if Assigned(EeRuns) and EeRuns.Active and (EeRuns.RecordCount > 0) then
    begin
      FW2List.First;
      FW2List.Next; // if there is only one, it should be uploaded even if it belongs to the other EE (but the same CL_PERSON)
                    // does not look good, but at the moment I see no other way with such requirements: #103660 vs #105595
      while not FW2List.Eof do
      begin
        if not EeRuns.Locate('CO_TAX_RETURN_RUNS_NBR', FW2List.FieldByName('CO_TAX_RETURN_RUNS_NBR').AsInteger, []) then
          FW2List.Delete
        else
          FW2List.Next;
      end;
    end;
  finally
    if Assigned(EeRuns) then
      FreeAndNil(EeRuns);
  end;
end;

end.
