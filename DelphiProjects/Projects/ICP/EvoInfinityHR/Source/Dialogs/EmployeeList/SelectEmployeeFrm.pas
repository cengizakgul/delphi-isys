unit SelectEmployeeFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ActnList, ExtCtrls, EmployeeSelectCommon, gdycommonlogger,
  isSettings, common;

type
  TSelecEmployeeFrame = class(TFrame)
    pnlButtons: TPanel;
    btnEditEeList: TButton;
    btnClear: TButton;
    lbSelected: TListBox;
    Actions: TActionList;
    Add: TAction;
    Clear: TAction;
    procedure AddExecute(Sender: TObject);
    procedure ClearExecute(Sender: TObject);
  private
    FLogger: ICommonLogger;
    FSettings: IisSettings;
    FEvoConnStr: string;
    FEmployeeList: TSelectedEmployees;

    procedure SetAsString(const aValue: string);
    function GetAsString: string;
  public
    procedure ShowSelectedEmployees;
    procedure InitFrame(aLogger: ICommonLogger; aSettings: IisSettings; sEvoConn: string; aSimpleDialog: boolean = False);

    property EmployeeList: TSelectedEmployees read FEmployeeList write FEmployeeList;
    property AsString: string read GetAsString write SetAsString;
  end;

implementation

{$R *.dfm}

uses EmployeeSelect, EvoAPIConnection;

procedure TSelecEmployeeFrame.AddExecute(Sender: TObject);
var
  EvoAPI: IEvoAPIConnection;
begin
  EvoAPI := CreateEvoAPIConnection( LoadEvoAPIConnectionParam( FLogger, FSettings, FEvoConnStr ), FLogger );
  if OpenEmployeeSelect( FLogger, EvoAPI, FEmployeeList ) then // load result
    ShowSelectedEmployees;
end;

procedure TSelecEmployeeFrame.ClearExecute(Sender: TObject);
begin
  SetLength( FEmployeeList, 0 );
  ShowSelectedEmployees;
end;

function TSelecEmployeeFrame.GetAsString: string;
var
  sl: TStrings;
  i: integer;
begin
  sl := TStringList.Create;
  try
    for i := Low(FEmployeeList) to High(FEmployeeList) do
    begin
      sl.Add( FEmployeeList[i].Caption );
      sl.Add( IntToStr(FEmployeeList[i].CL_NBR) );
      sl.Add( IntToStr(FEmployeeList[i].CO_NBR) );
      sl.Add( IntToStr(FEmployeeList[i].EE_NBR) );
      sl.Add( FEmployeeList[i].SSN );
    end;
    Result := sl.CommaText;
  finally
    FreeAndNil(sl);
  end;
end;

procedure TSelecEmployeeFrame.InitFrame(aLogger: ICommonLogger; aSettings: IisSettings;
  sEvoConn: string; aSimpleDialog: boolean = False);
begin
  FSettings := aSettings;
  FLogger := aLogger;
  FEvoConnStr := sEvoConn;
end;

procedure TSelecEmployeeFrame.SetAsString(const aValue: string);
var
  sl: TStrings;
  i: integer;
begin
  sl := TStringList.Create;
  try
    sl.CommaText := aValue;
    SetLength(FEmployeeList, sl.Count div 5);
    for i := Low(FEmployeeList) to High(FEmployeeList) do
    begin
      FEmployeeList[i].Caption := sl[i*5];
      FEmployeeList[i].CL_NBR := StrToInt(sl[i*5 + 1]);
      FEmployeeList[i].CO_NBR := StrToInt(sl[i*5 + 2]);
      FEmployeeList[i].EE_NBR := StrToInt(sl[i*5 + 3]);
      FEmployeeList[i].SSN := sl[i*5 + 4];
    end;
  finally
    FreeAndNil(sl);
  end;
  ShowSelectedEmployees;
end;

procedure TSelecEmployeeFrame.ShowSelectedEmployees;
var
  i: integer;
begin
  lbSelected.Clear;

  for i := low(FEmployeeList) to high(FEmployeeList) do
    lbSelected.Items.Add( FEmployeeList[i].Caption );
end;

end.
