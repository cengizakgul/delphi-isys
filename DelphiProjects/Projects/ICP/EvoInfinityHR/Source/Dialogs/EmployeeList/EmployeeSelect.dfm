object frmEmployeeSelect: TfrmEmployeeSelect
  Left = 294
  Top = 64
  BorderStyle = bsDialog
  Caption = 'Select Employee '
  ClientHeight = 558
  ClientWidth = 806
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 806
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlCoList: TPanel
      Left = 0
      Top = 0
      Width = 306
      Height = 403
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        306
        403)
      object gbCompany: TGroupBox
        Left = 5
        Top = 4
        Width = 296
        Height = 333
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Company'
        TabOrder = 0
        DesignSize = (
          296
          333)
        object lblCoNumber: TLabel
          Left = 9
          Top = 19
          Width = 122
          Height = 13
          Caption = 'Custom Company Number'
        end
        object lblCompany: TLabel
          Left = 9
          Top = 66
          Width = 75
          Height = 13
          Caption = 'Company Name'
        end
        object edCoNumber: TEdit
          Left = 10
          Top = 36
          Width = 273
          Height = 21
          Hint = 
            'please, type here Evolution custom company number (whole number ' +
            'or just a part of number)'
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          OnKeyPress = edCoNumberKeyPress
        end
        object edCoName: TEdit
          Left = 10
          Top = 83
          Width = 273
          Height = 21
          Hint = 
            'please, type here Evolution company name (whole name or just a p' +
            'art of name)'
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          OnKeyPress = edCoNumberKeyPress
        end
        object dbgCompanies: TReDBGrid
          Left = 11
          Top = 120
          Width = 272
          Height = 198
          DisableThemesInTitle = False
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataSource = CompanyDS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
          ReadOnly = True
          TabOrder = 2
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
        end
      end
      object gbGetEmployees: TGroupBox
        Left = 5
        Top = 339
        Width = 296
        Height = 58
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        object Button1: TButton
          Left = 101
          Top = 16
          Width = 89
          Height = 29
          Action = GetEmployees
          TabOrder = 0
        end
      end
    end
    object pnlChecks: TPanel
      Left = 306
      Top = 0
      Width = 500
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        500
        403)
      object bvlTopLine1: TBevel
        Left = 3
        Top = 10
        Width = 490
        Height = 15
        Anchors = [akLeft, akTop, akRight]
        Shape = bsTopLine
      end
      object lblCap1: TLabel
        Left = 8
        Top = 20
        Width = 139
        Height = 13
        Caption = 'Select all the employees'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblComment1: TLabel
        Left = 8
        Top = 36
        Width = 297
        Height = 28
        AutoSize = False
        Caption = '(All the employees from the selected Company)'
        WordWrap = True
      end
      object Bevel1: TBevel
        Left = 2
        Top = 78
        Width = 490
        Height = 15
        Anchors = [akLeft, akTop, akRight]
        Shape = bsTopLine
      end
      object lblCap3: TLabel
        Left = 8
        Top = 87
        Width = 162
        Height = 13
        Caption = 'Select a particular employee'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbgEmployees: TReDBGrid
        Left = 3
        Top = 124
        Width = 374
        Height = 271
        DisableThemesInTitle = False
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Anchors = [akLeft, akTop, akBottom]
        DataSource = EmployeeDS
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        ReadOnly = True
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = dbgEmployeesDblClick
      end
      object btnCheckByPeriod: TButton
        Left = 384
        Top = 22
        Width = 97
        Height = 25
        Action = AddCompany
        TabOrder = 1
      end
      object btnCheck: TButton
        Left = 384
        Top = 124
        Width = 97
        Height = 26
        Action = Add
        TabOrder = 2
      end
    end
  end
  object pnlResult: TPanel
    Left = 0
    Top = 403
    Width = 806
    Height = 114
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      806
      114)
    object lbSelected: TListBox
      Left = 5
      Top = 7
      Width = 733
      Height = 98
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
    end
    object btnNo: TButton
      Left = 744
      Top = 8
      Width = 53
      Height = 29
      Action = Remove
      TabOrder = 1
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 517
    Width = 806
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      806
      41)
    object btnOk: TButton
      Left = 638
      Top = 8
      Width = 75
      Height = 25
      Action = Ok
      Anchors = [akTop, akRight]
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 718
      Top = 8
      Width = 75
      Height = 25
      Action = Cancel
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
  end
  object Actions: TActionList
    Left = 128
    Top = 48
    object Ok: TAction
      Caption = 'Ok'
      OnExecute = OkExecute
    end
    object Cancel: TAction
      Caption = 'Cancel'
      OnExecute = CancelExecute
    end
    object GetEmployees: TAction
      Caption = 'Get Employees'
      OnExecute = GetEmployeesExecute
      OnUpdate = GetEmployeesUpdate
    end
    object Add: TAction
      Caption = 'Select Employee'
      OnExecute = AddExecute
      OnUpdate = AddUpdate
    end
    object Remove: TAction
      Caption = 'Remove'
      OnExecute = RemoveExecute
      OnUpdate = RemoveUpdate
    end
    object AddCompany: TAction
      Caption = 'Select All'
      OnExecute = AddCompanyExecute
      OnUpdate = AddCompanyUpdate
    end
  end
  object CompanyDS: TDataSource
    Left = 96
    Top = 49
  end
  object EmployeeDS: TDataSource
    Left = 94
    Top = 89
  end
end
