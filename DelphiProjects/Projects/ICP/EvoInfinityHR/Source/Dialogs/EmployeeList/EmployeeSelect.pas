unit EmployeeSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, dbcomp,
  DB, evoApiconnection, gdycommonlogger, EmployeeSelectCommon,
  EeHolder, common, wwdblook;

type
  TfrmEmployeeSelect = class(TForm)
    pnlMain: TPanel;
    Actions: TActionList;
    pnlResult: TPanel;
    pnlBottom: TPanel;
    btnOk: TButton;
    btnCancel: TButton;
    CompanyDS: TDataSource;
    Ok: TAction;
    Cancel: TAction;
    pnlCoList: TPanel;
    pnlChecks: TPanel;
    lbSelected: TListBox;
    GetEmployees: TAction;
    dbgEmployees: TReDBGrid;
    btnNo: TButton;
    Add: TAction;
    Remove: TAction;
    EmployeeDS: TDataSource;
    gbCompany: TGroupBox;
    lblCoNumber: TLabel;
    lblCompany: TLabel;
    edCoNumber: TEdit;
    edCoName: TEdit;
    dbgCompanies: TReDBGrid;
    bvlTopLine1: TBevel;
    btnCheckByPeriod: TButton;
    btnCheck: TButton;
    lblCap1: TLabel;
    lblComment1: TLabel;
    Bevel1: TBevel;
    lblCap3: TLabel;
    AddCompany: TAction;
    gbGetEmployees: TGroupBox;
    Button1: TButton;
    procedure CancelExecute(Sender: TObject);
    procedure OkExecute(Sender: TObject);
    procedure GetEmployeesExecute(Sender: TObject);
    procedure GetEmployeesUpdate(Sender: TObject);
    procedure AddUpdate(Sender: TObject);
    procedure RemoveUpdate(Sender: TObject);
    procedure AddExecute(Sender: TObject);
    procedure dbgChecksDblClick(Sender: TObject);
    procedure edCoNumberKeyPress(Sender: TObject; var Key: Char);
    procedure RemoveExecute(Sender: TObject);
    procedure AddCompanyUpdate(Sender: TObject);
    procedure AddCompanyExecute(Sender: TObject);
    procedure dbgEmployeesDblClick(Sender: TObject);
  private
    FCompanies: TEvoCoEeList;
    FLogger: ICommonLogger;
    FEvoAPI: IEvoAPIConnection;
    SelectedEmployees: TSelectedEmployees;

  protected
    procedure LoadCompanyList;

    procedure ClearForm;
    procedure Init(aEvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
    procedure GetResult(var Res: TSelectedEmployees);
    procedure SetResult(Res: TSelectedEmployees);
  end;


function OpenEmployeeSelect(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; var Res: TSelectedEmployees): boolean;

implementation

uses Math;

{$R *.dfm}

function OpenEmployeeSelect(Logger: ICommonLogger; EvoAPI: IEvoAPIConnection; var Res: TSelectedEmployees): boolean;
var
  frmEmployeeSelect: TfrmEmployeeSelect;
begin
  frmEmployeeSelect := TfrmEmployeeSelect.Create(nil);
  try
    frmEmployeeSelect.SetResult( Res );
    frmEmployeeSelect.Init( EvoAPI, Logger );
    Result := frmEmployeeSelect.ShowModal = mrOk;
    if Result then
      frmEmployeeSelect.GetResult( Res );
  finally
    frmEmployeeSelect.ClearForm;
    FreeAndNil(frmEmployeeSelect);
  end;
end;

{ TfrmEmployeeSelect }

procedure TfrmEmployeeSelect.GetResult(var Res: TSelectedEmployees);
var i: integer;
begin
  SetLength(Res, Length(SelectedEmployees));
  for i := low(SelectedEmployees) to high(SelectedEmployees) do
  begin
    Res[i].Caption := SelectedEmployees[i].Caption;
    Res[i].CL_NBR := SelectedEmployees[i].CL_NBR;
    Res[i].CO_NBR := SelectedEmployees[i].CO_NBR;
    Res[i].EE_NBR := SelectedEmployees[i].EE_NBR;
    Res[i].SSN := SelectedEmployees[i].SSN;
  end;
end;

procedure TfrmEmployeeSelect.Init(aEvoAPI: IEvoAPIConnection; Logger: ICommonLogger);
begin
  FEvoAPI := aEvoAPI;
  FLogger := Logger;
  FCompanies := TEvoCoEeList.Create(FLogger, FEvoAPI);

  AddSelected(dbgCompanies.Selected, 'CoNumber', 10, 'Number', true);
  AddSelected(dbgCompanies.Selected, 'NAME', 25, 'Name', true);

  AddSelected(dbgEmployees.Selected, 'EeCode', 15, 'EeCode', true);
  AddSelected(dbgEmployees.Selected, 'SSN', 15, 'SSN', true);
  AddSelected(dbgEmployees.Selected, 'FirstName', 25, 'First Name', true);
  AddSelected(dbgEmployees.Selected, 'MI', 2, 'MI', true);
  AddSelected(dbgEmployees.Selected, 'LastName', 25, 'Last Name', true);

  dbgCompanies.Enabled := False;
end;

procedure TfrmEmployeeSelect.CancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmEmployeeSelect.OkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmEmployeeSelect.GetEmployeesExecute(Sender: TObject);
begin
  FCompanies.LoadEmployees;
  EmployeeDS.DataSet := FCompanies.EeList;
end;

procedure TfrmEmployeeSelect.GetEmployeesUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0);
end;

procedure TfrmEmployeeSelect.AddUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0) and Assigned(EmployeeDS.DataSet) and EmployeeDS.DataSet.Active and
    (EmployeeDS.DataSet.RecordCount > 0);
end;

procedure TfrmEmployeeSelect.RemoveUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (lbSelected.Items.Count > 0) and (lbSelected.ItemIndex >= 0);
end;

procedure TfrmEmployeeSelect.ClearForm;
begin
  FLogger := nil;
  CompanyDS.DataSet := nil;
  EmployeeDS.DataSet := nil;

  FreeAndNil(FCompanies);
end;

procedure TfrmEmployeeSelect.AddExecute(Sender: TObject);
var
  s: string;
begin
  s := 'Company #' + CompanyDS.DataSet.FieldByName('CONUMBER').AsString +
      ', Ee #' + EmployeeDS.DataSet.FieldByName('EeCode').AsString;
  if lbSelected.Items.IndexOf( s ) < 0 then
  begin
    SetLength( SelectedEmployees, Length(SelectedEmployees) + 1 );

    with SelectedEmployees[High(SelectedEmployees)] do
    begin
      CL_NBR := CompanyDS.DataSet.FieldByName('CL_NBR').AsInteger;
      CO_NBR := CompanyDS.DataSet.FieldByName('CO_NBR').AsInteger;
      EE_NBR := EmployeeDS.DataSet.FieldByName('EE_NBR').AsInteger;
      SSN := StringReplace( Trim(EmployeeDS.DataSet.FieldByName('SSN').AsString), '-', '', [rfReplaceAll]);
      
      SelectedEmployees[High(SelectedEmployees)].Caption := s;

      lbSelected.Items.Add( SelectedEmployees[High(SelectedEmployees)].Caption );
    end;
  end;  
end;

procedure TfrmEmployeeSelect.SetResult(Res: TSelectedEmployees);
var i: integer;
begin
  lbSelected.Clear;
  SetLength(SelectedEmployees, Length(Res));
  for i := low(res) to high(res) do
  begin
    lbSelected.Items.Add( res[i].Caption );

    SelectedEmployees[i].Caption := res[i].Caption;
    SelectedEmployees[i].CL_NBR := res[i].CL_NBR;
    SelectedEmployees[i].CO_NBR := res[i].CO_NBR;
    SelectedEmployees[i].EE_NBR := res[i].EE_NBR;
    SelectedEmployees[i].SSN := res[i].SSN;
  end;
end;

procedure TfrmEmployeeSelect.dbgChecksDblClick(Sender: TObject);
begin
  AddExecute(Sender);
end;

procedure TfrmEmployeeSelect.LoadCompanyList;
begin
  dbgCompanies.Enabled := False;
  CompanyDS.DataSet := nil;
  try
    if (Trim(edCoNumber.Text) <> '') or (Trim(edCoName.Text) <> '') then
      CompanyDS.DataSet := FCompanies.GetCoList( Trim(edCoNumber.Text), Trim(edCoName.Text) )
    else
      ShowMessage('Please, type in either Custom Company Number or Company Name');
  finally
    if assigned(CompanyDS.DataSet) then
    begin
      if CompanyDS.DataSet.RecordCount > 0 then
        dbgCompanies.Enabled := True
      else
        ShowMessage('No Companies with such name or number were found');
    end;
  end;
end;

procedure TfrmEmployeeSelect.edCoNumberKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    LoadCompanyList;
end;

procedure TfrmEmployeeSelect.RemoveExecute(Sender: TObject);
var
  i: integer;
begin
  for i := lbSelected.ItemIndex to Length(SelectedEmployees) - 2 do
  begin
    SelectedEmployees[i].CL_NBR := SelectedEmployees[i+1].CL_NBR;
    SelectedEmployees[i].CO_NBR := SelectedEmployees[i+1].CO_NBR;
    SelectedEmployees[i].EE_NBR := SelectedEmployees[i+1].EE_NBR;
    SelectedEmployees[i].Caption := SelectedEmployees[i+1].Caption;
    SelectedEmployees[i].SSN := SelectedEmployees[i+1].SSN;
  end;

  SetLength( SelectedEmployees, Length(SelectedEmployees) - 1 );

  lbSelected.Items.Delete( lbSelected.ItemIndex );
end;

procedure TfrmEmployeeSelect.AddCompanyUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(CompanyDS.DataSet) and CompanyDS.DataSet.Active and
    (CompanyDS.DataSet.RecordCount > 0);
end;

procedure TfrmEmployeeSelect.AddCompanyExecute(Sender: TObject);
begin
  SetLength( SelectedEmployees, Length(SelectedEmployees) + 1 );

  with SelectedEmployees[High(SelectedEmployees)] do
  begin
    CL_NBR := CompanyDS.DataSet.FieldByName('CL_NBR').AsInteger;
    CO_NBR := CompanyDS.DataSet.FieldByName('CO_NBR').AsInteger;
    EE_NBR := -1;

    SelectedEmployees[High(SelectedEmployees)].Caption := 'Company #' + CompanyDS.DataSet.FieldByName('CONUMBER').AsString +
      ', All the employees';

    lbSelected.Items.Add( SelectedEmployees[High(SelectedEmployees)].Caption );
  end;
end;

procedure TfrmEmployeeSelect.dbgEmployeesDblClick(Sender: TObject);
begin
  Add.Execute;  
end;

end.
