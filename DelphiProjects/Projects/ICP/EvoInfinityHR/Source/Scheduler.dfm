object frmScheduler: TfrmScheduler
  Left = 416
  Top = 200
  Width = 545
  Height = 287
  Caption = 'Scheduled Task List'
  Color = clBtnFace
  Constraints.MinHeight = 250
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    537
    253)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TPanel
    Left = 8
    Top = 8
    Width = 369
    Height = 237
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      369
      237)
    object lbTasks: TListBox
      Left = 8
      Top = 8
      Width = 354
      Height = 221
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
      OnDblClick = lbTasksDblClick
    end
  end
  object btnAdd: TBitBtn
    Left = 384
    Top = 8
    Width = 144
    Height = 25
    Action = Add
    Anchors = [akTop, akRight]
    Caption = 'Add Import Task'
    TabOrder = 1
  end
  object btnDelete: TBitBtn
    Left = 384
    Top = 139
    Width = 144
    Height = 25
    Action = Remove
    Anchors = [akTop, akRight]
    Caption = 'Remove'
    TabOrder = 2
  end
  object btnClose: TBitBtn
    Left = 384
    Top = 220
    Width = 144
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 3
  end
  object BitBtn1: TBitBtn
    Left = 384
    Top = 107
    Width = 144
    Height = 25
    Action = Edit
    Anchors = [akTop, akRight]
    Caption = 'Edit'
    TabOrder = 4
  end
  object btnAdd2: TBitBtn
    Left = 384
    Top = 39
    Width = 144
    Height = 25
    Action = Add2
    Anchors = [akTop, akRight]
    Caption = 'Add Upload Paystubs Task'
    TabOrder = 5
  end
  object btnAdd3: TBitBtn
    Left = 384
    Top = 70
    Width = 144
    Height = 25
    Action = Add3
    Anchors = [akTop, akRight]
    Caption = 'Add Upload W2 Task'
    TabOrder = 6
  end
  object Actions: TActionList
    Left = 32
    Top = 32
    object Remove: TAction
      Caption = 'Remove'
      OnExecute = RemoveExecute
      OnUpdate = RemoveUpdate
    end
    object Add: TAction
      Caption = 'Add Import Task'
      Hint = 'Add Employee Information Import Task'
      OnExecute = AddExecute
      OnUpdate = AddUpdate
    end
    object Edit: TAction
      Caption = 'Edit'
      OnExecute = EditExecute
      OnUpdate = EditUpdate
    end
    object Add2: TAction
      Caption = 'Add Upload Paystubs Task'
      Hint = 'Add upload check paystubs task'
      OnExecute = Add2Execute
      OnUpdate = AddUpdate
    end
    object Add3: TAction
      Caption = 'Add Upload W2 Task'
      Hint = 'Add Upload W2 Task'
      OnExecute = Add3Execute
      OnUpdate = AddUpdate
    end
  end
end
