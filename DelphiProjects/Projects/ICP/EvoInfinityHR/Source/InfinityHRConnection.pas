unit InfinityHRConnection;

interface

uses gdyCommonLogger, xmlintf, EvoInfinityHRConstAndProc, EvoInfinityMapping,
  classes, sysutils, IdHTTP, IdSSLOpenSSL, idintercept, gdycommon, Types,
  gdyRedir, common, xmldoc, variants, PayrollSync, Rio, SOAPHTTPClient,
  InvokeRegistry, SOAPConn, DB, kbmMemTable, XSBuiltIns, WinInet, FMTBcd,
  Status;

type
  IInfinityHRConnection = interface
    procedure GetEmployeesByDateRange(dat_b, dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger; aEnterprise: boolean);
    procedure UpsertEmployeePayStubs(aFile: TUploadFile; FLogger: ICommonLogger; aEnterprise: boolean);
    procedure UpsertEmployeeW2(aFile: TUploadW2File; FLogger: ICommonLogger; aEnterprise: boolean);
  end;

  TInfinityHRConnection = class(TInterfacedObject, IInfinityHRConnection)
  public
    constructor Create(logger: ICommonLogger; param: TInfinityHRConnectionParam);
    destructor Destroy; override;
  private
    FService: PayrollSyncSoap;
    FLogger: ICommonLogger;
    FParam: TInfinityHRConnectionParam;
    FSession: string;

    procedure Login;

    procedure BeginConnection;
    procedure EndConnection;

    {IInfinityHRConnection}
    procedure GetEmployeesByDateRange(dat_b, dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger; aEnterprise: boolean); virtual;
    procedure UpsertEmployeePayStubs(aFile: TUploadFile; FLogger: ICommonLogger; aEnterprise: boolean); virtual;
    procedure UpsertEmployeeW2(aFile: TUploadW2File; FLogger: ICommonLogger; aEnterprise: boolean); virtual;
  end;

  TStableInfinityHRConnection = class(TInfinityHRConnection)
    {IInfinityHRConnection}
    procedure GetEmployeesByDateRange(dat_b, dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger; aEnterprise: boolean); override;
    procedure UpsertEmployeePayStubs(aFile: TUploadFile; FLogger: ICommonLogger; aEnterprise: boolean); override;
    procedure UpsertEmployeeW2(aFile: TUploadW2File; FLogger: ICommonLogger; aEnterprise: boolean); override;
  end;

function CreateInfinityHRConnection(logger: ICommonLogger; param: TInfinityHRConnectionParam): IInfinityHRConnection;

implementation

uses
  gdyGlobalWaitIndicator, DateUtils, DelphiNotesUTC, TypInfo;

const
  sCtxComponentHRISLink = 'InfintyHR connection';

function CreateInfinityHRConnection(logger: ICommonLogger; param: TInfinityHRConnectionParam): IInfinityHRConnection;
begin
  Result := TStableInfinityHRConnection.Create(logger, param);
end;

procedure TInfinityHRConnection.BeginConnection;
begin
  WaitIndicator.StartWait('Connecting to Web Service...');
  FService := GetPayrollSyncSoap( false, FParam.URL );
end;

constructor TInfinityHRConnection.Create(logger: ICommonLogger;
  param: TInfinityHRConnectionParam);
begin
  FLogger := logger;
  FParam := param;
  Login;
end;

destructor TInfinityHRConnection.Destroy;
begin
  inherited;
end;

procedure TInfinityHRConnection.EndConnection;
begin
  try
    FService := nil;
  finally
    WaitIndicator.EndWait;
  end;
end;

procedure TInfinityHRConnection.GetEmployeesByDateRange(dat_b,
  dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger; aEnterprise: boolean);
var
  EeReq: EmployeeRequest;
  CoReq: CompanyRequest;
  BeginDate, EndDate: TXSDateTime;
  EeResp{, EeIDs}: EmployeeData;
  CoResp: CompanyData;
  TimeOut: integer;
  i: integer;
  TimeDiff: TDateTime;

  function GetMinSecDifference(aServerTime, aLocalTime: TDateTime): TDateTime;
  var
    Hour1, Hour2, Min1, Min2, Sec1, Sec2, MSec1, MSec2: Word;
  begin
    DecodeTime(aServerTime, Hour1, Min1, Sec1, MSec1);
    DecodeTime(aLocalTime, Hour2, Min2, Sec2, MSec2);

    Result := EncodeTime(Hour1, Min1, Sec1, MSec1) - EncodeTime(Hour1, Min2, Sec2, MSec2);
  end;

  procedure GetEmployeesAttempt;
{  var
    i, InfinityHREmployeeID: integer;}
  begin
    if dat_e < Today then
    begin
      EeResp := FService.GetEmployeesByDateRange( EeReq, BeginDate, EndDate );
      EeData.LoadFromEeXML( EeResp );
    end
    else begin
      EeResp := FService.GetEmployeesByEffectiveDate( EeReq, BeginDate );
      EeData.LoadFromEeXML( EeResp );
      // reso #97814  
{      EeIDs := FService.GetEmployeeIDs_ByEffectiveDate( EeReq, BeginDate );
      for i := Low(EeIDs.Employees) to High(EeIDs.Employees) do
      begin
        InfinityHREmployeeID := GetOrdProp(EeIDs.Employees[i], 'InfinityHREmployeeID');
        EeResp := FService.GetEmployeeByHREmployeeID(EeReq, InfinityHREmployeeID);
        EeData.LoadFromEeXML( EeResp );
      end;}
    end;
  end;

  procedure CallGetEmployees(const aVendorID: string);
  var
    SecondAttempt: boolean;
  begin
    FLogger.LogContextItem('Get employees for VendorID ', aVendorID);
    EeReq.VendorID := aVendorID;
    EeReq.SessionID := FSession;
    EeReq.ShowAllDeductions := True;
    SecondAttempt := False;

    try
      GetEmployeesAttempt;
    except
      on e: Exception do
      begin
        SecondAttempt := True;
        FLogger.LogDebug('Error occured. Try one more.', e.Message);
      end;
    end;

    if SecondAttempt then
      GetEmployeesAttempt;
  end;
begin
  FLogger.LogEntry('InfinityHR GetEnterpriseCompanyInfo');
  EeReq := EmployeeRequest.Create;
  BeginDate := TXSDateTime.Create;
  EndDate := TXSDateTime.Create;
  try
    FLogger.LogContextItem('VendorID', FParam.UserName);
    FLogger.LogContextItem('Enterprise', BoolToStr(FParam.Enterprise));
    FLogger.LogContextItem('SessionID', FSession);
    try
      BeginConnection;
      try
        TimeDiff := GetMinSecDifference( FService.GetServerTime.AsDateTime, Now );

        BeginDate.AsUTCDateTime := LocalTimeToUTC(dat_b + TimeDiff);
        EndDate.AsUTCDateTime := LocalTimeToUTC(dat_e + TimeDiff);
        FLogger.LogContextItem('Begin Date', DateTimeToStr(BeginDate.AsUTCDateTime));
        FLogger.LogContextItem('End Date', DateTimeToStr(EndDate.AsUTCDateTime));

        TimeOut := 1800000; // 30 min
        InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
        InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
        InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

        if aEnterprise then
        begin
          CoReq := CompanyRequest.Create;
          try
            CoReq.VendorID := {'6a9e57fe-6dcd-43d3-ac64-d99ed16be370';//}FParam.UserName;
            CoReq.SessionID := FSession;
            CoResp := FService.GetEnterpriseCompanyInfo( CoReq );
            for i := Low(CoResp.Companies) to High(CoResp.Companies) do
              CallGetEmployees( CoResp.Companies[i].Vendor_ID );
          finally
            FreeAndNil(CoReq);
          end
        end
        else
          CallGetEmployees( FParam.UserName );

      finally
        EndConnection;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FreeAndNil(EeReq);
    FreeAndNil(BeginDate);
    FreeAndNil(EndDate);
    FLogger.LogExit;
  end;
end;

procedure TInfinityHRConnection.Login;
var
  Req: AuthRequest;
  Resp: AuthResponse;
begin
  FSession := '';
  FLogger.LogEntry('InfinityHR authenticate');
  Req := AuthRequest.Create;
  try
    Req.VendorID := FParam.UserName;
    Req.VendorPwd := FParam.Password;

    FLogger.LogContextItem('InfinityHR User Name', FParam.UserName);
    try
      BeginConnection;
      try
        Resp := FService.Authenticate( Req );
      finally
        EndConnection;
      end;
      FSession := Resp.SessionID;
    except
      FLogger.PassthroughException;
    end;
  finally
    Req.Free;
    FLogger.LogExit;
  end;
end;

procedure TInfinityHRConnection.UpsertEmployeePayStubs(
  aFile: TUploadFile; FLogger: ICommonLogger; aEnterprise: boolean);
var
  FileReq: PaystubRequest;
  sFile: PaystubFile;
  APaystubFiles: ArrayOfPaystubFile;
  TimeOut: integer;
  FileStream: TStringStream;
  // * for debug only *
  //testFS: TFileStream;
  // ******************
  fContent: TByteDynArray;
  CoReq: CompanyRequest;
  CoResp: CompanyData;

  procedure UploadPaystubs;
  var
    i: integer;
    UploadSuccessfull: boolean;
    errMessage: string;
  begin
    // send paystubs
    BeginConnection;
    try
      FileReq.SessionID := FSession;
      FileReq.SSN := aFile.SSN;
      FileReq.Paystub_Files := APaystubFiles;

      TimeOut := 1800000; // 30 min
      InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

      UploadSuccessfull := False;
      errMessage := '';

      if aEnterprise then
      begin
        CoReq := CompanyRequest.Create;
        try
          CoReq.VendorID := FParam.UserName;
          CoReq.SessionID := FSession;
          CoResp := FService.GetEnterpriseCompanyInfo( CoReq );
          FLogger.LogEntry('Upload paystub file <Enterprise> (SSN: ' + aFile.SSN + ', Check Number: ' + aFile.CheckNumber + ')');
          try
            i := Low(CoResp.Companies);
            while (i <= High(CoResp.Companies)) and not UploadSuccessfull do
            begin
              FLogger.LogContextItem('Vendor_ID Num.' + IntToStr(i), CoResp.Companies[i].Vendor_ID);
              FLogger.LogContextItem('InfinityCompany_ID Num.' + IntToStr(i), CoResp.Companies[i].InfinityCompany_ID);
              FLogger.LogContextItem('VendorCompany_ID Num.' + IntToStr(i), CoResp.Companies[i].VendorCompany_ID);

              FileReq.VendorID := CoResp.Companies[i].Vendor_ID;
              try
                FService.UpsertEmployeePaystubs( FileReq );
                UploadSuccessfull := True;
              except
                on e: Exception do
                begin
                  FLogger.LogDebug('Vendor_ID: ' + FileReq.VendorID + ', paystub uploading failed', e.Message);
                  errMessage := e.Message;
                end;
              end;
              i := i + 1;
            end;
          finally
            FLogger.LogExit;
          end;
        finally
          FreeAndNil(CoReq);
        end
      end
      else begin
        FileReq.VendorID := FParam.UserName;
        FLogger.LogEvent('Upload paystub file (SSN: ' + aFile.SSN + ', Check Number: ' + aFile.CheckNumber + ')');
        try
          FService.UpsertEmployeePaystubs( FileReq );
          UploadSuccessfull := True;
        except
          on e: Exception do
          begin
            FLogger.LogDebug('Vendor_ID: ' + FileReq.VendorID + ', paystub uploading failed', e.Message);
            errMessage := e.Message;
          end;
        end;
      end;
      if not UploadSuccessfull then
        raise Exception.Create('Paystub uploading failed (SSN: ' + aFile.SSN + ', Check Number: ' + aFile.CheckNumber + ')! Error: ' + errMessage);
    finally
      EndConnection;
    end;
  end;
begin
  FileReq := PaystubRequest.Create;
  try
    // prepare param for the next ee paystubs batch
    SetLength(APaystubFiles, 0);

    sFile := PaystubFile.Create;
    sFile.PayPeriod_StartDate := TXSDateTime.Create;
    sFile.PayPeriod_StartDate.AsUTCDateTime := aFile.PayPeriod_StartDate;
//    sFile.PayPeriod_StartDate.AsUTCDateTime := LocalTimeToUTC( aFile.PayPeriod_StartDate );
//    sFile.PayPeriod_StartDate.AsDateTime := aFile.PayPeriod_StartDate;

    sFile.PayPeriod_EndDate := TXSDateTime.Create;
    sFile.PayPeriod_EndDate.AsUTCDateTime := aFile.PayPeriod_EndDate;
//    sFile.PayPeriod_EndDate.AsUTCDateTime := LocalTimeToUTC( aFile.PayPeriod_EndDate );
//    sFile.PayPeriod_EndDate.AsDateTime := aFile.PayPeriod_EndDate;

    sFile.PayDate := TXSDateTime.Create;
    sFile.PayDate.AsUTCDateTime := aFile.PayDate;
//    sFile.PayDate.AsUTCDateTime := LocalTimeToUTC( aFile.PayDate );
//    sFile.PayDate.AsDateTime := aFile.PayDate;

    sFile.CheckNumber := aFile.CheckNumber;

    sFile.DepositAmount := TXSDecimal.Create;
    sFile.DepositAmount.AsBcd := aFile.DepositAmount;

    sFile.FileName := aFile.FileName;
    sFile.FileExtension := '.pdf';
    sFile.Indicator := 'ON';

    FileStream := TStringStream.Create( aFile.FileContent );
//    testFS := TFileStream.Create('D:\iSystems\AccuRev\dev1\DelphiProjects\Bin\EvoInfinityHR\To Send\checkstub.pdf', fmCreate);
    try
      FileStream.Position := 0;
      SetLength( fContent, FileStream.Size );
      FileStream.ReadBuffer( fContent[0], FileStream.Size );

      sFile.FileLength := FileStream.Size;
      sFile.FileContent := fContent;
//      testFS.CopyFrom(FileStream, 0);
    finally
      FreeAndNil(FileStream);
      //FreeAndNil(testFS);
    end;

    SetLength( APaystubFiles, Length(APaystubFiles) + 1);
    APaystubFiles[High(APaystubFiles)] := sFile;

    UploadPaystubs;
  finally
    FileReq.Free;
  end;
end;

{ TStableInfinityHRConnection }

procedure TStableInfinityHRConnection.GetEmployeesByDateRange(dat_b,
  dat_e: TDateTime; EeData: TEvoInfinityMapping; FLogger: ICommonLogger;
  aEnterprise: boolean);
begin
  try
    inherited GetEmployeesByDateRange(dat_b, dat_e, EeData, FLogger, aEnterprise);
  except
    on e: Exception do
      if Pos('re-authenticate a new session', e.Message) > 0 then
      begin
        Login;
        inherited GetEmployeesByDateRange(dat_b, dat_e, EeData, FLogger, aEnterprise);
      end
      else
        FLogger.PassthroughException;
  end;
end;

procedure TStableInfinityHRConnection.UpsertEmployeePayStubs(
  aFile: TUploadFile; FLogger: ICommonLogger; aEnterprise: boolean);
begin
  try
    inherited UpsertEmployeePaystubs(aFile, FLogger, aEnterprise);
  except
    on e: Exception do
      if Pos('re-authenticate a new session', e.Message) > 0 then
      begin
        Login;
        inherited UpsertEmployeePaystubs(aFile, FLogger, aEnterprise);
      end
      else
        FLogger.PassthroughException;
  end;
end;

procedure TInfinityHRConnection.UpsertEmployeeW2(aFile: TUploadW2File;
  FLogger: ICommonLogger; aEnterprise: boolean);
var
  W2Req: W2Request;
  sFile: W2File;
  TimeOut: integer;
  FileStream: TStringStream;
  fContent: TByteDynArray;
  AW2Files: ArrayOfW2File;
  CoReq: CompanyRequest;
  CoResp: CompanyData;
//  testFS: TFileStream;

  procedure UploadW2Files;
  var
    i: integer;
    UploadSuccessfull: boolean;
  begin
    // send paystubs
    BeginConnection;
    try
      W2Req.SessionID := FSession;
      W2Req.SSN := aFile.SSN;
      W2Req.W2_Files := AW2Files;

      TimeOut := 1800000; // 30 min
      InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));
      InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@TimeOut), SizeOf(TimeOut));

      if aEnterprise then
      begin
        CoReq := CompanyRequest.Create;
        try
          CoReq.VendorID := FParam.UserName;
          CoReq.SessionID := FSession;
          CoResp := FService.GetEnterpriseCompanyInfo( CoReq );
          FLogger.LogEntry('Upload W2 file <Enterprise> (Employee: ' + aFile.EeCaption + ', TaxYear: ' + IntToStr(aFile.TaxYear));
          try
            UploadSuccessfull := False;
            i := Low(CoResp.Companies);
            while (i <= High(CoResp.Companies)) and not UploadSuccessfull do
            begin
              FLogger.LogContextItem('Vendor_ID Num.' + IntToStr(i), CoResp.Companies[i].Vendor_ID);
              FLogger.LogContextItem('InfinityCompany_ID Num.' + IntToStr(i), CoResp.Companies[i].InfinityCompany_ID);
              FLogger.LogContextItem('VendorCompany_ID Num.' + IntToStr(i), CoResp.Companies[i].VendorCompany_ID);

              W2Req.VendorID := CoResp.Companies[i].Vendor_ID;
              try
                FService.UploadEmployeeW2_BySSN( W2Req );
                UploadSuccessfull := True;
              except
                FLogger.LogDebug('Vendor_ID: ' + CoResp.Companies[i].Vendor_ID, 'W2 uploading failed');
              end;
              i := i + 1;
            end;
            if not UploadSuccessfull then
              raise Exception.Create('W2 uploading failed <Enterprise> (Employee: ' + aFile.EeCaption + ', TaxYear: ' + IntToStr(aFile.TaxYear));
          finally
            FLogger.LogExit;
            FreeAndNil(CoResp);
          end;
        finally
          FreeAndNil(CoReq);
        end
      end
      else begin
        FLogger.LogEvent('Upload W2 file (Employee: ' + aFile.EeCaption + ', TaxYear: ' + IntToStr(aFile.TaxYear));
        W2Req.VendorID := FParam.UserName;
        FService.UploadEmployeeW2_BySSN( W2Req );
      end;
    finally
      EndConnection;
    end;
  end;
begin
  W2Req := W2Request.Create;
  sFile := W2File.Create;
  try
    // prepare param for the next ee paystubs batch
    SetLength(AW2Files, 0);

    sFile.TaxYear := aFile.TaxYear;
    sFile.FileExtension := 'pdf';
    sFile.Notes := aFile.EeCaption;

    FileStream := TStringStream.Create( aFile.FileContent );
//    testFS := TFileStream.Create('C:\iSystems\AccuRev\dev1\DelphiProjects\Bin\ICP\EvoInfinityHR\w2ee191.pdf', fmCreate);
    try
      FileStream.Position := 0;
      SetLength( fContent, FileStream.Size );
      FileStream.ReadBuffer( fContent[0], FileStream.Size );

      sFile.FileLength := FileStream.Size;
      sFile.FileContent := fContent;
//      testFS.CopyFrom(FileStream, 0);
    finally
      FreeAndNil(FileStream);
//      FreeAndNil(testFS);
    end;

    SetLength( AW2Files, Length(AW2Files) + 1);
    AW2Files[High(AW2Files)] := sFile;

    UploadW2Files;
  finally
    FreeAndNil(sFile);
    FreeAndNil(W2Req);
  end;
end;

procedure TStableInfinityHRConnection.UpsertEmployeeW2(
  aFile: TUploadW2File; FLogger: ICommonLogger; aEnterprise: boolean);
begin
  try
    inherited UpsertEmployeeW2(aFile, FLogger, aEnterprise);
  except
    on e: Exception do
      if Pos('re-authenticate a new session', e.Message) > 0 then
      begin
        Login;
        inherited UpsertEmployeeW2(aFile, FLogger, aEnterprise);
      end
      else
        FLogger.PassthroughException;
  end;
end;

end.
