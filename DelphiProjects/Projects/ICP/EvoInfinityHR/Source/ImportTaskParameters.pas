unit ImportTaskParameters;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, DateRangeFrame, EvoInfinityHRConstAndProc,
  common, SimpleScheduler, gdyCommonLogger, jclTask;

type
  TfrmImportTaskParameters = class(TfrmDialog)
    gbDestination: TGroupBox;
    rbtnEvoInfiity: TRadioButton;
    rbtnInfinityEvo: TRadioButton;
    gbDatePeriod: TGroupBox;
    DateRange: TfrmDateRange;
    Scheduler: TfrmSimpleScheduler;
    gbReport: TGroupBox;
    chbSendEmail: TCheckBox;
    chbAttachLog: TCheckBox;
    chbElectedED: TCheckBox;
    chbTrasferHourlyRates: TCheckBox;
  private
    FParameter: string;
  protected
    procedure DoOK; override;
  end;

  function EditImportTask(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;

implementation

{$R *.dfm}

function EditImportTask(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;
var
  FSysScheduler: TJclTaskSchedule;
begin
  FSysScheduler := nil;
  with TfrmImportTaskParameters.Create( Application ) do
  try
    FParameter := Parameter;
    with GetTaskParam( FParameter ) do
    begin
      if Title = 'InfinityEvo' then
        rbtnInfinityEvo.Checked := True
      else
        rbtnEvoInfiity.Checked := True;
          
      DateRange.AsString := InputParameters;
      chbSendEmail.Checked := SendEmail;
      chbAttachLog.Checked := AttachLog;
      chbElectedED.Checked := ElectedED;
      chbTrasferHourlyRates.Checked := TransHRates;
    end;
    if Assigned(SysScheduler) then
      Scheduler.Init(Parameter, Logger, SysScheduler, Task)
    else begin
      if not TJclTaskSchedule.IsRunning then
        TJclTaskSchedule.Start;
      FSysScheduler := TJclTaskSchedule.Create;
      Scheduler.Init(Parameter, Logger, FSysScheduler, Task);
    end;

    Result := ShowModal = mrOk;
  finally
    if Assigned(FSysScheduler) then
      FreeAndNil(FSysScheduler);
    Free;
  end;
end;

{ TfrmImportTaskParameters }

procedure TfrmImportTaskParameters.DoOK;
var
  TaskParam: TTaskParam;
begin
  inherited;

  if Scheduler.Save then
  begin
    // create/update task directory
    TaskParam.Title := 'InfinityEvo';
    TaskParam.InputParameters := DateRange.AsString;
    TaskParam.SendEmail := chbSendEmail.Checked;
    TaskParam.AttachLog := chbAttachLog.Checked;
    TaskParam.ElectedED := chbElectedED.Checked;
    TaskParam.TransHRates := chbTrasferHourlyRates.Checked;
    SetTaskParam( FParameter, TaskParam );
    ModalResult := mrOk;
  end
  else begin
    ShowMessage('Please, schedule the task');
    ModalResult := mrNone;
  end;
end;

end.
