unit SimpleScheduler;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ActnList, jclTask, gdycommon,
  comobj, ExtCtrls, gdycommonlogger, EvoInfinityHRConstAndProc;

type
  TfrmSimpleScheduler = class(TFrame)
    gbSchedule: TGroupBox;
    lblScheduleInfo: TLabel;
    btnSchedule: TBitBtn;
    Actions: TActionList;
    Schedule: TAction;
    Bevel1: TBevel;
    procedure lblScheduleInfoDblClick(Sender: TObject);
    procedure ScheduleExecute(Sender: TObject);
  private
    FLogger: ICommonLogger;
    FSysScheduler: TJclTaskSchedule;
    FTask: TJclScheduledTask;
    FParameter: string;

    procedure Edit;
  public
    function Save: boolean;
    procedure ScheduleTask;
    procedure Init(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask);
  end;

implementation

{$R *.dfm}

{ TfrmSimpleScheduler }

procedure TfrmSimpleScheduler.Init(const Parameter: string; Logger: ICommonLogger;
  SysScheduler: TJclTaskSchedule; Task: TJclScheduledTask);
begin
  FParameter := Parameter;
  FLogger := Logger;
  FSysScheduler := SysScheduler;
  FTask := Task;

  if not Assigned( FTask) then
    lblScheduleInfo.Caption := '<no scheduled task>'
  else
    lblScheduleInfo.Caption := GetNextRunTime( FTask.NextRunTime );
end;

procedure TfrmSimpleScheduler.ScheduleTask;
var
  uname: widestring;
  i: integer;
begin
  if not Assigned(FTask) then
  for i := 0 to FSysScheduler.TaskCount-1 do
  if (WideCompareText(FSysScheduler.Tasks[i].ApplicationName, GetModuleName(0)) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Parameters, FParameter) = 0) and
    (WideCompareText(FSysScheduler.Tasks[i].Comment, Format('Please use "%s" to manage this task.', [Application.Title])) = 0) then
  begin
    FTask := FSysScheduler.Tasks[i];
    Break;
  end;

  if not Assigned( FTask) then
  begin
    Ftask := FSysScheduler.Add( 'EvoInfinityHR Task ' + FParameter );
    Ftask.ApplicationName := GetModuleName(0);
    Ftask.Parameters := FParameter;
    Ftask.Comment := Format('Please use "%s" to manage this task.', [Application.Title]);

    uname := GetUserName(NameSamCompatible);
    Ftask.Flags := Ftask.Flags + [tfRunOnlyIfLoggedOn]; //!! because password isn't specified
    OleCheck(Ftask.ScheduledWorkItem.SetAccountInformation(PWideChar(uname), nil));
  end;

  Edit;
end;

procedure TfrmSimpleScheduler.lblScheduleInfoDblClick(Sender: TObject);
begin
  ScheduleTask;
end;

procedure TfrmSimpleScheduler.ScheduleExecute(Sender: TObject);
begin
  ScheduleTask;
end;

procedure TfrmSimpleScheduler.Edit;
begin
  if Assigned(FTask) and FTask.ShowPage([ppSchedule], Application.Title, Application.MainForm.Handle) then
    lblScheduleInfo.Caption := GetNextRunTime( FTask.NextRunTime );
end;

function TfrmSimpleScheduler.Save: boolean;
begin
  Result := False;
  if Assigned(FTask) then
  begin
    FTask.Save;
    FTask.Refresh;
    Result := True;
  end;  
end;

end.

