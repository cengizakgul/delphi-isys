inherited frmImportTaskParameters: TfrmImportTaskParameters
  Left = 445
  Top = 192
  Caption = 'Import Employee Information Task'
  ClientHeight = 350
  ClientWidth = 395
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOk: TBitBtn
    Left = 231
    Top = 315
    ModalResult = 0
  end
  inherited btnCancel: TBitBtn
    Left = 313
    Top = 315
  end
  object gbDestination: TGroupBox
    Left = 6
    Top = 3
    Width = 383
    Height = 51
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Destination'
    TabOrder = 2
    object rbtnEvoInfiity: TRadioButton
      Left = 193
      Top = 19
      Width = 139
      Height = 17
      Caption = 'Evolution -> Infinity HR'
      Enabled = False
      TabOrder = 0
    end
    object rbtnInfinityEvo: TRadioButton
      Left = 11
      Top = 19
      Width = 137
      Height = 17
      Caption = 'Infinity HR -> Evolution'
      Checked = True
      TabOrder = 1
      TabStop = True
    end
  end
  object gbDatePeriod: TGroupBox
    Left = 6
    Top = 57
    Width = 383
    Height = 109
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Date Range'
    TabOrder = 3
    inline DateRange: TfrmDateRange
      Left = 5
      Top = 17
      Width = 358
      Height = 84
      TabOrder = 0
      inherited lblTo: TLabel
        Left = 182
      end
      inherited edDatB: TDateTimePicker
        Left = 2
        Top = 58
      end
      inherited edDatE: TDateTimePicker
        Left = 182
        Top = 58
      end
      inherited edTimeB: TDateTimePicker
        Left = 88
        Top = 58
      end
      inherited edTimeE: TDateTimePicker
        Left = 268
        Top = 58
      end
      inherited cbPeriod: TComboBox
        Left = 3
        Top = 9
        Width = 352
      end
    end
  end
  inline Scheduler: TfrmSimpleScheduler
    Left = 6
    Top = 194
    Width = 383
    Height = 61
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 4
    inherited gbSchedule: TGroupBox
      Width = 383
      Height = 61
      inherited lblScheduleInfo: TLabel
        Left = 102
        Top = 21
        Width = 273
        Height = 24
      end
      inherited Bevel1: TBevel
        Left = 97
        Top = 17
        Width = 269
        Height = 32
        Shape = bsLeftLine
      end
      inherited btnSchedule: TBitBtn
        Left = 8
        Top = 21
        Width = 82
        Caption = 'Schedule Task'
      end
    end
  end
  object gbReport: TGroupBox
    Left = 6
    Top = 260
    Width = 383
    Height = 46
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Report'
    TabOrder = 5
    object chbSendEmail: TCheckBox
      Left = 11
      Top = 18
      Width = 162
      Height = 17
      Caption = 'Send the report when done'
      TabOrder = 0
    end
    object chbAttachLog: TCheckBox
      Left = 176
      Top = 18
      Width = 125
      Height = 17
      Caption = 'Attach log files'
      TabOrder = 1
    end
  end
  object chbElectedED: TCheckBox
    Left = 8
    Top = 170
    Width = 175
    Height = 17
    Caption = 'Sync elected Sched. E/Ds only'
    Checked = True
    State = cbChecked
    TabOrder = 6
  end
  object chbTrasferHourlyRates: TCheckBox
    Left = 193
    Top = 170
    Width = 194
    Height = 17
    Caption = 'Transfer hourly rates for salaried EEs'
    TabOrder = 7
  end
end
