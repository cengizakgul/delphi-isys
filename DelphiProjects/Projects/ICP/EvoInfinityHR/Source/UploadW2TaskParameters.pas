unit UploadW2TaskParameters;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CommonDlg, StdCtrls, Buttons, DateRangeFrame, EvoInfinityHRConstAndProc,
  common, SimpleScheduler, gdyCommonLogger, jclTask, SelectEmployeeFrm;

type
  TfrmUploadW2TaskParameters = class(TfrmDialog)
    gbDestination: TGroupBox;
    gbDatePeriod: TGroupBox;
    Scheduler: TfrmSimpleScheduler;
    gbReport: TGroupBox;
    chbSendEmail: TCheckBox;
    chbAttachLog: TCheckBox;
    lblTaskCaption: TLabel;
    SelectEmployeeFrm: TSelecEmployeeFrame;
  private
    FParameter: string;
  protected
    procedure DoOK; override;
    procedure BeforeShowDialog; override;
  public  
    constructor Create(AOwner: TComponent); override;
  end;

  function EditUploadW2Task(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;

implementation

{$R *.dfm}

function EditUploadW2Task(const Parameter: string; Logger: ICommonLogger; SysScheduler: TJclTaskSchedule;
      Task: TJclScheduledTask = nil): boolean;
var
  FSysScheduler: TJclTaskSchedule;
begin
  FSysScheduler := nil;
  with TfrmUploadW2TaskParameters.Create( Application ) do
  try
    FParameter := Parameter;
    with GetTaskParam( FParameter ) do
    begin
      SelectEmployeeFrm.InitFrame(frmLogger.Logger, FSettings, sEvoConn, True);
      SelectEmployeeFrm.AsString := InputParameters;
      chbSendEmail.Checked := SendEmail;
      chbAttachLog.Checked := AttachLog;
    end;
    if Assigned(SysScheduler) then
      Scheduler.Init(Parameter, Logger, SysScheduler, Task)
    else begin
      if not TJclTaskSchedule.IsRunning then
        TJclTaskSchedule.Start;
      FSysScheduler := TJclTaskSchedule.Create;
      Scheduler.Init(Parameter, Logger, FSysScheduler, Task);
    end;

    Result := ShowModal = mrOk;
  finally
    if Assigned(FSysScheduler) then
      FreeAndNil(FSysScheduler);
    Free;
  end;
end;

{ TfrmUploadW2TaskParameters }

procedure TfrmUploadW2TaskParameters.BeforeShowDialog;
begin
  inherited;
  SelectEmployeeFrm.InitFrame(frmLogger.Logger, FSettings, sEvoConn);
end;

constructor TfrmUploadW2TaskParameters.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FSettings := AppSettings;
end;

procedure TfrmUploadW2TaskParameters.DoOK;
var
  TaskParam: TTaskParam;
begin
  inherited;

  if Scheduler.Save then
  begin
    // create/update task directory
    TaskParam.Title := 'UploadW2Paystubs';
    TaskParam.InputParameters := SelectEmployeeFrm.AsString;
    TaskParam.SendEmail := chbSendEmail.Checked;
    TaskParam.AttachLog := chbAttachLog.Checked;
    TaskParam.ElectedED := False;
    TaskParam.TransHRates := False;
    SetTaskParam( FParameter, TaskParam );
    ModalResult := mrOk;
  end
  else begin
    ShowMessage('Please, schedule the task');
    ModalResult := mrNone;
  end;
end;

end.
