unit Log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdySendMailLoggerView, gdyCommonLoggerView, gdyCommonLogger,
  gdyclasses, gdyLoggerImpl, isSettings, ExtCtrls, common, StdCtrls,
  Buttons, newsevenzip;

type
  TfrmLogger = class(TForm, ILogRecorder)
    pnlLogger: TPanel;
  private
    FLogOutput: TStringList;
    FLogOutputWriter: ILoggerEventSink;
    FLoggerFrame: TSendMailLoggerViewFrame;

    {ILogRecorder}
    procedure LogRecorder_Start(html: boolean);
    function LogRecorder_Stop: string;
    procedure ILogRecorder.Start = LogRecorder_Start;
    function ILogRecorder.Stop = LogRecorder_Stop;
    procedure LeaveRecentSessionsInDevLog(aCount: integer);
  protected
    FSettings: IisSettings;
    FLoggerFrameClass: TSendMailLoggerViewFrameClass;
    procedure SaveLogArchiveTo(filename: string);

    function AddDateTimeToDebuLogName(aDebugLogFileName: string): string;
  public
    function PackLogFiles(const folder: string; aUserLog, aDevLog: boolean): string;

    function Logger: ICommonLogger;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

uses
  gdyRedir, gdyLogWriters, gdycommon, dateutils, gdyGlobalWaitIndicator,
  printers, gdyWaitForm, EvoInfinityHRConstAndProc;

{ TfrmLogger }

function TfrmLogger.AddDateTimeToDebuLogName(
  aDebugLogFileName: string): string;
begin
  Result := Copy(aDebugLogFileName, 1, Length(aDebugLogFileName)-4) + '_' + FormatDateTime('yyyy-mm-dd-hhnn', Now) + '.log';
end;

constructor TfrmLogger.Create(Owner: TComponent);
begin
  if FLoggerFrameClass = nil then
    FLoggerFrameClass := TSendMailLoggerViewFrame;
  FLogOutput := TStringList.Create;
  inherited;
  FLoggerFrame := FLoggerFrameClass.Create( pnlLogger );
  FLoggerFrame.Parent := pnlLogger;
  FLoggerFrame.Align := alClient;

  FSettings := AppSettings;

  LeaveRecentSessionsInDevLog(9);

{$ifndef FINAL_RELEASE}
  try
    FLoggerFrame.LoggerKeeper.StartDevLoggingToFile( AddDateTimeToDebuLogName( Redirection.GetFilename(sDebugLogFileAlias) ) );
  except
    Logger.StopException;
  end;
{$endif}
  try
    FLoggerFrame.LoggerKeeper.StartLoggingToFile( Redirection.GetFilename(sLogFileAlias) );
  except
    Logger.StopException;
  end;
  FLoggerFrame.Password := GetISystemsPassword;
  FLoggerFrame.EMail := 'support@evolutionpayroll.com';//GetISystemsEmail;
  Logger.LogDebug('Caption: ' + Caption );
  Logger.LogDebug('Timestamp: ' + DateTimeToStr(Now) );

  LoadFormSize(Self, FSettings);
end;

destructor TfrmLogger.Destroy;
begin
  SaveFormSize(Self, FSettings);
  FLoggerFrame.LoggerKeeper.StopLoggingToFile;
{$ifndef FINAL_RELEASE}
  FLoggerFrame.LoggerKeeper.StopDevLoggingToFile;
{$endif}
  SetWaitIndicator(nil);
  inherited;
  FreeAndNil(FLogOutput);
end;

procedure TfrmLogger.LeaveRecentSessionsInDevLog(aCount: integer);
var
  sl: TStringList;
  i: integer;
begin
  sl := TStringList.Create;
  sl.Sorted := True;
  try
    GetDebugLogFiles(sl);

    for i := 0 to sl.Count - aCount - 1 do
      DeleteFile(sl[i]);
  finally
    sl.Free;
  end;
end;

function TfrmLogger.Logger: ICommonLogger;
begin
  Result := FLoggerFrame.LoggerKeeper.Logger;
end;

procedure TfrmLogger.LogRecorder_Start(html: boolean);
var
  output: ILogOutput;
  dir: string;
begin
  Assert(FLogOutputWriter = nil);
  FLogOutput.Clear;
  output := TStringsLogOutput.Create(FLogOutput);
  dir := Redirection.GetDirectory(sHtmlTemplatesDirAlias);
  if html then
    FLogOutputWriter := THtmlUserLogWriter.Create(output, dir, Application.Title + ' log' )
  else
    FLogOutputWriter := TPlainTextUserLogWriter.Create(output);
  FLoggerFrame.LoggerKeeper.StatefulLogger.Advise( FLogOutputWriter );
end;

function TfrmLogger.LogRecorder_Stop: string;
begin
  FLoggerFrame.LoggerKeeper.StatefulLogger.UnAdvise(FLogOutputWriter);
  FLogOutputWriter := nil;
  Result := FLogOutput.Text;
end;

function TfrmLogger.PackLogFiles(const folder: string; aUserLog,
  aDevLog: boolean): string;
var
  fDevLog: string;
  Arch: I7zOutArchive;
  base: string;
begin
  Result := '';

  fDevLog := '';
  if aDevLog then
  begin
    Assert(FLoggerFrame <> nil);
    fDevLog := FLoggerFrame.SaveArchiveTo(folder);
  end;

  if aUserLog or aDevLog then
  begin
    WaitIndicator.StartWait('Packing log files');
    try
      Arch := CreateOutArchive(CLSID_CFormat7z, Redirection.GetFilename(sSevenZipDllAlias));
      SetCompressionLevel(Arch, 5);

      if fDevLog <> '' then
        Arch.AddFile( fDevLog, ExtractFileName( fDevLog ));

      if aUserLog then
      begin
        if FLoggerFrame.LoggerKeeper.UserLogFileName <> '' then
        begin
          FLoggerFrame.LoggerKeeper.CloseUserLogFileTemporarily;
          Arch.AddFile(FLoggerFrame.LoggerKeeper.UserLogFileName, ExtractFileName(FLoggerFrame.LoggerKeeper.UserLogFileName) );
        end;
        FLoggerFrame.AddUserLogToArch( Arch );
      end;

      base := ChangeFileExt(GetAppFilename,'') + '-task-log-';
      Result := WithTrailingSlash(folder) + base + FormatDateTime('yyyy-mm-dd-hhnn', now) + '.7z';
      ForceDirectories(folder);
      Arch.SaveToFile(Result);
    finally
      if fDevLog <> '' then
        DeleteFile( fDevLog );

      WaitIndicator.EndWait;
    end;
  end;  
end;

procedure TfrmLogger.SaveLogArchiveTo(filename: string);
begin
  Assert(FLoggerFrame <> nil);
  FLoggerFrame.SaveArchiveTo(filename);
end;

end.

