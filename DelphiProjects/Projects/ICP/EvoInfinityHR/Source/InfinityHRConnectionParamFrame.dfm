object InfinityHRConnectionParamFrm: TInfinityHRConnectionParamFrm
  Left = 0
  Top = 0
  Width = 400
  Height = 105
  TabOrder = 0
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 400
    Height = 105
    Align = alClient
    Caption = 'Infinity HR'
    TabOrder = 0
    object Label4: TLabel
      Left = 14
      Top = 21
      Width = 48
      Height = 13
      Caption = 'Vendor ID'
    end
    object Label7: TLabel
      Left = 14
      Top = 45
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object Label3: TLabel
      Left = 14
      Top = 69
      Width = 22
      Height = 13
      Caption = 'URL'
    end
    object SiteEdit: TEdit
      Left = 82
      Top = 69
      Width = 206
      Height = 21
      TabOrder = 3
    end
    object PasswordEdit: TPasswordEdit
      Left = 82
      Top = 45
      Width = 206
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object UserNameEdit: TEdit
      Left = 82
      Top = 20
      Width = 206
      Height = 21
      TabOrder = 0
    end
    object cbSavePassword: TCheckBox
      Left = 297
      Top = 47
      Width = 97
      Height = 17
      Caption = 'Save password'
      TabOrder = 2
    end
    object cbEnterprise: TCheckBox
      Left = 297
      Top = 22
      Width = 97
      Height = 17
      Caption = 'Enterprise'
      TabOrder = 4
    end
  end
end
