inherited frmUploadW2TaskParameters: TfrmUploadW2TaskParameters
  Left = 665
  Top = 189
  Caption = 'Upload Employee W2 Task'
  ClientHeight = 333
  ClientWidth = 384
  PixelsPerInch = 96
  TextHeight = 13
  inherited btnOk: TBitBtn
    Left = 220
    Top = 298
    ModalResult = 0
  end
  inherited btnCancel: TBitBtn
    Left = 302
    Top = 298
  end
  object gbDestination: TGroupBox
    Left = 6
    Top = 3
    Width = 372
    Height = 51
    Caption = 'Task'
    TabOrder = 2
    object lblTaskCaption: TLabel
      Left = 16
      Top = 23
      Width = 103
      Height = 13
      Caption = 'Upload Employee W2'
    end
  end
  object gbDatePeriod: TGroupBox
    Left = 6
    Top = 60
    Width = 372
    Height = 109
    Caption = 'Employees'
    TabOrder = 3
    inline SelectEmployeeFrm: TSelecEmployeeFrame
      Left = 6
      Top = 15
      Width = 362
      Height = 87
      TabOrder = 0
      inherited pnlButtons: TPanel
        Left = 309
        Height = 87
      end
      inherited lbSelected: TListBox
        Width = 309
        Height = 87
      end
    end
  end
  inline Scheduler: TfrmSimpleScheduler
    Left = 6
    Top = 176
    Width = 372
    Height = 61
    TabOrder = 4
    inherited gbSchedule: TGroupBox
      Width = 372
      Height = 61
      inherited lblScheduleInfo: TLabel
        Left = 102
        Top = 21
        Width = 262
        Height = 24
      end
      inherited Bevel1: TBevel
        Left = 97
        Top = 17
        Width = 269
        Height = 32
        Shape = bsLeftLine
      end
      inherited btnSchedule: TBitBtn
        Left = 8
        Top = 21
        Width = 82
        Caption = 'Schedule Task'
      end
    end
  end
  object gbReport: TGroupBox
    Left = 6
    Top = 244
    Width = 372
    Height = 46
    Caption = 'Report'
    TabOrder = 5
    object chbSendEmail: TCheckBox
      Left = 11
      Top = 18
      Width = 162
      Height = 17
      Caption = 'Send the report when done'
      TabOrder = 0
    end
    object chbAttachLog: TCheckBox
      Left = 176
      Top = 18
      Width = 125
      Height = 17
      Caption = 'Attach log files'
      TabOrder = 1
    end
  end
end
