object Form1: TForm1
  Left = 118
  Top = 198
  BorderStyle = bsDialog
  Caption = 'Form1'
  ClientHeight = 343
  ClientWidth = 1380
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnAuth: TButton
    Left = 8
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Authenticate'
    TabOrder = 0
    OnClick = btnAuthClick
  end
  object edVendorID: TEdit
    Left = 8
    Top = 8
    Width = 313
    Height = 21
    TabOrder = 1
    Text = '049607e4-644c-484e-9127-3b74b3563b34'
  end
  object edPassword: TEdit
    Left = 8
    Top = 40
    Width = 313
    Height = 21
    TabOrder = 2
    Text = 'summit.2011'
  end
  object edSessionID: TEdit
    Left = 8
    Top = 128
    Width = 313
    Height = 21
    TabOrder = 3
    Text = 'SessionID'
  end
  object mmDetail: TMemo
    Left = 8
    Top = 160
    Width = 313
    Height = 169
    ScrollBars = ssBoth
    TabOrder = 4
  end
  object Panel1: TPanel
    Left = 328
    Top = 8
    Width = 417
    Height = 321
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    object edMethodName: TEdit
      Left = 24
      Top = 16
      Width = 377
      Height = 21
      TabOrder = 0
    end
    object mmSOAPRequest: TMemo
      Left = 24
      Top = 56
      Width = 377
      Height = 121
      TabOrder = 1
    end
    object mmSOAPResponse: TMemo
      Left = 24
      Top = 184
      Width = 377
      Height = 129
      TabOrder = 2
    end
  end
  object btnGetEmployees: TButton
    Left = 96
    Top = 80
    Width = 153
    Height = 25
    Caption = 'GetEmplyeesByDateRange'
    TabOrder = 6
    OnClick = btnGetEmployeesClick
  end
  object Button1: TButton
    Left = 760
    Top = 8
    Width = 137
    Height = 25
    Caption = 'Base 64 Save as pdf'
    TabOrder = 7
    OnClick = Button1Click
  end
  object mBase64Pdf: TMemo
    Left = 760
    Top = 40
    Width = 609
    Height = 289
    Lines.Strings = (
      'mBase64Pdf')
    TabOrder = 8
  end
  object HTTPRIO: THTTPRIO
    OnBeforeExecute = HTTPRIOBeforeExecute
    WSDLLocation = 'D:\ICP\EvoInfinityHR\wsdl\PayrollSync.wsdl'
    Service = 'PayrollSync'
    Port = 'PayrollSyncSoap'
    HTTPWebNode.Agent = 'Borland SOAP 1.2'
    HTTPWebNode.UseUTF8InHeader = False
    HTTPWebNode.InvokeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse]
    Left = 128
    Top = 72
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.pdf'
    Filter = '*.pdf|*.pdf'
    Left = 936
    Top = 8
  end
end
