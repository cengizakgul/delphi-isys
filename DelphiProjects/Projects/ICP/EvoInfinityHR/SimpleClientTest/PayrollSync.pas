// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : https://www.infinityhrconnect.com/test/services/payrollsync.asmx?wsdl
// Encoding : utf-8
// Version  : 1.0
// (4/16/2013 10:51:42 AM - 1.33.2.5)
// ************************************************************************ //

unit payrollsync1;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"
  // !:double          - "http://www.w3.org/2001/XMLSchema"
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"

  AuthRequest          = class;                 { "http://www.infinity-ss.com/services" }
  AuthResponse         = class;                 { "http://www.infinity-ss.com/services" }
  EmployeeRequest      = class;                 { "http://www.infinity-ss.com/services" }
  Deduction            = class;                 { "http://www.infinity-ss.com/services" }
  Compensation         = class;                 { "http://www.infinity-ss.com/services" }
  DirectDeposit        = class;                 { "http://www.infinity-ss.com/services" }
  StateTax             = class;                 { "http://www.infinity-ss.com/services" }
  FederalTax           = class;                 { "http://www.infinity-ss.com/services" }
  EmployeeInfo         = class;                 { "http://www.infinity-ss.com/services" }
  EmployeeData         = class;                 { "http://www.infinity-ss.com/services" }
  PaystubFile          = class;                 { "http://www.infinity-ss.com/services" }
  PaystubRequest       = class;                 { "http://www.infinity-ss.com/services" }
  Paystub_TimeOff      = class;                 { "http://www.infinity-ss.com/services" }
  Paystub_Tax          = class;                 { "http://www.infinity-ss.com/services" }
  Paystub_Summary      = class;                 { "http://www.infinity-ss.com/services" }
  Paystub_Detail       = class;                 { "http://www.infinity-ss.com/services" }
  PaystubData          = class;                 { "http://www.infinity-ss.com/services" }
  PaystubDataRequest   = class;                 { "http://www.infinity-ss.com/services" }
  AccrualBalance       = class;                 { "http://www.infinity-ss.com/services" }
  TimeOffBalanceRequest = class;                { "http://www.infinity-ss.com/services" }
  W2File               = class;                 { "http://www.infinity-ss.com/services" }
  W2Request            = class;                 { "http://www.infinity-ss.com/services" }
  BenefitRequest       = class;                 { "http://www.infinity-ss.com/services" }
  BenefitStructureItem = class;                 { "http://www.infinity-ss.com/services" }
  BenefitStructure     = class;                 { "http://www.infinity-ss.com/services" }
  Lookup               = class;                 { "http://www.infinity-ss.com/services" }
  LookupData           = class;                 { "http://www.infinity-ss.com/services" }
  PaySchedule          = class;                 { "http://www.infinity-ss.com/services" }
  PayScheduleData      = class;                 { "http://www.infinity-ss.com/services" }
  CompanyRequest       = class;                 { "http://www.infinity-ss.com/services" }
  CompanyInfo          = class;                 { "http://www.infinity-ss.com/services" }
  CompanyData          = class;                 { "http://www.infinity-ss.com/services" }
  PayrollServiceException = class;              { "http://www.infinity-ss.com/services" }
  AuditRequest         = class;                 { "http://www.infinity-ss.com/services" }
  ErrorRequest         = class;                 { "http://www.infinity-ss.com/services" }



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AuthRequest = class(TRemotable)
  private
    FVendorID: WideString;
    FVendorPwd: WideString;
  published
    property VendorID: WideString read FVendorID write FVendorID;
    property VendorPwd: WideString read FVendorPwd write FVendorPwd;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AuthResponse = class(TRemotable)
  private
    FSessionID: WideString;
    FIsVendorActive: Boolean;
    FSessionStart: TXSDateTime;
    FSessionEnd: TXSDateTime;
    FSyncDate: TXSDateTime;
  public
    destructor Destroy; override;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property IsVendorActive: Boolean read FIsVendorActive write FIsVendorActive;
    property SessionStart: TXSDateTime read FSessionStart write FSessionStart;
    property SessionEnd: TXSDateTime read FSessionEnd write FSessionEnd;
    property SyncDate: TXSDateTime read FSyncDate write FSyncDate;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
    FShowAllDeductions: Boolean;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property ShowAllDeductions: Boolean read FShowAllDeductions write FShowAllDeductions;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Deduction = class(TRemotable)
  private
    FID: Integer;
    FEffectiveDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
    FPremiumAmount: TXSDecimal;
    FBenefitType: WideString;
    FEmployeeCost: TXSDecimal;
    FEmployerCost: TXSDecimal;
    FCreditAmount: TXSDecimal;
    FCoverageAmount: TXSDecimal;
    FPreTaxDeduction: TXSDecimal;
    FPostTaxDeduction: TXSDecimal;
    FOptionName: WideString;
    FPlanID: Integer;
    FPlanName: WideString;
    FBenefitName: WideString;
    FPackageName: WideString;
    FPerPayEmployeeCost: TXSDecimal;
    FPerPayEmployerCost: TXSDecimal;
    FAnnualPremiumAmount: TXSDecimal;
    FAnnualCreditAmount: TXSDecimal;
    FAnnualEmployeeCost: TXSDecimal;
    FDeductionCode: WideString;
    FDeductionCodeAlt1: WideString;
    FDeductionCodeAlt2: WideString;
    FCarrierCode: WideString;
    FCarrierPlanCode: WideString;
    FEDCode: WideString;
    FDeductionEffectiveDate: TXSDateTime;
    FDeductionExpirationDate: TXSDateTime;
    FBenefitCategory: WideString;
    FCoverageInputType: WideString;
  public
    destructor Destroy; override;
  published
    property ID: Integer read FID write FID;
    property EffectiveDate: TXSDateTime read FEffectiveDate write FEffectiveDate;
    property ExpirationDate: TXSDateTime read FExpirationDate write FExpirationDate;
    property PremiumAmount: TXSDecimal read FPremiumAmount write FPremiumAmount;
    property BenefitType: WideString read FBenefitType write FBenefitType;
    property EmployeeCost: TXSDecimal read FEmployeeCost write FEmployeeCost;
    property EmployerCost: TXSDecimal read FEmployerCost write FEmployerCost;
    property CreditAmount: TXSDecimal read FCreditAmount write FCreditAmount;
    property CoverageAmount: TXSDecimal read FCoverageAmount write FCoverageAmount;
    property PreTaxDeduction: TXSDecimal read FPreTaxDeduction write FPreTaxDeduction;
    property PostTaxDeduction: TXSDecimal read FPostTaxDeduction write FPostTaxDeduction;
    property OptionName: WideString read FOptionName write FOptionName;
    property PlanID: Integer read FPlanID write FPlanID;
    property PlanName: WideString read FPlanName write FPlanName;
    property BenefitName: WideString read FBenefitName write FBenefitName;
    property PackageName: WideString read FPackageName write FPackageName;
    property PerPayEmployeeCost: TXSDecimal read FPerPayEmployeeCost write FPerPayEmployeeCost;
    property PerPayEmployerCost: TXSDecimal read FPerPayEmployerCost write FPerPayEmployerCost;
    property AnnualPremiumAmount: TXSDecimal read FAnnualPremiumAmount write FAnnualPremiumAmount;
    property AnnualCreditAmount: TXSDecimal read FAnnualCreditAmount write FAnnualCreditAmount;
    property AnnualEmployeeCost: TXSDecimal read FAnnualEmployeeCost write FAnnualEmployeeCost;
    property DeductionCode: WideString read FDeductionCode write FDeductionCode;
    property DeductionCodeAlt1: WideString read FDeductionCodeAlt1 write FDeductionCodeAlt1;
    property DeductionCodeAlt2: WideString read FDeductionCodeAlt2 write FDeductionCodeAlt2;
    property CarrierCode: WideString read FCarrierCode write FCarrierCode;
    property CarrierPlanCode: WideString read FCarrierPlanCode write FCarrierPlanCode;
    property EDCode: WideString read FEDCode write FEDCode;
    property DeductionEffectiveDate: TXSDateTime read FDeductionEffectiveDate write FDeductionEffectiveDate;
    property DeductionExpirationDate: TXSDateTime read FDeductionExpirationDate write FDeductionExpirationDate;
    property BenefitCategory: WideString read FBenefitCategory write FBenefitCategory;
    property CoverageInputType: WideString read FCoverageInputType write FCoverageInputType;
  end;

  ArrayOfDeduction = array of Deduction;        { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Compensation = class(TRemotable)
  private
    FEffectiveDate: TXSDateTime;
    FRate: TXSDecimal;
    FHoursWorked: TXSDecimal;
    FAnnualRate: TXSDecimal;
    FHourlyRate: TXSDecimal;
    FOverTimeRate: TXSDecimal;
    FOtherRate: TXSDecimal;
    FNotes: WideString;
    FLastModifiedDate: TXSDateTime;
    FPayPeriodAmount: TXSDecimal;
    FRateType: WideString;
    FExpirationDate: TXSDateTime;
    FAutoPay: Boolean;
    FAutoPayHours: Double;
  public
    destructor Destroy; override;
  published
    property EffectiveDate: TXSDateTime read FEffectiveDate write FEffectiveDate;
    property Rate: TXSDecimal read FRate write FRate;
    property HoursWorked: TXSDecimal read FHoursWorked write FHoursWorked;
    property AnnualRate: TXSDecimal read FAnnualRate write FAnnualRate;
    property HourlyRate: TXSDecimal read FHourlyRate write FHourlyRate;
    property OverTimeRate: TXSDecimal read FOverTimeRate write FOverTimeRate;
    property OtherRate: TXSDecimal read FOtherRate write FOtherRate;
    property Notes: WideString read FNotes write FNotes;
    property LastModifiedDate: TXSDateTime read FLastModifiedDate write FLastModifiedDate;
    property PayPeriodAmount: TXSDecimal read FPayPeriodAmount write FPayPeriodAmount;
    property RateType: WideString read FRateType write FRateType;
    property ExpirationDate: TXSDateTime read FExpirationDate write FExpirationDate;
    property AutoPay: Boolean read FAutoPay write FAutoPay;
    property AutoPayHours: Double read FAutoPayHours write FAutoPayHours;
  end;

  ArrayOfCompensation = array of Compensation;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  DirectDeposit = class(TRemotable)
  private
    FID: Integer;
    FDirectDepositTransitNumber: WideString;
    FDirectDepositAccountNumber: WideString;
    FBankName: WideString;
    FAccountType: WideString;
    FDepositAmount: TXSDecimal;
    FDepositPercent: TXSDecimal;
    FIsActive: Boolean;
    FModifiedDate: TXSDateTime;
    FEDCode: WideString;
    FEffectiveDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
    FOverridePreNote: Boolean;
  public
    destructor Destroy; override;
  published
    property ID: Integer read FID write FID;
    property DirectDepositTransitNumber: WideString read FDirectDepositTransitNumber write FDirectDepositTransitNumber;
    property DirectDepositAccountNumber: WideString read FDirectDepositAccountNumber write FDirectDepositAccountNumber;
    property BankName: WideString read FBankName write FBankName;
    property AccountType: WideString read FAccountType write FAccountType;
    property DepositAmount: TXSDecimal read FDepositAmount write FDepositAmount;
    property DepositPercent: TXSDecimal read FDepositPercent write FDepositPercent;
    property IsActive: Boolean read FIsActive write FIsActive;
    property ModifiedDate: TXSDateTime read FModifiedDate write FModifiedDate;
    property EDCode: WideString read FEDCode write FEDCode;
    property EffectiveDate: TXSDateTime read FEffectiveDate write FEffectiveDate;
    property ExpirationDate: TXSDateTime read FExpirationDate write FExpirationDate;
    property OverridePreNote: Boolean read FOverridePreNote write FOverridePreNote;
  end;

  ArrayOfDirectDeposit = array of DirectDeposit;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  StateTax = class(TRemotable)
  private
    FTaxYear: Integer;
    FFilingStatus: WideString;
    FIncomeFilingState: WideString;
    FUnemploymentState: WideString;
    FAllowances: Integer;
    FAdditionalWithholding: TXSDecimal;
    FPercentWithholding: TXSDecimal;
    FIsExempt: Boolean;
    FEffectiveDate: TXSDateTime;
    FIsActive: Boolean;
    FOverrideAmount: TXSDecimal;
    FOverridePercent: TXSDecimal;
    FCounty: WideString;
    FClaimAmount: WideString;
    FAdditionalPercentWithheld: TXSDecimal;
    FStartDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
  public
    destructor Destroy; override;
  published
    property TaxYear: Integer read FTaxYear write FTaxYear;
    property FilingStatus: WideString read FFilingStatus write FFilingStatus;
    property IncomeFilingState: WideString read FIncomeFilingState write FIncomeFilingState;
    property UnemploymentState: WideString read FUnemploymentState write FUnemploymentState;
    property Allowances: Integer read FAllowances write FAllowances;
    property AdditionalWithholding: TXSDecimal read FAdditionalWithholding write FAdditionalWithholding;
    property PercentWithholding: TXSDecimal read FPercentWithholding write FPercentWithholding;
    property IsExempt: Boolean read FIsExempt write FIsExempt;
    property EffectiveDate: TXSDateTime read FEffectiveDate write FEffectiveDate;
    property IsActive: Boolean read FIsActive write FIsActive;
    property OverrideAmount: TXSDecimal read FOverrideAmount write FOverrideAmount;
    property OverridePercent: TXSDecimal read FOverridePercent write FOverridePercent;
    property County: WideString read FCounty write FCounty;
    property ClaimAmount: WideString read FClaimAmount write FClaimAmount;
    property AdditionalPercentWithheld: TXSDecimal read FAdditionalPercentWithheld write FAdditionalPercentWithheld;
    property StartDate: TXSDateTime read FStartDate write FStartDate;
    property ExpirationDate: TXSDateTime read FExpirationDate write FExpirationDate;
  end;

  ArrayOfStateTax = array of StateTax;          { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  FederalTax = class(TRemotable)
  private
    FTaxYear: Integer;
    FAllowances: Integer;
    FAdditionalWithholding: TXSDecimal;
    FMaritalStatus: WideString;
    FExempt: Boolean;
    FLastNameDiffers: Boolean;
    FEffectiveDate: TXSDateTime;
    FOverrideAmount: TXSDecimal;
    FOverridePercent: TXSDecimal;
    FStartDate: TXSDateTime;
    FExpirationDate: TXSDateTime;
  public
    destructor Destroy; override;
  published
    property TaxYear: Integer read FTaxYear write FTaxYear;
    property Allowances: Integer read FAllowances write FAllowances;
    property AdditionalWithholding: TXSDecimal read FAdditionalWithholding write FAdditionalWithholding;
    property MaritalStatus: WideString read FMaritalStatus write FMaritalStatus;
    property Exempt: Boolean read FExempt write FExempt;
    property LastNameDiffers: Boolean read FLastNameDiffers write FLastNameDiffers;
    property EffectiveDate: TXSDateTime read FEffectiveDate write FEffectiveDate;
    property OverrideAmount: TXSDecimal read FOverrideAmount write FOverrideAmount;
    property OverridePercent: TXSDecimal read FOverridePercent write FOverridePercent;
    property StartDate: TXSDateTime read FStartDate write FStartDate;
    property ExpirationDate: TXSDateTime read FExpirationDate write FExpirationDate;
  end;

  ArrayOfFederalTax = array of FederalTax;      { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeInfo = class(TRemotable)
  private
    FInfinityHREmployeeID: Integer;
    FPayrollEmployeeID: WideString;
    FCompanyID: Integer;
    FVendorCompany_ID: WideString;
    FLastName: WideString;
    FFirstName: WideString;
    FMiddleName: WideString;
    FUserName: WideString;
    FGender: WideString;
    FTobaccoUser: Boolean;
    FEthnicity: WideString;
    FOccupation: WideString;
    FMaritalStatus: WideString;
    FAddress1: WideString;
    FAddress2: WideString;
    FCity: WideString;
    FHomePhone: WideString;
    FState: WideString;
    FZip: WideString;
    FSSN: WideString;
    FBirthDate: TXSDateTime;
    FAutoPay: Boolean;
    FCostCenter1: WideString;
    FCostCenter2: WideString;
    FCostCenter3: WideString;
    FCostCenter4: WideString;
    FCostCenter5: WideString;
    FDefaultHours: TXSDecimal;
    FEmployeeStatus: WideString;
    FBenefitStatus: WideString;
    FDepartment: WideString;
    FHireDate: TXSDateTime;
    FOrigHireDate: TXSDateTime;
    FAlternateServiceDate1: TXSDateTime;
    FAlternateServiceDate2: TXSDateTime;
    FRetirementDate: TXSDateTime;
    FWorkPhone: WideString;
    FWorkFax: WideString;
    FMobilePhone: WideString;
    FHoursWorked: Double;
    FPayFrequency: WideString;
    FSalary: TXSDecimal;
    FTaxForm: WideString;
    FWorkersCompCode: WideString;
    FEmployeeType: WideString;
    FReHireDate: TXSDateTime;
    FTerminationDate: TXSDateTime;
    FJobCode: WideString;
    FTerminationReason: WideString;
    FFacility1: WideString;
    FFacility2: WideString;
    FManager: Boolean;
    FPTOManager: WideString;
    FPAManager: WideString;
    FPaySchedule_Master: WideString;
    FEmployee_UserDefined1: WideString;
    FEmployee_UserDefined2: WideString;
    FEmployee_UserDefined3: WideString;
    FEmployee_UserDefined4: WideString;
    FEmployee_UserDefined5: WideString;
    FSchedule: WideString;
    FPWD: WideString;
    FBaseRate: WideString;
    FPension: WideString;
    FHighCompensation: WideString;
    FIsOwner: WideString;
    FUserDefinedLookup1: WideString;
    FUserDefinedLookup2: WideString;
    FUserDefinedLookup3: WideString;
    FUserDefinedLookup4: WideString;
    FUserDefinedLookup5: WideString;
    FUserDefinedLookup6: WideString;
    FUserDefinedLookup7: WideString;
    FUserDefinedLookup8: WideString;
    FUserDefinedLookup9: WideString;
    FUserDefinedLookup10: WideString;
    FUserDefinedLookup11: WideString;
    FUserDefinedLookup12: WideString;
    FUserDefinedLookup13: WideString;
    FUserDefinedLookup14: WideString;
    FUserDefinedLookup15: WideString;
    FWorkAddress1: WideString;
    FWorkAddress2: WideString;
    FWorkEmail: WideString;
    FHomeEmail: WideString;
    FBenefitServiceDate: TXSDateTime;
    FEmpStatusEffDate: TXSDateTime;
    FCompensationCategory: WideString;
    FHourlyRate: Double;
    FEEOClass: WideString;
    FEEOClassDescription: WideString;
    FOTExempt: Boolean;
    FVisaExpiration: TXSDateTime;
    FVisaType: WideString;
    FIsVeteran: Boolean;
    FNickName: WideString;
    FDeductions: ArrayOfDeduction;
    FCompensations: ArrayOfCompensation;
    FDirectDeposits: ArrayOfDirectDeposit;
    FStateTax: ArrayOfStateTax;
    FFederalTax: ArrayOfFederalTax;
    FHandicapped: Boolean;
    FFullTimeStudent: Boolean;
    FPartTimeStudent: Boolean;
    FShowAllDeductions: Boolean;
    FPayScheduleName: WideString;
  public
    destructor Destroy; override;
  published
    property InfinityHREmployeeID: Integer read FInfinityHREmployeeID write FInfinityHREmployeeID;
    property PayrollEmployeeID: WideString read FPayrollEmployeeID write FPayrollEmployeeID;
    property CompanyID: Integer read FCompanyID write FCompanyID;
    property VendorCompany_ID: WideString read FVendorCompany_ID write FVendorCompany_ID;
    property LastName: WideString read FLastName write FLastName;
    property FirstName: WideString read FFirstName write FFirstName;
    property MiddleName: WideString read FMiddleName write FMiddleName;
    property UserName: WideString read FUserName write FUserName;
    property Gender: WideString read FGender write FGender;
    property TobaccoUser: Boolean read FTobaccoUser write FTobaccoUser;
    property Ethnicity: WideString read FEthnicity write FEthnicity;
    property Occupation: WideString read FOccupation write FOccupation;
    property MaritalStatus: WideString read FMaritalStatus write FMaritalStatus;
    property Address1: WideString read FAddress1 write FAddress1;
    property Address2: WideString read FAddress2 write FAddress2;
    property City: WideString read FCity write FCity;
    property HomePhone: WideString read FHomePhone write FHomePhone;
    property State: WideString read FState write FState;
    property Zip: WideString read FZip write FZip;
    property SSN: WideString read FSSN write FSSN;
    property BirthDate: TXSDateTime read FBirthDate write FBirthDate;
    property AutoPay: Boolean read FAutoPay write FAutoPay;
    property CostCenter1: WideString read FCostCenter1 write FCostCenter1;
    property CostCenter2: WideString read FCostCenter2 write FCostCenter2;
    property CostCenter3: WideString read FCostCenter3 write FCostCenter3;
    property CostCenter4: WideString read FCostCenter4 write FCostCenter4;
    property CostCenter5: WideString read FCostCenter5 write FCostCenter5;
    property DefaultHours: TXSDecimal read FDefaultHours write FDefaultHours;
    property EmployeeStatus: WideString read FEmployeeStatus write FEmployeeStatus;
    property BenefitStatus: WideString read FBenefitStatus write FBenefitStatus;
    property Department: WideString read FDepartment write FDepartment;
    property HireDate: TXSDateTime read FHireDate write FHireDate;
    property OrigHireDate: TXSDateTime read FOrigHireDate write FOrigHireDate;
    property AlternateServiceDate1: TXSDateTime read FAlternateServiceDate1 write FAlternateServiceDate1;
    property AlternateServiceDate2: TXSDateTime read FAlternateServiceDate2 write FAlternateServiceDate2;
    property RetirementDate: TXSDateTime read FRetirementDate write FRetirementDate;
    property WorkPhone: WideString read FWorkPhone write FWorkPhone;
    property WorkFax: WideString read FWorkFax write FWorkFax;
    property MobilePhone: WideString read FMobilePhone write FMobilePhone;
    property HoursWorked: Double read FHoursWorked write FHoursWorked;
    property PayFrequency: WideString read FPayFrequency write FPayFrequency;
    property Salary: TXSDecimal read FSalary write FSalary;
    property TaxForm: WideString read FTaxForm write FTaxForm;
    property WorkersCompCode: WideString read FWorkersCompCode write FWorkersCompCode;
    property EmployeeType: WideString read FEmployeeType write FEmployeeType;
    property ReHireDate: TXSDateTime read FReHireDate write FReHireDate;
    property TerminationDate: TXSDateTime read FTerminationDate write FTerminationDate;
    property JobCode: WideString read FJobCode write FJobCode;
    property TerminationReason: WideString read FTerminationReason write FTerminationReason;
    property Facility1: WideString read FFacility1 write FFacility1;
    property Facility2: WideString read FFacility2 write FFacility2;
    property Manager: Boolean read FManager write FManager;
    property PTOManager: WideString read FPTOManager write FPTOManager;
    property PAManager: WideString read FPAManager write FPAManager;
    property PaySchedule_Master: WideString read FPaySchedule_Master write FPaySchedule_Master;
    property Employee_UserDefined1: WideString read FEmployee_UserDefined1 write FEmployee_UserDefined1;
    property Employee_UserDefined2: WideString read FEmployee_UserDefined2 write FEmployee_UserDefined2;
    property Employee_UserDefined3: WideString read FEmployee_UserDefined3 write FEmployee_UserDefined3;
    property Employee_UserDefined4: WideString read FEmployee_UserDefined4 write FEmployee_UserDefined4;
    property Employee_UserDefined5: WideString read FEmployee_UserDefined5 write FEmployee_UserDefined5;
    property Schedule: WideString read FSchedule write FSchedule;
    property PWD: WideString read FPWD write FPWD;
    property BaseRate: WideString read FBaseRate write FBaseRate;
    property Pension: WideString read FPension write FPension;
    property HighCompensation: WideString read FHighCompensation write FHighCompensation;
    property IsOwner: WideString read FIsOwner write FIsOwner;
    property UserDefinedLookup1: WideString read FUserDefinedLookup1 write FUserDefinedLookup1;
    property UserDefinedLookup2: WideString read FUserDefinedLookup2 write FUserDefinedLookup2;
    property UserDefinedLookup3: WideString read FUserDefinedLookup3 write FUserDefinedLookup3;
    property UserDefinedLookup4: WideString read FUserDefinedLookup4 write FUserDefinedLookup4;
    property UserDefinedLookup5: WideString read FUserDefinedLookup5 write FUserDefinedLookup5;
    property UserDefinedLookup6: WideString read FUserDefinedLookup6 write FUserDefinedLookup6;
    property UserDefinedLookup7: WideString read FUserDefinedLookup7 write FUserDefinedLookup7;
    property UserDefinedLookup8: WideString read FUserDefinedLookup8 write FUserDefinedLookup8;
    property UserDefinedLookup9: WideString read FUserDefinedLookup9 write FUserDefinedLookup9;
    property UserDefinedLookup10: WideString read FUserDefinedLookup10 write FUserDefinedLookup10;
    property UserDefinedLookup11: WideString read FUserDefinedLookup11 write FUserDefinedLookup11;
    property UserDefinedLookup12: WideString read FUserDefinedLookup12 write FUserDefinedLookup12;
    property UserDefinedLookup13: WideString read FUserDefinedLookup13 write FUserDefinedLookup13;
    property UserDefinedLookup14: WideString read FUserDefinedLookup14 write FUserDefinedLookup14;
    property UserDefinedLookup15: WideString read FUserDefinedLookup15 write FUserDefinedLookup15;
    property WorkAddress1: WideString read FWorkAddress1 write FWorkAddress1;
    property WorkAddress2: WideString read FWorkAddress2 write FWorkAddress2;
    property WorkEmail: WideString read FWorkEmail write FWorkEmail;
    property HomeEmail: WideString read FHomeEmail write FHomeEmail;
    property BenefitServiceDate: TXSDateTime read FBenefitServiceDate write FBenefitServiceDate;
    property EmpStatusEffDate: TXSDateTime read FEmpStatusEffDate write FEmpStatusEffDate;
    property CompensationCategory: WideString read FCompensationCategory write FCompensationCategory;
    property HourlyRate: Double read FHourlyRate write FHourlyRate;
    property EEOClass: WideString read FEEOClass write FEEOClass;
    property EEOClassDescription: WideString read FEEOClassDescription write FEEOClassDescription;
    property OTExempt: Boolean read FOTExempt write FOTExempt;
    property VisaExpiration: TXSDateTime read FVisaExpiration write FVisaExpiration;
    property VisaType: WideString read FVisaType write FVisaType;
    property IsVeteran: Boolean read FIsVeteran write FIsVeteran;
    property NickName: WideString read FNickName write FNickName;
    property Deductions: ArrayOfDeduction read FDeductions write FDeductions;
    property Compensations: ArrayOfCompensation read FCompensations write FCompensations;
    property DirectDeposits: ArrayOfDirectDeposit read FDirectDeposits write FDirectDeposits;
    property StateTax: ArrayOfStateTax read FStateTax write FStateTax;
    property FederalTax: ArrayOfFederalTax read FFederalTax write FFederalTax;
    property Handicapped: Boolean read FHandicapped write FHandicapped;
    property FullTimeStudent: Boolean read FFullTimeStudent write FFullTimeStudent;
    property PartTimeStudent: Boolean read FPartTimeStudent write FPartTimeStudent;
    property ShowAllDeductions: Boolean read FShowAllDeductions write FShowAllDeductions;
    property PayScheduleName: WideString read FPayScheduleName write FPayScheduleName;
  end;

  ArrayOfEmployeeInfo = array of EmployeeInfo;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  EmployeeData = class(TRemotable)
  private
    FEmployees: ArrayOfEmployeeInfo;
    FSyncDate: TXSDateTime;
  public
    destructor Destroy; override;
  published
    property Employees: ArrayOfEmployeeInfo read FEmployees write FEmployees;
    property SyncDate: TXSDateTime read FSyncDate write FSyncDate;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubFile = class(TRemotable)
  private
    FPayPeriod_StartDate: TXSDateTime;
    FPayPeriod_EndDate: TXSDateTime;
    FPayDate: TXSDateTime;
    FCheckNumber: WideString;
    FDepositAmount: TXSDecimal;
    FIndicator: WideString;
    FFileContent: TByteDynArray;
    FFileName: WideString;
    FFileExtension: WideString;
    FFileLength: Double;
  public
    destructor Destroy; override;
  published
    property PayPeriod_StartDate: TXSDateTime read FPayPeriod_StartDate write FPayPeriod_StartDate;
    property PayPeriod_EndDate: TXSDateTime read FPayPeriod_EndDate write FPayPeriod_EndDate;
    property PayDate: TXSDateTime read FPayDate write FPayDate;
    property CheckNumber: WideString read FCheckNumber write FCheckNumber;
    property DepositAmount: TXSDecimal read FDepositAmount write FDepositAmount;
    property Indicator: WideString read FIndicator write FIndicator;
    property FileContent: TByteDynArray read FFileContent write FFileContent;
    property FileName: WideString read FFileName write FFileName;
    property FileExtension: WideString read FFileExtension write FFileExtension;
    property FileLength: Double read FFileLength write FFileLength;
  end;

  ArrayOfPaystubFile = array of PaystubFile;    { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
    FSSN: WideString;
    FPaystub_Files: ArrayOfPaystubFile;
  public
    destructor Destroy; override;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property SSN: WideString read FSSN write FSSN;
    property Paystub_Files: ArrayOfPaystubFile read FPaystub_Files write FPaystub_Files;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_TimeOff = class(TRemotable)
  private
    FPayDate: TXSDateTime;
    FIndicator: WideString;
    FLeaveType: WideString;
    FStartBalance: TXSDecimal;
    FEarned: TXSDecimal;
    FTaken: TXSDecimal;
    FAdjustment: TXSDecimal;
    FEndBalance: TXSDecimal;
  public
    destructor Destroy; override;
  published
    property PayDate: TXSDateTime read FPayDate write FPayDate;
    property Indicator: WideString read FIndicator write FIndicator;
    property LeaveType: WideString read FLeaveType write FLeaveType;
    property StartBalance: TXSDecimal read FStartBalance write FStartBalance;
    property Earned: TXSDecimal read FEarned write FEarned;
    property Taken: TXSDecimal read FTaken write FTaken;
    property Adjustment: TXSDecimal read FAdjustment write FAdjustment;
    property EndBalance: TXSDecimal read FEndBalance write FEndBalance;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_Tax = class(TRemotable)
  private
    FPayDate: TXSDateTime;
    FIndicator: WideString;
    FFederalFilingStatus: WideString;
    FFederalAllowances: TXSDecimal;
    FFederalAdditionalAllowances: TXSDecimal;
    FFederalAdditionalAmount: TXSDecimal;
    FFilingState: WideString;
    FUnemploymentFilingState: WideString;
    FStateTaxFilingStatus: WideString;
    FStateAllowances: TXSDecimal;
    FStateAdditionalAllowances: TXSDecimal;
    FStateAdditionalPercentage: TXSDecimal;
    FStateAdditionalAmount: TXSDecimal;
  public
    destructor Destroy; override;
  published
    property PayDate: TXSDateTime read FPayDate write FPayDate;
    property Indicator: WideString read FIndicator write FIndicator;
    property FederalFilingStatus: WideString read FFederalFilingStatus write FFederalFilingStatus;
    property FederalAllowances: TXSDecimal read FFederalAllowances write FFederalAllowances;
    property FederalAdditionalAllowances: TXSDecimal read FFederalAdditionalAllowances write FFederalAdditionalAllowances;
    property FederalAdditionalAmount: TXSDecimal read FFederalAdditionalAmount write FFederalAdditionalAmount;
    property FilingState: WideString read FFilingState write FFilingState;
    property UnemploymentFilingState: WideString read FUnemploymentFilingState write FUnemploymentFilingState;
    property StateTaxFilingStatus: WideString read FStateTaxFilingStatus write FStateTaxFilingStatus;
    property StateAllowances: TXSDecimal read FStateAllowances write FStateAllowances;
    property StateAdditionalAllowances: TXSDecimal read FStateAdditionalAllowances write FStateAdditionalAllowances;
    property StateAdditionalPercentage: TXSDecimal read FStateAdditionalPercentage write FStateAdditionalPercentage;
    property StateAdditionalAmount: TXSDecimal read FStateAdditionalAmount write FStateAdditionalAmount;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_Summary = class(TRemotable)
  private
    FPayPeriod_Start: TXSDateTime;
    FPayPeriod_End: TXSDateTime;
    FPayDate: TXSDateTime;
    FPayGroup: WideString;
    FDepositType: WideString;
    FCheckNumber: WideString;
    FDepositAccountType: WideString;
    FDepositAccountNumber: WideString;
    FDepositAmount: TXSDecimal;
    FDepositAmountYTD: TXSDecimal;
    FIndicator: WideString;
  public
    destructor Destroy; override;
  published
    property PayPeriod_Start: TXSDateTime read FPayPeriod_Start write FPayPeriod_Start;
    property PayPeriod_End: TXSDateTime read FPayPeriod_End write FPayPeriod_End;
    property PayDate: TXSDateTime read FPayDate write FPayDate;
    property PayGroup: WideString read FPayGroup write FPayGroup;
    property DepositType: WideString read FDepositType write FDepositType;
    property CheckNumber: WideString read FCheckNumber write FCheckNumber;
    property DepositAccountType: WideString read FDepositAccountType write FDepositAccountType;
    property DepositAccountNumber: WideString read FDepositAccountNumber write FDepositAccountNumber;
    property DepositAmount: TXSDecimal read FDepositAmount write FDepositAmount;
    property DepositAmountYTD: TXSDecimal read FDepositAmountYTD write FDepositAmountYTD;
    property Indicator: WideString read FIndicator write FIndicator;
  end;

  ArrayOfPaystub_Summary = array of Paystub_Summary;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Paystub_Detail = class(TRemotable)
  private
    FPayDate: TXSDateTime;
    FIndicator: WideString;
    FRecordType: WideString;
    FItemDescription: WideString;
    FAmount: TXSDecimal;
    FAmountYTD: TXSDecimal;
    FRate: TXSDecimal;
    FHours: TXSDecimal;
    FHoursYTD: TXSDecimal;
    FRecordTypeIndicator: WideString;
    FCheckNumber: WideString;
  public
    destructor Destroy; override;
  published
    property PayDate: TXSDateTime read FPayDate write FPayDate;
    property Indicator: WideString read FIndicator write FIndicator;
    property RecordType: WideString read FRecordType write FRecordType;
    property ItemDescription: WideString read FItemDescription write FItemDescription;
    property Amount: TXSDecimal read FAmount write FAmount;
    property AmountYTD: TXSDecimal read FAmountYTD write FAmountYTD;
    property Rate: TXSDecimal read FRate write FRate;
    property Hours: TXSDecimal read FHours write FHours;
    property HoursYTD: TXSDecimal read FHoursYTD write FHoursYTD;
    property RecordTypeIndicator: WideString read FRecordTypeIndicator write FRecordTypeIndicator;
    property CheckNumber: WideString read FCheckNumber write FCheckNumber;
  end;

  ArrayOfPaystub_Detail = array of Paystub_Detail;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubData = class(TRemotable)
  private
    FPayStub_Summary: ArrayOfPaystub_Summary;
    FPayStub_Detail: ArrayOfPaystub_Detail;
    FPayStub_TimeOff: Paystub_TimeOff;
    FPayStub_Tax: Paystub_Tax;
  public
    destructor Destroy; override;
  published
    property PayStub_Summary: ArrayOfPaystub_Summary read FPayStub_Summary write FPayStub_Summary;
    property PayStub_Detail: ArrayOfPaystub_Detail read FPayStub_Detail write FPayStub_Detail;
    property PayStub_TimeOff: Paystub_TimeOff read FPayStub_TimeOff write FPayStub_TimeOff;
    property PayStub_Tax: Paystub_Tax read FPayStub_Tax write FPayStub_Tax;
  end;

  ArrayOfPaystubData = array of PaystubData;    { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaystubDataRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
    FSSN: WideString;
    FPaystub_Data: ArrayOfPaystubData;
  public
    destructor Destroy; override;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property SSN: WideString read FSSN write FSSN;
    property Paystub_Data: ArrayOfPaystubData read FPaystub_Data write FPaystub_Data;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AccrualBalance = class(TRemotable)
  private
    FSSN: WideString;
    FTransactionDate: TXSDateTime;
    FTimeOffType: WideString;
    FTransactionType: WideString;
    FBalance: TXSDecimal;
    FComments: WideString;
  public
    destructor Destroy; override;
  published
    property SSN: WideString read FSSN write FSSN;
    property TransactionDate: TXSDateTime read FTransactionDate write FTransactionDate;
    property TimeOffType: WideString read FTimeOffType write FTimeOffType;
    property TransactionType: WideString read FTransactionType write FTransactionType;
    property Balance: TXSDecimal read FBalance write FBalance;
    property Comments: WideString read FComments write FComments;
  end;

  ArrayOfAccrualBalance = array of AccrualBalance;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  TimeOffBalanceRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
    FTimeOffBalance: ArrayOfAccrualBalance;
  public
    destructor Destroy; override;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property TimeOffBalance: ArrayOfAccrualBalance read FTimeOffBalance write FTimeOffBalance;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  W2File = class(TRemotable)
  private
    FNotes: WideString;
    FTaxYear: Integer;
    FFileContent: TByteDynArray;
    FFileExtension: WideString;
    FFileLength: Double;
  published
    property Notes: WideString read FNotes write FNotes;
    property TaxYear: Integer read FTaxYear write FTaxYear;
    property FileContent: TByteDynArray read FFileContent write FFileContent;
    property FileExtension: WideString read FFileExtension write FFileExtension;
    property FileLength: Double read FFileLength write FFileLength;
  end;

  ArrayOfW2File = array of W2File;              { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  W2Request = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
    FInfinityHREmployee_ID: Integer;
    FSSN: WideString;
    FW2_Files: ArrayOfW2File;
  public
    destructor Destroy; override;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property InfinityHREmployee_ID: Integer read FInfinityHREmployee_ID write FInfinityHREmployee_ID;
    property SSN: WideString read FSSN write FSSN;
    property W2_Files: ArrayOfW2File read FW2_Files write FW2_Files;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  BenefitRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  BenefitStructureItem = class(TRemotable)
  private
    FPackageName: WideString;
    FPackage_ID: Integer;
    FPackageEffectiveDate: TXSDateTime;
    FPackageExpirationDate: TXSDateTime;
    FBenefit_ID: Integer;
    FBenefitName: WideString;
    FBenefitCarrierCode: WideString;
    FPlan_ID: Integer;
    FPlanName: WideString;
    FPlanCarrierCode: WideString;
    FOption_ID: Integer;
    FOptionName: WideString;
  public
    destructor Destroy; override;
  published
    property PackageName: WideString read FPackageName write FPackageName;
    property Package_ID: Integer read FPackage_ID write FPackage_ID;
    property PackageEffectiveDate: TXSDateTime read FPackageEffectiveDate write FPackageEffectiveDate;
    property PackageExpirationDate: TXSDateTime read FPackageExpirationDate write FPackageExpirationDate;
    property Benefit_ID: Integer read FBenefit_ID write FBenefit_ID;
    property BenefitName: WideString read FBenefitName write FBenefitName;
    property BenefitCarrierCode: WideString read FBenefitCarrierCode write FBenefitCarrierCode;
    property Plan_ID: Integer read FPlan_ID write FPlan_ID;
    property PlanName: WideString read FPlanName write FPlanName;
    property PlanCarrierCode: WideString read FPlanCarrierCode write FPlanCarrierCode;
    property Option_ID: Integer read FOption_ID write FOption_ID;
    property OptionName: WideString read FOptionName write FOptionName;
  end;

  ArrayOfBenefitStructureItem = array of BenefitStructureItem;   { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  BenefitStructure = class(TRemotable)
  private
    FBenefitStructureItems: ArrayOfBenefitStructureItem;
  public
    destructor Destroy; override;
  published
    property BenefitStructureItems: ArrayOfBenefitStructureItem read FBenefitStructureItems write FBenefitStructureItems;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  Lookup = class(TRemotable)
  private
    FID: Integer;
    FText: WideString;
  published
    property ID: Integer read FID write FID;
    property Text: WideString read FText write FText;
  end;

  ArrayOfLookup = array of Lookup;              { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  LookupData = class(TRemotable)
  private
    FLookups: ArrayOfLookup;
  public
    destructor Destroy; override;
  published
    property Lookups: ArrayOfLookup read FLookups write FLookups;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PaySchedule = class(TRemotable)
  private
    FID: Integer;
    FName: WideString;
    FDescription: WideString;
    FPayPeriods: Integer;
    FIsDefaultSchedule: Boolean;
  published
    property ID: Integer read FID write FID;
    property Name: WideString read FName write FName;
    property Description: WideString read FDescription write FDescription;
    property PayPeriods: Integer read FPayPeriods write FPayPeriods;
    property IsDefaultSchedule: Boolean read FIsDefaultSchedule write FIsDefaultSchedule;
  end;

  ArrayOfPaySchedule = array of PaySchedule;    { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PayScheduleData = class(TRemotable)
  private
    FPaySchedules: ArrayOfPaySchedule;
  public
    destructor Destroy; override;
  published
    property PaySchedules: ArrayOfPaySchedule read FPaySchedules write FPaySchedules;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CompanyRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CompanyInfo = class(TRemotable)
  private
    FVendor_ID: WideString;
    FInfinityCompany_ID: WideString;
    FVendorCompany_ID: WideString;
    FVendorCompanyPassword: WideString;
    FDirectDepositExcluded: Boolean;
    FCompensationExcluded: Boolean;
    FStateTaxExcluded: Boolean;
    FFederalTaxExcluded: Boolean;
    FDeductionsExcluded: Boolean;
  published
    property Vendor_ID: WideString read FVendor_ID write FVendor_ID;
    property InfinityCompany_ID: WideString read FInfinityCompany_ID write FInfinityCompany_ID;
    property VendorCompany_ID: WideString read FVendorCompany_ID write FVendorCompany_ID;
    property VendorCompanyPassword: WideString read FVendorCompanyPassword write FVendorCompanyPassword;
    property DirectDepositExcluded: Boolean read FDirectDepositExcluded write FDirectDepositExcluded;
    property CompensationExcluded: Boolean read FCompensationExcluded write FCompensationExcluded;
    property StateTaxExcluded: Boolean read FStateTaxExcluded write FStateTaxExcluded;
    property FederalTaxExcluded: Boolean read FFederalTaxExcluded write FFederalTaxExcluded;
    property DeductionsExcluded: Boolean read FDeductionsExcluded write FDeductionsExcluded;
  end;

  ArrayOfCompanyInfo = array of CompanyInfo;    { "http://www.infinity-ss.com/services" }


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  CompanyData = class(TRemotable)
  private
    FEnterpriseVendor_ID: WideString;
    FCompanies: ArrayOfCompanyInfo;
  public
    destructor Destroy; override;
  published
    property EnterpriseVendor_ID: WideString read FEnterpriseVendor_ID write FEnterpriseVendor_ID;
    property Companies: ArrayOfCompanyInfo read FCompanies write FCompanies;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  PayrollServiceException = class(TRemotable)
  private
    FErrorMessage: WideString;
    FErrorException: WideString;
    FErrorStackTrace: WideString;
  published
    property ErrorMessage: WideString read FErrorMessage write FErrorMessage;
    property ErrorException: WideString read FErrorException write FErrorException;
    property ErrorStackTrace: WideString read FErrorStackTrace write FErrorStackTrace;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  AuditRequest = class(TRemotable)
  private
    FSessionID: WideString;
    FVendorID: WideString;
    FAuditMessage: WideString;
  published
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property AuditMessage: WideString read FAuditMessage write FAuditMessage;
  end;



  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // ************************************************************************ //
  ErrorRequest = class(TRemotable)
  private
    FErrorMessage: WideString;
    FSessionID: WideString;
    FVendorID: WideString;
    FErrorInnerException: WideString;
    FErrorStackTrace: WideString;
    FErrorProcedureName: WideString;
  published
    property ErrorMessage: WideString read FErrorMessage write FErrorMessage;
    property SessionID: WideString read FSessionID write FSessionID;
    property VendorID: WideString read FVendorID write FVendorID;
    property ErrorInnerException: WideString read FErrorInnerException write FErrorInnerException;
    property ErrorStackTrace: WideString read FErrorStackTrace write FErrorStackTrace;
    property ErrorProcedureName: WideString read FErrorProcedureName write FErrorProcedureName;
  end;


  // ************************************************************************ //
  // Namespace : http://www.infinity-ss.com/services
  // soapAction: http://www.infinity-ss.com/services/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : PayrollSyncSoap
  // service   : PayrollSync
  // port      : PayrollSyncSoap
  // URL       : https://www.infinityhrconnect.com/test/services/payrollsync.asmx
  // ************************************************************************ //
  PayrollSyncSoap = interface(IInvokable)
  ['{182DD499-16FD-3404-0686-6834DBB5202E}']
    function  Authenticate(const request: AuthRequest): AuthResponse; stdcall;
    procedure UpdateEmployees(const employeeRequest: EmployeeRequest; const employeeInfos: ArrayOfEmployeeInfo); stdcall;
    function  GetEmployeesByEffectiveDate(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime): EmployeeData; stdcall;
    function  GetEmployeesByDateRange(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime; const endDate: TXSDateTime): EmployeeData; stdcall;
    function  GetEmployeeByHREmployeeID(const employeeRequest: EmployeeRequest; const HREmployee_ID: Integer): EmployeeData; stdcall;
    function  GetEmployeeByUserName(const employeeRequest: EmployeeRequest; const UserName: WideString): EmployeeData; stdcall;
    function  GetEmployeesAll(const employeeRequest: EmployeeRequest): EmployeeData; stdcall;
    function  GetEmployeeIDs_All(const employeeRequest: EmployeeRequest): EmployeeData; stdcall;
    function  GetEmployeeIDs_ByEffectiveDate(const employeeRequest: EmployeeRequest; const beginDate: TXSDateTime): EmployeeData; stdcall;
    function  UpsertEmployees(const empRequest: EmployeeRequest; const employeeData: EmployeeData): EmployeeData; stdcall;
    procedure UpsertEmployeePaystubs(const paystubRequest: PaystubRequest); stdcall;
    procedure UpsertEmployeePaystubData(const paystubDataRequest: PaystubDataRequest); stdcall;
    procedure UploadTimeOffBalance(const balanceRequest: TimeOffBalanceRequest); stdcall;
    procedure UploadEmployeeW2(const w2Request: W2Request); stdcall;
    procedure UploadEmployeeW2_BySSN(const w2Request: W2Request); stdcall;
    function  GetCurrentBenefitStructure(const benefitRequest: BenefitRequest): BenefitStructure; stdcall;
    function  GetCurrentBenefitStructureByEffectiveDate(const benefitRequest: BenefitRequest; const EffectiveDate: TXSDateTime): BenefitStructure; stdcall;
    function  GetEmployeeTypes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetPTOTypes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetPTOTransactionTypes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetDepartments(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetPaySchedules(const empRequest: EmployeeRequest; const PaySchedule_Year: Integer): PayScheduleData; stdcall;
    function  GetJobCodes(const empRequest: EmployeeRequest): LookupData; stdcall;
    function  GetServerTime: TXSDateTime; stdcall;
    function  GetEnterpriseCompanyInfo(const CompanyRequest: CompanyRequest): CompanyData; stdcall;
    function  GetCompanyInfo(const CompanyRequest: CompanyRequest): CompanyData; stdcall;
    procedure LogException(const PayrollServiceException: PayrollServiceException); stdcall;
    procedure LogAudit(const audit: AuditRequest); stdcall;
    procedure LogEvent(const audit: AuditRequest); stdcall;
    procedure LogError(const serviceError: ErrorRequest); stdcall;
  end;

function GetPayrollSyncSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): PayrollSyncSoap;


implementation

function GetPayrollSyncSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): PayrollSyncSoap;
const
  defWSDL = 'https://www.infinityhrconnect.com/test/services/payrollsync.asmx?wsdl';
  defURL  = 'https://www.infinityhrconnect.com/test/services/payrollsync.asmx';
  defSvc  = 'PayrollSync';
  defPrt  = 'PayrollSyncSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as PayrollSyncSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor AuthResponse.Destroy;
begin
  if Assigned(FSessionStart) then
    FSessionStart.Free;
  if Assigned(FSessionEnd) then
    FSessionEnd.Free;
  if Assigned(FSyncDate) then
    FSyncDate.Free;
  inherited Destroy;
end;

destructor Deduction.Destroy;
begin
  if Assigned(FEffectiveDate) then
    FEffectiveDate.Free;
  if Assigned(FExpirationDate) then
    FExpirationDate.Free;
  if Assigned(FPremiumAmount) then
    FPremiumAmount.Free;
  if Assigned(FEmployeeCost) then
    FEmployeeCost.Free;
  if Assigned(FEmployerCost) then
    FEmployerCost.Free;
  if Assigned(FCreditAmount) then
    FCreditAmount.Free;
  if Assigned(FCoverageAmount) then
    FCoverageAmount.Free;
  if Assigned(FPreTaxDeduction) then
    FPreTaxDeduction.Free;
  if Assigned(FPostTaxDeduction) then
    FPostTaxDeduction.Free;
  if Assigned(FPerPayEmployeeCost) then
    FPerPayEmployeeCost.Free;
  if Assigned(FPerPayEmployerCost) then
    FPerPayEmployerCost.Free;
  if Assigned(FAnnualPremiumAmount) then
    FAnnualPremiumAmount.Free;
  if Assigned(FAnnualCreditAmount) then
    FAnnualCreditAmount.Free;
  if Assigned(FAnnualEmployeeCost) then
    FAnnualEmployeeCost.Free;
  if Assigned(FDeductionEffectiveDate) then
    FDeductionEffectiveDate.Free;
  if Assigned(FDeductionExpirationDate) then
    FDeductionExpirationDate.Free;
  inherited Destroy;
end;

destructor Compensation.Destroy;
begin
  if Assigned(FEffectiveDate) then
    FEffectiveDate.Free;
  if Assigned(FRate) then
    FRate.Free;
  if Assigned(FHoursWorked) then
    FHoursWorked.Free;
  if Assigned(FAnnualRate) then
    FAnnualRate.Free;
  if Assigned(FHourlyRate) then
    FHourlyRate.Free;
  if Assigned(FOverTimeRate) then
    FOverTimeRate.Free;
  if Assigned(FOtherRate) then
    FOtherRate.Free;
  if Assigned(FLastModifiedDate) then
    FLastModifiedDate.Free;
  if Assigned(FPayPeriodAmount) then
    FPayPeriodAmount.Free;
  if Assigned(FExpirationDate) then
    FExpirationDate.Free;
  inherited Destroy;
end;

destructor DirectDeposit.Destroy;
begin
  if Assigned(FDepositAmount) then
    FDepositAmount.Free;
  if Assigned(FDepositPercent) then
    FDepositPercent.Free;
  if Assigned(FModifiedDate) then
    FModifiedDate.Free;
  if Assigned(FEffectiveDate) then
    FEffectiveDate.Free;
  if Assigned(FExpirationDate) then
    FExpirationDate.Free;
  inherited Destroy;
end;

destructor StateTax.Destroy;
begin
  if Assigned(FAdditionalWithholding) then
    FAdditionalWithholding.Free;
  if Assigned(FPercentWithholding) then
    FPercentWithholding.Free;
  if Assigned(FEffectiveDate) then
    FEffectiveDate.Free;
  if Assigned(FOverrideAmount) then
    FOverrideAmount.Free;
  if Assigned(FOverridePercent) then
    FOverridePercent.Free;
  if Assigned(FAdditionalPercentWithheld) then
    FAdditionalPercentWithheld.Free;
  if Assigned(FStartDate) then
    FStartDate.Free;
  if Assigned(FExpirationDate) then
    FExpirationDate.Free;
  inherited Destroy;
end;

destructor FederalTax.Destroy;
begin
  if Assigned(FAdditionalWithholding) then
    FAdditionalWithholding.Free;
  if Assigned(FEffectiveDate) then
    FEffectiveDate.Free;
  if Assigned(FOverrideAmount) then
    FOverrideAmount.Free;
  if Assigned(FOverridePercent) then
    FOverridePercent.Free;
  if Assigned(FStartDate) then
    FStartDate.Free;
  if Assigned(FExpirationDate) then
    FExpirationDate.Free;
  inherited Destroy;
end;

destructor EmployeeInfo.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FDeductions)-1 do
    if Assigned(FDeductions[I]) then
      FDeductions[I].Free;
  SetLength(FDeductions, 0);
  for I := 0 to Length(FCompensations)-1 do
    if Assigned(FCompensations[I]) then
      FCompensations[I].Free;
  SetLength(FCompensations, 0);
  for I := 0 to Length(FDirectDeposits)-1 do
    if Assigned(FDirectDeposits[I]) then
      FDirectDeposits[I].Free;
  SetLength(FDirectDeposits, 0);
  for I := 0 to Length(FStateTax)-1 do
    if Assigned(FStateTax[I]) then
      FStateTax[I].Free;
  SetLength(FStateTax, 0);
  for I := 0 to Length(FFederalTax)-1 do
    if Assigned(FFederalTax[I]) then
      FFederalTax[I].Free;
  SetLength(FFederalTax, 0);
  if Assigned(FBirthDate) then
    FBirthDate.Free;
  if Assigned(FDefaultHours) then
    FDefaultHours.Free;
  if Assigned(FHireDate) then
    FHireDate.Free;
  if Assigned(FOrigHireDate) then
    FOrigHireDate.Free;
  if Assigned(FAlternateServiceDate1) then
    FAlternateServiceDate1.Free;
  if Assigned(FAlternateServiceDate2) then
    FAlternateServiceDate2.Free;
  if Assigned(FRetirementDate) then
    FRetirementDate.Free;
  if Assigned(FSalary) then
    FSalary.Free;
  if Assigned(FReHireDate) then
    FReHireDate.Free;
  if Assigned(FTerminationDate) then
    FTerminationDate.Free;
  if Assigned(FBenefitServiceDate) then
    FBenefitServiceDate.Free;
  if Assigned(FEmpStatusEffDate) then
    FEmpStatusEffDate.Free;
  if Assigned(FVisaExpiration) then
    FVisaExpiration.Free;
  inherited Destroy;
end;

destructor EmployeeData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FEmployees)-1 do
    if Assigned(FEmployees[I]) then
      FEmployees[I].Free;
  SetLength(FEmployees, 0);
  if Assigned(FSyncDate) then
    FSyncDate.Free;
  inherited Destroy;
end;

destructor PaystubFile.Destroy;
begin
  if Assigned(FPayPeriod_StartDate) then
    FPayPeriod_StartDate.Free;
  if Assigned(FPayPeriod_EndDate) then
    FPayPeriod_EndDate.Free;
  if Assigned(FPayDate) then
    FPayDate.Free;
  if Assigned(FDepositAmount) then
    FDepositAmount.Free;
  inherited Destroy;
end;

destructor PaystubRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPaystub_Files)-1 do
    if Assigned(FPaystub_Files[I]) then
      FPaystub_Files[I].Free;
  SetLength(FPaystub_Files, 0);
  inherited Destroy;
end;

destructor Paystub_TimeOff.Destroy;
begin
  if Assigned(FPayDate) then
    FPayDate.Free;
  if Assigned(FStartBalance) then
    FStartBalance.Free;
  if Assigned(FEarned) then
    FEarned.Free;
  if Assigned(FTaken) then
    FTaken.Free;
  if Assigned(FAdjustment) then
    FAdjustment.Free;
  if Assigned(FEndBalance) then
    FEndBalance.Free;
  inherited Destroy;
end;

destructor Paystub_Tax.Destroy;
begin
  if Assigned(FPayDate) then
    FPayDate.Free;
  if Assigned(FFederalAllowances) then
    FFederalAllowances.Free;
  if Assigned(FFederalAdditionalAllowances) then
    FFederalAdditionalAllowances.Free;
  if Assigned(FFederalAdditionalAmount) then
    FFederalAdditionalAmount.Free;
  if Assigned(FStateAllowances) then
    FStateAllowances.Free;
  if Assigned(FStateAdditionalAllowances) then
    FStateAdditionalAllowances.Free;
  if Assigned(FStateAdditionalPercentage) then
    FStateAdditionalPercentage.Free;
  if Assigned(FStateAdditionalAmount) then
    FStateAdditionalAmount.Free;
  inherited Destroy;
end;

destructor Paystub_Summary.Destroy;
begin
  if Assigned(FPayPeriod_Start) then
    FPayPeriod_Start.Free;
  if Assigned(FPayPeriod_End) then
    FPayPeriod_End.Free;
  if Assigned(FPayDate) then
    FPayDate.Free;
  if Assigned(FDepositAmount) then
    FDepositAmount.Free;
  if Assigned(FDepositAmountYTD) then
    FDepositAmountYTD.Free;
  inherited Destroy;
end;

destructor Paystub_Detail.Destroy;
begin
  if Assigned(FPayDate) then
    FPayDate.Free;
  if Assigned(FAmount) then
    FAmount.Free;
  if Assigned(FAmountYTD) then
    FAmountYTD.Free;
  if Assigned(FRate) then
    FRate.Free;
  if Assigned(FHours) then
    FHours.Free;
  if Assigned(FHoursYTD) then
    FHoursYTD.Free;
  inherited Destroy;
end;

destructor PaystubData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPayStub_Summary)-1 do
    if Assigned(FPayStub_Summary[I]) then
      FPayStub_Summary[I].Free;
  SetLength(FPayStub_Summary, 0);
  for I := 0 to Length(FPayStub_Detail)-1 do
    if Assigned(FPayStub_Detail[I]) then
      FPayStub_Detail[I].Free;
  SetLength(FPayStub_Detail, 0);
  if Assigned(FPayStub_TimeOff) then
    FPayStub_TimeOff.Free;
  if Assigned(FPayStub_Tax) then
    FPayStub_Tax.Free;
  inherited Destroy;
end;

destructor PaystubDataRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPaystub_Data)-1 do
    if Assigned(FPaystub_Data[I]) then
      FPaystub_Data[I].Free;
  SetLength(FPaystub_Data, 0);
  inherited Destroy;
end;

destructor AccrualBalance.Destroy;
begin
  if Assigned(FTransactionDate) then
    FTransactionDate.Free;
  if Assigned(FBalance) then
    FBalance.Free;
  inherited Destroy;
end;

destructor TimeOffBalanceRequest.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FTimeOffBalance)-1 do
    if Assigned(FTimeOffBalance[I]) then
      FTimeOffBalance[I].Free;
  SetLength(FTimeOffBalance, 0);
  inherited Destroy;
end;

destructor W2Request.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FW2_Files)-1 do
    if Assigned(FW2_Files[I]) then
      FW2_Files[I].Free;
  SetLength(FW2_Files, 0);
  inherited Destroy;
end;

destructor BenefitStructureItem.Destroy;
begin
  if Assigned(FPackageEffectiveDate) then
    FPackageEffectiveDate.Free;
  if Assigned(FPackageExpirationDate) then
    FPackageExpirationDate.Free;
  inherited Destroy;
end;

destructor BenefitStructure.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FBenefitStructureItems)-1 do
    if Assigned(FBenefitStructureItems[I]) then
      FBenefitStructureItems[I].Free;
  SetLength(FBenefitStructureItems, 0);
  inherited Destroy;
end;

destructor LookupData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FLookups)-1 do
    if Assigned(FLookups[I]) then
      FLookups[I].Free;
  SetLength(FLookups, 0);
  inherited Destroy;
end;

destructor PayScheduleData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPaySchedules)-1 do
    if Assigned(FPaySchedules[I]) then
      FPaySchedules[I].Free;
  SetLength(FPaySchedules, 0);
  inherited Destroy;
end;

destructor CompanyData.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FCompanies)-1 do
    if Assigned(FCompanies[I]) then
      FCompanies[I].Free;
  SetLength(FCompanies, 0);
  inherited Destroy;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(PayrollSyncSoap), 'http://www.infinity-ss.com/services', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(PayrollSyncSoap), 'http://www.infinity-ss.com/services/%operationName%');
  RemClassRegistry.RegisterXSClass(AuthRequest, 'http://www.infinity-ss.com/services', 'AuthRequest');
  RemClassRegistry.RegisterXSClass(AuthResponse, 'http://www.infinity-ss.com/services', 'AuthResponse');
  RemClassRegistry.RegisterXSClass(EmployeeRequest, 'http://www.infinity-ss.com/services', 'EmployeeRequest');
  RemClassRegistry.RegisterXSClass(Deduction, 'http://www.infinity-ss.com/services', 'Deduction');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfDeduction), 'http://www.infinity-ss.com/services', 'ArrayOfDeduction');
  RemClassRegistry.RegisterXSClass(Compensation, 'http://www.infinity-ss.com/services', 'Compensation');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCompensation), 'http://www.infinity-ss.com/services', 'ArrayOfCompensation');
  RemClassRegistry.RegisterXSClass(DirectDeposit, 'http://www.infinity-ss.com/services', 'DirectDeposit');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfDirectDeposit), 'http://www.infinity-ss.com/services', 'ArrayOfDirectDeposit');
  RemClassRegistry.RegisterXSClass(StateTax, 'http://www.infinity-ss.com/services', 'StateTax');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfStateTax), 'http://www.infinity-ss.com/services', 'ArrayOfStateTax');
  RemClassRegistry.RegisterXSClass(FederalTax, 'http://www.infinity-ss.com/services', 'FederalTax');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfFederalTax), 'http://www.infinity-ss.com/services', 'ArrayOfFederalTax');
  RemClassRegistry.RegisterXSClass(EmployeeInfo, 'http://www.infinity-ss.com/services', 'EmployeeInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfEmployeeInfo), 'http://www.infinity-ss.com/services', 'ArrayOfEmployeeInfo');
  RemClassRegistry.RegisterXSClass(EmployeeData, 'http://www.infinity-ss.com/services', 'EmployeeData');
  RemClassRegistry.RegisterXSClass(PaystubFile, 'http://www.infinity-ss.com/services', 'PaystubFile');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystubFile), 'http://www.infinity-ss.com/services', 'ArrayOfPaystubFile');
  RemClassRegistry.RegisterXSClass(PaystubRequest, 'http://www.infinity-ss.com/services', 'PaystubRequest');
  RemClassRegistry.RegisterXSClass(Paystub_TimeOff, 'http://www.infinity-ss.com/services', 'Paystub_TimeOff');
  RemClassRegistry.RegisterXSClass(Paystub_Tax, 'http://www.infinity-ss.com/services', 'Paystub_Tax');
  RemClassRegistry.RegisterXSClass(Paystub_Summary, 'http://www.infinity-ss.com/services', 'Paystub_Summary');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystub_Summary), 'http://www.infinity-ss.com/services', 'ArrayOfPaystub_Summary');
  RemClassRegistry.RegisterXSClass(Paystub_Detail, 'http://www.infinity-ss.com/services', 'Paystub_Detail');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystub_Detail), 'http://www.infinity-ss.com/services', 'ArrayOfPaystub_Detail');
  RemClassRegistry.RegisterXSClass(PaystubData, 'http://www.infinity-ss.com/services', 'PaystubData');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaystubData), 'http://www.infinity-ss.com/services', 'ArrayOfPaystubData');
  RemClassRegistry.RegisterXSClass(PaystubDataRequest, 'http://www.infinity-ss.com/services', 'PaystubDataRequest');
  RemClassRegistry.RegisterXSClass(AccrualBalance, 'http://www.infinity-ss.com/services', 'AccrualBalance');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfAccrualBalance), 'http://www.infinity-ss.com/services', 'ArrayOfAccrualBalance');
  RemClassRegistry.RegisterXSClass(TimeOffBalanceRequest, 'http://www.infinity-ss.com/services', 'TimeOffBalanceRequest');
  RemClassRegistry.RegisterXSClass(W2File, 'http://www.infinity-ss.com/services', 'W2File');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfW2File), 'http://www.infinity-ss.com/services', 'ArrayOfW2File');
  RemClassRegistry.RegisterXSClass(W2Request, 'http://www.infinity-ss.com/services', 'W2Request');
  RemClassRegistry.RegisterXSClass(BenefitRequest, 'http://www.infinity-ss.com/services', 'BenefitRequest');
  RemClassRegistry.RegisterXSClass(BenefitStructureItem, 'http://www.infinity-ss.com/services', 'BenefitStructureItem');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfBenefitStructureItem), 'http://www.infinity-ss.com/services', 'ArrayOfBenefitStructureItem');
  RemClassRegistry.RegisterXSClass(BenefitStructure, 'http://www.infinity-ss.com/services', 'BenefitStructure');
  RemClassRegistry.RegisterXSClass(Lookup, 'http://www.infinity-ss.com/services', 'Lookup');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfLookup), 'http://www.infinity-ss.com/services', 'ArrayOfLookup');
  RemClassRegistry.RegisterXSClass(LookupData, 'http://www.infinity-ss.com/services', 'LookupData');
  RemClassRegistry.RegisterXSClass(PaySchedule, 'http://www.infinity-ss.com/services', 'PaySchedule');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfPaySchedule), 'http://www.infinity-ss.com/services', 'ArrayOfPaySchedule');
  RemClassRegistry.RegisterXSClass(PayScheduleData, 'http://www.infinity-ss.com/services', 'PayScheduleData');
  RemClassRegistry.RegisterXSClass(CompanyRequest, 'http://www.infinity-ss.com/services', 'CompanyRequest');
  RemClassRegistry.RegisterXSClass(CompanyInfo, 'http://www.infinity-ss.com/services', 'CompanyInfo');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfCompanyInfo), 'http://www.infinity-ss.com/services', 'ArrayOfCompanyInfo');
  RemClassRegistry.RegisterXSClass(CompanyData, 'http://www.infinity-ss.com/services', 'CompanyData');
  RemClassRegistry.RegisterXSClass(PayrollServiceException, 'http://www.infinity-ss.com/services', 'PayrollServiceException');
  RemClassRegistry.RegisterXSClass(AuditRequest, 'http://www.infinity-ss.com/services', 'AuditRequest');
  RemClassRegistry.RegisterXSClass(ErrorRequest, 'http://www.infinity-ss.com/services', 'ErrorRequest');

end.
