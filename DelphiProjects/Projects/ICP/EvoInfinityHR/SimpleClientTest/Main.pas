unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, InvokeRegistry, StdCtrls, Rio, SOAPHTTPClient, DB, DBClient,
  SOAPConn, PayrollSync, ExtCtrls, XSBuiltIns, Types;

type
  TForm1 = class(TForm)
    HTTPRIO: THTTPRIO;
    btnAuth: TButton;
    edVendorID: TEdit;
    edPassword: TEdit;
    edSessionID: TEdit;
    mmDetail: TMemo;
    Panel1: TPanel;
    edMethodName: TEdit;
    mmSOAPRequest: TMemo;
    mmSOAPResponse: TMemo;
    btnGetEmployees: TButton;
    Button1: TButton;
    mBase64Pdf: TMemo;
    SaveDialog1: TSaveDialog;
    procedure btnAuthClick(Sender: TObject);
    procedure HTTPRIOBeforeExecute(const MethodName: String;
      var SOAPRequest: WideString);
    procedure btnGetEmployeesClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnAuthClick(Sender: TObject);
var
  Req: AuthRequest;
  Resp: AuthResponse;
begin
  Req := AuthRequest.Create;
  try
    Req.VendorID := edVendorID.Text;
    Req.VendorPwd := edPassword.Text;

    try
      Resp := (HTTPRIO as PayrollSyncSoap).Authenticate( Req );
      edSessionID.Text := Resp.SessionID;
    except
      on e: Exception do
      begin
        edSessionID.Text := 'Error!';
        mmDetail.Lines.Text := e.Message;
      end;
    end;
  finally
    Req.Free;
  end;
end;

procedure TForm1.HTTPRIOBeforeExecute(const MethodName: String;
  var SOAPRequest: WideString);
begin
{  SOAPRequest := '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'+
  '<soap:Body><Authenticate xmlns="http://www.infinity-ss.com/services"><request><VendorID>049607e4-644c-484e-9127-3b74b3563b34</VendorID>'+
  '<VendorPwd>summit.2011</VendorPwd></request></Authenticate></soap:Body></soap:Envelope>';}

  edMethodName.Text := MethodName;
  mmSOAPRequest.Lines.Text := SOAPRequest;
end;

procedure TForm1.btnGetEmployeesClick(Sender: TObject);
var
  EeReq: EmployeeRequest;
  BeginDate, EndDate: TXSDateTime;
begin
  EeReq := EmployeeRequest.Create;
  BeginDate := TXSDateTime.Create;
  EndDate := TXSDateTime.Create;
  try
    EeReq.VendorID := edVendorID.Text;
    EeReq.SessionID := edSessionID.Text;
    BeginDate.AsDateTime := StrToDate('8/12/2011');
    EndDate.AsDateTime := StrToDate('10/12/2011');

    try
      (HTTPRIO as PayrollSyncSoap).GetEmployeesByDateRange( EeReq, BeginDate, EndDate );
    except
      on e: Exception do
      begin
        edSessionID.Text := 'Error!';
        mmDetail.Lines.Text := e.Message;
      end;
    end;
  finally
    EeReq.Free;
    BeginDate.Free;
    EndDate.Free;
  end;
end;

function DecodeBase64(Value: String): String;
const b64alphabet: PChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  function DecodeChunk(const Chunk: String): String;
  var
    W: LongWord;
    i: Byte;
  begin
    W := 0; Result := '';
    for i := 1 to 4 do
      if Pos(Chunk[i], b64alphabet) <> 0 then
        W := W + Word((Pos(Chunk[i], b64alphabet) - 1)) shl ((4 - i) * 6);
    for i := 1 to 3 do
      Result := Result + Chr(W shr ((3 - i) * 8) and $ff);
  end;
begin
  Result := '';
  if Length(Value) mod 4 <> 0 then Exit;
  while Length(Value) > 0 do
  begin
    Result := Result + DecodeChunk(Copy(Value, 0, 4));
    System.Delete(Value, 1, 4);
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Base64str: string;
  i: integer;
  testFS: TFileStream;
  FileStream: TStringStream;
  fContent: TByteDynArray;
begin
  Base64str := '';

  if SaveDialog1.Execute then
  begin
    for i := 0 to mBase64Pdf.Lines.Count - 1 do
      Base64str := Base64str + Trim(mBase64Pdf.Lines[i]); 

    //base64 to string
    Base64str := DecodeBase64(Base64str);

    testFS := TFileStream.Create(SaveDialog1.FileName, fmCreate);
    FileStream := TStringStream.Create( Base64str );
    try
      FileStream.Position := 0;
      SetLength( fContent, FileStream.Size );
      FileStream.ReadBuffer( fContent[0], FileStream.Size );

      testFS.CopyFrom(FileStream, 0);
    finally
      FreeAndNil(FileStream);
      FreeAndNil(testFS);
    end;
  end;
end;

end.
