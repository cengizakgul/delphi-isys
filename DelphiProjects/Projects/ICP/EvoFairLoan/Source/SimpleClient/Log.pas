unit Log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdySendMailLoggerView, gdyCommonLoggerView, gdyCommonLogger,
  gdyclasses, gdyLoggerImpl, isSettings, ExtCtrls, common, StdCtrls,
  Buttons;

type
  TfrmLogger = class(TForm, ILogRecorder)
    pnlLogger: TPanel;
  private
    FLogOutput: TStringList;
    FLogOutputWriter: ILoggerEventSink;
    FLoggerFrame: TSendMailLoggerViewFrame;

    {ILogRecorder}
    procedure LogRecorder_Start(html: boolean);
    function LogRecorder_Stop: string;
    procedure ILogRecorder.Start = LogRecorder_Start;
    function ILogRecorder.Stop = LogRecorder_Stop;
  protected
    FSettings: IisSettings;
    FLoggerFrameClass: TSendMailLoggerViewFrameClass;
    procedure SaveLogArchiveTo(filename: string);
  public
    function Logger: ICommonLogger;
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmLogger: TfrmLogger;
  
implementation

{$R *.dfm}

uses
  gdyRedir, gdyLogWriters, gdycommon, dateutils, gdyGlobalWaitIndicator,
  printers, gdyWaitForm;

{ TfrmLogger }

constructor TfrmLogger.Create(Owner: TComponent);
begin
  if FLoggerFrameClass = nil then
    FLoggerFrameClass := TSendMailLoggerViewFrame;
  FLogOutput := TStringList.Create;
  inherited;
  FLoggerFrame := FLoggerFrameClass.Create( pnlLogger );
  FLoggerFrame.Parent := pnlLogger;
  FLoggerFrame.Align := alClient;

  FSettings := AppSettings;

{$ifndef FINAL_RELEASE}
  try
    FLoggerFrame.LoggerKeeper.StartDevLoggingToFile( Redirection.GetFilename(sDebugLogFileAlias) );
  except
    Logger.StopException;
  end;
{$endif}
  try
    FLoggerFrame.LoggerKeeper.StartLoggingToFile( Redirection.GetFilename(sLogFileAlias) );
  except
    Logger.StopException;
  end;
  FLoggerFrame.Password := GetISystemsPassword;
  FLoggerFrame.EMail := GetISystemsEmail;
  Logger.LogDebug('Caption: ' + Caption );
  Logger.LogDebug('Timestamp: ' + DateTimeToStr(Now) );

end;

destructor TfrmLogger.Destroy;
begin
  FLoggerFrame.LoggerKeeper.StopLoggingToFile;
{$ifndef FINAL_RELEASE}
  FLoggerFrame.LoggerKeeper.StopDevLoggingToFile;
{$endif}
  SetWaitIndicator(nil);
  inherited;
  FreeAndNil(FLogOutput);
end;

function TfrmLogger.Logger: ICommonLogger;
begin
  Result := FLoggerFrame.LoggerKeeper.Logger;
end;

procedure TfrmLogger.LogRecorder_Start(html: boolean);
var
  output: ILogOutput;
  dir: string;
begin
  Assert(FLogOutputWriter = nil);
  FLogOutput.Clear;
  output := TStringsLogOutput.Create(FLogOutput);
  dir := Redirection.GetDirectory(sHtmlTemplatesDirAlias);
  if html then
    FLogOutputWriter := THtmlUserLogWriter.Create(output, dir, Application.Title + ' log' )
  else
    FLogOutputWriter := TPlainTextUserLogWriter.Create(output);
  FLoggerFrame.LoggerKeeper.StatefulLogger.Advise( FLogOutputWriter );
end;

function TfrmLogger.LogRecorder_Stop: string;
begin
  FLoggerFrame.LoggerKeeper.StatefulLogger.UnAdvise(FLogOutputWriter);
  FLogOutputWriter := nil;
  Result := FLogOutput.Text;
end;

procedure TfrmLogger.SaveLogArchiveTo(filename: string);
begin
  Assert(FLoggerFrame <> nil);
  FLoggerFrame.SaveArchiveTo(filename);
end;

end.
