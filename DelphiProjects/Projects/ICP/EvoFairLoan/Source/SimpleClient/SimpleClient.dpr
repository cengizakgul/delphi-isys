program SimpleClient;

uses
  Forms,
  evoapiconnection in '..\..\..\common\evoAPIConnection.pas',
  Main in 'Main.pas' {Form1},
  FairLoanAPI in '..\Service\FairLoanAPI.pas',
  EvoFairLoanServerMod in '..\Service\EvoFairLoanServerMod.pas',
  Log in 'Log.pas' {frmLogger};

{$R *.res}

begin
  LicenseKey := 'B9C39C8BA053485C98499909772470EE'; //EvoPayEarly
//  LicenseKey := 'DF279E6922C54D2B801540348F51EAC0' //EvoFairLoan
  
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfrmLogger, frmLogger);
  Application.Run;
end.
