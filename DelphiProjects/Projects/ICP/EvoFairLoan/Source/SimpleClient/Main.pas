unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, EvoFairLoanServerMod, ExtCtrls, IdIOHandler,
  IdIOHandlerSocket, IdSSLOpenSSL, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, IniFiles, ComCtrls, IdAuthentication;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    MethodComboBox: TComboBox;
    Button1: TButton;
    CheckBox1: TCheckBox;
    HTTP: TIdHTTP;
    IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket;
    Panel1: TPanel;
    pnGetEmployeeInfoBySSN: TPanel;
    Label1: TLabel;
    GEI_EmployeeSSN: TEdit;
    pnGetEmployeeInfoByUID: TPanel;
    GEI_EmployeeUID: TEdit;
    Label2: TLabel;
    GEI_IncludePastEmployment: TCheckBox;
    pnCreateDirectDiposit: TPanel;
    CDD_EmployeeSSN: TEdit;
    Label3: TLabel;
    CDD_EmployerUID: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    CDD_RoutingNumber: TEdit;
    Label6: TLabel;
    CDD_AccountNumber: TEdit;
    Label7: TLabel;
    CDD_LoanID: TEdit;
    Label8: TLabel;
    CDD_PaymentID: TEdit;
    CDD_Amount: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    pnGetLoanPaymentInfo: TPanel;
    Label11: TLabel;
    GLP_LoanId: TEdit;
    Label12: TLabel;
    GLP_PaymentID: TEdit;
    GLP_Posted: TCheckBox;
    CDD_PaydDate: TDateTimePicker;
    ServerAddress: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    ServerPort: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    UserName: TEdit;
    Password: TEdit;
    Label18: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure MethodComboBoxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure HTTPAuthorization(Sender: TObject;
      Authentication: TIdAuthentication; var Handled: Boolean);
  private
    FIniFile: TIniFile;
    procedure DisplayEmployeeInfoBySSN;
    procedure DisplayEmployeeInfoByUID;
    procedure DisplayCreateDirectDepositResponse;
    procedure DisplayLoanPayments;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses FairLoanAPI, IdCustomHTTPServer, Log;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Button1.Enabled := False;
  Screen.Cursor := crHourGlass;
  try
    Memo1.Clear;
//    HTTP.Request.Username := UserName.Text;
//    HTTP.Request.Password := Password.Text;
    case MethodComboBox.ItemIndex of
      0: DisplayEmployeeInfoBySSN;
      1: DisplayEmployeeInfoByUID;
      2: DisplayCreateDirectDepositResponse;
      3: DisplayLoanPayments;
    end;
  finally
    Button1.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

procedure TForm1.DisplayEmployeeInfoBySSN;
var
  Request: TGetEmployeeInfo_POST;
  RequestStream: TStringStream;
  ResponseStream: TStringStream;
  Json: TStringList;
  Server: TEvoFairLoanServer;
  AResponseInfo: TIdHTTPResponseInfo;
begin
  Request.SSN := GEI_EmployeeSSN.Text;
  Json := TStringList.Create;
  try
    if CheckBox1.Checked then
    begin
      HTTP.Host := ServerAddress.Text;
      HTTP.Port := StrToInt(ServerPort.Text);
      RequestStream := TStringStream.Create(FormatGetEmployeeInfoBySSNRequest(Request));
      ResponseStream := TStringStream.Create('');
      try
        HTTP.DoRequest(hmPost, 'https://' + HTTP.Host + ':' + IntToStr(HTTP.Port) + '/api/v1/GetEmployeeInfo.json', RequestStream, ResponseStream );
        ResponseStream.Position := 0;
        Memo1.Lines.LoadFromStream(ResponseStream);
      except
        on E: EIdHTTPProtocolException do
        begin
          Memo1.Lines.Add(HTTP.Response.ResponseText);
        end;
        on E: Exception do
        begin
          Memo1.Lines.Add(E.ClassName + ': ' + E.Message);
        end;
      end;
      RequestStream.Free;
      ResponseStream.Free;
    end else
    begin
      Json.Text := FormatGetEmployeeInfoBySSNRequest(Request);
      Server:= TEvoFairLoanServer.Create(frmLogger.Logger);
      AResponseInfo := TIdHTTPResponseInfo.Create(nil);
      try
        Server.ProcessRequest('GetEmployeeInfo', 'POST', JSon.Text, AResponseInfo);
        Memo1.Lines.Add(AResponseInfo.ContentText);
      finally
        AResponseInfo.Free;
        Server.Free;
      end;
    end;
  finally
    Json.Free;
  end;
end;

procedure TForm1.DisplayEmployeeInfoByUID;
var
  Server: TEvoFairLoanServer;
  Request: TGetEmployeeInfo_GET;
  Req: string;
  AResponseInfo: TIdHTTPResponseInfo;
  Json: TMemoryStream;
begin
  Request.EmployeeID := GEI_EmployeeUID.Text;
  Request.IncludePastEmployment := GEI_IncludePastEmployment.Checked;
  Req := 'employee_id=' + Request.EmployeeID;
  if Request.IncludePastEmployment then
    Req := Req + '&include_past_employment=yes';

  Json := TMemoryStream.Create;
  try
    if CheckBox1.Checked then
    begin
      HTTP.Host := ServerAddress.Text;
      HTTP.Port := StrToInt(ServerPort.Text);
      try
        HTTP.Get('https://' + HTTP.Host + ':' + IntToStr(HTTP.Port) + '/api/v1/GetEmployeeInfo.json?'+ Req, Json);
        Memo1.Lines.LoadFromStream(Json);
      except
        on E: EIdHTTPProtocolException do
        begin
          Memo1.Lines.Add(HTTP.Response.ResponseText);
        end;
        on E: Exception do
        begin
          Memo1.Lines.Add(E.ClassName + ': ' + E.Message);
        end;
      end;
    end else
    begin
      Server:= TEvoFairLoanServer.Create(frmLogger.Logger);
      AResponseInfo := TIdHTTPResponseInfo.Create(nil);
      try
        Server.ProcessRequest('GetEmployeeInfo', 'GET', Req, AResponseInfo);
        Memo1.Lines.Add(AResponseInfo.ContentText);
      finally
        AResponseInfo.Free;
        Server.Free;
      end;
    end;
  finally
    Json.Free;
  end;
end;

procedure TForm1.DisplayLoanPayments;
var
  Server: TEvoFairLoanServer;
  Request: TGetLoanPaymentInfo_GET;
  Req: string;
  AResponseInfo: TIdHTTPResponseInfo;
  Json: TMemoryStream;
begin
  Request.LoanID := GLP_LoanId.Text;
  Request.PaymentID := GLP_PaymentID.Text;
  Request.Posted := GLP_Posted.Checked;

  Req := 'loan_id=' + Request.LoanID + '&payment_id=' + Request.PaymentID;
  if Request.Posted then
    Req := Req + '&posted=yes';

  Json := TMemoryStream.Create;
  try
    if CheckBox1.Checked then
    begin
      HTTP.Host := ServerAddress.Text;
      HTTP.Port := StrToInt(ServerPort.Text);
      try
        HTTP.Get('https://' + HTTP.Host + ':' + IntToStr(HTTP.Port) + '/api/v1/GetLoanPaymentInfo.json?'+ Req, Json);
        Memo1.Lines.LoadFromStream(Json);
      except
        on E: EIdHTTPProtocolException do
        begin
          Memo1.Lines.Add(HTTP.Response.ResponseText);
        end;
        on E: Exception do
        begin
          Memo1.Lines.Add(E.ClassName + ': ' + E.Message);
        end;
      end;
    end else
    begin
      Server:= TEvoFairLoanServer.Create(frmLogger.Logger);
      AResponseInfo := TIdHTTPResponseInfo.Create(nil);
      try
        Server.ProcessRequest('GetLoanPaymentInfo', 'GET', Req, AResponseInfo);
        Memo1.Lines.Add(AResponseInfo.ContentText);
      finally
        AResponseInfo.Free;
        Server.Free;
      end;
    end;
  finally
    Json.Free;
  end;
end;

procedure TForm1.DisplayCreateDirectDepositResponse;
var
  Server: TEvoFairLoanServer;
  Request: TCreateDirectDeposit_POST;
  JSon: TStringList;
  AResponseInfo: TIdHTTPResponseInfo;
begin
  Request.EmployerID := CDD_EmployerUID.Text;
  Request.EmployeeSSN := CDD_EmployeeSSN.Text;
  Request.RoutingNumber := CDD_RoutingNumber.Text;
  Request.AccountNumber := CDD_AccountNumber.Text;
  Request.LoanID := CDD_LoanID.Text;
  Request.PaymentID := CDD_PaymentID.Text;
  Request.Amount := StrToFloat(CDD_Amount.Text);
  Request.PaidDate := CDD_PaydDate.Date;

  Json := TStringList.Create;
  try
    Json.Text := FormatCreateDirectDepositeRequest(Request);
    if CheckBox1.Checked then
    begin
      HTTP.Host := ServerAddress.Text;
      HTTP.Port := StrToInt(ServerPort.Text);
      try
        Memo1.Lines.Add(HTTP.Post('https://' + HTTP.Host + ':' + IntToStr(HTTP.Port) + '/api/v1/CreateDirectDeposit.json', Json));
      except
        on E: EIdHTTPProtocolException do
        begin
          Memo1.Lines.Add(HTTP.Response.ResponseText);
        end;
        on E: Exception do
        begin
          Memo1.Lines.Add(E.ClassName + ': ' + E.Message);
        end;      
      end;
    end else
    begin
      Server:= TEvoFairLoanServer.Create(frmLogger.Logger);
      AResponseInfo := TIdHTTPResponseInfo.Create(nil);
      try
        Server.ProcessRequest('CreateDirectDeposit', 'POST', JSon.Text, AResponseInfo);
        Memo1.Lines.Add(AResponseInfo.ContentText);
      finally
        AResponseInfo.Free;
        Server.Free;
      end;
    end;
  finally
    Json.Free;
  end;
end;

procedure TForm1.MethodComboBoxChange(Sender: TObject);
begin
  pnGetEmployeeInfoBySSN.Visible := False;
  pnGetEmployeeInfoByUID.Visible := False;
  pnCreateDirectDiposit.Visible := False;
  pnGetLoanPaymentInfo.Visible := False;
  case MethodComboBox.ItemIndex of
    0: pnGetEmployeeInfoBySSN.Visible := True;
    1: pnGetEmployeeInfoByUID.Visible := True;
    2: pnCreateDirectDiposit.Visible := True;
    3: pnGetLoanPaymentInfo.Visible := True;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ServerConfig.Load;
  FIniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + '/Config.ini');
  ServerAddress.Text := FIniFile.ReadString('Server', 'Address', '127.0.0.1');
  ServerPort.Text := FIniFile.ReadString('Server', 'Port', '80');
  UserName.Text := FIniFile.ReadString('Server', 'UserName', '');
  Password.Text := FIniFile.ReadString('Server', 'Password', '');

  GEI_EmployeeSSN.Text := FIniFile.ReadString('GetEmployeeInfo', 'EmployeeSSN', '555-12-456');
  GEI_EmployeeUID.Text := FIniFile.ReadString('GetEmployeeInfo', 'EmployeeUID', 'C1|E1');
  GEI_IncludePastEmployment.Checked := FIniFile.ReadBool('GetEmployeeInfo', 'IncludePastEmployment', False);

  CDD_EmployerUID.Text := FIniFile.ReadString('CreateDirectDeposit', 'EmployerUID', 'C1|E1');
  CDD_EmployeeSSN.Text := FIniFile.ReadString('CreateDirectDeposit', 'EmployeeSSN', '555-12-456');
  CDD_RoutingNumber.Text := FIniFile.ReadString('CreateDirectDeposit', 'RoutingNumber', '6455332455');
  CDD_AccountNumber.Text := FIniFile.ReadString('CreateDirectDeposit', 'AccountNumber', '7766533345');
  CDD_LoanID.Text := FIniFile.ReadString('CreateDirectDeposit', 'LoanID', 'CO20/X001');
  CDD_PaymentID.Text := FIniFile.ReadString('CreateDirectDeposit', 'PaymentID', '1');
  CDD_Amount.Text := FIniFile.ReadString('CreateDirectDeposit', 'Amount', '200');
  CDD_PaydDate.Date := FIniFile.ReadDate('CreateDirectDeposit', 'PaidDate', Now);

  GLP_LoanId.Text := FIniFile.ReadString('GetLoanPaiments', 'LoanID', 'CO20/X001');
  GLP_PaymentID.Text := FIniFile.ReadString('GetLoanPaiments', 'PaymentID', '1');
  GLP_Posted.Checked := FIniFile.ReadBool('GetLoanPaiments', 'Posted', False);
  MethodComboBox.ItemIndex := 0;
  MethodComboBoxChange(Self);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FIniFile.WriteString('Server', 'Address', ServerAddress.Text);
  FIniFile.WriteInteger('Server', 'Port', StrToInt(ServerPort.Text));
  FIniFile.WriteString('Server', 'UserName', UserName.Text);
  FIniFile.WriteString('Server', 'Password', Password.Text);

  FIniFile.WriteString('GetEmployeeInfo', 'EmployeeSSN', GEI_EmployeeSSN.Text);
  FIniFile.WriteString('GetEmployeeInfo', 'EmployeeUID', GEI_EmployeeUID.Text);
  FIniFile.WriteBool('GetEmployeeInfo', 'IncludePastEmployment', GEI_IncludePastEmployment.Checked);

  FIniFile.WriteString('CreateDirectDeposit', 'EmployerUID', CDD_EmployerUID.Text);
  FIniFile.WriteString('CreateDirectDeposit', 'EmployeeSSN',CDD_EmployeeSSN.Text);
  FIniFile.WriteString('CreateDirectDeposit', 'RoutingNumber', CDD_RoutingNumber.Text);
  FIniFile.WriteString('CreateDirectDeposit', 'AccountNumber', CDD_AccountNumber.Text);
  FIniFile.WriteString('CreateDirectDeposit', 'LoanID', CDD_LoanID.Text);
  FIniFile.WriteString('CreateDirectDeposit', 'PaymentID', CDD_PaymentID.Text);
  FIniFile.WriteString('CreateDirectDeposit', 'Amount', CDD_Amount.Text);
  FIniFile.WriteDate('CreateDirectDeposit', 'PaidDate', CDD_PaydDate.Date);
  FIniFile.WriteString('GetLoanPaiments', 'LoanID', GLP_LoanId.Text);
  FIniFile.WriteString('GetLoanPaiments', 'PaymentID', GLP_PaymentID.Text);
  FIniFile.WriteBool('GetLoanPaiments', 'Posted', GLP_Posted.Checked);
  FIniFile.Free;
end;

procedure TForm1.HTTPAuthorization(Sender: TObject;
  Authentication: TIdAuthentication; var Handled: Boolean);
begin
  Authentication.Username := UserName.Text;
  Authentication.Password := Password.Text;
end;

end.
