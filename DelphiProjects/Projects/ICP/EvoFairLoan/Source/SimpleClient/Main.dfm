object Form1: TForm1
  Left = 358
  Top = 114
  Width = 752
  Height = 648
  Caption = 'Evolution FairLoan Test Client'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    744
    621)
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel
    Left = 8
    Top = 8
    Width = 136
    Height = 13
    Caption = 'EvoEairLoan Server Address'
  end
  object Label14: TLabel
    Left = 184
    Top = 10
    Width = 19
    Height = 13
    Caption = 'Port'
  end
  object Label15: TLabel
    Left = 8
    Top = 93
    Width = 36
    Height = 13
    Caption = 'Method'
  end
  object Label16: TLabel
    Left = 8
    Top = 132
    Width = 91
    Height = 13
    Caption = 'Method parameters'
  end
  object Label17: TLabel
    Left = 8
    Top = 56
    Width = 53
    Height = 13
    Caption = 'User Name'
  end
  object Label18: TLabel
    Left = 120
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object Memo1: TMemo
    Left = 232
    Top = 8
    Width = 505
    Height = 599
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object MethodComboBox: TComboBox
    Left = 8
    Top = 107
    Width = 177
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnChange = MethodComboBoxChange
    Items.Strings = (
      'GetEmployeeInfo By SSN'
      'GetEmployeeInfo By UID'
      'CreateDirectDeposit'
      'GetLoanPaymentInfo')
  end
  object Button1: TButton
    Left = 184
    Top = 107
    Width = 41
    Height = 20
    Caption = 'GO'
    TabOrder = 2
    OnClick = Button1Click
  end
  object CheckBox1: TCheckBox
    Left = 176
    Top = 135
    Width = 49
    Height = 17
    Caption = 'HTTP'
    Checked = True
    State = cbChecked
    TabOrder = 3
    Visible = False
  end
  object Panel1: TPanel
    Left = 8
    Top = 152
    Width = 217
    Height = 452
    Anchors = [akLeft, akTop, akBottom]
    BevelOuter = bvNone
    TabOrder = 4
    object pnGetEmployeeInfoBySSN: TPanel
      Left = 0
      Top = 0
      Width = 217
      Height = 57
      Align = alTop
      TabOrder = 0
      Visible = False
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 22
        Height = 13
        Caption = 'SSN'
      end
      object GEI_EmployeeSSN: TEdit
        Left = 8
        Top = 24
        Width = 193
        Height = 21
        TabOrder = 0
      end
    end
    object pnGetEmployeeInfoByUID: TPanel
      Left = 0
      Top = 57
      Width = 217
      Height = 80
      Align = alTop
      TabOrder = 1
      Visible = False
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 74
        Height = 13
        Caption = 'Employee Code'
      end
      object GEI_EmployeeUID: TEdit
        Left = 8
        Top = 24
        Width = 193
        Height = 21
        TabOrder = 0
      end
      object GEI_IncludePastEmployment: TCheckBox
        Left = 8
        Top = 52
        Width = 185
        Height = 17
        Caption = 'Include Past Employment'
        TabOrder = 1
      end
    end
    object pnCreateDirectDiposit: TPanel
      Left = 0
      Top = 137
      Width = 217
      Height = 348
      Align = alTop
      TabOrder = 2
      Visible = False
      object Label3: TLabel
        Left = 8
        Top = 51
        Width = 71
        Height = 13
        Caption = 'Employee SSN'
      end
      object Label4: TLabel
        Left = 8
        Top = 8
        Width = 57
        Height = 13
        Caption = 'Employer ID'
      end
      object Label5: TLabel
        Left = 8
        Top = 96
        Width = 77
        Height = 13
        Caption = 'Routing Number'
      end
      object Label6: TLabel
        Left = 8
        Top = 136
        Width = 80
        Height = 13
        Caption = 'Account Number'
      end
      object Label7: TLabel
        Left = 8
        Top = 176
        Width = 38
        Height = 13
        Caption = 'Loan ID'
      end
      object Label8: TLabel
        Left = 8
        Top = 216
        Width = 55
        Height = 13
        Caption = 'Payment ID'
      end
      object Label9: TLabel
        Left = 8
        Top = 256
        Width = 36
        Height = 13
        Caption = 'Amount'
      end
      object Label10: TLabel
        Left = 8
        Top = 296
        Width = 47
        Height = 13
        Caption = 'Paid Date'
      end
      object CDD_EmployeeSSN: TEdit
        Left = 8
        Top = 67
        Width = 193
        Height = 21
        TabOrder = 0
      end
      object CDD_EmployerUID: TEdit
        Left = 8
        Top = 24
        Width = 193
        Height = 21
        TabOrder = 1
      end
      object CDD_RoutingNumber: TEdit
        Left = 8
        Top = 112
        Width = 193
        Height = 21
        TabOrder = 2
      end
      object CDD_AccountNumber: TEdit
        Left = 8
        Top = 152
        Width = 193
        Height = 21
        TabOrder = 3
      end
      object CDD_LoanID: TEdit
        Left = 8
        Top = 192
        Width = 193
        Height = 21
        TabOrder = 4
      end
      object CDD_PaymentID: TEdit
        Left = 8
        Top = 232
        Width = 193
        Height = 21
        TabOrder = 5
      end
      object CDD_Amount: TEdit
        Left = 8
        Top = 272
        Width = 193
        Height = 21
        TabOrder = 6
      end
      object CDD_PaydDate: TDateTimePicker
        Left = 8
        Top = 312
        Width = 193
        Height = 21
        Date = 41487.589316134260000000
        Time = 41487.589316134260000000
        TabOrder = 7
      end
    end
    object pnGetLoanPaymentInfo: TPanel
      Left = 0
      Top = 485
      Width = 217
      Height = 124
      Align = alTop
      TabOrder = 3
      Visible = False
      object Label11: TLabel
        Left = 8
        Top = 8
        Width = 38
        Height = 13
        Caption = 'Loan ID'
      end
      object Label12: TLabel
        Left = 8
        Top = 48
        Width = 55
        Height = 13
        Caption = 'Payment ID'
      end
      object GLP_LoanId: TEdit
        Left = 8
        Top = 24
        Width = 193
        Height = 21
        TabOrder = 0
      end
      object GLP_PaymentID: TEdit
        Left = 8
        Top = 64
        Width = 193
        Height = 21
        TabOrder = 1
      end
      object GLP_Posted: TCheckBox
        Left = 8
        Top = 92
        Width = 185
        Height = 17
        Caption = 'Posted'
        TabOrder = 2
      end
    end
  end
  object ServerAddress: TEdit
    Left = 8
    Top = 24
    Width = 169
    Height = 21
    TabOrder = 5
    Text = '127.0.0.1'
  end
  object ServerPort: TEdit
    Left = 184
    Top = 24
    Width = 41
    Height = 21
    TabOrder = 6
  end
  object UserName: TEdit
    Left = 8
    Top = 72
    Width = 105
    Height = 21
    TabOrder = 7
  end
  object Password: TEdit
    Left = 120
    Top = 72
    Width = 105
    Height = 21
    TabOrder = 8
  end
  object HTTP: TIdHTTP
    IOHandler = IdSSLIOHandlerSocket1
    MaxLineAction = maException
    Host = 'localhost'
    Port = 81
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = 0
    Request.ContentRangeStart = 0
    Request.ContentType = 'application/json'
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    HTTPOptions = [hoForceEncodeParams]
    OnAuthorization = HTTPAuthorization
    Left = 400
    Top = 112
  end
  object IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket
    SSLOptions.Method = sslvSSLv23
    SSLOptions.Mode = sslmClient
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 440
    Top = 112
  end
end
