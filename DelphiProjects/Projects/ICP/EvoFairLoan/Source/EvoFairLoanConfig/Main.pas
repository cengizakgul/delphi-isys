unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IniFiles, StdCtrls;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    Button2: TButton;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Edit4: TEdit;
    CheckBox1: TCheckBox;
    GroupBox3: TGroupBox;
    IPWitelist: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FIniFile: TIniFile;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FIniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + '\EvoFairLoan.cfg');
  Edit1.Text :=  FIniFile.ReadString('EvoAPIConnection', 'APIServerAddress', 'localhost');
  Edit2.Text :=  FIniFile.ReadString('EvoAPIConnection', 'Username', '');
  Edit3.Text :=  FIniFile.ReadString('EvoAPIConnection', 'Password', '');
  Edit4.Text :=  IntToStr(FIniFile.ReadInteger('Server', 'HTTP_Port', 80));
  Checkbox1.Checked := FIniFile.ReadBool('Server', 'ReadableOutput', True);

  FIniFile.ReadSection('Whitelist', IPWitelist.Lines);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  FIniFile.WriteString('EvoAPIConnection', 'APIServerAddress', Edit1.Text);
  FIniFile.WriteString('EvoAPIConnection', 'Username', Edit2.Text);
  FIniFile.WriteString('EvoAPIConnection', 'Password', Edit3.Text);
  FIniFile.WriteInteger('Server', 'HTTP_Port', StrToInt(Edit4.Text));
  FIniFile.WriteBool('Server', 'ReadableOutput', Checkbox1.Checked);
  FIniFile.EraseSection('Whitelist');
  for i := 0 to IPWitelist.Lines.Count - 1 do
    FIniFile.WriteString ('Whitelist', IPWitelist.Lines[i], '1');
  ShowMessage('Server configurtion has been changed.'#13 +
              'Restart Evolution FairLoan Service that'#13 +
              'the new settings take effect.');
  Close;
end;

end.
