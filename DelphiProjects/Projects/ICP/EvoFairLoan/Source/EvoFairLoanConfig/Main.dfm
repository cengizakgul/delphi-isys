object Form1: TForm1
  Left = 302
  Top = 120
  Width = 484
  Height = 378
  Caption = 'Evo FairLoan Configuration'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 16
    Top = 16
    Width = 257
    Height = 129
    Caption = 'Evo API Connection'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 26
      Width = 92
      Height = 13
      Caption = 'API Server Address'
    end
    object Label2: TLabel
      Left = 8
      Top = 58
      Width = 95
      Height = 13
      Caption = 'Evolution Username'
    end
    object Label3: TLabel
      Left = 8
      Top = 90
      Width = 93
      Height = 13
      Caption = 'Evolution Password'
    end
    object Edit1: TEdit
      Left = 112
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Edit1'
    end
    object Edit2: TEdit
      Left = 112
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit2'
    end
    object Edit3: TEdit
      Left = 112
      Top = 88
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
      Text = 'Edit3'
    end
  end
  object Button1: TButton
    Left = 144
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 232
    Top = 296
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Button2Click
  end
  object GroupBox2: TGroupBox
    Left = 16
    Top = 160
    Width = 257
    Height = 105
    Caption = 'Server Parameters'
    TabOrder = 3
    object Label4: TLabel
      Left = 8
      Top = 24
      Width = 51
      Height = 13
      Caption = 'HTTP Port'
    end
    object Edit4: TEdit
      Left = 112
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Edit4'
    end
    object CheckBox1: TCheckBox
      Left = 112
      Top = 56
      Width = 113
      Height = 17
      Caption = 'Readable Output'
      TabOrder = 1
    end
  end
  object GroupBox3: TGroupBox
    Left = 280
    Top = 16
    Width = 186
    Height = 249
    Caption = 'IP Witelist'
    TabOrder = 4
    object IPWitelist: TMemo
      Left = 8
      Top = 24
      Width = 169
      Height = 217
      Lines.Strings = (
        'Memo1')
      TabOrder = 0
    end
  end
end
