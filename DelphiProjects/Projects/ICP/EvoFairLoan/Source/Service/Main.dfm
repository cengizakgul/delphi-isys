object EvoFairLoanService: TEvoFairLoanService
  OldCreateOrder = False
  AllowPause = False
  DisplayName = 'Evolution FairLoan Service'
  OnStart = ServiceStart
  OnStop = ServiceStop
  Left = 378
  Top = 187
  Height = 262
  Width = 413
  object HTTPServer: TIdHTTPServer
    Bindings = <>
    CommandHandlers = <>
    DefaultPort = 81
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    OnCommandOther = HTTPServerCommandOther
    OnCommandGet = HTTPServerCommandGet
    Left = 40
    Top = 8
  end
  object HandlerSSL: TIdServerIOHandlerSSL
    SSLOptions.Method = sslvSSLv23
    SSLOptions.Mode = sslmServer
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 2
    Left = 112
    Top = 8
  end
end
