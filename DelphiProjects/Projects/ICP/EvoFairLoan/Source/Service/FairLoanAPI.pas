unit FairLoanAPI;

interface

type
  TGetEmployeeInfo_GET = record
    EmployeeID: string;
    IncludePastEmployment: Boolean;
  end;

  TGetEmployeeInfo_POST = record
    SSN: string;
  end;

  TCreateDirectDeposit_POST = record
    EmployerID: string;
    EmployeeSSN: string;
    RoutingNumber: string;
    AccountNumber: string;
    LoanID: string;
    PaymentID: string;
    Amount: Real;
    PaidDate: TDateTime;
  end;

  TGetLoanPaymentInfo_GET = record
    LoanID: string;
    PaymentID: string;
    Posted: Boolean;
  end;

  TAddressInfo = record
    Street: string;
    City: string;
    State: string;
    Zip: string;
  end;

  TPaymentInfo = record
    GrossWages: Real;
    RegularAmount: Real;
    TipsAmount: Real;
    BonusAmount: Real;
    OvertimeAmount: Real;
    Hours: Real;
    Garnishments: Real;
    PreTaxDeduction: Real;
    AfterTaxDeduction: Real;
    NetWages: Real;
    CheckDate: TDateTime;
    IsDirectDeposit: Boolean;
    Misc: Real;
  end;

  TPaymentsArray = array of TPaymentInfo;

  TEmploymentInfo = record
    EmployeeID: string;
    EmpoloyerID: string;
    EmployerTaxID: string;
    EmployerAddress: TAddressInfo;
    EmployeeIsActive: Boolean;
    HireDate: TDateTime;
    TerminationDate: TDateTime;
    RehireDate: TDateTime;
    PayRate: Real;
    PayType: string;
    EarningsToDate: Real;
    PayFrequency: string;
    LastPayDate: TDateTime;
    NextPayDate: TDateTime;
    Payments: TPaymentsArray;
  end;

  TEmploymentsArray = array of TEmploymentInfo;

  TEmployeeInfo = record
    FirstName: string;
    LastName: string;
    SSN: string;
    BirthDate: TDateTime;
    ContactNumber: string;
    Address: TAddressInfo;
    EMailAddress: string;
    Employments: TEmploymentsArray;
  end;

  TEmployeeInfoArray = array of TEmployeeInfo;

  TCreateDirectDepositeResult = record
    SheduledPayDate: TDateTime;
  end;

  TLoanPaymentInfo = record
    LoanID: string;
    PaymentID: string;
    PaymentDate: TDateTime;
    Amount: Real;
  end;

  TLoanPaymentsArray = array of TLoanPaymentInfo;

procedure SetReadableOutput(Value: Boolean);

function FormatEmployeeInfo(AEmployeeInfo: TEmployeeInfo): string;

function FormatAddressInfo(AAddressInfo: TAddressInfo): string;

function FormatEmploymentInfo(AEmploymentInfo: TEmploymentInfo): string;

function FormatPaymentInfo(APaymentInfo: TPaymentInfo): string;

function FormatCreateDirectDepositeResult(ACreateDirectDepositeResult: TCreateDirectDepositeResult): string;

function FormatCreateDirectDepositeRequest(ACreateDirectDeposit_POST: TCreateDirectDeposit_POST): string;

function FormatGetEmployeeInfoBySSNRequest(AGetEmployeeInfo_POST: TGetEmployeeInfo_POST): string;

function FormatEmployeeInfoArray(AEmployeeInfoArray: TEmployeeInfoArray): string;

function FormatGetEmployeeInfoResult(AEmployeeInfoArray: TEmployeeInfoArray): string;

function FormatEmploymentsArray(AEmploymentsArray: TEmploymentsArray): string;

function FormatPaymentsArray(APaymentsArray: TPaymentsArray): string;

function FormatLoanPaymentsArray(ALoanPaymentsArray: TLoanPaymentsArray): string;

function FormatLoanPaymentInfo(ALoanPaymentInfo: TLoanPaymentInfo): string;

function FormatLoanPaymentsResponse(ALoanPaymentsArray: TLoanPaymentsArray): string;

function FormatCreateDirectDepositeResponse(ACreateDirectDepositeResult: TCreateDirectDepositeResult): string;

function ParseGetEmployeeInfoRequest(const JsonText: string): TGetEmployeeInfo_POST;

function ParseCreateDirectDepositRequest(const JsonText: string): TCreateDirectDeposit_POST;


implementation

uses
  SysUtils, StrUtils;

var
  ReadableOutput: Boolean;
  IndentLevel: Integer;
  IndentStr: string;
  Space: string;
  EndOfLine: string;

procedure SetReadableOutput(Value: Boolean);
begin
  ReadableOutput := Value;
  if ReadableOutput then
  begin
    EndOfLine := #13#10 ;
    Space := ' '
  end else
  begin
    EndOfLine := '';
    Space := '';
  end;
  IndentLevel := 0;
  IndentStr := '';
end;

procedure IncIndent;
var
  i: Integer;
begin
  if ReadableOutput then
  begin
    IndentStr := '';
    IndentLevel := IndentLevel + 2;
    for i := 1 to IndentLevel do
      IndentStr := IndentStr + ' ';
  end;
end;

procedure DecIndent;
var
  i: Integer;
begin
  if ReadableOutput then
  begin
    IndentStr := '';
    IndentLevel := IndentLevel - 2;
    for i := 1 to IndentLevel do
      IndentStr := IndentStr + ' ';
  end;
end;

function DateField(const Name: string; Value: TDateTime): string;
begin
  Result := '"' + Name + '":' + Space + '"';
  if Value <> 0 then
    Result := Result + FormatDateTime('yyyy-mm-dd', Value);
  Result := Result + '"';
end;

function DateTimeField(const Name: string; Value: TDateTime): string;
begin
  Result := '"' + Name + '":' + Space + '"' + FormatDateTime('yyyy-mm-dd', Value)+ 'T' +
  FormatDateTime('hh:mm:nn', Value) + 'Z' + '"';
end;

function BoolField(const Name: string; Value: Boolean): string;
var
  YesOrNo: string;
begin
  if Value then
    YesOrNo := 'yes'
  else
    YesOrNo := 'no';
  Result := '"' + Name + '":' + Space + '"' + YesOrNo + '"';
end;

function StringField(const Name: string; const Value: string): string;
begin
  Result := '"' + Name + '":' + Space + '"' + Value + '"';
end;

function IntegerField(const Name: string; const Value: Integer): string;
begin
  Result := '"' + Name + '":' + Space + '"' + IntToStr(Value) + '"';
end;

function RealField(const Name: string; const Value: Real): string;
begin
  Result := '"' + Name + '":' + Space + '"' + FloatToStr(Value) + '"';
end;

function CurrField(const Name: string; const Value: Currency): string;
begin
  Result := '"' + Name + '":' + Space + '"' + Format('%.2f', [Value]) + '"';
end;

function AddField(const AText, Value: string): string;
begin
  if AText = '' then
    Result := IndentStr + Value
  else
    Result := AText + ',' + EndOfLine + IndentStr + Value;
end;

function AddRecord(const AText, Name: string): string;
begin
  if AText = '' then
    Result := IndentStr + '"' + Name + '":' + Space + '{' + EndOfLine
  else
    Result := AText + ',' + EndOfLine + IndentStr + '"' + Name + '":' + Space + '{' + EndOfLine;
  IncIndent;
end;

function AddRecordContent(const AText, Content: string): string;
begin
  Result := AText + Content + EndOfLine;
  DecIndent;
  Result := Result + IndentStr + '}';
end;

function AddArray(const AText, Name: string): string;
begin
  if AText = '' then
    Result := IndentStr + '"' + Name + '":' + Space + '{' + EndOfLine
  else
    Result := AText + ',' + EndOfLine + IndentStr + '"' + Name + '":' + Space + '[' + EndOfLine;
  IncIndent;
end;

function AddArrayContent(const AText, Content: string): string;
begin
  Result := AText + Content + EndOfLine;
  DecIndent;
  Result := Result + IndentStr + ']';
end;

function FormatEmploymentsArray(AEmploymentsArray: TEmploymentsArray): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(AEmploymentsArray) - 1 do
  begin
    if Result <> '' then
      Result := Result + ',' + EndOfLine;
    Result := Result + IndentStr + '{' + EndOfLine;
    IncIndent;
    Result := AddRecordContent(Result, FormatEmploymentInfo(AEmploymentsArray[i]));
  end;
end;

function FormatPaymentsArray(APaymentsArray: TPaymentsArray): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(APaymentsArray) - 1 do
  begin
    if Result <> '' then
      Result := Result + ',' + EndOfLine;
    Result := Result + IndentStr + '{' + EndOfLine;
    IncIndent;
    Result := AddRecordContent(Result, FormatPaymentInfo(APaymentsArray[i]));
  end;
end;

function FormatEmployeeInfoArray(AEmployeeInfoArray: TEmployeeInfoArray): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(AEmployeeInfoArray) - 1 do
  begin
    if Result <> '' then
      Result := Result + ',' + EndOfLine;
    Result := Result + IndentStr + '{' + EndOfLine;
    IncIndent;
    Result := AddRecordContent(Result, FormatEmployeeInfo(AEmployeeInfoArray[i]));
  end;
end;

function FormatEmployeeInfo(AEmployeeInfo: TEmployeeInfo): string;
begin
  IncIndent;
  Result := '';
  Result := AddField(Result, StringField('first_name', AEmployeeInfo.FirstName));
  Result := AddField(Result, StringField('last_name', AEmployeeInfo.LastName));
  Result := AddField(Result, StringField('ssn', AEmployeeInfo.SSN));
  Result := AddField(Result, DateField('birth_date', AEmployeeInfo.BirthDate));
  Result := AddField(Result, StringField('contact_number', AEmployeeInfo.ContactNumber));
  Result := AddField(Result, StringField('e_mail_address', AEmployeeInfo.EMailAddress));
  Result := AddRecord(Result, 'address');
  Result := AddRecordContent(Result, FormatAddressInfo(AEmployeeInfo.Address));
  Result := AddArray(Result, 'employments');
  Result := AddArrayContent(Result, FormatEmploymentsArray(AEmployeeInfo.Employments));
end;

function FormatAddressInfo(AAddressInfo: TAddressInfo): string;
begin
  Result := '';
  Result := AddField(Result, StringField('street', AAddressInfo.Street));
  Result := AddField(Result, StringField('city', AAddressInfo.City));
  Result := AddField(Result, StringField('state', AAddressInfo.State));
  Result := AddField(Result, StringField('zip', AAddressInfo.Zip));
end;

function FormatEmploymentInfo(AEmploymentInfo: TEmploymentInfo): string;
begin
  Result := '';
  Result := AddField(Result, StringField('employee_id', AEmploymentInfo.EmployeeID));
  Result := AddField(Result, StringField('employer_id', AEmploymentInfo.EmpoloyerID));
  Result := AddField(Result, StringField('employer_tax_id', AEmploymentInfo.EmployerTaxID));
  Result := AddRecord(Result, 'address');
  Result := AddRecordContent(Result, FormatAddressInfo(AEmploymentInfo.EmployerAddress));
  Result := AddField(Result, BoolField('employee_is_active', AEmploymentInfo.EmployeeIsActive));
  Result := AddField(Result, DateField('hire_date', AEmploymentInfo.HireDate));
  Result := AddField(Result, DateField('termination_date', AEmploymentInfo.TerminationDate));
  Result := AddField(Result, DateField('rehire_date', AEmploymentInfo.RehireDate));
  Result := AddField(Result, RealField('pay_rate', AEmploymentInfo.PayRate));
  Result := AddField(Result, StringField('pay_type', AEmploymentInfo.PayType));
  Result := AddField(Result, CurrField('earnings_to_date', AEmploymentInfo.EarningsToDate));
  Result := AddField(Result, StringField('pay_frequency', AEmploymentInfo.PayFrequency));
  Result := AddField(Result, DateField('last_pay_date', AEmploymentInfo.LastPayDate));
  Result := AddField(Result, DateField('next_pay_date', AEmploymentInfo.NextPayDate));
  Result := AddArray(Result, 'payments');
  Result := AddArrayContent(Result, FormatPaymentsArray(AEmploymentInfo.Payments));
end;

function FormatPaymentInfo(APaymentInfo: TPaymentInfo): string;
begin
  Result := '';
  Result := AddField(Result, CurrField('gross_wages', APaymentInfo.GrossWages));
  Result := AddField(Result, CurrField('regular_amount', APaymentInfo.RegularAmount));
  Result := AddField(Result, CurrField('tips_amount', APaymentInfo.TipsAmount));
  Result := AddField(Result, CurrField('bonus_amount', APaymentInfo.BonusAmount));
  Result := AddField(Result, CurrField('overtime_amount', APaymentInfo.OvertimeAmount));
  Result := AddField(Result, RealField('hours', APaymentInfo.Hours));
  Result := AddField(Result, CurrField('pre_tax_deductions', APaymentInfo.PreTaxDeduction));
  Result := AddField(Result, CurrField('after_tax_deductions', APaymentInfo.AfterTaxDeduction));
  Result := AddField(Result, CurrField('net_wages', APaymentInfo.NetWages));
  Result := AddField(Result, DateField('check_date', APaymentInfo.CheckDate));
  Result := AddField(Result, BoolField('is_direct_deposite', APaymentInfo.IsDirectDeposit));
  Result := AddField(Result, RealField('bonus_amount', APaymentInfo.Misc));
end;

function FormatCreateDirectDeposit_POST(ACreateDirectDeposit_POST: TCreateDirectDeposit_POST): string;
begin
  Result := '';
  Result := AddField(Result, StringField('employer_id', ACreateDirectDeposit_POST.EmployerID));
  Result := AddField(Result, StringField('employee_ssn', ACreateDirectDeposit_POST.EmployeeSSN));
  Result := AddField(Result, StringField('routing_number', ACreateDirectDeposit_POST.RoutingNumber));
  Result := AddField(Result, StringField('account_number', ACreateDirectDeposit_POST.AccountNumber));
  Result := AddField(Result, StringField('loan_id', ACreateDirectDeposit_POST.LoanID));
  Result := AddField(Result, StringField('payment_id', ACreateDirectDeposit_POST.PaymentID));
  Result := AddField(Result, CurrField('amount', ACreateDirectDeposit_POST.Amount));
  Result := AddField(Result, DateField('paid_date', ACreateDirectDeposit_POST.PaidDate));
end;

function FormatGetEmployeeInfoBySSNRequest(AGetEmployeeInfo_POST: TGetEmployeeInfo_POST): string;
var
  Body: string;
begin
  Result := Result + IndentStr + '{' + EndOfLine;
  IncIndent;
  Body := AddField(Body, StringField('ssn', AGetEmployeeInfo_POST.SSN));
  DecIndent;
  Result := Result + Body + IndentStr + '}' + EndOfLine;
end;

function FormatCreateDirectDepositeRequest(ACreateDirectDeposit_POST: TCreateDirectDeposit_POST): string;
begin
  Result := Result + IndentStr + '{' + EndOfLine;
  IncIndent;
  Result := AddRecordContent(Result, FormatCreateDirectDeposit_POST(ACreateDirectDeposit_POST));
end;

function FormatCreateDirectDepositeResult(ACreateDirectDepositeResult: TCreateDirectDepositeResult): string;
begin
  Result := '';
  Result := AddField(Result, DateTimeField('scheduled_pay_date', ACreateDirectDepositeResult.SheduledPayDate));
end;

function FormatGetEmployeeInfoResult(AEmployeeInfoArray: TEmployeeInfoArray): string;
begin
  Result := IndentStr + '[' + EndOfLine + FormatEmployeeInfoArray(AEmployeeInfoArray) +
    IndentStr + '[' + EndOfLine;
end;

function FormatCreateDirectDepositeResponse(ACreateDirectDepositeResult: TCreateDirectDepositeResult): string;
begin
  Result := Result + IndentStr + '{' + EndOfLine;
  IncIndent;
  Result := AddRecordContent(Result, FormatCreateDirectDepositeResult(ACreateDirectDepositeResult));
end;

function FormatLoanPaymentsArray(ALoanPaymentsArray: TLoanPaymentsArray): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(ALoanPaymentsArray) - 1 do
  begin
    if Result <> '' then
      Result := Result + ',' + EndOfLine;
    Result := Result + IndentStr + '{' + EndOfLine;
    IncIndent;
    Result := AddRecordContent(Result, FormatLoanPaymentInfo(ALoanPaymentsArray[i]));
  end;
end;

function FormatLoanPaymentInfo(ALoanPaymentInfo: TLoanPaymentInfo): string;
begin
  Result := '';
  Result := AddField(Result, StringField('loan_id', ALoanPaymentInfo.LoanID));
  Result := AddField(Result, StringField('payment_id', ALoanPaymentInfo.PaymentID));
  Result := AddField(Result, DateField('payment_date', ALoanPaymentInfo.PaymentDate));
  Result := AddField(Result, CurrField('Amount', ALoanPaymentInfo.Amount));
end;

function FormatLoanPaymentsResponse(ALoanPaymentsArray: TLoanPaymentsArray): string;
begin
  Result := IndentStr + '[' + EndOfLine;
  IncIndent;
  Result := AddArrayContent(Result, FormatLoanPaymentsArray(ALoanPaymentsArray));
end;

function FindValue(const Text, Key: string; var Value: string): Boolean;
var
  Offset: Integer;
  Len: Integer;
begin
  Offset := Pos('"' + UpperCase(Key) + '"', UpperCase(Text));
  if Offset = 0 then
  begin
    Result := False;
    Exit;
  end;
  Offset := Offset + Length(Key) + 3;
  while Text[Offset] = ' ' do
    Inc(Offset);

  if Text[Offset] <> '"' then
  begin
    Result := False;
    Exit;
  end;

  Inc(Offset);
  Len := PosEx('"', Text, Offset) - Offset;
  if Len < 0 then
  begin
    Result := False;
    Exit;
  end else if Len = 0 then
  begin
    Result := True;
    Value := '';
  end else
  begin
    Result := True;
    Value := Copy(Text, Offset, Len);
  end;
end;

function ParseGetEmployeeInfoRequest(const JsonText: string): TGetEmployeeInfo_POST;
var
  Value: string;
begin
  if FindValue(JsonText, 'ssn', Value) then
  begin
    Result.SSN := Value;
  end else
    raise Exception.Create('400 - Bad Request');
end;

function ParseDateTimeValue(const JsonText: string): TDateTime;
var
  AYear, AMonth, ADay: Word;
begin
  AYear := StrToInt(Copy(JsonText, 1, 4));
  AMonth := StrToInt(Copy(JsonText, 6, 2));
  ADay := StrToInt(Copy(JsonText, 9, 2));
  Result := EncodeDate(AYear, AMonth, ADay);
end;

function ParseCreateDirectDepositRequest(const JsonText: string): TCreateDirectDeposit_POST;
var
  Value: string;
begin
  if FindValue(JsonText, 'employer_id', Value) then
    Result.EmployerID := Value
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'employee_ssn', Value) then
    Result.EmployeeSSN := Value
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'routing_number', Value) then
    Result.RoutingNumber := Value
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'account_number', Value) then
    Result.AccountNumber := Value
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'loan_id', Value) then
    Result.LoanID := Value
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'payment_id', Value) then
    Result.PaymentID := Value
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'Amount', Value) then
    try
      Result.Amount := StrToFloat(Value);
    except
      raise Exception.Create('400 - Bad Request');
    end
  else
    raise Exception.Create('400 - Bad Request');

  if FindValue(JsonText, 'paid_date', Value) then
    try
      Result.PaidDate := ParseDateTimeValue(Value);
    except
      raise Exception.Create('400 - Bad Request');
    end
  else
    raise Exception.Create('400 - Bad Request');
end;

end.
