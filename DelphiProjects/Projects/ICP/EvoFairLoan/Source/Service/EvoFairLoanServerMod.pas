unit EvoFairLoanServerMod;

interface

uses
   XmlRpcTypes, Classes, SysUtils, IdCustomHTTPServer, kbmMemTable, DB,
   gdyCommonLogger, evoApiconnection, common, SyncObjs, FairLoanAPI;

type
  EInvalidRequestParam = class(Exception);

  TEvoFairLoanServer = class
  private
    FEvoAPI: IEvoAPIConnection;
    FLogger: ICommonLogger;
    FReadableOutput: Boolean;
    FResponseInfo: TIdHTTPResponseInfo;
    FServiceName: string;
    FCommand: string;
    FParams: string;
  protected
    //Response errors
    procedure MethodNotAllowed;
    procedure ResourceNotFound;
    procedure BadRequest(const Message: string = '');
    procedure ServerError(const Message: string);
    //API Methods
    procedure GetEmployeeInfoBySSN;
    procedure GetEmployeeInfoByUID;
    procedure CreateDirectDeposit;
    procedure GetLoanPaymentInfo;
    // Evolution data access methods
    procedure Connect;
    function RunQuery(QueryName: string; params: IRpcStruct): TkbmCustomMemTable;
    function ApplyRecordChange(var aFields: IRpcStruct; const aTableName, aType: string;
      aKey: Integer): Integer;
    function FindCompany(var aCompany: TEvoCompanyDef): Boolean;
    function QueryEmploymentInfo(ClientNbr, CompanyNbr, EmployeeNbr: Integer): TEmploymentInfo;
    procedure SetSchedDefaultED(var aFields: IRpcStruct; const EdCode: string);
  public
    constructor Create(Logger: ICommonLogger); reintroduce;
    procedure ProcessRequest(const AServiceName, ACommand, AParams: string;
      AResponseInfo: TIdHTTPResponseInfo);
    function CheckAuth(const UserName, Password: string): Boolean;
    property ReadableOutput: Boolean read FReadableOutput write FReadableOutput;
  end;

  TServerConfig = class
  private
    FEvoConnectionParam: TEvoAPIConnectionParam;
    FPort: Integer;
    FWitelist: TStringList;
    FReadableOutput: Boolean;
    FRootCertFile: string;
    FCertFile: string;
    FKeyFile: string;
    FUseSSL: Boolean;
    FUserName: string;
    FPassword: string;
    procedure SetReadableOutput(const Value: Boolean);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Load;
    function IsForbidden(IP: string): Boolean;
    property EvoConnectionParam: TEvoAPIConnectionParam read FEvoConnectionParam;
    property Port: Integer read FPort;
    property ReadableOutput: Boolean read FReadableOutput write SetReadableOutput;
    property RootCertFile: string read FRootCertFile;
    property CertFile: string read FCertFile;
    property KeyFile: string read FKeyFile;
    property UseSSL: Boolean read FUseSSL;
    property UserName: string read FUserName;
    property Password: string read FPassword;
  end;

var
  ServerConfig: TServerConfig;

  function RemoveExtension(const Value: string): string;

  function GetServiceName(const Document: string): string;


implementation

uses StrUtils, gdyRedir, Variants, Forms, IniFiles, DateUtils;

function GetServiceName(const Document: string): string;
var
  Temp: string;
  Offset: Integer;
begin
  Temp := ReverseString(Document);
  Offset := Pos('/', Temp);
  if Offset > 0 then
  begin
    Temp := Copy(Temp, 1, Offset - 1);
    Result := ReverseString(Temp);
  end;
  Result := RemoveExtension(Result);
end;

function RemoveExtension(const Value: string): string;
begin
  Result := StringReplace(Value, '.json', '', [rfIgnoreCase, rfReplaceAll]);
end;

function TEvoFairLoanServer.CheckAuth(const UserName,
  Password: string): Boolean;
begin
  Result := (UpperCase(UserName) = UpperCase(ServerConfig.Username)) and
    (Password = ServerConfig.Password);
end;

procedure TEvoFairLoanServer.ProcessRequest(const AServiceName, ACommand,
  AParams: string; AResponseInfo: TIdHTTPResponseInfo);
begin
  FServiceName := AServiceName;
  FCommand := ACommand;
  FParams := AParams;
  FResponseInfo := AResponseInfo;
  if (CompareText(FServiceName, 'GetEmployeeInfo') <> 0) and
     (CompareText(FServiceName, 'GetLoanPaymentInfo') <> 0) and
     (CompareText(FServiceName, 'CreateDirectDeposit') <> 0) then
  begin
    ResourceNotFound;
  end else
  begin
    try
      if FCommand = 'GET' then
      begin
        if CompareText(FServiceName, 'GetEmployeeInfo') = 0 then
        begin
          GetEmployeeInfoByUID;
        end else if CompareText(FServiceName, 'GetLoanPaymentInfo') = 0 then
        begin
          GetLoanPaymentInfo;
        end else
        begin
          MethodNotAllowed;
        end;
      end else if FCommand = 'POST' then
      begin
        if CompareText(FServiceName, 'GetEmployeeInfo') = 0 then
        begin
          GetEmployeeInfoBySSN;
        end else if CompareText(FServiceName, 'CreateDirectDeposit') = 0 then
        begin
          CreateDirectDeposit;
        end else
        begin
          MethodNotAllowed;
        end;
      end else
      begin
        MethodNotAllowed;
      end;
    except
      on E: EInvalidRequestParam do
        BadRequest(E.Message);
      on E:Exception do
        ServerError(E.Message);
    end
  end;
end;

procedure TEvoFairLoanServer.MethodNotAllowed;
begin
  FResponseInfo.ResponseNo := 405;
  FResponseInfo.ResponseText := 'Method Not Allowed';
end;


procedure TEvoFairLoanServer.ResourceNotFound;
begin
  FResponseInfo.ResponseNo := 404;
  FResponseInfo.ResponseText := 'Resource Not Found';
end;

procedure TEvoFairLoanServer.BadRequest(const Message: string = '');
begin
  FResponseInfo.ResponseNo := 400;
  FResponseInfo.ResponseText := 'Bad Request';
  if Message <> '' then
    FResponseInfo.ContentText := '[{"request_error":"' + Message + '"}]';
end;

procedure TEvoFairLoanServer.ServerError(const Message: string);
begin
  FResponseInfo.ResponseNo := 500;
  FResponseInfo.ResponseText := 'Server Error';
  FResponseInfo.ContentText := '[{"request_error":"' + Message + '"}]';
end;

procedure TEvoFairLoanServer.CreateDirectDeposit;
var
  Request: TCreateDirectDeposit_POST;
  Response: TCreateDirectDepositeResult;
  ClNbr: Integer;
  CoNbr: Integer;
  EeNbr: Integer;
  EmployerParams: TStringList;
  Params: IRpcStruct;
  Fields: IRpcStruct;
  qEEDirectDeposit: TkbmCustomMemTable;
  qEEScheduledEDs: TkbmCustomMemTable;
  qEmployeeBySsn: TkbmCustomMemTable;
  EEDirectDepositNbr: Integer;
  EeScheduledEDsNbr: Integer;
begin
  Request := ParseCreateDirectDepositRequest(FParams);

  EmployerParams := TStringList.Create;
  try
    EmployerParams.Delimiter := '-';
    EmployerParams.DelimitedText := Request.EmployerID;
    ClNbr := StrToInt(EmployerParams.Strings[0]);
    CoNbr := StrToInt(EmployerParams.Strings[1]);
  finally
    EmployerParams.Free;
  end;
  
  Connect;
  FEvoApi.OpenClient(ClNbr);

  Fields := TRpcStruct.Create;
  Params := TRpcStruct.Create;

  Params.AddItem( 'co_nbr', CoNbr );
  Params.AddItem( 'ssn', Request.EmployeeSSN );
  qEmployeeBySsn := RunQuery('EmployeeBySsn', Params);
  try
    EeNbr := qEmployeeBySsn.FieldByName('EE_NBR').AsInteger;
    if qEmployeeBySsn.Eof then
      raise EInvalidRequestParam.Create('Invalid employee_ssn parameter value. Employee not found.');
  finally
    qEmployeeBySsn.Free;
  end;

  Params.Clear;
  Params.AddItem( 'ee_nbr', EeNbr );
  Params.AddItem( 'routing_number', Request.RoutingNumber );
  Params.AddItem( 'account_number', Request.AccountNumber );

  qEEDirectDeposit := RunQuery('EeDirectDeposit', Params);
  try
    if not qEEDirectDeposit.Eof then
    begin
      //Direct deposit found
      EEDirectDepositNbr := qEEDirectDeposit.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger;
    end else
    begin
      //Direct deposit not found, create new
      Fields.AddItem('Ee_Nbr', EeNbr);
      Fields.AddItem('Ee_Aba_Number', Request.RoutingNumber);
      Fields.AddItem('Ee_Bank_Account_Number', Request.AccountNumber);
      Fields.AddItem('IN_PRENOTE', 'N' );
      Fields.AddItem('ALLOW_HYPHENS', 'N' );
      Fields.AddItem('FORM_ON_FILE', 'Y' );
      Fields.AddItem('EE_BANK_ACCOUNT_TYPE', 'C');
      EEDirectDepositNbr := ApplyRecordChange( Fields, 'EE_DIRECT_DEPOSIT', 'I', -1 );
    end;
  finally
    qEEDirectDeposit.Free;
  end;

  Params.Clear;
  Fields.Clear;

  Params.AddItem( 'EeNbr', EeNbr );
  Params.AddItemDateTime( 'StartDate', Request.PaidDate );
  Params.AddItem( 'EdCode', Request.PaymentID );
  qEEScheduledEDs := RunQuery('EeSchedED', Params);
  try
    if not qEEScheduledEDs.Eof then
    begin
      //Found, update
      EeScheduledEDsNbr := qEEScheduledEDs.FieldByName('Ee_Scheduled_E_Ds_Nbr').AsInteger;
      if qEEScheduledEDs.FieldByName('Amount').AsFloat <> Request.Amount then
        Fields.AddItem('Amount', Request.Amount);
      if qEEScheduledEDs.FindField('Effective_Start_Date').AsDateTime <> Request.PaidDate then
        Fields.AddItem('Effective_Start_Date', Request.PaidDate);
      if qEEScheduledEDs.FindField('Target_Amount').AsFloat <> Request.Amount then
        Fields.AddItem('Target_Amount', Request.Amount);
      if qEEScheduledEDs.FindField('Ee_Direct_Deposit_Nbr').AsInteger <> EEDirectDepositNbr then
        Fields.AddItem('Ee_Direct_Deposit_Nbr', EEDirectDepositNbr);
      if qEEScheduledEDs.FindField('Garnisnment_ID').AsString <> Request.LoanID then
        Fields.AddItem('Garnisnment_ID', Request.LoanID);

      ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'U', EeScheduledEDsNbr );
    end else
    begin
      //Not found, create
      Fields.AddItem('Ee_Nbr', EeNbr);
      Fields.AddItem('Amount', Request.Amount);
      Fields.AddItem('Effective_Start_Date', Request.PaidDate);
      Fields.AddItem('Target_Amount', Request.Amount);
      Fields.AddItem('Ee_Direct_Deposit_Nbr', EEDirectDepositNbr);
      Fields.AddItem('Garnisnment_ID', Request.LoanID);
      Fields.AddItem('BENEFIT_AMOUNT_TYPE', 'N');
      Fields.AddItem('CAP_STATE_TAX_CREDIT', 'N');
      Fields.AddItem('SCHEDULED_E_D_ENABLED', 'Y');
      Fields.AddItem('CALCULATION_TYPE', 'F'); // Fixed
      Fields.AddItem('TARGET_ACTION', 'L'); // "Do Not Reset Balance but Leave Line"
      SetSchedDefaultED(Fields, Request.PaymentID);

      ApplyRecordChange( Fields, 'EE_SCHEDULED_E_DS', 'I', -1 );
    end;
  finally
    qEEScheduledEDs.Free;
  end;

  Response.SheduledPayDate := Request.PaidDate;
  FResponseInfo.ContentText := FormatCreateDirectDepositeResponse(Response);
end;

procedure TEvoFairLoanServer.GetEmployeeInfoBySSN;
var
  Request: TGetEmployeeInfo_POST;
  EmployeeInfo: TEmployeeInfo;
  Params: IRpcStruct;
  qEmployments: TkbmCustomMemTable;
  qPersonInfo: TkbmCustomMemTable;
  ClientNbr: Integer;
  ClPersonNbr: Integer;
  CoNbr: Integer;
  EeNbr: Integer;
begin
  Request := ParseGetEmployeeInfoRequest(FParams);
  Connect;
  FEvoApi.OpenClient(0);
  Params := TRpcStruct.Create;
  Params.AddItem('ssn', Request.SSN);
  qEmployments := RunQuery('EmploymentsBySSN', Params);
  try
    if qEmployments.Eof then
    begin
      ResourceNotFound;
      Exit;
    end;

    ClientNbr := qEmployments.FieldValues['CL_NBR'];
    ClPersonNbr := qEmployments.FieldValues['CL_PERSON_NBR'];
    FEvoApi.OpenClient(ClientNbr);
    Params.Clear;
    Params.AddItem('cl_person_number', ClPersonNbr);
    qPersonInfo := RunQuery('PersonInfo', Params);
    try
      EmployeeInfo.FirstName := qPersonInfo.FieldValues['FIRST_NAME'];
      EmployeeInfo.LastName := qPersonInfo.FieldValues['LAST_NAME'];
      EmployeeInfo.SSN := qPersonInfo.FieldValues['SSN'];
      EmployeeInfo.BirthDate := qPersonInfo.FieldValues['BIRTHDATE'];
      EmployeeInfo.ContactNumber := qPersonInfo.FieldValues['CONTACT_NUMBER'];
      EmployeeInfo.EMailAddress := qPersonInfo.FieldValues['E_MAIL_ADDRESS'];
      EmployeeInfo.Address.Street := qPersonInfo.FieldValues['STREET'];
      EmployeeInfo.Address.City := qPersonInfo.FieldValues['CITY'];
      EmployeeInfo.Address.State := qPersonInfo.FieldValues['STATE'];
      EmployeeInfo.Address.Zip := qPersonInfo.FieldValues['ZIP'];
    finally
      qPersonInfo.Free;
    end;

    SetLength(EmployeeInfo.Employments, qEmployments.RecordCount);
    qEmployments.First;
    while not qEmployments.Eof do
    begin
      if ClientNbr <> qEmployments.FieldValues['CL_NBR'] then
      begin
        ClientNbr := qEmployments.FieldValues['CL_NBR'];
        FEvoApi.OpenClient(ClientNbr);
      end;
      CoNbr := qEmployments.FieldByName('CO_NBR').AsInteger;
      EeNbr := qEmployments.FieldByName('EE_NBR').AsInteger;
      EmployeeInfo.Employments[qEmployments.RecNo - 1] := QueryEmploymentInfo(ClientNbr, CoNbr, EeNbr);
      qEmployments.Next;
    end;

    FResponseInfo.ContentText := '[' + #13#10 + '{' + #13#10 +
       FormatEmployeeInfo(EmployeeInfo) + #13#10 + '}' + #13#10 + ']';
  finally
    qEmployments.Free;
  end;
end;

procedure TEvoFairLoanServer.GetEmployeeInfoByUID;
var
  Request: TGetEmployeeInfo_GET;
  RequestParams: TStringList;
  Params: IRpcStruct;
  qEmployments: TkbmCustomMemTable;
  qPersonInfo: TkbmCustomMemTable;
  ClientNbr: Integer;
  ClPersonNbr: Integer;
  CoNbr: Integer;
  EeNbr: Integer;
  EmployeeInfo: TEmployeeInfo;
begin

  RequestParams := TStringList.Create;
  try
    RequestParams.Delimiter := '&';
    RequestParams.DelimitedText := FParams;
    Request.EmployeeID := RemoveExtension(RequestParams.Values['employee_id']);
    Request.IncludePastEmployment := RemoveExtension(RequestParams.Values['include_past_employment']) = 'yes';
    RequestParams.Clear;
    RequestParams.Delimiter := '-';
    RequestParams.DelimitedText := Request.EmployeeID;
    ClientNbr := StrToInt(RequestParams.Strings[0]);
    CoNbr := StrToInt(RequestParams.Strings[1]);
    EeNbr := StrToInt(RequestParams.Strings[2]);
  finally
    RequestParams.Free;
  end;
  Connect;
  FEvoApi.OpenClient(0);
  Params := TRpcStruct.Create;
  Params.AddItem('cl_nbr', ClientNbr);
  Params.AddItem('co_nbr', CoNbr);
  Params.AddItem('ee_nbr', EeNbr);
  qEmployments := RunQuery('EmploymentsByCode', Params);
  try
    if qEmployments.Eof then
    begin
      ResourceNotFound;
      Exit;
    end;

    ClientNbr := qEmployments.FieldValues['CL_NBR'];
    ClPersonNbr := qEmployments.FieldValues['CL_PERSON_NBR'];
    FEvoApi.OpenClient(ClientNbr);
    Params.Clear;
    Params.AddItem('cl_person_number', ClPersonNbr);
    qPersonInfo := RunQuery('PersonInfo', Params);
    try
      EmployeeInfo.FirstName := qPersonInfo.FieldValues['FIRST_NAME'];
      EmployeeInfo.LastName := qPersonInfo.FieldValues['LAST_NAME'];
      EmployeeInfo.SSN := qPersonInfo.FieldValues['SSN'];
      EmployeeInfo.BirthDate := qPersonInfo.FieldValues['BIRTHDATE'];
      EmployeeInfo.ContactNumber := qPersonInfo.FieldValues['CONTACT_NUMBER'];
      EmployeeInfo.EMailAddress := qPersonInfo.FieldValues['E_MAIL_ADDRESS'];
      EmployeeInfo.Address.Street := qPersonInfo.FieldValues['STREET'];
      EmployeeInfo.Address.City := qPersonInfo.FieldValues['CITY'];
      EmployeeInfo.Address.State := qPersonInfo.FieldValues['STATE'];
      EmployeeInfo.Address.Zip := qPersonInfo.FieldValues['ZIP'];
    finally
      qPersonInfo.Free;
    end;

    SetLength(EmployeeInfo.Employments, qEmployments.RecordCount);
    while not qEmployments.Eof do
    begin
      if ClientNbr <> qEmployments.FieldValues['CL_NBR'] then
      begin
        ClientNbr := qEmployments.FieldValues['CL_NBR'];
        FEvoApi.OpenClient(ClientNbr);
      end;
      CoNbr := qEmployments.FieldByName('CO_NBR').AsInteger;
      EeNbr := qEmployments.FieldByName('EE_NBR').AsInteger;
      EmployeeInfo.Employments[qEmployments.RecNo - 1] := QueryEmploymentInfo(ClientNbr, CoNbr, EeNbr);
      qEmployments.Next;
    end;

    FResponseInfo.ContentText := '[' + #13#10 + '  {' + #13#10 +
       FormatEmployeeInfo(EmployeeInfo) + #13#10 + '  }' + #13#10 + ']';
  finally
    qEmployments.Free;
  end;
end;

procedure TEvoFairLoanServer.GetLoanPaymentInfo;
var
  Request: TGetLoanPaymentInfo_GET;
  Response: TLoanPaymentsArray;
  RequestParams: TStringList;
  Params: IRpcStruct;
  qLoanPayments: TkbmCustomMemTable;
  Idx: Integer;
begin
  RequestParams := TStringList.Create;
  try
    RequestParams.Delimiter := '&';
    RequestParams.DelimitedText := FParams;
    Request.LoanID := RemoveExtension(RequestParams.Values['loan_id']);
    Request.PaymentID := RemoveExtension(RequestParams.Values['payment_id']);
    Request.Posted := RemoveExtension(RequestParams.Values['posted']) = 'yes';
  finally
    RequestParams.Free;
  end;

  Params := TRpcStruct.Create;
  Params.AddItem('loan_id', Request.LoanID);
  Params.AddItem('payment_id', Request.PaymentID);
  Params.AddItem('posted', Request.Posted);
  Connect;
  FEvoAPI.OpenClient(163);
  qLoanPayments := RunQuery('LoanPayments', Params);
  try
    SetLength(Response, qLoanPayments.RecordCount);
    while not qLoanPayments.Eof do
    begin
      Idx := qLoanPayments.RecNo - 1;
      Response[Idx].LoanID := qLoanPayments.FieldByName('loan_id').AsString;
      Response[Idx].PaymentID := qLoanPayments.FieldByName('payment_id').AsString;
      Response[Idx].PaymentDate := qLoanPayments.FieldByName('payment_date').AsDateTime;
      Response[Idx].Amount := qLoanPayments.FieldByName('amount').AsFloat;
      qLoanPayments.Next;
    end;
  finally
    qLoanPayments.Free;
  end;
  FResponseInfo.ContentText := FormatLoanPaymentsResponse(Response);
end;

// Evolution data access methods

procedure TEvoFairLoanServer.Connect;
begin
  FEvoAPI := CreateEvoAPIConnection(ServerConfig.EvoConnectionParam, FLogger);
end;

function TEvoFairLoanServer.RunQuery(QueryName: string;
  params: IRpcStruct): TkbmCustomMemTable;
begin
  Result := FEvoAPI.RunQuery(Redirection.GetDirectory(sQueryDirAlias) + QueryName + '.rwq', Params);
end;

function TEvoFairLoanServer.ApplyRecordChange(var aFields: IRpcStruct;
  const aTableName, aType: string; aKey: integer): integer;
var
  Changes: IRpcArray;
  RowChange, EvoResult: IRpcStruct;
begin
  Result := -1;
  if aFields.Count > 0 then
  begin
    Result := aKey;

    Changes := TRpcArray.Create;
    RowChange := TRpcStruct.Create;
    EvoResult := TRpcStruct.Create;

    RowChange.AddItem('T', aType);
    RowChange.AddItem('D', aTableName);
    if aType = 'U' then
      RowChange.AddItem('K', aKey)
    else
      aFields.AddItem(aTableName + '_NBR', -1);

    RowChange.AddItem('F', aFields);

    Changes.AddItem( RowChange );
    EvoResult := FEvoAPI.applyDataChangePacket( Changes );

    if Result = -1 then
      if EvoResult.KeyExists('-1') then
        Result := StrToInt(EvoResult.Keys['-1'].AsString);
  end;
end;

function TEvoFairLoanServer.FindCompany(var aCompany: TEvoCompanyDef): boolean;
var
  FClCoData: TkbmCustomMemTable;
  Param: IRpcStruct;
begin
  Result := False;
  aCompany.ClNbr := -1;
  aCompany.CoNbr := -1;

  param := TRpcStruct.Create;
  Param.AddItem( 'CoNumber', aCompany.Custom_Company_Number );
  FClCoData :=  RunQuery('TmpCoQuery.rwq', Param);
  try
    if (FClCoData.RecordCount > 0) and Assigned(FClCoData.FindField('CL_NBR')) and Assigned(FClCoData.FindField('CO_NBR')) then
    begin
      aCompany.ClNbr := FClCoData.FindField('CL_NBR').AsInteger;
      aCompany.CoNbr := FClCoData.FindField('CO_NBR').AsInteger;
      Result := True;
    end;
  finally
    FClCoData.Indexes.Clear;
    FreeAndNil( FClCoData );
    Param := nil;
  end;
end;

function TEvoFairLoanServer.QueryEmploymentInfo(ClientNbr, CompanyNbr,
  EmployeeNbr: Integer): TEmploymentInfo;
var
  Params: IRpcStruct;
  qEmployment: TkbmCustomMemTable;
  qPayments: TkbmCustomMemTable;
  i: Integer;
  DatB: TDateTime;
begin
  DatB := StartOfTheMonth(IncMonth(Date, -6));
  Params := TRpcStruct.Create;
  try
    Params.AddItem('co_nbr', CompanyNbr);
    Params.AddItem('ee_nbr', EmployeeNbr);
    qEmployment := RunQuery('Employment', Params);
    try
      if not qEmployment.Eof then
      begin
        Result.EmployeeID := IntToStr(ClientNbr) + '-' + IntToStr(CompanyNbr) + '-' + IntToStr(EmployeeNbr);
        Result.EmpoloyerID := IntToStr(ClientNbr) + '-' + IntToStr(CompanyNbr);
        Result.EmployerAddress.Street := qEmployment.FieldByName('STREET').AsString;
        Result.EmployerAddress.City := qEmployment.FieldByName('CITY').AsString;
        Result.EmployerAddress.State := qEmployment.FieldByName('STATE').AsString;
        Result.EmployerAddress.Zip := qEmployment.FieldByName('ZIP').AsString;
        Result.EmployeeIsActive := qEmployment.FieldByName('EMPLOYEE_IS_ACTIVE').AsBoolean;
        Result.HireDate := qEmployment.FieldByName('HIRE_DATE').AsDateTime;
        Result.TerminationDate := qEmployment.FieldByName('TERMINATION_DATE').AsDateTime;
        Result.RehireDate := qEmployment.FieldByName('REHIRE_DATE').AsDateTime;
        Result.PayRate := qEmployment.FieldByName('PAY_RATE').AsFloat;
        Result.PayType := qEmployment.FieldByName('PAY_TYPE').AsString;
        Result.EarningsToDate := qEmployment.FieldByName('EARNINGS_TO_DATE').AsFloat;
        Result.PayFrequency := qEmployment.FieldByName('PAY_FREQUENCY').AsString;
        Result.LastPayDate := qEmployment.FieldByName('LAST_PAY_DATE').AsDateTime;
        Result.NextPayDate := qEmployment.FieldByName('NEXT_PAY_DATE').AsDateTime;

        Params.AddItem('dat_b', DatB);
        qPayments := RunQuery('Payments', Params);
        try
          SetLength(Result.Payments, qPayments.RecordCount);
          qPayments.First;
          while not qPayments.Eof do
          begin
            i := qPayments.RecNo - 1;
            Result.Payments[i].GrossWages := qPayments.FieldByName('GROSS_WAGES').AsFloat;
            Result.Payments[i].RegularAmount := qPayments.FieldByName('REGULAR_AMOUNT').AsFloat;
            Result.Payments[i].TipsAmount := qPayments.FieldByName('TIPS_AMOUNT').AsFloat;
            Result.Payments[i].BonusAmount := qPayments.FieldByName('BONUS_AMOUNT').AsFloat;
            Result.Payments[i].OvertimeAmount := qPayments.FieldByName('OVERTIME_AMOUNT').AsFloat;
            Result.Payments[i].Hours := qPayments.FieldByName('HOURS').AsFloat;
            Result.Payments[i].Garnishments := qPayments.FieldByName('GARNISHMENTS').AsFloat;
            Result.Payments[i].PreTaxDeduction := qPayments.FieldByName('PRE_TAX_DEDUCTION').AsFloat;
            Result.Payments[i].AfterTaxDeduction := qPayments.FieldByName('AFTER_TAX_DEDUCTION').AsFloat;
            Result.Payments[i].NetWages := qPayments.FieldByName('NET_WAGES').AsFloat;
            Result.Payments[i].CheckDate := qPayments.FieldByName('CHECK_DATE').AsDateTime;
            Result.Payments[i].IsDirectDeposit := qPayments.FieldByName('IS_DIRECT_DEPOSIT').AsBoolean;
            Result.Payments[i].Misc := qPayments.FieldByName('MISC').AsFloat;
            qPayments.Next;
          end;
        finally
          qPayments.Free;
        end;    
      end;
    finally
       qEmployment.Free;
    end;
  finally
    Params := nil;
  end;
end;

procedure TEvoFairLoanServer.SetSchedDefaultED(var aFields: IRpcStruct; const EdCode: string);
var
  FDs: TkbmCustomMemTable;
  param: IRpcStruct;
begin
  param := TRpcStruct.Create;
  Param.AddItem('EdCode', EdCode);
  FDs := RunQuery('D1_ClEDCode', param);
  try
    if FDs.RecordCount <= 0 then
      raise Exception.Create('E/D code "' + EdCode + '" is not set!')
    else begin
      aFields.AddItem('CL_E_DS_NBR', FDs.FieldByName('Cl_E_Ds_Nbr').AsInteger);
      if FDs.FieldByName('Scheduled_Defaults').AsString = 'Y' then
      begin
        aFields.AddItem('FREQUENCY', FDs.FieldByName('Frequency').AsString);
        aFields.AddItem('EXCLUDE_WEEK_1', FDs.FieldByName('EXCLUDE_WEEK_1').AsString);
        aFields.AddItem('EXCLUDE_WEEK_2', FDs.FieldByName('EXCLUDE_WEEK_2').AsString);
        aFields.AddItem('EXCLUDE_WEEK_3', FDs.FieldByName('EXCLUDE_WEEK_3').AsString);
        aFields.AddItem('EXCLUDE_WEEK_4', FDs.FieldByName('EXCLUDE_WEEK_4').AsString);
        aFields.AddItem('EXCLUDE_WEEK_5', FDs.FieldByName('EXCLUDE_WEEK_5').AsString);
        aFields.AddItem('ALWAYS_PAY', FDs.FieldByName('ALWAYS_PAY').AsString);
        aFields.AddItem('DEDUCTIONS_TO_ZERO', FDs.FieldByName('DEDUCTIONS_TO_ZERO').AsString);
        aFields.AddItem('DEDUCT_WHOLE_CHECK', FDs.FieldByName('DEDUCT_WHOLE_CHECK').AsString);
        aFields.AddItem('PLAN_TYPE', FDs.FieldByName('PLAN_TYPE').AsString);
        aFields.AddItem('WHICH_CHECKS', FDs.FieldByName('WHICH_CHECKS').AsString);
        aFields.AddItem('USE_PENSION_LIMIT', FDs.FieldByName('USE_PENSION_LIMIT').AsString);
      end
      else begin
        aFields.AddItem('FREQUENCY', 'D'); //Every Pay
        aFields.AddItem('EXCLUDE_WEEK_1', 'N');
        aFields.AddItem('EXCLUDE_WEEK_2', 'N');
        aFields.AddItem('EXCLUDE_WEEK_3', 'N');
        aFields.AddItem('EXCLUDE_WEEK_4', 'N');
        aFields.AddItem('EXCLUDE_WEEK_5', 'N');
        aFields.AddItem('ALWAYS_PAY', 'C'); //Current Payroll
        aFields.AddItem('DEDUCTIONS_TO_ZERO', 'N');
        aFields.AddItem('DEDUCT_WHOLE_CHECK', 'N');
        aFields.AddItem('PLAN_TYPE', 'N');
        aFields.AddItem('WHICH_CHECKS', 'A'); // All
        aFields.AddItem('USE_PENSION_LIMIT', 'N');
      end;
    end;
  finally
    Param := nil;
    FreeAndNil(FDs);
  end;
end;
{ TServerConfig }

constructor TServerConfig.Create;
begin
  FWitelist := TStringList.Create;
end;

destructor TServerConfig.Destroy;
begin
  FWitelist.Free;
  inherited;
end;

function TServerConfig.IsForbidden(IP: string): Boolean;
begin
  Result := FWitelist.IndexOf(IP) >= 0;
end;

procedure TServerConfig.Load;
var
  FIniFile: TIniFile;
begin
  FIniFile := TIniFile.Create(Redirection.GetFilename('Config'));
  FEvoConnectionParam.APIServerAddress := FIniFile.ReadString('EvoAPIConnection', 'APIServerAddress', 'localhost');
  FEvoConnectionParam.APIServerPort := FIniFile.ReadInteger('EvoAPIConnection', 'APIServerPort', 9943);
  FEvoConnectionParam.Username := FIniFile.ReadString('EvoAPIConnection', 'Username', '');
  FEvoConnectionParam.Password := FIniFile.ReadString('EvoAPIConnection', 'Password', '');
  FPort := FIniFile.ReadInteger('Server', 'HTTP_Port', 80);
  ReadableOutput := FIniFile.ReadBool('Server', 'ReadableOutput', False);
  FUseSSL := FIniFile.ReadBool('Server', 'UseSSL', False);
  FRootCertFile := FIniFile.ReadString('Server', 'RootCertFile', Redirection.GetFilename('RootCertificate'));
  FCertFile := FIniFile.ReadString('Server', 'CertFile', Redirection.GetFilename('Certificate'));
  FKeyFile := FIniFile.ReadString('Server', 'KeyFile', Redirection.GetFilename('KeyCertificate'));
  FUserName := FIniFile.ReadString('Server', 'UserName', '');
  FPassword := FIniFile.ReadString('Server', 'Password', '');
  FIniFile.ReadSection('Whitelist', FWitelist);
  SetReadableOutput(ReadableOutput);
  FIniFile.Free;
end;

procedure TServerConfig.SetReadableOutput(const Value: Boolean);
begin
  FReadableOutput := Value;
  FairLoanApi.SetReadableOutput(Value);
end;

constructor TEvoFairLoanServer.Create(Logger: ICommonLogger);
begin
  FLogger := Logger;
end;

initialization
  ServerConfig := TServerConfig.Create;

finalization
  ServerConfig.Free;

end.
