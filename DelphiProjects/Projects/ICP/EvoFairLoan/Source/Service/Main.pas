unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  IdBaseComponent, IdComponent, IdTCPServer, IdCustomHTTPServer, gdycommonlogger,
  IdHTTPServer, IdServerIOHandler, IdSSLOpenSSL;

type
  TEvoFairLoanService = class(TService, ICommonLogger)
    HTTPServer: TIdHTTPServer;
    HandlerSSL: TIdServerIOHandlerSSL;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure HTTPServerCommandGet(AThread: TIdPeerThread;
      ARequestInfo: TIdHTTPRequestInfo;
      AResponseInfo: TIdHTTPResponseInfo);
    procedure HTTPServerCommandOther(Thread: TIdPeerThread;
      const asCommand, asData, asVersion: String);
  private
    FLog: Text;
    FFileOpened: boolean;
    procedure FileNeeded;
    procedure CloseFileTemporarily;
    procedure LogMessage(const s: string);
    {ICommonLogger}
    procedure LogEntry( blockname: string );
    procedure LogExit; //must match LogEntry call
    procedure LogContextItem( tag: string; value: string );
    procedure LogEvent( s: string; d: string = '' );
    procedure LogEventFmt( s: string; const args: array of const; d: string = '' );
    procedure LogWarning( s: string; d: string = '' );
    procedure LogWarningFmt( s: string; const args: array of const; d: string = '' );
    procedure LogError( s: string; d: string = '' );
    procedure LogErrorFmt( s: string; const args: array of const; d: string = '' );
    procedure LogDebug( s: string; fulltext: string = '' );
    procedure PassthroughException;
    procedure StopException;
    procedure PassthroughExceptionAndWarnFmt( s: string; const args: array of const );
    procedure StopExceptionAndWarnFmt( s: string; const args: array of const );
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  EvoFairLoanService: TEvoFairLoanService;

implementation

uses IniFiles, gdyRedir, EvoFairLoanServerMod;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  EvoFairLoanService.Controller(CtrlCode);
end;

function TEvoFairLoanService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TEvoFairLoanService.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  ServerConfig.Load;
  HTTPServer.DefaultPort := ServerConfig.Port;
  HandlerSSL.SSLOptions.RootCertFile := ServerConfig.RootCertFile;
  HandlerSSL.SSLOptions.CertFile := ServerConfig.CertFile;
  HandlerSSL.SSLOptions.KeyFile := ServerConfig.KeyFile;
  HTTPServer.IOHandler := HandlerSSL;
  LogMessage('Running web server at port ' + IntToStr(HTTPServer.DefaultPort));
  HTTPServer.Active := True;
  Started := HTTPServer.Active;
end;

procedure TEvoFairLoanService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  HTTPServer.Active := False;
  CloseFileTemporarily;
  Stopped := not HTTPServer.Active;
end;

procedure TEvoFairLoanService.HTTPServerCommandGet(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  Server: TEvoFairLoanServer;
  ServiceName: string;
  Method: string;
  Params: string;
begin
  ServiceName := GetServiceName(ARequestInfo.Document);
  Method := ARequestInfo.Command;
  Params := ARequestInfo.UnparsedParams;
  Server := TEvoFairLoanServer.Create(Self);
  try
    if ARequestInfo.AuthExists and Server.CheckAuth(ARequestInfo.AuthUsername, ARequestInfo.AuthPassword) then
    begin
      Server.ProcessRequest(ServiceName, Method, Params, AResponseInfo);
    end else
    begin
      AResponseInfo.ResponseNo := 401;
      AResponseInfo.ResponseText := 'Unauthorized Access';
      AResponseInfo.AuthRealm := 'Evolution FairLoan Service';
    end;
  finally
    Server.Free;
  end;
end;

procedure TEvoFairLoanService.HTTPServerCommandOther(Thread: TIdPeerThread;
  const asCommand, asData, asVersion: String);
begin
  Thread.Connection.OpenWriteBuffer;
  Thread.Connection.WriteLn('HTTP/1.1 405 Method Not Allowed');
  Thread.Connection.CloseWriteBuffer;
end;

procedure TEvoFairLoanService.CloseFileTemporarily;
begin
  if FFileOpened then
  try
    system.Flush( FLog );
    system.CloseFile( FLog );
    FFileOpened := false;
  except
    //do nothing
  end;
end;

procedure TEvoFairLoanService.FileNeeded;
var
  fn: string;
begin
  if not FFileOpened then
    try
      fn := Redirection.GetFilename('Log');
      ForceDirectories( ExtractFilePath(fn) );
      system.assignfile( FLog, fn );
      system.Rewrite( FLog );
      FFileOpened := true;  
    except
      //do nothing
    end;
end;

{ICommonLogger}

procedure TEvoFairLoanService.LogContextItem(tag, value: string);
begin
  LogMessage(tag + ' = ' + value);
end;

procedure TEvoFairLoanService.LogDebug(s, fulltext: string);
begin
  LogEvent(s, fulltext);
end;

procedure TEvoFairLoanService.LogEntry(blockname: string);
begin
  LogMessage(blockname + ' -> (');
end;

procedure TEvoFairLoanService.LogError(s, d: string);
begin
  LogEvent( 'Error: ' + s, d );
end;

procedure TEvoFairLoanService.LogErrorFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( 'Error: ' + Format( s, args ), d );
end;

procedure TEvoFairLoanService.LogEvent(s, d: string);
begin
  if d <> '' then
    LogMessage(s + ', details: ' + d)
  else
    LogMessage(s);
end;

procedure TEvoFairLoanService.LogEventFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( Format( s, args ), d );
end;

procedure TEvoFairLoanService.LogExit;
begin
  LogMessage(') <-');
end;

procedure TEvoFairLoanService.LogMessage(const s: string);
begin
  try
    FileNeeded;
    system.Writeln( FLog, s );
    system.Flush( FLog );
  except
    //do nothing
  end;
end;

procedure TEvoFairLoanService.LogWarning(s, d: string);
begin
  LogEvent('Warning! ' + s, d);
end;

procedure TEvoFairLoanService.LogWarningFmt(s: string;
  const args: array of const; d: string);
begin
  LogEvent( 'Warning! ' + Format( s, args ), d );
end;

procedure TEvoFairLoanService.PassthroughException;
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
  begin
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
    raise Exception.Create((ExceptObject as Exception).Message);
  end;
end;

procedure TEvoFairLoanService.PassthroughExceptionAndWarnFmt(s: string;
  const args: array of const);
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
  begin
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
    raise Exception.Create((ExceptObject as Exception).Message);
  end;
end;

procedure TEvoFairLoanService.StopException;
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
end;

procedure TEvoFairLoanService.StopExceptionAndWarnFmt(s: string;
  const args: array of const);
begin
  if (ExceptObject <> nil) and (ExceptObject is Exception) then
    LogMessage('Exception: ' + (ExceptObject as Exception).Message );
end;

end.
