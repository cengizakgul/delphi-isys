@ECHO OFF

IF "%1" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. Application Version
ECHO    2. DevTools folder (optional)
GOTO end

:start

SET pAppVersion=%1
IF "%pAppVersion%" NEQ ""  SET pAppVersion=%pAppVersion:"=%
SET pDevToolsDir=%2
IF "%pDevToolsDir%" NEQ "" SET pDevToolsDir=%pDevToolsDir:"=%

SET HomeDir=%CD%

CD "%HomeDir%"\AchCombine
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoAppraisal
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoDEMS
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoEeImport
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoFairLoan
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoInfinityHR
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoJBC
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoMRC
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoPayEarly
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoPayrollPlans
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoScheduledEDImport
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoSlavic
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoWorkersChoice
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\ReplaceEIN
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoMyHRAdmin
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoSwipeclock
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoTimeConversion
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoADI
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoAoD
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoAloha
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoMA
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoMJD
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvopayManager
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoPizza
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoStatesModify
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoQ
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoADIBasic
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoSwipeclockBasic
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoM3
CALL Build.cmd %pAppVersion% %pDevToolsDir%

CD "%HomeDir%"\EvoTacobell
CALL Build.cmd %pAppVersion% %pDevToolsDir%

:end
