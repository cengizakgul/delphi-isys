@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVer=""

cd ..\..\..\..\Bin\ICP\EvoMyHRAdmin
SET pExePath=%cd%
SET pExePath=%pExePath:\=\\%\\EvoMyHRAdmin.exe
cd ..\..\..\Projects\ICP\EvoMyHRAdmin\Installer\Projects

IF NOT EXIST ..\..\..\..\..\Bin\ICP\Installers mkdir ..\..\..\..\..\Bin\ICP\Installers

for /f %%v in ('wmic datafile where name^='%pExePath%' get version /value^|find "Version"') do set pAppVer=%%v

set pAppVer=%pAppVer:~8,100%

ECHO Creating EvoMyHRAdmin.msi 

"%pWiXDir%\candle.exe" .\EvoMyHRAdmin.wxs -wx -out ..\..\..\..\..\Tmp\EvoMyHRAdmin.wixobj -dproductVersion="%pAppVer%"
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\heat.exe" dir ..\..\Resources\Queries -gg -sfrag -srd -cg RwQueries -dr QUERIESDIR -out ..\..\..\..\..\Tmp\EvoMyHRAdminRwQueries.wxs
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\candle.exe" ..\..\..\..\..\Tmp\EvoMyHRAdminRwQueries.wxs -wx -out ..\..\..\..\..\Tmp\EvoMyHRAdminRwQueries.wixobj
IF ERRORLEVEL 1 EXIT 1

"%pWiXDir%\light.exe" ..\..\..\..\..\Tmp\EvoMyHRAdmin.wixobj ..\..\..\..\..\Tmp\EvoMyHRAdminRwQueries.wixobj -out ..\..\..\..\..\Bin\ICP\Installers\EvoMyHRAdmin.msi -ext WixUIExtension  -wx -sw1076 -b ..\..\Resources\Queries 
IF ERRORLEVEL 1 EXIT 1

del ..\..\..\..\..\Bin\ICP\Installers\EvoMyHRAdmin.wixpdb
IF EXIST ..\..\..\..\..\Bin\ICP\Installers\EvoMyHRAdmin_%pAppVer%.msi del ..\..\..\..\..\Bin\ICP\Installers\EvoMyHRAdmin_%pAppVer%.msi > nul
ren ..\..\..\..\..\Bin\ICP\Installers\EvoMyHRAdmin.msi EvoMyHRAdmin_%pAppVer%.msi > nul
IF ERRORLEVEL 1 EXIT 1
