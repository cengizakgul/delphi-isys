unit ExtAppTasks;

interface

uses
  scheduledtask, isSettings, gdyCommonLogger, common,
  evodata, classes, scheduledCustomTask, extappdecl, timeclockimport,
  extappconnection, evoapiconnectionutils, toaimport;

type
  TExtAppTaskAdapter = class(TTaskAdapterCustomBase)
  private
    function GetExtAppConnectionParam: TExtAppConnectionParam;
  published
    procedure EEExport_Execute;
    function EEExport_Describe: string;
    procedure EEImport_Execute;
    function EEImport_Describe: string;
    procedure DictExport_Execute;
    function DictExport_Describe: string;
  end;


function NewEEExportTask(ClCo: TEvoCompanyDef{; EEExportOptions: TEEExportOptions}): IScheduledTask;
function NewEEImportTask(ClCo: TEvoCompanyDef{; EEImportOptions: TEEImportOptions}): IScheduledTask;
function NewDictExportTask(ClCo: TEvoCompanyDef): IScheduledTask;

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);

implementation

uses
  gdyBinderImpl, gdyBinder, sysutils, evoapiconnection,
  gdyclasses, gdyCommon, dialogs,
  gdyDialogEngine, controls, kbmMemTable, {EEExportDialog,} extapp, userActionHelpers;


function NewEEExportTask(ClCo: TEvoCompanyDef{; EEExportOptions: TEEExportOptions}): IScheduledTask;
begin
  Result := NewTask('EEExport', 'Employee Export', ClCo);
//  SaveEEExportOptions( EEExportOptions, Result.ParamSettings, '' );
end;

function NewEEImportTask(ClCo: TEvoCompanyDef{; EEImportOptions: TEEImportOptions}): IScheduledTask;
begin
  Result := NewTask('EEImport', 'Employee Import', ClCo);
//  SaveEEImportOptions( EEImportOptions, Result.ParamSettings, '' );
end;

function NewDictExportTask(ClCo: TEvoCompanyDef): IScheduledTask;
begin
  Result := NewTask('DictExport', 'Dictionary Export', ClCo);
end;

{
procedure EditEEExportTask(task: IScheduledTask; Owner: TComponent);
var
  dlg: TEEExportDlg;
begin
  dlg := TEEExportDlg.Create(nil);
  try
    dlg.Caption := task.UserFriendlyName + ' Task Parameters';
    dlg.EEExportOptionsFrame.Options := LoadEEExportOptions(task.ParamSettings, '');
    with DialogEngine(dlg, Owner) do
      if ShowModal = mrOk then
        SaveEEExportOptions( dlg.EEExportOptionsFrame.Options, task.ParamSettings, '' );
  finally
    FreeAndNil(dlg);
  end;
end;
}

procedure EditTask(task: IScheduledTask; Logger: ICommonLogger; Owner: TComponent);
begin
  if task.Name = 'EEExport' then
//    EditEEExportTask(task, Owner)
    ShowMessage('Employee Export task has no parameters')
  else if task.Name = 'EEImport' then
//    EditEEImportTask(task, Owner)
    ShowMessage('Employee Import task has no parameters')
  else if task.Name = 'DictExport' then
    ShowMessage('Dictionary Export task has no parameters')
  else
    Assert(false);
end;

{ TExtAppTaskAdapter }

function TExtAppTaskAdapter.DictExport_Describe: string;
begin
  Result := '';
end;

procedure TExtAppTaskAdapter.DictExport_Execute;
begin
  extapp.DoDictExport(FLogger, FTask.Company, GetEvoAPICOnnection, GetExtAppConnectionParam, NullProgressIndicator, false);
end;

function TExtAppTaskAdapter.EEExport_Describe: string;
begin
  Result := '';//DescribeEEExportOptions( LoadEEExportOptions(FTask.ParamSettings, '') );
end;

procedure TExtAppTaskAdapter.EEExport_Execute;
var
  opname: string;
  {
  EeExportOptions: TEEExportOptions;
  EEStatusMatcher: IMatcher;
  WorkgroupMatcher: IMatcher;
  }
  procedure DoIt;
  var
    stat: TUpdateEmployeesStat;
    EEData: TEvoEEData;
    analysisResult: TAnalyzeResult;
    updater: TExtAppEmployeeExporter;
  begin
    EEData := TEvoEEData.Create( GetEvoAPICOnnection, FLogger, FTask.Company);
    try
      updater := TExtAppEmployeeExporter.Create(FLogger, FTask.Company, EEData, GetExtAppConnectionParam, NullProgressIndicator);
      try
        analysisResult := updater.Analyze_EEExport;
        if analysisResult.Actions.Count > 0 then
          FLogger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        stat := updater.Apply_EEExport(analysisResult.Actions);
        stat.Errors := stat.Errors + analysisResult.Errors;
      finally
        FreeAndNil(updater);
      end;

      if stat.Errors = 0 then
        FLogger.LogEvent(opname + ' ' + SyncStatToString(stat))
      else
        FLogger.LogWarning(opname + ' ' + SyncStatToString(stat));
    finally
      FreeAndNil(EEData);
    end;
  end;

begin
  opname := Format('%s employee records update', [ExtAppName]);
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      {
      EeExportOptions := LoadEEExportOptions(FTask.ParamSettings, '');
      LogEEExportOptions(FLogger, EeExportOptions);

      WorkgroupMatcher := CreateMatcher( WorkgroupBinding, FSettings.AsString[CompanyUniquePath(FTask.Company)+'\WorkgroupMapping\DataPacket'] );
      EEStatusMatcher := CreateMatcher( EEStatusBinding, FSettings.AsString[CompanyUniquePath(FTask.Company)+'\EEStatusMapping\DataPacket'] );

      FLogger.LogDebug('Workgroup mapping', WorkgroupMatcher.Dump);
      FLogger.LogDebug('EEStatus mapping', EEStatusMatcher.Dump);
      }
      DoIt;
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;

  extapp.DoGroupedOp(FLogger, FTask.Company, GetEvoAPICOnnection, GetExtAppConnectionParam, NullProgressIndicator, false, nil, TExtAppRatesExporter);
  extapp.DoGroupedOp(FLogger, FTask.Company, GetEvoAPICOnnection, GetExtAppConnectionParam, NullProgressIndicator, false, nil, TExtAppScheduledEDsExporter);

end;

function TExtAppTaskAdapter.EEImport_Describe: string;
begin
  Result := '';//DescribeEEImportOptions( LoadEEImportOptions(FTask.ParamSettings, '') );
end;

procedure TExtAppTaskAdapter.EEImport_Execute;
var
  opname: string;
  {
  EeImportOptions: TEEImportOptions;
  EEStatusMatcher: IMatcher;
  WorkgroupMatcher: IMatcher;
  }
  procedure DoIt;
  var
    stat: TUpdateEmployeesStat;
    EEData: TEvoEEData;
    analysisResult: TAnalyzeResult;
    updater: TExtAppEmployeeImporter;
  begin
    EEData := TEvoEEData.Create( GetEvoAPICOnnection, FLogger, FTask.Company);
    try
      updater := TExtAppEmployeeImporter.Create(FLogger, FTask.Company, EEData, GetExtAppConnectionParam, NullProgressIndicator, CreateSilentEvoXImportExecutor(GetEvoAPICOnnection));
      try
        analysisResult := updater.Analyze_EEImport;
        if analysisResult.Actions.Count > 0 then
          FLogger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        stat := updater.Apply_EEImport(analysisResult.Actions);
        stat.Errors := stat.Errors + analysisResult.Errors;
      finally
        FreeAndNil(updater);
      end;

      if stat.Errors = 0 then
        FLogger.LogEvent(opname + ' ' + SyncStatToString(stat))
      else
        FLogger.LogWarning(opname + ' ' + SyncStatToString(stat));
    finally
      FreeAndNil(EEData);
    end;
  end;

begin
  opname := Format('%s employee records update', ['Evolution']);
  FLogger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(FLogger, FTask.Company);
      FLogger.LogEventFmt('%s started', [opname]);

      {
      EeImportOptions := LoadEEImportOptions(FTask.ParamSettings, '');
      LogEEImportOptions(FLogger, EeImportOptions);

      WorkgroupMatcher := CreateMatcher( WorkgroupBinding, FSettings.AsString[CompanyUniquePath(FTask.Company)+'\WorkgroupMapping\DataPacket'] );
      EEStatusMatcher := CreateMatcher( EEStatusBinding, FSettings.AsString[CompanyUniquePath(FTask.Company)+'\EEStatusMapping\DataPacket'] );

      FLogger.LogDebug('Workgroup mapping', WorkgroupMatcher.Dump);
      FLogger.LogDebug('EEStatus mapping', EEStatusMatcher.Dump);
      }
      DoIt;
    except
      FLogger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    FLogger.LogExit;
  end;

  extapp.DoGroupedOp(FLogger, FTask.Company, GetEvoAPICOnnection, GetExtAppConnectionParam, NullProgressIndicator, false, CreateSilentEvoXImportExecutor(GetEvoAPICOnnection), TExtAppRatesImporter);
//  if LoadGlobalSettings(FSettings, '').ImportScheduledEDs then
//    extapp.DoGroupedOp(FLogger, FTask.Company, GetEvoAPICOnnection, GetExtAppConnectionParam, NullProgressIndicator, false, CreateSilentEvoXImportExecutor(GetEvoAPICOnnection), TExtAppScheduledEDsImporter);
end;

function TExtAppTaskAdapter.GetExtAppConnectionParam: TExtAppConnectionParam;
begin
  Result := LoadExtAppConnectionParam(FSettings, CompanyUniquePath(FTask.Company));
end;

end.
