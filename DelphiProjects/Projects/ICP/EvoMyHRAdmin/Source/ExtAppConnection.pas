{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
unit ExtAppConnection;

interface

uses
  extappdecl, gdyCommonLogger, xmlintf, evodata, IdException,
  plannedActions, evoapiconnectionutils, resp;

type
  TExtTable = (etCompany, etJobTitle, etWorkersComp, etJob, etCompanyDeduction,
    etPayrollStatus, etTaxState, etStateMaritalStatus,
    etDivision, etLocation, etDepartment, etTeam, etEmployee, etPayFrequency, etRate, etDeduction, etCalcType, etPayGroup,

    etEmployeeGuid, etPositionGuid, etWCGuid, etJobGuid, etEDGuid, etPaygroupGuid, etSMSGuid, etRateGuid, etDeductionGuid);

  TTableLevel = (tlSystem, {tlClient,} tlCompany);

  TExtTableDesc = record
    tableLevel: TTableLevel;
    viewId: integer;
    name: string;
    codeField: string;
    nameField: string;
    parentField: string;
    fieldList: string;
    ///
    SingularName: string;
    PluralName: string;
    Fields: array of string;
  end;

  IExtAppRecord = interface
['{80A5B2F7-36E9-4405-B484-80DA4078E533}']
    function GetAsString(name: string): string;
    function GetAsInteger(name: string): integer;
    function GetAsDateVar(name: string): Variant;
    function GetAsComposite(name: string): IExtAppRecord;
    function GetAsCompositeCode(name: string): Variant;
    function GetAsCompositeId(name: string): Variant;
    property AsString[name: string]: string read GetAsString;
    property AsInteger[name: string]: integer read GetAsInteger;
    property AsDateVar[name: string]: Variant read GetAsDateVar;
    property AsComposite[name: string]: IExtAppRecord read GetAsComposite;
    property AsCompositeCode[name: string]: Variant read GetAsCompositeCode;
    property AsCompositeId[name: string]: Variant read GetAsCompositeId;
    function Id: integer;
    function Name: string;
    function IsCode(code: string): boolean;
    function Code: string;
  end;

  IExtAppResp = interface
['{89C058CF-049B-41E8-985E-6FF5FD8D1B2C}']
    function Desc: TExtTableDesc;
    function Count: integer;
    function GetRec(i: integer): IExtAppRecord;
    function GetRecordByCode(code: widestring): IExtAppRecord;
    function FindRecordByCode(code: widestring): IExtAppRecord;
    function FindRecordByCodeAndParentId(code: widestring; parentId: integer): IExtAppRecord;
    function GetRecordByCodeAndParentId(code: widestring; parentId: integer): IExtAppRecord;
    function FindRecordByCodeAndParentCode(code: widestring; parentCode: widestring): IExtAppRecord;
    function FindSchedED(eecode: string; edcode: string; effStartDate: TDateTime): IExtAppRecord;
    property Rec[i: integer]: IExtAppRecord read GetRec;
  end;

  IExtAppConnection = interface
  ['{8FD1D745-099F-4C59-A88B-9FC510FF0430}']
    procedure CreateObject(table: TExtTable; action: TSyncActionInfo);
    procedure UpdateObject(table: TExtTable; action: TSyncActionInfo);
    function GetTable(table: TExtTable): IExtAppResp;
    function GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
  end;

function CreateExtAppConnection(param: TExtAppConnectionParam; logger: ICommonLogger): IExtAppConnection;
function ExtTableName(table: TExtTable): string;
function ExtTableDesc(table: TExtTable): TExtTableDesc;

implementation

uses
  IdHTTP, IdSSLOpenSSL, idintercept, gdyClasses, classes, gdycommon, common, DateUtils,
  gdyRedir, variants, XMLConst, xmldoc, sysutils, iduri, XmlRpcCommon, gdystrset, windows, gdyGlobalWaitIndicator, isSettings;
//  evConsts;

type
  EExtAppRelogin = class(EExtAppError);

  TExtAppConnection = class(TInterfacedObject, IExtAppConnection)
  private
    FSentLog: string;
    FReceivedLog: string;
    procedure HandleSend(ASender: TIdConnectionIntercept; AStream: TStream);
    procedure HandleReceive(ASender: TIdConnectionIntercept; AStream: TStream);
    function Params: IStr;
    function CallGetPage(params: IStr; dumpname: string): IXMLDocument;
    procedure FetchObjectDefinition(table: TExtTable);
  private
    FServer: string;
    FLogger: ICommonLogger;
    FSession: string;
    procedure Login(param: TExtAppConnectionParam);
    function Call(params: TStrings; name: string; xml: string = ''; dumpname: string = ''): IXMLDocument;
    function ToXMLDoc(res: string): IXMLDocument;
    function MakeGetPageParams(table: TExtTable): IStr;
    procedure FetchObjectDefinitions;
  private
    {IExtAppConnection}
    function GetTable(table: TExtTable): IExtAppResp;
    function GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
    procedure CreateObject(table: TExtTable; action: TSyncActionInfo);
    procedure UpdateObject(table: TExtTable; action: TSyncActionInfo);
  public
    constructor Create(logger: ICommonLogger; param: TExtAppConnectionParam);
    destructor Destroy; override;
  end;

  TStableExtAppConnection = class (TInterfacedObject, IExtAppConnection)
  public
    constructor Create(logger: ICommonLogger; param: TExtAppConnectionParam);
  private
    FExtAppConnectionParam: TExtAppConnectionParam;
    FLogger: ICommonLogger;
    FConn: IExtAppConnection;
    FRetryCount: integer;

    procedure ReconnectOrRaise;
    procedure BeforeCall;

    {IExtAppConnection}
    function GetTable(table: TExtTable): IExtAppResp;
    function GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
    procedure CreateObject(table: TExtTable; action: TSyncActionInfo);
    procedure UpdateObject(table: TExtTable; action: TSyncActionInfo);
  end;

  TUniqueExtAppConnection = class (TInterfacedObject, IExtAppConnection)
  public
    constructor Create(logger: ICommonLogger; param: TExtAppConnectionParam);
    destructor Destroy; override;
  private
    FLogger: ICommonLogger;
    FMutex: THandle;
    FConn: IExtAppConnection;
//  doesn't work (dtor isn't called); probably need aggregated object here
//  protected
//    property Conn: IExtAppConnection read FConn implements IExtAppConnection;

    {IExtAppConnection}
    function GetTable(table: TExtTable): IExtAppResp;
    function GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
    procedure CreateObject(table: TExtTable; action: TSyncActionInfo);
    procedure UpdateObject(table: TExtTable; action: TSyncActionInfo);
  end;


var
  ExtTableDescs: array [TExtTable] of TExtTableDesc =
  (
    (tableLevel: tlSystem; ViewId: 27384;   name: 'hr_companies'; codeField: 'company_code'{; nameField: 'name'}),
    (tableLevel: tlCompany; ViewId: 1966745; name: 'job_title'; codeField: 'name'; nameField: 'job_description'),
    (tableLevel: tlCompany; ViewId: 1966739; name: 'hr_workerscomp'; codeField: 'wc_code'; nameField: 'wc_description'),
    (tableLevel: tlCompany; ViewId: 1966696; name: 'job_level_5'; codeField: 'job_code'; nameField: 'description'),
    (tableLevel: tlCompany; ViewId: 1966719; name: 'company_deduction'; codeField: 'ded_code'; nameField: 'field_name'),
    (tableLevel: tlSystem; ViewId: 2152962; name: 'payroll_status'; codeField: 'integration_code'; nameField: 'name'),
    (tableLevel: tlSystem; ViewId: 2152958; name: 'hr_tax_states'; codeField: 'name'),
    (tableLevel: tlSystem; ViewId: 2152960; name: 'state_marital_status'; codeField: 'marital_status_code'; nameField: 'name'; parentField: 'tax_state'),
    (tableLevel: tlCompany; ViewId: 1966998; name: 'division_level_1'; codeField: 'level_1_code'; nameField: 'level_1_name'; fieldList: 'level_1_guid'),
    (tableLevel: tlCompany; ViewId: 1967003; name: 'location_level_2'; codeField: 'level_2_code'; nameField: 'level_2_name'; parentField: 'division_level_1'; fieldList: 'location_guid'),
    (tableLevel: tlCompany; ViewId: 1966989; name: 'department_level_3'; codeField: 'level_3_code'; nameField: 'level_3_name'; parentField: 'location_level_2'; fieldList: 'department_guid'),
    (tableLevel: tlCompany; ViewId: 1967008; name: 'team_level_4'; codeField: 'level_4_code'; nameField: 'level_4_name'; parentField: 'department_level_3'; fieldList: 'level_4_guid'),
    (tableLevel: tlCompany; ViewId: 1966950; name: 'employee'; codeField: 'empid'; fieldList: 'firstName,lastName,ssn,date_of_birth,hire_date,streetAddr1,streetAddr2,city,zip,'+
    'state,email,ethnicity,payroll_status,status,term_date,gender,middleName,pay_frequency,federal_marital_stat,federal_dependents,tax_state,state_marital_status,'+
    'state_dependents,veteran,vietnam_veteran,phone,mobilePhone,original_hire_date,rehire_eligible,salary_amount,sui_state,new_hire_report_sent,'+
    'flsa_exempt,w_2_or_1099_employee,i_9_on_file,military_reserve,smoker,job_title,workers_comp,job_level_5,division_level_1,location_level_2,department_level_3,team_level_4,'+
    'federal_override,additional_federal,state_override,additional_state,state_tax_exempt,standard_hours,vmr_password,pay_group'),
    (tableLevel: tlSystem; ViewId: 24871; name: 'hr_pay_frequencies'; codeField: 'payfreq_code'),
    (tableLevel: tlCompany; ViewId: 1966967; name: 'hr_pay_rate_info'; codeField: 'rate_number'; parentField: 'employee'; fieldList: 'primary_rate,rate_amount,start_date,end_date'), {number_of_pays}
    (tableLevel: tlCompany; ViewId: 1966960; name: 'deduction'; parentField: 'employee'; fieldList: 'deduction_code,eff_start_date,eff_end_date,amount,percentage,calculation_type'),
    (tableLevel: tlSystem; ViewId: 29119; name: 'calculation_type'; codeField: 'integration_code'; nameField: 'name'),
    (tableLevel: tlCompany; ViewId: 5869532; name: 'pay_group'; codeField: 'name'),

    (tableLevel: tlCompany; ViewId: 1966950; name: 'employee'; codeField: 'empid'; fieldList: 'employee_guid'),
    (tableLevel: tlCompany; ViewId: 1966745; name: 'job_title'; codeField: 'name'; fieldList: 'job_title_guid'),
    (tableLevel: tlCompany; ViewId: 1966739; name: 'hr_workerscomp'; codeField: 'wc_code'; fieldList: 'workers_comp_guid'),
    (tableLevel: tlCompany; ViewId: 1966696; name: 'job_level_5'; codeField: 'job_code'; fieldList: 'job_level_5_guid'),
    (tableLevel: tlCompany; ViewId: 1966719; name: 'company_deduction'; codeField: 'ded_code'; fieldList: 'deduction_guid'),
    (tableLevel: tlCompany; ViewId: 5869532; name: 'pay_group'; codeField: 'name'; fieldList: 'pay_group_guid'),
    (tableLevel: tlSystem; ViewId: 2152960; name: 'state_marital_status'; codeField: 'marital_status_code'; parentField: 'tax_state'; fieldList: 'guid'),
    (tableLevel: tlCompany; ViewId: 1966967; name: 'hr_pay_rate_info'; codeField: 'rate_number'; parentField: 'employee'; fieldList: 'rates_guid'),
    (tableLevel: tlCompany; ViewId: 1966960; name: 'deduction'; parentField: 'employee'; fieldList: 'deduction_guid,deduction_code,eff_start_date')

  );

  cNumberOfRecordsRequested: integer = 1000;

function ExtTableName(table: TExtTable): string;
begin
  Result := LowerCase(ExtTableDescs[table].name);
end;

function ExtTableDesc(table: TExtTable): TExtTableDesc;
begin
  Result := ExtTableDescs[table];
end;

type
  TExtCompositeDesc = record
    table: TExtTable;
    field: string;
    compositeTable: TExtTable;
  end;

const
  ExtCompositeDescs: array [0..24] of TExtCompositeDesc = (
    (table: etLocation; field: 'division_level_1'; compositeTable: etDivision),
    (table: etDepartment; field: 'location_level_2'; compositeTable: etLocation),
    (table: etTeam; field: 'department_level_3'; compositeTable: etDepartment),
    (table: etStateMaritalStatus; field: 'tax_state'; compositeTable: etTaxState),
    (table: etEmployee; field: 'payroll_status'; compositeTable: etPayrollStatus),
    (table: etEmployee; field: 'pay_frequency'; compositeTable: etPayFrequency),
    (table: etEmployee; field: 'tax_state'; compositeTable: etTaxState),
    (table: etEmployee; field: 'state_marital_status'; compositeTable: etStateMaritalStatus),
    (table: etEmployee; field: 'sui_state'; compositeTable: etTaxState),
    (table: etEmployee; field: 'job_title'; compositeTable: etJobTitle),
    (table: etEmployee; field: 'workers_comp'; compositeTable: etWorkersComp),
    (table: etEmployee; field: 'job_level_5'; compositeTable: etJob),
    (table: etEmployee; field: 'division_level_1'; compositeTable: etDivision),
    (table: etEmployee; field: 'location_level_2'; compositeTable: etLocation),
    (table: etEmployee; field: 'department_level_3'; compositeTable: etDepartment),
    (table: etEmployee; field: 'team_level_4'; compositeTable: etTeam),
    (table: etEmployee; field: 'pay_group'; compositeTable: etPayGroup),
    (table: etRate; field: 'employee'; compositeTable: etEmployee),
    (table: etDeduction; field: 'employee'; compositeTable: etEmployee),
    (table: etDeduction; field: 'deduction_code'; compositeTable: etCompanyDeduction),
    (table: etDeduction; field: 'calculation_type'; compositeTable: etCalcType),
    (table: etRateGuid; field: 'employee'; compositeTable: etEmployeeGuid),
    (table: etDeductionGuid; field: 'employee'; compositeTable: etEmployeeGuid),
    (table: etDeductionGuid; field: 'deduction_code'; compositeTable: etCompanyDeduction),
    (table: etSMSGuid; field: 'tax_state'; compositeTable: etTaxState)
  );

function ExtComposite(table: TExtTable; field: string): TExtTable;
var
  i: integer;
begin
  field := trim(field);
  for i := low(ExtCompositeDescs) to high(ExtCompositeDescs) do
    if (ExtCompositeDescs[i].table = table) and SameText(trim(ExtCompositeDescs[i].field), field) then
    begin
      Result := ExtCompositeDescs[i].compositeTable;
      exit;
    end;
  raise Exception.CreateFmt('No composite metadata for %s.%s', [ExtTableName(table), field]);
end;

//!!kludge
function DeXmlize(v: Variant): string;
begin
  Result := trim(DecodeEntities(VarToStr(v)));
end;


function CreateExtAppConnection(param: TExtAppConnectionParam; logger: ICommonLogger): IExtAppConnection;
begin
  Result := TUniqueExtAppConnection.Create(logger, param);
end;

const
  sCtxComponentExtApp = ExtAppName + ' connection';

//from XmlDoc.pas, there are another version of this function in xmlutil.pas, I have no idea which one is better
function CloneNodeToDoc(const SourceNode: IXMLNode; const TargetDoc: IXMLDocument;
  Deep: Boolean = True): IXMLNode;
var
  I: Integer;
begin
  with SourceNode do
    case nodeType of
      ntElement:
        begin
          Result := TargetDoc.CreateElement(NodeName, NamespaceURI);
          for I := 0 to AttributeNodes.Count - 1 do
            Result.AttributeNodes.Add(CloneNodeToDoc(AttributeNodes[I], TargetDoc, False));
          if Deep then
            for I := 0 to ChildNodes.Count - 1 do
              Result.ChildNodes.Add(CloneNodeToDoc(ChildNodes[I], TargetDoc, Deep));
        end;
      ntAttribute:
        begin
          Result := TargetDoc.CreateNode(NodeName, ntAttribute, NamespaceURI);
          Result.NodeValue := NodeValue;
        end;
      ntText, ntCData, ntComment:
          Result := TargetDoc.CreateNode(NodeValue, NodeType);
      ntEntityRef:
          Result := TargetDoc.createNode(nodeName, NodeType);
      ntProcessingInstr:
          Result := TargetDoc.CreateNode(NodeName, ntProcessingInstr, NodeValue);
      ntDocFragment:
        begin
          Result := TargetDoc.CreateNode('', ntDocFragment);
          if Deep then
            for I := 0 to ChildNodes.Count - 1 do
              Result.ChildNodes.Add(CloneNodeToDoc(ChildNodes[I], TargetDoc, Deep));
        end;
      else
       {ntReserved, ntEntity, ntDocument, ntDocType:}
        XMLDocError(SInvalidNodeType);
    end;
end;

procedure CopyChildNodes(SrcNode, DestNode: IXMLNode);
var
  i: integer;
begin
  for i := 0 to SrcNode.ChildNodes.Count - 1 do
    DestNode.ChildNodes.Add( CloneNodeToDoc(SrcNode.ChildNodes[i], DestNode.OwnerDocument, true) );
end;

type
  IExtAppRecordInternal = interface
  ['{4969BA77-0ED4-4428-B5DA-5BF651A5B3BF}']
    function IndexKey: string;
  end;

  TExtAppRecord = class(TInterfacedObject, IExtAppRecord, IExtAppRecordInternal)
  private
    FRoot: IExtAppResp;
    FExtTable: TExtTable;
    FDesc: TExtTableDesc;
    FRec: IXMLDataType;
    function GetAsString(name: string): string;
    function GetAsInteger(name: string): integer;
    function GetAsDateVar(name: string): Variant;
    function GetAsComposite(name: string): IExtAppRecord;
    function GetAsCompositeCode(name: string): Variant;
    function GetAsCompositeId(name: string): Variant;
    function IndexKey: string;
    constructor Create(resp: IExtAppResp; rec: IXMLDataType);
    function Id: integer;
    function Name: string;
    function IsCode(code: string): boolean;
    function Code: string;
  end;

  TExtAppResp = class(TInterfacedObject, IExtAppResp)
  private
    FResp: IXMLRespType;
    FIndex: TStringList;
    FExtTable: TExtTable;
    FLogger: ICommonLogger; 
    function Desc: TExtTableDesc;
    function Count: integer;
    function GetRec(i: integer): IExtAppRecord;
    function GetRecordByCode(code: widestring): IExtAppRecord;
    function FindRecordByCode(code: widestring): IExtAppRecord;
    function FindRecordByCodeAndParentId(code: widestring; parentId: integer): IExtAppRecord;
    function FindRecordByCodeAndParentCode(code: widestring; parentCode: widestring): IExtAppRecord;
    function GetRecordByCodeAndParentId(code: widestring; parentId: integer): IExtAppRecord;
    procedure BuildIndex;
    function FindSchedED(eecode, edcode: string; effStartDate: TDateTime): IExtAppRecord;
  public
    constructor Create(logger: ICommonLogger; xml: IXMLDocument; table: TExtTable);
    destructor Destroy; override;
  end;

{ TExtAppConnection }

procedure TExtAppConnection.HandleReceive(ASender: TIdConnectionIntercept; AStream: TStream);
begin
  FReceivedLog := FReceivedLog + StreamToString(AStream);
end;

procedure TExtAppConnection.HandleSend(ASender: TIdConnectionIntercept; AStream: TStream);
begin
  FSentLog := FSentLog + StreamToString(AStream);
end;

type
  TMyIdHTTPRequest = class(TIdHTTPRequest)
  protected
    procedure SetUseProxy(aValue: TIdHTTPConnectionType);
  end;

  TMyIdHttp = class(TIdHttp)
  protected
    procedure MyConnectToHost(ARequest: TMyIdHTTPRequest; AResponse: TIdHTTPResponse);
  public
    procedure DoRequest(const AMethod: TIdHTTPMethod; AURL: string;
      const ASource, AResponseContent: TStream); override;
  end;

  TMyMemoryStream = class(TMemoryStream)
  end;

function TExtAppConnection.Call(params: TStrings; name, xml, dumpname: string): IXMLDocument;
var
  url: string;
  uri: TIdURI;
  http: TMyIdHttp;
  ssl: TIdSSLIOHandlerSocket;
  res: string;
  intercept: TIdConnectionIntercept;
  ss: TStringStream;
  respStream: TMyMemoryStream;
  err: string;
  lStartTime: TDateTime;
begin
  FLogger.LogEntry('Call');
  try
    try
      FLogger.LogContextItem(sCtxComponent, sCtxComponentExtApp);
    //  FLogger.LogDebug( name + ' ' + dumpname + ' request (xml)', req );
    //{$ifndef FINAL_RELEASE}
    //  StringToFile( req, Redirection.GetDirectory(sDumpDirAlias) + name + ' ' + dumpname + ' request.xml');
    //{$endif}

      url := FServer + '/rest/api/' + name;

      http := TMyIdHttp.Create(nil);
      try
        http.ReadTimeout := 1000*60*60*6; //6 hours
        uri := TIdURI.Create(url);
        try
          uri.Params := '?' + http.SetRequestParams(params);
          url := uri.uri;
        finally
          FreeAndNil(uri);
        end;

        if Pos('https://', url) = 1 then
        begin
          ssl := TIdSSLIOHandlerSocket.Create(http);
          ssl.SSLOptions.Method := sslvSSLv23;
          http.IOHandler := ssl;
        end;
    //    http.HTTPOptions := http.HTTPOptions - [hoForceEncodeParams];

        intercept := TIdConnectionIntercept.Create(http);
        intercept.OnSend := HandleSend;
        intercept.OnReceive := HandleReceive;
        http.Intercept := intercept;

        http.Request.Accept := '*/*'; // all media types
        http.Request.Connection := 'keep-alive';

        FSentLog := '';
        FReceivedLog := '';
        try
          lStartTime := Now;
          try
            if xml <> '' then
            begin
              http.Request.ContentType := 'text/xml';
              ss := TStringStream.Create( Utf8Encode('<?xml version="1.0" encoding="utf-8" ?>' + xml));
              try
                res := http.Post(url, ss);
              finally
                FreeAndNil(ss);
              end
            end
            else
            begin
              respStream := TMyMemoryStream.Create;
              try
                respStream.Capacity := 1024*1024;
                http.Response.KeepAlive := True;
                http.Get(url, respStream);
                SetLength(res, respStream.Size);
                if respStream.Size > 0 then
                  Move(PChar(respStream.Memory)^, res[1], respStream.Size);
              finally
                FreeAndNil(respStream);
              end;
            end
          except
            on E: EIdHTTPProtocolException do
            begin
              err := trim(E.ErrorMessage);
              if (err <> '') and (err[1] = '<') and (Pos('<!DOCTYPE', err) <> 1) then //hopefully it is xml
                {Result := }ToXmlDoc(err); //will throw meaningful exception when getting error 500
              raise;
            end;
            on E: EIdConnClosedGracefully do
            begin
              FLogger.LogWarning( Format('After %9.3f sec MyHrAdmin server closed connetion', [SecondSpan(Now, lStartTime)]) );
              raise;
            end;
            else
              raise;
          end;
        finally
          FLogger.LogDebug( name + ' ' + dumpname + ' request', FSentLog );
    {$ifndef FINAL_RELEASE}
          StringToFile( FSentLog, Redirection.GetDirectory(sDumpDirAlias) + name + ' ' + dumpname + ' request.txt');
    {$endif}
          FLogger.LogDebug( name + ' ' + dumpname + ' response', FReceivedLog );
      {$ifndef FINAL_RELEASE}
          StringToFile( FReceivedLog, Redirection.GetDirectory(sDumpDirAlias) + name + ' ' + dumpname + ' response.txt');
          StringToFile( res, Redirection.GetDirectory(sDumpDirAlias) + name + ' ' + dumpname + ' response.xml');
      {$endif}
        end;
        Result := ToXmlDoc(res);
      finally
        FreeAndNil(http);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.ToXMLDoc(res: string): IXMLDocument;
var
  status: IXMLNode;
  err: string;
begin
  Result := LoadXMLData(res);
  Result.Options := Result.Options - [doNodeAutoCreate] + [doNodeAutoIndent];

  if Result.DocumentElement = nil then
    raise Exception.Create(ExtAppName + ' returned nothing');
  {if Result.DocumentElement.NodeName <> 'resp' then
    raise Exception.Create(ExtAppName + 'response doesn''t have <resp> element');}

  status := Result.DocumentElement.AttributeNodes.FindNode('status');

{  if status = nil then
    raise Exception.Create(ExtAppName + ' response doesn''t have status attribute');}

  if Result.DocumentElement.ChildNodes.FindNode('err') <> nil then
		err := VarToStr(Result.DocumentElement.ChildValues['err']);

  if status <> nil then
  begin
    if status.Text = 'fail' then
    begin
      if Result.DocumentElement.ChildNodes.FindNode('err') = nil then
        raise Exception.Create(ExtAppName + ' response doesn''t have <err> element');

      raise EExtAppError.Create(ExtAppName + ' error: ' + err );
    end
    else if status.Text = 'login' then //session expired or invalid login creds
    begin
      FSession := ''; //thus avoiding logout in dtor
      raise EExtAppRelogin.Create(ExtAppName + ' responded with status <login>: ' + err)
    end
    else if status.Text <> 'ok' then
      raise EExtAppError.Create(ExtAppName + ' response has unknown status: ' + status.Text + ', error text:' + err);
  end;    
end;

constructor TExtAppConnection.Create(logger: ICommonLogger; param: TExtAppConnectionParam);
begin
  Flogger := logger;
  FServer := ReadFromIniFile(Redirection.Getfilename(sConfigFileAlias), 'MyHRAdmin\Server', '');
  if FServer = '' then
    raise Exception.CreateFmt('%s doesn''t specify MyHRAdmin server url.', [Redirection.Getfilename(sConfigFileAlias)]);
  Login(param);
  FetchObjectDefinitions;
end;

procedure TExtAppConnection.Login(param: TExtAppConnectionParam);
begin
  FLogger.LogEntry(Format('%s login', [ExtAppName]));
  try
    try
      FLogger.LogContextItem(ExtAppName+' User Name', param.UserName);

      with EmptyIStr do
      begin
        Add('loginName='+ param.UserName);
        Add('password='+ param.Password);
        FSession := Call(AsTStrings, 'login').ChildNodes['resp'].ChildValues['sessionId'];
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.Params: IStr;
begin
  Result := EmptyIStr;
  Result.Add('sessionId='+ FSession);
end;

destructor TExtAppConnection.Destroy;
begin
//!! this dtor may be called from finally block and by calling LogEntry erase info that exception was already reported causing double reporting
  if FSession <> '' then
  try
    Call(Params.AsTStrings, 'logout');
  except
    FLogger.StopException;
  end;
  inherited;
end;

function TExtAppConnection.MakeGetPageParams(table: TExtTable): IStr;
var
  desc: TExtTableDesc;
  fieldList: string;
  objNames: string;
  i: integer;
  bNeedsComposite: boolean;
  compTableDesc: TExtTableDesc;
begin
  desc := ExtTableDescs[table];
  objNames := desc.name + ',';
  fieldList := 'id,name';
  if desc.codeField <> '' then
    fieldList := fieldList + ',' + desc.codeField;
  if desc.nameField <> '' then
    fieldList := fieldList + ',' + desc.nameField;
  if desc.parentField <> '' then
    fieldList := fieldList + ',' + desc.parentField;
  if desc.fieldList <> '' then
    fieldList := fieldList + ',' + desc.fieldList;

  Result := Params;

  bNeedsComposite := false;
  for i := low(ExtCompositeDescs) to high(ExtCompositeDescs) do
    if ExtCompositeDescs[i].table = table then
    begin
      bNeedsComposite := true;
      compTableDesc := ExtTableDesc(ExtCompositeDescs[i].compositeTable);
      objNames := objNames + ',' + compTableDesc.name;
      fieldList := fieldList + ',' + compTableDesc.codeField;
      if compTableDesc.nameField <> '' then
        fieldList := fieldList + ',' + compTableDesc.nameField;
    end;

  if bNeedsComposite then
  begin
    Result.Add('composite=1');
    Result.Add('objNames='+objNames);
  end;
  Result.Add(Format('viewId=%d',[desc.viewId]));
  Result.Add(Format('fieldList=%s',[fieldList]));
  Result.Add(Format('rowsPerPage=%d', [cNumberOfRecordsRequested]));
end;

function TExtAppConnection.CallGetPage(params: IStr; dumpname: string): IXMLDocument;
var
  p: IStr;
  recs: IXMLDocument;
  offset: integer;
begin
  FLogger.LogEntry('CallGetPage()');
  try
    try
      Result := nil;
      offset := 0;
      repeat
        p := params.Clone;
        p.Add(Format('startRow=%d',[offset]));
        recs := Call(p.AsTStrings, 'getPage', '', Format('%s(%d)', [dumpname, offset]));
        if Result = nil then
          Result := recs
        else
          CopyChildNodes(recs.DocumentElement, Result.DocumentElement);
        offset := offset + cNumberOfRecordsRequested;
      until recs.DocumentElement.ChildNodes.Count < cNumberOfRecordsRequested;

      //magic, mimics TExtAppConnection.ToXmlDoc
      Result := LoadXMLData(Result.XML.Text);
      Result.Options := Result.Options - [doNodeAutoCreate] + [doNodeAutoIndent];

      FLogger.LogDebug( 'getPage ' + dumpname + '(combined) response (xml)', Result.XML.Text );
{$ifndef FINAL_RELEASE}
      StringToFile( Result.XML.Text, Redirection.GetDirectory(sDumpDirAlias) + 'getPage ' + dumpname + '(combined) response.xml');
{$endif}
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.GetTable(table: TExtTable): IExtAppResp;
begin
  FLogger.LogEntry('GetTable');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', ExtTableDescs[table].PluralName);
      Result := TExtAppResp.Create( FLogger, CallGetPage(MakeGetPageParams(table), ExtTableDescs[table].PluralName), table );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppConnection.GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
var
  params: IStr;
begin
  FLogger.LogEntry('GetCompanyTable');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', ExtTableDescs[table].PluralName);
      params := MakeGetPageParams(table);
      params.Add('filterName=company');
      params.Add(Format('filterValue=%d',[companyRecordId]));
      Result := TExtAppResp.Create( FLogger, CallGetPage(params, ExtTableDescs[table].PluralName), table );
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.CreateObject(table: TExtTable; action: TSyncActionInfo);
begin
  FLogger.LogEntry('CreateObject');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', ExtTableDescs[table].PluralName);
      Call(Params.AsTStrings, 'create', action.NewVersion, ExtTableName(table));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.UpdateObject(table: TExtTable; action: TSyncActionInfo);
begin
  FLogger.LogEntry('UpdateObject');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', ExtTableDescs[table].PluralName);
      Call(Params.AsTStrings, 'update', action.NewVersion, ExtTableName(table));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.FetchObjectDefinition(table: TExtTable);
var
  params: IStr;
  def: IXMLDocument;
  fields: IXMLNodeList;
  i: integer;
begin
  FLogger.LogEntry('getObjectDef');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', ExtTableName(table));
      params := Self.Params;
      params.Add(Format('objName=%s', [ExtTableName(table)]));
      def := Call(Params.AsTStrings, 'getObjectDef', '', ExtTableName(table));
      ExtTableDescs[table].SingularName := VarToStr(def.DocumentElement.{ChildNodes['DataObjectDef'].}ChildValues['SingularName']);
      ExtTableDescs[table].PluralName := VarToStr(def.DocumentElement.{ChildNodes['DataObjectDef'].}ChildValues['PluralName']);
      fields := def.DocumentElement.{ChildNodes['DataObjectDef'].}ChildNodes['DataFieldDefs'].ChildNodes;
      SetLength(ExtTableDescs[table].Fields, fields.Count);
      for i := 0 to fields.Count-1 do
        ExtTableDescs[table].Fields[i] := LowerCase(VarToStr(fields[i].Attributes['fieldName']));
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppConnection.FetchObjectDefinitions;
var
  table: TExtTable;
begin
  FLogger.LogEntry('FetchObjectDefinitions');
  try
    try
      for table := low(TExtTable) to high(TExtTable) do
        FetchObjectDefinition(table);
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

{ TExtAppResp }

function TExtAppResp.Count: integer;
begin
  Result := FResp.Count;
end;

constructor TExtAppResp.Create(logger: ICommonLogger; xml: IXMLDocument; table: TExtTable);
begin
  FLogger := logger;
  FResp := Getresp(xml);
  FExtTable := table;
  FIndex := TStringList.Create;
  FIndex.Sorted := true;
  FIndex.Duplicates := dupIgnore;//dupError; //EStringListError
  BuildIndex;
  inherited Create;
end;

procedure TExtAppResp.BuildIndex;
var
  i: integer;
  rec: IExtAppRecord;
  key: string;
begin
  FLogger.LogEntry('BuildIndex');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', Desc.PluralName);
      FIndex.Clear;
      FIndex.Capacity := FResp.Count;
      for i := 0 to FResp.Count-1 do
      begin
{
        FLogger.LogEntry('processing record');
        try
          try
}
            rec := GetRec(i);
//            FLogger.LogContextItem('id', inttostr(rec.Id));
            key := (rec as IExtAppRecordInternal).IndexKey;
//            FLogger.LogContextItem('key', key);
            FIndex.AddObject(key, Pointer(i));
{
          except
            FLogger.PassthroughException;
          end;
        finally
          FLogger.LogExit;
        end;
}
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppResp.Desc: TExtTableDesc;
begin
  Result := ExtTableDescs[FExtTable];
end;

const
  sIndexFieldsSeparator = '|';

function TExtAppResp.FindRecordByCode(code: widestring): IExtAppRecord;
var
  idx: integer;
  key: string;
begin
  FLogger.LogEntry('FindRecordByCode');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', Desc.PluralName);
      FLogger.LogContextItem('Code', code);
      Assert(Desc.parentField = '');
      Assert(FExtTable <> etDeduction);
      key := trim(code);
      FLogger.LogContextItem('key', key);
      if FIndex.Find(key, idx) then
      begin
        Result := GetRec( Integer(Pointer(FIndex.Objects[idx])) );
        FLogger.LogDebug(Format('Found a record: id=%d, code=%s', [Result.Id, Result.Code]));
        Assert(Result.IsCode(code));
      end
      else
        Result := nil;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppResp.FindRecordByCodeAndParentID(code: widestring; parentId: integer): IExtAppRecord;
var
  idx: integer;
  key: string;
begin
  FLogger.LogEntry('FindRecordByCodeAndParentID');
  try
    try
      FLogger.LogContextItem(ExtAppName+' table', Desc.PluralName);
      FLogger.LogContextItem('Code', code);
      FLogger.LogContextItem('Parent ID', IntToStr(parentId));

      Assert(Desc.parentField <> '');
      Assert(FExtTable <> etDeduction);
      key := IntToStr(parentId) + sIndexFieldsSeparator + trim(code);
      FLogger.LogContextItem('key', key);
      if FIndex.Find(key, idx) then
      begin
        Result := GetRec( Integer(Pointer(FIndex.Objects[idx])) );
        FLogger.LogDebug(Format('Found a record: id=%d, code=%s', [Result.Id, Result.Code]));
        Assert(Result.IsCode(code));
        Assert(Result.AsCompositeId[Desc.parentField] = parentId);
      end
      else
        Result := nil;

    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppResp.FindSchedED(eecode: string; edcode: string; effStartDate: TDateTime): IExtAppRecord;
var
  idx: integer;
  key: string;
begin
  Assert((FExtTable = etDeduction) or (FExtTable = etDeductionGuid));
  key := trim(eecode) + sIndexFieldsSeparator + trim(edcode) + sIndexFieldsSeparator + DateToStr(effStartDate);
  if FIndex.Find(key, idx) then
  begin
    Result := GetRec( Integer(Pointer(FIndex.Objects[idx])) );
    Assert(trim(VarToStr(Result.AsCompositeCode['employee'])) = trim(eecode));
    Assert(trim(VarToStr(Result.AsCompositeCode['deduction_code'])) = trim(edcode));
    Assert(Result.AsDateVar['eff_start_date'] = effStartDate);
  end
  else
    Result := nil;
end;

function TExtAppResp.GetRec(i: integer): IExtAppRecord;
begin
  Result := TExtAppRecord.Create(Self, FResp.Data[i]);
end;

function TExtAppResp.GetRecordByCode(code: widestring): IExtAppRecord;
begin
  Result := FindRecordByCode(code);
  if Result = nil then
    raise Exception.CreateFmt('Cannot find a %s %s with code %s', [ExtAppName, LowerCase(Desc.SingularName), code]);
end;

function TExtAppResp.FindRecordByCodeAndParentCode(code, parentCode: widestring): IExtAppRecord;
var
  i: integer;
begin
  Result := nil;
  parentCode := trim(parentCode);
  for i := 0 to FResp.Count-1 do
    if GetRec(i).IsCode(code) and (trim(VarToStr(GetRec(i).AsCompositeCode[Desc.parentField])) = parentCode) then
    begin
      Result := GetRec(i);
      Exit;
    end;
end;

function TExtAppResp.GetRecordByCodeAndParentId(code: widestring; parentId: integer): IExtAppRecord;
begin
  Result := FindRecordByCodeAndParentId(code, parentId);
  if Result = nil then
    raise Exception.CreateFmt('Cannot find a %s %s with code %s and parent Id %d', [ExtAppName, LowerCase(Desc.SingularName), code, parentId]);
end;

destructor TExtAppResp.Destroy;
begin
  FreeAndNil(FIndex);
  inherited;
end;

{ TExtAppRecord }

function TExtAppRecord.IsCode(code: string): boolean;
begin
  Result := trim(Self.Code) = trim(code);
end;

function TExtAppRecord.Code: string;
begin
  Assert(FDesc.codeField <> '');
  Result := GetAsString(FDesc.codeField);
end;

constructor TExtAppRecord.Create(resp: IExtAppResp; rec: IXMLDataType);
var
  table: TExtTable;
begin
  FRoot := resp;
  FRec := rec;

  for table := low(ExtTableDescs) to high(ExtTableDescs) do
    if ExtTableDescs[table].name = FRec.ObjName then
    begin
      FExtTable := table;
      FDesc := ExtTableDescs[table];
      exit;
    end;

  raise Exception.CreateFmt('Unexpected object name: %s', [FRec.ObjName]);
end;

function TExtAppRecord.GetAsString(name: string): string;
var
  i: integer;
begin
  name := trim(name);
  Assert(name <> '');
  for i := 0 to FRec.Field.Count-1 do
    if SameText(trim(FRec.Field[i].Name), name) then
    begin
      Result := DeXmlize(FRec.Field[i].NodeValue);
      exit;
    end;
  if InSet(LowerCase(name), FDesc.Fields) then
    Result := ''
  else
    raise Exception.CreateFmt('Cannot find %s field <%s> in %s (id = %d)', [ExtAppName, name, FRec.ObjName, FRec.Id]);
end;

function TExtAppRecord.Id: integer;
begin
  Result := FRec.Id;
end;

function TExtAppRecord.GetAsInteger(name: string): integer;
begin
  Result := StrToInt(GetAsString(name));
end;

function TExtAppRecord.Name: string;
begin
  Assert(FDesc.nameField <> '');
  Result := GetAsString(FDesc.nameField);
end;

function TExtAppRecord.GetAsComposite(name: string): IExtAppRecord;
var
  i: integer;
  val: string;
  compositeDesc: TExtTableDesc;
begin
  Result := nil;
  val := GetAsString(name);
  if val <> '' then
  begin
    compositeDesc := ExtTableDesc(ExtComposite(FExtTable, name));
    for i := 0 to FRec.Composite.Count-1 do
      if FRec.Composite.Data[i].ObjName = compositeDesc.name then
      begin
        Result := TExtAppRecord.Create(FRoot, FRec.Composite.Data[i]);
        if Result.AsString['name'] = val then
          exit
        else
          Result := nil;
      end;
    raise Exception.CreateFmt('Cannot find a subobject of type %s with name %s for composite object of type %s with code %s', [compositeDesc.name, val, FDesc.name, Self.Code]);
  end;
end;

function TExtAppRecord.GetAsDateVar(name: string): Variant;
var
  val: string;
begin
  val := GetAsString(name);
  if trim(val) = '' then
    Result := Null
  else
    Result := StdDateTimeToDateTime(val);
end;

function TExtAppRecord.GetAsCompositeCode(name: string): Variant;
var
  rec: IExtAppRecord;
begin
  rec := GetAsComposite(name);
  if rec = nil then
    Result := Null
  else
    Result := rec.Code;
end;

function TExtAppRecord.GetAsCompositeId(name: string): Variant;
var
  rec: IExtAppRecord;
begin
  rec := GetAsComposite(name);
  if rec = nil then
    Result := Null
  else
    Result := rec.Id;
end;

function TExtAppRecord.IndexKey: string;
begin
  if FExtTable = etDeduction then
    Result := trim(VarToStr(GetAsCompositeCode('employee'))) + sIndexFieldsSeparator + trim(VarToStr(GetAsCompositeCode('deduction_code'))) + sIndexFieldsSeparator + VarToStr(GetAsDateVar('eff_start_date'))
  else
  begin
    if FDesc.parentField = '' then
      Result := trim(Code)
    else
      Result := VarToStr(GetAsCompositeId(FDesc.parentField)) + sIndexFieldsSeparator + trim(Code)
  end
end;

{ TStableExtAppConnection }

constructor TStableExtAppConnection.Create(logger: ICommonLogger; param: TExtAppConnectionParam);
begin
  FExtAppConnectionParam := param;
  FLogger := logger;
  FConn := TExtAppConnection.Create(FLogger, FExtAppConnectionParam);
end;

procedure TStableExtAppConnection.BeforeCall;
begin
  FRetryCount := 0;
end;

procedure TStableExtAppConnection.ReconnectOrRaise;
begin
  Assert( ExceptObject <> nil );
  if (FRetryCount = 0) and (ExceptObject is EExtAppRelogin) then
  begin
    FConn := nil; //hits the same error again
    Flogger.LogEvent(Format('Reconnecting to %s',[ExtAppName]));
    FRetryCount := FRetryCount + 1;
    FConn := TExtAppConnection.Create(FLogger, FExtAppConnectionParam);
    FLogger.LogEvent(Format('Reconnected to %s',[ExtAppName]));
  end
  else
    Flogger.PassthroughException;
end;

procedure TStableExtAppConnection.CreateObject(table: TExtTable; action: TSyncActionInfo);
begin
  BeforeCall;
  while true do
  try
    FConn.CreateObject(table, action);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableExtAppConnection.GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetCompanyTable(companyRecordId, table);
    break;
  except
    ReconnectOrRaise;
  end;
end;

function TStableExtAppConnection.GetTable(table: TExtTable): IExtAppResp;
begin
  BeforeCall;
  while true do
  try
    Result := FConn.GetTable(table);
    break;
  except
    ReconnectOrRaise;
  end;
end;

procedure TStableExtAppConnection.UpdateObject(table: TExtTable; action: TSyncActionInfo);
begin
  BeforeCall;
  while true do
  try
    FConn.UpdateObject(table, action);
    break;
  except
    ReconnectOrRaise;
  end;
end;

{ TUniqueExtAppConnection }

type
  TSecAttr = record
    SecAttr: SECURITY_ATTRIBUTES;
    SecDesc: array [1..SECURITY_DESCRIPTOR_MIN_LENGTH] of Byte;
  end;

procedure BuildSecAttrWithNullDACL(out sa: TSecAttr);
begin
  // Remove any security restriction
  sa.secAttr.nLength := SizeOf(sa.secAttr);
  sa.secAttr.bInheritHandle := False;
  sa.secAttr.lpSecurityDescriptor := @sa.secDesc;
  InitializeSecurityDescriptor(sa.secAttr.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
  SetSecurityDescriptorDacl(sa.secAttr.lpSecurityDescriptor, True, nil, False);
end;

constructor TUniqueExtAppConnection.Create(logger: ICommonLogger; param: TExtAppConnectionParam);
var
  secAttr: TSecAttr;
begin
  FLogger := logger;
  FLogger.LogEntry('Acquiring myHRAdmin connection');
  try
    try
      BuildSecAttrWithNullDACL({out}secAttr);
      FMutex := CreateMutex(@secAttr.SecAttr, False, PChar('Global\MyHRAdminConnectionMutex'));
      Win32Check(FMutex <> 0);
      FLogger.LogDebug('Mutex created');
      WaitIndicator.StartWait(Format('Waiting for other instances to disconnect from %s',[ExtAppName]));
      try
        case WaitForSingleObject(FMutex, 12*60*60*1000) of
          WAIT_FAILED: Win32Check(false);
          WAIT_TIMEOUT: raise Exception.CreateFmt('%s connection has been used by other instances of the program on this PC for 12 hours. Waiting aborted.', [ExtAppName]);
          WAIT_OBJECT_0: ; //ok
          WAIT_ABANDONED: ; //ok
        end;
      finally
        WaitIndicator.EndWait;
      end;
      FLogger.LogDebug('Mutex acquired');
      FConn := TStableExtAppConnection.Create(logger, param);
      FLogger.LogDebug('connection created');
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

destructor TUniqueExtAppConnection.Destroy;
begin
  FLogger.LogEntry('Releasing myHRAdmin connection');
  try
    try
      try
        FConn := nil;
      except
        FLogger.StopException;
      end;
      if FMutex <> 0 then
      begin
        Win32Check(ReleaseMutex(FMutex));
        FLogger.LogDebug('Mutex released');
        Win32Check(CloseHandle(FMutex));
        FLogger.LogDebug('Mutex closed');
      end
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
  inherited;
end;

procedure TUniqueExtAppConnection.CreateObject(table: TExtTable; action: TSyncActionInfo);
begin
  FConn.CreateObject(table, action);
end;

function TUniqueExtAppConnection.GetCompanyTable(companyRecordId: integer; table: TExtTable): IExtAppResp;
begin
  Result := FConn.GetCompanyTable(companyRecordId, table);
end;

function TUniqueExtAppConnection.GetTable(table: TExtTable): IExtAppResp;
begin
  Result := FConn.GetTable(table);
end;

procedure TUniqueExtAppConnection.UpdateObject(table: TExtTable; action: TSyncActionInfo);
begin
  FConn.UpdateObject(table, action);
end;

{ TMyIdHttp }

procedure TMyIdHttp.DoRequest(const AMethod: TIdHTTPMethod; AURL: string;
  const ASource, AResponseContent: TStream);
var
  LResponseLocation: Integer;
begin
  if Assigned(AResponseContent) then
  begin
    LResponseLocation := AResponseContent.Position;
  end
  else
    LResponseLocation := 0; // Just to avoid the waringing message

  Request.URL := AURL;
  Request.Method := AMethod;
  Request.Source := ASource;
  Response.ContentStream := AResponseContent;

  try
    repeat
      Inc(FRedirectCount);

      PrepareRequest(Request);
      MyConnectToHost(TMyIdHTTPRequest(Request), Response);

      // Workaround for servers wich respond with 100 Continue on GET and HEAD
      // This workaround is just for temporary use until we have final HTTP 1.1
      // realisation
      repeat
        Response.ResponseText := ReadLn;
        FHTTPProto.RetrieveHeaders;
        ProcessCookies(Request, Response);
      until Response.ResponseCode <> 100;

      case FHTTPProto.ProcessResponse of
        wnAuthRequest: begin
            Dec(FRedirectCount);
            Request.URL := AURL;
          end;
        wnReadAndGo: begin
            ReadResult(Response);
            if Assigned(AResponseContent) then
            begin
              AResponseContent.Position := LResponseLocation;
              AResponseContent.Size := LResponseLocation;
            end;
          end;
        wnGoToURL: begin
            if Assigned(AResponseContent) then
            begin
              AResponseContent.Position := LResponseLocation;
              AResponseContent.Size := LResponseLocation;
            end;
          end;
        wnJustExit: begin
            break;
          end;
        wnDontKnow:
          // TODO: This is for temporary use. Will remove it for final release
          raise EIdException.Create('Undefined situation');
      end;
    until false;
  finally
    if not Response.KeepAlive then begin
      Disconnect;
    end;
  end;
  FRedirectCount := 0;
end;

procedure TMyIdHttp.MyConnectToHost(ARequest: TMyIdHTTPRequest;
  AResponse: TIdHTTPResponse);
var
  LLocalHTTP: TIdHTTPProtocol;
begin
  ARequest.SetUseProxy( SetHostAndPort );

  if ARequest.UseProxy = ctProxy then
  begin
    ARequest.URL := FURI.URI;
  end;

  case ARequest.UseProxy of
    ctNormal:
      if (ProtocolVersion = pv1_0) and (ARequest.Connection = '') then
        ARequest.Connection := 'keep-alive';
//    ctSSL, ctSSLProxy: ARequest.Connection := '';
    ctProxy:
      if (ProtocolVersion = pv1_0) and (ARequest.Connection = '') then
        ARequest.ProxyConnection := 'keep-alive';
  end;

  if ARequest.UseProxy = ctSSLProxy then begin
    LLocalHTTP := TIdHTTPProtocol.Create(Self);

    with LLocalHTTP do begin
      Request.UserAgent := ARequest.UserAgent;
      Request.Host := ARequest.Host;
      Request.ContentLength := ARequest.ContentLength;
      Request.Pragma := 'no-cache';
      Request.URL := URL.Host + ':' + URL.Port;
      Request.Method := hmConnect;
      Request.ProxyConnection := 'keep-alive';
      Response.ContentStream := TMemoryStream.Create;
      try
        try
          repeat
            CheckAndConnect(Response);
            BuildAndSendRequest(nil);

            Response.ResponseText := ReadLn;
            if Length(Response.ResponseText) = 0 then begin
              Response.ResponseText := 'HTTP/1.0 200 OK'; // Support for HTTP responses whithout Status line and headers
              Response.Connection := 'close';
            end
            else begin
              RetrieveHeaders;
              ProcessCookies(LLocalHTTP.Request, LLocalHTTP.Response);
            end;

            if Response.ResponseCode = 200 then
            begin
              // Connection established
              (IOHandler as TIdSSLIOHandlerSocket).PassThrough := false;
              break;
            end
            else begin
              ProcessResponse;
            end;
          until false;
        except
          raise;
          // TODO: Add property that will contain the error messages.
        end;
      finally
        LLocalHTTP.Response.ContentStream.Free;
        LLocalHTTP.Free;
      end;
    end;
  end
  else begin
    CheckAndConnect(AResponse);
  end;

  FHTTPProto.BuildAndSendRequest(URL);

  if (ARequest.Method in [hmPost, hmPut]) then
  begin
    WriteStream(ARequest.Source, True, false);
  end;
end;

{ TMyIdHTTPRequest }

procedure TMyIdHTTPRequest.SetUseProxy(aValue: TIdHTTPConnectionType);
begin
  FUseProxy := aValue;
end;

end.


