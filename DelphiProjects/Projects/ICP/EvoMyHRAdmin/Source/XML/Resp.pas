
{*************************************************************************}
{                                                                         }
{                            XML Data Binding                             }
{                                                                         }
{         Generated on: 1/26/2012 7:41:01                                 }
{       Generated from: E:\job\IS\integration\EvoMyHRAdmin\xml\resp.xml   }
{   Settings stored in: E:\job\IS\integration\EvoMyHRAdmin\xml\resp.xdb   }
{                                                                         }
{*************************************************************************}

unit resp;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRespType = interface;
  IXMLDataType = interface;
  IXMLFieldType = interface;
  IXMLFieldTypeList = interface;
  IXMLCompositeType = interface;

{ IXMLRespType }

  IXMLRespType = interface(IXMLNodeCollection)
    ['{4BB6573D-1116-4127-94AA-BDAD8A33E1B4}']
    { Property Accessors }
    function Get_Status: WideString;
    function Get_Data(Index: Integer): IXMLDataType;
    procedure Set_Status(Value: WideString);
    { Methods & Properties }
    function Add: IXMLDataType;
    function Insert(const Index: Integer): IXMLDataType;
    property Status: WideString read Get_Status write Set_Status;
    property Data[Index: Integer]: IXMLDataType read Get_Data; default;
  end;

{ IXMLDataType }

  IXMLDataType = interface(IXMLNode)
    ['{2153A637-BBCC-4135-93DE-709FB5397F8B}']
    { Property Accessors }
    function Get_Id: Integer;
    function Get_ObjName: WideString;
    function Get_Field: IXMLFieldTypeList;
    function Get_Composite: IXMLCompositeType;
    procedure Set_Id(Value: Integer);
    procedure Set_ObjName(Value: WideString);
    { Methods & Properties }
    property Id: Integer read Get_Id write Set_Id;
    property ObjName: WideString read Get_ObjName write Set_ObjName;
    property Field: IXMLFieldTypeList read Get_Field;
    property Composite: IXMLCompositeType read Get_Composite;
  end;

{ IXMLFieldType }

  IXMLFieldType = interface(IXMLNode)
    ['{F10B8CD4-6062-4BB5-A1B3-253CECE27E51}']
    { Property Accessors }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
    { Methods & Properties }
    property Name: WideString read Get_Name write Set_Name;
  end;

{ IXMLFieldTypeList }

  IXMLFieldTypeList = interface(IXMLNodeCollection)
    ['{D0B1C4B5-C5C7-408A-9A40-B803B62399A9}']
    { Methods & Properties }
    function Add: IXMLFieldType;
    function Insert(const Index: Integer): IXMLFieldType;
    function Get_Item(Index: Integer): IXMLFieldType;
    property Items[Index: Integer]: IXMLFieldType read Get_Item; default;
  end;

{ IXMLCompositeType }

  IXMLCompositeType = interface(IXMLNodeCollection)
    ['{4EDCADE4-CE96-46C8-AD95-7AC986801D0F}']
    { Property Accessors }
    function Get_Data(Index: Integer): IXMLDataType;
    { Methods & Properties }
    function Add: IXMLDataType;
    function Insert(const Index: Integer): IXMLDataType;
    property Data[Index: Integer]: IXMLDataType read Get_Data; default;
  end;

{ Forward Decls }

  TXMLRespType = class;
  TXMLDataType = class;
  TXMLFieldType = class;
  TXMLFieldTypeList = class;
  TXMLCompositeType = class;

{ TXMLRespType }

  TXMLRespType = class(TXMLNodeCollection, IXMLRespType)
  protected
    { IXMLRespType }
    function Get_Status: WideString;
    function Get_Data(Index: Integer): IXMLDataType;
    procedure Set_Status(Value: WideString);
    function Add: IXMLDataType;
    function Insert(const Index: Integer): IXMLDataType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDataType }

  TXMLDataType = class(TXMLNode, IXMLDataType)
  private
    FField: IXMLFieldTypeList;
  protected
    { IXMLDataType }
    function Get_Id: Integer;
    function Get_ObjName: WideString;
    function Get_Field: IXMLFieldTypeList;
    function Get_Composite: IXMLCompositeType;
    procedure Set_Id(Value: Integer);
    procedure Set_ObjName(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFieldType }

  TXMLFieldType = class(TXMLNode, IXMLFieldType)
  protected
    { IXMLFieldType }
    function Get_Name: WideString;
    procedure Set_Name(Value: WideString);
  end;

{ TXMLFieldTypeList }

  TXMLFieldTypeList = class(TXMLNodeCollection, IXMLFieldTypeList)
  protected
    { IXMLFieldTypeList }
    function Add: IXMLFieldType;
    function Insert(const Index: Integer): IXMLFieldType;
    function Get_Item(Index: Integer): IXMLFieldType;
  end;

{ TXMLCompositeType }

  TXMLCompositeType = class(TXMLNodeCollection, IXMLCompositeType)
  protected
    { IXMLCompositeType }
    function Get_Data(Index: Integer): IXMLDataType;
    function Add: IXMLDataType;
    function Insert(const Index: Integer): IXMLDataType;
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function Getresp(Doc: IXMLDocument): IXMLRespType;
function Loadresp(const FileName: WideString): IXMLRespType;
function Newresp: IXMLRespType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function Getresp(Doc: IXMLDocument): IXMLRespType;
begin
  Result := Doc.GetDocBinding('resp', TXMLRespType, TargetNamespace) as IXMLRespType;
end;

function Loadresp(const FileName: WideString): IXMLRespType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('resp', TXMLRespType, TargetNamespace) as IXMLRespType;
end;

function Newresp: IXMLRespType;
begin
  Result := NewXMLDocument.GetDocBinding('resp', TXMLRespType, TargetNamespace) as IXMLRespType;
end;

{ TXMLRespType }

procedure TXMLRespType.AfterConstruction;
begin
  RegisterChildNode('data', TXMLDataType);
  ItemTag := 'data';
  ItemInterface := IXMLDataType;
  inherited;
end;

function TXMLRespType.Get_Status: WideString;
begin
  Result := AttributeNodes['status'].Text;
end;

procedure TXMLRespType.Set_Status(Value: WideString);
begin
  SetAttribute('status', Value);
end;

function TXMLRespType.Get_Data(Index: Integer): IXMLDataType;
begin
  Result := List[Index] as IXMLDataType;
end;

function TXMLRespType.Add: IXMLDataType;
begin
  Result := AddItem(-1) as IXMLDataType;
end;

function TXMLRespType.Insert(const Index: Integer): IXMLDataType;
begin
  Result := AddItem(Index) as IXMLDataType;
end;

{ TXMLDataType }

procedure TXMLDataType.AfterConstruction;
begin
  RegisterChildNode('field', TXMLFieldType);
  RegisterChildNode('composite', TXMLCompositeType);
  FField := CreateCollection(TXMLFieldTypeList, IXMLFieldType, 'field') as IXMLFieldTypeList;
  inherited;
end;

function TXMLDataType.Get_Id: Integer;
begin
  Result := AttributeNodes['id'].NodeValue;
end;

procedure TXMLDataType.Set_Id(Value: Integer);
begin
  SetAttribute('id', Value);
end;

function TXMLDataType.Get_ObjName: WideString;
begin
  Result := AttributeNodes['objName'].Text;
end;

procedure TXMLDataType.Set_ObjName(Value: WideString);
begin
  SetAttribute('objName', Value);
end;

function TXMLDataType.Get_Field: IXMLFieldTypeList;
begin
  Result := FField;
end;

function TXMLDataType.Get_Composite: IXMLCompositeType;
begin
  Result := ChildNodes['composite'] as IXMLCompositeType;
end;

{ TXMLFieldType }

function TXMLFieldType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

procedure TXMLFieldType.Set_Name(Value: WideString);
begin
  SetAttribute('name', Value);
end;

{ TXMLFieldTypeList }

function TXMLFieldTypeList.Add: IXMLFieldType;
begin
  Result := AddItem(-1) as IXMLFieldType;
end;

function TXMLFieldTypeList.Insert(const Index: Integer): IXMLFieldType;
begin
  Result := AddItem(Index) as IXMLFieldType;
end;
function TXMLFieldTypeList.Get_Item(Index: Integer): IXMLFieldType;
begin
  Result := List[Index] as IXMLFieldType;
end;

{ TXMLCompositeType }

procedure TXMLCompositeType.AfterConstruction;
begin
  RegisterChildNode('data', TXMLDataType);
  ItemTag := 'data';
  ItemInterface := IXMLDataType;
  inherited;
end;

function TXMLCompositeType.Get_Data(Index: Integer): IXMLDataType;
begin
  Result := List[Index] as IXMLDataType;
end;

function TXMLCompositeType.Add: IXMLDataType;
begin
  Result := AddItem(-1) as IXMLDataType;
end;

function TXMLCompositeType.Insert(const Index: Integer): IXMLDataType;
begin
  Result := AddItem(Index) as IXMLDataType;
end;

end. 