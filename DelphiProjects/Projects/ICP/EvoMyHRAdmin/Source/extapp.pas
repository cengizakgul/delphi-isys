unit extapp;

interface

uses
  ExtAppConnection, extappdecl, evconsts, xmlintf, evodata,
  gdycommonlogger, PlannedActions,
  evoapiconnectionutils, gdyClasses, common, kbmMemTable, EvoAPIConnection;

type
  TAnalyzeResult = record
    Errors: integer;
    Actions: ISyncActionsRO;
  end;

  TGroupedAnalyzeResult = record
    Errors: integer;
    Groups: TSyncActionGroups;
    ObjName: string;
  end;

  TEvoXContent = record
    Errors: integer;
    UpdateFileContent: string;
    CreateFileContent: string;
  end;

  TDBDTStrings = array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of string;
  TSyncDictionary = (dictPosition, dictWC, dictJob, dictED, dictPayrollStatus, dictCalcType, dictPayGroup, dictStates, dictPayFrequencies, dictSMS);

type
  TExtAppUpdater = class
  private
    function GetSMSRecordIdByNullableCodes(state, sms: Variant): string;
  protected
    FLogger: ICommonLogger;
    FExtConn: IExtAppConnection;
    FCompany: TEvoCompanyDef;
    FCompanyRecordId: integer;
    FExtAppDBDTs: array [CLIENT_LEVEL_DIVISION..CLIENT_LEVEL_TEAM] of IExtAppResp;
    FDictionaries: array [TSyncDictionary] of IExtAppResp;
    FProgressIndicator: IProgressIndicator;
    FEvoXImportExecutor: IEvoXImportExecutor;

    procedure FetchExtAppDBDTs;
    procedure FetchDictionaries;
    function RecordHeader(recordId: Variant; extTable: TExtTable): string;
    function GetDictRecordIdByNullableCode(dict: TSyncDictionary; code: Variant): string;
    function GetDBDTRecordIDsByCodes(codes: TDBDTStrings): TDBDTStrings;
    function ApplyActions(extTable: TExtTable; actions: TSyncActionInfos): TSyncStat; overload;
    function ApplyActions(extTable: TExtTable; actions: ISyncActionsRO): TSyncStat; overload;
    function BuildExtAppRecord(extTable: TExtTable; recordId, code, name, parentId: Variant): string;
    function FetchExtAppTable(table: TExtTable): IExtAppResp;

    function GroupedImportActionsToEvoXContent(groups: TSyncActionGroups): TEvoXContent;
    function Apply_ImportOp(const content: TEvoXContent; objName, mapFileAlias, header: string): TSyncStat;
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; connParam: TExtAppConnectionParam; pi: IProgressIndicator; EvoX: IEvoXImportExecutor);
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; virtual;
    function Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat; virtual;
    class function Opname: string; virtual;
  end;

  TExtAppUpdaterClass = class of TExtAppUpdater;

  ///

  TExtAppEmployeeUpdater = class (TExtAppUpdater)
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; EEData: TEvoEEData; connParam: TExtAppConnectionParam; pi: IProgressIndicator; evox: IEvoXImportExecutor );
  protected
    FEEData: TEvoEEData;
    function CompareFields(const extRec: IExtAppRecord; EEData: TEvoEEData; exporting: boolean): TChangedFieldRecs;
    function EvoPayFrequencyToExtApp(v: Variant): Variant;
    function ExtAppPayFrequencyToEvo(v: Variant): Variant;
  end;

  TExtAppEmployeeExporter = class (TExtAppEmployeeUpdater)
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; EEData: TEvoEEData; connParam: TExtAppConnectionParam; pi: IProgressIndicator);
    function Analyze_EEExport: TAnalyzeResult;
    function Apply_EEExport(actions: ISyncActionsRO): TUpdateEmployeesStat;
  private
    function BuildExtAppEERecord(EEData: TEvoEEData; oldExtRec: IExtAppRecord): string;
  end;

  TExtAppEmployeeImporter = class (TExtAppEmployeeUpdater)
  public
    function Analyze_EEImport: TAnalyzeResult;
    function Apply_EEImport(actions: ISyncActionsRO): TUpdateEmployeesStat;
  private
    function BuildEEEvoXImportHeader: string;
    function BuildEEEvoXImportRecord(const extEE: IExtAppRecord; isNew: boolean): string;
    function ImportActionsToEvoXContent(actions: ISyncActionsRO): TEvoXContent;
  end;

  TExtAppEmployeeGUIDExporter = class (TExtAppEmployeeUpdater)
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; EEData: TEvoEEData; connParam: TExtAppConnectionParam; pi: IProgressIndicator);
    function Analyze_EEExport: TAnalyzeResult;
    function Apply_EEExport(actions: ISyncActionsRO): TUpdateEmployeesStat;
  private
    function SetGuidField(const extRec: IExtAppRecord; EEData: TEvoEEData; exporting: boolean): TChangedFieldRecs;
    function BuildExtAppEERecord(EEData: TEvoEEData; oldExtRec: IExtAppRecord): string;
  end;

  ///

  TExtAppDBDTUpdater = class (TExtAppUpdater)
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; EvoConn: IEvoAPIConnection; connParam: TExtAppConnectionParam; pi: IProgressIndicator);
    destructor Destroy; override;
    function ExportDBDTs: TSyncStat;
  private
    FEvoDBDTs: TEvoDBDTs;
    function ExportDBDTLevel(level: char; parentID: integer): TSyncStat; virtual;
  end;

  TExtAppDBDTGuidUpdater = class (TExtAppDBDTUpdater)
  private
    function ExportDBDTLevel(level: char; parentID: integer): TSyncStat; override;
    function BuildExtAppDBDTGuidRecord(extTable: TExtTable; recordId, fGuid: Variant): string;
  end;

  TExtAppDictUpdater = class (TExtAppUpdater)
  private
    FEvoConn: IEvoAPIConnection;
    function CompareSMSFields(extSMS: IExtAppRecord; evoSMS: TkbmCustomMemTable): TChangedFieldRecs;
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; EvoConn: IEvoAPIConnection; connParam: TExtAppConnectionParam; pi: IProgressIndicator);

    function Analyze_DictExport(dict: TSyncDictionary): TAnalyzeResult;
    function Apply_DictExport(dict: TSyncDictionary; actions: ISyncActionsRO): TSyncStat;
    function ExportDict(dict: TSyncDictionary): TSyncStat;

    function Analyze_StateMaritalStatusExport: TGroupedAnalyzeResult;
    function Apply_StateMaritalStatusExport(groups: TSyncActionGroups): TSyncStat;
    function ExportStateMaritalStatus: TSyncStat;
  end;

  TExtAppDictGuidUpdater = class (TExtAppUpdater)
  private
    FEvoConn: IEvoAPIConnection;
    procedure FetchGuidDictionaries;
  public
    constructor Create(logger: ICommonLogger; company: TEvoCompanyDef; EvoConn: IEvoAPIConnection; connParam: TExtAppConnectionParam; pi: IProgressIndicator);

    function Analyze_DictExport(dict: TSyncDictionary): TAnalyzeResult;
    function Apply_DictExport(dict: TSyncDictionary; actions: ISyncActionsRO): TSyncStat;
    function ExportDict(dict: TSyncDictionary): TSyncStat;

    function Analyze_StateMaritalStatusExport: TGroupedAnalyzeResult;
    function Apply_StateMaritalStatusExport(groups: TSyncActionGroups): TSyncStat;
    function ExportStateMaritalStatus: TSyncStat;
  end;

  ///

  TExtAppRatesUpdater = class (TExtAppUpdater)
  private
    function CompareFields(extRate: IExtAppRecord; evoRates: TkbmCustomMemTable) : TChangedFieldRecs;
  end;

  TExtAppRatesExporter = class (TExtAppRatesUpdater)
  public
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; override;
    function Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat; override;
    class function Opname: string; override;
  end;

  TExtAppRatesGuidExporter = class (TExtAppRatesExporter)
  public
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; override;
    class function Opname: string; override;
  end;

  TExtAppRatesImporter = class (TExtAppRatesUpdater)
  public
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; override;
    function Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat; override;
    class function Opname: string; override;
  end;

  ///

  TExtAppScheduledEDsUpdater = class (TExtAppUpdater)
  private
    function CompareFields(extSchedED: IExtAppRecord; evoSchedEDs: TkbmCustomMemTable): TChangedFieldRecs;
  end;

  TExtAppScheduledEDsExporter = class (TExtAppScheduledEDsUpdater)
  public
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; override;
    function Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat; override;
    class function Opname: string; override;
  end;

  TExtAppScheduledEDsGuidExporter = class (TExtAppScheduledEDsExporter)
  public
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; override;
    class function Opname: string; override;
  end;

  TExtAppScheduledEDsImporter = class (TExtAppScheduledEDsUpdater)
  public
    function Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult; override;
    function Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat; override;
    class function Opname: string; override;
  end;

  procedure DoDictExport(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; extAppConnParam: TExtAppConnectionParam; pi: IProgressIndicator; showGUI: boolean);
  procedure DoGroupedOp(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; extAppConnParam: TExtAppConnectionParam; pi: IProgressIndicator; showGUI: boolean; evox: IEvoXImportExecutor; updaterClass: TExtAppUpdaterClass);

  procedure DoDictGuidExport(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; extAppConnParam: TExtAppConnectionParam; pi: IProgressIndicator; showGUI: boolean);

implementation

uses
  sysutils, variants, gdycommon,
  XmlRpcTypes, EvoWaitForm, gdyRedir, gdyGlobalWaitIndicator, userActionHelpers,
  XmlRpcCommon, PlannedActionConfirmationForm, forms, dateutils, DB;

const
  ExtAppDBDTLevels: array [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM] of TExtTable = (etCompany, etDivision, etLocation, etDepartment, etTeam);

procedure AddVal(var res: string; tag: string; val: Variant);
begin
  res := res + Format('<Field name="%s">%s</Field>',[tag, EncodeEntities(trim(VarToStr(val))), tag]);
end;

type
  TDictDesc = record
    EvoUserFriendlyName: string;
    EvoQueryFilename: string;
    EvoTableLevel: TTableLevel;
    ExtTable: TExtTable;

    EvoKeyField: string;  //used for user friendly messages too so it needs to be lowercase
    ExtGuidField: string; //used for patch guid values in ExtApp
    ExtGuidTable: TExtTable;
  end;

const
  DictDescs: array [TSyncDictionary] of TDictDesc = (
  (
    EvoUserFriendlyName: 'position';
    EvoQueryFilename: 'CUSTOM_CO_HR_POSITIONS.rwq';
    EvoTableLevel: tlCompany;
    ExtTable: etJobTitle;
    EvoKeyField: 'description';
    ExtGuidField: 'job_title_guid';
    ExtGuidTable: etPositionGuid;
  ),
  (
    EvoUserFriendlyName: 'worker compensation code';
    EvoQueryFilename: 'CUSTOM_CO_WORKERS_COMP.rwq';
    EvoTableLevel: tlCompany;
    ExtTable: etWorkersComp;
    EvoKeyField: 'code';
    ExtGuidField: 'workers_comp_guid';
    ExtGuidTable: etWCGuid;
  ),
  (
    EvoUserFriendlyName: 'job';
    EvoQueryFilename: 'CUSTOM_CO_JOBS.rwq';
    EvoTableLevel: tlCompany;
    ExtTable: etJob;
    EvoKeyField: 'code';
    ExtGuidField: 'job_level_5_guid';
    ExtGuidTable: etJobGuid;
  ),
  (
    EvoUserFriendlyName: 'E/D code';
    EvoQueryFilename: 'CUSTOM_CO_E_D_CODES.rwq';
    EvoTableLevel: tlCompany;
    ExtTable: etCompanyDeduction;
    EvoKeyField: 'code';
    ExtGuidField: 'deduction_guid';
    ExtGuidTable: etEDGuid;
  ),
  (
    EvoUserFriendlyName: 'payroll status';
    EvoQueryFilename: 'EE_TerminationCode_ComboChoices.rwq';
    EvoTableLevel: tlSystem;
    ExtTable: etPayrollStatus;
    EvoKeyField: 'value';
  ),
  (
    EvoUserFriendlyName: 'calculation type';
    EvoQueryFilename: 'EE_SCHEDULED_E_DS_CalculationType_ComboChoices.rwq';
    EvoTableLevel: tlSystem;
    ExtTable: etCalcType;
    EvoKeyField: 'value';
  ),
  (
    EvoUserFriendlyName: 'pay group';
    EvoQueryFilename: 'CUSTOM_CO_PAY_GROUP.rwq';
    EvoTableLevel: tlCompany;
    ExtTable: etPayGroup;
    EvoKeyField: 'description';
    ExtGuidField: 'pay_group_guid';
    ExtGuidTable: etPaygroupGuid;
  ),
  (
    EvoUserFriendlyName: 'state';
    EvoQueryFilename: 'SY_STATES.rwq';
    EvoTableLevel: tlSystem;
    ExtTable: etTaxState;
    EvoKeyField: 'state';
  ),

  (
    EvoUserFriendlyName: 'pay frequency';
    EvoTableLevel: tlSystem;
    ExtTable: etPayFrequency;
  ),
  (
    EvoUserFriendlyName: 'state marital status';
    EvoTableLevel: tlSystem;
    ExtTable: etStateMaritalStatus;
    ExtGuidField: 'guid';
    ExtGuidTable: etSMSGuid;
  )
  );

function EmptyToDash(v: Variant): string;
begin
  if VarIsNull(v) then
    Result := ''
  else
  begin
    Result := trim(VarToStr(v));
    if Result = '' then
      Result := '-';
  end;    
end;

function DateVarToStr(v: Variant): string;
begin
  if VarHasValue(v) then
    Result := FormatDateTime('mm/dd/yyyy', VarToDateTime(v))
  else
    Result := '';
end;

function MoneyStrToFloatStr(str: string): string;
begin
  Result := trim(str);
  if (Result <> '') and (Result[1] = '$') then
    Result := trim( StringReplace(copy(Result, 2, 9999),',','',[rfReplaceAll]) );
end;

function MoneyStrToFloat(str: string): Double;
begin
  str := trim(str);
  if trim(str) = '' then
    Result := 0
  else
    Result := StrToFloat(MoneyStrToFloatStr(str));
end;

function FormattedNumberToFloatVar(v: Variant): Variant;
var
  s, r: string;
  i: integer;
begin
  s := trim(VarToStr(v));
  if s = '' then
  begin
    Result := Null;
    exit;
  end;

  r := '';
  for i := 1 to Length(s) do
    if s[i] in ['0'..'9', '.', '-'] then
      r := r + s[i];
  Result := StrToFloat(r);
end;

function YNVarToBooleanStr(v: Variant): string;
var
  str: string;
begin
  str := trim(VarToStr(v));
  if str = GROUP_BOX_YES then
    Result := 'true'
  else if str = GROUP_BOX_NO then
    Result := 'false'
  else if str = '' then
    Result := ''
  else
    raise Exception.CreateFmt('Unexpected code: %s',[str]);
end;

function BooleanStrToYN(str: string): string;
begin
  str := trim(str);
  if str = '' then
    Result := ''
  else
    Result := IIF(StrToBool(str), GROUP_BOX_YES, GROUP_BOX_NO);
end;

function PhoneToExtAppPhone(s: Variant): string;
var
  i: integer;
  phone: string;
begin
  Result := '';
  phone := VarToStr(s);
  for i := 1 to Length(phone) do
    if phone[i] in ['0'..'9'] then
      Result := Result + phone[i];
end;

constructor TExtAppEmployeeUpdater.Create(logger: ICommonLogger; company: TEvoCompanyDef; EEData: TEvoEEData; connParam: TExtAppConnectionParam; pi: IProgressIndicator; evox: IEvoXImportExecutor);
begin
  inherited Create(logger, company, connParam, pi, evox);
  FEEData := EEData;
end;

function CurrentTermCodeToExtAppStatus(status: string): string;
begin
  if status = EE_TERM_ACTIVE then
    Result := 'Active'
  else
    Result := 'Terminated';
end;

function ExtAppGenderToEvo(v: Variant): Variant;
begin
  Result := VarToStr(v);
  if Result = 'NotSpecifi' then
    Result := GROUP_BOX_UNKOWN;
end;

function EvoW2or1099ToExtApp(v: Variant): Variant;
var
  s: string;
begin
  s := VarToStr(v);
  if s = '' then
    Result := Null
  else
  begin
    case s[1] of
      GROUP_BOX_COMPANY: Result := '1099R';
      GROUP_BOX_INDIVIDUAL: Result := 'W2';
    else
      raise Exception.Create('Unexpected Evolution W2/1099 code: '+s);
    end;
  end;
end;

function ExtW2or1099ToEvo(v: Variant): Variant;
var
  s: string;
begin
  s := VarToStr(v);
  if s = '' then
    Result := Null
  else
  begin
    if (s = '1099R') or (s = '1099M') then
      Result := GROUP_BOX_COMPANY
    else if s = 'W2' then
      Result := GROUP_BOX_INDIVIDUAL
    else
      raise Exception.CreateFmt('Unexpected %s W2/1099 code: %s',[ExtAppName, s]);
  end;
end;

function TExtAppEmployeeUpdater.EvoPayFrequencyToExtApp(v: Variant): Variant;
var
  pf: string;
begin
  pf := VarToStr(v);
  if pf = '' then
    Result := Null
  else
  begin
    case pf[1] of
      FREQUENCY_TYPE_DAILY: Result := Null;
      FREQUENCY_TYPE_WEEKLY: Result := 'W';
      FREQUENCY_TYPE_BIWEEKLY: Result := 'B';
      FREQUENCY_TYPE_SEMI_MONTHLY: Result := 'S';
      FREQUENCY_TYPE_MONTHLY: Result := 'M';
      FREQUENCY_TYPE_QUARTERLY: Result := Null;
    else
      raise Exception.Create('Unexpected Evolution pay frequency code: '+pf);
    end;
    if Result = '' then
      FLogger.LogWarningFmt('Cannot map Evolution pay frequency code <%s> to a %s pay frequency', [pf, ExtAppName]);
  end;
end;

function TExtAppEmployeeUpdater.ExtAppPayFrequencyToEvo(v: Variant): Variant;
var
  pf: string;
begin
  pf := VarToStr(v);
  if pf = '' then
    Result := Null
  else
  begin
    if pf = 'W' then
      Result := FREQUENCY_TYPE_WEEKLY
    else if pf = 'B' then
      Result := FREQUENCY_TYPE_BIWEEKLY
    else if pf = 'S' then
      Result := FREQUENCY_TYPE_SEMI_MONTHLY
    else if pf = 'M' then
      Result := FREQUENCY_TYPE_MONTHLY
{    else if pf = '' then
      Result := FREQUENCY_TYPE_DAILY
    else if pf = '' then
      Result := FREQUENCY_TYPE_QUARTERLY
      }
    else
      raise Exception.CreateFmt('Unexpected %s pay frequency code: %s', [ExtAppName, pf]);
  end;
end;

function TExtAppUpdater.GetSMSRecordIdByNullableCodes(state: Variant; sms: Variant): string;
var
  rec: IExtappRecord;
begin
  if not VarHasValue(state) or not VarHasValue(sms) then
  begin
    Result := '';
    exit;
  end;
  rec := FDictionaries[dictSMS].FindRecordByCodeAndParentCode(VarToStr(sms), VarToStr(state));
  if rec <> nil then
    Result := IntToStr(rec.Id)
  else
    raise Exception.CreateFmt('Cannot find a %s %s with code <%s>', [ExtAppName, ExtTableDesc(etStateMaritalStatus).SingularName, trim(VarToStr(sms))] );
end;

function TExtAppEmployeeExporter.BuildExtAppEERecord(EEData: TEvoEEData; oldExtRec: IExtAppRecord): string;
var
  dbdts: TDBDTStrings;
  dbdtRecordIDs: TDBDTStrings;
  level: char;
  ssn: string;
  username: string;
  password: string;

  procedure AddField(tag: string; fn: string);
  begin
    AddVal(Result, tag, EEData.EEs[fn]);
  end;
begin
  if oldExtRec <> nil then
    Result := RecordHeader(oldExtRec.Id, etEmployee)
  else
    Result := RecordHeader(Null, etEmployee);

  AddField('empid', 'CUSTOM_EMPLOYEE_NUMBER');
  AddField('firstName', 'FIRST_NAME');
  AddField('lastName', 'LAST_NAME');
  AddField('ssn', 'SOCIAL_SECURITY_NUMBER');

  AddVal(Result, 'date_of_birth', DateVarToStr(FEEData.EEs['BIRTH_DATE']) );
  AddVal(Result, 'hire_date', DateVarToStr(FEEData.EEs['CURRENT_HIRE_DATE']) );
  AddField('streetAddr1', 'ADDRESS1');
  AddField('streetAddr2', 'ADDRESS2');
  AddField('city', 'CITY');
  AddField('zip', 'ZIP_CODE');
  AddField('state', 'STATE');
  AddField('email', 'E_MAIL_ADDRESS');
  AddField('ethnicity', 'ETHNICITY');
  AddVal(Result, 'status', CurrentTermCodeToExtAppStatus(FEEData.EEs['Current_Termination_Code']) );
  AddVal(Result, 'payroll_status', GetDictRecordIdByNullableCode(dictPayrollStatus, FEEData.EEs['Current_Termination_Code']));
  AddVal(Result, 'term_date', DateVarToStr(FEEData.EEs['Current_Termination_Date']) );
  AddField('gender', 'Gender');
  if (oldExtRec = nil) or (Length(oldExtRec.AsString['middleName']) <= 1) then
    AddField('middleName', 'Middle_initial');
  AddVal(Result, 'pay_frequency', GetDictRecordIdByNullableCode(dictPayFrequencies, EvoPayFrequencyToExtApp(FEEData.EEs['Pay_Frequency'])) );
  AddField('federal_marital_stat', 'Federal_Marital_Status');
  AddField('federal_dependents', 'Number_Of_Dependents');
  AddVal(Result, 'tax_state', GetDictRecordIdByNullableCode(dictStates, FEEData.EEs['home_state']) );
  AddVal(Result, 'state_marital_status', GetSMSRecordIdByNullableCodes(FEEData.EEs['home_state'], FEEData.EEs['home_state_marital_status']) );
  AddField('state_dependents', 'home_state_dependents');
  AddVal(Result, 'veteran', FEEData.EEs['Veteran'] );
  AddVal(Result, 'vietnam_veteran', FEEData.EEs['Vietnam_veteran']);

  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
    dbdts[level] := EmptyToDash(FEEData.EEs[CDBDTMetadata[level].CodeField]);
  dbdtRecordIDs := GetDBDTRecordIDsByCodes(dbdts);
  AddVal(Result, 'division_level_1', dbdtRecordIDs[CLIENT_LEVEL_DIVISION]);
  AddVal(Result, 'location_level_2', dbdtRecordIDs[CLIENT_LEVEL_BRANCH]);
  AddVal(Result, 'department_level_3', dbdtRecordIDs[CLIENT_LEVEL_DEPT]);
  AddVal(Result, 'team_level_4', dbdtRecordIDs[CLIENT_LEVEL_TEAM]);

  AddVal(Result, 'phone', PhoneToExtAppPhone(FEEData.EEs['Phone1']) );
  AddVal(Result, 'mobilePhone', PhoneToExtAppPhone(FEEData.EEs['Phone2']) );
  AddVal(Result, 'original_hire_date', DateVarToStr(FEEData.EEs['Original_Hire_Date']) );
  AddField('rehire_eligible', 'Eligible_For_Rehire');
  AddField('salary_amount', 'salary_amount');
  AddVal(Result, 'sui_state', GetDictRecordIdByNullableCode(dictStates, FEEData.EEs['sui_state']));
  AddField('new_hire_report_sent', 'new_hire_report_sent');
  AddField('flsa_exempt', 'flsa_exempt');
  AddVal(Result, 'w_2_or_1099_employee', EvoW2or1099ToExtApp(FEEData.EEs['Comp_Or_Indiv_Name']));
  AddVal(Result, 'i_9_on_file', YNVarToBooleanStr(FEEData.EEs['I9_ON_FILE']));
  AddField('military_reserve', 'military_reserve');
  AddVal(Result, 'smoker', YNVarToBooleanStr(FEEData.EEs['smoker']));
  AddVal(Result, 'job_title', GetDictRecordIdByNullableCode(dictPosition, FEEData.EEs['POSITION']) );
  AddVal(Result, 'workers_comp', GetDictRecordIdByNullableCode(dictWC, FEEData.EEs['WORKERS_COMP_CODE']));
  AddVal(Result, 'job_level_5', GetDictRecordIdByNullableCode(dictJob, FEEData.EEs['JOB']));
  AddField('federal_override', 'Override_Fed_Tax_Type');
  AddField('additional_federal', 'Override_Fed_Tax_Value');
  AddField('state_override', 'Override_State_Tax_Type');
  AddField('additional_state', 'Override_State_Tax_Value');
  AddField('state_tax_exempt', 'State_Exempt_Exclude');
  AddField('standard_hours', 'STANDARD_HOURS');
  AddField('vmr_password', 'WEB_PASSWORD');
  AddVal(Result, 'pay_group', GetDictRecordIdByNullableCode(dictPayGroup, FEEData.EEs['PAY_GROUP']) );

  if oldExtRec = nil then
  begin
    ssn := StringReplace(FEEData.EEs['SOCIAL_SECURITY_NUMBER'], '-', '', [rfReplaceAll]);
    username := Copy(FEEData.EEs['FIRST_NAME'], 1, 1) + FEEData.EEs['LAST_NAME'] + Copy(ssn, Length(ssn)-3, 4);
    username := StringReplace(username, ' ', '', [rfReplaceAll]);
    password := Copy(FEEData.EEs['FIRST_NAME'], 1, 1) + FEEData.EEs['LAST_NAME'] + Copy(FCompany.CUSTOM_COMPANY_NUMBER, Length(FCompany.CUSTOM_COMPANY_NUMBER)-3, 4);
    password := StringReplace(password, ' ', '', [rfReplaceAll]);
    AddVal(Result, 'loginName',  username);
    AddVal(Result, 'isActive', IIF(FEEData.EEs['Current_Termination_Code'] = EE_TERM_ACTIVE, 'true', 'false') );
    AddVal(Result, 'password', password );
  end;

  Result := Result + '</data>';
  if oldExtRec = nil then
    Flogger.LogDebug('New employee', Result)
  else
    Flogger.LogDebug('New version of employee', Result);
end;

function TExtAppEmployeeUpdater.CompareFields(const extRec: IExtAppRecord; EEData: TEvoEEData; exporting: boolean): TChangedFieldRecs;
begin
  Assert( trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(extRec.AsString['empid']) );
  SetLength(Result, 0);

  CheckIfChanged(Result, extRec.AsString['firstName'], EEData.EEs['FIRST_NAME'], 'First name');
  CheckIfChanged(Result, extRec.AsString['lastName'], EEData.EEs['LAST_NAME'], 'Last name');
  CheckIfSSNChanged(Result, extRec.AsString['ssn'], EEData.EEs['SOCIAL_SECURITY_NUMBER'], 'SSN');

  CheckIfChanged(Result, extRec.AsDateVar['date_of_birth'], FEEData.EEs['BIRTH_DATE'], 'Birth date');
  CheckIfChanged(Result, extRec.AsDateVar['hire_date'], FEEData.EEs['CURRENT_HIRE_DATE'], 'Hire date');
  CheckIfChanged(Result, extRec.AsString['streetAddr1'], FEEData.EEs['ADDRESS1'], 'Address 1');
  CheckIfChanged(Result, extRec.AsString['streetAddr2'], FEEData.EEs['ADDRESS2'], 'Address 2');
  CheckIfChanged(Result, extRec.AsString['city'], FEEData.EEs['CITY'], 'City');
  CheckIfChanged(Result, extRec.AsString['zip'], FEEData.EEs['ZIP_CODE'], 'Zip code');
  CheckIfChanged(Result, extRec.AsString['state'], FEEData.EEs['STATE'], 'State');
  CheckIfChanged(Result, extRec.AsString['email'], FEEData.EEs['E_MAIL_ADDRESS'], 'E-mail');
  CheckIfChanged(Result, extRec.AsString['ethnicity'], FEEData.EEs['ETHNICITY'], 'Ethnicity');
  CheckIfChanged(Result, extRec.AsString['status'], CurrentTermCodeToExtAppStatus(FEEData.EEs['Current_Termination_Code']), 'MyHRAdmin status');
  CheckIfChanged(Result, extRec.AsCompositeCode['payroll_status'], FEEData.EEs['Current_Termination_Code'], 'Evolution Status');
  CheckIfChanged(Result, extRec.AsDateVar['term_date'], FEEData.EEs['Current_Termination_Date'], 'Termination date');
  CheckIfChanged(Result, extRec.AsString['gender'], FEEData.EEs['GENDER'], 'Gender');
  CheckIfChanged(Result, Copy(extRec.AsString['middleName'],1,1), FEEData.EEs['Middle_Initial'], 'Middle initial');
  CheckIfChanged(Result, extRec.AsCompositeCode['pay_frequency'], EvoPayFrequencyToExtApp(FEEData.EEs['Pay_Frequency']), 'Pay frequency');
  CheckIfChanged(Result, extRec.AsString['federal_marital_stat'], FEEData.EEs['Federal_Marital_Status'], 'Federal marital status');
  CheckIfChanged(Result, extRec.AsString['federal_dependents'], FEEData.EEs['Number_Of_Dependents'], 'Federal dependents');
  CheckIfChanged(Result, extRec.AsCompositeCode['tax_state'], FEEData.EEs['home_state'], 'Home state');
  CheckIfChanged(Result, extRec.AsCompositeCode['state_marital_status'], trim(VarToStr(FEEData.EEs['home_state_marital_status'])), 'State marital status');
  CheckIfChanged(Result, extRec.AsString['state_dependents'], FEEData.EEs['home_state_dependents'], 'State dependents');
  CheckIfChanged(Result, extRec.AsString['veteran'], FEEData.EEs['Veteran'], 'Veteran');
  CheckIfChanged(Result, extRec.AsString['vietnam_veteran'], FEEData.EEs['Vietnam_veteran'], 'Vietnam veteran');
  CheckIfChanged(Result, extRec.AsCompositeCode['division_level_1'], EmptyToDash(FEEData.EEs['CUSTOM_DIVISION_NUMBER']), 'Division');
  CheckIfChanged(Result, extRec.AsCompositeCode['location_level_2'], EmptyToDash(FEEData.EEs['CUSTOM_BRANCH_NUMBER']), 'Branch');
  CheckIfChanged(Result, extRec.AsCompositeCode['department_level_3'], EmptyToDash(FEEData.EEs['CUSTOM_DEPARTMENT_NUMBER']), 'Department');
  CheckIfChanged(Result, extRec.AsCompositeCode['team_level_4'], EmptyToDash(FEEData.EEs['CUSTOM_TEAM_NUMBER']), 'Team');
  CheckIfChanged(Result, PhoneToExtAppPhone(extRec.AsString['phone']), PhoneToExtAppPhone(FEEData.EEs['Phone1']), 'Primary phone');
  CheckIfChanged(Result, PhoneToExtAppPhone(extRec.AsString['mobilePhone']), PhoneToExtAppPhone(FEEData.EEs['Phone2']), 'Secondary phone');
  CheckIfChanged(Result, extRec.AsDateVar['original_hire_date'], FEEData.EEs['Original_Hire_Date'], 'Original hire date');
  CheckIfChanged(Result, extRec.AsString['rehire_eligible'], FEEData.EEs['Eligible_For_Rehire'], 'Eligible for rehire');
  CheckIfFloatChanged(Result, MoneyStrToFloat(extRec.AsString['salary_amount']), FEEData.EEs.FieldByName('SALARY_AMOUNT').AsFloat, 'Salary');
  CheckIfChanged(Result, extRec.AsCompositeCode['sui_state'], FEEData.EEs['sui_state'], 'SUI state');
  if exporting then
    CheckIfChanged(Result, extRec.AsString['new_hire_report_sent'], FEEData.EEs['new_hire_report_sent'], 'New hire report sent');
  CheckIfChanged(Result, extRec.AsString['flsa_exempt'], FEEData.EEs['flsa_exempt'], 'FLSA exempt');
  CheckIfChanged(Result, extRec.AsString['w_2_or_1099_employee'], EvoW2or1099ToExtApp(FEEData.EEs['Comp_Or_Indiv_Name']), '1099 or employee');
  CheckIfChanged(Result, extRec.AsString['i_9_on_file'], YNVarToBooleanStr(FEEData.EEs['I9_ON_FILE']), 'I-9 on file');
  CheckIfChanged(Result, extRec.AsString['military_reserve'], FEEData.EEs['military_reserve'], 'Military reserve');
  CheckIfChanged(Result, extRec.AsString['smoker'], YNVarToBooleanStr(FEEData.EEs['smoker']), 'Smoker');
  CheckIfChanged(Result, extRec.AsCompositeCode['job_title'], FEEData.EEs['POSITION'], 'Position');
  CheckIfChanged(Result, extRec.AsCompositeCode['workers_comp'], FEEData.EEs['WORKERS_COMP_CODE'], 'Workers compensation code');
  CheckIfChanged(Result, extRec.AsCompositeCode['job_level_5'], FEEData.EEs['JOB'], 'Job');

  CheckIfChanged(Result, extRec.AsString['federal_override'], FEEData.EEs['Override_Fed_Tax_Type'], 'Override federal tax type');
  CheckIfFloatChanged(Result, MoneyStrToFloat(extRec.AsString['additional_federal']), FEEData.EEs.FieldByName('Override_Fed_Tax_Value').AsFloat, 'Override federal tax value');
  CheckIfChanged(Result, extRec.AsString['state_override'], FEEData.EEs['Override_State_Tax_Type'], 'Override state tax type');
  CheckIfFloatChanged(Result, MoneyStrToFloat(extRec.AsString['additional_state']), FEEData.EEs.FieldByName('Override_State_Tax_Value').AsFloat, 'Override state tax value');
  if exporting then
    CheckIfChanged(Result, extRec.AsString['state_tax_exempt'], FEEData.EEs['State_Exempt_Exclude'], 'State tax exempt status');
  CheckIfFloatChanged(Result, FormattedNumberToFloatVar(extRec.AsString['standard_hours']), FEEData.EEs['STANDARD_HOURS'], 'Standard hours', 0.01);
  CheckIfChanged(Result, extRec.AsString['vmr_password'], FEEData.EEs['WEB_PASSWORD'], 'VMR password');
  CheckIfChanged(Result, extRec.AsCompositeCode['pay_group'], FEEData.EEs['PAY_GROUP'], 'Pay group');
end;

function TExtAppEmployeeExporter.Analyze_EEExport: TAnalyzeResult;
var
  SyncActions: ISyncActions;
  extRecs: IExtAppResp;
  extRec: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  eecode: string;
  ctx: IStr;
  opname: string;
begin
  SyncActions := CreateSyncActions(ExtAppName, 'Employee');
  Result.Errors := 0;
  Result.Actions := SyncActions;
  opname := 'Analyzing employee records for export';
  FLogger.LogEntry(opname);
  try
    try
      FetchExtAppDBDTs;
      FetchDictionaries;

      extRecs := FetchExtAppTable(etEmployee);

      FEEData.EEs.Last;
      FProgressIndicator.StartWait(opname, FEEData.EEs.RecordCount);
      try
        FEEData.EEs.First;
        while not FEEData.EEs.Eof do
        begin
          FLogger.LogEntry('Analyzing Evolution employee record');
          try
            try
              ctx := nil;
              eecode := trim(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
              FLogger.LogContextItem(sCtxEECode, eecode);

              FLogger.LogContextItem('First Name', FEEData.EEs.FieldByName('FIRST_NAME').AsString);
              FLogger.LogContextItem('Middle Initial', FEEData.EEs.FieldByName('MIDDLE_INITIAL').AsString);
              FLogger.LogContextItem('Last Name', FEEData.EEs.FieldByName('LAST_NAME').AsString);

              extRec := extRecs.FindRecordByCode(eecode);
              if extRec <> nil then
              begin
                changedFields := CompareFields(extRec, FEEData, true);
                if Length(changedFields) > 0 then
                  ctx := SyncActions.Add(eecode, euaUpdate, changedFields, -1, BuildExtAppEERecord(FEEData, extRec) ).Context;
              end
              else
                ctx := SyncActions.Add(eecode, euaCreate, nil, -1, BuildExtAppEERecord(FEEData, nil) ).Context;
              if ctx <> nil then
                ctx.Add(sCtxEECode + '=' + eecode);

            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          FProgressIndicator.Step(FEEData.EEs.RecNo);
          FEEData.EEs.Next;
        end;
        //don't delete anything
      finally
        FProgressIndicator.EndWait;
      end
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppEmployeeExporter.Apply_EEExport(actions: ISyncActionsRO): TUpdateEmployeesStat;
begin
  Result := ApplyActions(etEmployee, actions);
end;

function TExtAppEmployeeImporter.Analyze_EEImport: TAnalyzeResult;
var
  i: integer;
  changedFields: TChangedFieldRecs;
  code: string;
  ExtEEs: IExtAppResp;
  ExtEE: IExtAppRecord;
  SyncActions: ISyncActions;
  opname: string;
begin
  SyncActions := CreateSyncActions('Evolution', 'Employee');
  Result.Errors := 0;
  Result.Actions := SyncActions;
  opname := 'Analyzing employee records for import';
  FLogger.LogEntry(opname);
  try
    try
      ExtEEs := FetchExtAppTable(etEmployee);

      FProgressIndicator.StartWait(opname, ExtEEs.Count);
      try
        for i := 0 to ExtEEs.Count-1 do
        begin
          FLogger.LogEntry( Format('Analyzing %s employee record', [ExtAppName]));
          try
            try
              ExtEE := ExtEEs.Rec[i];
              code := trim(ExtEE.Code);

              FLogger.LogContextItem('First Name',  ExtEE.AsString['firstName']);
              FLogger.LogContextItem('Middle Name', ExtEE.AsString['middleName']);
              FLogger.LogContextItem('Last Name', ExtEE.AsString['lastName']);
              FLogger.LogContextItem(sCtxEECode, code );

              if FEEData.LocateEE(code) then
              begin
                changedFields := CompareFields(ExtEE, FEEData, false);
                InvertChangedFields(changedFields);
                if Length(changedFields) > 0 then
                  SyncActions.Add(code, euaUpdate, changedFields, -1, BuildEEEvoXImportRecord(ExtEE, false) );
              end
              else
              begin
                SyncActions.Add(code, euaCreate, nil, -1, BuildEEEvoXImportRecord(ExtEE, true) );
              end
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          FProgressIndicator.Step(i+1);
        end;
        //cannot delete via EvoX API
      finally
        FProgressIndicator.EndWait;
      end
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppEmployeeImporter.BuildEEEvoXImportHeader: string;
begin
  Result := '"CUSTOM_COMPANY_NUMBER","SSN","CUSTOM_EMPLOYEE_NUMBER","LAST_NAME","FIRST_NAME",'+
         '"BIRTH_DATE","CURRENT_HIRE_DATE","ADDRESS1","ADDRESS2","CITY",'+
         '"ZIP_CODE","STATE","E_MAIL_ADDRESS","ETHNICITY","Current_Status_Code",'+
         '"Current_Termination_Date","GENDER","Middle_initial","pay_frequency","federal_marital_status",'+
         '"federal_dependents","home_state","home_state_marital_status","home_state_dependents","veteran",'+
         '"vietnam_veteran","CUSTOM_DIVISION_NUMBER","CUSTOM_BRANCH_NUMBER","CUSTOM_DEPARTMENT_NUMBER","CUSTOM_TEAM_NUMBER",'+
         '"PHONE1","PHONE2","Original_Hire_Date","Eligible_For_Rehire","Salary_Amount",'+
         '"sui_state","flsa_exempt","COMPANY_OR_INDIVIDUAL_NAME","I9_ON_FILE",'+
         '"military_reserve","smoker","position","WORKERS_COMP_CODE","JOB",'+
         '"Override_Fed_Tax_Type","Override_Fed_Tax_Value","Override_State_Tax_Type","Override_State_Tax_Value",'+
         '"STANDARD_HOURS","VMR_PASSWORD","PAY_GROUP"';
end;

function TExtAppEmployeeImporter.BuildEEEvoXImportRecord(const extEE: IExtAppRecord; isNew: boolean): string;
  function AddVal(v: Variant): string;
  begin
     Result := '"' + StringReplace( VarToStr(v), '"', '""' , [rfReplaceAll] ) + '",';
  end;
  function ExtAppExemptToEvo(val: string): string;
  begin
    val := trim(val);
    if isNew and (Length(val)=1) and (val[1] in [GROUP_BOX_EXEMPT, GROUP_BOX_EXCLUDE, GROUP_BOX_INCLUDE]) then
      Result := val
    else
      Result := ''
  end;
begin
  Result := AddVal(FCompany.CUSTOM_COMPANY_NUMBER) +
            AddVal(extEE.AsString['ssn']) +
            AddVal(extEE.Code) +
            AddVal(extEE.AsString['lastName']) +
            AddVal(extEE.AsString['firstName']) +

            AddVal(extEE.AsDateVar['date_of_birth']) +
            AddVal(extEE.AsDateVar['hire_date']) +
            AddVal(extEE.AsString['streetAddr1']) +
            AddVal(extEE.AsString['streetAddr2']) +
            AddVal(extEE.AsString['city']) +

            AddVal(extEE.AsString['zip']) +
            AddVal(extEE.AsString['state']) +
            AddVal(extEE.AsString['email']) +
            AddVal(extEE.AsString['ethnicity']) +
            AddVal(extEE.AsCompositeCode['payroll_status']) +

            AddVal(extEE.AsDateVar['term_date']) +
            AddVal(ExtAppGenderToEvo(extEE.AsString['gender'])) +
            AddVal(Copy(extEE.AsString['middleName'],1,1)) +
            AddVal(ExtAppPayFrequencyToEvo(ExtEE.AsCompositeCode['pay_frequency'])) +
            AddVal(extEE.AsString['federal_marital_stat']) +

            AddVal(extEE.AsString['federal_dependents']) +
            AddVal(extEE.AsCompositeCode['tax_state']) +
            AddVal(extEE.AsCompositeCode['state_marital_status']) +
            AddVal(extEE.AsString['state_dependents']) +
            AddVal(extEE.AsString['veteran']) +

            AddVal(extEE.AsString['vietnam_veteran']) +
            AddVal(extEE.AsCompositeCode['division_level_1']) +
            AddVal(extEE.AsCompositeCode['location_level_2']) +
            AddVal(extEE.AsCompositeCode['department_level_3']) +
            AddVal(extEE.AsCompositeCode['team_level_4']) +

            AddVal(extEE.AsString['phone']) +
            AddVal(extEE.AsString['mobilePhone']) +
            AddVal(extEE.AsDateVar['original_hire_date']) +
            AddVal(extEE.AsString['rehire_eligible']) +
            AddVal(MoneyStrToFloat(extEE.AsString['salary_amount'])) +

            AddVal(extEE.AsCompositeCode['sui_state']) +
            AddVal(extEE.AsString['flsa_exempt']) +
            AddVal(ExtW2or1099ToEvo(extEE.AsString['w_2_or_1099_employee'])) +
            AddVal(BooleanStrToYN(extEE.AsString['i_9_on_file'])) +

            AddVal(extEE.AsString['military_reserve']) +
            AddVal(BooleanStrToYN(extEE.AsString['smoker'])) +
            AddVal(extEE.AsCompositeCode['job_title']) +
            AddVal(extEE.AsCompositeCode['workers_comp']) +
            AddVal(extEE.AsCompositeCode['job_level_5']) +

            AddVal(extEE.AsString['federal_override']) +
            AddVal(MoneyStrToFloat(extEE.AsString['additional_federal'])) +
            AddVal(extEE.AsString['state_override']) +
            AddVal(MoneyStrToFloat(extEE.AsString['additional_state']))+

            AddVal(FormattedNumberToFloatVar(extEE.AsString['standard_hours'])) +
            AddVal(extEE.AsString['vmr_password']) +
            AddVal(extEE.AsCompositeCode['pay_group']);

  Result := Copy(Result, 1, Length(Result)-1);
end;

function TExtAppEmployeeImporter.ImportActionsToEvoXContent(actions: ISyncActionsRO): TEvoXContent;
var
  i: integer;
begin
  Result.Errors := 0;
  Result.UpdateFileContent := '';
  Result.CreateFileContent := '';

  for i := 0 to actions.Count-1 do
  begin
    FLogger.LogEntry('Processing employee');
    try
      try
        FLogger.LogContextItem(sCtxEECode, actions.Get(i).Code);
        FLogger.LogContextItem('Action', SyncActionNames[actions.Get(i).Action]);
        case actions.Get(i).Action of
          euaCreate: Result.CreateFileContent := Result.CreateFileContent + #13#10 + actions.Get(i).NewVersion;
          euaUpdate: Result.UpdateFileContent := Result.UpdateFileContent + #13#10 + actions.Get(i).NewVersion;
          euaDelete: Assert(false);
        else
          Assert(false);
        end;
      except
        inc(Result.Errors);
        FLogger.StopException;
      end;
    finally
      FLogger.LogExit;
    end;
  end;
end;

function TExtAppEmployeeImporter.Apply_EEImport(actions: ISyncActionsRO): TUpdateEmployeesStat;
begin
  Result := Apply_ImportOp(ImportActionsToEvoXContent(actions), 'Employee', sEvoXMapFileAlias, BuildEEEvoXImportHeader);
end;

constructor TExtAppUpdater.Create(logger: ICommonLogger; company: TEvoCompanyDef; connParam: TExtAppConnectionParam; pi: IProgressIndicator; EvoX: IEvoXImportExecutor);
begin
  FLogger := logger;
  FExtConn := CreateExtAppConnection(connParam, FLogger);
  FCompany := company;
  FCompanyRecordId := FetchExtAppTable(etCompany).GetRecordByCode(FCompany.CUSTOM_COMPANY_NUMBER).Id;
  FProgressIndicator := pi;
  FEvoXImportExecutor := EvoX;
end;

function TExtAppUpdater.Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
begin
  Assert(false);
end;

function TExtAppUpdater.Apply_GroupedOp( groups: TSyncActionGroups): TSyncStat;
begin
  Assert(false);
end;

class function TExtAppUpdater.Opname: string;
begin
  Assert(false);
end;

function TExtAppUpdater.RecordHeader(recordId: Variant; extTable: TExtTable): string;
begin
  if VarIsNull(recordId) then
    Result := Format('<data objName="%s" useIds="false">', [ExtTableName(extTable)])
  else
    Result := Format('<data id="%d" useIds="false">', [Integer(recordId)]);
  if ExtTableDesc(extTable).TableLevel = tlCompany then
    AddVal(Result, 'company', IntToStr(FCompanyRecordId));
end;

function TExtAppUpdater.BuildExtAppRecord(extTable: TExtTable; recordId: Variant; code: Variant; name: Variant; parentId: Variant): string;
var
  desc: TExtTableDesc;
begin
  desc := ExtTableDesc(ExtTable);
  Result := RecordHeader(recordId, ExtTable);

  AddVal( Result, desc.codeField, code);
  if desc.nameField <> '' then
    AddVal( Result, desc.nameField, name);
  if desc.parentField <> '' then
    AddVal( Result, desc.parentField, parentID);

  Result := Result + '</data>';
  if VarIsNull(recordId) then
    Flogger.LogDebug('New ' + desc.SingularName, Result)
  else
    Flogger.LogDebug('New version of ' + desc.SingularName, Result);
end;

function TExtAppUpdater.ApplyActions(extTable: TExtTable; actions: ISyncActionsRO): TSyncStat;
begin
  Result := ApplyActions(extTable, SelectByActionType(actions,[euaCreate,euaUpdate,euaDelete]));
end;

function TExtAppUpdater.ApplyActions(extTable: TExtTable; actions: TSyncActionInfos): TSyncStat;
var
  i: integer;
  opname: string;
begin
  Result := BuildEmptySyncStat;
  opname := Format('Synchronizing %s %s', [ExtAppName, ExtTableDesc(extTable).PluralName]);
  FLogger.LogEntry(opname);
  try
    try
      FProgressIndicator.StartWait(opname, Length(actions));
      try
        for i := 0 to high(actions) do
        begin
          FLogger.LogEntry(Format('Synchronizing %s %s', [ExtAppName, ExtTableDesc(extTable).SingularName]));
          try
            try
              LogContext(FLogger, actions[i].Context);
              FLogger.LogContextItem('Action', SyncActionNames[actions[i].Action]);
              case actions[i].Action of
                euaCreate:
                begin
                  FExtConn.CreateObject(extTable, actions[i]);
                  inc(Result.Created);
                end;
                euaUpdate:
                begin
                  FExtConn.UpdateObject(extTable, actions[i]);
                  inc(Result.Modified);
                end;
                euaDelete:  Assert(false);
              else
                Assert(false);
              end;
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          FProgressIndicator.Step(i+1);
        end
      finally
        FProgressIndicator.EndWait;
      end
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppUpdater.FetchExtAppDBDTs;
var
  level: char;
begin
  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
    FExtAppDBDTs[level] := FetchExtAppTable(ExtAppDBDTLevels[level]);
end;

function TExtAppUpdater.GetDBDTRecordIDsByCodes(codes: TDBDTStrings): TDBDTStrings;
var
  level: char;
  parentID: integer;
  rec: IExtAppRecord;
begin
  parentID := 0; //to make compiler happy
  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
    Result[level] := '';

  for level := CLIENT_LEVEL_DIVISION to CLIENT_LEVEL_TEAM do
    if trim(codes[level]) <> '' then
    begin
      if level = CLIENT_LEVEL_DIVISION then
        rec := FExtAppDBDTs[level].GetRecordByCode(codes[level])
      else
        rec := FExtAppDBDTs[level].GetRecordByCodeAndParentId(codes[level], parentID);
      Result[level] := IntToStr(rec.Id);
      parentID := rec.Id;
    end
    else
      Exit;
end;

{ TExtAppDBDTUpdater }

constructor TExtAppDBDTUpdater.Create(logger: ICommonLogger;
  company: TEvoCompanyDef; EvoConn: IEvoAPIConnection;
  connParam: TExtAppConnectionParam; pi: IProgressIndicator);
begin
  inherited Create(logger, company, connParam, pi, nil);

  FEvoDBDTs := TEvoDBDTs.Create(EvoConn, company, Logger);
end;

destructor TExtAppDBDTUpdater.Destroy;
begin
  FreeAndNil(FEvoDBDTs);
  inherited;
end;

function TExtAppDBDTUpdater.ExportDBDTLevel(level: char; parentID: integer): TSyncStat;
var
  actions: ISyncActions;
  rec: IExtAppRecord;
  ds: TkbmCustomMemTable;
  code: string;
  name: string;
  changedFields: TChangedFieldRecs;
  ctx: IStr;

  function BuildRec(recordID: variant): string;
  begin
    Result := BuildExtAppRecord(ExtAppDBDTLevels[level], recordID, code, name, parentID);
  end;
begin
  FLogger.LogEntry(Format('Synchronizing %s', [Plural(LowerCase(CDBDTMetadata[level].Name))]));
  try
    try
      FLogger.LogDebug(Format('Parent id = %d', [parentId]));
      actions := CreateSyncActions(ExtAppName, CDBDTMetadata[level].Name);
      ds := FEvoDBDTs.DS[level];
      ds.First;
      while not ds.Eof do
      begin
        FLogger.LogEntry(Format('Synchronizing %s', [LowerCase(CDBDTMetadata[level].Name)]));
        try
          try
            code := trim(ds[CDBDTMetadata[level].CodeField]);

            ctx := EmptyIStr;
            ctx.Add(CDBDTMetadata[level].Name + ' code=' + code);
            LogContext(FLogger, ctx);

            code := EmptyToDash(code);
            name := EmptyToDash(ds[CDBDTMetadata[level].NameField]);

            if level = CLIENT_LEVEL_DIVISION then
              rec := FExtAppDBDTs[level].FindRecordByCode(code)
            else
              rec := FExtAppDBDTs[level].FindRecordByCodeAndParentId(code, parentID);

            if rec <> nil then
            begin
              FLogger.LogDebug(Format('Found a record: id = %d, code = %s', [rec.Id, rec.Code]));
              SetLength(changedFields, 0);
              CheckIfChanged(changedFields, rec.Name, name, CDBDTMetadata[level].Name + ' name');
              if Length(changedFields) > 0 then
                actions.Add(code, euaUpdate, changedFields, -1, BuildRec(rec.ID)).Context := ctx
              else
                FLogger.LogDebug('Unchanged');
            end
            else
            begin
              actions.Add(code, euaCreate, nil, -1, BuildRec(Null)).Context := ctx;
            end;
          except
            FLogger.StopException;
            inc(Result.Errors);
          end;
        finally
          FLogger.LogExit;
        end;
        ds.Next;
      end;

      Result := ApplyActions(ExtAppDBDTLevels[level], actions);

      if Length(SelectByActionType(actions, [euaCreate])) > 0 then
        FExtAppDBDTs[level] := FExtConn.GetCompanyTable(FCompanyRecordId, ExtAppDBDTLevels[level]); //read back

      ///////////

      if level < CLIENT_LEVEL_TEAM then
      begin
        ds.First;
        while not ds.Eof do
        begin
          code := trim(ds[CDBDTMetadata[level].CodeField]);
          if level = CLIENT_LEVEL_DIVISION then
            rec := FExtAppDBDTs[level].FindRecordByCode(code)
          else
            rec := FExtAppDBDTs[level].FindRecordByCodeAndParentId(code, parentID);
          if rec <> nil then
            Result := AddSyncStats( Result, ExportDBDTLevel(succ(level), rec.Id) );
          //else we have failed to create it, so it is already logged as error
          ds.Next;
        end;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppDBDTUpdater.ExportDBDTs: TSyncStat;
begin
  FetchExtAppDBDTs;
  Result := ExportDBDTLevel(CLIENT_LEVEL_DIVISION, FCompanyRecordId);
end;

{ TExtAppDictUpdater }

function TExtAppDictUpdater.Analyze_DictExport(dict: TSyncDictionary): TAnalyzeResult;
var
  SyncActions: ISyncActions;
  dictDesc: TDictDesc;
  evoDict: TkbmCustomMemTable;

  function BuildExtAppDictRecord(recordId: Variant): string;
  var
    nm: Variant;
  begin
    nm := Null;
    if ExtTableDesc(DictDesc.ExtTable).nameField <> '' then
      nm := evoDict['description'];
    Result := BuildExtAppRecord(DictDesc.ExtTable, recordId, evoDict[dictDesc.EvoKeyField], nm, Null);
  end;

var
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  evoKey: string;
  extDict: IExtAppResp;
  keyCtxCode: string;
  extRec: IExtAppRecord;

begin
  dictDesc := DictDescs[dict];
  SyncActions := CreateSyncActions(ExtAppName, dictDesc.EvoUserFriendlyName); //!!capitalize first letter
  Result.Errors := 0;
  Result.Actions := SyncActions;

  keyCtxCode := dictDesc.EvoUserFriendlyName + ' ' + dictDesc.EvoKeyField;

  FLogger.LogEntry(Format('Analyzing %s %s (%s)', [ExtAppName, ExtTableDesc(DictDesc.ExtTable).SingularName, DictDesc.EvoUserFriendlyName]));
  try
    try
      extDict := FetchExtAppTable(dictDesc.ExtTable);

      case DictDesc.EvoTableLevel of
        tlSystem: evoDict := ExecQuery(FEvoConn, FLogger, dictDesc.EvoQueryFilename, Format('Getting Evolution %s',[Plural(dictDesc.EvoUserFriendlyName)]) );
      //  tlClient: evoDict := ExecClQuery(FEvoConn, FLogger, FCompany.ClNbr, dictDesc.EvoQueryFilename, Format('Getting Evolution %s',[Plural(dictDesc.EvoUserFriendlyName)]) );
        tlCompany: evoDict := ExecCoQuery(FEvoConn, FLogger, FCompany, dictDesc.EvoQueryFilename, Format('Getting Evolution %s',[Plural(dictDesc.EvoUserFriendlyName)]) );
      else
        Assert(false)
      end;

      try
        evoDict.First;
        while not evoDict.Eof do
        begin
          FLogger.LogEntry(Format('Processing Evolution %s record',[dictDesc.EvoUserFriendlyName]));
          try
            try
              ctx := nil;
              evoKey := evoDict.FieldByName(dictDesc.EvoKeyField).AsString;
              FLogger.LogContextItem(keyCtxCode, evoKey);
               extRec := extDict.FindRecordByCode(evoKey);
              if extRec <> nil then
              begin
                SetLength(changedFields, 0);
                if ExtTableDesc(dictDesc.ExtTable).nameField <> '' then
                  CheckIfChanged(changedFields, extRec.AsString[ExtTableDesc(dictDesc.ExtTable).nameField], evoDict['DESCRIPTION'], 'Description');
                if Length(changedFields) > 0 then
                  ctx := SyncActions.Add(evoKey, euaUpdate, changedFields, -1, BuildExtAppDictRecord(extRec.Id) ).Context;
              end
              else
                ctx := SyncActions.Add(evoKey, euaCreate, nil, -1, BuildExtAppDictRecord(Null) ).Context;
              if ctx <> nil then
                ctx.Add(keyCtxCode+'='+evoKey);
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          evoDict.Next;
        end;

        //don't delete for now
      finally
        FreeAndNil(evoDict);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppDictUpdater.Apply_DictExport(dict: TSyncDictionary; actions: ISyncActionsRO): TSyncStat;
var
  dictDesc: TDictDesc;
begin
  dictDesc := DictDescs[dict];
  Result := ApplyActions( DictDesc.ExtTable, actions);
end;

function TExtAppDictUpdater.ExportDict(dict: TSyncDictionary): TSyncStat;
var
  analyzeResult: TAnalyzeResult;
begin
  analyzeResult := Analyze_DictExport(dict);
  Result := Apply_DictExport(dict, analyzeResult.Actions);
  Result.Errors := Result.Errors + analyzeResult.Errors;
end;

function TExtAppUpdater.FetchExtAppTable(table: TExtTable): IExtAppResp;
var
  desc: TExtTableDesc;
  opname: string;
begin
  desc := ExtTableDesc(table);
  opname := Format('Getting %s %s', [ExtAppName, desc.PluralName]);

  FLogger.LogEntry(opName);
  try
    try
      WaitIndicator.StartWait(opName);
      try

        if desc.TableLevel = tlCompany then
          Result := FExtConn.GetCompanyTable(FCompanyRecordId, table)
        else
          Result := FExtConn.GetTable(table);

      finally
        WaitIndicator.EndWait;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

procedure TExtAppUpdater.FetchDictionaries;
var
  dict: TSyncDictionary;
begin
  for dict := low(TSyncDictionary) to high(TSyncDictionary) do
    FDictionaries[dict] := FetchExtAppTable(DictDescs[dict].ExtTable);
end;

function TExtAppUpdater.GroupedImportActionsToEvoXContent(groups: TSyncActionGroups): TEvoXContent;
var
  i: integer;
  j: integer;
  action: TSyncActionInfo;
begin
  Result.Errors := 0;
  Result.UpdateFileContent := '';
  Result.CreateFileContent := '';

  for i := 0 to High(groups) do
    for j := 0 to groups[i].Actions.Count-1 do
    begin
      FLogger.LogEntry('Processing action');
      try
        try
          action := groups[i].Actions.Get(j);
          FLogger.LogContextItem('Action', SyncActionNames[action.Action]);
          LogContext(FLogger, action.Context);

          case action.Action of
            euaCreate: Result.CreateFileContent := Result.CreateFileContent + #13#10 + action.NewVersion;
            euaUpdate: Result.UpdateFileContent := Result.UpdateFileContent + #13#10 + action.NewVersion;
            euaDelete: Assert(false);
          else
            Assert(false);
          end;
        except
          inc(Result.Errors);
          FLogger.StopException;
        end;
      finally
        FLogger.LogExit;
      end;
    end;
end;

function TExtAppUpdater.Apply_ImportOp(const content: TEvoXContent; objName: string; mapFileAlias: string; header: string): TSyncStat;
var
  files: TEvoXImportInputFiles;
  evoxUpdateFileIdx: integer;
  evoxCreateFileIdx: integer;
  res: IEvoXImportResultsWrapper;
  abbr: string;
begin
  FLogger.LogEntry(Format('Synchronizing %s %s records', ['Evolution', AnsiLowerCase(objName)]));
  try
    try
      Result := BuildEmptySyncStat;
      Result.Errors := content.Errors;

      SetLength(files, 0);
      evoxCreateFileIdx := -1;
      evoxUpdateFileIdx := -1;

      abbr := StringReplace(Plural(objname), ' ', '', [rfReplaceAll]);
      if content.CreateFileContent <> '' then
      begin
        SetLength(files, Length(files)+1);
        evoxCreateFileIdx := High(files);
        files[evoxCreateFileIdx].Filename := Format('Create%sFrom%s.csv',[abbr,ExtAppName]);
        files[evoxCreateFileIdx].Filedata := header + content.CreateFileContent;
      end;
      if content.UpdateFileContent <> '' then
      begin
        SetLength(files, Length(files)+1);
        evoxUpdateFileIdx := High(files);
        files[evoxUpdateFileIdx].Filename := Format('Update%sFrom%s.csv',[abbr,ExtAppName]);
        files[evoxUpdateFileIdx].Filedata := header + content.UpdateFileContent;
      end;

      if Length(files) > 0 then
      begin
        Assert(FEvoXImportExecutor <> nil);
        res := FEvoXImportExecutor.RunEvoXImport(files, FileToString(Redirection.GetFilename(mapFileAlias)), objName);

        if evoxCreateFileIdx <> -1 then
        begin
          Assert(evoxCreateFileIdx < res.Count);
          Result.Created := res.Result[evoxCreateFileIdx].ImportedRowsCount;
          Result.Errors := Result.Errors + res.Result[evoxCreateFileIdx].ErrorCount;
          LogEvoXImportMessages(FLogger, res.Result[evoxCreateFileIdx].Messages);
        end;
        if evoxUpdateFileIdx <> -1 then
        begin
          Assert(evoxUpdateFileIdx < res.Count);
          Result.Modified := res.Result[evoxUpdateFileIdx].ImportedRowsCount;
          Result.Errors := Result.Errors + res.Result[evoxUpdateFileIdx].ErrorCount;
          LogEvoXImportMessages(FLogger, res.Result[evoxUpdateFileIdx].Messages);
        end;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

constructor TExtAppDictUpdater.Create(logger: ICommonLogger; company: TEvoCompanyDef; EvoConn: IEvoAPIConnection; connParam: TExtAppConnectionParam; pi: IProgressIndicator);
begin
  inherited Create(logger, company, connParam, pi, nil);
  FEvoConn := EvoConn;
  FetchDictionaries;
end;

function TExtAppUpdater.GetDictRecordIdByNullableCode(dict: TSyncDictionary; code: Variant): string;
var
  dictDesc: TDictDesc;
  rec: IExtappRecord;
begin
  dictDesc := DictDescs[dict];

  if not VarHasValue(code) then
  begin
    Result := '';
    exit;
  end;

  rec := FDictionaries[dict].FindRecordByCode(VarToStr(code));
  if rec <> nil then
  begin
    Result := IntToStr(rec.Id);
    exit;
  end;
  raise Exception.CreateFmt('Cannot find a %s %s with code <%s>', [ExtAppName, ExtTableDesc(dictDesc.ExtTable).SingularName, VarToStr(code)] );
end;

procedure DoDictExport(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; extAppConnParam: TExtAppConnectionParam; pi: IProgressIndicator; showGUI: boolean);
var
  opname: string;
  stat: TSyncStat;
  dictUpdater: TExtAppDictUpdater;
  DBDTUpdater: TExtAppDBDTUpdater;
  dict: TSyncDictionary;
begin
  stat := BuildEmptySyncStat;
  opname := Format('%s dictionary records update', [ExtAppName]);
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);
      WaitIndicator.StartWait( Format('%s dictionary update', [ExtAppName]) );
      try
        dictUpdater := TExtAppDictUpdater.Create(logger, company, evoConn, extAppConnParam, pi);
        try
          for dict := low(TSyncDictionary) to dictStates do
            stat := AddSyncStats(stat, dictUpdater.ExportDict(dict));
          stat := AddSyncStats(stat, dictUpdater.ExportStateMaritalStatus);
        finally
          FreeAndNil(dictUpdater);
        end;
        DBDTupdater := TExtAppDBDTUpdater.Create(logger, company, evoConn, extAppConnParam, pi);
        try
          stat := AddSyncStats(stat, DBDTupdater.ExportDBDTs);
        finally
          FreeAndNil(DBDTupdater);
        end;
      finally
        WaitIndicator.EndWait;
      end;
      ReportSyncStat(Logger, stat, opname, showGUI);
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure DoDictGuidExport(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; extAppConnParam: TExtAppConnectionParam; pi: IProgressIndicator; showGUI: boolean);
var
  opname: string;
  stat: TSyncStat;
  dictUpdater: TExtAppDictGuidUpdater;
  DBDTUpdater: TExtAppDBDTGuidUpdater;
begin
  stat := BuildEmptySyncStat;
  opname := Format('%s dictionary records GUID field update', [ExtAppName]);
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);
      WaitIndicator.StartWait( Format('%s dictionary GUID field update', [ExtAppName]) );
      try
        dictUpdater := TExtAppDictGuidUpdater.Create(logger, company, evoConn, extAppConnParam, pi);
        try
          stat := AddSyncStats(stat, dictUpdater.ExportDict(dictPosition));
          stat := AddSyncStats(stat, dictUpdater.ExportDict(dictWC));
          stat := AddSyncStats(stat, dictUpdater.ExportDict(dictJob));
          stat := AddSyncStats(stat, dictUpdater.ExportDict(dictED));
          stat := AddSyncStats(stat, dictUpdater.ExportDict(dictPayGroup));
          stat := AddSyncStats(stat, dictUpdater.ExportStateMaritalStatus);
        finally
          FreeAndNil(dictUpdater);
        end;

        DBDTupdater := TExtAppDBDTGuidUpdater.Create(logger, company, evoConn, extAppConnParam, pi);
        try
          stat := AddSyncStats(stat, DBDTupdater.ExportDBDTs);
        finally
          FreeAndNil(DBDTupdater);
        end;
      finally
        WaitIndicator.EndWait;
      end;
      ReportSyncStat(Logger, stat, opname, showGUI);
    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure DoGroupedOp(Logger: ICommonLogger; company: TEvoCompanyDef; evoConn: IEvoAPIConnection; extAppConnParam: TExtAppConnectionParam; pi: IProgressIndicator; showGUI: boolean; evox: IEvoXImportExecutor; updaterClass: TExtAppUpdaterClass);
var
  updater: TExtAppUpdater;
  opname: string;
  groupedAnalyzeResult: TGroupedAnalyzeResult;
  stat: TSyncStat;
begin
  stat := BuildEmptySyncStat;
  opname := updaterClass.Opname;
  Logger.LogEntry(opname);
  try
    try
      LogEvoCompanyDef(Logger, company);
      Logger.LogEventFmt('%s started', [opname]);

      WaitIndicator.StartWait( opname );
      try
        updater := nil;
        try
          WaitIndicator.StartWait('Preparing');
          try
            updater := updaterClass.Create(logger, company, extAppConnParam, pi, evox);

            groupedAnalyzeResult := updater.Analyze_GroupedOp(evoconn);

            if Length(groupedAnalyzeResult.Groups) > 0 then
              Logger.LogEvent( 'Planned actions. See details.', SyncActionGroupsToText(groupedAnalyzeResult.Groups, 'Employee') );
            if showGUI and not ConfirmActions( Application.MainForm, SyncActionGroupsToText(groupedAnalyzeResult.Groups, 'Employee') ) then
              raise Exception.Create('User cancelled operation');
          finally
            WaitIndicator.EndWait;
          end;

          WaitIndicator.StartWait('Updating');
          try
            stat := updater.Apply_GroupedOp(groupedAnalyzeResult.Groups);
            stat.Errors := stat.Errors + groupedAnalyzeResult.Errors;
          finally
            WaitIndicator.EndWait;
          end;

        finally
          FreeAndNil(updater);
        end;
      finally
        WaitIndicator.EndWait;
      end;

      ReportSyncStat(Logger, stat, opname, showGUI);

    except
      Logger.PassthroughExceptionAndWarnFmt(opname + ' failed',[]);
    end;
  finally
    Logger.LogExit;
  end;
end;

function GetSyncActions(var ar: TGroupedAnalyzeResult; newParentCode: string): ISyncActions;
begin
  if (Length(ar.Groups) = 0) or (ar.Groups[high(ar.Groups)].ParentCode <> newParentCode) then
  begin
    SetLength(ar.Groups, Length(ar.Groups)+1);
    ar.Groups[high(ar.Groups)].ParentCode := newParentCode;
    ar.Groups[high(ar.Groups)].Actions := CreateSyncActions(ExtAppName, ar.ObjName);
  end;
  Result := ar.Groups[high(ar.Groups)].Actions as ISyncActions;
end;

function BuildGroupedAnalyzeResult(objName: string): TGroupedAnalyzeResult;
begin
  Result.Errors := 0;
  SetLength(Result.Groups, 0);
  Result.ObjName := objName;
end;

function TExtAppDictUpdater.CompareSMSFields(extSMS: IExtAppRecord; evoSMS: TkbmCustomMemTable): TChangedFieldRecs;
begin
  SetLength(Result, 0);
  CheckIfChanged(Result, extSMS.AsString['name'], evoSMS['status_description'], 'Description');
end;

function TExtAppDictUpdater.Analyze_StateMaritalStatusExport: TGroupedAnalyzeResult;
var
  evoSMS: TkbmCustomMemTable;
  extState: IExtAppRecord;

  function BuildExtAppSMSRecord(oldExtRec: IExtAppRecord): string;
  begin
    if oldExtRec <> nil then
      Result := RecordHeader(oldExtRec.Id, etStateMaritalStatus)
    else
      Result := RecordHeader(Null, etStateMaritalStatus);
    AddVal(Result, 'name', evoSMS['status_description'] );
    if oldExtRec = nil then //new record
    begin
      AddVal(Result, 'marital_status_code', evoSMS['status_type'] );
      AddVal(Result, 'tax_state', extState.Id );
    end;
    Result := Result + '</data>';
    if oldExtRec = nil then
      Flogger.LogDebug('New state marital status', Result)
    else
      Flogger.LogDebug('New version of state marital status', Result);
  end;

var
  state: string;
  sms: string;
  extStates: IExtAppResp;
  extSMSes: IExtAppResp;
  extSMS: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
begin
  Result := BuildGroupedAnalyzeResult('State marital status');

  FLogger.LogEntry('Analyzing state marital status records');
  try
    try
      FDictionaries[dictStates] := FetchExtAppTable(DictDescs[dictStates].ExtTable); //refresh after dict import
      extStates := FDictionaries[dictStates];
      extSMSes := FDictionaries[dictSMS];
      evoSMS := ExecQuery(FEvoConn, FLogger, 'SY_STATE_MARITAL_STATUS.rwq', 'Getting Evolution state marital statuses' );
      try
        evoSMS.First;
        while not evoSMS.Eof do
        begin
          FLogger.LogEntry('Processing Evolution state marital status');
          try
            try
              state := trim(evoSMS['STATE']);
              FLogger.LogContextItem('State', state);
              sms := evoSMS.FieldByName('STATUS_TYPE').AsString;
              FLogger.LogContextItem('State marital status', sms);

              extState := extStates.FindRecordByCode(state);
              if extState <> nil then
              begin
                ctx := nil;
                extSMS := extSMSes.FindRecordByCodeAndParentId(sms, extState.Id);
                if extSMS <> nil then
                begin
                  changedFields := CompareSMSFields(extSMS, evoSMS);
                  if Length(changedFields) > 0 then
                    ctx := GetSyncActions(Result,state).Add(sms, euaUpdate, changedFields, -1, BuildExtAppSMSRecord(extSMS)).Context;
                end
                else
                  ctx := GetSyncActions(Result,state).Add(sms, euaCreate, nil, -1, BuildExtAppSMSRecord(nil)).Context;
                if ctx <> nil then
                begin
                  ctx.Add('State =' + state);
                  ctx.Add('State marital status =' + sms);
                end;
              end;
              //otherwise it's already reported as an error during employee export
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          evoSMS.Next;
        end;
      finally
        FreeAndNil(evoSMS);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppDictUpdater.Apply_StateMaritalStatusExport(groups: TSyncActionGroups): TSyncStat;
begin
  Result := ApplyActions(etStateMaritalStatus, SelectByActionType(groups, [euaCreate,euaUpdate]));
end;

function TExtAppDictUpdater.ExportStateMaritalStatus: TSyncStat;
var
  analyzeResult: TGroupedAnalyzeResult;
begin
  analyzeResult := Analyze_StateMaritalStatusExport;
  Result := Apply_StateMaritalStatusExport(analyzeResult.Groups);
  Result.Errors := Result.Errors + analyzeResult.Errors;
end;

{ TExtAppRatesUpdater }

function TExtAppRatesExporter.Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
var
  evoRates: TkbmCustomMemTable;
  extEE: IExtAppRecord;

  function BuildExtAppRateRecord(oldExtRec: IExtAppRecord): string;
  begin
    if oldExtRec <> nil then
      Result := RecordHeader(oldExtRec.Id, etRate)
    else
      Result := RecordHeader(Null, etRate);
    AddVal(Result, 'rate_amount', evoRates['rate_amount'] );
    AddVal(Result, 'primary_rate', YNVarToBooleanStr(evoRates['primary_rate']) );
    if oldExtRec = nil then //new record
    begin
      AddVal(Result, 'employee', extEE.Id );
      AddVal(Result, 'rate_number', evoRates['rate_number'] );
    end;
    Result := Result + '</data>';
    if oldExtRec = nil then
      Flogger.LogDebug('New rate', Result)
    else
      Flogger.LogDebug('New version of rate', Result);
  end;

var
  eecode: string;
  extEEs: IExtAppResp;
  extRates: IExtAppResp;
  extRate: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  opname: string;
begin
  Result := BuildGroupedAnalyzeResult('Rate');

  opname := 'Analyzing rates records for export';
  FLogger.LogEntry(opname);
  try
    try
      extEEs := FetchExtAppTable(etEmployee);
      extRates := FetchExtAppTable(etRate);
      evoRates := ExecCoQuery(EvoConn, FLogger, FCompany, 'CUSTOM_EE_RATES.rwq', 'Getting Evolution rates' );
      try
        evoRates.Last;
        FProgressIndicator.StartWait(opname, evoRates.RecordCount);
        try
          evoRates.First;
          while not evoRates.Eof do
          begin
            FLogger.LogEntry('Processing Evolution rate');
            try
              try
                eecode := trim(evoRates['CUSTOM_EMPLOYEE_NUMBER']);
                FLogger.LogContextItem(sCtxEECode, eecode );
                FLogger.LogContextItem('Rate number', evoRates.FieldByName('RATE_NUMBER').AsString);
                FLogger.LogContextItem('Primary rate', YNVarToBooleanStr(evoRates['PRIMARY_RATE']) );

                extEE := extEEs.FindRecordByCode(eecode);
                if extEE <> nil then
                begin
                  ctx := nil;
                  extRate := extRates.FindRecordByCodeAndParentId(evoRates.FieldByName('RATE_NUMBER').AsString, extEE.Id);
                  if extRate <> nil then
                  begin
                    changedFields := CompareFields(extRate, evoRates);
                    if Length(changedFields) > 0 then
                      ctx := GetSyncActions(Result,eecode).Add( evoRates.FieldByName('RATE_NUMBER').AsString, euaUpdate, changedFields, -1, BuildExtAppRateRecord(extRate) ).Context;
                  end
                  else
                    ctx := GetSyncActions(Result,eecode).Add( evoRates.FieldByName('RATE_NUMBER').AsString, euaCreate, nil, -1, BuildExtAppRateRecord(nil) ).Context;
                  if ctx <> nil then
                  begin
                    ctx.Add(sCtxEECode + '=' + eecode);
                    ctx.Add('Rate number' + '=' + evoRates.FieldByName('RATE_NUMBER').AsString);
                  end;
                end;
                //otherwise it's already reported as an error during employee export
              except
                inc(Result.Errors);
                FLogger.StopException;
              end;
            finally
              FLogger.LogExit;
            end;
            FProgressIndicator.Step(evoRates.RecNo);
            evoRates.Next;
          end;
        finally
          FProgressIndicator.EndWait;
        end
      finally
        FreeAndNil(evoRates);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppRatesUpdater.CompareFields(extRate: IExtAppRecord; evoRates: TkbmCustomMemTable): TChangedFieldRecs;
begin
  SetLength(Result, 0);
  CheckIfFloatChanged(Result, MoneyStrToFloat(extRate.AsString['rate_amount']), evoRates.FieldByName('RATE_AMOUNT').AsFloat, 'Rate amount', 0.01);
  CheckIfChanged(Result, extRate.AsString['primary_rate'], YNVarToBooleanStr(evoRates['PRIMARY_RATE']), 'Primary rate');
end;

function TExtAppRatesExporter.Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat;
begin
  Result := ApplyActions(etRate, SelectByActionType(groups, [euaCreate,euaUpdate]));
end;

class function TExtAppRatesExporter.Opname: string;
begin
  Result := Format('%s rates update', [ExtAppName]);
end;

function TExtAppRatesImporter.Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
var
  eecode: string;
  rateNumber: string;
  extRate: IExtAppRecord;

  function BuildRatesEvoXImportRecord: string;
    function AddVal(v: Variant): string;
    begin
       Result := '"' + StringReplace(VarToStr(v), '"', '""' , [rfReplaceAll]) + '",';
    end;
  begin
    Result := AddVal(FCompany.CUSTOM_COMPANY_NUMBER) +
            AddVal(eecode) +
            AddVal(rateNumber) +
            AddVal(MoneyStrToFloatStr(extRate.AsString['rate_amount'])) +
            AddVal(BooleanStrToYN(extRate.AsString['primary_rate']));
    Result := Copy(Result, 1, Length(Result)-1);
  end;

var
  evoRates: TkbmCustomMemTable;
  extRates: IExtAppResp;
  i: integer;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  opname: string;
  startDate, endDate: Variant;
begin
  Result := BuildGroupedAnalyzeResult('Rate');
  opname := 'Analyzing rates records for import';
  FLogger.LogEntry(opname);
  try
    try
      extRates := FetchExtAppTable(etRate);
      evoRates := ExecCoQuery(EvoConn, FLogger, FCompany, 'CUSTOM_EE_RATES.rwq', 'Getting Evolution rates' );
      try
        FProgressIndicator.StartWait(opname, extRates.Count);
        try
          for i := 0 to extRates.Count-1 do
          begin
            FLogger.LogEntry(Format('Processing %s rate record', [ExtAppName]));
            try
              try
                extRate := extRates.Rec[i];
                rateNumber := extRate.Code;
                FLogger.LogContextItem('Rate number', rateNumber);
                FLogger.LogContextItem('Primary rate', extRate.AsString['primary_rate']);

                eecode := trim(extRate.AsCompositeCode['employee']);
                FLogger.LogContextItem(sCtxEECode, eecode);
                startDate := extRate.AsDateVar['start_date'];
                FLogger.LogContextItem('Start date', VarToStr(startDate));
                endDate := extRate.AsDateVar['end_date'];
                FLogger.LogContextItem('End date', VarToStr(endDate));

                if not VarIsNull(startDate) and (startDate > DateOf(Now)) then
                begin
                  FLogger.LogDebug('Ignoring rate - start date in the future');
                  continue;
                end;
                if not VarIsNull(endDate) and (endDate < DateOf(Now)) then
                begin
                  FLogger.LogDebug('Ignoring rate - end date in the past');
                  continue;
                end;

                ctx := nil;
                if evoRates.Locate('CUSTOM_EMPLOYEE_NUMBER;RATE_NUMBER', VarArrayOf([eecode,StrToInt(rateNumber)]),[]) then
                begin
                  changedFields := CompareFields(extRate, evoRates);
                  InvertChangedFields(changedFields);
                  if Length(changedFields) > 0 then
                    ctx := GetSyncActions(Result,eecode).Add(rateNumber, euaUpdate, changedFields, -1, BuildRatesEvoXImportRecord).Context;
                end
                else
                  ctx := GetSyncActions(Result,eecode).Add(rateNumber, euaCreate, nil, -1, BuildRatesEvoXImportRecord).Context;
                if ctx <> nil then
                begin
                  ctx.Add(sCtxEECode + '=' + eecode);
                  ctx.Add('Rate number' + '=' + rateNumber);
                end;

              except
                inc(Result.Errors);
                FLogger.StopException;
              end;
            finally
              FLogger.LogExit;
            end;
            FProgressIndicator.Step(i+1);
          end;
        finally
          FProgressIndicator.EndWait;
        end
      finally
        FreeAndNil(evoRates);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppRatesImporter.Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat;
begin
  Result := Apply_ImportOp( GroupedImportActionsToEvoXContent(groups), 'Rate', sRatesXMapFileAlias, '"CUSTOM_COMPANY_NUMBER","CUSTOM_EMPLOYEE_NUMBER","RATE_NUMBER","RATE_AMOUNT","PRIMARY_RATE"');
end;

class function TExtAppRatesImporter.Opname: string;
begin
  Result := 'Evolution rates update';
end;

{ TExtAppScheduledEDsUpdater }

const
  sCalculationType = 'Calculation type';

function TExtAppScheduledEDsUpdater.CompareFields(extSchedED: IExtAppRecord; evoSchedEDs: TkbmCustomMemTable): TChangedFieldRecs;
begin
  SetLength(Result, 0);
//  CheckIfChanged(Result, extSchedED.AsString['name'], evoSchedEDs['DESCRIPTION'], 'Description');
  CheckIfChanged(Result, extSchedED.AsCompositeCode['calculation_type'], evoSchedEDs['Calculation_Type'], sCalculationType);
  CheckIfFloatChanged(Result, FormattedNumberToFloatVar(extSchedED.AsString['amount']), evoSchedEDs['AMOUNT'], 'Amount', 0.01);
  CheckIfFloatChanged(Result, FormattedNumberToFloatVar(extSchedED.AsString['percentage']), evoSchedEDs['PERCENTAGE'], 'Percentage', 0.01);
  CheckIfChanged(Result, extSchedED.AsDateVar['eff_end_date'], evoSchedEDs['Effective_End_Date'], 'Effective end date');
end;

function TExtAppScheduledEDsExporter.Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
var
  evoSchedEDs: TkbmCustomMemTable;
  extEE: IExtAppRecord;
  edcode: string;

  function BuildExtAppSchedEDRecord(oldExtRec: IExtAppRecord): string;
  begin
    if oldExtRec <> nil then
      Result := RecordHeader(oldExtRec.Id, etDeduction)
    else
      Result := RecordHeader(Null, etDeduction);
//    AddVal(Result, 'name', evoSchedEDs['DESCRIPTION']);
    AddVal(Result, 'calculation_type', GetDictRecordIdByNullableCode(dictCalcType, evoSchedEDs['Calculation_Type']) );
    AddVal(Result, 'amount', evoSchedEDs['AMOUNT']);
    AddVal(Result, 'percentage', evoSchedEDs['PERCENTAGE']);
    AddVal(Result, 'eff_end_date', DateVarToStr(evoSchedEDs['Effective_End_Date']) );

    if oldExtRec = nil then //new record
    begin
      AddVal(Result, 'employee', extEE.Id );
      AddVal(Result, 'deduction_code', GetDictRecordIdByNullableCode(dictED, edcode) );
      AddVal(Result, 'eff_start_date',  DateVarToStr(evoSchedEDs['Effective_Start_Date']) );
    end;

    Result := Result + '</data>';
    if oldExtRec = nil then
      Flogger.LogDebug('New scheduled e/d', Result)
    else
      Flogger.LogDebug('New version of scheduled e/d', Result);
  end;
var
  eecode: string;
  extEEs: IExtAppResp;
  extSchedEDs: IExtAppResp;
  extSchedED: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  opname: string;
begin
  Result := BuildGroupedAnalyzeResult('Scheduled E/D');

  FetchDictionaries;
  opname := 'Analyzing scheduled E/D records for export';
  FLogger.LogEntry(opname);
  try
    try
      extEEs := FetchExtAppTable(etEmployee);
      extSchedEDs := FetchExtAppTable(etDeduction);
      evoSchedEDs := ExecCoQuery(EvoConn, FLogger, FCompany, 'CUSTOM_EE_SCHEDULED_E_DS.rwq', 'Getting Evolution scheduled E/Ds' );
      try
        evoSchedEDs.Last;
        FProgressIndicator.StartWait(opname, evoSchedEDs.RecordCount);
        try
          evoSchedEDs.First;
          while not evoSchedEDs.Eof do
          begin
            FLogger.LogEntry('Processing Evolution scheduled E/D');
            try
              try
                eecode := trim(evoSchedEDs['CUSTOM_EMPLOYEE_NUMBER']);
                edcode := trim(evoSchedEDs['Custom_E_D_Code_Number']);

                ctx := EmptyIStr;
                ctx.Add(sCtxEECode + '=' + eecode);
                ctx.Add('E/D code=' + edcode);
                ctx.Add('E/D description=' + evoSchedEDs.FieldByName('DESCRIPTION').AsString);
                ctx.Add('Effective start date=' + evoSchedEDs.FieldByName('Effective_Start_Date').AsString);
                ctx.Add('Effective end date=' + evoSchedEDs.FieldByName('Effective_End_Date').AsString);
                LogContext(FLogger, ctx);

                extEE := extEEs.FindRecordByCode(eecode);
                if extEE <> nil then
                begin
                  extSchedED := extSchedEDs.FindSchedED(eecode, edcode, evoSchedEDs['Effective_Start_Date']);
                  if extSchedED <> nil then
                  begin
                    changedFields := CompareFields(extSchedED, evoSchedEDs);
                    if Length(changedFields) > 0 then
                      GetSyncActions(Result,eecode).Add(edcode, euaUpdate, changedFields, -1, BuildExtAppSchedEDRecord(extSchedED)).Context := ctx;
                  end
                  else
                    GetSyncActions(Result,eecode).Add(edcode, euaCreate, nil, -1, BuildExtAppSchedEDRecord(nil)).Context := ctx;
                end;
                //otherwise it's already reported as an error during employee export
              except
                inc(Result.Errors);
                FLogger.StopException;
              end;
            finally
              FLogger.LogExit;
            end;
            FProgressIndicator.Step(evoSchedEDs.RecNo);
            evoSchedEDs.Next;
          end;
        finally
          FProgressIndicator.EndWait;
        end
      finally
        FreeAndNil(evoSchedEDs);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppScheduledEDsExporter.Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat;
begin
  Result := ApplyActions(etDeduction, SelectByActionType(groups, [euaCreate,euaUpdate]));
end;

class function TExtAppScheduledEDsExporter.Opname: string;
begin
  Result := Format('%s deductions update', [ExtAppName]);
end;

{ TExtAppScheduledEDsImporter }

function IsCreatableCalcType(calcType: string): boolean;
begin
  Result := (calcType = CALC_METHOD_FIXED) or (calcType = CALC_METHOD_GROSS);
end;

function TExtAppScheduledEDsImporter.Analyze_GroupedOp(EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
var
  eecode: string;
  edcode: string;
  calcType: string;
  extSchedED: IExtAppRecord;

  function BuildSchedEDsEvoXImportRecord: string;
    function AddVal(v: Variant): string;
    begin
       Result := '"' + StringReplace(VarToStr(v), '"', '""' , [rfReplaceAll]) + '",';
    end;
  begin
    Result := AddVal(FCompany.CUSTOM_COMPANY_NUMBER) +
            AddVal(eecode) +
            AddVal(edcode) +
            AddVal(extSchedED.AsDateVar['eff_start_date']) +
            AddVal(calcType) +
            AddVal(FormattedNumberToFloatVar(extSchedED.AsString['amount'])) +
            AddVal(FormattedNumberToFloatVar(extSchedED.AsString['percentage'])) +
            AddVal(extSchedED.AsDateVar['eff_end_date']);
    Result := Copy(Result, 1, Length(Result)-1);
  end;

var
  evoSchedEDs: TkbmCustomMemTable;
  extSchedEDs: IExtAppResp;
  i: integer;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  evoCalcType: string;
  opname: string;
begin
  Result := BuildGroupedAnalyzeResult('Scheduled E/D');

  opname := Format('Analyzing %s %s records', [ExtAppName, AnsiLowerCase(ExtTableDesc(etDeduction).SingularName)]);
  FLogger.LogEntry(opname);
  try
    try
      extSchedEDs := FetchExtAppTable(etDeduction);
      evoSchedEDs := ExecCoQuery(EvoConn, FLogger, FCompany, 'CUSTOM_EE_SCHEDULED_E_DS.rwq', 'Getting Evolution scheduled E/Ds' );
      try
        FProgressIndicator.StartWait(opname, extSchedEDs.Count);
        try
          for i := 0 to extSchedEDs.Count-1 do
          begin
            FLogger.LogEntry(Format('Processing %s %s record', [ExtAppName, AnsiLowerCase(ExtTableDesc(etDeduction).SingularName)]));
            try
              try
                extSchedED := extSchedEDs.Rec[i];

                eecode := trim(VarToStr(extSchedED.AsCompositeCode['employee']));
                edcode := trim(VarToStr(extSchedED.AsCompositeCode['deduction_code']));
                calcType := trim(VarToStr(extSchedED.AsCompositeCode['calculation_type']));
                ctx := EmptyIStr;
                ctx.Add(sCtxEECode + '=' + eecode);
                ctx.Add('E/D code=' + edcode);
                ctx.Add('Calculation type=' + calcType);
                ctx.Add('Effective start date=' + VarToStr(extSchedED.AsDateVar['eff_start_date']));
                ctx.Add('Effective end date=' + VarToStr(extSchedED.AsDateVar['eff_end_date']));
                LogContext(FLogger, ctx);

                if evoSchedEDs.Locate('CUSTOM_EMPLOYEE_NUMBER;Custom_E_D_Code_Number;Effective_Start_Date', VarArrayOf([eecode,edcode,extSchedED.AsDateVar['eff_start_date']]),[]) then
                begin
                  evoCalcType := trim(VarToStr(evoSchedEDs['Calculation_Type']));
                  if (calcType <> evoCalcType) and (not IsCreatableCalcType(calcType) or not IsCreatableCalcType(evoCalcType)) then
                    raise Exception.CreateFmt('Not allowed to change scheduled E/D''s calculation type from <%s> to <%s>',[evoCalcType, calcType]);

                  changedFields := CompareFields(extSchedED, evoSchedEDs);
                  InvertChangedFields(changedFields);
                  if Length(changedFields) > 0 then
                    GetSyncActions(Result,eecode).Add(edcode, euaUpdate, changedFields, -1, BuildSchedEDsEvoXImportRecord).Context := ctx;
                end
                else
                  if IsCreatableCalcType(calcType) then
                    GetSyncActions(Result,eecode).Add(edcode, euaCreate, nil, -1, BuildSchedEDsEvoXImportRecord).Context := ctx
                  else
                    raise Exception.CreateFmt('Not allowed to create a scheduled E/D with <%s> calculation type',[calcType]);
              except
                inc(Result.Errors);
                FLogger.StopException;
              end;
            finally
              FLogger.LogExit;
            end;
            FProgressIndicator.Step(i+1);
          end;
        finally
          FProgressIndicator.EndWait;
        end
      finally
        FreeAndNil(evoSchedEDs);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppScheduledEDsImporter.Apply_GroupedOp(groups: TSyncActionGroups): TSyncStat;
begin
  Result := Apply_ImportOp( GroupedImportActionsToEvoXContent(groups), 'Scheduled E/D', sSchedEDsXMapFileAlias,
  '"CUSTOM_COMPANY_NUMBER","CUSTOM_EMPLOYEE_NUMBER","Custom_E_D_Code_Number","Effective_Start_Date","Calculation_Type","AMOUNT","PERCENTAGE","Effective_End_Date"');
end;

class function TExtAppScheduledEDsImporter.Opname: string;
begin
  Result := 'Evolution scheduled E/Ds update';
end;

constructor TExtAppEmployeeExporter.Create(logger: ICommonLogger;
  company: TEvoCompanyDef; EEData: TEvoEEData;
  connParam: TExtAppConnectionParam; pi: IProgressIndicator);
begin
  inherited Create(logger, company, EEData, connParam, pi, nil);
end;

{ TExtAppEmployeeGUIDExporter }

function TExtAppEmployeeGUIDExporter.Analyze_EEExport: TAnalyzeResult;
var
  SyncActions: ISyncActions;
  extRecs: IExtAppResp;
  extRec: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  eecode: string;
  ctx: IStr;
  opname: string;
begin
  SyncActions := CreateSyncActions(ExtAppName, 'Employee');
  Result.Errors := 0;
  Result.Actions := SyncActions;
  opname := 'Analyzing employee records for export GUIDs';
  FLogger.LogEntry(opname);
  try
    try
      extRecs := FetchExtAppTable(etEmployeeGuid);

      FEEData.EEs.Last;
      FProgressIndicator.StartWait(opname, FEEData.EEs.RecordCount);
      try
        FEEData.EEs.First;
        while not FEEData.EEs.Eof do
        begin
          FLogger.LogEntry('Analyzing Evolution employee record');
          try
            try
              ctx := nil;
              eecode := trim(FEEData.EEs['CUSTOM_EMPLOYEE_NUMBER']);
              FLogger.LogContextItem(sCtxEECode, eecode);

              FLogger.LogContextItem('First Name', FEEData.EEs.FieldByName('FIRST_NAME').AsString);
              FLogger.LogContextItem('Middle Initial', FEEData.EEs.FieldByName('MIDDLE_INITIAL').AsString);
              FLogger.LogContextItem('Last Name', FEEData.EEs.FieldByName('LAST_NAME').AsString);

              extRec := extRecs.FindRecordByCode(eecode);
              if extRec <> nil then
              begin
                changedFields := SetGuidField(extRec, FEEData, true);
                if Length(changedFields) > 0 then
                  ctx := SyncActions.Add(eecode, euaUpdate, changedFields, -1, BuildExtAppEERecord(FEEData, extRec) ).Context;
              end;

              if ctx <> nil then
                ctx.Add(sCtxEECode + '=' + eecode);
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          FProgressIndicator.Step(FEEData.EEs.RecNo);
          FEEData.EEs.Next;
        end;
        //don't delete anything
      finally
        FProgressIndicator.EndWait;
      end
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppEmployeeGUIDExporter.Apply_EEExport(
  actions: ISyncActionsRO): TUpdateEmployeesStat;
begin
  Result := ApplyActions(etEmployee, actions);
end;

function TExtAppEmployeeGUIDExporter.BuildExtAppEERecord(
  EEData: TEvoEEData; oldExtRec: IExtAppRecord): string;

  procedure AddField(tag: string; fn: string);
  begin
    AddVal(Result, tag, EEData.EEs[fn]);
  end;
begin
  Result := RecordHeader(oldExtRec.Id, etEmployee);
  AddField('empid', 'CUSTOM_EMPLOYEE_NUMBER');
  AddVal(Result, 'employee_guid', IntToStr(EEData.Company.ClNbr)+ '|' + EEData.EEs.FieldByName('EE_NBR').AsString );

  Result := Result + '</data>';
  Flogger.LogDebug('Update guid of employee', Result);
end;

constructor TExtAppEmployeeGUIDExporter.Create(logger: ICommonLogger;
  company: TEvoCompanyDef; EEData: TEvoEEData;
  connParam: TExtAppConnectionParam; pi: IProgressIndicator);
begin
  inherited Create(logger, company, EEData, connParam, pi, nil);
end;

function TExtAppEmployeeGUIDExporter.SetGuidField(
  const extRec: IExtAppRecord; EEData: TEvoEEData;
  exporting: boolean): TChangedFieldRecs;
begin
  Assert( trim(EEData.EEs.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) = trim(extRec.AsString['empid']) );
  SetLength(Result, 0);
  CheckIfChanged(Result, extRec.AsString['employee_guid'], IntToStr(EEData.Company.ClNbr)+ '|' + EEData.EEs.FieldByName('EE_NBR').AsString, 'Employee guid');
end;

{ TExtAppDictGuidUpdater }

function TExtAppDictGuidUpdater.Analyze_DictExport(
  dict: TSyncDictionary): TAnalyzeResult;
var
  SyncActions: ISyncActions;
  dictDesc: TDictDesc;
  evoDict: TkbmCustomMemTable;

  function BuildExtAppDictRecord(recordId: Variant): string;
  var
    desc: TExtTableDesc;
  begin
    desc := ExtTableDesc(DictDesc.ExtTable);
    Result := RecordHeader(recordId, DictDesc.ExtTable);

    AddVal( Result, dictDesc.ExtGuidField, evoDict['fGuid']);

    Result := Result + '</data>';
    if VarIsNull(recordId) then
      Flogger.LogDebug('New ' + desc.SingularName, Result)
    else
      Flogger.LogDebug('New version of ' + desc.SingularName, Result);
  end;

var
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  evoKey: string;
  extDict: IExtAppResp;
  keyCtxCode: string;
  extRec: IExtAppRecord;

begin
  dictDesc := DictDescs[dict];
  SyncActions := CreateSyncActions(ExtAppName, dictDesc.EvoUserFriendlyName); //!!capitalize first letter
  Result.Errors := 0;
  Result.Actions := SyncActions;

  keyCtxCode := dictDesc.EvoUserFriendlyName + ' ' + dictDesc.EvoKeyField;

  FLogger.LogEntry(Format('Analyzing %s %s (%s)', [ExtAppName, ExtTableDesc(DictDesc.ExtTable).SingularName, DictDesc.EvoUserFriendlyName]));
  try
    try
      extDict := FetchExtAppTable(dictDesc.ExtTable);
      evoDict := ExecCoQuery(FEvoConn, FLogger, FCompany, dictDesc.EvoQueryFilename, Format('Getting Evolution %s',[Plural(dictDesc.EvoUserFriendlyName)]) );
      try
        evoDict.First;
        while not evoDict.Eof do
        begin
          FLogger.LogEntry(Format('Processing Evolution %s record',[dictDesc.EvoUserFriendlyName]));
          try
            try
              ctx := nil;
              evoKey := evoDict.FieldByName(dictDesc.EvoKeyField).AsString;
              FLogger.LogContextItem(keyCtxCode, evoKey);
              extRec := extDict.FindRecordByCode(evoKey);
              if extRec <> nil then
              begin
                SetLength(changedFields, 0);
                CheckIfChanged(changedFields, extRec.AsString[dictDesc.ExtGuidField], evoDict['fGuid'], dictDesc.ExtGuidField);

                if Length(changedFields) > 0 then
                  ctx := SyncActions.Add(evoKey, euaUpdate, changedFields, -1, BuildExtAppDictRecord(extRec.Id) ).Context;
              end;
              if ctx <> nil then
                ctx.Add(keyCtxCode+'='+evoKey);
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          evoDict.Next;
        end;

        //don't delete for now
      finally
        FreeAndNil(evoDict);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppDictGuidUpdater.Analyze_StateMaritalStatusExport: TGroupedAnalyzeResult;
var
  evoSMS: TkbmCustomMemTable;
  extState: IExtAppRecord;

  function BuildExtAppSMSRecord(oldExtRec: IExtAppRecord): string;
  begin
    Result := RecordHeader(oldExtRec.Id, etStateMaritalStatus);
    AddVal(Result, 'guid', evoSMS.FieldByName('Nbr').AsString );
    Result := Result + '</data>';
    Flogger.LogDebug('New version of state marital status', Result);
  end;

var
  state: string;
  sms: string;
  extStates: IExtAppResp;
  extSMSes: IExtAppResp;
  extSMS: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
begin
  Result := BuildGroupedAnalyzeResult('State marital status');

  FLogger.LogEntry('Analyzing state marital status records');
  try
    try
      FDictionaries[dictStates] := FetchExtAppTable(DictDescs[dictStates].ExtTable); //refresh after dict import
      extStates := FDictionaries[dictStates];
      extSMSes := FDictionaries[dictSMS];
      evoSMS := ExecQuery(FEvoConn, FLogger, 'SY_STATE_MARITAL_STATUS.rwq', 'Getting Evolution state marital statuses' );
      try
        evoSMS.First;
        while not evoSMS.Eof do
        begin
          FLogger.LogEntry('Processing Evolution state marital status');
          try
            try
              state := trim(evoSMS['STATE']);
              FLogger.LogContextItem('State', state);
              sms := evoSMS.FieldByName('STATUS_TYPE').AsString;
              FLogger.LogContextItem('State marital status', sms);

              extState := extStates.FindRecordByCode(state);
              if extState <> nil then
              begin
                ctx := nil;
                extSMS := extSMSes.FindRecordByCodeAndParentId(sms, extState.Id);
                if extSMS <> nil then
                begin
                  SetLength(changedFields, 0);
                  CheckIfChanged(changedFields, 'guid', evoSMS.FieldByName('Nbr').AsString, 'SMS Guid');
                  if Length(changedFields) > 0 then
                    ctx := GetSyncActions(Result,state).Add(sms, euaUpdate, changedFields, -1, BuildExtAppSMSRecord(extSMS)).Context;
                end;
                if ctx <> nil then
                begin
                  ctx.Add('State =' + state);
                  ctx.Add('State marital status =' + sms);
                end;
              end;
              //otherwise it's already reported as an error during employee export
            except
              inc(Result.Errors);
              FLogger.StopException;
            end;
          finally
            FLogger.LogExit;
          end;
          evoSMS.Next;
        end;
      finally
        FreeAndNil(evoSMS);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

function TExtAppDictGuidUpdater.Apply_DictExport(dict: TSyncDictionary;
  actions: ISyncActionsRO): TSyncStat;
var
  dictDesc: TDictDesc;
begin
  dictDesc := DictDescs[dict];
  Result := ApplyActions( DictDesc.ExtTable, actions);
end;

function TExtAppDictGuidUpdater.Apply_StateMaritalStatusExport(
  groups: TSyncActionGroups): TSyncStat;
begin
  Result := ApplyActions(etStateMaritalStatus, SelectByActionType(groups, [euaCreate,euaUpdate]));
end;

constructor TExtAppDictGuidUpdater.Create(logger: ICommonLogger;
  company: TEvoCompanyDef; EvoConn: IEvoAPIConnection;
  connParam: TExtAppConnectionParam; pi: IProgressIndicator);
begin
  inherited Create(logger, company, connParam, pi, nil);
  FEvoConn := EvoConn;
  FetchGuidDictionaries;
end;

function TExtAppDictGuidUpdater.ExportDict(
  dict: TSyncDictionary): TSyncStat;
var
  analyzeResult: TAnalyzeResult;
begin
  analyzeResult := Analyze_DictExport(dict);
  Result := Apply_DictExport(dict, analyzeResult.Actions);
  Result.Errors := Result.Errors + analyzeResult.Errors;
end;

function TExtAppDictGuidUpdater.ExportStateMaritalStatus: TSyncStat;
var
  analyzeResult: TGroupedAnalyzeResult;
begin
  analyzeResult := Analyze_StateMaritalStatusExport;
  Result := Apply_StateMaritalStatusExport(analyzeResult.Groups);
  Result.Errors := Result.Errors + analyzeResult.Errors;
end;

procedure TExtAppDictGuidUpdater.FetchGuidDictionaries;
var
  dict: TSyncDictionary;
begin
  for dict := low(TSyncDictionary) to high(TSyncDictionary) do
    FDictionaries[dict] := FetchExtAppTable(DictDescs[dict].ExtGuidTable);
end;

{ TExtAppDBDTGuidUpdater }

function TExtAppDBDTGuidUpdater.BuildExtAppDBDTGuidRecord(
  extTable: TExtTable; recordId, fGuid: Variant): string;
var
  desc: TExtTableDesc;
begin
  desc := ExtTableDesc(ExtTable);
  Result := RecordHeader(recordId, ExtTable);

  if desc.nameField = 'level_1_name' then
    AddVal( Result, 'level_1_guid', fGuid)
  else if desc.nameField = 'level_2_name' then
    AddVal( Result, 'location_guid', fGuid)
  else if desc.nameField = 'level_3_name' then
    AddVal( Result, 'department_guid', fGuid)
  else if desc.nameField = 'level_4_name' then
    AddVal( Result, 'level_4_guid', fGuid)
  else
    Assert(False);

  Result := Result + '</data>';
  if VarIsNull(recordId) then
    Flogger.LogDebug('New ' + desc.SingularName, Result)
  else
    Flogger.LogDebug('New version of ' + desc.SingularName, Result);
end;

function TExtAppDBDTGuidUpdater.ExportDBDTLevel(level: char;
  parentID: integer): TSyncStat;
var
  actions: ISyncActions;
  rec: IExtAppRecord;
  ds: TkbmCustomMemTable;
  code: string;
  changedFields: TChangedFieldRecs;
  ctx: IStr;

  function GetGuidField: string;
  begin
    case level of
      CLIENT_LEVEL_DIVISION: Result := 'level_1_guid';
      CLIENT_LEVEL_BRANCH:   Result := 'location_guid';
      CLIENT_LEVEL_DEPT:     Result := 'department_guid';
      CLIENT_LEVEL_TEAM:     Result := 'level_4_guid';
    else
      Result := 'Company';
    end;
  end;

  function BuildRec(recordID: variant): string;
  begin
    Result := BuildExtAppDBDTGuidRecord(ExtAppDBDTLevels[level], recordID, ds['fGuid']);
  end;
begin
  FLogger.LogEntry(Format('Synchronizing %s', [Plural(LowerCase(CDBDTMetadata[level].Name))]));
  try
    try
      FLogger.LogDebug(Format('Parent id = %d', [parentId]));
      actions := CreateSyncActions(ExtAppName, CDBDTMetadata[level].Name);
      ds := FEvoDBDTs.DS[level];
      ds.First;
      while not ds.Eof do
      begin
        FLogger.LogEntry(Format('Synchronizing %s', [LowerCase(CDBDTMetadata[level].Name)]));
        try
          try
            code := trim(ds[CDBDTMetadata[level].CodeField]);

            ctx := EmptyIStr;
            ctx.Add(CDBDTMetadata[level].Name + ' code=' + code);
            LogContext(FLogger, ctx);

            code := EmptyToDash(code);

            if level = CLIENT_LEVEL_DIVISION then
              rec := FExtAppDBDTs[level].FindRecordByCode(code)
            else
              rec := FExtAppDBDTs[level].FindRecordByCodeAndParentId(code, parentID);

            if rec <> nil then
            begin
              FLogger.LogDebug(Format('Found a record: id = %d, code = %s', [rec.Id, rec.Code]));
              SetLength(changedFields, 0);
              CheckIfChanged(changedFields, rec.AsString[GetGuidField], ds['fGuid'], CDBDTMetadata[level].Name + ' name');
              if Length(changedFields) > 0 then
                actions.Add(code, euaUpdate, changedFields, -1, BuildRec(rec.ID)).Context := ctx
              else
                FLogger.LogDebug('Unchanged');
            end;
          except
            FLogger.StopException;
            inc(Result.Errors);
          end;
        finally
          FLogger.LogExit;
        end;
        ds.Next;
      end;

      Result := ApplyActions(ExtAppDBDTLevels[level], actions);

      ///////////

      if level < CLIENT_LEVEL_TEAM then
      begin
        ds.First;
        while not ds.Eof do
        begin
          code := trim(ds[CDBDTMetadata[level].CodeField]);
          if level = CLIENT_LEVEL_DIVISION then
            rec := FExtAppDBDTs[level].FindRecordByCode(code)
          else
            rec := FExtAppDBDTs[level].FindRecordByCodeAndParentId(code, parentID);
          if rec <> nil then
            Result := AddSyncStats( Result, ExportDBDTLevel(succ(level), rec.Id) );
          //else we have failed to create it, so it is already logged as error
          ds.Next;
        end;
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

{ TExtAppRatesGuidExporter }

function TExtAppRatesGuidExporter.Analyze_GroupedOp(
  EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
var
  evoRates: TkbmCustomMemTable;
  extEE: IExtAppRecord;

  function BuildExtAppRateRecord(oldExtRec: IExtAppRecord): string;
  begin
    Result := RecordHeader(oldExtRec.Id, etRate);
    AddVal(Result, 'rates_guid', evoRates['fGuid'] );
    Result := Result + '</data>';
    Flogger.LogDebug('New version of rate', Result);
  end;

var
  eecode: string;
  extEEs: IExtAppResp;
  extRates: IExtAppResp;
  extRate: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  opname: string;
begin
  Result := BuildGroupedAnalyzeResult('Rate');

  opname := 'Analyzing rates records for export guid values';
  FLogger.LogEntry(opname);
  try
    try
      extEEs := FetchExtAppTable(etEmployeeGuid);
      extRates := FetchExtAppTable(etRateGuid);
      evoRates := ExecCoQuery(EvoConn, FLogger, FCompany, 'CUSTOM_EE_RATES.rwq', 'Getting Evolution rates' );
      try
        evoRates.Last;
        FProgressIndicator.StartWait(opname, evoRates.RecordCount);
        try
          evoRates.First;
          while not evoRates.Eof do
          begin
            FLogger.LogEntry('Processing Evolution rate');
            try
              try
                eecode := trim(evoRates['CUSTOM_EMPLOYEE_NUMBER']);
                FLogger.LogContextItem(sCtxEECode, eecode );
                FLogger.LogContextItem('Rate number', evoRates.FieldByName('RATE_NUMBER').AsString);
                FLogger.LogContextItem('Primary rate', YNVarToBooleanStr(evoRates['PRIMARY_RATE']) );

                extEE := extEEs.FindRecordByCode(eecode);
                if extEE <> nil then
                begin
                  ctx := nil;
                  extRate := extRates.FindRecordByCodeAndParentId(evoRates.FieldByName('RATE_NUMBER').AsString, extEE.Id);
                  if extRate <> nil then
                  begin
                    SetLength(changedFields, 0);
                    CheckIfChanged(changedFields, extRate.AsString['rates_guid'], evoRates['fGuid'], 'Rate guid');
                    if Length(changedFields) > 0 then
                      ctx := GetSyncActions(Result,eecode).Add( evoRates.FieldByName('RATE_NUMBER').AsString, euaUpdate, changedFields, -1, BuildExtAppRateRecord(extRate) ).Context;
                  end;
                  
                  if ctx <> nil then
                  begin
                    ctx.Add(sCtxEECode + '=' + eecode);
                    ctx.Add('Rate number' + '=' + evoRates.FieldByName('RATE_NUMBER').AsString);
                  end;
                end;
                //otherwise it's already reported as an error during employee export
              except
                inc(Result.Errors);
                FLogger.StopException;
              end;
            finally
              FLogger.LogExit;
            end;
            FProgressIndicator.Step(evoRates.RecNo);
            evoRates.Next;
          end;
        finally
          FProgressIndicator.EndWait;
        end
      finally
        FreeAndNil(evoRates);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

class function TExtAppRatesGuidExporter.Opname: string;
begin
  Result := Format('%s rates guid field update', [ExtAppName]);
end;

{ TExtAppScheduledEDsGuidExporter }

function TExtAppScheduledEDsGuidExporter.Analyze_GroupedOp(
  EvoConn: IEvoAPIConnection): TGroupedAnalyzeResult;
var
  evoSchedEDs: TkbmCustomMemTable;
  extEE: IExtAppRecord;
  edcode: string;

  function BuildExtAppSchedEDRecord(oldExtRec: IExtAppRecord): string;
  begin
    Result := RecordHeader(oldExtRec.Id, etDeduction);
    AddVal(Result, 'deduction_guid', evoSchedEDs['fGuid']);
    Result := Result + '</data>';
    Flogger.LogDebug('New version of scheduled e/d', Result);
  end;
var
  eecode: string;
  extEEs: IExtAppResp;
  extSchedEDs: IExtAppResp;
  extSchedED: IExtAppRecord;
  changedFields: TChangedFieldRecs;
  ctx: IStr;
  opname: string;
begin
  Result := BuildGroupedAnalyzeResult('Scheduled E/D');

  FetchDictionaries;
  opname := 'Analyzing scheduled E/D records for export guid values';
  FLogger.LogEntry(opname);
  try
    try
      extEEs := FetchExtAppTable(etEmployeeGuid);
      extSchedEDs := FetchExtAppTable(etDeductionGuid);
      evoSchedEDs := ExecCoQuery(EvoConn, FLogger, FCompany, 'CUSTOM_EE_SCHEDULED_E_DS.rwq', 'Getting Evolution scheduled E/Ds' );
      try
        evoSchedEDs.Last;
        FProgressIndicator.StartWait(opname, evoSchedEDs.RecordCount);
        try
          evoSchedEDs.First;
          while not evoSchedEDs.Eof do
          begin
            FLogger.LogEntry('Processing Evolution scheduled E/D');
            try
              try
                eecode := trim(evoSchedEDs['CUSTOM_EMPLOYEE_NUMBER']);
                edcode := trim(evoSchedEDs['Custom_E_D_Code_Number']);

                ctx := EmptyIStr;
                ctx.Add(sCtxEECode + '=' + eecode);
                ctx.Add('E/D code=' + edcode);
                ctx.Add('E/D description=' + evoSchedEDs.FieldByName('DESCRIPTION').AsString);
                ctx.Add('Effective start date=' + evoSchedEDs.FieldByName('Effective_Start_Date').AsString);
                ctx.Add('Effective end date=' + evoSchedEDs.FieldByName('Effective_End_Date').AsString);
                LogContext(FLogger, ctx);

                extEE := extEEs.FindRecordByCode(eecode);
                if extEE <> nil then
                begin
                  extSchedED := extSchedEDs.FindSchedED(eecode, edcode, evoSchedEDs['Effective_Start_Date']);
                  if extSchedED <> nil then
                  begin
                    SetLength(changedFields, 0);
                    CheckIfChanged(changedFields, extSchedED.AsString['deduction_guid'], evoSchedEDs['fGuid'], 'Scheduled E/D guid');
                    if Length(changedFields) > 0 then
                      GetSyncActions(Result,eecode).Add(edcode, euaUpdate, changedFields, -1, BuildExtAppSchedEDRecord(extSchedED)).Context := ctx;
                  end;
                end;
                //otherwise it's already reported as an error during employee export
              except
                inc(Result.Errors);
                FLogger.StopException;
              end;
            finally
              FLogger.LogExit;
            end;
            FProgressIndicator.Step(evoSchedEDs.RecNo);
            evoSchedEDs.Next;
          end;
        finally
          FProgressIndicator.EndWait;
        end
      finally
        FreeAndNil(evoSchedEDs);
      end;
    except
      FLogger.PassthroughException;
    end;
  finally
    FLogger.LogExit;
  end;
end;

class function TExtAppScheduledEDsGuidExporter.Opname: string;
begin
  Result := Format('%s deductions guid field update', [ExtAppName]);
end;

end.

