program EvoMyHRAdmin;

uses
{$INCLUDE  ..\..\..\..\Common\IsClasses\isMemoryManager.inc}
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Forms,
  common,
  evoapiconnection,
  jclfileutils,
  gdyredir,
  EvoAPIClientMainForm in '..\..\common\EvoAPIClientMainForm.pas' {EvoAPIClientMainFm},
  gdyDialogBaseFrame in '..\..\common\gdycommon\dialogs\gdyDialogBaseFrame.pas' {DialogBase: TFrame},
  OptionsBaseFrame in '..\..\common\OptionsBaseFrame.pas' {OptionsBaseFrm: TFrame},
  gdyBaseFrame in '..\..\common\gdycommon\frames\gdyBaseFrame.pas' {BaseFrame: TFrame},
  gdyLoggerRichView in '..\..\common\gdycommon\gdyLoggerRichView.pas' {LoggerRichViewFrame: TFrame},
  gdyCommonLoggerView in '..\..\common\gdycommon\gdyCommonLoggerView.pas' {CommonLoggerViewFrame: TFrame},
  EvoAPIConnectionParamFrame in '..\..\common\EvoAPIConnectionParamFrame.pas' {EvoAPIConnectionParamFrm: TBaseFrame},
  EvolutionDSFrame in '..\..\common\EvolutionDSFrame.pas' {EvolutionDsFrm: TFrame},
  EvolutionCompanyFrame in '..\..\common\EvolutionCompanyFrame.pas' {EvolutionCompanyFrm: TFrame},
  EvolutionClCoFrame in '..\..\common\EvolutionClCoFrame.pas' {EvolutionClCoFrm: TFrame},
  gdySendMailLoggerView in '..\..\common\gdycommon\gdySendMailLoggerView.pas' {SendMailLoggerViewFrame: TCommonLoggerViewFrame},
  EeUtilityLoggerViewFrame in '..\..\common\EeUtilityLoggerViewFrame.pas' {EeUtilityLoggerViewFrm: TSendMailLoggerViewFrame},
  EvoAPIClientNewMainForm in '..\..\common\EvoAPIClientNewMainForm.pas' {EvoAPIClientNewMainFm: TEvoAPIClientMainFm},
  SchedulerFrame in '..\..\common\SchedulerFrame.pas' {SchedulerFrm: TFrame},
  SmtpConfigFrame in '..\..\common\SmtpConfigFrame.pas' {SmtpConfigFrm: TFrame},
  EvolutionCompanySelectorFrame in '..\..\common\EvolutionCompanySelectorFrame.pas' {EvolutionCompanySelectorFrm: TFrame},
  EvoWaitForm in '..\..\common\EvoWaitForm.pas' {EvoWaitFm},
  scheduledTask in '..\..\common\scheduledTask.pas',
  ExtAppDecl in 'ExtAppDecl.pas',
  ExtAppConnectionParamFrame in 'ExtAppConnectionParamFrame.pas' {ExtAppConnectionParamFrm: TFrame},
  ExtAppConnection in 'ExtAppConnection.pas',
  extapp in 'extapp.pas',
  MainForm in 'MainForm.pas' {MainFm: TEvoAPIClientNewMainFm},
  ExtAppConnectionParamDialog in 'ExtAppConnectionParamDialog.pas' {ExtAppConnectionParamDlg: TFrame},
  ExtAppTasks in 'ExtAppTasks.pas',
  Resp in 'xml\Resp.pas',
  GlobalSettingsFrame in 'GlobalSettingsFrame.pas' {GlobalSettingsFrm: TFrame};

{$R *.res}

begin
  LicenseKey := '2AE4A94AE3CF4CCD92F239D01114576C'; //EvoHRISLink
  ConfigVersion := 1;

{$ifndef FINAL_RELEASE}
  ForceDirectories( Redirection.GetDirectory(sDumpDirAlias) );
{$endif}

  Application.Initialize;
  Application.Title := 'Evolution MyHRAdmin Integration';
  if ParamCount = 0 then
  begin
    Application.CreateForm(TMainFm, MainFm);
  Application.Run;
  end
  else
  begin
    ExitCode := Ord(ExecuteScheduledTask(ParamStr(1), TExtAppTaskAdapter.Create));
  end
end.
