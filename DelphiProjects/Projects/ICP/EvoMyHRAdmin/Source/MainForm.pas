unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EvoAPIClientNewMainForm, ActnList,
  EvolutionCompanySelectorFrame, StdCtrls, Buttons, ComCtrls,
  SmtpConfigFrame, SchedulerFrame, ExtCtrls, OptionsBaseFrame,
  EvoAPIConnectionParamFrame, ExtAppConnectionParamFrame, scheduledTask,
  evoapiconnectionutils, passwordCache, common, extappdecl,
  GlobalSettingsFrame;

type
  TMainFm = class(TEvoAPIClientNewMainFm)
    GroupBox1: TGroupBox;
    ExtAppFrame: TExtAppConnectionParamFrm;
    actEEExport: TAction;
    actEEImport: TAction;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    actScheduleEEExport: TAction;
    BitBtn3: TBitBtn;
    actScheduleEEImport: TAction;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    actDictExport: TAction;
    actScheduleDictExport: TAction;
    GlobalSettingsFrame: TGlobalSettingsFrm;
    actGuidExport: TAction;
    btnPatchGuids: TBitBtn;
    chbExclRates: TCheckBox;
    chbExclDeductions: TCheckBox;
    procedure actEEExportUpdate(Sender: TObject);
    procedure actEEExportExecute(Sender: TObject);
    procedure actNeedExtAppAndCoUpdate(Sender: TObject);
    procedure actEEImportExecute(Sender: TObject);
    procedure actScheduleEEExportExecute(Sender: TObject);
    procedure actScheduleEEImportExecute(Sender: TObject);
    procedure actDictExportExecute(Sender: TObject);
    procedure actScheduleDictExportExecute(Sender: TObject);
    procedure actEEImportUpdate(Sender: TObject);
    procedure actGuidExportExecute(Sender: TObject);
  private
    FPasswordCache: TPasswordCache;
    function GetExtAppConnectionParam: TExtAppConnectionParam;

    procedure HandleEditParameters(task: IScheduledTask);
    procedure HandleExtAppConnectionParamChangedByUser(Sender: TObject);
    procedure HandleGlobalSettingsChangedByUser(Sender: TObject);

    function DoExportEmployees: TUpdateEmployeesStat;
    function DoImportEmployees: TUpdateEmployeesStat;
    procedure ForceSavePasswords;

    function DoExportEmployeeGuids: TUpdateEmployeesStat;
  protected
    procedure UnInitPerCompanySettings; override;
    procedure LoadPerCompanySettings(const Value: TEvoCompanyDef); override;
  public
    constructor Create( Owner: TComponent ); override;
    destructor Destroy; override;
  end;

var
  MainFm: TMainFm;

implementation

{$R *.dfm}

uses
  extapp, userActionHelpers, ExtAppConnectionParamDialog, gdyDialogEngine,
  extapptasks, evodata, PlannedActionConfirmationForm, gdyGlobalWaitIndicator,
  gdyClasses, EvoWaitForm;

{ TMainFm }

constructor TMainFm.Create(Owner: TComponent);
begin
  FPasswordCache := TPasswordCache.Create;
  inherited;
  try
    SchedulerFrame.Configure(Logger, TExtAppTaskAdapter.Create, HandleEditParameters);
  except
    Logger.StopException;
  end;

  ExtAppFrame.OnChangeByUser := HandleExtAppConnectionParamChangedByUser;
  GlobalSettingsFrame.OnChangeByUser := HandleGlobalSettingsChangedByUser;

  if EvoFrame.IsValid then
    PageControl1.ActivePageIndex := 2
  else
    PageControl1.ActivePageIndex := 0;
end;

destructor TMainFm.Destroy;
begin
  FreeAndNil(FPasswordCache);
  inherited;
end;

procedure TMainFm.HandleEditParameters(task: IScheduledTask);
begin
  extappTasks.EditTask(task, Logger, Self);
end;

procedure TMainFm.LoadPerCompanySettings(const Value: TEvoCompanyDef);
begin
  try
    ExtAppFrame.Clear(true);
    ExtAppFrame.Param := LoadExtAppConnectionParam(FSettings, CompanyUniquePath(Value));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.UnInitPerCompanySettings;
begin
  try
    ExtAppFrame.Clear(false);
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.HandleExtAppConnectionParamChangedByUser(Sender: TObject);
begin
  Assert(FEvoData <> nil);
  if FEvoData.CanGetClCo then
  try
    SaveExtAppConnectionParam( ExtAppFrame.Param, FSettings, CompanyUniquePath(FEvoData.GetClCo));
  except
    Logger.StopException;
  end;
end;

procedure TMainFm.actEEExportUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := ExtAppFrame.IsValid and FEvoData.CanGetClCo;// and ExtAppSettingsFrame.IsValid; // Check is used instead
end;

function TMainFm.DoExportEmployees: TUpdateEmployeesStat;
var
  ExtAppParam: TExtAppConnectionParam;
  EEData: TEvoEEData;
  updater: TExtAppEmployeeExporter;
  analysisResult: TAnalyzeResult;
begin
  ExtAppParam := GetExtAppConnectionParam;
  EEData := FEvoData.CreateEEData;
  try
    updater := nil;
    try
      WaitIndicator.StartWait('Preparing');
      try
        updater := TExtAppEmployeeExporter.Create(Logger, FEvoData.GetClCo, EEData, ExtAppParam, Self as IProgressIndicator);
        FPasswordCache.Add(ExtAppName, '', ExtAppParam.UserName, ExtAppParam.Password); //after first successfull call to ExtApp

        analysisResult := updater.Analyze_EEExport;
        if analysisResult.Actions.Count > 0 then
          Logger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        if not ConfirmActions(Self, analysisResult.Actions.ToText) then
          raise Exception.Create('User cancelled operation');
      finally
        WaitIndicator.EndWait;
      end;
      Result := updater.Apply_EEExport(analysisResult.Actions);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      FreeAndNil(updater);
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.actEEExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      userActionHelpers.RunEmployeeExport(Logger, DoExportEmployees, FEvoData.GetClCo, ExtAppName);
      extapp.DoGroupedOp(Logger, FEvoData.GetClCo, FEvoData.Connection, GetExtAppConnectionParam, Self as IProgressIndicator, true, nil, TExtAppRatesExporter);
      extapp.DoGroupedOp(Logger, FEvoData.GetClCo, FEvoData.Connection, GetExtAppConnectionParam, Self as IProgressIndicator, true, nil, TExtAppScheduledEDsExporter);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actNeedExtAppAndCoUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := ExtAppFrame.IsValid and FEvoData.CanGetClCo;
end;

function TMainFm.DoImportEmployees: TUpdateEmployeesStat;
var
  ExtAppParam: TExtAppConnectionParam;
  EEData: TEvoEEData;
  updater: TExtAppEmployeeImporter;
  analysisResult: TAnalyzeResult;
begin
  ExtAppParam := GetExtAppConnectionParam;

  EEData := FEvoData.CreateEEData;
  try
    updater := TExtAppEmployeeImporter.Create(Logger, FEvoData.GetClCo, EEData, ExtAppParam, Self as IProgressIndicator, CreateGUIEvoXImportExecutor(FEvoData.Connection, Self, Logger));
    try
      FPasswordCache.Add(ExtAppName, '', ExtAppParam.UserName, ExtAppParam.Password); //after first successfull call to ExtApp
      WaitIndicator.StartWait('Preparing');
      try
        analysisResult := updater.Analyze_EEImport;
        if analysisResult.Actions.Count > 0 then
          Logger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
        if not ConfirmActions(Self, analysisResult.Actions.ToText) then
          raise Exception.Create('User cancelled operation');
      finally
        WaitIndicator.EndWait;
      end;
      Result := updater.Apply_EEImport(analysisResult.Actions);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      FreeAndNil(updater);
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.actEEImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      userActionHelpers.RunEmployeeImport(Logger, DoImportEmployees, FEvoData.GetClCo);
      extapp.DoGroupedOp(Logger, FEvoData.GetClCo, FEvoData.Connection, GetExtAppConnectionParam, Self as IProgressIndicator, true, CreateGUIEvoXImportExecutor(FEvoData.Connection, Self, Logger), TExtAppRatesImporter);
//      if GlobalSettingsFrame.Settings.ImportScheduledEDs then
//        extapp.DoGroupedOp(Logger, FEvoData.GetClCo, FEvoData.Connection, GetExtAppConnectionParam, Self as IProgressIndicator, true, CreateGUIEvoXImportExecutor(FEvoData.Connection, Self, Logger), TExtAppScheduledEDsImporter);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

function TMainFm.GetExtAppConnectionParam: TExtAppConnectionParam;
var
  dlg: TExtAppConnectionParamDlg;
begin
  Result := ExtAppFrame.Param;

  if Result.Password = '' then
    Result.Password := FPasswordCache.Get(ExtAppName, '', Result.UserName);

      if Result.Password = '' then
  begin
    dlg := TExtAppConnectionParamDlg.Create(nil);
    try
      dlg.Caption := ExtAppLongName;
      dlg.Param := Result;
      with DialogEngine(dlg, Owner) do
        if ShowModal = mrOk then
          Result := dlg.Param;
      Repaint;
    finally
      FreeAndNil(dlg);
    end;
  end;

  if Result.Password = '' then
    raise Exception.Create(ExtAppLongName + ' password is not specified');
end;

procedure TMainFm.ForceSavePasswords;
var
  ExtAppParam: TExtAppConnectionParam;
begin
  EvoFrame.ForceSavePassword;

  ExtAppParam := GetExtAppConnectionParam;
  if trim(ExtAppFrame.Param.Password) = '' then
  begin
    ExtAppFrame.Param := ExtAppParam;
    HandleExtAppConnectionParamChangedByUser(nil);
  end;
end;

procedure TMainFm.actScheduleEEExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewEEExportTask(FEvoData.GetClCo{, EEExportOptionsFrame.Options}) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleEEImportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewEEImportTask(FEvoData.GetClCo{, EEImportOptionsFrame.Options}) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;


procedure TMainFm.actDictExportExecute(Sender: TObject);
var
  ExtAppParam: TExtAppConnectionParam;
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      if MessageDlg( Format('Export Evolution dictionaries and D/B/D/Ts to %s?',[ExtAppName]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        ExtAppParam := GetExtAppConnectionParam;
        DoDictExport(Logger, FEvoData.GetClCo, FEvoData.Connection, ExtAppParam, Self as IProgressIndicator, true);
        FPasswordCache.Add(ExtAppName, '', ExtAppParam.UserName, ExtAppParam.Password);
      end;
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.actScheduleDictExportExecute(Sender: TObject);
begin
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      ForceSavePasswords;
      PageControl1.ActivePage := tbshScheduler;
      SchedulerFrame.AddTask( NewDictExportTask(FEvoData.GetClCo) );
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
  end;
end;

procedure TMainFm.HandleGlobalSettingsChangedByUser(Sender: TObject);
begin
  SaveGlobalSettings(GlobalSettingsFrame.Settings, FSettings, '');
end;

procedure TMainFm.actEEImportUpdate(Sender: TObject);
begin
  actNeedExtAppAndCoUpdate(Sender);
  if GlobalSettingsFrame.Settings.ImportScheduledEDs then
    (Sender as TCustomAction).Caption := 'Employees, rates, scheduled E/Ds: MyHRAdmin -> Evolution'
  else
    (Sender as TCustomAction).Caption := 'Employees, rates: MyHRAdmin -> Evolution'
end;

function TMainFm.DoExportEmployeeGuids: TUpdateEmployeesStat;
var
  ExtAppParam: TExtAppConnectionParam;
  EEData: TEvoEEData;
  updater: TExtAppEmployeeGUIDExporter;
  analysisResult: TAnalyzeResult;
begin
  ExtAppParam := GetExtAppConnectionParam;
  EEData := FEvoData.CreateEEData;
  try
    updater := nil;
    try
      WaitIndicator.StartWait('Preparing');
      try
        updater := TExtAppEmployeeGUIDExporter.Create(Logger, FEvoData.GetClCo, EEData, ExtAppParam, Self as IProgressIndicator);
        FPasswordCache.Add(ExtAppName, '', ExtAppParam.UserName, ExtAppParam.Password); //after first successfull call to ExtApp

        analysisResult := updater.Analyze_EEExport;
        if analysisResult.Actions.Count > 0 then
          Logger.LogEvent( 'Planned actions. See details.', analysisResult.Actions.ToText );
//        if not ConfirmActions(Self, analysisResult.Actions.ToText) then
//          raise Exception.Create('User cancelled operation');
      finally
        WaitIndicator.EndWait;
      end;
      Result := updater.Apply_EEExport(analysisResult.Actions);
      Result.Errors := Result.Errors + analysisResult.Errors;
    finally
      FreeAndNil(updater);
    end;
  finally
    FreeAndNil(EEData);
  end;
end;

procedure TMainFm.actGuidExportExecute(Sender: TObject);
var
  stat: TUpdateEmployeesStat;
  ExtAppParam: TExtAppConnectionParam;
begin
  WaitIndicator.StartWait('Update Guid fields');
  Logger.LogEntry(Format('User clicked "%s"', [(Sender as TCustomAction).Caption]));
  try
    try
      stat := DoExportEmployeeGuids;
      ReportSyncStat(Logger, stat, 'Update employee Guid fields', false);

      ExtAppParam := GetExtAppConnectionParam;
      DoDictGuidExport(Logger, FEvoData.GetClCo, FEvoData.Connection, ExtAppParam, Self as IProgressIndicator, false);
      FPasswordCache.Add(ExtAppName, '', ExtAppParam.UserName, ExtAppParam.Password);

      if not chbExclRates.Checked then
        extapp.DoGroupedOp(Logger, FEvoData.GetClCo, FEvoData.Connection, GetExtAppConnectionParam, Self as IProgressIndicator, false, nil, TExtAppRatesGuidExporter);

      if not chbExclDeductions.Checked then
        extapp.DoGroupedOp(Logger, FEvoData.GetClCo, FEvoData.Connection, GetExtAppConnectionParam, Self as IProgressIndicator, false, nil, TExtAppScheduledEDsGuidExporter);
    except
      Logger.PassthroughException;
    end;
  finally
    Logger.LogExit;
    WaitIndicator.EndWait;
  end;
end;

end.
