inherited ExtAppConnectionParamFrm: TExtAppConnectionParamFrm
  Width = 397
  Height = 58
  object lblUserName: TLabel
    Left = 6
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Username'
  end
  object lblPassword: TLabel
    Left = 6
    Top = 33
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object lblPasswordComment: TLabel
    Left = 368
    Top = 40
    Width = 17
    Height = 13
  end
  object PasswordEdit: TPasswordEdit
    Left = 87
    Top = 29
    Width = 298
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object UserNameEdit: TEdit
    Left = 87
    Top = 4
    Width = 298
    Height = 21
    TabOrder = 0
  end
end
