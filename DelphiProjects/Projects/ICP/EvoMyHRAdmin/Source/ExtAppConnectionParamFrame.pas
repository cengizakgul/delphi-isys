unit ExtAppConnectionParamFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, extappdecl, PasswordEdit, OptionsBaseFrame;

type
  TExtAppConnectionParamFrm = class(TOptionsBaseFrm)
    lblUserName: TLabel;
    lblPassword: TLabel;
    PasswordEdit: TPasswordEdit;
    UserNameEdit: TEdit;
    lblPasswordComment: TLabel;
  private
    function GetParam: TExtAppConnectionParam;
    procedure SetParam(const Value: TExtAppConnectionParam);
  public
    property Param: TExtAppConnectionParam read GetParam write SetParam;
    function IsValid: boolean;
    procedure Check;
    procedure Clear(enable: boolean);
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TExtAppConnectionParamFrm }

procedure TExtAppConnectionParamFrm.Check;
var
  err: string;
  procedure AddErr(s: string);
  begin
    if err <> '' then
      err := err + ',';
    err := err + #13#10 + s;
  end;
begin
  err := '';
  if trim(Param.UserName) = '' then
    AddErr(lblUserName.Caption + ' is not specified');
  if err <> '' then
    raise Exception.CreateFmt('Invalid %s connection settings: %s', [ExtAppName, err]);
end;

procedure TExtAppConnectionParamFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    UserNameEdit.Text := '';
    PasswordEdit.Password := '';
  finally
    FBlockOnChange := false;
  end;
end;

function TExtAppConnectionParamFrm.GetParam: TExtAppConnectionParam;
begin
  Result.Username := UserNameEdit.Text;
  Result.Password := PasswordEdit.Password;
end;

function TExtAppConnectionParamFrm.IsValid: boolean;
begin
  Result := trim(UserNameEdit.Text) <> '';
end;

procedure TExtAppConnectionParamFrm.SetParam(const Value: TExtAppConnectionParam);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    UserNameEdit.Text := Value.Username;
    PasswordEdit.Password := Value.Password;
  finally
    FBlockOnChange := false;
  end;
end;

end.
