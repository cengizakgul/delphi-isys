unit ExtAppConnectionParamDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, gdyDialogBaseFrame, ExtAppConnectionParamFrame, OptionsBaseFrame, extappdecl;

type
  TExtAppConnectionParamDlg = class(TDialogBase)
    ExtAppConnectionParamFrame: TExtAppConnectionParamFrm;
  private
    function GetParam: TExtAppConnectionParam;
    procedure SetParam(const Value: TExtAppConnectionParam);
  public
    function MainControl: TWinControl; override;
    function CanClose: boolean; override;
  public
    property Param: TExtAppConnectionParam read GetParam write SetParam;
  end;

implementation

uses
  gdycommon;
{$R *.dfm}

{ TExtAppConnectionParamDlg }

function TExtAppConnectionParamDlg.CanClose: boolean;
begin
  Result := ExtAppConnectionParamFrame.Param.Password <> '';
end;

function TExtAppConnectionParamDlg.GetParam: TExtAppConnectionParam;
begin
  Result := ExtAppConnectionParamFrame.Param;
end;

function TExtAppConnectionParamDlg.MainControl: TWinControl;
begin
  Result := ExtAppConnectionParamFrame.PasswordEdit;
end;

procedure TExtAppConnectionParamDlg.SetParam(const Value: TExtAppConnectionParam);
begin
  ExtAppConnectionParamFrame.Param := Value;
  ExtAppConnectionParamFrame.UserNameEdit.Enabled := false;
end;

end.
