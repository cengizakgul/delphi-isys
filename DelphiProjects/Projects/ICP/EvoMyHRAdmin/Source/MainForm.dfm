inherited MainFm: TMainFm
  Left = 494
  Top = 163
  Width = 872
  Caption = 'MainFm'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    Width = 856
    inherited TabSheet3: TTabSheet
      inherited EvoFrame: TEvoAPIConnectionParamFrm
        Width = 848
        inherited GroupBox1: TGroupBox
          Width = 848
        end
      end
    end
    inherited tbshCompanySettings: TTabSheet
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 966
        Height = 89
        Align = alTop
        Caption = 'MyHRAdmin connection'
        TabOrder = 0
        inline ExtAppFrame: TExtAppConnectionParamFrm
          Left = 8
          Top = 24
          Width = 753
          Height = 57
          TabOrder = 0
          inherited lblPasswordComment: TLabel
            Left = 395
            Top = 33
            Width = 238
            Caption = 'Optional, required only for running scheduled tasks'
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Transfer'
      inherited pnlBottom: TPanel
        Width = 848
      end
      object BitBtn1: TBitBtn
        Left = 16
        Top = 56
        Width = 297
        Height = 25
        Action = actEEExport
        Caption = 'Employees, rates, scheduled E/Ds: Evolution -> MyHRAdmin'
        TabOrder = 3
      end
      object BitBtn2: TBitBtn
        Left = 16
        Top = 96
        Width = 297
        Height = 25
        Action = actEEImport
        Caption = 'Employees, rates, scheduled E/Ds: MyHRAdmin -> Evolution'
        TabOrder = 5
      end
      object BitBtn3: TBitBtn
        Left = 344
        Top = 56
        Width = 97
        Height = 25
        Action = actScheduleEEExport
        Caption = 'Create task'
        TabOrder = 4
      end
      object BitBtn5: TBitBtn
        Left = 344
        Top = 96
        Width = 97
        Height = 25
        Action = actScheduleEEImport
        Caption = 'Create task'
        TabOrder = 6
      end
      object BitBtn6: TBitBtn
        Left = 16
        Top = 16
        Width = 297
        Height = 25
        Action = actDictExport
        Caption = 'Dictionaries: Evolution -> MyHRAdmin'
        TabOrder = 1
      end
      object BitBtn7: TBitBtn
        Left = 344
        Top = 16
        Width = 97
        Height = 25
        Action = actScheduleDictExport
        Caption = 'Create task'
        TabOrder = 2
      end
      inline GlobalSettingsFrame: TGlobalSettingsFrm
        Left = 460
        Top = 93
        Width = 181
        Height = 30
        TabOrder = 7
        Visible = False
      end
      object btnPatchGuids: TBitBtn
        Left = 16
        Top = 146
        Width = 297
        Height = 25
        Action = actGuidExport
        Caption = 'Guid values: Evolution -> MyHRAdmin'
        TabOrder = 8
      end
      object chbExclRates: TCheckBox
        Left = 30
        Top = 183
        Width = 129
        Height = 17
        Caption = 'Exclude Rates'
        TabOrder = 9
      end
      object chbExclDeductions: TCheckBox
        Left = 30
        Top = 204
        Width = 149
        Height = 17
        Caption = 'Exclude Deductions'
        TabOrder = 10
      end
    end
    inherited tbshScheduler: TTabSheet
      inherited SchedulerFrame: TSchedulerFrm
        Width = 848
        inherited pnlTasksControl: TPanel
          Width = 848
        end
        inherited PageControl2: TPageControl
          Width = 848
          inherited tbshTasks: TTabSheet
            inherited Splitter1: TSplitter
              Width = 840
            end
            inherited pnlTasks: TPanel
              Width = 840
              inherited dgTasks: TReDBGrid
                Width = 768
                Height = 88
              end
              inherited Panel1: TPanel
                Top = 88
                Width = 768
              end
            end
            inherited pnlDetails: TPanel
              Width = 840
              inherited pcTaskDetails: TPageControl
                Width = 840
                inherited tbshResults: TTabSheet
                  inherited Panel2: TPanel
                    Left = 593
                  end
                  inherited dgResults: TReDBGrid
                    Width = 593
                  end
                end
              end
            end
          end
        end
      end
    end
    inherited tbshSchedulerSettings: TTabSheet
      inherited SmtpConfigFrame: TSmtpConfigFrm
        Width = 776
        inherited GroupBox1: TGroupBox
          Width = 776
        end
      end
    end
  end
  inherited StatusBar1: TStatusBar
    Width = 856
  end
  inherited pnlCompany: TPanel
    Width = 856
    inherited Panel1: TPanel
      Width = 99
      inherited BitBtn4: TBitBtn
        Width = 89
        Caption = 'Get data'
      end
    end
    inherited CompanyFrame: TEvolutionCompanySelectorFrm
      Left = 99
      Width = 757
    end
  end
  inherited ActionList1: TActionList
    Left = 180
    Top = 176
    inherited ConnectToEvo: TAction
      Caption = 'Get data'
    end
    object actEEExport: TAction
      Caption = 'Employees, rates, scheduled E/Ds: Evolution -> MyHRAdmin'
      OnExecute = actEEExportExecute
      OnUpdate = actEEExportUpdate
    end
    object actEEImport: TAction
      Caption = 'Employees, rates, scheduled E/Ds: MyHRAdmin -> Evolution'
      OnExecute = actEEImportExecute
      OnUpdate = actEEImportUpdate
    end
    object actScheduleEEExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEEExportExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
    object actScheduleEEImport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleEEImportExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
    object actDictExport: TAction
      Caption = 'Dictionaries: Evolution -> MyHRAdmin'
      OnExecute = actDictExportExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
    object actScheduleDictExport: TAction
      Caption = 'Create task'
      OnExecute = actScheduleDictExportExecute
      OnUpdate = actNeedExtAppAndCoUpdate
    end
    object actGuidExport: TAction
      Caption = 'Guid values: Evolution -> MyHRAdmin'
      OnExecute = actGuidExportExecute
      OnUpdate = actEEExportUpdate
    end
  end
end
