unit GlobalSettingsFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ExtAppdecl, PasswordEdit, OptionsBaseFrame;

type
  TGlobalSettingsFrm = class(TOptionsBaseFrm)
    cbScheduledEDs: TCheckBox;
  private
    function GetSettings: TGlobalSettings;
    procedure SetSettings(const Value: TGlobalSettings);
  public
    property Settings: TGlobalSettings read GetSettings write SetSettings;
    procedure Clear(enable: boolean);
  end;

implementation

uses
  gdycommon;

{$R *.dfm}

{ TGlobalSettingsFrm }

procedure TGlobalSettingsFrm.Clear(enable: boolean);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, enable);
    cbScheduledEDs.Checked := false;
  finally
    FBlockOnChange := false;
  end;
end;

function TGlobalSettingsFrm.GetSettings: TGlobalSettings;
begin
  Result.ImportScheduledEDs := cbScheduledEDs.Checked;
end;

procedure TGlobalSettingsFrm.SetSettings(const Value: TGlobalSettings);
begin
  FBlockOnChange := true;
  try
    EnableControlRecursively(Self, true);
    cbScheduledEDs.Checked := Value.ImportScheduledEDs;
  finally
    FBlockOnChange := false;
  end;
end;

end.
