unit ExtAppDecl;

interface

uses
  gdystrset, RateImportOptionFrame, isSettings, gdyCommonLogger, 
  common, sysutils, gdyBinder;

const
	ExtAppName = 'MyHRAdmin';
  ExtAppLongName  = 'MyHRAdmin';
  
type
  TExtAppConnectionParam = record
    UserName: string;
    Password: string;
  end;

  EExtAppError = class(Exception);

function LoadExtAppConnectionParam(conf: IisSettings; root: string): TExtAppConnectionParam;
procedure SaveExtAppConnectionParam( const param: TExtAppConnectionParam; conf: IisSettings; root: string);
procedure LogExtAppConnectionParam(Logger: ICommonLogger; const param: TExtAppConnectionParam);

type
  TGlobalSettings = record
    ImportScheduledEDs: boolean;
  end;


function LoadGlobalSettings(conf: IisSettings; root: string): TGlobalSettings;
procedure SaveGlobalSettings( const param: TGlobalSettings; conf: IisSettings; root: string);
procedure LogGlobalSettings(Logger: ICommonLogger; const param: TGlobalSettings);

implementation

uses
  gdycommon, gdyClasses, gdyCrypt;

const
  cKey: string = 'Form.Button';

function LoadExtAppConnectionParam(conf: IisSettings; root: string): TExtAppConnectionParam;
begin
  root := root + IIF(root='','','\') + 'ExtAppConnection\';
  Result.UserName := conf.AsString[root+'UserName'];
  Result.Password := Decrypt( HexToBin(conf.AsString[root+'PasswordHex']), cKey);
end;

procedure SaveExtAppConnectionParam( const param: TExtAppConnectionParam; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'ExtAppConnection\';
  conf.AsString[root+'Username'] := param.Username;
  conf.AsString[root+'PasswordHex'] := BinToHex(Encrypt(param.Password, cKey));
end;

procedure LogExtAppConnectionParam(Logger: ICommonLogger; const param: TExtAppConnectionParam);
begin
  logger.LogContextItem( 'User name', param.UserName );
end;

function LoadGlobalSettings(conf: IisSettings; root: string): TGlobalSettings;
begin
  root := root + IIF(root='','','\') + 'GlobalSettings\';
  Result.ImportScheduledEDs := conf.AsBoolean[root+'ImportScheduledEDs'];
end;

procedure SaveGlobalSettings( const param: TGlobalSettings; conf: IisSettings; root: string);
begin
  root := root + IIF(root='','','\') + 'GlobalSettings\';
  conf.AsBoolean[root+'ImportScheduledEDs'] := param.ImportScheduledEDs;
end;

procedure LogGlobalSettings(Logger: ICommonLogger; const param: TGlobalSettings);
begin
  logger.LogContextItem( 'Transfer of scheduled E/Ds enabled', BoolToStr(param.ImportScheduledEDs, true) );
end;

end.
