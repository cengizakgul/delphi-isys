inherited ExtAppConnectionParamDlg: TExtAppConnectionParamDlg
  Width = 402
  Height = 57
  inline ExtAppConnectionParamFrame: TExtAppConnectionParamFrm
    Left = 0
    Top = 0
    Width = 402
    Height = 57
    Align = alClient
    TabOrder = 0
    inherited lblPasswordComment: TLabel
      Width = 3
    end
    inherited PasswordEdit: TPasswordEdit
      Width = 306
      Anchors = [akLeft, akTop, akRight]
    end
    inherited UserNameEdit: TEdit
      Width = 306
      Anchors = [akLeft, akTop, akRight]
    end
  end
end
