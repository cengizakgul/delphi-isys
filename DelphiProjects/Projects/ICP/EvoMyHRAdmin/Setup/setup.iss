#define APPBASE "EvoMyHRAdmin"
#define APPNAME "Evolution MyHRAdmin Integration"

[Files]
Source: ..\Resources\map.xml; DestDir: {app}; Flags: ignoreversion
Source: ..\Resources\map-rates.xml; DestDir: {app}; Flags: ignoreversion
Source: ..\Resources\map-eds.xml; DestDir: {app}; Flags: ignoreversion
Source: ..\Resources\config.ini; DestDir: {app}; Flags: ignoreversion

#include <..\..\Common\Installer\common.iss>

