[Directories]
Resources=%AppDir%

MyLocalAppData=%LocalAppDataDir%\iSystems\Evolution MyHRAdmin Integration
MyAppData=%AppDataDir%\iSystems\Evolution MyHRAdmin Integration

Queries=%Resources%\queries
LogArchives=%MyLocalAppData%\To Send
SchedulerData=%MyLocalAppData%\Scheduler
HTMLTemplates=%Resources%\HTML Templates
DumpDir=%MyLocalAppData%\log

[Filenames]
Certificate=%Resources%\cert.pem
SevenZipDll=%Resources%\7za.dll
Log=%MyAppData%\user.log
DebugLog=%MyLocalAppData%\debug.log

EvoXMap=%Resources%\map.xml
RateEvoXMap=%Resources%\map-rates.xml
SchedEDsEvoXMap=%Resources%\map-eds.xml
Config=%Resources%\config.ini






;************  Redirection file documentation  ************
;
;Predefined macros:
;
;AppDir
;The directory where the executable file is located.
;
;WorkDir
;The current directory.
;
;TempDir
;Windows temporary directory.
;
;ThisDir
;The directory where this redirection file is located.
;
;AppDataDir
;The file system directory that serves as a common repository for application-specific data.
;A typical path is C:\Documents and Settings\username\Application Data.
;
;CommonAppDataDir
;The file system directory that contains application data for all users.
;A typical path is C:\Documents and Settings\All Users\Application Data.
;This folder is used for application data that is not user specific.
;For example, an application can store a spell-check dictionary,
;a database of clip art, or a log file in the CSIDL_COMMON_APPDATA folder.
;This information will not roam and is available to anyone using the computer.
;
;LocalAppDataDir
;The file system directory that serves as a data repository for local (nonroaming) applications.
;A typical path is C:\Documents and Settings\username\Local Settings\Application Data.
;
;The directory names DON'T have trailing backslash, macro expansion is case INsensitive.
