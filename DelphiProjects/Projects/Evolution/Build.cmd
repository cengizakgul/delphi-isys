@ECHO OFF

IF "%1" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. Application Version
ECHO    2. Application released versions root folder (optional) 
ECHO    3. WiX Installer root folder (optional)
GOTO end

:start

SET pAppVersion=%1
IF "%pAppVersion%" NEQ ""  SET pAppVersion=%pAppVersion:"=%
SET pReleasedDir=%2
IF "%pReleasedDir%" NEQ "" SET pReleasedDir=%pReleasedDir:"=%
SET pWiXDir=%3
IF "%pWiXDir%" NEQ "" SET pWiXDir=%pWiXDir:"=%

FOR %%a IN (..\..\Bin\Evolution\*.*) DO DEL /F /Q /S ..\..\Bin\Evolution\*.* > nul

CALL Compile.cmd "%pAppVersion%"

ECHO *Building version archive
..\..\Bin\Evolution\EvVersionPackageCreator.exe ..\..\Bin\Evolution ..\..\Bin\Evolution "%pReleasedDir%"
IF ERRORLEVEL 1 EXIT 1

IF "%pWiXDir%" == "" GOTO end
ECHO *Building installers
CD .\Installer
CALL .\Build.cmd "%pWiXDir%" %pAppVersion%

:end
