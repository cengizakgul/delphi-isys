// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit AtomContextPropertyEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, Spin,  Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, EvSecElement, ExtDlgs, DesignEditors, DesignIntf,
  ISBasicClasses, EvUIComponents;

type
  TAtomContextsEditScreen = class(TForm)
    bPopulate: TevButton;
    lbContexts: TListBox;
    edName: TevEdit;
    lCustomTag: TevLabel;
    edCustomTag: TevEdit;
    edPriority: TevSpinEdit;
    iIcon: TImage;
    bCustomIcon: TButton;
    edStandartIcon: TevDBComboBox;
    Bevel1: TBevel;
    cbPopulate: TevDBComboBox;
    edStandartTag: TevDBComboBox;
    bOk: TButton;
    edCustomAtomType: TevEdit;
    edAtomTag: TevEdit;
    edAtomCaption: TevEdit;
    bInsertContext: TevButton;
    bDeleteContext: TevButton;
    dOpenIcon: TOpenPictureDialog;
    edStandartAtomType: TevDBComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbContextsClick(Sender: TObject);
    procedure edAtomTagChange(Sender: TObject);
    procedure edAtomCaptionChange(Sender: TObject);
    procedure bInsertContextClick(Sender: TObject);
    procedure bDeleteContextClick(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure edStandartTagChange(Sender: TObject);
    procedure edCustomTagChange(Sender: TObject);
    procedure edPriorityChange(Sender: TObject);
    procedure edStandartIconChange(Sender: TObject);
    procedure bCustomIconClick(Sender: TObject);
    procedure bPopulateClick(Sender: TObject);
    procedure edStandartAtomTypeChange(Sender: TObject);
    procedure edCustomAtomTypeChange(Sender: TObject);
  private
    { Private declarations }
    FSecAtom: TevSecAtom;
    FUpdatingScreen: Boolean;
    procedure AssignIcon(Icon: TIcon; const Resource: string);
    function SelectedContext: TevSecAtomContext;
  public
    { Public declarations }
  end;

  TContextsEditor = class(TComponentEditor)
  public
    procedure Edit; override;
  end;

var
  AtomContextsEditScreen: TAtomContextsEditScreen;

implementation

uses SSecurityInterface;

{$R *.DFM}
{$R ..\Modules\Security\sSecurityResources.res}

{ TContextsProperty }

procedure TContextsEditor.Edit;
var
  c: TevSecAtom;
  d: IDesigner;
begin
  inherited;
  c := Component as TevSecAtom;
  d := Designer;
  with TAtomContextsEditScreen.Create(Application) do
  try
    FSecAtom.Assign(c);
    if ShowModal = mrOk then
    begin
      c.Assign(FSecAtom);
      d.Modified;
    end;
  finally
    Free;
  end;
end;

procedure TAtomContextsEditScreen.FormCreate(Sender: TObject);
  procedure Process(const r: TAtomContextRecord);
  begin
    edStandartIcon.Items.Append(r.Description+ '- '+ r.Caption+ #9+ r.IconName);
  end;
begin
  FSecAtom := TevSecAtom.Create(Self);
  edStandartIcon.Items.Clear;
  Process(secScreenOff);
  Process(secScreenOn);
  Process(secScreenRO);
  Process(secMenuOff);
  Process(secMenuOn);
  Process(secFunctionOff);
  Process(secFunctionOn);
end;

procedure TAtomContextsEditScreen.FormShow(Sender: TObject);
var
  i: Integer;
begin
  FUpdatingScreen := True;
  try
    edStandartAtomType.Value := FSecAtom.AtomType;
    if edStandartAtomType.ItemIndex = -1 then
    begin
      edStandartAtomType.Value := '-';
      edCustomAtomType.Text := FSecAtom.AtomType;
    end
    else
      edCustomAtomType.Text := '';
    edAtomTag.Text := FSecAtom.AtomTag;
    edAtomCaption.Text := FSecAtom.AtomCaption;
    lbContexts.Clear;
    lbContexts.ItemIndex := -1;
    for i := 0 to FSecAtom.Contexts.Count-1 do
      lbContexts.Items.Append(TevSecAtomContext(FSecAtom.Contexts.Items[i]).Caption);
    if lbContexts.Items.Count > 0 then
    begin
      lbContexts.ItemIndex := 0;
      lbContexts.OnClick(nil);
    end;
  finally
    FUpdatingScreen := False;
  end;
end;

procedure TAtomContextsEditScreen.lbContextsClick(Sender: TObject);
var
  c: TevSecAtomContext;
  i: Integer;
begin
  i := lbContexts.ItemIndex;
  if i <> -1 then
  begin
    FUpdatingScreen := True;
    c := FSecAtom.Contexts.Items[i] as TevSecAtomContext;
    try
      edName.Text := c.Caption;
      edPriority.Value := c.Priority;
      edStandartTag.Value := c.Tag;
      if edStandartTag.ItemIndex = -1 then
      begin
        edCustomTag.Text := c.Tag;
        edStandartTag.Value := '-';
      end
      else
        edCustomTag.Text := '';
      edStandartIcon.Value := c.IconName;
      {if edStandartIcon.ItemIndex = -1 then
      begin
        iIcon.Picture.Icon.Assign(c.Icon);
        edStandartIcon.Value := '-';
      end
      else}
        AssignIcon(iIcon.Picture.Icon, c.IconName);
    finally
      FUpdatingScreen := False;
    end;
  end
  else
  begin
    FUpdatingScreen := True;
    try
      edName.Text := '';
      edPriority.Value := 0;
      edStandartTag.Text := '';
      edCustomTag.Text := '';
      edStandartIcon.Text := '';
      iIcon.Picture.Icon.Assign(nil);
    finally
      FUpdatingScreen := False;
    end;
  end;
end;

procedure TAtomContextsEditScreen.edAtomTagChange(Sender: TObject);
begin
  if not FUpdatingScreen then
    FSecAtom.AtomTag := edAtomTag.Text;
end;

procedure TAtomContextsEditScreen.edAtomCaptionChange(Sender: TObject);
begin
  if not FUpdatingScreen then
    FSecAtom.AtomCaption := edAtomCaption.Text;
end;

procedure TAtomContextsEditScreen.bInsertContextClick(Sender: TObject);
begin
  FSecAtom.Contexts.Add;
  lbContexts.Items.Append('-empty-');
  lbContexts.ItemIndex := lbContexts.Items.Count-1;
  lbContexts.OnClick(nil);
end;

procedure TAtomContextsEditScreen.bDeleteContextClick(Sender: TObject);
var
  i: Integer;
begin
  i := lbContexts.ItemIndex;
  if i <> -1 then
  begin
    FSecAtom.Contexts.Delete(i);
    lbContexts.Items.Delete(i);
  end;
  lbContexts.OnClick(nil);
end;

procedure TAtomContextsEditScreen.edNameChange(Sender: TObject);
begin
  if not FUpdatingScreen then
  begin
    SelectedContext.Caption := edName.Text;
    lbContexts.Items[lbContexts.ItemIndex] := edName.Text;
  end;
end;

function TAtomContextsEditScreen.SelectedContext: TevSecAtomContext;
begin
  Result := FSecAtom.Contexts.Items[lbContexts.ItemIndex] as TevSecAtomContext;
end;

procedure TAtomContextsEditScreen.edStandartTagChange(Sender: TObject);
begin
  if not FUpdatingScreen then
  begin
    if edStandartTag.Value = '-' then
      SelectedContext.Tag := edCustomTag.Text
    else
    begin
      SelectedContext.Tag := edStandartTag.Value;
      FUpdatingScreen := True;
      try
        edCustomTag.Text := '';
      finally
        FUpdatingScreen := False;
      end;
    end;
  end;
end;

procedure TAtomContextsEditScreen.edCustomTagChange(Sender: TObject);
begin
  edStandartTag.Value := '-';
  edStandartTag.OnChange(nil);
end;

procedure TAtomContextsEditScreen.edPriorityChange(Sender: TObject);
begin
  if not FUpdatingScreen then
    SelectedContext.Priority := edPriority.Value;
end;

procedure TAtomContextsEditScreen.edStandartIconChange(Sender: TObject);
begin
  if not FUpdatingScreen then
  begin
    if edStandartIcon.Value = '-' then
      with SelectedContext do
      begin
        IconName := '';
        Icon.Assign(iIcon.Picture.Icon);
      end
    else
      with SelectedContext do
      begin
        IconName := edStandartIcon.Value;
        AssignIcon(iIcon.Picture.Icon, edStandartIcon.Value);
      end;
  end;
end;

procedure TAtomContextsEditScreen.bCustomIconClick(Sender: TObject);
begin
  if not FUpdatingScreen then
    if dOpenIcon.Execute then
    begin
      iIcon.Picture.Icon.LoadFromFile(dOpenIcon.FileName);
      edStandartIcon.Value := '-';
      edStandartIcon.OnChange(nil);
    end;
end;

procedure TAtomContextsEditScreen.AssignIcon(Icon: TIcon;
  const Resource: string);
begin
  if Resource = '' then
    Icon.Assign(nil)
  else
    Icon.Handle := Windows.LoadIcon(hInstance, PChar(Resource));
end;

procedure TAtomContextsEditScreen.bPopulateClick(Sender: TObject);
  procedure Process(const c: TevSecAtomContext; const r: TAtomContextRecord);
  begin
    c.Tag := r.Tag;
    c.Caption := r.Caption;
    c.Priority := r.Priority;
    c.IconName := r.SelectedIconName;
  end;
var
  c: Char;
begin
  if cbPopulate.Value <> '' then
  begin
    c := cbPopulate.Value[1];
    case c of
      'S':
        with FSecAtom do
        begin
          AtomType := 'S';
          Contexts.Clear;
          Process(Contexts.Add as TevSecAtomContext, secScreenOff);
          Process(Contexts.Add as TevSecAtomContext, secScreenRO);
          Process(Contexts.Add as TevSecAtomContext, secScreenOn);
        end;
      'M':
        with FSecAtom do
        begin
          AtomType := c;
          Contexts.Clear;
          Process(Contexts.Add as TevSecAtomContext, secMenuOff);
          Process(Contexts.Add as TevSecAtomContext, secMenuOn);
        end;
      'F':
        with FSecAtom do
        begin
          AtomType := c;
          Contexts.Clear;
          Process(Contexts.Add as TevSecAtomContext, secFunctionOff);
          Process(Contexts.Add as TevSecAtomContext, secFunctionOn);
        end;
    end;
    FormShow(nil);
  end;
end;

procedure TAtomContextsEditScreen.edStandartAtomTypeChange(Sender: TObject);
begin
  if not FUpdatingScreen then
  begin
    if edStandartAtomType.Value = '-' then
      FSecAtom.AtomType := edCustomAtomType.Text
    else
    begin
      FSecAtom.AtomType := edStandartAtomType.Value;
      FUpdatingScreen := True;
      try
        edCustomAtomType.Text := '';
      finally
        FUpdatingScreen := False;
      end;
    end;
  end;
end;

procedure TAtomContextsEditScreen.edCustomAtomTypeChange(Sender: TObject);
begin
  edStandartAtomType.Value := '-';
  edStandartAtomType.OnChange(nil);
end;

end.

