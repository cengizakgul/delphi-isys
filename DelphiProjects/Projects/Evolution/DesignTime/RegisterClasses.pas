// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit RegisterClasses;

interface

uses
  Classes, Windows, SysUtils, DesignEditors, wwprpedt, db,  ActnList, EvSecElement, SDataStructure,
  TypInfo, Menus, Forms, AtomContextPropertyEditor, DesignIntf, EvStreamUtils, isBaseClasses, EvUIComponents,
  EvUtils, VclEditors, DbReg, EvBasicUtils, SDDClasses, Dialogs, EvReportWriterProxy, SDataAccessMod, EvClientDataSet;

type
  TevDataSetProperty = class(TComponentProperty)
  public
    function GetValue: string; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  TSBShortCutProperty = class(TShortCutProperty)
  public
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TevDBLookupDisplayProperty = class(TwwDBLookupDisplayProperty)
  public
    procedure Edit; override;
  end;

  TevDBLookupDisplayComponentEditor = class(TwwDBLookupDisplayComponentEditor)
  public
    procedure Edit; override;
  end;

  TevSelectedFieldsProperty = class(TSelectedFieldsProperty)
  public
    procedure Edit; override;
  end;

  TevDBGridComponentEditor = class(TwwDBGridComponentEditor)
  public
    procedure Edit; override;
  end;


  TevQueryBuilderInfoProperty = class(TClassProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;


procedure Register;

implementation

uses
  SDM_CL,
  SDM_SY_FED_REPORTING_AGENCY,
  SDM_SY_FED_TAX_PAYMENT_AGENCY,
  SDM_SY_COUNTY,
  SDM_SY_LOCALS,
  SDM_SY_LOCAL_TAX_CHART,
  SDM_SY_LOCAL_MARITAL_STATUS,
  SDM_SY_STATE_TAX_CHART,
  SDM_SY_GL_AGENCY_REPORT,
  SDM_SY_HR_REFUSAL_REASON,
  SDM_CO_PENSIONS,
  SDM_SB_DELIVERY_COMPANY_SVCS,
  SDM_SB_REPORTS,
  SDM_SB_BANK_ACCOUNTS,
  SDM_SB_AGENCY_REPORTS,
  SDM_SB_GLOBAL_AGENCY_CONTACTS,
  SDM_SY_FED_TAX_TABLE_BRACKETS,
  SDM_SY_FED_TAX_TABLE,
  SDM_SY_GL_AGENCY_FIELD_OFFICE,
  SDM_SY_RECIPROCATED_STATES,
  SDM_SY_STATE_MARITAL_STATUS,
  SDM_CO,
  SDM_CL_BANK_ACCOUNT,
  SDM_CL_AGENCY,
  SDM_CL_E_D_LOCAL_EXMPT_EXCLD,
  SDM_CL_E_DS,
  SDM_CL_E_D_STATE_EXMPT_EXCLD,
  SDM_CL_E_D_GROUP_CODES,
  SDM_CO_E_D_CODES,
  SDM_EE,
  SDM_EE_SCHEDULED_E_DS,
  SDM_CO_UNIONS,
  SDM_CO_WORKERS_COMP,
  SDM_CO_DIVISION,
  SDM_CO_BRANCH,
  SDM_CO_DEPARTMENT,
  SDM_CO_TEAM,
  SDM_EE_LOCALS,
  SDM_CO_LOCAL_TAX,
  SDM_EE_PENSION_FUND_SPLITS,
  SDM_EE_STATES,
  SDM_EE_ADDITIONAL_INFO,
  SDM_CO_BILLING_HISTORY,
  SDM_CO_SALESPERSON,
  SDM_CO_BATCH_LOCAL_OR_TEMPS,
  SDM_CO_BATCH_STATES_OR_TEMPS,
  SDM_CO_PR_BATCH_DEFLT_ED,
  SDM_CO_DIV_PR_BATCH_DEFLT_ED,
  SDM_CO_BRCH_PR_BATCH_DEFLT_ED,
  SDM_CO_DEPT_PR_BATCH_DEFLT_ED,
  SDM_CO_TEAM_PR_BATCH_DEFLT_ED,
  SDM_CO_SERVICES,
  SDM_CO_GENERAL_LEDGER,
  SDM_EE_AUTOLABOR_DISTRIBUTION,
  SDM_PR_SCHEDULED_EVENT,
  SDM_CO_BILLING_HISTORY_DETAIL,
  SDM_CO_FED_TAX_LIABILITIES,
  SDM_CO_MANUAL_ACH,
  SDM_CO_PR_ACH,
  SDM_CO_SHIFTS,
  SDM_CO_STATE_TAX_LIABILITIES,
  SDM_CO_SUI_LIABILITIES,
  SDM_CO_SUI,
  SDM_CO_TAX_DEPOSITS,
  SDM_CO_LOCAL_TAX_LIABILITIES,
  SDM_CO_PHONE,  
  SDM_PR,
  SDM_PR_BATCH,
  SDM_PR_CHECK,
  SDM_PR_CHECK_LINES,
  SDM_PR_CHECK_STATES,
  SDM_PR_CHECK_SUI,
  SDM_PR_CHECK_LOCALS,
  SDM_PR_CHECK_LINE_LOCALS,
  SDM_PR_MISCELLANEOUS_CHECKS,
  SDM_PR_SCHEDULED_E_DS,
  SDM_PR_SERVICES,
  SDataAccessCommon,
  SDM_TMP_PR,
  SDM_SB_BANKS,
  SDM_SB_SALES_TAX_STATES,
  SDM_SB_SERVICES_CALCULATIONS,
  SDM_TMP_CO_MANUAL_ACH,
  SDM_EE_RATES,
  SDM_SB_SEC_GROUPS,
  SDM_CO_TIME_OFF_ACCRUAL_RATES,
  SDM_EE_TIME_OFF_ACCRUAL,
  SDM_CO_PR_FILTERS,
  SDM_TMP_CO,
  SDM_CO_STATES,
  SDM_CO_BENEFIT_STATES,
  SDM_CO_BENEFIT_CATEGORY,
  SDM_CO_BENEFIT_PKG_ASMNT,  
  SDM_CO_BENEFIT_PKG_DETAIL,
  SDM_CO_BENEFIT_RATES,
  SDM_CO_BENEFITS,  
  SDM_CO_DIVISION_LOCALS,
  SDM_CO_BRANCH_LOCALS,
  SDM_CO_DEPARTMENT_LOCALS,
  SDM_CO_TEAM_LOCALS,
  SDM_EE_HR_PROPERTY_TRACKING,
  SDM_EE_DIRECT_DEPOSIT,
  SDM_CL_HR_PERSON_SKILLS,
  SDM_EE_HR_ATTENDANCE,
  SDM_EE_HR_INJURY_OCCURENCE,
  SDM_CL_HR_PERSON_EDUCATION,
  SDM_CO_HR_APPLICANT,
  SDM_CO_HR_POSITION_GRADES,
  SDM_EE_HR_CO_PROVIDED_EDUCATN,
  SDM_CL_HR_PERSON_HANDICAPS,
  SDM_EE_BENEFITS,
  SDM_EE_BENEFICIARY,
  SDM_SB_USER,
  SDM_SB_SERVICES,  
  SDM_EE_WORK_SHIFTS,
  SDM_EE_HR_CAR,
  SDM_CO_REPORTS,
  SDM_CO_JOBS_LOCALS,
  SDM_CO_TIME_OFF_ACCRUAL,
  SDM_CO_JOBS,
  SDM_EE_PIECE_WORK,
  SDM_CL_PIECES,
  SDM_SY_REPORT_WRITER_REPORTS,
  SDM_SB_REPORT_WRITER_REPORTS,
  SDM_CL_REPORT_WRITER_REPORTS,
  SDM_CO_BANK_ACCOUNT_REGISTER,
  SDM_CO_PR_CHECK_TEMPLATE_E_DS,
  SDM_SB_TASK,
  SDM_SB_QUEUE_PRIORITY,
  SDM_SB_TEAM_MEMBERS,
  SDM_PR_REPORTS,
  SDM_CO_TAX_RETURN_QUEUE,
  SDM_CL_PERSON,
  SDM_EE_TIME_OFF_ACCRUAL_OPER,
  SDM_SB_DELIVERY_SERVICE,
  SDM_SB_DELIVERY_METHOD,
  SDM_SB_MEDIA_TYPE,
  SDM_SB_VENDOR,
  SDM_SB_VENDOR_DETAIL,
  SDM_SB_ENABLED_DASHBOARDS,  
  SDM_CO_VENDOR,
  SDM_CO_VENDOR_DETAIL_VALUES,
  SDM_CO_DASHBOARDS,
  SDM_CO_DASHBOARDS_USER,
  SDM_CL_MAIL_BOX_GROUP,
  SDM_SY_DELIVERY_METHOD,
  SDM_SY_REPORTS,
  SDM_EE_HR_PERFORMANCE_RATINGS,
  SDM_SY_FED_EXEMPTIONS,
  SDM_SY_STATE_EXEMPTIONS,
  SDM_SY_LOCAL_EXEMPTIONS,
  SDM_SY_REPORT_GROUP_MEMBERS,
  SDM_CL_PERSON_DOCUMENTS,
  SDM_CO_GROUP;

procedure Register;
begin
  RegisterPropertyEditor(TDataSet.ClassInfo, TevDataSource, 'DataSet', TevDataSetProperty);
  RegisterPropertyEditor(TDataSet.ClassInfo, TevProxyDataSet, 'DataSet', TevDataSetProperty); //SEG
  RegisterPropertyEditor(TDataSet.ClassInfo, TField, 'LookupDataSet', TevDataSetProperty);
  RegisterPropertyEditor(TDataSet.ClassInfo, TevClientDataSet, 'ParentDataSet', TevDataSetProperty);
  RegisterPropertyEditor(TDataSet.ClassInfo, TevDBLookupCombo, 'LookupTable', TevDataSetProperty);
  RegisterPropertyEditor(TypeInfo(TStrings), TevDBLookupCombo, 'Selected', TevDBLookupDisplayProperty);
  RegisterPropertyEditor(TypeInfo(TShortCut), TevSpeedButton, 'ShortCut', TSBShortCutProperty);
  RegisterPropertyEditor(TypeInfo(TShortCut), TevToolButton, 'ShortCut', TSBShortCutProperty);
  RegisterPropertyEditor(TypeInfo(TShortCut), TAction, 'ShortCut', TSBShortCutProperty);
  RegisterPropertyEditor(TypeInfo(TStrings), TevDBGrid, 'Selected', TevSelectedFieldsProperty);
  RegisterComponentEditor(TevDBGrid, TevDBGridComponentEditor);
  RegisterComponentEditor(TevDBLookupCombo, TevDBLookupDisplayComponentEditor);
  RegisterComponentEditor(TevSecAtom, TContextsEditor);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_SERVICE_BUREAU, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_SYSTEM_LOCAL, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_SYSTEM_STATE, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_SYSTEM_FEDERAL, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_SYSTEM_HR, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_TEMPORARY, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_COMPANY, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_CLIENT, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_SYSTEM_MISC, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_EMPLOYEE, '', nil);
  RegisterPropertyEditor(TDataSet.ClassInfo, TDM_PAYROLL, '', nil);

  RegisterPropertyEditor(TField.ClassInfo, TddTable, '', nil);

  RegisterPropertyEditor(TStringList.ClassInfo, TevSecAtom, 'SecurityContexts', nil);
  RegisterPropertyEditor(TypeInfo(string), TevDbTreeView, 'KeyField', TDataFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TevDbTreeView, 'CaptionField', TDataFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TevDbTreeView, 'UplinkField', TDataFieldProperty);

  RegisterPropertyEditor(TEvQueryBuilderInfo.ClassInfo, TEvQueryBuilderQuery, 'QueryBuilderInfo', TevQueryBuilderInfoProperty);
end;

{ TevDataSetProperty }

function TevDataSetProperty.GetValue: string;
var
  i: Integer;
begin
  Result := inherited GetValue;
  if (Pos('.', Result) > 0) and
     (Copy(Result, 1, Pred(Pos('.', Result))) = 'DM_' + Copy(Result, Succ(Pos('.', Result)), Length(Result))) then
    for i := 0 to Pred(Designer.GetRoot.ComponentCount) do
      if (Designer.GetRoot.Components[i] is TStrEntry) and
         IsPublishedProp(Designer.GetRoot.Components[i], Copy(Result, Succ(Pos('.', Result)), Length(Result))) then
      begin
        Result := Designer.GetRoot.Components[i].Name + Copy(Result, Pos('.', Result), Length(Result));
        Break;
      end;
end;

procedure TevDataSetProperty.GetValues(Proc: TGetStrProc);
var
  i, j: Integer;
  TypeData: PTypeData;
  PropList: PPropList;
  Count: Integer;
begin
  inherited;
  for i := 0 to Pred(Designer.GetRoot.ComponentCount) do
    if Designer.GetRoot.Components[i] is TStrEntry then
      with TStrEntry(Designer.GetRoot.Components[i]) do
      begin
        TypeData := GetTypeData(ClassInfo);
        if TypeData.PropCount <> 0 then
        begin
          GetMem(PropList, SizeOf(PPropInfo) * TypeData.PropCount);
          try
            Count := GetPropList(ClassInfo, [tkClass], PropList);
            for j := 0 to Pred(Count) do
              Proc(Name + '.' + PropList[j]^.Name);
          finally
            FreeMem(PropList);
          end;
        end;
      end;
end;

procedure TevDataSetProperty.SetValue(const Value: string);
var
  DM: TComponent;
  DS: TevClientDataSet;
begin
  DS := nil;
  if (Pos('.', Value) > 0) then
  begin
    DM := Designer.GetRoot.FindComponent(Copy(Value, 1, Pred(Pos('.', Value))));
    if Assigned(DM) and
       (DM is TstrEntry) then
    begin
      TstrEntry(DM).CustomAccess := True;
      try
        DS := GetObjectProp(DM, Copy(Value, Succ(Pos('.', Value)), Length(Value))) as TevClientDataSet;
      finally
        TstrEntry(DM).CustomAccess := False;
      end;
      SetOrdValue(Longint(DS));
    end;
  end;
  if not Assigned(DS) then
    inherited;
end;

{ TSBShortCutProperty }

procedure TSBShortCutProperty.GetValues(Proc: TGetStrProc);
begin
  inherited;
  Proc(ShortCutToText(VK_LEFT or scCtrl));
  Proc(ShortCutToText(VK_RIGHT or scCtrl));
  Proc(ShortCutToText(VK_UP or scCtrl));
  Proc(ShortCutToText(VK_DOWN or scCtrl));
  Proc(ShortCutToText(VK_HOME or scCtrl));
  Proc(ShortCutToText(VK_END or scCtrl));
  Proc(ShortCutToText(VK_LEFT or scAlt));
  Proc(ShortCutToText(VK_RIGHT or scAlt));
  Proc(ShortCutToText(VK_UP or scAlt));
  Proc(ShortCutToText(VK_DOWN or scAlt));
  Proc(ShortCutToText(VK_HOME or scAlt));
  Proc(ShortCutToText(VK_END or scAlt));
  Proc(ShortCutToText(VK_INSERT or scAlt));
  Proc(ShortCutToText(VK_DELETE or scAlt));
  Proc(ShortCutToText(VK_LEFT or scCtrl or scAlt));
  Proc(ShortCutToText(VK_RIGHT or scCtrl or scAlt));
  Proc(ShortCutToText(VK_UP or scCtrl or scAlt));
  Proc(ShortCutToText(VK_DOWN or scCtrl or scAlt));
  Proc(ShortCutToText(VK_HOME or scCtrl or scAlt));
  Proc(ShortCutToText(VK_END or scCtrl or scAlt));
  Proc(ShortCutToText(VK_LEFT or scShift or scAlt));
  Proc(ShortCutToText(VK_RIGHT or scShift or scAlt));
  Proc(ShortCutToText(VK_UP or scShift or scAlt));
  Proc(ShortCutToText(VK_DOWN or scShift or scAlt));
  Proc(ShortCutToText(VK_HOME or scShift or scAlt));
  Proc(ShortCutToText(VK_END or scShift or scAlt));
  Proc(ShortCutToText(VK_ADD or scCtrl));
  Proc(ShortCutToText(VK_MULTIPLY or scCtrl));
end;

{ TevDBLookupDisplayProperty }

procedure TevDBLookupDisplayProperty.Edit;
begin
  with GetComponent(0) as TevDBLookupCombo do
    if Assigned(LookupTable) then
      LookupTable.Open;
  inherited;
end;

{ TevSelectedFieldsProperty }

procedure TevSelectedFieldsProperty.Edit;
begin
  with GetComponent(0) as TevDBGrid do
    if Assigned(DataSource) and Assigned(DataSource.DataSet) then
      DataSource.DataSet.Open;
  inherited;
end;

{ TevDBGridComponentEditor }

procedure TevDBGridComponentEditor.Edit;
begin
  with Component as TevDBGrid do
    if Assigned(DataSource) and Assigned(DataSource.DataSet) then
      DataSource.DataSet.Open;
  inherited;
end;

{ TevDBLookupDisplayComponentEditor }

procedure TevDBLookupDisplayComponentEditor.Edit;
begin
  with Component as TevDBLookupCombo do
    if Assigned(LookupTable) then
      LookupTable.Open;
  inherited;
end;

{ TevQueryBuilderInfoProperty }

procedure TevQueryBuilderInfoProperty.Edit;
var
  Prop: TEvQueryBuilderInfo;
begin
  Prop := TEvQueryBuilderInfo(GetOrdValue);
  GetRWProxy.ShowQueryBuilder(Prop.Data, 0, 0, False);
end;

function TevQueryBuilderInfoProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paDialog, paReadOnly];
end;

var i: Integer;

initialization

finalization
  while i < Screen.DataModuleCount do
    if Copy(Screen.DataModules[i].Name, 1, 3) = 'DM_' then
      Screen.DataModules[i].Free
    else
      Inc(i);

end.
