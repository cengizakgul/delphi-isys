object AtomContextsEditScreen: TAtomContextsEditScreen
  Left = 488
  Top = 275
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Security Atom - Quick setup screen'
  ClientHeight = 493
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 384
    Top = 344
    Width = 50
    Height = 50
  end
  object TEvBevel
    Left = 88
    Top = 160
    Width = 65
    Height = 10
    Shape = bsBottomLine
  end
  object TEvBevel
    Left = 88
    Top = 8
    Width = 345
    Height = 10
    Shape = bsBottomLine
  end
  object TevLabel
    Left = 8
    Top = 8
    Width = 72
    Height = 13
    Caption = 'Quick populate'
  end
  object TevLabel
    Left = 8
    Top = 160
    Width = 71
    Height = 13
    Caption = 'List of contexts'
  end
  object TevLabel
    Left = 168
    Top = 160
    Width = 113
    Height = 13
    Caption = 'Selected context details'
  end
  object TEvBevel
    Left = 288
    Top = 160
    Width = 145
    Height = 10
    Shape = bsBottomLine
  end
  object TevLabel
    Left = 168
    Top = 184
    Width = 36
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Caption'
    ParentBiDiMode = False
  end
  object TevLabel
    Left = 168
    Top = 232
    Width = 19
    Height = 13
    Caption = 'Tag'
  end
  object lCustomTag: TevLabel
    Left = 304
    Top = 232
    Width = 53
    Height = 13
    Caption = 'Custom tag'
  end
  object TevLabel
    Left = 168
    Top = 280
    Width = 31
    Height = 13
    Caption = 'Priority'
  end
  object TevLabel
    Left = 168
    Top = 328
    Width = 21
    Height = 13
    Caption = 'Icon'
  end
  object TevLabel
    Left = 8
    Top = 56
    Width = 57
    Height = 13
    Caption = 'Atom details'
  end
  object TEvBevel
    Left = 72
    Top = 56
    Width = 361
    Height = 10
    Shape = bsBottomLine
  end
  object TevLabel
    Left = 120
    Top = 72
    Width = 58
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Custom type'
    ParentBiDiMode = False
  end
  object TevLabel
    Left = 8
    Top = 112
    Width = 19
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Tag'
    ParentBiDiMode = False
  end
  object TevLabel
    Left = 120
    Top = 112
    Width = 36
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Caption'
    ParentBiDiMode = False
  end
  object iIcon: TImage
    Left = 384
    Top = 344
    Width = 49
    Height = 49
    Center = True
    Transparent = True
  end
  object TEvBevel
    Left = 8
    Top = 448
    Width = 425
    Height = 10
    Shape = bsBottomLine
  end
  object TevLabel
    Left = 8
    Top = 72
    Width = 24
    Height = 13
    BiDiMode = bdLeftToRight
    Caption = 'Type'
    ParentBiDiMode = False
  end
  object bPopulate: TevButton
    Left = 160
    Top = 24
    Width = 71
    Height = 25
    Caption = 'Populate'
    TabOrder = 1
    OnClick = bPopulateClick
  end
  object lbContexts: TListBox
    Left = 8
    Top = 176
    Width = 145
    Height = 241
    ItemHeight = 13
    TabOrder = 6
    OnClick = lbContextsClick
  end
  object edName: TevEdit
    Left = 168
    Top = 200
    Width = 257
    Height = 21
    TabOrder = 9
    OnChange = edNameChange
  end
  object edCustomTag: TevEdit
    Left = 304
    Top = 248
    Width = 121
    Height = 21
    TabOrder = 11
    OnChange = edCustomTagChange
  end
  object edPriority: TevSpinEdit
    Left = 168
    Top = 296
    Width = 49
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 12
    Value = 0
    OnChange = edPriorityChange
  end
  object bCustomIcon: TButton
    Left = 304
    Top = 344
    Width = 71
    Height = 25
    Caption = 'Load'
    TabOrder = 14
    OnClick = bCustomIconClick
  end
  object edStandartIcon: TevDBComboBox
    Left = 168
    Top = 344
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Screen On'#9'SCREEN_ON'
      'Screen Read-only'#9'SCREEN_RO'
      'Screen Off'#9'SCREEN_OFF'
      'Menu On'#9'MENU'
      'Menu Off'#9'MENU_OFF'
      '-Custom-'#9'-')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 13
    UnboundDataType = wwDefault
    OnChange = edStandartIconChange
  end
  object cbPopulate: TevDBComboBox
    Left = 8
    Top = 24
    Width = 145
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Screen'#9'S'
      'Menu'#9'M'
      'Function'#9'F')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 0
    UnboundDataType = wwDefault
  end
  object edStandartTag: TevDBComboBox
    Left = 168
    Top = 248
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Disabled'#9'N'
      'Read only'#9'R'
      'Enabled'#9'Y'
      '-Custom-'#9'-')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 10
    UnboundDataType = wwDefault
    OnChange = edStandartTagChange
  end
  object bOk: TButton
    Left = 362
    Top = 464
    Width = 71
    Height = 25
    Caption = 'Ok'
    ModalResult = 1
    TabOrder = 15
  end
  object edCustomAtomType: TevEdit
    Left = 120
    Top = 88
    Width = 89
    Height = 21
    TabOrder = 3
    OnChange = edCustomAtomTypeChange
  end
  object edAtomTag: TevEdit
    Left = 8
    Top = 128
    Width = 97
    Height = 21
    TabOrder = 4
    OnChange = edAtomTagChange
  end
  object edAtomCaption: TevEdit
    Left = 120
    Top = 128
    Width = 193
    Height = 21
    TabOrder = 5
    OnChange = edAtomCaptionChange
  end
  object bInsertContext: TevButton
    Left = 8
    Top = 424
    Width = 71
    Height = 25
    Caption = 'Add'
    TabOrder = 7
    OnClick = bInsertContextClick
  end
  object bDeleteContext: TevButton
    Left = 82
    Top = 424
    Width = 71
    Height = 25
    Caption = 'Delete'
    TabOrder = 8
    OnClick = bDeleteContextClick
  end
  object edStandartAtomType: TevDBComboBox
    Left = 8
    Top = 88
    Width = 97
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Screen'#9'S'
      'Menu'#9'M'
      'Function'#9'F'
      '-Custom-'#9'-')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 2
    UnboundDataType = wwDefault
    OnChange = edStandartAtomTypeChange
  end
  object dOpenIcon: TOpenPictureDialog
    Filter = 'Icons (*.ico)|*.ico'
    Options = [ofReadOnly, ofHideReadOnly, ofEnableSizing]
    Left = 384
    Top = 408
  end
end
