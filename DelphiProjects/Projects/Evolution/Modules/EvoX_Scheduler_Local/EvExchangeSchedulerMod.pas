unit EvExchangeSchedulerMod;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvConsts, EvStreamUtils, EvTypes, isLogFile, EvExchangeConsts,
     isBasicUtils,  evContext, StrUtils, EvExchangeResultLog;

function TaskStatusToString(const AStatus: TEvoXTaskStatus): String;
function StringToTaskStatus(const AStatus: String): TEvoXTaskStatus;

implementation

uses isThreadManager, EvUtils, ISErrorUtils, EVClasses, ActiveX,
  Variants, isVCLBugFix, DateUtils, EvExchangeMapFileExt;

const
  RootSchedulerFolder = 'EvoXScheduler\';
  SpoiledTasksFile = 'SpoiledTasks.dat';
  ErrorCodeStartPos = 1;
  ErrorCodeLength = 2;
  TaskFileExt = '.tsk';
  SchedulerSnoozeInterval = 30; // seconds
  BackCopyExt = '.BAK';
  BackCopyPattern = '*' + BackCopyExt;
  EvoXSchedEventClassName = 'EvoX Scheduler';
  InProcessFolder = 'InProcess\';
  CompletedFolder = 'Completed\';
  ErrorsFolder = 'Errors\';

type
  IEvExchangeScheduledTaskStatus = interface(IisInterfacedObject)
  ['{4404C2FD-CEDE-4FF9-B306-107AB1D96ED3}']
    function  GetTaskQueueIds: IisListOfValues;
    procedure DeleteTaskStaff;
    procedure SuspendTask(const AReason: String);
    procedure ResumeTask;
    procedure MakeTaskIncorrect;
    procedure CreateTaskFolder;
    procedure EnableLogging;
    function  AddEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String): ILogEvent;
    function  SaveTaskToStorage(const ALogErrors: boolean = true): boolean;
    procedure ProcessTask;
  end;

  TEvoXDestinationTypes = (dtCompleted, dtErrors);

  TEvExchangeScheduledTask = class(TisInterfacedObject, IEvExchangeScheduledTask, IEvExchangeScheduledTaskStatus)
  private
    FGUID: TisGUID;
    FStatus: TEvoXTaskStatus;
    FParams: IisListOfValues;
    FLogFile: IevLogFile;
    FLastRunTime: TDateTime;
    FLastQueueStatusUpdated: TDateTime;

    FImportTaskID: TTaskId;
    FCheckStatusTaskId: TTaskId;

    // task status fields
    FTaskQueueIds: IisListOfValues; // Name = TaskQueueId, value = ListOfValues: name = original filename, value = filename + timestamp in progress folder

    procedure StartImport(const Params: TTaskParamList);
    procedure UpdateTaskQueueStatus(const Params: TTaskParamList);

    procedure ProcessNewTaskParams;
    procedure InitTaskParams;
    procedure SendEMailNotification(const AEventType: TEvoXTaskEventType; const AMessage: String; const AMessageDetails: String);
    function  LoadMapFile(const AMapFilePath: String): IEvExchangeMapFile;
    function  AddTimestampToFileName(const AFileName: String; const ATimestamp: TDateTime): String;
    function  GetTaskName: String;
    function  MoveFileWithRenaming(const ASourcePathAndFileName: String; ADestType: TEvoXDestinationTypes): boolean;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    // IEvExchangeScheduledTask
    function  GetTaskStatus: TEvoXTaskStatus;
    function  HasJobsInTaskQueue: boolean;
    procedure ActivateTask;
    procedure DeactivateTask;
    function  GetTaskParams: IisListOfValues;
    procedure SetTaskParams(const ATaskParams: IisListOfValues);
    function  GetTaskGUID: TisGUID;
    function  GetLogFileName: String;

    // IEvExchangeScheduledTaskStatus
    function  GetTaskQueueIds: IisListOfValues;
    procedure DeleteTaskStaff;
    procedure SuspendTask(const AReason: String);
    procedure ResumeTask;
    procedure MakeTaskIncorrect;
    procedure EnableLogging;
    procedure CreateTaskFolder;
    function  AddEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String): ILogEvent;
    function  SaveTaskToStorage(const ALogErrors: boolean = true): boolean;
    procedure ProcessTask;
  public
    class function GetTypeID: String; override;

    destructor Destroy; override;
  end;

  TEvExchangeSchedulerMod = class(TisInterfacedObject, IEvExchangeScheduler)
  private
    FActive: Boolean;
    FThreadId: TTaskID;
    FTaskList: IisListOfValues;
    FTasksRootStorageFolder: String;
    FSpoiledTasksList: IisListOfValues;  // Name = TaskGUID, Value = XX - Description  (where XX is error's code,
                                         // XX is being deleted when list goes out)

    procedure Run(const Params: TTaskParamList);

    function DeleteStorageForTask(const ATask: TisGUID; const AOneAttemptOnly: boolean = false;
      const ALogErrors: boolean = true): boolean;

    procedure StartUp;
    procedure CleanUP;

    procedure LoadSpoiledTasksList;
    procedure SaveSpoiledTasksList;
    procedure LoadTasksFromStorage;
    procedure ReadOptions;
    function  ErrorCodeToString(const AErrorCode: TEvoXTaskErrors): String;
    function  GetErrorCodeFromString(const AString: String): TEvoXTaskErrors;
  protected
    procedure DoOnConstruction; override;

    // IEvExchangeScheduler
    function  GetTaskGUIDsList: IisStringList;
    function  GetTask(const ATaskGUID: TisGUID): IEvExchangeScheduledTask;
    function  AddNewTask: IEvExchangeScheduledTask;
    procedure DeleteTask(const ATaskGUID: TisGUID);
    function  GetListOfSpoiledTasks: IisListOfValues;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
    procedure AddTaskToSpoiledList(const ATaskID: TisGUID; const AErrorCode: TEvoXTaskErrors; const ADescription: String);
    procedure CheckForTheSameFolder(const APath: String);
    function  GetTasksRootStorageFolder: String;
    procedure LoadTasksWithoutActivation;
  public
    destructor Destroy; override;
  end;

  function GetEvoXScheduler: IEvExchangeScheduler;
  begin
    Result := TEvExchangeSchedulerMod.Create;
  end;

  function GetStoragePathForTask(const ATask: TisGUID): String;
  begin
    Result := NormalizePath(Mainboard.EvoXScheduler.GetTasksRootStorageFolder + Context.UserAccount.User + '\' + ATask);
  end;

  function TaskStatusToString(const AStatus: TEvoXTaskStatus): String;
  begin
    case AStatus of
      tskEvoXInactive:        Result := EvoXTaskStatusInactive;
      tskEvoXActive:          Result := EvoXTaskStatusActive;
      tskEvoXActiveSuspended: Result := EvoXTaskStatusSuspended;
      tskEvoXIncorrect:       Result := EvoXTaskStatusIncorrect;
    else
      raise Exception.Create('Unknown task status');
    end;
  end;

  function StringToTaskStatus(const AStatus: String): TEvoXTaskStatus;
  begin
    if AStatus = EvoXTaskStatusInactive then
      Result := tskEvoXInactive
    else if AStatus = EvoXTaskStatusActive then
      Result := tskEvoXActive
    else if AStatus = EvoXTaskStatusSuspended then
      Result := tskEvoXActiveSuspended
    else if AStatus = EvoXTaskStatusIncorrect then
      Result := tskEvoXIncorrect
    else
      raise Exception.Create('Unknown task status: "' + AStatus + '"');
  end;

  function TryToDeleteFile(const AFileName: String; const AAttempts, ATimeout: integer): boolean;
  var
    i: integer;
  begin
    Result := false;

    if not FileExists(AFileName) then
    begin
      Result := true;
      exit;
    end;

    for i := 1 to AAttempts do
    begin
      Result := DeleteFile(AFileName);
      if Result then
        break;
      snooze(ATimeout);
    end; // for i
  end;

{ TEvExchangeSchedulerMod }

function TEvExchangeSchedulerMod.AddNewTask: IEvExchangeScheduledTask;
var
  ExtTask: IEvExchangeScheduledTaskStatus;
  bSaved: boolean;
begin
  Result := TEvExchangeScheduledTask.Create;
  ExtTask := Result as IEvExchangeScheduledTaskStatus;
  ExtTask.CreateTaskFolder;
  ExtTask.EnableLogging;
  bSaved := ExtTask.SaveTaskToStorage;
  if not bSaved then
    exit;

  Lock;
  try
    FTaskList.AddValue(Result.GetTaskGUID, Result);
  finally
    Unlock;
  end;
end;

procedure TEvExchangeSchedulerMod.AddTaskToSpoiledList(const ATaskId: TisGUID; const AErrorCode: TEvoXTaskErrors; const ADescription: String);
begin
  FSpoiledTaskslist.AddValue(ATaskId, ErrorCodeToString(AErrorCode) + ' - ' + ADescription);
  SaveSpoiledTasksList;
end;

procedure TEvExchangeSchedulerMod.CleanUP;
var
  i: integer;
  sTaskId: TisGUID;
  Task: IEvExchangeScheduledTaskStatus;
  ErrorCode: TEvoXTaskErrors;
  S: String;
  bUpdated, bSaved: boolean;
begin
  // trying to store status of tasks from spoiled list
  bUpdated := false;
  for i := FSpoiledTasksList.Count - 1 downto 0 do
  begin
    S := FSpoiledTasksList.Values[i].Value;
    ErrorCode := GetErrorCodeFromString(S);
    if ErrorCode = CannotSaveTaskStatusToFile then
    begin
      sTaskId := FSpoiledTasksList.Values[i].Name;
      Task := IInterface(FTaskList.Value[sTaskId]) as IEvExchangeScheduledTaskStatus;
      if (Task as IEvExchangeScheduledTask).GetTaskStatus = tskEvoXActiveSuspended then
      begin
        Task.ResumeTask;
        bSaved := Task.SaveTaskToStorage(false);
        if bSaved then
        begin
          FSpoiledTasksList.DeleteValue(sTaskId);
          bUpdated := true;
        end;
      end;
    end;
  end;
  if bUpdated then
    SaveSpoiledTasksList;

  // clearing list of tasks
  FTaskList.Clear;  
end;

function TEvExchangeSchedulerMod.DeleteStorageForTask(const ATask: TisGUID; const AOneAttemptOnly: boolean = false;
  const ALogErrors: boolean = true): boolean;
var
  FolderPath: String;
begin
  FolderPath := GetStoragePathForTask(ATask);
  Result := ClearDir(FolderPath, false);
  if Result then
    RemoveDir(FolderPath);

  if (not Result) and (ALogErrors) then
  begin
    mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot delete task''s folder. TaskId: "' +
      ATask + '"', 'Cannot delete task''s folder. Folder name: "' + FolderPath + '"');
    AddTaskToSpoiledList(ATask, CannotDeleteStorageFile, 'Cannot delete task''s folder');
  end;
end;

procedure TEvExchangeSchedulerMod.DeleteTask(const ATaskGUID: TisGUID);
var
  TaskToBeDeleted: IEvExchangeScheduledTask;
  ExtTaskToBeDeleted: IEvExchangeScheduledTaskStatus;
begin
  // inactivate and deleting task from tasks list
  Lock;
  try
    TaskToBeDeleted := GetTask(ATaskGUID);
    if Assigned(TaskToBeDeleted) then
    begin
      FTaskList.DeleteValue(ATaskGUID);
      TaskToBeDeleted.DeactivateTask;
    end;
  finally
    Unlock;
  end;

  // cleanup the task
  if Assigned(TaskToBeDeleted) then
  begin
    ExtTaskToBeDeleted := (TaskToBeDeleted as IEvExchangeScheduledTaskStatus);
    ExtTaskToBeDeleted.Lock;
    try
      try
        ExtTaskToBeDeleted.SaveTaskToStorage;
        ExtTaskToBeDeleted.DeleteTaskStaff;
      finally
        DeleteStorageForTask((ExtTaskToBeDeleted as IEvExchangeScheduledTask).GetTaskGUID);
      end;
    finally
      ExtTaskToBeDeleted.Unlock;
    end;
  end;
end;

destructor TEvExchangeSchedulerMod.Destroy;
begin
  if FActive then
    SetActive(False);

  inherited;
end;

procedure TEvExchangeSchedulerMod.DoOnConstruction;
begin
  inherited;
  InitLock;
  FTaskList := TisListOfValues.Create;
  FSpoiledTaskslist := TisListOfValues.Create;
end;

function TEvExchangeSchedulerMod.ErrorCodeToString(const AErrorCode: TEvoXTaskErrors): String;
begin
  Result := PadLeft(IntToStr(Integer(AErrorCode)), '0', ErrorCodeLength);
end;

function TEvExchangeSchedulerMod.GetActive: Boolean;
begin
  Result := FActive;
end;

function TEvExchangeSchedulerMod.GetErrorCodeFromString(const AString: String): TEvoXTaskErrors;
var
  S: String;
begin
  S := Copy(AString, ErrorCodeStartPos, ErrorCodeLength);
  try
    Result := TEvoXTaskErrors(StrToInt(S));
  except
    on E:Exception do
      raise Exception.Create('Unknown TaskErrorCode: ' + S);
  end;
end;

function TEvExchangeSchedulerMod.GetListOfSpoiledTasks: IisListOfValues;
var
  i: integer;
  S: String;
begin
  Lock;
  try
    Result := FSpoiledTaskslist.GetClone;
  finally
    Unlock;
  end;

  for i := 0 to Result.Count - 1 do
  begin
    S := Result.Values[i].Value;
    Delete(S, ErrorCodeStartPos, ErrorCodeLength);
    Result.Values[i].Value := S;
  end;
end;

function TEvExchangeSchedulerMod.GetTask(const ATaskGUID: TisGUID): IEvExchangeScheduledTask;
begin
  Lock;
  try
    Result := nil;
    if FTaskList.ValueExists(ATaskGUID) then
      Result := IInterface(FTaskList.Value[ATaskGUID]) as IEvExchangeScheduledTask;
  finally
    Unlock;
  end;
end;

function TEvExchangeSchedulerMod.GetTaskGUIDsList: IisStringList;
var
  i: integer;
begin
  Lock;
  try
    Result := TisStringList.Create;
    for i := 0 to FTaskList.Count - 1 do
      Result.Add(FTaskList.Values[i].Name);
  finally
    Unlock;
  end;
end;

procedure TEvExchangeSchedulerMod.LoadSpoiledTasksList;
var
  i: integer;
  S: String;
  ErrorCode: TEvoXTaskErrors;
  bDeleted, bUpdated: boolean;

  procedure Load;
  var
    tmpStream: IevDualStream;
    sFileName: String;
  begin
    sFileName := NormalizePath(GetTasksRootStorageFolder + Context.UserAccount.User) + SpoiledTasksFile;
    Lock;
    try
      FSpoiledTasksList.Clear;
      if FileExists(sFileName) then
      begin
        tmpStream := TevDualStreamHolder.CreateFromFile(sFileName);
        tmpStream.Position := 0;
        FSpoiledTasksList.ReadFromStream(tmpStream);
      end;
    finally
      Unlock;
    end;
  end;
begin
  Load;

  // trying to cleanup list;
  bUpdated := false;
  for i := FSpoiledTasksList.Count - 1 downto 0 do
  begin
    S := FSpoiledTasksList.Values[i].Value;
    ErrorCode := GetErrorCodeFromString(S);
    if ErrorCode = CannotDeleteStorageFile then
    begin
      bDeleted := DeleteStorageForTask(FSpoiledTasksList.Values[i].Name, true, false);
      if bDeleted then
      begin
        bUpdated := true;
        FSpoiledTasksList.DeleteValue(FSpoiledTasksList.Values[i].Name);
      end;
    end;
  end;
  if bUpdated then
    SaveSpoiledTasksList;
end;

procedure TEvExchangeSchedulerMod.LoadTasksFromStorage;
var
  sPath: String;
  srFolder, srFile: TSearchRec;
  FileAttrsFolder, FileAttrsFile: Integer;
  TaskStream: IevDualStream;
  Task: IEvExchangeScheduledTaskStatus;
begin
  FTaskList.Clear;
  sPath := NormalizePath(GetTasksRootStorageFolder + Context.UserAccount.User);
  if DirectoryExists(sPath) then
  begin
    FileAttrsFolder := faDirectory;
    if FindFirst(sPath + '*.*', FileAttrsFolder, srFolder) = 0 then
    begin
      try
        repeat
          if (srFolder.Name = '.') or (srFolder.Name = '..') then
            continue;
          FileAttrsFile := 0;
          if FindFirst(sPath + srFolder.Name + '\*' + TaskFileExt, FileAttrsFile, srFile) = 0 then
          begin
            try
              repeat
                try
                  TaskStream := TevDualStreamHolder.CreateFromFile(sPath + srFolder.Name + '\' + srFile.Name);
                  TaskStream.Position := 0;
                  Task := TEvExchangeScheduledTask.Create;
                  Task.ReadFromStream(TaskStream);
                  Task.EnableLogging;

                  if FSpoiledTasksList.ValueExists((Task as IEvExchangeScheduledTask).GetTaskGUID) then
                    Task.MakeTaskIncorrect;
                  FTaskList.AddValue((Task as IEvExchangeScheduledTask).GetTaskGUID, Task);
                except
                  on E:Exception do
                  begin
                    mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot load task from file',
                      'Cannot load task from file. Filename "' + sPath + srFolder.Name + '\' + srFile.Name + TaskFileExt + '"' +
                      '. Error: "' + E.Message + '"' + #13#10 + GetStack);
                  end;
                end;
              until FindNext(srFile) <> 0;
            finally
              FindClose(srFile);
            end;
          end;
        until FindNext(srFolder) <> 0;
      finally
        FindClose(srFolder);
      end;
    end;
  end;
end;

procedure TEvExchangeSchedulerMod.ReadOptions;
begin
  FTasksRootStorageFolder := NormalizePath(Mainboard.AppConfiguration.GetValue('EvoXScheduler\TasksRootStorageFolder',
    AppDir + RootSchedulerFolder));
end;

procedure TEvExchangeSchedulerMod.CheckForTheSameFolder(const APath: String);
var
  i: integer;
  Task: IEvExchangeScheduledTask;
  TaskParams: IisListOfValues;
  TaskPath: String;
  tmpPath: String;
begin
  if Trim(APath) = '' then
    exit;

  tmpPath := Trim(UpperCase(APath));
  Lock;
  try
    for i := 0 to FTaskList.Count - 1 do
    begin
      Task := IInterface(FTaskList.Values[i].Value) as IEvExchangeScheduledTask;
      if Task.GetTaskStatus = tskEvoXActive then
      begin
        TaskParams := Task.GetTaskParams;
        TaskPath := TaskParams.Value[EvoXTaskFolderPath];
        CheckCondition(Trim(UpperCase(TaskPath)) <> tmpPath,
          'Existing active task already has the same data files path. Task''s name: ' + TaskParams.Value[EvoXTaskName]);
      end;  
    end;
  finally
    Unlock;
  end;
end;

procedure TEvExchangeSchedulerMod.Run(const Params: TTaskParamList);
var
  iErrorCnt: integer;

  procedure MainLoop;
  var
    ActiveTasks: IisListOfValues;
    i: integer;
    Task: IEvExchangeScheduledTask;
  begin
    // filling list of tasks to be processed
    ActiveTasks := TisListOfValues.Create;
    Lock;
    try
      for i := 0 to FTaskList.Count - 1 do
      begin
        Task := IInterface(FTaskList.Values[i].Value) as IEvExchangeScheduledTask;
        // adding active tasks and tasks with executing task queue jobs
        if (Task.GetTaskStatus = tskEvoXActive) or Task.HasJobsInTaskQueue then
          ActiveTasks.AddValue(FTaskList.Values[i].Name, Task);
      end;
    finally
      Unlock;
    end;

    // processing tasks
    for i := 0 to ActiveTasks.Count - 1 do
    begin
      Task := IInterface(ActiveTasks.Values[i].Value) as IEvExchangeScheduledTask;
      try
        (Task as IEvExchangeScheduledTaskStatus).ProcessTask;
      except
        on E: Exception do
        begin
          Inc(iErrorCnt);
          try
            mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Exception in main scheduler thread when it tried to process task: '
              + Task.GetTaskGUID, E.Message + #13#10 + GetStack);
          except
          end;
        end;
      end;  
    end;  // i
  end;

begin
  iErrorCnt := 0;
  while not CurrentThreadTaskTerminated do
  begin
    try
      MainLoop;
    except
      on E: EAbort do
        raise;
      on E: Exception do
      begin
        mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Exception in main scheduler thread', E.Message + #13#10 + GetStack);
        Inc(iErrorCnt);
        if iErrorCnt > 1000 then
        begin
          mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'EvoX Scheduler is stopped. Reason: too many errors in its thread', '');
          AbortEx;
        end;
      end;
    end;
    Snooze(SchedulerSnoozeInterval * 1000);
    if not (Context.License.EvoX and (Context.Security.AccountRights.Screens.GetState('TEDIT_EVOX_MAP') = stEnabled)
      and (Context.Security.AccountRights.Screens.GetState('TEDIT_EVOX_TASKS') = stEnabled)) then
    begin
      FActive := false;
      FThreadId := 0;
      exit;
    end;
  end;
end;

procedure TEvExchangeSchedulerMod.SaveSpoiledTasksList;
var
  tmpStream: IevDualStream;
  bDeleted: boolean;
  sFileName: String;
begin
  sFileName := NormalizePath(GetTasksRootStorageFolder + Context.UserAccount.User) + SpoiledTasksFile;
  Lock;
  try
    if FileExists(sFileName) then
    begin
      bDeleted := TryToDeleteFile(sFileName, 3 , 1000);
      if not bDeleted then
      begin
        mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot delete spoiled tasks file for updating it',
          'Cannot delete spoiled tasks file for updating it. Filename "' + sFileName + '"');
        exit;
      end;
    end;
    tmpStream := TevDualStreamHolder.CreateFromNewFile(sFileName);
    FSpoiledTaskslist.WriteToStream(tmpStream);
  finally
    Unlock;
  end;
end;

procedure TEvExchangeSchedulerMod.SetActive(const AValue: Boolean);
begin
  if AValue then
  begin
    if not GetActive then
      if Context.License.EvoX and (ctx_AccountRights.Functions.GetState('USER_CAN_IMPORT_USING_EVOX') = stEnabled)
        and (Context.Security.AccountRights.Screens.GetState('TEDIT_EVOX_TASKS') = stEnabled) then
      begin
        FActive := True;
        StartUp;
        FThreadID := Context.RunParallelTask(Run, Self, MakeTaskParams([]), 'EvoX Scheduler');
      end;
  end
  else
  begin
    FActive := False;
    if FThreadId <> 0 then
    begin
      GlobalThreadManager.TerminateTask(FThreadId);
      GlobalThreadManager.WaitForTaskEnd(FThreadId);
      FThreadId := 0;
    end;
    CleanUp;
  end;
end;

procedure TEvExchangeSchedulerMod.StartUp;
begin
  ReadOptions;
  ForceDirectories(GetTasksRootStorageFolder);
  LoadSpoiledTasksList;
  LoadTasksFromStorage;
end;

function TEvExchangeSchedulerMod.GetTasksRootStorageFolder: String;
begin
  lock;
  try
    Result := FTasksRootStorageFolder;
  finally
    Unlock;
  end;
end;

procedure TEvExchangeSchedulerMod.LoadTasksWithoutActivation;
begin
  if not FActive then
    StartUp;  
end;

{ TEvExchangeScheduledTask }

procedure TEvExchangeScheduledTask.ActivateTask;
var
  bSaved: boolean;
  TaskPath: String;
begin
  Lock;
  try
    ProcessNewTaskParams;
    TaskPath := FParams.Value[EvoXTaskFolderPath];
    Mainboard.EvoXScheduler.CheckForTheSameFolder(TaskPath);
    FStatus := tskEvoXActive;
    bSaved := SaveTaskToStorage(true);
    CheckCondition(bSaved, 'Cannot save new task status to the disk');
    AddEvent(etInformation, 'EvoX Scheduled task', 'Task was activated', '');
  finally
    Unlock;
  end;
end;

function TEvExchangeScheduledTask.AddEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String): ILogEvent;
begin
  CheckCondition(Assigned(FLogFile), 'Logging object is not assigned');
  Lock;
  try
    Result := FLogFile.AddEvent(AEventType, AEventClass, AText, ADetails);
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.CreateTaskFolder;
var
  bFolderCreated: boolean;
begin
  bFolderCreated := ForceDirectories(GetStoragePathForTask(FGUID));
  CheckCondition(bFolderCreated, 'Cannot create folder: ' + GetStoragePathForTask(FGUID));
end;

procedure TEvExchangeScheduledTask.DeactivateTask;
var
  bSaved: boolean;
begin
  Lock;
  try
    FStatus := tskEvoXInactive;
    bSaved := SaveTaskToStorage(true);
    CheckCondition(bSaved, 'Cannot save new task status to the disk');
    AddEvent(etInformation, 'EvoX Scheduled task', 'Task was deactivated', '');
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.DeleteTaskStaff;
var
  TaskInfo: IevTaskInfo;
  TasksList: IisListOfValues;
  i: integer;
begin
  Lock;
  try
    TasksList := FTaskQueueIds.GetClone;
    FTaskQueueIds.Clear;
  finally
    Unlock;
  end;

  for i := 0 to TasksList.Count - 1 do
    try
      TaskInfo := mb_TaskQueue.GetTaskInfo(TasksList.Values[i].Name);
      if Assigned(TaskInfo) then
        mb_TaskQueue.RemoveTask(TasksList.Values[i].Name);
    except
      on E:Exception do
        mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot delete task from TaskQueue. TaskId: "' + TasksList.Values[i].Name + '"',
          'Cannot delete task from TaskQueue. Errror: "' + E.Message + '"' + #13#10 + GetStack);
    end;
end;

destructor TEvExchangeScheduledTask.Destroy;
begin
  if FCheckStatusTaskId <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FCheckStatusTaskId);
    GlobalThreadManager.WaitForTaskEnd(FCheckStatusTaskId);
    FCheckStatusTaskId := 0;
  end;

  if FImportTaskID <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FImportTaskID);
    GlobalThreadManager.WaitForTaskEnd(FImportTaskID);
    FImportTaskID := 0;
  end;

  inherited;
end;

procedure TEvExchangeScheduledTask.DoOnConstruction;
begin
  inherited;
  InitLock;
  FGUID := GetUniqueID;
  FParams := TisListOfValues.Create;
  FTaskQueueIds := TisListOfValues.Create(nil, true);
  FLastRunTime := 0;
  FLastQueueStatusUpdated := 0;
  FStatus := tskEvoXInactive;
  InitTaskParams;
end;

procedure TEvExchangeScheduledTask.EnableLogging;
begin
  if not Assigned(FLogFile) then
  try
    FLogFile := TevLogFile.Create(GetStoragePathForTask(FGUID) + FGUID + '.log');
  except
    on E: Exception do
    begin
      FLogFile := nil;
      mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot load log file for task#' + FGUID + '. Log file will be cleared', E.Message);
      DeleteFile(GetStoragePathForTask(FGUID) + FGUID + '.log');
      DeleteFile(GetStoragePathForTask(FGUID) + FGUID + '.log.ind');
      FLogFile := TevLogFile.Create(GetStoragePathForTask(FGUID) + FGUID + '.log');
    end;
  end;
  FLogFile.PurgeRecThreshold := DefaultPurgeRecThreshold;
  FLogFile.PurgeRecCount := DefaultPurgeRecCount;
end;

function TEvExchangeScheduledTask.GetLogFileName: String;
begin
  Lock;
  try
    if Assigned(FLogFile) then
      Result := FLogFile.FileName
    else
      Result := '';  
  finally
    Unlock
  end;  
end;

function TEvExchangeScheduledTask.GetTaskGUID: TisGUID;
begin
  Lock;
  try
    Result := FGUID;
  finally
    Unlock;
  end;
end;

function TEvExchangeScheduledTask.GetTaskParams: IisListOfValues;
begin
  Lock;
  try
    Result := FParams.GetClone;
  finally
    Unlock;
  end;
end;

function TEvExchangeScheduledTask.GetTaskQueueIds: IisListOfValues;
begin
  Lock;
  try
    Result := FTaskQueueIds;    
  finally
    Unlock;
  end;
end;

function TEvExchangeScheduledTask.GetTaskStatus: TEvoXTaskStatus;
begin
  Result := FStatus; 
end;

class function TEvExchangeScheduledTask.GetTypeID: String;
begin
  Result := 'EvExchangeScheduledTask';
end;

function TEvExchangeScheduledTask.HasJobsInTaskQueue: boolean;
begin
  Lock;
  try
    Result := FTaskQueueIds.Count > 0;
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.InitTaskParams;
begin
  FParams.AddValue(EvoXTaskFolderPath, '');
  FParams.AddValue(EvoXTaskMapFilePath, '');
  FParams.AddValue(EvoXTaskTimeInterval, 10); // minutes
  FParams.AddValue(EvoXTaskFilePattern, '*.csv');
  FParams.AddValue(EvoXTaskName, 'Noname');
  FParams.AddValue(EvoXTaskEmailAddress, '');
  FParams.AddValue(EvoXTaskEMailNotification, Integer(tskNever));
  FParams.AddValue(EvoXTaskStopOnError, 'N');
end;

procedure TEvExchangeScheduledTask.MakeTaskIncorrect;
begin
  Lock;
  try
    FStatus := tskEvoXIncorrect;
    AddEvent(etInformation, 'EvoX Scheduled task', 'Task was made incorrect', '');
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.SendEMailNotification(const AEventType: TEvoXTaskEventType; const AMessage: String;
 const AMessageDetails: String);
var
  NotifyType: TEvoxTaskEmailNotification;
  i: integer;
  sSubject, sFrom, sTo, sText: String;
begin
  try
    i := FParams.Value[EvoXTaskEMailNotification];
    NotifyType := TEvoxTaskEmailNotification(i);
    if NotifyType = tskNever then
      exit;

    if (NotifyType = tskAlways) or (NotifyType = AEventType) then
    begin
      // preparing values
      Lock;
      try
        if AEventType = tskSuccess then
          sSubject := '[Do not reply] EvoX import task finished successfully. Task name: "' + FParams.Value[EvoXTaskName]
        else
          sSubject := '[Do not reply] EvoX import task has problem(s). Task name: "' + FParams.Value[EvoXTaskName];

        sFrom := 'EvoXScheduler';
        sTo := FParams.Value[EvoXTaskEmailAddress];

        if AMessageDetails <> '' then
          sText := AMessage + #13#10#13#10 + AMessageDetails
        else
          sText := AMessage;
      finally
        Unlock;
      end;

      // sending
      if sTo <> '' then
        Context.EMailer.SendQuickMail(sTo, sFrom, sSubject, sText);
    end;
  except
    on E: Exception do
      mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot send e-mail notification', E.Message + #13#10 + GetStack);
  end;
end;

procedure TEvExchangeScheduledTask.ProcessNewTaskParams;
var
  S: String;
  i: integer;
  NotifType: TEvoxTaskEmailNotification;
begin
  // task name
  CheckCondition(FParams.ValueExists(EvoXTaskName), 'Task parameters should include name of task');
  S := VarToStr(FParams.Value[EvoXTaskName]);
  CheckCondition(Trim(S) <> '', 'Task should have not empty name');

  // path to the folder
  CheckCondition(FParams.ValueExists(EvoXTaskFolderPath), 'Task parameters should include path to the folder where data files are located');
  S := VarToStr(FParams.Value[EvoXTaskFolderPath]);
  CheckCondition(Trim(S) <> '', 'Task should have not empty data files folder''s path');
  FParams.Value[EvoXTaskFolderPath] := NormalizePath(S);

  // path to map file
  CheckCondition(FParams.ValueExists(EvoXTaskMapFilePath), 'Task parameters should include path to the map file');
  S := VarToStr(FParams.Value[EvoXTaskMapFilePath]);
  CheckCondition(Trim(S) <> '', 'Task should have not empty map file path');

  // file pattern
  CheckCondition(FParams.ValueExists(EvoXTaskFilePattern), 'Task parameters should include file names pattern');
  S := VarToStr(FParams.Value[EvoXTaskFilePattern]);
  if Trim(S) = '' then
    FParams.Value[EvoXTaskFilePattern] := '*.*'
  else
    CheckCondition(Trim(UpperCase(S)) <> BackCopyPattern, '''*.bak'' file''s  extention is reserved for internal use only');

  // time interval in minutes
  CheckCondition(FParams.ValueExists(EvoXTaskTimeInterval), 'Task parameters should include time interval');
  try
    i := FParams.Value[EvoXTaskTimeInterval];
  except
    on E:Exception do
      raise Exception.Create('Time interval is not integer value');
  end;
  CheckCondition(i > 0, 'Time interval should me above zero');

  // e-mail
  if not FParams.ValueExists(EvoXTaskEmailAddress) then
    FParams.AddValue(EvoXTaskEmailAddress, '');

  // e-mail notification type
  CheckCondition(FParams.ValueExists(EvoXTaskEMailNotification), 'Task parameters should include email notification type');
  i := FParams.Value[EvoXTaskEMailNotification];
  NotifType := TEvoxTaskEmailNotification(i);
  if NotifType <> tskNever then
  begin
    S := FParams.Value[EvoXTaskEMailNotification];
    CheckCondition(Trim(S) <> '', 'Task should have set email address up if email notification is on');
  end;

  // Stop on error flag
  if not FParams.ValueExists(EvoXTaskStopOnError) then
    FParams.AddValue(EvoXTaskStopOnError, 'N');

  // Task Status Check Interval
  if not FParams.ValueExists(EvoXTaskStatusCheckInterval) then
    FParams.AddValue(EvoXTaskStatusCheckInterval, EvoXDefaultTaskStatusCheckInterval)
  else
  begin
    try
      i := FParams.Value[EvoXTaskStatusCheckInterval];
    except
      on E:Exception do
        raise Exception.Create('Task Status Check interval is not integer value');
    end;
    CheckCondition(i > 0, 'Task Status Check interval should me above zero');
    CheckCondition(i <= EvoXDefaultTaskStatusCheckInterval, 'Task Status Check interval cannot be greater than ' + IntToStr(EvoXDefaultTaskStatusCheckInterval));
  end;
end;

procedure TEvExchangeScheduledTask.ProcessTask;
var
  ImportInterval, CheckStatusInterval: integer; // seconds
  bStatusChanged, bTimeToPickUpFile: boolean;
begin
  Lock;
  try
    if GlobalThreadManager.TaskIsTerminated(FImportTaskID) then
      FImportTaskID := 0;
    if GlobalThreadManager.TaskIsTerminated(FCheckStatusTaskId) then
      FCheckStatusTaskId := 0;
    bStatusChanged := false;

    // checking if it's time to pickup new files
    bTimeToPickUpFile := false;
    ImportInterval := FParams.Value[EvoXTaskTimeInterval] * 60;
    if (Abs(SecondsBetween(Now, FLastRunTime)) > ImportInterval) and (FImportTaskID = 0) then
      bTimeToPickUpFile := true;

    // checking status of existing tasks if it's time to check or if it's time to pickup with STopOnError ON
    CheckStatusInterval := FParams.Value[EvoXTaskStatusCheckInterval] * 60;
    if ((Abs(SecondsBetween(Now, FLastQueueStatusUpdated)) > CheckStatusInterval) and (FCheckStatusTaskId = 0)) or
      (bTimeToPickUpFile and (FCheckStatusTaskId = 0) and (FParams.Value[EvoXTaskStopOnError] = 'Y')) then
    begin
      FLastQueueStatusUpdated := Now;
      bStatusChanged := true;
      FCheckStatusTaskId := Context.RunParallelTask(UpdateTaskQueueStatus, Self, MakeTaskParams([]),
        'EvoX Scheduler. TaskQueue thread. Task#' + FGUID);
    end;

    // pickup new files
    if bTimeToPickUpFile then
    begin
      FLastRunTime := Now;
      bStatusChanged := true;
      FImportTaskID := Context.RunParallelTask(StartImport, Self, MakeTaskParams([]),
        'EvoX Scheduler. Import thread. Task#' + FGUID);
    end;

    if bStatusChanged then
      SaveTaskToStorage(true);
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FGUID := AStream.ReadString;
  FStatus := TEvoXTaskStatus(AStream.ReadInteger);
  FParams.ReadFromStream(AStream);
  FTaskQueueIds.ReadFromStream(AStream);
end;

procedure TEvExchangeScheduledTask.ResumeTask;
begin
  Lock;
  try
    if FStatus = tskEvoXActiveSuspended then
    begin
      FStatus := tskEvoXActive;
      AddEvent(etInformation, 'EvoX Scheduled task', 'Task was resumed', '');
    end;
  finally
    Unlock;
  end;
end;

function TEvExchangeScheduledTask.SaveTaskToStorage(const ALogErrors: boolean): boolean;
var
  sTaskId: String;
  bDeleted: boolean;
  TaskStream: IEvDualStream;
  FilePath, BackCopyFilePath: String;
begin
  Lock;

  try
    Result := false;
    sTaskid := GetTaskGUID;

    // trying to create backcopy
    FilePath := GetStoragePathForTask(sTaskId) + sTaskId + TaskFileExt;
    BackCopyFilePath := ChangeFileExt(FilePath, '.bak');

    if FileExists(BackCopyFilePath) then
      DeleteFile(BackCopyFilePath);

    if FileExists(FilePath) then
      bDeleted := RenameFile(FilePath, BackCopyFilePath)
    else
      bDeleted := true;

    if bDeleted then
      try
        TaskStream := TEvDualStreamHolder.CreateFromNewFile(FilePath);
        WriteToStream(TaskStream);
        Result := true;
      except
        on E:Exception do
        begin
          if ALogErrors then
          begin
            mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Cannot save task''s status to file. TaskId: "' + sTaskId + '"',
              'Cannot save task''s status to file. Errror: "' + E.Message + '". Filename: "' + FilePath + #13#10 + GetStack);
            DeactivateTask;
            Mainboard.EvoXScheduler.AddTaskToSpoiledList(sTaskId, CannotSaveTaskStatusToFile, 'Cannot save task''s status to file');
          end;
        end;
      end
    else
    begin
      if ALogErrors then
      begin
        DeactivateTask;
        Mainboard.EvoXScheduler.AddTaskToSpoiledList(sTaskId, CannotSaveTaskStatusToFile, 'Cannot save task''s status to file because it cannot delete existing file');
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.SetTaskParams(const ATaskParams: IisListOfValues);
var
  bSaved: boolean;
begin
  Lock;
  try
    FParams := ATaskParams;
    ProcessNewTaskParams;
    bSaved := SaveTaskToStorage(true);
    CheckCondition(bSaved, 'Cannot save new task status to the disk');
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.StartImport(const Params: TTaskParamList);
const
  FileReadWithError = 'Error:';
var
  sPattern, sFolder, sMapFilePath: String;
  MapFile: IEvExchangeMapFile;
  CheckResult: IisListOfValues;
  OldErrorsList: IisStringList;
  ExtErrorsList: IEvExchangeResultLog;
  Package: IevExchangePackage;
  PackagesList: IisStringListRO;
  Reader: IEvExchangeDataReader;
  S, sFileNames, sNewFileName, sErrorWithFiles: String;
  FilesList: IisStringList;
  DataSetsList: IisListOfValues;
  i: integer;
  TimeStamp: TDateTime;

  Task: IevEvoXImportTask;
  TaskFiles: IisListOfValues;
  TaskInfo: IevTaskInfo;
  bRenamed: boolean;

  procedure ReadFiles;
  var
    FileStreamsList: IisListOfValues;
    FileStream: IEvDualStream;
    InputData: IEvDataset;
    i: integer;
    ReaderErrorsList: IEvExchangeResultLog;
  begin
    DataSetsList.Clear;
    
    // trying to open files in write mode
    FileStreamsList := TisListOfValues.Create;
    for i := 0 to FilesList.Count - 1 do
    begin
      AbortIfCurrentThreadTaskTerminated;
      try
        FileStream := TEvDualStreamHolder.CreateFromFile(FilesList[i], false, false, false);
        FileStreamsList.AddValue(FilesList[i], FileStream);
      except
        // skipping of files which cannot be opened in silent mode
      end;
    end;
    if FileStreamsList.Count = 0 then
      exit;

    // reading data from files
    for i := 0 to FileStreamsList.Count - 1 do
    begin
      AbortIfCurrentThreadTaskTerminated;
      FileStream := IInterface(FileStreamsList.Values[i].Value) as IEvDualStream;
      ReaderErrorsList := TEvExchangeResultLog.Create;
      try
        InputData := Reader.ReadDataFromStream(FileStream, MapFile, ReaderErrorsList);
      except
        on E: Exception do
        begin
          mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Failed to call ReadDataFromStream', E.Message + #13#10 + GetStack);
          exit;
        end;
      end;
      if ReaderErrorsList.Count > 0 then
        DataSetsList.AddValue(FileReadWithError + FileStreamsList.Values[i].Name, ReaderErrorsList)
      else
        DataSetsList.AddValue(FileStreamsList.Values[i].Name, InputData);
    end;
  end;

begin
  try
    if FStatus <> tskEvoXActive then
      exit;

    // synchronizing with CheckStatus thread if STopOnError is ON
    if (FParams.Value[EvoXTaskStopOnError] = 'Y') and (FCheckStatusTaskId <> 0) then
      GlobalThreadManager.WaitForTaskEnd(FCheckStatusTaskId);

    TimeStamp := Now;
    sErrorWithFiles := '';

    sPattern := FParams.Value[EvoXTaskFilePattern];
    sFolder := NormalizePath(FParams.Value[EvoXTaskFolderPath]);
    if not DirectoryExists(sFolder) then
      exit;

    sMapFilePath := FParams.Value[EvoXTaskMapFilePath];
    if not FileExists(sMapFilePath) then
    begin
      S := 'Task suspended. Map file does not exist: "' + sMapFilePath + '"';
      SuspendTask(S);
      SaveTaskToStorage(true);
      SendEMailNotification(tskException, S, '');
      exit;
    end;

    // deleting *.bak files
    AbortIfCurrentThreadTaskTerminated;
    FilesList := GetFilesByMask(sFolder + sPattern);
    for i := FilesList.Count - 1 downto 0 do
      if Trim(UpperCase(ExtractFileExt(FilesList[i]))) = BackCopyExt then
      begin
        FilesList.Delete(i);
      end;
    if FilesList.Count = 0 then
      exit;

    CoInitialize(nil);
    // start of COM related code
    try
      // loading map file
      try
        AbortIfCurrentThreadTaskTerminated;
        MapFile := LoadMapFile(sMapFilePath);
        CheckCondition(Assigned(MapFile), 'Map file object is not assigned');
        Context.EvolutionExchange.UpgradeMapFileToLastPackageVer(MapFile);
      except
        on E:Exception do
        begin
          S := 'Task suspended. Cannot load map file. Error: ' + E.Message;
          SuspendTask(S);
          SaveTaskToStorage(true);
          MapFile := nil;
          SendEMailNotification(tskException, S, '');
          exit;
        end;
      end;

      // getting package
      try
        AbortIfCurrentThreadTaskTerminated;
        PackagesList := Context.EvolutionExchange.GetEvExchangePackagesList;
      except
        on E: Exception do
        begin
          mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Failed to call GetEvExchangePackagesList', E.Message + #13#10 + GetStack);
          exit;
        end;
      end;
      if (PackagesList.Count = 0) or (PackagesList.IndexOf(MapFile.GetPackageName) = -1) then
      begin
        S := 'Task suspended. Cannot find package with name: "' + MapFile.GetPackageName;
        SuspendTask(S);
        SaveTaskToStorage(true);
        SendEMailNotification(tskException, S, '');
        exit;
      end;

      try
        Package := Context.EvolutionExchange.GetEvExchangePackage(MapFile.GetPackageName);
        Context.EvolutionExchange.PreparePackageGroupRecords(MapFile, Package);
        Context.EvolutionExchange.UpgradeDataTypesInMapFile(MapFile, Package);
      except
        on E: Exception do
        begin
          mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Failed to call GetEvExchangePackage', E.Message + #13#10 + GetStack);
          exit;
        end;
      end;
      if not Assigned(Package) then
      begin
        S := 'Task suspended. Program was not able to package with name: "' + MapFile.GetPackageName;
        SuspendTask(S);
        SaveTaskToStorage(true);
        SendEMailNotification(tskException, S, '');
        exit;
      end;

    finally
      CoUninitialize;
    end;  // end of COM related code

    // checking map file
    try
      AbortIfCurrentThreadTaskTerminated;
      CheckResult := Context.EvolutionExchange.CheckMapFile(MapFile, nil);
      if CheckResult.ValueExists(EvoXStringErrorsList) then
      begin
        OldErrorsList := IInterface(CheckResult.FindValue(EvoXStringErrorsList).Value) as IisStringList;
        ExtErrorsList := TEvExchangeResultLog.Create;
        ExtErrorsList.LoadFromStringList(OldErrorsList);
      end
      else
        ExtErrorsList := IInterface(CheckResult.FindValue(EvoXExtErrorsList).Value) as IEvExchangeResultLog;
    except
      on E: Exception do
      begin
        mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Failed to call CheckMapFile', E.Message + #13#10 + GetStack);
        S := 'Task suspended. Failed to call CheckMapFile. Error:' + E.Message;
        SuspendTask(S);
        SaveTaskToStorage(true);
        SendEMailNotification(tskException, S, '');
        exit;
      end;
    end;
    if ExtErrorsList.Count <> 0 then
    begin
      S := 'Task suspended. Map file has errors';
      SuspendTask(S);
      SaveTaskToStorage(true);
      SendEMailNotification(tskException, S, ExtErrorsList.Text);
      exit;
    end;

    // preparing data reader object
    AbortIfCurrentThreadTaskTerminated;
    Reader := Context.EvolutionExchange.GetDataReader(MapFile.GetDataSourceName);
    if not Assigned(Reader) then
    begin
      S := 'Task suspended. Cannot find data reader with name: "' + MapFile.GetDataSourceName;
      SuspendTask(S);
      SaveTaskToStorage(true);
      SendEMailNotification(tskException, S, '');
      exit;
    end;
    try
      Reader.SetDataReaderOptions(MapFile.GetDataSourceOptions);
    except
      on E: Exception do
      begin
        S := 'Task suspended. Cannot setup options for data reader with name: "' + MapFile.GetDataSourceName + '" Error: ' + E.Message;
        SuspendTask(S);
        SaveTaskToStorage(true);
        SendEMailNotification(tskException, S, E.Message);
        exit;
      end;
    end;

    // reading data
    AbortIfCurrentThreadTaskTerminated;
    DataSetsList := TisListOfValues.Create;
    ReadFiles; // input files are not locked after this point
    if DataSetsList.Count = 0 then
      exit;
    for i := DataSetsList.Count - 1 downto 0 do
      // processing files wich reeader was not able to load
      if AnsiStartsStr(FileReadWithError, DataSetsList.Values[i].Name) then
        try
          S := Copy(DataSetsList.Values[i].Name, Length(FileReadWithError) + 1, MaxInt);
          sErrorWithFiles := sErrorWithFiles + 'Data reader find errors in file. Filename: ' + S;
          OldErrorsList := IInterface(DataSetsList.Values[i].Value) as IisStringList;
          AddEvent(etError, EvoXSchedEventClassName, 'Data reader find errors in file. Filename: ' + S,
            OldErrorsList.Text);
          MoveFileWithRenaming(S, dtErrors);
        finally
          DataSetsList.DeleteValue(DataSetsList.Values[i].Name);
        end;

    // creating task
    try
      Task := mb_TaskQueue.CreateTask(QUEUE_TASK_EVOX_IMPORT, True) as IevEvoXImportTask;
      Task.SetInputDataSets(DataSetsList);
      Task.SetMapFile(MapFile);
      Task.SetPackage(Package);
      Task.Caption := 'EvoX Import task created by EvoX Scheduler. Task''s name: "' + GetTaskName + '"'
        + IntToStr(DataSetsList.Count) + ' file(s) to import';
    except
      on E: Exception do
      begin
        mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Failed to create a new queue task',
          E.Message + #13#10 + GetStack);
        S := 'Task suspended. Failed to create a new queue task. Error:' + E.Message;
        SuspendTask(S);
        SaveTaskToStorage(true);
        SendEMailNotification(tskException, S, '');
        exit;
      end;
    end;

    // adding task to queue
    AbortIfCurrentThreadTaskTerminated;
    TaskFiles := TisListOfValues.Create(nil, true);
    sFileNames := '';  // it will contain list of filenames
    for i := 0 to DataSetsList.Count - 1 do
    begin
      TaskFiles.AddValue(DataSetsList.Values[i].Name, '');
      sFileNames := sFileNames + DataSetsList.Values[i].Name + #13#10;
    end;
    try
      mb_TaskQueue.AddTask(Task);
    except
      on E: Exception do
      begin
        mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Failed to add task to task queue',
          E.Message + #13#10 + GetStack);
        S := 'Task suspended. Failed to add task to task queue. Error:' + E.Message;
        SuspendTask(S);
        SaveTaskToStorage(true);
        SendEMailNotification(tskException, S, '');
        exit;
      end;
    end;

    // saving task status
    FTaskQueueIds.Lock;
    try
      FTaskQueueIds.AddValue(Task.GetId, TaskFiles);
    finally
      FTaskQueueIds.Unlock;
    end;
    Lock;
    try
      SaveTaskToStorage(true);
    finally
      Unlock;
    end;

    TaskInfo := mb_TaskQueue.GetTaskInfo(Task.GetId);
    AddEvent(etInformation, EvoXSchedEventClassName, 'Task created and added to task queue',
      'TaskId: ' + IntToStr(TaskInfo.GetNbr) + #13#10 + 'Data file(s) added:' + #13#10 + sFileNames);

    // moving files
    TaskFiles.Lock;
    try
      for i := 0 to TaskFiles.Count - 1 do
      begin
        sNewFileName := ChangeFileExt(TaskFiles.Values[i].Name, BackCopyExt);
        bRenamed := RenameFile(TaskFiles.Values[i].Name, sNewFileName);
        if bRenamed then
        begin
          try
            ForceDirectories(FParams.Value[EvoXTaskFolderPath] + InProcessFolder);
            S := AddTimestampToFileName(FParams.Value[EvoXTaskFolderPath] + InProcessFolder + ExtractFileName(TaskFiles.Values[i].Name), TimeStamp);
            MoveFile(sNewFileName, S);
            TaskFiles.Values[i].Value := S;
          except
            on E: Exception do
            begin
              AddEvent(etError, EvoXSchedEventClassName, 'Scheduler was not able to move backcopy of input file into processing folder.' ,
                'FileName: ' + sNewFileName);
              sErrorWithFiles := sErrorWithFiles + 'Scheduler was not able to move backcopy of input file into processing folder. FileName: ' + sNewFileName + #13#10;
            end;
          end;
        end
        else
        begin
          AddEvent(etError, EvoXSchedEventClassName, 'Scheduler was not able to rename input file. It will lead to multiple import of the same file' ,
            'FileName: ' + TaskFiles.Values[i].Name);
          sErrorWithFiles := sErrorWithFiles + 'Scheduler was not able to rename input file. FileName: ' + TaskFiles.Values[i].Name + #13#10;
        end;
      end;

      Lock;
      try
        SaveTaskToStorage(true);
      finally
        Unlock;
      end;

      if sErrorWithFiles <> '' then
        SendEMailNotification(tskException, 'Problems with some input data files:', sErrorWithFiles);
    finally
      TaskFiles.UnLock;
    end;
  except
    on E: EAbort do
      raise;
    on E: Exception do
      mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Exception in StartImport. Task# ' + GetTaskGUID, E.Message + #13#10 + GetStack);
  end;
end;

procedure TEvExchangeScheduledTask.SuspendTask(const AReason: String);
begin
  Lock;
  try
    if FStatus = tskEvoXActive then
    begin
      FStatus := tskEvoXActiveSuspended;
      AddEvent(etInformation, 'EvoX Scheduled task', 'Task was suspended', 'Reason: ' + AReason);
    end;
  finally
    Unlock;
  end;
end;

procedure TEvExchangeScheduledTask.UpdateTaskQueueStatus(const Params: TTaskParamList);
var
  TaskId : TisGUID;
  TaskNbr: integer;
  TaskFiles: IisListOfValues;
  i, j: integer;
  TaskInfo: IevTaskInfo;
  Tsk: IevTask;
  CloneOfFTaskQueueIds: IisListOfValues;
  S, sErrorDetals: String;
  ListOfRes, Res: IisListOfValues;
  inputRowsAmount, importedRowsAmount: integer;
  OldErrorsList: IisStringList;
  ExtErrorsList: IEvExchangeResultLog;
  ResultFileName, FileNameWithTimeStamp: String;
  bErrorsFound: boolean;
  ErrorsLogFileName: String;
begin
  if (FStatus = tskEvoXIncorrect) or (FTaskQueueIds.Count = 0) then
    exit;

  try
    // cloning FTaskQueueIds
    FTaskQueueIds.Lock;
    try
      CloneOfFTaskQueueIds := TisListOfValues.Create;
      for i := 0 to FTaskQueueIds.Count - 1 do
      begin
        TaskId := FTaskQueueIds.Values[i].Name;
        TaskFiles := IInterface(FTaskQueueIds.Values[i].Value) as IisListOfValues;
        CloneOfFTaskQueueIds.AddValue(TaskId, TaskFiles);
      end;
    finally
      FTaskQueueIds.Unlock;
    end;

    // processing tasks
    for i := 0 to CloneOfFTaskQueueIds.Count - 1 do
    begin
      TaskId := CloneOfFTaskQueueIds.Values[i].Name;
      TaskFiles := IInterface(CloneOfFTaskQueueIds.Values[i].Value) as IisListOfValues;
      TaskInfo := mb_TaskQueue.GetTaskInfo(TaskId);

      // if task not found
      if not Assigned(TaskInfo) then
      begin
        TaskNbr := -1;
        // deleting task from list, it's not found
        FTaskQueueIds.Lock;
        try
          if FTaskQueueIds.ValueExists(TaskId) then
            FTaskQueueIds.DeleteValue(TaskId);
        finally
          FTaskQueueIds.Unlock;
        end;
        AddEvent(etError, EvoXSchedEventClassName, 'File''s task not found in the task queue',
          'FileName: ' + TaskFiles.Values[i].Name);
        SaveTaskToStorage(true);

        // moving files to error's folder
        sErrorDetals := 'All files will be moved to errors' + #13#10;
        for j := 0 to TaskFiles.Count - 1 do
        begin
          if TaskFiles.Values[j].Value <> '' then
          begin
            AddEvent(etError, EvoXSchedEventClassName, 'File moved to errors because its task not found in the task queue',
              'FileName: ' + TaskFiles.Values[i].Value);
             sErrorDetals := sErrorDetals + TaskFiles.Values[i].Value;
            MoveFileWithRenaming(TaskFiles.Values[i].Value, dtErrors);
          end
          else
          begin
            AddEvent(etError, EvoXSchedEventClassName, 'File''s task not found in the task queue',
              'FileName: ' + TaskFiles.Values[i].Name);
             sErrorDetals := sErrorDetals + TaskFiles.Values[i].Name;
          end;
        end;
        SendEMailNotification(tskException, 'Import task not found in the task queue. TaskId: ' + IntToStr(TaskNbr), sErrorDetals);
      end
      else
      begin
        // task found
        TaskNbr := TaskInfo.Nbr;
        if TaskInfo.State = tsFinished then
        begin
          // deleting task from list, it's done
          FTaskQueueIds.Lock;
          try
            if FTaskQueueIds.ValueExists(TaskId) then
              FTaskQueueIds.DeleteValue(TaskId);
          finally
            FTaskQueueIds.Unlock;
          end;
          S := 'Task Queue Task (TaskId:' + IntToStr(TaskNbr) + ' for Import Task "' + GetTaskName + '" is finished' + #13#10;
          bErrorsFound := false;

          try
            // pulling out files information
            Tsk := mb_TaskQueue.GetTask(TaskId);
            ListOfRes := (Tsk as IevEvoXImportTask).Results;
            for j := 0 to ListOfRes.Count - 1 do
            begin
              Res := IInterface(ListOfRes.Values[j].Value) as IisListOfValues;
              inputRowsAmount := Res.Value[EvoXInputRowsAmount];
              importedRowsAmount := Res.Value[EvoXImportedRowsAmount];

              if Res.ValueExists(EvoXStringErrorsList) then
              begin
                OldErrorsList := IInterface(Res.Value[EvoXStringErrorsList]) as IisStringList;
                ExtErrorsList := TEvExchangeResultLog.Create;
                ExtErrorsList.LoadFromStringList(OldErrorsList);
              end
              else
                ExtErrorsList := IInterface(Res.Value[EvoXExtErrorsList]) as IEvExchangeResultLog;

              ResultFileName := ListOfRes.Values[j].Name;
              if TaskFiles.ValueExists(ResultFileName) and (VarToStr(TaskFiles.Value[ResultFileName]) <> '') then
                FileNameWithTimeStamp := TaskFiles.Value[ResultFileName]
              else
                FileNameWithTimeStamp := ResultFileName;

              S := S + StringOfChar('-', 50) + #13#10;
              S := S + '  File: ' + FileNameWithTimeStamp + #13#10;
              S := S + '    Input rows: ' + IntToStr(inputRowsAmount) + '. Imported rows: ' + IntToStr(importedRowsAmount);
              S := S + '. Errors found: ' + IntToStr(ExtErrorsList.GetAmountOfErrors);
              S := S + '. Warnings found: ' + IntToStr(ExtErrorsList.GetAmountOfWarnings) + #13#10#13#10;
              if TaskFiles.ValueExists(ResultFileName) then
                try
                  if ExtErrorsList.Count = 0 then
                  begin
                    MoveFileWithRenaming(TaskFiles.Value[ResultFileName], dtCompleted);
                  end
                  else
                  begin
                    bErrorsFound := true;
                    ErrorsLogFileName := ChangeFileExt(TaskFiles.Value[ResultFileName], '.log');
                    MoveFileWithRenaming(TaskFiles.Value[ResultFileName], dtErrors);
                    ExtErrorsList.GetAsStringList.SaveToFile(ErrorsLogFileName);
                    MoveFileWithRenaming(ErrorsLogFileName, dtErrors);
                  end;
                finally
                  TaskFiles.DeleteValue(ResultFileName);
                end;
            end;  // j

            // processing files which was not found in results list
            for j := 0 to TaskFiles.Count - 1 do
            begin
              bErrorsFound := true;
              AddEvent(etError, EvoXSchedEventClassName, 'Information about import results not found for file ' +  TaskFiles.Values[i].Name + '. File will be moved to errors', '');
              S := S + '  Information about import results not found for file ' +  TaskFiles.Values[i].Name + '. File will be moved to errors' + #13#10#13#10;
              MoveFileWithRenaming(TaskFiles.Values[i].Value, dtErrors);
            end;

            if bErrorsFound then
            begin
              SendEMailNotification(tskException, 'Import task finished in task queue. Some problems found. TaskId: ' + IntToStr(TaskNbr), S);
              if FParams.Value[EvoXTaskStopOnError] = 'Y' then
              begin
                S := 'Task suspended. Import finished with error(s) and "Suspend task on any import error" option is ON';
                SuspendTask(S);
              end;
            end
            else
              SendEMailNotification(tskSuccess, 'Import task finished in task queue. No problems found. TaskId: ' + IntToStr(TaskNbr), S);
              
          finally
            if bErrorsFound then
              AddEvent(etError, EvoXSchedEventClassName, 'Import task is finished in task queue. Some problems found. TaskId: ' + IntToStr(TaskNbr), '')
            else
              AddEvent(etInformation, EvoXSchedEventClassName, 'Import task is finished in task queue. No problems found. TaskId: ' + IntToStr(TaskNbr), '');
            SaveTaskToStorage(true);
          end;
        end;  // task is finished
      end; // task found/ not found
    end;  // i
  except
    on E: EAbort do
      raise;
    on E: Exception do
      mb_LogFile.AddEvent(etError, EvoXSchedEventClassName, 'Exception in UpdateTaskQueueStatus. Task# ' + GetTaskGUID, E.Message + #13#10 + GetStack);
  end;
end;

procedure TEvExchangeScheduledTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FGUID);
  AStream.WriteInteger(Integer(FStatus));
  FParams.WriteToStream(AStream);
  FTaskQueueIds.WriteToStream(AStream);
end;

function TEvExchangeScheduledTask.LoadMapFile(const AMapFilePath: String): IEvExchangeMapFile;
var
  MapFile: IEvExchangeMapFileExt;
begin
  Result := nil;
  MapFile := TEvExchangeMapFileExt.Create;
  MapFile.LoadFromXMLFile(AMapFilePath);
  Result := MapFile as IEvExchangeMapFile;
end;

function TEvExchangeScheduledTask.AddTimestampToFileName(const AFileName: String; const ATimestamp: TDateTime): String;
var
  sPath, sNameWithoutExt, sExt: String;
  i: integer;
  AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond: Word;
begin
  sPath := ExtractFilePath(AFileName);
  sNameWithoutExt := ExtractFileName(AFileName);
  sExt := ExtractFileExt(AFileName);
  if sExt <> '' then
  begin
    i := LastDelimiter('.' + PathDelim + DriveDelim, sNameWithoutExt);
    if (i > 0) and (sNameWithoutExt[i] = '.') then
      sNameWithoutExt := Copy(sNameWithoutExt, 1, i - 1);
  end;
  DecodeDateTime(ATimeStamp, AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond);
  Result := PadStringLeft(IntToStr(AYear), '0', 4) + '-' + PadStringLeft(IntTOStr(AMonth), '0', 2) +
    '-' + PadStringLeft(IntToStr(ADay), '0', 2) + '_';
  Result := Result + PadStringLeft(IntToStr(AHour), '0', 2) + '-' + PadStringLeft(IntToStr(AMinute), '0', 2) +
    '-' + PadStringLeft(IntToStr(ASecond), '0', 2) + '-' + PadStringLeft(intToStr(AMilliSecond), '0', 4);
  Result := NormalizePath(sPath) + sNameWithoutExt + '_' + Result + sExt;
end;

function TEvExchangeScheduledTask.GetTaskName: String;
begin
  Lock;
  try
    Result := '';
    if FParams.ValueExists(EvoXTaskName) then
      Result := FParams.Value[EvoXTaskName];
  finally
    Unlock;
  end;
end;

function TEvExchangeScheduledTask.MoveFileWithRenaming(const ASourcePathAndFileName: String; ADestType: TEvoXDestinationTypes): boolean;
var
  sFileName, sDestPath: String;
begin
  Result := false;

  sFileName := ExtractFileName(ASourcePathAndFileName);
  case ADestType of
    dtCompleted: sDestPath := FParams.Value[EvoXTaskFolderPath] + CompletedFolder;
    dtErrors: sDestPath := FParams.Value[EvoXTaskFolderPath] + ErrorsFolder;
  end;
  ForceDirectories(sDestPath);
  try
    MoveFile(ASourcePathAndFileName, sDestPath + sFileName);
    Result := true;
  except
    on E: Exception do
    begin
      AddEvent(etError, EvoXSchedEventClassName, 'Scheduler was not able to move file' ,
        'Source File: ' + ASourcePathAndFileName + #13#10 + 'Destination file: ' + sDestPath + sFileName);
    end;
  end;
end;

initialization
  ObjectFactory.Register([TEvExchangeScheduledTask]);
  Mainboard.ModuleRegister.RegisterModule(@GetEvoXScheduler, IEvExchangeScheduler, 'EvoX Scheduler Module');

finalization
  SafeObjectFactoryUnRegister([TEvExchangeScheduledTask]);

end.
