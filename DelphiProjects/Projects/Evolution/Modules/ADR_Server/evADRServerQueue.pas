unit evADRServerQueue;

interface

uses  SysUtils,
      evADRRequestQueue, EvCommonInterfaces, EvADRCommon, isBasicUtils, isThreadManager,
      EvStreamUtils, evMainboard, evContext, isDataSet, isBaseClasses, EvDataSet,
      EvTypes, evConsts, isExceptions, isErrorUtils, EvExceptions, EvBasicUtils;

implementation

uses evADRServerMod, DateUtils;

type
  TevServerADRDBRequest = class(TevADRDBRequest)
  private
    procedure RequestFullDBCopy;
  protected
    procedure BeforeRun; override;
    function  DeleteOnError: Boolean; override;
  end;


  TevADRFullDBCopyRequest = class(TevServerADRDBRequest)
  private
    FData: IisStream;
    function  GetData: IisStream;
  protected
    procedure Store; override;
    procedure RemoveFromStorage; override;
    function  Caption: String; override;
    procedure Run; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRSyncDBListRequest = class(TevServerADRDBRequest)
  private
    FErrors: String;
    FDBList: IisStringList;
    procedure Process;
  protected
    procedure Store; override;
    procedure Restore; override;
    function  Caption: String; override;
    procedure Run; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRRebuildTempTablesForAll = class(TevServerADRDBRequest)
  private
    FCleanTT: Boolean;
  protected
    procedure Store; override;
    procedure Restore; override;
    function  Caption: String; override;
    procedure Run; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRRebuildTempTables = class(TevServerADRDBRequest)
  private
    FAllClientsMode: Boolean;
    function RestoreTTInQueue: Boolean;
  protected
    procedure Store; override;
    procedure Restore; override;
    function  Caption: String; override;
    procedure Run; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRFinalizeRebuildTempTablesForAll = class(TevServerADRDBRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRApplySyncDataRequest = class(TevServerADRDBRequest)
  private
    FData: IisStream;
    function  GetData: IisStream;
  protected
    procedure Store; override;
    procedure RemoveFromStorage; override;
    function  Caption: String; override;
    procedure Run; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRDBDataCheckRequest = class(TevServerADRDBRequest)
  private
    FData: IisStream;
    function  GetData: IisStream;
  protected
    procedure Store; override;
    procedure RemoveFromStorage; override;
    function  Caption: String; override;
    procedure Run; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRDBTransCheckRequest = class(TevServerADRDBRequest)
  private
    FTransCount: Integer;
  protected
    procedure Store; override;
    procedure Restore; override;
    function  Caption: String; override;
    procedure Run; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRSweepDBRequest = class(TevServerADRDBRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
    function  DeleteOnError: Boolean; override;
    function  IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADREnlistSweepDBRequest = class(TevADRRequest)
  protected
    procedure Run; override;
    function  Caption: String; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
  end;



{ TevADRFullDBCopyRequest }

function TevADRFullDBCopyRequest.Caption: String;
begin
  Result := GetRestoreRequestName(Database);
end;

constructor TevADRFullDBCopyRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FData := IInterface(AParams[3]) as IisStream;
end;

function TevADRFullDBCopyRequest.GetData: IisStream;
begin
  if not Assigned(FData) then
    FData := Storage.AsBlob[GetID + '\Data'];
  Result := FData;
end;

class function TevADRFullDBCopyRequest.GetTypeID: String;
begin
  Result := ADR_Srv_FullDBCopy;
end;

function TevADRFullDBCopyRequest.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
    if IsTempDB(Database) then
      Result := (AExistingRequest.TypeID = ADR_Srv_SyncDBList) or (AExistingRequest.TypeID = ADR_Srv_RebuildTT) or
                (AExistingRequest.TypeID = ADR_Srv_RebuildAllTT) or      
                (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT) or
                (AExistingRequest.TypeID = ADR_Srv_SyncDBData) or (AExistingRequest.TypeID = ADR_Srv_SweepDB);
end;

procedure TevADRFullDBCopyRequest.RemoveFromStorage;
begin
  FData := nil; // to close file
  inherited;
end;

procedure TevADRFullDBCopyRequest.Run;
var
  emailSentTime: TDateTime;
begin
  inherited;

  try
    // Restore
    SetProgress('Restoring database');

    emailSentTime := 0;
    while not CurrentThreadTaskTerminated do
    begin
      try
        ctx_DBAccess.RestoreDB(Database, GetData, True);
        break;
      except
        on E: Exception do
        begin
          if MinutesBetween(emailSentTime, Now) > 10 then
          begin
            (QueueObj as IevADRRequestQueueExt).ADRModule.SendAlertEmail('ADR at critical state',
            'A potential never-ending request is detected. ' + Database + ' could not be restored.'#13#13 + E.Message);
            emailSentTime := Now;
          end;

          QueueExt.ADRModule.LogError(Database + ' could not be restored. Trying again...', MakeFullError(E));
          Snooze(30000);
        end;
      end;
    end;

    (Mainboard.ADRServer as IevADRServerExt).DBOfflineStatuses.ClearDBInfo(Database);

    QueueExt.ADRModule.LogDebugEvent(Database + ' has been restored');
  finally
    SetProgress('');
  end;
end;

procedure TevADRFullDBCopyRequest.Store;
begin
  inherited;
  Storage.AsBlob[GetID + '\Data'] := GetData;
  FData := nil;  // to close file
end;

{ TevADRSyncDBListRequest }

function TevADRSyncDBListRequest.Caption: String;
begin
  Result := GetSyncDBListName;
end;

constructor TevADRSyncDBListRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FDBList := IInterface(AParams[3]) as IisStringList;
end;

class function TevADRSyncDBListRequest.GetTypeID: String;
begin
  Result := ADR_Srv_SyncDBList;
end;

function TevADRSyncDBListRequest.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
      Result := (AExistingRequest.TypeID = ADR_Srv_SyncDBList) or (AExistingRequest.TypeID = ADR_Srv_RebuildTT) or
                (AExistingRequest.TypeID = ADR_Srv_SyncDBData) or
                (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT) or
                (AExistingRequest.TypeID = ADR_Srv_SweepDB) and IsTempDB(AExistingRequest.Database);
end;

procedure TevADRSyncDBListRequest.Restore;
begin
  inherited;
  FDBList := TisStringList.Create;
  FDBList.CommaText := Storage.AsString[GetID + '\DBList'];
end;

procedure TevADRSyncDBListRequest.Process;
var
  TargetClients: IisStringList;
  DB: String;
  i: Integer;
begin
   // Get list of clients
   TargetClients := TisStringList.Create();
   TargetClients.Text := ctx_DBAccess.GetClientsList;

   FDBList.CaseSensitive := False;
   FDBList.Sorted := True;

   // Compare target DB list with the source DB list
   for i := 0 to TargetClients.Count - 1 do
   begin
     AbortIfCurrentThreadTaskTerminated;

     DB := DB_Cl + TargetClients[i];
     if FDBList.IndexOf(DB) = -1 then
       try
         SetProgress('Deleting database ' + DB);
         Mainboard.ADRServer.DeleteDBRequests(DB);

{  See "SYNCHING TEMP TABLES" comment in EvFirebird.pas

         ctx_DBAccess.StartNestedTransaction([dbtTemporary]);
         try
           ctx_DBAccess.ExecQuery('DELETE FROM tmp_cl where cl_nbr = ' + IntToStr(CL_NBR), nil, nil);
           ctx_DBAccess.CommitNestedTransaction;
         except
           ctx_DBAccess.RollbackNestedTransaction;
           raise;
         end;
}

         ctx_DBAccess.DropDB(DB);
         (Mainboard.ADRServer as IevADRServerExt).DBOfflineStatuses.ClearDBInfo(DB);
       except
         on E: Exception do
           FErrors := FErrors + #13 + DB + ': ' + E.Message;
       end;
   end;
end;

procedure TevADRSyncDBListRequest.Run;
begin
  inherited;

  FErrors := '';
  try
    Process;
  finally
    SetProgress('');
  end;

  CheckCondition(FErrors = '', 'Errors' + #13 + FErrors);
  QueueExt.ADRModule.LogDebugEvent('DB list has been syncronized')
end;

procedure TevADRSyncDBListRequest.Store;
begin
  inherited;
  Storage.AsString[GetID + '\DBList'] := FDBList.CommaText;
end;

{ TevADRRebuildTempTablesForAll }

function TevADRRebuildTempTablesForAll.Caption: String;
begin
  Result := GetRebuildTmpTblsForAllRequestName;
end;

constructor TevADRRebuildTempTablesForAll.Create(
  const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FCleanTT := AParams[3];
end;

class function TevADRRebuildTempTablesForAll.GetTypeID: String;
begin
  Result := ADR_Srv_RebuildAllTT;
end;

function TevADRRebuildTempTablesForAll.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
    Result := (AExistingRequest.TypeID = ADR_Srv_RebuildAllTT) or
              (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT) or    
              FCleanTT and (AExistingRequest.TypeID = ADR_Srv_RebuildTT);
end;

procedure TevADRRebuildTempTablesForAll.Restore;
begin
  inherited;
  FCleanTT := Storage.AsBoolean[GetID + '\CleanTT'];
end;

procedure TevADRRebuildTempTablesForAll.Run;
var
  i, j: Integer;
  DB: String;
  RList: IisList;
  Clients: IisStringList;
  R: IevADRRequest;
begin
  inherited;

  // Create empty Temp Tables
  if FCleanTT then
  begin
    SetProgress('Cleaning temp tables');
    ctx_DBAccess.BeginRebuildAllClients;
    (Mainboard.ADRServer as IevADRServerExt).DBOfflineStatuses.ClearDBInfo(DB_TempTables);

     QueueExt.NewDBRequest(OriginatedAt, ADR_Srv_FinalizeRebuildAllTT, ['', '', 0], Self);
  end;

  // Schedule rebuild TT requests
  Clients := TisStringList.Create;
  Clients.Text := ctx_DBAccess.GetClientsList;

  while Clients.Count > 0 do
  begin
    AbortIfCurrentThreadTaskTerminated;

    // Randomize sequence to provide even load spread over DB servers
    i := ISBasicUtils.RandomInteger(0, Clients.Count);

    DB := DB_Cl + Clients[i];

    SetProgress('Creating rebuilding temp tables requests for ' + DB);
    RList := QueueExt.GetRequests('', [], DB);
    for j := 0 to RList.Count - 1 do
    begin
      R := RList[j] as IevADRRequest;
      if (R.TypeID = ADR_Srv_FullDBCopy) or (R.TypeID = ADR_Srv_RebuildTT) then
      begin
        DB := '';
        break;
      end;
    end;
    R := nil;
    RList := nil;

    if DB <> '' then
      QueueExt.NewDBRequest(OriginatedAt, ADR_Srv_RebuildTT, [DB, '', 0, True], Self);

    Clients.Delete(i);  
  end;
end;

procedure TevADRRebuildTempTablesForAll.Store;
begin
  inherited;
  Storage.AsBoolean[GetID + '\CleanTT'] := FCleanTT;
end;

{ TevADRRebuildTempTables }

function TevADRRebuildTempTables.Caption: String;
begin
  Result := GetRebuildTmpTblsRequestName(Database)
end;

constructor TevADRRebuildTempTables.Create(
  const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FAllClientsMode := AParams[3];
end;

class function TevADRRebuildTempTables.GetTypeID: String;
begin
  Result := ADR_Srv_RebuildTT;
end;

function TevADRRebuildTempTables.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
    Result := (AExistingRequest.TypeID = ADR_Srv_SyncDBList) or
              (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT) or
              (AExistingRequest.TypeID = ADR_Srv_SweepDB) and IsTempDB(AExistingRequest.Database);
end;

procedure TevADRRebuildTempTables.Restore;
begin
  inherited;
  FAllClientsMode := Storage.AsBoolean[GetID + '\AllClientsMode'];
end;

function TevADRRebuildTempTables.RestoreTTInQueue: Boolean;
begin
  Result := QueueExt.RequestsExist(ADR_Srv_FullDBCopy, [arsQueued, arsExecuting], DB_TempTables);
end;

procedure TevADRRebuildTempTables.Run;
var
  s: String;
  emailSentTime: TDateTime;
begin
  if not FAllClientsMode then
  begin
    // Check first that temp tables are available
    emailSentTime := 0;
    while not CurrentThreadTaskTerminated do
    begin
      try
        ctx_DBAccess.GetDBVersion(dbtTemporary);
        break;
      except
        on E: Exception do
        begin
          if RestoreTTInQueue then
            Exit;

          if MinutesBetween(emailSentTime, Now) > 10 then
          begin
            (QueueObj as IevADRRequestQueueExt).ADRModule.SendAlertEmail('ADR at critical state',
            'A potential never-ending request is detected. ' +  'Temp tables database is not available.'#13#13 + E.Message);
            emailSentTime := Now;
          end;

          QueueExt.ADRModule.LogError('Temp tables database is not available. Trying again...', MakeFullError(E));
          Snooze(30000);
        end;
      end;
    end;
  end;

  s := AnsiUpperCase(Database);
  GetNextStrValue(s, AnsiUpperCase(DB_Cl));

  emailSentTime := 0;
  while not CurrentThreadTaskTerminated do
  begin
    try
      ctx_DBAccess.RebuildTemporaryTables(StrToInt(s), FAllClientsMode);
      QueueExt.ADRModule.LogDebugEvent('Temp tables for ' + Database + ' have been rebuilt');
      break;
    except
      on E: EBrokenDatabase do
      begin
        // in case of deleted client
        QueueExt.ADRModule.LogError(E.Message, MakeFullError(E));
        break;
      end;

      on E: Exception do
      begin
        if Contains(E.Message, 'Database was forcedly disconnected') then
        begin
          if MinutesBetween(emailSentTime, Now) > 10 then
          begin
            (QueueObj as IevADRRequestQueueExt).ADRModule.SendAlertEmail('ADR at critical state',
            'A potential never-ending request is detected. ' + Database + ' was disconnected.'#13#13 + E.Message);
            emailSentTime := Now;
          end;

          QueueExt.ADRModule.LogError(Database + ' was disconnected. Trying again...', MakeFullError(E));
          Snooze(30000);
        end
        else
        begin
          if not CurrentThreadTaskTerminated then
          begin
            QueueExt.ADRModule.LogError(Database + ' rebuild TT failed', MakeFullError(E));
            if not FAllClientsMode then
              RequestFullDBCopy;
            break;
          end
          else
            raise;
        end;
      end;
    end;
  end;
end;

procedure TevADRRebuildTempTables.Store;
begin
  inherited;
  Storage.AsBoolean[GetID + '\AllClientsMode'] := FAllClientsMode;
end;

{ TevADRApplySyncDataRequest }

function TevADRApplySyncDataRequest.Caption: String;
begin
  Result := GetApplySyncDataRequestName(Database);
end;

constructor TevADRApplySyncDataRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FData := IInterface(AParams[3]) as IisStream;
end;

function TevADRApplySyncDataRequest.GetData: IisStream;
begin
  if not Assigned(FData) then
    FData := Storage.AsBlob[GetID + '\Data'];
  Result := FData;
end;

class function TevADRApplySyncDataRequest.GetTypeID: String;
begin
  Result := ADR_Srv_SyncDBData;
end;

function TevADRApplySyncDataRequest.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
    if IsClientDB(Database) then
      Result := (AExistingRequest.TypeID = ADR_Srv_SyncDBList) or
                IsTempDB(AExistingRequest.Database) or
                (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT);
end;

procedure TevADRApplySyncDataRequest.RemoveFromStorage;
begin
  FData := nil; // to close file
  inherited;
end;

procedure TevADRApplySyncDataRequest.Run;
var
  DBType: TevDBType;
  Ver: String;
begin
  inherited;

  DBType := OpenDB(Database);

  // Check first that database is available
  while not CurrentThreadTaskTerminated do
  begin
    try
      Ver := ctx_DBAccess.GetDBVersion(DBType);
      break;
    except
      on E: EMaintenance do
      begin
        QueueExt.ADRModule.LogError(Database + ': ' + E.Message + ' Trying again...', MakeFullError(E));
        Snooze(30000);
      end
      else
        raise;
    end;
  end;

  SetProgress('Starting sync data applying');
  try
    CheckCondition(Ver = DBVersion, 'DB version mismatch!');

    ctx_DBAccess.ApplyDataSyncPacket(DBType, GetData);
    (Mainboard.ADRServer as IevADRServerExt).DBOfflineStatuses.ClearDBInfo(Database);
    QueueExt.ADRModule.LogDebugEvent(Database + ' has been syncronized');
  except
    on E: Exception do
      if not CurrentThreadTaskTerminated then
      begin
        QueueExt.ADRModule.LogError(Database + ' data synchronization failed', MakeFullError(E));
        RequestFullDBCopy;
      end
      else
        raise;
  end;
end;

procedure TevADRApplySyncDataRequest.Store;
begin
  inherited;
  Storage.AsBlob[GetID + '\Data'] := GetData;
  FData := nil;  // to close file
end;

{ TevADRDBDataCheckRequest }

function TevADRDBDataCheckRequest.Caption: String;
begin
  Result := GetDBCompareRequestName(Database);
end;

constructor TevADRDBDataCheckRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FData := IInterface(AParams[3]) as IisStream;
end;

function TevADRDBDataCheckRequest.GetData: IisStream;
begin
  if not Assigned(FData) then
    FData := Storage.AsBlob[GetID + '\Data'];
  Result := FData;
end;

class function TevADRDBDataCheckRequest.GetTypeID: String;
begin
  Result := ADR_Srv_CheckDBData;
end;

procedure TevADRDBDataCheckRequest.RemoveFromStorage;
begin
  FData := nil; // to close file
  inherited;
end;

procedure TevADRDBDataCheckRequest.Run;
var
  sErrors: String;
  DBInfo: IevADRDBInfo;
begin
  inherited;
  SetProgress('Initial DB check');

  try
    // Check first that database is available
    while not CurrentThreadTaskTerminated do
    begin
      DBInfo := (Mainboard.ADRServer as IevADRServerExt).GetCommitedDBInfo(Database);
      if DBInfo.Status = dsMaintenance then
      begin
        QueueExt.ADRModule.LogError(Format('Database %s is in maintenance mode. Trying Again...', [Database]));
        Snooze(30000);
      end
      else
        break;
    end;

    ErrorIf(DBInfo.Status in [dsUnknown, dsAbsent], 'Target DB is missing');
    CheckCondition(DBInfo.Version = DBVersion, 'Source and target DBs version mismatch');
    CheckCondition(DBInfo.LastTransactionNbr = LastTransactionNbr, 'Source and target DBs transactions mismatch');

    ctx_DBAccess.CheckDBHash(OpenDB(Database), GetData, LastTransactionNbr, sErrors);
    if sErrors = '' then
      QueueExt.ADRModule.LogDebugEvent('DB ' + Database + ' compared successfully')
    else
      raise EisException.Create(sErrors);
  except
    on E: Exception do
      if not CurrentThreadTaskTerminated then
      begin
        QueueExt.ADRModule.LogError(Database + ' data check failed', MakeFullError(E));
        RequestFullDBCopy;
      end
      else
        raise;
  end;
end;

procedure TevADRDBDataCheckRequest.Store;
begin
  inherited;
  Storage.AsBlob[GetID + '\Data'] := GetData;
  FData := nil;  // to close file
end;

{ TevServerADRDBRequest }

procedure TevServerADRDBRequest.BeforeRun;
begin
  inherited;
  ctx_DBAccess.SetADRContext(True);
end;

function TevServerADRDBRequest.DeleteOnError: Boolean;
begin
  Result := Database = '';
end;

procedure TevServerADRDBRequest.RequestFullDBCopy;

  procedure RequestDBFromClient(const AParams: TTaskParamList);
  var
    QueueExt: IevADRRequestQueueExt;
    Database: String;
    OriginatedAt: TUTCDateTime;
  begin
    try
      OriginatedAt:= AParams[0];
      QueueExt := IInterface(AParams[1]) as IevADRRequestQueueExt;
      Database := AParams[2];
      Mainboard.ADRClient.FullDBCopy(OriginatedAt, Database, True, False);
      QueueExt.ADRModule.LogWarning('Full copy ' + Database + ' was requested from client side.');
    except
      on E: Exception do
        QueueExt.ADRModule.LogError(E.Message, MakeFullError(E));
    end;
  end;

begin
  // Delete all queued requests for this database because they are useless
  QueueExt.ADRModule.RequestQueue.DeleteDBRequests(Database, '', [arsQueued]);

  // This is important to run Mainboard.ADRClient.FullDBCopy(Database) in a separate thread
  // Because client side immediately calls DeleteRequest for this database
  // so this remote request would fail if it called right from ADR Request thread

  Context.RunParallelTask(@RequestDBFromClient, MakeTaskParams([OriginatedAt, QueueExt, Database]), 'RequestDBFromClient');
end;

{ TevADRSweepDBRequest }

function TevADRSweepDBRequest.Caption: String;
begin
  Result := GetSweepDBRequestName(Database);
end;

function TevADRSweepDBRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

class function TevADRSweepDBRequest.GetTypeID: String;
begin
  Result := ADR_Srv_SweepDB;
end;

function TevADRSweepDBRequest.IsBlockingExecutionRequest(const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
    if IsTempDB(Database) then
      Result := (AExistingRequest.TypeID = ADR_Srv_SyncDBList) or
                (AExistingRequest.TypeID = ADR_Srv_RebuildTT) or
                (AExistingRequest.TypeID = ADR_Srv_RebuildAllTT) or
                (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT) or
                (AExistingRequest.TypeID = ADR_Srv_SyncDBData);
end;

procedure TevADRSweepDBRequest.Run;
begin
  inherited;

  try
    // Restore
    SetProgress('Sweeping database');
    ctx_DBAccess.SweepDB(Database);
    (Mainboard.ADRServer as IevADRServerExt).DBOfflineStatuses.ClearDBInfo(Database);
    QueueExt.ADRModule.LogDebugEvent(Database + ' has been swept');
  finally
    SetProgress('');
  end;
end;

{ TevADREnlistSweepDBRequest }

function TevADREnlistSweepDBRequest.Caption: String;
begin
  Result := 'Enlist Sweep DB';
end;

function TevADREnlistSweepDBRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

class function TevADREnlistSweepDBRequest.GetTypeID: String;
begin
  Result := ADR_Srv_EnlistSweepDB;
end;

procedure TevADREnlistSweepDBRequest.Run;
var
  i: Integer;
  EvoDomain: IevDomainInfo;
begin
  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      try
        EvoDomain := mb_GlobalSettings.DomainInfoList[i];
        if mb_AppConfiguration.AsBoolean['ADRServer\Domains\' + EvoDomain.DomainName + '\Active'] then
        begin
          Mainboard.ContextManager.CreateThreadContext(
            EncodeUserAtDomain(sADRUserName, EvoDomain.DomainName),
            EvoDomain.SystemAccountInfo.Password);

          (QueueObj as IevADRRequestQueueExt).ADRModule.AddDBRequestsIfMissing(NowUTC, ADR_Srv_SweepDB, [DB_TempTables, '', 0], True);
        end;
      except
        on E: Exception do
          (QueueObj as IevADRRequestQueueExt).ADRModule.LogError('Cannot schedule "Sweep DB" for temp tables', MakeFullError(E));
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

    AbortIfCurrentThreadTaskTerminated;
  end;
end;

{ TevADRDBTransCheckRequest }

function TevADRDBTransCheckRequest.Caption: String;
begin
  Result := GetDBTransCheckRequestName(Database);
end;

constructor TevADRDBTransCheckRequest.Create(
  const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FTransCount := AParams[3];
end;

class function TevADRDBTransCheckRequest.GetTypeID: String;
begin
  Result := ADR_Srv_CheckDBTrans;
end;

procedure TevADRDBTransCheckRequest.Restore;
begin
  inherited;
  FTransCount := Storage.AsInteger[GetID + '\TransCount'];
end;

procedure TevADRDBTransCheckRequest.Run;
var
  DBInfo: IevADRDBInfo;
  TransStatus: IisListOfValues;
begin
  inherited;
  SetProgress('Initial DB check');

  try
    // Check first that database is available
    while not CurrentThreadTaskTerminated do
    begin
      DBInfo := (Mainboard.ADRServer as IevADRServerExt).GetCommitedDBInfo(Database);
      if DBInfo.Status = dsMaintenance then
      begin
        QueueExt.ADRModule.LogError(Format('Database %s is in maintenance mode. Trying Again...', [Database]));
        Snooze(30000);
      end
      else
        break;
    end;

    ErrorIf(DBInfo.Status in [dsUnknown, dsAbsent], 'Target DB is missing');
    CheckCondition(DBInfo.Version = DBVersion, 'Source and target DBs version mismatch');
    CheckCondition(DBInfo.LastTransactionNbr = LastTransactionNbr, 'Source and target DBs transactions mismatch');

    TransStatus := ctx_DBAccess.GetTransactionsStatus(OpenDB(Database));

    if TransStatus.Value['TransCount'] = FTransCount then
      QueueExt.ADRModule.LogDebugEvent('Transactions of ' + Database + ' checked')
    else
      raise EisException.Create('Source and target DBs transactions count mismatch');
  except
    on E: Exception do
      if not CurrentThreadTaskTerminated then
      begin
        QueueExt.ADRModule.LogError(Database + ' transaction check failed', MakeFullError(E));
        RequestFullDBCopy;
      end
      else
        raise;
  end;
end;

procedure TevADRDBTransCheckRequest.Store;
begin
  inherited;
  Storage.AsInteger[GetID + '\TransCount'] := FTransCount;
end;

{ TevADRFinalizeRebuildTempTablesForAll }

function TevADRFinalizeRebuildTempTablesForAll.Caption: String;
begin
  Result := GetFinalizeRebuildTmpTblsForAllRequestName;
end;

class function TevADRFinalizeRebuildTempTablesForAll.GetTypeID: String;
begin
  Result := ADR_Srv_FinalizeRebuildAllTT;
end;

function TevADRFinalizeRebuildTempTablesForAll.IsBlockingExecutionRequest(
  const AExistingRequest: IevADRDBRequest): Boolean;
begin
  Result := inherited IsBlockingExecutionRequest(AExistingRequest);

  if not Result then
    Result := (AExistingRequest.TypeID = ADR_Srv_RebuildAllTT) or
              (AExistingRequest.TypeID = ADR_Srv_FinalizeRebuildAllTT) or
              (AExistingRequest.TypeID = ADR_Srv_RebuildTT) or
              (AExistingRequest.TypeID = ADR_Srv_SweepDB) and IsTempDB(AExistingRequest.Database);
end;

procedure TevADRFinalizeRebuildTempTablesForAll.Run;
begin
  inherited;

  try
    ctx_DBAccess.EndRebuildAllClients;
  except
    on E: Exception do
      QueueExt.ADRModule.LogError('Finalizing rebuilding temp tables had errors', MakeFullError(E));
  end;

  QueueExt.ADRModule.LogDebugEvent('Temp tables rebuild for all clients has been finalized');

  (Mainboard.ADRServer as IevADRServerExt).DBOfflineStatuses.ClearDBInfo(DB_TempTables);
end;

initialization
  ObjectFactory.Register([TevADRFullDBCopyRequest, TevADRSyncDBListRequest, TevADRRebuildTempTablesForAll,
                          TevADRRebuildTempTables, TevADRApplySyncDataRequest, TevADRDBDataCheckRequest,
                          TevADRSweepDBRequest, TevADREnlistSweepDBRequest, TevADRDBTransCheckRequest,
                          TevADRFinalizeRebuildTempTablesForAll]);

finalization
  SafeObjectFactoryUnRegister([TevADRFullDBCopyRequest, TevADRSyncDBListRequest, TevADRRebuildTempTablesForAll,
                            TevADRRebuildTempTables, TevADRApplySyncDataRequest, TevADRDBDataCheckRequest,
                            TevADRSweepDBRequest, TevADREnlistSweepDBRequest, TevADRDBTransCheckRequest,
                            TevADRFinalizeRebuildTempTablesForAll]);

end.
