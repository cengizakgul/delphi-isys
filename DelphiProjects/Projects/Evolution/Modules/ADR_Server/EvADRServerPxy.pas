unit EvADRServerPxy;

interface

uses SysUtils,
     isBaseClasses, evCommonInterfaces, evTransportDatagrams, evTransportInterfaces,
     evTransportShared, EvStreamUtils, EvMainboard, EvContext, evConsts, evRemotemethods,
     evExceptions, evADRRequestQueue, isBasicUtils, evADRCommon, isThreadManager,
     EvClasses, isSettings, isLogFile, isErrorUtils, evBasicUtils, SEncryptionRoutines;

implementation

uses EvADRClientMod, Classes;

const  Request_Evo_Trans = 'Send Data';

type
  IevADRTCPClient = interface(IisTCPClient)
  ['{144D45C1-D045-4BE2-A425-C2F0E136D090}']
    function  Connected: Boolean;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  DomainOfSession(const ASessionID: TisGUID): String;
  end;

  IevADRServerExt = interface(IevADRServer)
  ['{4AFDF0E4-F595-479F-8692-A50437ADB1AC}']
    function  CreateDatagram(const AMethod: String): IevDatagram;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  ADRTCPClient: IevADRTCPClient;
    function  GetCommitedDBInfo(const ADatabase: String): IevADRDBInfo;
  end;


  TevADRServerProxy = class(TevCustomADRModule, IevProxyModule, IevADRServer, IevADRServerExt, IisTCPClient)
  private
    FTCPClient: IevADRTCPClient;
    FDBOfflineStatuses: IevDBStatusStorage;

    function  GetDBInfoFromTransQueue(const ADatabase: String): IevADRDBInfo;
    procedure SendQueuedRequest(const AMethod: String; const ADatagram: IevDatagram);
    function  GetCommitedDBInfo(const ADatabase: String): IevADRDBInfo;
  protected
    procedure DoOnConstruction; override;
    function  GetWorkingDir: String; override;
    procedure SetActive(const AValue: Boolean); override;
    property  TCPClient: IevADRTCPClient read FTCPClient implements IisTCPClient;

    function  Connected: Boolean;
    procedure OnDisconnect;
    function  CreateDatagram(const AMethod: String): IevDatagram;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  ADRTCPClient: IevADRTCPClient;

    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    procedure SyncDBList(const ADBList: IisStringList);
    procedure RestoreDB(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer;
                        const ADBData: IisStream);
    procedure ApplySyncData(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer; const ASyncData: IisStream);
    procedure CheckDBData(const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer; const ADBData: IisStream);
    procedure CheckDBTransactions(const ADataBase, ADBVersion: String; const ALastTransactionNbr, ATransCount: Integer);
    procedure DeleteDBRequests(const ADatabase: String);
    function  GetStatus: IisListOfValues;
    procedure RebuildTT(const AOriginatedAt:TUTCDateTime; const ADatabase: String);
  public
    class function GetTypeID: String; override;
  end;


  // AC  -> AS connections
  TevADRTCPClient = class(TevCustomTCPClient, IevADRTCPClient)
  private
    FSessionIDs: IisListOfValues;
    FConnectionWDTaskID: TTaskID;
    procedure Connect(const ADomain: String);
    function  GetSession(const ADomain: String): IevSession;
    function  GetValidSession(const ADomain: String): IevSession;
    procedure ConnectionWatchDog(const Params: TTaskParamList);
    procedure SyncOfflineStatus(const ADomain: String);
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  DomainOfSession(const ASessionID: TisGUID): String;
    function  Connected: Boolean;
  end;


  // AS -> AC dispatcher
  TevADRClientDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmGetStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmFullDBCopy(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetDBList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDBData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDBTransactions(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure RegisterHandlers; override;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;
  end;



  TevADRTransRequest = class(TevADRDBRequest, IevADRTransRequest)
  private
    FData: IisStream;
    FDataSize: Integer;
    FReceipt: TisGUID;
    FMethod: String;
    function  GetData: IisStream;
    function  IsOversizedData: Boolean;
  protected
    procedure Restore; override;
    procedure Store; override;
    procedure RemoveFromStorage; override;
    function  ReadyToRun: Boolean; override;

    function  ADRRequestID: String;
    function  Caption: String; override;
    procedure Run; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


function GetADRServer: IevADRServer;
begin
  Result := TevADRServerProxy.Create;
end;


{ TevADRServerProxy }

function TevADRServerProxy.ADRTCPClient: IevADRTCPClient;
begin
  Result := FTCPClient;
end;

procedure TevADRServerProxy.ApplySyncData(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String;
  const ALastTransactionNbr: Integer; const ASyncData: IisStream);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_APPLYSYNCDATA);
  D.Params.AddValue('AOriginatedAt', AOriginatedAt);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ADBVersion', ADBVersion);
  D.Params.AddValue('ALastTransactionNbr', ALastTransactionNbr);
  D.Params.AddValue('ASyncData', ASyncData);
  SendQueuedRequest('Sync DB', D);
end;

procedure TevADRServerProxy.CheckDBData(const ADatabase, ADBVersion: String;
  const ALastTransactionNbr: Integer; const ADBData: IisStream);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_CHECKDBDATA);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ADBVersion', ADBVersion);
  D.Params.AddValue('ALastTransactionNbr', ALastTransactionNbr);
  D.Params.AddValue('ADBData', ADBData);
  SendQueuedRequest('Check DB', D);
end;

procedure TevADRServerProxy.CheckDBTransactions(const ADataBase,
  ADBVersion: String; const ALastTransactionNbr, ATransCount: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_CHECKDBTRANSACTIONS);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ADBVersion', ADBVersion);
  D.Params.AddValue('ALastTransactionNbr', ALastTransactionNbr);
  D.Params.AddValue('ATransCount', ATransCount);
  SendQueuedRequest('Check DB', D);
end;

function TevADRServerProxy.Connected: Boolean;
begin
  Result := FTCPClient.Connected;
end;

function TevADRServerProxy.CreateDatagram(const AMethod: String): IevDatagram;
begin
  Result := TevDatagram.Create(AMethod);
end;

procedure TevADRServerProxy.DeleteDBRequests(const ADatabase: String);
var
  D: IevDatagram;
begin
  RequestQueue.DeleteDBRequests(ADatabase);

  D := CreateDatagram(METHOD_AS_DELETEDBREQUESTS);
  D.Params.AddValue('ADatabase', ADatabase);

  try
    D := SendRequest(D);
  except
    on E: EevTransport do
    else
      raise
  end;
end;

procedure TevADRServerProxy.DoOnConstruction;
begin
  inherited;
  FDBOfflineStatuses := TevDBStatusStorage.Create(GetWorkingDir + 'ServerStatus');
  FTCPClient := TevADRTCPClient.Create;
end;

function TevADRServerProxy.GetCommitedDBInfo(const ADatabase: String): IevADRDBInfo;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_GETDBINFO);
  D.Params.AddValue('ADatabase', ADatabase);
  try
    D := SendRequest(D);
    Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevADRDBInfo;
    FDBOfflineStatuses.SetDBInfo(Result);
  except
    on E: EevTransport do
    else
      raise;
  end;
end;

function TevADRServerProxy.GetDBInfo(const ADatabase: String): IevADRDBInfo;
begin
  Result := GetDBInfoFromTransQueue(ADatabase);

  if Result.Status = dsUnknown then
    GetCommitedDBInfo(ADatabase);

  if Result.Status in [dsUnknown, dsMaintenance] then
    Result := FDBOfflineStatuses.GetDBInfo(ADatabase);
end;

function TevADRServerProxy.GetDBInfoFromTransQueue(const ADatabase: String): IevADRDBInfo;
var
  R: IevADRDBRequest;
  RList: IisList;
  i: Integer;
begin
  Result := TevADRDBInfo.Create(ADatabase);

  // Check requests
  RList := RequestQueue.GetRequests('', [arsQueued, arsExecuting], ADatabase);
  for i := 0 to RList.Count - 1 do
  begin
    R := RList[i] as IevADRDBRequest;
    if Result.LastTransactionNbr <= R.LastTransactionNbr then
    begin
      Result.Version := R.DBVersion;
      Result.LastTransactionNbr := R.LastTransactionNbr;
      Result.Status := dsPreparing;
    end;
  end;
end;

function TevADRServerProxy.GetStatus: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_GETSTATUS);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

class function TevADRServerProxy.GetTypeID: String;
begin
  Result := 'ADR Communicator';
end;

function TevADRServerProxy.GetWorkingDir: String;
begin
  Result := NormalizePath(mb_AppConfiguration.GetValue('ADRClient\StagingFolder', AppDir + 'ADR')) + 'ClientTrans\';
end;

procedure TevADRServerProxy.OnDisconnect;
begin
end;

procedure TevADRServerProxy.RebuildTT(const AOriginatedAt: TUTCDateTime; const ADatabase: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_REBUILDTT);
  D.Params.AddValue('AOriginatedAt', AOriginatedAt);
  D.Params.AddValue('ADatabase', ADatabase);
  D := SendRequest(D);
end;

procedure TevADRServerProxy.RestoreDB(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String;
  const ALastTransactionNbr: Integer; const ADBData: IisStream);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_RESTOREDB);
  D.Params.AddValue('AOriginatedAt', AOriginatedAt);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ADBVersion', ADBVersion);
  D.Params.AddValue('ALastTransactionNbr', ALastTransactionNbr);
  D.Params.AddValue('ADBData', ADBData);
  SendQueuedRequest('Full DB Copy', D);
end;

procedure TevADRServerProxy.SendQueuedRequest(const AMethod: String; const ADatagram: IevDatagram);
var
  S: IisStream;
begin
  S := TisStream.Create;
  (ADatagram as IisInterfacedObject).WriteToStream(S);

  RequestQueue.NewDBRequest(ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC), Request_Evo_Trans,
    [ADatagram.Params.TryGetValue('ADatabase', ''),
     ADatagram.Params.TryGetValue('ADBVersion', ''),
     ADatagram.Params.TryGetValue('ALastTransactionNbr', 0),
     AMethod,
     S]);
end;

function TevADRServerProxy.SendRequest(const ADatagram: IevDatagram): IevDatagram;
begin
  Result := FTCPClient.SendRequest(ADatagram);
end;

procedure TevADRServerProxy.SetActive(const AValue: Boolean);
begin
  inherited;
  RequestQueue.ExecSlots := 5;
end;

procedure TevADRServerProxy.SyncDBList(const ADBList: IisStringList);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_AS_SYNCDBLIST);
  D.Params.AddValue('ADBList', ADBList);
  SendQueuedRequest('Sync DB List', D);
end;

{ TevADRTCPClient }

procedure TevADRTCPClient.Connect(const ADomain: String);
var
  SessionID: TisGUID;
  sHost, sPort, sUser, sPassword: String;
begin
  sHost := mb_AppConfiguration.AsString['ADRClient\Domains\' + ADomain + '\Host'];
  sPort := mb_AppConfiguration.AsString['ADRClient\Domains\' + ADomain + '\Port'];

  sUser := mb_AppConfiguration.AsString['ADRClient\Domains\' + ADomain + '\UserName'];
  sUser := DecryptStringLocaly(BinToStr(sUser), '');

  sPassword := mb_AppConfiguration.AsString['ADRClient\Domains\' + ADomain + '\Password'];
  sPassword := DecryptStringLocaly(BinToStr(sPassword), '');

  SessionID := CreateConnection(sHost, sPort, sUser, sPassword, '', '');

  FSessionIDs.AddValue(ADomain, SessionID);
end;

function TevADRTCPClient.Connected: Boolean;
var
  S: IevSession;
begin
  S := GetSession(ctx_DomainInfo.DomainName);
  Result := Assigned(S) and (S.State = ssActive);
end;

procedure TevADRTCPClient.ConnectionWatchDog(const Params: TTaskParamList);
var
  i: Integer;
  Domains: IisStringList;
begin
  Domains := TisStringList.Create;

  while not CurrentThreadTaskTerminated do
  begin
    Snooze(30000);

    mb_GlobalSettings.DomainInfoList.Lock;
    try
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
        if mb_GlobalSettings.DomainInfoList[i].DBChangesBroadcast and
           mb_AppConfiguration.AsBoolean['ADRClient\Domains\' + mb_GlobalSettings.DomainInfoList[i].DomainName + '\Active'] then
          Domains.Add(mb_GlobalSettings.DomainInfoList[i].DomainName); 
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;

    for i := 0 to Domains.Count - 1 do
    begin
      AbortIfCurrentThreadTaskTerminated;
      try
        GetValidSession(Domains[i]);
      except
        on E: EevTransport do ;
        on E: Exception do
          mb_LogFile.AddEvent(etError, 'ADR', E.Message, GetStack(E));
      end;
     end;
    Domains.Clear;
  end;
end;

function TevADRTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevADRClientDispatcher.Create;
end;

procedure TevADRTCPClient.DoOnConstruction;
begin
  inherited;
  FSSLCertFile := AppDir + 'cert.pem';
  FSessionIDs := TisListOfValues.Create;
  FConnectionWDTaskID := GlobalThreadManager.RunTask(ConnectionWatchDog, Self, MakeTaskParams([]), 'ADR Connection WD');
end;

procedure TevADRTCPClient.DoOnDestruction;
begin
  if FConnectionWDTaskID <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FConnectionWDTaskID);
    GlobalThreadManager.WaitForTaskEnd(FConnectionWDTaskID);
  end;
    
  inherited;
end;

function TevADRTCPClient.GetValidSession(const ADomain: String): IevSession;
var
  bNewSession: Boolean;
begin
  Lock;
  try
    bNewSession := False;
    Result := GetSession(ADomain);
    if not Assigned(Result) or (Result.State <> ssActive) then
    begin
      if Assigned(Result) and (Result.State <> ssInactive) then
        Disconnect(Result.SessionID);

      Connect(ADomain);

      Result := GetSession(ADomain);
      bNewSession := Assigned(Result);
    end;
  finally
    UnLock;
  end;

  if bNewSession then
    SyncOfflineStatus(ADomain);
end;

function TevADRTCPClient.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
begin
  Lock;
  try
    S := GetSession(ctx_DomainInfo.DomainName);
    if not Assigned(S) then
      S := GetValidSession(ctx_DomainInfo.DomainName);
  finally
    Unlock;
  end;

  ADatagram.Header.SessionID := S.SessionID;
  ADatagram.Header.User := S.Login;
  ADatagram.Header.Password := S.Password;  

  Result := S.SendRequest(ADatagram);
end;

function TevADRTCPClient.GetSession(const ADomain: String): IevSession;
var
  SessionID: TisGUID;
begin
  Result := nil;
  Lock;
  try
    SessionID := FSessionIDs.TryGetValue(ADomain, '');
    if SessionID <> '' then
    begin
      Result := FindSession(SessionID);
      if not Assigned(Result) then
        FSessionIDs.AddValue(ADomain, '');
    end;
  finally
    Unlock;
  end;
end;

procedure TevADRTCPClient.SyncOfflineStatus(const ADomain: String);

  procedure ProcessResults(const AParams: TTaskParamList);
  var
    D: IevDatagram;
    FullCopyDBs: IisStringList;
    ErroredDBList: IisListOfValues;
    i: Integer;
  begin
    D := IInterface(AParams[0]) as IevDatagram;

    // TODO: use only ErroredDBList starting "P" release
    if D.Params.ValueExists('ErroredDBList') then
    begin
      ErroredDBList := IInterface(D.Params.Value['ErroredDBList']) as IisListOfValues;
      for i := 0 to ErroredDBList.Count - 1 do
        Mainboard.ADRClient.FullDBCopy(ErroredDBList[i].Value, ErroredDBList[i].Name);
    end

    else
    begin
      FullCopyDBs := IInterface(D.Params.Value['FullDBCopyList']) as IisStringList;
      for i := 0 to FullCopyDBs.Count - 1 do
        Mainboard.ADRClient.FullDBCopy(NowUTC, FullCopyDBs[i]);
    end;
  end;

var
  D: IevDatagram;
  DomainInfo: IevDomainInfo;  
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomain);

    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sADRUserName, DomainInfo.DomainName),
      DomainInfo.SystemAccountInfo.Password);

    D := (Mainboard.ADRServer as IevADRServerExt).CreateDatagram(METHOD_AS_SYNCOFFLINESTATUS);
    D := SendRequest(D);

    // Result should be processed in different thread to avoid deadlock
    Context.RunParallelTask(@ProcessResults, MakeTaskParams([D]), 'SyncOfflineStatus');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevADRTCPClient.DomainOfSession(const ASessionID: TisGUID): String;
var
  i: Integer;
begin
  Result := '';
  Lock;
  try
    for i := 0 to FSessionIDs.Count - 1 do
      if AnsiSameStr(FSessionIDs[i].Value, ASessionID) then
      begin
        Result := FSessionIDs[i].Name;
        break;
      end;
  finally
    Unlock;
  end;
end;

{ TevADRTransRequest }

function TevADRTransRequest.ADRRequestID: String;
begin
  Result := FMethod;
end;

function TevADRTransRequest.Caption: String;
begin
  Result := FMethod;
  if Database <> '' then
    Result := Result + ': ' + Database;
end;

constructor TevADRTransRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FMethod := AParams[3];
  FData := IInterface(AParams[4]) as IisStream;
  FDataSize := FData.Size;
end;

function TevADRTransRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

function TevADRTransRequest.GetData: IisStream;
begin
  if not Assigned(FData) then
    if IsOversizedData then
      FData := Storage.AsBlob[GetID + '\Data']
    else
    begin
      FData := TisStream.Create;
      FData.AsString := BinToStr(Storage.AsString[GetID + '\Data']);
    end;

  Result := FData;
end;

class function TevADRTransRequest.GetTypeID: String;
begin
  Result := Request_Evo_Trans;
end;

procedure TevADRTransRequest.Restore;
begin
  inherited;
  FReceipt := Storage.AsString[GetID + '\Receipt'];
  FMethod := Storage.AsString[GetID + '\Method'];
  FDataSize := Storage.AsInteger[GetID + '\DataSize'];  
end;

function TevADRTransRequest.ReadyToRun: Boolean;
var
  ExecRequests: IisList;
begin
  // one uploading request per Domain
  ExecRequests := QueueExt.QueryRequests(Domain, [arsExecuting]);
  Result :=  ExecRequests.Count = 0;
end;

procedure TevADRTransRequest.RemoveFromStorage;
begin
  FData := nil; // to close file
  inherited;
end;

procedure TevADRTransRequest.Run;

  procedure StartTrans;
  var
    D: IevDatagram;
  begin
    D := (Mainboard.ADRServer as IevADRServerExt).CreateDatagram(METHOD_AS_STARTTRANS);
    D.Params.AddValue('OriginatedAt', OriginatedAt);
    D.Params.AddValue('Database', Database);
    D.Params.AddValue('DBVersion', DBVersion);
    D.Params.AddValue('LastTransactionNbr', LastTransactionNbr);
    D.Params.AddValue('Method', FMethod);    
    D.Params.AddValue('DataSize', GetData.Size);
    D := (Mainboard.ADRServer as IevADRServerExt).SendRequest(D);

    FReceipt :=  D.Params.Value[METHOD_RESULT];
    Storage.AsString[GetID + '\Receipt'] :=  FReceipt;
  end;


  function GetTransPosition: Integer;
  var
    D: IevDatagram;
  begin
    D := (Mainboard.ADRServer as IevADRServerExt).CreateDatagram(METHOD_AS_GETTRANSPOSITION);
    D.Params.AddValue('Receipt', FReceipt);
    D := (Mainboard.ADRServer as IevADRServerExt).SendRequest(D);
    Result := D.Params.Value[METHOD_RESULT];
  end;
  

  procedure SendFargment;
  var
    BlockSize: Integer;
    Fragment: IisStream;
    D: IevDatagram;
  begin
    BlockSize := ADR_TransBlockSize;
    if GetData.Position + BlockSize > GetData.Size then
      BlockSize := GetData.Size - GetData.Position;

    Fragment := TisStream.Create(BlockSize);
    Fragment.CopyFrom(GetData, BlockSize);

    D := (Mainboard.ADRServer as IevADRServerExt).CreateDatagram(METHOD_AS_TRANSFRAGMENT);
    D.Params.AddValue('Receipt', FReceipt);
    D.Params.AddValue('Fragment', Fragment);
    (Mainboard.ADRServer as IevADRServerExt).SendRequest(D);
  end;

var
  TransPos: Integer;
begin
  inherited;

  QueueExt.ADRModule.Statistics.SetCounter('LastActivity', Caption);

  while not CurrentThreadTaskTerminated do
    try
      SetProgress('Checking status on server');

      if FReceipt = '' then
        TransPos := -1
      else
        TransPos := GetTransPosition;

      if TransPos = -1 then
      begin
        StartTrans;
        TransPos := 0;
      end;
      GetData.Position := TransPos;

      while not CurrentThreadTaskTerminated and not GetData.EOS do
      begin
        SendFargment;
        SetProgress(Format('Transmited %s  (%s of %s)',
          [IntToStr(Round(GetData.Position / GetData.Size * 100)) + '%',
           SizeToString(GetData.Position), SizeToString(GetData.Size)]));
      end;

      if Database <> '' then
        (Mainboard.ADRServer as IevADRServerExt).GetCommitedDBInfo(Database); // Will refresh cache
        
      SetProgress('');

      break;
    except
      on E: EevTransport do
      begin
        (Mainboard.ADRClient as IevADRModule).LogError('Communication problem', E.Message);
        Snooze(10000);
      end;

      on E: EADRMissingRequest do
      begin
       (Mainboard.ADRClient as IevADRModule).LogError('Restart transmission', E.Message);
        FReceipt := '';
      end

      else
        raise;
    end;
end;

procedure TevADRTransRequest.Store;
begin
  inherited;
  Storage.AsString[GetID + '\Receipt'] :=  FReceipt;
  Storage.AsString[GetID + '\Method'] := FMethod;
  Storage.AsInteger[GetID + '\DataSize'] := FDataSize;

  if IsOversizedData then
    Storage.AsBlob[GetID + '\Data'] := GetData
  else
    Storage.AsString[GetID + '\Data'] := StrToBin(GetData.AsString);

  FData := nil;  // to close file
end;

function TevADRTransRequest.IsOversizedData: Boolean;
begin
  Result := FDataSize > ADR_TransBlockSize;
end;

{ TevADRClientDispatcher }

procedure TevADRClientDispatcher.AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram);
begin
  Mainboard.ContextManager.RestoreThreadContext;
  inherited;
end;

procedure TevADRClientDispatcher.BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader);
var
  DomainInfo: IevDomainInfo;
  Session: IevSession;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;

  Session := FindSessionByID(ADatagramHeader.SessionID);
  if (Session.State = ssActive) and not IsTransportDatagram(ADatagramHeader) then
  begin
    DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(
      (Mainboard.ADRServer as IevADRServerExt).ADRTCPClient.DomainOfSession(Session.SessionID));

    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sADRUserName, DomainInfo.DomainName),
      DomainInfo.SystemAccountInfo.Password, '', ADatagramHeader.ContextID);
    ctx_DBAccess.SetADRContext(False);
  end;
end;

procedure TevADRClientDispatcher.dmCheckDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRClient.CheckDB(
    ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC),
    ADatagram.Params.Value['ADatabase'],
    ADatagram.Params.Value['ATopPosition']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevADRClientDispatcher.dmCheckDBData(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRClient.CheckDBData(ADatagram.Params.Value['ADatabase'], ADatagram.Params.Value['ATopPosition']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevADRClientDispatcher.dmCheckDBTransactions(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRClient.CheckDBTransactions(ADatagram.Params.Value['ADatabase'], ADatagram.Params.Value['ATopPosition']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevADRClientDispatcher.dmFullDBCopy(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRClient.FullDBCopy(
    ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC),
    ADatagram.Params.Value['ADatabase'],
    ADatagram.Params.Value['ACheckQueue'],
    ADatagram.Params.Value['ATopPosition']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevADRClientDispatcher.dmGetDBList(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := Mainboard.ADRClient.GetDBList;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevADRClientDispatcher.dmGetStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Mainboard.ADRClient.GetStatus);
end;

procedure TevADRClientDispatcher.DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState);
begin
  inherited;
  if (APreviousState <> ssActive) and (ASession.State = ssActive) then
    (Mainboard.ADRServer as IevADRModule).LogEvent('Connected to server',
      Format('Server IP: %s' + #13 + 'Login: %s', [ASession.GetRemoteIPAddress, ASession.Login]))

  else if (APreviousState = ssActive) and (ASession.State <> ssActive) then
    (Mainboard.ADRServer as IevADRModule).LogEvent('Disconnected from server',
       Format('Login: %s', [ASession.Login]));
end;

procedure TevADRClientDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_AC_GETSTATUS, dmGetStatus);
  AddHandler(METHOD_AC_FULLDBCOPY, dmFullDBCopy);
  AddHandler(METHOD_AC_GETDBLIST, dmGetDBList);
  AddHandler(METHOD_AC_CHECKDB, dmCheckDB);
  AddHandler(METHOD_AC_CHECKDBDATA, dmCheckDBData);
  AddHandler(METHOD_AC_CHECKDBTRANSACTIONS, dmCheckDBTransactions);
end;

initialization
  ObjectFactory.Register([TevADRTransRequest]);
  Mainboard.ModuleRegister.RegisterModule(@GetADRServer, IevADRServer, 'ADR Server');

finalization
  SafeObjectFactoryUnRegister([TevADRTransRequest]);

end.