unit EvADRServerMod;

interface

uses Classes, SysUtils, Variants, DateUtils,
     isBaseClasses, isBasicUtils, EvCommonInterfaces, EvMainboard, EvStreamUtils,
     evADRCommon, EvContext, EvConsts, EvBasicUtils, EvADRRequestQueue, EvControllerInterfaces,
     evClasses, EvExceptions, isSchedule, IsTypes;

type
  IevADRServerExt = interface(IevADRServer)
  ['{B513DB7F-6D6F-4C11-B254-02FFF62C8816}']
    function  GetCommitedDBInfo(const ADatabase: String): IevADRDBInfo;
    function  DBOfflineStatuses: IevDBStatusStorage;
  end;

implementation

uses evADRServerQueue, evADRServerController;

type
  TevADRServer = class(TevCustomADRModule, IevADRServer, IevADRServerExt)
  private
    FScheduler: IisSchedule;
    FController: IevAppController;
    FDBOfflineStatuses: IevDBStatusStorage;
    procedure DoDeleteDBRequests(const AType: String; const ADatabase: String);
    function  DBOfflineStatuses: IevDBStatusStorage;
    function  GetCommitedDBInfo(const ADatabase: String): IevADRDBInfo;
  protected
    procedure DoOnConstruction; override;
    function  GetWorkingDir: String; override;

    //IevADRServer
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    procedure SetActive(const AValue: Boolean); override;
    procedure SyncDBList(const ADBList: IisStringList);
    procedure RestoreDB(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer;
                        const ADBData: IisStream);
    procedure ApplySyncData(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer; const ASyncData: IisStream);
    procedure CheckDBData(const ADatabase, ADBVersion: String; const ALastTransactionNbr: Integer; const ADBData: IisStream);
    procedure CheckDBTransactions(const ADataBase, ADBVersion: String; const ALastTransactionNbr, ATransCount: Integer);
    procedure DeleteDBRequests(const ADatabase: String);
    function  GetStatus: IisListOfValues;
    procedure RebuildTT(const AOriginatedAt: TUTCDateTime; const ADatabase: String);
  public
    class function GetTypeID: String; override;
  end;


  TisADRSchedule = class(TisSchedule)
  protected
    procedure DoOnStart; override;
    procedure RunTaskFor(const AEvent: IisScheduleEvent); override;
    procedure DoOnStop; override;
  end;


function GetADRServer: IevADRServer;
begin
  Result := TevADRServer.Create;
end;


{ TevADRServer }

procedure TevADRServer.ApplySyncData(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String;
  const ALastTransactionNbr: Integer; const ASyncData: IisStream);
begin
  RequestQueue.NewDBRequest(AOriginatedAt, ADR_Srv_SyncDBData, [ADatabase, ADBVersion, ALastTransactionNbr, ASyncData]);
end;

procedure TevADRServer.CheckDBData(const ADatabase, ADBVersion: String;
  const ALastTransactionNbr: Integer; const ADBData: IisStream);
begin
  RequestQueue.NewDBRequest(NowUTC, ADR_Srv_CheckDBData, [ADatabase, ADBVersion, ALastTransactionNbr, ADBData]);
end;

procedure TevADRServer.CheckDBTransactions(const ADataBase,
  ADBVersion: String; const ALastTransactionNbr, ATransCount: Integer);
begin
  RequestQueue.NewDBRequest(NowUTC, ADR_Srv_CheckDBTrans, [ADatabase, ADBVersion, ALastTransactionNbr, ATransCount]);
end;

function TevADRServer.DBOfflineStatuses: IevDBStatusStorage;
begin
  Result := FDBOfflineStatuses;
end;

procedure TevADRServer.DeleteDBRequests(const ADatabase: String);
begin
  (Mainboard.ADRClient as IevADRModule).RequestQueue.DeleteDBRequests(ADatabase);
  DoDeleteDBRequests('', ADatabase);
end;

procedure TevADRServer.DoDeleteDBRequests(const AType, ADatabase: String);
begin
  RequestQueue.DeleteDBRequests(ADatabase, AType);
end;

procedure TevADRServer.DoOnConstruction;
var
  Evt: IisScheduleEventWeekly;
begin
  inherited;
  RequestQueue.ExecSlots := mb_AppConfiguration.GetValue('ADRServer\MaxRequests', RequestQueue.ExecSlots);

  FDBOfflineStatuses := TevDBStatusStorage.Create(GetWorkingDir + 'ServerStatus');

  // Schedule Sweep DB 
  FScheduler := TisADRSchedule.Create('ADR Server Scheduler');

  Evt := TisScheduleEventWeekly.Create;
  Evt.TaskID := 'EnlistSweepTT';
  Evt.RunIfMissed := True;
  Evt.StartDate := Today;
  Evt.StartTime := StrToTime('10:00 PM');
  Evt.RunDays := [dwSaturday];
  try
    Evt.LastRunDate := TextToDate(mb_AppSettings.AsString['LastSweepTT']);
  except
  end;

{  See "SYNCHING TEMP TABLES" comment in EvFirebird.pas

  FScheduler.AddEvent(Evt);
}

  FController := CreateASController;
  FController.Active := True;
end;

function TevADRServer.GetCommitedDBInfo(const ADatabase: String): IevADRDBInfo;
var
  DBInfo: IevADRDBInfo;
begin
  Result := TevADRDBInfo.Create(ADatabase);
  Result.Status := dsAbsent;
  try
    try
      DBInfo := DoGetDBInfo(ADatabase);
    except
      DBOfflineStatuses.ClearDBInfo(ADatabase);
      raise;
    end;

    if DBInfo.Status = dsPresent then
      DBOfflineStatuses.SetDBInfo(DBInfo);

    Result := DBInfo;
  except
    // probably need to analyze the error
    on E: EMaintenance do
      Result.Status := dsMaintenance;

    on E: Exception do
    begin
      if not Contains(E.Message, 'No such file or directory') then
      begin
        Result.Status := dsError;
        LogError(ADatabase + ': GetCommitedDBInfo', E.Message);
      end;
    end;
  end;
end;

function TevADRServer.GetDBInfo(const ADatabase: String): IevADRDBInfo;
var
  R: IevADRDBRequest;
  RList: IisList;
  i: Integer;
  PrevADRContext: Boolean;
begin
  PrevADRContext := SetADRContext(True);
  try
    Result := TevADRDBInfo.Create(ADatabase);

    // Check requests
    RList := RequestQueue.GetRequests('', [arsQueued, arsExecuting, arsFinished, arsFinishedWithErrors], ADatabase);
    for i := 0 to RList.Count - 1 do
    begin
      R := RList[i] as IevADRDBRequest;

      if R.Status = arsFinishedWithErrors then
      begin
        DBOfflineStatuses.ClearDBInfo(ADatabase);

        Result.Version := '';
        Result.LastTransactionNbr := 0;
        Result.Status := dsError;
        break;
      end

      else if Result.LastTransactionNbr < R.LastTransactionNbr then
      begin
        Result.Version := R.DBVersion;
        Result.LastTransactionNbr := R.LastTransactionNbr;
        Result.Status := dsPreparing;
      end;
    end;

    if (Result.Status = dsUnknown) and GetActive then
    begin
      Result := DBOfflineStatuses.GetDBInfo(ADatabase);  // Check cached DB info
      if Result.Status <> dsPresent then
        Result := GetCommitedDBInfo(ADatabase);     // Check DB if no info has been found
    end;
  finally
    SetADRContext(PrevADRContext);
  end;
end;

function TevADRServer.GetStatus: IisListOfValues;
var
  Stat: IevADRStatCounter;
  s: String;
  AllRequests: IisList;
  R: IevADRRequest;
  TR: IevADRTransRequest;
  NowTime, MinOrigTime, d: TDateTime;
  i: Integer;
begin
  Result := TisListOfValues.Create;

  Stat := Statistics.GetCounter('LastActivity');
  if Assigned(Stat) then
    s := DateTimeToStr(Stat.LastUpdate) + '  ' + VarToStr(Stat.Value)
  else
    s := '';

  Result.AddValue('LastActivity', s);

  // Latency
  NowTime := UTCToGMTDateTime(NowUTC);
  MinOrigTime := NowTime;

  AllRequests := (Mainboard.ADRServer as IevADRModule).RequestQueue.GetRequests;
  for i := 0 to AllRequests.Count - 1 do
  begin
    R := AllRequests[i] as IevADRRequest;
    if R.ID <> ADR_Srv_CheckDBData then
    begin
      d := UTCToGMTDateTime(R.OriginatedAt);
      if DateTimeCompare(d, coLess, MinOrigTime) then
        MinOrigTime := d;
    end;
  end;
  R := nil;

  if Supports(Mainboard.ADRClient, IevProxyModule) then
  begin
    AllRequests := (Mainboard.ADRClient as IevADRModule).RequestQueue.GetRequests;
    for i := 0 to AllRequests.Count - 1 do
    begin
      TR := AllRequests[i] as IevADRTransRequest;
      if TR.ADRRequestID <> ADR_Srv_CheckDBData then
      begin
        d := UTCToGMTDateTime(TR.OriginatedAt);
        if DateTimeCompare(d, coLess, MinOrigTime) then
          MinOrigTime := d;
      end;
    end;
    TR := nil;
    AllRequests := nil;
  end;

  Result.AddValue('Latency', SecondsBetween(NowTime, MinOrigTime));
end;

class function TevADRServer.GetTypeID: String;
begin
  Result := 'ADR Server';
end;

function TevADRServer.GetWorkingDir: String;
begin
  Result := NormalizePath(mb_AppConfiguration.GetValue('ADRServer\StagingFolder', AppDir + 'ADR')) + 'Server\';
end;

procedure TevADRServer.RebuildTT(const AOriginatedAt: TUTCDateTime; const ADatabase: String);
begin
  if (ADatabase = '') or IsTempDB(ADatabase) then
  begin
    if not RequestQueue.RequestsExist(ADR_Srv_RebuildAllTT, [arsQueued, arsExecuting]) then
      RequestQueue.NewDBRequest(AOriginatedAt, ADR_Srv_RebuildAllTT, ['', '', 0, True]);
  end

  else if IsClientDB(ADatabase) then
  begin
    if not RequestQueue.RequestsExist(ADR_Srv_RebuildTT, [arsQueued, arsExecuting], ADatabase) then
      RequestQueue.NewDBRequest(AOriginatedAt, ADR_Srv_RebuildTT, [ADatabase, '', 0]);
  end;
end;

procedure TevADRServer.RestoreDB(const AOriginatedAt: TUTCDateTime; const ADatabase, ADBVersion: String;
  const ALastTransactionNbr: Integer; const ADBData: IisStream);
begin
  // clean up all related requests
  DoDeleteDBRequests('', ADatabase);

  if IsTempDB(ADatabase) then
  begin
    DoDeleteDBRequests(ADR_Srv_RebuildAllTT, '');
    DoDeleteDBRequests(ADR_Srv_RebuildTT, '');
  end;

  RequestQueue.NewDBRequest(AOriginatedAt, ADR_Srv_FullDBCopy,
    [ADatabase, ADBVersion, ALastTransactionNbr, ADBData]);
end;

procedure TevADRServer.SetActive(const AValue: Boolean);
begin
  inherited;
  FScheduler.Active := AValue;
end;

procedure TevADRServer.SyncDBList(const ADBList: IisStringList);
begin
  RequestQueue.NewDBRequest(NowUTC, ADR_Srv_SyncDBList, ['', '', 0, ADBList]);
end;

{ TisADRSchedule }

procedure TisADRSchedule.DoOnStart;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;
  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
end;

procedure TisADRSchedule.DoOnStop;
begin
  Mainboard.ContextManager.RestoreThreadContext;
  inherited;
end;

procedure TisADRSchedule.RunTaskFor(const AEvent: IisScheduleEvent);
begin
  inherited;
  if AEvent.TaskID = 'EnlistSweepTT' then
  begin
    (Mainboard.ADRServer as IevADRModule).AddDBRequestsIfMissing(NowUTC, ADR_Srv_EnlistSweepDB, ['', '', 0], True);
    AEvent.LastRunDate := Now;
    mb_AppSettings.AsString['LastSweepTT'] := DateToText(AEvent.LastRunDate);    
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetADRServer, IevADRServer, 'ADR Server');

end.
