unit EvDataChangeTriggers;

interface

uses SysUtils, Classes, Variants,
     isBaseClasses, EvContext, EvDataSet, evCommonInterfaces, IsBasicUtils, EvExceptions,
     EvConsts, EvUtils;

type
  IevDataChangeTriggers = interface
  ['{F3A34C3E-F49C-4D69-B2CF-806642D8CB1B}']
    procedure BeforeInsert(const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);
    procedure BeforeUpdate(const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);
    procedure BeforeDelete(const ATable: String; const ANbr: Integer);
  end;

  function DataChangeTriggers: IevDataChangeTriggers;

implementation

type
  TevDBOper = (dboBeforeInsert, dboBeforeUpdate, dboBeforeDelete);
  TevDBOpers = set of TevDBOper;

  TevTriggerProc = procedure(const AOper: TevDBOper; const ANbr: Integer; const AFields: IisListOfValues) of object;

  TevDataChangeTriggers = class(TisInterfacedObject, IevDataChangeTriggers)
  private
    FProcList: TStringList;
    procedure AddTriggerProc(const ATable: String; const AOperations: TevDBOpers; const AProc: TevTriggerProc);
    procedure RegisterTriggers;
    procedure ProcessEvent(const AOper: TevDBOper; const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);

    procedure biuEE(const AOper: TevDBOper; const ANbr: Integer; const AFields: IisListOfValues);
    procedure biuEE_SCHEDULED_E_DS(const AOper: TevDBOper; const ANbr: Integer; const AFields: IisListOfValues);

  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;

    procedure BeforeInsert(const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);
    procedure BeforeUpdate(const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);
    procedure BeforeDelete(const ATable: String; const ANbr: Integer);
  end;

const
  OperPrif: array [Low(TevDBOper)..High(TevDBOper)] of String = ('bi', 'bu', 'bd');

var
  FDataChangeTriggers: IevDataChangeTriggers;


function DataChangeTriggers: IevDataChangeTriggers;
begin
  Result := FDataChangeTriggers;
end;

{ TevDataChangeTriggers }

procedure TevDataChangeTriggers.AddTriggerProc(const ATable: String; const AOperations: TevDBOpers; const AProc: TevTriggerProc);
var
  oper: TevDBOper;
begin
  for oper := Low(TevDBOper) to High(TevDBOper) do
    if oper in AOperations then
      FProcList.AddObject(OperPrif[oper] + ATable, @AProc);
end;

procedure TevDataChangeTriggers.DoOnConstruction;
begin
  inherited;
  FProcList := TStringList.Create;
  FProcList.Duplicates := dupError;
  FProcList.Capacity :=  1000;
  FProcList.Sorted := True;
  RegisterTriggers;
  FProcList.Capacity := FProcList.Count;
end;

procedure TevDataChangeTriggers.DoOnDestruction;
begin
  FreeAndNil(FProcList);
  inherited;
end;

procedure TevDataChangeTriggers.RegisterTriggers;
begin
  AddTriggerProc('EE', [dboBeforeInsert, dboBeforeUpdate], biuEE);
  AddTriggerProc('EE_SCHEDULED_E_DS', [dboBeforeInsert, dboBeforeUpdate], biuEE_SCHEDULED_E_DS);
end;

procedure TevDataChangeTriggers.ProcessEvent(const AOper: TevDBOper; const ATable: String;
  const ANbr: Integer; const AFields: IisListOfValues);
var
  Proc: TMethod;
  Idx: Integer;
  s: String;
begin
  s := OperPrif[AOper] + AnsiUpperCase(ATable);

  Idx := FProcList.IndexOf(s);
  if Idx = -1 then
    Exit;

  Proc.Data := Self;
  Proc.Code := Pointer(FProcList.Objects[Idx]);

  try
    ctx_DBAccess.DisableSecurity;
    TevTriggerProc(Proc)(AOper, ANbr, AFields);
  finally
    ctx_DBAccess.EnableSecurity;
  end;
end;


procedure TevDataChangeTriggers.BeforeDelete(const ATable: String; const ANbr: Integer);
begin
  ProcessEvent(dboBeforeDelete, ATable, ANbr, nil);
end;

procedure TevDataChangeTriggers.BeforeInsert(const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);
begin
  ProcessEvent(dboBeforeInsert, ATable, ANbr, AFields);
end;

procedure TevDataChangeTriggers.BeforeUpdate(const ATable: String; const ANbr: Integer; const AFields: IisListOfValues);
begin
  ProcessEvent(dboBeforeUpdate, ATable, ANbr, AFields);
end;

procedure TevDataChangeTriggers.biuEE(const AOper: TevDBOper; const ANbr: Integer; const AFields: IisListOfValues);
var
  Q: IevQuery;
  user_id: String;
begin
  // Check uniqueness of ESS user ID
  user_id := ConvertNull(AFields.TryGetValue('selfserve_username', ''), '');
  if user_id <> '' then
  begin
    if AOper = dboBeforeInsert then
    begin
      Q := TevQuery.Create('SELECT COUNT(*) FROM tmp_ee t WHERE Upper(t.selfserve_username) = :user_id');
    end
    else
    begin
      Q := TevQuery.Create('SELECT COUNT(*) FROM tmp_ee t WHERE Upper(t.selfserve_username) = :user_id AND NOT (t.cl_nbr = :cl_nbr and t.ee_nbr = :ee_nbr)');
      Q.Params.AddValue('cl_nbr', ctx_DBAccess.CurrentClientNbr);
      Q.Params.AddValue('ee_nbr', ANbr);
    end;
    Q.Params.AddValue('user_id', AnsiUpperCase(user_id));

    ErrorIf(Q.Result.Fields[0].AsInteger > 0, 'ESS user name is not unique', EDataValidationError);
  end;
end;

procedure TevDataChangeTriggers.biuEE_SCHEDULED_E_DS(const AOper: TevDBOper; const ANbr: Integer;
  const AFields: IisListOfValues);
var
  Q: IevQuery;
  start_date, end_date: Variant;
  ee_nbr, cl_e_ds_nbr: Integer;
begin
  // Effective periods should not overlap
  if AOper = dboBeforeUpdate then
  begin
    start_date := AFields.TryGetValue('effective_start_date', 0);
    end_date := AFields.TryGetValue('effective_end_date', 0);
    cl_e_ds_nbr := AFields.TryGetValue('cl_e_ds_nbr', 0);
    ee_nbr := AFields.TryGetValue('ee_nbr', 0);

    if (cl_e_ds_nbr = 0) and (ee_nbr = 0) and (start_date = 0) and (end_date = 0) then
      Exit;

    if (cl_e_ds_nbr = 0) or (ee_nbr = 0) or (start_date = 0) or (end_date = 0) then
    begin
      Q := TevQuery.Create('SELECT t.ee_nbr, t.cl_e_ds_nbr, t.effective_start_date, t.effective_end_date FROM ee_scheduled_e_ds t WHERE {AsOfNow<t>} AND t.ee_scheduled_e_ds_nbr = :nbr');
      Q.Params.AddValue('nbr', ANbr);

      if Q.Result.RecordCount = 0 then
        Exit;  // Most likely the updating record was deleted in other transaction, so don't bother about validating it

      if ee_nbr = 0 then
        ee_nbr := Q.Result.Fields[0].AsInteger;

      if cl_e_ds_nbr = 0 then
        cl_e_ds_nbr := Q.Result.Fields[1].Value;

      if start_date = 0 then
        start_date := Q.Result.Fields[2].Value;

      if end_date = 0 then
        end_date := Q.Result.Fields[3].Value;
    end;
  end
  else
  begin
    start_date := AFields.TryGetValue('effective_start_date', Null);
    end_date := AFields.TryGetValue('effective_end_date', Null);
    cl_e_ds_nbr := AFields.TryGetValue('cl_e_ds_nbr', 0);
    ee_nbr := AFields.TryGetValue('ee_nbr', 0);
  end;

  Q := TevQuery.Create(
    'SELECT COUNT(*) FROM ee_scheduled_e_ds t '#13 +
    'WHERE {AsOfNow<t>} AND '#13 +
    '      t.ee_scheduled_e_ds_nbr <> :ee_scheduled_e_ds_nbr AND'#13 +
    '      t.ee_nbr = :ee_nbr AND t.cl_e_ds_nbr = :cl_e_ds_nbr AND'#13 +
    '      (:start_date < t.effective_start_date AND CAST(:end_date AS DATE) IS NULL OR '#13 +
    '       :start_date BETWEEN t.effective_start_date AND t.effective_end_date OR'#13 +
    '       :end_date BETWEEN t.effective_start_date AND t.effective_end_date OR'#13 +
    '       :start_date > t.effective_start_date AND t.effective_end_date IS NULL)');

  Q.Params.AddValue('ee_scheduled_e_ds_nbr', ANbr);
  Q.Params.AddValue('ee_nbr', ee_nbr);
  Q.Params.AddValue('cl_e_ds_nbr', cl_e_ds_nbr);
  Q.Params.AddValue('start_date', start_date);
  Q.Params.AddValue('end_date', end_date);

  ErrorIf(Q.Result.Fields[0].AsInteger > 0, 'The same scheduled E/D can not have effective dates that overlap', EDataValidationError);
end;

initialization
  FDataChangeTriggers := TevDataChangeTriggers.Create;

finalization
  FDataChangeTriggers := nil;

end.
