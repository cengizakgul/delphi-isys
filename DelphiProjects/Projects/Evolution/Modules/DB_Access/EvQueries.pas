// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvQueries;

interface

uses Classes, Windows, SysUtils, isBaseClasses, isBasicUtils, EvConsts, EvTypes, EvUtils,
     EvExceptions;

type
  TevLibQueryInfo = record
    SQL: String;
  end;

  IevDBQueryLibrary = interface
  ['{ACF438DA-A376-4F21-B922-9B295E3C9660}']
    function GetQueryText(const AQueryName: String): TevLibQueryInfo;
    function ProcessTemplates(const AEvSQL: String): String;
  end;

  function QueryLib: IevDBQueryLibrary;

implementation

type
  PTevLibQueryInfo = ^TevLibQueryInfo;

  TevDBQueryLibrary = class(TisInterfacedObject, IevDBQueryLibrary)
  private
    FList: TStringList;
    FNameMaxLength: Integer;
    procedure RegisterQuery(const AQueryName, AQuery: String);
    procedure Initialize0;
    procedure Initialize1;
    procedure Initialize2;
    procedure Initialize3;
    procedure Initialize4;
    procedure Initialize;
    procedure Clear;
  protected
    procedure DoOnConstruction; override;
    function  GetQueryText(const AQueryName: String): TevLibQueryInfo;
    function  ProcessTemplates(const AEvSQL: String): String;
  public
    destructor  Destroy; override;
  end;


var
  FQueryLib: IevDBQueryLibrary;

function QueryLib: IevDBQueryLibrary;
begin
  Result := FQueryLib;
end;



const
  GenericAsOfDate = '$1 >= $2.effective_date AND $1 < $2.effective_until';

  AsOfDate = '{GenericAsOfDate<:StatusDate, $1>}';

  AsOfNow = '{GenericAsOfDate<''now'', $1>}';

  DatabaseType = '/*$1*/';

  UpdateAsOfDate = 'UPDATE $1 SET $2 WHERE $1_nbr = :$1_nbr AND {AsOfDate<$1>}';

  DeleteRecord = 'EXECUTE PROCEDURE del_$1(:nbr)';
  DeleteVersionsAfterDate = 'DELETE FROM $1 WHERE $1_nbr = :$1_nbr AND effective_date > :StatusDate';
  DeleteAsOfDate = '{UpdateAsOfDate<$1, "effective_until = :StatusDate">}';

  CurrentSysTime = ':CurrentSysTime999';

  GenericSelect = 'select #COLUMNS from #TABLENAME';

  GenericNBRCnd = '#NBRFIELD_NBR = :RecordNbr';

  PayrollProcessedClause = 'STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''','''+ PAYROLL_STATUS_VOIDED+ ''') ';

  GenericOrder = ' order by #ORDER';

  GenericSelectCurrent = '{GenericSelect} WHERE {AsOfNow<#TABLENAME>}';

  GenericSelectWithCondition = '{GenericSelect} WHERE #CONDITION';

  LockTableWithCondition = 'SELECT $1_nbr FROM $1 WHERE $2 WITH LOCK';

  GenericSelectCurrentWithCondition = '{GenericSelectCurrent} AND #CONDITION';

  GenericSelectCurrentWithConditionGrouped = GenericSelectCurrentWithCondition + ' group by #Group';

  GenericSelectOrdered = GenericSelect + GenericOrder;

  GenericSelectCurrentNBR = GenericSelectCurrent + ' and ' + GenericNBRCnd;

  GenericSelectCurrentOrdered = GenericSelectCurrent + GenericOrder;

  GenericSelectCurrentNBROrdered = GenericSelectCurrent + ' and ' + GenericNBRCnd + GenericOrder;

  GenericSelectNBR = GenericSelect + ' WHERE ' + GenericNBRCnd;

  GenericSelectNBROrdered = GenericSelectNBR + GenericOrder;

  GenericSelectCurrentNBRWithCondition = GenericSelectCurrent + ' and ' + GenericNBRCnd + ' and #Condition';

  GenericSelectCurrentNBRWithConditionGrouped = GenericSelectCurrentNBRWithCondition + ' group by #Group';

  GenericInsert = 'insert into #TABLENAME (#FIELDLIST) values (#PARAMLIST)';

  GenericUpdate = 'update #TABLENAME set #ASSIGNLIST';

  GenericUpdateWithCondition = 'update #TABLENAME set #ASSIGNLIST WHERE #CONDITION';

  GenericUpdateNBR = GenericUpdate + ' WHERE ' + GenericNBRCnd;

  GenericSecondKey = '#SECONDKEY = :SecondKeyValue';

  GenericUpdateWithTwoKeys = GenericUpdateNBR + ' and ' + GenericSecondKey;

  SelectHist = '{GenericSelect} WHERE {AsOfDate<#TABLENAME>}';
  SelectHistWithCondition = '{SelectHist} AND #CONDITION';
  SelectHistNBR = '{SelectHist} AND {GenericNBRCnd}';

  GenericSelect2 = 'select #COLUMNS ' +
    'from #TABLE2 T2, #TABLE1 T1 ' +
    'where T1.#JOINFIELD_NBR = T2.#JOINFIELD_NBR';

  GenericSelect3 = 'select #COLUMNS ' +
    'from #TABLE3 T3, #TABLE2 T2, #TABLE1 T1 ' +
    'where T1.#JOINFIELD_NBR = T2.#JOINFIELD_NBR and T3.#JOINFIELD2_NBR = T2.#JOINFIELD2_NBR';

  GenericSelect3Current = 'select #COLUMNS ' +
    'from #TABLE3 T3, #TABLE2 T2, #TABLE1 T1 ' +
    'where T1.#JOINFIELD_NBR = T2.#JOINFIELD_NBR and T3.#JOINFIELD2_NBR = T2.#JOINFIELD2_NBR and '+
    '{AsOfNow<T1>} and {AsOfNow<T2>} and {AsOfNow<T3>}';

  GenericSelect3CurrentWithCondition = GenericSelect3Current+ ' and '+ '#CONDITION';

  GenericSelect2WithCondition = GenericSelect2 + ' and #CONDITION';

  GenericSelect2DiffNbrWithCondition = 'select #COLUMNS ' +
    'from #TABLE2 T2, #TABLE1 T1 ' +
    'where #CONDITION';

  GenericSelect2Current = 'select #COLUMNS ' +
    'from #TABLE2 T2, #TABLE1 T1 ' +
    'where T1.#JOINFIELD_NBR = T2.#JOINFIELD_NBR and ' +
    '{AsOfNow<T1>} and {AsOfNow<T2>}';

  GenericSelect2CurrentNbr = 'select #COLUMNS ' +
    'from #TABLE2 T2, #TABLE1 T1 ' +
    'where T2.#NBRFIELD_NBR = :RecordNbr and ' +
    'T1.#JOINFIELD_NBR = T2.#JOINFIELD_NBR and ' +
    '{AsOfNow<T1>} and {AsOfNow<T2>}';

  GenericSelect2CurrentGrouped = GenericSelect2CurrentNbr + ' group by #Group';

  GenericSelect2CurrentWithCondition = GenericSelect2Current + ' and #CONDITION';

  GenericSelect2CurrentNbrWithCondition = GenericSelect2CurrentNbr + ' and #CONDITION';

  GenericSelect2CurrentWithConditionGrouped = GenericSelect2CurrentWithCondition + ' group by #Group';

  GenericSelect2CurrentNBRWithConditionGrouped = GenericSelect2CurrentNBRWithCondition + ' group by #Group';

  GenericSelect2HistNbr = 'select #COLUMNS ' +
    'from #TABLE2 T2, #TABLE1 T1 ' +
    'where T2.#NBRFIELD_NBR = :RecordNbr and ' +
    'T1.#JOINFIELD_NBR = T2.#JOINFIELD_NBR and ' +
    '{AsOfNow<T1>} and {AsOfNow<T2>}';

  GenericSelect2HistNbrWithCondition = GenericSelect2HistNbr + ' and #CONDITION';


  GenericLastTableRecVerChange = '{DatabaseType<$1>} ' +
                          'SELECT tr.commit_time change_date, tr.user_id changed_by ' +
                          'FROM ev_transaction tr, ev_table_change tc, ev_table tbl ' +
                          'WHERE ' +
                          'tc.ev_transaction_nbr = tr.nbr AND ' +
                          'tc.ev_table_nbr = tbl.nbr AND ' +
                          'tbl.name = ''$2'' AND ' +
                          'tc.record_id = $3 ' +
                          'ORDER BY 1 DESC ' +
                          'ROWS 1';

  SystemLastTableRecVerChange = '{GenericLastTableRecVerChange<System, $1, $2>}';

  BureauLastTableRecVerChange = '{GenericLastTableRecVerChange<Bureau, $1, $2>}';

  ClientLastTableRecVerChange = '{GenericLastTableRecVerChange<Client, $1, $2>}';


  SelectEmployeesToAttach = 'select first_name, last_name, ee_nbr, custom_employee_number ' +
      'from ee e join ' +
      'cl_person cp on e.cl_person_nbr=cp.cl_person_nbr ' +
      'where e.co_nbr = :CoNbr and ' +
      '{AsOfNow<e>} and {AsOfNow<cp>} and e.CURRENT_TERMINATION_CODE=''A''';

  SelectLastScheduledFrequencyCheckDate = 'select s.scheduled_check_date, ' +
              's.scheduled_delivery_date, s.filler, ' +
              'b.period_begin_date, ' +
              'b.period_end_date, ' +
              'b.scheduled_call_in_date, b.frequency ' +
              'from pr_scheduled_event s join ' +
              'pr_scheduled_event_batch b on s.pr_scheduled_event_nbr=b.pr_scheduled_event_nbr ' +
              'where s.co_nbr = :CoNbr and b.frequency = :Freq and ' +
              '{AsOfNow<s>} and {AsOfNow<b>} ' +
              'order by s.scheduled_check_date';

  SelectClientsCompanies = 'SELECT c.cl_nbr, c.custom_client_number, c.name client_name, ' +
      'o.co_nbr, o.custom_company_number, o.name company_name, ' +
      'o.fein, o.tax_service, o.trust_service, o.termination_code ' +
      'FROM tmp_cl c ' +
      'JOIN tmp_co o ' +
      'ON c.cl_nbr = o.cl_nbr ' +
      'ORDER BY c.cl_nbr, o.co_nbr';

  ExecProc = 'Execute procedure #PROCNAME #INPUT';

  OpenProc = 'select #COLUMNS from #PROCNAME #INPUT';

  GetNextPeriodDate = 'select distinct B.PERIOD_#EDGE_DATE, B.PERIOD_#EDGE_DATE_STATUS ' +
      'from PR_BATCH B ' +
      'join PR P ' +
      'on B.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:Co_Nbr and P.SCHEDULED=''Y'' and ' +
      'B.FREQUENCY=:BatchFrequency and ' +
      '{AsOfNow<B>} and {AsOfNow<P>} ' +
      'order by B.PERIOD_BEGIN_DATE';

  CountBiWeeklyBatchs = 'select count(PR_BATCH_NBR) MY_COUNT ' +
      'from PR_BATCH B ' +
      'join PR P ' +
      'on B.PR_NBR=P.PR_NBR ' +
      'where B.FREQUENCY=:Frequency and ' +
      '{AsOfNow<B>} and {AsOfNow<P>} and P.SCHEDULED=''Y'' and ' +
      'P.CHECK_DATE=' +
      '(select max(CHECK_DATE) ' +
      'from PR ' +
      'where {AsOfNow<PR>} and SCHEDULED=''Y'' ' +
      'and CHECK_DATE<:CheckDate)';

  EmployeeLastEDCheckDate = 'select max(P.CHECK_DATE) CHECK_DATE ' +
      'from PR P ' +
      'join PR_CHECK C on P.PR_NBR=C.PR_NBR ' +
      'join PR_CHECK_LINES L on C.PR_CHECK_NBR=L.PR_CHECK_NBR ' +
      'where C.EE_NBR=:EmployeeNbr and P.SCHEDULED=''Y'' and ' +
      'P.PR_NBR<>:PRNbr and L.CL_E_DS_NBR=:EDNbr and C.CHECK_TYPE=''R'' and ' +
      '{AsOfNow<L>} and {AsOfNow<C>} and {AsOfNow<P>}';

  AllEmployeeLastEDCheckDates = 'select C.EE_NBR,L.CL_E_DS_NBR,max(P.CHECK_DATE) CHECK_DATE ' +
      'from PR P ' +
      'join PR_CHECK C on P.PR_NBR=C.PR_NBR ' +
      'join PR_CHECK_LINES L on C.PR_CHECK_NBR=L.PR_CHECK_NBR ' +
      'where P.CO_NBR=:CoNbr and P.SCHEDULED=''Y'' and ' +
      'P.PR_NBR<>:PRNbr and C.CHECK_TYPE=''R'' and ' +
      '{AsOfNow<L>} and {AsOfNow<C>} and {AsOfNow<P>} ' +
      'group by C.EE_NBR,L.CL_E_DS_NBR';

  SelectAllPayrollsAfterDate = 'select distinct P.CHECK_DATE ' +
      'from PR P ' +
      'join PR_BATCH B ' +
      'on P.PR_NBR=B.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and B.FREQUENCY=:Frequency and ' +
      'P.CHECK_DATE<=:EndCheckDate and ' +
      'P.SCHEDULED=''Y'' and ' +
      '{AsOfNow<P>} and {AsOfNow<B>} ' +
      'order by CHECK_DATE';

  PensionDeductions = 'select S.EE_SCHEDULED_E_DS_NBR, S.AMOUNT, S.PERCENTAGE , S.CALCULATION_TYPE, ' +
      'S.CL_E_D_GROUPS_NBR, S.MINIMUM_PAY_PERIOD_AMOUNT, S.MINIMUM_PAY_PERIOD_PERCENTAGE, ' +
      'S.MIN_PPP_CL_E_D_GROUPS_NBR, S.MAXIMUM_PAY_PERIOD_AMOUNT, S.MAXIMUM_PAY_PERIOD_PERCENTAGE, ' +
      'S.MAX_PPP_CL_E_D_GROUPS_NBR, S.MAX_PPP_CL_E_D_GROUPS_NBR, S.ANNUAL_MAXIMUM_AMOUNT, S.SCHEDULED_E_D_GROUPS_NBR, ' +
      'D.MATCH_FIRST_AMOUNT, D.MATCH_FIRST_PERCENTAGE, D.MATCH_SECOND_AMOUNT, D.MATCH_SECOND_PERCENTAGE, ' +
      'D.FLAT_MATCH_AMOUNT, D.CL_E_DS_NBR, D.E_D_CODE_TYPE ' +
      'from EE_SCHEDULED_E_DS S ' +
      'join CL_E_DS D ' +
      'on S.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
      'where S.EE_NBR=:EmployeeNbr and ' +
      'D.CL_PENSION_NBR=:PensionNbr and ' +
      'D.E_D_CODE_TYPE not in (#EDCodeType) and ' +
      'S.EFFECTIVE_START_DATE<=:CheckDate and (S.EFFECTIVE_END_DATE>=:CheckDate or S.EFFECTIVE_END_DATE is null) and ' +
      '{AsOfNow<D>} and {AsOfNow<S>} ' +
      'order by S.PRIORITY_NUMBER';

  MaxRunNumberForCheckDate = 'select max(RUN_NUMBER) max_run ' +
      'from PR ' +
      'where CHECK_DATE=:CheckDate and {AsOfNow<PR>}';

  NextRunNumberForCheckDate = MaxRunNumberForCheckDate + ' and CO_NBR=:Co_Nbr';

  NextRunNumberForCheckDatePrNbr = NextRunNumberForCheckDate + ' and PR_NBR <> :PrNbr';

  SumTaxLiabilities = 'select sum(AMOUNT) ' +
      'from CO_#LEVEL_TAX_LIABILITIES L ' +
      'join CO C ' +
      'on L.CO_NBR=C.CO_NBR ' +
      'where C.FEIN=:Fein and ' +
      'L.DUE_DATE=:DueDate and ' +
      'L.TAX_TYPE=:TaxType and ' +
      '{AsOfNow<L>} and {AsOfNow<C>}';

  MaxCheckDateForCo = 'select max(CHECK_DATE) MAX_CHECK_DATE ' +
      'from PR ' +
      'where CO_NBR=:Co_Nbr and ' +
      'SCHEDULED=''Y'' and ' +
      'CHECK_DATE<:CheckDate and {AsOfNow<PR>}';

  PrNbrForMaxCheckDateForCo = 'select max(PR_NBR) PR_NBR ' +
      'from PR ' +
      'where CO_NBR=:Co_Nbr and ' +
      'SCHEDULED=''Y'' and CHECK_DATE=:CheckDate and {AsOfNow<PR>}';

  SelectDistinctEDForPR = 'select distinct L.EE_SCHEDULED_E_DS_NBR ' +
      'from PR_CHECK_LINES L ' +
      'where L.PR_NBR=:Pr_Nbr and {AsOfNow<L>}';

  SelectDistinctLocalsForPR = 'select distinct L.EE_LOCALS_NBR ' +
      'from PR_CHECK_LOCALS L ' +
      'where L.PR_NBR=:Pr_Nbr and {AsOfNow<L>}';

  CheckFederalShortfalls = 'select PR_CHECK_NBR, FEDERAL_SHORTFALL from PR_CHECK ' +
      'where (PR_CHECK_NBR=(select max(PR_CHECK_NBR) from PR_CHECK p ' +
      'where (PR_CHECK_NBR<:CheckToVoid) and (EE_NBR=:EENbr) and ({AsOfNow<p>})) or ' +
      'PR_CHECK_NBR=(select max(PR_CHECK_NBR) from PR_CHECK ' +
      'where (EE_NBR=:EENbr) and ({AsOfNow<PR_CHECK>}))) ' +
      'and ({AsOfNow<PR_CHECK>}) ' +
      'order by PR_CHECK_NBR';

  CheckStatesShortfalls = 'select PR_CHECK_NBR, STATE_SHORTFALL, EE_SDI_SHORTFALL from PR_CHECK_STATES ' +
      'where (PR_CHECK_STATES_NBR=(select max(PR_CHECK_STATES_NBR) from PR_CHECK_STATES p ' +
      'where (PR_CHECK_NBR<:CheckToVoid) and (EE_STATES_NBR=:EEStatesNbr) and ({AsOfNow<p>})) or ' +
      'PR_CHECK_STATES_NBR=(select max(PR_CHECK_STATES_NBR) from PR_CHECK_STATES ' +
      'where (EE_STATES_NBR=:EEStatesNbr) and ({AsOfNow<PR_CHECK_STATES>}))) ' +
      'and ({AsOfNow<PR_CHECK_STATES>}) ' +
      'order by PR_CHECK_NBR';

  CheckLocalsShortfalls = 'select PR_CHECK_NBR, LOCAL_SHORTFALL from PR_CHECK_LOCALS ' +
      'where (PR_CHECK_LOCALS_NBR=(select max(PR_CHECK_LOCALS_NBR) from PR_CHECK_LOCALS p ' +
      'where (PR_CHECK_NBR<:CheckToVoid) and (EE_LOCALS_NBR=:EELocalsNbr) and ({AsOfNow<p>})) or ' +
      'PR_CHECK_LOCALS_NBR=(select max(PR_CHECK_LOCALS_NBR) from PR_CHECK_LOCALS ' +
      'where (EE_LOCALS_NBR=:EELocalsNbr) and ({AsOfNow<PR_CHECK_LOCALS>}))) ' +
      'and ({AsOfNow<PR_CHECK_LOCALS>}) ' +
      'order by PR_CHECK_NBR';

  CheckDeductionShortfalls = 'select PR_CHECK_NBR, DEDUCTION_SHORTFALL_CARRYOVER from PR_CHECK_LINES ' +
      'where (PR_CHECK_LINES_NBR=(select max(L.PR_CHECK_LINES_NBR) from PR_CHECK_LINES L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'where (C.PR_CHECK_NBR<:CheckToVoid) and (C.EE_NBR=:EENbr) and (L.CL_E_DS_NBR=:EDNbr) ' +
      'and ({AsOfNow<L>} and {AsOfNow<C>})) or ' +
      'PR_CHECK_LINES_NBR=(select max(L.PR_CHECK_LINES_NBR) from PR_CHECK_LINES L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'where (C.EE_NBR=:EENbr) and (L.CL_E_DS_NBR=:EdNbr) ' +
      'and {AsOfNow<L>} and {AsOfNow<C>})) ' +
      'and ({AsOfNow<PR_CHECK_LINES>}) ' +
      'order by PR_CHECK_NBR';

  FedTemplate = 'select sum(#F1) TAX_WAGE, sum(#F2) TAX, sum(#F3) TAX_TIP ' +
      'from PR_CHECK C ' +
      'join PR P on C.PR_NBR=P.PR_NBR and {AsOfNow<P>} and P.PR_NBR<>:PayrollNbr and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.STATUS in (#STATUS) ' +
      'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} and E.CL_PERSON_NBR=:ClPersonNbr ' +
      'join CO CO on E.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and (CO.CL_COMMON_PAYMASTER_NBR=(select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbr and M.COMBINE_#COMBINETAX=''Y'' and {AsOfNow<CO>} and {AsOfNow<M>}' +
      ') and P.CO_NBR<>:CONbr or (P.CO_NBR=:CONbr and (C.EE_NBR=:EENbr and :Act=1 or C.EE_NBR<>:EENbr and :Act=2 or :Act=3))) ' +
      'where {AsOfNow<C>} ';

  GlobalFedTemplate = 'select E.CL_PERSON_NBR, sum(#F1) TAX_WAGE, sum(#F2) TAX, sum(#F3) TAX_TIP ' +
      'from PR_CHECK C ' +
      'join PR P on C.PR_NBR=P.PR_NBR and '+
      '{AsOfNow<P>} and P.PR_NBR<>:PayrollNbr ' +
      'and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.STATUS in (#STATUS) ' +
      'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and ' +
      '(CO.CL_COMMON_PAYMASTER_NBR=(select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbr and M.COMBINE_#COMBINETAX=''Y'' and {AsOfNow<CO>} and {AsOfNow<M>}' +
      ') or CO.CO_NBR=:CONbr) ' +
      'where {AsOfNow<C>} ' +
      'GROUP BY E.CL_PERSON_NBR';

  SdiTemplate = 'select sum(S.#F1) TAX_WAGE, sum(S.#F2) TAX ' +
    'from PR_CHECK_STATES S ' +
    'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR and {AsOfNow<C>} ' +
    'join PR P on C.PR_NBR=P.PR_NBR ' +

    'and C.CHECK_STATUS<>''V'' ' +
    'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

    'and {AsOfNow<P>} and P.STATUS in (#STATUS) ' +
    'and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.PR_NBR<>:PayrollNbr ' +
    'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} and E.CL_PERSON_NBR=:ClPersonNbr ' +
    'join EE_STATES ES on ES.EE_STATES_NBR=S.EE_STATES_NBR and {AsOfNow<ES>} ' +
    'join CO_STATES CS on ES.CO_STATES_NBR=CS.CO_STATES_NBR and {AsOfNow<CS>} ' +
    'and CS.SY_STATES_NBR=(select CS.SY_STATES_NBR from CO_STATES CS ' +
    'join EE_STATES ES on ES.CO_STATES_NBR=CS.CO_STATES_NBR and ES.EE_STATES_NBR=:RecordNbr and {AsOfNow<ES>} ' +
    'where {AsOfNow<CS>}) ' +
    'join CO CO on CO.CO_NBR=CS.CO_NBR and (CO.CL_COMMON_PAYMASTER_NBR=(' +
    'select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
    'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
    'where CO.CO_NBR=:CONbrForPM and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<CO>} ' +
    'and {AsOfNow<M>}) and P.CO_NBR<>:CONbr or (P.CO_NBR=:CONbr and (C.EE_NBR=:EENbr and :Act=1 or C.EE_NBR<>:EENbr and :Act=2 or :Act=3))) ' +
    'and {AsOfNow<CO>} where S.PR_NBR=P.PR_NBR and #Filter {AsOfNow<S>}';

  SuiTemplate = 'select sum(S.SUI_TAXABLE_WAGES) TAX_WAGE, sum(S.SUI_TAX) TAX, sum(S.SUI_GROSS_WAGES) GROSS_WAGE ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR and {AsOfNow<C>} ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +

      'and C.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'and {AsOfNow<P>} and P.STATUS in (#STATUS) ' +
      'and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.PR_NBR<>:PayrollNbr ' +
      'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} and E.CL_PERSON_NBR=:ClPersonNbr ' +
      'join CO_SUI CS on S.CO_SUI_NBR=CS.CO_SUI_NBR and {AsOfNow<CS>} ' +
      'and CS.SY_SUI_NBR=(select S.SY_SUI_NBR from CO_SUI S where S.CO_SUI_NBR=:RecordNbr and {AsOfNow<S>}) ' +
      'join CO CO on CO.CO_NBR=CS.CO_NBR and (CO.CL_COMMON_PAYMASTER_NBR=(' +
      'select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbrForPM and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<CO>} ' +
      'and {AsOfNow<M>}) and P.CO_NBR<>:CONbr or (P.CO_NBR=:CONbr and (C.EE_NBR=:EENbr and :Act=1 or C.EE_NBR<>:EENbr and :Act=2 or :Act=3))) ' +
      'and {AsOfNow<CO>} where S.PR_NBR=P.PR_NBR and #Filter {AsOfNow<S>}';

  LocalTemplateDetails = 'select sum(L.LOCAL_TAXABLE_WAGE - '+

      'CONVERTNULLDOUBLE((select SUM(z.AMOUNT) from PR_CHECK_LINES z '+
      'join PR_CHECK_LINE_LOCALS x on x.PR_CHECK_LINES_NBR=z.PR_CHECK_LINES_NBR and x.EE_LOCALS_NBR=L.EE_LOCALS_NBR and x.EXEMPT_EXCLUDE=''C'' '+
      'where {AsOfNow<z>} and z.PR_CHECK_NBR = C.PR_CHECK_NBR),0) '+

      ') TAX_WAGE, sum(L.LOCAL_TAX) TAX, P.CHECK_DATE ' +
      'from PR_CHECK_LOCALS L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR and P.STATUS in (#STATUS) ' +
      'where C.EE_NBR=:EENbr and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.PR_NBR<>:PayrollNbr ' +
      'and L.EE_LOCALS_NBR=:RecordNbr and {AsOfNow<P>} and {AsOfNow<C>} and {AsOfNow<L>} and L.REPORTABLE=''Y'' ' +
      'and C.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
      'group by P.CHECK_DATE order by P.CHECK_DATE';

  DeductionTemplate = 'select sum(L.HOURS_OR_PIECES) TAX_WAGE, sum(L.AMOUNT) TAX ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where C.EE_NBR=:EENbr ' +
      'and P.PR_NBR=L.PR_NBR and ' +
      'P.CHECK_DATE>=:BeginDate and ' +
      'P.CHECK_DATE<=:EndDate and ' +
      'P.PR_NBR<>:PayrollNbr and ' +
      'not (L.CL_E_DS_NBR<>:RecordNbr) and ' +
      'P.STATUS in (#STATUS) and {AsOfNow<L>} and {AsOfNow<C>} and {AsOfNow<P>}';

  GlobalDeductionTemplate = 'select C.EE_NBR, L.CL_E_DS_NBR, sum(L.HOURS_OR_PIECES) TAX_WAGE, sum(L.AMOUNT) TAX ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and {AsOfNow<L>} and {AsOfNow<C>} and {AsOfNow<P>} and ' +
      'P.CHECK_DATE>=:BeginDate and ' +
      'P.CHECK_DATE<=:EndDate and ' +
      'P.PR_NBR<>:PayrollNbr and ' +
      'P.STATUS in (#STATUS) ' +
      'GROUP BY C.EE_NBR, L.CL_E_DS_NBR';

  SelectLastDDCheckDate = 'select min(P.CHECK_DATE) CHECK_DATE ' +
      'from PR P join PR_CHECK_LINES L on P.PR_NBR=L.PR_NBR ' +
      'where P.CHECK_DATE>:CheckDate and L.EE_SCHEDULED_E_DS_NBR=:DirDepNbr and ' +
      '{AsOfNow<P>} and {AsOfNow<L>}';

  SelectCheckWithEE = 'select #Columns ' +
      'from PR_CHECK C ' +
      'join EE E on C.EE_NBR=E.EE_NBR ' +
      'join CL_PERSON P on E.CL_PERSON_NBR=P.CL_PERSON_NBR ' +
      'left outer join CO_DIVISION V on E.CO_DIVISION_NBR=V.CO_DIVISION_NBR and {AsOfNow<V>} ' +
      'left outer join CO_BRANCH A on E.CO_BRANCH_NBR=A.CO_BRANCH_NBR and {AsOfNow<A>} ' +
      'left outer join CO_DEPARTMENT D on E.CO_DEPARTMENT_NBR=D.CO_DEPARTMENT_NBR and {AsOfNow<D>} ' +
      'left outer join CO_TEAM T on E.CO_TEAM_NBR=T.CO_TEAM_NBR and {AsOfNow<T>} ' +
      'where #NBRFIELD_NBR=:RecordNbr and {AsOfNow<C>} and {AsOfNow<E>} and {AsOfNow<P>}';

  SelectDBDT = 'select D.CUSTOM_DIVISION_NUMBER, B.CUSTOM_BRANCH_NUMBER, P.CUSTOM_DEPARTMENT_NUMBER, T.CUSTOM_TEAM_NUMBER, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR, T.CO_TEAM_NBR ' +
      'from CO_DIVISION D ' +
      'left outer join CO_BRANCH B '+
        'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} '+
      'left outer join CO_DEPARTMENT P '+
        'on P.CO_BRANCH_NBR=B.CO_BRANCH_NBR and {AsOfNow<P>} '+
      'left outer join CO_TEAM T '+
        'on T.CO_DEPARTMENT_NBR=P.CO_DEPARTMENT_NBR and {AsOfNow<T>} '+
      'where D.CO_NBR = :CoNbr and {AsOfNow<D>}';

  TC_SelectDBDT = 'select D.CUSTOM_DIVISION_NUMBER, B.CUSTOM_BRANCH_NUMBER, P.CUSTOM_DEPARTMENT_NUMBER, T.CUSTOM_TEAM_NUMBER, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR, T.CO_TEAM_NBR, ''4'' DBDT_LEVEL ' +
      'from CO_DIVISION D ' +
      'left outer join CO_BRANCH B ' +
        'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} ' +
      'left outer join CO_DEPARTMENT P ' +
        'on P.CO_BRANCH_NBR=B.CO_BRANCH_NBR and {AsOfNow<P>} ' +
      'left outer join CO_TEAM T ' +
        'on T.CO_DEPARTMENT_NBR=P.CO_DEPARTMENT_NBR and {AsOfNow<T>} ' +
      'where D.CO_NBR = :CoNbr and {AsOfNow<D>} ' +
      'union ' +
     'select D.CUSTOM_DIVISION_NUMBER, B.CUSTOM_BRANCH_NUMBER, P.CUSTOM_DEPARTMENT_NUMBER,null CUSTOM_TEAM_NUMBER, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR, null CO_TEAM_NBR, ''3''  DBDT_LEVEL ' +
      'from CO_DIVISION D ' +
      'left outer join CO_BRANCH B ' +
        'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} ' +
      'left outer join CO_DEPARTMENT P ' +
        'on P.CO_BRANCH_NBR=B.CO_BRANCH_NBR and {AsOfNow<P>} ' +
      'where D.CO_NBR = :CoNbr and {AsOfNow<D>} ' +
      'union ' +
     'select D.CUSTOM_DIVISION_NUMBER, B.CUSTOM_BRANCH_NUMBER, Null CUSTOM_DEPARTMENT_NUMBER,null CUSTOM_TEAM_NUMBER, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, null CO_DEPARTMENT_NBR, null CO_TEAM_NBR, ''2''  DBDT_LEVEL ' +
      'from CO_DIVISION D ' +
      'left outer join CO_BRANCH B ' +
        'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} ' +
      'where D.CO_NBR = :CoNbr and {AsOfNow<D>} ' +
      'union ' +
     'select D.CUSTOM_DIVISION_NUMBER,null CUSTOM_BRANCH_NUMBER, Null CUSTOM_DEPARTMENT_NUMBER,null CUSTOM_TEAM_NUMBER, D.CO_DIVISION_NBR, null CO_BRANCH_NBR, null CO_DEPARTMENT_NBR, null CO_TEAM_NBR, ''1'' DBDT_LEVEL ' +
      'from CO_DIVISION D ' +
      'where D.CO_NBR = :CoNbr and {AsOfNow<D>}';

  SelectDBDTWithName = 'select D.CUSTOM_DIVISION_NUMBER, D.NAME DNAME, B.CUSTOM_BRANCH_NUMBER, B.NAME BNAME, P.CUSTOM_DEPARTMENT_NUMBER, P.NAME PNAME, T.CUSTOM_TEAM_NUMBER, T.NAME TNAME, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR, T.CO_TEAM_NBR ' +
      'from CO_DIVISION D ' +
      'left outer join CO_BRANCH B '+
        'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} '+
      'left outer join CO_DEPARTMENT P '+
        'on P.CO_BRANCH_NBR=B.CO_BRANCH_NBR and {AsOfNow<P>} '+
      'left outer join CO_TEAM T '+
        'on T.CO_DEPARTMENT_NBR=P.CO_DEPARTMENT_NBR and {AsOfNow<T>} '+
      'where D.CO_NBR = :CoNbr and {AsOfNow<D>}';

  SelectSortedPayrollByCheckDateRunNumber = 'select p.* ' +
      'from PR p ' +
      'where {AsOfNow<p>} and ' +
      'CHECK_DATE between :BeginDate and :EndDate and {PayrollProcessedClause}' +
      ' and CO_NBR=:CoNbr';

  SelectSortedChecksByCheckDateRunNumber = 'select #Columns ' +
      'from PR_CHECK C ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where {AsOfNow<C>} and ' +
      '{AsOfNow<P>} and ' +
      'P.CHECK_DATE between :BeginDate and :EndDate and ' +
      'c.CHECK_STATUS <> ''V'' and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') and '+
      'P.{PayrollProcessedClause} and P.CO_NBR=:CoNbr and P.PAYROLL_TYPE<>''A''';

  SelectSortedTableByCheckDateRunNumber = 'select P.CHECK_DATE, B.PERIOD_BEGIN_DATE, P.RUN_NUMBER, P.PAYROLL_TYPE, C.EE_NBR, C.PAYMENT_SERIAL_NUMBER, S.* ' +
      'from #TableName S ' +
      'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +
      'join PR_BATCH B on B.PR_BATCH_NBR=C.PR_BATCH_NBR ' +
      'where {AsOfNow<S>} and ' +
      'P.PR_NBR=S.PR_NBR and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<B>} and ' +
      'P.CHECK_DATE between :BeginDate and :EndDate and ' +
      'P.{PayrollProcessedClause} and P.CO_NBR=:CoNbr';

  SelectCheckList = 'select P.CHECK_DATE, P.RUN_NUMBER, E.CUSTOM_EMPLOYEE_NUMBER, ' +
      'N.FIRST_NAME || '' '' || N.LAST_NAME EMPLOYEE_NAME, ' +
      'C.PAYMENT_SERIAL_NUMBER, C.GROSS_WAGES, C.NET_WAGES, C.CHECK_STATUS, C.PR_CHECK_NBR, ' +
      'C.PR_BATCH_NBR, C.PR_NBR, ''123456789012'' Status_Calculate ' +
      'from PR_CHECK C ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'join EE E ' +
      'on C.EE_NBR=E.EE_NBR ' +
      'join CL_PERSON N ' +
      'on E.CL_PERSON_NBR=N.CL_PERSON_NBR ' +
      'where {AsOfNow<C>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<E>} and ' +
      '{AsOfNow<N>} and ' +
      'P.CO_NBR=:CoNbr ' +
      '#FilterCondition ' +
      '#AdditionalFilter and ' +
      'P.{PayrollProcessedClause} ' +
      'order by P.CHECK_DATE, P.RUN_NUMBER, E.CUSTOM_EMPLOYEE_NUMBER, C.PAYMENT_SERIAL_NUMBER';

  SelectMiscCheckList = 'select P.CHECK_DATE, P.RUN_NUMBER, M.PAYMENT_SERIAL_NUMBER, M.CHECK_STATUS, ' +
      'M.PR_MISCELLANEOUS_CHECKS_NBR, M.PR_NBR, M.MISCELLANEOUS_CHECK_AMOUNT ' +
      'from PR_MISCELLANEOUS_CHECKS M ' +
      'join PR P ' +
      'on M.PR_NBR=P.PR_NBR ' +
      'where {AsOfNow<M>} and ' +
      '{AsOfNow<P>} and ' +
      'P.CO_NBR=:CoNbr and ' +
      'M.MISCELLANEOUS_CHECK_TYPE=:CheckType ' +
      '#FilterCondition ' +
      '#AdditionalFilter ' +
      'order by P.CHECK_DATE, P.RUN_NUMBER, M.PAYMENT_SERIAL_NUMBER';

  SelectSecurityRights = GenericSelectCurrent +
      ' and SB_SEC_GROUPS_NBR #GroupNbr and SB_USER_NBR #UserNbr';

  RemoveSecurityRights = GenericUpdate + ' WHERE {AsOfNow<#TABLENAME>} and '
      + '((SB_SEC_GROUPS_NBR #GroupNbr) and (SB_USER_NBR #UserNbr))';

  GenericSelectCurrentWithKey = GenericSelectCurrent + ' and ' + GenericSecondKey;

  DirDep = 'SELECT e.CUSTOM_EMPLOYEE_NUMBER, f.FIRST_NAME, f.MIDDLE_INITIAL, f.LAST_NAME, f.SOCIAL_SECURITY_NUMBER, ' +
      'l.AMOUNT, o.IN_PRENOTE, n.DEDUCT_WHOLE_CHECK, o.EE_BANK_ACCOUNT_NUMBER, o.EE_ABA_NUMBER, o.EE_BANK_ACCOUNT_TYPE, ' +
      'l.CO_DIVISION_NBR, l.CO_BRANCH_NBR, l.CO_DEPARTMENT_NBR, l.CO_TEAM_NBR, c.E_D_CODE_TYPE, e.CURRENT_TERMINATION_CODE, ' +
      'n.ee_child_support_cases_nbr, o.ALLOW_HYPHENS, k.CHECK_TYPE, o.ADDENDA ' +
      'FROM pr_check          k ' +
      'JOIN ee                e on e.EE_NBR=k.EE_NBR ' +
      'JOIN cl_person         f on f.CL_PERSON_NBR = e.CL_PERSON_NBR ' +
      'JOIN pr_check_lines    l on l.PR_CHECK_NBR=k.PR_CHECK_NBR ' +
      'JOIN ee_scheduled_e_ds n on n.EE_SCHEDULED_E_DS_NBR=l.EE_SCHEDULED_E_DS_NBR ' +
      'JOIN cl_e_ds           c on c.CL_E_DS_NBR=n.CL_E_DS_NBR ' +
      'JOIN ee_direct_deposit o on o.EE_DIRECT_DEPOSIT_NBR=n.EE_DIRECT_DEPOSIT_NBR ' +
      'JOIN pr                p on p.PR_NBR=:PrNbr ' +
    'WHERE #CustomCondition k.pr_nbr=:PrNbr and ' +
      'k.check_type in ('''+CHECK_TYPE2_REGULAR+''','''+CHECK_TYPE2_VOID+''') and ' +
      '{AsOfDate<n>} and ' +
      '{AsOfDate<c>} and ' +
      '{AsOfDate<o>} and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<k>} and ' +
      '{AsOfNow<l>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<f>} ' +
      'ORDER BY c.E_D_CODE_TYPE, k.ee_nbr';

  EEDirDep = 'SELECT e.CUSTOM_EMPLOYEE_NUMBER,'+
      'f.FIRST_NAME, f.MIDDLE_INITIAL, f.LAST_NAME,'+
      'f.SOCIAL_SECURITY_NUMBER,'+
      'l.AMOUNT, o.IN_PRENOTE,'+
      'n.DEDUCT_WHOLE_CHECK,'+
      'o.EE_BANK_ACCOUNT_NUMBER,'+
      'o.EE_ABA_NUMBER,'+
      'o.EE_BANK_ACCOUNT_TYPE,' +
      'k.CHECK_TYPE,'+
      'l.CO_DIVISION_NBR DIVISION,'+
      'l.CO_BRANCH_NBR BRANCH,'+
      'l.CO_DEPARTMENT_NBR DEPARTMENT,'+
      'l.CO_TEAM_NBR TEAM,'+
      'k.CHECK_TYPE,'+
      'o.EE_BANK_ACCOUNT_TYPE,'+
      'o.ALLOW_HYPHENS, ' +
      'c.E_D_CODE_TYPE, ' +
      'c.SKIP_HOURS, ' +
      'p.CHECK_DATE ' +
      'FROM pr_check          k ' +
      'JOIN ee                e on e.ee_nbr=k.ee_nbr ' +
      'JOIN cl_person         f on f.cl_person_nbr=e.cl_person_nbr ' +
      'JOIN pr_check_lines    l on l.pr_check_nbr=k.pr_check_nbr ' +
      'JOIN ee_scheduled_e_ds n on n.ee_scheduled_e_ds_nbr=l.ee_scheduled_e_ds_nbr ' +
      'JOIN cl_e_ds           c on c.cl_e_ds_nbr=n.cl_e_ds_nbr ' +
      'JOIN ee_direct_deposit o on o.ee_direct_deposit_nbr=n.ee_direct_deposit_nbr ' +
      'JOIN pr                p on p.PR_NBR=:PrNbr ' +
    'WHERE #CustomCondition k.pr_nbr=:PrNbr and ' +
      '{AsOfNow<k>} and c.E_D_CODE_TYPE <> ''' + ED_DED_CHILD_SUPPORT + ''' and '+
      ' not ((o.IN_PRENOTE <> '''+GROUP_BOX_YES+''') and (l.AMOUNT = 0)) and '+
      '{AsOfDate<n>} and ' +
      '{AsOfDate<c>} and ' +
      '{AsOfDate<o>} and ' +
      '{AsOfNow<l>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<p>} and ' +
      'k.check_type in ('''+CHECK_TYPE2_REGULAR+''','''+CHECK_TYPE2_VOID+''') and ' +
      '{AsOfNow<f>} ' +
      'ORDER BY k.ee_nbr';

  CSDirDep = 'SELECT '+
      'e.CUSTOM_EMPLOYEE_NUMBER,'+
      'f.FIRST_NAME, f.MIDDLE_INITIAL, f.LAST_NAME,'+
      'f.SOCIAL_SECURITY_NUMBER, ' +
      'l.AMOUNT,'+
      'o.IN_PRENOTE,'+
      'o.EE_BANK_ACCOUNT_NUMBER,'+
      'o.EE_ABA_NUMBER,'+
      's.IL_MED_INS_ELIGIBLE,'+
      'case when strcopy(trim(s.CUSTOM_CASE_NUMBER), char_length(s.CUSTOM_CASE_NUMBER)-1, 1) = ''X'' then s.CUSTOM_FIELD else s.CUSTOM_CASE_NUMBER end CUSTOM_CASE_NUMBER,' +
      's.FIPS_CODE,' +
      's.ORIGINATION_STATE,'+
      'k.CHECK_TYPE,'+

      'p.CHECK_DATE,' +
      'l.CO_DIVISION_NBR DIVISION,'+
      'l.CO_BRANCH_NBR BRANCH,'+
      'l.CO_DEPARTMENT_NBR DEPARTMENT,'+
      'l.CO_TEAM_NBR TEAM,'+

      'e.CURRENT_TERMINATION_CODE,'+
      'a.SB_AGENCY_NBR,'+
      'o.ALLOW_HYPHENS,'+
      'o.ADDENDA '+
      'FROM PR_CHECK               k ' +
      'JOIN EE                     e on e.ee_nbr=k.ee_nbr ' +
      'JOIN CL_PERSON              f on f.cl_person_nbr=e.cl_person_nbr ' +
      'JOIN PR_CHECK_LINES         l on l.pr_check_nbr=k.pr_check_nbr ' +
      'JOIN EE_SCHEDULED_E_DS      n on n.ee_scheduled_e_ds_nbr=l.ee_scheduled_e_ds_nbr ' +
      'JOIN EE_CHILD_SUPPORT_CASES s on s.EE_CHILD_SUPPORT_CASES_NBR = n.EE_CHILD_SUPPORT_CASES_NBR ' +
      'LEFT OUTER JOIN CL_AGENCY   a on a.CL_AGENCY_NBR=s.CL_AGENCY_NBR AND {AsOfNow<a>} ' +
      'JOIN CL_E_DS                c on c.cl_e_ds_nbr=n.cl_e_ds_nbr ' +
      'JOIN EE_DIRECT_DEPOSIT      o on o.ee_direct_deposit_nbr=n.ee_direct_deposit_nbr ' +
      'JOIN PR                     p on p.PR_NBR=:PrNbr ' +
    'WHERE #CustomCondition k.pr_nbr=:PrNbr and ' +
      'L.PR_NBR=:PrNbr and ' +
      '{AsOfNow<k>} and c.E_D_CODE_TYPE = ''' + ED_DED_CHILD_SUPPORT + ''' and '+
      ' not ((o.IN_PRENOTE <> '''+GROUP_BOX_YES+''') and (l.AMOUNT = 0)) and '+
      ' (#SbUsePrenote (o.IN_PRENOTE = '''+GROUP_BOX_NO+''')) and '+
      '{AsOfDate<n>} and ' +
      '{AsOfDate<c>} and ' +
      '{AsOfDate<o>} and ' +
      '{AsOfNow<l>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<s>} and ' +
      '{AsOfNow<p>} and ' +
      'k.check_type in ('''+CHECK_TYPE2_REGULAR+''','''+CHECK_TYPE2_VOID+''') and ' +
      '{AsOfNow<f>} ' +
      'ORDER BY k.ee_nbr';

  NewAgencyDirDep =
      'select a.SB_BANKS_NBR,'+
      'a.CUSTOM_BANK_ACCOUNT_NUMBER,'+
      'a.BANK_ACCOUNT_TYPE,' +
      'g.SB_AGENCY_NBR,'+
      'g.PAYMENT_TYPE,'+
      'c.NAME,' +
      'g.CLIENT_AGENCY_CUSTOM_FIELD,'+
      'm.MISCELLANEOUS_CHECK_TYPE,'+
      'm.MISCELLANEOUS_CHECK_AMOUNT ' +
      'FROM PR                      s ' +
      'JOIN CO                      c on c.CO_NBR = s.CO_NBR ' +
      'JOIN PR_MISCELLANEOUS_CHECKS m on m.PR_NBR = :PrNbr ' +
      'JOIN CL_AGENCY               g on g.CL_AGENCY_NBR = m.CL_AGENCY_NBR ' +
      'JOIN CL_BANK_ACCOUNT         a on a.CL_BANK_ACCOUNT_NBR = g.CL_BANK_ACCOUNT_NBR ' +
    'WHERE '+
      's.PR_NBR=:PrNbr and ' +
      '{AsOfNow<s>} and ' +
      '{AsOfNow<g>} and ' +
      '{AsOfNow<a>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<m>} and ' +
      'm.MISCELLANEOUS_CHECK_AMOUNT <> 0 and ' +
      '#CustomCondition ' +
      'm.MISCELLANEOUS_CHECK_TYPE in (''L'', ''G'') and ' +
      'g.PAYMENT_TYPE=''D'' ' +
      'ORDER BY a.SB_BANKS_NBR';

  AgencyDirDep = 'select a.SB_BANKS_NBR, a.CUSTOM_BANK_ACCOUNT_NUMBER, a.BANK_ACCOUNT_TYPE, ' +
      'g.SB_AGENCY_NBR, g.client_agency_custom_field, c.co_nbr, c.custom_company_number, ' +
      'c.name, c.dd_cl_bank_account_nbr, c.debit_number_of_days_prior_dd, ' +
      'c.trust_service, c.trust_sb_account_nbr, c.debit_number_days_prior_pr, ' +
      'c.obc, c.sb_obc_account_nbr, c.tax_service, c.tax_sb_bank_account_nbr, ' +
      'c.ach_sb_bank_account_nbr, m.MISCELLANEOUS_CHECK_TYPE, m.MISCELLANEOUS_CHECK_AMOUNT ' +
      'FROM pr                      s ' +
      'JOIN co                      c on c.CO_NBR=s.CO_NBR ' +
      'JOIN pr_miscellaneous_checks m on m.PR_NBR=:PrNbr ' +
      'JOIN cl_agency               g on g.CL_AGENCY_NBR=m.CL_AGENCY_NBR ' +
      'JOIN cl_bank_account         a on a.CL_BANK_ACCOUNT_NBR=g.CL_BANK_ACCOUNT_NBR ' +
    'WHERE ' +
      's.pr_nbr=:PrNbr and ' +
      '{AsOfNow<s>} and ' +
      '{AsOfNow<g>} and ' +
      '{AsOfNow<a>} and ' +
      '{AsOfNow<m>} and ' +
      '{AsOfNow<c>} and ' +
      '#CustomCondition ' +
      'm.MISCELLANEOUS_CHECK_TYPE in (''L'', ''G'') and ' +
      'g.PAYMENT_TYPE=''D'' ' +
      'ORDER BY a.SB_BANKS_NBR';

  BillingForACH =
    'SELECT '+
      '#DBDT p.PR_NBR, ' +
      'sum(ConvertNullDouble(d.AMOUNT, 0) + ConvertNullDouble(d.TAX, 0)) #REVERSAL AMOUNT ' +
      'FROM PR                        p ' +
      'JOIN CO                        c on c.CO_NBR = p.CO_NBR ' +
      'JOIN CO_BILLING_HISTORY        b on b.CO_NBR = p.CO_NBR and b.PR_NBR = p.PR_NBR ' +
      'JOIN CO_BILLING_HISTORY_DETAIL d on d.CO_BILLING_HISTORY_NBR = b.CO_BILLING_HISTORY_NBR ' +
      'WHERE p.PR_NBR=:PrNbr and ' +
      '{AsOfNow<b>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<d>} and ' +
      '{AsOfNow<p>} ' +
      'GROUP BY #DBDT p.PR_NBR ' +
      'HAVING sum(ConvertNullDouble(d.AMOUNT, 0) + ConvertNullDouble(d.TAX, 0)) <> 0';


  OldBillingForACH = 'SELECT b.co_nbr, b.pr_nbr, b.status, p.run_number, p.check_date, p.status, ' +
      'p.exclude_billing, c.custom_company_number, c.name, c.fein, ' +
      'c.debit_number_days_prior_bill, ' +
      'c.billing_cl_bank_account_nbr, c.billing_sb_bank_account_nbr, #DBDT ' +
      'sum(ConvertNullDouble(d.amount, 0) + ConvertNullDouble(d.tax, 0)) amount ' +
      'FROM co_billing_history        b ' +
      'join co                        c on c.co_nbr=b.co_nbr ' +
      'join pr                        p on p.co_nbr=b.co_nbr and p.pr_nbr=b.pr_nbr ' +
      'join co_billing_history_detail d on d.co_billing_history_nbr=b.co_billing_history_nbr ' +
      'WHERE b.co_nbr=:CoNbr and ' +
      'b.pr_nbr=:PrNbr and ' +
      '{AsOfNow<b>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<d>} and ' +
      '{AsOfNow<p>} ' +
      'GROUP BY b.co_nbr, b.pr_nbr, b.status, p.run_number, p.check_date, p.status, ' +
      'p.exclude_billing, c.custom_company_number, c.name, c.fein, ' +
      'c.debit_number_days_prior_bill, #DBDT ' +
      'c.billing_cl_bank_account_nbr, c.billing_sb_bank_account_nbr ';

  GetACHData = 'select A.CO_PR_ACH_NBR, ' +
      'A.CL_NBR, ' +
      'A.CO_NBR, ' +
      'A.ACH_DATE, ' +
      'A.AMOUNT, ' +
      'A.STATUS, ' +
      'A.STATUS_DATE, ' +
      'P.RUN_NUMBER, ' +
      'P.PAYROLL_TYPE, ' +
      'P.PROCESS_DATE, ' +
      'A.PR_NBR, ' +
      'A.FILLER, ' +
      'A.TAX_IMPOUND,' +
      'A.TRUST_IMPOUND,' +
      'A.BILLING_IMPOUND,' +
      'A.WC_IMPOUND,' +
      'A.DD_IMPOUND,' +
      'C.CUSTOM_COMPANY_NUMBER, ' +
      'C.NAME, ' +
      'L.NAME CLIENT_NAME,' +
      'C.FEIN COMPANY_FEIN,' +
      'L.CUSTOM_CLIENT_NUMBER, ' +
      'P.CHECK_DATE, ' +
      'C.BANK_ACCOUNT_REGISTER_NAME ' +
      'from TMP_CO_PR_ACH A, TMP_CO C, TMP_PR P, TMP_CL L ' +
      'where #DatePeriod and ' +
      'C.CO_NBR = A.CO_NBR and ' +
      'C.CL_NBR = A.CL_NBR and ' +
      'L.CL_NBR = A.CL_NBR and ' +
      'C.ACH_SB_BANK_ACCOUNT_NBR = :SB_BANK_ACCOUNTS_NBR and ' +
      'P.CL_NBR = A.CL_NBR and ' +
      'P.PR_NBR = A.PR_NBR ' +
      '#CustomFilter ' +
      'order by A.CL_NBR, A.CO_NBR, A.PR_NBR';

  InCondition = '#InField in (#InValues)';


  TmpLiabilitiesWithLocals = 'select c.TAX_SERVICE, s.PAYMENT_METHOD DEP_METHOD, ' +
      't.CO_LOCAL_TAX_LIABILITIES_NBR, t.co_tax_deposits_nbr, t.SY_LOCALS_NBR NBR, t.CL_NBR, ' +
      't.CO_NBR, t.AMOUNT, t.CHECK_DATE, p.RUN_NUMBER, t.DUE_DATE, t.Status, ' +
      ' -1 paymentSY_LOCALS_NBR ' +
      'from TMP_CO_LOCAL_TAX_LIABILITIES t ' +
      'join TMP_CO c on ' +
        'c.CL_NBR = t.CL_NBR and ' +
        'c.CO_NBR = t.CO_NBR ' +
        'and c.TERMINATION_CODE <> '''+ TERMINATION_SAMPLE+ ''' '+
        'and c.CREDIT_HOLD <> '''+ CREDIT_HOLD_LEVEL_MAINT + ''' ' +
        'join TMP_CL cl on ' +
        'c.CL_NBR = cl.CL_NBR ' +
        ' #cond ' +
        'join TMP_CO_LOCAL_TAX s on ' +
        's.SY_LOCALS_NBR = t.SY_LOCALS_NBR and ' +
        's.CL_NBR = t.CL_NBR and ' +
        's.CO_NBR = t.CO_NBR ' +
      'left outer join TMP_PR p on '+
        'p.PR_NBR = t.PR_NBR and ' +
        'p.CL_NBR = t.CL_NBR ' +
      'where DUE_DATE<=:EndDate AND AMOUNT <> 0 AND ' +
      '#StartDate ' +
      '#Condition and ' +
      't.CO_TAX_DEPOSITS_NBR is null and t.THIRD_PARTY = ''N'' and ' +
      '(t.Status = :Status1 or ' +
      't.status = :Status2 or ' +
      't.status = :Status4 or ' +
      't.status = :Status3) ' +
      'and Trim(STRCOPY(CAST(t.Filler AS VARCHAR(512)), 21, 10)) = '''' ';

  TmpLiabilitiesWithSUI = 'select c.TAX_SERVICE, ' +
      't.CO_SUI_LIABILITIES_NBR, t.co_tax_deposits_nbr, t.SY_SUI_NBR NBR, t.CL_NBR, ' +
      't.CO_NBR, t.AMOUNT, t.CHECK_DATE, p.RUN_NUMBER, t.DUE_DATE, t.Status ' +
      'from TMP_CO_SUI_LIABILITIES t '+
      'join TMP_CO c on ' +
        'c.CL_NBR = t.CL_NBR and ' +
        'c.CO_NBR = t.CO_NBR ' +
        'and c.TERMINATION_CODE <> ''' + TERMINATION_SAMPLE + ''' '+
        'and c.CREDIT_HOLD <> '''+ CREDIT_HOLD_LEVEL_MAINT + ''' ' +
        'join TMP_CL cl on ' +
        'c.CL_NBR = cl.CL_NBR ' +
        ' #cond ' +
      'left outer join TMP_PR p on '+
        'p.PR_NBR = t.PR_NBR and ' +
        'p.CL_NBR = t.CL_NBR ' +
      'where DUE_DATE<=:EndDate AND AMOUNT <> 0 AND ' +
      '#StartDate ' +
      't.SY_SUI_NBR in (#SUIList) and ' +
      't.CO_TAX_DEPOSITS_NBR is null and t.THIRD_PARTY = ''N'' and ' +
      '(t.Status = :Status1 or ' +
      't.status = :Status2 or ' +
      't.status = :Status4 or ' +
      't.status = :Status3)';


  TmpLiabilitiesWithStates = 'select c.TAX_SERVICE, s.STATE_TAX_DEPOSIT_METHOD DEP_METHOD, ' +
      't.CO_STATE_TAX_LIABILITIES_NBR, t.co_tax_deposits_nbr, t.STATE, t.CL_NBR, ' +
      't.CO_NBR, t.AMOUNT, t.CHECK_DATE, p.RUN_NUMBER, t.DUE_DATE, t.Status, t.TAX_TYPE ' +
      'from TMP_CO_STATE_TAX_LIABILITIES t '+
      'join TMP_CO c on ' +
        'c.CL_NBR = t.CL_NBR and ' +
        'c.CO_NBR = t.CO_NBR ' +
        'and c.TERMINATION_CODE <> '''+ TERMINATION_SAMPLE + ''' '+
        'and c.CREDIT_HOLD <> '''+ CREDIT_HOLD_LEVEL_MAINT + ''' ' +
        'join TMP_CL cl on ' +
        'c.CL_NBR = cl.CL_NBR ' +
        ' #cond ' +
        'join TMP_CO_STATES s on ' +
        's.STATE  = t.STATE  and ' +
        's.CL_NBR = t.CL_NBR and ' +
        's.CO_NBR = t.CO_NBR ' +
      'left outer join TMP_PR p on '+
        'p.PR_NBR = t.PR_NBR and ' +
        'p.CL_NBR = t.CL_NBR ' +
      'where DUE_DATE<=:EndDate AND AMOUNT <> 0 AND ' +
      '#StartDate ' +
      't.STATE in (#StateList) and ' +
      't.CO_TAX_DEPOSITS_NBR is null and t.THIRD_PARTY = ''N'' and ' +
      '(t.Status = :Status1 or ' +
      't.status = :Status2 or ' +
      't.status = :Status4 or ' +
      't.status = :Status3)';


  TmpLiabilities = 'select c.TAX_SERVICE, c.FEDERAL_TAX_PAYMENT_METHOD DEP_METHOD, ' +
      'c.SY_FED_TAX_PAYMENT_AGENCY_NBR, ' +
      't.CO_FED_TAX_LIABILITIES_NBR, t.co_tax_deposits_nbr, t.CL_NBR, t.CO_NBR, ' +
      't.AMOUNT, t.CHECK_DATE, t.DUE_DATE, p.RUN_NUMBER, t.TAX_TYPE, t.Status ' +
      'from TMP_CO_FED_TAX_LIABILITIES t ' +
      'join TMP_CO c on ' +
        'c.CL_NBR = t.CL_NBR and ' +
        'c.CO_NBR = t.CO_NBR ' +
        'and c.TERMINATION_CODE <> '''+ TERMINATION_SAMPLE + ''' '+
        'and c.CREDIT_HOLD <> '''+ CREDIT_HOLD_LEVEL_MAINT + ''' ' +
        'join TMP_CL cl on ' +
        'c.CL_NBR = cl.CL_NBR ' +
        ' #cond ' +
      'left outer join TMP_PR p on '+
        'p.PR_NBR = t.PR_NBR and ' +
        'p.CL_NBR = t.CL_NBR ' +
      'where DUE_DATE<=:EndDate AND AMOUNT <> 0 AND ' +
      '#StartDate ' +
      't.CO_TAX_DEPOSITS_NBR is null and t.THIRD_PARTY = ''N'' and ' +
      '(t.Status = :Status1 or ' +
      't.status = :Status2 or ' +
      't.status = :Status4 or ' +
      't.status = :Status3)';


  AgencyCheckDetails = 'select P.FIRST_NAME, P.MIDDLE_INITIAL, P.LAST_NAME, ' +
      'P.SOCIAL_SECURITY_NUMBER, E.CUSTOM_EMPLOYEE_NUMBER, E.EE_NBR, ' +
      'D.DESCRIPTION, D.CL_E_DS_NBR, sum(L.AMOUNT) AMOUNT ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join EE E ' +
      'on C.EE_NBR=E.EE_NBR ' +
      'join CL_PERSON P ' +
      'on E.CL_PERSON_NBR=P.CL_PERSON_NBR ' +
      'join CL_E_DS D ' +
      'on L.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
      'where L.PR_MISCELLANEOUS_CHECKS_NBR=:CheckNbr and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<E>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<D>} ' +
      'group by P.FIRST_NAME, P.MIDDLE_INITIAL, P.LAST_NAME, P.SOCIAL_SECURITY_NUMBER, ' +
      'E.CUSTOM_EMPLOYEE_NUMBER, E.EE_NBR, D.DESCRIPTION, D.CL_E_DS_NBR ' +
      'order by P.LAST_NAME, P.FIRST_NAME, D.DESCRIPTION';

  ChildSupportCases = 'select distinct C.EE_NBR, L.CL_E_DS_NBR, '+
      'case when strcopy(trim(H.CUSTOM_CASE_NUMBER), char_length(H.CUSTOM_CASE_NUMBER)-1, 1) = ''X'' then H.CUSTOM_FIELD else H.CUSTOM_CASE_NUMBER end CUSTOM_CASE_NUMBER,'+
      'H.FIPS_CODE ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join EE_SCHEDULED_E_DS S ' +
      'on L.EE_SCHEDULED_E_DS_NBR=S.EE_SCHEDULED_E_DS_NBR ' +
      'join EE_CHILD_SUPPORT_CASES H ' +
      'on S.EE_CHILD_SUPPORT_CASES_NBR=H.EE_CHILD_SUPPORT_CASES_NBR ' +
      'where L.PR_MISCELLANEOUS_CHECKS_NBR=:CheckNbr and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<H>}';

  GarnishmentIDs = 'select distinct C.EE_NBR, L.CL_E_DS_NBR, S.GARNISNMENT_ID ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join EE_SCHEDULED_E_DS S ' +
      'on L.EE_SCHEDULED_E_DS_NBR=S.EE_SCHEDULED_E_DS_NBR ' +
      'where L.PR_MISCELLANEOUS_CHECKS_NBR=:CheckNbr and ' +
      'S.GARNISNMENT_ID is not null and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<S>}';

  MaxCheckDatesForEE = 'select C.EE_NBR, max(P.CHECK_DATE) MAX_CHECK_DATE ' +
      'from PR P ' +
      'join PR_CHECK C ' +
      'on P.PR_NBR=C.PR_NBR ' +
      'where P.SCHEDULED=''Y'' and ' +
      'P.PR_NBR<:PrNbr and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<C>} ' +
      'group by C.EE_NBR';

  DistinctCheckDatePr = 'select count(distinct P.CHECK_DATE) PR_COUNT ' +
      'from PR P ' +
      'join PR_BATCH B ' +
      'on P.PR_NBR=B.PR_NBR ' +
      'where B.FREQUENCY=:Freq and ' +
      'P.CHECK_DATE>:StartDate and ' +
      'P.CHECK_DATE<:EndDate and ' +
      'P.SCHEDULED=''Y'' and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<B>}';

  ListEDWithShortfalls = 'select C.EE_NBR, L.CL_E_DS_NBR, sum(L.DEDUCTION_SHORTFALL_CARRYOVER) SHORTFALL ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'where {AsOfNow<C>} and ' +
      '{AsOfNow<L>} and ' +
      'C.PR_NBR=:PrNbr and ' +
      'L.PR_NBR=:PrNbr ' +
      'group by C.EE_NBR, L.CL_E_DS_NBR';

  ListFedTaxesWithShortfalls = 'select EE_NBR, sum(FEDERAL_SHORTFALL) SHORTFALL ' +
      'from PR_CHECK p ' +
      'where {AsOfNow<p>} and ' +
      'PR_NBR=:PRNbr ' +
      'group by EE_NBR';

  FedTaxesTotals = 'select count(distinct C.EE_NBR) EE_COUNT, sum(C.#SumExpression) TAXABLE_WAGES, ' +
      'sum (C.#TaxName) TAX ' +
      'from PR_CHECK C ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and ' +
      '(CO.CL_CO_CONSOLIDATION_NBR=(select CO.CL_CO_CONSOLIDATION_NBR from CO CO ' +
      'join CL_CO_CONSOLIDATION N on N.CL_CO_CONSOLIDATION_NBR=CO.CL_CO_CONSOLIDATION_NBR ' +
      'where CO.CO_NBR=:CONbr and {AsOfNow<CO>} and {AsOfNow<N>}' +
      ') or CO.CO_NBR=:CONbr) ' +
      'where P.CHECK_DATE between :BeginDate and :EndDate and ' +
      'C.#TaxName is not null and C.#TaxName <> 0 and #FilterCondition ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>}';

  StateTaxesTotals = 'select count(distinct C.EE_NBR) EE_COUNT, ' +
      'sum(S.#TaxName_TAXABLE_WAGES) TAXABLE_WAGES, ' +
      'sum(S.#TaxName_TAX) TAX ' +
      'from PR_CHECK_STATES S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'join EE_STATES E ' +
      'on S.EE_STATES_NBR=E.EE_STATES_NBR ' +
      'join CO_STATES CS ' +
      'on E.CO_STATES_NBR = CS.CO_STATES_NBR ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and ' +
      '(CO.CL_CO_CONSOLIDATION_NBR=(select CO.CL_CO_CONSOLIDATION_NBR from CO CO ' +
      'join CL_CO_CONSOLIDATION N on N.CL_CO_CONSOLIDATION_NBR=CO.CL_CO_CONSOLIDATION_NBR ' +
      'where CO.CO_NBR=:CONbr and {AsOfNow<CO>} and {AsOfNow<N>}' +
      ') or CO.CO_NBR=:CONbr) ' +
      'where P.CHECK_DATE between :BeginDate and :EndDate and ' +
      '((S.#TaxName_TAX is not null and CS.STATE <>'+'''AL''' + ') or ' +
      '(S.#TaxName_TAX is not null and S.#TaxName_TAX <> 0 and CS.STATE = '+'''AL'''+')) and ' +
      'E.CO_STATES_NBR=:CoStateNbr and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<CS>} and ' +
      '{AsOfNow<E>}';

  SUIDistinctTaxesTotals = 'select count(distinct C.EE_NBR) EE_COUNT, ' +
      'sum(S.SUI_TAXABLE_WAGES) TAXABLE_WAGES, ' +
      'sum(S.SUI_TAX) TAX ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'join CO_SUI CSI ' +
      'on S.CO_SUI_NBR = CSI.CO_SUI_NBR ' +
      'join CO_STATES CS ' +
      'on CSI.CO_STATES_NBR = CS.CO_STATES_NBR ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and ' +
      '(CO.CL_CO_CONSOLIDATION_NBR=(select CO.CL_CO_CONSOLIDATION_NBR from CO CO ' +
      'join CL_CO_CONSOLIDATION N on N.CL_CO_CONSOLIDATION_NBR=CO.CL_CO_CONSOLIDATION_NBR ' +
      'where CO.CO_NBR=:CONbr and {AsOfNow<CO>} and {AsOfNow<N>}' +
      ') or CO.CO_NBR=:CONbr) ' +
      'where P.CHECK_DATE between :BeginDate and :EndDate and ' +
      '((S.SUI_TAX is not null and CS.STATE <>'+'''AL''' + ') or ' +
      '(S.SUI_TAX is not null and S.SUI_TAX <> 0 and CS.STATE = '+'''AL'''+')) and ' +
      'S.CO_SUI_NBR=:COLevelNbr and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<CS>} and ' +
      '{AsOfNow<CSI>} and ' +
      '{AsOfNow<P>}';

  LocalTaxesTotals = 'select count(distinct C.EE_NBR) EE_COUNT, ' +
      'sum(S.LOCAL_TAXABLE_WAGE) TAXABLE_WAGES, ' +
      'sum(S.LOCAL_TAX) TAX ' +
      'from PR_CHECK_LOCALS S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join CO_LOCAL_TAX_LIABILITIES L ' +
      'on C.PR_NBR=L.PR_NBR ' +
      'join EE_LOCALS E ' +
      'on S.EE_LOCALS_NBR=E.EE_LOCALS_NBR ' +
      'join CO_LOCAL_TAX CS ' +
      'on E.CO_LOCAL_TAX_NBR=CS.CO_LOCAL_TAX_NBR ' +
      'where L.CO_TAX_DEPOSITS_NBR=:TaxDepositNbr and ' +
      '((S.LOCAL_TAX is not null and CS.SY_STATES_NBR <> 3 ) or ' +
      '(S.LOCAL_TAX is not null and S.LOCAL_TAX <> 0 and CS.SY_STATES_NBR  = 3 )) and ' +
      'L.CO_LOCAL_TAX_NBR=:CoLocalNbr and ' +
      'E.CO_LOCAL_TAX_NBR=:CoLocalNbr and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<CS>} and ' +
      '{AsOfNow<E>}';

  ListOtherWithShortfalls = 'select #Columns ' +
      'from PR_CHECK C ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CHECK_DATE between :StartDate and :EndDate and ' +
      'P.CO_NBR=:CONbr and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ' +
      'group by #Group';

  ListAllStateWithShortfalls = 'select C.EE_NBR, CS.STATE, sum(S.STATE_SHORTFALL) STATE_SHORTFALL, ' +
      'sum(S.EE_SDI_SHORTFALL) SDI_SHORTFALL ' +
      'from PR_CHECK_STATES S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join EE_STATES ES ' +
      'on S.EE_STATES_NBR=ES.EE_STATES_NBR ' +
      'join CO_STATES CS ' +
      'on ES.CO_STATES_NBR=CS.CO_STATES_NBR ' +
      'where {AsOfNow<C>} and ' +
      '{AsOfNow<ES>} and ' +
      '{AsOfNow<CS>} and ' +
      '{AsOfNow<S>} and ' +
      'S.PR_NBR=:PRNbr and ' +
      'C.PR_NBR=:PRNbr ' +
      'group by C.EE_NBR, CS.STATE';

  ListAllLocalWithShortfalls = 'select C.EE_NBR, CLT.SY_LOCALS_NBR, sum(L.LOCAL_SHORTFALL) LOCAL_SHORTFALL ' +
      'from PR_CHECK_LOCALS L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join EE_LOCALS EL ' +
      'on L.EE_LOCALS_NBR=EL.EE_LOCALS_NBR ' +
      'join CO_LOCAL_TAX CLT ' +
      'on EL.CO_LOCAL_TAX_NBR=CLT.CO_LOCAL_TAX_NBR ' +
      'where {AsOfNow<C>} and ' +
      '{AsOfNow<EL>} and ' +
      '{AsOfNow<CLT>} and ' +
      '{AsOfNow<L>} and ' +
      'S.PR_NBR=:PRNbr and ' +
      'C.PR_NBR=:PRNbr ' +
      'group by C.EE_NBR, CLT.SY_LOCALS_NBR';

  AmountsWorked = 'select a.EE_NBR, a.CO_TIME_OFF_ACCRUAL_NBR, sum(HOURS_OR_PIECES) HoursWorked ' +
      'from EE_TIME_OFF_ACCRUAL a, PR_CHECK_LINES l, PR_CHECK c, PR p ' +
      'where {AsOfNow<p>} and {AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<l>} and ' +
      'a.STATUS = ''Y'' and ' +
      'p.CO_NBR = :CoNbr and ' +
      'l.CL_E_DS_NBR in ( ' +
               ' Select b.CL_E_DS_NBR from CL_E_D_GROUP_CODES b ' +
                    'where {AsOfNow<b>}  and ' +
                      ' b.CL_E_D_GROUPS_NBR in ( ' +
                         'select t.CL_E_D_GROUPS_NBR from  CO_TIME_OFF_ACCRUAL t ' +
                                  ' where t.CALCULATION_METHOD = ' + TIME_OFF_ACCRUAL_CALC_METHOD_AVERAGE_THE_HOURS + ' and ' +
                                    ' {AsOfNow<t>} and t.CO_NBR = :CoNbr and ' +
                                    ' a.CO_TIME_OFF_ACCRUAL_NBR = t.CO_TIME_OFF_ACCRUAL_NBR ' +
                                    ')) and ' +
      'c.pr_nbr = p.pr_nbr and l.PR_CHECK_NBR = c.PR_CHECK_NBR and ' +
      'c.EE_NBR = a.EE_NBR and ' +                                          
      'p.SCHEDULED = ''Y'' and ' +
      'p.{PayrollProcessedClause} and ' +
      'p.EXCLUDE_TIME_OFF = ''N'' and ' +
      'p.check_date between :StartDate and :EndDate '+
      'group by a.EE_NBR, a.CO_TIME_OFF_ACCRUAL_NBR '+
      'having sum(HOURS_OR_PIECES) > 0';

  SchedulePayrollCount = 'select a.EE_NBR, a.CO_TIME_OFF_ACCRUAL_NBR, count(distinct p.pr_nbr) PeriodsWorked ' +
      'from EE_TIME_OFF_ACCRUAL a, PR_CHECK_LINES l, PR_CHECK c, PR p ' +
      'where {AsOfNow<a>} and {AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<l>} and ' +
      'a.STATUS = ''Y'' and  p.CO_NBR = :CoNbr and ' +
      'l.CL_E_DS_NBR in ( ' +
               ' Select b.CL_E_DS_NBR from CL_E_D_GROUP_CODES b ' +
                    'where {AsOfNow<b>}  and ' +
                      ' b.CL_E_D_GROUPS_NBR in ( ' +
                         'select t.CL_E_D_GROUPS_NBR from  CO_TIME_OFF_ACCRUAL t ' +
                                  ' where t.CALCULATION_METHOD = ' + TIME_OFF_ACCRUAL_CALC_METHOD_AVERAGE_THE_HOURS + ' and ' +
                                    ' {AsOfNow<t>} and t.CO_NBR = :CoNbr and ' +
                                    ' a.CO_TIME_OFF_ACCRUAL_NBR = t.CO_TIME_OFF_ACCRUAL_NBR ' +
                                    ')) and ' +
      'c.pr_nbr = p.pr_nbr and ' +
      'l.PR_CHECK_NBR = c.PR_CHECK_NBR and ' +
      'c.EE_NBR = a.EE_NBR and ' +
      'p.SCHEDULED = ''Y'' and ' +
      'not (l.HOURS_OR_PIECES is Null) and ' +
      'p.{PayrollProcessedClause} and ' +
      'p.EXCLUDE_TIME_OFF = ''N'' and ' +
      'p.check_date between :StartDate and :EndDate '+
      'group by a.EE_NBR, a.CO_TIME_OFF_ACCRUAL_NBR '+
      'having count(distinct p.pr_nbr) > 0 ';

  BatchAdditionalInfo = 'select max(p.CHECK_DATE) MAX_CHECK_DATE ' +
      'from PR p ' +
      'join PR_BATCH b ' +
      'on b.pr_nbr=p.pr_nbr and b.FREQUENCY = :BatchFreq and {AsOfNow<b>} ' +
      'where p.CO_NBR=:CoNbr and ' +
      'p.SCHEDULED=''Y'' and ' +
      'p.CHECK_DATE<:CheckDate and {AsOfNow<p>}';

  CheckLinesWithDD = 'select #Group, count (*) cnt ' +
      'from PR_CHECK C, PR_CHECK_LINES L, EE_SCHEDULED_E_DS S, EE E ' +
      'where C.EE_NBR = E.EE_NBR and C.PR_CHECK_NBR = L.PR_CHECK_NBR and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<E>} and ' +
      'L.PR_NBR=:PrNbr and ' +
      'C.PR_NBR = :PrNbr and ' +
      'L.EE_SCHEDULED_E_DS_NBR = S.EE_SCHEDULED_E_DS_NBR and ' +
      '{AsOfNow<S>} and ' +
      'S.EE_DIRECT_DEPOSIT_NBR is not null group by #Group';

  EarningsDeductionsYTDData = 'SELECT p.co_nbr, e.co_division_nbr , e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, cl.custom_e_d_code_number, ' +
      'cl.e_d_code_type, cl.description, #SumField ' +
      'FROM pr p, pr_check c, ee e, pr_check_lines l, cl_e_ds cl ' +
      'WHERE p.co_nbr=:CoNbr and e.ee_nbr=:EeNbr and ' +
      'c.pr_nbr = p.pr_nbr and ' +
      'c.pr_check_nbr = l.pr_check_nbr and ' +
      'c.ee_nbr = e.ee_nbr and ' +
      'l.cl_e_ds_nbr = cl.cl_e_ds_nbr and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<l>} and ' +
      '{AsOfNow<cl>} and ' +
      '(p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      'p.{PayrollProcessedClause} ' +
      '#Filter ' +
      'GROUP BY p.co_nbr, e.co_division_nbr , e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, cl.custom_e_d_code_number, ' +
      'cl.description, cl.e_d_code_type ';

  FederalTaxesYTDData = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, ' +
      'SUM(c.federal_tax) federal_tax, ' +
      'SUM(c.federal_taxable_wages) federal_taxable_wages, ' +
      'SUM(c.ee_oasdi_tax) ee_oasdi_tax, ' +
      'SUM(c.ee_oasdi_taxable_wages + c.ee_oasdi_taxable_tips) ee_oasdi_taxable_wages, ' +
      'SUM(c.ee_medicare_tax) ee_medicare_tax, ' +
      'SUM(c.ee_medicare_taxable_wages) ee_medicare_taxable_wages, ' +
      'SUM(c.ee_eic_tax)*-1 ee_eic_tax, ' +
      'SUM(c.or_check_back_up_withholding) back_up_withholding, ' +
      'SUM(c.er_oasdi_tax) er_oasdi_tax, ' +
      'SUM(c.er_medicare_tax) er_medicare_tax, ' +
      'SUM(c.er_fui_tax) er_fui_tax ' +
      'FROM pr p JOIN ' +
      'pr_check c ON ' +
      'p.pr_nbr = c.pr_nbr JOIN ' +
      'ee e ON ' +
      'c.ee_nbr = e.ee_nbr ' +
      'WHERE e.co_nbr=:CoNbr AND ' +
      '(p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<e>} and ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr';

  FederalTaxesYTDDataYTD = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, ' +
      'SUM(c.federal_tax) federal_tax, ' +
      'SUM(c.federal_taxable_wages) federal_taxable_wages, ' +
      'SUM(c.ee_oasdi_tax) ee_oasdi_tax, ' +
      'SUM(c.ee_oasdi_taxable_wages + c.ee_oasdi_taxable_tips) ee_oasdi_taxable_wages, ' +
      'SUM(c.ee_medicare_tax) ee_medicare_tax, ' +
      'SUM(c.ee_medicare_taxable_wages) ee_medicare_taxable_wages, ' +
      'SUM(c.ee_eic_tax)*-1 ee_eic_tax, ' +
      'SUM(c.or_check_back_up_withholding) back_up_withholding, ' +
      'SUM(c.er_oasdi_tax) er_oasdi_tax, ' +
      'SUM(c.er_medicare_tax) er_medicare_tax, ' +
      'SUM(c.er_fui_tax) er_fui_tax ' +
      'FROM pr p, pr_check c, ee e ' +
      'WHERE e.co_nbr=:CoNbr AND e.ee_nbr=:EeNbr and  ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<e>} and ' +
      'p.pr_nbr = c.pr_nbr and ' +
      'c.ee_nbr = e.ee_nbr and ' +
      '(p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr';

  FederalTaxesYTDData2 = 'SELECT c.ee_nbr, ' +
      'SUM(c.net_wages) net_wages, ' +
      'SUM(c.federal_tax) federal_tax, ' +
      'SUM(c.federal_taxable_wages) federal_taxable_wages, ' +
      'SUM(c.ee_oasdi_tax) ee_oasdi_tax, ' +
      'SUM(c.ee_oasdi_taxable_wages + c.ee_oasdi_taxable_tips) ee_oasdi_taxable_wages, ' +
      'SUM(c.ee_medicare_tax) ee_medicare_tax, ' +
      'SUM(c.ee_medicare_taxable_wages) ee_medicare_taxable_wages, ' +
      'SUM(c.ee_eic_tax)*-1 ee_eic_tax, ' +
      'SUM(c.or_check_back_up_withholding) back_up_withholding ' +
      'FROM pr p JOIN pr_check c ON p.pr_nbr = c.pr_nbr ' +
      'WHERE (p.pr_nbr=:PrNbr OR p.co_nbr=:CoNbr AND ' +
      '(p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      'p.{PayrollProcessedClause}) and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} #Filter ' +
      'GROUP BY c.ee_nbr';

  LocalTaxesYTDData = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, col.sy_locals_nbr, el.ee_locals_nbr, ' +
      'SUM(cl.local_tax) local_tax, ' +
      'SUM(cl.local_taxable_wage) local_taxable_wage ' +
      'FROM pr p ' +
      'JOIN pr_check c ' +
      'ON p.pr_nbr = c.pr_nbr ' +
      'JOIN ee e ' +
      'ON c.ee_nbr = e.ee_nbr ' +
      'JOIN pr_check_locals cl ' +
      'ON c.pr_check_nbr = cl.pr_check_nbr ' +
      'JOIN ee_locals el ' +
      'ON cl.ee_locals_nbr = el.ee_locals_nbr ' +
      'JOIN co_local_tax col ' +
      'ON el.co_local_tax_nbr = col.co_local_tax_nbr ' +
      'WHERE e.co_nbr=:CoNbr AND ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<cl>} and ' +
      '{AsOfNow<el>} and ' +
      '{AsOfNow<col>} ' +

      'and c.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'and (p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, col.sy_locals_nbr, el.ee_locals_nbr';

  LocalTaxesYTDDataYTD = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, col.sy_locals_nbr, el.ee_locals_nbr, ' +
      'SUM(cl.local_tax) local_tax, ' +
      'SUM(cl.local_taxable_wage) local_taxable_wage ' +
      'FROM pr p, pr_check c, ee e, pr_check_locals cl, ee_locals el, co_local_tax col ' +
      'WHERE e.co_nbr=:CoNbr AND e.ee_nbr=:EeNbr and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<el>} and ' +
      '{AsOfNow<cl>} and ' +
      '{AsOfNow<col>} ' +

      'and c.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'and p.pr_nbr = c.pr_nbr and ' +
      'c.ee_nbr = e.ee_nbr and ' +
      'c.pr_check_nbr = cl.pr_check_nbr and ' +
      'cl.ee_locals_nbr = el.ee_locals_nbr and ' +
      'el.co_local_tax_nbr = col.co_local_tax_nbr and ' +
      '(p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, col.sy_locals_nbr, el.ee_locals_nbr';

  SDIAndStatesTaxesYTDData = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, s.state, es.state_number_withholding_allow, ' +
      'es.state_marital_status, es.ee_states_nbr, ' +
      'SUM(cs.state_tax) state_tax, ' +
      'SUM(cs.state_taxable_wages) state_taxable_wages, ' +
      'SUM(cs.ee_sdi_tax) ee_sdi_tax, ' +
      'SUM(cs.state_taxable_wages) ee_sdi_taxable_wages, ' +
      'SUM(cs.er_sdi_tax) er_sdi_tax ' +
      'FROM pr_check_states cs ' +
      'JOIN ee_states es ON es.ee_states_nbr=cs.ee_states_nbr ' +
      'JOIN co_states s ON es.co_states_nbr=s.co_states_nbr ' +
      'JOIN pr_check c ON c.pr_check_nbr = cs.pr_check_nbr ' +
      'JOIN pr p ON p.pr_nbr = c.pr_nbr ' +
      'JOIN ee e ON c.ee_nbr = e.ee_nbr ' +
      'WHERE e.co_nbr=:CoNbr AND ' +
      '{AsOfNow<cs>} and ' +
      '{AsOfNow<es>} and ' +
      '{AsOfNow<s>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<e>} ' +

      'and c.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'and (p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) AND ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, s.state, es.state_marital_status, ' +
      'es.state_number_withholding_allow, es.ee_states_nbr';

  SDIAndStatesTaxesYTDDataYTD = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, s.state, es.state_number_withholding_allow, ' +
      'es.state_marital_status, es.ee_states_nbr, ' +
      'SUM(cs.state_tax) state_tax, ' +
      'SUM(cs.state_taxable_wages) state_taxable_wages, ' +
      'SUM(cs.ee_sdi_tax) ee_sdi_tax, ' +
      'SUM(cs.state_taxable_wages) ee_sdi_taxable_wages, ' +
      'SUM(cs.er_sdi_tax) er_sdi_tax ' +
      'FROM pr_check_states cs, ee_states es, co_states s, ' +
      'pr_check c, pr p, ee e  ' +
      'WHERE e.co_nbr=:CoNbr AND e.ee_nbr=:EeNbr and  ' +
      '{AsOfNow<cs>} and ' +
      '{AsOfNow<es>} and ' +
      '{AsOfNow<s>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<e>} and ' +
      'es.ee_states_nbr=cs.ee_states_nbr and ' +
      'es.co_states_nbr=s.co_states_nbr and ' +
      'c.pr_check_nbr = cs.pr_check_nbr and ' +
      'p.pr_nbr = c.pr_nbr and ' +
      'c.ee_nbr = e.ee_nbr ' +

      'and c.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'and (p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) AND ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, s.state, es.state_marital_status, ' +
      'es.state_number_withholding_allow, es.ee_states_nbr';

  SUITaxesYTDData = 'SELECT e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, cos.sy_sui_nbr, cos.co_sui_nbr, ' +
      'SUM(cs.sui_tax) sui_tax, ' +
      'SUM(cs.sui_taxable_wages) sui_taxable_wages ' +
      'FROM pr p, pr_check c, ee e, pr_check_sui cs, co_sui cos ' +
      'WHERE e.co_nbr=:CoNbr AND e.ee_nbr=:EeNbr and  ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<cs>} and ' +
      '{AsOfNow<cos>} ' +

      'and c.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'and p.pr_nbr = c.pr_nbr and ' +
      'c.ee_nbr = e.ee_nbr and ' +
      'c.pr_check_nbr = cs.pr_check_nbr and ' +
      'cs.co_sui_nbr = cos.co_sui_nbr and ' +
      '(p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
      '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
      'p.{PayrollProcessedClause} ' +
      'GROUP BY e.co_nbr, e.co_division_nbr, e.co_branch_nbr, e.co_department_nbr, ' +
      'e.co_team_nbr, e.ee_nbr, cos.sy_sui_nbr, cos.co_sui_nbr ' +
      'HAVING SUM(cs.sui_tax) <> 0';

  ExportAR = 'select b.pr_nbr, b.invoice_number, ' +
      'b.exported_to_a_r, b.invoice_date ' +
      'from co_billing_history b ' +
      'where b.exported_to_a_r=''N'' and ' +
      'b.co_nbr = :conbr and ' +
      'b.invoice_date between :BeginDate and :EndDate and ' +
      '{AsOfNow<b>}';

  GetPriorPRForEE = 'select P.PR_NBR, P.CHECK_DATE from PR P ' +
      'join PR_CHECK C on P.PR_NBR=C.PR_NBR ' +
      'where C.PR_CHECK_NBR=(select max(PR_CHECK_NBR) ' +
      'from PR_CHECK ' +
      'where EE_NBR=:EENbr and ' +
      'PR_CHECK_NBR<:CheckNbr and ' +
      'CHECK_TYPE=:CheckType and ' +
      '{AsOfNow<PR_CHECK>}) and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<C>}';

  FederalShortFall = 'select PR_CHECK_NBR, FEDERAL_SHORTFALL from PR_CHECK ' +
      'where PR_CHECK_NBR=(select max(PR_CHECK_NBR) ' +
      'from PR_CHECK ' +
      'where EE_NBR=:EENbr and ' +
      'PR_CHECK_NBR<:CheckNbr and ' +
      'DISABLE_SHORTFALLS=''N'' and ' +
      'CHECK_TYPE in (#CheckType) and ' +
      '{AsOfNow<PR_CHECK>}) and {AsOfNow<PR_CHECK>}';

  StateShortfall = 'select PR_CHECK_NBR, STATE_SHORTFALL ' +
      'from PR_CHECK_STATES ' +
      'where PR_CHECK_STATES_NBR=(select max(S.PR_CHECK_STATES_NBR) ' +
      'from PR_CHECK_STATES S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'where S.EE_STATES_NBR=:EEStateNbr and ' +
      'C.PR_CHECK_NBR<:CheckNbr and ' +
      'C.DISABLE_SHORTFALLS=''N'' and ' +
      'C.CHECK_TYPE in (#CheckType) and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>}) and ' +
      '{AsOfNow<PR_CHECK_STATES>}';

  LocalShortfall = 'select PR_CHECK_NBR, LOCAL_SHORTFALL ' +
      'from PR_CHECK_LOCALS ' +
      'where PR_CHECK_LOCALS_NBR=(select max(L.PR_CHECK_LOCALS_NBR) ' +
      'from PR_CHECK_LOCALS L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'where L.EE_LOCALS_NBR=:EELocalNbr and ' +
      'C.PR_CHECK_NBR<:CheckNbr and ' +
      'C.DISABLE_SHORTFALLS=''N'' and ' +
      'C.CHECK_TYPE=:CheckType and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>}) and ' +
      '{AsOfNow<PR_CHECK_LOCALS>}';

  EESDITaxes = 'select S.PR_CHECK_STATES_NBR, S.EE_SDI_TAXABLE_WAGES, S.EE_SDI_TAX, P.CHECK_DATE ' +
      'from PR_CHECK_STATES S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where S.EE_STATES_NBR=:EEStateNbr and ' +
      'C.EE_NBR=:EENbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      'P.PR_NBR<>:PRNbr and ' +
      'P.{PayrollProcessedClause} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ' +

      'and c.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

      'order by P.CHECK_DATE, P.RUN_NUMBER, S.PR_CHECK_STATES_NBR';

  StateTaxes = 'select #Columns ' +
      'from PR_CHECK_STATES S ' +
      'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +
      'where S.EE_STATES_NBR=:EEStateNbr and ' +
      'C.EE_NBR=:EENbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      'P.PR_NBR<>:PRNbr and ' +
      'P.{PayrollProcessedClause} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ';

  EESUITaxes = 'select #Columns ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where C.EE_NBR=:EENbr and ' +
      '#Filter ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      'P.PR_NBR<>:PrNbr and ' +
      'P.{PayrollProcessedClause} ' +
      'and C.CHECK_STATUS<>''V'' ' +
      'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
      'and {AsOfNow<S>} ' +
      'and {AsOfNow<C>} ' +
      'and {AsOfNow<P>} ' +
      '#Order';

  DeductGarnishment = 'select L.PR_CHECK_LINES_NBR, L.AMOUNT, L.LINE_TYPE, E.E_D_CODE_TYPE, E.PRIORITY_TO_EXCLUDE, S.PRIORITY_NUMBER ' +
      'from PR_CHECK_LINES L ' +
      'join CL_E_DS E ' +
      'on L.CL_E_DS_NBR=E.CL_E_DS_NBR ' +
      'join EE_SCHEDULED_E_DS S ' +
      'on S.EE_SCHEDULED_E_DS_NBR=L.EE_SCHEDULED_E_DS_NBR ' +
      'where L.PR_CHECK_NBR=:CheckNbr and ' +
      'E.E_D_CODE_TYPE=:CodeType and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<E>} ' +
      'order by E.PRIORITY_TO_EXCLUDE, S.PRIORITY_NUMBER';

  PriorCheckDeductionsShortfalls = 'select sum(L.DEDUCTION_SHORTFALL_CARRYOVER) SUM_AMOUNT, max(L.CL_AGENCY_NBR) MAX_NBR, ' +
      'L.PR_CHECK_NBR, L.CL_E_DS_NBR, L.EE_SCHEDULED_E_DS_NBR ' +
      'from PR_CHECK_LINES L ' +
      'join CL_E_DS D ' +
      'on L.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
      'where L.PR_CHECK_NBR=(select max(C.PR_CHECK_NBR) ' +
      'from PR_CHECK C ' +
      'where C.PR_CHECK_NBR<:PrNbr and ' +
      'C.CHECK_TYPE in (#CheckType) and ' +
      'C.EE_NBR=:EeNbr and ' +
      '{AsOfNow<C>}) and ' +
      'D.MAKE_UP_DEDUCTION_SHORTFALL=''Y'' and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<D>} ' +
      'group by L.PR_CHECK_NBR, L.CL_E_DS_NBR, L.EE_SCHEDULED_E_DS_NBR order by L.CL_E_DS_NBR';

  AgencyChecks = 'select #Columns ' +
      'from PR_MISCELLANEOUS_CHECKS M ' +
      'join CL_AGENCY A ' +
      'on M.CL_AGENCY_NBR=A.CL_AGENCY_NBR ' +
      'join PR P ' +
      'on P.PR_NBR=M.PR_NBR ' +
      'join CO C ' +
      'on C.CO_NBR=P.CO_NBR ' +
      'where #Filter ' +
      '{AsOfNow<M>} and ' +
      'M.MISCELLANEOUS_CHECK_TYPE in (#CheckType) and ' +
      '{AsOfNow<A>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<C>} and ' +
      'M.MISCELLANEOUS_CHECK_AMOUNT>=0 ' +
      'order by M.PAYMENT_SERIAL_NUMBER';

  TaxChecks = 'select #Columns ' +
      'from PR_MISCELLANEOUS_CHECKS M ' +
      'join PR P ' +
      'on P.PR_NBR=M.PR_NBR ' +
      'join CO C ' +
      'on C.CO_NBR=P.CO_NBR ' +
      'where #Filter ' +
      '{AsOfNow<M>} and ' +
      'M.MISCELLANEOUS_CHECK_TYPE in (#CheckType) and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<C>} ' +
      'order by M.PAYMENT_SERIAL_NUMBER';

  Checks = 'select R.CHECK_DATE, B.PERIOD_BEGIN_DATE, B.PERIOD_END_DATE, C.NET_WAGES, C.PAYMENT_SERIAL_NUMBER, ' +
      'P.SOCIAL_SECURITY_NUMBER, P.FIRST_NAME, P.LAST_NAME, P.MIDDLE_INITIAL, P.ADDRESS1, P.ADDRESS2, P.CITY, P.STATE, P.ZIP_CODE, ' +
      'E.FEDERAL_MARITAL_STATUS, E.CUSTOM_EMPLOYEE_NUMBER, E.CURRENT_HIRE_DATE, CO.CUSTOM_COMPANY_NUMBER, CO.NAME, ' +
      'CO.ADDRESS1, CO.ADDRESS2, CO.CITY, CO.STATE, CO.ZIP_CODE, E.NUMBER_OF_DEPENDENTS, C.PR_CHECK_NBR, C.EE_NBR, C.GROSS_WAGES, ' +
      'E.CO_DIVISION_NBR, E.CO_BRANCH_NBR, E.CO_DEPARTMENT_NBR, E.CO_TEAM_NBR, C.CUSTOM_PR_BANK_ACCT_NUMBER, CB.DATA CHECK_COMMENTS, ' +
      'C.CHECK_TYPE, CO.FEIN, CO.CO_NBR, R.RUN_NUMBER, E.ADDRESS1 EE_ADDRESS1, E.ADDRESS2 EE_ADDRESS2, E.PRINT_VOUCHER, ' +
      'E.CITY EE_CITY, E.STATE EE_STATE, E.ZIP_CODE EE_ZIP_CODE, V.CUSTOM_DIVISION_NUMBER, A.CUSTOM_BRANCH_NUMBER, ' +
      'D.CUSTOM_DEPARTMENT_NUMBER, T.CUSTOM_TEAM_NUMBER, R.STATUS, R.PROCESS_DATE, C.ABA_NUMBER ' +
      'from PR_CHECK C ' +
      'join EE E ' +
      'on E.EE_NBR=C.EE_NBR ' +
      'join CL_PERSON P ' +
      'on P.CL_PERSON_NBR=E.CL_PERSON_NBR ' +
      'join PR R ' +
      'on R.PR_NBR=C.PR_NBR ' +
      'join PR_BATCH B ' +
      'on B.PR_BATCH_NBR=C.PR_BATCH_NBR ' +
      'left outer join CL_BLOB CB on C.NOTES_NBR = CB.CL_BLOB_NBR and {AsOfNow<CB>} ' +
      'join CO CO ' +
      'on CO.CO_NBR=R.CO_NBR ' +
      'left outer join CO_DIVISION V on E.CO_DIVISION_NBR=V.CO_DIVISION_NBR and {AsOfNow<V>} ' +
      'left outer join CO_BRANCH A on E.CO_BRANCH_NBR=A.CO_BRANCH_NBR and {AsOfNow<A>} ' +
      'left outer join CO_DEPARTMENT D on E.CO_DEPARTMENT_NBR=D.CO_DEPARTMENT_NBR and {AsOfNow<D>} ' +
      'left outer join CO_TEAM T on E.CO_TEAM_NBR=T.CO_TEAM_NBR and {AsOfNow<T>} ' +
      'where #FilterCondition ' +
      '(not C.NET_WAGES<0) and ' +
      '((not C.GROSS_WAGES<0) or (C.GROSS_WAGES<0 and C.NET_WAGES>0)) and ' +
      'C.CHECK_TYPE <> ''' + CHECK_TYPE2_VOID + ''' and C.CHECK_TYPE <> ''' + CHECK_TYPE2_COBRA_CREDIT + ''' and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<E>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<R>} and ' +
      '{AsOfNow<B>} and ' +
      '{AsOfNow<CO>}';

  Companies = 'select DISTINCT p.*, c.name, c.custom_company_number, c.customer_service_sb_user_nbr, ' +
      '''#RepName'' CO_REP, l.custom_client_number, l.name ClName ' +
      'from pr_scheduled_event p, co c, cl l ' +
      'where c.Co_Nbr in (#InValues) and ' +
      'p.co_nbr=c.co_nbr and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<c>} and ' +
      '{AsOfNow<l>}  ' +
      'order by pr_scheduled_event_nbr';

  SDIDiscrepancies = 'select P.CHECK_DATE, S.EE_STATES_NBR, S.ER_SDI_TAXABLE_WAGES, S.ER_SDI_TAX ' +
      'from PR_CHECK_STATES S ' +
      'join PR P ' +
      'on S.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<P>} ' +
      'order by S.EE_STATES_NBR, P.CHECK_DATE';

  SUIDiscrepancies = 'select P.CHECK_DATE, C.EE_NBR, S.CO_SUI_NBR, S.SUI_TAXABLE_WAGES, S.SUI_TAX ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C ' +
      'on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ' +
      'order by P.CHECK_DATE';

  ERFedDiscrepancies = 'select C.PR_NBR, P.CHECK_DATE, sum(C.ER_OASDI_TAXABLE_WAGES + C.ER_OASDI_TAXABLE_TIPS) ' +
      'ER_OASDI_TAXABLE_WAGES, sum(C.ER_OASDI_TAX) ER_OASDI_TAX, sum(C.ER_MEDICARE_TAXABLE_WAGES) ER_MEDICARE_TAXABLE_WAGES, ' +
      'sum(C.ER_MEDICARE_TAX) ER_MEDICARE_TAX, sum(C.ER_FUI_TAXABLE_WAGES) ER_FUI_TAXABLE_WAGES, sum(C.ER_FUI_TAX) ER_FUI_TAX ' +
      'from PR_CHECK C ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ' +
      'group by P.CHECK_DATE, C.PR_NBR';

  ERSDIDiscrepancies = 'select S.PR_NBR, P.CHECK_DATE, ES.CO_STATES_NBR, sum(S.ER_SDI_TAXABLE_WAGES) ' +
      'ER_SDI_TAXABLE_WAGES, sum(S.ER_SDI_TAX) ER_SDI_TAX ' +
      'from PR_CHECK_STATES S ' +
      'join EE_STATES ES ' +
      'on S.EE_STATES_NBR=ES.EE_STATES_NBR ' +
      'join PR P ' +
      'on S.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      '{AsOfNow<ES>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<P>} ' +
      'group by S.PR_NBR, P.CHECK_DATE, ES.CO_STATES_NBR ' +
      'order by ES.CO_STATES_NBR, P.CHECK_DATE';

  ERSUIDiscrepancies = 'select S.PR_NBR, P.CHECK_DATE, S.CO_SUI_NBR, ' +
      'sum(S.SUI_TAXABLE_WAGES) SUI_TAXABLE_WAGES, sum(S.SUI_TAX) SUI_TAX ' +
      'from PR_CHECK_SUI S ' +
      'join PR P ' +
      'on S.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.CHECK_DATE between :StartDate and :EndDate and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<P>} ' +
      'group by P.CHECK_DATE, S.PR_NBR, S.CO_SUI_NBR';

  SumAgencyChecks = 'select sum(L.AMOUNT) ' +
    'from PR P, PR_CHECK_LINES L ' +
    'where L.CL_AGENCY_NBR=:AgencyNbr and ' +
    'L.AGENCY_STATUS=:AgencyStatus and ' +
    '{AsOfNow<L>} and ' +
    'L.PR_NBR = P.PR_NBR and ' +
    'P.CO_NBR=:CoNbr and ' +
    'P.{PayrollProcessedClause} and ' +
    'P.CHECK_DATE>=''01/01/2001'' and ' + // this is because Des wanted all Agency Check lines prior to the year 2001 to be ignored
    'P.CHECK_DATE<=:CheckDate and ' +
    '{AsOfNow<P>}';

  CalcAgencyChecks = 'select #Columns ' +
    'from PR P, PR_CHECK_LINES L ' +
    'where L.CL_AGENCY_NBR=:AgencyNbr and ' +
    'L.AGENCY_STATUS=:AgencyStatus and ' +
    '{AsOfNow<L>} and ' +
    'L.PR_NBR = P.PR_NBR and ' +
    'P.CO_NBR=:CoNbr and ' +
    'P.{PayrollProcessedClause} and ' +
    'P.CHECK_DATE>=''01/01/2001'' and ' + // this is because Des wanted all Agency Check lines prior to the year 2001 to be ignored
    'P.CHECK_DATE<=:CheckDate and ' +
    '{AsOfNow<P>}';

  CalcAgencyChecksForEE = 'select #Columns ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.{PayrollProcessedClause} and ' +
      'L.CL_AGENCY_NBR=:AgencyNbr and ' +
      'L.AGENCY_STATUS=:AgencyStatus and ' +
      'P.CHECK_DATE>=''01/01/2001'' and ' + // this is because Des wanted all Agency Check lines prior to the year 2001 to be ignored
      'P.CHECK_DATE<=:CheckDate and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>}';

  CalcAgencyChecksForEETargetMet = 'select #Columns ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'join EE_SCHEDULED_E_DS S ' +
      'on L.EE_SCHEDULED_E_DS_NBR=S.EE_SCHEDULED_E_DS_NBR ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.{PayrollProcessedClause} and ' +
      'L.CL_AGENCY_NBR=:AgencyNbr and ' +
      'L.AGENCY_STATUS=:AgencyStatus and ' +
      'P.CHECK_DATE>=''01/01/2001'' and ' + // this is because Des wanted all Agency Check lines prior to the year 2001 to be ignored
      'P.CHECK_DATE<=:CheckDate and ' +
      'S.BALANCE=0 and ' +
      'S.TARGET_ACTION=''C'' and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>}';

  CalcAgencyChecksByEE = 'select C.EE_NBR, sum(L.AMOUNT) ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.{PayrollProcessedClause} and ' +
      'L.CL_AGENCY_NBR=:AgencyNbr and ' +
      'L.AGENCY_STATUS=:AgencyStatus and ' +
      'P.CHECK_DATE>=''01/01/2001'' and ' + // this is because Des wanted all Agency Check lines prior to the year 2001 to be ignored
      'P.CHECK_DATE<=:CheckDate and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ' +
      'group by C.EE_NBR';

  CalcAgencyChecksByEETargetMet = 'select C.EE_NBR, sum(L.AMOUNT) ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P ' +
      'on C.PR_NBR=P.PR_NBR ' +
      'join EE_SCHEDULED_E_DS S ' +
      'on L.EE_SCHEDULED_E_DS_NBR=S.EE_SCHEDULED_E_DS_NBR ' +
      'where P.CO_NBR=:CoNbr and ' +
      'P.{PayrollProcessedClause} and ' +
      'L.CL_AGENCY_NBR=:AgencyNbr and ' +
      'L.AGENCY_STATUS=:AgencyStatus and ' +
      'P.CHECK_DATE>=''01/01/2001'' and ' + // this is because Des wanted all Agency Check lines prior to the year 2001 to be ignored
      'P.CHECK_DATE<=:CheckDate and ' +
      'S.BALANCE=0 and ' +
      'S.TARGET_ACTION=''C'' and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} ' +
      'group by C.EE_NBR';

  CalcCheckLineForAgencyCheck = 'select L.PR_CHECK_LINES_NBR, C.EE_NBR ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'where C.PR_NBR=:PrNbr and ' +
      'L.PR_NBR=:PrNbr and ' +
      'L.CL_AGENCY_NBR=:AgencyNbr and ' +
      'L.AGENCY_STATUS=:AgencyStatus and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>}';

  CalcCheckLineForAgencyCheckTargetMet = 'select L.PR_CHECK_LINES_NBR, C.EE_NBR ' +
      'from PR_CHECK_LINES L ' +
      'join PR_CHECK C ' +
      'on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join EE_SCHEDULED_E_DS S ' +
      'on L.EE_SCHEDULED_E_DS_NBR=S.EE_SCHEDULED_E_DS_NBR ' +
      'where C.PR_NBR=:PrNbr and ' +
      'L.PR_NBR=:PrNbr and ' +
      'L.CL_AGENCY_NBR=:AgencyNbr and ' +
      'L.AGENCY_STATUS=:AgencyStatus and ' +
      'S.BALANCE=0 and ' +
      'S.TARGET_ACTION=''C'' and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>}';

  SumAllTaxesForPr = 'select sum(FEDERAL_TAX), sum(EE_OASDI_TAX), ' +
      'sum(EE_MEDICARE_TAX), sum(EE_EIC_TAX), sum(OR_CHECK_BACK_UP_WITHHOLDING) ' +
      'from PR_CHECK p ' +
      'where PR_NBR=:PrNbr and ' +
      '#Filter and {AsOfNow<p>}';

  Sum945TaxesForPr = 'select sum(FEDERAL_TAX) ' +
      'from PR_CHECK p ' +
      'where PR_NBR=:PrNbr and CHECK_TYPE_945=''Y'' and ' +
      '#Filter and {AsOfNow<p>}';

  ThirdPartyStateLiab = 'select E.CO_STATES_NBR, sum(S.STATE_TAX), sum(S.EE_SDI_TAX), T.SY_STATE_DEPOSIT_FREQ_NBR, T.SY_STATES_NBR ' +
      'from PR_CHECK_STATES S ' +
      'join PR_CHECK C ' +
      'on C.PR_CHECK_NBR=S.PR_CHECK_NBR ' +
      'join EE_STATES E ' +
      'on E.EE_STATES_NBR=S.EE_STATES_NBR ' +
      'join CO_STATES T ' +
      'on T.CO_STATES_NBR=E.CO_STATES_NBR ' +
      'where C.PR_NBR=:PrNbr and ' +
      'S.PR_NBR=:PrNbr and ' +
      '#Filter and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<T>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<E>} ' +
      'group by E.CO_STATES_NBR, T.SY_STATE_DEPOSIT_FREQ_NBR, T.SY_STATES_NBR';

  ThirdPartyIndependent = 'select E.CO_STATES_NBR, sum(S.ER_SDI_TAX), T.SY_STATE_DEPOSIT_FREQ_NBR, T.SY_STATES_NBR ' +
      'from PR_CHECK_STATES S ' +
      'join EE_STATES E ' +
      'on E.EE_STATES_NBR=S.EE_STATES_NBR ' +
      'join CO_STATES T ' +
      'on T.CO_STATES_NBR=E.CO_STATES_NBR ' +
      'where S.PR_NBR=:PrNbr and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<T>} and ' +
      '{AsOfNow<E>} ' +
      'group by E.CO_STATES_NBR, T.SY_STATE_DEPOSIT_FREQ_NBR, T.SY_STATES_NBR';

  ThirdPartySUILiab = 'select S.CO_SUI_NBR, sum(S.SUI_TAX) ' +
      'from PR_CHECK_SUI S ' +
      'join PR_CHECK C ' +
      'on C.PR_CHECK_NBR=S.PR_CHECK_NBR ' +
      'where C.PR_NBR=:PrNbr and ' +
      'S.PR_NBR=:PrNbr and ' +
      '#Filter and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<C>} ' +
      'group by S.CO_SUI_NBR';

  ThirdPartyLocalLiab = 'select E.CO_LOCAL_TAX_NBR, sum(L.LOCAL_TAX) ' +
    'from PR_CHECK_LOCALS L ' +
    'join PR_CHECK C ' +
    'on C.PR_CHECK_NBR=L.PR_CHECK_NBR ' +
    'join EE_LOCALS E ' +
    'on E.EE_LOCALS_NBR=L.EE_LOCALS_NBR ' +
    'where C.PR_NBR=:PrNbr and ' +
    'L.PR_NBR=:PrNbr and ' +
    '#Filter and ' +
    '{AsOfNow<L>} and ' +
    '{AsOfNow<C>} and ' +
    '{AsOfNow<E>} ' +
    'group by E.CO_LOCAL_TAX_NBR';

  SelectPayrollAccountRegister = 'select r.* from CO_BANK_ACCOUNT_REGISTER r, pr_check c ' +
    'where {AsOfNow<c>} and {AsOfNow<r>} and ' +
    'c.pr_nbr = :PrNbr and r.pr_check_nbr = c.pr_check_nbr ' +
    'union ' +
    'select r.* from CO_BANK_ACCOUNT_REGISTER r, CO_PR_ACH c ' +
    'where {AsOfNow<c>} and {AsOfNow<r>} and ' +
    'c.pr_nbr = :PrNbr and r.CO_PR_ACH_nbr = c.CO_PR_ACH_nbr ' +
    'union ' +
    'select r.* from CO_BANK_ACCOUNT_REGISTER r, PR_MISCELLANEOUS_CHECKS c ' +
    'where {AsOfNow<c>} and {AsOfNow<r>} and ' +
    'c.pr_nbr = :PrNbr and r.PR_MISCELLANEOUS_CHECKS_nbr = c.PR_MISCELLANEOUS_CHECKS_nbr';

  CustomCompanySelect = 'select co.CO_NBR #ExtraFields from CO co #AdditTable where #Condition AND {AsOfNow<CO>}';

  AllPRCheckLines = 'select E.CUSTOM_EMPLOYEE_NUMBER, C.PAYMENT_SERIAL_NUMBER, L.PR_CHECK_LINES_NBR, ' +
    'L.CL_E_DS_NBR, L.AGENCY_STATUS, L.CL_AGENCY_NBR, L.PR_MISCELLANEOUS_CHECKS_NBR, ' +
    'L.HOURS_OR_PIECES, L.RATE_OF_PAY, L.AMOUNT, ''123456789012345678901234567890'' AGENCY_NAME ' +
    'from PR_CHECK_LINES L ' +
    'join PR_CHECK C ' +
    'on C.PR_CHECK_NBR=L.PR_CHECK_NBR ' +
    'join EE E ' +
    'on E.EE_NBR=C.EE_NBR ' +
    'join PR P ' +
    'on P.PR_NBR=C.PR_NBR ' +
    'where {AsOfNow<L>}  and ' +
    '{AsOfNow<C>} and ' +
    '{AsOfNow<E>} and ' +
    '{AsOfNow<P>} and ' +
    'L.PR_NBR=:PrNbr and ' +
    'P.PR_NBR=:PrNbr ' +
    'order by E.CUSTOM_EMPLOYEE_NUMBER';


  GetPretaxCount = 'select C.#Group, Count(distinct #Columns) cnt ' +
      'from EE_SCHEDULED_E_DS L ' +
      'join EE C ' +
      'on C.EE_NBR=L.EE_NBR ' +
      'join CL_E_DS E ' +
      'on E.CL_E_DS_NBR=L.CL_E_DS_NBR ' +
      'where {AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<E>} and ' +
      'C.CO_NBR=:CoNbr and ' +
      'C.CURRENT_TERMINATION_CODE = :Code and ' +
      'E.E_D_CODE_TYPE=:CodeType ' +
      'group by C.#Group';

  GetCheckTimeOfAccruals = 'select E.#Group, Count(distinct #Columns) cnt ' +
      'from EE E ' +
      'join EE_TIME_OFF_ACCRUAL A ' +
      'on A.EE_NBR=E.EE_NBR ' +
      'where {AsOfNow<A>} and ' +
      '{AsOfNow<E>} and ' +
      'E.CURRENT_TERMINATION_CODE = :Code and ' +
      'E.CO_NBR=:CoNbr ' +
      'group by E.#Group';

  GetUnpaidEFTPDebits =  'select a.TAX_PAYMENT_REFERENCE_NUMBER, a.CO_NBR, a.CL_NBR from TMP_CO_TAX_DEPOSITS a, TMP_CL b, TMP_CO c ' +
               ' where STATUS = :STATUS and DEPOSIT_TYPE = :DEPOSIT_TYPE and  a.CL_NBR = b.CL_NBR and ' +
                                        ' a.CL_NBR = c.CL_NBR and a.CO_NBR = c.CO_NBR #cond ';

  ReturnsQueue = 'select '+
      'q.CO_TAX_RETURN_QUEUE_NBR, q.cl_nbr, q.co_nbr, q.status, q.status_date, '+
      'q.SY_REPORTS_GROUP_NBR, n.CL_CO_CONSOLIDATION_NBR, q.PRODUCE_ASCII_FILE, '+
      'q.SB_COPY_PRINTED, q.CLIENT_COPY_PRINTED, q.AGENCY_COPY_PRINTED,'+
      'q.PERIOD_END_DATE, q.DUE_DATE, '+
      't2.HOLD_RETURN_QUEUE, t2.TAX_SERVICE, t2.name co_name, t2.CUSTOM_COMPANY_NUMBER co_number, '+
      't1.CUSTOM_CLIENT_NUMBER cl_number, q.SY_GL_AGENCY_REPORT_NBR '+
    'from TMP_CO_TAX_RETURN_QUEUE q '+
    'join TMP_CO t2 on t2.cl_nbr = q.cl_nbr and t2.co_nbr = q.co_nbr '+
    'join TMP_CL t1 on t1.cl_nbr = q.cl_nbr '+
    'left outer join TMP_CL_CO_CONSOLIDATION n on n.cl_nbr=q.cl_nbr and n.CL_CO_CONSOLIDATION_NBR=q.CL_CO_CONSOLIDATION_NBR and SCOPE='''+ CONSOLIDATION_SCOPE_ALL+ ''' '+
    'where q.#Date between :BegDate and :EndDate '+
      '#Cond '+
    'order by q.SY_REPORTS_GROUP_NBR, q.SY_GL_AGENCY_REPORT_NBR ';


  QuarterFuiWages = 'select sum(c.ER_FUI_TAXABLE_WAGES) wages '+
      'from PR_CHECK c, PR p '+
      'where '+
        'p.co_nbr = :CoNbr and p.check_date between :BegDate and :EndDate and {AsOfNow<p>} and '+
        'p.{PayrollProcessedClause} and '+
        'c.pr_nbr=p.pr_nbr and {AsOfNow<c>}';


  QuarterSuiWages = 'select s.co_sui_nbr, sum(s.SUI_TAXABLE_WAGES) wages '+
      'from PR p, PR_CHECK_SUI s '+
      'where p.check_date between :BegDate and :EndDate and {AsOfNow<p>} and '+
        'p.co_nbr = :CoNbr and p.{PayrollProcessedClause} and '+
        's.pr_nbr=p.pr_nbr and {AsOfNow<s>} '+
        'group by s.co_sui_nbr';


  LiabStatusedRight = '(('+
       'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_NONE+ ''' or '+
       'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP+ ''' or '+
       'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END+ ''' or '+
       'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_ROUNDING_TO_DOLLAR+ ''' or '+
       'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION+ ''' or '+
       'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_OTHER+ '''))';


  Consolidations = 'select n.*, c.co_nbr from TMP_CL_CO_CONSOLIDATION n, TMP_CO c '+
      'where c.CL_CO_CONSOLIDATION_NBR = n.CL_CO_CONSOLIDATION_NBR and '+
      'c.cl_nbr=n.cl_nbr #COND';

  ConsolidationsPreProcessStatus = 'select c1.cl_nbr, c1.CL_CO_CONSOLIDATION_NBR, n.PRIMARY_CO_NBR, c1.co_nbr, '+
       'c1.LAST_PREPROCESS, c1.LAST_PREPROCESS_MESSAGE, '+
       'min(c2.LAST_PREPROCESS) LAST_PREPROCESS_CONS, ' +
       ' max(c2.LAST_PREPROCESS_MESSAGE_NEW) LAST_PREPROCESS_MESSAGE_CONS #FIELDS ,'+
       'CL.CUSTOM_CLIENT_NUMBER,c1.CUSTOM_COMPANY_NUMBER ' +
       'from TMP_CO c1 '+
       'left outer JOIN TMP_CL CL on (CL.CL_NBR = c1.CL_NBR) '+
       'left outer JOIN TMP_CL_CO_CONSOLIDATION n '+
       'ON n.cl_nbr = c1.cl_nbr and n.PRIMARY_CO_NBR = c1.CO_NBR and n.CL_CO_CONSOLIDATION_NBR = c1.CL_CO_CONSOLIDATION_NBR and n.SCOPE='''+ CONSOLIDATION_SCOPE_ALL+ ''' '+
       'left outer JOIN ' +
             ' (select a.CL_NBR, a.CL_CO_CONSOLIDATION_NBR, a.LAST_PREPROCESS_MESSAGE, a.LAST_PREPROCESS, ' +
                    ' CASE SUBSTRING(a.LAST_PREPROCESS_MESSAGE from 1 for 19) ' +
                      ' when ''Manually Reconciled'' then '' Manually Reconciled''  ' +
                      ' else a.LAST_PREPROCESS_MESSAGE ' +
                    ' end LAST_PREPROCESS_MESSAGE_NEW ' +
                  ' from TMP_CO  a  ) c2 ' +
       'ON c2.cl_nbr = n.cl_nbr and c2.CL_CO_CONSOLIDATION_NBR = n.CL_CO_CONSOLIDATION_NBR '+
       'and not c2.CL_CO_CONSOLIDATION_NBR is null '+
       '#cond group by c1.cl_nbr,CL.CUSTOM_CLIENT_NUMBER, c1.CL_CO_CONSOLIDATION_NBR, n.PRIMARY_CO_NBR, c1.co_nbr,c1.CUSTOM_COMPANY_NUMBER, c1.LAST_PREPROCESS, c1.LAST_PREPROCESS_MESSAGE #FIELDS';

  ConsolidationsFull = 'SELECT c.cl_nbr, c.custom_client_number, c.name client_name, ' +
      'co.co_nbr, co.custom_company_number, co.name company_name, ' +
      'co.fein, co.tax_service, co.trust_service, co.termination_code, '+
      ' n.* #FIELDS ' +
      'FROM tmp_cl c ' +
      'JOIN tmp_co co ' +
      'ON c.cl_nbr = co.cl_nbr #CLCOFILTER ' +
      'left outer JOIN TMP_CL_CO_CONSOLIDATION n '+
      'ON n.CL_CO_CONSOLIDATION_NBR = co.CL_CO_CONSOLIDATION_NBR and n.cl_nbr = co.cl_nbr #CONSCOND '+
      'ORDER BY c.cl_nbr, co.co_nbr';

  ConsolidationQuarterSuiWages = 'select cs.co_sui_nbr, sum(s.SUI_TAXABLE_WAGES) wages, sum(s.SUI_GROSS_WAGES) gross_wages '+
      'from CO_SUI cs '+
      'join CO_STATES cst on '+
        'cst.co_states_nbr = cs.co_states_nbr and {AsOfNow<cst>} ' +
      'join CO_STATES cst2 on '+
        'cst2.state = cst.state and {AsOfNow<cst2>} and '+
        '(cst2.co_nbr = cst.co_nbr or exists ('+
          'select co_nbr from co o, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<o>} and {AsOfNow<cn>} and '+
            'o.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            '((cn.PRIMARY_CO_NBR = cst.co_nbr and cs.sy_sui_nbr <> 999118) or  '+  //   NV Business Tax - Special Logic
              ' cs.sy_sui_nbr = 999118) and '+
            'o.co_nbr=cst2.co_nbr and '+
            'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') and '+
            'cn.SCOPE <> '''+ CONSOLIDATION_SCOPE_PAYROLL_ONLY + ''' '+
        ')) '+
      'join CO_SUI cs2 on '+
        'cs2.sy_sui_nbr = cs.sy_sui_nbr and cs2.co_states_nbr = cst2.co_states_nbr and '+
        '{AsOfNow<cs2>} '+
      'join PR p on '+
        'p.check_date between :BegDate and :EndDate and {AsOfNow<p>} and '+
        'p.co_nbr = cs2.co_nbr and '+
        'p.'+ PayrollProcessedClause+
      'join PR_CHECK_SUI s on '+
        's.co_sui_nbr = cs2.co_sui_nbr and s.pr_nbr=p.pr_nbr and {AsOfNow<s>} ' +
      'where cs.co_nbr = :CoNbr and {AsOfNow<cs>} ' +
      'group by cs.co_sui_nbr';

  ConsolidationQuarterLocalWages = 'select cs.CO_LOCAL_TAX_nbr, sum(s.LOCAL_TAXABLE_WAGE) wages, sum(s.LOCAL_GROSS_WAGES) gross_wages '+
      'from CO_LOCAL_TAX cs '+
      'join CO_LOCAL_TAX cst2 on '+
        'cst2.SY_LOCALS_NBR = cs.SY_LOCALS_NBR and {AsOfNow<cst2>} and '+
        '(cst2.co_nbr = cs.co_nbr or exists ('+
          'select co_nbr from co o, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<o>} and {AsOfNow<cn>} and '+
            'o.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            'cn.PRIMARY_CO_NBR = cs.co_nbr and o.co_nbr=cst2.co_nbr and '+
            'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') '+
        ')) '+
      'join PR p on '+
        'p.check_date between :BegDate and :EndDate and {AsOfNow<p>} and '+
        'p.co_nbr = cst2.co_nbr and '+
        'p.'+ PayrollProcessedClause+
      'join EE_LOCALS ll on '+
       ' ll.CO_LOCAL_TAX_nbr = cst2.CO_LOCAL_TAX_nbr and {AsOfNow<ll>} ' +
      'join PR_CHECK_LOCALS s on '+
        's.EE_LOCALS_nbr = ll.EE_LOCALS_nbr and s.pr_nbr=p.pr_nbr and {AsOfNow<s>} ' +
      'where cs.co_nbr = :CoNbr and {AsOfNow<cs>} '+
      'group by cs.CO_LOCAL_TAX_nbr';

  ConsolidationQuarterSuiLiabs = 'select cs.co_sui_nbr, sum(l.amount) liabs '+
      'from CO_SUI cs '+
      'join CO_STATES cst on '+
        'cst.co_states_nbr = cs.co_states_nbr and {AsOfNow<cst>} ' +
      'join CO_STATES cst2 on '+
        'cst2.state = cst.state and {AsOfNow<cst2>} and '+
        '(cst2.co_nbr = cst.co_nbr or exists ('+
          'select co_nbr from co o, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<o>} and {AsOfNow<cn>} and '+
            'o.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            '((cn.PRIMARY_CO_NBR = cst.co_nbr and cs.sy_sui_nbr <> 999118) or  '+  //   NV Business Tax - Special Logic
              ' cs.sy_sui_nbr = 999118) and '+
            'o.co_nbr=cst2.co_nbr and '+
            'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') and '+
            'cn.SCOPE <> '''+ CONSOLIDATION_SCOPE_PAYROLL_ONLY + ''' '+
        ')) '+
      'join CO_SUI cs2 on '+
        'cs2.sy_sui_nbr = cs.sy_sui_nbr and cs2.co_states_nbr = cst2.co_states_nbr and '+
        '{AsOfNow<cs2>} ' +
      'join CO_SUI_LIABILITIES l on '+
        'l.check_date between :BegDate and :EndDate and {AsOfNow<l>} and '+
        'l.co_sui_nbr = cs2.co_sui_nbr and l.co_nbr = cs2.co_nbr and '+ LiabStatusedRight+ ' '+
        ' and (( cst.state = ''MO'' and (l.ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_NONE +''' or ADJUSTMENT_TYPE <> '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_OTHER+ ''')) or cst.state <> ''MO'') ' +
      'where cs.co_nbr = :CoNbr and {AsOfNow<cs>} ' +
      'group by cs.CO_SUI_NBR';

  ConsolidationQuarterLocalLiabs = 'select cs.CO_LOCAL_TAX_nbr, sum(l.amount) liabs '+
      'from CO_LOCAL_TAX cs '+
      'join CO_LOCAL_TAX cst2 on '+
        'cst2.SY_LOCALS_NBR = cs.SY_LOCALS_NBR and {AsOfNow<cst2>} and '+
        '(cst2.co_nbr = cs.co_nbr or exists ('+
          'select co_nbr from co o, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<o>} and {AsOfNow<cn>} and '+
            'o.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            'cn.PRIMARY_CO_NBR = cs.co_nbr and o.co_nbr=cst2.co_nbr and '+
            'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') '+
        ')) '+
      'join CO_LOCAL_TAX_LIABILITIES l on '+
        'l.check_date between :BegDate and :EndDate and {AsOfNow<l>} and '+
        'l.CO_LOCAL_TAX_nbr = cst2.CO_LOCAL_TAX_nbr and '+ LiabStatusedRight+ ' '+
      'where cs.co_nbr = :CoNbr and {AsOfNow<cs>} ' +
      'group by cs.CO_LOCAL_TAX_nbr';

  TmpDepositSummary = 'select cl_nbr, CO_TAX_DEPOSITS_NBR, sum(amount) amount '+
      'from #TableName '+
      'group by cl_nbr, CO_TAX_DEPOSITS_NBR';

  BranchQuery = 'SELECT b.co_branch_nbr, b.name, b.custom_branch_number, b.co_division_nbr ' +
      'FROM co_division d, co_branch b ' +
      'WHERE d.co_nbr = :Co_Nbr and ' +
      'd.co_division_nbr = b.co_division_nbr and ' +
      '{AsOfNow<d>} and ' +
      '{AsOfNow<b>}';

  DepartmentQuery = 'SELECT p.co_department_nbr, p.custom_department_number, ' +
      'p.name, p.co_branch_nbr ' +
      'FROM co_department p, co_branch b, co_division d ' +
      'WHERE d.co_nbr = :Co_Nbr and ' +
      'p.co_branch_nbr = b.co_branch_nbr and ' +
      'b.co_division_nbr = d.co_division_nbr and ' +
      '{AsOfNow<p>} and ' +
      '{AsOfNow<d>} and ' +
      '{AsOfNow<b>}';

  TeamQuery = 'SELECT e.co_team_nbr, e.custom_team_number, e.name, ' +
      'e.co_department_nbr ' +
      'FROM co_team e, co_department p, co_branch b, co_division d ' +
      'WHERE d.co_nbr = :Co_Nbr and ' +
            'e.co_department_nbr = p.co_department_nbr and ' +
            'p.co_branch_nbr = b.co_branch_nbr and ' +
            'b.co_division_nbr = d.co_division_nbr and ' +
            '{AsOfNow<e>} and ' +
            '{AsOfNow<p>} and ' +
            '{AsOfNow<d>} and ' +
            '{AsOfNow<b>}';

  CheckReturnQueueForProcessedReturns = 'select c.* '+
      'from CO_TAX_RETURN_QUEUE c '+
      'where c.period_end_date in (#dates) and {AsOfNow<c>} and '+
        '(c.status = '''+ TAX_RETURN_STATUS_PROCESSED+''' or c.sb_copy_printed = '''+GROUP_BOX_YES+
          ''' or c.client_copy_printed = '''+GROUP_BOX_YES+
          ''' or c.agency_copy_printed = '''+GROUP_BOX_YES+''') and '+
        '(c.co_nbr = :CoNbr or exists ('+
          'select co_nbr from co o, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<o>} and {AsOfNow<cn>} and '+
            'o.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            ' o.co_nbr=c.co_nbr'+
            ' and cn.CL_CO_CONSOLIDATION_NBR = c.CL_CO_CONSOLIDATION_NBR '+
        ')) ';

  GetAllChecksForState = 'select distinct(C.PR_CHECK_NBR), C.PR_NBR, C.EE_NBR from PR_CHECK_LINES L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +
      'join EE_STATES S on L.EE_STATES_NBR=S.EE_STATES_NBR ' +
      'where S.CO_STATES_NBR=:COStateNbr and ' +
      'P.CHECK_DATE between :BeginDate and :EndDate and ' +
      'P.{PayrollProcessedClause} and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<S>}';

  GetAllChecksForSUIState = 'select distinct(C.PR_CHECK_NBR), C.PR_NBR from PR_CHECK_LINES L ' +
      'join PR_CHECK C on L.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +
      'join EE_STATES S on L.EE_SUI_STATES_NBR=S.EE_STATES_NBR ' +
      'where S.SUI_APPLY_CO_STATES_NBR=:COStateNbr and ' +
      'P.PR_NBR=:PrNbr and ' +
      '{AsOfDate<S>} and ' +
      'P.' + PayrollProcessedClause + ' and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<P>}';

  GetUnprocessedPayrolls = 'select count(*) res from pr_scheduled_event ps where scheduled_check_date <= :EndDate and scheduled_check_date > :CurrDate and actual_process_date is null and {AsOfNow<ps>} and '+
      '(co_nbr = :CoNbr or co_nbr in (select c2.co_nbr from co c1, co c2 where {AsOfNow<c1>} and {AsOfNow<c2>} and '+
      'c1.co_nbr = :CoNbr and c1.cl_co_consolidation_nbr=c2.cl_co_consolidation_nbr and c1.cl_co_consolidation_nbr is not null))';

  GetConsolidatedCompanies = 'select o.*, n.PRIMARY_CO_NBR, n.CONSOLIDATION_TYPE, n.cl_co_consolidation_nbr, n.scope from co o, cl_co_consolidation n where {AsOfNow<o>}' +
      ' and {AsOfNow<n>} and '+ 'n.cl_co_consolidation_nbr=o.cl_co_consolidation_nbr and n.cl_co_consolidation_nbr = :Nbr';

  GetConsolidatedCompanies1 = 'select o.CO_NBR, n.PRIMARY_CO_NBR, n.CONSOLIDATION_TYPE, n.CL_CO_CONSOLIDATION_NBR, n.SCOPE  ' +
      'from co o, cl_co_consolidation n where {AsOfNow<o>} ' +
      'and {AsOfNow<n>} ' +
      'and n.cl_co_consolidation_nbr=o.cl_co_consolidation_nbr and ' +
      'n.cl_co_consolidation_nbr = :Nbr ';

  GetEDCountCO = 'select count(l.CL_E_DS_NBR) EdCount '+
                'from PR p join '+
                     'PR_CHECK_LINES l on p.PR_NBR=l.PR_NBR '+
                'where p.CO_NBR=:CoNbr and l.CL_E_DS_NBR=:CledNbr and '+
                      '{AsOfNow<p>} and '+
                      '{AsOfNow<l>}';

  AffectedPersonsTemplate = 'select distinct(CP.CL_PERSON_NBR) from PR_CHECK C ' +
      'join PR P on C.PR_NBR=P.PR_NBR ' +
      'join EE E on E.EE_NBR=C.EE_NBR ' +
      'join CL_PERSON CP on CP.CL_PERSON_NBR=E.CL_PERSON_NBR ' +
      'where P.CO_NBR in (#INSTATEMENT) and ' +
      'P.PROCESS_DATE>:ProcessDate and ' +
      'P.CHECK_DATE<=:CheckDate and ' +
      'P.{PayrollProcessedClause} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<E>} and ' +
      '{AsOfNow<CP>} and ' +
      '{AsOfNow<C>}';

  GetPayrollForEDRefresh = 'select distinct L.NAME CL_NAME, L.CL_NBR, ' +
                       'O.NAME CO_NAME, O.CO_NBR, C.EE_NBR, E.CUSTOM_EMPLOYEE_NUMBER, ' +
                       'N.FIRST_NAME, N.MIDDLE_INITIAL, N.LAST_NAME, ' +
                       'P.PR_NBR, P.CHECK_DATE, P.RUN_NUMBER ' +
                'from CL L, CO O, PR P, PR_CHECK C, EE E, CL_PERSON N ' +
                'where C.EE_NBR in (#EENbr) and ' +
                      'C.PR_NBR = P.PR_NBR and ' +
                      'P.CO_NBR = O.CO_NBR and ' +
                      'P.PAYROLL_TYPE <> ''' + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + ''' and ' +
                      'P.STATUS = ''' + PAYROLL_STATUS_PENDING + ''' and ' +
                      'C.EE_NBR = E.EE_NBR and ' +
                      'E.CL_PERSON_NBR = N.CL_PERSON_NBR and ' +
                      '{AsOfNow<L>} and ' +
                      '{AsOfNow<O>} and ' +
                      '{AsOfNow<P>} and ' +
                      '{AsOfNow<C>} and ' +
                      '{AsOfNow<E>} and ' +
                      '{AsOfNow<N>} ' +
                'order by L.CL_NBR, O.CO_NBR, C.EE_NBR';

  CheckW2_1099Existence = 'select Count(*) ' +
              'from CO_BILLING_HISTORY CO_BILLING_HISTORY, CO_BILLING_HISTORY_DETAIL CO_BILLING_HISTORY_DETAIL ' +
              'where CO_BILLING_HISTORY.CO_BILLING_HISTORY_NBR = CO_BILLING_HISTORY_DETAIL.CO_BILLING_HISTORY_NBR and ' +
                    'CO_BILLING_HISTORY_DETAIL.#LVL_SERVICES_NBR = :ServiceNbr and ' +
                    'CO_BILLING_HISTORY.CO_NBR = :CoNbr and ' +
                    'CO_BILLING_HISTORY.INVOICE_DATE between :InvoiceDate - 180 and :InvoiceDate + 180 and ' +
                    '{AsOfNow<CO_BILLING_HISTORY>} and ' +
                    '{AsOfNow<CO_BILLING_HISTORY_DETAIL>}';

  GetInvoicesForCompany = 'select CL.CL_NBR, CO.CO_NBR, ' +
                       'CO.NAME COMPANY_NAME, CO.CUSTOM_COMPANY_NUMBER, ' +
                       'CO_BILLING_HISTORY.CO_BILLING_HISTORY_NBR, CO_BILLING_HISTORY.INVOICE_NUMBER, ' +
                       'CO_BILLING_HISTORY.INVOICE_DATE ' +
                'from CL CL, CO CO, CO_BILLING_HISTORY CO_BILLING_HISTORY ' +
                'where CO.CO_NBR = :CoNbr and ' +
                      'CO_BILLING_HISTORY.CO_NBR = CO.CO_NBR and ' +
                      '{AsOfNow<CL>} and ' +
                      '{AsOfNow<CO>} and ' +
                      '{AsOfNow<CO_BILLING_HISTORY>} ' +
                'order by CL.CL_NBR, CO.CO_NBR, CO_BILLING_HISTORY.INVOICE_DATE';

  GetInvoiceDetailsForCompany = 'select CL.CL_NBR, CO_SERVICES.NAME, CO_BILLING_HISTORY_DETAIL.* ' +
                'from CL CL, ' +
                  'CO_BILLING_HISTORY_DETAIL CO_BILLING_HISTORY_DETAIL ' +
                  'left join CO_SERVICES ' +
                    'on CO_SERVICES.CO_SERVICES_NBR = CO_BILLING_HISTORY_DETAIL.CO_SERVICES_NBR ' +
                'where {AsOfNow<CL>} and {AsOfNow<CO_SERVICES>} and ' +
                      '{AsOfNow<CO_BILLING_HISTORY_DETAIL>}';

  BR_CheckLinesByEDType = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'e.PR_CHECK_LINES_NBR, e.AMOUNT, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'g.CL_PERSON_NBR, g.FIRST_NAME, g.MIDDLE_INITIAL, g.LAST_NAME, g.SOCIAL_SECURITY_NUMBER, ' +
                       'f.EE_NBR, f.CUSTOM_EMPLOYEE_NUMBER, ' +
                       'h.CL_E_DS_NBR, h.CUSTOM_E_D_CODE_NUMBER, h.E_D_CODE_TYPE, ' +
                       'h.DESCRIPTION, h.W2_BOX, ' +
                       'h.EE_EXEMPT_EXCLUDE_FEDERAL, h.EE_EXEMPT_EXCLUDE_EIC, ' +
                       'h.EE_EXEMPT_EXCLUDE_OASDI, h.EE_EXEMPT_EXCLUDE_MEDICARE, ' +
                       'h.ER_EXEMPT_EXCLUDE_OASDI, h.ER_EXEMPT_EXCLUDE_MEDICARE, ' +
                       'h.ER_EXEMPT_EXCLUDE_FUI ' +
                'from CL a, CO b, PR c, PR_CHECK d, PR_CHECK_LINES e, ' +
                     'EE f, CL_PERSON g, CL_E_DS h ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and {AsOfNow<g>} and {AsOfNow<h>} and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.PR_CHECK_NBR = e.PR_CHECK_NBR and ' +
                      'd.EE_NBR = f.EE_NBR and ' +
                      'f.CL_PERSON_NBR = g.CL_PERSON_NBR and ' +
                      'e.CL_E_DS_NBR = h.CL_E_DS_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList)';

  BR_CheckWagesTaxes = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'd.FEDERAL_TAXABLE_WAGES, d.FEDERAL_TAX, ' +
                       '-d.EE_EIC_TAX EE_EIC_TAX, ' +

                       'IfDouble(CompareString(e.MAKEUP_FICA_ON_CLEANUP_PR, ''='', ''' + 'Y' + '''), d.EE_OASDI_TAX, 0) EE_OASDI_TAX_, ' +
                       'IfDouble(CompareString(e.MAKEUP_FICA_ON_CLEANUP_PR, ''='', ''' + 'Y' + '''), d.EE_MEDICARE_TAX, 0) EE_MEDICARE_TAX_, ' +
                       'IfDouble(CompareString(e.MAKEUP_FICA_ON_CLEANUP_PR, ''='', ''' + 'Y' + '''), d.ER_OASDI_TAX, 0) ER_OASDI_TAX_, ' +
                       'IfDouble(CompareString(e.MAKEUP_FICA_ON_CLEANUP_PR, ''='', ''' + 'Y' + '''), d.ER_MEDICARE_TAX, 0) ER_MEDICARE_TAX_, ' +

                       'd.EE_OASDI_TAXABLE_WAGES + d.EE_OASDI_TAXABLE_TIPS EE_OASDI_TAXABLE_WAGES, ' +
                       'IfDouble(CompareString(e.MAKEUP_FICA_ON_CLEANUP_PR, ''='', ''' + 'Y' + '''), d.EE_OASDI_TAXABLE_WAGES + d.EE_OASDI_TAXABLE_TIPS, 0) FICAEE_OASDIWages_, ' +
                       'IfDouble(CompareString(e.MAKEUP_FICA_ON_CLEANUP_PR, ''='', ''' + 'Y' + '''), d.EE_MEDICARE_TAXABLE_WAGES, 0) FICAEE_MEDICAREWages_, ' +
                       'd.EE_OASDI_TAX EE_OASDI_TAX, ' +
                       'd.EE_MEDICARE_TAXABLE_WAGES EE_MEDICARE_TAXABLE_WAGES, ' +
                       'd.EE_MEDICARE_TAX EE_MEDICARE_TAX, ' +
                       'd.ER_OASDI_TAXABLE_WAGES + d.ER_OASDI_TAXABLE_TIPS ER_OASDI_TAXABLE_WAGES, ' +
                       'd.ER_OASDI_TAX, ' +
                       'd.ER_MEDICARE_TAXABLE_WAGES, ' +
                       'd.ER_MEDICARE_TAX, ' +
                       'd.ER_FUI_TAXABLE_WAGES, d.ER_FUI_TAX, ' +
                       'd.OR_CHECK_BACK_UP_WITHHOLDING BACKUP_WITHHOLDING, ' +
                       'f.CL_PERSON_NBR, f.FIRST_NAME, f.MIDDLE_INITIAL, f.LAST_NAME, f.SOCIAL_SECURITY_NUMBER, ' +
                       'e.EE_NBR, e.CUSTOM_EMPLOYEE_NUMBER, e.COMPANY_OR_INDIVIDUAL_NAME, ' +
                       'e.EXEMPT_EXCLUDE_EE_FED, e.EXEMPT_EMPLOYEE_OASDI, e.EXEMPT_EMPLOYEE_MEDICARE, ' +
                       'e.EXEMPT_EMPLOYER_OASDI, e.EXEMPT_EMPLOYER_MEDICARE, e.EXEMPT_EMPLOYER_FUI, e.MAKEUP_FICA_ON_CLEANUP_PR ' +
                'from CL a, CO b, PR c, PR_CHECK d, EE e, CL_PERSON f ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.EE_NBR = e.EE_NBR and ' +
                      'e.CL_PERSON_NBR = f.CL_PERSON_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList)  order by a.cl_nbr, b.co_nbr, e.ee_nbr';

  BR_FedLiab = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'c.PR_NBR, c.TAX_TYPE, c.AMOUNT, c.THIRD_PARTY ' +
                'from CL a, CO b, CO_FED_TAX_LIABILITIES c ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      '(c.ADJUSTMENT_TYPE in ('''+TAX_LIABILITY_ADJUSTMENT_TYPE_NONE+''',  ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP + ''',''' + TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION + ''')) and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'b.CO_NBR in (#CoList)';

  BR_CheckStateInfo = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'j.CL_PERSON_NBR, j.FIRST_NAME, j.MIDDLE_INITIAL, j.LAST_NAME, j.SOCIAL_SECURITY_NUMBER, ' +
                       'f.EE_NBR, f.CUSTOM_EMPLOYEE_NUMBER, f.COMPANY_OR_INDIVIDUAL_NAME, ' +
                       'h.CO_STATES_NBR, h.STATE, ' +
                       'e.STATE_TAXABLE_WAGES, e.STATE_TAX, ' +
                       'e.EE_SDI_TAXABLE_WAGES, e.EE_SDI_TAX, ' +
                       'e.ER_SDI_TAXABLE_WAGES, e.ER_SDI_TAX ' +
                'from CL a, CO b, PR c, PR_CHECK d, PR_CHECK_STATES e, EE f, ' +
                     'EE_STATES g, CO_STATES h, CL_PERSON j ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and {AsOfNow<e>} and ' +
                      '{AsOfNow<f} and {AsOfNow<g} and {AsOfNow<h} and {AsOfNow<j} and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.PR_CHECK_NBR = e.PR_CHECK_NBR and ' +
                      'd.EE_NBR = f.EE_NBR and ' +
                      'f.CL_PERSON_NBR = j.CL_PERSON_NBR and ' +
                      'e.EE_STATES_NBR = g.EE_STATES_NBR and ' +
                      'g.CO_STATES_NBR = h.CO_STATES_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList) order by a.cl_nbr, b.co_nbr, f.ee_nbr';

  BR_StateLiab = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'd.PR_NBR, d.TAX_TYPE, d.AMOUNT, ' +
                       'c.CO_STATES_NBR, c.STATE ' +
                'from CL a, CO b, CO_STATES c, CO_STATE_TAX_LIABILITIES d ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      'b.CO_NBR = d.CO_NBR and ' +
                      'd.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'd.CO_STATES_NBR = c.CO_STATES_NBR and ' +
                      '(d.ADJUSTMENT_TYPE in ('''+TAX_LIABILITY_ADJUSTMENT_TYPE_NONE+''', ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP + ''',''' + TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION + ''')) and ' +                      'b.CO_NBR in (#CoList)';

  BR_CheckStateLinesInfo = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'e.PR_CHECK_LINES_NBR, e.AMOUNT, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'j.CL_PERSON_NBR, j.FIRST_NAME, j.MIDDLE_INITIAL, j.LAST_NAME, j.SOCIAL_SECURITY_NUMBER, ' +
                       'f.EE_NBR, f.CUSTOM_EMPLOYEE_NUMBER, h.STATE, ' +
                       'k.CL_E_DS_NBR, k.CUSTOM_E_D_CODE_NUMBER, k.E_D_CODE_TYPE, ' +
                       'k.DESCRIPTION, l.SY_STATES_NBR, ' +
                       'l.EMPLOYEE_EXEMPT_EXCLUDE_STATE, l.EMPLOYEE_EXEMPT_EXCLUDE_SDI, ' +
                       'l.EMPLOYER_EXEMPT_EXCLUDE_SDI ' +
                'from CL a, CO b, PR c, PR_CHECK d, PR_CHECK_LINES e, ' +
                     'EE f, EE_STATES g, CO_STATES h, CL_PERSON j, CL_E_DS k, ' +
                     'CL_E_D_STATE_EXMPT_EXCLD l ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and {AsOfNow<g>} and {AsOfNow<h>} and ' +
                      '{AsOfNow<j>} and {AsOfNow<k>} and {AsOfNow<l>} and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.PR_CHECK_NBR = e.PR_CHECK_NBR and ' +
                      'd.EE_NBR = f.EE_NBR and ' +
                      'f.CL_PERSON_NBR = j.CL_PERSON_NBR and ' +
                      'e.CL_E_DS_NBR = k.CL_E_DS_NBR and ' +
                      'k.CL_E_DS_NBR = l.CL_E_DS_NBR and ' +
                      'e.EE_STATES_NBR = g.EE_STATES_NBR and ' +
                      'g.CO_STATES_NBR = h.CO_STATES_NBR and ' +
                      'h.SY_STATES_NBR = l.SY_STATES_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList)';

  BR_CheckSUIInfo = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'l.CL_PERSON_NBR, l.FIRST_NAME, l.MIDDLE_INITIAL, l.LAST_NAME, l.SOCIAL_SECURITY_NUMBER, ' +
                       'h.EE_NBR, h.CUSTOM_EMPLOYEE_NUMBER, h.COMPANY_OR_INDIVIDUAL_NAME, ' +
                       'g.CO_STATES_NBR, g.STATE, ' +
                       'e.SUI_TAXABLE_WAGES, e.SUI_TAX, e.SUI_GROSS_WAGES, ' +
                       'f.DESCRIPTION, f.SY_SUI_NBR ' +
                'from CL a, CO b, PR c, PR_CHECK d, PR_CHECK_SUI e, ' +
                     'CO_SUI f, CO_STATES g, ee h, CL_PERSON l ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and {AsOfNow<g>} and {AsOfNow<h>} and {AsOfNow<l>} and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.PR_CHECK_NBR = e.PR_CHECK_NBR and ' +
                      'd.EE_NBR = h.EE_NBR and ' +
                      'h.CL_PERSON_NBR = l.CL_PERSON_NBR and ' +
                      'e.CO_SUI_NBR = f.CO_SUI_NBR and ' +
                      'f.CO_STATES_NBR = g.CO_STATES_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList) order by a.cl_nbr, b.co_nbr, h.ee_nbr';

  BR_CheckSUILinesInfo = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'g.PR_CHECK_LINES_NBR, g.AMOUNT, ' +
                       'e.PR_NBR, e.RUN_NUMBER, e.CHECK_DATE, ' +
                       'f.PR_CHECK_NBR, f.PAYMENT_SERIAL_NUMBER, f.CHECK_TYPE, ' +
                       'l.CL_PERSON_NBR, l.FIRST_NAME, l.MIDDLE_INITIAL, l.LAST_NAME, l.SOCIAL_SECURITY_NUMBER, ' +
                       'h.EE_NBR, h.CUSTOM_EMPLOYEE_NUMBER, k.STATE, d.SY_SUI_NBR, ' +
                       'm.CL_E_DS_NBR, m.CUSTOM_E_D_CODE_NUMBER, m.E_D_CODE_TYPE, ' +
                       'm.DESCRIPTION, n.SY_STATES_NBR, ' +
                       'n.EMPLOYEE_EXEMPT_EXCLUDE_SUI, ' +
                       'n.EMPLOYER_EXEMPT_EXCLUDE_SUI ' +
                'from CL a, CO b, PR_CHECK_SUI c, CO_SUI d, PR e, ' +
                     'PR_CHECK f, PR_CHECK_LINES g, EE h, EE_STATES j, ' +
                     'CO_STATES k, CL_PERSON l, CL_E_DS m, CL_E_D_STATE_EXMPT_EXCLD n ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and {AsOfNow<g>} and {AsOfNow<h>} and {AsOfNow<j>} and ' +
                      '{AsOfNow<k>} and {AsOfNow<l>} and {AsOfNow<m>} and {AsOfNow<n>} and ' +
                      'b.CO_NBR = e.CO_NBR and ' +
                      'e.PR_NBR = f.PR_NBR and ' +
                      'f.PR_CHECK_NBR = g.PR_CHECK_NBR and ' +
                      'f.EE_NBR = h.EE_NBR and ' +
                      'h.CL_PERSON_NBR = l.CL_PERSON_NBR and ' +
                      'g.CL_E_DS_NBR = m.CL_E_DS_NBR and ' +
                      'm.CL_E_DS_NBR = n.CL_E_DS_NBR and ' +
                      'g.EE_SUI_STATES_NBR = j.EE_STATES_NBR and ' +
                      'j.CO_STATES_NBR = k.CO_STATES_NBR and ' +
                      'k.SY_STATES_NBR = n.SY_STATES_NBR and ' +
                      'c.PR_CHECK_NBR = f.PR_CHECK_NBR and ' +
                      'c.CO_SUI_NBR = d.CO_SUI_NBR and ' +
                      'd.CO_STATES_NBR = k.CO_STATES_NBR and ' +
                      'e.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'e.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList)';

  BR_SUILiab = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'e.PR_NBR, e.AMOUNT, ' +
                       'c.CO_STATES_NBR, c.STATE, d.SY_SUI_NBR ' +
                'from CL a, CO b, CO_STATES c, CO_SUI d, CO_SUI_LIABILITIES e ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and {AsOfNow<e>} and ' +
                      'b.CO_NBR = e.CO_NBR and ' +
                      'e.CO_SUI_NBR = d.CO_SUI_NBR and ' +
                      '(e.ADJUSTMENT_TYPE in ('''+TAX_LIABILITY_ADJUSTMENT_TYPE_NONE+''', ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP + ''',''' + TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION + ''')) and ' +
                      'd.CO_STATES_NBR = c.CO_STATES_NBR and ' +
                      'e.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'b.CO_NBR in (#CoList)';

  BR_CheckLocalInfo = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'j.CL_PERSON_NBR, j.FIRST_NAME, j.MIDDLE_INITIAL, j.LAST_NAME, j.SOCIAL_SECURITY_NUMBER, ' +
                       'f.EE_NBR, f.CUSTOM_EMPLOYEE_NUMBER, f.COMPANY_OR_INDIVIDUAL_NAME, ' +
                       'g.EE_LOCALS_NBR, h.CO_LOCAL_TAX_NBR, h.SY_LOCALS_NBR, ' +
                       'e.LOCAL_TAXABLE_WAGE, e.LOCAL_TAX ' +
                'from CL a, CO b, PR c, PR_CHECK d, PR_CHECK_LOCALS e, ' +
                     'EE f, EE_LOCALS g, CO_LOCAL_TAX h, CL_PERSON j ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and {AsOfNow<g>} and {AsOfNow<h>} and {AsOfNow<j>} and ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.PR_CHECK_NBR = e.PR_CHECK_NBR and ' +
                      'd.EE_NBR = f.EE_NBR and ' +
                      'f.CL_PERSON_NBR = j.CL_PERSON_NBR and ' +
                      'e.EE_LOCALS_NBR = g.EE_LOCALS_NBR and ' +
                      'g.CO_LOCAL_TAX_NBR = h.CO_LOCAL_TAX_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList) order by a.cl_nbr, b.co_nbr, f.ee_nbr';

  BR_CheckLocalLinesInfo = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'm.PR_CHECK_LINES_NBR, m.AMOUNT, ' +
                       'c.PR_NBR, c.RUN_NUMBER, c.CHECK_DATE, ' +
                       'd.PR_CHECK_NBR, d.PAYMENT_SERIAL_NUMBER, d.CHECK_TYPE, ' +
                       'h.CL_PERSON_NBR, h.FIRST_NAME, h.MIDDLE_INITIAL, h.LAST_NAME, h.SOCIAL_SECURITY_NUMBER, ' +
                       'g.EE_NBR, g.CUSTOM_EMPLOYEE_NUMBER, ' +
                       'j.CL_E_DS_NBR, j.CUSTOM_E_D_CODE_NUMBER, j.E_D_CODE_TYPE, ' +
                       'j.DESCRIPTION, k.SY_LOCALS_NBR, ' +
                       'k.EXEMPT_EXCLUDE, ' +
                       '(select count(*) from PR_CHECK_LINE_LOCALS where m.PR_CHECK_LINES_NBR = PR_CHECK_LINES_NBR and {AsOfNow<PR_CHECK_LINE_LOCALS>}) PR_CHECK_LINE_LOCALS_NBR ' +
                'from CL a, CO b, PR c, PR_CHECK d, EE_STATES e, CO_LOCAL_TAX f, ' +
                     'EE g, CL_PERSON h, CL_E_DS j, CL_E_D_LOCAL_EXMPT_EXCLD k, CO_STATES l, ' +
                     'PR_CHECK_LINES m ' +
                'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      '{AsOfNow<e>} and {AsOfNow<f>} and {AsOfNow<g>} and {AsOfNow<h>} and {AsOfNow<j>} and ' +
                      '{AsOfNow<k>} and {AsOfNow<l>} and {AsOfNow<m>} and  ' +
                      'b.CO_NBR = c.CO_NBR and ' +
                      'c.PR_NBR = d.PR_NBR and ' +
                      'd.PR_CHECK_NBR = m.PR_CHECK_NBR and ' +
                      'd.EE_NBR = g.EE_NBR and ' +
                      'g.CL_PERSON_NBR = h.CL_PERSON_NBR and ' +
                      'm.CL_E_DS_NBR = j.CL_E_DS_NBR and ' +
                      'j.CL_E_DS_NBR = k.CL_E_DS_NBR and ' +
                      'm.EE_STATES_NBR = e.EE_STATES_NBR and ' +
                      'e.CO_STATES_NBR = l.CO_STATES_NBR and ' +
                      'l.SY_STATES_NBR = f.SY_STATES_NBR and ' +
                      'f.SY_LOCALS_NBR = k.SY_LOCALS_NBR and ' +
                      'c.CHECK_DATE between :BeginDate and :EndDate and ' +
                      'c.STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''' , ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                      'b.CO_NBR in (#CoList)';

  BR_LocalLiab = 'select a.CL_NBR, a.CUSTOM_CLIENT_NUMBER, a.NAME CLIENT_NAME, ' +
                       'b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME COMPANY_NAME, ' +
                       'd.PR_NBR, d.AMOUNT, c.SY_LOCALS_NBR ' +
                'from CL a, CO b, CO_LOCAL_TAX c, CO_LOCAL_TAX_LIABILITIES d ' +
                'where  {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
                      'b.CO_NBR = d.CO_NBR and ' +
                      'd.CHECK_DATE between :BeginDate and :EndDate and ' +
                      '(d.ADJUSTMENT_TYPE in ('''+TAX_LIABILITY_ADJUSTMENT_TYPE_NONE+''', ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP + ''',''' + TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION + ''')) and ' +
                      'd.CO_LOCAL_TAX_NBR = c.CO_LOCAL_TAX_NBR and ' +
                      'b.CO_NBR in (#CoList)';
 

  CountW2_EEs = 'select #group, count(distinct CL_PERSON_NBR) cnt from ee ' +
                'where ((#CountCondition1) or (#CountCondition2)) '+
                'and co_nbr = :CoNbr and '+
                    '{AsOfNow<ee>} and ' +
                    ' 0 < ( select  ConvertNullDouble(sum(cl.Amount),0) ' +
                    ' from pr pr, pr_check pc , pr_check_lines cl, Cl_E_Ds ed ' +
                    ' where  {AsOfNow<pr>} and {AsOfNow<pc>} and  {AsOfNow<cl>} and {AsOfNow<ed>} ' +
                    '       and cl.pr_check_nbr = pc.pr_check_nbr '+
                    '       and pr.pr_nbr = pc.pr_nbr '+
                    '       and ed.Cl_E_Ds_Nbr = cl.Cl_E_Ds_Nbr '+

                    '       and ed.E_D_CODE_TYPE NOT IN ('''+ED_ST_EXEMPT_EARNINGS+''','''+ED_ST_SP_TAXED_1099_EARNINGS+''') and ed.E_D_CODE_TYPE <> ''D1'' ' +

                    '       and pc.ee_nbr  = ee.ee_nbr '+
                    '       and BeginYear(pr.check_date) = :BeginYear '+
                    '       and pr.'+PayrollProcessedClause + ' ) ' +
                    ' group by #group ';


  Count1099_EEs = 'select #group, count(distinct CL_PERSON_NBR) cnt from ee ' +
              'where W2_Type IN (''O'', ''B'') '+
              'and co_nbr = :CoNbr and {AsOfNow<ee>} and ' +
                    ' #Margin < ( select ConvertNullDouble(sum(cl.Amount),0) ' +
                    ' from pr pr, pr_check pc , pr_check_lines cl, Cl_E_Ds ed ' +
                    ' where {AsOfNow<pr>}' +
                    '       and {AsOfNow<pc>}' +
                    '       and {AsOfNow<cl>}' +
                    '       and {AsOfNow<ed>}' +

                    '       and cl.pr_check_nbr = pc.pr_check_nbr '+
                    '       and pr.pr_nbr = pc.pr_nbr '+
                    '       and ed.Cl_E_Ds_Nbr = cl.Cl_E_Ds_Nbr '+

                    '       and ed.E_D_CODE_TYPE IN ('''+ED_ST_EXEMPT_EARNINGS+''','''+ED_ST_SP_TAXED_1099_EARNINGS+''') '+

                    ' and ed.E_D_CODE_TYPE <> ''D1'' ' +

                    '       and pc.ee_nbr  = ee.ee_nbr '+
                    '       and BeginYear(pr.check_date) = :BeginYear '+
                    '       and pr.'+PayrollProcessedClause + ' ) ' +
                    ' group by #group';

  AllPendingLiabsForCo = 'select ''F'' LEVEL_, CO_FED_TAX_LIABILITIES.CO_FED_TAX_LIABILITIES_NBR NBR, ' +
                        'CO_FED_TAX_LIABILITIES.TAX_TYPE, CO_FED_TAX_LIABILITIES.AMOUNT, ' +
                        'CO_FED_TAX_LIABILITIES.CHECK_DATE, CO_FED_TAX_LIABILITIES.DUE_DATE, ' +
                        'CO_FED_TAX_LIABILITIES.STATUS, PR.RUN_NUMBER, ''  '' STATE, 0 REF_NBR, CO_FED_TAX_LIABILITIES.PR_NBR ' +
                'from CO_FED_TAX_LIABILITIES CO_FED_TAX_LIABILITIES left join PR PR on CO_FED_TAX_LIABILITIES.PR_NBR = PR.PR_NBR and {AsOfNow<PR>} ' +
                'where {AsOfNow<CO_FED_TAX_LIABILITIES>}' + ' and ' +
                      'CO_FED_TAX_LIABILITIES.THIRD_PARTY = ''' + GROUP_BOX_NO + ''' and ' +
                      'CO_FED_TAX_LIABILITIES.CO_TAX_DEPOSITS_NBR is null and ' +
                      'CO_FED_TAX_LIABILITIES.STATUS in (''' + TAX_DEPOSIT_STATUS_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_PENDING + ''') and ' +
                      'CO_FED_TAX_LIABILITIES.CO_NBR = :CoNbr ' +
                'union ' +
                'select ''S'' LEVEL_, CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIES_NBR NBR, ' +
                        'CO_STATE_TAX_LIABILITIES.TAX_TYPE, CO_STATE_TAX_LIABILITIES.AMOUNT, ' +
                        'CO_STATE_TAX_LIABILITIES.CHECK_DATE, CO_STATE_TAX_LIABILITIES.DUE_DATE, ' +
                        'CO_STATE_TAX_LIABILITIES.STATUS, PR.RUN_NUMBER, CO_STATES.STATE, 0 REF_NBR, CO_STATE_TAX_LIABILITIES.PR_NBR ' +
                'from CO_STATE_TAX_LIABILITIES CO_STATE_TAX_LIABILITIES left join PR PR on CO_STATE_TAX_LIABILITIES.PR_NBR = PR.PR_NBR and {AsOfNow<PR>}, CO_STATES ' +
                'where {AsOfNow<CO_STATE_TAX_LIABILITIES>} and ' +
                      '{AsOfNow<CO_STATES>} and ' +
                      'CO_STATE_TAX_LIABILITIES.CO_STATES_NBR = CO_STATES.CO_STATES_NBR and ' +
                      'CO_STATE_TAX_LIABILITIES.THIRD_PARTY = ''' + GROUP_BOX_NO + ''' and ' +
                      'CO_STATE_TAX_LIABILITIES.CO_TAX_DEPOSITS_NBR is null and ' +
                      'CO_STATE_TAX_LIABILITIES.STATUS in (''' + TAX_DEPOSIT_STATUS_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_PENDING + ''') and ' +
                      'CO_STATE_TAX_LIABILITIES.CO_NBR = :CoNbr ' +
                'union ' +
                'select ''U'' LEVEL_, CO_SUI_LIABILITIES.CO_SUI_LIABILITIES_NBR NBR, ' +
                        ''' '' TAX_TYPE, CO_SUI_LIABILITIES.AMOUNT, ' +
                        'CO_SUI_LIABILITIES.CHECK_DATE, CO_SUI_LIABILITIES.DUE_DATE, ' +
                        'CO_SUI_LIABILITIES.STATUS, PR.RUN_NUMBER, CO_STATES.STATE, CO_SUI.SY_SUI_NBR REF_NBR, CO_SUI_LIABILITIES.PR_NBR ' +
                'from CO_SUI_LIABILITIES CO_SUI_LIABILITIES left join PR PR on CO_SUI_LIABILITIES.PR_NBR = PR.PR_NBR and {AsOfNow<PR>}, CO_STATES, CO_SUI ' +
                'where {AsOfNow<CO_SUI_LIABILITIES>} and ' +
                      '{AsOfNow<CO_STATES>} and ' +
                      '{AsOfNow<CO_SUI>} and ' +
                      'CO_SUI_LIABILITIES.CO_SUI_NBR = CO_SUI.CO_SUI_NBR and ' +
                      'CO_SUI.CO_STATES_NBR = CO_STATES.CO_STATES_NBR and ' +
                      'CO_SUI_LIABILITIES.THIRD_PARTY = ''' + GROUP_BOX_NO + ''' and ' +
                      'CO_SUI_LIABILITIES.CO_TAX_DEPOSITS_NBR is null and ' +
                      'CO_SUI_LIABILITIES.STATUS in (''' + TAX_DEPOSIT_STATUS_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_PENDING + ''') and ' +
                      'CO_SUI_LIABILITIES.CO_NBR = :CoNbr ' +
                'union ' +
                'select ''L'' LEVEL_, CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIES_NBR NBR, ' +
                        ''' '' TAX_TYPE, CO_LOCAL_TAX_LIABILITIES.AMOUNT, ' +
                        'CO_LOCAL_TAX_LIABILITIES.CHECK_DATE, CO_LOCAL_TAX_LIABILITIES.DUE_DATE, ' +
                        'CO_LOCAL_TAX_LIABILITIES.STATUS, PR.RUN_NUMBER, ''  '' STATE, CO_LOCAL_TAX.SY_LOCALS_NBR REF_NBR, CO_LOCAL_TAX_LIABILITIES.PR_NBR ' +
                'from CO_LOCAL_TAX_LIABILITIES CO_LOCAL_TAX_LIABILITIES left join PR PR on CO_LOCAL_TAX_LIABILITIES.PR_NBR = PR.PR_NBR and {AsOfNow<PR>}, CO_LOCAL_TAX ' +
                'where {AsOfNow<CO_LOCAL_TAX_LIABILITIES>} and ' +
                      '{AsOfNow<CO_LOCAL_TAX>} and ' +
                      'CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_NBR = CO_LOCAL_TAX.CO_LOCAL_TAX_NBR and ' +
                      'CO_LOCAL_TAX_LIABILITIES.THIRD_PARTY = ''' + GROUP_BOX_NO + ''' and ' +
                      'CO_LOCAL_TAX_LIABILITIES.CO_TAX_DEPOSITS_NBR is null and ' +
                      'CO_LOCAL_TAX_LIABILITIES.STATUS in (''' + TAX_DEPOSIT_STATUS_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED + ''', ' +
                                                        '''' + TAX_DEPOSIT_STATUS_PENDING + ''') and ' +
                      'CO_LOCAL_TAX_LIABILITIES.CO_NBR = :CoNbr';

  SelectCheckLineLocals = 'select N.* from PR_CHECK_LINE_LOCALS N ' +
      'join PR_CHECK_LINES L on N.PR_CHECK_LINES_NBR=L.PR_CHECK_LINES_NBR ' +
      'where L.PR_NBR = :PrNbr and ' +
      '{AsOfNow<N>} and {AsOfNow<L>}';

  SelectPayrollsForAgencyCheck = 'select distinct P.* from PR P ' +
      'join PR_CHECK_LINES L on P.PR_NBR=L.PR_NBR ' +
      'where L.PR_MISCELLANEOUS_CHECKS_NBR=:MiscCheckNbr and ' +
      'P.PR_NBR<>:PrNbr and ' +
      '{AsOfNow<P>} and {AsOfNow<L>}';

  DistrEDEForACH = 'select sum(ld.amount) amount, ' +
                     'ld.pr_check_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check_lines ld ' +
              'WHERE ld.cl_e_ds_nbr in (#CodeNbr) and ' +
                    'ld.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<ld>} ' +
              'group by ld.pr_check_nbr, ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  DistrEDDForACH = 'select l.amount*ld.perc amount, l.amount amount1, ld.perc perc, ' +
                     'ld.pr_check_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check_lines l, pr_check_lines_distributed(''D'', #PrNbr, :dt) ld ' +
              'WHERE ld.pr_check_nbr = l.pr_check_nbr and ' +
                    'l.cl_e_ds_nbr in (#CodeNbr) and ' +
                    'l.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<l>} ' +
              'order by ld.pr_check_nbr, ' +
                       'ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  DistrEDCOUNTForACH = ' SELECT distinct '+
              '   l.Cl_E_Ds_Nbr,'+
              '   c.Ee_Nbr, '+
              '   l.Co_Branch_Nbr,'+
              '   l.Co_Department_Nbr,'+
              '   l.Co_Division_Nbr,'+
              '   l.Co_Team_Nbr,'+
              '   l.Pr_Nbr,'+
              '   l.Pr_Check_Nbr'+
              ' FROM'+
              '   Pr_Check  c,'+
              '   Pr_Check_Lines  l'+
              ' WHERE'+
              '   c.Pr_Check_Nbr = l.Pr_Check_Nbr'+
              '  AND'+
              '  {AsOfNow<l>} AND ' +
              '  l.Pr_Nbr = :PrNbr AND l.Cl_E_Ds_Nbr in (#CodeNbr)'+
              ' ORDER BY'+
              '   8, 5, 3, 4, 6';


  DistrDeductionsForACH = 'select l.amount*ld.perc amount, ' +
                     'ld.pr_check_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check_lines l, pr_check_lines_distributed(''D'', #PrNbr, :dt) ld, cl_e_ds cl ' +
              'WHERE ld.pr_check_nbr = l.pr_check_nbr and ' +
                    'l.cl_e_ds_nbr = cl.cl_e_ds_nbr and ' +
                    'cl.e_d_code_type starting with ''D'' and ' +
                    'l.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<cl>} and ' +
                    '{AsOfNow<l>} ' +
              'order by ld.pr_check_nbr, ' +
                       'ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  DistrFederalTaxesForACH = 'select c.federal_tax*ld.perc federal_tax, ' +
                     'IfDouble(CompareString(c.check_type, ''='', ''' + CHECK_TYPE2_3RD_PARTY + '''), 0, c.ee_oasdi_tax)*ld.perc ee_oasdi_tax, ' +
                     'IfDouble(CompareString(c.check_type, ''='', ''' + CHECK_TYPE2_3RD_PARTY + '''), 0, c.ee_medicare_tax)*ld.perc ee_medicare_tax, ' +
                     'c.ee_eic_tax*-1*ld.perc ee_eic_tax, ' +
                     'c.or_check_back_up_withholding*ld.perc or_check_back_up_withholding, ' +
                     'c.er_oasdi_tax*ld.perc er_oasdi_tax, ' +
                     'c.er_medicare_tax*ld.perc er_medicare_tax, ' +
                     'c.er_fui_tax*ld.perc er_fui_tax, ' +
                     'c.pr_check_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check c, pr_check_lines_distributed(''T'', #PrNbr, :dt) ld ' +
              'WHERE c.pr_check_nbr = ld.pr_check_nbr and ' +
                    'c.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<c>} ' +
              'order by c.pr_check_nbr, ' +
                       'ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  DistrStateTaxesForACH = 'select ps.state_tax*ld.perc state_tax, ' +
                     'ps.ee_sdi_tax*ld.perc ee_sdi_tax, ' +
                     'ps.er_sdi_tax*ld.perc er_sdi_tax, ' +
                     'ps.pr_check_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check_states ps, pr_check_lines_distributed(''T'', #PrNbr, :dt) ld ' +
              'WHERE ps.pr_check_nbr = ld.pr_check_nbr and ' +
                    'ps.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<ps>} ' +
              'order by ps.pr_check_nbr, ' +
                       'ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  DistrLocalTaxesForACH = 'select IfDouble(CompareString(c.check_type, ''='', ''' + CHECK_TYPE2_3RD_PARTY + '''), 0, ps.local_tax)*ld.perc local_tax, ' +
                     'ps.pr_check_nbr, ' +
                     'cl.sy_locals_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check c, pr_check_locals ps, pr_check_lines_distributed(''T'', #PrNbr, :dt) ld, ' +
                   'ee_locals el, co_local_tax cl ' +
              'WHERE c.pr_check_nbr = ps.pr_check_nbr and ' +
                    'ps.pr_check_nbr = ld.pr_check_nbr and ' +
                    'ps.ee_locals_nbr = el.ee_locals_nbr and ' +
                    'el.co_local_tax_nbr = cl.co_local_tax_nbr and ' +
                    'c.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<c>} and ' +
                    '{AsOfNow<el>} and ' +
                    '{AsOfNow<cl>} and ' +
                    '{AsOfNow<ps>} ' +
              'order by ps.pr_check_nbr, ' +
                       'ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr, ' +
                       'cl.sy_locals_nbr';

  DistrSUITaxesForACH = 'select ps.sui_tax*ld.perc sui_tax, ' +
                     'ps.pr_check_nbr, ' +
                     'cs.sy_sui_nbr, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_check_sui ps, pr_check_lines_distributed(''T'', #PrNbr, :dt) ld, ' +
                   'co_sui cs ' +
              'WHERE ps.pr_check_nbr = ld.pr_check_nbr and ' +
                    'ps.co_sui_nbr = cs.co_sui_nbr and ' +
                    'ps.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<cs>} and ' +
                    '{AsOfNow<ps>} ' +
              'order by ps.pr_check_nbr, ' +
                       'ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  DistrERSUIOtherTaxesForACH = 'select sum(csl.Amount) tax, '+
                      '0 pr_check_nbr, '+
                      '0 co_division_nbr, '+
                      '0 co_branch_nbr, '+
                      '0 co_department_nbr, '+
                      '0 co_team_nbr '+
              'FROM co_sui cs, '+
                    'Co_Sui_Liabilities csl '+
              'WHERE cs.Co_Sui_Nbr = csl.Co_Sui_Nbr and '+
                     'csl.pr_nbr = :PrNbr and '+
                     'cs.Sy_Sui_Nbr in (122, 124, 125) and '+
                     '{AsOfNow<cs>} and ' +
                     '{AsOfNow<csl>}';

  DistrEESUIOtherTaxesForACH = 'select sum(csl.Amount) tax, '+
                      '0 pr_check_nbr, '+
                      '0 co_division_nbr, '+
                      '0 co_branch_nbr, '+
                      '0 co_department_nbr, '+
                      '0 co_team_nbr '+
              'FROM co_sui cs, '+
                    'Co_Sui_Liabilities csl '+
              'WHERE cs.Co_Sui_Nbr = csl.Co_Sui_Nbr and '+
                     'csl.pr_nbr = :PrNbr and '+
                     'cs.Sy_Sui_Nbr in (121, 122, 123, 124, 125) and '+
                     '{AsOfNow<cs>} and ' +
                     '{AsOfNow<csl>}';

  DistrEarningsForACH =
        'select sum(ld.amount) amount, ' +
               'ld.pr_check_nbr, ' +
               'ld.co_division_nbr, ' +
               'ld.co_branch_nbr, ' +
               'ld.co_department_nbr, ' +
               'ld.co_team_nbr ' +
        'FROM pr_check_lines ld, cl_e_ds cl ' +
        'WHERE ld.cl_e_ds_nbr = cl.cl_e_ds_nbr and ' +
              'cl.e_d_code_type starting with ''E'' and ' +
              'cl.e_d_code_type not in (' + ListTaxableMemoTypes + ') and ' +
              'ld.pr_nbr = :PrNbr and ' +
              '{AsOfNow<cl>} and ' +
              '{AsOfNow<ld>} ' +
        'group by ld.pr_check_nbr, ld.co_division_nbr, ' +
                 'ld.co_branch_nbr, ' +
                 'ld.co_department_nbr, ' +
                 'ld.co_team_nbr';

  ConsolidationQuarterMoLiabs = 'select s1.co_states_nbr, sum(l.amount) liabs '+
      'from CO_STATES s1 '+
      'join CO_STATES s on '+
        's.state = s1.state and {AsOfNow<s>} and '+
        '(s.co_nbr = :CoNbr or exists ('+
          'select co_nbr from co co, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<co>} and {AsOfNow<cn>} and '+
            'co.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            'cn.PRIMARY_CO_NBR = :CoNbr and co.co_nbr=s.co_nbr and '+
            'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') and '+
            'cn.SCOPE <> '''+ CONSOLIDATION_SCOPE_PAYROLL_ONLY + ''' '+
        ')) '+
      'join CO_STATE_TAX_LIABILITIES l on '+
        'l.check_date between :BegDate and :EndDate and {AsOfNow<l>} and '+
        'l.CO_STATES_NBR = s.CO_STATES_NBR and l.THIRD_PARTY='''+ GROUP_BOX_NO+ ''' and (l.ADJUSTMENT_TYPE #m1 '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_MISSOURI_ER_COMP+ ''') '+
      'where s1.state = ''MO'' and {AsOfNow<s1>} and s1.co_nbr = :CoNbr '+
      'group by s1.co_states_nbr';

  ConsolidationQuarterSuiCreditLiabs = 'select s1.co_sui_nbr, sum(l.amount) liabs '+
      'from CO_SUI s1 '+
      'join CO_SUI s on '+
        's.sy_SUI_NBR = s1.sy_SUI_NBR and {AsOfNow<s>} and '+
        '(s.co_nbr = :CoNbr or exists ('+
          'select co_nbr from co co, CL_CO_CONSOLIDATION cn '+
          'where {AsOfNow<co>} and {AsOfNow<cn>} and '+
            'co.CL_CO_CONSOLIDATION_NBR = cn.CL_CO_CONSOLIDATION_NBR and '+
            'cn.PRIMARY_CO_NBR = :CoNbr and co.co_nbr=s.co_nbr and '+
            'cn.CONSOLIDATION_TYPE in ('''+ CONSOLIDATION_TYPE_ALL_REPORTS+ ''','''+ CONSOLIDATION_TYPE_STATE+ ''') '+
        ')) '+
      'join CO_SUI_LIABILITIES l on '+
        'l.check_date between :BegDate and :EndDate and {AsOfNow<l>} and '+
        'l.CO_sui_NBR = s.CO_sui_NBR and l.THIRD_PARTY='''+ GROUP_BOX_NO+ ''' and (l.ADJUSTMENT_TYPE #m1 '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT+ ''') '+
      'where s1.sy_sui_Nbr = :SySuiNbrs and {AsOfNow<s1>} and s1.co_nbr = :CoNbr '+
      'group by s1.co_sui_nbr';

  SelectLastChangeNBR = 'select #Columns from #TableName #TableName ' +
              'where CREATION_DATE = (select Max(tn.CREATION_DATE) from #TableName tn ' +
                       'where tn.#TableName_NBR = :RecordNbr and tn.EFFECTIVE_DATE = #TableName.EFFECTIVE_DATE) and ' +
                    '#TableName_NBR = :RecordNbr and ' +
                    'EFFECTIVE_DATE = (select Max(tn2.EFFECTIVE_DATE) from #TableName tn2 ' +
                       'where tn2.#TableName_NBR = :RecordNbr and tn2.EFFECTIVE_DATE < :EffectiveDate)';

  TotalSUITaxableWages = 'select sum(S.SUI_GROSS_WAGES) TAXABLE_WAGES from PR_CHECK_SUI S ' +
      'join PR P on S.PR_NBR=P.PR_NBR ' +
      'join CO_SUI CS on S.CO_SUI_NBR=CS.CO_SUI_NBR ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR ' +
      'where P.CHECK_DATE between :BeginDate and :EndDate and P.{PayrollProcessedClause} and ' +
      'CS.SY_SUI_NBR in (#SUINbrValues) and (CO.CL_COMMON_PAYMASTER_NBR=(' +
      'select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbr and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<CO>} ' +
      'and {AsOfNow<M>}) and CO.CO_NBR<>:CONbr or CO.CO_NBR=:CONbr) and ' +
      '{AsOfNow<CO>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<CS>}';

  TotalSUIEmployees = 'select count (distinct C.EE_NBR) EE_COUNT from PR_CHECK_SUI S ' +
      'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR P on S.PR_NBR=P.PR_NBR ' +
      'join CO_SUI CS on S.CO_SUI_NBR=CS.CO_SUI_NBR ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR ' +
      'where P.CHECK_DATE between :BeginDate and :EndDate and P.{PayrollProcessedClause} and ' +
      'CS.SY_SUI_NBR in (#SUINbrValues) and (CO.CL_COMMON_PAYMASTER_NBR=(' +
      'select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbr and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<CO>} ' +
      'and {AsOfNow<M>}) and CO.CO_NBR<>:CONbr or CO.CO_NBR=:CONbr) and ' +
      '{AsOfNow<CO>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<CS>}';

  TotalSUIEmployeesByPeriod = 'select count (distinct C.EE_NBR) EE_COUNT from PR_CHECK_SUI S ' +
      'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
      'join PR_BATCH B on C.PR_BATCH_NBR=B.PR_BATCH_NBR ' +
      'join PR P on S.PR_NBR=P.PR_NBR ' +
      'join CO_SUI CS on S.CO_SUI_NBR=CS.CO_SUI_NBR ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR ' +
      'where B.PERIOD_END_DATE>=:MidDate and B.PERIOD_BEGIN_DATE<=:MidDate and P.{PayrollProcessedClause} and ' +
      'CS.SY_SUI_NBR in (#SUINbrValues) and (CO.CL_COMMON_PAYMASTER_NBR=(' +
      'select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbr and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<CO>} ' +
      'and {AsOfNow<M>}) and CO.CO_NBR<>:CONbr or CO.CO_NBR=:CONbr) and ' +
      '{AsOfNow<CO>} and ' +
      '{AsOfNow<S>} and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<B>} and ' +
      '{AsOfNow<C>} and ' +
      '{AsOfNow<CS>}';


  CheckServiceInBilling = 'select count(*) ' +
              'from pr, co_billing_history bh, co_billing_history_detail bhd ' +
              'where pr.check_date = :CheckDate and ' +
                    'bhd.co_services_nbr = :CoServicesNbr and ' +
                    'pr.pr_nbr = bh.pr_nbr and ' +
                    'bh.co_billing_history_nbr = bhd.co_billing_history_nbr and ' +
                    '{AsOfNow<pr>} and ' +
                    '{AsOfNow<bh>} and ' +
                    '{AsOfNow<bhd>}';


  SelectClPersonChanges = 'select ee.#Group, 1/(select count(*) from EE EE where co_nbr=#CoNbr and cl_person_nbr = tn.cl_person_nbr and {AsOfNow<ee>}) cnt ' +
              'from CL_PERSON tn, ee ee ' +
              'where ee.co_nbr=#CoNbr and ee.cl_person_nbr = tn.cl_person_nbr and {AsOfNow<ee>} and ' +
                    'tn.CREATION_DATE = (select Max(CREATION_DATE) from CL_PERSON ' +
                                 'where CL_PERSON_NBR = tn.CL_PERSON_NBR and EFFECTIVE_DATE = tn.EFFECTIVE_DATE) and ' +
                    'tn.EFFECTIVE_DATE > :PrevCD and tn.EFFECTIVE_DATE <= :CD and ' +
                    '#Condition';

  SelectHistoryOfChanges = 'select ee.#Group, count(*) cnt from #TableName tn, ee ee ' +
              'where ee.co_nbr=:CoNbr and ee.ee_nbr = tn.ee_nbr and {AsOfNow<ee>} and ' +
                    'tn.CREATION_DATE = (select Max(CREATION_DATE) from #TableName ' +
                       'where #TableName_NBR = tn.#TableName_NBR and EFFECTIVE_DATE = tn.EFFECTIVE_DATE) and #Condition ' +
              'group by ee.#Group';

  SelectHistoryOfChangesForFields = 'select ee.#Group, count(*) cnt from #TableName tn, ee ee ' +
              'where ee.co_nbr=:CoNbr and ee.ee_nbr = tn.ee_nbr and {AsOfNow<ee>} and ' +
                    'tn.CREATION_DATE = (select Max(CREATION_DATE) from #TableName ' +
                       'where #TableName_NBR = tn.#TableName_NBR and ' +
                             'EFFECTIVE_DATE = tn.EFFECTIVE_DATE) and ' +
                    'tn.EFFECTIVE_DATE > :PrevCD and tn.EFFECTIVE_DATE <= :CD and ' +
                    '(exists (select * from #TableName tt ' +
                         'where #TableName_NBR = tn.#TableName_NBR and ' +
                               'CREATION_DATE = (select Max(CREATION_DATE) from #TableName ' +
                                                 'where #TableName_NBR = tt.#TableName_NBR and ' +
                                                       'EFFECTIVE_DATE = tt.EFFECTIVE_DATE) and ' +
                               'EFFECTIVE_DATE = (select Max(EFFECTIVE_DATE) from #TableName ' +
                                                  'where #TableName_NBR = tn.#TableName_NBR and ' +
                                                        'EFFECTIVE_DATE < tn.EFFECTIVE_DATE) and ' +
                               '(#Fields) ' +
                       ') or ' +
                   '(select Max(EFFECTIVE_DATE) from #TableName ' +
                                                  'where #TableName_NBR = tn.#TableName_NBR and ' +
                                                        'EFFECTIVE_DATE < tn.EFFECTIVE_DATE) is null) and #Condition ' +
              'group by ee.#Group';

  EEListWC = 'SELECT t2.ee_nbr, t4.co_nbr, t3.co_division_nbr, t3.co_branch_nbr, t3.co_department_nbr, t3.co_team_nbr, ' +
                     't3.AMOUNT,t3.rate_of_pay,t3.HOURS_OR_PIECES,t3.PR_CHECK_NBR, t4.WORKERS_COMP_WAGE_LIMIT, t4.WC_WAGE_LIMIT_FREQUENCY, ' +
                     't3.CO_WORKERS_COMP_NBR, t3.RATE_NUMBER, t3.CL_E_DS_NBR, t5.E_D_CODE_TYPE,t5.Overstate_Hours_For_Overtime reduce,  t5.REGULAR_OT_RATE ' +
              'FROM PR_CHECK t2, PR_CHECK_LINES t3, ' +
                   'Ee t4, Cl_E_DS t5 ' +
              'WHERE t2.PR_CHECK_NBR = t3.PR_CHECK_NBR AND t5.CL_E_DS_NBR = t3.CL_E_DS_NBR AND ' +
                    't2.PR_NBR = :PrNbr AND t4.EE_NBR = t2.ee_nbr  AND ' +
                    't3.PR_NBR=:PrNbr and {AsOfNow<t4>} AND '+
                    '{AsOfNow<t2>} AND {AsOfNow<t3>} AND {AsOfNow<t5>}';

  EESumOfAmount = 'SELECT t2.ee_nbr, Sum(t3.AMOUNT) AMT'#13 +
                  'FROM PR_CHECK t2, PR_CHECK_LINES t3'#13 +
                  'WHERE t2.PR_CHECK_NBR = t3.PR_CHECK_NBR'#13 +
                  'AND t2.PR_NBR = :PrNbr AND t3.CO_WORKERS_COMP_NBR > 0'#13 +
                  'AND {AsOfNow<t2>} AND {AsOfNow<t3>}'#13 +
                  'Group by t2.ee_nbr';

  FluctuatingListWC = 'SELECT DISTINCT 1 fluctuating, '+
                             't3.PR_CHECK_NBR ' +
                      'FROM PR_CHECK_LINES t3, Cl_E_DS t5 ' +
                      'WHERE t5.CL_E_DS_NBR = t3.CL_E_DS_NBR AND t3.PR_NBR = :PrNbr AND ' +
                      't3.AMOUNT > 0 AND t5.E_D_CODE_TYPE = '''+ED_OEARN_SALARY+''' AND {AsOfNow<t3>} AND {AsOfNow<t5>}';

  RatesWC = 'SELECT t1.RATE_NUMBER,t1.RATE_AMOUNT, t1.EE_NBR, t1.PRIMARY_RATE, t1.CO_WORKERS_COMP_NBR, t3.CL_E_DS_NBR ' +
              'FROM Ee_Rates t1, Co_Workers_Comp t2, Cl_E_D_Group_Codes t3 ' +
              'WHERE t2.CO_WORKERS_COMP_NBR = t1.CO_WORKERS_COMP_NBR AND t2.CL_E_D_GROUPS_NBR = t3.CL_E_D_GROUPS_NBR AND ' +
              '{AsOfNow<t1>} AND ' +
              '{AsOfNow<t2>} AND ' +
              '{AsOfNow<t3>}';

  TimeOffBalances = 'select e.ee_time_off_accrual_nbr, #FIELDS '+
    'COALESCE(e.current_accrued,0) + sum(COALESCE(o.accrued_capped, o.accrued, 0)) accrued, '+
    'COALESCE(e.current_used,0) + COALESCE(sum(o.used),0) used '+
    'from ee_time_off_accrual e '+
    'left outer join ee_time_off_accrual_oper o on '+
    ' o.ee_time_off_accrual_nbr=e.ee_time_off_accrual_nbr and '+
    ' o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') ' +
    ' and {AsOfNow<o>} ' +
    'where {AsOfNow<e>}' + ' #COND '+
    'group by e.ee_time_off_accrual_nbr,e.current_accrued,e.current_used '+
    ' #GRPC ';

  TimeOffBalanceCheck = 'select sum(o.USED) used, sum(o.ACCRUED) accrued, c.DESCRIPTION, c.CL_E_DS_NBR '+
    'from PR_CHECK k ' +
    'join EE_TIME_OFF_ACCRUAL a on a.EE_NBR=k.EE_NBR '+
    'join EE_TIME_OFF_ACCRUAL_OPER o on a.EE_TIME_OFF_ACCRUAL_NBR=o.EE_TIME_OFF_ACCRUAL_NBR '+
    'join CO_TIME_OFF_ACCRUAL c on c.CO_TIME_OFF_ACCRUAL_NBR = a.CO_TIME_OFF_ACCRUAL_NBR '+
    'where k.PR_CHECK_NBR=:PR_CHECK_NBR '+
    ' and {AsOfNow<o>}' +
    ' and {AsOfNow<a>}' +
    ' and {AsOfNow<c>}' +
    ' and {AsOfNow<k>}' +
    ' and (o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') ' +
    'or (o.PR_CHECK_NBR =k.PR_CHECK_NBR)) '+
    'and exists(select 1 from PR_CHECK_LINES l where l.PR_CHECK_NBR=k.PR_CHECK_NBR and l.CL_E_DS_NBR=c.CL_E_DS_NBR) ' +
    'group by a.CO_TIME_OFF_ACCRUAL_NBR, c.DESCRIPTION, c.CL_E_DS_NBR '+
    'order by a.CO_TIME_OFF_ACCRUAL_NBR, c.DESCRIPTION, c.CL_E_DS_NBR';

  TimeOffPrBalances = 'select e.ee_time_off_accrual_nbr, e.ee_nbr, e.EFFECTIVE_ACCRUAL_DATE, '+
    'c.description, c.SHOW_USED_BALANCE_ON_CHECK, c.PROBATION_PERIOD_MONTHS, c.DIVISOR, c.DIVISOR_DESCRIPTION, o2.accrued_this_pay, o2.used_this_pay, c.Filler, '+
    'c.DIGITS_AFTER_DECIMAL, ' +
    'ConvertNullDouble(e.current_accrued,0)+ConvertNullDouble(sum((select nvl from NVL_DOUBLE(o.accrued_capped, o.accrued))), 0) accrued, '+
    'ConvertNullDouble(e.current_used,0)+ConvertNullDouble(sum(o.used),0) used '+
    'from ee_time_off_accrual e '+
    'join co_time_off_accrual c on c.co_time_off_accrual_nbr=e.co_time_off_accrual_nbr '+
    'left outer join ee_time_off_accrual_oper o on '+
    '  o.ee_time_off_accrual_nbr=e.ee_time_off_accrual_nbr and '+
    '  o.accrual_date <= :CheckDate and '+
    '  o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' +
                                      EE_TOA_OPER_CODE_REQUEST_APPROVED +''', ''' +
                                      EE_TOA_OPER_CODE_REQUEST_DENIED + ''')  and ' +
    '  {AsOfNow<o>} ' +
    'left outer join (select o2.ee_time_off_accrual_nbr, '+
    '                   ConvertNullDouble(sum((select nvl from NVL_DOUBLE(o2.accrued_capped, o2.accrued))), 0) accrued_this_pay, '+
    '                   ConvertNullDouble(sum(o2.used),0) used_this_pay '+
    '                 from ee_time_off_accrual_oper o2 '+
    '                 where o2.accrual_date = :ThisPayDate and ' +
                          ' o2.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' +
                                                            EE_TOA_OPER_CODE_REQUEST_APPROVED+ ''', ''' +
                                                            EE_TOA_OPER_CODE_REQUEST_DENIED + ''') and ' +
                      ' {AsOfNow<o2>}'+
    '                 group by o2.ee_time_off_accrual_nbr) o2 on '+
    '  o2.ee_time_off_accrual_nbr=e.ee_time_off_accrual_nbr '+
    'where e.EE_NBR=:EeNbr and {asOfDate<e>} AND {asOfDate<c>} AND e.STATUS='''+ GROUP_BOX_YES+ '''' +
    'group by e.ee_time_off_accrual_nbr, e.ee_nbr, '+
    '  c.description, c.SHOW_USED_BALANCE_ON_CHECK, c.PROBATION_PERIOD_MONTHS, c.DIVISOR, c.DIVISOR_DESCRIPTION, o2.accrued_this_pay, o2.used_this_pay,'+
    '  e.EFFECTIVE_ACCRUAL_DATE, e.current_accrued, e.current_used, c.Filler, c.DIGITS_AFTER_DECIMAL ';

  DatedTimeOffBalances = 'select e.ee_time_off_accrual_nbr, #FIELDS '+
    'ConvertNullDouble(e.current_accrued,0)+ConvertNullDouble(sum((select nvl from NVL_DOUBLE(o.accrued_capped, o.accrued))), 0) accrued, '+
    'ConvertNullDouble(e.current_used,0)+ConvertNullDouble(sum(o.used),0) used '+
    'from ee_time_off_accrual e '+
    'left outer join ee_time_off_accrual_oper o on '+
    '  o.ee_time_off_accrual_nbr=e.ee_time_off_accrual_nbr and '+
    ' o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''')  and ' +
    '  o.accrual_date <= :EndDate and '+
    '  {AsOfNow<o>} '+
    'where ({AsOfNow<e>}) #COND '+
    'group by e.ee_time_off_accrual_nbr, e.current_accrued, e.current_used '+
    ' #GRPC ';

  BetweeenDateTimeOffBalances = 'select e.ee_time_off_accrual_nbr, e.CO_TIME_OFF_ACCRUAL_NBR, c.ANNUAL_ACCRUAL_MAXIMUM, c.ANNUAL_USAGE_MAXIMUM, '+
    'ConvertNullDouble(sum((select nvl from NVL_DOUBLE(o.accrued_capped, o.accrued))), 0) accrued, '+
    'ConvertNullDouble(sum(o.used),0) used '+
    'from ee_time_off_accrual e '+
    'left outer join ee_time_off_accrual_oper o on '+
    '  o.ee_time_off_accrual_nbr=e.ee_time_off_accrual_nbr and '+
    ' o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''')  and ' +
    '  o.accrual_date >= :BeginDate and o.accrual_date <= :EndDate and {AsOfNow<o>} '+
    ' inner join CO_TIME_OFF_ACCRUAL c on ' +
    '         {AsOfNow<c>} ' +
    '    and  e.CO_TIME_OFF_ACCRUAL_NBR=c.CO_TIME_OFF_ACCRUAL_NBR ' +
    'where ({AsOfNow<e>}) '+
    'group by e.ee_time_off_accrual_nbr , e.CO_TIME_OFF_ACCRUAL_NBR   , c.ANNUAL_ACCRUAL_MAXIMUM, c.ANNUAL_USAGE_MAXIMUM';


  TimeOffEeOperations = 'SELECT t.CL_E_DS_NBR, o.EE_TIME_OFF_ACCRUAL_NBR, o.EE_TIME_OFF_ACCRUAL_OPER_NBR, o.USED, a.STATUS '+
    'FROM EE_TIME_OFF_ACCRUAL_OPER o '+
    'JOIN EE_TIME_OFF_ACCRUAL a ON o.EE_TIME_OFF_ACCRUAL_NBR = a.EE_TIME_OFF_ACCRUAL_NBR '+
    'JOIN CO_TIME_OFF_ACCRUAL t ON t.CO_TIME_OFF_ACCRUAL_NBR = a.CO_TIME_OFF_ACCRUAL_NBR ' +
    'WHERE o.PR_NBR IS NULL AND o.OPERATION_CODE = '''+EE_TOA_OPER_CODE_REQUEST_APPROVED+''' AND '+
    'a.EE_NBR=:EE_NBR '+
    ' AND {AsOfNow<o>} AND {AsOfNow<a>}' +
    ' AND {AsOfNow<t>} AND o.ACCRUAL_DATE BETWEEN :BEGIN_DATE AND :END_DATE '+
    ' ORDER BY o.EE_TIME_OFF_ACCRUAL_NBR';

  TimeOffOperations = 'select o.*, cast(null as VARCHAR(30)) username, '+
    'c.DESCRIPTION TO_DESCRIPTION, p.check_date, p.run_number, pc.PAYMENT_SERIAL_NUMBER '+
    'from ee_time_off_accrual_oper o '+
    'join ee_time_off_accrual e on '+
      'e.ee_time_off_accrual_nbr=o.ee_time_off_accrual_nbr and {AsOfNow<e>} ' +
    'join co_time_off_accrual c on '+
      'c.co_time_off_accrual_nbr=e.co_time_off_accrual_nbr and {AsOfNow<c>} '+
    'left outer join pr p on '+
      'p.pr_nbr=o.pr_nbr and {AsOfNow<p>} '+
    'left outer join pr_check pc on '+
      'pc.pr_check_nbr=o.pr_check_nbr and {AsOfNow<pc>} ' +
    'where {AsOfNow<o>} and #COND ' +
    ' and o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''')';

  CollapsedTimeOffOperations = 'select o.ee_time_off_accrual_nbr, o.ee_time_off_accrual_oper_nbr, o.ee_time_off_accrual_oper_nbr order_nbr, o.accrual_date, ' +
    'o.CONNECTED_EE_TOA_OPER_NBR, o.OPERATION_CODE, o.pr_nbr, o.pr_batch_nbr, o.pr_check_nbr, cast(Null as Numeric(18,6)) payroll_cap_accrued, '+
    'o.note, o.accrued, o.accrued+ ConvertNullDouble(sum(o2.accrued), 0) adj_accrued, '+
    'o.accrued_capped, '+
    'o.accrued+ ConvertNullDouble(sum((select nvl from NVL_DOUBLE(o2.accrued_capped, o2.accrued))), 0) adj_accrued_capped, '+
    'o.used+ConvertNullDouble(sum(o2.used),0) adj_used '+
    'from ee_time_off_accrual_oper o '+
    'left outer join ee_time_off_accrual_oper o2 on '+
    '  o2.ADJUSTED_EE_TOA_OPER_NBR=o.ee_time_off_accrual_oper_nbr and {AsOfNow<o2>} ' +
    'where o.accrued_capped is null and o.ADJUSTED_EE_TOA_OPER_NBR is null and  {AsOfNow<o>} and #COND '+
    ' and o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') ' +
    'group by o.ee_time_off_accrual_nbr, o.ee_time_off_accrual_oper_nbr, o.accrual_date, '+
    'o.CONNECTED_EE_TOA_OPER_NBR, o.OPERATION_CODE, o.pr_nbr, o.pr_batch_nbr, o.pr_check_nbr, '+
    'o.note, o.accrued, o.accrued_capped, o.used '+

    'union all '+

    'select o.ee_time_off_accrual_nbr, o.ee_time_off_accrual_oper_nbr, o.ee_time_off_accrual_oper_nbr order_nbr, o.accrual_date, '+
    'o.CONNECTED_EE_TOA_OPER_NBR, o.OPERATION_CODE, o.pr_nbr, o.pr_batch_nbr, o.pr_check_nbr, cast(Null as Numeric(18,6)) payroll_cap_accrued, '+
    'o.note, o.accrued, o.accrued+ ConvertNullDouble(sum(o2.accrued), 0) adj_accrued, '+
    'o.accrued_capped, '+
    'o.accrued_capped+ ConvertNullDouble(sum((select nvl from NVL_DOUBLE(o2.accrued_capped, o2.accrued))), 0) adj_accrued_capped, '+
    'o.used+ConvertNullDouble(sum(o2.used),0) adj_used '+
    'from ee_time_off_accrual_oper o '+
    'left outer join ee_time_off_accrual_oper o2 on '+
    '  o2.ADJUSTED_EE_TOA_OPER_NBR=o.ee_time_off_accrual_oper_nbr and {AsOfNow<o2>} ' +
    'where o.accrued_capped is not null and o.ADJUSTED_EE_TOA_OPER_NBR is null and  {AsOfNow<o>} and #COND '+
    ' and o.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') ' +
    'group by o.ee_time_off_accrual_nbr, o.ee_time_off_accrual_oper_nbr, o.accrual_date, '+
    'o.CONNECTED_EE_TOA_OPER_NBR, o.OPERATION_CODE, o.pr_nbr, o.pr_batch_nbr, o.pr_check_nbr, '+
    'o.note, o.accrued, o.accrued_capped, o.used ';

  PayrollTimeOffEdHours = 'select l.cl_e_ds_nbr, c.ee_nbr, c.pr_batch_nbr, c.pr_check_nbr, '+
    'l.LINE_ITEM_END_DATE, l.LINE_ITEM_DATE, sum(l.HOURS_OR_PIECES) hours '+
    'from pr_check c '+
    'join pr_check_lines l on c.pr_check_nbr=l.pr_check_nbr and {AsOfNow<l>} ' +
    'where c.pr_nbr=:PrNbr and c.CHECK_TYPE in (:CheckTypeReg, :CheckTypeManual) and '+
    'c.CHECK_STATUS <> :StatusVoid and '+
    'c.EXCLUDE_TIME_OFF_ACCURAL in (#EXCLUDES) and {AsOfNow<c>} ' +
    'group by l.cl_e_ds_nbr, c.ee_nbr, c.pr_batch_nbr, c.pr_check_nbr, l.LINE_ITEM_END_DATE, l.LINE_ITEM_DATE '+
    'having sum(l.HOURS_OR_PIECES) <> 0';

  SelectDepositRecordsForReconByFED = 'select f.co_tax_deposits_nbr, f.due_date, sum(f.amount) amount '+
              'from tmp_co c join tmp_co_fed_tax_liabilities f on '+
                    'c.co_nbr=f.co_nbr and c.cl_nbr=f.cl_nbr '+
              'where cl_nbr=:CLNbr and co_nbr=:CONbr and '+
                     'c.FEDERAL_TAX_PAYMENT_METHOD in (''C'',''Z'') and '+
                     'f.TAX_TYPE in (''F'',''O'',''M'',''E'',''A'',''C'') and '+
                     'not f.CO_TAX_DEPOSITS_NBR is Null and '+
                     'f.STATUS in (''P'',''W'') and '+
                     'f.due_date between :BegDate and :EnDate '+
              'group by f.co_tax_deposits_nbr, f.due_date '+
              'order by f.due_date';

  SelectDepositRecordsForReconByFUI = 'select f.co_tax_deposits_nbr, f.due_date, sum(f.amount) amount '+
              'from tmp_co c join tmp_co_fed_tax_liabilities f on '+
                    'c.co_nbr=f.co_nbr and c.cl_nbr=f.cl_nbr '+
              'where cl_nbr=:CLNbr and co_nbr=:CONbr and '+
                     'c.FEDERAL_TAX_PAYMENT_METHOD in (''C'',''Z'') and '+
                     'f.TAX_TYPE=:TaxType and '+
                     'not f.CO_TAX_DEPOSITS_NBR is Null and '+
                     'f.STATUS in (''P'',''W'') and '+
                     'f.due_date between :BegDate and :EnDate '+
              'group by f.co_tax_deposits_nbr, f.due_date '+
              'order by f.due_date';

  SelectDepositRecordsForReconByState = 'select s.co_tax_deposits_nbr, s.due_date, sum(s.amount) amount '+
              'from tmp_co_states c join tmp_co_state_tax_liabilities s on '+
                    'c.co_nbr=s.co_nbr and c.cl_nbr=s.cl_nbr and c.state=s.state '+
              'where s.cl_nbr=:ClNbr and s.co_nbr=:CoNbr and s.state=:aState and '+
                     'c.STATE_TAX_DEPOSIT_METHOD in (''C'',''Z'') and '+
                     's.TAX_TYPE=:TaxType and not s.CO_TAX_DEPOSITS_NBR is Null and '+
                     's.STATUS in (''P'',''W'') and s.due_date between :BegDate and :EnDate '+
              'group by s.co_tax_deposits_nbr, s.due_date '+
              'order by s.due_date';

  VmrEeSettings = 'select co.co_nbr, pg.CO_PAY_GROUP_NBR, d.CO_DIVISION_NBR, '+
    'b.CO_BRANCH_NBR, dp.CO_DEPARTMENT_NBR, t.CO_TEAM_NBR, e.ee_nbr, '+
    'ConvertNullInteger(e.PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(t.PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(dp.PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(b.PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(d.PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(pg.PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.PR_CHECK_MB_GROUP_NBR, '+
    'cl.PR_CHECK_MB_GROUP_NBR))))))) PR_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(e.PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(t.PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(dp.PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(b.PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(d.PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(pg.PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.PR_REPORT_MB_GROUP_NBR, '+
    'cl.PR_REPORT_MB_GROUP_NBR))))))) PR_EE_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(e.PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(t.PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(dp.PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(b.PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(d.PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(pg.PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.PR_REPORT_SECOND_MB_GROUP_NBR, '+
    'cl.PR_REPORT_SECOND_MB_GROUP_NBR))))))) PR_EE_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(e.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(t.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(dp.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(b.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(d.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(pg.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.TAX_EE_RETURN_MB_GROUP_NBR, '+
    'cl.TAX_EE_RETURN_MB_GROUP_NBR))))))) TAX_EE_RETURN_MB_GROUP_NBR, '+
    'clp.WEB_PASSWORD, clp.E_MAIL_ADDRESS EMAIL '+
    'from CL CL '+
    'join CO CO '+
    ' on {AsOfNow<co>} ' +
    'left outer join CO_PAY_GROUP pg '+
    ' on pg.CO_NBR=co.CO_NBR and {AsOfNow<pg>} ' +
    'left outer join CO_DIVISION d '+
    ' on d.CO_NBR=co.CO_NBR and {AsOfNow<d>} ' +
    'left outer join CO_BRANCH b '+
    ' on b.CO_DIVISION_NBR=d.CO_DIVISION_NBR and {AsOfNow<b>} ' +
    'left outer join CO_DEPARTMENT dp '+
    ' on dp.CO_BRANCH_NBR=b.CO_BRANCH_NBR and {AsOfNow<dp>} ' +
    'left outer join CO_TEAM t '+
    ' on t.CO_DEPARTMENT_NBR=dp.CO_DEPARTMENT_NBR and {AsOfNow<t>} ' +
    'left outer join EE e '+
    ' on (e.CO_PAY_GROUP_NBR=pg.CO_PAY_GROUP_NBR or e.CO_PAY_GROUP_NBR is NULL) and '+
    ' (e.CO_DIVISION_NBR=d.CO_DIVISION_NBR or e.CO_DIVISION_NBR is NULL) and '+
    ' (e.CO_BRANCH_NBR=b.CO_BRANCH_NBR or e.CO_BRANCH_NBR is NULL) and '+
    ' (e.CO_DEPARTMENT_NBR=dp.CO_DEPARTMENT_NBR or e.CO_DEPARTMENT_NBR is NULL) and '+
    ' (e.CO_TEAM_NBR=t.CO_TEAM_NBR or e.CO_TEAM_NBR is NULL) and '+
    ' (e.CO_NBR=co.CO_NBR) and {AsOfNow<e>} ' +
    'left outer join CL_PERSON clp '+
    ' on clp.CL_PERSON_NBR=e.CL_PERSON_NBR and {AsOfNow<clp>} ' +
    'where {AsOfNow<cl>}';

  VmrCoSettings = 'select co.co_nbr, '+
    'ConvertNullInteger(co.PR_REPORT_MB_GROUP_NBR, cl.PR_REPORT_MB_GROUP_NBR) PR_REPORT_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.PR_REPORT_SECOND_MB_GROUP_NBR, cl.PR_REPORT_SECOND_MB_GROUP_NBR) PR_REPORT_SEC_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.AGENCY_CHECK_MB_GROUP_NBR, cl.AGENCY_CHECK_MB_GROUP_NBR) AGENCY_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.TAX_CHECK_MB_GROUP_NBR, cl.TAX_CHECK_MB_GROUP_NBR) TAX_CHECK_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.TAX_RETURN_MB_GROUP_NBR, cl.TAX_RETURN_MB_GROUP_NBR) TAX_RETURN_MB_GROUP_NBR, '+
    'ConvertNullInteger(co.TAX_RETURN_SECOND_MB_GROUP_NBR, cl.TAX_RETURN_SECOND_MB_GROUP_NBR) TAX_RETURN_SECOND_MB_GROUP_NBR '+
    'from CO CO '+
    'join CL CL '+
    'on {AsOfNow<cl>} '+
    'where {AsOfNow<co>}';

  VmrSbTopMailBoxes = 'select s.SB_MAIL_BOX_NBR,s.CL_MAIL_BOX_GROUP_NBR, s.addressee, s.BARCODE,l.UP_NBR,o.OPTION_STRING_VALUE BlockVouchers, '+
    's.SB_DELIVERY_METHOD_NBR, s.SB_MEDIA_TYPE_NBR, s.RELEASED_TIME, s.CREATED_TIME, s.SCANNED_TIME, s.PRINTED_TIME, s.SHIPPED_TIME, '+
    's.tracking_info, s.note, s.AUTO_RELEASE_TYPE, s.description, s.cl_nbr, s.pr_nbr, cast(sum(c.page_count) as INTEGER) page_count, '+
    'count(distinct s2.SB_MAIL_BOX_NBR) EMPTY_REQUIRED '+
    'from LIST_TOP_MAIL_BOXES2 l '+
    'join SB_MAIL_BOX s '+
    '  on l.up_nbr=s.SB_MAIL_BOX_NBR #CONDITION and {AsOfNow<s>} ' +
    'left outer join SB_MAIL_BOX s2 '+
    '  on l.nbr=s2.SB_MAIL_BOX_NBR and {AsOfNow<s>} and s2.required='''+ GROUP_BOX_YES+ ''' and '+
    '  not exists (select c2.SB_MAIL_BOX_CONTENT_nbr from SB_MAIL_BOX_CONTENT c2 where '+
    '  {AsOfNow<c2>} and c2.SB_MAIL_BOX_nbr=s2.SB_MAIL_BOX_NBR) '+
    'left outer join SB_MAIL_BOX_CONTENT c '+
    '  on c.SB_MAIL_BOX_NBR=s.SB_MAIL_BOX_NBR and {AsOfNow<c>} ' +
    'left outer join SB_MAIL_BOX_OPTION o '+
    '  on o.SB_MAIL_BOX_NBR=l.NBR and o.OPTION_TAG = '''+VmrBlockAutoEmailVouchers+''' and {AsOfNow<o>} '+
    'group by s.SB_MAIL_BOX_NBR,s.CL_MAIL_BOX_GROUP_NBR, s.addressee, s.BARCODE,l.UP_NBR,o.OPTION_STRING_VALUE, '+
    '  s.SB_DELIVERY_METHOD_NBR, s.SB_MEDIA_TYPE_NBR, s.RELEASED_TIME, s.CREATED_TIME, s.SCANNED_TIME, s.PRINTED_TIME, s.SHIPPED_TIME, '+
    '  s.tracking_info, s.note, s.AUTO_RELEASE_TYPE, s.description, s.cl_nbr, s.pr_nbr ';


  VmrSbMailBoxes = 'select s.SB_MAIL_BOX_NBR, s.addressee, s.PRINTED_TIME, s.SCANNED_TIME, s.BARCODE, s.UP_LEVEL_MAIL_BOX_NBR, '+
    's.SB_DELIVERY_METHOD_NBR, s.SB_MEDIA_TYPE_NBR, s.RELEASED_TIME, s.CREATED_TIME, '+
    's.note, s.AUTO_RELEASE_TYPE, s.description, s.cl_nbr, s.pr_nbr, cast(sum(c.page_count) as INTEGER) page_count '+
    'from LIST_TOP_MAIL_BOXES2 l '+
    'join SB_MAIL_BOX s on '+
    's.SB_MAIL_BOX_NBR=l.nbr and {AsOfNow<s>} ' +
    'left outer join SB_MAIL_BOX_CONTENT c '+
    'on c.SB_MAIL_BOX_NBR=s.SB_MAIL_BOX_NBR and {AsOfNow<c>} ' +
    'where l.up_nbr=:BoxNbr #condition '+
    'group by s.SB_MAIL_BOX_NBR, s.addressee, s.PRINTED_TIME, s.SCANNED_TIME, s.BARCODE, s.UP_LEVEL_MAIL_BOX_NBR, '+
    's.SB_DELIVERY_METHOD_NBR, s.SB_MEDIA_TYPE_NBR, s.RELEASED_TIME, s.CREATED_TIME, '+
    's.note, s.AUTO_RELEASE_TYPE, s.description, s.cl_nbr, s.pr_nbr';


  VmrSbMailBoxContents = 'select T2.SB_MAIL_BOX_CONTENT_NBR, ' +
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 0, 100)) as VARCHAR(100)) JOB_DESCR, '+
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 100, 100)) as VARCHAR(100)) REP_DESCR, '+
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 200, 8)) as VARCHAR(8)) EVENT_DATE, '+
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 208, 10)) as INTEGER) CO_NBR, '+
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 218, 10)) as INTEGER) PR_NBR, '+
      'USERSIDE_PRINTED REMOTEPRINTDATE, '+
      'T2.*, T1.OPTION_INTEGER_VALUE REPORT_TYPE, T0.OPTION_STRING_VALUE LAYERS, ' +
      'o5.OPTION_INTEGER_VALUE SIMPLEX_PAGES, o4.OPTION_INTEGER_VALUE DUPLEX_PAGES, ' +
      'o6.OPTION_INTEGER_VALUE DIRECTLY_EMAILED, '+
      'o3.OPTION_STRING_VALUE PASS_WORD, o2.OPTION_STRING_VALUE EMAIL, o1.OPTION_INTEGER_VALUE EE_NBR '+
      'from SB_MAIL_BOX_CONTENT T2 '+
      'left outer join SB_MAIL_BOX_OPTION o6 on '+
      '  o6.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<o6>} and '+
      '  o6.OPTION_TAG='''+ VMR_DIRECT_EMAILED_FLAG+ ''' '+
      'left outer join SB_MAIL_BOX_OPTION o5 on '+
      '  o5.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<o5>} and '+
      '  o5.OPTION_TAG='''+ VmrContentOptionSimplexPages+ ''' '+
      'left outer join SB_MAIL_BOX_OPTION o4 on '+
      '  o4.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<o4>} and '+
      '  o4.OPTION_TAG='''+ VmrContentOptionDuplexPages + ''' '+
      'left outer join SB_MAIL_BOX_OPTION o3 on '+
      '  o3.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<o3>} and '+
      '  o3.OPTION_TAG='''+ VmrContentOptionPassword + ''' '+
      'left outer join SB_MAIL_BOX_OPTION o2 on '+
      '  o2.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<o2>} and '+
      '  o2.OPTION_TAG='''+ VmrContentOptionEmail+ ''' '+
      'left outer join SB_MAIL_BOX_OPTION o1 on '+
      '  o1.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<o1>} and '+
      '  o1.OPTION_TAG='''+ VmrContentOptionEeNbr+ ''' '+
      'join SB_MAIL_BOX_OPTION T1 on '+
      '  T1.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<T1>} and '+
      '  T1.OPTION_TAG='''+ VmrContentOptionReportType + ''' '+
      'join SB_MAIL_BOX_OPTION T0 on '+
      '  T0.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<T0>} and '+
      '  T0.OPTION_TAG='''+ VmrContentOptionLayers+ ''' '+
      'where #condition T2.SB_MAIL_BOX_NBR = :BoxNbr and {AsOfNow<T2>} order by 2, 1';

  VmrSbTopMailBoxesWithVouchers =
  'select   distinct  l.UP_NBR,l.NBR, ' +
  ' o_d_em.OPTION_INTEGER_VALUE as D_EMAILED, b.* ' +
  'from LIST_TOP_MAIL_BOXES2 l ' +
  'JOIN  SB_MAIL_BOX_OPTION co on (l.NBR = co.SB_MAIL_BOX_NBR and co.OPTION_TAG = ''EE_NBR'' and {AsOfNow<co>}) ' +
  'JOIN SB_MAIL_BOX_CONTENT c on (c.SB_MAIL_BOX_CONTENT_NBR = co.SB_MAIL_BOX_CONTENT_NBR and {AsOfNow<c>})  ' +
  'JOIN SB_MAIL_BOX b on (b.SB_MAIL_BOX_NBR = l.UP_NBR and {AsOfNow<b>}) ' +

  'left outer join SB_MAIL_BOX_OPTION o_d_em on ' +
  '              o_d_em.SB_MAIL_BOX_NBR = b.SB_MAIL_BOX_NBR and {AsOfNow<o_d_em>} and ' +
  '              o_d_em.OPTION_TAG='''+VMR_DIRECT_EMAILED_COUNTER+''' ' +

  ' where (StrCopy(c.DESCRIPTION,200,8) between  #DateFrom  and #DateTo)';


  VmrSbJobs = 'select distinct T1.*, '+
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 0, 100)) as VARCHAR(100)) JOB_DESCR, '+
      'CAST(ConvertNullString(Trim(StrCopy(T2.DESCRIPTION, 208, 10)), ''0'') as INTEGER) JOB_CO_NBR, '+
      'CAST(ConvertNullString(Trim(StrCopy(T2.DESCRIPTION, 218, 10)), ''0'') as INTEGER) JOB_PR_NBR ' +
      'from LIST_TOP_MAIL_BOXES2 l  '+
      'join SB_MAIL_BOX_CONTENT T2 on '+
      '  t2.SB_MAIL_BOX_NBR=l.nbr and {AsOfNow<T2>} ' +
      'join SB_MAIL_BOX T1 on '+
      '  t1.SB_MAIL_BOX_NBR=l.up_nbr and {AsOfNow<T1>}'+
      ' #condition ';

  VmrRemotePrintJobs = 'select T2.SB_MAIL_BOX_CONTENT_NBR, T1.SB_MAIL_BOX_NBR,' +
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 0, 100)) as VARCHAR(100)) JOB_DESCR, '+
      'CAST(Trim(StrCopy(T2.DESCRIPTION, 100, 100)) as VARCHAR(100)) REP_DESCR, '+
      'T2.MEDIA_TYPE, T2.PAGE_COUNT, T2.USERSIDE_PRINTED, o1.OPTION_STRING_VALUE LOCATION '+
      'from SB_MAIL_BOX_CONTENT T2 '+
      'join SB_MAIL_BOX T1 on '+
      '  T1.SB_MAIL_BOX_NBR=T2.SB_MAIL_BOX_NBR and {AsOfNow<T1>} '+
      'left outer join SB_MAIL_BOX_OPTION o1 on '+
      '  o1.SB_MAIL_BOX_NBR = T1.SB_MAIL_BOX_NBR and {AsOfNow<o1>} and '+
      '  o1.OPTION_TAG='''+ VmrSbLocation+ ''' '+
      'where #condition and {AsOfNow<T2>} order by 2, 1 desc';

  VmrRemotePrintContent = 'select T2.SB_MAIL_BOX_CONTENT_NBR, T2.SB_MAIL_BOX_NBR,' +
      'T2.FILE_NAME, CAST(Trim(StrCopy(T2.DESCRIPTION, 100, 100)) as VARCHAR(100)) REPORT_DESCR,'+
      'o1.OPTION_INTEGER_VALUE REPORT_TYPE '+
      'from SB_MAIL_BOX_CONTENT T2 '+
      'left outer join SB_MAIL_BOX_OPTION o1 on '+
      '  o1.SB_MAIL_BOX_CONTENT_NBR = T2.SB_MAIL_BOX_CONTENT_NBR '+
      '  and  {AsOfNow<o1>} and o1.OPTION_TAG= ''REPORT_TYPE'' '+
      'where #condition and {AsOfNow<T2>}';

  CountThisYearChecksForEE = 'select count(*) Result ' +
      'from PR P ' +
      'join PR_CHECK C ' +
      'on P.PR_NBR=C.PR_NBR ' +
      'where P.CHECK_DATE >= BeginYear( current_date ) and ' +
      'C.EE_NBR = :EeNbr and ' +
      '{AsOfNow<P>} and ' +
      '{AsOfNow<C>}';


  GrossWagesYTD = 'SELECT t1.EE_NBR, ' +
              'SUM(IIF(t4.E_D_Code_Type like ''E%'', t3.Amount, -t3.Amount)) GROSS_WAGES ' +
              'FROM Pr_Check  t1, Pr  t2, Cl_E_Ds t4, Pr_Check_Lines t3  ' +
              'WHERE t2.PR_NBR = t1.PR_NBR AND ' +
                    't1.Pr_Check_Nbr = t3.Pr_Check_Nbr AND ' +
                    't4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr AND ' +
                    't1.EE_NBR = :EE_NBR AND '+
                    '{AsOfNow<t1>} and ' +
                    '{AsOfNow<t2>} and ' +
                    '{AsOfNow<t3>} and ' +
                    '{AsOfNow<t4>} and ' +
                    't2.CHECK_DATE >= :BeginDate AND ((t2.CHECK_DATE < :CheckDate OR (t2.CHECK_DATE = :CheckDate AND t2.RUN_NUMBER <= :RunNumber))) AND ' +
                    '(t4.E_D_Code_Type LIKE ''E%'' OR t3.Cl_E_Ds_Nbr IN (#WcEds) AND t3.Co_Workers_Comp_Nbr = :WcNbr) ' +
              'GROUP BY t1.EE_NBR';

  SelectDBDTG = 'select cast(''co_division_nbr'' as varchar(20)) level_field, cast(''Division'' as varchar(20)) level_name, '+
      '  co_division_nbr NBR, custom_division_number number, name from co_division '+
      'where co_nbr=:CoNbr and {AsOfDate<co_division>} '+
      '  and ((UpperCase(name) like :Input) or (UpperCase(custom_division_number) like :Input)) '+
      '  and PRINT_DIV_ADDRESS_ON_CHECKS <> '''+EXCLUDE_REPT_AND_ADDRESS_ON_CHECK+''' '+
      'union all '+
      'select cast(''co_branch_nbr'' as varchar(20)) level_field, cast(''Branch'' as varchar(20)) level_name, '+
      '  co_branch_nbr NBR, custom_branch_number number, name from co_branch '+
      'where co_division_nbr in (select co_division_nbr from co_division '+
      '  where co_nbr=:CoNbr and {AsOfDate<co_division>}) '+
      '  and {AsOfDate<co_branch>} and ((UpperCase(name) like :Input) or (UpperCase(custom_branch_number) like :Input)) '+
      '  and PRINT_BRANCH_ADDRESS_ON_CHECK <> '''+EXCLUDE_REPT_AND_ADDRESS_ON_CHECK+''' '+
      'union all '+
      'select cast(''CO_DEPARTMENT_NBR'' as varchar(20)) level_field, cast(''Department'' as varchar(20)) level_name, '+
      '  CO_DEPARTMENT_NBR NBR, custom_department_number number, name from CO_DEPARTMENT '+
      'where co_branch_nbr in (select co_branch_nbr from co_branch '+
      '  where co_division_nbr in (select co_division_nbr from co_division '+
      '    where {AsOfDate<co_division>} and co_nbr=:CoNbr) and {AsOfDate<co_branch>}) '+
      '  and {AsOfDate<CO_DEPARTMENT>} and ((UpperCase(name) like :Input) or (UpperCase(custom_department_number) like :Input)) '+
      '  and PRINT_DEPT_ADDRESS_ON_CHECKS <> '''+EXCLUDE_REPT_AND_ADDRESS_ON_CHECK+''' '+
      'union all '+
      'select cast(''CO_TEAM_NBR'' as varchar(20)) level_field, cast(''Team'' as varchar(20)) level_name, '+
      '  CO_TEAM_NBR NBR, custom_team_number number, name from CO_team '+
      'where co_department_nbr in (select co_department_nbr from co_department '+
      '  where {AsOfDate<co_department>} and co_branch_nbr in (select co_branch_nbr from co_branch '+
      '    where {AsOfDate<co_branch>} and co_division_nbr in (select co_division_nbr from co_division '+
      '      where {AsOfDate<co_division>} and co_nbr=:CoNbr))) '+
      '    and {AsOfDate<CO_team>} and ((UpperCase(name) like :Input) or (UpperCase(custom_team_number) like :Input)) '+
      '    and PRINT_GROUP_ADDRESS_ON_CHECK <> '''+EXCLUDE_REPT_AND_ADDRESS_ON_CHECK+''' '+
      'union all '+
      'select cast(''CO_PAY_GROUP_NBR'' as varchar(20)) level_field, cast(''Pay Group'' as varchar(20)) level_name, '+
      '  CO_PAY_GROUP_NBR NBR, cast('''' as varchar(20)) number, DESCRIPTION name from CO_PAY_GROUP '+
      'where {AsOfDate<CO_PAY_GROUP>} and co_nbr=:CoNbr '+
      '  and ((UpperCase(DESCRIPTION) like :Input)) ';

  SelectEe = 'select ee_nbr '+ //, custom_employee_number, SOCIAL_SECURITY_NUMBER, first_name, last_name '+
      'from ee e '+
      'join CL_PERSON p on p.CL_PERSON_NBR=e.CL_PERSON_NBR '+
      'and e.co_nbr=:CoNbr and {AsOfDate<e>} and {AsOfDate<p>} and '+
      '  (UpperCase(custom_employee_number) like :Input or UpperCase(first_name) like :Input or '+
      '   UpperCase(last_name) like :Input or UpperCase(SOCIAL_SECURITY_NUMBER) like :Input) ';

  MiscChecksForACH = 'select sum(ld.amount) amount, c.MISCELLANEOUS_CHECK_TYPE, ' +
                     'ld.co_division_nbr, ' +
                     'ld.co_branch_nbr, ' +
                     'ld.co_department_nbr, ' +
                     'ld.co_team_nbr ' +
              'FROM pr_miscellaneous_checks c, pr_check_lines ld ' +
              'WHERE c.pr_miscellaneous_checks_nbr = ld.pr_miscellaneous_checks_nbr and ' +
                    'c.MISCELLANEOUS_CHECK_TYPE in (''' + MISC_CHECK_TYPE_AGENCY + ''', ''' + MISC_CHECK_TYPE_AGENCY_VOID + ''') and ' +
                    'c.pr_nbr = :PrNbr and ' +
                    '{AsOfNow<ld>} and ' +
                    '{AsOfNow<c>} ' +
              'group by c.MISCELLANEOUS_CHECK_TYPE, ld.co_division_nbr, ' +
                       'ld.co_branch_nbr, ' +
                       'ld.co_department_nbr, ' +
                       'ld.co_team_nbr';

  GetTotalForEDType = 'select sum(L.AMOUNT)'#13 +
      'from PR_CHECK_LINES L ' +
      'join CL_E_DS C ' +
      'on L.CL_E_DS_NBR=C.CL_E_DS_NBR ' +
      'where L.PR_NBR=:PrNbr and ' +
      'C.E_D_CODE_TYPE=:EDType and ' +
      '{AsOfNow<L>} and ' +
      '{AsOfNow<C>}';

  SelectCheckDatesFromPr = 'select CHECK_DATE ' +
      'from PR ' +
      'where CHECK_DATE>:CheckDate and {AsOfNow<PR>} ' +
      'order by CHECK_DATE';

  SelectCheckNumbers = 'select C.PAYMENT_SERIAL_NUMBER ' +
      'from EE E, PR P, PR_CHECK C ' +
      'where E.CL_PERSON_NBR = :Nbr and ' +
            'E.CO_NBR = :CoNbr and ' +
            'E.EE_NBR = C.EE_NBR and ' +
            'P.PR_NBR = C.PR_NBR and ' +
            'P.CHECK_DATE > ''now'' - 30 and ' +
            'P.PAYROLL_TYPE = ''R'' and ' +
            'P.STATUS = ''P'' and ' +
            '{AsOfNow<P>} and ' +
            '{AsOfNow<C>} and ' +
            '{AsOfNow<E>} ' +
      'order by P.CHECK_DATE';

  SelectPrRelatedEeTimeOpers = 'select * from EE_TIME_OFF_ACCRUAL_OPER where {AsOfNow<EE_TIME_OFF_ACCRUAL_OPER>} and '+
      '(adjusted_ee_toa_oper_nbr in (select EE_TIME_OFF_ACCRUAL_OPER_NBR from EE_TIME_OFF_ACCRUAL_OPER where {AsOfNow<EE_TIME_OFF_ACCRUAL_OPER>} and PR_NBR=:PrNbr) '+
      'or connected_ee_toa_oper_nbr in (select EE_TIME_OFF_ACCRUAL_OPER_NBR from EE_TIME_OFF_ACCRUAL_OPER where {AsOfNow<EE_TIME_OFF_ACCRUAL_OPER>} and PR_NBR=:PrNbr) '+
      ')';

  CountWebUsers = 'select count(*) from SB_USER u where {AsOfNow<u>} and '+
         'trim(strcopy(user_functions, 0, 8)) = ''-2'' and ' +
         '(select count(*) from sb_sec_clients where {AsOfNow<sb_sec_clients>} and ' +
           'sb_user_nbr=u.sb_user_nbr) = 1 and ' +
         '(select avg(cl_nbr) from sb_sec_clients where {AsOfNow<sb_sec_clients>} and ' +
           'sb_user_nbr=u.sb_user_nbr) = :ClNbr';

  NewPrenotes = 'select #Level, count(distinct dd.ee_direct_deposit_nbr) from ee ee, ee_direct_deposit dd where {AsOfNow<ee>} and '+
         '{AsOfNow<dd>} and ' +
         'ee.ee_nbr = dd.ee_nbr and in_prenote = ''Y'' and co_nbr = :CoNbr and ' +
         'dd.effective_date > :PrevCheckDate ' +
         'group by #Level';


  SecurityStructure  = 'SELECT  Storage_Data Data FROM Sy_Storage WHERE {AsOfNow<Sy_Storage>} AND ' +
                       '        Tag = ''' + SecurityStructureStorageTag + '''';

  SecurityFieldsStructure  = 'SELECT  Storage_Data Data FROM Sy_Storage WHERE {AsOfNow<Sy_Storage>} AND ' +
                       '        Tag = ''' + FieldLevelStructureStorageTag + '''';


  SecurityUserGroups = 'SELECT g.* ' +
                       'FROM SB_SEC_GROUPS g, SB_SEC_GROUP_MEMBERS gm ' +
                       'WHERE {AsOfNow<g>} AND {AsOfNow<gm>} AND ' +
                       '      g.SB_SEC_GROUPS_NBR = gm.SB_SEC_GROUPS_NBR AND ' +
                       '      gm.SB_USER_NBR = :SB_USER_NBR';


  SecurityUserRights = 'SELECT ''G'' src, Cast(Trim(strcopy(tag, 2, 128)) as VarChar(128)) tag, context ' +
                       'FROM   SB_SEC_RIGHTS r, SB_SEC_GROUP_MEMBERS gm ' +
                       'WHERE  {AsOfNow<r>} AND {AsOfNow<gm>} AND ' +
                       '       r.SB_SEC_GROUPS_NBR = gm.SB_SEC_GROUPS_NBR AND ' +
                       '       gm.SB_USER_NBR = :SB_USER_NBR AND' +
                       '       r.tag STARTS :SEC_ELEMENT_TYPE ' +
                       'UNION ' +
                       'SELECT ''U'' src, Cast(Trim(strcopy(tag, 2, 128)) as VarChar(128)) tag, context ' +
                       'FROM   SB_SEC_RIGHTS r ' +
                       'WHERE  {AsOfNow<r>} AND ' +
                       '       r.SB_USER_NBR = :SB_USER_NBR AND ' +
                       '       r.tag STARTS :SEC_ELEMENT_TYPE ' +
                       'ORDER BY 1, 2';


  SecurityGroupRights = 'SELECT Cast(Trim(strcopy(tag, 2, 128)) as VarChar(128)) tag, context ' +
                        'FROM   SB_SEC_RIGHTS r ' +
                        'WHERE  {AsOfNow<r>} AND ' +
                        '       r.SB_SEC_GROUPS_NBR = :SB_SEC_GROUPS_NBR AND' +
                        '       r.tag STARTS :SEC_ELEMENT_TYPE ' +
                        'ORDER BY 1';


  SecurityGroupClients = 'SELECT Cl_Nbr ' +
                        'FROM   Sb_Sec_Clients c ' +
                        'WHERE  {AsOfNow<c>} AND ' +
                        '       c.SB_SEC_GROUPS_NBR = :SB_SEC_GROUPS_NBR ' +
                        'ORDER BY 1';


  SecurityUserClients = 'SELECT c.Cl_Nbr '+
                        'FROM  Sb_Sec_Clients c,  Sb_Sec_Group_Members gm '+
                        'WHERE {AsOfNow<c>} AND ' +
                        '      {AsOfNow<gm>} AND ' +
                        '      c.Sb_Sec_Groups_Nbr = gm.Sb_Sec_Groups_Nbr AND'+
                        '      gm.Sb_User_Nbr = :SB_USER_NBR ' +
                        'UNION ' +
                        'SELECT c.Cl_Nbr ' +
                        'FROM Sb_Sec_Clients c ' +
                        'WHERE {AsOfNow<c>} AND ' +
                        '      c.Sb_User_Nbr = :SB_USER_NBR';

  SecurityGroupRecords = 'SELECT s.Cl_Nbr, s.Filter_Type, s.Custom_Expr ' +
                         'FROM   Sb_Sec_Row_Filters s ' +
                         'WHERE  {AsOfNow<s>} AND ' +
                         '       s.SB_SEC_GROUPS_NBR = :SB_SEC_GROUPS_NBR';


  SecurityUserRecords = 'SELECT s.Cl_Nbr, s.Filter_Type, s.Custom_Expr '+
                        'FROM  Sb_Sec_Row_Filters s,  Sb_Sec_Group_Members gm '+
                        'WHERE {AsOfNow<s>} AND ' +
                        '      {AsOfNow<gm>} AND ' +
                        '      s.Sb_Sec_Groups_Nbr = gm.Sb_Sec_Groups_Nbr AND'+
                        '      gm.Sb_User_Nbr = :SB_USER_NBR ' +
                        'UNION ' +
                        'SELECT s.Cl_Nbr, s.Filter_Type, s.Custom_Expr ' +
                        'FROM Sb_Sec_Row_Filters s ' +
                        'WHERE {AsOfNow<s>} AND ' +
                        '      s.Sb_User_Nbr = :SB_USER_NBR';

  AllClientNumbers = 'SELECT CL_NBR from ALL_CL_NUMBERS(:DB_Path)';

  ProcCOPY_PAYROLL_BATCHES = 'EXECUTE PROCEDURE COPY_PAYROLL_BATCHES' +
                             '(:OLD_PAYROLL_NUMBER, :NEW_PAYROLL_NUMBER, :AN_ACTION, :USER_ID, :CHANGE_DATE)';

  ProcLOCK_PR = 'EXECUTE PROCEDURE LOCK_PR(:PR_NBR)';

  ProcUNLOCK_PR = 'EXECUTE PROCEDURE UNLOCK_PR(:PR_NBR)';

  ProcCL_NBR = 'EXECUTE PROCEDURE CL_NBR';

  ProcCL_COPY = 'EXECUTE PROCEDURE CL_COPY(:DB_PATH, :CLIENT_NAME)';

  ProcEE_YTD = 'EXECUTE PROCEDURE EE_YTD(:EE_NUMBER, :START_DATE, :END_DATE, :YTD_TYPE, :RECORD_NUMBER)';


  SelectDIVWithName = 'select D.CUSTOM_DIVISION_NUMBER, D.NAME DNAME, D.CO_DIVISION_NBR ' +
    'from CO_DIVISION D ' +
    'where D.CO_NBR = :CoNbr and {AsOfNow<D>}';

  SelectDBWithName = 'select D.CUSTOM_DIVISION_NUMBER, D.NAME DNAME, B.CUSTOM_BRANCH_NUMBER, B.NAME BNAME, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR ' +
    'from CO_DIVISION D ' +
    'left outer join CO_BRANCH B '+
    'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} '+
    'where D.CO_NBR = :CoNbr and {AsOfNow<D>}';

  SelectDBDWithName = 'select D.CUSTOM_DIVISION_NUMBER, D.NAME DNAME, B.CUSTOM_BRANCH_NUMBER, B.NAME BNAME, P.CUSTOM_DEPARTMENT_NUMBER, P.NAME PNAME, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR ' +
    'from CO_DIVISION D ' +
    'left outer join CO_BRANCH B '+
    'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>} '+
    'left outer join CO_DEPARTMENT P '+
    'on P.CO_BRANCH_NBR=B.CO_BRANCH_NBR and {AsOfNow<P>} '+
    'where D.CO_NBR = :CoNbr and {AsOfNow<D>}';


  CountMissedOPTPayrolls = 'select count(distinct P.PR_NBR) PAYROLL_COUNT ' +
    'from PR_CHECK_LOCALS L ' +
    'join PR P ' +
    'on P.PR_NBR=L.PR_NBR ' +
    'where L.EE_LOCALS_NBR=:EELocalNbr and ' +
    'P.STATUS=''' + PAYROLL_STATUS_PROCESSED + ''' and ' +
    'P.SCHEDULED=''Y'' and ' +
    'P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and ' +
    'L.LOCAL_TAXABLE_WAGE<>0 and ' +
    'L.LOCAL_TAX=0 and ' +
    '{AsOfNow<L>} and ' +
    '{AsOfNow<P>}';


  List_Reprint_History_Detail = 'SELECT  t2.PR_REPRINT_HISTORY_NBR, t2.PR_REPRINT_HISTORY_DETAIL_NBR, t2.Miscellaneous_Check,' +
    ' t2.Begin_Check_Number, t1.Payment_Serial_Number  BeginPayment_Serial_Number, t2.End_Check_Number,' +
    ' t3.Payment_Serial_Number  EndPayment_Serial_Number ' +
    '   FROM Pr_Reprint_History_Detail t2, Pr_Check t3, Pr_Check t1 ' +
    ' WHERE {AsOfNow<t2>} and ' +
            '{AsOfNow<t3>} and {AsOfNow<t1>}' +
    ' and t2.End_Check_Number = t3.Pr_Check_Nbr and t2.Begin_Check_Number = t1.Pr_Check_Nbr' +
    ' and t2.PR_REPRINT_HISTORY_NBR = :Pr_History_Nbr ';

  TaxDepositAmountList = 'SELECT t1.Co_Tax_Deposits_Nbr, SUM(t2.Amount)  Amount ' +
           ' FROM   Co_Tax_Deposits  t1,  Co_Fed_Tax_Liabilities  t2 ' +
           ' WHERE  t1.Co_Tax_Deposits_Nbr = t2.Co_Tax_Deposits_Nbr and ' +
           ' {AsOfNow<t1>} and {AsOfNow<t2>} and t1.DEPOSIT_TYPE =''D''' +
           ' GROUP BY 1 '+
           ' UNION ' +
           ' SELECT t4.Co_Tax_Deposits_Nbr, SUM(t5.Amount)  Amount ' +
           ' FROM   Co_Tax_Deposits  t4, Co_State_Tax_Liabilities  t5 ' +
           ' WHERE  t4.Co_Tax_Deposits_Nbr = t5.Co_Tax_Deposits_Nbr and ' +
           ' {AsOfNow<t4>} and {AsOfNow<t5>} and t4.DEPOSIT_TYPE =''D''' +
           ' GROUP BY  1 ' +
           ' UNION ' +
           ' SELECT  t6.Co_Tax_Deposits_Nbr,  SUM(t7.Amount)  Amount ' +
           ' FROM    Co_Tax_Deposits  t6,  Co_Sui_Liabilities  t7 ' +
           ' WHERE   t6.Co_Tax_Deposits_Nbr = t7.Co_Tax_Deposits_Nbr and ' +
           ' {AsOfNow<t6>} and {AsOfNow<t7>} and ' + ' t6.DEPOSIT_TYPE =''D''' +
           ' GROUP BY  1 ' +
           ' UNION ' +
           ' SELECT   t8.Co_Tax_Deposits_Nbr, SUM(t9.Amount)  Amount ' +
           ' FROM     Co_Tax_Deposits  t8,  Co_Local_Tax_Liabilities  t9 ' +
           ' WHERE    t8.Co_Tax_Deposits_Nbr = t9.Co_Tax_Deposits_Nbr and ' +
           ' {AsOfNow<t8>} and {AsOfNow<t9>} and t8.DEPOSIT_TYPE =''D''' +
           ' GROUP BY  1 ';


  SelectEmployeesWithDBDT = 'select first_name, last_name, ee_nbr, custom_employee_number, ' +
      'co_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, '+
      'Current_Termination_Code, Selfserve_Enabled ' +
      'from ee e join ' +
      'cl_person cp on e.cl_person_nbr=cp.cl_person_nbr ' +
      'where e.co_nbr = :CoNbr and ' +
      '{AsOfNow<e>} and ' +
      '{AsOfNow<cp>} #CONDITION';

  SelectCOGroupMangerName = ' select first_name || '' '' || last_name Name from co_group_manager gg '+
      'join ee e on e.EE_nbr = gg.EE_nbr '+
      'join cl_person cp on e.cl_person_nbr = cp.cl_person_nbr '+
      'where gg.Co_Group_Nbr = :CoGroupNbr '+
      'and {AsOfNow<gg>} ' +
      'and e.co_nbr = :CoNbr '+
      'and {AsOfNow<e>} ' +
      'and {AsOfNow<cp>} ' +
      'order by custom_employee_number ';

    CheckIfQecExists = 'select 1 '+
        'from CO CO ' +
        'join PR PR on PR.CO_NBR=CO.CO_NBR and PR.PAYROLL_TYPE='''+PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT+
                     ''' and PR.STATUS not in ('''+PAYROLL_STATUS_PROCESSED+''','''+PAYROLL_STATUS_VOIDED+''') '+
        'where  (CO.CL_COMMON_PAYMASTER_NBR= '+
           '( select C1.CL_COMMON_PAYMASTER_NBR '+
           ' from CO C1 '+
           '  join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=C1.CL_COMMON_PAYMASTER_NBR'+
           '  where C1.CO_NBR=:CO_NBR and M.COMBINE_STATE_SUI=''Y'' and {AsOfNow<C1>}' +
           ' and {AsOfNow<M>})) '+
       ' and {AsOfNow<CO>} and {AsOfNow<PR>}';

  NonVersionFieldAuditByNbr =
    '/*$1*/'#13 +
    'EXECUTE BLOCK'#13 +
    'RETURNS (commit_nbr INTEGER, table_change_nbr INTEGER, change_time TIMESTAMP,'#13 +
    '         user_id INTEGER, rec_nbr INTEGER, change_type CHAR(1),'#13 +
    '         field_nbr INTEGER, old_value VARCHAR(512))'#13 +
    'AS'#13 +
    'DECLARE VARIABLE table_nbr INTEGER;'#13 +
    'DECLARE VARIABLE date_b TIMESTAMP;'#13 +
    'BEGIN'#13 +
    '  table_nbr = $2;'#13 +
    '  date_b = ''$3'';'#13 +
    ''#13 +
    '  FOR SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
    '             t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
    '      FROM ev_transaction tr, ev_table_change t, ev_field_change f'#13 +
    '      WHERE'#13 +
    '        tr.nbr = t.ev_transaction_nbr AND'#13 +
    '        t.nbr = f.ev_table_change_nbr AND'#13 +
    '        f.ev_field_nbr IN ($4) AND'#13 +
    '        tr.commit_time >= :date_b AND'#13 +
    '        t.ev_table_nbr = :table_nbr AND'#13 +
    '        t.change_type = ''U'''#13 +
    '      UNION'#13 +
    '      SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
    '             t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
    '      FROM ev_transaction tr,'#13 +
    '           ev_table_change t LEFT JOIN ev_field_change f'#13 +
    '             ON t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN ($4)'#13 +
    '      WHERE'#13 +
    '        tr.nbr = t.ev_transaction_nbr AND'#13 +
    '        tr.commit_time >= :date_b AND'#13 +
    '        t.ev_table_nbr = :table_nbr AND'#13 +
    '        t.change_type IN (''I'', ''D'')'#13 +
    '      INTO commit_nbr, table_change_nbr, change_time, user_id, change_type,'#13 +
    '           rec_nbr, field_nbr, old_value'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    IF ((field_nbr IS NULL) OR (field_nbr IN ($4))) THEN'#13 +
    '      SUSPEND;'#13 +
    '  END'#13 +
    'END';

  NonVersionFieldAudit =
      '/*$1*/'#13 +
      'EXECUTE BLOCK'#13 +
      'RETURNS (commit_nbr INTEGER, table_change_nbr INTEGER, change_time TIMESTAMP,'#13 +
      '         user_id INTEGER, rec_nbr INTEGER, change_type CHAR(1),'#13 +
      '         field_nbr INTEGER, old_value VARCHAR(512))'#13 +
      'AS'#13 +
      'DECLARE VARIABLE table_nbr INTEGER;'#13 +
      'DECLARE VARIABLE date_b TIMESTAMP;'#13 +
      'BEGIN'#13 +
      '  table_nbr = $2;'#13 +
      '  date_b = ''$3'';'#13 +
      '  rec_nbr = $5;'#13 +
      ''#13 +
      '  FOR SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
      '             t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
      '      FROM ev_transaction tr, ev_table_change t, ev_field_change f'#13 +
      '      WHERE'#13 +
      '        tr.nbr = t.ev_transaction_nbr AND'#13 +
      '        t.nbr = f.ev_table_change_nbr AND'#13 +
      '        f.ev_field_nbr IN ($4) AND'#13 +
      '        tr.commit_time >= :date_b AND'#13 +
      '        t.ev_table_nbr = :table_nbr AND'#13 +
      '        t.change_type = ''U'' AND'#13 +
      '        t.record_nbr = :rec_nbr'#13 +
      '      UNION'#13 +
      '      SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
      '             t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
      '      FROM ev_transaction tr,'#13 +
      '           ev_table_change t LEFT JOIN ev_field_change f'#13 +
      '             ON t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN ($4)'#13 +
      '      WHERE'#13 +
      '        tr.nbr = t.ev_transaction_nbr AND'#13 +
      '        tr.commit_time >= :date_b AND'#13 +
      '        t.ev_table_nbr = :table_nbr AND'#13 +
      '        t.change_type IN (''I'', ''D'') AND'#13 +
      '        t.record_nbr = :rec_nbr'#13 +
      '      INTO commit_nbr, table_change_nbr, change_time, user_id, change_type,'#13 +
      '           rec_nbr, field_nbr, old_value'#13 +
      '  DO'#13 +
      '  BEGIN'#13 +
      '    IF ((field_nbr IS NULL) OR (field_nbr IN ($4))) THEN'#13 +
      '      SUSPEND;'#13 +
      '  END'#13 +
      'END';

  VersionFieldAsOf =
      '/*$1*/'#13 +
      'EXECUTE BLOCK'#13 +
      'RETURNS (commit_nbr INTEGER, table_change_nbr INTEGER, change_time TIMESTAMP,'#13 +
      '         user_id INTEGER, rec_version INTEGER, rec_nbr INTEGER, change_type CHAR(1),'#13 +
      '         field_kind CHAR(1), old_value VARCHAR(512))'#13 +
      'AS'#13 +
      'DECLARE VARIABLE table_name VARCHAR(32);'#13 +
      'DECLARE VARIABLE field_name VARCHAR(32);'#13 +
      'DECLARE VARIABLE table_nbr INTEGER;'#13 +
      'DECLARE VARIABLE field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE ed_field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE eu_field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE curr_field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE field_type CHAR(1);'#13 +
      'BEGIN'#13 +
      '  table_name = ''$2'';'#13 +
      '  field_name = ''$3'';'#13 +
      '  rec_nbr = $4;'#13 +
      ''#13 +
      '  SELECT t.nbr, f.nbr, f.field_type FROM ev_table t, ev_field f'#13 +
      '  WHERE'#13 +
      '    f.ev_table_nbr = t.nbr AND'#13 +
      '    t.name = :table_name AND'#13 +
      '    f.name = :field_name'#13 +
      '  INTO table_nbr, field_nbr, field_type;'#13 +
      ''#13 +
      '  SELECT f.nbr FROM ev_field f'#13 +
      '  WHERE'#13 +
      '    f.ev_table_nbr = :table_nbr AND'#13 +
      '    f.name = ''EFFECTIVE_DATE'''#13 +
      '  INTO ed_field_nbr;'#13 +
      ''#13 +
      '  SELECT f.nbr FROM ev_field f'#13 +
      '  WHERE'#13 +
      '    f.ev_table_nbr = :table_nbr AND'#13 +
      '    f.name = ''EFFECTIVE_UNTIL'''#13 +
      '  INTO eu_field_nbr;'#13 +
      ''#13 +
      '  FOR SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
      '             t.record_id, t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
      '      FROM ev_transaction tr, ev_table_change t, ev_field_change f'#13 +
      '      WHERE'#13 +
      '        tr.nbr = t.ev_transaction_nbr AND'#13 +
      '        t.nbr = f.ev_table_change_nbr AND'#13 +
      '        f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr) AND'#13 +
      '        t.record_nbr = :rec_nbr AND'#13 +
      '        t.ev_table_nbr = :table_nbr AND'#13 +
      '        t.change_type = ''U'''#13 +
      '      UNION'#13 +
      '      SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
      '             t.record_id, t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
      '      FROM ev_transaction tr,'#13 +
      '           ev_table_change t left join ev_field_change f'#13 +
      '             on t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr)'#13 +
      '      WHERE'#13 +
      '        tr.nbr = t.ev_transaction_nbr AND'#13 +
      '        t.record_nbr = :rec_nbr AND'#13 +
      '        t.ev_table_nbr = :table_nbr AND'#13 +
      '        t.change_type IN (''I'', ''D'')'#13 +
      '      INTO commit_nbr, table_change_nbr, change_time, user_id, change_type,'#13 +
      '           rec_version, rec_nbr, curr_field_nbr, old_value'#13 +
      '  DO'#13 +
      '  BEGIN'#13 +
      '    if ((curr_field_nbr is null) or (curr_field_nbr in (field_nbr, ed_field_nbr, eu_field_nbr))) then'#13 +
      '    BEGIN'#13 +
      '      IF (curr_field_nbr = ed_field_nbr) THEN'#13 +
      '        field_kind = ''E'';'#13 +
      '      ELSE IF (curr_field_nbr = eu_field_nbr) THEN'#13 +
      '        field_kind = ''U'';'#13 +
      '      ELSE if (curr_field_nbr = field_nbr) then'#13 +
      '        field_kind = ''F'';'#13 +
      '      ELSE'#13 +
      '        field_kind = NULL;'#13 +
      '    '#13 +
      '      SUSPEND;'#13 +
      '    END'#13 +
      '  END'#13 +
      'END';

  VersionFieldAsOfByNbr =
      '/*$1*/'#13 +
      'EXECUTE BLOCK'#13 +
      'RETURNS (commit_nbr INTEGER, table_change_nbr INTEGER, change_time TIMESTAMP,'#13 +
      '         user_id INTEGER, rec_version INTEGER, rec_nbr INTEGER, change_type CHAR(1),'#13 +
      '         field_kind CHAR(1), old_value VARCHAR(512))'#13 +
      'AS'#13 +
      'DECLARE VARIABLE table_name VARCHAR(32);'#13 +
      'DECLARE VARIABLE field_name VARCHAR(32);'#13 +
      'DECLARE VARIABLE table_nbr INTEGER;'#13 +
      'DECLARE VARIABLE field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE ed_field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE eu_field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE curr_field_nbr INTEGER;'#13 +
      'DECLARE VARIABLE field_type CHAR(1);'#13 +
      'DECLARE VARIABLE date_b TIMESTAMP;'#13 +
      'BEGIN'#13 +
      '  table_name = ''$2'';'#13 +
      '  field_name = ''$3'';'#13 +
      '  date_b = ''$4'';'#13 +
      ''#13 +
      '  SELECT t.nbr, f.nbr, f.field_type FROM ev_table t, ev_field f'#13 +
      '  WHERE'#13 +
      '    f.ev_table_nbr = t.nbr AND'#13 +
      '    t.name = :table_name AND'#13 +
      '    f.name = :field_name'#13 +
      '  INTO table_nbr, field_nbr, field_type;'#13 +
      ''#13 +
      '  SELECT f.nbr FROM ev_field f'#13 +
      '  WHERE'#13 +
      '    f.ev_table_nbr = :table_nbr AND'#13 +
      '    f.name = ''EFFECTIVE_DATE'''#13 +
      '  INTO ed_field_nbr;'#13 +
      ''#13 +
      '  SELECT f.nbr FROM ev_field f'#13 +
      '  WHERE'#13 +
      '    f.ev_table_nbr = :table_nbr AND'#13 +
      '    f.name = ''EFFECTIVE_UNTIL'''#13 +
      '  INTO eu_field_nbr;'#13 +
      ''#13 +
      '  FOR SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
      '             t.record_id, t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
      '      FROM ev_transaction tr, ev_table_change t, ev_field_change f'#13 +
      '      WHERE'#13 +
      '        tr.nbr = t.ev_transaction_nbr AND'#13 +
      '        t.nbr = f.ev_table_change_nbr AND'#13 +
      '        f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr) AND'#13 +
      '        tr.commit_time >= :date_b AND'#13 +
      '        t.ev_table_nbr = :table_nbr AND'#13 +
      '        t.change_type = ''U'''#13 +
      '      UNION'#13 +
      '      SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,'#13 +
      '             t.record_id, t.record_nbr, f.ev_field_nbr, f.old_value'#13 +
      '      FROM ev_transaction tr,'#13 +
      '           ev_table_change t LEFT JOIN ev_field_change f'#13 +
      '             ON t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr)'#13 +
      '      WHERE'#13 +
      '        tr.nbr = t.ev_transaction_nbr AND'#13 +
      '        tr.commit_time >= :date_b AND'#13 +
      '        t.ev_table_nbr = :table_nbr AND'#13 +
      '        t.change_type IN (''I'', ''D'')'#13 +
      '      INTO commit_nbr, table_change_nbr, change_time, user_id, change_type,'#13 +
      '           rec_version, rec_nbr, curr_field_nbr, old_value'#13 +
      '  DO'#13 +
      '  BEGIN'#13 +
      '    IF ((curr_field_nbr IS NULL) OR (curr_field_nbr IN (field_nbr, ed_field_nbr, eu_field_nbr))) THEN'#13 +
      '    BEGIN'#13 +
      '      IF (curr_field_nbr = ed_field_nbr) THEN'#13 +
      '        field_kind = ''E'';'#13 +
      '      ELSE IF (curr_field_nbr = eu_field_nbr) THEN'#13 +
      '        field_kind = ''U'';'#13 +
      '      ELSE IF (curr_field_nbr = field_nbr) THEN'#13 +
      '        field_kind = ''F'';'#13 +
      '      ELSE'#13 +
      '        field_kind = NULL;'#13 +
      '    '#13 +
      '      SUSPEND;'#13 +
      '    END'#13 +
      '  END'#13 +
      'END';

//  VersionFieldAsOfByNbrExt =
    VersionFieldChange =
      '/*$1*/'#13 +
'EXECUTE BLOCK   '#13 +
'RETURNS (table_name VARCHAR(32), field_name VARCHAR(32), field_nbr INTEGER,  commit_nbr INTEGER, table_change_nbr INTEGER, rec_nbr INTEGER, change_time TIMESTAMP,'#13 +
'	   user_id INTEGER, rec_version INTEGER,  change_type CHAR(1),   '#13 +
'	   field_kind CHAR(1), old_value VARCHAR(512))'#13 +
'AS   '#13 +
'DECLARE VARIABLE table_nbr INTEGER;   '#13 +
'DECLARE VARIABLE ed_field_nbr INTEGER;   '#13 +
'DECLARE VARIABLE eu_field_nbr INTEGER;   '#13 +
'DECLARE VARIABLE curr_field_nbr INTEGER;'#13 +
'DECLARE VARIABLE field_type CHAR(1);   '#13 +
'DECLARE VARIABLE date_b TIMESTAMP;'#13 +
'DECLARE variable sfield_list varchar(512);'#13 +
'DECLARE VARIABLE sfield varchar(512);'#13 +
'BEGIN'#13 +
''#13 +
'	   table_name = ''$2'';'#13 +
'	   field_name = ''$3'';'#13 +
'  	   date_b = ''$4'';'#13 +
'	 '#13 +
'	SELECT t.nbr, f.nbr, f.field_type, t.Name, f.Name'#13 +
'	FROM ev_table t, ev_field f   '#13 +
'	WHERE   '#13 +
'	  f.ev_table_nbr = t.nbr AND   '#13 +
'		  t.name = :table_name AND'#13 +
'		  f.name = :field_name'#13 +
'	INTO table_nbr, field_nbr, field_type, table_name, field_name;'#13 +
'	 '#13 +
'	SELECT f.nbr FROM ev_field f   '#13 +
'	WHERE   '#13 +
'	  f.ev_table_nbr = :table_nbr AND   '#13 +
'	  f.name = ''EFFECTIVE_DATE''   '#13 +
'	INTO ed_field_nbr;   '#13 +
'	 '#13 +
'	SELECT f.nbr FROM ev_field f   '#13 +
'	WHERE   '#13 +
'	  f.ev_table_nbr = :table_nbr AND   '#13 +
'	  f.name = ''EFFECTIVE_UNTIL''   '#13 +
'	INTO eu_field_nbr;   '#13 +
'	 '#13 +
'	FOR SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,   '#13 +
'			   t.record_id, t.record_nbr,'#13 +
'			   f.ev_field_nbr ,'#13 +
'			   f.old_value,'#13 +
'				case f.ev_field_nbr when :ed_field_nbr then ''E'' when :eu_field_nbr then ''U'' when :field_nbr then ''F'' else null end'#13 +
'		FROM ev_transaction tr, ev_table_change t, ev_field_change f   '#13 +
'		WHERE   '#13 +
'		  tr.nbr = t.ev_transaction_nbr AND   '#13 +
'		  t.nbr = f.ev_table_change_nbr AND   '#13 +
'		  f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr) AND   '#13 +
'		  tr.commit_time >= :date_b AND   '#13 +
'		  t.ev_table_nbr = :table_nbr AND   '#13 +
'		  t.change_type = ''U''   '#13 +
'		UNION   '#13 +
'		SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,   '#13 +
'			   t.record_id, t.record_nbr,'#13 +
'			   ev_field_nbr,'#13 +
'			   f.old_value,'#13 +
'			   case f.ev_field_nbr when :ed_field_nbr then ''E'' when :eu_field_nbr then ''U'' when :field_nbr then ''F'' else null end  '#13 +
'		FROM ev_transaction tr,   '#13 +
'			 ev_table_change t LEFT JOIN ev_field_change f   '#13 +
'			   ON t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr)   '#13 +
'		WHERE   '#13 +
'		  tr.nbr = t.ev_transaction_nbr AND   '#13 +
'		  tr.commit_time >= :date_b AND   '#13 +
'		  t.ev_table_nbr = :table_nbr AND   '#13 +
'		  t.change_type IN (''I'', ''D'')   '#13 +
'		INTO commit_nbr, table_change_nbr, change_time, user_id, change_type,   '#13 +
'			 rec_version, rec_nbr, curr_field_nbr, old_value,'#13 +
'			 field_kind'#13 +
'	DO   '#13 +
'		SUSPEND;   '#13 +
'END;'#13
      ;

//  VersionFieldAsOfByNbrExt = // added n_start and n_finish, to split output into chunks
    VersionFieldChangeX =
      '/*$1*/'#13 +
'EXECUTE BLOCK   '#13 +
'RETURNS (table_name VARCHAR(32), field_name VARCHAR(32), field_nbr INTEGER,  commit_nbr INTEGER, table_change_nbr INTEGER, rec_nbr INTEGER, change_time TIMESTAMP,'#13 +
'	   user_id INTEGER, rec_version INTEGER,  change_type CHAR(1),   '#13 +
'	   field_kind CHAR(1), old_value VARCHAR(512))'#13 +
'AS   '#13 +
'DECLARE VARIABLE table_nbr INTEGER;   '#13 +
'DECLARE VARIABLE ed_field_nbr INTEGER;   '#13 +
'DECLARE VARIABLE eu_field_nbr INTEGER;   '#13 +
'DECLARE VARIABLE curr_field_nbr INTEGER;'#13 +
'DECLARE VARIABLE field_type CHAR(1);   '#13 +
'DECLARE VARIABLE date_b TIMESTAMP;'#13 +
'DECLARE variable sfield_list varchar(512);'#13 +
'DECLARE VARIABLE sfield varchar(512);'#13 +
'DECLARE VARIABLE n_start INTEGER;'#13 +
'DECLARE VARIABLE n_finish INTEGER;'#13 +
'BEGIN'#13 +
''#13 +
'	   table_name = ''$2'';'#13 +
'	   field_name = ''$3'';'#13 +
'  	   date_b = ''$4'';'#13 +
'  	   n_start = ''$5'';'#13 +
'  	   n_finish = ''$6'';'#13 +
'	 '#13 +
'	SELECT t.nbr, f.nbr, f.field_type, t.Name, f.Name'#13 +
'	FROM ev_table t, ev_field f   '#13 +
'	WHERE   '#13 +
'	  f.ev_table_nbr = t.nbr AND   '#13 +
'		  t.name = :table_name AND'#13 +
'		  f.name = :field_name'#13 +
'	INTO table_nbr, field_nbr, field_type, table_name, field_name;'#13 +
'	 '#13 +
'	SELECT f.nbr FROM ev_field f   '#13 +
'	WHERE   '#13 +
'	  f.ev_table_nbr = :table_nbr AND   '#13 +
'	  f.name = ''EFFECTIVE_DATE''   '#13 +
'	INTO ed_field_nbr;   '#13 +
'	 '#13 +
'	SELECT f.nbr FROM ev_field f   '#13 +
'	WHERE   '#13 +
'	  f.ev_table_nbr = :table_nbr AND   '#13 +
'	  f.name = ''EFFECTIVE_UNTIL''   '#13 +
'	INTO eu_field_nbr;   '#13 +
'	 '#13 +
'	FOR SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,   '#13 +
'			   t.record_id, t.record_nbr,'#13 +
'			   f.ev_field_nbr ,'#13 +
'			   f.old_value,'#13 +
'				case f.ev_field_nbr when :ed_field_nbr then ''E'' when :eu_field_nbr then ''U'' when :field_nbr then ''F'' else null end'#13 +
'		FROM ev_transaction tr, ev_table_change t, ev_field_change f   '#13 +
'		WHERE   '#13 +
'		  tr.nbr = t.ev_transaction_nbr AND   '#13 +
'		  t.nbr = f.ev_table_change_nbr AND   '#13 +
'		  f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr) AND '#13 +
'		  tr.commit_time >= :date_b AND   '#13 +
'		  t.ev_table_nbr = :table_nbr AND   '#13 +
'		  t.change_type = ''U''   '#13 +
'		  AND t.record_nbr >= :n_start AND t.record_nbr <= :n_finish '#13 +
'		UNION   '#13 +
'		SELECT tr.commit_nbr, t.nbr, tr.commit_time, tr.user_id, t.change_type,   '#13 +
'			   t.record_id, t.record_nbr,'#13 +
'			   ev_field_nbr,'#13 +
'			   f.old_value,'#13 +
'			   case f.ev_field_nbr when :ed_field_nbr then ''E'' when :eu_field_nbr then ''U'' when :field_nbr then ''F'' else null end  '#13 +
'		FROM ev_transaction tr,   '#13 +
'			 ev_table_change t LEFT JOIN ev_field_change f   '#13 +
'			   ON t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN (:field_nbr, :ed_field_nbr, :eu_field_nbr)   '#13 +
'		WHERE   '#13 +
'		  tr.nbr = t.ev_transaction_nbr AND   '#13 +
'		  tr.commit_time >= :date_b AND   '#13 +
'		  t.ev_table_nbr = :table_nbr AND   '#13 +
'		  t.change_type IN (''I'', ''D'')   '#13 +
'		  AND t.record_nbr >= :n_start AND t.record_nbr <= :n_finish '#13 +
'		INTO commit_nbr, table_change_nbr, change_time, user_id, change_type,   '#13 +
'			 rec_version, rec_nbr, curr_field_nbr, old_value,'#13 +
'			 field_kind'#13 +
'	DO   '#13 +
'		SUSPEND;   '#13 +
'END;'#13
      ;

    EvFieldNbrDirect =
      '/*$1*/'#13 +
   'SELECT a.nbr FROM ev_field a INNER JOIN ev_table b ON a.ev_table_nbr=b.nbr WHERE a.name=''$2'' and b.name=''$3''';

    EvAuditListOfNumbers =
      '/*$1*/'#13 +
// field_nbr, EffDateFieldNbr, EffUntilFieldNbr, table_nbr, dat_b
   'SELECT record_nbr, COUNT(nbr) NN from ( '#13 +
   '    SELECT t.record_nbr, t.nbr '#13 +
	 '	  FROM ev_transaction tr, ev_table_change t, ev_field_change f '#13 +
	 '	WHERE '#13 +
	 '	  tr.nbr = t.ev_transaction_nbr AND '#13 +
	 '	  t.nbr = f.ev_table_change_nbr AND '#13 +
	 '	  f.ev_field_nbr IN ($2, $3, $4) AND '#13 +
	 '	  tr.commit_time >= ''$6'' AND '#13 +
	 '	  t.ev_table_nbr = $5 AND '#13 +
	 '	  t.change_type = ''U'' '#13 +
	 '	UNION  '#13 +
	 '	SELECT t.record_nbr, t.nbr '#13 +
	 '	FROM ev_transaction tr, '#13 +
	 '		 ev_table_change t LEFT JOIN ev_field_change f '#13 +
	 '		   ON t.nbr = f.ev_table_change_nbr AND f.ev_field_nbr IN ($2, $3, $4) '#13 +
	 '	WHERE '#13 +
	 '	  tr.nbr = t.ev_transaction_nbr AND '#13 +
	 '	  tr.commit_time >= ''$6'' AND '#13 +
	 '	  t.ev_table_nbr = $5 AND '#13 +
	 '	  t.change_type IN (''I'', ''D'') '#13 +
   '                 ) aa '#13 +
   ' GROUP BY record_nbr '#13 +
   ' ORDER BY record_nbr '#13
   ;


    SbTaskRuns =
      'SELECT tsk.sb_task_nbr, tsk.last_run run_date'#13 +
      'FROM sb_task tsk'#13 +
      'WHERE'#13 +
      '  tsk.last_run IS NOT NULL'#13 +
      ''#13 +
      'UNION ALL'#13 +
      ''#13 +
      'SELECT tsk.sb_task_nbr, CAST(fc.old_value as TIMESTAMP) run_date'#13 +
      'FROM ev_transaction tr, ev_table_change tc, ev_field_change fc, ev_table tbl, ev_field fld, sb_task tsk'#13 +
      'WHERE'#13 +
      '  tc.ev_transaction_nbr = tr.nbr AND'#13 +
      '  fc.ev_table_change_nbr = tc.nbr AND'#13 +
      '  tc.ev_table_nbr = tbl.nbr AND'#13 +
      '  fc.ev_field_nbr = fld.nbr AND'#13 +
      '  fld.ev_table_nbr = tbl.nbr AND'#13 +
      '  tbl.name = ''SB_TASK'' AND'#13 +
      '  fld.name = ''LAST_RUN'' AND'#13 +
      '  tc.record_nbr = tsk.sb_task_nbr AND'#13 +
      '  tc.change_type = ''U'' AND'#13 +
      '  tr.commit_time >= :date_b AND'#13 +
      '  fc.old_value IS NOT NULL';


    SetTransactionVar =
      '/*$1*/'#13 +
      'EXECUTE BLOCK'#13 +
      'AS'#13 +
      'BEGIN'#13 +
      '  rdb$set_context(''USER_TRANSACTION'', ''$2'', ''$3'');'#13 +
      'END';

    ClearTransactionVar =
      '/*$1*/'#13 +
      'EXECUTE BLOCK'#13 +
      'AS'#13 +
      'BEGIN'#13 +
      '  rdb$set_context(''USER_TRANSACTION'', ''$2'', NULL);'#13 +
      'END';

    SelectSomeRecordsNbrForBilling =
      '/*$1*/'#13+
      ' SELECT tc.record_nbr, $3(tr.commit_time) commit_time'#13 +
      ' FROM ev_table t'#13 +
      ' JOIN ev_table_change tc ON tc.ev_table_nbr=t.nbr AND tc.change_type=''$4'''#13 +
      ' JOIN ev_transaction tr ON tc.ev_transaction_nbr=tr.nbr'#13 +
      ' WHERE t.name=''$2'''#13 +
      ' GROUP BY tc.record_nbr'#13 +
      ' HAVING $3(tr.commit_time)>:time_b';

    SelectDeletedRecordsNbrForBilling = 
      '{SelectSomeRecordsNbrForBilling<$1,$2,MAX,D>}';

    SelectInsertedRecordsNbrForBilling =
      '{SelectSomeRecordsNbrForBilling<$1,$2,MIN,I>}';

    SelectTableChangesForBilling =
      '/*$1*/'#13+
      'SELECT tc.record_nbr, tr.nbr, COUNT(*) cnt'#13 +
      'FROM ev_table t'#13 +
      'JOIN ev_table_change tc ON tc.ev_table_nbr=t.nbr'#13 +
      'JOIN ev_transaction tr ON tc.ev_transaction_nbr=tr.nbr AND tr.commit_time>:time_b'#13 +
      'WHERE t.name=''$2'''#13 +
      'GROUP BY tc.record_nbr, tr.nbr'#13;

    SelectFieldChangesForBilling =
      '/*$1*/'#13+
      'SELECT f.name, tc.change_type, fc.old_value'#13 +
      'FROM ev_table_change tc'#13 +
      'LEFT JOIN ev_field_change fc ON fc.ev_table_change_nbr=tc.NBR'#13 +
      'LEFT JOIN ev_field f ON f.nbr=fc.ev_field_nbr'#13 +
      'WHERE tc.record_nbr=$2 and tc.ev_transaction_nbr=$3';

  InitialCreationNBRAudit =
      '/*$1*/'#13+
      'EXECUTE BLOCK'#13 +
      'RETURNS (table_name VARCHAR(64), nbr INTEGER,  creation_date TIMESTAMP, user_id INTEGER)'#13 +
      'AS'#13 +
      'DECLARE VARIABLE tr_nbr INTEGER;'#13 +
      'BEGIN'#13 +
      '  FOR SELECT tbl.name, tc.record_nbr, MIN(tc.ev_transaction_nbr)'#13 +
      '      FROM ev_table_change tc, ev_table tbl'#13 +
      '      WHERE'#13 +
      '        tbl.nbr = tc.ev_table_nbr AND'#13 +
      '        tc.change_type = ''I'' AND'#13 +
      '        tbl.name IN ($2)'#13 +
      '      GROUP BY tbl.name, tc.record_nbr'#13 +
      '      INTO table_name, nbr, tr_nbr'#13 +
      '  DO'#13 +
      '  BEGIN'#13 +
      '    SELECT tr.commit_time, tr.user_id'#13 +
      '    FROM ev_transaction tr'#13 +
      '    WHERE tr.nbr = :tr_nbr'#13 +
      '    INTO creation_date, user_id;'#13 +
      ''#13 +
      '    SUSPEND;'#13 +
      '  END'#13 +
      'END';


  RemapKey =
    '/*$1*/'#13 +
    'EXECUTE BLOCK'#13 +
    'returns (updated_rec_count INTEGER)'#13 +
    'AS'#13 +
    'DECLARE VARIABLE tbl VARCHAR(32);'#13 +
    'DECLARE VARIABLE fld VARCHAR(32);'#13 +
    'DECLARE VARIABLE tbl_nbr INTEGER;'#13 +
    'DECLARE VARIABLE fld_nbr INTEGER;'#13 +
    'DECLARE VARIABLE old_nbr INTEGER;'#13 +
    'DECLARE VARIABLE new_nbr INTEGER;'#13 +
    'BEGIN'#13 +
    '  tbl = ''$2'';'#13 +
    '  fld = ''$3'';'#13 +
    '  old_nbr = $4;'#13 +
    '  new_nbr = $5;'#13 +
    ''#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''MAINTENANCE'', 1);'#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''INT_CHANGE'', 1);'#13 +
    ''#13 +
    '  UPDATE $2 SET $3 = :new_nbr WHERE $3 = :old_nbr;'#13 +
    '  updated_rec_count = row_count;'#13 +
    ''#13 +
    '  SELECT nbr FROM ev_table t where t.name = :tbl into tbl_nbr;'#13 +
    '  SELECT nbr FROM ev_field t where t.name = :fld and t.ev_table_nbr = :tbl_nbr into fld_nbr;'#13 +
    ''#13 +
    '  if (fld = tbl || ''_NBR'') then'#13 +
    '    UPDATE ev_table_change SET record_nbr = :new_nbr WHERE ev_table_nbr = :tbl_nbr AND record_nbr = :old_nbr;'#13 +
    '  ELSE'#13 +
    '    UPDATE ev_field_change SET old_value = CAST(:new_nbr AS VARCHAR(32)) WHERE ev_field_nbr = :fld_nbr AND old_value = CAST(:old_nbr AS VARCHAR(32));'#13 +
    ''#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''MAINTENANCE'', NULL);'#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''INT_CHANGE'', NULL);'#13 +
    ''#13 +
    '  SUSPEND;'#13 +
    'END';


{ TevDBQueryLibrary }

procedure TevDBQueryLibrary.DoOnConstruction;
begin
  inherited;
  FList := TStringList.Create;
  FList.CaseSensitive := False;
  FList.Duplicates := dupError;
  Initialize;
  FList.Sorted := True;
end;

destructor TevDBQueryLibrary.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

function TevDBQueryLibrary.GetQueryText(const AQueryName: String): TevLibQueryInfo;
var
  i: Integer;
begin
  if (Length(AQueryName) <= FNameMaxLength) and (AQueryName[1] in ['A'..'Z', 'a'..'z']) then
    i := FList.IndexOf(AQueryName)
  else
    i := -1;

  if i <> - 1 then
    Result := PTevLibQueryInfo(FList.Objects[i])^
  else
    Result.SQL := '';
end;

procedure TevDBQueryLibrary.Initialize;
begin
  Clear;
  FList.Capacity := 1000;
  // Functions below should be relatively short otherwise compiler shows an error
  Initialize0;
  Initialize1;
  Initialize2;
  Initialize3;
  Initialize4;
  FList.Capacity := FList.Count;
end;

procedure TevDBQueryLibrary.RegisterQuery(const AQueryName, AQuery: String);
var
  pS: PTevLibQueryInfo;
begin
  New(pS);
  pS^.SQL := ProcessTemplates(AQuery);
  FList.AddObject(AQueryName, Pointer(pS));

  if FNameMaxLength < Length(AQueryName) then
    FNameMaxLength := Length(AQueryName);
end;

procedure TevDBQueryLibrary.Initialize1;
begin
  RegisterQuery('CurrentSysTime', CurrentSysTime);
  RegisterQuery('Now', CurrentSysTime);
  RegisterQuery('GenericSelect', GenericSelect);
  RegisterQuery('GenericNBRCnd', GenericNBRCnd);
  RegisterQuery('PayrollProcessedClause', PayrollProcessedClause);
  RegisterQuery('GenericOrder', GenericOrder);
  RegisterQuery('GenericSelectCurrent', GenericSelectCurrent);
  RegisterQuery('GenericSelectWithCondition', GenericSelectWithCondition);
  RegisterQuery('GenericSelectCurrentWithCondition', GenericSelectCurrentWithCondition);
  RegisterQuery('GenericSelectCurrentWithConditionGrouped', GenericSelectCurrentWithConditionGrouped);
  RegisterQuery('GenericSelectOrdered', GenericSelectOrdered);
  RegisterQuery('GenericSelectCurrentNBR', GenericSelectCurrentNBR);
  RegisterQuery('GenericSelectCurrentOrdered', GenericSelectCurrentOrdered);
  RegisterQuery('GenericSelectCurrentNBROrdered', GenericSelectCurrentNBROrdered);
  RegisterQuery('GenericSelectNBR', GenericSelectNBR);
  RegisterQuery('GenericSelectNBROrdered', GenericSelectNBROrdered);
  RegisterQuery('GenericSelectCurrentNBRWithCondition', GenericSelectCurrentNBRWithCondition);
  RegisterQuery('GenericSelectCurrentNBRWithConditionGrouped', GenericSelectCurrentNBRWithConditionGrouped);
  RegisterQuery('GenericInsert', GenericInsert);
  RegisterQuery('GenericUpdate', GenericUpdate);
  RegisterQuery('GenericUpdateWithCondition', GenericUpdateWithCondition);
  RegisterQuery('GenericUpdateNBR', GenericUpdateNBR);
  RegisterQuery('GenericSecondKey', GenericSecondKey);
  RegisterQuery('GenericUpdateWithTwoKeys', GenericUpdateWithTwoKeys);
  RegisterQuery('SelectHist', SelectHist);
  RegisterQuery('SelectHistWithCondition', SelectHistWithCondition);
  RegisterQuery('SelectHistNBR', SelectHistNBR);
  RegisterQuery('GenericSelect2', GenericSelect2);
  RegisterQuery('GenericSelect3', GenericSelect3);
  RegisterQuery('GenericSelect3Current', GenericSelect3Current);
  RegisterQuery('GenericSelect3CurrentWithCondition', GenericSelect3CurrentWithCondition);
  RegisterQuery('GenericSelect2WithCondition', GenericSelect2WithCondition);
  RegisterQuery('GenericSelect2DiffNbrWithCondition', GenericSelect2DiffNbrWithCondition);
  RegisterQuery('GenericSelect2Current', GenericSelect2Current);
  RegisterQuery('GenericSelect2CurrentNbr', GenericSelect2CurrentNbr);
  RegisterQuery('GenericSelect2CurrentGrouped', GenericSelect2CurrentGrouped);
  RegisterQuery('GenericSelect2CurrentWithCondition', GenericSelect2CurrentWithCondition);
  RegisterQuery('GenericSelect2CurrentNbrWithCondition', GenericSelect2CurrentNbrWithCondition);
  RegisterQuery('GenericSelect2CurrentWithConditionGrouped', GenericSelect2CurrentWithConditionGrouped);
  RegisterQuery('GenericSelect2CurrentNBRWithConditionGrouped', GenericSelect2CurrentNBRWithConditionGrouped);
  RegisterQuery('GenericSelect2HistNbr', GenericSelect2HistNbr);
  RegisterQuery('GenericSelect2HistNbrWithCondition', GenericSelect2HistNbrWithCondition);
  RegisterQuery('SelectEmployeesToAttach', SelectEmployeesToAttach);
  RegisterQuery('SelectLastScheduledFrequencyCheckDate', SelectLastScheduledFrequencyCheckDate);
  RegisterQuery('SelectClientsCompanies', SelectClientsCompanies);

  RegisterQuery('ExecProc', ExecProc);
  RegisterQuery('OpenProc', OpenProc);
  RegisterQuery('GetNextPeriodDate', GetNextPeriodDate);
  RegisterQuery('CountBiWeeklyBatchs', CountBiWeeklyBatchs);
  RegisterQuery('EmployeeLastEDCheckDate', EmployeeLastEDCheckDate);
  RegisterQuery('AllEmployeeLastEDCheckDates', AllEmployeeLastEDCheckDates);
  RegisterQuery('SelectAllPayrollsAfterDate', SelectAllPayrollsAfterDate);
  RegisterQuery('PensionDeductions', PensionDeductions);
  RegisterQuery('MaxRunNumberForCheckDate', MaxRunNumberForCheckDate);
  RegisterQuery('NextRunNumberForCheckDate', NextRunNumberForCheckDate);
  RegisterQuery('NextRunNumberForCheckDatePrNbr', NextRunNumberForCheckDatePrNbr);
  RegisterQuery('SumTaxLiabilities', SumTaxLiabilities);
  RegisterQuery('MaxCheckDateForCo', MaxCheckDateForCo);
  RegisterQuery('PrNbrForMaxCheckDateForCo', PrNbrForMaxCheckDateForCo);
  RegisterQuery('SelectDistinctEDForPR', SelectDistinctEDForPR);
  RegisterQuery('SelectDistinctLocalsForPR', SelectDistinctLocalsForPR);
  RegisterQuery('CheckFederalShortfalls', CheckFederalShortfalls);
  RegisterQuery('CheckStatesShortfalls', CheckStatesShortfalls);
  RegisterQuery('CheckLocalsShortfalls', CheckLocalsShortfalls);
  RegisterQuery('CheckDeductionShortfalls', CheckDeductionShortfalls);
  RegisterQuery('FedTemplate', FedTemplate);
  RegisterQuery('GlobalFedTemplate', GlobalFedTemplate);
  RegisterQuery('SdiTemplate', SdiTemplate);
  RegisterQuery('SuiTemplate', SuiTemplate);
  RegisterQuery('DeductionTemplate', DeductionTemplate);
  RegisterQuery('GlobalDeductionTemplate', GlobalDeductionTemplate);
  RegisterQuery('SelectLastDDCheckDate', SelectLastDDCheckDate);
  RegisterQuery('SelectCheckWithEE', SelectCheckWithEE);
  RegisterQuery('SelectDBDT', SelectDBDT);
  RegisterQuery('TC_SelectDBDT', TC_SelectDBDT);
  RegisterQuery('SelectDBDTWithName', SelectDBDTWithName);
  RegisterQuery('SelectSortedPayrollByCheckDateRunNumber', SelectSortedPayrollByCheckDateRunNumber);
  RegisterQuery('SelectSortedChecksByCheckDateRunNumber', SelectSortedChecksByCheckDateRunNumber);
  RegisterQuery('SelectSortedTableByCheckDateRunNumber', SelectSortedTableByCheckDateRunNumber);
  RegisterQuery('SelectCheckList', SelectCheckList);
  RegisterQuery('SelectMiscCheckList', SelectMiscCheckList);
  RegisterQuery('SelectSecurityRights', SelectSecurityRights);
  RegisterQuery('RemoveSecurityRights', RemoveSecurityRights);
  RegisterQuery('GenericSelectCurrentWithKey', GenericSelectCurrentWithKey);
  RegisterQuery('DirDep', DirDep);
  RegisterQuery('AgencyDirDep', AgencyDirDep);
  RegisterQuery('BillingForACH', BillingForACH);
  RegisterQuery('GetACHData', GetACHData);
  RegisterQuery('InCondition', InCondition);
end;

procedure TevDBQueryLibrary.Initialize2;
begin
  RegisterQuery('TmpLiabilitiesWithLocals', TmpLiabilitiesWithLocals);
  RegisterQuery('TmpLiabilitiesWithSUI', TmpLiabilitiesWithSUI);
  RegisterQuery('TmpLiabilitiesWithStates', TmpLiabilitiesWithStates);
  RegisterQuery('TmpLiabilities', TmpLiabilities);
  RegisterQuery('AgencyCheckDetails', AgencyCheckDetails);
  RegisterQuery('ChildSupportCases', ChildSupportCases);
  RegisterQuery('GarnishmentIDs', GarnishmentIDs);
  RegisterQuery('MaxCheckDatesForEE', MaxCheckDatesForEE);
  RegisterQuery('DistinctCheckDatePr', DistinctCheckDatePr);
  RegisterQuery('ListEDWithShortfalls', ListEDWithShortfalls);
  RegisterQuery('ListFedTaxesWithShortfalls', ListFedTaxesWithShortfalls);
  RegisterQuery('FedTaxesTotals', FedTaxesTotals);
  RegisterQuery('StateTaxesTotals', StateTaxesTotals);
  RegisterQuery('SUIDistinctTaxesTotals', SUIDistinctTaxesTotals);
  RegisterQuery('LocalTaxesTotals', LocalTaxesTotals);
  RegisterQuery('ListOtherWithShortfalls', ListOtherWithShortfalls);
  RegisterQuery('ListAllStateWithShortfalls', ListAllStateWithShortfalls);
  RegisterQuery('ListAllLocalWithShortfalls', ListAllLocalWithShortfalls);
  RegisterQuery('AmountsWorked', AmountsWorked);
  RegisterQuery('SchedulePayrollCount', SchedulePayrollCount);
  RegisterQuery('BatchAdditionalInfo', BatchAdditionalInfo);
  RegisterQuery('CheckLinesWithDD', CheckLinesWithDD);
  RegisterQuery('EarningsDeductionsYTDData', EarningsDeductionsYTDData);
  RegisterQuery('FederalTaxesYTDData', FederalTaxesYTDData);
  RegisterQuery('FederalTaxesYTDDataYTD', FederalTaxesYTDDataYTD);
  RegisterQuery('FederalTaxesYTDData2', FederalTaxesYTDData2);
  RegisterQuery('LocalTaxesYTDData', LocalTaxesYTDData);
  RegisterQuery('LocalTaxesYTDDataYTD', LocalTaxesYTDDataYTD);
  RegisterQuery('SDIAndStatesTaxesYTDData', SDIAndStatesTaxesYTDData);
  RegisterQuery('SDIAndStatesTaxesYTDDataYTD', SDIAndStatesTaxesYTDDataYTD);
  RegisterQuery('SUITaxesYTDData', SUITaxesYTDData);
  RegisterQuery('ExportAR', ExportAR);
  RegisterQuery('GetPriorPRForEE', GetPriorPRForEE);
  RegisterQuery('FederalShortFall', FederalShortFall);
  RegisterQuery('StateShortfall', StateShortfall);
  RegisterQuery('LocalShortfall', LocalShortfall);
  RegisterQuery('EESDITaxes', EESDITaxes);
  RegisterQuery('StateTaxes', StateTaxes);
  RegisterQuery('EESUITaxes', EESUITaxes);
  RegisterQuery('DeductGarnishment', DeductGarnishment);
  RegisterQuery('PriorCheckDeductionsShortfalls', PriorCheckDeductionsShortfalls);
  RegisterQuery('AgencyChecks', AgencyChecks);
  RegisterQuery('TaxChecks', TaxChecks);
  RegisterQuery('Checks', Checks);
  RegisterQuery('Companies', Companies);
  RegisterQuery('SDIDiscrepancies', SDIDiscrepancies);
  RegisterQuery('SUIDiscrepancies', SUIDiscrepancies);
  RegisterQuery('ERFedDiscrepancies', ERFedDiscrepancies);
  RegisterQuery('ERSDIDiscrepancies', ERSDIDiscrepancies);
  RegisterQuery('ERSUIDiscrepancies', ERSUIDiscrepancies);
  RegisterQuery('SumAgencyChecks', SumAgencyChecks);
  RegisterQuery('CalcAgencyChecks', CalcAgencyChecks);
  RegisterQuery('CalcAgencyChecksForEE', CalcAgencyChecksForEE);
  RegisterQuery('CalcAgencyChecksForEETargetMet', CalcAgencyChecksForEETargetMet);
  RegisterQuery('CalcAgencyChecksByEE', CalcAgencyChecksByEE);
  RegisterQuery('CalcAgencyChecksByEETargetMet', CalcAgencyChecksByEETargetMet);
  RegisterQuery('CalcCheckLineForAgencyCheck', CalcCheckLineForAgencyCheck);
  RegisterQuery('CalcCheckLineForAgencyCheckTargetMet', CalcCheckLineForAgencyCheckTargetMet);
  RegisterQuery('SumAllTaxesForPr', SumAllTaxesForPr);
  RegisterQuery('Sum945TaxesForPr', Sum945TaxesForPr);
  RegisterQuery('ThirdPartyStateLiab', ThirdPartyStateLiab);
  RegisterQuery('ThirdPartyIndependent', ThirdPartyIndependent);
  RegisterQuery('ThirdPartySUILiab', ThirdPartySUILiab);
  RegisterQuery('ThirdPartyLocalLiab', ThirdPartyLocalLiab);
  RegisterQuery('SelectPayrollAccountRegister', SelectPayrollAccountRegister);
  RegisterQuery('CustomCompanySelect', CustomCompanySelect);
  RegisterQuery('AllPRCheckLines', AllPRCheckLines);
  RegisterQuery('GetPretaxCount', GetPretaxCount);
  RegisterQuery('GetCheckTimeOfAccruals', GetCheckTimeOfAccruals);
  RegisterQuery('GetUnpaidEFTPDebits', GetUnpaidEFTPDebits);
  RegisterQuery('ReturnsQueue', ReturnsQueue);
  RegisterQuery('QuarterFuiWages', QuarterFuiWages);
  RegisterQuery('QuarterSuiWages', QuarterSuiWages);
  RegisterQuery('LiabStatusedRight', LiabStatusedRight);
  RegisterQuery('Consolidations', Consolidations);
  RegisterQuery('ConsolidationsPreProcessStatus', ConsolidationsPreProcessStatus);
  RegisterQuery('ConsolidationsFull', ConsolidationsFull);
  RegisterQuery('ConsolidationQuarterSuiWages', ConsolidationQuarterSuiWages);
  RegisterQuery('ConsolidationQuarterLocalWages', ConsolidationQuarterLocalWages);
  RegisterQuery('ConsolidationQuarterSuiLiabs', ConsolidationQuarterSuiLiabs);
  RegisterQuery('ConsolidationQuarterLocalLiabs', ConsolidationQuarterLocalLiabs);
  RegisterQuery('TmpDepositSummary', TmpDepositSummary);
  RegisterQuery('BranchQuery', BranchQuery);
  RegisterQuery('DepartmentQuery', DepartmentQuery);
  RegisterQuery('TeamQuery', TeamQuery);
  RegisterQuery('CheckReturnQueueForProcessedReturns', CheckReturnQueueForProcessedReturns);
  RegisterQuery('GetAllChecksForState', GetAllChecksForState);
  RegisterQuery('GetUnprocessedPayrolls', GetUnprocessedPayrolls);
  RegisterQuery('GetConsolidatedCompanies', GetConsolidatedCompanies);
  RegisterQuery('GetEDCountCO', GetEDCountCO);
  RegisterQuery('AffectedPersonsTemplate', AffectedPersonsTemplate);
  RegisterQuery('GetPayrollForEDRefresh', GetPayrollForEDRefresh);
  RegisterQuery('CheckW2_1099Existence', CheckW2_1099Existence);
  RegisterQuery('GetInvoicesForCompany', GetInvoicesForCompany);
  RegisterQuery('GetInvoiceDetailsForCompany', GetInvoiceDetailsForCompany);
  RegisterQuery('BR_CheckLinesByEDType', BR_CheckLinesByEDType);
  RegisterQuery('BR_CheckWagesTaxes', BR_CheckWagesTaxes);
  RegisterQuery('BR_FedLiab', BR_FedLiab);
  RegisterQuery('BR_CheckStateInfo', BR_CheckStateInfo);
  RegisterQuery('BR_StateLiab', BR_StateLiab);
  RegisterQuery('GetAllChecksForSUIState', GetAllChecksForSUIState);
end;

procedure TevDBQueryLibrary.Initialize3;
begin
  RegisterQuery('BR_CheckStateLinesInfo', BR_CheckStateLinesInfo);
  RegisterQuery('BR_CheckSUIInfo', BR_CheckSUIInfo);
  RegisterQuery('BR_CheckSUILinesInfo', BR_CheckSUILinesInfo);
  RegisterQuery('BR_SUILiab', BR_SUILiab);
  RegisterQuery('BR_CheckLocalInfo', BR_CheckLocalInfo);
  RegisterQuery('BR_CheckLocalLinesInfo', BR_CheckLocalLinesInfo);
  RegisterQuery('BR_LocalLiab', BR_LocalLiab);
  RegisterQuery('AllPendingLiabsForCo', AllPendingLiabsForCo);
  RegisterQuery('SelectCheckLineLocals', SelectCheckLineLocals);
  RegisterQuery('SelectPayrollsForAgencyCheck', SelectPayrollsForAgencyCheck);
  RegisterQuery('DistrEDEForACH', DistrEDEForACH);
  RegisterQuery('DistrEDDForACH', DistrEDDForACH);
  RegisterQuery('DistrEDCOUNTForACH', DistrEDCOUNTForACH);
  RegisterQuery('DistrDeductionsForACH', DistrDeductionsForACH);
  RegisterQuery('DistrFederalTaxesForACH', DistrFederalTaxesForACH);
  RegisterQuery('DistrStateTaxesForACH', DistrStateTaxesForACH);
  RegisterQuery('DistrLocalTaxesForACH', DistrLocalTaxesForACH);
  RegisterQuery('DistrSUITaxesForACH', DistrSUITaxesForACH);
  RegisterQuery('DistrERSUIOtherTaxesForACH', DistrERSUIOtherTaxesForACH);
  RegisterQuery('DistrEESUIOtherTaxesForACH', DistrEESUIOtherTaxesForACH);
  RegisterQuery('ConsolidationQuarterMoLiabs', ConsolidationQuarterMoLiabs);
  RegisterQuery('ConsolidationQuarterSuiCreditLiabs', ConsolidationQuarterSuiCreditLiabs);
  RegisterQuery('SelectLastChangeNBR', SelectLastChangeNBR);
  RegisterQuery('TotalSUITaxableWages', TotalSUITaxableWages);
  RegisterQuery('TotalSUIEmployees', TotalSUIEmployees);
  RegisterQuery('TotalSUIEmployeesByPeriod', TotalSUIEmployeesByPeriod);
  RegisterQuery('CheckServiceInBilling', CheckServiceInBilling);
  RegisterQuery('SelectClPersonChanges', SelectClPersonChanges);
  RegisterQuery('SelectHistoryOfChanges', SelectHistoryOfChanges);
  RegisterQuery('SelectHistoryOfChangesForFields', SelectHistoryOfChangesForFields);
  RegisterQuery('EEListWC', EEListWC);
  RegisterQuery('FluctuatingListWC', FluctuatingListWC);
  RegisterQuery('RatesWC', RatesWC);
  RegisterQuery('TimeOffBalances', TimeOffBalances);
  RegisterQuery('TimeOffPrBalances', TimeOffPrBalances);
  RegisterQuery('DatedTimeOffBalances', DatedTimeOffBalances);
  RegisterQuery('BetweeenDateTimeOffBalances', BetweeenDateTimeOffBalances);
  RegisterQuery('TimeOffOperations', TimeOffOperations);
  RegisterQuery('CollapsedTimeOffOperations', CollapsedTimeOffOperations);
  RegisterQuery('PayrollTimeOffEdHours', PayrollTimeOffEdHours);
  RegisterQuery('SelectDepositRecordsForReconByFED', SelectDepositRecordsForReconByFED);
  RegisterQuery('SelectDepositRecordsForReconByFUI', SelectDepositRecordsForReconByFUI);
  RegisterQuery('SelectDepositRecordsForReconByState', SelectDepositRecordsForReconByState);
  RegisterQuery('VmrEeSettings', VmrEeSettings);
  RegisterQuery('VmrCoSettings', VmrCoSettings);
  RegisterQuery('VmrSbTopMailBoxes', VmrSbTopMailBoxes);
  RegisterQuery('VmrSbMailBoxes', VmrSbMailBoxes);
  RegisterQuery('VmrSbMailBoxContents', VmrSbMailBoxContents);
  RegisterQuery('VmrSbJobs', VmrSbJobs);
  RegisterQuery('VmrRemotePrintJobs', VmrRemotePrintJobs);
  RegisterQuery('VmrRemotePrintContent', VmrRemotePrintContent);
  RegisterQuery('CountThisYearChecksForEE', CountThisYearChecksForEE);
  RegisterQuery('GrossWagesYTD', GrossWagesYTD);
  RegisterQuery('SelectDBDTG', SelectDBDTG);
  RegisterQuery('SelectEe', SelectEe);
  RegisterQuery('MiscChecksForACH', MiscChecksForACH);
  RegisterQuery('GetTotalForEDType', GetTotalForEDType);
  RegisterQuery('SelectCheckDatesFromPr', SelectCheckDatesFromPr);
  RegisterQuery('SelectCheckNumbers', SelectCheckNumbers);
  RegisterQuery('SelectPrRelatedEeTimeOpers', SelectPrRelatedEeTimeOpers);
  RegisterQuery('CountWebUsers', CountWebUsers);
  RegisterQuery('NewPrenotes', NewPrenotes);
  RegisterQuery('SecurityUserGroups', SecurityUserGroups);
  RegisterQuery('SecurityUserRights', SecurityUserRights);
  RegisterQuery('SecurityGroupRights', SecurityGroupRights);
  RegisterQuery('SecurityUserRecords', SecurityUserRecords);
  RegisterQuery('SecurityGroupRecords', SecurityGroupRecords);
  RegisterQuery('SecurityStructure', SecurityStructure);
  RegisterQuery('SecurityFieldsStructure', SecurityFieldsStructure);
  RegisterQuery('SecurityGroupClients', SecurityGroupClients);
  RegisterQuery('SecurityUserClients', SecurityUserClients);
  RegisterQuery('AllClientNumbers', AllClientNumbers);
  RegisterQuery('COPY_PAYROLL_BATCHES', ProcCOPY_PAYROLL_BATCHES);
  RegisterQuery('LOCK_PR', ProcLOCK_PR);
  RegisterQuery('UNLOCK_PR', ProcUNLOCK_PR);
  RegisterQuery('CL_NBR', ProcCL_NBR);
end;

procedure TevDBQueryLibrary.Initialize4;
begin
  RegisterQuery('CL_COPY', ProcCL_COPY);
  RegisterQuery('EE_YTD', ProcEE_YTD);
  RegisterQuery('SelectDIVWithName', SelectDIVWithName);
  RegisterQuery('SelectDBWithName', SelectDBWithName);
  RegisterQuery('SelectDBDWithName', SelectDBDWithName);
  RegisterQuery('CountMissedOPTPayrolls', CountMissedOPTPayrolls);
  RegisterQuery('VmrSbTopMailBoxesWithVouchers',VmrSbTopMailBoxesWithVouchers);
  RegisterQuery('TimeOffEeOperations', TimeOffEeOperations);
  RegisterQuery('List_Reprint_History_Detail', List_Reprint_History_Detail);
  RegisterQuery('TaxDepositAmountList', TaxDepositAmountList);
  RegisterQuery('SelectEmployeesWithDBDT', SelectEmployeesWithDBDT);
  RegisterQuery('SelectCOGroupMangerName', SelectCOGroupMangerName);
  RegisterQuery('DistrEarningsForACH', DistrEarningsForACH);
  RegisterQuery('TimeOffBalanceCheck', TimeOffBalanceCheck);
  RegisterQuery('CheckIfQecExists', CheckIfQecExists);
  RegisterQuery('GetConsolidatedCompanies1', GetConsolidatedCompanies1);
  RegisterQuery('CountW2_EEs', CountW2_EEs);
  RegisterQuery('Count1099_EEs', Count1099_EEs);
  RegisterQuery('EEDirDep', EEDirDep);
  RegisterQuery('CSDirDep', CSDirDep);
  RegisterQuery('NewAgencyDirDep', NewAgencyDirDep);
  RegisterQuery('OldBillingForACH', OldBillingForACH);
  RegisterQuery('LocalTemplateDetails', LocalTemplateDetails);
  RegisterQuery('VersionFieldAsOf', VersionFieldAsOf);
  RegisterQuery('VersionFieldAsOfByNbr', VersionFieldAsOfByNbr);
//  RegisterQuery('VersionFieldAsOfByNbrExt', VersionFieldAsOfByNbrExt);
  RegisterQuery('VersionFieldChange', VersionFieldChange);
  RegisterQuery('VersionFieldChangeX', VersionFieldChangeX);
  RegisterQuery('EvFieldNbrDirect', EvFieldNbrDirect);
  RegisterQuery('EvAuditListOfNumbers', EvAuditListOfNumbers);

  RegisterQuery('SbTaskRuns', SbTaskRuns);
  RegisterQuery('LockTableWithCondition', LockTableWithCondition);
  RegisterQuery('SetTransactionVar', SetTransactionVar);
  RegisterQuery('ClearTransactionVar', ClearTransactionVar);
  RegisterQuery('DeleteRecord', DeleteRecord);
  RegisterQuery('EESumOfAmount', EESumOfAmount);
  RegisterQuery('SelectSomeRecordsNbrForBilling', SelectSomeRecordsNbrForBilling);
  RegisterQuery('SelectDeletedRecordsNbrForBilling', SelectDeletedRecordsNbrForBilling);
  RegisterQuery('SelectInsertedRecordsNbrForBilling', SelectInsertedRecordsNbrForBilling);
  RegisterQuery('SelectTableChangesForBilling', SelectTableChangesForBilling);
  RegisterQuery('SelectFieldChangesForBilling', SelectFieldChangesForBilling);
  RegisterQuery('NonVersionFieldAuditByNbr', NonVersionFieldAuditByNbr);
  RegisterQuery('NonVersionFieldAudit', NonVersionFieldAudit);
  RegisterQuery('InitialCreationNBRAudit', InitialCreationNBRAudit);
  RegisterQuery('RemapKey', RemapKey);
end;

procedure TevDBQueryLibrary.Clear;
var
  i: Integer;
begin
  for i := 0 to FList.Count - 1 do
    Dispose(PString(FList.Objects[i]));
  FList.Clear;
end;

function TevDBQueryLibrary.ProcessTemplates(const AEvSQL: String): String;

  function ProcessTemplate(var sql: String): String;
  var
    template, template_name, template_text, template_param: String;
    i: Integer;
  begin
    Result := GetNextStrValue(sql, '{');
    if sql <> '' then
    begin
      template := GetNextStrValue(sql, '}');
      template_name := Trim(GetNextStrValue(template, '<'));
      template_text := GetQueryText(template_name).SQL;
      if template_text = '' then
        raise EevException.Create(Format('Template "%s" is not defined', [template_name]));

      if template <> '' then
      begin
        template := Trim(GetNextStrValue(template, '>'));
        i := 1;
        while template <> '' do
        begin
          if template[1] = '"' then
          begin
            Delete(template, 1, 1);
            template_param := GetNextStrValue(template, '"');
            GetNextStrValue(template, ',');
          end
          else
            template_param := Trim(GetNextStrValue(template, ','));
          template_text := StringReplace(template_text, '$' + IntToStr(i), template_param, [rfReplaceAll]);
          template := TrimLeft(template);
          Inc(i);
        end;
      end;

      Result := Result + ProcessTemplates(template_text); // recursion!
    end;
  end;

var
  sql: String;
begin
  Result := '';
  sql := AEvSQL;
  while sql <> '' do
    Result := Result + ProcessTemplate(sql);
end;

procedure TevDBQueryLibrary.Initialize0;
begin
  RegisterQuery('GenericAsOfDate', GenericAsOfDate);
  RegisterQuery('AsOfDate', AsOfDate);
  RegisterQuery('AsOfNow', AsOfNow);
  RegisterQuery('DatabaseType', DatabaseType);
  RegisterQuery('UpdateAsOfDate', UpdateAsOfDate);
  RegisterQuery('DeleteVersionsAfterDate', DeleteVersionsAfterDate);
  RegisterQuery('DeleteAsOfDate', DeleteAsOfDate);
  RegisterQuery('GenericLastTableRecVerChange', GenericLastTableRecVerChange);
  RegisterQuery('SystemLastTableRecVerChange', SystemLastTableRecVerChange);
  RegisterQuery('BureauLastTableRecVerChange', BureauLastTableRecVerChange);
  RegisterQuery('ClientLastTableRecVerChange', ClientLastTableRecVerChange);
end;

initialization
  FQueryLib := TevDBQueryLibrary.Create;

end.
