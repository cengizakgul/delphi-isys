// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sSqlParser;

interface

uses
   SysUtils, DB, EvTypes, EvUtils, Classes, SDataStructure, EvConsts,
  EvDataAccessComponents, SyncObjs, isBasicUtils, EvContext, EvCommonInterfaces,
  SDDStreamClasses, rwCustomDataDictionary, isBaseClasses;


type
  IevSecuredSQL = interface
  ['{A46384AE-5791-4AAD-891E-913D8DADF5D7}']
    function ApplySecurity(const ASQL: String; const ADBType: TevDBType): String;
  end;

  function GetSecuredSQL: IevSecuredSQL;

implementation

type
  TSQLSections = (ssStart, ssSelect, ssFrom, ssWhere, ssSelectExpr, ssSelectExprStar, ssOthers);

  TTableRecord = record
    iUnion: Integer;
    sTable: string;
    sAlias: string;
    bExternal: Boolean;
  end;

  TFieldRecord = record
    iUnion: Integer;
    sTable: string;
    sField: string;
    sAlias: string;
    iPos: Integer;
    iLen: Integer;
    sReplace: string;
    bMustHaveAlias: Boolean;
  end;

  TWhereRecord = record
    iUnion: Integer;
    iBegin: Integer;
    iEnd: Integer;
    sExpr: string;
  end;

  TTableRecords = array of TTableRecord;
  PTableRecords  = ^TTableRecords;


  TevCachedTableInfo = class
  private
    FTable: TDataDictTable;
    FFields: TStringList;
  public
    constructor Create;
    destructor  Destroy; override;
  end;

  TevSQLParser = class(TisInterfacedObject, IevSecuredSQL)
  private
    FInProcess: Boolean;
    FFieldList: TStringList;
    function  Avoid1500ConditionLimit(const FieldName, InCondition: string): string;
    function  GetFieldList(const Table, Alias: String): String;
    function  FindTableForField(const Field, TableList: String): String;
    function  MapLogicalTypeToDatabase(const TableName, FieldName, FieldType: string; const Length: Integer): String;
    function  Parser(const SQL: string; const DbType: TevDBType; Section: TSQLSections = ssStart;
                     iCurUnion: Integer = 1; const PTables: PTableRecords = nil): string;
  protected
    procedure DoOnConstruction; override;
    function  ApplySecurity(const ASQL: String; const ADBType: TevDBType): String;
  public
    destructor Destroy; override;
  end;


function GetSecuredSQL: IevSecuredSQL;
begin
  Result := TevSQLParser.Create;
end;

function TevSQLParser.Avoid1500ConditionLimit(const FieldName, InCondition: string): string;
var
  Oper, s: String;
begin
  if Contains(InCondition, '=', True) then
    Result := FieldName + InCondition

  else
  begin
    Result := '';
    s := InCondition;
    Oper := GetNextStrValue(s, '(');
    GetPrevStrValue(s, ')');
    Result := BuildINsqlStatement(FieldName, s);

    if Contains(Oper, 'NOT') then
      Result := ' NOT '+ Result;
  end;

   Result := '('+ Result+ ')';
end;

function CmpProc(const Item1, Item2: Pointer): Integer;
begin
  if Integer(Item1) > Integer(Item2) then
    Result := 1
  else
  if Integer(Item1) < Integer(Item2) then
    Result := -1
  else
    Result := 0;
end;

{$WARNINGS OFF}
// There is stupid warning in line  Result := SQL;
// Cound't find any other solution
function TevSQLParser.Parser(const SQL: string; const DbType: TevDBType; Section: TSQLSections = ssStart;
                             iCurUnion: Integer = 1; const PTables: PTableRecords = nil): string;
const
  Delimiters = [' ', #9, #13, #10];

var
  p: PChar;
  pStart, pEnd: PChar;
  s, s2: string;
  pToken, pTable, pField, pTokenEnd, pAlias: PChar;
  c: Char;
  iBrCount: Integer;
  recFields: array of TFieldRecord;
  recTables, rCopy: TTableRecords;
  recWhere: array of TWhereRecord;
  sTable: string;
  sCl: string;
  i, j, k: Integer;
  iWhereBegin, iWhereEnd: Integer;
  SecFieldInfo: IevSecFieldInfo;
  SecRecInfo: IevSecRecordInfo;
  sFieldName : string;

  procedure CopyArray(var Source, Dest: TTableRecords);
  var
    i: Integer;
  begin
    SetLength(Dest, Length(Source));
    for i := Low(Source) to High(Source) do
      Dest[i] := Source[i];
  end;

  // to avoid problem with IB limited "IN" operator
  function CreateInterbaseFilter(const FieldName: string; OriginalCondition: string): string;
  var
    i: Integer;
    s, n: string;
    j, start, stop: Integer;
    l, g: TList;

    procedure OutGroup;
    var
      i: Integer;
    begin
      if g.Count > 0 then
      begin
        g.Sort(@CmpProc);
        if s <> '' then
          s := s + 'and ';
        s := s + '@ not in(' + IntToStr(Integer(g[0]));
        for i := 1 to g.Count-1 do
          s := s + ','+IntToStr(Integer(g[i]));
        s := s + ')';
        g.Clear;
      end;
    end;

  begin
    Assert(Length(OriginalCondition) > 0);
    Result := '';

    l := TList.Create;
    g := TList.Create;
    try
      s := OriginalCondition;
      for i := 1 to Length(s) do
      begin
        if (s[i] in ['0','1','2','3','4','5','6','7','8','9']) then
          n := n + s[i];
        if not (s[i] in ['0','1','2','3','4','5','6','7','8','9']) or (i = Length(s)) then
        begin
          l.Add(Pointer(StrToInt(n)));
          n := '';
        end;
      end;

      l.Sort(@CmpProc);
      s := '';
      if l.Count > 0 then
      begin
        l.Sort(@CmpProc);
        start := Integer(l[0]);
        stop := Integer(l[0]);
        for i := 0 to l.Count - 1 do
        begin
          if Integer(l[i]) = (stop + 1) then
            stop := Integer(l[i]);
          if (stop <> Integer(l[i])) or (i = (l.Count - 1)) then
          begin
            if stop - start < 10 then
            begin
              for j := start to stop do
                g.Add(Pointer(j));
              if g.Count > 100 then
                OutGroup;
            end
            else
            begin
              if s <> '' then
                s := s + ' and ';
              s := s + 'not (@>='+IntToStr(start) + ' and @<=' + IntToStr(stop)+')';
            end;
            if i = (l.Count - 1) then
            begin
              g.Sort(@CmpProc);
              if stop <> Integer(l[i]) then
                g.Add(l[i]);
              OutGroup;
            end;
            start := Integer(l[i]);
            stop := Integer(l[i]);
          end;
        end;
      end;
      s := StringReplace(s, '@', FieldName, [rfReplaceAll]);
    finally
      l.Free;
      g.Free;
    end;
    Result := '('+ s+ ')';
  end;

  function GetTableName(aTable: TTableRecord): String;
  begin
    if aTable.sAlias = '' then
      Result := aTable.sTable
    else
      Result := aTable.sAlias;
  end;

  function CheckQuotedText: Boolean;
  var
    QuoteChar: Char;
  begin
   if p^ in ['''', '"'] then
   begin
     QuoteChar := P^;
     Inc(p);
     while p < pEnd do
     begin
       if p^ = QuoteChar then
         if (p + 1)^ = QuoteChar then
           Inc(p)
         else
         begin
           Inc(p);
           break;
         end;

       Inc(p);
     end;
     Result := True;
   end

   else
     Result := False;
  end;


  function CheckParenthesis: Boolean;
  var
    BrCount: Integer;
  begin
    if p^ = '(' then
    begin
      Inc(p);
      BrCount := 1;
      while (p < pEnd) and (BrCount > 0) do
      begin
        CheckQuotedText;
        if p^ = '(' then
          Inc(BrCount)
        else if p^ = ')' then
          Dec(BrCount);
        Inc(p);
      end;
      Result := True;
    end

    else
      Result := False;
  end;

begin
  Result := SQL;
  iWhereBegin := 0;
  iWhereEnd := 0;
  p := PChar(Result);
  pStart := p;
  pEnd := p + Length(SQL);
  if p = pEnd then
    Exit;
  if Assigned(PTables) then
  begin
    recTables := PTables^;
    if Section = ssStart then
      for j := Low(recTables) to High(recTables) do
        recTables[j].bExternal := True;
  end;

  repeat
    case Section of

    ssStart:
    begin
      iWhereBegin := 0;
      iWhereEnd := 0;
      while not (p^ in ['s', 'S']) do
        Inc(p);
      SetString(s, p, 6);
      if (AnsiCompareText(s, 'select') = 0) and ((p + 6)^ in Delimiters + ['''', '"', '(']) then
      begin
        Section := ssSelect;
        Inc(p, 6);
        while p^ in Delimiters do
          Inc(p);

        SetString(s, p, 8);
        if (AnsiCompareText(s, 'distinct') = 0) and ((p + 8)^ in Delimiters + ['''', '"', '(']) then
        begin
          Inc(p, 8);
          while p^ in Delimiters do
            Inc(p);
        end
        else if (AnsiCompareText(Copy(s, 1, 3), 'all') = 0) and ((p + 3)^ in Delimiters + ['''', '"', '(']) then
        begin
          Inc(p, 3);
          while p^ in Delimiters do
            Inc(p);
        end;
      end
      else
        Inc(p);
    end;

    ssSelect:
    begin
      while p^ in Delimiters do
        Inc(p);

      pField := p;
      pTokenEnd := p;
      while True do
      begin
        while not (p^ in Delimiters + [',']) do
        begin
          case p^ of
          '''', '"':
          begin
            c := p^;
            Inc(p);
            while p^ <> c do
              Inc(p);
            Inc(p);
            Break;
          end;
          '(':
          begin
            iBrCount := 1;
            while iBrCount > 0 do
            begin
              Inc(p);
              case p^ of
              '(':
                Inc(iBrCount);
              ')':
                Dec(iBrCount);
              end;
            end;
            Inc(p);
            Break;
          end;
          end;
          Inc(p);
        end;
        pTokenEnd := p;

        while p^ in Delimiters do
          Inc(p);
        SetString(s, pField, pTokenEnd - pField);
        if (p^ in ['A'..'Z', 'a'..'z', ',']) and
           not ((pTokenEnd - 1)^ in ['+', '-', '*', '/', '|']) then
          Break;

        if (p^ in ['A'..'Z', 'a'..'z', ',']) and
           ((pTokenEnd - 1)^ = '*') and
           ((Length(Trim(Copy(s, 1, Pred(Length(s))))) = 0) or
            (Copy(Trim(Copy(s, 1, Pred(Length(s)))), Length(Trim(Copy(s, 1, Pred(Length(s))))), 1) = '.')) then
          Break;
      end;

      SetLength(recFields, Succ(Length(recFields)));
      recFields[High(recFields)].iUnion := iCurUnion;
      recFields[High(recFields)].sTable := '';
      recFields[High(recFields)].sField := s;
      recFields[High(recFields)].sAlias := '';
      recFields[High(recFields)].iPos := Succ(pField - pStart);
      recFields[High(recFields)].iLen := pTokenEnd - pField;
      recFields[High(recFields)].sReplace := '';
      recFields[High(recFields)].bMustHaveAlias := True;

      SetString(s, p, 7);
      if (AnsiCompareText(s, 'collate') = 0) and ((p + 7)^ in Delimiters) then
      begin
        Inc(p, 7);
        while p^ in Delimiters do
          Inc(p);
        while not (p^ in Delimiters + [',']) do
          Inc(p);
        while p^ in Delimiters do
          Inc(p);
      end;

      SetString(s, p, 2);
      if (AnsiCompareText(Copy(s, 1, 2), 'as') = 0) and ((p + 2)^ in Delimiters) then
      begin
        Inc(p, 2);
        while p^ in Delimiters do
          Inc(p);
      end;

      pAlias := p;
      while not (p^ in Delimiters + [',', '(']) do
        Inc(p);
      pTokenEnd := p;
      while p^ in Delimiters do
        Inc(p);
      if pTokenEnd <> pAlias then
        SetString(s, pAlias, pTokenEnd - pAlias)
      else
        s := '';

      if (s <> '') and (AnsiCompareText(s, 'from') <> 0) then
        recFields[High(recFields)].sAlias := s;

      if p^ = ',' then
        Inc(p)
      else
      begin
        if AnsiCompareText(s, 'from') <> 0 then
          Inc(p, 4);
        while p^ in Delimiters do
          Inc(p);
        Section := ssFrom;
      end;
    end;

    ssFrom:
    begin
      while p^ in Delimiters do
        Inc(p);

      pTable := p;
      while (p < pEnd) and not (p^ in Delimiters + [',']) do
      begin
        case p^ of
        '''', '"':
        begin
          c := p^;
          Inc(p);
          while p^ <> c do
            Inc(p);
          Inc(p);
          Break;
        end;
        '(':
        begin
          iBrCount := 1;
          pToken := p;
          while iBrCount > 0 do
          begin
            Inc(p);
            pToken := p;
            case p^ of
            '(':
              Inc(iBrCount);
            ')':
              Dec(iBrCount);
            end;
          end;
          if p <> pToken then
            SetString(s, pTable, p - pToken)
          else
            s := '';
          if Trim(s) <> '' then
          begin
            SetLength(recFields, Succ(Length(recFields)));
            recFields[High(recFields)].iUnion := iCurUnion;
            recFields[High(recFields)].sTable := '';
            recFields[High(recFields)].sField := s;
            recFields[High(recFields)].sAlias := '';
            recFields[High(recFields)].iPos := Succ(pToken - pStart);
            recFields[High(recFields)].iLen := p - pToken;
            recFields[High(recFields)].sReplace := '';
            recFields[High(recFields)].bMustHaveAlias := False;
          end;

          Inc(p);
          Break;
        end;
        end;
        Inc(p);
      end;
      pTokenEnd := p;
      while (p < pEnd) and (p^ in Delimiters) do
        Inc(p);
      SetString(sTable, pTable, pTokenEnd - pTable);

      pToken := p;
      while (p < pEnd) and not (p^ in Delimiters + [',', '''', '"', '(']) do
        Inc(p);
      pTokenEnd := p;
      while (p < pEnd) and (p^ in Delimiters) do
        Inc(p);
      if pTokenEnd <> pToken then
        SetString(s, pToken, pTokenEnd - pToken)
      else
        s := '';

      SetLength(recTables, Succ(Length(recTables)));
      recTables[High(recTables)].iUnion := iCurUnion;
      recTables[High(recTables)].sTable := sTable;
      recTables[High(recTables)].bExternal := False;
      if (s <> '') and
         (AnsiCompareText(s, 'on') <> 0) and
         (AnsiCompareText(s, 'left') <> 0) and
         (AnsiCompareText(s, 'right') <> 0) and
         (AnsiCompareText(s, 'full') <> 0) and
         (AnsiCompareText(s, 'inner') <> 0) and
         (AnsiCompareText(s, 'outer') <> 0) and
         (AnsiCompareText(s, 'join') <> 0) and
         (AnsiCompareText(s, 'group') <> 0) and
         (AnsiCompareText(s, 'where') <> 0) and
         (AnsiCompareText(s, 'having') <> 0) and
         (AnsiCompareText(s, 'union') <> 0) and
         (AnsiCompareText(s, 'plan') <> 0) and
         (AnsiCompareText(s, 'order') <> 0) and
         (AnsiCompareText(s, 'for') <> 0) then
      begin
        recTables[High(recTables)].sAlias := s;
        pToken := p;
        while (p < pEnd) and not (p^ in Delimiters + [',']) do
          Inc(p);
        pTokenEnd := p;
        while (p < pEnd) and (p^ in Delimiters) do
          Inc(p);
        if pTokenEnd <> pToken then
          SetString(s, pToken, pTokenEnd - pToken)
        else
          s := '';
      end
      else
        recTables[High(recTables)].sAlias := '';

      if AnsiCompareText(s, 'on') = 0 then
      repeat
        pToken := p;
        while (p < pEnd) and not (p^ in Delimiters + [',']) do
        begin
          case p^ of
          '''', '"':
          begin
            if p <> pToken then
              Break;
            c := p^;
            Inc(p);
            while p^ <> c do
              Inc(p);
            Inc(p);
            Break;
          end;
          '(':
          begin
            if p <> pToken then
              Break;
            iBrCount := 1;
            while iBrCount > 0 do
            begin
              Inc(p);
              case p^ of
              '(':
                Inc(iBrCount);
              ')':
                Dec(iBrCount);
              end;
            end;
            Inc(p);
            Break;
          end;
          end;
          Inc(p);
        end;
        pTokenEnd := p;
        while (p < pEnd) and (p^ in Delimiters) do
          Inc(p);
        if pTokenEnd <> pToken then
          SetString(s, pToken, pTokenEnd - pToken)
        else
          s := '';
      until (p >= pEnd) or
            (p^ = ',') or
            (AnsiCompareText(s, 'left') = 0) or
            (AnsiCompareText(s, 'right') = 0) or
            (AnsiCompareText(s, 'full') = 0) or
            (AnsiCompareText(s, 'inner') = 0) or
            (AnsiCompareText(s, 'outer') = 0) or
            (AnsiCompareText(s, 'join') = 0) or
            (AnsiCompareText(s, 'group') = 0) or
            (AnsiCompareText(s, 'where') = 0) or
            (AnsiCompareText(s, 'having') = 0) or
            (AnsiCompareText(s, 'union') = 0) or
            (AnsiCompareText(s, 'plan') = 0) or
            (AnsiCompareText(s, 'order') = 0) or
            (AnsiCompareText(s, 'for') = 0);


      if (AnsiCompareText(s, 'left') = 0) or
         (AnsiCompareText(s, 'right') = 0) or
         (AnsiCompareText(s, 'full') = 0) then
      begin
        pToken := p;
        while (p < pEnd) and not (p^ in Delimiters + [',']) do
          Inc(p);
        pTokenEnd := p;
        while (p < pEnd) and (p^ in Delimiters) do
          Inc(p);
        if pTokenEnd <> pToken then
          SetString(s, pToken, pTokenEnd - pToken)
        else
          s := '';
      end;

      if (AnsiCompareText(s, 'inner') = 0) or
         (AnsiCompareText(s, 'outer') = 0) then
      begin
        pToken := p;
        while (p < pEnd) and not (p^ in Delimiters + [',']) do
          Inc(p);
        pTokenEnd := p;
        while (p < pEnd) and (p^ in Delimiters) do
          Inc(p);
        if pTokenEnd <> pToken then
          SetString(s, pToken, pTokenEnd - pToken)
        else
          s := '';
      end;

      if AnsiCompareText(s, 'where') = 0 then
      begin
        Section := ssWhere;
        iWhereBegin := Succ(pToken - pStart) + 5;
      end
      else if AnsiCompareText(s, 'union') = 0 then
      begin
        Section := ssStart;
        iWhereBegin := -Succ(pToken - pStart);
        SetLength(recWhere, Succ(Length(recWhere)));
        recWhere[High(recWhere)].iUnion := iCurUnion;
        recWhere[High(recWhere)].iBegin := iWhereBegin;
        recWhere[High(recWhere)].iEnd := iWhereEnd;
        recWhere[High(recWhere)].sExpr := '';
        Inc(iCurUnion);
      end
      else if (s = '') and (pToken = pEnd) then
      begin
        Section := ssOthers;
        iWhereBegin := -(pToken - pStart + 2);
      end
      else if (AnsiCompareText(s, 'group') = 0) or
              (AnsiCompareText(s, 'having') = 0) or
              (AnsiCompareText(s, 'plan') = 0) or
              (AnsiCompareText(s, 'order') = 0) or
              (AnsiCompareText(s, 'for') = 0) then
      begin
        Section := ssOthers;
        iWhereBegin := -Succ(pToken - pStart);
      end
      else if p^ = ',' then
        Inc(p);
    end;

    ssSelectExpr, ssSelectExprStar:
    begin
      if Copy(Trim(Result), Length(Trim(Result)), 1) = '*' then
      begin
        SetLength(recFields, Succ(Length(recFields)));
        recFields[High(recFields)].iUnion := iCurUnion;
        recFields[High(recFields)].sTable := '';
        recFields[High(recFields)].sField := Trim(Result);
        recFields[High(recFields)].sAlias := '';
        recFields[High(recFields)].iPos := Succ(p - pStart);
        recFields[High(recFields)].iLen := Length(Result);
        recFields[High(recFields)].bMustHaveAlias := False;

        CopyArray(recTables, rCopy);
        if Pos('.', Result) > 0 then
        begin
          sTable := Trim(Copy(recFields[High(recFields)].sField, 1, Pred(Pos('.', recFields[High(recFields)].sField))));
          s := '';
          for j := Low(recTables) to High(recTables) do
            if (recTables[j].iUnion = iCurUnion) and
               (AnsiCompareText(recTables[j].sAlias, sTable) = 0) then
            begin
              s := sTable;
              sTable := recTables[j].sTable;
            end;
          recFields[High(recFields)].sReplace := Parser(GetFieldList(sTable, s), DbType, ssSelectExprStar, iCurUnion, @rCopy);
        end
        else
        begin
          recFields[High(recFields)].sReplace := '';
          for j := Low(recTables) to High(recTables) do
            if (recTables[j].iUnion = iCurUnion) and not recTables[j].bExternal then
              recFields[High(recFields)].sReplace := recFields[High(recFields)].sReplace +
                Parser(GetFieldList(recTables[j].sTable, ''), DbType, ssSelectExprStar, iCurUnion, @rCopy) + ',';
          Delete(recFields[High(recFields)].sReplace, Length(recFields[High(recFields)].sReplace), 1);
        end;
        Break;
      end;

      while p^ in Delimiters + ['+', '-', '*', '/', '|', ','] do
        Inc(p);

      pField := p;
      case p^ of
      '''', '"':
      begin
        c := p^;
        Inc(p);
        while p^ <> c do
          Inc(p);
        Inc(p);
        Continue;
      end;
      '(':
      begin
        iBrCount := 1;
        while iBrCount > 0 do
        begin
          Inc(p);
          case p^ of
          '(':
            Inc(iBrCount);
          ')':
            Dec(iBrCount);
          end;
        end;
        Inc(p);
        SetString(s, pField + 1, p - pField - 2);
        CopyArray(recTables, rCopy);
        SetLength(recFields, Succ(Length(recFields)));
        recFields[High(recFields)].iUnion := iCurUnion;
        recFields[High(recFields)].sTable := '';
        recFields[High(recFields)].sField := s;
        recFields[High(recFields)].sAlias := '';
        recFields[High(recFields)].iPos := Succ(pField + 1 - pStart);
        recFields[High(recFields)].iLen := p - pField - 2;
        recFields[High(recFields)].bMustHaveAlias := False;
        if (AnsiCompareText(Copy(Trim(s), 1, 6), 'select') = 0) and (Trim(s)[7] in Delimiters + ['''', '"', '(']) then
          recFields[High(recFields)].sReplace := Parser(s, DbType, ssStart, iCurUnion, @rCopy)
        else if Trim(s) = '*' then
          recFields[High(recFields)].sReplace := s
        else
          recFields[High(recFields)].sReplace := Parser(s, DbType, Section, iCurUnion, @rCopy);
      end;
      else
        while (p < pEnd) and not (p^ in Delimiters + ['+', '-', '*', '/', '|', ',', '(']) do
          Inc(p);
        SetString(s, pField, p - pField);
        SetLength(recFields, Succ(Length(recFields)));
        recFields[High(recFields)].iUnion := iCurUnion;
        recFields[High(recFields)].sTable := '';
        recFields[High(recFields)].sField := s;
        recFields[High(recFields)].sAlias := '';
        recFields[High(recFields)].iPos := Succ(pField - pStart);
        recFields[High(recFields)].iLen := p - pField;
        recFields[High(recFields)].sReplace := '';
        recFields[High(recFields)].bMustHaveAlias := Section = ssSelectExprStar;
      end;

    end;

    ssWhere:
    begin
      while p^ in Delimiters do
        Inc(p);

      pToken := p;
      while (p < pEnd) and not (p^ in Delimiters) do
      begin
        if CheckQuotedText then
          break;

        if CheckParenthesis then
          break;

        Inc(p);
      end;
      pTokenEnd := p;

      while (p < pEnd) and (p^ in Delimiters) do
        Inc(p);
      if pTokenEnd <> pToken then
        SetString(s, pToken, pTokenEnd - pToken)
      else
        s := '';

      if Copy(s, 1, 1) = '(' then
      begin
        s := Copy(s, 2, Length(s) - 2);
        if (AnsiCompareText(Copy(Trim(s), 1, 6), 'select') = 0) and (Trim(s)[7] in Delimiters + ['''', '"', '(']) then
        begin
          CopyArray(recTables, rCopy);
          SetLength(recFields, Succ(Length(recFields)));
          recFields[High(recFields)].iUnion := iCurUnion;
          recFields[High(recFields)].sTable := '';
          recFields[High(recFields)].sField := s;
          recFields[High(recFields)].sAlias := '';
          recFields[High(recFields)].iPos := Succ(pToken + 1 - pStart);
          recFields[High(recFields)].iLen := pTokenEnd - pToken - 2;
          recFields[High(recFields)].bMustHaveAlias := False;
          recFields[High(recFields)].sReplace := Parser(s, DbType, ssStart, iCurUnion, @rCopy)
        end;
      end
      else if AnsiCompareText(s, 'union') = 0 then
      begin
        Section := ssStart;
        iWhereEnd := Succ(pToken - pStart);
        SetLength(recWhere, Succ(Length(recWhere)));
        recWhere[High(recWhere)].iUnion := iCurUnion;
        recWhere[High(recWhere)].iBegin := iWhereBegin;
        recWhere[High(recWhere)].iEnd := iWhereEnd;
        recWhere[High(recWhere)].sExpr := '';
        Inc(iCurUnion);
      end
      else if (AnsiCompareText(s, 'group') = 0) or
              (AnsiCompareText(s, 'having') = 0) or
              (AnsiCompareText(s, 'plan') = 0) or
              (AnsiCompareText(s, 'order') = 0) or
              (AnsiCompareText(s, 'for') = 0) then
      begin
        Section := ssOthers;
        iWhereEnd := Succ(pToken - pStart);
      end;
    end
    else
      while p^ in Delimiters do
        Inc(p);

      pToken := p;
      while (p < pEnd) and not (p^ in Delimiters) do
      begin
        if CheckQuotedText then
          break;

        if CheckParenthesis then
          break;

        Inc(p);
      end;

      pTokenEnd := p;
      while (p < pEnd) and (p^ in Delimiters) do
        Inc(p);
      if pTokenEnd <> pToken then
        SetString(s, pToken, pTokenEnd - pToken)
      else
        s := '';

      if AnsiCompareText(s, 'union') = 0 then
      begin
        Section := ssStart;
        SetLength(recWhere, Succ(Length(recWhere)));
        recWhere[High(recWhere)].iUnion := iCurUnion;
        recWhere[High(recWhere)].iBegin := iWhereBegin;
        recWhere[High(recWhere)].iEnd := iWhereEnd;
        recWhere[High(recWhere)].sExpr := '';
        Inc(iCurUnion);
      end;
    end;
  until p >= pEnd;

  if iWhereEnd = 0 then
    iWhereEnd := Succ(pEnd - pStart);

  SetLength(recWhere, Succ(Length(recWhere)));
  recWhere[High(recWhere)].iUnion := iCurUnion;
  recWhere[High(recWhere)].iBegin := iWhereBegin;
  recWhere[High(recWhere)].iEnd := iWhereEnd;
  recWhere[High(recWhere)].sExpr := '';

  SetLength(rCopy, 0);
  for j := Low(recTables) to High(recTables) do
    if (Length(recTables[j].sTable) > 0) and (Trim(recTables[j].sTable)[1] = '(') then
      Parser(Copy(Trim(recTables[j].sTable), 2, Length(Trim(recTables[j].sTable)) - 2), DbType, ssFrom, recTables[j].iUnion, @rCopy)
    else if Pos('(', recTables[j].sTable) > 0 then
      recTables[j].sTable := Trim(Copy(recTables[j].sTable, 1, Pred(Pos('(', recTables[j].sTable))));

  for j := Low(rCopy) to High(rCopy) do
  begin
    SetLength(recTables, Succ(Length(recTables)));
    recTables[High(recTables)] := rCopy[j];
  end;

  for i := Low(recFields) to High(recFields) do
    if recFields[i].sReplace = '' then
      if (Pos('+', recFields[i].sField) > 0) or
         (Pos('-', recFields[i].sField) > 0) or
         (Pos('/', recFields[i].sField) > 0) or
         (Pos('*', recFields[i].sField) > 0) or
         (Pos('|', recFields[i].sField) > 0) or
         (Pos('(', recFields[i].sField) > 0) or
         (Pos('''', recFields[i].sField) > 0) or
         (Pos('"', recFields[i].sField) > 0) then
      begin
        CopyArray(recTables, rCopy);
        recFields[i].bMustHaveAlias := False;
        recFields[i].sReplace := Parser(recFields[i].sField, DbType, ssSelectExpr, recFields[i].iUnion, @rCopy);
      end
      else if Pos('.', recFields[i].sField) > 0 then
      begin
        recFields[i].sTable := Trim(Copy(recFields[i].sField, 1, Pred(Pos('.', recFields[i].sField))));
        recFields[i].sField := Trim(Copy(recFields[i].sField, Succ(Pos('.', recFields[i].sField)), Length(recFields[i].sField)));
      end
      else
      begin
        s := '';
        for j := Low(recTables) to High(recTables) do
          if recTables[j].iUnion = recFields[i].iUnion then
            s := s + recTables[j].sTable + ',';
        if Length(s) > 0 then
          Delete(s, Length(s), 1);
        if Pos(',', s) > 0 then
          recFields[i].sTable := FindTableForField(recFields[i].sField, s)
        else
          recFields[i].sTable := s;
      end;

    for i := Low(recWhere) to High(recWhere) do
      if recWhere[i].iBegin <> 0 then
      begin
        s := ' ';

        for k := 0 to ctx_AccountRights.Records.Count - 1 do
        begin
          SecRecInfo := ctx_AccountRights.Records.GetSecRecordInfo(k);

          if (SecRecInfo.DBType = dbtBureau) and (DbType = dbtBureau) then
          begin
            for j := Low(recTables) to High(recTables) do
            begin
              if (recWhere[i].iUnion = recTables[j].iUnion) and not recTables[j].bExternal and
                 (AnsiSameText(recTables[j].sTable, 'SB_USER') or AnsiSameText(recTables[j].sTable, 'SB_USER_HIST')) then
                s := s + '(('+ GetTableName(recTables[j]) + '.DEPARTMENT='''+ USER_DEPARTMENT_CUSTOMER_SERVICE+ ''') or '+
                  '(' + GetTableName(recTables[j]) + '.SB_USER_NBR=' + IntToStr(Context.UserAccount.InternalNbr) + ') or ' +
                  '(Trim(StrCopy('+ GetTableName(recTables[j]) + '.USER_FUNCTIONS, 0, 8))='''+ SecRecInfo.Filter + ''')) and ';
            end
          end

          else if (SecRecInfo.DBType = dbtTemporary) and (DbType = dbtTemporary) then
          begin
            for j := Low(recTables) to High(recTables) do
            begin
              if (SecRecInfo.Filter <> '') and
                 (recWhere[i].iUnion = recTables[j].iUnion) and not recTables[j].bExternal and
                 (StartsWith(recTables[j].sTable, 'TMP_') or AnsiSameText(recTables[j].sTable, 'ALL_CL_NUMBERS')) then
                s := s + '(' + Avoid1500ConditionLimit(GetTableName(recTables[j]) + '.CL_NBR', SecRecInfo.Filter) +
                      ' or ' + GetTableName(recTables[j]) + '.CL_NBR is null) and ';
            end
          end

          else
            for j := Low(recTables) to High(recTables) do
              if (recWhere[i].iUnion = recTables[j].iUnion) and
                 not recTables[j].bExternal then
              begin
                sTable := AnsiUpperCase(GetFieldList(recTables[j].sTable, '') + ',');
                if DbType = dbtTemporary then
                  sCl := GetTableName(recTables[j]) + '.CL_NBR <> '+ IntToStr(SecRecInfo.ClientID)+ ' or '+ GetTableName(recTables[j]) + '.CL_NBR is null or '
                else
                  sCl := '';

                if (DbType = dbtTemporary) or
                   ((DbType = dbtClient) and (SecRecInfo.ClientID = ctx_DBAccess.CurrentClientNbr)) then
                begin
                  if (SecRecInfo.FilterType = 'CO') and
                     (Pos('.CO_NBR,', sTable) > 0) then
                    s := s + '(' + sCl+ '(' + CreateInterbaseFilter(GetTableName(recTables[j]) + '.CO_NBR', SecRecInfo.Filter)+ ' or ' + GetTableName(recTables[j]) + '.CO_NBR is null)) and '
                  else if (SecRecInfo.FilterType = 'CO') and
                          (Pos('.PRIMARY_CO_NBR,', sTable) > 0) then
                    s := s + '(' + sCl+ '(' + CreateInterbaseFilter(GetTableName(recTables[j]) + '.PRIMARY_CO_NBR', SecRecInfo.Filter)+ ' or ' + GetTableName(recTables[j]) + '.PRIMARY_CO_NBR is null)) and '
                  else if (SecRecInfo.FilterType = 'DV') and
                          (Pos('.CO_DIVISION_NBR,', sTable) > 0) then
                    s := s + '(' + sCl+ '(' + CreateInterbaseFilter(GetTableName(recTables[j]) + '.CO_DIVISION_NBR', SecRecInfo.Filter)+ ' or ' + GetTableName(recTables[j]) + '.CO_DIVISION_NBR is null)) and '
                  else if (SecRecInfo.FilterType = 'BR') and
                          (Pos('.CO_BRANCH_NBR,', sTable) > 0) then
                    s := s + '(' + sCl+ '(' + CreateInterbaseFilter(GetTableName(recTables[j]) + '.CO_BRANCH_NBR', SecRecInfo.Filter)+ ' or ' + GetTableName(recTables[j]) + '.CO_BRANCH_NBR is null)) and '
                  else if (SecRecInfo.FilterType = 'DP') and
                          (Pos('.CO_DEPARTMENT_NBR,', sTable) > 0) then
                    s := s + '(' + sCl+ '(' + CreateInterbaseFilter(GetTableName(recTables[j]) + '.CO_DEPARTMENT_NBR', SecRecInfo.Filter)+ ' or ' + GetTableName(recTables[j]) + '.CO_DEPARTMENT_NBR is null)) and '
                  else if (SecRecInfo.FilterType = 'TM') and
                          (Pos('.CO_TEAM_NBR,', sTable) > 0) then
                    s := s + '(' + sCl+ '(' + CreateInterbaseFilter(GetTableName(recTables[j]) + '.CO_TEAM_NBR', SecRecInfo.Filter)+ ' or ' + GetTableName(recTables[j]) + '.CO_TEAM_NBR is null)) and ';
                end
                else
                   if sCl <> '' then
                     s := s + '(' + sCl + ') and ';
              end;
        end;

        if Trim(s) <> '' then
        begin
          if recWhere[i].iBegin < 0 then
          begin
            Delete(s, Length(s) - 4, 5);
            s := ' where ' + s;
          end
          else
            s := s + '(';
          SetLength(recFields, Succ(Length(recFields)));
          recFields[High(recFields)].iUnion := 0;
          recFields[High(recFields)].sTable := '';
          recFields[High(recFields)].sField := '';
          recFields[High(recFields)].sAlias := '';
          recFields[High(recFields)].iPos := Abs(recWhere[i].iBegin);
          recFields[High(recFields)].iLen := 0;
          recFields[High(recFields)].sReplace := s;
          recFields[High(recFields)].bMustHaveAlias := False;

          if recWhere[i].iBegin > 0 then
          begin
            SetLength(recFields, Succ(Length(recFields)));
            recFields[High(recFields)].iUnion := 0;
            recFields[High(recFields)].sTable := '';
            recFields[High(recFields)].sField := '';
            recFields[High(recFields)].sAlias := '';
            recFields[High(recFields)].iPos := recWhere[i].iEnd;
            recFields[High(recFields)].iLen := 0;
            recFields[High(recFields)].sReplace := ') ';
            recFields[High(recFields)].bMustHaveAlias := False;
          end;
        end;
      end;

  for i := Low(recFields) to High(recFields) do
    if recFields[i].sReplace = '' then
      for j := Low(recTables) to High(recTables) do
        if (recTables[j].iUnion = recFields[i].iUnion) and
           (AnsiCompareText(recTables[j].sAlias, recFields[i].sTable) = 0) then
          recFields[i].sTable := recTables[j].sTable;

  for i := Low(recFields) to High(recFields) do
  begin
    s2 := recFields[i].sTable;
    if (AnsiCompareText(Copy(recFields[i].sTable, Length(recFields[i].sTable) - 4, 5), '_VIEW') = 0) or
       (AnsiCompareText(Copy(recFields[i].sTable, Length(recFields[i].sTable) - 4, 5), '_HIST') = 0) then
      recFields[i].sTable := Copy(recFields[i].sTable, 1, Length(recFields[i].sTable) - 5)
    else if AnsiCompareText(Copy(recFields[i].sTable, 1, 4), 'TMP_') = 0 then
      recFields[i].sTable := Copy(recFields[i].sTable, 5, Length(recFields[i].sTable));

    {if (recFields[i].sReplace = '') and
       (AnsiCompareText(recFields[i].sField, 'FILLER') = 0) then
    begin
      KeyTablesList := TStringList.Create;
      try
        KeyTablesList.Text := KeyTables;
        FillerLength := Parse(GetTableString(KeyTablesList, recFields[i].sTable), 'Filler');
        if FillerLength <> '' then
        begin
          recFields[i].sReplace := Trim(MapLogicalTypeToDatabase(recFields[i].sTable, s2 + '.' + recFields[i].sField, FieldMetaString, StrToInt(FillerLength)));
          if (recFields[i].sAlias = '') and recFields[i].bMustHaveAlias then
            recFields[i].sReplace := recFields[i].sReplace + ' ' + recFields[i].sField;
        end;
      finally
        KeyTablesList.Free;
      end;
    end;}
  end;

  for i := Low(recFields) to High(recFields) do
  begin
    if recFields[i].sReplace = '' then
    begin
      SecFieldInfo := ctx_AccountRights.Fields.GetSecFieldInfo(recFields[i].sTable, recFields[i].sField);
      if Assigned(SecFieldInfo) and (SecFieldInfo.State = stDisabled) then
      begin
       //    This part of source code added because of ticket number  55304 and   27760 and 60543
        sFieldName := '';
        if ((recFields[i].sTable = 'CL_PERSON') or (recFields[i].sTable = 'CL_PERSON_DEPENDENTS')) and
            (recFields[i].sField = 'SOCIAL_SECURITY_NUMBER') then
                     sFieldName := 'SOCIAL_SECURITY_NUMBER'
        else
        if ((recFields[i].sTable = 'CL_BANK_ACCOUNT') or (recFields[i].sTable = 'SB_BANK_ACCOUNTS')) and
            (recFields[i].sField = 'CUSTOM_BANK_ACCOUNT_NUMBER') then
                     sFieldName := 'CUSTOM_BANK_ACCOUNT_NUMBER'
        else
        if ((recFields[i].sTable = 'PR_CHECK') or (recFields[i].sTable = 'PR_MISCELLANEOUS_CHECKS')) and
            (recFields[i].sField = 'CUSTOM_PR_BANK_ACCT_NUMBER') then
                     sFieldName := 'CUSTOM_PR_BANK_ACCT_NUMBER'
        else
        if (recFields[i].sTable = 'EE_DIRECT_DEPOSIT') and (recFields[i].sField = 'EE_BANK_ACCOUNT_NUMBER') then
              sFieldName := 'EE_BANK_ACCOUNT_NUMBER';

        recFields[i].sReplace := Trim(MapLogicalTypeToDatabase(recFields[i].sTable, sFieldName, SecFieldInfo.FieldType, SecFieldInfo.FieldSize));
        if (recFields[i].sAlias = '') and recFields[i].bMustHaveAlias then
          recFields[i].sReplace := recFields[i].sReplace + ' ' + recFields[i].sField;
      end;
    end;
  end;

  for i := Low(recFields) to High(recFields) do
    if recFields[i].sReplace <> '' then
    begin
      Delete(Result, recFields[i].iPos, recFields[i].iLen);
      Insert(recFields[i].sReplace, Result, recFields[i].iPos);

      for j := Succ(i) to High(recFields) do
        if recFields[j].iPos > recFields[i].iPos then
          recFields[j].iPos := recFields[j].iPos + Length(recFields[i].sReplace) - recFields[i].iLen;
    end;

  if Assigned(PTables) then
    PTables^ := recTables;
end;
{$WARNINGS ON}

{ TevSQLParser }

function TevSQLParser.FindTableForField(const Field, TableList: String): String;
var
  s, sTbl: string;
  Tbl: TDataDictTable;
begin
  Result := '';

  s := TableList;
  while s <> '' do
  begin
    sTbl := Trim(GetPrevStrValue(s, ','));
    Tbl := EvoDataDictionary.Tables.TableByName(sTbl);
    if Assigned(Tbl) and (Tbl.Fields.FieldByName(Field) <> nil) then
    begin
      Result := sTbl;
      break;
    end;
  end;
end;

function TevSQLParser.GetFieldList(const Table, Alias: String): String;
var
  i: Integer;
  Tbl: TDataDictTable;
  CachedTbl: TevCachedTableInfo;
  sTbl: String;
  pStr: PString;
begin
  Result := '';

  if FinishesWith(Table, '_HIST') then
    sTbl := Copy(Table, 1, Length(Table) - 5)
  else
    sTbl := Table;

  i := FFieldList.IndexOf(sTbl);
  if i = -1 then
  begin
    CachedTbl := TevCachedTableInfo.Create;
    CachedTbl.FTable := EvoDataDictionary.Tables.TableByName(sTbl);
    FFieldList.AddObject(sTbl, CachedTbl);
  end
  else
    CachedTbl := TevCachedTableInfo(FFieldList.Objects[i]) ;

  Tbl := CachedTbl.FTable;
  if Assigned(Tbl) then
  begin
    if Alias <> '' then
      sTbl := Alias
    else
      sTbl := Table;

    i := CachedTbl.FFields.IndexOf(sTbl);
    if i = -1 then
    begin
      for i := 0 to Tbl.Fields.Count - 1 do
        Result := Result + sTbl + '.' + Tbl.Fields[i].Name + ',';
      Delete(Result, Length(Result), 1);
      New(pStr);
      pStr^ := Result;
      CachedTbl.FFields.AddObject(sTbl, Pointer(pStr));
    end
    else
      Result := PString(CachedTbl.FFields.Objects[i])^;
  end;
end;

function TevSQLParser.MapLogicalTypeToDatabase(const TableName, FieldName, FieldType: string; const Length: Integer): String;
begin
  if FieldType = FieldMetaString then
    if FieldName = '' then
      Result := 'CAST('' '' as VARCHAR('+ IntToStr(Length)+ '))'
    else
    begin
      if ((TableName = 'CL_PERSON') or (TableName = 'CL_PERSON_DEPENDENTS')) and (FieldName = 'SOCIAL_SECURITY_NUMBER') then
         Result := 'CAST(' + '''000-00-0000''' + ' as VARCHAR('+ IntToStr(Length)+ '))'
      else
      if ((TableName = 'CL_BANK_ACCOUNT') or (TableName = 'SB_BANK_ACCOUNTS')) and (FieldName = 'CUSTOM_BANK_ACCOUNT_NUMBER') then
         Result := 'TRIM(CAST(SUBSTRING(PadLeft(CUSTOM_BANK_ACCOUNT_NUMBER,'+ ''' ''' + ',20) FROM 1 FOR 16)  as VARCHAR('+ IntToStr(Length)+ ')) ||' + '''xxxx''' + ')'
      else
      if (TableName = 'EE_DIRECT_DEPOSIT') and (FieldName = 'EE_BANK_ACCOUNT_NUMBER') then
         Result := 'TRIM(CAST(SUBSTRING(PadLeft(EE_BANK_ACCOUNT_NUMBER,'+ ''' ''' + ',20) FROM 1 FOR 16)  as VARCHAR('+ IntToStr(Length)+ ')) ||' + '''xxxx''' + ')'
      else
      if ((TableName = 'PR_CHECK') or (TableName = 'PR_MISCELLANEOUS_CHECKS')) and (FieldName = 'CUSTOM_PR_BANK_ACCT_NUMBER') then
         Result := 'TRIM(CAST(SUBSTRING(PadLeft(CUSTOM_PR_BANK_ACCT_NUMBER,'+ ''' ''' + ',20) FROM 1 FOR 16)  as VARCHAR('+ IntToStr(Length)+ ')) ||' + '''xxxx''' + ')'
      else
         Result := 'CAST(' + FieldName + ' as VARCHAR('+ IntToStr(Length)+ '))';
    end
  else if FieldType = FieldMetaUniqueString then
    Result := 'CAST(''[Hidden#''||CAST('+ TableName+ '_NBR as VARCHAR(32))||'']'' as VARCHAR('+ IntToStr(Length)+ '))'
  else if FieldType = FieldMetaCurrency then
    Result := 'CAST(0 as NUMERIC(18, 6))'
  else if FieldType = FieldMetaInteger then
    Result := 'CAST(0 as INTEGER)'
  else if FieldType = FieldMetaFloat then
    Result := 'CAST(0 as NUMERIC(18, 6))'
  else if FieldType = FieldMetaTimeStamp then
    Result := 'CAST(''12/30/1899'' as TIMESTAMP)'
  else if FieldType = FieldMetaDate then
    Result := 'CAST(''12/30/1899'' as DATE)'
  else if FieldType = FieldMetaFK then
    Result := 'CAST(NULL as INTEGER)'
  else if FieldType = FieldMetaSbBlob then
    Result := '(select data from sb_blob where sb_blob_nbr=0)'
  else if FieldType = FieldMetaClBlob then
    Result := '(select data from cl_blob where cl_blob_nbr=0)'
  else
    Result := '';
end;

function TevSQLParser.ApplySecurity(const ASQL: String; const ADBType: TevDBType): String;
begin
  if FInProcess then
    Result := ASQL
  else
  begin
    FInProcess := True;
    try
      Result := Parser(ASQL, ADBType);
    finally
      FInProcess := False;
    end;
  end
end;

procedure TevSQLParser.DoOnConstruction;
begin
  inherited;
  FFieldList := TStringList.Create;
  FFieldList.Duplicates := dupError;
  FFieldList.CaseSensitive := False;
  FFieldList.Sorted := True;
end;

destructor TevSQLParser.Destroy;
var
  i: Integer;
begin
  for i := 0 to FFieldList.Count - 1 do
    FFieldList.Objects[i].Free;
  FreeAndNil(FFieldList);
  inherited;
end;

{ TevCachedTableInfo }

constructor TevCachedTableInfo.Create;
begin
  inherited;
  FFields := TStringList.Create;
  FFields.Duplicates := dupError;
  FFields.CaseSensitive := False;
  FFields.Sorted:= True;
end;

destructor TevCachedTableInfo.Destroy;
var
  i: Integer;
begin
  for i := 0 to FFields.Count - 1 do
    Dispose(PString(FFields.Objects[i]));
  FFields.Free;
  inherited;
end;

end.

