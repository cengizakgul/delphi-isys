// Module "DB Access"
unit pxy_DB_Access;

interface

uses
  Windows, Classes, Variants, isBaseClasses, EvCommonInterfaces, EvStreamUtils,
  EvMainBoard, EvContext, SysUtils, EvTransportDatagrams, evRemoteMethods, evProxy,
  IsBasicUtils, StrUtils, EvTypes, EvConsts, EvTransportInterfaces,
  EvBasicUtils, EvInitApp, EvDataAccessComponents, DateUtils, isTypes;

implementation

type
  TevDBAccessProxy = class(TevProxyModule, IevDBAccess)
  private
    FCurrentClientNbr: Integer;
    FADRContext: Boolean;
    FParams: IisParamsCollection;
    FDBInTran: TevDBTypes;
    FTransactionName: String;
    FTranCount: Integer;
    FNewRecKeyGen: Integer;
    function  AddLazyOperation(const AMethod: String): IisListOfValues;
  protected
    procedure DoOnConstruction; override;
    procedure EnableSecurity;
    procedure DisableSecurity;
    procedure SetChangedByNbr(const AValue: Integer = 0);
    function  GetChangedByNbr: Integer;
    procedure EnableReadTransaction;
    procedure DisableReadTransaction;
    procedure LockTableInTransaction(const aTable: String; const aCondition: String = '');
    function  PatchDB(const ADBName: String; const AAppVersion: String; AllowDowngrade: Boolean): Boolean;
    function  CustomScript(const ADBName: String; const AScript: String): Boolean;
    function  SyncDBAndApp(const ADBName: String): Boolean;
    procedure SyncUDFAndApp();
    procedure ImportDatabase(const ADBPath, ADBPassword: String);
    function  GetDBServerFolderContent(const APath, AUser, APassword: String): IevDataSet;
    function  GetDBFilesList: IevDataSet;
    procedure TidyUpDBFolders;
    procedure ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    procedure UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    function  FullTTRebuildInProgress: Boolean;
    procedure ChangeFBAdminPassword(const APassword, ANewPassword: String);
    procedure ChangeFBUserPassword(const AAdminPassword, ANewPassword: String);
    function  GetDBServersInfo: IisParamsCollection;

    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
    procedure SetCurrentClientNbr(const AValue: Integer);
    function  GetCurrentClientNbr: Integer;
    procedure StartTransaction(const ADBTypes: TevDBTypes; const ATransactionName: String = '');
    function  CommitTransaction: IisValueMap;
    procedure RollbackTransaction;
    function  InTransaction: Boolean;    
    function  GetDataSets(const DataSetParams: TGetDataParams): IisStream;
    procedure ApplyDataChangePacket(const APacket: IevDataChangePacket);
    procedure RunInTransaction(const AMethodName: String; const AParams: IisListOfValues);
    function  GetClientsList(const allList: Boolean = false; const fromTT: Boolean = false): string;
    procedure DeleteClientDB(ClientNumber: Integer);
    function  CreateClientDB(const ClDataset : IevDataset): Integer;
    function  OpenQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean = True): IevDataSet;
    procedure ExecQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues);
    function  ExecStoredProc(const AProcName: String; const AParams: IisListOfValues): IisListOfValues;
    function  GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
    procedure SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
    function  GetNewKeyValue(const ATableName: String; const AIncrement: Integer = 1): Integer;

    procedure BeginRebuildAllClients;
    procedure RebuildTemporaryTables(const aClientNbr: Integer; const aAllClientsMode: Boolean; const aTempClientsMode: Boolean = False);
    procedure EndRebuildAllClients;

    procedure CleanDeletedClientsFromTemp;
    function  GetDBVersion(const ADBType: TevDBType): String;
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    function  BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompression: Boolean): IisStream;
    procedure RestoreDB(const ADBName: String; const AData: IevDualStream; const ACompressed: Boolean);
    procedure SweepDB(const ADBName: String);
    procedure DropDB(const ADBName: String);
    procedure UndeleteClientDB(const AClientID: Integer);
    function  GetLastTransactionNbr(const ADBType: TevDBType): Integer;
    function  GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
    function  GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
    procedure ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
    function  GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IisStream;
    procedure CheckDBHash(const ADBType: TevDBType; const AData: IisStream; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
    procedure SetADRContext(const AValue: Boolean);
    function  GetADRContext: Boolean;
    property  CurrentClientNbr: Integer read GetCurrentClientNbr write SetCurrentClientNbr;
    function  GetClientROFlag: Char;
    procedure SetClientROFlag(const AValue: Char);

    function  GetFieldValues(const ATable, AField: String; const ANbr: Integer; const AsOfDate: TDateTime; const AEndDate: TDateTime; const AMultiColumn: Boolean): IisListOfValues;
    function  GetFieldVersions(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ASettings: IisListOfValues = nil): IevDataSet;
    procedure UpdateFieldVersion(const ATable: String; const ANbr: Integer; const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TDateTime;
                                 const AFieldsAndValues: IisListOfValues);
    procedure UpdateFieldValue(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime; const AEndDate: TDateTime; const AValue: Variant);

    function  GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
    function  GetVersionFieldAuditChange(const ATable, AField: String;  {const ANbr: Integer;} const ABeginDate: TDateTime): IevDataset;

    function  GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
    function  GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
    function  GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
    function  GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
    function  GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
  end;


  IevDisabledDBList = interface
  ['{DA403140-C1C2-4B50-B237-9CD2D226C821}']
    procedure DisableDBs(const ADomain: String; const ADBList: IisStringList);
    procedure EnableDBs(const ADomain: String; const ADBList: IisStringList);
    function  GetDisabledDBs(const ADomain: String): IisStringList;
    procedure SetDBMaintenance(const ADomain: String; const aDBName: String; const aMaintenance: Boolean);
  end;

  TevDisabledDBList = class(TisListOfValues, IevDisabledDBList)
  private
    procedure DisableDBs(const ADomain: String; const ADBList: IisStringList);
    procedure EnableDBs(const ADomain: String; const ADBList: IisStringList);
    function  GetDisabledDBs(const ADomain: String): IisStringList;
    procedure SetDBMaintenance(const ADomain: String; const aDBName: String; const aMaintenance: Boolean);
  protected
    procedure DoOnConstruction; override;
  end;

// This is not actual DB disabling feature, it's only an indicator for local usage
var DisabledDBs: IevDisabledDBList;

function GetDBAccess: IInterface;
begin
  Result := TevDBAccessProxy.Create;
end;

{ TevDBAccessProxy }

function TevDBAccessProxy.AddLazyOperation(const AMethod: String): IisListOfValues;
begin
  CheckCondition(FTranCount > 0, 'No active transaction');
  Result := FParams.AddParams(IntToStr(FParams.Count + 1));
  Result.AddValue('Method', AMethod);
end;


procedure TevDBAccessProxy.CleanDeletedClientsFromTemp;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_CLEANDELETEDCLIENTSFROMTEMP);
  D := SendRequest(D);
end;

function TevDBAccessProxy.CommitTransaction: IisValueMap;
var
  D: IevDatagram;
  S: String;
begin
  if FTranCount = 1 then
  begin
    CheckCondition(FDBInTran <> [], 'Transaction is not active');

    D := CreateDatagram(METHOD_DB_ACCESS_COMMITTRANSACTIONS);
    S := '';
    if dbtSystem in FDBInTran then
      S := S + CH_DATABASE_SYSTEM;
    if dbtClient in FDBInTran then
      S := S + CH_DATABASE_CLIENT;
    if dbtBureau in FDBInTran then
      S := S + CH_DATABASE_SERVICE_BUREAU;
    if dbtTemporary in FDBInTran then
      S := S + CH_DATABASE_TEMP;

    D.Params.AddValue('DBInTran', S);
    D.Params.AddValue('TransactionName', FTransactionName);
    D.Params.AddValue('Params', FParams);

    D := SendRequest(D);
    Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisValueMap;
    FDBInTran := [];
    FTransactionName := '';
    FParams.Clear;
  end
  else
    Result := nil;

  Dec(FTranCount);
end;

function TevDBAccessProxy.CreateClientDB(const ClDataset : IevDataset): Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_CREATECLIENTDB);
  D.Params.AddValue('ClDataset', ClDataset);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevDBAccessProxy.DeleteClientDB(ClientNumber: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_DELETECLIENTDB);
  D.Params.AddValue('ClientNumber', ClientNumber);
  D := SendRequest(D);
end;

procedure TevDBAccessProxy.DoOnConstruction;
begin
  inherited;
  FParams := TisParamsCollection.Create;
  FDBInTran := [];
end;

procedure TevDBAccessProxy.ExecQuery(const AQueryName: String; const AParams, AMacros: IisListOfValues);
var
  Op: IisListOfValues;
begin
  Op := AddLazyOperation('ExecQuery');
  Op.AddValue('AQueryName', AQueryName);
  Op.AddValue('AParams', AParams);
  Op.AddValue('AMacros', AMacros);
end;

function TevDBAccessProxy.ExecStoredProc(const AProcName: String;
  const AParams: IisListOfValues): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_EXECSTOREDPROC);
  D.Params.AddValue('AProcName', AProcName);
  D.Params.AddValue('AParams', AParams);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function  TevDBAccessProxy.GetClientsList(const allList: Boolean = false; const fromTT: Boolean = false): string;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETCLIENTSLIST);
  D.Params.AddValue('allList', allList);
  D.Params.AddValue('fromTT', fromTT);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevDBAccessProxy.GetCurrentClientNbr: Integer;
begin
  Result := FCurrentClientNbr;
end;

function TevDBAccessProxy.GetDataSets(
  const DataSetParams: TGetDataParams): IisStream;
var
  D: IevDatagram;
  S: IevDualStream;
  OldPos: Integer;
begin
  S := TEvDualStreamHolder.Create;
  OldPos := DataSetParams.MemStream.Position;
  try
    S.CopyFrom(DataSetParams.MemStream, 0);
  finally
    DataSetParams.MemStream.Position := OldPos;
  end;

  D := CreateDatagram(METHOD_DB_ACCESS_GETDATASETS);
  D.Params.AddValue('DataSetParams', IInterface(S));
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;

function TevDBAccessProxy.GetDBVersion(const ADBType: TevDBType): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETDBVERSION);
  D.Params.AddValue('ADBType', ADBType);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevDBAccessProxy.GetGeneratorValue(const AGeneratorName: String;
  const ADBType: TevDBType; const AIncrement: Integer): Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETGENERATORVALUE);
  D.Params.AddValue('AGeneratorName', AGeneratorName);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('AIncrement', AIncrement);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevDBAccessProxy.GetNewKeyValue(const ATableName: String; const AIncrement: Integer): Integer;
begin
  // This is not a real number!
  // It gets regenerated on server side and then substituted on client side by real number
  Inc(FNewRecKeyGen, AIncrement);
  Result := - FNewRecKeyGen;
end;

function TevDBAccessProxy.OpenQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean): IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_OPENQUERY);
  D.Params.AddValue('AQueryName', AQueryName);
  D.Params.AddValue('AParams', AParams);
  D.Params.AddValue('AMacros', AMacros);
  D.Params.AddValue('ALoadBlobs', ALoadBlobs);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataSet;
end;

procedure TevDBAccessProxy.RebuildTemporaryTables(const aClientNbr: Integer; const aAllClientsMode: Boolean; const aTempClientsMode: Boolean = False);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_REBUILDTEMPORARYTABLES);
  D.Params.AddValue('aClientNbr', aClientNbr);
  D.Params.AddValue('aAllClientsMode', aAllClientsMode);
  D.Params.AddValue('aTempClientsMode', aTempClientsMode);
  D := SendRequest(D);
end;

procedure TevDBAccessProxy.RollbackTransaction;
begin
  CheckCondition(FTranCount > 0, 'No active transaction');

  FParams.Clear;
  FDBInTran := [];
  FTransactionName := '';

  Dec(FTranCount);
end;

procedure TevDBAccessProxy.RunInTransaction(const AMethodName: String; const AParams: IisListOfValues);
var
  Op: IisListOfValues;
begin
  Op := AddLazyOperation('RunInTransaction');
  Op.AddValue('AMethodName', AMethodName);
  Op.AddValue('AParams', AParams);
end;

procedure TevDBAccessProxy.SetCurrentClientNbr(const AValue: Integer);
begin
  FCurrentClientNbr := AValue;
end;

procedure TevDBAccessProxy.SetGeneratorValue(const AGeneratorName: String;
  const ADBType: TevDBType; const AValue: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_SETGENERATORVALUE);
  D.Params.AddValue('AGeneratorName', AGeneratorName);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('AValue', AValue);
  D := SendRequest(D);
end;

procedure TevDBAccessProxy.StartTransaction(const ADBTypes: TevDBTypes;
  const ATransactionName: String = '');
begin
  if FDBInTran <> [] then
  begin
    CheckCondition((ADBTypes * FDBInTran) = ADBTypes, 'Cannot append database into active transaction');
    CheckCondition((ATransactionName = '') or AnsiSameText(ATransactionName, FTransactionName), 'Transaction name cannot be changed');
  end
  else
  begin
    FDBInTran := ADBTypes;
    FTransactionName := ATransactionName;
  end;

  Inc(FTranCount);
end;

function TevDBAccessProxy.GetDisabledDBs: IisStringList;
var
  sDomainID: String;
begin
  if ctx_DBAccess.GetADRContext then
    sDomainID := ctx_DomainInfo.DomainName + '-ADR'
  else
    sDomainID := ctx_DomainInfo.DomainName;

  Result := DisabledDBs.GetDisabledDBs(sDomainID);
end;

function TevDBAccessProxy.BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompression: Boolean): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_BACKUPDB);
  D.Params.AddValue('ADBName', ADBName);
  D.Params.AddValue('AMetadataOnly', AMetadataOnly);
  D.Params.AddValue('AScramble', AScramble);
  D.Params.AddValue('ACompression', ACompression);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;

procedure TevDBAccessProxy.RestoreDB(const ADBName: String; const AData: IevDualStream; const ACompressed: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_RESTOREDB);
  D.Params.AddValue('ADBName', ADBName);
  D.Params.AddValue('AData', AData);
  D.Params.AddValue('ACompressed', ACompressed);
  D := SendRequest(D);
end;

function TevDBAccessProxy.GetLastTransactionNbr(const ADBType: TevDBType): Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETLASTTRANSNBR);
  D.Params.AddValue('ADBType', ADBType);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevDBAccessProxy.GetClientROFlag: Char;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETCLIENTROFLAG);
  D := SendRequest(D);
  Result := VartoStr(D.Params.Value[METHOD_RESULT])[1];
end;

Procedure TevDBAccessProxy.SetClientROFlag(const AValue: Char);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_SETCLIENTROFLAG);
  D.Params.AddValue('AValue', AValue);
  D := SendRequest(D);
end;


procedure TevDBAccessProxy.DisableSecurity;
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.EnableSecurity;
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.SetChangedByNbr(const AValue: Integer);
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.DropDB(const ADBName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_DROPDB);
  D.Params.AddValue('ADBName', ADBName);
  D := SendRequest(D);
end;

procedure TevDBAccessProxy.ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_APPLYDATASYNCPACKET);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('AData', AData);
  D := SendRequest(D);
end;

procedure TevDBAccessProxy.CheckDBHash(const ADBType: TevDBType;
  const AData: IisStream; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_CHECKDBHASH);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('AData', AData);
  D.Params.AddValue('ARequiredLastTransactionNbr', ARequiredLastTransactionNbr);
  D := SendRequest(D);
  AErrors := D.Params.Value['AErrors'];
end;

function TevDBAccessProxy.GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETDATASYNCPACKET);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('AStartTransNbr', AStartTransNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
  AEndTransNbr := D.Params.Value['AEndTransNbr'];
end;

function TevDBAccessProxy.GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETDBHASH);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('ARequiredLastTransactionNbr', ARequiredLastTransactionNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;

procedure TevDBAccessProxy.SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
begin
  DisabledDBs.SetDBMaintenance(ctx_DomainInfo.DomainName, aDBName, aMaintenance);
end;

function TevDBAccessProxy.GetADRContext: Boolean;
begin
  Result := FADRContext;
end;

procedure TevDBAccessProxy.SetADRContext(const AValue: Boolean);
begin
  if FADRContext <> AValue then
  begin
    if AValue then
      CheckCondition(IsADRUser(Context.UserAccount.User), 'Not ADR account');
    ErrorIf(InTransaction, 'Transaction is active');

    FADRContext := AValue;
  end;  
end;

function TevDBAccessProxy.InTransaction: Boolean;
begin
  Result := FTranCount > 0;
end;

procedure TevDBAccessProxy.DisableDBs(const ADBList: IisStringList);
begin
  DisabledDBs.DisableDBs(ctx_DomainInfo.DomainName, ADBList);
end;

procedure TevDBAccessProxy.EnableDBs(const ADBList: IisStringList);
begin
  DisabledDBs.EnableDBs(ctx_DomainInfo.DomainName, ADBList);
end;

procedure TevDBAccessProxy.ApplyDataChangePacket(const APacket: IevDataChangePacket);
var
  Op: IisListOfValues;
  ChangePacket: IevDataChangePacket;

  procedure AddPacket;
  begin
    Op := AddLazyOperation('ApplyDataChangePacket');
    Op.AddValue('APacket', APacket);
  end;
begin
  if APacket.Count > 0 then
  begin
    if (FParams.Count > 0) and (FParams[FParams.Count - 1].Value['Method'] = 'ApplyDataChangePacket') then
    begin
      ChangePacket := IInterface(FParams[FParams.Count - 1].Value['APacket']) as IevDataChangePacket;
      if DateOf(APacket.AsOfDate) = DateOf(ChangePacket.AsOfDate) then
        ChangePacket.MergePacket(APacket)
      else
        AddPacket;
    end
    else
      AddPacket;
  end;
end;

procedure TevDBAccessProxy.SweepDB(const ADBName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_SWEEPDB);
  D.Params.AddValue('ADBName', ADBName);
  D := SendRequest(D);
end;

function TevDBAccessProxy.GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETTRANSSTATUS);
  D.Params.AddValue('ADBType', ADBType);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevDBAccessProxy.GetChangedByNbr: Integer;
begin
  NotRemotableMethod;
  Result := 0;
end;

procedure TevDBAccessProxy.DisableReadTransaction;
begin
// Aleksey: This call should be ignored on client side
//  NotRemotableMethod;
end;

procedure TevDBAccessProxy.EnableReadTransaction;
begin
// Aleksey: This call should be ignored on client side
//  NotRemotableMethod;
end;

procedure TevDBAccessProxy.BeginRebuildAllClients;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_BEGINREBUILDALLCLIENTS);
  D := SendRequest(D);
end;

procedure TevDBAccessProxy.EndRebuildAllClients;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_ENDREBUILDALLCLIENTS);
  D := SendRequest(D);
end;
                          
procedure TevDBAccessProxy.LockTableInTransaction(const aTable, aCondition: String);
begin
  NotRemotableMethod;
end;

function TevDBAccessProxy.GetFieldValues(const ATable, AField: String;
  const ANbr: Integer; const AsOfDate, AEndDate: TDateTime; const AMultiColumn: Boolean): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETFIELDVALUES);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('AField', AField);
  D.Params.AddValue('ANbr', ANbr);
  D.Params.AddValue('AsOfDate', AsOfDate);
  D.Params.AddValue('AEndDate', AEndDate);
  D.Params.AddValue('AMultiColumn', AMultiColumn);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevDBAccessProxy.GetFieldVersions(const ATable: String; const AFields: TisCommaDilimitedString;
  const ANbr: Integer; const ASettings: IisListOfValues): IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETFIELDVERSIONS);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('AFields', AFields);
  D.Params.AddValue('ANbr', ANbr);
  D.Params.AddValue('ASettings', ASettings);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataSet;
end;

procedure TevDBAccessProxy.UpdateFieldVersion(const ATable: String; const ANbr: Integer;
  const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TDateTime; const AFieldsAndValues: IisListOfValues);
var
  D: IevDatagram;
  Params: IisListOfValues;
begin
  if InTransaction then
    Params := TisListOfValues.Create
  else
  begin
    D := CreateDatagram(METHOD_DB_ACCESS_UPDATEFIELDVERSION);
    Params := D.Params;
  end;

  Params.AddValue('ATable', ATable);
  Params.AddValue('ANbr', ANbr);
  Params.AddValue('AOldBeginDate', AOldBeginDate);
  Params.AddValue('AOldEndDate', AOldEndDate);
  Params.AddValue('ANewBeginDate', ANewBeginDate);
  Params.AddValue('ANewEndDate', ANewEndDate);
  Params.AddValue('AFieldsAndValues', AFieldsAndValues);

  if InTransaction then
    RunInTransaction('UpdateFieldVersion', Params)
  else
    D := SendRequest(D);
end;

function TevDBAccessProxy.SyncDBAndApp(const ADBName: String): Boolean;
begin
  NotRemotableMethod;
  Result := False;
end;

function TevDBAccessProxy.GetDBServerFolderContent(const APath, AUser, APassword: String): IevDataSet;
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.ImportDatabase(const ADBPath, ADBPassword: String);
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.SyncUDFAndApp;
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
begin
  NotRemotableMethod;
end;

function TevDBAccessProxy.GetDBFilesList: IevDataSet;
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.TidyUpDBFolders;
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
begin
  NotRemotableMethod;
end;

function TevDBAccessProxy.FullTTRebuildInProgress: Boolean;
begin
  Result := False;
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.UpdateFieldValue(const ATable, AField: String; const ANbr: Integer;
  const ABeginDate, AEndDate: TDateTime; const AValue: Variant);
var
  D: IevDatagram;
  Params: IisListOfValues;
begin
  if InTransaction then
    Params := TisListOfValues.Create
  else
  begin
    D := CreateDatagram(METHOD_DB_ACCESS_UPDATEFIELDVALUE);
    Params := D.Params;
  end;

  Params.AddValue('ATable', ATable);
  Params.AddValue('AField', AField);
  Params.AddValue('ANbr', ANbr);
  Params.AddValue('ABeginDate', ABeginDate);
  Params.AddValue('AEndDate', AEndDate);
  Params.AddValue('AValue', AValue);

  if InTransaction then
    RunInTransaction('UpdateFieldValue', Params)
  else
    D := SendRequest(D);
end;

function TevDBAccessProxy.GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETVERSIONFIELDAUDIT);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('AField', AField);
  D.Params.AddValue('ANbr', ANbr);
  D.Params.AddValue('ABeginDate', ABeginDate);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevDBAccessProxy.ChangeFBAdminPassword(const APassword, ANewPassword: String);
begin
  NotRemotableMethod;
end;

procedure TevDBAccessProxy.ChangeFBUserPassword(const AAdminPassword, ANewPassword: String);
begin
  NotRemotableMethod;
end;

function TevDBAccessProxy.GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETBLOBAUDITDATA);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('AField', AField);
  D.Params.AddValue('ARefValue', ARefValue);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;

function TevDBAccessProxy.GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString;
  const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETRECORDAUDIT);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('AFields', AFields);
  D.Params.AddValue('ANbr', ANbr);
  D.Params.AddValue('ABeginDate', ABeginDate);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataSet;
end;

function TevDBAccessProxy.GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETTABLEAUDIT);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('ABeginDate', ABeginDate);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataSet;
end;

function TevDBAccessProxy.GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETDATABASEAUDIT);
  D.Params.AddValue('ADBType', ADBType);
  D.Params.AddValue('ABeginDate', ABeginDate);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataSet;
end;

function TevDBAccessProxy.GetDBServersInfo: IisParamsCollection;
begin
  NotRemotableMethod;
end;

function TevDBAccessProxy.GetInitialCreationNBRAudit(
  const ATables: TisCommaDilimitedString): IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_GETINITIALCREATIONNBRAUDIT);
  D.Params.AddValue('ATables', ATables);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataSet;
end;

procedure TevDBAccessProxy.UndeleteClientDB(const AClientID: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_UNDELETECLIENTDB);
  D.Params.AddValue('AClientID', AClientID);
  D := SendRequest(D);
end;

function TevDBAccessProxy.PatchDB(const ADBName, AAppVersion: String; AllowDowngrade: Boolean): Boolean;
begin
  NotRemotableMethod;
  Result := False;
end;

function TevDBAccessProxy.CustomScript(const ADBName,
  AScript: String): Boolean;
begin
  NotRemotableMethod;
  Result := False;
end;


function TevDBAccessProxy.GetVersionFieldAuditChange(const ATable,
  AField: String; {const ANbr: Integer;}
  const ABeginDate: TDateTime): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DB_ACCESS_GETVERSIONFIELDAUDITCHANGE);
  D.Params.AddValue('ATable', ATable);
  D.Params.AddValue('AField', AField);
//  D.Params.AddValue('ANbr', ANbr);
  D.Params.AddValue('ABeginDate', ABeginDate);

  D := SendRequest(D);

  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataset;
end;

{ TevDisabledDBList }

procedure TevDisabledDBList.DisableDBs(const ADomain: String; const ADBList: IisStringList);
var
  i: Integer;
  L: IisStringList;
  sDomainID: String;
begin
  Lock;
  try
    if ctx_DBAccess.GetADRContext then
      sDomainID := ctx_DomainInfo.DomainName + '-ADR'
    else
      sDomainID := ctx_DomainInfo.DomainName;

    if ADBList.IndexOf('*') <> -1 then
    begin
      L := TisStringList.Create;
      L.Add('*');
    end
    else
    begin
      L := GetDisabledDBs(sDomainID);
      L.CaseSensitive := False;
      L.Sorted := True;

      for i := 0 to ADBList.Count - 1 do
        if L.IndexOf(ADBList[i]) = -1 then
          L.Add(ADBList[i]);
    end;

    AddValue(sDomainID, L);
  finally
    UnLock;
  end;
end;

procedure TevDisabledDBList.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

procedure TevDisabledDBList.EnableDBs(const ADomain: String; const ADBList: IisStringList);
var
  L: IisStringList;
  i, j: Integer;
  sDomainID: String;
begin
  Lock;
  try
    if ctx_DBAccess.GetADRContext then
      sDomainID := ctx_DomainInfo.DomainName + '-ADR'
    else
      sDomainID := ctx_DomainInfo.DomainName;

    if ADBList.IndexOf('*') <> -1 then
      L := TisStringList.Create
    else
    begin
      L := GetDisabledDBs(sDomainID);
      L.CaseSensitive := False;
      L.Sorted := True;

      for i := 0 to ADBList.Count - 1 do
      begin
        j := L.IndexOf(ADBList[i]);
        if j <> -1 then
          L.Delete(j);
      end;
    end;

    AddValue(sDomainID, L);
  finally
    UnLock;
  end;
end;

function TevDisabledDBList.GetDisabledDBs(const ADomain: String): IisStringList;
var
  V: IisNamedValue;
begin
  Result := TisStringList.Create;

  Lock;
  try
    V := FindValue(ADomain);
    if Assigned(V) then
      Result.Text := (IInterface(V.Value) as IisStringList).Text;
  finally
    UnLock;
  end;
end;

procedure TevDisabledDBList.SetDBMaintenance(const ADomain: String; const aDBName: String; const aMaintenance: Boolean);
var
  L: IisStringList;
begin
  L := TisStringList.Create;
  L.Add(aDBName);
  if aMaintenance then
    DisableDBs(ADomain, L)
  else
    EnableDBs(ADomain, L);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetDBAccess, IevDBAccess, 'DB Access');
  DisabledDBs := TevDisabledDBList.Create;

end.
