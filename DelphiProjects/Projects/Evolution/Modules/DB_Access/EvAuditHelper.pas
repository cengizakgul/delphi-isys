unit EvAuditHelper;

interface
uses
  Windows, Messages, Forms, Classes, db, StrUtils, isBaseClasses, Math
  , EvCommonInterfaces
  , EvTypes, sysutils
  , isBasicUtils
  , Variants, EvStreamUtils
  , EvUtils
  , EvBasicUtils
  , DateUtils, ISBasicClasses, EvExceptions
  , EvDataSet
  , isDataSet
  , EvFieldValueToText
  , SEncryptionRoutines
  , rwCustomDataDictionary
  , EvSQLite
  , EvConsts
  , SDDStreamClasses
  , SDDClasses
  , SDataStructure
;
const
  CHUNKSIZEFORAUDIT = 10000;

type
  TRecFieldNumbersForAudit = record
    DbName: string;
    TableName: string;
    FieldName: string;
    FieldNumber: integer;
    EffDateFieldNumber: integer;
    EffUntilFieldNumber: integer;
    TableNbr: integer;
  end;

procedure SetDataIndex(const AIndexName: String; Current_nbr:integer; var Data:IevDataSet);

procedure ClearCurrentNbrEmptyData(bOneNbrMode:boolean; Current_nbr:integer;
                                   AField, ATable:string;
                                   var Res: IisListOfValues
                                  );
procedure PopulateDataForMultipleNbrs(ATable, AField:string;  ABeginDate, AEndDate:TDateTime;  var Changes, Data:IevDataSet);

procedure AddDataToResult(const AChangeTime: TDateTime; const AUserId: Integer;
                          const AUserName:string;
                          current_nbr: integer;
                          ResultDSFldsInfo:array of TDSFieldDef;
                          var Data:IevDataSet;
                          var Res: IisListOfValues
                         );

procedure SetResultDSFldsInfo(  Data:IevDataset;
                                var ResultDSFldsInfo: array of TDSFieldDef);

procedure SaveDatasetAsCSV(Dataset:TDataset;FileName:string);

function GetFieldNumbersInRecForAudit(const TableName,FieldName: string): TRecFieldNumbersForAudit;
function GetVersionFieldAuditChangeForList( const AuditRecord: TRecFieldNumbersForAudit;   const ABeginDate: TDateTime ): IevDataset;

implementation
uses
    SDBAccessMod;

procedure ClearCurrentNbrEmptyData(bOneNbrMode:boolean; Current_nbr:integer;
                                   AField, ATable:string;
                                   var Res: IisListOfValues
                                  );
var
  i:integer;
begin
    if not bOneNbrMode and (current_nbr <> 0) then
    begin
      // We do not need current state if there are no changes
      i := Res.IndexOf(IntToStr(current_nbr));
      if (IInterface(Res[i].Value) as IisParamsCollection).Count <= 1 then
        Res.Delete(i);
    end;
end;

procedure PopulateDataForMultipleNbrs(ATable, AField:string;  ABeginDate, AEndDate:TDateTime;  var  Changes, Data:IevDataSet);
const
  LIMIT_FOR_IN = 800;
var
  sCond: String;
  L: IisIntegerList;
  Als: TStringlist;
  j: integer;
  q2: IevQuery;
  jLim: integer;
  L2: IisIntegerList;
  jField:integer;
  sql: string;
  Q: IevQuery;
begin
  if ABeginDate > EncodeDate(1997, 1, 1) then   // Before this date changes were impossible as Evo did not exist
  begin
    L := TisIntegerList.Create;
    Changes.IndexFieldNames := 'rec_nbr:D';
    Changes.First;
// 102176 - split single query with segmented IN case into multiple queries
{     // this is bit of original code, producing multiple numbers in the list
    // we commented it out, because Firebird ignore multiples in list
    // and replaced it with unique-number-list
    // to ensure smooth data retrieval, in multiple chunks, if list longer than 800

    while not Changes.Eof do
    begin
      //L.Add(Changes.Fields[5].AsInteger);
      L.Add(Changes.FieldByName('REC_NBR').AsInteger);
      Changes.Next;
    end;
}
    Als := TStringList.Create;
    try

      Als.Sorted := True;
      Als.Duplicates := dupIgnore;
      while not Changes.Eof do
      begin
        Als.Add(Format('%.9d',[Changes.Fields[5].AsInteger]));
        Changes.Next;
      end;

      for j:= 0 to Als.Count-1 do
      begin
          if Length(Als[j]) > 0 then
          begin
            L.Add(StrToInt(Als[j]));
          end;
      end;

      // create empty dataset for consequent population
      sql := Format('SELECT rec_version, effective_date, effective_until, %s val, %s_nbr nbr FROM %s WHERE %s', [AField, ATable, ATable, '1=0']);
      Q := TevQuery.Create(sql);
      Q.Execute;
      Data := Q.Result;

      L2 := TisIntegerList.Create;
      for jLim := 0 to (L.Count div LIMIT_FOR_IN) do
      begin
         j := 0;
         L2.Clear;
         while (j < LIMIT_FOR_IN) and (LIMIT_FOR_IN * jLim + j < l.Count) do
         begin
               L2.Add(L[LIMIT_FOR_IN * jLim + j]);
               j := j + 1;
         end;
         sCond := BuildINsqlStatement(ATable + '_nbr', L2);

         sql := Format('SELECT xx.rec_version, xx.effective_date, xx.effective_until, xx.%s val, xx.%s_nbr nbr FROM %s xx WHERE ', [AField, ATable, ATable]);

         if AEndDate > 0.1 then // end  date defined
         begin
            sql := sql + ' xx.EFFECTIVE_DATE <= ''' + DateToUTC(AEndDate) + ''''; //''' AND ' ;
         end;

         if Length(Trim(sCond)) > 0 then
             sql := sql + ' AND ' + sCond
         else // empty dataset requested - should return zero rows -106129 ticket
            sql := sql + ' AND 0=1';


         Q2 := TevQuery.Create(sql);

         Q2.Execute;
         while NOT Q2.Result.EOF do begin
           Data.Insert;
           for jField := 0 to 4 do
             Data.Fields[jField].Value := Q2.Result.Fields[jField].Value;
           Data.Post;
           Q2.Result.Next;
         end;
         Q2 := nil;
      end;
    finally
       Als.Free;
    end;
  end
  else
  begin
    sCond := '1 = 1';
    sql := Format('SELECT rec_version, effective_date, effective_until, %s val, %s_nbr nbr FROM %s WHERE %s', [AField, ATable, ATable, sCond]);
    Q := TevQuery.Create(sql);
    Q.Execute;
    Data := Q.Result;
  end;
end;

procedure SetDataIndex(const AIndexName: String; Current_nbr:integer; var Data:IevDataSet);
var
  s: String;
begin
  s := 'nbr=' + IntToStr(current_nbr);
  if (Data.vclDataSet.IndexName <> AIndexName) or (Data.Filter <> s) then
  begin
    Data.vclDataSet.IndexName := AIndexName;
    Data.Filtered := False;
    Data.Filter := s;
    Data.Filtered := True;
  end;
end;

procedure AddDataToResult(
                          const AChangeTime: TDateTime;
                          const AUserId: Integer;
                          const AUserName:string;
                          current_nbr: integer;
                          ResultDSFldsInfo:array of TDSFieldDef;
                          var Data:IevDataSet;
                          var Res: IisListOfValues
                         );
var
  Item: IisListOfValues;
  FinalData: IevDataSet;
  val: Variant;
  d: TDateTime;
  LV: IisNamedValue;
  NbrData: IisParamsCollection;

begin
  FinalData := TevDataSet.Create(ResultDSFldsInfo);
  FinalData.vclDataSet.LogChanges := False;

  // Keep only unique values
  SetDataIndex('effective_date', Current_nbr, Data);

  Data.First;
  val := Null;
  while not Data.Eof do
  begin
    if (Data.RecNo = 1) or not VarSameValue(val, Data.Fields[3].Value) then
    begin
      val := Data.Fields[3].Value;
      FinalData.AppendRecord([Data.Fields[4].AsInteger, Data.Fields[1].AsDateTime, Data.Fields[2].AsDateTime, val]);
    end
    else if Data.RecNo = Data.RecordCount then
    begin
      FinalData.Last;
      FinalData.Edit;
      FinalData.Fields[2].AsDateTime := Data.Fields[2].AsDateTime;  // EndDateInd
      FinalData.Post;
    end;

    Data.Next;
  end;

  // Reconstruct effective_until field
  FinalData.Last;
  d := FinalData.Fields[2].AsDateTime; //EndDateInd
  while not FinalData.Bof do
  begin
    FinalData.Edit;
    FinalData.Fields[2].AsDateTime := IncDay(d, -1); // EndDateInd
    FinalData.Post;
    d := FinalData.Fields[1].AsDateTime; // BeginDateInd

    FinalData.Prior;
  end;

  // Skip a duplicate
  LV := Res.FindValue(IntToStr(current_nbr));
  if not Assigned(LV) then
  begin
    NbrData := TisParamsCollection.Create;
    Res.AddValue(IntToStr(current_nbr), NbrData);
  end
  else
    NbrData := IInterface(LV.Value) as IisParamsCollection;

  if (NbrData.Count = 0) or
     (CalcMD5(FinalData.Data.RealStream) <> CalcMD5((IInterface(NbrData[NbrData.Count - 1].Value['Data']) as IevDataSet).Data.RealStream))
     then
  begin
    FinalData.First;
    Item := NbrData.AddParams(IntToStr(NbrData.Count));
    Item.AddValue('ChangeTime', AChangeTime);
    Item.AddValue('UserID', 0);
    Item.AddValue('UserName', '');
    Item.AddValue('Nbr', current_nbr);
    Item.AddValue('Data', FinalData);

    if NbrData.Count > 1 then
    begin
      Item := NbrData[NbrData.Count - 2];
      Item.AddValue('ChangeTime', IncMilliSecond(AChangeTime, 1));
      Item.AddValue('UserID', AUserId);
      Item.AddValue('UserName', AUserName);
    end;
  end;
end;

procedure SetResultDSFldsInfo(  Data:IevDataset;
                                var ResultDSFldsInfo: array of TDSFieldDef);
begin
  ResultDSFldsInfo[0].FieldName := 'nbr';
  ResultDSFldsInfo[0].DataType := ftInteger;
  ResultDSFldsInfo[0].Size := 0;
  ResultDSFldsInfo[0].Required := False;
  ResultDSFldsInfo[1].FieldName := 'begin_date';
  ResultDSFldsInfo[1].DataType := ftDate;
  ResultDSFldsInfo[1].Size := 0;
  ResultDSFldsInfo[1].Required := False;
  ResultDSFldsInfo[2].FieldName := 'end_date';
  ResultDSFldsInfo[2].DataType := ftDate;
  ResultDSFldsInfo[2].Size := 0;
  ResultDSFldsInfo[2].Required := False;
  ResultDSFldsInfo[3].FieldName := 'value';
  ResultDSFldsInfo[3].DataType := Data.Fields[3].DataType;
  ResultDSFldsInfo[3].Size := Data.Fields[3].Size;
  ResultDSFldsInfo[3].Required := False;
  ResultDSFldsInfo[4].FieldName := 'text_value';
  ResultDSFldsInfo[4].DataType := ftString;
  ResultDSFldsInfo[4].Size := 64;
  ResultDSFldsInfo[4].Required := False;
end;

procedure SaveDatasetAsCSV(Dataset:TDataset;FileName:string);
var
  FFile: Text;
  iField:integer;
  s:string;
  sCell: string;
  BookMarkSavePlace: TBookmark;
begin
  AssignFile(FFile, FileName);
  Rewrite(FFile);
  s := '';
  for iField := 0 to Dataset.FieldCount - 1 do
     s := s + Dataset.Fields[iField].FieldName + ',';
  if Length(s) > 0 then
    s := System.Copy(s, 1, Length(s)-1);
  Writeln(FFile,s);
  BookMarkSavePlace := nil;
  if Dataset.Recordcount > 1 then
     BookMarkSavePlace := Dataset.GetBookmark;
  Dataset.First;
  while NOT(Dataset.EOF) do
  begin
    s := '';
    for iField := 0 to Dataset.FieldCount - 1 do
    begin
       if Dataset.Fields[iField].isNull then
         s := s + '<null>,'
       else
       begin
         sCell := Dataset.Fields[iField].AsString;
         if (Pos(' ', sCell) > 0) OR (Pos(',', sCell) > 0) then
              s := s + '"' + sCell + '",'
         else
              s := s + sCell + ',';
       end;
    end;
    if Length(s) > 0 then
      s := System.Copy(s, 1, Length(s)-1);
    Writeln(FFile,s);
    Dataset.Next;
  end;
  CloseFile(FFile);

  if Assigned(BookMarkSavePlace) then
  begin
     Dataset.GotoBookmark(BookMarkSavePlace);
     Dataset.FreeBookmark(BookMarkSavePlace);
  end;
end;

procedure InitiatePrev(var PrevRecord: Array of Variant; dataset:IevDataset);
var i: integer;
begin
  for i:=0 to Dataset.FieldCount-1 do
    PrevRecord[i] := unassigned;
end;

procedure PopulatePrev(var PrevRecord: Array of Variant; dataset:IevDataset);
var i: integer;
begin
  for i:=0 to Dataset.FieldCount-1 do
    PrevRecord[i] := Dataset.Fields[i].Value;
end;

function ExtractPrev( PrevRecord: Array of Variant; dataset:IevDataset; FieldName: string): Variant;
var
  i: integer;
begin
  i := dataset.vclDataSet.FindField(FieldName).Index;
  Result := PrevRecord[i];
end;

function ExtractPrevAsString( PrevRecord: Array of Variant; dataset:IevDataset; FieldName: string): string;
var
  i: integer;
  V: Variant;
begin
  i := dataset.vclDataSet.FindField(FieldName).Index;
  V := PrevRecord[i];
  if V = null then
     Result := '<null>'
  else
     Result := V;
end;

function ExtractPrevAsDate( PrevRecord: Array of Variant; dataset:IevDataset; FieldName: string): TDateTime;
var
  i: integer;
  V: Variant;
begin
  i := dataset.vclDataSet.FindField(FieldName).Index;
  V := PrevRecord[i];
  if V = null then
     Result := 0.0
  else
     Result := VarToDateTime(V);
end;

function ExtractPrevRecNbr( PrevRecord: Array of Variant ): integer;
var
  V: Variant;
begin
  Result := 0;
  V := PrevRecord[5]; // rec_nbr
  if V <> unassigned then
     Result := V;
end;

function DateBorderFix( const DateValue: Variant): double;
begin
   Result := DateValue;
   if (Result < BeginOfTime)
   then
      Result := BeginOfTime;

   if (Result > EndOfTime)
   then
      Result := EndOfTime;
end;

procedure  DateBorderFieldFix( Dataset: IevDataset; FieldName:string );
var dd: double;
begin
  if NOT Dataset.FieldByName(FieldName).isNull then
  begin
    dd := Dataset.FieldByName(FieldName).AsFloat;
    if (dd < BeginOfTime) or (dd > EndOfTime) then
    begin
       Dataset.Edit;
       Dataset.FieldbyName(FieldName).AsFloat := DateBorderFix(dd);
       Dataset.Post;
    end;
  end;
end;

function GetFieldNumbersInRecForAudit(const TableName,FieldName: string): TRecFieldNumbersForAudit;
var
  Qfldnbr: IevQuery;
  TableClass : TDDTableClass;
begin
  Result.dbName := GetDBNameByTable(TableName);
  Result.TableName := TableName;
  Result.FieldName := FieldName;

  Qfldnbr := TevQuery.Create(Format('{EvFieldNbrDirect<%s,%s,%s>}',[Result.DbName ,FieldName, TableName]));
  Qfldnbr.Execute;
  Result.FieldNumber := Qfldnbr.Result.Fields[0].asInteger;
  Qfldnbr := nil; // dispose of it ...

  Qfldnbr := TevQuery.Create(Format('{EvFieldNbrDirect<%s,%s,%s>}',[Result.dbName,'EFFECTIVE_DATE',TableName]));
  Qfldnbr.Execute;
  Result.EffDateFieldNumber := Qfldnbr.Result.Fields[0].asInteger;
  Qfldnbr := nil; // dispose of it ...

  Qfldnbr := TevQuery.Create(Format('{EvFieldNbrDirect<%s,%s,%s>}',[Result.dbName,'EFFECTIVE_UNTIL',TableName]));
  Qfldnbr.Execute;
  Result.EffUntilFieldNumber := Qfldnbr.Result.Fields[0].asInteger;
  Qfldnbr := nil; // dispose of it ...

//  tbl := EvoDataDictionary.Tables.TableByName(TableName);
  TableClass := GetddTableClassByName(TableName);
  Result.TableNbr := TableClass.GetEvTableNbr();
end;

procedure SetAuditNumberSplit(iiAllRecNumbers: iEvDataset; iaNbrChunk : IisStringList); // output as list of (first_nbr, =, last_nbr) in chunk; chunk size better not exceed 10K much
var
   jLeftBorder: integer;
   recInChunk: integer;
begin
  recInChunk := 0;
  iiAllRecNumbers.VCLDataSet.First;
 // first border on start
  jLeftBorder := iiAllRecNumbers.VCLDataSet.Fields[0].AsInteger;
  while NOT iiAllRecNumbers.VCLDataSet.EOF do
  begin
    recInChunk := recInChunk + iiAllRecNumbers.Fields[1].AsInteger;
    if RecInChunk >= CHUNKSIZEFORAUDIT then // exceed limit, write end-border, and reinitialize loop
    begin
      iaNbrChunk.Add( IntToStr(jLeftBorder) + '=' + IntToStr(iiAllRecNumbers.VCLDataSet.Fields[0].AsInteger) );
      recInChunk := 0;
      iiAllRecNumbers.VCLDataSet.Next;
      if iiAllRecNumbers.VCLDataSet.EOF then // end of chunk match the end of list - happenstance
        Exit
      else
      begin
        jLeftBorder := iiAllRecNumbers.VCLDataSet.Fields[0].AsInteger;
      end;
    end
    else
      iiAllRecNumbers.VCLDataSet.Next;
  end; // there may be two equal numbers in the last row
  // check for non-completed last line - regular case, unless we have exact end; if detected, then complete
  iaNbrChunk.Add( IntToStr(jLeftBorder) + '=' +  IntToStr(iiAllRecNumbers.VCLDataSet.Fields[0].AsInteger) );
end;

function GetVersionFieldAuditChangeForListChunk( const AuditRecord: TRecFieldNumbersForAudit; const ABeginDate: TDateTime;
                                                 fk: TDataDictForeignKey;
                                                 ValueConverter: IevFieldValueToTextConverter;
                                                 jLowerNbr, jUpperNbr:integer
                                                 ): IevDataset;
const
  NbrInd = 0;
  BeginDateInd = 1;
  EndDateInd = 2;
  ValueInd = 3;
  TextValueInd = 4;
var
  ATable, AField: string;
  jFieldNbr: integer;

  Data: IevDataSet;
  Q: IevQuery;
  Changes: IevDataSet;
  sql: String;
  iaSQLite: ISQLite;

  indelpat: IevDataSet;
  changdatt: IevDataset;

  dbName:string;
  MediaRes: IevDataset;
  histex: IevDataset;
  aLostBorders: IevDataset;
  aChangesOut: IevDataset;
  aFix: IevDataset;
  afixUbas: IevDataset;
  afix2: IevDataset;

  sChange: AnsiString;
  cchangetype: AnsiChar;
  idhistory: integer;

  prevRecord: Array[0..19] of Variant;
  sNew_value: string;
  sOld_Value: string;
  ChangeMoment: TDateTime;
  oldrow: Array[0..28] of Variant;

begin
  ATable := AuditRecord.TableName;
  AField :=  AuditRecord.FieldName;
  jFieldNbr := AuditRecord.FieldNumber;
  dbName := AuditRecord.dbName;

  sql := Format('{VersionFieldChangeX<%s, %s, %s, %s, %d, %d>}', [dbName, ATable, AField, DateToUTC(ABeginDate), jLowerNbr, jUpperNbr]);

  Q := TevQuery.Create(sql);
  Q.Execute;
  Changes := Q.Result; // changes is only part of whole array, since additional limitations by numbers
  Changes.IndexFieldNames := 'REC_NBR;COMMIT_NBR;TABLE_CHANGE_NBR;CHANGE_TIME';

  iaSQLite := TSQLite.Create(':memory:');
  Changes.First;
  iaSQLite.Insert('Chang', Q.Result.vclDataSet, 0.0,'', False);

  EvAuditHelper.PopulateDataForMultipleNbrs(ATable, AField, aBeginDate, EncodeDate(9999,12,31), Changes, Data  );

  Data.First;
  // list of datt fields: rec_version, effective_date, effective_until,val,nbr
  iaSQLite.Insert('datt', Data.vclDataSet, 0.0,'', False);
  changdatt := iaSQLite.Select('select Chang.*, datt.effective_date,datt.effective_until, datt.val,datt.nbr from Chang left outer join datt on Chang.rec_version=datt.rec_version and Chang.rec_nbr=datt.nbr' ,[] );
  changdatt.First;

  changdatt.first;
  iaSQLite.insert('Changdatt', changdatt.vclDataSet);

         // insert-delete empty pattern, list of record numbers to exclude from output
         indelpat :=  iaSQLite.Select(//'insert into idelpat '+
                    'select rec_version,max(rowid) as maxrowid,min(rowid) as minrowid,max(change_type) as maxtype, min(change_type) as mintype, '
                 +   ' max(effective_date) as effdatemin, min(effective_until) as effuntilmin, max(nbr) as maxnbr, count(rec_nbr) as cnt '
                 +   ' from Changdatt group by rec_version '
                 +   ' having count(rec_nbr)=2 and max(rowid)=min(rowid)+1 and max(nbr)=0 and max(change_type)=''I'' and min(change_type)=''D'''
                                 ,[] );

        iaSQLite.insert('indelpat', indelpat.vclDataSet);
        MediaRes := iaSQLite.Select( ' select * from ( '

                                 +  ' select '
                                 + ' table_name, field_name, field_nbr,  commit_nbr, table_change_nbr, rec_nbr, change_time, user_id, rec_version, change_type, field_kind, old_value,'
                                 + ' effective_date, effective_until, val, nbr, case field_kind when ''F'' then 1 when ''E'' then 2 else 0 end as field_ord '
                                 + ' from changdatt where rec_version not in (select rec_version from indelpat) ' //order by rec_nbr, rec_version, commit_nbr, table_change_nbr'
                                 + ' union all'

                                 + ' select ''' + ATable + ''' as TABLE_NAME, ''' + Afield + ''' as FIELD_NAME,' + IntToStr(jFieldNbr) + ' as field_nbr, 0 as commit_nbr, 0 as table_change_nbr,'
                                 + ' NBR as rec_nbr, cast(0.0 as datetime) as change_time, 0 as user_id, rec_version as record_version, null as change_type, null as field_kind, null as old_value, '
                                 + ' effective_date, effective_until, val, nbr, 0 as field_ord from datt where rec_version not in (select rec_version from chang) '
                                 + ' ) aa order by rec_nbr, rec_version, commit_nbr, table_change_nbr'

                                  ,[]);


// debugging for specific case

        iaSQLite.Execute('create table histex(field_nbr integer, idx integer, rec_nbr integer, rec_version integer, field_kind char(1), change_type varchar(16), change_datetime datetime, old_value varchar(255), new_value varchar(255), begin_date datetime )');
        iaSQLite.Execute('alter table histex add column end_date datetime');
        iaSQLite.Execute('alter table histex add column effective_date datetime');
        iaSQLite.Execute('alter table histex add column effective_until datetime');
        iaSQLite.Execute('alter table histex add column user_id integer');
        iaSQLite.Execute('alter table histex add column live_nbr integer');

        histex := iaSQLite.Select('select * from histex',[]);
        histex.LogChanges := False;
        MediaRes.Last;
        idHistory:= 0;

        while NOT MediaRes.BOF do // rewind from the end, restoring new values to the old
        begin
          Changemoment := BeginOfTime;
          if MediaRes.FieldbyName('change_time').AsFloat > BeginOfTime then
            ChangeMoment := MediaRes.FieldbyName('change_time').AsFloat
          else
          begin
             if MediaRes.FieldbyName('nbr').AsInteger > 0 then
               ChangeMoment := MediaRes.FieldbyName('effective_date').Asfloat;
          end;

          sChange :=MediaRes.FieldByName('change_type').AsString;
          if Length(sChange) = 0 then
            sChange := ' '; // historic records without traces of change
          cchangetype := sChange[1];

          if MediaRes.FieldByName('rec_nbr').AsInteger <> ExtractPrevRecNbr(PrevRecord) then
               InitiatePrev(PrevRecord, MediaRes);

          if MediaRes.FieldByName('NBR').asInteger = 0 then // orphaned record - new value may be lost; no traces of this version left
          begin
             case cchangetype of
              'D':begin
                    if MediaRes.FieldByName('field_kind').AsString = 'E' then
                    begin // do nothing, just populate previous record
                    end;
                    if (MediaRes.FieldByName('field_kind').AsString = 'F') then
                    begin
                        if // next record is data field with moment of creation
                           (ExtractPrevAsString( PrevRecord, MediaRes, 'field_kind') = 'E' )
                           and
                           (ExtractPrev(PrevRecord,MediaRes,'rec_version') = MediaRes.FieldByname('rec_version').AsInteger)
                        then
                        begin
                             idHistory := idHistory + 1; // display of deleted record without modern presentation
                             sOld_Value := MediaRes.FieldByName('old_value').AsString;
                             histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('REC_NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                                 'F', 'D', MediaRes.FieldByName('CHANGE_TIME').AsFloat, sOld_value, null, Double(ExtractPrevAsDate( PrevRecord, MediaRes, 'old_value'))
                                                 , MediaRes.FieldByName('CHANGE_TIME').AsFloat
                                                 ,BeginOfTime*1.0, MediaRes.FieldByName('CHANGE_TIME').AsFloat, MediaRes.FieldByname('user_id').AsInteger
                                                 ,MediaRes.FieldByname('nbr').AsInteger
                                                 ]);
                        end
                        else // isolated deletion of record that existed always
                        begin
                             idHistory := idHistory + 1;
                             sOld_Value := MediaRes.FieldByName('old_value').AsString;
                             histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('REC_NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                                 'F', 'D', MediaRes.FieldByName('CHANGE_TIME').AsFloat, sOld_value, null, BeginOfTime
                                                 , MediaRes.FieldByName('CHANGE_TIME').AsFloat
                                                 ,BeginOfTime*1.0, MediaRes.FieldByName('CHANGE_TIME').AsFloat, MediaRes.FieldByname('user_id').AsInteger
                                                 ,MediaRes.FieldByname('nbr').AsInteger
                                                 ]);

                        end;
                    end;
                  end; // end of Orphaned Delete
              'I':begin // insert of long-begone version (orphaned version)
                     if (ExtractPrev(PrevRecord,MediaRes,'rec_version') = MediaRes.FieldByname('rec_version').AsInteger)
                         and
                        (
                           (ExtractPrevAsString(PrevRecord,MediaRes,'field_kind') = 'F')
                        )
                     then
                     begin // insert with alterations
                       idHistory := idHistory + 1;
                       sNew_value := ExtractPrevAsString( PrevRecord, MediaRes, 'old_value');
                         histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('REC_NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                             'F', 'I', MediaRes.FieldByName('CHANGE_TIME').AsFloat, null, sNew_value, MediaRes.FieldByName('CHANGE_TIME').AsFloat
                                             , EndOfTime*1.0
                                             ,MediaRes.FieldByName('CHANGE_TIME').AsFloat, EndOfTime, MediaRes.FieldByname('user_id').AsInteger
                                             ,MediaRes.FieldByname('nbr').AsInteger
                                             ]);
                     end
                     else  // isolated insert
                     begin
                         idHistory := idHistory + 1;
                         histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('REC_NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                             'F', 'I', MediaRes.FieldByName('CHANGE_TIME').AsFloat, null, null, MediaRes.FieldByName('CHANGE_TIME').AsFloat
                                             , EndOfTime*1.0
                                             ,MediaRes.FieldByName('CHANGE_TIME').AsFloat, EndOfTime, MediaRes.FieldByname('user_id').AsInteger
                                             ,MediaRes.FieldByname('nbr').AsInteger
                                             ]);
                     end;
                  end;
              'U':begin // update of long-begone version (orphaned version)
                    if MediaRes.FieldByName('field_kind').AsString = 'F' then
                    begin
                       if (ExtractPrev( PrevRecord, MediaRes, 'rec_version') = MediaRes.FieldByName('rec_version').AsInteger) then
                       begin
                         if (ExtractPrevAsString(PrevRecord, MediaRes, 'change_type') = 'D')
                             and
                            (ExtractPrevAsString(PrevRecord, MediaRes, 'field_kind') <> 'F')
                             and
                            (ExtractPrev( PrevRecord, MediaRes, 'commit_nbr') = MediaRes.FieldByName('commit_nbr').AsInteger)
                         then // delete on update (recall ignored operation of deletion of effective_date field) , so it is delete operation, not update
                         begin
                            sNew_Value := '<null>';
                            idHistory := idHistory + 1;
                            histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('REC_NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                               'F', 'D', MediaRes.FieldByName('CHANGE_TIME').AsFloat, MediaRes.FieldByName('old_value').AsString,sNew_value, BeginOftime
                                               , changeMoment
                                               ,BeginOfTime*1.0, changeMoment, MediaRes.FieldByname('user_id').AsInteger
                                               ,MediaRes.FieldByname('nbr').AsInteger
                                               ]);
                         end
                         else
                         begin  // regular orphaned update
                           sNew_value := ExtractPrevAsString( PrevRecord, MediaRes, 'old_value');
                           idHistory := idHistory + 1;
                           histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('REC_NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                               'F', 'U', MediaRes.FieldByName('CHANGE_TIME').AsFloat, MediaRes.FieldByName('old_value').AsString,sNew_value, MediaRes.FieldByName('CHANGE_TIME').AsFloat
                                               , EndOfTime*1.0
                                               ,MediaRes.FieldByName('CHANGE_TIME').AsFloat, EndOfTime, MediaRes.FieldByname('user_id').AsInteger
                                               ,MediaRes.FieldByname('nbr').AsInteger
                                               ]);
                         end;
                       end;
                    end;
                  end;
             end;
          end
          else  // we have information on new value
          begin
             case cchangetype of
              'I': begin// once-creation of current value
                     idHistory := idHistory + 1;
                     histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                         'F', 'I', MediaRes.FieldByName('CHANGE_TIME').AsFloat,null,MediaRes.FieldByName('VAL').AsString, BeginOfTime*1.0, EndOfTime*1.0
                                         ,MediaRes.FieldByName('effective_date').AsFloat, MediaRes.FieldByName('effective_until').AsFloat,  MediaRes.FieldByname('user_id').AsInteger
                                         ,MediaRes.FieldByname('nbr').AsInteger
                                         ]);
                   end;
              ' ': begin

                      // old value not registered; it is to be taken from previous record
                      if ExtractPrevRecNbr(PrevRecord) = MediaRes.Fields[5].AsInteger then //rec_nbr is the same
                      begin
                        sNew_value := ExtractPrevAsString( PrevRecord, MediaRes, 'val');
                        if MediaRes.FieldbyName( 'Effective_Until').AsFloat = Double(ExtractPrevAsDate( PrevRecord, MediaRes, 'effective_date')) then
                          ChangeMoment := MediaRes.FieldbyName( 'Effective_Until').AsFloat
                        else
                          ChangeMoment :=  MediaRes.FieldbyName( 'change_time').AsFloat;
                      end
                      else
                      begin
                        snew_value := MediaRes.FieldByName('VAL').AsString;
                      end;
                      idHistory := idHistory + 1;
                      histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                         'F', 'U', ChangeMoment,MediaRes.FieldByName('VAL').AsString, sNew_Value, BeginOfTime*1.0, EndOfTime*1.0
                                         ,MediaRes.FieldByName('effective_date').AsFloat, MediaRes.FieldByName('effective_until').AsFloat,  MediaRes.FieldByname('user_id').AsInteger
                                         ,MediaRes.FieldByname('nbr').AsInteger
                                         ]);
                   end;
              'U': begin // update  of modern-present version
                        if MediaRes.FieldByName('Field_kind').AsString = 'U' then // until-end changes
                        begin
                          idHistory := idHistory + 1;
                          histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                             'U', 'U', ChangeMoment,MediaRes.FieldByName('VAL').AsString, MediaRes.FieldByName('VAL').AsString, ChangeMoment, EndOfTime*1.0
                                             ,MediaRes.FieldByName('effective_date').AsFloat, MediaRes.FieldByName('effective_until').AsFloat,  MediaRes.FieldByname('user_id').AsInteger
                                             ,MediaRes.FieldByname('nbr').AsInteger
                                             ]);
                        end;
                        if MediaRes.FieldByName('Field_kind').AsString = 'E' then
                        begin
                          idHistory := idHistory + 1;
                          histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                             'U', 'E', ChangeMoment,MediaRes.FieldByName('VAL').AsString, MediaRes.FieldByName('VAL').AsString, ChangeMoment, EndOfTime*1.0
                                             ,MediaRes.FieldByName('effective_date').AsFloat, MediaRes.FieldByName('effective_until').AsFloat,  MediaRes.FieldByname('user_id').AsInteger
                                             ,MediaRes.FieldByname('nbr').AsInteger
                                             ]);
                        end;
                        if MediaRes.FieldByName('Field_kind').AsString = 'F' then
                        begin
                          idHistory := idHistory + 1;
                          histex.AppendRecord([jFieldNbr, idHistory, MediaRes.FieldbyName('NBR').AsInteger, MediaRes.FieldByName('REC_VERSION').AsInteger,
                                             'U', 'E', ChangeMoment,MediaRes.FieldByName('VAL').AsString, MediaRes.FieldByName('VAL').AsString, ChangeMoment, EndOfTime*1.0
                                             ,MediaRes.FieldByName('effective_date').AsFloat, MediaRes.FieldByName('effective_until').AsFloat,  MediaRes.FieldByname('user_id').AsInteger
                                             ,MediaRes.FieldByname('nbr').AsInteger
                                             ]);
                        end;
                   end;
              end;
          end;
          PopulatePrev(PrevRecord, MediaRes);
          MediaRes.Prior;
        end;
        MediaRes.First;

        histex.First;

     iaSQLite.insert('histex', histex.vclDataSet);
        // we are looking for rec_versions having both insert and delete
        // and not existing any more, to set proper visibility borders for them
     aLostBorders := iaSQLite.Select(
        ' select xxa.rec_version, xxa.up_bord, xxa.up_idx, xxb.dwn_bord, xxb.dwn_idx '
      + ' from ( '
          + ' select a.rec_version, max(a.change_datetime) as up_bord, min(a.idx) as up_idx '
          + ' from histex a '
          + ' where a.live_nbr=0 and a.change_type=''D'''
          + ' group by a.rec_version '
          + '  ) xxa '
          + ' inner join '
          + ' ( '
          + ' select a.rec_version, min(a.change_datetime) as dwn_bord, min(a.idx) as dwn_idx '
          + ' from histex a '
          + ' where a.live_nbr=0 and a.change_type=''I'' '
          + ' group by a.rec_version '
          + ' ) xxb '
          + '    on xxa.rec_version=xxb.rec_version '

                                    ,[]);

  iaSQLite.insert('brd', aLostBorders.vclDataSet);

  // list of changes merged with modern rec_versions is extended by visibility borders
  aChangesOut := iaSQLite.Select(
         ' select a.idx, a.field_nbr, a.rec_nbr, a.rec_version, a.field_kind, a.change_type '
       + ', a.change_datetime, a.old_value, a.new_value, a.user_id, a.live_nbr '
       + ', a.begin_date, a.end_date '
       + ', a.effective_date, a.effective_until, cast('''' as varchar(255)) as old_text, cast('''' as varchar(255)) as new_text ' // no limiting of size in DisQLite

       + ', b.up_bord, b.dwn_bord'
       + ' from '
              + ' histex a '
       + ' left outer join '
       + '     brd b '
       + ' on a.rec_version=b.rec_version '
       + ' and a.idx >= b.up_idx and a.idx <= b.dwn_idx '
                             ,[]);

   // now we set correct begin-date and end-date for changes in rec_versions that were gone
   // so we will see correct timeline if we run query on aChangesOut dataset, asking for changes visible at any specific moment of time
   // something went wrong with assignment of float data in DisQlite, so borders assigned here

   // two last fields in output dataset are for internal processing, and will not be transferred to final output
  while NOT aChangesOut.EOF do
  begin
     if NOT aChangesOut.FieldbyName('up_bord').IsNull then
       if (aChangesOut.FieldByName('begin_date').AsInteger <= BeginOfTime)
           and
          (Length(aChangesOut.FieldbyName('dwn_bord').AsString) > 0)
       then
       begin
         aChangesOut.Edit;
         aChangesOut.FieldByName('begin_date').AsFloat := aChangesOut.FieldbyName('dwn_bord').AsFloat;
         aChangesOut.Post;
       end;
     if NOT aChangesOut.FieldbyName('dwn_bord').IsNull then
       if (aChangesOut.FieldByName('end_date').AsInteger >= EndOfTime)
           and
          (Length(aChangesOut.FieldbyName('up_bord').AsString) > 0)
       then
       begin
         aChangesOut.Edit;
         aChangesOut.FieldByName('end_date').AsFloat := aChangesOut.FieldbyName('up_bord').AsFloat;
         aChangesOut.Post;
       end;

     if Assigned(ValueConverter)
        and
        (
          (Length(aChangesOut.FieldByName('old_value').AsString) > 0)
          or
          (Length(aChangesOut.FieldByName('new_value').AsString) > 0)
        )
     then
     begin
        aChangesOut.Edit;
        try
          aChangesOut.FieldByName('old_text').AsString :=  ValueConverter.Convert(ATable,
                                                                                  AField,
                                                                                  aChangesOut.FieldByName('old_value').AsString,
                                                                                  aChangesOut.FieldByName('change_datetime').AsInteger,
                                                                                  True
                                                                                  );
        except
          aChangesOut.FieldByName('old_text').AsString := '<' + aChangesOut.FieldByName('old_value').AsString + '>-Broken Reference'; // happened for EE.ACA_CO_BENEFIT_SUBTYPE_NBR
        end;
        try
          aChangesOut.FieldByName('new_text').AsString :=  ValueConverter.Convert(ATable,
                                                                                AField,
                                                                                aChangesOut.FieldByName('new_value').AsString,
                                                                                aChangesOut.FieldByName('change_datetime').AsInteger,
                                                                                True
                                                                                );
        except
          aChangesOut.FieldByName('new_text').AsString := '<' + aChangesOut.FieldByName('new_value').AsString + '>-Broken Reference'; // happened for EE.ACA_CO_BENEFIT_SUBTYPE_NBR
        end;

        aChangesOut.Post;
     end
     else  // if no special text presentation, then just copy values , so old-new-value go to old-new-text
     begin
        aChangesOut.Edit;
        aChangesOut.FieldByName('old_text').AsString := aChangesOut.FieldByName('old_value').AsString;
        aChangesOut.FieldByName('new_text').AsString := aChangesOut.FieldByName('new_value').AsString;
        aChangesOut.Post;
     end;
     // sometimes negative date can appear in dataset; results are disastrous, because they generally cannot be rendered by default transformation
     // here we fix them to prevent their destructive behavior downstream
     DateBorderFieldFix( AChangesOut, 'change_datetime');
     DateBorderFieldFix( AChangesOut, 'begin_date');
     DateBorderFieldFix( AChangesOut, 'end_date');
     DateBorderFieldFix( AChangesOut, 'effective_date');
     DateBorderFieldFix( AChangesOut, 'effective_until');

     aChangesOut.Next;
  end;
  aChangesOut.First;

  iaSQLite.Execute('drop table histex');
  iaSQLite.Execute('drop table brd');
  iaSQLite.Execute('drop table indelpat');

  iaSQLite.Execute('drop table datt');
  iaSQLite.Execute('drop table Chang');

  MediaRes := nil;
  histex := nil;
  aLostBorders := nil;
  iaSQLite.Insert('fixupdate',aChangesOut.vclDataset);
  iaSQLite.Execute('alter table fixupdate add column cidx integer');
  iaSQLite.Execute('update fixupdate set cidx=idx');

  // regular versioned update ( new rec_version over existing rec_nbr )
  // presented as Insert-transactions (not Update-transactions),
  // because they insert new rec-version, and without registration updating effective_until field for last value

  // new rec-version inserted and then deleted; such incidents are invisible in original report S1075 so we are just dropping them here ...

  //  aVisUpdDeletes :=
              iaSQLite.Execute(
                            ' delete '
                          + ' from fixupdate where idx in ( '
                          + ' select a.idx '
                          +' from fixupdate a inner join fixupdate b '
                          +' on b.idx=a.idx+1 and a.rec_nbr=b.rec_nbr '
                          +' inner join fixupdate c on c.idx=a.idx-1 and c.rec_nbr=a.rec_nbr and c.effective_date=b.effective_until '
                          +' where a.live_nbr=0 and a.old_value='''' and a.new_value='''' and a.effective_date=a.change_datetime '
                          +')'
                          );
  iaSQLite.Execute( ' update fixupdate set idx=rowid' );
//                          ,[]);
  //insert record merged with previous base update one;  merged:     //--update record altered
  afix := iaSQLite.Select(' select  b.idx, a.field_nbr, a.rec_nbr, b.rec_version, a.field_kind, b.change_type,  a.change_datetime, ' // change-datatime for moment of change
                         +' b.old_value, b.new_value, a.user_id,a.live_nbr, a.begin_date, a.end_date, b.effective_date, a.change_datetime as effective_until, '
                         +' b.old_text, b.new_text, b.cidx,'
                         +' b.effective_date as old_effective, b.effective_date as new_effective, cast(2958465.0 as datetime) as old_until, a.change_datetime as new_until '
                         +' from fixupdate a inner join fixupdate b on '
                         +'   b.idx=a.idx+1 and a.Change_type=''I'' '
                         +' and b.Change_type=''U'' and a.Rec_nbr=b.rec_nbr  and a.new_value=b.new_value '
                         +' and a.Field_kind=''F'' and b.Field_kind=''F'' '
                         +' and cast(b.effective_until as integer)=cast(a.change_datetime as integer)'
                         ,[]);

  iaSQLite.Insert('ubase', afix.vclDataSet); // base update lines; above them version record inserts treated as updates

  afixUbas := iaSQLite.Select(               // combined version-inserts with corrected baselines
                          ' select rowid as idxx, * from ( '
                         +' select  a.idx, a.field_nbr, a.rec_nbr, a.rec_version, a.field_kind, a.change_type,  a.change_datetime, ' // change-datatime for moment of change
                         +' a.old_value, a.new_value, a.user_id,a.live_nbr, a.begin_date, a.end_date, a.effective_date, a.effective_until, '
                         +' a.old_text, a.new_text, a.cidx,'
                         +' a.old_effective, a.new_effective, a.old_until, a.new_until '
                         +' from ubase a '                 // fixed baseline of update chain
                         +' union all '
                         +' select  b.idx, b.field_nbr, b.rec_nbr, b.rec_version, b.field_kind, b.change_type,  b.change_datetime, ' // change-datatime for moment of change
                         +' b.old_value, b.new_value, b.user_id,b.live_nbr, b.begin_date, b.end_date, b.effective_date, b.effective_until, '
                         +' b.old_text, b.new_text, b.cidx,  null as old_effective, null as new_effective, null as old_until, null as new_until '
                         +' from fixupdate b where idx not in (select idx from ubase ) '// union all select idx-1 as idx from ubase)
                                       +' )order by idx '
                         ,[]);
  afixUbas.first;
  while NOT afixUbas.EOF do
  begin
   afixUbas.Edit;
   afixUbas.FieldByName('idxx').AsInteger := afixUbas.VclDataset.RecNo;
   afixUbas.Post;
   afixUBas.next;
  end;
  afixUbas.first;
  // now populate all empty old-values and new-values on record of chained updates
  iaSQLite.Execute('drop table fixupdate');
  iaSQLite.Insert('ordupd', afixUbas.vclDataSet); // list of ordered updates with aligned effective_date, effective_until

 // list of series to correct: min idxx, max idxx

 afix2 := iaSQLite.Select(
   ' select minx, maxx, NofVer, rec_nbr from ( '
  + ' select cast(min(idxx) as integer) as minx, cast(max(idxx) as integer) as maxx, cast(count(live_nbr) as integer) as NofVer, rec_nbr '
  + ' from ordupd where (change_type =''I'' or change_type=''U'') and live_nbr>0'
  + ' group by rec_nbr '
  + ' having count(live_nbr) > 1'
  + ' ) zz order by maxx desc '
  ,[]);

  afix2.First;

  iaSQLite.Execute('drop table ordupd');

  while NOT Afix2.EOF do
  begin
    // location to oldest record in chain of events, should be either U or I

    afixUbas.first;
    afixUbas.vclDataSet.MoveBy(aFix2.FieldByname('maxx').AsInteger - 1);
    //    Assert(afixUbas.FieldbyName('idxx').AsInteger = aFix2.FieldByname('maxx').AsInteger);

       populatePrev(oldRow, afixUbas);
       afixUbas.vclDataset.Prior;
       while (aFixUbas.FieldByName('idxx').asInteger >= aFix2.FieldByname('minx').AsInteger) and NOT aFixUbas.BOF  do // here we are dealing with continious chain of updates
       begin
         if (AnsiUpperCase(afixUbas.FieldbyName('change_type').AsString) = 'I' ) or (AnsiUpperCase(afixUbas.FieldbyName('change_type').AsString) = 'U' ) then
         begin  // cycle going from oldest to newest aligning old-new values ; deletes ignored
           if aFixUbas.FieldByName('change_type').AsString='U' then // historical record are ok, no ajustment necessary here
              populatePrev(oldRow, afixUbas);
           if aFixUbas.FieldByName('change_type').AsString='I' then // it is insert-transaction of creating new rec_version record with new value
           begin

             if (Trunc(ExtractPrevAsdate(oldrow, aFixUBas, 'effective_until')) = aFixUBas.fieldByName('effective_date').AsInteger)
                 and
                 (length(Trim(aFixUBas.fieldByName('old_value').AsString)) = 0)
             then
             begin
               aFixUbas.Edit;
               if aFixUBas.fieldByName('idxx').AsInteger <> aFix2.FieldByname('maxx').AsInteger then // oldest record in chain of updates may be either I or U;
                  aFixUBas.fieldByName('change_type').AsString := 'U';                               // all the others should be marked U, even if it was new rec_version Insert
               aFixUBas.fieldByName('old_value').AsString := ExtractPrevAsString(oldrow, aFixUBas, 'new_value');
               aFixUBas.fieldByName('old_text').AsString := ExtractPrevAsString(oldrow, aFixUBas, 'new_text');
               aFixUBas.fieldByName('old_text').AsString := ExtractPrevAsString(oldrow, aFixUBas, 'new_text');
               aFixUBas.fieldByName('effective_date').AsFloat:= ExtractPrev(oldrow, aFixUBas, 'effective_until');
               aFixUBas.fieldByName('old_effective').AsFloat := aFixUBas.fieldByName('Change_datetime').AsFloat;
               aFixUBas.fieldByName('new_effective').AsFloat := aFixUBas.fieldByName('Change_datetime').AsFloat;
               aFixUBas.fieldByName('old_until').AsFloat := EndOfTime;                                          // previous default value; otherwise it would be stored separately
               aFixUBas.fieldByName('new_until').AsFloat := aFixUBas.FieldByname('effective_until').asFloat; // become actual, changing from EndofTime
               aFixUbas.Post;
             end;
             populatePrev(oldRow, afixUbas);
           end;
         end;  // i or u check
         afixUbas.vclDataset.Prior;
       end; // while of reversal moving within update batch

    aFix2.Next;
  end; // cycle by update serii
// ajust new data values to be accepted
  aFixUBas.First;
  while NOT aFixUBas.EOF do
  begin
      aFixUbas.Edit;
      if Length(aFixUBas.FieldByName('old_effective').AsString) = 0 then
          aFixUBas.FieldByName('old_effective').value := null;
      if Length(aFixUBas.FieldByName('new_effective').AsString) = 0 then
          aFixUBas.FieldByName('new_effective').value := null;
      if Length(aFixUBas.FieldByName('old_until').AsString) = 0 then
          aFixUBas.FieldByName('old_until').value := null;
      if Length(aFixUBas.FieldByName('new_until').AsString) = 0 then
          aFixUBas.FieldByName('new_until').value := null;
      aFixUBas.Post;
      aFixUBas.Next;
  end;

  aFixUBas.First;
  Result := aFixUBas;
end;

function GetVersionFieldAuditChangeForList( const AuditRecord: TRecFieldNumbersForAudit;   const ABeginDate: TDateTime ): IevDataset;
var
  ValueConverter: IevFieldValueToTextConverter;

  Q: IevQuery;
  iiAllRecNumbers: IEvDataset;
  tbl: TDataDictTable;
  fld: TDataDictField;
  fk: TDataDictForeignKey;
  sql: String;

  iaNbrChunk: IisStringList;
  jChunk: integer;
  iiChunkResult: IevDataset;
begin

  tbl := EvoDataDictionary.Tables.TableByName(AuditRecord.TableName);
  fld := tbl.Fields.FieldByName(AuditRecord.FieldName);
  fk := tbl.ForeignKeys.ForeignKeyByField(fld);
  if Assigned(fk) or (fld.FieldValues.Count > 0) then
    ValueConverter := TevFieldValueToTextConverter.Create;

// Retrieve numbers for all transactions
// now we limit output by manageable chunks
   sql := Format('{EvAuditListOfNumbers<%s,%d,%d,%d,%d,%s>}',[ AuditRecord.dbName,
                                                            AuditRecord.Fieldnumber, AuditRecord.EffDateFieldNumber, AuditRecord.EffuntilFieldNumber,
                                                            AuditRecord.TableNbr,
                                                            DateToUTC(ABeginDate)
                                                          ]);
   Q := TevQuery.Create(sql);
   Q.Execute; // here we count numbers of record in original audit response by each N
   iiAllRecNumbers := Q.Result;
   Q := nil; // release query;

   iaNbrChunk := TisStringList.Create;
   SetAuditNumberSplit(iiAllRecNumbers, iaNbrChunk);  //split audit request in chunks by 10K by providing intervals of N
   for jChunk := 0 to iaNbrChunk.Count - 1 do         // iaNbrChunk - list if pairs start_REC_NBR=end_REC_NBR
   begin
      iiChunkResult := GetVersionFieldAuditChangeForListChunk(AuditRecord, AbeginDate, fk, ValueConverter, StrToInt(iaNbrChunk.Names[jChunk]), strToint(Copy(iaNbrChunk[jChunk], pos('=', iaNbrChunk[jChunk]) + 1, Length(iaNbrChunk[jChunk]))) );
      if jChunk = 0 then
         Result := iiChunkResult
      else
      begin
         iiChunkResult.First;
         while NOT iiChunkResult.EOF do
         begin
           Result.AppendRecord([
                               iiChunkResult.Fields[0].Value, iiChunkResult.Fields[1].Value,iiChunkResult.Fields[2].Value,iiChunkResult.Fields[3].Value,iiChunkResult.Fields[4].Value,
                               iiChunkResult.Fields[5].Value,iiChunkResult.Fields[6].Value,iiChunkResult.Fields[7].Value,iiChunkResult.Fields[8].Value,iiChunkResult.Fields[9].Value,
                               iiChunkResult.Fields[10].Value,iiChunkResult.Fields[11].Value,iiChunkResult.Fields[12].Value,iiChunkResult.Fields[13].Value,iiChunkResult.Fields[14].Value,
                               iiChunkResult.Fields[15].Value,iiChunkResult.Fields[16].Value,iiChunkResult.Fields[17].Value,iiChunkResult.Fields[18].Value,iiChunkResult.Fields[19].Value,
                               iiChunkResult.Fields[20].Value, iiChunkResult.Fields[21].Value,iiChunkResult.Fields[22].Value
                               ]);
           iiChunkResult.Next;
         end;
      end;
   end;
   Result.First;
end;

end.
