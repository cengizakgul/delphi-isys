unit EvFirebird;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}
//{$DEFINE SQL_MONITOR}

interface

uses Classes, Windows, SysUtils, DB, isBaseClasses, EvCommonInterfaces, EvDataAccessComponents, EvTypes,
     SUniversalDataReader,  EvStreamUtils, IBDatabase, IBSQL, IBHeader, EvConsts, isBasicUtils,
     EvContext, SIbProviders, sSQLParser, EvQueries, EvUtils, EvDataSet, isThreadManager, IBSQLMonitor,
     isErrorUtils, EvMainboard, isSocket, SyncObjs, Variants, DateUtils, StrUtils, isObjectPool, isVCLBugFix,
     isSettings, ISZippingRoutines, IB, EvInitApp, EvExceptions, IBIntf, isTypes, isUtils, SDataStructure,
     SEncryptionRoutines, evBasicUtils, isDataSet, isMessenger, isLogFile,
     isAppIDs, EvDBVersion;

type
  IevSQL = interface
  ['{87DF45E4-AB7F-4D77-B59D-03212988E62C}']
    procedure SetParam(const AIndex: Integer; const AValue: Variant); overload;
    procedure SetParam(const AName: String; const AValue: Variant); overload;
    function  GetParam(const AIndex: Integer): Variant; overload;
    function  GetParam(const AName: String): Variant; overload;
    function  GetParamIndex(const AName: String; const AErrorIfNotEsist: Boolean = False): Integer;
    procedure SetParams(const AParams: IisListOfValues); overload;
    procedure SetParams(const AParams: array of Variant); overload;
    procedure Exec;
    function  Open(const ALoadBlobs: Boolean): IevDataSet;
  end;


  IevDBServer = interface
  ['{4F9FC19F-23BA-4466-85D9-DA7895AC407F}']
    procedure StartTransaction(const ADBTypes: TevDBTypes);
    procedure Commit;
    procedure Rollback;
    procedure FlushTransactionChanges;
    function  CreateSQL(const ASQL: String; const AMacros: IisListOfValues = nil): IevSQL;
    function  CreateRawSQL(const ASQL: String; const ADBType: TevDBType): IevSQL;
    procedure PopulateDS(const ds: TEvBasicClientDataSet; const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil; const BlobsLazyLoad: Boolean = False);
    function  OpenDS(const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil): TEvBasicClientDataSet;
    function  OpenDSEx(const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil;
                       const PkFieldName: string = ''; const BlobsLazyLoad: Boolean = False): IEvQueryResult;
    procedure ExecBatchDs(const APreSQL: string; const AUpdateSQL: TevBatchUpdateSQL; const ASequence: TevBatchUpdateSequence);
    function  ExecDS(const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil): IisListOfValues;
    function  OpenClientListDS: IevDataSet;
    procedure CreateClient(var ClNbr: Integer);
    procedure DeleteClient(const ClNbr: Integer);
    procedure OpenClient(ClNbr: Integer);
    function  OpenQuery(const ADBType: TevDBType; const ASQL: String; const AParamVals: array of Variant; const ALoadBlobs: Boolean): IevDataSet;
    function  ExecRawQuery(const ADBType: TevDBType; const ASQL: String; const AParamVals: array of Variant; const ALoadBlobs: Boolean): IevDataSet;
    function  ExecQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean): IevDataSet;
    function  GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
    procedure SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
    function  GetNewKeyValue(const ATableName: String; const AIncrement: Integer = 1): Integer;
    function  GetDBVersion(const ADBType: TevDBType): String;
    procedure SetDataSecurityActive(const AValue: Boolean);
    function  GetDataSecurityActive: Boolean;
    procedure SetUseReadTransaction(const AValue: Boolean);
    function  GetUseReadTransaction: Boolean;
    procedure SendBroadcastNotification(const AEventID, Data: String);
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
    function  BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompress: Boolean): IisStream;
    procedure RestoreDB(const ADBName: String; const AData: IisStream; const ACompressed: Boolean);
    procedure DropDB(const ADBName: String);
    procedure SweepDB(const ADBName: String);
    function  PatchDB(const aDBName: String; const aAppVersion: String; AllowDowngrade: Boolean): Boolean;
    function  CustomScriptDB(const aDBName: String; const aScript: String): Boolean;
    function  SyncDBAndApp(const ADBName: String): Boolean;
    procedure SyncUDFAndApp;
    procedure ChangeFBAdminPassword(const ANewPassword: String);
    procedure ChangeFBUserPassword(const ANewPassword: String);
    procedure TidyUpDBFolders;
    procedure ImportDatabase(const ADBPath, ADBPassword: String);
    procedure ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    procedure UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    function  GetDBServerFolderContent(const APath, AUser, APassword, AMask: String; const AFileSize: Boolean): IevDataSet;
    function  GetDBFilesList: IevDataSet;
    function  GetLastTransactionNbr(const ADBType: TevDBType): Integer;
    function  GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
    function  GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
    procedure ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
    function  GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IevDataSet;
    procedure CheckDBHash(const ADBType: TevDBType; const ADBInfo: IevDataSet; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
    function  GetChangedTablesInTransaction(const ADBType: TevDBType; const ATableNameFilter: IisStringList = nil): IevDataSet;
    procedure InitializeTTForFullRebuild;
    procedure FinalizeTTAfterFullRebuild;
    function  GetDBServersInfo: IisParamsCollection;
    function  TryToGetDBVersion(const ADBPath: String): TisVersionInfo;

    property  DataSecurityActive: Boolean read GetDataSecurityActive write SetDataSecurityActive;
    property  UseReadTransaction: Boolean read GetUseReadTransaction write SetUseReadTransaction;
  end;


function  GetFirebirdDBServer: IevDBServer;

implementation

uses Types, SDDClasses;

{$R DbScripts.RES}
{$R EvFirebird.RES}

type
  TevFieldDefs = class(TFieldDefs)
  public
    procedure Add(const Name: string; DataType: TFieldType; Size: Integer = 0); reintroduce;
    procedure BeginUpdate; override;
    procedure EndUpdate; override;
  end;

  TevTransaction = class(TIBTransaction)
  private
    FReadOnly: Boolean;
    FReadCommited: Boolean;
    procedure SetTransParameters;
    procedure SetReadOnly(const AValue: Boolean);
    procedure SetReadCommited(const AValue: Boolean);
    procedure DoCleanupModifiedFlag;
    procedure DoBeforeCommitTransaction;
    procedure DoNotifyChanges;
    procedure DoStart;
    procedure DoCommit;
    procedure DoRollback;
  public
    procedure StartIfNeeds;
    procedure AfterConstruction; override;
    property  ReadOnly: Boolean read FReadOnly write SetReadOnly;
    property  ReadCommited: Boolean read FReadCommited write SetReadCommited;
  end;

  TevCustomIBSQL = class(TIBSQL)
  private
    procedure OnChangeSQL(Sender: TObject);
  public
    procedure ExecQuery; virtual;

    constructor Create(AOwner: TComponent); override;
  end;

  TevFirebird = class;

  TevRawSQL = class(TevCustomIBSQL)
  private
    FOwner: TevFirebird;
    FFieldDefs: TevFieldDefs;
    function    AsDataSet(const ALoadBlobs: Boolean): IevDataSet;
  public
    function    GetFieldDefs: TFieldDefs;
    procedure   SetParams(const AParams: IisListOfValues; const AClearUndefined: Boolean = True); overload;
    procedure   SetParams(const AParams: array of Variant); overload;

    procedure   SetParam(const AIndex: Integer; const AValue: Variant); overload;
    procedure   SetParam(const AName: String; const AValue: Variant); overload;
    function    GetParam(const AIndex: Integer): Variant; overload;
    function    GetParam(const AName: String): Variant; overload;
    function    GetParamIndex(const AName: String; const AErrorIfNotEsist: Boolean = False): Integer;

    function    ExecAsDataSet(const ALoadBlobs: Boolean): IevDataSet; virtual;

    constructor Create(const AOwner: TevFirebird); reintroduce;
    destructor  Destroy; override;
  end;


  TevCustomDatabase = class(TIBDatabase)
  private
    FReadOnlyTrans: TevTransaction;
    FDBVersion: TisVersionInfo;
    function  GetDBVersion: TisVersionInfo;
    procedure InitDBVersion(Sender: TObject);
  public
    constructor Create; reintroduce;
    procedure   AfterConstruction; override;
    procedure   ConnectIfNeeds; virtual;
    property    DBVersion: TisVersionInfo read GetDBVersion;
  end;


  TevServiceDatabase = class(TevCustomDatabase)
  private
    FCurrentOwner: TevFirebird;
    function  GetHost: String;
    procedure SetHost(const AValue: String); overload;
    function  CheckIfValid(const ACheckVersion: Boolean): Boolean;
    procedure CreateServiceDatabase;
    procedure CopyDatabaseFile(const ASourceDB, ATargetDB: String);
    function  GetUserName: String;
    function  GetPassword: String;
    procedure ScrambleDB(const ADBPath: String);
  public
    procedure SetupConnection(const ADBPath, AUser, APassword: String); overload;
    procedure EnsureDatabaseConnection(const ACheckVersion: Boolean = True);
    function  GetUDFsVersion: Integer;
    function  CheckAndUpdateUDFs: Boolean;
    procedure PatchDatabase(const ADatabasePath, AScriptFile, AProgressCaption: String);
    procedure TidyUpDBFolder(const ADatabaseFolder: String);
    procedure DefragmentDatabase(const ADatabasePath: String);
    procedure CopyDatabase(const ASourceDatabasePath, ASrcUser, ASrcPassword, ADestDatabasePath: String);
    function  GetFolderContent(const APath, AMask: String): IevDataSet;
    function  GetFolderContentAndSize(const APath, AMask: String): IevDataSet;
    function  ServerFileExists(const AFileName: String): Boolean;
    procedure DeleteServerFile(const AFileName: String);
    procedure DeleteDatabaseFile(const ADatabasePath: String);
    procedure MoveDatabaseFile(const ASourceDB, ATargetDB: String);
    procedure ChangeFBAdminPassword(const ANewPassword: String);
    procedure ChangeFBUserPassword(const ANewPassword: String);
    procedure ShutdownDatabase(const ADatabaseFile: String);
    procedure OnlineDatabase(const ADatabaseFile: String);
    procedure BackupDatabase(const ADatabaseFile: String; const ACompress, AScramble: Boolean; out ABackupData: IisStream);
    procedure RestoreDatabase(const ADatabaseFile: String; ABackupData: IisStream; const ACompressed: Boolean);
    function  GetSystemInfo: String;

    property  Host: String read GetHost write SetHost;
  end;


  TevDatabase = class(TevCustomDatabase)
  private
    FType: TevDBType;
    FClientID: string;
    FModified: Boolean;
    FVersionWasChecked: Boolean;
    procedure SetModified(const AValue: Boolean);
    procedure PrepareForChanges;
    function  FindRWTransaction: TevTransaction;
    function  GetEvDBName: String;
    procedure SetEvDBName(const AValue: String);
    procedure CheckDBVersion;
  public
    procedure   ConnectIfNeeds; override;
    property    Modified: Boolean read FModified write SetModified;
    property    EvDBName: String read GetEvDBName write SetEvDBName;
    property    DBType: TevDBType read FType;
  end;


  TevIBSQL = class(TevRawSQL)
  private
    function    GetDBForSQL: TevDatabase;
    procedure   ApplyDataSecurityRules;
    procedure   DoExecQuery;
  public
    constructor Create(const AOwner: TevFirebird; const ASQL: String; const ASecurityOn: Boolean); reintroduce;

    procedure   ExecQuery; override;
    function    ExecAsDataSet(const ALoadBlobs: Boolean): IevDataSet; override;
    procedure   GetLazyBlobInfo(const ABlobField: TIBXSQLVAR; out AData: String);
  end;


  TevSQL = class(TisInterfacedObject, IevSQL)
  private
    FSQLObj: TevRawSQL;
    FOwner: TevFirebird;
    procedure CheckSQLObj;
  protected
    procedure SetParam(const AIndex: Integer; const AValue: Variant); overload;
    procedure SetParam(const AName: String; const AValue: Variant); overload;
    function  GetParam(const AIndex: Integer): Variant; overload;
    function  GetParam(const AName: String): Variant; overload;
    function  GetParamIndex(const AName: String; const AErrorIfNotEsist: Boolean = False): Integer;
    procedure SetParams(const AParams: IisListOfValues); overload;
    procedure SetParams(const AParams: array of Variant); overload;
    procedure Exec;
    function  Open(const ALoadBlobs: Boolean): IevDataSet;
  public
    constructor Create(const AOwner: TevFirebird; const ASQL: String); reintroduce;
    constructor CreateRaw(const AOwner: TevFirebird; const ASQL: String; const ADBType: TevDBType);
    procedure   ForceDetach;
    destructor  Destroy; override;
  end;


  IevCustomDBConnection = interface(IisObjectFromPool)
  ['{445C39A7-F43A-414D-9A57-1E370EF91DD8}']
    function Database: TIBDatabase;
  end;


  IevDBConnection = interface(IisObjectFromPool)
  ['{445C39A7-F43A-414D-9A57-1E370EF91DD8}']
    function Database: TevDatabase;
  end;


  IevServiceDBConnection = interface(IisObjectFromPool)
  ['{898A7905-AC85-4A95-8DED-2C057B9A7F41}']
    function Database: TevServiceDatabase;
  end;


  // Firebird DB access
  TevFirebird = class(TisInterfacedObject, IevDBServer, IisMessageRecipient)
  private
    FContextID: TisGUID;
    FSystemDBConn: IevDBConnection;
    FBureauDBConn: IevDBConnection;
    FClientDBConn: IevDBConnection;
    FTempDBConn:   IevDBConnection;
    FClientDBNbr: Integer;
    FReadWriteTransaction: TevTransaction;
    FDataSecurityActive: Boolean;
    FSecuredSQL: IevSecuredSQL;
    FDBOperLock: IisLock;
    FSQLObjects: TList;
    FUseReadTransaction: Boolean;
    procedure Notify(const AMessage: String; const AParams: IisListOfValues);
    function  GetSystemDBConn: IevDBConnection;
    function  GetBureauDBConn: IevDBConnection;
    function  GetClientDBConn: IevDBConnection;
    function  GetTempDBConn: IevDBConnection;
    procedure DetachMultiDBTransaction;
    function  DoOpenDS(const sQuery: string; const ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil;
                        const PkFieldName: string = ''; const BlobsLazyLoad: Boolean = False): IEvQueryResult;
    function  DoExecDS(const sQuery: string; const ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil): IisListOfValues;
    function  GetDBByType(const ADBType: TevDBType): TevDatabase;
    function  GetDBByTable(const ATableName: String): TevDatabase;
    function  MakeSQL(const AQueryName: String; const AMacros: IisListOfValues): String;
    procedure SetTransactionForQuery(const Q: TevCustomIBSQL);
    function  BuildErrorDetails(const AQuery: TevRawSQL): String; overload;
    function  BuildErrorDetails(const AQuery: IevSQL): String; overload;
    function  AcquireDBConnection(const ADBName: String): IevDBConnection;
    function  AcquireServiceDBConnection(const AHost: string): IevServiceDBConnection;
    procedure ReturnDBConnection(const ADBConnection: IevDBConnection);
    procedure ReturnServiceDBConnection(const ADBConnection: IevServiceDBConnection);
    procedure DropCachedDBConnections;
    procedure DropCachedDBConnectionsForDisabledDBs(const AParams: TTaskParamList);
    function  GetIBPath: String;
    procedure RunGBAK(const AParams: String);
    procedure RunISQL(const AParams: String; const AProgressCaption: String);
    procedure DoRestoreDB(const ABackupFile, ADBPath: String);
    procedure DoDropDB(const ADBPath: String);
    function  GetTableList(const ADBType: TevDBType): IisStringList;
    function  CalcHashFor(const ASQL: TevRawSQL): String;
    function  SetTransactionVariableSQL(const AVarName: String; const AValue: Variant): String;
    function  GetAllDBHosts(const ADR: Boolean): IisListOfValues;
    function  CheckAndUpdateClBase: Boolean;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure OpenClient(ClNbr: Integer);
    procedure StartTransaction(const ADBTypes: TevDBTypes);
    procedure Commit;
    procedure Rollback;
    procedure FlushTransactionChanges;
    function  OpenQuery(const ADBType: TevDBType; const ASQL: String; const AParamVals: array of Variant; const ALoadBlobs: Boolean): IevDataSet;
    function  ExecRawQuery(const ADBType: TevDBType; const ASQL: String; const AParamVals: array of Variant; const ALoadBlobs: Boolean): IevDataSet;
    function  CreateSQL(const ASQL: String; const AMacros: IisListOfValues = nil): IevSQL;
    function  CreateRawSQL(const ASQL: String; const ADBType: TevDBType): IevSQL;
    function  OpenDS(const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil): TEvBasicClientDataSet;
    function  OpenDSEx(const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil;
                       const PkFieldName: string = ''; const BlobsLazyLoad: Boolean = False): IEvQueryResult;
    procedure PopulateDS(const ds: TEvBasicClientDataSet; const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil; const BlobsLazyLoad: Boolean = False);
    function  ExecDS(const sQuery: string; ParamList: IisListOfValues = nil; const MacroList: IisListOfValues = nil): IisListOfValues;
    procedure ExecBatchDs(const APreSQL: string; const AUpdateSQL: TevBatchUpdateSQL; const ASequence: TevBatchUpdateSequence);
    function  OpenClientListDS: IevDataSet;
    procedure CreateClient(var ClNbr: Integer);
    procedure DeleteClient(const ClNbr: Integer);
    function  ExecQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean): IevDataSet;
    function  GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
    procedure SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
    function  GetNewKeyValue(const ATableName: String; const AIncrement: Integer = 1): Integer;
    function  GetDBVersion(const ADBType: TevDBType): String;
    procedure SetDataSecurityActive(const AValue: Boolean);
    function  GetDataSecurityActive: Boolean;
    procedure SetUseReadTransaction(const AValue: Boolean);
    function  GetUseReadTransaction: Boolean;
    procedure SendBroadcastNotification(const AEventID, Data: String);
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
    function  BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompress: Boolean): IisStream;
    procedure RestoreDB(const ADBName: String; const AData: IisStream; const ACompressed: Boolean);
    procedure SweepDB(const ADBName: String);
    procedure DropDB(const ADBName: String);
    function  PatchDB(const aDBName: String; const aAppVersion: String; AllowDowngrade: Boolean): Boolean;
    function  CustomScriptDB(const aDBName: String; const aScript: String): Boolean;
    function  SyncDBAndApp(const ADBName: String): Boolean;
    procedure SyncUDFAndApp;
    procedure ChangeFBAdminPassword(const ANewPassword: String);
    procedure ChangeFBUserPassword(const ANewPassword: String);
    procedure TidyUpDBFolders;
    procedure ImportDatabase(const ADBPath, ADBPassword: String);
    procedure ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    procedure UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    function  GetDBServerFolderContent(const APath, AUser, APassword, AMask: String; const AFileSize: Boolean): IevDataSet;
    function  GetDBFilesList: IevDataSet;
    function  GetLastTransactionNbr(const ADBType: TevDBType): Integer;
    function  GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
    function  GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
    procedure ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
    function  GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IevDataSet;
    procedure CheckDBHash(const ADBType: TevDBType; const ADBInfo: IevDataSet; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
    function  GetChangedTablesInTransaction(const ADBType: TevDBType; const ATableNameFilter: IisStringList = nil): IevDataSet;
    procedure InitializeTTForFullRebuild;
    procedure FinalizeTTAfterFullRebuild;
    function  GetDBServersInfo: IisParamsCollection;
    function  TryToGetDBVersion(const ADBPath: String): TisVersionInfo;
  end;


  TevDataSyncPacketCreator = class
  private
    FOwner: TevFirebird;
    FDBType: TevDBType;
    FStartTransNbr: Integer;
    FdsEvTransaction: IevDataSet;
    FdsEvTableChange: IevDataSet;
    FdsEvFieldChange: IevDataSet;
    FResult: IisStream;

    function  BuildAuditTransactionDS: Integer;
    procedure BuildAuditTableChangeDS;
    procedure BuildAuditFieldChangeDS;
    procedure BuildGeneratorsChange;
    procedure BuildDataChange;
    function  BuildDataTableChange(const ATableName: String; const ATableNbr: Integer): IisListOfValues;
  public
    constructor Create(const AOwner: TevFirebird);
    function Execute(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
  end;


  TevDataSyncPacketApplier = class
  private
    FOwner: TevFirebird;
    FDBType: TevDBType;
    FData: IisStream;
    FdsEvTableChange: IevDataSet;
    FdsEvTransaction: IevDataSet;

    function  CreateQuery(const ASQL: String): IevSQL;
    function  ExecQuery(const ASQL: String; const AParamVals: array of Variant): IevDataSet;
    procedure SyncEvTransaction;
    procedure SyncEvTableChange;
    procedure SyncEvFieldChange;
    procedure SyncGenerators;
    procedure SyncData;
    procedure SyncDataTable(const ASyncData: IisListOfValues);
  public
    constructor Create(const AOwner: TevFirebird);
    procedure Execute(const ADBType: TevDBType; const ADataSyncPacket: IisStream);
  end;


  // DB change UDP broadcaster for ADR
  TevDBChangeNotifier = class
  private
    FCS: TCriticalSection;
    FUDPNotificator: IisUDPSocket;
    procedure   SendNotification(const AEventID, Data: String);
  public
    constructor Create;
    destructor  Destroy; override;
  end;


  // DB Pool. One for all domains.
  TevCustomDBConnection = class(TisObjectFromPool, IevCustomDBConnection)
  private
    FDB: TIBDatabase;
  protected
    function  Database: TIBDatabase;
  public
    destructor Destroy; override;
  end;

  TevDBConnection = class(TevCustomDBConnection, IevDBConnection)
  protected
    function  Database: TevDatabase;
  public
    constructor Create(const AName: String); override;
  end;

  TevServiceDBConnection = class(TevCustomDBConnection, IevServiceDBConnection)
  protected
    function  Database: TevServiceDatabase;
  public
    constructor Create(const AName: String); override;
  end;

  IevDBConnectionPool = interface(IisObjectPool)
  ['{9A8E7E63-EC2D-4E06-8147-AE2D198ABE17}']
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    function  IsDBDisabled(const ADBName: String): Boolean;
  end;


  TevDBConnectionPool = class(TisObjectPool, IevDBConnectionPool)
  private
    FDisabledDBs: IisListOfValues;
    procedure PrepareDBList;
    function  IsServiceDB(const AObjectName: String): Boolean;
  protected
    function  CreateObject(const AObjectName: String): IisObjectFromPool; override;
    procedure BeforeCheckPool; override;
    function  AcquireObject(const AObjectName: String): IisObjectFromPool; override;
    procedure ReturnObject(const AObject: IisObjectFromPool); override;
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    function  IsDBDisabled(const ADBName: String): Boolean;
  end;


  IevSQLLog = interface
  ['{B3BA42D8-FB29-47F3-A962-FE0121D67DA4}']
    procedure Add(const ASQL: TevCustomIBSQL);
  end;

  TevSQLLog = class(TisInterfacedObject, IevSQLLog)
  private
    FLog: IisStream;
    procedure Add(const ASQL: TevCustomIBSQL);
  public
    constructor Create; reintroduce;
  end;


var DBChangeNotifier: TevDBChangeNotifier;
    DBConnectionPool: IevDBConnectionPool;
    SQLMonitorLog: IevSQLLog;

const
  IM_DROP_DISABLED_DB_CONNECTIONS = 'IM_DROP_DISABLED_DB_CONNECTIONS';
  rebuildExt = '.rebuild';

// leading and trailing spaces are important here!
  sSystemDBStoredProcedures = ' GET_SYSTEM_TIME ';

  sSBDBStoredProcedures = ' ALL_CL_NUMBERS CL_COPY CL_NBR LIST_TOP_MAIL_BOXES LIST_TOP_MAIL_BOXES2 WRITELOGFILE ';

  sClientDBStoredProcedures = ' ALL_CL_NUMBERS NVL_DOUBLE ';

function GetFirebirdDBServer: IevDBServer;
begin
  Result := TevFirebird.Create;
end;


function IsExecuteBlock(const ASQL: String): Boolean;
begin
  Result := Contains(ASQL, 'EXECUTE BLOCK');
end;


function IsSelect(const ASQL: String): Boolean;
begin
  Result := AnsiStartsText('SELECT', ASQL) or
            IsExecuteBlock(ASQL) and Contains(ASQL, 'RETURNS (');
end;


function GetDBConnectionInfo(const ADatabaseName: String): TevDBConnectionInfo;
begin
   if ctx_DBAccess.GetADRContext then
     Result.Path := ctx_DomainInfo.ADRDBLocationList.GetDBPath(ADatabaseName)
   else
     Result.Path := ctx_DomainInfo.DBLocationList.GetDBPath(ADatabaseName);

  Result.User := FB_User;
  Result.Password := mb_GlobalSettings.DBUserPassword;
end;



{ TevDBChangeNotifier }

constructor TevDBChangeNotifier.Create;
begin
  FCS := TCriticalSection.Create;
end;

destructor TevDBChangeNotifier.Destroy;
begin
  FCS.Free;
  inherited;
end;

procedure TevDBChangeNotifier.SendNotification(const AEventID, Data: String);
var
  EnvID, Datagram, OldDatagram: String;
begin
  EnvID := mb_GlobalSettings.EnvironmentID;

  if (EnvID <> '') and ctx_DomainInfo.DBChangesBroadcast and not IsADRUser(Context.UserAccount.User) and not IsDBAUser(Context.UserAccount.User) then
  begin
    Datagram := 'EVOEVENT VER=2 ' +
         'ENV=' + UpperCase(EnvID) + ' ' +
         'EVD='  + ctx_DomainInfo.DomainName + ' ' +
         'ID=' + AEventID + ' ' +
         'USER=' + IntToStr(Context.UserAccount.InternalNbr) + ' ' +
         'DATA=' + Data;


    if ctx_DomainInfo.DomainName <> sDefaultDomain then
      EnvID := EnvID + ':' + ctx_DomainInfo.DomainName;
    OldDatagram := 'EVOEVENT VER=1 ' +
            'RD=' + UpperCase(EnvID) + ' ' +
            'ENV=' + UpperCase(EnvID) + ' ' +
            'ID=' + AEventID + ' ' +
            'USER=' + IntToStr(Context.UserAccount.InternalNbr) + ' ' +
            'DATA=' + Data;

    FCS.Enter;
    try
      if not Assigned(FUDPNotificator) then
        FUDPNotificator := TisUDPSocket.Create(mb_AppConfiguration.AsString['Network\ADRBroadcastIPAddr']);

      FUDPNotificator.SendString(Datagram, BROADCAST_IP, DB_Change_Notification_Port);
      FUDPNotificator.SendString(OldDatagram, BROADCAST_IP, DB_Change_Notification_Port); //Remove after complete transition on ADR2
    finally
      FCS.Leave;
    end;
  end;
end;


{ TevFirebird }

procedure TevFirebird.Commit;
begin
  FDBOperLock.Lock;
  try
    FReadWriteTransaction.DoCommit;
    DetachMultiDBTransaction;
  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.CreateClient(var ClNbr: Integer);
var
  sDBName, sDBPath, ITMessage: String;
  Params, Results: IisListOfValues;
  dbConn: TevDBConnectionInfo;

  function GetNextCLNbr(const clNbr:Integer):integer;
  var
     DS: IevDataSet;
     tmpCLNbr, i: integer;

     procedure setNewG_CL;
     begin
        SetGeneratorValue('G_CL', dbtBureau, tmpCLNbr);
        ITMessage:=ITMessage + #13#13 + ' G_CL Generator value updated with ' +  IntToStr(tmpCLNbr);
        Result := tmpCLNbr;
     end;

  begin
    try
      result:= clNbr;
      ctx_DBAccess.DisableSecurity;
      DS := OpenClientListDS;
      DS.IndexFieldNames:= 'CL_NBR';
      if DS.FindKey([clNbr]) then
      begin
        tmpCLNbr:= clNbr;
        DS.Next;
        while not DS.Eof do
        begin
          inc(tmpCLNbr);
          i := DS.FieldByName('CL_NBR').AsInteger - tmpCLNbr;

          case i of
            0   :;
            1..3:Begin
                   ITMessage:=ITMessage + ' [ ' +  IntToStr(tmpCLNbr) + ' - ' + IntToStr(DS.FieldByName('CL_NBR').AsInteger - 1) + ' ]';
                   tmpCLNbr:= DS.FieldByName('CL_NBR').AsInteger;
                 end;
          else
           begin
             setNewG_CL;
             break;
           end;
          end;
          DS.Next;
        end;

        if tmpCLNbr = clNbr then
        begin
          inc(tmpCLNbr);
          setNewG_CL;
        end;
      end;
    finally
      ctx_DBAccess.EnableSecurity;
      if ITMessage <> '' then
        mb_LogFile.AddContextEvent(etInformation, 'DB Access', 'Skipping CL Database Numbers', ITMessage);

    end;
  End;

begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.CreateClient');
    ctx_Statistics.ActiveEvent.Properties.AddValue('Cl_Nbr', ClNbr);
  end;

  ClNbr:= GetNextCLNbr(ClNbr);

  FDBOperLock.Lock;
  try
    sDBName := 'CL_' + IntToStr(ClNbr) + '.gdb';
    dbConn := GetDBConnectionInfo(sDBName);
    sDBPath := dbConn.Path;
    if Pos(':', sDBPath) <> 0 then
      GetNextStrValue(sDBPath, ':');
    sDBPath := ExtractFilePathUD(sDBPath);

    Params := TisListOfValues.Create;
    Params.AddValue('DB_PATH', sDBPath);
    Params.AddValue('CLIENT_NAME', sDBName);

    Results := ExecDS('CL_COPY', Params);
    ErrorIf(Results.Value['ERROR_TYPE'] = 1, 'Database already exists! Client was not created.', EClientCreate, IDH_ClientCreate);
    ErrorIf(Results.Value['ERROR_TYPE'] = 2, 'Could not copy database! Client was not created.', EClientCreate, IDH_ClientCreate);
    ErrorIf(Results.Value['ERROR_TYPE'] <> 0 , 'Client creation error <' + VarToStr(Results.Value['ERROR_TYPE']) + '>', EClientCreate, IDH_ClientCreate);

  finally
    FDBOperLock.Unlock;
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;

  SendBroadcastNotification('DBCREATED', 'CL_' + IntToStr(ClNbr));
end;

procedure TevFirebird.DetachMultiDBTransaction;
begin
  if Assigned(FReadWriteTransaction) then
  begin
    if Assigned(FSystemDBConn) then
      FSystemDBConn.Database.RemoveTransaction(FSystemDBConn.Database.FindTransaction(FReadWriteTransaction));

    if Assigned(FBureauDBConn) then
      FBureauDBConn.Database.RemoveTransaction(FBureauDBConn.Database.FindTransaction(FReadWriteTransaction));

    if Assigned(FClientDBConn) then
      FClientDBConn.Database.RemoveTransaction(FClientDBConn.Database.FindTransaction(FReadWriteTransaction));

    if Assigned(FTempDBConn) then
      FTempDBConn.Database.RemoveTransaction(FTempDBConn.Database.FindTransaction(FReadWriteTransaction));

    FReadWriteTransaction.RemoveDatabases;
  end;
end;

procedure TevFirebird.DoOnConstruction;
begin
  inherited;
  FContextID := Context.GetID;

  FDBOperLock := TisLock.Create;
  FSQLObjects := TList.Create;
  FSecuredSQL := GetSecuredSQL;
  FReadWriteTransaction := TevTransaction.Create(nil);
  FReadWriteTransaction.ReadOnly := False;
  FReadWriteTransaction.Name := 'RWTran';

  mb_Messenger.Subscribe(Self, [IM_DROP_DISABLED_DB_CONNECTIONS], False);
end;

function TevFirebird.DoOpenDS(const sQuery: string; const ParamList, MacroList: IisListOfValues; const PkFieldName: string; const BlobsLazyLoad: Boolean): IEvQueryResult;
var
  Q: TevIBSQL;
  p: TEvIbSqlKbmMemTableProvider;
begin
  Q := TevIBSQL.Create(Self, MakeSQL(sQuery, MacroList), FDataSecurityActive);
  try
    SetTransactionForQuery(Q);

    p := TEvIbSqlKbmMemTableProvider.Create;
    try
      try
        if BlobsLazyLoad then
          p.OnGetBlobData := Q.GetLazyBlobInfo;

        Q.SetParams(ParamList);

        if ctx_Statistics.Enabled then
        begin
          ctx_Statistics.BeginEvent('DB.Query');
          ctx_Statistics.ActiveEvent.Properties.AddValue('SQL', Q.SQL.Text);
        end;

        try
          Q.DoExecQuery;
          Result := p.GetData(Q, PkFieldName);
        finally
          if ctx_Statistics.Enabled then
          begin
            if Assigned(Result) then
              ctx_Statistics.ActiveEvent.Properties.AddValue('RecCount',p.RecCount);
            ctx_Statistics.EndEvent;
          end;
        end;
      except
        on E: Exception do
          raise EevException.CreateDetailed(E.Message, BuildErrorDetails(Q), True);
      end;
    finally
      p.Free;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevFirebird.ExecBatchDs(const APreSQL: string; const AUpdateSQL: TevBatchUpdateSQL; const ASequence: TevBatchUpdateSequence);
var
  qSupplemental, qOper: TevIBSQL;
  qIns, qUpd, qDel: TevIBSQL;
  i: integer;

  function PrepareQuery(const uk: TUpdateKind): TevIBSQL;
  var
    s: String;
    Q: ^TevIBSQL;
  begin
    case uk of
      ukModify: Q := @qUpd;
      ukInsert: Q := @qIns;
      ukDelete: Q := @qDel;
    end;

    if not Assigned(Q^) then
    begin
      s := MakeSQL(AUpdateSQL[uk].SQL, AUpdateSQL[uk].Macros);
      Q^ := TevIBSQL.Create(Self, s, False);
      SetTransactionForQuery(Q^);
      Q^.Prepare;
    end;

    Result := Q^;
  end;

begin
  FDBOperLock.Lock;
  try
    qIns := nil;
    qUpd := nil;
    qDel := nil;
    qOper := nil;

    try
      try
        if APreSQL <> '' then
        begin
          qSupplemental := TevIBSQL.Create(Self, APreSQL, False);
          try
            SetTransactionForQuery(qSupplemental);
            qSupplemental.ExecQuery;
          finally
            FreeAndNil(qSupplemental);
          end;
        end;


        for i := Low(ASequence) to High(ASequence) do
        begin
          qOper := PrepareQuery(ASequence[i].SQLType);
          qOper.SetParams(ASequence[i].Params, True);
          qOper.SetParams(AUpdateSQL[ASequence[i].SQLType].Params, False);

          if AUpdateSQL[ASequence[i].SQLType].NbrParamName <> '' then
            qOper.ParamByName(AUpdateSQL[ASequence[i].SQLType].NbrParamName).AsVariant := ASequence[i].Params.Value['PK'];

          qOper.ExecQuery;
        end;

      except
        on E: Exception do
        begin
          if Assigned(qOper) then
            raise EevException.CreateDetailed(E.Message, BuildErrorDetails(qOper), True)
          else
            raise;
        end;
      end;

    finally
      qIns.Free;
      qUpd.Free;
      qDel.Free;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.ExecDS(const sQuery: string; ParamList: IisListOfValues; const MacroList: IisListOfValues): IisListOfValues;
begin
  FDBOperLock.Lock;
  try
    Result := DoExecDS(sQuery, ParamList, MacroList);
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.GetDBByType(const ADBType: TevDBType): TevDatabase;
begin
  case ADBType of
    dbtSystem:
      Result := GetSystemDBConn.Database;
    dbtBureau:
      Result := GetBureauDBConn.Database;
    dbtTemporary:
      Result := GetTempDBConn.Database;
    dbtClient:
      Result := GetClientDBConn.Database;
  else
    Result := nil;
  end;
end;

procedure TevFirebird.OpenClient(ClNbr: Integer);
begin
  FDBOperLock.Lock;
  try
    if FClientDBNbr <> ClNbr then
    begin
      if Assigned(FClientDBConn) then
      begin
        ReturnDBConnection(FClientDBConn);
        FClientDBConn := nil;
      end;

      FClientDBNbr := ClNbr;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.OpenClientListDS: IevDataSet;
const
  ResStructConsts: array [0..0] of TDSFieldDef = (
    (FieldName: 'CL_NBR';  DataType: ftInteger;  Size: 0;  Required: False));
var
  s: String;
  i, cl_nbr: Integer;
  AllLocations: IisStringList;
  DBLocList: IevDBLocationList;
  DS: IevDataSet;
begin
  if ctx_Statistics.Enabled then
    ctx_Statistics.BeginEvent('DB.GetClientList');

  Result := TevDataSet.Create(ResStructConsts);
  Result.LogChanges := False;
  Result.IndexFieldNames := 'CL_NBR';

  FDBOperLock.Lock;
  try
    AllLocations := TisStringList.CreateUnique;
    AllLocations.CaseSensitive := True;

    if ctx_DBAccess.GetADRContext then
      DBLocList := ctx_DomainInfo.ADRDBLocationList
    else
      DBLocList := ctx_DomainInfo.DBLocationList;

    for i := 0 to DBLocList.Count - 1 do
      if DBLocList[i].DBType in [dbtUnspecified, dbtClient] then
        AllLocations.Add(DBLocList[i].Path);

    for i := 0 to AllLocations.Count - 1 do
    begin
      DS := GetDBServerFolderContent(NormalizePath(AllLocations[i]), FB_Admin, mb_GlobalSettings.DBAdminPassword, 'CL_%.gdb', False);

      DS.First;
      while not DS.EOF do
      begin
        s := DS.Fields[0].AsString;
        GetNextStrValue(s, 'CL_');
        s := GetNextStrValue(s, '.');
        cl_nbr := StrToIntDef(s, 0);

        if (cl_nbr > 0) and
           ((Context.UserAccount.AccountType = uatSBAdmin) or  not FDataSecurityActive or FDataSecurityActive and ctx_Security.AccountRights.Clients.IsClientEnabled(cl_nbr)) then
        begin
          // checking existence of CL_NBR in Result
          if not Result.FindKey([cl_nbr]) then
          begin
            Result.Append;
            Result.FieldByName('CL_NBR').AsInteger := cl_nbr;
            Result.Post;
          end;
        end;
        DS.Next;
      end;
      DS := nil;
    end;
  finally
    FDBOperLock.Unlock;
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

function TevFirebird.OpenDS(const sQuery: string; ParamList: IisListOfValues; const MacroList: IisListOfValues): TEvBasicClientDataSet;
begin
  FDBOperLock.Lock;
  try
    Result := TEvBasicClientDataSet.Create(nil);
    try
      PopulateDS(Result, sQuery, ParamList, MacroList);
    except
      FreeAndNil(Result);
      raise;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.OpenDSEx(const sQuery: string; ParamList: IisListOfValues;
  const MacroList: IisListOfValues; const PkFieldName: string; const BlobsLazyLoad: Boolean): IEvQueryResult;
begin
  FDBOperLock.Lock;
  try
    Result := DoOpenDS(sQuery, ParamList, MacroList, PkFieldName, BlobsLazyLoad);
  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.PopulateDS(const ds: TEvBasicClientDataSet; const sQuery: string;
  ParamList: IisListOfValues; const MacroList: IisListOfValues; const BlobsLazyLoad: Boolean);
var
  s: IEvQueryResult;
begin
  FDBOperLock.Lock;
  try
    s := OpenDSEx(sQuery, ParamList, MacroList, '', BlobsLazyLoad);
    ds.LoadFromUniversalStream(s.DualStream);
  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.Rollback;
begin
  if FReadWriteTransaction.InTransaction then
  begin
    FDBOperLock.Lock;
    try
      FReadWriteTransaction.DoRollback;
      DetachMultiDBTransaction;
    finally
      FDBOperLock.Unlock;
    end;
  end;
end;

procedure TevFirebird.StartTransaction(const ADBTypes: TevDBTypes);

  procedure AddTransToDB(const DBConn: IevDBConnection);
  begin
    DBConn.Database.AddTransaction(FReadWriteTransaction);
    FReadWriteTransaction.AddDatabase(DBConn.Database);
  end;

begin
  FDBOperLock.Lock;
  try
    CheckCondition(not FReadWriteTransaction.InTransaction, 'Transaction is active');

    try
      if dbtSystem in ADBTypes then
        AddTransToDB(GetSystemDBConn);

      if dbtBureau in ADBTypes then
        AddTransToDB(GetBureauDBConn);

      if dbtClient in ADBTypes then
        AddTransToDB(GetClientDBConn);

      // TODO: remove IsADRUser if synching of TTs is used
      if not IsADRUser(Context.UserAccount.User) and (dbtClient in ADBTypes) or (dbtTemporary in ADBTypes) then
        AddTransToDB(GetTempDBConn);

      FReadWriteTransaction.DoStart;
    except
      DetachMultiDBTransaction;
      raise;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.ExecQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean): IevDataSet;
var
  Q: TevIBSQL;
begin
  FDBOperLock.Lock;
  try
    Q := TevIBSQL.Create(Self, MakeSQL(AQueryName, AMacros), FDataSecurityActive);
    try
      SetTransactionForQuery(Q);
      try
        Q.SetParams(AParams);
        Result := Q.ExecAsDataSet(ALoadBlobs);
      except
        on E: Exception do
          raise EevException.CreateDetailed(E.Message, BuildErrorDetails(Q), True);
      end;
    finally
      Q.Free;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.MakeSQL(const AQueryName: String; const AMacros: IisListOfValues): String;
var
  sSQL: String;
  sMacroName: String;
  Macro: IisNamedValue;

  function AssignMacros(ASQLFragment: String): String;
  var
    sPart: String;
  begin
    ASQLFragment := StringReplace(ASQLFragment, #13, ' ', [rfReplaceAll]);
    ASQLFragment := StringReplace(ASQLFragment, #10, ' ', [rfReplaceAll]);

    Result := '';
    while ASQLFragment <> '' do
    begin
      // take care of quoted text
      sPart := GetNextStrValue(ASQLFragment, '''');

      while sPart <> '' do
      begin
        Result := Result + GetNextStrValue(sPart, '#');

        if sPart <> '' then
        begin
          sMacroName := GetNextTextValue(sPart, ValidTextChars - ['_']);
          if Assigned(AMacros) then
            Macro := AMacros.FindValue(sMacroName)
          else
            Macro := nil;

          if Assigned(Macro) then
            Result := Result + AssignMacros(Macro.Value)
          else
            Result := Result + AssignMacros(QueryLib.GetQueryText(sMacroName).SQL);
        end;
      end;

      if ASQLFragment <> '' then
      begin
        Result := Result + '''';
        Result := Result + GetNextStrValue(ASQLFragment, '''') + '''';
      end;
    end;
  end;

begin
  sSQL := QueryLib.GetQueryText(AQueryName).SQL;
  if sSQL = '' then
    sSQL := QueryLib.ProcessTemplates(AQueryName);

  sSQL := AssignMacros(Trim(sSQL));
  Result := QueryLib.ProcessTemplates(sSQL);

  Result := StringReplace(Result, '''now''', ':CurrentSysTime999', [rfReplaceAll, rfIgnoreCase]);
end;


function TevFirebird.GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
var
  Q: TevCustomIBSQL;
begin
  FDBOperLock.Lock;
  try
    Q := TevCustomIBSQL.Create(nil);
    try
      Q.SQL.Text := 'SELECT Gen_Id(' + AGeneratorName + ', ' + IntToStr(AIncrement) + ') FROM ev_database';
      Q.Database := GetDBByType(ADBType);
      SetTransactionForQuery(Q);
      Q.ExecQuery;
      Result := Q.Fields[0].AsInteger;
    finally
      Q.Free;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
var
  Q: TevCustomIBSQL;
begin
  FDBOperLock.Lock;
  try
    Q := TevCustomIBSQL.Create(nil);
    try
      Q.SQL.Text := 'SET GENERATOR ' + AGeneratorName + ' TO ' + IntToStr(AValue);
      Q.Database := GetDBByType(ADBType);
      SetTransactionForQuery(Q);
      Q.ExecQuery;
    finally
      Q.Free;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.GetNewKeyValue(const ATableName: String; const AIncrement: Integer): Integer;
begin
  FDBOperLock.Lock;
  try
    Result := GetGeneratorValue('G_' + ATableName, GetDBByTable(ATableName).DBType, AIncrement);
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.GetDBByTable(const ATableName: String): TevDatabase;
begin
  Result := GetDBByType(GetDBTypeByTable(ATableName));
  if not Assigned(Result) then
    raise EevException.Create('Cannot identify database type for table "' + ATableName + '"');
end;


procedure TevFirebird.SetTransactionForQuery(const Q: TevCustomIBSQL);
begin
  if not (GetUseReadTransaction and IsSelect(Q.SQL.Text)) and FReadWriteTransaction.InTransaction and (FReadWriteTransaction.FindDatabase(Q.Database) <> -1) then
    Q.Transaction := FReadWriteTransaction
  else
  begin
    Q.Transaction := Q.Database.DefaultTransaction;
    TevTransaction(Q.Transaction).StartIfNeeds;
  end;
end;

function TevFirebird.DoExecDS(const sQuery: string; const ParamList, MacroList: IisListOfValues): IisListOfValues;
var
  i: integer;
  Q: TevIBSQL;
begin
  Q := TevIBSQL.Create(Self, MakeSQL(sQuery, MacroList), FDataSecurityActive);
  try
    SetTransactionForQuery(Q);

    try
      Q.SetParams(ParamList);
      Q.ExecQuery;
    except
      on E: Exception do
        raise EevException.CreateDetailed(E.Message, BuildErrorDetails(Q), True);
    end;

    Result := TisListOfValues.Create;
    for i := 0 to Q.Current.Count-1 do
      Result.AddValue(Q.Fields[i].Name, Q.Fields[i].Value);
  finally
    Q.Free;
  end;
end;

function TevFirebird.BuildErrorDetails(const AQuery: TevRawSQL): String;
var
  i: Integer;
begin
  Result := 'Database: ' + AQuery.Database.DatabaseName +  #13#10#13#10'SQL: ' + AQuery.SQL.Text + #13#10#13#10;
  if AQuery.Prepared then
  begin
    if AQuery.Params.Count > 0 then
      Result := Result + 'Parameters:'#13#10;

    for i := 0 to AQuery.Params.Count - 1 do
      if AQuery.Params[i].SQLType and SQL_BLOB = SQL_BLOB then
        if AQuery.Params[i].IsNull then
          Result := Result + AQuery.Params[i].Name + ': blob'#13#10
        else
          Result := Result + AQuery.Params[i].Name + ': BLOB'#13#10
      else
        Result := Result + AQuery.Params[i].Name + ': ' + AQuery.Params[i].AsString + #13#10;
  end;
end;

function TevFirebird.GetDBVersion(const ADBType: TevDBType): String;
var
  DB: TevDatabase;
begin
  FDBOperLock.Lock;
  try
    DB := GetDBByType(ADBType);
    Result := VersionRecToStr(DB.DBVersion);
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.GetDataSecurityActive: Boolean;
begin
  Result := FDataSecurityActive and not IsADRUser(Context.UserAccount.User) and not IsDBAUser(Context.UserAccount.User);
end;

procedure TevFirebird.SetDataSecurityActive(const AValue: Boolean);
begin
  FDataSecurityActive := AValue;
end;


procedure TevFirebird.SendBroadcastNotification(const AEventID, Data: String);
begin
  DBChangeNotifier.SendNotification(AEventID, Data);
end;

function TevFirebird.GetBureauDBConn: IevDBConnection;
begin
  if not Assigned(FBureauDBConn) then
    FBureauDBConn := AcquireDBConnection('S_BUREAU');
  Result := FBureauDBConn;
end;

function TevFirebird.GetClientDBConn: IevDBConnection;
var
  s: String;
begin
  if not Assigned(FClientDBConn) then
  begin
    if FClientDBNbr = 0 then
      s := 'CL_BASE'
    else
      s := 'CL_' + IntToStr(FClientDBNbr);
    FClientDBConn := AcquireDBConnection(s);
  end;
  Result := FClientDBConn;
end;

function TevFirebird.GetSystemDBConn: IevDBConnection;
begin
  if not Assigned(FSystemDBConn) then
    FSystemDBConn := AcquireDBConnection('SYSTEM');
  Result := FSystemDBConn;
end;

function TevFirebird.GetTempDBConn: IevDBConnection;
begin
  if not Assigned(FTempDBConn) then
    FTempDBConn := AcquireDBConnection('TMP_TBLS');

  Result := FTempDBConn;
end;

function TevFirebird.AcquireDBConnection(const ADBName: String): IevDBConnection;
var
  sDomainID: String;
begin
  if ctx_DBAccess.GetADRContext then
    sDomainID := ctx_DomainInfo.DomainName + '-ADR'
  else
    sDomainID := ctx_DomainInfo.DomainName;

  if ctx_DBAccess.FullTTRebuildInProgress then
    Result := TevDBConnection.Create(sDomainID + '.' + ADBName) // DB connection pool must stay empty during rebuild TT
  else
    Result := DBConnectionPool.AcquireObject(sDomainID + '.' + ADBName) as IevDBConnection;
end;

procedure TevFirebird.ReturnDBConnection(const ADBConnection: IevDBConnection);
var
  i: Integer;
begin
  if FinishesWith(ADBConnection.Database.DatabaseName, '.gdb') and ADBConnection.Database.Connected then
  begin
    for i := 0 to ADBConnection.Database.TransactionCount - 1 do
    begin
      if ADBConnection.Database.Transactions[i].InTransaction then
        if CurrentThreadTaskTerminated or (Context = nil) or (ADBConnection.Database.Transactions[i] = ADBConnection.Database.InternalTransaction) then
          ADBConnection.Database.Transactions[i].Rollback  // if app is terminating
        else
          TevTransaction(ADBConnection.Database.Transactions[i]).DoRollback;

      TevTransaction(ADBConnection.Database.Transactions[i]).ReadCommited := True; // ReadCommited is a default isolation level for RW and RO transactions
    end;

    DBConnectionPool.ReturnObject(ADBConnection);
  end;
end;

function TevFirebird.GetDisabledDBs: IisStringList;
begin
  Result := DBConnectionPool.GetDisabledDBs;
end;

procedure TevFirebird.DropCachedDBConnections;
var
  i: Integer;
begin
  FDBOperLock.Lock;
  try
    for i := 0 to FSQLObjects.Count - 1 do
      TevSQL(FSQLObjects[i]).ForceDetach;

    DetachMultiDBTransaction;

    if Assigned(DBConnectionPool) then
    begin
      if Assigned(FSystemDBConn) then
      begin
        ReturnDBConnection(FSystemDBConn);
        FSystemDBConn := nil;
      end;

      if Assigned(FBureauDBConn) then
      begin
        ReturnDBConnection(FBureauDBConn);
        FBureauDBConn := nil;
      end;

      if Assigned(FClientDBConn) then
      begin
        ReturnDBConnection(FClientDBConn);
        FClientDBConn := nil;
      end;

      if Assigned(FTempDBConn) then
      begin
        ReturnDBConnection(FTempDBConn);
        FTempDBConn := nil;
      end;
    end;

  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompress: Boolean): IisStream;
var
  dbConnInfo: TevDBConnectionInfo;
  srvDBconn: IevServiceDBConnection;
  db_path, host, s: String;
  BkpFile: IisStream;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('Backup DB');
    ctx_Statistics.ActiveEvent.Properties.AddValue('DB Name', ADBName);
    ctx_Statistics.ActiveEvent.Properties.AddValue('Metadata only', AMetadataOnly);
    ctx_Statistics.ActiveEvent.Properties.AddValue('Scramble', AScramble);
    ctx_Statistics.ActiveEvent.Properties.AddValue('Compress', ACompress);
  end;

  if AMetadataOnly then
  begin
    BkpFile := TisStream.CreateFromFile(AppDir + ADBName + '.gbk');
    s := AppTempFolder + GetUniqueID + '.zip';
    AddToFileFromStream(s, AnsiUpperCase(ADBName) + '.gbk', BkpFile.RealStream);
    Result := TisStream.CreateFromFile(s, True);
  end
  else
  begin
    ctx_StartWait('Initializing');
    try
      dbConnInfo := GetDBConnectionInfo(ADBName);
      db_path := dbConnInfo.Path;
      host := GetNextStrValue(db_path, ':');

      srvDBconn := AcquireServiceDBConnection(host);
      try
        ctx_UpdateWait('Backing up database');
        srvDBconn.Database.BackupDatabase(db_path, ACompress, AScramble and StartsWith(ADBName, DB_Cl), Result);
        ctx_UpdateWait('');
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    finally
      ctx_EndWait;
      if ctx_Statistics.Enabled then
        ctx_Statistics.EndEvent;
    end;
  end;
end;

procedure TevFirebird.RestoreDB(const ADBName: String; const AData: IisStream; const ACompressed: Boolean);
var
  dbConnInfo: TevDBConnectionInfo;
  srvDBconn: IevServiceDBConnection;
  db_path, host, zipFileName, bkpFileName: String;
  BkpData: IisStream;
  compr: Boolean;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('Restore DB');
    ctx_Statistics.ActiveEvent.Properties.AddValue('DB Name', ADBName);
    ctx_Statistics.ActiveEvent.Properties.AddValue('ACompressed', ACompressed);
  end;

  BkpData := AData;
  compr := ACompressed;
  if compr then
  begin
    if IsZipCompressedStream(AData) then // ZIP compression must be uncompressed locally
    begin
      zipFileName := AppTempFolder + GetUniqueID + '.zip';
      try
        AData.SaveToFile(zipFileName);

        bkpFileName := AppTempFolder + GetUniqueID + '.tmp';
        BkpData := TisStream.CreateFromNewFile(bkpFileName, True);

        ExtractFromFileToStream(zipFileName, ADBName + '.gbk', BkpData);
      finally
        DeleteFile(zipFileName);
      end;

      compr := False;
    end;
  end;

  ctx_StartWait('Initializing');
  try
    dbConnInfo := GetDBConnectionInfo(ADBName);
    db_path := dbConnInfo.Path;
    host := GetNextStrValue(db_path, ':');

    srvDBconn := AcquireServiceDBConnection(host);
    try
      ctx_UpdateWait('Restore database');
      srvDBconn.Database.RestoreDatabase(db_path, BkpData, compr);
      ctx_UpdateWait('');
    finally
      ReturnServiceDBConnection(srvDBconn);
    end;
  finally
    ctx_EndWait;
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

function TevFirebird.GetIBPath: String;
var
  Reg: IisSettings;
begin
  Result := AppDir;

  if not FileExists(Result + 'gds32.dll') then
  begin
    Reg := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, '');

    Result := Reg.AsString['Software\Firebird Project\Firebird Server\Instances\DefaultInstance'];
    if Result = '' then
    begin
      Result := Reg.AsString['Software\FirebirdSQL\Firebird\CurrentVersion\RootDirectory'];
      if Result = '' then
      begin
        Result := Reg.AsString['Software\Borland\Interbase\CurrentVersion\RootDirectory'];
        if Result = '' then
          raise EevException.Create('Firebird/Inrebase client installation is not found');
      end;
    end;

    Result := NormalizePath(Result) + 'bin\';
  end;
end;

function TevFirebird.GetLastTransactionNbr(const ADBType: TevDBType): Integer;
var
  DS: IevDataSet;
begin
  FDBOperLock.Lock;
  try
    DS := OpenQuery(ADBType, 'SELECT MAX(commit_nbr) FROM ev_transaction', [], True);
    Result := DS.Fields[0].AsInteger;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.CreateSQL(const ASQL: String; const AMacros: IisListOfValues = nil): IevSQL;
begin
  FDBOperLock.Lock;
  try
    Result := TevSQL.Create(Self, MakeSQL(ASQL, AMacros));
  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.DropCachedDBConnectionsForDisabledDBs(const AParams: TTaskParamList);
var
  i: Integer;
  SQLobj: TevSQL;
  bNeedCloseAllConnections: Boolean;
begin
  FDBOperLock.Lock;
  try
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.SetThreadContext(Mainboard.ContextManager.FindContextByID(FContextID));

      bNeedCloseAllConnections := False;
      if Assigned(FReadWriteTransaction) then
        for i := 0 to FReadWriteTransaction.DatabaseCount - 1 do
          if DBConnectionPool.IsDBDisabled(FReadWriteTransaction.Databases[i].Name) then
          begin
            bNeedCloseAllConnections := True;
            break;
          end;

      if bNeedCloseAllConnections then
        DropCachedDBConnections // close all connections if at least one database in destributed transaction is in maintenance

      else
      begin
        for i := 0 to FSQLObjects.Count - 1 do
        begin
          SQLobj := TevSQL(FSQLObjects[i]);
          if Assigned(SQLobj.FSQLObj) then
          begin
            if DBConnectionPool.IsDBDisabled(SQLobj.FSQLObj.Database.Name) then
              SQLobj.ForceDetach;
          end;
        end;

        // othervise close only database in maintenace mode
        if Assigned(FSystemDBConn) and DBConnectionPool.IsDBDisabled(FSystemDBConn.Name) then
        begin
          ReturnDBConnection(FSystemDBConn);
          FSystemDBConn := nil;
        end;

        if Assigned(FBureauDBConn) and DBConnectionPool.IsDBDisabled(FBureauDBConn.Name) then
        begin
          ReturnDBConnection(FBureauDBConn);
          FBureauDBConn := nil;
        end;

        if Assigned(FClientDBConn) and DBConnectionPool.IsDBDisabled(FClientDBConn.Name) then
        begin
          ReturnDBConnection(FClientDBConn);
          FClientDBConn := nil;
        end;

        if Assigned(FTempDBConn) and DBConnectionPool.IsDBDisabled(FTempDBConn.Name) then
        begin
          ReturnDBConnection(FTempDBConn);
          FTempDBConn := nil;
        end;
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.DoRestoreDB(const ABackupFile, ADBPath: String);
var
  Params, dbPath, host, s: String;
  srvDBconn: IevServiceDBConnection;
begin
  CheckCondition(FileExists(ABackupFile), 'File ' + ABackupFile + ' does not exist');

  dbPath := ADBPath;
  host := GetNextStrValue(dbPath, ':');
  dbPath := ADBPath + '.tmp';

  srvDBconn := AcquireServiceDBConnection(host);
  try
    srvDBconn.Database.DeleteDatabaseFile(dbPath);
  finally
    ReturnServiceDBConnection(srvDBconn);
  end;

  Params := '-c -rep -p 8192';
  if IsADRUser(Context.UserAccount.User) then
    Params := Params + ' -v';
  Params := Params + ' -user ' + FB_Admin + ' -password ' + mb_GlobalSettings.DBAdminPassword + ' "' + ABackupFile + '" "' + dbPath + '"';
  RunGBAK(Params);

  if not AnsiSameStr(dbPath, ADBPath) then
  begin
    srvDBconn := AcquireServiceDBConnection(host);
    try
      try
        s := ADBPath;
        GetNextStrValue(s, ':');
        GetNextStrValue(dbPath, ':');
        srvDBconn.Database.MoveDatabaseFile(dbPath, s);
      except
        try
          srvDBconn.Database.DeleteDatabaseFile(dbPath);
        except
        end;
        raise;
      end;
    finally
      ReturnServiceDBConnection(srvDBconn);
    end;
  end;
end;

procedure TevFirebird.DeleteClient(const ClNbr: Integer);
begin
  DropDB('CL_' + IntToStr(ClNbr));
end;

procedure TevFirebird.DoDropDB(const ADBPath: String);
var
  srvDBconn: IevServiceDBConnection;
  db_path, host: String;
begin
  db_path := ADBPath;
  host := GetNextStrValue(db_path, ':');

  srvDBconn := AcquireServiceDBConnection(host);
  try
    srvDBconn.Database.DeleteDatabaseFile(db_path);
  finally
    ReturnServiceDBConnection(srvDBconn);
  end;
end;

procedure TevFirebird.DropDB(const ADBName: String);
var
  dbConnInfo: TevDBConnectionInfo;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.DropDB');
    ctx_Statistics.ActiveEvent.Properties.AddValue('DB Name', ADBName);
  end;

  try
    dbConnInfo := GetDBConnectionInfo(ADBName + '.gdb');
    DoDropDB(dbConnInfo.Path);
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;

  SendBroadcastNotification('DBDELETED', ADBName);
end;

function TevFirebird.OpenQuery(const ADBType: TevDBType; const ASQL: String; const AParamVals: array of Variant; const ALoadBlobs: Boolean): IevDataSet;
var
  Q: TevIBSQL;
begin
  FDBOperLock.Lock;
  try
    Result := nil;
    Q := TevIBSQL.Create(Self, '', False);
    try
      Q.SQL.Text := ASQL;
      Q.Database := GetDBByType(ADBType);
      SetTransactionForQuery(Q);
      Q.SetParams(AParamVals);
      try
        Result := Q.ExecAsDataSet(ALoadBlobs);
      except
        on E: Exception do
          raise EevException.CreateDetailed(E.Message, BuildErrorDetails(Q), True);
      end;
    finally
      Q.Free;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;


procedure TevFirebird.ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
var
  Applier: TevDataSyncPacketApplier;
begin
  ctx_StartWait;
  try
    Applier := TevDataSyncPacketApplier.Create(Self);
    FDBOperLock.Lock;
    try
      Applier.Execute(ADBType, AData);
    finally
      FDBOperLock.Unlock;
      Applier.Free;
    end;
  finally
    ctx_EndWait;
  end;
end;

function TevFirebird.GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
var
  Extractor: TevDataSyncPacketCreator;
begin
  ctx_StartWait;
  try
    Extractor := TevDataSyncPacketCreator.Create(Self);
    FDBOperLock.Lock;
    try
      Result := Extractor.Execute(ADBType, AStartTransNbr, AEndTransNbr);
    finally
      FDBOperLock.Unlock;    
      Extractor.Free;
    end;
  finally
    ctx_EndWait;
  end;
end;

function TevFirebird.GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IevDataSet;
var
  Tbls: IisStringList;
  i: Integer;
  TblHash, SQL: String;
  Q: TevRawSQL;
  StartTransNbr: Integer;
  BlobFieldsNbrs: IisStringList;
  DB: TevDatabase;

  procedure PrepareResStructure;
  var
    FieldDefs: TevFieldDefs;
  begin
    FieldDefs := TevFieldDefs.Create(nil);
    try
      FieldDefs.Add('TableName', ftString, 64);
      FieldDefs.Add('RecordCount', ftInteger);
      FieldDefs.Add('Hash', ftString, 32);
      Result := TevDataSet.Create(FieldDefs);
      Result.LogChanges := False;
    finally
      FieldDefs.Free;
    end;
  end;

begin
  FDBOperLock.Lock;
  try
    ctx_StartWait('Initializing');
    try
      DB := GetDBByType(ADBType);

      // Start snapshot transaction
      if DB.DefaultTransaction.InTransaction then
        DB.DefaultTransaction.Commit;
      (DB.DefaultTransaction as TevTransaction).ReadCommited := False;

      Tbls := GetTableList(ADBType);

      PrepareResStructure;

      Q := TevRawSQL.Create(Self);
      try
        Q.Database := DB;
        SetTransactionForQuery(Q);

        // DB has some changes which would make hash calculation useless
        if (ARequiredLastTransactionNbr > 0) and (GetLastTransactionNbr(ADBType) <> ARequiredLastTransactionNbr) then
          Exit;

        // Identify list of transaction to hash
        SQL := 'SELECT FIRST 1 SKIP 9 nbr FROM ev_transaction ORDER BY commit_nbr DESC';
        Q.SQL.Text := SQL;
        Q.ExecQuery;
        StartTransNbr := Q.Fields[0].AsInteger;
        Q.Close;

        Tbls.Sort;
        for i := 0 to Tbls.Count - 1 do
        begin
          try
            ctx_UpdateWait('Hashing table ' + Tbls[i]);

            if AnsiSameText(Tbls[i], 'ev_database') then
              SQL := 'SELECT * FROM ev_database'

            else if AnsiSameText(Tbls[i], 'ev_transaction') then
              SQL := Format('SELECT * FROM ev_transaction WHERE nbr >= %d ORDER BY nbr', [StartTransNbr])

            else if AnsiSameText(Tbls[i], 'ev_table_change') then
              SQL := Format('SELECT * FROM ev_table_change WHERE ev_transaction_nbr >= %d ORDER BY nbr', [StartTransNbr])

            else if AnsiSameText(Tbls[i], 'ev_field_change') then
              SQL := Format(
                'SELECT fc.* FROM ev_field_change fc, ev_table_change tc'#13 +
                'WHERE'#13 +
                '  fc.ev_table_change_nbr = tc.nbr AND'#13 +
                '  tc.ev_transaction_nbr >= %d'#13 +
                'ORDER BY fc.ev_table_change_nbr, fc.ev_field_nbr',
                [StartTransNbr])

            else  if AnsiSameText(Tbls[i], 'ev_field_change_blob') then
            begin
              Q.SQL.Text := 'SELECT f.nbr FROM ev_field f WHERE f.field_type = ''B''';
              Q.ExecQuery;
              BlobFieldsNbrs := Q.AsDataSet(True).GetColumnValues('nbr');
              Q.Close;

              SQL := Format(
                'EXECUTE BLOCK'#13 +
                'RETURNS(nbr INTEGER, data BLOB SUB_TYPE 0 SEGMENT SIZE 80)'#13 +
                'AS'#13 +
                'DECLARE VARIABLE ref_nbr VARCHAR(512);'#13 +
                'BEGIN'#13 +
                '  FOR SELECT fld.old_value'#13 +
                '      FROM ev_table_change tbl, ev_field_change fld'#13 +
                '      WHERE'#13 +
                '        fld.ev_table_change_nbr = tbl.nbr AND'#13 +
                '        fld.ev_field_nbr IN (%s) AND'#13 +
                '        tbl.ev_transaction_nbr >= %d'#13 +
                '      ORDER BY fld.ev_table_change_nbr, fld.ev_field_nbr'#13 +
                '      INTO ref_nbr'#13 +
                '  DO'#13 +
                '  BEGIN'#13 +
                '    data = null;'#13 +
                '    nbr = CAST(ref_nbr AS INTEGER);'#13 +
                '    IF (nbr IS NOT NULL) THEN'#13 +
                '    BEGIN'#13 +
                '      SELECT nbr, data FROM ev_field_change_blob blb'#13 +
                '      WHERE blb.nbr = :nbr'#13 +
                '      INTO nbr, data;'#13 +
                ''#13 +
                '      SUSPEND;'#13 +
                '    END'#13 +
                '  END'#13 +
                'END',
                [BlobFieldsNbrs.CommaText, StartTransNbr]);
            end

            else if StartsWith(Tbls[i], 'ev_') then
              SQL := Format('SELECT * FROM %s ORDER BY nbr', [Tbls[i]])

            else
              SQL := Format('SELECT * FROM %s ORDER BY rec_version', [Tbls[i]]);

            Q.SQL.Text := SQL;
            Q.ExecQuery;
            AbortIfCurrentThreadTaskTerminated;

            TblHash := CalcHashFor(Q);

            Result.AppendRecord([Tbls[i], Q.RecordCount, TblHash]);
            Q.Close;

          except
            on E: Exception do
            begin
              E.Message := 'Cannot hash data of table ' + Tbls[i] + #13 + E.Message;
              raise;
            end;
          end;

          AbortIfCurrentThreadTaskTerminated;
        end;
      finally
        Q.Free;
      end;
    finally
      ctx_EndWait;
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.GetTableList(const ADBType: TevDBType): IisStringList;
var
  DB: TevDatabase;
begin
  DB := GetDBByType(ADBType);
  DB.InternalTransaction.DefaultDatabase := DB;
  Result := TisStringList.Create;
  DB.GetTableNames(Result.VCLStrings);
end;

function TevFirebird.CalcHashFor(const ASQL: TevRawSQL): String;
var
  Data: IisStream;

  procedure SaveDataToStream;
  const
    NullMarker = 0;
    NotNullMarker = 1;
  var
    i: Integer;
    s: String;
  begin
    ASQL.GetFieldDefs;
    Data.WriteInteger(ASQL.FFieldDefs.Count);

    for i := 0 to ASQL.FFieldDefs.Count - 1 do
    begin
      Data.WriteString(ASQL.FFieldDefs[i].Name);
      Data.WriteInteger(Ord(ASQL.FFieldDefs[i].DataType));
      Data.WriteInteger(Ord(ASQL.FFieldDefs[i].Size));
      Data.WriteInteger(Ord(ASQL.FFieldDefs[i].Precision));
    end;

    while not ASQL.Eof do
    begin
      for i := 0 to ASQL.FieldCount - 1 do
      begin
        if ASQL.Fields[i].IsNull or
           (ASQL.Fields[i].SqlVar.SqlDef = SQL_BLOB) and (ASQL.Fields[i].AsString = '') then  // "thanks" to Borland for treating '' as a null in TBlobField
          Data.WriteByte(NullMarker)
        else
        begin
          s := ASQL.Fields[i].AsString;
          if ASQL.Fields[i].SqlVar.SqlDef = SQL_VARYING then
            s := TrimRight(s); // Varchar ignores trailing spaces in comparison operation
          if (s = '') or TextIsVisiallyEmpty(s) then
            Data.WriteByte(NullMarker)          
          else
          begin
            Data.WriteByte(NotNullMarker);
            Data.WriteString(s);
          end;
        end;
      end;

      AbortIfCurrentThreadTaskTerminated;

      ASQL.Next;
    end;
  end;

begin
  Data := TisStream.Create;
  SaveDataToStream;
  Result := CalcMD5(Data.RealStream);
end;

procedure TevFirebird.CheckDBHash(const ADBType: TevDBType; const ADBInfo: IevDataSet; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
var
  TargetData: IevDataSet;
  Tbl: String;
begin
  ctx_StartWait;
  try
    TargetData := GetDBHash(ADBType, ARequiredLastTransactionNbr);
    CheckCondition(Assigned(TargetData), 'Ev_Transaction does not match');
    CheckCondition(TargetData.RecordCount = ADBInfo.RecordCount, 'Table list does not match');
    AbortIfCurrentThreadTaskTerminated;

    ctx_UpdateWait('Analyzing data');
    // Compare table by table
    AErrors := '';
    ADBInfo.First;
    while not ADBInfo.Eof do
    begin
      Tbl := ADBInfo.FieldValues['TableName'];
      CheckCondition(TargetData.Locate('TableName', Tbl, [loCaseInsensitive]), 'Table list does not match');

      if TargetData.FieldValues['RecordCount'] <> ADBInfo.FieldValues['RecordCount'] then
        AddStrValue(AErrors, Tbl + ': Record count does not match', #13)
      else if TargetData.FieldValues['Hash'] <> ADBInfo.FieldValues['Hash'] then
        AddStrValue(AErrors, Tbl + ': Data does not match', #13);

      ADBInfo.Next;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TevFirebird.SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
var
  L: IisStringList;
begin
  L := TisStringList.Create;
  L.Add(aDBName);
  if aMaintenance then
    DisableDBs(L)
  else
    EnableDBs(L);
end;

procedure TevFirebird.EnableDBs(const ADBList: IisStringList);
begin
  DBConnectionPool.EnableDBs(ADBList);
end;

procedure TevFirebird.DisableDBs(const ADBList: IisStringList);
begin
  DBConnectionPool.DisableDBs(ADBList);
  mb_Messenger.Broadcast(IM_DROP_DISABLED_DB_CONNECTIONS);
end;

procedure TevFirebird.Notify(const AMessage: String; const AParams: IisListOfValues);
begin
  if AMessage = IM_DROP_DISABLED_DB_CONNECTIONS then
    GlobalThreadManager.RunTask(DropCachedDBConnectionsForDisabledDBs, Self, MakeTaskParams([]), 'DropCachedDBConnectionsForDisabledDBs')
end;

procedure TevFirebird.DoOnDestruction;
begin
  if Mainboard <> nil then
    mb_Messenger.Unsubscribe(Self, []);

  DropCachedDBConnections;
  FreeAndNil(FReadWriteTransaction);
  FreeAndNil(FSQLObjects);
  inherited;
end;

procedure TevFirebird.SweepDB(const ADBName: String);
var
  dbConnInfo: TevDBConnectionInfo;
  srvDBconn: IevServiceDBConnection;
  db_path, host: String;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('Sweep DB');
    ctx_Statistics.ActiveEvent.Properties.AddValue('DB Name', ADBName);
  end;

  ctx_StartWait('Initializing');
  try
    dbConnInfo := GetDBConnectionInfo(ADBName);
    db_path := dbConnInfo.Path;
    host := GetNextStrValue(db_path, ':');

    srvDBconn := AcquireServiceDBConnection(host);
    try
      ctx_UpdateWait('Sweeping database');
      Mainboard.GlobalCallbacks.NotifySetDBMaintenance(ADBName, True, ctx_DBAccess.GetADRContext);
      try
        srvDBconn.Database.DefragmentDatabase(db_path);
      finally
        Mainboard.GlobalCallbacks.NotifySetDBMaintenance(ADBName, False, ctx_DBAccess.GetADRContext);
      end;
      ctx_UpdateWait('');
    finally
      ReturnServiceDBConnection(srvDBconn);
    end;
  finally
    ctx_EndWait;
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

procedure TevFirebird.RunGBAK(const AParams: String);
var
  Cmd, s, sErr: String;
  Err: IisStream;
  LastUnfinishedLine: String;

  procedure ProgressCallback(const AData: Pointer; const ALength: Integer; const AContextData: Pointer);
  var
    sFragment, s: String;
  begin
    SetLength(sFragment, ALength);
    CopyMemory(@sFragment[1], AData, ALength);
    sFragment := PString(AContextData)^ + sFragment;
    s := GetPrevStrValue(sFragment, #13);
    sFragment := GetPrevStrValue(sFragment, #13);
    if sFragment = '' then
      PString(AContextData)^ := PString(AContextData)^ + s
    else
    begin
      PString(AContextData)^ := s;
      sFragment := StringReplace(sFragment, #10, '', [rfReplaceAll]);
      ctx_UpdateWait(sFragment);
    end;
  end;

begin
  ctx_UpdateWait('Executing GBAK');

  Cmd := AppDir + 'gbak.exe';
  if not FileExists(cmd) then
  begin
    Cmd := GetIBPath + 'gbak.exe';
    CheckCondition(FileExists(Cmd), 'gbak.exe utility is not found');
  end;

  // GBAK writes text messages in STDERR not in STDOUT!
  Err := TisStream.Create;
  LastUnfinishedLine := '';

  if not RunProcess(Cmd, AParams, [roHideWindow, roWaitForClose], nil, Err, nil, @ProgressCallback, @LastUnfinishedLine) then
  begin
    Err.Position := 0;
    sErr := '';
    while not Err.EOS do
    begin
      s := Err.ReadLn;
      if StartsWith(s, 'gbak: ERROR:', True) then
        AddStrValue(sErr, Copy(s, 13, Length(s)), #13);
    end;
    if sErr = '' then
      sErr := 'Process has terminated abnormally';

    raise EevException.CreateDetailed('GBAK error', sErr);
  end;

  ctx_UpdateWait('');
end;

function TevFirebird.GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
var
  DS: IevDataSet;
begin
  FDBOperLock.Lock;
  try
    DS := ExecRawQuery(
      ADBType,
      'EXECUTE BLOCK'#13 +
      'RETURNS (last_trans INTEGER, last_trans_count INTEGER)'#13 +
      'AS'#13 +
      'DECLARE VARIABLE last_commit_time TIMESTAMP;'#13 +
      'BEGIN'#13 +
      '  SELECT MAX(commit_nbr) FROM ev_transaction INTO last_trans;'#13 +
      '  SELECT commit_time FROM ev_transaction WHERE commit_nbr = :last_trans INTO last_commit_time;'#13 +
      '  SELECT COUNT(*) FROM ev_transaction WHERE commit_time BETWEEN (:last_commit_time - 0.041666667) AND :last_commit_time INTO last_trans_count;'#13 +
      ''#13 +
      '  SUSPEND;'#13 +
      'END', [], True);

    Result := TisListOfValues.Create;
    Result.AddValue('LastTransNbr', DS.Fields[0].AsInteger);
    Result.AddValue('TransCount', DS.Fields[1].AsInteger);
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.GetUseReadTransaction: Boolean;
begin
  Result := FUseReadTransaction;
end;

procedure TevFirebird.SetUseReadTransaction(const AValue: Boolean);
begin
  FUseReadTransaction := AValue;
end;

procedure TevFirebird.FlushTransactionChanges;
begin
  // This is a back door for rebuild temp tables logic
  // It is a good practice to commit every 500 INSERT operations
  if FReadWriteTransaction.InTransaction then
  begin
    Assert(FReadWriteTransaction.DatabaseCount = 1);
    Assert((FReadWriteTransaction.Databases[0] as TevDatabase).DBType = dbtTemporary);

    FReadWriteTransaction.Commit;
    FReadWriteTransaction.StartTransaction;
  end;
end;


function TevFirebird.SetTransactionVariableSQL(const AVarName: String; const AValue: Variant): String;
var
  v: String;
begin
  if AValue = Null then
    v := 'NULL'
  else
    v := '''' + String(AValue) + '''';

  Result := Format('execute block as begin rdb$set_context(''USER_TRANSACTION'', ''%s'', %s); end', [AVarName, v]);
end;


function TevFirebird.GetChangedTablesInTransaction(const ADBType: TevDBType;
  const ATableNameFilter: IisStringList): IevDataSet;
var
  DS: IevDataSet;
  CurrentTranID: Integer;
  sTableFilter: String;
  i: Integer;
  DBInTransaction: Boolean;
begin
  FDBOperLock.Lock;
  try
    DBInTransaction := False;
    if Assigned(FReadWriteTransaction) then
      for i := 0 to FReadWriteTransaction.DatabaseCount - 1 do
        if TevDatabase(FReadWriteTransaction.Databases[i]).DBType = ADBType then
        begin
          DBInTransaction := True;
          break;
        end;

    if not DBInTransaction then
    begin
      Result := nil;
      exit;
    end;

    DS := OpenQuery(ADBType, 'SELECT CAST(rdb$get_context(''USER_TRANSACTION'', ''TRN_ID'') AS INTEGER) FROM ev_database', [], True);
    CurrentTranID := DS.Fields[0].AsInteger;
    DS := nil;

    if Assigned(ATableNameFilter) and (ATableNameFilter.Count > 0) then
    begin
      sTableFilter := '';
      for i := 0 to ATableNameFilter.Count - 1 do
        AddStrValue(sTableFilter, '''' + AnsiUpperCase(ATableNameFilter[i]) + '''', ',');
      sTableFilter := 'AND t.name IN (' + sTableFilter + ')';
    end
    else
      sTableFilter := '';

    Result := OpenQuery(ADBType,
      'SELECT DISTINCT t.name table_name, record_nbr nbr'#13 +
      'FROM ev_table_change tc, ev_table t'#13 +
      'WHERE'#13 +
      '  tc.ev_table_nbr = t.nbr AND'#13 +
      '  tc.ev_transaction_nbr = :tran_id ' + sTableFilter,
      [CurrentTranID], True);

    if Result.RecordCount = 0 then
      Result := nil;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.SyncDBAndApp(const ADBName: String): Boolean;
begin
  if StartsWith(ADBName, DB_Cl_Base) then
  begin
    Result := CheckAndUpdateClBase;
    Exit;
  end;

  Result := PatchDB(ADBName, AppVersion, False);
end;

type
  TCallBackData = record
    ExecutedPartSize: Integer;
    LastActivity: String;
    IncomingActivityFragment: String;
    OriginalScriptSize: Integer;
    ProgressCaption: String;
  end;

procedure TevFirebird.RunISQL(const AParams: String; const AProgressCaption: String);
var
  Cmd, s, sErr: String;
  Err: IisStream;
  cbData: TCallBackData;

  procedure ProgressCallback(const AData: Pointer; const ALength: Integer; const AContextData: Pointer);
  var
    fragment: String;
    i: Integer;
  begin
    Inc(TCallBackData(AContextData^).ExecutedPartSize, ALength);

    SetLength(fragment, ALength);
    CopyMemory(@fragment[1], AData, ALength);
    TCallBackData(AContextData^).IncomingActivityFragment := TCallBackData(AContextData^).IncomingActivityFragment + fragment;
    fragment := TCallBackData(AContextData^).IncomingActivityFragment;

    while fragment <> '' do
    begin
      GetNextStrValue(fragment, '/*<progress>');
      if fragment <> '' then
      begin
        TCallBackData(AContextData^).IncomingActivityFragment := '/*<progress>' + fragment;
        i := Pos('</progress>*/', fragment);
        if i > 0 then
        begin
          TCallBackData(AContextData^).LastActivity := Copy(fragment, 1, i - 1);
          Delete(fragment, 1, i + 4);
          TCallBackData(AContextData^).IncomingActivityFragment := fragment;
        end
        else
          fragment := '';
      end;
    end;

    ctx_UpdateWait(Format(TCallBackData(AContextData^).ProgressCaption + '   %d%%   %s',
      [Round((TCallBackData(AContextData^).ExecutedPartSize / TCallBackData(AContextData^).OriginalScriptSize) * 100),
       TCallBackData(AContextData^).LastActivity]),
      TCallBackData(AContextData^).ExecutedPartSize, TCallBackData(AContextData^).OriginalScriptSize);
  end;

begin
  Cmd := AppDir + 'isql.exe';
  if not FileExists(cmd) then
  begin
    Cmd := GetIBPath + 'isql.exe';
    CheckCondition(FileExists(Cmd), 'isql.exe utility is not found');
  end;

  s := AParams;
  GetNextStrValue(s, ' -i "');
  s := GetNextStrValue(s, '"');
  cbData.OriginalScriptSize := Integer(GetFileInfo(s).Size);

  Err := TisStream.Create;
  cbData.ExecutedPartSize := 0;
  cbData.LastActivity := '';
  cbData.IncomingActivityFragment := '';  
  cbData.ProgressCaption := AProgressCaption;

  if not RunProcess(Cmd, AParams, [roHideWindow, roWaitForClose],  nil, Err, @ProgressCallback, nil, @cbData) then
  begin
    Err.Position := 0;
    sErr := '';
    while not Err.EOS do
    begin
      s := Err.ReadLn;
      AddStrValue(sErr, s, #13);
    end;
    if sErr = '' then
      sErr := 'Process has terminated abnormally';

    raise EevException.CreateDetailed('ISQL error', sErr);
  end;

  ctx_UpdateWait('');
end;

function TevFirebird.AcquireServiceDBConnection(const AHost: string): IevServiceDBConnection;
begin
  Result := DBConnectionPool.AcquireObject(AHost + ':' + DB_Service) as IevServiceDBConnection;
  Result.Database.FCurrentOwner := Self;
  Result.Database.EnsureDatabaseConnection;
end;

procedure TevFirebird.ReturnServiceDBConnection(const ADBConnection: IevServiceDBConnection);
var
  i: Integer;
begin
  ADBConnection.Database.FCurrentOwner := nil;
  if ADBConnection.Database.Connected then
  begin
    for i := 0 to ADBConnection.Database.TransactionCount - 1 do
      if ADBConnection.Database.Transactions[i].InTransaction then
        ADBConnection.Database.Transactions[i].Rollback;

    DBConnectionPool.ReturnObject(ADBConnection);
  end;
end;

procedure TevFirebird.ImportDatabase(const ADBPath, ADBPassword: String);
var
  conn: TevDBConnectionInfo;
  srvDBconn: IevServiceDBConnection;
  db_path, host: String;
begin
  conn := GetDBConnectionInfo(ChangeFileExt(ExtractFileNameUD(ADBPath), ''));

  db_path := conn.Path;
  host := GetNextStrValue(db_path, ':');

  srvDBconn := AcquireServiceDBConnection(host);
  try
    ctx_UpdateWait('Copying database');
    srvDBconn.Database.CopyDatabase(ADBPath, FB_Admin, ADBPassword, db_path);
  finally
    ctx_UpdateWait('');
    ReturnServiceDBConnection(srvDBconn);
  end;
end;

function TevFirebird.GetDBServerFolderContent(const APath, AUser, APassword, AMask: String; const AFileSize: Boolean): IevDataSet;
var
  srvDBconn: IevServiceDBConnection;
  DB: TevServiceDatabase;
  host, local_path, s: String;
begin
  DB := nil;
  try
    try
      local_path := APath;
      host := GetNextStrValue(local_path, ':');
      srvDBconn := AcquireServiceDBConnection(host);  // First try to find connection in the pool if host belongs this environment
      DB := srvDBconn.Database;
    except
      srvDBconn := nil;
      DB := TevServiceDatabase.Create;  // Create a service database here if host is unknown by this environment
      DB.FCurrentOwner := Self;
      DB.SetupConnection(APath, AUser, APassword);
      DB.EnsureDatabaseConnection(False);
      try
        DB.GetUDFsVersion;
      except
        on E: Exception do
        begin
          s := 'EvolUDFs file is incompatible or missing';
          mb_LogFile.AddContextEvent(etError, 'DB Access', s, MakeFullError(E));
          E.Message := s;
          raise;
        end;
      end;
    end;

    if AFileSize then
      Result := DB.GetFolderContentAndSize(local_path, AMask)    
    else
      Result := DB.GetFolderContent(local_path, AMask);
  finally
    if Assigned(srvDBconn) then
      ReturnServiceDBConnection(srvDBconn)
    else
    begin
      try
        DB.DropDatabase; // Drop the service database for unknown host
      except
      end;
      DB.Free;
    end;
  end;
end;

procedure TevFirebird.SyncUDFAndApp;
var
  i: Integer;
  DBList: IisListOfValues;
  srvDBconn: IevServiceDBConnection;
begin
  DBList := GetAllDBHosts(False);
  for i := 0 to DBList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
      srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
      if srvDBconn.Database.CheckAndUpdateUDFs then
        srvDBconn := nil                      // EvolUDFs was updated, so connection cannot be reused
      else
        ReturnServiceDBConnection(srvDBconn); // Will not return it back to pool in case of error
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

  DBList := GetAllDBHosts(True);
  for i := 0 to DBList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
      srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
      if srvDBconn.Database.CheckAndUpdateUDFs then
        srvDBconn := nil                      // EvolUDFs was updated, so connection cannot be reused
      else
        ReturnServiceDBConnection(srvDBconn); // Will not return it back to pool in case of error
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;
end;

function TevFirebird.GetDBFilesList: IevDataSet;
const
  ResultDSStruct: array [0..1] of TDSFieldDef = (
    (FieldName: 'file_name'; DataType: ftString; Size: 512; Required: False),
    (FieldName: 'file_size'; DataType: ftLargeint; Size: 0; Required: False));
var
  DBLocList: IevDBLocationList;
  i: Integer;
  PathList: IisStringList;
  FolderContentDS: IevDataSet;
  s: String;
begin
  PathList := TisStringList.CreateUnique;

  if ctx_DBAccess.GetADRContext then
    DBLocList := ctx_DomainInfo.ADRDBLocationList  
  else
    DBLocList := ctx_DomainInfo.DBLocationList;

  for i := 0 to DBLocList.Count - 1 do
    PathList.Add(NormalizePath(DBLocList[i].Path));

  Result := TevDataSet.Create(ResultDSStruct);
  Result.LogChanges := False;

  for i := 0 to PathList.Count - 1 do
  begin
    FolderContentDS := GetDBServerFolderContent(PathList[i], '', '', '', True);

    FolderContentDS.First;
    while not FolderContentDS.Eof do
    begin
      s := FolderContentDS['file_name'];
      if not StartsWith(s, AnsiUpperCase(DB_Service), True) and FinishesWith(s, '.gdb', True) then
        Result.AppendRecord([PathList[i] + s, FolderContentDS['file_size']]);
      FolderContentDS.Next;
    end;
  end;
  Result.First;
end;

procedure TevFirebird.TidyUpDBFolders;
var
  i: Integer;
  srvDBconn: IevServiceDBConnection;
  host, local_path: String;
  DBLocList: IevDBLocationList;
begin
  DBLocList := ctx_DomainInfo.DBLocationList;

  for i := 0 to DBLocList.Count - 1 do
  begin
    local_path := DBLocList[i].Path;
    host := GetNextStrValue(local_path, ':');
    srvDBconn := AcquireServiceDBConnection(host);
    try
      srvDBconn.Database.TidyUpDBFolder(local_path);
    finally
      ReturnServiceDBConnection(srvDBconn);
    end;
  end;
end;

procedure TevFirebird.ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
begin
end;

procedure TevFirebird.UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
begin

end;

procedure TevFirebird.InitializeTTForFullRebuild;
var
  dbPath: String;
  conn: TevDBConnectionInfo;
begin
  FDBOperLock.Lock;
  try
    FTempDBConn := nil; // To make sure that temp database has no cached connections

    conn := GetDBConnectionInfo(DB_TempTables);
    dbPath := conn.Path + rebuildExt;
    DoRestoreDB(AppDir + 'TMP_TBLS.gbk', dbPath);
  finally
    FDBOperLock.Unlock;
  end;
end;

procedure TevFirebird.FinalizeTTAfterFullRebuild;
var
  dbPath, host, s: String;
  conn: TevDBConnectionInfo;
  srvDBconn: IevServiceDBConnection;
begin
  FDBOperLock.Lock;
  try
    FTempDBConn := nil; // To make sure that .rebuild database has no cached connections

    conn := GetDBConnectionInfo(DB_TempTables);
    dbPath := conn.Path;
    host := GetNextStrValue(dbPath, ':');
    dbPath := conn.Path + rebuildExt;

    srvDBconn := AcquireServiceDBConnection(host);
    try
      try
        GetNextStrValue(dbPath, ':');
        s := conn.Path;
        GetNextStrValue(s, ':');
        srvDBconn.Database.MoveDatabaseFile(dbPath, s);
      except
        try
          srvDBconn.Database.DeleteDatabaseFile(dbPath);
        except
        end;
        raise;
      end;
    finally
      ReturnServiceDBConnection(srvDBconn);
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;



procedure TevFirebird.ChangeFBAdminPassword(const ANewPassword: String);
var
  i: Integer;
  DBList: IisListOfValues;
  srvDBconn: IevServiceDBConnection;
begin
  DBList := GetAllDBHosts(False);
  for i := 0 to DBList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
      srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
      try
        srvDBconn.Database.ChangeFBAdminPassword(ANewPassword);
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

  DBList := GetAllDBHosts(True);
  for i := 0 to DBList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
      srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
      try
        srvDBconn.Database.ChangeFBAdminPassword(ANewPassword);
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;
end;

procedure TevFirebird.ChangeFBUserPassword(const ANewPassword: String);
var
  i: Integer;
  DBList: IisListOfValues;
  srvDBconn: IevServiceDBConnection;
begin
  DBList := GetAllDBHosts(False);
  for i := 0 to DBList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
      srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
      try
        srvDBconn.Database.ChangeFBUserPassword(ANewPassword);
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

  DBList := GetAllDBHosts(True);
  for i := 0 to DBList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
      srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
      try
        srvDBconn.Database.ChangeFBUserPassword(ANewPassword);
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;
end;

function TevFirebird.GetAllDBHosts(const ADR: Boolean): IisListOfValues;

  procedure CheckDBLocations(const ADomainName: String; const ADBLocations: IevDBLocationList);
  var
    i: Integer;
    s: String;
  begin
    for i := 0 to ADBLocations.Count - 1 do
    begin
      s := ADBLocations[i].Path;
      s := GetNextStrValue(s, ':');
      Result.AddValue(s, ADomainName);
    end;
  end;

var
  i: Integer;
  Domains: IevDomainInfoList;
begin
  Result := TisListOfValues.Create;

  Domains := (mb_GlobalSettings.DomainInfoList as IisInterfacedObject).GetClone as IevDomainInfoList;
  for i := 0 to Domains.Count - 1 do
  begin
    if ADR then
      CheckDBLocations(Domains[i].DomainName, Domains[i].ADRDBLocationList)
    else
      CheckDBLocations(Domains[i].DomainName, Domains[i].DBLocationList);
  end;
end;

function TevFirebird.CreateRawSQL(const ASQL: String; const ADBType: TevDBType): IevSQL;
begin
  FDBOperLock.Lock;
  try
    Result := TevSQL.CreateRaw(Self, ASQL, ADBType);
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.ExecRawQuery(const ADBType: TevDBType; const ASQL: String; const AParamVals: array of Variant;
  const ALoadBlobs: Boolean): IevDataSet;
var
  Q: IevSQL;
begin
  FDBOperLock.Lock;
  try
    Result := nil;
    Q := CreateRawSQL(ASQL, ADBType);
    try
      Q.SetParams(AParamVals);
      Result := Q.Open(ALoadBlobs);
    except
      on E: Exception do
        raise EevException.CreateDetailed(E.Message, BuildErrorDetails(Q), True);
    end;
  finally
    FDBOperLock.Unlock;
  end;
end;

function TevFirebird.BuildErrorDetails(const AQuery: IevSQL): String;
begin
  BuildErrorDetails(((AQuery as IisInterfacedObject).GetImplementation as TevSQL).FSQLObj);
end;

function TevFirebird.GetDBServersInfo: IisParamsCollection;
var
  DBList: IisListOfValues;
  SysInfo: IisStringList;

  procedure Process;
  var
    i, j: Integer;
    srvDBconn: IevServiceDBConnection;
    Info: IisListOfValues;
    s, vName: String;
  begin
    for i := 0 to DBList.Count - 1 do
    begin
      Mainboard.ContextManager.StoreThreadContext;
      try
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, DBList[i].Value), '');
        srvDBconn := AcquireServiceDBConnection(DBList[i].Name);
        try
          try
            SysInfo.Text := srvDBconn.Database.GetSystemInfo;
          except
            SysInfo.Clear; // It is not that critical information, so errors should not blow up the whole thing
          end;
        finally
          Mainboard.ContextManager.RestoreThreadContext;
        end;
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;

      if (SysInfo.Count > 0) and (Result.ParamsByName(DBList[i].Name) = nil) then
      begin
        Info := Result.AddParams(DBList[i].Name);
        for j := 0 to SysInfo.Count - 1 do
        begin
          s := SysInfo[j];
          vName := GetNextStrValue(s, '=');
          Info.AddValue(vName, s);
        end;
      end;
    end;
  end;

begin
  Result := TisParamsCollection.Create;
  SysInfo := TisStringList.Create;

  DBList := GetAllDBHosts(False);
  Process;
  
  DBList := GetAllDBHosts(True);
  Process;
end;

function TevFirebird.CheckAndUpdateClBase: Boolean;
var
  L: IevDBLocationList;
  i: Integer;
  full_db_path, db_path, db_bkp, host: String;
  BkpData: IisStream;
  srvDBconn: IevServiceDBConnection;
  ver: TisVersionInfo;
  Err: String;
begin
  Result := False;
  Err := '';

  if ctx_DBAccess.GetADRContext then
    L := ctx_DomainInfo.ADRDBLocationList
  else
    L := ctx_DomainInfo.DBLocationList;

  for i := 0 to L.Count - 1 do
  begin
    full_db_path := NormalizePath(L[i].Path) + UpperCase(DB_Cl_Base) + '.gdb';
    ctx_UpdateWait('Checking ' + full_db_path);

    ver := TryToGetDBVersion(full_db_path);
    if (ver.Major <> AppDBVersions.Current.ClientDBVersion.Major) or (ver.Minor <> AppDBVersions.Current.ClientDBVersion.Minor) or
       (ver.Patch <> AppDBVersions.Current.ClientDBVersion.Patch) or (ver.Build <> AppDBVersions.Current.ClientDBVersion.Build) then
    begin
      ctx_UpdateWait('Creating ' + full_db_path);

      db_bkp := AppDir + UpperCase(DB_Cl_Base) + '.gbk';
      BkpData := TisStream.CreateFromFile(db_bkp);
      db_path := full_db_path;
      host := GetNextStrValue(db_path, ':');

      srvDBconn := AcquireServiceDBConnection(host);
      try
        try
          srvDBconn.Database.RestoreDatabase(db_path, BkpData, False);
        except
          on E: Exception do
            AddStrValue(Err, full_db_path + ': ' + E.Message, #13);
        end;
        Result := True;
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    end;
  end;

  ctx_UpdateWait('');
    
  if Err <> '' then
    raise EevException.CreateDetailed('Cannot create CL_BASE.gdb', Err);
end;

function TevFirebird.TryToGetDBVersion(const ADBPath: String): TisVersionInfo;
var
  Tran: TIBTransaction;
  DB: TevCustomDatabase;
begin
  try
    Tran := TIBTransaction.Create(nil);
    DB := TevCustomDatabase.Create; // exception of rules! DB pool should not be envolved
    try
      DB.DatabaseName := ADBPath;
      DB.Params.Clear;
      DB.Params.Add('USER_NAME=' + FB_Admin);
      DB.Params.Add('PASSWORD=' + mb_GlobalSettings.DBAdminPassword);

      DB.DefaultTransaction := Tran;
      Result := DB.DBVersion;
    finally
      if DB.Connected and Tran.Active then
        Tran.Rollback;
      DB.Free;
      Tran.Free;
    end;
  except
    on E: EIBInterBaseError do
    begin
      Result.Major := 0;
      Result.Minor := 0;
      Result.Patch := 0;
      Result.Build := 0;
    end
    else
      raise;  
  end;
end;

function TevFirebird.PatchDB(const aDBName, aAppVersion: String; AllowDowngrade: Boolean): Boolean;
const
  ResTypes: array [Low(TevDBType)..High(TevDBType)] of string = ('', '', 'SYSTEM_DB', 'BUREAU_DB', 'CLIENT_DB', 'TEMP_DB');
var
  srvDBconn: IevServiceDBConnection;
  conn: TevDBConnectionInfo;
  db_path, host, script_file: String;
  PatchList: IisStringList;
  script, oneVerScript: IisStream;
  Tran: TIBTransaction;
  DB: TevDatabase;
  resType: String;
  i: Integer;
  bDowngrade, bErr: Boolean;
  AppDBVerCompatibilityInfo: TevAppDBVerInfo;
  curDbVer, requiredDbVer, ver: TisVersionInfo;
  DBType: TevDBType;
begin
  Result := False;

  Mainboard.GlobalCallbacks.NotifySetDBMaintenance(ADBName, True, ctx_DBAccess.GetADRContext);
  try
    ctx_UpdateWait('Analyzing database');

    // Get DB version
    Tran := TIBTransaction.Create(nil);
    DB := TevDatabase.Create; // exception of rules! DB pool should not be envolved
    try
      DB.EvDBName := ADBName;
      DB.DefaultTransaction := Tran;
      curDbVer := DB.DBVersion;
      DBType := DB.DBType;
    finally
      if DB.Connected and Tran.Active then
        Tran.Rollback;
      DB.Free;
      Tran.Free;
    end;

    // Find out the required DB version
    AppDBVerCompatibilityInfo := AppDBVersions.GetCompatibilityInfo(aAppVersion);
    case DBType of
      dbtSystem:     RequiredDbVer := AppDBVerCompatibilityInfo.SystemDBVersion;
      dbtBureau:     RequiredDbVer := AppDBVerCompatibilityInfo.BureauDBVersion;
      dbtClient:     RequiredDbVer := AppDBVerCompatibilityInfo.ClientDBVersion;
      dbtTemporary:  RequiredDbVer := AppDBVerCompatibilityInfo.TempDBVersion;
    else
      Assert(False);
    end;

    if CompareVersions(curDbVer, RequiredDbVer) = EqualsValue then
      Exit // Database is up to date
    else
      bDowngrade := CompareVersions(curDbVer, RequiredDbVer) = GreaterThanValue;

    if bDowngrade and not AllowDowngrade then
      raise EevException.Create('Cannot downgrade DB structure. The operation is not allowed. Please, contact iSystems.');

    // Get list of incremental patches
    resType := ResTypes[DBType];
    if bDowngrade then
      resType := 'D_' + resType
    else
      resType := 'U_' + resType;

    PatchList := GetResourceList(resType);
    PatchList.Sort;

    if bDowngrade then
    begin
      if (PatchList.Count = 0) then
        bErr := True
      else
      begin
        ver := StrToVersionRec(StringReplace(PatchList[0], '_', '.', [rfReplaceAll]));
        if CompareVersions(curDbVer, ver) <> GreaterThanValue then
          bErr := True
        else
        begin
          ver := StrToVersionRec(StringReplace(PatchList[PatchList.Count - 1], '_', '.', [rfReplaceAll]));
          bErr := CompareVersions(requiredDbVer, ver) = GreaterThanValue;
        end;
      end;

      if bErr then
        raise EevException.Create('Cannot downgrade DB structure. Please use newer Evolution version.');
    end;

    // Combine inremental patches into one script file
    script_file := AppTempFolder + GetUniqueID + '.sql';
    script := TisStream.CreateFromNewFile(script_file);
    try
      if bDowngrade then
      begin
        for i := PatchList.Count - 1 downto 0 do
        begin
          ver := StrToVersionRec(StringReplace(PatchList[i], '_', '.', [rfReplaceAll]));
          if (CompareVersions(ver, requiredDbVer) <> LessThanValue) and
             (CompareVersions(ver, curDbVer) = LessThanValue) then
          begin
            script.WriteLn(Format('/************** Downgrade to version %s **************/', [VersionRecToStr(ver)]));
            oneVerScript := GetBinaryResource(PatchList[i], resType);
            script.CopyFrom(oneVerScript, 0);
            oneVerScript := nil;
            script.WriteLn('');
          end;
        end;
      end

      else
      begin
        for i := 0 to PatchList.Count - 1 do
        begin
          ver := StrToVersionRec(StringReplace(PatchList[i], '_', '.', [rfReplaceAll]));
          if (CompareVersions(ver, requiredDbVer) <> GreaterThanValue) and
             (CompareVersions(ver, curDbVer) = GreaterThanValue) then
          begin
            script.WriteLn(Format('/************** Patch to version %s **************/', [StringReplace(PatchList[i], '_', '.', [rfReplaceAll])]));
            oneVerScript := GetBinaryResource(PatchList[i], resType);
            script.CopyFrom(oneVerScript, 0);
            oneVerScript := nil;
            script.WriteLn('');
          end;
        end;
      end;
      ErrorIf(script.Size = 0, 'Cannot find target patch for database version ' + VersionRecToStr(requiredDbVer), EevException);

      script := nil; // close file

      ctx_UpdateWait('Preparing for pathing');

      conn := GetDBConnectionInfo(ADBName);
      db_path := conn.Path;
      host := GetNextStrValue(db_path, ':');

      srvDBconn := AcquireServiceDBConnection(host);
      try
        srvDBconn.Database.PatchDatabase(db_path, script_file, Format('%s -> %s', [VersionRecToStr(curDbVer), VersionRecToStr(requiredDbVer)]));
      finally
        ReturnServiceDBConnection(srvDBconn);
      end;
    finally
      DeleteFile(script_file);
    end;

  finally
    Mainboard.GlobalCallbacks.NotifySetDBMaintenance(ADBName, False, ctx_DBAccess.GetADRContext);
  end;

  Result := True;
end;

function TevFirebird.CustomScriptDB(const aDBName, aScript: String): Boolean;
var
  srvDBconn: IevServiceDBConnection;
  conn: TevDBConnectionInfo;
  db_path, host: String;
begin

  Mainboard.GlobalCallbacks.NotifySetDBMaintenance(ADBName, True, ctx_DBAccess.GetADRContext);
  try
    ctx_UpdateWait('Analyzing database');

    conn := GetDBConnectionInfo(ADBName);
    db_path := conn.Path;
    host := GetNextStrValue(db_path, ':');

    srvDBconn := AcquireServiceDBConnection(host);
    try
      srvDBconn.Database.PatchDatabase(db_path, aScript, 'Custom Script');
    finally
      ReturnServiceDBConnection(srvDBconn);
    end;

  finally
    Mainboard.GlobalCallbacks.NotifySetDBMaintenance(ADBName, False, ctx_DBAccess.GetADRContext);
  end;

  Result := True;
end;

{ TEvDatabase }

procedure TevDatabase.CheckDBVersion;

  function VersionRecToStr(const AVerInfo: TisVersionInfo): String;
  begin
    Result := IntToStr(AVerInfo.Major) + '.' + IntToStr(AVerInfo.Minor) + '.' + IntToStr(AVerInfo.Patch);

    if AVerInfo.Build > 0 then
      Result := Result + '.' + IntToStr(AVerInfo.Build)
    else
      Result := Result + '.x';
  end;

  procedure CompareMetadataVersions(const AThisVersion, ARequiredVersion: TisVersionInfo);
  var
    bValid: Boolean;
  begin
    bValid := (ARequiredVersion.Major = AThisVersion.Major) and
              (ARequiredVersion.Minor = AThisVersion.Minor) and
              (ARequiredVersion.Patch = AThisVersion.Patch) and
              (ARequiredVersion.Build = AThisVersion.Build);

    if not bValid  then
    begin
      Close;
      raise EVersionCheck.Create('Database metadata mismatch!' + #13 +
        'Database: ' + EvDBName + #13 +
        'Database Metadata Version: ' + VersionRecToStr(AThisVersion) + #13 +
        'Required Metadata Version: ' + VersionRecToStr(ARequiredVersion));
    end;
  end;

  
  procedure CheckSystemDBVersion;
  var
    Q: TevCustomIBSQL;
    s: String;
    ver, reqver: TisVersionInfo;
  begin
    if not IsDebugMode and (AppID <> EvoUtilISystemsAppInfo.AppID) then  // iSystems.exe utility should be able to ignore version mismatch
    begin
      Q := TevCustomIBSQL.Create(nil);
      try
        Q.SQL.Text := 'SELECT t.tag FROM sy_storage t WHERE t.tag LIKE ''VER %''';
        Q.Database := Self;
        Q.Transaction := DefaultTransaction;
        if not Q.Transaction.InTransaction then
          Q.Transaction.StartTransaction;
        try
          Q.ExecQuery;
          s := Q.Fields[0].AsString;
          Q.Close;
        finally
          TevTransaction(Q.Transaction).Rollback;
        end;
      finally
        Q.Free;
      end;

      Delete(s, 1, 4);
      ver.Major := StrToIntDef(GetNextStrValue(s, '.'), 0);
      ver.Minor := StrToIntDef(GetNextStrValue(s, '.'), 0);
      ver.Patch := StrToIntDef(GetNextStrValue(s, '.'), 0);
      ver.Build := StrToIntDef(s, 0);

      s := ModuleVersion;
      reqver.Major := StrToInt(GetNextStrValue(s, '.'));
      reqver.Minor := StrToInt(GetNextStrValue(s, '.'));
      reqver.Patch := StrToInt(GetNextStrValue(s, '.'));
      reqver.Build := 0;

      if (reqver.Major <> ver.Major) or
         (reqver.Minor <> ver.Minor) or
         (reqver.Patch <> ver.Patch) then
      begin
        Close;
        raise EVersionCheck.Create(
          'System database version mismatch!' + #13 +
          'Database Version: ' + VersionRecToStr(ver) + #13 +
          'Required Version: ' + VersionRecToStr(reqver));
      end;
    end;

    CompareMetadataVersions(FDBVersion, AppDBVersions.Current.SystemDBVersion);
  end;

begin
  if not FVersionWasChecked and
     not IsADRUser(Context.UserAccount.User) and not IsDBAUser(Context.UserAccount.User) then // ADR and DBA account should not care about version mismatch
  begin
    case DBType of
      dbtSystem:    CheckSystemDBVersion;
      dbtBureau:    CompareMetadataVersions(FDBVersion, AppDBVersions.Current.BureauDBVersion);
      dbtClient:    CompareMetadataVersions(FDBVersion, AppDBVersions.Current.ClientDBVersion);
      dbtTemporary: CompareMetadataVersions(FDBVersion, AppDBVersions.Current.TempDBVersion);
    end;

    FVersionWasChecked := True;
  end;
end;

procedure TevDatabase.ConnectIfNeeds;
var
  bStat: Boolean;
begin
  if not Connected and ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.Connect');
    ctx_Statistics.ActiveEvent.Properties.AddValue('DB Name', EvDBName);
    bStat := True;
  end
  else
    bStat := False;

  try
    inherited;
  finally
    if bStat then
      ctx_Statistics.EndEvent;
  end;

  CheckDBVersion;
end;

function TevDatabase.FindRWTransaction: TevTransaction;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to TransactionCount - 1 do
    if (Transactions[i] <> InternalTransaction) and not TevTransaction(Transactions[i]).ReadOnly then
    begin
      Result := TevTransaction(Transactions[i]);
      break;
    end;
end;

function TEvDatabase.GetEvDBName: String;
begin
  Result := DatabaseName;
end;

procedure TEvDatabase.PrepareForChanges;
var
  Q: TevCustomIBSQL;
begin
  Q := TevCustomIBSQL.Create(nil);
  try
    Q.SQL.Text := 'EXECUTE PROCEDURE DO_AFTER_START_TRANSACTION(:USER_ID, :START_TIME)';
    Q.Database := Self;
    Q.Transaction := FindRWTransaction;
    Q.ParamByName('USER_ID').AsInteger := ctx_DBAccess.GetChangedByNbr;
    Q.ParamByName('START_TIME').AsDateTime := SysTime;
    Q.ExecQuery;
  finally
    Q.Free;
  end;
end;

procedure TEvDatabase.SetEvDBName(const AValue: String);
var
  db_type, s: String;
  dbConn: TevDBConnectionInfo;
begin
  CheckCondition(FindRWTransaction = nil, 'Transaction is active');
  Close;

  FClientID := '';
  db_type := ChangeFileExt(AValue, '');
  
  if AnsiSameText(db_type, DB_System) then
    FType := dbtSystem
  else if AnsiSameText(db_type, DB_S_Bureau) then
    FType := dbtBureau
  else if AnsiSameText(db_type, DB_TempTables) then
    FType := dbtTemporary
  else
  begin
    FType := dbtClient;
    s := AnsiUpperCase(db_type);
    GetNextStrValue(s, AnsiUpperCase(DB_Cl));
    FClientID := s;
  end;

  dbConn := GetDBConnectionInfo(db_type);
  if ((FType = dbtTemporary) or (FType = dbtClient)) and ctx_DBAccess.FullTTRebuildInProgress then
  begin
    if (FType = dbtTemporary) then
       dbConn.Path := dbConn.Path + rebuildExt;
    dbConn.User := FB_Admin;
    dbConn.Password := mb_GlobalSettings.DBAdminPassword;
  end;

  DatabaseName := dbConn.Path;
  
  Params.Clear;
  Params.Add('USER_NAME=' + dbConn.User);
  Params.Add('PASSWORD=' + dbConn.Password);
end;

procedure TEvDatabase.SetModified(const AValue: Boolean);
begin
  if not FModified and AValue and (FType <> dbtTemporary) then
    PrepareForChanges; // only for very first time!

  FModified := AValue;
end;


{ TevTransaction }

procedure TevTransaction.AfterConstruction;
begin
  inherited;
  FReadCommited := True;
  SetReadOnly(True);
  AllowAutoStart := False;
end;

procedure TevTransaction.DoBeforeCommitTransaction;
var
  Q: TevCustomIBSQL;
  i: Integer;
  DB: TevDatabase;
begin
  Q := TevCustomIBSQL.Create(nil);
  try
    for i := 0 to DatabaseCount - 1 do
    begin
      DB := Databases[i] as TevDatabase;
      if DB.Modified and (DB.FType <> dbtTemporary) then
      begin
        Q.SQL.Text := 'EXECUTE PROCEDURE DO_BEFORE_COMMIT_TRANSACTION(:COMMIT_TIME)';
        Q.Database := DB;
        Q.Transaction := Self;
        Q.ParamByName('COMMIT_TIME').AsDateTime := SysTime;
        Q.ExecQuery;
      end;
    end;
  finally
    Q.Free;
  end;
end;


procedure TevTransaction.DoCleanupModifiedFlag;
var
  i: Integer;
  DB: TevDatabase;
begin
  for i := 0 to DatabaseCount - 1 do
  begin
    DB := Databases[i] as TevDatabase;
    DB.Modified := False;
  end;
end;

procedure TevTransaction.DoCommit;
var
  i: Integer;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.CommitTransaction');
    for i := 0 to DatabaseCount - 1 do
      ctx_Statistics.ActiveEvent.Properties.AddValue('DB #' + IntToStr(i+1), TevDatabase(Databases[i]).EvDBName);
  end;

  try
    DoBeforeCommitTransaction;
    Commit;
    DoNotifyChanges;
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
    DoCleanupModifiedFlag;
    SetReadCommited(True);
  end;    
end;


procedure TevTransaction.DoNotifyChanges;
var
  i: Integer;
  DB: TevDatabase;
  ChangedDatabases: String;
begin
  ChangedDatabases := '';
  for i := 0 to DatabaseCount - 1 do
  begin
    DB := Databases[i] as TevDatabase;
    if DB.Modified then
      case DB.FType of
        dbtSystem: AddStrValue(ChangedDatabases, DB_System, ',');
        dbtBureau: AddStrValue(ChangedDatabases, DB_S_Bureau, ',');
        dbtClient: AddStrValue(ChangedDatabases, DB_Cl + UpperCase(DB.FClientID), ',');
      end;
  end;

  if ChangedDatabases <> '' then
    DBChangeNotifier.SendNotification('DBCHANGE', ChangedDatabases);
end;

procedure TevTransaction.DoRollback;
var
  i: Integer;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.RollbackTransaction');
    for i := 0 to DatabaseCount - 1 do
      ctx_Statistics.ActiveEvent.Properties.AddValue('DB #' + IntToStr(i+1), TevDatabase(Databases[i]).EvDBName);
  end;

  try
    Rollback;
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
    DoCleanupModifiedFlag;
    SetReadCommited(True);
  end;
end;

procedure TevTransaction.DoStart;
var
  i: Integer;
begin
  for i := 0 to DatabaseCount - 1 do
    TevDatabase(Databases[i]).ConnectIfNeeds;

  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.StartTransaction');
    ctx_Statistics.ActiveEvent.Properties.AddValue('Read only', FReadOnly);
    for i := 0 to DatabaseCount - 1 do
      ctx_Statistics.ActiveEvent.Properties.AddValue('DB #' + IntToStr(i+1), TevDatabase(Databases[i]).EvDBName);
  end;

  try
    try
      StartTransaction;
    except
      on E: EIBInterBaseError do
      begin
        if Contains(E.Message, 'shutdown') then
        begin
          for i := 0 to DatabaseCount - 1 do
          begin
            TevDatabase(Databases[i]).Close;
            TevDatabase(Databases[i]).ConnectIfNeeds;
          end;
          StartTransaction;
        end;
      end
      else
        raise;
    end;
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;

  DoCleanupModifiedFlag;
end;

procedure TevTransaction.SetReadCommited(const AValue: Boolean);
begin
  if FReadCommited <> AValue then
  begin
    FReadCommited := AValue;
    SetTransParameters;
  end;
end;

procedure TevTransaction.SetReadOnly(const AValue: Boolean);
begin
  FReadOnly := AValue;
  SetTransParameters;
end;

procedure TevTransaction.SetTransParameters;
begin
  CheckNotInTransaction;
  Params.Clear;

  if FReadOnly then
  begin
    if FReadCommited then
    begin
      Params.Add('isc_tpb_read_committed');
      Params.Add('isc_tpb_rec_version');
    end
    else
      Params.Add('isc_tpb_concurrency');
    Params.Add('isc_tpb_nowait');
    Params.Add('isc_tpb_read');
  end
  else
  begin
    if FReadCommited then
    begin
      Params.Add('isc_tpb_read_committed');
      Params.Add('isc_tpb_no_rec_version');
      Params.Add('isc_tpb_wait');
    end
    else
    begin
      Params.Add('isc_tpb_concurrency');
      Params.Add('isc_tpb_nowait');
    end;
    Params.Add('isc_tpb_write');
  end;

  DefaultAction := TARollback;
end;

procedure TevTransaction.StartIfNeeds;
begin
  if not InTransaction then
    DoStart;
end;

{ TevIBSQL }

procedure TevIBSQL.ApplyDataSecurityRules;
begin
  ErrorIf(AnsiSameText(Context.UserAccount.User, sGuestUserName), 'DB access has been denied');

  if AnsiStartsText('SELECT', SQL.Text) then
    SQL.Text := FOwner.FSecuredSQL.ApplySecurity(SQL.Text, TevDatabase(Database).DBType);
end;

constructor TevIBSQL.Create(const AOwner: TevFirebird; const ASQL: String; const ASecurityOn: Boolean);
var
  DBWasAssigned: Boolean;
begin
  inherited Create(AOwner);
  SQL.Text := ASQL;
  if ASQL <> '' then
  begin
    Database := GetDBForSQL;
    if ASecurityOn then
    begin
      DBWasAssigned := Assigned(Database);
      ApplyDataSecurityRules;
      if not Assigned(Database) and DBWasAssigned then
        Database := GetDBForSQL;
    end;
  end;
end;

procedure TevIBSQL.ExecQuery;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.Query');
    ctx_Statistics.ActiveEvent.Properties.AddValue('SQL', SQL.Text);
  end;

  try
    DoExecQuery;
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

function TevIBSQL.ExecAsDataSet(const ALoadBlobs: Boolean): IevDataSet;
begin
  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('DB.Query');
    ctx_Statistics.ActiveEvent.Properties.AddValue('SQL', SQL.Text);
  end;

  try
    DoExecQuery;
    Result := AsDataSet(ALoadBlobs)
  finally
    if ctx_Statistics.Enabled then
    begin
      if Assigned(Result) then
        ctx_Statistics.ActiveEvent.Properties.AddValue('RecCount', Result.RecordCount);

      ctx_Statistics.EndEvent;
    end;
  end;
end;

function TevIBSQL.GetDBForSQL: TevDatabase;
var
  Tbl: String;
  sSQL: String;
  sType, s: String;
begin
  sSQL := AnsiUpperCase(SQL.Text);
  if StartsWith(sSQL, '/*', True) then
  begin
    GetNextStrValue(sSQL, '/*');
    s := Trim(GetNextStrValue(sSQL, '*/'));
    Result := FOwner.GetDBByType(GetDBTypeByDBName(s));
  end

  else
  begin
    sSQL := StringReplace(sSQL, #13, ' ', [rfReplaceAll]);
    sType := GetNextStrValue(sSQL, ' ');
    sSQL :=  TrimLeft(sSQL);

    if AnsiSameText(sType, 'SELECT') then
    begin
       GetNextStrValue(sSQL, ' FROM ');
      sSQL := TrimLeft(sSQL);
      Tbl := GetNextTextValue(sSQL);
    end

    else if AnsiSameText(sType, 'INSERT') then
    begin
      GetNextStrValue(sSQL, 'INTO ');
      sSQL := TrimLeft(sSQL);
      Tbl := GetNextTextValue(sSQL);
    end

    else if AnsiSameText(sType, 'DELETE') then
    begin
      GetNextStrValue(sSQL, 'FROM ');
      sSQL := TrimLeft(sSQL);
      Tbl := GetNextTextValue(sSQL);
    end

    else if AnsiSameText(sType, 'UPDATE') then
    begin
      sSQL := TrimLeft(sSQL);
      Tbl := GetNextTextValue(sSQL);
    end

    else if AnsiSameText(sType, 'EXECUTE') then
    begin
      sSQL := TrimLeft(sSQL);
      GetNextTextValue(sSQL);  // procedure
      sSQL := TrimLeft(sSQL);
      Tbl := GetNextTextValue(sSQL);
      if StartsWith(Tbl, 'DEL_') or StartsWith(Tbl, 'PACK_') then
        GetNextStrValue(Tbl, '_');
    end

    else
      raise EevException.Create('SQL statement was rejected.'#13 + SQL.Text);

    s := ' ' + Tbl + ' ';
    if AnsiContainsText(sSystemDBStoredProcedures, s) then
      Result := FOwner.GetSystemDBConn.Database
    else if AnsiContainsText(sSBDBStoredProcedures, s) then
      Result := FOwner.GetBureauDBConn.Database
    else if AnsiContainsText(sClientDBStoredProcedures, s) then
      Result := FOwner.GetClientDBConn.Database
    else
      Result := FOwner.GetDBByTable(Tbl);
  end;
end;

procedure TevIBSQL.GetLazyBlobInfo(const ABlobField: TIBXSQLVAR; out AData: String);
begin
  AData := LAZY_BLOB;
end;

procedure TevIBSQL.DoExecQuery;

  procedure CheckSysTime999;
  var
    CurrTimePar: TIBXSQLVAR;
  begin
    if Pos('CURRENTSYSTIME999', AnsiUpperCase(Params.Names)) <> 0 then
    begin
      CurrTimePar := Params.ByName('CurrentSysTime999');
      CurrTimePar.Value := SysTime;
    end;
  end;

begin
  if not IsSelect(SQL.Text) then
    if TevDatabase(Database).FindRWTransaction <> nil then  // RW transaction is opened
      TevDatabase(Database).Modified := True;  // calls AfterStartTransaction on the first DB change

  CheckSysTime999;
  inherited ExecQuery;
end;

{ TevFieldDefs }

procedure TevFieldDefs.Add(const Name: string; DataType: TFieldType; Size: Integer);
var
  Fld: TFieldDef;
begin
  Fld := TFieldDef.Create(nil);
  Fld.Name := Name;
  Fld.DataType := DataType;
  Fld.Size := Size;
  Fld.Collection := Self;
end;

procedure TevFieldDefs.BeginUpdate;
begin
end;

procedure TevFieldDefs.EndUpdate;
begin
end;

{ TevDBConnection }

constructor TevDBConnection.Create(const AName: String);
var
  s: String;
begin
  inherited;
  FDB := TevDatabase.Create;
  s := AName;
  GetNextStrValue(s, '.');
  Database.EvDBName := s;
  Database.Name := s;
end;

function TevDBConnection.Database: TevDatabase;
begin
  Result := TevDatabase(FDB);
end;


{ TevDBConnectionPool }

function TevDBConnectionPool.AcquireObject(const AObjectName: String): IisObjectFromPool;
var
  sDB: String;
begin
  if IsDBDisabled(AObjectName) then
  begin
    sDB := AObjectName;
    GetNextStrValue(sDB, '.');
    raise EMaintenance.CreateFmt('Database %s is in maintenance mode', [sDB]);
  end;

  Result := inherited AcquireObject(AObjectName);
end;

procedure TevDBConnectionPool.BeforeCheckPool;
begin
  if (mb_GlobalSettings <> nil) and (Mainboard.TCPServer <> nil) then // it's nil when application is about starting up
    SetMaxPoolSize(mb_GlobalSettings.DBConPoolSize);
  inherited;
end;

function TevDBConnectionPool.CreateObject(const AObjectName: String): IisObjectFromPool;
begin
  if IsServiceDB(AObjectName) then
    Result := TevServiceDBConnection.Create(AObjectName)
  else
    Result := inherited CreateObject(AObjectName);
end;

procedure TevDBConnectionPool.DisableDBs(const ADBList: IisStringList);
var
  L: IisStringList;
  i: Integer;
  sDomainID: String;
begin
  Lock;
  try
    PrepareDBList;

    if ctx_DBAccess.GetADRContext then
      sDomainID := ctx_DomainInfo.DomainName + '-ADR'
    else
      sDomainID := ctx_DomainInfo.DomainName;

    if ADBList.IndexOf('*') <> -1 then
    begin
      L := TisStringList.Create;
      L.Add('*');
    end
    else
    begin
      L := GetDisabledDBs;
      L.CaseSensitive := False;
      L.Sorted := True;

      for i := 0 to ADBList.Count - 1 do
        if L.IndexOf(ADBList[i]) = -1 then
          L.Add(ADBList[i]);
    end;

    FDisabledDBs.AddValue(sDomainID, L);

    //Drop cached connections
    if IsDBDisabled(sDomainID + '.*') then
      Clear
    else
      for i := 0 to L.Count - 1 do
        RemoveObject(sDomainID + '.' + L[i]);
  finally
    UnLock;
  end;
end;

procedure TevDBConnectionPool.EnableDBs(const ADBList: IisStringList);
var
  L: IisStringList;
  i, j: Integer;
  sDomainID: String;
begin
  Lock;
  try
    PrepareDBList;

    if ctx_DBAccess.GetADRContext then
      sDomainID := ctx_DomainInfo.DomainName + '-ADR'
    else
      sDomainID := ctx_DomainInfo.DomainName;

    if ADBList.IndexOf('*') <> -1 then
      L := TisStringList.Create
    else
    begin
      L := GetDisabledDBs;
      L.CaseSensitive := False;
      L.Sorted := True;

      for i := 0 to ADBList.Count - 1 do
      begin
        j := L.IndexOf(ADBList[i]);
        if j <> -1 then
          L.Delete(j);
      end;
    end;

    FDisabledDBs.AddValue(sDomainID, L);
  finally
    UnLock;
  end;
end;

function TevDBConnectionPool.GetDisabledDBs: IisStringList;
var
  V: IisNamedValue;
  sDomainID: String;
begin
  Result := TisStringList.Create;

  if ctx_DBAccess.GetADRContext then
    sDomainID := ctx_DomainInfo.DomainName + '-ADR'
  else
    sDomainID := ctx_DomainInfo.DomainName;

  Lock;
  try
    PrepareDBList;
    V := FDisabledDBs.FindValue(sDomainID);
    if Assigned(V) then
      Result.Text := (IInterface(V.Value) as IisStringList).Text;
  finally
    UnLock;
  end;
end;

function TevDBConnectionPool.IsDBDisabled(const ADBName: String): Boolean;
var
  sDB, sDomain: String;
  V: IisNamedValue;
  DBList: IisStringList;
begin
  Result := False;

  if IsServiceDB(ADBName) or (Context <> nil) and ctx_DBAccess.FullTTRebuildInProgress then
    Exit

  else if Contains(ADBName, '.') then
  begin
    sDB := ADBName;
    sDomain := GetNextStrValue(sDB, '.');
  end
  else
  begin
    sDB := ADBName;
    sDomain := ctx_DomainInfo.DomainName;
    if ctx_DBAccess.GetADRContext then
      sDomain := sDomain + '-ADR';
  end;

  Lock;
  try
    PrepareDBList;
    V := FDisabledDBs.FindValue(sDomain);
    if Assigned(V) then
    begin
      DBList := IInterface(V.Value) as IisStringList;
      if (DBList.Count = 1) and (DBList[0] = '*') then
        Result := True
      else
        Result := DBList.IndexOf(sDB) <> -1;
    end;
  finally
    Unlock;
  end;
end;

function TevDBConnectionPool.IsServiceDB(const AObjectName: String): Boolean;
begin
  Result := FinishesWith(AObjectName, DB_Service);
end;

procedure TevDBConnectionPool.PrepareDBList;
begin
  if not Assigned(FDisabledDBs) then
    FDisabledDBs := TisListOfValues.Create;
end;

procedure TevDBConnectionPool.ReturnObject(const AObject: IisObjectFromPool);
begin
  if not IsDBDisabled(AObject.Name) then
    try
      if (AObject as IevCustomDBConnection).Database.TestConnected then
        inherited;
    except
    end;
end;


{ TevSQL }

constructor TevSQL.Create(const AOwner: TevFirebird; const ASQL: String);
begin
  inherited Create;
  FOwner := AOwner;
  FSQLObj := TevIBSQL.Create(AOwner, ASQL, AOwner.FDataSecurityActive);
  AOwner.SetTransactionForQuery(FSQLObj);
  FSQLObj.Prepare;
  FOwner.FSQLObjects.Add(Self);
end;

destructor TevSQL.Destroy;
begin
  ForceDetach;
  inherited;
end;

procedure TevSQL.SetParams(const AParams: IisListOfValues);
begin
  CheckSQLObj;
  FSQLObj.SetParams(AParams);
end;

procedure TevSQL.Exec;
begin
  CheckSQLObj;

  FOwner.FDBOperLock.Lock;
  try
    FSQLObj.ExecQuery;
  finally
   FOwner.FDBOperLock.UnLock;
  end;
end;

procedure TevSQL.ForceDetach;
var
  i: Integer;
begin
  FOwner.FDBOperLock.Lock;
  try
    i := FOwner.FSQLObjects.IndexOf(Self);
    if i <> - 1 then
      FOwner.FSQLObjects.Delete(i);
    FreeAndNil(FSQLObj);
  finally
    FOwner.FDBOperLock.Unlock;
  end;
end;

function TevSQL.GetParam(const AIndex: Integer): Variant;
begin
  CheckSQLObj;
  Result := FSQLObj.GetParam(AIndex);
end;

function TevSQL.GetParam(const AName: String): Variant;
begin
  CheckSQLObj;
  Result := FSQLObj.GetParam(AName);
end;

procedure TevSQL.SetParam(const AIndex: Integer; const AValue: Variant);
begin
  CheckSQLObj;
  FSQLObj.SetParam(AIndex, AValue);
end;

function TevSQL.GetParamIndex(const AName: String; const AErrorIfNotEsist: Boolean): Integer;
begin
  CheckSQLObj;
  Result := FSQLObj.GetParamIndex(AName, AErrorIfNotEsist);
end;

procedure TevSQL.SetParam(const AName: String; const AValue: Variant);
begin
  CheckSQLObj;
  FSQLObj.SetParam(AName, AValue);
end;

procedure TevSQL.CheckSQLObj;
begin
  if not Assigned(FSQLObj) then
    raise EevException.Create('Database was forcedly disconnected');
end;

function TevSQL.Open(const ALoadBlobs: Boolean): IevDataSet;
begin
  CheckSQLObj;

  FOwner.FDBOperLock.Lock;
  try
    Result := FSQLObj.ExecAsDataSet(ALoadBlobs);
  finally
   FOwner.FDBOperLock.UnLock;
  end;
end;

constructor TevSQL.CreateRaw(const AOwner: TevFirebird; const ASQL: String; const ADBType: TevDBType);
begin
  inherited Create;
  FOwner := AOwner;
  FSQLObj := TevRawSQL.Create(AOwner);
  FSQLObj.SQL.Text := ASQL;
  FSQLObj.Database := FOwner.GetDBByType(ADBType);
  AOwner.SetTransactionForQuery(FSQLObj);
  FSQLObj.Prepare;
  FOwner.FSQLObjects.Add(Self);
end;

procedure TevSQL.SetParams(const AParams: array of Variant);
begin
  CheckSQLObj;
  FSQLObj.SetParams(AParams);
end;

{ TevRawSQL }

function TevRawSQL.AsDataSet(const ALoadBlobs: Boolean): IevDataSet;
var
  i, j: Integer;
begin
  if FieldCount > 0 then
  begin
    try
      Result := TevDataSet.Create(GetFieldDefs);
      Result.LogChanges := False;

      j := 0;
      while not Eof do
      begin
        // Process termination logic
        if j = 500 then
        begin
          if CurrentThreadTaskTerminated then
            AbortEx;
          j := 0;
        end;
        Inc(j);

        Result.Append;
        for i := 0 to FieldCount - 1 do
          if Fields[i].IsNull then
            Result.Fields[i].Clear
          else if (Fields[i].SqlVar.SqlDef = SQL_BLOB) and ALoadBlobs then
            Result.Fields[i].AsString := Fields[i].AsString
          else
            Result.Fields[i].Value := Fields[i].Value;

        Result.Post;
        Next;
      end;
    finally
      Close;
    end;

    Result.First;
  end;
end;

destructor TevRawSQL.Destroy;
begin
  FreeAndNil(FFieldDefs);
  inherited;
end;

function TevRawSQL.GetFieldDefs: TFieldDefs;
var
  FieldType: TFieldType;
  FieldSize: Word;
  i: Integer;
  Fld: TFieldDef;
begin
  if not Assigned(FFieldDefs) then
    FFieldDefs := TevFieldDefs.Create(nil);

  FFieldDefs.Clear;
  for i := 0 to FieldCount - 1 do
  begin
    FieldSize := 0;

    case Fields[i].Data.SqlDef of
      SQL_VARYING,
      SQL_TEXT:
        begin
          FieldSize := Fields[i].Data.sqllen;
          FieldType := ftString;
        end;

      SQL_FLOAT,
      SQL_DOUBLE:
        FieldType := ftFloat;

      SQL_LONG,
      SQL_SHORT:
        FieldType := ftInteger;

      SQL_INT64:
        if Fields[i].Data.sqlScale = 0 then // for Numeric() type
          FieldType := ftLargeint
        else
          FieldType := ftFloat;        

      SQL_TIMESTAMP:
        FieldType := ftDateTime;

      SQL_TYPE_TIME:
        FieldType := ftTime;

      SQL_TYPE_DATE:
        FieldType := ftDate;

      SQL_BLOB:
        begin
          FieldSize := sizeof (TISC_QUAD);
            if (Fields[i].Data.sqlsubtype = 1) then
              FieldType := ftmemo
            else
              FieldType := ftBlob;
        end;

      SQL_BOOLEAN:
        FieldType := ftBoolean;
    else
      FieldType := ftUnknown;
      Assert(False);
    end;

    Fld := TFieldDef.Create(nil);
    Fld.Name := Fields[i].Name;
    Fld.DataType := FieldType;
    Fld.Size := FieldSize;
    Fld.Collection := FFieldDefs;
  end;

  Result := FFieldDefs;
end;

function TevRawSQL.GetParam(const AName: String): Variant;
begin
  Result := GetParam(GetParamIndex(AName, True));
end;

procedure TevRawSQL.SetParam(const AIndex: Integer; const AValue: Variant);
var
  V: Variant;
  s: String;
  function CheckValidDatetime :boolean;
  var
    aTableName, aSql: String;
    T: TddTableClass;
  begin
    Result := True;
    aSql:= Sql.Text;
    aTableName := '';
    if StartsWith(aSql, 'UPDATE') then
    begin
       GetNextStrValue(aSql, ' ');
       aTableName := GetNextStrValue(aSql, ' ');
    end
    else if StartsWith(aSql, 'INSERT') then
    begin
      GetNextStrValue(aSql, 'INTO ');
      aTableName := GetNextStrValue(aSql, ' ');
    end;
    //check only Update, Insert
    if (aTableName <> '') then
    begin
      T := GetddTableClassByName(aTableName);
      if Assigned(T) and (not(T.GetFieldType(Params[AIndex].Name) in [ftDateTime, ftDate, ftTimeStamp])) then
        Result := False;
    end;
  end;
begin
  V := AValue;
  if VarType(V) = varString then
  begin
    if not VarIsNull(V) and (Params[AIndex].SQLType = SQL_VARYING) then
      V := TrimRight(V); // Varchar ignores trailing spaces in comparison operation, so there is no reason to store it.

    s := V;

    if (Length(s) = 3) and AnsiSameText(s, 'now') then
    begin
      if CheckValidDatetime then
        V := SysTime;
    end
    else if (Length(s) = 5) and AnsiSameText(s, 'today') then
    begin
      if CheckValidDatetime then
        V := DateOf(SysTime);
    end
    else if AnsiSameStr(s, BLANK_TAG) then
      V := '' // A back door for saving empty strings in not null fields
    else if ((Params[AIndex].SQLType = SQL_VARYING) or (Params[AIndex].SQLType = SQL_TEXT)) and (Length(s) = 0) then
      V := Null
    else if (Params[AIndex].SQLType = SQL_BLOB) and TextIsVisiallyEmpty(s) then
      V := Null;
  end;

  if not VarIsNull(V) and
     ((Params[AIndex].SQLType = SQL_TIMESTAMP) or (Params[AIndex].SQLType = SQL_TYPE_DATE) or (Params[AIndex].SQLType = SQL_TYPE_TIME)) then
    Params[AIndex].Value := VarAsType(V, varDate)

  else if VarType(V) = varUnknown then
  begin
    if (IInterface(V) as IisStream).Size = 0 then
      Params[AIndex].Clear
    else
      Params[AIndex].LoadFromStream((IInterface(V) as IisStream))
  end

  else
    Params[AIndex].Value := V;
end;

function TevRawSQL.GetParam(const AIndex: Integer): Variant;
begin
  Result := Params[AIndex].Value;
end;

procedure TevRawSQL.SetParam(const AName: String; const AValue: Variant);
begin
  SetParam(GetParamIndex(AName, True), AValue);
end;

procedure TevRawSQL.SetParams(const AParams: IisListOfValues; const AClearUndefined: Boolean);
var
  Param: IisNamedValue;
  i: Integer;
begin
  for i := 0 to Params.Count-1 do
  begin
    if Assigned(AParams) then
      Param := AParams.FindValue(Params[i].Name)
    else
      Param := nil;

    if Assigned(Param) then
      SetParam(i, Param.Value)

    else if AClearUndefined then
      Params[i].Clear;
  end;
end;


function TevRawSQL.GetParamIndex(const AName: String; const AErrorIfNotEsist: Boolean): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Params.Count - 1 do
    if AnsiSameText(Params[i].Name, AName) then
    begin
      Result := i;
      break;
    end;

  if AErrorIfNotEsist and (Result = -1) then
    raise EevException.Create('SQL parameter ' + AName + ' not found');
end;


function TevRawSQL.ExecAsDataSet(const ALoadBlobs: Boolean): IevDataSet;
begin
  ExecQuery;
  Result := AsDataSet(ALoadBlobs);
end;

procedure TevRawSQL.SetParams(const AParams: array of Variant);
var
  i: Integer;
begin
  for i := 0 to High(AParams) do
    SetParam(i, AParams[i]);
end;

constructor TevRawSQL.Create(const AOwner: TevFirebird);
begin
  inherited Create(nil);
  FOwner := AOwner;
end;

{ TevCustomIBSQL }

constructor TevCustomIBSQL.Create(AOwner: TComponent);
begin
  inherited;
  TStringList(SQL).OnChange := OnChangeSQL;
end;

procedure TevCustomIBSQL.ExecQuery;
begin
  if Assigned(SQLMonitorLog) then
    try
      SQLMonitorLog.Add(Self);
    except
    end;

  Assert(Length(SQL.Text) <= 65536, 'SQL statement length exceeds 64K limit');
  try
    inherited ExecQuery;
  except
    on E: Exception do
    begin
      if Contains(E.Message, 'module name or entrypoint could not be found') then
        E.Message := E.Message + #13#13 + 'EvolUDFs module is missing or incompatible';
      raise;
    end;
  end;
end;

procedure TevCustomIBSQL.OnChangeSQL(Sender: TObject);
begin
  ParamCheck := not IsExecuteBlock(SQL.Text);
end;

{ TevSQLLog }

procedure TevSQLLog.Add(const ASQL: TevCustomIBSQL);
var
  s: String;
  i: Integer;
begin
  s := ASQL.SQL.Text;

  Lock;
  try
    FLog.Writeln('### Task: ' + IntToStr(GetCurrentThreadTaskID) + '   Time: ' + FormatDateTime('mm/dd/yyyy hh:nn:ss zzz', Now));
    FLog.Writeln(ASQL.SQL.Text);
    if ASQL.Params.Count > 0 then
    begin
      FLog.Writeln('');
      for i := 0 to ASQL.Params.Count - 1 do
      begin
        s := ASQL.Params[i].Name + ' = ';
        if ASQL.Params[i].IsNull then
          s := s + '<NULL>'
        else
          s := s + ASQL.Params[i].AsString;
        FLog.WriteLn(s);
      end;
    end;
    FLog.Writeln('### END');
    FLog.Writeln('');
    FLog.Flush;
  finally
    Unlock;
  end;
end;

constructor TevSQLLog.Create;
var
  LogFileName: String;
begin
  inherited Create;
  InitLock;

  LogFileName := AppDir + 'EvoSQL.log';

  if not FileExists(LogFileName) then
  begin
    FLog := TisStream.CreateFromNewFile(LogFileName);
    FLog := nil;
  end;

  FLog := TisStream.CreateFromFile(LogFileName, False, False, True);
end;

{ TevCustomDatabase }

procedure TevCustomDatabase.AfterConstruction;
begin
  inherited;
  LoginPrompt := False;
  AfterConnect := InitDBVersion;
end;

procedure TevCustomDatabase.ConnectIfNeeds;
begin
  try
    Open;
  except
    on E: EIBError do
      raise;
    else
    begin
      // if AV raised in gds32.dll then try again... :(
      Sleep(10);
      Open;
    end;
  end;
end;

constructor TevCustomDatabase.Create;
begin
  inherited Create(nil);
  RemoveTransactions;
  FReadOnlyTrans := TevTransaction.Create(Self);
  FReadOnlyTrans.ReadOnly := True;
  FReadOnlyTrans.Name := 'ROTran';
  DefaultTransaction := FReadOnlyTrans;
end;

function TevCustomDatabase.GetDBVersion: TisVersionInfo;
begin
  ConnectIfNeeds;
  Result := FDBVersion;
end;

procedure TevCustomDatabase.InitDBVersion(Sender: TObject);
var
  Q: TevCustomIBSQL;
  Tr: TevTransaction;
  s: String;

  // TODO: remove after Plymouth release ("R" release?)
  procedure LegacyInitDBVersion;
  var
    iVersionBuildFieldIndex: integer;
  begin
    Q.SQL.Text := 'SELECT * FROM Version_Info';
    Q.ExecQuery;

    FDBVersion.Major := Q.FieldByName('MAJOR_VERSION').AsInteger;
    FDBVersion.Minor := Q.FieldByName('MINOR_VERSION').AsInteger;
    FDBVersion.Patch := Q.FieldByName('PATCH_VERSION').AsInteger;
    iVersionBuildFieldIndex := Q.FieldIndex['BUILD_VERSION'];
    if iVersionBuildFieldIndex <> -1 then
      FDBVersion.Build := Q.Fields[iVersionBuildFieldIndex].AsInteger
    else
      FDBVersion.Build := 0;
  end;

begin
  Q := TevCustomIBSQL.Create(nil);
  try
    Q.SQL.Text := 'SELECT version FROM ev_database';

    Tr := TevTransaction.Create(nil);
    try
      Tr.ReadOnly := True;
      Tr.AddDatabase(Self);
      Q.Database := Self;
      Q.Transaction := Tr;

      try
        Tr.StartTransaction;  // do not use DoStart here
        try
          Q.ExecQuery;

          s := Q.Fields[0].AsString;
          FDBVersion.Major := StrToIntDef(GetNextStrValue(s, '.'), 0);
          FDBVersion.Minor := StrToIntDef(GetNextStrValue(s, '.'), 0);
          FDBVersion.Patch := StrToIntDef(GetNextStrValue(s, '.'), 0);
          FDBVersion.Build := StrToIntDef(s, 0);
        except
          on E: Exception do
            if Contains(E.Message, 'ev_database') then
              LegacyInitDBVersion;
        end
      finally
        Tr.Rollback;       // do not use DoRollback here
      end;

    finally
      Tr.RemoveDatabases;
      Tr.Free;
    end;
  finally
    Q.Free;
  end;
end;

{ TevServiceDatabase }

function TevServiceDatabase.CheckIfValid(const ACheckVersion: Boolean): Boolean;
begin
  Result := False;
  try
    Open;
    if not ACheckVersion or (VersionRecToStr(DBVersion) = VersionRecToStr(AppDBVersions.Current.ServiceDBVersion)) then
      Result := True
    else
      Close;
  except
    on E: EIBInterBaseError do
    else
      raise;
  end
end;

procedure TevServiceDatabase.CopyDatabaseFile(const ASourceDB, ATargetDB: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE copy_database_file(:src, :trg, :pwd)';
    Q.SetParam('src', ASourceDB);
    Q.SetParam('trg', ATargetDB);
    Q.SetParam('pwd', GetPassword);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.CopyDatabase(const ASourceDatabasePath, ASrcUser, ASrcPassword, ADestDatabasePath: String);
var
  Q: TevRawSQL;
  SrcHost, SrcPath: String;
begin
  SrcPath := ASourceDatabasePath;
  SrcHost := GetNextStrValue(SrcPath, ':');

  if (SrcHost = '') or AnsiSameText(SrcHost, Host) and
      AnsiSameText(ASrcUser, GetUserName) and AnsiSameText(ASrcPassword, GetPassword) then
    CopyDatabaseFile(SrcPath, ADestDatabasePath)  //Copy DB as a file

  else
  begin
    // Copy DB via gbak
    Q := TevRawSQL.Create(FCurrentOwner);
    try
      Q.Database := Self;
      Q.SQL.Text := 'EXECUTE PROCEDURE copy_database(:srcdb, :srcusr, :srcpwd, :destdb, :destusr, :destpwd)';
      Q.SetParam('srcdb', ASourceDatabasePath);
      Q.SetParam('srcusr', ASrcUser);
      Q.SetParam('srcpwd', ASrcPassword);
      Q.SetParam('destdb', ADestDatabasePath);
      Q.SetParam('destusr', GetUserName);
      Q.SetParam('destpwd', GetPassword);

      Q.Database.DefaultTransaction.StartTransaction;
      try
        Q.ExecQuery;
      finally
        Q.Database.DefaultTransaction.Rollback;
      end;
    finally
      Q.Free;
    end;
  end;
end;

procedure TevServiceDatabase.CreateServiceDatabase;
var
  Params: String;
  bkpFileName: String;
begin
  ctx_UpdateWait('Creating service database');
  try
    bkpFileName := AppDir + 'SERVICE.gbk';
    CheckCondition(FileExists(bkpFileName), Format('File "%s" not found', [bkpFileName]));


    // Try to drop the database if it exists
    try
      Open;
      DropDatabase;
      Close;
    except
    end;

    Params := Format('-c -rep -p 8192 -user "%s" -password "%s" "%s" "%s"',
      [Self.GetUserName, Self.GetPassword, bkpFileName, DatabaseName]);

    FCurrentOwner.RunGBAK(Params);
  finally
    ctx_UpdateWait('');
  end;
end;

procedure TevServiceDatabase.DefragmentDatabase(const ADatabasePath: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE defragment_database(:db, :usr, :pwd)';
    Q.SetParam('db', ADatabasePath);
    Q.SetParam('usr', GetUserName);
    Q.SetParam('pwd', GetPassword);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

function TevServiceDatabase.GetHost: String;
var
  s: String;
begin
  s := DatabaseName;
  Result := GetNextStrValue(s, ':');
end;

procedure TevServiceDatabase.MoveDatabaseFile(const ASourceDB, ATargetDB: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE move_database_file(:src, :trg, :pwd)';
    Q.SetParam('src', ASourceDB);
    Q.SetParam('trg', ATargetDB);
    Q.SetParam('pwd', GetPassword);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.PatchDatabase(const ADatabasePath, AScriptFile, AProgressCaption: String);
var
  workDB, s: String;
begin
  workDB := ADatabasePath + '.tmp';
  try
    ctx_UpdateWait('Copying source database');
    CopyDatabaseFile(ADatabasePath, workDB);

    s := Format('-e -b -u "%s" -p "%s" -i "%s" "%s:%s"', [GetUserName, GetPassword, AScriptFile, Host, workDB]);
    FCurrentOwner.RunISQL(s, AProgressCaption);

    ctx_UpdateWait('Moving patched database');
    MoveDatabaseFile(workDB, ADatabasePath);
  except
    try
      if not Connected then
        Open;
      DeleteDatabaseFile(workDB);
    except
    end;
    raise;
  end;
end;

procedure TevServiceDatabase.SetHost(const AValue: String);
var
  Locations: IevDBLocationList;
  dbLoc: IevDBLocation;

  function GetPathFor(const ADBType: TevDBType): IevDBLocation;
  var
    s: String;
    i: Integer;
  begin
    Result := nil;
    for i := 0 to Locations.Count - 1 do
      if Locations[i].DBType = ADBType then
      begin
        s := Locations[i].Path;
        s := GetNextStrValue(s, ':');
        if AnsiSameText(s, AValue) then
        begin
          Result :=  Locations[i];
          break;
        end;
      end;
  end;

  procedure GetLoc;
  begin
    Locations.Lock;
    try
      // This should guarantee persistence service database path selection
      dbLoc := GetPathFor(dbtUnspecified);
      if not Assigned(dbLoc) then
      begin
        dbLoc := GetPathFor(dbtSystem);
        if not Assigned(dbLoc) then
        begin
          dbLoc := GetPathFor(dbtBureau);
          if not Assigned(dbLoc) then
          begin
            dbLoc := GetPathFor(dbtTemporary);
            if not Assigned(dbLoc) then
              dbLoc := GetPathFor(dbtClient);
          end;
        end;
      end;
    finally
      Locations.Unlock;
    end;
  end;

begin
  Close;

  Locations := ctx_DomainInfo.DBLocationList;
  GetLoc;

  if not Assigned(dbLoc) then
  begin
    Locations := ctx_DomainInfo.ADRDBLocationList;
    GetLoc;
  end;

  CheckCondition(Assigned(dbLoc), 'Cannot infer service database location');
  SetupConnection(dbLoc.Path, FB_Admin, mb_GlobalSettings.DBAdminPassword);
end;

function TevServiceDatabase.GetFolderContent(const APath, AMask: String): IevDataSet;
var
  Q: TevRawSQL;
  sql: String;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;

    sql := 'SELECT l.result file_name FROM get_file_list(:folder) l';
    if AMask <> '' then
      sql := sql + ' WHERE l.result like ''' + AMask + '''';
    Q.SQL.Text := sql;
    Q.SetParam('folder', APath);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Result := Q.ExecAsDataSet(True);
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.EnsureDatabaseConnection(const ACheckVersion: Boolean);
var
  i: Integer;
  bCreated: Boolean;
begin
  // Retry logic because it could be running from other threads
  bCreated := False;
  for i := 1 to 3 do
    if not CheckIfValid(ACheckVersion) then
    begin
      try
        CreateServiceDatabase;
        bCreated := True;
      except
        on E: Exception do
          mb_LogFile.AddContextEvent(etError, 'DB Access', 'Service database could not be created. Trying again...', MakeFullError(E));
      end;

      if bCreated then
        Assert(CheckIfValid(True))
      else
        Snooze(5000);
    end
    else
      break;

  CheckCondition(Connected, DatabaseName + ' could not be created.'#13'Is this directory owned by firebird?');
end;

procedure TevServiceDatabase.SetupConnection(const ADBPath, AUser, APassword: String);
begin
  DatabaseName := ChangeFilePath(AnsiUpperCase(DB_Service) + '.gdb', ADBPath);
  Params.Clear;
  Params.Add('USER_NAME=' + AUser); 
  Params.Add('PASSWORD=' + APassword);
end;

procedure TevServiceDatabase.DeleteServerFile(const AFileName: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE delete_file(:file)';
    Q.SetParam('file', AFileName);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

function TevServiceDatabase.CheckAndUpdateUDFs: Boolean;
var
  UDFsLib: IisStream;
  UDFsFile, s: String;
  ver: Integer;
  Q: TevRawSQL;
begin
  Result := False;
  
  try
    UDFsFile := AppDir + 'EvolUDFs';
    CheckCondition(FileExists(UDFsFile), Format('File "%s" not found',  [UDFsFile]));

    UDFsLib := TisStream.CreateFromFile(UDFsFile);

    // Scanning UDFs file for version information
    s := UDFsLib.AsString;
    GetNextStrValue(s, 'VERTAG_');
    s := GetNextStrValue(s, '_VERTAG');
    ver := StrToIntDef(s, 0);
    ErrorIf(ver = 0, Format('File "%s" is not a valid UDFs library', [UDFsFile]));

    try
      if ver <> GetUDFsVersion then
      begin
        Q := TevRawSQL.Create(FCurrentOwner);
        try
          Q.Database := Self;
          Q.SQL.Text := 'EXECUTE PROCEDURE update_udfs(:EvolUDFs)';

          Q.Database.DefaultTransaction.StartTransaction;
          try
            Q.SetParam('EvolUDFs', UDFsLib);
            Q.ExecQuery;
          finally
            Q.Database.DefaultTransaction.Rollback;
          end;

          Result := True;
        finally
          Q.Free;
        end;

        Close;
        EnsureDatabaseConnection;

        CheckCondition(ver = GetUDFsVersion, 'Version mismatch after update');
      end;
    except
      on E: Exception do
      begin
        if Contains(E.Message, 'GETEVOLUDFVERSION') then
          E.Message := E.Message + #13 + Format('Did you install the Plymouth UDFs on %s in /opt/firebird/UDFs/?', [GetHost]);
        raise;
      end;
    end;
  except
    on E: Exception do
    begin
      E.Message := 'EvolUDFs update failed: ' + E.Message;
      raise;
    end;
  end;
end;


function TevServiceDatabase.GetUDFsVersion: Integer;
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'SELECT result FROM get_udfs_version';

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
      Result := Q.Fields[0].AsInteger;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.TidyUpDBFolder(const ADatabaseFolder: String);
var
  Q: TevRawSQL;
  Lst: IevDataSet;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'SELECT l.result file_name FROM get_file_list(:folder) l WHERE l.result not like ''%.gdb'' and l.result not like ''%.gdb.delta''';
    Q.SetParam('folder', ADatabaseFolder);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Lst := Q.ExecAsDataSet(True);
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;

  Lst.First;
  while not Lst.Eof do
  begin
    DeleteServerFile(NormalizePath(ADatabaseFolder) + Lst.Fields[0].AsString);
    Lst.Next;
  end;
end;

procedure TevServiceDatabase.ChangeFBAdminPassword(const ANewPassword: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE change_sysdba_password(:cur_pwd, :new_pwd)';
    Q.SetParam('cur_pwd', GetPassword);
    Q.SetParam('new_pwd', ANewPassword);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.ChangeFBUserPassword(const ANewPassword: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE update_euser(:sysdba_pwd, :new_pwd)';
    Q.SetParam('sysdba_pwd', GetPassword);
    Q.SetParam('new_pwd', ANewPassword);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.ShutdownDatabase(const ADatabaseFile: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE shutdown_db(:sysdba_pwd, :db_path)';
    Q.SetParam('sysdba_pwd', GetPassword);
    Q.SetParam('db_path', ADatabaseFile);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

function TevServiceDatabase.ServerFileExists(const AFileName: String): Boolean;
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE file_exists(:file)';
    Q.SetParam('file', AFileName);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
      Result := Q.Fields[0].AsString = 'Y';
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.OnlineDatabase(const ADatabaseFile: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE online_db(:sysdba_pwd, :db_path)';
    Q.SetParam('sysdba_pwd', GetPassword);
    Q.SetParam('db_path', ADatabaseFile);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

function TevServiceDatabase.GetPassword: String;
begin
  Result := Params.Values['PASSWORD'];
end;

function TevServiceDatabase.GetUserName: String;
begin
  Result := Params.Values['USER_NAME'];
end;

procedure TevServiceDatabase.DeleteDatabaseFile(const ADatabasePath: String);
var
  Q: TevRawSQL;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.SQL.Text := 'EXECUTE PROCEDURE delete_database_file(:file, :password)';
    Q.SetParam('file', ADatabasePath);
    Q.SetParam('password', GetPassword);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.BackupDatabase(const ADatabaseFile: String; const ACompress, AScramble: Boolean; out ABackupData: IisStream);
var
  Q: TevRawSQL;
  BkpFile: String;
  ResData: TIBXSQLVAR;
  DBPath, s: String;
begin
  if AScramble then
  begin
    ctx_UpdateWait('Scrambling data');
    DBPath := NormalizePath(ExtractFilePathUD(ADatabaseFile)) + GetUniqueID + '.tmp';
    CopyDatabaseFile(ADatabaseFile, DBPath);
  end
  else
    DBPath := ADatabaseFile;

  try
    if AScramble then
      ScrambleDB(DBPath);

    s := 'Backing up ';
    if ACompress then
      s := s + 'and compressing ';
    s := s + 'database';

    ctx_UpdateWait(s);
    BkpFile := NormalizePath(ExtractFilePathUD(ADatabaseFile)) + GetUniqueID + '.gbk';
    if ACompress then
      BkpFile := BkpFile + '.bz2';

    Q := TevRawSQL.Create(FCurrentOwner);
    try
      Q.Database := Self;
      Q.SQL.Text := 'EXECUTE PROCEDURE backup_database(:db, :bkp, :usr, :pwd)';
      Q.SetParam('db',  DBPath);
      Q.SetParam('bkp', BkpFile);
      Q.SetParam('usr', GetUserName);
      Q.SetParam('pwd', GetPassword);

      Q.Database.DefaultTransaction.StartTransaction;
      try
        Q.ExecQuery;
        ResData := Q.Fields[0];
        BkpFile := AppTempFolder + GetUniqueID + '.tmp';
        ResData.SaveToFile(BkpFile);
      finally
        Q.Database.DefaultTransaction.Rollback;
      end;
    finally
      Q.Free;
    end;

    ABackupData := TisStream.CreateFromFile(BkpFile, True);
  finally
    if AScramble then
      DeleteDatabaseFile(DBPath);
  end;
end;

procedure TevServiceDatabase.RestoreDatabase(const ADatabaseFile: String;
  ABackupData: IisStream; const ACompressed: Boolean);
var
  Q: TevRawSQL;
  BkpFile: String;
begin
  BkpFile := NormalizePath(ExtractFilePathUD(ADatabaseFile)) + GetUniqueID + '.gbk';
  if ACompressed then
    BkpFile := BkpFile + '.bz2';

  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.SQL.Text := 'EXECUTE PROCEDURE restore_database(:bkp, :db, :usr, :pwd, :data)';
      Q.SetParam('bkp', BkpFile);
      Q.SetParam('db', ADatabaseFile);
      Q.SetParam('usr', GetUserName);
      Q.SetParam('pwd', GetPassword);
      Q.SetParam('data', ABackupData);

      Q.ExecQuery;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

function TevServiceDatabase.GetSystemInfo: String;
var
  Q: TevRawSQL;
begin
  Result := '';
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;
    Q.Database.DefaultTransaction.StartTransaction;
    try
      Q.SQL.Text := 'SELECT * FROM get_server_info';
      Q.ExecQuery;
      Result := Q.AsDataSet(True).Fields[0].AsString;
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

procedure TevServiceDatabase.ScrambleDB(const ADBPath: String);
const
  CREATE_SCRAMBLE_PROC =
    'CREATE OR ALTER PROCEDURE scramble_data(rw_placeholder BLOB SUB_TYPE 0 SEGMENT SIZE 80)'#13 +
    'AS'#13 +
    'DECLARE VARIABLE fld_nbr INTEGER;'#13 +
    'DECLARE VARIABLE tbl_nbr INTEGER;'#13 +
    'DECLARE VARIABLE n INTEGER;'#13 +
    'DECLARE VARIABLE rec_version INTEGER;'#13 +
    'DECLARE VARIABLE curr_val VARCHAR(20);'#13 +
    'DECLARE VARIABLE old_val VARCHAR(20);'#13 +
    'DECLARE VARIABLE val VARCHAR(20);'#13 +
    'BEGIN'#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''MAINTENANCE'', 1);'#13 +
    ''#13 +
    '  -- CL_NBR'#13 +
    '  n = 0;'#13 +
    '  SELECT t.cl_nbr FROM cl t WHERE CURRENT_DATE >= t.effective_date AND CURRENT_DATE < t.effective_until INTO n;'#13 +
    '  IF (n < 600000000) THEN'#13 +
    '    n = n + 600000000;'#13 +
    '  ELSE'#13 +
    '    EXIT; -- DB is already scrambled'#13 +
    ''#13 +
    '  SELECT t.nbr FROM ev_table t WHERE t.name = ''CL'' INTO tbl_nbr;'#13 +
    '  UPDATE ev_table_change t SET t.record_nbr = :n WHERE t.ev_table_nbr = :tbl_nbr;'#13 +
    ''#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''INT_CHANGE'', 1);'#13 +
    '  UPDATE cl t SET t.cl_nbr = :n, t.name = ''SCRAMBLED: '' || strcopy(t.name, 0, 29);'#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''INT_CHANGE'', NULL);'#13 +
    ''#13 +
    ''#13 +
    '  -- CO FEIN'#13 +
    '  SELECT f.nbr FROM ev_table t, ev_field f'#13 +
    '  WHERE t.nbr = f.ev_table_nbr AND t.name = ''CO'' AND f.name = ''FEIN'''#13 +
    '  INTO fld_nbr;'#13 +
    ''#13 +
    '  UPDATE ev_field_change t SET t.old_value = ''999999999'' WHERE t.ev_field_nbr = :fld_nbr;'#13 +
    ''#13 +
    '  n = 100000000;'#13 +
    '  old_val = '''';'#13 +
    '  FOR SELECT t.rec_version, t.fein FROM co t ORDER BY 2'#13 +
    '      INTO rec_version, curr_val'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    IF (curr_val <> old_val) THEN'#13 +
    '    BEGIN'#13 +
    '      old_val = curr_val;'#13 +
    '      n = n + 1;'#13 +
    '      val = n;'#13 +
    '    END'#13 +
    ''#13 +
    '    UPDATE co SET fein = :val WHERE rec_version = :rec_version;'#13 +
    '  END'#13 +
    ''#13 +
    ''#13 +
    '  -- EE SSN'#13 +
    '  SELECT f.nbr FROM ev_table t, ev_field f'#13 +
    '  WHERE t.nbr = f.ev_table_nbr AND t.name = ''CL_PERSON'' AND f.name = ''SOCIAL_SECURITY_NUMBER'''#13 +
    '  INTO fld_nbr;'#13 +
    ''#13 +
    '  UPDATE ev_field_change t SET t.old_value = ''999-99-9999'' WHERE t.ev_field_nbr = :fld_nbr;'#13 +
    ''#13 +
    '  n = 730000000;'#13 +
    '  old_val = '''';'#13 +
    '  FOR SELECT t.rec_version, t.social_security_number FROM cl_person t ORDER BY 2'#13 +
    '      INTO rec_version, curr_val'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    IF (curr_val <> old_val) THEN'#13 +
    '    BEGIN'#13 +
    '      old_val = curr_val;'#13 +
    '      n = n + 1;'#13 +
    '      IF (n > 999999999) THEN'#13 +
    '        EXECUTE PROCEDURE raise_error(''C1'', ''SSN scrambling error. Too many employees.'');'#13 +
    '      val = n;'#13 +
    '      val = strcopy(val, 0, 3) || ''-'' || strcopy(val, 3, 2) || ''-'' || strcopy(val, 5, 4);'#13 +
    '    END'#13 +
    ''#13 +
    '    UPDATE cl_person SET social_security_number = :val WHERE rec_version = :rec_version;'#13 +
    '  END'#13 +
    ''#13 +
    ''#13 +
    '  -- EE DEPENDANTS SSN'#13 +
    '  SELECT f.nbr FROM ev_table t, ev_field f'#13 +
    '  WHERE t.nbr = f.ev_table_nbr AND t.name = ''CL_PERSON_DEPENDENTS'' AND f.name = ''SOCIAL_SECURITY_NUMBER'''#13 +
    '  INTO fld_nbr;'#13 +
    ''#13 +
    '  UPDATE ev_field_change t SET t.old_value = ''999-99-9999'''#13 +
    '  WHERE t.ev_field_nbr = :fld_nbr AND t.old_value IS NOT NULL;'#13 +
    ''#13 +
    '--  n = 730000000; base is taken from the previous step'#13 +
    '  old_val = '''';'#13 +
    '  FOR SELECT t.rec_version, t.social_security_number FROM cl_person_dependents t'#13 +
    '      WHERE t.social_security_number IS NOT NULL ORDER BY 2'#13 +
    '      INTO rec_version, curr_val'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    IF (curr_val <> old_val) THEN'#13 +
    '    BEGIN'#13 +
    '      old_val = curr_val;'#13 +
    '      n = n + 1;'#13 +
    '      IF (n > 999999999) THEN'#13 +
    '        EXECUTE PROCEDURE raise_error(''C1'', ''SSN scrambling error. Too many dependants.'');'#13 +
    '      val = n;'#13 +
    '      val = strcopy(val, 0, 3) || ''-'' || strcopy(val, 3, 2) || ''-'' || strcopy(val, 5, 4);'#13 +
    '    END'#13 +
    ''#13 +
    '    UPDATE cl_person_dependents SET social_security_number = :val WHERE rec_version = :rec_version;'#13 +
    '  END'#13 +
    ''#13 +
    ''#13 +
    '  -- EE BANK ACCOUNTS'#13 +
    '  SELECT f.nbr FROM ev_table t, ev_field f'#13 +
    '  WHERE t.nbr = f.ev_table_nbr AND t.name = ''EE_DIRECT_DEPOSIT'' AND f.name = ''EE_BANK_ACCOUNT_NUMBER'''#13 +
    '  INTO fld_nbr;'#13 +
    ''#13 +
    '  UPDATE ev_field_change t SET t.old_value = ''99999999'' WHERE t.ev_field_nbr = :fld_nbr;'#13 +
    ''#13 +
    '  n = 10000000;'#13 +
    '  old_val = '''';'#13 +
    '  FOR SELECT t.rec_version, t.ee_bank_account_number FROM ee_direct_deposit t ORDER BY 2'#13 +
    '      INTO rec_version, curr_val'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    IF (curr_val <> old_val) THEN'#13 +
    '    BEGIN'#13 +
    '      old_val = curr_val;'#13 +
    '      n = n + 1;'#13 +
    '      val = n;'#13 +
    '    END'#13 +
    ''#13 +
    '    UPDATE ee_direct_deposit SET ee_bank_account_number = :val WHERE rec_version = :rec_version;'#13 +
    '  END'#13 +
    ''#13 +
    ''#13 +
    '  -- CL BANK ACCOUNTS'#13 +
    '  SELECT f.nbr FROM ev_table t, ev_field f'#13 +
    '  WHERE t.nbr = f.ev_table_nbr AND t.name = ''CL_BANK_ACCOUNT'' AND f.name = ''CUSTOM_BANK_ACCOUNT_NUMBER'''#13 +
    '  INTO fld_nbr;'#13 +
    ''#13 +
    '  UPDATE ev_field_change t SET t.old_value = ''99999999'' WHERE t.ev_field_nbr = :fld_nbr;'#13 +
    ''#13 +
    '--  n = 10000000; base is taken from the previous step'#13 +
    '  old_val = '''';'#13 +
    '  FOR SELECT t.rec_version, t.custom_bank_account_number FROM cl_bank_account t ORDER BY 2'#13 +
    '      INTO rec_version, curr_val'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    IF (curr_val <> old_val) THEN'#13 +
    '    BEGIN'#13 +
    '      old_val = curr_val;'#13 +
    '      n = n + 1;'#13 +
    '      val = n;'#13 +
    '    END'#13 +
    ''#13 +
    '    UPDATE cl_bank_account SET custom_bank_account_number = :val WHERE rec_version = :rec_version;'#13 +
    '  END'#13 +
    ''#13 +
    ''#13 +
    '  -- CL_BLOB'#13 +
    '  UPDATE ev_field_change_blob t SET t.data = NULL;'#13 +
    ''#13 +
    '  FOR SELECT b.rec_version FROM cl_blob b, co_tax_return_runs r'#13 +
    '      WHERE b.cl_blob_nbr = r.cl_blob_nbr AND b.data IS NOT NULL'#13 +
    '      INTO rec_version'#13 +
    '  DO'#13 +
    '  BEGIN'#13 +
    '    UPDATE cl_blob SET data = :rw_placeholder WHERE rec_version = :rec_version;'#13 +
    '  END'#13 +
    ''#13 +
    '  rdb$set_context(''USER_TRANSACTION'', ''MAINTENANCE'', NULL);'#13 +
    'END';

  EXECUTE_SCRAMBLE_PROC = 'EXECUTE PROCEDURE scramble_data(:rw_report)';

  DROP_SCRAMBLE_PROC = 'DROP PROCEDURE scramble_data';

var
  Q: TevRawSQL;
  ScramblingDB: TevCustomDatabase;
  ScramblingTransaction: TevTransaction;
  ReportData: IisStream;
begin
  ReportData := GetBinaryResource('SCRAMBLED_TAX_RETURN', 'DAT');

  ScramblingDB := TevCustomDatabase.Create;
  try
    ScramblingDB.DatabaseName := GetHost + ':' + ADBPath;
    ScramblingDB.Params.Add('USER_NAME=' + GetUserName);
    ScramblingDB.Params.Add('PASSWORD=' + GetPassword);
    ScramblingDB.Open;

    Q := TevRawSQL.Create(FCurrentOwner);
    ScramblingTransaction := TevTransaction.Create(nil);
    try
      ScramblingTransaction.ReadOnly := False;
      ScramblingTransaction.ReadCommited := True;
      ScramblingTransaction.AddDatabase(ScramblingDB);
      Q.Database := ScramblingDB;
      Q.Transaction := ScramblingTransaction;

      ScramblingTransaction.StartTransaction;
      try
        Q.SQL.Text := CREATE_SCRAMBLE_PROC;
        Q.ParamCheck := False;
        Q.ExecQuery;

        Q.Close;
        Q.SQL.Text := EXECUTE_SCRAMBLE_PROC;
        Q.ParamCheck := True;
        Q.SetParam('rw_report', ReportData);
        Q.ExecQuery;

        Q.Close;
        Q.SQL.Text := DROP_SCRAMBLE_PROC;
        Q.ExecQuery;

        ScramblingTransaction.Commit;
      except
        ScramblingTransaction.Rollback;
        raise;
      end;
    finally
      Q.Free;
      ScramblingTransaction.Free;
    end;

  finally
    ScramblingDB.Free;
  end;
end;

function TevServiceDatabase.GetFolderContentAndSize(const APath, AMask: String): IevDataSet;
var
  Q: TevRawSQL;
  sql: String;
begin
  Q := TevRawSQL.Create(FCurrentOwner);
  try
    Q.Database := Self;

    sql := 'SELECT l.result file_name, GetFileSize(CAST(:folder as VARCHAR(1024)) || ''/'' || l.result) file_size FROM get_file_list(:folder) l';
    if AMask <> '' then
      sql := sql + ' WHERE l.result like ''' + AMask + '''';
    Q.SQL.Text := sql;
    Q.SetParam('folder', APath);

    Q.Database.DefaultTransaction.StartTransaction;
    try
      Result := Q.ExecAsDataSet(True);
    finally
      Q.Database.DefaultTransaction.Rollback;
    end;
  finally
    Q.Free;
  end;
end;

{ TevServiceDBConnection }

constructor TevServiceDBConnection.Create(const AName: String);
var
  s: String;
begin
  inherited;
  FDB := TevServiceDatabase.Create;
  s := AName;
  s := GetNextStrValue(s, ':');
  Database.Host := s;
  Database.Name := MakeValidComponentName(Database.Host);
end;

function TevServiceDBConnection.Database: TevServiceDatabase;
begin
  Result := TevServiceDatabase(FDB);
end;

{ TevCustomDBConnection }
function TevCustomDBConnection.Database: TIBDatabase;
begin
  Result := FDB;
end;

destructor TevCustomDBConnection.Destroy;

  procedure DisconnectDBInBackground(const Params: TTaskParamList);
  begin
    // DB disconnect operation sometimes hangs forever!
    // It is a potential memory leak, but I've no other solution yet.
    TIBDatabase(Pointer(Integer(Params[0]))).Free;
  end;

begin
  // In orfer to avoid calling this in critical section
  if GlobalThreadManagerIsActive then
    GlobalThreadManager.RunTask(@DisconnectDBInBackground, MakeTaskParams([Integer(Pointer(FDB))]), 'Disconnect ' + FDB.DatabaseName)
  else
    FreeAndNil(FDB);

  inherited;
end;


{ TevDataSyncPacketCreator }

procedure TevDataSyncPacketCreator.BuildAuditFieldChangeDS;
var
  ds: IevDataSet;
  BlobNbrs: IisStringList;
begin
  FdsEvTableChange.First;

  ds := FOwner.ExecRawQuery(FDBType, 'SELECT f.nbr FROM ev_field f WHERE f.field_type = ''B''', [], True);
  BlobNbrs := ds.GetColumnValues('nbr');

  FdsEvFieldChange := FOwner.ExecRawQuery(
    FDBType,
    Format(
      'EXECUTE BLOCK'#13 +
      'RETURNS(ev_table_change_nbr INTEGER, ev_field_nbr INTEGER,'#13 +
      '        old_value VARCHAR(512), blob_data BLOB SUB_TYPE 0 SEGMENT SIZE 80)'#13 +
      'AS'#13 +
      'DECLARE VARIABLE ref_nbr VARCHAR(512);'#13 +
      'BEGIN'#13 +
      '  FOR SELECT fld.ev_table_change_nbr, fld.ev_field_nbr, fld.old_value'#13 +
      '      FROM ev_transaction tr, ev_table_change tbl, ev_field_change fld'#13 +
      '      WHERE'#13 +
      '        tbl.ev_transaction_nbr = tr.nbr AND'#13 +
      '        fld.ev_table_change_nbr = tbl.nbr AND'#13 +
      '        tr.commit_nbr >= %d'#13 +
      '      INTO ev_table_change_nbr, ev_field_nbr, old_value'#13 +
      '  DO'#13 +
      '  BEGIN'#13 +
      '    blob_data = NULL;'#13 +
      '    IF (ev_field_nbr IN (%s)) THEN'#13 +
      '      SELECT data FROM ev_field_change_blob blb WHERE blb.nbr = CAST(:old_value AS INTEGER)'#13 +
      '      INTO :blob_data;'#13 +
      ''#13 +
      '    SUSPEND;'#13 +
      '  END'#13 +
      'END',
      [FStartTransNbr, BlobNbrs.CommaText]),
    [], True);

  FResult.WriteObject(FdsEvFieldChange);

  AbortIfCurrentThreadTaskTerminated;  
end;

procedure TevDataSyncPacketCreator.BuildAuditTableChangeDS;
begin
  FdsEvTableChange := FOwner.ExecRawQuery(
    FDBType,
    'SELECT tbl.*'#13 +
    'FROM ev_transaction tr, ev_table_change tbl'#13 +
    'WHERE'#13 +
    '  tbl.ev_transaction_nbr = tr.nbr AND'#13 +
    '  tr.commit_nbr >= :TransNbr',
    [FStartTransNbr], True);

  FResult.WriteObject(FdsEvTableChange);

  AbortIfCurrentThreadTaskTerminated;  
end;

function TevDataSyncPacketCreator.BuildAuditTransactionDS: Integer;
begin
  FdsEvTransaction := FOwner.ExecRawQuery(FDBType,
    'SELECT t.* FROM ev_transaction t WHERE t.commit_nbr >= :TransNbr',
    [FStartTransNbr], True);

  FdsEvTransaction.IndexFieldNames := 'commit_nbr';
  FdsEvTransaction.Last;
  Result := FdsEvTransaction.FieldByName('commit_nbr').AsInteger;

  FResult.WriteObject(FdsEvTransaction);

  AbortIfCurrentThreadTaskTerminated;
end;

procedure TevDataSyncPacketCreator.BuildDataChange;
var
  TableList, TableSyncPacket: IisListOfValues;
  CurrNbr: Integer;
  NbrFld: TField;
  ds: IevDataSet;
  i: Integer;
begin
  TableList := TisListOfValues.Create;

  ds := FOwner.ExecRawQuery(FDBType, 'SELECT nbr, name FROM ev_table t', [], True);
  ds.IndexFieldNames := 'nbr';

  NbrFld := FdsEvTableChange.FieldByName('ev_table_nbr');
  FdsEvTableChange.IndexFieldNames := 'ev_table_nbr;record_id;nbr';
  CurrNbr := 0;

  // Build unique list of modified tables
  FdsEvTableChange.First;
  while not FdsEvTableChange.Eof do
  begin
    if CurrNbr <> NbrFld.AsInteger then
    begin
      CurrNbr := NbrFld.AsInteger;
      Assert(ds.FindKey([CurrNbr]));
      TableList.AddValue(ds.Fields[1].AsString, CurrNbr);
    end;

    FdsEvTableChange.Next;
  end;

  for i := 0 to TableList.Count - 1 do
  begin
    try
      TableSyncPacket := BuildDataTableChange(TableList[i].Name, TableList[i].Value);
      if Assigned(TableSyncPacket) then
        FResult.WriteObject(TableSyncPacket);
    except
      on E: Exception do
      begin
        E.Message := 'Cannot retrieve delta for table ' + TableList[i].Name + #13 + E.Message;
        raise;
      end;
    end;

    AbortIfCurrentThreadTaskTerminated;      
  end;
end;

function TevDataSyncPacketCreator.BuildDataTableChange(const ATableName: String; const ATableNbr: Integer): IisListOfValues;
var
  dsNonVerFields: IevDataSet;
  dsVerFields: IevDataSet;
  ChangedRecVers, ChangedNbrs: IisIntegerList;
  InsRec, UpdRec: Boolean;
  CurRecId, CurRecNbr: Integer;
  RecIdFld, RecNbrFld, ChangeTypeFld: TField;
  NonVerFieldNames: IisStringList;
  VerFieldNames: IisStringList;
  s: String;

  procedure AddOperInfo;
  begin
    if InsRec then
    begin
      if ChangedNbrs.IndexOf(CurRecNbr) = -1 then
        ChangedNbrs.Add(CurRecNbr);
      ChangedRecVers.Add(CurRecId);
    end
    else if UpdRec then
    begin
      if ChangedNbrs.IndexOf(CurRecNbr) = -1 then
        ChangedNbrs.Add(CurRecNbr);
      ChangedRecVers.Add(CurRecId);
    end
  end;

  procedure PrepareDSFields;
  var
    ds: IevDataSet;
    s: String;
  begin
    ds := FOwner.ExecRawQuery(FDBType,
      'SELECT f.name, f.versioned FROM ev_field f WHERE f.ev_table_nbr = :tbl_nbr', [ATableNbr], True);

    NonVerFieldNames := TisStringList.Create;
    VerFieldNames := TisStringList.Create;

    ds.First;
    while not ds.Eof do
    begin
      s := ds.Fields[0].AsString;
      if not Contains('EFFECTIVE_DATE,EFFECTIVE_UNTIL,REC_VERSION,'+ ATableName + '_NBR,', s + ',') then
      begin
        if (ds.Fields[1].AsString = 'Y') then
          VerFieldNames.Add(s)
        else
          NonVerFieldNames.Add(s);
      end;
      ds.Next;
    end;
  end;

  function BuildCurrRecData(const ASQL: String; const AKeyList: IisIntegerList): IevDataSet;
  var
    ds: IevDataSet;
    s: String;
    i, j: Integer;
  begin
    // SQL statement has a limit of 64K but transaction may contain lot of affected records
    i := 0;
    while i < AKeyList.Count do
    begin
      s := '';
      j := i;
      while (j < AKeyList.Count) and (j <= i + 999) do
      begin
        AddStrValue(s, IntToStr(AKeyList[j]), ',');
        Inc(j);
      end;
      Inc(i, 1000);
      s := StringReplace(ASQL, '#nbrs', s, [rfReplaceAll]);

      ds := FOwner.OpenQuery(FDBType, s, [], True);

      if not Assigned(Result) then
        Result := ds
      else
      begin
        ds.First;
        while not ds.Eof do
        begin
          Result.Append;
          for j := 0 to ds.Fields.Count - 1 do
            Result.Fields[j].Value := ds.Fields[j].Value;
          Result.Post;
          ds.Next;
        end;
      end;
    end;
  end;

begin
  FdsEvTableChange.SetRange([ATableNbr, 0, 0], [ATableNbr, MaxInt, MaxInt]);

  if FdsEvTableChange.RecordCount = 0 then
    Exit;

  RecIdFld := FdsEvTableChange.FieldByName('record_id');
  RecNbrFld := FdsEvTableChange.FieldByName('record_nbr');
  ChangeTypeFld := FdsEvTableChange.FieldByName('change_type');

  ChangedRecVers := TisIntegerList.Create;
  ChangedNbrs := TisIntegerList.Create;
  CurRecId := RecIdFld.AsInteger;
  CurRecNbr := RecNbrFld.AsInteger;
  InsRec := False;
  UpdRec := False;
  FdsEvTableChange.First;
  while not FdsEvTableChange.Eof do
  begin
    if CurRecId <> RecIdFld.AsInteger then
    begin
      AddOperInfo;
      CurRecId := RecIdFld.AsInteger;
      CurRecNbr := RecNbrFld.AsInteger;
      InsRec := False;
      UpdRec := False;
    end;

    case ChangeTypeFld.AsString[1] of
    'I': InsRec := True;
    'U': UpdRec := True;
    end;

    FdsEvTableChange.Next;
  end;
  AddOperInfo;

  // Pull out current data for changed records
  if ChangedRecVers.Count > 0 then
  begin
    PrepareDSFields;

    if NonVerFieldNames.Count > 0 then
    begin
      s := Format('SELECT %s_NBR, REC_VERSION, EFFECTIVE_DATE, EFFECTIVE_UNTIL, %s FROM %s WHERE %s_nbr IN (#nbrs) AND effective_date = ''%s''',
                  [ATableName, NonVerFieldNames.CommaText, ATableName, ATableName, DateToUTC(BeginOfTime)]);
      dsNonVerFields := BuildCurrRecData(s, ChangedNbrs);
    end;

    if VerFieldNames.Count > 0 then
    begin
      s := Format('SELECT %s_NBR, REC_VERSION, EFFECTIVE_DATE, EFFECTIVE_UNTIL, %s FROM %s WHERE rec_version IN (#nbrs)',
                  [ATableName, VerFieldNames.CommaText, ATableName]);
      dsVerFields := BuildCurrRecData(s, ChangedRecVers);
    end;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('Table', ATableName);
  Result.AddValue('TableNbr', ATableNbr);
  Result.AddValue('NonVerData', dsNonVerFields);
  Result.AddValue('VerData', dsVerFields);
end;

procedure TevDataSyncPacketCreator.BuildGeneratorsChange;
var
  dsGen, DS: IevDataSet;
  Res: IisListOfValues;
begin
  Res := TisListOfValues.Create;

  dsGen := FOwner.ExecRawQuery(FDBType, 'SELECT RDB$GENERATOR_NAME FROM RDB$GENERATORS WHERE RDB$SYSTEM_FLAG = 0 OR RDB$SYSTEM_FLAG IS NULL', [], True);
  while not dsGen.Eof do
  begin
    DS := FOwner.ExecRawQuery(FDBType, 'SELECT GEN_ID(' + dsGen.Fields[0].AsString + ', 0) FROM ev_database', [], True);
    Res.AddValue(Trim(dsGen.Fields[0].AsString), DS.Fields[0].AsInteger);
    dsGen.Next;
  end;

  FResult.WriteObject(Res);

  AbortIfCurrentThreadTaskTerminated;
end;

constructor TevDataSyncPacketCreator.Create(const AOwner: TevFirebird);
begin
  FOwner := AOwner;
end;

function TevDataSyncPacketCreator.Execute(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
begin
  FDBType := ADBType;
  FStartTransNbr := AStartTransNbr;

  try
    // Start snapshot transaction
    if FOwner.GetDBByType(FDBType).DefaultTransaction.InTransaction then
      FOwner.GetDBByType(FDBType).DefaultTransaction.Commit;
    (FOwner.GetDBByType(FDBType).DefaultTransaction as TevTransaction).ReadCommited := False;

    FResult := TisStream.Create;

    AEndTransNbr := BuildAuditTransactionDS;
    if AEndTransNbr = 0 then
      Exit;

    BuildAuditTableChangeDS;
    BuildAuditFieldChangeDS;
    BuildGeneratorsChange;
    BuildDataChange;

    Result := FResult;
    FResult := nil;
  finally
    FDBType := dbtUnknown;
    FStartTransNbr := 0;
  end;
end;

{ TevDataSyncPacketApplier }

constructor TevDataSyncPacketApplier.Create(const AOwner: TevFirebird);
begin
  FOwner := AOwner;
end;

procedure TevDataSyncPacketApplier.Execute(const ADBType: TevDBType; const ADataSyncPacket: IisStream);
begin
  FDBType := ADBType;
  FData := ADataSyncPacket;

  try
    FData.Position := 0;
    FOwner.StartTransaction([FDBType]);
    try
      // Turn off ALL triggers
      FOwner.ExecRawQuery(FDBType, FOwner.SetTransactionVariableSQL('MAINTENANCE', 1), [], True);

      SyncEvTransaction;
      SyncEvTableChange;
      SyncEvFieldChange;
      SyncGenerators;
      SyncData;

      FOwner.Commit;
    except
      FOwner.Rollback;
      raise;
    end;
  finally
    FData := nil;
    FDBType := dbtUnknown;
  end;
end;

function TevDataSyncPacketApplier.ExecQuery(const ASQL: String; const AParamVals: array of Variant): IevDataSet;
var
  Q: IevSQL;
begin
  FOwner.FDBOperLock.Lock;
  try
    Q := FOwner.CreateRawSQL(ASQL, FDBType);
    try
      Q.SetParams(AParamVals);
      Result := Q.Open(True);
    except
      on E: Exception do
        raise EevException.CreateDetailed(E.Message, FOwner.BuildErrorDetails(Q), True);
    end;
  finally
    FOwner.FDBOperLock.Unlock;
  end;
end;

procedure TevDataSyncPacketApplier.SyncData;
var
  SyncData: IisListOfValues;
begin
  FdsEvTableChange.IndexFieldNames := 'ev_table_nbr;record_id;nbr';

  while not FData.EOS do
  begin
    SyncData := FData.ReadObject(nil) as IisListOfValues;
    try
      SyncDataTable(SyncData);
    except
      on E: Exception do
      begin
        E.Message := 'Cannot synchronize table ' + SyncData.Value['Table'] + #13 + E.Message;
        raise;
      end;
    end;

    AbortIfCurrentThreadTaskTerminated;
  end;
end;

procedure TevDataSyncPacketApplier.SyncDataTable(const ASyncData: IisListOfValues);
var
  TableName: String;
  TableNbr: Integer;
  dsNonVerData, dsVerData: IevDataSet;
  InsOper, UpdOper: IisListOfValues;
  DelOper: IisIntegerList;
  i, j: Integer;
  Q: IevSQL;
  s: String;
  last_cnt: Integer;
  UpdatedNbrs: IisStringList;
  d: TisDate;

  procedure PrepareOperations;
  var
    InsRec, DelRec, UpdRec: Boolean;
    CurRecId, CurRecNbr: Integer;
    RecIdFld, RecNbrFld, ChangeTypeFld: TField;

    procedure AddOperInfo;
    begin
      if DelRec and not InsRec then
        DelOper.Add(CurRecId);

      if InsRec and not DelRec then
        InsOper.AddValue(IntToStr(CurRecId), CurRecNbr)
      else if UpdRec and not DelRec then
        UpdOper.AddValue(IntToStr(CurRecId), CurRecNbr);
    end;

  begin
    RecIdFld := FdsEvTableChange.FieldByName('record_id');
    RecNbrFld := FdsEvTableChange.FieldByName('record_nbr');
    ChangeTypeFld := FdsEvTableChange.FieldByName('change_type');

    InsOper := TisListOfValues.Create;
    UpdOper := TisListOfValues.Create;
    DelOper := TisIntegerList.Create;

    CurRecId := RecIdFld.AsInteger;
    CurRecNbr := RecNbrFld.AsInteger;
    InsRec := False;
    DelRec := False;
    UpdRec := False;
    FdsEvTableChange.First;
    while not FdsEvTableChange.Eof do
    begin
      if CurRecId <> RecIdFld.AsInteger then
      begin
        AddOperInfo;
        CurRecId := RecIdFld.AsInteger;
        CurRecNbr := RecNbrFld.AsInteger;
        InsRec := False;
        DelRec := False;
        UpdRec := False;
      end;

      case ChangeTypeFld.AsString[1] of
      'I': InsRec := True;
      'D': DelRec := True;
      'U': UpdRec := True;
      end;

      FdsEvTableChange.Next;
    end;
    AddOperInfo;
  end;

begin
  TableName := ASyncData.Value['Table'];
  TableNbr := ASyncData.Value['TableNbr'];
  dsNonVerData := IInterface(ASyncData.Value['NonVerData']) as IevDataSet;
  dsVerData := IInterface(ASyncData.Value['VerData']) as IevDataSet;

  FdsEvTableChange.SetRange([TableNbr, 0, 0], [TableNbr, MaxInt, MaxInt]);
  PrepareOperations;

  // Delete records
  // Delete non 1/1/1900 records first
  i := 0;
  while i < DelOper.Count do
  begin
    // SQL statement has a limit of 64K but transaction may contain lot of affected records
    s := '';
    j := 0;
    while (i < DelOper.Count) and (j <= 999) do
    begin
      AddStrValue(s, IntToStr(DelOper[i]), ',');
      Inc(j);
      Inc(i);
    end;
    if s <> '' then
      ExecQuery('DELETE FROM ' + TableName + ' WHERE rec_version IN (' + s + ') and effective_date <> ''1/1/1900''', []);
  end;

  // Delete the rest of records
  i := 0;
  while i < DelOper.Count do
  begin
    // SQL statement has a limit of 64K but transaction may contain lot of affected records
    s := '';
    j := 0;
    while (i < DelOper.Count) and (j <= 999) do
    begin
      AddStrValue(s, IntToStr(DelOper[i]), ',');
      Inc(j);
      Inc(i);
    end;
    if s <> '' then
      ExecQuery('DELETE FROM ' + TableName + ' WHERE rec_version IN (' + s + ')', []);
  end;
  DelOper := nil;

  if not Assigned(dsNonVerData) and not Assigned(dsVerData) then
    Exit; // no more operations

  if Assigned(dsVerData) then
    dsVerData.IndexFieldNames := 'rec_version';

  // Update versioned fields
  if Assigned(dsVerData) then
  begin
    s := '';
    for i := 2 to dsVerData.FieldCount - 1 do // ommit NBR and REC_VERSION fields
      AddStrValue(s, dsVerData.Fields[i].FieldName + '=:' + dsVerData.Fields[i].FieldName, ',');

    Q := FOwner.CreateSQL(Format('UPDATE %s SET %s WHERE rec_version = :rec_version', [TableName, s]));

    // Sequence of operations cannot be inferred, so here is a retry logic
    while UpdOper.Count > 0 do
    begin
      last_cnt := UpdOper.Count;
      for i := UpdOper.Count - 1 downto 0 do
      begin
        Assert(dsVerData.FindKey([StrToInt(UpdOper[i].Name)]));
        for j := 1 to dsVerData.FieldCount - 1 do // ommit NBR field
          Q.SetParam(dsVerData.Fields[j].FieldName, dsVerData.Fields[j].Value);

        try
          Q.Exec;
          UpdOper.Delete(i);
        except
          // Suppress exceptions here for further retry logic
        end;
      end;

      ErrorIf(last_cnt = UpdOper.Count, 'Cannot resolve update operation');
    end;
    Q := nil;
    UpdOper := nil;
  end;

  // Insert records
  s := TableName + '_NBR,REC_VERSION,EFFECTIVE_DATE,EFFECTIVE_UNTIL';
  if Assigned(dsNonVerData) then
    for i := 4 to dsNonVerData.FieldCount - 1 do // ommit system fields
      AddStrValue(s, dsNonVerData.Fields[i].FieldName, ',');

  if Assigned(dsVerData) then
    for i := 4 to dsVerData.FieldCount - 1 do // ommit system fields
      AddStrValue(s, dsVerData.Fields[i].FieldName, ',');

  Q := FOwner.CreateSQL(Format('INSERT INTO %s (%s) VALUES (%s)', [TableName, s, ':' + StringReplace(s, ',', ',:', [rfReplaceAll])]));

  UpdatedNbrs := TisStringList.CreateUnique;

  if Assigned(dsNonVerData) then
    dsNonVerData.IndexFieldNames := dsNonVerData.Fields[0].FieldName;

  // Insert 1/1/1900 records first
  i := 0;
  while i < InsOper.Count do
  begin
    if Assigned(dsVerData) then
    begin
      Assert(dsVerData.FindKey([StrToInt(InsOper[i].Name)]));
      d := Trunc(dsVerData.Fields[2].AsDateTime);
    end
    else
    begin
      Assert(dsNonVerData.FindKey([InsOper[i].Value]));
      d := Trunc(dsNonVerData.Fields[2].AsDateTime);
    end;

    if d = BeginOfTime then
    begin
      if Assigned(dsNonVerData) then
      begin
        if Assigned(dsVerData) then
          Assert(dsNonVerData.FindKey([InsOper[i].Value]));

        for j := 0 to dsNonVerData.FieldCount - 1 do
          Q.SetParam(dsNonVerData.Fields[j].FieldName, dsNonVerData.Fields[j].Value);
      end;

      if Assigned(dsVerData) then
        for j := 0 to dsVerData.FieldCount - 1 do
          Q.SetParam(dsVerData.Fields[j].FieldName, dsVerData.Fields[j].Value);

      Q.Exec;

      UpdatedNbrs.Add(InsOper[i].Value);
      InsOper.Delete(i);
    end
    else
      Inc(i);
  end;

  // Insert the rest of records
  i := 0;
  while i < InsOper.Count do
  begin
    if Assigned(dsNonVerData) then
    begin
      Assert(dsNonVerData.FindKey([InsOper[i].Value]));
      for j := 0 to dsNonVerData.FieldCount - 1 do
        Q.SetParam(dsNonVerData.Fields[j].FieldName, dsNonVerData.Fields[j].Value);
    end;

    if Assigned(dsVerData) then
    begin
      Assert(dsVerData.FindKey([StrToInt(InsOper[i].Name)]));
      for j := 0 to dsVerData.FieldCount - 1 do
        Q.SetParam(dsVerData.Fields[j].FieldName, dsVerData.Fields[j].Value);
    end;

    Q.Exec;

    UpdatedNbrs.Add(InsOper[i].Value);
    Inc(i);
  end;

  // Update the rest of non versioned fields
  if Assigned(dsNonVerData) then
  begin
    s := '';
    for i := 4 to dsNonVerData.FieldCount - 1 do
      AddStrValue(s, dsNonVerData.Fields[i].FieldName + '=:' + dsNonVerData.Fields[i].FieldName, ',');

    Q := FOwner.CreateSQL(Format('UPDATE %s SET %s WHERE %s_nbr = :%s_nbr', [TableName, s, TableName, TableName]));

    dsNonVerData.First;
    while not dsNonVerData.Eof do
    begin
      if UpdatedNbrs.IndexOf(dsNonVerData.Fields[0].AsString) = -1 then
      begin
        for i := 4 to dsNonVerData.FieldCount - 1 do
          Q.SetParam(dsNonVerData.Fields[i].FieldName, dsNonVerData.Fields[i].Value);
        Q.SetParam(TableName + '_NBR', dsNonVerData.Fields[0].Value);

        Q.Exec;
      end;

      dsNonVerData.Next;
    end;
  end;
end;

procedure TevDataSyncPacketApplier.SyncEvFieldChange;
var
  dsEvFieldChange, dsBlobFlds: IevDataSet;
  s: String;
  i: Integer;
  FldNbrField, BlobField, ValField: TField;
  UpdateSQL1, UpdateSQL2: IevSQL;
  TableChangeNbrFld: TField;
  CurrTableChangeNbr: Integer;
begin
  dsEvFieldChange := FData.ReadObject(nil) as IevDataSet;

  FdsEvTableChange.IndexFieldNames := 'nbr';
  dsEvFieldChange.IndexFieldNames := 'ev_table_change_nbr';
  TableChangeNbrFld := dsEvFieldChange.FieldByName('ev_table_change_nbr');

  dsBlobFlds := FOwner.OpenQuery(FDBType, 'SELECT f.nbr, f.name FROM ev_field f WHERE f.field_type = ''B''', [], True);
  dsBlobFlds.IndexFieldNames := 'nbr';

  FldNbrField := dsEvFieldChange.FieldByName('ev_field_nbr');
  ValField := dsEvFieldChange.FieldByName('old_value');
  BlobField := dsEvFieldChange.FieldByName('blob_data');

  s := '';
  for i := 0 to dsEvFieldChange.Fields.Count - 2 do
    AddStrValue(s, dsEvFieldChange.Fields[i].FieldName, ',');

  UpdateSQL1 := CreateQuery(Format('INSERT INTO ev_field_change (%s) VALUES(%s)', [s, ':' + StringReplace(s, ',', ',:', [rfReplaceAll])]));
  UpdateSQL2 := CreateQuery('INSERT INTO ev_field_change_blob (nbr, data) VALUES(:nbr, :data)');
  dsEvFieldChange.First;
  CurrTableChangeNbr := TableChangeNbrFld.AsInteger;
  while not dsEvFieldChange.Eof do
  begin
    if CurrTableChangeNbr <> TableChangeNbrFld.AsInteger then
    begin
      CurrTableChangeNbr := TableChangeNbrFld.AsInteger;
      if not FdsEvTableChange.FindKey([CurrTableChangeNbr]) then
      begin
        while not dsEvFieldChange.Eof and (CurrTableChangeNbr = TableChangeNbrFld.AsInteger) do
          dsEvFieldChange.Next;
        Continue;
      end;
    end;

    for i := 0 to dsEvFieldChange.Fields.Count - 2 do
      UpdateSQL1.SetParam(i, dsEvFieldChange.Fields[i].Value);
    UpdateSQL1.Exec;

    if not ValField.IsNull and dsBlobFlds.FindKey([FldNbrField.AsInteger]) then
    begin
      UpdateSQL2.SetParam(0, ValField.AsInteger);
      UpdateSQL2.SetParam(1, BlobField.Value);
      UpdateSQL2.Exec;
    end;

    dsEvFieldChange.Next;

    AbortIfCurrentThreadTaskTerminated;
  end;
end;

procedure TevDataSyncPacketApplier.SyncEvTableChange;
var
  s: String;
  i, CurrTrans: Integer;
  UpdateSQL1: IevSQL;
  TransFld: TField;
begin
  FdsEvTableChange := FData.ReadObject(nil) as IevDataSet;

  FdsEvTableChange.IndexFieldNames := 'ev_transaction_nbr';
  FdsEvTransaction.IndexFieldNames := 'nbr';
  TransFld := FdsEvTableChange.FieldByName('ev_transaction_nbr');

  s := '';
  for i := 0 to FdsEvTableChange.Fields.Count - 1 do
    AddStrValue(s, FdsEvTableChange.Fields[i].FieldName, ',');

  UpdateSQL1 := CreateQuery(Format('INSERT INTO ev_table_change (%s) VALUES(%s)', [s, ':' + StringReplace(s, ',', ',:', [rfReplaceAll])]));
  FdsEvTableChange.First;
  CurrTrans := 0;
  while not FdsEvTableChange.Eof do
  begin
    if CurrTrans <> TransFld.AsInteger then
    begin
      CurrTrans := TransFld.AsInteger;
      if not FdsEvTransaction.FindKey([CurrTrans]) then
      begin
        while not FdsEvTableChange.Eof and (CurrTrans = TransFld.AsInteger) do
          FdsEvTableChange.Delete;
        Continue;
      end;
    end;

    for i := 0 to FdsEvTableChange.Fields.Count - 1 do
      UpdateSQL1.SetParam(i, FdsEvTableChange.Fields[i].Value);
    UpdateSQL1.Exec;
    FdsEvTableChange.Next;

    AbortIfCurrentThreadTaskTerminated;
  end;

  FdsEvTransaction := nil;
end;

procedure TevDataSyncPacketApplier.SyncEvTransaction;
var
  s: String;
  i: Integer;
  UpdateSQL1: IevSQL;
begin
  FdsEvTransaction := FData.ReadObject(nil) as IevDataSet;

  s := '';
  for i := 0 to FdsEvTransaction.Fields.Count - 1 do
    AddStrValue(s, FdsEvTransaction.Fields[i].FieldName, ',');

  UpdateSQL1 := CreateQuery(Format('INSERT INTO ev_transaction (%s) VALUES(%s)', [s, ':' + StringReplace(s, ',', ',:', [rfReplaceAll])]));
  FdsEvTransaction.First;
  while not FdsEvTransaction.Eof do
  begin
    for i := 0 to FdsEvTransaction.Fields.Count - 1 do
      UpdateSQL1.SetParam(i, FdsEvTransaction.Fields[i].Value);

    try
      UpdateSQL1.Exec;
      FdsEvTransaction.Next;
    except
      on E: EIBInterBaseError do
      begin
        // It is inevitable in the existing ADR design to run into a situation of overlapped synchronization packets
        // So we have to check if this is the case and ignore errors related to already applied transactions
        if Contains(E.Message, 'PK_EV_TRANSACTION') or Contains(E.Message, 'AK_EV_TRANSACTION') then
          FdsEvTransaction.Delete // Just delete it. Thus the rest of related data will be omitted from synchronization
        else
          raise;
      end
      else
        raise;
    end;

    AbortIfCurrentThreadTaskTerminated;    
  end;
end;

procedure TevDataSyncPacketApplier.SyncGenerators;
var
  SyncData: IisListOfValues;
  i: Integer;
begin
  SyncData := FData.ReadObject(nil) as IisListOfValues;

  for i := 0 to SyncData.Count - 1 do
    FOwner.SetGeneratorValue(SyncData[i].Name, FDBType, SyncData[i].Value);

  AbortIfCurrentThreadTaskTerminated;    
end;

function TevDataSyncPacketApplier.CreateQuery(const ASQL: String): IevSQL;
begin
  Result := FOwner.CreateRawSQL(ASQL, FDBType);
end;

initialization
  SetEnvironmentVariable('FIREBIRD', PAnsiChar(AppDir)); // Because we use local FB installation
  GetGDSLibrary.LoadIBLibrary; //Aleksey: this call is not thread safe so we need to do it in main thread

  DBConnectionPool := TevDBConnectionPool.Create('DB Pool Garbage Collector', TevDBConnection);
  DBConnectionPool.MaxPoolSize := 0; // default DB connection pool size is 0 so it will force read this from global settings
  DBConnectionPool.ObjectExpirationTime := 10;  // How long unused connection may stay in pool (minutes)

  DBChangeNotifier := TevDBChangeNotifier.Create;
  DisableMonitoring;

{$IFDEF SQL_MONITOR}
  SQLMonitorLog := TevSQLLog.Create;
{$ENDIF}

finalization
  DBChangeNotifier.Free;
  DBConnectionPool := nil;
  SQLMonitorLog := nil;

  GetGDSLibrary.FreeIBLibrary; // Aleksey: symmetric call to unload dll


end.

