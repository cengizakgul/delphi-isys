// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDBAccessMod;

interface

uses
  Windows, Messages, Forms, Classes, db, StrUtils, isBaseClasses, Math,
  EvCommonInterfaces, EvMainboard, EvDataAccessComponents, EvFirebird,
  EvTypes, sysutils, SDDClasses, IniFiles, isBasicUtils, Types,
  Variants, EvStreamUtils, SUniversalDataReader, isTypes, EvInitApp,
  EvUtils, EvContext, EvConsts, EvBasicUtils, SDataStructure,
  DateUtils, ISBasicClasses, EvExceptions, EvClasses, isThreadManager,
  SDataDictClient, SFieldCodeValues, ISZippingRoutines, EvDataSet, isLogFile,
  SDataDictTemp, EvClientDataSet, SDDStreamClasses, rwCustomDataDictionary,
  isDataSet, rwEngineTypes, EvFieldValueToText, SEncryptionRoutines,
  isStrUtils, sShowDataset, EvDataChangeTriggers
  ,EvAuditHelper
//  ,EvSQLite
  ;

implementation

uses STimeOffUtils;

type
  IevDataChangeManager = interface;

  TevDBAccess = class(TisInterfacedObject, IevDBAccess)
  private
    FCurrentClientNbr: Integer;
    FDBServer: IevDBServer;
    FTransactionRefCount: Integer;
    FSecurityDisableRefCount: Integer;
    FEnableReadTransRefCount: Integer;
    FDBInTrans: TevDBTypes;
    FTransactionName: String;
    FTablesChangedInTransaction: IisStringList;
    FChangedByNbr: Integer;
    FChangingClientROFlag: Boolean;
    FADRContext: Boolean;
    FChangeManager: IevDataChangeManager;
    FFullTTRebuildMode: Boolean;

    procedure DeleteClient(const ClNbr: Integer);
    procedure InternalOpenClient(const AValue: Integer);
    procedure InternalCommit;
    procedure InternalRollback;
    procedure ActionForChangedTables;
    procedure UpdateAccessToNewClients(const AClientNbr: Integer);
    function  ClientExistsInTmpTbls(const AClientNbr: Integer): Boolean;
    procedure DropTmpTblsIntegrity;
    function  CheckParamValue(const AValue: Variant): Variant;
  protected
    procedure DoOnConstruction; override;
    function  CommitTransaction: IisValueMap;
    procedure RollbackTransaction;
    procedure StartTransaction(const ADBTypes: TevDBTypes; const ATransactionName: String = '');
    function  InTransaction: Boolean;
    procedure BeginRebuildAllClients;
    procedure RebuildTemporaryTables(const aClientNbr: Integer; const aAllClientsMode: Boolean; const aTempClientsMode: Boolean = False);
    procedure EndRebuildAllClients;
    function  CreateClientDB (const ClDataset:IevDataset): Integer;
    procedure DeleteClientDB(ClientNumber: Integer);
    function  GetDataSets(const ADataSetParams: TGetDataParams): IisStream;
    procedure RunInTransaction(const AMethodName: String; const AParams: IisListOfValues);
    procedure CleanDeletedClientsFromTemp;
    function GetClientsList(const allList: Boolean = false; const fromTT: Boolean = false): string;
    procedure DisableDBs(const ADBList: IisStringList);
    procedure EnableDBs(const ADBList: IisStringList);
    function  GetDisabledDBs: IisStringList;
    procedure ApplyDataChangePacket(const APacket: IevDataChangePacket);

    // local feature only!!! Proxy never calls it!
    procedure EnableSecurity;
    procedure DisableSecurity;
    procedure SetChangedByNbr(const AValue: Integer = 0);
    function  GetChangedByNbr: Integer;
    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
    procedure EnableReadTransaction;
    procedure DisableReadTransaction;
    procedure LockTableInTransaction(const aTable: String; const aCondition: String = '');
    function  PatchDB(const ADBName: String; const AAppVersion: String; AllowDowngrade: Boolean): Boolean;
    function  CustomScript(const ADBName: String; const AScript: String): Boolean;    
    function  SyncDBAndApp(const ADBName: String): Boolean;
    procedure SyncUDFAndApp();
    procedure ImportDatabase(const ADBPath, ADBPassword: String);
    function  GetDBServerFolderContent(const APath, AUser, APassword: String): IevDataSet;
    function  GetDBFilesList: IevDataSet;
    procedure TidyUpDBFolders;
    procedure ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    procedure UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
    function  FullTTRebuildInProgress: Boolean;
    procedure ChangeFBAdminPassword(const APassword, ANewPassword: String);
    procedure ChangeFBUserPassword(const AAdminPassword, ANewPassword: String);
    function  GetDBServersInfo: IisParamsCollection;
    //

    function  OpenQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean = True): IevDataSet;
    procedure ExecQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues);
    function  ExecStoredProc(const AProcName: String; const AParams: IisListOfValues): IisListOfValues;
    function  GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
    procedure SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
    function  GetNewKeyValue(const ATableName: String; const AIncrement: Integer = 1): Integer;
    procedure SetCurrentClientNbr(const AValue: Integer);
    function  GetCurrentClientNbr: Integer;
    function  GetDBVersion(const ADBType: TevDBType): String;
    function  BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompression: Boolean): IevDualStream;
    procedure RestoreDB(const ADBName: String; const AData: IevDualStream; const ACompressed: Boolean);
    procedure SweepDB(const ADBName: String);
    procedure DropDB(const ADBName: String);
    procedure UndeleteClientDB(const AClientID: Integer);
    function  GetLastTransactionNbr(const ADBType: TevDBType): Integer;
    function  GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
    function  GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
    procedure ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
    function  GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IisStream;
    procedure CheckDBHash(const ADBType: TevDBType; const AData: IisStream; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
    procedure SetADRContext(const AValue: Boolean);
    function  GetADRContext: Boolean;
    function  GetClientROFlag: Char;
    procedure SetClientROFlag(const AValue: Char);
    function  GetFieldValues(const ATable, AField: String; const ANbr: Integer; const AsOfDate: TDateTime; const AEndDate: TDateTime; const AMultiColumn: Boolean): IisListOfValues;
    function  GetFieldVersions(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ASettings: IisListOfValues = nil): IevDataSet;
    procedure UpdateFieldVersion(const ATable: String; const ANbr: Integer; const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TDateTime;
                                 const AFieldsAndValues: IisListOfValues);
    procedure UpdateFieldValue(const ATable, AField: String; const ANbr: Integer; const ABeginDate, AEndDate: TDateTime; const AValue: Variant);
    function  GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
    function  GetVersionFieldAuditChange(const ATable, AField: String;  {const ANbr: Integer; } const ABeginDate: TDateTime): IevDataset;
    function  GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
    function  GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
    function  GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
    function  GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
    function  GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
  end;



  IevFieldValuesBuilder = interface
  ['{DDCAEC46-675C-4AD8-8C0F-4E5622311FEF}']
    function Build(const AMultiColumn: Boolean): IisListOfValues;
  end;

  TevTablePath = array of TddsTable;

  TevFieldValuesBuilder = class(TisInterfacedObject, IevFieldValuesBuilder)
  private
    FTable: TddsTable;
    FField: TddsField;
    FRecordNbr: Integer;
    FAsOfDate: TisDate;
    FEndDate: TisDate;
    FValueConverter: IevFieldValueToTextConverter;
    function CheckChildBranch(AParentTable: TddsTable; AChildTable: TddsTable; APath: TevTablePath): String;
    function GetParentBranchKey(ATable, AExpectedParentTable: TddsTable; APath: TevTablePath): Integer;
    function GetTransitiveFilter(AFK: TddsForeignKey): String;
    procedure ApplyFilter(const AValues: IevDataSet);
  protected
    function Build(const AMultiColumn: Boolean): IisListOfValues;
  public
    constructor Create(const ATable, AField: String; const ARecordNbr: Integer; const AAsOfDate: TisDate; const AEndDate: TisDate); reintroduce;
  end;


  TFieldArray = array of TField;

  TEvLinkedClientDataset = class(TEvBasicClientDataSet)
  private
    FLinkedFields: TFieldArray;
  public
    property  FieldByLinkedIndex: TFieldArray read FLinkedFields;
    procedure InitLinkFields(const AFieldNames: IisStringList); overload;
    procedure InitLinkFields(const ADataSet: TDataSet); overload;
  end;

  IevDatasetRequest = interface(IisInterfacedObject)
  ['{C6734D7F-B95E-4CA7-B555-A9DDD28C0305}']
    function ProviderName: String;
    function Data: IEvQueryResult;
  end;

  IevDatasetRequests = interface(IisCollection)
  ['{D800CAF0-2F95-4F1D-B5BE-4F403AB61CE5}']
    function AddRequest(const AProviderName: String; const AClNbr: Integer; const AExecDsWrapper: IExecDsWrapper): IevDatasetRequest;
  end;


  TevDatasetRequest = class(TisInterfacedObject, IevDatasetRequest)
  private
    FProviderName: String;
    FClNbr: Integer;
    FExecDsWrapper: IExecDsWrapper;
    FData: IEvQueryResult;
    function  DBServer: IevDBServer;
    function  Requests: IevDatasetRequests;
    function  GetTableFieldList(const ATableName: string; const AFieldList: TisCommaDilimitedString): string;
    function  CreateLinkCondition(const AFieldName: string; const AData: IEvQueryResult): string;
    procedure PrepareCustomQuery;
    procedure PrepareTableQuery;
    procedure Execute;
  protected
    function  ProviderName: String;
    function  Data: IEvQueryResult;
  public
  end;


  TevDatasetRequests = class(TisCollection, IevDatasetRequests)
  protected
    FOwner: TevDBAccess;
    function AddRequest(const AProviderName: String; const AClNbr: Integer; const AExecDsWrapper: IExecDsWrapper): IevDatasetRequest;
  public
    constructor Create(const AOwner: TevDBAccess); reintroduce;
  end;


  IevTempTables = interface
  ['{A62C28A5-7F56-45F4-93A8-2E9CCEE3E0A0}']
    function Count: Integer;
    function GetTableName(const AIndex: Integer): String;
    function GetFields(const AIndex: Integer): IisStringList; overload;
    function GetSQLSelect(const AIndex: Integer): String; overload;
    function GetSQLClientSelect(const AIndex: Integer): String; overload;
    function GetSQLInsert(const AIndex: Integer): String; overload;
    function GetSQLUpdate(const AIndex: Integer): String; overload;
    function GetSQLDelete(const AIndex: Integer): String; overload;
    function GetClientTableName(const AIndex: Integer): String; overload;
    function GetFields(const ATable: String): IisStringList; overload;
    function GetSQLSelect(const ATable: String): String; overload;
    function GetSQLClientSelect(const ATable: String): String; overload;
    function GetSQLInsert(const ATable: String): String; overload;
    function GetSQLUpdate(const ATable: String): String; overload;
    function GetSQLDelete(const ATable: String): String; overload;
    function GetClientTableName(const ATable: String): String; overload;
  end;

  TevTempTables = class(TisParamsCollection, IevTempTables)
  protected
    procedure DoOnConstruction; override;

    function GetTableName(const AIndex: Integer): String;
    function GetFields(const AIndex: Integer): IisStringList; overload;
    function GetSQLSelect(const AIndex: Integer): String; overload;
    function GetSQLClientSelect(const AIndex: Integer): String; overload;
    function GetSQLInsert(const AIndex: Integer): String; overload;
    function GetSQLUpdate(const AIndex: Integer): String; overload;
    function GetSQLDelete(const AIndex: Integer): String; overload;
    function GetClientTableName(const AIndex: Integer): String; overload;
    function GetFields(const ATable: String): IisStringList; overload;
    function GetSQLSelect(const ATable: String): String; overload;
    function GetSQLClientSelect(const ATable: String): String; overload;
    function GetSQLInsert(const ATable: String): String; overload;
    function GetSQLUpdate(const ATable: String): String; overload;
    function GetSQLDelete(const ATable: String): String; overload;
    function GetClientTableName(const ATable: String): String; overload;
  end;

  TevTempTablesBuilder = class
  private
    FOwner: TevDBAccess;
    FClientNbr: Integer;
    FCO_STATES: TEvBasicClientDataSet;
    FCO_SUI: TEvBasicClientDataSet;
    FCO_LOCAL_TAX: TEvBasicClientDataSet;
    FChangeCount: Integer;
    FBackwardRefs: IisParamsCollection;
    procedure PrepareLookups;
    procedure PrepareBackwardRefs;
    procedure ProcessTable(const TempTableName: string);
    procedure FlushChanges;
    procedure UpdateBackwardRefs;
  public
    procedure   Rebuild(const AClientNbr: Integer; const aTempClientsMode: Boolean);
    constructor Create(const AOwner: TevDBAccess);
    destructor  Destroy; override;
  end;


  IevDataChangeManager = interface
  ['{A3A7FB36-DA61-4CF7-8BC1-8DF8231BDFFC}']
    procedure   Apply(const APacket: IevDataChangePacket);
    procedure   UpdateFieldVersion(const ATable: String; const ANbr: Integer;
                                   const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TisDate;
                                   const AFieldsAndValues: IisListOfValues);
    procedure   EnsureVerFieldValue(const ATable: String; const ANbr: Integer; const ABeginDate, AEndDate: TisDate;
                                    const AFieldsAndValues: IisListOfValues);
    procedure   UpdateTempTables;
    procedure   PackRecord(const ATable: String; const ANbr: Integer);
    procedure   Clear;
  end;

  TevDataChangeManager = class(TisInterfacedObject, IevDataChangeManager)
  private
    FOwner: TevDBAccess;
    FAsOfDate: TDateTime;
    class function IsFieldHidden(const ATable, AField: string): Boolean;
    procedure RemoveNoAccessFields(const AChangeItem: IevDataChangeItem; const AForInsert: Boolean);
    procedure DoInsert(const AChangeItem: IevDataChangeItem);
    procedure DoUpdate(const AChangeItem: IevDataChangeItem);
    procedure DoDelete(const AChangeItem: IevDataChangeItem);
//    procedure DoAsOfDateDelete(const AChangeItem: IevDataChangeItem);
    procedure SyncTempTable(const AClientTable: String; const AKeys: IisIntegerList);
    procedure DoBeforeOperation(const AChangeItem: IevDataChangeItem);
  public
    procedure   Apply(const APacket: IevDataChangePacket);
    procedure   UpdateFieldVersion(const ATable: String; const ANbr: Integer;
                                   const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TisDate;
                                   const AFieldsAndValues: IisListOfValues);
    procedure   EnsureVerFieldValue(const ATable: String; const ANbr: Integer; const ABeginDate, AEndDate: TisDate;
                                    const AFieldsAndValues: IisListOfValues);
    procedure   UpdateTempTables;
    procedure   PackRecord(const ATable: String; const ANbr: Integer);
    procedure   Clear;
    constructor Create(const AOwner: TevDBAccess); reintroduce;
  end;


  // Audit
  IevAuditData = interface
  ['{0205A41C-408F-41E4-85B7-CAE8D44C7F36}']
    function  GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
    function  GetVersionFieldAuditChange(const ATable, AField: String;  {const ANbr: Integer;} const ABeginDate: TDateTime): IevDataset;
    function  GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
    function  GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
    function  GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
    function  GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
    function  GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
  end;


  TevAuditData = class(TisInterfacedObject, IevAuditData)
  private
    FAllowToResolveUserName: Boolean;
    FSBUsers: IevDataSet;
    FEEUsers: IevDataSet;
    function  GetUserName(const AUserID: Integer): String;
  protected
    procedure AfterConstruction; override;

    function  GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
    function  GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
    function  GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
    function  GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
    function  GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
    function  GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
    function  GetVersionFieldAuditChange(const ATable, AField: String;  {const ANbr: Integer;} const ABeginDate: TDateTime): IevDataset;
  end;


var TempTables: IevTempTables;


function GetDBAccess: IInterface;
begin
  Result := TevDBAccess.Create;
end;

procedure TevDBAccess.DeleteClientDB(ClientNumber: Integer);
var
  bExists: Boolean;
  T: IevTable;
begin
  SetCurrentClientNbr(ClientNumber);
  StartTransaction([dbtBureau, dbtClient, dbtTemporary]);
  try
    bExists := ClientExistsInTmpTbls(ClientNumber);

    T := TevTable.Create('CL', 'CL_NBR');
    T.Open;
    T.Delete;
    T.SaveChanges;
    T := nil;

    DeleteClient(ClientNumber);

    T := TevTable.Create('SB_SEC_CLIENTS', 'SB_SEC_CLIENTS_NBR', 'CL_NBR = ' + InttoStr(ClientNumber));
    T.Open;

    while not T.Eof do
      T.Delete;
    T.SaveChanges;

    CommitTransaction;
  except
    RollbackTransaction;
    raise;
  end;

  if bExists then
    FDBServer.SendBroadcastNotification('DBLISTCHANGED', 'TMP_TBLS');
end;

function TevDBAccess.CreateClientDB (const ClDataset:IEvDataset): Integer;
var
  NewClNbr: Integer;
  Results, Params, Macros: IisListOfValues;
  OldClientNbr: Integer;
  RequiredFields: TddFieldList;
  i, j: Integer;
  sFlds, sVals: String;
  FieldsVals: TevFieldValues;
  v: Variant;
  bError, bPartialClientCreated: Boolean;
begin
  bError := False;
  NewClNbr := 0;
  OldClientNbr := GetCurrentClientNbr;
  bPartialClientCreated:= false;
  try
    DisableSecurity;
    try
      Results := FDBServer.ExecDS('CL_NBR');
      NewClNbr := Results.GetValueByName('NUMBER');

      try
        FDBServer.CreateClient(NewClNbr);
        bPartialClientCreated:= True;

        // Add bogus record in CL table
        // Evo was missing it for 10 years, but this is mandatory action from point of consistency.
        SetCurrentClientNbr(NewClNbr);
        StartTransaction([dbtClient, dbtTemporary]);
        try
          RequiredFields := TCL.GetRequiredFields;
          FieldsVals := FieldCodeValues.FindFields(TCL);

          Params := TisListOfValues.Create;
          for i := Low(RequiredFields) to High(RequiredFields) do
          begin
            j := FieldsVals.FieldIndex(RequiredFields[i]);
            if j <> - 1 then
              v := FieldsVals[j].DefValue
            else
              v := Null;
            Params.AddValue(RequiredFields[i], v);
          end;

          Params.Value['CL_NBR'] := NewClNbr;
          Params.Value['AUTO_SAVE_MINUTES'] := 0;
          Params.AddValue('READ_ONLY', 'N');

          if Assigned(ClDataset) then
          begin
            //for creating client in EvoExchange
            Params.Value['CUSTOM_CLIENT_NUMBER'] := ClDataset.vclDataset.FieldByName('Client Code').AsString;
            Params.Value['NAME'] :=  ClDataset.vclDataset.FieldByName('Client Name').AsString;
            Params.Value['ADDRESS1'] := ClDataset.vclDataset.FieldByName('ADDRESS1').AsString;
            Params.Value['CITY'] := ClDataset.vclDataset.FieldByName('City').AsString;
            Params.Value['ZIP_CODE'] := ClDataset.vclDataset.FieldByName('Zip code').AsString;
            Params.Value['STATE'] := ClDataset.vclDataset.FieldByName('State').AsString;
            Params.Value['START_DATE'] := ClDataset.vclDataset.FieldByName('Start date').AsDateTime;
          end
          else begin
            Params.Value['CUSTOM_CLIENT_NUMBER'] := 'NC' + IntToStr(NewClNbr);
            Params.Value['NAME'] := 'New Client ' + IntToStr(NewClNbr);
            Params.Value['ADDRESS1'] := 'Default Address1';
            Params.Value['CITY'] := 'Default City';
            Params.Value['ZIP_CODE'] := '00000';
            Params.Value['STATE'] := '  ';
            Params.Value['START_DATE'] := Date;
          end;

          sFlds := '';
          sVals := '';
          for i := 0 to Params.Count - 1 do
          begin
            AddStrValue(sFlds, Params[i].Name, ',');
            AddStrValue(sVals, ':' + Params[i].Name, ',');
          end;

          Macros := TisListOfValues.Create;
          Macros.AddValue('TABLENAME', 'CL');
          Macros.AddValue('FIELDLIST', sFlds);
          Macros.AddValue('PARAMLIST', sVals);

          ExecQuery('GenericInsert', Params, Macros);

          Macros.Clear;
          Macros.AddValue('TABLENAME', 'TMP_CL');
          Macros.AddValue('FIELDLIST', 'CL_NBR, CUSTOM_CLIENT_NUMBER, NAME, READ_ONLY');
          Macros.AddValue('PARAMLIST', ':CL_NBR, :CUSTOM_CLIENT_NUMBER, :NAME, :READ_ONLY');

          ExecQuery('GenericInsert', Params, Macros);

          CommitTransaction;
        except
          RollbackTransaction;
          raise;
        end;
      except
        bError := True;
        raise;
      end;
    finally
      SetCurrentClientNbr(OldClientNbr);
      EnableSecurity;
    end;
  except
    if bError and (NewClNbr <> 0) and bPartialClientCreated then
    begin
      try
        FDBServer.DeleteClient(NewClNbr);
      except
        on E: Exception do
          mb_LogFile.AddContextEvent(etError, 'DB Access', 'Cannot drop database', E.Message);
      end
    end;

    raise;
  end;

  UpdateAccessToNewClients(NewClNbr);
  Snooze(1000); // In order to allow propogation of security change notification

  Result := NewClNbr;
end;


procedure TevDBAccess.DeleteClient(const ClNbr: Integer);
var
  Q: IevQuery;
begin
  Q := TevQuery.Create(TempTables.GetSQLDelete('TMP_CL'), False);
  Q.Params.AddValue('cl_nbr', ClNbr);
  Q.Params.AddValue('nbr', ClNbr);
  Q.Execute;
end;

procedure TevDBAccess.RebuildTemporaryTables(const aClientNbr: Integer; const aAllClientsMode: Boolean; const aTempClientsMode: Boolean = False);
var
  Builder: TevTempTablesBuilder;
  FlagInfo: IevGlobalFlagInfo;
begin
  if not aAllClientsMode and not GetADRContext then
    if not mb_GlobalFlagsManager.TryLock(GF_REBUILD_TT_OPERATION, IntToStr(aClientNbr), FlagInfo) then
      raise EevException.Create('Rebuild temp tables for client database #' + IntToStr(aClientNbr) + ' already is processing by user ' + FlagInfo.UserName);

  FFullTTRebuildMode := aAllClientsMode;
  try
    Builder := TevTempTablesBuilder.Create(Self);
    try
      ctx_StartWait('Prepare to rebuild temp tables');
      try
        Builder.Rebuild(aClientNbr, aTempClientsMode);
      finally
        ctx_EndWait;
      end;
    finally
      Builder.Free;
    end;
  finally
    FFullTTRebuildMode := False;
    if not aAllClientsMode and not GetADRContext then
      mb_GlobalFlagsManager.ForcedUnlock(GF_REBUILD_TT_OPERATION, IntToStr(aClientNbr));
  end;
end;

function TevDBAccess.CommitTransaction: IisValueMap;
begin
  CheckCondition(InTransaction, 'Transaction is not active');

  if FTransactionRefCount = 1 then
    InternalCommit;

  Dec(FTransactionRefCount);

  Result := nil;
end;

procedure TevDBAccess.RollbackTransaction;
begin
  CheckCondition(InTransaction, 'Transaction is not active');

  InternalRollback;
  Dec(FTransactionRefCount);
end;

procedure TevDBAccess.StartTransaction(const ADBTypes: TevDBTypes; const ATransactionName: String = '');
var
  FlagInfo: IevGlobalFlagInfo;
  ClientROFlag: char;
begin
  if not InTransaction then
  begin
    if not GetADRContext and (dbtClient in ADBTypes) then
    begin
      if GetCurrentClientNbr = 0 then
         raise EevException.Create('Can''t save data to CL_BASE database!');

      FlagInfo := mb_GlobalFlagsManager.GetLockedFlag(GF_REBUILD_TT_OPERATION, IntToStr(GetCurrentClientNbr));

      if Assigned(FlagInfo) and (FlagInfo.ContextID <> Context.GetID) then
        raise EevException.Create('Write access to client denied (' + IntToStr(GetCurrentClientNbr) + '). Rebuild temp tables is in progress.');

      if not FChangingClientROFlag then   // check if Client is in maintenance mode
      begin
        ClientROFlag := GetClientROFlag;
        if ClientROFlag  = READ_ONLY_MAINTENANCE then
          raise EevException.Create('Write access to client denied (' + IntToStr(GetCurrentClientNbr) + '). Client in maintenance.');
        if (ClientROFlag  = READ_ONLY_READ_ONLY) and
           (ctx_AccountRights.Functions.GetState('UNLOCK_CL_READ_ONLY_MODE') <> stEnabled) then
          raise EevException.Create('Write access to client denied (' + IntToStr(GetCurrentClientNbr) + '). Client is Read Only.');
      end;
    end;

    FDBInTrans := ADBTypes;
    FDBServer.StartTransaction(FDBInTrans);

    if ATransactionName <> '' then
      LockResource(GF_DB_TRANSACTION, ATransactionName, '', rlTryUntilGet);
    FTransactionName := ATransactionName;

    FTablesChangedInTransaction.Clear;
    FChangeManager.Clear;
  end
  else
  begin
    CheckCondition((FDBInTrans * ADBTypes) = ADBTypes, 'Cannot append database into active transaction');
    CheckCondition((ATransactionName = '') or AnsiSameText(ATransactionName, FTransactionName), 'Transaction name cannot be changed');
  end;

  Inc(FTransactionRefCount);
end;

function TevDBAccess.GetClientsList(const allList: Boolean = false; const fromTT: Boolean = false): string;
var
  DS: IevDataSet;
  aList: IisStringList;

  function GetTmpClients:IisStringList;
  var
    Q: IevQuery;
  begin
    Q := TevQuery.Create('SELECT CL_NBR FROM TMP_CL');
    Q.Execute;
    Result:= Q.Result.GetColumnValues('CL_NBR');
  end;

begin
  try
    if allList then
       ctx_DBAccess.DisableSecurity;
    if fromTT then
     aList:= GetTmpClients
    else
    begin
      DS := FDBServer.OpenClientListDS;
      aList := TisStringList.Create;
      DS.First;
      while not DS.Eof do
      begin
        aList.Add(DS.FieldByName('CL_NBR').AsString);
        DS.Next;
      end;
    end;
  finally
    Result := aList.Text;
    if allList then
       ctx_DBAccess.EnableSecurity;
  end;
end;



{ TEvLinkedClientDataset }

procedure TEvLinkedClientDataset.InitLinkFields(const AFieldNames: IisStringList);
var
  i: Integer;
begin
  SetLength(FLinkedFields, AFieldNames.Count);
  for i := 0 to AFieldNames.Count - 1 do
    FLinkedFields[i] := FindField(AFieldNames[i]);
end;

procedure TevDBAccess.CleanDeletedClientsFromTemp;
var
  DS1: TEvBasicClientDataSet;
  DS: IevDataset;
  aMacros: IisListOfValues;
begin
  aMacros := TisListOfValues.Create;
  DS := FDBServer.OpenClientListDS;
  aMacros.AddValue('TABLENAME', 'TMP_CL');
  aMacros.AddValue('COLUMNS', 'CL_NBR');
  DS1 := FDBServer.OpenDS('GenericSelect', nil, aMacros);
  try
    DS.IndexFieldNames := 'CL_NBR';
    DS1.IndexFieldNames := 'CL_NBR';
    StartTransaction([dbtTemporary]);
    try
      DS1.First;
      while not DS1.Eof do
      begin
        if not DS.FindKey([DS1['CL_NBR']]) then
          DeleteClient(DS1['CL_NBR']);
        DS1.Next;
      end;
      CommitTransaction;

      FDBServer.SendBroadcastNotification('DBLISTCHANGED', 'TMP_TBLS');
    except
      RollbackTransaction;
      raise;
    end;
  finally
    DS1.Free;
  end;
end;


procedure TevDBAccess.InternalCommit;
begin
  try
    try
      FChangeManager.UpdateTempTables;
      FDBServer.Commit;
    except
      InternalRollback;
      raise;
    end;
  finally
    FDBInTrans := [];

    try
      if FTransactionName <> '' then
        UnLockResource(GF_DB_TRANSACTION, FTransactionName, rlTryUntilGet);
    finally
      FTransactionName := '';
    end;  
  end;
    
  ActionForChangedTables;
end;

procedure TevDBAccess.InternalRollback;
begin
  FChangeManager.Clear;
  FDBServer.Rollback;
  FDBInTrans := [];

  try
    if FTransactionName <> '' then
      UnLockResource(GF_DB_TRANSACTION, FTransactionName, rlTryUntilGet);
  finally
    FTransactionName := '';
  end;

  FTablesChangedInTransaction.Clear;
end;

procedure TevDBAccess.DoOnConstruction;
begin
  inherited;
  FDBServer := GetFirebirdDBServer;
  FDBServer.DataSecurityActive := True;
  FTablesChangedInTransaction := TisStringList.CreateUnique;
  FChangeManager := TevDataChangeManager.Create(Self);
end;

function TevDBAccess.OpenQuery(const AQueryName: String; const AParams: IisListOfValues; const AMacros: IisListOfValues; const ALoadBlobs: Boolean): IevDataSet;
begin
  Result := FDBServer.ExecQuery(AQueryName, AParams, AMacros, ALoadBlobs);
end;

function TevDBAccess.GetDataSets(const ADataSetParams: TGetDataParams): IisStream;
var
  sProviderName: string;
  iClNbr: Integer;
  ExecDsWrapper: IExecDSWrapper;
  Res: TGetDataResults;
  i: Integer;
  RequestResult: IEvQueryResult;
  Requests: IevDatasetRequests;
begin
  // restoring streamed data back to structures and run requests one by one
  Requests := TevDatasetRequests.Create(Self);
  ADataSetParams.GotoFirst;
  while not ADataSetParams.EndOfParams do
  begin
    ADataSetParams.Read(sProviderName, iClNbr, ExecDsWrapper);
    sProviderName := UpperCase(sProviderName);
    CheckCondition(sProviderName <> '', 'Provider name is not set');
    try
      Requests.AddRequest(sProviderName, iClNbr, ExecDsWrapper);
    except
      raise;
    end;
  end;

  Res := TGetDataResults.Create;
  try
    for i := 0 to Requests.ChildCount - 1 do
    begin
      RequestResult := (Requests.Children[i] as IevDatasetRequest).Data;
      Res.Write(RequestResult);
      RequestResult := nil;
    end;

    Res.FinalizeWrite;

    Result := Res.MemStream;
    Result.Position := 0;
  finally
    Res.Free;
  end;
end;

procedure TEvLinkedClientDataset.InitLinkFields(const ADataSet: TDataSet);
var
  i: Integer;
begin
  SetLength(FLinkedFields, ADataSet.FieldCount);
  for i := 0 to ADataSet.FieldCount - 1 do
    FLinkedFields[i] := FindField(ADataSet.Fields[i].FieldName);
end;

{ TevDatasetRequests }

constructor TevDatasetRequests.Create(const AOwner: TevDBAccess);
begin
  inherited Create;
  FOwner := AOwner;
end;


function TevDatasetRequests.AddRequest(const AProviderName: String; const AClNbr: Integer; const AExecDsWrapper: IExecDsWrapper): IevDatasetRequest;
begin
  Result := TevDatasetRequest.Create(Self, False);
  TevDatasetRequest(Result.GetImplementation).FProviderName := AProviderName;
  TevDatasetRequest(Result.GetImplementation).FClNbr := AClNbr;
  TevDatasetRequest(Result.GetImplementation).FExecDsWrapper := AExecDsWrapper;
end;


{ TevDatasetRequest }

function TevDatasetRequest.CreateLinkCondition(const AFieldName: string; const AData: IEvQueryResult): string;
var
  i, j: Integer;
  iKey, iMaxKey, iMinKey: Integer;
  s: string;
  ArrayOfInteger: TArrayOfInteger;
begin
  ArrayOfInteger := nil; // to get rid of warning
  Result := AData.Tags.Values['CONDITION_'+ AFieldName];
  if Result = '' then
  begin
    iMaxKey := Low(iMaxKey);
    iMinKey := High(iMinKey);
    s := '';
    ArrayOfInteger := AData.FieldValues(AFieldName);
    j := High(ArrayOfInteger);
    for i := 0 to j do
    begin
      iKey := ArrayOfInteger[i];
      s := s+ ','+ IntToStr(iKey);
      iMaxKey := Max(iKey, iMaxKey);
      iMinKey := Min(iKey, iMinKey);
      if (((i mod 1400) = 0) and (i <> 0)) or (i = j) then
        if s <> '' then
        begin
          Delete(s, 1, 1);
          if Result = '' then
            Result := AFieldName+ ' in ('+ s+ ') '
          else
            Result := Result+ ' or '+ AFieldName+ ' in ('+ s+ ') ';
          s := '';
        end;
    end;
    if Result = '' then
      Result := AlwaysFalseCond
    else
    if Length(Result) > 20*1024 then // to stay below 32K limit
      Result := '('+ AFieldName+ ' between '+ IntToStr(iMinKey)+ ' and '+ IntToStr(iMaxKey)+ ')'
    else
      Result := '('+ Result+ ')';
    AData.Tags.Values['CONDITION_'+ AFieldName] := Result;
  end;
end;

function TevDatasetRequest.Data: IEvQueryResult;
begin
  if not Assigned(FData) then
    Execute;
  Result := FData;
end;


function TevDatasetRequest.DBServer: IevDBServer;
begin
  Result := TevDatasetRequests(GetOwner.GetImplementation).FOwner.FDBServer;
end;


procedure TevDatasetRequest.Execute;
begin
  PrepareCustomQuery;
  if not Assigned(FData) then
    PrepareTableQuery;
end;

function TevDatasetRequest.GetTableFieldList(const ATableName: string;
  const AFieldList: TisCommaDilimitedString): string;
var
  TblClass: TddTableClass;
  i: Integer;
  Flds: IisStringList;
  s: String;
begin
  Result := '';

  TblClass := GetddTableClassByName(ATableName);
  CheckCondition(Assigned(TblClass), 'Table class "' + ATableName + '" not found');

  if AFieldList = '' then
    Flds := TblClass.GetFieldNameList
  else
  begin
    Flds := TisStringList.Create;
    Flds.CommaText := AnsiUpperCase(AFieldList);

    s := TblClass.GetTableName + '_NBR';
    if Flds.IndexOf(s) = -1 then
      Flds.Add(s);
  end;

  for i := 0 to Flds.Count - 1 do
    AddStrValue(Result, Trim(Flds[i]), ', ');
end;

procedure TevDatasetRequest.PrepareCustomQuery;
var
  Res: IisListOfValues;
  sQueryText: String;
  sTableName: String;
  bExecuteStatement: Boolean;
begin
  sTableName := FProviderName;
  GetPrevStrValue(sTableName, '_PROV');

  if (sTableName <> 'SY_CUSTOM') and (sTableName <> 'SB_CUSTOM') and
     (sTableName <> 'TEMP_CUSTOM') and (sTableName <> 'CL_CUSTOM') then
    exit;

  sQueryText:= FExecDSWrapper.QueryText;

  bExecuteStatement := sQueryText[1] = ';';

  if bExecuteStatement then
  begin
    Delete(sQueryText, 1, 1);
    if not FExecDsWrapper.Params.ValueExists('USER_ID') then
      FExecDsWrapper.Params.AddValue('USER_ID', TevDatasetRequests(GetOwner.GetImplementation).FOwner.GetChangedByNbr);
  end
  else if (Pos(' ', sQueryText) = 0) and (Pos(';', sQueryText) > 0) then
  begin
    sTableName := GetNextStrValue(sQueryText, ';');
    FProviderName := sTableName + '_PROV';
  end;

  if bExecuteStatement then
  begin
    Res := DBServer.ExecDS(sQueryText, FExecDsWrapper.Params, FExecDsWrapper.Macros);
    FData := TEvQueryResult.Create(sfExecQuery, Res.AsStream, nil);
  end
  else
    FData := DBServer.OpenDSEx(sQueryText, FExecDsWrapper.Params, FExecDsWrapper.Macros, '', FExecDsWrapper.BlobsLazyLoad);
end;

procedure TevDatasetRequest.PrepareTableQuery;
var
  sLoadValuesFieldList, sSelect, sFieldList,
  sMacroStr, sNumber, sUplinkProviderName, sUplinkFieldName: String;
  i: Integer;
  UplinkData: IEvQueryResult;
  sTableName: String;
  sQueryText: String;
  AsOfNowRequest: Boolean;
begin
  AsOfNowRequest := FExecDsWrapper.Params.TryGetValue('StatusDate', 0) = 0;

  sSelect := GetNamedValue(FExecDsWrapper.QueryText, 'SELECT', ',');

  if StartsWith(sSelect, 'SELECT ') then
  begin
    sQueryText := sSelect;
    sLoadValuesFieldList := '';
  end

  else
  begin
    sTableName := FProviderName;
    GetPrevStrValue(sTableName, '_PROV');

    sFieldList := GetNamedValue(FExecDsWrapper.QueryText, 'FIELDLIST', ',');

    FExecDsWrapper.Macros.AddValue('Columns', GetTableFieldList(sTableName, sFieldList));
    FExecDsWrapper.Macros.AddValue('TableName', sTableName);
    FExecDsWrapper.Macros.AddValue('NBRField', sTableName);

    sUplinkProviderName := GetNamedValue(FExecDsWrapper.QueryText, 'UPPROVIDER', ',');
    sUplinkFieldName := GetNamedValue(FExecDsWrapper.QueryText, 'UPFIELD', ',');

    sNumber := GetNamedValue(FExecDsWrapper.QueryText, 'NO', ',');

    if (sUplinkProviderName <> '') and (sUplinkFieldName <> '') then
    begin
      UplinkData := nil;
      for i := 0 to Requests.ChildCount - 1 do
        if AnsiSameText((Requests.Children[i] as IevDatasetRequest).ProviderName, sUplinkProviderName) then
        begin
          UplinkData := (Requests.Children[i] as IevDatasetRequest).Data;
          Break;
        end;

      CheckCondition(Assigned(UplinkData), 'Missing uplink provider "'+ sUplinkProviderName+ '" for "'+ sTableName + '"');

      if sSelect = '' then
        sSelect := CreateLinkCondition(sUplinkFieldName, UplinkData)
      else
        sSelect := '('+ sSelect+ ') and '+ CreateLinkCondition(sUplinkFieldName, UplinkData);
    end;


    if sSelect = '1=1' then
      sSelect := '';

    if (sSelect = '') and (sNumber = '') then
      sMacroStr := ''
    else
    begin
      if sNumber <> '' then
      begin
        sMacroStr := '#NBRField = :RecordNbr';
        FExecDsWrapper.Params.AddValue('RecordNbr', StrToInt(sNumber));
      end
      else
        sMacroStr := '';

      if sSelect <> '' then
      begin
        if sMacroStr <> '' then
          sMacroStr := sMacroStr + ' and ';
        sMacroStr := sMacroStr + sSelect;
      end;

      FExecDsWrapper.Macros.AddValue('Condition', '('+ sMacroStr+ ')');
    end;

    if ((GetDBTypeByTableName(sTableName) = dbtTemporary) or not GetddTableClassByName(sTableName).GetIsHistorical) and (sMacroStr = '') then
      sQueryText := 'GenericSelect'
    else if ((GetDBTypeByTableName(sTableName) = dbtTemporary) or not GetddTableClassByName(sTableName).GetIsHistorical) then
      sQueryText := 'GenericSelectWithCondition'
    else if GetddTableClassByName(sTableName).GetIsCurrent and AsOfNowRequest and (sMacroStr = '') then
      sQueryText := 'GenericSelectCurrent'
    else if GetddTableClassByName(sTableName).GetIsCurrent and AsOfNowRequest then
      sQueryText := 'GenericSelectCurrentWithCondition'
    else if sMacroStr = '' then
      sQueryText := 'SelectHist'
    else
      sQueryText := 'SelectHistWithCondition';

    FExecDsWrapper.Macros.AddValue('StatusDate', ':StatusDate');
    sLoadValuesFieldList := GetNamedValue(FExecDsWrapper.QueryText, 'LOADVALS', ',');
  end;

  if AsOfNowRequest then
    FExecDsWrapper.Params.AddValue('StatusDate', SysDate);

  FData := DBServer.OpenDSEx(sQueryText, FExecDsWrapper.Params, FExecDsWrapper.Macros, sLoadValuesFieldList, FExecDsWrapper.BlobsLazyLoad);
end;

function TevDatasetRequest.ProviderName: String;
begin
  Result := FProviderName;
end;

function TevDatasetRequest.Requests: IevDatasetRequests;
begin
  Result := GetOwner as IevDatasetRequests;
end;

function TevDBAccess.GetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AIncrement: Integer): Integer;
begin
  Result := FDBServer.GetGeneratorValue(AGeneratorName, ADBType, AIncrement);
end;

procedure TevDBAccess.SetGeneratorValue(const AGeneratorName: String; const ADBType: TevDBType; const AValue: Integer);
begin
  FDBServer.SetGeneratorValue(AGeneratorName, ADBType, AValue);
end;

function TevDBAccess.GetNewKeyValue(const ATableName: String; const AIncrement: Integer = 1): Integer;
begin
  Result := FDBServer.GetNewKeyValue(AtableName, AIncrement);
end;

function TevDBAccess.GetCurrentClientNbr: Integer;
begin
  Result := FCurrentClientNbr;
end;

procedure TevDBAccess.SetCurrentClientNbr(const AValue: Integer);
begin
  if FCurrentClientNbr <> AValue then
  begin
    if (AValue <> 0) and FDBServer.DataSecurityActive then
      ErrorIf(ctx_AccountRights.Clients.GetState(IntToStr(AValue)) <> stEnabled,
        'Access to client denied (' + IntToStr(AValue) + ')', ENoRightsToClient, IDH_SecurityViolation);

    InternalOpenClient(AValue)
  end;
end;

function TevDBAccess.InTransaction: Boolean;
begin
  Result := FTransactionRefCount > 0;
end;

procedure TevDBAccess.ActionForChangedTables;
begin
  try
    if FTablesChangedInTransaction.IndexOf('SB_TASK') <> -1 then
      Mainboard.GlobalCallbacks.NotifyTaskSchedulerChange('');

    if (FTablesChangedInTransaction.IndexOf('SB_SEC_CLIENTS') <> -1) or
       (FTablesChangedInTransaction.IndexOf('SB_SEC_GROUP_MEMBERS') <> -1) or
       (FTablesChangedInTransaction.IndexOf('SB_SEC_RIGHTS') <> -1) or
       (FTablesChangedInTransaction.IndexOf('SB_SEC_ROW_FILTERS') <> -1) or
       (FTablesChangedInTransaction.IndexOf('SB') <> -1) then
      Mainboard.GlobalCallbacks.NotifySecurityChange('');

  finally
    FTablesChangedInTransaction.Clear;
  end;
end;

function TevDBAccess.GetDBVersion(const ADBType: TevDBType): String;
begin
  Result := FDBServer.GetDBVersion(ADBType);
end;

procedure TevDBAccess.InternalOpenClient(const AValue: Integer);
begin
  FDBServer.OpenClient(AValue);
  FCurrentClientNbr := AValue;
end;


function TevDBAccess.ExecStoredProc(const AProcName: String;
  const AParams: IisListOfValues): IisListOfValues;
var
  sPar: String;
  i: Integer;
  Macros: IisListOfValues;
begin
  sPar := '';
  for i := 0 to AParams.Count - 1 do
    AddStrValue(sPar, ':' + AParams[i].Name, ',');
  if sPar <> '' then
    sPar := '(' + sPar + ')';

  Macros := TisListOfValues.Create;
  Macros.AddValue('PROCNAME', AProcName);
  Macros.AddValue('INPUT', sPar);

  Result := FDBServer.ExecDS('ExecProc', AParams, Macros);
end;

procedure TevDBAccess.RunInTransaction(const AMethodName: String; const AParams: IisListOfValues);
var
  s: String;
begin
  if AnsiSameText(AMethodName, 'VoidPrTimeOff') then
    VoidPrTimeOff(AParams.Value['PrNbr'] , AParams.Value['AccrualDate'], AParams.Value['VoidingPrNbr'])

  else if AnsiSameText(AMethodName, 'SetPayrollLock') then
    ctx_PayrollProcessing.SetPayrollLock(AParams.Value['PrNbr'], AParams.Value['Unlocked'])

  else if StartsWith(AMethodName, 'StoredProc;') then
  begin
    s := AMethodName;
    Delete(s, 1, 11);
    ExecStoredProc(s, AParams);
  end

  else if StartsWith(AMethodName, 'UpdateFieldVersion') then
  begin
    UpdateFieldVersion(
      AParams.Value['ATable'],
      AParams.Value['ANbr'],
      AParams.Value['AOldBeginDate'],
      AParams.Value['AOldEndDate'],
      AParams.Value['ANewBeginDate'],
      AParams.Value['ANewEndDate'],
      IInterface(AParams.Value['AFieldsAndValues']) as IisListOfValues);
  end

  else if StartsWith(AMethodName, 'UpdateFieldValue') then
  begin
    UpdateFieldValue(
      AParams.Value['ATable'],
      AParams.Value['AField'],
      AParams.Value['ANbr'],
      AParams.Value['ABeginDate'],
      AParams.Value['AEndDate'],
      AParams.Value['AValue']);
  end

  else
    Assert(False, 'Unknown method ' + AMethodName);
end;

procedure TevDBAccess.ExecQuery(const AQueryName: String; const AParams, AMacros: IisListOfValues);
begin
  OpenQuery(AQueryName, AParams, AMacros);
end;

function TevDBAccess.GetDisabledDBs: IisStringList;
begin
  Result := FDBServer.GetDisabledDBs;
end;

function TevDBAccess.BackupDB(const ADBName: String; const AMetadataOnly: Boolean; const AScramble, ACompression: Boolean): IevDualStream;
var
  s: String;
  iPreviousClNbr: Integer;
begin
  if FDBServer.DataSecurityActive then
    CheckCondition(IsADRUser(Context.UserAccount.User), 'DB backup is allowed only for ADR');

  if not StartsWith(ADBName, DB_Cl_Base) and StartsWith(ADBName, DB_Cl) then
  begin
    s := AnsiUpperCase(ADBName);
    GetNextStrValue(s, 'CL_');
    s := GetNextStrValue(s, '.');
    iPreviousClNbr := FCurrentClientNbr;
    try
      SetCurrentClientNbr(StrToInt(s));
    finally
      SetCurrentClientNbr(iPreviousClNbr);
    end;
  end;

  Result := FDBServer.BackupDB(ADBName, AMetadataOnly, AScramble, ACompression);
end;

procedure TevDBAccess.RestoreDB(const ADBName: String; const AData: IevDualStream; const ACompressed: Boolean);
var
  s: String;
begin
  if FDBServer.DataSecurityActive then
    CheckCondition(IsADRUser(Context.UserAccount.User), 'DB restore is allowed only for ADR');

  if not AnsiSameText(ADBName, 'CL_BASE') and StartsWith(ADBName, 'CL_') then
  begin
    s := AnsiUpperCase(ADBName);
    GetNextStrValue(s, 'CL_');
    if FDBServer.DataSecurityActive then
      ErrorIf(ctx_AccountRights.Clients.GetState(s) <> stEnabled,
        'Access to client denied (' + s + ')', ENoRightsToClient, IDH_SecurityViolation);
  end;

  FDBServer.RestoreDB(ADBName, AData, ACompressed);
end;

function TevDBAccess.GetLastTransactionNbr(const ADBType: TevDBType): Integer;
begin
  Result := FDBServer.GetLastTransactionNbr(ADBType);
end;

procedure TevDBAccess.DisableSecurity;
begin
  if FSecurityDisableRefCount = 0 then
    FDBServer.DataSecurityActive := False;
  Inc(FSecurityDisableRefCount);
end;

procedure TevDBAccess.EnableSecurity;
begin
  Dec(FSecurityDisableRefCount);
  Assert(FSecurityDisableRefCount >= 0);
  if FSecurityDisableRefCount = 0 then
    FDBServer.DataSecurityActive := True;
end;

procedure TevDBAccess.SetChangedByNbr(const AValue: Integer = 0);
begin
  FChangedByNbr := AValue;
end;

function TevDBAccess.GetChangedByNbr: Integer;
begin
  if FChangedByNbr > 0 then
    Result := FChangedByNbr
  else
    Result := Context.UserAccount.InternalNbr;
end;

procedure TevDBAccess.UpdateAccessToNewClients(const AClientNbr: Integer);
var
  Q: IevQuery;
  s: String;
begin
  StartTransaction([dbtBureau]);
  try
    s := '';
    Q := TevQuery.Create('SecurityUserGroups');
    Q.Params.AddValue('SB_USER_NBR', Context.UserAccount.InternalNbr);
    DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Close;
    while not Q.Result.Eof do
    begin
      if StartsWith(Q.Result.FieldByName('Name').AsString, 'Access to new clients') then
      begin
        DM_SERVICE_BUREAU.SB_SEC_CLIENTS.DataRequired(Format('SB_SEC_GROUPS_NBR = %d AND CL_NBR = %d', [Q.Result.FieldByName('SB_SEC_GROUPS_NBR').AsInteger, AClientNbr]));

        if DM_SERVICE_BUREAU.SB_SEC_CLIENTS.RecordCount = 0 then
        begin
          DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Append;
          DM_SERVICE_BUREAU.SB_SEC_CLIENTS.SB_SEC_GROUPS_NBR.AsInteger := Q.Result.FieldByName('SB_SEC_GROUPS_NBR').AsInteger;
          DM_SERVICE_BUREAU.SB_SEC_CLIENTS.CL_NBR.AsInteger := AClientNbr;
          DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Post;
          ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_SEC_CLIENTS]);
          DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Close;
        end;
      end;
      Q.Result.Next;
    end;
    CommitTransaction;
  except
    RollbackTransaction;
    raise;
  end;

end;

function TevDBAccess.ClientExistsInTmpTbls(const AClientNbr: Integer): Boolean;
var
  Q: IevQuery;
begin
    Q := TevQuery.Create('SELECT CL_NBR FROM TMP_CL WHERE CL_NBR = ' + IntToStr(AClientNbr));
    Result := Q.Result.FieldByName('CL_NBR').AsInteger > 0;
end;

{ TevTempTables }

procedure TevTempTables.DoOnConstruction;

  function BuildWhere(const ATable: String): String;
  begin
    Result := 'WHERE cl_nbr = :cl_nbr and ' + GetClientTableName(ATable) + '_nbr = :nbr';
  end;

var
  i, j: Integer;
  Tbls: IisStringList;
  DetInfo: IisListOfValues;
  FieldList: IisStringList;
  flds, fld, vals, sql: String;
begin
  inherited;

  Tbls := TisStringList.Create;
  Tbls.SetCommaText(
    'TMP_CL, ' +
    'TMP_CO, ' +
    'TMP_CL_CO_CONSOLIDATION, ' +
    'TMP_PR, ' +
    'TMP_PR_SCHEDULED_EVENT, ' +
    'TMP_CL_PERSON, ' +
    'TMP_EE, ' +
    'TMP_CO_STATES, ' +
    'TMP_CO_LOCAL_TAX, ' +
    'TMP_CO_PR_ACH, ' +
    'TMP_CO_MANUAL_ACH, ' +
    'TMP_CO_TAX_DEPOSITS, ' +
    'TMP_PR_MISCELLANEOUS_CHECKS, ' +
    'TMP_CO_TAX_PAYMENT_ACH, ' +
    'TMP_CO_BANK_ACCOUNT_REGISTER, ' +
    'TMP_CO_TAX_RETURN_QUEUE, ' +
    'TMP_CO_FED_TAX_LIABILITIES, ' +
    'TMP_CO_LOCAL_TAX_LIABILITIES, ' +
    'TMP_CO_STATE_TAX_LIABILITIES, ' +
    'TMP_CO_SUI_LIABILITIES, ' +
    'TMP_CO_REPORTS');


  for i := 0 to Tbls.Count - 1 do
  begin
    DetInfo := AddParams(Tbls[i]);

    FieldList := GetddTableClassByName(Tbls[i]).GetFieldNameList;
    DetInfo.AddValue('Fields', FieldList);

    sql := Format('SELECT %s FROM %s WHERE cl_nbr = :cl_nbr', [FieldList.CommaText, Tbls[i]]);
    DetInfo.AddValue('SQL.Select', sql);

    flds := '';
    for j := 0 to FieldList.Count - 1 do
      if AnsiSameText(Tbls[i], 'TMP_CL') or not AnsiSameText(FieldList[j], 'CL_NBR') then
      begin
        if AnsiSameText(Tbls[i], 'TMP_CO_STATE_TAX_LIABILITIES') and AnsiSameText(FieldList[j], 'STATE') then
          fld := '(SELECT t2.state FROM co_states t2 WHERE {AsOfNow<t2>} AND t2.co_states_nbr = t.co_states_nbr) state'
        else if AnsiSameText(Tbls[i], 'TMP_CO_LOCAL_TAX_LIABILITIES') and AnsiSameText(FieldList[j], 'SY_LOCALS_NBR') then
          fld := '(SELECT t2.sy_locals_nbr FROM co_local_tax t2 WHERE {AsOfNow<t2>} AND t2.co_local_tax_nbr = t.co_local_tax_nbr) sy_locals_nbr'
        else if AnsiSameText(Tbls[i], 'TMP_CO_SUI_LIABILITIES') and AnsiSameText(FieldList[j], 'SY_SUI_NBR') then
          fld := '(SELECT t2.sy_sui_nbr FROM co_sui t2 WHERE {AsOfNow<t2>} AND t2.co_sui_nbr = t.co_sui_nbr) sy_sui_nbr'
        else if AnsiSameText(Tbls[i], 'TMP_CO_BANK_ACCOUNT_REGISTER') and AnsiSameText(FieldList[j], 'NOTES_POPULATED') then
          fld := '(CASE getblobsize(notes) WHEN -1 THEN ''N'' WHEN 0 THEN ''N'' ELSE ''Y'' END) notes_populated'
        else
          fld := FieldList[j];

        AddStrValue(flds, fld, ',');
      end;

    sql := Format('SELECT %s FROM %s t WHERE {AsOfNow<t>}', [flds, GetClientTableName(Tbls[i])]);
    DetInfo.AddValue('SQL.SelectClient', sql);

    flds := '';
    for j := 0 to FieldList.Count - 1 do
      AddStrValue(flds, FieldList[j], ',');
    vals := ':' + StringReplace(flds, ',', ',:', [rfReplaceAll]);
    sql := Format('INSERT INTO %s (%s) VALUES(%s)', [Tbls[i], flds, vals]);
    DetInfo.AddValue('SQL.Insert', sql);

    flds := '';
    for j := 0 to FieldList.Count - 1 do
      if not AnsiSameText(FieldList[j], 'CL_NBR') and not AnsiSameText(FieldList[j], Tbls[i] + '_NBR') then
        AddStrValue(flds, FieldList[j] + '=:' + FieldList[j], ',');
    sql := Format('UPDATE %s SET %s ' + BuildWhere(Tbls[i]), [Tbls[i], flds]);
    DetInfo.AddValue('SQL.Update', sql);

    sql := Format('DELETE FROM %s ' + BuildWhere(Tbls[i]), [Tbls[i]]);
    DetInfo.AddValue('SQL.Delete', sql);
  end;
end;

function TevTempTables.GetClientTableName(const ATable: String): String;
begin
  Result := Copy(ATable, 5, Length(ATable));
end;

function TevTempTables.GetClientTableName(const AIndex: Integer): String;
begin
  Result := GetClientTableName(GetTableName(AIndex));
end;

function TevTempTables.GetFields(const ATable: String): IisStringList;
var
  i: Integer;
begin
  i := IndexOf(ATable);
  if i <> -1 then
    Result := GetFields(i)
  else
    Result := nil;
end;

function TevTempTables.GetFields(const AIndex: Integer): IisStringList;
begin
  Result := IInterface(GetParams(AIndex).Value['Fields']) as IisStringList;
end;

function TevTempTables.GetSQLClientSelect(const ATable: String): String;
var
  i: Integer;
begin
  i := IndexOf(ATable);
  if i <> -1 then
    Result := GetSQLClientSelect(i)
  else
    Result := '';
end;

function TevTempTables.GetSQLClientSelect(const AIndex: Integer): String;
begin
  Result := GetParams(AIndex).Value['SQL.SelectClient'];
end;

function TevTempTables.GetSQLDelete(const ATable: String): String;
var
  i: Integer;
begin
  i := IndexOf(ATable);
  if i <> -1 then
    Result := GetSQLDelete(i)
  else
    Result := '';
end;

function TevTempTables.GetSQLDelete(const AIndex: Integer): String;
begin
  Result := GetParams(AIndex).Value['SQL.Delete'];
end;

function TevTempTables.GetSQLInsert(const ATable: String): String;
var
  i: Integer;
begin
  i := IndexOf(ATable);
  if i <> -1 then
    Result := GetSQLInsert(i)
  else
    Result := '';
end;

function TevTempTables.GetSQLInsert(const AIndex: Integer): String;
begin
  Result := GetParams(AIndex).Value['SQL.Insert'];
end;

function TevTempTables.GetSQLSelect(const ATable: String): String;
var
  i: Integer;
begin
  i := IndexOf(ATable);
  if i <> -1 then
    Result := GetSQLSelect(i)
  else
    Result := '';
end;

function TevTempTables.GetSQLSelect(const AIndex: Integer): String;
begin
  Result := GetParams(AIndex).Value['SQL.Select'];
end;

function TevTempTables.GetSQLUpdate(const ATable: String): String;
var
  i: Integer;
begin
  i := IndexOf(ATable);
  if i <> -1 then
    Result := GetSQLUpdate(i)
  else
    Result := '';
end;

function TevTempTables.GetSQLUpdate(const AIndex: Integer): String;
begin
  Result := GetParams(AIndex).Value['SQL.Update'];
end;

function TevTempTables.GetTableName(const AIndex: Integer): String;
begin
  Result := ParamName(AIndex);
end;

{ TevTempTablesBuilder }

constructor TevTempTablesBuilder.Create(const AOwner: TevDBAccess);
begin
  FOwner := AOwner;
end;

destructor TevTempTablesBuilder.Destroy;
begin
  FreeAndNil(FCO_STATES);
  FreeAndNil(FCO_SUI);
  FreeAndNil(FCO_LOCAL_TAX);
  inherited;
end;

procedure TevTempTablesBuilder.FlushChanges;
begin
  ctx_UpdateWait('Flush intermediate changes');
  FOwner.FDBServer.FlushTransactionChanges;
  FChangeCount := 0;
end;

procedure TevTempTablesBuilder.PrepareBackwardRefs;
var
  Item: IisListOfValues;
begin
  FBackwardRefs := TisParamsCollection.Create;

  Item := FBackwardRefs.AddParams('CO');
  Item.AddValue('Fields', 'HOME_CO_STATES_NBR,CL_CO_CONSOLIDATION_NBR');
  Item.AddValue('Data', TisListOfValues.Create as IisListOfValues);

  Item := FBackwardRefs.AddParams('CL_CO_CONSOLIDATION');
  Item.AddValue('Fields', 'PRIMARY_CO_NBR');
  Item.AddValue('Data', TisListOfValues.Create as IisListOfValues);
end;

procedure TevTempTablesBuilder.PrepareLookups;
var
  aMacros: IisListOfValues;
begin
  aMacros := TisListOfValues.Create;

  if not Assigned(FCO_STATES) then
  begin
    aMacros.Clear;
    aMacros.AddValue('Columns', 'CO_STATES_NBR,STATE');
    aMacros.AddValue('TableName', 'CO_STATES');
    FCO_STATES := FOwner.FDBServer.OpenDS('GenericSelectCurrent', nil, aMacros);
    FCO_STATES.IndexFieldNames := 'CO_STATES_NBR';
  end;

  if not Assigned(FCO_SUI) then
  begin
    aMacros.Clear;
    aMacros.AddValue('Columns', 'CO_SUI_NBR,SY_SUI_NBR');
    aMacros.AddValue('TableName', 'CO_SUI');
    FCO_SUI := FOwner.FDBServer.OpenDS('GenericSelectCurrent', nil, aMacros);
    FCO_SUI.IndexFieldNames := 'CO_SUI_NBR';
  end;

  if not Assigned(FCO_LOCAL_TAX) then
  begin
    aMacros.Clear;
    aMacros.AddValue('Columns', 'CO_LOCAL_TAX_NBR,SY_LOCALS_NBR');
    aMacros.AddValue('TableName', 'CO_LOCAL_TAX');
    FCO_LOCAL_TAX := FOwner.FDBServer.OpenDS('GenericSelectCurrent', nil, aMacros);
    FCO_LOCAL_TAX.IndexFieldNames := 'CO_LOCAL_TAX_NBR';
  end;
end;

procedure TevTempTablesBuilder.ProcessTable(const TempTableName: string);
var
  cd: TEvLinkedClientDataset;
  TableName: string;
  lookup_source_table: TEvBasicClientDataSet;
  lookup_source: TField;
  lookup_key: TField;
  f: TField;
  i: Integer;
  sFields, sValues, s: string;
  SQL: IevSQL;
  Macros: IisListOfValues;
  Progress, ProgressDelta, ProgressStep: Real;
  TblFields, TmpTablFields: IisStringList;
  ClNbrParam, LookupParam: Integer;
  TmpTblClass: TddTableClass;
  BackwardRef: IisListOfValues;
  BackwardRefData: IisListOfValues;
  RefFieldValues: IisListOfValues;
  BackwardRefFields: IisStringList;
begin
  ctx_UpdateWait('Retrive data for ' + TempTableName);

  TableName := Copy(TempTableName, 5, 255);

  // Make list of fields
  TblFields := GetddTableClassByName(TableName).GetFieldNameList;
  TmpTblClass := GetddTableClassByName(TempTableName);
  TmpTablFields := TmpTblClass.GetFieldNameList;
  TmpTablFields.Sort;

  for i := TblFields.Count - 1 downto 0 do
  begin
    if TmpTablFields.IndexOf(TblFields[i]) = -1 then
      if not (
         AnsiSameText(TblFields[i], 'CO_STATES_NBR') and (TmpTablFields.IndexOf('STATE') <> -1) or
         AnsiSameText(TblFields[i], 'CO_SUI_NBR') and (TmpTablFields.IndexOf('SY_SUI_NBR') <> -1) or
         AnsiSameText(TblFields[i], 'CO_LOCAL_TAX_NBR') and (TmpTablFields.IndexOf('SY_LOCALS_NBR') <> -1)) then
      begin
        if AnsiSameText(TableName, 'CO_BANK_ACCOUNT_REGISTER') and AnsiSameText(TblFields[i], 'NOTES') then
          TblFields[i] := '(CASE getblobsize(notes) WHEN -1 THEN ''N'' WHEN 0 THEN ''N'' ELSE ''Y'' END) notes_populated'
        else
          TblFields.Delete(i);
      end;
  end;

  if not FOwner.FullTTRebuildInProgress then
  begin
    // Check for backward references
    BackwardRef := FBackwardRefs.ParamsByName(TableName);
    if Assigned(BackwardRef) then
    begin
      BackwardRefFields := TisStringList.Create;
      BackwardRefFields.CommaText := BackwardRef.Value['Fields'];
      BackwardRefData := IInterface(BackwardRef.Value['Data']) as IisListOfValues;
    end;
  end;

  cd := TEvLinkedClientDataset.Create(nil);
  try
    Macros := TisListOfValues.Create;

    // Retrieve CL data
    Macros.AddValue('TABLENAME', TableName);

    s := '';
    for i := 0 to TblFields.Count - 1 do
      AddStrValue(s, TblFields[i], ',');

    Macros.AddValue('COLUMNS', s);

    FOwner.FDBServer.PopulateDS(cd, 'GenericSelectCurrent', nil, Macros);
    if cd.RecordCount = 0 then
      Exit;

    // Prepare lookups
    cd.InitLinkFields(TmpTablFields);
    if (TmpTablFields.IndexOf('STATE') <> -1) and (TblFields.IndexOf('STATE') = -1) then
    begin
      lookup_source_table := FCO_STATES;
      lookup_source := FCO_STATES.FieldByName('STATE');
      lookup_key := cd.FieldByName('CO_STATES_NBR');
    end

    else if (TmpTablFields.IndexOf('SY_SUI_NBR') <> -1) and (TblFields.IndexOf('SY_SUI_NBR') = -1) then
    begin
      lookup_source_table := FCO_SUI;
      lookup_source := FCO_SUI.FieldByName('SY_SUI_NBR');
      lookup_key := cd.FieldByName('CO_SUI_NBR');
    end

    else if (TmpTablFields.IndexOf('SY_LOCALS_NBR') <> -1) and (TblFields.IndexOf('SY_LOCALS_NBR') = -1) then
    begin
      lookup_source_table := FCO_LOCAL_TAX;
      lookup_source := FCO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR');
      lookup_key := cd.FieldByName('CO_LOCAL_TAX_NBR');
    end

    else
    begin
      lookup_source_table := nil;
      lookup_source := nil;
      lookup_key := nil;
    end;

    // Copy CL data in TT
    sFields := '';
    sValues := '';
    for i := 0 to TmpTablFields.Count - 1 do
    begin
      AddStrValue(sFields, TmpTablFields[i], ',');
      AddStrValue(sValues, ':' + TmpTablFields[i], ',');
    end;

    Macros.Clear;
    Macros.AddValue('TABLENAME', TempTableName);
    Macros.AddValue('FIELDLIST', sFields);
    Macros.AddValue('PARAMLIST', sValues);

    SQL := FOwner.FDBServer.CreateSQL('GenericInsert', Macros);

    ClNbrParam := SQL.GetParamIndex('CL_NBR');
    if Assigned(lookup_key) then
      LookupParam := SQL.GetParamIndex(lookup_source.FieldName)
    else
      LookupParam := -1;

    Progress := 0;
    ProgressDelta := 100 / cd.RecordCount;
    ProgressStep := 0;
    ctx_UpdateWait('Populate ' + TempTableName + '   ' + IntToStr(Round(Progress)) + '%');

    cd.First;
    while not cd.Eof do
    begin
      if Assigned(BackwardRefFields) then
      begin
        RefFieldValues := TisListOfValues.Create;              
        for i := 0 to BackwardRefFields.Count - 1 do
        begin
          f := cd.FieldByname(BackwardRefFields[i]);
          f.Tag := 1;
          RefFieldValues.AddValue(BackwardRefFields[i], f.Value);
        end;

        if RefFieldValues.Count > 0 then
          BackwardRefData.AddValue(cd.FieldByName(TableName + '_NBR').AsString, RefFieldValues);
      end;

      for i := 0 to TmpTablFields.Count - 1 do
      begin
        f := cd.FieldByLinkedIndex[i];
        if Assigned(f) and (f.Tag = 0) then
          SQL.SetParam(i, FOwner.CheckParamValue(f.Value))
        else
          SQL.SetParam(i, Null);
      end;

      if (LookupParam <> -1) and lookup_source_table.FindKey([lookup_key.AsInteger]) then
        SQL.SetParam(LookupParam, lookup_source.Value);

      SQL.SetParam(ClNbrParam, FClientNbr);

      SQL.Exec;

      if FOwner.FullTTRebuildInProgress then
      begin
        Inc(FChangeCount);
        if FChangeCount >= 5000 then
        begin
          FlushChanges;
          ctx_UpdateWait('Populate ' + TempTableName + '   ' + IntToStr(Round(Progress)) + '%');
        end;
      end;

      ProgressStep := ProgressStep + ProgressDelta;
      if ProgressStep >= 1 then
      begin
        Progress := Progress + ProgressStep;
        ProgressStep := 0;
        ctx_UpdateWait('Populate ' + TempTableName + '   ' + IntToStr(Round(Progress)) + '%');
      end;

      cd.Next;
    end;

  finally
    cd.Free;
  end;
end;


procedure TevTempTablesBuilder.Rebuild(const AClientNbr: Integer; const aTempClientsMode: Boolean);
var
  i: Integer;
  Q: IevQuery;
  iPreviousClNbr: Integer;
  bNewClient: Boolean;
begin
  ctx_UpdateWait('Preparation');

  FClientNbr := AClientNbr;
  FOwner.DisableSecurity;
  try
    iPreviousClNbr := FClientNbr;
    bNewClient:= not aTempClientsMode;
    try
      FOwner.SetCurrentClientNbr(FClientNbr);
      FOwner.StartTransaction([dbtTemporary]);
      try
        if bNewClient then
        begin
          // Check if client is valid
          Q := TevQuery.Create('SELECT cl_nbr FROM cl WHERE {AsOfNow<cl>}');
          if Q.Result.RecordCount = 0 then
            raise EBrokenDatabase.Create('This client database is marked as deleted. Please undelete it first.');
          if FClientNbr <> Q.Result.Fields[0].AsInteger then
            raise EBrokenDatabase.Create('Number mismatch! Database contains internal Client #' + Q.Result.Fields[0].AsString);
          Q := nil;
        end
        else
        begin
          // Delete existing data in temp tables
          FOwner.DeleteClient(FClientNbr);
          if FOwner.FullTTRebuildInProgress then
            FlushChanges;
        end;

        PrepareLookups;

        if not FOwner.FullTTRebuildInProgress then
          PrepareBackwardRefs;

        for i := 0 to TempTables.Count - 1 do
          ProcessTable(TempTables.GetTableName(i));

        if not FOwner.FullTTRebuildInProgress then
          UpdateBackwardRefs;

        ctx_UpdateWait('Commiting transaction');
        FOwner.CommitTransaction;
      except
        FOwner.RollbackTransaction;

        // Clean up commited data if there is any (see FlushTransactionChanges() )
        FOwner.StartTransaction([dbtTemporary]);
        try
          FOwner.DeleteClient(FClientNbr);
          FOwner.CommitTransaction;
        except
          FOwner.RollbackTransaction;
          raise;
        end;

        raise;
      end;

      if bNewClient and not FOwner.GetADRContext then
      begin
        ctx_UpdateWait('Updating security');

        if not FOwner.FFullTTRebuildMode then
          FOwner.FDBServer.SendBroadcastNotification('DBLISTCHANGED', 'TMP_TBLS');

        FOwner.StartTransaction([dbtBureau]);
        try
          FOwner.UpdateAccessToNewClients(FClientNbr);
          FOwner.CommitTransaction;
        except
          on E: Exception do
          begin
            FOwner.RollbackTransaction;
            E.Message := '"Access to new clients" security group is not updated due errors. Please log in as Admin and do it manually. ' + E.Message;
            raise;
          end;
        end;
      end;

    finally
      FOwner.SetCurrentClientNbr(iPreviousClNbr);
    end;

  finally
    FOwner.EnableSecurity;
    FClientNbr := 0;
  end;
end;

procedure TevDBAccess.DropDB(const ADBName: String);
begin
  if FDBServer.DataSecurityActive then
    CheckCondition(IsADRUser(Context.UserAccount.User), 'DB drop is allowed only for ADR');

  FDBServer.DropDB(ADBName);
end;

function TevDBAccess.GetDataSyncPacket(const ADBType: TevDBType; const AStartTransNbr: Integer; out AEndTransNbr: Integer): IisStream;
begin
  Result := FDBServer.GetDataSyncPacket(ADBType, AStartTransNbr, AEndTransNbr);
end;

procedure TevDBAccess.ApplyDataSyncPacket(const ADBType: TevDBType; const AData: IisStream);
begin
  FDBServer.ApplyDataSyncPacket(ADBType, AData);
end;

function TevDBAccess.GetDBHash(const ADBType: TevDBType; const ARequiredLastTransactionNbr: Integer): IisStream;
begin
  Result := FDBServer.GetDBHash(ADBType, ARequiredLastTransactionNbr).AsStream;
end;

procedure TevDBAccess.CheckDBHash(const ADBType: TevDBType; const AData: IisStream; const ARequiredLastTransactionNbr: Integer; out AErrors: String);
var
  DS: IevDataSet;
begin
  DS := TevDataSet.Create;
  DS.AsStream := AData;
  FDBServer.CheckDBHash(ADBType, DS, ARequiredLastTransactionNbr, AErrors);
end;

procedure TevDBAccess.SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean);
begin
  FDBServer.SetDBMaintenance(aDBName, aMaintenance);
end;

function TevDBAccess.GetADRContext: Boolean;
begin
  Result := FADRContext;
end;

procedure TevDBAccess.SetADRContext(const AValue: Boolean);
begin
  if FADRContext <> AValue then
  begin
    if AValue then
      CheckCondition(IsADRUser(Context.UserAccount.User) or IsDBAUser(Context.UserAccount.User), 'Not ADR or DBA account');
    ErrorIf(InTransaction, 'Transaction is active');

    FADRContext := AValue;
  end;
end;

procedure TevDBAccess.DisableDBs(const ADBList: IisStringList);
begin
  FDBServer.DisableDBs(ADBList);
end;

procedure TevDBAccess.EnableDBs(const ADBList: IisStringList);
begin
  FDBServer.EnableDBs(ADBList);
end;

function TevDBAccess.GetClientROFlag: Char;
var
  DS: IevDataSet;
begin
  DS := OpenQuery('SELECT read_only FROM cl WHERE {AsOfNow<cl>}', nil, nil);
  if DS.RecordCount = 0 then
    Result := READ_ONLY_FALSE
  else
    Result := DS.Fields[0].AsString[1];
end;

procedure TevDBAccess.SetClientROFlag(const AValue: Char);
var
  DS: TevClientDataSet;
begin
  if ctx_AccountRights.Functions.GetState('UNLOCK_CL_READ_ONLY_MODE') <> stEnabled then
    raise EevException.Create('You have no rights');

  FChangingClientROFlag := True;
  try
    StartTransaction([dbtClient]);
    try
      DS := TevClientDataSet.Create(nil);
      try
        DS.ProviderName := 'CL_CUSTOM_PROV';
        DS.CustomProviderTableName := 'CL';
        DS.ClientID := GetCurrentClientNbr;

        with TExecDSWrapper.Create('GenericSelectCurrent') do
        begin
          SetMacro('Columns', '*');
          SetMacro('TableName', 'CL');
          DS.DataRequest(AsVariant);
        end;
        DS.Open;

        DS.Edit;
        DS.FieldValues['READ_ONLY'] := AValue;
        DS.Post;
        ctx_DataAccess.PostDataSets([DS]);
      finally
        DS.Free;
      end;

      CommitTransaction;
    except
      RollbackTransaction;
    end;
  finally
    FChangingClientROFlag := False;
  end;
end;

procedure TevDBAccess.ApplyDataChangePacket(const APacket: IevDataChangePacket);
begin
  if Assigned(APacket) then
    FChangeManager.Apply(APacket);
end;

procedure TevDBAccess.SweepDB(const ADBName: String);
begin
  FDBServer.SweepDB(ADBName);
end;

function TevDBAccess.GetTransactionsStatus(const ADBType: TevDBType): IisListOfValues;
begin
  Result := FDBServer.GetTransactionsStatus(ADBType);
end;

procedure TevDBAccess.DisableReadTransaction;
begin
  Dec(FEnableReadTransRefCount);
  Assert(FEnableReadTransRefCount >= 0);
  if FEnableReadTransRefCount = 0 then
    FDBServer.UseReadTransaction := False;
end;

procedure TevDBAccess.EnableReadTransaction;
begin
  if FEnableReadTransRefCount = 0 then
    FDBServer.UseReadTransaction := True;
  Inc(FEnableReadTransRefCount);
end;

procedure TevDBAccess.BeginRebuildAllClients;
begin
  // Clean up Temp_Tbls database
  DisableSecurity;
  try
    FDBServer.InitializeTTForFullRebuild;
    FFullTTRebuildMode := True;
    try
      DropTmpTblsIntegrity;
    finally
      FFullTTRebuildMode := False;
    end;
  finally
    EnableSecurity;
  end;
end;

procedure TevDBAccess.EndRebuildAllClients;
                                     
  function ProcessConstraint(const AConstraint: String): String;
  var
    Q: IevQuery;
    err: Boolean;
    cnstr, s, tbl1, tbl2, fld1, fld2: String;

    procedure CreateConstraint;
    begin
      ctx_UpdateWait('Creating constraint ' + cnstr);

      StartTransaction([dbtTemporary]);
      try
        Q := TevQuery.Create('/*TMP_TBLS*/ ' + AConstraint);
        Q.Execute;

        CommitTransaction;
        err := False;
      except
        on E: Exception do
        begin
          mb_LogFile.AddContextEvent(etError, 'DB Access', 'Cannot restore table constraint ' + cnstr, E.Message);
          RollbackTransaction;
          err := True;
        end;
      end;
    end;

  begin
    Result := '';

    s := AConstraint;
    GetNextStrValue(s, ' CONSTRAINT ');
    cnstr := GetNextStrValue(s, ' ');

    CreateConstraint;

    // Delete data for this client
    if err then
    begin
      ctx_UpdateWait('Preparing for deleting a broken client');

      s := AConstraint;
      GetNextStrValue(s, 'ALTER TABLE ');
      tbl1 := Trim(GetNextStrValue(s, ' ADD '));
      GetNextStrValue(s, ' FOREIGN KEY (');
      fld1 := GetNextStrValue(s, ')');
      GetNextStrValue(fld1, 'CL_NBR, ');
      fld1 := Trim(fld1);

      GetNextStrValue(s, 'REFERENCES ');
      tbl2 := Trim(GetNextStrValue(s, '('));
      fld2 := GetNextStrValue(s, ')');
      GetNextStrValue(fld2, 'CL_NBR, ');
      fld2 := Trim(fld2);

      s := 'select distinct cl_nbr from ' + tbl1 + ' t where ';
      if fld1 <> '' then
        s := s + 't.' + fld1 + ' is not null and ';
      s := s + ' not exists (select cl_nbr from ' + tbl2 + ' where cl_nbr = t.cl_nbr ';
      if fld1 <> '' then
        s := s + ' and ' + fld2 + ' = t.' + fld1;
      s := s + ')';

      Q := TevQuery.Create(s);
      Q.Execute;

      while not Q.Result.Eof do
      begin
        AddStrValue(Result, 'CL_' + Q.Result.Fields[0].AsString + ' has a broken constraint ' + cnstr, #13);

        StartTransaction([dbtTemporary]);
        try
          ctx_UpdateWait('Deleting broken client CL_' + Q.Result.Fields[0].AsString);
          DeleteClient(Q.Result.Fields[0].AsInteger);
          CommitTransaction;
        except
          on E: Exception do
          begin
            mb_LogFile.AddContextEvent(etError, 'DB Access', 'Cannot delete broken client data for CL_' + Q.Result.Fields[0].AsString, E.Message);
            RollbackTransaction;
          end;
        end;

        Q.Result.Next;
      end;

      Q := nil;

      CreateConstraint; // Try again...
    end;
  end;

  function ProcessIndex(const AIndex: String): String;
  var
    Q, Qcl: IevQuery;
    err: Boolean;
    ind, s, tbl: String;
    uniqueInd: Boolean;
    DeletedClients: IisIntegerList;
    IndexFileds: IisStringList;
    CurClNbr, i: Integer;

    procedure CreateIndex;
    begin
      ctx_UpdateWait('Creating index ' + ind);
      StartTransaction([dbtTemporary]);
      try
        Q := TevQuery.Create('/*TMP_TBLS*/ ' + AIndex);
        Q.Execute;

        CommitTransaction;
        err := False;
      except
        on E: Exception do
        begin
          mb_LogFile.AddContextEvent(etError, 'DB Access', 'Cannot restore table index ' + ind, E.Message);
          RollbackTransaction;
          err := True;
        end;
      end;
    end;

    procedure DeleteBrokenClient;
    begin
      if DeletedClients.IndexOf(CurClNbr) = -1 then
      begin
        AddStrValue(Result, 'CL_' + IntToStr(CurClNbr) + ' is not unique on ' + IndexFileds.CommaText, #13);

        StartTransaction([dbtTemporary]);
        try
          ctx_UpdateWait('Deleting broken client CL_' + IntToStr(CurClNbr));
          DeleteClient(CurClNbr);
          CommitTransaction;
          DeletedClients.Add(CurClNbr);
        except
          on E: Exception do
          begin
            mb_LogFile.AddContextEvent(etError, 'DB Access', 'Cannot delete broken client data for CL_' + IntToStr(CurClNbr), E.Message);
            RollbackTransaction;
          end;
        end;
      end;
    end;

  begin
    Result := '';

    s := AIndex;
    GetNextStrValue(s, 'CREATE ');
    uniqueInd := AnsiSameText(Trim(GetNextStrValue(s, ' INDEX ')), 'UNIQUE');

    ind := GetNextStrValue(s, ' ON ');

    if uniqueInd then
    begin
      GetNextStrValue(s, '(');
      GetNextStrValue(s, ')');
    end;

    CreateIndex;

    // Delete data for this client
    try
      if err then
      begin
        s := AIndex;
        GetNextStrValue(s, ' ON ');
        tbl := GetNextStrValue(s, ' (');

        IndexFileds := TisStringList.Create;
        IndexFileds.CommaText := GetNextStrValue(s, ')');

        if uniqueInd then
        begin
          s := 'SELECT ' + IndexFileds.CommaText + ', Count(*) cnt FROM ' + tbl + ' GROUP BY ' + IndexFileds.CommaText + ' HAVING Count(*) > 1';
          Q := TevQuery.Create(s);
          Q.Execute;

          DeletedClients := TisIntegerList.Create;
          while not Q.Result.Eof do
          begin
            if Q.Result.Fields.FindField('CL_NBR') = nil then
            begin
              s := '';
              for i := 0 to IndexFileds.Count - 1 do
                AddStrValue(s, IndexFileds[i] + ' = :' + IndexFileds[i], ' AND ');
              Qcl := TevQuery.Create('SELECT DISTINCT cl_nbr FROM ' + tbl + ' WHERE ' +  s);
              for i := 0 to IndexFileds.Count - 1 do 
                Qcl.Params.AddValue(IndexFileds[i], Q.Result.FieldValues[IndexFileds[i]]);
              Qcl.Execute;

              while not Qcl.Result.Eof do
              begin
                CurClNbr := Qcl.Result.FieldByName('CL_NBR').AsInteger;
                DeleteBrokenClient;
                Qcl.Result.Next;
              end;
            end
            else
            begin
              CurClNbr := Q.Result.FieldByName('CL_NBR').AsInteger;
              DeleteBrokenClient;
            end;

            Q.Result.Next;
          end;

          Q := nil;

          CreateIndex;
        end;
      end;
    except
      on E: Exception do
      begin
        err := True;
        mb_LogFile.AddContextEvent(etError, 'DB Access', 'Cannot delete broken client data', E.Message);
      end;  
    end;

    if err then
      raise EevException.CreateDetailed('Index ' + ind + ' was not created.', 'Fix all uniqueness issues and rebuild temp tables for all clients again');
  end;


var
  i: Integer;
  Err, IndErr, s: String;
begin
  ctx_StartWait('Rebuilding temp tables integrity');
  DisableSecurity;
  try
    FFullTTRebuildMode := True;
    try
      DropTmpTblsIntegrity; // just in case to set the initial point

      Err := '';
      for i := 0 to High(TMP_Constraints) do
      begin
         s := ProcessConstraint(TMP_Constraints[i]);
         if s <> '' then
           AddStrValue(Err, s, #13);
      end;

      if Err <> '' then
        AddStrValue(Err, 'Solution: Fix all constraint issues in client databases and then rebuild temp tables for these clients', #13);

      IndErr := '';
      for i := 0 to High(TMP_Indices) do
      begin
         s := ProcessIndex(TMP_Indices[i]);
         if s <> '' then
           AddStrValue(IndErr, s, #13);
      end;

      if IndErr <> '' then
      begin
        AddStrValue(IndErr, 'Solution: Fix all uniqueness issues in client databases and then rebuild temp tables for these clients', #13);
        AddStrValue(Err, IndErr, #13#13);
      end;
    finally
      FFullTTRebuildMode := False;
    end;

    FDBServer.FinalizeTTAfterFullRebuild;

  finally
    EnableSecurity;
    ctx_EndWait;
  end;

  Mainboard.GlobalCallbacks.NotifySecurityChange('');
  FDBServer.SendBroadcastNotification('DBLISTCHANGED', 'TMP_TBLS');

  if Err <> '' then
    raise EevException.CreateDetailed('Temp tables integrity rebuild finished with errors', Err);
end;

procedure TevDBAccess.DropTmpTblsIntegrity;
var
  Q: IevQuery;
begin
  StartTransaction([dbtTemporary]);
  try
    // Drop all constraints
    Q := TevQuery.Create(
      '/*TMP_TBLS*/ ' +
      'EXECUTE BLOCK AS ' +
      'DECLARE VARIABLE stmt VARCHAR(1000); ' +
      'BEGIN ' +
      'FOR ' +
      '  select ''alter table ''||r.rdb$relation_name ||'' drop constraint ''||r.rdb$constraint_name||'';'' ' +
      '  from rdb$relation_constraints r ' +
      '  where (r.rdb$constraint_type=''FOREIGN KEY'') ' +
      '  into stmt ' +
      '  DO ' +
      '  BEGIN ' +
      '    execute statement stmt; ' +
      '  END ' +
      'END');

    Q.Execute;

    // Disable all indexes
    Q := TevQuery.Create(
      '/*TMP_TBLS*/ ' +
      'EXECUTE BLOCK AS ' +
      'DECLARE VARIABLE stmt VARCHAR(1000); ' +
      'BEGIN ' +
      '  for select ''DROP INDEX ''||rdb$index_name ||'';'' ' +
      '      from rdb$indices i ' +
      '      where (rdb$system_flag is null or rdb$system_flag = 0) and ' +
      '             not exists (select rdb$index_name from RDB$RELATION_CONSTRAINTS where rdb$index_name = i.rdb$index_name) ' +
      '      order by rdb$foreign_key nulls first ' +
      '  into stmt ' +
      '  do EXECUTE STATEMENT stmt; ' +
      'END');

    Q.Execute;

    CommitTransaction;
  except
    RollbackTransaction;
    raise;
  end;
end;

procedure TevTempTablesBuilder.UpdateBackwardRefs;
var
  i, j, k: Integer;
  tbl, flds, s, upd_set: String;
  BackwardRefData, RefFieldValues: IisListOfValues;
  SQL: IevSQL;
begin
  if not Assigned(FBackwardRefs) then
    Exit;

  for i := 0 to FBackwardRefs.Count - 1 do
  begin
    try
      upd_set := '';
      flds := FBackwardRefs[i].Value['Fields'];
      while flds <> '' do
      begin
        s := GetNextStrValue(flds, ',');
        AddStrValue(upd_set, s + '=:' + s, ',');
      end;

      tbl := FBackwardRefs.ParamName(i);

      BackwardRefData := IInterface(FBackwardRefs[i].Value['Data']) as IisListOfValues;
      for j := 0 to BackwardRefData.Count - 1 do
      begin
        RefFieldValues := IInterface(BackwardRefData[j].Value) as IisListOfValues;

        SQL := FOwner.FDBServer.CreateSQL(Format('update TMP_%s set %s where cl_nbr = :cl_nbr and %s_nbr = :nbr', [tbl, upd_set, tbl]));

        for k := 0 to RefFieldValues.Count - 1 do
          SQL.SetParam(RefFieldValues[k].Name, RefFieldValues[k].Value);

        SQL.SetParam('cl_nbr', FClientNbr);
        SQL.SetParam('nbr', StrToInt(BackwardRefData[j].Name));

        SQL.Exec;
      end;
    except
      raise;
    end;
  end;
end;

{ TevDataChangeManager }

constructor TevDataChangeManager.Create(const AOwner: TevDBAccess);
begin
  inherited Create;
  FOwner := AOwner;
end;

procedure TevDataChangeManager.Apply(const APacket: IevDataChangePacket);
var
  i: Integer;
  ChangeItem: IevDataChangeItem;
begin
  if APacket.AsOfDate = 0 then
    FAsOfDate := SysDate
  else
  begin
    FAsOfDate := APacket.AsOfDate;
    Assert(FAsOfDate = SysDate);  // As-of-date editing is not allowed
  end;

  for i := 0 to APacket.Count - 1 do
  begin
    ChangeItem := APacket[i];
    DoBeforeOperation(ChangeItem);

    case ChangeItem.ChangeType of
      dctInsertRecord: DoInsert(ChangeItem);
      dctUpdateFields: DoUpdate(ChangeItem);
      dctDeleteRecord: DoDelete(ChangeItem);
    end;
  end;
end;

procedure TevDataChangeManager.DoInsert(const AChangeItem: IevDataChangeItem);
var
  i: Integer;
  FieldList, ValueList: String;
  Q, Q1: IevQuery;
  EENbr, tmpNbr: integer;
  Item: IisNamedValue;
begin
  if AChangeItem.Fields.TryGetValue(AChangeItem.DataSource + '_nbr', 0) < 0 then
    raise EevException.Create('Attempt to write negative primary key value for ' + AChangeItem.DataSource);

   if (ctx_DBAccess.GetChangedByNbr < 0) and (ctx_DBAccess.GetChangedByNbr <> sGuestInternalNbr) and
       ((UpperCase(AChangeItem.DataSource) = 'CL_PERSON_DEPENDENTS') or (StartsWith(UpperCase(AChangeItem.DataSource), 'EE')))  then
   begin
     if (AChangeItem.Key > 0) then
     begin
         Q := TevQuery.Create('SELECT DISTINCT t.EE_NBR FROM EE t WHERE  t.EE_NBR = :Nbr AND {AsOfNow<t>} and  ' +
                                                        't.SELFSERVE_USERNAME = :pUserName AND t.SELFSERVE_PASSWORD = :pPassword ');
         Q.Params.AddValue('Nbr', Abs(ctx_DBAccess.GetChangedByNbr));
         Q.Params.AddValue('pUserName', Copy(Context.UserAccount.User, 4, Length(Context.UserAccount.User)-1));
         Q.Params.AddValue('pPassword', Context.UserAccount.Password);
         Q.Execute;
         if Q.Result.RecordCount = 0 then
          raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr));

         EENbr:= 0;
         tmpNbr:= 0;
         if (UpperCase(AChangeItem.DataSource) = 'CL_PERSON_DEPENDENTS') then
         begin
          Item:= AChangeItem.Fields.FindValue('CL_PERSON_NBR');
          if Assigned(Item) then
          tmpNbr :=  VarToInt(Item.Value);

          Q1 := TevQuery.Create('select DISTINCT t.EE_NBR from EE t where {AsOfNow<t>} AND t.CL_PERSON_NBR = :Nbr');
          Q1.Params.AddValue('Nbr', tmpNbr);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
          if Q1.Result.RecordCount = 1 then
               EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger
          else
               EENbr:= -1;
         end
         else
         if (UpperCase(AChangeItem.DataSource) = 'EE_TIME_OFF_ACCRUAL_OPER') then
         begin
          Item:= AChangeItem.Fields.FindValue('EE_TIME_OFF_ACCRUAL_NBR');
          if Assigned(Item) then
          tmpNbr :=  VarToInt(Item.Value);

          Q1 := TevQuery.Create('SELECT DISTINCT t.EE_NBR FROM EE_TIME_OFF_ACCRUAL t WHERE {AsOfNow<t>} AND t.EE_TIME_OFF_ACCRUAL_NBR = :Nbr ');
          Q1.Params.AddValue('Nbr', tmpNbr);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
             EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger;
         end
         else
         if (UpperCase(AChangeItem.DataSource) = 'EE_BENEFICIARY') then
         begin
          Item:= AChangeItem.Fields.FindValue('EE_BENEFITS_NBR');
          if Assigned(Item) then
          tmpNbr :=  VarToInt(Item.Value);
          if tmpNbr > 0 then
          begin
            Q1 := TevQuery.Create('select DISTINCT t.EE_NBR from EE_BENEFITS t where {AsOfNow<t>} t.EE_BENEFITS_NBR = :Nbr ');
            Q1.Params.AddValue('Nbr', tmpNbr);
            Q1.Execute;
            if Q1.Result.RecordCount = 0 then
              raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
            else
               EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger;
          end;
         end
         else
         begin
          Item:= AChangeItem.Fields.FindValue('EE_NBR');
          if Assigned(Item) then
          EENbr :=  VarToInt(Item.Value);
          if EENbr < 1 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
         end;

         if (Abs(ctx_DBAccess.GetChangedByNbr) <> EENbr) and (EENbr <> -1) then
         begin
             Q1 := TevQuery.Create('select co_group_nbr from co_group_member b where {AsOfNow<b>} and b.ee_nbr=:Nbr');
             Q1.Params.AddValue('Nbr', EENbr);
             Q1.Execute;
             if Q1.Result.RecordCount > 0 then
             begin
               Q := TevQuery.Create('select ee_nbr from co_group_manager a where {AsOfNow<a>} ' +
                 ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and b.ee_nbr=:p1' +
                 ' and co_group_nbr in (select co_group_nbr from co_group )) and ' +
                 ' ee_nbr=:P3');
               Q.Params.AddValue('P1', EENbr);
               Q.Params.AddValue('P3', Abs(ctx_DBAccess.GetChangedByNbr));
               Q.Execute;
               if Q.Result.RecordCount = 0 then
                 raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr));
             end
             else
              raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr));
         end;
     end
   end;


  RemoveNoAccessFields(AChangeItem, True);

  if AChangeItem.Fields.Count = 0 then
    Exit;

  FieldList := '';
  for i := 0 to AChangeItem.Fields.Count - 1 do
    AddStrValue(FieldList, AChangeItem.Fields[i].Name, ',');

  ValueList := ':' + StringReplace(FieldList, ',', ',:', [rfReplaceAll]);

  FOwner.FDBServer.ExecDS(Format('INSERT INTO %s (%s) VALUES (%s)', [AChangeItem.DataSource, FieldList, ValueList]), AChangeItem.Fields, nil);

  FOwner.FTablesChangedInTransaction.Add(AChangeItem.DataSource);
end;

procedure TevDataChangeManager.DoUpdate(const AChangeItem: IevDataChangeItem);
var
  i: Integer;
  FieldList: String;
  TblClass: TddTableClass;
  VerFields: IisIntegerList;
  VerFldsAndVals: IisListOfValues;
  Q, Q1: IevQuery;
  bPackRec: Boolean;
  UpdateAsOfDate, UntilAsOfDate: TisDate;
  EENbr: integer;
begin
  if AChangeItem.Fields.Count = 0 then
    exit;

   if (ctx_DBAccess.GetChangedByNbr < 0) and (ctx_DBAccess.GetChangedByNbr <> sGuestInternalNbr) and
       ((UpperCase(AChangeItem.DataSource) = 'CL_PERSON') or (UpperCase(AChangeItem.DataSource) = 'CL_PERSON_DEPENDENTS') or (StartsWith(UpperCase(AChangeItem.DataSource), 'EE')))  then
   begin
     if (AChangeItem.Key > 0) then
     begin
         Q := TevQuery.Create('SELECT DISTINCT t.EE_NBR FROM EE t WHERE  t.EE_NBR = :Nbr AND {AsOfNow<t>} and  ' +
                                                        't.SELFSERVE_USERNAME = :pUserName AND t.SELFSERVE_PASSWORD = :pPassword ');
         Q.Params.AddValue('Nbr', Abs(ctx_DBAccess.GetChangedByNbr));
         Q.Params.AddValue('pUserName', Copy(Context.UserAccount.User, 4, Length(Context.UserAccount.User)-1));
         Q.Params.AddValue('pPassword', Context.UserAccount.Password);
         Q.Execute;
         if Q.Result.RecordCount = 0 then
          raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr));

         if (UpperCase(AChangeItem.DataSource) = 'CL_PERSON') then
         begin
          Q1 := TevQuery.Create('SELECT DISTINCT t.EE_NBR FROM EE t WHERE {AsOfNow<t>} AND t.CL_PERSON_NBR = :Nbr');
          Q1.Params.AddValue('Nbr', AChangeItem.Key);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
          if Q1.Result.RecordCount = 1 then
               EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger
          else
               EENbr:= -1;
         end
         else
         if (UpperCase(AChangeItem.DataSource) = 'CL_PERSON_DEPENDENTS') then
         begin
          Q1 := TevQuery.Create('select DISTINCT t.EE_NBR from CL_PERSON_DEPENDENTS a, CL_PERSON b, EE t ' +
                                     'where {AsOfNow<a>} AND {AsOfNow<b>} AND {AsOfNow<t>} AND a.CL_PERSON_DEPENDENTS_NBR = :Nbr  AND ' +
                                     '      a.CL_PERSON_NBR = b.CL_PERSON_NBR and ' +
                                     '      b.CL_PERSON_NBR = t.CL_PERSON_NBR ' );
          Q1.Params.AddValue('Nbr', AChangeItem.Key);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
          if Q1.Result.RecordCount = 1 then
               EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger
          else
               EENbr:= -1;
         end
         else
         if (UpperCase(AChangeItem.DataSource) = 'EE_TIME_OFF_ACCRUAL_OPER') then
         begin
          Q1 := TevQuery.Create('SELECT DISTINCT t.EE_NBR FROM EE_TIME_OFF_ACCRUAL_OPER a, EE_TIME_OFF_ACCRUAL t WHERE {AsOfNow<t>} AND {AsOfNow<a>} AND ' +
                                      ' a.EE_TIME_OFF_ACCRUAL_OPER_NBR = :Nbr and t.EE_TIME_OFF_ACCRUAL_NBR = a.EE_TIME_OFF_ACCRUAL_NBR ');
          Q1.Params.AddValue('Nbr', AChangeItem.Key);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
             EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger;
         end
         else
         if (UpperCase(AChangeItem.DataSource) = 'EE_BENEFICIARY') then
         begin
          Q1 := TevQuery.Create('select DISTINCT t.EE_NBR from EE_BENEFICIARY a, EE_BENEFITS t ' +
                                 ' where {AsOfNow<a>} AND {AsOfNow<t>} a.EE_BENEFICIARY_NBR = :Nbr and ' +
                                      '  a.EE_BENEFITS_NBR = t.EE_BENEFITS_NBR ');
          Q1.Params.AddValue('Nbr', AChangeItem.Key);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
             EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger;
         end
         else
         begin
          Q1 := TevQuery.CreateFmt('SELECT DISTINCT t.EE_NBR FROM %s t WHERE t.%s_nbr = :Nbr ',
                   [AChangeItem.DataSource, AChangeItem.DataSource]);
          Q1.Params.AddValue('Nbr', AChangeItem.Key);
          Q1.Execute;
          if Q1.Result.RecordCount = 0 then
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr))
          else
            EENbr:= Q1.Result.FieldByName('EE_NBR').AsInteger;
         end;

         if (Abs(ctx_DBAccess.GetChangedByNbr) <> EENbr) and (EENbr <> -1) then
         begin
           Q1 := TevQuery.Create('select co_group_nbr from co_group_member b where {AsOfNow<b>} and b.ee_nbr=:Nbr');
           Q1.Params.AddValue('Nbr', EENbr);
           Q1.Execute;
           if Q1.Result.RecordCount > 0 then
           begin
             Q := TevQuery.Create('select ee_nbr from co_group_manager a where {AsOfNow<a>} ' +
               ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and b.ee_nbr=:p1' +
               ' and co_group_nbr in (select co_group_nbr from co_group )) and ' +
               ' ee_nbr=:P3');
             Q.Params.AddValue('P1', EENbr);
             Q.Params.AddValue('P3', Abs(ctx_DBAccess.GetChangedByNbr));
             Q.Execute;
             if Q.Result.RecordCount = 0 then
               raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr));
           end
           else
            raise EevException.CreateDetailed('System Error', IntToStr(ctx_DBAccess.GetChangedByNbr));
         end;
     end
   end;

  AChangeItem.Fields.DeleteValue(AChangeItem.DataSource + '_nbr');

  RemoveNoAccessFields(AChangeItem, False);

  UpdateAsOfDate := Trunc(AChangeItem.Fields.TryGetValue('effective_date', FAsOfDate));
  UntilAsOfDate := DayBeforeEndOfTime;
  AChangeItem.Fields.DeleteValue('effective_date');

  if AChangeItem.Fields.Count = 0 then
    Exit;

  bPackRec := False;

  TblClass := GetddTableClassByName(AChangeItem.DataSource);
  if TblClass.IsVersionedTable then
  begin
    // Find changing versioned fields
    VerFields := TisIntegerList.Create;
    for i := 0 to AChangeItem.Fields.Count - 1 do
      if Assigned(VerFields) and TblClass.IsVersionedField(AChangeItem.Fields[i].Name) then
        VerFields.Add(i);

    if VerFields.Count > 0 then
    begin
      // Check if there is a rec_version with required effective_date
      Q := TevQuery.CreateFmt('SELECT t.effective_date, t.effective_until FROM %s t WHERE t.%s_nbr = :Nbr AND t.effective_until > :Dat ORDER BY 1',
        [AChangeItem.DataSource, AChangeItem.DataSource]);
      Q.Params.AddValue('Nbr', AChangeItem.Key);
      Q.Params.AddValue('Dat', UpdateAsOfDate);
      Q.Execute;

      if Q.Result.RecordCount = 1 then
      begin
        if Trunc(Q.Result.Fields[0].AsDateTime) = UpdateAsOfDate then
        begin
          VerFields := nil;  // It means update versioned fields by generic UPDATE statement
          bPackRec := True;
        end
      end
      else
      begin
        Q.Result.Last;
        UntilAsOfDate := Trunc(IncDay(Q.Result.Fields[1].AsDateTime, -1));
      end;
      Q := nil;
    end
    else
      VerFields := nil;
  end;

  FieldList := '';
  for i := 0 to AChangeItem.Fields.Count - 1 do
    if not Assigned(VerFields) or (VerFields.IndexOf(i) = -1) then
      AddStrValue(FieldList, AChangeItem.Fields[i].Name + '= :' + AChangeItem.Fields[i].Name, ',');

  AChangeItem.Fields.AddValue(AChangeItem.DataSource + '_nbr', AChangeItem.Key);
  AChangeItem.Fields.AddValue('StatusDate', UpdateAsOfDate);

  if FieldList <> '' then
  begin
    FOwner.FDBServer.ExecDS(Format('{UpdateAsOfDate<%s, "%s">}', [AChangeItem.DataSource, FieldList]), AChangeItem.Fields, nil);
    if bPackRec then
      PackRecord(AChangeItem.DataSource, AChangeItem.Key);
  end;

  if Assigned(VerFields) then
  begin
    VerFldsAndVals := TisListOfValues.Create;
    for i := 0 to VerFields.Count - 1 do
      VerFldsAndVals.AddValue(AChangeItem.Fields[VerFields[i]].Name, AChangeItem.Fields[VerFields[i]].Value);

    EnsureVerFieldValue(AChangeItem.DataSource, AChangeItem.Key, UpdateAsOfDate, UntilAsOfDate, VerFldsAndVals);
  end;

  FOwner.FTablesChangedInTransaction.Add(AChangeItem.DataSource);
end;

procedure TevDataChangeManager.DoDelete(const AChangeItem: IevDataChangeItem);
var
  P: IisListOfValues;
begin
  P := TisListOfValues.Create;
  P.AddValue('nbr', AChangeItem.Key);
  FOwner.FDBServer.ExecDS(Format('{DeleteRecord<%s>}', [AChangeItem.DataSource]), P, nil);

  FOwner.FTablesChangedInTransaction.Add(AChangeItem.DataSource);
end;

procedure TevDataChangeManager.UpdateTempTables;
var
  i: Integer;
  Tbl: String;
  KeyList: IisIntegerList;
  ChangedTables: IevDataSet;
  NbrFld: TField;
  ClientTables: IisStringList;
begin
  FOwner.DisableSecurity;
  try
    ClientTables := TisStringList.Create;
    for i := 0 to TempTables.Count - 1 do
      ClientTables.Add(TempTables.GetClientTableName(i));

    ChangedTables := FOwner.FDBServer.GetChangedTablesInTransaction(dbtClient, ClientTables);
    if not Assigned(ChangedTables) or (ChangedTables.RecordCount = 0) then
      Exit;

    ChangedTables.IndexFieldNames := 'table_name';
    NbrFld := ChangedTables.FieldByName('nbr');

    KeyList := TisIntegerList.Create;

    for i := 0 to ClientTables.Count - 1 do
    begin
      Tbl := ClientTables[i];

      KeyList.Clear;
      ChangedTables.SetRange([Tbl], [Tbl]);
      while not ChangedTables.Eof do
      begin
        KeyList.Add(NbrFld.AsInteger);
        ChangedTables.Next;
      end;

      if KeyList.Count > 0 then
        SyncTempTable(Tbl, KeyList);
    end;

    Clear;
  finally
    FOwner.EnableSecurity;
  end;
end;

procedure TevDataChangeManager.SyncTempTable(const AClientTable: String; const AKeys: IisIntegerList);
var
  TmpTbl, recFlt, KeyFldName: String;
  bSyncAllRecords: Boolean;
  i: Integer;
  qTblData, qTTData: IevQuery;
  qDelTT, qInsTT, qUpdTT: IevSQL;
  KeyFld: TField;

  procedure DeleteTTRecord(const AKey: Integer);
  begin
    if not Assigned(qDelTT) then
      qDelTT := FOwner.FDBServer.CreateSQL(TempTables.GetSQLDelete(TmpTbl));
    qDelTT.SetParam('cl_nbr', FOwner.GetCurrentClientNbr);
    qDelTT.SetParam('nbr', AKey);
    qDelTT.Exec;
  end;

  procedure InsertTTRecord;
  var
    i: Integer;
  begin
    if not Assigned(qInsTT) then
      qInsTT := FOwner.FDBServer.CreateSQL(TempTables.GetSQLInsert(TmpTbl));
    qInsTT.SetParam('cl_nbr', FOwner.GetCurrentClientNbr);
    for i := 0 to qTblData.Result.Fields.Count - 1 do
      qInsTT.SetParam(qTblData.Result.Fields[i].FieldName, FOwner.CheckParamValue(qTblData.Result.Fields[i].Value));
    qInsTT.Exec;
  end;

  procedure UpdateTTRecord(const AKey: Integer);
  var
    i: Integer;
  begin
    if not Assigned(qUpdTT) then
      qUpdTT := FOwner.FDBServer.CreateSQL(TempTables.GetSQLUpdate(TmpTbl));
    qUpdTT.SetParam('cl_nbr', FOwner.GetCurrentClientNbr);
    qUpdTT.SetParam('nbr', AKey);
    for i := 0 to qTblData.Result.Fields.Count - 1 do
      qUpdTT.SetParam(qTblData.Result.Fields[i].FieldName, FOwner.CheckParamValue(qTblData.Result.Fields[i].Value));
    qUpdTT.Exec;
  end;

begin
  KeyFldName := AClientTable + '_nbr';
  TmpTbl := 'TMP_' + AClientTable;

  bSyncAllRecords := not Assigned(AKeys) or (AKeys.Count = 0);
  if bSyncAllRecords then
    recFlt := ''
  else
  begin
    if AKeys.Count = 1 then
      recFlt := ' AND ' + KeyFldName + ' = ' + IntToStr(AKeys[0])
    else if AKeys.Count > 1 then
      recFlt := ' AND ' + BuildINsqlStatement(KeyFldName, AKeys)
  end;

  qTblData := TevQuery.Create(TempTables.GetSQLClientSelect(TmpTbl) + recFlt);
  qTblData.Execute;

  if not bSyncAllRecords then
  begin
    qTblData.Result.IndexFieldNames := KeyFldName;
    for i := 0 to AKeys.Count - 1 do
    begin
      if not qTblData.Result.FindKey([AKeys[i]]) then
        DeleteTTRecord(AKeys[i]);
    end;

    qTTData := TevQuery.Create(TempTables.GetSQLSelect(TmpTbl) + recFlt);
    qTTData.Params.AddValue('cl_nbr', FOwner.GetCurrentClientNbr);
    qTTData.Execute;
    qTTData.Result.IndexFieldNames := KeyFldName;
  end
  else
    qTTData := nil;

  KeyFld := qTblData.Result.FieldByName(KeyFldName);
  qTblData.Result.First;
  while not qTblData.Result.Eof do
  begin
    if not Assigned(qTTData) or not qTTData.Result.FindKey([KeyFld.AsInteger]) then
      InsertTTRecord
    else
      UpdateTTRecord(KeyFld.AsInteger);

    qTblData.Result.Next;
  end;
end;

procedure TevDataChangeManager.Clear;
begin
  FAsOfDate := 0;
end;

procedure TevDataChangeManager.RemoveNoAccessFields(const AChangeItem: IevDataChangeItem; const AForInsert: Boolean);
var
  i, j: Integer;
  Flds: TevFieldValues;
  bDelField: Boolean;
begin
  AChangeItem.Fields.DeleteValue('rec_version');
  AChangeItem.Fields.DeleteValue('effective_until');

  if FOwner.FDBServer.DataSecurityActive then
    for i := AChangeItem.Fields.Count - 1 downto 0 do
      if IsFieldHidden(AChangeItem.DataSource, AChangeItem.Fields[i].Name) then
      begin
        bDelField := True;
        if AForInsert then
        begin
          Flds := FieldCodeValues.FindFields(AChangeItem.DataSource);
          if Assigned(Flds) then
          begin
            j := Flds.FieldIndex(AChangeItem.Fields[i].Name);
            if j <> -1 then
              if Flds[j].DefValue <> '' then
              begin
                AChangeItem.Fields[i].Value := Flds[j].DefValue;
                bDelField := False;
              end;
          end
        end;

        if bDelField then
          AChangeItem.Fields.Delete(i);
      end;
end;

procedure TevDataChangeManager.UpdateFieldVersion(const ATable: String; const ANbr: Integer;
  const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TisDate; const AFieldsAndValues: IisListOfValues);

  procedure DuplicateRecord(const ADataSet: IevDataSet; const AEffective_Date: TisDate; const AEffective_Until: TisDate);
  var
    i, rec_version_ind, effective_until_ind: Integer;
    Q: IevQuery;
    sFields, sValues: String;
  begin
    sFields := '';
    sValues := '';
    rec_version_ind := ADataSet.FieldByName('rec_version').FieldNo - 1;
    effective_until_ind := ADataSet.FieldByName('effective_until').FieldNo - 1;

    for i := 0 to ADataSet.FieldCount - 1 do
      if (i <> rec_version_ind) and (i <> effective_until_ind) then
      begin
        AddStrValue(sFields, ADataSet.Fields[i].FieldName, ', ');
        AddStrValue(sValues, ':' + ADataSet.Fields[i].FieldName, ', ');
      end;

    Q := TevQuery.CreateFmt('INSERT INTO %s (%s) VALUES (%s)', [ATable, sFields, sValues]);
    for i := 0 to ADataSet.FieldCount - 1 do
      if (i <> rec_version_ind) and (i <> effective_until_ind) then
        Q.Params.AddValue(ADataSet.Fields[i].FieldName, FOwner.CheckParamValue(ADataSet.Fields[i].Value));

    for i := 0 to AFieldsAndValues.Count - 1 do
      Q.Params.Value[AFieldsAndValues[i].Name] := AFieldsAndValues[i].Value;

    Q.Params.AddValue('effective_date', AEffective_Date);
    Q.Params.AddValue('effective_until', AEffective_Until);
    Q.Execute;
  end;

  procedure UpdateValueForPeriod(ABeginDate, AEndDate: TisDate; const AFldVal: IisListOfValues);
  var
    Q: IevQuery;
    SetPart: String;
    i: Integer;
  begin
    SetPart := '';
    for i := 0 to AFldVal.Count - 1 do
      AddStrValue(SetPart, AFldVal[i].Name + ' = :' + AFldVal[i].Name, ', ');

    if (ABeginDate = 0) or (AEndDate = 0) then
      Q := TevQuery.CreateFmt('UPDATE %s SET %s WHERE %s_nbr = :nbr', [ATable, SetPart, ATable], False)
    else
    begin
      Q := TevQuery.CreateFmt('UPDATE %s SET %s WHERE %s_nbr = :nbr AND effective_date BETWEEN :begin_date AND :end_date', [ATable, SetPart, ATable], False);
      Q.Params.AddValue('begin_date', ABeginDate);
      Q.Params.AddValue('end_date', AEndDate);
    end;
    Q.Params.AddValue('nbr', ANbr);

    for i := 0 to AFldVal.Count - 1 do
      Q.Params.AddValue(AFldVal[i].Name, AFldVal[i].Value);

    Q.Execute;
  end;

  procedure PropogateNeighborValue(const AValueAsOfDate, ABeginDate, AEndDate: TisDate);
  var
    Q: IevQuery;
    FldsPart: String;
    LV: IisListOfValues;
    i: Integer;
  begin
    FldsPart := '';
    for i := 0 to AFieldsAndValues.Count - 1 do
      AddStrValue(FldsPart, 't.' + AFieldsAndValues[i].Name, ', ');

    Q := TevQuery.CreateFmt('SELECT %s FROM %s t WHERE t.%s_nbr = :Nbr AND {AsOfDate<t>}', [FldsPart, ATable, ATable]);
    Q.Params.AddValue('Nbr', ANbr);
    Q.Params.AddValue('StatusDate', AValueAsOfDate);
    Q.Execute;

    LV := TisListOfValues.Create;
    for i := 0 to Q.Result.Fields.Count - 1 do
      LV.AddValue(Q.Result.Fields[i].FieldName, Q.Result.Fields[i].Value);

    UpdateValueForPeriod(ABeginDate, AEndDate, LV);
  end;

var
  Q: IevQuery;
  TblClass: TddTableClass;
  NewEndDate, UntilDate, d: TisDate;
  bNewPeriod, bUpdateBeginDate, bUpdateEndDate: Boolean;
  i: Integer;
begin
  for i := 0 to AFieldsAndValues.Count - 1 do
    if IsFieldHidden(ATable, AFieldsAndValues[i].Name) then
      Exit;

  TblClass := GetddTableClassByName(ATable);
  Assert(TblClass.IsVersionedTable);

  for i := 0 to AFieldsAndValues.Count - 1 do
    Assert(TblClass.IsVersionedField(AFieldsAndValues[i].Name));

  FOwner.DisableSecurity;
  try
    FOwner.StartTransaction([TblClass.GetDBType]);
    try
      bNewPeriod := (AOldBeginDate = EmptyDate) or (AOldEndDate = EmptyDate);
      bUpdateBeginDate := (ANewBeginDate <> EmptyDate) and (AOldBeginDate <> ANewBeginDate);
      bUpdateEndDate := (ANewEndDate <> EmptyDate) and (AOldEndDate <> ANewEndDate);

      NewEndDate := ANewEndDate;
      if NewEndDate <> EmptyDate then
        UntilDate := Trunc(IncDay(NewEndDate, 1))
      else
        UntilDate := EmptyDate;

      // ======== PERIOD BEGIN CORRECTION ========
      if bUpdateBeginDate then
      begin
        // Get version on period begin
        Q := TevQuery.CreateFmt('SELECT t.* FROM %s t WHERE t.%s_nbr = :Nbr AND {AsOfDate<t>}', [ATable, ATable]);
        Q.Params.AddValue('Nbr', ANbr);
        Q.Params.AddValue('StatusDate', ANewBeginDate);

        // Duplicate version record
        if Q.Result.RecordCount > 0 then
          if Q.Result['effective_date'] <> ANewBeginDate then
          begin
            d := Q.Result['effective_until'];
            if UntilDate = EmptyDate then
            begin
              UntilDate := d;
              NewEndDate := Trunc(IncDay(UntilDate, -1));
            end
            else if UntilDate <= d then
              bUpdateEndDate := False // It's enough to make only one copy of the previous record
            else
              UntilDate := d;

            DuplicateRecord(Q.Result, ANewBeginDate, UntilDate);
          end;
      end;

      // ======== PERIOD END CORRECTION ========
      if bUpdateEndDate then
      begin
        Assert(UntilDate <> EmptyDate);

        // Get version on next day of period end
        Q := TevQuery.CreateFmt('SELECT t.* FROM %s t WHERE t.%s_nbr = :Nbr AND {AsOfDate<t>}', [ATable, ATable]);
        Q.Params.AddValue('Nbr', ANbr);
        Q.Params.AddValue('StatusDate', UntilDate);

        // Duplicate version record
        if Q.Result.RecordCount > 0 then
          if Q.Result['effective_date'] <> UntilDate then
            DuplicateRecord(Q.Result, UntilDate, Q.Result['effective_until']);
      end;

      // ======== VALUE CORRECTION ========
      // Update Field in all records of the period
      UpdateValueForPeriod(ANewBeginDate, NewEndDate, AFieldsAndValues);

      // Propogate neighbour values
      if not bNewPeriod and bUpdateBeginDate and (ANewBeginDate > AOldBeginDate) then
        PropogateNeighborValue(Trunc(IncDay(AOldBeginDate, -1)), AOldBeginDate, Trunc(IncDay(ANewBeginDate, -1)));
      if not bNewPeriod and bUpdateEndDate and (NewEndDate < AOldEndDate) then
        PropogateNeighborValue(Trunc(IncDay(NewEndDate, 1)), Trunc(IncDay(AOldEndDate, 1)), NewEndDate);

      PackRecord(ATable, ANbr);

      FOwner.CommitTransaction;
    except
      FOwner.RollbackTransaction;
      raise;
    end;
  finally
    FOwner.EnableSecurity;
  end;
end;

(* Do not delete this! It may be used in future.   -Aleksey
procedure TevDataChangeManager.DoAsOfDateDelete(const AChangeItem: IevDataChangeItem);
begin
  AChangeItem.Fields.AddValue(AChangeItem.DataSource + '_nbr', AChangeItem.Key);
  AChangeItem.Fields.AddValue('StatusDate', FAsOfDate);

  FOwner.FDBServer.ExecDS(Format('{DeleteVersionsAfterDate<%s>}', [AChangeItem.DataSource]), AChangeItem.Fields, nil);
  FOwner.FDBServer.ExecDS(Format('{DeleteAsOfDate<%s>}', [AChangeItem.DataSource]), AChangeItem.Fields, nil);
end;
*)

function TevDBAccess.GetFieldVersions(const ATable: String; const AFields: TisCommaDilimitedString;
  const ANbr: Integer; const ASettings: IisListOfValues): IevDataSet;
var
  Q: IevQuery;
  oldValues: array of Variant;

  function ValuesChanged: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to High(oldValues) do
      if not VarSameValue(oldValues[i], Q.Result.Fields[i + 2].Value) then
      begin
        Result := True;
        break;
      end;
  end;

var
  FieldList: IisStringList;
  tbl: TDataDictTable;
  fld: TDataDictField;
  fk: TDataDictForeignKey;
  FData: IevDataSet;
  ValueConverter: IevFieldValueToTextConverter;
  i: Integer;
  ResultDSFldsInfo: array of TDSFieldDef;
  d: TDateTime;
  asofDateDate: TisDate;
  s: String;
begin
  FieldList := TisStringList.Create;
  FieldList.CommaText := AFields;

  Q := TevQuery.CreateFmt('SELECT effective_date, effective_until, %s FROM %s WHERE %s_nbr = %d ORDER BY effective_date', [AFields, ATable, ATable, ANbr]);
  Q.Execute;

  // Build the result dataset structure
  SetLength(ResultDSFldsInfo, FieldList.Count + 2);
  ResultDSFldsInfo[0].FieldName := 'begin_date';
  ResultDSFldsInfo[0].DataType := ftDate;
  ResultDSFldsInfo[0].Size := 0;
  ResultDSFldsInfo[0].Required := False;
  ResultDSFldsInfo[1].FieldName := 'end_date';
  ResultDSFldsInfo[1].DataType := ftDate;
  ResultDSFldsInfo[1].Size := 0;
  ResultDSFldsInfo[1].Required := False;

  for i := 0 to FieldList.Count - 1 do
  begin
    ResultDSFldsInfo[i + 2].FieldName := FieldList[i];
    ResultDSFldsInfo[i + 2].DataType := Q.Result.Fields[i + 2].DataType;
    ResultDSFldsInfo[i + 2].Size := Q.Result.Fields[i + 2].Size;
    ResultDSFldsInfo[i + 2].Required := False;
  end;

  // Check if we need translate field value in text
  tbl := EvoDataDictionary.Tables.TableByName(ATable);
  for i := 0 to FieldList.Count - 1 do
  begin
    fld := tbl.Fields.FieldByName(FieldList[i]);
    fk := tbl.ForeignKeys.ForeignKeyByField(fld);
    if Assigned(fk) or (fld.FieldValues.Count > 0) then
    begin
      // Warning! Boolean conversion is mandatory here due Variant unconditional evaluation
      if not Assigned(ASettings) or Boolean(ASettings.TryGetValue(FieldList[i] + '\ReferenceAsText', True)) then
      begin
        SetLength(ResultDSFldsInfo, Length(ResultDSFldsInfo) + 1);
        ResultDSFldsInfo[High(ResultDSFldsInfo)].FieldName := '$' + FieldList[i];
        ResultDSFldsInfo[High(ResultDSFldsInfo)].DataType := ftString;
        ResultDSFldsInfo[High(ResultDSFldsInfo)].Size := 64;
        ResultDSFldsInfo[High(ResultDSFldsInfo)].Required := False;

        if not Assigned(ValueConverter) then
          ValueConverter := TevFieldValueToTextConverter.Create;
      end;
    end
  end;

  FData := TevDataSet.Create(ResultDSFldsInfo);
  FData.vclDataSet.LogChanges := False;

  // Keep only unique combination of field values
  SetLength(oldValues, FieldList.Count);
  Q.Result.First;
  while not Q.Result.Eof do
  begin
    if (Q.Result.RecNo = 1) or ValuesChanged then
    begin
      FData.Append;
      for i := 0 to Q.Result.Fields.Count - 1 do
        FData.Fields[i].Value := Q.Result.Fields[i].Value;

      for i := 0 to High(oldValues) do
        oldValues[i] := Q.Result.Fields[i + 2].Value;
    end
    else if Q.Result.RecNo = Q.Result.RecordCount then
    begin
      FData.Last;
      FData.Edit;
      FData.Fields[1].AsDateTime := Q.Result.Fields[1].AsDateTime;
      FData.Post;
    end;

    Q.Result.Next;
  end;

  // Reconstruct data of end_date field and make text values
  FData.Last;
  d := FData.Fields[1].AsDateTime;
  while not FData.Bof do
  begin
    FData.Edit;
    FData.Fields[1].AsDateTime := IncDay(d, -1);

    for i := FieldList.Count + 2 to FData.FieldCount - 1 do
    begin
      s := FData.Fields[i].FieldName;
      GetNextStrValue(s, '$');

      if s <> '' then
      begin
        if Trunc(FData.Fields[1].AsDateTime) > SysDate then
          asofDateDate := SysDate
        else
          asofDateDate := Trunc(FData.Fields[1].AsDateTime);

        FData.Fields[i].AsString := ValueConverter.Convert(ATable, s, FData[s], asofDateDate, False);
      end;
    end;

    FData.Post;

    d := FData.Fields[0].AsDateTime;
    FData.Prior;
  end;

  FData.First;
  Result := FData;
end;

function TevDBAccess.GetFieldValues(const ATable, AField: String; const ANbr: Integer; const AsOfDate: TDateTime; const AEndDate: TDateTime; const AMultiColumn: Boolean): IisListOfValues;
var
  Builder: IevFieldValuesBuilder;
begin
  Builder := TevFieldValuesBuilder.Create(ATable, AField, ANbr, Trunc(AsOfDate), Trunc(AEndDate));
  Result := Builder.Build(AMultiColumn);
end;

procedure TevDataChangeManager.PackRecord(const ATable: String; const ANbr: Integer);
var
  Q: IevQuery;
begin
  // Delete not unique record versions
  Q := TevQuery.CreateFmt('EXECUTE PROCEDURE pack_%s (:nbr)', [ATable], False);
  Q.Params.AddValue('Nbr', ANbr);
  Q.Execute;
end;

procedure TevDataChangeManager.EnsureVerFieldValue(const ATable: String;
  const ANbr: Integer; const ABeginDate, AEndDate: TisDate; const AFieldsAndValues: IisListOfValues);
begin
  UpdateFieldVersion(ATable, ANbr, EmptyDate, EmptyDate, ABeginDate, AEndDate, AFieldsAndValues);
end;

class function TevDataChangeManager.IsFieldHidden(const ATable, AField: string): Boolean;
var
  FldInfo: IevSecFieldInfo;
begin
  FldInfo := ctx_AccountRights.Fields.GetSecFieldInfo(ATable, AField);
  Result := Assigned(FldInfo) and (FldInfo.State = stDisabled);
end;

procedure TevDataChangeManager.DoBeforeOperation(const AChangeItem: IevDataChangeItem);
begin
  case AChangeItem.ChangeType of
    dctInsertRecord: DataChangeTriggers.BeforeInsert(AChangeItem.DataSource, AChangeItem.Key, AChangeItem.Fields);
    dctUpdateFields: DataChangeTriggers.BeforeUpdate(AChangeItem.DataSource, AChangeItem.Key, AChangeItem.Fields);
    dctDeleteRecord: DataChangeTriggers.BeforeDelete(AChangeItem.DataSource, AChangeItem.Key);
  end;
end;


{ TevFieldValuesBuilder }

procedure TevFieldValuesBuilder.ApplyFilter(const AValues: IevDataSet);
var
  Q: IevQuery;
  FilterOutExemptValue: boolean;
begin
  FilterOutExemptValue := False;

  if AnsiSameText(FTable.Name, 'CL_E_DS') and
    (AnsiSameText(FField.Name, 'EE_EXEMPT_EXCLUDE_FEDERAL') or AnsiSameText(FField.Name, 'EE_EXEMPT_EXCLUDE_OASDI') or
    AnsiSameText(FField.Name, 'EE_EXEMPT_EXCLUDE_MEDICARE') or AnsiSameText(FField.Name, 'EE_EXEMPT_EXCLUDE_EIC') or
    AnsiSameText(FField.Name, 'ER_EXEMPT_EXCLUDE_OASDI') or AnsiSameText(FField.Name, 'ER_EXEMPT_EXCLUDE_MEDICARE') or
    AnsiSameText(FField.Name, 'ER_EXEMPT_EXCLUDE_FUI')) then
  begin
    Q := TevQuery.CreateFmt('SELECT t.E_D_Code_Type FROM %s t WHERE {AsOfDate<t>} AND t.%s = %d',
        [FTable.Name, FTable.PrimaryKey[0].Field.Name, FRecordNbr]);
    Q.Params.AddValue('StatusDate', FAsOfDate);

    FilterOutExemptValue := AnsiSameText(Q.Result.Fields[0].AsString, ED_EWOD_TAXABLE_AUTO);
  end;

  if (AnsiSameText(FTable.Name, 'CL_E_D_STATE_EXMPT_EXCLD') and
    (AnsiSameText(FField.Name, 'EMPLOYEE_EXEMPT_EXCLUDE_STATE') or AnsiSameText(FField.Name, 'EMPLOYEE_EXEMPT_EXCLUDE_SDI') or
    AnsiSameText(FField.Name, 'EMPLOYEE_EXEMPT_EXCLUDE_SUI') or AnsiSameText(FField.Name, 'EMPLOYER_EXEMPT_EXCLUDE_SDI') or
    AnsiSameText(FField.Name, 'EMPLOYER_EXEMPT_EXCLUDE_SUI'))) or

    (AnsiSameText(FTable.Name, 'CL_E_D_LOCAL_EXMPT_EXCLD') and AnsiSameText(FField.Name, 'EXEMPT_EXCLUDE')) then
  begin
    Q := TevQuery.CreateFmt('SELECT t2.E_D_Code_Type FROM %s t1 JOIN CL_E_DS t2 ON t1.CL_E_DS_NBR = t2.CL_E_DS_NBR ' +
      'WHERE {AsOfDate<t1>} AND {AsOfDate<t2>} AND t1.%s = %d',
        [FTable.Name, FTable.PrimaryKey[0].Field.Name, FRecordNbr]);
    Q.Params.AddValue('StatusDate', FAsOfDate);

    FilterOutExemptValue := AnsiSameText(Q.Result.Fields[0].AsString, ED_EWOD_TAXABLE_AUTO);
  end;

  if FilterOutExemptValue and AValues.Locate('description', 'Exempt', []) then
    AValues.Delete;
end;

function TevFieldValuesBuilder.Build(const AMultiColumn: Boolean): IisListOfValues;
var
  ResStructConsts: array of TDSFieldDef;
  Q: IevQuery;
  fk, ref: TddsForeignKey;
  fld: TddsField;
  i, j: Integer;
  s, v, dispFld: String;
  sPartialSQL: String;
  ImpliedTables: IisStringList;
  sDispKey: String;
  ResDS: IevDataSet;
  Titles: IisStringList;

  procedure BuildImplTableList(ATable: TddsTable);
  var
    mk: TddsForeignKey;
  begin
    ImpliedTables.Add(ATable.Name);
    mk := ATable.GetMasterKey;
    if Assigned(mk) then
    begin
      ImpliedTables.Add(TddsTable(mk.Table).Name);
      BuildImplTableList(TddsTable(mk.Table));
    end;
  end;

  procedure AddFiledToResStruct(AFK: TddsForeignKey);
  var
    dispFld, DisplayKey: String;
    fld: TddsField;
    ref: TddsForeignKey;
  begin
    DisplayKey := TddsTable(AFK.Table).DisplayKey;
    while DisplayKey <> '' do
    begin
      GetNextStrValue(DisplayKey, '[');
      dispFld := GetNextStrValue(DisplayKey, ']');
      dispFld := GetNextStrValue(dispFld, ':');

      fld := TddsField(AFK.Table.Fields.FieldByName(dispFld));
      Assert(Assigned(fld));

      if fld.FieldType = ddtInteger then
        ref := TddsForeignKey(AFK.Table.ForeignKeys.ForeignKeyByField(fld))
      else
        ref := nil;

      if Assigned(ref) then
      begin
        if ImpliedTables.IndexOf(ref.Table.Name) = -1 then
          AddFiledToResStruct(ref);
      end
      else
      begin
        SetLength(ResStructConsts, Length(ResStructConsts) + 1);
        ResStructConsts[High(ResStructConsts)].FieldName := 'description' + IntToStr(High(ResStructConsts));
        ResStructConsts[High(ResStructConsts)].Size := 0;
        ResStructConsts[High(ResStructConsts)].Required := False;
        Titles.Add(fld.DisplayName);

        if fld.FieldType = ddtInteger then
          ResStructConsts[High(ResStructConsts)].DataType := ftInteger
        else if fld.FieldType = ddtFloat then
          ResStructConsts[High(ResStructConsts)].DataType := ftFloat
        else if fld.FieldType = ddtDateTime then
          ResStructConsts[High(ResStructConsts)].DataType := ftDateTime
        else if fld.FieldType = ddtString then
        begin
          ResStructConsts[High(ResStructConsts)].DataType := ftString;
          if fld.FieldValues.Count = 0 then
            ResStructConsts[High(ResStructConsts)].Size := fld.Size
          else
            ResStructConsts[High(ResStructConsts)].Size := 40;
        end
        else
        begin
          ResStructConsts[High(ResStructConsts)].DataType := ftString;
          ResStructConsts[High(ResStructConsts)].Size := 255;
        end;
      end;
    end;
  end;

begin
  if FField.FieldValues.Count = 0 then
    fk := TddsForeignKey(FTable.ForeignKeys.ForeignKeyByField(FField))
  else
    fk := nil;

  if not Assigned(fk) and (FField.FieldValues.Count = 0) then
  begin
    Result := nil;
    Exit;
  end;

  Titles := TisStringList.Create;

  if Assigned(fk) then
  begin
    // Lookup
    sPartialSQL := CheckChildBranch(FTable, TddsTable(fk.Table), nil);

    s := GetTransitiveFilter(fk);
    if s <> '' then
      sPartialSQL := sPartialSQL + ' AND ' + s; 

    sPartialSQL := sPartialSQL + ' AND ' + Format(':EndDate < (SELECT MAX(effective_until) FROM %s WHERE %s = t0.%s)',
      [fk.Table.Name, fk.Table.PrimaryKey[0].Field.Name, fk.Table.PrimaryKey[0].Field.Name]);

    ImpliedTables := TisStringList.CreateUnique;
    BuildImplTableList(FTable);

    s := fk.Table.PrimaryKey[0].Field.Name;
    sDispKey := TddsTable(fk.Table).DisplayKey;
    while sDispKey <> '' do
    begin
      GetNextStrValue(sDispKey, '[');
      dispFld := GetNextStrValue(sDispKey, ']');
      dispFld := GetNextStrValue(dispFld, ':');

      fld := TddsField(fk.Table.Fields.FieldByName(dispFld));
      Assert(Assigned(fld));
      if fld.FieldType = ddtInteger then
      begin
        ref := TddsForeignKey(fk.Table.ForeignKeys.ForeignKeyByField(fld));
        if Assigned(ref) and (ImpliedTables.IndexOf(ref.Table.Name) <> -1) then
          continue;
        AddStrValue(s, 'CAST(' + fld.Name + ' AS VARCHAR(80)) AS ' + fld.Name, ', ');
      end
      else if (fld.FieldType = ddtString) and (fld.Size < 80) then
        AddStrValue(s, 'CAST(' + fld.Name + ' AS VARCHAR(80)) AS ' + fld.Name, ', ')
      else
        AddStrValue(s, fld.Name, ', ');
    end;

    Q := TevQuery.CreateFmt('SELECT %s %s', [s, sPartialSQL]);
    Q.Params.AddValue('StatusDate', FAsOfDate);
    Q.Params.AddValue('EndDate', FEndDate);

    SetLength(ResStructConsts, 1);
    ResStructConsts[0].FieldName := 'value';
    ResStructConsts[0].DataType := ftString;
    ResStructConsts[0].Size := 10;
    ResStructConsts[0].Required := False;
    Titles.Add('Value');

    if AMultiColumn then
      AddFiledToResStruct(fk)
    else
    begin
      SetLength(ResStructConsts, 2);
      ResStructConsts[1].FieldName := 'description';
      ResStructConsts[1].DataType := ftString;
      ResStructConsts[1].Size := 256;
      ResStructConsts[1].Required := False;
      Titles.Add('Description');
    end;


    ResDS := TevDataSet.Create(ResStructConsts);
    ResDS.LogChanges := False;
    while not Q.Result.Eof do
    begin
      Q.Result.Edit;
      for i := 1 to Q.Result.Fields.Count - 1 do
      begin
        if Q.Result.Fields[i].DataType = ftString then
          Q.Result.Fields[i].Value := FValueConverter.Convert(
            fk.Table.Name, Q.Result.Fields[i].FieldName,
            Q.Result.Fields[i].Value, FAsOfDate, False);
      end;

      if not AMultiColumn then
      begin
        s := FormatDisplayKey(Q.Result.Fields, TddsTable(fk.Table).DisplayKey);
        ResDS.AppendRecord([Q.Result.Fields[0].AsString, s]);
      end
      else
      begin
        ResDS.Append;
        ResDS.Fields[0].AsString := Q.Result.Fields[0].AsString;
        j := 1;
        for i := 1 to Q.Result.FieldCount - 1 do
        begin
          s := Q.Result.Fields[i].AsString;
          if s = '' then
            Inc(j)
          else
            while s <> '' do
            begin
              ResDS.Fields[j].AsString := GetNextStrValue(s, '   ');
              Inc(j);
            end;
        end;
        ResDS.Post;
      end;

      Q.Result.Cancel;

      Q.Result.Next;
    end;
  end

  else
  begin
    // Static list
    SetLength(ResStructConsts, 2);
    ResStructConsts[0].FieldName := 'value';
    ResStructConsts[0].DataType := ftString;
    ResStructConsts[0].Size := 10;
    ResStructConsts[0].Required := False;
    ResStructConsts[1].FieldName := 'description';
    ResStructConsts[1].DataType := ftString;
    ResStructConsts[1].Size := 64;
    ResStructConsts[1].Required := False;

    ResDS := TevDataSet.Create(ResStructConsts);
    ResDS.LogChanges := False;
    for i := 0 to FField.FieldValues.Count - 1 do
    begin
      s := FField.FieldValues[i];
      v := GetNextStrValue(s, '=');
      ResDS.AppendRecord([v, s]);
      if FField.DisplayFormat <> '' then
      begin
        ResDS.Edit;
        ResDS.Fields[1].AsString := FormatDisplayKey(ResDS.Fields, FField.DisplayFormat);
        ResDS.Post;
      end;
    end;

    Titles.Add('Value');
    Titles.Add('Description');
  end;

  ApplyFilter(ResDS);

  Result := TisListOfValues.Create;
  Result.AddValue('Titles', Titles);
  Result.AddValue('Data', ResDS);
end;

function TevFieldValuesBuilder.CheckChildBranch(AParentTable, AChildTable: TddsTable; APath: TevTablePath): String;
var
  ChildMasterKey, MasterKey: TddsForeignKey;
  Nbr: Integer;
  sFrom, sWhere: String;
  i: Integer;
  s, tblAlias: String;
begin
  Result := '';

  SetLength(APath, Length(APath) + 1);
  APath[High(APath)] := TddsTable(AChildTable);

  ChildMasterKey := TddsForeignKey(AChildTable.GetMasterKey);
  if Assigned(ChildMasterKey) then
  begin
    Nbr := GetParentBranchKey(AParentTable, TddsTable(ChildMasterKey.Table), nil);
    if Nbr = 0 then
      Result := CheckChildBranch(AParentTable, TddsTable(ChildMasterKey.Table), APath) // Recursion
    else
    begin
      sFrom := '';
      sWhere := '';

      for i := 0 to High(APath) do
      begin
        tblAlias := 't' + IntToStr(i);
        AddStrValue(sFrom, APath[i].Name + ' ' + tblAlias);

        if i > 0 then
        begin
          MasterKey := APath[i - 1].GetMasterKey;
          s := Format('%s.%s = %s.%s', ['t' + IntToStr(i - 1), MasterKey.Fields[0].Field.Name, tblAlias, APath[i].PrimaryKey[0].Field.Name]);
        end;

        if i = High(APath) then
          s := Format('%s.%s = %d', [tblAlias, ChildMasterKey.Fields[0].Field.Name, Nbr]);

        AddStrValue(s, Format('{AsOfDate<%s>}', [tblAlias]), ' AND ');

        AddStrValue(sWhere, s, ' AND ');
      end;

      Result := 'FROM ' + sFrom + ' WHERE ' + sWhere;
    end;
  end

  else
    Result := 'FROM ' + APath[0].Name + ' t0 WHERE {AsOfDate<t0>}' ;
end;

constructor TevFieldValuesBuilder.Create(const ATable, AField: String; const ARecordNbr: Integer; const AAsOfDate: TisDate; const AEndDate: TisDate);
begin
  inherited Create;

  FValueConverter := TevFieldValueToTextConverter.Create;

  FTable :=  TddsTable(EvoDataDictionary.Tables.TableByName(ATable));
  FField := TddsField(FTable.Fields.FieldByName(AField));

  FRecordNbr := ARecordNbr;

  if AAsOfDate = EmptyDate then
    FAsOfDate := SysDate
  else
    FAsOfDate := AAsOfDate;

  if AEndDate = EmptyDate then
    FEndDate := DayBeforeEndOfTime
  else
    FEndDate := AEndDate;
end;

function TevFieldValuesBuilder.GetParentBranchKey(ATable, AExpectedParentTable: TddsTable; APath: TevTablePath): Integer;
var
  MasterKey: TddsForeignKey;
  SQL, sFrom, sWhere: String;
  i: Integer;
  s, tblAlias: String;
  Q: IevQuery;
begin
  Result := 0;
  SetLength(APath, Length(APath) + 1);
  APath[High(APath)] := ATable;

  if AExpectedParentTable = ATable then
  begin
    sFrom := '';
    sWhere := '';

    for i := 0 to High(APath) do
    begin
      tblAlias := 't' + IntToStr(i);
      AddStrValue(sFrom, APath[i].Name + ' ' + tblAlias, ', ');

      if i > 0 then
      begin
        MasterKey := APath[i - 1].GetMasterKey;
        s := Format('%s.%s = %s.%s', ['t' + IntToStr(i - 1), MasterKey.Fields[0].Field.Name, tblAlias, APath[i].PrimaryKey[0].Field.Name]);
      end
      else
        s := Format('%s.%s = %d', [tblAlias, APath[i].PrimaryKey[0].Field.Name, FRecordNbr]);

      AddStrValue(s, Format('{AsOfDate<%s>}', [tblAlias]), ' AND ');

      AddStrValue(sWhere, s, ' AND ');
    end;

    SQL := Format('SELECT %s.%s FROM %s WHERE %s', [tblAlias, ATable.PrimaryKey[0].Field.Name, sFrom, sWhere]);

    Q := TevQuery.Create(SQL);
    Q.Params.AddValue('StatusDate', FAsOfDate);
    Result := Q.Result.Fields[0].AsInteger;
  end
  else
  begin
    MasterKey := ATable.GetMasterKey;
    if Assigned(MasterKey) then
      Result := GetParentBranchKey(TddsTable(MasterKey.Table), AExpectedParentTable, APath); // Recursion
  end
end;

procedure TevDBAccess.UpdateFieldVersion(const ATable: String; const ANbr: Integer;
  const AOldBeginDate, AOldEndDate, ANewBeginDate, ANewEndDate: TDateTime; const AFieldsAndValues: IisListOfValues);
begin
  FChangeManager.UpdateFieldVersion(ATable, ANbr, Trunc(AOldBeginDate), Trunc(AOldEndDate),
    Trunc(ANewBeginDate), Trunc(ANewEndDate), AFieldsAndValues);
end;

procedure TevDBAccess.LockTableInTransaction(const aTable, aCondition: String);

  procedure DoLock;
  var
    Q: IevQuery;
    cond: String;
  begin
    if aCondition = '' then
      cond := '1 = 1'
    else
      cond := aCondition;

    Q := TevQuery.CreateFmt('{LockTableWithCondition<%s, %s>}', [aTable, cond]);
    Q.Execute;
  end;

begin
  CheckCondition(InTransaction, 'Transaction is not started');

  while True do
  begin
    try
      DoLock;
      Break;
    except
      on E: Exception do
        if not Contains(E.Message, 'DEADLOCK') then
          raise;
    end;
  end;
end;

function TevDBAccess.SyncDBAndApp(const ADBName: String): Boolean;
begin
  Result := FDBServer.SyncDBAndApp(ADBName);
end;

procedure TevDBAccess.ImportDatabase(const ADBPath, ADBPassword: String);
begin
  FDBServer.ImportDatabase(ADBPath, ADBPassword);
end;

function TevDBAccess.GetDBServerFolderContent(const APath, AUser, APassword: String): IevDataSet;
begin
  Result := FDBServer.GetDBServerFolderContent(APath, AUser, APassword, '', True);
end;

procedure TevDBAccess.SyncUDFAndApp;
begin
  FDBServer.SyncUDFAndApp();
end;

function TevDBAccess.GetDBFilesList: IevDataSet;
begin
  Result := FDBServer.GetDBFilesList;
end;

procedure TevDBAccess.TidyUpDBFolders;
begin
  FDBServer.TidyUpDBFolders;
end;

procedure TevDBAccess.ArchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
begin

end;

procedure TevDBAccess.UnarchiveDB(const ADBName: String; const AArchiveDate: TDateTime);
begin

end;

function TevDBAccess.FullTTRebuildInProgress: Boolean;
begin
  Result := FFullTTRebuildMode;
end;

procedure TevDBAccess.UpdateFieldValue(const ATable, AField: String;
  const ANbr: Integer; const ABeginDate, AEndDate: TDateTime; const AValue: Variant);
var
  LV: IisListOfValues;
begin
  LV := TisListOfValues.Create;
  LV.AddValue(AField, AValue);

  FChangeManager.EnsureVerFieldValue(ATable, ANbr, Trunc(ABeginDate), Trunc(AEndDate), LV);
end;

function TevDBAccess.GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetVersionFieldAudit(ATable, AField, ANbr, ABeginDate);
end;

function TevDBAccess.GetVersionFieldAuditChange(const ATable, AField: String;  {const ANbr: Integer;} const ABeginDate: TDateTime): IevDataset;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetVersionFieldAuditChange(ATable, AField, {ANbr,} ABeginDate);
end;

procedure TevDBAccess.ChangeFBAdminPassword(const APassword, ANewPassword: String);
begin
  CheckCondition(APassword = mb_GlobalSettings.DBAdminPassword, 'Cannot change password');
  FDBServer.ChangeFBAdminPassword(ANewPassword);
end;

procedure TevDBAccess.ChangeFBUserPassword(const AAdminPassword, ANewPassword: String);
begin
  CheckCondition(AAdminPassword = mb_GlobalSettings.DBAdminPassword, 'Cannot change password');
  FDBServer.ChangeFBUserPassword(ANewPassword);
end;

function TevDBAccess.GetRecordAudit(const ATable: String; const AFields: TisCommaDilimitedString; const ANbr: Integer; const ABeginDate: TDateTime): IevDataSet;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetRecordAudit(ATable, AFields, ANbr, ABeginDate);
end;

function TevDBAccess.GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetBlobAuditData(ATable, AField, ARefValue);
end;

function TevDBAccess.GetTableAudit(const ATable: String; const ABeginDate: TDateTime): IevDataSet;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetTableAudit(ATable, ABeginDate);
end;

function TevDBAccess.GetDatabaseAudit(const ADBType: TevDBType; const ABeginDate: TDateTime): IevDataSet;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetDatabaseAudit(ADBType, ABeginDate);
end;

function TevFieldValuesBuilder.GetTransitiveFilter(AFK: TddsForeignKey): String;
var
  i, j: Integer;
  Q: IevQuery;
begin
  Result := '';

  for i := 0 to AFK.Table.ForeignKeys.Count - 1 do
  begin
    for j := 0 to FTable.ForeignKeys.Count - 1 do
      if (FTable.ForeignKeys[j] <> AFK) and (AFK.Table.ForeignKeys[i].Table = FTable.ForeignKeys[j].Table) then
      begin
        Q := TevQuery.CreateFmt('SELECT t.%s FROM %s t WHERE {AsOfDate<t>} AND t.%s = %d',
            [FTable.ForeignKeys[j].Fields[0].Field.Name, FTable.Name, FTable.PrimaryKey[0].Field.Name, FRecordNbr]);
        Q.Params.AddValue('StatusDate', FAsOfDate);

        if not Q.Result.Fields[0].IsNull then
        begin
          Result := Format('%s = %d', [AFK.Table.ForeignKeys[i].Fields[0].Field.Name, Q.Result.Fields[0].AsInteger]);
          break;
        end;
      end;

    if Result <> '' then
      break;
  end
end;

{ TevAuditData }

procedure TevAuditData.AfterConstruction;
begin
  inherited;
  FAllowToResolveUserName := ctx_Security.AccountRights.Functions.GetState('DISPLAY_USERNAME_IN_AUDIT') = stEnabled;
end;

function TevAuditData.GetBlobAuditData(const ATable, AField: String; const ARefValue: Integer): IisStream;
var
  Q: IevQuery;
  s: String;
  ref: Integer;
  TblClass: TddTableClass;
begin
  Result := nil;

  if TevDataChangeManager.IsFieldHidden(ATable, AField) then
    Exit;

  if ARefValue < 0 then
  begin
    ref := -ARefValue;
    Q := TevQuery.CreateFmt('/*%s*/ SELECT data FROM ev_field_change_blob WHERE nbr = %d', [GetDBNameByTable(ATable), ref]);
  end

  else
  begin
    ref := ARefValue;

    TblClass := GetddTableClassByName(ATable);
    if TblClass.GetFieldType(AField) = ftBlob then
      Q := TevQuery.CreateFmt('SELECT t.%s FROM %s t WHERE t.%s_nbr = %d AND {AsOfNow<t>}', [AField, ATable, ATable, ref])
    else
    begin
      s := TblClass.GetRefBlobTableClass.GetTableName;
      Q := TevQuery.CreateFmt('SELECT t.%s FROM %s t WHERE t.%s_nbr = %d AND {AsOfNow<t>}', ['DATA', s, s, ref]);
    end;
  end;

  if not Q.Result.Fields[0].IsNull then
  begin
    Result := TisStream.Create;
    (Q.Result.Fields[0] as TBlobField).SaveToStream(Result.RealStream);
  end;
end;

function TevAuditData.GetDatabaseAudit(const ADBType: TevDBType;
  const ABeginDate: TDateTime): IevDataSet;
const
  ResStruct: array [0..1] of TDSFieldDef = (
    (FieldName: 'table_name';  DataType: ftString;  Size: 40;  Required: False),
    (FieldName: 'last_change_date';  DataType: ftDateTime;  Size: 0;  Required: False));

var
  Q: IevQuery;
  Tables: IisListOfValues;
  Res, Changes: IevDataSet;
  db: TddDatabaseClass;
  tableList: TddTableList;
  TableClass: TddTableClass;
  i: Integer;
begin
  // Initialize Result
  Res := TevDataSet.Create(ResStruct);
  Res.LogChanges := False;

  case ADBType of
    dbtSystem: db := TDM_SYSTEM;
    dbtBureau: db := TDM_SERVICE_BUREAU;
    dbtClient: db := TDM_CLIENT;
  else
    Exit;
  end;

  tableList := db.GetTableList;
  Tables := TisListOfValues.Create;
  for i := 0 to High(tableList) do
  begin
    TableClass := GetddTableClassByName(tableList[i]);
    Tables.AddValue(IntToStr(TableClass.GetEvTableNbr), TableClass.GetTableName);
  end;

  // Get changes
  Q := TevQuery.CreateFmt(
    '/*%s*/'#13 +
    'SELECT tc.ev_table_nbr, MAX(tr.commit_time) last_change'#13 +
    'FROM ev_transaction tr, ev_table_change tc'#13 +
    'WHERE'#13 +
    '  tc.ev_transaction_nbr = tr.nbr AND'#13 +
    '  tr.commit_time >= :date_b'#13 +
    'GROUP BY 1',
    [GetDBNameByType(ADBType)]);

  Q.Param['date_b'] := DateOf(ABeginDate);
  Q.Execute;
  Changes := Q.Result;

  // Build audit data
  Changes.First;
  while not Changes.Eof do
  begin
    Res.AppendRecord([Tables.Value[Changes.Fields[0].AsString], Changes.Fields[1].AsDateTime]);
    Changes.Next;
  end;

  Result := Res;
end;

function TevAuditData.GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
const
  ResStruct: array [0..4] of TDSFieldDef = (
    (FieldName: 'Table_Name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'Nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'Creation_Date';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'User_Id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'User_Name';  DataType: ftString;  Size: 30;  Required: False));

  procedure ProcessDB(const ADBType: TevDBType; const ATables: String);
  var
    Q: IevQuery;
  begin
    if ATables = '' then
      Exit;
      
    Q := TevQuery.CreateFmt('{InitialCreationNBRAudit<%s,"%s">}', [GetDBNameByType(ADBType), ATables]);

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      Result.AppendRecord([
        Q.Result.Fields[0].AsString,
        Q.Result.Fields[1].AsInteger,
        Q.Result.Fields[2].AsDateTime,
        Q.Result.Fields[3].AsInteger,
        GetUserName(Q.Result.Fields[3].AsInteger)]);

      Q.Result.Next;
    end;
  end;

var
  L: IisStringList;
  SystemTables, SBTables, ClientTables, s: String;
  i: Integer;
  DBType: TevDBType;
begin
  // Initialize Result
  Result := TevDataSet.Create(ResStruct);
  Result.LogChanges := False;

  SystemTables := '';
  SBTables := '';
  ClientTables := '';
  L := TisStringList.Create;
  L.CommaText := ATables;

  for i := 0 to L.Count - 1 do
  begin
     s := AnsiUpperCase(Trim(L[i]));

     DBType := GetDBTypeByTableName(s);
     case DBType of
       dbtSystem: AddStrValue(SystemTables, '''' + s + '''', ',');
       dbtBureau: AddStrValue(SBTables, '''' + s + '''', ',');
       dbtClient: AddStrValue(ClientTables, '''' + s + '''', ',');
     end;
  end;

  ProcessDB(dbtSystem, SystemTables);
  ProcessDB(dbtBureau, SBTables);
  ProcessDB(dbtClient, ClientTables);

  Result.First;
end;

function TevAuditData.GetRecordAudit(const ATable: String;
  const AFields: TisCommaDilimitedString; const ANbr: Integer;
  const ABeginDate: TDateTime): IevDataSet;
const
  ResStruct: array [0..9] of TDSFieldDef = (
    (FieldName: 'change_date';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'user_name';  DataType: ftString;  Size: 30;  Required: False),
    (FieldName: 'change_type';  DataType: ftString;  Size: 1;  Required: False),
    (FieldName: 'field_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'old_value';  DataType: ftString;  Size: 512;  Required: False),
    (FieldName: 'new_value';  DataType: ftString;  Size: 512;  Required: False),
    (FieldName: 'old_value_text';  DataType: ftString;  Size: 512;  Required: False),
    (FieldName: 'new_value_text';  DataType: ftString;  Size: 512;  Required: False),
    (FieldName: 'nbr';  DataType: ftInteger;  Size: 0;  Required: False));

var
  Q: IevQuery;
  CurrentPoint: IevDataSet;
  Res, Changes: IevDataSet;
  FilterFields, NonVerFields, VerFields: IisStringList;
  RefBlobChanges: IisParamsCollection;
  TblClass: TddTableClass;
  i, n, last_nbr, last_table_change_nbr: Integer;
  sql, s, fld_name: String;
  FieldIndexMap, BlobFields: IisListOfValues;
  last_change_date, change_date: TDateTime;
  field_name_index: Integer;
  ValueConverter: IevFieldValueToTextConverter;
  UserName: String;
  Val: Variant;
  BlobFldInfos: TddReferencesDesc;
  Item: IisNamedValue;
  Fld: TField;
  BlobChDS: IevDataSet;
  SecuredFields: IisStringList;

  procedure RemoveFilteredFields(const AList: IisStringList);
  var
    i: Integer;
  begin
    for i := AList.Count - 1 downto 0 do
      if (FilterFields.Count > 0) and (FilterFields.IndexOf(AList[i]) = -1) or
         TevDataChangeManager.IsFieldHidden(ATable, AList[i]) then
        AList.Delete(i);
  end;

  function GetBlobRefTableChanges(const ABlobRefTable, ARefField: String; const ANbr, ARefTableNbr: Integer): IevDataSet;
  var
    LV: IisListOfValues;
  begin
    LV := RefBlobChanges.ParamsByName(ARefField + ' ' + IntToStr(ARefTableNbr));
    if Assigned(LV) then
    begin
      Result := IInterface(LV.Value['D']) as IevDataSet;
      Exit;
    end;

    // Recursion
    Result := GetRecordAudit(ABlobRefTable, 'DATA', ARefTableNbr, BeginOfTime);
    Result.IndexFieldNames := 'change_date';

    LV := RefBlobChanges.AddParams(ARefField + ' ' + IntToStr(ARefTableNbr));
    LV.AddValue('D', Result);
    LV.AddValue('#', ANbr);
  end;

begin
  // Initialize Result
  Res := TevDataSet.Create(ResStruct);
  Res.LogChanges := False;

  // Separate versioned and non-versioned fields
  TblClass := GetddTableClassByName(ATable);
  Assert(Assigned(TblClass));

  FilterFields := TisStringList.CreateUnique();
  FilterFields.CommaText := AFields;

  NonVerFields := TblClass.GetFieldNameList(fmNonVersioned);
  RemoveFilteredFields(NonVerFields);

  if TblClass.IsVersionedTable then
  begin
    VerFields := TblClass.GetFieldNameList(fmVersioned);
    RemoveFilteredFields(VerFields);
  end;

  if NonVerFields.Count = 0 then
    Exit;

  SecuredFields := GetSecuredFields;

  // Prepare current non-versioned values dataset
  sql := Format('SELECT %s_nbr, cast(1 as SMALLINT), %s FROM %s WHERE {AsOfDate<%s>}', [ATable, NonVerFields.CommaText, ATable, ATable]);
  if ANbr <> 0 then
    sql := sql + Format(' AND %s_nbr = %d', [ATable, ANbr]);
  Q := TevQuery.Create(sql);
  Q.Param['StatusDate'] := BeginOfTime;
  Q.LoadBlobs := False;
  CurrentPoint := Q.Result;
  CurrentPoint.LogChanges := False;
  CurrentPoint.IndexFieldNames := CurrentPoint.Fields[0].FieldName;

  // Count rec_version records for tracking initial insert and final delete operations
  if TblClass.IsVersionedTable then
  begin
    sql := Format('SELECT COUNT(*) cnt, %s_nbr nbr FROM %s', [ATable, ATable]);
    if ANbr <> 0 then
      sql := sql + Format(' WHERE %s_nbr = %d', [ATable, ANbr]);
    sql := sql +' GROUP BY 2';
    Q := TevQuery.Create(sql);
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      Assert(CurrentPoint.FindKey([Q.Result.Fields[1].AsInteger]));
      CurrentPoint.Edit;
      CurrentPoint.Fields[1].AsInteger := Q.Result.Fields[0].AsInteger;
      CurrentPoint.Post;
      Q.Result.Next;
    end;
  end;

  // Set BLOBs values to NBR
  for i := 0 to CurrentPoint.FieldCount - 1 do
    if CurrentPoint.Fields[i].IsBlob and not CurrentPoint.Fields[i].IsNull then
    begin
      CurrentPoint.Edit;
      CurrentPoint.Fields[i].AsString := CurrentPoint.Fields[0].AsString;
      CurrentPoint.Post;
    end;

  // Collect blob reference fields
  BlobFields := TisListOfValues.Create;
  BlobFldInfos := TblClass.GetRefBlobFieldList;
  for i := 0 to High(BlobFldInfos) do
  begin
    s := BlobFldInfos[i].SrcFields[0];
    if (FilterFields.Count = 0) or (FilterFields.IndexOf(s) <> -1) then
      BlobFields.AddValue(s, BlobFldInfos[i].Table.GetTableName);
  end;

  if BlobFields.Count > 0 then
    RefBlobChanges := TisParamsCollection.Create;

  for i := 0 to CurrentPoint.FieldCount - 1 do
    if CurrentPoint.Fields[i].IsBlob then
      BlobFields.AddValue(CurrentPoint.Fields[i].FieldName, '');

  // Check reference blobs of initial point for changes
  CurrentPoint.First;
  while not CurrentPoint.Eof do
  begin
    for i := 0 to BlobFields.Count - 1 do
      if BlobFields[i].Value <> '' then
      begin
        Fld := CurrentPoint.FieldByName(BlobFields[i].Name);
        if Fld.Value <> Null then
          GetBlobRefTableChanges(BlobFields[i].Value, BlobFields[i].Name, CurrentPoint.Fields[0].AsInteger, Fld.Value);
      end;
    CurrentPoint.Next;
  end;

  // Build list of ev_field_nbr
  FieldIndexMap := TisListOfValues.Create;
  s := '';
  for i := 0 to NonVerFields.Count - 1 do
  begin
    n := TblClass.GetEvFieldNbr(NonVerFields[i]);
    AddStrValue(s, IntToStr(n), ',');
    FieldIndexMap.AddValue(IntToStr(n), i + 2);
  end;

  // Get changes non-versioned fields
  if ANbr <> 0 then
    Q := TevQuery.CreateFmt('{NonVersionFieldAudit<%s,%d,%s,"%s",%d>}',
      [GetDBNameByTable(ATable), TblClass.GetEvTableNbr, DateToUTC(DateOf(ABeginDate)), s, ANbr])
  else
    Q := TevQuery.CreateFmt('{NonVersionFieldAuditByNbr<%s,%d,%s,"%s">}',
      [GetDBNameByTable(ATable), TblClass.GetEvTableNbr, DateToUTC(DateOf(ABeginDate)), s]);
  Q.Execute;
  Changes := Q.Result;
  Changes.IndexFieldNames := 'rec_nbr;change_time;commit_nbr;table_change_nbr';

  // Build audit data for non-versioned fields
  last_nbr := 0;
  last_table_change_nbr := 0;
  last_change_date := EmptyDate;
  change_date := EmptyDate;
  Changes.Last;
  while not Changes.Bof do
  begin
    if last_nbr <> Changes.Fields[4].AsInteger then
    begin
      last_change_date := EmptyDate;
      change_date := EmptyDate;
      last_table_change_nbr := 0;
      last_nbr := Changes.Fields[4].AsInteger;
    end;

    if (last_table_change_nbr <> Changes.Fields[1].AsInteger) and (CompareDateTime(last_change_date, Changes.Fields[2].AsDateTime) = 0) then
      change_date := IncMilliSecond(change_date, -1) // Change date must be unique for later proper sorting
    else
    begin
      last_change_date := Changes.Fields[2].AsDateTime;
      change_date := Changes.Fields[2].AsDateTime;
    end;
    last_table_change_nbr := Changes.Fields[1].AsInteger;

    if Changes.Fields[6].IsNull then
      field_name_index := -1
    else
    begin
      field_name_index := FieldIndexMap.Value[Changes.Fields[6].AsString];
      if Changes.Fields[7].IsNull or not CurrentPoint.Fields[field_name_index].IsBlob then
      begin
        Val := Changes.Fields[7].Value;
        if (Val <> Null) and (CurrentPoint.Fields[field_name_index].DataType in [ftDate, ftTime, ftDateTime]) then
          Val := UTCToDateTime(Val, i);
      end
      else
        Val := -Changes.Fields[7].AsInteger;  // Inverting for ev_blob_change refs
    end;

    UserName := GetUserName(Changes.Fields[3].AsInteger);

    if CurrentPoint.Fields[0].AsInteger <> last_nbr then
      if not CurrentPoint.FindKey([last_nbr]) and (Changes.Fields[5].AsString = 'D') then
      begin
        //Assert((Changes.Fields[5].AsString = 'D'), Changes.Fields[5].AsString + ' ' + ATable + ' ' + IntToStr(ANbr));
        CurrentPoint.Append;
        CurrentPoint.Fields[0].AsInteger := last_nbr;
        CurrentPoint.Fields[1].AsInteger := iff(Changes.Fields[6].IsNull, 0, 1);
        CurrentPoint.Post;
      end;

    // at this moment CurrentPoint should have at least one record, otherwise user's restricted to see all the records from ATable
    if CurrentPoint.RecordCount > 0 then
    case Changes.Fields[5].AsString[1] of
      'I':
      begin
        CurrentPoint.Edit;
        CurrentPoint.Fields[1].AsInteger := CurrentPoint.Fields[1].AsInteger - 1;
        CurrentPoint.Post;

        if CurrentPoint.Fields[1].AsInteger = 0 then
        begin
          for i := 2 to CurrentPoint.Fields.Count - 1 do
            if (FilterFields.Count > 0) or (not CurrentPoint.Fields[i].IsNull and (Trim(CurrentPoint.Fields[i].AsString) <> '')) then
              Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName,
                                   'I', CurrentPoint.Fields[i].FieldName,
                                   Null, CurrentPoint.Fields[i].Value,
                                   Null, Null, last_nbr]);

          CurrentPoint.Delete;
        end;
      end;

      'U':
      begin
        if Changes.Fields[7].AsString <> CurrentPoint.Fields[field_name_index].AsString then
          Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName,
                               'U', CurrentPoint.Fields[field_name_index].FieldName,
                               Val, CurrentPoint.Fields[field_name_index].Value,
                               Null, Null, last_nbr]);

        CurrentPoint.Edit;
        CurrentPoint.Fields[field_name_index].Value := Val;
        CurrentPoint.Post;
      end;

      'D':
      begin
        if field_name_index = -1 then
        begin
          CurrentPoint.Edit;
          CurrentPoint.Fields[1].AsInteger := CurrentPoint.Fields[1].AsInteger + 1;
          CurrentPoint.Post;
        end
        else
        begin
          if CurrentPoint.Fields[1].AsInteger = 1 then
            Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName,
                                 'D', CurrentPoint.Fields[field_name_index].FieldName,
                                 Val, CurrentPoint.Fields[field_name_index].Value,
                                 Null, Null, last_nbr]);

          CurrentPoint.Edit;
          CurrentPoint.Fields[field_name_index].Value := Val;
          CurrentPoint.Post;
        end;
      end;
    end;

    Changes.Prior;
  end;

  // Fixup ref blob values
  if Assigned(RefBlobChanges) then
  begin
    Res.First;
    while not Res.Eof do
    begin
      fld_name := Res.Fields[4].AsString;
      Item := BlobFields.FindValue(fld_name);
      if Assigned(Item) and (String(Item.Value) <> '') then
      begin
        change_date := Res.Fields[0].AsDateTime;
        BlobChDS := nil;

        if not Res.Fields[5].IsNull and (Res.Fields[5].AsString <> '') and (Res.Fields[5].AsInteger > 0) then
        begin
          BlobChDS := GetBlobRefTableChanges(Item.Value, fld_name, Res.Fields[9].AsInteger, Res.Fields[5].AsInteger);
          if BlobChDS.FindNearest([change_date]) then
          begin
            if (BlobChDS.Fields[3].AsString = 'U') and (CompareDateTime(change_date, BlobChDS.Fields[0].AsDateTime) = LessThanValue) or (BlobChDS.Fields[3].AsString = 'I') then
              Val := BlobChDS.Fields[6].Value
            else
              Val := BlobChDS.Fields[5].Value;

            Res.Edit;
            Res.Fields[7].AsString := '(Blob) [' + Res.Fields[5].AsString + ']';
            Res.Fields[5].Value := Val;
            Res.Post;
          end;
        end;

        if not Res.Fields[6].IsNull and (Res.Fields[6].AsString <> '') and (Res.Fields[6].AsInteger > 0) then
        begin
          BlobChDS := GetBlobRefTableChanges(Item.Value, fld_name, Res.Fields[9].AsInteger, Res.Fields[6].AsInteger);
          if BlobChDS.FindNearest([change_date]) then
          begin
            if (BlobChDS.Fields[3].AsString = 'U') and (CompareDateTime(change_date, BlobChDS.Fields[0].AsDateTime) = LessThanValue) or (BlobChDS.Fields[3].AsString = 'I') then
              Val := BlobChDS.Fields[6].Value
            else
              Val := BlobChDS.Fields[5].Value;

            Res.Edit;
            Res.Fields[8].AsString := '(Blob) [' + Res.Fields[6].AsString + ']';
            Res.Fields[6].Value := Val;
            Res.Post;
          end;
        end;
      end;

      Res.Next;
    end;

    // Add UPDATE operations of RefBlobTable
    for i := 0 to RefBlobChanges.Count - 1 do
    begin
      s := RefBlobChanges.ParamName(i);
      fld_name := GetNextStrValue(s, ' ');
      BlobChDS := IInterface(RefBlobChanges[i].Value['D']) as IevDataSet;
      last_nbr := RefBlobChanges[i].Value['#'];
      BlobChDS.First;
      while not BlobChDS.Eof do
      begin
        if BlobChDS.Fields[3].Value = 'U' then
          if CompareDateTime(ABeginDate, BlobChDS.Fields[0].AsDateTime) = LessThanValue  then
          begin
            Res.AppendRecord(
              [BlobChDS.Fields[0].Value, BlobChDS.Fields[1].Value, BlobChDS.Fields[2].Value, BlobChDS.Fields[3].Value,
               fld_name, BlobChDS.Fields[5].Value, BlobChDS.Fields[6].Value,
               '(Blob) [' + s+ ']', '(Blob) [' + s + ']',
               last_nbr]);
          end;
        BlobChDS.Next;
      end;
    end;
  end;

  // Convert values to text
  ValueConverter := TevFieldValueToTextConverter.Create;
  Res.First;
  while not Res.Eof do
  begin
    Res.Edit;
    Item := BlobFields.FindValue(Res.Fields[4].AsString);
    if Assigned(Item) then
    begin
      if Item.Value = '' then
      begin
        if not Res.Fields[5].IsNull and (Res.Fields[5].AsString <> '') then
          Res.Fields[7].AsString := '(Blob)';
        if not Res.Fields[6].IsNull and (Res.Fields[6].AsString <> '') then
          Res.Fields[8].AsString := '(Blob)';
      end;
    end
    else
    begin
      if SecuredFields.IndexOf(ATable + '.' + Res.Fields[4].AsString) = -1 then
      begin
        Res.Fields[7].AsString := ValueConverter.Convert(ATable, Res.Fields[4].AsString, Res.Fields[5].Value, Trunc(Res.Fields[0].AsDateTime), True);
        Res.Fields[8].AsString := ValueConverter.Convert(ATable, Res.Fields[4].AsString, Res.Fields[6].Value, Trunc(Res.Fields[0].AsDateTime), True);
      end
      else
      begin
        if not Res.Fields[5].IsNull then
        begin
          Res.Fields[5].Value := '*****';
          Res.Fields[7].Value := '*****';
        end;
        if not Res.Fields[6].IsNull then
        begin
          Res.Fields[6].Value := '*****';
          Res.Fields[8].Value := '*****';
        end;
      end;
    end;
    Res.Post;

    Res.Next;
  end;

  Result := Res;
end;

function TevAuditData.GetTableAudit(const ATable: String;
  const ABeginDate: TDateTime): IevDataSet;
const
  ResStruct: array [0..5] of TDSFieldDef = (
    (FieldName: 'change_date';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'user_name';  DataType: ftString;  Size: 30;  Required: False),
    (FieldName: 'change_type';  DataType: ftString;  Size: 1;  Required: False),
    (FieldName: 'text_id';  DataType: ftString;  Size: 512;  Required: False),
    (FieldName: 'nbr';  DataType: ftInteger;  Size: 0;  Required: False));

var
  Q: IevQuery;
  CurrentPoint: IevDataSet;
  Res, Changes: IevDataSet;
  TblClass: TddTableClass;
  last_change_date, change_date: TDateTime;
  UserName: String;
  bNbrFound: Boolean;
begin
  TblClass := GetddTableClassByName(ATable);
  Assert(Assigned(TblClass));

  // Initialize Result
  Res := TevDataSet.Create(ResStruct);
  Res.LogChanges := False;

  // Prepare current non-versioned values dataset
  Q := TevQuery.CreateFmt('SELECT %s_nbr nbr, COUNT(*) ver_count FROM %s GROUP BY 1', [ATable, ATable]);
  CurrentPoint := Q.Result;
  CurrentPoint.LogChanges := False;
  CurrentPoint.IndexFieldNames := 'nbr';

  // Get changes non-versioned fields
  Q := TevQuery.CreateFmt(
    '/*%s*/'#13 +
    'SELECT tc.nbr, tr.commit_time, tc.record_nbr, tr.user_id, tc.change_type'#13 +
    'FROM ev_transaction tr, ev_table_change tc'#13 +
    'WHERE'#13 +
    '  tc.ev_transaction_nbr = tr.nbr AND'#13 +
    '  tc.ev_table_nbr = :table_nbr AND'#13 +
    '  tr.commit_time >= :date_b'#13 +
    'ORDER BY 3, 2, 1',
    [GetDBNameByTable(ATable)]);

  Q.Param['table_nbr'] := TblClass.GetEvTableNbr;
  Q.Param['date_b'] := DateOf(ABeginDate);
  Q.Execute;
  Changes := Q.Result;

  // Build audit data
  last_change_date := EmptyDate;
  change_date := EmptyDate;
  Changes.Last;
  while not Changes.Bof do
  begin
    if CompareDateTime(last_change_date, Changes.Fields[1].AsDateTime) = 0 then
      change_date := IncMilliSecond(change_date, -1) // Change date must be unique for later proper sorting
    else
    begin
      last_change_date := Changes.Fields[1].AsDateTime;
      change_date := Changes.Fields[1].AsDateTime;
    end;

    UserName := GetUserName(Changes.Fields[3].AsInteger);

    bNbrFound := CurrentPoint.FindKey([Changes.Fields[2].AsInteger]);

    case Changes.Fields[4].AsString[1] of
      'I':
      begin
        Assert(bNbrFound);

        CurrentPoint.Edit;
        CurrentPoint.Fields[1].AsInteger := CurrentPoint.Fields[1].AsInteger - 1;
        CurrentPoint.Post;

        if CurrentPoint.Fields[1].AsInteger = 0 then
        begin
          Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName, 'I', Null, Changes.Fields[2].AsInteger]);
          CurrentPoint.Delete;
        end
        else
          Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName, 'N', Null, Changes.Fields[2].AsInteger]);
      end;

      'U':
      begin
        Assert(bNbrFound);
        Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName, 'U', Null, Changes.Fields[2].AsInteger]);
      end;

      'D':
      begin
        if not bNbrFound then
        begin
          // Restore deleted record
          CurrentPoint.AppendRecord([Changes.Fields[2].AsInteger, 1]);
          Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName, 'D', Null, Changes.Fields[2].AsInteger]);
        end
        else
        begin
          CurrentPoint.Edit;
          CurrentPoint.Fields[1].AsInteger := CurrentPoint.Fields[1].AsInteger + 1;
          CurrentPoint.Post;
          Res.AppendRecord([change_date, Changes.Fields[3].AsInteger, UserName, 'R', Null, Changes.Fields[2].AsInteger]);
        end;
      end;
    end;

    Changes.Prior;
  end;

  Result := Res;
end;

function TevAuditData.GetUserName(const AUserID: Integer): String;
var
  Q: IevQuery;
  DS: IevDataSet;
begin
  if FAllowToResolveUserName then
  begin
    if AUserId = sGuestInternalNbr then
      Result := sGuestUserName
    else
    begin
      if AUserID < 0 then
      begin
        if not Assigned(FEEUsers) then
        begin
          ctx_DBAccess.DisableSecurity;
          try
            Q := TevQuery.Create('SELECT -e.ee_nbr nbr, p.first_name || '' '' || p.last_name user_name FROM ee e, cl_person p ' +
                                 'WHERE e.cl_person_nbr = p.cl_person_nbr AND {AsOfNow<e>} AND {AsOfNow<p>}');
            FEEUsers := Q.Result;
          finally
            ctx_DBAccess.EnableSecurity;
          end;
          FEEUsers.IndexFieldNames := 'nbr';
        end;
        DS := FEEUsers;
      end
      else
      begin
        if not Assigned(FSBUsers) then
        begin
          ctx_DBAccess.DisableSecurity;
          try
            Q := TevQuery.Create('SELECT sb_user_nbr nbr, first_name || '' '' || last_name user_name FROM sb_user WHERE {AsOfNow<sb_user>}');
            FSBUsers := Q.Result;
          finally
            ctx_DBAccess.EnableSecurity;
          end;
          FSBUsers.IndexFieldNames := 'nbr';
        end;
        DS := FSBUsers;
      end;

      if DS.FindKey([AUserId]) then
        Result := DS['user_name']
      else
        Result := 'Unknown User';
    end;
  end
  else
    Result := 'User';

  Result := Result + ' [' + IntToStr(AUserId) + ']';
end;

function TevAuditData.GetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ABeginDate: TDateTime): IisListOfValues;
const
  NbrInd = 0;
  BeginDateInd = 1;
  EndDateInd = 2;
  ValueInd = 3;
  TextValueInd = 4;
var
  Res: IisListOfValues; // List of IisParamCollection
  Data: IevDataSet;
  ValueConverter: IevFieldValueToTextConverter;
  ResultDSFldsInfo: array of TDSFieldDef;
  current_nbr: Integer;
  bOneNbrMode: Boolean;
  NbrData: IisParamsCollection;

  procedure SetDataIndex(const AIndexName: String);
  var
    s: String;
  begin
    s := 'nbr=' + IntToStr(current_nbr);
    if (Data.vclDataSet.IndexName <> AIndexName) or (Data.Filter <> s) then
    begin
      Data.vclDataSet.IndexName := AIndexName;
      Data.Filtered := False;
      Data.Filter := s;
      Data.Filtered := True;
    end;
  end;

  procedure AddDataToResult(const AChangeTime: TDateTime; const AUserId: Integer);
  var
    Item: IisListOfValues;
    FinalData: IevDataSet;
    val: Variant;
    d: TDateTime;
    LV: IisNamedValue;
  begin
    FinalData := TevDataSet.Create(ResultDSFldsInfo);
    FinalData.vclDataSet.LogChanges := False;

    // Keep only unique values
    SetDataIndex('effective_date');

    Data.First;
    val := Null;
    while not Data.Eof do
    begin
      if (Data.RecNo = 1) or not VarSameValue(val, Data.Fields[3].Value) then
      begin
        val := Data.Fields[3].Value;
        FinalData.AppendRecord([Data.Fields[4].AsInteger, Data.Fields[1].AsDateTime, Data.Fields[2].AsDateTime, val]);
      end
      else if Data.RecNo = Data.RecordCount then
      begin
        FinalData.Last;
        FinalData.Edit;
        FinalData.Fields[EndDateInd].AsDateTime := Data.Fields[2].AsDateTime;
        FinalData.Post;
      end;

      Data.Next;
    end;

    // Reconstruct effective_until field
    FinalData.Last;
    d := FinalData.Fields[EndDateInd].AsDateTime;
    while not FinalData.Bof do
    begin
      FinalData.Edit;
      FinalData.Fields[EndDateInd].AsDateTime := IncDay(d, -1);
      FinalData.Post;
      d := FinalData.Fields[BeginDateInd].AsDateTime;

      FinalData.Prior;
    end;

    // Skip a duplicate
    LV := Res.FindValue(IntToStr(current_nbr));
    if not Assigned(LV) then
    begin
      NbrData := TisParamsCollection.Create;
      Res.AddValue(IntToStr(current_nbr), NbrData);
    end
    else
      NbrData := IInterface(LV.Value) as IisParamsCollection;

    if (NbrData.Count = 0) or
       (CalcMD5(FinalData.Data.RealStream) <> CalcMD5((IInterface(NbrData[NbrData.Count - 1].Value['Data']) as IevDataSet).Data.RealStream))
       then
    begin
      FinalData.First;
      Item := NbrData.AddParams(IntToStr(NbrData.Count));
      Item.AddValue('ChangeTime', AChangeTime);
      Item.AddValue('UserID', 0);
      Item.AddValue('UserName', '');
      Item.AddValue('Nbr', current_nbr);
      Item.AddValue('Data', FinalData);

      if NbrData.Count > 1 then
      begin
        Item := NbrData[NbrData.Count - 2];
        Item.AddValue('ChangeTime', IncMilliSecond(AChangeTime, 1));
        Item.AddValue('UserID', AUserId);
        Item.AddValue('UserName', GetUserName(AUserId));
      end;
    end;
  end;

var
  Q: IevQuery;
  Changes: IevDataSet;
  rec_id: Integer;
  change_time, ed, eu: TDateTime;
  val: Variant;
  commit_nbr, table_change_nbr, i, j: Integer;
  user_id: Integer;
  tbl: TDataDictTable;
  fld: TDataDictField;
  fk: TDataDictForeignKey;
  b: Boolean;
  sql: String;

  procedure PopulateDataForMultipleNbrs;
  var
    sCond: String;
    L: IisIntegerList;
  begin
    if ABeginDate > EncodeDate(1997, 1, 1) then   // Before this date changes were impossible as Evo did not exist
    begin
      L := TisIntegerList.Create;
      Changes.IndexFieldNames := 'rec_nbr:D';
      Changes.First;
      while not Changes.Eof do
      begin
        L.Add(Changes.Fields[5].AsInteger);
        Changes.Next;
      end;

      sCond := BuildINsqlStatement(ATable + '_nbr', L);
    end
    else
      sCond := '1 = 1';

    sql := Format('SELECT rec_version, effective_date, effective_until, %s val, %s_nbr nbr FROM %s WHERE %s', [AField, ATable, ATable, sCond]);
    Q := TevQuery.Create(sql);
    Q.Execute;
    Data := Q.Result
  end;

  procedure ClearCurrentNbrEmptyData;
  begin
    if not bOneNbrMode and (current_nbr <> 0) then
    begin
      // We do not need current state if there are no changes
      i := Res.IndexOf(IntToStr(current_nbr));
      if (IInterface(Res[i].Value) as IisParamsCollection).Count <= 1 then
        Res.Delete(i);
    end;
  end;

begin
  Res := TisListOfValues.Create;  // List of IisParamCollection

  if TevDataChangeManager.IsFieldHidden(ATable, AField) then
  begin
    Result := Res;
    Exit;
  end;

  bOneNbrMode := ANbr > 0;

  tbl := EvoDataDictionary.Tables.TableByName(ATable);
  fld := tbl.Fields.FieldByName(AField);
  fk := tbl.ForeignKeys.ForeignKeyByField(fld);
  if Assigned(fk) or (fld.FieldValues.Count > 0) then
    ValueConverter := TevFieldValueToTextConverter.Create;


  // Retrieve changes for all transactions
  if bOneNbrMode then
    sql := Format('{VersionFieldAsOf<%s, %s, %s, %d>}', [GetDBNameByTable(ATable), ATable, AField, ANbr])
  else
    sql := Format('{VersionFieldAsOfByNbr<%s, %s, %s, %s>}', [GetDBNameByTable(ATable), ATable, AField, DateToUTC(ABeginDate)]);

  Q := TevQuery.Create(sql);
  Q.Execute;
  Changes := Q.Result;

  if Changes.RecordCount = 0 then
    Exit;


  // Initial record state

  if bOneNbrMode then // audit of specific record from Evolution screen, was erroneously commented out on 105183
  begin
    sql := Format('SELECT rec_version, effective_date, effective_until, %s val, %s_nbr nbr FROM %s WHERE %s_nbr = %d', [AField, ATable, ATable, ATable, ANbr]);
    Q := TevQuery.Create(sql);
    Q.Execute;
    Data := Q.Result;
  end
  else
    PopulateDataForMultipleNbrs;

//  if Double(Now()) < 0 then // never-condition; but prevent linker from eliminating function call
//  begin                     // so we can use it at debug
//    EvAudithelper.SaveDatasetAsCSV(Changes.vclDataSet,'C:\Temp\changes.csv');
//  end;

  Data.vclDataSet.LogChanges := False;
  Data.vclDataSet.IndexDefs.Add('rec_version', 'rec_version', []);
  Data.vclDataSet.IndexDefs.Add('effective_date', 'nbr;effective_date', []);

  SetLength(ResultDSFldsInfo, 4);
  ResultDSFldsInfo[0].FieldName := 'nbr';
  ResultDSFldsInfo[0].DataType := ftInteger;
  ResultDSFldsInfo[0].Size := 0;
  ResultDSFldsInfo[0].Required := False;
  ResultDSFldsInfo[1].FieldName := 'begin_date';
  ResultDSFldsInfo[1].DataType := ftDate;
  ResultDSFldsInfo[1].Size := 0;
  ResultDSFldsInfo[1].Required := False;
  ResultDSFldsInfo[2].FieldName := 'end_date';
  ResultDSFldsInfo[2].DataType := ftDate;
  ResultDSFldsInfo[2].Size := 0;
  ResultDSFldsInfo[2].Required := False;
  ResultDSFldsInfo[3].FieldName := 'value';
  ResultDSFldsInfo[3].DataType := Data.Fields[3].DataType;
  ResultDSFldsInfo[3].Size := Data.Fields[3].Size;
  ResultDSFldsInfo[3].Required := False;
  SetLength(ResultDSFldsInfo, 5);
  ResultDSFldsInfo[4].FieldName := 'text_value';
  ResultDSFldsInfo[4].DataType := ftString;
  ResultDSFldsInfo[4].Size := 64;
  ResultDSFldsInfo[4].Required := False;

  Changes.IndexFieldNames := 'rec_nbr;commit_nbr;table_change_nbr';
  Changes.Last;

  // Rewind all changes and apply over current state
  current_nbr := 0;
  commit_nbr := Changes.FieldByName('commit_nbr').AsInteger;
  change_time := Changes.FieldByName('change_time').AsDateTime;
  user_id := Changes.FieldByName('user_id').AsInteger;
  while not Changes.Bof do
  begin
    if (commit_nbr <> Changes.FieldByName('commit_nbr').AsInteger) or
       (current_nbr <> 0) and (current_nbr <> Changes.FieldByName('rec_nbr').AsInteger) then
    begin
      AddDataToResult(change_time, user_id);
      commit_nbr := Changes.FieldByName('commit_nbr').AsInteger;
      change_time := Changes.FieldByName('change_time').AsDateTime;
      user_id := Changes.FieldByName('user_id').AsInteger;
    end;

    if (current_nbr <> Changes.FieldByName('rec_nbr').AsInteger) then
    begin
      ClearCurrentNbrEmptyData;

      current_nbr := Changes.FieldByName('rec_nbr').AsInteger;

      if Res.IndexOf(IntToStr(current_nbr)) = -1 then
        AddDataToResult(EmptyDate, 0);
    end;

    SetDataIndex('rec_version');

    rec_id := Changes.FieldByName('rec_version').AsInteger;
    val := Changes.FieldByName('old_value').Value;

    case Changes.FieldByName('change_type').AsString[1] of
      'I':
      begin
//        Assert(Data.FindKey([rec_id]));
        if Data.FindKey([rec_id]) then
        begin
          ed := Data.FieldByName('effective_date').AsDateTime;
          eu := Data.FieldByName('effective_until').AsDateTime;

          SetDataIndex('effective_date');

          Assert(Data.FindKey([current_nbr, ed]));
          if Data.RecNo = Data.RecordCount then
          begin
            Data.Prior;
            Data.Edit;
            Data.FieldByName('effective_until').AsDateTime := eu;
            Data.Post;
          end;

          SetDataIndex('rec_version');
          Assert(Data.FindKey([rec_id]));

          Data.Delete;
        end;
        Changes.Prior;
      end;

      'U':
      begin
//        Assert(Data.FindKey([rec_id]));
        if Data.FindKey([rec_id]) then
        begin
          Data.Edit;
          case Changes.FieldByName('field_kind').AsString[1] of
            'E': Data.FieldByName('effective_date').AsDateTime := UTCToDate(val);
            'U': Data.FieldByName('effective_until').AsDateTime := UTCToDate(val);
            'F': begin
              if (Data.FieldByName('val').DataType = ftDateTime) and not VarIsNull(val) then
                val := Copy(val, 1, 19); // cut off milliseconds "YYYY-MM-DD HH:NN:SS.ZZZZ -> "YYYY-MM-DD HH:NN:SS"
              Data.FieldByName('val').Value := val;
            end
          else
            Assert(False);
          end;
          Data.Post;
        end;
        Changes.Prior;
      end;

      'D':
      begin
        Assert(not Data.FindKey([rec_id])); // record should not exist

        Data.Append;
        Data.FieldByName('rec_version').AsInteger := rec_id;
        Data.FieldByName('effective_date').AsDateTime := BeginOfTime;
        Data.FieldByName('effective_until').AsDateTime := EndOfTime;
        Data.FieldByName('nbr').AsInteger := current_nbr;

        if Changes.FieldByName('field_kind').IsNull then
        begin
          // This is deletion of record with default values (Null, BeginOfTime, EndOfTime)
          b := True;
          Changes.Prior;
        end
        else
        begin
          // Apply it as one operation because it may require to copy values form previous effective period
          b := False;
          table_change_nbr := Changes.FieldByName('table_change_nbr').AsInteger;
          while (not Changes.Bof) and (table_change_nbr = Changes.FieldByName('table_change_nbr').AsInteger) do
          begin
            val := Changes.FieldByName('old_value').Value;
            case Changes.FieldByName('field_kind').AsString[1] of
              'E': Data.FieldByName('effective_date').AsDateTime := UTCToDate(val);
              'U': Data.FieldByName('effective_until').AsDateTime := UTCToDate(val);
            else
              b := True;
              Data.FieldByName('val').Value := val;
            end;

            Changes.Prior;
          end;
        end;

        Data.Post;

        ed := Data.FieldByName('effective_date').AsDateTime;
        if not b and (ed <> BeginOfTime) then
        begin
          // Copy field value from previous effective period
          SetDataIndex('effective_date');
          Assert(Data.FindKey([current_nbr, ed]));
          Data.Prior;
          val := Data.FieldByName('val').Value;
          SetDataIndex('rec_version');
          Assert(Data.FindKey([rec_id]));
          Data.Edit;
          Data.FieldByName('val').Value := val;
          Data.Post;
        end;
      end;

      else
        Assert(False);
    end;
  end;

  AddDataToResult(change_time, user_id);
  ClearCurrentNbrEmptyData;

  // Convert values to text
  if Assigned(ValueConverter) then
  begin
    for i := 0 to Res.Count - 1 do
    begin
      NbrData := IInterface(Res[i].Value) as IisParamsCollection;

      for j := 0 to NbrData.Count - 1 do
      begin
        Data := IInterface(NbrData[j].Value['Data']) as IevDataSet;
        Data.First;
        while not Data.Eof do
        begin
          Data.Edit;
          Data.Fields[4].AsString := ValueConverter.Convert(ATable, AField, Data.Fields[3].Value, Trunc(Data.Fields[2].AsDateTime), True);
          Data.Post;

          Data.Next;
        end;
      end;
    end;
  end;

  Result := Res;
end;


function TevAuditData.GetVersionFieldAuditChange(const ATable, AField: String;   const ABeginDate: TDateTime): IevDataset;
var
  AuditInfoRecord: TRecFieldNumbersForAudit;
begin
  AuditInfoRecord := EvAuditHelper.GetFieldNumbersInRecForAudit(ATable, AField);
  Result := EvAuditHelper.GetVersionFieldAuditChangeForList( AuditInfoRecord, ABeginDate);
end;

function TevDBAccess.GetDBServersInfo: IisParamsCollection;
begin
  Result := FDBServer.GetDBServersInfo;
end;

function TevDBAccess.GetInitialCreationNBRAudit(const ATables: TisCommaDilimitedString): IevDataSet;
var
  A: IevAuditData;
begin
  A := TevAuditData.Create;
  Result := A.GetInitialCreationNBRAudit(ATables);
end;

function TevDBAccess.CheckParamValue(const AValue: Variant): Variant;
begin
  Result := AValue;
  if VarIsStr(Result) then
    if TrimRight(Result) = '' then
      Result := BLANK_TAG; // Stupid legacy! This is for "not null" fields which contain empty text
end;

procedure TevDBAccess.UndeleteClientDB(const AClientID: Integer);
begin
  Assert(not InTransaction);

  FDBServer.OpenClient(AClientID);
  try
    FDBServer.StartTransaction([dbtClient]);
    try
      FDBServer.ExecQuery('UPDATE cl SET effective_until = ''12/31/9999'' WHERE rec_version IN (SELECT FIRST 1 rec_version FROM cl ORDER BY effective_until DESC)', nil, nil, False);
      FDBServer.Commit;
    except
      FDBServer.Rollback;
      raise;
    end;
  finally
    FDBServer.OpenClient(FCurrentClientNbr);
  end;
end;

function TevDBAccess.PatchDB(const ADBName, AAppVersion: String; AllowDowngrade: Boolean): Boolean;
begin
  Result := FDBServer.PatchDB(ADBName, AAppVersion, AllowDowngrade);
end;


function TevDBAccess.CustomScript(const ADBName, AScript: String): Boolean;
begin
  Result := FDBServer.CustomScriptDB(ADBName, AScript);
end;


initialization
  TempTables := TevTempTables.Create;
  Mainboard.ModuleRegister.RegisterModule(@GetDBAccess, IevDBAccess, 'DB Access');

end.

