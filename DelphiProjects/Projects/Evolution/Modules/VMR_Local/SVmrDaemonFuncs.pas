// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrDaemonFuncs;

interface

uses evTypes, EvClientDataSet;

type
  TVmrNbrArray = array of Integer;

procedure ProcessRelease(const TopBoxNbr: Integer;ReleaseType:TVmrReleaseTypeSet);
procedure ProcessRevert(const TopBoxNbr: Integer);
function  CheckScannedBarcode(const BarCode: string; out ExpectedBarcode, LastScannedBarcode: string): Boolean; // True if scan accepted

implementation

uses EvUtils, EvBasicUtils, SysUtils,  Classes, Variants,
  SVmrClasses, SServiceE, SServiceUSPS, SReportSettings, EvConsts,
  EvDataAccessComponents, EvContext, EvExceptions,sDataStructure;

{ TVmrDaemonPart }

function CheckScannedBarcode(const BarCode: string;
  out ExpectedBarcode, LastScannedBarcode: string): Boolean;
begin
  Result := True;
end;

procedure ProcessRevert(const TopBoxNbr: Integer);
var
  cdBoxes: TevClientDataSet;
  aBoxes: array of TVmrMediaType;
  aMethod: TVmrDeliveryMethod;
  sLocation: string;
  lMethodMediaTypes: TVmrMediaTypeClassArray;
  i: Integer;
  lChainedMailBoxes: TStringList;

  procedure ClearRemotePrintTime(const aSbNbr : integer);
  begin
    if CheckRemoteVMRActive then
    begin
      DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired('SB_MAIL_BOX_NBR = ' + inttostr(aSbNbr));
      DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.First;
      while not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Eof do
      begin
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Edit;
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.FieldByName('USERSIDE_PRINTED').Clear;
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Post;
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
    end;
  end;
begin
  lMethodMediaTypes := nil;
  ctx_DataAccess.SB_CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GenericSelectWithCondition') do
  begin
    SetMacro('TABLENAME', 'LIST_TOP_MAIL_BOXES2');
    SetMacro('COLUMNS', 'NBR');
    SetMacro('CONDITION', 'UP_NBR=:RecordNbr');
    SetParam('RecordNbr', TopBoxNbr);
    ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.SB_CUSTOM_VIEW]);
  end;
  lChainedMailBoxes := ctx_DataAccess.SB_CUSTOM_VIEW.GetFieldValueList('NBR', True);
  cdBoxes := TevClientDataSet.Create(nil);
  try
    ctx_DataAccess.StartNestedTransaction([dbtBureau], 'SB_VMR');
    try
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('TABLENAME', 'SB_MAIL_BOX_OPTION');
        SetMacro('COLUMNS', 'OPTION_STRING_VALUE');
        SetMacro('CONDITION', 'SB_MAIL_BOX_NBR=:RecordNbr and OPTION_TAG=:OptionTag');
        SetParam('RecordNbr', TopBoxNbr);
        SetParam('OptionTag', VmrSbLocation);
        ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.OpenDataSets([ctx_DataAccess.SB_CUSTOM_VIEW]);
      end;
      if ctx_DataAccess.SB_CUSTOM_VIEW.RecordCount > 0 then
        sLocation := ConvertNull(ctx_DataAccess.SB_CUSTOM_VIEW['OPTION_STRING_VALUE'], '')
      else
        sLocation := '';
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;

      cdBoxes.ProviderName := 'SB_CUSTOM_PROV';
      cdBoxes.CustomProviderTableName := 'SB_MAIL_BOX';
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('COLUMNS', '*');
        SetMacro('TABLENAME', 'SB_MAIL_BOX');
        SetMacro('CONDITION', BuildInSqlStatement ('SB_MAIL_BOX_NBR', lChainedMailBoxes.CommaText));
        cdBoxes.DataRequest(AsVariant);
      end;
      ctx_DataAccess.OpenDataSets([cdBoxes]);
      Assert(cdBoxes.RecordCount > 0);
      cdBoxes.IndexFieldNames := 'UP_LEVEL_MAIL_BOX_NBR;SB_MAIL_BOX_NBR';
      cdBoxes.First;
      aMethod := InstanceSbDeliveryMethod(cdBoxes);
      aMethod.Location := sLocation;
      Assert(Assigned(aMethod));
      lMethodMediaTypes := aMethod.GetMediaClasses;
      SetLength(aBoxes, cdBoxes.RecordCount);
      try
        cdBoxes.Last;
        i := 0;
        while not cdBoxes.Bof do
        begin
          if not cdBoxes.FieldByName('RELEASED_TIME').IsNull then
          begin
            cdBoxes.Edit;
            cdBoxes.FieldByName('RELEASED_TIME').Clear;
            cdBoxes.Post;
            ClearRemotePrintTime(cdBoxes.FieldByName('SB_MAIL_BOX_NBR').asInteger);
          end;
          aBoxes[i] := InstanceSbMedia(cdBoxes);
          Assert(Assigned(aBoxes[i]));
          aMethod.RevertToUnreleased(aBoxes[i], cdBoxes);
          Inc(i);
          cdBoxes.Prior;
        end;
        ctx_DataAccess.PostDataSets([cdBoxes]);
      finally
        for i := 0 to High(aBoxes) do
          FreeAndNil(aBoxes[i]);
        FreeAndNil(aMethod);
      end;
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  finally
    cdBoxes.Free;
    lChainedMailBoxes.Free;
  end;
end;

procedure ProcessRelease(const TopBoxNbr: Integer;ReleaseType:TVmrReleaseTypeSet);
var
  cdBoxes: TevClientDataSet;
  aBoxes: array of TVmrMediaType;
  aMethod: TVmrDeliveryMethod;
  sLocation : string;
  lMethodMediaTypes: TVmrMediaTypeClassArray;
  i: Integer;
  lChainedMailBoxes: TStringList;
  function FindMediaType(const m: TVmrMediaType): Boolean;
  var
    j: Integer;
  begin
    Result := False;
    for j := 0 to High(lMethodMediaTypes) do
      if m is lMethodMediaTypes[j] then
      begin
        Result := True;
        Break;
      end;
  end;
begin
  ctx_DataAccess.SB_CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GenericSelectWithCondition') do
  begin
    SetMacro('TABLENAME', 'LIST_TOP_MAIL_BOXES2');
    SetMacro('COLUMNS', 'NBR');
    SetMacro('CONDITION', 'UP_NBR=:RecordNbr');
    SetParam('RecordNbr', TopBoxNbr);
    ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.SB_CUSTOM_VIEW]);
  end;
  lChainedMailBoxes := ctx_DataAccess.SB_CUSTOM_VIEW.GetFieldValueList('NBR', True);
  cdBoxes := TevClientDataSet.Create(nil);
  try
    ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('TABLENAME', 'SB_MAIL_BOX');
      SetMacro('COLUMNS', 'CL_NBR');
      SetMacro('CONDITION', 'SB_MAIL_BOX_NBR=:RecordNbr');
      SetParam('RecordNbr', TopBoxNbr);
      ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.OpenDataSets([ctx_DataAccess.SB_CUSTOM_VIEW]);
    end;
    ctx_DataAccess.OpenClient(ctx_DataAccess.SB_CUSTOM_VIEW['CL_NBR']);

    ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('TABLENAME', 'SB_MAIL_BOX_OPTION');
      SetMacro('COLUMNS', 'OPTION_STRING_VALUE');
      SetMacro('CONDITION', 'SB_MAIL_BOX_NBR=:RecordNbr and OPTION_TAG=:OptionTag');
      SetParam('RecordNbr', TopBoxNbr);
      SetParam('OptionTag', VmrSbLocation);
      ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.OpenDataSets([ctx_DataAccess.SB_CUSTOM_VIEW]);
    end;
    if ctx_DataAccess.SB_CUSTOM_VIEW.RecordCount > 0 then
      sLocation := ConvertNull(ctx_DataAccess.SB_CUSTOM_VIEW['OPTION_STRING_VALUE'], '')
    else
      sLocation := '';
    ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    cdBoxes.ProviderName := 'SB_CUSTOM_PROV';
    cdBoxes.CustomProviderTableName := 'SB_MAIL_BOX';
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', '*');
      SetMacro('TABLENAME', 'SB_MAIL_BOX');
      SetMacro('CONDITION', BuildInSqlStatement('SB_MAIL_BOX_NBR', lChainedMailBoxes.CommaText));
      cdBoxes.DataRequest(AsVariant);
    end;

    ctx_DataAccess.OpenDataSets([cdBoxes]);
    Assert(cdBoxes.RecordCount > 0);
    cdBoxes.IndexFieldNames := 'UP_LEVEL_MAIL_BOX_NBR;SB_MAIL_BOX_NBR';
    cdBoxes.First;
    aMethod := InstanceSbDeliveryMethod(cdBoxes);
    aMethod.Location := sLocation;
    aMethod.ReleaseType := ReleaseType;

    aMethod.PrepareToRelease;
    Assert(Assigned(aMethod));
    lMethodMediaTypes := aMethod.GetMediaClasses;
    SetLength(aBoxes, cdBoxes.RecordCount);
    try
      cdBoxes.Last;
      i := 0;
      while not cdBoxes.Bof do
      begin
        if (vrtPrinter in ReleaseType) then
        begin
          if cdBoxes.FieldByName('RELEASED_TIME').IsNull then
          begin
            cdBoxes.Edit;
            cdBoxes.FieldByName('RELEASED_TIME').AsDateTime := SysTime;
            cdBoxes.Post;
          end;
        end;
        aBoxes[i] := InstanceSbMedia(cdBoxes);
        Assert(Assigned(aBoxes[i]), 'Media type do not assigned for mail box '+ cdBoxes.fieldByname('Description').AsString);
        Assert(FindMediaType(aBoxes[i]), 'Media type is incompatible with delivery method for mail box '+ cdBoxes.fieldByname('Description').AsString);
        aMethod.AddToRelease(aBoxes[i], cdBoxes);
        Inc(i);
        cdBoxes.Prior;
      end;
      try
        aMethod.Release(cdBoxes);
      except
        //on E: ESendEmailError do //- ticket 104221 - queue-log all exceptions and problems, not only email-originated ( ESendEmailError generated only in very special cases)
        // originally created by  TVmrDeliveryMethod.Release [SVmrClasses.pas]
        on E: Exception do
        begin
          ctx_DataAccess.StartNestedTransaction([dbtBureau], 'SB_VMR');
          try
            aMethod.PostSentEmailContent(cdBoxes, false);
            ctx_DataAccess.CommitNestedTransaction;
          except
             ctx_DataAccess.RollbackNestedTransaction;
             raise;
          end;
          raise;
        end;
      end;
      ctx_DataAccess.StartNestedTransaction([dbtBureau], 'SB_VMR');
      try
        aMethod.FinishRelease(cdBoxes);
        ctx_DataAccess.PostDataSets([cdBoxes]);
        ctx_DataAccess.CommitNestedTransaction;
      except
         ctx_DataAccess.RollbackNestedTransaction;
         raise;
      end;
    finally
      for i := 0 to High(aBoxes) do
        FreeAndNil(aBoxes[i]);
      FreeAndNil(aMethod);
    end;
  finally
    cdBoxes.Free;
    lChainedMailBoxes.Free;
  end;
end;

end.

