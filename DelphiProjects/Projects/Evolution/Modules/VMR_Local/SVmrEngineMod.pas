// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrEngineMod;

interface

uses
  SysUtils, Variants, Controls, DB, Classes, Windows,  Forms,
  SSecurityInterface, SReportSettings, EvStreamUtils, EvCommonInterfaces, EvContext, EvMainboard,
  EvUtils, SDataStructure, ISUtils, ISErrorUtils,sFieldCodeValues, isBaseClasses, EvLegacy,
  SVmrClasses, EvConsts,  EvTypes, EvBasicUtils, SVmrDaemonFuncs, EvExceptions,
  EvDataset, isBasicUtils, EvDataAccessComponents, EvClientDataSet;

implementation

type
  TevVmrEngine = class(TisInterfacedObject, IevVmrEngine)
  private
    FChecksExtraInfo:IisStringList;
    function  InternalVmrPostProcessReportResult(const Res: TrwReportResults; const MarkShipped: Boolean): Boolean; overload;
    procedure SetChecksExtraInfo(const EInfo:IisStringList);
    function  GetChecksExtraInfo:IisStringList;
  protected
    procedure VmrPreProcessReportList(const RepList: TrwReportList);
    procedure VmrCalcExtraInfo(const RepList: TrwReportList);
    function  VmrPostProcessReportResult(var ms: IEvDualStream): Boolean; overload;
    function  VmrPostProcessReportResult(const Res: TrwReportResults): Boolean; overload;
    function  VmrStorePostProcessReportResult(const ms: IEvDualStream): Boolean; overload;
    function  VmrStorePostProcessReportResult(const Res: TrwReportResults): Boolean; overload;
    procedure ProcessRelease(const TopBoxNbr: Integer;ReleaseType:TVmrReleaseTypeSet);
    procedure ProcessRevert(const TopBoxNbr: Integer);
  public
  end;

type
  TSettingRecord = record
    ClNbr: Integer;
    VmrStatus: Variant;
    EeSettings: TevClientDataSet;
    DbdtSettings: TevClientDataSet;
    CoSettings: TevClientDataSet;
  end;

  PSettingRecord = ^TSettingRecord;
  TSettingArray = array of TSettingRecord;

  TVmrSettings = class
  private
    a: TSettingArray;
    function FindClientSettings(const ClNbr: Integer): PSettingRecord;
  public
    constructor Create;
    destructor Destroy; override;
    function CheckVmrActive(const ClNbr: Integer): Boolean;
    function FindEeSettings(const ClNbr: Integer): TevClientDataSet;
    function FindCoSettings(const ClNbr: Integer): TevClientDataSet;
  end;

function GetVmrEngine: IevVmrEngine;
begin
  if Context.License.VMR then
    Result := TevVmrEngine.Create
  else
    Result := nil;
end;

{ TVmrEngine }

function TevVmrEngine.VmrPostProcessReportResult(var ms: IEvDualStream): Boolean;
var
  Res: TrwReportResults;
begin
  Result := False;
  if not CheckVmrActive then
    Exit;

  Res := TrwReportResults.Create;
  try
    ms.Position := 0;
    Res.SetFromStream(ms);
    Result := VmrPostProcessReportResult(Res);

    if Res.Count = 0 then
      ms := nil
    else
      ms := Res.GetAsStream;
  finally
    Res.Free;
  end;
end;

procedure TevVmrEngine.ProcessRelease(const TopBoxNbr: Integer;ReleaseType:TVmrReleaseTypeSet);
begin
  SVmrDaemonFuncs.ProcessRelease(TopBoxNbr,ReleaseType);
end;

function TevVmrEngine.InternalVmrPostProcessReportResult(const Res: TrwReportResults; const MarkShipped: Boolean): Boolean;

var
  bResWasCleared: Boolean;

  function  GetMailBoxGroupOptionValue(Cl_Nbr:integer;Box_Group_Nbr:integer;OptionName:string):string;
   var
    DataSetIntf: IevDataSetHolder;
  begin
     ctx_DataAccess.OpenClient(cl_nbr);
     DataSetIntf := TevDataSetHolder.CreateCloneAndRetrieveData(DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION,
      Format('CL_MAIL_BOX_GROUP_NBR=%u and (OPTION_TAG=''%s'')', [box_group_nbr,OptionName]));
      if DataSetIntf.DataSet.Locate('OPTION_TAG',OptionName, []) then
        result := DataSetIntf.DataSet['OPTION_STRING_VALUE']
      else
        result := 'N';
  end;

  procedure ProcessAutoRelease;
  var
    s: string;
    cd: TevClientDataSet;
    RelType :TVmrReleaseTypeSet;
    UnprintedReports: TrwReportResults;
    i: Integer;
  begin
    cd := TevClientDataSet.Create(nil);

    cd.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    try
      s := '-1';
      DM_SERVICE_BUREAU.SB_MAIL_BOX.First;
      while not DM_SERVICE_BUREAU.SB_MAIL_BOX.Eof do
      begin
        s := s+ ','+ DM_SERVICE_BUREAU.SB_MAIL_BOX.FieldByName('SB_MAIL_BOX_NBR').AsString;
        DM_SERVICE_BUREAU.SB_MAIL_BOX.Next;
      end;

      with TExecDSWrapper.Create('GenericSelectWithCondition') do
      begin
        SetMacro('COLUMNS', 'DISTINCT ConvertNullInteger(up_nbr, nbr) UP_NBR');
        SetMacro('TABLENAME', 'LIST_TOP_MAIL_BOXES');
        SetMacro('CONDITION', '(up_nbr in (#nbr) or nbr in (#nbr))');
        SetMacro('NBR', s);
        cd.Close;
        cd.DataRequest(AsVariant);
      end;
      cd.Open;
      s := '-1';
      cd.First;
      while not cd.Eof do
      begin
        s := s+ ','+ cd.FieldByName('UP_NBR').AsString;
        cd.Next;
      end;
      with TExecDSWrapper.Create('VmrSbTopMailBoxes') do
      begin
        SetMacro('CONDITION', ' and s.RELEASED_TIME is null and s.SB_MAIL_BOX_NBR in ('+ s+ ')');
        cd.Close;
        cd.DataRequest(AsVariant);
      end;
      cd.Open;

      cd.First;
      while not cd.Eof do
      begin
        if (cd['AUTO_RELEASE_TYPE'] = MAIL_BOX_AUTO_RELEASE_IMMED) and (cd['EMPTY_REQUIRED'] = 0) then
        begin
          if  GetMailBoxGroupOptionValue(cd.FieldByName('CL_NBR').AsInteger,cd.FieldByName('CL_MAIL_BOX_GROUP_NBR').AsInteger,VmrBlockAutoEmailVouchers) = 'Y' then
              RelType := [vrtPrinter,vrtStoreASCIIFiles]
          else
              RelType := [vrtPrinter,vrtVouchers,vrtStoreASCIIFiles];

          ctx_VmrRemote.ProcessRelease(cd.FieldByName('SB_MAIL_BOX_NBR').AsInteger,RelType, UnprintedReports);
          if Assigned(UnprintedReports) then
          begin
            if not bResWasCleared then
            begin
              Res.Clear;
              bResWasCleared := True;
            end;

            for i := 0 to UnprintedReports.Count - 1 do
              UnprintedReports[i].VmrTag := '';
            Res.AddReportResults(UnprintedReports);
            Res.PrinterName := UnprintedReports.PrinterName;
            Res.Descr := UnprintedReports.Descr;
            FreeAndNil(UnprintedReports);
          end;
       end;
        cd.Next;
      end;
    finally
      cd.Free;
    end;
  end;

  function StripControlChars(const s: string): string;
  var
    i: Integer;
  begin
    Result := Trim(s);
    for i := 1 to Length(Result) do
      if Result[i] in ['\', '/', ':'] then
        Result[i] := '_';
  end;
var
  a: TVmrSettings;
  function Process(const ClNbr, CoNbr, EeNbr: Integer): string;
  var
    cd: TevClientDataSet;
  begin
    if not a.CheckVmrActive(ClNbr) then
      Result := ''
    else
      if EeNbr = -1 then
      begin
        cd := a.FindCoSettings(ClNbr);
        Assert(cd.Locate('CO_NBR', CoNbr, []));
        if ConvertNull(cd['TAX_RETURN_SECOND_MB_GROUP_NBR'], 0) = 0 then
          Result := MakeString([ClNbr, cd['TAX_RETURN_MB_GROUP_NBR']], ';')
        else
          Result := MakeString([ClNbr, cd['TAX_RETURN_MB_GROUP_NBR'], cd['TAX_RETURN_SECOND_MB_GROUP_NBR']], ';');
      end
      else
      begin
        cd := a.FindEeSettings(ClNbr);
        Assert(cd.Locate('EE_NBR', EeNbr, []));
        Result := MakeString([ClNbr, cd['TAX_EE_RETURN_MB_GROUP_NBR'], '', EeNbr, Trim(ConvertNull(cd['WEB_PASSWORD'], '')), Trim(ConvertNull(cd['EMAIL'], ''))], ';');
      end;
  end;
var
  i, j, k: Integer;
  cdSorted: TevClientDataSet;
  iClNbr, iCoNbr, iEeNbr, iBoxNbr: Integer;
  sEeEmail, sEePassword: string;
  s, sFileName: string;
  sCond: string;
  R: TrwReportResult;
  lCls, lBoxes, lLoopCheck: TStringList;
  lMediaType: TVmrMediaType;
  lDeliveryMethod: TVmrDeliveryMethod;
  aKeyFields: TVmtKeyFieldType;
  aKeyValues: TVmtKeyValueType;
  tmpMB_NBR,tmpMB_C_NBR:variant;
  SelfServeEmailFlag:string;
  DupTaxReturnRpts_Content,
  DupTaxReturnRpts_File : IisStringList;
  tmpRes: TrwReportResults;
  tmpRep: TrwReportResult;

  procedure AddDownlinks(const UpLinkNbr: Integer);
  var
    cd: TevClientDataSet;
  begin
    cd := TevClientDataSet.Create(nil);
    try
      cd.Data := DM_CLIENT.CL_MAIL_BOX_GROUP.Data;
      cd.IndexFieldNames := 'UP_LEVEL_MAIL_BOX_GROUP_NBR';
      cd.SetRange([UpLinkNbr], [UpLinkNbr]);
      cd.First;
      while not cd.Eof do
      begin
        if cd['REQUIRED'] = GROUP_BOX_YES then
          if lBoxes.IndexOf(IntToStr(cd['CL_MAIL_BOX_GROUP_NBR'])) = -1 then
            lBoxes.Append(IntToStr(cd['CL_MAIL_BOX_GROUP_NBR']));
        AddDownlinks(cd['CL_MAIL_BOX_GROUP_NBR']);
        cd.Next;
      end;
    finally
      cd.Free;
    end;
  end;
  function CalcVmrLocation(BoxNbr:integer):string;
  begin
     Result := Trim(ExtractFromFiller(DM_CLIENT.CL.FILLER.AsString, 1, 40));
     if DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Locate('CL_MAIL_BOX_GROUP_NBR;OPTION_TAG', VarArrayOf([BoxNbr, VmrSbLocation]), []) then
        if Trim(ConvertNull(DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.FieldByName('OPTION_STRING_VALUE').AsString,'')) <> '' then
           Result := DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.FieldByName('OPTION_STRING_VALUE').AsString;
  end;

  function CheckMailBoxOption(MB_NBR,MB_CNT_NBR:variant;OptionName,FieldName:string;OptionValue:variant):Boolean;
  begin
    Result := DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Locate('SB_MAIL_BOX_NBR;OPTION_TAG',VarArrayOf([MB_NBR,OptionName]), []);
    if Result then
      Result := VarSameValue(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName(FieldName).Value, OptionValue);
  end;

  procedure AppendOption(MB_NBR,MB_CNT_NBR:variant;OptionName,FieldName:string;OptionValue:variant);
  begin
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Append;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION['SB_MAIL_BOX_NBR'] := MB_NBR;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION['SB_MAIL_BOX_CONTENT_NBR'] := MB_CNT_NBR;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION['OPTION_TAG'] := OptionName;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION[FieldName] := OptionValue;
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Post;
  end;
  procedure AppendStringOption(OptionName,OptionValue:variant);
  begin
      AppendOption(tmpMB_NBR,tmpMB_C_NBR,OptionName,'OPTION_STRING_VALUE',OptionValue);
  end;
  procedure AppendIntegerOption(OptionName,OptionValue:variant);
  begin
      AppendOption(tmpMB_NBR,tmpMB_C_NBR,OptionName,'OPTION_INTEGER_VALUE',OptionValue);
  end;
  procedure AddSelfServeEeNbrs(const aPos:integer);
   var
     l:IisStringList;
     i:integer;
     s,sep:string;
  begin

     l := TisStringList.Create;
     if not Assigned(FChecksExtraInfo) then
       exit;
     if ( FChecksExtraInfo.Count-1 < aPos)  then
       exit;
     l.CommaText := FChecksExtraInfo[aPos];
     s := '';
     for i := 0 to l.Count -1 do
     begin
        if Length(s)+ Length(l[i]) > 126 then
        begin
           AppendStringOption(VMR_SELF_SERVE_EE_NBRS,s);
           s := '';
           sep :='';
        end;
        s:= s + sep + l[i];
        sep :=',';
     end;
     if s <>'' then
        AppendStringOption(VMR_SELF_SERVE_EE_NBRS,s);
  end;

  procedure DeleteDupTaxReturnRpts;
  var
    i : integer;
  begin
    ctx_VmrRemote.PurgeMailBoxContentList(DupTaxReturnRpts_Content);
    for i :=0 to DupTaxReturnRpts_File.Count - 1 do
      ctx_VmrRemote.DeleteFile(DupTaxReturnRpts_File[i]);
  end;

  procedure CollectDupTaxReturnReports;
  var
    qContent: IevQuery;
    qMainCompany: IevQuery;
    qChildCompanies: IevQuery;
    sFixedReporName: String;
    sMainCustomCompanyName: String;
  begin
      qContent := TevQuery.Create('Select SB_MAIL_BOX_CONTENT_NBR, FILE_NAME From SB_MAIL_BOX_CONTENT a Where {AsOfNow<a>} '
                  +' and a.SB_MAIL_BOX_NBR = ' + DM_SERVICE_BUREAU.SB_MAIL_BOX.FieldByName('SB_MAIL_BOX_NBR').asString
                  +' and a.DESCRIPTION = ''' + StringReplace(FormatJobInfo(R.VmrCoNbr, R.VmrPrNbr, R.VmrEventDate, Res.Descr, R.ReportName), '''', '''''', [rfReplaceAll]) + '''');
      qContent.Execute;
      while not qContent.Result.Eof do
      begin
        DupTaxReturnRpts_Content.Add(qContent.Result.Fields[0].asString);
        DupTaxReturnRpts_File.Add(qContent.Result.Fields[1].asString);
        qContent.Result.next;
      end;

      if R.VmrConsolidation then
      begin
        // checking if it's main company
        qMainCompany := TevQuery.Create('select CL_CO_CONSOLIDATION_NBR from CL_CO_CONSOLIDATION a where '
          + '{AsOfNow<a>} and a.CONSOLIDATION_TYPE=:p1 and a.PRIMARY_CO_NBR=:p2');
        qMainCompany.Params.AddValue('P1', CONSOLIDATION_TYPE_ALL_REPORTS);
        qMainCompany.Params.AddValue('P2', R.VmrCoNbr);
        qMainCompany.Execute;
        if qMainCompany.Result.RecordCount > 0 then
        begin
          // reading list of child companies
          qChildCompanies := TevQuery.Create('select co_nbr, CUSTOM_COMPANY_NUMBER from CO a where {AsOfNow<a>} and ' +
            'a.CL_CO_CONSOLIDATION_NBR=:P1');
          qChildCOmpanies.Params.AddValue('P1', qMainCompany.Result.Fields[0].AsInteger);
          qChildCOmpanies.Execute;

          CheckCondition(qChildCOmpanies.Result.Locate('CO_NBR', R.VmrCoNbr, []), 'Cannot find main company!');
          sMainCustomCompanyName := qChildCOmpanies.Result.Fields[1].AsString;


          qChildCOmpanies.Result.First;
          while not qChildCOmpanies.Result.eof do
          begin
            if (qChildCOmpanies.Result.Fields[0].AsInteger <> R.VmrCoNbr) then
            begin
              if Pos('-' + sMainCustomCompanyName, R.ReportName) > 0 then
                sFixedReporName := StringReplace(R.ReportName, '-' + sMainCustomCompanyName, '-' + qChildCOmpanies.Result.Fields[1].AsString, [])
              else
                sFixedReporName := R.ReportName;

              qContent := TevQuery.Create('Select SB_MAIL_BOX_CONTENT_NBR, FILE_NAME From SB_MAIL_BOX_CONTENT a Where {AsOfNow<a>} '
                          +' and a.SB_MAIL_BOX_NBR = ' + DM_SERVICE_BUREAU.SB_MAIL_BOX.FieldByName('SB_MAIL_BOX_NBR').asString
                          +' and a.DESCRIPTION = ''' + StringReplace(FormatJobInfo(qChildCOmpanies.Result.Fields[0].AsInteger, R.VmrPrNbr, R.VmrEventDate, Res.Descr, sFixedReporName), '''', '''''', [rfReplaceAll]) + '''');
              qContent.Execute;
              while not qContent.Result.Eof do
              begin
                DupTaxReturnRpts_Content.Add(qContent.Result.Fields[0].asString);
                DupTaxReturnRpts_File.Add(qContent.Result.Fields[1].asString);
                qContent.Result.next;
              end;

            end;
            qChildCOmpanies.Result.Next;
          end;  // while
        end;  // if qMainCompany.Result.RecordCount > 0
      end;  // if R.VmrConsolidation
  end;
begin
  Result := False;
  bResWasCleared := false;

  if not CheckVmrActive then Exit;
  SetLength(aKeyFields, 1);
  aKeyFields[0] := 'SB_MAIL_BOX_NBR';
  SetLength(aKeyValues, 1);
  DM_CLIENT.CL_MAIL_BOX_GROUP.IndexFieldNames := 'CL_MAIL_BOX_GROUP_NBR';
  DM_SERVICE_BUREAU.SB_MAIL_BOX.IndexFieldNames := 'CL_NBR;CL_MAIL_BOX_GROUP_NBR';
  a := TVmrSettings.Create;
  DupTaxReturnRpts_Content := TisStringList.Create;
  DupTaxReturnRpts_File    := TisStringList.Create;
  cdSorted := TevClientDataSet.Create(nil);
  try
    cdSorted.FieldDefs.Add('CL_NBR', ftInteger);
    cdSorted.FieldDefs.Add('BOX_NBR', ftInteger);
    cdSorted.FieldDefs.Add('POS', ftInteger);
     cdSorted.CreateDataSet;
    cdSorted.LogChanges := False;
    cdSorted.IndexFieldNames := 'CL_NBR;BOX_NBR;POS';
    for i := 0 to Res.Count-1 do
    begin
      s := Res[i].TaxReturnVmrTag;
      if s <> '' then
      begin
        iClNbr := StrToInt(Fetch(s, ';'));
        iCoNbr := StrToInt(Fetch(s, ';'));
        if s <> '' then
          iEeNbr := StrToInt(Fetch(s, ';'))
        else
          iEeNbr := -1;
        Res[i].VmrTag := Process(iClNbr, iCoNbr, iEeNbr);
      end;
    end;
    for i := 0 to Res.Count-1 do
      if Res[i].Data <> nil then
      begin
        s := Res[i].VmrTag;
        if s <> '' then
        begin
          iClNbr := StrToInt(Fetch(s, ';'));
          iBoxNbr := StrToInt(Fetch(s, ';'));
          cdSorted.AppendRecord([iClNbr, iBoxNbr, i]);
          if s <> '' then
          begin
            iBoxNbr := StrToIntDef(Fetch(s, ';'), 0);
            if iBoxNbr <> 0 then
              cdSorted.AppendRecord([iClNbr, iBoxNbr, i]);
          end;
        end;
      end;
    tmpRes := TrwReportResults.create;
    for i:=0 to Res.Count-1 do
    begin
      tmpRep := tmpRes.Add();
      tmpRep.Assign(Res[i]);
      tmpRep.TaxReturnVmrTag := Res[i].TaxReturnVmrTag;
    end;
    tmpRes.PrinterName := Res.PrinterName;
    tmpRes.Descr := Res.Descr;
    Result := cdSorted.RecordCount > 0;
    lCls := GetFieldValueList(cdSorted, 'CL_NBR');
    try
      for i := 0 to lCls.Count-1 do
      begin

        LockResource(VMR_EXCLUSIVE_OPERATION, lCls[i], 'A VMR  for this client is already being processed.', rlTryUntilGet);
        try
          sFileName := '';
          ctx_DataAccess.OpenClient(StrToInt(lCls[i]));
          cdSorted.SetRange([StrToInt(lCls[i])], [StrToInt(lCls[i])]);
          lBoxes := GetFieldValueList(cdSorted, 'BOX_NBR');
          lLoopCheck := TStringList.Create;
          try
            lLoopCheck.Sorted := True;
            ctx_DataAccess.StartNestedTransaction([dbtBureau], 'SB_VMR');
            try
              DM_CLIENT.CL_MAIL_BOX_GROUP.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
              DM_CLIENT.CL.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
              DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
              ctx_DataAccess.OpenDataSets([DM_CLIENT.CL, DM_CLIENT.CL_MAIL_BOX_GROUP, DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION]);
              sCond := lBoxes.CommaText;
              while sCond <> '' do
              begin
                lLoopCheck.Clear;
                Assert(DM_CLIENT.CL_MAIL_BOX_GROUP.FindKey([StrToInt(Fetch(sCond, ','))]));
                while not VarIsNull(DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR']) do
                begin
                  if lLoopCheck.IndexOf(IntToStr(DM_CLIENT.CL_MAIL_BOX_GROUP['CL_MAIL_BOX_GROUP_NBR'])) <> -1 then
                    raise EInconsistentData.CreateHelp('Circular references in mail box group setup', IDH_ConsistencyViolation);
                  lLoopCheck.Append(IntToStr(DM_CLIENT.CL_MAIL_BOX_GROUP['CL_MAIL_BOX_GROUP_NBR']));
                  if lBoxes.IndexOf(IntToStr(DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR'])) = -1 then
                    lBoxes.Insert(0,IntToStr(DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR']))
                  else
                    lBoxes.Move(lBoxes.IndexOf(IntToStr(DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR'])),0);
                  if not VarIsNull(DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR']) then
                    Assert(DM_CLIENT.CL_MAIL_BOX_GROUP.FindKey([DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR']]));
                end;
                AddDownlinks(DM_CLIENT.CL_MAIL_BOX_GROUP['CL_MAIL_BOX_GROUP_NBR']);
              end;
              sCond := '(CL_NBR='+ lCls[i]+ ' and ' + BuildInSqlStatement('CL_MAIL_BOX_GROUP_NBR', lBoxes.CommaText) + ' and RELEASED_TIME IS NULL)';
              DM_SERVICE_BUREAU.SB_MAIL_BOX.Close;
              DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.CheckDSCondition(AlwaysFalseCond);
              DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.CheckDSCondition(AlwaysFalseCond);
              if MarkShipped then
                DM_SERVICE_BUREAU.SB_MAIL_BOX.CheckDSCondition(AlwaysFalseCond)
              else
                DM_SERVICE_BUREAU.SB_MAIL_BOX.CheckDSCondition(sCond);
              ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX, DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT,
                DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION]);
              for j := 0 to lBoxes.Count-1 do
              begin
                if not DM_SERVICE_BUREAU.SB_MAIL_BOX.FindKey([StrToInt(lCls[i]), StrToInt(lBoxes[j])]) then
                begin
                  Assert(DM_CLIENT.CL_MAIL_BOX_GROUP.FindKey([StrToInt(lBoxes[j])]));
                  DM_SERVICE_BUREAU.SB_MAIL_BOX.Append;

                  DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR'] := ctx_DBAccess.GetGeneratorValue('G_SB_MAIL_BOX', dbtBureau, 1);

                  DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_NBR'] := StrToInt(lCls[i]);
                  DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_MAIL_BOX_GROUP_NBR'] := StrToInt(lBoxes[j]);
                  DM_SERVICE_BUREAU.SB_MAIL_BOX['DESCRIPTION;REQUIRED;NOTIFICATION_EMAIL;SB_COVER_LETTER_REPORT_NBR;SY_COVER_LETTER_REPORT_NBR;AUTO_RELEASE_TYPE;NOTE'] :=
                    DM_CLIENT.CL_MAIL_BOX_GROUP['DESCRIPTION;REQUIRED;NOTIFICATION_EMAIL;SB_COVER_LETTER_REPORT_NBR;SY_COVER_LETTER_REPORT_NBR;AUTO_RELEASE_TYPE;NOTE'];

                  lMediaType := nil;
                  lDeliveryMethod := nil;
                  tmpMB_NBR := DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR'];
                  tmpMB_C_NBR := NULL;
                  try
                    if not VarIsNull(DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_METHOD_ACTIVE_FROM'])
                    and not VarIsNull(DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_METHOD_ACTIVE_TO'])
                    and (Date >= DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_METHOD_ACTIVE_FROM'])
                    and (Date <= DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_METHOD_ACTIVE_TO']) then
                    begin
                      DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_DELIVERY_METHOD_NBR;SB_MEDIA_TYPE_NBR'] :=
                        DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_DELIVERY_METHOD_NBR;SEC_SB_MEDIA_TYPE_NBR'];
                      lDeliveryMethod := InstanceClSecondaryDeliveryMethod(DM_CLIENT.CL_MAIL_BOX_GROUP);
                      lMediaType := InstanceClSecondaryMedia(DM_CLIENT.CL_MAIL_BOX_GROUP);
                    end
                    else
                    begin
                      DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_DELIVERY_METHOD_NBR;SB_MEDIA_TYPE_NBR'] :=
                        DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_DELIVERY_METHOD_NBR;PRIM_SB_MEDIA_TYPE_NBR'];
                      lDeliveryMethod := InstanceClPrimaryDeliveryMethod(DM_CLIENT.CL_MAIL_BOX_GROUP);
                      lMediaType := InstanceClPrimaryMedia(DM_CLIENT.CL_MAIL_BOX_GROUP);
                    end;
                    if not Assigned(lDeliveryMethod) then
                      raise EInconsistentData.CreateHelp('Delivery method is not setup correctly', IDH_ConsistencyViolation);
                    if not Assigned(lMediaType) then
                      raise EInconsistentData.CreateHelp('Media type is not setup correctly', IDH_ConsistencyViolation);
                    lDeliveryMethod.PopulateNewMailBox;
                    if ConvertNull(DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'], '') = '' then
                       DM_SERVICE_BUREAU.SB_MAIL_BOX['ADDRESSEE'] := 'N/A';
                    DM_SERVICE_BUREAU.SB_MAIL_BOX['BARCODE'] := VarToStr(DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR']);
                    DM_SERVICE_BUREAU.SB_MAIL_BOX['CREATED_TIME'] := SysTime;
                    if MarkShipped then
                    begin
                     DM_SERVICE_BUREAU.SB_MAIL_BOX['RELEASED_TIME'] := Now;
                     DM_SERVICE_BUREAU.SB_MAIL_BOX['PRINTED_TIME'] := Now;
                     DM_SERVICE_BUREAU.SB_MAIL_BOX['SCANNED_TIME'] := Now;
                     DM_SERVICE_BUREAU.SB_MAIL_BOX['SHIPPED_TIME'] := Now;
                    end;
                    DM_SERVICE_BUREAU.SB_MAIL_BOX['DISPOSE_CONTENT_AFTER_SHIPPING'] := GROUP_BOX_NO;
                    DM_SERVICE_BUREAU.SB_MAIL_BOX.Post;
                    aKeyValues[0] := DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR'];
                    lDeliveryMethod.ExtraOptions.Apply(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION, aKeyFields,
                      aKeyValues, 'PD');
                    lMediaType.ExtraOptions.Apply(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION, aKeyFields,
                    aKeyValues, 'PM');
                    AppendStringOption(VmrSbLocation,CalcVmrLocation(StrToInt(lBoxes[j])));
                    AppendStringOption(VmrBlockAutoEmailVouchers,
                                       GetMailBoxGroupOptionValue(DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_NBR'],DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_MAIL_BOX_GROUP_NBR'],VmrBlockAutoEmailVouchers));
                    AppendStringOption(VmrIncludeDescriptioninEmails,
                                       GetMailBoxGroupOptionValue(DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_NBR'],DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_MAIL_BOX_GROUP_NBR'],VmrIncludeDescriptioninEmails));
                    SelfServeEmailFlag := GetMailBoxGroupOptionValue( DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_NBR'], DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_MAIL_BOX_GROUP_NBR'], VmrEmailSelfServe);
                    AppendStringOption(VmrEmailSelfServe,SelfServeEmailFlag);
                    AppendStringOption(VmrDirectEmailVouchers,
                                       GetMailBoxGroupOptionValue(DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_NBR'],DM_SERVICE_BUREAU.SB_MAIL_BOX['CL_MAIL_BOX_GROUP_NBR'],VmrDirectEmailVouchers));
                  finally
                    lMediaType.Free;
                    lDeliveryMethod.Free;
                  end;
                end;
              end;
              for j := 0 to lBoxes.Count-1 do
              begin
                Assert(DM_CLIENT.CL_MAIL_BOX_GROUP.FindKey([StrToInt(lBoxes[j])]));
                if not VarIsNull(DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR']) then
                begin
                  Assert(DM_SERVICE_BUREAU.SB_MAIL_BOX.FindKey([StrToInt(lCls[i]), DM_CLIENT.CL_MAIL_BOX_GROUP['UP_LEVEL_MAIL_BOX_GROUP_NBR']]));
                  iBoxNbr := DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR'];
                  Assert(DM_SERVICE_BUREAU.SB_MAIL_BOX.FindKey([StrToInt(lCls[i]), StrToInt(lBoxes[j])]));
                  DM_SERVICE_BUREAU.SB_MAIL_BOX.Edit;
                  DM_SERVICE_BUREAU.SB_MAIL_BOX['UP_LEVEL_MAIL_BOX_NBR'] := iBoxNbr;
                  DM_SERVICE_BUREAU.SB_MAIL_BOX.Post;
                end;
              end;
              for j := 0 to lBoxes.Count-1 do
              begin
                cdSorted.SetRange([StrToInt(lCls[i]), StrToInt(lBoxes[j])], [StrToInt(lCls[i]), StrToInt(lBoxes[j])]);
                cdSorted.First;
                while not cdSorted.Eof do
                begin
                  R := tmpRes[cdSorted['POS']];
                  Assert(Assigned(R));
                  Assert(DM_SERVICE_BUREAU.SB_MAIL_BOX.FindKey([StrToInt(lCls[i]), StrToInt(lBoxes[j])]));

                  if (R.TaxReturnVmrTag <> '') then //only Tax Return
                    CollectDupTaxReturnReports;

                  if (R.VmrPrNbr <> 0) and VarIsNull(DM_SERVICE_BUREAU.SB_MAIL_BOX['PR_NBR']) then
                  begin
                    DM_SERVICE_BUREAU.SB_MAIL_BOX.Edit;
                    DM_SERVICE_BUREAU.SB_MAIL_BOX['PR_NBR'] := R.VmrPrNbr;
                    DM_SERVICE_BUREAU.SB_MAIL_BOX.Post;
                  end;
                  for k := 1 to R.Copies do
                  begin
                    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Append;
                    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['SB_MAIL_BOX_NBR'] := DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR'];
                    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['DESCRIPTION'] := FormatJobInfo(R.VmrCoNbr, R.VmrPrNbr, R.VmrEventDate, tmpRes.Descr, R.ReportName);
                    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['MEDIA_TYPE'] := R.MediaType;

                    sFileName := IncludeTrailingPathDelimiter(lCls[i])+
                      IncludeTrailingPathDelimiter(FormatDateTime('MMDDYY', SysTime))+
                      IncludeTrailingPathDelimiter(StripControlChars(Copy(tmpRes.Descr, 1, 10)))+
                      IntToHex(RandomInteger(0, High(Integer)), 8)+ '.rwa';
                    Assert(Length(sFileName) <= 80, 'Error saving FILE_NAME to SB_MAILBOX_CONTENT');
                    sFileName := ctx_VmrRemote.StoreFile(sFileName, R.Data, True);
                    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['FILE_NAME'] := sFileName;

                    tmpMB_NBR := DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR'];
                    tmpMB_C_NBR := DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['SB_MAIL_BOX_CONTENT_NBR'];
                    AppendStringOption(VmrContentOptionLayers,LayersToStr(R.Layers));
                    AppendStringOption(VmrContentOptionReportNO, R.Level + IntToStr(R.NBR));
                    AppendIntegerOption(VmrContentOptionReportType,Integer(R.ReportType));
                   // AppendIntegerOption(VmrContentOptionPRReprintHistoryNbr, Res.PrReprintHistoryNBR);

                    if R.ReportType = rtASCIIFile then
                    begin
                       AppendStringOption(VmrContentASCII_FileName,R.FileName);
                       if Pos(ExtractFileName(R.Filename),R.WarningMessage)>0 then // not a best way to detect autogenerated file name...
                         AppendIntegerOption(VmrContentASCII_FileName_Generated,1);
                    end;
                    if R.PagesInfo.Count = 1 then
                    begin
                       DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['PAGE_COUNT'] := R.PagesInfo[0].DuplexPageCount+ R.PagesInfo[0].SimplexPageCount;
                       AppendIntegerOption(VmrContentOptionSimplexPages,R.PagesInfo[0].SimplexPageCount);
                       AppendIntegerOption(VmrContentOptionDuplexPages,R.PagesInfo[0].DuplexPageCount);
                    end else
                       DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['PAGE_COUNT'] := 0;

                    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Post;

                    // direct email voucher
                    s := R.VmrTag;
                    Fetch(s, ';');
                    Fetch(s, ';');
                    Fetch(s, ';');
//                    if SendDirectEmails and (s <> '') then
                    if (s <> '') then
                    begin
                      iEeNbr := StrToInt(Fetch(s, ';'));
                      sEePassword := Fetch(s, ';');
                      sEeEmail := Fetch(s, ';');
                      if sEePassword <> '' then  AppendStringOption(VmrContentOptionPassword,sEePassword);
                      if sEeEmail <> '' then  AppendStringOption(VmrContentOptionEmail,sEeEmail);
                      AppendIntegerOption(VmrContentOptionEeNbr,iEeNbr);
                    end;
                    if SelfServeEmailFlag = 'Y' then
                       AddSelfServeEeNbrs(cdSorted['POS']);
                  end;
                  cdSorted.Next;
                end;
              end;

              ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX, DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT, DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION]);
              ctx_DataAccess.CommitNestedTransaction;

              if Assigned(FChecksExtraInfo) then
                 FChecksExtraInfo.Clear;
           except
              ctx_DataAccess.RollbackNestedTransaction;
              if sFileName <> '' then
                ctx_VmrRemote.DeleteFile(sFileName);
              raise
           end;

           if not MarkShipped then
              ProcessAutoRelease;

           if (DupTaxReturnRpts_Content.Count > 0) then
             DeleteDupTaxReturnRpts;

          finally
            lBoxes.Free;
            lLoopCheck.Free;
          end;
        finally
          UnLockResource(VMR_EXCLUSIVE_OPERATION, lCls[i], rlTryUntilGet);
        end;
      end;
    finally
      lCls.Free;
      FreeandNil(tmpRes);
    end;

    for i := Res.Count-1 downto 0 do
    begin
      if (Res[i].VmrTag <> '')
      and (Res[i].Data <> nil) then
        Res.Delete(i);
    end;

  finally
    cdSorted.Free;
    a.Free;
  end;
end;

procedure TevVmrEngine.VmrPreProcessReportList(const RepList: TrwReportList);
var
  i: Integer;
  sRepAncestor: string;
  lRepList: TrwReportList;
  a: TVmrSettings;
  function IntegerListed(const v: Integer; const a: array of Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to High(a) do
      if v = a[i] then
      begin
        Result := True;
        Break;
      end;
  end;
  function ExctractClientId(const r: TrwRepParams): Integer;
  var
    p: TrwReportParam;
    v: Variant;
  begin
    p := r.Params.ParamByName('Clients');
    Assert(Assigned(p), 'Parameter Clients is not found');
    v := p.Value;
    Assert(VarIsArray(v) and ((VarArrayHighBound(v, 1)- VarArrayLowBound(v, 1))=0), 'Parameter Clients should have only one element');
    Result := v[VarArrayHighBound(v, 1)];
  end;
  procedure ProcessEeReport(const r: TrwRepParams; const cd: TevClientDataSet; const ClNbr: Integer;
    const GroupFieldName1, GroupFieldName2: string; Filters: array of string);
  var
    r2: TrwRepParams;
    procedure SubProcess(const GroupFieldName: string);
    var
      i, j: Integer;
      s, s2: string;
      p: TrwReportParam;
      v: Variant;
      lBoxes: TStringList;
    begin
      Assert((Length(Filters) mod 2) = 0);
      s := '';
      for i := 0 to (Length(Filters) div 2)- 1 do
      begin
        p := r.Params.ParamByName(Filters[i*2]);
        if Assigned(p) then
        begin
          v := p.Value;
          if VarIsArray(v) then
          begin
            s2 := '';
            for j := VarArrayLowBound(v, 1) to VarArrayHighBound(v, 1) do
            begin
              if s2 <> '' then
                s2 := s2+ ',';
              s2 := s2+ VarToStr(v[j]);
            end;
            if s2 <> '' then
            begin
              if s <> '' then
                s := s+ ' and ';
              s := s + BuildInSqlStatement ( Filters[i*2+1], s2 );
            end;
          end;
        end;
      end;
      cd.Filter := s;
      cd.Filtered := True;
      cd.IndexFieldNames := GroupFieldName;
      lBoxes := GetFieldValueList(cd, GroupFieldName);
      try
        for i := 0 to lBoxes.Count-1 do
          if lBoxes[i] <> '0' then
          begin
            r2 := lRepList.AddReport;
            r2.Assign(r);
            r2.VmrTag := MakeString([ClNbr, lBoxes[i]], ';');
            if lBoxes.Count > 1 then // only if there's a split to be made
            begin
              cd.SetRange([StrToInt(lBoxes[i])], [StrToInt(lBoxes[i])]);
              for j := 0 to (Length(Filters) div 2)- 1 do
              begin
                p := r2.Params.ParamByName(Filters[j*2]);
                if Assigned(p) then
                  p.Value := GetVarArrayFieldValueList(cd, Filters[j*2+1], varInteger, True);
              end;
            end;
          end;
      finally
        lBoxes.Free;
        cd.CancelRange;
      end;
    end;
  begin
    if a.CheckVmrActive(ClNbr) then
    begin
      if r.OverrideVmrMbGroupNbr <> 0 then
      begin
        r2 := lRepList.AddReport;
        r2.Assign(r);
        r2.VmrTag := MakeString([ClNbr, r.OverrideVmrMbGroupNbr], ';');
      end
      else
      begin
        SubProcess(GroupFieldName1);
        if GroupFieldName2 <> '' then
          SubProcess(GroupFieldName2);
      end;
    end
    else
    begin
      r2 := lRepList.AddReport;
      r2.Assign(r);
      r2.VmrTag := '';
    end;
  end;
  procedure ProcessEeChecks(const r: TrwRepParams; const cd: TevClientDataSet; const ClNbr: Integer;
    const GroupFieldName1: string);
  var
    r2: TrwRepParams;
    cdEe, cdEe2: TevClientDataSet;
    cdDirectEmailVoucgher: TevClientDataSet;
    cdEeWebPassword: TevClientDataSet;
    procedure SubProcess(const GroupFieldName: string; aLiteDataset: boolean = false);
    var
      i, j: Integer;
      v: Variant;
      lBoxes, lBoxes2, lBoxes3: TStringList;
    begin
      lBoxes := GetFieldValueList(cd, GroupFieldName);
      try
        cd.IndexFieldNames := GroupFieldName;
        for i := 0 to lBoxes.Count-1 do
          if lBoxes[i] <> '0' then
          begin
            cd.SetRange([StrToInt(lBoxes[i])], [StrToInt(lBoxes[i])]);
            lBoxes2 := GetFieldValueList(cd, 'EE_NBR', True);
            try
              if lBoxes2.Count > 0 then
              begin
                cdEe.Filtered := True;
                cdEe.FilterList := lBoxes2;
                cdEe.FilterListFieldName := 'EE_NBR';
                if cdDirectEmailVoucgher.FindKey([StrToInt(lBoxes[i])])
                and (cdDirectEmailVoucgher['FLAG'] = GROUP_BOX_YES) then
                begin
                  if aLiteDataset then
                    cdEe.Filter := '(Voucher=1)'
                  else
                    cdEe.Filter := '(CheckNumberOrVoucher=''Memo'')';
                  lBoxes3 := cdEe.GetFieldValueList('EE_NBR', True);
                  try
                    for j := lBoxes3.Count-1 downto 0 do
                      if not cdEeWebPassword.FindKey([StrToIntDef(lBoxes3[j], 0)]) then
                        lBoxes3.Delete(j)
                      else
                      begin
                        r2 := lRepList.AddReport;
                        r2.Assign(r);
                        r2.VmrTag := MakeString([ClNbr, lBoxes[i], '', StrToIntDef(lBoxes3[j], 0), cdEeWebPassword['WEB_PASSWORD'], Trim(cdEeWebPassword['EMAIL'])], ';');
                        v := r2.Params.ParamByName('DataSets').Value;
                        if aLiteDataset then
                          cdEe2.Filter := '(Voucher=1)'
                        else
                          cdEe2.Filter := '(CheckNumberOrVoucher=''Memo'')';
                        cdEe2.Filtered := true;
                        cdEe2.SetRange([StrToIntDef(lBoxes3[j], 0)], [StrToIntDef(lBoxes3[j], 0)]);

                        Assert(cdEe2.RecordCount>0);
                        v[0] := CreateLookupData(cdEe2);
                        r2.Params.ParamByName('DataSets').Value := v;
                        v := Unassigned;
                        cdEE2.First;
                        while not cdEE2.Eof do
                        begin
                          if(cdEe.Locate('CheckNbr', cdEE2.FieldByName('CheckNbr').Value, [])) then
                            cdEE.Delete;
                          cdEE2.Next;
                        end;
                       {
                        while cdEe.Locate('EE_NBR;CHECK_NBR', StrToIntDef(lBoxes3[j], 0), []) do
                          cdEe.Delete;
                       }
                        cdEe2.Filtered := false;
                        cdEe2.Filter := '';
                      end;
                  finally
                    FreeAndNil(lBoxes3);
                  end;
                end;
                cdEe.Filter := '';
                if cdEe.RecordCount > 0 then
                begin
                  r2 := lRepList.AddReport;
                  r2.Assign(r);
                  r2.VmrTag := MakeString([ClNbr, lBoxes[i]], ';');
                  v := r2.Params.ParamByName('DataSets').Value;
                  v[0] := CreateLookupData(cdEe);
                  r2.Params.ParamByName('DataSets').Value := v;
                  v := Unassigned;
                end;
              end;
            finally
              lBoxes2.Free;
              cdEe.FilterListFieldName := '';
            end;
          end;
      finally
        lBoxes.Free;
      end;
    end;

    procedure SubProcessNewCheckForms(const GroupFieldName: string;CheckNbrs:Variant);
    var
      i, j, l: Integer;
      v: Variant;
      lBoxes, lBoxes2, lBoxes3,ChecksList: TStringList;
    begin
      ChecksList := TStringList.Create;
      for i := VarArrayLowBound(CheckNbrs,1) to VarArrayHighBound(CheckNbrs,1) do
      begin
          if Length(string(CheckNbrs[i])) >0 then
             ChecksList.Add(CheckNbrs[i]);
      end;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
          SetMacro('COLUMNS', 'EE_NBR,Pr_Check_Nbr,NET_WAGES');
          SetMacro('TABLENAME', 'PR_CHECK');
          if Trim(ChecksList.CommaText) <>'' then
            SetMacro('CONDITION', BuildInSqlStatement('Pr_Check_Nbr', ChecksList.CommaText))
          else
            SetMacro('CONDITION', '('  + AlwaysFalseCond + ')');
          ctx_DataAccess.GetCustomData(cdEe, AsVariant);
      end;
      FreeAndNil(ChecksList);
      lBoxes := GetFieldValueList(cd, GroupFieldName);
      try
        cd.IndexFieldNames := GroupFieldName;
        for i := 0 to lBoxes.Count-1 do
          if lBoxes[i] <> '0' then
          begin
            cd.SetRange([StrToInt(lBoxes[i])], [StrToInt(lBoxes[i])]);
            lBoxes2 := GetFieldValueList(cd, 'EE_NBR', True);
            try
              if lBoxes2.Count > 0 then
              begin
                cdEe.Filtered := True;
                cdEe.FilterList := lBoxes2;
                cdEe.FilterListFieldName := 'EE_NBR';
                if cdDirectEmailVoucgher.FindKey([StrToInt(lBoxes[i])])
                and (cdDirectEmailVoucgher['FLAG'] = GROUP_BOX_YES) then
                begin
                  cdEe.Filter := '(NET_WAGES=0)';
                  lBoxes3 := cdEe.GetFieldValueList('EE_NBR', True);
                  try
                    for j := lBoxes3.Count-1 downto 0 do
                      if not cdEeWebPassword.FindKey([StrToIntDef(lBoxes3[j], 0)]) then
                        lBoxes3.Delete(j)
                      else
                      begin
                        r2 := lRepList.AddReport;
                        r2.Assign(r);
                        r2.VmrTag := MakeString([ClNbr, lBoxes[i], '', StrToIntDef(lBoxes3[j], 0), cdEeWebPassword['WEB_PASSWORD'], Trim(cdEeWebPassword['EMAIL'])], ';');
                        ChecksList := TStringList.Create;
                        while cdEe.Locate('EE_NBR', StrToIntDef(lBoxes3[j], 0), []) do
                        begin
                          ChecksList.Add(cdEe.FieldByName('Pr_Check_Nbr').AsString);
                          cdEe.Delete;
                        end;
                        v := VarArrayCreate([0, ChecksList.Count - 1], varVariant);
                        for l := 0 to ChecksList.Count -1 do
                        begin
                           v[l] := ChecksList.Strings[l];
                        end;
                        r2.Params.ParamByName('CheckNbrs').Value := v;
                        v := Unassigned;
                       FreeAndNil(ChecksList);
                      end;
                  finally
                    FreeAndNil(lBoxes3);
                  end;
                end;
                cdEe.Filter := '';
                if cdEe.RecordCount > 0 then
                begin

                  v := VarArrayCreate([0, cdEe.RecordCount - 1], varVariant);
                  l := 0;
                  cdEe.First;
                  while not cdEe.eof do
                  begin
                     v[l] := cdEe.FieldByName('Pr_Check_Nbr').AsString;
                     cdEe.Next;
                     Inc(l);
                  end;
                  r2 := lRepList.AddReport;
                  r2.Assign(r);
                  r2.VmrTag := MakeString([ClNbr, lBoxes[i]], ';');
                  r2.Params.ParamByName('CheckNbrs').Value := v;
                  v := Unassigned;
                end;
              end;
            finally
              lBoxes2.Free;
              cdEe.FilterListFieldName := '';
            end;
          end;
      finally
        lBoxes.Free;
      end;
    end;

  var
    v: Variant;
  begin
    if a.CheckVmrActive(ClNbr) then
    begin
      cdEe := TevClientDataSet.Create(nil);
      cdEe2 := TevClientDataSet.Create(nil);
      cdDirectEmailVoucgher := TevClientDataSet.Create(nil);
      cdEeWebPassword := TevClientDataSet.Create(nil);
      try
        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('COLUMNS', 'CL_MAIL_BOX_GROUP_NBR, OPTION_STRING_VALUE FLAG');
          SetMacro('TABLENAME', 'CL_MAIL_BOX_GROUP_OPTION');
          SetMacro('CONDITION', 'OPTION_TAG=:OTag');
          SetParam('OTag', VmrDirectEmailVouchers);
          ctx_DataAccess.GetCustomData(cdDirectEmailVoucgher, AsVariant);
        end;
        cdDirectEmailVoucgher.IndexFieldNames := 'CL_MAIL_BOX_GROUP_NBR';
        with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
        begin
          SetMacro('COLUMNS', 'EE_NBR, WEB_PASSWORD, t2.E_MAIL_ADDRESS EMAIL');
          SetMacro('TABLE1', 'EE');
          SetMacro('TABLE2', 'CL_PERSON');
          SetMacro('JOINFIELD', 'CL_PERSON');
          SetMacro('CONDITION', 'WEB_PASSWORD is not null and (WEB_PASSWORD <> '''') and '+
            '((t2.E_MAIL_ADDRESS is not null and t2.E_MAIL_ADDRESS <> ''''))');
          ctx_DataAccess.GetCustomData(cdEeWebPassword, AsVariant);
        end;
        cdEeWebPassword.IndexFieldNames := 'EE_NBR';
        if R.Params.ParamByName('DataSets') <> nil then
        begin
          v := R.Params.ParamByName('DataSets').Value;
          cdEe.Data := v[0];
          cdEe2.Data := v[0];
          cdEe2.IndexFieldNames := 'EE_NBR';
          v := Unassigned;
          SubProcess(GroupFieldName1, R.Params.ParamByName('DontPrintBankInfo') <> nil);
        end
        else
        begin

          if R.Params.ParamByName('CheckNbrs') = nil then
            v := VarArrayCreate([0, 0], varVariant)
          else
            v := R.Params.ParamByName('CheckNbrs').Value;
            SubProcessNewCheckForms(GroupFieldName1,v);

        end;
      finally
        cdEeWebPassword.Free;
        cdDirectEmailVoucgher.Free;
        cdEe2.Free;
        cdEe.Free;
      end;
    end
    else
    begin
      r2 := lRepList.AddReport;
      r2.Assign(r);
      r2.VmrTag := '';
    end;
  end;
  procedure ProcessMiscChecks(const r: TrwRepParams; const cd: TevClientDataSet; const ClNbr: Integer);
  var
    r2: TrwRepParams;
  begin
    if a.CheckVmrActive(ClNbr) then
    begin
      r2 := lRepList.AddReport;
      r2.Assign(r);
      Assert(cd.Locate('CO_NBR', r.VmrCoNbr, []));
      case r.OverrideVmrMiscTaxCheckType of
      ctAgency, ctBilling:
        r2.VmrTag := MakeString([ClNbr, cd['AGENCY_CHECK_MB_GROUP_NBR']], ';');
      ctTax:
        r2.VmrTag := MakeString([ClNbr, cd['TAX_CHECK_MB_GROUP_NBR']], ';');
      else
        Assert(False);
      end;
    end
    else
    begin
      r2 := lRepList.AddReport;
      r2.Assign(r);
      r2.VmrTag := '';
    end;
  end;
  procedure ProcessMiscReport(const r: TrwRepParams; const cd: TevClientDataSet; const ClNbr: Integer;
    const GroupFieldName1, GroupFieldName2: string);
  var
    r2: TrwRepParams;
    procedure SubProcess(const GroupFieldName: string);
    begin
      if cd[GroupFieldName] <> 0 then
      begin
        r2 := lRepList.AddReport;
        r2.Assign(r);
        r2.VmrTag := MakeString([ClNbr, cd[GroupFieldName]], ';');
      end;
    end;
  var
    cdData: TevClientDataSet;
    v: Variant;
  begin
    if a.CheckVmrActive(ClNbr) then
    begin
      if r.OverrideVmrMbGroupNbr <> 0 then
      begin
        r2 := lRepList.AddReport;
        r2.Assign(r);
        r2.VmrTag := MakeString([ClNbr, r.OverrideVmrMbGroupNbr], ';');
      end
      else
      begin
        if (r.Level = 'S') then
          if (r.NBR = 257) then
          begin
            if R.Params.ParamByName('DataSets') <> nil then
            begin
               cdData := TevClientDataSet.Create(nil);
               try
                 v := R.Params.ParamByName('DataSets').Value;

                 cdData.Data := v[0];
                 Assert(cd.Locate('CO_NBR', cdData['CO_NBR'], []));
               finally
                 cdData.Free;
               end;
            end else
              raise EInconsistentData.CreateHelp('This report should NOT be set up as a payroll report.'+chr(13)+'DO NOT add it to the Reports - Setup Reports screen.', IDH_ConsistencyViolation);
          end
          else
          if IntegerListed(r.NBR, [1, 11, 31, 210, 250, 252, 342, 475, 538, 614, 780]) then
          begin
            v := R.Params.ParamByName('Companies').Value;
            Assert(not VarIsNull(v) and VarIsArray(v));
            Assert(cd.Locate('CO_NBR', v[0], []));
          end
          else
          if r.VmrCoNbr <> 0 then
            Assert(cd.Locate('CO_NBR', r.VmrCoNbr, []))
          else
            Assert(False, 'Unexpected payroll report '+ R.Level+ IntToStr(R.NBR));
        SubProcess(GroupFieldName1);
        if GroupFieldName2 <> '' then
          SubProcess(GroupFieldName2);
      end;
    end
    else
    begin
      r2 := lRepList.AddReport;
      r2.Assign(r);
      r2.VmrTag := '';
    end;
  end;
begin
  if not CheckVmrActive then Exit;
  lRepList := TrwReportList.Create;
  lRepList.Descr := RepList.Descr;
  a := TVmrSettings.Create;
  try
    for i := 0 to RepList.Count-1 do
    begin
      if (RepList[i].Level = 'S') and IntegerListed(RepList[i].NBR,RegularCheckReportNbrs{ [220,230,579,732,232,877,757,929,240,838,1103,1142,1193,1459,1113]})
        //and (RepList[i].NBR <> 1602) // Pressure Seal Legal New Check form has no "Datasets" parameter
        then
        ProcessEeChecks(RepList[i], a.FindEeSettings(ctx_DataAccess.ClientID), ctx_DataAccess.ClientID, 'PR_CHECK_MB_GROUP_NBR')
      else
      if (RepList[i].Level = 'S') and IntegerListed(RepList[i].NBR, MiscCheckReportNbrs {[221,231,580,733,233,878,756,930,239,839,1104]}) then
        ProcessMiscChecks(RepList[i], a.FindCoSettings(ctx_DataAccess.ClientID), ctx_DataAccess.ClientID)
      else
      begin
        sRepAncestor := ctx_RWLocalEngine.GetRepAncestor(RepList[i].NBR, RepList[i].Level);
        if (sRepAncestor = 'TrmlEeCheckReport') or (sRepAncestor = 'TrmlBaseCheckReport') or (sRepAncestor = 'TrwlCustomPrCheck') then
           ProcessEeChecks(RepList[i], a.FindEeSettings(ctx_DataAccess.ClientID), ctx_DataAccess.ClientID, 'PR_CHECK_MB_GROUP_NBR')
        else
        if (sRepAncestor = 'TrwlDBDTReport') or (sRepAncestor = 'TrwlSimpleDBDTReport') then
          ProcessEeReport(RepList[i], a.FindEeSettings(ExctractClientId(RepList[i])), ExctractClientId(RepList[i]),
            'PR_EE_REPORT_MB_GROUP_NBR', 'PR_EE_REPORT_SEC_MB_GROUP_NBR',
            ['Employees', 'EE_NBR', 'Companies', 'CO_NBR', 'Divisions', 'CO_DIVISION_NBR',
            'Branches', 'CO_BRANCH_NBR', 'Departments', 'CO_DEPARTMENT_NBR', 'Teams', 'CO_TEAM_NBR'])
        else
        if sRepAncestor = 'TrwlPrEeFiltReport' then
          ProcessEeReport(RepList[i], a.FindEeSettings(ExctractClientId(RepList[i])), ExctractClientId(RepList[i]),
            'PR_EE_REPORT_MB_GROUP_NBR', 'PR_EE_REPORT_SEC_MB_GROUP_NBR',
            ['Employees', 'EE_NBR', 'Companies', 'CO_NBR'])
        else
        if sRepAncestor = 'TrwlPayrollReport' then
          ProcessEeReport(RepList[i], a.FindCoSettings(ExctractClientId(RepList[i])), ExctractClientId(RepList[i]),
            'PR_REPORT_MB_GROUP_NBR', 'PR_REPORT_SEC_MB_GROUP_NBR',
            ['Companies', 'CO_NBR'])
        else
        if sRepAncestor = 'TrwlTaxCouponReport' then
          ProcessMiscReport(RepList[i], a.FindCoSettings(ctx_DataAccess.ClientID), ctx_DataAccess.ClientID,
            'PR_REPORT_MB_GROUP_NBR', '')
        else
          ProcessMiscReport(RepList[i], a.FindCoSettings(ctx_DataAccess.ClientID), ctx_DataAccess.ClientID,
            'PR_REPORT_MB_GROUP_NBR', 'PR_REPORT_SEC_MB_GROUP_NBR');
      end;
    end;
    RepList.Assign(lRepList);
    VmrCalcExtraInfo(nil);
    VmrCalcExtraInfo(RepList);
  finally
    lRepList.Free;
    a.Free;
  end;
end;

function TevVmrEngine.VmrPostProcessReportResult(const Res: TrwReportResults): Boolean;
begin
  Result := InternalVmrPostProcessReportResult(Res, False);
end;

function TevVmrEngine.VmrStorePostProcessReportResult(const ms: IEvDualStream): Boolean;
var
  Res: TrwReportResults;
begin
  Result := False;
  if not CheckVmrActive then Exit;
  Res := TrwReportResults.Create;
  try
    ms.Position := 0;
    Res.SetFromStream(ms);
    Result := VmrStorePostProcessReportResult(Res);
  finally
    Res.Free;
  end;
end;

function TevVmrEngine.VmrStorePostProcessReportResult(const Res: TrwReportResults): Boolean;
begin
  Result := False;
  if not CheckVmrActive then Exit;
  Result := InternalVmrPostProcessReportResult(Res, True);
end;

procedure TevVmrEngine.ProcessRevert(const TopBoxNbr: Integer);
begin
  SVmrDaemonFuncs.ProcessRevert(TopBoxNbr);
end;


{ TVmrSettings }

function TVmrSettings.CheckVmrActive(const ClNbr: Integer): Boolean;
var
  p: PSettingRecord;
begin
  p := FindClientSettings(ClNbr);
  if VarIsEmpty(p^.VmrStatus) then
  try
    ctx_DataAccess.OpenClient(ClNbr);
    DM_CLIENT.CL.DataRequired('');
    p^.VmrStatus := CheckVmrSetupActive;
  except
    p^.VmrStatus := UnAssigned;
    raise;
  end;
  Result := p^.VmrStatus;
end;

constructor TVmrSettings.Create;
begin
  SetLength(a, 0);
end;

destructor TVmrSettings.Destroy;
var
  i: Integer;
begin
  for i := 0 to High(a) do
  begin
    FreeAndNil(a[i].CoSettings);
    FreeAndNil(a[i].EeSettings);
    FreeAndNil(a[i].DbdtSettings);
  end;
  inherited;
end;

function TVmrSettings.FindClientSettings(
  const ClNbr: Integer): PSettingRecord;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to High(a) do
    if a[i].ClNbr = ClNbr then
    begin
      Result := @(a[i]);
      Break;
    end;
  if not Assigned(Result) then
  begin
    SetLength(a, Length(a)+1);
    a[High(a)].ClNbr := ClNbr;
    Result := @(a[High(a)]);
  end;
end;

function TVmrSettings.FindCoSettings(
  const ClNbr: Integer): TevClientDataSet;
var
  p: PSettingRecord;
begin
  p := FindClientSettings(ClNbr);
  if not Assigned(p^.CoSettings) then
  try
    ctx_DataAccess.OpenClient(ClNbr);
    p^.CoSettings := TevClientDataSet.Create(nil);
    with TExecDSWrapper.Create('VmrCoSettings') do
      ctx_DataAccess.GetCustomData(p^.CoSettings, AsVariant);
  except
    FreeAndNil(p^.CoSettings);
    raise;
  end;
  Result := p^.CoSettings;
end;

function TVmrSettings.FindEeSettings(
  const ClNbr: Integer): TevClientDataSet;
var
  p: PSettingRecord;
begin
  p := FindClientSettings(ClNbr);
  if not Assigned(p^.EeSettings) then
  try
    ctx_DataAccess.OpenClient(ClNbr);
    p^.EeSettings := TevClientDataSet.Create(nil);
    with TExecDSWrapper.Create('VmrEeSettings') do
      ctx_DataAccess.GetCustomData(p^.EeSettings, AsVariant);
  except
    FreeAndNil(p^.EeSettings);
    raise;
  end;
  Result := p^.EeSettings;
end;

procedure TevVmrEngine.VmrCalcExtraInfo(
  const RepList: TrwReportList);
var
  i: Integer;
  a: TVmrSettings;
  function IntegerListed(const v: Integer; const a: array of Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to High(a) do
      if v = a[i] then
      begin
        Result := True;
        Break;
      end;
  end;

  procedure ProcessEeChecks(const r: TrwRepParams);
  var
    cdEe,cdEeWebPassword: TevClientDataSet;

    procedure CreateSelfServeEEs{(aCD:TevClientDataSet;v:Variant)};
        var
          i:integer;
          EEs :TstringList;
    begin
          EEs := GetFieldValueList(cdEE, 'EE_NBR', True);
          try
            for i:=EEs.Count - 1 downto 0 do
            begin
              if not cdEeWebPassword.FindKey([StrToIntDef(EEs[i], 0)]) then
                EEs.Delete(i);
            end;
            FChecksExtraInfo.Add(EEs.CommaText);
          finally
            EEs.Free;
          end;
    end;
    procedure GetEEsByCheckNbrs(CheckNbrs:Variant);
    var
      i: Integer;
      {lBoxes2,} ChecksList: TStringList;
    begin
      ChecksList := TStringList.Create;
      for i := VarArrayLowBound(CheckNbrs,1) to VarArrayHighBound(CheckNbrs,1) do
      begin
          if Length(string(CheckNbrs[i])) >0 then
             ChecksList.Add(CheckNbrs[i]);
      end;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
          SetMacro('COLUMNS', 'EE_NBR,Pr_Check_Nbr,NET_WAGES');
          SetMacro('TABLENAME', 'PR_CHECK');
          if Trim(ChecksList.CommaText) <>'' then
            SetMacro('CONDITION', BuildInSqlStatement('Pr_Check_Nbr', ChecksList.CommaText))
          else
            SetMacro('CONDITION', '(' + AlwaysFalseCond + ')');
          ctx_DataAccess.GetCustomData(cdEe, AsVariant);
      end;
    end;
  var
    v: Variant;
    c:string;
  begin
    if a.CheckVmrActive(ctx_DataAccess.ClientID) then
    begin
      cdEe := TevClientDataSet.Create(nil);
      cdEeWebPassword := TevClientDataSet.Create(nil);
      with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
      begin
            c := 't1.E_MAIL_ADDRESS <> '''' and t1.E_MAIL_ADDRESS is not null and '+
                 't1.SELFSERVE_ENABLED <> ''N'' and ' +
                 't1.SELFSERVE_USERNAME <>'''' and t1.SELFSERVE_USERNAME is not null and '+
                 't1.SELFSERVE_PASSWORD <>'''' and t1.SELFSERVE_PASSWORD is not null ';
            SetMacro('COLUMNS', 'EE_NBR, t1.E_MAIL_ADDRESS EMAIL');
            SetMacro('TABLE1', 'EE');
            SetMacro('TABLE2', 'CL_PERSON');
            SetMacro('JOINFIELD', 'CL_PERSON');
            SetMacro('CONDITION', c);
            ctx_DataAccess.GetCustomData(cdEeWebPassword, AsVariant);
      end;
      cdEeWebPassword.IndexFieldNames := 'EE_NBR';
      try
        if R.Params.ParamByName('DataSets') <> nil then
        begin
          v := R.Params.ParamByName('DataSets').Value;
          cdEe.Data := v[0];
          v := Unassigned;
          CreateSelfServeEEs;
        end
        else
        begin
          if R.Params.ParamByName('CheckNbrs') <> nil then
          begin
            GetEEsByCheckNbrs(R.Params.ParamByName('CheckNbrs').Value);
            CreateSelfServeEEs;
          end;
        end;
      finally
        cdEe.Free;
      end;
    end;
  end;
begin
  if not Assigned(FChecksExtraInfo) then
     FChecksExtraInfo := TisStringList.Create;
  if RepList = nil then
  begin
    FChecksExtraInfo.Clear;
    exit;
  end;
  if not CheckVmrActive then Exit;
  a := TVmrSettings.Create;
  try
    for i := 0 to RepList.Count-1 do
    begin
      if (RepList[i].Level = 'S') and IntegerListed(RepList[i].NBR,RegularCheckReportNbrs) then
        ProcessEeChecks(RepList[i])
      else
        FChecksExtraInfo.Add('');
    end;
  finally
    a.Free;
  end;

end;

function TevVmrEngine.GetChecksExtraInfo: IisStringList;
begin
    Result := FChecksExtraInfo;
end;

procedure TevVmrEngine.SetChecksExtraInfo(const EInfo: IisStringList);
begin
   FChecksExtraInfo := EInfo;
end;


initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVmrEngine, IevVmrEngine, 'VMR Engine');

end.



