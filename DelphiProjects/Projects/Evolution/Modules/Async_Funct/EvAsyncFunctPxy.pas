unit EvAsyncFunctPxy;

interface

implementation

uses SysUtils, EvCommonInterfaces, isBaseClasses, EvMainboard, evRemoteMethods,
     evProxy, EvTransportInterfaces, EvContext, EvConsts;

type
  TevAsyncFunctionsProxy = class(TevProxyModule, IevAsyncFunctions)
  protected
    function  MakeCall(const AFunctionName: String; const AParams: IisListOfValues; const APriority: Integer;
                       var ACallID: TisGUID): Boolean;
    function  SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
    function  GetCalcCapacity: Integer;
    procedure SetCalcCapacity(const AValue: Integer);
    procedure TerminateCall(const ACallID: TisGUID);
    function  GetExecSatus: IisList; //list of IevAsyncCallInfo
  end;


function GetAsyncFunctionsProxy: IevAsyncFunctions;
begin
  Result := TevAsyncFunctionsProxy.Create;
end;

{ TevAsyncFunctionsProxy }

function TevAsyncFunctionsProxy.GetCalcCapacity: Integer;
begin
  Result := 0;
  Assert(False);
end;

function TevAsyncFunctionsProxy.GetExecSatus: IisList;
begin
  NotRemotableMethod;
end;

function TevAsyncFunctionsProxy.MakeCall(const AFunctionName: String;
  const AParams: IisListOfValues; const APriority: Integer; var ACallID: TisGUID): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ASYNCFUNCTIONS_MAKECALL);
  D.Params.AddValue('AFunctionName', AFunctionName);
  D.Params.AddValue('AParams', AParams);
  D.Params.AddValue('APriority', APriority);
  D.Params.AddValue('ACallID', ACallID);

  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
  ACallID := D.Params.Value['ACallID'];  
end;

procedure TevAsyncFunctionsProxy.SetCalcCapacity(const AValue: Integer);
begin
  Assert(False);
end;

function TevAsyncFunctionsProxy.SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ASYNCFUNCTIONS_SETCALLPRIORITY);
  D.Params.AddValue('ACallID', ACallID);
  D.Params.AddValue('ANewPriority', ANewPriority);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevAsyncFunctionsProxy.TerminateCall(const ACallID: TisGUID);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_ASYNCFUNCTIONS_TERMINATECALL);
  D.Params.AddValue('ACallID', ACallID);
  D := SendRequest(D, False);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetAsyncFunctionsProxy, IevAsyncFunctions, 'Async Functions');

end.
