unit EvAsyncFunctMod;

interface

uses Classes, SyncObjs, Windows, SysUtils, DateUtils, isBaseClasses, isBasicUtils, EvCommonInterfaces, EvMainboard,
     isThreadManager, EvContext, EvConsts, SReportSettings, EvStreamUtils, Math, Variants, isExceptions,
       EvUtils, SDataStructure, EvTypes, isErrorUtils, EvAsyncCall, EvDataSet, EvComponentsExt,
     SFieldCodeValues, EvClasses, EvBasicUtils, ISZippingRoutines,DB, EvExceptions, EvClientDataSet;

implementation

uses isDataSet, EvExchangeConsts, ISKbmMemDataSet, SDataDictbureau, EvDataAccessComponents,
  SDataDicttemp;

type
  TevAsyncFunctions = class(TisInterfacedObject, IevAsyncFunctions)
  private
    FAsyncCallHandler: IevAsyncCalls;
    function CollectDashboardResults(aRes: TrwReportResults): IisListOfValues;
    procedure RegisterFunctions;

 // ************** Async functions **************
    // Reports
    function  RunReports(const AParams: IisListOfValues): IisListOfValues;

    //Create Payroll
    function  PrepareForPayrollCreation(const AParams: IisListOfValues): IisListOfValues;
    function  CreatePayroll(const AParams: IisListOfValues): IisListOfValues;
    function  AfterCreatePayroll(const AParams: IisListOfValues): IisListOfValues;

    // PR Process
    function  PrepareForPayrollProcessing(const AParams: IisListOfValues): IisListOfValues;
    function  ProcessPayroll(const AParams: IisListOfValues): IisListOfValues;
    function  PrintPayroll(const AParams: IisListOfValues): IisListOfValues;
    function  ReprintPayroll(const AParams: IisListOfValues): IisListOfValues;

    // Tax Payments
    function  PayTaxes(const AParams: IisListOfValues): IisListOfValues;
    function  PrintChecksAndCouponsForTaxes(const AParams: IisListOfValues): IisListOfValues;
    function  FinalizeTaxPayments(const AParams: IisListOfValues): IisListOfValues;

    // ACH

    function  PrepareCashManagement(const AParams: IisListOfValues): IisListOfValues;
    function  ProcessCashManagementForCompany(const AParams: IisListOfValues): IisListOfValues;
    function  FinalizeCashManagement(const AParams: IisListOfValues): IisListOfValues;
    function  UpdateAchStatus(const AParams: IisListOfValues): IisListOfValues;
    function  ProcessPrenoteACH(const AParams: IisListOfValues): IisListOfValues;
    function  ProcessManualACH(const AParams: IisListOfValues): IisListOfValues;

    // Rebuild Temp Tables
    function  PrepareForRebuildTempTables(const AParams: IisListOfValues): IisListOfValues;
    function  RebuildTempTables(const AParams: IisListOfValues): IisListOfValues;
    function  FinalizeRebuildAllClients(const AParams: IisListOfValues): IisListOfValues;

    // QEC
    function  QuarterEndCleanup(const AParams: IisListOfValues): IisListOfValues;
    function  PAMonthEndCleanup(const AParams: IisListOfValues): IisListOfValues;

    // Preprocess for Quarter End
    function  PrepareQuarterEndPreprocCompanyFilter(const AParams: IisListOfValues): IisListOfValues;
    function  PreprocessQuarterEnd(const AParams: IisListOfValues): IisListOfValues;

    //  ACA Update
    function  PrepareTaskCompanyFilter(const AParams: IisListOfValues): IisListOfValues;
    function  ProcessACAUpdate(const AParams: IisListOfValues): IisListOfValues;

    // Rebuild VMR History
    function  PrepareCompsRebuildVMRHist(const AParams: IisListOfValues): IisListOfValues;
    function  RebuildVMRHistory(const AParams: IisListOfValues): IisListOfValues;

    // Process Tax Returns
    function  PrepareForProcessTaxReturns(const AParams: IisListOfValues): IisListOfValues;
    function  ProcessTaxReturns(const AParams: IisListOfValues): IisListOfValues;

    // EvoX Engine
    function  EvoXEngineImport(const AParams: IisListOfValues): IisListOfValues;
    function  EvoXEngineCheckMapFile(const AParams: IisListOfValues): IisListOfValues;

    // Benefits Enrollment
    function  PrepareForGetBenefitEmailNotifications(const AParams: IisListOfValues): IisListOfValues;
    function  GetBenefitEmailNotifications(const AParams: IisListOfValues): IisListofValues;

// *********************************************

  protected
    procedure DoOnConstruction; override;
    function  MakeCall(const AFunctionName: String; const AParams: IisListOfValues; const APriority: Integer;
                       var ACallID: TisGUID): Boolean;
    function  SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
    function  GetCalcCapacity: Integer;
    procedure SetCalcCapacity(const AValue: Integer);
    procedure TerminateCall(const ACallID: TisGUID);
    function  GetExecSatus: IisList; //list of IevAsyncCallInfo
  public
    destructor Destroy; override;
  end;


  TevAsyncCallInfo = class(TisInterfacedObject, IevAsyncCallInfo)
  private
    FName: String;
    FCallID: TisGUID;
    FPriority: Integer;
    FUser: String;
    FStartTime: TDateTime;
  protected
    function  Name: String;
    function  CallID: TisGUID;
    function  Priority: Integer;
    function  User: String;
    function  StartTime: TDateTime;
  end;


function GetAsyncFunctions: IevAsyncFunctions;
begin
  Result := TevAsyncFunctions.Create;
end;


{ TevAsyncFunctions }

destructor TevAsyncFunctions.Destroy;
begin
  FAsyncCallHandler := nil;
  inherited;
end;

procedure TevAsyncFunctions.DoOnConstruction;
begin
  inherited;
  FAsyncCallHandler := TevAsyncCalls.Create;
  RegisterFunctions;
end;

function TevAsyncFunctions.RunReports(const AParams: IisListOfValues): IisListOfValues;
var
  Stream: IevDualStream;
  MTParams: IisListOfValues;
  EmptyInt: IInterface;
  RepList: TrwReportList;
  Res: TrwReportResults;
  i: Integer;
  s: String;
  RunControl: TrwReportParam;
  Phase: TReportRequestType;
  DashboardData: IisListOfValues;

{  procedure CollectDashboardResults;
  var
    i: Integer;
    RepRes: TrwReportResult;
    repParam: TrwReportParam;
  begin
    // Collect dashboard data
    DashboardData := TisListOfValues.Create;
    for i := 0 to Res.Count - 1 do
    begin
      RepRes := Res[i];
      repParam := RepRes.ReturnValues.ParamByName(__DASHBOARD);
      if (RepRes.ErrorMessage = '') and Assigned(repParam) then
        if Boolean(repParam.Value) then
        begin
          repParam := RepRes.ReturnValues.ParamByName(__DASHBOARD_DATA);
          if Assigned(repParam) then
            DashboardData.AddValue(IntToStr(DashboardData.Count), repParam.Value);
        end;
    end;

    if DashboardData.Count = 0 then
      DashboardData := nil;
  end;}

begin
  EmptyInt := nil;
  MTParams := IInterface(AParams.TryGetValue('MTParams', EmptyInt)) as IisListOfValues;
  Stream := IInterface(AParams.Value['ReportList']) as IevDualStream;
  Result := TisListOfValues.Create;

  RepList := TrwReportList.Create;
  try
    Stream.Position := 0;
    RepList.SetFromStream(Stream);

    // Initialization of multithreaded reports
    if Assigned(MTParams) then
      for i := 0 to RepList.Count - 1 do
        RepList[i].Params.Add(__RUN_CONTROL, VarArrayOf([MTParams.Value['Funct'],
                                                         MTParams.Value['Phase'],
                                                         MTParams.Value['Params'],
                                                         VarArrayOf([0]),
                                                         null]));

    Res := ctx_RWRemoteEngine.RunReports2(RepList);  // RUN REPORT

    try
      // Add Errors and Warnings
      s := Res.GetAllExceptions;
      if s <> '' then
        Result.AddValue(PARAM_ERROR, s);
      s := Res.GetAllWarnings;
      if s <> '' then
        Result.AddValue(PARAM_WARNING, s);


      if not Assigned(MTParams) then
        Stream := Res.GetAsStream  // Report Result

      else
      begin
        // Analyze MT output
        Phase := MTParams.Value['Phase'];
        MTParams.Clear;
        RunControl := Res[0].ReturnValues.ParamByName(__RUN_CONTROL);
        if Assigned(RunControl) then
        begin
          Assert(Res.Count = 1, 'Multithreaded report should run as a standalone report');
          if VarArrayHighBound(RunControl.Value, 1) >= 4 then
          begin
            MTParams.AddValue('Funct', RunControl.Value[0]);
            MTParams.AddValue('Phase', RunControl.Value[1]);
            MTParams.AddValue('Result', RunControl.Value[4]);
          end;
          RunControl.Free;  // Result is converted in better format, so delete it.
        end;

        if MTParams.Count > 0 then
        begin
          Result.AddValue('MTParams', MTParams);

          if Phase = __DO_PARALLEL then
            Stream := nil
          else
          begin
            // make next phase report (output params should be passed further)
            for i := 0 to Res[0].ReturnValues.Count - 1 do
              RepList[0].Params.Add(Res[0].ReturnValues[i].Name, Res[0].ReturnValues[i].Value);
            Stream := RepList.GetAsStream; // Report for running next phase (except parallel)
          end;

          if MTParams.Value['Phase'] = __DO_PARALLEL then
          begin
            // make parallel report (need to cleanup redundant data)
            RepList[0].Params.Add('Clients', VarArrayOf([0]));  // it's important to have dummy item!
            RepList[0].Params.Add('Companies', VarArrayOf([0]));// it deceive  AllCompanies report logic
            MTParams.AddValue('Report', RepList.GetAsStream);   // Report for running parallel phase
          end;
        end
        else
        begin
          DashboardData := CollectDashboardResults(Res);
          Stream := Res.GetAsStream;  // Report Result
        end;
      end;

    finally
      FreeAndNil(Res);
    end;

  finally
    RepList.Free;
  end;

  Result.AddValue(PARAM_RESULT, Stream);

  if Assigned(DashboardData) then
    Result.AddValue('DashboardData', DashboardData);
end;

function TevAsyncFunctions.GetCalcCapacity: Integer;
begin
  Result := FAsyncCallHandler.GetCalcCapacity;
end;

function TevAsyncFunctions.MakeCall(const AFunctionName: String; const AParams: IisListOfValues;
  const APriority: Integer; var ACallID: TisGUID): Boolean;
begin
  Result := FAsyncCallHandler.MakeCall(AFunctionName, AParams, APriority, ACallID);
end;


procedure TevAsyncFunctions.RegisterFunctions;
begin
  FAsyncCallHandler.RegisterCall('RunReports', Self, RunReports);

  FAsyncCallHandler.RegisterCall('PrepareForPayrollCreation', Self, PrepareForPayrollCreation);
  FAsyncCallHandler.RegisterCall('CreatePayroll', Self, CreatePayroll);
  FAsyncCallHandler.RegisterCall('AfterCreatePayroll', Self, AfterCreatePayroll);

  FAsyncCallHandler.RegisterCall('PrepareForPayrollProcessing', Self, PrepareForPayrollProcessing);
  FAsyncCallHandler.RegisterCall('ProcessPayroll', Self, ProcessPayroll);
  FAsyncCallHandler.RegisterCall('PrintPayroll', Self, PrintPayroll);
  FAsyncCallHandler.RegisterCall('ReprintPayroll', Self, ReprintPayroll);

  FAsyncCallHandler.RegisterCall('PayTaxes', Self, PayTaxes);
  FAsyncCallHandler.RegisterCall('PrintChecksAndCouponsForTaxes', Self, PrintChecksAndCouponsForTaxes);
  FAsyncCallHandler.RegisterCall('FinalizeTaxPayments', Self, FinalizeTaxPayments);

  FAsyncCallHandler.RegisterCall('UpdateAchStatus', Self, UpdateAchStatus);

  FAsyncCallHandler.RegisterCall('PrepareCashManagement', Self, PrepareCashManagement);
  FAsyncCallHandler.RegisterCall('ProcessCashManagementForCompany', Self, ProcessCashManagementForCompany);
  FAsyncCallHandler.RegisterCall('FinalizeCashManagement', Self, FinalizeCashManagement);

  FAsyncCallHandler.RegisterCall('ProcessPrenoteACH', Self, ProcessPrenoteACH);
  FAsyncCallHandler.RegisterCall('ProcessManualACH', Self, ProcessManualACH);

  FAsyncCallHandler.RegisterCall('PrepareForRebuildTempTables', Self, PrepareForRebuildTempTables);
  FAsyncCallHandler.RegisterCall('RebuildTempTables', Self, RebuildTempTables);
  FAsyncCallHandler.RegisterCall('FinalizeRebuildAllClients', Self, FinalizeRebuildAllClients);

  FAsyncCallHandler.RegisterCall('QuarterEndCleanup', Self, QuarterEndCleanup);
  FAsyncCallHandler.RegisterCall('PAMonthEndCleanup', Self, PAMonthEndCleanup);

  FAsyncCallHandler.RegisterCall('PrepareQuarterEndPreprocCompanyFilter', Self, PrepareQuarterEndPreprocCompanyFilter);
  FAsyncCallHandler.RegisterCall('PreprocessQuarterEnd', Self, PreprocessQuarterEnd);

  FAsyncCallHandler.RegisterCall('PrepareCompsRebuildVMRHist', Self, PrepareCompsRebuildVMRHist);
  FAsyncCallHandler.RegisterCall('RebuildVMRHistory', Self, RebuildVMRHistory);

  FAsyncCallHandler.RegisterCall('PrepareForProcessTaxReturns', Self, PrepareForProcessTaxReturns);
  FAsyncCallHandler.RegisterCall('ProcessTaxReturns', Self, ProcessTaxReturns);

  FAsyncCallHandler.RegisterCall('EvoXEngineImport', Self, EvoXEngineImport);
  FAsyncCallHandler.RegisterCall('EvoXEngineCheckMapFile', Self, EvoXEngineCheckMapFile);

  FAsyncCallHandler.RegisterCall('PrepareForGetBenefitEmailNotifications', Self, PrepareForGetBenefitEmailNotifications);
  FAsyncCallHandler.RegisterCall('GetBenefitEmailNotifications', Self, GetBenefitEmailNotifications);

  FAsyncCallHandler.RegisterCall('PrepareTaskCompanyFilter', Self, PrepareTaskCompanyFilter);
  FAsyncCallHandler.RegisterCall('ProcessACAUpdate', Self, ProcessACAUpdate);
end;

procedure TevAsyncFunctions.SetCalcCapacity(const AValue: Integer);
begin
  FAsyncCallHandler.SetCalcCapacity(AValue);
end;


function TevAsyncFunctions.SetCallPriority(const ACallID: TisGUID; const ANewPriority: Integer): Integer;
begin
  Result := FAsyncCallHandler.SetCallPriority(ACallID, ANewPriority);
end;

procedure TevAsyncFunctions.TerminateCall(const ACallID: TisGUID);
begin
  FAsyncCallHandler.TerminateCall(ACallID);
end;

function TevAsyncFunctions.PrepareForPayrollProcessing(const AParams: IisListOfValues): IisListOfValues;
var
  CoList: String;
  AllPayrolls: Boolean;
  PrList: String;
  PayrollAge: TDateTime;
  L: String;
  d: TDateTime;
  ClId: Integer;
  S: String;
  Q: IevQuery;
  PrInfo: IisListOfValues;
  i: Integer;
begin
  AllPayrolls := AParams.Value['AllPayrolls'];
  CoList := AParams.Value['CoList'];
  PrList := AParams.Value['PrList'];
  PayrollAge := AParams.Value['PayrollAge'];

  if AllPayrolls then
  begin
    PrList := '';
    with TExecDSWrapper.Create('GenericSelectWithCondition') do
    begin
      SetMacro('TableName', 'TMP_PR');
      SetMacro('Columns', '*');
      SetMacro('Condition', 'STATUS = ''' + PAYROLL_STATUS_COMPLETED + ''' and STATUS_DATE <= :StatusDate' + GetReadOnlyClintCompanyListFilter);
      d := Now - PayrollAge;
      SetParam('StatusDate', d);
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
      try
        DM_TEMPORARY.TMP_PR.Data := ctx_DataAccess.TEMP_CUSTOM_VIEW.Data;
        DM_TEMPORARY.TMP_PR.IndexFieldNames := 'PROCESS_PRIORITY;CHECK_DATE;RUN_NUMBER';
        DM_TEMPORARY.TMP_PR.First;
        while not DM_TEMPORARY.TMP_PR.Eof do
        begin
          if (CoList = '') or
             (Pos(' ' + DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').AsString + ';' + DM_TEMPORARY.TMP_PR.FieldByName('CO_NBR').AsString + ' ', ' ' + CoList + ' ') > 0) then
            PrList := PrList + DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').AsString + ';' + DM_TEMPORARY.TMP_PR.FieldByName('PR_NBR').AsString + ' ';
          DM_TEMPORARY.TMP_PR.Next;
        end;
      finally
        DM_TEMPORARY.TMP_PR.IndexFieldNames := '';
        DM_TEMPORARY.TMP_PR.Close;
      end;
    end;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('PrList', PrList);  
  L := PrList;
  i := 0;
  if Trim(L) <> '' then
    repeat
      Inc(i);
      PrInfo := TisListOfValues.Create;
      S := GetNextStrValue(L, ' ');
      ClId := StrToInt(GetNextStrValue(S, ';'));

      if pos('*', S) > 0 then
      begin
        Q := TevQuery.Create('SELECT T1.CL_NBR, T1.CO_NBR, T1.PR_NBR, T1.CHECK_DATE, T1.RUN_NUMBER, T2.NAME ' +
                             'FROM TMP_PR t1, TMP_CO t2 WHERE t1.CL_NBR = t2.CL_NBR AND t1.CO_NBR = t2.CO_NBR ' +
                             'AND  t1.CL_NBR = :CL_NBR AND t1.PR_NBR = :PR_NBR');
        Q.Params.AddValue('CL_NBR', ClId);
        Q.Params.AddValue('PR_NBR', StrToInt(copy(S, 1, pos('*', S) - 1)));

        PrInfo.AddValue('ClNbr', Q.Result.FieldByName('CL_NBR').AsInteger);
        PrInfo.AddValue('CoNbr', Q.Result.FieldByName('CO_NBR').AsInteger);
        PrInfo.AddValue('PrNbr', Q.Result.FieldByName('PR_NBR').AsInteger);
        PrInfo.AddValue('PrBatchNbr', StrToInt(Trim(copy(S, pos('*', S)+1, 10))));
        PrInfo.AddValue('CheckDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);
        PrInfo.AddValue('RunNumber', Q.Result.FieldByName('RUN_NUMBER').AsInteger);
        PrInfo.AddValue('CompanyName', Trim(Q.Result.FieldByName('Name').AsString));

        Result.AddValue('PrInfo' + IntToStr(i), PrInfo);
      end
      else
      begin
        Q := TevQuery.Create('SELECT T1.CL_NBR, T1.CO_NBR, T1.PR_NBR, T1.CHECK_DATE, T1.RUN_NUMBER, T2.NAME ' +
                             'FROM TMP_PR t1, TMP_CO t2 WHERE t1.CL_NBR = t2.CL_NBR AND t1.CO_NBR = t2.CO_NBR ' +
                             'AND  t1.CL_NBR = :CL_NBR AND t1.PR_NBR = :PR_NBR');
        Q.Params.AddValue('CL_NBR', ClId);
        Q.Params.AddValue('PR_NBR', StrToInt(S));

        PrInfo.AddValue('ClNbr', Q.Result.FieldByName('CL_NBR').AsInteger);
        PrInfo.AddValue('CoNbr', Q.Result.FieldByName('CO_NBR').AsInteger);
        PrInfo.AddValue('PrNbr', Q.Result.FieldByName('PR_NBR').AsInteger);
        PrInfo.AddValue('CheckDate', Q.Result.FieldByName('CHECK_DATE').AsDateTime);
        PrInfo.AddValue('RunNumber', Q.Result.FieldByName('RUN_NUMBER').AsInteger);
        PrInfo.AddValue('CompanyName', Trim(Q.Result.FieldByName('Name').AsString));

        Result.AddValue('PrInfo' + IntToStr(i), PrInfo);
      end;
    until Trim(L) = '';


  Q := TevQuery.Create('GenericSelectCurrent');
  Q.Macros.AddValue('TABLENAME', 'SB');
  Q.Macros.AddValue('COLUMNS', 'DEVELOPMENT_FTP_PASSWORD');
  Result.AddValue('EmailTo', Q.Result.Fields[0].AsString);
end;

function TevAsyncFunctions.ProcessPayroll(const AParams: IisListOfValues): IisListOfValues;
var
  Log, Warnings: String;
  PrBatchNbr: Integer;
begin
  Log := '';

  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  if AParams.ValueExists('PrBatchNbr') then
    PrBatchNbr := AParams.Value['PrBatchNbr']
  else
    PrBatchNbr := 0;

  Warnings := ctx_PayrollProcessing.ProcessPayroll(AParams.Value['PayrollNumber'],
                                   AParams.Value['JustPreProcess'],
                                   Log,
                                   AParams.Value['CheckReturnQueue'],
                                   TResourceLockType(Integer(AParams.Value['LockType'])),
                                   PrBatchNbr);

  Result := TisListOfValues.Create;
  Result.AddValue('Log', Trim(Log));
  Result.AddValue(PARAM_WARNING, Warnings);
end;

function TevAsyncFunctions.PrintPayroll(const AParams: IisListOfValues): IisListOfValues;
var
  s: String;
  RepRes: IevDualStream;
  j: Integer;
  DashboardData: IisListOfValues;
  RepResultsDash: TrwReportResults;

  function TrimInvisibleCharacters(const S: String): String;
  begin
    Result := S;
    while Pos(' ', Result) <> 0 do
      Delete(Result, Pos(' ', Result), 1);
    while Pos(#10, Result) <> 0 do
      Delete(Result, Pos(#10, Result), 1);
    while Pos(#13, Result) <> 0 do
      Delete(Result, Pos(#13, Result), 1);
  end;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  RepRes := ctx_PayrollProcessing.PrintPayroll(AParams.Value['PrNbr'], AParams.Value['PrintChecks'],
                                           AParams.Value['UseSbMiskCheckForm']);

// ticket 102738 - memory leak
//  DashboardData := CollectDashboardResults( TrwReportResults.Create( RepRes ) );
  RepResultsDash := TrwReportResults.Create( RepRes );
  try
    DashboardData := CollectDashboardResults( RepResultsDash ); // Dashboard data still must be collected
  finally
    RepResultsDash.Free;
  end;

  Result := TisListOfValues.Create;

  if Assigned(RepRes) then
  begin
    RepRes.Position := RepRes.Size - SizeOf(j);
    j := RepRes.ReadInteger;
    RepRes.Position := j;
    s := RepRes.ReadString;

    if TrimInvisibleCharacters(s) <> '' then
      Result.AddValue(PARAM_ERROR, s);

    s := RepRes.ReadString;
    if TrimInvisibleCharacters(s) <> '' then
      Result.AddValue({PARAM_ERROR}PARAM_WARNING, s);

    RepRes.Size := j; // cut off exceptions and warnings
  end;

  Result.AddValue(PARAM_RESULT, RepRes);
  if Assigned(DashboardData) then
    Result.AddValue('DashboardData', DashboardData);
  if (ctx_VmrEngine <> nil) and (Context.License.VMR) and (ctx_VmrEngine.GetChecksExtraInfo <> nil) then
  begin
    Result.AddValue('CHECKS_EXTRA_INFO', ctx_VmrEngine.GetChecksExtraInfo);
    ctx_VmrEngine.SetChecksExtraInfo(TisStringList.Create);
  end;
end;

function TevAsyncFunctions.ReprintPayroll(const AParams: IisListOfValues): IisListOfValues;
var
  RepRes: IevDualStream;
  sWarnings, sErrors: String;
  DashboardData: IisListOfValues;
  RepResultsDashboard: TrwReportResults;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  RepRes := ctx_PayrollProcessing.ReprintPayroll(AParams.Value['PrNbr'],
                                   AParams.Value['PrintReports'],
                                   AParams.Value['PrintInvoices'],
                                   AParams.Value['CurrentTOA'],
                                   AParams.Value['PayrollChecks'],
                                   AParams.Value['AgencyChecks'],
                                   AParams.Value['TaxChecks'],
                                   AParams.Value['BillingChecks'],
                                   AParams.Value['CheckForm'],
                                   AParams.Value['MiscCheckForm'],
                                   AParams.Value['CheckDate'],
                                   AParams.Value['SkipCheckStubCheck'],
                                   AParams.Value['DontPrintBankInfo'],
                                   AParams.Value['HideBackground'],
                                   AParams.Value['DontUseVMRSettings'],
                                   AParams.Value['ForcePrintVoucher'],
                                   AParams.Value['PrReprintHistoryNBR'],
                                   AParams.Value['Reason'],
                                   sWarnings, sErrors);
// ticket 102378 - memory leak on printing
//  DashboardData := CollectDashboardResults( TrwReportResults.Create( RepRes ) );
  RepResultsDashboard := TrwReportResults.Create( RepRes );
  try
    DashboardData := CollectDashboardResults( RepResultsDashboard);
  finally
    RepResultsDashboard.Free;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, RepRes);

  if Assigned(DashboardData) then
    Result.AddValue('DashboardData', DashboardData);

  if (ctx_VmrEngine <> nil) and (Context.License.VMR) and (ctx_VmrEngine.GetChecksExtraInfo <> nil) then
  begin
    Result.AddValue('CHECKS_EXTRA_INFO', ctx_VmrEngine.GetChecksExtraInfo);
    ctx_VmrEngine.SetChecksExtraInfo(TisStringList.Create);
  end;

  if sWarnings <> '' then
    Result.AddValue(PARAM_WARNING, sWarnings);
  if sErrors <> '' then
    Result.AddValue(PARAM_ERROR, sErrors);
end;

function TevAsyncFunctions.PayTaxes(const AParams: IisListOfValues): IisListOfValues;
var
  ResStream: IevDualStream;
  Res: TTaxPaymentParams;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  ResStream := ctx_RemoteMiscRoutines.PayTaxes(
      AParams.Value['FedTaxLiabilityList'],
      AParams.Value['StateTaxLiabilityList'],
      AParams.Value['SUITaxLiabilityList'],
      AParams.Value['LocalTaxLiabilityList'],
      AParams.Value['ForceChecks'],
      AParams.Value['EftpReference'],
      AParams.Value['SbTaxPaymentNbr'],
      AParams.Value['PostProcessReportName']);

  Result := TisListOfValues.Create;
  Res := TTaxPaymentParams.Create(ResStream);
  try
    if Res.Warnings.Text <> '' then
      Result.AddValue(PARAM_WARNING, Res.Warnings.Text);
    Result.AddValue(PARAM_RESULT, ResStream);
    Result.AddValue('TaxPayrollFilter', Res.TaxPayrollFilter);
  finally
    Res.Free;
  end;
end;

function TevAsyncFunctions.PrintChecksAndCouponsForTaxes(const AParams: IisListOfValues): IisListOfValues;
var
  RepRes: TrwReportResults;
  RepStr: IEvDualStream;
  TaxPaymentRes: TTaxPaymentParams;
  TaxPaymentResStr: IEvDualStream;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  RepRes := ctx_RemoteMiscRoutines.PrintChecksAndCouponsForTaxes(AParams.Value['TaxPayrollFilter'],AParams.Value['DefaultMiscCheckForm']);
  Result := TisListOfValues.Create;
  try
    RepStr := RepRes.GetAsStream;
    if RepRes.GetAllWarnings <> '' then
      Result.AddValue(PARAM_WARNING, RepRes.GetAllWarnings);
    if RepRes.GetAllExceptions <> '' then
      Result.AddValue(PARAM_ERROR, RepRes.GetAllExceptions);
  finally
    RepRes.Free;
  end;

  TaxPaymentResStr := IInterface(AParams.Value['TaxPaymentResult']) as IEvDualStream;
  TaxPaymentResStr.Position := 0;
  TaxPaymentRes := TTaxPaymentParams.Create(TaxPaymentResStr);
  try
    TaxPaymentRes.Checks := RepStr;
    TaxPaymentResStr := TaxPaymentRes.AsMemStream;
    Result.AddValue(PARAM_RESULT, TaxPaymentResStr);
  finally
    TaxPaymentRes.Free;
  end;
end;

function TevAsyncFunctions.FinalizeTaxPayments(const AParams: IisListOfValues): IisListOfValues;
var
  ResStr: IEvDualStream;
begin
  ResStr := ctx_RemoteMiscRoutines.FinalizeTaxPayments(IInterface(AParams.Value['Params']) as IevDualStream,
                                                        IInterface(AParams.Value['ACHOptions.ini']) as IisStringList,
                                                        AParams.Value['SbTaxPaymentNbr'],
                                                        AParams.Value['PostProcessReportName'],
                                                        AParams.Value['DefaultMiscCheckForm']);
  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, ResStr);
end;

function TevAsyncFunctions.PrepareCashManagement(const AParams: IisListOfValues): IisListOfValues;
var
  SbData: IisListOfValues;
  Companies: IisParamsCollection;
  Payrolls: IevDataset;
  Options: IevAchOptions;
  Exceptions: String;
begin
  try
    SbData := TisListOfValues.Create;
    Companies := TisParamsCollection.Create;
    Result := TisListOfValues.Create;
    Payrolls := TevDataset.Create;
    Payrolls.Data := IInterface(AParams.Value['ACHDataStream']) as IisStream;
    Options := IInterface(AParams.Value['Options']) as IevAchOptions;
    ctx_CashManagement.PrepareCashManagement(Payrolls, Options, SbData, Companies);
  except
    on E: Exception do
      Exceptions := E.Message;
  end;

  Result.AddValue('Companies', Companies);
  Result.AddValue('SbData', SbData);

  if Exceptions <> '' then
    Result.AddValue(PARAM_ERROR, Exceptions);
end;

function TevAsyncFunctions.ProcessCashManagementForCompany(const AParams: IisListOfValues): IisListOfValues;
var
  CompanyData: IisListOfValues;
  Options: IevAchOptions;
  SbData: IisListOfValues;
  Exceptions: String;
  IncludeOffsets: Integer;
begin
  try
    CompanyData := IInterface(AParams.Value['CompanyData']) as IisListOfValues;
    SbData := IInterface(AParams.Value['SbData']) as IisListOfValues;
    Options := IInterface(AParams.Value['Options']) as IevAchOptions;
    IncludeOffsets := AParams.Value['IncludeOffsets'];
    ctx_CashManagement.ProcessCashManagementForCompany(IncludeOffsets, CompanyData, Options, SbData);
  except
    on E: Exception do
      Exceptions := E.Message;
  end;
  Result := TisListOfValues.Create;
  Result.AddValue('CompanyData', CompanyData);

  if Exceptions <> '' then
    Result.AddValue(PARAM_ERROR, Exceptions);
end;

function TevAsyncFunctions.FinalizeCashManagement(
  const AParams: IisListOfValues): IisListOfValues;
var
  AchFileReport: IisStream;
  AchRegularReport: IisStream;
  AchDetailedReport: IisStream;
  NonAchReport: IisStream;
  WTFile: IisStream;
  FileName: String;
  AchSave: IisListOfValues;
  Exceptions: String;
  Warnings: String;
  Companies: IisListOfValues;
begin
  try
    Companies := IInterface(AParams.Value['Companies']) as IisListOfValues;
    if Companies.Count > 0 then
      ctx_CashManagement.FinalizeCashManagement(
        Companies,
        IInterface(AParams.Value['Options']) as IevACHOptions,
        IInterface(AParams.Value['SbData']) as IisListOfValues,
        AParams.Value['IncludeOffsets'],
        AParams.Value['Preprocess'],
        AchFileReport,
        AchRegularReport,
        AchDetailedReport,
        NonAchReport,
        WTFile,
        FileName,
        AchSave,
        Exceptions,
        Warnings,
        AParams.Value['From'],
        AParams.Value['To'],
        AParams.Value['ACHFolder']);
  except
    on E: Exception do
      Exceptions := E.Message;
  end;

  Result := TisListOfValues.Create;

  Result.AddValue('AchFile', AchFileReport);
  Result.AddValue('AchRegularReport', AchRegularReport);
  Result.AddValue('AchDetailedReport', AchDetailedReport);
  Result.AddValue('AchSave', AchSave);
  Result.AddValue('AchFileName', FileName);
  Result.AddValue('NonAchReport', NonAchReport);
  Result.AddValue('WTFile', WTFile);

  if Exceptions <> '' then
    Result.AddValue(PARAM_ERROR, Exceptions);

  if Warnings <> '' then
    Result.AddValue(PARAM_WARNING, Warnings);

end;

function TevAsyncFunctions.UpdateAchStatus(
  const AParams: IisListOfValues): IisListOfValues;
var
  Payrolls: TevClientDataset;
  ACHDataStream: IevDualStream;
  sExceptions: String;
begin
  try
    Payrolls := TevClientDataSet.Create(nil);
    try
      ACHDataStream := IInterface(AParams.Value['ACHDataStream']) as IevDualStream;
      ACHDataStream.Position := 0;
      Payrolls.LoadFromUniversalStream(ACHDataStream);
      ctx_CashManagement.UpdateAchStatus(Payrolls, AParams.Value['StatusToIndex']);
    finally
      Payrolls.Free;
    end;
  except
    on E: Exception do
      sExceptions := E.Message;
  end;

  Result := TisListOfValues.Create;

  if sExceptions <> '' then
    Result.AddValue(PARAM_ERROR, sExceptions);

end;

function TevAsyncFunctions.ProcessPrenoteACH(
  const AParams: IisListOfValues): IisListOfValues;
var
  AchFile, AchRegularReport, AchDetailedReport: IEvDualStream;
  AchSave: IisListOfValues;
  sExceptions: String;
  AchFileName: String;
begin
  ctx_PayrollProcessing.ProcessPrenoteACH(
    IInterface(AParams.Value['ACHOptions']) as IevACHOptions,
    AParams.Value['PrenoteCLAccounts'],
    IInterface(AParams.Value['ACHDataStream']) as IevDualStream,
    AchFile, AchRegularReport, AchDetailedReport, AchSave, AchFileName, sExceptions,
    AParams.Value['ACHFolder']);

  Result := TisListOfValues.Create;
  Result.AddValue('AchFile', AchFile);
  Result.AddValue('AchRegularReport', AchRegularReport);
  Result.AddValue('AchDetailedReport', AchDetailedReport);
  Result.AddValue('AchSave', AchSave);
  Result.AddValue('AchFileName', AchFileName);

  if sExceptions <> '' then
    Result.AddValue(PARAM_ERROR, sExceptions);
end;

function TevAsyncFunctions.PrepareForRebuildTempTables(const AParams: IisListOfValues): IisListOfValues;
var
  sCl: String;
  i: Integer;
  Clients, TempClients: IisStringList;
  FullRebuild, UseRange, UseNumbers: Boolean;
  FromClient, ToClient: Integer;
  ClientNumbers: String;
begin
  ctx_StartWait('Preparing for temp tables rebuilding');
  try
    FullRebuild := AParams.Value['FullRebuild'];
    UseRange := AParams.Value['UseRange'];
    UseNumbers := AParams.Value['UseNumbers'];
    FromClient := AParams.Value['FromClient'];
    ToClient := AParams.Value['ToClient'];
    ClientNumbers := AParams.Value['ClientNumbers'];

    if not FullRebuild then
    begin
      ctx_UpdateWait('Cleaning Deleted Clients');
      ctx_DBAccess.CleanDeletedClientsFromTemp;
    end;

    ctx_UpdateWait('Defining client list');

    Clients := TisStringList.CreateUnique;
    TempClients := TisStringList.CreateUnique;
    TempClients.Text := ctx_DBAccess.GetClientsList(True, True);

    if FullRebuild then
      Clients.Text := ctx_DBAccess.GetClientsList(True)
    else
    if UseRange then
      Clients.Text := ctx_DBAccess.GetClientsList;

    if not FullRebuild then
    begin
      if UseRange then
        for i := Clients.Count - 1 downto 0 do
          if not ((StrToInt(Clients[i]) >= FromClient) and (StrToInt(Clients[i]) <= ToClient)) then
            Clients.Delete(i);

      if UseNumbers then
      begin
        ClientNumbers := Trim(ClientNumbers);
        while ClientNumbers <> '' do
        begin
          sCl := Trim(GetNextStrValue(ClientNumbers, ','));
          Clients.Append(sCl);
        end;
      end;
    end

    else
    begin
      ctx_UpdateWait('Creating blank temp tables');
      ctx_DBAccess.BeginRebuildAllClients;
    end;

    Result := TisListOfValues.Create;
    Result.AddValue(PARAM_RESULT, Clients.Text);
    Result.AddValue('TEMPCLIENTS', TempClients.Text);
  finally
    ctx_EndWait;
  end;
end;

function TevAsyncFunctions.RebuildTempTables(const AParams: IisListOfValues): IisListOfValues;
begin
  Result := TisListOfValues.Create;
  try
    ctx_DBAccess.RebuildTemporaryTables(AParams.Value['ClientID'], AParams.Value['AllClientsMode'], AParams.Value['tempClientsMode']);
  except
    on E: Exception do
      Result.AddValue(PARAM_ERROR, E.Message);
  end;
end;

function TevAsyncFunctions.CreatePayroll(const AParams: IisListOfValues): IisListOfValues;
var
  TcFileOut: IevDualStream;
  L: IisStringList;
  EEs: Variant;
  EEList: String;
  i, eeCount: Integer;
begin
  TcFileOut := TevDualStreamHolder.Create;

  EEs := null;
  EEList := AParams.Value['EENumbers'];
  if EEList <> '' then
  begin
    eeCount := 0;
    for i := 1 to Length(EEList) do
      if EEList[i] = ',' then
        Inc(eeCount);
    EEs := VarArrayCreate([0, Pred(eeCount)], varInteger);

    i := 0;
    while EEList <> '' do
    begin
      EEs[i] := StrToInt(GetNextStrValue(EEList, ','));
      Inc(i);
    end;
   AParams.Value['EENumbers'] := EEs;
  end
  else
   AParams.Value['EENumbers'] := null;

  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  ctx_PayrollProcessing.CreatePRBatchDetails(
    AParams.Value['PrBatchNumber'],
    AParams.Value['TwoChecksPerEE'],
    AParams.Value['CalcCheckLines'],
    AParams.Value['CheckType'],
    AParams.Value['Check945Type'],
    AParams.Value['NumberOfChecks'],
    AParams.Value['SalaryOrHourly'],
    AParams.Value['EENumbers'],
    IInterface(AParams.Value['TcFileIn']) as IevDualStream,
    TcFileOut,
    AParams.Value['TcLookupOption'],
    AParams.Value['TcDBDTOption'],
    AParams.Value['TcFourDigitYear'],
    AParams.Value['TcFileFormat'],
    AParams.Value['TcAutoImportJobCodes'],
    AParams.Value['TcUseEmployeePayRates'],
    AParams.Value['UpdateBalance'],
    AParams.Value['IncludeTimeOffRequests']);

  Result := TisListOfValues.Create;

  L := TisStringList.Create;
  TcFileOut.Position := 0;
  L.LoadFromStream(TcFileOut.RealStream);
  Result.AddValue(PARAM_WARNING, L.Text);
end;

function TevAsyncFunctions.PrepareForPayrollCreation(const AParams: IisListOfValues): IisListOfValues;
var
  sFileName: string;
  NextCheckDate: TDateTime;
  FreqList: TStringList;
  I: Integer;
  COFreq: String;
  B: Boolean;
  dBegin, dEnd: TDateTime;
  sBatchFreq: String;
  iBlock401K: Integer;
  iBlockACH: Integer;
  iBlockAgencies: Integer;
  iBlockBilling: Integer;
  iBlockChecks: Integer;
  iBlockTaxDeposits: Integer;
  iBlockTimeOff: Integer;
  sCheckComments: String;
  dCheckDate: TDateTime;
  iCheckDateReplace: Integer;
  sCheckFreq: String;
  iCLNbr: Integer;
  iCONbr: Integer;
  sEEList: string;
  iLoadDefaults: Integer;
  iPayHours: Integer;
  iPaySalary: Integer;
  dPeriodBeginDate: TDateTime;
  dPeriodEndDate: TDateTime;
  iPRFilterNbr: Integer;
  iPRTemplateNbr: Integer;
  sTcImportSource: string;

  procedure CreateBatch(const Freq: string; const dBegin, dEnd: TDateTime);
  var
    R: IisListOfValues;
  begin
    with DM_PAYROLL do
    begin
      PR_BATCH.Insert;
      PR_BATCH.FieldByName('FREQUENCY').Value := Freq;
      PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := dBegin;
      PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      PR_BATCH.FieldByName('PERIOD_END_DATE').Value := dEnd;
      PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      if iPaySalary = 0 then
        PR_BATCH.FieldByName('PAY_SALARY').Value := 'Y'
      else
        PR_BATCH.FieldByName('PAY_SALARY').Value := 'N';
      if iPayHours = 0 then
        PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'Y'
      else
        PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N';
      if iLoadDefaults = 0 then
        PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'Y'
      else
        PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'N';
      if iPRTemplateNbr <> 0  then
        PR_BATCH.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').Value := iPRTemplateNbr;
      if iPRFilterNbr <> 0  then
        PR_BATCH.FieldByName('CO_PR_FILTERS_NBR').Value := iPRFilterNbr;
      PR_BATCH.Post;
      ctx_DataAccess.PostDataSets([PR_BATCH]);

      sFileName := IncludeTrailingPathDelimiter(sTcImportSource)+
          DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '_' +
          FormatDateTime('MMDDYYYY', PR.FieldByName('CHECK_DATE').AsDateTime)+ '.txt';

      R := TisListOfValues.Create;
      R.AddValue('PrNbr', DM_PAYROLL.PR.PR_NBR.AsInteger);
      R.AddValue('PrBatchNbr', PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger);
      R.AddValue('CompanyNumber', DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
      R.AddValue('CheckDate', PR.FieldByName('CHECK_DATE').AsString);
      R.AddValue('ImportFileName', sFileName);

      Result.AddValue('Batch' + PR_BATCH.FieldByName('PR_BATCH_NBR').AsString, R);
    end;
  end;
begin
  sBatchFreq := AParams.Value['BatchFreq'];
  iBlock401K := AParams.Value['Block401K'];
  iBlockACH := AParams.Value['BlockACH'];
  iBlockAgencies := AParams.Value['BlockAgencies'];
  iBlockBilling := AParams.Value['BlockBilling'];
  iBlockChecks := AParams.Value['BlockChecks'];
  iBlockTaxDeposits := AParams.Value['BlockTaxDeposits'];
  iBlockTimeOff := AParams.Value['BlockTimeOff'];
  sCheckComments := AParams.Value['CheckComments'];
  dCheckDate := AParams.Value['CheckDate'];
  iCheckDateReplace := AParams.Value['CheckDateReplace'];
  sCheckFreq := AParams.Value['CheckFreq'];
  iClNbr := AParams.Value['ClNbr'];
  iCoNbr := AParams.Value['CoNbr'];
  sEEList := AParams.Value['EEList'];
  iLoadDefaults := AParams.Value['LoadDefaults'];
  iPayHours := AParams.Value['PayHours'];
  iPaySalary := AParams.Value['PaySalary'];
  dPeriodBeginDate := AParams.Value['PeriodBeginDate'];
  dPeriodEndDate := AParams.Value['PeriodEndDate'];
  iPRFilterNbr := AParams.Value['PRFilterNbr'];
  iPRTemplateNbr := AParams.Value['PRTemplateNbr'];
  sTcImportSource := AParams.Value['TcImportSource'];

  Result := TisListOfValues.Create;

  try
    ctx_DataAccess.OpenClient(iCLNbr);
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    try
      with DM_PAYROLL do
      begin
        DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(iCONbr));
        NextCheckDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
        if NextCheckDate = 0  then
          raise ENoNextCheckDate.CreateHelp('Creating PR for CO #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ': There are no more check dates in the calendar available.', IDH_InconsistentData);

        PR.DataRequired('PR_NBR=0');
        PR_BATCH.DataRequired('PR_BATCH_NBR=0');
        PR_CHECK.DataRequired('PR_CHECK_NBR=0');
        PR_CHECK_STATES.DataRequired('PR_CHECK_STATES_NBR=0');
        PR_CHECK_SUI.DataRequired('PR_CHECK_SUI_NBR=0');
        PR_CHECK_LOCALS.DataRequired('PR_CHECK_LOCALS_NBR=0');
        PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR=0');

        PR.Insert;

        PR.FieldByName('PAYROLL_COMMENTS').Value := sCheckComments;
        if dCheckDate = 0 then
          PR.FieldByName('CHECK_DATE').Value := NextCheckDate
        else
          PR.FieldByName('CHECK_DATE').Value := dCheckDate;
        PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_NORMAL;



        case iCheckDateReplace of
          0: PR.FieldByName('SCHEDULED').Value := 'Y';
          1:
          begin
            PR.FieldByName('SCHEDULED').Value := 'Y';
            DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
            PR_SCHEDULED_EVENT.IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
            PR_SCHEDULED_EVENT.Locate('SCHEDULED_CHECK_DATE', ctx_PayrollCalculation.NextCheckDate(Date, True), []);
            PR_SCHEDULED_EVENT.Edit;
            PR_SCHEDULED_EVENT.SCHEDULED_CHECK_DATE.Value := dCheckDate;
            PR_SCHEDULED_EVENT.Post;
          end;
          2:
          begin
            PR.FieldByName('SCHEDULED').AsString := 'N';
            PR.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
            // exclude frequency based time-off
            PR.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
          end;
          end;
          PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_REGULAR;
          PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING;
          if iBlockAgencies = 0 then
            PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y'
          else
            PR.FieldByName('EXCLUDE_AGENCY').Value := 'N';
          if iBlock401K = 0 then
            PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'Y'
          else
            PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
            
          if iBlockACH = 0 then
          begin
            PR.FieldByName('EXCLUDE_ACH').Value := 'Y';
            PR.TAX_IMPOUND.Value := PAYMENT_TYPE_NONE;
            PR.TRUST_IMPOUND.Value := PAYMENT_TYPE_NONE;
            PR.BILLING_IMPOUND.Value := PAYMENT_TYPE_NONE;
            PR.DD_IMPOUND.Value := PAYMENT_TYPE_NONE;
            PR.WC_IMPOUND.Value := PAYMENT_TYPE_NONE;
          end
          else
          begin
            PR.FieldByName('EXCLUDE_ACH').Value := 'N';
            PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
            PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
            PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
            PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
            PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
          end;

          if iBlockBilling = 0 then
            PR.FieldByName('EXCLUDE_BILLING').Value := 'Y'
          else
            PR.FieldByName('EXCLUDE_BILLING').Value := 'N';

          case iBlockTaxDeposits of
            0: PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := GROUP_BOX_DEPOSIT;
            1: PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := GROUP_BOX_LIABILITIES;
            2: PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := GROUP_BOX_BOTH2;
            3: PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := GROUP_BOX_NONE;
          end;

          case iBlockChecks of
            0: PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_REPORTS;
            1: PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_CHECKS;
            2: PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_BOTH2;
            3: PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_NONE;
          end;

          case iBlockTimeOff of
            0: PR.FieldByName('EXCLUDE_TIME_OFF').Value := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
            1: PR.FieldByName('EXCLUDE_TIME_OFF').Value := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
            2: PR.FieldByName('EXCLUDE_TIME_OFF').Value := TIME_OFF_EXCLUDE_ACCRUAL_NONE;
          end;

          PR_SCHEDULED_EVENT.DisableControls;
          try
            PR.Post;
            ctx_DataAccess.PostDataSets([PR, PR_SCHEDULED_EVENT]);
          finally
            ctx_DataAccess.CancelDataSets([PR_SCHEDULED_EVENT]);
            PR_SCHEDULED_EVENT.EnableControls;
          end;

          if sBatchFreq = '' then
          begin
            COFreq := Copy(#13 + PayFrequencies_ComboChoices, Pos(#13 + sCheckFreq + #9, #13 + PayFrequencies_ComboChoices) + Length(#13 + sCheckFreq + #9), 1);
            FreqList := BreakCompanyFreqIntoPayFreqList(COFreq);
            try
              for I := 0 to FreqList.Count - 1 do
              begin
                B := ctx_PayrollCalculation.FindNextPeriodBeginEndDate;
                if B then
                  dBegin := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE']
                else
                  dBegin := ctx_PayrollCalculation.NextPeriodBeginDate;
                if B then
                  dEnd := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE']
                else
                  dEnd := ctx_PayrollCalculation.NextPeriodEndDate;
                CreateBatch(FreqList[i], dBegin, dEnd);
              end;
            finally
              FreqList.Free;
            end;
          end
          else
            CreateBatch(sBatchFreq, dPeriodBeginDate, dPeriodEndDate);
      end;

      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;

  finally
    DM_COMPANY.CO.Close;
    DM_PAYROLL.PR.Close;
    DM_PAYROLL.PR_BATCH.Close;
    DM_PAYROLL.PR_CHECK.Close;
    DM_PAYROLL.PR_CHECK_STATES.Close;
    DM_PAYROLL.PR_CHECK_SUI.Close;
    DM_PAYROLL.PR_CHECK_LOCALS.Close;
    DM_PAYROLL.PR_CHECK_LINES.Close;
  end;
end;

function TevAsyncFunctions.AfterCreatePayroll(const AParams: IisListOfValues): IisListOfValues;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  DM_PAYROLL.PR.DataRequired('PR_NBR = ' +  IntToStr(AParams.Value['PrNbr']));
  try
    DM_PAYROLL.PR.Edit;
    DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
    DM_PAYROLL.PR.Post;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
  finally
    DM_PAYROLL.PR.Close;
  end;
end;

function TevAsyncFunctions.QuarterEndCleanup(const AParams: IisListOfValues): IisListOfValues;
var
  Res: Boolean;
  Q: IevQuery;
  PrNbr: Integer;
  Warnings: String;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  Res := ctx_PayrollProcessing.DoQuarterEndCleanup(
    AParams.Value['CoNbr'],
    AParams.Value['QTREndDate'],
    AParams.Value['MinTax'],
    AParams.Value['CleanupAction'],
    AParams.Value['ReportFilePath'],
    AParams.Value['EECustomNbr'],
    AParams.Value['ForceCleanup'],
    AParams.Value['PrintReports'],
    AParams.Value['CurrentQuarterOnly'],
    TResourceLockType(AParams.Value['LockType']),
    AParams.Value['ExtraOptions'],
    Warnings);

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, Res);
  Result.AddValue(PARAM_WARNING, Warnings);
  PrNbr := 0;

  if Res then
  begin
    Q := TevQuery.Create('SELECT PR_NBR, Max(RUN_NUMBER) FROM TMP_PR WHERE CL_NBR = :cl_nbr AND CO_NBR = :co_nbr ' +
                         'AND STATUS = ''' + PAYROLL_STATUS_PROCESSED + ''' ' +
                         'AND PAYROLL_TYPE = ''' + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + ''' ' +
                         'AND CHECK_DATE = :Check_Date ' +
                         'GROUP BY PR_NBR');
    Q.Params.AddValue('cl_nbr', AParams.Value['ClientID']);
    Q.Params.AddValue('co_nbr', AParams.Value['CoNbr']);
    Q.Params.AddValue('Check_Date', AParams.Value['QTREndDate']);

    PrNbr :=  Q.Result.FieldByName('PR_NBR').AsInteger;
  end;

  Result.AddValue('PayrollNumber', PrNbr);
end;

function TevAsyncFunctions.PrepareTaskCompanyFilter(const AParams: IisListOfValues): IisListOfValues;
var
  Filter: IevACATaskCompanyFilter;
  QCoList: IevQuery;

  function GetCoList(const ClNbr:integer):string;
  var
     cfilter : IisStringList;
  begin
    result:= '-1';
    cfilter:= TisStringList.Create;

    QCoList.Result.SetRange([ClNbr],[ClNbr]);
    QCoList.Result.First;
    while not QCoList.Result.Eof do
    begin
      cfilter.Add(QCoList.Result.FieldByName('CO_NBR').AsString);
      QCoList.Result.Next;
    end;
    if cfilter.Count > 0 then
      result:= cfilter.CommaText;
  end;

begin
  Filter := TevACATaskCompanyFilter.Create;
  try
    QCoList := TevQuery.Create(' SELECT CL_NBR, CO_NBR, CUSTOM_COMPANY_NUMBER FROM TMP_CO order by CL_NBR,CO_NBR' );
    QCoList.Execute;
    QCoList.Result.IndexFieldNames:= 'CL_NBR';

    DM_TEMPORARY.TMP_CL.DataRequired(GetReadOnlyClintCompanyListFilter);
    DM_TEMPORARY.TMP_CL.First;
    while not DM_TEMPORARY.TMP_CL.Eof do
    begin
        with Filter.Add do
        begin
          ClNbr := DM_TEMPORARY.TMP_CL['CL_NBR'];
          CoFilter:= GetCoList(DM_TEMPORARY.TMP_CL['CL_NBR']);
        end;

      DM_TEMPORARY.TMP_CL.Next;
    end;
  finally
    DM_TEMPORARY.TMP_CL.Close;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, (Filter as IisInterfacedObject).AsStream);
end;

function TevAsyncFunctions.ProcessACAUpdate(const AParams: IisListOfValues): IisListOfValues;
var
  Check_b, Check_e, Hire_b, Hire_e, StartDate: TDateTime;
  MaxUnpaidWeeks: integer;
  ACAResult: IevACATaskCompanyEE;
  Q2731, Q2834, Q2713, Q2713M1, Q2713New,Q2713NewM1, QEE: IevQuery;
  ds2834, ds2713, ds2713New: IevDataset;

  F2731, F2834, F2713, F2713New: IisStringList;
  aHour, aMeasurementDays, tmp:integer;
  Count2713, mCount2713, mCountNew2713: integer;
  sACA_STATUS: string;

  procedure s2731;
  begin
        Q2731 := TevQuery.Create(
' SELECT t1.EE_NBR, t1.CUSTOM_EMPLOYEE_NUMBER, cp.FIRST_NAME, cp.LAST_NAME'#13 +
'     FROM Cl_Person cp, Ee t1'#13 +
'     left join'#13 +
'       ( SELECT  distinct t2.Ee_Nbr FROM    Pr t5,    Pr_Check  t2'#13 +
'         WHERE CURRENT_DATE >= t5.EFFECTIVE_DATE and CURRENT_DATE < t5.EFFECTIVE_UNTIL  and'#13 +
'                CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL  and'#13 +
'                t5.Co_Nbr = :CoNbr AND'#13 +
'                t5.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                t5.Check_Date BETWEEN :check_b AND :check_e AND'#13 +
'                t5.PAYROLL_TYPE IN (''R'', ''S'', ''V'', ''C'', ''B'', ''K'') AND t5.Status = ''P'''#13 +
''#13 +
'         UNION ALL'#13 +
''#13 +
'      SELECT distinct t2.Ee_Nbr FROM Pr  t5, Pr_Check  t2, Pr_Check_Lines t10, Cl_E_Ds  t11'#13 +
'      WHERE'#13 +
'        CURRENT_DATE >= t5.EFFECTIVE_DATE and CURRENT_DATE < t5.EFFECTIVE_UNTIL  and'#13 +
'        :check_e >= t2.EFFECTIVE_DATE and :check_e < t2.EFFECTIVE_UNTIL  and'#13 +
'        CURRENT_DATE >= t10.EFFECTIVE_DATE and CURRENT_DATE < t10.EFFECTIVE_UNTIL  and'#13 +
'        CURRENT_DATE >= t11.EFFECTIVE_DATE and CURRENT_DATE < t11.EFFECTIVE_UNTIL  and'#13 +
'        t5.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'        t2.Pr_Check_Nbr = t10.Pr_Check_Nbr  AND'#13 +
'        t11.Cl_E_Ds_Nbr = t10.Cl_E_Ds_Nbr AND'#13 +
'        EXTRACTYear(t5.Check_Date) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) AND'#13 +
'        t5.PAYROLL_TYPE in (''E'', ''I'') AND t5.Status = ''P'' AND'#13 +
'        t5.Co_Nbr = :CoNbr  AND'#13 +
'        t10.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'        t11.E_D_Code_Type = ''M1'' AND Trim(Upper(t11.Description)) = ''ACA HOURS'''#13 +
''#13 +
'        ) t4 on t1.Ee_Nbr = t4.Ee_Nbr'#13 +
' WHERE CURRENT_DATE >= cp.EFFECTIVE_DATE and CURRENT_DATE < cp.EFFECTIVE_UNTIL  and'#13 +
'       CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL  and'#13 +
'       cp.Cl_Person_Nbr = t1.Cl_Person_Nbr AND'#13 +
'       t1.CO_NBR = :CoNbr and'#13 +
'       t4.Ee_Nbr IS NULL AND'#13 +
'       t1.COMPANY_OR_INDIVIDUAL_NAME = ''I'' AND'#13 +
'       t1.ACA_STATUS <>  ''E'' AND'#13 +
'       t1.ACA_STATUS <>  ''N'' AND'#13 +
'       t1.Current_Termination_Code IN (''A'', ''S'', ''P'' ) AND'#13 +
'       t1.CURRENT_HIRE_DATE < :check_b'#13 +
'       order by t1.EE_NBR');
        Q2731.Params.AddValue('check_b', Check_b);
        Q2731.Params.AddValue('check_e', Check_e);
        Q2731.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        Q2731.Execute;
        Q2731.Result.IndexFieldNames:= 'EE_Nbr';
  end;

  procedure s2834;
  var
   QWeek: IevQuery;

   procedure CalculateWeek;
   var
    PrevDate, Last_Check_Date: TDateTime;
    WeeksUnpaid, WeeksPaid, PayrollDays: integer;
    b2834 : boolean;
   begin
        WeeksPaid:= 0;
        Last_Check_Date:= QWeek.Result['Check_Date'];
        PrevDate := QWeek.Result['Check_Date'];

        WeeksUnpaid:= RoundInt64((Trunc(Check_e) - Trunc(QWeek.Result.FieldByName('Check_Date').AsDateTime))/7);

        if QWeek.Result.FieldByName('Pay_Frequency').AsString[1]  = 'D' then
          PayrollDays := 1
        else if QWeek.Result.FieldByName('Pay_Frequency').AsString[1] = 'W' then
          PayrollDays := 7
        else if QWeek.Result.FieldByName('Pay_Frequency').AsString[1] = 'B' then
          PayrollDays := 14
        else if QWeek.Result.FieldByName('Pay_Frequency').AsString[1] = 'S' then
          PayrollDays := 15
        else if QWeek.Result.FieldByName('Pay_Frequency').AsString[1] = 'M' then
          PayrollDays := 30
        else if QWeek.Result.FieldByName('Pay_Frequency').AsString[1] = 'Q' then
          PayrollDays := 89
        else
          PayrollDays := 14;

        While not QWeek.Result.Bof do
        begin
          if ((PrevDate - QWeek.Result['Check_Date']) > (PayrollDays + 28)) then
          begin
            WeeksPaid:= RoundInt64((Trunc(Last_Check_Date) - Trunc(PrevDate) + PayrollDays) /7);
            break;
          end
          else
            PrevDate := QWeek.Result['Check_Date'];

          QWeek.Result.Prior;
        end;

        if WeeksPaid = 0 then
        begin
          if PrevDate - Check_b > PayrollDays + 28 then
            WeeksPaid:= RoundInt64((Trunc(Last_Check_Date) - Trunc(PrevDate) + PayrollDays)/7)
          else
            WeeksPaid:= RoundInt64(Trunc(Last_Check_Date) - Trunc(Check_b)/7);
        end;

       b2834:= false;
       if (WeeksUnpaid <= WeeksPaid ) then
       begin
          if (WeeksUnpaid > MaxUnpaidWeeks) then
           b2834:= true;
       end
       else
          if WeeksUnpaid >= 4 then
            b2834:= true;

        if b2834 then
        begin
          ds2834.Append;
          ds2834['EE_NBR'] := Q2834.Result['EE_NBR'];
          ds2834['CUSTOM_EMPLOYEE_NUMBER'] := Q2834.Result['CUSTOM_EMPLOYEE_NUMBER'];
          ds2834['FIRST_NAME'] := Q2834.Result['FIRST_NAME'];
          ds2834['LAST_NAME'] := Q2834.Result['LAST_NAME'];
          ds2834.post;
        end;
   end;

  begin
        QWeek := TevQuery.Create(
        'SELECT DISTINCT  t3.EE_NBR, t8.Pay_Frequency, t3.Check_Date Check_Date FROM Ee t8,'#13 +
        '   (   SELECT t5.EE_NBR, t4.Check_Date  Check_Date'#13 +
        '       FROM  Pr  t4, Pr_Check  t5'#13 +
        '       WHERE'#13 +
        '          CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
        '          CURRENT_DATE >= t5.EFFECTIVE_DATE and CURRENT_DATE < t5.EFFECTIVE_UNTIL and'#13 +
        '          t4.Pr_Nbr = t5.Pr_Nbr AND'#13 +
        '         t4.PAYROLL_TYPE IN (''R'', ''S'', ''V'', ''C'', ''B'', ''K'') AND'#13 +
        '         t4.Check_Date BETWEEN :check_b AND :check_e AND'#13 +
        '         t4.Co_Nbr = :Co_Nbr'#13 +
        ''#13 +
        '     UNION ALL'#13 +
        ''#13 +
        '       SELECT  t5.EE_NBR,  t12.Line_Item_Date  Check_Date'#13 +
        '       FROM  Pr  t4, Pr_Check  t5, Pr_Check_Lines  t12, Cl_E_Ds  t13'#13 +
        '       WHERE'#13 +
        '          CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
        '          CURRENT_DATE >= t5.EFFECTIVE_DATE and CURRENT_DATE < t5.EFFECTIVE_UNTIL and'#13 +
        '          CURRENT_DATE >= t12.EFFECTIVE_DATE and CURRENT_DATE < t12.EFFECTIVE_UNTIL and'#13 +
        '          CURRENT_DATE >= t13.EFFECTIVE_DATE and CURRENT_DATE < t13.EFFECTIVE_UNTIL and'#13 +
        '          t4.Pr_Nbr = t5.Pr_Nbr  AND'#13 +
        '          t5.Pr_Check_Nbr = t12.Pr_Check_Nbr  AND'#13 +
        '          t13.Cl_E_Ds_Nbr = t12.Cl_E_Ds_Nbr'#13 +
        '         AND'#13 +
        '         t4.PAYROLL_TYPE IN (''E'', ''I'') AND'#13 +
        '         EXTRACTYear(CAST(t4.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) AND'#13 +
        '         t4.Co_Nbr = :Co_Nbr  AND'#13 +
        '         t12.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
        '         t13.E_D_Code_Type = ''M1'' AND Trim(Upper(t13.Description)) = ''ACA HOURS'''#13 +
        '   )  t3'#13 +
        'WHERE'#13 +
        '   :check_e >= t8.EFFECTIVE_DATE and :check_e < t8.EFFECTIVE_UNTIL and'#13 +
        '   t8.Ee_Nbr = t3.EE_NBR AND'#13 +
        '   t8.Co_Nbr = :Co_Nbr'#13 +
        'ORDER BY'#13 +
        '   1, 3 DESC');

        QWeek.Params.AddValue('check_b', Check_b);
        QWeek.Params.AddValue('check_e', Check_e);
        QWeek.Params.AddValue('Co_Nbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        QWeek.Execute;
        QWeek.Result.IndexFieldNames:= 'EE_Nbr;Check_Date';


        Q2834 := TevQuery.Create(
        'SELECT e.EE_NBR, e.CUSTOM_EMPLOYEE_NUMBER, cp.FIRST_NAME, cp.LAST_NAME'#13 +
        'FROM Cl_PERSON cp, EE e'#13 +
        'WHERE'#13 +
        '   CURRENT_DATE >= cp.EFFECTIVE_DATE and CURRENT_DATE < cp.EFFECTIVE_UNTIL and'#13 +
        '   :check_e >= e.EFFECTIVE_DATE and :check_e < e.EFFECTIVE_UNTIL and'#13 +
        '   cp.CL_PERSON_NBR = e.CL_PERSON_NBR  AND'#13 +
        '   e. Current_Termination_Code IN (''A'', ''S'', ''P'') and'#13 +
        '   e.CURRENT_HIRE_DATE < :check_b and'#13 +
        '   e.CO_NBR = :CoNbr'#13 +
        '   order by ee_NBR'
        );
        Q2834.Params.AddValue('check_b', Check_b);
        Q2834.Params.AddValue('check_e', Check_e);
        Q2834.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        Q2834.Execute;
        Q2834.Result.IndexFieldNames:= 'Ee_Nbr';

        ds2834.Clear;
        Q2834.Result.First;
        while not Q2834.Result.Eof do
        begin
          QWeek.Result.Filter := 'EE_NBR = ' + Q2834.Result.FieldByName('EE_NBR').AsString;
          QWeek.Result.Filtered:= true;
          if QWeek.Result.RecordCount > 0 then
          begin
            QWeek.Result.last;
            CalculateWeek;
          end;
          Q2834.Result.Next;
        End;

  end;

  procedure s2713;
  begin
        Q2713M1:= TevQuery.Create(
'   SELECT t1.EE_NBR, t1.CUSTOM_EMPLOYEE_NUMBER, cp.FIRST_NAME,  cp.LAST_NAME, t1.ACA_STATUS,'#13 +
'      CAST(COALESCE(t31.ACA_Hours, 0) AS FLOAT)  Hours,'#13 +
'     case :AvgPeriod'#13 +
'         when ''M'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t31.Months, ''='', 0), 0, t31.ACA_Hours/CAST(COALESCE(t31.Months, 0) AS integer)), 2)'#13 +
'         when ''W'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t31.Weeks, ''='', 0), 0, t31.ACA_Hours/CAST(COALESCE(t31.Weeks, 0) AS integer)), 2)'#13 +
'         else 0'#13 +
'       end  AvgHours'#13 +
'   FROM  Cl_Person  cp, Ee  t1'#13 +
'    LEFT JOIN'#13 +
'      (SELECT  t5.Ee_Nbr,  t34.ACA_Hours, t36.Weeks, t38.Months'#13 +
'           FROM  Ee t5,'#13 +
'              (SELECT t2.Ee_Nbr, COALESCE(SUM(t3.Hours_Or_Pieces), 0)  ACA_Hours'#13 +
'                 FROM Pr t1, Pr_Check t2, Pr_Check_Lines t3, Cl_E_Ds t4'#13 +
'                  WHERE'#13 +
'                         CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'                         CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL and'#13 +
'                         CURRENT_DATE >= t3.EFFECTIVE_DATE and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                         CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
'                         t1.Co_Nbr = :CoNbr AND'#13 +
'                         t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                         t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'                         t4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'                         t1.PAYROLL_TYPE IN (''E'', ''I'') AND EXTRACTYear(CAST(t1.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) and'#13 +
'                         t1.Status = ''P'' AND'#13 +
'                         t3.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'                         t4.E_D_Code_Type = ''M1'' AND Trim(Upper(t4.Description)) = ''ACA HOURS'''#13 +
'                  GROUP BY  1'#13 +
'              )  t34,'#13 +
'             (SELECT t2.Ee_Nbr,'#13 +
'                    SUM(IFDOUBLE(CompareString(t32.Frequency, ''='', ''D''), 0,'#13 +
'                        IFDOUBLE(CompareString(t32.Frequency, ''='', ''W''), 1,'#13 +
'                           IFDOUBLE(CompareString(t32.Frequency, ''='', ''B''), 2,'#13 +
'                             IFDOUBLE(CompareString(t32.Frequency, ''='', ''S''), 2.17,'#13 +
'                               IFDOUBLE(CompareString(t32.Frequency, ''='', ''M''), 4.33,'#13 +
'                                 IFDOUBLE(CompareString(t32.Frequency, ''='', ''Q''), 13, 0)))))))  Weeks'#13 +
''#13 +
'                    FROM Pr t1, Pr_Check t2, Pr_Check_Lines t3, Cl_E_Ds t4, Pr_Batch t32'#13 +
'                    WHERE'#13 +
'                       CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t3.EFFECTIVE_DATE and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t32.EFFECTIVE_DATE and CURRENT_DATE < t32.EFFECTIVE_UNTIL and'#13 +
'                       t1.Co_Nbr = :CoNbr AND'#13 +
'                       t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                       t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'                       t4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'                       t1.Pr_Nbr = t32.Pr_Nbr AND'#13 +
'                       t1.PAYROLL_TYPE IN (''E'', ''I'') AND EXTRACTYear(CAST(t1.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) and'#13 +
'                       t1.Status = ''P'' AND'#13 +
'                       t3.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'                       t4.E_D_Code_Type = ''M1'' AND Trim(Upper(t4.Description)) = ''ACA HOURS'''#13 +
'               GROUP BY  1'#13 +
'              )  t36,'#13 +
'              (SELECT  t2.Ee_Nbr, COUNT(DISTINCT CAST(EXTRACTMONTH(t1.Check_Date) AS VARCHAR(5)) || CAST(EXTRACTYear(t1.Check_Date) AS VARCHAR(5)))  Months'#13 +
'               FROM Pr  t1, Pr_Check  t2, Pr_Check_Lines  t3, Cl_E_Ds  t4'#13 +
'               WHERE'#13 +
'                  CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'                  CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL and'#13 +
'                  CURRENT_DATE >= t3.EFFECTIVE_DATE and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                  CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
'                  t1.Co_Nbr = :CoNbr AND'#13 +
'                  t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                  t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'                  t4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'                  t1.PAYROLL_TYPE IN (''E'', ''I'') AND EXTRACTYear(CAST(t1.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) and'#13 +
'                  t1.Status = ''P''  AND'#13 +
'                  t3.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'                  t4.E_D_Code_Type = ''M1'' AND Trim(Upper(t4.Description)) = ''ACA HOURS'''#13 +
'                GROUP BY 1'#13 +
''#13 +
'             )  t38'#13 +
'           WHERE'#13 +
'              CURRENT_DATE >= t5.EFFECTIVE_DATE and CURRENT_DATE < t5.EFFECTIVE_UNTIL and'#13 +
'              t5.Co_Nbr = :CoNbr AND'#13 +
'              t5.Ee_Nbr = t34.Ee_Nbr  AND'#13 +
'              t5.Ee_Nbr = t36.Ee_Nbr  AND'#13 +
'              t5.Ee_Nbr = t38.Ee_Nbr'#13 +
'      )  t31 on t1.Ee_Nbr = t31.Ee_Nbr'#13 +
''#13 +
'   WHERE'#13 +
'      CURRENT_DATE >= cp.EFFECTIVE_DATE and CURRENT_DATE < cp.EFFECTIVE_UNTIL and'#13 +
'      CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'      cp.CL_PERSON_NBR = t1.CL_PERSON_NBR  AND'#13 +
'      t31.Ee_Nbr = t1.Ee_Nbr and'#13 +
'      t1.Co_Nbr = :CoNbr AND'#13 +
'      t1.Current_Termination_Code IN (''A'', ''S'', ''P'') AND'#13 +
'      t1.CURRENT_HIRE_DATE < :check_b'
        );
        Q2713M1.Params.AddValue('AvgPeriod', DM_COMPANY.CO.fieldByName('ACA_AVG_HOURS_WORKED_PERIOD').AsString);
        Q2713M1.Params.AddValue('check_b', Check_b);
        Q2713M1.Params.AddValue('check_e', Check_e);
        Q2713M1.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        Q2713M1.Execute;
        Q2713M1.Result.IndexFieldNames:= 'Ee_Nbr';


        Q2713 := TevQuery.Create(
'SELECT e1.EE_NBR, tt1.CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME,  LAST_NAME, tt1.ACA_STATUS, SUM(Hours) Hours, SUM(AvgHours) AvgHours'#13 +
'from EE e1,'#13 +
'('#13 +
'  SELECT'#13 +
'     t1.EE_NBR,  t1.CUSTOM_EMPLOYEE_NUMBER, cp.FIRST_NAME, cp.LAST_NAME,'#13 +
'     t1.ACA_STATUS,'#13 +
'     t13.Hours Hours,'#13 +
'     case :AvgPeriod'#13 +
'         when ''M'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t22.MonthNbr, ''='', 0), 0, t13.Hours/CAST(COALESCE(t22.MonthNbr, 0) AS integer)), 2)'#13 +
'         when ''W'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t22.WeekNbr, ''='', 0), 0, t13.Hours/CAST(COALESCE(t22.WeekNbr, 0) AS integer)), 2)'#13 +
'         else 0'#13 +
'       end  AvgHours'#13 +
'  FROM'#13 +
'     Cl_Person  cp,'#13 +
'     Ee  t1,'#13 +
'     ( SELECT'#13 +
'       SUM(case :AvgPeriod'#13 +
'         when ''W'' then'#13 +
'                 IFDOUBLE(COMPARESTRING(t4.BiWeekly, ''='', ''Y''),'#13 +
'                   IFDOUBLE(COMPAREINTEGER(t4.WeekPeriod, ''='', 1), t4.Week1_hours, t4.Week2_hours), t4.Hours)'#13 +
'         when ''M'' then'#13 +
'                 t4.Hours'#13 +
'         else 0'#13 +
'       end)  Hours,'#13 +
'        t4.Ee_Nbr'#13 +
'       FROM'#13 +
'        ( SELECT'#13 +
'             t15.Ee_Nbr,'#13 +
'             SUM(CAST(COALESCE('#13 +
'                IFDOUBLE(COMPAREINTEGER(POSSUBSTR(t25.E_D_Code_Type, ''EA EB EF EE ED EO EP EQ''), ''>'', 0), t16.Hours_Or_Pieces,'#13 +
'                    IFDOUBLE(COMPAREINTEGER(POSSUBSTR(t25.E_D_Code_Type, ''EG EH EI EJ''), ''>'', 0),'#13 +
'                         IFDOUBLE(COMPARESTRING(t25.Overstate_Hours_For_Overtime, ''='', ''N''), t16.Hours_Or_Pieces,'#13 +
'                             IFDOUBLE(COMPAREINTEGER(COALESCE(t23.Cl_E_Ds_Nbr,0), ''='', 0), 0, t16.Hours_Or_Pieces) ),'#13 +
'                                IFDOUBLE(COMPAREINTEGER(COALESCE(t23.Cl_E_Ds_Nbr,0), ''='', 0), 0, t16.Hours_Or_Pieces) ) ) , 0) AS FLOAT))  Hours,'#13 +
''#13 +
'             CAST(IFINTEGER(COMPAREINTEGER(EXTRACT(Day from t17.Check_Date), ''<='', 7), 1,'#13 +
'                             IFINTEGER(COMPAREINTEGER(EXTRACT(Day from t17.Check_Date), ''<='', 14), 2,'#13 +
'                                IFINTEGER(COMPAREINTEGER(EXTRACT(Day from t17.Check_Date), ''<='', 21), 1, 2) )) AS INTEGER)  WeekPeriod,'#13 +
''#13 +
'             SUM(CAST(COALESCE(IFDOUBLE(COMPAREINTEGER(COALESCE(t14.Cl_E_Ds_Nbr, 0), ''<>'', 0), t16.Hours_Or_Pieces, 0), 0) AS FLOAT))  Week1_hours,'#13 +
'             SUM(CAST(COALESCE(IFDOUBLE(COMPAREINTEGER(COALESCE(t20.Cl_E_Ds_Nbr, 0), ''<>'', 0), t16.Hours_Or_Pieces, 0), 0) AS FLOAT))  Week2_hours,'#13 +
'             CAST(IFSTRING(COMPARESTRING(t3.Frequency, ''='', ''B''), ''Y'', ''N'') AS VARCHAR(2))  BiWeekly,'#13 +
'             t17.PAYROLL_TYPE,'#13 +
'             t25.E_D_Code_Type,'#13 +
'             t25.Description'#13 +
'            FROM'#13 +
'             Pr_Batch  t3, Pr_Check  t15, Pr  t17, Cl_E_Ds t25, Pr_Check_Lines  t16'#13 +
''#13 +
'             LEFT JOIN'#13 +
'             ('#13 +
'               SELECT DISTINCT t19.Cl_E_Ds_Nbr'#13 +
'               FROM Cl_E_D_Groups t18,  Cl_E_D_Group_Codes t19'#13 +
'               WHERE CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'                      t18.Cl_E_D_Groups_Nbr = t19.Cl_E_D_Groups_Nbr  AND'#13 +
'                      t18.Cl_E_D_Groups_Nbr = :EDGroup1'#13 +
'             )  t14 on t16.Cl_E_Ds_Nbr = t14.Cl_E_Ds_Nbr'#13 +
''#13 +
'             LEFT JOIN'#13 +
'             ('#13 +
'               SELECT DISTINCT  t19.Cl_E_Ds_Nbr'#13 +
'               FROM Cl_E_D_Groups t18, Cl_E_D_Group_Codes t19'#13 +
'               WHERE CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'                      t18.Cl_E_D_Groups_Nbr = t19.Cl_E_D_Groups_Nbr AND'#13 +
'                      t18.Cl_E_D_Groups_Nbr = :EDGroup2'#13 +
'             )  t20 on t16.Cl_E_Ds_Nbr = t20.Cl_E_Ds_Nbr'#13 +
''#13 +
'             LEFT JOIN'#13 +
'             ( SELECT DISTINCT  t19.Cl_E_Ds_Nbr  Cl_E_Ds_Nbr'#13 +
'               FROM Cl_E_D_Groups t18, Cl_E_D_Group_Codes t19, Cl_E_Ds  t24'#13 +
'               WHERE CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t24.EFFECTIVE_DATE and CURRENT_DATE < t24.EFFECTIVE_UNTIL and'#13 +
'                      t18.Cl_E_D_Groups_Nbr = t19.Cl_E_D_Groups_Nbr  AND'#13 +
'                      t24.Cl_E_Ds_Nbr = t19.Cl_E_Ds_Nbr AND'#13 +
'                      t18.Cl_E_D_Groups_Nbr = :EDGroupAWH'#13 +
'             ) t23 on t16.Cl_E_Ds_Nbr = t23.Cl_E_Ds_Nbr'#13 +
'           WHERE'#13 +
'                CURRENT_DATE >= t3.EFFECTIVE_DATE  and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t15.EFFECTIVE_DATE and CURRENT_DATE < t15.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t16.EFFECTIVE_DATE and CURRENT_DATE < t16.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t17.EFFECTIVE_DATE and CURRENT_DATE < t17.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t25.EFFECTIVE_DATE and CURRENT_DATE < t25.EFFECTIVE_UNTIL and'#13 +
''#13 +
'                t15.Check_Status <> ''V''  AND'#13 +
'                t17.Status = ''P'' AND t17.Check_Date BETWEEN :check_b AND :check_e and'#13 +
'                t17.Pr_Nbr = t3.Pr_Nbr  AND'#13 +
'                t3.Pr_Batch_Nbr = t15.Pr_Batch_Nbr  AND'#13 +
'                t15.Pr_Check_Nbr = t16.Pr_Check_Nbr AND'#13 +
'                t25.Cl_E_Ds_Nbr = t16.Cl_E_Ds_Nbr'#13 +
'           GROUP BY 1, 3, 6, 7,8,9'#13 +
'          )  t4'#13 +
'         WHERE'#13 +
'          NOT (t4.PAYROLL_TYPE IN (''E'', ''I'')) AND t4.E_D_Code_Type <> ''M1'' AND Trim(Upper(t4.Description)) <> Trim(Upper(''ACA HOURS''))'#13 +
''#13 +
''#13 +
'         GROUP BY 2'#13 +
'     )  t13,'#13 +
''#13 +
'     ('#13 +
'        SELECT  t4.Ee_Nbr,  t4.MonthNbr,  t29.WeekNbr'#13 +
'        FROM'#13 +
'          (   SELECT  t15.Ee_Nbr, COUNT(DISTINCT CAST(EXTRACTMONTH(t17.Check_Date) AS VARCHAR(5)) || CAST(EXTRACTYear(t17.Check_Date) AS VARCHAR(5)))  MonthNbr'#13 +
'              FROM    Pr_Check t15, Pr t17'#13 +
'              WHERE'#13 +
'                CURRENT_DATE >= t15.EFFECTIVE_DATE and CURRENT_DATE < t15.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t17.EFFECTIVE_DATE and CURRENT_DATE < t17.EFFECTIVE_UNTIL and'#13 +
'                t15.PR_NBR = t17.PR_NBR and'#13 +
'                t15.Check_Status <> ''V'' AND t15.Check_Type IN (''R'', ''M'', ''3'')  AND'#13 +
'                t17.Status = ''P'' AND t17.Check_Date BETWEEN :check_b AND :check_e'#13 +
'              GROUP BY 1'#13 +
'          )  t4,'#13 +
'          ('#13 +
'             SELECT  t30.Ee_Nbr, SUM(t30.Weeks)  WeekNbr'#13 +
'              FROM'#13 +
'               ('#13 +
'                 SELECT DISTINCT'#13 +
'                    t15.Ee_Nbr, t17.Check_Date, t3.Frequency,'#13 +
'                    IFDOUBLE(CompareString(t3.Frequency, ''='', ''D''), 0,'#13 +
'                        IFDOUBLE(CompareString(t3.Frequency, ''='', ''W''), 1,'#13 +
'                           IFDOUBLE(CompareString(t3.Frequency, ''='', ''B''), 2,'#13 +
'                             IFDOUBLE(CompareString(t3.Frequency, ''='', ''S''), 2.17,'#13 +
'                               IFDOUBLE(CompareString(t3.Frequency, ''='', ''M''), 4.33,'#13 +
'                                 IFDOUBLE(CompareString(t3.Frequency, ''='', ''Q''), 13, 0))))))  Weeks'#13 +
'                 FROM Pr_Check t15, Pr  t17, Pr_Batch  t3'#13 +
'                  WHERE'#13 +
'                      CURRENT_DATE >= t3.EFFECTIVE_DATE  and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t15.EFFECTIVE_DATE and CURRENT_DATE < t15.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t17.EFFECTIVE_DATE and CURRENT_DATE < t17.EFFECTIVE_UNTIL and'#13 +
'                      t17.Pr_Nbr = t3.Pr_Nbr  AND'#13 +
'                      t3.Pr_Batch_Nbr = t15.Pr_Batch_Nbr  AND'#13 +
'                      t15.Check_Status <> ''V'' AND t15.Check_Type IN (''R'', ''M'', ''3'')  AND'#13 +
'                      t17.Status = ''P'' AND t17.Check_Date BETWEEN :check_b AND :check_e'#13 +
'               )  t30'#13 +
'              GROUP BY 1'#13 +
'          )  t29'#13 +
'        WHERE t29.Ee_Nbr = t4.Ee_Nbr'#13 +
'     )  t22'#13 +
'    WHERE'#13 +
'     CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'     CURRENT_DATE >= cp.EFFECTIVE_DATE and CURRENT_DATE < cp.EFFECTIVE_UNTIL and'#13 +
''#13 +
'     cp.CL_PERSON_NBR = t1.CL_PERSON_NBR  AND'#13 +
'     t1.Co_Nbr = :CoNbr AND'#13 +
'     t1.Current_Termination_Code IN (''A'', ''S'', ''P'') AND'#13 +
'     t1.CURRENT_HIRE_DATE < :check_b and'#13 +
'     t1.Ee_Nbr = t22.Ee_Nbr AND'#13 +
'     t1.Ee_Nbr = t13.Ee_Nbr'#13 +
''#13 +
') tt1'#13 +
'   where'#13 +
'     CURRENT_DATE >= e1.EFFECTIVE_DATE and CURRENT_DATE < e1.EFFECTIVE_UNTIL and'#13 +
'     e1.EE_NBR = tt1.EE_NBR and'#13 +
'     e1.Co_Nbr = :CoNbr AND'#13 +
'     e1.Current_Termination_Code IN (''A'', ''S'', ''P'') AND'#13 +
'     e1.CURRENT_HIRE_DATE < :check_b'#13 +
'   group by 1,2,3,4,5'
        );
        Q2713.Params.AddValue('AvgPeriod', DM_COMPANY.CO.fieldByName('ACA_AVG_HOURS_WORKED_PERIOD').AsString);
        Q2713.Params.AddValue('EDGroup1', DM_COMPANY.CO.fieldByName('ACA_ADD_EARN_CL_E_D_GROUPS_NBR').AsInteger);
        Q2713.Params.AddValue('EDGroup2', DM_COMPANY.CO.fieldByName('ACA_ADD_EARN_CL_E_D_GROUPS_NBR').AsInteger);
        Q2713.Params.AddValue('EDGroupAWH', DM_COMPANY.CO.fieldByName('ACA_ADD_EARN_CL_E_D_GROUPS_NBR').AsInteger);
        Q2713.Params.AddValue('check_b', Check_b);
        Q2713.Params.AddValue('check_e', Check_e);
        Q2713.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        Q2713.Execute;
        Q2713.Result.IndexFieldNames:= 'Ee_Nbr';

        ds2713.Clear;
        Q2713.Result.First;
        while not Q2713.Result.Eof do
        begin
          ds2713.AppendRecord([Q2713.Result['EE_NBR'], Q2713.Result['CUSTOM_EMPLOYEE_NUMBER'],
                               Q2713.Result['FIRST_NAME'], Q2713.Result['LAST_NAME'],
                               Q2713.Result['ACA_STATUS'], Q2713.Result['Hours'], Q2713.Result['AvgHours']]);
          Q2713.Result.Next;
        End;

        Q2713M1.Result.First;
        while not Q2713M1.Result.Eof do
        begin
          if ds2713.Locate('EE_NBR', Q2713M1.Result['EE_NBR'], [] ) then
          begin
            ds2713.Edit;
            ds2713.FieldByName('Hours').AsFloat := ds2713.FieldByName('Hours').AsFloat + Q2713M1.Result.fieldByName('Hours').AsFloat;
            ds2713.FieldByName('AvgHours').AsFloat := ds2713.FieldByName('AvgHours').AsFloat + Q2713M1.Result.fieldByName('AvgHours').AsFloat;
            ds2713.post;
          end
          else
            ds2713.AppendRecord([Q2713M1.Result['EE_NBR'], Q2713M1.Result['CUSTOM_EMPLOYEE_NUMBER'],
                                Q2713M1.Result['FIRST_NAME'], Q2713M1.Result['LAST_NAME'],
                                Q2713M1.Result['ACA_STATUS'], Q2713M1.Result['Hours'], Q2713.Result['AvgHours']]);
          Q2713M1.Result.Next;
        End;
  end;

  procedure sNew2713;
  begin

        Q2713NewM1:= TevQuery.Create(
'   SELECT t1.EE_NBR, t1.CUSTOM_EMPLOYEE_NUMBER, cp.FIRST_NAME,  cp.LAST_NAME, t1.ACA_STATUS,'#13 +
'      CAST(COALESCE(t31.ACA_Hours, 0) AS FLOAT)  Hours,'#13 +
'     case :AvgPeriod'#13 +
'         when ''M'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t31.Months, ''='', 0), 0, t31.ACA_Hours/CAST(COALESCE(t31.Months, 0) AS integer)), 2)'#13 +
'         when ''W'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t31.Weeks, ''='', 0), 0, t31.ACA_Hours/CAST(COALESCE(t31.Weeks, 0) AS integer)), 2)'#13 +
'         else 0'#13 +
'       end  AvgHours'#13 +
'   FROM  Cl_Person  cp, Ee  t1'#13 +
'    LEFT JOIN'#13 +
'      (SELECT  t5.Ee_Nbr,  t34.ACA_Hours, t36.Weeks, t38.Months'#13 +
'           FROM  Ee t5,'#13 +
'              (SELECT t2.Ee_Nbr, COALESCE(SUM(t3.Hours_Or_Pieces), 0)  ACA_Hours'#13 +
'                 FROM Pr t1, Pr_Check t2, Pr_Check_Lines t3, Cl_E_Ds t4'#13 +
'                  WHERE'#13 +
'                         CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'                         CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL and'#13 +
'                         CURRENT_DATE >= t3.EFFECTIVE_DATE and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                         CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
'                         t1.Co_Nbr = :CoNbr AND'#13 +
'                         t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                         t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'                         t4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'                         t1.PAYROLL_TYPE IN (''E'', ''I'') AND EXTRACTYear(CAST(t1.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) and'#13 +
'                         t1.Status = ''P'' AND'#13 +
'                         t3.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'                         t4.E_D_Code_Type = ''M1'' AND Trim(Upper(t4.Description)) = ''ACA HOURS'''#13 +
'                  GROUP BY  1'#13 +
'              )  t34,'#13 +
'             (SELECT t2.Ee_Nbr,'#13 +
'                    SUM(IFDOUBLE(CompareString(t32.Frequency, ''='', ''D''), 0,'#13 +
'                        IFDOUBLE(CompareString(t32.Frequency, ''='', ''W''), 1,'#13 +
'                           IFDOUBLE(CompareString(t32.Frequency, ''='', ''B''), 2,'#13 +
'                             IFDOUBLE(CompareString(t32.Frequency, ''='', ''S''), 2.17,'#13 +
'                               IFDOUBLE(CompareString(t32.Frequency, ''='', ''M''), 4.33,'#13 +
'                                 IFDOUBLE(CompareString(t32.Frequency, ''='', ''Q''), 13, 0)))))))  Weeks'#13 +
''#13 +
'                    FROM Pr t1, Pr_Check t2, Pr_Check_Lines t3, Cl_E_Ds t4, Pr_Batch t32'#13 +
'                    WHERE'#13 +
'                       CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t3.EFFECTIVE_DATE and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
'                       CURRENT_DATE >= t32.EFFECTIVE_DATE and CURRENT_DATE < t32.EFFECTIVE_UNTIL and'#13 +
'                       t1.Co_Nbr = :CoNbr AND'#13 +
'                       t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                       t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'                       t4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'                       t1.Pr_Nbr = t32.Pr_Nbr AND'#13 +
'                       t1.PAYROLL_TYPE IN (''E'', ''I'') AND EXTRACTYear(CAST(t1.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) and'#13 +
'                       t1.Status = ''P'' AND'#13 +
'                       t3.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'                       t4.E_D_Code_Type = ''M1'' AND Trim(Upper(t4.Description)) = ''ACA HOURS'''#13 +
'               GROUP BY  1'#13 +
'              )  t36,'#13 +
'              (SELECT  t2.Ee_Nbr, COUNT(DISTINCT CAST(EXTRACTMONTH(t1.Check_Date) AS VARCHAR(5)) || CAST(EXTRACTYear(t1.Check_Date) AS VARCHAR(5)))  Months'#13 +
'               FROM Pr  t1, Pr_Check  t2, Pr_Check_Lines  t3, Cl_E_Ds  t4'#13 +
'               WHERE'#13 +
'                  CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'                  CURRENT_DATE >= t2.EFFECTIVE_DATE and CURRENT_DATE < t2.EFFECTIVE_UNTIL and'#13 +
'                  CURRENT_DATE >= t3.EFFECTIVE_DATE and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                  CURRENT_DATE >= t4.EFFECTIVE_DATE and CURRENT_DATE < t4.EFFECTIVE_UNTIL and'#13 +
'                  t1.Co_Nbr = :CoNbr AND'#13 +
'                  t1.Pr_Nbr = t2.Pr_Nbr  AND'#13 +
'                  t2.Pr_Check_Nbr = t3.Pr_Check_Nbr  AND'#13 +
'                  t4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr  AND'#13 +
'                  t1.PAYROLL_TYPE IN (''E'', ''I'') AND EXTRACTYear(CAST(t1.Check_Date as DATE)) IN (EXTRACTYear(CAST(:check_b as DATE)), EXTRACTYear(CAST(:check_e as DATE))) and'#13 +
'                  t1.Status = ''P''  AND'#13 +
'                  t3.Line_Item_Date BETWEEN :check_b AND :check_e  AND'#13 +
'                  t4.E_D_Code_Type = ''M1'' AND Trim(Upper(t4.Description)) = ''ACA HOURS'''#13 +
'                GROUP BY 1'#13 +
''#13 +
'             )  t38'#13 +
'           WHERE'#13 +
'              CURRENT_DATE >= t5.EFFECTIVE_DATE and CURRENT_DATE < t5.EFFECTIVE_UNTIL and'#13 +
'              t5.Co_Nbr = :CoNbr AND'#13 +
'              t5.Ee_Nbr = t34.Ee_Nbr  AND'#13 +
'              t5.Ee_Nbr = t36.Ee_Nbr  AND'#13 +
'              t5.Ee_Nbr = t38.Ee_Nbr'#13 +
'      )  t31 on t1.Ee_Nbr = t31.Ee_Nbr'#13 +
''#13 +
'   WHERE'#13 +
'      CURRENT_DATE >= cp.EFFECTIVE_DATE and CURRENT_DATE < cp.EFFECTIVE_UNTIL and'#13 +
'      CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'      cp.CL_PERSON_NBR = t1.CL_PERSON_NBR  AND'#13 +
'      t31.Ee_Nbr = t1.Ee_Nbr and'#13 +
'      t1.Co_Nbr = :CoNbr AND'#13 +
'      t1.Current_Termination_Code IN (''A'', ''S'', ''P'') AND'#13 +
'      t1.CURRENT_HIRE_DATE BETWEEN :hire_b AND :hire_e'
        );
        Q2713NewM1.Params.AddValue('hire_b', Hire_b);
        Q2713NewM1.Params.AddValue('hire_e', Hire_e);
        Q2713NewM1.Params.AddValue('AvgPeriod', DM_COMPANY.CO.fieldByName('ACA_AVG_HOURS_WORKED_PERIOD').AsString);
        Q2713NewM1.Params.AddValue('check_b', Check_b);
        Q2713NewM1.Params.AddValue('check_e', Check_e);
        Q2713NewM1.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        Q2713NewM1.Execute;
        Q2713NewM1.Result.IndexFieldNames:= 'Ee_Nbr';

        Q2713New := TevQuery.Create(
'SELECT e1.EE_NBR, tt1.CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME,  LAST_NAME, tt1.ACA_STATUS, SUM(Hours) Hours, SUM(AvgHours) AvgHours'#13 +
'from EE e1,'#13 +
'('#13 +
'  SELECT'#13 +
'     t1.EE_NBR,  t1.CUSTOM_EMPLOYEE_NUMBER, cp.FIRST_NAME, cp.LAST_NAME,'#13 +
'     t1.ACA_STATUS,'#13 +
'     t13.Hours Hours,'#13 +
'     case :AvgPeriod'#13 +
'         when ''M'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t22.MonthNbr, ''='', 0), 0, t13.Hours/CAST(COALESCE(t22.MonthNbr, 0) AS integer)), 2)'#13 +
'         when ''W'' then'#13 +
'                ROUNDALL(IFDOUBLE(COMPAREINTEGER(t22.WeekNbr, ''='', 0), 0, t13.Hours/CAST(COALESCE(t22.WeekNbr, 0) AS integer)), 2)'#13 +
'         else 0'#13 +
'       end  AvgHours'#13 +
'  FROM'#13 +
'     Cl_Person  cp,'#13 +
'     Ee  t1,'#13 +
'     ( SELECT'#13 +
'       SUM(case :AvgPeriod'#13 +
'         when ''W'' then'#13 +
'                 IFDOUBLE(COMPARESTRING(t4.BiWeekly, ''='', ''Y''),'#13 +
'                   IFDOUBLE(COMPAREINTEGER(t4.WeekPeriod, ''='', 1), t4.Week1_hours, t4.Week2_hours), t4.Hours)'#13 +
'         when ''M'' then'#13 +
'                 t4.Hours'#13 +
'         else 0'#13 +
'       end)  Hours,'#13 +
'        t4.Ee_Nbr'#13 +
'       FROM'#13 +
'        ( SELECT'#13 +
'             t15.Ee_Nbr,'#13 +
'             SUM(CAST(COALESCE('#13 +
'                IFDOUBLE(COMPAREINTEGER(POSSUBSTR(t25.E_D_Code_Type, ''EA EB EF EE ED EO EP EQ''), ''>'', 0), t16.Hours_Or_Pieces,'#13 +
'                    IFDOUBLE(COMPAREINTEGER(POSSUBSTR(t25.E_D_Code_Type, ''EG EH EI EJ''), ''>'', 0),'#13 +
'                         IFDOUBLE(COMPARESTRING(t25.Overstate_Hours_For_Overtime, ''='', ''N''), t16.Hours_Or_Pieces,'#13 +
'                             IFDOUBLE(COMPAREINTEGER(COALESCE(t23.Cl_E_Ds_Nbr,0), ''='', 0), 0, t16.Hours_Or_Pieces) ),'#13 +
'                                IFDOUBLE(COMPAREINTEGER(COALESCE(t23.Cl_E_Ds_Nbr,0), ''='', 0), 0, t16.Hours_Or_Pieces) ) ) , 0) AS FLOAT))  Hours,'#13 +
''#13 +
'             CAST(IFINTEGER(COMPAREINTEGER(EXTRACT(Day from t17.Check_Date), ''<='', 7), 1,'#13 +
'                             IFINTEGER(COMPAREINTEGER(EXTRACT(Day from t17.Check_Date), ''<='', 14), 2,'#13 +
'                                IFINTEGER(COMPAREINTEGER(EXTRACT(Day from t17.Check_Date), ''<='', 21), 1, 2) )) AS INTEGER)  WeekPeriod,'#13 +
''#13 +
'             SUM(CAST(COALESCE(IFDOUBLE(COMPAREINTEGER(COALESCE(t14.Cl_E_Ds_Nbr, 0), ''<>'', 0), t16.Hours_Or_Pieces, 0), 0) AS FLOAT))  Week1_hours,'#13 +
'             SUM(CAST(COALESCE(IFDOUBLE(COMPAREINTEGER(COALESCE(t20.Cl_E_Ds_Nbr, 0), ''<>'', 0), t16.Hours_Or_Pieces, 0), 0) AS FLOAT))  Week2_hours,'#13 +
'             CAST(IFSTRING(COMPARESTRING(t3.Frequency, ''='', ''B''), ''Y'', ''N'') AS VARCHAR(2))  BiWeekly,'#13 +
'             t17.PAYROLL_TYPE,'#13 +
'             t25.E_D_Code_Type,'#13 +
'             t25.Description'#13 +
'            FROM'#13 +
'             Pr_Batch  t3, Pr_Check  t15, Pr  t17, Cl_E_Ds t25, Pr_Check_Lines  t16'#13 +
''#13 +
'             LEFT JOIN'#13 +
'             ('#13 +
'               SELECT DISTINCT t19.Cl_E_Ds_Nbr'#13 +
'               FROM Cl_E_D_Groups t18,  Cl_E_D_Group_Codes t19'#13 +
'               WHERE CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'                      t18.Cl_E_D_Groups_Nbr = t19.Cl_E_D_Groups_Nbr  AND'#13 +
'                      t18.Cl_E_D_Groups_Nbr = :EDGroup1'#13 +
'             )  t14 on t16.Cl_E_Ds_Nbr = t14.Cl_E_Ds_Nbr'#13 +
''#13 +
'             LEFT JOIN'#13 +
'             ('#13 +
'               SELECT DISTINCT  t19.Cl_E_Ds_Nbr'#13 +
'               FROM Cl_E_D_Groups t18, Cl_E_D_Group_Codes t19'#13 +
'               WHERE CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'                      t18.Cl_E_D_Groups_Nbr = t19.Cl_E_D_Groups_Nbr AND'#13 +
'                      t18.Cl_E_D_Groups_Nbr = :EDGroup2'#13 +
'             )  t20 on t16.Cl_E_Ds_Nbr = t20.Cl_E_Ds_Nbr'#13 +
''#13 +
'             LEFT JOIN'#13 +
'             ( SELECT DISTINCT  t19.Cl_E_Ds_Nbr  Cl_E_Ds_Nbr'#13 +
'               FROM Cl_E_D_Groups t18, Cl_E_D_Group_Codes t19, Cl_E_Ds  t24'#13 +
'               WHERE CURRENT_DATE >= t18.EFFECTIVE_DATE and CURRENT_DATE < t18.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t19.EFFECTIVE_DATE and CURRENT_DATE < t19.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t24.EFFECTIVE_DATE and CURRENT_DATE < t24.EFFECTIVE_UNTIL and'#13 +
'                      t18.Cl_E_D_Groups_Nbr = t19.Cl_E_D_Groups_Nbr  AND'#13 +
'                      t24.Cl_E_Ds_Nbr = t19.Cl_E_Ds_Nbr AND'#13 +
'                      t18.Cl_E_D_Groups_Nbr = :EDGroupAWH'#13 +
'             ) t23 on t16.Cl_E_Ds_Nbr = t23.Cl_E_Ds_Nbr'#13 +
'           WHERE'#13 +
'                CURRENT_DATE >= t3.EFFECTIVE_DATE  and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t15.EFFECTIVE_DATE and CURRENT_DATE < t15.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t16.EFFECTIVE_DATE and CURRENT_DATE < t16.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t17.EFFECTIVE_DATE and CURRENT_DATE < t17.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t25.EFFECTIVE_DATE and CURRENT_DATE < t25.EFFECTIVE_UNTIL and'#13 +
''#13 +
'                t15.Check_Status <> ''V''  AND'#13 +
'                t17.Status = ''P'' AND t17.Check_Date BETWEEN :check_b AND :check_e and'#13 +
'                t17.Pr_Nbr = t3.Pr_Nbr  AND'#13 +
'                t3.Pr_Batch_Nbr = t15.Pr_Batch_Nbr  AND'#13 +
'                t15.Pr_Check_Nbr = t16.Pr_Check_Nbr AND'#13 +
'                t25.Cl_E_Ds_Nbr = t16.Cl_E_Ds_Nbr'#13 +
'           GROUP BY 1, 3, 6, 7,8,9'#13 +
'          )  t4'#13 +
'         WHERE'#13 +
'          NOT (t4.PAYROLL_TYPE IN (''E'', ''I'')) AND t4.E_D_Code_Type <> ''M1'' AND Trim(Upper(t4.Description)) <> Trim(Upper(''ACA HOURS''))'#13 +
''#13 +
''#13 +
'       GROUP BY 2'#13 +
'     )  t13,'#13 +
''#13 +
'     ('#13 +
'        SELECT  t4.Ee_Nbr,  t4.MonthNbr,  t29.WeekNbr'#13 +
'        FROM'#13 +
'          (   SELECT  t15.Ee_Nbr, COUNT(DISTINCT CAST(EXTRACTMONTH(t17.Check_Date) AS VARCHAR(5)) || CAST(EXTRACTYear(t17.Check_Date) AS VARCHAR(5)))  MonthNbr'#13 +
'              FROM    Pr_Check t15, Pr t17'#13 +
'              WHERE'#13 +
'                CURRENT_DATE >= t15.EFFECTIVE_DATE and CURRENT_DATE < t15.EFFECTIVE_UNTIL and'#13 +
'                CURRENT_DATE >= t17.EFFECTIVE_DATE and CURRENT_DATE < t17.EFFECTIVE_UNTIL and'#13 +
'                t15.PR_NBR = t17.PR_NBR and'#13 +
'                t15.Check_Status <> ''V'' AND t15.Check_Type IN (''R'', ''M'', ''3'')  AND'#13 +
'                t17.Status = ''P'' AND t17.Check_Date BETWEEN :check_b AND :check_e'#13 +
'              GROUP BY 1'#13 +
'          )  t4,'#13 +
'          ('#13 +
'             SELECT  t30.Ee_Nbr, SUM(t30.Weeks)  WeekNbr'#13 +
'              FROM'#13 +
'               ('#13 +
'                 SELECT DISTINCT'#13 +
'                    t15.Ee_Nbr, t17.Check_Date, t3.Frequency,'#13 +
'                    IFDOUBLE(CompareString(t3.Frequency, ''='', ''D''), 0,'#13 +
'                        IFDOUBLE(CompareString(t3.Frequency, ''='', ''W''), 1,'#13 +
'                           IFDOUBLE(CompareString(t3.Frequency, ''='', ''B''), 2,'#13 +
'                             IFDOUBLE(CompareString(t3.Frequency, ''='', ''S''), 2.17,'#13 +
'                               IFDOUBLE(CompareString(t3.Frequency, ''='', ''M''), 4.33,'#13 +
'                                 IFDOUBLE(CompareString(t3.Frequency, ''='', ''Q''), 13, 0))))))  Weeks'#13 +
'                 FROM Pr_Check t15, Pr  t17, Pr_Batch  t3'#13 +
'                  WHERE'#13 +
'                      CURRENT_DATE >= t3.EFFECTIVE_DATE  and CURRENT_DATE < t3.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t15.EFFECTIVE_DATE and CURRENT_DATE < t15.EFFECTIVE_UNTIL and'#13 +
'                      CURRENT_DATE >= t17.EFFECTIVE_DATE and CURRENT_DATE < t17.EFFECTIVE_UNTIL and'#13 +
'                      t17.Pr_Nbr = t3.Pr_Nbr  AND'#13 +
'                      t3.Pr_Batch_Nbr = t15.Pr_Batch_Nbr  AND'#13 +
'                      t15.Check_Status <> ''V'' AND t15.Check_Type IN (''R'', ''M'', ''3'')  AND'#13 +
'                      t17.Status = ''P'' AND t17.Check_Date BETWEEN :check_b AND :check_e'#13 +
'               )  t30'#13 +
'              GROUP BY 1'#13 +
'          )  t29'#13 +
'        WHERE t29.Ee_Nbr = t4.Ee_Nbr'#13 +
'     )  t22'#13 +
'    WHERE'#13 +
'     CURRENT_DATE >= t1.EFFECTIVE_DATE and CURRENT_DATE < t1.EFFECTIVE_UNTIL and'#13 +
'     CURRENT_DATE >= cp.EFFECTIVE_DATE and CURRENT_DATE < cp.EFFECTIVE_UNTIL and'#13 +
''#13 +
'     cp.CL_PERSON_NBR = t1.CL_PERSON_NBR  AND'#13 +
'     t1.Co_Nbr = :CoNbr AND'#13 +
'     t1.Current_Termination_Code IN (''A'', ''S'', ''P'') AND'#13 +
'     t1.Ee_Nbr = t22.Ee_Nbr AND'#13 +
'     t1.Ee_Nbr = t13.Ee_Nbr'#13 +
''#13 +
') tt1'#13 +
'   where'#13 +
'     CURRENT_DATE >= e1.EFFECTIVE_DATE and CURRENT_DATE < e1.EFFECTIVE_UNTIL and'#13 +
'     e1.EE_NBR = tt1.EE_NBR and'#13 +
'     e1.Co_Nbr = :CoNbr AND'#13 +
'     e1.Current_Termination_Code IN (''A'', ''S'', ''P'') AND'#13 +
'     e1.CURRENT_HIRE_DATE BETWEEN :hire_b AND :hire_e'#13 +
'   group by 1,2,3,4,5'
      );

        Q2713New.Params.AddValue('AvgPeriod', DM_COMPANY.CO.fieldByName('ACA_AVG_HOURS_WORKED_PERIOD').AsString);
        Q2713New.Params.AddValue('EDGroup1', DM_COMPANY.CO.fieldByName('ACA_ADD_EARN_CL_E_D_GROUPS_NBR').AsInteger);
        Q2713New.Params.AddValue('EDGroup2', DM_COMPANY.CO.fieldByName('ACA_ADD_EARN_CL_E_D_GROUPS_NBR').AsInteger);
        Q2713New.Params.AddValue('EDGroupAWH', DM_COMPANY.CO.fieldByName('ACA_ADD_EARN_CL_E_D_GROUPS_NBR').AsInteger);
        Q2713New.Params.AddValue('check_b', Check_b);
        Q2713New.Params.AddValue('check_e', Check_e);
        Q2713New.Params.AddValue('hire_b', Hire_b);
        Q2713New.Params.AddValue('hire_e', Hire_e);
        Q2713New.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
        Q2713New.Execute;
        Q2713New.Result.IndexFieldNames:= 'Ee_Nbr';

        Q2713New.Result.First;
        while not Q2713New.Result.Eof do
        begin
          ds2713New.AppendRecord([Q2713New.Result['EE_NBR'], Q2713New.Result['CUSTOM_EMPLOYEE_NUMBER'],
                               Q2713New.Result['FIRST_NAME'], Q2713New.Result['LAST_NAME'],
                               Q2713New.Result['ACA_STATUS'], Q2713New.Result['Hours'], Q2713New.Result['AvgHours']]);
          Q2713New.Result.Next;
        End;

        Q2713NewM1.Result.First;
        while not Q2713NewM1.Result.Eof do
        begin
          if ds2713New.Locate('EE_NBR', Q2713NewM1.Result['EE_NBR'], [] ) then
          begin
            ds2713New.Edit;
            ds2713New.FieldByName('Hours').AsFloat := ds2713New.FieldByName('Hours').AsFloat + Q2713NewM1.Result.fieldByName('Hours').AsFloat;
            ds2713New.FieldByName('AvgHours').AsFloat := ds2713New.FieldByName('AvgHours').AsFloat + Q2713NewM1.Result.fieldByName('AvgHours').AsFloat;
            ds2713New.post;
          end
          else
            ds2713New.AppendRecord([Q2713NewM1.Result['EE_NBR'], Q2713NewM1.Result['CUSTOM_EMPLOYEE_NUMBER'],
                                Q2713NewM1.Result['FIRST_NAME'], Q2713NewM1.Result['LAST_NAME'],
                                Q2713NewM1.Result['ACA_STATUS'], Q2713NewM1.Result['Hours'], Q2713NewM1.Result['AvgHours']]);
          Q2713NewM1.Result.Next;
        End;
  end;

  procedure AddToResultList(const ds:IevDataSet; const eeNbr:variant; const uType, newAcaStatus:Char);
  begin
      if DM_EMPLOYEE.EE.Locate('EE_NBR', eeNbr ,[]) then
        with ACAResult.Add do
        begin
          OLD_ACA_STATUS:= DM_EMPLOYEE.EE['ACA_STATUS'];
          NEW_ACA_STATUS:= newAcaStatus;

          DM_EMPLOYEE.EE.Edit;
          DM_EMPLOYEE.EE['ACA_STATUS'] := newAcaStatus;
          DM_EMPLOYEE.EE.post;

          UpdateType:= uType;
          CO_NBR:= DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger;
          EE_NBR:= ds.FieldValues['EE_NBR'];
          CUSTOM_EMPLOYEE_NUMBER:= ds.FieldValues['CUSTOM_EMPLOYEE_NUMBER'];
          FIRST_NAME:= ds.FieldValues['FIRST_NAME'];
          LAST_NAME:= ds.FieldValues['LAST_NAME'];
        end;
  end;

  procedure Update2713(const ds: IevDataSet; const uType:Char);
  var
   sHours: integer;
  begin
    QEE.Result.First;
    while not QEE.Result.Eof do
    begin
     if (F2731.IndexOf(VarToStr(QEE.Result['EE_NBR'])) = -1) and
        (F2834.IndexOf(VarToStr(QEE.Result['EE_NBR'])) = -1) then
      if ds.Locate('EE_NBR', QEE.Result['EE_NBR'],[]) then
      begin
        sACA_STATUS:= QEE.Result['ACA_STATUS'];

        if Count2713 <> 0 then
          sHours:= RoundInt64(ds.FieldByName('Hours').AsInteger /Count2713)
        else
          sHours:= ds.FieldByName('AvgHours').AsInteger;

        case QEE.Result.FieldByName('ACA_STATUS').asString[1] of
          ACA_STATUS_NEW_HIRE:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_FULL
              else sACA_STATUS := ACA_STATUS_PART;
          ACA_STATUS_PART:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_PART
              else sACA_STATUS := ACA_STATUS_PART;
          ACA_STATUS_PART_ONGOING:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_PART_ONGOING
              else sACA_STATUS := ACA_STATUS_PART_ONGOING;
          ACA_STATUS_FULL:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_FULL_ONGOING
              else sACA_STATUS := ACA_STATUS_FULL;
          ACA_STATUS_FULL_ONGOING:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_FULL_ONGOING
              else sACA_STATUS := ACA_STATUS_FULL_ONGOING;
          ACA_STATUS_VARIABLE:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_FULL
              else sACA_STATUS := ACA_STATUS_PART;
          ACA_STATUS_SEASONAL:
              if sHours >= aHour then
                   sACA_STATUS := ACA_STATUS_FULL
              else sACA_STATUS := ACA_STATUS_PART;
        end;

        if (QEE.Result['ACA_STATUS'] <> sACA_STATUS) then
            AddToResultList(ds, QEE.Result['EE_NBR'], uType, sACA_STATUS[1]);

      end;
      QEE.Result.Next;
    End;
  end;

  procedure GetQEmployee(const eeList:IisStringList);
  var
   s: String;
  begin
    s:= BuildINsqlStatement('EE_NBR', eeList);
    if s='' then
       s:='0=1';
    QEE := TevQuery.Create(
          ' SELECT EE_NBR, ACA_STATUS FROM Ee'#13 +
          ' WHERE CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL  and'#13 +
          '       CO_NBR = :CoNbr and  #CONDITION order by EE_NBR'
          );
    QEE.Params.AddValue('CoNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
    QEE.Macros.AddValue('CONDITION', s);
    QEE.Execute;
    QEE.Result.IndexFieldNames:= 'EE_NBR';
  end;
var
 i, j, mCount : integer;
 done: Boolean;
 cfilter: IisStringList;

begin
    Result := TisListOfValues.Create;
    ACAResult := TevACATaskCompanyEE.Create;
    cfilter:= TisStringList.Create;
    ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

    ACAResult.ClNbr:=  AParams.Value['ClientID'];
    ACAResult.UpdateDate:= SysDate;
    cfilter.CommaText := AParams.Value['CoFilter'];

    ds2834 := TevDataset.Create;
    ds2834.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, false);
    ds2834.vclDataSet.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString, 9, false);
    ds2834.vclDataSet.FieldDefs.Add('FIRST_NAME', ftString,20, false);
    ds2834.vclDataSet.FieldDefs.Add('LAST_NAME', ftString,30, false);
    ds2834.vclDataSet.Open;

    ds2713 := TevDataset.Create;
    ds2713.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, false);
    ds2713.vclDataSet.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString, 9, false);
    ds2713.vclDataSet.FieldDefs.Add('FIRST_NAME', ftString,20, false);
    ds2713.vclDataSet.FieldDefs.Add('LAST_NAME', ftString,30, false);
    ds2713.vclDataSet.FieldDefs.Add('ACA_STATUS', ftString, 1, false);
    ds2713.vclDataSet.FieldDefs.Add('Hours', ftFloat, 0, false);
    ds2713.vclDataSet.FieldDefs.Add('AvgHours', ftFloat, 0, false);
    ds2713.vclDataSet.Open;

    ds2713New  := TevDataset.Create;
    ds2713New.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, false);
    ds2713New.vclDataSet.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString, 9, false);
    ds2713New.vclDataSet.FieldDefs.Add('FIRST_NAME', ftString,20, false);
    ds2713New.vclDataSet.FieldDefs.Add('LAST_NAME', ftString,30, false);
    ds2713New.vclDataSet.FieldDefs.Add('ACA_STATUS', ftString, 1, false);
    ds2713New.vclDataSet.FieldDefs.Add('Hours', ftFloat, 0, false);
    ds2713New.vclDataSet.FieldDefs.Add('AvgHours', ftFloat, 0, false);
    ds2713New.vclDataSet.Open;

  try
    for i:=0 to cfilter.Count -1 do
    begin
      DM_COMPANY.CO.DataRequired('CO_NBR=' + cfilter[i]);

      if (DM_COMPANY.CO.RecordCount > 0) and
         (DM_COMPANY.CO.fieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime > 0) and
         (DM_COMPANY.CO.fieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime > 0)   then
      begin
          Check_e:= SysDate;
          if DM_COMPANY.CO['ACA_EDUCATION_ORG'] = GROUP_BOX_YES then
            Check_b:= IncDay(Check_e,-(26*7))
          else
            Check_b:= IncDay(Check_e,-(13*7));

          s2731;

          StartDate:= SysDate;
          Check_e := StartDate;
          if DM_COMPANY.CO['ACA_EDUCATION_ORG'] = GROUP_BOX_YES then
          begin
            Check_b := IncDay(StartDate - (52 * 7));
            MaxUnpaidWeeks:= 25;
          end
          else
          begin
            Check_b := IncDay(StartDate - (39 * 7));
            MaxUnpaidWeeks:= 12;
          end;

          s2834;

          Check_b := GetBeginMonth(DM_COMPANY.CO.fieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime);
          Check_e := GetEndMonth(DM_COMPANY.CO.fieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime);
          aMeasurementDays:= Trunc(Check_e) - Trunc(Check_b);

          tmp:= 0;
          done:=false;
          repeat
            if (Check_e + aMeasurementDays + tmp) <  SysDate then
             tmp:= tmp + aMeasurementDays
            else
              done:= true;
          until done;

          Check_b := GetBeginMonth(Check_b + tmp);
          Check_e := GetEndMonth(Check_e + tmp);

          mCount2713:= 0;
          if DM_COMPANY.CO['ACA_USE_AVG_HOURS_WORKED'] <>  GROUP_BOX_YES then
          begin
            if DM_COMPANY.CO['ACA_AVG_HOURS_WORKED_PERIOD']= FREQUENCY_TYPE_WEEKLY then
              mCount2713:= RoundInt64((Trunc(Check_e) - Trunc(Check_b))/7)
            else
              mCount2713:= RoundInt64((Trunc(Check_e) - Trunc(Check_b))/30);
          end;

          s2713;
          mCountNew2713:= 0;

          if SysDate > Check_e then
          begin
            mCount:= MonthsBetween(Check_e, SysDate) + 1;

            Hire_b := Check_b;
            Hire_e := Check_b;

            for j := 0 to mCount - 1 do
            begin
              mCountNew2713:= 0;
              if DM_COMPANY.CO['ACA_USE_AVG_HOURS_WORKED'] <> GROUP_BOX_YES then
              begin
                if DM_COMPANY.CO['ACA_AVG_HOURS_WORKED_PERIOD']= FREQUENCY_TYPE_WEEKLY then
                  mCountNew2713:= RoundInt64((Trunc(Check_e) - Trunc(Check_b))/7)
                else
                  mCountNew2713:= RoundInt64((Trunc(Check_e) - Trunc(Check_b))/30);
              end;

              sNew2713;

              Check_b := GetBeginMonth(IncMonth(Check_b, 1));
              Check_e := GetEndMonth(IncMonth(Check_e, 1));

              Hire_b := GetBeginMonth(IncMonth(Check_b, -1)) + 1;
              Hire_e := Check_b;
            end;
          end;


          DM_EMPLOYEE.EE.DataRequired('CO_NBR=' +  DM_COMPANY.CO.fieldByName('CO_NBR').AsString);

          F2731 := Q2731.Result.GetColumnValues('EE_NBR');
          GetQEmployee(F2731);

          QEE.Result.First;
          while not QEE.Result.Eof do
          begin
            if Q2731.Result.Locate('EE_NBR', QEE.Result['EE_NBR'],[]) and
              (not (QEE.Result.FieldByName('ACA_STATUS').AsString[1] in [ACA_STATUS_NEW_HIRE, ACA_STATUS_NA, ACA_STATUS_LESS_SEASONAL])) then
                AddToResultList(Q2731.Result, QEE.Result['EE_NBR'], '1', ACA_STATUS_NEW_HIRE);

            QEE.Result.Next;
          End;

          F2834 := ds2834.GetColumnValues('EE_NBR');
          GetQEmployee(F2834);

          QEE.Result.First;
          while not QEE.Result.Eof do
          begin
            if F2731.IndexOf(VarToStr(QEE.Result['EE_NBR'])) = -1 then
            begin
              if ds2834.Locate('EE_NBR', QEE.Result['EE_NBR'],[]) and
                 (not (QEE.Result.FieldByName('ACA_STATUS').AsString[1] in [ACA_STATUS_NEW_HIRE, ACA_STATUS_NA, ACA_STATUS_LESS_SEASONAL])) then
                AddToResultList(ds2834, QEE.Result['EE_NBR'],'2', ACA_STATUS_NEW_HIRE);

            end;
            QEE.Result.Next;
          End;

          F2713 := ds2713.GetColumnValues('EE_NBR');
          GetQEmployee(F2713);
          aHour:=  130;
          if DM_COMPANY.CO['ACA_AVG_HOURS_WORKED_PERIOD']= FREQUENCY_TYPE_WEEKLY then
             aHour:=  30;

          Count2713:= mCount2713;
          Update2713(ds2713, '3');

          F2713New := ds2713New.GetColumnValues('EE_NBR');
          GetQEmployee(F2713New);

          Count2713:= mCountNew2713;
          Update2713(ds2713New, '4');

          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE]);
          Result.AddValue('ACATaskCompanyEE', (ACAResult as IisInterfacedObject).AsStream);
      end;
    end;
  except
    DM_EMPLOYEE.EE.Cancel;
    Raise;
  end;

end;


function TevAsyncFunctions.PrepareQuarterEndPreprocCompanyFilter(const AParams: IisListOfValues): IisListOfValues;
var
  s: String;
  Filter: IevPreprocessQuarterEndTaskFilter;
begin
  case TTaxServiceFilterType(AParams.Value['TaxServiceFilter']) of
    tsAll:    s := '';
    tsTax:    s := ' and TAX_SERVICE='+ QuotedStr(GROUP_BOX_YES);
    tsNonTax: s := ' and TAX_SERVICE='+ QuotedStr(GROUP_BOX_NO);
  else
    Assert(False);
  end;

  Filter := TevPreprocessQuarterEndTaskFilter.Create;

  try
    DM_TEMPORARY.TMP_CO.DataRequired('(LAST_PREPROCESS is null or LAST_PREPROCESS<>'+ QuotedStr(DateToStr(AParams.Value['EndDate']))+ ')'+ s + GetReadOnlyClintCompanyListFilter);
    DM_TEMPORARY.TMP_CO.IndexFieldNames := 'CL_NBR;CO_NBR';
    DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.DataRequired('PERIOD_END_DATE='+ QuotedStr(DateToStr(AParams.Value['EndDate'])) + GetReadOnlyClintCompanyListFilter);
    DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.IndexFieldNames := 'CL_NBR;CO_NBR';
    DM_TEMPORARY.TMP_CO.First;
    try
      while not DM_TEMPORARY.TMP_CO.Eof do
      begin
        DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.SetRange([DM_TEMPORARY.TMP_CO['CL_NBR'], DM_TEMPORARY.TMP_CO['CO_NBR']], [DM_TEMPORARY.TMP_CO['CL_NBR'], DM_TEMPORARY.TMP_CO['CO_NBR']]);
        if DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.RecordCount > 0 then
          with Filter.Add do
          begin
            ClNbr := DM_TEMPORARY.TMP_CO['CL_NBR'];
            CoNbr := DM_TEMPORARY.TMP_CO['CO_NBR'];
            Consolidation := False;
            Caption := DM_TEMPORARY.TMP_CO['CUSTOM_COMPANY_NUMBER'];
          end;
        DM_TEMPORARY.TMP_CO.Next;
      end;
    finally
      DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.CancelRange;
    end;

  finally
    DM_TEMPORARY.TMP_CO.Close;
    DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.Close;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, (Filter as IisInterfacedObject).AsStream);
end;

function TevAsyncFunctions.PreprocessQuarterEnd(const AParams: IisListOfValues): IisListOfValues;
begin
  Result := ctx_RemoteMiscRoutines.PreProcessForQuarter(
    AParams.Value['Cl_Nbr'],
    AParams.Value['Co_Nbr'],
    AParams.Value['ForceProcessing'],
    AParams.Value['Consolidation'],
    AParams.Value['BegDate'],
    AParams.Value['EndDate'],
    AParams.Value['QecCleanUpLimit'],
    AParams.Value['TaxAdjLimit'],
    AParams.Value['PrintReports']);
end;

function TevAsyncFunctions.PrepareCompsRebuildVMRHist(const AParams: IisListOfValues): IisListOfValues;
var
  FClNbr, FCoNbr: Integer;
  i: Integer;
  CoList: IisStringList;
  CoInfoList: IisStringList;
  s: String;
begin
  CoList := IInterface(AParams.Value['CoList']) as IisStringList;
  CoInfoList := TisStringList.Create;

  DM_TEMPORARY.TMP_CO.DataRequired(GetReadOnlyClintCompanyListFilter(false));
  try
    DM_TEMPORARY.TMP_CO.IndexFieldNames := 'CL_NBR;CO_NBR';

    for i := 0 to CoList.Count - 1 do
    begin
      s := CoList[i];
      FClNbr := StrToIntDef(GetNextStrValue(s, ','), 0);
      FCoNbr := StrToIntDef(GetNextStrValue(s, ','), 0);
      if (FClNbr <> 0) and (FCoNbr <> 0) and DM_TEMPORARY.TMP_CO.FindKey([FClNbr, FCoNbr]) then
      begin
        s := '';
        AddStrValue(s, IntToStr(FClNbr), ',');
        AddStrValue(s, IntToStr(FCoNbr), ',');
        AddStrValue(s, DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, ',');
        CoInfoList.Add(s);
      end;
    end;
  finally
    DM_TEMPORARY.TMP_CO.Close;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, CoInfoList);
end;

function TevAsyncFunctions.RebuildVMRHistory(const AParams: IisListOfValues): IisListOfValues;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  ctx_PayrollProcessing.ReCreateVmrHistory(
    AParams.Value['CoNbr'], AParams.Value['PeriodBegDate'], AParams.Value['PeriodEndDate']);
end;

function TevAsyncFunctions.PrepareForProcessTaxReturns(const AParams: IisListOfValues): IisListOfValues;
var
  BegDate: TDateTime;
  EndDate: TDateTime;
  Filter: IisStringList;
  TaxFilter: TProcessTaxReturnsFilterSelections;
  EeSortMode: TProcessTaxReturnsEeSort;
  AskForRefund: Boolean;
  SecureReport: Boolean;
  Reports: TrwReportList;
  EnlistCLNbr : integer;

  procedure AddResultItem(const Caption: string; const ClNbr: Integer);
  var
    RepInfo: IisListOfValues;
  begin
    RepInfo := TisListOfValues.Create;
    RepInfo.AddValue('Caption', Caption);
    RepInfo.AddValue('ClientID', ClNbr);
    RepInfo.AddValue('ReportList', Reports.GetAsStream);

    Result.AddValue('TaxReturns' + IntToStr(Result.Count), RepInfo);
  end;

  procedure EnlistReport(const ClNbr, CoNbr: Integer);
  var
    ReportParams: TrwReportParams;
    lBegDate, lEndDate: TDateTime;
  begin
    ReportParams := TrwReportParams.Create;
    try
      ReportParams.Add('Clients', VarArrayOf([ClNbr]));
      ReportParams.Add('Companies', VarArrayOf([CoNbr]));
      ReportParams.Add('Consolidation', not VarIsNull(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['cl_co_consolidation_nbr']));
      ReportParams.Add('Consolidations', VarArrayOf([not VarIsNull(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['cl_co_consolidation_nbr'])]));
      case EeSortMode of
        eesLastName: ReportParams.Add('DetailSort', 3);  //Last Name
        eesSSN: ReportParams.Add('DetailSort', 1);  //SSN
      else
        Assert(False)
      end;
      lBegDate := 0;
      lEndDate := 0;

     if (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE) then
       ReportParams.Add('Employees', VarArrayCreate([0, -1], varVariant));

      case string(ctx_DataAccess.SY_CUSTOM_VIEW['RETURN_FREQUENCY'])[1] of
      FREQUENCY_TYPE_SEMI_MONTHLY:
        if GetDay(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']) < 16 then
        begin
          lBegDate := GetBeginMonth(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
          lEndDate := lBegDate+ 14; // the 15th
        end
        else
        begin
          lBegDate := GetBeginMonth(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE'])+ 15; // the 16th
          lEndDate := GetEndMonth(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE'])
        end;
      FREQUENCY_TYPE_MONTHLY:
        begin
          lBegDate := GetBeginMonth(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
          lEndDate := GetEndMonth(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
        end;
      FREQUENCY_TYPE_QUARTERLY:
        begin
          lBegDate := GetBeginQuarter(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
          lEndDate := GetEndQuarter(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
        end;
      FREQUENCY_TYPE_ANNUAL:
        begin
          lBegDate := GetBeginYear(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
          lEndDate := GetEndYear(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['PERIOD_END_DATE']);
        end;
      else
        Assert(False);
      end;
      ReportParams.Add('Dat_b', lBegDate);
      ReportParams.Add('Dat_e', lEndDate);
      DM_TEMPORARY.TMP_PR.SetRange([ClNbr, CoNbr, lBegDate], [ClNbr, CoNbr, lEndDate]);
      DM_TEMPORARY.TMP_PR.First;
      while not DM_TEMPORARY.TMP_PR.Eof do
      begin
        ReportParams.Add('Payrolls', VarArrayOf([DM_TEMPORARY.TMP_PR['PR_NBR']]));
        DM_TEMPORARY.TMP_PR.Next;
      end;
      ReportParams.Add('ProcessDate', Now);
      ReportParams.Add('AgencyNbr', IntToStr(ConvertNull(ctx_DataAccess.SY_CUSTOM_VIEW['SY_GLOBAL_AGENCY_NBR'], 0)));
      ReportParams.Add('AgencyFieldOfficeNbr', IntToStr(ConvertNull(ctx_DataAccess.SY_CUSTOM_VIEW['SY_GL_AGENCY_FIELD_OFFICE_NBR'], 0)));
      ReportParams.Add('AskForRefund', AskForRefund);
      ReportParams.Add('SecureReport', SecureReport);
      ReportParams.FileName := ''; // no magmedia is expected
      ReportParams.Add('DueDate', DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['DUE_DATE']);
      Reports.AddReport(ctx_DataAccess.SY_CUSTOM_VIEW['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM,
        ReportParams, MakeString([DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['CO_TAX_RETURN_QUEUE_NBR'], ClNbr, ctx_DataAccess.SY_CUSTOM_VIEW['DESCRIPTION']]));
    finally
      ReportParams.Free;
    end;
  end;

  procedure CheckClientReports(const ClNbr : integer; const bAddReport : Boolean = false);
  begin
      if EnlistCLNbr = -1 then
         EnlistCLNbr := ClNbr;

      Assert(DM_TEMPORARY.TMP_CL.Locate('CL_NBR', EnlistCLNbr,[]));

      if (EnlistCLNbr <> ClNbr) or bAddReport then
      begin
        if (Reports.Count > 0)  then
        begin
          AddResultItem('#'+ DM_TEMPORARY.TMP_CL['CUSTOM_CLIENT_NUMBER'], EnlistCLNbr);
          Reports.Clear;
        end;
        EnlistCLNbr := ClNbr;
      end;
  end;

  procedure ProcessSelected(const ClNbr: Integer; CoNbrs: string);
  var
    i: Integer;
  begin
    CheckClientReports(ClNbr);

    while CoNbrs <> '' do
    begin
      i := StrToInt(Fetch(CoNbrs));
      Assert(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', VarArrayOf([ClNbr, i]), []));

  //  this part must fix, when Company select option added to scheduler screen...
      if ctx_DataAccess.SY_CUSTOM_VIEW.FindKey([DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['SY_REPORTS_GROUP_NBR']]) then
        if (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095B) and
           (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095C) and
           (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy) and
           (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy_W2)  then
          EnlistReport(ClNbr, DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['CO_NBR']);
    end;
  end;

  procedure ProcessAll(const ClNbr, CoNbr: Integer; const Cons: Boolean);
  begin
    CheckClientReports(ClNbr);

    DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.SetRange([ClNbr, CoNbr], [ClNbr, CoNbr]);
    DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.First;
    while not DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.Eof do
    begin
      if (VarIsNull(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['cl_co_consolidation_nbr']) and not Cons)
      or (not VarIsNull(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['cl_co_consolidation_nbr']) and Cons) then
      begin
        ctx_DataAccess.SY_CUSTOM_VIEW.IndexFieldNames := 'SY_GL_AGENCY_REPORT_NBR';
        if VarIsNull(DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE['SY_GL_AGENCY_REPORT_NBR']) then
          ctx_DataAccess.SY_CUSTOM_VIEW.IndexFieldNames := 'SY_REPORTS_GROUP_NBR';

        if ctx_DataAccess.SY_CUSTOM_VIEW.FindKey([DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE[ctx_DataAccess.SY_CUSTOM_VIEW.IndexFieldNames]]) then
          if (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095B) and
             (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095C) and
             (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy) and
             (ctx_DataAccess.SY_CUSTOM_VIEW['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy_W2) then
            if ConvertNull(ctx_DataAccess.SY_CUSTOM_VIEW['REPORT_TYPE'], '') = SYSTEM_REPORT_TYPE_TAX_RETURN then
              EnlistReport(ClNbr, CoNbr);
      end;

      DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.Next;
    end;
  end;

  function isTaxDirect(TaxServiceLevel: string = ''): Boolean;
  begin
    if TaxServiceLevel = '' then
      TaxServiceLevel := DM_COMPANY.CO.TAX_SERVICE.AsString;
    Result := TaxServiceLevel = TAX_SERVICE_DIRECT;
  end;

var
  i: Integer;
  s: String;
begin
  EnlistCLNbr := -1;

  EndDate := AParams.Value['EndDate'];
//  BegDate := AParams.Value['BegDate'];
  BegDate := GetBeginQuarter(EndDate);

  Filter := IInterface(AParams.Value['Filter']) as IisStringList;
  TaxFilter := TProcessTaxReturnsFilterSelections(Integer(AParams.Value['TaxFilter']));
  EeSortMode := TProcessTaxReturnsEeSort(Integer(AParams.Value['EeSortMode']));
  AskForRefund := AParams.Value['AskForRefund'];
  SecureReport := AParams.Value['SecureReport'];

  Result := TisListOfValues.Create;

  DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.CheckDSCondition('STATUS in ('+ QuotedStr(TAX_RETURN_STATUS_UNPROCESSED)+
    ','+ QuotedStr(TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED)+ ') and PERIOD_END_DATE between '''+ DateToStr(BegDate)+
    ''' and '''+ DateToStr(EndDate)+ '''' + GetReadOnlyClintCompanyListFilter);

  DM_TEMPORARY.TMP_PR.CheckDSCondition('CHECK_DATE between '''+ DateToStr(BegDate)+ ''' and '''+

  DateToStr(EndDate)+ ''''+
    ' and STATUS in ('''+ PAYROLL_STATUS_PROCESSED+ ''','''+ PAYROLL_STATUS_VOIDED+ ''')');

  DM_TEMPORARY.TMP_CO.CheckDSCondition(GetReadOnlyClintCompanyListFilter(false));
  DM_TEMPORARY.TMP_CL.CheckDSCondition('ALL');

  s :='select t1.SY_REPORTS_GROUP_NBR, t2.description, t3.SY_REPORT_WRITER_REPORTS_NBR, ' +
       't3.REPORT_TYPE, t4.SY_GLOBAL_AGENCY_NBR, t5.RETURN_FREQUENCY, t5.SYSTEM_TAX_TYPE, ' +
       't5.SY_GL_AGENCY_REPORT_NBR, T5.SY_GL_AGENCY_FIELD_OFFICE_NBR ' +
       'from SY_GL_AGENCY_REPORT T5, SY_GL_AGENCY_FIELD_OFFICE T4, SY_REPORT_WRITER_REPORTS T3, ' +
       'SY_REPORTS T2, SY_REPORTS_GROUP T1 ' +
       'where T1.SY_REPORTS_NBR = T2.SY_REPORTS_NBR and ' +
       'T5.SY_GL_AGENCY_REPORT_NBR = T1.SY_GL_AGENCY_REPORT_NBR and ' +
       'T4.SY_GL_AGENCY_FIELD_OFFICE_NBR = T5.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
       'T3.SY_REPORT_WRITER_REPORTS_NBR = T2.SY_REPORT_WRITER_REPORTS_NBR and ' +
       '{AsOfNow<T1>} and {AsOfNow<T2>} and {AsOfNow<T3>} ' +
       'and {AsOfNow<T4>} and {AsOfNow<T5>} and ' +
       't5.SY_GL_AGENCY_REPORT_NBR < 7000 ' +
     'union ' +
      'select t2.SY_REPORTS_GROUP_NBR, t4.NAME description, ' +
       't3.SY_REPORT_WRITER_REPORTS_NBR, t3.REPORT_TYPE, ' +
       't5.SY_GLOBAL_AGENCY_NBR, t1.RETURN_FREQUENCY, ' +
       't1.SYSTEM_TAX_TYPE, t1.SY_GL_AGENCY_REPORT_NBR, t5.SY_GL_AGENCY_FIELD_OFFICE_NBR ' +
      'from SY_GL_AGENCY_REPORT t1, SY_REPORT_WRITER_REPORTS t3, ' +
          'SY_REPORTS t2, SY_REPORTS_GROUP t4, ' +
            'SY_GL_AGENCY_FIELD_OFFICE t5 ' +
       'where ' +
         't1.SY_GL_AGENCY_FIELD_OFFICE_NBR = t5.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
         't1.SY_REPORTS_GROUP_NBR = t2.SY_REPORTS_GROUP_NBR and ' +
         't1.SY_REPORTS_GROUP_NBR = t4.SY_REPORTS_GROUP_NBR and ' +
         't3.SY_REPORT_WRITER_REPORTS_NBR = t2.SY_REPORT_WRITER_REPORTS_NBR and ' +
         't1.SY_GL_AGENCY_REPORT_NBR > 7000 and ' +
         '{AsOfNow<t2>} and {AsOfNow<t3>} and ' +
         '{AsOfNow<t1>} and {AsOfNow<t4>} and ' +
         '{AsOfNow<t5>} and ' +
         '( ' +
          '(t2.ACTIVE_YEAR_FROM is null and t2.ACTIVE_YEAR_TO is null) ' +
           'or ' +
            '(t2.ACTIVE_YEAR_FROM<= :pENDDATE and :pENDDATE <= t2.ACTIVE_YEAR_TO ) ' +
           'or ' +
            '(t2.ACTIVE_YEAR_FROM <= :pENDDATE and t2.ACTIVE_YEAR_TO is null) ' +
          ') ';

  with TExecDSWrapper.Create(s) do
  begin
   SetParam('pENDDATE', EndDate);
   ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
  end;

  with TExecDSWrapper.Create('ConsolidationsPreProcessStatus') do
  begin
    SetMacro('FIELDS', ',c1.TAX_SERVICE');
    ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
  end;

  DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
  DM_TEMPORARY.TMP_CO.LookupsEnabled := False;
  DM_TEMPORARY.TMP_CL.LookupsEnabled := False;
  DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.LookupsEnabled := False;
  try
    ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_PR, DM_TEMPORARY.TMP_CO, DM_TEMPORARY.TMP_CL,
                   DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE,
                    ctx_DataAccess.TEMP_CUSTOM_VIEW, ctx_DataAccess.SY_CUSTOM_VIEW]);

    ctx_DataAccess.SY_CUSTOM_VIEW.IndexFieldNames := 'SY_GL_AGENCY_REPORT_NBR';
    DM_TEMPORARY.TMP_PR.IndexFieldNames := 'CL_NBR;CO_NBR;CHECK_DATE';
    DM_TEMPORARY.TMP_CO.IndexFieldNames := 'CL_NBR;CO_NBR';
    DM_TEMPORARY.TMP_CL.IndexFieldNames := 'CL_NBR';

    Reports := TrwReportList.Create;
    try
        if TaxFilter <> fsCustom then
        begin
          DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.IndexFieldNames := 'CL_NBR;CO_NBR';
          ctx_DataAccess.TEMP_CUSTOM_VIEW.IndexFieldNames := 'CUSTOM_CLIENT_NUMBER;CUSTOM_COMPANY_NUMBER;';
              ctx_DataAccess.TEMP_CUSTOM_VIEW.First;
              while not ctx_DataAccess.TEMP_CUSTOM_VIEW.Eof do
              begin
                if (TaxFilter = fsAll)
                or ((TaxFilter = fsTaxOnly) and SbSendsTaxPaymentsForCompany(ctx_DataAccess.TEMP_CUSTOM_VIEW['TAX_SERVICE']))
                or ((TaxFilter = fsNonTaxOnly) and not SbSendsTaxPaymentsForCompany(ctx_DataAccess.TEMP_CUSTOM_VIEW['TAX_SERVICE']))
                or ((TaxFilter = fsTaxDirect) and isTaxDirect(ctx_DataAccess.TEMP_CUSTOM_VIEW['TAX_SERVICE'])) then
                begin
                  if (ConvertNull(ctx_DataAccess.TEMP_CUSTOM_VIEW['LAST_PREPROCESS_CONS'], 0) = EndDate)  and
                      ((ConvertNull(ctx_DataAccess.TEMP_CUSTOM_VIEW['LAST_PREPROCESS_MESSAGE_CONS'], '') = '')
                         or StartsWith(Trim(ctx_DataAccess.TEMP_CUSTOM_VIEW.fieldByName('LAST_PREPROCESS_MESSAGE_CONS').AsString), MANUALLY_RECONCILED)) then
                    ProcessAll(ctx_DataAccess.TEMP_CUSTOM_VIEW['CL_NBR'], ctx_DataAccess.TEMP_CUSTOM_VIEW['CO_NBR'], True);
                  if (ConvertNull(ctx_DataAccess.TEMP_CUSTOM_VIEW['LAST_PREPROCESS'], 0) = EndDate) and
                      ((ConvertNull(ctx_DataAccess.TEMP_CUSTOM_VIEW['LAST_PREPROCESS_MESSAGE'], '') = '')
                         or StartsWith(Trim(ctx_DataAccess.TEMP_CUSTOM_VIEW.fieldByName('LAST_PREPROCESS_MESSAGE').AsString), MANUALLY_RECONCILED)) then
                    ProcessAll(ctx_DataAccess.TEMP_CUSTOM_VIEW['CL_NBR'], ctx_DataAccess.TEMP_CUSTOM_VIEW['CO_NBR'], False);
                end;
                ctx_DataAccess.TEMP_CUSTOM_VIEW.Next;
              end;
        end
        else
        begin
          DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.IndexFieldNames := 'CL_NBR;CO_TAX_RETURN_QUEUE_NBR';
          for i := 0 to Filter.Count-1 do
          begin
            s := Filter[i];
            ProcessSelected(StrToInt(GetNextStrValue(s, ',')), s);
          end;
        end;
    finally
      if (Reports.Count > 0)  then
         CheckClientReports(-1, true);
      Reports.Free;
    end;

  finally
    DM_TEMPORARY.TMP_PR.Close;
    DM_TEMPORARY.TMP_CO.Close;
    DM_TEMPORARY.TMP_CL.Close;
    DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.Close;
    ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
    ctx_DataAccess.SY_CUSTOM_VIEW.Close;
  end;
end;

function TevAsyncFunctions.ProcessTaxReturns(const AParams: IisListOfValues): IisListOfValues;
var
  ReportResults: TrwReportResults;
  r: TrwReportResults;
  RS, CompressedSignedStream: IevDualStream;
  s, Cond: string;
  i, ClNbr: Integer;
  RepRes: IisListOfValues;
  sExceptions: String;
  RepList: TrwReportList;

begin

  RepList := TrwReportList.Create;
  try
    RS := IInterface(AParams.Value['ReportList']) as IevDualStream;
    RS.Position := 0;
    RepList.SetFromStream(RS);
    ReportResults := ctx_RWRemoteEngine.RunReports2(RepList);
  finally
    RepList.Free;
  end;

  Result := TisListOfValues.Create;
  try
    if ReportResults.Count > 0 then
    begin
      try
          sExceptions := ReportResults.GetAllExceptions;

          s := ReportResults[0].Tag;
          Fetch(s);
          ClNbr := StrToInt(Fetch(s));

          ctx_DataAccess.OpenClient(ClNbr);
          Cond := '-1';
          for i := 0 to ReportResults.Count - 1 do
          begin
            s := ReportResults[i].Tag;
            if ReportResults[i].ErrorMessage = '' then
              Cond := Cond + ',' + Fetch(s)
            else
              Fetch(s);
            Fetch(s);

            RepRes := TisListOfValues.Create;
            RepRes.AddValue('ReportDesc', s);

            if ReportResults[i].ErrorMessage <> '' then
              s := ReportResults[i].ErrorMessage
            else if ReportResults[i].WarningMessage <> '' then
              s := ReportResults[i].WarningMessage;

            RepRes.AddValue('Message', StringReplace(StringReplace(s, #10, '', [rfReplaceAll]), #13, '; ', [rfReplaceAll]));

            Result.AddValue('Report' + IntToStr(i), RepRes);
          end;

          // save report results into DB
          ctx_DataAccess.StartNestedTransaction([dbtClient]);
          try
            DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ Cond+ ')');
            DM_COMPANY.CO_TAX_RETURN_RUNS.DataRequired('CO_TAX_RETURN_RUNS_NBR = -1');

            for i := 0 to ReportResults.Count-1 do
            begin
              if ReportResults[i].ErrorMessage = '' then
              begin
                s := ReportResults[i].Tag;
                s := Fetch(s);
                Assert(s <> '');
                if not DM_COMPANY.CO_TAX_RETURN_QUEUE.Locate('CO_TAX_RETURN_QUEUE_NBR', StrToInt(s), []) then
                  raise EInconsistentData.CreateFmtHelp('Client #%u is out of sync with temporary tables', [ctx_DataAccess.ClientId], IDH_InconsistentData);

                try
                  DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
                except
                  on E: Exception do
                  begin
                    E.Message := Format('Company #%s %s generated error: %s', [
                      DM_COMPANY.CO.ShadowDataSet.CachedLookup(DM_COMPANY.CO_TAX_RETURN_QUEUE.CO_NBR.AsInteger,
                        'CUSTOM_COMPANY_NUMBER'),
                      DM_COMPANY.CO.ShadowDataSet.CachedLookup(DM_COMPANY.CO_TAX_RETURN_QUEUE.CO_NBR.AsInteger,
                        'NAME'),
                      E.Message]);
                    raise;
                  end;
                end;

                if DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] <> TAX_RETURN_STATUS_PROCESSED then
                  DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_PROCESSED;

                DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS_DATE'] := Now;

                CompressedSignedStream := TEvDualStreamHolder.Create;
                CompressedSignedStream.WriteInteger($FF);
                r := TrwReportResults.Create;
                try
                  r.Add.Assign(ReportResults[i]);
                  CompressStreamToStream(r.GetAsStream, CompressedSignedStream);
                finally
                  r.Free;
                end;
                CompressedSignedStream.Position := 0;

                if DM_COMPANY.CO_TAX_RETURN_QUEUE.Modified then
                begin


                  DM_COMPANY.CO_TAX_RETURN_QUEUE.Post;

                  DM_COMPANY.CO_TAX_RETURN_RUNS.Append;
                  DM_COMPANY.CO_TAX_RETURN_RUNS.CO_TAX_RETURN_QUEUE_NBR.AsInteger :=
                                DM_COMPANY.CO_TAX_RETURN_QUEUE.CO_TAX_RETURN_QUEUE_NBR.AsInteger;
                  DM_COMPANY.CO_TAX_RETURN_RUNS.UpdateBlobData('CL_BLOB_NBR', CompressedSignedStream);
                  DM_COMPANY.CO_TAX_RETURN_RUNS.CHANGE_DATE.AsDateTime := SysTime;
                  DM_COMPANY.CO_TAX_RETURN_RUNS.Post;
                end
                else
                  DM_COMPANY.CO_TAX_RETURN_QUEUE.Cancel;

                CompressedSignedStream := nil;
              end;
            end;

            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE, DM_COMPANY.CO_TAX_RETURN_RUNS]);

            ctx_DataAccess.CommitNestedTransaction;
          except
            ctx_DataAccess.RollbackNestedTransaction;
            raise;
          end;
      except
        on E: Exception do
          AddStrValue(sExceptions, BuildStackedErrorStr(E), #13);
      end;

      if sExceptions <> '' then
        Result.AddValue(PARAM_ERROR, sExceptions);
    end;

  finally
    ReportResults.Free;
    DM_COMPANY.CO_TAX_RETURN_QUEUE.Close;
    DM_COMPANY.CO_TAX_RETURN_RUNS.Close;
  end;
end;

function TevAsyncFunctions.GetExecSatus: IisList;
var
  Calls: IevCurrentCalls;
  Call: IisInterfacedObject;
  i: Integer;
begin
  Result := TisList.Create;
  Calls := FAsyncCallHandler.GetCurrentCalls;
  Calls.Lock;
  try
    Result.Capacity := Calls.Count;
    for i := 0 to Calls.Count - 1 do
    begin
      Call := TevAsyncCallInfo.Create;
      TevAsyncCallInfo(Call.GetImplementation).FName := Calls[i].Name;
      TevAsyncCallInfo(Call.GetImplementation).FCallID := Calls[i].CallID;
      TevAsyncCallInfo(Call.GetImplementation).FUser := Calls[i].User;
      TevAsyncCallInfo(Call.GetImplementation).FPriority := Calls[i].EvoPriority;
      TevAsyncCallInfo(Call.GetImplementation).FStartTime := Calls[i].StartTime;
      Result.Add(Call);
    end;
  finally
    Calls.Unlock;
  end;
end;

function TevAsyncFunctions.ProcessManualACH(
  const AParams: IisListOfValues): IisListOfValues;
var
  AchFile, AchRegularReport, AchDetailedReport: IEvDualStream;
  AchSave: IisListOfValues;
  sExceptions: String;
  AchFileName: String;
begin
  ctx_PayrollProcessing.ProcessManualACH(
    IInterface(AParams.Value['ACHOptions']) as IevACHOptions,
    AParams.Value['bPreprocess'],
    AParams.Value['BatchBANK_REGISTER_TYPE'],
    IInterface(AParams.Value['ACHDataStream']) as IEvDualStream,
    AchFile, AchRegularReport, AchDetailedReport, AchSave, AchFileName, sExceptions,
    AParams.Value['ACHFolder']);

  Result := TisListOfValues.Create;
  Result.AddValue('AchFile', AchFile);
  Result.AddValue('AchRegularReport', AchRegularReport);
  Result.AddValue('AchDetailedReport', AchDetailedReport);
  Result.AddValue('AchSave', AchSave);
  Result.AddValue('AchFileName', AchFileName);

  if sExceptions <> '' then
    Result.AddValue(PARAM_ERROR, sExceptions);
end;

function TevAsyncFunctions.EvoXEngineImport(const AParams: IisListOfValues): IisListOfValues;
var
  ImportResult: IisListOfValues;
  InputData: IevDataSet;
  MapFile: IEvExchangeMapFile;
  Package: IEvExchangePackage;
  Options: IisStringListRO;
  CustomCoNbr: String;
  EffectiveDate: TDateTime;

  iMessagesAmount: Integer;
  ExtErrorsList: IEvExchangeResultLog;
  s: String;
begin
  Result := TisListOfValues.Create;

  InputData := IInterface(AParams.Value['InputData']) as IevDataSet;
  MapFile := IInterface(AParams.Value['MapFile']) as IEvExchangeMapFile;
  Package := IInterface(AParams.Value['Package']) as IEvExchangePackage;
  Options := IInterface(AParams.Value['Options']) as IisStringListRO;
  CustomCoNbr := AParams.Value['CustomCoNbr'];
  EffectiveDate := AParams.Value['EffectiveDate'];

  ImportResult := Context.EvolutionExchangeEngine.Import(InputData, MapFile, Package, Options, CustomCoNbr, EffectiveDate);

  iMessagesAmount :=  ImportResult.Value[EvoXAllMessagesAmount];
  if iMessagesAmount > 0 then
  begin
    if ImportResult.ValueExists(EvoXExtErrorsList) then
    begin
      ExtErrorsList := IInterface(ImportResult.Value[EvoXExtErrorsList]) as IEvExchangeResultLog;
      s := 'Import finished with ' + IntToStr(ExtErrorsList.GetAmountOfErrors) + ' error(s) and ' +
        IntToStr(ExtErrorsList.GetAmountOfWarnings) + ' warning(s). Please see results for more details';
    end
    else
      s := 'Import finished with ' + IntToStr(iMessagesAmount) + ' error(s). Please see results for more details';
    Result.AddValue(PARAM_ERROR, s);
  end;

  Result.AddValue(PARAM_RESULT, ImportResult);
end;

function TevAsyncFunctions.EvoXEngineCheckMapFile(const AParams: IisListOfValues): IisListOfValues;
var
  CheckMapResult: IisListOfValues;
  MapFile: IEvExchangeMapFile;
  Package: IEvExchangePackage;
  Options: IisStringListRO;

  iAllMessagesAmount: Integer;
  s: String;
begin
  Result := TisListOfValues.Create;

  Package := IInterface(AParams.Value['Package']) as IEvExchangePackage;
  MapFile := IInterface(AParams.Value['MapFile']) as IEvExchangeMapFile;
  Options := IInterface(AParams.Value['Options']) as IisStringListRO;

  CheckMapResult := Context.EvolutionExchangeEngine.CheckMapFile(MapFile, Package, Options);

  iAllMessagesAmount :=  CheckMapResult.Value[EvoXAllMessagesAmount];
  if iAllMessagesAmount > 0 then
  begin
    s := 'Check map finished with ' + IntToStr(iAllMessagesAmount) + ' error(s). Please see results for more details';
    Result.AddValue(PARAM_ERROR, s);
  end;

  Result.AddValue(PARAM_RESULT, CheckMapResult);
end;

function TevAsyncFunctions.GetBenefitEmailNotifications(const AParams: IisListOfValues): IisListofValues;
var
  iClientId: integer;
  EMailNotifications: IisListOfValues;
begin
  Result := TisListOfValues.Create;
  iClientId := AParams.Value['ClientId'];
  ctx_DataAccess.OpenClient(iClientId);
  EMailNotifications := Context.RemoteMiscRoutines.GetBenefitEmailNotificationsList;
  Result.AddValue(PARAM_RESULT, EMailNotifications);
end;

function TevAsyncFunctions.PrepareForGetBenefitEmailNotifications(const AParams: IisListOfValues): IisListOfValues;
var
  sCl: String;
  Clients: IisStringList;
  ForAllClients: Boolean;
  ClientNumbers: String;
  qTmpCl: IevQuery;
begin
  ctx_UpdateWait('Defining client list');
  try
    ForAllClients := AParams.Value['ForAllClients'];
    ClientNumbers := AParams.Value['ClientNumbers'];

    Clients := TisStringList.CreateUnique;

    if ForAllClients then
    begin
      qTmpCl := TevQuery.Create('select CL_NBR from TMP_CL');
      qTmpCl.Execute;
      qTmpCl.Result.First;
      while not qTmpCl.Result.Eof do
      begin
        Clients.Add(VarToStr(qTmpCl.Result['CL_NBR']));
        qTmpCl.Result.Next;
      end;
    end
    else
    begin
      ClientNumbers := Trim(ClientNumbers);
      while ClientNumbers <> '' do
      begin
        sCl := Trim(GetNextStrValue(ClientNumbers, ','));
        Clients.Append(sCl);
      end;
    end;

    Result := TisListOfValues.Create;
    Result.AddValue(PARAM_RESULT, Clients.Text);
  finally
    ctx_EndWait;
  end;
end;

function TevAsyncFunctions.PAMonthEndCleanup(
  const AParams: IisListOfValues): IisListOfValues;
var
  Res: Boolean;
  Q: IevQuery;
  PrNbr: Integer;
  Warnings: String;
begin
  ctx_DataAccess.OpenClient(AParams.Value['ClientID']);

  Res := ctx_PayrollProcessing.DoPAMonthEndCleanup(
    AParams.Value['CoNbr'],
    AParams.Value['MonthEndDate'],
    AParams.Value['MinTax'],
    AParams.Value['AutoProcess'],
    AParams.Value['EECustomNbr'],
    AParams.Value['PrintReports'],
    TResourceLockType(AParams.Value['LockType']),
    Warnings);

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, Res);
  Result.AddValue(PARAM_WARNING, Warnings);
  PrNbr := 0;

  if Res then
  begin
    Q := TevQuery.Create('SELECT PR_NBR, Max(RUN_NUMBER) FROM TMP_PR WHERE CL_NBR = :cl_nbr AND CO_NBR = :co_nbr ' +
                         'AND STATUS = ''' + PAYROLL_STATUS_PROCESSED + ''' ' +
                         'AND PAYROLL_TYPE = ''' + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + ''' ' +
                         'AND CHECK_DATE = :Check_Date ' +
                         'GROUP BY PR_NBR');
    Q.Params.AddValue('cl_nbr', AParams.Value['ClientID']);
    Q.Params.AddValue('co_nbr', AParams.Value['CoNbr']);
    Q.Params.AddValue('Check_Date', AParams.Value['MonthEndDate']);

    PrNbr :=  Q.Result.FieldByName('PR_NBR').AsInteger;
  end;

  Result.AddValue('PayrollNumber', PrNbr);
end;

function TevAsyncFunctions.FinalizeRebuildAllClients(const AParams: IisListOfValues): IisListOfValues;
begin
  ctx_StartWait('Finalizing temp tables rebuilding');
  try
    Result := TisListOfValues.Create;
    try
      ctx_DBAccess.EndRebuildAllClients;
    except
      on E: EisException do
        Result.AddValue(PARAM_ERROR, E.GetDatailedMessage);
      else
        raise;
    end;
  finally
    ctx_EndWait;
  end;
end;

function TevAsyncFunctions.CollectDashboardResults(
  aRes: TrwReportResults): IisListOfValues;
var
  i: Integer;
  RepRes: TrwReportResult;
  repParam: TrwReportParam;
begin
  // Collect dashboard data
  Result := TisListOfValues.Create;
  for i := 0 to aRes.Count - 1 do
  begin
    RepRes := aRes[i];
    repParam := RepRes.ReturnValues.ParamByName(__DASHBOARD);
    if (RepRes.ErrorMessage = '') and Assigned(repParam) then
      if Boolean(repParam.Value) then
      begin
        repParam := RepRes.ReturnValues.ParamByName(__DASHBOARD_DATA);
        if Assigned(repParam) then
          Result.AddValue(IntToStr(Result.Count), repParam.Value);
      end;
  end;

  if Result.Count = 0 then
    Result := nil;
end;

{ TevAsyncCallInfo }

function TevAsyncCallInfo.CallID: TisGUID;
begin
  Result := FCallID;
end;

function TevAsyncCallInfo.Name: String;
begin
  Result := FName;
end;

function TevAsyncCallInfo.Priority: Integer;
begin
  Result := FPriority;
end;

function TevAsyncCallInfo.StartTime: TDateTime;
begin
  Result := FStartTime;
end;

function TevAsyncCallInfo.User: String;
begin
  Result := FUser;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetAsyncFunctions, IevAsyncFunctions, 'Async Functions');


end.
