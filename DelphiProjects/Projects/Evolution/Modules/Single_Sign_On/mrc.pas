unit mrc;
// My Recruiting Center

interface

uses
  sysutils;

type
(*
What happens when a User is logged in
When a Single Sign-on request is made, the following happens.
1)	The requestor is authenticated with the Company UserID and Password
2)	The UID is looked up in our system under the requestors account.
3)	If the UID is located, the User's information is updated in our system.
4)	If the UID is not located, a new User login is created.
5)	A onetime use Single Sign-on Login URL is generated and returned.
After all testing is completed contact IAM at ctarver@iamresources.com to have a production Company UserID and Password generated for you.
*)

  TMRCLoginInfo = record
    UserID: string;
    FirstName: string;
    LastName: string;
    MiddleName: string;
    Email: string;
    CompanyCode: string;
    CompanyName: string;
  end;

  EMRCLoginError = Exception;

function GetMRCOneTimeURL(superUserID, superUserPassword: string; const mrcLoginInfo: TMRCLoginInfo): string;

implementation

uses
  wininet, xmldoc, xmlintf, windows, xmlrpccommon, variants, activex;


type
  EisException = Exception; //stub

function HttpPost(const UserAgent: string; const Server: string; const Resource: string; const Data: AnsiString): string;
var
  hInet: HINTERNET;
  hHTTP: HINTERNET;
  hReq: HINTERNET;
  Buffer: array[0..1023] of AnsiChar;
  tmp: string;
  BufferLen: Cardinal;
const
  accept: packed array[0..1] of LPSTR = (PChar('*/*'), nil);
  header: string = 'Content-Type: application/x-www-form-urlencoded';
begin
  Result := '';

  hInet := InternetOpen(PChar(UserAgent), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if hInet = nil then
    raise EisException.Create('Unable to get access to WinInet.Dll');
  try
    hHTTP := InternetConnect(hInet, PChar(Server), INTERNET_DEFAULT_HTTPS_PORT, nil, nil, INTERNET_SERVICE_HTTP, 0, 1);
    if hHTTP = nil then
      raise EisException.CreateFmt('Cannot connect to %s',[Server]);
    try
      hReq := HttpOpenRequest(hHTTP, PChar('POST'), PChar(Resource), nil, nil, @accept, INTERNET_FLAG_SECURE, 1);
      if hReq = nil then
        raise EisException.CreateFmt('Cannot connect to %s',[Server+'\'+Resource]);
      try
        if not HttpSendRequest(hReq, PChar(header), Length(header), PChar(Data), Length(Data)) then
          raise EisException.CreateFmt('Cannot connect to %s: %s',[Server+'\'+Resource, SysErrorMessage(GetLastError)]);
        repeat
          if not InternetReadFile(hReq, @Buffer, SizeOf(Buffer), BufferLen) then
            raise EisException.CreateFmt('Cannot read data from %s: %s',[Server+'\'+Resource, SysErrorMessage(GetLastError)]);
          if BufferLen > 0 then
          begin
          	SetString(tmp, Buffer, BufferLen);
            Result := Result + tmp;
          end;
        until BufferLen = 0;
      finally
        InternetCloseHandle(hReq);
      end;
    finally
      InternetCloseHandle(hHTTP);
    end;
  finally
    InternetCloseHandle(hInet);
  end;
end;

function URLFromXML(xml: string): string;
var
  LoginURLDoc: IXmlDocument;
begin
  if xml = '' then
    raise EMRCLoginError.Create('MRC AutoLoginHTTP.aspx returned empty result');

  LoginURLDoc := LoadXMLData(xml);
  if LoginURLDoc.DocumentElement = nil then
    raise EMRCLoginError.Create('MRC AutoLoginHTTP.aspx returned empty xml document');
  LoginURLDoc.Options := LoginURLDoc.Options + [doNodeAutoCreate];

  if SameText(varToStr(LoginURLDoc.DocumentElement.ChildValues['IsValidated']), 'true') then
  begin
    Result := trim(varToStr(LoginURLDoc.DocumentElement.ChildValues['URL']));
    if Result = '' then
      raise EMRCLoginError.Create('MRC AutoLoginHTTP.aspx returned empty url');
  end
  else
    raise EMRCLoginError.CreateFmt('MRC AutoLoginHTTP.aspx returned an error: %s', [varToStr(LoginURLDoc.DocumentElement.ChildValues['ValidationError'])]);
end;

function GetMRCOneTimeURL(superUserID, superUserPassword: string; const mrcLoginInfo: TMRCLoginInfo): string;
var
  data: string;
begin
  try
    CoInitialize(nil);
    try
      data := 'CompanyUserID=' + URLEncode(superUserID) +
              '&CompanyPassword=' + URLEncode(superUserPassword) +
              '&UID=' + URLEncode(mrcLoginInfo.UserID) +
              '&Embedded=N' +
              '&ShowLogo=Y' +
              '&FirstName=' + URLEncode(mrcLoginInfo.FirstName) +
              '&MiddleName=' + URLEncode(mrcLoginInfo.MiddleName) +
              '&LastName=' + URLEncode(mrcLoginInfo.LastName) +
              '&EmailAddress=' + URLEncode(mrcLoginInfo.Email) +
              '&ClientID=' + URLEncode(mrcLoginInfo.CompanyCode) +
              '&ClientName=' + URLEncode(mrcLoginInfo.CompanyName) +
  {
        '&Address=' +
        '&Address2=' +
        '&City=' +
        '&State=' +
        '&Zip=' +
        '&Country=' +
        '&Phone=' +
  }
              '&UserRole=' + URLEncode('Internal Portal Client');
        Result := UrlFromXML( HttpPost('ISYSTEMS', 'www.myrecruitingcenter.com', 'AutoLoginHTTP.aspx', data) );
    finally
      CoUnInitialize;
    end
  except
    on E: EMRCLoginError do raise;
    on E: Exception do raise EMRCLoginError.Create(E.Message);
  end
end;


end.


