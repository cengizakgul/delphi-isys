// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : E:\job\IS\integration\EvoSwipeclockLogin\EmployeeWebInterface.wsdl
// Encoding : UTF-8
// Version  : 1.0
// (2/9/2012 11:21:47 AM - 1.33.2.5)
// ************************************************************************ //

unit EmployeeWebInterface;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"
  // !:float           - "http://www.w3.org/2001/XMLSchema"
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"
  // !:long            - "http://www.w3.org/2001/XMLSchema"
  // !:string          - "http://mc2cs.com/sc"

  AuthHeader           = class;                 { "https://mc2cs.com/scci"[H] }
  TerminalPunch        = class;                 { "http://mc2cs.com/sc" }
  ArrayOfTerminalPunch = class;                 { "http://mc2cs.com/sc"[A] }
  TerminalBatch        = class;                 { "http://mc2cs.com/sc" }



  // ************************************************************************ //
  // Namespace : https://mc2cs.com/scci
  // ************************************************************************ //
  AuthHeader = class(TSOAPHeader)
  private
    FuserName: WideString;
    Fpassword: WideString;
    Fsite: WideString;
  published
    property userName: WideString read FuserName write FuserName;
    property password: WideString read Fpassword write Fpassword;
    property site: WideString read Fsite write Fsite;
  end;

  ArrayOfString = array of WideString;          { "https://mc2cs.com/scci" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/sc
  // ************************************************************************ //
  TerminalPunch = class(TRemotable)
  private
    Frecnum: WideString;
    Fdupe: WideString;
  published
    property recnum: WideString read Frecnum write Frecnum stored AS_ATTRIBUTE;
    property dupe: WideString read Fdupe write Fdupe stored AS_ATTRIBUTE;
  end;

  Punch      = array of TerminalPunch;          { "http://mc2cs.com/sc" }


  // ************************************************************************ //
  // Namespace : http://mc2cs.com/sc
  // Serializtn: [xoInlineArrays]
  // ************************************************************************ //
  ArrayOfTerminalPunch = class(TRemotable)
  private
    FPunch: Punch;
  public
    constructor Create; override;
    destructor Destroy; override;
    function   GetTerminalPunchArray(Index: Integer): TerminalPunch;
    function   GetTerminalPunchArrayLength: Integer;
    property   TerminalPunchArray[Index: Integer]: TerminalPunch read GetTerminalPunchArray; default;
    property   Len: Integer read GetTerminalPunchArrayLength;
  published
    property Punch: Punch read FPunch write FPunch;
  end;



  // ************************************************************************ //
  // Namespace : http://mc2cs.com/sc
  // ************************************************************************ //
  TerminalBatch = class(TRemotable)
  private
    FPunchRecords: ArrayOfTerminalPunch;
    FBatchRecordNumber: Int64;
    FBatchNumber: WideString;
    FBatchRevision: Integer;
    FSiteName: WideString;
    FSiteNumber: WideString;
    FRecordsExpected: Integer;
    FPlatformName: WideString;
    FTimeOffsetMinutes: Integer;
    FClockSerialNumber: WideString;
    FVariableNames: WideString;
    FDialPrefix: WideString;
    FDialNumber1: WideString;
    FDialNumber2: WideString;
    FEpromVersion: WideString;
    FSoftwareVersion: WideString;
    FFingerFirmwareVersion: WideString;
    FConnectionMethod: WideString;
    FLocationStamp: WideString;
    FLastUpdateUTC: WideString;
    FUsingTcpModem: WideString;
    FAbbreviated: WideString;
  public
    destructor Destroy; override;
  published
    property PunchRecords: ArrayOfTerminalPunch read FPunchRecords write FPunchRecords;
    property BatchRecordNumber: Int64 read FBatchRecordNumber write FBatchRecordNumber stored AS_ATTRIBUTE;
    property BatchNumber: WideString read FBatchNumber write FBatchNumber stored AS_ATTRIBUTE;
    property BatchRevision: Integer read FBatchRevision write FBatchRevision stored AS_ATTRIBUTE;
    property SiteName: WideString read FSiteName write FSiteName stored AS_ATTRIBUTE;
    property SiteNumber: WideString read FSiteNumber write FSiteNumber stored AS_ATTRIBUTE;
    property RecordsExpected: Integer read FRecordsExpected write FRecordsExpected stored AS_ATTRIBUTE;
    property PlatformName: WideString read FPlatformName write FPlatformName stored AS_ATTRIBUTE;
    property TimeOffsetMinutes: Integer read FTimeOffsetMinutes write FTimeOffsetMinutes stored AS_ATTRIBUTE;
    property ClockSerialNumber: WideString read FClockSerialNumber write FClockSerialNumber stored AS_ATTRIBUTE;
    property VariableNames: WideString read FVariableNames write FVariableNames stored AS_ATTRIBUTE;
    property DialPrefix: WideString read FDialPrefix write FDialPrefix stored AS_ATTRIBUTE;
    property DialNumber1: WideString read FDialNumber1 write FDialNumber1 stored AS_ATTRIBUTE;
    property DialNumber2: WideString read FDialNumber2 write FDialNumber2 stored AS_ATTRIBUTE;
    property EpromVersion: WideString read FEpromVersion write FEpromVersion stored AS_ATTRIBUTE;
    property SoftwareVersion: WideString read FSoftwareVersion write FSoftwareVersion stored AS_ATTRIBUTE;
    property FingerFirmwareVersion: WideString read FFingerFirmwareVersion write FFingerFirmwareVersion stored AS_ATTRIBUTE;
    property ConnectionMethod: WideString read FConnectionMethod write FConnectionMethod stored AS_ATTRIBUTE;
    property LocationStamp: WideString read FLocationStamp write FLocationStamp stored AS_ATTRIBUTE;
    property LastUpdateUTC: WideString read FLastUpdateUTC write FLastUpdateUTC stored AS_ATTRIBUTE;
    property UsingTcpModem: WideString read FUsingTcpModem write FUsingTcpModem stored AS_ATTRIBUTE;
    property Abbreviated: WideString read FAbbreviated write FAbbreviated stored AS_ATTRIBUTE;
  end;

  ArrayOfTerminalBatch = array of TerminalBatch;   { "https://mc2cs.com/scci" }

  // ************************************************************************ //
  // Namespace : https://mc2cs.com/scci
  // soapAction: https://mc2cs.com/scci/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : EmployeeWebInterfaceSoap
  // service   : EmployeeWebInterface
  // port      : EmployeeWebInterfaceSoap
  // URL       : https://www.payrollservers.us:8003/scci/xml/EmployeeWebInterface.asmx
  // ************************************************************************ //
  EmployeeWebInterfaceSoap = interface(IInvokable)
  ['{69FAA31D-17A5-35A0-4044-582F83FEB7CD}']
    function  GetEmployees: WideString; stdcall;
    function  GetEmployees2(const ExcludeTerminated: Boolean): WideString; stdcall;
    function  GetEmployeesByFilter(const filterBy: WideString; const filter: WideString): WideString; stdcall;
    function  GetEmployeesByFilter2(const filterBy: WideString; const filter: WideString; const ExcludeTerminated: Boolean): WideString; stdcall;
    function  GetEmployeesOverload(const doesNothing: WideString): WideString; stdcall;
    function  GetEmployee(const employee: WideString): WideString; stdcall;
    function  GetEmployeeCard(const employeeCode: WideString; const period: WideString): WideString; stdcall;
    function  AddEmployee(const employeeCode: WideString; const lastName: WideString; const firstName: WideString; const middleName: WideString; const designation: WideString; const title: WideString; const ssn: WideString; const dept: WideString; const location: WideString; const supervisor: WideString; 
                          const startDate: WideString; const endDate: WideString; const lunch: Integer; const lunchHours: Single; const payRate0: TXSDecimal; const payRate1: TXSDecimal; const payRate2: TXSDecimal; const payRate3: TXSDecimal; const cardnum1: WideString; 
                          const cardNum2: WideString; const cardNum3: WideString; const password: WideString; const options: WideString; const home1: WideString; const home2: WideString; const home3: WideString; const schedule: WideString; const exportBlock: Integer
                          ): WideString; stdcall;
    function  AddEmployeeRequired(const lastName: WideString; const firstName: WideString): WideString; stdcall;
    function  RemoveEmployee(const employeeCode: WideString; const delDate: WideString): WideString; stdcall;
    function  GetOneTimeCredential(const login: WideString): WideString; stdcall;
    function  GetPayPeriod(const theDate: WideString): WideString; stdcall;
    function  UpdateSiteSetting(const FieldToUpdate: WideString; const NewValue: WideString): WideString; stdcall;
    function  UpdateEmployeeFields(const AddIfNotFound: Boolean; const employeeCode: WideString; const lastName: WideString; const firstName: WideString; const middleName: WideString; const FieldsToUpdate: ArrayOfString): WideString; stdcall;
    function  UpdateEmployee(const employeeCode: WideString; const employee: Integer; const lastName: WideString; const firstName: WideString; const middleName: WideString; const designation: WideString; const title: WideString; const ssn: WideString; const dept: WideString; const location: WideString; 
                             const supervisor: WideString; const startDate: WideString; const endDate: WideString; const lunch: Integer; const lunchHours: Single; const payRate0: TXSDecimal; const payRate1: TXSDecimal; const payRate2: TXSDecimal; const payRate3: TXSDecimal; 
                             const cardnum1: WideString; const cardNum2: WideString; const cardNum3: WideString; const password: WideString; const options: WideString; const home1: WideString; const home2: WideString; const home3: WideString; const schedule: WideString; 
                             const exportBlock: Integer): WideString; stdcall;
    function  GetSiteInfo: WideString; stdcall;
    function  GetSiteCards(const startDate: WideString; const endDate: WideString): WideString; stdcall;
    function  GetSiteCardsFiltered(const startDate: WideString; const endDate: WideString; const eeListMatchField: WideString; const eeList: WideString): WideString; stdcall;
    function  GetAllSiteCardsFiltered(const startDate: WideString; const endDate: WideString; const eeListMatchField: WideString; const eeList: WideString): WideString; stdcall;
    function  GetActivityFile(const fileName: WideString; const startDate: WideString; const endDate: WideString): WideString; stdcall;
    function  GetActivityFileLaborMapped(const fileName: WideString; const startDate: WideString; const endDate: WideString; const laborMapping: WideString): WideString; stdcall;
    function  UpdateEmployeeTime(const punchID: Integer; const employee: Integer; const inPunch: WideString; const outPunch: WideString; const punchDate: WideString; const iData: WideString; const jData: WideString; const kData: WideString; const xData: WideString; const yData: WideString; 
                                 const zData: WideString; const hours: WideString; const lunch: Integer; const payrate: WideString; const category: WideString; const pay: WideString; const payType: WideString): WideString; stdcall;
    function  UpdateEmployeeTimeUTC(const punchID: Integer; const employee: Integer; const inPunch: WideString; const outPunch: WideString; const punchDate: WideString; const iData: WideString; const jData: WideString; const kData: WideString; const xData: WideString; const yData: WideString; 
                                    const zData: WideString; const hours: WideString; const lunch: Integer; const payrate: WideString; const category: WideString; const pay: WideString; const payType: WideString): WideString; stdcall;
    function  GetPunchInfo(const punchID: Integer): WideString; stdcall;
    function  GetTerminalBatches(const IgnoreThru: Int64): ArrayOfTerminalBatch; stdcall;
    function  DeleteEmployeePunch(const punchID: Integer): WideString; stdcall;
    function  GetSites(const login: WideString): WideString; stdcall;
    function  GetSitesByMasterReseller: WideString; stdcall;
    function  GetActiveEmployeeCount(const year: WideString; const month: WideString): WideString; stdcall;
  end;

function GetEmployeeWebInterfaceSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): EmployeeWebInterfaceSoap;


implementation

function GetEmployeeWebInterfaceSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): EmployeeWebInterfaceSoap;
const
  defWSDL = 'E:\job\IS\integration\EvoSwipeclockLogin\EmployeeWebInterface.wsdl';
  defURL  = 'https://www.payrollservers.us:8003/scci/xml/EmployeeWebInterface.asmx';
  defSvc  = 'EmployeeWebInterface';
  defPrt  = 'EmployeeWebInterfaceSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as EmployeeWebInterfaceSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


constructor ArrayOfTerminalPunch.Create;
begin
  inherited Create;
  FSerializationOptions := [xoInlineArrays];
end;

destructor ArrayOfTerminalPunch.Destroy;
var
  I: Integer;
begin
  for I := 0 to Length(FPunch)-1 do
    if Assigned(FPunch[I]) then
      FPunch[I].Free;
  SetLength(FPunch, 0);
  inherited Destroy;
end;

function ArrayOfTerminalPunch.GetTerminalPunchArray(Index: Integer): TerminalPunch;
begin
  Result := FPunch[Index];
end;

function ArrayOfTerminalPunch.GetTerminalPunchArrayLength: Integer;
begin
  if Assigned(FPunch) then
    Result := Length(FPunch)
  else
  Result := 0;
end;

destructor TerminalBatch.Destroy;
begin
  if Assigned(FPunchRecords) then
    FPunchRecords.Free;
  inherited Destroy;
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(EmployeeWebInterfaceSoap), 'https://mc2cs.com/scci', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(EmployeeWebInterfaceSoap), 'https://mc2cs.com/scci/%operationName%');
  InvRegistry.RegisterHeaderClass(TypeInfo(EmployeeWebInterfaceSoap), AuthHeader, 'AuthHeader', '');
  RemClassRegistry.RegisterXSClass(AuthHeader, 'https://mc2cs.com/scci', 'AuthHeader');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfString), 'https://mc2cs.com/scci', 'ArrayOfString');
  RemClassRegistry.RegisterXSClass(TerminalPunch, 'http://mc2cs.com/sc', 'TerminalPunch');
  RemClassRegistry.RegisterXSInfo(TypeInfo(Punch), 'http://mc2cs.com/sc', 'Punch');
  RemClassRegistry.RegisterXSClass(ArrayOfTerminalPunch, 'http://mc2cs.com/sc', 'ArrayOfTerminalPunch');
  RemClassRegistry.RegisterSerializeOptions(ArrayOfTerminalPunch, [xoInlineArrays]);
  RemClassRegistry.RegisterXSClass(TerminalBatch, 'http://mc2cs.com/sc', 'TerminalBatch');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfTerminalBatch), 'https://mc2cs.com/scci', 'ArrayOfTerminalBatch');

end.
 