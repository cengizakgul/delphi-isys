unit EvSingleSignOnMod;

interface

uses Classes, SysUtils, Variants,
     isBaseClasses, EvCommonInterfaces, EvMainboard, EvContext, isBasicUtils,
     SwipeClock, EvConsts, EvTypes, mrc, EvDataSet, EvUtils,EvStreamUtils,DB, SEncryptionRoutines;

implementation

type
  TevSingleSignOnObject = class(TisInterfacedObject, IevSingleSignOn)
  protected
    function  GetSwipeClockOneTimeURL(const ACoNbr: integer): String;
    function  GetMRCOneTimeURL(const ACoNbr: integer): string;
    function  GetSwipeClockInfo: IisListOfValues;    
  end;

function GetSingleSignOn: IevSingleSignOn;
begin
  Result := TevSingleSignOnObject.Create;
end;

{ TevSingleSignOnObject }

function TevSingleSignOnObject.GetSwipeClockOneTimeURL(const ACoNbr: integer): String;
const
  NoSetupErrorMessage = 'Your account is not setup, please contact your Service Bureau.';
  NoAdminErrorMessage = 'No Admin username and password for EvoClock';
  NoSiteNumberErrorMessage = 'No site number for EvoClock';
var
  Q: IevQuery;
  sAdminUser, sAdminPassword: String;
  iSiteNumber: integer;
  List : IisListOfValues;
  tmpStream: IisStream;
begin
  sAdminUser := '';
  sAdminPassword := '';

  CheckCondition(Context.License.FindAPIKey(tvkEvoSwipeClockAdvanced) <> nil, 'You have no license for EvoClock');

  CheckCondition(ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_SWIPECLOCK') = stEnabled, 'User has no access to swipeclock login');

  Q := TevQuery.Create('select * from SB_STORAGE where {asOfNow<SB_STORAGE>} AND TAG=''SWIPECLOCK'' order by SB_STORAGE_NBR');
  Q.Execute;

  if not Q.Result.FieldByName('Storage_data').IsNull then
  begin
    tmpStream := TevDualStreamHolder.CreateInMemory;
    TBlobField(Q.Result.FieldByName('STORAGE_DATA')).SaveToStream(tmpStream.RealStream);
    tmpStream.Position := 0;
    List := TisListOfValues.Create;
    List.ReadFromStream(tmpStream);
    if List.ValueExists(SBSTORAGE_TAG_SSOUSERNAME) then
       sAdminUser := List.Value[SBSTORAGE_TAG_SSOUSERNAME];

    if List.ValueExists(SBSTORAGE_TAG_SSOPASSWORD) then
       sAdminPassword := DecryptString(List.Value[SBSTORAGE_TAG_SSOPASSWORD]);
  end;
  CheckCondition(sAdminUser <> '', NoSetupErrorMessage + #13 + NoAdminErrorMessage);
  CheckCondition(sAdminPassword <> '', NoSetupErrorMessage + #13 + NoAdminErrorMessage);

  Q := TEvQuery.Create('select WC_OFFS_BANK_ACCOUNT_NBR from CO where {AsOfNow<CO>} and CO_NBR=:p_co_nbr');
  Q.Params.AddValue('p_co_nbr', ACoNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(ACoNbr));
  CheckCondition(Q.Result['WC_OFFS_BANK_ACCOUNT_NBR'] <> null, NoSetupErrorMessage + #13 + NoSiteNumberErrorMessage);
  iSiteNumber := Q.Result['WC_OFFS_BANK_ACCOUNT_NBR'];

  Result := SwipeClock.GetSwipeClockSignOnURL(sAdminUser, sAdminPassword, iSiteNumber, Context.UserAccount.User);
end;

function TevSingleSignOnObject.GetMRCOneTimeURL(const ACoNbr: integer): string;
const
  NoSetupErrorMessage = 'Your account is not setup, please contact your Service Bureau.';
  NoAdminErrorMessage = 'No Admin username and password for Evo My Recruiting Center';
var
  Q: IevQuery;
  companyUserID, companyPassword : String;
  mrcLoginInfo : TMRCLoginInfo;
  List : IisListOfValues;
  tmpStream: IevDualStream;
begin
  companyUserID := '';
  companyPassword := '';

  CheckCondition(Context.License.FindAPIKey(tvkEvoMyRecruitingCenter) <> nil, 'You have no license for Evo My Recruiting Center');

  CheckCondition(ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_MY_RECRUITING_CENTER') = stEnabled, 'User has no access to My Recruiting Center Single login');

  Q := TevQuery.Create('select * from SB_STORAGE where {AsOfNow<SB_STORAGE>} AND TAG=''MYRECRUITINGCENTER'' order by SB_STORAGE_NBR');
  Q.Execute;

  if not Q.Result.FieldByName('Storage_data').IsNull then
  begin
    tmpStream := TevDualStreamHolder.CreateInMemory;
    TBlobField(Q.Result.FieldByName('STORAGE_DATA')).SaveToStream(tmpStream.RealStream);
    tmpStream.Position := 0;
    List := TisListOfValues.Create;
    List.ReadFromStream(tmpStream);
    if List.ValueExists(SBSTORAGE_TAG_SSOUSERNAME) then
       companyUserID := List.Value[SBSTORAGE_TAG_SSOUSERNAME];

    if List.ValueExists(SBSTORAGE_TAG_SSOPASSWORD) then
       companyPassword := DecryptString(List.Value[SBSTORAGE_TAG_SSOPASSWORD]);
  end;
  CheckCondition(companyUserID <> '', NoSetupErrorMessage + #13 + NoAdminErrorMessage);
  CheckCondition(companyPassword <> '', NoSetupErrorMessage + #13 + NoAdminErrorMessage);

  Q := TEvQuery.Create('select CUSTOM_COMPANY_NUMBER, NAME from CO where {AsOfNow<CO>} and CO_NBR=:p_co_nbr');
  Q.Params.AddValue('p_co_nbr', ACoNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(ACoNbr));

  mrcLoginInfo.CompanyCode := Q.Result['CUSTOM_COMPANY_NUMBER'];
  mrcLoginInfo.CompanyName := Q.Result['NAME'];

  mrcLoginInfo.UserID := Context.UserAccount.User;
  mrcLoginInfo.FirstName := Context.UserAccount.FirstName;
  mrcLoginInfo.MiddleName := '';
  mrcLoginInfo.LastName := Context.UserAccount.LastName;
  mrcLoginInfo.Email := Context.UserAccount.EMail;
  Result := mrc.GetMRCOneTimeURL(companyUserID, companyPassword, mrcLoginInfo);
end;

function TevSingleSignOnObject.GetSwipeClockInfo: IisListOfValues;
const
  NoSetupErrorMessage = 'Your account is not setup, please contact your Service Bureau.';
  NoAdminErrorMessage = 'No Admin username and password for EvoClock';
var
  Q: IevQuery;
  sAdminUser, sAdminPassword: String;
  List : IisListOfValues;
  tmpStream: IisStream;
begin
  Result := TisListOfValues.Create;

  sAdminUser := '';
  sAdminPassword := '';

  CheckCondition(Context.License.FindAPIKey(tvkEvoSwipeClockAdvanced) <> nil, 'You have no license for EvoClock');

  if Context.UserAccount.InternalNbr > 0 then
     CheckCondition(ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_SWIPECLOCK') = stEnabled, 'User has no access to swipeclock login');

  Q := TevQuery.Create('select * from SB_STORAGE where {asOfNow<SB_STORAGE>} AND TAG=''SWIPECLOCK'' order by SB_STORAGE_NBR');
  Q.Execute;

  if not Q.Result.FieldByName('Storage_data').IsNull then
  begin
    tmpStream := TevDualStreamHolder.CreateInMemory;
    TBlobField(Q.Result.FieldByName('STORAGE_DATA')).SaveToStream(tmpStream.RealStream);
    tmpStream.Position := 0;
    List := TisListOfValues.Create;
    List.ReadFromStream(tmpStream);
    if List.ValueExists(SBSTORAGE_TAG_SSOUSERNAME) then
       sAdminUser := List.Value[SBSTORAGE_TAG_SSOUSERNAME];

    if List.ValueExists(SBSTORAGE_TAG_SSOPASSWORD) then
       sAdminPassword := DecryptString(List.Value[SBSTORAGE_TAG_SSOPASSWORD]);
  end;
  CheckCondition(sAdminUser <> '', NoSetupErrorMessage + #13 + NoAdminErrorMessage);
  CheckCondition(sAdminPassword <> '', NoSetupErrorMessage + #13 + NoAdminErrorMessage);

  Result.AddValue('Username', sAdminUser);
  Result.AddValue('Password', sAdminPassword);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSingleSignOn, IevSingleSignOn, 'Single Sign On Module');

end.
