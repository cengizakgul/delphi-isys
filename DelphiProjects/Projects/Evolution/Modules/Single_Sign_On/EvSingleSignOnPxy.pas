unit EvSingleSignOnPxy;

interface

uses
  EvCommonInterfaces, EvMainboard, EvProxy, evRemoteMethods, EvContext,
  evTransportInterfaces, isBaseClasses;

implementation

type
  TevSingleSignOnProxy = class(TevProxyModule, IevSingleSignOn)
  protected
    function  GetSwipeClockOneTimeURL(const ACoNbr: integer): String;
    function  GetMRCOneTimeURL(const ACoNbr: integer): string;
    function  GetSwipeClockInfo: IisListOfValues;
  end;


function GetSingleSignOn: IevSingleSignOn;
begin
  Result := TevSingleSignOnProxy.Create;
end;


{ TevSingleSignOnProxy }

function TevSingleSignOnProxy.GetMRCOneTimeURL(const ACoNbr: integer): string;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SINGLESIGNON_GETMRCONETIMEURL);
  D.Params.AddValue('ACoNbr', ACoNbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevSingleSignOnProxy.GetSwipeClockOneTimeURL( const ACoNbr: integer): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SINGLESIGNON_GETSWIPECLOCKONETIMEURL);
  D.Params.AddValue('ACoNbr', ACoNbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevSingleSignOnProxy.GetSwipeClockInfo: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SINGLESIGNON_GETSWIPECLOCKINFO);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSingleSignOn, IevSingleSignOn, 'Single Sign On Module');

end.
