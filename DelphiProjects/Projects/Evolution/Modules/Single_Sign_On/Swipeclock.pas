unit SwipeClock;

interface

function GetSwipeclockSignOnURL(AdminName, AdminPassword: string; SiteNumber: integer; UserName: string): string;

implementation

uses
  EmployeeWebInterface, InvokeRegistry, wininet, SOAPHTTPClient, rio, sysutils, SimpleXML, ActiveX;

const errmsg : string = 'Please contact Swipe Clock Administrator (EmployeeWebInterface.EmployeeWebInterfaceSoap.GetOneTimeCredential API):';

procedure CheckResult(const S: string);
var
  sMessage, sDescription: String;
  aDoc: IXmlDocument;
  anElem: IXmlNode;
  sXML: String;
begin
  if copy(S, 1, 3) = 'OTC' then
    Exit;
  if S = '' then
    raise Exception.Create('GetOneTimeCredential returned empty result');

  sXML := StringReplace(S, '&lt;', '<', [rfReplaceAll]);
  sXML := StringReplace(sXML, '&gt;', '>', [rfReplaceAll]);
  aDoc := CreateXmlDocument;
  aDoc.LoadXML(sXML);
  anElem := aDoc.DocumentElement.SelectSingleNode('message');
  sMessage := anElem.Text;
  anElem := aDoc.DocumentElement.SelectSingleNode('description');
  sDescription := anElem.Text;

  raise Exception.CreateFmt('GetOneTimeCredential returned %s: %s', [sMessage, sDescription]);
end;

function GetSwipeclockSignOnURL(AdminName, AdminPassword: string; SiteNumber: integer; UserName: string): string;
var
  FAuthHeader: AuthHeader;
  FService2: EmployeeWebInterface.EmployeeWebInterfaceSoap;
begin
  CoInitialize(nil);
  try
    try
      FService2 := EmployeeWebInterface.GetEmployeeWebInterfaceSoap(false, 'https://www.payrollservers.us/scci/xml/EmployeeWebInterface.asmx');
    except
      on E: exception do
         raise Exception.Create(errmsg +'Soap initialization error:'+ e.Message);
    end;

    if not Assigned(FService2) then raise Exception.Create(errmsg +'EmployeeWebInterfaceSoap is not assigned ');

    try
      FAuthHeader := AuthHeader.Create;
      try
        FAuthHeader.userName := AdminName;
        FAuthHeader.password := AdminPassword;
        FAuthHeader.site := inttostr(SiteNumber);

        try
          (FService2 as ISOAPHeaders).send(FAuthHeader);
          Result := FService2.GetOneTimeCredential(UserName);
        except
          on E: Exception do
            raise Exception.Create(errmsg + E.Message);
        end;

        CheckResult(Result);
        Result := Format('https://www.swipeclock.com/sc/login.asp?login=%s&password=%s',[UserName, Result]);
      finally
        FService2 := nil; //do it before freeing FAuthHeader
        FreeAndNil(FAuthHeader);
      end;
    finally
      FService2 := nil;
    end;
  finally
    CoUnInitialize;
  end;
end;

initialization
  InvRegistry.RegisterInvokeOptions(TypeInfo(EmployeeWebInterfaceSoap), ioDocument);
end.
