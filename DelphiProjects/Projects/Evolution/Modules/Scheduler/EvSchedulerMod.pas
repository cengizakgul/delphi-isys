unit EvSchedulerMod;

interface

uses Classes, isBaseClasses, EvCommonInterfaces, EvMainboard, IsSMTPClient, EvStreamUtils, SysUtils,
     EvContext, isThreadManager, EvDataSet, DB, EvLegacy, EvConsts,  isErrorUtils,
     EvUtils, isBasicUtils, DateUtils, isLogFile, EvTypes, EvBasicUtils, isSchedule,
     IsTypes, EvDataAccessComponents, EvClasses;

implementation

type
  TevTaskScheduler = class(TisCollection, IevTaskScheduler)
  protected
    procedure DoOnDestruction; override;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    procedure RefreshTaskList;
  end;


  IevDomainSchedule = interface(IisSchedule)
  ['{923990DA-A106-4EA6-8E34-4D977AAA03DA}']
    function  DomainName: String;
    procedure Refresh;
  end;


  TevDomainSchedule = class(TisSchedule, IevDomainSchedule)
  private
    FDomainName: String;
    FNeedToRebuildSchedule: Boolean;
    FSysAccountCtx: IevContext;
    procedure BuildSchedule;
    procedure AddScheduleEvent(const AScheduleData, ATaskClass, ATaskDescription: String;
                               const ATaskParams: IisStream; const ATaskNbr: Integer;
                               const ALastRunDate: TDateTime);
  protected
    function  DomainName: String;
    procedure Refresh;
    procedure DoOnStart; override;
    function  BeforeCheckingTasks: Boolean; override;
    procedure DoOnStop; override;
    procedure RunTaskFor(const AEvent: IisScheduleEvent); override;
  public
    constructor Create(const ADomainInfo: IevDomainInfo); reintroduce;
  end;


function GetTaskScheduler: IevTaskScheduler;
begin
  Result := TevTaskScheduler.Create;
end;

{ TevTaskScheduler }

procedure TevTaskScheduler.DoOnDestruction;
begin
  SetActive(False);
  inherited;
end;

function TevTaskScheduler.GetActive: Boolean;
begin
  Result := ChildCount > 0;
end;

procedure TevTaskScheduler.RefreshTaskList;
var
  S: IevDomainSchedule;
  i: Integer;
begin
  mb_LogFile.AddContextEvent(etInformation, 'Task Scheduler', 'Event "RefreshTaskList" was received', '');

  for i := 0 to ChildCount - 1 do
  begin
    S := GetChild(i) as IevDomainSchedule;
    if AnsiSameText(S.DomainName, ctx_DomainInfo.DomainName) then
    begin
      S.Refresh;
      break;
    end;
  end;
end;

procedure TevTaskScheduler.SetActive(const AValue: Boolean);
var
  i: Integer;
  S: IevDomainSchedule;
begin
  if GetActive <> AValue then
    if AValue then
    begin
      // Initialization of domain holders
      // Each domain holder has its own schedule
      mb_GlobalSettings.DomainInfoList.Lock;
      try
        for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
        begin
          S := TevDomainSchedule.Create(mb_GlobalSettings.DomainInfoList[i]);
          AddChild(S as IisInterfacedObject);
          S.Active := True;
        end;
      finally
        mb_GlobalSettings.DomainInfoList.Unlock;
      end
    end

    else
      try
        RemoveAllChildren;
      except
        on E: Exception do
          Mainboard.LogFile.AddContextEvent(etError, 'Task Scheduler', E.Message, '');
      end;
end;


{ TevDomainSchedule }

procedure TevDomainSchedule.AddScheduleEvent(const AScheduleData,
  ATaskClass, ATaskDescription: String; const ATaskParams: IisStream;
  const ATaskNbr: Integer; const ALastRunDate: TDateTime);
var
  Event: IisScheduleEvent;
begin
  Event := OldFormatToScheduleEvent(AScheduleData);
  Event.TaskID := MethodNameToTaskType(ATaskClass);
  Event.Description := ATaskDescription;
  Event.TaskParams.AddValue('Params', ATaskParams);
  Event.TaskParams.AddValue('TaskNbr', ATaskNbr);
  Event.LastRunDate := ALastRunDate;
  AddEvent(Event);
end;

procedure TevDomainSchedule.BuildSchedule;
var
  qTasks: IevQuery;
  TaskParams: IisStream;
begin
  qTasks := TevQuery.Create('GenericSelectCurrent');
  qTasks.Macros.AddValue('TableName', 'SB_TASK');
  qTasks.Macros.AddValue('Columns', '*');
  qTasks.Execute;

  Lock;
  try
    Clear;
    // convert DB data into Schedule objects
    while not qTasks.Result.Eof do
    begin
      TaskParams := TisStream.Create;
      TBlobField(qTasks.Result.FieldByName('Task')).SaveToStream(TaskParams.RealStream);
      AddScheduleEvent(qTasks.Result['SCHEDULE'], qTasks.Result['Description'],
                       'SB_TASK_NBR=' + qTasks.Result.FieldByName('SB_TASK_NBR').AsString, TaskParams,
                       qTasks.Result['SB_TASK_NBR'], qTasks.Result.FieldByName('last_run').AsDateTime);
      qTasks.Result.Next;
    end;
  finally
    Unlock;
  end;

  mb_LogFile.AddContextEvent(etInformation, 'Task Scheduler', 'Schedule has been rebuilt', '');
end;

constructor TevDomainSchedule.Create(const ADomainInfo: IevDomainInfo);
begin
  FDomainName := ADomainInfo.DomainName;
  inherited Create('Task schedule (' + FDomainName + ')');
end;

procedure TevDomainSchedule.DoOnStop;
begin
  FSysAccountCtx := nil;
  Mainboard.ContextManager.DestroyThreadContext;
  inherited;
end;

procedure TevDomainSchedule.RunTaskFor(const AEvent: IisScheduleEvent);
var
  Task: IevTask;
  TaskNbr: Integer;
  dNow: TDateTime;
  ChangePacket: IevDataChangePacket;
  ChangeItem: IevDataChangeItem;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.SetThreadContext(FSysAccountCtx);
    try
      Task := TaskFromStream(AEvent.TaskID, IInterface(AEvent.TaskParams.Value['Params']) as IisStream);
      TaskNbr := AEvent.TaskParams.Value['TaskNbr'];

      // Add task and update SB_TASK table
      ctx_DBAccess.StartTransaction([dbtBureau]);
      try
        dNow := SysTime;
        ChangePacket := TevDataChangePacket.Create;
        ChangeItem := ChangePacket.NewUpdateFields('SB_TASK', TaskNbr);
        ChangeItem.Fields.AddValue('LAST_RUN', dNow);
        ctx_DBAccess.ApplyDataChangePacket(ChangePacket);

        mb_TaskQueue.AddTask(Task);

        ctx_DBAccess.CommitTransaction;
      except
        ctx_DBAccess.RollbackTransaction;
        raise;
      end;

      AEvent.LastRunDate := dNow;
    except
      on E: Exception do
        Mainboard.LogFile.AddContextEvent(etError, 'Task Scheduler', AEvent.Description + #13#13 + E.Message, '');
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;

 inherited;
end;

function TevDomainSchedule.BeforeCheckingTasks: Boolean;
begin
  try
    Result := inherited BeforeCheckingTasks and not IsMaintenanceMode and
              (ctx_DomainInfo.LicenseInfo.Code <> '');
    if Result then
    begin
      if not Assigned(FSysAccountCtx) then
        FSysAccountCtx := Mainboard.ContextManager.CreateContext(
          EncodeUserAtDomain(ctx_DomainInfo.SystemAccountInfo.UserName, ctx_DomainInfo.DomainName),
                            ctx_DomainInfo.SystemAccountInfo.Password);

      if FNeedToRebuildSchedule then
      begin
        Mainboard.ContextManager.StoreThreadContext;
        try
          Mainboard.ContextManager.SetThreadContext(FSysAccountCtx);
          BuildSchedule;
          FNeedToRebuildSchedule := False;
        finally
          Mainboard.ContextManager.RestoreThreadContext;
        end;
      end;
    end;

  except
    on E: Exception do
    begin
      Mainboard.LogFile.AddContextEvent(etError, 'Task Scheduler', E.Message, '');
      Result := False;
    end;
  end;
end;

procedure TevDomainSchedule.DoOnStart;
var
  D: IevDomainInfo;
begin
  inherited;
  D := mb_GlobalSettings.DomainInfoList.GetDomainInfo(FDomainName);
  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, D.DomainName), '');
  FNeedToRebuildSchedule := True;
end;

function TevDomainSchedule.DomainName: String;
begin
  Result := FDomainName;
end;

procedure TevDomainSchedule.Refresh;
begin
  FNeedToRebuildSchedule := True;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTaskScheduler, IevTaskScheduler, 'Scheduler');

end.
