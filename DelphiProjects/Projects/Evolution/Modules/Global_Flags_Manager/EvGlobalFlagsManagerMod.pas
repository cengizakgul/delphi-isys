unit EvGlobalFlagsManagerMod;

interface

uses SysUtils, SyncObjs, isBaseClasses, isBasicUtils, EvCommonInterfaces, EvClasses, EvMainboard, EvContext;

implementation

type
  IevDomainFlagHolder = interface(IisCollection)
  ['{1B061BAC-1817-4E72-8B3C-C6B10ED989B9}']
    function GetFlagHolder(const AType: String; const ATag: String): IevGlobalFlagInfo; overload;
    function GetFlagHolder(const AFlagName: String): IevGlobalFlagInfo; overload;
    function GetFlagHolder(const AIndex: Integer): IevGlobalFlagInfo; overload;
  end;


  TevGlobalFlagsManager = class(TisInterfacedObject, IevGlobalFlagsManager)
  private
    FDomains: IisStringList;
    function  GetDomainFlagHolder(const ADomain: String): IevDomainFlagHolder;
    procedure NotifyChanges(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
  protected
    procedure DoOnConstruction; override;
    procedure EnableExclusiveMode;
    procedure DisableExclusiveMode;
    function  SetFlag(const AFlagName: String): Boolean; overload;
    function  ReleaseFlag(const AFlagName: String): Boolean; overload;
    function  TryLock(const AType: String; const ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean; overload;
    function  TryLock(const AFlagName: String): Boolean; overload;
    function  TryUnlock(const AType: String; const ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean; overload;
    function  TryUnlock(const AFlagName: String): Boolean; overload;
    procedure ForcedUnlock(const AType: String; const ATag: String); overload;
    procedure ForcedUnlock(const AFlagName: String); overload;
    function  GetLockedFlag(const AType: String; const ATag: String): IevGlobalFlagInfo; overload;
    function  GetLockedFlag(const AFlagName: String): IevGlobalFlagInfo; overload;
    function  GetLockedFlagList(const AType: String = ''): IisList;
    function  GetSetFlagList(const AType: String = ''): IisList;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
  end;


  TevDomainFlagHolder = class(TisCollection, IevDomainFlagHolder)
  protected
    procedure DoOnConstruction; override;
    function  GetFlagHolder(const AType: String; const ATag: String): IevGlobalFlagInfo; overload;
    function  GetFlagHolder(const AFlagName: String): IevGlobalFlagInfo; overload;
    function  GetFlagHolder(const AIndex: Integer): IevGlobalFlagInfo; overload;
  end;


function GetGlobalFlagsManager: IevGlobalFlagsManager;
begin
  Result := TevGlobalFlagsManager.Create;
end;



{ TevGlobalFlagsManager }

procedure TevGlobalFlagsManager.DoOnConstruction;
begin
  inherited;
  FDomains := TisStringList.CreateAsIndex;
end;

procedure TevGlobalFlagsManager.ForcedUnlock(const AType, ATag: String);
var
  DomainFlagHolder: IevDomainFlagHolder;
  FlagInfo: IevGlobalFlagInfo;
  bNotify: Boolean;
begin
  bNotify := False;
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    FlagInfo := DomainFlagHolder.GetFlagHolder(AType, ATag);
    if Assigned(FlagInfo) then
    begin
      CheckCondition(FlagInfo.LockTime <> 0, 'Cannot lock used flag');
      (FlagInfo as IisInterfacedObject).Owner := nil;
       bNotify := True;
    end;
  finally
    DomainFlagHolder.Unlock;
  end;

  if bNotify then
    NotifyChanges(FlagInfo, 'U');
end;

function TevGlobalFlagsManager.GetLockedFlag(const AType, ATag: String): IevGlobalFlagInfo;
var
  DomainFlagHolder: IevDomainFlagHolder;
begin
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    Result := DomainFlagHolder.GetFlagHolder(AType, ATag);
    if Assigned(Result) and (Result.LockTime = 0) then
      Result := nil;
  finally
    DomainFlagHolder.Unlock;
  end;
end;

function TevGlobalFlagsManager.GetLockedFlagList(const AType: String): IisList;
var
  DomainFlagHolder: IevDomainFlagHolder;
  FlagInfo: IevGlobalFlagInfo;
  i: Integer;
begin
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    Result := TisList.Create;
    Result.Capacity := DomainFlagHolder.ChildCount;
    for i := 0 to DomainFlagHolder.ChildCount - 1 do
    begin
      FlagInfo := DomainFlagHolder.GetFlagHolder(i);
      if FlagInfo.LockTime <> 0 then
        if (AType = '') or AnsiSameText(FlagInfo.FlagType, AType) then
          Result.Add(FlagInfo);
    end;
  finally
    DomainFlagHolder.Unlock;
  end;
end;

function TevGlobalFlagsManager.GetDomainFlagHolder(const ADomain: String): IevDomainFlagHolder;
var
  i: Integer;
begin
  i := FDomains.IndexOf(ADomain);
  if i <> -1 then
    Result := FDomains.Objects[i] as IevDomainFlagHolder
  else
    Result := nil;

  CheckCondition(Assigned(Result), 'Domain "' + ADomain + '" is not registered');
end;

procedure TevGlobalFlagsManager.NotifyChanges(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
  if (AOperation = 'U') and Assigned(Mainboard.TaskQueue) then
    mb_TaskQueue.Pulse; // to inform task queue about unlocked flag

  if Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.NotifyGlobalFlagChange(AFlagInfo, AOperation);
end;

function TevGlobalFlagsManager.TryLock(const AType, ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean;
var
  DomainFlagHolder: IevDomainFlagHolder;
  bNotify: Boolean;
begin
  bNotify := False;
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    AFlagInfo := DomainFlagHolder.GetFlagHolder(AType, ATag);
    if Assigned(AFlagInfo) and (AFlagInfo.ContextID <> Context.GetRemoteID) then
    begin
      Result := False;
      CheckCondition(AFlagInfo.LockTime <> 0, 'Cannot lock used flag');
    end
    else
    begin
      if not Assigned(AFlagInfo) then
      begin
        AFlagInfo := TevGlobalFlagInfo.CreateLocked(AType, ATag);
        DomainFlagHolder.AddChild(AFlagInfo as IisInterfacedObject);
        AFlagInfo.IncUsage;
        bNotify := True;
      end
      else
        AFlagInfo.IncUsage;

      Result := True;
    end;
  finally
    DomainFlagHolder.Unlock;
  end;

  if bNotify then
    NotifyChanges(AFlagInfo, 'L');
end;

function TevGlobalFlagsManager.TryUnlock(const AType, ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean;
var
  DomainFlagHolder: IevDomainFlagHolder;
  bNotify: Boolean;
begin
  bNotify := False;
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    AFlagInfo := DomainFlagHolder.GetFlagHolder(AType, ATag);
    if Assigned(AFlagInfo) then
    begin
      CheckCondition(AFlagInfo.LockTime <> 0, 'Cannot unlock used flag');
      if AFlagInfo.ContextID = Context.GetRemoteID then
      begin
        if AFlagInfo.DecUsage = 0 then
        begin
          (AFlagInfo as IisInterfacedObject).Owner := nil;
          bNotify := True;
        end;
        Result := True;
      end
      else
        Result := False;
    end
    else
    begin
      AFlagInfo := nil;
      Result := True;
    end;
  finally
    DomainFlagHolder.Unlock;
  end;

  if bNotify then
    NotifyChanges(AFlagInfo, 'U');
end;

function TevGlobalFlagsManager.TryLock(const AFlagName: String): Boolean;
var
  FlagInfo: IevGlobalFlagInfo;
  sType, sTag, s, sLocked: String;
begin
  Result := True;
  s := AFlagName;
  sLocked := '';
  while s <> '' do
  begin
    sTag := Trim(GetNextStrValue(s, ','));
    sType := GetNextStrValue(sTag, '.');

    if not TryLock(sType, sTag, FlagInfo) then
    begin
      Result := False;
      if sLocked <> '' then
        TryUnlock(sLocked);
      break;
    end
    else
      AddStrValue(sLocked, FlagInfo.FlagName, ',');
  end;
end;

function TevGlobalFlagsManager.TryUnlock(const AFlagName: String): Boolean;
var
  FlagInfo: IevGlobalFlagInfo;
  sType, sTag, s: String;
begin
  Result := True;
  s := AFlagName;
  while s <> '' do
  begin
    sTag := GetPrevStrValue(s, ',');
    sType := GetNextStrValue(sTag, '.');
    Result := Result and TryUnlock(sType, sTag, FlagInfo);
  end;
end;

function TevGlobalFlagsManager.GetLockedFlag(const AFlagName: String): IevGlobalFlagInfo;
var
  sType, sTag: String;
begin
  sTag := AFlagName;
  sType := GetNextStrValue(sTag, '.');
  Result := GetLockedFlag(sType, sTag);
end;

function TevGlobalFlagsManager.ReleaseFlag(const AFlagName: String): Boolean;
var
  DomainFlagHolder: IevDomainFlagHolder;
  FlagInfo: IevGlobalFlagInfo;
  bNotify: Boolean;
begin
  bNotify := False;
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    FlagInfo := DomainFlagHolder.GetFlagHolder(AFlagName);
    if Assigned(FlagInfo) then
    begin
      CheckCondition(FlagInfo.LockTime = 0, 'Cannot release locked flag');
      if AnsiSameText(FlagInfo.Domain, Context.UserAccount.Domain) then
      begin
        if FlagInfo.DecUsage = 0 then
          (FlagInfo as IisInterfacedObject).Owner := nil;
        bNotify := True;
        Result := True;
      end
      else
        Result := False;
    end
    else
      Result := True;
  finally
    DomainFlagHolder.Unlock;
  end;

  if bNotify then
    NotifyChanges(FlagInfo, 'R');
end;

function TevGlobalFlagsManager.SetFlag(const AFlagName: String): Boolean;
var
  DomainFlagHolder: IevDomainFlagHolder;
  FlagInfo: IevGlobalFlagInfo;
  sType, sTag: String;
begin
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    FlagInfo := DomainFlagHolder.GetFlagHolder(AFlagName);
    if Assigned(FlagInfo) then
    begin
      CheckCondition(FlagInfo.LockTime = 0, 'Cannot set locked flag');
      Result := AnsiSameText(FlagInfo.Domain, Context.UserAccount.Domain);
      if Result then
        FlagInfo.IncUsage;
    end

    else
    begin
      sTag := AFlagName;
      sType := GetNextStrValue(sTag, '.');
      FlagInfo := TevGlobalFlagInfo.Create(sType, sTag);
      FlagInfo.IncUsage;
      DomainFlagHolder.AddChild(FlagInfo as IisInterfacedObject);
      Result := True;
    end;
  finally
    DomainFlagHolder.Unlock;
  end;

  if Result then
    NotifyChanges(FlagInfo, 'S');
end;

function TevGlobalFlagsManager.GetSetFlagList(const AType: String): IisList;
var
  DomainFlagHolder: IevDomainFlagHolder;
  FlagInfo: IevGlobalFlagInfo;
  i: Integer;
begin
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);

  DomainFlagHolder.Lock;
  try
    Result := TisList.Create;
    Result.Capacity := DomainFlagHolder.ChildCount;
    for i := 0 to DomainFlagHolder.ChildCount - 1 do
    begin
      FlagInfo := DomainFlagHolder.GetFlagHolder(i);
      if FlagInfo.LockTime = 0 then
        if (AType = '') or AnsiSameText(FlagInfo.FlagType, AType) then
          Result.Add(FlagInfo);
    end;
  finally
    DomainFlagHolder.Unlock;
  end;
end;

procedure TevGlobalFlagsManager.DisableExclusiveMode;
var
  DomainFlagHolder: IevDomainFlagHolder;
begin
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);
  DomainFlagHolder.UnLock;
end;

procedure TevGlobalFlagsManager.EnableExclusiveMode;
var
  DomainFlagHolder: IevDomainFlagHolder;
begin
  DomainFlagHolder := GetDomainFlagHolder(Context.UserAccount.Domain);
  DomainFlagHolder.Lock;
end;

procedure TevGlobalFlagsManager.ForcedUnlock(const AFlagName: String);
var
  sType, sTag, s: String;
begin
  s := AFlagName;
  while s <> '' do
  begin
    sTag := GetNextStrValue(s, ',');
    sType := GetNextStrValue(sTag, '.');
    ForcedUnlock(sType, sTag);
  end;
end;

function TevGlobalFlagsManager.GetActive: Boolean;
begin
  Result := FDomains.Count > 0;
end;

procedure TevGlobalFlagsManager.SetActive(const AValue: Boolean);
var
  i: Integer;
  FlagHolder: IevDomainFlagHolder;
begin
  if GetActive <> AValue then
  begin
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      FDomains.Clear;
      if AValue then
        for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
        begin
          FlagHolder := TevDomainFlagHolder.Create;
          FDomains.AddObject(mb_GlobalSettings.DomainInfoList[i].DomainName, FlagHolder);
        end;
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
  end;
end;

{ TevDomainFlagHolder }

function TevDomainFlagHolder.GetFlagHolder(const AType, ATag: String): IevGlobalFlagInfo;
begin
  Result := GetFlagHolder(AType + '.' + ATag);
end;

procedure TevDomainFlagHolder.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

function TevDomainFlagHolder.GetFlagHolder(const AIndex: Integer): IevGlobalFlagInfo;
begin
  Result := GetChild(AIndex) as IevGlobalFlagInfo;
end;

function TevDomainFlagHolder.GetFlagHolder(const AFlagName: String): IevGlobalFlagInfo;
begin
  Result := FindChildByName(AFlagName) as IevGlobalFlagInfo;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalFlagsManager, IevGlobalFlagsManager, 'Global Flags Manager');

end.
