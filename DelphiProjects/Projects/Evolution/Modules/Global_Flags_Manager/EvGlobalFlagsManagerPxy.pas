// Proxy Module "Global Flags Manager"
unit EvGlobalFlagsManagerPxy;

interface

implementation

uses SysUtils, Windows, isBaseClasses, EvCommonInterfaces, EvMainboard, EvTransportDatagrams,
     evRemoteMethods, EvStreamUtils, evProxy, EvTransportInterfaces;

type
  TevGlobalFlagsManagerProxy = class(TevProxyModule, IevGlobalFlagsManager)
  protected
    procedure EnableExclusiveMode;
    procedure DisableExclusiveMode;
    function  SetFlag(const AFlagName: String): Boolean; overload;
    function  ReleaseFlag(const AFlagName: String): Boolean; overload;
    function  TryLock(const AType: String; const ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean; overload;
    function  TryLock(const AFlagName: String): Boolean; overload;
    function  TryUnlock(const AType: String; const ATag: String; out AFlagInfo: IevGlobalFlagInfo): Boolean; overload;
    function  TryUnlock(const AFlagName: String): Boolean; overload;
    procedure ForcedUnlock(const AType: String; const ATag: String); overload;
    procedure ForcedUnlock(const AFlagName: String); overload;
    function  GetLockedFlag(const AType: String; const ATag: String): IevGlobalFlagInfo; overload;
    function  GetLockedFlag(const AFlagName: String): IevGlobalFlagInfo; overload;
    function  GetLockedFlagList(const AType: String = ''): IisList;
    function  GetSetFlagList(const AType: String = ''): IisList;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
  end;


function GetGlobalFlagsManagerProxy: IevGlobalFlagsManager;
begin
  Result := TevGlobalFlagsManagerProxy.Create;
end;


{ TevGlobalFlagsManagerProxy }

procedure TevGlobalFlagsManagerProxy.DisableExclusiveMode;
begin
  NotRemotableMethod;
end;

procedure TevGlobalFlagsManagerProxy.EnableExclusiveMode;
begin
  NotRemotableMethod;
end;

procedure TevGlobalFlagsManagerProxy.ForcedUnlock(const AType,
  ATag: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_FORCEDUNLOCK);
  D.Params.AddValue('AType', AType);
  D.Params.AddValue('ATag', ATag);
  D := SendRequest(D, False);
end;

function TevGlobalFlagsManagerProxy.GetLockedFlag(const AType,
  ATag: String): IevGlobalFlagInfo;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAG);
  D.Params.AddValue('AType', AType);
  D.Params.AddValue('ATag', ATag);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevGlobalFlagInfo;
end;

procedure TevGlobalFlagsManagerProxy.ForcedUnlock(const AFlagName: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_FORCEDUNLOCK2);
  D.Params.AddValue('AFlagName', AFlagName);
  D := SendRequest(D, False);
end;

function TevGlobalFlagsManagerProxy.GetLockedFlag(
  const AFlagName: String): IevGlobalFlagInfo;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAG2);
  D.Params.AddValue('AFlagName', AFlagName);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevGlobalFlagInfo;
end;

function TevGlobalFlagsManagerProxy.GetLockedFlagList(
  const AType: String): IisList;
var
  D: IevDatagram;
  S: IevDualStream;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAGLIST);
  D.Params.AddValue('AType', AType);
  D := SendRequest(D, False);
  S := IInterface(D.Params.Value['Result']) as IevDualStream;
  Result := TisList.Create;
  Result.LoadItemsFromStream(S);
end;

function TevGlobalFlagsManagerProxy.GetSetFlagList(
  const AType: String): IisList;
var
  D: IevDatagram;
  S: IevDualStream;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_GETSETFLAGLIST);
  D.Params.AddValue('AType', AType);
  D := SendRequest(D, False);
  S := IInterface(D.Params.Value['Result']) as IevDualStream;
  Result := TisList.Create;
  Result.LoadItemsFromStream(S);
end;

function TevGlobalFlagsManagerProxy.ReleaseFlag(
  const AFlagName: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_RELEASEFLAG);
  D.Params.AddValue('AFlagName', AFlagName);
  D := SendRequest(D, False);
  Result := D.Params.Value['Result'];
end;

function TevGlobalFlagsManagerProxy.SetFlag(
  const AFlagName: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_SETFLAG);
  D.Params.AddValue('AFlagName', AFlagName);
  D := SendRequest(D, False);
  Result := D.Params.Value['Result'];
end;

function TevGlobalFlagsManagerProxy.TryLock(
  const AFlagName: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_TRYLOCK2);
  D.Params.AddValue('AFlagName', AFlagName);
  D := SendRequest(D, False);
  Result := D.Params.Value['Result'];
end;

function TevGlobalFlagsManagerProxy.TryLock(const AType, ATag: String;
  out AFlagInfo: IevGlobalFlagInfo): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_TRYLOCK);
  D.Params.AddValue('AType', AType);
  D.Params.AddValue('ATag', ATag);
  D.Params.AddValue('AFlagInfo', AFlagInfo);
  D := SendRequest(D, False);
  Result := D.Params.Value['Result'];
  AFlagInfo := IInterface(D.Params.Value['AFlagInfo']) as IevGlobalFlagInfo;
end;

function TevGlobalFlagsManagerProxy.TryUnlock(
  const AFlagName: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_TRYUNLOCK2);
  D.Params.AddValue('AFlagName', AFlagName);
  D := SendRequest(D, False);
  Result := D.Params.Value['Result'];
end;

function TevGlobalFlagsManagerProxy.TryUnlock(const AType, ATag: String;
  out AFlagInfo: IevGlobalFlagInfo): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_GLOBALFLAGMANAGER_TRYUNLOCK);
  D.Params.AddValue('AType', AType);
  D.Params.AddValue('ATag', ATag);
  D.Params.AddValue('AFlagInfo', AFlagInfo);
  D := SendRequest(D, False);
  Result := D.Params.Value['Result'];
  AFlagInfo := IInterface(D.Params.Value['AFlagInfo']) as IevGlobalFlagInfo;
end;

function TevGlobalFlagsManagerProxy.GetActive: Boolean;
begin
  Result := True;
  NotRemotableMethod;
end;

procedure TevGlobalFlagsManagerProxy.SetActive(const AValue: Boolean);
begin
  NotRemotableMethod;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalFlagsManagerProxy, IevGlobalFlagsManager, 'Global Flags Manager');

end.