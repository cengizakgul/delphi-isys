unit EvVersionUpdateRemotePxy;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, isBasicUtils,
     EvStreamUtils, evRemoteMethods, EvTransportInterfaces, EvProxy;

implementation

type
  TevVersionUpdateRemote = class(TevProxyModule, IevVersionUpdateRemote)
  protected
    function  GetVersionInfo: IisListOfValues;
    function  Check(const aAppFileInfo: IisParamsCollection; out aUpdateInfo: IisParamsCollection): Boolean;
    function  GetUpdatePack(const aAppFileInfo: IisParamsCollection): IisParamsCollection;
  end;

function GetVersionUpdateRemote: IevVersionUpdateRemote;
begin
  Result := TevVersionUpdateRemote.Create;
end;

{ TevVersionUpdateRemote }

function TevVersionUpdateRemote.Check(const aAppFileInfo: IisParamsCollection;
  out aUpdateInfo: IisParamsCollection): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VERSION_UPDATE_REMOTE_CHECK);
  D.Params.AddValue('AppFileInfo', aAppFileInfo);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
  aUpdateInfo := IInterface(D.Params.Value['UpdateInfo']) as IisParamsCollection;
end;

function TevVersionUpdateRemote.GetUpdatePack(const aAppFileInfo: IisParamsCollection): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VERSION_UPDATE_REMOTE_GETUPDATEPACK);
  D.Params.AddValue('AppFileInfo', aAppFileInfo);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevVersionUpdateRemote.GetVersionInfo: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VERSION_UPDATE_REMOTE_GETVERSIONINFO);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVersionUpdateRemote, IevVersionUpdateRemote, 'Version Update Remote');

end.



