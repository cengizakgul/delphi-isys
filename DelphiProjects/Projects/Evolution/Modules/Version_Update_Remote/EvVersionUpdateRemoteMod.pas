unit EvVersionUpdateRemoteMod;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, isBasicUtils,
     ISZippingRoutines, isSettings, evConsts, EvStreamUtils, SEncryptionRoutines;


implementation

{$R xDelta.res}

type
  TevVersionUpdateRemote = class(TisCollection, IevVersionUpdateRemote)
  private
    FCurrentVersionLocation: String;
    FUpdatableApps: IisStringList;
    FDiffsInfo: IisListOfValues;
    procedure InitVersionArchive;
    function  GetCurVerFileInfo(const AAppID: String): IisParamsCollection;
    function  NormalizeVersion(AVersion: String): String;
    function  PrepareUpdatePack(const aAppFileInfo: IisParamsCollection; const aEstimation: Boolean;
                                out AEstimatedTotalSize: Integer): IisParamsCollection;
  protected
    procedure DoOnConstruction; override;
    function  GetVersionInfo: IisListOfValues;
    function  Check(const aAppFileInfo: IisParamsCollection; out aUpdateInfo: IisParamsCollection): Boolean;
    function  GetUpdatePack(const aAppFileInfo: IisParamsCollection): IisParamsCollection;
  end;


function GetVersionUpdateRemote: IevVersionUpdateRemote;
begin
  Result := TevVersionUpdateRemote.Create;
end;


{ TevVersionUpdateRemote }

function TevVersionUpdateRemote.Check(const aAppFileInfo: IisParamsCollection;
  out aUpdateInfo: IisParamsCollection): Boolean;
var
  EstimatedTotalSize: Integer;
  FilesPar: IisListOfValues;
begin
  aUpdateInfo := PrepareUpdatePack(aAppFileInfo, True, EstimatedTotalSize);

  FilesPar :=  aUpdateInfo.ParamsByName('Files');
  Result := FilesPar.Count = 0;

  aUpdateInfo.ParamsByName('Description').AddValue('EstimatedSize', EstimatedTotalSize);
end;

procedure TevVersionUpdateRemote.DoOnConstruction;
begin
  inherited;
  InitLock;
  FCurrentVersionLocation := AppDir + 'Autoupdate\';
  ClearDir(FCurrentVersionLocation, True);
  FUpdatableApps := TisStringList.Create;
end;

function TevVersionUpdateRemote.GetCurVerFileInfo(const AAppID: String): IisParamsCollection;
var
  DeploymentFile: IisSettings;
  L: IisStringList;
  DeployApp: IisStringListRO;
  i, j: Integer;
  FI: IisListOfValues;
  FInf: TisFileInfo;
  sArchive: String;
begin
  Lock;
  try
    i := FUpdatableApps.IndexOf(AAppID);
    if i = -1 then
    begin
      Result := nil;
      DeploymentFile := TisSettingsINI.Create(FCurrentVersionLocation + 'Deployment.cfg');
      DeployApp := DeploymentFile.GetChildNodes('');
      for i := 0 to DeployApp.Count - 1 do
        if DeploymentFile.AsString[DeployApp[i] + '\AppID'] = AAppID then
        begin
          // Add version content
          L := TisStringList.Create;
          L.CommaText := DeploymentFile.AsString[DeployApp[i] + '\Content'];
          Result := TisParamsCollection.Create;
          for j := 0 to L.Count - 1 do
            if FileExists(FCurrentVersionLocation + L[j]) then
            begin
              FInf := GetFileInfo(FCurrentVersionLocation + L[j]);
              FI := Result.AddParams(L[j]);
              FI.AddValue('Location', FInf.FileName);
              FI.AddValue('Version', GetFileVersion(FInf.FileName));
              FI.AddValue('Size', FInf.Size);
            end;

          // add archive file info (this file contain version content in highly compressed form)
          sArchive := DeploymentFile.AsString[DeployApp[i] + '\Archive'];
          if (sArchive <> '') and FileExists(FCurrentVersionLocation + sArchive)  then
          begin
            FInf := GetFileInfo(FCurrentVersionLocation + sArchive);
            FI := Result.AddParams('Archive');
            FI.AddValue('Location', FInf.FileName);
            FI.AddValue('Version', '');
            FI.AddValue('Size', FInf.Size);
          end;

          FUpdatableApps.AddObject(AAppID, Result);
        end;
    end
    else
      Result := FUpdatableApps.Objects[i] as IisParamsCollection;
  finally
    Unlock
  end;

  CheckCondition(Assigned(Result), 'Version Update information is not available for application ' + AAppID);
end;

function TevVersionUpdateRemote.GetUpdatePack(const aAppFileInfo: IisParamsCollection): IisParamsCollection;
var
  F: IEvDualStream;
  sHash: String;
  EstimatedTotalSize: Integer;
begin
  Result := PrepareUpdatePack(aAppFileInfo, False, EstimatedTotalSize);

  // if hash of client's update pack the same then we don't need to send it
  if aAppFileInfo.ParamsByName('ExistingPack') <> nil then
  begin
    F := TevDualStreamHolder.Create;
    Result.WriteToStream(F);
    sHash := CalcMD5(F.RealStream);
    if sHash = aAppFileInfo.ParamsByName('ExistingPack').Value['Hash'] then
      Result.DeleteParams('Files');
    F := nil;
  end;
end;

function TevVersionUpdateRemote.GetVersionInfo: IisListOfValues;
begin
  Result := TisListOfValues.Create;
  Result.AddValue('Version', AppVersion);
  Result.AddValue('VersionName', EvVerName);
end;

procedure TevVersionUpdateRemote.InitVersionArchive;
var
  sArchiveLocation: String;
  i: Integer;
  DiffsFolder: IisStringList;
begin
  Lock;
  try
    if not Assigned(FDiffsInfo) then
    begin
      sArchiveLocation := AppDir + AppVersion + '.zip';
      if not FileExists(sArchiveLocation) then
      begin
        sArchiveLocation := AppDir + NormalizeVersion(AppVersion) + '.zip';
        CheckCondition(FileExists(sArchiveLocation), 'Version archive is not found');
      end;

      ForceDirectories(FCurrentVersionLocation);
      ExtractFromFile(sArchiveLocation, FCurrentVersionLocation, '*');

      DiffsFolder := GetSubDirs(FCurrentVersionLocation);
      FDiffsInfo := TisListOfValues.Create;
      for i := 0 to DiffsFolder.Count - 1 do
        FDiffsInfo.AddValue(NormalizeVersion(ExtractFileName(DiffsFolder[i])), DiffsFolder[i]);
      FDiffsInfo.Sort;
    end;
  finally
    Unlock;
  end;
end;

function TevVersionUpdateRemote.NormalizeVersion(AVersion: String): String;
var
  n: Integer;
begin
  // normalazing version number to 00.00.00.00
  Result := '';
  while AVersion <> '' do
  begin
    n := StrToIntDef(GetNextStrValue(AVersion, '.'), 0);
    AddStrValue(Result, FormatFloat('00', n), '.');
  end;
end;

function TevVersionUpdateRemote.PrepareUpdatePack(const aAppFileInfo: IisParamsCollection;
  const aEstimation: Boolean; out AEstimatedTotalSize: Integer): IisParamsCollection;
const
  EstimatedCompressionRation = 2.5; // everage ZIP ratio for binaries
var
  i, j, iSize: Integer;
  AppFiles, AppFile, ArchFile, Par, FilesPar, DiffData, UpdFile, Archive: IisListOfValues;
  FileData: IevDualStream;
  sNormalizedAppVer, sAppFileVer, sFileName, s: String;
  KnownFiles: IisStringList;
  CurVerFileInfo: IisParamsCollection;
  bAddPatchUtility: Boolean;
  HR: HRSRC;
  SR: Cardinal;
  PR: Pointer;
  F: IevDualStream;

  procedure AddCopyOper;
  var
    s, comp: String;
  begin
    UpdFile.AddValue('Operation', 'copy');

    // what is there is compressed copy of the file
    s := ArchFile.Value['Location'];
    if FileExists(s + '.7z') then
    begin
      comp := '7z';
      s := s + '.7z';
    end
    else if FileExists(s + '.zip') then
    begin
      comp := 'zip';
      s := s + '.zip';
    end
    else
      comp := '';

    if not aEstimation then
    begin
      FileData := TevDualStreamHolder.CreateFromFile(s);
      UpdFile.AddValue('Data', FileData);
    end;

    if comp = '' then
      iSize := ArchFile.Value['Size']
    else
    begin
      UpdFile.AddValue('Compression', comp);
      iSize := GetFileInfo(s).Size;
    end;
  end;

begin
  InitVersionArchive;

  CurVerFileInfo := GetCurVerFileInfo(aAppFileInfo.ParamsByName('Description').Value['AppID']);
  sNormalizedAppVer := NormalizeVersion(aAppFileInfo.ParamsByName('Description').Value['Version']);

  KnownFiles := TisStringList.CreateAsIndex;
  KnownFiles.Add('.EXE');
  KnownFiles.Add('.DLL');
  KnownFiles.Add('.BPL');

  AppFiles := aAppFileInfo.ParamsByName('Files');

  Result := TisParamsCollection.Create;
  Par := Result.AddParams('Description');
  Par.AddValue('Version', AppVersion);
  Par.AddValue('VersionName', EvVerName);
  s := mb_AppConfiguration.AsString['General\AlternativeVerUpdSource'];
  if s <> '' then
    Par.AddValue('AlternativeSource', NormalizePath(NormalizePath(s) + AppVersion)); //  example: http://www.myserver.com/Evolution/10.0.1.25/
  FilesPar := Result.AddParams('Files');

  // files to delete
  for i := 0 to AppFiles.Count - 1 do
    if (CurVerFileInfo.ParamsByName(AppFiles[i].Name) = nil) and
       (KnownFiles.IndexOf(ExtractFileExt(AppFiles[i].Name)) <> -1) then
    begin
      Par := TisListOfValues.Create;
      FilesPar.AddValue(AppFiles[i].Name, Par);
      Par.AddValue('Operation', 'delete');
    end;

  // files to patch
  AEstimatedTotalSize := 0;
  bAddPatchUtility := False;
  for i := 0 to CurVerFileInfo.Count - 1 do
  begin
    if AnsiSameText(CurVerFileInfo.ParamName(i), 'Archive') then  // Skip this item, it's special one
      Continue;

    ArchFile := CurVerFileInfo[i];
    sFileName := CurVerFileInfo.ParamName(i);
    if AppFiles.ValueExists(sFileName) then
      AppFile := IInterface(AppFiles.Value[sFileName]) as IisListOfValues
    else
      AppFile := nil;

    if not(Assigned(AppFile) and (AppFile.Value['Version'] = ArchFile.Value['Version'])) then
    begin
      UpdFile := TisListOfValues.Create;
      FilesPar.AddValue(sFileName, UpdFile);

      if Assigned(AppFile) then
        sAppFileVer := NormalizeVersion(AppFile.Value['Version'])
      else
        sAppFileVer := '';

      if (sAppFileVer = '') or not FDiffsInfo.ValueExists(sAppFileVer) then
        AddCopyOper

      else
      begin
        iSize := 0;
        DiffData := TisListOfValues.Create;
        for j := 0 to FDiffsInfo.Count - 1 do
//          if sNormalizedAppVer <= FDiffsInfo[j].Name then  // for incremental update
          if sNormalizedAppVer = FDiffsInfo[j].Name then     // for differential update
          begin
            s := NormalizePath(FDiffsInfo[j].Value) + sFileName;
            if FileExists(s) then
            begin
              FileData := TevDualStreamHolder.CreateFromFile(s);
              Inc(iSize, FileData.Size);
              if aEstimation then
                FileData := nil;
              GetPrevStrValue(s, '\');  // remove file name
              s := ExtractFileName(s);
              DiffData.AddValue(s, FileData);
            end;
          end;

        // if total diffs size is more than 2/3 of file then it's better perform full copy rather than patch it
        if (iSize = 0) or (iSize >  ArchFile.Value['Size'] div 3) then
          AddCopyOper
        else
        begin
          UpdFile.AddValue('Operation', 'patch');
          UpdFile.AddValue('Diffs', DiffData);
          bAddPatchUtility := True;
        end;
      end;

      UpdFile.AddValue('Size', iSize);
      if UpdFile.Value['Operation'] = 'copy' then
        iSize := Round(iSize/EstimatedCompressionRation);

      Inc(AEstimatedTotalSize, iSize);
    end;
  end;

  // Check if total size is bigger than archive file
  Archive := CurVerFileInfo.ParamsByName('Archive');
  if Assigned(Archive) and (AEstimatedTotalSize > Archive.Value['Size']) then
  begin
    for i := 0 to FilesPar.Count - 1 do
    begin
      UpdFile := IInterface(FilesPar[i].Value) as IisListOfValues;
      if not AnsiSameText(FilesPar[i].Name, '7-Zip32.dll') and
        ((UpdFile.Value['Operation'] = 'copy') or (UpdFile.Value['Operation'] = 'patch')) then
      begin
        UpdFile.Value['Operation'] := 'extract';
        UpdFile.Value['Size'] := 0;
      end;
    end;

    Par := Result.AddParams('Archive');
    Par.AddValue('Name', ExtractFileName(Archive.Value['Location']));
    AEstimatedTotalSize := Archive.Value['Size'];
    Par.AddValue('Size', AEstimatedTotalSize);
    if not aEstimation then
    begin
      FileData := TevDualStreamHolder.CreateFromFile(Archive.Value['Location']);
      Par.AddValue('File', FileData);
    end;
  end

  else
  begin
    // Add patch utility
    if bAddPatchUtility then
    begin
      HR := FindResource(HINSTANCE, 'xdelta', 'EXE');
      if HR <> 0 then
      begin
        SR := SizeofResource(HINSTANCE, HR);
        if SR > 0 then
        begin
          Par := Result.AddParams('PatchUtil');
          Par.AddValue('Name', 'xDelta.exe');
          Par.AddValue('Params', '-d -f -s %source% %delta% %target%');
          if not aEstimation then
          begin
            PR := Pointer(LoadResource(HINSTANCE, HR));
            F := TEvDualStreamHolder.Create(SR);
            F.WriteBuffer(PR^, SR);
            Par.AddValue('File', F);
          end;
          Inc(AEstimatedTotalSize, SR);
        end;
      end;
    end;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVersionUpdateRemote, IevVersionUpdateRemote, 'Version Update Remote');


end.
