// Proxy Module "Misc Calculations and Routines"
unit pxy_Misc_Calc;

interface

implementation

uses isBaseClasses, EvCommonInterfaces, evRemoteMethods, EvTransportDatagrams, EvStreamUtils,
     SReportSettings, EvMainboard, evProxy, EvTransportInterfaces, Controls;

type
  TevRemoteMiscRoutinesProxy = class(TevProxyModule, IevRemoteMiscRoutines)
  protected

    function  PreProcessForQuarter(const Cl_NBR, CO_NBR: Integer; const ForceProcessing, Consolidation: Boolean;
                                   const BegDate, EndDate: TDateTime; const QecCleanUpLimit: Currency;
                                   const TaxAdjLimit: Currency; const PrintReports: boolean): IisListOfValues;
    function  PayTaxes(const FedTaxLiabilityList, StateTaxLiabilityList, SUITaxLiabilityList, LocalTaxLiabilityList: string;
                      const ForceChecks: Boolean; const EftpReference: string; const SbTaxPaymentNbr : Integer;
                      const PostProcessReportName: String): IisStream;
    function  GetSystemAccountNbr: Integer;
    function  PrintChecksAndCouponsForTaxes(const TaxPayrollFilter, DefaultMiscCheckForm: string): TrwReportResults;
    function  FinalizeTaxPayments(const Params: IisStream; const AACHOptions: IisStringList; const SbTaxPaymentNbr : Integer;
      const PostProcessReportName, DefaultMiscCheckForm: String): IisStream;
    function  GetSBEmail(const aCoNbr: integer): String;
    function  GetWCReportList(const aCoNbr: integer; const AReportNbrs: IisStringList): IevDataset;
    procedure GetPayrollTaxTotals(const PrNbr: Integer; var Totals: Variant);
    procedure GetPayrollEDTotals(const PrNbr: Integer; var Totals: Variant;
      var Checks: Integer; var Hours, GrossWages, NetWages: Currency);
    procedure  DeleteRowsInDataSets(const AClientNbr: Integer; const ATableNames, AConditions: IisStringList;
      ARowNbrs: IisListOfValues);
    procedure SetEeEmail(const AEENbr: integer; const ANewEMail: String);
    function StoreReportParametersFromXML(const aCoNbr: integer; const aCoReportsNbr: integer; const aXMLParameters: String):integer;
    function GetCoReportsParametersAsXML(const aCoNbr: integer; const aCoReportsNbr: integer): string;
    function ConvertXMLtoRWParameters(  const sXMLParameters: AnsiString): IIsStream;

    function  RunReport(const aReportNbr: integer; const AParams: IisListOfValues): String;
    function  GetPDFReport(const aTaskId: string): IisStream;
    function  GetAllPDFReports(const aTaskId: string): IEvDataSet;
    function  GetTaskList: IEvDataSet;
    function  GetSysReportByNbr(const aReportNbr: integer): IisStream;
    function  GetReportFileByNbrAndType(const aReportNbr: integer; const aReportType: string): IisStream;

    // former API DoInvoke
    function CalculateTaxes(const Params: IisStream): IisStream;
    function CalculateChecklines(const Params: IisStream): IisStream;
    function RefreshEDS(const Params: IisStream): IisStream;
    function CreateCheck(const Params: IisStream): IisStream;
    function TCImport(const Params: IisStream): IisStream;
    function GetW2(const Params: IisStream; const Param : String): IisStream;
    function ChangeCheckStatus(const Params: IisStream): IisStream;
    function HolidayDay(const Params: IisStream): IisStream;
    function NextPayrollDates(const Params: IisStream): IisStream;
    function ReportToPDF(const Params: IisStream): IisStream;
    function AllReportsToPDF(const Params: IisStream): IEvDataSet;
    function GetEEW2List( const pEENBR : Integer): IEvDataSet;
    function GetEEScheduled_E_DS(const eeList: string): IEvDataSet;

    procedure CalcEEScheduled_E_DS_Rate(const aCO_BENEFIT_SUBTYPE_NBR, aCL_E_DS_NBR: Integer;
                                        const aBeginDate, aEndDate: TDateTime;
                                        out aRate_Type: String; out aValue: Variant); overload;

    procedure CalcEEScheduled_E_DS_Rate(const aEE_BENEFITS_NBR, aCL_E_DS_NBR: Integer;
                                      const aBeginDate, aEndDate: TDateTime;
                                      const aAMOUNT, aANNUAL_MAXIMUM_AMOUNT, aTARGET_AMOUNT: variant;
                                      const aFREQUENCY:string;
                                      out aRate_Type: String; out aValue: Variant); overload;

    function GetCoCalendarDefaults(const ACoNbr: integer): IisListOfValues;
    function CreateCoCalendar(const ACoNbr: integer; const ACoSettings: IisListOfValues; const ABasedOn, AMoveCheckDate, AMoveCallInDate, AMoveDeliveryDate: String): IisStringList;
    function CreateManualCheck(const Params: IisStream): IisStream;
    function GetManualTaxesListForCheck(const APrCheckNbr: integer): IevDataset;


    // API Billing
    procedure SaveAPIBilling(const Params : IisListOfValues);
    function GetAPIBilling :IevDataSet;
    //TOA requests
    procedure AddTOARequests(const Requests: IisInterfaceList);  // Requests contains IEvTOARequest
    procedure UpdateTOARequests(const Requests: IisInterfaceList);  // Requests contains IEvTOARequest
    function  GetTOARequestsForEE(const EENbr: integer): IisInterfaceList; // Result contains IEvTOABusinessRequest
    function  GetTOARequestsForMgr(const MgrEENbr: integer): IisInterfaceList; // Result contains IEvTOABusinessRequest
    procedure ProcessTOARequests(const Operation: TevTOAOperation; const Notes: String;
      const EETimeOffAccrualOperNbrs: IisStringList; const CheckMgr: boolean = True);
    function  GetTOARequestsForCO(const CoNBR:Integer; const Codes:string; const FromDate, ToDate : TDateTime; const Manager, TOType, EECode: Variant): IisInterfaceList;
    procedure UpdateTOARequestsForCO(const Requests: IisInterfaceList; const CustomCoNbr:string);

    // for EvOX and API
    procedure AddTOAForNewEE(const CoNbr: integer; const EENbr: integer);
    procedure AddEDsForNewEE(const CoNbr: integer; const EENbr: integer);
    // EE change requests
    procedure CreateEEChangeRequest(const AEEChangeRequest: IevEEChangeRequest);
    function  GetEEChangeRequestList(const AEENbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList; // Result contains list of IevEEChangeRequest
    function  GetEEChangeRequestListForMgr(const AMgrNbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList; // Result contains list of IevEEChangeRequest
    function  ProcessEEChangeRequests(const ARequestsNbrs: IisStringList; const AOperation: boolean): IisStringList; // Returns numbers of requests which processed with erros

    // EE benefits requests
    function  GetBenefitRequest(const ARequestNbr: integer): IisListOfValues;
    function  StoreBenefitRequest(const AEENbr: integer; const ARequestData: IisListOfValues; const ARequestNbr: integer): integer;
    function  GetBenefitRequestList(const AEENbr: integer): IisStringList;
    function  SendEMailToHR(const AEENbr: integer; const ASubject: String; const AMessage: String; const AAttachements: IisListOfValues): IisListOfValues;
    procedure SendEmailToManagers(const AEeNbr: integer; const ARequestType: String; const AMessageDetails: String = '');
    function  GetEeExistingBenefits(const AEENbr: integer; const ACurrentOnly: boolean = True): IevDataset;
    procedure DeleteBenefitRequest(const ARequestNbr: integer);
    function  GetEeBenefitPackageAvaliableForEnrollment(const AEENbr: integer; const ASearchForDate: TDateTime; const ACategory: String;
      const AReturnCoBenefitsDataOnly: boolean): IisListOfValues;
    function  GetSummaryBenefitEnrollmentStatus(const AEENbrList: IisStringList; const AType: String): IevDataset;
    procedure RevertBenefitRequest(const ARequestNbr: integer);
    procedure SubmitBenefitRequest(const ARequestNbr: integer; const ASignatureParams: IisListOfValues);
    function  GetBenefitRequestConfirmationReport(const ARequestNbr: integer; const isResultPDF: boolean=true): IisStream;
    procedure SendBenefitEmailToEE(const AEENbr: integer; const ASubject: String; const AMessage: String;
      const ARequestNbr: integer = -1; const ACategoryType: String = '');
    function  GetEmptyBenefitRequest: IisListOfValues;
    procedure CloseBenefitEnrollment(const AEENbr: integer; const ACategoryType: String; const ARequestNbr: integer = -1);
    procedure ApproveBenefitRequest(const ARequestNbr: integer);
    function  GetBenefitEmailNotificationsList: IisListofValues;

    function  GetDataDictionaryXML(): IisStream;

    function DeleteEE(const AEENbr : Integer): boolean;

    function GetACAHistory(const aEENbr, aYear : Integer): IEvDataSet;
    function PostACAHistory(const aEENbr, aYear: Integer; const aCd: IEvDataSet): boolean;
    function GetACAOfferCodes(const aCoNbr: Integer; Const AsOfDate: TDateTime): IEvDataSet;
    function GetACAReliefCodes(const aCoNbr: Integer; Const AsOfDate: TDateTime): IEvDataSet;
  end;


function GetRemoteMiscRoutinesProxy: IevRemoteMiscRoutines;
begin
  Result := TevRemoteMiscRoutinesProxy.Create;
end;


{ TevRemoteMiscRoutinesProxy }

function TevRemoteMiscRoutinesProxy.CalculateChecklines(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CALCULATECHECKLINES);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.CalculateTaxes(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CALCULATETAXES);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.ChangeCheckStatus(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CHANGECHECKSTATUS);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.CreateCheck(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CREATECHECK);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.FinalizeTaxPayments(
  const Params: IisStream; const AACHOptions: IisStringList;
  const SbTaxPaymentNbr : Integer;
  const PostProcessReportName,DefaultMiscCheckForm: String): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_FINALIZETAXPAYMENTS);
  D.Params.AddValue('Params', Params);
  D.Params.AddValue('ACHOptions.ini', AACHOptions);
  D.Params.AddValue('SbTaxPaymentNbr', SbTaxPaymentNbr);
  D.Params.AddValue('PostProcessReportName', PostProcessReportName);
  D.Params.AddValue('DefaultMiscCheckForm', DefaultMiscCheckForm);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.GetAPIBilling: IevDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GET_API_BILLING);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IevDataSet;
end;

function TevRemoteMiscRoutinesProxy.GetW2(const Params: IisStream; const Param: String): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETW2);
  D.Params.AddValue('Params', Params);
  D.Params.AddValue('Param', Param);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.GetEEW2List(const pEENBR: Integer): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETEEW2LIST);
  D.Params.AddValue('pEENBR', pEENBR);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;
end;

function TevRemoteMiscRoutinesProxy.GetEEScheduled_E_DS(const eeList: string): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETEESCHEDULED_E_DS);
  D.Params.AddValue('eeList', eeList);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;
end;

procedure TevRemoteMiscRoutinesProxy.CalcEEScheduled_E_DS_Rate(const aCO_BENEFIT_SUBTYPE_NBR, aCL_E_DS_NBR: Integer;
                                        const aBeginDate, aEndDate: TDateTime;
                                        out aRate_Type: String; out aValue: Variant);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CALC_EESCHEDULED_E_DS_RATE);
  D.Params.AddValue('aCO_BENEFIT_SUBTYPE_NBR', aCO_BENEFIT_SUBTYPE_NBR);
  D.Params.AddValue('aCL_E_DS_NBR', aCL_E_DS_NBR);
  D.Params.AddValue('aBeginDate', aBeginDate);
  D.Params.AddValue('aEndDate', aEndDate);

  D := SendRequest(D);

  aRate_Type := D.Params.Value['aRate_Type'];
  aValue := D.Params.Value['aValue'];
end;

function TevRemoteMiscRoutinesProxy.HolidayDay(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_HOLIDAYDAY);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.NextPayrollDates(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_NEXTPAYROLLDATES);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.PayTaxes(const FedTaxLiabilityList,
  StateTaxLiabilityList, SUITaxLiabilityList,
  LocalTaxLiabilityList: string; const ForceChecks: Boolean;
  const EftpReference: string; const SbTaxPaymentNbr : Integer;
  const PostProcessReportName: String): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_PAYTAXES);
  D.Params.AddValue('FedTaxLiabilityList', FedTaxLiabilityList);
  D.Params.AddValue('StateTaxLiabilityList', StateTaxLiabilityList);
  D.Params.AddValue('SUITaxLiabilityList', SUITaxLiabilityList);
  D.Params.AddValue('LocalTaxLiabilityList', LocalTaxLiabilityList);
  D.Params.AddValue('ForceChecks', ForceChecks);
  D.Params.AddValue('EftpReference', EftpReference);
  D.Params.AddValue('SbTaxPaymentNbr', SbTaxPaymentNbr);
  D.Params.AddValue('PostProcessReportName', PostProcessReportName);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.PreProcessForQuarter(
  const Cl_NBR, CO_NBR: Integer;
  const ForceProcessing, Consolidation: Boolean; const BegDate,
  EndDate: TDateTime; const QecCleanUpLimit, TaxAdjLimit: Currency;
  const PrintReports: boolean): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_PREPROCESSFORQUARTER);
  D.Params.AddValue('Cl_NBR', Cl_NBR);
  D.Params.AddValue('CO_NBR', CO_NBR);
  D.Params.AddValue('ForceProcessing', ForceProcessing);
  D.Params.AddValue('Consolidation', Consolidation);
  D.Params.AddValue('BegDate', BegDate);
  D.Params.AddValue('EndDate', EndDate);
  D.Params.AddValue('QecCleanUpLimit', QecCleanUpLimit);
  D.Params.AddValue('TaxAdjLimit', TaxAdjLimit);
  D.Params.AddValue('PrintReports', PrintReports);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevRemoteMiscRoutinesProxy.PrintChecksAndCouponsForTaxes(
  const TaxPayrollFilter, DefaultMiscCheckForm: string): TrwReportResults;
var
  D: IevDatagram;
  Res: IevDualStream;
begin
  D := CreateDatagram(METHOD_MISC_CALC_PRINTCHECKSANDCOUPONSFORTAXES);
  D.Params.AddValue('TaxPayrollFilter', TaxPayrollFilter);
  D.Params.AddValue('DefaultMiscCheckForm', DefaultMiscCheckForm);
  D := SendRequest(D);
  Result := TrwReportResults.Create;
  Res := IInterface(D.Params.Value['Result']) as IEvDualStream;
  Result.SetFromStream(Res);
end;

function TevRemoteMiscRoutinesProxy.RefreshEDS(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_REFRESHEDS);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.ReportToPDF( const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_REPORT_TO_PDF);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.AllReportsToPDF( const Params: IisStream): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_ALL_REPORTS_TO_PDF);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;;
end;

function TevRemoteMiscRoutinesProxy.GetPDFReport(const aTaskId: string): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GET_PDF_REPORT);
  D.Params.AddValue('aTaskId', aTaskId);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.GetAllPDFReports(const aTaskId: string): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GET_ALL_PDF_REPORTS);
  D.Params.AddValue('aTaskId', aTaskId);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;
end;


function TevRemoteMiscRoutinesProxy.RunReport(const aReportNbr: integer; const AParams: IisListOfValues): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_RUN_REPORT);
  D.Params.AddValue('aReportNbr', aReportNbr);
  D.Params.AddValue('AParams', AParams);

  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevRemoteMiscRoutinesProxy.GetTaskList: IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GET_TASK_LIST);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IevDataSet;
end;


procedure TevRemoteMiscRoutinesProxy.SaveAPIBilling(const Params: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_SAVE_API_BILLING);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.TCImport(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_TCIMPORT);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.GetSBEmail(const aCoNbr: integer): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETSBEMAIL);
  D.Params.AddValue('CoNbr', aCoNbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

procedure TevRemoteMiscRoutinesProxy.AddTOARequests(const Requests: IisInterfaceList);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_ADDTOAREQUESTS);
  D.Params.AddValue('Requests', Requests);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetTOARequestsForEE(const EENbr: integer): IisInterfaceList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETTOAREQUESTSFOREE);
  D.Params.AddValue('EENbr', EENbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisInterfaceList;
end;

function TevRemoteMiscRoutinesProxy.GetTOARequestsForMgr(const MgrEENbr: integer): IisInterfaceList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETTOAREQUESTSFORMGR);
  D.Params.AddValue('MgrEENbr', MgrEENbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisInterfaceList;
end;

procedure TevRemoteMiscRoutinesProxy.ProcessTOARequests(const Operation: TevTOAOperation; const Notes: String;
  const EETimeOffAccrualOperNbrs: IisStringList; const CheckMgr: boolean = True);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_PROCESSTOAREQUESTS);
  D.Params.AddValue('Operation', Integer(Operation));
  D.Params.AddValue('Notes', Notes);
  D.Params.AddValue('EETimeOffAccrualOperNbrs', EETimeOffAccrualOperNbrs);
  D.Params.AddValue('CheckMgr', CheckMgr);
  D := SendRequest(D);
end;

procedure TevRemoteMiscRoutinesProxy.UpdateTOARequests(const Requests: IisInterfaceList);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_UPDATETOA_REQUESTS);
  D.Params.AddValue('Requests', Requests);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetWCReportList(const aCoNbr: integer; const AReportNbrs: IisStringList): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETWCREPORTLIST);
  D.Params.AddValue('CoNbr', aCoNbr);
  D.Params.AddValue('ReportNbrs', AReportNbrs);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IevDataset;
end;

procedure TevRemoteMiscRoutinesProxy.GetPayrollTaxTotals(
  const PrNbr: Integer; var Totals: Variant);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETPAYROLLTAXTOTALS);
  D.Params.AddValue('PrNbr', PrNbr);
  D := SendRequest(D);
  Totals := D.Params.Value['Totals'];
end;

procedure TevRemoteMiscRoutinesProxy.GetPayrollEDTotals(
  const PrNbr: Integer; var Totals: Variant;
  var Checks: Integer; var Hours, GrossWages, NetWages: Currency);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETPAYROLLEDTOTALS);
  D.Params.AddValue('PrNbr', PrNbr);
  D := SendRequest(D);
  Totals := D.Params.Value['Totals'];
  Checks := D.Params.Value['Checks'];
  Hours := D.Params.Value['Hours'];
  GrossWages := D.Params.Value['GrossWages'];
  NetWages := D.Params.Value['NetWages'];
end;

procedure TevRemoteMiscRoutinesProxy.AddTOAForNewEE(const CoNbr, EENbr: integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_ADDTOAFORNEWEE);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('EENbr', EENbr);
  D := SendRequest(D);
end;

procedure TevRemoteMiscRoutinesProxy.AddEDsForNewEE(const CoNbr, EENbr: integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_ADDEDSFORNEWEE);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('EENbr', EENbr);
  D := SendRequest(D);
end;

procedure TevRemoteMiscRoutinesProxy.DeleteRowsInDataSets(const AClientNbr: Integer; const ATableNames, AConditions: IisStringList;
  ARowNbrs: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_DELETEROWSINDATASETS);
  D.Params.AddValue('AClientNbr', AClientNbr);
  D.Params.AddValue('ATableNames', ATableNames);
  D.Params.AddValue('AConditions', AConditions);
  D.Params.AddValue('ARowNbrs', ARowNbrs);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetTOARequestsForCO(const CoNBR: Integer; const Codes: string; const FromDate,
  ToDate: TDateTime; const Manager, TOType, EECode: Variant): IisInterfaceList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETTOAREQUESTSFORCO);
  D.Params.AddValue('CONBR', CoNBR);
  D.Params.AddValue('Codes', Codes);
  D.Params.AddValue('FromDate', FromDate);
  D.Params.AddValue('ToDate', ToDate);
  D.Params.AddValue('Manager', Manager);
  D.Params.AddValue('TOType', TOType);
  D.Params.AddValue('EECode', EECode);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisInterfaceList;
end;

procedure TevRemoteMiscRoutinesProxy.UpdateTOARequestsForCO(const Requests: IisInterfaceList; const CustomCoNbr:string);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_UPDATETOAREQUESTSFORCO);
  D.Params.AddValue('Requests', Requests);
  D.Params.AddValue('CustomCoNbr', CustomCoNbr);
  D := SendRequest(D);
end;

procedure TevRemoteMiscRoutinesProxy.CreateEEChangeRequest(const AEEChangeRequest: IevEEChangeRequest);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CREATEEECHANGEREQUEST);
  D.Params.AddValue('EEChangeRequest', AEEChangeRequest);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetEEChangeRequestList(const AEENbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETEECHANGEREQUESTLIST);
  D.Params.AddValue('EENbr', AEENbr);
  D.Params.AddValue('RequestType', Integer(AType));
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisInterfaceList;
end;

function TevRemoteMiscRoutinesProxy.GetEEChangeRequestListForMgr(const AMgrNbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_FORMGRGETEECHANGEREQUESTLIST);
  D.Params.AddValue('MgrNbr', AMgrNbr);
  D.Params.AddValue('RequestType', Integer(AType));
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisInterfaceList;
end;

function TevRemoteMiscRoutinesProxy.ProcessEEChangeRequests(const ARequestsNbrs: IisStringList;
  const AOperation: boolean): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_PROCESSEECHANGEREQUESTS);
  D.Params.AddValue('RequestsNbrs', ARequestsNbrs);
  D.Params.AddValue('Operation', AOperation);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStringList;
end;


function TevRemoteMiscRoutinesProxy.GetCoCalendarDefaults(const ACoNbr: integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETCOCALENDARDEFAULTS);
  D.Params.AddValue('CoNbr', ACoNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevRemoteMiscRoutinesProxy.CreateCoCalendar(const ACoNbr: integer; const ACoSettings: IisListOfValues; const ABasedOn, AMoveCheckDate,
  AMoveCallInDate, AMoveDeliveryDate: String): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CREATECOCALENDAR);
  D.Params.AddValue('CoNbr', ACoNbr);
  D.Params.AddValue('CoSettings', ACoSettings);
  D.Params.AddValue('BasedOn', ABasedOn);
  D.Params.AddValue('MoveCheckDate', AMoveCheckDate);
  D.Params.AddValue('MoveCallInDate', AMoveCallInDate);
  D.Params.AddValue('MoveDeliveryDate', AMoveDeliveryDate);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStringList;
end;

procedure TevRemoteMiscRoutinesProxy.SetEeEmail(const AEENbr: integer; const ANewEMail: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_SETEEEMAIL);
  D.Params.AddValue('AEENbr', AEENbr);
  D.Params.AddValue('ANewEmail', ANewEMail);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.StoreReportParametersFromXML(const aCoNbr: integer; const aCoReportsNbr: integer; const aXMLParameters: String):integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_STOREREPORTPARAMETERSFROMXML);
  D.Params.AddValue('aCoNbr', aCoNbr);
  D.Params.AddValue('aCoReportsNbr', aCoReportsNbr);
  D.Params.AddValue('aXMLParameters', aXMLParameters);
  D := SendRequest(D);
  Result := integer(D.Params.Value['Result']);
end;

function TevRemoteMiscRoutinesProxy.ConvertXMLtoRWParameters(  const sXMLParameters: AnsiString): IIsStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CONVERTXMLTORWPARAMETERS);
  D.Params.AddValue('sXMLParameters', sXMLParameters);
  D := SendRequest(D);
  Result :=  IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.GetCoReportsParametersAsXML(const aCoNbr: integer; const aCoReportsNbr: integer): string;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETCOREPORTSPARAMETERSASXML);
  D.Params.AddValue('aCoNbr', aCoNbr);
  D.Params.AddValue('aCoReportsNbr', aCoReportsNbr);
  D := SendRequest(D);
  Result := string(D.Params.Value['Result']);
end;



function TevRemoteMiscRoutinesProxy.GetBenefitRequest(const ARequestNbr: integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETBENEFITREQUEST);
  D.Params.AddValue('RequestNbr', ARequestNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevRemoteMiscRoutinesProxy.StoreBenefitRequest(const AEENbr: integer; const ARequestData: IisListOfValues; const ARequestNbr: integer): integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_STOREBENEFITREQUEST);
  D.Params.AddValue('EENbr', AEENbr);
  D.Params.AddValue('RequestData', ARequestData);
  D.Params.AddValue('RequestNbr', ARequestNbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevRemoteMiscRoutinesProxy.GetBenefitRequestList(const AEENbr: integer): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETBENEFITREQUESTLIST);
  D.Params.AddValue('EENbr', AEENbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStringList;
end;

function TevRemoteMiscRoutinesProxy.SendEMailToHR(const AEENbr: integer; const ASubject, AMessage: String; const AAttachements: IisListOfValues): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_SENDEMAILTOHR);
  D.Params.AddValue('EENbr', AEENbr);
  D.Params.AddValue('Subject', ASubject);
  D.Params.AddValue('Message', AMessage);
  D.Params.AddValue('Attachements', AAttachements);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

procedure TevRemoteMiscRoutinesProxy.SendEmailToManagers(
  const AEeNbr: integer; const ARequestType, AMessageDetails: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_SENDEMAILTOMANAGERS);
  D.Params.AddValue('EENbr', AEENbr);
  D.Params.AddValue('ARequestType', ARequestType);
  D.Params.AddValue('AMessageDetails', AMessageDetails);
  D := SendRequest(D);
end;


function TevRemoteMiscRoutinesProxy.GetEeExistingBenefits(const AEENbr: integer;  const ACurrentOnly: boolean = True): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETEEEXISTINGBENEFITS);
  D.Params.AddValue('EENbr', AEENbr);
  D.Params.AddValue('ACurrentOnly', ACurrentOnly);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IevDataset;
end;

procedure TevRemoteMiscRoutinesProxy.DeleteBenefitRequest(const ARequestNbr: integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_DELETEBENEFITREQUEST);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetEeBenefitPackageAvaliableForEnrollment(const AEENbr: integer; const ASearchForDate: TDateTime; const ACategory: String;
  const AReturnCoBenefitsDataOnly: boolean): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETEEBENEFITPACKAGEAVALIABLEFORENROLLMENT);
  D.Params.AddValue('EENbr', AEENbr);
  D.Params.AddValue('ASearchForDate', ASearchForDate);
  D.Params.AddValue('ACategory', ACategory);
  D.Params.AddValue('AReturnCoBenefitsDataOnly', AReturnCoBenefitsDataOnly);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevRemoteMiscRoutinesProxy.GetSummaryBenefitEnrollmentStatus(const AEENbrList: IisStringList; const AType: String): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETSUMMARYBENEFITENROLLMENTSTATUS);
  D.Params.AddValue('EENbrList', AEENbrList);
  D.Params.AddValue('Type', AType);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IevDataset;
end;

procedure TevRemoteMiscRoutinesProxy.RevertBenefitRequest(const ARequestNbr: integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_REVERTBENEFITREQUEST);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetBenefitRequestConfirmationReport(const ARequestNbr: integer;const isResultPDF: boolean=true): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETBENEFITREQUESTCONFIRMATIONREPORT);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D.Params.AddValue('isResultPDF', isResultPDF);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

procedure TevRemoteMiscRoutinesProxy.SubmitBenefitRequest(const ARequestNbr: integer; const ASignatureParams: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_SUBMITBENEFITREQUEST);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D.Params.AddValue('ASignatureParams', ASignatureParams);
  D := SendRequest(D);
end;

procedure TevRemoteMiscRoutinesProxy.SendBenefitEmailToEE(const AEENbr: integer; const ASubject, AMessage: String;
  const ARequestNbr: integer; const ACategoryType: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_SENDBENEFITEMAILTOEE);
  D.Params.AddValue('AEENbr', AEENbr);
  D.Params.AddValue('ASubject', ASubject);
  D.Params.AddValue('AMessage', AMessage);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D.Params.AddValue('ACategoryType', ACategoryType);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetEmptyBenefitRequest: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETEMPTYBENEFITREQUEST);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

procedure TevRemoteMiscRoutinesProxy.CloseBenefitEnrollment(const AEENbr: integer; const ACategoryType: String;
  const ARequestNbr: integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CLOSEBENEFITENROLLMENT);
  D.Params.AddValue('AEENbr', AEENbr);
  D.Params.AddValue('ACategoryType', ACategoryType);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetDataDictionaryXML: IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETDATADICTIONARYXML);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;


procedure TevRemoteMiscRoutinesProxy.ApproveBenefitRequest(const ARequestNbr: integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_APPROVEBENEFITREQUEST);
  D.Params.AddValue('ARequestNbr', ARequestNbr);
  D := SendRequest(D);
end;

function TevRemoteMiscRoutinesProxy.GetBenefitEmailNotificationsList: IisListofValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETBENEFITEMAILNOTIFICATIONSLIST);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevRemoteMiscRoutinesProxy.GetSystemAccountNbr: Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETSYSTEMACCOUNTNBR);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevRemoteMiscRoutinesProxy.CreateManualCheck(const Params: IisStream): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CREATEMANUALCHECK);
  D.Params.AddValue('Params', Params);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.GetManualTaxesListForCheck(const APrCheckNbr: integer): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETMANUALTAXLISTFORCHECK);
  D.Params.AddValue('PrCheckNbr', APrCheckNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IevDataset;
end;

procedure TevRemoteMiscRoutinesProxy.CalcEEScheduled_E_DS_Rate(
  const aEE_BENEFITS_NBR, aCL_E_DS_NBR: Integer; const aBeginDate,
  aEndDate: TDateTime; const aAMOUNT, aANNUAL_MAXIMUM_AMOUNT,
  aTARGET_AMOUNT: variant; const aFREQUENCY: string;
  out aRate_Type: String; out aValue: Variant);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_CALC_EESCHEDULED_E_DS_RATE2);
  D.Params.AddValue('aEE_BENEFITS_NBR', aEE_BENEFITS_NBR);
  D.Params.AddValue('aCL_E_DS_NBR', aCL_E_DS_NBR);
  D.Params.AddValue('aBeginDate', aBeginDate);
  D.Params.AddValue('aEndDate', aEndDate);
  D.Params.AddValue('aAMOUNT', aAMOUNT);
  D.Params.AddValue('aANNUAL_MAXIMUM_AMOUNT', aANNUAL_MAXIMUM_AMOUNT);
  D.Params.AddValue('aTARGET_AMOUNT', aTARGET_AMOUNT);
  D.Params.AddValue('aFREQUENCY', aFREQUENCY);

  D := SendRequest(D);

  aRate_Type := D.Params.Value['aRate_Type'];
  aValue := D.Params.Value['aValue'];
end;

function TevRemoteMiscRoutinesProxy.GetSysReportByNbr(
  const aReportNbr: integer): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GET_SYS_REPORT);
  D.Params.AddValue('aReportNbr', aReportNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevRemoteMiscRoutinesProxy.DeleteEE(
  const AEENbr: Integer): boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_DELETEEE);
  D.Params.AddValue('AEENbr', AEENbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevRemoteMiscRoutinesProxy.GetReportFileByNbrAndType(
  const aReportNbr: integer; const aReportType: string): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GET_REPORT_FILE);
  D.Params.AddValue('aReportNbr', aReportNbr);
  D.Params.AddValue('aReportType', aReportType);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;



function TevRemoteMiscRoutinesProxy.GetACAHistory(const aEENbr,
  aYear: Integer): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETACAHISTORY);
  D.Params.AddValue('aEENbr', aEENbr);
  D.Params.AddValue('aYear', aYear);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;
end;

function TevRemoteMiscRoutinesProxy.PostACAHistory(const aEENbr,
  aYear: Integer; const aCd: IEvDataSet): boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_POSTACAHISTORY);
  D.Params.AddValue('aEENBR', aEENbr);
  D.Params.AddValue('aYear', aYear);
  D.Params.AddValue('aCd', aCd);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevRemoteMiscRoutinesProxy.GetACAOfferCodes(const aCoNbr: Integer; const AsOfDate: TDateTime): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETACAOFFERCODES);
  D.Params.AddValue('aCoNbr', aCoNbr);
  D.Params.AddValue('AsOfDate', AsOfDate);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;
end;

function TevRemoteMiscRoutinesProxy.GetACAReliefCodes(const aCoNbr: Integer; const AsOfDate: TDateTime): IEvDataSet;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_MISC_CALC_GETACARELIEFCODES);
  D.Params.AddValue('aCoNbr', aCoNbr);
  D.Params.AddValue('AsOfDate', AsOfDate);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IEvDataSet;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRemoteMiscRoutinesProxy, IevRemoteMiscRoutines, 'Remote Misc Routines Proxy');
end.
