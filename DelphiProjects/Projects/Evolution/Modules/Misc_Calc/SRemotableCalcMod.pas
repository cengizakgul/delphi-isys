// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SRemotableCalcMod;

interface

uses
  Classes, ActnList,  Menus, ExtCtrls, EvBasicUtils,
  ImgList, Controls, ComCtrls, ToolWin, Buttons,  EvStreamUtils,
  Variants, DB,
  SReportSettings,
  SPD_TaxCalculator, Windows, ISZippingRoutines, isBaseClasses,
  EvCommonInterfaces, EvMainboard, EvContext, IsThreadManager, StrUtils, SProcessTCImport,
  EvDataSet, isDataSet, EvExceptions, isBasicUtils, EvClasses, SCalendarCreation,
  isErrorUtils, EvClientDataSet, EvLegacy
  ,XMLRWParams
  ;

implementation

uses SysUtils, EvUtils, EvConsts, SDataStructure, SBalanceInformation, EvTypes,
  SDM_MISC_LIAB_CORRECT, STaxPaymentsProcedures, SPayrollReport, isLogFile,
  EvTOARequest, DateUtils, kbmMemTable, EvDataAccessComponents, EvEEChangeRequest,
  SDDStreamClasses, SFieldCodeValues, SDDClasses, SDataDictclient,
  ISDataAccessComponents;

const
  cESSFrom = 'EvolutionSelfServe';

type
  TBenefitRequestDescription = record
    CoBenefitNbr: integer;
    CategoryType: String;
    Status: String;
    Declined: boolean;
  end;

  TevRemoteMiscRoutines = class(TisInterfacedObject, IevRemoteMiscRoutines)
  private
    dmBalance: TBalanceDM;

    function  GetCoBenefitEnrollmentNbr(const AcategoryNbr: integer; const CategoryDesc:String; const bEvent: boolean):variant;
    procedure preTaxCalculatorData(i : integer; const  pr_check : TevClientDataSet);
    procedure SendEmailToManagers(const AEeNbr: integer; const ARequestType: String; const AMessageDetails: String = '');
    procedure SendEmailToEmployeeAndManager(const AEeNbr: integer; const ASubject: String; const AMessageDetails: String = '');
    procedure GetEENameByNbr(const AEeNbr: integer; out AFirstName, ALastName, ACustomEeNumber: String);
    function  APIGetDataSet(const ClientId: Integer; sTableName, sTableCondition: string): TevClientDataSet;
    procedure CheckCompanyLock(const ACoNbr: Variant; const AAsOfDate: TdateTime = 0;
      const ACheckForTaxPmtsFlag: boolean = false);
    function GetTOARequests (const AEENbr: integer; const Q: IEvQuery ) : IisInterfaceList;
    function PackBenefitRequestDescription(const AValue: TBenefitRequestDescription) : String;
    function UnpackBenefitRequestDescription(const AValue: String): TBenefitRequestDescription;
    function GetHRPersonsForOpenedClient: IisParamsCollection;
    procedure StoreBenefitRefusalReasonForEE(const AEENbr: integer; const ACategory: String;
      const ARefusalReason: integer; const AHomeState: Variant; const ARefusalDate: TDate);
    function BlobFieldToString(const AField: TBlobField): String;
    function PopulateCheck(CONBR, PRNBR, EENBR, PrCheckTempNBR, VoidCheckNBR : Integer;
                           CheckType : string; IsPaySalary, IsPayHours, IsRefreshSchedEds, IsSave: Boolean;
                           PaymentSerialCheckNBR :integer = 0; UpdateBalance: string = '') : IisStream;
  protected
    function PreProcessForQuarter(const Cl_NBR, CO_NBR: Integer;
      const ForceProcessing, Consolidation: Boolean;
      const BegDate, EndDate: TDateTime; const QecCleanUpLimit: Currency; const TaxAdjLimit: Currency;
      const PrintReports: boolean): IisListOfValues;
    function PayTaxes(const FedTaxLiabilityList, StateTaxLiabilityList,
      SUITaxLiabilityList, LocalTaxLiabilityList: string; const ForceChecks: Boolean;
      const EftpReference: string; const SbTaxPaymentNbr : Integer; const PostProcessReportName: String): IisStream;
    function PrintChecksAndCouponsForTaxes(const TaxPayrollFilter, DefaultMiscCheckForm: string): TrwReportResults;
    function FinalizeTaxPayments(const Params: IisStream; const AACHOptions: IisStringList;
      const SbTaxPaymentNbr : Integer; const PostProcessReportName, DefaultMiscCheckForm: String): IisStream;
    function  GetSystemAccountNbr: Integer;
    function GetSBEmail(const aCoNbr: integer): String;
    function GetWCReportList(const aCoNbr: integer; const AReportNbrs: IisStringList): IevDataset;
    procedure GetPayrollTaxTotals(const PrNbr: Integer; var Totals: Variant);
    procedure GetPayrollEDTotals(const PrNbr: Integer; var Totals: Variant;
      var Checks: Integer; var Hours, GrossWages, NetWages: Currency);
    procedure DeleteRowsInDataSets(const AClientNbr: Integer; const ATableNames, AConditions: IisStringList;
      ARowNbrs: IisListOfValues);  // ARowNbrs should contain IisStringLists with RowNbrs with the same index as ATableNames
    function  GetCoCalendarDefaults(const ACoNbr: integer): IisListOfValues;
    function  CreateCoCalendar(const ACoNbr: integer; const ACoSettings: IisListOfValues; const ABasedOn, AMoveCheckDate, AMoveCallInDate, AMoveDeliveryDate: String): IisStringList;
    procedure SetEeEmail(const AEENbr: integer; const ANewEMail: String);
// new functionality for AdHoc reports via Web
    function StoreReportParametersFromXML(const Co_nbr, Co_Reports_Nbr: integer; const XMLParameters: String): integer;
    function GetCoReportsParametersAsXML(const Co_nbr: integer; const Co_Reports_Nbr: integer): string;
    function ConvertXMLtoRWParameters(const sXMLParameters: AnsiString):IIsStream;

    // former API DoInvoke
    function CalculateTaxes(const Params: IisStream): IisStream;
    function CalculateCheckLines(const Params: IisStream): IisStream;
    function RefreshEDS(const Params: IisStream): IisStream;
    function CreateCheck(const Params: IisStream): IisStream;
    function CreateManualCheck(const Params: IisStream): IisStream;
    function TCImport(const Params: IisStream): IisStream;
    function GetW2(const Params: IisStream; const Param : String): IisStream;
    function ChangeCheckStatus(const Params: IisStream): IisStream;
    function HolidayDay(const Params: IisStream): IisStream;
    function NextPayrollDates(const Params: IisStream): IisStream;
    function ReportToPDF(const Params: IisStream): IisStream;
    function AllReportsToPDF(const Params: IisStream): IEvDataSet;
    function GetEEW2List(const pEENBR : Integer): IEvDataSet;

    function  RunReport(const aReportNbr: integer; const AParams: IisListOfValues): string;
    function  GetPDFReport(const aTaskId: string): IisStream;
    function  GetAllPDFReports(const aTaskId: string): IEvDataSet;
    function  GetTaskList: IEvDataSet;
    function  GetSysReportByNbr(const aReportNbr: integer): IisStream;
    function  GetReportFileByNbrAndType(const aReportNbr: integer; const aReportType: string): IisStream;

    function GetEEScheduled_E_DS(const eeList: string): IEvDataSet;

    procedure CalcEEScheduled_E_DS_Rate(const aCO_BENEFIT_SUBTYPE_NBR, aCL_E_DS_NBR: Integer;
                                        const aBeginDate, aEndDate: TDateTime;
                                        out aRate_Type: String; out aValue: Variant); overload;
    procedure CalcEEScheduled_E_DS_Rate(const aEE_BENEFITS_NBR, aCL_E_DS_NBR: Integer;
                                      const aBeginDate, aEndDate: TDateTime;
                                      const aAMOUNT, aANNUAL_MAXIMUM_AMOUNT, aTARGET_AMOUNT: variant;
                                      const aFREQUENCY:string;
                                      out aRate_Type: String; out aValue: Variant); overload;

    function GetManualTaxesListForCheck(const APrCheckNbr: integer): IevDataset;

    // API Billing
    procedure SaveAPIBilling(const Params : IisListOfValues); // Params - billing info for ONE domain
    function GetAPIBilling: IevDataSet;

    //TOA requests
    procedure AddTOARequests(const Requests: IisInterfaceList);  // Requests contains IEvTOARequest
    procedure UpdateTOARequests(const Requests: IisInterfaceList);  // Requests contains IEvTOARequest
    function  GetTOARequestsForEE(const EENbr: integer): IisInterfaceList; // Result contains IEvTOABusinessRequest
    function  GetTOARequestsForMgr(const MgrEENbr: integer): IisInterfaceList; // Result contains IEvTOABusinessRequest
    procedure ProcessTOARequests(const Operation: TevTOAOperation; const Notes: String;
      const EETimeOffAccrualOperNbrs: IisStringList; const CheckMgr: boolean = True);
    function  GetTOARequestsForCO(const CoNBR:Integer; const Codes:string; const FromDate, ToDate : TDateTime; const Manager, TOType, EECode: Variant): IisInterfaceList;
    procedure UpdateTOARequestsForCO(const Requests: IisInterfaceList; const CustomCoNbr:string);

    // for EvOX and API
    procedure AddTOAForNewEE(const CoNbr: integer; const EENbr: integer);
    procedure AddEDsForNewEE(const CoNbr: integer; const EENbr: integer);

    // EE change requests
    procedure CreateEEChangeRequest(const AEEChangeRequest: IevEEChangeRequest);
    function  GetEEChangeRequestList(const AEENbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList; // Result contains list of IevEEChangeRequest
    function  GetEEChangeRequestListForMgr(const AMgrNbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList; // Result contains list of IevEEChangeRequest
    function  ProcessEEChangeRequests(const ARequestsNbrs: IisStringList; const AOperation: boolean): IisStringList; // Returns numbers of requests which processed with erros

    // Benefits requests
    function  GetBenefitRequest(const ARequestNbr: integer): IisListOfValues;
    function  StoreBenefitRequest(const AEENbr: integer; const ARequestData: IisListOfValues; const ARequestNbr: integer): integer;
    function  GetBenefitRequestList(const AEENbr: integer): IisStringList;
    function  SendEMailToHR(const AEENbr: integer; const ASubject: String; const AMessage: String; const AAttachements: IisListOfValues): IisListOfValues;
    function  GetEeExistingBenefits(const AEENbr: integer; const ACurrentOnly: boolean = True): IevDataset;
    procedure DeleteBenefitRequest(const ARequestNbr: integer);
    function  GetEeBenefitPackageAvaliableForEnrollment(const AEENbr: integer; const ASearchForDate: TDateTime; const ACategory: String;
      const AReturnCoBenefitsDataOnly: boolean): IisListOfValues;
    function  GetSummaryBenefitEnrollmentStatus(const AEENbrList: IisStringList; const AType: String): IevDataset;
    procedure RevertBenefitRequest(const ARequestNbr: integer);
    procedure SubmitBenefitRequest(const ARequestNbr: integer; const ASignatureParams: IisListOfValues);
    function  GetBenefitRequestConfirmationReport(const ARequestNbr: integer;const isResultPDF: boolean=true): IisStream;
    procedure SendBenefitEmailToEE(const AEENbr: integer; const ASubject: String; const AMessage: String;
      const ARequestNbr: integer = -1; const ACategoryType: String = '');
    function  GetEmptyBenefitRequest: IisListOfValues;
    procedure CloseBenefitEnrollment(const AEENbr: integer; const ACategoryType: String; const ARequestNbr: integer = -1);
    function  CheckIfUserIsHR: boolean;
    procedure ApproveBenefitRequest(const ARequestNbr: integer);
    function  GetBenefitEmailNotificationsList: IisListofValues;

    function  GetDataDictionaryXML: IisStream;

    function DeleteEE(const AEENbr : Integer): boolean;

    function GetACAHistory(const aEENbr, aYear : Integer): IEvDataSet;
    function PostACAHistory(const aEENbr, aYear: Integer; const aCd: IEvDataSet): boolean;
    function GetACAOfferCodes(const aCoNbr: Integer; Const AsOfDate: TDateTime): IEvDataSet;
    function GetACAReliefCodes(const aCoNbr: Integer; Const AsOfDate: TDateTime): IEvDataSet;
  end;


function GetRemoteMiscRoutines: IevRemoteMiscRoutines;
begin
  Result := TevRemoteMiscRoutines.Create;
end;

{ TTasksComp }

type
  TMyActionType = (aTax, aBalance, aQEC);
  TMyActionNode = record
    ActionType: TMyActionType;
    OkLink, FailureLink: Integer;
  end;

function TevRemoteMiscRoutines.PreProcessForQuarter(const Cl_NBR, CO_NBR: Integer;
  const ForceProcessing, Consolidation: Boolean; const BegDate, EndDate: TDateTime; const QecCleanUpLimit: Currency;
  const TaxAdjLimit: Currency; const PrintReports: boolean): IisListOfValues;

const
  MyActions: array [1..8] of TMyActionNode = (
    (ActionType: aTax; OkLink: 2; FailureLink: 5),         //1
    (ActionType: aBalance; OkLink: 0; FailureLink: 3),     //2
    (ActionType: aQEC; OkLink: 8; FailureLink: 0),         //3
    (ActionType: aBalance; OkLink: 0; FailureLink: 0),     //4
    (ActionType: aQEC; OkLink: 6; FailureLink: 0),         //5
    (ActionType: aTax; OkLink: 7; FailureLink: 0),         //6
    (ActionType: aBalance; OkLink: 0; FailureLink: 0),     //7
    (ActionType: aTax; OkLink: 4; FailureLink: 0));    //8
  mNeedsManualCorrection = 'Taxes exceed %m threshold';
  mQuarterEndCleanupFailed = 'Quarter end cleanup suspended';
  mBalanceCheckFailed = 'Balance check failed';
  mOutOfBalance = 'Out of balance';
  mTaxWageLimit = 'Taxable Wages exceed the Wage Limit.';
  mOutOfBalanceNeg = 'There are negative wages';

var
  TaxThreshhold: Currency;
  j: integer;
  Logs : TStringList;
  PrList: variant;
  LastProcessDate: TDateTime;
  ErrorMessageString: string;
  ManualProcessingRequiredFlag, YearEndFlag: Boolean;
  BalanceControl : boolean;
  a: array of TCompanyDescr;
  s: string;
  Warnings: String;
  m: IisStream;

  function CompanyDescr: string;
  begin
    with DM_COMPANY do
      if CO.Locate('CO_NBR', CO_NBR, []) then
        if Consolidation then
          Result := 'Consolidation '+ CO['CUSTOM_COMPANY_NUMBER']+ '-'+ CO['NAME']
        else
          Result := 'Company '+ CO['CUSTOM_COMPANY_NUMBER']+ '-'+ CO['NAME'];
  end;

  function GetTaxLiabAmount(aPrNbr: string; aLiabTable: string): Currency;
  begin
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then Close;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', 'Sum(Amount)');
        SetMacro('TABLENAME', aLiabTable);
        SetMacro('CONDITION', 'PR_NBR=' + aPrNbr);
        DataRequest(AsVariant);
      end;
      Open;
      Result := Abs( Fields[0].AsCurrency );
      Close;
    end;
  end;

  function FillPrList: boolean;
  var
    PrL, TaxPr: TStrings;
    i: integer;
  begin
    PrL := TStringList.Create;
    TaxPr := TStringList.Create;
    try
      PrL.Clear;
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then Close;

        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('Columns', 'PR_NBR');
          SetMacro('TABLENAME', 'PR');
          SetMacro('CONDITION', 'CO_NBR=:CoNbr and PAYROLL_TYPE=:PrType and CHECK_DATE=:CheckDate');

          SetParam('CoNbr', CO_NBR);
          SetParam('PrType', PAYROLL_TYPE_TAX_ADJUSTMENT);
          SetParam('CheckDate', EndDate);

          DataRequest(AsVariant);
        end;
        Open;
        while not Eof do
        begin
          TaxPr.Add( FieldByName('PR_NBR').AsString );
          Next;
        end;
        Close;

        for i := 0 to TaxPr.Count - 1 do
        if ( GetTaxLiabAmount(TaxPr[i], 'Co_Fed_Tax_Liabilities') +
             GetTaxLiabAmount(TaxPr[i], 'Co_Local_Tax_Liabilities') +
             GetTaxLiabAmount(TaxPr[i], 'Co_State_Tax_Liabilities') +
             GetTaxLiabAmount(TaxPr[i], 'Co_Sui_Liabilities') ) >= TaxAdjLimit then
          PrL.Add( TaxPr[i] );

        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('Columns', 'PR_NBR');
          SetMacro('TABLENAME', 'PR');
          SetMacro('CONDITION', 'CO_NBR=:CoNbr and PAYROLL_TYPE=:PrType and CHECK_DATE=:CheckDate');

          SetParam('CoNbr', CO_NBR);
          SetParam('PrType', PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
          SetParam('CheckDate', EndDate);

          DataRequest(AsVariant);
        end;
        Open;
        while not Eof do
        begin
          // if QEC payrolls are created the reports should print
          PrL.Add(FieldByName('PR_NBR').AsString);
          Next;
        end;
        Close;
      end;

      Result := PrL.Count > 0;

      PrList := VarArrayCreate([0, PrL.Count - 1], varVariant);
      for i := 0 to PrL.Count - 1 do
        PrList[i] := StrToInt(PrL[i]);

    finally
      PrL.Free;
      TaxPr.Free;
    end;
  end;

begin
  TaxThreshhold := 2.0;
  Assert(not Consolidation, 'Paramaters are outdated.');

  ctx_StartWait;
  ctx_DataAccess.PayrollIsProcessing := True;
  Result := TisListOfValues.Create;
  s := '';

  Logs := TStringList. Create;
  Logs.Add('PreProcess Start...           '+ FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));

  try
    YearEndFlag := GetEndYear(EndDate) = EndDate;
    ctx_DataAccess.OpenClient(Cl_NBR);

    DM_CLIENT.CL_CO_CONSOLIDATION.DataRequired('ALL');
    DM_COMPANY.CO.DataRequired('ALL');
    if not DM_COMPANY.CO.Locate('CO_NBR', CO_NBR, []) then
      raise EevException.CreateHelp('Temp tables for client (Internal Number '+ IntToStr(Cl_NBR)+ ') are out of sync', IDH_InconsistentData);
    Result.AddValue('TimeStampStarted', Now);
    Result.AddValue('CoCustomNumber', DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER']);
    Result.AddValue('CoName', DM_COMPANY.CO['NAME']);
    Result.AddValue('Consolidation', Consolidation);
    if not VarIsNull(DM_COMPANY.CO['MINIMUM_TAX_THRESHOLD']) then
      TaxThreshhold := DM_COMPANY.CO['MINIMUM_TAX_THRESHOLD'];

    LastProcessDate := DM_COMPANY.CO.FieldByName('LAST_PREPROCESS').AsDateTime;
    ErrorMessageString := DM_COMPANY.CO.FieldByName('LAST_PREPROCESS_MESSAGE').AsString;

    dmBalance := nil;
    begin
      try
        if (LastProcessDate <> EndDate) or (ErrorMessageString <> '') or ForceProcessing then
         begin
         ctx_UpdateWait('Checking "As Of Date" changes...');
         if ctx_DataAccess.QecRecommended(CO_NBR, BegDate) then
          begin
            ctx_UpdateWait('Quarter end cleanup...');
            Logs.Add('1: Quarter end cleanup... ' + FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));
            ctx_StartWait;
            try
              try
                ManualProcessingRequiredFlag := ctx_PayrollProcessing.DoQuarterEndCleanup(CO_NBR, EndDate, QecCleanUpLimit, Ord(cpaProcess), ''{edQePath.Text}, '', True, False, False, rlDoNotLock, Null, Warnings);
              except
                on Exception do
                  raise EevException.CreateHelp(mQuarterEndCleanupFailed+ #13 + Exception(ExceptObject).Message, Exception(ExceptObject).HelpContext);
              end;

              if not ManualProcessingRequiredFlag then
                raise EErrorMessageException.CreateHelp('Previous QEC was not finished or EE tax needs to be reviewed.', IDH_NeedsManualCorrection);
            finally
              Logs.Add('         Finish Time...          ' + FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));
              ctx_EndWait;
              ctx_DataAccess.PayrollIsProcessing := True;
            end;
          end;
          j := Low(MyActions);
          while True do
          try
            case MyActions[j].ActionType of
            aTax:
              begin
                ctx_UpdateWait('Tax correction...');
                Logs.Add('Tax correction...              ' + FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));
                with TDM_MISC_LIAB_CORRECT.Create(nil, Cl_NBR, CO_NBR, SbSendsTaxPaymentsForCompany, EndDate, YearEndFlag, True, True, True) do
                try
                  cdTaxes.First;
                  while not cdTaxes.Eof do
                  begin
                    if cdTaxes['ToDo']
                    and (cdTaxes['AdjType'] = TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END) then
                      if (cdTaxes['TaxType'] = Ord(ttSUI)) and (cdTaxes['State'] = 'MN') then
                      begin
                        if (Abs(cdTaxes['TotalDiff']) > 5) then
                          raise EErrorMessageException.CreateFmtHelp(mNeedsManualCorrection, [5.00], IDH_OutOfBalance);
                      end
                      else
                      begin
                        if (Abs(cdTaxes['TotalDiff']) > TaxThreshhold) then
                          raise EErrorMessageException.CreateFmtHelp(mNeedsManualCorrection, [TaxThreshhold], IDH_OutOfBalance);
                      end;
                    cdTaxes.Next
                  end;
                  ApplyChanges;
                finally
                  Free;
                end;
              end;
            aBalance:
              try
                ctx_UpdateWait('Checking balance...');
                Logs.Add('Checking balance...        ' + FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));
                try
                  SetLength(a, 1);
                  a[0].ClNbr := Cl_NBR;
                  a[0].CoNbr := CO_NBR;
                  a[0].Consolidation := Consolidation;
                  //if not YearEndFlag then
                  begin
                    dmBalance := GetBalanceInformation(nil, a, BegDate, EndDate);

                    try
                      Assert(dmBalance.cdsCoSum.RecordCount = 1);

                      s := '';
                      if dmBalance.cdsCoSum['bTaxWageLimit'] then
                        s := s + mTaxWageLimit;
                      if dmBalance.cdsCoSum['OutBalance'] then
                      begin
                        BalanceControl := True;
                        if dmBalance.cdsCoSum['OutBalance_EE_ER'] then
                             BalanceControl := dmBalance.cdsCoSum['OutBalance_'];
                        if BalanceControl = true then
                         begin
                          s := s + mOutOfBalance;
                          if dmBalance.cdsCoSum['OutBalWages'] then
                            s := s + #13#10 + mOutOfBalanceNeg;
                         end;
                      end;
                      if s <> '' then
                        raise EErrorMessageException.CreateHelp(s, IDH_OutOfBalance);
                    finally
                      FreeAndNil(dmBalance);
                    end;
                  end
                except
                  on EErrorMessageException do
                    raise;
                  on E: Exception do
                    raise EevException.CreateHelp(mBalanceCheckFailed+ #13+ E.Message, E.HelpContext);
                end;
              finally
                DM_COMPANY.CO.DataRequired('ALL');
              end;
            aQEC:
              begin
                ctx_UpdateWait('Quarter end cleanup...');
                Logs.Add('2: Quarter end cleanup... ' + FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));
                begin
                  ctx_StartWait;
                  try
                    try
                      ManualProcessingRequiredFlag := ctx_PayrollProcessing{(NotQueued)}.DoQuarterEndCleanup(CO_NBR, EndDate, QecCleanUpLimit, Ord(cpaProcess), ''{edQePath.Text}, '', True, False, False, rlDoNotLock, Null, Warnings);
                    except
                      on E: Exception do
                        raise EevException.CreateHelp(mQuarterEndCleanupFailed+ #13+ E.Message, E.HelpContext);
                    end;
                    if not ManualProcessingRequiredFlag then
                      raise EErrorMessageException.CreateHelp('Previous QEC was not finished or EE tax needs to be reviewed.', IDH_NeedsManualCorrection);
                  finally
                    ctx_EndWait;
                    ctx_DataAccess.PayrollIsProcessing := True;
                  end;
                end;
              end;
            end;

          Logs.Add('         Finish Time...          ' + FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));

            j := MyActions[j].OkLink;
            if j = 0 then Break;
          except
            on EErrorMessageException do
            begin
              j := MyActions[j].FailureLink;
              if j = 0 then raise;
            end;
          end;

          Assert(DM_COMPANY.CO.Locate('CO_NBR', CO_NBR, []));
          DM_COMPANY.CO.Edit;
          DM_COMPANY.CO['LAST_SUI_CORRECTION'] := EndDate;
          if YearEndFlag then
            DM_COMPANY.CO['LAST_FUI_CORRECTION'] := EndDate;
          DM_COMPANY.CO.Post;
        end;
        ErrorMessageString := '';
      except
        on E: EErrorMessageException do
          ErrorMessageString := E.Message;
        on E: Exception do
        begin
          //if MessageList.Count <> 0 then
          E.Message := CompanyDescr+ #13'stopped pre-processing with error message'#13#13+ E.Message;
          raise;
        end;
      end;
      LastProcessDate := EndDate;
      Assert(DM_COMPANY.CO.Locate('CO_NBR', CO_NBR, []));
      DM_COMPANY.CO.Edit;

      DM_COMPANY.CO.FieldByName('LAST_PREPROCESS').AsDateTime := LastProcessDate;
      DM_COMPANY.CO.FieldByName('LAST_PREPROCESS_MESSAGE').AsString := ErrorMessageString;

      DM_COMPANY.CO.Post;
      ctx_DataAccess.PayrollIsProcessing := True;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR, DM_COMPANY.CO, DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_SUI_LIABILITIES]);

      Result.AddValue('TimeStampFinished', Now);
      Result.AddValue('ErrorMessage', ErrorMessageString);
    end;

    AbortIfCurrentThreadTaskTerminated;

    Logs.Add('PreProcess Finished...     '+ FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "', Now));
    Result.AddValue('Log', Logs.CommaText);

    m := TisStream.Create;
    if PrintReports and FillPrList then
      m := PayrollsReports(CO_NBR, CL_NBR, PrList);

    Result.AddValue('TaxAdjReports', m);
  finally
    ctx_DataAccess.PayrollIsProcessing := False;
    Logs.free;
    ctx_EndWait;
  end;
end;

function TevRemoteMiscRoutines.PayTaxes(const FedTaxLiabilityList, StateTaxLiabilityList,
  SUITaxLiabilityList, LocalTaxLiabilityList: string; const ForceChecks: Boolean;
  const EftpReference: string; const SbTaxPaymentNbr: Integer; const PostProcessReportName: String): IisStream;
begin
  with STaxPaymentsProcedures.PayTaxesExt(FedTaxLiabilityList, StateTaxLiabilityList, SUITaxLiabilityList,
    LocalTaxLiabilityList, ForceChecks, EftpReference, SbTaxPaymentNbr ) do
  try
    Result := AsMemStream;
    Result.Position := 0;
  finally
    Free;
  end;
end;

function TevRemoteMiscRoutines.FinalizeTaxPayments(
  const Params: IisStream; const AACHOptions: IisStringList; const SbTaxPaymentNbr : Integer;
  const PostProcessReportName, DefaultMiscCheckForm: String): IisStream;
begin
  Result := STaxPaymentsProcedures.FinalizeTaxPayments(Params, AACHOptions, SbTaxPaymentNbr, PostProcessReportName, DefaultMiscCheckForm);
end;

function TevRemoteMiscRoutines.PrintChecksAndCouponsForTaxes(const TaxPayrollFilter, DefaultMiscCheckForm: string): TrwReportResults;
begin
  Result := STaxPaymentsProcedures.PrintChecksAndCouponsForTaxes(TaxPayrollFilter, DefaultMiscCheckForm);
end;


function TevRemoteMiscRoutines.CalculateChecklines(
  const Params: IisStream): IisStream;
var
  OutParams: IisStream;
  pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals: TevClientDataSet;
  i : integer;
  stream : IEVDualStream;

  procedure CalculateCL(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals: TevClientDataSet);
  var
    Nbr: Integer;
  begin
    Nbr := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger;
    ctx_StartWait;
    PR_CHECK_LINES.DisableControls;
    ctx_PayrollCalculation.AssignTempCheckLines(PR_CHECK_LINES);
    try
      DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired('ALL');
      DM_CLIENT.CL_PIECES.DataRequired('ALL');
      DM_COMPANY.CO_SHIFTS.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_RATES, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_WORK_SHIFTS, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);

      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.Eof do
      begin
        PR_CHECK_LINES.Edit;
        ctx_PayrollCalculation.CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_EMPLOYEE.EE_SCHEDULED_E_DS);
        if PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
          ctx_PayrollCalculation.CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_E_D_GROUP_CODES, DM_CLIENT.CL_PERSON,
            DM_CLIENT.CL_PIECES, DM_COMPANY.CO_SHIFTS, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
            DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_WORK_SHIFTS, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
            DM_SYSTEM_STATE.SY_STATES)
        else
        begin
          if not ((PR_CHECK_LINES.FieldByName('LINE_TYPE').AsString = CHECK_LINE_TYPE_USER)
          and (Abs(PR_CHECK_LINES.FieldByName('AMOUNT').AsCurrency) > 0.005)) then
          begin
            Assert(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []));
            PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
  //          if DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') <> ED_DIRECT_DEPOSIT then
              ctx_PayrollCalculation.ScheduledEDRecalc(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'),
                  PR_CHECK.FieldByName('GROSS_WAGES').AsFloat, PR_CHECK.FieldByName('NET_WAGES').AsFloat, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES,
                  PR_CHECK_SUI, PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES,
                  DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
                  DM_EMPLOYEE.EE_STATES, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
                  DM_SYSTEM_STATE.SY_STATES);
          end;
        end;
        PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundAll(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 2);
        PR_CHECK_LINES.Post;
        PR_CHECK_LINES.Next;
      end;
    finally
      if DM_PAYROLL.PR_CHECK_LINES.Active then
        ctx_PayrollCalculation.AssignTempCheckLines(DM_PAYROLL.PR_CHECK_LINES)
      else
        ctx_PayrollCalculation.ReleaseTempCheckLines;
      PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', Nbr, []);
      PR_CHECK_LINES.EnableControls;
      ctx_EndWait;
    end;
  end;

begin
  result := nil;

  pr_check := TevClientDataSet.Create(nil);
  pr_check_lines := TevClientDataSet.Create(nil);
  pr_check_states := TevClientDataSet.Create(nil);
  pr_check_sui := TevClientDataSet.Create(nil);
  pr_check_locals := TevClientDataSet.Create(nil);
  try
    Params.Position := 0;
    Params.ReadByte;
    i := Params.ReadInteger;
    Params.ReadByte;
    Params.ReadInteger;
    pr_check.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_lines.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_states.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_sui.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_locals.LoadFromUniversalStream(Params);

    preTaxCalculatorData(i, pr_check);

    CalculateCL(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals);

    OutParams := TisStream.Create;
    stream := PR_CHECK.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_LINES.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_LINES');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_STATES.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_STATES');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_SUI.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_SUI');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_LOCALS.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_LOCALS');
    OutParams.WriteStream(stream);
  finally
    pr_check.Free;
    pr_check_lines.Free;
    pr_check_states.Free;
    pr_check_sui.Free;
    pr_check_locals.Free;
  end;

  result := OutParams;
end;

procedure TevRemoteMiscRoutines.preTaxCalculatorData(i : integer; const  pr_check : TevClientDataSet);
begin
  DM_SYSTEM_STATE.SY_STATES.CheckDSCondition('');
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.CheckDSCondition('');
  DM_SYSTEM_STATE.SY_SUI.CheckDSCondition('');
  DM_CLIENT.CL_E_DS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_COMPANY.CO_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + IntToStr(i));
  DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + IntToStr(i));
  DM_COMPANY.CO_LOCAL_TAX.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + IntToStr(i));
  DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + IntToStr(i));

  DM_CLIENT.CL_PERSON.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_CLIENT.CL_PENSION.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_COMPANY.CO_E_D_CODES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + IntToStr(i));
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_EMPLOYEE.EE_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');

  ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES, DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, DM_SYSTEM_STATE.SY_SUI,
    DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PERSON, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_E_D_CODES,
    DM_COMPANY.CO_STATES, DM_COMPANY.CO_SUI, DM_COMPANY.CO_LOCAL_TAX,
    DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_STATES]);

  DM_PAYROLL.PR.DataRequired(AlwaysFalseCond);
  DM_PAYROLL.PR_BATCH.DataRequired(AlwaysFalseCond);
  DM_PAYROLL.PR_CHECK_LINE_LOCALS.DataRequired(AlwaysFalseCond);

  DM_PAYROLL.PR.Append;
  DM_PAYROLL.PR.PR_NBR.Value := pr_check.FieldByName('PR_NBR').AsInteger;
  DM_PAYROLL.PR.CHECK_DATE.Value := Date+1;
  DM_PAYROLL.PR.RUN_NUMBER.Value := 1;
  DM_PAYROLL.PR.PAYROLL_TYPE.Value := PAYROLL_TYPE_REGULAR;
//  DM_PAYROLL.PR.SCHEDULED.Value := GROUP_BOX_NO;
  DM_PAYROLL.PR.CO_NBR.Value := i;
  DM_PAYROLL.PR.Post;

  DM_PAYROLL.PR_BATCH.Append;
  DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.Value := pr_check.FieldByName('PR_BATCH_NBR').AsInteger;
  DM_PAYROLL.PR_BATCH.PR_NBR.Value := DM_PAYROLL.PR.PR_NBR.Value;
  with Context.PayrollCalculation do
  begin
    DM_PAYROLL.PR_BATCH.FREQUENCY.Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);//FREQUENCY_TYPE_WEEKLY;
    DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE.Value := GetNextPeriodBeginDate(DM_PAYROLL.PR_BATCH.FREQUENCY.AsString[1]);
    DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE_STATUS.Value := DATE_STATUS_NORMAL;
    DM_PAYROLL.PR_BATCH.PERIOD_END_DATE.Value := GetNextPeriodEndDate(DM_PAYROLL.PR_BATCH.FREQUENCY.AsString[1], DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE.Value);
    DM_PAYROLL.PR_BATCH.PERIOD_END_DATE_STATUS.Value := DATE_STATUS_NORMAL;
  end;
  DM_PAYROLL.PR_BATCH.Post;
end;

function TevRemoteMiscRoutines.CalculateTaxes(
  const Params: IisStream): IisStream;
var
  OutParams: IisStream;
  pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals: TevClientDataSet;
  i : integer;
  b, b1, b2 : Boolean;
  f: Double;
  stream: IEvDualStream;

  procedure checkZerosInNbrs(const ds: TevClientDataSet);
  var
    i: Integer;
  begin
    ds.First;
    while not ds.Eof do
    begin
      ds.Edit;
      for i := 0 to Pred(ds.FieldCount) do
        if (SameText(RightStr(ds.Fields[i].FieldName, 4), '_NBR')) and
           (ds.Fields[i].AsInteger = 0)  then
          ds.Fields[i].Clear;
      ds.CheckBrowseMode;
      ds.Next;
    end;
  end;

  procedure DeleteERSUI;
  var
    V: Variant;
  begin
    DM_COMPANY.CO_SUI.First;
    while not DM_COMPANY.CO_SUI.EOF do
    begin
      V := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DM_COMPANY.CO_SUI['SY_SUI_NBR'], 'EE_ER_OR_EE_OTHER_OR_ER_OTHER');
      if VarIsNull(V) or Is_ER_SUI(V) then
        DM_COMPANY.CO_SUI.Delete
      else
        DM_COMPANY.CO_SUI.Next;
    end;
  end;

  procedure CalculateCL(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals: TevClientDataSet);
  var
    Nbr: Integer;
  begin
    Nbr := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger;
    ctx_StartWait;
    PR_CHECK_LINES.DisableControls;
    ctx_PayrollCalculation.AssignTempCheckLines(PR_CHECK_LINES);
    try
      DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired('ALL');
      DM_CLIENT.CL_PIECES.DataRequired('ALL');
      DM_COMPANY.CO_SHIFTS.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_RATES, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_WORK_SHIFTS, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);

      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.Eof do
      begin
        PR_CHECK_LINES.Edit;
        ctx_PayrollCalculation.CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_EMPLOYEE.EE_SCHEDULED_E_DS);
        if PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
          ctx_PayrollCalculation.CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_E_D_GROUP_CODES, DM_CLIENT.CL_PERSON,
            DM_CLIENT.CL_PIECES, DM_COMPANY.CO_SHIFTS, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
            DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_WORK_SHIFTS, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
            DM_SYSTEM_STATE.SY_STATES)
        else
        begin
          if not ((PR_CHECK_LINES.FieldByName('LINE_TYPE').AsString = CHECK_LINE_TYPE_USER)
          and (Abs(PR_CHECK_LINES.FieldByName('AMOUNT').AsCurrency) > 0.005)) then
          begin
            Assert(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []));
            PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
  //          if DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') <> ED_DIRECT_DEPOSIT then
              ctx_PayrollCalculation.ScheduledEDRecalc(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'),
                  PR_CHECK.FieldByName('GROSS_WAGES').AsFloat, PR_CHECK.FieldByName('NET_WAGES').AsFloat, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES,
                  PR_CHECK_SUI, PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES,
                  DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
                  DM_EMPLOYEE.EE_STATES, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
                  DM_SYSTEM_STATE.SY_STATES);
          end;
        end;
        PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundAll(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 2);
        PR_CHECK_LINES.Post;
        PR_CHECK_LINES.Next;
      end;
    finally
      if DM_PAYROLL.PR_CHECK_LINES.Active then
        ctx_PayrollCalculation.AssignTempCheckLines(DM_PAYROLL.PR_CHECK_LINES)
      else
        ctx_PayrollCalculation.ReleaseTempCheckLines;
      PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', Nbr, []);
      PR_CHECK_LINES.EnableControls;
      ctx_EndWait;
    end;
  end;

  procedure CalculateTax(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals, pr_check_line_locals: TevClientDataSet;
                         const disableYTD, disableShortfalls, netToGross: boolean; const net: Double);
  var
    RegularClientED: Integer;
    AmountHigh, NetHigh: Double;
    AmountLow, NetLow: Double;
    AmountOrig: Double;
    InBracket: Boolean;
    direction: integer;
    counter: integer;
  begin
    RegularClientED := 0;

    PR_CHECK.CheckBrowseMode;
    PR_CHECK_LINES.CheckBrowseMode;
    PR_CHECK_STATES.CheckBrowseMode;
    PR_CHECK_SUI.CheckBrowseMode;
    PR_CHECK_LOCALS.CheckBrowseMode;

    PR_CHECK_STATES.First;
    while not PR_CHECK_STATES.EOF do
    begin
      if not PR_CHECK_STATES.FieldByName('STATE_TAX').IsNull then
      begin
        PR_CHECK_STATES.Edit;
        PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := PR_CHECK_STATES['STATE_TAX'];
        PR_CHECK_STATES['STATE_TAX'] := 0;
        PR_CHECK_STATES.Post;
      end;
      PR_CHECK_STATES.Next;
    end;

    ctx_StartWait;
    ctx_PayrollCalculation.AssignTempCheckLines(PR_CHECK_LINES);
    try
      if netToGross then
      begin
        try
          RegularClientED := DM_CLIENT.CL_E_DS.Lookup('E_D_CODE_TYPE', ED_OEARN_REGULAR, 'CL_E_DS_NBR');
        except
          raise EInconsistentData.CreateHelp('Regular earning is not setup properly for the company! Can not calculate check.', IDH_InconsistentData);
        end;
        if not PR_CHECK_LINES.Locate('CL_E_DS_NBR', RegularClientED, []) then
        begin
          PR_CHECK_LINES.Insert;

  {TODO?  it happens in TevClientDataSet.DoOnNewRecord

          GlobalNameSpace.BeginWrite;
          try
            Dec(NewKeyValue);
            PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value := NewKeyValue;
          finally
            GlobalNameSpace.EndWrite;
          end;
  }

          PR_CHECK_LINES['PR_NBR'] := PR_CHECK['PR_NBR'];
          PR_CHECK_LINES['PR_CHECK_NBR'] := PR_CHECK['PR_CHECK_NBR'];
          PR_CHECK_LINES['CL_E_DS_NBR'] := RegularClientED;
          PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
          PR_CHECK_LINES['USER_OVERRIDE'] := GROUP_BOX_NO;
          PR_CHECK_LINES['AMOUNT'] := net * 2;
  //for WebClient: Assigned default value 'N'
          PR_CHECK_LINES['REDUCED_HOURS'] := GROUP_BOX_NO;
          PR_CHECK_LINES.Post;
        end;
      end;

      DM_COMPANY.CO_SUI.Close;
      DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_SUI]);
      try
        ctx_PayrollCalculation.GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
          PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, disableYTD, disableShortfalls, NetToGross);
      finally
        DeleteERSUI;
      end;

      DM_COMPANY.CO_SUI.Close;
      DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_SUI]);
      if netToGross then
      begin
        try
          PR_CHECK_LINES.Locate('CL_E_DS_NBR', RegularClientED, []);
          NetHigh := PR_CHECK.FieldByName('NET_WAGES').Value;
          NetLow := PR_CHECK.FieldByName('NET_WAGES').Value;
          AmountHigh := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          AmountLow := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          AmountOrig := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          direction := 0;
          counter := 0;
          InBracket := True;
          while Abs(net - PR_CHECK.FieldByName('NET_WAGES').AsFloat) >= 0.005 do
          begin
            PR_CHECK_LINES.Locate('CL_E_DS_NBR', RegularClientED, []);
            PR_CHECK_LINES.Edit;
            if direction < 0 then
              if not InBracket then
              begin
                AmountLow := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetLow := PR_CHECK.FieldByName('NET_WAGES').Value;
              end
              else
              begin
                AmountHigh := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetHigh := PR_CHECK.FieldByName('NET_WAGES').Value;
              end
            else if direction > 0 then
              if not InBracket then
              begin
                AmountHigh := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetHigh := PR_CHECK.FieldByName('NET_WAGES').Value;
              end
              else
              begin
                AmountLow := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetLow := PR_CHECK.FieldByName('NET_WAGES').Value;
              end;

            direction := 0;
            InBracket := True;
            if Abs(net - PR_CHECK.FieldByName('NET_WAGES').AsFloat) < 0.025 then
            begin
              PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat + net - PR_CHECK.FieldByName('NET_WAGES').Value;
              counter := counter + 1;
              if counter > 5 then
                EevException.Create('There is no Regular Earning amount that will produce the ' + FloatToStrF(Net, ffFixed, 15, 2) + ' net amount. In order to achieve this amount, you can adjust and then plug taxes.');
            end
            else
            begin
              InBracket := (Net <= NetHigh) and (Net >= NetLow);
              if not InBracket and (Net > NetHigh) or
                 InBracket and (Net >= (NetHigh + NetLow) / 2) then
                direction := 1
              else if not InBracket and (Net < NetLow) or
                 InBracket and (Net < (NetHigh + NetLow) / 2) then
                direction := -1;

              if not InBracket then
              begin
                if (NetHigh = NetLow) and (AmountOrig = 0) then
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat + direction * RoundTwo(Abs(NetHigh - Net))
                else
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat + direction * (RoundTwo(Abs(NetHigh - NetLow)) + AmountOrig);
                AmountOrig := 0;
              end
              else
                PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo((AmountHigh + AmountLow) / 2);
            end;
            PR_CHECK_LINES.Post;
            PR_CHECK_STATES.First;
            while not PR_CHECK_STATES.EOF do
            begin
              PR_CHECK_STATES.Edit;
              PR_CHECK_STATES.FieldByName('STATE_TAX').Value := 0;
              PR_CHECK_STATES.Post;
              PR_CHECK_STATES.Next;
            end;
            ctx_PayrollCalculation.GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
              PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, disableYTD, disableShortfalls, netToGross);
          end;
        finally
          DeleteERSUI;
        end;
      end;
    finally
      if DM_PAYROLL.PR_CHECK_LINES.Active then
        ctx_PayrollCalculation.AssignTempCheckLines(DM_PAYROLL.PR_CHECK_LINES)
      else
        ctx_PayrollCalculation.ReleaseTempCheckLines;
      ctx_EndWait;
    end;
  end;

begin
  result := nil;

  pr_check := TevClientDataSet.Create(nil);
  pr_check.ProviderName := DM_PAYROLL.PR_CHECK.ProviderName;

  pr_check_lines := TevClientDataSet.Create(nil);
  pr_check_lines.ProviderName := DM_PAYROLL.PR_CHECK_LINES.ProviderName;

  pr_check_states := TevClientDataSet.Create(nil);
  pr_check_states.ProviderName := DM_PAYROLL.PR_CHECK_STATES.ProviderName;

  pr_check_sui := TevClientDataSet.Create(nil);
  pr_check_sui.ProviderName := DM_PAYROLL.PR_CHECK_SUI.ProviderName;

  pr_check_locals := TevClientDataSet.Create(nil);
  pr_check_locals.ProviderName := DM_PAYROLL.PR_CHECK_LOCALS.ProviderName;

  try
    Params.Position := 0;
    Params.ReadByte;
    i := Params.ReadInteger;
    Params.ReadByte;
    Params.ReadInteger;
    pr_check.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_lines.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_states.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_sui.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_locals.LoadFromUniversalStream(Params);

    Params.ReadByte;
    b := Params.ReadBoolean;
    Params.ReadByte;
    b1 := Params.ReadBoolean;
    Params.ReadByte;
    b2 := Params.ReadBoolean;
    Params.ReadByte;
    f := Params.ReadDouble;

    checkZerosInNbrs(pr_check);
    checkZerosInNbrs(pr_check_lines);
    checkZerosInNbrs(pr_check_states);
    checkZerosInNbrs(pr_check_sui);
    checkZerosInNbrs(pr_check_locals);

    preTaxCalculatorData(i, pr_check);
    CalculateCL(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals);
    CalculateTax(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states,
                 pr_check_sui, pr_check_locals, DM_PAYROLL.PR_CHECK_LINE_LOCALS, b, b1, b2, f);

    if b2 then
       CalculateTax(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states,
                 pr_check_sui, pr_check_locals, DM_PAYROLL.PR_CHECK_LINE_LOCALS, b, b1, False, 0);
    if f <= 0.0 then
    begin
      CalculateCL(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals);
      CalculateTax(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states,
                  pr_check_sui, pr_check_locals, DM_PAYROLL.PR_CHECK_LINE_LOCALS, b, b1, b2, f);
    end;

    if pr_check_locals.Active then
    begin
      pr_check_locals.first;
      while not pr_check_locals.eof do
      begin
        if (pr_check_locals.FieldByName('PR_Check_nbr').asInteger <= 0)
           or (pr_check_locals.FieldByName('PR_nbr').asInteger <= 0) then
        begin
          pr_check_locals.edit;
          pr_check_locals.FieldByName('PR_nbr').asInteger := pr_check.FieldByName('PR_nbr').asInteger;
          pr_check_locals.FieldByName('PR_Check_nbr').asInteger := pr_check.FieldByName('PR_Check_nbr').asInteger;
          pr_check_locals.post;
        end;
        pr_check_locals.next;
      end;
    end;

    OutParams := TisStream.Create;
    stream := PR_CHECK.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_LINES.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_LINES');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_STATES.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_STATES');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_SUI.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_SUI');
    OutParams.WriteStream(stream);
    stream := PR_CHECK_LOCALS.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_LOCALS');
    OutParams.WriteStream(stream);
  finally
    pr_check.Free;
    pr_check_lines.Free;
    pr_check_states.Free;
    pr_check_sui.Free;
    pr_check_locals.Free;
  end;

  result := OutParams;
end;
function TevRemoteMiscRoutines.ChangeCheckStatus(const Params: IisStream): IisStream;
var
  OutParams: IisStream;
  i : integer;
  s : string;
begin
  result := nil;

  Params.Position := 0;
  Params.ReadByte;
  i := Params.ReadInteger;
  Params.ReadByte;
  s := Params.ReadString;

  DM_COMPANY.PR_CHECK.DataRequired('PR_CHECK_NBR=' + IntToStr(i));
  DM_COMPANY.PR.DataRequired('PR_NBR=' + IntToStr(DM_COMPANY.PR_CHECK.PR_NBR.Value));
  ctx_DataAccess.UnlockPR;
  try
    DM_COMPANY.PR_CHECK.Edit;
    DM_COMPANY.PR_CHECK.CHECK_STATUS.Value := s;
    DM_COMPANY.PR_CHECK.Post;
    ctx_DataAccess.PostDataSets([DM_COMPANY.PR_CHECK]);
  finally
    ctx_DataAccess.LockPR(True);
  end;

  result := OutParams;
end;

function TevRemoteMiscRoutines.GetW2(const Params: IisStream; const param : String): IisStream;
var
  OutParams: IisStream;
  stream : IEvDualStream;
  reportResults: TrwReportResults;
  PDFInfoRec: TrwPDFDocInfo;
  Nbr: string;

  function CheckEENBR(const EENBR:integer):boolean;
  var
    Q: IEvQuery;
  begin
    Q := TEvQuery.Create('select  EE_NBR from EE b, ' +
                   '( Select CL_PERSON_NBR from  EE a ' +
                       ' where {AsOfNow<a>} and EE_NBR = :P_EeNbr) a1 ' +
            ' where  b.CL_PERSON_NBR = a1.cl_person_nbr and {AsOfNow<b>} ');
    Q.Params.AddValue('P_EeNbr', Abs(Context.UserAccount.InternalNbr));
    Q.Execute;
    Result:= Q.Result.Locate('EE_NBR', EENBR, []);
  end;

begin
  result := nil;

  // negative numbers are passed as parameter from the EvoInfinityHR to get round CheckEENBR
  if Copy(Param, 1, 1) = '-' then
    Nbr := Copy(Param, 2, Length(Param))
  else
    Nbr := Param;

  DM_COMPANY.CO_TAX_RETURN_RUNS.DataRequired('CO_TAX_RETURN_RUNS_NBR=' + Nbr);
  Assert(DM_COMPANY.CO_TAX_RETURN_RUNS.RecordCount = 1, 'ClientID = ' + IntToStr(ctx_DataAccess.ClientID) + ', Nbr = ' + Nbr + ', DM_COMPANY.CO_TAX_RETURN_RUNS.RecordCount = ' + IntToStr(DM_COMPANY.CO_TAX_RETURN_RUNS.RecordCount));

  if CheckEENBR(DM_COMPANY.CO_TAX_RETURN_RUNS.fieldByName('EE_NBR').AsInteger) or (Nbr <> Param) then
  begin
    stream := DM_COMPANY.CO_TAX_RETURN_RUNS.GetBlobData('CL_BLOB_NBR');
    Assert(stream.ReadInteger = $FF);
    stream := UnCompressStreamFromStream(stream);
    stream.Position := 0;
    reportResults := TrwReportResults.Create(stream);
    with reportResults.Items[0] do
    try
      if ErrorMessage = '' then
      begin
        with PDFInfoRec do
        begin
          Author := 'Evolution Report Writer';
          Creator := 'Evolution Report Engine';
          Subject := '';
          Title := ReportName;
          Compression := True;
          ProtectionOptions := [];
          OwnerPassword := '';
          UserPassword := '';
        end;
        OutParams := Context.RWLocalEngine.ConvertRWAtoPDF(Data, PDFInfoRec)
      end
      else
        OutParams := TisStream.Create;
    finally
      reportResults.Free;
    end;
  end
  else
        OutParams := TisStream.Create;
  result := OutParams;
end;


function TevRemoteMiscRoutines.HolidayDay(
  const Params: IisStream): IisStream;
var
  OutParams: IisStream;
  d : TDateTime;
  s : string;
begin
  result := nil;

  Params.Position := 0;
  Params.ReadByte;
  d := Params.ReadDouble;
  s := Context.PayrollCalculation.HolidayDay(d);
  OutParams := TisStream.Create;
  OutParams.WriteByte(Byte(ftString));
  OutParams.WriteString(s);

  result := OutParams;
end;

function TevRemoteMiscRoutines.NextPayrollDates(
  const Params: IisStream): IisStream;
var
  OutParams: IisStream;
  i : integer;
  d, d1, d2 : TDateTime;
  b, b1 : boolean;
  s, s1 : string;
begin
  result := nil;

  Params.Position := 0;
  Params.ReadByte;
  i := Params.ReadInteger;
  Params.ReadByte;
  d1 := Params.ReadDouble;
  DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(i));
  DM_COMPANY.PR.DataRequired(AlwaysFalseCond);
  d := Context.PayrollCalculation.NextCheckDate(d1, True);
  if d = 0 then
    raise ENoNextCheckDate.CreateHelp('There are no entries in the calendar after ' + DateToStr(d1) + '. Please, update it.', IDH_InconsistentData);

  b1 := ctx_DataAccess.TempCommitDisable;
  try
    ctx_DataAccess.TempCommitDisable := True;
    DM_PAYROLL.PR.Append;
    DM_PAYROLL.PR.CHECK_DATE.Value := d;
    DM_PAYROLL.PR_SCHEDULED_EVENT.Edit;
    DM_PAYROLL.PR_SCHEDULED_EVENT.PR_NBR.Value := DM_PAYROLL.PR.PR_NBR.Value;
    DM_PAYROLL.PR_SCHEDULED_EVENT.Post;
    b := Context.PayrollCalculation.FindNextPeriodBeginEndDate;
  finally
    DM_PAYROLL.PR.Cancel;
    DM_PAYROLL.PR_SCHEDULED_EVENT.CancelUpdates;
    ctx_DataAccess.TempCommitDisable := b1;
  end;
  if DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecordCount > 0 then
  begin
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
    with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
    try
      s := Strings[0];
    finally
      Free;
    end;
  end
  else
  begin
    s1 := Context.PayrollCalculation.GetCompanyPayFrequencies;
    s := Copy(s1, Pos(#9, s1) + 1, 1);
  end;

  if not b then
  begin
    d1 := Context.PayrollCalculation.GetNextPeriodBeginDate(s[1]);
    d2 := Context.PayrollCalculation.GetNextPeriodEndDate(s[1], d1);
  end
  else
  begin
    d1 := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE'];
    d2 := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE'];
  end;

  OutParams := TisStream.Create;
  OutParams.WriteByte(Byte(ftDate));
  OutParams.WriteDouble(d);
  OutParams.WriteByte(Byte(ftDate));
  OutParams.WriteDouble(d1);
  OutParams.WriteByte(Byte(ftDate));
  OutParams.WriteDouble(d2);
  OutParams.WriteByte(Byte(ftString));
  OutParams.WriteString(s);

  result := OutParams;
end;

function TevRemoteMiscRoutines.RefreshEDS(
  const Params: IisStream): IisStream;
var
  OutParams: IisStream;
  pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals: TevClientDataSet;
  i : integer;
  stream : IEvDualStream;
begin
  result := nil;

  pr_check := TevClientDataSet.Create(nil);
  pr_check_lines := TevClientDataSet.Create(nil);
  pr_check_states := TevClientDataSet.Create(nil);
  pr_check_sui := TevClientDataSet.Create(nil);
  pr_check_locals := TevClientDataSet.Create(nil);
  try
    Params.Position := 0;
    Params.ReadByte;
    i := Params.ReadInteger;
    Params.ReadByte;
    Params.ReadInteger;
    pr_check.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_lines.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_states.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_sui.LoadFromUniversalStream(Params);
    Params.ReadByte;
    Params.ReadInteger;
    pr_check_locals.LoadFromUniversalStream(Params);

    preTaxCalculatorData(i, pr_check);

    PopulateScheduledEDs(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals, DM_PAYROLL.PR_CHECK_LINE_LOCALS);

    pr_check_lines.First;
    while not pr_check_lines.Eof do
    begin
      pr_check_lines.Edit;
      pr_check_lines.FieldByName('PR_CHECK_LINES_NBR').Value := ctx_DBAccess.GetNewKeyValue('PR_CHECK_LINE');
      pr_check_lines.FieldByName('PR_CHECK_NBR').Value := pr_check.FieldByName('PR_CHECK_NBR').Value;
      pr_check_lines.FieldByName('PR_NBR').Value := pr_check.FieldByName('PR_NBR').Value;
      PR_CHECK_LINES.FieldByName('USER_OVERRIDE').Value := GROUP_BOX_NO;
// WebClient: assign default value for 'REDUCED_HOURS'
      if PR_CHECK_LINES.FieldByName('REDUCED_HOURS').IsNull then
        PR_CHECK_LINES.FieldByName('REDUCED_HOURS').Value := GROUP_BOX_NO;
      pr_check_lines.Post;
      pr_check_lines.Next;
    end;

    OutParams := TisStream.Create;
    stream := PR_CHECK_LINES.GetDataStream(False, False);
    OutParams.WriteByte(Byte(ftDataSet));
    OutParams.WriteString('PR_CHECK_LINES');
    OutParams.WriteStream(stream);
  finally
    pr_check.Free;
    pr_check_lines.Free;
    pr_check_states.Free;
    pr_check_sui.Free;
    pr_check_locals.Free;
  end;

  result := OutParams;
end;

function TevRemoteMiscRoutines.TCImport(const Params: IisStream): IisStream;
const
  LookupOption = 'LookupOption';
  DBDTOption = 'DBDTOption';
  FourDigitYear = 'FourDigitYear';
  FileFormat = 'FileFormat';
  AutoImportJobCodes = 'AutoImportJobCodes';
  UseEmployeePayRates = 'UseEmployeePayRates';
  RejectedRecords = 'RejectedRecords';
  LogFile = 'LogFile';
  PrNbr = 'PrNbr';
  CompanyId = 'CompanyId';
  PayrollId = 'PayrollId';
  ClientNbr = 'ClientNbr';
  BatchNbr  = 'BatchNbr';
var
  OutParams: IisStream;
  FileIn, FileOut : IisStream;
  pLookupOption : TCImportLookupOption;
  pDBDTOption : TCImportDBDTOption;
  pFourDigitYear : Boolean;
  pFileFormat : TCFileFormatOption;
  pAutoImportJobCodes : Boolean;
  pUseEmployeePayRates : boolean;
  pCompanyId : integer;
  pPayrollId : integer;
  pClientNbr : integer;
  pBatchNbr  :integer;

  InputParams : IisListOfValues;
  tmpInteger : integer;
  RejectedRecordsAmount : integer;
  ResultLisOfValues : IisListOfValues;
begin
  result := nil;
  OutParams := TisStream.Create;

  FileOut := TisStream.Create;
  Params.Position := 0;
  Filein := Params.ReadStream(nil); // reading import data
  InputParams := TisListOfValues.Create;
  InputParams.ReadFromStream(Params);
  // translating import options
  tmpInteger := InputParams.GetValueByName(LookupOption);;
  case tmpInteger of
    0 : pLookupOption := loByName;
    1 : pLookupOption := loByNumber;
    2 : pLookupOption := loBySSN;
    else
      raise EevException.Create('Wrong value for "' + LookupOption + '" parameter.');
  end;

  tmpInteger := InputParams.GetValueByName(DBDTOption);
  case tmpInteger of
    0 : pDBDTOption := loFull;
    1 : pDBDTOption := loSmart;
    else
      raise EevException.Create('Wrong value for "' + DBDTOption + '" parameter.');
  end;

  pFourDigitYear := InputParams.GetValuebyName(FourDigitYear);

  tmpInteger := InputParams.GetValueByName(FileFormat);
  case tmpInteger of
    0 : pFileFormat := loFixed;
    1 : pFileFormat := loCommaDelimited;
    else
      raise EevException.Create('Wrong value for "' + FileFormat + '" parameter.');
  end;

  pAutoImportJobCodes := InputParams.GetValuebyName(AutoImportJobCodes);
  pUseEmployeePayRates := InputParams.GetValuebyName(UseEmployeePayRates);
  pCompanyId := InputParams.GetValueByName(CompanyId);
  pPayrollId := InputParams.GetValueByName(PayrollId);
  pClientNbr := InputParams.GetValueByName(ClientNbr);
  pBatchNbr := InputParams.GetValueByName(BatchNbr);
  // opening and filling datasets
  ctx_DataAccess.OpenClient(pClientNbr);
  try
    DM_EMPLOYEE.CO.DataRequired('CO_NBR='+ IntToStr(pCompanyId));
    DM_EMPLOYEE.CO_STATES.DataRequired('CO_NBR='+ IntToStr(pCompanyId));
    DM_EMPLOYEE.CO_JOBS.DataRequired('CO_NBR='+ IntToStr(pCompanyId));
    DM_EMPLOYEE.CO_SHIFTS.DataRequired('CO_NBR='+ IntToStr(pCompanyId));

    DM_EMPLOYEE.EE.DataRequired('CO_NBR='+ IntToStr(pCompanyId));

    DM_PAYROLL.PR.DataRequired('PR_NBR='+ IntToStr(pPayrollId));

    PopulateDataSets([DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM], '');
    ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM]);

    PopulateDataSets([DM_CLIENT.CL_PERSON, DM_CLIENT.CL_E_DS], '');
    ctx_DataAccess.OpenDataSets([DM_CLIENT.CL_PERSON, DM_CLIENT.CL_E_DS]);

    PopulateDataSets([DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_RATES], '');
    ctx_DataAccess.OpenDataSets([DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_RATES]);

    DM_PAYROLL.PR_BATCH.DataRequired('PR_NBR='+ IntToStr(pPayrollId));
    DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR='+ IntToStr(pPayrollId));
    DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR=-1');

    try
      RejectedRecordsAmount := ProcessTCImport(FileIn, FileOut, pLookupOption, pDBDTOption, pFourDigitYear,
        pFileFormat, pAutoImportJobCodes, pUseEmployeePayRates, true,False,pBatchNbr);

      ResultLisOfValues := TisListOfValues.Create;
      ResultLisOfValues.AddValue(RejectedRecords, RejectedRecordsAmount);
      ResultLisOfValues.AddValue(LogFile, FileOut);
      ResultLisOfValues.WriteToStream(OutParams);

      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES ]);
    except
      ctx_DataAccess.CancelDataSets([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_BATCH, DM_COMPANY.CO_JOBS]);
      raise;
    end;
  finally
    DM_COMPANY.CO_DIVISION.Close;
    DM_COMPANY.CO_BRANCH.Close;
    DM_COMPANY.CO_DEPARTMENT.Close;
    DM_COMPANY.CO_TEAM.Close;
    DM_COMPANY.CO_JOBS.Close;
    DM_COMPANY.CO_SHIFTS.Close;
    DM_COMPANY.CO_STATES.Close;
    DM_COMPANY.CO.Close;

    DM_CLIENT.CL_PERSON.Close;
    DM_CLIENT.CL_E_DS.Close;

    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Close;
    DM_EMPLOYEE.EE_STATES.Close;
    DM_EMPLOYEE.EE_RATES.Close;
    DM_EMPLOYEE.EE.Close;

    DM_PAYROLL.PR_CHECK_LINES.Close;
    DM_PAYROLL.PR_CHECK.Close;
    DM_PAYROLL.PR_BATCH.Close;
    DM_PAYROLL.PR.Close;
  end;

  result := OutParams;
end;

function TevRemoteMiscRoutines.ReportToPDF(const Params: IisStream): IisStream;
var
  PDFInfoRec: TrwPDFDocInfo;
  ReportResults: TrwReportResults;
  sRepName: String;
  Tsk: IevTask;
  tmpId : TisGUID;
  Res: IevDualStream;
begin
  Result := nil;

  if Assigned(Params) then
  begin
    Params.Position := 0;
    tmpId := Params.ReadString;

    Tsk := mb_TaskQueue.GetTask(tmpId);
    if (Tsk.Exceptions = '') and Supports(Tsk, IevPrintableTask) then
    begin
      Res := (Tsk as IevPrintableTask).ReportResults;
      // check for ticket 104097 - to provide empty output for connection to evoHCM site in case if stored report was empty; otherwise nasty excepion
      if Assigned(Res) then
        Res.Position := 0
      else
        Exit;

      ReportResults := TrwReportResults.Create(Res);
      try
        if ReportResults[0].ErrorMessage <> '' then
          raise EevException.Create('Report finished with errors: ' + ReportResults[0].ErrorMessage);
        Result := ReportResults[0].Data;
        sRepName := ReportResults[0].ReportName;
      finally
        ReportResults.Free;
      end;
      with PDFInfoRec do
      begin
         Author := 'Evolution Report Writer';
         Creator := 'Evolution Report Engine';
         Subject := '';
         Title := sRepName;
         Compression := True;
         ProtectionOptions := [];
         OwnerPassword := '';
         UserPassword := '';
       end;
       Result.Position := 0;
       Result := ctx_RWLocalEngine.ConvertRWAtoPDF(Result, PDFInfoRec);
    end
    else
      raise EevException.Create('Report calculated with exceptions or it is not printable task. ' + Tsk.Exceptions);
  end
  else
    raise EevException.Create('Input stream is not assigned');
end;

function TevRemoteMiscRoutines.AllReportsToPDF(const Params: IisStream): IevDataset;
const
  dsStruct: array [0..5] of TDSFieldDef = (
            (FieldName: 'NBR'; DataType: ftInteger;  Size: 0;   Required: True),
            (FieldName: 'Level'; DataType: ftString;   Size: 1;   Required: True),
            (FieldName: 'ReportName'; DataType: ftString;  Size: 128;   Required: True),
            (FieldName: 'Data'; DataType: ftBlob;  Size: 0;   Required: false),
            (FieldName: 'ErrorMessage'; DataType: ftString;  Size: 1024;   Required: false),
            (FieldName: 'WarningMessage'; DataType: ftString;  Size: 1024;   Required: false) );
var
  PDFInfoRec: TrwPDFDocInfo;
  ReportResults: TrwReportResults;
  i: integer;
  data: IisStream;
  tmpBlobField: TBlobField;
begin
  Result := nil;
  if Assigned(Params) then
  begin
    Result := TevDataSet.Create(dsStruct);

    Params.Position := 0;
    ReportResults := TrwReportResults.Create(Params);
    try
      for i := 0 to ReportResults.Count - 1 do
      begin
         Result.Append;
         Result.FieldByName('NBR').AsInteger := ReportResults.Items[i].NBR;
         Result.FieldByName('Level').AsString := ReportResults.Items[i].Level;
         Result.FieldByName('ReportName').AsString := ReportResults.Items[i].ReportName;
         Result.FieldByName('ErrorMessage').AsString := ReportResults.Items[i].ErrorMessage;
         Result.FieldByName('WarningMessage').AsString := ReportResults.Items[i].WarningMessage;

         if Assigned(ReportResults[i].Data) then
         begin
          if not IsRWAFormat(ReportResults[i].Data) or (ReportResults.Items[i].ReportType = rtASCIIFile) then  // Can not convert ASCII files to PDF format
          begin
            data := ReportResults[i].Data;
            data.Position := 0;
            tmpBlobField := TBlobField(Result.FieldByName('Data'));
            tmpblobField.LoadFromStream(data.realStream);
          end
          else
          begin
            with PDFInfoRec do
            begin
               Author := 'Evolution Report Writer';
               Creator := 'Evolution Report Engine';
               Subject := '';
               Title := ReportResults.Items[i].ReportName;
               Compression := True;
               ProtectionOptions := [];
               OwnerPassword := '';
               UserPassword := '';
             end;

            data := ReportResults[i].Data;
            data.Position := 0;
            data := ctx_RWLocalEngine.ConvertRWAtoPDF(data, PDFInfoRec);
            tmpBlobField := TBlobField(Result.FieldByName('Data'));
            data.Position := 0;
            tmpblobField.LoadFromStream(data.realStream);
          end;
         end;

         Result.Post;
      end;
    finally
      ReportResults.Free;
    end;
  end
  else
    raise EevException.Create('Input stream is not assigned');

end;

function TevRemoteMiscRoutines.GetPDFReport(
  const aTaskId: string): IisStream;
var
  TskInf: IevTaskInfo;
  Params: IevDualStream;
  tmpId : TisGUID;
begin
  Result := nil;
  tmpId := TaskNbrToId(aTaskId);
  TskInf := mb_TaskQueue.GetTaskInfo(tmpId);

  if not Assigned(TskInf) then
    raise EevException.Create('Queue Task Not Found.');

  if TskInf.State <> tsFinished then
    raise EevException.Create('Queue Task Not Finished.');

  if Assigned(TskInf) and (TskInf.State = tsFinished) then
  begin
    Params := TisStream.Create;
    Params.WriteString(tmpId);
    Result := Context.RemoteMiscRoutines.ReportToPDF(Params);
  end;

  if not Assigned(Result) then
    Result := TEvDualStreamHolder.Create;
end;

function TevRemoteMiscRoutines.GetAllPDFReports(const aTaskId: string): IevDataset;
var
  TskInf: IevTaskInfo;
  Params: IevDualStream;
  tmpId : TisGUID;
  Tsk: IevTask;

begin
  Result := nil;
  tmpId := TaskNbrToId(aTaskId);
  TskInf := mb_TaskQueue.GetTaskInfo(tmpId);

  if not Assigned(TskInf) then
    raise EevException.Create('Queue Task Not Found.');

  if TskInf.State <> tsFinished then
    raise EevException.Create('Queue Task Not Finished.');

  if Assigned(TskInf) and (TskInf.State = tsFinished) then
  begin
    Tsk := mb_TaskQueue.GetTask(tmpId);
    if Supports(Tsk, IevPrintableTask) then
    begin
      Params := (Tsk as IevPrintableTask).ReportResults;
      if Assigned(Params) then
      begin
        Params.Position := 0;
        Result := AllReportsToPDF(Params);
      end;
    end
  end;
end;


function TevRemoteMiscRoutines.GetTaskList: IEvDataSet;
const
  DSStruct: array [0..14] of TDSFieldDef = (
            (FieldName: 'Id';           DataType: ftInteger;  Size: 0;   Required: False),
            (FieldName: 'ClassName';    DataType: ftString;   Size: 255;    Required: False),
            (FieldName: 'Description';  DataType: ftString;   Size: 128;  Required: False),
            (FieldName: 'Caption';      DataType: ftString;   Size: 255;  Required: False),
            (FieldName: 'Status';       DataType: ftString;   Size: 255;  Required: False),
            (FieldName: 'Finished';     DataType: ftString;   Size: 1;    Required: False),
            (FieldName: 'Seen';         DataType: ftBoolean;  Size: 0;    Required: False),
            (FieldName: 'TimeStamp';    DataType: ftDateTime; Size: 0;    Required: False),
            (FieldName: 'Priority';     DataType: ftInteger;  Size: 0;    Required: False),
            (FieldName: 'Requests';     DataType: ftMemo;     Size: 0;    Required: False),
            (FieldName: 'UseEmail';     DataType: ftBoolean;  Size: 0;    Required: False),
            (FieldName: 'EMail';        DataType: ftString;   Size: 100;  Required: False),
            (FieldName: 'Message';      DataType: ftString;   Size: 255;  Required: False),
            (FieldName: 'UserName';     DataType: ftString;   Size: 128;  Required: False),
            (FieldName: 'SbId';         DataType: ftString;   Size: 128;  Required: False));

var
  TaskInf: IevTaskInfo;
  TaskList: IisListOfValues;
  i: Integer;
  sDomain, sUser : String;

  function GetTaskState: String;
  begin
    case TaskInf.State of
      tsInactive:  Result := 'Queued';
      tsWaiting:   Result := 'Waiting for resources';
      tsExecuting: Result := 'Executing';
      tsFinished:  Result := TaskInf.ProgressText;
    end;
  end;

  function GetTaskProgressText: String;
  begin
    if TaskInf.State = tsFinished then
      Result := ''
    else
      Result := TaskInf.ProgressText;
  end;

begin
  Result := TevDataSet.Create(DSStruct);
  TaskList := mb_TaskQueue.GetUserTaskInfoList;
  sDomain := Context.UserAccount.Domain;
  sUser := Context.UserAccount.User;

  for i := 0 to TaskList.Count - 1 do
  begin
    TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
    with TaskInf do
      if  AnsiSameText(sDomain, TaskInf.GetDomain) and AnsiSameText(sUser, TaskInf.GetUser) then
        Result.AppendRecord([Nbr, TaskTypeToMethodName(TaskType), TaskType, Caption, GetTaskState,
                         Iff(State = tsFinished, 'Y', 'N'),  SeenByUser,
                         LastUpdate, Priority, '', SendEmailNotification, NotificationEmail,
                         GetTaskProgressText, User, '']);
  end;

end;

function TevRemoteMiscRoutines.RunReport(const aReportNbr: integer;
  const AParams: IisListOfValues): String;
var
  ReportParams: TrwReportParams;
  sParamName, sReportName, sRepLevel: string;
  i, j: Integer;
  sTaskID: TisGUID;
  v, pList: Variant;
  vType, vCount: Integer;

  function TaskIdToNbr(const ATaskId: TisGUID): String;
  var
    TaskList: IisListOfValues;
  begin
    TaskList := mb_TaskQueue.GetUserTaskInfoList;
    if TaskList.ValueExists(ATaskId) then
      result := IntToStr((IInterface(TaskList.Value[ATaskId]) as IevTaskInfo).Nbr)
    else
      raise EevException.Create('Cannot Convert Task Id To Nbr');
  end;

begin
  ReportParams := TrwReportParams.Create;
  try
    for i := 0 to AParams.Count -1 do
    begin
      sParamName := AParams.Values[i].Name;
      vType := VarType(AParams.Values[i].Value) and VarTypeMask;

      if vType = varInteger then
      begin
        VarClear(v);      
        v:= AParams.Values[i].Value;
        if VarIsType(v, varInteger) then
          ReportParams.Add(sParamName, v)
        else
        begin
          vCount := 1+VarArrayHighBound(v, 1)-VarArrayLowBound(v, 1);
          VarClear(pList);
          pList := VarArrayCreate([0, vCount-1], varVariant);
          for j := VarArrayLowBound(v, 1) to VarArrayHighBound(v, 1) do
             pList[j]:=v[j];

          ReportParams.Add(sParamName, pList);
        end
      end
      else
         ReportParams.Add(sParamName, AParams.Values[i].Value);
      if UpperCase(sParamName) = 'REPORTNAME' then
        sReportName := AParams.Values[i].Value;
    end;

    sRepLevel := REPORT_LEVEL_SYSTEM;
    ctx_RWLocalEngine.StartGroup;
    ctx_RWLocalEngine.CalcPrintReport(aReportNbr, sRepLevel, ReportParams, sReportName);
    sTaskID := ctx_RWLocalEngine.EndGroupForTask(rdtNone);
  finally
    reportParams.Free;
  end;

  Result:= TaskIdToNbr(sTaskID);
end;




function TevRemoteMiscRoutines.GetEEW2List(const pEENBR : Integer): IevDataset;
const
  dsStruct: array [0..2] of TDSFieldDef = (
            (FieldName: 'CO_TAX_RETURN_RUNS_NBR'; DataType: ftInteger;  Size: 0;   Required: True),
            (FieldName: 'NAME'; DataType: ftString;   Size: 60;   Required: True),
            (FieldName: 'Year'; DataType: ftInteger;  Size: 0;   Required: True));
var
  cdEEW2List, cdW2ReportName  : TevClientDataset;
  s : String;
  pClPersonNbr : integer;
begin
  result := nil;
  DM_EMPLOYEE.EE.DataRequired('EE_NBR = ' + IntToStr(pEENBR));
  Assert(DM_EMPLOYEE.EE.RecordCount <> 0, 'ClientID = ' + IntToStr(ctx_DataAccess.ClientID) + ', EE_NBR = ' + IntToStr(pEENBR));
  pClPersonNbr := DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').AsInteger;
  DM_EMPLOYEE.EE.DataRequired('CL_PERSON_NBR = ' + IntToStr(pClPersonNbr));

  cdEEW2List := TevClientDataSet.Create(nil);
  cdW2ReportName := TevClientDataSet.Create(nil);
  Result := TevDataSet.Create(dsStruct);

  try
    s :='select r.CO_TAX_RETURN_QUEUE_nbr, r.CO_TAX_RETURN_RUNS_NBR, q.SY_REPORTS_GROUP_NBR, q.PERIOD_END_DATE, ' +
               ' CONVERTNULLINTEGER(q.SY_GL_AGENCY_REPORT_NBR,0) SY_GL_AGENCY_REPORT_NBR, EXTRACTYEAR(q.PERIOD_END_DATE) rYear ' +
          'from CO_TAX_RETURN_RUNS r, CO_TAX_RETURN_QUEUE Q ' +
          'where r.EE_NBR in ( ' + DM_EMPLOYEE.EE.GetFieldValueList('EE_NBR',True).CommaText + ' ) ' +
                 'and {AsOfNow<r>} and {AsOfNow<q>} ' +
                 'and r.CO_TAX_RETURN_QUEUE_NBR = q.CO_TAX_RETURN_QUEUE_NBR ';

    if not cdEEW2List.Active then
    begin
       ctx_DataAccess.SY_CUSTOM_VIEW.Close;
       with TExecDSWrapper.Create(s) do
          ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
       ctx_DataAccess.SY_CUSTOM_VIEW.Open;
       cdEEW2List.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
       Assert(cdEEW2List.FindField('SY_REPORTS_GROUP_NBR') <> nil);
    end;

    s :='select Distinct a.NAME, a.SY_REPORTS_GROUP_NBR, 0 SY_GL_AGENCY_REPORT_NBR ' +
         'from SY_REPORTS_GROUP a, SY_GL_AGENCY_REPORT b ' +
         'where {AsOfNow<a>} and {AsOfNow<b>} ' +
                 'and a.SY_GL_AGENCY_REPORT_NBR is not null ' +
                'and a.SY_REPORTS_GROUP_NBR <= 20000 ' +
                 'and a.SY_GL_AGENCY_REPORT_NBR = b.SY_GL_AGENCY_REPORT_NBR ' +
                 'and b.RETURN_FREQUENCY = ''A'' ' +
                 'and b.SYSTEM_TAX_TYPE = ''O'' ' +
         'union ' +
         'select Distinct a.NAME, 0 SY_REPORTS_GROUP_NBR, b.SY_GL_AGENCY_REPORT_NBR ' +
          'from SY_REPORTS_GROUP a, SY_GL_AGENCY_REPORT b ' +
          'where {AsOfNow<a>} and {AsOfNow<b>} ' +
             'and  a.SY_REPORTS_GROUP_NBR = b.SY_REPORTS_GROUP_NBR ' +
            'and a.SY_REPORTS_GROUP_NBR > 20000 ' +
             'and b.RETURN_FREQUENCY = ''A'' ' +
             'and b.SYSTEM_TAX_TYPE in (''O'', ''W'', ''P'', ''Q'') ';
    if not cdW2ReportName.Active then
    begin
       ctx_DataAccess.SY_CUSTOM_VIEW.Close;
        with TExecDSWrapper.Create(s) do
          ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
       ctx_DataAccess.SY_CUSTOM_VIEW.Open;
       cdW2ReportName.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
       Assert(cdW2ReportName.FindField('SY_REPORTS_GROUP_NBR') <> nil);
    end;

    cdEEW2List.First;
    while not cdEEW2List.Eof do
    begin
      if cdW2ReportName.Locate('SY_REPORTS_GROUP_NBR;SY_GL_AGENCY_REPORT_NBR',
            VarArrayOf([cdEEW2List.FieldByName('SY_REPORTS_GROUP_NBR').AsInteger,cdEEW2List.FieldByName('SY_GL_AGENCY_REPORT_NBR').AsInteger]),[]) then
         Result.AppendRecord([cdEEW2List.FieldByName('CO_TAX_RETURN_RUNS_NBR').AsInteger,
                              cdW2ReportName.FieldByName('NAME').AsString,
                               cdEEW2List.FieldByName('rYear').AsInteger]);
      cdEEW2List.Next;
    end;

  finally
    cdEEW2List.Free;
    cdW2ReportName.Free;
  end;
end;

procedure TevRemoteMiscRoutines.SaveAPIBilling(const Params: IisListOfValues);
var
  Q: IevQuery;
  ExistingBillingList, BillingForDay, BillingForKey, BillingData, ReducedBillingData : IisListOfValues;
  tmpStream : IevDualStream;
  i, j : integer;
  S : String;
  CurrentDate, LogDate : integer;
  sLogDate : String;
begin
  Context.DBAccess.DisableSecurity;
  try
    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      // checking amount of records
      Q := TevQuery.Create('SELECT SB_BLOB_NBR FROM SB_BLOB WHERE SB_BLOB_NBR=:SbBlobNbr');
      Q.Params.AddValue('SbBlobNbr', iBillingSb_Blob_Nbr);
      Q.Execute;
      if Q.Result.RecordCount = 0 then
      begin
        Q := TevQuery.Create('insert into SB_BLOB(SB_BLOB_NBR, DATA, CHANGE_DATE) values(:SbBlobNbr, null, :ChangeDate)');
        Q.Params.AddValue('SbBlobNbr', iBillingSb_Blob_Nbr);
        Q.Params.AddValue('ChangeDate', SysTime);
        Q.Execute;
      end
      else
      begin
        // pseudo update in order to lock record
        Q := TevQuery.Create('update SB_BLOB set CHANGE_DATE=:ChangeDate where SB_BLOB_NBR=:SbBlobNbr');
        Q.Params.AddValue('SbBlobNbr', iBillingSb_Blob_Nbr);
        Q.Params.AddValue('ChangeDate', SysTime);
        Q.Execute;
      end;

      // reading current data
      Q := TevQuery.Create('SELECT DATA FROM SB_BLOB WHERE SB_BLOB_NBR=:SbBlobNbr');
      Q.Params.AddValue('SbBlobNbr', iBillingSb_Blob_Nbr);
      Q.Execute;
      tmpStream := TisStream.Create;
      ExistingBillingList := TisListOfValues.Create;
      if not (TBlobField(Q.Result.FieldByName('DATA')).IsNull) then
      begin
        TBlobField(Q.Result.FieldByName('DATA')).SaveToStream(tmpStream.RealStream);
        tmpStream.Position := 0;
        ExistingBillingList.ReadFromStream(tmpStream);
      end;

      // { adding new data
      for i := 0 to Params.Count - 1 do
      begin
        BillingData := IInterface(Params.Values[i].Value) as IisListOfValues;
        sLogDate := IntToStr(Trunc(Double(BillingData.Value[sBillingDate])));
        // looking for proper day
        if ExistingBillingList.ValueExists(sLogDate) then
          BillingForDay := IInterface(ExistingBillingList.Value[sLogDate]) as IisListOfValues
        else
        begin
          BillingForDay := TisListOfValues.Create;
          ExistingBillingList.AddValue(sLogDate, BillingForDay);
        end;
        // looking for proper key
        if  BillingForDay.ValueExists(BillingData.Value[sBillingAPIKey]) then
          BillingForKey := IInterface(BillingForDay.Value[BillingData.Value[sBillingAPIKey]]) as IisListOfValues
        else
        begin
          BillingForKey := TisListOfValues.Create;
          BillingForDay.AddValue(UpperCase(BillingData.Value[sBillingAPIKey]), BillingForKey);
        end;
        // looking for proper user and API call
        ReducedBillingData := nil;
        for j := 0 to BillingForKey.Count - 1 do
        begin
          ReducedBillingData := IInterface(BillingForKey.Values[j].Value) as IisListOfValues;
          if (ReducedBillingData.Value[sBillingUserName] = BillingData.Value[sBillingUserName]) and
            (ReducedBillingData.Value[sBillingMethod] = BillingData.Value[sBillingMethod]) then
            break
          else
            ReducedBillingData := nil;
        end;  // for j
        if not Assigned(ReducedBillingData) then
        begin
          ReducedBillingData := TisListOfValues.Create;
          ReducedBillingData.AddValue(sBillingUserName, BillingData.Value[sBillingUserName]);
          ReducedBillingData.AddValue(sBillingMethod, BillingData.Value[sBillingMethod]);
          ReducedBillingData.AddValue(sBillingAppName, BillingData.Value[sBillingAppName]);
          ReducedBillingData.AddValue(sBillingVendorName, BillingData.Value[sBillingVendorName]);
          ReducedBillingData.AddValue(sBillingAmount, 1);
          BillingForKey.AddValue(IntToStr(BillingForKey.Count), ReducedBillingData)
        end
        else
          ReducedBillingData.Value[sBillingAmount] := ReducedBillingData.Value[sBillingAmount] + 1;
      end;  // for i

      // purging in 183 days
      CurrentDate := Trunc(Double(SysTime));
      for i := ExistingBillingList.Count - 1 downto 0 do
      begin
        LogDate := StrToInt(ExistingBillingList.Values[i].Name);
        if Abs(CurrentDate - LogDate) >= 183 then
          ExistingBillingList.DeleteValue(ExistingBillingList.Values[i].Name);
      end;

      // saving changed data
      tmpStream.Size := 0;
      ExistingBillingList.WriteToStream(tmpStream);
      tmpStream.Position := 0;
      SetLength(S, tmpStream.Size);
      tmpStream.ReadBuffer(S[1], tmpStream.Size);
      Q := TevQuery.Create('update SB_BLOB set DATA=:BlobData, CHANGE_DATE=:ChangeDate where SB_BLOB_NBR=:SbBlobNbr');
      Q.Params.AddValue('SbBlobNbr', iBillingSb_Blob_Nbr);
      Q.Params.AddValue('BlobData', S);
      Q.Params.AddValue('ChangeDate', SysTime);
      Q.Execute;

      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  finally
    Context.DBAccess.EnableSecurity;
  end;
end;

function TevRemoteMiscRoutines.GetAPIBilling: IevDataSet;
const
  DSStruct: array [0..6] of TDSFieldDef = (
            (FieldName: sBillingDate; DataType: ftDateTime;  Size: 0;   Required: True),
            (FieldName: sBillingAPIKey; DataType: ftString;   Size: 40;   Required: True),
            (FieldName: sBillingUserName; DataType: ftString;   Size: 40;  Required: True),
            (FieldName: sBillingMethod; DataType: ftString;   Size: 40;  Required: True),
            (FieldName: sBillingVendorName; DataType: ftString;   Size: 50; Required: False),
            (FieldName: sBillingAppName; DataType: ftString;   Size: 50;    Required: False),
            (FieldName: sBillingAmount; DataType: ftInteger;   Size: 0;  Required: True));
var
  Q: IevQuery;
  ExistingBillingList, BillingForDay, BillingForKey, ReducedBillingData : IisListOfValues;
  tmpStream : IevDualStream;
  i, j, k : integer;
  dBillingDate : TDate;
  sAPIKey : String;
  tmpValue : Double;
begin
  Result := TevDataSet.Create(DSStruct);

  Context.DBAccess.DisableSecurity;
  try
    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      Q := TevQuery.Create('SELECT DATA FROM SB_BLOB WHERE SB_BLOB_NBR=:SbBlobNbr');
      Q.Params.AddValue('SbBlobNbr', iBillingSb_Blob_Nbr);
      Q.Execute;
      if Q.Result.RecordCount = 0 then
        exit;
      tmpStream := TisStream.Create;
      ExistingBillingList := TisListOfValues.Create;
      if not (TBlobField(Q.Result.FieldByName('DATA')).IsNull) then
      begin
        TBlobField(Q.Result.FieldByName('DATA')).SaveToStream(tmpStream.RealStream);
        tmpStream.Position := 0;
        ExistingBillingList.ReadFromStream(tmpStream);
        // loop by days
        for i := 0 to ExistingBillingList.Count - 1 do
        begin
          tmpValue := StrToFloat(ExistingBillingList.Values[i].Name);
          BillingForDay := IInterface(ExistingBillingList.Values[i].Value) as IisListOfValues;
          dBillingDate := TDateTime(tmpValue);
          // loop by keys
          for j := 0 to BillingForDay.Count - 1 do
          begin
            sAPIKey := BillingForDay.Values[j].Name;
            BillingForKey := IInterface(BillingForDay.Values[j].Value) as IisListOfValues;
            // loop by billing records
            for k := 0 to BillingForKey.Count - 1 do
            begin
              ReducedBillingData := IInterface(BillingForKey.Values[k].Value) as IisListOfValues;
              Result.AppendRecord([dBillingDate, sAPIKey, ReducedBillingData.Value[sBillingUserName],
                ReducedBillingData.Value[sBillingMethod], ReducedBillingData.Value[sBillingVendorName],
                ReducedBillingData.Value[sBillingAppName], ReducedBillingData.Value[sBillingAmount] ]);
            end;  // for k
          end;  // for j
        end;  // for i
      end;
    finally
      ctx_DataAccess.RollbackNestedTransaction;
    end;
  finally
    Context.DBAccess.EnableSecurity;
  end;
end;

function TevRemoteMiscRoutines.GetSBEmail(const aCoNbr: integer): String;
var
  Q: IevQuery;
  iSbUserNbr: integer;
begin
  Result := '';

  Q := TevQuery.Create('select CUSTOMER_SERVICE_SB_USER_NBR from CO a where {AsOfNow<a>} and a.CO_NBR=:P_CO_NBR');
  Q.Params.AddValue('P_CO_NBR', aCoNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found');
  if Q.Result.FieldValues['CUSTOMER_SERVICE_SB_USER_NBR'] <> null then
  begin
    iSbUserNbr := Q.Result.FieldValues['CUSTOMER_SERVICE_SB_USER_NBR'];
    Context.Security.EnableSystemAccountRights;
    try
      Q := TevQuery.Create('select EMAIL_ADDRESS from SB_USER a where {AsOfNow<a>} and a.SB_USER_NBR=:P_SB_USER_NBR');
      Q.Params.AddValue('P_SB_USER_NBR', iSbUserNbr);
      Q.Execute;
      if (Q.Result.RecordCount > 0) and  (Q.Result.FieldValues['EMAIL_ADDRESS'] <> null) then
        Result := Q.Result.FieldValues['EMAIL_ADDRESS'];
    finally
      Context.Security.DisableSystemAccountRights;
    end;
  end;
end;

procedure TevRemoteMiscRoutines.AddTOARequests(const Requests: IisInterfaceList);
var
  Q: IEvQuery;
  i: integer;
  iEENbr: integer;
  iEETimeOffAccrualNbr: integer;
  iMgrNbr: integer;
  StorageTable, TOATable: TevClientDataSet;
  sMessageForManager, sMessageForEE: String;
  sTypeOffType: String;
  sMgrName: String;
  sOrigin: String;
  Request: IEvTOARequest;
  dRequestDate: TDate;
begin
  CheckCondition(Requests.Count > 0, 'Request is empty');
  dRequestDate := Today;

  // integrity check: EENbr and EETimeOffAccrualNbr has to be the same for all rows
  Request := IInterface(Requests.Items[0]) as IEvTOARequest;
  iEENbr := Request.EENbr;
  iEETimeOffAccrualNbr := Request.EETimeOffAccrualNbr;
  for i := 0 to Requests.Count - 1 do
  begin
    Request := IInterface(Requests.Items[i]) as IEvTOARequest;
    CheckCondition(iEENbr = Request.EENbr, 'You cannot add requests for several EEs at one call');
    CheckCondition(iEETimeOffAccrualNbr = Request.EETimeOffAccrualNbr, 'You cannot add different request types at one call');
    CheckCondition(Request.EETimeOffAccrualOperNbr < 0, 'Data inconsistency: you cannot insert and edit records at the same call');
  end;

  if iEENbr = Abs(Context.UserAccount.InternalNbr) then
    sOrigin := 'E'
  else
    sOrigin := 'M';

  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee a where a.ee_nbr=:EENbr and {AsOfNow<a>} ');
  Q.Params.AddValue('EENbr', iEENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(iEeNbr));
  // checking that iTypeOffTypeNbr exists
  TOATable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_TIME_OFF_ACCRUAL'));
  TOATable.Active := false;
  TOATable.Filtered := false;
  TOATable.DataRequired('EE_TIME_OFF_ACCRUAL_NBR=' + IntToStr(iEETimeOffAccrualNbr) +
    ' and EE_NBR=' + IntTOStr(iEeNbr));
  CheckCondition(TOATable.RecordCount > 0, 'TOA Type not found: ' + intToStr(iEETimeOffAccrualNbr) +
    '. Employee: ' + IntToStr(iEeNbr));
  Q := TEvQuery.Create('Select DESCRIPTION from CO_TIME_OFF_ACCRUAL a where a.CO_TIME_OFF_ACCRUAL_NBR=:P_Nbr and {AsOfNow<a>} ');
  Q.Params.AddValue('P_Nbr', TOATable['CO_TIME_OFF_ACCRUAL_NBR']);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'TOA type not found on company level: ' + VarToStr(TOATable['CO_TIME_OFF_ACCRUAL_NBR']));
  sTypeOffType := VarToStr(Q.Result['DESCRIPTION']);

  // checking manager's rights
  sMgrName := '';
  if sOrigin = 'M' then
  begin
    iMgrNbr := Abs(Context.UserAccount.InternalNbr);
    // cheking that current user is manager of this employee
    Q := TevQuery.Create('select ee_nbr from co_group_manager a where {AsOfNow<a>} ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and b.ee_nbr=:p1' +
      ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2)) and ' +
      ' ee_nbr=:P3');
    Q.Params.AddValue('P1', iEeNbr);
    Q.Params.AddValue('P2', 'T');
    Q.Params.AddValue('P3', iMgrNbr);
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Current user is not a TOA manager of employee');

    // getting information about manager
    Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR ' +
      ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
      ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ');
    Q.Params.AddValue('EENbr', iMgrNbr);
    Q.Execute;
    sMgrName := VarToStr(Q.Result['FIRST_NAME']) + ' ' + VarToStr(Q.Result['LAST_NAME']);
  end;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_TIME_OFF_ACCRUAL_OPER'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  sMessageForManager := '';
  sMessageForEE := '';

  sMessageForManager := sMessageForManager + 'Employee created a new request' + #13#10 + 'Time Off Type:' + sTypeOffType + #13#10;
  StorageTable.DataRequired('1=2');
  for i := 0 to Requests.Count - 1 do
  begin
    Request := IInterface(Requests.Items[i]) as IEvTOARequest;
    sMessageForManager := sMessageForManager + '  ' + DateToStr(Request.AccrualDate) + ' - ' +
      IntToStr(Request.Hours) + ' hr' + IntToStr(Request.Minutes) + ' min' + #13#10;
    if (Request.Hours > 0) or (Request.Minutes > 0) then
    begin
      StorageTable.Append;
      StorageTable['EE_TIME_OFF_ACCRUAL_NBR'] := Request.EETimeOffAccrualNbr;
      StorageTable['ACCRUAL_DATE'] := Request.AccrualDate;
      StorageTable['ACCRUED'] := 0;
      StorageTable['USED'] := Request.HoursAndMinAsDouble;
      StorageTable['NOTE'] := TOARequestNotesHeader + Request.Notes;
      StorageTable['OPERATION_CODE'] := EE_TOA_OPER_CODE_REQUEST_PENDING;
      StorageTable['REQUEST_DATE'] := dRequestDate;
      if Request.CL_E_DS_NBR > 0 then
        StorageTable['CL_E_DS_NBR'] := Request.CL_E_DS_NBR;

      StorageTable.Post;
      Request.EETimeOffAccrualOperNbr := StorageTable['EE_TIME_OFF_ACCRUAL_OPER_NBR'];
    end;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  try
    if sOrigin = 'E' then
      SendEMailToManagers(iEeNbr, 'T', sMessageForManager)
    else
      SendEmailToEmployeeAndManager(iEENbr, 'Change to Time Off Request', sMessageForEE);
  except
    on E: ESendEmailError do
      GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email', E.Message + #13#10 + GetErrorCallStack(E));
  end;
end;

function TevRemoteMiscRoutines.GetTOARequestsForEE(const EENbr: integer): IisInterfaceList;
var
  Q: IEvQuery;
  iMgrNbr: integer;
begin
  Result := TisInterfaceList.Create;

  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee where ee_nbr=:EENbr and {AsOfNow<ee>}');
  Q.Params.AddValue('EENbr', EeNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(EeNbr));

  // checking sequrity
  if EENbr <> Abs(Context.UserAccount.InternalNbr) then
  begin
    iMgrNbr := Abs(Context.UserAccount.InternalNbr);
    // cheking that current user is manager of this employee
    Q := TevQuery.Create('select ee_nbr from co_group_manager where {AsOfNow<co_group_manager>} ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member where {AsOfNow<co_group_member>} and ee_nbr=:p1' +
      ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2)) and ' +
      ' ee_nbr=:P3');
    Q.Params.AddValue('P1', EeNbr);
    Q.Params.AddValue('P2', 'T');
    Q.Params.AddValue('P3', iMgrNbr);
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Current user is not a TOA manager of employee');
  end;

  // main query
  Q := TEvQuery.Create('select EE_TIME_OFF_ACCRUAL_OPER_NBR, ACCRUAL_DATE, USED, NOTE, OPERATION_CODE, PR_NBR, ' +
    ' a.EE_TIME_OFF_ACCRUAL_NBR, a.REQUEST_DATE, a.CL_E_DS_NBR, MANAGER_NOTE ' +
    ' from EE_TIME_OFF_ACCRUAL_OPER a, EE_TIME_OFF_ACCRUAL b ' +
    ' where {AsOfNow<a>} and {AsOfNow<b>} and a.EE_TIME_OFF_ACCRUAL_NBR=b.EE_TIME_OFF_ACCRUAL_NBR ' +
    ' and ee_nbr = :EENbr and OPERATION_CODE in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') ' +
    ' order by EE_TIME_OFF_ACCRUAL_NBR, OPERATION_CODE, ACCRUAL_DATE');
  Q.Params.AddValue('EENbr', EeNbr);
  Q.Execute;
  if Q.Result.RecordCount = 0 then
    exit;

  Result :=GetTOARequests(EENbr, Q);

end;

function TevRemoteMiscRoutines.GetTOARequestsForMgr(const MgrEENbr: integer): IisInterfaceList;
var
  Q: IEvQuery;
  i: integer;
  EERequests: IisInterfaceList;
begin
  //checking that manager exists
  Q := TEvQuery.Create('Select ee_nbr from ee where ee_nbr=:EENbr and {AsOfNow<ee>}');
  Q.Params.AddValue('EENbr', MgrEeNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Manager not found: ' + intToStr(MgrEeNbr));

  Q := TEvQuery.Create('SELECT EE_NBR FROM CO_GROUP_MEMBER where {AsOfNow<CO_GROUP_MEMBER>}' +
    ' and CO_GROUP_NBR in (select CO_GROUP_NBR from CO_GROUP where {AsOfNow<CO_GROUP>} and ' +
    ' GROUP_TYPE=:p_rt and ' +
    ' CO_GROUP_NBR in (select CO_GROUP_NBR from CO_GROUP_MANAGER where {AsOfNow<CO_GROUP_MANAGER>}' +
    ' and EE_NBR=:p_ee_nbr)) order by EE_NBR');
  Q.Params.AddValue('p_rt', ESS_GROUPTYPE_TIMEOFF);
  Q.Params.AddValue('p_ee_nbr', MgrEeNbr);
  Q.Execute;

  Result := TisInterfaceList.Create;
  while not Q.Result.eof do
  begin
    EERequests := GetTOARequestsForEE(Q.Result['EE_NBR']);
    for i := 0 to EERequests.Count - 1 do
      Result.Add(EERequests.Items[i]);
    Q.Result.Next;
  end;
end;

procedure TevRemoteMiscRoutines.ProcessTOARequests(const Operation: TevTOAOperation; const Notes: String;
  const EETimeOffAccrualOperNbrs: IisStringList; const CheckMgr: boolean = True);
var
  j: integer;
  iNbr: integer;
  StorageTable: TevClientDataSet;
  iEENbr, iMgrNbr: integer;
  sSubject, sMessage: String;
  sMgrName: String;
  Q: IEvQuery;
  iCoTOANbr: integer;
  iEETimeOffAccrualNbr: integer;
  sTimeOffType: String;
  iHours, iMinutes: integer;
begin
  iMgrNbr := Abs(Context.UserAccount.InternalNbr);
  if CheckMgr then
  begin
    // getting information about manager
    Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR ' +
      ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
      ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ');
    Q.Params.AddValue('EENbr', iMgrNbr);
    Q.Execute;
    sMgrName := VarToStr(Q.Result['FIRST_NAME']) + ' ' + VarToStr(Q.Result['LAST_NAME']);
  end
  else
    sMgrName := Context.UserAccount.FirstName + ' ' + Context.UserAccount.LastName;

  Q := TEvQuery.Create('select ee_nbr, CO_TIME_OFF_ACCRUAL_NBR, a.EE_TIME_OFF_ACCRUAL_NBR from EE_TIME_OFF_ACCRUAL_OPER a, EE_TIME_OFF_ACCRUAL b ' +
    ' where a.EE_TIME_OFF_ACCRUAL_NBR=b.EE_TIME_OFF_ACCRUAL_NBR and {AsOfNow<a>} and ' +
    ' {AsOfNow<b>} and a.EE_TIME_OFF_ACCRUAL_OPER_NBR=:P1');
  Q.Params.AddValue('P1', StrToInt(EETimeOffAccrualOperNbrs.Strings[0]));  // using NBR from the first line as base
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee''s TOA type not found');
  iEENbr := Q.Result['EE_NBR'];
  iCoTOANbr := Q.Result['CO_TIME_OFF_ACCRUAL_NBR'];
  iEETimeOffAccrualNbr := Q.Result['EE_TIME_OFF_ACCRUAL_NBR'];

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_TIME_OFF_ACCRUAL_OPER'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  // filtering data by OPERATION_CODE and EE_TIME_OFF_ACCRUAL_NBR from the first row in order to ensure that
  // all rows belongs to the same TOA type and the same employee
  StorageTable.DataRequired('OPERATION_CODE=''' + EE_TOA_OPER_CODE_REQUEST_PENDING +
       ''' and EE_TIME_OFF_ACCRUAL_NBR=' + IntToStr(iEETimeOffAccrualNbr));

  Q := TEvQuery.Create('Select DESCRIPTION from CO_TIME_OFF_ACCRUAL a where a.CO_TIME_OFF_ACCRUAL_NBR=:P_Nbr and {AsOfNow<a>} ');
  Q.Params.AddValue('P_Nbr', iCoTOANbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'TOA type not found on company level: ' + IntToStr(iCoTOANbr));
  sTimeOffType := VarToStr(Q.Result['DESCRIPTION']);

  if CheckMgr then
  begin
    // cheking that current user is manager of this employee
    Q := TevQuery.Create('select ee_nbr from co_group_manager a where {AsOfNow<a>} ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and ee_nbr=:p1' +
      ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2)) and ' +
      ' ee_nbr=:P3');
    Q.Params.AddValue('P1', iEeNbr);
    Q.Params.AddValue('P2', 'T');
    Q.Params.AddValue('P3', iMgrNbr);
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Current user is not a TOA manager of employee');
  end;

  if Operation = toaApprove then
    sSubject := 'Your Time Off request has been approved by ' + sMgrName
  else
    sSubject := 'Your Time Off request has been denied by ' + sMgrName;

  sMessage := sSubject + #13#10#13#10 + 'Time Off Type:' + sTimeOffType + #13#10;

  for j := 0 to EETimeOffAccrualOperNbrs.Count - 1 do
  begin
    iNbr := StrToInt(EETimeOffAccrualOperNbrs.Strings[j]);

    CheckCondition(StorageTable.Locate('EE_TIME_OFF_ACCRUAL_OPER_NBR', iNbr, []), 'Cannot find a pending request for editing: ' + IntToStr(iNbr));
    CheckCondition((StorageTable['PR_NBR'] = null), 'You cannot process record if record is used: ' + IntToStr(iNbr));
    if (j = 0) and (not (Operation = toaApprove)) then
    // adding reason for denied requests from the first record
      sMessage := sMessage + 'Reason: ' + Copy(VarToStr(StorageTable['Note']), Length(TOARequestNotesHeader) + 1, MaxInt) + #13#10;

    iHours := 0;
    iMinutes := 0;
    ConvertDoubleToIntegerHourAndMin(StorageTable['USED'], iHours, iMinutes);
    sMessage := sMessage + '   ' + VarToStr(StorageTable['ACCRUAL_DATE']) + ' - ' + IntToStr(iHours) + ' hr ' +
      IntToStr(iMinutes) + ' min' + #13#10;

    StorageTable.Edit;
    if Operation = toaApprove then
      StorageTable['OPERATION_CODE'] := EE_TOA_OPER_CODE_REQUEST_APPROVED
    else
      StorageTable['OPERATION_CODE'] := EE_TOA_OPER_CODE_REQUEST_DENIED;

    if Notes <> '' then
      StorageTable['NOTE'] := TOARequestNotesHeader + Notes;
    StorageTable.Post;
  end;  // for j

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  try
    SendEmailToEmployeeAndManager(iEENbr, sSubject, sMessage);
  except
    on E: ESendEmailError do
      GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email', E.Message + #13#10 + GetErrorCallStack(E));
  end;
end;

procedure TevRemoteMiscRoutines.UpdateTOARequests(const Requests: IisInterfaceList);
var
  Q: IEvQuery;
  i: integer;
  iEENbr: integer;
  iEETimeOffAccrualNbr: integer;
  iMgrNbr: integer;
  StorageTable, TOATable: TevClientDataSet;
  sMessageForManager, sMessageForEE: String;
  sTypeOffType: String;
  sMgrName: String;
  sOrigin: String;
  Request: IEvTOARequest;
  sNotes: String;
  sCondition: String;
begin
  CheckCondition(Requests.Count > 0, 'Request is empty');

  // integrity check: EENbr and EETimeOffAccrualNbr has to be the same for all rows
  Request := IInterface(Requests.Items[0]) as IEvTOARequest;
  iEENbr := Request.EENbr;
  iEETimeOffAccrualNbr := Request.EETimeOffAccrualNbr;
  sNotes := Request.Notes;
  for i := 0 to Requests.Count - 1 do
  begin
    Request := IInterface(Requests.Items[i]) as IEvTOARequest;
    CheckCondition(iEENbr = Request.EENbr, 'You cannot edit requests for several EEs at one call');
    CheckCondition(iEETimeOffAccrualNbr = Request.EETimeOffAccrualNbr, 'You cannot edit different request types at one call');
    CheckCondition(Request.EETimeOffAccrualOperNbr > 0, 'Data inconsistency: you cannot insert and edit records at the same call');
  end;

  if iEENbr = Abs(Context.UserAccount.InternalNbr) then
  begin
    // checking if this employee is a manager
    iMgrNbr := Abs(Context.UserAccount.InternalNbr);
    // cheking that current user is manager of this employee
    Q := TevQuery.Create('select ee_nbr from co_group_manager where {AsOfNow<co_group_manager>} ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member where {AsOfNow<co_group_member>} and ee_nbr=:p1' +
      ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2)) and ' +
      ' ee_nbr=:P3');
    Q.Params.AddValue('P1', iEeNbr);
    Q.Params.AddValue('P2', 'T');
    Q.Params.AddValue('P3', iMgrNbr);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
      sOrigin := 'M'
    else
      sOrigin := 'E';
  end
  else
    sOrigin := 'M';

  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee where ee_nbr=:EENbr and {AsOfNow<ee>}');
  Q.Params.AddValue('EENbr', iEENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(iEeNbr));
  // checking that iTypeOffTypeNbr exists
  TOATable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_TIME_OFF_ACCRUAL'));
  TOATable.Active := false;
  TOATable.Filtered := false;
  TOATable.DataRequired('EE_TIME_OFF_ACCRUAL_NBR=' + IntToStr(iEETimeOffAccrualNbr) +
    ' and EE_NBR=' + IntTOStr(iEeNbr));
  CheckCondition(TOATable.RecordCount > 0, 'TOA Type not found: ' + intToStr(iEETimeOffAccrualNbr) +
    '. Employee: ' + IntToStr(iEeNbr));
  Q := TEvQuery.Create('Select DESCRIPTION from CO_TIME_OFF_ACCRUAL where CO_TIME_OFF_ACCRUAL_NBR=:P_Nbr and {AsOfNow<CO_TIME_OFF_ACCRUAL>}');
  Q.Params.AddValue('P_Nbr', TOATable['CO_TIME_OFF_ACCRUAL_NBR']);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'TOA type not found on company level: ' + VarToStr(TOATable['CO_TIME_OFF_ACCRUAL_NBR']));
  sTypeOffType := VarToStr(Q.Result['DESCRIPTION']);

  // checking manager's rights
  sMgrName := '';
  if sOrigin = 'M' then
  begin
    iMgrNbr := Abs(Context.UserAccount.InternalNbr);
    // cheking that current user is manager of this employee
    Q := TevQuery.Create('select ee_nbr from co_group_manager where {AsOfNow<co_group_manager>} ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member where {AsOfNow<co_group_member>} and ee_nbr=:p1' +
      ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2)) and ' +
      ' ee_nbr=:P3');
    Q.Params.AddValue('P1', iEeNbr);
    Q.Params.AddValue('P2', 'T');
    Q.Params.AddValue('P3', iMgrNbr);
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Current user is not a TOA manager of employee');

    // getting information about manager
    Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR ' +
      ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
      ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>}');
    Q.Params.AddValue('EENbr', iMgrNbr);
    Q.Execute;
    sMgrName := VarToStr(Q.Result['FIRST_NAME']) + ' ' + VarToStr(Q.Result['LAST_NAME']);
  end;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_TIME_OFF_ACCRUAL_OPER'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  sMessageForManager := '';
  sMessageForEE := '';

  if sOrigin = 'E' then
    sMessageForManager := sMessageForManager + 'Employee edited an existing request' + #13#10#13#10 +
      'Time Off Type:' + sTypeOffType + #13#10
  else
    sMessageForEE := sMessageForEE + 'Your Time Off request has been changed by ' + sMgrName +
      #13#10#13#10 + 'Time Off Type:' + sTypeOffType + #13#10 + 'Notes: ' + sNotes + #13#10;

  sCondition := 'EE_TIME_OFF_ACCRUAL_OPER_NBR in (';
  for i := 0 to Requests.Count - 1 do
  begin
    Request := IInterface(Requests.Items[i]) as IEvTOARequest;
    sCondition := sCondition + IntToStr(Request.EETimeOffAccrualOperNbr) + ',';
  end;
  sCondition := Copy(sCondition, 1, Length(sCondition) - 1) + ')';
  StorageTable.DataRequired(sCondition);

  for i := 0 to Requests.Count - 1 do
  begin
    Request := IInterface(Requests.Items[i]) as IEvTOARequest;
    sMessageForManager := sMessageForManager + '  ' + DateToStr(Request.AccrualDate) + ' - ' + IntToStr(Request.Hours) + ' hr' +
      IntToStr(Request.Minutes) + ' min' + #13#10;
    sMessageForEE := sMessageForEE + '  ' + DateToStr(Request.AccrualDate) + ' - ' + IntToStr(Request.Hours) + ' hr' +
      IntToStr(Request.Minutes) + ' min' + #13#10;
    CheckCondition(StorageTable.Locate('EE_TIME_OFF_ACCRUAL_OPER_NBR', Request.EETimeOffAccrualOperNbr, []), 'Cannot find record to edit: ' + IntToStr(Request.EETimeOffAccrualOperNbr));
    if sOrigin = 'E' then
      CheckCondition(StorageTable['OPERATION_CODE'] = EE_TOA_OPER_CODE_REQUEST_PENDING, 'You cannot change not pending requests');
    CheckCondition((StorageTable['PR_NBR'] = null) and (StorageTable['PR_BATCH_NBR'] = null) and ((StorageTable['PR_CHECK_NBR'] = null)),
      'You cannot change requests, if they are assigned to payroll.');
    if (Request.Hours <> 0) or (Request.Minutes <> 0) then
    begin
      StorageTable.Edit;
      StorageTable['EE_TIME_OFF_ACCRUAL_NBR'] := Request.EETimeOffAccrualNbr;
      StorageTable['ACCRUAL_DATE'] := Request.AccrualDate;
      StorageTable['ACCRUED'] := 0;
      StorageTable['USED'] := Request.HoursAndMinAsDouble;
      StorageTable['NOTE'] := TOARequestNotesHeader + sNotes;
      if Request.CL_E_DS_NBR > 0 then
        StorageTable['CL_E_DS_NBR'] := Request.CL_E_DS_NBR
      else
        StorageTable['CL_E_DS_NBR'] := Null;

      StorageTable.Post;
    end
    else
      StorageTable.Delete;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  try
    if sOrigin = 'E' then
      SendEMailToManagers(iEeNbr, 'T', sMessageForManager)
    else
      SendEmailToEmployeeAndManager(iEENbr, 'Change to Time Off Request', sMessageForEE);
  except
    on E: ESendEmailError do
      GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email', E.Message + #13#10 + GetErrorCallStack(E));
  end;
end;

procedure TevRemoteMiscRoutines.SendEmailToEmployeeAndManager(const AEeNbr: integer; const ASubject, AMessageDetails: String);
var
  Q: IEvQuery;
  sFrom, sTo : String;
  EMail1, EMail2: String;

  function GetManagerEmails(const AEeNbr: Integer): IisStringList;
  var
    Q, Q1: IevQuery;
    EMail1, EMail2: String;
  begin
    Result := TisStringList.Create;

    Q := TevQuery.Create('select ee_nbr from co_group_manager a where {AsOfNow<a>} and send_email=''Y'' ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and ee_nbr=:p1' +
      ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2) )');
    Q.Params.AddValue('P1', AEeNbr);
    Q.Params.AddValue('P2', 'T');
    Q.Execute;
    while not Q.Result.eof do
    begin
      // reading email addresses
      Q1 := TEvQuery.Create('select a.e_mail_address as EE_MAIL, b.e_mail_address as CL_PERSON_MAIL ' +
        ' from ee a,cl_person b where a.cl_person_nbr=b.cl_person_nbr and {AsOfNow<a>} ' +
        ' and {AsOfNow<b>} and a.ee_nbr=:EENbr');
      Q1.Params.AddValue('EENbr', Q.Result.FieldByName('EE_NBR').AsInteger);
      Q1.Execute;
      if (Q1.Result.RecordCount > 0) and ((Q1.result.FieldByName('EE_MAIL').Value <> null) or
        (Q1.result.FieldByName('CL_PERSON_MAIL').Value <> null)) then
      begin
        EMail1 := VarToStr(Q1.result.FieldByName('EE_MAIL').Value);
        EMail2 := VarToStr(Q1.result.FieldByName('CL_PERSON_MAIL').Value);

        if EMail1 <> '' then
          Result.Add(EMail1)
        else
        if EMail2 <> '' then
          Result.Add(EMail2);
      end;
      Q.Result.Next;
    end;
  end;

var
  EMails: IisStringList;
  Mes: IevMailMessage;
  i: Integer;
begin
  // getting information about employee
  Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR, ' +
    ' a.e_mail_address as EE_MAIL, b.e_mail_address as CL_PERSON_MAIL ' +
    ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
    ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ');
  Q.Params.AddValue('EENbr', AEeNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + IntToStr(AEeNbr));

  // preparing message
  sFrom := 'EvolutionSelfServe';

  if (Q.result.FieldByName('EE_MAIL').Value <> null) or (Q.result.FieldByName('CL_PERSON_MAIL').Value <> null) then
  begin
    EMail1 := VarToStr(Q.result.FieldByName('EE_MAIL').Value);
    EMail2 := VarToStr(Q.result.FieldByName('CL_PERSON_MAIL').Value);
    if EMail1 <> '' then
      sTo := EMail1
    else
      sTo := EMail2;

    if Trim(sTo) <> '' then
    begin
      EMails := GetManagerEmails(AEeNbr);

      Mes := TevMailMessage.Create;

      Mes.AddressFrom := sFrom;
      Mes.AddressTo := sTo;
      Mes.Subject := ASubject;
      Mes.Body := AMessageDetails;

      if EMails.Count > 0 then
      begin
        Mes.AddressCC := EMails[0];
        for i := 1 to EMails.Count - 1 do
          Mes.AddressCC := Mes.AddressCC + ';' + EMails[i];
      end;

      try
        ctx_EMailer.SendMail(Mes);
      except
        on E: Exception do
          raise ESendEmailError.Create('Cannot send email to employee. From: ' + sFrom + '. To: ' + sTo + #13#10 + 'Error:' + E.Message);
      end;
    end;
  end;
end;

procedure TevRemoteMiscRoutines.SendEmailToManagers(const AEeNbr: integer; const ARequestType, AMessageDetails: String);
var
  Q, Q1: IEvQuery;
  EeCustomNbr: String;
  EECustomCoNbr: String;
  EeFirstName, EELastName: String;
  sSubject, sFrom, sTo, sMessage : String;
  EMail1, EMail2: String;
begin
  // getting information about employee
  Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR ' +
    ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
    ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ');
  Q.Params.AddValue('EENbr', AEeNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + IntToStr(AEeNbr));
  EeCustomNbr := Trim(VarToStr(Q.Result.Fields.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value));
  EECustomCoNbr := Trim(VarToStr(Q.Result.Fields.FieldByName('CUSTOM_COMPANY_NUMBER').Value));
  EeFirstName := VarToStr(Q.Result.Fields.FieldByName('FIRST_NAME').Value);
  EELastName := VarToStr(Q.Result.Fields.FieldByName('LAST_NAME').Value);

  // preparing message
  sFrom := 'EvolutionSelfServe';
  if ARequestType = 'H' then
    sSubject := 'Request for W4 changes from employee'
  else
  if ARequestType = 'C' then
    sSubject := 'Request for ESS configuration changes from employee'
  else
  if ARequestType = 'T' then
    sSubject := 'Request for Time Off from employee'
  else
  if ARequestType = 'D' then
    sSubject := 'Request for direct deposit change'
  else
    raise Exception.Create('Unknown request''s type: ' + ARequestType);

  sMessage := sSubject + #13#10 + 'Employee''s information:' + #13#10 +
    '  Custom Company Number: ' + EECustomCoNbr + #13#10 +
    '  Custom Employee Number: ' + EeCustomNbr + #13#10 +
    '  First Name: ' + EeFirstName + #13#10 +
    '  Last Name: ' + EELastName + #13#10;

  // getting list of managers
  Q := TevQuery.Create('select ee_nbr from co_group_manager a where {AsOfNow<a>} and send_email=''Y'' ' +
    ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and ee_nbr=:p1' +
    ' and co_group_nbr in (select co_group_nbr from co_group where group_type=:p2) )');
  Q.Params.AddValue('P1', AEeNbr);
  if (ARequestType = 'H') or (ARequestType = 'C') or (ARequestType = 'D') then
    Q.Params.AddValue('P2', 'H')
  else
    Q.Params.AddValue('P2', 'T');
  Q.Execute;
  while not Q.Result.eof do
  begin
    // reading email addresses
    Q1 := TEvQuery.Create('select a.e_mail_address as EE_MAIL, b.e_mail_address as CL_PERSON_MAIL ' +
      ' from ee a,cl_person b where a.cl_person_nbr=b.cl_person_nbr and {AsOfNow<a>} ' +
      ' and {AsOfNow<b>} and a.ee_nbr=:EENbr');
    Q1.Params.AddValue('EENbr', Q.Result.FieldByName('EE_NBR').AsInteger);
    Q1.Execute;
    if (Q1.Result.RecordCount > 0) and ((Q1.result.FieldByName('EE_MAIL').Value <> null) or
      (Q1.result.FieldByName('CL_PERSON_MAIL').Value <> null)) then
    begin
      EMail1 := VarToStr(Q1.result.FieldByName('EE_MAIL').Value);
      EMail2 := VarToStr(Q1.result.FieldByName('CL_PERSON_MAIL').Value);
      if EMail1 <> '' then
        sTo := EMail1
      else
        sTo := EMail2;

      if Trim(sTo) <> '' then
      begin
        try
          if AMessageDetails <> '' then
            ctx_EMailer.SendQuickMail(sTo, sFrom, sSubject, sMessage + #13#10 + AMessageDetails)
          else
            ctx_EMailer.SendQuickMail(sTo, sFrom, sSubject, sMessage);
        except
          on E: Exception do
            raise ESendEmailError.Create('Cannot send email to manager. From: ' + sFrom + '. To: ' + sTo + #13#10 + 'Error:' + E.Message);
        end;
      end;
    end;

    Q.Result.Next;
  end;  // while
end;

procedure TevRemoteMiscRoutines.GetEENameByNbr(const AEeNbr: integer; out AFirstName, ALastName, ACustomEeNumber: String);
var
  Q: IEvQuery;
begin
  Q := TEvQuery.Create('select FIRST_NAME, LAST_NAME, b.CUSTOM_EMPLOYEE_NUMBER from CL_PERSON a, EE b ' +
    ' where {AsOfNow<a>} and {AsOfNow<b>} and ' +
    ' a.cl_person_nbr=b.cl_person_nbr and b.ee_nbr = :P_EeNbr');
  Q.Params.AddValue('P_EeNbr', AEeNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found: ' + intToStr(AEeNbr));
  AFirstName := VarToStr(Q.Result['FIRST_NAME']);
  ALastName := VarToStr(Q.Result['LAST_NAME']);
  ACustomEeNumber := VarToStr(Q.Result['CUSTOM_EMPLOYEE_NUMBER']);
end;

function TevRemoteMiscRoutines.GetWCReportList(const aCoNbr: integer; const AReportNbrs: IisStringList): IevDataset;
var
  Q: IevQuery;
  sCondition: String;
  RunParams: TevPrRepPrnSettings;
  i: integer;
  oldReportNbr: integer;
  SortIndex: String;
  tmpDataSet: TkbmCustomMemTable;
begin
  Result := TevDataset.Create;
  Result.vclDataSet.FieldDefs.Add('CO_REPORTS_NBR', ftInteger, 0, true);
  Result.vclDataSet.FieldDefs.Add('CUSTOM_NAME', ftString, 40, true);
  Result.vclDataSet.FieldDefs.Add('REPORT_WRITER_REPORTS_NBR', ftInteger, 0, true);
  Result.vclDataSet.Active := true;

  sCondition := '';
  for i := 0 to AReportNbrs.Count - 1 do
    if i = 0 then
      sCondition := AReportNbrs[i]
    else
      sCondition := sCondition + ',' + AReportNbrs[i];

  Q := TevQuery.Create('select CO_REPORTS_NBR, CUSTOM_NAME, REPORT_WRITER_REPORTS_NBR, RUN_PARAMS, 0 as PRIORITY_LEVEL ' +
    ' from CO_REPORTS a where {AsOfNow<a>} and REPORT_LEVEL=''' + REPORT_LEVEL_SYSTEM + ''' and OVERRIDE_MB_GROUP_NBR is null ' +
    ' and REPORT_WRITER_REPORTS_NBR in (' + sCondition + ') and CO_NBR=:P_CO_NBR');
  Q.Params.AddValue('P_CO_NBR', aCoNbr);
  Q.Execute;

  // calculatin 'priority level' and filtering by 'Hide For Remotes' and 'Print with Adjustment Payrolls Only' flags
  RunParams := TevPrRepPrnSettings.Create(nil);
  try
    Q.Result.First;
    while not Q.Result.eof do
    begin
      if not Q.Result.FieldByName('RUN_PARAMS').IsNull then
        try
          RunParams.ReadFromBlobField(TBlobField(Q.Result.FieldByName('RUN_PARAMS')))
        except
          RunParams.Clear;
        end
      else
        RunParams.Clear;

      if RunParams.HideForRemotes or RunParams.PrnWithAdjPayrolls then
        Q.Result.Delete
      else
      begin
        Q.Result.Edit;
        Q.Result['PRIORITY_LEVEL'] := RunParams.Priority;
        Q.Result.Post;

        Q.Result.Next;
      end;
    end;  // while
  finally
    FreeAndNil(RunParams);
  end;

  // skipping duplicates and filling up result
  SortIndex := RandomString(10);
  tmpDataSet := Q.Result.vclDataSet as TkbmCustomMemTable;
  tmpDataSet.AddIndex(SortIndex, 'REPORT_WRITER_REPORTS_NBR; PRIORITY_LEVEL', []);
  tmpDataSet.IndexDefs.Update;
  tmpDataSet.IndexName := SortIndex;
  try
    oldReportNbr := -1;
    Q.Result.First;
    while not Q.Result.eof do
    begin
      if Q.Result['REPORT_WRITER_REPORTS_NBR'] <> oldReportNbr then
      begin
        oldReportNbr := Q.Result['REPORT_WRITER_REPORTS_NBR'];
        Result.Append;
        Result['CO_REPORTS_NBR'] := Q.Result['CO_REPORTS_NBR'];
        Result['CUSTOM_NAME'] := Q.Result['CUSTOM_NAME'];
        Result['REPORT_WRITER_REPORTS_NBR'] := Q.Result['REPORT_WRITER_REPORTS_NBR'];
        Result.Post;
      end;
      Q.Result.Next;
    end;
  finally
    tmpDataSet.IndexName := '';
    tmpDataSet.DeleteIndex(SortIndex);
    tmpDataSet.IndexDefs.Update;
  end;
end;

procedure TevRemoteMiscRoutines.GetPayrollTaxTotals(const PrNbr: Integer; var Totals: Variant);
type
  DS_ROW_TYPES = (
    DS_FED_OASDI_TAX,
    DS_FED_OASDI_WAGES,
    DS_FED_OASDI_TIPS,
    DS_FED_EE_MEDICARE_WAGES,
    DS_FED_EE_MEDICARE_TAX,
    DS_FED_SHORTFALL,
    DS_FED_WAGES,
    DS_FED_TAX,
    DS_FED_EE_EIC_TAX,
    DS_FED_OR_CHECK_BACK_UP_WITHHOLDING,
    DS_STATE_WAGE,
    DS_STATE_SHORTFALL,
    DS_STATE_TAX,
    DS_STATE_EE_SDI_WAGE,
    DS_STATE_EE_SDI_SHORTFALL,
    DS_STATE_EE_SDI_TAX,
    DS_STATE_SUI_WAGE,
    DS_STATE_SUI_TAX,
    DS_LOCAL_WAGE,
    DS_LOCAL_TAX,
    DS_LOCAL_SHORTFALL
    );
var
  s: String;
  ManualOverride: String;
  Payroll_TAX_Master, Payroll_TAX_Detail: TevClientDataSet;
  TAX_Master_ID: Integer;  

  procedure AddToTAX_MASTERTable(RowType: DS_ROW_TYPES; Description: string;
    Amount: Currency);
  var
    CO_STATES_NBR: integer;
    CO_LOCAL_TAX_NBR: integer;
    CO_SUI_NBR: integer;
    V: Variant;
  begin
    CO_STATES_NBR := 0;
    CO_LOCAL_TAX_NBR := 0;
    CO_SUI_NBR := 0;

    begin

      case RowType of
      DS_STATE_WAGE,
      DS_STATE_SHORTFALL,
      DS_STATE_TAX,
      DS_STATE_EE_SDI_WAGE,
      DS_STATE_EE_SDI_SHORTFALL,
      DS_STATE_EE_SDI_TAX:
      begin
        V := '';
        if DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value, []) then
          V := DM_EMPLOYEE.EE_STATES.FieldByName('STATE_LOOKUP').AsString;
        Description := 'STATE (' + V + ') ' + Description;
        CO_STATES_NBR := DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').AsInteger;
      end;

      DS_STATE_SUI_WAGE,
      DS_STATE_SUI_TAX:
      begin
        CO_SUI_NBR := DM_PAYROLL.PR_CHECK_SUI.FieldByName('CO_SUI_NBR').AsInteger;

        if not DM_COMPANY.CO_SUI.Locate('CO_SUI_NBR', CO_SUI_NBR, [loCaseInsensitive]) then
          raise EInconsistentData.CreateHelp('Co_SUI not found', IDH_InconsistentData);

        CO_STATES_NBR := DM_COMPANY.CO_SUI.FieldByName('CO_STATES_NBR').AsInteger;
        V := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CO_STATES_NBR, 'STATE');
        Description := 'STATE (' + string(V) + ') ' + DM_COMPANY.CO_SUI.FieldByName('Description').AsString + ' ' + Description;
      end;

      DS_LOCAL_WAGE,
      DS_LOCAL_TAX,
      DS_LOCAL_SHORTFALL:
      begin
        if DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value, []) then
          V := DM_EMPLOYEE.EE_LOCALS.FieldByName('Local_Name_Lookup').AsString;
        Description := 'LOCAL (' + V + ') ' + Description;
        CO_LOCAL_TAX_NBR := DM_EMPLOYEE.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
      end;

      end;

      if not Payroll_TAX_Master.Locate('OVRD;RowType;CO_STATES_NBR;CO_LOCAL_TAX_NBR;CO_SUI_NBR',
        VarArrayOf([ManualOverride, RowType, CO_STATES_NBR, CO_LOCAL_TAX_NBR, CO_SUI_NBR]), [loCaseInsensitive]) then
      begin
        Tax_Master_ID := Tax_Master_ID + 1;
        Payroll_TAX_Master.Append;

        Payroll_TAX_Master.FieldByName('TAX_NBR').AsInteger := Tax_Master_ID;
        Payroll_TAX_Master.FieldByName('OVRD').AsString := ManualOverride;
        Payroll_TAX_Master.FieldByName('RowType').AsInteger := ord(RowType);
        Payroll_TAX_Master.FieldByName('Description').AsString := Description;
        Payroll_TAX_Master.FieldByName('CO_STATES_NBR').AsInteger := CO_STATES_NBR;
        Payroll_TAX_Master.FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX_NBR;
        Payroll_TAX_Master.FieldByName('CO_SUI_NBR').AsInteger := CO_SUI_NBR;
        Payroll_TAX_Master.FieldByName('Amount').AsCurrency := Amount;
        Payroll_TAX_Master.FieldByName('Count').AsInteger := 1;
        Payroll_TAX_Master.Post;
      end
      else
      begin
        Payroll_TAX_Master.Edit;
        Payroll_TAX_Master.FieldByName('COUNT').AsInteger := Payroll_TAX_Master.FieldByName('COUNT').AsInteger + 1;
        Payroll_TAX_Master.FieldByName('AMOUNT').AsCurrency := Payroll_TAX_Master.FieldByName('AMOUNT').AsCurrency + Amount;
        Payroll_TAX_Master.Post;
      end;

      Payroll_TAX_Detail.Append;
      Payroll_TAX_Detail.FieldByName('TAX_NBR').AsInteger := Payroll_TAX_Master.FieldByName('TAX_NBR').AsInteger;
      Payroll_TAX_Detail.FieldByName('Amount').AsCurrency := Amount;
      Payroll_TAX_Detail.FieldByName('PR_CHECK_NBR').AsInteger := PrNbr;
      Payroll_TAX_Detail.FieldByName('Name').AsString := DM_PAYROLL.PR_CHECK.FieldByName('Employee_Name_Calculate').AsString;
      Payroll_TAX_Detail.FieldByName('EE_Custom_Number').AsString := DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString;
      Payroll_TAX_Detail.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DIVISION_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DIVISION_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_BRANCH_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_BRANCH_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_TEAM_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_TEAM_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_COMPANY_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_COMPANY_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DIVISION_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DIVISION_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_BRANCH_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_BRANCH_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DEPARTMENT_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DEPARTMENT_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_TEAM_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_TEAM_NAME').AsString;
      Payroll_TAX_Detail.Post;
    end;
  end;

begin

  DM_PAYROLL.PR.DataRequired('PR_NBR = ' + IntToStr(PrNbr));
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger));
  DM_COMPANY.CO_SUI.DataRequired('CO_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger));
  DM_EMPLOYEE.EE_STATES.DataRequired('');
  DM_EMPLOYEE.EE_LOCALS.DataRequired('');

  DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR='+IntToStr(PrNbr));

  DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_NBR='+IntToStr(PrNbr));
  DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_NBR='+IntToStr(PrNbr));
  DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_NBR='+IntToStr(PrNbr));

  Payroll_TAX_Master := TevClientDataSet.Create(nil);
  Payroll_TAX_Detail := TevClientDataSet.Create(nil);
  try
    Payroll_TAX_Master.Name := 'TAX';
    Payroll_TAX_Master.FieldDefs.Add('TAX_NBR', ftInteger, 0);
    Payroll_TAX_Master.FieldDefs.Add('OVRD', ftString, 1);
    Payroll_TAX_Master.FieldDefs.Add('RowType', ftInteger, 0);
    Payroll_TAX_Master.FieldDefs.Add('Description', ftString, 30);
    Payroll_TAX_Master.FieldDefs.Add('CO_STATES_NBR', ftInteger, 0);
    Payroll_TAX_Master.FieldDefs.Add('CO_LOCAL_TAX_NBR', ftInteger, 0);
    Payroll_TAX_Master.FieldDefs.Add('CO_SUI_NBR', ftInteger, 0);
    Payroll_TAX_Master.FieldDefs.Add('Amount', ftCurrency, 0);
    Payroll_TAX_Master.FieldDefs.Add('Count', ftInteger, 0);
    Payroll_TAX_Master.CreateDataSet;
    Payroll_TAX_Master.LogChanges := False;

    Payroll_TAX_Detail.FieldDefs.Add('TAX_NBR', ftInteger, 0);
    Payroll_TAX_Detail.FieldDefs.Add('Amount', ftCurrency, 0);
    Payroll_TAX_Detail.FieldDefs.Add('PR_CHECK_NBR', ftInteger, 0);
    Payroll_TAX_Detail.FieldDefs.Add('Name', ftString, 40);
    Payroll_TAX_Detail.FieldDefs.Add('EE_Custom_Number', ftString, 9);
    Payroll_TAX_Detail.FieldDefs.Add('SOCIAL_SECURITY_NUMBER', ftString, 11);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_COMPANY_NUMBER', ftString, 20);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_DIVISION_NUMBER', ftString, 20);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_BRANCH_NUMBER', ftString, 20);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_DEPARTMENT_NUMBER', ftString, 20);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_TEAM_NUMBER', ftString, 20);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_COMPANY_NAME', ftString, 40);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_DIVISION_NAME', ftString, 40);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_BRANCH_NAME', ftString, 40);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_DEPARTMENT_NAME', ftString, 40);
    Payroll_TAX_Detail.FieldDefs.Add('CUSTOM_TEAM_NAME', ftString, 40);

    Payroll_TAX_Detail.CreateDataSet;
    Payroll_TAX_Detail.LogChanges := False;

    if DM_PAYROLL.PR_CHECK.IndexName = 'SORT' then
      s := ''
    else
      s := DM_PAYROLL.PR_CHECK.IndexFieldNames;
    s := StringReplace(s, 'Employee_Name_Calculate', 'NAME', [rfIgnoreCase]);
    Payroll_TAX_Detail.IndexFieldNames := s;
    if Payroll_TAX_Master.Active then
      Payroll_TAX_Master.EmptyDataSet;
    if Payroll_TAX_Detail.Active then
      Payroll_TAX_Detail.EmptyDataSet;
    TAX_Master_ID := 0;

    Payroll_TAX_Master.DisableControls;
    Payroll_TAX_Detail.DisableControls;


    DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_LOCALS.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_SUI.IndexFieldNames := 'PR_CHECK_NBR';

    DM_PAYROLL.PR_CHECK.First;
    DM_PAYROLL.PR_CHECK_STATES.First;
    DM_PAYROLL.PR_CHECK_LOCALS.First;
    DM_PAYROLL.PR_CHECK_SUI.First;
    ManualOverride := #9;

    DM_PAYROLL.PR_CHECK.First;
    while not DM_PAYROLL.PR_CHECK.EOF do
    begin
      if False then
        if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency = 0 then
          ManualOverride := 'N'
        else
          ManualOverride := 'Y';

      if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency <> 0) and
         (DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency < 0) then
        AddToTAX_MASTERTable(DS_FED_OASDI_TAX, 'FEDERAL OASDI TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency)
      else if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency <> 0 then
        AddToTAX_MASTERTable(DS_FED_OASDI_TAX, 'FEDERAL OASDI TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency)
      else if DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency <> 0 then
      begin
        AddToTAX_MASTERTable(DS_FED_OASDI_TAX, 'FEDERAL OASDI TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency);
        AddToTAX_MASTERTable(DS_FED_OASDI_WAGES, 'FEDERAL EE OASDI WAGES', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').AsCurrency);
        AddToTAX_MASTERTable(DS_FED_OASDI_TIPS, 'FEDERAL EE OASDI TIPS', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').AsCurrency);
      end;

      if False then
        if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency = 0 then
          ManualOverride := 'N'
        else
          ManualOverride := 'Y';

      if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency <> 0) and
         (DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency < 0) then
        AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_TAX, 'FEDERAL EE_MEDICARE_TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency)
      else if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency <> 0 then
        AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_TAX, 'FEDERAL EE_MEDICARE_TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency)
      else if DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency <> 0 then
      begin
        AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_TAX, 'FEDERAL EE_MEDICARE_TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency);
        AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_WAGES, 'FEDERAL EE_MEDICARE_TAXABLE_WAGES', DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsCurrency);
      end;

      if False then
        if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency = 0 then
          ManualOverride := 'N'
        else
          ManualOverride := 'Y';

      if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency <> 0) and
         (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency < 0) and
         (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
        AddToTAX_MASTERTable(DS_FED_TAX, 'FEDERAL_TAX', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency)
      else if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency <> 0) and
              (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
        AddToTAX_MASTERTable(DS_FED_TAX, 'FEDERAL_TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency)
      else if (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency <> 0) or
              (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').AsCurrency <> 0) then
      begin
        AddToTAX_MASTERTable(DS_FED_TAX, 'FEDERAL_TAX', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency);
        AddToTAX_MASTERTable(DS_FED_WAGES, 'FEDERAL_TAXABLE_WAGES', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').AsCurrency);
        if (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').AsCurrency <> 0) then
          AddToTAX_MASTERTable(DS_FED_SHORTFALL, 'FEDERAL_SHORTFALL', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').AsCurrency);
      end;

      if False then
        if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').AsCurrency = 0 then
          ManualOverride := 'N'
        else
          ManualOverride := 'Y';

      if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').AsCurrency <> 0) and
         (DM_PAYROLL.PR_CHECK.FieldByName('EE_EIC_TAX').AsCurrency >= 0) then
        AddToTAX_MASTERTable(DS_FED_EE_EIC_TAX, 'FEDERAL EE_EIC_TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').AsCurrency)
      else if DM_PAYROLL.PR_CHECK.FieldByName('EE_EIC_TAX').AsCurrency <> 0 then
        AddToTAX_MASTERTable(DS_FED_EE_EIC_TAX, 'FEDERAL EE_EIC_TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_EIC_TAX').AsCurrency);

      if False then
        if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsCurrency = 0 then
          ManualOverride := 'N'
        else
          ManualOverride := 'Y';

      if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsCurrency <> 0 then
        AddToTAX_MASTERTable(DS_FED_OR_CHECK_BACK_UP_WITHHOLDING, 'FEDERAL OR_CHECK_BACK_UP_WITHHOLDING', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsCurrency);

      while not DM_PAYROLL.PR_CHECK_STATES.Eof and
            (DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
        DM_PAYROLL.PR_CHECK_STATES.Next;

      while not DM_PAYROLL.PR_CHECK_STATES.EOF and
            (DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
      begin

        if False then
          if DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency <> 0) and
           (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency < 0) and
           (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
          AddToTAX_MASTERTable(DS_STATE_TAX, 'TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency)
        else if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency <> 0) and
                (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
          AddToTAX_MASTERTable(DS_STATE_TAX, 'TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency)
        else if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency <> 0) or
                (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').AsCurrency <> 0) then
        begin
          AddToTAX_MASTERTable(DS_STATE_TAX, 'TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_STATE_WAGE, 'WAGE', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').AsCurrency);

          if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').AsCurrency <> 0) then
            AddToTAX_MASTERTable(DS_STATE_SHORTFALL, 'SHORTFALL', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').AsCurrency);

        end;

        if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').AsCurrency <> 0) or
           (DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').AsCurrency <> 0) then
        begin
          AddToTAX_MASTERTable(DS_STATE_EE_SDI_TAX, 'SDI TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_STATE_EE_SDI_WAGE, 'SDI WAGE', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').AsCurrency);
          if DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').AsCurrency <> 0 then
            AddToTAX_MASTERTable(DS_STATE_EE_SDI_SHORTFALL, 'SDI SHORTFALL', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').AsCurrency);
        end;

        DM_PAYROLL.PR_CHECK_STATES.Next;
      end;

      while not DM_PAYROLL.PR_CHECK_LOCALS.Eof and
            (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
        DM_PAYROLL.PR_CHECK_LOCALS.Next;

      while not DM_PAYROLL.PR_CHECK_LOCALS.EOF and
            (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
      begin

        if False then
          if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency <> 0) and
           (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency < 0) then
          AddToTAX_MASTERTable(DS_LOCAL_TAX, 'TAX', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency)
        else if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency <> 0 then
          AddToTAX_MASTERTable(DS_LOCAL_TAX, 'TAX', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency)
        else if (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency <> 0) or
                (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').AsCurrency <> 0) then
        begin
          AddToTAX_MASTERTable(DS_LOCAL_TAX, 'TAX', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_LOCAL_WAGE, 'WAGE', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').AsCurrency);
          if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').AsCurrency <> 0 then
            AddToTAX_MASTERTable(DS_LOCAL_SHORTFALL, 'SHORTFALL', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').AsCurrency);
        end;
        DM_PAYROLL.PR_CHECK_LOCALS.Next;
      end;

      while not DM_PAYROLL.PR_CHECK_SUI.Eof and
            (DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
        DM_PAYROLL.PR_CHECK_SUI.Next;

      while not DM_PAYROLL.PR_CHECK_SUI.EOF and
            (DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
      begin
        if DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').AsCurrency <> 0 then
        begin
          AddToTAX_MASTERTable(DS_STATE_SUI_TAX, 'TAX', DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_STATE_SUI_WAGE, 'WAGE', DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').AsCurrency);
        end;
        DM_PAYROLL.PR_CHECK_SUI.Next;

      end;

      DM_PAYROLL.PR_CHECK.Next;
    end;

    Totals := Payroll_Tax_Master.Data;

  finally
    Payroll_TAX_Master.Free;
    Payroll_TAX_Detail.Free;
  end;
end;

procedure TevRemoteMiscRoutines.GetPayrollEDTotals(const PrNbr: Integer;
   var Totals: Variant; var Checks: Integer; var Hours, GrossWages, NetWages: Currency);
var
  Payroll_ED_Master: TevClientDataset;
begin
  Hours := 0;
  GrossWages := 0;
  NetWages := 0;
  Payroll_ED_Master := TevClientDataset.Create(nil);
  try
//    Payroll_ED_Master.FieldDefs.Add('ED_NBR', ftInteger);
    Payroll_ED_Master.FieldDefs.Add('ED_CODE', ftString, 10);
    Payroll_ED_Master.FieldDefs.Add('DESCRIPTION', ftString, 20);
//    Payroll_ED_Master.FieldDefs.Add('CL_E_DS_NBR', ftInteger);
    Payroll_ED_Master.FieldDefs.Add('HOURS_OR_PIECES', ftCurrency);
    Payroll_ED_Master.FieldDefs.Add('AMOUNT', ftCurrency);
    Payroll_ED_Master.FieldDefs.Add('COUNT', ftInteger);
    Payroll_ED_Master.CreateDataSet;
    Payroll_ED_Master.LogChanges := False;

    DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR='+IntToStr(PrNbr));    
    DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_NBR='+IntToStr(PrNbr));

    DM_PAYROLL.PR_CHECK.First;
    while not DM_PAYROLL.PR_CHECK.Eof do
    begin
      GrossWages := GrossWages + ConvertNull(DM_PAYROLL.PR_CHECK['GROSS_WAGES'], 0);
      NetWages := NetWages + ConvertNull(DM_PAYROLL.PR_CHECK['NET_WAGES'], 0);
      DM_PAYROLL.PR_CHECK.Next;
    end;

    Checks := DM_PAYROLL.PR_CHECK.RecordCount;

    DM_PAYROLL.PR_CHECK_LINES.First;
    while not DM_PAYROLL.PR_CHECK_LINES.EOF do
    begin
      if not DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'], []) then
         raise EInconsistentData.CreateHelp('Could not find cl_e_ds', IDH_InconsistentData);
      if not Payroll_ED_Master.Locate('ED_CODE', DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER'], []) then
      begin
        Payroll_ED_Master.Append;
        Payroll_ED_Master.FieldByName('ED_CODE').AsString := DM_CLIENT.CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
        Payroll_ED_Master.FieldByName('DESCRIPTION').AsString := DM_CLIENT.CL_E_DS.FieldByName('DESCRIPTION').AsString;
//        Payroll_ED_Master.FieldByName('CL_E_DS_NBR').AsInteger := DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
      end
      else
        Payroll_ED_Master.Edit;

      Payroll_ED_Master.FieldByName('COUNT').AsInteger := Payroll_ED_Master.FieldByName('COUNT').AsInteger+1;

      if not DM_PAYROLL.PR_CHECK_LINES.AMOUNT.IsNull then
      begin
        if Payroll_ED_Master.FieldByName('AMOUNT').IsNull then
          Payroll_ED_Master['AMOUNT'] := 0;
        Payroll_ED_Master['AMOUNT'] := Payroll_ED_Master['AMOUNT'] + DM_PAYROLL.PR_CHECK_LINES['AMOUNT'];
      end;

      if not DM_PAYROLL.PR_CHECK_LINES.HOURS_OR_PIECES.IsNull then
      begin
        Hours := Hours + DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'];
        if Payroll_ED_Master.FieldByName('HOURS_OR_PIECES').IsNull then
          Payroll_ED_Master['HOURS_OR_PIECES'] := 0;
        Payroll_ED_Master['HOURS_OR_PIECES'] := Payroll_ED_Master['HOURS_OR_PIECES'] + DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'];
      end;

      Payroll_ED_Master.Post;

      DM_PAYROLL.PR_CHECK_LINES.Next;
    end;
    Totals := Payroll_ED_Master.Data;
  finally
    Payroll_ED_Master.Free;
  end;
end;

procedure TevRemoteMiscRoutines.CreateEEChangeRequest(const AEEChangeRequest: IevEEChangeRequest);
var
  BlobField: TBlobField;
  DataChangePackage: IevDataChangePacket;
  tmpStream: IevDualStream;
  StorageTable: TevClientDataSet;
  Envelope: IisListOfValues;
  Q: IEvQuery;
  sType: String;
begin
  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee a where a.ee_nbr=:EENbr and {AsOfNow<a>} ');
  Q.Params.AddValue('EENbr', AEEChangeRequest.EENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(AEEChangeRequest.EENbr));

  CheckCondition(AEEChangeRequest.RequestData <> nil, 'Unsupported format of data change in request');
  DataChangePackage := AEEChangeRequest.RequestData;
  sType := EEChangeRequestTypeToString(AEEChangeRequest.EEChangeRequestType);

  tmpStream := TevDualStreamHolder.Create;
  Envelope := TisListOfValues.Create;
  Envelope.AddValue('1', DataChangePackage);
  Envelope.WriteToStream(tmpStream);
  tmpStream.Position := 0;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired('EE_NBR=' + IntToStr(AEEChangeRequest.EENbr) +
    ' and REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpHRChangeRequest) + '''');

  StorageTable.Append;
  StorageTable['EE_NBR'] := AEEChangeRequest.EENbr;
  StorageTable['REQUEST_TYPE'] := sType;
  BlobField := TBlobField(StorageTable.FieldByName('REQUEST_DATA'));
  BlobField.LoadFromStream(tmpStream.realStream);
  try
    StorageTable.Post;
  except
    StorageTable.Cancel;
    raise;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  // sending E-mail to managers
  try
    SendEMailToManagers(AEEChangeRequest.EENbr, sType);
  except
    on E: ESendEmailError do
      GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email', E.Message + #13#10 + GetErrorCallStack(E));
  end;
end;

function TevRemoteMiscRoutines.GetEEChangeRequestList(const AEENbr: integer; const AType: TevEEChangeRequestType): IisInterfaceList;
var
  StorageTable: TevClientDataSet;
  Q: IEvQuery;
  Request: IevEEChangeRequest;
  sFirstName, sLastName, sCustomEeNumber: String;
begin
  Result := TisInterfaceList.Create;

  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee a where a.ee_nbr=:EENbr and {AsOfNow<a>} ');
  Q.Params.AddValue('EENbr', AEENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(AEeNbr));

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired('EE_NBR=' + IntToStr(AEeNbr));

  StorageTable.Filtered := false;
  StorageTable.Filter := 'REQUEST_TYPE = ''' + EEChangeRequestTypeToString(AType) +''' and EE_NBR=' + intToStr(AEENbr);
  StorageTable.Filtered := true;

  while not StorageTable.eof do
  begin
    Request := LoadEEChangeRequestFromDataset(StorageTable);
    GetEENameByNbr(Request.EENbr, sFirstName, sLastName, sCustomEeNumber);
    Request.CustomEENumber := sCustomEeNumber;
    Request.EEFirstName := sFirstName;
    Request.EELastName := sLastName;
    Result.Add(Request);
    StorageTable.Next;
  end;
end;

function TevRemoteMiscRoutines.GetEEChangeRequestListForMgr(const AMgrNbr: integer;const AType: TevEEChangeRequestType): IisInterfaceList;
var
  Q: IEvQuery;
  Request: IevEEChangeRequest;
  sFirstName, sLastName, sCustomEeNumber: String;
begin
  Result := TisInterfaceList.Create;
  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee a where a.ee_nbr=:EENbr and {AsOfNow<a>} ');
  Q.Params.AddValue('EENbr', AMgrNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Manager not found. EE_NBR=' + intToStr(AMgrNbr));

  Q := TEvQuery.Create('select * from EE_CHANGE_REQUEST ' +
    ' where {AsOfNow<EE_CHANGE_REQUEST>}  and REQUEST_TYPE=:p_rt and EE_NBR in (SELECT EE_NBR FROM CO_GROUP_MEMBER b where {AsOfNow<b>} ' +
    ' and CO_GROUP_NBR in (select CO_GROUP_NBR from CO_GROUP c where {AsOfNow<c>} and ' +
    ' CO_GROUP_NBR in (select CO_GROUP_NBR from CO_GROUP_MANAGER d where {AsOfNow<d>} ' +
    ' and EE_NBR=:p_ee_nbr))) order by EE_NBR');
  Q.Params.AddValue('p_rt', EEChangeRequestTypeToString(AType));
  Q.Params.AddValue('p_ee_nbr', AMgrNbr);
  Q.Execute;

  while not Q.Result.eof do
  begin
    Request := LoadEEChangeRequestFromDataset(Q.Result.vclDataSet);
    GetEENameByNbr(Request.EENbr, sFirstName, sLastName, sCustomEeNumber);
    Request.CustomEENumber := sCustomEeNumber;
    Request.EEFirstName := sFirstName;
    Request.EELastName := sLastName;
    Result.Add(Request);
    Q.Result.Next;
  end;
end;

function TevRemoteMiscRoutines.ProcessEEChangeRequests(const ARequestsNbrs: IisStringList; const AOperation: boolean): IisStringList;
var
  StorageTable: TevClientDataSet;
  sNbr: String;
  Request: IevEEChangeRequest;
  bApplied: boolean;
begin
  Result := TisSTringList.Create;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired();

  StorageTable.First;
  while not StorageTable.eof do
  begin
    sNbr := IntToStr(StorageTable['EE_CHANGE_REQUEST_NBR']);
    if ARequestsNbrs.IndexOf(sNbr) <> -1 then
    begin
      if AOperation then
      begin
        // approve
        Request := LoadEEChangeRequestFromDataset(StorageTable);
        if Request.RequestData <> nil then
        begin
          // new format
          bApplied := false;
          ctx_DataAccess.StartNestedTransaction([dbtClient]);
          try
            ctx_DBAccess.ApplyDataChangePacket(Request.RequestData);
            ctx_DataAccess.CommitNestedTransaction;
            bApplied := true;
          except
            on E: Exception do
            begin
              ctx_DataAccess.RollbackNestedTransaction;
              Result.Add(sNbr);
            end;
          end;
          if bApplied then
            StorageTable.Delete
          else
            StorageTable.Next;
        end
        else
        begin
          // old format
          Result.Add(IntToStr(-StorageTable['EE_CHANGE_REQUEST_NBR']));  // returning minus Nbr as flag that this request has old format and it has to be processed on API side
          StorageTable.Next;
        end;
      end
      else  // deny
        StorageTable.Delete;
    end
    else  // not this request
      StorageTable.Next;
  end;  // while

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

procedure TevRemoteMiscRoutines.AddTOAForNewEE(const CoNbr, EENbr: integer);
begin
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    try
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.ClientID := ctx_DataAccess.ClientID;
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookupsEnabled := false;
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.RetrieveCondition := 'CO_NBR=' + IntToStr(CoNbr);

      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.ClientID := ctx_DataAccess.ClientID;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.LookupsEnabled := false;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.RetrieveCondition := 'EE_NBR=' + IntToStr(EENbr);

      DM_EMPLOYEE.EE.ClientId := ctx_DataAccess.ClientId;
      DM_EMPLOYEE.EE.LookupsEnabled := false;
      DM_EMPLOYEE.EE.RetrieveCondition := 'EE_NBR=' + IntToStr(EENbr);

      DM_COMPANY.CO_TIME_OFF_ACCRUAL.DataRequired(DM_COMPANY.CO_TIME_OFF_ACCRUAL.RetrieveCondition);
      DM_EMPLOYEE.EE.DataRequired(DM_EMPLOYEE.EE.RetrieveCondition);
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.DataRequired(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.RetrieveCondition);

      ctx_DataAccess.AddTOAForEE;

      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL]);
      ctx_DataAccess.CommitNestedTransaction;
    finally
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.LookupsEnabled := True;
      DM_EMPLOYEE.EE.LookupsEnabled := True;
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookupsEnabled := True;
    end;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

procedure TevRemoteMiscRoutines.AddEDsForNewEE(const CoNbr, EENbr: integer);
begin
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    try
      DM_CLIENT.CL_E_DS.ClientID := ctx_DataAccess.ClientID;
      DM_CLIENT.CL_E_DS.LookupsEnabled := false;
      DM_CLIENT.CL_E_DS.RetrieveCondition := '';

      DM_COMPANY.CO_E_D_CODES.ClientID := ctx_DataAccess.ClientID;
      DM_COMPANY.CO_E_D_CODES.LookupsEnabled := false;
      DM_COMPANY.CO_E_D_CODES.RetrieveCondition := 'CO_NBR=' +  IntToStr(CoNbr);

      DM_COMPANY.CO.ClientID := ctx_DataAccess.ClientID;
      DM_COMPANY.CO.LookupsEnabled := false;
      DM_COMPANY.CO.RetrieveCondition := 'CO_NBR=' + IntToSTr(CoNbr);

      DM_COMPANY.CO_STATES.ClientID := ctx_DataAccess.ClientID;
      DM_COMPANY.CO_STATES.LookupsEnabled := false;
      DM_COMPANY.CO_STATES.RetrieveCondition := 'CO_NBR=' +  IntToStr(CoNbr);

      DM_EMPLOYEE.EE.ClientID := ctx_DataAccess.ClientID;
      DM_EMPLOYEE.EE.LookupsEnabled := false;
      DM_EMPLOYEE.EE.RetrieveCondition := 'EE_NBR=' + IntToStr(EENbr);

      DM_EMPLOYEE.EE_STATES.ClientID := ctx_DataAccess.ClientID;
      DM_EMPLOYEE.EE_STATES.LookupsEnabled := false;
      DM_EMPLOYEE.EE_STATES.RetrieveCondition := 'EE_NBR=' + IntToStr(EENbr);

      DM_EMPLOYEE.EE_SCHEDULED_E_DS.ClientID := ctx_DataAccess.ClientID;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.LookupsEnabled := false;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition := 'EE_NBR=' + IntToStr(EENbr);

      DM_COMPANY.CO_E_D_CODES.DataRequired(DM_COMPANY.CO_E_D_CODES.RetrieveCondition);
      DM_CLIENT.CL_E_DS.DataRequired(DM_CLIENT.CL_E_DS.RetrieveCondition);
      DM_COMPANY.CO.DataRequired(DM_COMPANY.CO.RetrieveCondition);
      DM_COMPANY.CO_STATES.DataRequired(DM_COMPANY.CO_STATES.RetrieveCondition);
      DM_EMPLOYEE.EE.DataRequired(DM_EMPLOYEE.EE.RetrieveCondition);
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired(DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition);
      DM_EMPLOYEE.EE_STATES.DataRequired(DM_EMPLOYEE.EE_STATES.RetrieveCondition);

      DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', DM_EMPLOYEE.EE['HOME_TAX_EE_STATES_NBR'], []);

      ctx_DataAccess.AddEdsForEE;

      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
      ctx_DataAccess.CommitNestedTransaction;
    finally
      DM_EMPLOYEE.EE_STATES.LookupsEnabled := True;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.LookupsEnabled := True;
      DM_EMPLOYEE.EE.LookupsEnabled := True;
      DM_COMPANY.CO_STATES.LookupsEnabled := True;
      DM_COMPANY.CO.LookupsEnabled := True;
      DM_CLIENT.CL_E_DS.LookupsEnabled := True;
      DM_COMPANY.CO_E_D_CODES.LookupsEnabled := True;
    end;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevRemoteMiscRoutines.GetTOARequests (const AEENbr: integer; const Q: IEvQuery ) : IisInterfaceList;
var
  OneRequestStruct: IEvTOABusinessRequest;
  OneDayStruct: IEvTOARequest;
  ArrayOfDays: IisInterfaceList;
  PrevType: integer;
  PrevCode: String;
  PrevDate: TdateTime;
  PrevPrNbr: Variant;
  dRequestDate: TDateTime;
  sRequestCode: String;
  sRequestNotes: String;
  sRequestManagerNote: String;
  bIntervalIsBroken: boolean;
  iInterval: integer;
  i, T_EENbr: integer;
  iHours, iMinutes: integer;

  function CalculateCode(const ANewCode: String; const APrNbr: Variant): String;
  begin
    Result := ANewCode;
    if APrNbr <> null then
      Result := EE_TOA_OPER_CODE_USED;
  end;

  procedure FillNewRequest;
  var
    BalanceQuery: IEvQuery;
    TOADescriptionQuery: IEvQuery;
    sFirstName, sLastName, sCustomEeNumber: String;
    iHours, iMinutes: integer;
  begin
    OneRequestStruct := TEvTOABusinessRequest.Create;
    PrevType := Q.Result['EE_TIME_OFF_ACCRUAL_NBR'];
    PrevCode := Q.Result.FieldByName('OPERATION_CODE').AsString;
    PrevPrNbr := Q.Result.FieldByName('PR_NBR').AsString;
    sRequestCode := CalculateCode(Q.Result.FieldByName('OPERATION_CODE').AsString, Q.Result['PR_NBR']);
    PrevDate := Q.Result['ACCRUAL_DATE'];

    // calculating balance
    BalanceQuery := TevQuery.Create('TimeOffBalances');
    BalanceQuery.Macros.AddValue('COND', 'and e.ee_time_off_accrual_nbr= :Nbr');
    BalanceQuery.Params.AddValue('Nbr', Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger);
    BalanceQuery.Execute;
    CheckCondition(BalanceQuery.Result.RecordCount > 0, '"TimeOffBalances" returned no records');

    // request information
    dRequestDate := Q.Result['REQUEST_DATE'];
    sRequestNotes := Copy(VarToStr(Q.Result['Note']), Length(TOARequestNotesHeader) + 1, MaxInt);
    sRequestManagerNote := Q.Result.FieldByName('MANAGER_NOTE').AsString;

    if ( AEENbr = -1 ) and Assigned(Q.Result.vclDataSet.FindField('EE_NBR'))
    then T_EENbr := Q.Result['EE_NBR']
    else T_EENbr := AEENbr;

    OneRequestStruct.EENbr := T_EENbr;
    GetEENameByNbr(T_EENbr, sFirstName, sLastName, sCustomEeNumber);
    OneRequestStruct.FirstName := sFirstName;
    OneRequestStruct.LastName := sLastName;
    OneRequestStruct.CustomEENumber := sCustomEeNumber;
    OneRequestStruct.EETimeOffAccrualNbr := Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger;
    OneRequestStruct.Balance := BalanceQuery.Result.FieldByName('accrued').AsFloat - BalanceQuery.Result.FieldByName('used').AsFloat;
//    OneRequestStruct.Manager_Note := Q.Result.FieldByName('MANAGER_NOTE').AsString;

    if Assigned(Q.Result.vclDataSet.FindField('description')) then
       OneRequestStruct.Description := VarToStr(Q.Result['description'])
    else begin
      TOADescriptionQuery := TEvQuery.Create('select a.description from CO_TIME_OFF_ACCRUAL a, EE_TIME_OFF_ACCRUAL b ' +
        ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_TIME_OFF_ACCRUAL_NBR=b.CO_TIME_OFF_ACCRUAL_NBR ' +
        ' and b.EE_TIME_OFF_ACCRUAL_NBR=:P_TOANbr');
      TOADescriptionQuery.Params.AddValue('P_TOANbr', Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger);
      TOADescriptionQuery.Execute;
      CheckCondition(TOADescriptionQuery.Result.RecordCount > 0, 'Cannot find TOA description');
      OneRequestStruct.Description := VarToStr(TOADescriptionQuery.Result['description']);
    end;

    ArrayOfDays := OneRequestStruct.Items;

    // day information
    OneDayStruct := TEvTOARequest.Create(T_EENbr,  Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger);
    OneDayStruct.AccrualDate := Q.Result.FieldByName('ACCRUAL_DATE').AsDateTime;
    iHours := 0; iMinutes := 0;
    ConvertDoubleToIntegerHourAndMin(Q.Result['USED'], iHours, iMinutes);
    OneDayStruct.Hours := iHours;
    OneDayStruct.Minutes := iMinutes;
    OneDayStruct.EETimeOffAccrualOperNbr := Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_OPER_NBR').AsInteger;
    OneDayStruct.Status := Q.Result.FieldByName('OPERATION_CODE').AsString;
    OneDayStruct.CL_E_DS_NBR := Q.Result.FieldByName('CL_E_DS_NBR').AsInteger;

    ArrayOfDays.Add(OneDayStruct);
  end;

  procedure StoreRequestTotals;
  var
    i: integer;
    DayRequest: IEvTOARequest;
  begin
    OneRequestStruct.Date := dRequestDate;
    OneRequestStruct.Status := sRequestCode;
    OneRequestStruct.Notes := sRequestNotes;
    OneRequestStruct.Manager_Note := sRequestManagerNote;

    // correcting some fields
    for i := 0 to OneRequestStruct.Items.Count - 1 do
    begin
      DayRequest := IInterface(OneRequestStruct.Items[i]) as IEvTOARequest;
      DayRequest.Notes := sRequestNotes;
    end;
  end;

begin
  OneRequestStruct := nil;
  PrevType := -1;
  PrevDate := 0;
  PrevCode := '';
  PrevPrNbr := null;
  dRequestDate := 0;
  sRequestNotes := '';

  while not Q.Result.Eof do
  begin
    if Not Assigned(OneRequestStruct) then
      FillNewRequest
    else
    begin
      bIntervalIsBroken := false;
      iInterval := Abs(DaysBetween(Q.Result.FieldByName('ACCRUAL_DATE').AsDateTime, PrevDate));
      if iInterval > 1 then
        for i := 1 to iInterval - 1 do
          if not Context.PayrollCalculation.IsNonBusinessDay(PrevDate + i) then
          begin
            bIntervalIsBroken := true;
            break;
          end;
      if bIntervalIsBroken or (Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger <> PrevType)
         or (Q.Result.FieldByName('OPERATION_CODE').AsString <> PrevCode)
         or (Q.Result.FieldByName('PR_NBR').AsString <> PrevPrNbr) then
      begin
        // new type or interval is broken or day is used
        StoreRequestTotals;
        Result.Add(OneRequestStruct);
        FillNewRequest;
      end
      else
      begin
        // type is the same and interval is not broken
        if sRequestNotes = '' then
        begin
          sRequestNotes := Copy(VarToStr(Q.Result['Note']), Length(TOARequestNotesHeader) + 1, MaxInt);
          sRequestManagerNote := Q.Result.FieldByName('MANAGER_NOTE').AsString;
        end;

        if DateTimeCompare(Q.Result.FieldByName('REQUEST_DATE').AsDateTime, coGreater, dRequestDate) then
          dRequestDate := Q.Result.FieldByName('REQUEST_DATE').AsDateTime;
        PrevDate := Q.Result['ACCRUAL_DATE'];

        if ( AEENbr = -1 ) and Assigned(Q.Result.vclDataSet.FindField('EE_NBR'))
        then T_EENbr := Q.Result['EE_NBR']
        else T_EENbr := AEENbr;

        OneDayStruct := TEvTOARequest.Create(T_EENbr,  Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger);
        OneDayStruct.AccrualDate := Q.Result.FieldByName('ACCRUAL_DATE').AsDateTime;
        iHours := 0; iMinutes := 0;
        ConvertDoubleToIntegerHourAndMin(Q.Result['USED'], iHours, iMinutes);
        OneDayStruct.Hours := iHours;
        OneDayStruct.Minutes := iMinutes;
        OneDayStruct.EETimeOffAccrualOperNbr := Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_OPER_NBR').AsInteger;
        OneDayStruct.Status := CalculateCode(Q.Result.FieldByName('OPERATION_CODE').AsString, Q.Result['PR_NBR']);
        if sRequestCode <> EE_TOA_OPER_CODE_USED then
          sRequestCode := CalculateCode(Q.Result.FieldByName('OPERATION_CODE').AsString, Q.Result['PR_NBR']);
        OneDayStruct.CL_E_DS_NBR := Q.Result.FieldByName('CL_E_DS_NBR').AsInteger;
        ArrayOfDays.Add(OneDayStruct);
      end;
    end;
    Q.Result.Next;
  end;  // while
  if Assigned(OneRequestStruct) then
  begin
    StoreRequestTotals;
    Result.Add(OneRequestStruct);
  end;
end;

function  TevRemoteMiscRoutines.GetTOARequestsForCO(const CoNBR:Integer; const Codes: String;
          const FromDate, ToDate : TDateTime; const Manager, TOType, EECode: Variant): IisInterfaceList;
const
  csql: String =
  ' select EE_TIME_OFF_ACCRUAL_OPER_NBR, ACCRUAL_DATE, USED, NOTE, OPERATION_CODE, PR_NBR, '+
  ' a.EE_TIME_OFF_ACCRUAL_NBR,  a.REQUEST_DATE, b.EE_NBR, C.DESCRIPTION, a.CL_E_DS_NBR, MANAGER_NOTE '+
  ' from EE_TIME_OFF_ACCRUAL_OPER a, EE_TIME_OFF_ACCRUAL b, CO_TIME_OFF_ACCRUAL c '+
  ' where b.EE_TIME_OFF_ACCRUAL_NBR = a.EE_TIME_OFF_ACCRUAL_NBR '+
  ' and c.CO_TIME_OFF_ACCRUAL_NBR = b.CO_TIME_OFF_ACCRUAL_NBR '+
  ' and c.co_nbr = :CONBR and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} '+
  ' and b.status = ''Y''  '+
  ' %s '+
  ' order by ee_time_off_accrual_nbr, operation_code, accrual_date ';


  function GetFilter: String;
  var
    Q: IevQuery;
  begin
    Result := '';
    if not VarIsNull(EECode) then
    begin
      Q := TevQuery.Create('select EE_NBR from EE where {AsOfNow<EE>} and trim(CUSTOM_EMPLOYEE_NUMBER)='''+EECode+''' and CO_NBR='+
        IntToStr(CoNBR));
      if not Q.Result.Eof then
        Result := Result + 'b.EE_NBR='+Q.Result.FieldByName('EE_NBR').AsString;
    end;
    if not VarIsNull(TOType) then
    begin
      if Length(Result) > 0 then
        Result := Result + ' and ';
      Result := Result + 'c.CO_TIME_OFF_ACCRUAL_NBR='+TOType;
    end;

    if not VarIsNull(Manager) then
    begin
      if Length(Result) > 0 then
        Result := Result + ' and ';
      Result := Result +
        ' b.EE_NBR in (select r.EE_NBR ' +
        'from CO_GROUP_MANAGER m ' +
        'join CO_GROUP g on g.CO_GROUP_NBR=m.CO_GROUP_NBR ' +
        'join CO_GROUP_MEMBER r on r.CO_GROUP_NBR=g.CO_GROUP_NBR ' +
        'where {AsOfNow<m>} and {AsOfNow<g>} and {AsOfNow<r>} ' +
        'and g.CO_NBR=c.CO_NBR ' +
        'and m.CO_GROUP_MANAGER_NBR=' + Manager+')';
    end;
  end;

var
  Q: IEvQuery;
  code_list, manager_filter, used_filter, oper_code_filter, date_filter, condition: String;
  i: Integer;
begin
  Result := TisInterfaceList.Create;

  manager_filter := GetFilter;
  oper_code_filter := '';
  date_filter := '';
  code_list := Codes;

  if pos(EE_TOA_OPER_CODE_USED, code_list) > 0 then
  begin
    used_filter := '((a.operation_code = '''+EE_TOA_OPER_CODE_REQUEST_APPROVED+''') and (PR_NBR IS NOT null))';
    Delete(code_list, pos(EE_TOA_OPER_CODE_USED, code_list), 1);
  end;

  if Length(code_list) > 0 then
  begin
    oper_code_filter :=  'a.operation_code in ('''+copy(code_list,1,1)+'''';
    for i := 2 to Length(code_list) do
      oper_code_filter := oper_code_filter + ',''' + copy(code_list,i,1)+'''';
    oper_code_filter := oper_code_filter + ')';

    if pos(EE_TOA_OPER_CODE_REQUEST_APPROVED, code_list) > 0 then
      oper_code_filter := oper_code_filter + ' and (PR_NBR is null)';

    if used_filter <> '' then
      oper_code_filter := '('+used_filter+' or '+oper_code_filter+')';
  end
  else
    oper_code_filter := used_filter;

  if Trunc(FromDate) <> 0 then
    date_filter := 'a.REQUEST_DATE >= :FromDate and a.REQUEST_DATE < :ToDate';

  condition := '';

  if manager_filter <> '' then
    condition := condition + ' and '+ manager_filter;

  if oper_code_filter <> '' then
    condition := condition + ' and ' + oper_code_filter;

  if date_filter <> '' then
    condition := condition + ' and ' + date_filter;

  Q := TevQuery.Create(Format(csql, [condition]));
  Q.Params.AddValue('CONBR', IntTostr(CoNBR));


  if Trunc(FromDate) <> 0 then
  begin
    Q.Params.AddValue('FromDate', Trunc(FromDate));
    Q.Params.AddValue('Todate', Trunc(ToDate) + 1);
  end;

   Q.Execute;

  if Q.Result.RecordCount <> 0 then
    Result := GetTOARequests(-1, Q);

end;

procedure TevRemoteMiscRoutines.UpdateTOARequestsForCO(const Requests: IisInterfaceList; const CustomCoNbr:string);

var
  i: integer;
  iEENbr: integer;
  StorageTable : TevClientDataSet;
  sMessageForEE, sMessageForManager: String;
  sTypeOffType: String;
  sMgrName: String;
  sOrigin: String;
  RegTOABusiness : IEvTOABusinessRequest;
  Request  : IEvTOARequest;
  sNotes: String;
  sCondition, sSubject, sMessage : String;
begin
  CheckCondition(Requests.Count > 0, 'Request is empty');

  RegTOABusiness :=  IInterface(Requests[0]) as IEvTOABusinessRequest;
  sTypeOffType := RegTOABusiness.Description;

  CheckCondition(RegTOABusiness.Items.Count > 0, 'Request is empty');
  Request := IInterface(RegTOABusiness.Items[0]) as IEvTOARequest;
  iEENbr  := Request.EENbr;
  sNotes  := Request.Notes;

  if iEENbr = Abs(Context.UserAccount.InternalNbr) then
    sOrigin := 'E'
  else
    sOrigin := 'M';

  // checking manager's rights
  sMgrName := '';
  if sOrigin = 'M' then
    sMgrName := Context.UserAccount.FirstName + ' ' + Context.UserAccount.LastName;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_TIME_OFF_ACCRUAL_OPER'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  sMessageForManager := '';
  sMessageForEE := '';

  if sOrigin = 'E' then
    sMessageForManager := sMessageForManager + 'Employee edited an existing request' + #13#10#13#10 +
      'Time Off Type:' + sTypeOffType + #13#10
  else
    sMessageForEE := sMessageForEE + 'Your Time Off request has been changed by ' + sMgrName +
      #13#10#13#10 + 'Time Off Type:' + sTypeOffType + #13#10 + 'Notes: ' + sNotes + #13#10;

  sCondition := 'EE_TIME_OFF_ACCRUAL_OPER_NBR in (';
  for i := 0 to RegTOABusiness.Items.Count - 1 do
  begin
    Request := IInterface(RegTOABusiness.Items[i]) as IEvTOARequest;
    sCondition := sCondition + IntToStr(Request.EETimeOffAccrualOperNbr) + ',';
  end;
  sCondition := Copy(sCondition, 1, Length(sCondition) - 1) + ')';
  StorageTable.DataRequired(sCondition);

  for i := 0 to RegTOABusiness.Items.Count - 1 do
  begin
    Request := IInterface(RegTOABusiness.Items[i]) as IEvTOARequest;
    sMessageForEE := sMessageForEE + '  ' + DateToStr(Request.AccrualDate) + ' - ' + IntToStr(Request.Hours) + ' hr' +
      IntToStr(Request.Minutes) + ' min' + #13#10;
    CheckCondition(StorageTable.Locate('EE_TIME_OFF_ACCRUAL_OPER_NBR', Request.EETimeOffAccrualOperNbr, []), 'Cannot find record to edit: ' + IntToStr(Request.EETimeOffAccrualOperNbr));

    CheckCondition((StorageTable['PR_NBR'] = null) and (StorageTable['PR_BATCH_NBR'] = null) and ((StorageTable['PR_CHECK_NBR'] = null)),
      'You cannot change requests, if they are assigned to payroll.');

    if (Request.Hours <> 0) or (Request.Minutes <> 0) then
    begin
      StorageTable.Edit;
      StorageTable['EE_TIME_OFF_ACCRUAL_NBR'] := Request.EETimeOffAccrualNbr;
      StorageTable['ACCRUAL_DATE'] := Request.AccrualDate;
      StorageTable['ACCRUED'] := 0;
      StorageTable['USED'] := Request.HoursAndMinAsDouble;
      StorageTable['NOTE'] := TOARequestNotesHeader + sNotes;
      StorageTable.Post;
    end
    else
      StorageTable.Delete;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
 if sOrigin = 'E' then
 begin   //sendEmailtoManager
   sSubject := 'Request for Time Off from employee';
   sMessage := sSubject+ #13#10 + 'Employee''s information:' + #13#10 +
      '  Custom Company Number: ' + CustomCoNbr + #13#10 +
      '  Custom Employee Number: ' + RegTOABusiness.CustomEENumber + #13#10 +
      '  First Name: ' + RegTOABusiness.FirstName + #13#10 +
      '  Last Name: ' + RegTOABusiness.LastName + #13#10;
   try
     if sMessageForManager <> '' then
        ctx_EMailer.SendQuickMail(Context.UserAccount.EMail, 'EvolutionSelfServe', sSubject, sMessage + #13#10 + sMessageForManager)
     else
        ctx_EMailer.SendQuickMail(Context.UserAccount.EMail, 'EvolutionSelfServe', sSubject, sMessage);
   except
     on E: Exception do
       raise ESendEmailError.Create('Cannot send email to manager. From: EvolutionSelfServe' + '. To: ' +Context.UserAccount.EMail + #13#10 + 'Error:' + E.Message);
   end;
 end
 else
   try
     SendEmailToEmployeeAndManager(iEENbr, 'Change to Time Off Request', sMessageForEE);
   except
     on E: ESendEmailError do
       GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email', E.Message + #13#10 + GetErrorCallStack(E));
   end;
end;

function TevRemoteMiscRoutines.APIGetDataSet(const ClientId: Integer; sTableName, sTableCondition: string): TevClientDataSet;
// that function will be deleted on some day when all related API logic be moved to server side
var
  DataSetParams: TGetDataParams;
  dataSet: TevClientDataSet;
  callResult: TGetDataResults;
  dsWrapper: IExecDSWrapper;
  ms: IisStream;
  DS: string;
begin
  DataSetParams := TGetDataParams.Create;
  dataSet := TevClientDataSet.Create(nil);

  try
    if sTableName = '' then
    begin
      // Prepare custom query
      sTableName := 'SQL_QUERY';
      dsWrapper := TExecDSWrapper.Create( sTableCondition );
      DS := StringReplace(UpperCase(sTableCondition),' ','', [rfReplaceAll]);
      if Pos('FROMSY_', DS)>0 then
        DS := 'SY_CUSTOM_PROV'
      else if Pos('FROMSB', DS)>0 then
        DS := 'SB_CUSTOM_PROV'
      else if Pos('FROMTMP_', DS)>0 then
        DS := 'TEMP_CUSTOM_PROV'
      else
        DS := 'CL_CUSTOM_PROV';
      end
    else
    begin
      // Prepare one-table request with condition
      dsWrapper := TExecDSWrapper.Create('SELECT="' + Trim(sTableCondition) + '"');
      DS := UpperCase(sTableName) + '_PROV';
    end;

    DataSetParams.Write(DS, ClientId, dsWrapper);

    ms := Context.DBAccess.GetDataSets(DataSetParams);

    callResult := TGetDataResults.Create(ms);
    try
      callResult.Read(dataSet, True);
    finally
      callResult.Free;
    end;

    dataSet.Name := sTableName;
    dataSet.ProviderName := sTableName + '_PROV';
    Result := dataSet;
  finally
    DataSetParams.Free;
  end;
end;

procedure TevRemoteMiscRoutines.DeleteRowsInDataSets(const AClientNbr: Integer; const ATableNames, AConditions: IisStringList;
      ARowNbrs: IisListOfValues);
// ARowNbrs should contain IisStringLists with RowNbrs with the same index as ATableNames
// deletes rows from several tables in one transaction
var
  sTableName: String;
  sTableCondition: String;
  i, j: integer;
  RowsNbrsArray: IisStringList;
  dataSet: TevClientDataSet;
  dsList: TStringList;
  dsFieldValue: integer;
  aDS: TArrayDS;
  iPrevEENbr: integer;
  iPrevCoNbr: integer;
  Q: IevQuery;
begin
  CheckCondition((ATableNames.Count = AConditions.Count) and (ATableNames.Count = ARowNbrs.Count), 'Wrong parameters');
  CheckCondition(ATableNames.Count > 0, 'Table names list is empty');

  dsList := TStringList.Create;
  try
    for i := 0 to ATableNames.Count-1 do
    begin
      iPrevCoNbr := -1;
      sTableName := AnsiUpperCase(ATableNames[i]);
      RowsNbrsArray := IInterface(ARowNbrs.Values[i].Value) as IisStringList;
      sTableCondition := AConditions[i];

      dataSet := APIGetDataSet(AClientNbr, sTableName, sTableCondition);
      dsList.AddObject(sTableName, dataSet);

      iPrevEENbr := -1;
      for j := 0 to RowsNbrsArray.Count-1 do
      begin
        dsFieldValue := StrToInt(RowsNbrsArray[j]);
        if dataSet.Locate(sTableName + '_NBR', dsFieldValue, []) then
        begin
          //////////////////////////////////////////////////////////////////////////
          // checking company lock for some tables
          if AnsiSameText(sTableName, 'EE_RATES') then
          begin
            if iPrevEENbr <> dataSet['EE_NBR'] then
            begin
              iPrevEENbr := dataSet['EE_NBR'];
              Q := TevQuery.Create('select CO_NBR from EE a, EE_RATES b where {AsOfNow<a>} ' +
                ' and {AsOfNow<b>} and a.EE_NBR=b.EE_NBR and b.EE_RATES_NBR=' + IntToStr(dsFieldValue));
              Q.Execute;
              CheckCondition(Q.Result.RecordCount > 0, 'Company not found');
              if iPrevCoNbr <> Q.Result['CO_NBR'] then
              begin
                iPrevCoNbr := Q.Result['CO_NBR'];
                CheckCompanyLock(Q.Result['CO_NBR']);
              end;
            end;
          end;
          if AnsiSameText(sTableName, 'PR') then
          begin
            if iPrevCoNbr <> dataSet['CO_NBR'] then
            begin
              iPrevCoNbr := dataSet['CO_NBR'];
              if dataSet.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT then
                CheckCompanyLock(dataSet['CO_NBR'], dataSet.FieldByName('CHECK_DATE').AsDateTime);
            end;
          end;
          ///////////////////////////////////////////////////////////////////////////

          dataSet.Delete;
        end;
      end;
    end;

    SetLength(aDS, dsList.Count);
    for i := 0 to Pred(dsList.Count) do
    begin
      aDS[i] := TevClientDataSet(dsList.Objects[i]);
    end;
    ctx_DataAccess.PostDataSets(aDS);
  finally
    for i := 0 to Pred(dsList.Count) do
      dsList.Objects[i].Free;
    dsList.Free;
  end;
end;
procedure TevRemoteMiscRoutines.CheckCompanyLock(const ACoNbr: Variant; const AAsOfDate: TdateTime = 0;
  const ACheckForTaxPmtsFlag: boolean = false);
var
  dAsOfDate: TDateTime;
  LockDate: TDateTime;
  LockTaxPmtsFlag: boolean;
  TermCode: string;
  CoName: string;
  Q: IevQuery;
begin
  CheckCondition(ACoNbr <> null, 'CoNbr cannot be null');
  if AAsOfDate <> 0 then
    dAsOfDate := AAsOfDate
  else
    dAsOfDate := Date;

  Q := TevQuery.Create('select CUSTOM_COMPANY_NUMBER, LOCK_DATE, QTR_LOCK_FOR_TAX_PMTS, TERMINATION_CODE from CO a ' +
    ' where {AsOfNow<a>} and CO_NBR=:P1');
  Q.Params.AddValue('P1', ACoNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found');

  LockDate := ConvertNull(Q.Result['LOCK_DATE'], 0);
  CoName := ConvertNull(Q.Result['CUSTOM_COMPANY_NUMBER'],'');
  LockTaxPmtsFlag := ConvertNull(Q.Result['QTR_LOCK_FOR_TAX_PMTS'], 'Y')='Y';
  TermCode := ConvertNull(Q.Result['TERMINATION_CODE'], '');
  if TermCode = TERMINATION_SAMPLE then
    exit;
  if (LockDate <> 0) and (LockDate >= dAsOfDate)  then
    if (not ACheckForTaxPmtsFlag) or  (ACheckForTaxPmtsFlag  and LockTaxPmtsFlag = true) then
      raise EPeriodIsLocked.CreateHelp('The company "'+CoName+'" is locked up to ' + DateToStr(LockDate) + '. You can not make this change.', IDH_PeriodIsLocked);
end;
function TevRemoteMiscRoutines.GetCoCalendarDefaults(const ACoNbr: integer): IisListOfValues;
var
  sPayFrequency: String;
  bWeekly: boolean;
  bBIWeekly: boolean;
  bSemiMonthly: boolean;
  bMonthly: boolean;
  bQuarterly: boolean;
  CalendarDefaults: TCalendarDefaults;
  CheckDate, PeriodBegin, PeriodEnd, CheckDate2, PeriodBegin2, PeriodEnd2: TDate;
  Callin, Delivery, CallIn2, Delivery2: TDateTime;
  DAYS_PRIOR, DAYS_POST, DAYS_PRIOR2, DAYS_POST2: Integer;
  OneFreqList: IisListOfValues;

  function AdjastmentToString(const AAdjustment: integer): String;
  begin
    if AAdjustment > 0 then
      Result := 'F'
    else if AAdjustment < 0 then
      Result := 'B'
    else
      Result := 'K';
  end;

  function DefaultsAsListOfValues(const ACalendarDefaults: TCalendarDefaults; const ASemiMonthly: boolean): IisListOfValues;
  begin
    Result := TisListOfValues.Create;
    Result.AddValue('NUMBER_OF_DAYS_PRIOR', ACalendarDefaults.NUMBER_OF_DAYS_PRIOR);
    Result.AddValue('CALL_IN_TIME', ACalendarDefaults.CALL_IN_TIME);
    Result.AddValue('NUMBER_OF_DAYS_AFTER', ACalendarDefaults.NUMBER_OF_DAYS_AFTER);
    Result.AddValue('DELIVERY_TIME', ACalendarDefaults.DELIVERY_TIME);
    Result.AddValue('LAST_REAL_SCHEDULED_CHECK_DATE', ACalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE);
    Result.AddValue('PERIOD_BEGIN_DATE', ACalendarDefaults.PERIOD_BEGIN_DATE);
    Result.AddValue('PERIOD_END_DATE', ACalendarDefaults.PERIOD_END_DATE);
    Result.AddValue('NUMBER_OF_MONTHS', ACalendarDefaults.NUMBER_OF_MONTHS);
    Result.AddValue('FILLER', ACalendarDefaults.FILLER);
    Result.AddValue('MOVE_CHECK_DATE', AdjastmentToString(ACalendarDefaults.MOVE_CHECK_DATE));
    Result.AddValue('MOVE_CALLIN_DATE', AdjastmentToString(ACalendarDefaults.MOVE_CALLIN_DATE));
    Result.AddValue('MOVE_DELIVERY_DATE', AdjastmentToString(ACalendarDefaults.MOVE_DELIVERY_DATE));
    if ASemiMonthly then
    begin
      Result.AddValue('NUMBER_OF_DAYS_PRIOR2', ACalendarDefaults.NUMBER_OF_DAYS_PRIOR2);
      Result.AddValue('CALL_IN_TIME2', ACalendarDefaults.CALL_IN_TIME2);
      Result.AddValue('NUMBER_OF_DAYS_AFTER2', ACalendarDefaults.NUMBER_OF_DAYS_AFTER2);
      Result.AddValue('DELIVERY_TIME2', ACalendarDefaults.DELIVERY_TIME2);
      Result.AddValue('LAST_REAL_SCHED_CHECK_DATE2', ACalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2);
      Result.AddValue('PERIOD_BEGIN_DATE2', ACalendarDefaults.PERIOD_BEGIN_DATE2);
      Result.AddValue('PERIOD_END_DATE2', ACalendarDefaults.PERIOD_END_DATE2);
    end;
  end;

begin
  DM_CLIENT.CL.DataRequired();
  DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(ACoNbr));
  DM_TEMPORARY.TMP_CO.DataRequired('CO_NBR=' + IntToStr(ACoNbr));
  DM_COMPANY.CO_CALENDAR_DEFAULTS.DataRequired('CO_NBR=' + IntToStr(ACoNbr));

  try
    sPayFrequency := DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString;

    if sPayFrequency = '' then
      raise EInconsistentData.CreateHelp('Pay frequency field has been left blank, this field is required to create a calendar!',
         IDH_InconsistentData);

    bWeekly := HasWeekly(sPayFrequency[1]);
    bBIWeekly := HasBiWeekly(sPayFrequency[1]);
    bSemiMonthly := HasSemiMonthly(sPayFrequency[1]);
    bMonthly := HasMonthly(sPayFrequency[1]);
    bQuarterly := HasQuarterly(sPayFrequency[1]);

    Result := TisListOfValues.Create;

   //Weekly
    if bWeekly then
    begin
      if GetDefaultsFromTable(ACoNbr, CO_FREQ_WEEKLY, CalendarDefaults) then
        CheckCalendarDefaults(CalendarDefaults, CO_FREQ_WEEKLY)
      else
      begin
        GetWeekly_BiWeeklyDefaults(ACoNbr, 7, CO_FREQ_WEEKLY, CheckDate, PeriodBegin, PeriodEnd, Callin, Delivery, DAYS_PRIOR, DAYS_POST);
        CalendarDefaults.NUMBER_OF_MONTHS := 12;
        CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := CheckDate;
        CalendarDefaults.PERIOD_BEGIN_DATE := PeriodBegin;
        CalendarDefaults.PERIOD_END_DATE := PeriodEnd;
        CalendarDefaults.CALL_IN_TIME := CallIn;
        CalendarDefaults.DELIVERY_TIME := Delivery;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR := DAYS_PRIOR;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER := DAYS_POST;
        CalendarDefaults.MOVE_CHECK_DATE := 0;
        CalendarDefaults.MOVE_CALLIN_DATE := 0;
        CalendarDefaults.MOVE_DELIVERY_DATE := 0;
        CalendarDefaults.FILLER := 'R';
      end;
      OneFreqList := DefaultsAsListOfValues(CalendarDefaults, false);
      Result.AddValue('W', OneFreqList);
    end;

    //Bi-Weekly
    if bBIWeekly then
    begin
      if GetDefaultsFromTable(ACoNbr, CO_FREQ_BWEEKLY, CalendarDefaults) then
        CheckCalendarDefaults(CalendarDefaults, CO_FREQ_BWEEKLY)
      else
      begin
        GetWeekly_BiWeeklyDefaults(ACoNbr, 14, CO_FREQ_BWEEKLY, CheckDate, PeriodBegin, PeriodEnd, Callin, Delivery, DAYS_PRIOR, DAYS_POST);
        CalendarDefaults.NUMBER_OF_MONTHS := 12;
        CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := CheckDate;
        CalendarDefaults.PERIOD_BEGIN_DATE := PeriodBegin;
        CalendarDefaults.PERIOD_END_DATE := PeriodEnd;
        CalendarDefaults.CALL_IN_TIME := CallIn;
        CalendarDefaults.DELIVERY_TIME := Delivery;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR := DAYS_PRIOR;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER := DAYS_POST;
        CalendarDefaults.MOVE_CHECK_DATE := 0;
        CalendarDefaults.MOVE_CALLIN_DATE := 0;
        CalendarDefaults.MOVE_DELIVERY_DATE := 0;
        CalendarDefaults.FILLER := 'R';
      end;
      OneFreqList := DefaultsAsListOfValues(CalendarDefaults, false);
      Result.AddValue('B', OneFreqList);
    end;

    //Semi-Montly
    if bSemiMonthly then
    begin
      if GetDefaultsFromTable(ACoNbr, CO_FREQ_SMONTHLY, CalendarDefaults) then
        CheckCalendarDefaults(CalendarDefaults, CO_FREQ_SMONTHLY)
      else
      begin
        GetSemiMonthlyDefaults(ACoNbr, CO_FREQ_SMONTHLY, CheckDate, PeriodBegin, PeriodEnd, CallIn, Delivery, CheckDate2, PeriodBegin2, PeriodEnd2, CallIn2, Delivery2, DAYS_PRIOR, DAYS_POST, DAYS_PRIOR2, DAYS_POST2);
        CalendarDefaults.NUMBER_OF_MONTHS := 12;
        CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := CheckDate;
        CalendarDefaults.PERIOD_BEGIN_DATE := PeriodBegin;
        CalendarDefaults.PERIOD_END_DATE := PeriodEnd;
        CalendarDefaults.CALL_IN_TIME := CallIn;
        CalendarDefaults.DELIVERY_TIME := Delivery;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR := DAYS_PRIOR;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER := DAYS_POST;
        CalendarDefaults.MOVE_CHECK_DATE := 0;
        CalendarDefaults.MOVE_CALLIN_DATE := 0;
        CalendarDefaults.MOVE_DELIVERY_DATE := 0;
        CalendarDefaults.FILLER := 'R';

        CalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2 := CheckDate2;
        CalendarDefaults.PERIOD_BEGIN_DATE2 := PeriodBegin2;
        CalendarDefaults.PERIOD_END_DATE2 := PeriodEnd2;
        CalendarDefaults.CALL_IN_TIME2 := Callin2;
        CalendarDefaults.DELIVERY_TIME2 := Delivery2;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR2 := DAYS_PRIOR2;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER2 := DAYS_POST2;
      end;
      OneFreqList := DefaultsAsListOfValues(CalendarDefaults, true);
      Result.AddValue('S', OneFreqList);
    end;

      //Monthly
    if bMonthly then
    begin
      if GetDefaultsFromTable(ACoNbr, CO_FREQ_MONTHLY, CalendarDefaults) then
        CheckCalendarDefaults(CalendarDefaults, CO_FREQ_MONTHLY)
      else
      begin
        GetMonthlyDefaults(ACoNbr, CO_FREQ_MONTHLY, CheckDate, PeriodBegin, PeriodEnd, CallIn, Delivery, DAYS_PRIOR, DAYS_POST);
        CalendarDefaults.NUMBER_OF_MONTHS := 12;
        CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := CheckDate;
        CalendarDefaults.PERIOD_BEGIN_DATE := PeriodBegin;
        CalendarDefaults.PERIOD_END_DATE := PeriodEnd;
        CalendarDefaults.CALL_IN_TIME := CallIn;
        CalendarDefaults.DELIVERY_TIME := Delivery;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR := DAYS_PRIOR;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER := DAYS_POST;
        CalendarDefaults.MOVE_CHECK_DATE := 0;
        CalendarDefaults.MOVE_CALLIN_DATE := 0;
        CalendarDefaults.MOVE_DELIVERY_DATE := 0;
        CalendarDefaults.FILLER := 'R';
      end;
      OneFreqList := DefaultsAsListOfValues(CalendarDefaults, false);
      Result.AddValue('M', OneFreqList);
    end;

    //Quarterly
    if bQuarterly then
    begin
      if GetDefaultsFromTable(ACoNbr, CO_FREQ_QUARTERLY, CalendarDefaults) then
        CheckCalendarDefaults(CalendarDefaults, CO_FREQ_QUARTERLY)
      else
      begin
        GetQuarterlyDefaults(ACoNbr, CO_FREQ_QUARTERLY, CheckDate, PeriodBegin, PeriodEnd, CallIn, Delivery, DAYS_PRIOR, DAYS_POST);
        CalendarDefaults.NUMBER_OF_MONTHS := 12;
        CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE := CheckDate;
        CalendarDefaults.PERIOD_BEGIN_DATE := PeriodBegin;
        CalendarDefaults.PERIOD_END_DATE := PeriodEnd;
        CalendarDefaults.CALL_IN_TIME := CallIn;
        CalendarDefaults.DELIVERY_TIME := Delivery;
        CalendarDefaults.NUMBER_OF_DAYS_PRIOR := DAYS_PRIOR;
        CalendarDefaults.NUMBER_OF_DAYS_AFTER := DAYS_POST;
        CalendarDefaults.MOVE_CHECK_DATE := 0;
        CalendarDefaults.MOVE_CALLIN_DATE := 0;
        CalendarDefaults.MOVE_DELIVERY_DATE := 0;
        CalendarDefaults.FILLER := 'R';
      end;
      OneFreqList := DefaultsAsListOfValues(CalendarDefaults, false);
      Result.AddValue('Q', OneFreqList);
    end;
  finally
    DM_COMPANY.CO_CALENDAR_DEFAULTS.Close;
    DM_TEMPORARY.TMP_CO.Close;
    DM_COMPANY.CO.Close;
    DM_CLIENT.CL.Close;
  end;
end;

function TevRemoteMiscRoutines.CreateCoCalendar(const ACoNbr: integer; const ACoSettings: IisListOfValues; const ABasedOn, AMoveCheckDate,
  AMoveCallInDate, AMoveDeliveryDate: String): IisStringList;
const
  M = ' calendar has check dates and batch dates in different years.  If this is not correct, you will need to recreate the calendar.';
var
  Calendar: TEvBasicClientDataSet;
  evcsHoliday: TevClientDataSet;
  WCrossed, BCrossed, SCrossed, MCrossed, QCrossed: Boolean;
  CalendarDefaults: TCalendarDefaults;
  i: integer;
  sFreq: String;
  FreqList: IisListOfValues;
  CheckDate, PeriodBegin,  PeriodEnd: TDate;
  CheckAlwaysLast, CheckAlwaysLast2, PeriodAlwaysLast, PeriodAlwaysLast2: Boolean;
  sAllFreq: String;

  function StringTotime(const AValue: String): TTime;
  var
    formatSettings: TFormatSettings;
  begin
    FillChar(formatSettings, SizeOf(formatSettings), 0);
    GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
    formatSettings.ShortTimeFormat := 'hh:nn:ss';
    formatSettings.TimeSeparator := ':';
    Result := StrToTime(AValue, formatSettings);
  end;

  function StringToAdjustment(const AValue: String): integer;
  begin
    if AValue = 'F' then
      Result := 1
    else if AValue = 'B' then
      Result := -1
    else if AValue = 'K' then
      Result := 0
    else
      raise Exception.Create('Wrong value for Adjustment: ' + AValue);
  end;

  function SettingFromListToRecord(const AFreq: String; const AList: IisListOfValues): TCalendarDefaults;
  begin
    Result.CO_NBR := ACoNbr;
    Result.MOVE_CHECK_DATE := StringToAdjustment(AMoveCheckDate);
    if ABasedOn = 'R' then
    begin
      Result.MOVE_CALLIN_DATE := StringToAdjustment(AMoveCallInDate);
      Result.MOVE_DELIVERY_DATE := StringToAdjustment(AMoveDeliveryDate);
    end;

    Result.NUMBER_OF_MONTHS := AList.Value['NUMBER_OF_MONTHS'];

    if AFreq = 'W' then
      Result.FREQUENCY := CO_FREQ_WEEKLY
    else if AFreq = 'B' then
      Result.FREQUENCY := CO_FREQ_BWEEKLY
    else if AFreq = 'S' then
      Result.FREQUENCY := CO_FREQ_SMONTHLY
    else if AFreq = 'M' then
      Result.FREQUENCY := CO_FREQ_MONTHLY
    else
      Result.FREQUENCY := CO_FREQ_QUARTERLY;

    Result.CALL_IN_TIME := StringTotime(AList.Value['CALL_IN_TIME']);
    Result.DELIVERY_TIME := StringTotime(AList.Value['DELIVERY_TIME']);
    Result.NUMBER_OF_DAYS_PRIOR := AList.Value['NUMBER_OF_DAYS_PRIOR'];
    Result.NUMBER_OF_DAYS_AFTER := AList.Value['NUMBER_OF_DAYS_AFTER'];

    if AFreq = 'S' then
    begin
      Result.CALL_IN_TIME2 := StringTotime(AList.Value['CALL_IN_TIME2']);
      Result.DELIVERY_TIME2 := StringTotime(AList.Value['DELIVERY_TIME2']);
      Result.NUMBER_OF_DAYS_PRIOR2 := AList.Value['NUMBER_OF_DAYS_PRIOR2'];
      Result.NUMBER_OF_DAYS_AFTER2 := AList.Value['NUMBER_OF_DAYS_AFTER2'];
    end;
  end;

begin
  Result := TisStringList.Create;
  CheckCondition((ABasedOn = 'R') or (ABasedOn = 'B'), 'Wrong value for BasedOn parameter: ' + ABasedOn);

  try
    WCrossed := False;
    BCrossed := False;
    SCrossed := False;
    MCrossed := False;
    QCrossed := False;

    DM_CLIENT.CL.DataRequired();
    DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(ACoNbr));
    CheckCondition(DM_COMPANY.CO.RecordCount > 0, 'Company not found. CL_NBR= ' + IntToStr(ctx_DataAccess.ClientId) + ' CO_NBR=' + IntToStr(ACoNbr));
    DM_TEMPORARY.TMP_CO.DataRequired('CO_NBR=' + IntToStr(ACoNbr));
    DM_COMPANY.CO_CALENDAR_DEFAULTS.DataRequired('CO_NBR=' + IntToStr(ACoNbr));

    evcsHoliday := TevClientDataSet.Create(nil);
    try
      GetHolidayList(evcsHoliday);
      Calendar := TEvBasicClientDataSet.Create(nil);
      try
        Calendar.FieldDefs.Add('PAY_FREQUENCY', ftString, 2, false);
        Calendar.FieldDefs.Add('CHECK_DATE', ftDate, 0, false);
        Calendar.FieldDefs.Add('PERIOD_BEGIN_DATE', ftDate, 0, false);
        Calendar.FieldDefs.Add('PERIOD_END_DATE', ftDate, 0, true);
        Calendar.FieldDefs.Add('CALL_IN_DATE', ftDateTime, 0, true);
        Calendar.FieldDefs.Add('DELIVERY_DATE', ftDateTime, 0, true);
        Calendar.FieldDefs.Add('LAST_REAL_CHECK_DATE', ftDate, 0, true);
        Calendar.FieldDefs.Add('LAST_REAL_CALL_IN_DATE', ftDateTime, 0, true);
        Calendar.FieldDefs.Add('LAST_REAL_DELIVERY_DATE', ftDateTime, 0, true);
        Calendar.FieldDefs.Add('DAYS_PRIOR', ftInteger, 0, true);
        Calendar.FieldDefs.Add('DAYS_POST', ftInteger, 0, true);
        Calendar.Open;

        sAllFreq := '';
        for i := 0 to ACoSettings.Count - 1 do
        begin
          sFreq := ACoSettings.Values[i].Name;
          CheckCondition((sFreq = 'W') or (sFreq = 'B') or (sFreq = 'S') or (sFreq = 'M') or (sFreq = 'Q'), 'Unknown frequency: ' + sFreq);

          if Pos(sFreq, sAllFreq) > 0 then
            raise Exception.Create('You cannot process the same frequency twice: ' + sFreq)
          else
            sAllFreq := sAllFreq + sFreq;

          FreqList := IInterface(ACoSettings.Values[i].Value) as IisListOfValues;
          CalendarDefaults := SettingFromListToRecord(sFreq, FreqList);
          CalendarDefaults.FILLER := ABasedOn;
          if (sFreq = 'W') or (sFreq = 'B') then
          begin
            CheckDate := FreqList.Value['LAST_REAL_SCHEDULED_CHECK_DATE'];
            PeriodBegin := FreqList.Value['PERIOD_BEGIN_DATE'];
            PeriodEnd := FreqList.Value['PERIOD_END_DATE'];
            CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBegin, PeriodEnd,false,false,True);
          end
          else if sFreq = 'S' then
          begin
            CheckDate := FreqList.Value['LAST_REAL_SCHEDULED_CHECK_DATE'];
            PeriodBegin := FreqList.Value['PERIOD_BEGIN_DATE'];
            PeriodEnd := FreqList.Value['PERIOD_END_DATE'];

            CheckAlwaysLast := FreqList.Value['CheckEndOfMonth'];
            CheckAlwaysLast2 := FreqList.Value['CheckEndOfMonth2'];
            PeriodAlwaysLast := FreqList.Value['PeriodEndOfMonth'];
            PeriodAlwaysLast2 := FreqList.Value['PeriodEndOfMonth2'];

            CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast, PeriodAlwaysLast,True);

            CheckDate := FreqList.Value['LAST_REAL_SCHED_CHECK_DATE2'];
            PeriodBegin := FreqList.Value['PERIOD_BEGIN_DATE2'];
            PeriodEnd := FreqList.Value['PERIOD_END_DATE2'];

            CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast2, PeriodAlwaysLast2,False);
          end
          else if (sFreq = 'M') or (sFreq = 'Q') then
          begin
            CheckDate := FreqList.Value['LAST_REAL_SCHEDULED_CHECK_DATE'];
            PeriodBegin := FreqList.Value['PERIOD_BEGIN_DATE'];
            PeriodEnd := FreqList.Value['PERIOD_END_DATE'];
            CheckAlwaysLast := FreqList.Value['CheckEndOfMonth'];
            PeriodAlwaysLast := FreqList.Value['PeriodEndOfMonth'];
            CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast, PeriodAlwaysLast,True);
          end;

          UpdateDefaultsByFrequency(CalendarDefaults);
        end; // for

        try
          DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + IntToStr(ACoNbr));
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DataRequired('ALL');

          AddToCalendar(Calendar, evcsHoliday, WCrossed, BCrossed, SCrossed, MCrossed, QCrossed);

          if (Pos('W', sAllFreq) > 0) and WCrossed then Result.Add('Weekly' + M);
          if (Pos('B', sAllFreq) > 0) and BCrossed then Result.Add('Bi-Weekly' + M);
          if (Pos('S', sAllFreq) > 0) and SCrossed then Result.Add('Semi-Monthly' + M);
          if (Pos('M', sAllFreq) > 0) and MCrossed then Result.Add('Monthly' + M);
          if (Pos('Q', sAllFreq) > 0) and  QCrossed then Result.Add('Quarterly' + M);

          try
            ctx_DataAccess.StartNestedTransaction([dbtClient]);
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_CALENDAR_DEFAULTS, DM_PAYROLL.PR_SCHEDULED_EVENT, DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH]);
            ctx_DataAccess.CommitNestedTransaction;
          except
            ctx_DataAccess.RollbackNestedTransaction;
            raise;
          end;
        finally
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Close;
          DM_PAYROLL.PR_SCHEDULED_EVENT.Close;
        end;
      finally
        FreeAndNil(Calendar);
      end;
    finally
      FreeAndNil(evcsHoliday);
    end;
  finally
    DM_COMPANY.CO_CALENDAR_DEFAULTS.Close;
    DM_TEMPORARY.TMP_CO.Close;
    DM_COMPANY.CO.Close;
    DM_CLIENT.CL.Close;
  end;
end;

procedure TevRemoteMiscRoutines.SetEeEmail(const AEENbr: integer; const ANewEMail: String);
var
  StorageTable: TevClientDataSet;
begin
  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    StorageTable.DataRequired('EE_NBR=' + IntToStr(AEENbr));

    CheckCondition(StorageTable.RecordCount > 0, 'Employee not found in EE table. EE_NBR=' + IntToStr(AEENbr));

    try
      StorageTable.Edit;
      if Trim(ANewEmail) = '' then
        StorageTable['E_MAIL_ADDRESS'] := null
      else
        StorageTable['E_MAIL_ADDRESS'] := ANewEmail;
      StorageTable.Post;
    except
      StorageTable.Cancel;
      raise;
    end;

    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevRemoteMiscRoutines.GetCoReportsParametersAsXML(const Co_nbr: integer; const Co_Reports_Nbr: integer): string;
var
  StorageTable: TevClientDataSet;
  RWParamInstance: TrwReportParams;
  oStream: IisStream;
begin
  Result := '';
  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('CO_REPORTS'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;

    StorageTable.DataRequired('CO_NBR=' + IntToStr(Co_nbr) +' and CO_REPORTS_NBR=' + IntToStr(Co_reports_Nbr));

    CheckCondition(StorageTable.RecordCount > 0, 'Record not found in CO_REPORTS where CO_NBR=' + IntToStr(Co_nbr) +' and CO_REPORTS_NBR=' + IntToStr(Co_reports_nbr));
    CheckCondition(StorageTable.RecordCount = 1, 'Multiple records cannot be updated in CO_REPORTS where CO_NBR=' + IntToStr(Co_nbr) +' and CO_REPORTS_NBR='  + IntToStr(Co_reports_nbr));

      oStream := TisStream.CreateInMemory();
      RWParamInstance := TrwReportParams.Create;
      try
        RWParamInstance.ReadFromBlobField(TBlobField(StorageTable.FieldByName('INPUT_PARAMS')));
        XMLRWParams.TXMLRWConvertor.SaveReportParametersToXML(RWParamInstance, oStream);  //.ReportParametersFromXML(XMLParameters, RWParamInstance);
        Result := oStream.AsString;
      finally
         RWParamInstance.Free;
      end;
end;

function TevRemoteMiscRoutines.StoreReportParametersFromXML(const Co_nbr: integer; const Co_Reports_Nbr: integer; const XMLParameters: String): integer;
var
  StorageTable: TevClientDataSet;
  RWParamInstance: TrwReportParams;
begin
  Result := 0;
  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('CO_REPORTS'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    StorageTable.DataRequired('CO_NBR=' + IntToStr(Co_nbr) +' and CO_REPORTS_NBR=' + IntToStr(Co_reports_nbr));

    CheckCondition(StorageTable.RecordCount > 0, 'Record not found in CO_REPORTS where CO_NBR=' + IntToStr(Co_nbr) +' and CO_REPORTS_NBR=' + IntToStr(Co_reports_nbr));
    CheckCondition(StorageTable.RecordCount = 1, 'Multiple records cannot be updated in CO_REPORTS where CO_NBR=' + IntToStr(Co_nbr) +' and CO_REPORTS_NBR='  + IntToStr(Co_reports_nbr));

    try
      RWParamInstance := TrwReportParams.Create;
      try
        XMLRWParams.TXMLRWConvertor.ReportParametersFromXML(XMLParameters, RWParamInstance);
        StorageTable.Edit;
        RWParamInstance.SaveToBlobField(TBlobField(StorageTable.FieldByName('INPUT_PARAMS')));
        StorageTable.Post;
        Assert(Result = 0);
        Result := StorageTable.FieldByName('CO_REPORTS_NBR').AsInteger;
      finally
         RWParamInstance.Free;
      end;
    except
      StorageTable.Cancel;
      raise;
    end;

    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevRemoteMiscRoutines.ConvertXMLtoRWParameters(
  const sXMLParameters: AnsiString): IIsStream;
var
  RWParamInstance: TrwReportParams;
begin
  Result := TisStream.CreateInMemory;
  RWParamInstance := TrwReportParams.Create;
  try
    XMLRWParams.TXMLRWConvertor.ReportParametersFromXML(sXMLparameters, RWParamInstance);
    RWParamInstance.SaveToStream(Result.RealStream);
    Result.Position := 0;
  finally
    RWParamInstance.Free;
  end;
end;


function  TevRemoteMiscRoutines.GetCoBenefitEnrollmentNbr(const ACategoryNbr: integer; const CategoryDesc: String; const bEvent: boolean): variant;
var
   Q: IevQuery;
   dNow: TDateTime;
   s:string;
begin
  result := null;
  dNow:= Trunc(Mainboard.TimeSource.GetDateTime);
  if bEvent then
    s:= ' CATEGORY_COVERAGE_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and CATEGORY_COVERAGE_END_DATE >=' + QuotedStr(DateToStr(dNow))
  else
    s:= ' ENROLLMENT_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and ENROLLMENT_END_DATE >=' + QuotedStr(DateToStr(dNow));

  s:= 'select CO_BENEFIT_ENROLLMENT_NBR from CO_BENEFIT_ENROLLMENT where CO_BENEFIT_CATEGORY_NBR = :cNbr and {AsOfNow<CO_BENEFIT_ENROLLMENT>} and ' + s;
  Q := TEvQuery.Create(s);
  Q.Params.AddValue('cNbr', ACategoryNbr);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
     result := Q.Result.Fields[0].AsVariant;
end;

function TevRemoteMiscRoutines.GetBenefitRequest(const ARequestNbr: integer): IisListOfValues;
var
  Q: IevQuery;
  BlobField: TBlobField;
  tmpStream: IevDualStream;
  iNbr: integer;
begin
  // it returns two additional values with datasets: 'EE_NBR' and 'SIGNATURE_NBR'
  Result := TisListOfValues.Create;

  Q := TEvQuery.Create('select EE_CHANGE_REQUEST_NBR, EE_NBR, REQUEST_DATA, SIGNATURE_NBR ' +
    ' from EE_CHANGE_REQUEST a where EE_CHANGE_REQUEST_NBR=:RequestNbr and {AsOfNow<a>} ' +
    ' and REQUEST_TYPE=:RequestType');
  Q.Params.AddValue('RequestNbr', ARequestNbr);
  Q.Params.AddValue('RequestType', EEChangeRequestTypeToString(tpBenefitRequest));
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Benefit Request not found. EE_CHANGE_REQUEST_NBR=' + intToStr(ARequestNbr));

  BlobField := TBlobField(Q.Result.FieldByName('REQUEST_DATA'));
  tmpStream := TevDualStreamHolder.CreateInMemory;
  BlobField.SaveToStream(tmpStream.RealStream);
  tmpStream.Position := 0;
  Result.ReadFromStream(tmpStream);

  iNbr := Q.Result['EE_NBR'];
  Result.AddValue('EE_NBR', iNbr);
  Result.AddValue('SIGNATURE_NBR', Q.Result['SIGNATURE_NBR']);
end;

function TevRemoteMiscRoutines.StoreBenefitRequest(const AEENbr: integer; const ARequestData: IisListOfValues;
  const ARequestNbr: integer): integer;
var
  Q: IevQuery;
  tmpStream: IevDualStream;
  StorageTable: TevClientDataSet;
  BlobField: TBlobField;
  BenefitMetaDataset, BenefitDataset: IevDataset;
  RequestInfo, qRequestInfo: TBenefitRequestDescription;
begin
  CheckCondition(AEENbr = Abs(Context.UserAccount.InternalNbr), 'Request was not created by current user');

  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr, BENEFITS_ENABLED from ee a where a.ee_nbr=:EENbr and {AsOfNow<a>} ');
  Q.Params.AddValue('EENbr', AEENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(AEENbr));
  CheckCondition(Q.Result['BENEFITS_ENABLED'] = GROUP_BOX_FULL_ACCESS, 'Benefits enrollment is not enabled for employee. EE_NBR=' + intToStr(AEENbr));

  tmpStream := TevDualStreamHolder.Create;
  ARequestData.WriteToStream(tmpStream);
  tmpStream.Position := 0;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired('EE_NBR=' + IntToStr(AEENbr) +
    ' and REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) + ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));

  if ARequestNbr > 0 then
  begin
    CheckCondition(StorageTable.RecordCount > 0, 'Benefit Request not found. EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
    CheckCondition(StorageTable['SIGNATURE_NBR'] = null, 'You cannot change submitted requests');
  end;

  BenefitMetaDataset := IInterface(ARequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;
  BenefitDataset := IInterface(ARequestData.Value['EE_HR_BENEFIT_REQ_DATASET']) as IevDataset;
  RequestInfo.CoBenefitNbr := BenefitDataset['CO_BENEFITS_NBR'];
  RequestInfo.CategoryType := BenefitDataset['CATEGORY_TYPE'];
  RequestInfo.Status := BenefitMetaDataset['REQUEST_STATUS'];
  RequestInfo.Declined := BenefitDataset['DECLINED'];
  CheckCondition(RequestInfo.Status = 'P', 'You can store penging requests only');

  if ARequestNbr < 0 then
  begin
    // checking for duplicates
    Q := TevQuery.Create('select DESCRIPTION from EE_CHANGE_REQUEST a where {AsOfNow<a>} ' +
      ' and EE_NBR=:p_ee_nbr and REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) + '''');
    Q.Params.Addvalue('p_ee_nbr', AEENbr);
    Q.Execute;
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      qRequestInfo := UnpackBenefitRequestDescription(Q.Result['DESCRIPTION']);
      if not RequestInfo.Declined then
        CheckCondition(Requestinfo.CoBenefitNbr <> qRequestinfo.CoBenefitNbr, 'Employee already has a benefit request for this benefit');
      CheckCondition(Requestinfo.CategoryType <> qRequestinfo.CategoryType, 'Employee already has a benefit request for this category');
      Q.Result.Next;
    end;
  end;

  if StorageTable.RecordCount = 0 then
  begin
    StorageTable.Append;
  end
  else
    StorageTable.Edit;

  Result := StorageTable['EE_CHANGE_REQUEST_NBR'];

  try
    StorageTable['EE_NBR'] := AEENbr;
    StorageTable['REQUEST_TYPE'] := EEChangeRequestTypeToString(tpBenefitRequest);
    StorageTable['DESCRIPTION'] := PackBenefitRequestDescription(RequestInfo);
    StorageTable['STATUS'] := 'P';
    StorageTable['STATUS_DATE'] := Mainboard.TimeSource.GetDateTime;;

    BlobField := TBlobField(StorageTable.FieldByName('REQUEST_DATA'));
    BlobField.LoadFromStream(tmpStream.realStream);
    StorageTable.Post;
  except
    StorageTable.Cancel;
    raise;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevRemoteMiscRoutines.GetBenefitRequestList(const AEENbr: integer): IisStringList;
var
  Q: IevQuery;
  iRequestNbr: integer;
  iSignatureNbr: integer;
begin
// each string has format: EE_CHANGE_REQUEST_NBR,SIGNATURE_NBR
// if SIGNATURE_NBR = null, it's value will be returned as -1
  Result := TisStringList.Create;

  Q := TEvQuery.Create('select EE_CHANGE_REQUEST_NBR, SIGNATURE_NBR ' +
    ' from EE_CHANGE_REQUEST a where a.EE_NBR=:EENbr and {AsOfNow<a>} ' +
    ' and REQUEST_TYPE=:RequestType');
  Q.Params.AddValue('EENbr', AEENbr);
  Q.Params.AddValue('RequestType', EEChangeRequestTypeToString(tpBenefitRequest));
  Q.Execute;

  Q.Result.First;
  while not Q.Result.Eof do
  begin
    iRequestNbr := Q.Result['EE_CHANGE_REQUEST_NBR'];
    if Q.Result['SIGNATURE_NBR'] <> null then
      iSignatureNbr := Q.Result['SIGNATURE_NBR']
    else
      iSignatureNbr := -1;
    Result.Add(IntToStr(iRequestNbr) + ',' + intToStr(iSignatureNbr));

    Q.Result.Next;
  end;
end;

function TevRemoteMiscRoutines.SendEMailToHR(const AEENbr: integer; const ASubject, AMessage: String; const AAttachements: IisListOfValues): IisListOfValues;
var
  qEE: IevQuery;
  S: String;
  EeCustomNbr: String;
  EECustomCoNbr: String;
  EeFirstName, EELastName: String;
  sSubject, sFrom, sMessage : String;
  Mes: IevMailMessage;
  i, j: integer;
  sFileName: String;
  Blob: IevDualStream;
  HRPersons: IisParamsCollection;
  OneHrInfo: IisListOfValues;

  function GetEmail(const AValue: Variant): String;
  var
    S: String;
  begin
    S := VarToStr(AValue);
    Result := GetPrevStrValue(S, Chr(9));
  end;
begin
  // Format of result: key is SB_USER_NBR,
  // value is string in such format: FIRST_NAME + ' ' + LAST_NAME + CHR(9) + EMAIL_ADDRESS
  CheckCondition(Context.UserAccount.InternalNbr < 0, 'This call is allowed for ESS users only');
  CheckCondition(Length(Trim(AMessage)) > 0, 'Message cannot be empty');

  // getting information about employee
  qEE := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR, ' +
    ' a.e_mail_address as EE_MAIL, b.e_mail_address as CL_PERSON_MAIL ' +
    ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
    ' and {AsOfNow<a>} and {AsOfNow<b>} and  {AsOfNow<c>}');
  qEE.Params.AddValue('EENbr', AEeNbr);
  qEE.Execute;
  CheckCondition(qEE.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + IntToStr(AEeNbr));

  Result := TisListOfValues.Create;

  HRPersons := GetHRPersonsForOpenedClient;

  CheckCondition(HRPersons.Count > 0, 'No HR personnel has been defined for your company');
  for i := 0 to HRPersons.Count - 1 do
  begin
    OneHrInfo := HRPersons[i];
    if Trim(OneHrInfo.Value['EMAIL_ADDRESS']) <> '' then
    begin
      S := OneHrInfo.Value['FIRST_NAME'] + ' '+ OneHrInfo.Value['LAST_NAME'] + Chr(9) + OneHrInfo.Value['EMAIL_ADDRESS'];
      Result.AddValue(IntToStr(OneHrInfo.Value['SB_USER_NBR']), S);
    end;
  end;
  CheckCondition(Result.Count > 0, 'No email addresses have been defined for HR personnel');

  EeCustomNbr := Trim(VarToStr(qEE.Result.Fields.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value));
  EECustomCoNbr := Trim(VarToStr(qEE.Result.Fields.FieldByName('CUSTOM_COMPANY_NUMBER').Value));
  EeFirstName := VarToStr(qEE.Result.Fields.FieldByName('FIRST_NAME').Value);
  EELastName := VarToStr(qEE.Result.Fields.FieldByName('LAST_NAME').Value);
  sFrom := 'EvolutionSelfServe';
  sSubject := 'HR Benefits email from employee';

  sMessage := ASubject + #13#10 + 'Employee''s information:' + #13#10 +
    '  Custom Company Number: ' + EECustomCoNbr + #13#10 +
    '  Custom Employee Number: ' + EeCustomNbr + #13#10 +
    '  First Name: ' + EeFirstName + #13#10 +
    '  Last Name: ' + EELastName + #13#10 +
    '----------------------------------------------------' + #13#10#13#10;
  sMessage := sMessage + AMessage;

  Mes := TevMailMessage.Create;
  Mes.AddressFrom := sFrom;
  Mes.AddressTo := GetEmail(Result.Values[0].Value);
  Mes.Subject := sSubject;
  Mes.Body := sMessage;

  if Result.Count > 1 then
  begin
    Mes.AddressCC := GetEmail(Result.Values[1].Value);
    for j := 2 to Result.Count - 1 do
      Mes.AddressCC := Mes.AddressCC + ';' + GetEmail(Result.Values[j].Value);
  end;

  if AAttachements.Count > 0 then
  begin
    for j := 0 to AAttachements.Count - 1 do
    begin
      sFileName := AAttachements.Values[j].Name;
      Blob := IInterface(AAttachements.Values[j].Value) as IevDualStream;
      Blob.Position := 0;
      Mes.AttachStream(sFileName, Blob);
    end;
  end;

  try
    ctx_EMailer.SendMail(Mes);
  except
    on E: Exception do
      raise ESendEmailError.Create('Cannot send email to HR personnel. From: ' + sFrom + '. To: ' + Mes.AddressTo + #13#10 + 'Error:' + E.Message);
  end;
end;

function TevRemoteMiscRoutines.GetEeExistingBenefits(const AEENbr: integer; const ACurrentOnly: boolean = True): IevDataset;
var
  bFound: boolean;
  qRates: IevQuery;
  qClED: IEvQuery;
  dNow: TDateTime;
  bPeriodOK: boolean;
begin
  dNow := Trunc(Mainboard.TimeSource.GetDateTime);

  Result := TevDataset.Create;
  Result.vclDataSet.FieldDefs.Add('CO_BENEFIT_CATEGORY_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('CATEGORY_TYPE', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('CATEGORY_TYPE_DESC', ftString, 40, false);

  Result.vclDataSet.FieldDefs.Add('CO_BENEFITS_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('BENEFIT_NAME', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('SHOW_RATES', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE1', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE2', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE3', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('REQUIRES_BENEFICIARIES', ftString, 1, true);
  Result.vclDataSet.FieldDefs.Add('REQUIRES_DOB', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('REQUIRES_SSN', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('REQUIRES_PCP', ftString, 1, false);

  Result.vclDataSet.FieldDefs.Add('CO_BENEFIT_SUBTYPE_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('DESCRIPTION', ftString, 40, false);

  Result.vclDataSet.FieldDefs.Add('CO_BENEFIT_RATES_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('MAX_DEPENDENTS', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('PERIOD_BEGIN', ftDate, 0, false);
  Result.vclDataSet.FieldDefs.Add('PERIOD_END', ftDate, 0, false);
  Result.vclDataSet.FieldDefs.Add('CO_RATE_TYPE', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('CO_EE_RATE', ftFloat, 0, false);
  Result.vclDataSet.FieldDefs.Add('CO_ER_RATE', ftFloat, 0, false);
  Result.vclDataSet.FieldDefs.Add('CO_COBRA_RATE', ftFloat, 0, false);

  Result.vclDataSet.FieldDefs.Add('EE_BENEFITS_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('COBRA_STATUS', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('ENROLLMENT_STATUS', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('TOTAL_PREMIUM_AMOUNT', ftFloat, 0, false);
  Result.vclDataSet.FieldDefs.Add('EE_RATE_TYPE', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('EE_EE_RATE', ftFloat, 0, false);
  Result.vclDataSet.FieldDefs.Add('EE_ER_RATE', ftFloat, 0, false);
  Result.vclDataSet.FieldDefs.Add('EE_COBRA_RATE', ftFloat, 0, false);
  Result.vclDataSet.FieldDefs.Add('BENEFIT_EFFECTIVE_DATE', ftDate, 0, false);
  Result.vclDataSet.FieldDefs.Add('EXPIRATION_DATE', ftDate, 0, false);
  Result.vclDataSet.FieldDefs.Add('COBRA_MEDICAL_PARTICIPANT', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('COBRA_SS_DISABILITY', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('COBRA_TERMINATION_EVENT', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('COBRA_QUALIFYING_EVENT_DATE', ftDate, 0, false);
  Result.vclDataSet.FieldDefs.Add('COBRA_REASON_FOR_REFUSAL', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('DEDUCTION_FREQUENCY', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO1', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO2', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO3', ftString, 40, false);

  Result.vclDataSet.FieldDefs.Add('CL_E_D_GROUPS_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('CL_E_D_GROUPS_NAME', ftString, 40, false);

  Result.vclDataSet.Open;

  DM_CLIENT.EE_BENEFITS.Close;
  DM_COMPANY.CO_BENEFIT_CATEGORY.Close;
  DM_COMPANY.CO_BENEFITS.Close;
  DM_COMPANY.CO_BENEFIT_SUBTYPE.Close;
  DM_EMPLOYEE.EE.DataRequired('EE_NBR=' + IntToStr(AEENbr));
  DM_CLIENT.EE_BENEFITS.DataRequired('EE_NBR=' + IntToStr(AEENbr));
  DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
  DM_COMPANY.CO_BENEFITS.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
  DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired('ALL');

  DM_CLIENT.EE_BENEFITS.First;
  while not DM_CLIENT.EE_BENEFITS.eof do
  begin
    if (DM_CLIENT.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'] <> null) or not ACurrentOnly then
    begin
      if ACurrentOnly then
      begin
        if (DM_CLIENT.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE'] <> null) and (DM_CLIENT.EE_BENEFITS['EXPIRATION_DATE'] <> null) then
          bPeriodOK := (dNow >= DM_CLIENT.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE']) and (dNow <= DM_CLIENT.EE_BENEFITS['EXPIRATION_DATE'])
        else if (DM_CLIENT.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE'] <> null) and (DM_CLIENT.EE_BENEFITS['EXPIRATION_DATE'] = null) then
          bPeriodOK := dNow >= DM_CLIENT.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE']
        else if (DM_CLIENT.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE'] = null) and (DM_CLIENT.EE_BENEFITS['EXPIRATION_DATE'] <> null) then
          bPeriodOK := dNow <= DM_CLIENT.EE_BENEFITS['EXPIRATION_DATE']
        else
          bPeriodOK := True;
      end
      else
        bPeriodOK := True;

      if bPeriodOK then
      begin
        Result.Append;

        // EE_BENEFITS
        Result['EE_BENEFITS_NBR'] := DM_CLIENT.EE_BENEFITS['EE_BENEFITS_NBR'];
        Result['COBRA_STATUS'] := DM_CLIENT.EE_BENEFITS['COBRA_STATUS'];
        Result['ENROLLMENT_STATUS'] := DM_CLIENT.EE_BENEFITS['ENROLLMENT_STATUS'];
        Result['TOTAL_PREMIUM_AMOUNT'] := DM_CLIENT.EE_BENEFITS['TOTAL_PREMIUM_AMOUNT'];
        Result['EE_RATE_TYPE'] := DM_CLIENT.EE_BENEFITS['RATE_TYPE'];
        Result['BENEFIT_EFFECTIVE_DATE'] := DM_CLIENT.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE'];
        Result['EXPIRATION_DATE'] := DM_CLIENT.EE_BENEFITS['EXPIRATION_DATE'];
        Result['COBRA_MEDICAL_PARTICIPANT'] := DM_CLIENT.EE_BENEFITS['COBRA_MEDICAL_PARTICIPANT'];
        Result['COBRA_SS_DISABILITY'] := DM_CLIENT.EE_BENEFITS['COBRA_SS_DISABILITY'];
        Result['COBRA_TERMINATION_EVENT'] := DM_CLIENT.EE_BENEFITS['COBRA_TERMINATION_EVENT'];
        Result['COBRA_QUALIFYING_EVENT_DATE'] := DM_CLIENT.EE_BENEFITS['COBRA_QUALIFYING_EVENT_DATE'];
        Result['COBRA_REASON_FOR_REFUSAL'] := DM_CLIENT.EE_BENEFITS['COBRA_REASON_FOR_REFUSAL'];
        Result['DEDUCTION_FREQUENCY'] := DM_CLIENT.EE_BENEFITS['DEDUCTION_FREQUENCY'];
        Result['ADDITIONAL_INFO1'] := DM_CLIENT.EE_BENEFITS['ADDITIONAL_INFO1'];
        Result['ADDITIONAL_INFO2'] := DM_CLIENT.EE_BENEFITS['ADDITIONAL_INFO2'];
        Result['ADDITIONAL_INFO3'] := DM_CLIENT.EE_BENEFITS['ADDITIONAL_INFO3'];

        //CO_BENEFIT_SUBTYPE
        if DM_CLIENT.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'] <> null then
        begin
          bFound := DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR', DM_CLIENT.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'], []);
          CheckCondition(bFound, 'CO_BENEFIT_SUBTYPE_NBR not found. EE_BENEFITS_NBR="' + DM_CLIENT.EE_BENEFITS.EE_BENEFITS_NBR.AsString + '"');
          Result['CO_BENEFIT_SUBTYPE_NBR'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR'];
          Result['DESCRIPTION'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['DESCRIPTION'];
        end;

        // CO_BENEFITS
        bFound := False;
        if DM_CLIENT.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'] <> null then
          bFound := DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFITS_NBR'], [])
        else if DM_CLIENT.EE_BENEFITS['CO_BENEFITS_NBR'] <> null then
          bFound := DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', DM_CLIENT.EE_BENEFITS['CO_BENEFITS_NBR'], []);

        if not (bFound and (DM_COMPANY.CO_BENEFITS.EE_BENEFIT.Value = 'Y')) then
        begin
          Result.vclDataset.Cancel;
          DM_CLIENT.EE_BENEFITS.Next;
          continue;
        end;

        Result['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
        Result['BENEFIT_NAME'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];
        Result['SHOW_RATES'] := DM_COMPANY.CO_BENEFITS['SHOW_RATES'];
        Result['ADDITIONAL_INFO_TITLE1'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE1'];
        Result['ADDITIONAL_INFO_TITLE2'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE2'];
        Result['ADDITIONAL_INFO_TITLE3'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE3'];
        Result['REQUIRES_BENEFICIARIES'] := DM_COMPANY.CO_BENEFITS['REQUIRES_BENEFICIARIES'];
        Result['REQUIRES_DOB'] := DM_COMPANY.CO_BENEFITS['REQUIRES_DOB'];
        Result['REQUIRES_SSN'] := DM_COMPANY.CO_BENEFITS['REQUIRES_SSN'];
        Result['REQUIRES_PCP'] := DM_COMPANY.CO_BENEFITS['REQUIRES_PCP'];

        // CO_BENEFIT_RATES
        if DM_CLIENT.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'] <> null then
        begin
          qRates := TevQuery.Create('select a.* from CO_BENEFIT_RATES a where {AsOfNow<a>} and a.CO_BENEFIT_SUBTYPE_NBR=:p1 ' +
            ' and a.PERIOD_BEGIN <= :p2 and a.PERIOD_END >= :p2');
          qRates.Params.AddValue('p1', DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR']);
          qRates.Params.AddValue('p2', Trunc(Mainboard.Timesource.GetDateTime));
          qRates.Execute;
          if qRates.Result.RecordCount = 0 then
          begin
            // if there's no rate for current date, let's take the latest
            qRates := TevQuery.Create('select a.* from CO_BENEFIT_RATES a where {AsOfNow<a>} and a.CO_BENEFIT_SUBTYPE_NBR=:p1 ' +
              ' order by a.PERIOD_BEGIN desc');
            qRates.Params.AddValue('p1', DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR']);
            qRates.Execute;
          end;
          CheckCondition(qRates.Result.RecordCount > 0, 'CO_BENEFIT_RATES_NBR not found. CO_BENEFIT_SUBTYPE_NBR="' + DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE_NBR.AsString + '"');
          Result['CO_BENEFIT_RATES_NBR'] := qRates.Result['CO_BENEFIT_RATES_NBR'];
          Result['MAX_DEPENDENTS'] := qRates.Result['MAX_DEPENDENTS'];
          Result['PERIOD_BEGIN'] := qRates.Result['PERIOD_BEGIN'];
          Result['PERIOD_END'] := qRates.Result['PERIOD_END'];
          Result['CL_E_D_GROUPS_NBR'] := qRates.Result['CL_E_D_GROUPS_NBR'];
          Result['CO_RATE_TYPE'] := qRates.Result['RATE_TYPE'];
          if qRates.Result['RATE_TYPE'] = BENEFIT_RATES_RATE_TYPE_PERCENT then
          begin
            CheckCondition(qRates.Result['CL_E_D_GROUPS_NBR'] <> null, 'CL_E_D_GROUPS_NBR cannot be null, if RATE TYPE is percentage!');
            Result['CL_E_D_GROUPS_NBR'] := qRates.Result['CL_E_D_GROUPS_NBR'];
            qClED := TEvQuery.Create('select NAME from CL_E_D_GROUPS a where {AsOfNow<a>} and a.CL_E_D_GROUPS_NBR=' + VarToStr(qRates.Result['CL_E_D_GROUPS_NBR']));
            qClED.Execute;
            Result['CL_E_D_GROUPS_NAME'] := qClED.Result['NAME'];
          end;
        end;

        // RATES (EE_BENEFITS and CO_BENEFIT_RATES)
        Result['EE_EE_RATE'] := DM_CLIENT.EE_BENEFITS['EE_RATE'];
        Result['EE_ER_RATE'] := DM_CLIENT.EE_BENEFITS['ER_RATE'];
        Result['EE_COBRA_RATE'] := DM_CLIENT.EE_BENEFITS['COBRA_RATE'];
        if DM_CLIENT.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'] <> null then
        begin
          Result['CO_EE_RATE'] := qRates.Result['EE_RATE'];
          Result['CO_ER_RATE'] := qRates.Result['ER_RATE'];
          Result['CO_COBRA_RATE'] := qRates.Result['COBRA_RATE'];
        end;

        // CO_BENEFIT_CATEGORY
        if DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'] <> null then
        begin
          DM_COMPANY.CO_BENEFIT_CATEGORY.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []);
          Result['CO_BENEFIT_CATEGORY_NBR'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR'];
          Result['CATEGORY_TYPE'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE'];
          Result['CATEGORY_TYPE_DESC'] := DM_COMPANY.CO_BENEFIT_CATEGORY['NAME'];
        end;

        Result.Post;
      end;
    end;

    DM_CLIENT.EE_BENEFITS.Next;
  end;
end;

procedure TevRemoteMiscRoutines.DeleteBenefitRequest(const ARequestNbr: integer);
var
  StorageTable: TevClientDataSet;
begin
  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
    ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));

  CheckCondition(StorageTable.RecordCount > 0, 'Benefit Request not found. EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
  CheckCondition(StorageTable['SIGNATURE_NBR'] = null, 'You cannot delete submitted request');

  if Context.UserAccount.InternalNbr < 0 then
    CheckCondition(StorageTable['EE_NBR'] = Abs(Context.UserAccount.InternalNbr), 'Request was not created by current user');

  try
    StorageTable.Delete;
  except;
    StorageTable.Cancel;
    raise;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevRemoteMiscRoutines.GetEeBenefitPackageAvaliableForEnrollment(const AEENbr: integer; const ASearchForDate: TDateTime; const ACategory: String;
  const AReturnCoBenefitsDataOnly: boolean): IisListOfValues;
var
  qCoGroup: IevQuery;
  qPkgDetail: IevQuery;
  sPkgCondition: String;
  bCathegoryFound: boolean;
  CoBenefitPackageNbr: Variant;
  dNow: TDateTime;
  sPackageQuery: String;
  bCoGroupFound: boolean;
  bCoDBDTFound: boolean;
  bEvent, bOpen: boolean;
  BenefitsDataset, SubtypesDataset: IevDataset;
  qEEChangeRequest, qEEBenefit, qEventCoBenefitEnrollment, qOpenCoBenefitEnrollment : IevQuery;
  pEnrollmentNbr: Variant;

  function CheckState: boolean;
  var
    Q: IevQuery;
  begin
    Result := false;

    // checking RESIDENTIAL_STATE_NBR
    Q := TevQuery.Create('select SY_STATES_NBR from CO_BENEFIT_STATES a where {AsOfNow<a>} ' +
      ' and a.CO_BENEFITS_NBR=:p_benefit and a.SY_STATES_NBR=:p_resid_state');
    Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
    Q.Params.Addvalue('p_resid_state', DM_CLIENT.CL_PERSON['RESIDENTIAL_STATE_NBR']);
    Q.Execute;
    if Q.result.RecordCount > 0 then
    begin
      Result := true;
      exit;
    end;

    // checking CO_STATES
    Q := TevQuery.Create('select SY_STATES_NBR from CO_BENEFIT_STATES p where {AsOfNow<p>} and p.CO_BENEFITS_NBR=:p_benefit ' +
      ' and p.SY_STATES_NBR in (select SY_STATES_NBR from EE_STATES a, CO_STATES b where {AsOfNow<a>}  ' +
      ' and a.EE_NBR=:p_ee and {AsOfNow<b>} and a.CO_STATES_NBR=b.CO_STATES_NBR)');
    Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
    Q.Params.AddValue('p_ee', DM_EMPLOYEE.EE['EE_NBR']);
    Q.Execute;
    if Q.result.RecordCount > 0 then
    begin
      Result := true;
      exit;
    end;
  end;

  function CheckEligibilityByType: boolean;
  begin
    Result := false;
    // position status
    if DM_COMPANY.CO_BENEFITS['EMPLOYEE_TYPE'] = EE_TYPE_ALL then
      Result := True
    else
    begin
      if (DM_EMPLOYEE.EE['POSITION_STATUS'] <> null) and (DM_EMPLOYEE.EE['POSITION_STATUS'] = DM_COMPANY.CO_BENEFITS['EMPLOYEE_TYPE']) then
        Result := True;
    end;
  end;

  function CheckEligibilityByAge: boolean;
  begin
    Result := True;
    if (DM_COMPANY.CO_BENEFITS['MINIMUM_AGE_REQUIREMENT'] <> null) and (DM_COMPANY.CO_BENEFITS['MINIMUM_AGE_REQUIREMENT'] > 0) then
      if (DM_CLIENT.CL_PERSON['BIRTH_DATE'] = null) or
        (YearsBetween(Trunc(dNow), DM_CLIENT.CL_PERSON['BIRTH_DATE']) < DM_COMPANY.CO_BENEFITS['MINIMUM_AGE_REQUIREMENT']) then
        Result := False;
  end;

  function CheckEligibilityByWaitingPeriod: boolean;
  begin
    Result := True;
    if (DM_COMPANY.CO_BENEFITS['ELIGIBILITY_WAITING_PERIOD'] <> null) and (DM_COMPANY.CO_BENEFITS['ELIGIBILITY_WAITING_PERIOD'] > 0) then
      if (DM_EMPLOYEE.EE['CURRENT_HIRE_DATE'] = null) or
        ((Trunc(dNow) - DM_EMPLOYEE.EE['CURRENT_HIRE_DATE']) < DM_COMPANY.CO_BENEFITS['ELIGIBILITY_WAITING_PERIOD']) then
        Result := False;
  end;

  function CheckIfDRefusalExists: boolean;
  var
    Q: IevQuery;
  begin
    Result := false;
    if not VarisNull(pEnrollmentNbr) then
    begin
      Q := TevQuery.Create('select count(EE_BENEFIT_REFUSAL_NBR) CNT from EE_BENEFIT_REFUSAL ' +
            ' where {AsOfNow<EE_BENEFIT_REFUSAL>} and EE_NBR=:pEEnbr and CO_BENEFIT_ENROLLMENT_NBR=:pENROLLMENT_NBR ');
      Q.Params.AddValue('pEEnbr', AEENbr);
      Q.Params.AddValue('pENROLLMENT_NBR', pEnrollmentNbr);
      Q.Execute;
      Result := Q.Result['CNT'] > 0;
     end;
  end;

  function CheckIfApprovedRequestExists: boolean;
  var
    DescriptionData: TBenefitRequestDescription;
  begin
    Result := False;
    if not bEvent then
    begin
      qEEChangeRequest.Result.First;
      while not qEEChangeRequest.Result.Eof do
      begin
        DescriptionData := UnpackBenefitRequestDescription(qEEChangeRequest.Result['DESCRIPTION']);
        if bEvent then
        begin
          if (DescriptionData.CategoryType = DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE']) and
            (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] <> null) and (DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'] <> null) and
            (qEEChangeRequest.Result['STATUS_DATE'] >= DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE']) and
            (qEEChangeRequest.Result['STATUS_DATE'] <= (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] + DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'])) and
            (DescriptionData.Status = 'A') then
          begin
            Result := True;
            Exit;
          end;
        end
        else
        begin
          if (DescriptionData.CategoryType = DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE']) and
             (qEEChangeRequest.Result['STATUS_DATE'] >= qOpenCoBenefitEnrollment.Result['ENROLLMENT_START_DATE']) and
             (qEEChangeRequest.Result['STATUS_DATE'] <= qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE']) and
             (DescriptionData.Status = 'A') then
          begin
            Result := True;
            Exit;
          end;
        end;
        qEEChangeRequest.Result.Next;
      end;


      if not VarisNull(pEnrollmentNbr) then
      begin
        qEEBenefit.Result.First;
        while not qEEBenefit.Result.Eof do
        begin
         if qEEBenefit.Result['CO_BENEFIT_ENROLLMENT_NBR'] = pEnrollmentNbr then
         begin
           Result := True;
           Exit;
         end;
         qEEBenefit.Result.Next;
        end;
      end;
    end;

  end;

  procedure AddDataToResult;
  var
    dDate: TDate;
    Q, qClED: IevQuery;
    BlobField: TBlobField;
    tmpStream: IevDualStream;

    Procedure AddBenefits(const bItsEvent: Boolean);
    begin

      BenefitsDataset.Append;

      if bItsEvent then
      begin
        BenefitsDataset['LAST_QUAL_BENEFIT_EVENT_START_DATE'] := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'];
        BenefitsDataset['LAST_QUAL_BENEFIT_EVENT_END_DATE'] := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] + DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'];
        dDate := BenefitsDataset['LAST_QUAL_BENEFIT_EVENT_START_DATE'];
      end
      else
        dDate := qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE'];

      BenefitsDataset['CO_BENEFIT_CATEGORY_NBR'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR'];
      BenefitsDataset['CATEGORY_TYPE'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE'];
      BenefitsDataset['CATEGORY_TYPE_DESC'] := DM_COMPANY.CO_BENEFIT_CATEGORY['NAME'];
      BenefitsDataset['ENROLLMENT_NOTIFY_EE_DAYS'] := DM_COMPANY.CO_BENEFIT_CATEGORY['ENROLLMENT_NOTIFY_EE_DAYS'];
      BenefitsDataset['ENROLLMENT_REMINDER_EE_DAYS'] := DM_COMPANY.CO_BENEFIT_CATEGORY['ENROLLMENT_REMINDER_EE_DAYS'];
      BenefitsDataset['REMIND_EE_DAYS'] := DM_COMPANY.CO_BENEFIT_CATEGORY['REMIND_EE_DAYS'];

      tmpStream :=  DM_COMPANY.CO_BENEFIT_CATEGORY.GetBlobData('NOTES');
      BlobField := TBlobField(BenefitsDataset.vclDataset.FieldByName('NOTES'));
      BlobField.LoadFromStream(tmpStream.realStream);

      if bItsEvent then
      begin
       BenefitsDataset['ENROLLMENT_START_DATE'] := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'];
       BenefitsDataset['ENROLLMENT_END_DATE'] := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] + DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'];
      end
      else
      begin
       BenefitsDataset['ENROLLMENT_START_DATE'] := qOpenCoBenefitEnrollment.Result['ENROLLMENT_START_DATE'];
       BenefitsDataset['ENROLLMENT_END_DATE'] := qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE'];
      end;

      BenefitsDataset['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
      BenefitsDataset['BENEFIT_NAME'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];
      BenefitsDataset['SHOW_RATES'] := DM_COMPANY.CO_BENEFITS['SHOW_RATES'];
      BenefitsDataset['REQUIRES_BENEFICIARIES'] := DM_COMPANY.CO_BENEFITS['REQUIRES_BENEFICIARIES'];
      BenefitsDataset['ALLOW_HSA'] := DM_COMPANY.CO_BENEFITS['ALLOW_HSA'];
      BenefitsDataset['READ_ONLY'] := DM_COMPANY.CO_BENEFIT_CATEGORY['READ_ONLY'];
      BenefitsDataset['REQUIRES_DOB'] := DM_COMPANY.CO_BENEFITS['REQUIRES_DOB'];
      BenefitsDataset['REQUIRES_SSN'] := DM_COMPANY.CO_BENEFITS['REQUIRES_SSN'];
      BenefitsDataset['REQUIRES_PCP'] := DM_COMPANY.CO_BENEFITS['REQUIRES_PCP'];
      BenefitsDataset['DISPLAY_COST_PERIOD'] := DM_COMPANY.CO_BENEFITS['DISPLAY_COST_PERIOD'];
      BenefitsDataset['ALLOW_EE_CONTRIBUTION'] := DM_COMPANY.CO_BENEFITS['ALLOW_EE_CONTRIBUTION'];
      BenefitsDataset['ADDITIONAL_INFO_TITLE1'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE1'];
      BenefitsDataset['ADDITIONAL_INFO_TITLE2'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE2'];
      BenefitsDataset['ADDITIONAL_INFO_TITLE3'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE3'];

      BenefitsDataset['CO_BENEFIT_PACKAGE_NBR'] := CoBenefitPackageNbr;

      // looking for EE_ANNUAL_MAXIMUM_AMOUNT if it's 'Employee Defined'
      if (DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR'] <> null) and (DM_COMPANY.CO_BENEFITS['ALLOW_EE_CONTRIBUTION'] = GROUP_BOX_YES) then
      begin
        if not DM_COMPANY.CO_E_D_CODES.Locate('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR'], []) then
          raise Exception.Create('Cannot find record in CO_E_D_CODES_NBR table. CO_E_D_CODES_NBR=' + IntToStr(DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR']));
        if not DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES['CL_E_DS_NBR'], []) then
          raise Exception.Create('Cannot find record in CL_E_DS table. CL_E_DS_NBR=' + VarToStr(DM_COMPANY.CO_E_D_CODES['CL_E_DS_NBR']));
        BenefitsDataset['EE_ANNUAL_MAXIMUM_AMOUNT'] := DM_CLIENT.CL_E_DS['ANNUAL_MAXIMUM'];
      end;

      BenefitsDataset.Post;
    end;

  begin
    //looking for rate and subtype
    if bOpen then
    begin
      dDate := qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE'];

      Q := TevQuery.Create('select a.DESCRIPTION, b.* from CO_BENEFIT_SUBTYPE a, CO_BENEFIT_RATES b ' +
        ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFIT_SUBTYPE_NBR=b.CO_BENEFIT_SUBTYPE_NBR ' +
        ' and b.PERIOD_BEGIN > :p_date and a.CO_BENEFITS_NBR=:p_benefit ' +
        ' order by PERIOD_BEGIN');
      Q.Params.AddValue('p_date', dDate);
      Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
      Q.Execute;
    end
    else
    begin
      dDate := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'];
      Q := TevQuery.Create('select a.DESCRIPTION, b.* from CO_BENEFIT_SUBTYPE a, CO_BENEFIT_RATES b ' +
        ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFIT_SUBTYPE_NBR=b.CO_BENEFIT_SUBTYPE_NBR ' +
        ' and :p_date >= b.PERIOD_BEGIN and :p_date<= b.PERIOD_END and a.CO_BENEFITS_NBR=:p_benefit ' +
        ' order by PERIOD_BEGIN');
      Q.Params.AddValue('p_date', dDate);
      Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
      Q.Execute;
    end;
    If Q.Result.RecordCount = 0 then
      exit;

    if not AReturnCoBenefitsDataOnly then
    begin
      // adding subtype and rates
      Q.Result.First;
      while not Q.Result.Eof do
      begin
        SubtypesDataset.Append;
        SubtypesDataset['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
        SubtypesDataset['CO_BENEFIT_SUBTYPE_NBR'] := Q.Result['CO_BENEFIT_SUBTYPE_NBR'];
        SubtypesDataset['DESCRIPTION'] := Q.Result['DESCRIPTION'];
        SubtypesDataset['CO_BENEFIT_RATES_NBR'] := Q.Result['CO_BENEFIT_RATES_NBR'];
        SubtypesDataset['MAX_DEPENDENTS'] := Q.Result['MAX_DEPENDENTS'];
        SubtypesDataset['PERIOD_BEGIN'] := Q.Result['PERIOD_BEGIN'];
        SubtypesDataset['PERIOD_END'] := Q.Result['PERIOD_END'];
        SubtypesDataset['RATE_TYPE'] := Q.Result['RATE_TYPE'];
        SubtypesDataset['EE_RATE'] := Q.Result['EE_RATE'];
        SubtypesDataset['ER_RATE'] := Q.Result['ER_RATE'];
        SubtypesDataset['COBRA_RATE'] := Q.Result['COBRA_RATE'];
        if Q.Result['RATE_TYPE'] = BENEFIT_RATES_RATE_TYPE_PERCENT then
        begin
          CheckCondition(Q.Result['CL_E_D_GROUPS_NBR'] <> null, 'CL_E_D_GROUPS_NBR cannot be null, if RATE TYPE is percentage!');
          SubtypesDataset['CL_E_D_GROUPS_NBR'] := Q.Result['CL_E_D_GROUPS_NBR'];
          qClED := TEvQuery.Create('select NAME from CL_E_D_GROUPS a where {AsOfNow<a>} and a.CL_E_D_GROUPS_NBR=' + VarToStr(Q.Result['CL_E_D_GROUPS_NBR']));
          qClED.Execute;
          SubtypesDataset['CL_E_D_GROUPS_NAME'] := qClED.Result['NAME'];
        end;
        SubtypesDataset.Post;

        Q.Result.Next;
      end;
    end;  // if not ACoBenefitsOnlyAReturnCoBenefitsDataOnly

    if bOpen then
       AddBenefits(false)
    else
       AddBenefits(True);

  end;

  function SearchForTeam: boolean;
  begin
    sPkgCondition := 'CO_TEAM_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_TEAM_NBR']) + ' and CO_DEPARTMENT_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR']) +
      ' and CO_BRANCH_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_BRANCH_NBR']) + ' and CO_DIVISION_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
    qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
    qPkgDetail.Execute;
    Result := qPkgDetail.Result.RecordCount > 0;
  end;

  function SearchForDept: boolean;
  begin
    sPkgCondition := 'CO_TEAM_NBR is NULL and CO_DEPARTMENT_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR']) +
      ' and CO_BRANCH_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_BRANCH_NBR']) + ' and CO_DIVISION_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
    qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
    qPkgDetail.Execute;
    Result := qPkgDetail.Result.RecordCount > 0;
  end;

  function SearchForBranch: boolean;
  begin
    sPkgCondition := 'CO_TEAM_NBR IS NULL and CO_DEPARTMENT_NBR IS NULL '  +
      ' and CO_BRANCH_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_BRANCH_NBR']) + ' and CO_DIVISION_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
    qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
    qPkgDetail.Execute;
    Result := qPkgDetail.Result.RecordCount > 0;
  end;

  function SearchForDivision: boolean;
  begin
    sPkgCondition := 'CO_TEAM_NBR IS NULL and CO_DEPARTMENT_NBR IS NULL and CO_BRANCH_NBR IS NULL and CO_DIVISION_NBR='
      + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
    qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
    qPkgDetail.Execute;
    Result := qPkgDetail.Result.RecordCount > 0;
  end;


var
 s:string;
begin
  Result := TisListOfValues.Create;

  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('Misc_Calc.getEeBenefitPackageAvaliableForEnrollment');
    ctx_Statistics.ActiveEvent.Properties.AddValue('EE_Nbr', AEENbr);
    ctx_Statistics.ActiveEvent.Properties.AddValue('ASearchForDate', ASearchForDate);
    ctx_Statistics.ActiveEvent.Properties.AddValue('ACategory', ACategory);
    ctx_Statistics.ActiveEvent.Properties.AddValue('AReturnCoBenefitsDataOnly', AReturnCoBenefitsDataOnly);
  end;

  try
    BenefitsDataset := TevDataset.Create;
    SubtypesDataset := TevDataset.Create;
    Result.AddValue('CO_BENEFITS', BenefitsDataset);
    Result.AddValue('CO_BENEFIT_SUBTYPE', SubtypesDataset);

    if ASearchForDate = 0 then
      dNow := Mainboard.TimeSource.GetDateTime
    else
      dNow := ASearchForDate;

    // CO_BENEFIT_CATEGORY
    BenefitsDataset.vclDataSet.FieldDefs.Add('CO_BENEFIT_CATEGORY_NBR', ftInteger, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('CATEGORY_TYPE', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('CATEGORY_TYPE_DESC', ftString, 40, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('NOTES', ftBlob, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ENROLLMENT_START_DATE', ftDate, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ENROLLMENT_END_DATE', ftDate, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ENROLLMENT_NOTIFY_EE_DAYS', ftInteger, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ENROLLMENT_REMINDER_EE_DAYS', ftInteger, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('REMIND_EE_DAYS', ftInteger, 0, false);

    // EE
    BenefitsDataset.vclDataSet.FieldDefs.Add('LAST_QUAL_BENEFIT_EVENT_START_DATE', ftDate, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('LAST_QUAL_BENEFIT_EVENT_END_DATE', ftDate, 0, false);

    // CO_BENEFITS
    BenefitsDataset.vclDataSet.FieldDefs.Add('CO_BENEFITS_NBR', ftInteger, 0, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('BENEFIT_NAME', ftString, 40, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('SHOW_RATES', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('REQUIRES_BENEFICIARIES', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ALLOW_HSA', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('READ_ONLY', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('REQUIRES_DOB', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('REQUIRES_SSN', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('REQUIRES_PCP', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('DISPLAY_COST_PERIOD', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ALLOW_EE_CONTRIBUTION', ftString, 1, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE1', ftString, 40, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE2', ftString, 40, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE3', ftString, 40, false);
    BenefitsDataset.vclDataSet.FieldDefs.Add('EE_ANNUAL_MAXIMUM_AMOUNT', ftFloat, 0, false);

    // CO_BENEFIT_PACKAGE
    BenefitsDataset.vclDataSet.FieldDefs.Add('CO_BENEFIT_PACKAGE_NBR', ftInteger, 0, false);

    // CO_BENEFIT_SUBTYPE
    SubtypesDataset.vclDataSet.FieldDefs.Add('CO_BENEFITS_NBR', ftInteger, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('CO_BENEFIT_SUBTYPE_NBR', ftInteger, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('DESCRIPTION', ftString, 40, false);

    // CO_BENEFIT_RATES
    SubtypesDataset.vclDataSet.FieldDefs.Add('CO_BENEFIT_RATES_NBR', ftInteger, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('MAX_DEPENDENTS', ftInteger, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('PERIOD_BEGIN', ftDate, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('PERIOD_END', ftDate, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('RATE_TYPE', ftString, 1, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('REFERENCE', ftString, 1, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('EE_RATE', ftFloat, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('ER_RATE', ftFloat, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('COBRA_RATE', ftFloat, 0, false);

    // for RATE_TYPE = BENEFIT_RATES_RATE_TYPE_PERCENT
    SubtypesDataset.vclDataSet.FieldDefs.Add('CL_E_D_GROUPS_NBR', ftInteger, 0, false);
    SubtypesDataset.vclDataSet.FieldDefs.Add('CL_E_D_GROUPS_NAME', ftString, 40, false);

    BenefitsDataset.vclDataSet.Open;
    SubtypesDataset.vclDataSet.Open;

    DM_EMPLOYEE.EE.Close;
    DM_CLIENT.CL_PERSON.Close;
    DM_COMPANY.CO_BENEFITS.Close;
    DM_COMPANY.CO_BENEFIT_CATEGORY.Close;
    DM_COMPANY.CO_E_D_CODES.Close;
    DM_CLIENT.CL_E_DS.Close;

    DM_EMPLOYEE.EE.DataRequired('EE_NBR=' + IntToStr(AEENbr));
    CheckCondition(DM_EMPLOYEE.EE['EE_NBR'] = AEENbr, 'Employee not found. EE_NBR=' + IntToStr(AEENbr));

    if DM_EMPLOYEE.EE['BENEFITS_ENABLED'] <> GROUP_BOX_FULL_ACCESS then
      exit;  // employee has no benefits enabled

    qEEBenefit := TevQuery.Create('select  EE_NBR, EE_BENEFITS_NBR, CO_BENEFIT_ENROLLMENT_NBR from EE_BENEFITS ' +
      ' where {AsOfNow<EE_BENEFITS>} ' +
      ' and EE_NBR=' + IntToStr(AEENbr) +
      ' and EXPIRATION_DATE >=' + QuotedStr(DateToStr(dNow)) +
      ' and CO_BENEFIT_ENROLLMENT_NBR is not null ');
    qEEBenefit.Execute;


    DM_CLIENT.CL_PERSON.DataRequired('CL_PERSON_NBR=' + VarToStr(DM_EMPLOYEE.EE['CL_PERSON_NBR']));

    DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
    s :=GetFieldValueListString(DM_COMPANY.CO_BENEFIT_CATEGORY, 'CO_BENEFIT_CATEGORY_NBR');
    if s = '' then
    s := '-1';

    qOpenCoBenefitEnrollment := TevQuery.Create(
    'select CO_BENEFIT_ENROLLMENT_NBR, CO_BENEFIT_CATEGORY_NBR, CATEGORY_COVERAGE_START_DATE, CATEGORY_COVERAGE_END_DATE, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE ' +
        ' from CO_BENEFIT_ENROLLMENT ' +
        ' where {AsOfNow<CO_BENEFIT_ENROLLMENT>} ' +
        ' and CO_BENEFIT_CATEGORY_NBR in ('+ s +
                           ') and ENROLLMENT_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and ENROLLMENT_END_DATE >=' + QuotedStr(DateToStr(dNow)));

    qOpenCoBenefitEnrollment.Execute;
    qOpenCoBenefitEnrollment.Result.IndexFieldNames := 'CO_BENEFIT_ENROLLMENT_NBR';

    qEventCoBenefitEnrollment := TevQuery.Create(
    'select CO_BENEFIT_ENROLLMENT_NBR, CO_BENEFIT_CATEGORY_NBR, CATEGORY_COVERAGE_START_DATE, CATEGORY_COVERAGE_END_DATE, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE ' +
        ' from CO_BENEFIT_ENROLLMENT ' +
        ' where {AsOfNow<CO_BENEFIT_ENROLLMENT>} ' +
        ' and CO_BENEFIT_CATEGORY_NBR in ('+ s +
                           ') and CATEGORY_COVERAGE_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and CATEGORY_COVERAGE_END_DATE >=' + QuotedStr(DateToStr(dNow)));
    qEventCoBenefitEnrollment.Execute;
    qEventCoBenefitEnrollment.Result.IndexFieldNames := 'CO_BENEFIT_ENROLLMENT_NBR';

    DM_COMPANY.CO_BENEFITS.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));

    DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
    DM_CLIENT.CL_E_DS.DataRequired('ALL');

    qCoGroup := TevQuery.Create('select co_group_nbr from co_group a where group_type=''' + ESS_GROUPTYPE_BENEFIT +
      ''' and {AsOfNow<a>} ' +
      ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and ee_nbr=:p1)');
    qCoGroup.Params.AddValue('p1', AEENbr);
    qCoGroup.Execute;

    qEEChangeRequest := TevQuery.Create('select EE_CHANGE_REQUEST_NBR, DESCRIPTION, STATUS_DATE '+
                ' from EE_CHANGE_REQUEST where EE_NBR=:p1 ' +
                ' and REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) + '''');
    qEEChangeRequest.Params.AddValue('p1', AEENbr);
    qEEChangeRequest.Execute;

    // searching for package by assigment
    sPackageQuery := 'select * from CO_BENEFIT_PKG_DETAIL where {AsOfNow<CO_BENEFIT_PKG_DETAIL>} and ' +
      ' CO_BENEFIT_PACKAGE_NBR in (select a.CO_BENEFIT_PACKAGE_NBR from CO_BENEFIT_PACKAGE a, CO_BENEFIT_PKG_ASMNT b ' +
      ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFIT_PACKAGE_NBR=b.CO_BENEFIT_PACKAGE_NBR and ';

    bCoGroupFound := false;
    if qCoGroup.Result.RecordCount > 0 then
    begin
      sPkgCondition := 'CO_GROUP_NBR=' + VarToStr(qCoGroup.Result['CO_GROUP_NBR']);
      qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
      qPkgDetail.Execute;
      bCoGroupFound := qPkgDetail.Result.RecordCount > 0;
    end;

    bCoDBDTFound := false;
    if not bCoGroupFound then
    begin
      if DM_EMPLOYEE.EE['CO_TEAM_NBR'] <> null then
      begin
        bCoDBDTFound := SearchForTeam;
        if not bCoDBDTFound then
        begin
          bCoDBDTFound := SearchForDept;
          if not bCoDBDTFound then
          begin
            bCoDBDTFound := SearchForBranch;
            if not bCoDBDTFound then
              bCoDBDTFound := SearchForDivision;
          end;
        end;
      end
      else
      begin
        if DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR'] <> null then
        begin
          bCoDBDTFound := SearchForDept;
          if not bCoDBDTFound then
          begin
            bCoDBDTFound := SearchForBranch;
            if not bCoDBDTFound then
              bCoDBDTFound := SearchForDivision;
          end;
        end
        else
        begin
          if DM_EMPLOYEE.EE['CO_BRANCH_NBR'] <> null then
          begin
            bCoDBDTFound := SearchForBranch;
            if not bCoDBDTFound then
              bCoDBDTFound := SearchForDivision;
          end
          else if DM_EMPLOYEE.EE['CO_DIVISION_NBR'] <> null then
            bCoDBDTFound := SearchForDivision;
        end;  // else CO_DEPARTMENT
      end;  // else CO_TEAM
    end;

    DM_COMPANY.CO_BENEFITS.First;
    while not DM_COMPANY.CO_BENEFITS.Eof do
    begin
      if DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'] <> null then
      begin
        bCathegoryFound := DM_COMPANY.CO_BENEFIT_CATEGORY.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []);
        CheckCondition(bCathegoryFound, 'Benefit''s cathegory does not exist. CO_BENEFITS_NBR=' + VarToStr('CO_BENEFITS_NBR'));

        bOpen:= false;
        bEvent := false;

        // is it event?
        if (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT'] <> null) and
          (DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ELIGIBLE'] = GROUP_BOX_YES) and
          (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] <> null) and (DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'] <> null) and
          (dNow >= DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE']) and
          (dNow <= (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] + DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'])) then
        begin
          if qEventCoBenefitEnrollment.Result.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []) then
          begin
            bEvent := True;
            pEnrollmentNbr:= qEventCoBenefitEnrollment.Result['CO_BENEFIT_ENROLLMENT_NBR'];
          end;
        end;

        if bEvent then
        begin
          // determining package
          CoBenefitPackageNbr := null;
          if bCoGroupFound or bCoDBDTFound then
          begin
            if qPkgDetail.Result.Locate('CO_BENEFITS_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'], []) then
              CoBenefitPackageNbr := qPkgDetail.Result['CO_BENEFIT_PACKAGE_NBR']
            else
              bEvent:= false;
          end;

          // determine eligibility
          if (not CheckEligibilityByType) or (not CheckEligibilityByAge)
            or (not CheckEligibilityByWaitingPeriod) or (not CheckState) then
            bEvent:= false;

          // looking for refusals and approved requests
          if (CheckIfDRefusalExists or CheckIfApprovedRequestExists) then
            bEvent:= false;

          // store record
          if bEvent then
             AddDataToResult;

        end;


        // benefit must be withing enrollment period and category parameter, if it is not event

        if not bEvent then
        begin
          if qOpenCoBenefitEnrollment.Result.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []) then
          begin
            bOpen:= True;
            pEnrollmentNbr:= qOpenCoBenefitEnrollment.Result['CO_BENEFIT_ENROLLMENT_NBR'];
          end;

          if ((ACategory <> '') and (DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE'] <> ACategory)) or
            (not ((qOpenCoBenefitEnrollment.Result['ENROLLMENT_START_DATE'] <= dNow)
            and (qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE'] >= Trunc(dNow)))) then
             bOpen:= false;

          if bOpen then
          begin
            // determining package
            CoBenefitPackageNbr := null;
            if bCoGroupFound or bCoDBDTFound then
            begin
              if qPkgDetail.Result.Locate('CO_BENEFITS_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'], []) then
                CoBenefitPackageNbr := qPkgDetail.Result['CO_BENEFIT_PACKAGE_NBR']
              else
                bOpen:= false;
            end;

            // determine eligibility
            if (not CheckEligibilityByType) or (not CheckEligibilityByAge)
              or (not CheckEligibilityByWaitingPeriod) or (not CheckState) then
              bOpen:= false;

            // looking for refusals and approved requests
            if (CheckIfDRefusalExists or CheckIfApprovedRequestExists) then
               bOpen:= false;

            // store record
            if bOpen then
               AddDataToResult;

          end;
        end;
      end;

      DM_COMPANY.CO_BENEFITS.Next;
    end;  // while
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

function TevRemoteMiscRoutines.GetSummaryBenefitEnrollmentStatus(const AEENbrList: IisStringList; const AType: String): IevDataset;
var
  sEENbrs,saEENbrs: String;
  qEE, qCompleted, qInProgress, qApproved, qCategory, qEEBenefit, qEventCoBenefitEnrollment, qOpenCoBenefitEnrollment: IevQuery;
  BenefitsDataset: IevDataset;
  RequestInfo: TBenefitRequestDescription;
  bExists: boolean;
  dNow: TDateTime;
  pEnrollmentNbr: Variant;

  function GetEeBenefitPackageAvaliableForEnrollment(const AEENbr: integer): IevDataset;
  var
    qCoGroup: IevQuery;
    qPkgDetail: IevQuery;
    sPkgCondition: String;
    bCathegoryFound: boolean;
    CoBenefitPackageNbr: Variant;
    dNow: TDateTime;
    sPackageQuery: String;
    bCoGroupFound: boolean;
    bCoDBDTFound: boolean;
    bEvent: boolean;

    function CheckState: boolean;
    var
      Q: IevQuery;
    begin
      Result := false;

      // checking RESIDENTIAL_STATE_NBR
      Q := TevQuery.Create('select SY_STATES_NBR from CO_BENEFIT_STATES a where {AsOfNow<a>} ' +
        ' and a.CO_BENEFITS_NBR=:p_benefit and a.SY_STATES_NBR=:p_resid_state');
      Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
      Q.Params.Addvalue('p_resid_state', DM_CLIENT.CL_PERSON['RESIDENTIAL_STATE_NBR']);
      Q.Execute;
      if Q.result.RecordCount > 0 then
      begin
        Result := true;
        exit;
      end;

      // checking CO_STATES
      Q := TevQuery.Create('select SY_STATES_NBR from CO_BENEFIT_STATES p where {AsOfNow<p>} and p.CO_BENEFITS_NBR=:p_benefit ' +
        ' and p.SY_STATES_NBR in (select SY_STATES_NBR from EE_STATES a, CO_STATES b where {AsOfNow<a>}  ' +
        ' and a.EE_NBR=:p_ee and {AsOfNow<b>} and a.CO_STATES_NBR=b.CO_STATES_NBR)');
      Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
      Q.Params.AddValue('p_ee', DM_EMPLOYEE.EE['EE_NBR']);
      Q.Execute;
      if Q.result.RecordCount > 0 then
      begin
        Result := true;
        exit;
      end;
    end;

    function CheckEligibilityByType: boolean;
    begin
      Result := false;
      // position status
      if DM_COMPANY.CO_BENEFITS['EMPLOYEE_TYPE'] = EE_TYPE_ALL then
        Result := True
      else
      begin
        if (DM_EMPLOYEE.EE['POSITION_STATUS'] <> null) and (DM_EMPLOYEE.EE['POSITION_STATUS'] = DM_COMPANY.CO_BENEFITS['EMPLOYEE_TYPE']) then
          Result := True;
      end;
    end;

    function CheckEligibilityByAge: boolean;
    begin
      Result := True;
      if (DM_COMPANY.CO_BENEFITS['MINIMUM_AGE_REQUIREMENT'] <> null) and (DM_COMPANY.CO_BENEFITS['MINIMUM_AGE_REQUIREMENT'] > 0) then
        if (DM_CLIENT.CL_PERSON['BIRTH_DATE'] = null) or
          (YearsBetween(Trunc(dNow), DM_CLIENT.CL_PERSON['BIRTH_DATE']) < DM_COMPANY.CO_BENEFITS['MINIMUM_AGE_REQUIREMENT']) then
          Result := False;
    end;

    function CheckEligibilityByWaitingPeriod: boolean;
    begin
      Result := True;
      if (DM_COMPANY.CO_BENEFITS['ELIGIBILITY_WAITING_PERIOD'] <> null) and (DM_COMPANY.CO_BENEFITS['ELIGIBILITY_WAITING_PERIOD'] > 0) then
        if (DM_EMPLOYEE.EE['CURRENT_HIRE_DATE'] = null) or
          ((Trunc(dNow) - DM_EMPLOYEE.EE['CURRENT_HIRE_DATE']) < DM_COMPANY.CO_BENEFITS['ELIGIBILITY_WAITING_PERIOD']) then
          Result := False;
    end;

    function CheckIfDRefusalExists: boolean;
    var
      Q: IevQuery;
    begin
      Result := false;
      if not VarisNull(pEnrollmentNbr) then
      begin
        Q := TevQuery.Create('select count(EE_BENEFIT_REFUSAL_NBR) CNT from EE_BENEFIT_REFUSAL ' +
              ' where {AsOfNow<EE_BENEFIT_REFUSAL>} and EE_NBR=:pEEnbr and CO_BENEFIT_ENROLLMENT_NBR=:pENROLLMENT_NBR ');
        Q.Params.AddValue('pEEnbr', AEENbr);
        Q.Params.AddValue('pENROLLMENT_NBR', pEnrollmentNbr);
        Q.Execute;
        Result := Q.Result['CNT'] > 0;
      end;
    end;

    function CheckIfApprovedRequestExists: boolean;
    var
      DescriptionData: TBenefitRequestDescription;
    begin
      Result := False;
      DM_EMPLOYEE.EE_CHANGE_REQUEST.First;
      while not DM_EMPLOYEE.EE_CHANGE_REQUEST.Eof do
      begin
        DescriptionData := UnpackBenefitRequestDescription(DM_EMPLOYEE.EE_CHANGE_REQUEST['DESCRIPTION']);
        if DescriptionData.CategoryType = DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE'] then
        begin
          Result := True;
          Exit;
        end;
        DM_EMPLOYEE.EE_CHANGE_REQUEST.Next;
      end;

      if not VarisNull(pEnrollmentNbr) then
      begin
        qEEBenefit.Result.First;
        while not qEEBenefit.Result.Eof do
        begin
         if qEEBenefit.result['CO_BENEFIT_ENROLLMENT_NBR'] = pEnrollmentNbr then
         begin
           Result := True;
           Exit;
         end;
         qEEBenefit.Result.Next;
        end;
      end;
    end;

    function SearchForTeam: boolean;
    begin
      sPkgCondition := 'CO_TEAM_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_TEAM_NBR']) + ' and CO_DEPARTMENT_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR']) +
        ' and CO_BRANCH_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_BRANCH_NBR']) + ' and CO_DIVISION_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
      qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
      qPkgDetail.Execute;
      Result := qPkgDetail.Result.RecordCount > 0;
    end;

    function SearchForDept: boolean;
    begin
      sPkgCondition := 'CO_TEAM_NBR is NULL and CO_DEPARTMENT_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR']) +
        ' and CO_BRANCH_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_BRANCH_NBR']) + ' and CO_DIVISION_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
      qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
      qPkgDetail.Execute;
      Result := qPkgDetail.Result.RecordCount > 0;
    end;

    function SearchForBranch: boolean;
    begin
      sPkgCondition := 'CO_TEAM_NBR IS NULL and CO_DEPARTMENT_NBR IS NULL '  +
        ' and CO_BRANCH_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_BRANCH_NBR']) + ' and CO_DIVISION_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
      qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
      qPkgDetail.Execute;
      Result := qPkgDetail.Result.RecordCount > 0;
    end;

    function SearchForDivision: boolean;
    begin
      sPkgCondition := 'CO_TEAM_NBR IS NULL and CO_DEPARTMENT_NBR IS NULL and CO_BRANCH_NBR IS NULL and CO_DIVISION_NBR='
        + VarToStr(DM_EMPLOYEE.EE['CO_DIVISION_NBR']);
      qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
      qPkgDetail.Execute;
      Result := qPkgDetail.Result.RecordCount > 0;
    end;

  var
    BlobField: TBlobField;
    tmpStream: IevDualStream;
    dDate: TDate;
    Q: IevQuery;
  begin

    Result := TevDataset.Create;
    try
      // CO_BENEFIT_CATEGORY
      Result.vclDataSet.FieldDefs.Add('CO_BENEFIT_CATEGORY_NBR', ftInteger, 0, false);
      Result.vclDataSet.FieldDefs.Add('CATEGORY_TYPE', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('CATEGORY_TYPE_DESC', ftString, 40, false);
      Result.vclDataSet.FieldDefs.Add('NOTES', ftBlob, 0, false);
      Result.vclDataSet.FieldDefs.Add('ENROLLMENT_NOTIFY_EE_DAYS', ftInteger, 0, false);
      Result.vclDataSet.FieldDefs.Add('ENROLLMENT_REMINDER_EE_DAYS', ftInteger, 0, false);
      Result.vclDataSet.FieldDefs.Add('REMIND_EE_DAYS', ftInteger, 0, false);

       // CO_BENEFIT_ENROLLMENT
      Result.vclDataSet.FieldDefs.Add('CO_BENEFIT_ENROLLMENT_NBR', ftInteger, 0, false);
      Result.vclDataSet.FieldDefs.Add('ENROLLMENT_START_DATE', ftDate, 0, false);
      Result.vclDataSet.FieldDefs.Add('ENROLLMENT_END_DATE', ftDate, 0, false);

      // EE
      Result.vclDataSet.FieldDefs.Add('LAST_QUAL_BENEFIT_EVENT_START_DATE', ftDate, 0, false);
      Result.vclDataSet.FieldDefs.Add('LAST_QUAL_BENEFIT_EVENT_END_DATE', ftDate, 0, false);

      // CO_BENEFITS
      Result.vclDataSet.FieldDefs.Add('CO_BENEFITS_NBR', ftInteger, 0, false);
      Result.vclDataSet.FieldDefs.Add('BENEFIT_NAME', ftString, 40, false);
      Result.vclDataSet.FieldDefs.Add('SHOW_RATES', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('REQUIRES_BENEFICIARIES', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('ALLOW_HSA', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('READ_ONLY', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('REQUIRES_DOB', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('REQUIRES_SSN', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('REQUIRES_PCP', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('DISPLAY_COST_PERIOD', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('ALLOW_EE_CONTRIBUTION', ftString, 1, false);
      Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE1', ftString, 40, false);
      Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE2', ftString, 40, false);
      Result.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE3', ftString, 40, false);
      Result.vclDataSet.FieldDefs.Add('EE_ANNUAL_MAXIMUM_AMOUNT', ftFloat, 0, false);

      // CO_BENEFIT_PACKAGE
      Result.vclDataSet.FieldDefs.Add('CO_BENEFIT_PACKAGE_NBR', ftInteger, 0, false);

      Result.vclDataSet.Open;

      if DM_EMPLOYEE.EE['BENEFITS_ENABLED'] <> GROUP_BOX_FULL_ACCESS then
        exit;  // employee has no benefits enabled

      qCoGroup := TevQuery.Create('select co_group_nbr from co_group a where group_type=''' + ESS_GROUPTYPE_BENEFIT +
        ''' and {AsOfNow<a>} ' +
        ' and co_group_nbr in (select co_group_nbr from co_group_member b where {AsOfNow<b>} and ee_nbr=:p1)');
      qCoGroup.Params.AddValue('p1', AEENbr);
      qCoGroup.Execute;


      // searching for package by assigment
      sPackageQuery := 'select * from CO_BENEFIT_PKG_DETAIL where {AsOfNow<CO_BENEFIT_PKG_DETAIL>} and ' +
        ' CO_BENEFIT_PACKAGE_NBR in (select a.CO_BENEFIT_PACKAGE_NBR from CO_BENEFIT_PACKAGE a, CO_BENEFIT_PKG_ASMNT b ' +
        ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFIT_PACKAGE_NBR=b.CO_BENEFIT_PACKAGE_NBR and ';

      bCoGroupFound := false;
      if qCoGroup.Result.RecordCount > 0 then
      begin
        sPkgCondition := 'CO_GROUP_NBR=' + VarToStr(qCoGroup.Result['CO_GROUP_NBR']);
        qPkgDetail := TevQuery.Create(sPackageQuery + sPkgCondition + ')');
        qPkgDetail.Execute;
        bCoGroupFound := qPkgDetail.Result.RecordCount > 0;
      end;

      bCoDBDTFound := false;
      if not bCoGroupFound then
      begin
        if DM_EMPLOYEE.EE['CO_TEAM_NBR'] <> null then
        begin
          bCoDBDTFound := SearchForTeam;
          if not bCoDBDTFound then
          begin
            bCoDBDTFound := SearchForDept;
            if not bCoDBDTFound then
            begin
              bCoDBDTFound := SearchForBranch;
              if not bCoDBDTFound then
                bCoDBDTFound := SearchForDivision;
            end;
          end;
        end
        else
        begin
          if DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR'] <> null then
          begin
            bCoDBDTFound := SearchForDept;
            if not bCoDBDTFound then
            begin
              bCoDBDTFound := SearchForBranch;
              if not bCoDBDTFound then
                bCoDBDTFound := SearchForDivision;
            end;
          end
          else
          begin
            if DM_EMPLOYEE.EE['CO_BRANCH_NBR'] <> null then
            begin
              bCoDBDTFound := SearchForBranch;
              if not bCoDBDTFound then
                bCoDBDTFound := SearchForDivision;
            end
            else if DM_EMPLOYEE.EE['CO_DIVISION_NBR'] <> null then
              bCoDBDTFound := SearchForDivision;
          end;  // else CO_DEPARTMENT
        end;  // else CO_TEAM
      end;

      dNow:= Mainboard.TimeSource.GetDateTime;
      DM_COMPANY.CO_BENEFITS.First;
      while not DM_COMPANY.CO_BENEFITS.Eof do
      begin
        if DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'] <> null then
        begin
          bCathegoryFound := DM_COMPANY.CO_BENEFIT_CATEGORY.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []);
          CheckCondition(bCathegoryFound, 'Benefit''s cathegory does not exist. CO_BENEFITS_NBR=' + VarToStr('CO_BENEFITS_NBR'));

          // is it event?
          if (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT'] <> null) and
            (DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ELIGIBLE'] = GROUP_BOX_YES) and
            (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] <> null) and (DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'] <> null) and
            (dNow >= DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE']) and
            (dNow <= (DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] + DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION']) + 14) then
          begin
            bEvent := True;
            if not qEventCoBenefitEnrollment.Result.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []) then
            begin
              DM_COMPANY.CO_BENEFITS.Next;
              continue;
            end;
            pEnrollmentNbr:= qEventCoBenefitEnrollment.Result['CO_BENEFIT_ENROLLMENT_NBR'];

          end
          else
          begin
            bEvent := False;
            // benefit must be withing enrollment period and category parameter, if it is not event
            if not qOpenCoBenefitEnrollment.Result.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], []) then
            begin
              DM_COMPANY.CO_BENEFITS.Next;
              continue;
            end;
            pEnrollmentNbr:= qOpenCoBenefitEnrollment.Result['CO_BENEFIT_ENROLLMENT_NBR'];

            if (not ((qOpenCoBenefitEnrollment.Result['ENROLLMENT_START_DATE'] <= dNow)
              and (Trunc(qOpenCoBenefitEnrollment.Result.fieldByName('ENROLLMENT_END_DATE').AsDateTime) + 14 >= Trunc(dNow)))) then
            begin
              DM_COMPANY.CO_BENEFITS.Next;
              continue;
            end;
          end;

          // determining package
          CoBenefitPackageNbr := null;
          if bCoGroupFound or bCoDBDTFound then
          begin
            if qPkgDetail.Result.Locate('CO_BENEFITS_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'], []) then
              CoBenefitPackageNbr := qPkgDetail.Result['CO_BENEFIT_PACKAGE_NBR']
            else
            begin
              DM_COMPANY.CO_BENEFITS.Next;
              continue;
            end;
          end;

          // determine eligibility
          if (not CheckEligibilityByType) or (not CheckEligibilityByAge) or
             (not CheckEligibilityByWaitingPeriod) or (not CheckState) then
          begin
            DM_COMPANY.CO_BENEFITS.Next;
            continue;
          end;

          // looking for refusals and approved requests
          if (CheckIfDRefusalExists  or CheckIfApprovedRequestExists) then
          begin
            DM_COMPANY.CO_BENEFITS.Next;
            continue;
          end;

          Result.Append;

          if bEvent then
          begin
            Result['LAST_QUAL_BENEFIT_EVENT_START_DATE'] := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'];
            Result['LAST_QUAL_BENEFIT_EVENT_END_DATE'] := DM_EMPLOYEE.EE['LAST_QUAL_BENEFIT_EVENT_DATE'] + DM_COMPANY.CO_BENEFITS['QUAL_EVENT_ENROLL_DURATION'];
            dDate := Result['LAST_QUAL_BENEFIT_EVENT_START_DATE']
          end
          else
            dDate := qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE'];

          //looking for rate and subtype
          if bEvent then
          begin
            Q := TevQuery.Create('select a.DESCRIPTION, b.* from CO_BENEFIT_SUBTYPE a, CO_BENEFIT_RATES b ' +
              ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFIT_SUBTYPE_NBR=b.CO_BENEFIT_SUBTYPE_NBR ' +
              ' and :p_date >= b.PERIOD_BEGIN and :p_date<= b.PERIOD_END and a.CO_BENEFITS_NBR=:p_benefit ' +
              ' order by PERIOD_BEGIN');
            Q.Params.AddValue('p_date', dDate);
            Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
            Q.Execute;
          end
          else
          begin
            Q := TevQuery.Create('select a.DESCRIPTION, b.* from CO_BENEFIT_SUBTYPE a, CO_BENEFIT_RATES b ' +
              ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFIT_SUBTYPE_NBR=b.CO_BENEFIT_SUBTYPE_NBR ' +
              ' and b.PERIOD_BEGIN > :p_date and a.CO_BENEFITS_NBR=:p_benefit ' +
              ' order by PERIOD_BEGIN');
            Q.Params.AddValue('p_date', dDate);
            Q.Params.AddValue('p_benefit', DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']);
            Q.Execute;
          end;

          If Q.Result.RecordCount = 0 then
          begin
            Result.vclDataset.Cancel;
          end
          else
          begin
            Result['CO_BENEFIT_CATEGORY_NBR'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR'];
            Result['CATEGORY_TYPE'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE'];
            Result['CATEGORY_TYPE_DESC'] := DM_COMPANY.CO_BENEFIT_CATEGORY['NAME'];
            Result['ENROLLMENT_NOTIFY_EE_DAYS'] := DM_COMPANY.CO_BENEFIT_CATEGORY['ENROLLMENT_NOTIFY_EE_DAYS'];
            Result['ENROLLMENT_REMINDER_EE_DAYS'] := DM_COMPANY.CO_BENEFIT_CATEGORY['ENROLLMENT_REMINDER_EE_DAYS'];
            Result['REMIND_EE_DAYS'] := DM_COMPANY.CO_BENEFIT_CATEGORY['REMIND_EE_DAYS'];

            tmpStream :=  DM_COMPANY.CO_BENEFIT_CATEGORY.GetBlobData('NOTES');
            BlobField := TBlobField(Result.vclDataset.FieldByName('NOTES'));
            BlobField.LoadFromStream(tmpStream.realStream);

            Result['CO_BENEFIT_ENROLLMENT_NBR'] := qOpenCoBenefitEnrollment.Result['CO_BENEFIT_ENROLLMENT_NBR'];


            Result['ENROLLMENT_START_DATE'] := qOpenCoBenefitEnrollment.Result['ENROLLMENT_START_DATE'];
            Result['ENROLLMENT_END_DATE'] := qOpenCoBenefitEnrollment.Result['ENROLLMENT_END_DATE'];


            Result['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
            Result['BENEFIT_NAME'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];
            Result['SHOW_RATES'] := DM_COMPANY.CO_BENEFITS['SHOW_RATES'];
            Result['REQUIRES_BENEFICIARIES'] := DM_COMPANY.CO_BENEFITS['REQUIRES_BENEFICIARIES'];
            Result['ALLOW_HSA'] := DM_COMPANY.CO_BENEFITS['ALLOW_HSA'];
            Result['READ_ONLY'] := DM_COMPANY.CO_BENEFIT_CATEGORY['READ_ONLY'];
            Result['REQUIRES_DOB'] := DM_COMPANY.CO_BENEFITS['REQUIRES_DOB'];
            Result['REQUIRES_SSN'] := DM_COMPANY.CO_BENEFITS['REQUIRES_SSN'];
            Result['REQUIRES_PCP'] := DM_COMPANY.CO_BENEFITS['REQUIRES_PCP'];
            Result['DISPLAY_COST_PERIOD'] := DM_COMPANY.CO_BENEFITS['DISPLAY_COST_PERIOD'];
            Result['ALLOW_EE_CONTRIBUTION'] := DM_COMPANY.CO_BENEFITS['ALLOW_EE_CONTRIBUTION'];
            Result['ADDITIONAL_INFO_TITLE1'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE1'];
            Result['ADDITIONAL_INFO_TITLE2'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE2'];
            Result['ADDITIONAL_INFO_TITLE3'] := DM_COMPANY.CO_BENEFITS['ADDITIONAL_INFO_TITLE3'];

            Result['CO_BENEFIT_PACKAGE_NBR'] := CoBenefitPackageNbr;

            // looking for EE_ANNUAL_MAXIMUM_AMOUNT if it's 'Employee Defined'
            if (DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR'] <> null) and (DM_COMPANY.CO_BENEFITS['ALLOW_EE_CONTRIBUTION'] = GROUP_BOX_YES) then
            begin
              if not DM_COMPANY.CO_E_D_CODES.Locate('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR'], []) then
                raise Exception.Create('Cannot find record in CO_E_D_CODES_NBR table. CO_E_D_CODES_NBR=' + IntToStr(DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR']));
              if not DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES['CL_E_DS_NBR'], []) then
                raise Exception.Create('Cannot find record in CL_E_DS table. CL_E_DS_NBR=' + VarToStr(DM_COMPANY.CO_E_D_CODES['CL_E_DS_NBR']));
              Result['EE_ANNUAL_MAXIMUM_AMOUNT'] := DM_CLIENT.CL_E_DS['ANNUAL_MAXIMUM'];
            end;

            Result.Post;

          end;
        end;

        DM_COMPANY.CO_BENEFITS.Next;
      end;  // while
    finally
      if ctx_Statistics.Enabled then
        ctx_Statistics.EndEvent;
    end;
  end;

  procedure AddRequestInfo(const AQ: IevQuery);
  var
    bFound: boolean;
    Info: TBenefitRequestDescription;
  begin

    if qEE.Result.Locate('EE_NBR',AQ.Result.FieldValues['EE_NBR'],[]) then
    begin
      Result.Append;

      Result['SELECT'] := false;
      Result['EE_CHANGE_REQUEST_NBR'] := AQ.Result['EE_CHANGE_REQUEST_NBR'];

      // EE information
      Result['CL_PERSON_NBR'] := qEE.Result['CL_PERSON_NBR'];
      Result['EE_NBR'] := qEE.Result['EE_NBR'];
      Result['SOCIAL_SECURITY_NUMBER'] := qEE.Result['SOCIAL_SECURITY_NUMBER'];
      Result['CUSTOM_EMPLOYEE_NUMBER'] := qEE.Result['CUSTOM_EMPLOYEE_NUMBER'];
      Result['FIRST_NAME'] := qEE.Result['FIRST_NAME'];
      Result['MIDDLE_INITIAL'] := qEE.Result['MIDDLE_INITIAL'];
      Result['LAST_NAME'] := qEE.Result['LAST_NAME'];

      // Benefit information
      Info := UnpackBenefitRequestDescription(AQ.Result['DESCRIPTION']);

      if not Info.Declined then
      begin
        bFound := DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', Info.CoBenefitNbr, []);
        CheckCondition(bFound, 'Cannot find benefit assossiated with request. EE_CHANGE_REQUEST_NBR=' + VarToStr(AQ.Result['EE_CHANGE_REQUEST_NBR']));
        Result['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
        Result['BENEFIT_NAME'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];
      end;

      bFound := qCategory.Result.Locate('CATEGORY_TYPE', Info.CategoryType, []);
      if not bFound then
        Result['CATEGORY_TYPE_DESC'] := 'Category is missed'
      else
      begin
        Result['CATEGORY_TYPE'] := qCategory.Result['CATEGORY_TYPE'];
        if qCategory.Result['CATEGORY_TYPE'] <> null then
          Result['CATEGORY_TYPE_DESC'] := ReturnDescription(Benefit_Category_Type_ComboChoices, VarToStr(qCategory.Result['CATEGORY_TYPE']));
      end;

      Result.Post;
    end;
  end;

var
 s :String;
begin
  CheckCondition((AType = 'C') or (AType = 'P') or (AType = 'N') or (AType = 'A'), 'Wrong value for AType parameter. Allowed: C, P, N, A');
  CheckCOndition(AEENbrList.Count > 0, 'List of employees cannot be empty');

  dNow := Mainboard.TimeSource.GetDateTime;

  Result := TevDataset.Create;
  Result.vclDataSet.FieldDefs.Add('CL_PERSON_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('SOCIAL_SECURITY_NUMBER', ftString, 11, false);
  Result.vclDataSet.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString, 9, false);
  Result.vclDataSet.FieldDefs.Add('CO_BENEFITS_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('BENEFIT_NAME', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('CATEGORY_TYPE', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('CATEGORY_TYPE_DESC', ftString, 40, false);
  Result.vclDataSet.FieldDefs.Add('EE_CHANGE_REQUEST_NBR', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('FIRST_NAME', ftString, 20, false);
  Result.vclDataSet.FieldDefs.Add('MIDDLE_INITIAL', ftString, 1, false);
  Result.vclDataSet.FieldDefs.Add('LAST_NAME', ftString, 30, false);
  Result.vclDataSet.FieldDefs.Add('SELECT', ftBoolean, 0, true);

  Result.vclDataSet.Open;

  DM_COMPANY.CO_BENEFITS.Close;
  DM_EMPLOYEE.EE.Close;
  DM_EMPLOYEE.EE_CHANGE_REQUEST.close;
  DM_CLIENT.CL_PERSON.Close;
  DM_COMPANY.CO_BENEFIT_CATEGORY.Close;
  DM_COMPANY.CO_E_D_CODES.Close;
  DM_CLIENT.CL_E_DS.Close;

  sEENbrs:= BuildINsqlStatement('EE_NBR',AEENbrList);
  saEENbrs:= BuildINsqlStatement('a.EE_NBR',AEENbrList);

  qEE := TevQuery.Create('select EE_NBR, a.CL_PERSON_NBR, SOCIAL_SECURITY_NUMBER, CUSTOM_EMPLOYEE_NUMBER, ' +
    ' a.FIRST_NAME, a.MIDDLE_INITIAL, a.LAST_NAME, b.CO_NBR ' +
    ' from CL_PERSON a, EE b where {AsOfNow<a>} and {AsOfNow<b>} ' +
    ' and a.CL_PERSON_NBR=b.CL_PERSON_NBR and ' + sEENbrs +
    ' order by CUSTOM_EMPLOYEE_NUMBER');
  qEE.Execute;
  qEE.Result.First;

  DM_CLIENT.CL_E_DS.DataRequired('ALL');
  DM_CLIENT.CL_PERSON.DataRequired('ALL');

  qCategory :=
    TevQuery.Create('select a.CO_NBR, a.CO_BENEFIT_CATEGORY_NBR, a.CATEGORY_TYPE, a.ENROLLMENT_FREQUENCY, a.ENROLLMENT_NOTIFY_EE_DAYS, ' +
                       ' a.ENROLLMENT_NOTIFY_HR_DAYS, a.ENROLLMENT_REMINDER_EE_DAYS, a.ENROLLMENT_REMINDER_HR_DAYS, a.NOTES, a.READ_ONLY, ' +
                       ' a.REMIND_EE_DAYS, a.REMIND_HR_DAYS, b.CO_BENEFIT_ENROLLMENT_NBR, b.ENROLLMENT_START_DATE, b.ENROLLMENT_END_DATE, ' +
                       ' b.CATEGORY_COVERAGE_START_DATE, b.CATEGORY_COVERAGE_END_DATE ' +
                     ' from CO_BENEFIT_CATEGORY a ' +
                     ' LEFT OUTER JOIN CO_BENEFIT_ENROLLMENT b on a.CO_BENEFIT_CATEGORY_NBR = b.CO_BENEFIT_CATEGORY_NBR ' +
                     ' where {AsOfNow<a>} and {AsOfNow<b>}');
  qCategory.Params.AddValue('P_CO_NBR', qEE.Result['CO_NBR']);
  qCategory.Execute;

  DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CO_NBR=' + VarToStr(qEE.Result['CO_NBR']));
  s :=GetFieldValueListString(DM_COMPANY.CO_BENEFIT_CATEGORY, 'CO_BENEFIT_CATEGORY_NBR');
  if s = '' then
     s := '-1';

  qOpenCoBenefitEnrollment := TevQuery.Create(
    'select CO_BENEFIT_ENROLLMENT_NBR, CO_BENEFIT_CATEGORY_NBR, CATEGORY_COVERAGE_START_DATE, CATEGORY_COVERAGE_END_DATE, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE ' +
        ' from CO_BENEFIT_ENROLLMENT ' +
        ' where {AsOfNow<CO_BENEFIT_ENROLLMENT>} ' +
        ' and CO_BENEFIT_CATEGORY_NBR in ('+ s +
                           ') and ENROLLMENT_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and ENROLLMENT_END_DATE >=' + QuotedStr(DateToStr(dNow-14)));
  qOpenCoBenefitEnrollment.Execute;


  qEventCoBenefitEnrollment := TevQuery.Create(
    'select CO_BENEFIT_ENROLLMENT_NBR, CO_BENEFIT_CATEGORY_NBR, CATEGORY_COVERAGE_START_DATE, CATEGORY_COVERAGE_END_DATE, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE ' +
        ' from CO_BENEFIT_ENROLLMENT ' +
        ' where {AsOfNow<CO_BENEFIT_ENROLLMENT>} ' +
        ' and CO_BENEFIT_CATEGORY_NBR in ('+ s +
                           ') and CATEGORY_COVERAGE_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and CATEGORY_COVERAGE_END_DATE >=' + QuotedStr(DateToStr(dNow)));
  qEventCoBenefitEnrollment.Execute;

  DM_COMPANY.CO_BENEFITS.DataRequired('CO_NBR=' + VarToStr(qEE.Result['CO_NBR']));
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR=' + VarToStr(qEE.Result['CO_NBR']));
  DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + VarToStr(qEE.Result['CO_NBR']));

  if AType = 'C' then
  begin
    qCompleted := TEvQuery.Create('select EE_NBR, EE_CHANGE_REQUEST_NBR, SIGNATURE_NBR, DESCRIPTION ' +
      ' from EE_CHANGE_REQUEST a where {AsOfNow<a>} and a.REQUEST_TYPE=:p_request_type ' +
      ' and a.SIGNATURE_NBR is not NULL and ' + saEENbrs);
    qCompleted.Params.AddValue('p_request_type', EEChangeRequestTypeToString(tpBenefitRequest));
    qCompleted.Execute;
    qCompleted.Result.First;
    while not qCompleted.Result.Eof do
    begin
      RequestInfo := UnpackBenefitRequestDescription(qCompleted.Result['DESCRIPTION']);
      if RequestInfo.Status = 'S' then
        AddRequestInfo(qCompleted);
      qCompleted.Result.next;
    end;
  end
  else if AType = 'P' then
  begin
    qInProgress := TEvQuery.Create('select EE_NBR, EE_CHANGE_REQUEST_NBR, SIGNATURE_NBR, DESCRIPTION ' +
      ' from EE_CHANGE_REQUEST a where {AsOfNow<a>} and a.REQUEST_TYPE=:p_request_type ' +
      ' and a.SIGNATURE_NBR is NULL and ' + saEENbrs );
    qInProgress.Params.AddValue('p_request_type', EEChangeRequestTypeToString(tpBenefitRequest));
    qInProgress.Execute;
    qInProgress.Result.First;
    while not qInProgress.Result.Eof do
    begin
      RequestInfo := UnpackBenefitRequestDescription(qInProgress.Result['DESCRIPTION']);
      if RequestInfo.Status = 'P' then
        AddRequestInfo(qInProgress);
      qInProgress.Result.Next;
    end;
  end
  else if AType = 'A' then
  begin
    qApproved := TEvQuery.Create('select EE_NBR, EE_CHANGE_REQUEST_NBR, SIGNATURE_NBR, DESCRIPTION ' +
      ' from EE_CHANGE_REQUEST where REQUEST_TYPE=:p_request_type ' +
      ' and SIGNATURE_NBR is not NULL and ' + saEENbrs );
    qApproved.Params.AddValue('p_request_type', EEChangeRequestTypeToString(tpBenefitRequest));
    qApproved.Execute;
    qApproved.Result.First;
    while not qApproved.Result.Eof do
    begin
      RequestInfo := UnpackBenefitRequestDescription(qApproved.Result['DESCRIPTION']);
      qCategory.Result.Locate('CATEGORY_TYPE', RequestInfo.CategoryType, []);
      if (dNow >= qCategory.Result['ENROLLMENT_START_DATE']) and (RequestInfo.Status = 'A') then
        AddRequestInfo(qApproved);
      qApproved.Result.Next;
    end;
  end
  else
  begin
      DM_EMPLOYEE.EE_CHANGE_REQUEST.DataRequired(sEENbrs +
                                      ' and REQUEST_TYPE = ''' + EEChangeRequestTypeToString(tpBenefitRequest) + '''');
      DM_EMPLOYEE.EE_CHANGE_REQUEST.IndexFieldNames := 'EE_NBR';

      try
          qEE.Result.First;

          qEEBenefit := TevQuery.Create('select  EE_NBR, EE_BENEFITS_NBR, CO_BENEFIT_ENROLLMENT_NBR from EE_BENEFITS ' +
            ' where {AsOfNow<EE_BENEFITS>} ' +
            ' and ' + sEENbrs +
            ' and EXPIRATION_DATE >=' + QuotedStr(DateToStr(dNow)) +
            ' and CO_BENEFIT_ENROLLMENT_NBR is not null order by EE_NBR');
          qEEBenefit.Execute;
          qEEBenefit.Result.IndexFieldNames := 'EE_NBR';

          while not qEE.Result.Eof do
          begin
              DM_EMPLOYEE.EE.Locate('EE_NBR', qEE.Result['EE_NBR'], []);
              CheckCondition(DM_EMPLOYEE.EE['EE_NBR'] = qEE.Result['EE_NBR'], 'Employee not found. EE_NBR=' + IntToStr(qEE.Result['EE_NBR']));
              DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE['CL_PERSON_NBR'],[]);
              DM_EMPLOYEE.EE_CHANGE_REQUEST.SetRange([qEE.Result['EE_NBR']], [qEE.Result['EE_NBR']]);

              qEEBenefit.Result.SetRange([qEE.Result['EE_NBR']], [qEE.Result['EE_NBR']]);

              BenefitsDataset := nil;
              BenefitsDataset := GetEeBenefitPackageAvaliableForEnrollment(qEE.Result['EE_NBR']);

              BenefitsDataset.First;
              while not BenefitsDataset.Eof do
              begin
                if dNow >= BenefitsDataset['ENROLLMENT_START_DATE'] then
                  BenefitsDataset.Next
                else
                  BenefitsDataset.Delete;
              end;

              BenefitsDataset.First;
              while not BenefitsDataset.Eof do
              begin

                bExists := False;
                DM_EMPLOYEE.EE_CHANGE_REQUEST.First;
                while not DM_EMPLOYEE.EE_CHANGE_REQUEST.Eof do
                begin
                  RequestInfo := UnpackBenefitRequestDescription(DM_EMPLOYEE.EE_CHANGE_REQUEST['DESCRIPTION']);
                  if (RequestInfo.CoBenefitNbr = BenefitsDataset['CO_BENEFITS_NBR']) or
                    (RequestInfo.CategoryType = BenefitsDataset['CATEGORY_TYPE']) then
                  begin
                    bExists := True;
                    break;
                  end;
                  DM_EMPLOYEE.EE_CHANGE_REQUEST.Next;
                end;

                if not bExists then
                begin
                  If not Result.Locate('CATEGORY_TYPE;EE_NBR', VarArrayOf([BenefitsDataset['CATEGORY_TYPE'], qEE.Result['EE_NBR']]), []) then
                  begin
                    Result.Append;
                    Result['SELECT'] := false;
                    Result['CL_PERSON_NBR'] := qEE.Result['CL_PERSON_NBR'];
                    Result['EE_NBR'] := qEE.Result['EE_NBR'];
                    Result['SOCIAL_SECURITY_NUMBER'] := qEE.Result['SOCIAL_SECURITY_NUMBER'];
                    Result['CUSTOM_EMPLOYEE_NUMBER'] := qEE.Result['CUSTOM_EMPLOYEE_NUMBER'];
                    Result['FIRST_NAME'] := qEE.Result['FIRST_NAME'];
                    Result['MIDDLE_INITIAL'] := qEE.Result['MIDDLE_INITIAL'];
                    Result['LAST_NAME'] := qEE.Result['LAST_NAME'];
                    if BenefitsDataset['CO_BENEFIT_CATEGORY_NBR'] = null then
                      Result['CATEGORY_TYPE_DESC'] := 'Category is missed'
                    else
                    begin
                      Result['CATEGORY_TYPE'] := BenefitsDataset['CATEGORY_TYPE'];
                      Result['CATEGORY_TYPE_DESC'] := BenefitsDataset['CATEGORY_TYPE_DESC'];
                    end;
                    Result.Post;
                  end;
                end;

                BenefitsDataset.Next;
              end;  // while

            qEE.Result.Next;
          end;
      finally
        DM_EMPLOYEE.EE_CHANGE_REQUEST.IndexFieldNames := '';
      end;
  end;

end;


function TevRemoteMiscRoutines.PackBenefitRequestDescription(const AValue: TBenefitRequestDescription): String;
begin
  if not AValue.Declined then
    CheckCondition(AValue.CoBenefitNbr > 0, 'CoBenefitNbr must be greater 0, if request is not declined');
  CheckCondition(AValue.CategoryType <> '', 'Category Type cannot be empty');
  CheckCondition(AValue.Status <> '', 'Status cannot be empty');
  Result := IntToStr(AValue.CoBenefitNbr) + ',' + AValue.CategoryType + ',' + AValue.Status + ',' + sysutils.BoolToStr(AValue.Declined);
end;

function TevRemoteMiscRoutines.UnpackBenefitRequestDescription(const AValue: String): TBenefitRequestDescription;
var
  s1, s2: String;
begin
  s1 := AValue;

  s2 := GetNextStrValue(s1, ',');
  if s2 <> '' then
    Result.CoBenefitNbr := StrToInt(s2)
  else
    Result.CoBenefitNbr := 0;

  s2 := GetNextStrValue(s1, ',');
  if s2 <> '' then
    Result.CategoryType := s2
  else
    raise Exception.Create('Category Type cannot be empty');

  s2 := GetNextStrValue(s1, ',');
  if s2 <> '' then
  begin
    Result.Status := s2;
    CheckCondition((Result.Status = 'P') or (Result.Status = 'S') or (Result.Status = 'A'), 'Wrong value for status');
  end
  else
    raise Exception.Create('Request Status cannot be empty');

  s2 := GetNextStrValue(s1, ',');
  if s2 <> '' then
    Result.Declined := StrToBool(s2)
  else
    raise Exception.Create('Declined cannot be empty');

  if (not Result.Declined) and (Result.CoBenefitNbr = 0) then
    raise Exception.Create('CoBenefitNbr cannot be empty, if request is not declined');
end;

procedure TevRemoteMiscRoutines.RevertBenefitRequest(const ARequestNbr: integer);
var
  BlobField: TBlobField;
  tmpStream: IevDualStream;
  RequestData: IisListOfValues;
  DescriptionData: TBenefitRequestDescription;
  ReqStorageTable, SignatureStorageTable: TevClientDataSet;
  MetaDataset: IevDataset;
begin
  ReqStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  ReqStorageTable.Active := false;
  ReqStorageTable.Filtered := false;
  ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
    ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));

  CheckCondition(ReqStorageTable.RecordCount > 0, 'Benefit Request not found. EE_CHANGE_REQUEST_NBR=' + intToStr(ARequestNbr));
  CheckCondition(ReqStorageTable['SIGNATURE_NBR'] <> null, 'Benefit Request has not been submited yet. EE_CHANGE_REQUEST_NBR=' + intToStr(ARequestNbr));

  if Context.UserAccount.InternalNbr < 0 then
    CheckCondition(ReqStorageTable['EE_NBR'] = Abs(Context.UserAccount.InternalNbr), 'Request was not created by current user');

  tmpStream := ReqStorageTable.GetBlobData('REQUEST_DATA');
  RequestData := TisListOfValues.Create;
  RequestData.ReadFromStream(tmpStream);

  DescriptionData := UnpackBenefitRequestDescription(ReqStorageTable['DESCRIPTION']);
  CheckCondition(DescriptionData.Status = 'S', 'Cannot be reverted. Request''s status should be submited');
  MetaDataset := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;

  SignatureStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_SIGNATURE'));
  SignatureStorageTable.Active := false;
  SignatureStorageTable.Filtered := false;
  SignatureStorageTable.DataRequired('EE_SIGNATURE_NBR=' + IntToStr(ReqStorageTable['SIGNATURE_NBR']));
  CheckCondition(SignatureStorageTable.RecordCount > 0, 'Signature not found. EE_CHANGE_REQUEST_NBR=' + intToStr(ARequestNbr));
  CheckCondition(SignatureStorageTable.RecordCount = 1, 'Too many signatures found. EE_CHANGE_REQUEST_NBR=' + intToStr(ARequestNbr));

  DescriptionData.Status := 'P';
  MetaDataset.Edit;
  MetaDataset['REQUEST_STATUS'] := 'P';
  MetaDataset.Post;
  try
    SignatureStorageTable.Delete;
  except
    SignatureStorageTable.Cancel;
    raise;
  end;

  ReqStorageTable.Edit;
  try
    ReqStorageTable['SIGNATURE_NBR'] := null;
    ReqStorageTable['DESCRIPTION'] := PackBenefitRequestDescription(DescriptionData);
    ReqStorageTable['STATUS'] := 'P';
    ReqStorageTable['STATUS_DATE'] := Mainboard.TimeSource.GetDateTime;

    BlobField := TBlobField(ReqStorageTable.FieldByName('REQUEST_DATA'));
    tmpStream.Size := 0;
    RequestData.WriteToStream(tmpStream);
    tmpStream.Position := 0;
    BlobField.LoadFromStream(tmpStream.realStream);
    ReqStorageTable.Post;
  except
    ReqStorageTable.Cancel;
    raise;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([ReqStorageTable, SignatureStorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

procedure TevRemoteMiscRoutines.SubmitBenefitRequest(const ARequestNbr: integer; const ASignatureParams: IisListOfValues);
var
  ReqBlobField, ReportBlobField: TBlobField;
  tmpStream: IevDualStream;
  RequestData: IisListOfValues;
  DescriptionData: TBenefitRequestDescription;
  ReqStorageTable, SignatureStorageTable: TevClientDataSet;
  MetaDataset, BenefitDataset: IevDataset;
  sIPAddress: String;
  ReportData: IisStream;
  bEventBased: boolean;
begin
  CheckCondition(Context.UserAccount.InternalNbr < 0, 'This is not ESS user');

  ReqStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  ReqStorageTable.Active := false;
  ReqStorageTable.Filtered := false;
  ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
    ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
  CheckCondition(ReqStorageTable['SIGNATURE_NBR'] = null, 'Benefit Request has been submited already. EE_CHANGE_REQUEST_NBR=' + intToStr(ARequestNbr));

  sIPAddress := ASignatureParams.Value['IP'];

  tmpStream := ReqStorageTable.GetBlobData('REQUEST_DATA');
  RequestData := TisListOfValues.Create;
  RequestData.ReadFromStream(tmpStream);

  DescriptionData := UnpackBenefitRequestDescription(ReqStorageTable['DESCRIPTION']);
  CheckCondition(DescriptionData.Status = 'P', 'Only pending requests may be submitted');
  MetaDataset := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;
  BenefitDataset := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DATASET']) as IevDataset;
  if not DescriptionData.Declined then
    CheckCondition(ReqStorageTable['EE_NBR'] = Abs(Context.UserAccount.InternalNbr), 'Only the ESS user who created the request may submit it');

  try
    bEventBased := BenefitDataset['LAST_QUAL_BENEFIT_EVENT_START_DATE'] <> null;
  except
    bEventBased := False; // for compatibility with old requests without LAST_QUAL_BENEFIT_EVENT_START_DATE field
  end;

  SignatureStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_SIGNATURE'));
  SignatureStorageTable.Active := false;
  SignatureStorageTable.Filtered := false;
  SignatureStorageTable.DataRequired('1=2');

  ReportData := GetBenefitRequestConfirmationReport(ARequestNbr);

  DescriptionData.Status := 'S';

  MetaDataset.Edit;
  MetaDataset['REQUEST_STATUS'] := 'S';
  MetaDataset.Post;

  SignatureStorageTable.Append;
  try
    SignatureStorageTable['EE_NBR'] := Abs(Context.UserAccount.InternalNbr);

    SignatureStorageTable['CO_BENEFIT_ENROLLMENT_NBR'] := GetCoBenefitEnrollmentNbr(BenefitDataset['CO_BENEFIT_CATEGORY_NBR'], VarToStr(BenefitDataset['Category_type_desc']), bEventBased);
    SignatureStorageTable['DATA'] := sIPAddress;
    ReportBlobField := TBlobField(SignatureStorageTable.FieldByName('DOCUMENT'));
    ReportData.Position := 0;
    ReportBlobField.LoadFromStream(ReportData.RealStream);
    SignatureStorageTable.Post;
  except
    SignatureStorageTable.Cancel;
    raise;
  end;

  ReqStorageTable.Edit;
  try
    ReqStorageTable['SIGNATURE_NBR'] := SignatureStorageTable['EE_SIGNATURE_NBR'];
    ReqStorageTable['DESCRIPTION'] := PackBenefitRequestDescription(DescriptionData);
    ReqStorageTable['STATUS'] := 'S';
    ReqStorageTable['STATUS_DATE'] := Mainboard.TimeSource.GetDateTime;;
    ReqBlobField := TBlobField(ReqStorageTable.FieldByName('REQUEST_DATA'));
    tmpStream.Size := 0;
    RequestData.WriteToStream(tmpStream);
    tmpStream.Position := 0;
    ReqBlobField.LoadFromStream(tmpStream.realStream);
    ReqStorageTable.Post;
  except
    ReqStorageTable.Cancel;
    raise;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([SignatureStorageTable, ReqStorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevRemoteMiscRoutines.GetBenefitRequestConfirmationReport(const ARequestNbr: integer; const isResultPDF: boolean=true): IisStream;
var
  ReportParams: TrwReportParams;
  tmpStream: IisStream;
  ReportResults : TrwReportResults;
  PDFInfoRec: TrwPDFDocInfo;
begin
  Result := TisStream.Create;

  ReportParams := TrwReportParams.Create;
  try
    ReportParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('RequestNbr', ARequestNbr);
    ctx_RWLocalEngine.StartGroup;
    ctx_RWLocalEngine.CalcPrintReport(2351, REPORT_LEVEL_SYSTEM, ReportParams);
    tmpStream := ctx_RWLocalEngine.EndGroup(rdtNone);

    if isResultPDF then
    begin
      ReportResults := TrwReportResults.Create;
      try
        ReportResults.SetFromStream(tmpStream);
        if (ReportResults[0].ErrorMessage <> '') or not Assigned(ReportResults[0].Data) then
          raise Exception.Create('Error in Benefit Enrollment Confirmation report' + #13#10 + ReportResults[0].ErrorMessage);
        PDFInfoRec.Author := 'Evolution Report Writer';
        PDFInfoRec.Creator := 'Evolution Report Engine';
        PDFInfoRec.Subject := '';
        PDFInfoRec.Title := ReportResults[0].ReportName;
        PDFInfoRec.Compression := True;
        PDFInfoRec.ProtectionOptions := [];
        PDFInfoRec.OwnerPassword := '';
        PDFInfoRec.UserPassword := '';
        Result := Context.RWLocalEngine.ConvertRWAtoPDF(ReportResults[0].Data, PDFInfoRec)
      finally
        FreeAndNil(ReportResults);
      end;
    end
    else
     result:= tmpStream;

  finally
    reportParams.Free;
  end;
end;

procedure TevRemoteMiscRoutines.SendBenefitEmailToEE(const AEENbr: integer; const ASubject, AMessage: String;
  const ARequestNbr: integer; const ACategoryType: String);
var
  Q: IEvQuery;
  sFrom, sTo : String;
  EMail1, EMail2, EMail3: String;
  Mes: IevMailMessage;
  sRequestInfo: String;
  ReqStorageTable: TevClientDataSet;
  MetaDataset, BenefitDataset: IevDataset;
  tmpStream: IevDualStream;
  RequestData: IisListOfValues;
  sStatus: String;
begin
  // getting information about employee
  Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR, ' +
    ' a.e_mail_address as EE_MAIL, b.e_mail_address as CL_PERSON_MAIL, a.BENEFIT_E_MAIL_ADDRESS ' +
    ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and ee_nbr=:EENbr' +
    ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ');
  Q.Params.AddValue('EENbr', AEeNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + IntToStr(AEeNbr));

  // preparing message
  sFrom := 'EvolutionSelfServe';

  sRequestInfo := '';
  if ARequestNbr > 0 then
  begin
    ReqStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
    ReqStorageTable.Active := false;
    ReqStorageTable.Filtered := false;
    ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
      ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
    CheckCondition(ReqStorageTable.RecordCount > 0, 'Request not found. EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
    CheckCondition(ReqStorageTable['EE_NBR'] = AEENbr, 'Benefit enrollment request''s information belongs to different employee');

    tmpStream := ReqStorageTable.GetBlobData('REQUEST_DATA');
    RequestData := TisListOfValues.Create;
    RequestData.ReadFromStream(tmpStream);

    MetaDataset := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;
    BenefitDataset := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DATASET']) as IevDataset;
    if MetaDataset['REQUEST_STATUS'] = 'P' then
      sStatus := 'PENDING'
    else if MetaDataset['REQUEST_STATUS'] = 'S' then
      sStatus := 'SUBMITED'
    else if MetaDataset['REQUEST_STATUS'] = 'A' then
      sStatus := 'APPROVED'
    else
      raise Exception.Create('Unknown value of status: ' + VarToStr(MetaDataset['REQUEST_STATUS']));

    sRequestInfo := '   Benefit Enrollment Request Information' + #13#10;
    sRequestInfo := sRequestInfo + '--------------------------------------------' + #13#10;
    sRequestInfo := sRequestInfo + 'Category: ' + VarToStr(BenefitDataset['Category_type_desc']) + #13#10;
    sRequestInfo := sRequestInfo + 'Benefit: ' + VarToStr(BenefitDataset['benefit_name']) + #13#10;
    sRequestInfo := sRequestInfo + 'Status: ' + sStatus + #13#10;
  end;

  if ACategoryType <> '' then
  begin
    DM_COMPANY.CO_BENEFIT_CATEGORY.Close;
    DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CATEGORY_TYPE=''' + VarToStr(ACategoryType) +
      ''' and CO_NBR=' + VarToStr(Q.Result['CO_NBR']));
    CheckCondition(DM_COMPANY.CO_BENEFIT_CATEGORY.RecordCOunt = 1, 'Benefit Category not found in CO_BENEFIT_CATEGORY table. CATEGORY_TYPE=' + VarToStr(ACategoryType));

    sRequestInfo := '   Benefit Enrollment Information' + #13#10;
    sRequestInfo := sRequestInfo + '--------------------------------------------' + #13#10;
    sRequestInfo := sRequestInfo + 'Category: ' + VarToStr(DM_COMPANY.CO_BENEFIT_CATEGORY['NAME']) + #13#10;
  end;

  if (Q.result.FieldByName('BENEFIT_E_MAIL_ADDRESS').Value <> null) or
    (Q.result.FieldByName('EE_MAIL').Value <> null) or (Q.result.FieldByName('CL_PERSON_MAIL').Value <> null) then
  begin
    EMail1 := VarToStr(Q.result.FieldByName('BENEFIT_E_MAIL_ADDRESS').Value);
    EMail2 := VarToStr(Q.result.FieldByName('EE_MAIL').Value);
    EMail3 := VarToStr(Q.result.FieldByName('CL_PERSON_MAIL').Value);
    if EMail1 <> '' then
      sTo := EMail1
    else if EMail2 <> '' then
      sTo := EMail2
    else
      sTo := EMail3;

    if Trim(sTo) <> '' then
    begin
      Mes := TevMailMessage.Create;

      Mes.AddressFrom := sFrom;
      Mes.AddressTo := sTo;
      Mes.Subject := ASubject;

      if (ARequestNbr > 0) or (ACategoryType <> '') then
        Mes.Body := AMessage + #13#10#13#10 + sRequestInfo
      else
        Mes.Body := AMessage;

      try
        ctx_EMailer.SendMail(Mes);
      except
        on E: Exception do
          raise ESendEmailError.Create('Cannot send benefit enrollment email to employee. EE#' + Q.Result['CUSTOM_EMPLOYEE_NUMBER'] + '. From: ' + sFrom + '. To: ' + sTo + #13#10 + 'Error:' + E.Message);
      end;
    end;
  end;
end;

function TevRemoteMiscRoutines.GetEmptyBenefitRequest: IisListOfValues;
var
  dsMeta, dsBenefit, dsDependent, dsBeneficiary: IevDataset;
begin
  Result := TisListOfValues.Create;
  dsMeta := TevDataset.Create;
  dsBenefit := TevDataset.Create;
  dsDependent := TevDataset.Create;
  dsBeneficiary := TevDataset.Create;

  Result.AddValue('EE_HR_BENEFIT_REQ_META_DATASET', dsMeta);
  Result.AddValue('EE_HR_BENEFIT_REQ_DATASET', dsBenefit);
  Result.AddValue('EE_HR_BENEFIT_REQ_DEPENDENT_DATASET', dsDependent);
  Result.AddValue('EE_HR_BENEFIT_REQ_BENEFICIARY_DATASET', dsBeneficiary);

  dsMeta.vclDataSet.FieldDefs.Add('request_status', ftString, 1, true);
  dsMeta.vclDataSet.Open;
  dsMeta.Append;
  dsMeta['request_status'] := 'P';
  dsMeta.Post;

  dsBenefit.vclDataSet.FieldDefs.Add('declined', ftBoolean, 0, true);
  dsBenefit.vclDataSet.FieldDefs.Add('sy_hr_refusal_reason_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('refusal_description', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('healthcare_coverage', ftString, 1, false);
  dsBenefit.vclDataSet.FieldDefs.Add('sy_states_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ee_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('co_benefit_category_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('category_type', ftString, 1, true);
  dsBenefit.vclDataSet.FieldDefs.Add('category_type_desc', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('co_benefits_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('benefit_name', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('show_rates', ftString, 1, false);
  dsBenefit.vclDataSet.FieldDefs.Add('allow_hsa', ftString, 1, false);
  dsBenefit.vclDataSet.FieldDefs.Add('enrollment_start_date', ftDateTime, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('enrollment_end_date', ftDateTime, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('co_benefit_subtype_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('description', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('co_benefit_rates_nbr', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ee_rate', ftFloat, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('cobra_rate', ftFloat, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('max_dependents', ftInteger, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('period_begin', ftDateTime, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('period_end', ftDateTime, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('pcp', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('existing_patient', ftString, 1, false);
  dsBenefit.vclDataSet.FieldDefs.Add('additional_info1', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('additional_info2', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('additional_info3', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ALLOW_EE_CONTRIBUTION', ftString, 1, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE1', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE2', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ADDITIONAL_INFO_TITLE3', ftString, 40, false);
  dsBenefit.vclDataSet.FieldDefs.Add('ANNUAL_MAXIMUM_AMOUNT', ftFloat, 0, false); // actually it's employee's maximum amount
  dsBenefit.vclDataSet.FieldDefs.Add('ANNUAL_EE_CONTRIBUTION', ftFloat, 0, false);
  dsBenefit.vclDataSet.FieldDefs.Add('RATE_TYPE', ftString, 1, false);  // from CO_BENEFIT_RATES
  dsBenefit.vclDataSet.FieldDefs.Add('LAST_QUAL_BENEFIT_EVENT_START_DATE', ftDate, 0, false); // fill up if request is event based
  dsBenefit.vclDataSet.Open;

  dsDependent.vclDataSet.FieldDefs.Add('ee_beneficiary_nbr', ftInteger, 0, true);
  dsDependent.vclDataSet.FieldDefs.Add('cl_person_dependents_nbr', ftInteger, 0, true);
  dsDependent.vclDataSet.FieldDefs.Add('primary_beneficiary', ftString, 1, false);
  dsDependent.vclDataSet.FieldDefs.Add('percentage', ftInteger, 0, false);
  dsDependent.vclDataSet.Open;

  dsBeneficiary.vclDataSet.FieldDefs.Add('ee_beneficiary_nbr', ftInteger, 0, true);
  dsBeneficiary.vclDataSet.FieldDefs.Add('cl_person_dependents_nbr', ftInteger, 0, true);
  dsBeneficiary.vclDataSet.FieldDefs.Add('primary_beneficiary', ftString, 1, true);
  dsBeneficiary.vclDataSet.FieldDefs.Add('percentage', ftInteger, 0, true);
  dsBeneficiary.vclDataSet.Open;
end;

procedure TevRemoteMiscRoutines.CloseBenefitEnrollment(const AEENbr: integer; const ACategoryType: String;
  const ARequestNbr: integer);
var
  iRequestNbr: integer;
  ReqStorageTable: TevClientDataSet;
  DescriptionData, NewDescriptionData: TBenefitRequestDescription;
  bFound: boolean;
  RequestData: IisListOfValues;
  dsMeta, dsBenefit: IevDataset;
  BlobField: TBlobField;
  tmpStream: IevDualStream;
  qRefusalReason: IEvQuery;
  sBenefitEEEmailAddress, sEEEnd, sRequestInfo: String;
  dNow: TDateTime;
  bEvent: boolean;

begin
  DM_EMPLOYEE.EE.Close;
  DM_EMPLOYEE.EE.DataRequired('EE_NBR=' + IntToStr(AEENbr));
  CheckCondition(DM_EMPLOYEE.EE.RecordCount = 1, 'Employee not found. EE_NR=' + IntToStr(AEENbr));
  DM_CLIENT.CL_PERSON.Close;
  DM_CLIENT.CL_PERSON.DataRequired('CL_PERSON_NBR=' + VarToStr(DM_EMPLOYEE.EE['CL_PERSON_NBR']));
  CheckCondition(DM_CLIENT.CL_PERSON.RecordCount = 1, 'Person not found. CL_PERSON_NR=' + VarToStr(DM_EMPLOYEE.EE['CL_PERSON_NBR']));

  ReqStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  ReqStorageTable.Active := false;
  ReqStorageTable.Filtered := false;
  ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
    ''' and EE_NBR=' + IntToStr(AEENbr));


  dNow:= Mainboard.TimeSource.GetDateTime;

  DM_COMPANY.CO_BENEFIT_CATEGORY.Close;

  DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CATEGORY_TYPE=''' + ACategoryType +
    ''' and CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
  CheckCondition(DM_COMPANY.CO_BENEFIT_CATEGORY.RecordCount > 0, 'Benefit Category Type not found: "' + ACategoryType + '"');
  CheckCondition(DM_COMPANY.CO_BENEFIT_CATEGORY.RecordCount = 1, 'Not unique Benefit Category Type: "' + ACategoryType + '"');


  DM_COMPANY.CO_BENEFIT_SETUP.Close;
  DM_COMPANY.CO_BENEFIT_SETUP.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));

  qRefusalReason := TEvQuery.Create('select a.* from SY_HR_REFUSAL_REASON a where {AsOfNow<a>} and a.SY_HR_REFUSAL_REASON_NBR=1');
  qRefusalReason.Execute;
  CheckCondition(qRefusalReason.Result.RecordCount = 1, 'Cannot find record in SY_HR_REFUSAL_REASON for "No Election - Closed By HR"');

  sEEEnd := '';
  if DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_END_TEXT'] <> null then
    sEEEnd := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('EE_ENROLL_END_TEXT')));
  sBenefitEEEmailAddress := Trim(VarToStr(DM_EMPLOYEE.EE['BENEFIT_E_MAIL_ADDRESS']));
  if sBenefitEEEmailAddress = '' then
  begin
    sBenefitEEEmailAddress := Trim(VarToStr(DM_EMPLOYEE.EE['e_mail_address']));
    if sBenefitEEEmailAddress = '' then
      sBenefitEEEmailAddress := Trim(VarToStr(DM_CLIENT.CL_PERSON['e_mail_address']));
  end;

  iRequestNbr := -1;
  if ARequestNbr > 0 then
  begin
    iRequestNbr := ARequestNbr;
    bFound := ReqStorageTable.Locate('EE_CHANGE_REQUEST_NBR', ARequestNbr, []);
    CheckCondition(bFound, 'Benefit request not found. EE_CHANGE_REQUEST=' + IntToStr(ARequestNbr));
    DescriptionData := UnpackBenefitRequestDescription(ReqStorageTable['DESCRIPTION']);
    CheckCondition(DescriptionData.Status = 'P', 'Only pending requests may be closed. EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
    CheckCondition(DescriptionData.CategoryType = ACategoryType, 'Logical error in data. Request''s category does not match. EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
  end
  else
  begin
    // looking for existing request with such category
    ReqStorageTable.First;
    while not ReqStorageTable.Eof do
    begin
      DescriptionData := UnpackBenefitRequestDescription(ReqStorageTable['DESCRIPTION']);
      if DescriptionData.CategoryType = ACategoryType then
      begin
        CheckCondition(DescriptionData.Status = 'P', 'Only pending requests may be closed. EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
        iRequestNbr := ReqStorageTable['EE_CHANGE_REQUEST_NBR'];
        break;
      end;
      ReqStorageTable.Next;
    end;
  end;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    tmpStream := ReqStorageTable.GetBlobData('REQUEST_DATA');
    RequestData := TislistOfValues.Create;
    RequestData.ReadFromStream(tmpStream);

    // preparing request's data
    dsMeta := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;
    dsBenefit := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DATASET']) as IevDataset;
    try
      bEvent := dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE'] <> null;
    except
      bEvent := False;
    end;

    DM_COMPANY.CO_BENEFIT_ENROLLMENT.Close;
    if bEvent then
      DM_COMPANY.CO_BENEFIT_ENROLLMENT.DataRequired(
                 'CO_BENEFIT_CATEGORY_NBR='+ DM_COMPANY.CO_BENEFIT_CATEGORY.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsString +
                           ' and CATEGORY_COVERAGE_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and CATEGORY_COVERAGE_END_DATE >=' + QuotedStr(DateToStr(dNow)))
    else
      DM_COMPANY.CO_BENEFIT_ENROLLMENT.DataRequired(
                 'CO_BENEFIT_CATEGORY_NBR='+ DM_COMPANY.CO_BENEFIT_CATEGORY.fieldByName('CO_BENEFIT_CATEGORY_NBR').AsString +
                           ' and ENROLLMENT_START_DATE <=' + QuotedStr(DateToStr(dNow)) + ' and ENROLLMENT_END_DATE >=' + QuotedStr(DateToStr(dNow)));
    DM_COMPANY.CO_BENEFIT_ENROLLMENT.First;


    // editing pending request
    if iRequestNbr > 0 then
    begin

      dsMeta.Edit;
      dsMeta['request_status'] := 'A';
      dsMeta.Post;
      dsBenefit.Edit;
      dsBenefit['ee_nbr'] := AEENbr;
      dsBenefit['declined'] := true;
      dsBenefit['co_benefit_category_nbr'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR'];
      dsBenefit['category_type'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE'];
      dsBenefit['category_type_desc'] := DM_COMPANY.CO_BENEFIT_CATEGORY['NAME'];
      dsBenefit['sy_hr_refusal_reason_nbr'] := qRefusalReason.Result['SY_HR_REFUSAL_REASON_NBR'];
      dsBenefit['refusal_description'] := qRefusalReason.Result['DESCRIPTION'];
      dsBenefit.Post;

      NewDescriptionData.CoBenefitNbr := 0;
      NewDescriptionData.CategoryType := ACategoryType;
      NewDescriptionData.Status := 'A';
      NewDescriptionData.Declined := True;
      ReqStorageTable.Edit;
      try
        ReqStorageTable['DESCRIPTION'] := PackBenefitRequestDescription(NewDescriptionData);
        tmpStream := TevDualStreamHolder.Create;
        RequestData.WriteToStream(tmpStream);
        tmpStream.Position := 0;
        BlobField := TBlobField(ReqStorageTable.FieldByName('REQUEST_DATA'));
        BlobField.LoadFromStream(tmpStream.realStream);
        ReqStorageTable.Post;
      except
        ReqStorageTable.Cancel;
        raise;
      end;
    end;

     CheckCondition(not DM_COMPANY.CO_BENEFIT_ENROLLMENT.FieldByName('CO_BENEFIT_ENROLLMENT_NBR').IsNull, 'Missing benefit category enrollment date range. CATEGORY_TYPE=' + ReturnDescription(Benefit_Category_Type_ComboChoices, ACategoryType));
     StoreBenefitRefusalReasonForEE(AEENbr, DM_COMPANY.CO_BENEFIT_ENROLLMENT['CO_BENEFIT_ENROLLMENT_NBR'], qRefusalReason.Result['SY_HR_REFUSAL_REASON_NBR'],
       DM_EMPLOYEE.EE['HOME_TAX_EE_STATES_NBR'], DM_COMPANY.CO_BENEFIT_ENROLLMENT.ENROLLMENT_END_DATE.AsDateTime + 1);

    DM_EMPLOYEE.EE.Edit;
    try
      DM_EMPLOYEE.EE['BENEFIT_ENROLLMENT_COMPLETE'] := Mainboard.TimeSource.GetDateTime;
      DM_EMPLOYEE.EE.post;
    except
      DM_EMPLOYEE.EE.Cancel;
      raise;
    end;

    ctx_DataAccess.PostDataSets([ReqStorageTable, DM_EMPLOYEE.EE]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  if iRequestNbr > 0 then
  begin
    Sleep(1000);
    try
      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
        ''' and EE_NBR=' + IntToStr(AEENbr));
      bFound := ReqStorageTable.Locate('EE_CHANGE_REQUEST_NBR', iRequestNbr, []);
      CheckCondition(bFound, 'Cannot delete benefit request because it has not been found. EE_CHANGE_REQUEST=' + IntToStr(iRequestNbr));
      try
        ReqStorageTable.Delete;
      except
        ReqStorageTable.Cancel;
        raise;
      end;
      ctx_DataAccess.PostDataSets([ReqStorageTable]);
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;

  if (sBenefitEEEmailAddress <> '') and (sEEEnd <> '') then
    try
      sRequestInfo := '   Benefit Enrollment Information' + #13#10;
      sRequestInfo := sRequestInfo + '--------------------------------------------' + #13#10;
      sRequestInfo := sRequestInfo + 'Category: ' + VarToStr(DM_COMPANY.CO_BENEFIT_CATEGORY['NAME']) + #13#10 + 'Your enrollment has been closed by HR person';
      ctx_EMailer.SendQuickMail(sBenefitEEEmailAddress, cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_END_SUBJECT']) + '. Category: ' + VarToStr(DM_COMPANY.CO_BENEFIT_CATEGORY['NAME']),
        sEEEnd + #13#10#13#10 + sRequestInfo);
    except
      on E: Exception do
        GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email about closed benefit enrollment to employee. EE#' + DM_EMPLOYEE.EE['CUSTOM_EMPLOYEE_NUMBER'], E.Message + #13#10 + GetErrorCallStack(E));
    end;
end;

function TevRemoteMiscRoutines.GetDataDictionaryXML: IisStream;
begin
  Result := EvoDataDictionaryXML;
end;


function TevRemoteMiscRoutines.CheckIfUserIsHR: boolean;
var
  qSbUser: IevQuery;
  iSbUserNbr: Integer;
  iClientId: integer;
begin
  Result := False;
  iSbUserNbr := Context.UserAccount.InternalNbr;

  Context.Security.EnableSystemAccountRights;
  try
    Context.DBAccess.DisableSecurity;
    try
      qSbUser := TevQuery.Create('select SB_USER_NBR, USER_ID, EMAIL_ADDRESS, LAST_NAME, FIRST_NAME from SB_USER a where {AsOfNow<a>} and a.HR_PERSONNEL=''Y''' +
        ' and a.ACTIVE_USER=''Y'' and a.SB_USER_NBR=' + IntToStr(iSbUserNbr));
      qSbUser.Execute;
    finally
      Context.DBAccess.EnableSecurity;
    end;

    if qSbUser.Result.RecordCount = 0 then
      exit;

    iClientId := ctx_DataAccess.ClientId;
    while not qSbUser.Result.Eof do
    begin
      if Context.Security.GetUserRights(qSbUser.Result['SB_USER_NBR']).Clients.IsClientEnabled(iClientId) then
      begin
        Result := True;
        Exit;
      end;
      qSbUser.Result.Next;
    end;
  finally
    Context.Security.DisableSystemAccountRights;
  end;
end;


procedure TevRemoteMiscRoutines.StoreBenefitRefusalReasonForEE(const AEENbr: integer; const ACategory: String;
  const ARefusalReason: integer; const AHomeState: Variant; const ARefusalDate: TDate);
var
  bFound: boolean;
  Q: IevQuery;
begin
    DM_EMPLOYEE.EE_BENEFIT_REFUSAL.DataRequired('ALL');
    bFound := DM_EMPLOYEE.EE_BENEFIT_REFUSAL.Locate('EE_NBR;CO_BENEFIT_ENROLLMENT_NBR', VarArrayOf([AEENbr, ACategory]), []);

    if bFound then
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL.Edit
    else
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL.Append;

    try
      if not bFound then
      begin
        DM_EMPLOYEE.EE_BENEFIT_REFUSAL['EE_NBR'] := AEENbr;
        DM_EMPLOYEE.EE_BENEFIT_REFUSAL['CO_BENEFIT_ENROLLMENT_NBR'] := ACategory;
      end;
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL['SY_HR_REFUSAL_REASON_NBR'] := ARefusalReason;
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL['HOME_STATE_NBR'] := AHomeState;
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL['STATUS'] := 'C';
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL['STATUS_DATE'] := ARefusalDate;

      if AHomeState <> null then
      begin
        DM_EMPLOYEE.EE_STATES.Close;
        DM_EMPLOYEE.EE_STATES.DataRequired('EE_STATES_NBR=' + VarToStr(AHomeState));
        if DM_EMPLOYEE.EE_STATES['SUI_APPLY_CO_STATES_NBR'] <> null then
        begin
          Q := TevQuery.Create('select EE_STATES_NBR from EE_STATES a, CO_STATES b ' +
            ' where {AsOfNow<a>} and {AsOfNow<a>} ' +
            ' and b.CO_STATES_NBR =:p1 and a.CO_STATES_NBR = b.CO_STATES_NBR and a.EE_NBR=:p2');
          Q.Params.AddValue('p1', DM_EMPLOYEE.EE_STATES['SUI_APPLY_CO_STATES_NBR']);
          Q.Params.AddValue('p2', AEENbr);
          Q.Execute;
          if Q.Result.RecordCount > 0 then
            DM_EMPLOYEE.EE_BENEFIT_REFUSAL['SUI_STATE_NBR'] := Q.Result['EE_STATES_NBR'];
        end;
      end;
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL.Post;

      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_BENEFIT_REFUSAL]);
    except
      DM_EMPLOYEE.EE_BENEFIT_REFUSAL.Cancel;
      raise;
    end;
end;

function TevRemoteMiscRoutines.GetHRPersonsForOpenedClient: IisParamsCollection;
var
  qSbUser: IevQuery;
  iClientId: integer;
  OneHRInfo: IisListOfValues;
begin
  Result := TisParamsCollection.Create;

  Context.Security.EnableSystemAccountRights;
  try
    Context.DBAccess.DisableSecurity;
    try
      qSbUser := TevQuery.Create('select SB_USER_NBR, USER_ID, EMAIL_ADDRESS, LAST_NAME, FIRST_NAME from SB_USER a where {AsOfNow<a>} and a.HR_PERSONNEL=''Y''' +
        ' and a.ACTIVE_USER=''Y''');
      qSbUser.Execute;
      CheckCondition(qSbUser.Result.RecordCount > 0, 'No HR personnel found to send email');
    finally
      Context.DBAccess.EnableSecurity;
    end;

    iClientId := ctx_DataAccess.ClientId;

    while not qSbUser.Result.Eof do
    begin
      if Context.Security.GetUserRights(qSbUser.Result['SB_USER_NBR']).Clients.IsClientEnabled(iClientId) then
      begin
        OneHRInfo := Result.AddParams(IntToStr(Result.Count));
        OneHRInfo.AddValue('SB_USER_NBR', qSbUser.Result['SB_USER_NBR']);
        OneHRInfo.AddValue('USER_ID', qSbUser.Result['USER_ID']);
        OneHRInfo.AddValue('EMAIL_ADDRESS', VarToStr(qSbUser.Result['EMAIL_ADDRESS']));
        OneHRInfo.AddValue('LAST_NAME', qSbUser.Result['LAST_NAME']);
        OneHRInfo.AddValue('FIRST_NAME', qSbUser.Result['FIRST_NAME']);
      end;
      qSbUser.Result.Next;
    end;
  finally
    Context.Security.DisableSystemAccountRights;
  end;
end;
procedure TevRemoteMiscRoutines.ApproveBenefitRequest(const ARequestNbr: integer);
type
  TEEorER = (tEEFlag, tERFlag);
var
  ReqStorageTable, SignatureStorageTable: TevClientDataSet;
  DescriptionData, newDescriptionData: TBenefitRequestDescription;
  RequestData: IisListOfValues;
  dsMeta, dsBenefit, dsDependent, dsBenef: IevDataset;
  BlobField: TBlobField;
  tmpStream: IevDualStream;
  iEENbr: integer;
  Q, Q1: IevQuery;
  bFound: boolean;
  bDeclined: boolean;
  bEventBased, bAddScheduledEDS: boolean;
  sEESummary, sEEEnd: String;
  sBenefitEEEmailAddress: String;
  ReportData: IisStream;
  EEMailMessage: IevMailMessage;
  sRequestInfo: String;
  sRefusalReasonDescription: String;
  bEmployeeDefined: boolean;
  dsEEBenefits: TevClientDataSet;
  Tbl: TClass;
  qCoEdCodes, qCoBenefits, qCoBenefitSubtype, qClEDs, qCoBenefitRates: IevQuery;
  tmpDate: TDateTime;

  procedure AddSchedED(const ACoEdCodeNbr: integer; const AEEOrER: TEEorER);
  begin
    if not qCoEdCodes.result.Locate('CO_E_D_CODES_NBR', ACoEdCodeNbr, []) then
      raise Exception.Create('Cannot find record in CO_E_D_CODES_NBR table. CO_E_D_CODES_NBR=' + IntToStr(ACoEdCodeNbr));

    if not qClEDs.Result.Locate('CL_E_DS_NBR', qCoEdCodes.result['CL_E_DS_NBR'], []) then
      raise Exception.Create('Cannot find record in CL_E_DS table. CL_E_DS_NBR=' + VarToStr(qCoEdCodes.result['CL_E_DS_NBR']));

    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Append;
    try
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR'] := iEENbr;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_AGENCY_NBR'] := qCoBenefits.Result['CL_AGENCY_NBR'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_DS_NBR'] := qClEDs.Result['CL_E_DS_NBR'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'] := qCoBenefitRates.Result['CL_E_D_GROUPS_NBR'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['FREQUENCY'] := qCoBenefits.Result['FREQUENCY'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_1'] := qClEDs.Result['SD_EXCLUDE_WEEK_1'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_2'] := qClEDs.Result['SD_EXCLUDE_WEEK_2'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_3'] := qClEDs.Result['SD_EXCLUDE_WEEK_3'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_4'] := qClEDs.Result['SD_EXCLUDE_WEEK_4'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_5'] := qClEDs.Result['SD_EXCLUDE_WEEK_5'];
      if bEventBased then
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE']
      else
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := qCoBenefitRates.Result['PERIOD_BEGIN'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] := qCoBenefitRates.Result['PERIOD_END'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['CALCULATION_TYPE'] := CALC_METHOD_FIXED;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['ALWAYS_PAY'] := qClEDs.Result['SD_ALWAYS_PAY'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['BENEFIT_AMOUNT_TYPE'] := BENEFIT_AMOUNT_TYPE_NONE;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['DEDUCTIONS_TO_ZERO'] := qClEDs.Result['SD_DEDUCTIONS_TO_ZERO'];
      DM_EMPLOYEE.EE_SCHEDULED_E_DS['USE_PENSION_LIMIT'] := qClEDs.Result['SD_USE_PENSION_LIMIT'];

      if bEmployeeDefined then
      begin
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_BENEFITS_NBR'] := dsEEBenefits['EE_BENEFITS_NBR'];
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['CO_BENEFIT_SUBTYPE_NBR'] := null;
      end
      else
      begin
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_BENEFITS_NBR'] := null;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['CO_BENEFIT_SUBTYPE_NBR'] := qCoBenefitSubtype.Result['CO_BENEFIT_SUBTYPE_NBR'];
      end;

      DM_EMPLOYEE.EE_SCHEDULED_E_DS['ANNUAL_MAXIMUM_AMOUNT'] := qClEDs.Result['ANNUAL_MAXIMUM'];

      // FSA, HSA, etc.
      if bEmployeeDefined then
      begin
        CheckCondition(Trunc(qCoBenefitRates.Result['PERIOD_END']) <> EncodeDate(2100,  1, 1), 'Wrong setup of benefit rate. Period End date should be provided');
        DM_EMPLOYEE.EE_SCHEDULED_E_DS['TARGET_ACTION'] := SCHED_ED_TARGET_LEAVE;
        if AEEOrER = tEEFlag then
        begin
          CheckCondition(dsBenefit['ANNUAL_EE_CONTRIBUTION'] <= qClEDs.Result['ANNUAL_MAXIMUM'], 'Employee''s contribution cannot be greater annual maximum');
          if RoundTwo(dsBenefit['ANNUAL_EE_CONTRIBUTION']) = 0 then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
            exit;
          end;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS['TARGET_AMOUNT'] := dsBenefit['ANNUAL_EE_CONTRIBUTION'];
          DM_EMPLOYEE.EE_SCHEDULED_E_DS['ANNUAL_MAXIMUM_AMOUNT'] := dsBenefit['ANNUAL_EE_CONTRIBUTION'];
        end
        else
        begin
          if (qClEDs.Result['ANNUAL_MAXIMUM'] = null) or (RoundTwo(qClEDs.Result['ANNUAL_MAXIMUM']) = 0) then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
            exit;
          end;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS['TARGET_AMOUNT'] := qClEDs.Result['ANNUAL_MAXIMUM'];
          DM_EMPLOYEE.EE_SCHEDULED_E_DS['ANNUAL_MAXIMUM_AMOUNT'] := qClEDs.Result['ANNUAL_MAXIMUM'];
        end;
      end;

      DM_EMPLOYEE.EE_SCHEDULED_E_DS.Post;
    except
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
      raise;
    end;
  end;

begin
  ReqStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  ReqStorageTable.Active := false;
  ReqStorageTable.Filtered := false;
  ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
    ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
  CheckCondition(ReqStorageTable.RecordCount > 0, 'Request not found. EE_CHANGE_REQUEST_NBR=' + IntTOStr(ARequestNbr));
  CheckCondition(ReqStorageTable['REQUEST_TYPE'] = EEChangeRequestTypeToString(tpBenefitRequest), 'It is not benefit enrollment request');
  CheckCondition(ReqStorageTable['STATUS_DATE'] <> null, 'EE_CHANGE_REQUEST Status Date cannot be null');
  iEENbr := ReqStorageTable['EE_NBR'];

  DescriptionData := UnpackBenefitRequestDescription(ReqStorageTable['DESCRIPTION']);
  CheckCondition(DescriptionData.Status = 'S', 'Only submitted requests may be approved');
  newDescriptionData.CoBenefitNbr := DescriptionData.CoBenefitNbr;
  newDescriptionData.CategoryType := DescriptionData.CategoryType;
  newDescriptionData.Status := 'A';
  newDescriptionData.Declined := DescriptionData.Declined;

  SignatureStorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_SIGNATURE'));
  SignatureStorageTable.Active := false;
  SignatureStorageTable.Filtered := false;
  SignatureStorageTable.DataRequired('EE_SIGNATURE_NBR=' + VarToStr(ReqStorageTable['SIGNATURE_NBR']));
  CheckCondition(SignatureStorageTable.RecordCount > 0, 'Signature not found for request. EE_CHANGE_REQUEST_NBR=' + IntTOStr(ARequestNbr));

  tmpStream := ReqStorageTable.GetBlobData('REQUEST_DATA');
  RequestData := TislistOfValues.Create;
  RequestData.ReadFromStream(tmpStream);

  dsMeta := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_META_DATASET']) as IevDataset;
  dsBenefit := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DATASET']) as IevDataset;
  dsDependent := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_DEPENDENT_DATASET']) as IevDataset;
  dsBenef := IInterface(RequestData.Value['EE_HR_BENEFIT_REQ_BENEFICIARY_DATASET']) as IevDataset;

  sRefusalReasonDescription := '';

  try
    bEventBased := dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE'] <> null;
  except
    bEventBased := False; // for compatibility with old requests without LAST_QUAL_BENEFIT_EVENT_START_DATE field
  end;

  CheckCondition(dsMeta['request_status'] = 'S', 'Only submitted requests may be approved');
  CheckCOndition(dsBenefit['EE_NBR'] = ReqStorageTable['EE_NBR'], 'Requst has been created for different employee');
  bDeclined := dsBenefit['declined'];

  // changing status to approved
  dsMeta.Edit;
  dsMeta['request_status'] := 'A';
  dsMeta.Post;
  ReqStorageTable.Edit;
  try
    ReqStorageTable['DESCRIPTION'] := PackBenefitRequestDescription(newDescriptionData);
    tmpStream := TevDualStreamHolder.Create;
    RequestData.WriteToStream(tmpStream);
    tmpStream.Position := 0;
    BlobField := TBlobField(ReqStorageTable.FieldByName('REQUEST_DATA'));
    BlobField.LoadFromStream(tmpStream.realStream);
    ReqStorageTable.Post;
  except
    ReqStorageTable.Cancel;
    raise;
  end;

  DM_CLIENT.CL_PERSON.Close;
  DM_EMPLOYEE.EE.Close;
  DM_COMPANY.CO_BENEFIT_CATEGORY.Close;
  DM_COMPANY.CO_BENEFIT_ENROLLMENT.Close;
  DM_EMPLOYEE.EE_BENEFICIARY.Close;
  DM_CLIENT.CL_PERSON_DEPENDENTS.Close;
  DM_COMPANY.CO_BENEFIT_SETUP.Close;

  DM_EMPLOYEE.EE.DataRequired('EE_NBR=' + IntToStr(iEENbr));
  CheckCondition(DM_EMPLOYEE.EE.RecordCount = 1, 'Employee not found. EE_NR=' + IntToStr(iEENbr));
  DM_CLIENT.CL_PERSON.DataRequired('CL_PERSON_NBR=' + VarToStr(DM_EMPLOYEE.EE['CL_PERSON_NBR']));
  CheckCondition(DM_CLIENT.CL_PERSON.RecordCount = 1, 'Person not found. CL_PERSON_NR=' + VarToStr(DM_EMPLOYEE.EE['CL_PERSON_NBR']));

  bEmployeeDefined := false;
  bAddScheduledEDS := false;
  if not bDeclined then
  begin
    qCoBenefits := TevQuery.Create('select a.* from CO_BENEFITS a where {AsOfNow<a>} and a.CO_BENEFITS_NBR=' + VarToStr(dsBenefit['co_benefits_nbr']));
    qCoBenefits.Execute;
    CheckCondition(qCoBenefits.Result.RecordCount = 1, 'Benefit not found in CO_BENEFITS table. CO_BENEFITS_NBR=' + VarToStr(dsBenefit['co_benefits_nbr']));
    qCoBenefitSubtype := TevQuery.Create('select a.* from CO_BENEFIT_SUBTYPE a where {AsOfNow<a>} and a.CO_BENEFIT_SUBTYPE_NBR=' + VarToStr(dsBenefit['co_benefit_subtype_nbr']));
    qCoBenefitSubtype.Execute;
    CheckCondition(qCoBenefitSubtype.Result.RecordCount = 1, 'Record not found in CO_BENEFIT_SUBTYPE table. CO_BENEFIT_SUBTYPE_NBR=' + VarToStr(dsBenefit['co_benefit_subtype_nbr']));
    qCoBenefitRates := TevQuery.Create('select a.* from CO_BENEFIT_RATES a where {AsOfNow<a>} and a.CO_BENEFIT_RATES_NBR=' + VarToStr(dsBenefit['CO_BENEFIT_RATES_NBR']));
    CheckCondition(qCoBenefitRates.Result.RecordCount = 1, 'Record not found in CO_BENEFIT_RATES table. CO_BENEFIT_RATES_NBR=' + VarToStr(dsBenefit['CO_BENEFIT_RATES_NBR']));
    bEmployeeDefined := AnsiSameText(VarToStr(qCoBenefitSubtype.Result['DESCRIPTION']), sEmployeeDefinedSubtypeDescription) and
      (qCoBenefits.Result['ALLOW_EE_CONTRIBUTION'] = GROUP_BOX_YES);
    if bEmployeeDefined then
      CheckCondition(dsBenefit['ANNUAL_EE_CONTRIBUTION'] <> null, 'Employee contribution cannot be null');
  end;

  // you cannot use DM_COMPANY.CO_E_D_CODES due DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+EE_SCHEDULED_E_DSCO_NBR.AsString); in TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSCalcFields
  qCoEdCodes := TevQuery.Create('select a.* from CO_E_D_CODES a where {AsOfNow<a>} and CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
  qCoEdCodes.Execute;

  DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CATEGORY_TYPE=''' + VarToStr(dsBenefit['category_type']) +
    ''' and CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));
  CheckCondition(DM_COMPANY.CO_BENEFIT_CATEGORY.RecordCOunt = 1, 'Benefit Category not found in CO_BENEFIT_CATEGORY table. CATEGORY_TYPE=' + VarToStr(dsBenefit['category_type']));


  if bEventBased then
    DM_COMPANY.CO_BENEFIT_ENROLLMENT.DataRequired(
               'CO_BENEFIT_CATEGORY_NBR='+ DM_COMPANY.CO_BENEFIT_CATEGORY.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsString + ' and CATEGORY_COVERAGE_START_DATE <=' + QuotedStr(DateToStr(ReqStorageTable['STATUS_DATE'])) +
                               ' and CATEGORY_COVERAGE_END_DATE >=' + QuotedStr(DateToStr(ReqStorageTable['STATUS_DATE'])))
   else
    DM_COMPANY.CO_BENEFIT_ENROLLMENT.DataRequired(
               'CO_BENEFIT_CATEGORY_NBR='+ DM_COMPANY.CO_BENEFIT_CATEGORY.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsString + ' and ENROLLMENT_START_DATE <=' + QuotedStr(DateToStr(ReqStorageTable['STATUS_DATE'])) +
                               ' and ENROLLMENT_END_DATE >=' + QuotedStr(DateToStr(ReqStorageTable['STATUS_DATE'])));
  DM_COMPANY.CO_BENEFIT_ENROLLMENT.First;


  Tbl := GetClass('TEE_BENEFITS');
  CheckCondition(Assigned(Tbl), 'Cannot get class: TEE_BENEFITS');
  dsEEBenefits := TddTableClass(Tbl).Create(nil);  // you cannot use DM_EMPLOYEE.EE_BENEFITS due DM_EMPLOYEE.EE_BENEFITS.DataRequired('ALL'); in TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSCalcFields
  try
    dsEEBenefits.SkipFieldCheck := True;
    dsEEBenefits.ProviderName := 'EE_BENEFITS_PROV';
    dsEEBenefits.Active := false;
    dsEEBenefits.DataRequired('EE_NBR=' + IntToStr(iEENbr));

    DM_EMPLOYEE.EE_BENEFICIARY.DataRequired('ALL');
    DM_EMPLOYEE.EE_BENEFICIARY.IndexFieldNames:= 'EE_BENEFITS_NBR';

    DM_CLIENT.CL_PERSON_DEPENDENTS.DataRequired('ALL');
    qClEDs := TevQuery.Create('select a.* from CL_E_DS a where {AsOfNow<a>} ');
    qClEDs.Execute;
    DM_COMPANY.CO_BENEFIT_SETUP.DataRequired('CO_NBR=' + VarToStr(DM_EMPLOYEE.EE['CO_NBR']));

    // doing setup for email notifications after approval
    sEESummary := '';
    if DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_SUMMARY_TEXT'] <> null then
      sEESummary := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('EE_ENROLL_SUMMARY_TEXT')));
    sEEEnd := '';
    if DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_END_TEXT'] <> null then
      sEEEnd := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('EE_ENROLL_END_TEXT')));
    sBenefitEEEmailAddress := Trim(VarToStr(DM_EMPLOYEE.EE['BENEFIT_E_MAIL_ADDRESS']));
    if sBenefitEEEmailAddress = '' then
    begin
      sBenefitEEEmailAddress := Trim(VarToStr(DM_EMPLOYEE.EE['e_mail_address']));
      if sBenefitEEEmailAddress = '' then
        sBenefitEEEmailAddress := Trim(VarToStr(DM_CLIENT.CL_PERSON['e_mail_address']));
    end;

    try
      ctx_DataAccess.StartNestedTransaction([dbtClient]);

      // closing existing benefits in EE_BENEFITS
      dsEEBenefits.First;
      while not dsEEBenefits.Eof do
      begin
        // getting the category type
        Q := nil;
        if dsEEBenefits['CO_BENEFITS_NBR'] <> null then
        begin
          Q := TevQuery.Create('select CATEGORY_TYPE from CO_BENEFIT_CATEGORY a, CO_BENEFITS b ' +
            ' where b.CO_BENEFIT_CATEGORY_NBR is not null and a.CO_BENEFIT_CATEGORY_NBR = b.CO_BENEFIT_CATEGORY_NBR ' +
            ' and {AsOfNow<a>} and  {AsOfNow<b>} and b.CO_BENEFITS_NBR=:p1');
          Q.Params.AddValue('p1', dsEEBenefits['CO_BENEFITS_NBR']);
        end
        else if dsEEBenefits['CO_BENEFIT_SUBTYPE_NBR'] <> null then
        begin
          Q := TevQuery.Create('select CATEGORY_TYPE from CO_BENEFIT_CATEGORY a, CO_BENEFITS b, CO_BENEFIT_SUBTYPE c ' +
            ' where b.CO_BENEFIT_CATEGORY_NBR is not null and a.CO_BENEFIT_CATEGORY_NBR = b.CO_BENEFIT_CATEGORY_NBR ' +
            ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
            ' b.CO_BENEFITS_NBR=c.CO_BENEFITS_NBR and c.CO_BENEFIT_SUBTYPE_NBR=:p1');
          Q.Params.AddValue('p1', dsEEBenefits['CO_BENEFIT_SUBTYPE_NBR']);
        end;

       if Assigned(Q) then
        begin
          Q.Execute;
          if (Q.Result.RecordCount > 0) and (Q.Result['CATEGORY_TYPE'] = DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE']) then
          begin
            if Assigned(qCoBenefitRates) then
            begin

              if bEventBased then
                tmpDate:= dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE']-1
              else
                tmpDate:= qCoBenefitRates.Result['PERIOD_BEGIN'] - 1;

              if ((dsEEBenefits['EXPIRATION_DATE'] = null) or
                 ((dsEEBenefits['EXPIRATION_DATE'] >= qCoBenefitRates.Result['PERIOD_BEGIN']) and
                  (dsEEBenefits['EXPIRATION_DATE'] > tmpDate)) ) then
              begin

                dsEEBenefits.Edit;
                try

                  dsEEBenefits['EXPIRATION_DATE'] := tmpDate;
                  dsEEBenefits.Post;

                  DM_EMPLOYEE.EE_BENEFICIARY.SetRange([dsEEBenefits['EE_BENEFITS_NBR']], [dsEEBenefits['EE_BENEFITS_NBR']]);
                  try
                    DM_EMPLOYEE.EE_BENEFICIARY.First;
                    while not DM_EMPLOYEE.EE_BENEFICIARY.Eof do
                    begin
                      if not DM_EMPLOYEE.EE_BENEFICIARY.FieldByName('ACA_DEP_PLAN_BEGIN_DATE').IsNull then
                      begin
                        DM_EMPLOYEE.EE_BENEFICIARY.Edit;
                        DM_EMPLOYEE.EE_BENEFICIARY.FieldByName('ACA_DEP_PLAN_END_DATE').AsDateTime:= dsEEBenefits['EXPIRATION_DATE'];
                        DM_EMPLOYEE.EE_BENEFICIARY.Post;
                      end;
                      DM_EMPLOYEE.EE_BENEFICIARY.Next;
                    end;
                  finally
                    DM_EMPLOYEE.EE_BENEFICIARY.CancelRange;
                  end;

                except
                  dsEEBenefits.Cancel;
                  raise;
                end;

              end;
            end
            else // Declined case
            begin
              if dsEEBenefits['CO_BENEFIT_SUBTYPE_NBR'] <> null then
              begin
                Q1 := TevQuery.Create('select Period_begin from co_benefit_rates'#13 +
                                      'where {AsOfNow<co_benefit_rates>} and CO_BENEFIT_SUBTYPE_NBR = :p1'#13 +
                                      'and extractYear(Period_begin) = extractYear(''Now'') + 1');

                Q1.Params.AddValue('p1', dsEEBenefits['CO_BENEFIT_SUBTYPE_NBR']);
                if (dsEEBenefits['EXPIRATION_DATE'] = null) or
                   ((Q1.Result.RecordCount > 0) and (Q1.Result['PERIOD_BEGIN'] <> null) and
                    (dsEEBenefits['EXPIRATION_DATE'] >= Q1.Result['PERIOD_BEGIN']))
                then begin
                  dsEEBenefits.Edit;
                  try
                    dsEEBenefits['EXPIRATION_DATE'] := Q1.Result['PERIOD_BEGIN']- 1;
                    dsEEBenefits.Post;
                  except
                    dsEEBenefits.Cancel;
                    raise;
                  end;
                end;
              end;
            end;
          end;
        end;

        dsEEBenefits.Next;
      end;  // while

      if not bDeclined then
      begin
        // changing EE table
        if VarToStr(dsBenefit['EXISTING_PATIENT']) <> '' then
        begin
          DM_EMPLOYEE.EE.Edit;
          try
            DM_EMPLOYEE.EE['EXISTING_PATIENT'] := dsBenefit['EXISTING_PATIENT'];
            DM_EMPLOYEE.EE.Post;
          except
            DM_EMPLOYEE.EE.Cancel;
            raise;
          end;
        end;
        if qCoBenefits.Result['REQUIRES_PCP'] = GROUP_BOX_YES then
        begin
          DM_EMPLOYEE.EE.Edit;
          try
            DM_EMPLOYEE.EE['PCP'] := dsBenefit['PCP'];
            DM_EMPLOYEE.EE.Post;
          except
            DM_EMPLOYEE.EE.Cancel;
            raise;
          end;
        end;

        // adding new records to EE_BENEFITS and EE_BENFICIARY
        dsEEBenefits.Append;
        try
          dsEEBenefits['EE_NBR'] := iEENbr;
          dsEEBenefits['CO_BENEFITS_NBR'] := dsBenefit['co_benefits_nbr'];

          dsEEBenefits['STATUS'] := 'C';
          dsEEBenefits['BENEFIT_ENROLLMENT_COMPLETE'] := Mainboard.TimeSource.GetDateTime;
          dsEEBenefits['CO_BENEFIT_ENROLLMENT_NBR'] := DM_COMPANY.CO_BENEFIT_ENROLLMENT['CO_BENEFIT_ENROLLMENT_NBR'];

          if bEmployeeDefined then
          begin
            dsEEBenefits['DEDUCTION_FREQUENCY'] := SCHED_FREQ_ANNUALLY;
            dsEEBenefits['EE_RATE'] := dsBenefit['ANNUAL_EE_CONTRIBUTION'];

            if qCoBenefits.Result['ER_DEDUCTION_NBR'] <> null then
            begin
              if not qCoEdCodes.result.Locate('CO_E_D_CODES_NBR', qCoBenefits.Result['ER_DEDUCTION_NBR'], []) then
                raise Exception.Create('Cannot find record in CO_E_D_CODES_NBR table. CO_E_D_CODES_NBR=' + IntToStr(qCoBenefits.Result['ER_DEDUCTION_NBR']));
              if not qClEDs.Result.Locate('CL_E_DS_NBR', qCoEdCodes.result['CL_E_DS_NBR'], []) then
                raise Exception.Create('Cannot find record in CL_E_DS table. CL_E_DS_NBR=' + VarToStr(qCoEdCodes.result['CL_E_DS_NBR']));
              dsEEBenefits['ER_RATE'] := qClEDs.Result['ANNUAL_MAXIMUM'];
            end;
          end
          else
            dsEEBenefits['DEDUCTION_FREQUENCY'] := qCoBenefits.Result['FREQUENCY'];

          if bEventBased then
            dsEEBenefits['BENEFIT_EFFECTIVE_DATE'] := dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE']
          else
            dsEEBenefits['BENEFIT_EFFECTIVE_DATE'] := qCoBenefitRates.Result['PERIOD_BEGIN'];

          dsEEBenefits['BENEFIT_TYPE'] := qCoBenefits.Result['BENEFIT_TYPE'];
          dsEEBenefits['CO_BENEFIT_SUBTYPE_NBR'] := qCoBenefitSubtype.Result['CO_BENEFIT_SUBTYPE_NBR'];
          dsEEBenefits['COBRA_TERMINATION_EVENT'] :=   COBRA_TERMINATION_EVENT_NA;
          dsEEBenefits['ADDITIONAL_INFO1'] := dsBenefit['ADDITIONAL_INFO1'];
          dsEEBenefits['ADDITIONAL_INFO2'] := dsBenefit['ADDITIONAL_INFO2'];
          dsEEBenefits['ADDITIONAL_INFO3'] := dsBenefit['ADDITIONAL_INFO3'];
          dsEEBenefits['EXPIRATION_DATE'] := qCoBenefitRates.Result['PERIOD_END'];
          dsEEBenefits['RATE_TYPE'] := BENEFIT_RATES_RATE_TYPE_AMOUNT;
          dsEEBenefits.Post;
        except
          dsEEBenefits.Cancel;
          raise;
        end;

        dsDependent.First;
        while not dsDependent.Eof do
        begin
          bFound := DM_CLIENT.CL_PERSON_DEPENDENTS.Locate('CL_PERSON_DEPENDENTS_NBR', dsDependent['cl_person_dependents_nbr'], []);
          CheckCondition(bFound, 'Dependent not found in CL_PERSON_DEPENDENTS table. CL_PERSON_DEPENDENTS_NBR=' + VarToStr(dsDependent['cl_person_dependents_nbr']));
          DM_EMPLOYEE.EE_BENEFICIARY.Append;
          try
            DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'] := dsEEBenefits['EE_BENEFITS_NBR'];
            DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'] := dsDependent['cl_person_dependents_nbr'];
            DM_EMPLOYEE.EE_BENEFICIARY['PRIMARY_BENEFICIARY'] := GROUP_BOX_NO;
            DM_EMPLOYEE.EE_BENEFICIARY['ACA_DEP_PLAN_BEGIN_DATE'] := dsEEBenefits['BENEFIT_EFFECTIVE_DATE'];
            DM_EMPLOYEE.EE_BENEFICIARY['ACA_DEP_PLAN_END_DATE'] := dsEEBenefits['EXPIRATION_DATE'];

            DM_EMPLOYEE.EE_BENEFICIARY.Post;
          except
            DM_EMPLOYEE.EE_BENEFICIARY.Cancel;
            raise;
          end;

          dsDependent.Next;
        end;

        dsBenef.First;
        while not dsBenef.Eof do
        begin
          bFound := DM_CLIENT.CL_PERSON_DEPENDENTS.Locate('CL_PERSON_DEPENDENTS_NBR', dsBenef['cl_person_dependents_nbr'], []);
          CheckCondition(bFound, 'Dependent not found in CL_PERSON_DEPENDENTS table. CL_PERSON_DEPENDENTS_NBR=' + VarToStr(dsBenef['cl_person_dependents_nbr']));
          DM_EMPLOYEE.EE_BENEFICIARY.Append;
          try
            DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'] := dsEEBenefits['EE_BENEFITS_NBR'];
            DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'] := dsBenef['cl_person_dependents_nbr'];
            DM_EMPLOYEE.EE_BENEFICIARY['PRIMARY_BENEFICIARY'] := dsBenef['primary_beneficiary'];
            DM_EMPLOYEE.EE_BENEFICIARY['PERCENTAGE'] := dsBenef['percentage'];
            DM_EMPLOYEE.EE_BENEFICIARY.Post;
          except
            DM_EMPLOYEE.EE_BENEFICIARY.Cancel;
            raise;
          end;

          dsBenef.Next;
        end;

        // looking for existing SCHEDULED EDs for the same category and closing them
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Close;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('EE_NBR=' + IntToStr(iEENbr));
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.First;
        while not DM_EMPLOYEE.EE_SCHEDULED_E_DS.eof do
        begin
          qClEDs.Result.Locate('CL_E_DS_NBR', DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_DS_NBR'], []);
          if not AnsiContainsText(VarToStr(qClEDs.Result['DESCRIPTION']), 'COBRA') then
          begin
            if DM_EMPLOYEE.EE_SCHEDULED_E_DS['CO_BENEFIT_SUBTYPE_NBR'] <> null then
            begin
              Q := TevQuery.Create('select CATEGORY_TYPE from CO_BENEFIT_CATEGORY a, CO_BENEFITS b, CO_BENEFIT_SUBTYPE c ' +
                ' where b.CO_BENEFIT_CATEGORY_NBR is not null and a.CO_BENEFIT_CATEGORY_NBR = b.CO_BENEFIT_CATEGORY_NBR ' +
                ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                ' b.CO_BENEFITS_NBR=c.CO_BENEFITS_NBR and c.CO_BENEFIT_SUBTYPE_NBR=:p1');
              Q.Params.AddValue('p1', DM_EMPLOYEE.EE_SCHEDULED_E_DS['CO_BENEFIT_SUBTYPE_NBR']);
              Q.Execute;
              if (Q.Result['CATEGORY_TYPE'] = DM_COMPANY.CO_BENEFIT_CATEGORY['CATEGORY_TYPE']) and
               ((DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] = null) or
                ((not bEventBased and (DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] >= qCoBenefitRates.Result['PERIOD_BEGIN'])) or
                 (bEventBased and (DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] >= dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE'])) )
               ) then
              begin
                DM_EMPLOYEE.EE_SCHEDULED_E_DS.Edit;
                if bEventBased then
                  DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] := dsBenefit['LAST_QUAL_BENEFIT_EVENT_START_DATE']-1
                else
                  DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] := qCoBenefitRates.Result['PERIOD_BEGIN'] - 1;
                DM_EMPLOYEE.EE_SCHEDULED_E_DS.Post;
              end;
            end;
          end;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.Next;
        end;

        bAddScheduledEDS:= True;

      end
      else
      begin

        StoreBenefitRefusalReasonForEE(iEENbr, DM_COMPANY.CO_BENEFIT_ENROLLMENT['CO_BENEFIT_ENROLLMENT_NBR'], dsBenefit['sy_hr_refusal_reason_nbr'],
           DM_EMPLOYEE.EE['HOME_TAX_EE_STATES_NBR'], DM_COMPANY.CO_BENEFIT_ENROLLMENT.ENROLLMENT_END_DATE.AsDateTime + 1);  // declined
        Q := TevQuery.Create('select DESCRIPTION from SY_HR_REFUSAL_REASON a where {AsOfNow<a>} and a.SY_HR_REFUSAL_REASON_NBR=:p1');
        Q.Params.AddValue('p1', dsBenefit['sy_hr_refusal_reason_nbr']);
        Q.Execute;
        CheckCondition(Q.Result.RecordCount = 1, 'Cannot find unique record for refusal reason. SY_HR_REFUSAL_REASON=' + dsBenefit.FieldByName('sy_hr_refusal_reason_nbr').AsString);
        sRefusalReasonDescription := Q.Result['DESCRIPTION'];
      end;

      DM_EMPLOYEE.EE.Edit;
      try
        DM_EMPLOYEE.EE['BENEFIT_ENROLLMENT_COMPLETE'] := Mainboard.TimeSource.GetDateTime;
        DM_EMPLOYEE.EE.post;
      except
        DM_EMPLOYEE.EE.Cancel;
        raise;
      end;

      ctx_DataAccess.PostDataSets([ReqStorageTable, DM_EMPLOYEE.EE, dsEEBenefits, DM_EMPLOYEE.EE_BENEFICIARY,
        DM_EMPLOYEE.EE_SCHEDULED_E_DS]);

      ctx_DataAccess.CommitNestedTransaction;

      if bAddScheduledEDS then
      begin
        try
          ctx_DataAccess.StartNestedTransaction([dbtClient]);

                // adding new Scheduled EDs
          if qCoBenefits.Result['EE_DEDUCTION_NBR'] <> null then
            AddSchedED(qCoBenefits.Result['EE_DEDUCTION_NBR'], tEEFlag);
          if qCoBenefits.Result['ER_DEDUCTION_NBR'] <> null then
            AddSchedED(qCoBenefits.Result['ER_DEDUCTION_NBR'], tERFlag);

          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
          ctx_DataAccess.CommitNestedTransaction;
        except
          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      end;


      // emailing employee
      if not bDeclined then
      begin
        if (sBenefitEEEmailAddress <> '') and (sEESummary <> '') then
          try
            EEMailMessage := TevMailMessage.Create;
            EEMailMessage.AddressFrom := cESSFrom;
            EEMailMessage.AddressTo := sBenefitEEEmailAddress;
            EEMailMessage.Subject := VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_SUMMARY_SUBJECT']) +
              '. Category: ' + VarToStr(DM_COMPANY.CO_BENEFIT_CATEGORY['NAME']);
            EEMailMessage.Body := sEESummary;
            ReportData := TisStream.Create;
            if SignatureStorageTable['DOCUMENT'] <> null then
            begin
              ReportData := SignatureStorageTable.getBlobData('DOCUMENT');
              if ReportData.Size > 0 then
              begin
                ReportData.Position := 0;
                EEMailMessage.AttachStream('Summary.pdf', ReportData);
              end;
            end;
            ctx_EMailer.SendMail(EEMailMessage);
          except
            on E: Exception do
              GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email about approved benefit enrollment to employee. EE#' + DM_EMPLOYEE.EE['CUSTOM_EMPLOYEE_NUMBER'], E.Message + #13#10 + GetErrorCallStack(E));
          end;
      end
      else
      begin
        if (sBenefitEEEmailAddress <> '') and (sEEEnd <> '') then
          try
            sRequestInfo := '   Benefit Enrollment Information' + #13#10;
            sRequestInfo := sRequestInfo + '--------------------------------------------' + #13#10;
            sRequestInfo := sRequestInfo + 'Category: ' + VarToStr(DM_COMPANY.CO_BENEFIT_CATEGORY['NAME']) + #13#10;
            sRequestInfo := sRequestInfo + 'Refusal Reason: ' + sRefusalReasonDescription;
            ctx_EMailer.SendQuickMail(sBenefitEEEmailAddress, cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_END_SUBJECT']) + '. Category: ' + VarToStr(DM_COMPANY.CO_BENEFIT_CATEGORY['NAME']),
              sEEEnd + #13#10#13#10 + sRequestInfo);
          except
            on E: Exception do
              GlobalLogFile.AddEvent(etError, 'Misc Calc', 'Cannot send email about declined benefit enrollment to employee. EE#' + DM_EMPLOYEE.EE['CUSTOM_EMPLOYEE_NUMBER'], E.Message + #13#10 + GetErrorCallStack(E));
          end;
      end;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;

    Sleep(1000);

    try
      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      ReqStorageTable.DataRequired('REQUEST_TYPE=''' + EEChangeRequestTypeToString(tpBenefitRequest) +
        ''' and EE_CHANGE_REQUEST_NBR=' + IntToStr(ARequestNbr));
      CheckCondition(ReqStorageTable.RecordCount > 0, 'Request not found. EE_CHANGE_REQUEST_NBR=' + IntTOStr(ARequestNbr));
      try
        ReqStorageTable.Delete;
      except
        ReqStorageTable.Cancel;
        raise;
      end;
      ctx_DataAccess.PostDataSets([ReqStorageTable]);
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  finally
    FreeAndNil(dsEEBenefits);
  end;
end;

function TevRemoteMiscRoutines.GetBenefitEmailNotificationsList: IisListofValues;
var
  sClientId: String;
  HRList: IisParamsCollection;
  dNow: TDateTime;
  fDifference: Double;
  EEMailMessage: IevMailMessage;
  bHREnrollmentStart: boolean;
  bHREnrollmentEnd: boolean;
  bHRXDaysNotification: boolean;
  qEE, qCategory, qCo: IevQuery;
  AvaliableBenefits: IisListOfValues;
  BenefitsDataset: IevDataset;
  bBenefitSetupFound: boolean;
  sHREnrollBegin, sHRReminder: String;
  sEEEvent, sEEReminder, sEEEnrollBegin: String;

  procedure SendEmailToAllHr(const AHRList: IisParamsCollection; const AFrom: String; const ASubject: String;
    const AText: String; const AEmailsList: IisListOfValues; const ADescription: String);
  var
    i: integer;
    OneHRInfo: IisListOfValues;
    sEMail: String;
    HRMailMessage: IevMailMessage;
  begin
    for i := 0 to AHRList.Count - 1 do
    begin
      OneHRInfo := AHRList[i];
      sEMail := Trim(VarToStr(OneHRInfo.Value['EMAIL_ADDRESS']));
      if sEMail <> '' then
      begin
        HRMailMessage := TevMailMessage.Create;
        AEmailsList.AddValue(sClientId + ',' + IntToStr(AEmailsList.Count) + ',' + ADescription, HRMailMessage);
        HRMailMessage.AddressFrom := AFrom;
        HRMailMessage.AddressTo := sEMail;
        HRMailMessage.Subject := ASubject;
        HRMailMessage.Body := AText;
      end;
    end;
  end;

  procedure AddHrAsCC(const AHRList: IisParamsCollection; const AEMail: IevMailMessage);
  var
    i: integer;
    OneHRInfo: IisListOfValues;
    sEMail: String;
  begin
    AEMail.AddressCC := '';
    for i := 0 to AHRList.Count - 1 do
    begin
      OneHRInfo := AHRList[i];
      sEMail := Trim(VarToStr(OneHRInfo.Value['EMAIL_ADDRESS']));
      if sEMail <> '' then
        if AEMail.AddressCC <> '' then
          AEMail.AddressCC := AEMail.AddressCC + ';' + sEMail
        else
          AEMail.AddressCC := sEMail;
    end;
  end;

  function SendEMailToCurrentEE(const AFrom: String; const ASubject: String; const AText: String): IevMailMessage;
  var
    EMail1, EMail2, EMail3: String;
    sTo: String;
  begin
    Result := nil;

    EMail1 := Trim(VarToStr(qEE.Result['BENEFIT_E_MAIL_ADDRESS']));
    EMail2 := Trim(VarToStr(qEE.Result['e_mail_address']));
    EMail3 := Trim(VarToStr(qEE.Result['person_email']));
    if EMail1 <> '' then
      sTo := EMail1
    else if EMail2 <> '' then
      sTo := EMail2
    else
      sTo := EMail3;

    if Trim(sTo) <> '' then
    begin
      Result := TevMailMessage.Create;
      Result.AddressFrom := AFrom;
      Result.AddressTo := sTo;
      Result.Subject := ASubject;
      Result.Body := AText;
    end;
  end;

begin
  // this function works for current client, it's supposed to be called on DAILY basis
  // returns list of IevMailMessage, each Name has such structure: ClientId, RowNum in the list, Description
  // Description could looks like: 'to HR before start', 'to EE for event. EE# "XXXXX", etc.
  // using queries in order to be able to call GetEeBenefitPackageAvaliableForEnrollment, if needed
  Result := TisListOfValues.Create;
  if not Context.License.Benefits then
    exit;

  sClientId := IntToStr(ctx_DataAccess.ClientId);
  HRList := GetHRPersonsForOpenedClient;
  dNow := Mainboard.TimeSource.GetDateTime;

  DM_COMPANY.CO_BENEFIT_SETUP.Close;
  DM_COMPANY.CO_BENEFIT_SETUP.DataRequired('ALL');

  qCo := TevQuery.Create('select CO_NBR, ENABLE_HR, CUSTOM_COMPANY_NUMBER from CO a where {AsOfNow<a>} order by a.CO_NBR');
  qCo.Execute;

  qEE := TevQuery.Create('select a.e_mail_address, EE_NBR, CO_NBR, LAST_QUAL_BENEFIT_EVENT, ' +
    ' LAST_QUAL_BENEFIT_EVENT_DATE, CUSTOM_EMPLOYEE_NUMBER, BENEFIT_E_MAIL_ADDRESS, BENEFITS_ENABLED, ' +
    ' b.e_mail_address as person_email, SELFSERVE_ENABLED ' +
    ' from EE a, CL_PERSON b where {AsOfNow<a>} and {AsOfNow<b>} and a.CL_PERSON_NBR=b.CL_PERSON_NBR');
  qEE.Execute;

  qCategory :=
    TevQuery.Create('select a.CO_NBR, a.CO_BENEFIT_CATEGORY_NBR, a.CATEGORY_TYPE, a.ENROLLMENT_FREQUENCY, a.ENROLLMENT_NOTIFY_EE_DAYS, ' +
                       ' a.ENROLLMENT_NOTIFY_HR_DAYS, a.ENROLLMENT_REMINDER_EE_DAYS, a.ENROLLMENT_REMINDER_HR_DAYS, a.NOTES, a.READ_ONLY, ' +
                       ' a.REMIND_EE_DAYS, a.REMIND_HR_DAYS, b.CO_BENEFIT_ENROLLMENT_NBR, b.ENROLLMENT_START_DATE, b.ENROLLMENT_END_DATE ' +
                     ' from CO_BENEFIT_CATEGORY a ' +
                     ' LEFT OUTER JOIN CO_BENEFIT_ENROLLMENT b on a.CO_BENEFIT_CATEGORY_NBR = b.CO_BENEFIT_CATEGORY_NBR ' +
                     ' where {AsOfNow<a>} and {AsOfNow<b>}');
  qCategory.Execute;

  bHREnrollmentStart := false;
  bHREnrollmentEnd := false;
  bHRXDaysNotification := false;

  qCo.Result.First;
  while not qCo.Result.Eof do
  begin
    bBenefitSetupFound := DM_COMPANY.CO_BENEFIT_SETUP.Locate('CO_NBR', qCategory.Result['CO_NBR'], []);
    if (qCo.Result['ENABLE_HR'] = GROUP_BOX_BENEFITS) and bBenefitSetupFound then
    begin
      // reading texts from blobs
      if DM_COMPANY.CO_BENEFIT_SETUP['HR_ENROLL_BEGIN_TEXT'] <> null then
        sHREnrollBegin := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('HR_ENROLL_BEGIN_TEXT')))
      else
        sHREnrollBegin := '';
      if DM_COMPANY.CO_BENEFIT_SETUP['HR_REMINDER_TEXT'] <> null then
        sHRReminder := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('HR_REMINDER_TEXT')))
      else
        sHRReminder := '';
      if DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_EVENT_TEXT'] <> null then
        sEEEvent := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('EE_ENROLL_EVENT_TEXT')))
      else
        sEEEvent := '';
      if DM_COMPANY.CO_BENEFIT_SETUP['EE_REMINDER_TEXT'] <> null then
        sEEReminder := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('EE_REMINDER_TEXT')))
      else
        sEEReminder := '';
      if DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_BEGIN_TEXT'] <> null then
        sEEEnrollBegin := BlobFieldToString(TBlobField(DM_COMPANY.CO_BENEFIT_SETUP.FieldByName('EE_ENROLL_BEGIN_TEXT')))
      else
        sEEEnrollBegin := '';

      ///////////////////// HR /////////////////////////////////////////////////////////////////////////
      qCategory.Result.First;
      while not qCategory.Result.Eof do
      begin
        if qCategory.Result['CO_NBR'] = qCo.Result['CO_NBR'] then
        begin
          // notifications to HR before beginning of enrollment period, sending once
          if (not bHREnrollmentStart) and (sHREnrollBegin <> '') and (qCategory.Result['ENROLLMENT_NOTIFY_HR_DAYS'] <> null) then
          begin
            fDifference := dNow - (qCategory.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime - qCategory.Result.FieldByName('ENROLLMENT_NOTIFY_HR_DAYS').AsInteger);
            if (fDifference <= 1) and (fDifference >= 0) then
            begin
              SendEmailToAllHr(HRList, cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['HR_ENROLL_BEGIN_SUBJECT']),
                sHREnrollBegin, Result, 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To HR before start');
              bHREnrollmentStart := True;
            end;
          end;

          // notifications to HR before end of enrollment period, sending once
          if (not bHREnrollmentEnd) and (sHRReminder <> '') and (qCategory.Result['ENROLLMENT_REMINDER_HR_DAYS'] <> null) then
          begin
            fDifference := dNow - (qCategory.Result.FieldByName('ENROLLMENT_END_DATE').AsDateTime - qCategory.Result.FieldByName('ENROLLMENT_REMINDER_HR_DAYS').AsInteger);
            if (Abs(fDifference) <= 1) and (fDifference >= 0) then
            begin
              SendEmailToAllHr(HRList, cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['HR_REMINDER_SUBJECT']),
                sHRReminder, Result, 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To HR before end');
              bHREnrollmentEnd := True;
            end;
          end;

          // reminders to HR each X days of enrollment
          if (not bHRXDaysNotification) and (sHRReminder <> '') then
          begin
            if (dNow >= qCategory.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime) and
              (dNow <= qCategory.Result.FieldByName('ENROLLMENT_END_DATE').AsDateTime) and
              (qCategory.Result['REMIND_HR_DAYS'] <> null) and (qCategory.Result['REMIND_HR_DAYS'] <> 0) and
              (Trunc(dNow - qCategory.Result['ENROLLMENT_START_DATE']) > 0) then
            begin
              if (Trunc(dNow - qCategory.Result['ENROLLMENT_START_DATE']) mod qCategory.Result['REMIND_HR_DAYS']) = 0 then
              begin
                SendEmailToAllHr(HRList, cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['HR_REMINDER_SUBJECT']),
                  sHRReminder, Result, 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To HR each ' + VarToStr(qCategory.Result['REMIND_HR_DAYS']) + ' days');
                bHRXDaysNotification := True;
              end;
            end;
          end;
        end; // if CO_NBR

        qCategory.Result.Next;
      end; // while qCategory

      ////////////////////// EE ///////////////////////////////////////////////////////////////////////////////
      qEE.Result.First;
      while not qEE.Result.Eof do
      begin
        if (qEE.Result['CO_NBR'] = qCo.Result['CO_NBR']) and
          (qEE.Result['BENEFITS_ENABLED'] = GROUP_BOX_FULL_ACCESS) and (qEE.Result['SELFSERVE_ENABLED'] <> GROUP_BOX_NO) then
        begin
          // qualifying events, sending once
          if sEEEvent <> '' then
          begin
            if (qEE.Result['LAST_QUAL_BENEFIT_EVENT'] <> null) and (qEE.Result['LAST_QUAL_BENEFIT_EVENT_DATE'] <> null)  and
              (dNow >= qEE.Result['LAST_QUAL_BENEFIT_EVENT_DATE']) and
              ((dNow - qEE.Result['LAST_QUAL_BENEFIT_EVENT_DATE']) <= 1) then
            begin
              if DM_COMPANY.CO_BENEFIT_SETUP.Locate('CO_NBR', qEE.Result['CO_NBR'], []) and
                (DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_EVENT_TEXT'] <> null) then
              begin
                EEMailMessage := SendEMailToCurrentEE(cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_EVENT_SUBJECT']), sEEEvent);
                if Assigned(EEMailMessage) then
                begin
                  Result.AddValue(sClientId + ',' + IntToStr(Result.Count) +
                    ',' + 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To EE for event. EE# "' + qEE.Result['CUSTOM_EMPLOYEE_NUMBER'] + '"', EEMailMessage);
                  AddHrAsCC(HRList, EEMailMessage); // hardcopy to all HR
                end;
              end;
            end;
          end;

          if sEEReminder <> '' then
          begin
            AvaliableBenefits := GetEeBenefitPackageAvaliableForEnrollment(qEE.Result['EE_NBR'], 0, '', True);
            BenefitsDataset := IInterface(AvaliableBenefits.Value['CO_BENEFITS']) as IevDataset;

            // to EE each X days
            BenefitsDataset.First;
            while not BenefitsDataset.Eof do
            begin
              if (dNow >= BenefitsDataset.FieldByName('ENROLLMENT_START_DATE').AsDateTime) and
                (dNow <= BenefitsDataset.FieldByName('ENROLLMENT_END_DATE').AsDateTime) and
                (BenefitsDataset['REMIND_EE_DAYS'] <> null) and (BenefitsDataset['REMIND_EE_DAYS'] <> 0) and
                (Trunc(dNow - BenefitsDataset['ENROLLMENT_START_DATE']) > 0) then
              begin
                if (Trunc(dNow - BenefitsDataset['ENROLLMENT_START_DATE']) mod BenefitsDataset['REMIND_EE_DAYS']) = 0 then
                begin
                  EEMailMessage := SendEMailToCurrentEE(cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_REMINDER_SUBJECT']), sEEReminder);
                  if Assigned(EEMailMessage) then
                  begin
                    Result.AddValue(sClientId + ',' + IntToStr(Result.Count) +
                      ',' + 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To EE for each ' + VarToStr(BenefitsDataset['REMIND_EE_DAYS']) + ' days. EE# "' + qEE.Result['CUSTOM_EMPLOYEE_NUMBER'] + '"', EEMailMessage);
                  end;
                  Break;
                end;
              end;  // if
              BenefitsDataset.Next;
            end; // while

            // to EE before end
            BenefitsDataset.First;
            while not BenefitsDataset.Eof do
            begin
              if BenefitsDataset['ENROLLMENT_REMINDER_EE_DAYS'] <> null then
              begin
                fDifference := dNow - (BenefitsDataset.FieldByName('ENROLLMENT_END_DATE').AsDateTime - BenefitsDataset.FieldByName('ENROLLMENT_REMINDER_EE_DAYS').AsInteger);
                if (Abs(fDifference) <= 1) and (fDifference >= 0) then
                begin
                  EEMailMessage := SendEMailToCurrentEE(cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_REMINDER_SUBJECT']), sEEReminder);
                  if Assigned(EEMailMessage) then
                  begin
                    Result.AddValue(sClientId + ',' + IntToStr(Result.Count) +
                      ',' + 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To EE before end. EE# "' + qEE.Result['CUSTOM_EMPLOYEE_NUMBER'] + '"', EEMailMessage);
                  end;
                  Break;
                end;
              end;
              BenefitsDataset.Next;
            end; // while
          end;  // if sEEReminder <> ''

          // to EE before enroll begin
          if sEEEnrollBegin <> '' then
          begin
            qCategory.Result.First;
            while not qCategory.Result.Eof do
            begin
              if (qCategory.Result['CO_NBR'] = qCo.Result['CO_NBR']) and (qCategory.Result['ENROLLMENT_NOTIFY_EE_DAYS'] <> null) then
              begin
                fDifference := dNow - (qCategory.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime - qCategory.Result.FieldByName('ENROLLMENT_NOTIFY_EE_DAYS').AsInteger);
                if (fDifference <= 1) and (fDifference >= 0) then
                begin
                  AvaliableBenefits := GetEeBenefitPackageAvaliableForEnrollment(qEE.Result['EE_NBR'], qCategory.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime, qCategory.Result['CATEGORY_TYPE'], True);
                  BenefitsDataset := IInterface(AvaliableBenefits.Value['CO_BENEFITS']) as IevDataset;
                  if BenefitsDataset.RecordCount > 0 then
                  begin
                    EEMailMessage := SendEMailToCurrentEE(cESSFrom, VarToStr(DM_COMPANY.CO_BENEFIT_SETUP['EE_ENROLL_BEGIN_SUBJECT']), sEEEnrollBegin);
                    if Assigned(EEMailMessage) then
                    begin
                      Result.AddValue(sClientId + ',' + IntToStr(Result.Count) +
                        ',' + 'CO#' + qCo.Result['CUSTOM_COMPANY_NUMBER'] + '. To EE before start. EE# "' + qEE.Result['CUSTOM_EMPLOYEE_NUMBER'] + '"', EEMailMessage);
                    end;
                  Break;
                  end;
                end;
              end;
              qCategory.Result.Next;
            end;  // while
          end;
        end;  // if (qEE.Result['CO_NBR'] = qCo.Result['CO_NBR'])...

        qEE.Result.Next;
      end;  // while qEE
    end;  // if Co.ENABLE_HR

    qCo.Result.Next;
  end;  // while qCo
end;

function TevRemoteMiscRoutines.BlobFieldToString(const AField: TBlobField): String;
var
  tmpStream: IisStream;
begin
  Result := '';
  if not AField.IsNull then
  begin
    if AField.DataSet is TevClientDataSet then
      tmpStream := TevClientDataSet(AField.DataSet).GetBlobData(AField.FieldName)
    else
    begin
      tmpStream := TisStream.Create;
      AField.SaveToStream(tmpStream.RealStream);
    end;

    if Assigned(tmpStream) and (tmpStream.Size > 0) then
    begin
      tmpStream.Position := 0;
      SetLength(Result, tmpStream.Size);
      tmpStream.ReadBuffer(Result[1], Length(Result));
    end;
  end;
end;


function TevRemoteMiscRoutines.GetSystemAccountNbr: Integer;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Result := ctx_DBAccess.GetChangedByNbr;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevRemoteMiscRoutines.PopulateCheck(CONBR, PRNBR, EENBR, PrCheckTempNBR, VoidCheckNBR : Integer;
                       CheckType : string; IsPaySalary, IsPayHours, IsRefreshSchedEds, IsSave: Boolean;
                       PaymentSerialCheckNBR :integer = 0; UpdateBalance: string = '') : IisStream;
var
  stream: IEvDualStream;
begin
  with DM_PAYROLL, DM_CLIENT, DM_COMPANY, DM_SYSTEM_FEDERAL, DM_SYSTEM_STATE, DM_EMPLOYEE do
  begin
    PR_SCHEDULED_E_DS.CheckDSCondition('PR_NBR=' + IntToStr(PRNBR));
    CO.CheckDSCondition('CO_NBR=' + IntToStr(CONBR));
    CO_E_D_CODES.CheckDSCondition('CO_NBR=' + IntToStr(CONBR));
    CO_STATES.CheckDSCondition('CO_NBR=' + IntToStr(CONBR));
    EE.CheckDSCondition('CO_NBR=' + IntToStr(CONBR));
    PR_CHECK.Close;
    PR_CHECK_LINES.Close;
    PR_CHECK_STATES.Close;
    PR_CHECK_SUI.Close;
    PR_CHECK_LOCALS.Close;
    PR.DataRequired('PR_NBR=' + IntToStr(PRNBR));
    PR_BATCH.DataRequired('PR_NBR=' + IntToStr(PRNBR));
    PR_CHECK.DataRequired(AlwaysFalseCond);
    PR_CHECK_LINES.DataRequired(AlwaysFalseCond);
    PR_CHECK_STATES.DataRequired(AlwaysFalseCond);
    PR_CHECK_SUI.DataRequired(AlwaysFalseCond);
    PR_CHECK_LOCALS.DataRequired(AlwaysFalseCond);
    ctx_DataAccess.OpenDataSets([PR_CHECK_LINE_LOCALS,
      PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
      EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES]);
    Assert(EE.Locate('EE_NBR', EENBR, []));

    if PR.RecordCount = 0 then
    begin
      PR.Append;
      PR.CHECK_DATE.Value := Date + 1;
      PR.RUN_NUMBER.Value := 1;
      PR.PAYROLL_TYPE.Value := CheckType;
    //  PR.SCHEDULED.Value := GROUP_BOX_NO;
      PR.CO_NBR.Value := CONBR;
      PR.Post;
    end;

    if PR_BATCH.RecordCount = 0 then
    begin
      PR_BATCH.Append;
      PR_BATCH.PR_NBR.Value := PR.PR_NBR.Value;
      with Context.PayrollCalculation do
      begin
        PR_BATCH.FREQUENCY.Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);//FREQUENCY_TYPE_WEEKLY;
        PR_BATCH.PERIOD_BEGIN_DATE.Value := GetNextPeriodBeginDate(PR_BATCH.FREQUENCY.AsString[1]);
        PR_BATCH.PERIOD_BEGIN_DATE_STATUS.Value := DATE_STATUS_NORMAL;
        PR_BATCH.PERIOD_END_DATE.Value := GetNextPeriodEndDate(PR_BATCH.FREQUENCY.AsString[1], PR_BATCH.PERIOD_BEGIN_DATE.Value);
        PR_BATCH.PERIOD_END_DATE_STATUS.Value := DATE_STATUS_NORMAL;
      end;
      PR_BATCH.Post;
    end;

    PR_CHECK.Append;
    PR_CHECK.EE_NBR.Value := EENBR;
    PR_CHECK.PR_NBR.Value := PR.PR_NBR.Value;
    PR_CHECK.PR_BATCH_NBR.Value := PR_BATCH.PR_BATCH_NBR.Value;
    PR_CHECK.CHECK_TYPE.Value := CheckType;

    if CheckType = CHECK_TYPE2_VOID then
      PR_CHECK.FILLER.Value := PutIntoFiller(PR_CHECK.FILLER.Value, IntToStr(VoidCheckNBR), 1, 8)
    else if ( CheckType = CHECK_TYPE2_MANUAL ) and (Length(UpdateBalance) > 0) then
      PR_CHECK.UPDATE_BALANCE.Value := UpdateBalance[1]
    else if ( CheckType = CHECK_TYPE2_3RD_PARTY ) then
      PR_CHECK.UPDATE_BALANCE.Value := GROUP_BOX_NO;

    // for CHECK_TYPE2_MANUAL or  CHECK_TYPE2_3RD_PARTY
    if PaymentSerialCheckNBR > 0 then
      PR_CHECK.PAYMENT_SERIAL_NUMBER.Value := PaymentSerialCheckNBR;

    PR_CHECK.TAX_FREQUENCY.Value := EE.PAY_FREQUENCY.Value;
    PR_CHECK.Post;
    Context.PayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK,
      PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
      PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
      EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES, False, IsPaySalary, IsPayHours, IsRefreshSchedEds, False, True);
    if PR_CHECK.State <> dsBrowse then
      PR_CHECK.Post;
    if PrCheckTempNBR <> 0 then
      with DM_PAYROLL, DM_COMPANY, DM_EMPLOYEE do
      begin
        ctx_DataAccess.OpenDataSets([CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_LOCALS]);
        Context.PayrollCalculation.CopyPRTemplateDetails(PrCheckTempNBR, PR_CHECK, PR_CHECK_STATES,
          PR_CHECK_LOCALS, CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS);
      end;
    PR_CHECK.RetrieveCondition := 'PR_CHECK_NBR=' + PR_CHECK.PR_CHECK_NBR.AsString;
    PR_CHECK_LINES.RetrieveCondition := 'PR_CHECK_NBR=' + PR_CHECK.PR_CHECK_NBR.AsString;

    Result := TisStream.Create;
    stream := PR_CHECK.GetDataStream(False, False);
    Result.WriteByte(Byte(ftDataSet));
    Result.WriteString('PR_CHECK');
    Result.WriteStream(stream);
    stream := PR_CHECK_LINES.GetDataStream(False, False);
    Result.WriteByte(Byte(ftDataSet));
    Result.WriteString('PR_CHECK_LINES');
    Result.WriteStream(stream);

    if IsSave then
      ctx_DataAccess.PostDataSets([PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS]);
  end;
end;


function TevRemoteMiscRoutines.CreateCheck(const Params: IisStream): IisStream;
var
  CheckType : string;
  CONBR, PRNBR, EENBR, PrCheckTempNBR, VoidCheckNBR : Integer;
  IsPaySalary, IsPayHours, IsRefreshSchedEds, IsSave: Boolean;
begin
  result := nil;

  Params.Position := 0;
  Params.ReadByte;
  CONBR := Params.ReadInteger; //CO_NBR
  Params.ReadByte;
  PRNBR := Params.ReadInteger; // PR_NBR
  Params.ReadByte;
  EENBR := Params.ReadInteger; // EE_NBR
  Params.ReadByte;
  PrCheckTempNBR := Params.ReadInteger; // CO_PR_CHECK_TEMPLATES_NBR
  Params.ReadByte;
  IsPaySalary := Params.ReadBoolean; // Pay Salary
  Params.ReadByte;
  IsPayHours := Params.ReadBoolean; // Pay Standard Hours
  Params.ReadByte;
  IsRefreshSchedEds := Params.ReadBoolean; // Refresh Scheduled EDs
  Params.ReadByte;
  CheckType := Params.ReadString; // Check Type
  Params.ReadByte;
  VoidCheckNBR := Params.ReadInteger; // check to void

  IsSave := false;
  if Params.RealStream.Position < Params.RealStream.Size then // for backward compatibility
  begin
    Params.ReadByte;
    IsSave := Params.ReadBoolean; // Post flag
  end;

  result := PopulateCheck(CONBR, PRNBR, EENBR, PrCheckTempNBR, VoidCheckNBR, CheckType, IsPaySalary, IsPayHours,
            IsRefreshSchedEds, IsSave);

end;

function TevRemoteMiscRoutines.CreateManualCheck(const Params: IisStream): IisStream;
var
  CheckType, UpdateBalance : string;
  CONBR, PRNBR, EENBR, PrCheckTempNBR, VoidCheckNBR, PaymentSerialCheckNBR : Integer;
  IsPaySalary, IsPayHours, IsRefreshSchedEds, IsSave : Boolean;
begin
  result := nil;

  Params.Position := 0;
  Params.ReadByte;
  CONBR := Params.ReadInteger; //CO_NBR
  Params.ReadByte;
  PRNBR := Params.ReadInteger; // PR_NBR
  Params.ReadByte;
  EENBR := Params.ReadInteger; // EE_NBR
  Params.ReadByte;
  PrCheckTempNBR := Params.ReadInteger; // CO_PR_CHECK_TEMPLATES_NBR
  Params.ReadByte;
  IsPaySalary := Params.ReadBoolean; // Pay Salary
  Params.ReadByte;
  IsPayHours := Params.ReadBoolean; // Pay Standard Hours
  Params.ReadByte;
  IsRefreshSchedEds := Params.ReadBoolean; // Refresh Scheduled EDs
  Params.ReadByte;
  CheckType := Params.ReadString; // Check Type
  Params.ReadByte;
  VoidCheckNBR := Params.ReadInteger; // check to void

  // for manual
  Params.ReadByte;
  PaymentSerialCheckNBR := Params.ReadInteger; // Manual Check Number
  Params.ReadByte;
  UpdateBalance := Params.ReadString; // Update Balance

  IsSave := false;
  if Params.RealStream.Position < Params.RealStream.Size then // for backward compatibility
  begin
    Params.ReadByte;
    IsSave := Params.ReadBoolean; // Post flag
  end;

  result := PopulateCheck(CONBR, PRNBR, EENBR, PrCheckTempNBR, VoidCheckNBR, CheckType, IsPaySalary, IsPayHours,
                          IsRefreshSchedEds, IsSave, PaymentSerialCheckNBR, UpdateBalance);
end;

function TevRemoteMiscRoutines.GetManualTaxesListForCheck(const APrCheckNbr: integer): IevDataset;
var
  qPrCheck, qCoStates: IevQuery;
  qEEStates, qCoSUI, qEELocals, qCoLocalTax, qSyLocals: IevQuery;
  RowNbr, aPRNbr: integer;
  bRoOasdiMedicare: boolean;
  sStateLookup, sSDILookup, sSUILookup, sLocalsLookup: String;
  bFound: boolean;
  qPrCheckStates, qPrCheckLocals, qPrCheckSUI : TevClientDataSet;

  procedure AddControl(
    LabelCaption: string;
    DataSetName: string;
    IDFieldName: string;
    ID: integer;
    AmountFieldName: string;
    Amount: Variant;
    const RO: Boolean = False);
  begin
    RowNbr := RowNbr + 1;

    Result.Insert;
    Result.FieldByName('Label').AsString := LabelCaption;
    Result.FieldByName('Amount').Value := Amount;
    Result.FieldByName('DataSetName').AsString := DataSetName;
    Result.FieldByName('IDFieldName').AsString := IDFieldName;
    Result.FieldByName('AmountFieldName').AsString := AmountFieldName;
    Result.FieldByName('ID').AsInteger := ID;
    Result.FieldByName('RowNbr').AsInteger := RowNbr;
    Result.FieldByName('RO').AsBoolean := RO;
    Result.Post;
  end;

  procedure AddField(LabelCaption: string;
    DatasetName: String;
    IDFieldName: string;
    aField: TField;
    const RO: Boolean = False);
  begin
    AddControl(
      LabelCaption,
      DatasetName,
      IDFieldName,
      aField.DataSet.FieldByName(IDFieldName).AsInteger,
      aField.FieldName,
      aField.Value,
      RO);
  end;
begin
  Result := TevDataset.Create;
  Result.vclDataSet.FieldDefs.Add('Label', ftString, 40, False);
  Result.vclDataSet.FieldDefs.Add('AmountFieldName', ftString, 40, False);
  Result.vclDataSet.FieldDefs.Add('RowNbr', ftInteger, 0, false);
  Result.vclDataSet.FieldDefs.Add('DataSetName', ftString, 60, False);
  Result.vclDataSet.FieldDefs.Add('IDFieldName', ftString, 40, False);
  Result.vclDataSet.FieldDefs.Add('ID', ftInteger, 0, False);
  Result.vclDataSet.FieldDefs.Add('Amount', ftFloat, 0, False);
  Result.vclDataSet.FieldDefs.Add('RO', ftBoolean, 0, False);
  Result.vclDataSet.Active := True;

  qPrCheck := TevQuery.Create('select * from PR_CHECK where {AsOfNow<PR_CHECK>} and PR_CHECK_NBR=:p_pr_check_nbr');
  qPrCheck.Params.AddValue('p_pr_check_nbr', APrChecknbr);
  qPrCheck.Execute;
  CheckCondition(qPrCheck.Result.RecordCount > 0, 'Cannot find record in PR_CHECK table. PR_CHECK_NBR=' + IntToStr(APrCheckNbr));
  CheckCondition(qPrCheck.Result.FieldByName('PR_NBR').AsInteger > 0, 'Cannot find PR_NBR in PR_CHECK table. PR_CHECK_NBR=' + IntToStr(APrCheckNbr));
  aPRNbr := qPrCheck.Result.FieldByName('PR_NBR').AsInteger;

  qPrCheckStates := ctx_DataAccess.GetDataSet(GetddTableClassByName('PR_CHECK_STATES'));
  qPrCheckStates.Active := false;
  qPrCheckStates.Filtered := false;
  qPrCheckStates.DataRequired(' PR_CHECK_NBR=' + IntToStr(APrChecknbr));


  qPrCheckLocals := ctx_DataAccess.GetDataSet(GetddTableClassByName('PR_CHECK_LOCALS'));
  qPrCheckLocals.Active := false;
  qPrCheckLocals.Filtered := false;
  qPrCheckLocals.DataRequired(' PR_CHECK_NBR=' + IntToStr(APrChecknbr));

  qPrCheckSUI := ctx_DataAccess.GetDataSet(GetddTableClassByName('PR_CHECK_SUI'));
  qPrCheckSUI.Active := false;
  qPrCheckSUI.Filtered := false;
  qPrCheckSUI.DataRequired(' PR_CHECK_NBR=' + IntToStr(APrChecknbr));


  qCoStates := TevQuery.Create('select * from CO_STATES where {AsOfNow<CO_STATES>}');
  qCoStates.Execute;

  qCoLocalTax := TevQuery.Create('select * from CO_LOCAL_TAX where {AsOfNow<CO_LOCAL_TAX>}');
  qCoLocalTax.Execute;

  qEEStates := TevQuery.Create('select * from EE_STATES where {AsOfNow<EE_STATES>} and EE_NBR=:p_ee_nbr');
  qEEStates.Params.AddValue('p_ee_nbr', qPrCheck.Result['EE_NBR']);
  qEEStates.Execute;

  qEELocals := TevQuery.Create('select * from EE_LOCALS where {AsOfNow<EE_LOCALS>} and EE_NBR=:p_ee_nbr');
  qEELocals.Params.AddValue('p_ee_nbr', qPrCheck.Result['EE_NBR']);
  qEELocals.Execute;

  qCoSUI := TevQuery.Create('select * from CO_SUI where {AsOfNow<CO_SUI>}');
  qCoSUI.Execute;

  qSyLocals := TevQuery.Create('select * from SY_LOCALS where {AsOfNow<SY_LOCALS>}');
  qSyLocals.Execute;

  RowNbr := 0;
  bRoOasdiMedicare := ctx_AccountRights.Functions.GetState('MANUAL_OASDI_MEDICARE') <> stEnabled;

  if (qPrCheck.Result.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) or
     (qPrCheck.Result.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_NONE) then
    AddField('Federal',
      'PR_CHECK',
      'PR_CHECK_NBR',
      qPrCheck.Result.FieldByName('OR_CHECK_FEDERAL_VALUE'));

  AddField('OASDI',
    'PR_CHECK',
    'PR_CHECK_NBR',
    qPrCheck.Result.FieldByName('OR_CHECK_OASDI'),
    bRoOasdiMedicare);

  AddField('Medicare',
    'PR_CHECK',
    'PR_CHECK_NBR',
    qPrCheck.Result.FieldByName('OR_CHECK_MEDICARE'),
    bRoOasdiMedicare);

  AddField('EIC',
    'PR_CHECK',
    'PR_CHECK_NBR',
    qPrCheck.Result.FieldByName('OR_CHECK_EIC'));

  AddField('Back-up Withholding',
    'PR_CHECK',
    'PR_CHECK_NBR',
    qPrCheck.Result.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING'));


  qEEStates.Result.First;
  // Add State
  while not qEEStates.Result.Eof do
  begin
    if not qPrCheckStates.Locate('EE_STATES_NBR', qEEStates.Result['EE_STATES_NBR'], []) then
    begin
      qPrCheckStates.Insert;
      qPrCheckStates['PR_NBR'] := aPRNbr;
      qPrCheckStates['PR_CHECK_NBR'] := APrChecknbr;
      qPrCheckStates['EE_STATES_NBR'] := qEEStates.Result['EE_STATES_NBR'];
      qPrCheckStates['TAX_AT_SUPPLEMENTAL_RATE'] := GROUP_BOX_NO;
      qPrCheckStates['EXCLUDE_STATE'] := GROUP_BOX_NO;
      qPrCheckStates['EXCLUDE_SDI'] := GROUP_BOX_NO;
      qPrCheckStates['EXCLUDE_SUI'] := GROUP_BOX_NO;
      qPrCheckStates['EXCLUDE_ADDITIONAL_STATE'] := GROUP_BOX_NO;
      qPrCheckStates['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
      qPrCheckStates.Post;
    end;

    qCoSUI.Result.First;
    while not qCoSUI.Result.Eof do
    begin
      if (qCoSUI.Result['CO_STATES_NBR'] =qEEStates.Result['SUI_APPLY_CO_STATES_NBR']) and
         not qPrCheckSUI.Locate('CO_SUI_NBR', qCoSUI.Result['CO_SUI_NBR'], []) then
      begin
        qPrCheckSUI.Insert;
        qPrCheckSUI['PR_NBR'] := aPRNbr;
        qPrCheckSUI['PR_CHECK_NBR'] := APrChecknbr;
        qPrCheckSUI['CO_SUI_NBR'] := qCoSUI.Result['CO_SUI_NBR'];
        qPrCheckSUI.Post;
      end;
      qCoSUI.Result.Next;
    end;

    qEEStates.Result.Next;
  end;

  ctx_DataAccess.PostDataSets([qPrCheckStates, qPrCheckSUI]);

  qEELocals.Result.First;
  // Add Local
  while not qEELocals.Result.Eof do
  begin
    if not qPrCheckLocals.Locate('EE_LOCALS_NBR', qEELocals.Result['EE_LOCALS_NBR'], []) then
    begin
      qPrCheckLocals.Insert;
      qPrCheckLocals['PR_NBR'] := aPRNbr;
      qPrCheckLocals['PR_CHECK_NBR'] := APrChecknbr;
      qPrCheckLocals['EE_LOCALS_NBR'] := qEELocals.Result['EE_LOCALS_NBR'];
      qPrCheckLocals['EXCLUDE_LOCAL'] := GROUP_BOX_NO;
      qPrCheckLocals.Post;
    end;

    qEELocals.Result.Next;
  end;

  ctx_DataAccess.PostDataSets([qPrCheckLocals]);

  qEEStates.Result.First;
  qEELocals.Result.First;
  qCoSUI.Result.First;

  qPrCheckStates.First;
  qPrCheckSUI.First;
  qPrCheckLocals.First;

  while not qPrCheckStates.EOF do
  begin
    bFound := qEEStates.Result.Locate('EE_STATES_NBR', qPrCheckStates['EE_STATES_NBR'], []);
    CheckCondition(bFound, 'State not found. EE_STATES_NBR=' + VarToStr(qPrCheckStates['EE_STATES_NBR']));

    if (qPrCheckStates.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) or
       (qPrCheckStates.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_NONE) then
    begin
      bFound := qCoStates.Result.Locate('CO_STATES_NBR', qEEStates.Result['CO_STATES_NBR'], []);
      CheckCondition(bFound, 'State not found. CO_STATES_NBR=' + VarToStr(qEEStates.Result['CO_STATES_NBR']));
      sStateLookup := qCoStates.Result.FieldByName('STATE').AsString;
      AddField('STATE - ' + sStateLookup,
        'PR_CHECK_STATES',
        'EE_STATES_NBR',
        qPrCheckStates.FieldByName('STATE_OVERRIDE_VALUE'));
    end;

    bFound := qCoStates.Result.Locate('CO_STATES_NBR', qEEStates.Result['SDI_APPLY_CO_STATES_NBR'], []);
    CheckCondition(bFound, 'State not found. CO_STATES_NBR=' + VarToStr(qEEStates.Result['SDI_APPLY_CO_STATES_NBR']));
    sSDILookup := qCoStates.Result.FieldByName('STATE').AsString;
    AddField('SDI - ' + sSDILookup,
      'PR_CHECK_STATES',
      'EE_STATES_NBR',
      qPrCheckStates.FieldByName('EE_SDI_OVERRIDE'));
    qPrCheckStates.Next;
  end;

  qPrCheckSUI.First;
  while not qPrCheckSUI.EOF do
  begin
    bFound := qCoSUI.Result.Locate('CO_SUI_NBR', qPrCheckSUI['CO_SUI_NBR'], []);
    CheckCondition(bFound, 'Cannot find SUI name. CO_SUI_NBR=' + VarToStr(qPrCheckSUI['CO_SUI_NBR']));
    sSUILookup := qCoSUI.Result.FieldByName('DESCRIPTION').AsString;
    AddField('SUI - ' + sSUILookup,
      'PR_CHECK_SUI',
      'CO_SUI_NBR',
      qPrCheckSUI.FieldByName('OVERRIDE_AMOUNT'));
    qPrCheckSUI.Next;
  end;

  qPrCheckLocals.First;
  while not qPrCheckLocals.EOF do
  begin
    bFound := qEELocals.Result.Locate('EE_LOCALS_NBR', qPrCheckLocals['EE_LOCALS_NBR'], []);
    CheckCondition(bFound, 'Cannot find LOCALS name. EE_LOCALS_NBR=' + VarToStr(qPrCheckLocals['EE_LOCALS_NBR']));
    bFound := qCoLocalTax.Result.Locate('CO_LOCAL_TAX_NBR', qEELocals.Result['CO_LOCAL_TAX_NBR'], []);
    CheckCondition(bFound, 'Cannot find LOCALS name. CO_LOCAL_TAX_NBR=' + VarToStr(qEELocals.Result['CO_LOCAL_TAX_NBR']));
    bFound := qSyLocals.Result.Locate('SY_LOCALS_NBR', qCoLocalTax.Result['SY_LOCALS_NBR'], []);
    CheckCondition(bFound, 'Cannot find LOCALS name. SY_LOCALS_NBR=' + VarToStr(qCoLocalTax.Result['SY_LOCALS_NBR']));
    sLocalsLookup := qSylocals.Result.FieldByName('NAME').AsString;
    AddField('Local - ' + sLocalsLookup,
      'PR_CHECK_LOCALS',
      'EE_LOCALS_NBR',
      qPrCheckLocals.FieldByName('OVERRIDE_AMOUNT'));
    qPrCheckLocals.Next;
  end;
end;


function TevRemoteMiscRoutines.GetEEScheduled_E_DS(const eeList: string): IevDataset;
var
  rate_value: Variant;
  rate_type: String;

  function getWebScheduledED: IevDataSet;
  var
    Q: IevQuery;
    s : String;
  begin
     s :='select  ee_scheduled_e_ds_nbr, e.ee_nbr, e.cl_e_ds_nbr, ' +
                   'calculation_type, amount, percentage, frequency, plan_type, e.which_checks, e.cl_e_d_groups_nbr, cl_agency_nbr, ' +
                   'effective_start_date, effective_end_date, priority_number, scheduled_e_d_groups_nbr, ' +
                   'ee_direct_deposit_nbr, deduct_whole_check, take_home_pay, ee_child_support_cases_nbr, garnisnment_id, ' +
                   'exclude_week_1, exclude_week_2, exclude_week_3, exclude_week_4, exclude_week_5, always_pay, deductions_to_zero, ' +
                   'SCHEDULED_E_D_ENABLED, ' +
                   'target_action, number_of_targets_remaining, target_amount, balance, ' +
                   'minimum_wage_multiplier, maximum_garnish_percent, annual_maximum_amount, c.annual_maximum, ' +
                   'min_ppp_cl_e_d_groups_nbr, minimum_pay_period_percentage, minimum_pay_period_amount, ' +
                   'max_ppp_cl_e_d_groups_nbr, maximum_pay_period_percentage, maximum_pay_period_amount, ' +
                   'threshold_e_d_groups_nbr, threshold_amount, use_pension_limit ,co_benefit_subtype_nbr, ee_benefits_nbr, ' +
                   'c.description, c.custom_e_d_code_number, c.e_d_code_type ' +
              'from ee_scheduled_e_ds e ' +
                  'left outer join cl_e_ds c on c.cl_e_ds_nbr = e.cl_e_ds_nbr and {AsOfNow<c>} ' +
              'where  ' + BuildINsqlStatement('e.ee_nbr', eeList) + ' and {AsOfNow<e>} ';
     Q := TevQuery.Create(s);
     Q.Execute;
     Result := Q.Result;
  end;

var
  Items: IisStringList;
  i : integer;
begin

    Result :=  getWebScheduledED;
    DM_EMPLOYEE.EE.DataRequired(BuildINsqlStatement('ee_nbr', eeList));
    Assert(DM_EMPLOYEE.EE.RecordCount <> 0, 'ClientID = ' + IntToStr(ctx_DataAccess.ClientID) + ', EE List = ' + eeList);

    Items := TisStringList.Create;
    Items.CommaText := eeList;

    Result.IndexFieldNames:= 'EE_NBR';
    try

     for i := 0 to Items.Count - 1 do
     begin

        Result.SetRange([StrToInt(Items[i])], [StrToInt(Items[i])]);
        Result.First;
        while not Result.Eof do
        begin
          if Result['SCHEDULED_E_D_ENABLED'] <> GROUP_BOX_NO then
          begin
            if not Result.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull then
               CalcEEScheduled_E_DS_Rate(
                              Result.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger,
                              Result.FieldByName('cl_e_ds_nbr').AsInteger,
                              Result.FieldByName('effective_start_date').AsDateTime,
                              Result.FieldByName('effective_end_date').AsDateTime,
                              rate_type, rate_value)
            else if not Result.FieldByName('EE_BENEFITS_NBR').IsNull then
               CalcEEScheduled_E_DS_Rate(
                              Result.FieldByName('ee_benefits_nbr').AsInteger,
                              Result.FieldByName('cl_e_ds_nbr').AsInteger,
                              Result.FieldByName('effective_start_date').AsDateTime,
                              Result.FieldByName('effective_end_date').AsDateTime,
                              Result.FieldByName('amount').Value,
                              Result.FieldByName('target_amount').Value,
                              Result.FieldByName('annual_maximum_amount').Value,
                              Result.FieldByName('frequency').AsString,
                              rate_type, rate_value)
            else
              rate_value := null;

            if not varisnull(rate_value) then
            begin
              result.Edit;
              if rate_type = BENEFIT_RATES_RATE_TYPE_PERCENT then
              begin
                 result['PERCENTAGE'] := rate_value;
                 result['AMOUNT'] := null;
              end
              else
              begin
                 result['AMOUNT'] := rate_value;
                 result['PERCENTAGE'] := null;
              end;
              result.Post;
            end;
          end;

          result.Next;
        end;

     end;
    finally
      Result.CancelRange;
    end;
end;


procedure TevRemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(
  const aCO_BENEFIT_SUBTYPE_NBR, aCL_E_DS_NBR: Integer; const aBeginDate,
  aEndDate: TDateTime; out aRate_Type: String; out aValue: Variant);
var
  Q: IevQuery;
begin
  Q := TevQuery.CreateFmt(
    '/*Cl_*/' +
    'EXECUTE BLOCK'#13 +
    'RETURNS (rate_value NUMERIC(18, 6), rate_type CHAR(1))'#13 +
    'AS'#13 +
    'DECLARE VARIABLE co_benefit_subtype_nbr INTEGER;'#13 +
    'DECLARE VARIABLE cl_e_ds_nbr INTEGER;'#13 +
    'DECLARE VARIABLE sed_begin_date DATE;'#13 +
    'DECLARE VARIABLE sed_end_date DATE;'#13 +
    'DECLARE VARIABLE ee_or_er_benefit CHAR;'#13 +
    'DECLARE VARIABLE rate_as_of_date DATE;'#13 +
    'DECLARE VARIABLE co_nbr INTEGER;'#13 +
    'DECLARE VARIABLE asofdate DATE;'#13 +
    'BEGIN'#13 +
    '  co_benefit_subtype_nbr = %d;'#13 +
    '  cl_e_ds_nbr = %d;'#13 +
    '  sed_begin_date = %s;'#13 +
    '  sed_end_date = %s;'#13 +
    '  asofdate = ''%s'';'#13 +
    ''#13 +
    '  IF (sed_begin_date IS NULL) THEN'#13 +
    '    rate_as_of_date = CURRENT_DATE;'#13 +
    '  ELSE'#13 +
    '  BEGIN'#13 +
    '    IF (sed_begin_date < CURRENT_DATE) THEN'#13 +
    '      rate_as_of_date = CURRENT_DATE;'#13 +
    '    ELSE'#13 +
    '      rate_as_of_date = sed_begin_date;'#13 +
    '  END'#13 +
    ''#13 +
    '  IF (sed_end_date IS NOT NULL AND sed_end_date < CURRENT_DATE) THEN'#13 +
    '    rate_as_of_date = sed_end_date;'#13 +
    ''#13 +
    '  SELECT b.co_nbr FROM co_benefits b, co_benefit_subtype s'#13 +
    '  WHERE'#13 +
    '    b.co_benefits_nbr = s.co_benefits_nbr AND'#13 +
    '    s.co_benefit_subtype_nbr = :co_benefit_subtype_nbr AND'#13 +
    '    {GenericAsOfDate<:asofdate, b>} AND'#13 +
    '    {GenericAsOfDate<:asofdate, s>} '#13 +
    '  INTO :co_nbr;'#13 +
    ''#13 +
    '  SELECT c.ee_or_er_benefit FROM co_e_d_codes c'#13 +
    '  WHERE'#13 +
    '    {GenericAsOfDate<:asofdate, c>} AND'#13 +
    '    c.cl_e_ds_nbr = :cl_e_ds_nbr AND'#13 +
    '    c.co_nbr = :co_nbr'#13 +
    '  INTO :ee_or_er_benefit;'#13 +
    ''#13 +
    '  SELECT r.rate_type,'#13 +
    '  CAST('#13 +
    '  CASE :ee_or_er_benefit'#13 +
    '    WHEN ''E'' THEN r.ee_rate'#13 +
    '    WHEN ''R'' THEN r.er_rate'#13 +
    '    ELSE NULL'#13 +
    '  END'#13 +
    '  AS NUMERIC(18, 6))'#13 +
    '  FROM co_benefit_rates r'#13 +
    '  WHERE'#13 +
    '    {GenericAsOfDate<:asofdate, r>} AND'#13 +
    '    r.co_benefit_subtype_nbr = :co_benefit_subtype_nbr AND'#13 +
    '    :rate_as_of_date BETWEEN r.period_begin AND r.period_end'#13 +
    '  INTO :rate_type, :rate_value;'#13 +
    ''#13 +
    '  SUSPEND;'#13 +
    'END',
      [aCO_BENEFIT_SUBTYPE_NBR,
       aCL_E_DS_NBR,
       Iff(aBeginDate = 0, 'NULL', '''' + DateToUTC(aBeginDate) + ''''),
       Iff(aEndDate = 0, 'NULL', '''' + DateToUTC(aEndDate) + ''''),
       DateToUTC(SysDate)]);
  Q.Execute;

  aValue := Q.Result.Fields[0].Value;
  aRate_Type := Q.Result.Fields[1].AsString;
end;

procedure TevRemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(
  const aEE_BENEFITS_NBR, aCL_E_DS_NBR: Integer;
  const aBeginDate, aEndDate: TDateTime;
  const aAMOUNT, aANNUAL_MAXIMUM_AMOUNT, aTARGET_AMOUNT: variant;
  const aFREQUENCY:string;
  out aRate_Type: String; out aValue: Variant);
var
  TempAmount, AnnualMaximum: Real;
  Q, Q1: IevQuery;
begin
  Q := TevQuery.CreateFmt(
      '/*Cl_*/' +
      'EXECUTE BLOCK'#13 +
      'RETURNS (rate_value NUMERIC(18, 6), rate_type CHAR(1))'#13 +
      'AS'#13 +
      'DECLARE VARIABLE ee_benefits_nbr INTEGER;'#13 +
      'DECLARE VARIABLE cl_e_ds_nbr INTEGER;'#13 +
      'DECLARE VARIABLE co_benefit_subtype_nbr INTEGER ;'#13 +
      'DECLARE VARIABLE sed_begin_date DATE;'#13 +
      'DECLARE VARIABLE sed_end_date DATE;'#13 +
      'DECLARE VARIABLE ben_begin_date DATE;'#13 +
      'DECLARE VARIABLE ben_end_date DATE;'#13 +
      'DECLARE VARIABLE ee_rate NUMERIC(18,6);'#13 +
      'DECLARE VARIABLE er_rate NUMERIC(18,6);'#13 +
      'DECLARE VARIABLE cobra_rate NUMERIC(18,6);'#13 +
      'DECLARE VARIABLE total_premium_amount NUMERIC(18,6);'#13 +
      ''#13 +
      'DECLARE VARIABLE ee_or_er_benefit CHAR;'#13 +
      'DECLARE VARIABLE rate_as_of_date DATE;'#13 +
      'DECLARE VARIABLE co_nbr INTEGER;'#13 +
      'DECLARE VARIABLE asofdate DATE;'#13 +
      'BEGIN'#13 +
      '  ee_benefits_nbr = %d;'#13 +
      '  cl_e_ds_nbr = %d;'#13 +
      '  sed_begin_date = %s;'#13 +
      '  sed_end_date = %s;'#13 +
      '  asofdate = ''%s'';'#13 +
      ''#13 +
      '  SELECT b.co_nbr FROM ee b, ee_benefits s'#13 +
      '  WHERE'#13 +
      '    b.ee_nbr = s.ee_nbr AND'#13 +
      '    s.ee_benefits_nbr = :ee_benefits_nbr AND'#13 +
      '    {GenericAsOfDate<:asofdate, b>} AND'#13 +
      '    {GenericAsOfDate<:asofdate, s>}'#13 +
      '  INTO :co_nbr;'#13 +
      ''#13 +
      '  SELECT c.ee_or_er_benefit FROM co_e_d_codes c'#13 +
      '  WHERE'#13 +
      '    {GenericAsOfDate<:asofdate, c>} AND'#13 +
      '    c.cl_e_ds_nbr = :cl_e_ds_nbr AND'#13 +
      '    c.co_nbr = :co_nbr'#13 +
      '  INTO :ee_or_er_benefit;'#13 +
      ''#13 +
      '  SELECT z.co_benefit_subtype_nbr, z.benefit_effective_date, z.expiration_date,'#13 +
      '       z.ee_rate, z.er_rate,z.cobra_rate, z.total_premium_amount, z.rate_type, '#13 +
      '  CAST('#13 +
      '  CASE :ee_or_er_benefit'#13 +
      '    WHEN ''E'' THEN z.ee_rate'#13 +
      '    WHEN ''R'' THEN z.er_rate'#13 +
      '    ELSE NULL'#13 +
      '  END'#13 +
      '  AS NUMERIC(18, 6))'#13 +
      '   FROM   ee_benefits z WHERE'#13 +
      '     {GenericAsOfDate<:asofdate, z>} AND z.ee_benefits_nbr = :ee_benefits_nbr'#13 +
      '  INTO :co_benefit_subtype_nbr, :ben_begin_date,'#13 +
      '       :ben_end_date,'#13 +
      '       :ee_rate, :er_rate, :cobra_rate, :total_premium_amount, rate_type, '#13 +
      '       :rate_value;'#13 +
      ''#13 +
      ''#13 +
      '   IF (ben_begin_date IS NULL) THEN'#13 +
      '     rate_as_of_date = CURRENT_DATE;'#13 +
      '   ELSE'#13 +
      '   BEGIN'#13 +
      '     IF (ben_begin_date < CURRENT_DATE) THEN'#13 +
      '      rate_as_of_date = CURRENT_DATE;'#13 +
      '     ELSE'#13 +
      '      rate_as_of_date = ben_begin_date;'#13 +
      '   END'#13 +
      '    '#13 +
      '   IF (ben_end_date IS NOT NULL AND ben_end_date < CURRENT_DATE) THEN'#13 +
      '      rate_as_of_date = ben_end_date;'#13 +
      ''#13 +
      '      IF ((co_benefit_subtype_nbr IS NOT NULL ) AND'#13 +
      '          (ee_rate IS NULL) AND (er_rate IS NULL) AND'#13 +
      '          (cobra_rate IS NULL) AND (total_premium_amount IS NULL)) THEN'#13 +
      '      BEGIN'#13 +
      '                SELECT r.rate_type,'#13 +
      '                  CAST(CASE :ee_or_er_benefit'#13 +
      '                        WHEN ''E'' THEN r.ee_rate'#13 +
      '                        WHEN ''R'' THEN r.er_rate'#13 +
      '                        ELSE NULL'#13 +
      '                       END AS NUMERIC(18, 6))'#13 +
      '                  FROM co_benefit_rates r'#13 +
      '                  WHERE'#13 +
      '                    {GenericAsOfDate<:asofdate, r>} AND'#13 +
      '                    r.co_benefit_subtype_nbr = :co_benefit_subtype_nbr AND'#13 +
      '                    :rate_as_of_date BETWEEN r.period_begin AND r.period_end'#13 +
      '                  INTO :rate_type, :rate_value;'#13 +
      '    '#13 +
      '      END'#13 +
      ''#13 +
      '  SUSPEND;'#13 +
      'END',
      [aEE_BENEFITS_NBR,
       aCL_E_DS_NBR,
       Iff(aBeginDate = 0, 'NULL', '''' + DateToUTC(aBeginDate) + ''''),
       Iff(aEndDate = 0, 'NULL', '''' + DateToUTC(aEndDate) + ''''),
       DateToUTC(SysDate)]);

  Q.Execute;

  aValue := Q.Result.Fields[0].Value;
  aRate_Type := Q.Result.Fields[1].AsString;

  if aValue = Null then
     aValue := aAMOUNT;

  if VarIsNull(aAMOUNT) then
  begin
        Q := TevQuery.Create('select a.CO_BENEFIT_SUBTYPE_NBR from EE_BENEFITS a, CO_BENEFITS b, CO_BENEFIT_SUBTYPE c'#13 +
                          ' where {AsOfNow<a>} and {AsOfNow<b>} and'#13 +
                          '        {AsOfNow<c>} and'#13 +
                          '        a.EE_BENEFITS_NBR = :ee_benefit_nbr and'#13 +
                          '        a.CO_BENEFIT_SUBTYPE_NBR = c.CO_BENEFIT_SUBTYPE_NBR and'#13 +
                          '        c.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and'#13 +
                          '        c.DESCRIPTION like  ''Employee Defined%'' and'#13 +
                          '        b.ALLOW_EE_CONTRIBUTION = ''Y''');

        Q.Params.AddValue('ee_benefit_nbr', aEE_BENEFITS_NBR);
        Q.Execute;
        if (Q.Result.RecordCount >0)  and (Q.Result.Fields[0].Value <> null ) then
        begin
          TempAmount := 0;

          Q1 := TevQuery.Create(
                        'select a.PAY_FREQUENCY from EE a, EE_BENEFITS b ' +
                        '  where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                        '         b.EE_BENEFITS_NBR = :ee_benefits_nbr and a.EE_NBR = b.EE_NBR');
          Q1.Params.AddValue('ee_benefits_nbr', aEE_BENEFITS_NBR);
          Q1.Execute;
          if (Q1.Result.RecordCount >0) and (not VarIsNull(aANNUAL_MAXIMUM_AMOUNT)) then
          begin
              AnnualMaximum := aANNUAL_MAXIMUM_AMOUNT;
              case aFREQUENCY[1] of
                SCHED_FREQ_RUN_ONCE, SCHED_FREQ_ONE_TIME: TempAmount := ConvertNull(aTARGET_AMOUNT, 0);
                SCHED_FREQ_DAILY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/260);
                      if AnnualMaximum > (TempAmount*260) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_SCHEDULED_PAY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/GetTaxFreq(Q1.Result.FieldByName('PAY_FREQUENCY').AsString));
                      if AnnualMaximum > (TempAmount*GetTaxFreq(Q1.Result.FieldByName('PAY_FREQUENCY').AsString)) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_FIRST_SCHED_PAY, // : TempAmount := ConvertNull(aTARGET_AMOUNT, 0);
                SCHED_FREQ_LAST_SCHED_PAY,
                SCHED_FREQ_MID_SCHED_PAY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/12);
                      if AnnualMaximum > (TempAmount*12) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_ANNUALLY: TempAmount := AnnualMaximum;
                SCHED_FREQ_SEMI_ANNUALLY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/2);
                      if AnnualMaximum > (TempAmount*2) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_QUARTERLY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/4);
                      if AnnualMaximum > (TempAmount*4) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_SCHEDULED_MONTHLY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/12);
                      if AnnualMaximum > (TempAmount*12) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_EVRY_OTHER_PAY:
                    begin
                      TempAmount := RoundTwo(AnnualMaximum/(GetTaxFreq(Q1.Result.FieldByName('PAY_FREQUENCY').AsString) div 2));
                      if AnnualMaximum > (TempAmount*(GetTaxFreq(Q1.Result.FieldByName('PAY_FREQUENCY').AsString) div 2)) then
                        TempAmount := TempAmount + 0.01;
                    end;
                SCHED_FREQ_USER_ENTERED: TempAmount := 0;
              end;
          end;
          if TempAmount <> 0 then
            aValue := TempAmount;
        end;
  end;
end;

function TevRemoteMiscRoutines.GetSysReportByNbr(
  const aReportNbr: integer): IisStream;
begin
  Result := TEvDualStreamHolder.CreateInMemory;
  try
    ctx_RWLocalEngine.GetReportResources(aReportNbr, CH_DATABASE_SYSTEM, Result);
  except
    on E: Exception do
    begin
      Result.Clear;
      raise EevException.CreateHelp('RemoteMiscRoutines.GetSysReportByNbr, GetReportResources' + #13 + E.Message, E.HelpContext);
    end;
  end;
  Result.Position := 0;
end;

function TevRemoteMiscRoutines.GetReportFileByNbrAndType(
  const aReportNbr: integer; const aReportType: string): IisStream;
begin
  Result := TEvDualStreamHolder.CreateInMemory;
  try
    ctx_RWLocalEngine.GetReportResources(aReportNbr, aReportType, Result);
  except
    on E: Exception do
    begin
      Result.Clear;
      raise EevException.CreateHelp('RemoteMiscRoutines.GetReportFileByNbrAndType, GetReportResources' + #13 + E.Message, E.HelpContext);
    end;
  end;
  Result.Position := 0;
end;

function TevRemoteMiscRoutines.DeleteEE(const AEENbr: Integer): boolean;
var
  pList: IisStringList;
  sFilter: String;
  Q: IevQuery;
  Proc: IevStoredProc;
  clPersonNbr: Integer;
begin
       Assert(AEENbr > 0, 'EE_NBR has to be bigger than zero.');
       DM_COMPANY.PR_CHECK.DataRequired('EE_NBR = ' + IntToStr(AEENbr));
       Assert(DM_COMPANY.PR_CHECK.RecordCount = 0, 'ClientID = ' + IntToStr(ctx_DataAccess.ClientID) + ', EE_NBR = ' + IntToStr(AEENbr) + #13 + 'Employee has payroll history.' + #13);

       ctx_DataAccess.StartNestedTransaction([dbtClient]);
       try
         ctx_DBAccess.ExecQuery('DELETE FROM CO_GROUP_MANAGER where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM CO_GROUP_MEMBER where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM CO_MANUAL_ACH where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM CO_TAX_RETURN_RUNS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);

         ctx_DBAccess.ExecQuery('DELETE FROM EE_ADDITIONAL_INFO where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_AUTOLABOR_DISTRIBUTION where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);

         ctx_DBAccess.ExecQuery('DELETE FROM EE_SCHEDULED_E_DS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_CHANGE_REQUEST where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_CHILD_SUPPORT_CASES where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_DIRECT_DEPOSIT where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_EMERGENCY_CONTACTS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_HR_ATTENDANCE where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_HR_CAR where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_HR_CO_PROVIDED_EDUCATN where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_HR_INJURY_OCCURRENCE where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_HR_PERFORMANCE_RATINGS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_HR_PROPERTY_TRACKING where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_LOCALS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_PENSION_FUND_SPLITS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_PIECE_WORK where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_RATES where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_SIGNATURE where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_SPREADREPORT where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_WORK_SHIFTS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_BENEFIT_PAYMENT where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_BENEFIT_REFUSAL where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);

         Q := TevQuery.Create('select * from EE_BENEFITS where EE_NBR = :eeNBR');
         Q.Params.AddValue('eeNBR', AEENbr);
         Q.Execute;

         if (Q.Result.RecordCount > 0) then
         begin
           pList:= Q.Result.GetColumnValues('EE_BENEFITS_NBR');
           sFilter := BuildINsqlStatement('EE_BENEFITS_NBR', pList.CommaText);
           ctx_DBAccess.ExecQuery('DELETE FROM EE_BENEFICIARY where ' + sFilter + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM EE_BENEFITS where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         end;

         ctx_DBAccess.ExecQuery('UPDATE EE set home_tax_ee_states_nbr= null where EE_NBR  = ' + IntToStr(AEENbr), nil, nil);
         ctx_DBAccess.ExecQuery('DELETE FROM EE_STATES where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);

         Q := TevQuery.Create('select * from EE_TIME_OFF_ACCRUAL where EE_NBR = :eeNBR');
         Q.Params.AddValue('eeNBR', AEENbr);
         Q.Execute;
         if (Q.Result.RecordCount > 0) then
         begin
           pList:= Q.Result.GetColumnValues('EE_TIME_OFF_ACCRUAL_NBR');
           sFilter := BuildINsqlStatement('EE_TIME_OFF_ACCRUAL_NBR', pList.CommaText);
           ctx_DBAccess.ExecQuery('UPDATE EE_TIME_OFF_ACCRUAL_OPER set ADJUSTED_EE_TOA_OPER_NBR= null, CONNECTED_EE_TOA_OPER_NBR = null where ' + sFilter, nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM EE_TIME_OFF_ACCRUAL_OPER where ' + sFilter + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM EE_TIME_OFF_ACCRUAL where EE_NBR  = ' + IntToStr(AEENbr) + ' ORDER BY effective_date DESC ', nil, nil);
         end;

         clPersonNbr:= 0;
         Q := TevQuery.Create('select CL_PERSON_NBR, count(DISTINCT EE_NBR) from EE where CL_PERSON_NBR in (select DISTINCT CL_PERSON_NBR from EE where EE_NBR = :eeNBR)  group by  CL_PERSON_NBR');
         Q.Params.AddValue('eeNBR', AEENbr);
         Q.Execute;
         if (Q.Result.Fields[1].AsInteger = 1) then
           clPersonNbr:= Q.Result.Fields[0].AsInteger;

         Proc := TevStoredProc.Create('DEL_EE');
         Proc.Params.AddValue('NBR', AEENbr);
         Proc.Execute;
         if clPersonNbr > 0 then
         begin
           ctx_DBAccess.ExecQuery('DELETE FROM CL_HR_PERSON_EDUCATION where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM CL_HR_PERSON_HANDICAPS where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM CL_HR_PERSON_SKILLS where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM CL_PERSON_DEPENDENTS where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM CL_PERSON_DOCUMENTS where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM CO_HR_APPLICANT where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
           ctx_DBAccess.ExecQuery('DELETE FROM CL_PERSON where CL_PERSON_NBR  = ' + IntToStr(clPersonNbr) + ' ORDER BY effective_date DESC ', nil, nil);
         end;

         ctx_DataAccess.CommitNestedTransaction;
         result:= true;
       except
         ctx_DataAccess.RollbackNestedTransaction;
         raise;
       end;
end;

function TevRemoteMiscRoutines.GetACAHistory(const aEENbr,
  aYear: Integer): IEvDataSet;
const
  dsStruct: array [0..2] of TDSFieldDef = (
            (FieldName: 'Month'; DataType: ftInteger;  Size: 0;   Required: True),
            (FieldName: 'SY_FED_ACA_OFFER_CODES_NBR'; DataType: ftInteger;  Size: 0;   Required: false),
            (FieldName: 'SY_FED_ACA_RELIEF_CODES_NBR'; DataType: ftInteger;  Size: 0;   Required: false));
var
  Q: IEvQuery;
  s: String;
  i: integer;

  procedure addToResult(const sField: String);
  var
    tmpAca : variant;

    procedure UpdatePreMonths;
    begin
      if (result.vclDataSet.FieldValues['Month'] = Q.Result.FieldValues['rmonth']) and
         (result.FieldValues[sField] <> Q.Result.FieldValues[sField]) then
        begin
          if (result.FieldByName(sField).AsInteger <> 9999) and
             (Q.Result.FieldValues[sField] <> TmpAca) then
          begin
           Result.Edit;
           if (StartOfTheMonth(Q.Result.FieldValues['EFFECTIVE_DATE']) <> Q.Result.FieldValues['EFFECTIVE_DATE']) then
             result.FieldByName(sField).AsInteger:= 9999
           else
             result.FieldValues[sField]:= Q.Result.FieldValues[sField];
           Result.Post;
          end;
          TmpAca:= Q.Result.FieldByName(sField).Value;
        end
      else
        while (not Result.Eof) and
              (result.vclDataSet.FieldValues['Month'] <> Q.Result.FieldValues['rmonth']) do
        begin
            if result.FieldByName(sField).IsNull and
               (TmpAca <> result.FieldByName(sField).Value) then
            begin
              Result.Edit;
              result.FieldValues[sField]:= TmpAca;
              Result.Post;
            end;
            Result.Next;
        end;
    end;

    procedure UpdateMonths;
    begin
      if (Q.Result.FieldValues['rmonth'] <> 12) or (Q.Result.RecordCount = 1) then
        while not Result.Eof do
        begin
            if result.FieldByName(sField).IsNull and
               (TmpAca <> result.FieldByName(sField).Value) then
            begin
              Result.Edit;
              result.FieldValues[sField]:= TmpAca;
              Result.Post;
            end;
            Result.Next;
        end;

      Result.First;
      while not Result.Eof do
      begin
          if result.FieldByName(sField).AsInteger = 9998 then
          begin
            Result.Edit;
            result.FieldValues[sField]:= null;
            Result.Post;
          end;
          Result.Next;
      end;
    end;

  begin
    Q := TevQuery.Create(s);
    Q.Params.AddValue('aYear', aYear);
    Q.Params.AddValue('aEENbr', aEENbr);
    Q.Execute;
    Q.Result.IndexFieldNames:= 'rYear;rmonth;EFFECTIVE_DATE';

    if Q.Result.RecordCount > 0 then
    begin
      Q.Result.First;
      result.First;
      Result.Edit;    //  first month
      result.FieldValues[sField]:= Q.Result.FieldValues[sField];
      Result.Post;
      TmpAca:= Q.Result.FieldByName(sField).Value;
      Q.Result.Next;
      while not Q.Result.Eof do
      begin
        if (result.vclDataSet.FieldValues['Month'] <> Q.Result.FieldValues['rmonth']) or
            ((result.vclDataSet.FieldValues['Month'] = Q.Result.FieldValues['rmonth']) and
             (result.FieldValues[sField] <> Q.Result.FieldValues[sField])) then
           UpdatePreMonths;

        if result.Locate('Month',Q.Result.FieldValues['rmonth'],[]) then
        begin
           Result.Edit;
           if (Q.Result.FieldValues['rYear'] = aYear) and
              (StartOfTheMonth(Q.Result.FieldValues['EFFECTIVE_DATE']) <> Q.Result.FieldValues['EFFECTIVE_DATE']) then
           begin
             if Q.Result.FieldValues[sField] <> tmpAca then
                result.FieldByName(sField).AsInteger := 9999
             else
              if result.FieldByName(sField).AsInteger <>  9999 then
                result.FieldValues[sField]:= Q.Result.FieldValues[sField];

           end
           else
           if result.FieldByName(sField).IsNull then
           begin
              if Q.Result.FieldByName(sField).IsNull then
                result.FieldByName(sField).AsInteger := 9998
              else
                result.FieldValues[sField]:= Q.Result.FieldValues[sField];
           end;

           Result.Post;
           tmpAca := Q.Result.FieldByName(sField).Value;
        end;
        Q.Result.Next;
      end;

      UpdateMonths;
    end;
  end;

begin
  result := nil;
  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee where ee_nbr=:EENbr and {AsOfNow<ee>}');
  Q.Params.AddValue('EENbr', aEENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(aEENbr));

  Result := TevDataSet.Create(dsStruct);

  for i := 1 to 12 do
    result.AppendRecord([i]);
  s:= 'select distinct EFFECTIVE_DATE, EFFECTIVE_UNTIL, extractmonth(EFFECTIVE_DATE) rmonth,  extractyear(EFFECTIVE_DATE) rYear, SY_FED_ACA_OFFER_CODES_NBR ' +
        'from EE  where  extractyear(EFFECTIVE_DATE) < :aYear + 1 and  EE_NBR = :aEENbr and ' +
                       'EFFECTIVE_DATE >= ( ' +
                               'select COALESCE(Max(EFFECTIVE_DATE), ''1/1/1900'') ' +
                                      'from EE where extractyear(EFFECTIVE_DATE) < :aYear and  EE_NBR = :aEENbr) ';

  addToResult('SY_FED_ACA_OFFER_CODES_NBR');

  s:= 'select distinct EFFECTIVE_DATE, EFFECTIVE_UNTIL, extractmonth(EFFECTIVE_DATE) rmonth,  extractyear(EFFECTIVE_DATE) rYear, SY_FED_ACA_RELIEF_CODES_NBR ' +
        'from EE  where  extractyear(EFFECTIVE_DATE) < :aYear + 1 and  EE_NBR = :aEENbr and ' +
                       'EFFECTIVE_DATE >= ( ' +
                               'select COALESCE(Max(EFFECTIVE_DATE), ''1/1/1900'') ' +
                                      'from EE where extractyear(EFFECTIVE_DATE) < :aYear and  EE_NBR = :aEENbr) ';


  addToResult('SY_FED_ACA_RELIEF_CODES_NBR');

end;

function TevRemoteMiscRoutines.PostACAHistory(const aEENbr, aYear: Integer;
  const aCd: IEvDataSet): boolean;
var
 tCd: IEvDataSet;
 bDate, eDate: TDateTime;
 Q: IEvQuery;
 FData: IevDataSet;

 function UpdateDatabase(const aField: String):boolean;
 begin
    ctx_DBAccess.StartTransaction([dbtClient]);
    try
        tCd.First;
        while not tCd.Eof do
        begin
          if aCd.Locate('Month', tCd.FieldValues['Month'],[]) and
             (tCd.FieldValues[aField] <> aCd.FieldValues[aField]) and
             (aCd.FieldByName(aField).AsInteger <> 9999) then
          begin
           bDate:= EncodeDate(aYear, tCd.FieldValues['Month'], 1);
           eDate:= Trunc(EndOfTheMonth(bDate));

           FData := ctx_DBAccess.GetFieldVersions('EE', aField, aEENbr, nil);
           FData.First;
           while not FData.Eof do
           begin
              if eDate > FData['begin_date']  then
              begin
                   if eDate < FData['end_date'] then
                      eDate:= FData['end_date']
              end
              else break;
            FData.Next;
           end;
           ctx_DBAccess.UpdateFieldValue('EE',aField, aEENbr, bDate, eDate, aCd.FieldValues[aField]);
          end;
          tCd.Next;
        end;
        ctx_DBAccess.CommitTransaction;
        result:= True;
    except
      ctx_DBAccess.RollbackTransaction;
      raise;
    end;
 end;

begin
  //checking that employee exists
  Q := TEvQuery.Create('Select ee_nbr from ee where ee_nbr=:EENbr and {AsOfNow<ee>}');
  Q.Params.AddValue('EENbr', aEENbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + intToStr(aEENbr));

  tCd:= GetACAHistory(aEENbr, aYear);
  result:= UpdateDatabase('SY_FED_ACA_OFFER_CODES_NBR');
  if result then
     result:= UpdateDatabase('SY_FED_ACA_RELIEF_CODES_NBR');
end;

function TevRemoteMiscRoutines.GetACAOfferCodes(const aCoNbr: integer; const AsOfDate: TDateTime): IEvDataSet;

const
  dsStruct: array [0..3] of TDSFieldDef = (
            (FieldName: 'SY_FED_ACA_OFFER_CODES_NBR'; DataType: ftInteger;  Size: 0;   Required: True),
            (FieldName: 'ACA_OFFER_CODE'; DataType: ftString;   Size: 2;   Required: True),
            (FieldName: 'ACA_OFFER_CODE_DESCRIPTION'; DataType: ftString;  Size: 40;   Required: True),
            (FieldName: 'DESCRIPTION'; DataType: ftString;  Size: 44;   Required: True));
var
  Q: IEvQuery;

  procedure addToResult;
  begin
    if Q.Result.RecordCount > 0 then
    begin
      Q.Result.First;
      while not Q.Result.Eof do
      begin
        Result.AppendRecord([Q.Result.FieldByName('SY_FED_ACA_OFFER_CODES_NBR').AsInteger,
                              Q.Result.FieldByName('ACA_OFFER_CODE').AsString,
                               Q.Result.FieldByName('ACA_OFFER_CODE_DESCRIPTION').AsString,
                                Q.Result.FieldByName('ACA_OFFER_CODE').AsString + '  ' + Q.Result.FieldByName('ACA_OFFER_CODE_DESCRIPTION').AsString]);
        Q.Result.Next;
      end;
    end;
  end;

begin
 Result := TevDataSet.Create(dsStruct);

 Q := TevQuery.Create('select SY_FED_ACA_OFFER_CODES_NBR, ACA_OFFER_CODE,ACA_OFFER_CODE_DESCRIPTION  from SY_FED_ACA_OFFER_CODES where {AsOfDate<SY_FED_ACA_OFFER_CODES>} ');
 Q.Params.AddValue('StatusDate', AsOfDate);
 Q.Execute;
 addToResult;

end;

function TevRemoteMiscRoutines.GetACAReliefCodes(const aCoNbr: integer; const AsOfDate: TDateTime): IEvDataSet;

const
  dsStruct: array [0..3] of TDSFieldDef = (
            (FieldName: 'SY_FED_ACA_RELIEF_CODES_NBR'; DataType: ftInteger;  Size: 0;   Required: True),
            (FieldName: 'ACA_RELIEF_CODE'; DataType: ftString;   Size: 2;   Required: True),
            (FieldName: 'ACA_RELIEF_CODE_DESCRIPTION'; DataType: ftString;  Size: 40;   Required: True),
            (FieldName: 'DESCRIPTION'; DataType: ftString;  Size: 44;   Required: True));
var
  Q: IEvQuery;

  procedure addToResult;
  begin
    if Q.Result.RecordCount > 0 then
    begin
      Q.Result.First;
      while not Q.Result.Eof do
      begin
        Result.AppendRecord([Q.Result.FieldByName('SY_FED_ACA_RELIEF_CODES_NBR').AsInteger,
                              Q.Result.FieldByName('ACA_RELIEF_CODE').AsString,
                               Q.Result.FieldByName('ACA_RELIEF_CODE_DESCRIPTION').AsString,
                                Q.Result.FieldByName('ACA_RELIEF_CODE').AsString + '  ' + Q.Result.FieldByName('ACA_RELIEF_CODE_DESCRIPTION').AsString]);
        Q.Result.Next;
      end;
    end;
  end;

begin
 Result := TevDataSet.Create(dsStruct);
 Q := TevQuery.Create('select SY_FED_ACA_RELIEF_CODES_NBR, ACA_RELIEF_CODE,ACA_RELIEF_CODE_DESCRIPTION  from SY_FED_ACA_RELIEF_CODES where {AsOfDate<SY_FED_ACA_RELIEF_CODES>} ');
 Q.Params.AddValue('StatusDate', AsOfDate);
 Q.Execute;
 addToResult;

end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRemoteMiscRoutines, IevRemoteMiscRoutines, 'Remote Misc Routines');

end.
