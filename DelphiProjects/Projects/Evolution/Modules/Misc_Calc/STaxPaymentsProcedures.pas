// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STaxPaymentsProcedures;

interface

uses  EvComponentsExt, Variants, EvStreamUtils, EvBasicUtils, EvContext, isThreadManager,
     SReportSettings, Math, isBasicUtils, IsBaseClasses, evAchBase, EvExceptions, EvClientDataSet;

function PayTaxesExt(const FedTaxLiabilityList, StateTaxLiabilityList,
  SUITaxLiabilityList, LocalTaxLiabilityList: string; const ForceChecks: Boolean;
  const EftpReference: string; const SbTaxPaymentNbr : integer): TTaxPaymentParams;

function PrintChecksAndCouponsForTaxes(const TaxPayrollFilter, DefaultMiscCheckForm: string): TrwReportResults;

function FinalizeTaxPayments(const Params: IisStream; const AACHOptions: IisStringList; const SbTaxPaymentNbr : Integer;
  PostProcessReportName, DefaultMiscCheckForm: String): IisStream;

function CreateMiscCheckPayroll(const CoNbr : Integer):integer;

implementation

uses
  EvDataAccessComponents,
  SysUtils, EvConsts, EvUtils, SDataStructure, Classes, Db,
  EvTypes, SMiscCheckProcedures, Windows,
  SDM_NyDebitPayments, SPD_EFTPS_Payment,
  evAchEFT, ISUtils, DateUtils, SDataDictsystem, rwEvUtils, EvDataset, EvCommonInterfaces,
  ISKbmMemDataSet, SDataDictclient;

const
  ForceAchKeySplit = False;

type
  TMaDebitPayments = class(TComponent, IMaDebitFile)
  private
    FCd: TevClientDataSet;
  public
    constructor Create(AOwner: TComponent); reintroduce;
    procedure FillStringList(const ResultList: TStringList);
    procedure AddPayment(const RecordIdentifier: Char; const TaxPeriodEnd: TDateTime; const FEID: string;
      const BusinessName: string; const Amount: Currency; const SettlementDate: TDateTime;
      const SbBankNbr: Integer; const BankAccNumber: string; const AccountType: Char);
  end;
  TCaDebitPayments = class(TComponent, ICaDebitFile)
  private
    FCd: TevClientDataSet;
  public
    constructor Create(AOwner: TComponent); reintroduce;
    procedure FillStringList(const ResultList: TStringList);
    procedure AddPayment(const CheckDate, DueDate: TDateTime; const AccountNumber, SecurityCode, TaxTypeCode: string;
      const StateWithholding, Sdi: Currency);
  end;
  TUniversalDebitPayments = class(TComponent, IUniversalDebitPaymentParams)
  private
    FCd: TevClientDataSet;
  public
    constructor Create(AOwner: TComponent); reintroduce;
    procedure FillStringList(const ResultList: TStringList);
    procedure AddPayment(const SyGlobalAgencyNbr, ClientId, GroupId, MainCoNbr: Integer;
                          const DueDate: TDateTime; const Amount : currency;
                          const StateLiabNbrs, SuiLiabNbrs, LocalLiabNbrs: string);
  end;
  TOneMiscCheckPayments = class(TComponent, IOneMiscCheckPaymentParams)
  private
    FCd: TevClientDataSet;
  public
    constructor Create(AOwner: TComponent); reintroduce;
    Function OneCheckResult(const SbTaxPaymentNbr : integer;const DefaultMiscCheckForm :String): IisStream;
    Function OneCheckResultReport: IisStream;

    // TrwReportResults;
    procedure AddPayment(const SyGlobalAgencyNbr, ClientId,MainCoNbr,
                            TaxDepositId, BankAccRegNbr, FieldOfficeNbr : Integer;
                            const DueDate : TDateTime;
                            const TotalAmount : Currency; const GroupKey : String);
  end;


const
  Fed941Filter = '(TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_ER_FUI + '''  and TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_FEDERAL_945 + ''' and TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING + ''')';
  Fed940Filter = 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_ER_FUI + '''';
  Fed945Filter = '(TAX_TYPE= ''' + TAX_LIABILITY_TYPE_FEDERAL_945 + ''' or TAX_TYPE = ''' + TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING + ''')';


function CreateMiscCheckPayroll(const CoNbr : Integer):integer;
    function GetNextRunNumber(CheckDate: TDateTime): Integer;
    begin
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then
          Close;
        with TExecDSWrapper.Create('NextRunNumberForCheckDate') do
        begin
          SetParam('CheckDate', CheckDate);
          SetParam('Co_Nbr', CoNbr);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        Result := ConvertNull(Fields[0].Value, 0);
        Close;
        Result := Result + 1;
      end;
    end;
begin
        with DM_PAYROLL do
        begin
          Assert(CoNbr <> 0);
          if not PR.Active then
             PR.Open;
          PR.Insert;
          PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
          PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
          PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
          PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
          PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;


          PR.FieldByName('CO_NBR').AsInteger := CoNbr;
          PR.FieldByName('CHECK_DATE').AsDateTime := Date;
          PR.FieldByName('RUN_NUMBER').AsInteger := GetNextRunNumber(Date);
          PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
          PR.FieldByName('SCHEDULED').Value := 'N';
          PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_TAX_DEPOSIT;
          PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
          PR.FieldByName('EXCLUDE_ACH').Value := 'N';
          PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
          PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y';
          PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
          PR.FieldByName('EXCLUDE_BILLING').Value := 'Y';
          PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_REPORTS;
          PR.Post;
          Result := PR['PR_NBR'];
        end
end;

procedure UpdateMiscCheck(const CoTaxDepositNbr: Integer; const Amount: Currency);
begin
  with DM_PAYROLL.PR_MISCELLANEOUS_CHECKS do
  begin
    Assert(Active);
    if Locate('CO_TAX_DEPOSITS_NBR', CoTaxDepositNbr, []) then   // if deposit is a check
    begin
      Assert(DM_PAYROLL.PR.Locate('PR_NBR', FieldValues['PR_NBR'], []));
      Edit;
      FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency := FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency + Amount;
      Post;
    end;
  end;
end;

Function GetPrMiscCheckNbr(const CoTaxDepositNbr: Integer):Integer;
begin
  Result := 0;
  Assert(DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Active);
  if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Locate('CO_TAX_DEPOSITS_NBR', CoTaxDepositNbr, []) then
     Result := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsInteger;
end;


function CreateNewTaxDeposit(const CoNbr, SbTaxPaymentNbr : integer;
                             const Reference, TAX_DEPOSIT_TYPE, TAX_DEPOSIT_STATUS: string;
                             const aPR_NBR: Variant;
                             const Global_Agency_Nbr: Integer;
                             const FieldOfficeNbr: Integer): Integer;
begin

  with DM_COMPANY.CO_TAX_DEPOSITS do
  begin
    if not Active then
      Open;
    Append;
    Assert(CoNbr <> 0);
    FieldByName('CO_NBR').AsInteger := CoNbr;
    FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := Reference;
    FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS;
    if aPR_NBR <> -1 then
       FieldByName('PR_NBR').Value := aPR_NBR;
    FieldByName('SB_TAX_PAYMENT_NBR').Value := SbTaxPaymentNbr;
    FieldByName('DEPOSIT_TYPE').AsString := TAX_DEPOSIT_TYPE;
    if FieldOfficeNbr <> -1 then
      FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger := FieldOfficeNbr
    else
      if Global_Agency_Nbr <> -1 then
        FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger := GetGlobalAgencyFieldOffice(Global_Agency_Nbr);
    Post;
    Result := FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
  end;
end;


function PayTaxesExt(const FedTaxLiabilityList, StateTaxLiabilityList,
  SUITaxLiabilityList, LocalTaxLiabilityList: string; const ForceChecks: Boolean;
  const EftpReference: string; const SbTaxPaymentNbr : integer): TTaxPaymentParams;
type
  TPendingAchRecord = record
    RecNo: Integer;
    v: Variant;
  end;
var
  SuperCheckStates: IisStringList;
  Pr_Nbr_List: TStringList;
  iPr_Nbr: Integer;  // for misc tax checks we probably will need to cut
  CurrentTaxName: string; // for EFTPS
  EftQuarterNumber: Integer;
  NegativeWaitingTotal: Currency;
  MainCoNbr, MainCoStateNbr: Integer;
  cdPendingDeposits, cdPendingEftDebits: TevClientDataSet;
  FPendingAchRecords: array of TPendingAchRecord;
  lAchDate: TDateTime;
  LockedCompanies:TStringList;
  FedMainCoControl : boolean;


  function FindAchRecordIndex(const RecNo: Integer): Integer;
  var
    s: string;
  begin
    s := '';
    Result := High(FPendingAchRecords);
    while Result >= Low(FPendingAchRecords) do
      if FPendingAchRecords[Result].RecNo <> RecNo then
      begin
        s := s + ';' + IntToStr(FPendingAchRecords[Result].RecNo);
        Dec(Result)
      end
      else
        Break;
    Assert(Result >= Low(FPendingAchRecords), MakeString(['Error data:', RecNo, Low(FPendingAchRecords), High(FPendingAchRecords), s]));
  end;

  function CO_NBR: integer;
  begin
    result := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
  end;

  function FullTaxService: boolean;
  begin
    result := DM_COMPANY.CO.FieldByName('TAX_SERVICE').AsString = 'Y';
  end;

  procedure LoadDataSetWithList(const ds: TevClientDataset; const Filter: string);
  var
    sl:TStringList;
    f: string;
  begin
    ds.Close;
    if Filter <> '' then
      ds.DataRequired(Filter)
    else
      ds.DataRequired(AlwaysFalseCond);
    ds.First;
    sl := TStringList.Create;

    try
      while not ds.Eof do
      begin
        try
           ctx_DataAccess.CheckCompanyLock(DS.FieldByName('CO_NBR').AsInteger, DS.FieldByName('CHECK_DATE').AsDateTime, True, True);
           sl.Add(DS.FieldByName(ds.TableName+ '_NBR').AsString);
         except
            on E: Exception do
            begin
              if LockedCompanies.IndexOf(DS.FieldByName('CO_NBR').AsString) =-1 then
              begin
                Result.Warnings.Add(E.Message);
                LockedCompanies.Add(DS.FieldByName('CO_NBR').AsString);
              end;
            end
         end;
         ds.Next;
      end;
      if sl.Count > 0 then
        f := BuildINsqlStatement(ds.TableName+ '_NBR', sl.CommaText)
      else
        f := AlwaysFalseCond;
      ds.DataRequired(F);
    finally
     sl.Free;

    end;
  end;


  procedure AddToBankAccountRegister(const LiabilityDataSet: TevClientDataSet; CoNbr, SbBankAccNbr, DepositNbr: Integer;
                                     const DueDate: TDateTime; const Amount: Currency;
                                      const RegType : Char);
  var
    BankAccRegNbr : integer;
  begin
    ctx_PayrollCalculation.AddToBankAccountRegister(CoNbr, SbBankAccNbr, DepositNbr,
                              -Amount, BANK_REGISTER_STATUS_Outstanding, RegType,
                               DateOf(SysTime), DueDate, DateOf(SysTime), BankAccRegNbr);
    if Assigned(LiabilityDataSet) then
      ctx_PayrollCalculation.AddConsolidatedPaymentsToBar([LiabilityDataSet], DepositNbr,
                     BANK_REGISTER_STATUS_Outstanding,
                      BANK_REGISTER_TYPE_Consolidated, False);
  end;


  function GetPrNbr: Integer;
  var
    s: string;
  begin
    if iPr_Nbr = -1 then
    begin
      s := Pr_Nbr_List.Values[IntToStr(MainCoNbr)];
      if s = '' then
      begin
          iPr_Nbr := CreateMiscCheckPayroll(MainCoNbr);
          Pr_Nbr_List.Append(IntToStr(MainCoNbr) + '=' + IntToStr(iPr_Nbr));
      end
      else
      begin
        iPr_Nbr := StrToInt(s);
        Assert(DM_PAYROLL.PR.Locate('PR_NBR', iPr_Nbr, []));
      end;
    end;
    Result := iPr_Nbr;
  end;

  procedure ResetPrNbr;
  begin
    iPr_Nbr := -1;
  end;

  function CheckIfAllLiabilityStatusesAreLegal(aDataSet: TDataSet): Boolean;
  var
    LegalLiabilityStatus: set of Char;
  begin
    if not CompanyRequiresTaxesBeImpounded then
      LegalLiabilityStatus := [TAX_DEPOSIT_STATUS_PENDING, TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED]
    else
      LegalLiabilityStatus := [TAX_DEPOSIT_STATUS_IMPOUNDED, TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED];
    Result := True;
    with aDataSet do
      if Active then
      begin
        First;
        while not EOF do
        begin
          if FieldByName('CO_TAX_DEPOSITS_NBR').IsNull
            and (FieldByName('Status').AsString <> '')
            and not (FieldByName('Status').AsString[1] in LegalLiabilityStatus) then
          begin
            Result := False;
            Exit;
          end;
          Next;
        end;
        First;
      end;
  end;

  procedure FillAchKey(const DataSet: TevClientDataSet; const DefaultReportsGroupNbr, DefaultFieldOfficeNbr: Integer;
                        const TaxType: TTaxTable;
                         const isItTCD : Boolean = false; const TCDDepositNbr : integer = -1);
  var
    s: ShortString;
    cdDepFreq: TevClientDataSet;
    fFieldOfficeNbr, fCouponNbr, fAchKey, fCheckDate, fFiller, fSyFieldOfficeNbr, fSyReportsGroupNbr, fPeriodEndDate, fAdjPeriodEndDate: TField;
    Nbr: Integer;

    Function GetReportNumber(cd: TevClientDataSet; ReportsGroupNbr: integer; PeriodEndDate : TDateTime): Integer;
    begin
       result := -1;
       cd.FindNearest([ReportsGroupNbr]);
       while (cd['SY_REPORTS_GROUP_NBR'] = ReportsGroupNbr) and
             (not cd.Eof) do
       begin
         if (VarIsNull(cd['ACTIVE_YEAR_FROM']) and VarIsNull(cd['ACTIVE_YEAR_TO'])) or
            ((cd['ACTIVE_YEAR_FROM'] <= PeriodEndDate) and
             (VarIsNull(cd['ACTIVE_YEAR_TO']))) or
            ((cd['ACTIVE_YEAR_FROM'] <= PeriodEndDate) and
             (cd['ACTIVE_YEAR_TO'] >= PeriodEndDate)) then
          begin
           Result := cd['SY_REPORTS_NBR'];
           break;
          end;
          cd.Next;
       end;
    end;

  begin
    fFieldOfficeNbr := DataSet.FindField('FIELD_OFFICE_NBR');
    fCouponNbr := DataSet.FindField('TAX_COUPON_SY_REPORTS_NBR');
    fAchKey := DataSet.FindField('ACH_KEY');
    fCheckDate := DataSet.FindField('CHECK_DATE');
    fFiller := DataSet.FindField('FILLER');
    fPeriodEndDate := DataSet.FieldByName('PERIOD_END_DATE');
    fAdjPeriodEndDate := DataSet.FieldByName('ADJ_PERIOD_END_DATE');

    if isItTCD then
    begin
      DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.IndexFieldNames := 'SY_AGENCY_DEPOSIT_FREQ_NBR';
      cdDepFreq := DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ;
      fSyReportsGroupNbr := cdDepFreq.FieldByName('SY_REPORTS_GROUP_NBR');

      DataSet.First;
      while not DataSet.Eof do
      begin
        DataSet.Edit;
        if fCheckDate.IsNull then
          raise EInconsistentData.CreateHelp('Check date can''t be empty', IDH_InconsistentData);
        if fAchKey.AsString = '' then
          fAchKey.AsString := FormatDateTime('mm/dd/yyyy', GetEndQuarter(fCheckDate.AsDateTime)) +
            IntToStr(GetQuarterNumber(fCheckDate.AsDateTime));
        if fPeriodEndDate.IsNull then
          fAdjPeriodEndDate.AsDateTime := GetEndQuarter(fCheckDate.AsDateTime)
        else
          fAdjPeriodEndDate.AsDateTime := Min(Trunc(GetEndQuarter(fCheckDate.AsDateTime)),
            Trunc(fPeriodEndDate.AsDateTime));

        if Assigned(fFieldOfficeNbr) then
           fFieldOfficeNbr.AsInteger := DefaultFieldOfficeNbr;

        if Assigned(fCouponNbr) then
        begin
          if TCDDepositNbr > 0 then
          begin
            if cdDepFreq.FindKey([TCDDepositNbr]) then
            begin
              if Assigned(fCouponNbr) then
                fCouponNbr.AsInteger := GetReportNumber(DM_SYSTEM_STATE.SY_REPORTS, ConvertNull(fSyReportsGroupNbr.Value, -1), fCheckDate.AsDateTime);
            end
            else
            begin
              if Assigned(fCouponNbr) then
                fCouponNbr.AsInteger := GetReportNumber(DM_SYSTEM_STATE.SY_REPORTS, DefaultReportsGroupNbr, fCheckDate.AsDateTime);
            end;
          end
          else
          begin
            if Assigned(fCouponNbr) then
              fCouponNbr.AsInteger := -1;
          end;
        end;
        DataSet.Post;
        DataSet.Next;
      end;
      DataSet.First;

    end
    else
    begin
      if TaxType = ttLoc then
      begin
        DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.IndexFieldNames := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
        cdDepFreq := DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ;
      end
      else
        cdDepFreq := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ;

      fSyFieldOfficeNbr := cdDepFreq.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR');
      fSyReportsGroupNbr := cdDepFreq.FieldByName('SY_REPORTS_GROUP_NBR');

      DataSet.First;
      while not DataSet.Eof do
      begin
        DataSet.Edit;
        if fCheckDate.IsNull then
          raise EInconsistentData.CreateHelp('Check date can''t be empty', IDH_InconsistentData);
        if fAchKey.AsString = '' then
          fAchKey.AsString := FormatDateTime('mm/dd/yyyy', GetEndQuarter(fCheckDate.AsDateTime)) +
            IntToStr(GetQuarterNumber(fCheckDate.AsDateTime));
        if fPeriodEndDate.IsNull then
          fAdjPeriodEndDate.AsDateTime := GetEndQuarter(fCheckDate.AsDateTime)
        else
          fAdjPeriodEndDate.AsDateTime := Min(Trunc(GetEndQuarter(fCheckDate.AsDateTime)),
            Trunc(fPeriodEndDate.AsDateTime));
        if Assigned(fFieldOfficeNbr) or Assigned(fCouponNbr) then
        begin
          s := Trim(Copy(fFiller.AsString, 12, 8));
          if s <> '' then
          begin
            try
              Nbr := StrToInt(s);
            except
              Nbr := -1;
            end;
            if cdDepFreq.FindKey([Nbr]) then
            begin
              if Assigned(fFieldOfficeNbr) then
                fFieldOfficeNbr.AsInteger := ConvertNull(fSyFieldOfficeNbr.Value, -1);
              if Assigned(fCouponNbr) then
                fCouponNbr.AsInteger := GetReportNumber(DM_SYSTEM_STATE.SY_REPORTS, ConvertNull(fSyReportsGroupNbr.Value, -1), fCheckDate.AsDateTime);
            end
            else
            begin
              if Assigned(fFieldOfficeNbr) then
                fFieldOfficeNbr.AsInteger := DefaultFieldOfficeNbr;
              if Assigned(fCouponNbr) then
                fCouponNbr.AsInteger := GetReportNumber(DM_SYSTEM_STATE.SY_REPORTS, DefaultReportsGroupNbr, fCheckDate.AsDateTime);
            end;
          end
          else
          begin
            if Assigned(fFieldOfficeNbr) then
              fFieldOfficeNbr.AsInteger := DefaultFieldOfficeNbr;
            if Assigned(fCouponNbr) then
              fCouponNbr.AsInteger := GetReportNumber(DM_SYSTEM_STATE.SY_REPORTS, DefaultReportsGroupNbr, fCheckDate.AsDateTime);
          end;
        end;
        DataSet.Post;
        DataSet.Next;
      end;
      DataSet.First;
    end;
  end;

  function BuildLiabFilter(const ds: TevClientDataSet; const FilterFieldName, FilterFieldValue, KeyFieldName: string): string;
  var
    lFilter: string;
    dataset: IevDataset;
  begin
    Result := '';
    dataset := TevDataSet.Create;
    dataset.Data := ds.GetDataStream(False, False);
    if FilterFieldName <> '' then
    begin
      case dataset.FieldByName(FilterFieldName).DataType of
        ftString:
          lFilter := FilterFieldName + '=''' + FilterFieldValue + '''';
        else
          lFilter := FilterFieldName + '=' + FilterFieldValue;
      end;
      if (dataset.Filter <> '') and dataset.Filtered then
        dataset.Filter := '(' + dataset.Filter + ') and (' + lFilter + ')'
      else
        dataset.Filter := lFilter;
      dataset.Filtered := True;
    end;
    dataset.First;
    while not dataset.Eof do
    begin
      if Result <> '' then
        Result := Result + ',';
      case dataset.FieldByName(KeyFieldName).DataType of
        ftString:
          Result := Result + '''' + dataset.FieldByName(KeyFieldName).AsString + '''';
        else
          Result := Result + dataset.FieldByName(KeyFieldName).AsString;
      end;
      dataset.Next;
    end;
    if Result <> '' then
      Result := KeyFieldName + ' in (' + Result + ')';
  end;

  procedure PrepareTable(const ds: TevClientDataSet; const sFilter: string; const SkipStatusCheck: Boolean = True);
  begin
    ds.Filter := sFilter;
    ds.Filtered := True;
  end;

  procedure UnPrepareTable(const ds: TevClientDataSet);
  begin
    ds.Filtered := False;
    ds.Filter := '';
  end;

  function GetTaxLiabilities(const LiabilityDataSet: TevClientDataSet; const ExtraFilter: string = ''; const OverrideCurrentFilter: Boolean = False): Currency;
  var
    d: TevClientDataSet;
    f: TField;
  begin
    Result := 0;
    if not LiabilityDataSet.Active then
      Exit;
    d := TevClientDataSet.Create(nil);
    with d do
    try
      CloneCursor(LiabilityDataSet, (ExtraFilter <> '') and OverrideCurrentFilter);
      Filtered := True;
      f := FieldByName('AMOUNT');
      if ExtraFilter = '' then
        Filter := LiabilityDataSet.Filter + ' and (CO_TAX_DEPOSITS_NBR is null or CO_TAX_DEPOSITS_NBR = -1)'
      else
        if OverrideCurrentFilter then
          Filter := ExtraFilter
        else
          Filter := LiabilityDataSet.Filter + ' and (CO_TAX_DEPOSITS_NBR is null or CO_TAX_DEPOSITS_NBR = -1) and ' + ExtraFilter;
      First;
      while not Eof do
      begin
        Result := Result + f.AsCurrency;
        Next;
      end;
    finally
      Free
    end;
  end;

  function ConvertTaxPayment(const ds: TevClientDataSet;
    const TaxType: TTaxTable;
    const TaxPaymentMethod: Char; const TotalAmount, Amount1: Currency;
    Amount2: Variant; out NewTaxDepositStatus: Char;
    const Global_Agency_Nbr, FieldOfficeNbr, CouponReportNbr: Integer;
    const sEIN, sKey, stcdEIN: string): integer;
  var
    NewTaxDepositId: Integer;
    bNewTaxDepositId : Boolean;
    Reference: string;
    CheckGlobal_Agency_Nbr: Integer;
    TAX_DEPOSIT_TYPE: string;
    Pr_Nbr: Variant;
    s: ShortString;
    tmpEIN: string;
    Filler: string;
    j: Integer;
    dPeriodEnd, dMaxCheckDate, tmpCADate: TDateTime;

    procedure SetNyDebit;
    begin
       if NewTaxDepositId > 0 then
       begin
          if cdPendingEftDebits.FindKey([NewTaxDepositId]) then
            cdPendingEftDebits.Edit
          else
          begin
            cdPendingEftDebits.Insert;
            cdPendingEftDebits['DEPOSIT_NBR'] := NewTaxDepositId;
            cdPendingEftDebits['EIN'] := sEIN;
            case TaxType of
              ttState:
                begin
                  cdPendingEftDebits['LAST_CHECK_DATE'] := ConvertNull(DM_COMPANY.CO_STATE_TAX_LIABILITIES['PERIOD_END_DATE'], Date);
                  cdPendingEftDebits['DUE_DATE'] := ConvertNull(DM_COMPANY.CO_STATE_TAX_LIABILITIES['DUE_DATE'], Date);
                end;
              ttLoc:
                begin
                  cdPendingEftDebits['LAST_CHECK_DATE'] := ConvertNull(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES['PERIOD_END_DATE'], Date);
                  cdPendingEftDebits['DUE_DATE'] := ConvertNull(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES['DUE_DATE'], Date);
                end;
            end;
            cdPendingEftDebits['PAYROLLS_TOTAL'] := 1;
            cdPendingEftDebits['NyStateTax'] := 0;
            cdPendingEftDebits['NyCityTax'] := 0;
            cdPendingEftDebits['CityOfYorkersTax'] := 0;
          end;
          case TaxType of
            ttState:
              cdPendingEftDebits['NyStateTax'] := cdPendingEftDebits['NyStateTax'] + TotalAmount;
            ttLoc:
              case Integer(ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], 'SY_COUNTY_NBR'), -1)) of
                165, 1:
                  cdPendingEftDebits['CityOfYorkersTax'] := cdPendingEftDebits['CityOfYorkersTax'] + TotalAmount;
                110, 38:
                  cdPendingEftDebits['NyCityTax'] := cdPendingEftDebits['NyCityTax'] + TotalAmount;
              end;
          end;
          cdPendingEftDebits.Post;
       end;
    end;
    function getCALastCheckDate:TDateTime;
    begin
       DM_COMPANY.CO_STATE_TAX_LIABILITIES.last;
       result:= DM_COMPANY.CO_STATE_TAX_LIABILITIES.CHECK_DATE.AsDateTime;
       DM_COMPANY.CO_STATE_TAX_LIABILITIES.First;
    end;

  begin
    CheckGlobal_Agency_Nbr := Global_Agency_Nbr;
    tmpEIN := sEIN;
    if stcdEIN <> '' then
      tmpEIN := stcdEIN;
    case TaxPaymentMethod of
      TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT:
        begin
          TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_CREDIT;
          tmpEIN := sEIN;
        end;
      TAX_PAYMENT_METHOD_DEBIT, TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_DEBIT;
      TAX_PAYMENT_METHOD_NOTICES:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_NOTICE;
      TAX_PAYMENT_METHOD_CHECK, TAX_PAYMENT_METHOD_NOTICES_CHECKS:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_CHECK;
    end;

    NewTaxDepositId := -1;
    if TaxType <> ttFed then
    begin
      cdPendingDeposits.SetRange([TAX_DEPOSIT_TYPE, Global_Agency_Nbr, tmpEIN, sKey], [TAX_DEPOSIT_TYPE, Global_Agency_Nbr, tmpEIN, sKey]);
      if (cdPendingDeposits.RecordCount > 0) then
      begin
        cdPendingDeposits.First;
        while not cdPendingDeposits.Eof do
        begin
          if (cdPendingDeposits['COUPON_NBR'] = CouponReportNbr) and (cdPendingDeposits['OFFICE_NBR'] = FieldOfficeNbr) then
          begin
            NewTaxDepositId := cdPendingDeposits['DEPOSIT_NBR'];
            Break;
          end;
          cdPendingDeposits.Next;
        end;
        if (NewTaxDepositId = -1) and (CouponReportNbr = -1) and (FieldOfficeNbr = -1) then
          NewTaxDepositId := cdPendingDeposits['DEPOSIT_NBR'];
      end;
    end;

    case TaxPaymentMethod of
      TAX_PAYMENT_METHOD_DEBIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
        begin
          Pr_Nbr := Null;
          case TaxType of
            ttFed:
              with DM_COMPANY do
              begin
                NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PENDING;
                if (VarIsNull(CO['PIN_NUMBER']) or (Length(Trim(CO.FieldByName('PIN_NUMBER').AsString)) = 0) )  then
                  raise ETaxDepositException.CreateHelp('Company is not ready to use EFTP. Please enroll the company first', IDH_CanNotPerformOperation);
                if VarIsNull(CO['TAX_SB_BANK_ACCOUNT_NBR']) then
                  raise ETaxDepositException.CreateHelp('Company SB tax account is not set.', IDH_CanNotPerformOperation);
                s := CO_FED_TAX_LIABILITIES['ACH_KEY'];
                if not TryEncodeDate(StrToIntDef(Copy(s, 7, 4), 0), StrToIntDef(Copy(s, 1, 2), 0), StrToIntDef(Copy(s, 4, 2), 0), lAchDate) then
                  raise ETaxDepositException.CreateFmtHelp('Invalid ach key value "%s"', [s], IDH_CanNotPerformOperation);
                Assert(Trim(Copy(s, 1, 10)) <> '');
                Reference := EftpReference;
                if (CurrentTaxName = '940')
                or (CurrentTaxName = '943')
                or (CurrentTaxName = '944')
                or (CurrentTaxName = '945') then // always December
                  PayTaxesExt.EFTPS.AddPayment(CO['TAX_SB_BANK_ACCOUNT_NBR'], CO['FEIN'], CO['PIN_NUMBER'], CurrentTaxName,
                    CO_FED_TAX_LIABILITIES['DUE_DATE'],
                    EncodeDate(StrToInt(Copy(s, 7, 4)), 12, 31),
                    TotalAmount, Reference)
                else
                  PayTaxesExt.EFTPS.AddPayment(CO['TAX_SB_BANK_ACCOUNT_NBR'], CO['FEIN'], CO['PIN_NUMBER'], CurrentTaxName,
                    CO_FED_TAX_LIABILITIES['DUE_DATE'],
                    EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2))),
                    TotalAmount, Reference);
              end;
            ttState:
              with DM_COMPANY do
              begin
                if DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_SY_REPORTS_NBR') <> '' then
                  NewTaxDepositStatus := StrToChar(DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_AFTER_STATUS'))
                else
                  if (CO_STATES['STATE'] = 'NY') or (CO_STATES['STATE'] = 'MA') or (CO_STATES['STATE'] = 'CA') then
                    NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID
                  else
                    raise ETaxDepositException.CreateHelp('EFTP doesn''t support ' + TaxTableName(TaxType) + ' tax deposit for ' + CO_STATES['STATE'], IDH_CanNotPerformOperation);
                Reference := 'Debit ' + DateToStr(Date) + ' ' + DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'AGENCY_NAME');
              end;
            ttLoc:
              with DM_COMPANY do
              begin
                if DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_SY_REPORTS_NBR') <> '' then
                  NewTaxDepositStatus := StrToChar(DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_AFTER_STATUS'))
                else
                  if CO_STATES['STATE'] = 'NY' then
                    NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID
                  else
                    raise ETaxDepositException.CreateHelp('EFTP doesn''t support ' + TaxTableName(TaxType) + ' tax deposit for ' + CO_STATES['STATE'], IDH_CanNotPerformOperation);
                Reference := 'Debit ' + DateToStr(Date) + ' ' + DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'AGENCY_NAME');
              end;
            ttSUI:
              with DM_COMPANY do
              begin
                if DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_SY_REPORTS_NBR') <> '' then
                  NewTaxDepositStatus := StrToChar(DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_AFTER_STATUS'))
                else
                  raise ETaxDepositException.CreateHelp('EFTP doesn''t support ' + TaxTableName(TaxType) + ' tax deposit for ' + CO_STATES['STATE'], IDH_CanNotPerformOperation);
                Reference := 'Debit ' + DateToStr(Date) + ' ' + DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'AGENCY_NAME');
              end;
            else
              raise ETaxDepositException.CreateHelp('EFTP doesn''t support ' + TaxTableName(TaxType) + ' tax deposit', IDH_CanNotPerformOperation);
          end;
        end;
      TAX_PAYMENT_METHOD_CREDIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT:
        begin
          if not SbSendsTaxPaymentsForCompany then
            raise ETaxDepositException.CreateHelp('Company without tax service can''t use EFTP deposit method', IDH_CanNotPerformOperation);
          Pr_Nbr := Null;
          Reference := 'ACH ' + DateToStr(Date);
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID;
        end;
      TAX_PAYMENT_METHOD_NOTICES:
        begin
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE; //TAX_DEPOSIT_STATUS_SEND_NOTICE;
          Reference := 'Notice';
          Pr_Nbr := Null;
        end;
      TAX_PAYMENT_METHOD_NOTICES_CHECKS,
        TAX_PAYMENT_METHOD_CHECK:
        begin
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PENDING;
          if NewTaxDepositId = -1 then
          begin
            if TaxPaymentMethod = TAX_PAYMENT_METHOD_NOTICES_CHECKS then
               Reference := 'Notice - Check# unknown ' + DateTimeToStr(Now)
            else
               Reference := 'Check# unknown ' + DateTimeToStr(Now);

            Pr_Nbr := -1;
            if (DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CHECK_ONE_PAYMENT_TYPE') = GROUP_BOX_YES) and
                FullTaxService and (DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger <> 0) and
                  Assigned(SuperCheckStates) and (SuperCheckStates.IndexOf(DM_COMPANY.CO_STATES['STATE']) > -1) then
              begin
                Reference := 'Super Chk ' + DateTimeToStr(Now);
                NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID;
              end
            else
              Pr_Nbr := GetPrNbr;

            if TaxType = ttFed then
              CheckGlobal_Agency_Nbr := -1;
          end;
        end;
      else
        raise ETaxDepositException.CreateHelp('Unknown ' + TaxTableName(TaxType) + ' tax payment for CO_NBR=' + IntToStr(CO_NBR), IDH_InconsistentData);
    end;

    bNewTaxDepositId := False;
    if NewTaxDepositId = -1 then
    begin
      NewTaxDepositId := CreateNewTaxDeposit(MainCoNbr, SbTaxPaymentNbr,Reference, TAX_DEPOSIT_TYPE, NewTaxDepositStatus, Pr_Nbr, Global_Agency_Nbr, FieldOfficeNbr);
      cdPendingDeposits.Append;
      cdPendingDeposits['PAY_METHOD;AGENCY_NBR;OFFICE_NBR;EIN;KEY;COUPON_NBR'] := VarArrayOf([TAX_DEPOSIT_TYPE, Global_Agency_Nbr, FieldOfficeNbr, tmpEIN, sKey, CouponReportNbr]);
      cdPendingDeposits['DEPOSIT_NBR'] := NewTaxDepositId;
      cdPendingDeposits.Post;
      bNewTaxDepositId := True;
    end;

    Result := NewTaxDepositId;
    case TaxPaymentMethod of
      TAX_PAYMENT_METHOD_NOTICES_CHECKS,
        TAX_PAYMENT_METHOD_CHECK:
        begin
            if  (DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CHECK_ONE_PAYMENT_TYPE') =  GROUP_BOX_YES) and
                (TaxType <> ttFed) and FullTaxService and
                  (DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger <> 0) and
                    Assigned(SuperCheckStates) and
                     (SuperCheckStates.IndexOf(DM_COMPANY.CO_STATES['STATE']) > -1)  then
            begin
               PayTaxesExt.OneMiscCheck.AddPayment(
                           Global_Agency_Nbr, ctx_DataAccess.ClientID,  MainCoNbr,
                           NewTaxDepositId, -1, FieldOfficeNbr, ds['DUE_DATE'],
                           TotalAmount, sKey);
            end
            else
            begin
              if bNewTaxDepositId then
                CreateMiscCheck(Pr_Nbr, MISC_CHECK_TYPE_TAX , TotalAmount, ctx_PayrollCalculation.GetNextPaymentSerialNbr, '', IntToStr(NewTaxDepositId), CheckGlobal_Agency_Nbr, FieldOfficeNbr, CouponReportNbr)
              else
                UpdateMiscCheck(NewTaxDepositId, TotalAmount);
            end;
        end;

      TAX_PAYMENT_METHOD_DEBIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
        begin
          if TaxType in [ttSui] then
          begin
            if DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_SY_REPORTS_NBR') <> '' then
                   PayTaxesExt.UnivDebitFile.AddPayment(Global_Agency_Nbr, ctx_DataAccess.ClientID, NewTaxDepositId, MainCoNbr,
                                                   DM_COMPANY.CO_SUI_LIABILITIES.DUE_DATE.AsDateTime, Amount1)
            else
              Assert(False);
          end
          else
          if TaxType in [ttState, ttLoc] then
          begin
             with DM_COMPANY do
             begin
                if CO_STATES['STATE'] = 'CA' then
                begin
                  s := ExtractFromFiller(CO_STATE_TAX_LIABILITIES.FILLER.AsString, 12, 8);
                  s := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.ShadowDataSet.CachedLookup(StrToIntDef(s, 0), 'FREQUENCY_TYPE');
                  if s = FREQUENCY_TYPE_FOLLOW_FED then
                    s := CO.FEDERAL_TAX_DEPOSIT_FREQUENCY.AsString;
                  if s = '' then
                    s := FREQUENCY_TYPE_QUARTERLY;

                  tmpCADate:= getCALastCheckDate;
                  case s[1] of
                    FREQUENCY_TYPE_QUARTERLY:
                     begin
                      s := '01104';
                      tmpCADate:= CO_STATE_TAX_LIABILITIES.PERIOD_END_DATE.AsDateTime;
                     end;
                    FREQUENCY_TYPE_MONTHLY:
                      s := '01101';
                    FREQUENCY_TYPE_DAILY:
                      s := '01102';
                    FREQUENCY_TYPE_SEMI_WEEKLY:
                      s := '01100';
                    FREQUENCY_TYPE_CHECK_DATE:
                      s := '01102';
                    else
                      Assert(False, 'Invalid Frequency Type: ' + s[1]);
                  end;
                  AddToBankAccountRegister(ds, CO['CO_NBR'], CO['TAX_SB_BANK_ACCOUNT_NBR'], NewTaxDepositId, CO_STATE_TAX_LIABILITIES['DUE_DATE'], Amount1 + Amount2, BANK_REGISTER_TYPE_EFTPayment);
                  PayTaxesExt.CaDebitFile.AddPayment(tmpCADate,
                    CO_STATE_TAX_LIABILITIES.DUE_DATE.AsDateTime,
                    ConvertNull(CO_STATES['STATE_EFT_EIN'], CO_STATES.STATE_EIN.Value),
                    CO_STATES.STATE_EFT_PIN_NUMBER.AsString, s, Amount1, Amount2);
                end
                else
                if DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(Global_Agency_Nbr, 'CUSTOM_DEBIT_SY_REPORTS_NBR') <> '' then
                begin
                      if TaxType = ttState then
                        PayTaxesExt.UnivDebitFile.AddPayment(Global_Agency_Nbr, ctx_DataAccess.ClientID, NewTaxDepositId, MainCoNbr,
                                                                  CO_STATE_TAX_LIABILITIES.DUE_DATE.AsDateTime, Amount1)
                      else
                      if TaxType = ttLoc then
                        PayTaxesExt.UnivDebitFile.AddPayment(Global_Agency_Nbr, ctx_DataAccess.ClientID, NewTaxDepositId, MainCoNbr,
                                             CO_LOCAL_TAX_LIABILITIES.DUE_DATE.AsDateTime, Amount1)
                      else
                        Assert(False);

                      if CO_STATES['STATE'] = 'NY' then
                          SetNyDebit;
                end
                else
                if CO_STATES['STATE'] = 'MA' then
                begin
                          Assert(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.FieldByName('FREQUENCY_TYPE').AsString <> '');
                          dPeriodEnd := ConvertNull(CO_STATE_TAX_LIABILITIES['CHECK_DATE'], Date);
                          case DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.FieldByName('FREQUENCY_TYPE').AsString[1] of
                            FREQUENCY_TYPE_QUAD_MONTHLY, FREQUENCY_TYPE_QUAD_MONTHLY_IL_MA:
                              begin
                                s := 'D';
                                dPeriodEnd := GetEndQuarter(dPeriodEnd);
                              end;
                            FREQUENCY_TYPE_MONTHLY:
                              begin
                                s := 'M';
                                dPeriodEnd := GetEndMonth(dPeriodEnd);
                              end;
                            FREQUENCY_TYPE_QUARTERLY:
                              begin
                                s := 'Q';
                                dPeriodEnd := GetEndQuarter(dPeriodEnd);
                              end;
                            FREQUENCY_TYPE_ANNUAL:
                              begin
                                s := 'A';
                                dPeriodEnd := GetEndYear(dPeriodEnd);
                              end;
                            else
                              Assert(False);
                          end;

                          if CompanyRequiresTaxesBeImpounded then
                          begin
                            AddToBankAccountRegister(ds, CO['CO_NBR'], CO['TAX_SB_BANK_ACCOUNT_NBR'], NewTaxDepositId, CO_STATE_TAX_LIABILITIES['DUE_DATE'], Amount1, BANK_REGISTER_TYPE_EFTPayment);
                            PayTaxesExt.MaDebitFile.AddPayment(s[1], dPeriodEnd,
                              sEIN, ConvertNull(CO['NAME'], ''), Amount1, ConvertNull(CO_STATE_TAX_LIABILITIES['DUE_DATE'], Date + 1) - 1,
                              CO.TAX_SB_BANK_ACCOUNT_NBR.AsInteger, -1);
                          end
                          else
                          begin
                            AddToBankAccountRegister(ds, CO['CO_NBR'], CO['TAX_CL_BANK_ACCOUNT_NBR'], NewTaxDepositId, CO_STATE_TAX_LIABILITIES['DUE_DATE'], Amount1, BANK_REGISTER_TYPE_EFTPayment);
                            PayTaxesExt.MaDebitFile.AddPayment(s[1], dPeriodEnd,
                              sEIN, ConvertNull(CO['NAME'], ''), Amount1, ConvertNull(CO_STATE_TAX_LIABILITIES['DUE_DATE'], Date + 1) - 1,
                              -1, CO.TAX_CL_BANK_ACCOUNT_NBR.AsInteger);
                          end;
                end
                else
                if CO_STATES['STATE'] = 'NY' then
                   SetNyDebit;
             end;
          end;
        end;

      TAX_PAYMENT_METHOD_CREDIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT:
        begin
          with DM_COMPANY do
            if not CO_TAX_PAYMENT_ACH.Locate('CO_TAX_DEPOSITS_NBR', NewTaxDepositId, []) then
            begin
              case TaxType of
                ttFed:
                  begin
                    s := CO_FED_TAX_LIABILITIES['ACH_KEY'];
                    if not TryEncodeDate(StrToIntDef(Copy(s, 7, 4), 0), StrToIntDef(Copy(s, 1, 2), 0), StrToIntDef(Copy(s, 4, 2), 0), lAchDate) then
                      raise ETaxDepositException.CreateFmtHelp('Invalid ach key value "%s"', [s], IDH_CanNotPerformOperation);
                    Assert(Trim(Copy(s, 1, 11)) <> '');
                    Filler := PutIntoFiller(Filler, DateToStr(CO_FED_TAX_LIABILITIES['DUE_DATE']), 1, 10);
                    if (CurrentTaxName = '943') or (CurrentTaxName = '944') then
                      Filler := PutIntoFiller(Filler, DateToStr(EncodeDate(StrToInt(Copy(s, 7, 4)), 12, 1)), 11, 10)
                    else
                      Filler := PutIntoFiller(Filler, DateToStr(EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), 1)), 11, 10);
                    Filler := PutIntoFiller(Filler, 'F', 21, 1);
                    Filler := PutIntoFiller(Filler, IntToStr(CO['CO_NBR']), 22, 15);
                    Filler := PutIntoFiller(Filler, CurrentTaxName, 37, 20);
                  end;
                ttState:
                  begin
                    s := CO_STATE_TAX_LIABILITIES['ACH_KEY'];
                    if not TryEncodeDate(StrToIntDef(Copy(s, 7, 4), 0), StrToIntDef(Copy(s, 1, 2), 0), StrToIntDef(Copy(s, 4, 2), 0), lAchDate) then
                      raise ETaxDepositException.CreateFmtHelp('Invalid ach key value "%s"', [s], IDH_CanNotPerformOperation);
                    Assert(Trim(Copy(s, 1, 11)) <> '');
                    Filler := PutIntoFiller(Filler, DateToStr(CO_STATE_TAX_LIABILITIES['DUE_DATE']), 1, 10);
                    Filler := PutIntoFiller(Filler, DateToStr(EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2)))), 11, 10);
                    Filler := PutIntoFiller(Filler, 'S', 21, 1);
                    if MainCoStateNbr <> - 1 then
                      Filler := PutIntoFiller(Filler, IntToStr(MainCoStateNbr), 22, 15)
                    else
                      Filler := PutIntoFiller(Filler, IntToStr(CO_STATES['CO_STATES_NBR']), 22, 15);
                    Filler := PutIntoFiller(Filler, CurrentTaxName, 37, 20);
                    Filler := PutIntoFiller(Filler, Copy(s, 11, 1), 57, 1);
                  end;
                ttSui:
                  begin
                    Assert(CO_SUI.Locate('CO_SUI_NBR', CO_SUI_LIABILITIES['CO_SUI_NBR'], []));
                    s := CO_SUI_LIABILITIES['ACH_KEY'];
                    if not TryEncodeDate(StrToIntDef(Copy(s, 7, 4), 0), StrToIntDef(Copy(s, 1, 2), 0), StrToIntDef(Copy(s, 4, 2), 0), lAchDate) then
                      raise ETaxDepositException.CreateFmtHelp('Invalid ach key value "%s"', [s], IDH_CanNotPerformOperation);
                    Assert(Trim(Copy(s, 1, 11)) <> '');
                    Filler := PutIntoFiller(Filler, DateToStr(CO_SUI_LIABILITIES['DUE_DATE']), 1, 10);
                    Filler := PutIntoFiller(Filler, DateToStr(EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2)))), 11, 10);
                    Filler := PutIntoFiller(Filler, 'U', 21, 1);
                    Filler := PutIntoFiller(Filler, IntToStr(CO_SUI['CO_SUI_NBR']), 22, 15);
                    Filler := PutIntoFiller(Filler, CurrentTaxName, 37, 20);
                    Filler := PutIntoFiller(Filler, Copy(s, 11, 1), 57, 1);
                  end;
                ttLoc:
                  begin
                    s := CO_LOCAL_TAX_LIABILITIES['ACH_KEY'];
                    if not TryEncodeDate(StrToIntDef(Copy(s, 7, 4), 0), StrToIntDef(Copy(s, 1, 2), 0), StrToIntDef(Copy(s, 4, 2), 0), lAchDate) then
                      raise ETaxDepositException.CreateFmtHelp('Invalid ach key value "%s"', [s], IDH_CanNotPerformOperation);
                    Assert(Trim(Copy(s, 1, 11)) <> '');
                    Filler := PutIntoFiller(Filler, DateToStr(CO_LOCAL_TAX_LIABILITIES['DUE_DATE']), 1, 10);
                    if DM_COMPANY.CO_LOCAL_TAX.SY_LOCALS_NBR.AsInteger  = 5724 then // NY Mobility tax
                    begin
                      CO_LOCAL_TAX_LIABILITIES.First;
                      dMaxCheckDate := CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime;
                      while not CO_LOCAL_TAX_LIABILITIES.Eof do
                      begin
                        if CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime > dMaxCheckDate then
                          dMaxCheckDate := CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime;
                        CO_LOCAL_TAX_LIABILITIES.Next;
                      end;
                      Filler := PutIntoFiller(Filler, DateToStr(dMaxCheckDate), 11, 10);
                    end
                    else
                      Filler := PutIntoFiller(Filler, DateToStr(EncodeDate(StrToInt(Copy(s, 7, 4)), StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 4, 2)))), 11, 10);
                    Filler := PutIntoFiller(Filler, 'L', 21, 1);
                    Filler := PutIntoFiller(Filler, IntToStr(CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']), 22, 15);
                    Filler := PutIntoFiller(Filler, CurrentTaxName, 37, 20);
                    Filler := PutIntoFiller(Filler, Copy(s, 11, 1), 57, 1);
                  end;
                else
                  raise ETaxDepositException.CreateHelp('EFTP doesn''t support ' + TaxTableName(TaxType) + ' tax deposit', IDH_CanNotPerformOperation);
              end;

              CO_TAX_PAYMENT_ACH.Append;
              CO_TAX_PAYMENT_ACH['ACH_DATE'] := Date;
              CO_TAX_PAYMENT_ACH['Amount'] := Amount1;
              CO_TAX_PAYMENT_ACH['Status'] := COMMON_REPORT_STATUS__COMPLETED;
              CO_TAX_PAYMENT_ACH['Status_Date'] := SysTime;
              CO_TAX_PAYMENT_ACH['CO_NBR'] := CO_NBR;
              CO_TAX_PAYMENT_ACH['CO_TAX_DEPOSITS_NBR'] := NewTaxDepositId;
              Filler := PutIntoFiller(Filler, IntToStr(Global_Agency_Nbr), 58, 15);
              CO_TAX_PAYMENT_ACH['FILLER'] := Filler;
              CO_TAX_PAYMENT_ACH.Post;
              SetLength(FPendingAchRecords, Length(FPendingAchRecords) + 1);
              with FPendingAchRecords[High(FPendingAchRecords)] do
              begin
                RecNo := CO_TAX_PAYMENT_ACH.RecNo;
                v := Amount2;
              end;
            end
            else
            begin
              CO_TAX_PAYMENT_ACH.Edit;
              CO_TAX_PAYMENT_ACH['Amount'] := CO_TAX_PAYMENT_ACH['Amount'] + Amount1;
              CO_TAX_PAYMENT_ACH.Post;
              if TaxType <> ttFed then
                with FPendingAchRecords[FindAchRecordIndex(CO_TAX_PAYMENT_ACH.RecNo)] do
                  if VarIsArray(Amount2) then
                  begin
                    if VarIsNull(v) then
                      v := Amount2
                    else
                    begin
                      Assert(VarArrayLowBound(Amount2, 1) = VarArrayLowBound(v, 1));
                      Assert(VarArrayHighBound(Amount2, 1) = VarArrayHighBound(v, 1));
                      for j := VarArrayLowBound(Amount2, 1) to VarArrayHighBound(Amount2, 1) do
                        if not VarIsNull(Amount2[j]) then
                          v[j] := ConvertNull(v[j], 0) + ConvertNull(Amount2[j], 0);
                    end;
                  end
                  else
                  begin
                    if not VarIsNull(Amount2) then
                      v := ConvertNull(v, 0) + ConvertNull(Amount2, 0);
                  end;
            end;
        end;

    end;
  end;

  procedure UpdateLiability(const ds: TevClientDataSet; const NewTaxDepositId: Integer; const NewTaxDepositStatus: Char; const OverrideFilter: string = '-');
  var
    d: TevClientDataSet;
    lDepositStatus: Char;
  begin
    d := TevClientDataSet.Create(nil);
    with d do
    try
      CloneCursor(ds, True);
      if OverrideFilter <> '-' then
        Filter := OverrideFilter
      else
        Filter := ds.Filter;
      Filtered := True;
      First;
      while not Eof do
      begin
        if ((OverrideFilter = '-') and VarIsNull(FieldValues['CO_TAX_DEPOSITS_NBR']))
          or ((OverrideFilter <> '-') and not VarIsNull(FieldValues['CO_TAX_DEPOSITS_NBR']) and (FieldValues['CO_TAX_DEPOSITS_NBR'] = -1)) then
        begin
          Edit;
          if NewTaxDepositStatus <> '*' then
          begin
            FieldByName('PREV_STATUS').AsString := FieldByName('STATUS').AsString ;

            if (CompanyRequiresTaxesBeImpounded and not (FieldByName('STATUS').AsString[1] in [TAX_DEPOSIT_STATUS_IMPOUNDED, TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED])) then
            begin
              lDepositStatus := NewTaxDepositStatus;
              if lDepositStatus in [TAX_DEPOSIT_STATUS_PAID, TAX_DEPOSIT_STATUS_SEND_NOTICE] then
                lDepositStatus := TAX_DEPOSIT_STATUS_PAID_WO_FUNDS;
              FieldByName('STATUS').AsString := lDepositStatus;
            end
            else
            begin
              FieldByName('STATUS').AsString := NewTaxDepositStatus;
            end;
          end;
          FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger := NewTaxDepositId;
          Post;
        end;
        Next;
      end;
    finally
      Free;
    end;
  end;

  procedure UpdateLiabilitiesWithDummyDepNumber(const ds: TevClientDataSet);
  begin
    UpdateLiability(ds, -1, '*'); // will populate with real deposits_nbr later
  end;

  procedure ProcessLiabilities(const ds: TevClientDataSet; const TaxTypeTable: TTaxTable;
    DepositMethod: Char; const DepositAgencyNbr, FieldOfficeNbr, CouponReportNbr: Variant;
    const CurrentRootFilter, sEIN, sKey: string; const stcdEIN : string = '');

    function CreditTotal: Currency;
    begin
      Result := GetTaxLiabilities(ds, CurrentRootFilter + ' and (CO_TAX_DEPOSITS_NBR is null or CO_TAX_DEPOSITS_NBR = -1) and DUE_DATE <= ' + QuotedStr(DateToStr(ds['DUE_DATE'])) + {' and ACH_KEY='+ QuotedStr(ds['ACH_KEY'])+} ' and ADJUSTMENT_TYPE=' + QuotedStr(TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT), True);
    end;

    function PenaltyTotal: Currency;
    begin
      Result := GetTaxLiabilities(ds, CurrentRootFilter + ' and (CO_TAX_DEPOSITS_NBR is null or CO_TAX_DEPOSITS_NBR = -1) and DUE_DATE <= ' + QuotedStr(DateToStr(ds['DUE_DATE'])) + {' and ACH_KEY='+ QuotedStr(ds['ACH_KEY'])+} ' and ADJUSTMENT_TYPE=' + QuotedStr(TAX_LIABILITY_ADJUSTMENT_TYPE_PENALTY), True);
    end;

    function InterestTotal: Currency;
    begin
      Result := GetTaxLiabilities(ds, CurrentRootFilter + ' and (CO_TAX_DEPOSITS_NBR is null or CO_TAX_DEPOSITS_NBR = -1) and DUE_DATE <= ' + QuotedStr(DateToStr(ds['DUE_DATE'])) + {' and ACH_KEY='+ QuotedStr(ds['ACH_KEY'])+} ' and ADJUSTMENT_TYPE=' + QuotedStr(TAX_LIABILITY_ADJUSTMENT_TYPE_INTEREST), True);
    end;

    function MoCreditTotal: Currency;
    begin
      Result := GetTaxLiabilities(ds, CurrentRootFilter + ' and (CO_TAX_DEPOSITS_NBR is null or CO_TAX_DEPOSITS_NBR = -1) and DUE_DATE <= ' + QuotedStr(DateToStr(ds['DUE_DATE'])) + {' and ACH_KEY='+ QuotedStr(ds['ACH_KEY'])+} ' and ADJUSTMENT_TYPE=' + QuotedStr(TAX_LIABILITY_ADJUSTMENT_TYPE_MISSOURI_ER_COMP), True);
    end;

    function SdiTotal: Currency;
    begin
      Result := GetTaxLiabilities(ds, '(TAX_TYPE=' + QuotedStr(TAX_LIABILITY_TYPE_EE_SDI) +
        ' or TAX_TYPE=' + QuotedStr(TAX_LIABILITY_TYPE_ER_SDI) + ')');
    end;

    function CoSUIFilteredTotal(const FilterString: string): Currency;
    var
      s: string;
    begin
      s := '-1';
      with TevCLientDataSet.Create(nil) do
      try
        CloneCursor(DM_COMPANY.CO_SUI, True);
        Filter := FilterString;
        Filtered := True;
        First;
        while not Eof do
        begin
          s := s + ',' + IntToStr(FieldValues['CO_SUI_NBR']);
          Next;
        end;
      finally
        Free;
      end;
      Result := GetTaxLiabilities(ds, 'CO_SUI_NBR in (' + s + ')');
    end;

    function CoLocalsFilteredTotal(const FilterString: string): Currency;
    var
      s: string;
    begin
      s := '-1';
      with TevCLientDataSet.Create(nil) do
      try
        CloneCursor(DM_COMPANY.CO_LOCAL_TAX, True);
        Filter := FilterString;
        Filtered := True;
        First;
        while not Eof do
        begin
          s := s + ',' + IntToStr(FieldValues['CO_LOCAL_TAX_NBR']);
          Next;
        end;
      finally
        Free;
      end;
      Result := GetTaxLiabilities(ds, 'CO_LOCAL_TAX_NBR in (' + s + ')');
    end;

    function NyNycTotal: Currency;
    begin
      Result := CoLocalsFilteredTotal('SY_LOCALS_NBR in (215,217)');
    end;

    function NyYonkerTotal: Currency;
    begin
      Result := CoLocalsFilteredTotal('SY_LOCALS_NBR in (218,216)');
    end;

  var
    NewTaxDepositId: Integer;
    NewTaxDepositStatus: Char;
    TotalAmount, Amount1: Currency;
    Amount2: Variant;
    sFilter : String;
  begin
    if ForceChecks then
      DepositMethod := TAX_PAYMENT_METHOD_CHECK;
    Amount1 := GetTaxLiabilities(ds);
    if Amount1 > 0 then
    begin
      if (Amount1 + NegativeWaitingTotal) > 0 then
      begin
        Amount1 := Amount1 + NegativeWaitingTotal;
        TotalAmount := Amount1;
        NegativeWaitingTotal := 0;
        if (DepositAgencyNbr = 21) and (FieldOfficeNbr = 76)
          and (ds = DM_COMPANY.CO_SUI_LIABILITIES) then // ME quarterly payment
        begin
          Amount2 := VarArrayOf([Amount1, DM_COMPANY.CO_SUI['CO_SUI_NBR']]);
          Amount1 := 0;
        end
        else
          if (ds = DM_COMPANY.CO_SUI_LIABILITIES) and
            (DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'NJ') then
          begin
            Amount2 := Amount1;
            Amount1 := 0;
          end
          else

            if (ds = DM_COMPANY.CO_STATE_TAX_LIABILITIES) and
              (DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'CO') then
            begin
              Amount2 := VarArrayOf([PenaltyTotal, InterestTotal]);
              Amount1 := Amount1 - Amount2[0] - Amount2[1];
            end
            else
            if (ds = DM_COMPANY.CO_STATE_TAX_LIABILITIES) and
              (DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'DC') then
            begin
              Amount2 := VarArrayOf([PenaltyTotal, InterestTotal]);
              Amount1 := Amount1 - Amount2[0] - Amount2[1];
            end
            else
              if (ds = DM_COMPANY.CO_STATE_TAX_LIABILITIES) and
                (DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'MO') then
              begin
                Amount2 := -MoCreditTotal;
                Amount1 := Amount1 + Amount2;
              end
              else
                if (DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'CA') then
                begin
                  if (ds = DM_COMPANY.CO_STATE_TAX_LIABILITIES) then
                  begin
                    Amount2 := SdiTotal;
                    Amount1 := Amount1 - Amount2;
                  end
                  else
                    if (ds = DM_COMPANY.CO_SUI_LIABILITIES) or (ds = DM_COMPANY.CO_LOCAL_TAX_LIABILITIES) then
                      Amount2 := 0
                end
                else
                  if DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'NY' then
                  begin
                    if ds = DM_COMPANY.CO_LOCAL_TAX_LIABILITIES then
                    begin
                      Amount2 := VarArrayOf([NyNycTotal, NyYonkerTotal]);
                      Amount1 := Amount1 - Amount2[0] - Amount2[1];
                    end
                    else
                      Amount2 := VarArrayOf([0, 0]);
                  end
                  else
                  if DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DepositAgencyNbr, 'STATE') = 'OR' then
                  begin
                    if ds = DM_COMPANY.CO_LOCAL_TAX_LIABILITIES then
                    begin
                      Amount2 := CoLocalsFilteredTotal('SY_LOCALS_NBR=969'); // Lane County Transit Tax
                      Amount1 := Amount1 - Amount2; // TriMet Transit Tax
                    end
                    else
                    if ds = DM_COMPANY.CO_SUI_LIABILITIES then
                    begin
                      Amount2 := CoSUIFilteredTotal('SY_SUI_NBR in (96,97)'); // Workers' Benefit Fund EE and ER
                      Amount1 := Amount1 - Amount2; // OR-SUI and OR-JOBS Plus Wage Fund
                    end;
                  end
                  else
                  if (ds = DM_COMPANY.CO_LOCAL_TAX_LIABILITIES)
                    and ((DepositAgencyNbr = 211) or (DepositAgencyNbr = 405)
                    or (DepositAgencyNbr = 208) or (DepositAgencyNbr = 566) {Lancaster agencies}
                    or (DepositAgencyNbr = 2121) {MI Albion Income Tax Division}
                    or (DepositAgencyNbr = 217) {Dayton agency}) then
                  begin
                    Amount2 := VarArrayOf([CreditTotal, PenaltyTotal, InterestTotal]);
                  end
                  else
                    Amount2 := Null;
        NewTaxDepositId := ConvertTaxPayment(ds, TaxTypeTable, DepositMethod, TotalAmount, Amount1, Amount2, NewTaxDepositStatus, ConvertNull(DepositAgencyNbr, -1), ConvertNull(FieldOfficeNbr, -1), ConvertNull(CouponReportNbr, -1), sEIN, sKey, stcdEIN);
        UpdateLiability(ds, NewTaxDepositId, NewTaxDepositStatus);
        UpdateLiability(ds, NewTaxDepositId, NewTaxDepositStatus, CurrentRootFilter);
      end
      else
        if (Amount1 + NegativeWaitingTotal) = 0 then
        begin
          NegativeWaitingTotal := 0;
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_SEND_NOTICE;
          NewTaxDepositId := CreateNewTaxDeposit(MainCoNbr, SbTaxPaymentNbr,'Zero sum deposit', TAX_DEPOSIT_TYPE_NOTICE, NewTaxDepositStatus, Null, -1, -1);
          UpdateLiability(ds, NewTaxDepositId, NewTaxDepositStatus);
          UpdateLiability(ds, NewTaxDepositId, NewTaxDepositStatus, CurrentRootFilter);
        end
        else
        begin
          NegativeWaitingTotal := NegativeWaitingTotal + Amount1;
          UpdateLiabilitiesWithDummyDepNumber(ds);
        end;
    end
    else
      if (Amount1 = 0) and (ds.RecordCount > 0) then
      begin
        sFilter := ds.Filter;
        ds.Filter := ds.Filter + ' and status = ''I''';
        if (ds.RecordCount > 0) then
        begin
           NewTaxDepositId := CreateNewTaxDeposit(MainCoNbr, SbTaxPaymentNbr,'Zero sum deposit', TAX_DEPOSIT_TYPE_NOTICE, TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE, Null, -1, -1);
           ds.Filter := ds.Name+'_NBR in (' + ds.GetFieldValueList(ds.Name+'_NBR',True).CommaText + ')';
           UpdateLiability(ds, NewTaxDepositId, TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE);
        end
        else
           NewTaxDepositId := CreateNewTaxDeposit(MainCoNbr, SbTaxPaymentNbr,'Zero sum deposit', TAX_DEPOSIT_TYPE_NOTICE, TAX_DEPOSIT_STATUS_PAID_WO_FUNDS, Null, -1, -1);

        ds.Filter := sFilter + ' and status <> ''I''';
        if (ds.RecordCount > 0) then
        begin
          ds.Filter := ds.Name+'_NBR in (' + ds.GetFieldValueList(ds.Name+'_NBR',True).CommaText + ')';
          UpdateLiability(ds, NewTaxDepositId, TAX_DEPOSIT_STATUS_PAID_WO_FUNDS);
        end;
        ds.Filter := sFilter;
      end
      else
        if Amount1 < 0 then
        begin // do nothing
        end;
  end;

  function ExtractPrNbr(const s: string): Integer;
  begin
    Result := StrToInt(Copy(s, Pos('=', s) + 1, 255));
  end;

  function GetNegativeWaitingTotal(const ds: TevClientDataSet; const dsFilter: string; const ListFieldNames: array of string): Currency;
    procedure DoStep(const Level: Integer; const Filter: string);
    var
      i: Integer;
      Amount: Currency;
      List: TStringList;
    begin
      PrepareTable(ds, Filter);
      if Level > High(ListFieldNames) then
      begin
        Amount := GetTaxLiabilities(ds);
        if Amount < 0 then
        begin
          Result := Result + Amount;
          UpdateLiabilitiesWithDummyDepNumber(ds);
        end;
      end
      else
      begin
        List := GetFieldValueList(ds, ListFieldNames[Level]);
        try
          for i := 0 to List.Count - 1 do
            DoStep(Level + 1, Filter + ' and ' + ListFieldNames[Level] + '=' + QuotedStr(List[i]));
        finally
          List.Free;
        end;
      end;
    end;
  begin
    Result := 0;
    if ds.RecordCount > 0 then
    begin
      DoStep(0, dsFilter);
      PrepareTable(ds, dsFilter);
    end;
  end;
  procedure SortDateList(const l: TStringList);
  var
    i: Integer;
    s, sDate: string;
  begin
    s := l.Text;
    l.Clear;
    l.Sorted := False;
    while s <> '' do
    begin
      sDate := GetNextStrValue(s, #13#10);
      for i := 0 to l.Count do
        if i > l.Count - 1 then
          l.Append(sDate)
        else
          if StrToDateTime(sDate) < StrToDateTime(l[i]) then
          begin
            l.Insert(i, sDate);
            Break;
          end;
    end;
  end;
  procedure SetMainCoNbr(const cd: TevClientDataset);
  var
    d: TevClientDataset;
    i: Integer;
    r: Integer;
  begin
    d := TevClientDataSet.Create(nil);
    try
      d.CloneCursor(DM_COMPANY.CO, True);
      cd.First;
      with GetFieldValueList(cd, 'CO_NBR') do
      try
        if Count > 0 then
        begin
          r := StrToInt(Strings[0]);
          for i := 0 to Count - 1 do
          begin
            Assert(d.Locate('CO_NBR', StrToInt(Strings[i]), []));
            if not (FedMainCoControl and ( Count = 1 )) and
               not VarIsNull(d['CL_CO_CONSOLIDATION_NBR']) and
                   DM_CLIENT.CL_CO_CONSOLIDATION.Locate('CL_CO_CONSOLIDATION_NBR', d['CL_CO_CONSOLIDATION_NBR'], []) then
             begin
               r := DM_CLIENT.CL_CO_CONSOLIDATION['PRIMARY_CO_NBR'];
               Break;
             end;
            d.Next;
          end;
          MainCoNbr := r;
        end;
      finally
        Free;
      end;
    finally
      d.Free;
    end;
  end;

  function DetermineMainCoNbrForState(const CurrentCoNbr: Integer; const CurrentStateNbr: Integer; const CurrentState: String; const SEIN: String): Integer;
  var
    DS: TExecDSWrapper;
    ConsolDS: TevClientDataSet;
    CloneCoStates: TevClientDataSet;
  begin
    //Default the MainCoNbr/Result to those of the currently selected company.
    MainCoNbr := CurrentCoNbr;
    result := CurrentStateNbr;

    Assert(DM_COMPANY.CO.Locate('CO_NBR', CurrentCoNbr, []));

    if DM_COMPANY.CO.FieldByName('cl_co_consolidation_nbr').IsNull then
      Exit;

    ConsolDS := TevClientDataSet.Create(Nil);
    ConsolDS.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;

    CloneCoStates := TevClientDataSet.Create(nil);

    try
      DS := TExecDSWrapper.Create('GetConsolidatedCompanies');
      DS.SetParam('Nbr', DM_COMPANY.CO['cl_co_consolidation_nbr']);
      ConsolDS.DataRequest(DS.AsVariant);
      ConsolDS.Open;

      ConsolDS.Filter := 'SCOPE=''' + CONSOLIDATION_SCOPE_ALL + ''' ';
      ConsolDS.Filtered := True;

      CloneCoStates.CloneCursor(DM_COMPANY.CO_STATES, True);
      //Check to see if company participates in the correct consolidation
      if (ConsolDS.RecordCount > 0) then
      begin
        //We now know that the current company participates in the correct consolidation
        //now we have to determine if the company has the correct state setup and
        //if the SEIN's match if not we default to the currently payee.
        CloneCoStates.Filter := 'CO_NBR = ' + QuotedStr(ConsolDS['PRIMARY_CO_NBR']) + ' and STATE = '''+ CurrentState + ''' and STATE_EIN = ''' + SEIN + '''';
        CloneCoStates.Filtered := True;
        if CloneCoStates.RecordCount > 0 then
        begin
          MainCoNbr := CloneCoStates['CO_NBR'];
          Result := CloneCoStates['CO_STATES_NBR'];
        end;
      end;
    finally
      ConsolDS.Free;
      CloneCoStates.Free;
    end;
  end;

  procedure ProcessPendingEftDebitRecords;
  begin
    cdPendingEftDebits.First;
    while not cdPendingEftDebits.Eof do
    begin
      Assert(DM_COMPANY.CO_TAX_DEPOSITS.Locate('CO_TAX_DEPOSITS_NBR', cdPendingEftDebits['DEPOSIT_NBR'], []));
      Assert(DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_TAX_DEPOSITS['CO_NBR'], []));
      AddToBankAccountRegister(nil, DM_COMPANY.CO['CO_NBR'], DM_COMPANY.CO['TAX_SB_BANK_ACCOUNT_NBR'], cdPendingEftDebits['DEPOSIT_NBR'], cdPendingEftDebits['DUE_DATE'],
        cdPendingEftDebits['NyStateTax'] + cdPendingEftDebits['NyCityTax'] +
        cdPendingEftDebits['CityOfYorkersTax'], BANK_REGISTER_TYPE_EFTPayment);

      DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR=' + cdPendingEftDebits.FieldByName('DEPOSIT_NBR').AsString;
      DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filtered := True;
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR=' + cdPendingEftDebits.FieldByName('DEPOSIT_NBR').AsString;
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filtered := True;

      ctx_PayrollCalculation.AddConsolidatedPaymentsToBar(
                     [DM_COMPANY.CO_STATE_TAX_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES],
                      cdPendingEftDebits.FieldByName('DEPOSIT_NBR').AsInteger,
                      BANK_REGISTER_STATUS_Outstanding, BANK_REGISTER_TYPE_Consolidated, False);

      PayTaxesExt.NyDebitFile.AddPayment(cdPendingEftDebits['EIN'], cdPendingEftDebits['LAST_CHECK_DATE'],
        cdPendingEftDebits['PAYROLLS_TOTAL'], cdPendingEftDebits['NyStateTax'],
        cdPendingEftDebits['NyCityTax'], cdPendingEftDebits['CityOfYorkersTax']);
      cdPendingEftDebits.Next;
    end;
    DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filter := '';
    DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filtered := false;
    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filter := '';
    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filtered := false;
  end;
  procedure PreProcessPostedAchRecords(const ds: TevClientDataset);
  var
    s: string;
    t1, t2, t3: Currency;
  begin
    ds.First;
    while not ds.Eof do
      with FPendingAchRecords[FindAchRecordIndex(ds.RecNo)] do
      begin
        s := ds.FieldByName('filler').AsString;
        if ExtractFromFiller(s, 21, 1) = 'F' then
        begin
          t1 := 0;
          t2 := 0;
          t3 := 0;
          DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR=' + IntToStr(ds['CO_TAX_DEPOSITS_NBR']);
          DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := True;
          try
            DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
            while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
            begin
              case string(DM_COMPANY.CO_FED_TAX_LIABILITIES['TAX_TYPE'])[1] of
                TAX_LIABILITY_TYPE_FEDERAL,
                  TAX_LIABILITY_TYPE_EE_EIC,
                  TAX_LIABILITY_TYPE_FEDERAL_945,
                  TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING,
                  TAX_LIABILITY_TYPE_COBRA_CREDIT:
                  t3 := t3 + DM_COMPANY.CO_FED_TAX_LIABILITIES['amount'];
                TAX_LIABILITY_TYPE_ER_FUI,
                  TAX_LIABILITY_TYPE_EE_OASDI,
                  TAX_LIABILITY_TYPE_ER_OASDI:
                  t1 := t1 + DM_COMPANY.CO_FED_TAX_LIABILITIES['amount'];
                TAX_LIABILITY_TYPE_EE_MEDICARE,
                  TAX_LIABILITY_TYPE_ER_MEDICARE:
                  t2 := t2 + DM_COMPANY.CO_FED_TAX_LIABILITIES['amount'];
              end;
              DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
            end;
          finally
            DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := False;
          end;
          v := VarArrayOf([t1, t2, t3]);
        end;
        ds.Next;
      end;
    DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := False;
  end;

  procedure ProcessPostedAchRecords(const ds: TevClientDataset);

    function ConvertEmptyToZero(const s: string): Char;
    begin
      if s = '' then
        Result := '0'
      else
        Result := s[1];
    end;

    function CobraCredit(pTaxDepositNbr: integer): Boolean;
    begin
      result := False;
      if (GetTaxLiabilities(DM_COMPANY.CO_FED_TAX_LIABILITIES, '(TAX_TYPE=' + QuotedStr(TAX_LIABILITY_TYPE_COBRA_CREDIT) + ' and CO_TAX_DEPOSITS_NBR = ' + IntToStr(pTaxDepositNbr) + ')', True) < 0 ) then
        Result := True;
    end;
  var
    s: string;
  begin
    ds.First;
    while not ds.Eof do begin
      with FPendingAchRecords[FindAchRecordIndex(ds.RecNo)] do
      begin
        s := ds.FieldByName('filler').AsString;
        if ExtractFromFiller(s, 21, 1) = 'F' then
        begin
          PayTaxesExt.AchPayments.ProcessEFTPSPayment(v[0],
            v[1],
            v[2],
            StrToDate(ExtractFromFiller(s, 1, 10)),
            StrToDate(ExtractFromFiller(s, 11, 10)),
            StrToInt(ExtractFromFiller(s, 22, 15)),
            ds['CO_TAX_PAYMENT_ACH_NBR'],
            ExtractFromFiller(s, 37, 20),
            CobraCredit(ds['CO_TAX_DEPOSITS_NBR']))
        end
        else
        begin
          if ExtractFromFiller(s, 21, 1) = 'S' then
          begin
            PayTaxesExt.AchPayments.ProcessEFTPayment(ds['amount'],
              v,
              StrToDate(ExtractFromFiller(s, 1, 10)),
              StrToDate(ExtractFromFiller(s, 11, 10)),
              HexCharToInt(ConvertEmptyToZero(ExtractFromFiller(s, 57, 1))),
              StrToInt(ExtractFromFiller(s, 22, 15)),
              ds['CO_TAX_PAYMENT_ACH_NBR'],
              ttState)
          end
          else
          begin
            if ExtractFromFiller(s, 21, 1) = 'U' then
            begin
              PayTaxesExt.AchPayments.ProcessEFTPayment(ds['amount'],
                v,
                StrToDate(ExtractFromFiller(s, 1, 10)),
                StrToDate(ExtractFromFiller(s, 11, 10)),
                HexCharToInt(ConvertEmptyToZero(ExtractFromFiller(s, 57, 1))),
                StrToInt(ExtractFromFiller(s, 22, 15)),
                ds['CO_TAX_PAYMENT_ACH_NBR'],
                ttSui)
            end
            else
            begin
              if ExtractFromFiller(s, 21, 1) = 'L' then
              begin
                PayTaxesExt.AchPayments.ProcessEFTPayment(ds['amount'],
                  v,
                  StrToDate(ExtractFromFiller(s, 1, 10)),
                  StrToDate(ExtractFromFiller(s, 11, 10)),
                  HexCharToInt(ConvertEmptyToZero(ExtractFromFiller(s, 57, 1))),
                  StrToInt(ExtractFromFiller(s, 22, 15)),
                  ds['CO_TAX_PAYMENT_ACH_NBR'],
                  ttLoc)
              end
              else
              begin
                Assert(False, 'Unknown ACH tax payment type');
              end;
            end;
          end;
        end;

        DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(ds.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
        DM_COMPANY.CO_FED_TAX_LIABILITIES.Filtered := True;
        DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(ds.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
        DM_COMPANY.CO_STATE_TAX_LIABILITIES.Filtered := True;
        DM_COMPANY.CO_SUI_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(ds.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
        DM_COMPANY.CO_SUI_LIABILITIES.Filtered := True;
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filter := 'CO_TAX_DEPOSITS_NBR='+QuotedStr(ds.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Filtered := True;

        ctx_PayrollCalculation.AddConsolidatedPaymentsToBar(
                     [DM_COMPANY.CO_FED_TAX_LIABILITIES,DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                       DM_COMPANY.CO_SUI_LIABILITIES,DM_COMPANY.CO_LOCAL_TAX_LIABILITIES],
                      ds.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger,
                      BANK_REGISTER_STATUS_Outstanding,
                      BANK_REGISTER_TYPE_Consolidated, False);

        ds.Next;
      end;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
  end;

  function MakeKeyFilter(const DueDate, AchKey: string): string;
  begin
    Result := DueDate + ';' + AchKey;
  end;

  function Ca_CheckStateLiabDepFreqAndNegatifAmount(ds: TEvBasicClientDataSet): Boolean;
  begin
    result := true;
    ds.First;
    while not ds.Eof do
    begin
     if ds.FieldByName('AMOUNT').AsFloat < 0 then
        Result := false;
     if (ds.FieldByName('cSY_STATE_DEPOSIT_FREQ_NBR').IsNull) or
        (trim(ds.FieldByName('cSY_STATE_DEPOSIT_FREQ_NBR').AsString) = '') then
        Result := false;
     if not Result then exit;
     ds.Next;
    end;
  end;

  function GetSuperCheckStates(const tmpCL_NBR : integer) : IisStringList;
  var
    Q: IevQuery;
  begin
    Result := TisStringList.Create;
    if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger > 0 then
    begin
      try
         ctx_DataAccess.OpenClient(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger);
         Q := TevQuery.Create('select distinct state from CO_STATES a where {AsOfNow<a>} ');
         Q.Execute;
         while not Q.Result.eof do
         begin
           Result.Add(Q.result.FieldByName('state').AsString);
           Q.Result.Next;
         end;
      finally
         ctx_DataAccess.OpenClient(tmpCL_NBR);
      end;
    end;
  end;


  procedure CheckDifferentPaymentMethod(const level:string; const filter: string;const ein:string; const msgInfo:string = '');
  var
    Q: IevQuery;
    flag, flag1, flag2, flag3, flag4 : integer;
    paymentField, msg:string;
  begin
        Q := TevQuery.Create('GenericSelectCurrentWithCondition');

        if level = 'Fed' then
        begin
          Q.Macros.AddValue('Columns', 'CO_NBR, FEIN, Federal_Tax_Payment_Method');
          Q.Macros.AddValue('TableName', 'CO');
          Q.Macros.AddValue('CONDITION', filter);
          paymentField := 'Federal_Tax_Payment_Method';
          msg := '(FEIN = ' + ein +')';
        end
        else
        if level = 'State' then
        begin
          Q.Macros.AddValue('Columns', 'CO_NBR, STATE_EIN, STATE_TAX_DEPOSIT_METHOD');
          Q.Macros.AddValue('TableName', 'CO_STATES');
          Q.Macros.AddValue('CONDITION', filter);
          paymentField := 'STATE_TAX_DEPOSIT_METHOD';
          msg := '(STATE=' + msgInfo +'; STATE_EIN = ' + ein +')';
        end
        else
        if level = 'SUI' then
        begin
          Q.Macros.AddValue('Columns', 'CO_NBR, SUI_EIN, SUI_TAX_DEPOSIT_METHOD');
          Q.Macros.AddValue('TableName', 'CO_STATES');
          Q.Macros.AddValue('CONDITION', filter);
          paymentField := 'SUI_TAX_DEPOSIT_METHOD';
          msg := '(STATE=' + msgInfo +'; SUI_EIN = ' + ein +')';
        end
        else
        if level = 'Local' then
        begin
          Q.Macros.AddValue('Columns', 'CO_NBR, LOCAL_EIN_NUMBER, PAYMENT_METHOD');
          Q.Macros.AddValue('TableName', 'CO_LOCAL_TAX');
          Q.Macros.AddValue('CONDITION', filter);
          paymentField := 'PAYMENT_METHOD';
          msg := '(LOCAL_EIN_NUMBER = ' + ein +')';
        end
        else
          exit;

        Q.Execute;
        flag1:=0; flag2:=0; flag3:=0; flag4:=0;
        Q.Result.First;
        while not Q.Result.eof do
        begin
           if not Q.result.FieldByName(paymentField).IsNull then
             case Q.result.FieldByName(paymentField).AsString[1] of
                TAX_PAYMENT_METHOD_DEBIT,
                 TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
                     flag1 := 1;
                TAX_PAYMENT_METHOD_NOTICES_CHECKS,
                 TAX_PAYMENT_METHOD_CHECK:
                     flag2 := 2;
                TAX_PAYMENT_METHOD_CREDIT,
                 TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT:
                     flag3 := 4;
                TAX_PAYMENT_METHOD_NOTICES:
                     flag4 := 8;
              end
           else
              raise ETaxDepositException.CreateHelp(level + ' level missing tax payment method. Please review setup.', IDH_CanNotPerformOperation);

           flag := flag1+flag2+flag3+flag4;
           if not (flag in [ 1, 2, 4, 8]) then
              raise ETaxDepositException.CreateHelp('"Companies in this client have same account numbers, but different payment method for '+ level + msg + '. Please review setup.', IDH_CanNotPerformOperation);

           Q.Result.Next;
        end;
  end;

var
  StateList: TStringList;
  SUIList: TStringList;
  EinList: TStringList;
  DueDateList: TStringList;
  AchKeyList: TStringList;
  StateDepFreqList : TStringList;
  EinOrPinList: string;
  AchKeyField: string;
  tmpFilter, tmpIndex : String;
  FieldOfficeKeyList, TaxCouponList: TStringList;
  LiabFilter, LiabTypeFilter, LiabDueDateFilter, SuiFilter, SuiNbrFilter, AchFilter,
  tmpTCDEIN, KeyFilter, FieldOfficeFilter, TaxCouponFilter: string;
  FilterDataSet, cdCoSui: TevClientDataSet;
  i, j, k, l, m, n, o, p: Integer;
  tmp_clnbr : integer;
  StateDepositNbr: Integer;
  FieldOfficeNbr, TaxCouponNumber: Variant;
  Log: string;
  cdTCDLocals: IevDataSet;
  isItTCDLocals : Boolean;

  DepositAgencyNbr : Integer;
  DepositPaymentMethod : Char;
begin
  Result := TTaxPaymentParams.Create;

  Result.ClNbr := ctx_DataAccess.ClientID;
  tmp_clnbr := ctx_DataAccess.ClientID;

  ctx_DataAccess.AsOfDate := 0;

  with DM_SYSTEM_LOCAL, DM_SYSTEM_MISC, DM_SYSTEM_STATE, DM_SYSTEM_FEDERAL,
    DM_COMPANY, DM_CLIENT, DM_SERVICE_BUREAU, DM_PAYROLL do
  begin
    SB.DataRequired('ALL');
    SuperCheckStates := GetSuperCheckStates(ctx_DataAccess.ClientID);

    SY_LOCALS.DataRequired('ALL');
    CL.DataRequired('ALL');
    SY_GLOBAL_AGENCY.DataRequired('ALL');
    SY_GL_AGENCY_FIELD_OFFICE.DataRequired('ALL');
    SY_FED_TAX_PAYMENT_AGENCY.DataRequired('ALL');
    SY_STATES.DataRequired('ALL');
    SY_SUI.DataRequired('ALL');
    SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');
    SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
    SY_REPORTS.DataRequired('ALL');
    SY_AGENCY_DEPOSIT_FREQ.DataRequired('ALL');
    Assert(SY_REPORTS.FindField('SY_REPORTS_GROUP_NBR') <> nil);
    SY_REPORTS.AddIndex('SY_REPORTS_GROUPNBR', 'SY_REPORTS_GROUP_NBR;ACTIVE_YEAR_FROM', []);
    SY_REPORTS.IndexDefs.Update;
    SY_REPORTS.IndexName := 'SY_REPORTS_GROUPNBR';

    SY_LOCAL_DEPOSIT_FREQ.IndexFieldNames := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
    SY_STATE_DEPOSIT_FREQ.IndexFieldNames := 'SY_STATE_DEPOSIT_FREQ_NBR';
    CO.DataRequired('ALL');
    CO_STATES.DataRequired('ALL');
    CO_SUI.DataRequired('ALL');
    CO_LOCAL_TAX.DataRequired('ALL');
    CO_TAX_DEPOSITS.DataRequired('CO_TAX_DEPOSITS_NBR = -1');
    CO_TAX_PAYMENT_ACH.Close;
    CO_TAX_PAYMENT_ACH.DataRequired('CO_TAX_PAYMENT_ACH_NBR = -1');
    CO_TAX_PAYMENT_ACH.IndexFieldNames := '';
    CO_TAX_PAYMENT_ACH.IndexName := '';
    CL_BANK_ACCOUNT.DataRequired('ALL');
    CL_CO_CONSOLIDATION.DataRequired('SCOPE=''' + CONSOLIDATION_SCOPE_ALL + ''' ');
    PR.Close;
    PR.DataRequired('PR_NBR=-1');
    PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=-1');
    CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_BANK_ACCOUNT_REGISTER_NBR=0');

    SetLength(FPendingAchRecords, 0);

    Pr_Nbr_List := TStringList.Create;
    Pr_Nbr_List.Sorted := True;

    cdPendingDeposits := TevClientDataSet.Create(nil);
    cdPendingDeposits.FieldDefs.Add('PAY_METHOD', ftString, 1, True);
    cdPendingDeposits.FieldDefs.Add('AGENCY_NBR', ftInteger, 0, True);
    cdPendingDeposits.FieldDefs.Add('OFFICE_NBR', ftInteger, 0, True);
    cdPendingDeposits.FieldDefs.Add('EIN', ftString, 128, True);
    cdPendingDeposits.FieldDefs.Add('KEY', ftString, 255, True);
    cdPendingDeposits.FieldDefs.Add('COUPON_NBR', ftInteger, 0, True);
    cdPendingDeposits.FieldDefs.Add('DEPOSIT_NBR', ftInteger, 0, True);
    cdPendingDeposits.CreateDataSet;
    cdPendingDeposits.AddIndex('1', 'PAY_METHOD;AGENCY_NBR;EIN;KEY', []);
    cdPendingDeposits.IndexDefs.Update;
    cdPendingDeposits.IndexName := '1';
    cdPendingDeposits.LogChanges := False;

    cdPendingEftDebits := TevClientDataSet.Create(nil);
    cdPendingEftDebits.FieldDefs.Add('DEPOSIT_NBR', ftInteger);
    cdPendingEftDebits.FieldDefs.Add('EIN', ftString, 11);
    cdPendingEftDebits.FieldDefs.Add('LAST_CHECK_DATE', ftDateTime);
    cdPendingEftDebits.FieldDefs.Add('DUE_DATE', ftDateTime);
    cdPendingEftDebits.FieldDefs.Add('PAYROLLS_TOTAL', ftInteger);
    cdPendingEftDebits.FieldDefs.Add('NyStateTax', ftCurrency);
    cdPendingEftDebits.FieldDefs.Add('NyCityTax', ftCurrency);
    cdPendingEftDebits.FieldDefs.Add('CityOfYorkersTax', ftCurrency);
    cdPendingEftDebits.CreateDataSet;
    cdPendingEftDebits.LogChanges := False;
    cdPendingEftDebits.IndexFieldNames := 'DEPOSIT_NBR';

    cdCoSui := TevClientDataSet.Create(nil);
    cdCoSui.FieldDefs.Add('CO_SUI_NBR', ftInteger);
    cdCoSui.FieldDefs.Add('SY_SUI_NBR', ftInteger);
    cdCoSui.FieldDefs.Add('SY_AGENCY_NBR', ftInteger);
    cdCoSui.CreateDataSet;
    cdCoSui.LogChanges := False;

    FedMainCoControl := False;

    tmpIndex:= CO_STATE_TAX_LIABILITIES.IndexFieldNames;
    CO_STATE_TAX_LIABILITIES.IndexFieldNames:= 'CHECK_DATE';

    ctx_DataAccess.StartNestedTransaction([dbtClient, dbtBureau]);
    try
      LockedCompanies := TStringList.Create; /// List of Locked companies
      try
        FilterDataSet := TevClientDataSet.Create(nil);
        try
          try
            try

              LoadDataSetWithList(CO_FED_TAX_LIABILITIES, FedTaxLiabilityList);

              // federal & FUI
              if CO_FED_TAX_LIABILITIES.Active and (CO_FED_TAX_LIABILITIES.RecordCount > 0) then
              begin
                FillAchKey(CO_FED_TAX_LIABILITIES, -1, -1, ttFed);
                FedMainCoControl := True;

                EinOrPinList:= 'FEIN';
                if CO_FED_TAX_LIABILITIES.RecordCount > 0 then
                begin
                  MainCoNbr := 0;
                  SetMainCoNbr(CO_FED_TAX_LIABILITIES);
                  Assert(MainCoNbr <> 0);
                  Assert(CO.Locate('CO_NBR', MainCoNbr, []));
                  if (string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_DEBIT,
                                                                      TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT]) and
                     (string(CO['TAX_SERVICE'])[1] = TAX_SERVICE_DIRECT) then
                            EinOrPinList:= 'PIN_NUMBER';
                end;

                EinList := GetFieldValueList(CO, EinOrPinList,True);
                try
                  for i := 0 to EinList.Count - 1 do
                  begin
                    ResetPrNbr;
                    Assert(CO.Locate(EinOrPinList, EinList[i], []));

                    if ConvertNull(CO['BUSINESS_TYPE'], '') = BUSINESS_TYPE_AGRICULTURE then
                      CurrentTaxName := '943'
                    else
                    if ConvertNull(CO['BUSINESS_TYPE'], '') = BUSINESS_TYPE_944 then
                      CurrentTaxName := '944'
                    else
                      CurrentTaxName := '941';

                    LiabFilter := BuildLiabFilter(CO, EinOrPinList, EinList[i], 'CO_NBR');

                    LiabTypeFilter := LiabFilter + ' and ' + Fed941Filter;
                    PrepareTable(CO_FED_TAX_LIABILITIES, LiabTypeFilter);
                    if CO_FED_TAX_LIABILITIES.RecordCount > 0 then
                    begin
                      if EinOrPinList = 'FEIN' then
                         CheckDifferentPaymentMethod('Fed','FEIN=''' + EinList[i] + '''', EinList[i]);

                      SetMainCoNbr(CO_FED_TAX_LIABILITIES);
                      if ForceAchKeySplit or (not ForceChecks and (string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_DEBIT,
                        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT])) then
                        AchKeyField := 'ACH_KEY'
                      else
                        AchKeyField := 'ADJ_PERIOD_END_DATE';
                      if GetTaxLiabilities(CO_FED_TAX_LIABILITIES) >= 0 then
                      begin
                        NegativeWaitingTotal := GetNegativeWaitingTotal(CO_FED_TAX_LIABILITIES, LiabTypeFilter, ['DUE_DATE', AchKeyField]);
                        DueDateList := GetFieldValueList(CO_FED_TAX_LIABILITIES, 'DUE_DATE');
                        try
                          SortDateList(DueDateList);
                          for j := 0 to DueDateList.Count - 1 do
                          begin
                            LiabDueDateFilter := LiabTypeFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                            PrepareTable(CO_FED_TAX_LIABILITIES, LiabDueDateFilter, False);
                            AchKeyList := GetFieldValueList(CO_FED_TAX_LIABILITIES, AchKeyField);
                            try
                              for m := 0 to AchKeyList.Count - 1 do
                              begin
                                AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                PrepareTable(CO_FED_TAX_LIABILITIES, AchFilter);
                                ProcessLiabilities(CO_FED_TAX_LIABILITIES, ttFed, string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1],
                                  SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'],
                                  'SY_GLOBAL_AGENCY_NBR'), Null, Null, LiabTypeFilter, EinList[i], MakeString([DueDateList[j], AchKeyList[m]]));
                              end;
                            finally
                              AchKeyList.Free;
                            end;
                          end;
                        finally
                          DueDateList.Free;
                        end
                      end
                      else
                        raise ETaxSilentException.CreateHelp('The total of 941 you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                    end;
                    CurrentTaxName := '940';
                    LiabTypeFilter := LiabFilter + ' and ' + Fed940Filter;
                    PrepareTable(CO_FED_TAX_LIABILITIES, LiabTypeFilter);
                    if CO_FED_TAX_LIABILITIES.RecordCount > 0 then
                    begin
                      SetMainCoNbr(CO_FED_TAX_LIABILITIES);
                      if ForceAchKeySplit or (not ForceChecks and (string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_DEBIT,
                        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT])) then
                        AchKeyField := 'ACH_KEY'
                      else
                        AchKeyField := 'ADJ_PERIOD_END_DATE';
                      if GetTaxLiabilities(CO_FED_TAX_LIABILITIES) >= 0 then
                      begin
                        NegativeWaitingTotal := GetNegativeWaitingTotal(CO_FED_TAX_LIABILITIES, LiabTypeFilter, ['DUE_DATE']);
                        DueDateList := GetFieldValueList(CO_FED_TAX_LIABILITIES, 'DUE_DATE');
                        try
                          SortDateList(DueDateList);
                          for j := 0 to DueDateList.Count - 1 do
                          begin
                            LiabDueDateFilter := LiabTypeFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                            PrepareTable(CO_FED_TAX_LIABILITIES, LiabDueDateFilter, False);
                            AchKeyList := GetFieldValueList(CO_FED_TAX_LIABILITIES, AchKeyField);
                            try
                              for m := 0 to AchKeyList.Count - 1 do
                              begin
                                AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                PrepareTable(CO_FED_TAX_LIABILITIES, AchFilter);
                                ProcessLiabilities(CO_FED_TAX_LIABILITIES, ttFed, string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1],
                                  SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['FUI_SY_TAX_PAYMENT_AGENCY'],
                                  'SY_GLOBAL_AGENCY_NBR'), Null, Null, LiabTypeFilter, EinList[i], MakeString([DueDateList[j], AchKeyList[m]]));
                              end;
                            finally
                              AchKeyList.Free;
                            end;
                          end;
                        finally
                          DueDateList.Free;
                        end
                      end
                      else
                        raise ETaxSilentException.CreateHelp('The total of 940 you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                    end;
                    CurrentTaxName := '945';
                    LiabTypeFilter := LiabFilter + ' and ' + Fed945Filter;
                    PrepareTable(CO_FED_TAX_LIABILITIES, LiabTypeFilter);
                    if CO_FED_TAX_LIABILITIES.RecordCount > 0 then
                    begin
                      SetMainCoNbr(CO_FED_TAX_LIABILITIES);
                      if ForceAchKeySplit or (not ForceChecks and (string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_DEBIT,
                        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT])) then
                        AchKeyField := 'ACH_KEY'
                      else
                        AchKeyField := 'ADJ_PERIOD_END_DATE';
                      if GetTaxLiabilities(CO_FED_TAX_LIABILITIES) >= 0 then
                      begin
                        NegativeWaitingTotal := GetNegativeWaitingTotal(CO_FED_TAX_LIABILITIES, LiabTypeFilter, ['DUE_DATE']);
                        DueDateList := GetFieldValueList(CO_FED_TAX_LIABILITIES, 'DUE_DATE');
                        try
                          SortDateList(DueDateList);
                          for j := 0 to DueDateList.Count - 1 do
                          begin
                            LiabDueDateFilter := LiabTypeFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                            PrepareTable(CO_FED_TAX_LIABILITIES, LiabDueDateFilter, False);
                            AchKeyList := GetFieldValueList(CO_FED_TAX_LIABILITIES, AchKeyField);
                            try
                              for m := 0 to AchKeyList.Count - 1 do
                              begin
                                AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                PrepareTable(CO_FED_TAX_LIABILITIES, AchFilter);
                                ProcessLiabilities(CO_FED_TAX_LIABILITIES, ttFed, string(CO['FEDERAL_TAX_PAYMENT_METHOD'])[1],
                                  SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'],
                                  'SY_GLOBAL_AGENCY_NBR'), Null, Null, LiabTypeFilter, EinList[i], AchFilter);
                              end;
                            finally
                              AchKeyList.Free;
                            end;
                          end;
                        finally
                          DueDateList.Free;
                        end
                      end
                      else
                        raise ETaxSilentException.CreateHelp('The total of 941 you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                    end;
                  end;
                finally
                  UnPrepareTable(CO_FED_TAX_LIABILITIES);
                  EinList.Free;
                  FedMainCoControl := False;
                end;
              end;

              LoadDataSetWithList(CO_STATE_TAX_LIABILITIES, StateTaxLiabilityList);

              // state
              if CO_STATE_TAX_LIABILITIES.Active and (CO_STATE_TAX_LIABILITIES.RecordCount > 0) then
              begin
                StateList := GetFieldValueList(CO_STATES, 'STATE');
                try
                  FilterDataSet.CloneCursor(CO_STATES, False);
                  FilterDataSet.Filter := '';
                  FilterDataSet.Filtered := True;
                  for k := 0 to StateList.Count - 1 do
                  begin

                    FilterDataSet.Filter := 'STATE = ''' + StateList[k] + '''';
                    EinList := GetFieldValueList(FilterDataSet, 'STATE_EIN');
                    try
                      for i := 0 to EinList.Count - 1 do
                      begin
                        ResetPrNbr;
                        LiabFilter := BuildLiabFilter(FilterDataSet, 'STATE_EIN', EinList[i], 'CO_STATES_NBR');
                        PrepareTable(CO_STATE_TAX_LIABILITIES, LiabFilter);
                        if CO_STATE_TAX_LIABILITIES.RecordCount > 0 then
                        begin
                          Assert(FilterDataSet.Locate('STATE_EIN', EinList[i], []));
                          Assert(CO_STATES.Locate('CO_STATES_NBR', FilterDataSet['CO_STATES_NBR'], []));

                          MainCoStateNbr := DetermineMainCoNbrForState(DM_COMPANY.CO_STATE_TAX_LIABILITIES['CO_NBR'], FilterDataSet['CO_STATES_NBR'], StateList[k], EinList[i]);

                          if not SY_STATE_DEPOSIT_FREQ.FindKey([CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR']]) then
                            raise EInconsistentData.CreateHelp('State deposit frequency is not correct for state ' +
                              ConvertNull(CO_STATES['STATE'], 'N/A'), IDH_InconsistentData);
                          FillAchKey(CO_STATE_TAX_LIABILITIES,
                            ConvertNull(SY_STATE_DEPOSIT_FREQ.FieldByName('SY_REPORTS_GROUP_NBR').Value, -1),
                            ConvertNull(SY_STATE_DEPOSIT_FREQ.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').Value, -1), ttState);
                          Assert(SY_STATES.Locate('SY_STATES_NBR', CO_STATES['SY_STATES_NBR'], []));
                          Assert(CO.Locate('CO_NBR', FilterDataSet['CO_NBR'], []));

                          if ForceAchKeySplit or (not ForceChecks and (string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT,
                            TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT])) then
                            AchKeyField := 'ACH_KEY'
                          else
                            AchKeyField := 'ADJ_PERIOD_END_DATE';
                          if SY_STATES['PAY_SDI_WITH'] <> GROUP_BOX_YES then
                          begin
                            if GetTaxLiabilities(CO_STATE_TAX_LIABILITIES) >= 0 then
                            begin
                              CheckDifferentPaymentMethod('State','STATE_EIN=''' + EinList[i] + ''' and STATE = ''' + StateList[k] + '''',EinList[i],StateList[k]);

                              NegativeWaitingTotal := GetNegativeWaitingTotal(CO_STATE_TAX_LIABILITIES, LiabFilter, ['DUE_DATE', AchKeyField, 'TAX_COUPON_SY_REPORTS_NBR', 'FIELD_OFFICE_NBR']);
                              DueDateList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'DUE_DATE');
                              try
                                SortDateList(DueDateList);
                                for j := 0 to DueDateList.Count - 1 do
                                begin
                                  LiabDueDateFilter := LiabFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                                  PrepareTable(CO_STATE_TAX_LIABILITIES, LiabDueDateFilter);
                                  AchKeyList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, AchKeyField);
                                  try
                                    for m := 0 to AchKeyList.Count - 1 do
                                    begin
                                      AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                      PrepareTable(CO_STATE_TAX_LIABILITIES, AchFilter);
                                      TaxCouponList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'TAX_COUPON_SY_REPORTS_NBR');
                                      try
                                        TaxCouponList.Sort;
                                        for o := TaxCouponList.Count - 1 downto 0 do
                                        begin
                                          TaxCouponFilter := AchFilter + ' and TAX_COUPON_SY_REPORTS_NBR=''' + TaxCouponList[o] + '''';
                                          KeyFilter := MakeKeyFilter(DueDateList[j], AchKeyList[m]);
                                          PrepareTable(CO_STATE_TAX_LIABILITIES, TaxCouponFilter);
                                          FieldOfficeKeyList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'FIELD_OFFICE_NBR');
                                          try
                                            FieldOfficeKeyList.Sort;
                                            for n := FieldOfficeKeyList.Count - 1 downto 0 do
                                            begin
                                              FieldOfficeNbr := StrToInt(FieldOfficeKeyList[n]);
                                              TaxCouponNumber := StrToInt(TaxCouponList[o]);
                                              FieldOfficeFilter := TaxCouponFilter + ' and FIELD_OFFICE_NBR = ' + FieldOfficeKeyList[n];
                                              PrepareTable(CO_STATE_TAX_LIABILITIES, FieldOfficeFilter, False);
                                              if (FieldOfficeNbr = 39)
                                                and (StrToDateTime(DueDateList[j]) <> CheckForNDaysRule(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], 0, RecodeDay(StrToDateTime(DueDateList[j]), 15))) then
                                              begin
                                                FieldOfficeNbr := 723; // NJ
                                                TaxCouponNumber := -1;
                                              end;
                                              if (FieldOfficeNbr = 577)
                                                and (StrToDateTime(DueDateList[j]) = CheckForNDaysRule(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], 0, GetEndMonth(GetEndQuarter(CO_STATE_TAX_LIABILITIES.CHECK_DATE.AsDateTime) + 1))) then
                                              begin
                                                FieldOfficeNbr := 576; // NY Quarter End
                                                TaxCouponNumber := -1;
                                              end;

                                              StateDepFreqList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'cSY_STATE_DEPOSIT_FREQ_NBR');
                                              try
                                                StateDepFreqList.Sort;
                                                                       //   Special logic for CA  ACH process...
                                                if (StateList[k] = 'CA') and
                                                   (string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]) and
                                                    Ca_CheckStateLiabDepFreqAndNegatifAmount(CO_STATE_TAX_LIABILITIES)  then
                                                begin
                                                  for p := StateDepFreqList.Count - 1 downto 0 do
                                                  begin
                                                    PrepareTable(CO_STATE_TAX_LIABILITIES, FieldOfficeFilter + ' and cSY_STATE_DEPOSIT_FREQ_NBR = ' + StateDepFreqList[p], False);
                                                    tmpFilter := KeyFilter + ';' + StateDepFreqList[p];
                                                    ProcessLiabilities(CO_STATE_TAX_LIABILITIES, ttState, string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1],
                                                       SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], FieldOfficeNbr, TaxCouponNumber, LiabFilter,
                                                       EinList[i], tmpFilter);
                                                  end;
                                                end
                                                else
                                                 ProcessLiabilities(CO_STATE_TAX_LIABILITIES, ttState, string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1],
                                                    SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], FieldOfficeNbr, TaxCouponNumber, LiabFilter,
                                                    EinList[i], KeyFilter);
                                              finally
                                                StateDepFreqList.Free;
                                              end;
                                            end;
                                          finally
                                            FieldOfficeKeyList.Free;
                                          end;
                                        end;
                                      finally
                                        TaxCouponList.Free;
                                      end;
                                    end;
                                  finally
                                    AchKeyList.Free
                                  end;
                                end;
                              finally
                                DueDateList.Free;
                              end;
                            end
                            else
                              raise ETaxSilentException.CreateHelp('The total of state "' + StateList[k] + '" you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                          end
                          else
                          begin
                            if not (CO_SUI.Locate('CO_STATES_NBR', CO_STATES['CO_STATES_NBR'], [])) then
                              raise EInconsistentData.CreateHelp('State ' + StateList[k] + ' setup conflict. There is no SUI to pay SDI with', IDH_InconsistentData);
                            LiabTypeFilter := LiabFilter + ' and TAX_TYPE = ''' + TAX_LIABILITY_TYPE_STATE + '''';
                            PrepareTable(CO_STATE_TAX_LIABILITIES, LiabTypeFilter);
                            if GetTaxLiabilities(CO_STATE_TAX_LIABILITIES) >= 0 then
                            begin

                              CheckDifferentPaymentMethod('State','STATE_EIN=''' + EinList[i] + ''' and STATE = ''' + StateList[k] + '''',EinList[i],StateList[k]);

                              NegativeWaitingTotal := GetNegativeWaitingTotal(CO_STATE_TAX_LIABILITIES, LiabFilter, ['DUE_DATE', AchKeyField, 'TAX_COUPON_SY_REPORTS_NBR', 'FIELD_OFFICE_NBR']);
                              DueDateList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'DUE_DATE');
                              try
                                SortDateList(DueDateList);
                                for j := 0 to DueDateList.Count - 1 do
                                begin
                                  LiabDueDateFilter := LiabTypeFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                                  PrepareTable(CO_STATE_TAX_LIABILITIES, LiabDueDateFilter);
                                  AchKeyList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, AchKeyField);
                                  try
                                    for m := 0 to AchKeyList.Count - 1 do
                                    begin
                                      AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                      PrepareTable(CO_STATE_TAX_LIABILITIES, AchFilter);
                                      TaxCouponList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'TAX_COUPON_SY_REPORTS_NBR');
                                      try
                                        TaxCouponList.Sort;
                                        for o := TaxCouponList.Count - 1 downto 0 do
                                        begin
                                          TaxCouponFilter := AchFilter + ' and TAX_COUPON_SY_REPORTS_NBR=''' + TaxCouponList[o] + '''';
                                          KeyFilter := MakeKeyFilter(DueDateList[j], AchKeyList[m]);
                                          PrepareTable(CO_STATE_TAX_LIABILITIES, TaxCouponFilter);
                                          FieldOfficeKeyList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'FIELD_OFFICE_NBR');
                                          try
                                            FieldOfficeKeyList.Sort;
                                            for n := FieldOfficeKeyList.Count - 1 downto 0 do
                                            begin
                                              FieldOfficeNbr := StrToInt(FieldOfficeKeyList[n]);
                                              TaxCouponNumber := StrToInt(TaxCouponList[o]);
                                              FieldOfficeFilter := TaxCouponFilter + ' and FIELD_OFFICE_NBR = ' + FieldOfficeKeyList[n];
                                              PrepareTable(CO_STATE_TAX_LIABILITIES, FieldOfficeFilter, False);
                                              if (FieldOfficeNbr = 39)
                                                and (StrToDateTime(DueDateList[j]) <> CheckForNDaysRule(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], 0, RecodeDay(StrToDateTime(DueDateList[j]), 15))) then
                                              begin
                                                FieldOfficeNbr := 723; // NJ
                                                TaxCouponNumber := -1;
                                              end;
                                              if (FieldOfficeNbr = 577)
                                                and (StrToDateTime(DueDateList[j]) = CheckForNDaysRule(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], 0, GetEndMonth(GetEndQuarter(CO_STATE_TAX_LIABILITIES.CHECK_DATE.AsDateTime) + 1))) then
                                              begin
                                                FieldOfficeNbr := 576; // NY Quarter End
                                                TaxCouponNumber := -1;
                                              end;

                                              StateDepFreqList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'cSY_STATE_DEPOSIT_FREQ_NBR');
                                              try
                                                StateDepFreqList.Sort;
                                                                       //   Special logic for CA  ACH process...
                                                if (StateList[k] = 'CA') and
                                                   (string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]) and
                                                    Ca_CheckStateLiabDepFreqAndNegatifAmount(CO_STATE_TAX_LIABILITIES) then
                                                begin
                                                  for p := StateDepFreqList.Count - 1 downto 0 do
                                                  begin
                                                    PrepareTable(CO_STATE_TAX_LIABILITIES, FieldOfficeFilter + ' and cSY_STATE_DEPOSIT_FREQ_NBR = ' + StateDepFreqList[p], False);
                                                    tmpFilter := KeyFilter + ';' + StateDepFreqList[p];
                                                    ProcessLiabilities(CO_STATE_TAX_LIABILITIES, ttState, string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1],
                                                       SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], FieldOfficeNbr, TaxCouponNumber, LiabFilter,
                                                         EinList[i], tmpFilter);
                                                  end;
                                                end
                                                else
                                                 ProcessLiabilities(CO_STATE_TAX_LIABILITIES, ttState, string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1],
                                                    SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], FieldOfficeNbr, TaxCouponNumber, LiabFilter,
                                                     EinList[i], KeyFilter);
                                              finally
                                                StateDepFreqList.Free;
                                              end;
                                            end;
                                          finally
                                            FieldOfficeKeyList.Free;
                                          end;
                                        end;
                                      finally
                                        TaxCouponList.Free;
                                      end;
                                    end;
                                  finally
                                    AchKeyList.Free
                                  end;
                                end;
                              finally
                                DueDateList.Free;
                              end;
                            end
                            else
                              raise ETaxSilentException.CreateHelp('The total of state "' + StateList[k] + '" tax you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                            LiabTypeFilter := LiabFilter + ' and TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_STATE + '''';
                            PrepareTable(CO_STATE_TAX_LIABILITIES, LiabTypeFilter);
                            if GetTaxLiabilities(CO_STATE_TAX_LIABILITIES) >= 0 then
                            begin
                              CheckDifferentPaymentMethod('State','STATE_EIN=''' + EinList[i] + ''' and STATE = ''' + StateList[k] + '''',EinList[i],StateList[k]);

                              if ForceAchKeySplit or (not ForceChecks and (string(CO_STATES['SUI_TAX_DEPOSIT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT,
                                TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT])) then
                                AchKeyField := 'ACH_KEY'
                              else
                                AchKeyField := 'ADJ_PERIOD_END_DATE';
                              NegativeWaitingTotal := GetNegativeWaitingTotal(CO_STATE_TAX_LIABILITIES, LiabFilter, ['DUE_DATE', AchKeyField]);
                              DueDateList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'DUE_DATE');
                              try
                                SortDateList(DueDateList);
                                for j := 0 to DueDateList.Count - 1 do
                                begin
                                  LiabDueDateFilter := LiabTypeFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                                  PrepareTable(CO_STATE_TAX_LIABILITIES, LiabDueDateFilter);
                                  AchKeyList := GetFieldValueList(CO_STATE_TAX_LIABILITIES, AchKeyField);
                                  try
                                    for m := 0 to AchKeyList.Count - 1 do
                                    begin
                                      AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                      KeyFilter := MakeKeyFilter(DueDateList[j], AchKeyList[m]);
                                      PrepareTable(CO_STATE_TAX_LIABILITIES, AchFilter, False);
                                      ProcessLiabilities(CO_STATE_TAX_LIABILITIES, ttState, string(CO_STATES['SUI_TAX_DEPOSIT_METHOD'])[1],
                                        SY_SUI.Lookup('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], 'SY_SUI_TAX_PMT_AGENCY_NBR'), Null, Null, LiabFilter,
                                        CO_STATES.SUI_EIN.AsString {EinList[i]}, KeyFilter);
                                    end;
                                  finally
                                    AchKeyList.Free
                                  end;
                                end;
                              finally
                                DueDateList.Free;
                              end;
                            end
                            else
                              raise ETaxSilentException.CreateHelp('The total of state "' + StateList[k] + '" SDI you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                          end;
                        end;
                      end;
                    finally
                      UnPrepareTable(CO_STATE_TAX_LIABILITIES);
                      EinList.Free;
                    end;
                  end;
                finally
                  FilterDataSet.Close;
                  StateList.Free;
                end;
              end;

              LoadDataSetWithList(CO_SUI_LIABILITIES, SUITaxLiabilityList);
              // SUI
              if CO_SUI_LIABILITIES.Active and (CO_SUI_LIABILITIES.RecordCount > 0) then
              begin
                StateList := GetFieldValueList(CO_STATES, 'STATE');
                try
                  FilterDataSet.CloneCursor(CO_STATES, False);
                  FilterDataSet.Filter := '';
                  FilterDataSet.Filtered := True;
                  for k := 0 to StateList.Count - 1 do
                  begin
                    FilterDataSet.Filter := 'STATE = ''' + StateList[k] + '''';
                    EinList := GetFieldValueList(FilterDataSet, 'SUI_EIN');
                    try
                      for i := 0 to EinList.Count - 1 do
                      begin
                        ResetPrNbr;
                        Assert(FilterDataSet.Locate('SUI_EIN', EinList[i], []));
                        Assert(CO_STATES.Locate('CO_STATES_NBR', FilterDataSet['CO_STATES_NBR'], []));
                        Assert(SY_STATES.Locate('SY_STATES_NBR', CO_STATES['SY_STATES_NBR'], []));
                        if not SY_STATE_DEPOSIT_FREQ.FindKey([CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR']]) then
                          raise EInconsistentData.CreateHelp('State deposit frequency is not correct for state ' +
                            ConvertNull(CO_STATES['STATE'], 'N/A'), IDH_InconsistentData);
                        Assert(CO.Locate('CO_NBR', FilterDataSet['CO_NBR'], []));
                        SuiFilter := BuildLiabFilter(FilterDataSet, 'SUI_EIN', EinList[i], 'CO_STATES_NBR');

                        PrepareTable(CO_SUI, SuiFilter);
                        cdCoSui.EmptyDataSet;
                        CO_SUI.First;
                        while not CO_SUI.Eof do
                        begin
                          cdCoSui.AppendRecord([CO_SUI['CO_SUI_NBR'], CO_SUI['SY_SUI_NBR'], SY_SUI.Lookup('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], 'SY_SUI_TAX_PMT_AGENCY_NBR')]);
                          Assert(not VarIsNull(cdCoSui['SY_AGENCY_NBR']), 'Check company ' + CO_SUI['description'] + ' SUI setup');
                          CO_SUI.Next;
                        end;
                        SUIList := GetFieldValueList(cdCoSui, 'SY_AGENCY_NBR');
                        try
                          for l := 0 to SUIList.Count - 1 do
                          begin
                            LiabFilter := BuildLiabFilter(cdCoSui, 'SY_AGENCY_NBR', SUIList[l], 'CO_SUI_NBR');
                            Assert(cdCoSui.Locate('SY_AGENCY_NBR', SUIList[l], []));
                            Assert(SY_SUI.Locate('SY_SUI_NBR', cdCoSui['SY_SUI_NBR'], []));
                            PrepareTable(CO_SUI_LIABILITIES, LiabFilter);
                            if CO_SUI_LIABILITIES.RecordCount > 0 then
                            begin
                              CheckDifferentPaymentMethod('SUI','SUI_EIN=''' + EinList[i] + ''' and STATE = ''' + StateList[k] + '''',EinList[i],StateList[k] );

                              FillAchKey(CO_SUI_LIABILITIES, ConvertNull(SY_SUI.FieldByName('SY_REPORTS_GROUP_NBR').Value, -1), -1, ttSUI);
                              SetMainCoNbr(CO_SUI_LIABILITIES);
                              if ForceAchKeySplit or (not ForceChecks and (string(CO_STATES['SUI_TAX_DEPOSIT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT,
                                TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT])) then
                                AchKeyField := 'ACH_KEY'
                              else
                                AchKeyField := 'ADJ_PERIOD_END_DATE';
                              if GetTaxLiabilities(CO_SUI_LIABILITIES) >= 0 then
                              begin
                                NegativeWaitingTotal := GetNegativeWaitingTotal(CO_SUI_LIABILITIES, LiabFilter, ['DUE_DATE', AchKeyField]);
                                DueDateList := GetFieldValueList(CO_SUI_LIABILITIES, 'DUE_DATE');
                                try
                                  SortDateList(DueDateList);
                                  for j := 0 to DueDateList.Count - 1 do
                                  begin
                                    LiabDueDateFilter := LiabFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                                    PrepareTable(CO_SUI_LIABILITIES, LiabDueDateFilter);
                                    AchKeyList := GetFieldValueList(CO_SUI_LIABILITIES, AchKeyField);
                                    try
                                      for m := 0 to AchKeyList.Count - 1 do
                                      begin
                                        AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                        KeyFilter := MakeKeyFilter(DueDateList[j], AchKeyList[m]);
                                        PrepareTable(CO_SUI_LIABILITIES, AchFilter, False);
                                        TaxCouponList := GetFieldValueList(CO_SUI_LIABILITIES, 'TAX_COUPON_SY_REPORTS_NBR');
                                        try
                                          TaxCouponList.Sort;
                                          for o := TaxCouponList.Count - 1 downto 0 do
                                          begin
                                            TaxCouponFilter := AchFilter + ' and TAX_COUPON_SY_REPORTS_NBR=''' + TaxCouponList[o] + '''';
                                            PrepareTable(CO_SUI_LIABILITIES, TaxCouponFilter);

                                            FieldOfficeNbr := -1;
                                            TaxCouponNumber := StrToInt(TaxCouponList[o]);
                                            if CO_STATES['STATE'] = 'NY' then // NY!
                                            begin
                                              if (SY_STATE_DEPOSIT_FREQ.SY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger = 577)
                                                and (StrToDateTime(DueDateList[j]) = CheckForNDaysRule(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], 0, GetEndMonth(GetEndQuarter(CO_SUI_LIABILITIES.CHECK_DATE.AsDateTime) + 1))) then
                                              begin
                                                FieldOfficeNbr := 576; // NY Quarter End
                                                TaxCouponNumber := -1;
                                              end
                                              else
                                                FieldOfficeNbr := ConvertNull(SY_STATE_DEPOSIT_FREQ.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').Value, -1);

                                              ProcessLiabilities(CO_SUI_LIABILITIES, ttSUI, string(CO_STATES['STATE_TAX_DEPOSIT_METHOD'])[1],
                                                StrToInt(SUIList[l]),
                                                FieldOfficeNbr, TaxCouponNumber, LiabFilter,
                                                CO_STATES['STATE_EIN'], KeyFilter)
                                            end
                                            else
                                            begin
                                                if SY_STATE_DEPOSIT_FREQ.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').Value = 39 then // NJ
                                                begin
                                                  FieldOfficeNbr := 723;
                                                  TaxCouponNumber := -1;
                                                end;

                                                ProcessLiabilities(CO_SUI_LIABILITIES, ttSUI, string(CO_STATES['SUI_TAX_DEPOSIT_METHOD'])[1],
                                                    StrToInt(SUIList[l]),
                                                    FieldOfficeNbr, TaxCouponNumber, LiabFilter,
                                                    EinList[i], KeyFilter);
                                            end;
                                          end;
                                        finally
                                          TaxCouponList.Free;
                                        end;
                                      end;
                                    finally
                                      AchKeyList.Free;
                                    end;
                                  end;
                                finally
                                  DueDateList.Free;
                                end;
                              end
                              else
                                raise ETaxSilentException.CreateHelp('The total of SUI payment for "' + ConvertNull(SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', StrToInt(SUIList[l]), 'AGENCY_NAME'), '-') + '" you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                            end;
                          end;
                        finally
                          SUIList.Free;
                        end;
                      end;
                    finally
                      UnPrepareTable(CO_SUI_LIABILITIES);
                      UnPrepareTable(CO_SUI);
                      EinList.Free;
                    end;
                  end;
                finally
                  FilterDataSet.Close;
                  StateList.Free;
                end;
              end;

              LoadDataSetWithList(CO_LOCAL_TAX_LIABILITIES, LocalTaxLiabilityList);

              // local
              if CO_LOCAL_TAX_LIABILITIES.Active and (CO_LOCAL_TAX_LIABILITIES.RecordCount > 0) then
              begin
                cdTCDLocals:= CreatecdTCDLocalsDataset;        //  TCD

                StateList := GetFieldValueList(CO_LOCAL_TAX, 'SY_LOCALS_NBR');
                try
                  FilterDataSet.CloneCursor(CO_LOCAL_TAX, False);
                  FilterDataSet.Filter := '';
                  FilterDataSet.Filtered := True;
                  for k := 0 to StateList.Count - 1 do
                  begin
                    FilterDataSet.Filter := 'SY_LOCALS_NBR = ' + StateList[k];
                    Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', StrToInt(StateList[k]), []));
                    EinList := GetFieldValueList(FilterDataSet, 'LOCAL_EIN_NUMBER');
                    try
                      for i := 0 to EinList.Count - 1 do
                      begin
                        ResetPrNbr;
                        Assert(FilterDataSet.Locate('LOCAL_EIN_NUMBER', EinList[i], []));
                        Assert(CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', FilterDataSet['CO_LOCAL_TAX_NBR'], []));
                        Assert(SY_LOCAL_DEPOSIT_FREQ.Locate('SY_LOCAL_DEPOSIT_FREQ_NBR', CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR'], []));
                        Assert(CO_STATES.Locate('CO_NBR;SY_STATES_NBR', CO_LOCAL_TAX['CO_NBR;SY_STATES_NBR'], []),
                          Format(
                            'The combination CO_NBR %d, SY_STATES_NBR %d could not be located in the CO_STATES table. '+#13#10+
                            'This means that a local is set up for a State that is not set up.',
                            [CO_LOCAL_TAX.FieldByName('CO_NBR').AsInteger, CO_LOCAL_TAX.FieldByName('SY_STATES_NBR').AsInteger])
                          );
                        Assert(SY_STATES.Locate('SY_STATES_NBR', CO_STATES['SY_STATES_NBR'], []));
                        Assert(SY_STATE_DEPOSIT_FREQ.FindKey([CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR']]));
                        Assert(CO.Locate('CO_NBR', FilterDataSet['CO_NBR'], []));
                        LiabFilter := 'payment'+BuildLiabFilter(FilterDataSet, 'LOCAL_EIN_NUMBER', EinList[i], 'CO_LOCAL_TAX_NBR');

                        PrepareTable(CO_LOCAL_TAX_LIABILITIES, LiabFilter);
                        if CO_LOCAL_TAX_LIABILITIES.RecordCount > 0 then
                         begin

                          CheckDifferentPaymentMethod('Local','LOCAL_EIN_NUMBER=''' + EinList[i] + ''' and SY_LOCALS_NBR = ' + StateList[k], EinList[i]);

                           //    TCD
                          isItTCDLocals := cdTCDLocals.Locate('CO_LOCAL_TAX_NBR', CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'], []) and
                                             (SY_LOCALS.FieldByName('COMBINE_FOR_TAX_PAYMENTS').AsString = GROUP_BOX_YES);
                          if isItTCDLocals then
                          begin
                            FillAchKey(CO_LOCAL_TAX_LIABILITIES,
                              -1, -1, ttLoc, True,cdTCDLocals.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger);
                          end
                          else
                          if (SY_STATES.STATE.AsString = 'NY') and
                                (CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsInteger   = 5724) then
                              FillAchKey(CO_LOCAL_TAX_LIABILITIES, ConvertNull(SY_LOCAL_DEPOSIT_FREQ.FieldByName('SY_REPORTS_GROUP_NBR').Value, -1),
                                                         1506, ttLoc)     //   Field Office : Commissioner Of Taxation And Finance
                          else
                            FillAchKey(CO_LOCAL_TAX_LIABILITIES,
                              ConvertNull(SY_LOCAL_DEPOSIT_FREQ.FieldByName('SY_REPORTS_GROUP_NBR').Value, -1),
                              ConvertNull(SY_LOCAL_DEPOSIT_FREQ.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').Value, -1), ttLoc);

                          SetMainCoNbr(CO_LOCAL_TAX_LIABILITIES);
                          if ForceAchKeySplit or (not ForceChecks and (string(CO_LOCAL_TAX['PAYMENT_METHOD'])[1] in [TAX_PAYMENT_METHOD_CREDIT,
                            TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT])) then
                            AchKeyField := 'ACH_KEY'
                          else
                            AchKeyField := 'ADJ_PERIOD_END_DATE';

                          if isItTCDLocals then
                            AchKeyField := 'ACH_KEY';

                          if GetTaxLiabilities(CO_LOCAL_TAX_LIABILITIES) >= 0 then
                          begin
                            NegativeWaitingTotal := GetNegativeWaitingTotal(CO_LOCAL_TAX_LIABILITIES, LiabFilter, ['DUE_DATE', AchKeyField, 'TAX_COUPON_SY_REPORTS_NBR', 'FIELD_OFFICE_NBR']);
                            DueDateList := GetFieldValueList(CO_LOCAL_TAX_LIABILITIES, 'DUE_DATE');
                            try
                              SortDateList(DueDateList);
                              for j := 0 to DueDateList.Count - 1 do
                              begin
                                LiabDueDateFilter := LiabFilter + ' and DUE_DATE=''' + DueDateList[j] + '''';
                                PrepareTable(CO_LOCAL_TAX_LIABILITIES, LiabDueDateFilter);
                                AchKeyList := GetFieldValueList(CO_LOCAL_TAX_LIABILITIES, AchKeyField);
                                try
                                  for m := 0 to AchKeyList.Count - 1 do
                                  begin
                                    AchFilter := LiabDueDateFilter + ' and ' + AchKeyField + '=''' + AchKeyList[m] + '''';
                                    PrepareTable(CO_LOCAL_TAX_LIABILITIES, AchFilter);
                                    TaxCouponList := GetFieldValueList(CO_LOCAL_TAX_LIABILITIES, 'TAX_COUPON_SY_REPORTS_NBR');
                                    try
                                      TaxCouponList.Sort;
                                      for o := TaxCouponList.Count - 1 downto 0 do
                                      begin
                                        TaxCouponFilter := AchFilter + ' and TAX_COUPON_SY_REPORTS_NBR=''' + TaxCouponList[o] + '''';
                                        KeyFilter := MakeKeyFilter(DueDateList[j], AchKeyList[m]);
                                        PrepareTable(CO_LOCAL_TAX_LIABILITIES, TaxCouponFilter);
                                        FieldOfficeKeyList := GetFieldValueList(CO_LOCAL_TAX_LIABILITIES, 'FIELD_OFFICE_NBR');
                                        try
                                          FieldOfficeKeyList.Sort;
                                          for n := FieldOfficeKeyList.Count - 1 downto 0 do
                                          begin
                                            FieldOfficeNbr := StrToInt(FieldOfficeKeyList[n]);
                                            TaxCouponNumber := StrToInt(TaxCouponList[o]);

                                            if (SY_STATE_DEPOSIT_FREQ.SY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger = 577) and
                                               ( not ((CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsInteger= 5724) or (CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsInteger =1438)) )
                                                and (StrToDateTime(DueDateList[j]) = CheckForNDaysRule(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'], 0, GetEndMonth(GetEndQuarter(CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime) + 1))) then
                                            begin
                                                FieldOfficeNbr := 576; // NY Quarter End
                                                TaxCouponNumber := -1;
                                            end;

                                            FieldOfficeFilter := TaxCouponFilter + ' and FIELD_OFFICE_NBR = ' + FieldOfficeKeyList[n];
                                            PrepareTable(CO_LOCAL_TAX_LIABILITIES, FieldOfficeFilter, False);

                                            DepositAgencyNbr := SY_LOCALS.Lookup('SY_LOCALS_NBR', CO_LOCAL_TAX['SY_LOCALS_NBR'], 'SY_LOCAL_TAX_PMT_AGENCY_NBR');
                                            DepositPaymentMethod := string(CO_LOCAL_TAX['PAYMENT_METHOD'])[1];
                                            tmpTCDEIN := '';
                                            if isItTCDLocals then
                                            begin
                                              Assert(SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', cdTCDLocals.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsVariant, []),'Missing TCD Agency deposit frequency. Please contact with Isystems LLC.' );
                                              Assert(SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR', SY_AGENCY_DEPOSIT_FREQ['SY_GLOBAL_AGENCY_NBR'], []), 'Missing TCD Agency set up. Please contact with Isystems LLC.');
                                              Assert((SY_GLOBAL_AGENCY.FieldByName('AGENCY_TYPE').AsString = 'Y'), 'TCD Agency is inactive.  Select an active TCD Agency.');
                                              DepositAgencyNbr := SY_GLOBAL_AGENCY.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger;
                                              DepositPaymentMethod := cdTCDLocals.FieldByName('TCD_PAYMENT_METHOD').asString[1] ;
                                              tmpTCDEIN := 'TCD_EIN';
                                            end;
                                            ProcessLiabilities(CO_LOCAL_TAX_LIABILITIES, ttLoc, DepositPaymentMethod,
                                                     DepositAgencyNbr, FieldOfficeNbr, TaxCouponNumber, LiabFilter, EinList[i], KeyFilter, tmpTCDEIN);
                                          end;
                                        finally
                                          FieldOfficeKeyList.Free;
                                        end;
                                      end;
                                    finally
                                      TaxCouponList.Free;
                                    end;
                                  end
                                finally
                                  AchKeyList.Free;
                                end;
                              end;
                            finally
                              DueDateList.Free;
                            end;
                          end
                          else
                            raise ETaxSilentException.CreateHelp('The total of local "' + ConvertNull(SY_LOCALS['NAME'], '-') + '" you picked up to pay is negative. Should be positive', IDH_InvalidParameters);
                        end;
                      end;
                    finally
                      UnPrepareTable(CO_LOCAL_TAX_LIABILITIES);
                      EinList.Free;
                    end;
                  end;
                finally
                  FilterDataSet.Close;
                  StateList.Free;
                end;
              end;

            except
              on E: ECountMaxHitException do
                ;
            end;

            ProcessPendingEftDebitRecords;
            PayTaxesExt.UnivDebitFile.FinalizePayments;
            PayTaxesExt.OneMiscCheck.FinalizePayments;

            PreProcessPostedAchRecords(DM_COMPANY.CO_TAX_PAYMENT_ACH);
            ctx_DataAccess.PostDataSets(
              [ DM_PAYROLL.PR,
                DM_COMPANY.CO,
                DM_COMPANY.CO_TAX_DEPOSITS,
                DM_COMPANY.CO_FED_TAX_LIABILITIES,
                DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                DM_COMPANY.CO_SUI_LIABILITIES,
                DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
                DM_CLIENT.CL_BANK_ACCOUNT,
                DM_PAYROLL.PR_MISCELLANEOUS_CHECKS,
                DM_COMPANY.CO_TAX_PAYMENT_ACH,
                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER
                ]);
            ProcessPostedAchRecords(DM_COMPANY.CO_TAX_PAYMENT_ACH);
          finally
            FilterDataSet.Free;
          end;
        except
          ctx_DataAccess.CancelDataSets(
            [ DM_PAYROLL.PR,
              DM_COMPANY.CO,
              DM_COMPANY.CO_TAX_DEPOSITS,
              DM_COMPANY.CO_FED_TAX_LIABILITIES,
              DM_COMPANY.CO_STATE_TAX_LIABILITIES,
              DM_COMPANY.CO_SUI_LIABILITIES,
              DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
              DM_CLIENT.CL_BANK_ACCOUNT,
              DM_PAYROLL.PR_MISCELLANEOUS_CHECKS,
              DM_COMPANY.CO_TAX_PAYMENT_ACH
              ]);
          raise;
        end;
        if Pr_Nbr_List.Count > 0 then
        begin
          Pr_Nbr_List.Clear;
          with DM_PAYROLL.PR do
          begin
            First;
            while not Eof do
            begin
              Pr_Nbr_List.Append(IntToStr(FieldValues['PR_NBR']));
              Next;
            end;
          end;
          PayTaxesExt.TaxPayrollFilter := Pr_Nbr_List.CommaText;
          Sleep(2000);
          for i := 0 to Pr_Nbr_List.Count - 1 do
             ctx_PayrollProcessing.ProcessPayroll(StrToInt(Pr_Nbr_List[i]), False, Log, True, rlDoNotLock);
        end;
      finally
        Pr_Nbr_List.Free;
        cdCoSui.Free;
        cdPendingDeposits.Free;
        cdPendingEftDebits.Free;
        LockedCompanies.Free;
        CO_STATE_TAX_LIABILITIES.IndexFieldNames:= tmpIndex;
      end;
      AbortIfCurrentThreadTaskTerminated;
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      FreeAndNil(Result);
      raise;
    end;
    if SY_REPORTS.IndexDefs.IndexOf('SY_REPORTS_GROUPNBR') <> -1 then
          SY_REPORTS.DeleteIndex('SY_REPORTS_GROUPNBR');
  end;
end;

function PrintChecksAndCouponsForTaxes(const TaxPayrollFilter, DefaultMiscCheckForm : string): TrwReportResults;
var
  ms: IisStream;
  s: string;
  i: Integer;
begin
  Result := TrwReportResults.Create;
  try
    s := TaxPayrollFilter;
    while s <> '' do
    begin
      i := StrToIntDef(Fetch(s, ','), 0);
      if i <> 0 then
      begin
        ms := ctx_PayrollProcessing.PrintPayroll(i, True, True, DefaultMiscCheckForm);
        Result.AddReportResults(ms);
      end;

    end;
  except
    on E: Exception do
    begin
      Result.Free;
      raise;
    end;
  end;
end;

function FinalizeTaxPayments(const Params: IisStream; const AACHOptions: IisStringList; const SbTaxPaymentNbr : Integer;
  PostProcessReportName, DefaultMiscCheckForm: String): IisStream;
var
  i: Integer;
  s: string;
  m: IisStream;
  mm : IisStream;
  r: TTaxPaymentParams;
  FNy: TNyDebitPayments;
  FMa: TMaDebitPayments;
  FCa: TCaDebitPayments;
  FUniv: TUniversalDebitPayments;
  FEFTPS: TEFTPS_Payment;
  FChecks: TrwReportResults;
  FAch: TevAchEFT;
  FOneMiscCheck: TOneMiscCheckPayments;
  {FWarnings,}l: TStringList;
  FParamList: TStringList;
  ReportID: Integer;
  ReportDB: String;
  ReportName, Num: String;  
  Res: TrwReportResults;
  ReportResult: TrwReportResult;
  ResData: IevDualStream;
  ReportParams: TrwReportParams;
begin
  Sleep(2000);
  Params.Position := 0;
  Assert(Params.Size > 0);
  Result := TisStream.Create;
  FParamList := TStringList.Create;
  FParamList.Sorted := True;
  FNy := TNyDebitPayments.Create(nil);
  FMa := TMaDebitPayments.Create(nil);
  FCa := TCaDebitPayments.Create(nil);
  FUniv := TUniversalDebitPayments.Create(nil);
  FOneMiscCheck := TOneMiscCheckPayments.Create(nil);
  FEFTPS := TEFTPS_Payment.Create(nil);
  FEFTPS.FileDateTime := Copy(Params.ReadString, 6, 255);
  FChecks := TrwReportResults.Create;

  FAch := TevAchEFT.Create(nil);
  FAch.LoadACHOptionsFromIniFile(AACHOptions);
  l := TStringList.Create;
  //FWarnings := TStringList.Create;
  try
    while not Params.EOS do
    begin
      s := Params.ReadString;
      m := TisStream.Create;
      Params.ReadStream(m);
      m.Position := 0;
      r := TTaxPaymentParams.Create(m);
      FParamList.AddObject(s, r);
    end;
    m := nil;
    for i := 0 to FParamList.Count - 1 do
    begin
      r := FParamList.Objects[i] as TTaxPaymentParams;
      ctx_DataAccess.OpenClient(r.ClNbr);
      DM_COMPANY.CO.DataRequired('ALL');
      FChecks.AddReportResults(r.Checks);
      while not r.AchPayments.EndOfParams do
        r.AchPayments.Read(FAch);
      while not r.NyDebitFile.EndOfParams do
        r.NyDebitFile.Read(FNy);
      while not r.MaDebitFile.EndOfParams do
        r.MaDebitFile.Read(FMa);
      while not r.CaDebitFile.EndOfParams do
        r.CaDebitFile.Read(FCa);
      while not r.EFTPS.EndOfParams do
        r.EFTPS.Read(FEFTPS);
      while not r.UnivDebitFile.EndOfParams do
        r.UnivDebitFile.Read(FUniv);
      while not r.OneMiscCheck.EndOfParams do
        r.OneMiscCheck.Read(FOneMiscCheck);
      //FWarnings.AddStrings(r.Warnings);
    end;
    m := FChecks.GetAsStream;
    Result.WriteStream(m);

    mm := FOneMiscCheck.OneCheckResult(SbTaxPaymentNbr, DefaultMiscCheckForm);
    Result.WriteStream(mm);
    if StreamIsAssigned(mm) then
     mm := FOneMiscCheck.OneCheckResultReport
    else
     mm := nil;
    Result.WriteStream(mm);

    m := TisStream.Create;
    FNy.FillStringList(l);
    l.SaveToStream(m.RealStream);
    Result.WriteStream(m);
    m.Clear;
    FMa.FillStringList(l);
    l.SaveToStream(m.RealStream);
    Result.WriteStream(m);
    m.Clear;
    FCa.FillStringList(l);
    l.SaveToStream(m.RealStream);
    Result.WriteStream(m);
    m.Clear;
    FUniv.FillStringList(l);
    l.SaveToStream(m.RealStream);
    Result.WriteStream(m);
    m.Clear;
    FEFTPS.FillStringList(l);
    l.SaveToStream(m.RealStream);
    Result.WriteStream(m);
    m.Clear;
    FAch.FillStringList(l);

    ReportId := -1;
    if (Length(PostProcessReportName) > 0)
    and (PostProcessReportName <> 'N/A') then
    begin
      ReportName := PostProcessReportName;
      ReportDB := '';
      Num := '';
      for i := Length(ReportName)-1 downto 1 do
      begin
        if ReportName[i] = '(' then
          break
        else
          Num := ReportName[i] + Num;
      end;
      if (Length(Num) > 0) and (Num[1] in ['S','B']) then
      begin
        ReportDB := Num[1];
        ReportId := StrToIntDef(copy(Num,2,100), -1);
      end;
    end;

    if ReportId > 0 then
    begin
      ReportParams := TrwReportParams.Create;
      try
        ReportParams.Add('Data', l.Text);
        ctx_RWLocalEngine.StartGroup;
        try
          ctx_RWLocalEngine.CalcPrintReport(ReportID, ReportDB, ReportParams);
        finally
          ResData := ctx_RWLocalEngine.EndGroup(rdtNone);
          Res := TrwReportResults.Create(ResData);
          try
            ReportResult := Res[0];
            if Assigned(ReportResult.Data) then
              l.Text := ReportResult.Data.AsString;
          finally
            Res.Free;
          end;
        end;
      finally
        ReportParams.Free;
      end;
    end;

    l.SaveToStream(m.RealStream);
    Result.WriteStream(m);
    m := nil;
    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(False, True, FAch.FileSaved, FAch.FileName, FAch.ACHFolder, FAch.ACHFile, FAch.ACHDescription);
    finally
      m := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;
    Result.WriteStream(m);


    //Result.WriteString(FWarnings.Text);
    Result.Position := 0;
  finally
    l.Free;
    //FWarnings.Free;
    FAch.Free;
    FNy.Free;
    FMa.Free;
    FCa.Free;
    FUniv.Free;
    FEFTPS.Free;
    FChecks.Free;
    FOneMiscCheck.Free;
    for i := 0 to FParamList.Count - 1 do
      FParamList.Objects[i].Free;
    FParamList.Free;

    DM_SERVICE_BUREAU.SB_TAX_PAYMENT.DataRequired('SB_TAX_PAYMENT_NBR = ' + IntToStr(SbTaxPaymentNbr));
    if DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('SB_TAX_PAYMENT_NBR').AsInteger = SbTaxPaymentNbr then
    begin
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Edit;
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS').AsString := SB_TAX_PAYMENT_PROCESSED;
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS_DATE').AsDateTime := Now;
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Post;
     ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_TAX_PAYMENT]);
    end;
  end;
end;

{ TMaDebitPayments }

procedure TMaDebitPayments.AddPayment(const RecordIdentifier: Char;
  const TaxPeriodEnd: TDateTime; const FEID, BusinessName: string;
  const Amount: Currency; const SettlementDate: TDateTime;
  const SbBankNbr: Integer; const BankAccNumber: string; const AccountType: Char);
begin
  FCD.AppendRecord([RecordIdentifier, TaxPeriodEnd, FEID, BusinessName, Amount,
    SettlementDate, SbBankNbr, BankAccNumber, AccountType]);
end;

constructor TMaDebitPayments.Create(AOwner: TComponent);
begin
  inherited;
  FCD := TevClientDataSet.Create(Self);
  FCD.FieldDefs.Add('RecordIdentifier', ftString, 1);
  FCD.FieldDefs.Add('TaxPeriodEnd', ftDateTime);
  FCD.FieldDefs.Add('FEIN', ftString, 40);
  FCD.FieldDefs.Add('BusinessName', ftString, 80);
  FCD.FieldDefs.Add('Amount', ftCurrency);
  FCD.FieldDefs.Add('SettlementDate', ftDateTime);
  FCD.FieldDefs.Add('SbBankNbr', ftInteger);
  FCD.FieldDefs.Add('BankAccNumber', ftString, 40);
  FCD.FieldDefs.Add('AccountType', ftString, 1);
  FCD.CreateDataSet;
  FCD.LogChanges := False;
end;

procedure TMaDebitPayments.FillStringList(const ResultList: TStringList);
var
  s: string;
begin
  ResultList.Clear;
  if FCD.RecordCount > 0 then
  begin
    ctx_DataAccess.Activate([DM_SERVICE_BUREAU.SB, DM_SERVICE_BUREAU.SB_BANKS]);
    s := StringOfChar(' ', 280);
    s := PutIntoFiller(s, 'MA941X', 1, 6);
    s := PutIntoFiller(s, Format('%6.6u', [FCD.RecordCount]), 7, 6);
    s := PutIntoFiller(s, StringReplace(DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString, '-', '', [rfReplaceAll]), 13, 9);
    s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString, 22, 30);
    s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS1').AsString + ' ' + DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS2').AsString, 52, 30);
    s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB.FieldByName('CITY').AsString, 82, 30);
    s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB.FieldByName('STATE').AsString, 112, 2);
    s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB.FieldByName('ZIP_CODE').AsString, 114, 10);
    ResultList.Append(s);
    FCd.First;
    while not FCd.Eof do
    begin
      s := StringOfChar(' ', 280);
      s[1] := FCd.FieldByName('RecordIdentifier').AsString[1];
      s := PutIntoFiller(s, Format('%6.6u', [FCD.RecNo]), 2, 6);
      s := PutIntoFiller(s, FormatDateTime('MMDDYYYY', FCd.FieldByName('TaxPeriodEnd').AsDateTime), 8, 8);
      s := PutIntoFiller(s, StringReplace(FCd.FieldByName('FEIN').AsString, '-', '', [rfReplaceAll]), 16, 9);
      s := PutIntoFiller(s, FCd.FieldByName('BusinessName').AsString, 27, 30);
      s[57] := 'N';
      s[169] := 'Y';
      Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', FCd.FieldByName('SbBankNbr').AsInteger, []));
      s := PutIntoFiller(s, FCd.FieldByName('BankAccNumber').AsString, 209, 18);
      if FCd.FieldByName('AccountType').AsString = GROUP_BOX_CHECKING then
        s := PutIntoFiller(s, '01', 227, 2)
      else
        s := PutIntoFiller(s, '02', 227, 2);
      s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB_BANKS.FieldByName('ABA_NUMBER').AsString, 170, 9);
      s := PutIntoFiller(s, DM_SERVICE_BUREAU.SB_BANKS.FieldByName('PRINT_NAME').AsString, 179, 30);
      s := PutIntoFiller(s, Format('%10.10d', [Trunc(FCd.FieldByName('Amount').AsCurrency)]), 229, 10);
      s := PutIntoFiller(s, Format('%2.2u', [Round(Abs(Frac(FCd.FieldByName('Amount').AsCurrency)) * 100)]), 239, 2);
      //s := PutIntoFiller(s, FormatDateTime('MMDDYYYY', FCd.FieldByName('SettlementDate').AsDateTime), 241, 8);
      ResultList.Append(s);
      FCd.Next;
    end;
  end;
end;

{ TCaDebitPayments }

procedure TCaDebitPayments.AddPayment(const CheckDate,
  DueDate: TDateTime; const AccountNumber, SecurityCode,
  TaxTypeCode: string; const StateWithholding, Sdi: Currency);
begin
  FCd.AppendRecord([CheckDate, DueDate, AccountNumber, SecurityCode,
    TaxTypeCode, StateWithholding, Sdi]);
end;

constructor TCaDebitPayments.Create(AOwner: TComponent);
begin
  inherited;
  FCD := TevClientDataSet.Create(Self);
  FCD.FieldDefs.Add('CheckDate', ftDateTime);
  FCD.FieldDefs.Add('DueDate', ftDateTime);
  FCD.FieldDefs.Add('AccountNumber', ftString, 18);
  FCD.FieldDefs.Add('SecurityCode', ftString, 4);
  FCD.FieldDefs.Add('TaxTypeCode', ftString, 10);
  FCD.FieldDefs.Add('StateWithholding', ftCurrency);
  FCD.FieldDefs.Add('Sdi', ftCurrency);
  FCD.CreateDataSet;
  FCD.LogChanges := False;
end;

procedure TCaDebitPayments.FillStringList(const ResultList: TStringList);
var
  s, sAccountNumber: string;
begin
  ResultList.Clear;
  FCd.First;
  while not FCd.Eof do
  begin
    sAccountNumber:=StringReplace(FCD.FieldByName('AccountNumber').AsString, ' ', '', [rfReplaceAll]);
    sAccountNumber:=StringReplace(sAccountNumber, '-', '', [rfReplaceAll]);
    s := FCD.FieldByName('TaxTypeCode').AsString + ',' +
         sAccountNumber + ',' +
         FCD.FieldByName('SecurityCode').AsString + ',' +
         FormatDateTime('MMDDYY', FCD.FieldByName('CheckDate').AsDateTime) + ',' +
         FormatDateTime('MMDDYY', FCD.FieldByName('DueDate').AsDateTime) + ',' +
         FormatCurr('0.00', FCD['Sdi']) + ',' +
         FormatCurr('0.00', FCD['StateWithholding']);
    ResultList.Append(s);
    FCd.Next;
  end;
end;

{ TUniversalDebitPayments }

procedure TUniversalDebitPayments.AddPayment(const SyGlobalAgencyNbr, ClientId,
  GroupId, MainCoNbr: Integer;
  const DueDate: TDateTime; const Amount : Currency;
  const StateLiabNbrs, SuiLiabNbrs, LocalLiabNbrs: string);
begin
  FCd.Append;
  FCd.FieldByName('SyGlobalAgencyNbr').AsInteger := SyGlobalAgencyNbr;
  FCd.FieldByName('ClientId').AsInteger := ctx_DataAccess.ClientId;
  FCd.FieldByName('GroupId').AsInteger := GroupId;
  FCd.FieldByName('MainCoNbr').AsInteger := MainCoNbr;
  FCd.FieldByName('DueDate').AsDateTime := DueDate;
  FCd.FieldByName('Amount').AsCurrency := Amount;
  FCd.FieldByName('StateLiabNbrs').AsString := StateLiabNbrs;
  FCd.FieldByName('SuiLiabNbrs').AsString := SuiLiabNbrs;
  FCd.FieldByName('LocalLiabNbrs').AsString := LocalLiabNbrs;
  FCd.Post;
end;

constructor TUniversalDebitPayments.Create(AOwner: TComponent);
begin
  FCd := TevClientDataSet.Create(Self);
  FCd.FieldDefs.Add('SyGlobalAgencyNbr', ftInteger, 0, True);
  FCd.FieldDefs.Add('ClientId', ftInteger, 0, True);
  FCd.FieldDefs.Add('GroupId', ftInteger, 0, True);
  FCd.FieldDefs.Add('MainCoNbr', ftInteger, 0, True);
  FCd.FieldDefs.Add('DueDate', ftDateTime, 0, True);
  FCd.FieldDefs.Add('Amount', ftCurrency, 0, True);
  FCd.FieldDefs.Add('StateLiabNbrs', ftBlob);
  FCd.FieldDefs.Add('SuiLiabNbrs', ftBlob);
  FCd.FieldDefs.Add('LocalLiabNbrs', ftBlob);
  FCd.CreateDataSet;
end;

procedure TUniversalDebitPayments.FillStringList(
  const ResultList: TStringList);
var
  FClientId, FGroupId, FClNbrCoNbr, FMainCoNbr, FStateLiabNbrs, FSuiLiabNbrs, FLocalLiabNbrs, FConsolidations: Variant;
  i, j: Integer;
  FParams: TrwReportParams;
  FResults: TrwReportResults;
  FResultStrings: TStringList;
  FSyAgencyNbrList: TStringList;
  FSyReportNbr: Integer;
begin
  FResultStrings := nil;
  FResults := nil;
  ResultList.Clear;
  ctx_DataAccess.Activate([DM_SYSTEM.SY_GLOBAL_AGENCY, DM_SYSTEM.SY_REPORTS]);
  FSyAgencyNbrList := FCd.GetFieldValueList('SyGlobalAgencyNbr', True);
  try
    FCd.IndexFieldNames := 'SyGlobalAgencyNbr;ClientId;MainCoNbr';
    for j := 0 to FSyAgencyNbrList.Count - 1 do
    begin
      FCd.SetRange([StrToIntDef(FSyAgencyNbrList[j], 0)], [StrToIntDef(FSyAgencyNbrList[j], 0)]);
      FSyReportNbr := StrToIntDef(DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(StrToIntDef(FSyAgencyNbrList[j], 0), 'CUSTOM_DEBIT_SY_REPORTS_NBR'), 0);
      if (FCd.RecordCount > 0) and (FSyReportNbr <> 0) then
      begin
        FClientId := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FGroupId := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FClNbrCoNbr := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FMainCoNbr := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FStateLiabNbrs := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FSuiLiabNbrs := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FLocalLiabNbrs := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        FConsolidations := VarArrayCreate([0, FCd.RecordCount - 1], varVariant);
        i := 0;
        FCd.First;
        while not FCd.Eof do
        begin
          FClientId[i] := FCd['ClientId'];
          FGroupId[i] := FCd['GroupId'];
          FMainCoNbr[i] := FCd['MainCoNbr'];
          FClNbrCoNbr[i] := Format('%u:%u', [Integer(FCd['ClientId']), Integer(FCd['MainCoNbr'])]);
          FStateLiabNbrs[i] := FCd['StateLiabNbrs'];
          FSuiLiabNbrs[i] := FCd['SuiLiabNbrs'];
          FLocalLiabNbrs[i] := FCd['LocalLiabNbrs'];
          FConsolidations[i] := True;
          FCd.Next;
          Inc(i);
        end;
        FParams := TrwReportParams.Create;
        try
          FParams.Add('SyGlobalAgencyNbr', StrToIntDef(FSyAgencyNbrList[j], 0));
          FParams.Add('Clients', FClientId);
          FParams.Add('ClientCompany', FClNbrCoNbr);
          FParams.Add('Companies', FMainCoNbr);
          FParams.Add('Consolidation', True);
          FParams.Add('Consolidations', FConsolidations);
          FParams.Add('GroupIdArray', FGroupId);
          FParams.Add('StateLiabNbrsArray', FStateLiabNbrs);
          FParams.Add('SuiLiabNbrsArray', FSuiLiabNbrs);
          FParams.Add('LocalLiabNbrsArray', FLocalLiabNbrs);
          ctx_RWLocalEngine.StartGroup;
          ctx_RWLocalEngine.CalcPrintReport(StrToIntDef(DM_SYSTEM.SY_REPORTS.ShadowDataSet.CachedLookup(FSyReportNbr, 'SY_REPORT_WRITER_REPORTS_NBR'), 0),
            CH_DATABASE_SYSTEM, FParams);
          FResults := TrwReportResults.Create(ctx_RWLocalEngine.EndGroup(rdtNone));
          Assert(FResults.Count = 1);
          Assert(Assigned(FResults[0].Data), FResults[0].ErrorMessage);
          FResultStrings := TStringList.Create;
          FResultStrings.LoadFromStream(FResults[0].Data.RealStream);
          ResultList.Append(DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(StrToIntDef(FSyAgencyNbrList[j], 0), 'AGENCY_NAME'));
          ResultList.Append(DM_SYSTEM.SY_REPORTS.ShadowDataSet.CachedLookup(FSyReportNbr, 'DESCRIPTION'));
          ResultList.Append(IntToStr(FResultStrings.Count));
          ResultList.AddStrings(FResultStrings);
        finally
          FreeAndNil(FParams);
          FreeAndNil(FResults);
          FreeAndNil(FResultStrings);
        end;
      end;
    end;
  finally
    FSyAgencyNbrList.Free;
  end;
end;


{ TOneMiscCheckPayments }

procedure TOneMiscCheckPayments.AddPayment(const SyGlobalAgencyNbr, ClientId,MainCoNbr,
                            TaxDepositId, BankAccRegNbr, FieldOfficeNbr: Integer;
                            const DueDate : TDateTime;
                            const TotalAmount : Currency; const GroupKey : String);
begin
  FCd.Append;
  FCd.FieldByName('SyGlobalAgencyNbr').AsInteger := SyGlobalAgencyNbr;
  FCd.FieldByName('ClientId').AsInteger := ctx_DataAccess.ClientID;
  FCd.FieldByName('MainCoNbr').AsInteger := MainCoNbr;
  FCd.FieldByName('TaxDepositId').AsInteger := TaxDepositId;
  FCd.FieldByName('BankAccRegNbr').AsInteger := BankAccRegNbr;
  FCd.FieldByName('FieldOfficeNbr').AsInteger := FieldOfficeNbr;
  FCd.FieldByName('DueDate').AsDateTime := DueDate;
  FCd.FieldByName('TotalAmount').AsCurrency := TotalAmount;
  FCd.FieldByName('GroupKey').AsString := GroupKey;
  FCd.FieldByName('PrMiscCheckNbr').AsInteger := 0;
  FCd.FieldByName('BarDetBankAccRegNbr').AsInteger := 0;
  FCd.Post;
end;

constructor TOneMiscCheckPayments.Create(AOwner: TComponent);
begin
  FCd := TevClientDataSet.Create(Self);
  FCd.FieldDefs.Add('SyGlobalAgencyNbr', ftInteger, 0, True);
  FCd.FieldDefs.Add('ClientId', ftInteger, 0, True);
  FCd.FieldDefs.Add('MainCoNbr', ftInteger, 0, True);
  FCd.FieldDefs.Add('TaxDepositId', ftInteger, 0, true);
  FCd.FieldDefs.Add('BankAccRegNbr', ftInteger, 0, true);
  FCd.FieldDefs.Add('FieldOfficeNbr', ftInteger, 0, true);
  FCd.FieldDefs.Add('DueDate', ftDateTime, 0, true);
  FCd.FieldDefs.Add('TotalAmount', ftCurrency, 0, true);
  FCd.FieldDefs.Add('GroupKey', ftString, 30, true);
  FCd.FieldDefs.Add('PrMiscCheckNbr', ftInteger, 0, true);
  FCd.FieldDefs.Add('BarDetBankAccRegNbr', ftInteger, 0, true);
  FCd.CreateDataSet;
  FCd.AddIndex('NBR', 'SyGlobalAgencyNbr;GroupKey', []);
  FCd.IndexDefs.Update;
  FCd.IndexName := 'NBR';

end;

function TOneMiscCheckPayments.OneCheckResultReport: IisStream;
var
 ReportParams: TrwReportParams;
begin
 Result := nil;

 if Assigned(FCd) then
 begin
  FCd.Filter :='';
  FCd.Filtered := false;
  if FCd.RecordCount > 0 then
  begin
        ReportParams := TrwReportParams.Create;
        try
          ReportParams.Add('DataSets', DataSetsToVarArray([FCd]));
          ctx_RWLocalEngine.StartGroup;
          try
            ctx_RWLocalEngine.CalcPrintReport(2143, 'S', ReportParams);
          finally
            Result := ctx_RWLocalEngine.EndGroup(rdtNone);
          end;
        finally
          ReportParams.Free;
        end;
  end;
 end;
end;


function TOneMiscCheckPayments.OneCheckResult(const SbTaxPaymentNbr: integer; const DefaultMiscCheckForm :string): IisStream;

  function CreateCoBankAccRegDetails(const BankAccRegNbr, ClNbr, ClientBankAccRegNbr : integer): Integer;
  begin
      if not DM_COMPANY.CO_BANK_ACC_REG_DETAILS.Active then
             DM_COMPANY.CO_BANK_ACC_REG_DETAILS.Open;
      DM_COMPANY.CO_BANK_ACC_REG_DETAILS.Append;
      DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger := BankAccRegNbr;
      DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CL_NBR').AsInteger := ClNbr;
      DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CLIENT_CO_BANK_ACC_REG_NBR').AsInteger := ClientBankAccRegNbr;
      DM_COMPANY.CO_BANK_ACC_REG_DETAILS.Post;
      Result := DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CO_BANK_ACC_REG_DETAILS_NBR').AsInteger;
  end;

var
 PrNbr, SyGlobalAgencyNbr, TaxDepNbr : Integer;
 GroupKey, Log : String;

begin
  Result :=nil;

  if Assigned(FCd) then
   if FCd.RecordCount > 0 then
   begin

    Assert(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger <> 0);
    ctx_DataAccess.OpenClient(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger);
    DM_SYSTEM.SY_GLOBAL_AGENCY.DataRequired('ALL');
    DM_SYSTEM.SY_GL_AGENCY_FIELD_OFFICE.DataRequired('ALL');


    ctx_DataAccess.StartNestedTransaction([dbtClient, dbtBureau]);
    try
        DM_COMPANY.CO.DataRequired('ALL');

        DM_COMPANY.CO.First;
        Assert(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger <> 0);
        PrNbr := CreateMiscCheckPayroll(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
        SyGlobalAgencyNbr := -1;
        TaxDepNbr := 0;
        GroupKey := '';
        FCd.First;
        while not FCd.Eof do
        begin
           if (SyGlobalAgencyNbr <> FCd.FieldByName('SyGlobalAgencyNbr').AsInteger) or
              (GroupKey <> FCd.FieldByName('GroupKey').AsString ) then
           begin
            TaxDepNbr := CreateNewTaxDeposit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, SbTaxPaymentNbr,
                     'Sb client total deposit', TAX_DEPOSIT_TYPE_CHECK,
                     TAX_DEPOSIT_STATUS_PAID,
                     PrNbr,
                     FCd['SyGlobalAgencyNbr'], FCd['FieldOfficeNbr']);

            CreateMiscCheck(PrNbr, MISC_CHECK_TYPE_TAX ,
                     FCd['TotalAmount'],
                      ctx_PayrollCalculation.GetNextPaymentSerialNbr,
                      '',
                       IntToStr(TaxDepNbr),
                        FCd['SyGlobalAgencyNbr'],
                         FCd['FieldOfficeNbr'], -1);

            SyGlobalAgencyNbr := FCd.FieldByName('SyGlobalAgencyNbr').AsInteger;
            GroupKey := FCd.FieldByName('GroupKey').AsString;
           end
           else
             UpdateMiscCheck(TaxDepNbr, FCd['TotalAmount']);

           FCd.Edit;
           FCd['PrMiscCheckNbr'] :=  GetPrMiscCheckNbr(TaxDepNbr);
           FCd.post;

           FCd.Next;
        end;

        ctx_DataAccess.PostDataSets(
          [DM_PAYROLL.PR,
            DM_COMPANY.CO_TAX_DEPOSITS,
             DM_PAYROLL.PR_MISCELLANEOUS_CHECKS]);

        ctx_PayrollProcessing.ProcessPayroll(PrNbr, False, Log, True, rlDoNotLock);

        DM_COMPANY.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR='+ IntToStr(PrNbr));

        if DM_COMPANY.PR_MISCELLANEOUS_CHECKS.RecordCount > 0 then
        begin
          DM_COMPANY.PR_MISCELLANEOUS_CHECKS.First;
          while not DM_COMPANY.PR_MISCELLANEOUS_CHECKS.Eof do
          begin
             DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR='+ DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString +
                    ' and CHECK_SERIAL_NUMBER =' + DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('PAYMENT_SERIAL_NUMBER').AsString);

             Assert(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.RecordCount = 1,'Missing Bank Account Register Record.');

             FCd.Filter :='PrMiscCheckNbr = '+DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString;
             FCd.Filtered := True;
             while not FCd.Eof do
             begin
                CreateCoBankAccRegDetails(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger,
                                           FCd['ClientId'], FCd['BankAccRegNbr']);

                FCd.Edit;
                FCd['BarDetBankAccRegNbr'] :=  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger;
                FCd.post;

                FCd.Next;
             end;

             DM_COMPANY.PR_MISCELLANEOUS_CHECKS.Next;
          end;
        end;

        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACC_REG_DETAILS]);

        AbortIfCurrentThreadTaskTerminated;
        ctx_DataAccess.CommitNestedTransaction;

        Result := ctx_PayrollProcessing.PrintPayroll(PrNbr, True, True, DefaultMiscCheckForm);
    except
        ctx_DataAccess.RollbackNestedTransaction;
        raise;
    end;
   end;

end;

end.

