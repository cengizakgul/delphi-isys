// Module "Misc Calculations and Routines"
unit EvTOARequest;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvStreamUtils, EVConsts, isBasicUtils;

const
  TOARequestNotesHeader = 'WR:';
  MaxTOARequestNotesLength = 40 - Length(TOARequestNotesHeader);

type
  TEvTOARequest = class(TisInterfacedObject, IEvTOARequest)
  private
    FStatus: String;
    FNotes: String;
    FHours: integer;
    FMinutes: integer;
    FEETimeOffAccrualNbr: integer;
    FEETimeOffAccrualOperNbr: integer;
    FAccrualDate: TDateTime;
    FEENbr: integer;
    FCL_E_DS_NBR: integer;

    procedure SetAccrualDate(const Value: TDateTime);
    procedure SetHours(const Value: integer);
    procedure SetMinutes(const Value: integer);
    procedure SetNotes(const Value: String);
    procedure SetEETimeOffAccrualOperNbr(const Value: integer);
    procedure SetStatus(const Value: String);
    procedure SetCL_E_DS_NBR(const Value: integer);

    function  GetEETimeOffAccrualOperNbr: integer;
    function  GetEETimeOffAccrualNbr: integer;
    function  GetAccrualDate: TDateTime;
    function  GetHours: integer;
    function  GetMinutes: integer;
    function  GetStatus: String;
    function  GetNotes: String;
    function  GetEENbr: integer;
    function  GetHoursAndMinAsDouble: Double;
    function  GetCL_E_DS_NBR: integer;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);  override;
    function  Revision: TisRevision; override;
  public
    property EENbr: integer read GetEENbr;
    property EETimeOffAccrualOperNbr: integer read GetEETimeOffAccrualOperNbr write SetEETimeOffAccrualOperNbr;
    property EETimeOffAccrualNbr: integer read GetEETimeOffAccrualNbr;
    property AccrualDate: TDateTime read GetAccrualDate write SetAccrualDate;
    property Hours: integer read GetHours write SetHours;
    property Minutes: integer read GetMinutes write SetMinutes;
    property HoursAndMinAsDouble: Double read GetHoursAndMinAsDouble;
    property Status: String read GetStatus write SetStatus;
    property Notes: String read GetNotes write SetNotes;
    property CL_E_DS_NBR: integer read GetCL_E_DS_NBR write SetCL_E_DS_NBR;

    constructor Create(const EENbr: integer; const EETimeOffAccrualNbr: integer); reintroduce;
    class function GetTypeID: String; override;
  end;

  TEvTOABusinessRequest = class(TisInterfacedObject, IEvTOABusinessRequest)
  private
    FItems: IisInterfaceList;
    FEENbr: integer;
    FBalance: Double;
    FEETimeOffAccrualNbr: integer;
    FLastName: String;
    FCustomEENumber: String;
    FDescription: String;
    FFirstName: String;
    FStatus: String;
    FNotes: String;
    FManager_Note: String;
    FDate: TDateTime;

    procedure SetBalance(const Value: Double);
    procedure SetCustomEENumber(const Value: String);
    procedure SetDate(const Value: TDateTime);
    procedure SetDescription(const Value: String);
    procedure SetEENbr(const Value: integer);
    procedure SetEETimeOffAccrualNbr(const Value: integer);
    procedure SetFirstName(const Value: String);
    procedure SetLastName(const Value: String);
    procedure SetNotes(const Value: String);
    procedure SetManagerNote(const Value: String);
    procedure SetStatus(const Value: String);

    function  GEENbr: integer;
    function  GetFirstName: String;
    function  GetLastName: String;
    function  GetCustomEENumber: String;
    function  GetEETimeOffAccrualNbr: integer;
    function  GetBalance: Double;
    function  GetDescription: String;
    function  GetStatus: String;
    function  GetDate: TDateTime;
    function  GetNotes: String;
    function  GetManagerNote: String;
    function  GetItems: IisInterfaceList;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);  override;
    function  Revision: TisRevision; override;    
  public
    property EENbr: integer read GEENbr write SetEENbr;
    property FirstName: String read GetFirstName write SetFirstName;
    property LastName: String read GetLastName write SetLastName;
    property CustomEENumber: String read GetCustomEENumber write SetCustomEENumber;
    property EETimeOffAccrualNbr: integer read GetEETimeOffAccrualNbr write SetEETimeOffAccrualNbr;
    property Balance: Double read GetBalance write SetBalance;
    property Description: String read GetDescription write SetDescription;
    property Status: String read GetStatus write SetStatus;
    property Date: TDateTime read GetDate write SetDate;
    property Notes: String read GetNotes write SetNotes;
    property Manager_Note: String read GetManagerNote write SetManagerNote;
    property Items: IisInterfaceList read GetItems;

    class function GetTypeID: String; override;
  end;

  procedure ConvertDoubleToIntegerHourAndMin(const ADouble: Double; var AHours, AMinutes: integer);

implementation

  // it rounds floating part to 0, 15, 30, 45 minutes
  procedure ConvertDoubleToIntegerHourAndMin(const ADouble: Double; var AHours, AMinutes: integer);
  var
    AllowedMinutes: array[0..3] of integer;
    iRoundedMin: integer;
    iMinDistance: integer;
    iDistance: integer;
    i: integer;
  begin
    AllowedMinutes[0] := 0; AllowedMinutes[1] := 15; AllowedMinutes[2] := 30; AllowedMinutes[3] := 45;
    AHours := Trunc(ADouble);
    AMinutes := 0;
    iRoundedMin := Round(FRac(ADouble) * 60);
    iMinDistance := MaxInt;

    for i := 0 to Length(AllowedMinutes) - 1 do
    begin
      iDistance := Abs(iRoundedMin - AllowedMinutes[i]);
      if iDistance < iMinDistance then
      begin
        iMinDistance := iDistance;
        AMinutes := AllowedMinutes[i];
      end;
    end;  // for

    if iMinDistance > 1 then
      raise Exception.Create('Wrong value of minutes: ' + IntToStr(iRoundedMin) + '. Allowed values: 0, 15, 30, 45');
  end;

{ TEvTOARequest }

constructor TEvTOARequest.Create(const EENbr,  EETimeOffAccrualNbr: integer);
begin
  inherited Create;

  CheckCondition(EETimeOffAccrualNbr > 0, 'EE_TIME_OFF_ACCRUAL_NBR cannot be zero or below zero');
  CheckCondition(EENbr > 0, 'EE_NBR cannot be zero or below zero');
  FEENbr := EENbr;
  FEETimeOffAccrualNbr := EETimeOffAccrualNbr;
end;

procedure TEvTOARequest.DoOnConstruction;
begin
  inherited;
  FStatus := EE_TOA_OPER_CODE_REQUEST_PENDING;
end;

function TEvTOARequest.GetAccrualDate: TDateTime;
begin
  Result := FAccrualDate;
end;

function TEvTOARequest.GetCL_E_DS_NBR: integer;
begin
  Result := FCL_E_DS_NBR;
end;

function TEvTOARequest.GetEENbr: integer;
begin
  Result := FEENbr;
end;

function TEvTOARequest.GetEETimeOffAccrualNbr: integer;
begin
  Result := FEETimeOffAccrualNbr;
end;

function TEvTOARequest.GetEETimeOffAccrualOperNbr: integer;
begin
  Result := FEETimeOffAccrualOperNbr;
end;

function TEvTOARequest.GetHours: integer;
begin
  Result := FHours;
end;

function TEvTOARequest.GetHoursAndMinAsDouble: Double;
begin
  Result := FHours + FMinutes/60;
end;

function TEvTOARequest.GetMinutes: integer;
begin
  Result := FMinutes;
end;

function TEvTOARequest.GetNotes: String;
begin
  Result := FNotes;
end;

function TEvTOARequest.GetStatus: String;
begin
  Result := FStatus;
end;

class function TEvTOARequest.GetTypeID: String;
begin
  Result := 'EvTOARequest';
end;

procedure TEvTOARequest.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FStatus := AStream.ReadString;
  FNotes := AStream.ReadString;
  FHours := AStream.ReadInteger;
  FEETimeOffAccrualNbr := AStream.ReadInteger;
  FEETimeOffAccrualOperNbr := AStream.ReadInteger;
  FAccrualDate := AStream.ReadDouble;
  FEENbr := AStream.ReadInteger;
  if ARevision > 0 then
    Minutes := AStream.ReadInteger;
  if ARevision > 1 then
    FCL_E_DS_NBR := AStream.ReadInteger;

end;

function TEvTOARequest.Revision: TisRevision;
begin
  Result := 2;
end;

procedure TEvTOARequest.SetAccrualDate(const Value: TDateTime);
begin
  CheckCondition(Trunc(Value) = Value, 'Dates cannot include time');
  FAccrualDate := Value;
end;

procedure TEvTOARequest.SetCL_E_DS_NBR(const Value: integer);
begin
  FCL_E_DS_NBR := Value;
end;

procedure TEvTOARequest.SetEETimeOffAccrualOperNbr(const Value: integer);
begin
  FEETimeOffAccrualOperNbr := Value;
end;

procedure TEvTOARequest.SetHours(const Value: integer);
begin
  CheckCondition(Value >= 0, 'Hours cannot be below zero');
  FHours := Value;
end;

procedure TEvTOARequest.SetMinutes(const Value: integer);
begin
  CheckCondition((Value = 0) or (Value = 15) or (Value = 30) or (Value = 45), 'Allowed values: 0, 15, 30, 45 minutes'); 
  FMinutes := Value;
end;

procedure TEvTOARequest.SetNotes(const Value: String);
begin
  FNotes := Copy(Trim(Value), 1, MaxTOARequestNotesLength);
end;

procedure TEvTOARequest.SetStatus(const Value: String);
begin
  CheckCondition((Value = EE_TOA_OPER_CODE_REQUEST_PENDING) or (Value = EE_TOA_OPER_CODE_REQUEST_APPROVED) or
    (Value = EE_TOA_OPER_CODE_REQUEST_DENIED) or (Value = EE_TOA_OPER_CODE_USED), 'Not supported value for status: "' + Value + '"');
  FStatus := Value;
end;

procedure TEvTOARequest.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FStatus);
  AStream.WriteString(FNotes);
  AStream.WriteInteger(FHours);
  AStream.WriteInteger(FEETimeOffAccrualNbr);
  AStream.WriteInteger(FEETimeOffAccrualOperNbr);
  AStream.WriteDouble(FAccrualDate);
  AStream.WriteInteger(FEENbr);
  AStream.WriteInteger(FMinutes);
  AStream.WriteInteger(FCL_E_DS_NBR);
end;

{ TEvTOABusinessRequest }

procedure TEvTOABusinessRequest.DoOnConstruction;
begin
  inherited;
  FItems := TisInterfaceList.Create;
end;

function TEvTOABusinessRequest.GEENbr: integer;
begin
  Result := FEENbr;
end;

function TEvTOABusinessRequest.GetBalance: Double;
begin
  Result := FBalance;
end;

function TEvTOABusinessRequest.GetCustomEENumber: String;
begin
  Result := FCustomEENumber;
end;

function TEvTOABusinessRequest.GetDate: TDateTime;
begin
  Result := FDate;
end;

function TEvTOABusinessRequest.GetDescription: String;
begin
  Result := FDescription;
end;

function TEvTOABusinessRequest.GetEETimeOffAccrualNbr: integer;
begin
  Result := FEETimeOffAccrualNbr;
end;

function TEvTOABusinessRequest.GetFirstName: String;
begin
  Result := FFirstName;
end;

function TEvTOABusinessRequest.GetItems: IisInterfaceList;
begin
  Result := FItems;
end;

function TEvTOABusinessRequest.GetLastName: String;
begin
  Result := FLastName;
end;

function TEvTOABusinessRequest.GetManagerNote: String;
begin
  Result := FManager_Note;
end;

function TEvTOABusinessRequest.GetNotes: String;
begin
  Result := FNotes;
end;

function TEvTOABusinessRequest.GetStatus: String;
begin
  Result := FStatus;
end;

class function TEvTOABusinessRequest.GetTypeID: String;
begin
  Result := 'EvTOABusinessRequest';
end;

procedure TEvTOABusinessRequest.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FItems as IisInterfacedObject).ReadFromStream(AStream);
  FEENbr := AStream.ReadInteger;
  FBalance := AStream.ReadDouble;
  FEETimeOffAccrualNbr := AStream.ReadInteger;
  FLastName := AStream.ReadString;
  FCustomEENumber := AStream.ReadString;
  FDescription := AStream.ReadString;
  FFirstName := AStream.ReadString;
  FStatus := AStream.ReadString;
  FNotes := AStream.ReadString;
  FDate := AStream.ReadDouble;
  if ARevision > 0 then
     FManager_Note := AStream.ReadString;
end;

function TEvTOABusinessRequest.Revision: TisRevision;
begin
  Result := 1;
end;

procedure TEvTOABusinessRequest.SetBalance(const Value: Double);
begin
  FBalance := Value;
end;

procedure TEvTOABusinessRequest.SetCustomEENumber(const Value: String);
begin
  FCustomEENumber := Value;
end;

procedure TEvTOABusinessRequest.SetDate(const Value: TDateTime);
begin
  FDate := Value;
end;

procedure TEvTOABusinessRequest.SetDescription(const Value: String);
begin
  FDescription := Value;
end;

procedure TEvTOABusinessRequest.SetEENbr(const Value: integer);
begin
  FEENbr := Value;
end;

procedure TEvTOABusinessRequest.SetEETimeOffAccrualNbr(
  const Value: integer);
begin
  FEETimeOffAccrualNbr := Value;
end;

procedure TEvTOABusinessRequest.SetFirstName(const Value: String);
begin
  FFirstName := Value;
end;

procedure TEvTOABusinessRequest.SetLastName(const Value: String);
begin
  FLastName := Value;
end;

procedure TEvTOABusinessRequest.SetManagerNote(const Value: String);
begin
  FManager_Note := Value;
end;

procedure TEvTOABusinessRequest.SetNotes(const Value: String);
begin
  FNotes := Value;
end;

procedure TEvTOABusinessRequest.SetStatus(const Value: String);
begin
  CheckCondition((Value = EE_TOA_OPER_CODE_REQUEST_PENDING) or (Value = EE_TOA_OPER_CODE_REQUEST_APPROVED) or
    (Value = EE_TOA_OPER_CODE_REQUEST_DENIED) or (Value = EE_TOA_OPER_CODE_USED), 'Not supported value for status: "' + Value + '"');
  FStatus := Value;
end;

procedure TEvTOABusinessRequest.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FItems as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteInteger(FEENbr);
  AStream.WriteDouble(FBalance);
  AStream.WriteInteger(FEETimeOffAccrualNbr);
  AStream.WriteString(FLastName);
  AStream.WriteString(FCustomEENumber);
  AStream.WriteString(FDescription);
  AStream.WriteString(FFirstName);
  AStream.WriteString(FStatus);
  AStream.WriteString(FNotes);
  AStream.WriteDouble(FDate);
  AStream.WriteString(FManager_Note);
end;

initialization
  ObjectFactory.Register([TEvTOARequest, TEvTOABusinessRequest]);

finalization
  SafeObjectFactoryUnRegister([TEvTOARequest, TEvTOABusinessRequest]);
end.
