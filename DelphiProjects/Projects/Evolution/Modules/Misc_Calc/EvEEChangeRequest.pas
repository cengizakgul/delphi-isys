// Module "Misc Calculations and Routines"
unit EvEEChangeRequest;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvStreamUtils, EVConsts, isBasicUtils,
  EvClasses, DB, EvTypes, EvClientDataSet;

type
  TEvEEChangeRequest = class(TisInterfacedObject, IevEEChangeRequest)
  private
    FEENbr: integer;
    FCustomEENumber: String;
    FEEFirstName: String;
    FEELastName: String;
    FEEChangeRequestNbr: integer;
    FRequestData: IevDataChangePacket;
    FRequestType: TevEEChangeRequestType;
    FOldFormatRequestData: String;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);  override;

    // IevEEChangeRequest
    function  GetEENbr: integer;
    function  GetCustomEENumber: String;
    procedure SetCustomEENumber(const ACustomEENumber: String);
    function  GetEEFirstName: String;
    procedure SetEEFirstName(const AEEFirstName: String);
    function  GetEELastName: String;
    procedure SetEELastName(const AEELastName: String);
    function  GetRequestData: IevDataChangePacket;
    procedure SetRequestData(const ARequestData: IevDataChangePacket);
    function  GetEEChangeRequestNbr: integer;
    procedure SetEEChangeRequestNbr(const AEEChangeRequestNbr: integer);
    function  GetEEChangeRequestType: TevEEChangeRequestType;
    procedure SetEEChangeRequestType(const AValue: TevEEChangeRequestType);
    function  GetOldFormatRequestData: String;
    procedure SetOldFormatRequestData(const AValue: String);

    property  EENbr: integer read GetEENbr;
    property  CustomEENumber: String read GetCustomEENumber write SetCustomEENumber;
    property  EEFirstName: String read GetEEFirstName write SetEEFirstName;
    property  EELastName: String read GetEELastName write SetEELastName;
    property  EEChangeRequestNbr: integer read GetEEChangeRequestNbr write SetEEChangeRequestNbr;
    property  EEChangeRequestType: TevEEChangeRequestType read GetEEChangeRequestType write SetEEChangeRequestType;
    property  RequestData: IevDataChangePacket read GetRequestData write SetRequestData;
    property  OldFormatRequestData: String read GetOldFormatRequestData write SetOldFormatRequestData;
  public
    constructor Create(const EENbr: integer); reintroduce;
    class function GetTypeID: String; override;
  end;

  // creates object and fills it up from current record of dataset (dataset has to be filled from EE_CHANGE_REQUEST table)
  // doesn't fill up First Name, Last Name and CustomEENumber
  // if data is in old format, RequestData = nil and OldFormatRequestData = string representaion of request in xml format
  function LoadEEChangeRequestFromDataset(const AEEChangeRequestTable: TDataSet): IevEEChangeRequest;
  
  function EEChangeRequestTypeToString(const ARequestType: TevEEChangeRequestType): String;
  function EEChangeRequestStringToType(const ARequestType: String): TevEEChangeRequestType;

implementation

function EEChangeRequestTypeToString(const ARequestType: TevEEChangeRequestType): String;
begin
  if ARequestType = tpHRChangeRequest then
    Result := 'H'
  else if ARequestType = tpBenefitRequest then
    Result := 'B'
  else
    raise Exception.Create('Unsupported type of EE change request');
end;

function EEChangeRequestStringToType(const ARequestType: String): TevEEChangeRequestType;
begin
  if ARequestType = 'H' then
    Result := tpHRChangeRequest
  else if ARequestType = 'B' then
    Result := tpBenefitRequest
  else
    raise Exception.Create('Unsupported type of EE change request');
end;

function LoadEEChangeRequestFromDataset(const AEEChangeRequestTable: TDataSet): IevEEChangeRequest;
var
  iEENbr: integer;
  tmpStream: IisStream;
  ListFromBlob: IisListOfValues;
  sXML: String;
begin
  iEENbr := AEEChangeRequestTable['EE_NBR'];
  Result := TEvEEChangeRequest.Create(iEENbr);
  Result.EEChangeRequestType := EEChangeRequestStringToType(AEEChangeRequestTable['REQUEST_TYPE']);
  Result.EEChangeRequestNbr := AEEChangeRequestTable['EE_CHANGE_REQUEST_NBR'];

  // reading RequestData from BLOB field
  tmpStream := TevClientDataSet(AEEChangeRequestTable).GetBlobData('REQUEST_DATA');

  // trying to read
  try
    ListFromBlob := TisListOfValues.Create;
    ListFromBlob.ReadFromStream(tmpStream);
    CheckCondition(ListFromBlob.Count > 0, 'No REQUEST_DATA found');

    if ListFromBlob.ValueExists('1') then
    begin
      // new format
      Result.RequestData := IInterface(ListFromBlob.Value['1']) as IevDataChangePacket;
      CheckCondition(Assigned(Result.RequestData), 'Request data cannot be not assigned');
      CheckCondition(Result.RequestData.Count > 0, 'Request cannot be empty');
    end
    else if ListFromBlob.ValueExists('0') then
    begin
      // old fomat - XML
      sXML := ListFromBlob.Value['0'];
      Result.OldFormatRequestData := sXML;
    end
    else
      raise Exception.Create('Unknown format of data');
  except
    on E: Exception do
      raise Exception.Create('Cannot read request''s data. Error: ' + E.Message);
  end;
end;

{ TEvEEChangeRequest }

constructor TEvEEChangeRequest.Create(const EENbr: integer);
begin
  FEENbr := EENbr;
end;

procedure TEvEEChangeRequest.DoOnConstruction;
begin
  inherited;
  FRequestData := TevDataChangePacket.Create;
  FRequestType := tpHRChangeRequest;
end;

function TEvEEChangeRequest.GetCustomEENumber: String;
begin
  Result := FCustomEENumber;
end;

function TEvEEChangeRequest.GetEEChangeRequestNbr: integer;
begin
  Result := FEEChangeRequestNbr;
end;

function TEvEEChangeRequest.GetEEChangeRequestType: TevEEChangeRequestType;
begin
  Result := FRequestType;
end;

function TEvEEChangeRequest.GetEEFirstName: String;
begin
  Result := FEEFirstName;
end;

function TEvEEChangeRequest.GetEELastName: String;
begin
  Result := FEELastName;
end;

function TEvEEChangeRequest.GetEENbr: integer;
begin
  Result := FEENbr;
end;

function TEvEEChangeRequest.GetOldFormatRequestData: String;
begin
  Result := FOldFormatRequestData;
end;

function TEvEEChangeRequest.GetRequestData: IevDataChangePacket;
begin
  Result := FRequestData;
end;

class function TEvEEChangeRequest.GetTypeID: String;
begin
  Result := 'EvEEChangeRequest';
end;

procedure TEvEEChangeRequest.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEENbr := AStream.ReadInteger;
  FCustomEENumber := AStream.ReadString;
  FEEFirstName := AStream.ReadString;
  FEELastName := AStream.ReadString;
  FEEChangeRequestNbr := AStream.ReadInteger;
  FRequestType := TevEEChangeRequestType(AStream.ReadInteger);
  FOldFormatRequestData := AStream.ReadString;
  FRequestData := Astream.ReadObject(nil) as IevDataChangePacket;
end;

procedure TEvEEChangeRequest.SetCustomEENumber(const ACustomEENumber: String);
begin
  FCustomEENumber := ACustomEENumber;
end;

procedure TEvEEChangeRequest.SetEEChangeRequestNbr(const AEEChangeRequestNbr: integer);
begin
  FEEChangeRequestNbr := AEEChangeRequestNbr;
end;

procedure TEvEEChangeRequest.SetEEChangeRequestType(const AValue: TevEEChangeRequestType);
begin
  FRequestType := AValue;
end;

procedure TEvEEChangeRequest.SetEEFirstName(const AEEFirstName: String);
begin
  FEEFirstName := AEEFirstName;
end;

procedure TEvEEChangeRequest.SetEELastName(const AEELastName: String);
begin
  FEELastName := AEELastName;
end;

procedure TEvEEChangeRequest.SetOldFormatRequestData(const AValue: String);
begin
  FOldFormatRequestData := Avalue;
  FRequestData := nil;
end;

procedure TEvEEChangeRequest.SetRequestData(const ARequestData: IevDataChangePacket);
begin
  FRequestData := ARequestData;
  FOldFormatRequestData := '';
end;

procedure TEvEEChangeRequest.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FEENbr);
  AStream.WriteString(FCustomEENumber);
  AStream.WriteString(FEEFirstName);
  AStream.WriteString(FEELastName);
  AStream.WriteInteger(FEEChangeRequestNbr);
  AStream.WriteInteger(Integer(FRequestType));
  AStream.WriteString(FOldFormatRequestData);
  AStream.WriteObject(FRequestData);
end;

initialization
  ObjectFactory.Register([TEvEEChangeRequest]);

finalization
  SafeObjectFactoryUnRegister([TEvEEChangeRequest]);
end.
