// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SGrossToNet;

interface

uses
  Windows, Messages, SysUtils, Classes, EvBasicUtils, Db,  Variants,
  EvContext, EvExceptions, DateUtils, Types, ISBasicUtils, Math, isBaseClasses, EvClientDataSet, ISDataSetExplorerFrm;

type

  TevGenericGrossToNet = class
  private
    NegCheckAllowed:    Boolean;
    YTDsDisabled:       Boolean;
    ShortfallsDisabled: Boolean;
    TaxCalculator:      Boolean;
    FSecondCheck:       Boolean;
    FNet: Double;
    procedure SetNet(const Value: Double);
    procedure CalcEESDITaxes(CheckNumber, COStateNumber: Integer;
                             var SDITax, SDITaxWages, SDIGrossWages: Real;
                             PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES: TevClientDataSet);
    procedure CalcEICTaxes(CheckNumber: Integer; var EICTax: Real; PR_CHECK: TevClientDataSet);
    procedure CalcEEMedicareTaxes(CheckNumber: Integer; var MedicareTax, MedicareTaxWages, MedicareGrossWages: Real;
                                  PR, PR_CHECK, PR_CHECK_LINES: TevClientDataSet);
    procedure CalcEEOASDITaxes(CheckNumber: Integer; var OASDITax, OASDITaxWages, OASDITaxTips, OASDIGrossWages: Real;
                               PR, PR_CHECK, PR_CHECK_LINES: TevClientDataSet);
    procedure CalcStateTaxes(var Warnings: String; CheckNumber, COStateNumber: Integer;
                             var StateTax, StateTaxWages, RecipStateTax: Real;
                             var RecipEEStateNumber: Integer;
                             PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES: TevClientDataSet);
    procedure CalcFedTaxes(CheckNumber: Integer; var FedTax, FedTaxWages: Real;
                           PR_CHECK, PR_CHECK_LINES, PR: TevClientDataSet);
    function  CalcFedTaxAmount(AnnualFedTaxWages: Real; Dependents: Byte; MaritalStatus: string): Real;
    property Net: Double read FNet write SetNet;
  public
    // Result indicates if there were any shortfalls
    function Calculate(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction, FetchPayrollData: Boolean;
               PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS: TevClientDataSet;
               PayrollIsProcessing: Boolean = False; JustPreProcessing: Boolean = True; ForTaxCalculator: Boolean = False;
               DisableYTDs: Boolean = False; DisableShortfalls: Boolean = False; SecondCheck: Boolean = False; NetToGrossCalc: Boolean = False): Boolean;

  end;

implementation

uses
  EvUtils, EvTypes, SDataStructure, EvConsts, SPD_DM_HISTORICAL_TAXES,
  STaxCalculations, EvCommonInterfaces, EvDataset, EvDataAccessComponents;

type
  SpecTaxedDed = record
    LineNbr: Integer;
    Amount: Real;
  end;

  ptrSpecTaxedDed = ^SpecTaxedDed;

  TSpecTaxedDeds = class
  private
    DedsList: TList;
    function FindItem(LineNbr: Integer): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function Recalculated(LineNbr: Integer; Amount: Real): Boolean;
  end;



constructor TSpecTaxedDeds.Create;
begin
  DedsList := TList.Create;
end;

destructor TSpecTaxedDeds.Destroy;
var
  i: Integer;
begin
  for i := 0 to DedsList.Count - 1 do
    Dispose(ptrSpecTaxedDed(DedsList[i]));
  DedsList.Free;
  inherited;
end;

function TSpecTaxedDeds.FindItem(LineNbr: Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to DedsList.Count - 1 do
    if ptrSpecTaxedDed(DedsList.Items[I])^.LineNbr = LineNbr then
    begin
      Result := I;
      Exit;
    end;
end;

function TSpecTaxedDeds.Recalculated(LineNbr: Integer; Amount: Real): Boolean;
var
  TempSpecTaxedDed: ptrSpecTaxedDed;
  I: Integer;
begin
  Result := True;
  I := FindItem(LineNbr);
  if I = -1 then
  begin
    New(TempSpecTaxedDed);
    TempSpecTaxedDed^.LineNbr := LineNbr;
    TempSpecTaxedDed^.Amount := Amount;
    DedsList.Add(TempSpecTaxedDed);
  end
  else
  begin
    if ptrSpecTaxedDed(DedsList.Items[I])^.Amount <> Amount then
      ptrSpecTaxedDed(DedsList.Items[I])^.Amount := Amount
    else
      Result := False;
  end;
end;

{ SR-90.10.B }

function TevGenericGrossToNet.CalcFedTaxAmount(AnnualFedTaxWages: Real; Dependents: Byte; MaritalStatus: string): Real;
var
  TaxableAmount, TempTax: Real;
begin
  with DM_HISTORICAL_TAXES do
  begin
    TaxableAmount := AnnualFedTaxWages - Dependents * SY_FED_TAX_TABLE.FieldByName('EXEMPTION_AMOUNT').Value;
    TempTax := 0;

    SY_FED_TAX_TABLE_BRACKETS.IndexFieldNames := 'MARITAL_STATUS;LESS_THAN_VALUE';
    SY_FED_TAX_TABLE_BRACKETS.SetRange([MaritalStatus[1]], [MaritalStatus[1]]);
    try
      SY_FED_TAX_TABLE_BRACKETS.First;
      while not SY_FED_TAX_TABLE_BRACKETS.EOF do
      begin
        if TaxableAmount <= SY_FED_TAX_TABLE_BRACKETS.FieldByName('LESS_THAN_VALUE').Value then
        begin
          TempTax := TempTax + (TaxableAmount - SY_FED_TAX_TABLE_BRACKETS.FieldByName('GREATER_THAN_VALUE').Value)
            * SY_FED_TAX_TABLE_BRACKETS.FieldByName('PERCENTAGE').Value;
          break;
        end;
        TempTax := TempTax + (SY_FED_TAX_TABLE_BRACKETS.FieldByName('LESS_THAN_VALUE').Value
          - SY_FED_TAX_TABLE_BRACKETS.FieldByName('GREATER_THAN_VALUE').Value)
          * SY_FED_TAX_TABLE_BRACKETS.FieldByName('PERCENTAGE').Value;
        SY_FED_TAX_TABLE_BRACKETS.Next;
      end;
    finally
      SY_FED_TAX_TABLE_BRACKETS.CancelRange;
      SY_FED_TAX_TABLE_BRACKETS.IndexFieldNames := '';
    end;
    Result := TempTax;
  end;
end;

{SR-90, SR-90.10}

procedure TevGenericGrossToNet.CalcFedTaxes(CheckNumber: Integer; var FedTax, FedTaxWages: Real; PR_CHECK, PR_CHECK_LINES, PR: TevClientDataSet);
var
  AdjFedTaxWages, NonResidentWages: Real;
  EDCodeType, TaxORType: string;
  TaxFreq, EENumber: Integer;

  procedure CalcFedTaxesCont;
  var
    Freq: Integer;
    IsNonResidentAlien: Boolean;
    NumberOfDependents: Integer;
    ExemptionAmount: Real;
    AnnualAmount: Real;
  begin
// SR-90.10
    if (PR_CHECK.FieldByName('EXCLUDE_FEDERAL').Value = 'Y') or (DM_HISTORICAL_TAXES.EE.FieldByName('EXEMPT_EXCLUDE_EE_FED').AsString = 'X') then
    begin
      FedTax := 0;
      Exit;
    end;

    TaxFreq := GetTaxFreq(PR_CHECK.FieldByName('TAX_FREQUENCY').Value);

    if TaxFreq = 365 then
      Freq := 260
    else
      Freq := TaxFreq;

    if not PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull then
    begin
      TaxORType := PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value;
      case TaxORType[1] of
        OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT:
          begin
            FedTax := PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value;
            Exit;
          end;
        OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
          begin
            FedTax := FedTax + PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value * AdjFedTaxWages * 0.01;
            Exit;
          end;
        OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT:
          FedTax := FedTax + PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value;
        OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT:
          FedTax := FedTax + PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value * AdjFedTaxWages * 0.01;
      end;
    end
    else
    if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_3RD_PARTY]) and not DM_HISTORICAL_TAXES.EE.FieldByName('OVERRIDE_FED_TAX_VALUE').IsNull then
    begin
      TaxORType := DM_HISTORICAL_TAXES.EE.FieldByName('OVERRIDE_FED_TAX_TYPE').Value;
      if (PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value = EXCLUDE_TAXES_NONE) or
      (PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value = EXCLUDE_TAXES_ADDITIONAL) then
        case TaxORType[1] of
          OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT:
            begin
              FedTax := DM_HISTORICAL_TAXES.EE.FieldByName('OVERRIDE_FED_TAX_VALUE').Value;
              Exit;
            end;
          OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
            begin
              FedTax := FedTax + DM_HISTORICAL_TAXES.EE.FieldByName('OVERRIDE_FED_TAX_VALUE').Value * AdjFedTaxWages * 0.01;
              Exit;
            end;
        end;
      if (PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value = EXCLUDE_TAXES_NONE) or
      (PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value = EXCLUDE_TAXES_OVERRIDE_REGULAR) then
        case TaxORType[1] of
          OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT:
            FedTax := FedTax + DM_HISTORICAL_TAXES.EE.FieldByName('OVERRIDE_FED_TAX_VALUE').Value;
          OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT:
            FedTax := FedTax + DM_HISTORICAL_TAXES.EE.FieldByName('OVERRIDE_FED_TAX_VALUE').Value * AdjFedTaxWages * 0.01;
        end;
    end;

    if AdjFedTaxWages < 0 then
      Exit;

    IsNonResidentAlien := (ConvertNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'), 'VISA_NUMBER'), '') <> '')
      and (DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'), 'VISA_TYPE') <> VISA_TYPE_NONE)
      and (DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'), 'VISA_TYPE') <> VISA_TYPE_H1B_RES)
      and not VarIsNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'), 'VISA_EXPIRATION_DATE'))
      and (DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'), 'VISA_EXPIRATION_DATE') >= DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);

    if IsNonResidentAlien then
    begin
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2015, 12, 31)) = GreaterThanValue) then
        AdjFedTaxWages := AdjFedTaxWages + 2250 / TaxFreq
      else
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2014, 12, 31)) = GreaterThanValue) then
        AdjFedTaxWages := AdjFedTaxWages + 2300 / TaxFreq
      else
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2013, 12, 31)) = GreaterThanValue) then
        AdjFedTaxWages := AdjFedTaxWages + 2250 / TaxFreq
      else
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue) then
        AdjFedTaxWages := AdjFedTaxWages + 2200 / TaxFreq
      else
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2011, 12, 31)) = GreaterThanValue) then
        AdjFedTaxWages := AdjFedTaxWages + 2150 / TaxFreq
      else
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2010, 12, 31)) = GreaterThanValue) then
        AdjFedTaxWages := AdjFedTaxWages + 2100 / TaxFreq
      else if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2009, 12, 31)) = GreaterThanValue) then
        // For non-resident aliens for wages paid in 2010
        AdjFedTaxWages := AdjFedTaxWages + 2050 / TaxFreq
      else
        AdjFedTaxWages := AdjFedTaxWages + 7180 / Freq; // New provision for non-resident aliens
    end;

    if (PR_CHECK.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value = 'Y') and
    (ConvertNull(DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE.FieldByName('SUPPLEMENTAL_TAX_PERCENTAGE').Value, 0) <> 0) then
      FedTax := AdjFedTaxWages * DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE.FieldByName('SUPPLEMENTAL_TAX_PERCENTAGE').Value
    else
    if IsNonResidentAlien then
    begin
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2010, 12, 31)) = GreaterThanValue) then
      begin
        // For non-resident aliens for wages paid in 2010
        if TaxCalculator then
          NumberOfDependents := PR_CHECK.FieldByName('NUMBER_OF_DEPENDENTS').Value
        else
          NumberOfDependents := DM_HISTORICAL_TAXES.EE.FieldByName('NUMBER_OF_DEPENDENTS').Value;

        AnnualAmount := AdjFedTaxWages * Freq;

        if TaxCalculator then
          FedTax := FedTax + CalcFedTaxAmount(AnnualAmount, NumberOfDependents,
            PR_CHECK.FieldByName('FEDERAL_MARITAL_STATUS').Value) / Freq
        else
          FedTax := FedTax + CalcFedTaxAmount(AnnualAmount, NumberOfDependents,
            DM_HISTORICAL_TAXES.EE.FieldByName('FEDERAL_MARITAL_STATUS').Value) / Freq;

      end
      else
      if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2009, 12, 31)) = GreaterThanValue) then
      begin
        // For non-resident aliens for wages paid in 2010

        if TaxCalculator then
        begin
          NumberOfDependents := PR_CHECK.FieldByName('NUMBER_OF_DEPENDENTS').Value;
          ExemptionAmount := DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('EXEMPTION_AMOUNT').Value;
        end
        else
        begin
          NumberOfDependents := DM_HISTORICAL_TAXES.EE.FieldByName('NUMBER_OF_DEPENDENTS').Value;
          ExemptionAmount := DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE.FieldByName('EXEMPTION_AMOUNT').Value;
        end;

        NonResidentWages := AdjFedTaxWages * Freq - NumberOfDependents * ExemptionAmount;

        if TaxCalculator then
          FedTax := FedTax + CalcFedTaxAmount(AdjFedTaxWages * Freq, NumberOfDependents,
            PR_CHECK.FieldByName('FEDERAL_MARITAL_STATUS').Value) / Freq
        else
          FedTax := FedTax + CalcFedTaxAmount(AdjFedTaxWages * Freq, NumberOfDependents,
            DM_HISTORICAL_TAXES.EE.FieldByName('FEDERAL_MARITAL_STATUS').Value) / Freq;

        if DoubleCompare(NonResidentWages, coGreater, 2050) and DoubleCompare(NonResidentWages, coLess, 6050) then
          FedTax := FedTax + (NonResidentWages - 2050) * 0.1 / Freq
        else
        if DoubleCompare(NonResidentWages, coGreaterOrEqual, 6050) and DoubleCompare(NonResidentWages, coLess, 67700) then
          FedTax := FedTax + 400 / Freq
        else
        if DoubleCompare(NonResidentWages, coGreaterOrEqual, 67700) and DoubleCompare(NonResidentWages, coLess, 87700) then
          FedTax := FedTax + (400 - (NonResidentWages - 67700) * 0.02) / Freq;
      end;
    end
    else
    begin
      if TaxCalculator then
        FedTax := FedTax + CalcFedTaxAmount(AdjFedTaxWages * Freq,
          PR_CHECK.FieldByName('NUMBER_OF_DEPENDENTS').Value,
          PR_CHECK.FieldByName('FEDERAL_MARITAL_STATUS').Value) / Freq
      else
        FedTax := FedTax + CalcFedTaxAmount(AdjFedTaxWages * Freq,
          DM_HISTORICAL_TAXES.EE.FieldByName('NUMBER_OF_DEPENDENTS').Value,
          DM_HISTORICAL_TAXES.EE.FieldByName('FEDERAL_MARITAL_STATUS').Value) / Freq;
    end;

  end;

var
  Q: IevQuery;
const
  FederalShortFall = 'select x.PR_CHECK_NBR, x.FEDERAL_SHORTFALL ' +
      'from PR_CHECK x ' +
      'join PR p on p.PR_NBR=x.PR_NBR ' +
      'join PR_CHECK z on z.PR_CHECK_NBR=:CheckNbr ' +
      'join PR d on d.PR_NBR=z.PR_NBR ' +
      'where x.EE_NBR=:EENbr and EXTRACT(year from p.CHECK_DATE) = EXTRACT(year from d.CHECK_DATE) and x.PR_CHECK_NBR=(select max(y.PR_CHECK_NBR) ' +
      'from PR_CHECK y ' +
      'where y.EE_NBR=:EENbr and ' +
      'y.PR_CHECK_NBR<:CheckNbr and ' +
      'y.DISABLE_SHORTFALLS=''N'' and ' +
      'y.CHECK_TYPE in (#CheckType) and ' +
      '{AsOfNow<y>}) and {AsOfNow<x>} and {AsOfNow<p>} and {AsOfNow<z>} and {AsOfNow<d>';
   
begin
// SR-90
  FedTax := 0;
  FedTaxWages := 0;
  with DM_HISTORICAL_TAXES do
  begin
    if DM_COMPANY.CO.FieldByName('FEDERAL_TAX_EXEMPT_STATUS').Value = 'Y' then Exit;
    EENumber := PR_CHECK.FieldByName('EE_NBR').Value;
    if EE.FieldByName('EXEMPT_EXCLUDE_EE_FED').AsString = 'E' then Exit;
    AdjFedTaxWages := 0;
    PR_CHECK_LINES.First;
    with PR_CHECK_LINES do
      while not EOF do
      begin
        EDCodeType := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
        if TypeIsTaxable(EDCodeType) then
          if SY_FED_EXEMPTIONS.NewLookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_FEDERAL') <> 'E' then
            if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'EE_EXEMPT_EXCLUDE_FEDERAL') <> 'E' then
            begin
              FedTaxWages := FedTaxWages + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
              if SY_FED_EXEMPTIONS.NewLookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_FEDERAL') <> 'X' then
                if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'EE_EXEMPT_EXCLUDE_FEDERAL') <> 'X' then
                begin
                  if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_3RD_PARTY]) and (ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_FED_TAX_VALUE'), 0) <> 0) then
                  begin
                    TaxORType := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_FED_TAX_TYPE');
                    case TaxORType[1] of
                      OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT:
                        FedTax := FedTax + ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value,
                          'OVERRIDE_FED_TAX_VALUE'), 0);
                      OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
                        FedTax := FedTax + ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value,
                          'OVERRIDE_FED_TAX_VALUE'), 0) * 0.01 * ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
                    end;
                  end
                  else
                    AdjFedTaxWages := AdjFedTaxWages + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
                end;
            end;
        Next;
      end;
    CalcFedTaxesCont;
    if PR_CHECK.FieldByName('EXCLUDE_FEDERAL').Value = 'Y' then
    begin
      FedTax := 0;
      Exit;
    end;

    if not ShortfallsDisabled and (DM_COMPANY.CO.FieldByName('MAKE_UP_TAX_DEDUCT_SHORTFALLS').Value = 'Y') then
    begin
      Q := TevQuery.Create(FederalShortFall);
      Q.Param['EENbr'] := EENumber;
      Q.Param['CheckNbr'] := CheckNumber;
      Q.Macro['CheckType'] := '''' + CHECK_TYPE2_REGULAR + ''', ''' + CHECK_TYPE2_VOID + '''';
      FedTax := FedTax + ConvertNull(Q.Result['FEDERAL_SHORTFALL'], 0);
    end;
  end;
end;

procedure TevGenericGrossToNet.CalcStateTaxes(var Warnings: String; CheckNumber, COStateNumber: Integer; var StateTax, StateTaxWages, RecipStateTax: Real; var RecipEEStateNumber: Integer; PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES:
  TevClientDataSet);
var
  AdjStateTaxWages, OverrideValue, TaxCredit, TaxMultiplier, TaxWagesMultiplier: Real;
  EDCodeType, State, TaxORType, RecipMethod, ExemptStateCode, ExemptClStateCode: string;
  EEStateNumber, TempCOStateNbr, SYStateNumber, RecipCOStateNumber: Integer;
  HasTaxOverride: Boolean;

  function NbrOfAllowances: Integer;
  var
    TmpNbr: Integer;
  begin
    if RecipEEStateNumber <> 0 then
      TmpNbr := RecipEEStateNumber
    else
      TmpNbr := EEStateNumber;
    if TaxCalculator then
      Result := ConvertNull(PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, TmpNbr]), 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0)
    else
      Result := ConvertNull(DM_HISTORICAL_TAXES.EE_STATES.NewLookup('EE_STATES_NBR', TmpNbr, 'STATE_NUMBER_WITHHOLDING_ALLOW'), 0);
  end;

  function CalcStateTaxAmount(AnnualStateTaxWages: Real; MaritalStatusNbr: Integer): Real;
  var
    TaxableAmount, TempTax, Minimum, Maximum, Percentage: Real;
  begin
    with DM_HISTORICAL_TAXES do
    begin
      TaxableAmount := AnnualStateTaxWages;
      TempTax := 0;

      SY_STATE_TAX_CHART.IndexFieldNames := 'ENTRY_TYPE;SY_STATE_MARITAL_STATUS_NBR;MAXIMUM';
      SY_STATE_TAX_CHART.SetRange(['S', MaritalStatusNbr], ['S', MaritalStatusNbr]);
      try
        SY_STATE_TAX_CHART.First;
        while not SY_STATE_TAX_CHART.EOF do
        begin
          Maximum := SY_STATE_TAX_CHART.FieldByName('MAXIMUM').Value;
          Minimum := SY_STATE_TAX_CHART.FieldByName('MINIMUM').Value;
          Percentage := SY_STATE_TAX_CHART.FieldByName('PERCENTAGE').Value;
          if TaxableAmount <= SY_STATE_TAX_CHART.FieldByName('MAXIMUM').Value then
          begin
            if State = 'UT' then
              TempTax := TaxableAmount * Percentage
            else
              TempTax := TempTax + (TaxableAmount - Minimum) * Percentage;
            Break;
          end;
          TempTax := TempTax + (Maximum - Minimum) * Percentage;
          SY_STATE_TAX_CHART.Next;
        end;
      finally
        SY_STATE_TAX_CHART.CancelRange;
        SY_STATE_TAX_CHART.IndexFieldNames := '';
      end;
      Result := TempTax;
    end;
  end;

  procedure CalcStateTaxesPartII(CheckNumber, COStateNumber: Integer; AdjStateTaxWages: Real; var StateTax: Real; Reciprocation: Boolean);
  var
    TaxFreq, SYStateNumber, EEStateNumber: Integer;
    EEStateMaritalStatus, State: string;


    function GetYTDFICA: Real;
    var
      TempWages, TempTax: Real;
    begin
      Result := 0;
      if not YTDsDisabled then
      begin
        ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(CheckNumber, 0, ltEEOASDI, PR, PR_CHECK, nil, nil, nil, TempWages, TempTax, 0, GetEndYear(PR.FieldByName('CHECK_DATE').Value));
        Result := Result + TempTax;
        ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(CheckNumber, 0, ltEEMedicare, PR, PR_CHECK, nil, nil, nil, TempWages, TempTax, 0, GetEndYear(PR.FieldByName('CHECK_DATE').Value));
        Result := Result + TempTax;
      end;
      Result := Result + ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0) + ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);
    end;

    procedure CalcStateCT;
    var
      TaxableAmount, PersonalTaxCredit, FlatAmount: Real;
      MaritalStatusNbr: Integer;
    begin
      with DM_HISTORICAL_TAXES do
      begin
        FlatAmount := 0;

        TaxableAmount := AdjStateTaxWages * TaxFreq;
        MaritalStatusNbr := SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]),
          'SY_STATE_MARITAL_STATUS_NBR');
        SY_STATE_TAX_CHART.IndexFieldNames := 'ENTRY_TYPE;SY_STATE_MARITAL_STATUS_NBR;MAXIMUM';
        SY_STATE_TAX_CHART.SetRange([STATE_TAX_CHART_TYPE_CT_EXEMPTION, MaritalStatusNbr],
                                    [STATE_TAX_CHART_TYPE_CT_EXEMPTION, MaritalStatusNbr]);
        try
          SY_STATE_TAX_CHART.First;
          while not SY_STATE_TAX_CHART.EOF do
          begin
            if AdjStateTaxWages * TaxFreq <= SY_STATE_TAX_CHART.FieldByName('MAXIMUM').Value then
            begin
              TaxableAmount := AdjStateTaxWages * TaxFreq - SY_STATE_TAX_CHART.FieldByName('PERCENTAGE').Value;
              break;
            end;
            SY_STATE_TAX_CHART.Next;
          end;

          // new CT

          SY_STATE_TAX_CHART.SetRange([STATE_TAX_CHART_TYPE_CT_FLAT_AMOUNT, MaritalStatusNbr],
                                      [STATE_TAX_CHART_TYPE_CT_FLAT_AMOUNT, MaritalStatusNbr]);
          SY_STATE_TAX_CHART.First;
          while not SY_STATE_TAX_CHART.EOF do
          begin
            if AdjStateTaxWages * TaxFreq <= SY_STATE_TAX_CHART.FieldByName('MAXIMUM').Value then
            begin
              FlatAmount := SY_STATE_TAX_CHART.FieldByName('PERCENTAGE').Value;
              break;
            end;
            SY_STATE_TAX_CHART.Next;
          end;

          // end new CT

          SY_STATE_TAX_CHART.SetRange([STATE_TAX_CHART_TYPE_CT_PERS_TAX_CR, MaritalStatusNbr],
                                      [STATE_TAX_CHART_TYPE_CT_PERS_TAX_CR, MaritalStatusNbr]);
          SY_STATE_TAX_CHART.First;
          while not SY_STATE_TAX_CHART.EOF do
          begin
            if AdjStateTaxWages * TaxFreq <= SY_STATE_TAX_CHART.FieldByName('MAXIMUM').Value then
            begin
              PersonalTaxCredit := SY_STATE_TAX_CHART.FieldByName('PERCENTAGE').Value;
              StateTax := StateTax + (CalcStateTaxAmount(TaxableAmount, MaritalStatusNbr) * PersonalTaxCredit + FlatAmount) / TaxFreq;
              break;
            end;
            SY_STATE_TAX_CHART.Next;
          end;

        finally
          SY_STATE_TAX_CHART.CancelRange;
          SY_STATE_TAX_CHART.IndexFieldNames := '';
        end;
      end;
    end;

    procedure CalcStateOR_2010_2013;
    var
      StdDeduction, TaxableAmount, BasicTax,
      StandardPerExemptionAllow, DefinedHighIncome,
      HighIncomePerExemptAllow, DeductFICAMaximum, PerDependentAllow,
      StdDeductionPctnGross, StdDeductionFlatAmount, PersonalTaxCreditAmount,
      StdDeductionMinAmount, StdDeductionMaxAmount, TaxCreditPerDependent,
      DeductFederalMaximumAmount, FederalTax, YWage,
      TaxCreditPerAllowance, EeOasdiTax, EeMedicareTax: Real;
      PersonalExemptions: Integer;
      DeductFederal, DeductFICA: Boolean;
      SavePlace: TBookmark;
    begin

      SavePlace := nil;
      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]), 'EE_STATES_NBR');

        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');

        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;

        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        StandardPerExemptionAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0);
        PersonalExemptions := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);
        DefinedHighIncome := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0);
        HighIncomePerExemptAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0);
        PerDependentAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);
        StdDeductionPctnGross := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0);
        StdDeductionFlatAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
        StdDeductionMinAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);
        StdDeductionMaxAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
        DeductFederal := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y';
        DeductFICA := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y';
        DeductFICAMaximum := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0);
        PersonalTaxCreditAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        TaxCreditPerDependent := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        TaxCreditPerAllowance := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);
        DeductFederalMaximumAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0);

        StdDeduction := StdDeductionMinAmount;

        if StdDeductionMinAmount <= AdjStateTaxWages * TaxFreq * StdDeductionPctnGross + StdDeductionFlatAmount then
        begin
          StdDeduction := AdjStateTaxWages * TaxFreq * StdDeductionPctnGross + StdDeductionFlatAmount;
          if (StdDeductionMaxAmount <> 0) and (StdDeduction > StdDeductionMaxAmount) then
            StdDeduction := StdDeductionMaxAmount;
        end;

        EeOasdiTax := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0);
        EeMedicareTax := ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);
        FederalTax := ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0);

        YWage := AdjStateTaxWages * TaxFreq;

        if Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S' then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 125000 then
          begin
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue) then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - (Floor((YWage-125000)/5000)+1)*1250
            else
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - (Floor((YWage-125000)/5000)+1)*1200;
            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        if Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M' then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 250000 then
          begin
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue) then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - (Floor((YWage-250000)/10000)+1)*1250
            else
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - (Floor((YWage-250000)/10000)+1)*1200;
            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        if Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S3' then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 125000 then
          begin
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue) then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - (Floor((YWage-125000)/5000)+1)*1250
            else
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - (Floor((YWage-125000)/5000)+1)*1200;
            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        TaxableAmount := StandardPerExemptionAllow * PersonalExemptions;

        if (AdjStateTaxWages * TaxFreq > DefinedHighIncome) and (DefinedHighIncome <> 0) then
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances - PersonalExemptions) * HighIncomePerExemptAllow
        else
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances - PersonalExemptions) * PerDependentAllow;

        if DeductFederal then
        begin
          if (FederalTax * TaxFreq > DeductFederalMaximumAmount) and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - DeductFederalMaximumAmount
          else
            TaxableAmount := TaxableAmount - FederalTax * TaxFreq;
        end;

        if DeductFICA and ((DeductFICAMaximum = 0) or (GetYTDFICA < DeductFICAMaximum)) then
          TaxableAmount := TaxableAmount - EeOasdiTax * TaxFreq - EeMedicareTax * TaxFreq;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);

        BasicTax := BasicTax - PersonalTaxCreditAmount;

        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;

        BasicTax := BasicTax - (NbrOfAllowances - PersonalExemptions) * TaxCreditPerDependent;
        BasicTax := BasicTax - NbrOfAllowances * TaxCreditPerAllowance;

        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / TaxFreq;
        if StateTax < 0 then
          StateTax := 0;


      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

    procedure CalcStateOR;
    var
      StdDeduction, TaxableAmount, BasicTax,
      StandardPerExemptionAllow, DefinedHighIncome,
      HighIncomePerExemptAllow, DeductFICAMaximum, PerDependentAllow,
      StdDeductionPctnGross, StdDeductionFlatAmount, PersonalTaxCreditAmount,
      StdDeductionMinAmount, StdDeductionMaxAmount, TaxCreditPerDependent,
      DeductFederalMaximumAmount, FederalTax, YWage,
      TaxCreditPerAllowance, EeOasdiTax, EeMedicareTax: Real;
      PersonalExemptions: Integer;
      DeductFederal, DeductFICA: Boolean;
      SavePlace: TBookmark;
    begin

      SavePlace := nil;
      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]), 'EE_STATES_NBR');

        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');

        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;

        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        StandardPerExemptionAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0);
        PersonalExemptions := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);
        DefinedHighIncome := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0);
        HighIncomePerExemptAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0);
        PerDependentAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);
        StdDeductionPctnGross := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0);
        StdDeductionFlatAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
        StdDeductionMinAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);
        StdDeductionMaxAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
        DeductFederal := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y';
        DeductFICA := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y';
        DeductFICAMaximum := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0);
        PersonalTaxCreditAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        TaxCreditPerDependent := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        TaxCreditPerAllowance := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);
        DeductFederalMaximumAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0);

        StdDeduction := StdDeductionMinAmount;

        if StdDeductionMinAmount <= AdjStateTaxWages * TaxFreq * StdDeductionPctnGross + StdDeductionFlatAmount then
        begin
          StdDeduction := AdjStateTaxWages * TaxFreq * StdDeductionPctnGross + StdDeductionFlatAmount;
          if (StdDeductionMaxAmount <> 0) and (StdDeduction > StdDeductionMaxAmount) then
            StdDeduction := StdDeductionMaxAmount;
        end;

        EeOasdiTax := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0);
        EeMedicareTax := ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);
        FederalTax := ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0);

        YWage := AdjStateTaxWages * TaxFreq;

        if (Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S')
        or (Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S3') then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 50000 then
          begin
            if YWage < 125000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount
            else  
            if YWage < 130000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300
            else if YWage < 135000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1300
            else if YWage < 140000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1300 - 1300
            else if YWage < 145000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1300 - 1300 - 1300
            else
              DeductFederalMaximumAmount := 0;

            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        if Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M' then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 50000 then
          begin
            if YWage < 250000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount
            else  
            if YWage < 260000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300
            else if YWage < 270000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1300
            else if YWage < 280000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1300 - 1300
            else if YWage < 290000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1300 - 1300 - 1300
            else
              DeductFederalMaximumAmount := 0;

            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        if ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M') and (AdjStateTaxWages * TaxFreq < 200000))
        or ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) <> 'M') and (AdjStateTaxWages * TaxFreq < 100000)) then
        begin
          if (AdjStateTaxWages * TaxFreq > DefinedHighIncome) and (DefinedHighIncome <> 0) then
            TaxableAmount := AdjStateTaxWages * TaxFreq
                             - StdDeduction
                             - StandardPerExemptionAllow * PersonalExemptions
                             - (NbrOfAllowances - PersonalExemptions) * HighIncomePerExemptAllow
          else
            TaxableAmount := AdjStateTaxWages * TaxFreq
                             - StdDeduction
                             - StandardPerExemptionAllow * PersonalExemptions
                             - (NbrOfAllowances - PersonalExemptions) * PerDependentAllow;
        end
        else
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - StandardPerExemptionAllow * PersonalExemptions;

        if DeductFederal then
        begin
          if (FederalTax * TaxFreq > DeductFederalMaximumAmount) and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - DeductFederalMaximumAmount
          else
            TaxableAmount := TaxableAmount - FederalTax * TaxFreq;
        end;

        if DeductFICA and ((DeductFICAMaximum = 0) or (GetYTDFICA < DeductFICAMaximum)) then
          TaxableAmount := TaxableAmount - EeOasdiTax * TaxFreq - EeMedicareTax * TaxFreq;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);

        BasicTax := BasicTax - PersonalTaxCreditAmount;

        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;

        if ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M') and (AdjStateTaxWages * TaxFreq < 200000))
        or ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) <> 'M') and (AdjStateTaxWages * TaxFreq < 100000)) then
          BasicTax := BasicTax - (NbrOfAllowances - PersonalExemptions) * TaxCreditPerDependent - NbrOfAllowances * TaxCreditPerAllowance;

        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / TaxFreq;
        if StateTax < 0 then
          StateTax := 0;


      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

    procedure CalcStateOR_2014;
    var
      StdDeduction, TaxableAmount, BasicTax,
      StandardPerExemptionAllow, DefinedHighIncome,
      HighIncomePerExemptAllow, DeductFICAMaximum, PerDependentAllow,
      StdDeductionPctnGross, StdDeductionFlatAmount, PersonalTaxCreditAmount,
      StdDeductionMinAmount, StdDeductionMaxAmount, TaxCreditPerDependent,
      DeductFederalMaximumAmount, FederalTax, YWage,
      TaxCreditPerAllowance, EeOasdiTax, EeMedicareTax: Real;
      PersonalExemptions: Integer;
      DeductFederal, DeductFICA: Boolean;
      SavePlace: TBookmark;
    begin

      SavePlace := nil;
      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]), 'EE_STATES_NBR');

        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');

        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;

        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        StandardPerExemptionAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0);
        PersonalExemptions := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);
        DefinedHighIncome := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0);
        HighIncomePerExemptAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0);
        PerDependentAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);
        StdDeductionPctnGross := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0);
        StdDeductionFlatAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
        StdDeductionMinAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);
        StdDeductionMaxAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
        DeductFederal := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y';
        DeductFICA := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y';
        DeductFICAMaximum := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0);
        PersonalTaxCreditAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        TaxCreditPerDependent := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        TaxCreditPerAllowance := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);
        DeductFederalMaximumAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0);

        StdDeduction := StdDeductionMinAmount;

        if StdDeductionMinAmount <= AdjStateTaxWages * TaxFreq * StdDeductionPctnGross + StdDeductionFlatAmount then
        begin
          StdDeduction := AdjStateTaxWages * TaxFreq * StdDeductionPctnGross + StdDeductionFlatAmount;
          if (StdDeductionMaxAmount <> 0) and (StdDeduction > StdDeductionMaxAmount) then
            StdDeduction := StdDeductionMaxAmount;
        end;

        EeOasdiTax := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0);
        EeMedicareTax := ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);
        FederalTax := ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0);

        YWage := AdjStateTaxWages * TaxFreq;

        if (Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S')
        or (Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S3') then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 125000 then
          begin
            if YWage < 130000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300
            else if YWage < 135000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1250
            else if YWage < 140000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1250 - 1300
            else if YWage < 145000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1250 - 1300 - 1250
            else
              DeductFederalMaximumAmount := 0;

            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        if Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M' then
        begin
          if YWage > 50000 then
            PersonalTaxCreditAmount := 0;
          if YWage > 250000 then
          begin
            if YWage < 260000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300
            else if YWage < 270000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1250
            else if YWage < 280000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1250 - 1300
            else if YWage < 290000 then
              DeductFederalMaximumAmount := DeductFederalMaximumAmount - 1300 - 1250 - 1300 - 1250
            else
              DeductFederalMaximumAmount := 0;

            if DeductFederalMaximumAmount < 0 then
              DeductFederalMaximumAmount := 0;
          end;
        end;

        if ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M') and (AdjStateTaxWages * TaxFreq < 200000))
        or ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) <> 'M') and (AdjStateTaxWages * TaxFreq < 100000)) then
        begin
          if (AdjStateTaxWages * TaxFreq > DefinedHighIncome) and (DefinedHighIncome <> 0) then
            TaxableAmount := AdjStateTaxWages * TaxFreq
                             - StdDeduction
                             - StandardPerExemptionAllow * PersonalExemptions
                             - (NbrOfAllowances - PersonalExemptions) * HighIncomePerExemptAllow
          else
            TaxableAmount := AdjStateTaxWages * TaxFreq
                             - StdDeduction
                             - StandardPerExemptionAllow * PersonalExemptions
                             - (NbrOfAllowances - PersonalExemptions) * PerDependentAllow;
        end
        else
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - StandardPerExemptionAllow * PersonalExemptions;

        if DeductFederal then
        begin
          if (FederalTax * TaxFreq > DeductFederalMaximumAmount) and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - DeductFederalMaximumAmount
          else
            TaxableAmount := TaxableAmount - FederalTax * TaxFreq;
        end;

        if DeductFICA and ((DeductFICAMaximum = 0) or (GetYTDFICA < DeductFICAMaximum)) then
          TaxableAmount := TaxableAmount - EeOasdiTax * TaxFreq - EeMedicareTax * TaxFreq;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);

        BasicTax := BasicTax - PersonalTaxCreditAmount;

        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;

        if ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'M') and (AdjStateTaxWages * TaxFreq < 200000))
        or ((Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) <> 'M') and (AdjStateTaxWages * TaxFreq < 100000)) then
          BasicTax := BasicTax - (NbrOfAllowances - PersonalExemptions) * TaxCreditPerDependent - NbrOfAllowances * TaxCreditPerAllowance;

        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / TaxFreq;
        if StateTax < 0 then
          StateTax := 0;


      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

    procedure CalcStateME;
    var
      StdDeduction, TaxableAmount, BasicTax, PersonalExemptions,
      PersonalCredit, CreditPerDependent, TaxCreditPerAllowance: Real;
      SavePlace: TBookmark;
      Freq: Integer;
      VisaNumber, VisaType: String;
      ExpDate: Variant;
      Fraction: Real;
    const
      ME_SINGLE_MARITAL_STATUS_NBR = 32;
      SINGLE_NR_LIMIT_2016 = 67150;
      SINGLE_NR_MULTPLIER_2016 = 11600;
      MARRIED_NR_LIMIT_2016 = 137150;
      MARRIED_NR_MULTPLIER_2016 = 23200;
    begin

      if TaxFreq = 365 then
        Freq := 260
      else
        Freq := TaxFreq;

      //New(SavePlace);
      SavePlace := nil;
      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]),
          'EE_STATES_NBR');
// Get SY_STATE_NBR based on CO_STATE_NBR
        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');
// --------------------------------------
        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;
        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);
        if StdDeduction <= AdjStateTaxWages * Freq *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) +
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0) then
        begin
          StdDeduction := AdjStateTaxWages * Freq *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) +
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
          if (StdDeduction > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0)) and
            (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0) <> 0) then
            StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
        end;

        TaxableAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0) *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);

        if (AdjStateTaxWages * Freq > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0))
        and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0) <> 0) then
          TaxableAmount := AdjStateTaxWages * Freq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0)
        else
          TaxableAmount := AdjStateTaxWages * Freq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);
        if SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y' then
        begin
          if (ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * Freq >
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0))
            and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0)
          else
            TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * Freq;
        end;

        if (SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y') and
          (((ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0) = 0)) or (GetYTDFICA < ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0))) then
          TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0) * Freq -
            ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0) * Freq;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);

        // Phase-out of the standard deduction 2016
        if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2015, 12, 31)) = GreaterThanValue) then
        begin
          if (TaxableAmount > SINGLE_NR_LIMIT_2016) and (SY_STATE_MARITAL_STATUS['SY_STATE_MARITAL_STATUS_NBR'] = ME_SINGLE_MARITAL_STATUS_NBR) then
          begin
            Fraction := (AdjStateTaxWages * Freq - SINGLE_NR_LIMIT_2016) / 75000;
            if Fraction > 1 then
              Fraction := 1;
            TaxableAmount := TaxableAmount + Fraction * SINGLE_NR_MULTPLIER_2016;
          end
          else if (TaxableAmount > MARRIED_NR_LIMIT_2016) and (SY_STATE_MARITAL_STATUS['SY_STATE_MARITAL_STATUS_NBR'] <> ME_SINGLE_MARITAL_STATUS_NBR) then
          begin
            Fraction := (AdjStateTaxWages * Freq - MARRIED_NR_LIMIT_2016) / 150000;
            if Fraction > 1 then
              Fraction := 1;
            TaxableAmount := TaxableAmount + Fraction * MARRIED_NR_MULTPLIER_2016;
          end;
          BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS['SY_STATE_MARITAL_STATUS_NBR']);
        end;

        // The logic for Non Resident Aliens 2011-2015
        if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2010, 12, 31)) = GreaterThanValue)
        and (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2016, 1, 1)) = LessThanValue) then
        begin
          VisaNumber := ConvertNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK.FieldByName('EE_NBR').Value, 'CL_PERSON_NBR'), 'VISA_NUMBER'), '');
          VisaType := DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK.FieldByName('EE_NBR').Value, 'CL_PERSON_NBR'), 'VISA_TYPE');
          ExpDate := DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK.FieldByName('EE_NBR').Value, 'CL_PERSON_NBR'), 'VISA_EXPIRATION_DATE');
          if (VisaNumber <> '') and (VisaType <> VISA_TYPE_NONE) and (VisaType <> VISA_TYPE_H1B_RES) and not VarIsNull(ExpDate) and (ExpDate >= DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) then
          begin
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2014, 12, 31)) = GreaterThanValue) then
            begin
              BasicTax := CalcStateTaxAmount(TaxableAmount+6300.00, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);
            end
            else
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2013, 12, 31)) = GreaterThanValue) then
            begin
              BasicTax := CalcStateTaxAmount(TaxableAmount+6200.00, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);
            end
            else
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue) then
            begin
              BasicTax := CalcStateTaxAmount(TaxableAmount+6100.00, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);
            end
            else
            if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2011, 12, 31)) = GreaterThanValue) then
            begin
              BasicTax := CalcStateTaxAmount(TaxableAmount+5950.00, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);
            end
            else
            begin
              if Trim(SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').AsString) = 'S' then
                BasicTax := CalcStateTaxAmount(TaxableAmount+5800.00, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value)
              else
                BasicTax := CalcStateTaxAmount(TaxableAmount+9650.00, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);
            end;
          end;
        end;

        PersonalCredit        := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        PersonalExemptions    := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);
        CreditPerDependent    := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        TaxCreditPerAllowance := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);

        BasicTax := BasicTax - PersonalCredit;

        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;
        BasicTax := BasicTax - (NbrOfAllowances - PersonalExemptions) * CreditPerDependent;

        BasicTax := BasicTax - NbrOfAllowances * TaxCreditPerAllowance;

        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / Freq;
        if StateTax < 0 then
          StateTax := 0;

      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

    procedure CalcStateMA;
    var
      StdDeduction, TaxableAmount, BasicTax,
      StandardPerExemptionAllow, PersonalExemptions, DefinedHighIncome,
      HighIncomePerExemptAllow, DeductFICAMaximum, PerDependentAllow,
      EeOasdiTax, EeMedicareTax: Real;
      DeductFederal, DeductFICA: Boolean;
      SavePlace: TBookmark;
    begin

      SavePlace := nil;
      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]),
          'EE_STATES_NBR');

        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');

        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;

        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);

        if StdDeduction <= AdjStateTaxWages * TaxFreq *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) +
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0) then
        begin
          StdDeduction := AdjStateTaxWages * TaxFreq *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) +
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
          if (StdDeduction > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0)) and
            (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0) <> 0) then
            StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
        end;

        StandardPerExemptionAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0);
        PersonalExemptions := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);
        DefinedHighIncome := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0);
        HighIncomePerExemptAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0);
        PerDependentAllow := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);

        DeductFederal := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y';
        DeductFICA := SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y';
        DeductFICAMaximum := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0);

        EeOasdiTax := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0);
        EeMedicareTax := ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);

        TaxableAmount := StandardPerExemptionAllow * PersonalExemptions;

        if (AdjStateTaxWages * TaxFreq > DefinedHighIncome) and (DefinedHighIncome <> 0) then
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances - PersonalExemptions) * HighIncomePerExemptAllow
        else
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances - PersonalExemptions) * PerDependentAllow;

        if DeductFederal then
        begin
          if (ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * TaxFreq >
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0))
            and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0)
          else
            TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * TaxFreq;
        end;
        
        if DeductFICA and ((DeductFICAMaximum = 0) or (GetYTDFICA < DeductFICAMaximum)) then
          TaxableAmount := TaxableAmount - EeOasdiTax * TaxFreq - EeMedicareTax * TaxFreq;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);
        
        BasicTax := BasicTax - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;
        BasicTax := BasicTax - (NbrOfAllowances - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        BasicTax := BasicTax - NbrOfAllowances * ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);
        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / TaxFreq;
        if StateTax < 0 then
          StateTax := 0;

      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

// SR-120

    procedure CalcStateNC;
    var
      StdDeduction, TaxableAmount, BasicTax, StandardDeductionFlatAmount: Real;
      SavePlace: TBookmark;
      IsNonResidentAlien: Boolean;
      VisaType, VisaNumber: String;
      VisaExpirationDate: Variant;
    begin
      //New(SavePlace);
      SavePlace := nil;

      VisaType := DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK['EE_NBR'], 'CL_PERSON_NBR'), 'VISA_TYPE');
      VisaNumber := ConvertNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK['EE_NBR'], 'CL_PERSON_NBR'), 'VISA_NUMBER'), '');
      VisaExpirationDate := DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK['EE_NBR'], 'CL_PERSON_NBR'), 'VISA_EXPIRATION_DATE');

      IsNonResidentAlien := (VisaNumber <> '') and (VisaType <> VISA_TYPE_NONE) and (VisaType <> VISA_TYPE_H1B_RES)
        and not VarIsNull(VisaExpirationDate) and (VisaExpirationDate >= DM_PAYROLL.PR['CHECK_DATE']);

      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]),
          'EE_STATES_NBR');
// Get SY_STATE_NBR based on CO_STATE_NBR
        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');
// --------------------------------------
        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;
        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);

        StandardDeductionFlatAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
        if IsNonResidentAlien then
          StandardDeductionFlatAmount := 0;

        if StdDeduction <= AdjStateTaxWages * TaxFreq *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) + StandardDeductionFlatAmount then
        begin
          StdDeduction := AdjStateTaxWages * TaxFreq *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) + StandardDeductionFlatAmount;
          if (StdDeduction > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0)) and
            (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0) <> 0) then
            StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
        end;

        TaxableAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0) *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);

        if (AdjStateTaxWages * TaxFreq > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0))
        and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0) <> 0) then
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0)
        else
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);
        if SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y' then
        begin
          if (ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * TaxFreq >
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0))
            and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0)
          else
            TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * TaxFreq;
        end;

        begin
          if (SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y') and
          (((ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0) = 0)) or (GetYTDFICA < ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0))) then
            TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0) * TaxFreq -
              ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0) * TaxFreq;
        end;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);

        BasicTax := BasicTax - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;
        BasicTax := BasicTax - (NbrOfAllowances - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        BasicTax := BasicTax - NbrOfAllowances * ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);
        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / TaxFreq;
        if StateTax < 0 then
          StateTax := 0;


      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

    procedure CalcStateOther;
    var
      StdDeduction, TaxableAmount, BasicTax, TempWages: Real;
      SavePlace: TBookmark;
    begin
      //New(SavePlace);
      SavePlace := nil;
      with DM_HISTORICAL_TAXES do
      try
        EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]),
          'EE_STATES_NBR');
// Get SY_STATE_NBR based on CO_STATE_NBR
        State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
        SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');
// --------------------------------------
        SavePlace := SY_STATE_MARITAL_STATUS.GetBookmark;
        if not SY_STATE_MARITAL_STATUS.Locate('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]), []) then
          raise EMaritalStatusNotSetup.CreateHelp('Marital status ' + EEStateMaritalStatus + ' is not setup for ' + State + '.', IDH_InconsistentData);

        if (State = 'AL') and (PR.FieldByName('CHECK_DATE').Value >= StrToDate('01/01/2007')) then
        begin
          StdDeduction := SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value;
          if AdjStateTaxWages * TaxFreq > SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value then
          begin
            TempWages := AdjStateTaxWages * TaxFreq;
            if TempWages > SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value then
              TempWages := SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value;
            StdDeduction := StdDeduction - SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value
              * Int((TempWages - SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value) / 500);
          end;
        end
        else
        begin
          StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0);
          if StdDeduction <= AdjStateTaxWages * TaxFreq *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) +
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0) then
          begin
            StdDeduction := AdjStateTaxWages * TaxFreq *
              ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_PCNT_GROSS').Value, 0) +
              ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_FLAT_AMOUNT').Value, 0);
            if (StdDeduction > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0)) and
              (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0) <> 0) then
              StdDeduction := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MAX_AMOUNT').Value, 0);
          end;
        end;

        TaxableAmount := ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_PER_EXEMPTION_ALLOW').Value, 0) *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0);
        if (State = 'AL') and (PR.FieldByName('CHECK_DATE').Value >= StrToDate('01/01/2007'))
        and (AdjStateTaxWages * TaxFreq <= ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('STANDARD_DEDUCTION_MIN_AMOUNT').Value, 0)) then
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0) * 2
        else
        if (AdjStateTaxWages * TaxFreq > ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0))
        and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEFINED_HIGH_INCOME_AMOUNT').Value, 0) <> 0) then
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('HIGH_INCOME_PER_EXEMPT_ALLOW').Value, 0)
        else
          TaxableAmount := AdjStateTaxWages * TaxFreq - StdDeduction - TaxableAmount - (NbrOfAllowances
            - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PER_DEPENDENT_ALLOWANCE').Value, 0);
        if SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL').Value = 'Y' then
        begin
          if (ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * TaxFreq >
            ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0))
            and (ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0) <> 0) then
            TaxableAmount := TaxableAmount - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FEDERAL_MAXIMUM_AMOUNT').Value, 0)
          else
            TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0) * TaxFreq;
        end;

        begin
          if (SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA').Value = 'Y') and
          (((ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0) = 0)) or (GetYTDFICA < ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('DEDUCT_FICA__MAXIMUM_AMOUNT').Value, 0))) then
            TaxableAmount := TaxableAmount - ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0) * TaxFreq -
              ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0) * TaxFreq;
        end;

        BasicTax := CalcStateTaxAmount(TaxableAmount, SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Value);

        BasicTax := BasicTax - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_TAX_CREDIT_AMOUNT').Value, 0);
        if (SY_STATES.FieldByName('CAP_STATE_TAX_CREDIT').Value = 'Y') and (BasicTax < 0) then
          BasicTax := 0;
        BasicTax := BasicTax - (NbrOfAllowances - ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('PERSONAL_EXEMPTIONS').Value, 0)) *
          ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_DEPENDENT').Value, 0);
        BasicTax := BasicTax - NbrOfAllowances * ConvertNull(SY_STATE_MARITAL_STATUS.FieldByName('TAX_CREDIT_PER_ALLOWANCE').Value, 0);
        if BasicTax > 0 then
          StateTax := StateTax + (BasicTax) / TaxFreq;
        if StateTax < 0 then
          StateTax := 0;


      finally
        SY_STATE_MARITAL_STATUS.GotoBookmark(SavePlace);
        SY_STATE_MARITAL_STATUS.FreeBookmark(SavePlace);
      end;
    end;

  begin
  // SR-110.10
    with DM_HISTORICAL_TAXES do
    begin

      if CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE_EXEMPT') = 'Y' then
      begin
        StateTax := 0;
        Exit;
        // Will resume at SR-110.20
      end;
      EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]),
        'EE_STATES_NBR');
      if EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'STATE_EXEMPT_EXCLUDE') = 'E' then
      begin
        StateTax := 0;
        Exit;
        // Will resume at SR-110.20
      end;
      State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
      SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');
      if (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_STATE') = 'Y')
        or (EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'STATE_EXEMPT_EXCLUDE') = 'X') then
      begin
        StateTax := 0;
        Exit;
        // Will resume at SR-110.20
      end;

      if not Reciprocation then
      begin
        if not VarIsNull(PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_OVERRIDE_VALUE')) then
        begin
          TaxORType := PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_OVERRIDE_TYPE');
          case TaxORType[1] of
            OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT:
              begin
                StateTax := PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_OVERRIDE_VALUE');
                HasTaxOverride := True;
                Exit;
              // Will resume at SR-110.20
              end;
            OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
              begin
                StateTax := StateTax + PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]),
                  'STATE_OVERRIDE_VALUE') * AdjStateTaxWages * 0.01;
                if (DM_SYSTEM_STATE.SY_STATES.NewLookup('STATE', State, 'ROUND_TO_NEAREST_DOLLAR') = 'Y') and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
                  StateTax := RoundAll(StateTax, 0);
                HasTaxOverride := True;
                Exit;
              // Will resume at SR-110.20
              end;
            OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT:
              StateTax := StateTax + PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]),
                'STATE_OVERRIDE_VALUE');
            OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT:
              StateTax := StateTax + PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]),
                'STATE_OVERRIDE_VALUE') * AdjStateTaxWages * 0.01;
          end;
        end
        else
        if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_3RD_PARTY]) and not VarIsNull(EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'OVERRIDE_STATE_TAX_VALUE')) then
        begin
          TaxORType := EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'OVERRIDE_STATE_TAX_TYPE');
          if (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_ADDITIONAL_STATE') = EXCLUDE_TAXES_NONE) or
          (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_ADDITIONAL_STATE') = EXCLUDE_TAXES_ADDITIONAL) then
            case TaxORType[1] of
              OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT:
                begin
                  StateTax := EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'OVERRIDE_STATE_TAX_VALUE');
                  HasTaxOverride := True;
                  Exit;
                // Will resume at SR-110.20
                end;
              OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
                begin
                  StateTax := StateTax + EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'OVERRIDE_STATE_TAX_VALUE') * AdjStateTaxWages * 0.01;
                  if (DM_SYSTEM_STATE.SY_STATES.NewLookup('STATE', State, 'ROUND_TO_NEAREST_DOLLAR') = 'Y') and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
                    StateTax := RoundAll(StateTax, 0);
                  HasTaxOverride := True;
                  Exit;
                // Will resume at SR-110.20
                end;
            end;
          if (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_ADDITIONAL_STATE') = EXCLUDE_TAXES_NONE) or
          (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_ADDITIONAL_STATE') = EXCLUDE_TAXES_OVERRIDE_REGULAR) then
            case TaxORType[1] of
              OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT:
                StateTax := StateTax + EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'OVERRIDE_STATE_TAX_VALUE');
              OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT:
                StateTax := StateTax + EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'OVERRIDE_STATE_TAX_VALUE') * AdjStateTaxWages * 0.01;
            end;
        end;
      end;

      TaxFreq := GetTaxFreq(PR_CHECK.FieldByName('TAX_FREQUENCY').Value);
      if TaxCalculator then
      begin
        EEStateMaritalStatus := PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'STATE_MARITAL_STATUS');
//        if Length(EEStateMaritalStatus) = 1 then
//          EEStateMaritalStatus := EEStateMaritalStatus + ' ';
      end
      else
        EEStateMaritalStatus := EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'STATE_MARITAL_STATUS');

      if (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'TAX_AT_SUPPLEMENTAL_RATE') = 'Y')
        and (ConvertNull(SY_STATES.NewLookup('SY_STATES_NBR', SYStateNumber, 'SUPPLEMENTAL_WAGES_PERCENTAGE'), 0) <> 0) then
      begin
        StateTax := StateTaxWages * SY_STATES.NewLookup('SY_STATES_NBR', SYStateNumber, 'SUPPLEMENTAL_WAGES_PERCENTAGE');
        Exit;
      end;

      if ConvertNull(SY_STATE_MARITAL_STATUS.NewLookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]),
      'MINIMUM_TAXABLE_INCOME'), 0) > 0 then
      begin
        if AdjStateTaxWages * TaxFreq < SY_STATE_MARITAL_STATUS.NewLookup('SY_STATES_NBR;STATUS_TYPE',
        VarArrayOf([SYStateNumber, EEStateMaritalStatus]), 'MINIMUM_TAXABLE_INCOME') then
          Exit;
      end;

      if ConvertNull(SY_STATE_MARITAL_STATUS.NewLookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber, EEStateMaritalStatus]),
        'STATE_PERCENT_OF_FEDERAL'), 0) > 0 then
      begin
        if ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value, 0) <> 0 then
        begin
          StateTax := StateTax + SY_STATE_MARITAL_STATUS.NewLookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([SYStateNumber,
            EEStateMaritalStatus]), 'STATE_PERCENT_OF_FEDERAL')
            * PR_CHECK.FieldByName('FEDERAL_TAX').Value * AdjStateTaxWages / PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value;
        end;
        Exit;
      end;

      if (StateTaxWages < 0) or (AdjStateTaxWages <= 0) then
        Exit;

      if State = 'CT' then
        CalcStateCT
      else
      if State = 'ME' then
        CalcStateME
      else
      if State = 'MA' then
        CalcStateMA
      else  
      if State = 'NC' then
        CalcStateNC
      else
      if (State = 'OR') and (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2010, 12, 31)) = GreaterThanValue) then
      begin
        if CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2014, 12, 31)) = GreaterThanValue then
          CalcStateOR
        else
        if CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2013, 12, 31)) = GreaterThanValue then
          CalcStateOR_2014
        else
          CalcStateOR_2010_2013;
      end
      else
        CalcStateOther;
    end;
  end;

var
  Q: IevQuery;  
begin

  with DM_HISTORICAL_TAXES do
  begin
    HasTaxOverride := False;
    AdjStateTaxWages := 0;
    StateTax := 0;
    StateTaxWages := 0;
    TaxCredit := 0;
    TaxMultiplier := 1;
    TaxWagesMultiplier := 0;
    RecipEEStateNumber := 0;
    RecipStateTax := 0;
    if CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE_EXEMPT') = 'Y' then
      Exit;
    State := DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNumber, 'STATE');
    try
      EEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COStateNumber]), 'EE_STATES_NBR');
    except
      raise EInconsistentData.CreateHelp('State ' + State + ' is not setup for the employee #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
    end;

    if EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'STATE_EXEMPT_EXCLUDE') = 'E' then Exit;

    PR_CHECK_LINES.First;
// Get SY_STATE_NBR based on CO_STATE_NBR
    SYStateNumber := SY_STATES.NewLookup('STATE', State, 'SY_STATES_NBR');
// --------------------------------------
    while not PR_CHECK_LINES.EOF do
    begin
      TempCOStateNbr := 0;
      if PR_CHECK_LINES.FieldByName('EE_STATES_NBR').IsNull then
      begin
        if not PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull then
          TempCOStateNbr := ConvertNull(DM_COMPANY.CO_JOBS.NewLookup('CO_JOBS_NBR', PR_CHECK_LINES['CO_JOBS_NBR'], 'HOME_CO_STATES_NBR'), 0);

        if TempCOStateNbr = 0 then
        begin
          if not PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').IsNull then
          begin
            if DM_COMPANY.CO_TEAM.NewLookup('CO_TEAM_NBR', PR_CHECK_LINES['CO_TEAM_NBR'], 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
              TempCOStateNbr := ConvertNull(DM_COMPANY.CO_TEAM.NewLookup('CO_TEAM_NBR', PR_CHECK_LINES['CO_TEAM_NBR'], 'HOME_CO_STATES_NBR'), 0);
          end
          else
          if not PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').IsNull then
          begin
            if DM_COMPANY.CO_DEPARTMENT.NewLookup('CO_DEPARTMENT_NBR', PR_CHECK_LINES['CO_DEPARTMENT_NBR'], 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
              TempCOStateNbr := ConvertNull(DM_COMPANY.CO_DEPARTMENT.NewLookup('CO_DEPARTMENT_NBR', PR_CHECK_LINES['CO_DEPARTMENT_NBR'], 'HOME_CO_STATES_NBR'), 0);
          end
          else
          if not PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').IsNull then
          begin
            if DM_COMPANY.CO_BRANCH.NewLookup('CO_BRANCH_NBR', PR_CHECK_LINES['CO_BRANCH_NBR'], 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
              TempCOStateNbr := ConvertNull(DM_COMPANY.CO_BRANCH.NewLookup('CO_BRANCH_NBR', PR_CHECK_LINES['CO_BRANCH_NBR'], 'HOME_CO_STATES_NBR'), 0);
          end
          else
          if not PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').IsNull then
          begin
            if DM_COMPANY.CO_DIVISION.NewLookup('CO_DIVISION_NBR', PR_CHECK_LINES['CO_DIVISION_NBR'], 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
              TempCOStateNbr := ConvertNull(DM_COMPANY.CO_DIVISION.NewLookup('CO_DIVISION_NBR', PR_CHECK_LINES['CO_DIVISION_NBR'], 'HOME_CO_STATES_NBR'), 0);
          end;
        end;  
      end;

      if (PR_CHECK_LINES['EE_STATES_NBR'] = EEStateNumber) or (COStateNumber = TempCOStateNbr) or (PR_CHECK_LINES.FieldByName('EE_STATES_NBR').IsNull
      and (EE['HOME_TAX_EE_STATES_NBR'] = EEStateNumber)
      and (TempCOStateNbr = 0)) then
      begin
        EDCodeType := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
        if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'SY_STATE_NBR') = SYStateNumber then
        begin
          if EDCodeType = ED_DED_PERCENT_OF_TAX then
            TaxMultiplier := TaxMultiplier - ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'PERCENTAGE'), 0) * 0.01
          else
          if EDCodeType = ED_DED_PERCENT_OF_TAX_WAGES then
            TaxWagesMultiplier := TaxWagesMultiplier + ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'PERCENTAGE'), 0) * 0.01
          else
            TaxCredit := TaxCredit + ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
        end;
        if TypeIsTaxable(EDCodeType) then
        begin

          ExemptStateCode := ConvertNull(SY_STATE_EXEMPTIONS.NewLookup('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([SYStateNumber, EDCodeType]), 'EXEMPT_STATE'), '');
          ExemptClStateCode := ConvertNull(CL_E_D_STATE_EXMPT_EXCLD.NewLookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber, PR_CHECK_LINES['CL_E_DS_NBR']]), 'EMPLOYEE_EXEMPT_EXCLUDE_STATE'), '');
          if ExemptClStateCode <> GROUP_BOX_EXE then
          begin
            if (ExemptClStateCode = '') and (ExemptStateCode <> 'E') then
            begin
              StateTaxWages := StateTaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
              if (ExemptStateCode <> GROUP_BOX_EXE) then
                AdjStateTaxWages := AdjStateTaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
            end
            else
            if (ExemptStateCode <> GROUP_BOX_EXC) or (ExemptClStateCode = 'I') then
            begin
              StateTaxWages := StateTaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
              if ExemptClStateCode <> '' then
              begin
                if (ExemptStateCode <> GROUP_BOX_EXC) or (ExemptClStateCode = 'I') then
                begin
                  if ExemptClStateCode <> GROUP_BOX_EXC then
                  begin
                    OverrideValue := ConvertNull(CL_E_D_STATE_EXMPT_EXCLD.NewLookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber,
                      PR_CHECK_LINES['CL_E_DS_NBR']]), 'EMPLOYEE_STATE_OR_VALUE'), 0);
                    if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD]) and (OverrideValue > 0)
                    and ( not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_3RD_PARTY]) or TypeIs3rdParty(EDCodeType)) then
                    begin
                      TaxORType := CL_E_D_STATE_EXMPT_EXCLD.NewLookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber,
                        PR_CHECK_LINES['CL_E_DS_NBR']]), 'EMPLOYEE_STATE_OR_TYPE');
                      case TaxORType[1] of
                        OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT:
                          StateTax := StateTax + OverrideValue;
                        OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
                          StateTax := StateTax + OverrideValue  * 0.01 * ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
                      else
                        AdjStateTaxWages := AdjStateTaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
                      end;
                    end
                    else
                      AdjStateTaxWages := AdjStateTaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
      PR_CHECK_LINES.Next;
    end;

    CalcStateTaxesPartII(CheckNumber, COStateNumber, AdjStateTaxWages, StateTax, False);

    if PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, EEStateNumber]), 'EXCLUDE_STATE') = 'Y' then
    begin
      StateTax := 0;
      Exit;
    end;
// SR-110.20
    RecipMethod := EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'RECIPROCAL_METHOD');
    if (RecipMethod <> STATE_RECIPROCAL_TYPE_NONE) and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_IMPORT) and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
    begin
      RecipCOStateNumber := ConvertNull(EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'RECIPROCAL_CO_STATES_NBR'), 0);
      try
        RecipEEStateNumber := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value,
          RecipCOStateNumber]), 'EE_STATES_NBR');
      except
        if RecipCOStateNumber = 0 then
          raise EInconsistentData.CreateHelp('Can not reciprocate because reciprocal state is not defined for employee #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData)
        else
          raise EInconsistentData.CreateHelp('Can not reciprocate because state ' + DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR',
            RecipCOStateNumber, 'STATE') + ' is not setup for the employee #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
      end;
      if not ((PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO) and
      (PR_CHECK_STATES.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, RecipEEStateNumber]), 'STATE_OVERRIDE_TYPE') = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT)) then
      begin
        case RecipMethod[1] of
          STATE_RECIPROCAL_TYPE_DIFFERENCE:
            begin
              RecipStateTax := 0;
              CalcStateTaxesPartII(CheckNumber, RecipCOStateNumber, AdjStateTaxWages, RecipStateTax, True);
              RecipStateTax := RecipStateTax - StateTax;
              if RecipStateTax < 0 then
                RecipStateTax := 0;
              if HasTaxOverride then
                RecipStateTax := 0;
            end;
          STATE_RECIPROCAL_TYPE_FULL:
            begin
              RecipStateTax := 0;
              CalcStateTaxesPartII(CheckNumber, RecipCOStateNumber, AdjStateTaxWages, RecipStateTax, True);
              if HasTaxOverride then
                RecipStateTax := 0;
            end;
          STATE_RECIPROCAL_TYPE_FLAT:
            RecipStateTax := EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'RECIPROCAL_AMOUNT_PERCENTAGE');
          STATE_RECIPROCAL_TYPE_PERCENTAGE:
            RecipStateTax := AdjStateTaxWages * EE_STATES.NewLookup('EE_STATES_NBR', EEStateNumber, 'RECIPROCAL_AMOUNT_PERCENTAGE');
        end;
        if RecipStateTax <> 0 then
        begin
          if (SY_STATES.NewLookup('SY_STATES_NBR', DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', RecipCOStateNumber, 'SY_STATES_NBR'), 'ROUND_TO_NEAREST_DOLLAR') = 'Y') and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
            RecipStateTax := RoundAll(RecipStateTax, 0);
          if not ShortfallsDisabled and (DM_COMPANY.CO.FieldByName('MAKE_UP_TAX_DEDUCT_SHORTFALLS').Value = 'Y') then
          begin
            Q := TevQuery.Create('StateShortfall');
            Q.Param['EEStateNbr'] := RecipEEStateNumber;
            Q.Param['CheckNbr'] := CheckNumber;
            Q.Macro['CheckType'] := '''' + CHECK_TYPE2_REGULAR + ''', ''' + CHECK_TYPE2_VOID + '''';
            if ConvertNull(Q.Result['STATE_SHORTFALL'], 0) <> 0 then
            begin
              RecipStateTax := RecipStateTax + Q.Result['STATE_SHORTFALL'];
              if (SY_STATES.NewLookup('SY_STATES_NBR', DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', RecipCOStateNumber, 'SY_STATES_NBR'), 'ROUND_TO_NEAREST_DOLLAR') = 'Y') and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
                RecipStateTax := RoundAll(RecipStateTax, 0);
            end;
          end;
        end;
      end;
    end;
    if not ShortfallsDisabled and (DM_COMPANY.CO.FieldByName('MAKE_UP_TAX_DEDUCT_SHORTFALLS').Value = 'Y') then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('StateShortfall') do
      begin
        SetParam('EEStateNbr', EEStateNumber);
        SetParam('CheckNbr', CheckNumber);
        SetMacro('CheckType', '''' + CHECK_TYPE2_REGULAR + ''', ''' + CHECK_TYPE2_VOID + '''');
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.Last;
      StateTax := StateTax + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('STATE_SHORTFALL').Value, 0);
      ctx_DataAccess.CUSTOM_VIEW.Close;
    end;

    if TaxCredit <> 0 then
    begin
      if TaxCredit > StateTax then
        StateTax := 0
      else
        StateTax := StateTax - TaxCredit;
    end;
    ctx_PayrollCalculation.SetStateTax(RoundAll(StateTax, 2));
    StateTax := StateTax * TaxMultiplier;
    if TaxWagesMultiplier <> 0 then
    begin
      if StateTaxWages * TaxWagesMultiplier > StateTax then
        StateTax := 0
      else
        StateTax := StateTax - StateTaxWages * TaxWagesMultiplier;
    end;
    if (DM_SYSTEM_STATE.SY_STATES.NewLookup('STATE', State, 'ROUND_TO_NEAREST_DOLLAR') = 'Y') and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) and not HasTaxOverride then
      StateTax := RoundAll(StateTax, 0);
  end;
end;

{SR-70}

procedure TevGenericGrossToNet.CalcEEOASDITaxes(CheckNumber: Integer; var OASDITax, OASDITaxWages, OASDITaxTips, OASDIGrossWages: Real; PR, PR_CHECK, PR_CHECK_LINES:
  TevClientDataSet);
var
  YTDOASDITaxWagesTips, YTDOASDITax, OASDIWageLimit: Real;
  EDCodeType: string;

  function TypeIsTips(const ACode: String): Boolean;
  begin
    if CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2013, 12, 31)) = GreaterThanValue then
      Result := TypeIsTipsAfter2013(ACode)
    else
      Result := TypeIsTipsBefore2014(ACode);  
  end;

begin
  OASDITax := 0;
  OASDITaxWages := 0;
  OASDITaxTips := 0;
  OASDIGrossWages := 0;
  with DM_HISTORICAL_TAXES do
  begin
    if not YTDsDisabled then
      ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(CheckNumber, 0, ltEEOASDI, PR, PR_CHECK, nil, nil, nil, YTDOASDITaxWagesTips,
        YTDOASDITax, 0, GetEndYear(PR.FieldByName('CHECK_DATE').Value))
    else
    begin
      YTDOASDITaxWagesTips := 0;
      YTDOASDITax := 0;
    end;
    if DM_COMPANY.CO.FieldByName('FED_TAX_EXEMPT_EE_OASDI').Value = 'Y' then
      Exit;
    if EE.FieldByName('EXEMPT_EMPLOYEE_OASDI').AsString = 'Y' then
      Exit;
    PR_CHECK_LINES.First;
    with PR_CHECK_LINES do
    while not EOF do
    begin
      EDCodeType := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
      if TypeIsTaxable(EDCodeType) then
        if SY_FED_EXEMPTIONS.NewLookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_EMPLOYEE_OASDI') <> 'Y' then
          if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'EE_EXEMPT_EXCLUDE_OASDI') <> 'E' then
          begin
            if TypeIsTips(EDCodeType) then
              OASDITaxTips := OASDITaxTips + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value)
            else
              OASDITaxWages := OASDITaxWages + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
          end;
      Next;
    end;
    OASDIGrossWages := OASDITaxWages + OASDITaxTips;
    OASDIWageLimit := SY_FED_TAX_TABLE.FieldByName('OASDI_WAGE_LIMIT').Value;
    if YTDOASDITaxWagesTips >= OASDIWageLimit then
    begin
      if SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value * OASDIWageLimit <> YTDOASDITax then
        OASDITax := SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value * OASDIWageLimit - YTDOASDITax;
      OASDITaxWages := 0;
      OASDITaxTips := 0;
      Exit;
    end;
    if OASDITaxWages + YTDOASDITaxWagesTips > OASDIWageLimit then
    begin
      OASDITaxWages := OASDIWageLimit - YTDOASDITaxWagesTips;
      OASDITaxTips := 0;
    end
    else
    begin
      if OASDITaxWages + OASDITaxTips + YTDOASDITaxWagesTips > OASDIWageLimit then
        OASDITaxTips := OASDIWageLimit - YTDOASDITaxWagesTips - OASDITaxWages;
    end;
    if PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_OASDI').Value = 'Y' then
      Exit;
    if not PR_CHECK.FieldByName('OR_CHECK_OASDI').IsNull then
    begin
      OASDITax := PR_CHECK.FieldByName('OR_CHECK_OASDI').Value;
      Exit;
    end;

    OASDITax := SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value * (OASDITaxWages + OASDITaxTips);
    if not ShortfallsDisabled and (Abs(SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value * YTDOASDITaxWagesTips - YTDOASDITax) > TaxShortfallPickupLimit) then
      OASDITax := OASDITax + SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value * YTDOASDITaxWagesTips - YTDOASDITax;

  end;
end;

{SR-80}

procedure TevGenericGrossToNet.CalcEEMedicareTaxes(CheckNumber: Integer;
  var MedicareTax, MedicareTaxWages, MedicareGrossWages: Real; PR, PR_CHECK, PR_CHECK_LINES: TevClientDataSet);
var
  YTDMedicareTaxWages, YTDMedicareTax,
  MedicareWageLimit, MedicareThresholdValue,
  MedicareThresholdRate, WagesBelowThreshold,
  WagesOverThreshold, MedicareRate,
  CalcYTDMedicareTax: Real;
  EDCodeType: string;
begin
  MedicareTax := 0;
  MedicareTaxWages := 0;
  MedicareGrossWages := 0;
  with DM_HISTORICAL_TAXES do
  begin
    if not YTDsDisabled then
      ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(CheckNumber, 0, ltEEMedicare, PR, PR_CHECK, nil, nil, nil, YTDMedicareTaxWages,
        YTDMedicareTax, 0, GetEndYear(PR.FieldByName('CHECK_DATE').Value))
    else
    begin
      YTDMedicareTaxWages := 0;
      YTDMedicareTax := 0;
    end;
    if DM_COMPANY.CO.FieldByName('FED_TAX_EXEMPT_EE_MEDICARE').Value = 'Y' then Exit;
    if EE.FieldByName('EXEMPT_EMPLOYEE_MEDICARE').AsString = 'Y' then Exit;
    PR_CHECK_LINES.First;
    with PR_CHECK_LINES do
    while not EOF do
    begin
      EDCodeType := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
      if TypeIsTaxable(EDCodeType) then
        if SY_FED_EXEMPTIONS.NewLookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_EMPLOYEE_MEDICARE') <> 'Y' then
          if CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'EE_EXEMPT_EXCLUDE_MEDICARE') <> 'E' then
            MedicareTaxWages := MedicareTaxWages + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
      Next;
    end;
    MedicareGrossWages := MedicareTaxWages;
    MedicareWageLimit := SY_FED_TAX_TABLE.FieldByName('MEDICARE_WAGE_LIMIT').Value;
    if YTDMedicareTaxWages > MedicareWageLimit then
    begin
      if SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value * MedicareWageLimit <> YTDMedicareTax then
        MedicareTax := SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value * MedicareWageLimit - YTDMedicareTax;
      MedicareTaxWages := 0;
      Exit;
    end;
    if MedicareTaxWages + YTDMedicareTaxWages > MedicareWageLimit then
      MedicareTaxWages := MedicareWageLimit - YTDMedicareTaxWages;
    if PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').Value = 'Y' then
      Exit;
    if not PR_CHECK.FieldByName('OR_CHECK_MEDICARE').IsNull then
    begin
      MedicareTax := PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value;
      Exit;
    end;

    MedicareRate := SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value;
    MedicareThresholdValue := SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_LIMIT').Value;
    MedicareThresholdRate := SY_FED_TAX_TABLE.FieldByName('EE_MED_TH_RATE').Value;

    if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue)
    and ((YTDMedicareTaxWages + MedicareTaxWages) > MedicareThresholdValue) then
    begin
      WagesOverThreshold := YTDMedicareTaxWages + MedicareTaxWages - MedicareThresholdValue;
      if WagesOverThreshold > MedicareTaxWages then
      begin
        WagesOverThreshold := MedicareTaxWages;
        WagesBelowThreshold := 0;
      end
      else
      begin
        WagesBelowThreshold := MedicareTaxWages - WagesOverThreshold;
      end;
    end
    else
    begin
      WagesBelowThreshold := MedicareTaxWages;
      WagesOverThreshold := 0;
    end;

    if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2012, 12, 31)) = GreaterThanValue)
    and (YTDMedicareTaxWages > MedicareThresholdValue) then
    begin
      CalcYTDMedicareTax := MedicareRate * MedicareThresholdValue +
       (YTDMedicareTaxWages - MedicareThresholdValue) * MedicareThresholdRate;
    end
    else
    begin
      CalcYTDMedicareTax := YTDMedicareTaxWages * MedicareRate;
    end;


    MedicareTax := MedicareRate * WagesBelowThreshold + WagesOverThreshold * MedicareThresholdRate;

    if not ShortfallsDisabled and (Abs(CalcYTDMedicareTax - YTDMedicareTax) > TaxShortfallPickupLimit) then
      MedicareTax := MedicareTax + CalcYTDMedicareTax - YTDMedicareTax;
  end;
end;

{SR-100}

procedure TevGenericGrossToNet.CalcEICTaxes(CheckNumber: Integer; var EICTax: Real; PR_CHECK: TevClientDataSet);
var
  AnnualFedTaxWages, TaxFreq, MaritalStatusMultiplier, SecondLimit: Real;
begin
  EICTax := 0;
  with DM_HISTORICAL_TAXES do
  begin
    if PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_EIC').Value = 'Y' then Exit;
    if not PR_CHECK.FieldByName('OR_CHECK_EIC').IsNull then
    begin
      EICTax := PR_CHECK.FieldByName('OR_CHECK_EIC').Value;
      Exit;
    end;
    if (Trim(EE.FieldByName('EIC').AsString) = '') or (EE.FieldByName('EIC').Value = EIC_NONE) then
      Exit;
    TaxFreq := GetTaxFreq(PR_CHECK.FieldByName('TAX_FREQUENCY').Value);
    AnnualFedTaxWages := PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value * TaxFreq;

    if AnnualFedTaxWages < 0 then
      Exit;

    if EE.FieldByName('EIC').AsString = EIC_BOTH_SPOUSES then
      MaritalStatusMultiplier := 0.5
    else
      MaritalStatusMultiplier := 1;
    if AnnualFedTaxWages < SY_FED_TAX_TABLE.FieldByName('FIRST_EIC_LIMIT').Value * MaritalStatusMultiplier then
      EICTax := AnnualFedTaxWages * SY_FED_TAX_TABLE.FieldByName('FIRST_EIC_PERCENTAGE').Value
    else
    begin
      SecondLimit := SY_FED_TAX_TABLE.FieldByName('SECOND_EIC_LIMIT').Value;
      if EE.FieldByName('EIC').AsString = EIC_SINGLE then // This will be in the database later
      begin
        if DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime < StrToDate('01/01/2005') then
          SecondLimit := SecondLimit - 1000
        else
          SecondLimit := SecondLimit - 2000;
      end;
      if AnnualFedTaxWages < SecondLimit * MaritalStatusMultiplier then
        EICTax := SY_FED_TAX_TABLE.FieldByName('SECOND_EIC_AMOUNT').Value * MaritalStatusMultiplier
      else
      begin
        EICTax := SY_FED_TAX_TABLE.FieldByName('SECOND_EIC_AMOUNT').Value * MaritalStatusMultiplier
          - (AnnualFedTaxWages - SecondLimit * MaritalStatusMultiplier)
          * SY_FED_TAX_TABLE.FieldByName('THIRD_EIC_ADDITIONAL_PERCENT').Value;
        if EICTax < 0 then
        begin
          EICTax := 0;
          Exit;
        end;
      end;
    end;
    EICTax := EICTax / TaxFreq;
  end;
end;

{SR-160, SR-170}

procedure TevGenericGrossToNet.CalcEESDITaxes(CheckNumber, COStateNumber: Integer;
  var SDITax, SDITaxWages, SDIGrossWages: Real; PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES: TevClientDataSet);
var
  YTDTaxOwed, YTDTaxWages, TempTaxWages, TaxLimit, YTDSDITax,
  AdjTaxWages, AdjYTDTaxWages, CutOffWages: Real;
  TaxFreq, SYStateNumber, EENumber, EEStateNumber, PRNumber: Integer;
  State, EDCodeType: string;
  CheckDate, FollowingCheckDate: TDateTime;
  PR_CHECK_2, PR_CHECK_STATES_2: TevClientDataSet;
  Q, SS: IevQuery;
  view: IevDataSet;
begin
  SDITax := 0;
  YTDSDITax := 0;
  SDITaxWages := 0;
  SDIGrossWages := 0;
// Get SY_STATE_NBR based on CO_STATE_NBR
  State := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'STATE');

  if (State = 'NY') or (State = 'HI') then
    Exit; {SDIs for NY and HI are done as deduction}

  SYStateNumber := HT.SY_STATES.Lookup('STATE', State, 'SY_STATES_NBR');
  if ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE'), 0) = 0 then
    Exit;
  if HT.CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'EE_SDI_EXEMPT') = 'Y' then
    Exit;
  EENumber := PR_CHECK.FieldByName('EE_NBR').Value;
  EEStateNumber := HT.EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EENumber, COStateNumber]), 'EE_STATES_NBR');
  if HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SDI_EXEMPT_EXCLUDE') = 'E' then
    Exit;

  if State = 'NM' then
  begin
    if FSecondCheck then
      Exit;
    if PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value = 'Y' then
      Exit;
    if HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SDI_EXEMPT_EXCLUDE') = 'X' then
      Exit;
    FollowingCheckDate := ctx_PayrollCalculation.GetNextCheckDate(PR.FieldByName('CHECK_DATE').Value, DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString[1]);
    if GetQuarterNumber(PR.FieldByName('CHECK_DATE').Value) <> GetQuarterNumber(FollowingCheckDate) then
      SDITax := HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE');
    Exit;
  end;

  CheckDate := PR.FieldByName('CHECK_DATE').AsDateTime;
  PRNumber := PR_CHECK.FieldByName('PR_NBR').Value;
  TaxFreq := GetTaxFreq(PR_CHECK.FieldByName('TAX_FREQUENCY').Value);
// SR-170
  YTDTaxOwed := 0;
  YTDTaxWages := 0;
  AdjYTDTaxWages := 0;
  if not YTDsDisabled then
  begin

    Q := TevQuery.Create('EESDITaxes');
    Q.Params.AddValue('EEStateNbr', EEStateNumber);
    Q.Params.AddValue('EENbr', EENumber);
    Q.Params.AddValue('PrNbr', PrNumber);
    Q.Params.AddValue('StartDate', GetBeginYear(CheckDate));
    Q.Params.AddValue('EndDate', GetEndYear(CheckDate));
    view := Q.Result;

    SS := TevQuery.Create('select WEEKLY_SDI_TAX_CAP, EE_SDI_RATE, EE_SDI_MAXIMUM_WAGE from SY_STATES where SY_STATES_NBR=' + IntToStr(SYStateNumber)+
      ' and {AsOfDate<SY_STATES>}');
    SS.Params.AddValue('StatusDate', CheckDate);

    view.First;
    while not view.Eof do
    begin

      TempTaxWages := YTDTaxWages;
      AdjTaxWages := ConvertNull(view.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0);
      TaxLimit := ConvertNull(SS.Result.FieldByName('WEEKLY_SDI_TAX_CAP').Value, 0) * 52 / TaxFreq;
      if ConvertNull(view.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0) * ConvertNull(SS.Result.FieldByName('EE_SDI_RATE').Value, 0) > TaxLimit then
      begin
        YTDTaxOwed := YTDTaxOwed + TaxLimit;
        YTDTaxWages := YTDTaxWages + TaxLimit / SS.Result.FieldByName('EE_SDI_RATE').Value;
      end
      else
      if AdjTaxWages < 0 then
      begin
        // for void check we need to calculate the actual tax wages we are rolling back
        if YTDTaxWages <= AdjYTDTaxWages then
        begin
          // calculating the amount that was above the maximum and has been ignored
          CutOffWages := AdjYTDTaxWages - YTDTaxWages;

          if (YTDTaxWages + AdjTaxWages) + CutOffWages > ConvertNull(SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value, 0) then
            CutOffWages := ConvertNull(SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value, 0) - (YTDTaxWages + AdjTaxWages);

          YTDTaxWages := YTDTaxWages + ConvertNull(view.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0) + CutOffWages;
          AdjTaxWages := AdjTaxWages + CutOffWages;

        end;
      end
      else
        YTDTaxWages := YTDTaxWages + ConvertNull(view.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0);

      AdjYTDTaxWages := AdjYTDTaxWages + ConvertNull(view.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0);

      YTDSDITax := YTDSDITax + ConvertNull(view.FieldByName('EE_SDI_TAX').Value, 0);
      if (ConvertNull(SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value, 0) > 0)
      and (YTDTaxWages >= SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value) then
      begin
        YTDTaxWages := SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value;
        YTDTaxOwed := YTDTaxOwed + RoundTwo((YTDTaxWages - TempTaxWages) * ConvertNull(SS.Result.FieldByName('EE_SDI_RATE').Value, 0));
      end
      else
        YTDTaxOwed := YTDTaxOwed + RoundTwo(AdjTaxWages * ConvertNull(SS.Result.FieldByName('EE_SDI_RATE').Value, 0));
      view.Next;
    end;

// This piece is to take into consideration multiple checks for one EE within the same payroll
    TaxLimit := ConvertNull(SS.Result.FieldByName('WEEKLY_SDI_TAX_CAP').Value, 0) * 52 / TaxFreq;
    PR_CHECK_2 := TevClientDataSet.Create(nil);
    PR_CHECK_STATES_2 := TevClientDataSet.Create(nil);
    try
      PR_CHECK_2.CloneCursor(PR_CHECK, False);
      PR_CHECK_STATES_2.CloneCursor(PR_CHECK_STATES, False);
      PR_CHECK_2.First;
      while (not PR_CHECK_2.EOF) and (PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger <> CheckNumber) do
      begin
        if PR_CHECK_2.FieldByName('PR_NBR').AsInteger = PRNumber then
        begin
          TempTaxWages := YTDTaxWages;
          YTDSDITax := YTDSDITax + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
            VarArrayOf([PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, EEStateNumber]), 'EE_SDI_TAX'), 0);
          if ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value,
            EEStateNumber]), 'EE_SDI_TAXABLE_WAGES'), 0) * ConvertNull(SS.Result.FieldByName('EE_SDI_RATE').Value, 0) > TaxLimit then
          begin
            YTDTaxOwed := YTDTaxOwed + TaxLimit;
            YTDTaxWages := YTDTaxWages + TaxLimit / SS.Result.FieldByName('EE_SDI_RATE').Value;
          end
          else
          begin
            YTDTaxWages := YTDTaxWages + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
              VarArrayOf([PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, EEStateNumber]), 'EE_SDI_TAXABLE_WAGES'), 0);
          end;
          if (ConvertNull(SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value, 0) > 0)
          and (YTDTaxWages >= SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value) then
          begin
            YTDTaxWages := SS.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value;
            YTDTaxOwed := YTDTaxOwed + (YTDTaxWages - TempTaxWages) * ConvertNull(SS.Result.FieldByName('EE_SDI_RATE').Value, 0);
          end
          else
            YTDTaxOwed := YTDTaxOwed + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
              VarArrayOf([PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, EEStateNumber]), 'EE_SDI_TAXABLE_WAGES'), 0)
              * ConvertNull(SS.Result.FieldByName('EE_SDI_RATE').Value, 0);
        end;
        PR_CHECK_2.Next;
      end;
    finally
      PR_CHECK_2.Free;
      PR_CHECK_STATES_2.Free;
    end;
// End of multiple checks piece...

// Get SDI tax wages for other companies that belong to the same Common Paymaster
    with TExecDSWrapper.Create('SdiTemplate') do
    begin
      SetMacro('F1', 'EE_SDI_TAXABLE_WAGES');
      SetMacro('F2', 'EE_SDI_TAX');
      SetMacro('Filter', 'CO.CO_NBR<>:CONbr and');
      SetParam('CONbr', PR.Lookup('PR_NBR', PRNumber, 'CO_NBR'));
      SetParam('CONbrForPM', PR.Lookup('PR_NBR', PRNumber, 'CO_NBR'));
      SetParam('CLPersonNbr', HT.EE.Lookup('EE_NBR', EENumber, 'CL_PERSON_NBR'));
      SetParam('RecordNbr', EEStateNumber);
      SetParam('EENbr', EENumber);
      SetParam('Act', 2);
      SetParam('PayrollNbr', PRNumber);
      SetParam('BeginDate', GetBeginYear(CheckDate));
      SetParam('EndDate', GetEndYear(CheckDate));
      SetMacro('Status', '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''');

      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.CUSTOM_VIEW.Open;

      YTDTaxWages := YTDTaxWages + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('TAX_WAGE').Value, 0);
    end;
  end
  else
    YTDSDITax := 0;

  PR_CHECK_LINES.First;
  with PR_CHECK_LINES do
    while not EOF do
    begin
      if (FieldByName('EE_STATES_NBR').IsNull
      and (HT.EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EENumber, HT.EE_STATES.Lookup('EE_STATES_NBR', HT.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value , 'SDI_APPLY_CO_STATES_NBR')]), 'EE_STATES_NBR') = EEStateNumber))
      or (HT.EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EENumber, HT.EE_STATES.Lookup('EE_STATES_NBR', FieldByName('EE_STATES_NBR').Value , 'SDI_APPLY_CO_STATES_NBR')]), 'EE_STATES_NBR') = EEStateNumber) then
      begin
        EDCodeType := HT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
        if TypeIsTaxable(EDCodeType) then
          if HT.SY_STATE_EXEMPTIONS.NewLookup('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([SYStateNumber, EDCodeType]), 'EXEMPT_EMPLOYEE_SDI') <> 'Y' then
            if HT.CL_E_D_STATE_EXMPT_EXCLD.Lookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber,
              PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value]), 'EMPLOYEE_EXEMPT_EXCLUDE_SDI') <> GROUP_BOX_EXE then
            begin
              SDITaxWages := SDITaxWages + ConvertDeduction(EDCodeType, FieldByName('AMOUNT').Value);
            end;
      end;
      Next;
    end;
  SDIGrossWages := SDITaxWages;
  TaxLimit := ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'WEEKLY_SDI_TAX_CAP'), 0) * 52 / TaxFreq;
  if SDITaxWages * ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE'), 0) > TaxLimit then
  begin
    SDITax := TaxLimit;
    SDITaxWages := TaxLimit / HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE');
  end
  else
    SDITax := SDITaxWages * ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE'), 0);
  if (PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value = 'Y')
  or (HT.EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SDI_EXEMPT_EXCLUDE') = 'X') then
  begin
    SDITax := 0;
    if (YTDTaxWages >= HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE'))
    and (ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE'), 0) <> 0) then
      SDITaxWages := 0;
    Exit;
  end;
  if (YTDTaxWages >= HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE'))
  and (ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE'), 0) <> 0) then
  begin
    SDITax := YTDTaxOwed - YTDSDITax;
    if not ShortfallsDisabled and (SDITax - 0.005 <= TaxShortfallPickupLimit) then
      SDITax := 0;
    SDITaxWages := 0;
    Exit;
  end;
  if (SDITaxWages + YTDTaxWages > HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE')) and
  (ConvertNull(HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE'), 0) <> 0) then
  begin
    SDITaxWages := HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_MAXIMUM_WAGE') - YTDTaxWages;
    SDITax := SDITaxWages * HT.SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE');
  end;
  if not PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').IsNull then
  begin
    SDITax := PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value;
    Exit;
  end;
  if not ShortfallsDisabled and (Abs(YTDTaxOwed - YTDSDITax) > TaxShortfallPickupLimit) then
    SDITax := SDITax + YTDTaxOwed - YTDSDITax;
end;

{SR-170, SR-180, SR-190}

function TevGenericGrossToNet.Calculate(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction, FetchPayrollData: Boolean;
  PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS: TevClientDataSet;
  PayrollIsProcessing: Boolean = False; JustPreProcessing: Boolean = True; ForTaxCalculator: Boolean = False;
  DisableYTDs: Boolean = False; DisableShortfalls: Boolean = False; SecondCheck: Boolean = False; NetToGrossCalc: Boolean = False): Boolean;
var
  Gross, Amount, OldNet, Hours, TaxWages, TaxTips, TaxRate, Tax, RecipTax, GrossWages, ShortFall, Percentage, Differential, DirectDeposit: Real;
  RecalcCount, SchedDirDepNumber, SaveCheckNbr, EERateNbr, EEStatesNbr, RecipStateNbr, EmployeeNbr, ALDGroupNbr, I, EDNbr, OldRecordCount: Integer;
  EDCodeType, Temp: string;
  Recalculate, bOldVal, IsImport, HasTips, SubtractChildSupport: Boolean;
  PR_CHECK_LINES_2, PR_CHECK_STATES_2, PR_CHECK_SUI_2, PR_CHECK_LOCALS_2, CUSTOM_VIEW_2: TevClientDataSet;
  MyTransactionManager: TTransactionManager;
  SpecTaxedDeds: TSpecTaxedDeds;
  StatesList, SUIList: TStringList;
  CORecipStateNbr: Variant;
  AsOfDate: TDateTime;

  function PermitType(EDType: String): Boolean;
  begin
    Result := True;
    if EDType = ED_DED_CHILD_SUPPORT then
      Result := SubtractChildSupport;
    if (EDType = ED_DED_GARNISH)
    and ((DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_GARNISHMENT_CHILD)
    or (DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_CHILD_GARNISHMENT)
    or (DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_GARNISHMENT)) then
      Result := False;
  end;

  procedure AdjustDeduction(LineNumber: Integer);
  var
    DEDUCTIONS_TO_ZERO: String;
    Adjusted: Boolean;
  begin
    Adjusted := False;
    PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', LineNumber, []);
    Amount := ConvertNull(PR_CHECK_LINES['AMOUNT'], 0) + ConvertNull(PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'], 0);
    if PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_MANUAL then
    begin
      ctx_DataAccess.RecalcLineBeforePost := False;
      try
        Adjusted := (Net - Amount) < 0.005;
        if (Amount > Net) and (Amount - Net >= 0.005) then
        begin
          Adjusted := True;
          PR_CHECK_LINES.Edit;

          DEDUCTIONS_TO_ZERO := ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'DEDUCTIONS_TO_ZERO'),'');
          if DEDUCTIONS_TO_ZERO = '' then
          begin
            DEDUCTIONS_TO_ZERO := ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_NBR;CL_E_DS_NBR', VarArrayOf([PR_CHECK['EE_NBR'], PR_CHECK_LINES['CL_E_DS_NBR']]), 'DEDUCTIONS_TO_ZERO'),'');
          end;

          if DEDUCTIONS_TO_ZERO = 'Y' then
          begin
            if Net > 0 then
            begin
              PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := RoundTwo(Amount - Net);
              PR_CHECK_LINES['AMOUNT'] := RoundTwo(Net);
            end
            else
            begin
              PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := RoundTwo(Amount);
              PR_CHECK_LINES['AMOUNT'] := RoundTwo(0);
            end;
          end
          else
          begin
            PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := RoundTwo(Amount);
            PR_CHECK_LINES['AMOUNT'] := 0;
          end;
          PR_CHECK_LINES.Post;
        end
        else
        begin
          if ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0) <> Amount then
          begin
            PR_CHECK_LINES.Edit;
            PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value := Null;
            PR_CHECK_LINES.FieldByName('AMOUNT').Value := Amount;
            PR_CHECK_LINES.Post;
          end;
        end;
      finally
        ctx_DataAccess.RecalcLineBeforePost := True;
      end;
    end;
    if not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and not IsImport and (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value <> CHECK_LINE_TYPE_SCHEDULED) then
    begin
      if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) and not Adjusted then
      begin
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
        PR_CHECK_LINES.Edit;
        if ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, EmployeeNbr, JustPreProcessing) then
          PR_CHECK_LINES.Post
        else
        begin
          PR_CHECK_LINES.Edit;
          PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
          PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value := 0;
          PR_CHECK_LINES.Post;
        end;
      end
      else
      if (PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_MANUAL) and Adjusted then
      begin
        ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, EmployeeNbr, JustPreProcessing, True);        
      end;
    end;
    Net := Net - ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
  end;

//SR-210
  procedure DeductChildSupport(BeforeTaxes: Boolean);
  var
    EDCodeType: String;
    ProrateNbr, ProrateSum: Integer;
    OldNet, ProrateRatio: Real;
  begin
    if not SubtractChildSupport or BeforeTaxes then
      Exit;
    if not BeforeTaxes then
      SubtractChildSupport := False;
    DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.IndexFieldNames := 'EE_NBR;PRIORITY_NUMBER';
    DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.SetRange([EmployeeNbr], [EmployeeNbr]);
    try
      ProrateNbr := 0;
      ProrateSum := 0;
      OldNet := Net;
      DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.First;
      if DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('PRIORITY_NUMBER').Value < 0 then
      begin
        while not DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.EOF do
        begin
          if DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('PRIORITY_NUMBER').Value < 0 then
          begin
            if DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_CHILD_SUPPORT_CASES_NBR;SCHEDULED_E_D_ENABLED', VarArrayOf([DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').Value, GROUP_BOX_YES]), []) then
            if (PR.FieldByName('CHECK_DATE').Value >= DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').Value)
            and ((PR.FieldByName('CHECK_DATE').Value <= DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').Value)
            or DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').IsNull) then
            begin
              ProrateSum := ProrateSum + Abs(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('PRIORITY_NUMBER').AsInteger);
              Inc(ProrateNbr);
            end;
          end;
          DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.Next;
        end;
        DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.First;
      end;
      while not DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.EOF do
      begin
        PR_CHECK_LINES.First;
        while not PR_CHECK_LINES.EOF do
        begin
          if (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED)
          and (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
            EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
            if (EDCodeType = ED_DED_CHILD_SUPPORT) and (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').Value = DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').Value) then
            begin
              if DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('PRIORITY_NUMBER').Value >= 0 then
                ProrateRatio := 1
              else
              begin
                if ProrateSum = 0 then
                  raise EevException.Create('Scheduled E/Ds need to be refreshed for EE#'+
                    DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString);
                ProrateRatio := Abs(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.FieldByName('PRIORITY_NUMBER').AsInteger) / ProrateSum;
              end;  
              PR_CHECK_LINES.Edit;
              if ctx_PayrollCalculation.ScheduledEDRecalc(EDCodeType, Gross, Net, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
              PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES,
              DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE, DM_HISTORICAL_TAXES.SY_STATES, JustPreProcessing, ProrateNbr, ProrateRatio, OldNet) then
                PR_CHECK_LINES.Post;
              AdjustDeduction(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value);
            end;
          end;
          PR_CHECK_LINES.Next;
        end;
        DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.Next;
      end;

      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.EOF do
      begin
        if (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE') = ED_DED_CHILD_SUPPORT)
        and ((PR_CHECK_LINES.FieldByName('LINE_TYPE').Value <> CHECK_LINE_TYPE_SCHEDULED)
        or VarIsNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, 'EE_CHILD_SUPPORT_CASES_NBR'))) then
          AdjustDeduction(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value);
        PR_CHECK_LINES.Next;
      end;
    finally
      DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.CancelRange;
      DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.IndexFieldNames := '';
    end;
  end;

//SR-220

  procedure DeductGarnishment;
  var
    CUSTOM_VIEW_2: TevClientDataSet;
  begin
    ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('DeductGarnishment') do
    begin
      SetParam('CheckNbr', CheckNumber);
      SetParam('CodeType', ED_DED_GARNISH);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;

    CUSTOM_VIEW_2 := TevClientDataSet.Create(nil);
    try
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
      ctx_DataAccess.CUSTOM_VIEW.Close;

      CUSTOM_VIEW_2.First;
      with CUSTOM_VIEW_2 do
        while not EOF do
        begin
          PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
          if (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) and (not
          PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
            PR_CHECK_LINES.Edit;
            if ctx_PayrollCalculation.ScheduledEDRecalc(ED_DED_GARNISH, Gross, Net, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
            PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES,
            DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE, DM_HISTORICAL_TAXES.SY_STATES, JustPreProcessing) then
              PR_CHECK_LINES.Post;
          end;

          AdjustDeduction(FieldByName('PR_CHECK_LINES_NBR').Value);
          Next;
        end;
    finally
      CUSTOM_VIEW_2.Free;
    end;
  end;

  procedure AddStateSUI(COStateNbr: Variant);
  var
    CoSuiNbr: String;
  begin
    if VarIsNull(COStateNbr) then
      Exit;

    DM_HISTORICAL_TAXES.CO_SUI.Filter := 'CO_STATES_NBR=' + VarToStr(COStateNbr);
    DM_HISTORICAL_TAXES.CO_SUI.Filtered := True;
    try
      DM_HISTORICAL_TAXES.CO_SUI.First;
      while not DM_HISTORICAL_TAXES.CO_SUI.EOF do
      begin
        CoSuiNbr := DM_HISTORICAL_TAXES.CO_SUI.FieldByName('CO_SUI_NBR').AsString;
        if (SUIList.IndexOf(CoSuiNbr) = -1) then
        begin
          if (DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').Value, 'SUI_ACTIVE') = 'Y') then
          begin
            if (DM_HISTORICAL_TAXES.CO_SUI.FieldByName('FINAL_TAX_RETURN'{Really means "SUI Inactive"}).AsString = 'N') then
              SUIList.Add(CoSuiNbr);
          end;
        end;
        DM_HISTORICAL_TAXES.CO_SUI.Next;
      end;
    finally
      DM_HISTORICAL_TAXES.CO_SUI.Filtered := False;
      DM_HISTORICAL_TAXES.CO_SUI.Filter := '';
    end;

  end;

  procedure ProcessFed;
  begin
    if (not IsImport) or (IsImport and (PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').IsNull
    or PR_CHECK.FieldByName('FEDERAL_TAX').IsNull)) then
    begin
      CalcFedTaxes(CheckNumber, Tax, TaxWages, PR_CHECK, PR_CHECK_LINES, PR);
      if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO) and
      (PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
      begin
        Tax := PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value;
        if (Tax > Net) and (Tax - Net >= 0.005) and not NegCheckAllowed then
          raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
      end;
    end;

    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').IsNull) then
      PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value := RoundTwo(TaxWages);
    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('FEDERAL_TAX').IsNull) then
      PR_CHECK.FieldByName('FEDERAL_TAX').Value := Tax;

    TaxWages := ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value, 0);
    Tax := ConvertNull(PR_CHECK.FieldByName('FEDERAL_TAX').Value, 0);

    Tax := RoundTwo(Tax);
    ShortFall := 0;
    if (Tax > Net) and (not NegCheckAllowed) then
    begin
      ShortFall := Tax - Net;
      Tax := Net;
      Net := 0;
      Result := True;
    end
    else
      Net := Net - Tax;
    PR_CHECK.FieldByName('FEDERAL_TAX').Value := Tax;
    PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value := ShortFall;
    PR_CHECK.FieldByName('FEDERAL_GROSS_WAGES').Value := PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').Value;

// EIC (needs to be done after Federal)
    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('EE_EIC_TAX').IsNull) then
      CalcEICTaxes(CheckNumber, Tax, PR_CHECK)
    else
      Tax := ConvertNull(PR_CHECK.FieldByName('EE_EIC_TAX').Value, 0);
    Tax := RoundTwo(Tax);
    if (Tax * (-1) >= Net) and (not NegCheckAllowed) then
    begin
      Tax := Net * (-1);
      Net := 0;
    end
    else
      Net := Net + Tax;
    PR_CHECK.FieldByName('EE_EIC_TAX').Value := Tax;
  end;

  procedure ProcessSUIs;
  var
    I: Integer;
  begin
// Construct a list of SUIs for the check
    if VarIsNull(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value, 'SUI_APPLY_CO_STATES_NBR')) then
      raise EInconsistentData.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) +
        ' home SUI state is empty! Check calculation has been stopped.', IDH_InconsistentData);
    SUIList := TStringList.Create;

    try
      if IsImport or ForTaxCalculator then
      with PR_CHECK_SUI_2 do
      begin
        PR_CHECK_SUI_2.CloneCursor(PR_CHECK_SUI, True);
        Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
        Filtered := True;
        First;
        while not EOF do
        begin
          if (SUIList.IndexOf(FieldByName('CO_SUI_NBR').AsString) = -1) then
            SUIList.Add(FieldByName('CO_SUI_NBR').AsString);
          Next;
        end;
      end;

      with PR_CHECK_LINES_2 do
      begin
        AddStateSUI(DM_EMPLOYEE.EE_STATES.NewLookup('EE_STATES_NBR', DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value, 'SUI_APPLY_CO_STATES_NBR'));
        First;
        while not EOF do
        begin
          if not FieldByName('EE_SUI_STATES_NBR').IsNull then
          begin
            AddStateSUI(DM_EMPLOYEE.EE_STATES.NewLookup('EE_STATES_NBR', FieldByName('EE_SUI_STATES_NBR').Value, 'SUI_APPLY_CO_STATES_NBR'));
          end
          else
          begin
            if not FieldByName('EE_STATES_NBR').IsNull then
            begin
              AddStateSUI(DM_EMPLOYEE.EE_STATES.NewLookup('EE_STATES_NBR', FieldByName('EE_STATES_NBR').Value, 'SUI_APPLY_CO_STATES_NBR'));
            end;
            if ctx_DataAccess.PayrollIsProcessing then
            begin
              ctx_DataAccess.RecalcLineBeforePost := False;
              try
                PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
                PR_CHECK_LINES.Edit;
                if not FieldByName('EE_STATES_NBR').IsNull then
                  PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').Value := FieldByName('EE_STATES_NBR').Value
                else
                  PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').Value := DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value;
                PR_CHECK_LINES.Post;
              finally
                ctx_DataAccess.RecalcLineBeforePost := True;
              end;
            end;
          end;
          Next;
        end;
      end;

  // Add SUIs that have tax overrides
      if (not IsImport) and (not ForTaxCalculator) then
      with PR_CHECK_SUI_2 do
      begin
        PR_CHECK_SUI_2.CloneCursor(PR_CHECK_SUI, True);
        Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
        Filtered := True;
        First;
        while not EOF do
        begin
          if (SUIList.IndexOf(FieldByName('CO_SUI_NBR').AsString) = -1) and
            (ConvertNull(PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value, 0) <> 0) then
          begin
            SUIList.Add(FieldByName('CO_SUI_NBR').AsString);
          end;
          Next;
        end;
      end;
  // List constructed

      with SUIList do
      for I := 0 to Count - 1 do
      begin
        if not PR_CHECK_SUI.Locate('PR_CHECK_NBR;CO_SUI_NBR', VarArrayOf([CheckNumber, StrToInt(Strings[I])]), []) then
        begin
          PR_CHECK_SUI.Insert;
          PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value := StrToInt(Strings[I]);
        end
        else
          PR_CHECK_SUI.Edit;
        if (not IsImport) or (IsImport and (PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').IsNull
        or PR_CHECK_SUI.FieldByName('SUI_TAX').IsNull)) then
        begin
          CalcSUITaxes(CheckNumber, PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, False {Employee SUI}, Tax, TaxWages, GrossWages,
            PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, DisableYTDs, ShortfallsDisabled, PR_CHECK);
          if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO)
          and not PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').IsNull then
          begin
            Tax := PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value;
            if (Tax > Net) and (Tax - Net >= 0.005) and not NegCheckAllowed then
              raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
          end;
        end;

        if not DM_HISTORICAL_TAXES.CO_SUI.Locate('CO_SUI_NBR', PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, []) then
          raise EInconsistentData.CreateHelp('Failed to find CO_SUI with NBR= '+ PR_CHECK_SUI.FieldByName('CO_SUI_NBR').AsString, IDH_InconsistentData);
        if not DM_HISTORICAL_TAXES.SY_SUI.Locate('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI['SY_SUI_NBR'], []) then
          raise EInconsistentData.CreateHelp('Failed to find SY_SUI with NBR= '+ DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').AsString, IDH_InconsistentData);
        if not Is_ER_SUI(DM_HISTORICAL_TAXES.SY_SUI.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString) then
        begin
          if (not IsImport) or IsImport and (PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').IsNull) then
          begin
            if (DM_EMPLOYEE.EE.FieldByName('COMPANY_OR_INDIVIDUAL_NAME').AsString = GROUP_BOX_COMPANY) and
            (DM_EMPLOYEE.EE_STATES.NewLookup('SUI_APPLY_CO_STATES_NBR', PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, 'CALCULATE_TAXABLE_WAGES_1099') = 'N') then
              PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value := 0
            else
              PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value := RoundTwo(TaxWages);
            PR_CHECK_SUI.FieldByName('SUI_GROSS_WAGES').Value := RoundTwo(GrossWages);
          end;
          if (not IsImport) or IsImport and (PR_CHECK_SUI.FieldByName('SUI_TAX').IsNull) then
            PR_CHECK_SUI.FieldByName('SUI_TAX').Value := Tax;

          TaxWages := ConvertNull(PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value, 0);
          Tax := ConvertNull(PR_CHECK_SUI.FieldByName('SUI_TAX').Value, 0);

          Tax := RoundTwo(Tax);
          if (Tax >= Net) and (not NegCheckAllowed) then
          begin
            Tax := Net;
            Net := 0;
          end
          else
            Net := Net - Tax;
          PR_CHECK_SUI.FieldByName('SUI_TAX').Value := Tax;
        end;
        if PR_CHECK_SUI.FieldByName('SUI_GROSS_WAGES').IsNull then
          PR_CHECK_SUI.FieldByName('SUI_GROSS_WAGES').Value := PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value;
        PR_CHECK_SUI.Post;
      end;

  // Clear all check SUIs that are not used on check lines
  //          if not IsImport then
      if not ForTaxCalculator then
      with PR_CHECK_SUI_2 do
      begin
        First;
        while not EOF do
        begin
          if (SUIList.IndexOf(FieldByName('CO_SUI_NBR').AsString) = -1) and
            (ConvertNull(PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value, 0) = 0) then
          begin
            PR_CHECK_SUI.Locate('PR_CHECK_SUI_NBR', FieldByName('PR_CHECK_SUI_NBR').Value, []);
            PR_CHECK_SUI.Delete;
          end;
          Next;
        end;
      end;
    finally
      SUIList.Free;
    end;
  end;

  procedure ProcessLocals;
  var
    LocalsList: IisParamsCollection;
    IsResidential: Boolean;
    i, j, N, k: Integer;
    Q: IevQuery;
    LocalFreq, CheckType: Char;
    PriorCheckDate, FollowingCheckDate: TDateTime;
    PriorPRLocals: IevQuery;
    CheckLineNbr, EELocalsNbr, CoLocalTaxNbr, NonResEeLocalTaxNbr, LocationNbr, SyLocalsNbr: Integer;
    CoLocalActive, SyLocalActive: Boolean;
    TaxType, LocationLevel: String;
    ResRate, NonResRate: Real;
    Locations, LocationsM: IevDataset;
    Div1, Bra1, Dep1, Tea1, Job1: Variant;
    Flag: Boolean;
    TaxAmount, TaxTotal: Double;
    Act32: Boolean;
    OverrideAmount: Double;
    PAOverride: IisParamsCollection;
    PAItem: IisListOfValues;
    Found: Boolean;
  begin
  
    PriorCheckDate := 0;
    FollowingCheckDate := 0;
    N := 0;

    // Construct a list of locals for the check
    LocalsList := TisParamsCollection.Create;
    if IsImport or ForTaxCalculator then
    begin
      PR_CHECK_LOCALS_2.Data := PR_CHECK_LOCALS.Data;
      PR_CHECK_LOCALS_2.Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
      PR_CHECK_LOCALS_2.Filtered := True;
      PR_CHECK_LOCALS_2.First;
      while not PR_CHECK_LOCALS_2.EOF do
      begin
        if LocalsList.IndexOf(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsString) = -1 then
          LocalsList.AddParams(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsString);
        PR_CHECK_LOCALS_2.Next;
      end;
    end
    else
    begin
      DM_HISTORICAL_TAXES.EE_LOCALS.First;
      while not DM_HISTORICAL_TAXES.EE_LOCALS.Eof do
      begin
        EeLocalsNbr   := DM_HISTORICAL_TAXES.EE_LOCALS['EE_LOCALS_NBR'];      
        if (LocalsList.IndexOf(IntToStr(EELocalsNbr)) = -1)
        and (DM_HISTORICAL_TAXES.EE_LOCALS['EE_NBR'] = PR_CHECK['EE_NBR'])
        and (DM_HISTORICAL_TAXES.EE_LOCALS['LOCAL_ENABLED'] = 'Y') then
        begin
          CoLocalTaxNbr := DM_HISTORICAL_TAXES.EE_LOCALS['CO_LOCAL_TAX_NBR'];
          SyLocalsNbr   := DM_COMPANY.CO_LOCAL_TAX.NewLookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr, 'SY_LOCALS_NBR');
          CoLocalActive := DM_HISTORICAL_TAXES.CO_LOCAL_TAX.NewLookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr, 'LOCAL_ACTIVE') = 'Y';
          SyLocalActive := DM_HISTORICAL_TAXES.SY_LOCALS.NewLookup('SY_LOCALS_NBR', SyLocalsNbr, 'LOCAL_ACTIVE') = 'Y';

          if CoLocalActive and SyLocalActive then
          begin
            LocalFreq := VarToStr(DM_HISTORICAL_TAXES.SY_LOCALS.NewLookup('SY_LOCALS_NBR', SyLocalsNbr, 'TAX_FREQUENCY'))[1];
            CheckType := PR_CHECK.FieldByName('CHECK_TYPE').AsString[1];
            if (LocalFreq <> SCHED_FREQ_DAILY) and not (CheckType in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD]) then
            begin
              if LocalFreq = SCHED_FREQ_SCHEDULED_PAY then
              begin
                if PR['SCHEDULED'] = 'N' then
                begin
                  DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                  Continue;
                end;
              end
              else
              begin
                if PriorCheckDate = 0 then
                begin
                  Q := TevQuery.Create('MaxCheckDateForCo');
                  Q.Params.AddValue('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
                  Q.Params.AddValue('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
                  PriorCheckDate := ConvertNull(Q.Result.FieldByName('MAX_CHECK_DATE').Value, 0);
                end;
                if LocalFreq = SCHED_FREQ_EVRY_OTHER_PAY then
                begin
                  if N = 0 then
                  begin
                    Q := TevQuery.Create('PrNbrForMaxCheckDateForCo');
                    Q.Params.AddValue('CheckDate', PriorCheckDate);
                    Q.Params.AddValue('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
                    N := ConvertNull(Q.Result.FieldByName('PR_NBR').Value, 0);
                    PriorPRLocals := TevQuery.Create('SelectDistinctLocalsForPR');
                    PriorPRLocals.Params.AddValue('Pr_Nbr', N);
                  end;
                  if PriorPRLocals.Result.Locate('EE_LOCALS_NBR', EeLocalsNbr, []) then
                  begin
                    DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                    Continue;
                  end;
                end
                else
                begin
                  if FollowingCheckDate = 0 then
                    FollowingCheckDate := ctx_PayrollCalculation.GetNextCheckDate(PR['CHECK_DATE'], DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString[1]);

                  if LocalFreq = SCHED_FREQ_FIRST_SCHED_PAY then
                  begin
                    if PriorCheckDate <> 0 then
                    begin
                      if GetMonth(PR['CHECK_DATE']) = GetMonth(PriorCheckDate) then
                      begin
                        DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                        Continue;
                      end;
                    end;
                  end
                  else
                  if LocalFreq = SCHED_FREQ_LAST_SCHED_PAY then
                  begin
                    if GetMonth(PR['CHECK_DATE']) = GetMonth(FollowingCheckDate) then
                    begin
                      DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                      Continue;
                    end;
                  end
                  else
                  if LocalFreq = SCHED_FREQ_MID_SCHED_PAY then
                  begin
                    if (Abs(GetDay(PR['CHECK_DATE']) - 15) >= Abs(GetDay(FollowingCheckDate) - 15))
                    or (Abs(GetDay(PR['CHECK_DATE']) - 15) > Abs(GetDay(PriorCheckDate) - 15)) then
                    begin
                      DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                      Continue;
                    end;
                  end
                  else
                  if LocalFreq = SCHED_FREQ_QUARTERLY then
                  begin
                    if GetQuarterNumber(PR['CHECK_DATE']) = GetQuarterNumber(FollowingCheckDate) then
                    begin
                      DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                      Continue;
                    end;
                  end
                  else
                  if LocalFreq = SCHED_FREQ_ANNUALLY then
                  begin
                    if GetYear(PR['CHECK_DATE']) = GetYear(FollowingCheckDate) then
                    begin
                      DM_HISTORICAL_TAXES.EE_LOCALS.Next;
                      Continue;
                    end;
                  end;
                end;
              end;
            end;
            LocalsList.AddParams(IntToStr(EeLocalsNbr));
          end;
        end;
        DM_HISTORICAL_TAXES.EE_LOCALS.Next;
      end;
    end;

    if PR_CHECK['CALCULATE_OVERRIDE_TAXES'] = GROUP_BOX_NO then
    begin
      PR_CHECK_LOCALS_2.Data := PR_CHECK_LOCALS.Data;
      PR_CHECK_LOCALS_2.Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
      PR_CHECK_LOCALS_2.Filtered := True;
      PR_CHECK_LOCALS_2.First;
      while not PR_CHECK_LOCALS_2.EOF do
      begin
        if not PR_CHECK_LOCALS_2.FieldByName('OVERRIDE_AMOUNT').IsNull
        and (LocalsList.IndexOf(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsString) = -1) then
          LocalsList.AddParams(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsString);
        PR_CHECK_LOCALS_2.Next;
      end;
    end;

    PR_CHECK_LOCALS_2.Data := PR_CHECK_LOCALS.Data;
    PR_CHECK_LOCALS_2.Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
    PR_CHECK_LOCALS_2.Filtered := True;
    PR_CHECK_LOCALS_2.First;

    PAOverride := TisParamsCollection.Create;
    PR_CHECK_LOCALS_2.First;
    while not PR_CHECK_LOCALS_2.EOF do
    begin
      if not PR_CHECK_LOCALS_2.FieldByName('OVERRIDE_AMOUNT').IsNull
      and (HT.IsAct32(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsInteger)
           or HT.IsPhiladelphia(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsInteger)) then
      begin
        PAItem := PAOverride.AddParams;
        PAItem.AddValue('EE_LOCALS_NBR', PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').Value);
        PAItem.AddValue('CO_LOCATIONS_NBR', PR_CHECK_LOCALS_2.FieldByName('CO_LOCATIONS_NBR').Value);
        PAItem.AddValue('NONRES_EE_LOCALS_NBR', PR_CHECK_LOCALS_2.FieldByName('NONRES_EE_LOCALS_NBR').Value);
        PAItem.AddValue('REPORTABLE', PR_CHECK_LOCALS_2.FieldByName('REPORTABLE').Value);
        PAItem.AddValue('OVERRIDE_AMOUNT', PR_CHECK_LOCALS_2.FieldByName('OVERRIDE_AMOUNT').Value);
      end;
      PR_CHECK_LOCALS_2.Next;
    end;

    // List constructed

    LocationsM := ctx_DataAccess.PrCheckLineLocationsM;
    LocationsM.IndexFieldNames := 'PR_CHECK_NBR';
    LocationsM.SetRange([CheckNumber], [CheckNumber]);
    while not LocationsM.Eof do
      LocationsM.Delete;
    LocationsM.CancelRange;
    LocationsM.First;
    LocationsM.IndexFieldNames := 'PR_CHECK_LINES_NBR';

    Locations := ctx_DataAccess.PrCheckLineLocations;
    Locations.IndexFieldNames := 'PR_CHECK_NBR';
    Locations.SetRange([CheckNumber], [CheckNumber]);
    while not Locations.Eof do
      Locations.Delete;
    Locations.CancelRange;
    Locations.First;
    Locations.IndexFieldNames := 'PR_CHECK_LINES_NBR';

    for i := 0 to LocalsList.Count - 1 do
    begin
      EELocalsNbr   := StrToInt(LocalsList.ParamName(i));
      CoLocalTaxNbr := DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalsNbr, 'CO_LOCAL_TAX_NBR');
      SyLocalsNbr   := DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr, 'SY_LOCALS_NBR');
      TaxType       := DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'TAX_TYPE');

      if (TaxType = GROUP_BOX_EE) then
        PreparePaTax(CheckNumber, CoLocalTaxNbr, PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, DisableYTDs);
    end;

    Locations.CancelRange;
    Locations.First;
    while not Locations.Eof do
    begin
      if Abs(Locations.FieldByName('AMOUNT').AsFloat) < 0.005 then
        Locations.Delete
      else
      begin
        Locations.Edit;
        if Locations['DBDT_ATTACHED'] = 'Y' then
          Locations['DBDT_ATTACHED'] := '0'
        else
          Locations['DBDT_ATTACHED'] := '1';
        Locations.Post;
        Locations.Next;
      end;
    end;

    PR_CHECK_LINES.First;
    while not PR_CHECK_LINES.Eof do
    begin
      Locations.IndexFieldNames := 'PR_CHECK_LINES_NBR;DBDT_ATTACHED';
      CheckLineNbr := PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
      Locations.SetRange([CheckLineNbr], [CheckLineNbr]);
      Locations.First;
      if not Locations.Eof then
      begin
        while not Locations.Eof do
        begin
          if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR',
            VarArrayOf([CheckLineNbr, Locations['EE_LOCALS_NBR']]), []) then
          begin
            if PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] = GROUP_BOX_EXEMPT then
              Locations.Delete
            else
            if PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] = GROUP_BOX_EXCLUDE then
            begin
              Locations.Edit;
              Locations['BLOCKED'] := 'Y';
              Locations.Post;
            end;
          end;
          Locations.Next;
        end;
        Locations.First;
        NonResEeLocalTaxNbr := 0;
        LocationNbr := 0;
        if not Locations.Eof then
        begin
          // looking for the non res tax
          if PR_CHECK_LINES['WORK_ADDRESS_OVR'] = 'Y' then
          begin
            while not Locations.Eof do
            begin
              if (Locations['ISRES'] = 'N') and (Locations['BLOCKED'] <> 'Y') and (Locations['WORK_ADDRESS_OVR']='Y') then
              begin
                NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
                if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                  raise Exception.Create('Locations are not set up correctly.');
                LocationNbr := Locations['CO_LOCATIONS_NBR'];
                Locations.Edit;
                Locations['LOCATION_LEVEL'] := 'E';
                Locations.Post;
                break;
              end;
              Locations.Next;
            end;
          end;
          Locations.First;
          if NonResEeLocalTaxNbr = 0 then
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'N') and (Locations['LOCATION_LEVEL'] = '5')
            and (Locations['WORK_ADDRESS_OVR']='N') then
            begin
              NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
              if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                raise Exception.Create('Locations are not set up correctly.');
              LocationNbr := Locations['CO_LOCATIONS_NBR'];
              break;
            end;
            Locations.Next;
          end;
          Locations.First;
          if NonResEeLocalTaxNbr = 0 then
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'N') and (Locations['LOCATION_LEVEL'] = '4')
            and (Locations['WORK_ADDRESS_OVR']='N') then
            begin
              NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
              if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                raise Exception.Create('Locations are not set up correctly.');
              LocationNbr := Locations['CO_LOCATIONS_NBR'];
              break;
            end;
            Locations.Next;
          end;
          Locations.First;
          if NonResEeLocalTaxNbr = 0 then
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'N') and (Locations['LOCATION_LEVEL'] = '3')
            and (Locations['WORK_ADDRESS_OVR']='N') then
            begin
              NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
              if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                raise Exception.Create('Locations are not set up correctly.');
              LocationNbr := Locations['CO_LOCATIONS_NBR'];
              break;
            end;
            Locations.Next;
          end;
          Locations.First;
          if NonResEeLocalTaxNbr = 0 then
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'N') and (Locations['LOCATION_LEVEL'] = '2')
            and (Locations['WORK_ADDRESS_OVR']='N') then
            begin
              NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
              if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                raise Exception.Create('Locations are not set up correctly.');
              LocationNbr := Locations['CO_LOCATIONS_NBR'];
              break;
            end;
            Locations.Next;
          end;
          Locations.First;
          if NonResEeLocalTaxNbr = 0 then
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'N') and (Locations['LOCATION_LEVEL'] = '1')
            and (Locations['WORK_ADDRESS_OVR']='N') then
            begin
              NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
              if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                raise Exception.Create('Locations are not set up correctly.');
              LocationNbr := Locations['CO_LOCATIONS_NBR'];
              break;
            end;
            Locations.Next;
          end;
          Locations.First;
          if NonResEeLocalTaxNbr = 0 then
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'N') and (Locations['LOCATION_LEVEL'] = 'C')
            and (Locations['WORK_ADDRESS_OVR']='N') then
            begin
              NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
              if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull
              and (DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', Locations['SY_LOCALS_NBR'], 'COMBINE_FOR_TAX_PAYMENTS') = 'Y') then
                raise Exception.Create('Locations are not set up correctly.');
              LocationNbr := Locations.FieldByName('CO_LOCATIONS_NBR').AsInteger;
              break;
            end;
            Locations.Next;
          end;
          // otherwise stop if non res with the work address populated
          if NonResEeLocalTaxNbr = 0 then
          begin
            Locations.First;
            while not Locations.Eof do
            begin
              if (Locations['ISRES'] = 'N') and not ((PR_CHECK_LINES['WORK_ADDRESS_OVR'] = 'N') and (Locations['WORK_ADDRESS_OVR'] = 'N'))
              and (DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', Locations['SY_LOCALS_NBR'], 'COMBINE_FOR_TAX_PAYMENTS') = 'Y') then
              begin
                NonResEeLocalTaxNbr := Locations['EE_LOCALS_NBR'];
                if Locations.FieldByName('CO_LOCATIONS_NBR').IsNull then
                  raise Exception.Create('Locations are not set up correctly.');
                LocationNbr := Locations['CO_LOCATIONS_NBR'];
                DM_HISTORICAL_TAXES.EE_LOCALS.Locate('EE_LOCALS_NBR', NonResEeLocalTaxNbr, []);
                if DM_HISTORICAL_TAXES.EE_LOCALS.FieldByName('WORK_ADDRESS1').AsString <> '' then
                begin
                  Locations.Edit;
                  Locations['LOCATION_LEVEL'] := 'E';
                  Locations.Post;
                  break;
                end
              end;
              Locations.Next;
            end;
          end;
          LocationLevel := 'C';
          Flag := False;
          if NonResEeLocalTaxNbr <> 0 then
          begin
            Locations.First;
            while not Locations.Eof do
            begin
              if Locations['EE_LOCALS_NBR'] = NonResEeLocalTaxNbr then
              begin
                LocationLevel := Locations['LOCATION_LEVEL'];
                Div1 := Locations['DIVISION_NBR'];
                Bra1 := Locations['BRANCH_NBR'];
                Dep1 := Locations['DEPARTMENT_NBR'];
                Tea1 := Locations['TEAM_NBR'];
                Job1 := Locations['JOBS_NBR'];
                Flag := True;
                break;
              end;
              Locations.Next;
            end;
          end;
          if Flag then
          begin
            Locations.First;
            while not Locations.Eof do
            begin
              Locations.Edit;
              Locations['LOCATION_LEVEL'] := LocationLevel;
              Locations['DIVISION_NBR'] := Div1;
              Locations['BRANCH_NBR'] := Bra1;
              Locations['DEPARTMENT_NBR'] := Dep1;
              Locations['TEAM_NBR'] := Tea1;
              Locations['JOBS_NBR'] := Job1;
              Locations.Post;
              Locations.Next;
            end;
          end;
          // found non res that should be calculated, other non res marked as excluded
          Locations.First;
          while not Locations.Eof do
          begin
            Locations.Edit;
            Locations['NONRES_EE_LOCALS_NBR'] := NonResEeLocalTaxNbr;
            Locations['CO_LOCATIONS_NBR'] := LocationNbr;
            if (Locations['ISRES'] = 'N') and (Locations['EE_LOCALS_NBR'] <> NonResEeLocalTaxNbr) then
              Locations['EXCLUDED'] := 'Y';
            Locations.Post;
            Locations.Next;
          end;
          // calculating Non Res and Res tax rates
          ResRate := 0;
          NonResRate := 0;
          Locations.First;
          while not Locations.Eof do
          begin
            if (Locations['ISRES'] = 'Y') then
              ResRate := ResRate + Locations['RATE']
            else
            if Locations['EXCLUDED'] <> 'Y' then
              NonResRate := NonResRate + Locations['RATE'];
            Locations.Next;
          end;
          IsResidential := not DoubleCompare(NonResRate, coGreater, ResRate);
          Locations.First;
          while not Locations.Eof do
          begin
            if IsResidential <> (Locations['ISRES'] = 'Y') then
            begin
              Locations.Edit;
              Locations['ISLOSER'] := 'Y';
              Locations.Post;
              if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR',
                VarArrayOf([Locations['PR_CHECK_LINES_NBR'], Locations['EE_LOCALS_NBR']]), []) then
              begin
                if PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString <> GROUP_BOX_EXEMPT then
                begin
                  PR_CHECK_LINE_LOCALS.Edit;
                  PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_PABLOCK;
                  PR_CHECK_LINE_LOCALS.Post;
                end;  
              end
              else
              begin
                PR_CHECK_LINE_LOCALS.Insert;
                PR_CHECK_LINE_LOCALS.FieldByName('PR_CHECK_LINES_NBR').Value := Locations['PR_CHECK_LINES_NBR'];
                PR_CHECK_LINE_LOCALS.FieldByName('EE_LOCALS_NBR').Value := Locations['EE_LOCALS_NBR'];
                PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_PABLOCK;
                PR_CHECK_LINE_LOCALS.Post;
              end;
            end
            else
            begin
              if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR',
                VarArrayOf([Locations['PR_CHECK_LINES_NBR'], Locations['EE_LOCALS_NBR']]), []) then
              begin
                if PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString = GROUP_BOX_PABLOCK then
                begin
                  PR_CHECK_LINE_LOCALS.Delete;
                end;
              end;
            end;
            Locations.Next;
          end;
          LocationsM.SetRange([CheckLineNbr], [CheckLineNbr]);
          LocationsM.First;

          LocationsM.First;
          while not LocationsM.Eof do
          begin
            if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR',
                VarArrayOf([LocationsM['PR_CHECK_LINES_NBR'], LocationsM['EE_LOCALS_NBR']]), []) then
            begin
              if PR_CHECK_LINE_LOCALS.FieldByName('EXEMPT_EXCLUDE').AsString = GROUP_BOX_INCLUDE then
              begin
                Locations.Insert;
                for k := 0 to Locations.Fields.Count - 1 do
                  Locations.Fields[k].Value := LocationsM.Fields[k].Value;
                Locations['NONRES_EE_LOCALS_NBR'] := NonResEeLocalTaxNbr;
                Locations['CALCULATE'] := 'Y';
                Locations.Post;
              end;
            end;
            LocationsM.Next;
          end;

          if NonResEeLocalTaxNbr = 0 then
          begin
            LocationsM.First;
            while not LocationsM.Eof do
            begin
              if LocationsM['ISRES'] = 'N' then
              begin
                NonResEeLocalTaxNbr := LocationsM['NONRES_EE_LOCALS_NBR'];
                DM_HISTORICAL_TAXES.EE_LOCALS.Locate('EE_LOCALS_NBR', NonResEeLocalTaxNbr, []);
                if DM_HISTORICAL_TAXES.EE_LOCALS.FieldByName('WORK_ADDRESS1').AsString <> '' then
                  break;
              end;
              LocationsM.Next;
            end;
            if NonResEeLocalTaxNbr <> 0 then
            begin
              Locations.First;
              while not Locations.Eof do
              begin
                Locations.Edit;
                Locations['NONRES_EE_LOCALS_NBR'] := NonResEeLocalTaxNbr;
                Locations.Post;
                Locations.Next;
              end;
            end;
          end;
        end;
      end;
      PR_CHECK_LINES.Next;
    end;
    Locations.CancelRange;
    Locations.First;

    Locations.First;
    while not Locations.Eof do
    begin
      Locations.Edit;

      if PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR',
        VarArrayOf([Locations['PR_CHECK_LINES_NBR'], Locations['EE_LOCALS_NBR']]), [])
          and (PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] = GROUP_BOX_EXEMPT) then
      begin
        Locations['LOCAL_GROSS_WAGES'] := 0;
        Locations['LOCAL_TAXABLE_WAGES'] := 0;
      end
      else
      begin
        Locations['LOCAL_GROSS_WAGES'] := Locations['AMOUNT'];
        Locations['LOCAL_TAXABLE_WAGES'] := Locations['LOCAL_TAXABLE_WAGES'];
      end;

      Locations['LOCAL_TAX'] := 0;

      if (Locations['EXCLUDED'] = 'Y') or (Locations['BLOCKED'] = 'Y') or (Locations['ISLOSER'] = 'Y') then
      begin
        Locations['AMOUNT'] := 0;
        Locations['CALCULATE'] := 'N';
      end
      else
        Locations['CALCULATE'] := 'Y';

      if Locations['NONRES_EE_LOCALS_NBR'] <> 0 then
      begin
        if DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', Locations['SY_LOCALS_NBR'], 'COMBINE_FOR_TAX_PAYMENTS') = 'N' then
          Locations['NONRES_EE_LOCALS_NBR'] := Locations['EE_LOCALS_NBR'];
      end;

      Locations.Post;
      Locations.Next;
    end;

    for i := 0 to LocalsList.Count - 1 do
    begin
      EELocalsNbr := StrToInt(LocalsList.ParamName(i));
      CoLocalTaxNbr := DM_HISTORICAL_TAXES.EE_LOCALS.Lookup('EE_LOCALS_NBR', EELocalsNbr, 'CO_LOCAL_TAX_NBR');
      SyLocalsNbr := DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr, 'SY_LOCALS_NBR');

      if DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr,'TAX_TYPE') = GROUP_BOX_EE then
      begin

        if (not IsImport) or (IsImport and (PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').IsNull
        or PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').IsNull)) then
        begin
          if (HT.IsAct32(EELocalsNbr) or HT.IsPhiladelphia(EELocalsNbr)) then
          begin
            PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []);
            while not PR_CHECK_LOCALS.Eof
            and (PR_CHECK_LOCALS['PR_CHECK_NBR'] = CheckNumber)
            and (PR_CHECK_LOCALS['EE_LOCALS_NBR'] = EELocalsNbr) do
            begin
              PR_CHECK_LOCALS.Edit;
              PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Clear;
              PR_CHECK_LOCALS.Post;
              PR_CHECK_LOCALS.Next;
            end

          end;
          PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []);

          CalcEmployeeLocalTax(CheckNumber, CoLocalTaxNbr, TaxRate, Tax, TaxWages, GrossWages,
            PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS, ForTaxCalculator, DisableYTDs, ShortfallsDisabled);

          PR_CHECK_LOCALS.IndexFieldNames := 'PR_CHECK_NBR;EE_LOCALS_NBR';
          PR_CHECK_LOCALS.First;
          PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []);

          OverrideAmount := 0;
          
          if not (HT.IsAct32(EELocalsNbr) or HT.IsPhiladelphia(EELocalsNbr)) then
          begin
            while not PR_CHECK_LOCALS.Eof
            and (PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger=CheckNumber)
            and (PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger=EELocalsNbr) do
            begin
              OverrideAmount := OverrideAmount + PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsFloat;
              PR_CHECK_LOCALS.Next;
            end;

            if Abs(OverrideAmount) > 0.005 then
              Tax := OverrideAmount;

            if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO)
            and (Abs(OverrideAmount) > 0.005) then
            begin
              Tax := OverrideAmount;

              if (Tax > Net) and (Tax - Net < 0.005) and not NegCheckAllowed then
                raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
            end;

          end;


        end;

        if (HT.IsAct32(EELocalsNbr) or HT.IsPhiladelphia(EELocalsNbr)) then
        begin
          while PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []) do
            PR_CHECK_LOCALS.Delete;

          Locations.IndexFieldNames := 'PR_CHECK_NBR;EE_LOCALS_NBR;CALCULATE;LOCAL_TAXABLE_WAGES';
          Locations.SetRange([CheckNumber, EELocalsNbr, 'Y'], [CheckNumber, EELocalsNbr, 'Y']);
          Locations.First;

          TaxWages := 0;

          while not Locations.Eof do
          begin
            if HT.PercentageOfLocalTaxWages(Locations['EE_LOCALS_NBR']) <> 0 then
            begin
              Locations.Edit;
              Locations['LOCAL_TAXABLE_WAGES'] := RoundTwo(Locations['LOCAL_TAXABLE_WAGES'] * HT.PercentageOfLocalTaxWages(Locations['EE_LOCALS_NBR']) * 0.01);
              Locations.Post;
            end;

            TaxWages := TaxWages + Locations.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat;
            Locations.Next;
          end;

          Locations.IndexFieldNames := 'PR_CHECK_NBR;EE_LOCALS_NBR;CALCULATE;LOCAL_TAXABLE_WAGES';
          Locations.SetRange([CheckNumber, EELocalsNbr, 'Y'], [CheckNumber, EELocalsNbr, 'Y']);
          Locations.First;
          if not Locations.Eof then
          begin
            TaxTotal := Tax;
            if Tax > 0.005 then
            begin
              Locations.First;
              while not Locations.Eof do
              begin
                TaxAmount := RoundTwo(Tax * Locations.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat / TaxWages);
                if TaxAmount > TaxTotal then
                  TaxAmount := TaxTotal;
                TaxTotal := TaxTotal - TaxAmount;
                Locations.Edit;
                Locations['LOCAL_TAX'] := TaxAmount;
                Locations.Post;
                Locations.Next;
              end;
            end
            else if Tax < -0.005 then
            begin
              Locations.First;
              while not Locations.Eof do
              begin
                TaxAmount := RoundTwo(Tax * Locations.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat / TaxWages);
                if TaxAmount < TaxTotal then
                  TaxAmount := TaxTotal;
                TaxTotal := TaxTotal - TaxAmount;
                Locations.Edit;
                Locations['LOCAL_TAX'] := TaxAmount;
                Locations.Post;
                Locations.Next;
              end;
            end;
          end;

          Locations.SetRange([CheckNumber, EELocalsNbr], [CheckNumber, EELocalsNbr]);
          Locations.First;

          for j := 0 to PAOverride.Count - 1 do
          begin
            PAItem := PAOverride[j];
            Locations.First;

            while not Locations.Eof do
            begin
              Locations.Edit;
              Locations.FieldByName('OVERRIDE_AMOUNT').Clear;
              Locations.Post;
              Locations.Next;
            end;

          end;

          for j := 0 to PAOverride.Count - 1 do
          begin
            PAItem := PAOverride[j];
            Locations.First;
            Found := False;
            if PAItem.Value['EE_LOCALS_NBR'] = EELocalsNbr then
            begin
              while not Locations.Eof do
              begin
                if Found then
                begin
                  Locations.Edit;
                  Locations.FieldByName('OVERRIDE_AMOUNT').Clear;
                  Locations.FieldByName('LOCAL_TAX').Value := 0;
                  Locations.Post;
                end
                else
                begin
                  Found := (ConvertNull(Locations['CO_LOCATIONS_NBR'], 0) = ConvertNull(PAItem.Value['CO_LOCATIONS_NBR'], 0))
                    and (ConvertNull(Locations['NONRES_EE_LOCALS_NBR'], 0) = ConvertNull(PAItem.Value['NONRES_EE_LOCALS_NBR'], 0))
                    and (ConvertNull(Locations['ISLOSER'], '') <> ConvertNull(PAItem.Value['REPORTABLE'], ''));

                  if Found then
                  begin
                    Locations.Edit;
                    Locations.FieldByName('OVERRIDE_AMOUNT').Value := PAItem.Value['OVERRIDE_AMOUNT'];
                    Locations.FieldByName('LOCAL_TAX').Value := PAItem.Value['OVERRIDE_AMOUNT'];
                    Locations.Post;
                  end;
                end;
                Locations.Next;
              end;

              if not Found then
              begin
                Locations.Insert;
                Locations['PR_CHECK_NBR'] := CheckNumber;
                Locations['EE_LOCALS_NBR'] := PAItem.Value['EE_LOCALS_NBR'];
                Locations['LOCAL_TAX'] := PAItem.Value['OVERRIDE_AMOUNT'];
                Locations['OVERRIDE_AMOUNT'] := PAItem.Value['OVERRIDE_AMOUNT'];
                Locations.FieldByName('LOCAL_TAXABLE_WAGES').Clear;
                Locations.FieldByName('LOCAL_GROSS_WAGES').Clear;
                if PAItem.Value['REPORTABLE'] = 'Y' then
                  Locations['ISLOSER'] := 'N'
                else
                  Locations['ISLOSER'] := 'Y';
                Locations['CO_LOCATIONS_NBR'] := PAItem.Value['CO_LOCATIONS_NBR'];
                Locations['NONRES_EE_LOCALS_NBR'] := PAItem.Value['NONRES_EE_LOCALS_NBR'];
                Locations.Post;
              end;
              
            end;
          end;

          Locations.First;
          while not Locations.Eof do
          begin

            if Locations['EXCLUDED'] = 'N' then
            begin
              Tax := RoundTwo(Locations.FieldByName('LOCAL_TAX').AsFloat);
              if (Tax > Net) and (not NegCheckAllowed) then
              begin
                ShortFall := Tax - Net;
                Tax := Net;
                Net := 0;
                Result := True;
              end
              else
                Net := Net - Tax;

              PR_CHECK_LOCALS.Insert;
              PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := CheckNumber;
              PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := EELocalsNbr;
              PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
              PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := Tax;
              PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := Locations['LOCAL_TAXABLE_WAGES'];
              PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').Value := Locations['LOCAL_GROSS_WAGES'];
              if Locations.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger <> 0 then
                PR_CHECK_LOCALS.FieldByName('NONRES_EE_LOCALS_NBR').Value := Locations['NONRES_EE_LOCALS_NBR'];

              if Locations.FieldByName('ISLOSER').AsString = 'Y' then
                PR_CHECK_LOCALS.FieldByName('REPORTABLE').AsString := 'N'
              else
                PR_CHECK_LOCALS.FieldByName('REPORTABLE').AsString := 'Y';

              if Locations['WORK_ADDRESS_OVR'] = 'Y' then
                PR_CHECK_LOCALS.FieldByName('CO_LOCATIONS_NBR').Value := DM_EMPLOYEE.EE_LOCALS.Lookup('EE_LOCALS_NBR', Locations['NONRES_EE_LOCALS_NBR'], 'CO_LOCATIONS_NBR')
              else
              if Locations.FieldByName('CO_LOCATIONS_NBR').AsInteger <> 0 then
                PR_CHECK_LOCALS.FieldByName('CO_LOCATIONS_NBR').Value := Locations['CO_LOCATIONS_NBR'];

              PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value := Locations['OVERRIDE_AMOUNT'];

              PR_CHECK_LOCALS.Post;
            end;
            Locations.Next;
          end;
        end
        else
        begin

          if not PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []) then
          begin
            PR_CHECK_LOCALS.Insert;
            PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := CheckNumber;
            PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := EELocalsNbr;
            PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
          end
          else
            PR_CHECK_LOCALS.Edit;

          if (not IsImport) or IsImport and (PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').IsNull) then
          begin
            PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := RoundTwo(TaxWages);
            PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').Value := RoundTwo(GrossWages);
          end;
          if (not IsImport) or IsImport and (PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').IsNull) then
            PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := Tax;

          TaxWages := ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value, 0);
          Tax := ConvertNull(PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value, 0);

          ShortFall := 0;
          Tax := RoundTwo(Tax);
          if (Tax > Net) and (not NegCheckAllowed) then
          begin
            ShortFall := Tax - Net;
            Tax := Net;
            Net := 0;
            Result := True;
          end
          else
            Net := Net - Tax;
          PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := Tax;

          PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value := ShortFall;
          if PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').IsNull then
            PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').Value := PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value;
          PR_CHECK_LOCALS.Post;

        end;
      end;

      if not PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []) then
      begin
        PR_CHECK_LOCALS.Insert;
        PR_CHECK_LOCALS['PR_CHECK_NBR'] := CheckNumber;
        PR_CHECK_LOCALS['EE_LOCALS_NBR'] := EELocalsNbr;
        PR_CHECK_LOCALS['EXCLUDE_LOCAL'] := 'N';

        PR_CHECK_LOCALS['LOCAL_TAXABLE_WAGE'] := 0;
        PR_CHECK_LOCALS['LOCAL_GROSS_WAGES'] := 0;
        PR_CHECK_LOCALS['LOCAL_TAX'] := 0;

        Locations.First;
        if Locations.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([CheckNumber, EELocalsNbr]), []) then
        begin
          PR_CHECK_LOCALS['LOCAL_TAX'] := Locations['OVERRIDE_AMOUNT'];
          PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := Locations['OVERRIDE_AMOUNT'];
          PR_CHECK_LOCALS['CO_LOCATIONS_NBR'] := Locations['CO_LOCATIONS_NBR'];
          PR_CHECK_LOCALS['NONRES_EE_LOCALS_NBR'] := Locations['NONRES_EE_LOCALS_NBR'];
          if Locations['ISLOSER'] = 'Y' then
            PR_CHECK_LOCALS['REPORTABLE'] := 'N'
          else
            PR_CHECK_LOCALS['REPORTABLE'] := 'Y';
        end;

        PR_CHECK_LOCALS.Post;
      end

    end;

  // Clear all check Locals that are not used on check lines
    if (not IsImport) and (not ForTaxCalculator) then
    begin
      PR_CHECK_LOCALS_2.Data := PR_CHECK_LOCALS.Data;
      PR_CHECK_LOCALS_2.Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
      PR_CHECK_LOCALS_2.Filtered := True;
      PR_CHECK_LOCALS_2.First;
      while not PR_CHECK_LOCALS_2.Eof do
      begin
        if (LocalsList.IndexOf(PR_CHECK_LOCALS_2.FieldByName('EE_LOCALS_NBR').AsString) = -1) and
          (ConvertNull(PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value, 0) = 0) then
        begin
          PR_CHECK_LOCALS.Locate('PR_CHECK_LOCALS_NBR', PR_CHECK_LOCALS_2.FieldByName('PR_CHECK_LOCALS_NBR').Value, []);
          PR_CHECK_LOCALS.Delete;
        end;
        PR_CHECK_LOCALS_2.Next;
      end;
    end;

  end;

  procedure SubtractDeductions(BeforeTaxes: Boolean);
  begin
    if BeforeTaxes and not HasTips then
      Exit;
    CUSTOM_VIEW_2.First;
    with CUSTOM_VIEW_2 do
    while not EOF do
    begin
      EDCodeType := FieldByName('E_D_CODE_TYPE').Value;
      if ((EDCodeType[1] = 'D') and PermitType(EDCodeType) and (EDCodeType <> ED_DED_LEVY)
      and not(((FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER) or (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_USER)) and (FieldByName('AMOUNT').Value < 0))
      and (EDCodeType <> ED_DIRECT_DEPOSIT) and (not TypeIsTaxableMemo(EDCodeType)))
      and (FieldByName('LINE_TYPE').Value <> CHECK_LINE_TYPE_SHORTFALL)
      and (not HasTips or (BeforeTaxes and (FieldByName('TAXED_NORMALLY').Value = 'B'))
      or (not BeforeTaxes and (FieldByName('TAXED_NORMALLY').Value <> 'B')))
      or (not BeforeTaxes and (EDCodeType[1] = 'M')) then
      begin
        if EDCodeType = ED_DED_CHILD_SUPPORT then
          DeductChildSupport(BeforeTaxes)
        else
        begin
          PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
          if (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) and (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
          begin
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
            PR_CHECK_LINES.Edit;
            if ctx_PayrollCalculation.ScheduledEDRecalc(EDCodeType, Gross, Net, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
            PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES,
            DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE, DM_HISTORICAL_TAXES.SY_STATES, JustPreProcessing) then
              PR_CHECK_LINES.Post;
          end;
          if (EDCodeType[1] <> 'M' {memo}) then
            AdjustDeduction(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value);
          if TypeIsSpecTaxedDed(EDCodeType)
          and SpecTaxedDeds.Recalculated(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value, ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0)) then
            Recalculate := True;
        end;
      end;
      Next;
    end;
  end;

  procedure ZeroOutScheduledEDs(BeforeTaxes: Boolean);
  var
    ApplyDDBeforeTaxes: String;
  begin
    if BeforeTaxes and not HasTips then
      Exit;
    ctx_DataAccess.RecalcLineBeforePost := False;
    try
      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.Eof do
      begin
        EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
        ApplyDDBeforeTaxes := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'APPLY_BEFORE_TAXES');
        if (EDCodeType[1] = 'D') and (not TypeIsTaxableMemo(EDCodeType))
        and (PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_SCHEDULED) and (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull)
        and ((BeforeTaxes and (ApplyDDBeforeTaxes = 'Y')) or (not BeforeTaxes and (ApplyDDBeforeTaxes = 'N'))) then
        begin
          PR_CHECK_LINES.Edit;
          PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
          if not PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').IsNull then
            PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value := Null;
          PR_CHECK_LINES.Post;
        end
        else
        if not PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').IsNull and (PR_CHECK_LINES['LINE_TYPE'] <> CHECK_LINE_TYPE_SHORTFALL) then
        begin
          PR_CHECK_LINES.Edit;
          PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value := Null;
          PR_CHECK_LINES.Post;
        end;
        PR_CHECK_LINES.Next;
      end;
    finally
      ctx_DataAccess.RecalcLineBeforePost := True;
    end;
  end;

var
  Q: IevQuery;
  CoStatesNbr, EeSuiStatesNbr: Integer;  
begin
  Result := False;
  YTDsDisabled := DisableYTDs;
  ShortfallsDisabled := DisableShortfalls;
  TaxCalculator := ForTaxCalculator;
  FSecondCheck := SecondCheck;
  DirectDeposit := 0;

  if FetchPayrollData then
  begin
    PR.Activate;
    if not ctx_DataAccess.PayrollIsProcessing then
    begin
      if PR_CHECK.Active and PR_CHECK.Locate('PR_CHECK_NBR', CheckNumber, []) then
      else
        PR_CHECK.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckNumber));

      if PR_CHECK_LINES.Active and (PR_CHECK_LINES.OpenCondition = 'PR_NBR = ' + PR_CHECK.FieldByName('PR_NBR').AsString) then
      else
        PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckNumber));
    end;
  end;

  if not ctx_DataAccess.PayrollIsProcessing then
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
  if not ctx_DataAccess.PayrollIsProcessing then
    DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
  // commented it out as gets reopened with a different condition during fund splits in SPayrollCalculations
  //if not ctx_DataAccess.PayrollIsProcessing then
    //DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);

// Initialization part
  PR.Locate('PR_NBR', PR_CHECK.FieldByName('PR_NBR').Value, []);
  EmployeeNbr := PR_CHECK.Lookup('PR_CHECK_NBR', CheckNumber, 'EE_NBR');

  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GetPriorPRForEE') do
  begin
    SetParam('EENbr', EmployeeNbr);
    SetParam('CheckNbr', CheckNumber);
    SetParam('CheckType', CHECK_TYPE2_REGULAR);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0 then
  begin
    ctx_DataAccess.CUSTOM_VIEW.First;
    if GetYear(PR.FieldByName('CHECK_DATE').AsDateTime) <> GetYear(ctx_DataAccess.CUSTOM_VIEW.FieldByName('CHECK_DATE').AsDateTime) then
      ShortfallsDisabled := True;
  end;
  ctx_DataAccess.CUSTOM_VIEW.Close;

  if not ctx_DataAccess.PayrollIsProcessing then
  begin
    AsOfDate := PR.FieldByName('CHECK_DATE').AsDateTime + 1 - 1 / 1440;
    DM_HISTORICAL_TAXES.LoadDataAsOfDate(AsOfDate);
  end;
  if not HT.EE.Locate('EE_NBR', EmployeeNbr, []) then
    raise EInconsistentData.CreateHelp('Employee is not setup as of Check Date.', IDH_InconsistentData);

  PR_CHECK_LINES.SetLockedIndexFieldNames('PR_CHECK_NBR');
  PR_CHECK_LINES.SetRange([CheckNumber], [CheckNumber]);
// Make sure the check is not empty
  if PR_CHECK_LINES.BOF and PR_CHECK_LINES.EOF then
  begin
    if not ctx_DataAccess.PayrollIsProcessing then
      if not CheckHasTaxOverrides(CheckNumber) then
      begin
        PR_CHECK_LINES.CancelRange;
        PR_CHECK_LINES.ResetLockedIndexFieldNames('');
        raise EInvalidParameters.CreateHelp('The check is empty. Nothing to do!', IDH_InvalidParameters);
      end;
  end;

  DM_CLIENT.CL_PERSON.Activate;
  DM_CLIENT.CL_E_DS.Activate;
  DM_EMPLOYEE.EE.Activate;
  DM_COMPANY.CO.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

  DM_COMPANY.CO_DIVISION_LOCALS.DataRequired('ALL');
  DM_COMPANY.CO_BRANCH_LOCALS.DataRequired('ALL');
  DM_COMPANY.CO_DEPARTMENT_LOCALS.DataRequired('ALL');
  DM_COMPANY.CO_TEAM_LOCALS.DataRequired('ALL');
  DM_COMPANY.CO_JOBS_LOCALS.DataRequired('ALL');

  PR_CHECK.DisableControls;
  PR_CHECK_LINES.DisableControls;

  if ApplyUpdatesInTransaction then
  begin
    MyTransactionManager := TTransactionManager.Create(nil);
    MyTransactionManager.Initialize([PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINES]);
  end;

  SpecTaxedDeds := TSpecTaxedDeds.Create;

  ctx_DataAccess.CheckCalcInProgress := True;

// SR-60
    Recalculate := True;
    DM_HISTORICAL_TAXES.EE.Locate('EE_NBR', EmployeeNbr, []);
    DM_EMPLOYEE.EE.Locate('EE_NBR', EmployeeNbr, []);
    SaveCheckNbr := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
    if not ctx_DataAccess.PayrollIsProcessing then
      PR_CHECK.SetLockedIndexFieldNames('EE_NBR;PAYMENT_SERIAL_NUMBER');
    PR_CHECK.Locate('PR_CHECK_NBR', CheckNumber, []);
    if not ShortfallsDisabled then
      ShortfallsDisabled := PR_CHECK.FieldByName('DISABLE_SHORTFALLS').Value = 'Y';
    if not ctx_DataAccess.PayrollIsProcessing then
    begin
      DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR=' + IntToStr(EmployeeNbr));
      DM_EMPLOYEE.EE_LOCALS.DataRequired('EE_NBR=' + IntToStr(EmployeeNbr));
    end
    else
    begin
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.SetLockedIndexFieldNames('EE_NBR');
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.SetRange([PR_CHECK.FieldByName('EE_NBR').AsInteger], [PR_CHECK.FieldByName('EE_NBR').AsInteger]);
    end;
    DM_EMPLOYEE.EE_STATES.SetLockedIndexFieldNames('EE_NBR');
    DM_EMPLOYEE.EE_STATES.SetRange([EmployeeNbr], [EmployeeNbr]);
    DM_EMPLOYEE.EE_LOCALS.SetLockedIndexFieldNames('EE_NBR');
    DM_EMPLOYEE.EE_LOCALS.SetRange([EmployeeNbr], [EmployeeNbr]);

    if FetchPayrollData then
    begin
      if not ctx_DataAccess.PayrollIsProcessing then
      begin
        PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckNumber));
        PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckNumber));
        PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckNumber));
// Get PR_CHECK_LINE_LOCALS
        with ctx_DataAccess.CUSTOM_VIEW do
        if not PR_CHECK_LINE_LOCALS.Active or (PR_CHECK_LINE_LOCALS.ClientID <> ClientID)
        or (PR_CHECK_LINE_LOCALS.OpenCondition <> 'PR_CHECK_NBR = ' + IntToStr(CheckNumber)) then
        begin
          if Active then
            Close;
          if PR_CHECK_LINE_LOCALS.Active then
            PR_CHECK_LINE_LOCALS.Close;
          with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
          begin
            SetMacro('Columns', 'T1.*');
            SetMacro('TABLE1', PR_CHECK_LINE_LOCALS.Name);
            SetMacro('TABLE2', 'PR_CHECK_LINES');
            SetMacro('NBRFIELD', 'PR_CHECK');
            SetMacro('JOINFIELD', 'PR_CHECK_LINES');
            SetParam('RecordNbr', CheckNumber);
            ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          end;
          ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
          PR_CHECK_LINE_LOCALS.ClientID := ctx_DataAccess.ClientID;
          PR_CHECK_LINE_LOCALS.Data := Data;
          PR_CHECK_LINE_LOCALS.SetOpenCondition('PR_CHECK_NBR = ' + IntToStr(CheckNumber));
        end;
      end;
      PR_BATCH.DataRequired('PR_NBR=' + PR_CHECK.FieldByName('PR_NBR').AsString);
    end;

    PR_BATCH.Locate('PR_BATCH_NBR', PR_CHECK.FieldByName('PR_BATCH_NBR').Value, []);

    IsImport := (PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_IMPORT);
//    NegCheckAllowed := (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_3RD_PARTY]);

    NegCheckAllowed := (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD, CHECK_TYPE2_3RD_PARTY])
                        and not ((PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] <> CHECK_TYPE2_MANUAL) and
                                  (PR_CHECK.FieldByName('UPDATE_BALANCE').AsString = GROUP_BOX_YES));

    PR_CHECK.Edit;
    PR_CHECK_STATES_2 := TevClientDataSet.Create(Nil);
    PR_CHECK_SUI_2 := TevClientDataSet.Create(Nil);
    PR_CHECK_LOCALS_2 := TevClientDataSet.Create(Nil);
    ctx_PayrollCalculation.SetStateTax(0);
    bOldVal := ctx_DataAccess.Validate;
    ctx_DataAccess.Validate := False;
    try
// Prior Check Deductions Shortfalls
      if (PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_REGULAR) and (not IsImport) and (not ForTaxCalculator) then
      begin
        Q := TevQuery.Create('PriorCheckDeductionsShortfalls');
        Q.Param['PrNbr'] := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
        Q.Macro['CheckType'] := '''' + CHECK_TYPE2_REGULAR + ''', ''' + CHECK_TYPE2_VOID + '''';
        Q.Param['EeNbr'] := PR_CHECK.FieldByName('EE_NBR').AsInteger;
        while not Q.Result.EOF do
        begin
          if ConvertNull(Q.Result.Fields[0].Value, 0) <> 0 then
          begin
            if PR_CHECK_LINES.Locate('CL_E_DS_NBR;LINE_TYPE', VarArrayOf([Q.Result['CL_E_DS_NBR'], CHECK_LINE_TYPE_SHORTFALL]), []) then
              PR_CHECK_LINES.Edit
            else
              PR_CHECK_LINES.Insert;
            PR_CHECK_LINES['CL_E_DS_NBR'] := Q.Result['CL_E_DS_NBR'];
            PR_CHECK_LINES['AMOUNT'] := Q.Result['SUM_AMOUNT'];
            PR_CHECK_LINES['CL_AGENCY_NBR'] := Q.Result['MAX_NBR'];
            PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := Q.Result['EE_SCHEDULED_E_DS_NBR'];
            PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_SHORTFALL;
            PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := Null;
            PR_CHECK_LINES.Post;
          end;
          Q.Result.Next;
        end;
        PR_CHECK_LINES.First;
        while not PR_CHECK_LINES.EOF do
        begin
          if PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_SHORTFALL then
            if VarIsNull(Q.Result.vclDataset.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'PR_CHECK_NBR')) then
            begin
              PR_CHECK_LINES.Delete;
              Continue;
            end;
          PR_CHECK_LINES.Next;
        end;
      end;
// Prior Check Deductions Shortfalls are done

// Check if all E/D codes are setup correctly
      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.EOF do
      begin
        if not DM_HISTORICAL_TAXES.CL_E_DS.Locate('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, []) then
          raise EInconsistentData.CreateHelp('E/D Code #' + DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'CUSTOM_E_D_CODE_NUMBER') +
            ' set up with an Effective Date greater than the Check Date.', IDH_InconsistentData);
        PR_CHECK_LINES.Next;
      end;

// Refresh salary and rate
      if (not IsImport) and (not ForTaxCalculator) then
      begin
        try
          EDNbr := ctx_PayrollCalculation.GetCompanyED(ED_OEARN_SALARY);
        except
          raise ESalaryNotSetup.CreateHelp('Salary earning is not setup for the client. Can not continue.', IDH_InconsistentData);
        end;
        PR_CHECK_LINES.First;
        while not PR_CHECK_LINES.EOF do
        begin
          if Trim(ExtractFromFiller(ConvertNull(PR_CHECK_LINES.FieldByName('FILLER').Value, ''), 5, 8)) = '' then
          begin
            if (PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value = EDNbr) and (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) then
            // Salary
            begin
              PR_CHECK_LINES.Edit;
              if PR_CHECK_LINES.FieldByName('AMOUNT').Value <> DM_EMPLOYEE.EE.FieldByName('SALARY_AMOUNT').Value then
              begin
                if ConvertNull(PR_CHECK.FieldByName('SALARY').Value, 0) = 0 then
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(ConvertNull(DM_EMPLOYEE.EE.FieldByName('SALARY_AMOUNT').Value, 0))
                else
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK.FieldByName('SALARY').Value;
              end;
              PR_CHECK_LINES.Post;
            end
            else
            // Rate
            begin
              EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
              if ctx_PayrollCalculation.RecalcRateForED(EDCodeType) then
              begin
                PR_CHECK_LINES.Edit;
                PR_CHECK_LINES.Post;
              end;
            end;
          end;
          PR_CHECK_LINES.Next;
        end;
      end;
// Salary and rate refreshed

      RecalcCount := 0;
      while Recalculate do
      begin
        if RecalcCount = 50 then
          raise EInconsistentData.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString
          + '. This check is unable to complete calculations due to the data in the check being incompatible with' + #13
          + 'an incorrect setup on a Scheduled E/D. Please change the check data and/or the Scheduled E/D to fix this issue.'), IDH_InconsistentData);
        Inc(RecalcCount);
        Net := 0;
        Recalculate := False;
        HasTips := True;
        SubtractChildSupport := True;
        OldRecordCount := PR_CHECK_LINES.RecordCount;
        if not Assigned(CUSTOM_VIEW_2) then
          CUSTOM_VIEW_2 := TevClientDataSet.Create(Nil);
// Here is the alternative way to get necessary data into CUSTOM_VIEW_2 to satisfy remote client
        with CUSTOM_VIEW_2 do
        begin
          if not Active then
          begin
            FieldDefs.Add('PR_CHECK_LINES_NBR', ftInteger, 0, False);
            FieldDefs.Add('AMOUNT', ftFloat, 0, False);
            FieldDefs.Add('LINE_TYPE', ftString, 1, False);
            FieldDefs.Add('E_D_CODE_TYPE', ftString, 2, False);
            FieldDefs.Add('PRIORITY_TO_EXCLUDE', ftInteger, 0, False);
            FieldDefs.Add('PRIORITY_NUMBER', ftInteger, 0, False);
            FieldDefs.Add('TAXED_NORMALLY', ftString, 1, False);
            FieldDefs.Add('EE_SCHEDULED_E_DS_NBR', ftInteger, 0, False);
            FieldDefs.Add('CL_E_DS_NBR', ftInteger, 0, False);
            CreateDataSet;

            PR_CHECK_LINES.First;
            while not PR_CHECK_LINES.EOF do
            begin
              Insert;
              FieldByName('PR_CHECK_LINES_NBR').Value := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value;
              FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').Value;
              FieldByName('LINE_TYPE').Value := PR_CHECK_LINES.FieldByName('LINE_TYPE').Value;
              FieldByName('E_D_CODE_TYPE').Value := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value,
                'E_D_CODE_TYPE');
              FieldByName('PRIORITY_TO_EXCLUDE').Value := ConvertNull(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR',
                PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'PRIORITY_TO_EXCLUDE'), 0);
              if not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
                FieldByName('PRIORITY_NUMBER').Value := ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR',
                  PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, 'PRIORITY_NUMBER'), 0);
              if TypeIsSpecTaxedDed(FieldByName('E_D_CODE_TYPE').AsString) then
                FieldByName('TAXED_NORMALLY').Value := 'N'
              else
                FieldByName('TAXED_NORMALLY').Value := 'Y';
              if DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'APPLY_BEFORE_TAXES') = GROUP_BOX_YES then
                FieldByName('TAXED_NORMALLY').Value := 'B';
              FieldByName('EE_SCHEDULED_E_DS_NBR').Value := PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
              FieldByName('CL_E_DS_NBR').Value := PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value;
              if FieldByName('E_D_CODE_TYPE').Value = ED_DED_NM_WORKERS_COMP then
                FieldByName('PRIORITY_TO_EXCLUDE').Value := - MaxInt;
              Post;
              PR_CHECK_LINES.Next;
            end;
          end;
          IndexFieldNames := 'PRIORITY_TO_EXCLUDE;PRIORITY_NUMBER;TAXED_NORMALLY;E_D_CODE_TYPE;CL_E_DS_NBR';
// Done getting data into CUSTOM_VIEW_2

// Zero out scheduled memos
          if not (NegCheckAllowed or IsImport or ForTaxCalculator) then
          begin
            ctx_DataAccess.RecalcLineBeforePost := False;
            with PR_CHECK_LINES do
            try
              First;
              while not EOF do
              begin
                EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
                if (EDCodeType[1] = 'M') and (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) and (not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
                begin
                  Edit;
                  FieldByName('AMOUNT').Value := 0;
                  FieldByName('HOURS_OR_PIECES').Value := Null;
                  Post;
                end;
                Next;
              end;
            finally
              ctx_DataAccess.RecalcLineBeforePost := True;
            end;
          end;

// Add up all earnings
          First;
          while not EOF do
          begin
// Check if this line has Direct Deposit attached when it is not allowed
            if (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) and (not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
            begin
              if (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'SKIP_HOURS') = 'Y') and (not
                VarIsNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value,
                'EE_DIRECT_DEPOSIT_NBR'))) then
                raise EDirDepIsNotAllowed.CreateHelp('E/D ' + DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'DESCRIPTION')
                  + '" has a direct deposit attached but it is not allowed. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
            end;
            EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
// Refresh overtime
            if ((EDCodeType = ED_OEARN_WEIGHTED_HALF_TIME_OT) or (EDCodeType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT)
            or (EDCodeType = ED_OEARN_AVG_OVERTIME) {or (EDCodeType = ED_OEARN_AVG_MULT_OVERTIME) DC-219 }) and (not IsImport) and (not ForTaxCalculator)
            and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP)
            and (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] <> CHECK_TYPE2_MANUAL) then
            begin
              PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
              PR_CHECK_LINES.Edit;
              if EDCodeType = ED_OEARN_AVG_MULT_OVERTIME then
                ctx_PayrollCalculation.CalculateMultipleOvertime(PR_CHECK_LINES, PR_CHECK.FieldByName('EE_NBR').AsInteger)
              else
                ctx_PayrollCalculation.CalculateOvertime(PR_CHECK_LINES, EDCodeType, PR_CHECK.FieldByName('EE_NBR').AsInteger);
              PR_CHECK_LINES.Post;
              Edit;
              FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').Value;
              Post;
            end;
// Overtime refreshed

            if (EDCodeType[1] = 'E') and not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and not IsImport then
            begin
              PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
              PR_CHECK_LINES.Edit;
              if FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED then
              begin
                if ctx_PayrollCalculation.ScheduledEDRecalc(EDCodeType, Gross, Net, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
                PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES,
                DM_HISTORICAL_TAXES.SY_FED_TAX_TABLE, DM_HISTORICAL_TAXES.SY_STATES, JustPreProcessing) then
                  PR_CHECK_LINES.Post
                else
                begin
                  PR_CHECK_LINES.Edit;
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
                  PR_CHECK_LINES.Post;
                end;
              end
              else
              begin
                if ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, 0, JustPreProcessing) then
                  PR_CHECK_LINES.Post
                else
                begin
                  PR_CHECK_LINES.Edit;
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
                  PR_CHECK_LINES.Post;
                end;
              end;
              Edit;
              FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').Value;
              Post;
            end;

            if (ConvertNull(FieldByName('AMOUNT').Value, 0) >= 0) and (EDCodeType[1] = 'E')
            and (EDCodeType <> ED_MU_TIPPED_CREDIT_MAKEUP) then
            begin
              if not TypeIsTaxableMemo(EDCodeType) then
                Net := Net + ConvertNull(FieldByName('AMOUNT').Value, 0);
            end;
{            if TypeIsTips(EDCodeType) and (ConvertNull(FieldByName('AMOUNT').Value, 0) <> 0) then
              HasTips := True;}  // Commented out per ticket #41388, but am not sure if this is right
            Next;
          end;
          First;
          while not EOF do
          begin
            EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
            if (ConvertNull(FieldByName('AMOUNT').Value, 0) < 0) and (EDCodeType[1] = 'E')
            and (not TypeIsTaxableMemo(EDCodeType)) and (EDCodeType <> ED_MU_TIPPED_CREDIT_MAKEUP) then
            begin
              if (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) and not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and not IsImport then
              begin
                PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
                DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
                PR_CHECK_LINES.Edit;
                if ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, 0, JustPreProcessing) then
                  PR_CHECK_LINES.Post
                else
                begin
                  PR_CHECK_LINES.Edit;
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
                  PR_CHECK_LINES.Post;
                end;
                Edit;
                FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').Value;
                Post;
              end;

              Net := Net + FieldByName('AMOUNT').Value;
            end;
            if (Net < 0) and (not NegCheckAllowed) and (not ForTaxCalculator) then
            begin
              raise ENegativeCheck.CreateHelp('Negative earnings exceed positive on check #'
                + PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString + '. Check can not be calculated!', IDH_InconsistentData);
            end;
            Next;
          end;
        end;

        Gross := Net;
        if (not IsImport) or (IsImport and PR_CHECK.FieldByName('GROSS_WAGES').IsNull) then
          PR_CHECK.FieldByName('GROSS_WAGES').Value := RoundTwo(Gross);

        if not (NegCheckAllowed or IsImport or ForTaxCalculator) then
        begin
          ZeroOutScheduledEDs(True);
          SubtractDeductions(True);
        end;

// EE OASDI
        TaxTips := 0;
        if (not IsImport) or (IsImport and (PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').IsNull
        or PR_CHECK.FieldByName('EE_OASDI_TAX').IsNull)) then
        begin
          CalcEEOASDITaxes(CheckNumber, Tax, TaxWages, TaxTips, GrossWages, PR, PR_CHECK, PR_CHECK_LINES);
          if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO)
          and not PR_CHECK.FieldByName('OR_CHECK_OASDI').IsNull then
          begin
            Tax := PR_CHECK.FieldByName('OR_CHECK_OASDI').Value;
            if (Tax > Net) and (Tax - Net >= 0.005) and not NegCheckAllowed then
              raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
          end;
        end;

        if (not IsImport) or (IsImport and PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').IsNull) then
        begin
          PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value := RoundTwo(TaxWages);
          PR_CHECK.FieldByName('EE_OASDI_GROSS_WAGES').Value := RoundTwo(GrossWages);
        end;
        PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value := RoundTwo(TaxTips);
        if (not IsImport) or (IsImport and PR_CHECK.FieldByName('EE_OASDI_TAX').IsNull) then
          PR_CHECK.FieldByName('EE_OASDI_TAX').Value := Tax;

        TaxWages := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value, 0);
        TaxTips := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value, 0);
        Tax := ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0);

        Tax := RoundTwo(Tax);
        if (Tax >= Net) and (not NegCheckAllowed) then
        begin
          Tax := Net;
          Net := 0;
        end
        else
          Net := Net - Tax;
        PR_CHECK.FieldByName('EE_OASDI_TAX').Value := Tax;
        if PR_CHECK.FieldByName('EE_OASDI_GROSS_WAGES').IsNull then
          PR_CHECK.FieldByName('EE_OASDI_GROSS_WAGES').Value := PR_CHECK.FieldByName('EE_OASDI_GROSS_WAGES').Value;

// EE Medicare
        if (not IsImport) or (IsImport and (PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').IsNull
        or PR_CHECK.FieldByName('EE_MEDICARE_TAX').IsNull)) then
        begin
          CalcEEMedicareTaxes(CheckNumber, Tax, TaxWages, GrossWages, PR, PR_CHECK, PR_CHECK_LINES);
          if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO)
          and not PR_CHECK.FieldByName('OR_CHECK_MEDICARE').IsNull then
          begin
            Tax := PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value;
            if (Tax > Net) and (Tax - Net >= 0.005) and not NegCheckAllowed then
              raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
          end;
        end;

        if (not IsImport) or (IsImport and PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').IsNull) then
        begin
          PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').Value := RoundTwo(TaxWages);
          PR_CHECK.FieldByName('EE_MEDICARE_GROSS_WAGES').Value := RoundTwo(GrossWages);
        end;
        if (not IsImport) or (IsImport and PR_CHECK.FieldByName('EE_MEDICARE_TAX').IsNull) then
          PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value := Tax;

        TaxWages := ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').Value, 0);
        Tax := ConvertNull(PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value, 0);

        Tax := RoundTwo(Tax);
        if (Tax >= Net) and (not NegCheckAllowed) then
        begin
          Tax := Net;
          Net := 0;
        end
        else
          Net := Net - Tax;
        PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value := Tax;
        if PR_CHECK.FieldByName('EE_MEDICARE_GROSS_WAGES').IsNull then
          PR_CHECK.FieldByName('EE_MEDICARE_GROSS_WAGES').Value := PR_CHECK.FieldByName('EE_MEDICARE_GROSS_WAGES').Value;

// Backup Withholding
        Net := Net - RoundTwo(ConvertNull(PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value, 0));
        if (Net < 0) and (not NegCheckAllowed) then
        begin
          PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value := PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value + Net;
          Net := 0;
        end;

        if (DM_COMPANY.CO.FieldByName('CALCULATE_LOCALS_FIRST').Value = 'Y') and (DM_COMPANY.CO.FieldByName('CALCULATE_STATES_FIRST').Value = CALC_FED_STATE_SUI) then
          ProcessLocals;

// Construct a list of states for the check
        if VarIsNull(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value, 'CO_STATES_NBR')) then
          raise EEmptyEEHomeState.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) +
            ' home state is empty! Check calculation has been stopped.', IDH_InconsistentData);

        CreateNewClientDataSet(Nil, PR_CHECK_LINES_2, PR_CHECK_LINES);
        PR_CHECK_LINES.First;
        while not PR_CHECK_LINES.EOF do
        begin
          PR_CHECK_LINES_2.Append;
          for I := 0 to PR_CHECK_LINES_2.FieldCount - 1 do
            PR_CHECK_LINES_2.Fields[I].Value := PR_CHECK_LINES.FieldByName(PR_CHECK_LINES_2.Fields[I].FieldName).Value;
          PR_CHECK_LINES_2.Post;
          PR_CHECK_LINES.Next;
        end;
        StatesList := TStringList.Create;
        try
          if IsImport or ForTaxCalculator then
          with PR_CHECK_STATES_2 do
          begin
            PR_CHECK_STATES_2.CloneCursor(PR_CHECK_STATES, True);
            Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
            Filtered := True;
            First;
            while not EOF do
            begin
              if (StatesList.IndexOf(FieldByName('EE_STATES_NBR').AsString) = -1) then
                StatesList.Add(FieldByName('EE_STATES_NBR').AsString);
              Next;
            end;
          end
          else
          with PR_CHECK_LINES_2 do
          begin
            StatesList.Add(DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsString);
            First;
            while not EOF do
            begin
              if not FieldByName('EE_STATES_NBR').IsNull then
              begin
                EEStatesNbr := FieldByName('EE_STATES_NBR').AsInteger;
              end
              else
              begin
                EEStatesNbr := 0;
                if not FieldByName('CO_JOBS_NBR').IsNull then
                begin
                  DM_COMPANY.CO_JOBS.DataRequired('ALL');
                  EEStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.CO_JOBS.Lookup('CO_JOBS_NBR', FieldByName('CO_JOBS_NBR').Value, 'HOME_CO_STATES_NBR'), 'EE_STATES_NBR'), 0);
                end;

                if EEStatesNbr = 0 then
                begin
                  if not FieldByName('CO_TEAM_NBR').IsNull then
                  begin
                    DM_COMPANY.CO_TEAM.DataRequired('ALL');
                    if DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', FieldByName('CO_TEAM_NBR').Value, 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
                      EEStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', FieldByName('CO_TEAM_NBR').Value, 'HOME_CO_STATES_NBR'), 'EE_STATES_NBR'), 0);
                  end
                  else
                  if not FieldByName('CO_DEPARTMENT_NBR').IsNull then
                  begin
                    DM_COMPANY.CO_DEPARTMENT.DataRequired('ALL');
                    if DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', FieldByName('CO_DEPARTMENT_NBR').Value, 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
                      EEStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', FieldByName('CO_DEPARTMENT_NBR').Value, 'HOME_CO_STATES_NBR'), 'EE_STATES_NBR'), 0);
                  end
                  else
                  if not FieldByName('CO_BRANCH_NBR').IsNull then
                  begin
                    DM_COMPANY.CO_BRANCH.DataRequired('ALL');
                    if DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', FieldByName('CO_BRANCH_NBR').Value, 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
                      EEStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', FieldByName('CO_BRANCH_NBR').Value, 'HOME_CO_STATES_NBR'), 'EE_STATES_NBR'), 0);
                  end
                  else
                  if not FieldByName('CO_DIVISION_NBR').IsNull then
                  begin
                    DM_COMPANY.CO_DIVISION.DataRequired('ALL');
                    if DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', FieldByName('CO_DIVISION_NBR').Value, 'HOME_STATE_TYPE') = HOME_STATE_TYPE_OVERRIDE then
                      EEStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', FieldByName('CO_DIVISION_NBR').Value, 'HOME_CO_STATES_NBR'), 'EE_STATES_NBR'), 0);
                  end;
                end;
                  
                if EEStatesNbr = 0 then
                  EEStatesNbr := DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsInteger;

                if ctx_DataAccess.PayrollIsProcessing then
                begin
                  ctx_DataAccess.RecalcLineBeforePost := False;
                  try
                    if EEStatesNbr = 0 then
                      EEStatesNbr := DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsInteger;
                    PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
                    if (PR_CHECK_LINES.FieldByName('EE_STATES_NBR').Value <> EEStatesNbr)
                    or (PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').Value <> ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', DM_EMPLOYEE.EE_STATES['SUI_APPLY_CO_STATES_NBR'], 'EE_STATES_NBR'), EEStatesNbr)) then
                    begin
                      PR_CHECK_LINES.Edit;
                      PR_CHECK_LINES.FieldByName('EE_STATES_NBR').Value := EEStatesNbr;
                      DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', EEStatesNbr, []);
                      CoStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES['SUI_APPLY_CO_STATES_NBR'], DM_EMPLOYEE.EE_STATES['CO_STATES_NBR']);
                      EeSuiStatesNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CoStatesNbr, 'EE_STATES_NBR'), EEStatesNbr);
                      PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').Value := EeSuiStatesNbr;
                      PR_CHECK_LINES.Post;
                      PR_CHECK_LINES_2.Edit;
                      PR_CHECK_LINES_2.FieldByName('EE_STATES_NBR').Value := EEStatesNbr;
                      PR_CHECK_LINES_2.FieldByName('EE_SUI_STATES_NBR').Value := EeSuiStatesNbr;
                      PR_CHECK_LINES_2.Post;
                    end;
                  finally
                    ctx_DataAccess.RecalcLineBeforePost := True;
                  end;
                end;
              end;

              if EEStatesNbr <> 0 then
              begin
                if StatesList.IndexOf(IntToStr(EEStatesNbr)) = -1 then
                  StatesList.Add(IntToStr(EEStatesNbr));
              end;

              Next;
            end;
          end;
// Add reciprocal states
          if (not IsImport) and (not ForTaxCalculator) then
          with StatesList do
          for I := 0 to Count - 1 do
          begin
            CORecipStateNbr := DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', StrToInt(Strings[I]), 'RECIPROCAL_CO_STATES_NBR');
            if not VarIsNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CORecipStateNbr, 'EE_STATES_NBR')) then
              if StatesList.IndexOf(VarToStr(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CORecipStateNbr, 'EE_STATES_NBR'))) = -1 then
                StatesList.Add(VarToStr(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CORecipStateNbr, 'EE_STATES_NBR')));
          end;
// Add SDI states
          if (not IsImport) and (not ForTaxCalculator) then
          with StatesList do
          for I := 0 to Count - 1 do
          begin
            CORecipStateNbr := DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', StrToInt(Strings[I]), 'SDI_APPLY_CO_STATES_NBR');
            if not VarIsNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CORecipStateNbr, 'EE_STATES_NBR')) then
            begin
              if StatesList.IndexOf(VarToStr(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CORecipStateNbr, 'EE_STATES_NBR'))) = -1 then
                StatesList.Add(VarToStr(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', CORecipStateNbr, 'EE_STATES_NBR')));
            end
            else
              raise EEmployeeStateNotSetup.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString)
                + ' does not have SDI state setup.', IDH_InconsistentData);
          end;
// List constructed

// Clear all check states
          if (not IsImport) and (not ForTaxCalculator) then
          with PR_CHECK_STATES_2 do
          begin
            PR_CHECK_STATES_2.CloneCursor(PR_CHECK_STATES, True);
            Filter := 'PR_CHECK_NBR=' + IntToStr(CheckNumber);
            Filtered := True;
            First;
            while not EOF do
            begin
              PR_CHECK_STATES.Locate('PR_CHECK_STATES_NBR', FieldByName('PR_CHECK_STATES_NBR').Value, []);
              if (StatesList.IndexOf(FieldByName('EE_STATES_NBR').AsString) = -1)
              and (ConvertNull(PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value, 0) = 0) then
              begin
                PR_CHECK_STATES.Delete;
              end
              else
              begin
                if StatesList.IndexOf(FieldByName('EE_STATES_NBR').AsString) = -1 then
                  StatesList.Add(FieldByName('EE_STATES_NBR').AsString);
                PR_CHECK_STATES.Edit;
                PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Value := 0;
                PR_CHECK_STATES.FieldByName('STATE_TAX').Value := 0;
                PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value := 0;
                PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value := 0;
                PR_CHECK_STATES.Post;
              end;
              Next;
            end;
          end;

// Add check states that are missing
          if (not IsImport) and (not ForTaxCalculator) then
          with StatesList do
          for I := 0 to Count - 1 do
          begin
            if not PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, StrToInt(Strings[I])]), []) then
            begin
              PR_CHECK_STATES.Insert;
              PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value := StrToInt(Strings[I]);
              PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
              PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').Value := 'N';
              PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value := 'N';
              PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').Value := 'N';
              PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').Value := EXCLUDE_TAXES_NONE;
              PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE;
              PR_CHECK_STATES.Post;
            end;
          end;

// Process SDIs for check states
          with StatesList do
          for I := 0 to Count - 1 do
          begin
            PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, StrToInt(Strings[I])]), []);
            if VarIsNull(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', StrToInt(Strings[I]), 'CO_STATES_NBR')) then
              raise EEmployeeStateNotSetup.CreateHelp('Employee #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString)
              + ' state integrity check failed! Please, reset all empty states and SUI states for the check to EE home state.', IDH_InconsistentData);

            if not (PR_CHECK_STATES.State in [dsInsert, dsEdit]) then
              PR_CHECK_STATES.Edit;

            if (not IsImport) or (IsImport and (PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').IsNull
            or PR_CHECK_STATES.FieldByName('EE_SDI_TAX').IsNull)) then
            begin
              CalcEESDITaxes(CheckNumber, DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', StrToInt(Strings[I]), 'CO_STATES_NBR'), Tax,
                TaxWages, GrossWages, PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES);
              if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO)
              and not PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').IsNull then
              begin
                Tax := PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value;
                if (Tax > Net) and (Tax - Net >= 0.005) and not NegCheckAllowed then
                  raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
              end;
            end;

            if (not IsImport) or IsImport and (PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').IsNull) then
            begin
              PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value := RoundTwo(TaxWages);
              PR_CHECK_STATES.FieldByName('EE_SDI_GROSS_WAGES').Value := RoundTwo(GrossWages);
            end;
            if (not IsImport) or IsImport and (PR_CHECK_STATES.FieldByName('EE_SDI_TAX').IsNull) then
              PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value := Tax;

            TaxWages := ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value, 0);
            Tax := ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value, 0);

            ShortFall := 0;
            Tax := RoundTwo(Tax);
            if (Tax > Net) and (not NegCheckAllowed) then
            begin
              ShortFall := Tax - Net;
              Tax := Net;
              Net := 0;
              Result := True;
            end
            else
              Net := Net - Tax;
            PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value := Tax;
            PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').Value := ShortFall;
            if PR_CHECK_STATES.FieldByName('EE_SDI_GROSS_WAGES').IsNull then
              PR_CHECK_STATES.FieldByName('EE_SDI_GROSS_WAGES').Value := PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value;
            PR_CHECK_STATES.Post;
          end;

          if DM_COMPANY.CO.FieldByName('CALCULATE_STATES_FIRST').Value = CALC_FED_STATE_SUI then
            ProcessFed;

          if DM_COMPANY.CO.FieldByName('CALCULATE_STATES_FIRST').Value = CALC_SUI_STATE_FED then
            ProcessSUIs;

// Process State tax for check states
          if not IsImport then
          begin
            for I := 0 to StatesList.Count - 1 do
            begin
              PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, StrToInt(StatesList.Strings[I])]), []);
              PR_CHECK_STATES.Edit;
              PR_CHECK_STATES.FieldByName('STATE_TAX').Value := 0;
              PR_CHECK_STATES.Post;
            end;
          end;  

          with StatesList do
          for I := 0 to Count - 1 do
          begin
            PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, StrToInt(Strings[I])]), []);
            RecipTax := 0;
            RecipStateNbr := 0;
            if (not IsImport) or (IsImport and (PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').IsNull
            or PR_CHECK_STATES.FieldByName('STATE_TAX').IsNull)) then
            begin
              CalcStateTaxes(Warnings, CheckNumber, DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', StrToInt(Strings[I]), 'CO_STATES_NBR'), Tax,
                TaxWages, RecipTax, RecipStateNbr, PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES);
              if (PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').Value = GROUP_BOX_NO) and
              (PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
              begin
                Tax := PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value;
                if (Tax > Net) and (Tax - Net >= 0.005) and not NegCheckAllowed then
                  raise EInconsistentData.CreateHelp('The tax amounts entered will create a negative check. Please, change them. EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
              end;
            end;

            if (not IsImport) or IsImport and (PR_CHECK_STATES.FieldByName('STATE_TAX').IsNull) then
              Tax := RoundTwo(Tax)
            else
              Tax := ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0);
            ShortFall := 0;
            if (Tax > Net) and (not NegCheckAllowed) then
            begin
              ShortFall := Tax - Net;
              Tax := Net;
              Net := 0;
              Result := True;
            end
            else
              Net := Net - Tax;
            PR_CHECK_STATES.Edit;
            if (not IsImport) or IsImport and (PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').IsNull) then
              PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Value := RoundTwo(TaxWages);

            if (not IsImport) or IsImport and (PR_CHECK_STATES.FieldByName('STATE_TAX').IsNull) then
              PR_CHECK_STATES.FieldByName('STATE_TAX').Value := ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0) + Tax
            else
              PR_CHECK_STATES.FieldByName('STATE_TAX').Value := Tax;

            PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value := ShortFall;
            PR_CHECK_STATES.FieldByName('STATE_GROSS_WAGES').Value := PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Value;

            if (RecipTax <> 0) and (RecipStateNbr <> 0) then
            begin
              PR_CHECK_STATES.Post;
              PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, RecipStateNbr]), []);
              if (RoundTwo(RecipTax) > Net) and (not NegCheckAllowed) then
              begin
                RecipTax := Net;
                Net := 0;
              end
              else
                Net := Net - RoundTwo(RecipTax);
              PR_CHECK_STATES.Edit;
              PR_CHECK_STATES.FieldByName('STATE_TAX').Value := ConvertNull(PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0) + RoundTwo(RecipTax);
              PR_CHECK_STATES.Post;
              PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([CheckNumber, StrToInt(Strings[I])]), []);
            end
            else
              PR_CHECK_STATES.Post;
          end;
        finally
          StatesList.Free;
        end;

        if DM_COMPANY.CO.FieldByName('CALCULATE_STATES_FIRST').Value = CALC_STATE_SUI_FED then
          ProcessSUIs;

        if DM_COMPANY.CO.FieldByName('CALCULATE_STATES_FIRST').Value <> CALC_FED_STATE_SUI then
        begin
          if DM_COMPANY.CO.FieldByName('CALCULATE_LOCALS_FIRST').Value = 'Y' then
            ProcessLocals;
          ProcessFed;
        end;

        if DM_COMPANY.CO.FieldByName('CALCULATE_STATES_FIRST').Value = CALC_FED_STATE_SUI then
          ProcessSUIs;

        if DM_COMPANY.CO.FieldByName('CALCULATE_LOCALS_FIRST').Value = 'N' then
          ProcessLocals;

        if NegCheckAllowed or IsImport or ForTaxCalculator then
        begin
          PR_CHECK_LINES.First;
          while not PR_CHECK_LINES.EOF do
          begin
            EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
            if (EDCodeType[1] = 'D' {Deduction}) and (not TypeIsTaxableMemo(EDCodeType)) then
            begin
              //if not ( ForTaxCalculator and (not NegCheckAllowed) ) then
              if EDCodeType <> ED_DIRECT_DEPOSIT then
                Net := Net - ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
            end;

            // to update balance for NegCheckAllowed or  ForTaxCalculator
            if not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and not IsImport  then
            begin
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
              PR_CHECK_LINES.Edit;
              if ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, EmployeeNbr, JustPreProcessing) then
                PR_CHECK_LINES.Post
            end;

            PR_CHECK_LINES.Next;
          end;
          Break;
        end;

        ZeroOutScheduledEDs(False);
        if DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_GARNISHMENT_CHILD then
        begin
          DeductGarnishment;
          DeductChildSupport(False);
        end
        else
        if DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_CHILD_GARNISHMENT then
        begin
          DeductChildSupport(False);
          DeductGarnishment;
        end
        else
        if DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_CHILD then
          DeductChildSupport(False)
        else
        if DM_EMPLOYEE.EE.FieldByName('GOV_GARNISH_PRIOR_CHILD_SUPPT').AsString = TAKE_FIRST_GARNISHMENT then
          DeductGarnishment;

        CUSTOM_VIEW_2.First;
        with CUSTOM_VIEW_2 do
        while not EOF do
        begin
          EDCodeType := FieldByName('E_D_CODE_TYPE').Value;
          if (EDCodeType[1] = 'D') and ((FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER) or (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_USER))
          and (FieldByName('AMOUNT').Value < 0) and PermitType(EDCodeType) and (not TypeIsTaxableMemo(EDCodeType))
          and (FieldByName('LINE_TYPE').Value <> CHECK_LINE_TYPE_SHORTFALL) then
            AdjustDeduction(FieldByName('PR_CHECK_LINES_NBR').Value);
          Next;
        end;
        SubtractDeductions(False);
// Shortfalls
        if (not NegCheckAllowed) and (not IsImport) and (not ForTaxCalculator) then
        with CUSTOM_VIEW_2 do
        begin
          First;
          while not EOF do
          begin
            EDCodeType := FieldByName('E_D_CODE_TYPE').Value;
            if (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SHORTFALL) and PermitType(EDCodeType) then
            begin
              AdjustDeduction(FieldByName('PR_CHECK_LINES_NBR').Value);
              if TypeIsSpecTaxedDed(FieldByName('E_D_CODE_TYPE').AsString)
              and SpecTaxedDeds.Recalculated(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value, ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0)) then
                Recalculate := True;
            end;
            Next;
          end;
        end;
        if PR_CHECK_LINES.RecordCount <> OldRecordCount then
        begin
          Recalculate := True;
          FreeAndNil(CUSTOM_VIEW_2);
        end;
      end; // "Recalculate" while loop

      if (not NegCheckAllowed) and (not IsImport) and (not ForTaxCalculator) then
      begin
// Levy
        if CUSTOM_VIEW_2.Locate('E_D_CODE_TYPE', ED_DED_LEVY, []) then
        begin
          if not (((CUSTOM_VIEW_2['LINE_TYPE'] = CHECK_LINE_TYPE_USER) or (CUSTOM_VIEW_2['LINE_TYPE'] = CHECK_LINE_TYPE_DISTR_USER)) and (CUSTOM_VIEW_2['AMOUNT'] < 0)) then
          begin
            PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', CUSTOM_VIEW_2.FieldByName('PR_CHECK_LINES_NBR').Value, []);
            DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], []);
            if DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('TAKE_HOME_PAY').IsNull then
              raise EInconsistentData.CreateHelp('Levy requires take home pay on Scheduled E/D.', IDH_InconsistentData);
            Amount := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('TAKE_HOME_PAY').Value;
            PR_CHECK_LINES.Edit;
            if Net > Amount then
            begin
              Amount := RoundTwo(Amount);
              PR_CHECK_LINES.FieldByName('AMOUNT').Value := Net - Amount;
              if ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, 1, JustPreProcessing) then
              begin
                PR_CHECK_LINES.Post;
                Net := Net - PR_CHECK_LINES.FieldByName('AMOUNT').Value;
              end;
            end
            else
            begin
              PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
              PR_CHECK_LINES.Post;
            end;
          end;
          PR_CHECK_LINES.First;
          while not PR_CHECK_LINES.Eof do
          begin
            if PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_USER then
            begin
              if DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], []) then
              begin
                DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_DS_NBR'], []);
                if DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_DED_LEVY then
                begin
                  if Net < PR_CHECK_LINES['AMOUNT'] then
                  begin
                    PR_CHECK_LINES.Edit;
                    PR_CHECK_LINES['AMOUNT'] := Net;
                    PR_CHECK_LINES.Post;
                    Net := 0;
                  end
                  else
                  begin
                    Net := Net - PR_CHECK_LINES.FieldByName('AMOUNT').Value;
                  end;
                end;
              end;
            end;
            PR_CHECK_LINES.Next;
          end;
        end;
      end;

      if not NegCheckAllowed and not IsImport
      and not ((PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL]) and
              (PR_CHECK.FieldByName('UPDATE_BALANCE').AsString = GROUP_BOX_YES))
      or (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL])
      or ForTaxCalculator then
      begin
// Direct Deposits
        OldNet := Net;
        CUSTOM_VIEW_2.First;
        with CUSTOM_VIEW_2 do
        while not EOF do
        begin
          EDCodeType := FieldByName('E_D_CODE_TYPE').Value;
          if (EDCodeType = ED_DIRECT_DEPOSIT) and not(((FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER) or (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_USER)) and (FieldByName('AMOUNT').Value < 0)) then
          begin
            PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
            if VarIsNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_NBR;CL_E_DS_NBR', VarArrayOf([PR_CHECK['EE_NBR'], PR_CHECK_LINES['CL_E_DS_NBR']]), 'EE_DIRECT_DEPOSIT_NBR')) then
            begin
              raise ENoDirDepAttached.CreateHelp('Deduction "' + DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR',
                PR_CHECK_LINES['CL_E_DS_NBR'], 'DESCRIPTION')
                + '" does not have a direct deposit attached for EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
            end;

            PR_CHECK_LINES.Edit;
            if PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and (EDCodeType = ED_DIRECT_DEPOSIT) then
            begin
              Q := TevQuery.Create('select x.EE_SCHEDULED_E_DS_NBR from EE_SCHEDULED_E_DS x '+
                'where x.EE_NBR=:EE_NBR and x.CL_E_DS_NBR=:CL_E_DS_NBR and x.EFFECTIVE_START_DATE<=:CHECK_DATE and (x.EFFECTIVE_END_DATE is null or x.EFFECTIVE_END_DATE <=:CHECK_DATE) and {AsOfNow<x>}');
              Q.Param['EE_NBR'] := PR_CHECK['EE_NBR'];
              Q.Param['CL_E_DS_NBR'] := PR_CHECK_LINES['CL_E_DS_NBR'];
              Q.Param['CHECK_DATE'] := ConvertNull(PR['CHECK_DATE'], Now);
              PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := Q.Result['EE_SCHEDULED_E_DS_NBR'];
            end;

            if (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) and (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value =
            CHECK_LINE_TYPE_SCHEDULED) {Scheduled Dir.Dep.} then
            begin
              SchedDirDepNumber := PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
              if DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedDirDepNumber, 'DEDUCT_WHOLE_CHECK') = 'Y' then
              begin
                PR_CHECK_LINES.Cancel;
                Next;
                Continue;
              end
              else
              begin
                if ctx_PayrollCalculation.CheckDDPrenote(SchedDirDepNumber, PR.FieldByName('CHECK_DATE').Value) then
                begin
                  PR_CHECK_LINES.Cancel;
                  Next;
                  Continue;
                end
                else
                begin
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedDirDepNumber, 'AMOUNT'), 0)
                    + OldNet * ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedDirDepNumber, 'PERCENTAGE'), 0) * 0.01);
                end;
              end;
            end;
//            if PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL] then
//            begin
//              Net := Net + PR_CHECK_LINES.FieldByName('AMOUNT').Value;
//              PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
//            end
//            else
            if ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0) > Net then
              PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(Net);
            if not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and not IsImport then
            begin
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
              if ctx_PayrollCalculation.AnalyzeTargets(DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, 1, JustPreProcessing) then
              begin
                ctx_DataAccess.SetRecalcLineBeforePost(False);
                try
                  PR_CHECK_LINES.Post;
                finally
                  ctx_DataAccess.SetRecalcLineBeforePost(True);
                end;
                Net := Net - PR_CHECK_LINES.FieldByName('AMOUNT').Value;
              end;
            end
            else
            begin
              PR_CHECK_LINES.Post;
              Net := Net - ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
            end;
          end;
          Next;
        end;
// This loop is for 'Deduct Whole Check' only
        CUSTOM_VIEW_2.First;
        with CUSTOM_VIEW_2 do
        while not EOF do
        begin
          EDCodeType := FieldByName('E_D_CODE_TYPE').Value;
          if (EDCodeType = ED_DIRECT_DEPOSIT) and not(((FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER) or (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_USER)) and (FieldByName('AMOUNT').Value < 0)) then
          begin
            PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
            if (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull)
            and not ((PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER)
            or (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_USER)) {Scheduled Dir.Dep.} then
            begin
              SchedDirDepNumber := PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
              if DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedDirDepNumber, 'DEDUCT_WHOLE_CHECK') = 'Y' then
              begin
                if not ctx_PayrollCalculation.CheckDDPrenote(SchedDirDepNumber, PR.FieldByName('CHECK_DATE').Value) then
                begin
                  if ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedDirDepNumber, 'TAKE_HOME_PAY'), 0) <= Net then
                  begin
                    PR_CHECK_LINES.Edit;
                    PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(Net -
                      ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedDirDepNumber, 'TAKE_HOME_PAY'), 0));
                    if NetToGrossCalc then
                    begin
                      Net := Net - PR_CHECK_LINES.FieldByName('AMOUNT').Value;
                      DirectDeposit := DirectDeposit + PR_CHECK_LINES.FieldByName('AMOUNT').Value;
                      PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
                    end
                    else
                      Net := Net - PR_CHECK_LINES.FieldByName('AMOUNT').Value;
                    ctx_DataAccess.RecalcLineBeforePost := False;
                    try
                      PR_CHECK_LINES.Post;
                    finally
                      ctx_DataAccess.RecalcLineBeforePost := True;
                    end;
                    if Net = 0 then
                    begin
                      Break;
                    end;
                  end;
                end;
              end;
            end;
          end;
          Next;
        end;
      end;

      if ctx_DataAccess.PayrollIsProcessing then
      begin
        FreeAndNil(PR_CHECK_LINES_2);
        CreateNewClientDataSet(Nil, PR_CHECK_LINES_2, PR_CHECK_LINES);
        PR_CHECK_LINES.First;
        while not PR_CHECK_LINES.EOF do
        begin
          PR_CHECK_LINES_2.Append;
          for I := 0 to PR_CHECK_LINES_2.FieldCount - 1 do
            PR_CHECK_LINES_2.Fields[I].Value := PR_CHECK_LINES.FieldByName(PR_CHECK_LINES_2.Fields[I].FieldName).Value;
          PR_CHECK_LINES_2.Post;
          PR_CHECK_LINES.Next;
        end;
      end;
// Distribute labor
      if ctx_DataAccess.PayrollIsProcessing and (DM_COMPANY.CO.FieldByName('AUTO_LABOR_DIST_SHOW_DEDUCTS').Value <> 'N')
      and (not IsImport) and (not ForTaxCalculator) and (PR_CHECK.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').Value = 'N')
      and (not DM_EMPLOYEE.EE.FieldByName('ALD_CL_E_D_GROUPS_NBR').IsNull) then
      try
        with PR_CHECK_LINES_2 do
        begin
          ctx_DataAccess.RecalcLineBeforePost := False;
          DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
          DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.DataRequired('ALL');
          DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.SetLockedIndexFieldNames('EE_NBR');
          DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.SetRange([EmployeeNbr], [EmployeeNbr]);
          {DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.Filter := 'EE_NBR=' + IntToStr(EmployeeNbr);
          DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.Filtered := True;}

          // Test Auto Labor percentages for EE
          try
            Percentage := 0;
            DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.First;
            while not DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.EOF do
            begin
              Percentage := Percentage + ConvertNull(DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0);
              DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.Next;
            end;
            if RoundAll(Percentage, 2) <> 100 then
              raise EInconsistentData.CreateHelp('A sum of Auto-Labor Distribution percentages for EE #'
              + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + ' does not equal 100%. Can not continue.', IDH_InconsistentData);

            ALDGroupNbr := DM_EMPLOYEE.EE.FieldByName('ALD_CL_E_D_GROUPS_NBR').Value;
            First;
            while not EOF do
            begin
              EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
              if ((EDCodeType[1] = 'E') or TypeIsDistributedMemo(EDCodeType))
              and (FieldByName('CO_DIVISION_NBR').IsNull) and (FieldByName('CO_BRANCH_NBR').IsNull)
              and (FieldByName('CO_DEPARTMENT_NBR').IsNull) and (FieldByName('CO_TEAM_NBR').IsNull)
              and (FieldByName('CO_JOBS_NBR').IsNull) and (FieldByName('CO_WORKERS_COMP_NBR').IsNull)
              and DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([ALDGroupNbr, FieldByName('CL_E_DS_NBR').Value]), []) then
              begin
                Amount := 0;
                Hours := 0;
                Percentage := 0;
                Differential := 0;
                DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.First;
                while not DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.EOF do
                begin
                  PR_CHECK_LINES.Append;
                  PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value := DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_DIVISION_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value := DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_BRANCH_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value :=
                    DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_DEPARTMENT_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value := DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_TEAM_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').Value := DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_JOBS_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CO_WORKERS_COMP_NBR').Value :=
                    DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_WORKERS_COMP_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value := FieldByName('CL_E_DS_NBR').Value;
                  PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value := FieldByName('PR_CHECK_NBR').Value;
                  if FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED then
                    PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := CHECK_LINE_TYPE_DISTR_SCHEDULED
                  else
                  if FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER then
                    PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := CHECK_LINE_TYPE_DISTR_USER
                  else
                    PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := FieldByName('LINE_TYPE').Value;
                  PR_CHECK_LINES.FieldByName('LINE_ITEM_DATE').Value := FieldByName('LINE_ITEM_DATE').Value;
                  PR_CHECK_LINES.FieldByName('EE_STATES_NBR').Value := FieldByName('EE_STATES_NBR').Value;
                  PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').Value := FieldByName('EE_SUI_STATES_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CL_AGENCY_NBR').Value := FieldByName('CL_AGENCY_NBR').Value;
                  PR_CHECK_LINES.FieldByName('RATE_NUMBER').Value := FieldByName('RATE_NUMBER').Value;
                  PR_CHECK_LINES.FieldByName('RATE_OF_PAY').Value := FieldByName('RATE_OF_PAY').Value;
                  PR_CHECK_LINES.FieldByName('AGENCY_STATUS').Value := FieldByName('AGENCY_STATUS').Value;
                  PR_CHECK_LINES.FieldByName('CO_SHIFTS_NBR').Value := FieldByName('CO_SHIFTS_NBR').Value;
                  PR_CHECK_LINES.FieldByName('CL_PIECES_NBR').Value := FieldByName('CL_PIECES_NBR').Value;
                  PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value := FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
                  PR_CHECK_LINES.FieldByName('PUNCH_IN').Value := FieldByName('PUNCH_IN').Value;
                  PR_CHECK_LINES.FieldByName('PUNCH_OUT').Value := FieldByName('PUNCH_OUT').Value;
                  PR_CHECK_LINES.FieldByName('REDUCING_CL_E_D_GROUPS_NBR').Value := FieldByName('REDUCING_CL_E_D_GROUPS_NBR').Value;
                  PR_CHECK_LINES.FieldByName('REDUCED_HOURS').Value := FieldByName('REDUCED_HOURS').Value;
                  PR_CHECK_LINES.FieldByName('FILLER').Value := PutIntoFiller('', FieldByName('PR_CHECK_LINES_NBR').AsString, 5, 8);

                  Percentage := Percentage + ConvertNull(DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0);
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(ConvertNull(FieldByName('AMOUNT').Value *
                    ConvertNull(DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0) * 0.01, 0));
                  Amount := Amount + ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
                  if not FieldByName('HOURS_OR_PIECES').IsNull then
                    PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value := RoundTwo(ConvertNull(FieldByName('HOURS_OR_PIECES').Value *
                      ConvertNull(DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0) * 0.01, 0));
                  Hours := Hours + ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0);
                  PR_CHECK_LINES.FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value := RoundTwo(ConvertNull(FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value *
                    ConvertNull(DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').Value, 0) * 0.01, 0));
                  Differential := Differential + ConvertNull(PR_CHECK_LINES.FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value, 0);
                  if RoundAll(Percentage, 2) = 100 then
                  begin
                    PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value +
                      (FieldByName('AMOUNT').Value - Amount), 0));
                    if not FieldByName('HOURS_OR_PIECES').IsNull then
                      PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value := RoundTwo(ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value +
                        (FieldByName('HOURS_OR_PIECES').Value - Hours), 0));
                    PR_CHECK_LINES.FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value := RoundTwo(ConvertNull(PR_CHECK_LINES.FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value +
                      (FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value - Differential), 0));
                  end;

                  PR_CHECK_LINES.Post;
                  DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.Next;
                end;
                PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', FieldByName('PR_CHECK_LINES_NBR').Value, []);
                PR_CHECK_LINES.Delete;
                PR_CHECK_LINES_2.Delete;
              end
              else
                Next;
            end;
          finally
            DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.CancelRange;
            DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.ResetLockedIndexFieldNames('');
            {DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.Filtered := False;
            DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.Filter := '';}
          end;
        end;
      finally
        ctx_DataAccess.RecalcLineBeforePost := True;
      end;
// Labor is distributed
// Auto-populate empty DBDTs, W/Cs and shifts for auto-shift
      if ctx_DataAccess.PayrollIsProcessing then
      try
        ctx_DataAccess.RecalcLineBeforePost := False;
        with PR_CHECK_LINES do
        begin
          DM_EMPLOYEE.EE_RATES.Activate;
          DM_COMPANY.CO_SHIFTS.Activate;
          DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
          PR_CHECK_LINES.First;
          while not PR_CHECK_LINES.EOF do
          begin
{            if not Locate('PR_CHECK_LINES_NBR', PR_CHECK_LINES_2.FieldByName('PR_CHECK_LINES_NBR').Value, []) then
              raise EInconsistentData.CreateHelp('Could not locate a check line while assigning DBDTs! EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);}
            EERateNbr := ConvertNull(DM_EMPLOYEE.EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr,
              Abs(PR_CHECK_LINES.FieldByName('RATE_NUMBER').AsInteger)]), 'EE_RATES_NBR'), 0);
            Edit;
            if EERateNbr <> 0 then
              ctx_PayrollCalculation.FillInDBDTBasedOnRate(EERateNbr, PR_CHECK_LINES, DM_EMPLOYEE.EE_RATES);
            if FieldByName('CO_DIVISION_NBR').IsNull then
              FieldByName('CO_DIVISION_NBR').Value := DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').Value;
            if FieldByName('CO_BRANCH_NBR').IsNull then
              FieldByName('CO_BRANCH_NBR').Value := DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').Value;
            if FieldByName('CO_DEPARTMENT_NBR').IsNull then
              FieldByName('CO_DEPARTMENT_NBR').Value := DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').Value;
            if FieldByName('CO_TEAM_NBR').IsNull then
              FieldByName('CO_TEAM_NBR').Value := DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').Value;
            if FieldByName('CO_JOBS_NBR').IsNull then
              FieldByName('CO_JOBS_NBR').Value := DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').Value;

            if FieldByName('CO_WORKERS_COMP_NBR').IsNull and not FieldByName('CO_JOBS_NBR').IsNull then
            begin
              DM_COMPANY.CO_JOBS.DataRequired('CO_NBR=' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString);
              FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_JOBS.NewLookup('CO_JOBS_NBR', FieldByName('CO_JOBS_NBR').Value, 'CO_WORKERS_COMP_NBR');
            end;
            if FieldByName('CO_WORKERS_COMP_NBR').IsNull then
              FieldByName('CO_WORKERS_COMP_NBR').Value := DM_EMPLOYEE.EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_WORKERS_COMP_NBR');
            if FieldByName('CO_WORKERS_COMP_NBR').IsNull then
              FieldByName('CO_WORKERS_COMP_NBR').Value := DM_EMPLOYEE.EE.FieldByName('CO_WORKERS_COMP_NBR').Value;
            if FieldByName('CO_WORKERS_COMP_NBR').IsNull then
            begin
              if not FieldByName('CO_TEAM_NBR').IsNull then
              begin
                DM_COMPANY.CO_TEAM.DataRequired('ALL');
                FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_TEAM.NewLookup('CO_TEAM_NBR', FieldByName('CO_TEAM_NBR').Value, 'CO_WORKERS_COMP_NBR');
              end
              else
              if not FieldByName('CO_DEPARTMENT_NBR').IsNull then
              begin
                DM_COMPANY.CO_DEPARTMENT.DataRequired('ALL');
                FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_DEPARTMENT.NewLookup('CO_DEPARTMENT_NBR', FieldByName('CO_DEPARTMENT_NBR').Value, 'CO_WORKERS_COMP_NBR');
              end
              else
              if not FieldByName('CO_BRANCH_NBR').IsNull then
              begin
                DM_COMPANY.CO_BRANCH.DataRequired('ALL');
                FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_BRANCH.NewLookup('CO_BRANCH_NBR', FieldByName('CO_BRANCH_NBR').Value, 'CO_WORKERS_COMP_NBR');
              end
              else
              if not FieldByName('CO_DIVISION_NBR').IsNull then
              begin
                DM_COMPANY.CO_DIVISION.DataRequired('ALL');
                FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_DIVISION.NewLookup('CO_DIVISION_NBR', FieldByName('CO_DIVISION_NBR').Value, 'CO_WORKERS_COMP_NBR');
              end;
            end;
            if FieldByName('CO_WORKERS_COMP_NBR').IsNull then
              FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO.FieldByName('CO_WORKERS_COMP_NBR').Value;

            if FieldByName('CO_SHIFTS_NBR').IsNull then
            begin
              if not DM_EMPLOYEE.EE.FieldByName('AUTOPAY_CO_SHIFTS_NBR').IsNull then
              begin
                if not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR',
                  VarArrayOf([DM_COMPANY.CO_SHIFTS.Lookup('CO_SHIFTS_NBR', DM_EMPLOYEE.EE.FieldByName('AUTOPAY_CO_SHIFTS_NBR').Value, 'CL_E_D_GROUPS_NBR'),
                  PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value]), 'CL_E_D_GROUP_CODES_NBR')) then
                  FieldByName('CO_SHIFTS_NBR').Value := DM_EMPLOYEE.EE.FieldByName('AUTOPAY_CO_SHIFTS_NBR').Value;
              end;
            end;
            Post;
            PR_CHECK_LINES.Next;
          end;
        end;
      finally
        ctx_DataAccess.RecalcLineBeforePost := True;
      end;
// Empty DBDTs and shifts are auto-populated

// Get rid of all empty check lines
      if ctx_DataAccess.PayrollIsProcessing then
      begin
        PR_CHECK_LINES.First;
        while not PR_CHECK_LINES.EOF do
        begin
          if (ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0) = 0) and
          (ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0) = 0) and
          (ConvertNull(PR_CHECK_LINES.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value, 0) = 0) then
          begin
            if VarIsNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value,
            'EE_DIRECT_DEPOSIT_NBR')) then
            begin
              PR_CHECK_LINES.Delete;
              Continue;
            end;

            if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL])
            and ctx_PayrollCalculation.CheckDDPrenote(PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, PR.FieldByName('CHECK_DATE').Value) then
              PR_CHECK_LINES.Next
            else
              PR_CHECK_LINES.Delete;
          end
          else
            PR_CHECK_LINES.Next;
        end;
      end;

      if netToGrossCalc then
        Net := Net + DirectDeposit;

      PR_CHECK.FieldByName('NET_WAGES').Value := RoundTwo(Net);
      PR_CHECK.Post;
      if ApplyUpdatesInTransaction then
        MyTransactionManager.ApplyUpdates;
    finally
      ctx_DataAccess.Validate := bOldVal;

      PR_CHECK.ResetLockedIndexFieldNames('');
      if not ctx_DataAccess.PayrollIsProcessing then
      begin
        PR_CHECK_LINES.CancelRange;
        PR_CHECK_LINES.ResetLockedIndexFieldNames('');
      end;
      if not ctx_DataAccess.PayrollIsProcessing then
      else
      begin
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.CancelRange;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.ResetLockedIndexFieldNames('');
      end;
      DM_EMPLOYEE.EE_STATES.CancelRange;
      DM_EMPLOYEE.EE_STATES.ResetLockedIndexFieldNames('');
      DM_EMPLOYEE.EE_LOCALS.CancelRange;
      DM_EMPLOYEE.EE_LOCALS.ResetLockedIndexFieldNames('');
      ctx_DataAccess.CheckCalcInProgress := False;
      PR_CHECK_LINES_2.Free;
      PR_CHECK_STATES_2.Free;
      PR_CHECK_SUI_2.Free;
      PR_CHECK_LOCALS_2.Free;
      CUSTOM_VIEW_2.Free;
      if ApplyUpdatesInTransaction then
      begin
        MyTransactionManager.Free;
      end;
      SpecTaxedDeds.Free;
      PR_CHECK_LINES.Cancel;
      PR_CHECK.EnableControls;
      PR_CHECK_LINES.EnableControls;
      if not ctx_DataAccess.PayrollIsProcessing then
        PR_CHECK.IndexFieldNames := '';
      PR_CHECK.Locate('PR_CHECK_NBR', SaveCheckNbr, []);
    end;
end;

procedure TevGenericGrossToNet.SetNet(const Value: Double);
begin
  FNet := Value;
end;

end.
