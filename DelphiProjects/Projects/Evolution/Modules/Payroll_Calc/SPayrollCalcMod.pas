// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPayrollCalcMod;

interface

uses
  Windows, SysUtils, Classes,  Graphics, DB, Math, Dialogs, Controls, EvTypes, StrUtils, StdCtrls,
  ActiveX, Forms, isBaseClasses, EvUtils, SSecurityInterface, SGrossToNet, Variants, EvBasicUtils,
  EvConsts, SFieldCodeValues, SDataStructure, SCheckLineManager, isVCLBugFix, Types,
  EvCommonInterfaces, EvContext, EvMainboard, EvExceptions, EvUIUtils, EvClientDataSet;

implementation

uses STimeOffUtils, DateUtils, SPD_DM_HISTORICAL_TAXES, SDataDictbureau, isBasicUtils, evDataSet,
  SDataDictclient, EvDataAccessComponents, isDataSet;

type
  TPeriodType = (ptQuarter, ptSemiAnnual, ptAnnual);
  
  TevPayrollCalculation = class(TisInterfacedObject, IevPayrollCalculation)
  private
    cdsFilterString: string;
    cdstTempCheckLines: TEvClientDataSet;
    LinesToAdd: TLinesToAdd;
    LastNewBatchNbr: Integer;
    LastClientNbrForLimitedEDs: Integer;
    LastPayrollNbrForLimitedEDs: Integer;
    cdstLimitedEDs: TevClientDataSet;
    cdstEDCheckDates: TevClientDataSet;
    cdstLimitedEEOASDI: TevClientDataSet;
    cdstLimitedEROASDI: TevClientDataSet;
    cdstLimitedEEMedicare: TevClientDataSet;
    cdstLimitedERMedicare: TevClientDataSet;
    cdstLimitedFUI: TevClientDataSet;
    cdstHomeDBDTFieldValues: TevClientDataSet;
    PriorPayrollEDs: TStringList;
    PriorCheckDate: TDateTime;
    FollowingCheckDate: TDateTime;
    FLastAppliedCheckRangePrCheckNbr: Integer;
    FStateTax: Double;
    FDM_HISTORICAL_TAXES: TDataModule;
    FGenericGrossToNet: TevGenericGrossToNet;
    FEeFilter: string;
    FEeFilterCaption: string;
    FLastFeedBack: string;
    FABANumber: string;
    function CheckDueDateForHoliday(DueDate: TDateTime; const GlAgencyNbr: Integer): TDateTime;
    procedure GetTaxDepositPeriodFromDataSet(const ds: TDataSet; const Date: TDateTime; const SY_AGENCY_NBR: Integer; var BeginDate, EndDate, DueDate: TDateTime; const TaxState: string);
    function GetStateTaxPaymenyAgencyNbr(State: string): Integer;
    function CheckForFedDaysRule(const DefaultDueDate, PeriodEndDate: TDateTime): TDateTime;
    function ReturnMonthNumber(sMonth: String; PeriodType: TPeriodType): Integer;
    procedure cdsLocalFilter(DataSet: TDataSet; var Accept: Boolean);
    procedure cdsSUIFilter(DataSet: TDataSet; var Accept: Boolean);
    procedure CheckQuarterBeginEnd(const CheckDate: TDateTime; var BegDate, EndDate: TDateTime);
    function ApplyTempCheckLinesCheckRange(const PrCheckNbr: Integer): Boolean;
    function GetMinimumWage(EE_STATES_NBR: Variant; EE_NBR: Variant): Real;
    function GetCoMinimumWage(CO_STATES_NBR: Variant; EE_NBR: Variant): Real;
    function ReturnHistValue(aDataSet: TevClientDataSet; aFieldName: String): Variant;
    function GetFixedAmount(EE_SCHEDULED_E_DS, PR: TevClientDataset; DefaultValue: Variant): Real;
  protected
    procedure  DoOnConstruction; override;
  public
    destructor Destroy; override;
    function CheckStateTaxDeposit(State: string; Amount: Double): Boolean;
    function GetCompanyED(EDType: string): Integer;
    procedure CopyPRTemplateDetails(TemplateNumber: Integer; PR_CHECK, PR_CHECK_STATES, PR_CHECK_LOCALS,
      CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS: TevClientDataSet);
    function GetHomeDBDTFieldValue(FieldName, Level: string; EmployeeNbr: Integer;
      CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE: TevClientDataSet; UsedByServer: Boolean): Variant;
    function GetEEPrimaryRate(EENumber: Integer; EE, EE_RATES: TevClientDataSet; UsedByServer: Boolean): Integer;
    function RecalcRateForED(EDCodeType: string): Boolean;
    procedure FillInDBDTBasedOnRate(EERateNbr: Integer; PR_CHECK_LINES, EE_RATES: TevClientDataSet);
    procedure DoRecalcRate(PR_CHECK, PR_CHECK_LINES, CL_E_DS, CO, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS, EE,
      EE_RATES: TevClientDataSet; UsedByServer: Boolean);
    function HolidayDay(Day: TDateTime; BankOnly: Boolean = False): string;
    function IsNonBusinessDay(Day: TDateTime): Boolean;
    function GetFutureMonthDate(MyDate: TDateTime; N: Word): TDateTime;
    function GetNextDate(MyDate: TDateTime; N: Word): TDateTime;
    function GetNextCheckDate(AfterDate: TDateTime; CompanyFrequency: Char; WithNoPayrollAttached: Boolean = False): TDateTime;
    function NextCheckDate(AfterDate: TDateTime; WithNoPayrollAttached: Boolean = False): TDateTime;
    function NextScheduledCallInDate(AfterDate: TDateTime; WithNoPayrollAttached: Boolean = False): TDateTime;
    function GetNextRunNumber(CheckDate: TDateTime): Integer;
    function GetNextPeriodBeginDate(BatchFrequency: Char): TDateTime;
    function NextPeriodBeginDate: TDateTime;
    function GetNextPeriodEndDate(BatchFrequency: Char; PeriodBeginDate: TDateTime): TDateTime;
    function NextPeriodEndDate: TDateTime;
    function FindNextPeriodBeginEndDate: Boolean;
    function FindNextPeriodBeginEndDateExt(PR, PR_BATCH: TevClientDataSet): Boolean;
    procedure GetFreqListForCheckDate(CheckDate: TDateTime; FreqList: TStringList);
    procedure CalculateOvertime(DataSet: TevClientDataSet; EDType: string; EE_NBR: Integer);
    function ScheduledEDRecalc(EDType: string; Gross, Net: Real; PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LOCALS, CL_E_DS, CL_PENSION, CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE,
      SY_STATES: TevClientDataSet; JustPreProcessing: Boolean = True; ProrateNbr: Integer = 1; ProrateRatio: Real = 1; OldNet: Real = 0): Boolean;
    function AnalyzeTargets(EE, EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK: TevClientDataSet; EmployeeNbr: Integer; JustPreProcessing: Boolean; AdjustBalanceOnly: Boolean = False): Boolean;
    procedure CopyPayroll(OldPayrollNbr: Integer);
    procedure VoidPayroll(OldPayrollNbr: Integer);
    function EarnCodeGroupCalc(GroupNbr: Variant; CalcAmount: Boolean; PR_CHECK: TevClientDataSet): Double;
    function EarnCodeGroupYTDCalc(GroupNbr: Variant; CalcAmount: Boolean): Double;
    function GetCompanyFrequencies(CompanyDataSet: TevClientDataSet): Char;
    function ListBackdatedPayrollProblems(CheckDate: TDateTime): string;
    // New methods
    procedure Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES,
      CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet; DoPrePost, PaySalary, PayHourly: Boolean;
      RefreshScheduledEDs: Boolean = False; CreatingNewBatch: Boolean = False; const DontPost: Boolean = False);
    procedure Generic_CreateRegularPRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
      EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet; ProirScheduledCheckDate,
      NextScheduledCheckDate: TDateTime; ScheduledEDsInPriorPayroll: TStringList; DoPrePost, PaySalary, PayHourly: Boolean;
      RefreshScheduledEDs: Boolean = False; CreatingNewBatch: Boolean = False);
    procedure Generic_CreateVoidPRCheckDetails(CheckToVoid: Integer; PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS: TevClientDataSet; const DontPost: Boolean = False);
    function CheckIfBlockCheckLineOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet): Boolean;
    procedure BlockEverythingOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet; WithTransaction: Boolean);
    procedure CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, CL_E_D_GROUP_CODES, CL_PERSON, CL_PIECES, CO_SHIFTS,
      CO_STATES, EE, EE_SCHEDULED_E_DS, EE_RATES, EE_STATES, EE_WORK_SHIFTS, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet;
     LogEvent :TOnPrCheckLinesLogMessage = nil);
    procedure AddExtraCheckLines(PR_CHECK_LINES: TevClientDataSet);
    function GetCustomBankAccountNumber(PR_CHECK: TevClientDataSet): string;
    function GetABANumber: string; //call after GetCustomBankAccountNumber
    function GetBankAccountNumber(PR_CHECK: TevClientDataSet; var AccountNbr: Integer): Boolean;
    function GetBankAccountNumberForMisc(PR_MISCELLANEOUS_CHECKS: TevClientDataSet; var AccountNbr: Integer): Boolean;
    procedure CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet);
    procedure DoAutoPost(PR_CHECK, PR_CHECK_LINES, EE, CO_E_D_CODES, CO_TEAM_PR_BATCH_DEFLT_ED, CO_DEPT_PR_BATCH_DEFLT_ED,
      CO_BRCH_PR_BATCH_DEFLT_ED, CO_DIV_PR_BATCH_DEFLT_ED, CO_PR_BATCH_DEFLT_ED: TevClientDataSet);
    procedure GetLimitedTaxAndDedYTD(PayrollNbr, CheckNbr, EENbr, RecordNbr: Integer; TaxType: TLimitedTaxType; BeginDate, EndDate: TDateTime;
      PR, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LINES: TevClientDataSet; var YTDTaxWage, YTDTax: Real; IgnoreCurrentPayroll: Boolean = False; ForConsolidatesOnly: Boolean = False);
    procedure GetLimitedTaxAndDedYTDforGTN(CheckNbr, RecordNbr: Integer; TaxType: TLimitedTaxType; PR, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI,
      PR_CHECK_LINES: TevClientDataSet; var YTDTaxWage, YTDTax: Real; StartDate: TDateTime = 0; EndDate: TDateTime = 0; IgnoreCurrentPayroll: Boolean = False);
    function CheckDDPrenote(SchedEDNbr: Integer; CheckDate: TDateTime): Boolean;
    function ConstructCheckSortIndex: string;
    function EDIsBlockedForPR(PayrollNbr, CONbr, CL_E_DS_Nbr: Integer; PR_SCHEDULED_E_DS, CO_E_D_CODES: TevClientDataSet): Boolean;
    function FrequencyBelongs(OneFrequency, MultipleFrequencies: string): Boolean;
    procedure UpdateLiabilityTableStatus(LiabilityTable: TevClientDataSet; TaxDepositNbr, PayingCompanyNbr: Integer);
    procedure AddToBankAccountRegister(
                const CoNbr, BankAccountNbr, TaxDepositNbr : integer;
                  const Amount : currency;
                   const Status, BankRegisterType : Char;
                    const CheckDate, DueDate, ProcessDate : TDateTime;
                     out BankAccRegNbr : Integer);
    procedure AddConsolidatedPaymentsToBankAccountRegister(
               LiabilityCoTable: TevClientDataset;
                PayingCompanyNbr, TaxDepositNbr : integer;
                 const Status, BankRegisterType : Char);
    procedure AddConsolidatedPaymentsToBar(
                    DS: array of TevClientDataSet;
                     const TaxDepositNbr: Integer;
                      const Status, BankRegisterType : char;
                      const bDepNbrFilter : Boolean = True);

    procedure UpdateAllLiabilityTablesStatus(TaxDepositNbr: Integer);

    function CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDueDate, PeriodEndDate: TDateTime): TDateTime; overload;
    function CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime; overload;
    function CheckForReversedNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
    procedure GetFedTaxDepositPeriod(Frequency: Char; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    function GetFedTaxDepositFreq(CoNbr: Integer): string;
    procedure GetStateTaxDepositPeriod(State: string; FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    procedure GetStateTaxDepositPeriodAndSeqNumber(State: string; FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate:
      TDateTime; out SeqNumber: Integer);
    function GetStateTaxDepositFreq(SyFreqNbr: Integer; CoNbr: Integer): string;
    procedure GetLocalTaxDepositPeriod(LocalNbr, FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    procedure GetLocalTaxDepositPeriodAndSeqNumber(LocalNbr, FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate:
      TDateTime; out SeqNumber: Integer);
    function LiabilityACH_KEY_CalcMethod(TaxTypeDesc : char; DepFreqNbr, SyNbr : Integer;
      const PrCheckDate:TDateTime; const calcEndDate:TDateTime = 0): String;
    procedure GetSuiTaxDepositPeriod(SySuiNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
    function GetLocalTaxDepositFreq(SyFreqNbr: Integer; CoNbr: Integer): string;
    function GetSequenceNumber(const Date: TDateTime; const Freq: Char = FREQUENCY_TYPE_QUARTERLY): Integer;
    function GetTempCheckLinesFieldValue(FieldName: string): Variant;
    function GetTempCheckLinesDS: TevClientDataSet;
    procedure AssignTempCheckLines(DS: TevClientDataSet);
    procedure ReleaseTempCheckLines;
    function GetCompanyPayFrequencies: String;
    procedure AssignCheckDefaults(TemplateNbr, PaymentSerialNbr: Integer);
    procedure CopyPRTemplate(TemplateNumber: Integer);
    function GetNextPaymentSerialNbr: Integer;
    function GrossToNet(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction: Boolean;
      JustPreProcess: Boolean; FetchPayrollData: Boolean; SecondCheck: Boolean): Boolean; // Result indicates if there were any shortfalls
    procedure GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
      PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS: TevClientDataSet; DisableYTDs, DisableShortfalls, netToGross: Boolean);
    procedure GetLimitedEDs(CoNbr, PayrollNbr: Integer; BeginDate, EndDate: TDateTime);
    procedure EraseLimitedEDs;
    procedure GetLimitedTaxes(CoNbr, PayrollNbr: Integer; BeginDate, EndDate: TDateTime);
    procedure EraseLimitedTaxes;
    procedure OpenDetailPayroll(PrNbr: Integer);
    procedure RefreshScheduledEDsForBatch(const BatchNbr: Integer; const NoManuals: Boolean = False);
    function CalcWorkersComp(PrNbr: Integer; sLevel: string = 'CO_NBR'; dds: TevClientDataSet = nil; const wcompNbr: integer = 0;FromMainProcessing:Boolean = False): Currency;
    procedure GetDistrTaxes(PrNbr: Integer; var cdsTaxDistr: TevClientDataSet; EEOnly: Integer = 0);
    function CalculateGroupTermLife(PR: TevClientDataSet; Multiplier: Real): Real;
    procedure SetStateTax(NewStateTax: Double);
    function GetAnnualMax(Amount: Variant; EDType: String; CLEDNbr: Integer): Double;
    procedure AttachPayrollToCalendar(PR: TevClientDataSet);
    function DM_HISTORICAL_TAXES: TDataModule;
    procedure SetEeFilter(const AFilter, ACaption: string);
    function  GetEeFilter: string;
    function  GetEeFilterCaption: string;
    procedure ProcessFeedback(const AShow: Boolean; const ADisplayString: String = '');
    function  GetLastFeedBack: string;
    procedure CalculateMultipleOvertime(DataSet: TevClientDataSet; EE_NBR: Integer);
  end;


function GetPayrollCalculation: IevPayrollCalculation;
begin
  Result := TevPayrollCalculation.Create;
end;


function SY_GLOBAL_AGENCY_NBR: Integer;
begin
  DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Activate;
  SY_GLOBAL_AGENCY_NBR := ConvertNull(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR'), 0);
end;

function ThisIsRightPayroll(WhichPayrolls: String; CheckDate, PriorCheckDate, NextCheckDate: TDateTime): Boolean;
begin
  Result := False;
  if WhichPayrolls = GROUP_BOX_FIRST then
  begin
    if (PriorCheckDate <> 0) and (GetMonth(CheckDate) <> GetMonth(PriorCheckDate)) or (PriorCheckDate = 0) then
      Result := True;
  end
  else
  if WhichPayrolls = GROUP_BOX_LAST then
  begin
    if GetMonth(CheckDate) <> GetMonth(NextCheckDate) then
      Result := True;
  end
  else
  if WhichPayrolls = GROUP_BOX_CLOSEST_TO_15 then
  begin
    if ((Abs(GetDay(CheckDate) - 15) < Abs(GetDay(NextCheckDate) - 15))
    or (Abs(GetDay(CheckDate) - 15) = Abs(GetDay(NextCheckDate) - 15))
    and (GetMonth(CheckDate) = 2) and (GetMonth(NextCheckDate) = 3))
    and (Abs(GetDay(CheckDate) - 15) <= Abs(GetDay(PriorCheckDate) - 15)) then
      Result := True;
  end
  else
    Result := True;
end;

function TevPayrollCalculation.CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDueDate, PeriodEndDate: TDateTime): TDateTime;
var
  d: TDateTime;
begin
  d := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, Days, PeriodEndDate);
  if d > DefaultDueDate then
    Result := d
  else
    Result := DefaultDueDate;
end;

function TevPayrollCalculation.CheckForReversedNDaysRule(const SY_GLOBAL_AGENCY_NBR: Integer; const Days: Integer; const DefaultDate: TDateTime): TDateTime;
begin
  Result := EvUtils.CheckForReversedNDaysRule(SY_GLOBAL_AGENCY_NBR, Days, DefaultDate);
end;

function TevPayrollCalculation.CheckForFedDaysRule(const DefaultDueDate, PeriodEndDate: TDateTime): TDateTime;
begin
  Result := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 3, DefaultDueDate, PeriodEndDate);
end;

function TevPayrollCalculation.CheckStateTaxDeposit(State: string; Amount: Double): Boolean;
var
  StateNbr: Integer;
  Limit: Double;
begin
  Result := False;
  DM_SYSTEM_STATE.SY_STATES.Activate;
  StateNbr := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', State, 'SY_STATES_NBR');
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Activate;
  with DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ do
  begin
    Limit := ConvertNull(Lookup('SY_STATES_NBR;FREQUENCY_TYPE', VarArrayOf([StateNbr, FREQUENCY_TYPE_NOW]), 'THRESHOLD_AMOUNT'), 0);
    if (Amount > Limit) and (Limit <> 0) then
      Result := True;
  end;
end;

function TevPayrollCalculation.GetCompanyED(EDType: string): Integer;
var
  Q: IEvQuery;
begin
  Q := TEvQuery.Create('SELECT cld.CL_E_DS_NBR FROM CL_E_DS cld '+
  ' JOIN co_e_d_codes cod ON cld.cl_e_ds_nbr=cod.cl_e_ds_nbr'+
  ' WHERE cod.co_nbr=:CO_NBR and cld.e_d_code_type=:E_D_CODE_TYPE '+
  ' and {AsOfNow<cld>} and {AsOfNow<cod>}'+
  ' ORDER BY cld.custom_e_d_code_number');
  Q.Param['CO_NBR'] := DM_CLIENT.CO['CO_NBR'];
  Q.Param['E_D_CODE_TYPE'] := EDType;
  if Q.Result.EOF then
    raise Exception.CreateFmt('E/D code is not set up for %s',[EDType]);
  Result := Q.Result.FieldByName('CL_E_DS_NBR').AsInteger;
end;

procedure TevPayrollCalculation.CopyPRTemplateDetails(TemplateNumber: Integer; PR_CHECK, PR_CHECK_STATES, PR_CHECK_LOCALS,
  CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS: TevClientDataSet);
var
  HomeStateNbr, TempNbr: Variant;
begin
  if PR_CHECK['CHECK_TYPE'] = CHECK_TYPE2_VOID then
    Exit;
  with CO_BATCH_STATES_OR_TEMPS do
  begin
    PR_CHECK_STATES.Filter := 'PR_CHECK_NBR=''' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString + '''';
    PR_CHECK_STATES.Filtered := True;
    while not PR_CHECK_STATES.EOF do
      PR_CHECK_STATES.Delete;
    PR_CHECK_STATES.Filtered := False;
    PR_CHECK_STATES.Filter := '';

    TempNbr := DM_EMPLOYEE.EE.Lookup('EE_NBR', PR_CHECK['EE_NBR'], 'HOME_TAX_EE_STATES_NBR');
    HomeStateNbr := EE_STATES.Lookup('EE_STATES_NBR', TempNbr, 'CO_STATES_NBR');


    CO_BATCH_STATES_OR_TEMPS.DataRequired('ALL');

    if not VarIsNull(HomeStateNbr) then
    begin
      Filter := 'CO_PR_CHECK_TEMPLATES_NBR=''' + IntToStr(ctx_DataAccess.TemplateNumber) + '''';
      Filtered := True;
      First;
      while not EOF do
      begin
        if FieldByName('CO_STATES_NBR').Value = HomeStateNbr then
        begin
          PR_CHECK_STATES.Insert;
          PR_CHECK_STATES['EE_STATES_NBR'] := TempNbr;
          PR_CHECK_STATES['TAX_AT_SUPPLEMENTAL_RATE'] := FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value;
          PR_CHECK_STATES['EXCLUDE_STATE'] := FieldByName('EXCLUDE_STATE').Value;
          PR_CHECK_STATES['EXCLUDE_SDI'] := FieldByName('EXCLUDE_SDI').Value;
          PR_CHECK_STATES['EXCLUDE_SUI'] := FieldByName('EXCLUDE_SUI').Value;
          PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := FieldByName('STATE_OVERRIDE_TYPE').Value;
          PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := FieldByName('STATE_OVERRIDE_VALUE').Value;
          PR_CHECK_STATES['EXCLUDE_ADDITIONAL_STATE'] := FieldByName('EXCLUDE_ADDITIONAL_STATE').Value;
          PR_CHECK_STATES.Post;
        end;
        Next;
      end;
      Filtered := False;
    end;
  end;

  with CO_BATCH_LOCAL_OR_TEMPS do
  begin
    PR_CHECK_LOCALS.Filter := 'PR_CHECK_NBR=''' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString + '''';
    PR_CHECK_LOCALS.Filtered := True;
    while not PR_CHECK_LOCALS.EOF do
      PR_CHECK_LOCALS.Delete;
    PR_CHECK_LOCALS.Filtered := False;
    PR_CHECK_LOCALS.Filter := '';

    CO_BATCH_LOCAL_OR_TEMPS.DataRequired('ALL');
    Filter := 'CO_PR_CHECK_TEMPLATES_NBR=''' + IntToStr(ctx_DataAccess.TemplateNumber) + '''';
    Filtered := True;
    First;
    while not EOF do
    begin
      TempNbr := EE_LOCALS.Lookup('CO_LOCAL_TAX_NBR;EE_NBR', VarArrayOf([FieldByName('CO_LOCAL_TAX_NBR').Value,
        PR_CHECK['EE_NBR']]), 'EE_LOCALS_NBR');
      if not VarIsNull(TempNbr) and (DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', HomeStateNbr, 'SY_STATES_NBR') =
      DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', FieldByName('CO_LOCAL_TAX_NBR').Value, 'SY_STATES_NBR')) then
      begin
        PR_CHECK_LOCALS.Insert;
        PR_CHECK_LOCALS['EE_LOCALS_NBR'] := TempNbr;
        PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := FieldByName('OVERRIDE_AMOUNT').Value;
        PR_CHECK_LOCALS['EXCLUDE_LOCAL'] := FieldByName('EXCLUDE_LOCAL').Value;
        PR_CHECK_LOCALS.Post;
      end;
      Next;
    end;
    Filtered := False;
  end;

  with DM_COMPANY.CO_PR_CHECK_TEMPLATE_E_DS do
  begin
    DM_COMPANY.CO_PR_CHECK_TEMPLATE_E_DS.DataRequired('ALL');
    Filter := 'CO_PR_CHECK_TEMPLATES_NBR=''' + IntToStr(ctx_DataAccess.TemplateNumber) + '''';
    Filtered := True;
    First;
    while not EOF do
    begin
      while DM_PAYROLL.PR_CHECK_LINES.Locate('CL_E_DS_NBR;PR_CHECK_NBR', VarArrayOf([FieldByName('CL_E_DS_NBR').Value,
      PR_CHECK['PR_CHECK_NBR']]), []) do
        DM_PAYROLL.PR_CHECK_LINES.Delete;
      Next;
    end;
    Filtered := False;
  end;
end;

function TevPayrollCalculation.GetHomeDBDTFieldValue(FieldName, Level: string; EmployeeNbr: Integer;
  CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE: TevClientDataSet; UsedByServer: Boolean): Variant;
var
  FoundRecord: Boolean;

  procedure AddRecordToDataSet(FromDataSet: TevClientDataSet);
  begin
    if not FoundRecord then
    with cdstHomeDBDTFieldValues do
    begin
      Append;
      FieldByName('CL_NBR').Value := ctx_DataAccess.ClientID;
      FieldByName('EE_NBR').Value := EmployeeNbr;
      FieldByName('LEVEL').Value := Level;
      if FromDataSet.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE then
      begin
        FieldByName('OVERRIDE_PAY_RATE').Value := FromDataSet['OVERRIDE_PAY_RATE'];
        FieldByName('OVERRIDE_EE_RATE_NUMBER').Value := FromDataSet['OVERRIDE_EE_RATE_NUMBER'];
      end;
      FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').Value := FromDataSet['PAYROLL_CL_BANK_ACCOUNT_NBR'];
      Post;
    end;
  end;

begin
  Result := Null;
  FoundRecord := False;
  with cdstHomeDBDTFieldValues do
  begin
    Last;
    while not BOF do
    begin
      if (FieldByName('CL_NBR').Value = ctx_DataAccess.ClientID) and (FieldByName('EE_NBR').Value = EmployeeNbr) and (FieldByName('LEVEL').Value = Level) then
      begin
        FoundRecord := True;
        Break;
      end;
      Prior;
    end;
  end;

  if FoundRecord and (cdstHomeDBDTFieldValues.FindField(FieldName) <> Nil) then
  begin
    Result := cdstHomeDBDTFieldValues.FieldByName(FieldName).Value;
    Exit;
  end;

  case Level[1] of
    CLIENT_LEVEL_DIVISION:
      begin
        if VarIsNull(EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_DIVISION_NBR')) then
          raise EInconsistentData.CreateHelp('There is no home division set up for employee #' +
            EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
        CO_DIVISION.Activate;
        if not CO_DIVISION.NewLocate('CO_DIVISION_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_DIVISION_NBR'), []) then
        begin
          CO_DIVISION.DataRequired('ALL');
          CO_DIVISION.Locate('CO_DIVISION_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_DIVISION_NBR'), []);
        end;
        AddRecordToDataSet(CO_DIVISION);
      end;
    CLIENT_LEVEL_BRANCH:
      begin
        if VarIsNull(EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_BRANCH_NBR')) then
          raise EInconsistentData.CreateHelp('There is no home branch set up for employee #' +
            EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
        CO_BRANCH.Activate;
        if not CO_BRANCH.NewLocate('CO_BRANCH_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_BRANCH_NBR'), []) then
        begin
          CO_BRANCH.DataRequired('ALL');
          CO_BRANCH.Locate('CO_BRANCH_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_BRANCH_NBR'), []);
        end;
        AddRecordToDataSet(CO_BRANCH);
      end;
    CLIENT_LEVEL_DEPT:
      begin
        if VarIsNull(EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_DEPARTMENT_NBR')) then
          raise EInconsistentData.CreateHelp('There is no home department set up for employee #' +
            EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
        CO_DEPARTMENT.Activate;
        if not CO_DEPARTMENT.NewLocate('CO_DEPARTMENT_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_DEPARTMENT_NBR'), []) then
        begin
          CO_DEPARTMENT.DataRequired('ALL');
          CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_DEPARTMENT_NBR'), []);
        end;
        AddRecordToDataSet(CO_DEPARTMENT);
      end;
    CLIENT_LEVEL_TEAM:
      begin
        if VarIsNull(EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_TEAM_NBR')) then
          raise EInconsistentData.CreateHelp('There is no home team set up for employee #' +
            EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
        CO_TEAM.Activate;
        if not CO_TEAM.NewLocate('CO_TEAM_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_TEAM_NBR'), []) then
        begin
          CO_TEAM.DataRequired('ALL');
          CO_TEAM.Locate('CO_TEAM_NBR', EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_TEAM_NBR'), []);
        end;
        AddRecordToDataSet(CO_TEAM);
      end;
  end;

  if (cdstHomeDBDTFieldValues.FindField(FieldName) <> Nil) then
      Result := cdstHomeDBDTFieldValues.FieldByName(FieldName).Value;

end;

function TevPayrollCalculation.GetEEPrimaryRate(EENumber: Integer; EE, EE_RATES: TevClientDataSet; UsedByServer: Boolean): Integer;
begin
  if (not DM_EMPLOYEE.EE_RATES.Active) or (not DM_EMPLOYEE.EE_RATES.NewLocate('EE_NBR', EENumber, [])) then
    DM_EMPLOYEE.EE_RATES.DataRequired('EE_NBR=' + IntToStr(EENumber));

  try
    Result := EE_RATES.NewLookup('EE_NBR;PRIMARY_RATE', VarArrayOf([EENumber, 'Y']), 'RATE_NUMBER');
  except
    raise EInconsistentData.CreateHelp('There is no primary rate set up for employee #' +
      EE.Lookup('EE_NBR', EENumber, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
  end;
end;

function TevPayrollCalculation.RecalcRateForED(EDCodeType: string): Boolean;
begin
  Result := False;
  if (EDCodeType = ED_OEARN_OVERTIME) or (EDCodeType = ED_OEARN_WAITSTAFF_OVERTIME) or (EDCodeType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) or (EDCodeType = ED_OEARN_REGULAR)
  or (EDCodeType = ED_OEARN_STD_EARNINGS) or (EDCodeType = ED_OEARN_SICK) or (EDCodeType = ED_OERAN_VACATION)
  or (EDCodeType = ED_DED_REIMBURSEMENT) or (EDCodeType = ED_ST_EXEMPT_EARNINGS) or (EDCodeType = ED_ST_1099_R_SP_TAXED) or (EDCodeType = ED_ST_1099_R) or (EDCodeType = ED_ST_1099_DIV)
  or (EDCodeType = ED_ST_INTEREST) or (EDCodeType = ED_DED_TIMES_RATE)
  or (EDCodeType = ED_ST_SP_TAXED_EARNING)
  or (EDCodeType = ED_ST_SP_TAXED_1099_EARNINGS)
  or (EDCodeType = ED_3RD_CA_SUP_RELIGIOUS) or (EDCodeType = ED_3RD_CA_SUP_STOCKHOLDER) or (EDCodeType = ED_OEARN_UNWORKED)
  or (EDCodeType = ED_OEARN_SEVERANCE_PAY) or (EDCodeType = ED_OEARN_SALARY) or (EDCodeType = ED_ST_NON_TAXABLE_EARNING)
  or (EDCodeType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDCodeType = ED_OEARN_AVG_OVERTIME) or (EDCodeType = ED_OEARN_AVG_MULT_OVERTIME) then
    Result := True;
end;

procedure TevPayrollCalculation.FillInDBDTBasedOnRate(EERateNbr: Integer; PR_CHECK_LINES, EE_RATES: TevClientDataSet);
var
  Temp: Variant;
begin
  Temp := EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_DIVISION_NBR');
  if not VarIsNull(Temp) then
    PR_CHECK_LINES['CO_DIVISION_NBR'] := Temp;
  Temp := EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_BRANCH_NBR');
  if not VarIsNull(Temp) then
    PR_CHECK_LINES['CO_BRANCH_NBR'] := Temp;
  Temp := EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_DEPARTMENT_NBR');
  if not VarIsNull(Temp) then
    PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := Temp;
  Temp := EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_TEAM_NBR');
  if not VarIsNull(Temp) then
    PR_CHECK_LINES['CO_TEAM_NBR'] := Temp;
  Temp := EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_JOBS_NBR');
  if not VarIsNull(Temp) then
    PR_CHECK_LINES['CO_JOBS_NBR'] := Temp;
end;

procedure TevPayrollCalculation.DoRecalcRate(PR_CHECK, PR_CHECK_LINES, CL_E_DS, CO, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS, EE,
  EE_RATES: TevClientDataSet; UsedByServer: Boolean);
  
var
  EmployeeNbr, JobNbr, N: Integer;
  TempAmount, OverrideRate: Real;
  DBDTLevel, OverrideType: string;

  function GetDBDTValue(aField: String): Variant;
  begin
    Result := Null;
    with PR_CHECK_LINES do
    case DBDTLevel[1] of
      CLIENT_LEVEL_DIVISION:
        if not FieldByName('CO_DIVISION_NBR').IsNull then
        begin
          CO_DIVISION.Activate;
          if not CO_DIVISION.NewLocate('CO_DIVISION_NBR', FieldByName('CO_DIVISION_NBR').Value, []) then
          begin
            CO_DIVISION.DataRequired('ALL');
            CO_DIVISION.NewLocate('CO_DIVISION_NBR', FieldByName('CO_DIVISION_NBR').Value, []);
          end;
          Result := CO_DIVISION.FieldByName(aField).Value;
        end;
      CLIENT_LEVEL_BRANCH:
        if not FieldByName('CO_BRANCH_NBR').IsNull then
        begin
          CO_BRANCH.Activate;
          if not CO_BRANCH.NewLocate('CO_BRANCH_NBR', FieldByName('CO_BRANCH_NBR').Value, []) then
          begin
            CO_BRANCH.DataRequired('ALL');
            CO_BRANCH.NewLocate('CO_BRANCH_NBR', FieldByName('CO_BRANCH_NBR').Value, []);
          end;
          Result := CO_BRANCH.FieldByName(aField).Value;
        end;
      CLIENT_LEVEL_DEPT:
        if not FieldByName('CO_DEPARTMENT_NBR').IsNull then
        begin
          CO_DEPARTMENT.Activate;
          if not CO_DEPARTMENT.NewLocate('CO_DEPARTMENT_NBR', FieldByName('CO_DEPARTMENT_NBR').Value, []) then
          begin
            CO_DEPARTMENT.DataRequired('ALL');
            CO_DEPARTMENT.NewLocate('CO_DEPARTMENT_NBR', FieldByName('CO_DEPARTMENT_NBR').Value, []);
          end;
          Result := CO_DEPARTMENT.FieldByName(aField).Value;
        end;
      CLIENT_LEVEL_TEAM:
        if not FieldByName('CO_TEAM_NBR').IsNull then
        begin
          CO_TEAM.Activate;
          if not CO_TEAM.NewLocate('CO_TEAM_NBR', FieldByName('CO_TEAM_NBR').Value, []) then
          begin
            CO_TEAM.DataRequired('ALL');
            CO_TEAM.NewLocate('CO_TEAM_NBR', FieldByName('CO_TEAM_NBR').Value, []);
          end;
          Result := CO_TEAM.FieldByName(aField).Value;
        end;
    end;
  end;

begin
  EmployeeNbr := PR_CHECK['EE_NBR'];
  DBDTLevel := CO['DBDT_LEVEL'];
  with PR_CHECK_LINES do
  begin
    if FieldByName('RATE_NUMBER').IsNull and (not FieldByName('RATE_OF_PAY').IsNull) then // User-defined rate of pay
      Exit;
    if FieldByName('RATE_NUMBER').Value = 0 then // Memo line
      Exit;

    CL_E_DS.Activate;
    OverrideRate := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_RATE'), 0);
    OverrideType := CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_RATE_TYPE');
    if FieldByName('RATE_NUMBER').AsInteger > 0 then // User-defined rate number
    begin
      EE_RATES.Activate;
      if VarIsNull(EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr, FieldByName('RATE_NUMBER').Value]), 'RATE_AMOUNT')) then
        raise EInconsistentData.CreateHelp('Rate number ' + FieldByName('RATE_NUMBER').AsString + ' is not setup for employee #' + EE.NewLookup('EE_NBR',
          EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
      FieldByName('RATE_OF_PAY').Value := EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr,
        FieldByName('RATE_NUMBER').Value]), 'RATE_AMOUNT');
      case OverrideType[1] of
        OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT:
          FieldByName('RATE_OF_PAY').Value := FieldByName('RATE_OF_PAY').Value + OverrideRate;
        OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT:
          FieldByName('RATE_OF_PAY').Value := RoundAll(FieldByName('RATE_OF_PAY').Value * (1 + OverrideRate / 100), 6);
      end;
      FillInDBDTBasedOnRate(EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr, FieldByName('RATE_NUMBER').Value]),
        'EE_RATES_NBR'), PR_CHECK_LINES, EE_RATES);
      Exit;
    end;
    if (FieldByName('RATE_NUMBER').IsNull and FieldByName('RATE_OF_PAY').IsNull) or (FieldByName('RATE_NUMBER').AsInteger < 0) then
    // System-defined rate number
    begin
      FieldByName('RATE_NUMBER').Value := Null;
      if VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_RATE')) or (OverrideType <> OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
      begin
        if ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_EE_RATE_NUMBER'), 0) <> 0 then
        begin
          FieldByName('RATE_NUMBER').Value := CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'OVERRIDE_EE_RATE_NUMBER') * (-1);
          FieldByName('RATE_OF_PAY').Value := EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr,
            Abs(FieldByName('RATE_NUMBER').Value)]), 'RATE_AMOUNT');
        end
        else
        begin
          JobNbr := ConvertNull(FieldByName('CO_JOBS_NBR').Value, 0);
          if JobNbr = 0 then
            JobNbr := ConvertNull(EE.Lookup('EE_NBR', EmployeeNbr, 'CO_JOBS_NBR'), 0);
          CO_JOBS.Activate;
          if VarIsNull(CO_JOBS.NewLookup('CO_JOBS_NBR', JobNbr, 'RATE_PER_HOUR')) then
          begin
            TempAmount := ConvertNull(GetDBDTValue('OVERRIDE_PAY_RATE'), 0);
            if TempAmount = 0 then
              TempAmount := ConvertNull(GetHomeDBDTFieldValue('OVERRIDE_PAY_RATE', DBDTLevel, EmployeeNbr, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, UsedByServer), 0);
            if TempAmount = 0 then
            begin
              N := ConvertNull(GetDBDTValue('OVERRIDE_EE_RATE_NUMBER'), 0);
              if N = 0 then
                N := ConvertNull(GetHomeDBDTFieldValue('OVERRIDE_EE_RATE_NUMBER', DBDTLevel, EmployeeNbr, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, UsedByServer), 0);
              if N = 0 then
                FieldByName('RATE_NUMBER').Value := GetEEPrimaryRate(EmployeeNbr, EE, EE_RATES, UsedByServer) * (-1)
              else
                FieldByName('RATE_NUMBER').Value := N * (-1);
              EE_RATES.Activate;
              FieldByName('RATE_OF_PAY').Value := EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr,
                Abs(FieldByName('RATE_NUMBER').Value)]), 'RATE_AMOUNT');
            end
            else
              FieldByName('RATE_OF_PAY').Value := TempAmount;
          end
          else
            FieldByName('RATE_OF_PAY').Value := CO_JOBS.NewLookup('CO_JOBS_NBR', JobNbr, 'RATE_PER_HOUR');
        end;
        if OverrideRate <> 0 then
        begin
          case OverrideType[1] of
            OVERRIDE_VALUE_TYPE_REGULAR_PERCENT:
              FieldByName('RATE_OF_PAY').Value := RoundAll(FieldByName('RATE_OF_PAY').Value * OverrideRate / 100, 6);
            OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT:
              FieldByName('RATE_OF_PAY').Value := FieldByName('RATE_OF_PAY').Value + OverrideRate;
            OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT:
              FieldByName('RATE_OF_PAY').Value := RoundAll(FieldByName('RATE_OF_PAY').Value * (1 + OverrideRate / 100), 6);
          end;
        end;
      end
      else
      begin
        FieldByName('RATE_OF_PAY').Value := OverrideRate;
      end;
    end;
  end;
end;

function TevPayrollCalculation.HolidayDay(Day: TDateTime; BankOnly: Boolean = False): string;
var
  Q: IevQuery;
begin
  Result := '';
  Q := TevQuery.Create('select HOLIDAY_NAME from SB_HOLIDAYS where {AsOfNow<SB_HOLIDAYS>} and HOLIDAY_DATE=:HOLIDAY_DATE and USED_BY=:USED_BY');
  Q.Params.AddValue('HOLIDAY_DATE', Day);
  Q.Params.AddValue('USED_BY', 'A'); {Used By SB and Banks}
  if Q.Result.RecordCount <> 0 then
    Result := Q.Result.FieldByName('HOLIDAY_NAME').AsString
  else
  begin
    if not BankOnly then
    begin
      Q := TevQuery.Create('select HOLIDAY_NAME from SB_HOLIDAYS where {AsOfNow<SB_HOLIDAYS>} and HOLIDAY_DATE=:HOLIDAY_DATE and USED_BY=:USED_BY');
      Q.Params.AddValue('HOLIDAY_DATE', Day);
      Q.Params.AddValue('USED_BY', 'S'); {Used By SB}
      if Q.Result.RecordCount <> 0 then
        Result := Q.Result.FieldByName('HOLIDAY_NAME').AsString;
    end
    else
    begin
      Q := TevQuery.Create('select HOLIDAY_NAME from SB_HOLIDAYS where {AsOfNow<SB_HOLIDAYS>} and HOLIDAY_DATE=:HOLIDAY_DATE and USED_BY=:USED_BY');
      Q.Params.AddValue('HOLIDAY_DATE', Day);
      Q.Params.AddValue('USED_BY', 'B');
      if Q.Result.RecordCount <> 0 then
        Result := Q.Result.FieldByName('HOLIDAY_NAME').AsString;
    end
  end;
end;

function TevPayrollCalculation.GetFutureMonthDate(MyDate: TDateTime; N: Word): TDateTime;
var
  Year, Month, Day, NN: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  NN := Month + N - 1;
  Year := Year + (NN div 12);
  Month := (NN mod 12) + 1;
  if (Month in [4, 6, 9, 11]) and (Day > 30) then Day := 30;
  if (Month = 2) and (Day > 28) then Day := 28;
  Result := EncodeDate(Year, Month, Day);
end;

function TevPayrollCalculation.GetNextDate(MyDate: TDateTime; N: Word): TDateTime;
var
  Year, Month, Day: Word;
begin
  DecodeDate(MyDate, Year, Month, Day);
  if Day >= N then Inc(Month);
  if Month > 12 then
  begin
    Inc(Year);
    Month := 1;
  end;
  Day := N;
  if (Month in [4, 6, 9, 11]) and (Day > 30) then Day := 30;
  if (Month = 2) and (Day > 28) then Day := 28;
  Result := EncodeDate(Year, Month, Day);
end;

// 4-20

function TevPayrollCalculation.NextCheckDate(AfterDate: TDateTime; WithNoPayrollAttached: Boolean = False): TDateTime;
begin
  Result := GetNextCheckDate(AfterDate, GetCompanyFrequencies(DM_COMPANY.CO), WithNoPayrollAttached);
end;

function TevPayrollCalculation.GetNextCheckDate(AfterDate: TDateTime; CompanyFrequency: Char; WithNoPayrollAttached: Boolean = False): TDateTime;
begin
  DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  with DM_PAYROLL.PR_SCHEDULED_EVENT do
  begin
    IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
    First;
    if WithNoPayrollAttached then
    begin
      while (not EOF) and ((FieldByName('SCHEDULED_CHECK_DATE').AsDateTime < AfterDate) or (not FieldByName('PR_NBR').IsNull)) do
        Next;
      if EOF then
      begin
        Result := 0;
        Exit;
        {raise ENoNextCheckDate.CreateHelp('There are no available entries in the calendar after ' + DateToStr(AfterDate) +
          '. Please, update it.', IDH_InconsistentData);}
      end;
    end
    else
    begin
      while (not EOF) and (FieldByName('SCHEDULED_CHECK_DATE').AsDateTime <= AfterDate) do
        Next;
      if EOF then
      begin
        Result := 0;
        Exit;
//        raise ENoNextCheckDate.CreateHelp('There are no entries in the calendar after ' + DateToStr(AfterDate) + '. Please, update it.', IDH_InconsistentData);
      end;
    end;
    Result := FieldByName('SCHEDULED_CHECK_DATE').AsDateTime;
    IndexFieldNames := '';
  end;
end;

function TevPayrollCalculation.NextScheduledCallInDate(AfterDate: TDateTime; WithNoPayrollAttached: Boolean = False): TDateTime;
begin
  with DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH do
  begin
    if Active and (DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.ClientID = ctx_DataAccess.ClientID)
    and (OpenCondition = 'CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString) then
    else
    begin
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
      begin
        SetMacro('Columns', 'T1.*');
        SetMacro('TABLE1', 'PR_SCHEDULED_EVENT_BATCH');
        SetMacro('TABLE2', 'PR_SCHEDULED_EVENT');
        SetMacro('NBRFIELD', 'CO');
        SetMacro('JOINFIELD', 'PR_SCHEDULED_EVENT');
        SetParam('RecordNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.ClientID := ctx_DataAccess.ClientID;
      Data := ctx_DataAccess.CUSTOM_VIEW.Data;
      SetOpenCondition('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    end;

    IndexFieldNames := 'SCHEDULED_CALL_IN_DATE;PR_SCHEDULED_EVENT_BATCH_NBR';
    First;
    if WithNoPayrollAttached then
    begin
      while (not EOF) and ((FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime < AfterDate) or (not FieldByName('PR_NBR').IsNull)) do
        Next;
    end
    else
    begin
      while (not EOF) and (FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime <= AfterDate) do
        Next;
    end;
    if EOF then
      Result := 0
    else
      Result := FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime;
    IndexFieldNames := '';
  end;
end;

function TevPayrollCalculation.NextPeriodBeginDate: TDateTime;
begin
  Result := GetNextPeriodBeginDate(DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').AsString[1]);
end;

function TevPayrollCalculation.GetNextPeriodBeginDate(BatchFrequency: Char): TDateTime;
var
  I: Integer;
begin
  Result := Date;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;
    with TExecDSWrapper.Create('GetNextPeriodDate') do
    begin
      SetMacro('Edge', 'BEGIN');
      SetParam('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      SetParam('BatchFrequency', BatchFrequency);
      DataRequest(AsVariant);
    end;
    Open;
    Last;
    case BatchFrequency of
      FREQUENCY_TYPE_DAILY:
        begin
          if not VarIsNull(FieldByName('PERIOD_BEGIN_DATE').Value) then
          begin
            I := 1;
            while FieldByName('PERIOD_BEGIN_DATE_STATUS').Value <> DATE_STATUS_NORMAL do
            begin
              Inc(I);
              Prior;
              if BOF then Break;
            end;
            Result := FieldByName('PERIOD_BEGIN_DATE').Value + I;
            if DayOfWeek(Result) = 7 then
              Result := Result + 2;
            if DayOfWeek(Result) = 1 then
              Result := Result + 1;
          end;
        end;
      FREQUENCY_TYPE_WEEKLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_BEGIN_DATE').Value) then
          begin
            I := 1;
            while FieldByName('PERIOD_BEGIN_DATE_STATUS').Value <> DATE_STATUS_NORMAL do
            begin
              Inc(I);
              Prior;
              if BOF then Break;
            end;
            Result := FieldByName('PERIOD_BEGIN_DATE').Value + 7 * I;
          end;
        end;
      FREQUENCY_TYPE_BIWEEKLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_BEGIN_DATE').Value) then
          begin
            I := 1;
            while FieldByName('PERIOD_BEGIN_DATE_STATUS').Value <> DATE_STATUS_NORMAL do
            begin
              Inc(I);
              Prior;
              if BOF then Break;
            end;
            Result := FieldByName('PERIOD_BEGIN_DATE').Value + 14 * I;
          end;
        end;
      FREQUENCY_TYPE_SEMI_MONTHLY:
        begin
          Prior;
          if not VarIsNull(FieldByName('PERIOD_BEGIN_DATE').Value) then
            Result := GetFutureMonthDate(FieldByName('PERIOD_BEGIN_DATE').Value, 1);
        end;
      FREQUENCY_TYPE_MONTHLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_BEGIN_DATE').Value) then
            Result := GetFutureMonthDate(FieldByName('PERIOD_BEGIN_DATE').Value, 1);
        end;
      FREQUENCY_TYPE_QUARTERLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_BEGIN_DATE').Value) then
            Result := GetFutureMonthDate(FieldByName('PERIOD_BEGIN_DATE').Value, 3);
        end;
    end;
    Close;
  end;
end;

function TevPayrollCalculation.NextPeriodEndDate: TDateTime;
begin
  Result := GetNextPeriodEndDate(DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').AsString[1],
    DM_PAYROLL.PR_BATCH['PERIOD_BEGIN_DATE']);
end;

function TevPayrollCalculation.GetNextPeriodEndDate(BatchFrequency: Char; PeriodBeginDate: TDateTime): TDateTime;
var
  I: Integer;
begin
  Result := Date;
  case BatchFrequency of
    FREQUENCY_TYPE_DAILY:
      Result := PeriodBeginDate + 1;
    FREQUENCY_TYPE_WEEKLY:
      Result := PeriodBeginDate + 7;
    FREQUENCY_TYPE_BIWEEKLY:
      Result := PeriodBeginDate + 14;
    FREQUENCY_TYPE_SEMI_MONTHLY:
      Result := PeriodBeginDate + 15;
    FREQUENCY_TYPE_MONTHLY:
      Result := GetFutureMonthDate(PeriodBeginDate, 1);
    FREQUENCY_TYPE_QUARTERLY:
      Result := GetFutureMonthDate(PeriodBeginDate, 3);
  end;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;
    with TExecDSWrapper.Create('GetNextPeriodDate') do
    begin
      SetMacro('Edge', 'END');
      SetParam('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      SetParam('BatchFrequency', BatchFrequency);
      DataRequest(AsVariant);
    end;
    Open;
    Last;
    case BatchFrequency of
      FREQUENCY_TYPE_DAILY:
        begin
          if not VarIsNull(FieldByName('PERIOD_END_DATE').Value) then
          begin
            I := 1;
            while FieldByName('PERIOD_END_DATE_STATUS').Value <> DATE_STATUS_NORMAL do
            begin
              Inc(I);
              Prior;
              if BOF then Break;
            end;
            Result := FieldByName('PERIOD_END_DATE').Value + I;
            if DayOfWeek(Result) = 7 then
              Result := Result + 2;
            if DayOfWeek(Result) = 1 then
              Result := Result + 1;
          end;
        end;
      FREQUENCY_TYPE_WEEKLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_END_DATE').Value) then
          begin
            I := 1;
            while FieldByName('PERIOD_END_DATE_STATUS').Value <> DATE_STATUS_NORMAL do
            begin
              Inc(I);
              Prior;
              if BOF then Break;
            end;
            Result := FieldByName('PERIOD_END_DATE').Value + 7 * I;
          end;
        end;
      FREQUENCY_TYPE_BIWEEKLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_END_DATE').Value) then
          begin
            I := 1;
            while FieldByName('PERIOD_END_DATE_STATUS').Value <> DATE_STATUS_NORMAL do
            begin
              Inc(I);
              Prior;
              if BOF then Break;
            end;
            Result := FieldByName('PERIOD_END_DATE').Value + 14 * I;
          end;
        end;
      FREQUENCY_TYPE_SEMI_MONTHLY:
        begin
          Prior;
          if not VarIsNull(FieldByName('PERIOD_END_DATE').Value) then
            Result := GetFutureMonthDate(FieldByName('PERIOD_END_DATE').Value, 1);
        end;
      FREQUENCY_TYPE_MONTHLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_END_DATE').Value) then
            Result := GetFutureMonthDate(FieldByName('PERIOD_END_DATE').Value, 1);
        end;
      FREQUENCY_TYPE_QUARTERLY:
        begin
          if not VarIsNull(FieldByName('PERIOD_END_DATE').Value) then
            Result := GetFutureMonthDate(FieldByName('PERIOD_END_DATE').Value, 3);
        end;
    end;
    Close;
  end;
end;

procedure TevPayrollCalculation.GetFreqListForCheckDate(CheckDate: TDateTime; FreqList: TStringList);
var
  CompanyFrequency: Char;
begin
  CompanyFrequency := GetCompanyFrequencies(DM_COMPANY.CO);
  FreqList.Clear;

  if CompanyFrequency = CO_FREQ_WEEKLY then
  begin
    FreqList.Add(FREQUENCY_TYPE_WEEKLY);
    Exit;
  end;

  if CompanyFrequency = CO_FREQ_DAILY then
  begin
    FreqList.Add(FREQUENCY_TYPE_DAILY);
    Exit;
  end;

  if CompanyFrequency = CO_FREQ_BWEEKLY then
  begin
    FreqList.Add(FREQUENCY_TYPE_BIWEEKLY);
    Exit;
  end;

  if CompanyFrequency in [CO_FREQ_WEEKLY_BWEEKLY, CO_FREQ_WEEKLY_SMONTHLY, CO_FREQ_WEEKLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
  begin
    FreqList.Add(FREQUENCY_TYPE_WEEKLY);
  end;

  if CompanyFrequency in [CO_FREQ_WEEKLY_BWEEKLY, CO_FREQ_BWEEKLY_SMONTHLY, CO_FREQ_BWEEKLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
  begin
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;
// See if we had bi-weekly batch in a prior scheduled payroll
      with TExecDSWrapper.Create('CountBiWeeklyBatchs') do
      begin
        SetParam('Frequency', FREQUENCY_TYPE_BIWEEKLY);
        SetParam('CheckDate', CheckDate);
        DataRequest(AsVariant);
      end;
      Open;
      if FieldByName('MY_COUNT').AsInteger = 0 then
      begin
        FreqList.Add(FREQUENCY_TYPE_BIWEEKLY);
      end;
    end;
  end;

  if CompanyFrequency in [CO_FREQ_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY, CO_FREQ_BWEEKLY_SMONTHLY, CO_FREQ_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
  begin
    try
      if GetDay(CheckDate) = DM_COMPANY.CO['FIRST_MONTHLY_PAYROLL_DAY'] then
      begin
        FreqList.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
      end;
    except
      raise ENoPayrollDay.CreateHelp('Your company is semi-monthly, but "First Monthly Payroll Day" is not setup!', IDH_InconsistentData);
    end;
    if FreqList.IndexOf(FREQUENCY_TYPE_SEMI_MONTHLY) = -1 then
    begin
      try
        if GetDay(CheckDate) = DM_COMPANY.CO['SECOND_MONTHLY_PAYROLL_DAY'] then
        begin
          FreqList.Add(FREQUENCY_TYPE_SEMI_MONTHLY);
        end;
      except
        raise ENoPayrollDay.CreateHelp('Your company is semi-monthly, but "Second Monthly Payroll Day" is not setup!', IDH_InconsistentData);
      end;
    end;
  end;

  if CompanyFrequency in [CO_FREQ_MONTHLY, CO_FREQ_WEEKLY_MONTHLY, CO_FREQ_BWEEKLY_MONTHLY, CO_FREQ_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
  begin
    try
      if GetDay(CheckDate) = DM_COMPANY.CO['FIRST_MONTHLY_PAYROLL_DAY'] then
      begin
        FreqList.Add(FREQUENCY_TYPE_MONTHLY);
      end;
    except
      raise ENoPayrollDay.CreateHelp('Your company is monthly, but "First Monthly Payroll Day" is not setup!', IDH_InconsistentData);
    end;
  end;
end;

// SR-52



procedure TevPayrollCalculation.CalculateOvertime(DataSet: TevClientDataSet; EDType: string; EE_NBR: Integer);
var
  EDGroupNbr, RateNbr: Integer;
  Sum, Hours, BaseRate, RegRate, OTMinWage, RateOfPay, HoursOrPieces, RegularOtRate, Amount: Real;
  OvertimeMult: Double;
  EE_STATES_NBR, CO_STATES_NBR: Variant;
  RoundOtCalculation: Boolean;
begin
  if DataSet.FieldByName('RATE_OF_PAY').IsNull and DataSet.FieldByName('HOURS_OR_PIECES').IsNull and not DataSet.FieldByName('AMOUNT').IsNull then
    Exit;
  Sum := 0;
  Hours := 0;
  EE_STATES_NBR := DataSet['EE_SUI_STATES_NBR'];
  if VarIsNull(EE_STATES_NBR) then
  begin
    if not DM_EMPLOYEE.EE_RATES.Active then
      DM_EMPLOYEE.EE_RATES.DataRequired('');

    DM_EMPLOYEE.EE_STATES.DataRequired('');

    EE_STATES_NBR := DM_EMPLOYEE.EE.NewLookup('EE_NBR', EE_NBR, 'HOME_TAX_EE_STATES_NBR');
    if VarIsNull(EE_STATES_NBR) then
      raise EInconsistentData.CreateHelp('There is no home state set up for employee #' +
        DM_EMPLOYEE.EE.NewLookup('EE_NBR', EE_NBR, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
    CO_STATES_NBR := DM_EMPLOYEE.EE_STATES.NewLookup('EE_STATES_NBR', EE_STATES_NBR, 'SUI_APPLY_CO_STATES_NBR');
    EE_STATES_NBR := DM_EMPLOYEE.EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EE_NBR, CO_STATES_NBR]), 'EE_STATES_NBR');
  end;

  if not VarIsNull(EE_STATES_NBR) then
    OTMinWage := GetMinimumWage(EE_STATES_NBR, Null)
  else
    OTMinWage := GetCoMinimumWage(CO_STATES_NBR, Null);

  RateOfPay := ConvertNull(DataSet.FieldByName('RATE_OF_PAY').Value, 0);
  HoursOrPieces := ConvertNull(DataSet.FieldByName('HOURS_OR_PIECES').Value, 0);
  RegularOtRate := ConvertNull(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'REGULAR_OT_RATE'), 0);

  RoundOtCalculation := (DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'ROUND_OT_CALCULATION') = 'Y') and
    ((EDType = ED_OEARN_OVERTIME) or (EDType = ED_OEARN_WAITSTAFF_OVERTIME) or (EDType = ED_OEARN_WEIGHTED_HALF_TIME_OT)
      or (EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDType = ED_OEARN_AVG_OVERTIME));

  if (EDType = ED_OEARN_OVERTIME) or ((EDType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) and (DataSet['RATE_OF_PAY'] >= OTMinWage)) then
  begin
    if VarIsNull(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'REGULAR_OT_RATE')) then
      raise EInconsistentData.CreateHelp('Overtime rate multiplier is missing for ' + DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR',
        DataSet['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
    if RoundOtCalculation then
      Amount := RoundTwo(RateOfPay * RegularOtRate) * HoursOrPieces
    else
      Amount := RateOfPay * RegularOtRate * HoursOrPieces;
    DataSet['AMOUNT'] := Amount;
  end;
  if (EDType = ED_OEARN_WAITSTAFF_OVERTIME) or ((EDType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) and (DataSet['RATE_OF_PAY'] < OTMinWage)) then
  begin
    ApplyTempCheckLinesCheckRange(DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
    if RoundOtCalculation then
      Amount := RoundTwo(0.5 * OTMinWage + RateOfPay - Sum) * HoursOrPieces
    else
      Amount := (0.5 * OTMinWage + RateOfPay - Sum) * HoursOrPieces;
    DataSet['AMOUNT'] := Amount;
  end;
  if (EDType = ED_OEARN_WEIGHTED_HALF_TIME_OT) or (EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDType = ED_OEARN_AVG_OVERTIME) then
  begin
    DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
    try
      EDGroupNbr := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'OT_ALL_CL_E_D_GROUPS_NBR');
    except
      raise EInconsistentData.CreateHelp('E/D Group for Overtime Calculation is not setup for employee #' + DM_EMPLOYEE.EE.NewLookup('EE_NBR',
        DM_PAYROLL.PR_CHECK['EE_NBR'], 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
    end;
    ApplyTempCheckLinesCheckRange(DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
    cdstTempCheckLines.First;
    while not cdstTempCheckLines.EOF do
    begin
      if (not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR',
      VarArrayOf([EDGroupNbr, cdstTempCheckLines['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')))
      and (cdstTempCheckLines['PR_CHECK_LINES_NBR'] <> DataSet.FieldByName('PR_CHECK_LINES_NBR').Value) then
      begin
        Hours := Hours + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0);
// DC-219        Sum := Sum + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);

//////////// DC-219 old
        if EDType = ED_OEARN_AVG_OVERTIME then
        begin
          if ConvertNull(GetTempCheckLinesFieldValue('RATE_OF_PAY'), 0) <> 0 then
            BaseRate := ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * OTMinWage
          else
            BaseRate := 0;
          if BaseRate < ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0) then
            BaseRate := ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
          Sum := Sum + BaseRate;
        end
        else
          Sum := Sum + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
//////////// DC-219 old          
      end;
      cdstTempCheckLines.Next;
    end;

    if EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT then
      Hours := Hours + ConvertNull(DataSet['HOURS_OR_PIECES'], 0);
    if Hours = 0 then
      DataSet['AMOUNT'] := 0
    else
    begin
      BaseRate := 0;
      if (EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDType = ED_OEARN_AVG_OVERTIME) then
      begin
        DM_CLIENT.CL_E_DS.Activate;
        cdstTempCheckLines.First;
        while not cdstTempCheckLines.EOF do
        begin
          if (DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_SALARY)
          and (not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR',
          VarArrayOf([EDGroupNbr, cdstTempCheckLines['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR'))) then
            BaseRate := BaseRate + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
          cdstTempCheckLines.Next;
        end;
        if BaseRate <> 0 then
          BaseRate := 0
        else
        begin
          BaseRate := ConvertNull(DataSet['RATE_OF_PAY'], 0);
          if BaseRate = 0 then
          begin
            RateNbr := Abs(VarToInt(ConvertNull(DataSet['RATE_NUMBER'], 0)));
            if RateNbr = 0 then
              RateNbr := GetEEPrimaryRate(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_RATES, True);
            BaseRate := DM_EMPLOYEE.EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'], RateNbr]), 'RATE_AMOUNT');
          end;
        end;
      end;
      if EDType = ED_OEARN_AVG_OVERTIME then
      begin
        if DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'OVERSTATE_HOURS_FOR_OVERTIME') = 'N' then
          Hours := Hours + ConvertNull(DataSet['HOURS_OR_PIECES'], 0);
        if VarIsNull(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'REGULAR_OT_RATE')) then
          raise EInconsistentData.CreateHelp('Overtime rate multiplier is missing for ' + DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR',
            DataSet['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
        OvertimeMult := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'REGULAR_OT_RATE');
        if OvertimeMult > 1.0000001 then
          OvertimeMult := OvertimeMult - 1
        else
          BaseRate := 0;
        if Hours = 0 then
          DataSet['AMOUNT'] := 0
        else
        begin
          if (OTMinWage > BaseRate) and (BaseRate <> 0) then
            RegRate := OTMinWage
          else
            RegRate := BaseRate;
          RegRate := RoundTwo((Sum + ConvertNull(DataSet['HOURS_OR_PIECES'], 0) * RegRate) / Hours);
          if OTMinWage > RegRate then
            RegRate := OTMinWage;
          if RoundOtCalculation then
            DataSet['AMOUNT'] := RoundTwo(RoundTwo(RegRate * OvertimeMult) + BaseRate) * HoursOrPieces
          else
            DataSet['AMOUNT'] := (RoundTwo(RegRate * OvertimeMult) + BaseRate) * HoursOrPieces;
        end;
      end
      else
      begin
        if RoundOtCalculation then
          DataSet['AMOUNT'] := RoundTwo(0.5 * (Sum + HoursOrPieces * BaseRate) / Hours + BaseRate) * HoursOrPieces
        else
          DataSet['AMOUNT'] := (0.5 * (Sum + HoursOrPieces * BaseRate) / Hours + BaseRate) * HoursOrPieces;
      end;
    end;
  end;

  if RoundOtCalculation then
    DataSet['AMOUNT'] := RoundTwo(DataSet['AMOUNT']);

end;

// SR-20.10

function TevPayrollCalculation.ScheduledEDRecalc(EDType: string; Gross, Net: Real; PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
  PR_CHECK_LOCALS, CL_E_DS, CL_PENSION, CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE,
  SY_STATES: TevClientDataSet; JustPreProcessing: Boolean = True; ProrateNbr: Integer = 1; ProrateRatio: Real = 1; OldNet: Real = 0): Boolean;
var
  EmployeeNbr, EERateNbr, PensionNbr, EEStatesNbr, EarnCodeGroupNbr, PrCount, RecNumber, SchedGroupNbr: Integer;
  YTD, YTDHours, PensionLimit, TempAmount, TempAmount2, TempPercentage, CalcPercentage, SDITaxLimit, MinWage, MinWageMult, MaxGarnPercent, ScheduledGroupAmount: Real;
  PensAnnualMaxAmount, PensMatch1Amount, PensMatch1Percent, PensMatch2Amount,
  PensMatch2Percent, TotalPensionAmount, TotalMatchAmount, SchedGroupAmount,
  PayPeriodMinimum, PayPeriodMaximum, PensionEarnings, FlatMatchAmount, Earnings: Real;
  CalcType, CodeType, CodeType2, EDType2: String;
  EEStateNbr, COSUINbr, WorkCompNbr, TempNbr: Variant;
  CheckDate, PriorCheckDate, NextCheckDate, LastPaidCheckDate: TDateTime;
  FundType: Char;
  EDList: TStringList;
  EELineFound, HasChildSupport, ReachedLimit: Boolean;
  Temp, AnnualMaximum: Real;
  EDGroupTemp: Integer;
  EDCodeTemp, EDCodeCurrent: String;
  Q: IevQuery;
  Pensions: IevDataset;

  function NYChildSupportEarnCodeGroupCalc(GroupNbr: Variant; PR_CHECK: TevClientDataSet): Double;
  var
  EDCodeType: String;
  begin
    Result := 0;
    if VarIsNull(GroupNbr) then
      Exit;
    DM_CLIENT.CL_E_D_GROUPS.Activate;
    DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
    DM_CLIENT.CL_E_DS.Activate;
    ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
    with cdstTempCheckLines do
    begin
      First;
      while not EOF do
      begin
        if DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([GroupNbr, FieldByName('CL_E_DS_NBR').Value]), []) then
        begin
          if DM_CLIENT.CL_E_D_GROUPS.NewLookup('CL_E_D_GROUPS_NBR', GroupNbr, 'SUBTRACT_DEDUCTIONS') = GROUP_BOX_NO then
          begin
            Result := Result + ConvertNull(FieldByName('AMOUNT').Value, 0)
          end
          else
          begin
            EDCodeType := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
            if (EDCodeType[1] = 'D') and (ConvertNull(FieldByName('AMOUNT').Value, 0) = 0) then
              Result := Result + ConvertDeduction(EDCodeType, ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR;CL_E_DS_NBR',
                                                              VarArrayOf([FieldByName('EE_SCHEDULED_E_DS_NBR').Value,
                                                              FieldByName('CL_E_DS_NBR').Value]), 'Amount'), 0))
            else
              Result := Result + ConvertDeduction(EDCodeType, ConvertNull(FieldByName('AMOUNT').Value, 0))
          end;
        end;
        Next;
      end;
    end;
  end;

  function DisposableEarnings(EDGroupNbr: Variant; IsNYChildSupport:boolean=false): Real;
{  var - Commented out per Reso #35356
    PR_CHECK_LINES_2: TevClientDataSet;
    EDType: string;}
  var
    SySuiNbr: Integer;
    RC: String;
  begin
    if VarIsNull(EDGroupNbr) then
      Result := ConvertNull(PR_CHECK['GROSS_WAGES'], 0)
    else begin
      if IsNYChildSupport then
        Result := NYChildSupportEarnCodeGroupCalc(EDGroupNbr,PR_CHECK)
      else
        Result := EarnCodeGroupCalc(EDGroupNbr, True, PR_CHECK);
    end;

// Subtract taxes
    Result := Result - ConvertNull(PR_CHECK['FEDERAL_TAX'], 0) - ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAX').Value, 0)
    - ConvertNull(PR_CHECK['EE_MEDICARE_TAX'], 0) + ConvertNull(PR_CHECK.FieldByName('EE_EIC_TAX').Value, 0);
    PR_CHECK_STATES.Activate;
    PR_CHECK_STATES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
    PR_CHECK_STATES.Filtered := True;
    while not PR_CHECK_STATES.EOF do
    begin
      Result := Result - ConvertNull(PR_CHECK_STATES['STATE_TAX'], 0) -
        ConvertNull(PR_CHECK_STATES['EE_SDI_TAX'], 0);
      PR_CHECK_STATES.Next;
    end;
    PR_CHECK_STATES.Filtered := False;
    PR_CHECK_SUI.Activate;
    RC := DM_COMPANY.CO_SUI.RetrieveCondition;
    if RC = '' then
      RC := 'CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString;
    DM_COMPANY.CO_SUI.Close;
    DM_COMPANY.CO_SUI.DataRequired(RC);
    DM_SYSTEM_STATE.SY_SUI.Activate;
    PR_CHECK_SUI.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
    PR_CHECK_SUI.Filtered := True;
    while not PR_CHECK_SUI.EOF do
    begin
      SySuiNbr := DM_COMPANY.CO_SUI.Lookup('CO_SUI_NBR', PR_CHECK_SUI['CO_SUI_NBR'], 'SY_SUI_NBR');
      if not Is_ER_SUI(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', SySuiNbr, 'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) then
        Result := Result - ConvertNull(PR_CHECK_SUI['SUI_TAX'], 0);
      PR_CHECK_SUI.Next;
    end;
    PR_CHECK_SUI.Filtered := False;
    PR_CHECK_LOCALS.Activate;
    PR_CHECK_LOCALS.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
    PR_CHECK_LOCALS.Filtered := True;
    DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
    DM_EMPLOYEE.EE_LOCALS.Activate;
    while not PR_CHECK_LOCALS.EOF do
    begin
      if DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR',
      DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS.Lookup('EE_LOCALS_NBR',
      PR_CHECK_LOCALS['EE_LOCALS_NBR'], 'CO_LOCAL_TAX_NBR'), 'SY_LOCALS_NBR'), 'TAX_TYPE') = GROUP_BOX_EE then
        Result := Result - ConvertNull(PR_CHECK_LOCALS['LOCAL_TAX'], 0);
      PR_CHECK_LOCALS.Next;
    end;
    PR_CHECK_LOCALS.Filtered := False;
  end;

  function ScheduledGroupCalc(GroupNbr: Variant): Double;
  begin
    Result := ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
    if VarIsNull(GroupNbr) then
      Exit;
    ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
    with cdstTempCheckLines do
    begin
{      if Filter <> 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString then
        Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
      if not Filtered then
        Filtered := True;}
      First;
      while not EOF do
      begin
        if (EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, 'SCHEDULED_E_D_GROUPS_NBR') = GroupNbr)
        and (PR_CHECK_LINES['PR_CHECK_LINES_NBR'] <> FieldByName('PR_CHECK_LINES_NBR').Value) then
          Result := Result + ConvertNull(FieldByName('AMOUNT').Value, 0);
        Next;
      end;
    end;
  end;

  function GetPercentage: Double;
  begin
    Result := GetFixedAmount(EE_SCHEDULED_E_DS, PR, EE_SCHEDULED_E_DS['PERCENTAGE']);
  end;

  function GetPensionLimit(PensCodeType: String): Double;
  begin
    Result := 0;
    SY_FED_TAX_TABLE.ProviderName := 'SY_FED_TAX_TABLE_PROV';
    SY_FED_TAX_TABLE.Activate;
    if (PensCodeType = ED_ST_401K) or (PensCodeType = ED_ST_ROTH_401K) or (PensCodeType = ED_ST_401K_CATCH_UP) then
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_401K_LIMIT'), 0);
    if (PensCodeType = ED_ST_ROTH_IRA) or (PensCodeType = ED_ST_ROTH_CATCH_UP) then
    begin
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'ROTH_IRA_LIMIT'), 0);
      if not DM_CLIENT.CL_PERSON.Active then
        DM_CLIENT.CL_PERSON.Activate;
      if not DM_EMPLOYEE.EE.Active then
        DM_EMPLOYEE.EE.Activate;
      DM_EMPLOYEE.EE.Locate('EE_NBR', PR_CHECK['EE_NBR'], []);  
      if VarIsNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.CL_PERSON_NBR.Value, 'BIRTH_DATE')) then
      raise ENoBirthDate.CreateHelp('Birth date is not defined for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString)
        + '. RothIRA can not be calculated.', IDH_InconsistentData);
      if 50 <= CalculateAge(PR['CHECK_DATE'], DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').Value, 'BIRTH_DATE'), True) then
        Result := Result + ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_IRA_CATCHUP_LIMIT'), 0);
    end;
    if (PensCodeType = ED_ST_403B) or (PensCodeType = ED_ST_ROTH_403B) or (PensCodeType = ED_ST_403B_CATCH_UP) then
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_403B_LIMIT'), 0);
    if (PensCodeType = ED_ST_457) or (PensCodeType = ED_ST_457_CATCH_UP) then
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_457_LIMIT'), 0);
    if (PensCodeType = ED_ST_501C) or (PensCodeType = ED_ST_501C_CATCH_UP) then
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_501C_LIMIT'), 0);
    if (PensCodeType = ED_ST_SIMPLE) or (PensCodeType = ED_ST_SIMPLE_CATCH_UP) then
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SIMPLE_LIMIT'), 0);
    if PensCodeType = ED_ST_SEP then
      Result := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SEP_LIMIT'), 0);
  end;

  function GetNYChildSupportAmount : double;
  var qry, qryArrear : IevQuery;
      DisposableIncome, MaximumWithholdingAmount,TotalAmountToHold, TotalArrears, DependenthealthAmount, RemainAmt : Double;


    function ScheduledGroupCalcForNYChildSupport(GroupNbr: Variant): Double;
    begin
      Result := 0;
      if VarIsNull(GroupNbr) then
      begin
         Result := ConvertNull(EE_SCHEDULED_E_DS['Amount'], 0);
         Exit;
      end;

      DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
      ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
      with cdstTempCheckLines do
      begin
        First;
        while not EOF do
        begin
          if DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([GroupNbr, FieldByName('CL_E_DS_NBR').Value]), [])
          then Result := Result + ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR;CL_E_DS_NBR;CALCULATION_TYPE', VarArrayOf([FieldByName('EE_SCHEDULED_E_DS_NBR').Value, FieldByName('CL_E_DS_NBR').Value, CALC_METHOD_NYCHILDSUPPORT]), 'Amount'), 0);
          Next;
        end;
      end;
    end;

    function GetProratedArrears : Double;
    var
      sNBR : String;
    begin
      Result := 0;

      if ( TotalArrears = 0) or (RemainAmt = 0) then exit;
      Result := ConvertNull(EE_SCHEDULED_E_DS['AMOUNT'],0);
      sNBR := ConvertNull(EE_SCHEDULED_E_DS['EE_CHILD_SUPPORT_CASES_NBR'],'');
      if sNBR = '' then exit;

      qry := TevQuery.Create('SELECT ARREARS_AMOUNT FROM EE_CHILD_SUPPORT_CASES WHERE {AsOfNow<EE_CHILD_SUPPORT_CASES>} ' +
                             ' AND EE_CHILD_SUPPORT_CASES_NBR =' + sNBR);
      qry.Execute;
      if not qry.Result.vclDataSet.IsEmpty then
        Result :=  Result + RoundAll((ConvertNull(qry.Result['ARREARS_AMOUNT'],0)/TotalArrears),6) * RemainAmt;
    end;
    
  begin
    DisposableIncome :=  DisposableEarnings(EE_SCHEDULED_E_DS['Max_PPP_CL_E_D_Groups_Nbr'], true);
    MaximumWithholdingAmount := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['MAXIMUM_PAY_PERIOD_PERCENTAGE'],0) * DisposableIncome * 0.01);

    DependenthealthAmount := 0;
    qry := TevQuery.Create('select distinct (Amount) Amt from EE_SCHEDULED_E_DS'#13 +
                           'where EE_NBR = :EENBR AND {AsOfNow<EE_SCHEDULED_E_DS>} '#13 +
                           'and CL_E_DS_NBR in ( SELECT DEPENDENT_HEALTH_CL_E_DS_NBR FROM EE_CHILD_SUPPORT_CASES ec'#13 +
                                                'join EE_SCHEDULED_E_DS eds on eds.ee_child_support_cases_nbr = ec.ee_child_support_cases_nbr'#13 +
                                                'WHERE {AsOfNow<ec>} and {AsOfNow<eds>} '#13 +
                                                'and eds.calculation_type = ''U'' and eds.ee_nbr = :EENBR )');
    qry.Params.AddValue('EENBR',PR_CHECK.FieldByName('EE_NBR').AsInteger);
    qry.Execute;
    while not qry.Result.Eof do begin
      DependenthealthAmount := DependenthealthAmount +  convertNull(qry.Result['Amt'],0);
      qry.Result.Next;
    end;

    TotalArrears := 0;
    qryArrear := TevQuery.Create(' SELECT ec.ARREARS_AMOUNT, eds.EE_SCHEDULED_E_DS_NBR  FROM EE_CHILD_SUPPORT_CASES ec'#13 +
                                 ' join EE_SCHEDULED_E_DS eds on eds.ee_child_support_cases_nbr = ec.ee_child_support_cases_nbr'#13 +
                                 ' WHERE {AsOfNow<ec>} and {AsOfNow<eds>} '#13 +
                                 ' and eds.calculation_type = ''U'' and eds.ee_nbr = :EENBR');

    qryArrear.Params.AddValue('EENBR',PR_CHECK.FieldByName('EE_NBR').AsInteger);
    qryArrear.Execute;
    while not qryArrear.Result.Eof do
    begin
      TotalArrears :=  TotalArrears + ConvertNull(qryArrear.Result['ARREARS_AMOUNT'],0);
      qryArrear.Result.next;
    end;

    TotalAmountToHold := RoundTwo(ScheduledGroupCalcForNYChildSupport(EE_SCHEDULED_E_DS['Scheduled_E_D_Groups_Nbr']));

    if ( (DependenthealthAmount = 0) and  (TotalArrears = 0)) then
    begin
      if TotalAmountToHold <= MaximumWithholdingAmount then
        Result := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['Amount'], 0))
      else
      begin
        if ProrateRatio > 0 then
          Result := RoundTwo(MaximumWithholdingAmount * ProrateRatio)
        else // set up is not correct (if exceed MaximumWithholdingAmount, should have a ProrateRatio to distribute )
          Result := 0;
      end;
    end
    else begin
      if ((TotalAmountToHold + DependenthealthAmount +  TotalArrears) <= MaximumWithholdingAmount )then
      begin
        if ( (TotalArrears <> 0) and
            qryArrear.Result.Locate('EE_SCHEDULED_E_DS_NBR',EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').asInteger, []))
        then
          Result := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['Amount'], 0) + qryArrear.Result.FieldByName('ARREARS_AMOUNT').AsFloat)
        else
          Result := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['Amount'], 0));
      end
      else if ( (TotalAmountToHold + DependenthealthAmount) <= MaximumWithholdingAmount )then
      begin
         RemainAmt := MaximumWithholdingAmount-(TotalAmountToHold + DependenthealthAmount);
         Result := RoundTwo(GetProratedArrears);
      end
      else if ( (TotalAmountToHold + TotalArrears) <= MaximumWithholdingAmount )then
      begin
        if ((TotalArrears <> 0) and
            qryArrear.Result.Locate('EE_SCHEDULED_E_DS_NBR',EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').asInteger, []))
        then
          Result := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['Amount'], 0) + qryArrear.Result.FieldByName('ARREARS_AMOUNT').AsFloat)
        else
          Result := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['Amount'], 0));
      end
      else if ( TotalAmountToHold <= MaximumWithholdingAmount )then
      begin
        RemainAmt := MaximumWithholdingAmount-TotalAmountToHold;
        Result := RoundTwo(GetProratedArrears);
      end
      else
      begin
        if ProrateRatio > 0 then
          Result := RoundTwo(MaximumWithholdingAmount * ProrateRatio)
        else // set up is not correct (if exceed MaximumWithholdingAmount, should have a ProrateRatio to distribute )
          Result := 0;
      end;
    end;
  end;
  procedure CheckEffectiveDateLimits(Dataset: TDataset);
  var
    s, sTmp: String;
    Q: IevQuery;
  begin

    sTmp := ' and EE_SCHEDULED_E_DS_NBR <> '+ DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').AsString;

    s:= 'select EFFECTIVE_START_DATE, EFFECTIVE_END_DATE ' +
           ' from EE_SCHEDULED_E_DS where {AsOfNow<EE_SCHEDULED_E_DS>} and ' +
           ' CL_E_DS_NBR= :cledsnbr AND EE_NBR= :eenbr ' + sTmp +
           ' order by EFFECTIVE_START_DATE desc ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('cledsnbr', Dataset.FieldByName('CL_E_DS_NBR').AsInteger);
    Q.Params.AddValue('eenbr', Dataset.FieldByName('EE_NBR').AsInteger);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    begin
      if Q.Result.Locate('EFFECTIVE_START_DATE', DataSet['EFFECTIVE_START_DATE'], []) then
        raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation)
      else
      begin
        Q.Result.First;
        while not Q.Result.Eof do
        begin
          if ((( DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               ( DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime <= Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
              or
               ((DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                (Q.Result.FieldByName('EFFECTIVE_END_DATE').IsNull)))  then // EXCEPTION WRONG_EFFECTIVE_START_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;
          if (((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               (DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime <= Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
               or
              ((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               (Q.Result.FieldByName('EFFECTIVE_END_DATE').IsNull))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;
          if ((DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime < Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
             (((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime > Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime) and (Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime <> 0)) or
               (DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime = 0))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;

          Q.Result.Next;
        end;
      end;
    end;
  end;

begin
  Result := True;
  CL_E_DS.Activate;
  with PR_CHECK_LINES do
  begin
    CheckEffectiveDateLimits(EE_SCHEDULED_E_DS);

    if EDType = ED_EWOD_GROUP_TERM_LIFE then
    begin
      Exit;
    end;
    if CheckDDPrenote(EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR'], PR.FieldByName('CHECK_DATE').Value) then
    begin
      FieldByName('AMOUNT').Value := 0;
      Exit;
    end;

    if ((EDType[1] <> 'D') or (EDType = ED_DED_REIMBURSEMENT)) and (EDType <> ED_MEMO_PENSION) and (EDType <> ED_MEMO_PENSION_SUI)
    and not TypeIsCobraCredit(EDType, VarToStr(CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'DESCRIPTION')))
    and (EE_SCHEDULED_E_DS.FieldByName('ALWAYS_PAY').AsString = 'N') then
    begin
      TempAmount := 0;
      ApplyTempCheckLinesCheckRange(PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').AsInteger);
      cdstTempCheckLines.First;
      while not cdstTempCheckLines.EOF do
      begin
        CodeType2 := CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
        if (CodeType2 = ED_OEARN_SALARY) or ((CodeType2[1] = 'E') and (cdstTempCheckLines.FieldByName('LINE_TYPE').AsString[1] in [CHECK_LINE_TYPE_USER, CHECK_LINE_TYPE_DISTR_USER])) then
          TempAmount := TempAmount + ConvertNull(cdstTempCheckLines['AMOUNT'], 0);
        cdstTempCheckLines.Next;
      end;
      if TempAmount = 0 then
      begin
        CalcType := ConvertNull(EE_SCHEDULED_E_DS['CALCULATION_TYPE'], '');
        if (CalcType = CALC_METHOD_RATE_HOURS) or (CalcType = CALC_METHOD_RATE_AMOUNT) then
          FieldByName('RATE_OF_PAY').Value := EE_SCHEDULED_E_DS['AMOUNT'];
        Exit;
      end;
    end;

    if not EE_SCHEDULED_E_DS.FieldByName('THRESHOLD_E_D_GROUPS_NBR').IsNull then
    begin
      if EE_SCHEDULED_E_DS['USE_PENSION_LIMIT'] = 'Y' then
      begin
        if (GetPensionLimit(EDType) <> 0)
        and (EarnCodeGroupYTDCalc(EE_SCHEDULED_E_DS['THRESHOLD_E_D_GROUPS_NBR'], True) < GetPensionLimit(EDType)) then
          Exit;
      end
      else
      begin
        if (ConvertNull(EE_SCHEDULED_E_DS['THRESHOLD_AMOUNT'], 0) <> 0)
        and (EarnCodeGroupYTDCalc(EE_SCHEDULED_E_DS['THRESHOLD_E_D_GROUPS_NBR'], True) < EE_SCHEDULED_E_DS.FieldByName('THRESHOLD_AMOUNT').Value) then
          Exit;
      end;
    end;

    EmployeeNbr := PR_CHECK['EE_NBR'];
    if (EDType = ED_DED_PERCENT_OF_TAX) or (EDType = ED_DED_PERCENT_OF_TAX_WAGES) then
    begin
      CO_STATES.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
      EE_STATES.Activate;
      EEStateNbr := EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EmployeeNbr, CO_STATES.Lookup('SY_STATES_NBR',
      CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'SY_STATE_NBR'),
      'CO_STATES_NBR')]), 'EE_STATES_NBR');
      if VarIsNull(EEStateNbr) then
        raise EInconsistentData.CreateHelp('Employee #' + Trim(VarToStr(EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER')))
          + ' you are trying to do % of state deduction for is not setup in that state!', IDH_InconsistentData);

      if PR_CHECK_STATES.Active and PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([FieldByName('PR_CHECK_NBR').Value, EEStateNbr]), []) then
      begin
        if EDType = ED_DED_PERCENT_OF_TAX then
          FieldByName('AMOUNT').Value := ConvertNull(PR_CHECK_STATES['STATE_TAX'], 0) /
          (1 - ConvertNull(EE_SCHEDULED_E_DS['PERCENTAGE'], 0) * 0.01) *
          ConvertNull(EE_SCHEDULED_E_DS['PERCENTAGE'], 0) * 0.01
        else
          FieldByName('AMOUNT').Value := ConvertNull(PR_CHECK_STATES['STATE_TAXABLE_WAGES'], 0)
            * ConvertNull(EE_SCHEDULED_E_DS['PERCENTAGE'], 0) * 0.01;
      end
      else
        FieldByName('AMOUNT').Value := 0;
      if FieldByName('AMOUNT').Value > FStateTax then
        FieldByName('AMOUNT').Value := FStateTax;
      Exit;
    end;

    if EDType = ED_MEMO_PERCENT_OF_TAX_WAGES then
    begin
      DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
      DM_SYSTEM_STATE.SY_SUI.Activate;
      COSUINbr := DM_COMPANY.CO_SUI.Lookup('SY_SUI_NBR', DM_SYSTEM_STATE.SY_SUI.Lookup('SY_STATES_NBR;EE_ER_OR_EE_OTHER_OR_ER_OTHER',
        VarArrayOf([CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'],
        'SY_STATE_NBR'), GROUP_BOX_EE]), 'SY_SUI_NBR'), 'CO_SUI_NBR');
      if PR_CHECK_SUI.Active and not VarIsNull(COSUINbr) and PR_CHECK_SUI.Locate('PR_CHECK_NBR;CO_SUI_NBR', VarArrayOf([FieldByName('PR_CHECK_NBR').Value, COSUINbr]), []) then
      begin
        FieldByName('AMOUNT').Value := ConvertNull(PR_CHECK_SUI['SUI_TAXABLE_WAGES'], 0)
          * ConvertNull(EE_SCHEDULED_E_DS['PERCENTAGE'], 0) * 0.01;
      end
      else
        FieldByName('AMOUNT').Value := 0;
      Exit;
    end;

    CalcType := ConvertNull(EE_SCHEDULED_E_DS['CALCULATION_TYPE'], '');
    if (CalcType = '') or (CalcType = CALC_METHOD_NONE) then
    begin
{      if EE_SCHEDULED_E_DS.FieldByName('DEDUCT_WHOLE_CHECK').AsString = 'N' then
      begin
        raise ENoCalcTypeForScheduledED.CreateHelp('Calculation Type of scheduled E/D '
          + CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER')
          + ' for EE #' + Trim(VarToStr(EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER'))) + ' is missing!', IDH_InconsistentData);
      end;}  // Commented out per Laurie's request. Let's see if they like it...
      if FieldByName('AMOUNT').IsNull then
        FieldByName('AMOUNT').Value := 0;
      Exit;
    end;

    case CalcType[1] of
      CALC_METHOD_FIXED:
        begin

          TempAmount := GetFixedAmount(EE_SCHEDULED_E_DS, PR, EE_SCHEDULED_E_DS['AMOUNT']);
          if not EE_SCHEDULED_E_DS.FieldByName('EE_BENEFITS_NBR').IsNull then
          begin
            DM_EMPLOYEE.EE_BENEFITS.DataRequired('ALL');
            Assert(DM_EMPLOYEE.EE_BENEFITS.Locate('EE_BENEFITS_NBR', EE_SCHEDULED_E_DS['EE_BENEFITS_NBR'], []));
            if not DM_EMPLOYEE.EE_BENEFITS.CO_BENEFIT_SUBTYPE_NBR.IsNull then
            begin
              DM_EMPLOYEE.CO_BENEFITS.DataRequired('CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString);
              DM_EMPLOYEE.CO_BENEFIT_SUBTYPE.DataRequired();

              DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR', DM_EMPLOYEE.EE_BENEFITS.CO_BENEFIT_SUBTYPE_NBR.Value, []);
              DM_EMPLOYEE.CO_BENEFITS.Locate('CO_BENEFITS_NBR', DM_EMPLOYEE.EE_BENEFITS.CO_BENEFITS_NBR.Value, []);

              if AnsiSameText(VarToStr(DM_COMPANY.CO_BENEFIT_SUBTYPE['DESCRIPTION']), sEmployeeDefinedSubtypeDescription)
                and (DM_COMPANY.CO_BENEFITS['ALLOW_EE_CONTRIBUTION'] = GROUP_BOX_YES) then
              begin
                AnnualMaximum := DM_EMPLOYEE.EE_SCHEDULED_E_DS.ANNUAL_MAXIMUM_AMOUNT.AsFloat;
                case DM_EMPLOYEE.EE_SCHEDULED_E_DS.FREQUENCY.AsString[1] of
                SCHED_FREQ_RUN_ONCE, SCHED_FREQ_ONE_TIME: TempAmount := DM_EMPLOYEE.EE_SCHEDULED_E_DS.TARGET_AMOUNT.AsFloat;
                SCHED_FREQ_DAILY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/260);
                  if AnnualMaximum > (TempAmount*260) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_SCHEDULED_PAY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/GetTaxFreq(DM_EMPLOYEE.EE.PAY_FREQUENCY.AsString));
                  if AnnualMaximum > (TempAmount*GetTaxFreq(DM_EMPLOYEE.EE.PAY_FREQUENCY.AsString)) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_FIRST_SCHED_PAY, // : TempAmount := DM_EMPLOYEE.EE_SCHEDULED_E_DS.TARGET_AMOUNT.AsFloat;
                SCHED_FREQ_LAST_SCHED_PAY,
                SCHED_FREQ_MID_SCHED_PAY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/12);
                  if AnnualMaximum > (TempAmount*12) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_ANNUALLY: TempAmount := AnnualMaximum;
                SCHED_FREQ_SEMI_ANNUALLY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/2);
                  if AnnualMaximum > (TempAmount*2) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_QUARTERLY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/4);
                  if AnnualMaximum > (TempAmount*4) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_SCHEDULED_MONTHLY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/12);
                  if AnnualMaximum > (TempAmount*12) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_EVRY_OTHER_PAY:
                begin
                  TempAmount := RoundTwo(AnnualMaximum/(GetTaxFreq(DM_EMPLOYEE.EE.PAY_FREQUENCY.AsString) div 2));
                  if AnnualMaximum > (TempAmount*(GetTaxFreq(DM_EMPLOYEE.EE.PAY_FREQUENCY.AsString) div 2)) then
                    TempAmount := TempAmount + 0.01;
                end;
                SCHED_FREQ_USER_ENTERED: TempAmount := 0;
                end;
              end;
            end;
          end;

          FieldByName('AMOUNT').Value := TempAmount;
          if (EE_SCHEDULED_E_DS['ALWAYS_PAY'] = 'Y') then
          begin
// Find the last scheduled payroll check date when the employee was paid
            LastPaidCheckDate := 0;
            if Assigned(cdstEDCheckDates) then
            begin
              if cdstEDCheckDates.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([EmployeeNbr, EE_SCHEDULED_E_DS['CL_E_DS_NBR']]), []) then
                LastPaidCheckDate := cdstEDCheckDates['CHECK_DATE'];
            end
            else
            begin
              if ctx_DataAccess.CUSTOM_VIEW.Active then
                ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('EmployeeLastEDCheckDate') do
              begin
                SetParam('EmployeeNbr', EmployeeNbr);
                SetParam('PrNbr', PR.FieldByName('PR_NBR').AsInteger);
                SetParam('EDNbr', EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              if not ctx_DataAccess.CUSTOM_VIEW.FieldByName('CHECK_DATE').IsNull then
                LastPaidCheckDate := ctx_DataAccess.CUSTOM_VIEW['CHECK_DATE'];
            end;
            if LastPaidCheckDate <> 0 then
            begin
// Count all scheduled payrolls after that one
              if EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] > LastPaidCheckDate then
                CheckDate := EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE']
              else
                CheckDate := LastPaidCheckDate;
              ctx_DataAccess.CUSTOM_VIEW.Close;
              if CheckDate < EE.FieldByName('CURRENT_HIRE_DATE').AsDateTime then
                CheckDate := EE.FieldByName('CURRENT_HIRE_DATE').AsDateTime;

              with TExecDSWrapper.Create('SelectAllPayrollsAfterDate') do
              begin
                SetParam('Frequency', EE.FieldByName('PAY_FREQUENCY').AsString);
                SetParam('CoNbr', PR['CO_NBR']);
                SetParam('EndCheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;

              PrCount := 0;
              PriorCheckDate := 0;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                if (ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime > CheckDate) and (ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime < PR.FieldByName('CHECK_DATE').AsDateTime) then
                case EE_SCHEDULED_E_DS.FieldByName('FREQUENCY').AsString[1] of
                  SCHED_FREQ_FIRST_SCHED_PAY:
                    if ThisIsRightPayroll(GROUP_BOX_FIRST, ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime, PriorCheckDate, 0) then
                      Inc(PrCount);
                  SCHED_FREQ_LAST_SCHED_PAY:
                    begin
                      ctx_DataAccess.CUSTOM_VIEW.Next;
                      if not ctx_DataAccess.CUSTOM_VIEW.EOF then
                      begin
                        NextCheckDate := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
                        ctx_DataAccess.CUSTOM_VIEW.Prior;
                        if ThisIsRightPayroll(GROUP_BOX_LAST, ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime, 0, NextCheckDate) then
                          Inc(PrCount);
                      end;
                    end;
                  SCHED_FREQ_MID_SCHED_PAY:
                    begin
                      ctx_DataAccess.CUSTOM_VIEW.Next;
                      if not ctx_DataAccess.CUSTOM_VIEW.EOF then
                      begin
                         NextCheckDate := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
                        ctx_DataAccess.CUSTOM_VIEW.Prior;
                        if ThisIsRightPayroll(GROUP_BOX_CLOSEST_TO_15, ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime, PriorCheckDate, NextCheckDate) then
                          Inc(PrCount);
                      end;
                    end;
                  SCHED_FREQ_QUARTERLY:
                    begin
                      ctx_DataAccess.CUSTOM_VIEW.Next;
                      if not ctx_DataAccess.CUSTOM_VIEW.EOF then
                      begin
                        NextCheckDate := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
                        ctx_DataAccess.CUSTOM_VIEW.Prior;
                        if ThisIsRightPayroll(EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString, ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime, PriorCheckDate, NextCheckDate)
                        and (GetMonth(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime) - (GetQuarterNumber(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime) - 1) * 3 = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptQuarter)) then
                          Inc(PrCount);
                      end;
                    end;
                  SCHED_FREQ_SEMI_ANNUALLY:
                    begin
                      ctx_DataAccess.CUSTOM_VIEW.Next;
                      if not ctx_DataAccess.CUSTOM_VIEW.EOF then
                      begin
                        NextCheckDate := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
                        ctx_DataAccess.CUSTOM_VIEW.Prior;
                        if ThisIsRightPayroll(EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString, ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime, PriorCheckDate, NextCheckDate)
                        and ((GetMonth(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime) = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptSemiAnnual))
                        or (GetMonth(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime) - 6 = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptSemiAnnual))) then
                          Inc(PrCount);
                      end;
                    end;
                  SCHED_FREQ_ANNUALLY:
                    begin
                      ctx_DataAccess.CUSTOM_VIEW.Next;
                      if not ctx_DataAccess.CUSTOM_VIEW.EOF then
                      begin
                        NextCheckDate := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
                        ctx_DataAccess.CUSTOM_VIEW.Prior;
                        if ThisIsRightPayroll(EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString, ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime, PriorCheckDate, NextCheckDate)
                        and (GetMonth(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime) = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptAnnual)) then
                          Inc(PrCount);
                      end;
                    end;
                  SCHED_FREQ_USER_ENTERED:
                    begin
                    end;
                else
                  Inc(PrCount);
                  case GetWeekNumber(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime) of
                    1:
                      if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_1'] = 'Y' then
                        Dec(PrCount);
                    2:
                      if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_2'] = 'Y' then
                        Dec(PrCount);
                    3:
                      if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_3'] = 'Y' then
                        Dec(PrCount);
                    4:
                      if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_4'] = 'Y' then
                        Dec(PrCount);
                    5:
                      if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_5'] = 'Y' then
                        Dec(PrCount);
                  end;
                end;
                PriorCheckDate := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;
              ctx_DataAccess.CUSTOM_VIEW.Close;
              if EE_SCHEDULED_E_DS.FieldByName('FREQUENCY').AsString = SCHED_FREQ_EVRY_OTHER_PAY then
                PrCount := Trunc(PrCount / 2);
              FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value + TempAmount * PrCount;
            end;
          end;
        end;
      CALC_METHOD_GROSS:
        FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * Gross * 0.01);
      CALC_METHOD_NET:
        FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * Net * 0.01);
      CALC_METHOD_TAX_WAGES:
        begin
          if PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').IsNull then // We are just creating a check line
            Exit;
          FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat * 0.01);
        end;
      CALC_METHOD_DISP_EARN:
        FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * DisposableEarnings(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR']) * 0.01);
      CALC_METHOD_GARNISHMENT, CALC_METHOD_GARNISHMENT_GROSS:
        begin
          FieldByName('AMOUNT').Value := 0;
          if PR_CHECK.FieldByName('GROSS_WAGES').IsNull then // We are just creating a check line
            Exit;

          TempAmount := DisposableEarnings(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR']);

          if not EE_SCHEDULED_E_DS.FieldByName('THRESHOLD_E_D_GROUPS_NBR').IsNull then
            TempAmount2 := DisposableEarnings(EE_SCHEDULED_E_DS['THRESHOLD_E_D_GROUPS_NBR'])
          else
            TempAmount2 := TempAmount;
          if ConvertNull(EE_SCHEDULED_E_DS['PERCENTAGE'], 0) <> 0 then
          begin
            if CalcType = CALC_METHOD_GARNISHMENT_GROSS then
            begin
              if EE_SCHEDULED_E_DS.FieldByName('CL_E_D_GROUPS_NBR').IsNull then
                TempAmount := ConvertNull(PR_CHECK['GROSS_WAGES'], 0) * EE_SCHEDULED_E_DS.FieldByName('PERCENTAGE').Value * 0.01
              else
                TempAmount := EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], True, PR_CHECK) * EE_SCHEDULED_E_DS.FieldByName('PERCENTAGE').Value * 0.01;
            end
            else
              TempAmount := TempAmount * EE_SCHEDULED_E_DS['PERCENTAGE'] * 0.01;
          end;

          SY_STATES.Activate;
          CO_STATES.Activate;
          EE_STATES.Activate;

          MinWageMult := ConvertNull(EE_SCHEDULED_E_DS['MINIMUM_WAGE_MULTIPLIER'], 0) * (52 /
            GetCheckFreq(PR_BATCH['FREQUENCY']));
          MaxGarnPercent := ConvertNull(EE_SCHEDULED_E_DS['MAXIMUM_GARNISH_PERCENT'], 0) * 0.01;

          EEStatesNbr := EE_STATES.Lookup('EE_STATES_NBR', EE.Lookup('EE_NBR', EmployeeNbr, 'HOME_TAX_EE_STATES_NBR'), 'CO_STATES_NBR');
          if MinWageMult = 0 then
            MinWageMult := ConvertNull(SY_STATES.Lookup('STATE', CO_STATES.Lookup('CO_STATES_NBR', EEStatesNbr, 'STATE'),
              'GARNISH_MIN_WAGE_MULTIPLIER'), 0) * (52 / GetCheckFreq(PR_BATCH['FREQUENCY']));
          if MaxGarnPercent = 0 then
            MaxGarnPercent := ConvertNull(SY_STATES.Lookup('STATE', CO_STATES.Lookup('CO_STATES_NBR', EEStatesNbr, 'STATE'),
              'MAXIMUM_GARNISHMENT_PERCENT'), 0);

          if SY_STATES.Lookup('STATE', CO_STATES.Lookup('CO_STATES_NBR', EEStatesNbr, 'STATE'), 'DD_CHILD_SUPPORT' {Use State Minimum Wage}) = 'Y' then
          begin
            MinWage := ConvertNull(EE_STATES.Lookup('EE_STATES_NBR', EE.Lookup('EE_NBR', EmployeeNbr, 'HOME_TAX_EE_STATES_NBR'), 'OVERRIDE_STATE_MINIMUM_WAGE'), 0);
            if MinWage = 0 then // if a state doesn't have override set up it should use one from EE level
              MinWage := ConvertNull(EE.Lookup('EE_NBR', EmployeeNbr, 'OVERRIDE_FEDERAL_MINIMUM_WAGE'), 0);
            if MinWage = 0 then
              MinWage := ConvertNull(SY_STATES.Lookup('STATE', CO_STATES.Lookup('CO_STATES_NBR', EEStatesNbr, 'STATE'), 'STATE_MINIMUM_WAGE'), 0);
          end
          else
          begin
            SY_FED_TAX_TABLE.Activate;
            MinWage := ConvertNull(EE.Lookup('EE_NBR', EmployeeNbr, 'OVERRIDE_FEDERAL_MINIMUM_WAGE'), 0);
            if MinWage = 0 then
              MinWage := ConvertNull(SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'], 0);
          end;

          if TempAmount2 <= MinWageMult * MinWage then
          begin
            FieldByName('AMOUNT').Value := 0;
            Exit;
          end;
          if (TempAmount > MaxGarnPercent * TempAmount2) and (CO_STATES.Lookup('CO_STATES_NBR', EEStatesNbr, 'STATE') <> 'IL') then
            FieldByName('AMOUNT').Value := TempAmount2 * MaxGarnPercent
          else
            FieldByName('AMOUNT').Value := TempAmount;
          if TempAmount2 - FieldByName('AMOUNT').Value <= MinWageMult * MinWage then
          begin
            FieldByName('AMOUNT').Value := TempAmount2 - MinWageMult * MinWage;
            if FieldByName('AMOUNT').Value <= 0 then
            begin
              FieldByName('AMOUNT').Value := 0;
              Exit;
            end;
          end;
          if TempAmount2 - FieldByName('AMOUNT').Value <= ConvertNull(EE_SCHEDULED_E_DS['TAKE_HOME_PAY'], 0) then
          begin
            FieldByName('AMOUNT').Value := TempAmount2 - ConvertNull(EE_SCHEDULED_E_DS['TAKE_HOME_PAY'], 0);
            if FieldByName('AMOUNT').Value <= 0 then
            begin
              FieldByName('AMOUNT').Value := 0;
              Exit;
            end;
          end;
          ScheduledGroupAmount := ScheduledGroupCalc(EE_SCHEDULED_E_DS['SCHEDULED_E_D_GROUPS_NBR']);
          FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value - ScheduledGroupAmount + FieldByName('AMOUNT').Value;
          if FieldByName('AMOUNT').Value < 0 then
            FieldByName('AMOUNT').Value := 0;
          if ConvertNull(EE_SCHEDULED_E_DS['AMOUNT'], 0) <> 0 then
          if ConvertNull(EE_SCHEDULED_E_DS['AMOUNT'], 0) < FieldByName('AMOUNT').Value then
            FieldByName('AMOUNT').Value := EE_SCHEDULED_E_DS['AMOUNT'];
        end;
      CALC_METHOD_PERC:
        begin
          if EDType = ED_ST_MASS_RETIREMENT then
          begin
            // Decided to hard-code 30,000 because Des thinks it should not be changing
            TempAmount := EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], True, PR_CHECK) - 30000 / GetCheckFreq(PR_BATCH.FieldByName('FREQUENCY').Value);
            if TempAmount < 0 then
              TempAmount := 0;
            FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * TempAmount * 0.01);
          end
          else
            FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], True, PR_CHECK) * 0.01);
        end;
      CALC_METHOD_PERC_HOURS:
        begin
          FieldByName('AMOUNT').Value := 0;
          FieldByName('HOURS_OR_PIECES').Value := RoundTwo(GetPercentage * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], False, PR_CHECK) * 0.01);
          FieldByName('RATE_NUMBER').Value := 0;
          FieldByName('RATE_OF_PAY').Value := Null;
        end;
      CALC_METHOD_PERC_AMT_HOURS:
        begin
          FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], True, PR_CHECK) * 0.01);
          FieldByName('HOURS_OR_PIECES').Value := RoundTwo(GetPercentage * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], False, PR_CHECK) * 0.01);
          FieldByName('RATE_NUMBER').Value := 0;
          FieldByName('RATE_OF_PAY').Value := Null;
        end;
      CALC_METHOD_PERC_PRIMARY_RATE:
        begin
          FieldByName('AMOUNT').Value := RoundTwo(GetPercentage * DM_EMPLOYEE.EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr, GetEEPrimaryRate(EmployeeNbr, EE, DM_EMPLOYEE.EE_RATES, True)]), 'RATE_AMOUNT') * 0.01);
        end;
      CALC_METHOD_LEVY:
        FieldByName('AMOUNT').Value := 0;
      CALC_METHOD_PENSION:
        begin
          try
            PensionNbr := CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'CL_PENSION_NBR');
          except
            raise EWrongMatchSetup.CreateHelp('Cannot calculate match because pension number is not attached to ' +
              VarToStr(CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER')) + '.', IDH_InconsistentData);
          end;

          Q := TevQuery.Create('select S.EE_SCHEDULED_E_DS_NBR, P.STOP_WHEN_EE_REACHES_MAX ' +
            'from EE_SCHEDULED_E_DS S ' +
            'join CL_E_DS D on S.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
            'join CL_PENSION P on P.CL_PENSION_NBR=D.CL_PENSION_NBR '+
            'where S.EE_NBR=:EmployeeNbr and ' +
            'D.CL_PENSION_NBR=:PensionNbr and ' +
            'D.E_D_CODE_TYPE not in (#EDCodeType) and ' +
            'S.EFFECTIVE_START_DATE<=:CheckDate and (S.EFFECTIVE_END_DATE>=:CheckDate or S.EFFECTIVE_END_DATE is null) and ' +
            '{AsOfNow<D>} and {AsOfNow<S>} and {AsOfNow<P>} ' +
            'order by S.PRIORITY_NUMBER');
            
          Q.Params.AddValue('EmployeeNbr', EmployeeNbr);
          Q.Params.AddValue('PensionNbr', PensionNbr);
          Q.Macros.AddValue('EDCodeType', '''' + ED_MEMO_PENSION + ''', ''' + ED_MEMO_PENSION_SUI + '''');
          Q.Params.AddValue('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
          Pensions := Q.Result;
          Q := nil;


          if Pensions.RecordCount = 0 then
            raise EWrongMatchSetup.CreateHelp('Corresponding pension deduction does not exist for match for employee #' + EE.Lookup('EE_NBR', EmployeeNbr, 'CUSTOM_EMPLOYEE_NUMBER') + '!', IDH_InconsistentData);

          FieldByName('AMOUNT').Value := 0;
          CL_PENSION.Activate;

          Pensions.First;
          if Pensions['STOP_WHEN_EE_REACHES_MAX'] = 'Y' then
          begin
            EELineFound := False;
            while not Pensions.EOF do
            begin
              if ConvertNull(cdstTempCheckLines.Lookup('PR_CHECK_NBR;EE_SCHEDULED_E_DS_NBR', VarArrayOf([PR_CHECK['PR_CHECK_NBR'],
                Pensions['EE_SCHEDULED_E_DS_NBR']]), 'AMOUNT'), 0) <> 0 then
              begin
                EELineFound := True;
                Break;
              end;
              Pensions.Next;
            end;
            if not EELineFound then
              Exit;
          end;

          Q := TEvQuery.Create('select D.E_D_CODE_TYPE ' +
              'from EE_SCHEDULED_E_DS S ' +
              'join CL_E_DS D on S.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
              'where S.EE_NBR=:EmployeeNbr and ' +
              'D.CL_PENSION_NBR=:PensionNbr and ' +
              'S.PERCENTAGE is null and D.FLAT_MATCH_AMOUNT is null and ' +
              'D.E_D_CODE_TYPE not in (''' + ED_MEMO_PENSION + ''',''' + ED_DED_PENSION_LOAN + ''', ''' + ED_MEMO_PENSION_SUI + ''') and ' +
              'S.EFFECTIVE_START_DATE<=:CheckDate and (S.EFFECTIVE_END_DATE>=:CheckDate or S.EFFECTIVE_END_DATE is null) and ' +
              '{AsOfNow<D>} and {AsOfNow<S>} ' +
              'order by S.PRIORITY_NUMBER');

          Q.Params.AddValue('EmployeeNbr', EmployeeNbr);
          Q.Params.AddValue('PensionNbr', PensionNbr);
          Q.Params.AddValue('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);

          if Q.Result.RecordCount <> 0 then
            raise EInconsistentData.CreateHelp('Scheduled pension with corresponding match does not have a percentage setup!', IDH_InconsistentData);

          Q := TevQuery.Create('select sum(D.FLAT_MATCH_AMOUNT) FLAT_MATCH_AMOUNT, COUNT(1) CNT ' +
            'from EE_SCHEDULED_E_DS S ' +
            'join CL_E_DS D on S.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
            'join CL_PENSION P on P.CL_PENSION_NBR=D.CL_PENSION_NBR '+
            'where S.EE_NBR=:EmployeeNbr and ' +
            'D.CL_PENSION_NBR=:PensionNbr and D.FLAT_MATCH_AMOUNT is not null and ' +
            'D.E_D_CODE_TYPE not in (#EDCodeType) and ' +
            'S.EFFECTIVE_START_DATE<=:CheckDate and (S.EFFECTIVE_END_DATE>=:CheckDate or S.EFFECTIVE_END_DATE is null) and ' +
            '{AsOfNow<D>} and {AsOfNow<S>} and {AsOfNow<P>} ');

          Q.Params.AddValue('EmployeeNbr', EmployeeNbr);
          Q.Params.AddValue('PensionNbr', PensionNbr);
          Q.Macros.AddValue('EDCodeType', '''' + ED_MEMO_PENSION + ''', ''' + ED_MEMO_PENSION_SUI + '''');
          Q.Params.AddValue('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);

          FlatMatchAmount := ConvertNull(Q.Result['FLAT_MATCH_AMOUNT'], 0);

          Q := TevQuery.Create('select S.EE_SCHEDULED_E_DS_NBR, S.AMOUNT, S.PERCENTAGE , S.CALCULATION_TYPE, ' +
            'S.CL_E_D_GROUPS_NBR, S.MINIMUM_PAY_PERIOD_AMOUNT, S.MINIMUM_PAY_PERIOD_PERCENTAGE, ' +
            'S.MIN_PPP_CL_E_D_GROUPS_NBR, S.MAXIMUM_PAY_PERIOD_AMOUNT, S.MAXIMUM_PAY_PERIOD_PERCENTAGE, ' +
            'S.MAX_PPP_CL_E_D_GROUPS_NBR, S.MAX_PPP_CL_E_D_GROUPS_NBR, S.ANNUAL_MAXIMUM_AMOUNT, S.SCHEDULED_E_D_GROUPS_NBR, ' +
            'D.MATCH_FIRST_AMOUNT, D.MATCH_FIRST_PERCENTAGE, D.MATCH_SECOND_AMOUNT, D.MATCH_SECOND_PERCENTAGE, ' +
            'D.FLAT_MATCH_AMOUNT, D.CL_E_DS_NBR, D.E_D_CODE_TYPE, P.STOP_WHEN_EE_REACHES_MAX ' +
            'from EE_SCHEDULED_E_DS S ' +
            'join CL_E_DS D on S.CL_E_DS_NBR=D.CL_E_DS_NBR ' +
            'join CL_PENSION P on P.CL_PENSION_NBR=D.CL_PENSION_NBR '+
            'where S.EE_NBR=:EmployeeNbr and S.PERCENTAGE is not null and FLAT_MATCH_AMOUNT is null and ' +
            'D.CL_PENSION_NBR=:PensionNbr and ' +
            'D.E_D_CODE_TYPE not in (#EDCodeType) and ' +
            'S.EFFECTIVE_START_DATE<=:CheckDate and (S.EFFECTIVE_END_DATE>=:CheckDate or S.EFFECTIVE_END_DATE is null) and ' +
            '{AsOfNow<D>} and {AsOfNow<S>} and {AsOfNow<P>} ' +
            'order by S.PRIORITY_NUMBER');

          Q.Params.AddValue('EmployeeNbr', EmployeeNbr);
          Q.Params.AddValue('PensionNbr', PensionNbr);
          Q.Macros.AddValue('EDCodeType', '''' + ED_MEMO_PENSION + ''', ''' + ED_MEMO_PENSION_SUI + '''');
          Q.Params.AddValue('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
          Pensions := Q.Result;
          Q := nil;

          PensionEarnings := 0;
          TotalPensionAmount := 0;
          TotalMatchAmount := 0;
          SchedGroupNbr := 0;
          ScheduledGroupAmount := 0;
          TempPercentage := 0;          

          Pensions.First;

          while not Pensions.Eof do
          begin
            TempAmount := 0;
            CalcPercentage := 0;
            Earnings := 0;

            if SchedGroupNbr = 0 then
              SchedGroupNbr := ConvertNull(Pensions['SCHEDULED_E_D_GROUPS_NBR'], 0);

            if Pensions['CALCULATION_TYPE'] = CALC_METHOD_FIXED then
            begin
              TempAmount := ConvertNull(Pensions['AMOUNT'], 0);
            end
            else if Pensions['CALCULATION_TYPE'] = CALC_METHOD_GROSS then
            begin
              if PR_CHECK.FieldByName('GROSS_WAGES').IsNull then
                Earnings := RoundTwo(Gross)
              else
                Earnings := ConvertNull(PR_CHECK['GROSS_WAGES'], 0);

              CalcPercentage := Pensions['PERCENTAGE'];
              TempAmount := Earnings * CalcPercentage * 0.01;
            end
            else
            begin
              Earnings := EarnCodeGroupCalc(Pensions['CL_E_D_GROUPS_NBR'], True, PR_CHECK);
              CalcPercentage := Pensions['PERCENTAGE'];
              TempAmount := Earnings * CalcPercentage * 0.01;
            end;

            if (TempAmount < ConvertNull(Pensions['MINIMUM_PAY_PERIOD_AMOUNT'], 0)) and (not Pensions.FieldByName('MINIMUM_PAY_PERIOD_AMOUNT').IsNull) then
              TempAmount := Pensions['MINIMUM_PAY_PERIOD_AMOUNT']
            else
            begin
              PayPeriodMinimum := Abs(ConvertNull(Pensions['MINIMUM_PAY_PERIOD_PERCENTAGE'], 0)) * EarnCodeGroupCalc(Pensions['MIN_PPP_CL_E_D_GROUPS_NBR'], True, PR_CHECK) * 0.01;

              if TempAmount < PayPeriodMinimum then
                TempAmount := PayPeriodMinimum
              else
              begin
                if not Pensions.FieldByName('SCHEDULED_E_D_GROUPS_NBR').IsNull and (SchedGroupNbr = Pensions['SCHEDULED_E_D_GROUPS_NBR']) then
                  TempAmount2 := ScheduledGroupAmount + TempAmount
                else
                  TempAmount2 := TempAmount;

                PayPeriodMaximum := ConvertNull(Pensions['MAXIMUM_PAY_PERIOD_AMOUNT'], 9999999999);

                if TempAmount2 > PayPeriodMaximum then
                  TempAmount := PayPeriodMaximum - (TempAmount2 - TempAmount);

                if TempAmount < 0 then
                  TempAmount := 0;

                if (not Pensions.FieldByName('MAXIMUM_PAY_PERIOD_PERCENTAGE').IsNull) and (not Pensions.FieldByName('MAX_PPP_CL_E_D_GROUPS_NBR').IsNull) then
                begin
                  PayPeriodMaximum := Abs(Pensions['MAXIMUM_PAY_PERIOD_PERCENTAGE']) * EarnCodeGroupCalc(Pensions['MAX_PPP_CL_E_D_GROUPS_NBR'], True, PR_CHECK) * 0.01;
                  if TempAmount2 > PayPeriodMaximum then
                  begin
                    TempAmount := PayPeriodMaximum - (TempAmount2 - TempAmount);
                    if TempAmount < 0 then
                      TempAmount := 0;
                  end;
                end;
                if Earnings <> 0 then
                  CalcPercentage := TempAmount / Earnings * 100
                else
                  CalcPercentage := 0;
              end;
            end;

            if Pensions['STOP_WHEN_EE_REACHES_MAX'] = 'Y' then
            begin
              PensAnnualMaxAmount := GetAnnualMax(Pensions['ANNUAL_MAXIMUM_AMOUNT'], CL_E_DS.Lookup('CL_E_DS_NBR', Pensions['CL_E_DS_NBR'], 'E_D_CODE_TYPE'), Pensions['CL_E_DS_NBR']);
              GetLimitedTaxAndDedYTDforGTN(PR_CHECK['PR_CHECK_NBR'], Pensions['CL_E_DS_NBR'], ltDeduction, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, YTD, 0, 0, True);
              if PensAnnualMaxAmount <> 0 then
              begin
                if TempAmount + YTD > PensAnnualMaxAmount then
                  TempAmount := PensAnnualMaxAmount - YTD;
                if Earnings <> 0 then
                  CalcPercentage := TempAmount / Earnings * 100;
              end
              else
              begin
                PensionLimit := GetPensionLimit(Pensions['E_D_CODE_TYPE']);
                if (TempAmount + YTD > PensionLimit) and (PensionLimit <> 0) then
                begin
                  TempAmount := PensionLimit - YTD;
                  if Earnings <> 0 then
                    CalcPercentage := TempAmount / Earnings * 100;
                end;
              end;
            end;

            if Pensions['CALCULATION_TYPE'] = CALC_METHOD_FIXED then
            begin
              if Pensions.FieldByName('CL_E_D_GROUPS_NBR').IsNull then
              begin
                if PR_CHECK.FieldByName('GROSS_WAGES').IsNull then
                  Earnings := RoundTwo(Gross)
                else
                  Earnings := ConvertNull(PR_CHECK['GROSS_WAGES'], 0);
              end
              else
                Earnings := EarnCodeGroupCalc(Pensions['CL_E_D_GROUPS_NBR'], True, PR_CHECK);

              if Earnings <> 0 then
                CalcPercentage := TempAmount / Earnings * 100
              else
                CalcPercentage := 0;

            end;

            TotalPensionAmount := TotalPensionAmount + TempAmount;

            PensionEarnings := PensionEarnings + Earnings * CalcPercentage;
            TempPercentage := TempPercentage + CalcPercentage;

            if SchedGroupNbr = ConvertNull(Pensions['SCHEDULED_E_D_GROUPS_NBR'], -1) then
              ScheduledGroupAmount := ScheduledGroupAmount + TempAmount;

            Pensions.Next;
          end;

          if (TotalPensionAmount <> 0) and (TempPercentage <> 0) then
          begin

            PensionEarnings := PensionEarnings / TempPercentage;

            PensMatch1Amount  := ConvertNull(Pensions['MATCH_FIRST_AMOUNT'], 0);
            PensMatch1Percent := ConvertNull(Pensions['MATCH_FIRST_PERCENTAGE'], 0);
            PensMatch2Amount  := ConvertNull(Pensions['MATCH_SECOND_AMOUNT'], 0);
            PensMatch2Percent := ConvertNull(Pensions['MATCH_SECOND_PERCENTAGE'], 0);

            if PensMatch1Amount > TempPercentage then
            begin
              PensMatch1Amount := TempPercentage;
              PensMatch2Amount := 0;
            end  
            else
            if PensMatch1Amount + PensMatch2Amount > TempPercentage then
            begin
              PensMatch2Amount := TempPercentage - PensMatch1Amount;
              if PensMatch2Amount < 0 then
                PensMatch2Amount := 0;
            end;

            TotalMatchAmount := PensionEarnings * (PensMatch1Percent * 0.01 * PensMatch1Amount + PensMatch2Amount  * 0.01 * PensMatch2Percent) * 0.01;

          end;

          PR_CHECK_LINES['AMOUNT'] := TotalMatchAmount + FlatMatchAmount;

        end; // Pension Calculation
      CALC_METHOD_RATE_HOURS:
        begin
          if EDType = ED_EWOD_MEAL_CREDIT then
          begin
            TempAmount := 0;
            if not EE_SCHEDULED_E_DS.FieldByName('CL_E_D_GROUPS_NBR').IsNull then
            begin
              DM_CLIENT.CL_E_D_GROUPS.Activate;
              DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
              DM_CLIENT.CL_E_DS.Activate;
              ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
              cdstTempCheckLines.First;
              while not cdstTempCheckLines.EOF do
              begin
                if DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], cdstTempCheckLines.FieldByName('CL_E_DS_NBR').Value]), []) then
                if (ConvertNull(cdstTempCheckLines['RATE_OF_PAY'], 0) = 0) or (cdstTempCheckLines.FieldByName('RATE_OF_PAY').Value < GetMinimumWage(cdstTempCheckLines.FieldByName('EE_STATES_NBR').Value, Null)) then
                begin
                  if DM_CLIENT.CL_E_D_GROUPS.NewLookup('CL_E_D_GROUPS_NBR', EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], 'SUBTRACT_DEDUCTIONS') = GROUP_BOX_NO then
                    TempAmount := TempAmount + ConvertNull(cdstTempCheckLines['HOURS_OR_PIECES'], 0)
                  else
                    TempAmount := TempAmount + ConvertDeduction(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE'), ConvertNull(cdstTempCheckLines.FieldByName('HOURS_OR_PIECES').Value, 0));
                end;
                cdstTempCheckLines.Next;
              end;
            end;
          end
          else
            TempAmount := EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], False, PR_CHECK);

          if EE_SCHEDULED_E_DS.FieldByName('AMOUNT').IsNull then
          begin
            FieldByName('AMOUNT').Value := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'OVERRIDE_RATE'), 0) * TempAmount;
            FieldByName('RATE_OF_PAY').Value := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'OVERRIDE_RATE'), 0);
          end
          else
          begin
            FieldByName('AMOUNT').Value := EE_SCHEDULED_E_DS['AMOUNT'] * TempAmount;
            FieldByName('RATE_OF_PAY').Value := EE_SCHEDULED_E_DS['AMOUNT'];
          end;

          FieldByName('HOURS_OR_PIECES').Value := TempAmount;
          FieldByName('RATE_NUMBER').Value := Null;
        end;
      CALC_METHOD_RATE_AMOUNT:
        begin
          TempAmount := EarnCodeGroupCalc(EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'], True, PR_CHECK);
          FieldByName('AMOUNT').Value := EE_SCHEDULED_E_DS['AMOUNT'] * TempAmount;
          FieldByName('RATE_OF_PAY').Value := EE_SCHEDULED_E_DS['AMOUNT'];
        end;
      CALC_METHOD_NY_SDI:
        begin
          CO_STATES.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
          EE.Activate;
          EE_STATES.Activate;
          EEStateNbr := EE.Lookup('EE_NBR', EmployeeNbr, 'HOME_TAX_EE_STATES_NBR');
          if (CO_STATES.Lookup('CO_STATES_NBR', EE_STATES.Lookup('EE_STATES_NBR', EEStateNbr, 'SDI_APPLY_CO_STATES_NBR'), 'STATE') <> 'NY')
          and (CO_STATES.Lookup('CO_STATES_NBR', EE_STATES.Lookup('EE_STATES_NBR', EEStateNbr, 'SDI_APPLY_CO_STATES_NBR'), 'STATE') <> 'HI') then
            FieldByName('AMOUNT').Value := 0
          else
          begin
            SY_STATES.Activate;
            try
              SDITaxLimit := SY_STATES.Lookup('SY_STATES_NBR',
                CO_STATES.Lookup('CO_STATES_NBR', EE_STATES.Lookup('EE_STATES_NBR',
                EEStateNbr, 'SDI_APPLY_CO_STATES_NBR'), 'SY_STATES_NBR'), 'WEEKLY_SDI_TAX_CAP') * 52 /
                GetTaxFreq(PR_CHECK['TAX_FREQUENCY']);
            except
              raise ENoWeeklySDICap.CreateHelp(CO_STATES.Lookup('CO_STATES_NBR', EE_STATES.Lookup('EE_STATES_NBR',
                EEStateNbr, 'SDI_APPLY_CO_STATES_NBR'), 'STATE') + ' weekly SDI cap is not setup!', IDH_InconsistentData);
            end;
            if PR_CHECK_STATES.Active then
            begin
              TempAmount := ConvertNull(PR_CHECK['GROSS_WAGES'], 0);

              ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
              {if cdstTempCheckLines.Filter <> 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString then
                cdstTempCheckLines.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
              if not cdstTempCheckLines.Filtered then
                cdstTempCheckLines.Filtered := True;}
              cdstTempCheckLines.First;
              while not cdstTempCheckLines.EOF do
              begin
                CodeType := {DM_CLIENT.}CL_E_DS.Lookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
                if TypeIsTaxableMemo(CodeType) then
                  TempAmount := TempAmount + ConvertDeduction(CodeType, ConvertNull(cdstTempCheckLines['AMOUNT'], 0));
                cdstTempCheckLines.Next;
              end;

              FieldByName('AMOUNT').Value := TempAmount * ConvertNull(SY_STATES.Lookup('SY_STATES_NBR',
                CO_STATES.Lookup('CO_STATES_NBR', EE_STATES.Lookup('EE_STATES_NBR',
                EEStateNbr, 'SDI_APPLY_CO_STATES_NBR'), 'SY_STATES_NBR'),  'EE_SDI_RATE'), 0);
              if FieldByName('AMOUNT').Value > SDITaxLimit then
                FieldByName('AMOUNT').Value := SDITaxLimit;
            end;
          end;
        end;
      CALC_METHOD_WASHINGTON_L_I:
        begin
          DM_COMPANY.CO_WORKERS_COMP.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
          FieldByName('AMOUNT').Value := 0;
          ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
          cdstTempCheckLines.First;
          while not cdstTempCheckLines.EOF do
          begin
            WorkCompNbr := cdstTempCheckLines['CO_WORKERS_COMP_NBR'];
            if VarIsNull(WorkCompNbr) then
            begin
              ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_RATES, {DM_PAYROLL.}PR.FieldByName('CO_NBR').AsInteger);
              EERateNbr := ConvertNull(DM_EMPLOYEE.EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([EmployeeNbr,
                Abs(cdstTempCheckLines.FieldByName('RATE_NUMBER').AsInteger)]), 'EE_RATES_NBR'), 0);
              if EERateNbr <> 0 then
                FillInDBDTBasedOnRate(EERateNbr, PR_CHECK_LINES, DM_EMPLOYEE.EE_RATES);
              TempNbr := cdstTempCheckLines.FieldByName('CO_JOBS_NBR').IsNull;
              if VarIsNull(TempNbr) then
                TempNbr := EE['CO_JOBS_NBR'];
              if not VarIsNull(TempNbr) then
              begin
                DM_COMPANY.CO_JOBS.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
                WorkCompNbr := DM_COMPANY.CO_JOBS.NewLookup('CO_JOBS_NBR', TempNbr, 'CO_WORKERS_COMP_NBR');
              end;
              if VarIsNull(WorkCompNbr) then
                WorkCompNbr := DM_EMPLOYEE.EE_RATES.NewLookup('EE_RATES_NBR', EERateNbr, 'CO_WORKERS_COMP_NBR');
              if VarIsNull(WorkCompNbr) then
                WorkCompNbr := {DM_EMPLOYEE.}EE.NewLookup('EE_NBR', EmployeeNbr, 'CO_WORKERS_COMP_NBR');
              if VarIsNull(WorkCompNbr) then
              begin
                TempNbr := cdstTempCheckLines['CO_TEAM_NBR'];
                if VarIsNull(TempNbr) then
                  TempNbr := EE['CO_TEAM_NBR'];
                if not VarIsNull(TempNbr) then
                begin
                  DM_COMPANY.CO_TEAM.DataRequired('ALL');
                  WorkCompNbr := DM_COMPANY.CO_TEAM.NewLookup('CO_TEAM_NBR', TempNbr, 'CO_WORKERS_COMP_NBR');
                end
                else
                begin
                  TempNbr := cdstTempCheckLines['CO_DEPARTMENT_NBR'];
                  if VarIsNull(TempNbr) then
                    TempNbr := EE['CO_DEPARTMENT_NBR'];
                  if not VarIsNull(TempNbr) then
                  begin
                    DM_COMPANY.CO_DEPARTMENT.DataRequired('ALL');
                    WorkCompNbr := DM_COMPANY.CO_DEPARTMENT.NewLookup('CO_DEPARTMENT_NBR', TempNbr, 'CO_WORKERS_COMP_NBR');
                  end
                  else
                  begin
                    TempNbr := cdstTempCheckLines['CO_BRANCH_NBR'];
                    if VarIsNull(TempNbr) then
                      TempNbr := EE['CO_BRANCH_NBR'];
                    if not VarIsNull(TempNbr) then
                    begin
                      DM_COMPANY.CO_BRANCH.DataRequired('ALL');
                      WorkCompNbr := DM_COMPANY.CO_BRANCH.NewLookup('CO_BRANCH_NBR', TempNbr, 'CO_WORKERS_COMP_NBR');
                    end
                    else
                    begin
                      TempNbr := cdstTempCheckLines['CO_DIVISION_NBR'];
                      if VarIsNull(TempNbr) then
                        TempNbr := EE['CO_DIVISION_NBR'];
                      if not VarIsNull(TempNbr) then
                      begin
                        DM_COMPANY.CO_DIVISION.DataRequired('ALL');
                        WorkCompNbr := DM_COMPANY.CO_DIVISION.NewLookup('CO_DIVISION_NBR', TempNbr, 'CO_WORKERS_COMP_NBR');
                      end;
                    end;
                  end;
                end;
              end;
              if VarIsNull(WorkCompNbr) then
                WorkCompNbr := DM_COMPANY.CO['CO_WORKERS_COMP_NBR'];
            end;

            if not VarIsNull(WorkCompNbr) then
            begin
              DM_CLIENT.CL_E_D_GROUP_CODES.Activate;

              TempNbr := DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', WorkCompNbr, 'CL_E_D_GROUPS_NBR');

              if DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([
                DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', WorkCompNbr, 'CL_E_D_GROUPS_NBR'), cdstTempCheckLines['CL_E_DS_NBR']]), []) then
              begin
                if EDType = ED_DED_WASHINGTON_L_I then
                  FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value + ConvertNull(cdstTempCheckLines['HOURS_OR_PIECES'], 0)
                  * ConvertNull(DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', WorkCompNbr, 'RATE'), 0);
                if EDType = ED_MEMO_WASHINGTON_L_I then
                  FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value + ConvertNull(cdstTempCheckLines['HOURS_OR_PIECES'], 0)
                  * ConvertNull(DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', WorkCompNbr, 'EXPERIENCE_RATING'), 0);
                if EDType = ED_MEMO_WY_WORKERS_COMP then
                begin
                  CodeType := CL_E_DS.Lookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
                  if CodeType[1] = 'D' then
                  begin
                    Q := TevQuery.Create('select SUBTRACT_DEDUCTIONS from CL_E_D_GROUPS where {AsOfDate<CL_E_D_GROUPS>} and CL_E_D_GROUPS_NBR='+IntToStr(TempNbr));
                    Q.Param['StatusDate'] := DM_CLIENT.PR.CHECK_DATE.Value;
                    if Q.Result.FieldByName('SUBTRACT_DEDUCTIONS').AsString = GROUP_BOX_YES then
                      FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value + ConvertDeduction(CodeType, ConvertNull(cdstTempCheckLines['AMOUNT'], 0))
                       * ConvertNull(DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', WorkCompNbr, 'EXPERIENCE_RATING'), 0);
                  end
                  else
                    FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value + ConvertDeduction(CodeType, ConvertNull(cdstTempCheckLines['AMOUNT'], 0))
                    * ConvertNull(DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', WorkCompNbr, 'EXPERIENCE_RATING'), 0);
                end;
              end;
            end;
            cdstTempCheckLines.Next;
          end;
        end;
      CALC_METHOD_NYCHILDSUPPORT:
        begin
          FieldByName('AMOUNT').Value :=GetNYChildSupportAmount;
          Exit;
        end;
    end;

    if ((EDType = ED_DED_REIMBURSEMENT) or (EDType = ED_MEMO_COBRA_CREDIT)) and (PR_CHECK_LINES.State <> dsInsert) then
      PR_CHECK_LINES['AMOUNT'] := PR_CHECK_LINES.FieldByName('AMOUNT').Value * (-1);

    GetLimitedTaxAndDedYTDforGTN(PR_CHECK['PR_CHECK_NBR'], EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value,
      ltDeduction, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, YTD);
    EDType2 := '';
    HasChildSupport := (EDType = ED_DED_CHILD_SUPPORT);
    if not TypeIsCatchUp(EDType) and not EE_SCHEDULED_E_DS.FieldByName('SCHEDULED_E_D_GROUPS_NBR').IsNull then
    begin
      EDList := TStringList.Create;
      try
        EDList.Add(EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsString);
        HasChildSupport := (EDType = ED_DED_CHILD_SUPPORT) or (EDType = ED_DED_GARNISH);
        TempNbr := EE_SCHEDULED_E_DS['SCHEDULED_E_D_GROUPS_NBR'];
        ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
        cdstTempCheckLines.First;
        while not cdstTempCheckLines.EOF do
        begin
          if (EDList.IndexOf(cdstTempCheckLines.FieldByName('CL_E_DS_NBR').AsString) = -1) then
          begin
            EDGroupTemp := ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', cdstTempCheckLines['EE_SCHEDULED_E_DS_NBR'], 'SCHEDULED_E_D_GROUPS_NBR'), -1);
            EDCodeTemp := CL_E_DS.Lookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
            if not TypeIsCatchUp(EDCodeTemp) and
            (EDGroupTemp = TempNbr) then
            begin
              RecNumber := cdstTempCheckLines.RecNo;
              GetLimitedTaxAndDedYTDforGTN(PR_CHECK['PR_CHECK_NBR'], cdstTempCheckLines.FieldByName('CL_E_DS_NBR').Value,
                ltDeduction, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, TempAmount);
              cdstTempCheckLines.RecNo := RecNumber;
              YTD := YTD + TempAmount;
              EDList.Add(cdstTempCheckLines.FieldByName('CL_E_DS_NBR').AsString);

              if not HasChildSupport then
                HasChildSupport := (CL_E_DS.Lookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DED_CHILD_SUPPORT)
                or (CL_E_DS.Lookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DED_GARNISH);
            end;
          end;
          cdstTempCheckLines.Next;
        end;

        RecNumber := EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR'];
        EE_SCHEDULED_E_DS.First;
        while not EE_SCHEDULED_E_DS.EOF do
        begin
          if (EDList.IndexOf(EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsString) = -1) and
          (EE_SCHEDULED_E_DS['SCHEDULED_E_D_GROUPS_NBR'] = TempNbr) and
          not TypeIsCatchUp(CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'E_D_CODE_TYPE')) then
          begin
            CheckDate := PR.Lookup('PR_NBR', PR_CHECK['PR_NBR'], 'CHECK_DATE');
            GetLimitedTaxAndDedYTD(0, 0, PR_CHECK['EE_NBR'], EE_SCHEDULED_E_DS['CL_E_DS_NBR'],
              ltDeduction, GetBeginYear(CheckDate), CheckDate, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, TempAmount);
            YTD := YTD + TempAmount;
            EDList.Add(EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsString);
          end;
          if TypeIsPension(EDType) then
          begin
            if CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'SD_ROUND'{Primary Pension For Limits}) = 'Y' then
              EDType2 := CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
          end;
          EE_SCHEDULED_E_DS.Next;
        end;
        EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', RecNumber, []);
      finally
        EDList.Free;
      end;
    end;

    if (EDType = ED_ST_CATCH_UP) or (EDType = ED_ST_ROTH_CATCH_UP)then
    begin
      EDType2 := ED_ST_CATCH_UP;
      EDList := TStringList.Create;
      try
        EDList.Add(EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsString);
        ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
        cdstTempCheckLines.First;
        while not cdstTempCheckLines.EOF do
        begin
          if (EDList.IndexOf(cdstTempCheckLines.FieldByName('CL_E_DS_NBR').AsString) = -1) then
          begin
            EDCodeTemp := CL_E_DS.Lookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
            if (EDCodeTemp = ED_ST_CATCH_UP) or (EDCodeTemp = ED_ST_ROTH_CATCH_UP) then
            begin
              RecNumber := cdstTempCheckLines.RecNo;
              GetLimitedTaxAndDedYTDforGTN(PR_CHECK['PR_CHECK_NBR'], cdstTempCheckLines.FieldByName('CL_E_DS_NBR').Value,
                ltDeduction, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, TempAmount);
              cdstTempCheckLines.RecNo := RecNumber;
              YTD := YTD + TempAmount;
              EDList.Add(cdstTempCheckLines.FieldByName('CL_E_DS_NBR').AsString);
            end;
          end;
          cdstTempCheckLines.Next;
        end;

        RecNumber := EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR'];
        EE_SCHEDULED_E_DS.First;
        while not EE_SCHEDULED_E_DS.EOF do
        begin
          if (EDList.IndexOf(EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsString) = -1) and
          ((CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_ST_CATCH_UP)
            or (CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_ST_ROTH_CATCH_UP))then
          begin
            CheckDate := PR.Lookup('PR_NBR', PR_CHECK['PR_NBR'], 'CHECK_DATE');
            GetLimitedTaxAndDedYTD(0, 0, PR_CHECK['EE_NBR'], EE_SCHEDULED_E_DS['CL_E_DS_NBR'],
              ltDeduction, GetBeginYear(CheckDate), CheckDate, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, TempAmount);
            YTD := YTD + TempAmount;
            EDList.Add(EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsString);
            EDType2 := CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
          end;
          EE_SCHEDULED_E_DS.Next;
        end;
        EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', RecNumber, []);
      finally
        EDList.Free;
      end;
    end;

    if FieldByName('AMOUNT').Value < EE_SCHEDULED_E_DS['MINIMUM_PAY_PERIOD_AMOUNT'] then
    begin
      FieldByName('AMOUNT').Value := EE_SCHEDULED_E_DS['MINIMUM_PAY_PERIOD_AMOUNT'];
    end
    else
    begin
      TempAmount := Abs(ConvertNull(EE_SCHEDULED_E_DS['MINIMUM_PAY_PERIOD_PERCENTAGE'], 0)) * 0.01
        * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['MIN_PPP_CL_E_D_GROUPS_NBR'], True, PR_CHECK);
      if (FieldByName('AMOUNT').Value < TempAmount) and (TempAmount <> 0) then
      begin
        FieldByName('AMOUNT').Value := TempAmount;
      end
      else
      begin
        TempAmount := ConvertNull(EE_SCHEDULED_E_DS['MAXIMUM_PAY_PERIOD_AMOUNT'], 0);
        if (TempAmount = 0) and not EE_SCHEDULED_E_DS.FieldByName('MAXIMUM_PAY_PERIOD_PERCENTAGE').IsNull then
        begin
          if not HasChildSupport then
            TempAmount := Abs(EE_SCHEDULED_E_DS['MAXIMUM_PAY_PERIOD_PERCENTAGE']) * 0.01
            * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['MAX_PPP_CL_E_D_GROUPS_NBR'], True, PR_CHECK)
          else
            if not EE_SCHEDULED_E_DS.FieldByName('THRESHOLD_E_D_GROUPS_NBR').IsNull then
              TempAmount := Abs(EE_SCHEDULED_E_DS['MAXIMUM_PAY_PERIOD_PERCENTAGE']) * 0.01
              * EarnCodeGroupCalc(EE_SCHEDULED_E_DS['THRESHOLD_E_D_GROUPS_NBR'], True, PR_CHECK)
            else
              TempAmount := Abs(EE_SCHEDULED_E_DS['MAXIMUM_PAY_PERIOD_PERCENTAGE']) * 0.01
              * DisposableEarnings(EE_SCHEDULED_E_DS['MAX_PPP_CL_E_D_GROUPS_NBR']);
        end;
        if TempAmount = 0 then
        begin
          TempAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'PAY_PERIOD_MAXIMUM_AMOUNT'), 0);
          if TempAmount = 0 then
          begin
            TempAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'],
              'PAY_PERIOD_MAXIMUM_PERCENT_OF'), 0);
            if TempAmount <> 0 then
            begin
              try
                EarnCodeGroupNbr := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'MAX_CL_E_D_GROUPS_NBR');
              except
                raise EInconsistentData.CreateHelp('Earn Code Group is not setup for maximum pay period percentage calculation for ' +
                  CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
              end;
              TempAmount := EarnCodeGroupCalc(EarnCodeGroupNbr, True, PR_CHECK) * TempAmount * 0.01;
            end;
          end;
        end;
        if (TempAmount = 0) and not EE_SCHEDULED_E_DS.FieldByName('MAX_AVG_AMT_CL_E_D_GROUPS_NBR').IsNull then
        begin
          try
            EarnCodeGroupNbr := EE_SCHEDULED_E_DS['MAX_AVG_HRS_CL_E_D_GROUPS_NBR'];
          except
            raise EInconsistentData.CreateHelp('Maximum Average Hours need to be setup for Average Hourly Wage calculation', IDH_InconsistentData);
          end;
          if EE_SCHEDULED_E_DS.FieldByName('MAX_AVERAGE_HOURLY_WAGE_RATE').IsNull then
            raise EInconsistentData.CreateHelp('Maximum Average Hourly Wage Rate needs to be setup for Average Hourly Wage calculation', IDH_InconsistentData);
          TempAmount := EarnCodeGroupCalc(EarnCodeGroupNbr, False, PR_CHECK);
          if TempAmount <> 0 then
            TempAmount := EarnCodeGroupCalc(EE_SCHEDULED_E_DS['MAX_AVG_AMT_CL_E_D_GROUPS_NBR'], True, PR_CHECK) /
              TempAmount * EE_SCHEDULED_E_DS['MAX_AVERAGE_HOURLY_WAGE_RATE'];
        end;
        if TempAmount <> 0 then
        begin
          if ProrateNbr > 1 then
          begin
            if FieldByName('AMOUNT').Value / ProrateRatio > TempAmount then
              FieldByName('AMOUNT').Value := RoundTwo(TempAmount * ProrateRatio);
          end
          else
          begin
            if (EDType = ED_DED_NM_WORKERS_COMP) or (EDType = ED_MEMO_NM_WORKERS_COMP) then
            begin
              GetLimitedTaxAndDedYTDforGTN(PR_CHECK['PR_CHECK_NBR'], EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value,
                ltDeduction, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, ScheduledGroupAmount,
                GetBeginQuarter(PR.FieldByName('CHECK_DATE').AsDateTime), GetEndQuarter(PR.FieldByName('CHECK_DATE').AsDateTime));
              ScheduledGroupAmount := ScheduledGroupAmount + FieldByName('AMOUNT').Value;
            end
            else
              ScheduledGroupAmount := ScheduledGroupCalc(EE_SCHEDULED_E_DS['SCHEDULED_E_D_GROUPS_NBR']);
            if ScheduledGroupAmount > TempAmount then
              FieldByName('AMOUNT').Value := TempAmount - (ScheduledGroupAmount - FieldByName('AMOUNT').Value);
          end;
        end;
      end;
    end;

    if GetAnnualMax(EE_SCHEDULED_E_DS['ANNUAL_MAXIMUM_AMOUNT'], EDType, EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value) <> 0 then
    begin
      if Abs(FieldByName('AMOUNT').Value + YTD) > Abs(GetAnnualMax(EE_SCHEDULED_E_DS['ANNUAL_MAXIMUM_AMOUNT'], EDType, EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value)) then
        FieldByName('AMOUNT').Value := GetAnnualMax(EE_SCHEDULED_E_DS['ANNUAL_MAXIMUM_AMOUNT'], EDType, EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value) - YTD;
    end
    else
    begin
      TempAmount := ConvertNull(CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'ANNUAL_MAXIMUM'), 0);
      if TempAmount <> 0 then
      begin
        TempAmount := GetAnnualMax(TempAmount, EDType, EE_SCHEDULED_E_DS['CL_E_DS_NBR']);
        if Abs(FieldByName('AMOUNT').Value + YTD) > Abs(TempAmount) then
          FieldByName('AMOUNT').Value := TempAmount - YTD;
      end
      else
      begin
        if TypeIsPension(EDType) or TypeIsHSA(EDType) or (EDType2 = ED_ST_DEPENDENT_CARE)
        or (EDType2 = ED_ST_HSA_SINGLE) or (EDType2 = ED_ST_HSA_FAMILY)
        or (EDType2 = ED_EWOD_TAXABLE_ER_HSA_SINGLE) or (EDType2 = ED_EWOD_TAXABLE_ER_HSA_FAMILY)
        or (EDType2 = ED_MEMO_ER_HSA_SINGLE) or (EDType2 = ED_MEMO_ER_HSA_FAMILY)
        or (EDType2 = ED_ST_CATCH_UP) or (EDType2 = ED_ST_ROTH_CATCH_UP) then
        begin
          if EDType2 = '' then
            EDType2 := EDType;
          PensionLimit := GetPensionLimit(EDType2);
          if TypeIsCatchUp(EDType2) then
          begin
            PensionLimit := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_CATCH_UP_LIMIT'), 0);
            if EDType2 = ED_ST_SIMPLE_CATCH_UP then
            begin
              if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2014, 12, 31)) = GreaterThanValue) then
                PensionLimit := 3000
              else
                PensionLimit := 2500; // This is new for 2009. Used to be
              // PensionLimit := PensionLimit * 0.5;
            end;
          end;
          if EDType2 = ED_ST_DEPENDENT_CARE then
            PensionLimit := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_DEPENDENT_CARE_LIMIT'), 0);
          if (EDType2 = ED_ST_HSA_SINGLE) or (EDType2 = ED_EWOD_TAXABLE_ER_HSA_SINGLE) or (EDType2 = ED_MEMO_ER_HSA_SINGLE) then
            PensionLimit := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_HSA_SINGLE_LIMIT'), 0);
          if (EDType2 = ED_ST_HSA_FAMILY) or (EDType2 = ED_EWOD_TAXABLE_ER_HSA_FAMILY) or (EDType2 = ED_MEMO_ER_HSA_FAMILY) then
            PensionLimit := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_HSA_FAMILY_LIMIT'), 0);
          if EDType2 = ED_ST_HSA_CATCH_UP then
            PensionLimit := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_HSA_CATCH_UP_LIMIT'), 0);
          if (FieldByName('AMOUNT').Value + YTD > PensionLimit) and (PensionLimit <> 0) then
            FieldByName('AMOUNT').Value := PensionLimit - YTD;
         end;
      end;
    end;

// Fund Splits
    ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_PENSION_FUND_SPLITS, PR.FieldByName('CO_NBR').AsInteger);
    if DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Locate('EE_NBR;CL_E_DS_NBR',
    VarArrayOf([EmployeeNbr, EE_SCHEDULED_E_DS['CL_E_DS_NBR']]), []) then
    begin
      FundType := DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.FieldByName('EMPLOYEE_OR_EMPLOYER').AsString[1];

      DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Filter := 'EE_NBR=' + IntToStr(EmployeeNbr) +
        ' and EMPLOYEE_OR_EMPLOYER=''' + FundType + '''';
      DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Filtered := True;
      DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.IndexFieldNames := 'EE_PENSION_FUND_SPLITS_NBR';

      DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Last;
      if DM_EMPLOYEE.EE_PENSION_FUND_SPLITS['CL_E_DS_NBR'] = EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value then
      begin
        TempAmount := 0;
        DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.First;
        while not DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.EOF do
        begin
          TempAmount := TempAmount + RoundTwo(FieldByName('AMOUNT').Value * DM_EMPLOYEE.EE_PENSION_FUND_SPLITS['PERCENTAGE'] * 0.01);
          DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Next;
        end;
        FieldByName('AMOUNT').Value := RoundTwo(FieldByName('AMOUNT').Value - TempAmount +
          RoundTwo(FieldByName('AMOUNT').Value * DM_EMPLOYEE.EE_PENSION_FUND_SPLITS['PERCENTAGE'] * 0.01));
      end
      else
      begin
        DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Locate('EE_NBR;CL_E_DS_NBR;EMPLOYEE_OR_EMPLOYER',
        VarArrayOf([EmployeeNbr, EE_SCHEDULED_E_DS['CL_E_DS_NBR'], FundType]), []);
        FieldByName('AMOUNT').Value := RoundTwo(FieldByName('AMOUNT').Value * DM_EMPLOYEE.EE_PENSION_FUND_SPLITS['PERCENTAGE'] * 0.01);
      end;
      DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.Filtered := False;
    end;
// End Fund Splits
    if FieldByName('AMOUNT').IsNull then
      Exit;
    if ((EDType = ED_DED_GARNISH) or (EDType = ED_DED_CHILD_SUPPORT)) and (FieldByName('AMOUNT').Value < 0) then
      FieldByName('AMOUNT').Value := 0;
// Prorate Child Supports
    if (ProrateNbr > 1) and (FieldByName('AMOUNT').Value / ProrateRatio > OldNet) then
    begin
      FieldByName('AMOUNT').Value := RoundTwo(OldNet * ProrateRatio);
      if Abs(FieldByName('AMOUNT').Value - Net) < 1 then
        FieldByName('AMOUNT').Value := Net;
    end;
// Deal with targets
    if EDType[1] = 'D' then
      TempPercentage := 1
    else
      TempPercentage := -1;
    TempAmount := FieldByName('AMOUNT').Value;
    if (FieldByName('AMOUNT').Value * TempPercentage > Net) and (FieldByName('AMOUNT').Value * TempPercentage - Net >= 0.005) then
    begin
      if DM_EMPLOYEE.EE_SCHEDULED_E_DS.DEDUCTIONS_TO_ZERO.Value = 'Y' then
        FieldByName('AMOUNT').Value := Net * TempPercentage
      else
        FieldByName('AMOUNT').Value := 0;
    end;
    TempPercentage := FieldByName('AMOUNT').Value;
    Result := AnalyzeTargets(EE, EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK, EmployeeNbr, JustPreProcessing);
    if Result and (TempPercentage = FieldByName('AMOUNT').Value) then
      FieldByName('AMOUNT').Value := TempAmount;
  end;
end;

function TevPayrollCalculation.AnalyzeTargets(EE, EE_SCHEDULED_E_DS, PR_CHECK_LINES, PR_CHECK: TevClientDataSet; EmployeeNbr: Integer; JustPreProcessing: Boolean; AdjustBalanceOnly: Boolean = False): Boolean;
var
  Balance, Amount, TargetAmount: Double;
  TempAmount: Double;
  TargetMet: Boolean;
  Q: IevQuery;
begin
  Result := True;
  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;

  if EE_SCHEDULED_E_DS['TARGET_ACTION'] = SCHED_ED_TARGET_REMOVE then
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('NbrField', 'EE_SCHEDULED_E_DS');
      SetMacro('Columns', '*');
      SetMacro('TableName', 'EE_SCHEDULED_E_DS');
      SetParam('RecordNbr', EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR']);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    ctx_DataAccess.CUSTOM_VIEW.First;
    if Abs(ConvertNull(ctx_DataAccess.CUSTOM_VIEW['BALANCE'], 0)) >= Abs(ConvertNull(EE_SCHEDULED_E_DS.FieldByName('TARGET_AMOUNT').Value, 0)) then
    begin
      PR_CHECK_LINES.Cancel;
      Result := False;
      Exit;
    end;
  end;

  if ((EmployeeNbr = 0) and not JustPreProcessing or (EmployeeNbr <> 0))
  and (ConvertNull(EE_SCHEDULED_E_DS['TARGET_AMOUNT'], 0) <> 0) then
  begin
    if not ctx_DataAccess.CUSTOM_VIEW.Active then
    begin
      with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
      begin
        SetMacro('NbrField', 'EE_SCHEDULED_E_DS');
        SetMacro('Columns', '*');
        SetMacro('TableName', 'EE_SCHEDULED_E_DS');
        SetParam('RecordNbr', EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
    end;

    TempAmount := 0;
    ApplyTempCheckLinesCheckRange(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
    cdstTempCheckLines.Last;
    while not cdstTempCheckLines.BOF do
    begin
      if cdstTempCheckLines.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
      begin
        Q := TevQuery.Create('select EE_SCHEDULED_E_DS_NBR from EE_SCHEDULED_E_DS where EE_NBR='+EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsString+
          ' and CL_E_DS_NBR='+cdstTempCheckLines.FieldByName('CL_E_DS_NBR').AsString+' and {AsOfNow<EE_SCHEDULED_E_DS>}');
        if Q.Result.RecordCount > 0 then
        begin
          if (Q.Result['EE_SCHEDULED_E_DS_NBR']= EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR'])
          and (cdstTempCheckLines['PR_CHECK_LINES_NBR'] <> PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value) then
            TempAmount := TempAmount + ConvertNull(cdstTempCheckLines['AMOUNT'], 0);
        end;
      end
      else
      begin
        if (cdstTempCheckLines['EE_SCHEDULED_E_DS_NBR'] = EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').Value)
        and (cdstTempCheckLines['PR_CHECK_LINES_NBR'] <> PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value) then
          TempAmount := TempAmount + ConvertNull(cdstTempCheckLines['AMOUNT'], 0);
      end;    
      cdstTempCheckLines.Prior;
    end;

    if not JustPreProcessing and (PR_CHECK.FieldByName('UPDATE_BALANCE').AsString <> GROUP_BOX_NO) then
    begin
      EE_SCHEDULED_E_DS.Edit;
      EE_SCHEDULED_E_DS['BALANCE'] := RoundTwo(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.FieldByName('BALANCE').Value, 0))
        + RoundTwo(ConvertNull(PR_CHECK_LINES['AMOUNT'], 0)) + RoundTwo(TempAmount);
      if AdjustBalanceOnly then
        Exit;  
    end;

    Balance      := RoundTwo(ConvertNull(ctx_DataAccess.CUSTOM_VIEW['BALANCE'],0));
    Amount       := RoundTwo(ConvertNull(PR_CHECK_LINES['AMOUNT'], 0));
    TargetAmount := RoundTwo(ConvertNull(EE_SCHEDULED_E_DS['TARGET_AMOUNT'], 0));

    TargetMet := False;

    if TargetAmount < -0.005 then
    begin
      TargetMet := DoubleCompare(Balance + Amount + TempAmount, coLessOrEqual, TargetAmount);
      if Balance < TargetAmount then
        Balance := TargetAmount;
    end
    else
    if TargetAmount > 0.005 then
    begin
      TargetMet := DoubleCompare(Balance + Amount + TempAmount, coGreaterOrEqual, TargetAmount);
      if Balance > TargetAmount then
        Balance := TargetAmount;
    end;

    if TargetMet then
      Amount := TargetAmount - Balance - TempAmount;

    if (Abs(TargetAmount) > 0.005) and (not PR_CHECK_LINES.FieldByName('RATE_OF_PAY').IsNull) then
      PR_CHECK_LINES['RATE_OF_PAY'] := Null;

    if TargetMet then
    begin
      if EE_SCHEDULED_E_DS.FieldByName('TARGET_ACTION').IsNull or (EE_SCHEDULED_E_DS.FieldByName('TARGET_ACTION').AsString = SCHED_ED_TARGET_NONE) then
        raise ENoTargetActionSpecified.CreateHelp('The target for scheduled E/D has been reached for employee #' +
          Trim(VarToStr(EE.Lookup('EE_NBR', EE_SCHEDULED_E_DS['EE_NBR'], 'CUSTOM_EMPLOYEE_NUMBER')))
            + ', but there is no target action specified.', IDH_InconsistentData);

      PR_CHECK_LINES['AMOUNT'] := Amount;
      if not JustPreProcessing and (PR_CHECK.FieldByName('UPDATE_BALANCE').AsString <> GROUP_BOX_NO) then
      begin
        EE_SCHEDULED_E_DS['BALANCE'] := RoundTwo(EE_SCHEDULED_E_DS.FieldByName('TARGET_AMOUNT').Value);
        if (EE_SCHEDULED_E_DS.FieldByName('TARGET_ACTION').AsString = SCHED_ED_TARGET_RESET)
        or (EE_SCHEDULED_E_DS.FieldByName('TARGET_ACTION').AsString = SCHED_ED_TARGET_RESET_WITH_CHECK) then
        begin
          EE_SCHEDULED_E_DS['BALANCE'] := 0;
          if not ctx_DataAccess.CUSTOM_VIEW.FieldByName('NUMBER_OF_TARGETS_REMAINING').IsNull then
          begin
            EE_SCHEDULED_E_DS.FieldByName('NUMBER_OF_TARGETS_REMAINING').AsInteger := ctx_DataAccess.CUSTOM_VIEW.FieldByName('NUMBER_OF_TARGETS_REMAINING').AsInteger - 1;
            if EE_SCHEDULED_E_DS.FieldByName('NUMBER_OF_TARGETS_REMAINING').AsInteger <= 0 then
              EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
          end;
        end
        else
          EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'] := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
      end;
      if (EE_SCHEDULED_E_DS['TARGET_ACTION'] = SCHED_ED_TARGET_REMOVE) and (PR_CHECK_LINES.FieldByName('AMOUNT').Value = 0) then
      begin
        PR_CHECK_LINES.Cancel;
        Result := False;
      end;
    end;
    if not JustPreProcessing and (PR_CHECK.FieldByName('UPDATE_BALANCE').AsString <> GROUP_BOX_NO) then
      EE_SCHEDULED_E_DS.Post;
  end;
end;

procedure TevPayrollCalculation.CopyPayroll(OldPayrollNbr: Integer);
begin
  ctx_PayrollProcessing.CopyPayroll(OldPayrollNbr);
end;

procedure TevPayrollCalculation.VoidPayroll(OldPayrollNbr: Integer);
var
  VoidTaxChecks, VoidAgencyChecks, VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling: Boolean;
begin

// Ask a few questions
  case EvMessage('Would you like to void agency checks?', mtConfirmation, mbYesNoCancel) of
    mrYes: VoidAgencyChecks := True;
    mrNo: VoidAgencyChecks := False;
  else
    Exit;
  end;

  case EvMessage('Would you like to void tax checks?', mtConfirmation, mbYesNoCancel) of
    mrYes: VoidTaxChecks := True;
    mrNo: VoidTaxChecks := False;
  else
    Exit;
  end;

  case EvMessage('Would you like to void billing checks?', mtConfirmation, mbYesNoCancel) of
    mrYes: VoidBillingChecks := True;
    mrNo: VoidBillingChecks := False;
  else
    Exit;
  end;

  case EvMessage('Would you like to print reports?', mtConfirmation, mbYesNoCancel) of
    mrYes: PrintReports := True;
    mrNo: PrintReports := False;
  else
    Exit;
  end;

  case EvMessage('Would you like to keep invoice?', mtConfirmation, mbYesNoCancel) of
    mrYes: DeleteInvoice := False;
    mrNo: DeleteInvoice := True;
  else
    Exit;
  end;

  case EvMessage('Would you like to block billing?', mtConfirmation, mbYesNoCancel) of
    mrYes: BlockBilling := True;
    mrNo: BlockBilling := False;
  else
    Exit;
  end;

  ctx_PayrollProcessing.VoidPayroll(OldPayrollNbr,VoidTaxChecks, VoidAgencyChecks, VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling);
end;

function TevPayrollCalculation.EarnCodeGroupCalc(GroupNbr: Variant; CalcAmount: Boolean; PR_CHECK: TevClientDataSet): Double;
var
  EDCodeType: String;
begin
  Result := 0;
  if VarIsNull(GroupNbr) then
    Exit;
  DM_CLIENT.CL_E_D_GROUPS.Activate;
  DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
  DM_CLIENT.CL_E_DS.Activate;
  ApplyTempCheckLinesCheckRange(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
  cdstTempCheckLines.First;
  while not cdstTempCheckLines.EOF do
  begin
    if DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([GroupNbr, cdstTempCheckLines['CL_E_DS_NBR']]), []) then
    begin
      if DM_CLIENT.CL_E_D_GROUPS.NewLookup('CL_E_D_GROUPS_NBR', GroupNbr, 'SUBTRACT_DEDUCTIONS') = GROUP_BOX_NO then
      begin
        if CalcAmount then
          Result := Result + ConvertNull(cdstTempCheckLines['AMOUNT'], 0)
        else
          Result := Result + ConvertNull(cdstTempCheckLines['HOURS_OR_PIECES'], 0);
      end
      else
      begin
        EDCodeType := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
        if CalcAmount then
          Result := Result + ConvertDeduction(EDCodeType, ConvertNull(cdstTempCheckLines['AMOUNT'], 0))
        else
          Result := Result + ConvertDeduction(EDCodeType, ConvertNull(cdstTempCheckLines['HOURS_OR_PIECES'], 0));
      end;
    end;
    cdstTempCheckLines.Next;
  end;
end;

function TevPayrollCalculation.GetCompanyFrequencies(CompanyDataSet: TevClientDataSet): Char;
begin
  try
    Result := CompanyDataSet.FieldByName('PAY_FREQUENCIES').AsString[1];
  except
    raise EInconsistentData.CreateHelp('The company does not have pay frequencies setup.', IDH_InconsistentData);
  end;
end;

function TevPayrollCalculation.ListBackdatedPayrollProblems(CheckDate: TDateTime): string;
var
  BeginDate, EndDate, WhenDue: TDateTime;
  WarningList: TStringList;
begin
  WarningList := TStringList.Create;
  try
// Check Federal Tax Deposits
    DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.DataRequired('ALL');
    GetFedTaxDepositPeriod(DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString[1], CheckDate, BeginDate, EndDate, WhenDue);
    if WhenDue - 1 < Date then
      WarningList.Add('Possibility of a late penalty for Federal.')
    else
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then Close;
        with TExecDSWrapper.Create('SumTaxLiabilities') do
        begin
          SetParam('Fein', DM_COMPANY.CO.FieldByName('FEIN').AsString);
          SetParam('DueDate', WhenDue);
          SetParam('TaxType', 'F');
          SetMacro('Level', 'FED');
          DataRequest(AsVariant);
        end;
        Open;
        First;
        DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
        if not Fields[0].IsNull then
          if Fields[0].Value > DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE['FED_DEPOSIT_FREQ_THRESHOLD'] then
            WarningList.Add('Possibility of a late penalty for Federal.');
        Close;
      end;
// Check States Tax Deposits
    DM_COMPANY.CO_STATES.Activate;
    DM_COMPANY.CO_STATES.First;
    while not DM_COMPANY.CO_STATES.EOF do
    begin
      if DM_COMPANY.CO_STATES['CO_NBR'] = DM_COMPANY.CO.FieldByName('CO_NBR').Value then
      begin
        GetStateTaxDepositPeriod(DM_COMPANY.CO_STATES['STATE'],
          DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'], CheckDate, BeginDate, EndDate, WhenDue);
        if WhenDue - 1 < Date then
          WarningList.Add('Possibility of a late penalty for ' + DM_COMPANY.CO_STATES['STATE'] + '.')
        else
          with ctx_DataAccess.CUSTOM_VIEW do
          begin
            if Active then Close;
            with TExecDSWrapper.Create('SumTaxLiabilities') do
            begin
              SetParam('Fein', DM_COMPANY.CO.FieldByName('FEIN').AsString);
              SetParam('DueDate', WhenDue);
              SetParam('TaxType', 'S');
              SetMacro('Level', 'STATE');
              DataRequest(AsVariant);
            end;
            Open;
            First;
            if not Fields[0].IsNull then
              if CheckStateTaxDeposit(DM_COMPANY.CO_STATES['STATE'], Fields[0].Value) then
                WarningList.Add('Possibility of a late penalty for ' + DM_COMPANY.CO_STATES['STATE'] + '.');
            Close;
          end;
      end;
      DM_COMPANY.CO_STATES.Next;
    end;
// Check Local Tax Deposits
    DM_COMPANY.CO_LOCAL_TAX.Activate;
    DM_COMPANY.CO_LOCAL_TAX.First;
    while not DM_COMPANY.CO_LOCAL_TAX.EOF do
    begin
      if DM_COMPANY.CO_LOCAL_TAX['CO_NBR'] = DM_COMPANY.CO.FieldByName('CO_NBR').Value then
      begin
        GetLocalTaxDepositPeriod(DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'],
          DM_COMPANY.CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR'], CheckDate, BeginDate, EndDate, WhenDue);
        if WhenDue - 1 < Date then
          WarningList.Add('Possibility of a late penalty for ' + DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR',
            DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], 'NAME') + '.');
      end;
      DM_COMPANY.CO_LOCAL_TAX.Next;
    end;
    if (GetYear(CheckDate) <> GetYear(Date)) or (GetQuarterNumber(CheckDate) <> GetQuarterNumber(Date)) then
      WarningList.Add('This is a prior quarter payroll!');
// Check tax return queue
    {Activate(DM_COMPANY.CO_TAX_RETURN_QUEUE);
    if not VarIsNull(DM_COMPANY.CO_TAX_RETURN_QUEUE.Lookup('PERIOD_END_DATE;STATUS',
      VarArrayOf([GetEndQuarter(CheckDate), DELIVERY_STATUS_PROCESSED]), 'SY_GL_AGENCY_REPORT_NBR')) then
      WarningList.Add('Quarterlies have been processed for this payroll!');}
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      Close;
      with TExecDSWrapper.Create('CheckReturnQueueForProcessedReturns') do
      begin
        SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
        SetMacro('Dates', ''''+ DateToStr(GetEndMonth(CheckDate))+''','''+
          DateToStr(GetEndQuarter(CheckDate))+''','''+
          DateToStr(GetEndYear(CheckDate))+'''');
        DataRequest(AsVariant);
      end;
      Open;
      if RecordCount > 0 then
        WarningList.Add('Some tax forms have been processed for this payroll!');
      Close;
    end;
    Result := WarningList.Text;
  finally
    WarningList.Free;
  end;
end;

function TevPayrollCalculation.GetNextRunNumber(CheckDate: TDateTime): Integer;
begin
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;
    with TExecDSWrapper.Create('NextRunNumberForCheckDate') do
    begin
      SetParam('CheckDate', CheckDate);
      SetParam('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      DataRequest(AsVariant);
    end;
    Open;
    First;
    Result := ConvertNull(Fields[0].Value, 0);
    Close;
    Result := Result + 1;
  end;
end;

procedure TevPayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
  PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES,
  CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet; DoPrePost, PaySalary, PayHourly: Boolean;
  RefreshScheduledEDs: Boolean = False; CreatingNewBatch: Boolean = False; const DontPost: Boolean = False);
var
  N: Integer;
begin
  try
    if (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR) or ((PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) and RefreshScheduledEDs) or
      (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_COBRA_CREDIT) then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if (LastNewBatchNbr <> PR_BATCH['PR_BATCH_NBR']) or (not CreatingNewBatch) then
      begin
        LastNewBatchNbr := PR_BATCH['PR_BATCH_NBR'];

        FollowingCheckDate := GetNextCheckDate(PR['CHECK_DATE'], CO.FieldByName('PAY_FREQUENCIES').AsString[1]);

        if Active then
          Close;
        with TExecDSWrapper.Create('MaxCheckDateForCo') do
        begin
          SetParam('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
          SetParam('Co_Nbr', CO.FieldByName('CO_NBR').AsInteger);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        PriorCheckDate := ConvertNull(FieldByName('MAX_CHECK_DATE').Value, 0);
        Close;

        with TExecDSWrapper.Create('PrNbrForMaxCheckDateForCo') do
        begin
          SetParam('CheckDate', PriorCheckDate);
          SetParam('Co_Nbr', CO.FieldByName('CO_NBR').AsInteger);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        N := ConvertNull(FieldByName('PR_NBR').Value, 0);
        Close;
        with TExecDSWrapper.Create('SelectDistinctEDForPR') do
        begin
          SetParam('Pr_Nbr', N);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        PriorPayrollEDs.Clear;
        while not EOF do
        begin
          PriorPayrollEDs.Add(FieldByName('EE_SCHEDULED_E_DS_NBR').AsString);
          Next;
        end;
        Close;
      end;
      Generic_CreateRegularPRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
        PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES,
        CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES, PriorCheckDate, FollowingCheckDate,
        PriorPayrollEDs, DoPrePost, PaySalary, PayHourly, RefreshScheduledEDs, CreatingNewBatch);
    end;

    if RefreshScheduledEDs then
      Exit;

    if PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD] then
    begin
      if PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').AsString = 'Y' then
      begin
        if not PR_CHECK_LINES.Active then
          PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        DoAutoPost(PR_CHECK, PR_CHECK_LINES, DM_EMPLOYEE.EE, CO_E_D_CODES, DM_COMPANY.CO_TEAM_PR_BATCH_DEFLT_ED,
          DM_COMPANY.CO_DEPT_PR_BATCH_DEFLT_ED, DM_COMPANY.CO_BRCH_PR_BATCH_DEFLT_ED, DM_COMPANY.CO_DIV_PR_BATCH_DEFLT_ED,
          DM_COMPANY.CO_PR_BATCH_DEFLT_ED);
      end;
    end;

    if PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_VOID then
      Generic_CreateVoidPRCheckDetails(StrToInt(ExtractFromFiller(ConvertNull(PR_CHECK['FILLER'], ''), 1, 8)),
        PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, DontPost);
  finally
    ctx_DataAccess.NewCheckInserted := False;
  end;
end;

procedure TevPayrollCalculation.Generic_CreateRegularPRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
  PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
  EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet; ProirScheduledCheckDate,
  NextScheduledCheckDate: TDateTime; ScheduledEDsInPriorPayroll: TStringList; DoPrePost, PaySalary, PayHourly: Boolean;
  RefreshScheduledEDs: Boolean = False; CreatingNewBatch: Boolean = False);
var
  EmployeeNbr, N, GTLTimes: Integer;
  DBDTLevel, EDType, ScheduledEDFreq: string;
  CheckAmount, PolicyAmount: Real;
  CalendarDate: TDateTime;
  CalendarDateStr: String;
  TempVar: Variant;

  procedure DoPrePostCheck;
  begin
    if not DoPrePost then
      Exit;

    DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired('ALL');
    DM_CLIENT.CL_PIECES.DataRequired('ALL');
    DM_COMPANY.CO_SHIFTS.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
    DM_EMPLOYEE.EE_RATES.DataRequired('ALL');
    DM_EMPLOYEE.EE_WORK_SHIFTS.DataRequired('ALL');

    CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS);
    CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, DM_CLIENT.CL_E_D_GROUP_CODES, CL_PERSON, DM_CLIENT.CL_PIECES,
      DM_COMPANY.CO_SHIFTS, CO_STATES, EE, EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_RATES, EE_STATES, DM_EMPLOYEE.EE_WORK_SHIFTS,
      SY_FED_TAX_TABLE, SY_STATES);
    if not PR_CHECK_LINES.FieldByName('AMOUNT').IsNull then
      PR_CHECK_LINES['AMOUNT'] := RoundTwo(PR_CHECK_LINES.FieldByName('AMOUNT').Value);
  end;

begin
  CheckAmount := 0;
  EmployeeNbr := PR_CHECK['EE_NBR'];

  DBDTLevel := CO['DBDT_LEVEL'];

  if not PR_CHECK_LINES.Active then
    PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
  if PaySalary and (ConvertNull(EE.NewLookup('EE_NBR', EmployeeNbr, 'SALARY_AMOUNT'), 0) <> 0) then
  begin
    try
      N := GetCompanyED(ED_OEARN_SALARY);
    except
      raise EInconsistentData.CreateHelp('Salary earning is not setup properly for the company! Can not create check.', IDH_InconsistentData);
    end;
    if not EDIsBlockedForPR(PR.FieldByName('PR_NBR').AsInteger, PR.FieldByName('CO_NBR').AsInteger, N, PR_SCHEDULED_E_DS, CO_E_D_CODES)
      then
    begin
      PR_CHECK_LINES.Insert;
      PR_CHECK_LINES['CL_E_DS_NBR'] := N;
      if ConvertNull(PR_CHECK['SALARY'], 0) = 0 then
        PR_CHECK_LINES['AMOUNT'] := EE.Lookup('EE_NBR', EmployeeNbr, 'SALARY_AMOUNT')
      else
        PR_CHECK_LINES['AMOUNT'] := PR_CHECK.FieldByName('SALARY').Value;
      CheckAmount := CheckAmount + PR_CHECK_LINES['AMOUNT'];
      PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_SCHEDULED;
      if EE.FieldByName('WORKATHOME').AsString = 'Y' then
        PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString := 'Y';
      DoPrePostCheck;
      PR_CHECK_LINES.Post;
    end;
  end;
  if PayHourly and (not RefreshScheduledEDs) and (PaySalary or (not PaySalary and (ConvertNull(EE.NewLookup('EE_NBR', EmployeeNbr, 'SALARY_AMOUNT'), 0) = 0))) then
  begin
    try
      N := GetCompanyED(ED_OEARN_REGULAR);
    except
      raise EInconsistentData.CreateHelp('Regular earning is not setup properly for the company! Can not create check.', IDH_InconsistentData);
    end;
    if not EDIsBlockedForPR(PR.FieldByName('PR_NBR').AsInteger, PR.FieldByName('CO_NBR').AsInteger, N, PR_SCHEDULED_E_DS, CO_E_D_CODES) then
    begin
      PR_CHECK_LINES.Insert;
      PR_CHECK_LINES['CL_E_DS_NBR'] := N;
      PR_CHECK_LINES['HOURS_OR_PIECES'] := EE.NewLookup('EE_NBR', EmployeeNbr, 'STANDARD_HOURS');
      if EE.FieldByName('WORKATHOME').AsString = 'Y' then
        PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString := 'Y';
      DoPrePostCheck;
      PR_CHECK_LINES.Post;
      CheckAmount := CheckAmount + ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
    end;
  end;

  if not ctx_DataAccess.NewBatchInserted and
     (not EE_SCHEDULED_E_DS.Active or
      (EE_SCHEDULED_E_DS.OpenCondition <> '') and
      (EE_SCHEDULED_E_DS.OpenCondition <> 'EE_NBR=' + IntToStr(EmployeeNbr))) then
    EE_SCHEDULED_E_DS.DataRequired('EE_NBR=' + IntToStr(EmployeeNbr));
  EE_SCHEDULED_E_DS.Filter := 'EE_NBR=' + IntToStr(EmployeeNbr);
  EE_SCHEDULED_E_DS.Filtered := True;
  EE_SCHEDULED_E_DS.IndexFieldNames := 'PRIORITY_NUMBER';
  EE_SCHEDULED_E_DS.First;
  while not EE_SCHEDULED_E_DS.EOF do
  begin
    if EDIsBlockedForPR(PR.FieldByName('PR_NBR').AsInteger, PR.FieldByName('CO_NBR').AsInteger,
      EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger, PR_SCHEDULED_E_DS, CO_E_D_CODES) then
    begin
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;
    if PR['CHECK_DATE'] < EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').Value then
    begin
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;
    if (PR['CHECK_DATE'] > EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').Value)
      and (not VarIsNull(EE_SCHEDULED_E_DS['EFFECTIVE_END_DATE'])) then
    begin
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;
    if (PR_CHECK['EXCLUDE_DD'] = 'Y') and (not EE_SCHEDULED_E_DS.FieldByName('EE_DIRECT_DEPOSIT_NBR').IsNull) then
    begin
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;
    if PR_CHECK['EXCLUDE_DD_EXCEPT_NET'] = 'Y' then
    begin
      if (not EE_SCHEDULED_E_DS.FieldByName('EE_DIRECT_DEPOSIT_NBR').IsNull) and (EE_SCHEDULED_E_DS.FieldByName('DEDUCT_WHOLE_CHECK').Value = 'N') then
      begin
        EE_SCHEDULED_E_DS.Next;
        Continue;
      end;
    end;
    if PR_CHECK['EXCLUDE_ALL_SCHED_E_D_CODES'] = 'Y' then
    begin
      // The name is misleading. We also don't want to exclude DD
      if EE_SCHEDULED_E_DS.FieldByName('EE_DIRECT_DEPOSIT_NBR').IsNull then
      begin
        EE_SCHEDULED_E_DS.Next;
        Continue;
      end;
    end;
    if PR_CHECK['EXCLUDE_SCH_E_D_EXCEPT_PENSION'] = 'Y' then
    begin
      // The name is misleading. We also don't want to exclude DD
      if EE_SCHEDULED_E_DS.FieldByName('EE_DIRECT_DEPOSIT_NBR').IsNull then
      begin
        TempVar := EE_SCHEDULED_E_DS['CL_E_DS_NBR'];
        if not VarIsNull(TempVar) then
        begin
          CL_E_DS.Activate;
          if not TypeIsPension(VarToStr(CL_E_DS.Lookup('CL_E_DS_NBR', TempVar, 'E_D_CODE_TYPE'))) then
          if (CL_E_DS.Lookup('CL_E_DS_NBR', TempVar, 'E_D_CODE_TYPE') <> ED_MEMO_PENSION) and (CL_E_DS.Lookup('CL_E_DS_NBR', TempVar, 'E_D_CODE_TYPE') <> ED_MEMO_PENSION_SUI) then
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      end;
    end;

    CalendarDate := PR['CHECK_DATE'];

    DM_CLIENT.PR_SCHEDULED_EVENT.DataRequired('PR_NBR='+PR.FieldByName('PR_NBR').AsString);

    if DM_CLIENT.PR_SCHEDULED_EVENT.RecordCount > 0 then
    begin
      CalendarDateStr := Trim(DM_CLIENT.PR_SCHEDULED_EVENT.FILLER.AsString);
      if CalendarDateStr <> '' then
        CalendarDate := StrToDate(Copy(CalendarDateStr,1,10));
    end;

    case GetWeekNumber(CalendarDate) of
      1:
        if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_1'] = 'Y' then
        begin
          EE_SCHEDULED_E_DS.Next;
          Continue;
        end;
      2:
        if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_2'] = 'Y' then
        begin
          EE_SCHEDULED_E_DS.Next;
          Continue;
        end;
      3:
        if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_3'] = 'Y' then
        begin
          EE_SCHEDULED_E_DS.Next;
          Continue;
        end;
      4:
        if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_4'] = 'Y' then
        begin
          EE_SCHEDULED_E_DS.Next;
          Continue;
        end;
      5:
        if EE_SCHEDULED_E_DS['EXCLUDE_WEEK_5'] = 'Y' then
        begin
          EE_SCHEDULED_E_DS.Next;
          Continue;
        end;
    end;
    ScheduledEDFreq := EE_SCHEDULED_E_DS['FREQUENCY'];
    GTLTimes := GetCheckFreq(PR_BATCH['FREQUENCY']);
    case ScheduledEDFreq[1] of
      SCHED_FREQ_USER_ENTERED:
        begin
          EE_SCHEDULED_E_DS.Next;
          Continue;
        end;
      SCHED_FREQ_SCHEDULED_PAY:
        begin
          if PR['SCHEDULED'] = 'N' then
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      SCHED_FREQ_EVRY_OTHER_PAY:
        begin
          if ScheduledEDsInPriorPayroll.IndexOf(EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').AsString) <> -1 then
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
          GTLTimes := Round(GTLTimes / 2);
        end;
      SCHED_FREQ_FIRST_SCHED_PAY:
        begin
          if (PR['SCHEDULED'] = 'Y')
          {ThisIsRightPayroll(GROUP_BOX_FIRST, PR['CHECK_DATE'], ProirScheduledCheckDate, 0)} then
          begin
            DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired(
              'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' AND SCHEDULED_CHECK_DATE >= '''+DateToStr(GetBeginMonth(PR.FieldByName('CHECK_DATE').AsDateTime))+'''');
            DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := 'SCHEDULED_CHECK_DATE';

            DM_PAYROLL.PR_SCHEDULED_EVENT.First;
            if not DM_PAYROLL.PR_SCHEDULED_EVENT.Eof and
              (DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsString = PR.FieldByName('CHECK_DATE').AsString) then
            begin
              DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := '';
              GTLTimes := 12
            end
            else
            begin
              DM_PAYROLL.PR_SCHEDULED_EVENT.IndexFieldNames := '';
              EE_SCHEDULED_E_DS.Next;
              Continue;
            end;
          end
          else
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      SCHED_FREQ_LAST_SCHED_PAY:
        begin
          if (PR.FieldByName('SCHEDULED').AsString = GROUP_BOX_YES) and ThisIsRightPayroll(GROUP_BOX_LAST, PR.FieldByName('CHECK_DATE').Value, 0, NextScheduledCheckDate) then
            GTLTimes := 12
          else
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      SCHED_FREQ_MID_SCHED_PAY:
        begin
          if ThisIsRightPayroll(GROUP_BOX_CLOSEST_TO_15, PR['CHECK_DATE'], ProirScheduledCheckDate, NextScheduledCheckDate) then
            GTLTimes := 12
          else
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      SCHED_FREQ_QUARTERLY:
        begin
          if (PR['SCHEDULED'] = 'Y') and ThisIsRightPayroll(EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString, PR.FieldByName('CHECK_DATE').Value, ProirScheduledCheckDate, NextScheduledCheckDate)
          and (GetMonth(PR.FieldByName('CHECK_DATE').AsDateTime) - (GetQuarterNumber(PR.FieldByName('CHECK_DATE').AsDateTime) - 1) * 3 = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptQuarter)) then
            GTLTimes := 4
          else
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      SCHED_FREQ_SEMI_ANNUALLY:
        begin
          if (PR['SCHEDULED'] = 'Y') and ThisIsRightPayroll(EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString, PR.FieldByName('CHECK_DATE').Value, ProirScheduledCheckDate, NextScheduledCheckDate)
          and ((GetMonth(PR.FieldByName('CHECK_DATE').AsDateTime) = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptSemiAnnual))
          or (GetMonth(PR.FieldByName('CHECK_DATE').AsDateTime) - 6 = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptSemiAnnual))) then
            GTLTimes := 2
          else
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
      SCHED_FREQ_ANNUALLY:
        begin
          if (PR['SCHEDULED'] = 'Y') and ThisIsRightPayroll(EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString, PR.FieldByName('CHECK_DATE').Value, ProirScheduledCheckDate, NextScheduledCheckDate)
          and (GetMonth(PR.FieldByName('CHECK_DATE').AsDateTime) = ReturnMonthNumber(EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString, ptAnnual)) then
            GTLTimes := 1
          else
          begin
            EE_SCHEDULED_E_DS.Next;
            Continue;
          end;
        end;
    end;

    CL_E_DS.Activate;
    EDType := CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'E_D_CODE_TYPE');

    if (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_COBRA_CREDIT) and
      not TypeIsCobraCredit(EDType, VarToStr(CL_E_DS.Lookup('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], 'DESCRIPTION'))) then
    begin
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;

    // Cobra Credit scheduled E/D code should be ignored for all the check types except of Cobra Credit check
    if (PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_COBRA_CREDIT) and
      (EDType = ED_MEMO_COBRA_CREDIT) then
    begin
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;

// Do GTL calculation
    if EDType = ED_EWOD_GROUP_TERM_LIFE then
    begin
      EE.Locate('EE_NBR', EmployeeNbr, []);
      PolicyAmount := CalculateGroupTermLife(PR, 12 / GTLTimes);
      if PolicyAmount <> 0 then
      begin
        PR_CHECK_LINES.Insert;
        PR_CHECK_LINES['AMOUNT'] := PolicyAmount;
        PR_CHECK_LINES['CL_E_DS_NBR'] := EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value;
        PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_SCHEDULED;
        PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
        if EE.FieldByName('WORKATHOME').AsString = 'Y' then
          PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString := 'Y';
        DoPrePostCheck;
        PR_CHECK_LINES.Post;
      end;
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;

// SR-20.10
    PR_CHECK_LINES.Insert;
    PR_CHECK_LINES['CL_E_DS_NBR'] := EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value;
    PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_SCHEDULED;
    if EE.FieldByName('WORKATHOME').AsString = 'Y' then
      PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString := 'Y';
    if EDType = ED_DIRECT_DEPOSIT then
    begin
      PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
      DoPrePostCheck;
      PR_CHECK_LINES.Post;
      EE_SCHEDULED_E_DS.Next;
      Continue;
    end;
    if ctx_DataAccess.RecalcLineBeforePost and Assigned(SY_FED_TAX_TABLE) and Assigned(SY_STATES) and Assigned(CL_PENSION) then
    begin
      if not ScheduledEDRecalc(EDType, CheckAmount, CheckAmount, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES,
        PR_CHECK_SUI, PR_CHECK_LOCALS, CL_E_DS, CL_PENSION, CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES,
        SY_FED_TAX_TABLE, SY_STATES) then
      begin
        EE_SCHEDULED_E_DS.Next;
        Continue;
      end;
    end;
    PR_CHECK_LINES['CL_AGENCY_NBR'] := EE_SCHEDULED_E_DS.FieldByName('CL_AGENCY_NBR').Value;
    PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := EE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
    if not VarIsNull(PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR']) then
      PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_PENDING;
    if PR_CHECK['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'] = 'Y' then
    begin
      if (not PR_CHECK_LINES.FieldByName('CL_AGENCY_NBR').IsNull) and (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
        PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_IGNORE;
    end;
    DoPrePostCheck;
    PR_CHECK_LINES.Post;
    EE_SCHEDULED_E_DS.Next;
  end;
  EE_SCHEDULED_E_DS.CancelRange;
  EE_SCHEDULED_E_DS.IndexFieldNames := '';
  {EE_SCHEDULED_E_DS.Filtered := False;
  EE_SCHEDULED_E_DS.Filter := '';}
end;

procedure TevPayrollCalculation.Generic_CreateVoidPRCheckDetails(CheckToVoid: Integer; PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
  PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS: TevClientDataSet; const DontPost: Boolean = False);
var
  TempDataSet, TEMP_PR_CHECK_LINE_LOCALS: TevClientDataSet;
  I: Integer;
  S: String;
  AllOK, WasManualCheck, Was3rdPartyCheck: Boolean;
begin
  if ctx_AccountRights.Functions.GetState('FIELDS_RATE_OF_') <> stEnabled then
    raise ENoRights.CreateHelp('You have no rights to rates and wages and hence cannot void a check.', IDH_SecurityViolation);
  ctx_DataAccess.UnlockPRSimple;
  PR_CHECK.DisableControls;
  PR_CHECK.LookupsEnabled := False;
  PR_CHECK_LINES.DisableControls;
  PR_CHECK_LINES.LookupsEnabled := False;
  TempDataSet := TevClientDataSet.Create(nil);
  with ctx_DataAccess.CUSTOM_VIEW do
  try
    if Active then
      Close;
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('NbrField', 'PR_CHECK');
      SetMacro('Columns', '*');
      SetMacro('TableName', 'PR_CHECK');
      SetParam('RecordNbr', CheckToVoid);
      DataRequest(AsVariant);
    end;
    Open;
    WasManualCheck := FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL;
    Was3rdPartyCheck := FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_3RD_PARTY;
    try
      if PR_CHECK.State in [dsInsert, dsEdit] then
      begin
  // Copy Check
        TempDataSet.Data := Data;
        Close;
        for I := 0 to TempDataSet.FieldCount - 1 do
        if (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') and
        (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE') and
        (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD') and
        (TempDataSet.Fields[I].FieldName <> 'NOTES_NBR') and
        (TempDataSet.Fields[I].FieldName <> 'DATA_NBR') and
        (TempDataSet.Fields[I].FieldName <> 'CHECK_TYPE') and (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
        (TempDataSet.Fields[I].FieldName <> 'PR_BATCH_NBR') and (TempDataSet.Fields[I].FieldName <> 'FILLER') then
        begin
          if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) or (TempDataSet.Fields[I].FieldName = 'EE_OASDI_TAXABLE_TIPS')
          or (TempDataSet.Fields[I].FieldName = 'ER_OASDI_TAXABLE_TIPS') or (TempDataSet.Fields[I].FieldName = 'OR_CHECK_BACK_UP_WITHHOLDING') then
            PR_CHECK.FieldByName(TempDataSet.Fields[I].FieldName).Value := - TempDataSet.Fields[I].Value
          else
            PR_CHECK.FieldByName(TempDataSet.Fields[I].FieldName).Value := TempDataSet.Fields[I].Value;
        end;
        PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_VOID;
        PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        PR_CHECK['OR_CHECK_FEDERAL_VALUE'] := PR_CHECK.FieldByName('FEDERAL_TAX').Value;
        PR_CHECK['OR_CHECK_OASDI'] := PR_CHECK.FieldByName('EE_OASDI_TAX').Value;
        PR_CHECK['OR_CHECK_MEDICARE'] := PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value;
        PR_CHECK['OR_CHECK_EIC'] := PR_CHECK.FieldByName('EE_EIC_TAX').Value;
(*        if not TempDataSet.FieldByName('NOTES_NBR').IsNull then
          PR_CHECK.UpdateBlobData('NOTES_NBR', TempDataSet.GetBlobData('NOTES_NBR'));
        if not TempDataSet.FieldByName('DATA_NBR').IsNull then
          PR_CHECK.UpdateBlobData('DATA_NBR', TempDataSet.GetBlobData('DATA_NBR'));
*)
  // Deal with Check shortfalls
        if DM_COMPANY.CO['MAKE_UP_TAX_DEDUCT_SHORTFALLS'] = 'Y' then
        begin
          with TExecDSWrapper.Create('CheckFederalShortfalls') do
          begin
            SetParam('EENbr', PR_CHECK.FieldByName('EE_NBR').AsInteger);
            SetParam('CheckToVoid', CheckToVoid);
            DataRequest(AsVariant);
          end;
          Open;
          Last;
          PR_CHECK['FEDERAL_SHORTFALL'] := FieldByName('FEDERAL_SHORTFALL').Value -
            PR_CHECK['FEDERAL_SHORTFALL'];
          Prior;
          PR_CHECK['FEDERAL_SHORTFALL'] := FieldByName('FEDERAL_SHORTFALL').Value -
            PR_CHECK['FEDERAL_SHORTFALL'];
          Close;
        end;
      end
      else
      begin
        TEMP_PR_CHECK_LINE_LOCALS := TevClientDataSet.Create(Nil);
        try
  // Get PR_CHECK_LINE_LOCALS
          PR_CHECK_LINE_LOCALS.DataRequired('PR_CHECK_LINES_NBR=0');
          if Active then
            Close;
          with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
          begin
            SetMacro('Columns', 'T1.*');
            SetMacro('TABLE1', 'PR_CHECK_LINE_LOCALS');
            SetMacro('TABLE2', 'PR_CHECK_LINES');
            SetMacro('NBRFIELD', 'PR_CHECK');
            SetMacro('JOINFIELD', 'PR_CHECK_LINES');
            SetParam('RecordNbr', CheckToVoid);
            DataRequest(AsVariant);
          end;
          Open;
          TEMP_PR_CHECK_LINE_LOCALS.Data := Data;
          Close;

  // Copy Check States
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR_CHECK');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_CHECK_STATES');
            SetParam('RecordNbr', CheckToVoid);
            DataRequest(AsVariant);
          end;
          Open;
          TempDataSet.Data := Data;
          Close;
          TempDataSet.First;
          PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
          while not TempDataSet.EOF do
          begin
            PR_CHECK_STATES.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_STATES_NBR') then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) then
                PR_CHECK_STATES.FieldByName(TempDataSet.Fields[I].FieldName).Value := -TempDataSet.Fields[I].Value
              else
                PR_CHECK_STATES.FieldByName(TempDataSet.Fields[I].FieldName).Value := TempDataSet.Fields[I].Value;
            end;
            PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
            PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := PR_CHECK_STATES.FieldByName('STATE_TAX').Value;
            PR_CHECK_STATES['EE_SDI_OVERRIDE'] := PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value;
  // Deal with Check States shortfalls
            if DM_COMPANY.CO['MAKE_UP_TAX_DEDUCT_SHORTFALLS'] = 'Y' then
            begin
              with TExecDSWrapper.Create('CheckStatesShortfalls') do
              begin
                SetParam('EEStatesNbr', PR_CHECK_STATES.FieldByName('EE_STATES_NBR').AsInteger);
                SetParam('CheckToVoid', CheckToVoid);
                DataRequest(AsVariant);
              end;
              Open;
              Last;
              PR_CHECK_STATES['STATE_SHORTFALL'] := FieldByName('STATE_SHORTFALL').Value -
                PR_CHECK_STATES['STATE_SHORTFALL'];
              PR_CHECK_STATES['EE_SDI_SHORTFALL'] := FieldByName('EE_SDI_SHORTFALL').Value -
                PR_CHECK_STATES['EE_SDI_SHORTFALL'];
              Prior;
              PR_CHECK_STATES['STATE_SHORTFALL'] := FieldByName('STATE_SHORTFALL').Value -
                PR_CHECK_STATES['STATE_SHORTFALL'];
              PR_CHECK_STATES['EE_SDI_SHORTFALL'] := FieldByName('EE_SDI_SHORTFALL').Value -
                PR_CHECK_STATES['EE_SDI_SHORTFALL'];
              Close;
            end;
            PR_CHECK_STATES.Post;
            TempDataSet.Next;
          end;

  // Copy Check Locals
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR_CHECK');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_CHECK_LOCALS');
            SetParam('RecordNbr', CheckToVoid);
            DataRequest(AsVariant);
          end;
          Open;
          TempDataSet.Data := Data;
          Close;
          TempDataSet.First;
          PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
          while not TempDataSet.EOF do
          begin
            PR_CHECK_LOCALS.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_LOCALS_NBR') then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) then
                PR_CHECK_LOCALS.FieldByName(TempDataSet.Fields[I].FieldName).Value := -TempDataSet.Fields[I].Value
              else
                PR_CHECK_LOCALS.FieldByName(TempDataSet.Fields[I].FieldName).Value := TempDataSet.Fields[I].Value;
            end;
            PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value;
  // Deal with Check Locals shortfalls
            if DM_COMPANY.CO['MAKE_UP_TAX_DEDUCT_SHORTFALLS'] = 'Y' then
            begin
              with TExecDSWrapper.Create('CheckLocalsShortfalls') do
              begin
                SetParam('EELocalsNbr', PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger);
                SetParam('CheckToVoid', CheckToVoid);
                DataRequest(AsVariant);
              end;
              Open;
              Last;
              PR_CHECK_LOCALS['LOCAL_SHORTFALL'] := FieldByName('LOCAL_SHORTFALL').Value -
                PR_CHECK_LOCALS['LOCAL_SHORTFALL'];
              Prior;
              PR_CHECK_LOCALS['LOCAL_SHORTFALL'] := FieldByName('LOCAL_SHORTFALL').Value -
                PR_CHECK_LOCALS['LOCAL_SHORTFALL'];
              Close;
            end;
            PR_CHECK_LOCALS.Post;
            TempDataSet.Next;
          end;

  // Copy Check SUI
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR_CHECK');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_CHECK_SUI');
            SetParam('RecordNbr', CheckToVoid);
            DataRequest(AsVariant);
          end;
          Open;
          TempDataSet.Data := Data;
          Close;
          TempDataSet.First;
          if not PR_CHECK_SUI.Active then
            PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
          while not TempDataSet.EOF do
          begin
            PR_CHECK_SUI.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_SUI_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') and (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and
              (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE') and (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD') then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) then
                PR_CHECK_SUI.FieldByName(TempDataSet.Fields[I].FieldName).Value := -TempDataSet.Fields[I].Value
              else
                PR_CHECK_SUI.FieldByName(TempDataSet.Fields[I].FieldName).Value := TempDataSet.Fields[I].Value;
            end;
            PR_CHECK_SUI['OVERRIDE_AMOUNT'] := PR_CHECK_SUI.FieldByName('SUI_TAX').Value;
            PR_CHECK_SUI.Post;
            TempDataSet.Next;
          end;

  // Copy Check Lines
          DM_CLIENT.CL_E_DS.DataRequired('ALL');
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR_CHECK');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_CHECK_LINES');
            SetParam('RecordNbr', CheckToVoid);
            DataRequest(AsVariant);
          end;
          Open;
          TempDataSet.Data := Data;
          Close;
          TempDataSet.First;
          if not PR_CHECK_LINES.Active then
            PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
          while not TempDataSet.EOF do
          begin
            PR_CHECK_LINES.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_LINES_NBR') and (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') and
              (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE') and
              (TempDataSet.Fields[I].FieldName <> 'CO_DIVISION_NBR') and (TempDataSet.Fields[I].FieldName <> 'CO_BRANCH_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'CO_DEPARTMENT_NBR') and (TempDataSet.Fields[I].FieldName <> 'CO_TEAM_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD')  then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) or (TempDataSet.Fields[I].FieldName = 'HOURS_OR_PIECES')
                or (TempDataSet.Fields[I].FieldName = 'AMOUNT') or (TempDataSet.Fields[I].FieldName = 'E_D_DIFFERENTIAL_AMOUNT') then
                PR_CHECK_LINES.FieldByName(TempDataSet.Fields[I].FieldName).Value := -TempDataSet.Fields[I].Value
              else
                PR_CHECK_LINES.FieldByName(TempDataSet.Fields[I].FieldName).Value := TempDataSet.Fields[I].Value;
            end;
            PR_CHECK_LINES['CO_DIVISION_NBR'] := TempDataSet.FieldByName('CO_DIVISION_NBR').Value;
            PR_CHECK_LINES['CO_BRANCH_NBR'] := TempDataSet.FieldByName('CO_BRANCH_NBR').Value;
            PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := TempDataSet.FieldByName('CO_DEPARTMENT_NBR').Value;
            PR_CHECK_LINES['CO_TEAM_NBR'] := TempDataSet.FieldByName('CO_TEAM_NBR').Value;
            PR_CHECK_LINES['WORK_ADDRESS_OVR'] := TempDataSet.FieldByName('WORK_ADDRESS_OVR').Value;

            if PR_CHECK_LINES['AGENCY_STATUS'] = CHECK_LINE_AGENCY_STATUS_SENT then
            begin
              PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_PENDING;
              PR_CHECK_LINES['PR_MISCELLANEOUS_CHECKS_NBR'] := Null;
            end;
  // Deal with Check Lines Deduction shortfalls
            if DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'MAKE_UP_DEDUCTION_SHORTFALL') = 'Y' then
            begin
              with TExecDSWrapper.Create('CheckDeductionShortfalls') do
              begin
                SetParam('EENbr', PR_CHECK.FieldByName('EE_NBR').AsInteger);
                SetParam('EDNbr', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger);
                SetParam('CheckToVoid', CheckToVoid);
                DataRequest(AsVariant);
              end;
              Open;
              Last;
              PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value
                - PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'];

              Prior;
              PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value -
                PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'];
              Close;
            end;
            PR_CHECK_LINES.Post;
  // Copy Check Line Locals
            TEMP_PR_CHECK_LINE_LOCALS.Filter := 'PR_CHECK_LINES_NBR=' + TempDataSet.FieldByName('PR_CHECK_LINES_NBR').AsString;
            TEMP_PR_CHECK_LINE_LOCALS.Filtered := True;
            TEMP_PR_CHECK_LINE_LOCALS.First;
            while not TEMP_PR_CHECK_LINE_LOCALS.EOF do
            begin
              PR_CHECK_LINE_LOCALS.Insert;
              for I := 0 to TEMP_PR_CHECK_LINE_LOCALS.FieldCount - 1 do
              begin
                if (TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName <> 'PR_CHECK_LINES_NBR') and
                   (TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName <> 'PR_CHECK_LINE_LOCALS_NBR') and
                   Assigned(PR_CHECK_LINE_LOCALS.FindField(TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName)) then
                  PR_CHECK_LINE_LOCALS.FieldByName(TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName).Value := TEMP_PR_CHECK_LINE_LOCALS.Fields[I].Value;
              end;
              PR_CHECK_LINE_LOCALS.Post;
              TEMP_PR_CHECK_LINE_LOCALS.Next;
            end;
            TEMP_PR_CHECK_LINE_LOCALS.Filtered := False;
  // End Copy Check Line Locals
            TempDataSet.Next;
          end;
          if WasManualCheck or Was3rdPartyCheck then
          begin
            PR_CHECK.Edit;
            if WasManualCheck then
              PR_CHECK.FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_MANUAL;
            if Was3rdPartyCheck then
              PR_CHECK.FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_3RD_PARTY;
            ctx_DataAccess.NewCheckInserted := True;
            try
              PR_CHECK.Post;
            finally
              ctx_DataAccess.NewCheckInserted := False;
            end;
          end
          else if not DontPost then
          begin
  // Void prior check
            if Active then
              Close;
            with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
            begin
              SetMacro('NbrField', 'pr_check');
              SetMacro('Columns', '*');
              SetMacro('TableName', 'pr_check');
              SetParam('RecordNbr', CheckToVoid);
              DataRequest(AsVariant);
            end;
            Open;
            First;
            I := PR_CHECK.FieldByName('PR_NBR').AsInteger;
            S := DM_PAYROLL.PR.RetrieveCondition;
            DM_PAYROLL.PR.RetrieveCondition := '';
            DM_PAYROLL.PR.Locate('PR_NBR', FieldByName('PR_NBR').Value, []);
            Close;
            ctx_DataAccess.UnlockPR;
            AllOK := False;
            with TExecDSWrapper.Create('pr_check;GenericSelectCurrentNBR') do
            begin
              SetMacro('NbrField', 'pr_check');
              SetMacro('Columns', '*');
              SetMacro('TableName', 'pr_check');
              SetParam('RecordNbr', CheckToVoid);
              DataRequest(AsVariant);
            end;
            Open;
            First;
            Edit;
            FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_VOID;
            Post;
            try
              ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
              AllOK := True;
            finally
              ctx_DataAccess.LockPR(AllOK);
              DM_PAYROLL.PR.RetrieveCondition := S;
              DM_PAYROLL.PR.Locate('PR_NBR', I, []);
            end;
          end;
        finally
          TEMP_PR_CHECK_LINE_LOCALS.Free;
        end;
  // Clear Bank Account Register
        {DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckToVoid));
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
        while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
        begin
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Cleared;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['CLEARED_DATE'] := Now;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
        end;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);}
      end;
    except
      ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
      raise;
    end;
  finally
    TempDataSet.Free;
    PR_CHECK_LINES.EnableControls;
    PR_CHECK_LINES.LookupsEnabled := True;
    PR_CHECK.EnableControls;
    PR_CHECK.LookupsEnabled := True;
    ctx_DataAccess.LockPRSimple;
  end;
end;

procedure TevPayrollCalculation.BlockEverythingOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet; WithTransaction: Boolean);
var
  LocalTransactionManager: TTransactionManager;
  b: Boolean;
  IndName, IndFields: string;
begin
  if ctx_DataAccess.CheckCalcInProgress or ctx_DataAccess.NewBatchInserted or ctx_DataAccess.NewCheckInserted or ctx_DataAccess.PayrollIsProcessing then
    Exit;
  LocalTransactionManager := nil;
  if (not PR_CHECK_LINES.Active)
  or ((PR_CHECK_LINES.OpenCondition <> '')
  and (UpperCase(StringReplace(PR_CHECK_LINES.OpenCondition, ' ', '', [rfReplaceAll])) <> 'PR_NBR=' + PR_CHECK.FieldByName('PR_NBR').AsString)
  and (UpperCase(StringReplace(PR_CHECK_LINES.OpenCondition, ' ', '', [rfReplaceAll])) <> 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString))
  or ((PR_CHECK_LINES.RetrieveCondition <> '')
  and (UpperCase(StringReplace(PR_CHECK_LINES.RetrieveCondition, ' ', '', [rfReplaceAll])) <> 'PR_NBR=' + PR_CHECK.FieldByName('PR_NBR').AsString)
  and (UpperCase(StringReplace(PR_CHECK_LINES.RetrieveCondition, ' ', '', [rfReplaceAll])) <> 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString)) then
    PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
  if (not EE_SCHEDULED_E_DS.Active) or ((EE_SCHEDULED_E_DS.OpenCondition <> '') and (EE_SCHEDULED_E_DS.OpenCondition <> 'EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString))
  or ((EE_SCHEDULED_E_DS.RetrieveCondition <> '') and (EE_SCHEDULED_E_DS.RetrieveCondition <> 'EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString)) then
    EE_SCHEDULED_E_DS.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
  if WithTransaction then
    LocalTransactionManager := TTransactionManager.Create(nil);
  try
    if WithTransaction then
      LocalTransactionManager.Initialize([PR_CHECK_LINES]);
    b := PR_CHECK_LINES.LookupsEnabled;
    IndName := PR_CHECK_LINES.IndexName;
    IndFields := PR_CHECK_LINES.IndexFieldNames;
    PR_CHECK_LINES.LookupsEnabled := False;
    PR_CHECK_LINES.IndexFieldNames := 'PR_CHECK_NBR';
    try
      PR_CHECK_LINES.FindKey([PR_CHECK['PR_CHECK_NBR']]);
      with PR_CHECK_LINES do
        while (FieldByName('PR_CHECK_NBR').Value = PR_CHECK['PR_CHECK_NBR']) and not EOF do
        begin
          if (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) or (FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_SCHEDULED) then
          begin
            if CheckIfBlockCheckLineOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS) then
            begin
              Delete;
              Continue;
            end;
            if PR_CHECK['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'] = 'Y' then
            begin
              if (not FieldByName('CL_AGENCY_NBR').IsNull) and (not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
              begin
                Edit;
                FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_IGNORE;
                Post;
              end;
            end;
          end;
          Next;
        end;
    finally
      PR_CHECK_LINES.LookupsEnabled := b;
      if IndName <> '' then
        PR_CHECK_LINES.IndexName := IndName
      else
        PR_CHECK_LINES.IndexFieldNames := IndFields;
    end;
    if WithTransaction then
      LocalTransactionManager.ApplyUpdates;
  finally
    if WithTransaction then
      LocalTransactionManager.Free;
  end;
end;

procedure TevPayrollCalculation.CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, CL_E_D_GROUP_CODES, CL_PERSON, CL_PIECES, CO_SHIFTS,
  CO_STATES, EE, EE_SCHEDULED_E_DS, EE_RATES, EE_STATES, EE_WORK_SHIFTS, SY_FED_TAX_TABLE, SY_STATES: TevClientDataSet;
  LogEvent :TOnPrCheckLinesLogMessage = nil);
var
  EDType: string;
  AnnualAmount, YTD, YTDHours, MinAmount, MaxAmount: Real;
  lHours, lDollars: Currency;
  EarnCodeGroupNbr: Integer;
  employeeNumber :string;
  employeeName :string;
  MaxExceeds:integer;
  procedure CalculatePiecework;
  begin
    CL_PIECES.Activate;
    DM_EMPLOYEE.EE_PIECE_WORK.Activate;
    if DM_EMPLOYEE.EE_PIECE_WORK.Locate('EE_NBR;CL_PIECES_NBR', VarArrayOf([PR_CHECK['EE_NBR'], PR_CHECK_LINES.FieldByName('CL_PIECES_NBR').Value]), []) then
    begin
      if ConvertNull(PR_CHECK_LINES['RATE_OF_PAY'], 0) = 0 then
        PR_CHECK_LINES['RATE_OF_PAY'] := RoundAll(ConvertNull(DM_EMPLOYEE.EE_PIECE_WORK.FieldByName('RATE_AMOUNT').Value, 0) / ConvertNull(DM_EMPLOYEE.EE_PIECE_WORK.FieldByName('RATE_QUANTITY').Value, 1), 6);
      if ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) = 0 then
        PR_CHECK_LINES['HOURS_OR_PIECES'] := DM_EMPLOYEE.EE_PIECE_WORK.FieldByName('RATE_QUANTITY').Value;
    end
    else
    begin
      if ConvertNull(PR_CHECK_LINES['RATE_OF_PAY'], 0) = 0 then
        PR_CHECK_LINES['RATE_OF_PAY'] := RoundAll(ConvertNull(CL_PIECES.NewLookup('CL_PIECES_NBR', PR_CHECK_LINES.FieldByName('CL_PIECES_NBR').Value, 'DEFAULT_RATE'), 0) / ConvertNull(CL_PIECES.NewLookup('CL_PIECES_NBR', PR_CHECK_LINES.FieldByName('CL_PIECES_NBR').Value, 'DEFAULT_QUANTITY'), 1), 6);
      if ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) = 0 then
        PR_CHECK_LINES['HOURS_OR_PIECES'] := CL_PIECES.NewLookup('CL_PIECES_NBR', PR_CHECK_LINES.FieldByName('CL_PIECES_NBR').Value, 'DEFAULT_QUANTITY');
    end;
    PR_CHECK_LINES['AMOUNT'] := PR_CHECK_LINES.FieldByName('RATE_OF_PAY').Value * PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value;
  end;

  procedure CreateOffsets;
  var
    TempType: string;
    EDGroupNbr, COStateNbr, SYStateNbr, ShiftNbr, EEShiftNbr, EDNbr, JobNbr, I: Integer;
    ShiftDiffAmt, GuaranteedPay, RealPay, MinimumWage, AmtToMakeup: Real;
    MinWageLines, JobNumbers: TStringList;
    DataSetToCopy: TevClientDataSet;
    JobMakeup: Boolean;

    function GetCoStateNBR(EEStateNbr : integer) : integer;
    var qry : IevQuery;
    begin
      qry := TevQuery.Create('SELECT SUI_APPLY_CO_STATES_NBR FROM EE_STATES a WHERE {AsOfNow<a>} AND a.EE_STATES_NBR=:EE_STATES_NBR');
      qry.Params.AddValue('EE_STATES_NBR',EEStateNbr);
      qry.Execute;
      Result := ConvertNull(qry.Result['SUI_APPLY_CO_STATES_NBR'],0);
    end;

  begin
    CO_SHIFTS.DataRequired('CO_NBR=' + EE.FieldByName('CO_NBR').AsString);
    EE_WORK_SHIFTS.Activate;
    CL_E_D_GROUP_CODES.Activate;
    ShiftNbr := ConvertNull(PR_CHECK_LINES['CO_SHIFTS_NBR'], 0);
    if ShiftNbr = 0 then
    begin
      ShiftNbr := ConvertNull(EE['AUTOPAY_CO_SHIFTS_NBR'], 0);
      if ShiftNbr <> 0 then
      begin
        if VarIsNull(CL_E_D_GROUP_CODES.NewLookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR',
          VarArrayOf([CO_SHIFTS.NewLookup('CO_SHIFTS_NBR', ShiftNbr, 'CL_E_D_GROUPS_NBR'),
          PR_CHECK_LINES['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
          ShiftNbr := 0;
      end;
    end;
    if ShiftNbr <> 0 then
    begin
      try
        EEShiftNbr := EE_WORK_SHIFTS.NewLookup('EE_NBR;CO_SHIFTS_NBR', VarArrayOf([PR_CHECK['EE_NBR'], ShiftNbr]), 'EE_WORK_SHIFTS_NBR');
      except
        raise EInconsistentData.CreateHelp('Shift is not setup for EE #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
      end;
      if ConvertNull(EE_WORK_SHIFTS.NewLookup('EE_WORK_SHIFTS_NBR', EEShiftNbr, 'SHIFT_PERCENTAGE'), 0) <> 0 then
        ShiftDiffAmt := ConvertNull(PR_CHECK_LINES['AMOUNT'], 0) * EE_WORK_SHIFTS.NewLookup('EE_WORK_SHIFTS_NBR', EEShiftNbr, 'SHIFT_PERCENTAGE') * 0.01
      else
      begin
        if VarIsNull(EE_WORK_SHIFTS.NewLookup('EE_WORK_SHIFTS_NBR', EEShiftNbr, 'SHIFT_RATE')) then
          raise EInconsistentData.CreateHelp('Shift rate or % is not setup for EE #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);

        if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_OVERTIME then
        begin
          if VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'REGULAR_OT_RATE')) then
            raise EInconsistentData.CreateHelp('Overtime rate multiplier is missing for ' + CL_E_DS.NewLookup('CL_E_DS_NBR',
              PR_CHECK_LINES['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
          ShiftDiffAmt := ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) * EE_WORK_SHIFTS.NewLookup('EE_WORK_SHIFTS_NBR', EEShiftNbr, 'SHIFT_RATE')
            * CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'REGULAR_OT_RATE');
        end
        else
          ShiftDiffAmt := ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) * EE_WORK_SHIFTS.NewLookup('EE_WORK_SHIFTS_NBR', EEShiftNbr, 'SHIFT_RATE');
      end;
      PR_CHECK_LINES['AMOUNT'] := ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0) + ShiftDiffAmt;
      PR_CHECK_LINES['E_D_DIFFERENTIAL_AMOUNT'] := RoundTwo(ShiftDiffAmt);
    end;

    if ((EDType = ED_DED_REIMBURSEMENT) or (EDType = ED_MEMO_COBRA_CREDIT)) and (PR_CHECK_LINES.State = dsInsert) then
      PR_CHECK_LINES['AMOUNT'] := ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0) * (-1);

    if (EDType = ED_MU_TIPPED_CREDIT_MAKEUP) or (EDType = ED_MU_MIN_WAGE_MAKEUP) then
      Exit;

    MinWageLines := TStringList.Create;
    JobNumbers := TStringList.Create;
    try
// Min wage makeup calculation
      ApplyTempCheckLinesCheckRange(PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').AsInteger);
      cdstTempCheckLines.First;
      while not cdstTempCheckLines.EOF do
      begin
        if ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesFieldValue('CL_E_DS_NBR'), 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE'), TIP_MINWAGE_NONE) <> TIP_MINWAGE_NONE then
        if CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesFieldValue('CL_E_DS_NBR'), 'E_D_CODE_TYPE') <> ED_OEARN_BANQUET_TIPS then
        if not VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesFieldValue('CL_E_DS_NBR'), 'CL_E_D_GROUPS_NBR')) then
          MinWageLines.Add(VarToStr(GetTempCheckLinesFieldValue('PR_CHECK_LINES_NBR')) + '=' +  VarToStr(GetTempCheckLinesFieldValue('CL_E_DS_NBR')));
        cdstTempCheckLines.Next;
      end;

      if MinWageLines.Count <> 0 then
      begin
        SY_FED_TAX_TABLE.Activate;
        SY_STATES.Activate;
        EE_STATES.Activate;
        CO_STATES.DataRequired('CO_NBR=' + EE.FieldByName('CO_NBR').AsString);

        for I := 0 to MinWageLines.Count - 1 do
        begin
          GuaranteedPay := 0;
          RealPay := 0;
          cdstTempCheckLines.Locate('PR_CHECK_LINES_NBR', MinWageLines.Names[I], []);

          if (EE_STATES.RetrieveCondition <> '')
          and (EE_STATES.OpenCondition <> '')
          and (EE_STATES.RetrieveCondition <> EE_STATES.OpenCondition) then
          begin
            DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR='+EE.FieldByName('EE_NBR').AsString);
            EE_STATES.CancelRange;
          end;

          if not VarIsNull(GetTempCheckLinesFieldValue('EE_SUI_STATES_NBR')) then
            COStateNbr := GetCoStateNBR(GetTempCheckLinesFieldValue('EE_SUI_STATES_NBR'))
          else
          if not VarIsNull(GetTempCheckLinesFieldValue('EE_STATES_NBR')) then
            COStateNbr := GetCoStateNBR(GetTempCheckLinesFieldValue('EE_STATES_NBR'))
          else
          try
            COStateNbr := GetCoStateNBR(EE['HOME_TAX_EE_STATES_NBR']);
          except
            raise EInconsistentData.CreateHelp('There is no home state SUI set up for employee #' +
              EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString + '. Home state #' + EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsString, IDH_InconsistentData);
          end;
          SYStateNbr := SY_STATES.NewLookup('STATE', CO_STATES.NewLookup('CO_STATES_NBR', COStateNbr, 'STATE'), 'SY_STATES_NBR');
          if not VarIsNull(EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EE['EE_NBR'], COStateNbr]), 'OVERRIDE_STATE_MINIMUM_WAGE')) then
            MinimumWage := EE_STATES.NewLookup('EE_NBR;CO_STATES_NBR', VarArrayOf([EE['EE_NBR'], COStateNbr]), 'OVERRIDE_STATE_MINIMUM_WAGE')
          else
          if not VarIsNull(EE['OVERRIDE_FEDERAL_MINIMUM_WAGE']) then
            MinimumWage := EE['OVERRIDE_FEDERAL_MINIMUM_WAGE']
          else
          MinimumWage := SY_STATES.NewLookup('SY_STATES_NBR', SYStateNbr, 'STATE_MINIMUM_WAGE');
          if ConvertNull(EE['OVERRIDE_FEDERAL_MINIMUM_WAGE'], 0) > MinimumWage then
            MinimumWage := EE['OVERRIDE_FEDERAL_MINIMUM_WAGE']
          else
          if SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'] > MinimumWage then
            MinimumWage := SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'];

          cdstTempCheckLines.First;
          while not cdstTempCheckLines.EOF do
          begin
            if not VarIsNull(CL_E_D_GROUP_CODES.NewLookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR',
              VarArrayOf([CL_E_DS.NewLookup('CL_E_DS_NBR', MinWageLines.Values[MinWageLines.Names[I]], 'CL_E_D_GROUPS_NBR'),
              cdstTempCheckLines['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
            begin
              TempType := CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
              if ConvertNull(GetTempCheckLinesFieldValue('RATE_OF_PAY'), 0) < MinimumWage then
              begin
                if (TempType = ED_OEARN_WEIGHTED_HALF_TIME_OT) or (((TempType = ED_OEARN_AVG_OVERTIME) or(TempType = ED_OEARN_AVG_MULT_OVERTIME))
                and (CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'OVERSTATE_HOURS_FOR_OVERTIME') = 'Y')) then
                else
                begin
                  if (ConvertNull(GetTempCheckLinesFieldValue('RATE_OF_PAY'), 0) <> 0)
                  or (ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0) <> 0) then
                    GuaranteedPay := GuaranteedPay + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * MinimumWage;
                end;

                if (TempType = ED_OEARN_OVERTIME) or (TempType = ED_OEARN_WAITSTAFF_OVERTIME) or (TempType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME)
                or (TempType = ED_OEARN_WEIGHTED_HALF_TIME_OT) or (TempType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (TempType = ED_OEARN_AVG_OVERTIME)
                or (TempType = ED_OEARN_AVG_MULT_OVERTIME) then
                begin
                  if (TempType = ED_OEARN_WEIGHTED_HALF_TIME_OT) or (((TempType = ED_OEARN_AVG_OVERTIME) or(TempType = ED_OEARN_AVG_MULT_OVERTIME))
                  and (CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'OVERSTATE_HOURS_FOR_OVERTIME') = 'Y')) then
                  else
                    RealPay := RealPay + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * ConvertNull(GetTempCheckLinesFieldValue('RATE_OF_PAY'), 0);
                end
                else
                if (TempType <> ED_OEARN_BANQUET_TIPS) then
                  RealPay := RealPay + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
              end;
            end
            else
              if CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_EWOD_MEAL_CREDIT then
                RealPay := RealPay + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
            cdstTempCheckLines.Next;
          end;

          RealPay := RoundTwo(RealPay);
          if GuaranteedPay > RealPay then
            AmtToMakeup := GuaranteedPay - RealPay
          else
            AmtToMakeup := 0;

          if MinWageLines.Names[I] = PR_CHECK_LINES['PR_CHECK_LINES_NBR'] then
          begin
            DataSetToCopy := PR_CHECK_LINES;
          end
          else
          begin
            cdstTempCheckLines.Locate('PR_CHECK_LINES_NBR', MinWageLines.Names[I], []);
            DataSetToCopy := cdstTempCheckLines;
          end;

          if CL_E_DS.NewLookup('CL_E_DS_NBR', MinWageLines.Values[MinWageLines.Names[I]], 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE') = TIP_MINWAGE_PAY_FORCE_TIP_MAKEUP then
          begin
            if not CL_E_DS.NewLocate('E_D_CODE_TYPE;CL_E_D_GROUPS_NBR', VarArrayOf([ED_MU_TIPPED_CREDIT_MAKEUP, CL_E_DS.NewLookup('CL_E_DS_NBR', MinWageLines.Values[MinWageLines.Names[I]], 'CL_E_D_GROUPS_NBR')]), []) then
              raise EInconsistentData.CreateHelp('Tipped Credit Makeup E/D code is missing. Please, set it up.', IDH_InconsistentData);
          end
          else
          begin
            if not CL_E_DS.NewLocate('E_D_CODE_TYPE;CL_E_D_GROUPS_NBR', VarArrayOf([ED_MU_MIN_WAGE_MAKEUP, CL_E_DS.NewLookup('CL_E_DS_NBR', MinWageLines.Values[MinWageLines.Names[I]], 'CL_E_D_GROUPS_NBR')]), []) then
              raise EInconsistentData.CreateHelp('Minimum Wage Makeup E/D code is missing. Please, set it up.', IDH_InconsistentData);
          end;
          LinesToAdd.Add(CL_E_DS['CL_E_DS_NBR'], AmtToMakeup, DataSetToCopy);

        end;
      end;

// Piecework makeup calculation
      MinWageLines.Clear;
      cdstTempCheckLines.First;
      while not cdstTempCheckLines.EOF do
      begin
        if ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesFieldValue('CL_E_DS_NBR'), 'PW_MIN_WAGE_MAKE_UP_METHOD'), PW_MAKEUP_METHOD_NONE) <> PW_MAKEUP_METHOD_NONE then
        if CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesFieldValue('CL_E_DS_NBR'), 'E_D_CODE_TYPE') = ED_OEARN_PIECEWORK then
        if not VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesFieldValue('CL_E_DS_NBR'), 'PIECE_CL_E_D_GROUPS_NBR')) then
        if JobNumbers.IndexOf(VarToStr(GetTempCheckLinesFieldValue('CO_JOBS_NBR'))) = -1 then
        begin
          MinWageLines.Add(VarToStr(GetTempCheckLinesFieldValue('PR_CHECK_LINES_NBR')) + '=' + VarToStr(GetTempCheckLinesFieldValue('CL_E_DS_NBR')));
          if not VarIsNull(GetTempCheckLinesFieldValue('CO_JOBS_NBR')) then
            JobNumbers.Add(VarToStr(GetTempCheckLinesFieldValue('CO_JOBS_NBR')));
        end;
        cdstTempCheckLines.Next;
      end;

      if PR_CHECK_LINES.State = dsInsert then
      begin
        if ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'PW_MIN_WAGE_MAKE_UP_METHOD'), PW_MAKEUP_METHOD_NONE) <> PW_MAKEUP_METHOD_NONE then
        if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_PIECEWORK then
        if not VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'PIECE_CL_E_D_GROUPS_NBR')) then
        if JobNumbers.IndexOf(PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').AsString) = -1 then
        begin
          MinWageLines.Add(PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsString + '=' + PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsString);
          if not PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull then
            JobNumbers.Add(PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').AsString);
        end;
      end;

      if MinWageLines.Count <> 0 then
      begin
        AmtToMakeup := 0;
        CL_E_D_GROUP_CODES.Activate;
        for I := 0 to MinWageLines.Count - 1 do
        begin
          GuaranteedPay := 0;
          RealPay := 0;
          MinimumWage := 0;
          JobMakeup := False;
          JobNbr := 0;
          if cdstTempCheckLines.Locate('PR_CHECK_LINES_NBR', MinWageLines.Names[I], []) then
            EDNbr := GetTempCheckLinesFieldValue('CL_E_DS_NBR')
          else
            EDNbr := PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger;

          if CL_E_DS.NewLookup('CL_E_DS_NBR', EDNbr, 'PW_MIN_WAGE_MAKE_UP_METHOD') = PW_MAKEUP_METHOD_EE_FIRST_RATE then
          begin
            EE_RATES.Activate;
            MinimumWage := EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([PR_CHECK['EE_NBR'], 1]), 'RATE_AMOUNT');
          end
          else
          if CL_E_DS.NewLookup('CL_E_DS_NBR', EDNbr, 'PW_MIN_WAGE_MAKE_UP_METHOD') = PW_MAKEUP_METHOD_PIECEWORK then
            MinimumWage := CL_E_DS.NewLookup('CL_E_DS_NBR', EDNbr, 'PIECEWORK_MINIMUM_WAGE')
          else
          if CL_E_DS.NewLookup('CL_E_DS_NBR', EDNbr, 'PW_MIN_WAGE_MAKE_UP_METHOD') = PW_MAKEUP_METHOD_JOB_RATE then
          begin
            JobMakeup := True;
            DM_COMPANY.CO_JOBS.Activate;
            if cdstTempCheckLines.Locate('PR_CHECK_LINES_NBR', MinWageLines.Names[I], []) then
            begin
              MinimumWage := ConvertNull(DM_COMPANY.CO_JOBS.NewLookup('CO_JOBS_NBR', GetTempCheckLinesFieldValue('CO_JOBS_NBR'), 'RATE_PER_HOUR'), 0);
              JobNbr := GetTempCheckLinesFieldValue('CO_JOBS_NBR');
            end
            else
            begin
              MinimumWage := ConvertNull(DM_COMPANY.CO_JOBS.NewLookup('CO_JOBS_NBR', PR_CHECK_LINES['CO_JOBS_NBR'], 'RATE_PER_HOUR'), 0);
              JobNbr := PR_CHECK_LINES['CO_JOBS_NBR'];
            end;
          end;
          EDGroupNbr := CL_E_DS.NewLookup('CL_E_DS_NBR', EDNbr, 'PIECE_CL_E_D_GROUPS_NBR');

          cdstTempCheckLines.First;
          while not cdstTempCheckLines.EOF do
          begin
            if not VarIsNull(CL_E_D_GROUP_CODES.NewLookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([EDGroupNbr,
            cdstTempCheckLines['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
            begin
              if CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_OVERTIME then
              begin
                if VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'REGULAR_OT_RATE')) then
                  raise EInconsistentData.CreateHelp('Overtime rate multiplier is missing for ' + CL_E_DS.NewLookup('CL_E_DS_NBR',
                  cdstTempCheckLines['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
                GuaranteedPay := GuaranteedPay + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * MinimumWage
                  * CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'REGULAR_OT_RATE');
              end
              else
              if (CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') <> ED_OEARN_PIECEWORK)
              and (not JobMakeup or (JobMakeup and (GetTempCheckLinesFieldValue('CO_JOBS_NBR') = JobNbr))) then
                GuaranteedPay := GuaranteedPay + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * MinimumWage;

              if not JobMakeup or (JobMakeup and (GetTempCheckLinesFieldValue('CO_JOBS_NBR') = JobNbr)) then
                RealPay := RealPay + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
            end;
            cdstTempCheckLines.Next;
          end;

          if PR_CHECK_LINES.State = dsInsert then
          begin
            if not VarIsNull(CL_E_D_GROUP_CODES.NewLookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([EDGroupNbr,
            PR_CHECK_LINES['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
            begin
              if CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_OVERTIME then
              begin
                if VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'REGULAR_OT_RATE')) then
                  raise EInconsistentData.CreateHelp('Overtime rate multiplier is missing for ' + CL_E_DS.NewLookup('CL_E_DS_NBR',
                  PR_CHECK_LINES['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
                GuaranteedPay := GuaranteedPay + ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) * MinimumWage
                  * CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'REGULAR_OT_RATE');
              end
              else
              if (CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') <> ED_OEARN_PIECEWORK)
              and (not JobMakeup or (JobMakeup and (PR_CHECK_LINES['CO_JOBS_NBR'] = JobNbr))) then
                GuaranteedPay := GuaranteedPay + ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) * MinimumWage;

              if not JobMakeup or (JobMakeup and (PR_CHECK_LINES['CO_JOBS_NBR'] = JobNbr)) then
                RealPay := RealPay + ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
            end;
          end;

          if GuaranteedPay > RealPay then
          begin
            if JobMakeup then
              AmtToMakeup := AmtToMakeup + GuaranteedPay - RealPay
            else
              AmtToMakeup := GuaranteedPay - RealPay;
          end
          else
          begin
            if not JobMakeup then
              AmtToMakeup := 0
            else
            if AmtToMakeup > 0 then
            begin
              AmtToMakeup := AmtToMakeup - (RealPay - GuaranteedPay);
              if AmtToMakeup < 0 then
                AmtToMakeup := 0;
            end;
          end;

          if MinWageLines.Names[I] = PR_CHECK_LINES['PR_CHECK_LINES_NBR'] then
          begin
            DataSetToCopy := PR_CHECK_LINES;
          end
          else
          begin
            cdstTempCheckLines.Locate('PR_CHECK_LINES_NBR', MinWageLines.Names[I], []);
            DataSetToCopy := cdstTempCheckLines;
          end;

          if not CL_E_DS.NewLocate('E_D_CODE_TYPE', ED_MU_MIN_WAGE_MAKEUP, []) then
            raise EInconsistentData.CreateHelp('Minimum Wage Makeup E/D code is missing. Please, set it up.', IDH_InconsistentData);
          LinesToAdd.Add(CL_E_DS.NewLookup('E_D_CODE_TYPE', ED_MU_MIN_WAGE_MAKEUP, 'CL_E_DS_NBR'), AmtToMakeup, DataSetToCopy);
        end;
      end;
    finally
      MinWageLines.Free;
      JobNumbers.Free;
    end;
  end;

  procedure CheckAnnualMax;
  begin
    if ConvertNull(PR_CHECK_LINES['AMOUNT'], 0) = 0 then
      Exit;
    EE_SCHEDULED_E_DS.Activate;
    if VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'ANNUAL_MAXIMUM_AMOUNT')) then
      AnnualAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'ANNUAL_MAXIMUM'), 0)
    else
      AnnualAmount := ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'ANNUAL_MAXIMUM_AMOUNT'), 0);
    AnnualAmount := GetAnnualMax(AnnualAmount, EDType, PR_CHECK_LINES['CL_E_DS_NBR']);
    if (TypeIsPension(EDType) or (EDType = ED_ST_DEPENDENT_CARE)
    or (EDType = ED_ST_HSA_SINGLE) or (EDType = ED_ST_HSA_FAMILY)
    or (EDType = ED_EWOD_TAXABLE_ER_HSA_SINGLE) or (EDType = ED_EWOD_TAXABLE_ER_HSA_FAMILY)
    or (EDType = ED_MEMO_ER_HSA_SINGLE) or (EDType = ED_MEMO_ER_HSA_FAMILY))
    and (AnnualAmount = 0) then
    begin
      SY_FED_TAX_TABLE.Activate;
      if (EDType = ED_ST_401K) or (EDType = ED_ST_ROTH_401K) then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_401K_LIMIT'), 0);
      if (EDType = ED_ST_403B) or (EDType = ED_ST_ROTH_403B) then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_403B_LIMIT'), 0);
      if EDType = ED_ST_457 then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_457_LIMIT'), 0);
      if EDType = ED_ST_501C then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_501C_LIMIT'), 0);
      if EDType = ED_ST_SIMPLE then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SIMPLE_LIMIT'), 0);
      if EDType = ED_ST_SEP then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SEP_LIMIT'), 0);
      if (EDType = ED_ST_CATCH_UP) or (EDType = ED_ST_401K_CATCH_UP) or (EDType = ED_ST_403B_CATCH_UP)
      or (EDType = ED_ST_457_CATCH_UP) or (EDType = ED_ST_501C_CATCH_UP) or (EDType = ED_ST_ROTH_CATCH_UP)
      or (EDType = ED_ST_SIMPLE_CATCH_UP) then
      begin
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_CATCH_UP_LIMIT'), 0);
        if EDType = ED_ST_SIMPLE_CATCH_UP then
          if (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2014, 12, 31)) = GreaterThanValue) then
             AnnualAmount := 3000
          else
             AnnualAmount := 2500; // This is new for 2009. Used to be
          // AnnualAmount := AnnualAmount * 0.5;
      end;
      if EDType = ED_ST_DEPENDENT_CARE then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_DEPENDENT_CARE_LIMIT'), 0);
      if (EDType = ED_ST_HSA_SINGLE) or (EDType = ED_EWOD_TAXABLE_ER_HSA_SINGLE) or (EDType = ED_MEMO_ER_HSA_SINGLE) then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_HSA_SINGLE_LIMIT'), 0);
      if (EDType = ED_ST_HSA_FAMILY) or (EDType = ED_EWOD_TAXABLE_ER_HSA_FAMILY) or (EDType = ED_MEMO_ER_HSA_FAMILY) then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_HSA_FAMILY_LIMIT'), 0);
      if EDType = ED_ST_HSA_CATCH_UP then
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_HSA_CATCH_UP_LIMIT'), 0);
      if EDType = ED_ST_ROTH_IRA then
      begin
        AnnualAmount := ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'ROTH_IRA_LIMIT'), 0);
        DM_CLIENT.CL_PERSON.Activate;
        if VarIsNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.CL_PERSON_NBR.Value, 'BIRTH_DATE')) then
          raise ENoBirthDate.CreateHelp('Birth date is not defined for EE #' + Trim(DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString)
            + '. GTL can not be calculated.', IDH_InconsistentData);
        if 50 <= CalculateAge(PR['CHECK_DATE'], DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').Value, 'BIRTH_DATE'), True) then
            AnnualAmount := AnnualAmount + ConvertNull(ReturnHistValue(SY_FED_TAX_TABLE, 'SY_IRA_CATCHUP_LIMIT'), 0);
      end;

    end;
    if AnnualAmount = 0 then
      Exit;
    GetLimitedTaxAndDedYTDforGTN(PR_CHECK_LINES['PR_CHECK_NBR'], PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value,
      ltDeduction, PR, PR_CHECK, nil, nil, PR_CHECK_LINES, YTDHours, YTD);
    if Abs(PR_CHECK_LINES['AMOUNT'] + YTD) > Abs(AnnualAmount) then
      PR_CHECK_LINES['AMOUNT'] := AnnualAmount - YTD;
  end;


  procedure CheckMaxAmountHours;
  
  begin
  if ctx_DataAccess.CheckMaxAmountAndHours and (not DM_COMPANY.CO.FieldByName('MAXIMUM_HOURS_ON_CHECK').IsNull
  or not DM_COMPANY.CO.FieldByName('MAXIMUM_DOLLARS_ON_CHECK').IsNull) then
  begin
    ApplyTempCheckLinesCheckRange(PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').AsInteger);
    if PR_CHECK_LINES.State = dsInsert then
    begin
      if LeftStr(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE'), 1) = 'D' then
      begin
        lHours := -ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0);
        lDollars := -ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
      end
      else
      begin
        lHours := ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0);
        lDollars := ConvertNull(PR_CHECK_LINES['AMOUNT'], 0);
      end
    end
    else
    begin
      lHours := 0;
      lDollars := 0;
    end;
    cdstTempCheckLines.First;
    while not cdstTempCheckLines.EOF do
    begin
      if LeftStr(CL_E_DS.NewLookup('CL_E_DS_NBR', GetTempCheckLinesDS['CL_E_DS_NBR'], 'E_D_CODE_TYPE'), 1) = 'D' then
      begin
        lHours := lHours - ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0);
        lDollars := lDollars - ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
      end
      else
      begin
        lHours := lHours + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0);
        lDollars := lDollars + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
      end;
      cdstTempCheckLines.Next;
    end;
    MaxExceeds :=0;
    if not DM_COMPANY.CO.FieldByName('MAXIMUM_HOURS_ON_CHECK').IsNull
      and (lHours > DM_COMPANY.CO.FieldByName('MAXIMUM_HOURS_ON_CHECK').AsCurrency) then
    begin
      MaxExceeds := 1;
    end;
    if not DM_COMPANY.CO.FieldByName('MAXIMUM_DOLLARS_ON_CHECK').IsNull
    and (lDollars > DM_COMPANY.CO.FieldByName('MAXIMUM_DOLLARS_ON_CHECK').AsCurrency) then
    begin
        MaxExceeds := MaxExceeds + 2;
    end;
    if MaxExceeds > 0 then
    begin
       employeeNumber := Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
       CL_PERSON.Locate('CL_PERSON_NBR', EE['CL_PERSON_NBR'], []);
       employeeName := Trim(CL_PERSON.FieldByName('Last_Name').AsString)+' '+Trim(CL_PERSON.FieldByName('First_Name').AsString);
       if (MaxExceeds = 1)  then
       begin
         if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_WARN then
         begin
           if Assigned(LogEvent) then
              LogEvent(employeeNumber,employeeName,'hours')
           else if (EvMessage(employeeNumber +': ' +employeeName+ #13 +'Total number of hours exceeds the maximum. Would you like to continue?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
              AbortEx;
         end
         else
         if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_STOP then
         begin
           if Assigned(LogEvent) then
              LogEvent(employeeNumber,employeeName,'hours')
           else
           begin
             EvMessage(employeeNumber +': ' +employeeName+ #13 +'Total number of hours exceeds the maximum.', mtWarning, [mbOk]);
              AbortEx;
           end;   
         end;
       end;
       if MaxExceeds = 2  then
       begin
         if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_WARN then
         begin
           if Assigned(LogEvent) then
              LogEvent(employeeNumber,employeeName,'amounts')
           else if (EvMessage(employeeNumber +': ' +employeeName+ #13 +'Total amount exceeds the maximum. Would you like to continue?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
              AbortEx;
         end
         else
         if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_STOP then
         begin
           if Assigned(LogEvent) then
              LogEvent(employeeNumber,employeeName,'amounts')
           else
           begin
             EvMessage(employeeNumber +': ' +employeeName+ #13 +'Total amount exceeds the maximum.', mtWarning, [mbOk]);
             AbortEx;
           end;  
         end;
       end;
       if MaxExceeds = 3  then
       begin
         if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_WARN then
         begin
           if Assigned(LogEvent) then
              LogEvent(employeeNumber,employeeName,'amounts&hours')
           else if (EvMessage(employeeNumber +': ' +employeeName+ #13 +'Total amount and hours exceeds the maximum. Would you like to continue?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
              AbortEx;
         end
         else
         if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_STOP then
         begin
           if Assigned(LogEvent) then
              LogEvent(employeeNumber,employeeName,'amounts&hours')
           else
           begin
             EvMessage(employeeNumber +': ' +employeeName+ #13 +'Total amount and hours exceeds the maximum.', mtWarning, [mbOk]);
             AbortEx;
           end;
         end;
       end;
    end;
  end;
  end;

begin
  if PR_CHECK_LINES.FieldByName('CO_SHIFTS_NBR').IsNull then
    PR_CHECK_LINES['E_D_DIFFERENTIAL_AMOUNT'] := Null;
// SR-51
  EDType := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
  if PR_CHECK['EE_NBR'] <> EE.FieldByName('EE_NBR').Value then
    EE.Locate('EE_NBR', PR_CHECK['EE_NBR'], []);

  EE_SCHEDULED_E_DS.Activate;
  if RecalcRateForED(EDType) then
    DoRecalcRate(PR_CHECK, PR_CHECK_LINES, CL_E_DS, DM_COMPANY.CO, DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT,
      DM_COMPANY.CO_TEAM, DM_COMPANY.CO_JOBS, EE, EE_RATES, True);

  if ((EDType = ED_OEARN_WAITSTAFF_OVERTIME) or (EDType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME)) and (ConvertNull(PR_CHECK_LINES['RATE_OF_PAY'], 0) = 0) then
    raise EInconsistentData.CreateHelp('Waitstaff Overtime check line can not have a zero rate of pay for employee #' +
      Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);

  if (not PR_CHECK_LINES.FieldByName('RATE_OF_PAY').IsNull) or (EDType = ED_OEARN_WEIGHTED_HALF_TIME_OT)
  or (EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDType = ED_OEARN_AVG_OVERTIME) or (EDType = ED_OEARN_AVG_MULT_OVERTIME)
  or (EDType = ED_EWOD_GROUP_TERM_LIFE) or (EDType = ED_OEARN_PIECEWORK) then
  begin
    if (EDType = ED_MEMO_TIPPED_SALES) or (EDType = ED_MEMO_PENSION) or (EDType = ED_MEMO_PENSION_SUI)
    or (EDType = ED_MEMO_LINE_9) or (EDType = ED_MEMO_WASHINGTON_L_I)
    or (EDType = ED_MEMO_WY_WORKERS_COMP) or (EDType = ED_MEMO_NM_WORKERS_COMP)
    or (PR_CHECK_LINES['RATE_NUMBER'] = 0) then
    begin
      if ((EDType = ED_DED_REIMBURSEMENT) or (EDType = ED_MEMO_COBRA_CREDIT)) and (PR_CHECK_LINES.State = dsInsert) then
        PR_CHECK_LINES['AMOUNT'] := PR_CHECK_LINES.FieldByName('AMOUNT').Value * (-1);
      CheckAnnualMax;
      CheckMaxAmountHours;
      Exit;
    end;

    if ((ConvertNull(PR_CHECK_LINES['HOURS_OR_PIECES'], 0) = 0) and (EDType <> ED_EWOD_GROUP_TERM_LIFE)) then
    begin
      if ((EDType = ED_DED_REIMBURSEMENT) or (EDType = ED_MEMO_COBRA_CREDIT)) and (PR_CHECK_LINES.State = dsInsert) then
        PR_CHECK_LINES['AMOUNT'] := PR_CHECK_LINES.FieldByName('AMOUNT').Value * (-1);
      if not PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').IsNull then
        PR_CHECK_LINES['AMOUNT'] := 0;
      CheckAnnualMax;
      CheckMaxAmountHours;
      if ConvertNull(PR_CHECK_LINES['AMOUNT'], 0) = 0 then
        CreateOffsets; // This is to cleanup offset lines that could have been created when a line had hours/amount
      Exit;
    end;

    if EDType = ED_OEARN_AVG_MULT_OVERTIME then
      CalculateMultipleOvertime(PR_CHECK_LINES, PR_CHECK.FieldByName('EE_NBR').AsInteger)
    else
    if (EDType = ED_OEARN_OVERTIME) or (EDType = ED_OEARN_WAITSTAFF_OVERTIME) or (EDType = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME)
    or (EDType = ED_OEARN_WEIGHTED_HALF_TIME_OT) or (EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDType = ED_OEARN_AVG_OVERTIME) then
      CalculateOvertime(PR_CHECK_LINES, EDType, PR_CHECK.FieldByName('EE_NBR').AsInteger)
    else
    if EDType = ED_EWOD_GROUP_TERM_LIFE then
    begin
      if not PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').IsNull then
        PR_CHECK_LINES['AMOUNT'] := CalculateGroupTermLife(PR, PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value)
    end
    else
    if EDType = ED_OEARN_PIECEWORK then
      CalculatePiecework
    else
    begin
      PR_CHECK_LINES['AMOUNT'] := PR_CHECK_LINES.FieldByName('RATE_OF_PAY').Value *
        PR_CHECK_LINES['HOURS_OR_PIECES'];
      if ((EDType = ED_DED_REIMBURSEMENT) or (EDType = ED_MEMO_COBRA_CREDIT)) and (PR_CHECK_LINES.State <> dsInsert) then
        PR_CHECK_LINES['AMOUNT'] := PR_CHECK_LINES.FieldByName('AMOUNT').Value * (-1);
    end;
  end;

  MinAmount := 0;
  if not VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'EE_SCHEDULED_E_DS_NBR')) then
  begin
    MinAmount := ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'MINIMUM_PAY_PERIOD_AMOUNT'), 0);
    if MinAmount = 0 then
    begin
      MinAmount := Abs(ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'MINIMUM_PAY_PERIOD_PERCENTAGE'), 0));
      if MinAmount <> 0 then
      begin
        try
          EarnCodeGroupNbr := EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'MIN_PPP_CL_E_D_GROUPS_NBR');
        except
          raise EInconsistentData.CreateHelp('Earn Code Group is not setup for minimum pay period percentage calculation for employee #' +
            Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
        end;
        MinAmount := EarnCodeGroupCalc(EarnCodeGroupNbr, True, PR_CHECK) * MinAmount * 0.01;
      end;
    end;
    if (PR_CHECK_LINES['AMOUNT'] < MinAmount) and (MinAmount <> 0) then
    if (ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'TARGET_AMOUNT'), 0) = 0) or
    (ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'BALANCE'), 0) <
    ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'TARGET_AMOUNT'), 0)) then
      PR_CHECK_LINES['AMOUNT'] := MinAmount;
  end;
  if MinAmount = 0 then
  begin
    MinAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'PAY_PERIOD_MINIMUM_AMOUNT'), 0);
    if MinAmount = 0 then
    begin
      MinAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'],
        'PAY_PERIOD_MINIMUM_PERCENT_OF'), 0);
      if MinAmount <> 0 then
      begin
        try
          EarnCodeGroupNbr := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'MIN_CL_E_D_GROUPS_NBR');
        except
          raise EInconsistentData.CreateHelp('Earn Code Group is not setup for minimum pay period percentage calculation for ' +
            CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
        end;
        MinAmount := EarnCodeGroupCalc(EarnCodeGroupNbr, True, PR_CHECK) * MinAmount * 0.01;
      end;
    end;
    if (PR_CHECK_LINES['AMOUNT'] < MinAmount) and (MinAmount <> 0) then
      PR_CHECK_LINES['AMOUNT'] := MinAmount;
  end;

  MaxAmount := 0;
  if not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
  begin
    MaxAmount := ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'MAXIMUM_PAY_PERIOD_AMOUNT'), 0);
    if (MaxAmount = 0) and EE_SCHEDULED_E_DS.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').IsNull then
    begin
      MaxAmount := Abs(ConvertNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'MAXIMUM_PAY_PERIOD_PERCENTAGE'), 0));
      if MaxAmount <> 0 then
      begin
        try
          EarnCodeGroupNbr := EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'MAX_PPP_CL_E_D_GROUPS_NBR');
        except
          raise EInconsistentData.CreateHelp('Earn Code Group is not setup for maximum pay period percentage calculation for employee #' +
            Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), IDH_InconsistentData);
        end;
        MaxAmount := EarnCodeGroupCalc(EarnCodeGroupNbr, True, PR_CHECK) * MaxAmount * 0.01;
      end;
    end;
    if (PR_CHECK_LINES['AMOUNT'] > MaxAmount) and (MaxAmount <> 0) then
      PR_CHECK_LINES['AMOUNT'] := MaxAmount;
  end;
  if MaxAmount = 0 then
  begin
    MaxAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'PAY_PERIOD_MAXIMUM_AMOUNT'), 0);
    if MaxAmount = 0 then
    begin
      MaxAmount := ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'],
        'PAY_PERIOD_MAXIMUM_PERCENT_OF'), 0);
      if MaxAmount <> 0 then
      begin
        try
          EarnCodeGroupNbr := CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'MAX_CL_E_D_GROUPS_NBR');
        except
          raise EInconsistentData.CreateHelp('Earn Code Group is not setup for maximum pay period percentage calculation for ' +
            CL_E_DS.NewLookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
        end;
        MaxAmount := EarnCodeGroupCalc(EarnCodeGroupNbr, True, PR_CHECK) * MaxAmount * 0.01;
      end;
    end;
    if (PR_CHECK_LINES['AMOUNT'] > MaxAmount) and (MaxAmount <> 0) then
      PR_CHECK_LINES['AMOUNT'] := MaxAmount;
  end;
  CheckAnnualMax;
  CreateOffsets;
  CheckMaxAmountHours;
end;

procedure TevPayrollCalculation.AddExtraCheckLines(PR_CHECK_LINES: TevClientDataSet);
var
  CheckNbr, CheckLineNbr: Integer;
begin
  with PR_CHECK_LINES do
  begin
    CheckNbr := FieldByName('PR_CHECK_NBR').AsInteger;
    CheckLineNbr := FieldByName('PR_CHECK_LINES_NBR').AsInteger;
    while LinesToAdd.Count <> 0 do
    begin
      if cdstTempCheckLines.Locate('PR_CHECK_NBR;CL_E_DS_NBR', VarArrayOf([CheckNbr, LinesToAdd.GetEDCodeNbr]), [])
        then
      begin
        Locate('PR_CHECK_LINES_NBR', cdstTempCheckLines['PR_CHECK_LINES_NBR'], []);
        if LinesToAdd.GetAmount = 0 then
          Delete
        else
          Edit;
      end
      else
        if LinesToAdd.GetAmount <> 0 then
      begin
        Insert;
        FieldByName('CL_E_DS_NBR').Value := LinesToAdd.GetEDCodeNbr;
      end;
      if LinesToAdd.GetAmount <> 0 then
      begin
        FieldByName('AMOUNT').Value := LinesToAdd.GetAmount;
        LinesToAdd.AssignFields(PR_CHECK_LINES);
      end;
      LinesToAdd.Delete;
      if State in [dsInsert, dsEdit] then
        Post;
    end;
    Locate('PR_CHECK_LINES_NBR', CheckLineNbr, []);
  end;
end;

function TevPayrollCalculation.GetCustomBankAccountNumber(PR_CHECK: TevClientDataSet): string;
var
  BankAcctNbr, BankNbr: Integer;
begin
  if GetBankAccountNumber(PR_CHECK, BankAcctNbr) then
  begin
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
    try
      Result := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
    except
      raise EInconsistentData.CreateHelp('This company has an incorrect reference to a Service Bureau bank account.', IDH_InconsistentData);
    end;
    BankNbr := VarToInt(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, 'SB_BANKS_NBR'));
  end
  else
  begin
    DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
    Result := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
    BankNbr := VarToInt(DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'SB_BANKS_NBR'));
  end;
  DM_SERVICE_BUREAU.SB_BANKS.Activate;
  FABANumber := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', BankNbr, 'ABA_NUMBER'));
end;

function TevPayrollCalculation.GetBankAccountNumber(PR_CHECK: TevClientDataSet; var AccountNbr: Integer): Boolean; // True-SB Bank Account; False-CL Bank Account
var
  BankAcctLevel, sTableName: String;
  V: Variant;
begin
  Result := True;
  if not DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.DataRequired('ALL');
  if (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) and (not DM_COMPANY.CO.FieldByName('MANUAL_CL_BANK_ACCOUNT_NBR').IsNull) then
  begin
    Result := False;
    AccountNbr := DM_COMPANY.CO.FieldByName('MANUAL_CL_BANK_ACCOUNT_NBR').AsInteger;
  end
  else
  if (DM_COMPANY.CO['OBC'] = 'Y') and (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_VOID]) then
  begin
    if DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').IsNull then
      raise EInconsistentData.CreateHelp('The company uses OBC, but OBC service bureau bank account is not setup.', IDH_InconsistentData);
    AccountNbr := DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').AsInteger;
  end
  else
  if (DM_COMPANY.CO['TRUST_SERVICE'] = TRUST_SERVICE_ALL) and (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_VOID]) then
  begin
    if DM_COMPANY.CO.FieldByName('TRUST_SB_ACCOUNT_NBR').IsNull then
      raise EInconsistentData.CreateHelp('The company is trust, but trust service bureau bank account is not setup.', IDH_InconsistentData);
    AccountNbr := DM_COMPANY.CO.FieldByName('TRUST_SB_ACCOUNT_NBR').AsInteger;
  end
  else
  begin
    Result := False;
    BankAcctLevel := DM_COMPANY.CO['BANK_ACCOUNT_LEVEL'];
    if BankAcctLevel = CLIENT_LEVEL_COMPANY then
    begin
      AccountNbr := DM_COMPANY.CO.FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').AsInteger;
    end
    else
    begin
      V := GetHomeDBDTFieldValue('PAYROLL_CL_BANK_ACCOUNT_NBR', BankAcctLevel, PR_CHECK.FieldByName('EE_NBR').AsInteger,
        DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM, DM_EMPLOYEE.EE, True);
      if DM_COMPANY.CO['BREAK_CHECKS_BY_DBDT'] <> 'N' then
      begin
        case BankAcctLevel[1] of
          CLIENT_LEVEL_DIVISION:
            sTableName := 'CO_DIVISION';
          CLIENT_LEVEL_BRANCH:
            sTableName := 'CO_BRANCH';
          CLIENT_LEVEL_DEPT:
            sTableName := 'CO_DEPARTMENT';
          CLIENT_LEVEL_TEAM:
            sTableName := 'CO_TEAM';
        end;
        DM_PAYROLL.PR_CHECK_LINES.SaveState;
        try
          if not DM_PAYROLL.PR_CHECK_LINES.Active then
            DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
          DM_PAYROLL.PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
          DM_PAYROLL.PR_CHECK_LINES.Filtered := True;
          DM_PAYROLL.PR_CHECK_LINES.First;
          while not DM_PAYROLL.PR_CHECK_LINES.EOF do
          begin
            if not DM_PAYROLL.PR_CHECK_LINES.FieldByName(sTableName + '_NBR').IsNull then
            begin
              ctx_DataAccess.GetDataSet(GetDDTableClassByName(sTableName)).DataRequired('ALL');
              V := ctx_DataAccess.GetDataSet(GetDDTableClassByName(sTableName)).Lookup(sTableName + '_NBR',
                DM_PAYROLL.PR_CHECK_LINES.FieldByName(sTableName + '_NBR').Value, 'PAYROLL_CL_BANK_ACCOUNT_NBR');
              Break;
            end;
            DM_PAYROLL.PR_CHECK_LINES.Next;
          end;
        finally
          DM_PAYROLL.PR_CHECK_LINES.LoadState;
        end;
      end;
      if VarIsNull(V) then
        raise EInconsistentData.CreateHelp('You have chosen a bank account level other than Company and you do not have a payroll bank account setup on that level!', IDH_InconsistentData);
      AccountNbr := V;
    end;
  end;
end;

function TevPayrollCalculation.GetBankAccountNumberForMisc(PR_MISCELLANEOUS_CHECKS: TevClientDataSet; var AccountNbr: Integer): Boolean;
  // True-SB Bank Account; False-CL Bank Account
begin
  Result := True;
  AccountNbr := 0;
  if PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY, MISC_CHECK_TYPE_AGENCY_VOID] then
  begin
    if DM_COMPANY.CO['OBC'] = 'Y' then
    begin
      if DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').IsNull then
        raise EInconsistentData.CreateHelp('The company uses OBC, but OBC service bureau bank account is not setup.', IDH_InconsistentData);
      AccountNbr := DM_COMPANY.CO.FieldByName('SB_OBC_ACCOUNT_NBR').AsInteger;
    end
    else
    if (DM_COMPANY.CO['TRUST_SERVICE'] = TRUST_SERVICE_ALL) or (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value = TRUST_SERVICE_AGENCY) then
    begin
      AccountNbr := DM_COMPANY.CO['TRUST_SB_ACCOUNT_NBR'];
    end
    else
    begin
      DM_CLIENT.CL_AGENCY.Activate;
      AccountNbr := DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR', PR_MISCELLANEOUS_CHECKS['CL_AGENCY_NBR'],
        'CL_BANK_ACCOUNT_NBR');
      Result := False;
    end;
  end;
  if PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_TAX, MISC_CHECK_TYPE_TAX_VOID] then
  begin
    if DM_COMPANY.CO['TAX_SERVICE'] = 'Y' then
    try
      AccountNbr := DM_COMPANY.CO['TAX_SB_BANK_ACCOUNT_NBR'];
    except
      raise EInconsistentData.CreateHelp('Tax SB Bank Account is not setup.', IDH_InconsistentData);
    end
    else
    try
      AccountNbr := DM_COMPANY.CO['TAX_CL_BANK_ACCOUNT_NBR'];
      Result := False;
    except
      raise EInconsistentData.CreateHelp('Tax Client Bank Account is not setup.', IDH_InconsistentData);
    end;
  end;
  if PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_BILLING, MISC_CHECK_TYPE_BILLING_VOID] then
  begin
    if String(ConvertNull(PR_MISCELLANEOUS_CHECKS['EFTPS_TAX_TYPE'], 'N'))[1] in ['T', 'R'] then
    begin
      if PR_MISCELLANEOUS_CHECKS['MISCELLANEOUS_CHECK_AMOUNT'] < 0 then
      begin
        AccountNbr := ConvertNull(DM_COMPANY.CO['BILLING_SB_BANK_ACCOUNT_NBR'], 0);
      end
      else
      begin
        Result := False;
        if PR_MISCELLANEOUS_CHECKS.FieldByName('EFTPS_TAX_TYPE').AsString = 'T' then // Tax Impound Check
          AccountNbr := DM_COMPANY.CO['TAX_CL_BANK_ACCOUNT_NBR']
        else  // Trust ImpoundCheck
          AccountNbr := DM_COMPANY.CO['PAYROLL_CL_BANK_ACCOUNT_NBR'];
      end;
    end
    else
    begin
        if not DM_COMPANY.CO.FieldByName('BILLING_CL_BANK_ACCOUNT_NBR').IsNull then
        begin
          AccountNbr := DM_COMPANY.CO['BILLING_CL_BANK_ACCOUNT_NBR'];
          Result := False;
        end
        else
          AccountNbr := DM_COMPANY.CO['BILLING_SB_BANK_ACCOUNT_NBR'];
    end;
  end;
end;

procedure TevPayrollCalculation.CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet);
var
  EDCodeType, EDCodeDescription: string;
  ClonedEE_SCHEDULED_E_DS: TevClientDataSet;
  ChangeDate: TDateTime;
  DaysInPrenote: Integer;
  Q: IevQuery;
begin
  CL_E_DS.Activate;
  EDCodeType := ConvertNull(CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE'), '');
  EDCodeDescription := ConvertNull(CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'DESCRIPTION'), '');
  if Copy(PR_CHECK.FieldByName('CHECK_TYPE').AsString, 1, 1) = CHECK_TYPE2_3RD_PARTY then
  begin
    if not TypeIs3rdParty(EDCodeType) then
      raise EInconsistentData.CreateHelp('Earning types other than third party are not allowed for third party check.', IDH_InconsistentData);
  end
  else
  if TypeIs3rdParty(EDCodeType) then
    raise EInconsistentData.CreateHelp('Third party earning types are not allowed for check types other than third party.', IDH_InconsistentData);

  // Both Cobra credit (MC type) and Memo (M1 type) with Cobra word in description are allowed for Cobra credit checks

  if (Copy(PR_CHECK.FieldByName('CHECK_TYPE').AsString, 1, 1) = CHECK_TYPE2_COBRA_CREDIT) then
  begin
    if not TypeIsCobraCredit(EdCodeType, EDCodeDescription) then
      raise EInconsistentData.CreateHelp('Earning types other than Cobra credit and M1 type Cobra are not allowed for Cobra credit check type.', IDH_InconsistentData);
  end
  else if EDCodeType = ED_MEMO_COBRA_CREDIT then //TypeIsCobraCredit(EdCodeType, EDCodeDescription) then
    raise EInconsistentData.CreateHelp('Cobra credit is not allowed for check types other than Cobra credit.', IDH_InconsistentData);

  if PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').IsNull then
    PR_CHECK_LINES['PR_CHECK_NBR'] := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
  if PR_CHECK_LINES.FieldByName('LINE_TYPE').IsNull then
  begin
    PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
    EE_SCHEDULED_E_DS.Activate;
    ClonedEE_SCHEDULED_E_DS := EE_SCHEDULED_E_DS.GetClone('CreateCheckLineDefaults');
    ClonedEE_SCHEDULED_E_DS.IndexFieldNames := 'EE_NBR;CL_E_DS_NBR';
    ClonedEE_SCHEDULED_E_DS.Filtered := False;
    ClonedEE_SCHEDULED_E_DS.SetRange([PR_CHECK.FieldByName('EE_NBR').AsInteger, PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger],
      [PR_CHECK.FieldByName('EE_NBR').AsInteger, PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger]);
    ClonedEE_SCHEDULED_E_DS.First;
    if ClonedEE_SCHEDULED_E_DS.EOF then
    begin
      PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Clear;
      PR_CHECK_LINES.FieldByName('CL_AGENCY_NBR').Clear;
    end
    else
    begin
      while not ClonedEE_SCHEDULED_E_DS.EOF do
      begin
        if (PR['CHECK_DATE'] >= ClonedEE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').Value)
        and ((PR['CHECK_DATE'] <= ClonedEE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').Value)
        or ClonedEE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').IsNull) then
        begin
          PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := ClonedEE_SCHEDULED_E_DS.FieldByName('EE_SCHEDULED_E_DS_NBR').Value;
          PR_CHECK_LINES['CL_AGENCY_NBR'] := ClonedEE_SCHEDULED_E_DS.FieldByName('CL_AGENCY_NBR').Value;
          Break;
        end;
        ClonedEE_SCHEDULED_E_DS.Next;
      end;
    end;
  end;

  if PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and (EDCodeType = ED_DIRECT_DEPOSIT) then
  begin
    Q := TevQuery.Create('select x.EE_SCHEDULED_E_DS_NBR from EE_SCHEDULED_E_DS x '+
      'where x.EE_NBR=:EE_NBR and x.CL_E_DS_NBR=:CL_E_DS_NBR and x.EFFECTIVE_START_DATE<=:CHECK_DATE and (x.EFFECTIVE_END_DATE is null or x.EFFECTIVE_END_DATE <=:CHECK_DATE) and {AsOfNow<x>}');
    Q.Param['EE_NBR'] := PR_CHECK['EE_NBR'];
    Q.Param['CL_E_DS_NBR'] := PR_CHECK_LINES['CL_E_DS_NBR'];
    Q.Param['CHECK_DATE'] := ConvertNull(PR['CHECK_DATE'], Now);
    PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := Q.Result['EE_SCHEDULED_E_DS_NBR'];
  end;

  if not (PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_SETUP, PAYROLL_TYPE_IMPORT])
  and ((CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DED_GARNISH)
  or (CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DIRECT_DEPOSIT))
  and PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
  begin
    raise EUpdateError.CreateFmtHelp('Employee %s scheduled E/D code %s must be set up',
      [Trim(ConvertNull(DM_EMPLOYEE.EE.Lookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'CUSTOM_EMPLOYEE_NUMBER'), 'NULL')),
      ConvertNull(CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), 'NULL')],
      IDH_ConsistencyViolation);
  end;
  if (CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DIRECT_DEPOSIT)
  and (PR_CHECK_LINES['LINE_TYPE'] <> CHECK_LINE_TYPE_SCHEDULED) then
  if not VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'EE_DIRECT_DEPOSIT_NBR')) then
  begin
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_DIRECT_DEPOSIT_NBR=' + VarToStr(EE_SCHEDULED_E_DS.NewLookup('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], 'EE_DIRECT_DEPOSIT_NBR')));
    if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.IN_PRENOTE.AsString = 'Y' then
    begin
      DM_SERVICE_BUREAU.SB.DataRequired('ALL');
      if DM_SERVICE_BUREAU.SB['USE_PRENOTE'] <> 'N' then
      begin

        ChangeDate := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.IN_PRENOTE_DATE.AsDateTime;
        Q := TevQuery.Create('SelectLastDDCheckDate');
        Q.Param['CheckDate'] := ChangeDate;
        Q.Param['DirDepNbr'] := PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'];
        if (Q.Result.RecordCount > 0) and not Q.Result.FieldByName('CHECK_DATE').IsNull then
          ChangeDate := Q.Result.FieldByName('CHECK_DATE').AsDateTime
        else
          ChangeDate := StrToDate('01/01/2200');

        if DM_SERVICE_BUREAU.SB.FieldByName('DAYS_IN_PRENOTE').IsNull then
          DaysInPrenote := 14
        else
          DaysInPrenote := DM_SERVICE_BUREAU.SB.FieldByName('DAYS_IN_PRENOTE').AsInteger;

        if (ChangeDate + DaysInPrenote) > PR['CHECK_DATE'] then
          PR_CHECK_LINES['AMOUNT'] := 0;

      end;
    end;
  end;

  if PR_CHECK_LINES.FieldByName('AGENCY_STATUS').IsNull then
    PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_PENDING;
  if PR_CHECK['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'] = 'Y' then
  begin
    if (not PR_CHECK_LINES.FieldByName('CL_AGENCY_NBR').IsNull) and (not PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull) then
      PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_IGNORE;
  end;
  if PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').IsNull then
    PR_CHECK_LINES['HOURS_OR_PIECES'] := CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'DEFAULT_HOURS');
end;

procedure TevPayrollCalculation.DoAutoPost(PR_CHECK, PR_CHECK_LINES, EE, CO_E_D_CODES, CO_TEAM_PR_BATCH_DEFLT_ED, CO_DEPT_PR_BATCH_DEFLT_ED,
  CO_BRCH_PR_BATCH_DEFLT_ED, CO_DIV_PR_BATCH_DEFLT_ED, CO_PR_BATCH_DEFLT_ED: TevClientDataSet);
var
  EENumber: Integer;

  function AutoPostIt(DataSet: TevClientDataSet; KeyRecord: string): Boolean; // True - the dataset was AutoPosted
  var
    TempNumber: Variant;
  begin
    Result := False;
    TempNumber := EE.NewLookup('EE_NBR', EENumber, KeyRecord);
    if not VarIsNull(TempNumber) then
    begin
      DataSet.DataRequired('ALL');
      DataSet.Filter := KeyRecord + '=' + VarToStr(TempNumber);
      DataSet.Filtered := True;
      if DataSet.RecordCount > 0 then
      begin
        DataSet.IndexFieldNames := 'FILLER';
        DataSet.First;
        while not DataSet.EOF do
        begin
          if ((DataSet.FieldByName('SALARY_HOURLY_OR_BOTH').AsString = GROUP_BOX_SALARY) and (ConvertNull(EE.NewLookup('EE_NBR', EENumber,
            'SALARY_AMOUNT'), 0) <> 0))
            or ((DataSet.FieldByName('SALARY_HOURLY_OR_BOTH').AsString = GROUP_BOX_HOURLY) and (ConvertNull(EE.NewLookup('EE_NBR', EENumber,
            'SALARY_AMOUNT'), 0) = 0))
            or (DataSet.FieldByName('SALARY_HOURLY_OR_BOTH').AsString = GROUP_BOX_BOTH2) then
          begin
            PR_CHECK_LINES.Insert;
            PR_CHECK_LINES['CL_E_DS_NBR'] := CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR',
              DataSet['CO_E_D_CODES_NBR'], 'CL_E_DS_NBR');
            PR_CHECK_LINES['RATE_OF_PAY'] := DataSet.FieldByName('OVERRIDE_RATE').Value;
            PR_CHECK_LINES['RATE_NUMBER'] := DataSet.FieldByName('OVERRIDE_EE_RATE_NUMBER').Value;
            if PR_CHECK_LINES.FieldByName('RATE_NUMBER').IsNull and
            (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_SALARY) then
              PR_CHECK_LINES['RATE_NUMBER'] := 0;
            PR_CHECK_LINES['HOURS_OR_PIECES'] := DataSet.FieldByName('OVERRIDE_HOURS').Value;
            PR_CHECK_LINES['AMOUNT'] := DataSet.FieldByName('OVERRIDE_AMOUNT').Value;
            PR_CHECK_LINES['LINE_ITEM_DATE'] := DataSet.FieldByName('OVERRIDE_LINE_ITEM_DATE').Value;
            PR_CHECK_LINES['CO_JOBS_NBR'] := DataSet.FieldByName('CO_JOBS_NBR').Value;
            if EE.FieldByName('WORKATHOME').AsString = 'Y' then
              PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString := 'Y';

            PR_CHECK_LINES.Post;
          end;
          DataSet.Next;
        end;
        DataSet.IndexFieldNames := '';
        Result := True;
      end;
      DataSet.Filtered := False;
    end;
  end;

begin
  if not Assigned(CO_E_D_CODES) then
    Exit;

  EENumber := PR_CHECK.FieldByName('EE_NBR').AsInteger;
  CO_E_D_CODES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_CLIENT.CL_E_DS.Activate;
  if not EE.Active then
    EE.DataRequired('ALL');

  if AutoPostIt(CO_TEAM_PR_BATCH_DEFLT_ED, 'CO_TEAM_NBR') then
    Exit;
  if AutoPostIt(CO_DEPT_PR_BATCH_DEFLT_ED, 'CO_DEPARTMENT_NBR') then
    Exit;
  if AutoPostIt(CO_BRCH_PR_BATCH_DEFLT_ED, 'CO_BRANCH_NBR') then
    Exit;
  if AutoPostIt(CO_DIV_PR_BATCH_DEFLT_ED, 'CO_DIVISION_NBR') then
    Exit;
  AutoPostIt(CO_PR_BATCH_DEFLT_ED, 'CO_NBR');
end;

procedure TevPayrollCalculation.GetLimitedTaxAndDedYTD(PayrollNbr, CheckNbr, EENbr, RecordNbr: Integer; TaxType: TLimitedTaxType; BeginDate, EndDate: TDateTime;
  PR, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LINES: TevClientDataSet; var YTDTaxWage, YTDTax: Real; IgnoreCurrentPayroll: Boolean = False; ForConsolidatesOnly: Boolean = False);
var
  PR_CHECK_2, PR_CHECK_STATES_2, PR_CHECK_SUI_2: TevClientDataSet;
  CheckWasEdited: Boolean;
  q: TExecDSWrapper;
  Q1: IevQuery;
  Nbr, CLPersonNbr, COStateNbr, CoNbr: Integer;
  ees: array of Integer;
  iCount: Integer;
  EDType: String;

  function isEEinList(const Nbr: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to High(ees) do
      if ees[i] >= Nbr then
      begin
        Result := ees[i] = Nbr;
        Break;
      end;
  end;

begin
  Q1 := TevQuery.Create('select CL_PERSON_NBR, CO_NBR from EE where EE_NBR=:EeNbr and {AsOfNow<EE>}');
  Q1.Param['EeNbr'] := EENbr;
  CLPersonNbr := Q1.Result.FieldByName('CL_PERSON_NBR').AsInteger;
  CoNbr := Q1.Result.FieldByName('CO_NBR').AsInteger;

  Q1 := TevQuery.Create('select EE_NBR from EE where CL_PERSON_NBR=:ClPersonNbr and {AsOfNow<EE>} order by CL_PERSON_NBR, EE_NBR');
  Q1.Param['CLPersonNbr'] := CLPersonNbr;

  SetLength(ees, Q1.Result.RecordCount);
  iCount := 0;
  while not Q1.Result.Eof do
  begin
    ees[iCount] := Q1.Result.FieldByName('EE_NBR').AsInteger;
    Inc(iCount);
    Q1.Result.Next;
  end;

  if (BeginDate = GetBeginYear(BeginDate))
  and (Assigned(cdstLimitedEDs) and (TaxType = ltDeduction) and (LastClientNbrForLimitedEDs = ctx_DataAccess.ClientID) and (LastPayrollNbrForLimitedEDs = PayrollNbr)
  or Assigned(cdstLimitedEEOASDI) and (TaxType = ltEEOASDI)
  or Assigned(cdstLimitedEROASDI) and (TaxType = ltEROASDI)
  or Assigned(cdstLimitedEEMedicare) and (TaxType = ltEEMedicare)
  or Assigned(cdstLimitedERMedicare) and (TaxType = ltERMedicare)
  or Assigned(cdstLimitedFUI) and (TaxType = ltFUI)) then
  begin
    YTDTaxWage := 0;
    YTDTax := 0;
    case TaxType of
    ltEEOASDI:
      if cdstLimitedEEOASDI.Locate('CL_PERSON_NBR', CLPersonNbr, []) then
      begin
        YTDTaxWage := ConvertNull(cdstLimitedEEOASDI['TAX_WAGE'], 0);
        YTDTax := ConvertNull(cdstLimitedEEOASDI['TAX'], 0);
      end;
    ltEROASDI:
      if cdstLimitedEROASDI.Locate('CL_PERSON_NBR', CLPersonNbr, []) then
      begin
        YTDTaxWage := ConvertNull(cdstLimitedEROASDI['TAX_WAGE'], 0);
        YTDTax := ConvertNull(cdstLimitedEROASDI['TAX'], 0);
      end;
    ltEEMedicare:
      if cdstLimitedEEMedicare.Locate('CL_PERSON_NBR', CLPersonNbr, []) then
      begin
        YTDTaxWage := ConvertNull(cdstLimitedEEMedicare['TAX_WAGE'], 0);
        YTDTax := ConvertNull(cdstLimitedEEMedicare['TAX'], 0);
      end;
    ltERMedicare:
      if cdstLimitedERMedicare.Locate('CL_PERSON_NBR', CLPersonNbr, []) then
      begin
        YTDTaxWage := ConvertNull(cdstLimitedERMedicare['TAX_WAGE'], 0);
        YTDTax := ConvertNull(cdstLimitedERMedicare['TAX'], 0);
      end;
    ltFUI:
      if cdstLimitedFUI.Locate('CL_PERSON_NBR', CLPersonNbr, []) then
      begin
        YTDTaxWage := ConvertNull(cdstLimitedFUI['TAX_WAGE'], 0);
        YTDTax := ConvertNull(cdstLimitedFUI['TAX'], 0);
      end;
    else {ltDeduction}
      if cdstLimitedEDs.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([EENbr, RecordNbr]), []) then
      begin
        YTDTaxWage := ConvertNull(cdstLimitedEDs['TAX_WAGE'], 0);
        YTDTax := ConvertNull(cdstLimitedEDs['TAX'], 0);
      end;
    end;
  end
  else
  begin
    case TaxType of
      ltEEOASDI:
        begin
          q := TExecDSWrapper.Create('FedTemplate');
          q.SetMacro('F1', 'EE_OASDI_TAXABLE_WAGES+EE_OASDI_TAXABLE_TIPS');
          q.SetMacro('F2', 'EE_OASDI_TAX');
          q.SetMacro('COMBINETAX', 'OASDI');
        end;
      ltEROASDI:
        begin
          q := TExecDSWrapper.Create('FedTemplate');
          q.SetMacro('F1', 'ER_OASDI_TAXABLE_WAGES+ER_OASDI_TAXABLE_TIPS');
          q.SetMacro('F2', 'ER_OASDI_TAX');
          q.SetMacro('COMBINETAX', 'OASDI');
        end;
      ltEEMedicare:
        begin
          q := TExecDSWrapper.Create('FedTemplate');
          q.SetMacro('F1', 'EE_MEDICARE_TAXABLE_WAGES');
          q.SetMacro('F2', 'EE_MEDICARE_TAX');
          q.SetMacro('COMBINETAX', 'MEDICARE');
        end;
      ltERMedicare:
        begin
          q := TExecDSWrapper.Create('FedTemplate');
          q.SetMacro('F1', 'ER_MEDICARE_TAXABLE_WAGES');
          q.SetMacro('F2', 'ER_MEDICARE_TAX');
          q.SetMacro('COMBINETAX', 'MEDICARE');
        end;
      ltFUI:
        begin
          q := TExecDSWrapper.Create('FedTemplate');
          q.SetMacro('F1', 'ER_FUI_TAXABLE_WAGES');
          q.SetMacro('F2', 'ER_FUI_TAX');
          q.SetMacro('COMBINETAX', 'FUI');
        end;
      ltEESDI:
        begin
          q := TExecDSWrapper.Create('SdiTemplate');
          q.SetMacro('F1', 'EE_SDI_TAXABLE_WAGES');
          q.SetMacro('F2', 'EE_SDI_TAX');
          q.SetMacro('Filter', '');
          q.SetParam('CONbrForPM', CoNbr);
        end;
      ltERSDI:
        begin
          q := TExecDSWrapper.Create('SdiTemplate');
          q.SetMacro('F1', 'ER_SDI_TAXABLE_WAGES');
          q.SetMacro('F2', 'ER_SDI_TAX');
          q.SetMacro('Filter', '');
          q.SetParam('CONbrForPM', CoNbr);
        end;
      ltSUI:
        begin
          q := TExecDSWrapper.Create('SuiTemplate');
          q.SetMacro('Filter', '');
          q.SetParam('CONbrForPM', CoNbr);
        end;
    else {ltDeduction:}
      q := TExecDSWrapper.Create('DeductionTemplate');
    end;

    q.SetMacro('F3', '0');
    q.SetParam('CoNbr', CoNbr);
    if ForConsolidatesOnly then
      q.SetParam('Act', 2)
    else
      q.SetParam('Act', 3);
    q.SetParam('EENbr', EENbr);
    q.SetParam('ClPersonNbr', CLPersonNbr);
    q.SetParam('RecordNbr', RecordNbr);
    q.SetParam('PayrollNbr', PayrollNbr);
    q.SetParam('BeginDate', BeginDate);
    q.SetParam('EndDate', EndDate);
    q.SetMacro('Status', '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''');

    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(q.AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;

    YTDTaxWage := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['TAX_WAGE'], 0);
    YTDTax := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['TAX'], 0);
  end;

  if IgnoreCurrentPayroll and (TaxType <> ltDeduction) then
    Exit;
  if PayrollNbr = 0 then
    Exit;
  if PR.Lookup('PR_NBR', PayrollNbr, 'CHECK_DATE') > EndDate then
    Exit;

// Add Prior Checks From Current Payroll
  CheckWasEdited := (PR_CHECK.State in [dsInsert, dsEdit]);
  if CheckWasEdited then
    PR_CHECK.Post;
  PR_CHECK_2 := PR_CHECK;
  PR_CHECK_2.DisableControls;
  try
    PR_CHECK_2.First;
    while (not PR_CHECK_2.EOF) and ((PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger <> CheckNbr) or (TaxType = ltDeduction) and not IgnoreCurrentPayroll) do
    begin
      if (PR_CHECK_2.FieldByName('PR_NBR').AsInteger = PayrollNbr) and isEEinList(PR_CHECK_2.FieldByName('EE_NBR').AsInteger)
      and (not ForConsolidatesOnly or (ForConsolidatesOnly and PR_CHECK_2['EE_NBR'] <> EENbr)) then
      begin
        case TaxType of
          ltEEOASDI:
            begin
              YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_2['EE_OASDI_TAXABLE_WAGES'], 0) + ConvertNull(PR_CHECK_2.FieldByName('EE_OASDI_TAXABLE_TIPS').Value, 0);
              YTDTax := YTDTax + ConvertNull(PR_CHECK_2['EE_OASDI_TAX'], 0);
            end;
          ltEROASDI:
            begin
              YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_2['ER_OASDI_TAXABLE_WAGES'], 0) + ConvertNull(PR_CHECK_2.FieldByName('ER_OASDI_TAXABLE_TIPS').Value, 0);
              YTDTax := YTDTax + ConvertNull(PR_CHECK_2['ER_OASDI_TAX'], 0);
            end;
          ltEEMedicare:
            begin
              YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_2['EE_MEDICARE_TAXABLE_WAGES'], 0);
              YTDTax := YTDTax + ConvertNull(PR_CHECK_2['EE_MEDICARE_TAX'], 0);
            end;
          ltERMedicare:
            begin
              YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_2['ER_MEDICARE_TAXABLE_WAGES'], 0);
              YTDTax := YTDTax + ConvertNull(PR_CHECK_2['ER_MEDICARE_TAX'], 0);
            end;
          ltFUI:
            begin
              YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_2['ER_FUI_TAXABLE_WAGES'], 0);
              YTDTax := YTDTax + ConvertNull(PR_CHECK_2['ER_FUI_TAX'], 0);
            end;
          ltEESDI:
            begin
              if DM_EMPLOYEE.EE_STATES.IndexFieldNames <> 'EE_NBR' then
                DM_EMPLOYEE.EE_STATES.IndexFieldNames := 'EE_NBR';
              DM_EMPLOYEE.EE_STATES.SetRange([EENbr], [EENbr]);
              COStateNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', RecordNbr, 'CO_STATES_NBR'), 0);
              Nbr := 0;
              if COStateNbr <> 0 then
              begin
                DM_EMPLOYEE.EE_STATES.SetRange([PR_CHECK_2.FieldByName('EE_NBR').AsInteger], [PR_CHECK_2.FieldByName('EE_NBR').AsInteger]);
                Nbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', COStateNbr, 'EE_STATES_NBR'), 0);
              end;
              DM_EMPLOYEE.EE_STATES.SetRange([EENbr], [EENbr]);
              if Nbr <> 0 then
              begin
                PR_CHECK_STATES_2 := TevClientDataSet.Create(nil);
                try
                  PR_CHECK_STATES_2.CloneCursor(PR_CHECK_STATES, False);
                  YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
                    VarArrayOf([PR_CHECK_2['PR_CHECK_NBR'], Nbr]), 'EE_SDI_TAXABLE_WAGES'), 0);
                  YTDTax := YTDTax + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
                    VarArrayOf([PR_CHECK_2['PR_CHECK_NBR'], Nbr]), 'EE_SDI_TAX'), 0);
                finally
                  PR_CHECK_STATES_2.Free;
                end;
              end;
            end;
          ltERSDI:
            begin
              if DM_EMPLOYEE.EE_STATES.IndexFieldNames <> 'EE_NBR' then
                DM_EMPLOYEE.EE_STATES.IndexFieldNames := 'EE_NBR';
              DM_EMPLOYEE.EE_STATES.SetRange([EENbr], [EENbr]);
              COStateNbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', RecordNbr, 'CO_STATES_NBR'), 0);
              Nbr := 0;
              if COStateNbr <> 0 then
              begin
                DM_EMPLOYEE.EE_STATES.SetRange([PR_CHECK_2.FieldByName('EE_NBR').AsInteger], [PR_CHECK_2.FieldByName('EE_NBR').AsInteger]);
                Nbr := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR', COStateNbr, 'EE_STATES_NBR'), 0);
              end;
              DM_EMPLOYEE.EE_STATES.SetRange([EENbr], [EENbr]);
              if Nbr <> 0 then
              begin
                PR_CHECK_STATES_2 := TevClientDataSet.Create(nil);
                try
                  PR_CHECK_STATES_2.CloneCursor(PR_CHECK_STATES, False);
                  YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
                    VarArrayOf([PR_CHECK_2['PR_CHECK_NBR'], Nbr]), 'ER_SDI_TAXABLE_WAGES'), 0);
                  YTDTax := YTDTax + ConvertNull(PR_CHECK_STATES_2.Lookup('PR_CHECK_NBR;EE_STATES_NBR',
                    VarArrayOf([PR_CHECK_2['PR_CHECK_NBR'], Nbr]), 'ER_SDI_TAX'), 0);
                finally
                  PR_CHECK_STATES_2.Free;
                end;
              end;
            end;
          ltSUI:
            begin
              PR_CHECK_SUI_2 := TevClientDataSet.Create(nil);
              try
                PR_CHECK_SUI_2.CloneCursor(PR_CHECK_SUI, False);
                YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK_SUI_2.Lookup('PR_CHECK_NBR;CO_SUI_NBR',
                  VarArrayOf([PR_CHECK_2['PR_CHECK_NBR'], RecordNbr]), 'SUI_TAXABLE_WAGES'), 0);
                YTDTax := YTDTax + ConvertNull(PR_CHECK_SUI_2.Lookup('PR_CHECK_NBR;CO_SUI_NBR',
                  VarArrayOf([PR_CHECK_2['PR_CHECK_NBR'], RecordNbr]), 'SUI_TAX'), 0);
              finally
                PR_CHECK_SUI_2.Free;
              end;
            end;
          ltDeduction:
            begin
              EDType := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', RecordNbr, 'E_D_CODE_TYPE');

              Nbr := ConvertNull(cdstTempCheckLines['PR_CHECK_LINES_NBR'], 0);
              if ApplyTempCheckLinesCheckRange(PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger) then
                Nbr := 0;
              cdstTempCheckLines.First;

              while not cdstTempCheckLines.EOF do
              begin
                if ((PR_CHECK_LINES['PR_CHECK_LINES_NBR'] <> cdstTempCheckLines.FieldByName('PR_CHECK_LINES_NBR').Value)
                and (cdstTempCheckLines['CL_E_DS_NBR'] = RecordNbr))
                then
                begin
                  YTDTaxWage := YTDTaxWage + ConvertNull(cdstTempCheckLines['HOURS_OR_PIECES'], 0);
                  YTDTax := YTDTax + ConvertNull(cdstTempCheckLines['AMOUNT'], 0);
                end;
                cdstTempCheckLines.Next;
              end;
              if Nbr <> 0 then
                cdstTempCheckLines.Locate('PR_CHECK_LINES_NBR', Nbr, []);
              if PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger = CheckNbr then
                Break;
            end;
        end;
      end;
      PR_CHECK_2.Next;
    end;

    PR_CHECK.Locate('PR_CHECK_NBR', CheckNbr, []);
    if CheckWasEdited then
      PR_CHECK.Edit;
  finally
    PR_CHECK_2.EnableControls;
  end;
end;

procedure TevPayrollCalculation.GetLimitedTaxAndDedYTDforGTN(CheckNbr, RecordNbr: Integer; TaxType: TLimitedTaxType; PR, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI,
  PR_CHECK_LINES: TevClientDataSet; var YTDTaxWage, YTDTax: Real; StartDate: TDateTime = 0; EndDate: TDateTime = 0; IgnoreCurrentPayroll: Boolean = False);
var
  CheckDate, BeginDate: TDateTime;
  PayrollNbr, EENbr: Integer;
begin
  PayrollNbr := PR_CHECK['PR_NBR'];
  EENbr := PR_CHECK['EE_NBR'];

  if EndDate = 0 then
    CheckDate := PR.Lookup('PR_NBR', PayrollNbr, 'CHECK_DATE')
  else
    CheckDate := EndDate;

  if StartDate = 0 then
    BeginDate := GetBeginYear(CheckDate)
  else
    BeginDate := StartDate;

  if ctx_DataAccess.NewBatchInserted then
  begin
    YTDTaxWage := 0;
    YTDTax := 0;
  end
  else
  begin
    GetLimitedTaxAndDedYTD(PayrollNbr, CheckNbr, EENbr, RecordNbr, TaxType, BeginDate, CheckDate, PR, PR_CHECK, PR_CHECK_STATES,
      PR_CHECK_SUI, PR_CHECK_LINES, YTDTaxWage, YTDTax, IgnoreCurrentPayroll);
  end;
end;

function TevPayrollCalculation.CheckDDPrenote(SchedEDNbr: Integer; CheckDate: TDateTime): Boolean;
var
  ChangeDate: TDateTime;
  DaysInPrenote, EEDirDepNbr: Integer;
  Q: IevQuery;
begin
  Result := False;
  EEDirDepNbr := ConvertNull(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', SchedEDNbr, 'EE_DIRECT_DEPOSIT_NBR'), 0);
  if EEDirDepNbr = 0 then
    Exit;

  DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  if DM_SERVICE_BUREAU.SB['USE_PRENOTE'] = 'N' then
    Exit;

  DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_DIRECT_DEPOSIT_NBR=' + IntToStr(EEDirDepNbr));
  if (DM_EMPLOYEE.EE_DIRECT_DEPOSIT.RecordCount = 0)
  or (DM_EMPLOYEE.EE_DIRECT_DEPOSIT['IN_PRENOTE'] = 'N') then
    Exit;

  ChangeDate := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.IN_PRENOTE_DATE.AsDateTime;

  Q := TevQuery.Create('SelectLastDDCheckDate');
  Q.Param['CheckDate'] := ChangeDate;
  Q.Param['DirDepNbr'] := SchedEDNbr;
  if (Q.Result.RecordCount > 0) and not Q.Result.FieldByName('CHECK_DATE').IsNull then
    ChangeDate := Q.Result.FieldByName('CHECK_DATE').AsDateTime
  else
    ChangeDate := StrToDate('01/01/2200');

  if DM_SERVICE_BUREAU.SB.FieldByName('DAYS_IN_PRENOTE').IsNull then
    DaysInPrenote := 14
  else
    DaysInPrenote := DM_SERVICE_BUREAU.SB.FieldByName('DAYS_IN_PRENOTE').AsInteger;

  if (ChangeDate + DaysInPrenote) <= CheckDate then
  begin
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.Edit;
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT['IN_PRENOTE'] := 'N';
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.Post;
    ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_DIRECT_DEPOSIT]);
  end
  else
    Result := True;
end;

function TevPayrollCalculation.ConstructCheckSortIndex: string;
var
  SortOrder, Delimiter: string;
begin
  Result := '';
  SortOrder := DM_COMPANY.CO['CO_CHECK_SECONDARY_SORT'];
  case SortOrder[1] of
    CLIENT_LEVEL_DIVISION:
      Result := 'CUSTOM_DIVISION_NUMBER';
    CLIENT_LEVEL_BRANCH:
      Result := 'CUSTOM_DIVISION_NUMBER;CUSTOM_BRANCH_NUMBER';
    CLIENT_LEVEL_DEPT:
      Result := 'CUSTOM_DIVISION_NUMBER;CUSTOM_BRANCH_NUMBER;CUSTOM_DEPARTMENT_NUMBER';
    CLIENT_LEVEL_TEAM:
      Result := 'CUSTOM_DIVISION_NUMBER;CUSTOM_BRANCH_NUMBER;CUSTOM_DEPARTMENT_NUMBER;CUSTOM_TEAM_NUMBER';
  end;
  if SortOrder = CLIENT_LEVEL_COMPANY then
    Delimiter := ''
  else
    Delimiter := ';';
  SortOrder := DM_COMPANY.CO['CO_CHECK_PRIMARY_SORT'];
  case SortOrder[1] of
    SORT_FIELD_EE_Code:
      Result := Result + Delimiter + 'CUSTOM_EMPLOYEE_NUMBER';
    SORT_FIELD_SS_Nbr:
      Result := Result + Delimiter + 'SOCIAL_SECURITY_NUMBER';
    SORT_FIELD_ALPHA:
      Result := Result + Delimiter + 'LAST_NAME;FIRST_NAME';
  end;
  if Result <> '' then
    Result := Result + ';';
  Result := Result + 'CHECK_TYPE;PAYMENT_SERIAL_NUMBER';
end;

function TevPayrollCalculation.EDIsBlockedForPR(PayrollNbr, CONbr, CL_E_DS_Nbr: Integer; PR_SCHEDULED_E_DS, CO_E_D_CODES: TevClientDataSet): Boolean;
var
  CO_ED_Code_Nbr: Integer;
begin
  Result := False;
  PR_SCHEDULED_E_DS.Activate;
  CO_E_D_CODES.DataRequired('CO_NBR=' + IntToStr(CONbr));
  CO_ED_Code_Nbr := ConvertNull(CO_E_D_CODES.NewLookup('CL_E_DS_NBR', CL_E_DS_Nbr, 'CO_E_D_CODES_NBR'), 0);
  if CO_ED_Code_Nbr = 0 then
    Exit;
  if ConvertNull(PR_SCHEDULED_E_DS.NewLookup('PR_NBR;CO_E_D_CODES_NBR', VarArrayOf([PayrollNbr, CO_ED_Code_Nbr]), 'EXCLUDE'), 'N') = 'Y' then
    Result := True;
end;

function TevPayrollCalculation.FrequencyBelongs(OneFrequency, MultipleFrequencies: string): Boolean;
begin
  Result := FrequencyIn(OneFrequency, MultipleFrequencies);
end;



procedure TevPayrollCalculation.AddToBankAccountRegister(
             const CoNbr, BankAccountNbr, TaxDepositNbr : integer;
              const Amount : currency;
               const Status, BankRegisterType : Char;
                const CheckDate, DueDate, ProcessDate : TDateTime;
                 out BankAccRegNbr : Integer);
begin
  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;
  try
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_NBR').AsInteger := CoNbr;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').AsCurrency := Amount;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger := TaxDepositNbr;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger := BankAccountNbr;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').AsDateTime := CheckDate;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').AsDateTime := DueDate;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').AsDateTime := ProcessDate;

    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').AsString := BankRegisterType;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := Status;
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').AsString := BANK_REGISTER_MANUALTYPE_None;

    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
    BankAccRegNbr := DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger;
  except
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Cancel;
    raise;
  end;
end;


procedure TevPayrollCalculation.AddConsolidatedPaymentsToBankAccountRegister(
         LiabilityCoTable: TevClientDataset;
          PayingCompanyNbr, TaxDepositNbr : integer;
          const Status, BankRegisterType : Char);
var
  BankAccRegNbr : integer;
  PayingBankAccountNbr, LiabilityBankAccountNbr : integer;
  CheckDate, DueDate, ProcessDate: TDateTime;
begin

 if LiabilityCoTable.RecordCount > 1 then
  LiabilityCoTable.First;
  while not LiabilityCoTable.EOF do
  begin
    if PayingCompanyNbr <> LiabilityCoTable.FieldByName('CoNbr').AsInteger then
    begin
      DM_COMPANY.CO.ShadowDataSet.Locate('CO_NBR', VarArrayOf([LiabilityCoTable.FieldByName('CoNbr').AsInteger]), []);
      if DM_COMPANY.CO.ShadowDataSet['TAX_SERVICE'] <> TAX_SERVICE_NONE then
      begin     // making sure co is on tax service
              // Making consolidated payment
        LiabilityBankAccountNbr := DM_COMPANY.CO.ShadowDataSet['TAX_SB_BANK_ACCOUNT_NBR'];
        Assert(not (VarType(LiabilityBankAccountNbr) in [varEmpty, varNull]), 'Error making consolidated payments: Tax Bank Account of consolidated company is not defined.');

        DM_COMPANY.CO.ShadowDataSet.Locate('CO_NBR', VarArrayOf([PayingCompanyNbr]), []);
        PayingBankAccountNbr := DM_COMPANY.CO.ShadowDataSet['TAX_SB_BANK_ACCOUNT_NBR'];

        CheckDate := DateOf(SysTime);

        DueDate := LiabilityCoTable['DueDate'];
        ProcessDate := DateOf(SysTime);

        AddToBankAccountRegister(LiabilityCoTable.FieldByName('CoNbr').AsInteger,
                                  LiabilityBankAccountNbr,
                                  TaxDepositNbr,
                                  -LiabilityCoTable.FieldByName('Amount').AsCurrency,
                                  Status, BankRegisterType,
                                  CheckDate, DueDate, ProcessDate,
                                  BankAccRegNbr);
        AddToBankAccountRegister(PayingCompanyNbr,
                                  PayingBankAccountNbr,
                                  TaxDepositNbr,
                                  LiabilityCoTable.FieldByName('Amount').AsCurrency,
                                  Status, BankRegisterType,
                                  CheckDate, DueDate, ProcessDate,
                                  BankAccRegNbr);
      end;
    end;
   LiabilityCoTable.Next;
  end;

end;

procedure TevPayrollCalculation.AddConsolidatedPaymentsToBar(
                    DS: array of TevClientDataSet;
                     const TaxDepositNbr: Integer;
                      const Status, BankRegisterType : char;
                      const bDepNbrFilter : Boolean = True);
  Procedure UpdateLiabCoList(ds, cd : TevClientDataSet);
  begin
    if bDepNbrFilter then
    begin
     cd.Filter := 'CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr);
     cd.Filtered := True;
    end;
    cd.First;
    while not cd.Eof do
    begin
     Assert(DM_COMPANY.CO.Locate('CO_NBR', cd['CO_NBR'], []));
     if not ds.Locate('CoNbr', cd.fieldbyName('CO_NBR').AsInteger, []) then
       ds.AppendRecord([cd.fieldbyName('CO_NBR').AsInteger,
                         cd.fieldbyName('DUE_DATE').AsDateTime,
                          cd.fieldbyName('AMOUNT').AsCurrency])
     else
     begin
      ds.Edit;
      ds.FieldByName('Amount').AsCurrency := ds.FieldByName('Amount').AsCurrency + cd.fieldbyName('AMOUNT').AsCurrency;
      ds.post;
     end;
      cd.Next;
    end;
    if bDepNbrFilter then
       cd.Filtered := False;
  end;

var
  i, PayingCompanyNbr: integer;
  cdLiabCoList: TevClientDataset;

begin
  Assert(DM_COMPANY.CO_TAX_DEPOSITS.Locate('CO_TAX_DEPOSITS_NBR', TaxDepositNbr, []));
  PayingCompanyNbr := DM_Company.CO_TAX_DEPOSITS.FieldByName('CO_NBR').AsInteger;

  cdLiabCoList := TevClientDataSet.Create(nil);
  try
    cdLiabCoList.FieldDefs.Add('CoNbr', ftInteger, 0, True);
    cdLiabCoList.FieldDefs.Add('DueDate', ftDateTime, 0, True);
    cdLiabCoList.FieldDefs.Add('Amount', ftCurrency, 0, True);
    cdLiabCoList.CreateDataSet;
    cdLiabCoList.LogChanges := False;
    cdLiabCoList.AddIndex('idxCoNbr', 'CoNbr', []);
    cdLiabCoList.IndexDefs.Update;
    cdLiabCoList.IndexName := 'idxCoNbr';

    for i := Low(DS) to High(DS) do
       UpdateLiabCoList(cdLiabCoList,DS[i]);

    if cdLiabCoList.RecordCount > 0 then
       AddConsolidatedPaymentsToBankAccountRegister(cdLiabCoList, PayingCompanyNbr,
                                            TaxDepositNbr, Status, BankRegisterType);
  finally
   cdLiabCoList.Free;
  end;
end;


procedure TevPayrollCalculation.UpdateLiabilityTableStatus(
  LiabilityTable: TevClientDataSet; TaxDepositNbr, PayingCompanyNbr: integer);
begin
  LiabilityTable.Filter := 'CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr);
  LiabilityTable.Filtered := True;
  LiabilityTable.First;
  while not LiabilityTable.EOF do
  begin
    LiabilityTable.Edit;
    Assert(DM_COMPANY.CO.Locate('CO_NBR', LiabilityTable['CO_NBR'], []));
    if (LiabilityTable.FieldByName('IMPOUNDED').AsString = GROUP_BOX_NO) and
        CompanyRequiresTaxesBeImpounded then
      LiabilityTable['STATUS'] := TAX_DEPOSIT_STATUS_PAID_WO_FUNDS
    else
      LiabilityTable['STATUS'] := TAX_DEPOSIT_STATUS_PAID;
    LiabilityTable.Post;
    LiabilityTable.Next;
  end;
  LiabilityTable.Filtered := False;
end;

procedure TevPayrollCalculation.UpdateAllLiabilityTablesStatus(TaxDepositNbr: Integer);
var
  PayingCompanyNbr: integer;
begin
  Assert(DM_COMPANY.CO_TAX_DEPOSITS.Locate('CO_TAX_DEPOSITS_NBR', TaxDepositNbr, []));
  PayingCompanyNbr := DM_Company.CO_TAX_DEPOSITS.FieldByName('CO_NBR').AsInteger;

  UpdateLiabilityTableStatus(DM_COMPANY.CO_FED_TAX_LIABILITIES, TaxDepositNbr, PayingCompanyNbr);
  UpdateLiabilityTableStatus(DM_COMPANY.CO_STATE_TAX_LIABILITIES, TaxDepositNbr, PayingCompanyNbr);
  UpdateLiabilityTableStatus(DM_COMPANY.CO_SUI_LIABILITIES, TaxDepositNbr, PayingCompanyNbr);
  UpdateLiabilityTableStatus(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, TaxDepositNbr, PayingCompanyNbr);

  AddConsolidatedPaymentsToBar([DM_COMPANY.CO_FED_TAX_LIABILITIES,DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                                DM_COMPANY.CO_SUI_LIABILITIES,DM_COMPANY.CO_LOCAL_TAX_LIABILITIES], TaxDepositNbr,
                                 BANK_REGISTER_STATUS_Outstanding, BANK_REGISTER_TYPE_Consolidated);

end;

procedure TevPayrollCalculation.GetFedTaxDepositPeriod(Frequency: Char; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
begin
  case Frequency of
    FREQUENCY_TYPE_DAILY:
      begin
        BeginDate := Date;
        EndDate := Date;
        DueDate := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 1, Date+ 1, Date); //Next day
      end;
    FREQUENCY_TYPE_SEMI_WEEKLY:
      begin
        if DayOfWeek(Date) in [4, 5, 6] then // Wed-Fri
        begin
          BeginDate := Date - (DayOfWeek(Date) - 4); // Wed
          EndDate := BeginDate + 2; // Fri
          DueDate := CheckForFedDaysRule(BeginDate + 7, EndDate); // Next Wed
        end
        else
        begin
          BeginDate := Date - (DayOfWeek(Date) mod 7); // Sat
          EndDate := BeginDate + 3; // Tues
          DueDate := CheckForFedDaysRule(BeginDate + 6, EndDate); // Next Friday
        end;
        CheckQuarterBeginEnd(Date, BeginDate, EndDate);
      end;
    FREQUENCY_TYPE_MONTHLY:
      begin
        BeginDate := GetBeginMonth(Date);
        EndDate := GetEndMonth(Date);
        DueDate := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 0, EndDate + 15, EndDate + 15);
      end;
    FREQUENCY_TYPE_QUARTERLY:
      begin
        BeginDate := GetBeginQuarter(Date);
        EndDate := GetEndQuarter(Date);
        DueDate := GetEndMonth(EndDate+1);
        DueDate := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 0, DueDate, DueDate);
      end;
  else
    Assert(False);
  end;
end;

function TevPayrollCalculation.GetFedTaxDepositFreq(CoNbr: Integer): string;
var
  V: Variant;
begin
  with DM_Company do
  begin
    CO.DataRequired('ALL');
  end;
  V := DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'Federal_Tax_Deposit_Frequency');
  if not VARISNULL(V) then
    Result := V;
end;

procedure TevPayrollCalculation.GetTaxDepositPeriodFromDataSet(const ds: TDataSet; const Date: TDateTime; const SY_AGENCY_NBR: Integer; var BeginDate, EndDate, DueDate: TDateTime; const TaxState: string);
    function CheckForNBusDaysRule(const Days: Integer; const DefaultDate: TDateTime): TDateTime;
    var
      d: TDateTime;
      i: Integer;
    begin
      if Days > 0 then
        d := DefaultDate+ 1
      else
        d := DefaultDate;
      i := 1;
      repeat
        if (DayOfWeek(d) in [1, 7]) then
          d := d+ 1
        else
        if i < Days then
        begin
          d := d+ 1;
          Inc(i);
        end
        else
          Break;
      until False;
      Result := d;
    end;
var
  Year, Month, Day: word;
  NextEventDate: TDate;
  FedDueDate: TDate;
  WHEN_DUE_TYPE, F_TYPE: Char;
begin
  DueDate := 0;
  FedDueDate := -1;
  NextEventDate := -1;
  with ds do
  begin
    WHEN_DUE_TYPE := FieldByName('WHEN_DUE_TYPE').AsString[1];
    F_TYPE := FieldByName('FREQUENCY_TYPE').AsString[1];
    if F_TYPE = FREQUENCY_TYPE_FOLLOW_FED then
    begin
      F_TYPE := DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString[1];
      WHEN_DUE_TYPE := WHEN_DUE_FOLLOW_FEDERAL;
    end;
    case F_TYPE of
      FREQUENCY_TYPE_ANNUAL:
        begin
          BeginDate := GetBeginYear(Date);
          EndDate := GetEndYear(Date);
          NextEventDate := GetEndYear(GetEndYear(Date) + 1);
        end;
      FREQUENCY_TYPE_SEMI_ANNUAL:
        begin
          DecodeDate(Date, Year, Month, Day);
          if Month < 7 then
          begin
            BeginDate := EncodeDate(Year, 1, 1);
            EndDate := EncodeDate(Year, 6, 30);
            NextEventDate := EncodeDate(Year, 12, 31);
          end
          else
          begin
            BeginDate := EncodeDate(Year, 7, 1);
            EndDate := EncodeDate(Year, 12, 31);
            NextEventDate := EncodeDate(Year + 1, 6, 30);
          end;
        end;
      FREQUENCY_TYPE_QUARTERLY_NY, FREQUENCY_TYPE_QUARTERLY:
        begin
          BeginDate := GetBeginQuarter(Date);
          EndDate := GetEndQuarter(Date);
          FedDueDate := GetEndMonth(EndDate+1);
          NextEventDate := GetEndQuarter(GetEndQuarter(Date) + 1);
        end;
      FREQUENCY_TYPE_SPLIT_MONTHLY_ID:
        begin
          if DayOf(Date) >= 16 then
            BeginDate := GetBeginMonth(Date)+ 15 // 16th
          else
            BeginDate := GetBeginMonth(GetBeginMonth(Date)-1)+ 15; // 16th last month
          EndDate := GetEndMonth(BeginDate)+ 15; // 15th next month after begin date
          NextEventDate := GetEndMonth(EndDate)+ 15; // 15th next month after end date
          DueDate := EndDate+ 5; // the 20th
        end;
      FREQUENCY_TYPE_MONTHLY:
        begin
          BeginDate := GetBeginMonth(Date);
          EndDate := GetEndMonth(Date);
          NextEventDate := GetEndMonth(GetEndMonth(Date) + 1);
          FedDueDate := EndDate + 15;
          if (MonthOf(Date) = 12) and (TaxState = 'PA') then
            DueDate := EncodeDate(GetYear(Date)+1, 1, 31)
          else
          if (GetMonth(Date) = 1) and (TaxState = 'VT') then
            DueDate := EncodeDate(GetYear(Date), 2, 23)
          else
          if (GetMonth(Date) = 12) and (TaxState = 'VT') then
            NextEventDate := EncodeDate(GetYear(Date)+1, 2, 23)
          else
          if (GetMonth(Date) mod 3 = 0) and (TaxState = 'NJ') then
            DueDate := EndDate+ 30
          else
          if (name <> 'SY_AGENCY_DEPOSIT_FREQ') and   //    TCD deposit Freq..
             (FieldByName('THRD_MNTH_DUE_END_OF_NEXT_MNTH').Value = 'Y') and (GetMonth(Date) mod 3 = 0) then
               DueDate := GetEndMonth(GetEndMonth(Date) + 1);
        end;
      FREQUENCY_TYPE_SEMI_MONTHLY:
        begin
          DecodeDate(Date, Year, Month, Day);
          if Day < 16 then
          begin
            BeginDate := GetBeginMonth(Date);
            EndDate := EncodeDate(Year, Month, 15);
            NextEventDate := GetEndMonth(Date);
          end
          else
          begin
            BeginDate := EncodeDate(Year, Month, 16);
            EndDate := GetEndMonth(Date);
            Inc(Month);
            if Month > 12 then
            begin
              Month := 1;
              Inc(Year);
            end;
            NextEventDate := EncodeDate(Year, Month, 15);
          end;
        end;
      FREQUENCY_TYPE_QUAD_MONTHLY:
        begin
          DecodeDate(Date, Year, Month, Day);
          if Day < 9 then
          begin
            BeginDate := GetBeginMonth(Date);
            EndDate := EncodeDate(Year, Month, 8);
            NextEventDate := EncodeDate(Year, Month, 15);
          end
          else
            if Day < 16 then
          begin
            BeginDate := EncodeDate(Year, Month, 8);
            EndDate := EncodeDate(Year, Month, 15);
            NextEventDate := EncodeDate(Year, Month, 22);
          end
          else
            if Day < 23 then
          begin
            BeginDate := EncodeDate(Year, Month, 16);
            EndDate := EncodeDate(Year, Month, 22);
            NextEventDate := GetEndMonth(Date);
          end
          else
          begin
            BeginDate := EncodeDate(Year, Month, 23);
            EndDate := GetEndMonth(Date);
            Inc(Month);
            if Month > 12 then
            begin
              Month := 1;
              Inc(Year);
            end;
            NextEventDate := EncodeDate(Year, Month, 8);
          end;
        end;
      FREQUENCY_TYPE_QUAD_MONTHLY_IL_MA:
        begin
          DecodeDate(Date, Year, Month, Day);
          if Day < 8 then
          begin
            BeginDate := GetBeginMonth(Date);
            EndDate := EncodeDate(Year, Month, 7);
            NextEventDate := EncodeDate(Year, Month, 15);
          end
          else
            if Day < 16 then
          begin
            BeginDate := EncodeDate(Year, Month, 8);
            EndDate := EncodeDate(Year, Month, 15);
            NextEventDate := EncodeDate(Year, Month, 22);
          end
          else
            if Day < 23 then
          begin
            BeginDate := EncodeDate(Year, Month, 16);
            EndDate := EncodeDate(Year, Month, 22);
            NextEventDate := GetEndMonth(Date);
          end
          else
          begin
            BeginDate := EncodeDate(Year, Month, 23);
            EndDate := GetEndMonth(Date);
            Inc(Month);
            if Month > 12 then
            begin
              Month := 1;
              Inc(Year);
            end;
            NextEventDate := EncodeDate(Year, Month, 7);
          end;
        end;
      FREQUENCY_TYPE_QUAD_MONTHLY_KS:
        begin
          DecodeDate(Date, Year, Month, Day);
          if Day < 8 then
          begin
            BeginDate := GetBeginMonth(Date);
            EndDate := EncodeDate(Year, Month, 7);
            NextEventDate := EncodeDate(Year, Month, 15);
          end
          else
            if Day < 16 then
          begin
            BeginDate := EncodeDate(Year, Month, 8);
            EndDate := EncodeDate(Year, Month, 15);
            NextEventDate := EncodeDate(Year, Month, 21);
          end
          else
            if Day < 22 then
          begin
            BeginDate := EncodeDate(Year, Month, 16);
            EndDate := EncodeDate(Year, Month, 21);
            NextEventDate := GetEndMonth(Date);
          end
          else
          begin
            BeginDate := EncodeDate(Year, Month, 22);
            EndDate := GetEndMonth(Date);
            Inc(Month);
            if Month > 12 then
            begin
              Month := 1;
              Inc(Year);
            end;
            NextEventDate := EncodeDate(Year, Month, 7);
          end;
        end;
      FREQUENCY_TYPE_WEEKLY:
        begin
          BeginDate := Date - DayOfWeek(Date) + 1;
          EndDate := Date + 7 - DayOfWeek(Date);
          NextEventDate := EndDate + 7;
          CheckQuarterBeginEnd(Date, BeginDate, EndDate);
        end;
      FREQUENCY_TYPE_WEEKLY_CT:
        begin
          if DayOfWeek(Date) <> 7 {not Sat} then
            BeginDate := Date- DayOfWeek(Date) {find last Sat}
          else
            BeginDate := Date;
          EndDate := BeginDate+ 6;
          NextEventDate := EndDate + 7;
          CheckQuarterBeginEnd(Date, BeginDate, EndDate);
        end;
      FREQUENCY_TYPE_WEEKLY_CO:
        begin
          if DayOfWeek(Date) <> 7 {not Sat} then
            BeginDate := Date- DayOfWeek(Date) {find last Sat}
          else
            BeginDate := Date;
          EndDate := BeginDate+ 6;
          NextEventDate := EndDate + 7;

          if DayOfTheYear(Date)> 358 then
             CheckQuarterBeginEnd(Date, BeginDate, EndDate);
        end;
      FREQUENCY_TYPE_SEMI_WEEKLY:
        begin
          if DayOfWeek(Date) in [4, 5, 6] then // Wed-Fri
          begin
            BeginDate := Date - (DayOfWeek(Date) - 4); // Wed
            EndDate := BeginDate + 2; // Fri
            NextEventDate := EndDate + 4;
            FedDueDate := CheckForFedDaysRule(BeginDate + 7, EndDate); // Next Wed or later
          end
          else
          begin
            BeginDate := Date - (DayOfWeek(Date) mod 7); // Sat
            EndDate := BeginDate + 3; // Tues
            NextEventDate := EndDate + 3;
            FedDueDate := CheckForFedDaysRule(BeginDate + 6, EndDate); // Next Friday or later
          end;
          CheckQuarterBeginEnd(Date, BeginDate, EndDate);
        end;
      FREQUENCY_TYPE_CHECK_DATE_NY, FREQUENCY_TYPE_CHECK_DATE:
        begin
          BeginDate := Date;
          EndDate := Date;
          NextEventDate := NextCheckDate(Date);
        end;
      FREQUENCY_TYPE_NOW:
        begin
          BeginDate := Date;
          EndDate := Date;
          DueDate := Date + 1;
        end;
      FREQUENCY_TYPE_DAILY:
        begin
          BeginDate := GetBeginMonth(Date);
          EndDate := GetEndMonth(Date);
          DueDate := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 1, Date+ 1, Date); //Next day
          NextEventDate := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 2, Date+ 1, Date); //Next day
        end;
    end;
    if DueDate = 0 then
      case WHEN_DUE_TYPE of
        WHEN_DUE_SOME_DAYS_AFTER_THIS_EVENT:
          DueDate := EndDate + FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').AsInteger;
        WHEN_DUE_BUS_DAYS_AFTER_THIS_EVENT:
          DueDate := CheckForNDaysRule(SY_AGENCY_NBR, FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').AsInteger, EndDate, EndDate);
        WHEN_DUE_SOME_DAYS_BEFORE_NEXT_EVENT:
          DueDate := NextEventDate - FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').AsInteger;
        WHEN_DUE_FOLLOW_FEDERAL, WHEN_DUE_FOLLOW_FEDERAL_NO_100K :
          if FedDueDate = -1 then
            raise EInconsistentData.CreateHelp('"Follow federal" in "when due" can be used only with semi-weekly, monthly, qarterly and next day', IDH_InconsistentData)
          else
            DueDate := FedDueDate;
        WHEN_DUE_LAST_DAY_OF_NEXT_MONTH:
          DueDate := GetEndMonth(GetEndMonth(EndDate) + 1);
        WHEN_DUE_SEMI_MONTHLY_15TH_EOM:
          if DayOf(EndDate)>= 16 then
            DueDate := EndDate + FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').AsInteger
          else
            DueDate := GetEndMonth(EndDate);
        WHEN_DUE_LAST_DAY_OF_SECOND_MONTH:
          DueDate := GetEndMonth(GetEndMonth(GetEndMonth(EndDate) + 1) + 1);
        WHEN_DUE_SOME_DAYS_OF_WEEKDAYS :
          DueDate := CheckForNBusDaysRule(FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').AsInteger, EndDate);
      else
        Assert(False);
      end;
  end;

  DueDate := CheckDueDateForHoliday(DueDate, SY_AGENCY_NBR);

  if (TaxState = 'GA') and (SY_AGENCY_NBR = 9) then
  begin
    EndDate := GetEndMonth(Date);
  end;
end;

function TevPayrollCalculation.GetStateTaxPaymenyAgencyNbr(State: string): Integer;
begin
  with DM_SYSTEM_STATE do
  begin
    SY_STATES.Activate;
    Assert(SY_STATES.Locate('STATE',State,[]));
    Result := SY_STATES.FieldByName('SY_STATE_TAX_PMT_AGENCY_NBR').AsInteger;
  end;
end;

function TevPayrollCalculation.LiabilityACH_KEY_CalcMethod(TaxTypeDesc : char; DepFreqNbr, SyNbr : Integer;
                                    const PrCheckDate:TDateTime;
                                    const calcEndDate:TDateTime = 0): String;
  function LastCheckDateInPeriod(const BeginDate, EndDate: TDateTime): TDateTime;
  begin
      DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      with DM_PAYROLL.PR_SCHEDULED_EVENT do
      begin
        IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
        SetRange([BeginDate], [EndDate]);
        try
          Last;
          Result := FieldByName('SCHEDULED_CHECK_DATE').AsDateTime;
        finally
          CancelRange;
          IndexFieldNames := '';
        end;
      end;
  end;
var
 s: string;
 FrequencyType : Char;
 BeginDate, EndDate, DueDate, LastCheckDate: TDateTime;
 SeqNumber: Integer;
Begin
    result := '';
    FrequencyType := 'M';
    case TaxTypeDesc of
       STATE_TAX_DESC :
                 Begin
                   s := ExtractFromFiller(ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SyNbr, 'SY_STATE_TAX_PMT_AGENCY_NBR'), 'FILLER'), ''), 1, 1);
                   FrequencyType := string(ConvertNull(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATE_DEPOSIT_FREQ_NBR', DepFreqNbr, 'FREQUENCY_TYPE'), '-'))[1];
                 End;
       LOCAL_TAX_DESC :
                 Begin
                   s := ExtractFromFiller(ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DM_SYSTEM_STATE.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyNbr, 'SY_LOCAL_TAX_PMT_AGENCY_NBR'), 'FILLER'), ''), 2, 1);
                   FrequencyType := string(ConvertNull(DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Lookup('SY_LOCAL_DEPOSIT_FREQ_NBR', DepFreqNbr, 'FREQUENCY_TYPE'), '-'))[1];
                 End;
    end;
    if s <> '' then
      case s[1] of
        EFT_END_DATE_METHOD_QUARTER_END:
          result := FormatDateTime('MM/DD/YYYY', GetEndQuarter(PrCheckDate))+ IntToHexChar(GetQuarterNumber(PrCheckDate));

        EFT_END_DATE_METHOD_MONTH_END:
          result  := FormatDateTime('MM/DD/YYYY', GetEndMonth(PrCheckDate))+ IntToHexChar(GetMonth(PrCheckDate));

        EFT_END_DATE_METHOD_ON_FREQ:
           Begin
              case TaxTypeDesc of
               STATE_TAX_DESC :
                    ctx_PayrollCalculation.GetStateTaxDepositPeriodAndSeqNumber(DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SyNbr, 'STATE'), DepFreqNbr, PrCheckDate, BeginDate, EndDate, DueDate, SeqNumber);
               LOCAL_TAX_DESC :
                    ctx_PayrollCalculation.GetLocalTaxDepositPeriodAndSeqNumber(SyNbr, DepFreqNbr, PrCheckDate, BeginDate, EndDate, DueDate, SeqNumber);
              end;

              if calcEndDate <> 0 then
                 EndDate:= calcEndDate;

              result  := FormatDateTime('MM/DD/YYYY', EndDate) + IntToHexChar(SeqNumber);
           end;
        EFT_END_DATE_METHOD_ON_FREQ_VI:
          begin
            case FrequencyType of
            FREQUENCY_TYPE_MONTHLY:
              result  := FormatDateTime('MM/DD/YYYY', GetEndMonth(PrCheckDate))+ IntToHexChar(GetMonth(PrCheckDate));
            FREQUENCY_TYPE_SEMI_WEEKLY,
            FREQUENCY_TYPE_QUARTERLY:
              result  := FormatDateTime('MM/DD/YYYY', GetEndQuarter(PrCheckDate)) + IntToHexChar(GetQuarterNumber(PrCheckDate));
            else
              raise EInconsistentData.CreateHelp('Agency EFT end date calculation method does not match current deposit frequency', IDH_InconsistentData);
            end;
          end;

        EFT_END_DATE_METHOD_ON_FREQ_CA:
          begin
            if DepFreqNbr = 172 then // CA Quarterly
              result := FormatDateTime('MM/DD/YYYY', GetEndQuarter(PrCheckDate))+IntToHexChar(GetQuarterNumber(PrCheckDate))
            else
              result := FormatDateTime('MM/DD/YYYY', PrCheckDate)+ '0';
          end;

        EFT_END_DATE_METHOD_ON_FREQ_MA:
          begin
            case FrequencyType of
            FREQUENCY_TYPE_QUAD_MONTHLY_IL_MA, FREQUENCY_TYPE_QUARTERLY:
              result  := FormatDateTime('MM/DD/YYYY', GetEndQuarter(PrCheckDate))+
                IntToHexChar(GetQuarterNumber(PrCheckDate));
            FREQUENCY_TYPE_MONTHLY:
              result  := FormatDateTime('MM/DD/YYYY', GetEndMonth(PrCheckDate))+
                '0';
            FREQUENCY_TYPE_ANNUAL:
              result  := FormatDateTime('MM/DD/YYYY', GetEndYear(PrCheckDate))+
                '0';
            else
              raise EInconsistentData.CreateHelp('Agency EFT end date calculation method does not match current deposit frequency', IDH_InconsistentData);
            end;
          end;

        EFT_END_DATE_METHOD_PROCESSED_LAST_CHECK:
          result := FormatDateTime('MM/DD/YYYY', PrCheckDate) + Format('%1u', [GetQuarterNumber(PrCheckDate)]);
        EFT_END_DATE_METHOD_LAST_CHECK:
          begin
            case TaxTypeDesc of
              STATE_TAX_DESC :
                   ctx_PayrollCalculation.GetStateTaxDepositPeriodAndSeqNumber(DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', SyNbr, 'STATE'), DepFreqNbr, PrCheckDate, BeginDate, EndDate, DueDate, SeqNumber);
              LOCAL_TAX_DESC :
                   ctx_PayrollCalculation.GetLocalTaxDepositPeriodAndSeqNumber(SyNbr, DepFreqNbr, PrCheckDate, BeginDate, EndDate, DueDate, SeqNumber);
             end;

            LastCheckDate := LastCheckDateInPeriod(BeginDate, EndDate);
            if LastCheckDate = 0 then
              LastCheckDate := PrCheckDate;
            result  := FormatDateTime('MM/DD/YYYY', LastCheckDate)+ '0';
          end;
        else
          result := FormatDateTime('MM/DD/YYYY', GetEndQuarter(PrCheckDate))+ Format('%1u', [GetQuarterNumber(PrCheckDate)]);
      end;
end;


procedure TevPayrollCalculation.GetStateTaxDepositPeriod(State: string; FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
begin
  with DM_SYSTEM_STATE do
  begin
    SY_STATES.Activate;
    {if SY_STATES.Lookup('STATE',State,'TAX_DEPOSIT_FOLLOW_FEDERAL')='Y' then
    begin // changed to FREQUENCY_TYPE_FOLLOW_FED
      GetFedTaxDepositPeriod(DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString[1],Date,BeginDate,EndDate,DueDate);
      Exit;
    end;}
    SY_STATE_DEPOSIT_FREQ.Activate;
    if not SY_STATE_DEPOSIT_FREQ.Locate('SY_STATE_DEPOSIT_FREQ_NBR', FrequencyNbr, []) then
      raise EInconsistentData.CreateHelp('Frequency is not set for state ' + State, IDH_InconsistentData);
    GetTaxDepositPeriodFromDataSet(SY_STATE_DEPOSIT_FREQ, Date, GetStateTaxPaymenyAgencyNbr(State), BeginDate, EndDate, DueDate, State);
  end;
end;

function TevPayrollCalculation.GetStateTaxDepositFreq(SyFreqNbr: Integer; CoNbr: Integer): string;
var
  SyStatesNbr: Integer;
  FreqType: string;
begin
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Activate;
  DM_SYSTEM_STATE.SY_STATES.Activate;

  SyStatesNbr := ConvertNull(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('sy_state_deposit_freq_nbr', syFreqNbr, 'sy_states_nbr'), 0);

  if DM_SYSTEM_STATE.SY_STATES.Lookup('sy_states_nbr', SyStatesNbr, 'tax_deposit_follow_federal') = 'N' then
  begin
    FreqType := ConvertNull(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('sy_state_deposit_freq_nbr', syFreqNbr, 'frequency_type'), '');
    Result := ReturnDescription(LocalDepFrequencyType_ComboChoices, FreqType);
  end
  else
  begin
    FreqType := GetFedTaxDepositFreq(CoNbr);
    Result := ReturnDescription(FederalTaxDepositFrequency_ComboChoices, FreqType);
  end;
end;

procedure TevPayrollCalculation.GetLocalTaxDepositPeriod(LocalNbr, FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate: TDateTime);
var
  StateNbr: Integer;
  State: ShortString;
  Q : IevQuery;
  s: String;
begin
  with DM_SYSTEM_LOCAL, DM_SYSTEM_STATE, DM_COMPANY do
  begin
    SY_LOCALS.Activate;
    Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', LocalNbr, []));
    StateNbr := SY_LOCALS['SY_STATES_NBR'];
    State := SY_STATES.Lookup('SY_STATES_NBR', StateNbr, 'STATE');
    if SY_LOCALS['TAX_DEPOSIT_FOLLOW_STATE'] = 'Y' then
    begin
      s :='select CO_NBR, STATE, SY_STATE_DEPOSIT_FREQ_NBR from CO_STATES where ' +
             ' CO_NBR = :CoNbr and {AsOfDate<CO_STATES>}';
      Q := TevQuery.CreateAsOf(s,Date);
      Q.Param['CoNbr'] := CO['CO_NBR'];
      Q.Execute;
      GetStateTaxDepositPeriod(State, ConvertNull(Q.Result.vclDataSet.Lookup('CO_NBR;STATE', VarArrayOf([CO['CO_NBR'], State]),
        'SY_STATE_DEPOSIT_FREQ_NBR'), 0), Date, BeginDate, EndDate, DueDate);
      Exit;
    end;
    SY_LOCAL_DEPOSIT_FREQ.Activate;
    if not SY_LOCAL_DEPOSIT_FREQ.Locate('SY_LOCAL_DEPOSIT_FREQ_NBR', FrequencyNbr, []) then
      raise EInconsistentData.CreateHelp('Frequency is not set for local ' + State + '-' + SY_LOCALS['Name'], IDH_InconsistentData);
    GetTaxDepositPeriodFromDataSet(SY_LOCAL_DEPOSIT_FREQ, Date, SY_LOCALS.FieldByName('SY_LOCAL_TAX_PMT_AGENCY_NBR').AsInteger, BeginDate, EndDate, DueDate, '');
  end;
end;

procedure TevPayrollCalculation.GetLocalTaxDepositPeriodAndSeqNumber(LocalNbr, FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate:
  TDateTime; out SeqNumber: Integer);
var
  StateNbr: Integer;
  State: ShortString;
  Q : IevQuery;
  s: String;
begin
  with DM_SYSTEM_LOCAL, DM_SYSTEM_STATE, DM_COMPANY do
  begin
    SY_LOCALS.Activate;
    Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', LocalNbr, []));

    if SY_LOCALS['TAX_DEPOSIT_FOLLOW_STATE'] = 'Y' then
    begin
      StateNbr := SY_LOCALS['SY_STATES_NBR'];
      State := SY_STATES.Lookup('SY_STATES_NBR', StateNbr, 'STATE');

      s :='select CO_NBR, STATE, SY_STATE_DEPOSIT_FREQ_NBR from CO_STATES where ' +
             ' CO_NBR = :CoNbr and {AsOfDate<CO_STATES>}';
      Q := TevQuery.CreateAsOf(s,Date);
      Q.Param['CoNbr'] := CO['CO_NBR'];
      Q.Execute;
      GetStateTaxDepositPeriod(State, ConvertNull(Q.Result.vclDataSet.Lookup('CO_NBR;STATE', VarArrayOf([CO['CO_NBR'], State]),
        'SY_STATE_DEPOSIT_FREQ_NBR'), 0), Date, BeginDate, EndDate, DueDate);

      SeqNumber := GetSequenceNumber(Date, SY_STATE_DEPOSIT_FREQ.FieldByName('Frequency_Type').AsString[1]);
    end
    else
    begin
     SY_LOCAL_DEPOSIT_FREQ.Activate;
     if not SY_LOCAL_DEPOSIT_FREQ.Locate('SY_LOCAL_DEPOSIT_FREQ_NBR', FrequencyNbr, []) then
       raise EInconsistentData.CreateHelp('Frequency is not set for local ' + State + '-' + SY_LOCALS['Name'], IDH_InconsistentData);
     GetTaxDepositPeriodFromDataSet(SY_LOCAL_DEPOSIT_FREQ, Date, SY_LOCALS.FieldByName('SY_LOCAL_TAX_PMT_AGENCY_NBR').AsInteger, BeginDate, EndDate, DueDate, '');

     SeqNumber := GetSequenceNumber(Date, DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.FieldByName('Frequency_Type').AsString[1]);
    end;
  end;

end;


function TevPayrollCalculation.GetLocalTaxDepositFreq(SyFreqNbr: Integer; CoNbr: Integer): string;
var
  SyLocalsNbr, SyStatesNbr, Sy_DepFreqNbr: Integer;
  FreqType: string;
begin
  DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.Activate;
  DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Activate;
  DM_SYSTEM_STATE.SY_STATES.Activate;

  SyLocalsNbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.lookup('sy_local_deposit_freq_nbr', SyFreqNbr, 'sy_locals_nbr'), 0);

  if DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('Sy_locals_nbr', SyLocalsNbr, 'tax_deposit_follow_state') = 'N' then
  begin
    FreqType := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.Lookup('sy_local_deposit_freq_nbr', SyFreqNbr, 'frequency_type'), '');
    Result := ReturnDescription(LocalDepFrequencyType_ComboChoices, FreqType);
  end
  else
  begin
    SyLocalsNbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.lookup('sy_local_deposit_freq_nbr', SyFreqNbr, 'sy_locals_nbr'), 0);
    SyStatesNbr := ConvertNull(DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('sy_local_nbr', SyLocalsNbr, 'Sy_States_Nbr'), 0);
    if DM_SYSTEM_STATE.SY_STATES.LookUp('sy_states_nbr', SyStatesNbr, 'tax_deposit_follow_federal') = 'N' then
    begin
      Sy_DepFreqNbr := ConvertNull(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('sy_states_nbr', SyStatesNbr, 'Sy_State_Deposit_Freq_Nbr'),
        0);
      FreqType := GetStateTaxDepositFreq(Sy_DepFreqNbr, CoNbr);
      Result := ReturnDescription(LocalDepFrequencyType_ComboChoices, FreqType);
    end
    else
    begin
      FreqType := GetFedTaxDepositFreq(CoNbr);
      Result := ReturnDescription(FederalTaxDepositFrequency_ComboChoices, FreqType);
    end;
  end;
end;

function TevPayrollCalculation.GetSequenceNumber(const Date: TDateTime; const Freq: Char = FREQUENCY_TYPE_QUARTERLY): Integer;
begin
  case Freq of
    FREQUENCY_TYPE_QUARTERLY_NY, FREQUENCY_TYPE_QUARTERLY:
      Result := ((GetMonth(Date) - 1) div 3) + 1;
    FREQUENCY_TYPE_MONTHLY:
      Result := ((GetMonth(Date) - 1) mod 3) + 1;
  else
    Result := 0;
  end;
end;

procedure TevPayrollCalculation.GetStateTaxDepositPeriodAndSeqNumber(State: string; FrequencyNbr: Integer; Date: TDateTime; var BeginDate, EndDate, DueDate:
  TDateTime; out SeqNumber: Integer);
begin
  SeqNumber := 0;
  with DM_SYSTEM_STATE do
  begin
    SY_STATES.Activate;
    {if SY_STATES.Lookup('STATE', State, 'TAX_DEPOSIT_FOLLOW_FEDERAL') = 'Y' then
    begin
      GetFedTaxDepositPeriod(DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString[1], Date, BeginDate, EndDate, DueDate);
      Exit;
    end;}
    SY_STATE_DEPOSIT_FREQ.Activate;
    if not SY_STATE_DEPOSIT_FREQ.Locate('SY_STATE_DEPOSIT_FREQ_NBR', FrequencyNbr, []) then
      raise EInconsistentData.CreateHelp('Frequency is not set for state ' + State, IDH_InconsistentData);
    GetTaxDepositPeriodFromDataSet(SY_STATE_DEPOSIT_FREQ, Date, GetStateTaxPaymenyAgencyNbr(State), BeginDate, EndDate, DueDate, State);
    SeqNumber := GetSequenceNumber(Date, SY_STATE_DEPOSIT_FREQ.FieldByName('Frequency_Type').AsString[1]);
  end;
end;

function TevPayrollCalculation.GetTempCheckLinesFieldValue(FieldName: string): Variant;
begin
  if cdstTempCheckLines['PR_CHECK_LINES_NBR'] = DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value then
    Result := DM_PAYROLL.PR_CHECK_LINES.FieldByName(FieldName).Value
  else
    Result := cdstTempCheckLines.FieldByName(FieldName).Value;
end;

procedure TevPayrollCalculation.AssignTempCheckLines(DS: TevClientDataSet);
begin
  cdstTempCheckLines.CloneCursor(DS, True);
  cdstTempCheckLines.IndexFieldNames := 'PR_CHECK_NBR;LINE_TYPE';
end;

function TevPayrollCalculation.GetCompanyPayFrequencies: String;
var
  T: String;
begin
  Result := '';
  T := DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString;
  if T = '' then
    Exit;
  if T[1] in [CO_FREQ_WEEKLY, CO_FREQ_WEEKLY_BWEEKLY, CO_FREQ_WEEKLY_SMONTHLY, CO_FREQ_WEEKLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
    Result := Result + 'Weekly' + #9 + FREQUENCY_TYPE_WEEKLY + #13;
  if T[1] in [CO_FREQ_BWEEKLY, CO_FREQ_WEEKLY_BWEEKLY, CO_FREQ_BWEEKLY_SMONTHLY, CO_FREQ_BWEEKLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
    Result := Result + 'Bi-weekly' + #9 + FREQUENCY_TYPE_BIWEEKLY + #13;
  if T[1] in [CO_FREQ_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY, CO_FREQ_BWEEKLY_SMONTHLY, CO_FREQ_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
    Result := Result + 'Semi-monthly' + #9 + FREQUENCY_TYPE_SEMI_MONTHLY + #13;
  if T[1] in [CO_FREQ_MONTHLY, CO_FREQ_WEEKLY_MONTHLY, CO_FREQ_BWEEKLY_MONTHLY, CO_FREQ_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_MONTHLY, CO_FREQ_WEEKLY_SMONTHLY_MONTHLY, CO_FREQ_BWEEKLY_SMONTHLY_MONTHLY,
    CO_FREQ_WEEKLY_BWEEKLY_SMONTHLY_MONTHLY] then
    Result := Result + 'Monthly' + #9 + FREQUENCY_TYPE_MONTHLY + #13;
  if T[1] = CO_FREQ_DAILY then
    Result := Result + 'Daily' + #9 + FREQUENCY_TYPE_DAILY + #13;
  if T[1] = CO_FREQ_QUARTERLY then
    Result := Result + 'Quarterly' + #9 + FREQUENCY_TYPE_QUARTERLY + #13;
end;


procedure TevPayrollCalculation.AssignCheckDefaults(TemplateNbr, PaymentSerialNbr: Integer);
var
  b: Boolean;
begin
  b := DM_PAYROLL.PR_CHECK.LookupsEnabled;
  with DM_PAYROLL, DM_COMPANY, DM_EMPLOYEE do
  try
    PR_CHECK.LookupsEnabled := False;
    if TemplateNbr <> 0 then
      CopyPRTemplate(TemplateNbr)
    else
    begin
      PR_CHECK['EXCLUDE_DD'] := 'N';
      PR_CHECK['EXCLUDE_DD_EXCEPT_NET'] := 'N';
      if PR['PAYROLL_TYPE'] = PAYROLL_TYPE_REGULAR then
      begin
          PR_CHECK['EXCLUDE_TIME_OFF_ACCURAL'] := 'N';
      end
      else
        PR_CHECK['EXCLUDE_TIME_OFF_ACCURAL'] := 'Y';
      PR_CHECK['EXCLUDE_AUTO_DISTRIBUTION'] := 'N';
      PR_CHECK['EXCLUDE_ALL_SCHED_E_D_CODES'] := 'N';
      PR_CHECK['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'] := 'N';
      PR_CHECK['EXCLUDE_SCH_E_D_EXCEPT_PENSION'] := 'N';
      PR_CHECK['PRORATE_SCHEDULED_E_DS'] := 'N';
      PR_CHECK['EXCLUDE_FEDERAL'] := 'N';
      PR_CHECK['EXCLUDE_ADDITIONAL_FEDERAL'] := EXCLUDE_TAXES_NONE;
      PR_CHECK['EXCLUDE_EMPLOYEE_OASDI'] := 'N';
      PR_CHECK['EXCLUDE_EMPLOYER_OASDI'] := 'N';
      PR_CHECK['EXCLUDE_EMPLOYEE_MEDICARE'] := 'N';
      PR_CHECK['EXCLUDE_EMPLOYER_MEDICARE'] := 'N';
      PR_CHECK['EXCLUDE_EMPLOYEE_EIC'] := 'N';
      PR_CHECK['EXCLUDE_EMPLOYER_FUI'] := 'N';
      PR_CHECK['TAX_AT_SUPPLEMENTAL_RATE'] := 'N';
      PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
      if EE['EE_NBR'] <> PR_CHECK.FieldByName('EE_NBR').Value then
        PR_CHECK['TAX_FREQUENCY'] := EE.Lookup('EE_NBR', PR_CHECK.FieldByName('EE_NBR').Value, 'PAY_FREQUENCY')
      else
        PR_CHECK['TAX_FREQUENCY'] := EE.FieldByName('PAY_FREQUENCY').Value;
    end;
    PR_CHECK['EXCLUDE_FROM_AGENCY'] := 'N';

    if PaymentSerialNbr <> 0 then
    begin
      PR_CHECK['PAYMENT_SERIAL_NUMBER'] := PaymentSerialNbr;
    end;
  finally
    PR_CHECK.LookupsEnabled := b;
  end;
end;

procedure TevPayrollCalculation.CopyPRTemplate(TemplateNumber: Integer);
var
 Q :IEvQuery;
begin
  SaveDSStates([DM_PAYROLL.PR_CHECK]);
  try
    Q := TEvQuery.Create('SELECT EXCLUDE_DD, EXCLUDE_DD_EXCEPT_NET, EXCLUDE_TIME_OFF_ACCURAL, EXCLUDE_AUTO_DISTRIBUTION, '+
                         'EXCLUDE_ALL_SCHED_E_D_CODES, EXCLUDE_SCH_E_D_FROM_AGCY_CHK, EXCLUDE_SCH_E_D_EXCEPT_PENSION, '+
                         'PRORATE_SCHEDULED_E_DS, EXCLUDE_FEDERAL, EXCLUDE_ADDITIONAL_FEDERAL, EXCLUDE_EMPLOYEE_OASDI, '+
                         'EXCLUDE_EMPLOYER_OASDI, EXCLUDE_EMPLOYEE_MEDICARE, EXCLUDE_EMPLOYER_MEDICARE, EXCLUDE_EMPLOYEE_EIC, '+
                         'EXCLUDE_EMPLOYER_FUI, TAX_AT_SUPPLEMENTAL_RATE, FEDERAL_OVERRIDE_VALUE, FEDERAL_OVERRIDE_TYPE, OVERRIDE_FREQUENCY '+
                         'FROM CO_PR_CHECK_TEMPLATES WHERE CO_PR_CHECK_TEMPLATES_NBR=:NBR and {AsOfNow<CO_PR_CHECK_TEMPLATES>}');
    Q.Param['NBR'] := TemplateNumber;
    if Q.Result.RecordCount>0 then
    begin
      DM_PAYROLL.PR_CHECK['EXCLUDE_DD'] :=  Q.Result['EXCLUDE_DD'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_DD_EXCEPT_NET'] := Q.Result['EXCLUDE_DD_EXCEPT_NET'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_TIME_OFF_ACCURAL'] := Q.Result['EXCLUDE_TIME_OFF_ACCURAL'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_AUTO_DISTRIBUTION'] := Q.Result['EXCLUDE_AUTO_DISTRIBUTION'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_ALL_SCHED_E_D_CODES'] := Q.Result['EXCLUDE_ALL_SCHED_E_D_CODES'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'] := Q.Result['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_SCH_E_D_EXCEPT_PENSION'] := Q.Result['EXCLUDE_SCH_E_D_EXCEPT_PENSION'];
      DM_PAYROLL.PR_CHECK['PRORATE_SCHEDULED_E_DS'] := Q.Result['PRORATE_SCHEDULED_E_DS'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_FEDERAL'] := Q.Result['EXCLUDE_FEDERAL'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_ADDITIONAL_FEDERAL'] := Q.Result['EXCLUDE_ADDITIONAL_FEDERAL'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYEE_OASDI'] := Q.Result['EXCLUDE_EMPLOYEE_OASDI'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYER_OASDI'] := Q.Result['EXCLUDE_EMPLOYER_OASDI'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYEE_MEDICARE'] := Q.Result['EXCLUDE_EMPLOYEE_MEDICARE'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYER_MEDICARE'] := Q.Result['EXCLUDE_EMPLOYER_MEDICARE'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYEE_EIC'] := Q.Result['EXCLUDE_EMPLOYEE_EIC'];
      DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYER_FUI'] := Q.Result['EXCLUDE_EMPLOYER_FUI'];
      DM_PAYROLL.PR_CHECK['TAX_AT_SUPPLEMENTAL_RATE'] := Q.Result['TAX_AT_SUPPLEMENTAL_RATE'];
      DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] := Q.Result['FEDERAL_OVERRIDE_VALUE'];
      DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := Q.Result['FEDERAL_OVERRIDE_TYPE'];
      DM_PAYROLL.PR_CHECK['TAX_FREQUENCY'] := Q.Result['OVERRIDE_FREQUENCY'];
    end;
  finally
    LoadDSStates([DM_PAYROLL.PR_CHECK]);
  end;
end;

destructor TevPayrollCalculation.Destroy;
begin
  FreeAndNil(FGenericGrossToNet);
  FreeAndNil(LinesToAdd);
  FreeAndNil(cdstTempCheckLines);
  FreeAndNil(PriorPayrollEDs);
  FreeAndNil(cdstHomeDBDTFieldValues);
  FreeAndNil(cdstLimitedEDs);
  FreeAndNil(cdstEDCheckDates);
  FreeAndNil(cdstLimitedEEOASDI);
  FreeAndNil(cdstLimitedEROASDI);
  FreeAndNil(cdstLimitedEEMedicare);
  FreeAndNil(cdstLimitedERMedicare);
  FreeAndNil(cdstLimitedFUI);
  inherited;
end;

function TevPayrollCalculation.GetNextPaymentSerialNbr: Integer;
begin
  Result := ctx_DBAccess.GetGeneratorValue('G_NEXT_PAYMENT_SERIAL_NBR', dbtClient, -1);
  Result := -100000000 - Result;
end;

function TevPayrollCalculation.GrossToNet(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction,
  JustPreProcess, FetchPayrollData: Boolean; SecondCheck: Boolean): Boolean; // Result indicates if there were any shortfalls
var
  OldCheckValue, OldCheckLineValue: Boolean;
begin
  OldCheckValue := DM_PAYROLL.PR_CHECK.LookupsEnabled;
  OldCheckLineValue := DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled;
  with DM_PAYROLL do
  try
    SaveDSStates([PR, PR_BATCH, PR_CHECK]);
    PR_CHECK.LookupsEnabled := False;
    PR_CHECK_LINES.LookupsEnabled := False;
    PR_CHECK_LINES.DisableControls;
    PR_CHECK_LINE_LOCALS.DisableControls;
    PR_CHECK_STATES.DisableControls;
    PR_CHECK_SUI.DisableControls;
    PR_CHECK_LOCALS.DisableControls;
    Result := FGenericGrossToNet.Calculate(Warnings, CheckNumber, ApplyUpdatesInTransaction, FetchPayrollData, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
      PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, ctx_DataAccess.PayrollIsProcessing, JustPreProcess, False, False, False, SecondCheck);
  finally
    LoadDSStates([PR, PR_BATCH, PR_CHECK]);
    PR_CHECK_LINES.EnableControls;
    PR_CHECK_LINE_LOCALS.EnableControls;
    PR_CHECK_STATES.EnableControls;
    PR_CHECK_SUI.EnableControls;
    PR_CHECK_LOCALS.EnableControls;
    PR_CHECK.LookupsEnabled := OldCheckValue;
    PR_CHECK_LINES.LookupsEnabled := OldCheckLineValue;
  end;
end;

procedure TevPayrollCalculation.GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES,
  PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS: TevClientDataSet;
  DisableYTDs, DisableShortfalls, NetToGross: Boolean);
var
  Warnings: String;
begin
  FGenericGrossToNet.Calculate(Warnings, PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger, False, False, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
    PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, False, True, True, DisableYTDs, DisableShortfalls, False, netToGross);
end;

procedure TevPayrollCalculation.EraseLimitedEDs;
begin
  if Assigned(cdstLimitedEDs) then
  begin
    cdstLimitedEDs.Free;
    cdstLimitedEDs := Nil;
  end;
  if Assigned(cdstEDCheckDates) then
  begin
    cdstEDCheckDates.Free;
    cdstEDCheckDates := Nil;
  end;
end;

procedure TevPayrollCalculation.GetLimitedEDs(CoNbr, PayrollNbr: Integer; BeginDate, EndDate: TDateTime);
begin
  if Assigned(cdstLimitedEDs) and Assigned(cdstEDCheckDates) and (LastClientNbrForLimitedEDs = ctx_DataAccess.ClientID) and (LastPayrollNbrForLimitedEDs = PayrollNbr) then
    Exit;

  if not Assigned(cdstLimitedEDs) then
    cdstLimitedEDs := TevClientDataSet.Create(Nil);
  with TExecDSWrapper.Create('GlobalDeductionTemplate') do
  begin
    SetParam('CoNbr', CoNbr);
    SetParam('PayrollNbr', PayrollNbr);
    SetParam('BeginDate', BeginDate);
    SetParam('EndDate', EndDate);
    SetMacro('Status', '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''');
    ctx_DataAccess.GetCustomData(cdstLimitedEDs, AsVariant);
  end;
  if not Assigned(cdstEDCheckDates) then
    cdstEDCheckDates := TevClientDataSet.Create(Nil);
  with TExecDSWrapper.Create('AllEmployeeLastEDCheckDates') do
  begin
    SetParam('CoNbr', CoNbr);
    SetParam('PRNbr', PayrollNbr);
    ctx_DataAccess.GetCustomData(cdstEDCheckDates, AsVariant);
  end;
  LastClientNbrForLimitedEDs := ctx_DataAccess.ClientID;
  LastPayrollNbrForLimitedEDs := PayrollNbr;
end;

procedure TevPayrollCalculation.EraseLimitedTaxes;
begin
  if Assigned(cdstLimitedEEOASDI) then
  begin
    cdstLimitedEEOASDI.Free;
    cdstLimitedEEOASDI := Nil;
  end;
  if Assigned(cdstLimitedEROASDI) then
  begin
    cdstLimitedEROASDI.Free;
    cdstLimitedEROASDI := Nil;
  end;
  if Assigned(cdstLimitedEEMedicare) then
  begin
    cdstLimitedEEMedicare.Free;
    cdstLimitedEEMedicare := Nil;
  end;
  if Assigned(cdstLimitedERMedicare) then
  begin
    cdstLimitedERMedicare.Free;
    cdstLimitedERMedicare := Nil;
  end;
  if Assigned(cdstLimitedFUI) then
  begin
    cdstLimitedFUI.Free;
    cdstLimitedFUI := Nil;
  end;
end;

procedure TevPayrollCalculation.GetLimitedTaxes(CoNbr, PayrollNbr: Integer; BeginDate, EndDate: TDateTime);
var
  Q: IevQuery;
const
  GlobalFedTemplate = 'select E.CL_PERSON_NBR, sum(#F1) TAX_WAGE, sum(#F2) TAX, sum(#F3) TAX_TIP ' +
      'from PR_CHECK C ' +
      'join PR P on C.PR_NBR=P.PR_NBR and '+
      '{AsOfNow<P>} and P.PR_NBR<>:PayrollNbr ' +
      'and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.STATUS in (#STATUS) ' +
      'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} ' +
      'join CO CO on P.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and ' +
      '(CO.CL_COMMON_PAYMASTER_NBR=(select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
      'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
      'where CO.CO_NBR=:CONbr and M.COMBINE_#COMBINETAX=''Y'' and {AsOfNow<CO>} and {AsOfNow<M>}' +
      ') or CO.CO_NBR=:CONbr) ' +
      'where {AsOfNow<C>} ' +
      'GROUP BY E.CL_PERSON_NBR';

begin
  Q := TevQuery.Create(GlobalFedTemplate);
  Q.Macro['F3'] := '0';
  Q.Param['CoNbr'] := CoNbr;
  Q.Param['PayrollNbr'] := PayrollNbr;
  Q.Param['BeginDate'] := BeginDate;
  Q.Param['EndDate'] := EndDate;
  Q.Macro['Status'] := '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''';
  Q.Macro['F1'] := 'EE_OASDI_TAXABLE_WAGES+EE_OASDI_TAXABLE_TIPS';
  Q.Macro['F2'] := 'EE_OASDI_TAX';
  Q.Macro['COMBINETAX'] := 'OASDI';
  if not Assigned(cdstLimitedEEOASDI) then
    cdstLimitedEEOASDI := TevClientDataSet.Create(nil);
  cdstLimitedEEOASDI.Data := Q.Result.vclDataset.Data;

  Q := TevQuery.Create(GlobalFedTemplate);
  Q.Macro['F3'] := '0';
  Q.Param['CoNbr'] := CoNbr;
  Q.Param['PayrollNbr'] := PayrollNbr;
  Q.Param['BeginDate'] := BeginDate;
  Q.Param['EndDate'] := EndDate;
  Q.Macro['Status'] := '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''';
  Q.Macro['F1'] := 'ER_OASDI_TAXABLE_WAGES+ER_OASDI_TAXABLE_TIPS';
  Q.Macro['F2'] := 'ER_OASDI_TAX';
  Q.Macro['COMBINETAX'] := 'OASDI';
  if not Assigned(cdstLimitedEROASDI) then
    cdstLimitedEROASDI := TevClientDataSet.Create(nil);
  cdstLimitedEROASDI.Data := Q.Result.vclDataset.Data;

  Q := TevQuery.Create(GlobalFedTemplate);
  Q.Macro['F3'] := '0';
  Q.Param['CoNbr'] := CoNbr;
  Q.Param['PayrollNbr'] := PayrollNbr;
  Q.Param['BeginDate'] := BeginDate;
  Q.Param['EndDate'] := EndDate;
  Q.Macro['Status'] := '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''';
  Q.Macro['F1'] := 'EE_MEDICARE_TAXABLE_WAGES';
  Q.Macro['F2'] := 'EE_MEDICARE_TAX';
  Q.Macro['COMBINETAX'] := 'MEDICARE';
  if not Assigned(cdstLimitedEEMedicare) then
    cdstLimitedEEMedicare := TevClientDataSet.Create(nil);
  cdstLimitedEEMedicare.Data := Q.Result.vclDataset.Data;

  Q := TevQuery.Create(GlobalFedTemplate);
  Q.Macro['F3'] := '0';
  Q.Param['CoNbr'] := CoNbr;
  Q.Param['PayrollNbr'] := PayrollNbr;
  Q.Param['BeginDate'] := BeginDate;
  Q.Param['EndDate'] := EndDate;
  Q.Macro['Status'] := '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''';
  Q.Macro['F1'] := 'ER_MEDICARE_TAXABLE_WAGES';
  Q.Macro['F2'] := 'ER_MEDICARE_TAX';
  Q.Macro['COMBINETAX'] := 'MEDICARE';
  if not Assigned(cdstLimitedERMedicare) then
    cdstLimitedERMedicare := TevClientDataSet.Create(nil);
  cdstLimitedERMedicare.Data := Q.Result.vclDataset.Data;

  Q := TevQuery.Create(GlobalFedTemplate);
  Q.Macro['F3'] := '0';
  Q.Param['CoNbr'] := CoNbr;
  Q.Param['PayrollNbr'] := PayrollNbr;
  Q.Param['BeginDate'] := BeginDate;
  Q.Param['EndDate'] := EndDate;
  Q.Macro['Status'] := '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''';
  Q.Macro['F1'] := 'ER_FUI_TAXABLE_WAGES';
  Q.Macro['F2'] := 'ER_FUI_TAX';
  Q.Macro['COMBINETAX'] := 'FUI';
  if not Assigned(cdstLimitedFUI) then
    cdstLimitedFUI := TevClientDataSet.Create(nil);
  cdstLimitedFUI.Data := Q.Result.vclDataset.Data;
end;

procedure TevPayrollCalculation.OpenDetailPayroll(PrNbr: Integer);
begin
  if GetEeFilter <> '' then
    with DM_PAYROLL do
    begin
      PR_BATCH.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'EE_NBR', DM_EMPLOYEE.EE, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK_LINES.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'PR_CHECK_NBR', PR_CHECK);
      PR_CHECK_LINE_LOCALS.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'PR_CHECK_LINES_NBR', PR_CHECK_LINES);
      PR_CHECK_LOCALS.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'PR_CHECK_NBR', PR_CHECK);
      PR_CHECK_STATES.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'PR_CHECK_NBR', PR_CHECK);
      PR_CHECK_SUI.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'PR_CHECK_NBR', PR_CHECK);
      PR_SCHEDULED_E_DS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));

      PR_BATCH.Filter := '';
      PR_CHECK.Filter := '';
      PR_CHECK_LINES.Filter := '';
      PR_CHECK_LINE_LOCALS.Filter := '';
      PR_CHECK_LOCALS.Filter := '';
      PR_CHECK_STATES.Filter := '';
      PR_CHECK_SUI.Filter := '';
      
      PR_CHECK_LINES.LookupsEnabled := False;
      PR_CHECK_LINES.Tag := 1;
      PR_CHECK_LOCALS.LookupsEnabled := False;
      PR_CHECK_STATES.LookupsEnabled := False;
      PR_CHECK_SUI.LookupsEnabled := False;
      ctx_DataAccess.OpenDataSets([PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LOCALS, PR_CHECK_STATES, PR_CHECK_SUI,
                    PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS]);
      PR_CHECK_LINES.LookupsEnabled := True;
      PR_CHECK_LINES.Tag := 0;
      PR_CHECK_LOCALS.LookupsEnabled := True;
      PR_CHECK_STATES.LookupsEnabled := True;
      PR_CHECK_SUI.LookupsEnabled := True;
    end
  else
    with DM_PAYROLL do
    begin
      PR_BATCH.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK_LINES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK_LINE_LOCALS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
      PR_CHECK_LOCALS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_CHECK_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));
      PR_SCHEDULED_E_DS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR = ' + IntToStr(PrNbr));

      PR_BATCH.Filter := '';
      PR_CHECK.Filter := '';
      PR_CHECK_LINES.Filter := '';
      PR_CHECK_LINE_LOCALS.Filter := '';
      PR_CHECK_LOCALS.Filter := '';
      PR_CHECK_STATES.Filter := '';
      PR_CHECK_SUI.Filter := '';

      PR_CHECK_LINES.LookupsEnabled := False;
      PR_CHECK_LINES.Tag := 1;
      PR_CHECK_LOCALS.LookupsEnabled := False;
      PR_CHECK_STATES.LookupsEnabled := False;
      PR_CHECK_SUI.LookupsEnabled := False;
      if not PR_CHECK.Active then
      begin
        PR_CHECK.PrepareLookups;
        ctx_DataAccess.OpenEEDataSetForCompany(PR_CHECK, PR['CO_NBR'], 'PR_NBR = ' + IntToStr(PrNbr));
        PR_CHECK.UnPrepareLookups;
      end;
      ctx_DataAccess.OpenDataSets([PR_BATCH, PR_CHECK_LINES, PR_CHECK_LOCALS, PR_CHECK_STATES, PR_CHECK_SUI,
                    PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS]);
      PR_CHECK_LINES.LookupsEnabled := True;
      PR_CHECK_LINES.Tag := 0;
      PR_CHECK_LOCALS.LookupsEnabled := True;
      PR_CHECK_STATES.LookupsEnabled := True;
      PR_CHECK_SUI.LookupsEnabled := True;
    end;
end;

procedure TevPayrollCalculation.RefreshScheduledEDsForBatch(const BatchNbr: Integer; const NoManuals: Boolean = False);
var
  MyCount, NbrOfCheckLines: Integer;
  iGenNbr: Integer;
begin
  MyCount := 0;
  NbrOfCheckLines := 1;
  ctx_StartWait('Processing...');
  ctx_DataAccess.Validate := False;
  DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
  DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;
  DM_EMPLOYEE.EE.LookupsEnabled := False;
  DM_EMPLOYEE.EE_RATES.LookupsEnabled := False;
  with DM_PAYROLL do
  try
    DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger);

    if not NoManuals then
    begin
      PR_CHECK.First;
      while not PR_CHECK.EOF do
      begin
        if PR_CHECK['CHECK_TYPE'] = CHECK_TYPE2_MANUAL then
          VerifyManualCheckChange(PR_CHECK.FieldByName('PR_NBR').AsString, PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        PR_CHECK.Next;
      end;
    end;

    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
    begin
      SetMacro('Columns', 'T1.*');
      SetMacro('TABLE1', PR_CHECK_LINES.Name);
      SetMacro('TABLE2', 'PR_CHECK');
      SetMacro('NBRFIELD', 'PR_BATCH');
      SetMacro('JOINFIELD', 'PR_CHECK');
      SetParam('RecordNbr', BatchNbr);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    PR_CHECK_LINES.ClientID := ctx_DataAccess.ClientID;
    PR_CHECK_LINES.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
    PR_CHECK_LINES.SetOpenCondition('PR_BATCH_NBR = ' + IntToStr(BatchNbr));

    PR_CHECK_LINES.First;
    while not PR_CHECK_LINES.EOF do
    begin
      if (PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_SCHEDULED)
      or (PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_DISTR_SCHEDULED) then
      begin
        PR_CHECK.Locate('PR_CHECK_NBR', PR_CHECK_LINES['PR_CHECK_NBR'], []);
        if PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_MANUAL] then
        begin
          PR_CHECK_LINES.Delete;
          Inc(NbrOfCheckLines);
        end
        else
          PR_CHECK_LINES.Next;
      end
      else
        PR_CHECK_LINES.Next;
    end;

    iGenNbr := ctx_DBAccess.GetGeneratorValue('G_PR_CHECK_LINES', dbtClient, NbrOfCheckLines);
    ctx_DataAccess.NextInternalCheckLineNbr := iGenNbr - NbrOfCheckLines + 1;
    ctx_DataAccess.LastInternalCheckLineNbr := iGenNbr;

    GetLimitedEDs(PR.FieldByName('CO_NBR').AsInteger, PR.FieldByName('PR_NBR').AsInteger,
      GetBeginYear(PR.FieldByName('CHECK_DATE').AsDateTime), PR.FieldByName('CHECK_DATE').AsDateTime);
    ctx_DataAccess.NewBatchInserted := True;

    try
      PR_CHECK.First;
      while not PR_CHECK.EOF do
      begin
        if (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR) or
        not NoManuals and (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) then
        begin
          Inc(MyCount);
          ctx_UpdateWait('Refreshing check for EE #' + Trim(ConvertNull(PR_CHECK.FieldByName('EE_Custom_Number').AsString, '')) +
            '  (' + IntToStr(MyCount) + ' of ' + IntToStr(PR_CHECK.RecordCount) + ')...');
          Generic_CreatePRCheckDetails(
            PR,
            PR_BATCH,
            PR_CHECK,
            PR_CHECK_LINES,
            PR_CHECK_STATES,
            PR_CHECK_SUI,
            PR_CHECK_LOCALS,
            PR_CHECK_LINE_LOCALS,
            PR_SCHEDULED_E_DS,
            DM_CLIENT.CL_E_DS,
            DM_CLIENT.CL_PERSON,
            DM_CLIENT.CL_PENSION,
            DM_COMPANY.CO,
            DM_COMPANY.CO_E_D_CODES,
            DM_COMPANY.CO_STATES,
            DM_EMPLOYEE.EE,
            DM_EMPLOYEE.EE_SCHEDULED_E_DS,
            DM_EMPLOYEE.EE_STATES,
            DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
            DM_SYSTEM_STATE.SY_STATES,
            False,
            (PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y'),
            (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
            True
          );
        end;
        PR_CHECK.Next;
      end;
    finally
      ctx_DataAccess.NewBatchInserted := False;
      EraseLimitedEDs;
    end;
    ctx_UpdateWait('Saving...');
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINES]);
  finally
    ctx_DataAccess.Validate := True;
    DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
    DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
    DM_EMPLOYEE.EE.LookupsEnabled := True;
    DM_EMPLOYEE.EE_RATES.LookupsEnabled := True;
    ctx_EndWait;
  end;
end;

function TevPayrollCalculation.EarnCodeGroupYTDCalc(GroupNbr: Variant; CalcAmount: Boolean): Double;
var
  YTDHours, YTD: Real;
  EDCodeType: String;
  Q: IevQuery;
begin
  Result := 0;
  if VarIsNull(GroupNbr) then
    Exit;
  DM_CLIENT.CL_E_D_GROUPS.Activate;
  DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
  DM_CLIENT.CL_E_DS.Activate;
  DM_CLIENT.PR_CHECK_LINES.Activate;

  ApplyTempCheckLinesCheckRange(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);

  Q := TevQuery.Create(
  '  select distinct x.CL_E_DS_NBR'+
  '  from PR_CHECK_LINES x'+
  '  join PR_CHECK c on c.PR_CHECK_NBR=x.PR_CHECK_NBR'+
  '  join PR p on p.PR_NBR=c.PR_NBR'+
  '  where {AsOfNow<x>} and {AsOfNow<c>} and {AsOfNow<p>} and '+
  '  c.EE_NBR=:EE_NBR and p.CHECK_DATE>=:BEGIN_DATE and p.CHECK_DATE<=:END_DATE');
  Q.Param['EE_NBR'] :=  DM_CLIENT.PR_CHECK['EE_NBR'];
  Q.Param['BEGIN_DATE'] := GetBeginYear(ConvertNull(DM_PAYROLL.PR['CHECK_DATE'], SysDate));
  Q.Param['END_DATE'] := GetEndYear(ConvertNull(DM_PAYROLL.PR['CHECK_DATE'], SysDate));

  Q.Result.First;
  while not Q.Result.EOF do
  begin
    if DM_CLIENT.CL_E_D_GROUP_CODES.Locate('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([GroupNbr, Q.Result['CL_E_DS_NBR']]), []) then
    begin
      GetLimitedTaxAndDedYTDforGTN(DM_CLIENT.PR_CHECK['PR_CHECK_NBR'], Q.Result['CL_E_DS_NBR'], ltDeduction, DM_CLIENT.PR, DM_CLIENT.PR_CHECK, nil, nil, DM_CLIENT.PR_CHECK_LINES, YTDHours, YTD);
      if DM_CLIENT.CL_E_D_GROUPS.NewLookup('CL_E_D_GROUPS_NBR', GroupNbr, 'SUBTRACT_DEDUCTIONS') = GROUP_BOX_NO then
      begin
        if CalcAmount then
          Result := Result + YTD
        else
          Result := Result + YTDHours;
      end
      else
      begin
        EDCodeType := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', Q.Result['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
        if CalcAmount then
          Result := Result + ConvertDeduction(EDCodeType, YTD)
        else
          Result := Result + ConvertDeduction(EDCodeType, YTDHours);
      end;
    end;
    Q.Result.Next;
  end;
end;

function TevPayrollCalculation.CalcWorkersComp(PrNbr: Integer; sLevel: string = 'CO_NBR'; dds: TevClientDataSet = nil; const wcompNbr: integer = 0; FromMainProcessing:Boolean = False): Currency;
var
  rds: TevClientDataSet;
  Rates, EEList, FlList: IevDataset;
  RegRate: real;
  WcNbr, EeNbr, Nbr, Origin_EeNbr, GrNbr : Integer;
  sgw, sag, CurrentGW, op, m : real;
  bSubstrDed: Boolean;
  fAmount,fAmountOP, OTMinWage: real;
  BeginDate, EndDate, FiscalBaseDate : TDateTime;
  qCO_WORKERS_COMP, qWcEds: IevQuery;

  Limit, GrossWages, GrossWagesSum, LimitBalance: Real;
  NoLimit: Boolean;
  LastRec: Integer;

  CheckDate: TDateTime;
  RunNumber: Integer;  

  function IsFluctuating: boolean ;
  var b:boolean;
  begin
     b := FlList.Locate('pr_check_nbr',EEList['PR_CHECK_NBR'],[]);
     if b then
       result := true
     else
       result := false;
  end;

  function GetRateOfPay: real;
  var
    Q: IevQuery;
  begin
    if  EEList.FieldByName('rate_of_pay').AsFloat <> 0 then
       result := EEList.FieldByName('rate_of_pay').AsFloat
    else if EEList.FieldByName('rate_number').AsFloat <> 0 then
    begin
      Q := TevQuery.Create('select RATE_AMOUNT from EE_RATES where {AsOfDate<EE_RATES>} and EE_NBR='+IntToStr(EEList['ee_nbr'])+' and rate_number='+EEList.FieldByName('rate_number').AsString);
      Q.Param['StatusDate'] := CheckDate;
      if Q.Result.RecordCount > 0 then
        Result := Q.Result['RATE_AMOUNT']
      else
        raise EevException.Create('Unable to calc rate_of_pay');;
    end
    else
    begin
      Q := TevQuery.Create('select RATE_AMOUNT from EE_RATES where {AsOfDate<EE_RATES>} and EE_NBR='+IntToStr(EEList['ee_nbr'])+' and primary_rate=''Y''');
      Q.Param['StatusDate'] := CheckDate;
      if Q.Result.RecordCount > 0 then
        Result := Q.Result['RATE_AMOUNT']
      else
        raise EevException.Create('Unable to calc rate_of_pay');
    end
  end;

  function GetCOFiscalBeginDate(aCONBR : integer) : TDateTime;
  var qry : IevQuery;
  begin
    Result := 0;
    if aCONBR > 0 then
    begin
      qry := TevQuery.Create('SELECT WC_FISCAL_YEAR_BEGIN FROM CO a WHERE {AsOfNow<a>} AND a.CO_NBR=:CONBR');
      qry.Params.AddValue('CONBR',aCONBR);
      qry.Execute;
      Result := ConvertNull(qry.Result['WC_FISCAL_YEAR_BEGIN'],0);
    end;
  end;

  procedure GetYearPeriod (var aFiscalBeginDate, aFiscalEndDate : TDatetime);
  var
    CheckYear, CheckMonth, CheckDay,
    FiscalYear, FiscalMonth, FiscalDay : word;
    tmpDate : TDateTime;
  begin
    if FiscalBaseDate > 0 then //Fiscal Year
    begin
      decodeDate (aFiscalEndDate, CheckYear, CheckMonth, CheckDay);
      decodeDate (FiscalBaseDate, FiscalYear, FiscalMonth, FiscalDay);
      tmpDate := encodedate (CheckYear, FiscalMonth, FiscalDay);
      if tmpDate <= aFiscalEndDate then
      begin
        aFiscalBeginDate := tmpDate;
        aFiscalEndDate   := tmpDate + 365;
      end
      else begin
        aFiscalBeginDate := encodedate (CheckYear-1, FiscalMonth, FiscalDay);
        aFiscalEndDate   := aFiscalBeginDate + 365;
      end;
    end
    else // not Fiscal Year
      aFiscalBeginDate := GetBeginYear(aFiscalEndDate);
  end;

  function GetWcAssignedClEdsNbrs(aWcNbr: integer): string;
  begin
    Result := '-1';

    qWcEds := TevQuery.Create(
      'SELECT DISTINCT t9.Cl_E_Ds_Nbr ' +
      'FROM Co_Workers_Comp t6, Cl_E_D_Groups t7, Cl_E_D_Group_Codes t8, Cl_E_Ds t9 ' +
      'WHERE {AsOfNow<t6>} and {AsOfNow<t7>} and {AsOfNow<t8>} and {AsOfNow<t9>} and ' +
      ' t7.Cl_E_D_Groups_Nbr = t6.Cl_E_D_Groups_Nbr  AND ' +
      ' t7.Cl_E_D_Groups_Nbr = t8.Cl_E_D_Groups_Nbr  AND ' +
      ' t9.Cl_E_Ds_Nbr = t8.Cl_E_Ds_Nbr and ' +
      ' t6.Co_Workers_Comp_Nbr =' + IntToStr(aWcNbr));

    qWcEds.Result.IndexFieldNames := 'CL_E_DS_NBR';
    qWcEds.Result.First;
    while not qWcEds.Result.Eof do
    begin
      Result := Result + ',' + qWcEds.Result.FieldByName('Cl_E_Ds_Nbr').AsString;
      qWcEds.Result.Next;
    end;

  end;

var
  Q: IevQuery;
  Employees, Checks: IevDataset;
begin
  Result := 0;

  Q := TevQuery.Create('select CO_NBR, CHECK_DATE, RUN_NUMBER from PR where {AsOfNow<PR>} and PR_NBR='+IntToStr(PrNbr));

  CheckDate := Q.Result['CHECK_DATE'];
  RunNumber := Q.Result['RUN_NUMBER'];

  FiscalBaseDate := GetCOFiscalBeginDate(Q.Result['CO_NBR']);

  if Assigned(dds) then
  begin
    dds.Close;
    dds.FieldDefs.Clear;
    dds.FieldDefs.Add(sLevel, ftInteger);
    dds.FieldDefs.Add('Amount', ftCurrency);
    dds.CreateDataSet;
    dds.IndexFieldNames := sLevel;
  end;

  Origin_EeNbr := 0;

  Q := TevQuery.Create('SELECT DISTINCT t2.ee_nbr, t4.WORKERS_COMP_WAGE_LIMIT, t4.WC_WAGE_LIMIT_FREQUENCY ' +
              'FROM PR_CHECK t2, Ee t4 ' +
              'WHERE t2.PR_NBR = :PrNbr AND t4.EE_NBR = t2.ee_nbr AND {AsOfNow<t2>} AND {AsOfNow<t4>}');
  Q.Param['PrNbr'] := PrNbr;

  Employees := Q.Result;

  Employees.First;

  while not Employees.Eof do
  begin

    Q := TevQuery.Create('SELECT t2.pr_check_nbr, SUM(IIF(t5.E_D_Code_Type like ''E%'', t3.Amount, -t3.Amount)) GROSS_WAGES ' +
            'FROM PR_CHECK t2, PR_CHECK_LINES t3, Ee t4, Cl_E_DS t5 ' +
            'WHERE t2.PR_CHECK_NBR = t3.PR_CHECK_NBR AND t5.CL_E_DS_NBR = t3.CL_E_DS_NBR AND ' +
                  't2.PR_NBR = :PrNbr AND t4.EE_NBR = t2.ee_nbr  AND ' +
                  't3.PR_NBR=:PrNbr and {AsOfNow<t4>} AND t2.ee_nbr=:EeNbr AND '+
                  '{AsOfNow<t2>} AND {AsOfNow<t3>} AND {AsOfNow<t5>} '+
                  'GROUP BY t2.pr_check_nbr ORDER BY t2.pr_check_nbr');

    Q.Param['PrNbr'] := PrNbr;
    Q.Param['EeNbr'] := Employees['EE_NBR'];
    Checks := Q.Result;

    Checks.IndexFieldNames := 'GROSS_WAGES';

    Checks.First;

    while not Checks.Eof do
    begin

      rds := TevClientDataSet.Create(nil);

      try
        rds.SkipFieldCheck := True;

        Q := TevQuery.Create('SELECT t2.ee_nbr, t4.co_nbr, t3.co_division_nbr, t3.co_branch_nbr, t3.co_department_nbr, t3.co_team_nbr, ' +
                       't3.AMOUNT,t3.rate_of_pay,t3.HOURS_OR_PIECES,t3.PR_CHECK_NBR, t4.WORKERS_COMP_WAGE_LIMIT, t4.WC_WAGE_LIMIT_FREQUENCY, ' +
                       't3.CO_WORKERS_COMP_NBR, t3.RATE_NUMBER, t3.CL_E_DS_NBR, t5.E_D_CODE_TYPE,t5.Overstate_Hours_For_Overtime reduce,  t5.REGULAR_OT_RATE ' +
                'FROM PR_CHECK t2, PR_CHECK_LINES t3, ' +
                     'Ee t4, Cl_E_DS t5 ' +
                'WHERE t2.PR_CHECK_NBR = t3.PR_CHECK_NBR AND t5.CL_E_DS_NBR = t3.CL_E_DS_NBR AND ' +
                      't2.PR_NBR = :PrNbr AND t4.EE_NBR = t2.ee_nbr  AND ' +
                      't3.PR_NBR=:PrNbr and {AsOfNow<t4>} AND t2.ee_nbr=:EeNbr AND t2.pr_check_nbr=:CheckNbr AND '+
                      '{AsOfNow<t2>} AND {AsOfNow<t3>} AND {AsOfNow<t5>}');

        Q.Param['PrNbr'] := PrNbr;
        Q.Param['EeNbr'] := Employees['EE_NBR'];
        Q.Param['CheckNbr'] := Checks['PR_CHECK_NBR'];

        EEList := Q.Result;

        Q := TevQuery.Create('SELECT DISTINCT 1 fluctuating, '+
                               't3.PR_CHECK_NBR ' +
                        'FROM PR_CHECK_LINES t3, Cl_E_DS t5, PR_CHECK t2 ' +
                        'WHERE t5.CL_E_DS_NBR = t3.CL_E_DS_NBR AND t3.PR_NBR = :PrNbr AND t2.PR_CHECK_NBR=t3.PR_CHECK_NBR and t2.EE_NBR=:EeNbr AND t2.pr_check_nbr=:CheckNbr AND ' +
                        't3.AMOUNT > 0 AND t5.E_D_CODE_TYPE = '''+ED_OEARN_SALARY+''' AND {AsOfNow<t3>} AND {AsOfNow<t5>} AND {AsOfNow<t2>}');

        Q.Param['PrNbr'] := PrNbr;
        Q.Param['EeNbr'] := Employees['EE_NBR'];
        Q.Param['CheckNbr'] := Checks['PR_CHECK_NBR'];

        FlList := Q.Result;

        Q := TevQuery.Create('SELECT t1.RATE_NUMBER,t1.RATE_AMOUNT, t1.EE_NBR, t1.PRIMARY_RATE, t1.CO_WORKERS_COMP_NBR, t3.CL_E_DS_NBR ' +
                'FROM Ee_Rates t1, Co_Workers_Comp t2, Cl_E_D_Group_Codes t3 ' +
                'WHERE t2.CO_WORKERS_COMP_NBR = t1.CO_WORKERS_COMP_NBR AND t2.CL_E_D_GROUPS_NBR = t3.CL_E_D_GROUPS_NBR AND ' +
                '{AsOfNow<t1>} AND ' +
                '{AsOfNow<t2>} AND ' +
                '{AsOfNow<t3>} AND t1.EE_NBR=:EeNbr ');

        Q.Param['EeNbr'] := Employees['EE_NBR'];
        Rates := Q.Result;

        rds.FieldDefs.Add('EE_NBR', ftInteger);
        rds.FieldDefs.Add('wc_nbr', ftInteger);
        rds.FieldDefs.Add('gw', ftCurrency);
        rds.FieldDefs.Add('op', ftCurrency);
        rds.FieldDefs.Add('limit', ftCurrency);
        rds.FieldDefs.Add('GrossWagesByWCFrequency', ftCurrency);
        rds.FieldDefs.Add('wcrate', ftFloat);
        rds.FieldDefs.Add('er', ftFloat);
        rds.FieldDefs.Add('sp', ftString, 1);
        rds.FieldDefs.Add('CO_NBR', ftInteger);
        rds.FieldDefs.Add('CO_TEAM_NBR', ftInteger);
        rds.FieldDefs.Add('CO_DEPARTMENT_NBR', ftInteger);
        rds.FieldDefs.Add('CO_BRANCH_NBR', ftInteger);
        rds.FieldDefs.Add('CO_DIVISION_NBR', ftInteger);
        rds.CreateDataSet;

        while not EEList.Eof do
        begin
          EndDate := CheckDate;

          if ( Length(Trim(Employees.FieldByName('WC_WAGE_LIMIT_FREQUENCY').AsString)) > 0 ) then
          begin
            case Employees.FieldByName('WC_WAGE_LIMIT_FREQUENCY').AsString[1] of
            WC_LIMIT_FREQ_EVERYPAY :  BeginDate := CheckDate;
            WC_LIMIT_FREQ_MONTHLY  :  BeginDate := GetBeginMonth(CheckDate);
            WC_LIMIT_FREQ_QUARTERLY : BeginDate := GetBeginQuarter(CheckDate);
            WC_LIMIT_FREQ_SEMI_ANNUALLY :
            begin
              if ( GetMonth(CheckDate) <= 6 )
              then BeginDate := EncodeDate(GetYear(CheckDate), 1, 1)
              else BeginDate := EncodeDate(GetYear(CheckDate), 7, 1);
            end
            else //Default- Annual
              GetYearPeriod(BeginDate, EndDate);
            end;
          end
          else
            GetYearPeriod(BeginDate, EndDate);

          if not EEList.FieldByName('CO_WORKERS_COMP_NBR').IsNull then
            WcNbr := EEList['CO_WORKERS_COMP_NBR']
          else if Rates.Locate('ee_nbr;rate_number;cl_e_ds_nbr', EEList['ee_nbr;rate_number;cl_e_ds_nbr'], []) then
            WcNbr := Rates['CO_WORKERS_COMP_NBR']
          else if Rates.Locate('ee_nbr;primary_rate;cl_e_ds_nbr', VarArrayOf([EEList['ee_nbr'], GROUP_BOX_YES, EEList['cl_e_ds_nbr']]), []) then
            WcNbr := Rates['CO_WORKERS_COMP_NBR']
          else
            WcNbr := -1;

          //To sum grosswages based on EE WC limit Frequency
          //always return one record or empty

          if not FromMainProcessing then
          begin
            Q := TevQuery.Create('SELECT SUM(IIF(t4.E_D_Code_Type like ''E%'', t3.Amount, -t3.Amount)) GROSS_WAGES ' +
                  'FROM Pr_Check  t1, Pr  t2, Cl_E_Ds t4, Pr_Check_Lines t3  ' +
                  'WHERE t2.PR_NBR = t1.PR_NBR AND ' +
                        't1.Pr_Check_Nbr = t3.Pr_Check_Nbr AND ' +
                        't4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr AND ' +
                        't1.EE_NBR = :EE_NBR AND '+
                        '{AsOfNow<t1>} and ' +
                        '{AsOfNow<t2>} and ' +
                        '{AsOfNow<t3>} and ' +
                        '{AsOfNow<t4>} and ' +
                        't2.CHECK_DATE >= :BeginDate AND ((t2.CHECK_DATE < :CheckDate OR (t2.CHECK_DATE = :CheckDate AND t2.RUN_NUMBER < :RunNumber))) AND ' +
                        '(t4.E_D_Code_Type LIKE ''E%'' OR t3.Cl_E_Ds_Nbr IN (#WcEds) AND t3.Co_Workers_Comp_Nbr = :WcNbr) ');
            Q.Param['BeginDate'] := BeginDate;
            Q.Param['EE_NBR'] := EElist['EE_NBR'];
            Q.Param['CheckDate'] := EndDate;
            Q.Param['RunNumber'] := RunNumber;
            Q.Macro['WcEds'] :=  GetWcAssignedClEdsNbrs(WcNbr);
            Q.Param['WcNbr'] := WcNbr;

            CurrentGW := Q.Result.FieldByName('GROSS_WAGES').AsFloat;

            Q := TevQuery.Create('SELECT t1.pr_check_nbr, ' +
                  'SUM(IIF(t4.E_D_Code_Type like ''E%'', t3.Amount, -t3.Amount)) GROSS_WAGES ' +
                  'FROM Pr_Check  t1, Pr  t2, Cl_E_Ds t4, Pr_Check_Lines t3  ' +
                  'WHERE t2.PR_NBR = t1.PR_NBR AND ' +
                        't1.PR_NBR = :PrNbr AND ' +
                        't1.Pr_Check_Nbr = t3.Pr_Check_Nbr AND ' +
                        't4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr AND ' +
                        't1.EE_NBR = :EE_NBR AND '+
                        '{AsOfNow<t1>} and ' +
                        '{AsOfNow<t2>} and ' +
                        '{AsOfNow<t3>} and ' +
                        '{AsOfNow<t4>} and ' +
                        '(t4.E_D_Code_Type LIKE ''E%'' OR t3.Cl_E_Ds_Nbr IN (#WcEds) AND t3.Co_Workers_Comp_Nbr = :WcNbr) ' +
                  'GROUP BY t1.pr_check_nbr');
            Q.Param['BeginDate'] := BeginDate;
            Q.Param['EE_NBR'] := EElist['EE_NBR'];
            Q.Param['CheckDate'] := EndDate;
            Q.Param['PrNbr'] := PrNbr;
            Q.Param['RunNumber'] := RunNumber;
            Q.Macro['WcEds'] :=  GetWcAssignedClEdsNbrs(WcNbr);
            Q.Param['WcNbr'] := WcNbr;

            Q.Result.IndexFieldNames := 'GROSS_WAGES';
            Q.Result.First;

            while not Q.Result.Eof do
            begin
              CurrentGW := CurrentGW + Q.Result.FieldByName('GROSS_WAGES').AsFloat;
              if Q.Result['PR_CHECK_NBR'] = Checks['PR_CHECK_NBR'] then
                break;
              Q.Result.Next;
            end;

          end
          else
          begin
            Q := TevQuery.Create('SELECT SUM(IIF(t4.E_D_Code_Type like ''E%'', t3.Amount, -t3.Amount)) GROSS_WAGES ' +
                  'FROM Pr_Check  t1, Pr  t2, Cl_E_Ds t4, Pr_Check_Lines t3  ' +
                  'WHERE t2.PR_NBR = t1.PR_NBR AND ' +
                        't1.Pr_Check_Nbr = t3.Pr_Check_Nbr AND ' +
                        't4.Cl_E_Ds_Nbr = t3.Cl_E_Ds_Nbr AND ' +
                        't1.EE_NBR = :EE_NBR AND '+
                        '{AsOfNow<t1>} and ' +
                        '{AsOfNow<t2>} and ' +
                        '{AsOfNow<t3>} and ' +
                        '{AsOfNow<t4>} and ' +
                        't2.CHECK_DATE >= :BeginDate AND ((t2.CHECK_DATE < :CheckDate OR (t2.CHECK_DATE = :CheckDate AND t2.RUN_NUMBER < :RunNumber))) AND ' +
                        '(t4.E_D_Code_Type LIKE ''E%'' OR t3.Cl_E_Ds_Nbr IN (#WcEds) AND t3.Co_Workers_Comp_Nbr = :WcNbr) ');
            Q.Param['BeginDate'] := BeginDate;
            Q.Param['EE_NBR'] := EElist['EE_NBR'];
            Q.Param['CheckDate'] := EndDate;
            Q.Param['RunNumber'] := RunNumber;
            Q.Macro['WcEds'] :=  GetWcAssignedClEdsNbrs(WcNbr);
            Q.Param['WcNbr'] := WcNbr;

            CurrentGW := Q.Result.FieldByName('GROSS_WAGES').AsFloat;

            Q := TevQuery.Create('select pr_check_nbr, amount GROSS_WAGES from pr_check_lines where pr_check_lines_nbr=-1');


            if not AnsiSameText(DM_CLIENT.PR_CHECK.IndexFieldNames, 'EE_NBR;SortCheckType;PAYMENT_SERIAL_NUMBER') then
              DM_CLIENT.PR_CHECK.IndexFieldNames := 'EE_NBR;SortCheckType;PAYMENT_SERIAL_NUMBER';

            DM_CLIENT.PR_CHECK.SetRange([EElist['EE_NBR']], [EElist['EE_NBR']]);
            try
              DM_CLIENT.PR_CHECK.First;
              while not DM_CLIENT.PR_CHECK.Eof do
              begin
                if not AnsiSameText(DM_CLIENT.PR_CHECK_LINES.IndexFieldNames, 'PR_CHECK_NBR') then
                  DM_CLIENT.PR_CHECK_LINES.IndexFieldNames := 'PR_CHECK_NBR';

                Q.Result.Insert;
                Q.Result['pr_check_nbr'] := DM_CLIENT.PR_CHECK['PR_CHECK_NBR'];
                Q.Result['GROSS_WAGES'] := 0;


                DM_CLIENT.PR_CHECK_LINES.SetRange([DM_CLIENT.PR_CHECK['PR_CHECK_NBR']],[DM_CLIENT.PR_CHECK['PR_CHECK_NBR']]);
                try
                  DM_CLIENT.PR_CHECK_LINES.First;
                  while not DM_CLIENT.PR_CHECK_LINES.Eof do
                  begin

                    if copy(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DM_CLIENT.PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE'),1,1) = 'E' then
                      Q.Result['GROSS_WAGES'] := Q.Result['GROSS_WAGES'] + ConvertNull(DM_CLIENT.PR_CHECK_LINES['AMOUNT'], 0)
                    else if not DM_CLIENT.PR_CHECK_LINES.FieldBYName('Co_Workers_Comp_Nbr').IsNull and (DM_CLIENT.PR_CHECK_LINES['Co_Workers_Comp_Nbr'] = WcNbr) and qWcEds.Result.FindKey([DM_CLIENT.PR_CHECK_LINES['Cl_E_Ds_Nbr']]) then
                      Q.Result['GROSS_WAGES'] := Q.Result['GROSS_WAGES'] - ConvertNull(DM_CLIENT.PR_CHECK_LINES['AMOUNT'], 0);
                    DM_CLIENT.PR_CHECK_LINES.Next;
                  end;
                finally
                  DM_CLIENT.PR_CHECK_LINES.CancelRange;
                end;

                Q.Result.Post;

                DM_CLIENT.PR_CHECK.Next;
              end;
            finally
              DM_CLIENT.PR_CHECK.CancelRange;
            end;

            Q.Result.IndexFieldNames := 'GROSS_WAGES';

            Q.Result.First;

            while not Q.Result.Eof do
            begin
              CurrentGW := CurrentGW + Q.Result.FieldByName('GROSS_WAGES').AsFloat;
              if Q.Result['PR_CHECK_NBR'] = Checks['PR_CHECK_NBR'] then
                break;
              Q.Result.Next;
            end;

          end;



          if (EEList.FieldByName('WC_WAGE_LIMIT_FREQUENCY').AsString[1] = WC_LIMIT_FREQ_EVERYPAY) and (CurrentGW < 0) then
            CurrentGW := 0;

          bSubstrDed := True;
          if WcNbr <> -1 then
          begin
            qCO_WORKERS_COMP := TevQuery.Create('Select * from CO_WORKERS_COMP where CO_WORKERS_COMP_NBR =' + intTostr(WcNbr) + ' and {AsofDate<CO_WORKERS_COMP>}');
            qCO_WORKERS_COMP.Params.AddValue('StatusDate', CheckDate);

            if qCO_WORKERS_COMP.Result.RecordCount = 0 then
              WcNbr := -1
            else if qCO_WORKERS_COMP.Result.FieldByName('CL_E_D_GROUPS_NBR').IsNull then
            begin
              if Copy(EEList.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) <> 'E' then
                WcNbr := -1;
            end
            else
            begin
              GrNbr := qCO_WORKERS_COMP.Result.FieldByName('CL_E_D_GROUPS_NBR').asInteger;
              Q := TevQuery.Create('select 1 from CL_E_D_GROUP_CODES where {AsOfDate<CL_E_D_GROUP_CODES>} and '+
                'CL_E_D_GROUPS_NBR='+IntToStr(GrNbr)+' and CL_E_DS_NBR='+IntToStr(EEList['cl_e_ds_nbr']));
              Q.Param['StatusDate'] := CheckDate;
              if Q.Result.RecordCount > 0 then
              begin
                Q := TevQuery.Create('select SUBTRACT_DEDUCTIONS from CL_E_D_GROUPS where {AsOfDate<CL_E_D_GROUPS>} and '+
                  'CL_E_D_GROUPS_NBR='+IntToStr(GrNbr));
                Q.Param['StatusDate'] := CheckDate;
                bSubstrDed := Q.Result.FieldByName('SUBTRACT_DEDUCTIONS').AsString = 'Y';
              end
              else
                WcNbr := -1;
            end;
          end;

          if WcNbr <> -1 then
          begin
            if rds.Locate('EE_NBR;wc_nbr; CO_NBR; CO_TEAM_NBR; CO_DEPARTMENT_NBR; CO_BRANCH_NBR; CO_DIVISION_NBR',
                  VarArrayOf([EElist['EE_NBR'], WcNbr, EEList['CO_NBR'], EEList['CO_TEAM_NBR'], EEList['CO_DEPARTMENT_NBR'], EEList['CO_BRANCH_NBR'], EEList['CO_DIVISION_NBR']]), []) then
              rds.Edit
            else
            begin
              rds.Insert;
              rds['EE_NBR'] := EEList['EE_NBR'];
              rds['wc_nbr'] := WcNbr;
              rds['gw'] := 0;
              rds['op'] := 0;
              rds['limit'] := EEList['WORKERS_COMP_WAGE_LIMIT'];
              rds['GrossWagesByWCFrequency'] := CurrentGW;
              rds['CO_NBR'] := EEList['CO_NBR'];
              rds['CO_TEAM_NBR'] := EEList['CO_TEAM_NBR'];
              rds['CO_DEPARTMENT_NBR'] := EEList['CO_DEPARTMENT_NBR'];
              rds['CO_BRANCH_NBR'] := EEList['CO_BRANCH_NBR'];
              rds['CO_DIVISION_NBR'] := EEList['CO_DIVISION_NBR'];
              rds['sp'] := GROUP_BOX_WC_OVERTIME_ONLY;

              if qCO_WORKERS_COMP.Result.RecordCount > 0 then
              begin
                rds['wcrate'] := qCO_WORKERS_COMP.Result.FieldByName('RATE').value;
                rds['er'] := qCO_WORKERS_COMP.Result.FieldByName('EXPERIENCE_RATING').value;
                rds['sp'] := qCO_WORKERS_COMP.Result.FieldByName('SUBTRACT_PREMIUM').value;
              end;
            end;

            if (EEList['E_D_CODE_TYPE'] = ED_OEARN_OVERTIME)  then   // EF              ok
            begin
              RegRate := EEList.FieldByName('regular_ot_rate').AsFloat;
              if RegRate < 1 then
                RegRate := 1
              else
                RegRate := 1 - 1 / RegRate;
            end
            else if EEList['E_D_CODE_TYPE'] = ED_OEARN_WAITSTAFF_OVERTIME then  //EG  ok
              RegRate := 1 / 3
            else if EEList['E_D_CODE_TYPE'] = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME then
              RegRate := GetRateOfPay
            else if EEList['E_D_CODE_TYPE'] = ED_OEARN_WEIGHTED_HALF_TIME_OT then  //EH   ok
              RegRate := 1
            else if EEList['E_D_CODE_TYPE'] = ED_OEARN_WEIGHTED_3_HALF_TIME_OT then  // EI
              if (not IsFluctuating) and (rds['sp'] <> 'N')  then
                RegRate := GetRateOfPay
              else
                RegRate := 0// full amount

            else if (EEList['E_D_CODE_TYPE'] = ED_OEARN_AVG_OVERTIME) or (EEList['E_D_CODE_TYPE'] = ED_OEARN_AVG_MULT_OVERTIME) then      // EJ
            begin
              RegRate := EEList.FieldByName('regular_ot_rate').AsFloat;
              if (RegRate <= 1) or
                 ((rds['sp'] = GROUP_BOX_WC_OVERTIME_AND_PREMIUM) and (RegRate = 1.5) and (EEList['REDUCE']='N')) then
                 RegRate := 0
              else
                 RegRate := GetRateOfPay;
            end
            else
              RegRate := 0; // other codes aren't included in overtime premium
            if (RegRate > 0) and
               (EEList['E_D_CODE_TYPE'] <> ED_OEARN_WEIGHTED_3_HALF_TIME_OT) and
               (EEList['E_D_CODE_TYPE'] <> ED_OEARN_AVG_OVERTIME) and
               (EEList['E_D_CODE_TYPE'] <> ED_OEARN_AVG_MULT_OVERTIME) then
            begin
              if rds['sp'] = GROUP_BOX_WC_OVERTIME_AND_PREMIUM then
                RegRate := 1
              else if rds['sp'] = GROUP_BOX_WC_NONE then  //equivalent to changes in TrwlSumWorkersCompBuffer
                RegRate := 0
              else
                Assert( rds['sp'] = GROUP_BOX_WC_OVERTIME_ONLY );
            end;
            if bSubstrDed and (Copy(EEList.FieldByName('E_D_CODE_TYPE').AsString, 1, 1) <> 'E') then
              fAmount := -EEList.FieldByName('Amount').AsFloat
            else
              fAmount := EEList.FieldByName('Amount').AsFloat;

            if  (EEList['E_D_CODE_TYPE'] = 'EF') or
                (EEList['E_D_CODE_TYPE'] = 'EH')
                 then
               fAmountOP := fAmount * RegRate

            else if (EEList['E_D_CODE_TYPE'] = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) or
                    (EEList['E_D_CODE_TYPE'] = ED_OEARN_WAITSTAFF_OVERTIME) then
            begin
              if {(EEList['RATE_OF_PAY'] < OTMinWage)
              and} (rds['sp'] <> GROUP_BOX_WC_OVERTIME_AND_PREMIUM) then
              begin

                if (EEList['E_D_CODE_TYPE'] = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) then
                   fAmountOP := fAmount - RegRate * ConvertNull(EEList['HOURS_OR_PIECES'], 0)
                else
                begin
                  OTMinWage := GetMinimumWage(Null, EEList['EE_NBR']);
                  fAmountOP := (0.5 * OTMinWage) * ConvertNull(EEList['HOURS_OR_PIECES'], 0);
                end;
              end
              else
              begin
                fAmountOP := fAmount * RegRate;
              end;
              if (rds['sp'] = GROUP_BOX_WC_NONE) then
              begin
                fAmountOP := 0;
              end;
            end
            else if (EEList['E_D_CODE_TYPE'] = 'EI') or (EEList['E_D_CODE_TYPE'] = ED_OEARN_AVG_OVERTIME)
                 or (EEList['E_D_CODE_TYPE'] = ED_OEARN_AVG_MULT_OVERTIME) then
              fAmountOP := fAmount - RegRate * ConvertNull(EEList['HOURS_OR_PIECES'], 0)
            else
              fAmountOP := 0;

            rds['op'] := rds.FieldByName('op').AsFloat + RoundAll(fAmountOP, 2);
            rds['gw'] := rds.FieldByName('gw').AsFloat + fAmount;
            rds.Post;
          end;

          EEList.Next;
        end;


        EeNbr := 0;
        GrossWages := 0;
        LimitBalance := 0;
        LastRec := 0;
        NoLimit := False;
        rds.IndexFieldNames := 'EE_NBR;wc_nbr;co_division_nbr;co_branch_nbr;co_department_nbr;co_team_nbr';
        rds.First;
        while not rds.Eof do
        begin
          if (EeNbr <> rds.FieldByName('ee_nbr').AsInteger) then
          begin
            EeNbr := rds.FieldByName('ee_nbr').AsInteger;
            GrossWagesSum := rds.FieldByName('GrossWagesByWCFrequency').AsFloat;
            Limit := rds.FieldByName('limit').AsFloat;

            if (Limit = 0) then
            begin
              NoLimit := True;
            end else
            begin
              NoLimit := False;
              Nbr := rds.RecNo;
              GrossWages := 0;
              while (not rds.Eof) and (EeNbr = rds.FieldByName('EE_NBR').AsInteger) do
              begin
                GrossWages := GrossWages + rds.FieldByName('GW').AsFloat;
                LastRec := rds.RecNo;
                rds.Next;
              end;
              rds.RecNo := Nbr;
              if GrossWages > 0.005 then
              begin
                LimitBalance := GrossWages - GrossWagesSum + Limit;
                if LimitBalance < 0 then
                  LimitBalance := 0;
                if LimitBalance > rds.FieldByName('GW').AsFloat then
                  LimitBalance := rds.FieldByName('GW').AsFloat;  
              end
              else
              if GrossWages < 0.005 then
              begin
                LimitBalance := GrossWagesSum - Limit;
                if LimitBalance > 0 then
                  LimitBalance := 0;
                if LimitBalance < rds.FieldByName('GW').AsFloat then
                  LimitBalance := rds.FieldByName('GW').AsFloat;
              end
              else
                LimitBalance := 0;
            end;
          end;

          if NoLimit then
          begin
            sgw := rds.FieldByName('GW').AsFloat;
            sag := sgw;
          end else if (LimitBalance = 0) or (GrossWages = 0) then
          begin
            sgw := rds.FieldByName('GW').AsFloat;
            sag := 0;
          end else
          begin
            if rds.RecNo = LastRec then
            begin
              sag := LimitBalance;
              sgw := GrossWages;
            end else
            begin

              sgw := rds.FieldByName('GW').AsFloat;
              if GrossWages <> 0 then
                sag := RoundTwo(LimitBalance * sgw / GrossWages)
              else
                sag := 0;
              GrossWages := GrossWages - sgw;
              LimitBalance := LimitBalance - sag;

            end;
          end;

          op := rds.FieldByName('op').AsFloat;
          if (op > sgw ) and (sgw > 0) then
            op := sgw;

          if rds.FieldByName('sp').AsString <> GROUP_BOX_WC_NONE then
          begin
            sag := sag - op;
          end;
          if rds.FieldByName('er').AsFloat = 0 then
            m := RoundAll(sag * rds.FieldByName('wcrate').AsFloat, 2)
          else
            m := RoundAll(RoundAll(sag * rds.FieldByName('wcrate').AsFloat, 2) * rds.FieldByName('er').AsFloat, 2);

          Result := Result + m;

          if Assigned(dds) then
          begin
            if dds.FindKey([rds[sLevel]]) then
              dds.Edit
            else
            begin
              dds.Insert;
              dds[sLevel] := rds[sLevel];
            end;
            dds.FieldByName('Amount').AsCurrency := dds.FieldByName('Amount').AsCurrency + m;
            dds.Post
          end;
          rds.Next;
        end;

      finally
        rds.Free;
      end;

      Checks.Next;

    end;
    Employees.Next;
  end;  
end;

function TevPayrollCalculation.CheckDueDateForHoliday(
  DueDate: TDateTime; const GlAgencyNbr: Integer): TDateTime;
  function ExecuteRule(const Rule: Char; var DueDate: TDateTime): Boolean;
  begin
    Result := False;
    case Rule of
    HOLIDAY_RULE_BEFORE:
      DueDate := DueDate- 1;
    HOLIDAY_RULE_AFTER:
      DueDate := DueDate+ 1;
    HOLIDAY_RULE_DISREGARD:
      Result := True;
    else
      Assert(False);
    end;
  end;
var
  Dates: array of TDateTime;
  procedure AddToListAndCheckForLoop(const d: TDateTime);
  var
    i, j: Integer;
  begin
    i := Length(Dates);
    for j := 0 to i-1 do
      if Dates[j] = d then
        raise EInconsistentData.CreateFmtHelp('Agency %s'#13'Permanent loop detected during checking %s for holidays.',
          [DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', GlAgencyNbr, 'AGENCY_NAME'), DateToStr(d)], IDH_InconsistentData);
    SetLength(Dates, i+1);
    Dates[i] := d;
  end;
var
  WeekDaysOff: string;
  HolidayRule: char;
begin
  if (GlAgencyNbr = 98) {OR} or (GlAgencyNbr = 0 ) then
  begin
    Result := DueDate;
    Exit;
  end;

  SetLength(Dates, 0);
  with DM_SYSTEM_MISC do
  repeat
     Assert(SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR', GlAgencyNbr, []));
     case SY_GLOBAL_AGENCY.FieldByName('IGNORE_WEEKENDS').AsString[1] of
      IGNORE_WEEKENDS_SUNDAY:
            WeekDaysOff:='7';
      IGNORE_WEEKENDS_NONE:
            WeekDaysOff:='';
     else
            WeekDaysOff:='67';
     end;

     if Pos(IntToStr(DayOfTheWeek(DueDate)), WeekDaysOff) <> 0 then // skip day off
     begin
       if ExtractFromFiller(SY_GLOBAL_AGENCY.FieldByName('FILLER').AsString, 3, 1) = HOLIDAY_RULE_BEFORE then
          ExecuteRule(HOLIDAY_RULE_BEFORE, DueDate)
       else
          ExecuteRule(HOLIDAY_RULE_AFTER, DueDate);
       AddToListAndCheckForLoop(DueDate);
       Continue;
     end;

     if DateIsFederalHoliday(DueDate, HolidayRule,  GlAgencyNbr) then
     begin
       if HolidayRule = 'X' then
       begin
         if ExtractFromFiller(SY_GLOBAL_AGENCY.FieldByName('FILLER').AsString, 3, 1) = HOLIDAY_RULE_BEFORE then
            ExecuteRule(HOLIDAY_RULE_BEFORE, DueDate)
         else
            ExecuteRule(HOLIDAY_RULE_AFTER, DueDate);
         AddToListAndCheckForLoop(DueDate);
         Continue;
       end
       else
       begin

       // CACA



       end;
     end;
     Break;
  until False;
  Result := DueDate;
end;

function TevPayrollCalculation.FindNextPeriodBeginEndDate: Boolean;
begin
  Result := FindNextPeriodBeginEndDateExt(DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH);
end;

function TevPayrollCalculation.FindNextPeriodBeginEndDateExt(PR, PR_BATCH: TevClientDataSet): Boolean;
var
  SchEventNbr: Integer;

  function FindBatch: Boolean;
  begin
    Result := False;
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DataRequired('PR_SCHEDULED_EVENT_NBR=' + IntToStr(SchEventNbr));
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
    while not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Eof do
    begin
      with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
      try
        if PR_BATCH.FieldByName('FREQUENCY').IsNull
        or (IndexOf(PR_BATCH['FREQUENCY']) >= 0) then
        begin
          Result := True;
          Break;
        end;
      finally
        Free;
      end;
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Next;
    end;
  end;

begin
  Result := False;
  DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
  try
    SchEventNbr := DM_PAYROLL.PR_SCHEDULED_EVENT.Lookup('PR_NBR', PR['PR_NBR'], 'PR_SCHEDULED_EVENT_NBR');
  except
    raise EInconsistentData.CreateHelp('This is a scheduled payroll with no calendar entry attached! Can not proceed.', IDH_InconsistentData);
  end;

  with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
  begin
    SetMacro('Table1', 'PR');
    SetMacro('Table2', 'PR_SCHEDULED_EVENT');
    SetMacro('JoinField', 'PR');
    SetMacro('Condition', 'T1.CHECK_DATE=''' + PR.FieldByName('CHECK_DATE').AsString
      + ''' and T1.PR_NBR<>' + PR.FieldByName('PR_NBR').AsString
      + ' and T1.CO_NBR=' + PR.FieldByName('CO_NBR').AsString
      + ' and T1.SCHEDULED=''Y'' order by T2.PR_SCHEDULED_EVENT_NBR');
    SetMacro('Columns', 'T2.PR_SCHEDULED_EVENT_NBR');
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
    ctx_DataAccess.CUSTOM_VIEW.First;
    while not Result and not ctx_DataAccess.CUSTOM_VIEW.EOF do
    begin
      SchEventNbr := ctx_DataAccess.CUSTOM_VIEW['PR_SCHEDULED_EVENT_NBR'];
      Result := FindBatch;
      if Result then
        Break;
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;
  if not Result then
  begin
    SchEventNbr := DM_PAYROLL.PR_SCHEDULED_EVENT.Lookup('PR_NBR', PR['PR_NBR'], 'PR_SCHEDULED_EVENT_NBR');
    Result := FindBatch;
  end;
end;

function TevPayrollCalculation.ReturnMonthNumber(sMonth: String; PeriodType: TPeriodType): Integer;
begin
  Result := 1;
  case PeriodType of
    ptQuarter:
    begin
      if sMonth = 'N' then
        Result := 3
      else
        Result := StrToInt('$' + sMonth);
      if Result > 3 then
        Result := 3;
    end;
    ptSemiAnnual:
    begin
      if sMonth = 'N' then
        Result := 6
      else
        Result := StrToInt('$' + sMonth);
      if Result > 6 then
        Result := 6;
    end;
    ptAnnual:
    begin
      if sMonth = 'N' then
        Result := 12
      else
        Result := StrToInt('$' + sMonth);
    end;
  end;
end;

procedure TevPayrollCalculation.GetDistrTaxes(PrNbr: Integer; var cdsTaxDistr: TevClientDataSet; EEOnly: Integer = 0);
var
  cds: TevClientDataSet;
  b: Boolean;
  fname: Boolean;

  procedure FillWithData(const sQuery: string; const fEnd: Integer; const fStart: Integer = 1);
  var
    i: Integer;
    aExact: array[0..7] of real;
    aRound: array[0..7] of Currency;
    PrCheckNbr: Integer;
  begin
    with TExecDSWrapper.Create(sQuery) do
    begin
      SetMacro('PrNbr', IntToStr(PrNbr));
      SetParam('PrNbr', PrNbr);
      SetParam('dt', SysTime);
      cds.DataRequest(AsVariant);
      cds.Close;
      cds.Open;
    end;

    for i := 0 to 7 do
    begin
      aExact[i] := 0;
      aRound[i] := 0;
    end;
    PrCheckNbr := cds.FieldByName('pr_check_nbr').AsInteger;
    while not cds.Eof do
    begin
      if PrCheckNbr <> cds.FieldByName('pr_check_nbr').AsInteger then
      begin
        cds.Prior;
        for i := Pred(fStart) to Pred(fEnd) do
        begin
          if RoundAll(aExact[i], 2) - aRound[i] <> 0 then
          begin
            cdsTaxDistr.Append;
            cdsTaxDistr.FieldByName('tax').AsCurrency := RoundAll(aExact[i], 2) - aRound[i];
            cdsTaxDistr['pr_check_nbr'] := cds['pr_check_nbr'];
            cdsTaxDistr['co_division_nbr'] := cds['co_division_nbr'];
            cdsTaxDistr['co_branch_nbr'] := cds['co_branch_nbr'];
            cdsTaxDistr['co_department_nbr'] := cds['co_department_nbr'];
            cdsTaxDistr['co_team_nbr'] := cds['co_team_nbr'];
            if fname then
              cdsTaxDistr['fname'] := cds.Fields[i].FieldName;
            cdsTaxDistr.Post;
          end;
          aExact[i] := 0;
          aRound[i] := 0;
        end;
        cds.Next;
        PrCheckNbr := cds.FieldByName('pr_check_nbr').AsInteger;
      end;
      for i := Pred(fStart) to Pred(fEnd) do
      begin
        cdsTaxDistr.Append;
        cdsTaxDistr.FieldByName('tax').AsCurrency := RoundAll(cds.Fields[i].AsFloat, 2);
        cdsTaxDistr['pr_check_nbr'] := cds['pr_check_nbr'];
        cdsTaxDistr['co_division_nbr'] := cds['co_division_nbr'];
        cdsTaxDistr['co_branch_nbr'] := cds['co_branch_nbr'];
        cdsTaxDistr['co_department_nbr'] := cds['co_department_nbr'];
        cdsTaxDistr['co_team_nbr'] := cds['co_team_nbr'];
        if fname then
          cdsTaxDistr['fname'] := cds.Fields[i].FieldName;
        cdsTaxDistr.Post;
        aExact[i] := aExact[i] + cds.Fields[i].AsFloat;
        aRound[i] := aRound[i] + RoundAll(cds.Fields[i].AsFloat, 2);
      end;
      cds.Next;
    end;
    for i := Pred(fStart) to Pred(fEnd) do
    begin
      if RoundAll(aExact[i], 2) - aRound[i] <> 0 then
      begin
        cdsTaxDistr.Append;
        cdsTaxDistr.FieldByName('tax').AsCurrency := RoundAll(aExact[i], 2) - aRound[i];
        cdsTaxDistr['pr_check_nbr'] := cds['pr_check_nbr'];
        cdsTaxDistr['co_division_nbr'] := cds['co_division_nbr'];
        cdsTaxDistr['co_branch_nbr'] := cds['co_branch_nbr'];
        cdsTaxDistr['co_department_nbr'] := cds['co_department_nbr'];
        cdsTaxDistr['co_team_nbr'] := cds['co_team_nbr'];
        cdsTaxDistr.Post;
      end;
      aExact[i] := 0;
      aRound[i] := 0;
    end;
  end;

begin
  b := cdsTaxDistr.AggregatesActive;
  cdsTaxDistr.AggregatesActive := False;
  cds := TevClientDataSet.Create(nil);
  try
    cds.ProviderName := 'CL_CUSTOM_PROV';
    cdsTaxDistr.Close;
    cdsTaxDistr.CreateDataSet;
    fname := Assigned(cdsTaxDistr.FindField('fname'));
    if EEOnly = 2 then
    begin
      FillWithData('DistrFederalTaxesForACH', 8, 6);
      FillWithData('DistrStateTaxesForACH', 3, 3);
      DM_SYSTEM_STATE.SY_SUI.Activate;
      DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
      cdsFilterString := '';
      DM_SYSTEM_LOCAL.SY_LOCALS.First;
      while not DM_SYSTEM_LOCAL.SY_LOCALS.Eof do
      begin
        if DM_SYSTEM_LOCAL.SY_LOCALS['TAX_TYPE'] = GROUP_BOX_ER then
          cdsFilterString := cdsFilterString + DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('SY_LOCALS_NBR').AsString + ' ';
        DM_SYSTEM_LOCAL.SY_LOCALS.Next
      end;
      cds.Close;
      cds.OnFilterRecord := cdsLocalFilter;
      cds.Filtered := True
    end
    else if EEOnly = 1 then
    begin
      FillWithData('DistrFederalTaxesForACH', 5);
      FillWithData('DistrStateTaxesForACH', 2);
      DM_SYSTEM_STATE.SY_SUI.Activate;
      DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
      cdsFilterString := '';
      DM_SYSTEM_LOCAL.SY_LOCALS.First;
      while not DM_SYSTEM_LOCAL.SY_LOCALS.Eof do
      begin
        if DM_SYSTEM_LOCAL.SY_LOCALS['TAX_TYPE'] = GROUP_BOX_EE then
          cdsFilterString := cdsFilterString + DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('SY_LOCALS_NBR').AsString + ' ';
        DM_SYSTEM_LOCAL.SY_LOCALS.Next
      end;
      cds.Close;
      cds.OnFilterRecord := cdsLocalFilter;
      cds.Filtered := True
    end
    else
    begin
      FillWithData('DistrFederalTaxesForACH', 8);
      FillWithData('DistrStateTaxesForACH', 3)
    end;
    FillWithData('DistrLocalTaxesForACH', 1);
    if EEOnly = 2 then
    begin
      cdsFilterString := '';
      DM_SYSTEM_STATE.SY_SUI.First;
      while not DM_SYSTEM_STATE.SY_SUI.Eof do
      begin
        if (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_ER) or
           (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_ER_OTHER) then
          cdsFilterString := cdsFilterString + DM_SYSTEM_STATE.SY_SUI.FieldByName('SY_SUI_NBR').AsString + ' ';
        DM_SYSTEM_STATE.SY_SUI.Next
      end;
      cds.Close;
      cds.OnFilterRecord := cdsSUIFilter;
      cds.Filtered := True
    end
    else if EEOnly = 1 then
    begin
      cdsFilterString := '';
      DM_SYSTEM_STATE.SY_SUI.First;
      while not DM_SYSTEM_STATE.SY_SUI.Eof do
      begin
        if (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_EE) or
           (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_EE_OTHER) then
          cdsFilterString := cdsFilterString + DM_SYSTEM_STATE.SY_SUI.FieldByName('SY_SUI_NBR').AsString + ' ';
        DM_SYSTEM_STATE.SY_SUI.Next
      end;
      cds.Close;
      cds.OnFilterRecord := cdsSUIFilter;
      cds.Filtered := True
    end;
    FillWithData('DistrSUITaxesForACH', 1);
    cdsTaxDistr.First;
  finally
    cds.Free;
    cdsTaxDistr.AggregatesActive := b;
    DM_SYSTEM_STATE.SY_SUI.Filtered := False;
    DM_SYSTEM_STATE.SY_SUI.OnFilterRecord := nil;
    DM_SYSTEM_LOCAL.SY_LOCALS.Filtered := False;
    DM_SYSTEM_LOCAL.SY_LOCALS.OnFilterRecord := nil
  end;
end;

procedure TevPayrollCalculation.cdsLocalFilter(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := Pos(' ' + DataSet.FieldByName('SY_LOCALS_NBR').AsString + ' ', ' ' + cdsFilterString + ' ') > 0
end;

procedure TevPayrollCalculation.cdsSUIFilter(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := Pos(' ' + DataSet.FieldByName('SY_SUI_NBR').AsString + ' ', ' ' + cdsFilterString + ' ') > 0
end;

procedure TevPayrollCalculation.ReleaseTempCheckLines;
begin
  cdstTempCheckLines.CancelRange;
  FLastAppliedCheckRangePrCheckNbr := -1;
  cdstTempCheckLines.Close;
end;

function TevPayrollCalculation.CheckIfBlockCheckLineOnCheck(PR_CHECK, PR_CHECK_LINES, CL_E_DS, EE_SCHEDULED_E_DS: TevClientDataSet): Boolean;
var
  TempVar: Variant;
begin
  Result := False;
  with PR_CHECK_LINES do
  begin
    if PR_CHECK['EXCLUDE_DD'] = 'Y' then
      if not VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value,
        'EE_DIRECT_DEPOSIT_NBR')) then
      begin
        Result := True;
      end;
    if PR_CHECK['EXCLUDE_DD_EXCEPT_NET'] = 'Y' then
      if (not VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, 'EE_DIRECT_DEPOSIT_NBR')))
        and (EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, 'DEDUCT_WHOLE_CHECK') = 'N') then
      begin
        Result := True;
      end;
    if PR_CHECK['EXCLUDE_ALL_SCHED_E_D_CODES'] = 'Y' then
    begin
      if not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
      begin
        // The name is misleading. We also don't want to exclude DD
        if VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value,
          'EE_DIRECT_DEPOSIT_NBR')) then
        begin
        Result := True;
        end;
      end;
    end;
    if PR_CHECK['EXCLUDE_SCH_E_D_EXCEPT_PENSION'] = 'Y' then
    begin
      // The name is misleading. We also don't want to exclude DD
      if VarIsNull(EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value,
        'EE_DIRECT_DEPOSIT_NBR')) then
      begin
        TempVar := EE_SCHEDULED_E_DS.Lookup('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, 'CL_E_DS_NBR');
        if not VarIsNull(TempVar) then
        begin
          CL_E_DS.Activate;
          if not TypeIsPension(VarToStr(CL_E_DS.Lookup('CL_E_DS_NBR', TempVar, 'E_D_CODE_TYPE'))) then
            if (CL_E_DS.Lookup('CL_E_DS_NBR', TempVar, 'E_D_CODE_TYPE') <> ED_MEMO_PENSION) and (CL_E_DS.Lookup('CL_E_DS_NBR', TempVar, 'E_D_CODE_TYPE') <> ED_MEMO_PENSION_SUI) then
            begin
              Result := True;
            end;
        end;
      end;
    end;
  end;
end;

function TevPayrollCalculation.GetTempCheckLinesDS: TevClientDataSet;
begin
  if cdstTempCheckLines['PR_CHECK_LINES_NBR'] = DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value then
    Result := DM_PAYROLL.PR_CHECK_LINES
  else
    Result := cdstTempCheckLines;
end;

procedure TevPayrollCalculation.GetSuiTaxDepositPeriod(
  SySuiNbr: Integer; Date: TDateTime; var BeginDate, EndDate,
  DueDate: TDateTime);
var
  State: string;
//  DaysOff: string;  
begin
  ctx_DataAccess.Activate([DM_SYSTEM_STATE.SY_SUI, DM_SYSTEM_STATE.SY_STATES]);
  State := DM_SYSTEM_STATE.SY_STATES.ShadowDataSet.CachedLookup(
    StrToIntDef(DM_SYSTEM_STATE.SY_SUI.ShadowDataSet.CachedLookup(SySuiNbr, 'SY_STATES_NBR'), 0),
    'STATE');
  BeginDate := GetBeginQuarter(Date);
  EndDate := GetEndQuarter(Date);
  if (State = 'MI') then
    DueDate := GetEndQuarter(Date)+ 25
  else
  if (State = 'NJ') or (State = 'WY') or (State = 'LA') then
    DueDate := GetEndQuarter(Date)+ 30
  else
    DueDate := GetEndMonth(GetEndQuarter(Date)+ 1);


{
  if (State = 'VT')
  or (State = 'AK')
  or (State = 'IA')
  or (State = 'MD')
  or (State = 'ND')
  or (State = 'RI') then
    DaysOff := '7'
  else
    DaysOff := '67';

  DueDate := CheckDueDateForHoliday(DueDate,
    StrToIntDef(DM_SYSTEM_STATE.SY_SUI.ShadowDataSet.CachedLookup(SySuiNbr, 'SY_SUI_TAX_PMT_AGENCY_NBR'), 0),
    DaysOff);
}
  DueDate := CheckDueDateForHoliday(DueDate,
    StrToIntDef(DM_SYSTEM_STATE.SY_SUI.ShadowDataSet.CachedLookup(SySuiNbr, 'SY_SUI_TAX_PMT_AGENCY_NBR'), 0));

end;

function TevPayrollCalculation.CalculateGroupTermLife(PR: TevClientDataSet; Multiplier: Real): Real;
var
  GTLRate, PolicyAmount: Real;
begin
  Result := 0;
  if Multiplier = 0 then
    Exit;
  PolicyAmount := ConvertNull(DM_EMPLOYEE.EE['GROUP_TERM_POLICY_AMOUNT'], 0);
  if PolicyAmount = 0 then
    PolicyAmount := ConvertNull(DM_EMPLOYEE.EE['SALARY_AMOUNT'], 0)
      * ConvertNull(DM_EMPLOYEE.EE['GTL_RATE'], 0) * GetTaxFreq(DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value);
  if PolicyAmount = 0 then
  begin
    if (not DM_EMPLOYEE.EE_RATES.Active) or (not DM_EMPLOYEE.EE_RATES.NewLocate('EE_NBR', DM_EMPLOYEE.EE['EE_NBR'], [])) then
      DM_EMPLOYEE.EE_RATES.DataRequired('EE_NBR=' + DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString);
      PolicyAmount := ConvertNull(DM_EMPLOYEE.EE_RATES.NewLookup('EE_NBR;PRIMARY_RATE', VarArrayOf([DM_EMPLOYEE.EE['EE_NBR'], 'Y']), 'RATE_AMOUNT'), 0)
        * ConvertNull(DM_EMPLOYEE.EE['GTL_RATE'], 0) * ConvertNull(DM_EMPLOYEE.EE.FieldByName('GTL_NUMBER_OF_HOURS').Value, 0);
  end;
  if PolicyAmount > 50000 then
  begin
    DM_CLIENT.CL_PERSON.Activate;
    if VarIsNull(DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE['CL_PERSON_NBR'], 'BIRTH_DATE')) then
      raise ENoBirthDate.CreateHelp('Birth date is not defined for EE #' + Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString)
        + '. GTL can not be calculated.', IDH_InconsistentData);
    GTLRate := GroupTermLifeRate(CalculateAge(PR['CHECK_DATE'], DM_CLIENT.CL_PERSON.Lookup('CL_PERSON_NBR', DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').Value, 'BIRTH_DATE'), True));
    Result := (PolicyAmount - 50000) / 1000 * GTLRate * Multiplier;
  end;
end;

procedure TevPayrollCalculation.CheckQuarterBeginEnd(
  const CheckDate: TDateTime; var BegDate, EndDate: TDateTime);
begin
  if GetEndQuarter(CheckDate) < EndDate then
    EndDate := GetEndQuarter(CheckDate);
  if GetBeginQuarter(CheckDate) > BegDate then
    BegDate := GetBeginQuarter(CheckDate);
end;

function TevPayrollCalculation.ApplyTempCheckLinesCheckRange(
  const PrCheckNbr: Integer): Boolean;
begin
  Assert(cdstTempCheckLines.IndexFieldNames = 'PR_CHECK_NBR;LINE_TYPE');
  cdstTempCheckLines.SetRange([PrCheckNbr], [PrCheckNbr]);
  Result := FLastAppliedCheckRangePrCheckNbr <> PrCheckNbr;
  FLastAppliedCheckRangePrCheckNbr := PrCheckNbr;
end;

function TevPayrollCalculation.CheckForNDaysRule(const SY_GLOBAL_AGENCY_NBR, Days: Integer; const DefaultDate: TDateTime): TDateTime;
begin
  Result := EvUtils.CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, Days, DefaultDate);
end;

procedure TevPayrollCalculation.SetStateTax(NewStateTax: Double);
begin
  FStateTax := NewStateTax;
end;

function TevPayrollCalculation.IsNonBusinessDay(
  Day: TDateTime): Boolean;
begin
  Result := DayIsWeekend(Day);

  if not Result then
    Result := not (HolidayDay(Day, True) = '');
end;

function TevPayrollCalculation.GetAnnualMax(Amount: Variant; EDType: String;
  CLEDNbr: Integer): Double;
var
  MATCH_FIRST_AMOUNT,
  MATCH_FIRST_PERCENTAGE,
  MATCH_SECOND_AMOUNT,
  MATCH_SECOND_PERCENTAGE: Real;
begin
  if ((EDType = ED_MEMO_PENSION) or (EDType = ED_MEMO_PENSION_SUI)) and VarIsNull(Amount) then
  begin
    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
    if DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('COMPENSATION_LIMIT').IsNull then
      raise EInconsistentData.Create('Please, fill ''Compensation Limit'' field on screen ''iSystems -> Federal -> Tax Table'' with proper value!');
    DM_CLIENT.CL_E_DS.Activate;
    MATCH_FIRST_AMOUNT := ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', CLEDNbr, 'MATCH_FIRST_AMOUNT'), 0);
    MATCH_FIRST_PERCENTAGE := ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', CLEDNbr, 'MATCH_FIRST_PERCENTAGE'), 0);
    MATCH_SECOND_AMOUNT := ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', CLEDNbr, 'MATCH_SECOND_AMOUNT'), 0);
    MATCH_SECOND_PERCENTAGE := ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', CLEDNbr, 'MATCH_SECOND_PERCENTAGE'), 0);
    Result := DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE['COMPENSATION_LIMIT']
      * (MATCH_FIRST_AMOUNT * 0.01
      * MATCH_FIRST_PERCENTAGE * 0.01
      + MATCH_SECOND_AMOUNT * 0.01
      * MATCH_SECOND_PERCENTAGE * 0.01);
  end
  else
    Result := ConvertNull(Amount, 0);
end;

procedure TevPayrollCalculation.AttachPayrollToCalendar(PR: TevClientDataSet);
var
  CheckDate: TDateTime;
  NeedNewScheduledCheckDate: Boolean;
  CheckDateAction, I: Integer;
begin
  if PR.FieldByName('SCHEDULED').AsString = 'Y' then
  with DM_PAYROLL.PR_SCHEDULED_EVENT do
  begin
    CheckDate := 0;

    DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
    First;

    NeedNewScheduledCheckDate := False;
    while (not FieldByName('PR_NBR').IsNull) and (not EOF) do
      Next;
    if EOF then
      NeedNewScheduledCheckDate := True;

    First;
    if (not Locate('SCHEDULED_CHECK_DATE;PR_NBR', VarArrayOf([PR['CHECK_DATE'], Null]), [])) or NeedNewScheduledCheckDate then
    begin
      if not Unattended then
       with CreateMessageDialog('Would you like to insert a new scheduled date in the Company Calendar?', mtConfirmation, [mbYes, mbNo, mbCancel]) do
        try
          for I := 0 to ComponentCount - 1 do
          begin
            if Components[I].Name = 'Cancel' then
              TButton(Components[I]).Caption := 'Replace';
          end;
          CheckDateAction := ShowModal;
        finally
          Free;
        end
      else
       CheckDateAction := mrYes;
      case CheckDateAction of
      mrYes:
      begin
        if (Locate('PR_NBR', PR.FieldByName('PR_NBR').AsInteger, [])) then
        begin
          edit;
          FieldByName('PR_NBR').Value := Null;
          post;
        end;
        CheckDate := PR.FieldByName('CHECK_DATE').AsDateTime;
        Insert;
        FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
        FieldByName('STATUS').Value := DM_COMPANY.CO['PAY_FREQUENCIES'];
        FieldByName('EVENT_DATE').AsDateTime := 0;
        FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := Date;
        FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := 0;
        FieldByName('SCHEDULED_CHECK_DATE').AsDateTime := CheckDate;
        Post;
      end;
      mrCancel:
      begin
        if PR.State = dsInsert then
        begin
          CheckDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
          IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
          Locate('SCHEDULED_CHECK_DATE', CheckDate, []);
        end
        else
          Locate('PR_NBR', PR.FieldByName('PR_NBR').AsInteger, []);
        CheckDate := PR.FieldByName('CHECK_DATE').AsDateTime;
      end;
      mrNo:
      begin
        if (Locate('PR_NBR', PR.FieldByName('PR_NBR').AsInteger, [])) then
        begin
          edit;
          FieldByName('PR_NBR').Value := Null;
          post;
        end;
        PR.FieldByName('SCHEDULED').AsString := 'N';
        PR.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
        // exclude frequency based time-off
        PR.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
        Exit;
      end;
      end;
    end
    else
    begin
      if (Locate('PR_NBR', PR.FieldByName('PR_NBR').AsInteger, [])) then
      begin
        edit;
        FieldByName('PR_NBR').Value := Null;
        FieldByName('NORMAL_CHECK_DATE').Value := Null;
        post;
        Locate('SCHEDULED_CHECK_DATE;PR_NBR', VarArrayOf([PR['CHECK_DATE'], Null]), []);
      end;
      CheckDate := PR.FieldByName('CHECK_DATE').AsDateTime;
    end;

    Edit;
    FieldByName('PR_NBR').Value := PR['PR_NBR'];
    if FieldByName('SCHEDULED_CHECK_DATE').AsDateTime <> CheckDate then
      FieldByName('SCHEDULED_CHECK_DATE').AsDateTime := CheckDate;
    Post;
  end;
end;

function TevPayrollCalculation.GetMinimumWage(EE_STATES_NBR: Variant; EE_NBR: Variant): Real;
var
  EEStateNbr, COStateNbr, SYStateNbr: Integer;
  Temp: String;
  IndexWasLocked: Boolean;
  EEStateNbrVar: Variant;

  function GetCoStateNBR(EEStateNbr : integer) : integer;
  var qry : IevQuery;
  begin
    qry := TevQuery.Create('SELECT CO_STATES_NBR FROM EE_STATES a WHERE {AsOfNow<a>} AND a.EE_STATES_NBR=:EE_STATES_NBR');
    qry.Params.AddValue('EE_STATES_NBR',EEStateNbr);
    qry.Execute;
    Result := ConvertNull(qry.Result['CO_STATES_NBR'],0);
  end;

begin
  DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
  DM_SYSTEM_STATE.SY_STATES.Activate;
  Temp := DM_EMPLOYEE.EE_STATES.RetrieveCondition;
  IndexWasLocked := DM_EMPLOYEE.EE_STATES.IndexFieldNames <> '';
  DM_EMPLOYEE.EE_STATES.Activate;
  DM_EMPLOYEE.EE_STATES.CancelRange;
  DM_EMPLOYEE.EE_STATES.ResetLockedIndexFieldNames('');
  if not VarIsNull(EE_NBR) then
    DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR='+VarToStr(EE_NBR));

  DM_COMPANY.CO_STATES.Activate;
  if VarIsNull(EE_STATES_NBR) then
  begin
    // Use EE home state
    if not DM_EMPLOYEE.EE.Active then
      DM_EMPLOYEE.EE.DataRequired('ALL');

    if VarIsNull(EE_NBR) then
      EEStateNbrVar := DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'HOME_TAX_EE_STATES_NBR')
    else
      EEStateNbrVar := DM_EMPLOYEE.EE.NewLookup('EE_NBR', EE_NBR, 'HOME_TAX_EE_STATES_NBR');

    if not VarIsNull(EEStateNbrVar) then
    begin
      EEStateNbr := EEStateNbrVar;
      COStateNbr := GetCoStateNBR(EEStateNbr);
    end
    else
      raise EevException.Create('There is no home state set up for employee #' +
        DM_EMPLOYEE.EE.Lookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'CUSTOM_EMPLOYEE_NUMBER'));
  end
  else
  begin
    EEStateNbr := EE_STATES_NBR;
    COStateNbr :=  GetCoStateNBR(EEStateNbr);
  end;
  SYStateNbr := DM_SYSTEM_STATE.SY_STATES.NewLookup('STATE', DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', COStateNbr, 'STATE'), 'SY_STATES_NBR');
  if not VarIsNull(DM_EMPLOYEE.EE_STATES.NewLookup('EE_STATES_NBR', EEStateNbr, 'OVERRIDE_STATE_MINIMUM_WAGE')) then
    Result := DM_EMPLOYEE.EE_STATES.NewLookup('EE_STATES_NBR', EEStateNbr, 'OVERRIDE_STATE_MINIMUM_WAGE')
  else
  if not VarIsNull(DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE')) then
    Result := DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE')
  else
  Result := DM_SYSTEM_STATE.SY_STATES.NewLookup('SY_STATES_NBR', SYStateNbr, 'STATE_MINIMUM_WAGE');
  if Result < ConvertNull(DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE'), 0) then
    Result := DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE')
  else
  if Result < DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'] then
    Result := DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'];
  if IndexWasLocked or (DM_EMPLOYEE.EE_STATES.RetrieveCondition <> Temp) then
  begin
    DM_EMPLOYEE.EE_STATES.SetLockedIndexFieldNames('EE_NBR');
    DM_EMPLOYEE.EE_STATES.SetRange([DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger], [DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger]);
  end;
end;

procedure TevPayrollCalculation.DoOnConstruction;
begin
  inherited;

  FGenericGrossToNet := TevGenericGrossToNet.Create;

  cdstTempCheckLines := TevClientDataSet.Create(nil);
  cdstHomeDBDTFieldValues := TevClientDataSet.Create(nil);
  with cdstHomeDBDTFieldValues do
  begin
    with FieldDefs.AddFieldDef do
    begin
      DataType := ftInteger;
      Name := 'CL_NBR';
    end;
    with FieldDefs.AddFieldDef do
    begin
      DataType := ftInteger;
      Name := 'EE_NBR';
    end;
    with FieldDefs.AddFieldDef do
    begin
      DataType := ftString;
      Name := 'LEVEL';
      Size := 1;
    end;
    with FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      Name := 'OVERRIDE_PAY_RATE';
    end;
    with FieldDefs.AddFieldDef do
    begin
      DataType := ftInteger;
      Name := 'OVERRIDE_EE_RATE_NUMBER';
    end;
    with FieldDefs.AddFieldDef do
    begin
      DataType := ftInteger;
      Name := 'PAYROLL_CL_BANK_ACCOUNT_NBR';
    end;
    CreateDataSet;
  end;

  LastNewBatchNbr := 0;
  LinesToAdd := TLinesToAdd.Create;
  PriorPayrollEDs := TStringList.Create;

  FStateTax := 0;
end;

function TevPayrollCalculation.ReturnHistValue(aDataSet: TevClientDataSet; aFieldName: String): Variant;
begin
  if Assigned(DM_HISTORICAL_TAXES.FindComponent(aDataSet.Name)) and
  TevClientDataSet(DM_HISTORICAL_TAXES.FindComponent(aDataSet.Name)).Active and
  (DateToStr(TDM_HISTORICAL_TAXES(DM_HISTORICAL_TAXES).LastAsOfDate) = DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString) then
    Result := TevClientDataSet(DM_HISTORICAL_TAXES.FindComponent(aDataSet.Name)).FieldByName(aFieldName).Value
  else
  begin
    if DM_PAYROLL.PR.Active and not DM_PAYROLL.PR.FieldByName('CHECK_DATE').IsNull then
      aDataSet.AsOfDate := DM_PAYROLL.PR['CHECK_DATE']
    else
      aDataSet.AsOfDate := SysDate;

    ctx_DataAccess.OpenDataSets([aDataSet]);
    Result := aDataSet.FieldByName(aFieldName).Value;
    aDataSet.ClearAsOfDateOverride;
    aDataSet.Activate;
  end;
end;

function TevPayrollCalculation.DM_HISTORICAL_TAXES: TDataModule;
begin
  if not Assigned(FDM_HISTORICAL_TAXES) then
    FDM_HISTORICAL_TAXES := TDM_HISTORICAL_TAXES.Create(ctx_DataAccess.GetHolder);  // Will be destroyed automatically by DataAccess
  Result := FDM_HISTORICAL_TAXES;
end;

function TevPayrollCalculation.GetEeFilter: string;
begin
  Result := FEeFilter;
end;

function TevPayrollCalculation.GetEeFilterCaption: string;
begin
  Result := FEeFilterCaption;
end;

procedure TevPayrollCalculation.SetEeFilter(const AFilter, ACaption: string);
begin
  FEeFilter := AFilter;
  FEeFilterCaption := ACaption;
end;

function TevPayrollCalculation.GetLastFeedBack: string;
begin
  Result := FLastFeedBack;
end;

procedure TevPayrollCalculation.ProcessFeedback(const AShow: Boolean; const ADisplayString: String);
begin
  if AShow then
  begin
    if ADisplayString = '' then
    begin
      FLastFeedBack := 'Processing...' + #13#13 +'Company:  #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '  "' + DM_COMPANY.CO.FieldByName('NAME').AsString + '"';
      ctx_StartWait(FLastFeedBack);
    end
    else
    begin
      FLastFeedBack := ADisplayString + #13#13 +'Company:  #' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '  "' + DM_COMPANY.CO.FieldByName('NAME').AsString + '"';
      ctx_UpdateWait(FLastFeedBack);
    end;
  end
  else
  begin
    FLastFeedBack := '';
    ctx_EndWait;
  end;
end;

function TevPayrollCalculation.GetABANumber: string;
begin
  Result := FABANumber;
end;

function TevPayrollCalculation.GetFixedAmount(EE_SCHEDULED_E_DS, PR: TevClientDataset; DefaultValue: Variant): Real;
var
  cdEE_BENEFITS: TevClientDataSet;
  RateOk : Boolean;

  function GetRateBySubtype(var Rate: Real; const SubTypeNbr: Integer): Boolean;
  begin
    Result := False;
    DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString);
    if not DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], []) then
      raise Exception.Create('Company E/D code not found');

    if Assigned(PR) then
      RateOk := GetCoBenefitRateNumber(SubTypeNbr, PR.FieldByName('CHECK_DATE').AsDateTime, PR.FieldByName('CHECK_DATE').AsDateTime )
    else
      RateOk := DM_CLIENT.CO_BENEFIT_RATES.Locate('CO_BENEFIT_SUBTYPE_NBR', SubTypeNbr, []);

    if RateOk then
    begin
      if (DM_COMPANY.CO_E_D_CODES.EE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_ER)
      and not DM_CLIENT.CO_BENEFIT_RATES.FieldByName('ER_RATE').IsNull then
      begin
        Rate := DM_CLIENT.CO_BENEFIT_RATES['ER_RATE'];
        Result := True;
      end
      else if (DM_COMPANY.CO_E_D_CODES.EE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_EE)
      and not DM_CLIENT.CO_BENEFIT_RATES.FieldByName('EE_RATE').IsNull then
      begin
        Rate := DM_CLIENT.CO_BENEFIT_RATES['EE_RATE'];
        Result := True;
      end;
    end;
  end;

  function GetRateFromEeBenefits(var Rate: Real; EeBenefitsNbr: Integer): Boolean;
  begin
    Result := False;
    DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString);
    cdEE_BENEFITS.Locate('EE_BENEFITS_NBR', EE_SCHEDULED_E_DS['EE_BENEFITS_NBR'], []);
    if not DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', EE_SCHEDULED_E_DS['CL_E_DS_NBR'], []) then
      raise Exception.Create('Company E/D code not found');

    if (DM_COMPANY.CO_E_D_CODES.EE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_ER)
    and not cdEE_BENEFITS.FieldByName('ER_RATE').IsNull then
    begin
      Rate := cdEE_BENEFITS['ER_RATE'];
      Result := True;
    end
    else if (DM_COMPANY.CO_E_D_CODES.EE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_EE)
    and not cdEE_BENEFITS.FieldByName('EE_RATE').IsNull then
    begin
      Rate := cdEE_BENEFITS['EE_RATE'];
      Result := True;
    end;
  end;

var
  Found: Boolean;

begin
  Result := 0;
  Found := False;

  if not Found and not EE_SCHEDULED_E_DS.FieldByName('EE_BENEFITS_NBR').IsNull then
  begin
    cdEE_BENEFITS := TevClientDataSet.Create(nil);
    try
        if not DM_EMPLOYEE.EE_BENEFITS.Active then
           DM_EMPLOYEE.EE_BENEFITS.DataRequired('ALL');

        cdEE_BENEFITS.CloneCursor(DM_EMPLOYEE.EE_BENEFITS, True);

        Found := GetRateFromEeBenefits(Result, EE_SCHEDULED_E_DS.FieldByName('EE_BENEFITS_NBR').AsInteger);
        if not Found and not cdEE_BENEFITS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull then
          Found := GetRateBySubtype(Result, cdEE_BENEFITS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger);
    finally
      cdEE_BENEFITS.free;
    end;

  end;

  if not Found and not EE_SCHEDULED_E_DS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull then
  begin
    if not DM_CLIENT.CO_BENEFIT_RATES.Active then
       DM_CLIENT.CO_BENEFIT_RATES.DataRequired('ALL');
    Found := GetRateBySubtype(Result, EE_SCHEDULED_E_DS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger);
  end;

  if not Found then
    Result := ConvertNull(DefaultValue, 0);

end;

procedure TevPayrollCalculation.CalculateMultipleOvertime(
  DataSet: TevClientDataSet; EE_NBR: Integer);
var
  EDGroupNbr, RateNbr: Integer;
  Sum, Hours, BaseRate, RegRate, OTMinWage: Real;
//  Hrs: Real; // DC-219
  OvertimeMult: Double;
  EE_STATES_NBR: Variant;
  EDCodeType: String;
begin
  if DataSet.FieldByName('RATE_OF_PAY').IsNull and DataSet.FieldByName('HOURS_OR_PIECES').IsNull and not DataSet.FieldByName('AMOUNT').IsNull then
    Exit;
  Sum := 0;
  Hours := 0;
  EE_STATES_NBR := DataSet['EE_STATES_NBR'];
  if VarIsNull(EE_STATES_NBR) then
  begin
    if not DM_EMPLOYEE.EE.Active then
      DM_EMPLOYEE.EE_RATES.DataRequired('EE_NBR=' + IntToStr(EE_NBR));
    EE_STATES_NBR := DM_EMPLOYEE.EE.NewLookup('EE_NBR', EE_NBR, 'HOME_TAX_EE_STATES_NBR');
    if VarIsNull(EE_STATES_NBR) then
      raise EInconsistentData.CreateHelp('There is no home state set up for employee #' +
        DM_EMPLOYEE.EE.NewLookup('EE_NBR', EE_NBR, 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
  end;

  OTMinWage := GetMinimumWage(EE_STATES_NBR, Null);


  
  DM_CLIENT.CL_E_D_GROUP_CODES.Activate;
  try
    EDGroupNbr := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'OT_ALL_CL_E_D_GROUPS_NBR');
  except
    raise EInconsistentData.CreateHelp('E/D Group for Overtime Calculation is not setup for employee #' + DM_EMPLOYEE.EE.NewLookup('EE_NBR',
      DM_PAYROLL.PR_CHECK['EE_NBR'], 'CUSTOM_EMPLOYEE_NUMBER'), IDH_InconsistentData);
  end;
  ApplyTempCheckLinesCheckRange(DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
  cdstTempCheckLines.First;
  while not cdstTempCheckLines.EOF do
  begin
    if cdstTempCheckLines['PR_CHECK_LINES_NBR'] <> DataSet['PR_CHECK_LINES_NBR'] then
    begin
      EDCodeType := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
      if EDCodeType = ED_OEARN_AVG_MULT_OVERTIME then
      begin
        Hours := Hours + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0);
        BaseRate := ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * ConvertNull(GetTempCheckLinesFieldValue('RATE_OF_PAY'), 0);
        Sum := Sum + BaseRate;
      end
      else
      if not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR', VarArrayOf([EDGroupNbr, cdstTempCheckLines['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR')) then
      begin
/////////////// old DC-219
        Hours := Hours + ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0);
        if ConvertNull(GetTempCheckLinesFieldValue('RATE_OF_PAY'), 0) <> 0 then
          BaseRate := ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0) * OTMinWage
        else
          BaseRate := 0;
        if BaseRate < ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0) then
          BaseRate := ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
        Sum := Sum + BaseRate;
/////////////// old DC-219

(*     // this is a new code for DC-219
        Hrs := ConvertNull(GetTempCheckLinesFieldValue('HOURS_OR_PIECES'), 0);
        Hours := Hours + Hrs;
        Sum := Sum + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
*)
      end;
    end;
    cdstTempCheckLines.Next;
  end;

  if Hours = 0 then
    DataSet['AMOUNT'] := 0
  else
  begin
    BaseRate := 0;

    DM_CLIENT.CL_E_DS.Activate;
    cdstTempCheckLines.First;
    while not cdstTempCheckLines.EOF do
    begin
      if (DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', cdstTempCheckLines['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_OEARN_SALARY)
      and (not VarIsNull(DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_D_GROUPS_NBR;CL_E_DS_NBR',
      VarArrayOf([EDGroupNbr, cdstTempCheckLines['CL_E_DS_NBR']]), 'CL_E_D_GROUP_CODES_NBR'))) then
        BaseRate := BaseRate + ConvertNull(GetTempCheckLinesFieldValue('AMOUNT'), 0);
      cdstTempCheckLines.Next;
    end;
    if BaseRate <> 0 then
      BaseRate := 0
    else
    begin
      BaseRate := ConvertNull(DataSet['RATE_OF_PAY'], 0);
      if BaseRate = 0 then
      begin
        RateNbr := Abs(VarToInt(ConvertNull(DataSet['RATE_NUMBER'], 0)));
        if RateNbr = 0 then
          RateNbr := GetEEPrimaryRate(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_RATES, True);
        BaseRate := DM_EMPLOYEE.EE_RATES.NewLookup('EE_NBR;RATE_NUMBER', VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'], RateNbr]), 'RATE_AMOUNT');
      end;
    end;

    if DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'OVERSTATE_HOURS_FOR_OVERTIME') = 'N' then
      Hours := Hours + ConvertNull(DataSet['HOURS_OR_PIECES'], 0);
    if VarIsNull(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'REGULAR_OT_RATE')) then
      raise EInconsistentData.CreateHelp('Overtime rate multiplier is missing for ' + DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR',
        DataSet['CL_E_DS_NBR'], 'CUSTOM_E_D_CODE_NUMBER'), IDH_InconsistentData);
    OvertimeMult := DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'REGULAR_OT_RATE');
    if OvertimeMult > 1.0000001 then
      OvertimeMult := OvertimeMult - 1
    else
      BaseRate := 0;
    if Hours = 0 then
      DataSet['AMOUNT'] := 0
    else
    begin
      if (OTMinWage > BaseRate) and (BaseRate <> 0) then
        RegRate := OTMinWage
      else
        RegRate := BaseRate;
      RegRate := RoundTwo((Sum + ConvertNull(DataSet['HOURS_OR_PIECES'], 0) * RegRate) / Hours);
      if OTMinWage > RegRate then
        RegRate := OTMinWage;
      DataSet['AMOUNT'] := (RoundTwo(RegRate * OvertimeMult) + BaseRate) * ConvertNull(DataSet.FieldByName('HOURS_OR_PIECES').Value, 0);
    end;
  end;
end;

function TevPayrollCalculation.GetCoMinimumWage(CO_STATES_NBR,
  EE_NBR: Variant): Real;
var
  SYStateNbr: Integer;
  Temp: String;
  IndexWasLocked: Boolean;
begin
  DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
  DM_SYSTEM_STATE.SY_STATES.Activate;
  Temp := DM_EMPLOYEE.EE_STATES.RetrieveCondition;
  IndexWasLocked := DM_EMPLOYEE.EE_STATES.IndexFieldNames <> '';

  DM_COMPANY.CO_STATES.Activate;
  SYStateNbr := DM_SYSTEM_STATE.SY_STATES.NewLookup('STATE', DM_COMPANY.CO_STATES.NewLookup('CO_STATES_NBR', CO_STATES_NBR, 'STATE'), 'SY_STATES_NBR');
  if not VarIsNull(DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE')) then
    Result := DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE')
  else
  Result := DM_SYSTEM_STATE.SY_STATES.NewLookup('SY_STATES_NBR', SYStateNbr, 'STATE_MINIMUM_WAGE');
  if Result < ConvertNull(DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE'), 0) then
    Result := DM_EMPLOYEE.EE.NewLookup('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], 'OVERRIDE_FEDERAL_MINIMUM_WAGE')
  else
  if Result < DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'] then
    Result := DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE['FEDERAL_MINIMUM_WAGE'];
  if IndexWasLocked or (DM_EMPLOYEE.EE_STATES.RetrieveCondition <> Temp) then
  begin
    DM_EMPLOYEE.EE_STATES.SetLockedIndexFieldNames('EE_NBR');
    DM_EMPLOYEE.EE_STATES.SetRange([DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger], [DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger]);
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPayrollCalculation, IevPayrollCalculation, 'Payroll Calculations');

end.


