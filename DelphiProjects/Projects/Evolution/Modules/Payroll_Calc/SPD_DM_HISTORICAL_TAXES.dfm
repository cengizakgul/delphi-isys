object DM_HISTORICAL_TAXES: TDM_HISTORICAL_TAXES
  OldCreateOrder = True
  OnCreate = DM_HISTORICAL_TAXESCreate
  Left = 399
  Top = 172
  Height = 477
  Width = 545
  object SY_FED_EXEMPTIONS: TevClientDataSet
    Left = 152
    Top = 28
  end
  object SY_FED_TAX_TABLE_BRACKETS: TevClientDataSet
    Left = 272
    Top = 12
  end
  object SY_FED_TAX_TABLE: TevClientDataSet
    Left = 40
    Top = 12
  end
  object SY_STATES: TevClientDataSet
    Left = 36
    Top = 92
  end
  object SY_STATE_MARITAL_STATUS: TevClientDataSet
    Left = 188
    Top = 92
  end
  object SY_STATE_TAX_CHART: TevClientDataSet
    Left = 280
    Top = 108
  end
  object SY_STATE_EXEMPTIONS: TevClientDataSet
    Left = 88
    Top = 108
  end
  object SY_SUI: TevClientDataSet
    Left = 320
    Top = 88
  end
  object SY_LOCALS: TevClientDataSet
    Left = 32
    Top = 172
  end
  object SY_LOCAL_MARITAL_STATUS: TevClientDataSet
    Left = 192
    Top = 168
  end
  object SY_LOCAL_TAX_CHART: TevClientDataSet
    Left = 288
    Top = 184
  end
  object SY_LOCAL_EXEMPTIONS: TevClientDataSet
    Left = 92
    Top = 188
  end
  object CL_E_D_STATE_EXMPT_EXCLD: TevClientDataSet
    Left = 144
    Top = 288
  end
  object CL_E_DS: TevClientDataSet
    Left = 32
    Top = 272
  end
  object CL_E_D_LOCAL_EXMPT_EXCLD: TevClientDataSet
    Left = 264
    Top = 272
  end
  object CO_SUI: TevClientDataSet
    Left = 368
    Top = 280
  end
  object CO_LOCAL_TAX: TevClientDataSet
    Left = 368
    Top = 336
  end
  object EE_LOCALS: TevClientDataSet
    Left = 432
    Top = 216
  end
  object EE: TevClientDataSet
    Left = 432
    Top = 96
  end
  object EE_STATES: TevClientDataSet
    Left = 432
    Top = 160
  end
  object CO_STATES: TevClientDataSet
    Left = 368
    Top = 224
  end
end
