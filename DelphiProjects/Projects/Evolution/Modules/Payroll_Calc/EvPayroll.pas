unit EvPayroll;

interface

uses
  Classes,  DB, dialogs, windows, EvTypes,  Variants,
  EvBasicUtils, EvContext, EvExceptions, EvClientDataSet,
  isBaseClasses, EvCommonInterfaces, SysUtils, EvDataSet,
  EvUtils, EvConsts, DateUtils, Types;

const
  NJ_EE_SDI_SY_SUI_NBR = 89;
  NJ_ER_SDI_SY_SUI_NBR = 90;
  MA_ER_MED_SY_SUI_NBR = 58;
  NV_BUSINESS_SY_SUI_NBR = 118;
  NV_BUSINESS_FINANCIAL_SY_SUI_NBR = 223;
  IL_SUI_SY_SUI_NBR = 15;
  IL_FUND_BUILDING_SY_SUI_NBR = 83;

const
  NJ_NEWARK_PAYROLL_TAX = 118;  

type

  IevLine = interface
  ['{DF6A3D3E-BC70-44C7-BD11-CE7D0713CF61}']
    function GetClone: IInterface;
    function GetSuiState: String;
    function GetEDCode: String;
    function GetClEdNbr: Integer;
    function GetAmount: Double;
    procedure SetAmount(const Value: Double);
    function GetValue: Double;
    function GetCheckValue: Double;
    function GetNbr: Integer;
    function GetState: String;
    function GetSdiState: String;
    function Taxable: Boolean;
    function SystemLevelFederalExempt: Boolean;
    function ClientLevelFederalExempt: Boolean;
    function SystemLevelEeMedicareExempt: Boolean;
    function ClientLevelEeMedicareExempt: Boolean;
    function SystemLevelErMedicareExempt: Boolean;
    function ClientLevelErMedicareExempt: Boolean;
    function SystemLevelEeOasdiExempt: Boolean;
    function ClientLevelEeOasdiExempt: Boolean;
    function SystemLevelErOasdiExempt: Boolean;
    function ClientLevelErOasdiExempt: Boolean;
    function SystemLevelStateExempt: Boolean;
    function ClientLevelStateExempt: Boolean;
    function SystemLevelEeSdiExempt: Boolean;
    function ClientLevelEeSdiExempt: Boolean;

    function GetDivisionNbr: Integer;
    function GetBranchNbr: Integer;
    function GetDepartmentNbr: Integer;
    function GetTeamNbr: Integer;
    function GetJobNbr: Integer;

    function IsEarning: Boolean;
    function IsDeduction: Boolean;

    function SystemLevelFuiExempt: Boolean;
    function ClientLevelFuiExempt: Boolean;

    property Amount: Double read GetAmount write SetAmount;
    property Value: Double read GetValue;
    property CheckValue: Double read GetCheckValue;
    property Nbr: Integer read GetNbr;
    property EDCode: String read GetEDCode;
    property State: String read GetState;
    property SdiState: String read GetSdiState;
    property SuiState: String read GetSuiState;
    property ClEdNbr: Integer read GetClEdNbr;
    property DivisionNbr: Integer read GetDivisionNbr;
    property BranchNbr: Integer read GetBranchNbr;
    property DepartmentNbr: Integer read GetDepartmentNbr;
    property TeamNbr: Integer read GetTeamNbr;
    property JobNbr: Integer read GetJobNbr;
  end;

  IevStateTax = interface
  ['{3FCD5471-CC61-44BD-A634-006A66E4D4BB}']
    function GetState: String;
    function GetStateTax: Double;
    function GetEeSdiTax: Double;
    function GetStateTaxableWages: Double;
    function GetStateGrossWages: Double;
    function GetEeSdiTaxableWages: Double;
    function GetEeSdiGrossWages: Double;
    function GetYtdEeSdiTaxableWages: Double;
    procedure SetYtdEeSdiTaxableWages(const Value: Double);
    property State: String read GetState;
    property StateTaxableWages: Double read GetStateTaxableWages;
    property StateGrossWages: Double read GetStateGrossWages;
    property StateTax: Double read GetStateTax;
    property EeSdiTax: Double read GetEeSdiTax;
    property EeSdiTaxableWages: Double read GetEeSdiTaxableWages;
    property EeSdiGrossWages: Double read GetEeSdiGrossWages;
    property YtdEeSdiTaxableWages: Double read GetYtdEeSdiTaxableWages write SetYtdEeSdiTaxableWages;
  end;

  IevSuiTax = interface
  ['{AA3A0087-43A8-4DF3-AAC7-3FACB0881A86}']
    function GetName: String;
    function GetSySuiNbr: Integer;
    function GetTax: Double;
    function GetTaxWages: Double;
    function GetGrossWages: Double;
    function IsEmployeeTax: Boolean;
    function IsEmployerTax: Boolean;
    property Tax: Double read GetTax;
    property TaxWages: Double read GetTaxWages;
    property GrossWages: Double read GetGrossWages;
    property SySuiNbr: Integer read GetSySuiNbr;
    property Name: String read GetName;
  end;

  IevLocalTax = interface
  ['{5A8F195A-7D5A-4715-A572-FEC2E3A109BE}']
    function GetTax: Double;
    function GetState: String;
    property State: String read GetState;
    property Tax: Double read GetTax;
  end;

  IevCompany = interface
  ['{214B0C9A-D50D-40C1-9B29-C6FC972ACE37}']
    function CoNbr: Integer;
    function CoCombineStateSui(const ACoNbr: Integer): Boolean;
  end;

  IevCheck = interface
  ['{40AF33D2-5655-4ACD-8ECC-083B680836F5}']
    procedure RestoreOriginalCheckLines;
    procedure AddSui(const ASySuiNbr: Integer; GrossWages, TaxWages, Tax: Double);
    function GetReciprocateSui: Boolean;
    procedure SetFederalGrossWages(const Value: Double);
    function GetFederalGrossWages: Double;
    function GetEeOasdiGrossWages: Double;
    procedure SetEeOasdiGrossWages(const Value: Double);
    function GetErOasdiGrossWages: Double;
    procedure SetErOasdiGrossWages(const Value: Double);
    function GetEeOasdiTaxableWages: Double;
    procedure SetEeOasdiTaxableWages( const Value: Double);
    function GetErOasdiTaxableWages: Double;
    procedure SetErOasdiTaxableWages(const Value: Double);
    function GetEeOasdiTax: Double;
    procedure SetEeOasdiTax(const Value: Double);
    function GetErOasdiTax: Double;
    procedure SetErOasdiTax(const Value: Double);
    function CalcEeOasdiWages: Double;
    function CalcErOasdiWages: Double;
    function CalcEeOasdiTips: Double;
    function CalcErOasdiTips: Double;
    function EeNbr: Integer;
    function CheckNbr: Integer;
    function GetEarning(Index: Integer): IevLine;
    function GetDeduction(Index: Integer): IevLine;
    function EarningsCount: Integer;
    function DeductionsCount: Integer;
    function LinesCount: Integer;
    function GetLine(Index: Integer): IevLine;
    function StateTaxCount: Integer;
    function GetStateTaxes(Index: Integer): IevStateTax;
    function GetStateTax(Index: String): IevStateTax;
    function SuiCount: Integer;
    function GetSuiTax(Index: Integer): IevSuiTax;
    function GetLocalTax(Index: Integer): IevLocalTax;
    function LocalsCount: Integer;
    function FederalTax: Double;
    function EeMedicareTax: Double;
    function EeMedicareGrossWages: Double;
    function EeMedicareTaxableWages: Double;
    function CalcEeMedicareGrossWages: Double;
    function CalcEeMedicareTaxableWages(const AYTDWages:Double): Double;
    function CalcEeMedicareTax(const AYTDWage:Double): Double;
    function ErMedicareTax: Double;
    function ErMedicareGrossWages: Double;
    function ErMedicareTaxableWages: Double;
    function CalcErMedicareGrossWages: Double;
    function CalcErMedicareTaxableWages(const AYTDWages:Double): Double;
    function CalcErMedicareTax(const AYTDWage:Double): Double;

    function FuiTax: Double;
    function FuiGrossWages: Double;
    function FuiTaxableWages: Double;
    function CalcFuiTaxableWages(const AYTDWages: Double): Double;
    function CalcFuiGrossWages: Double;
    function CalcFuiTax(const AYTDWage:Double): Double;

    function EeEICTax: Double;
    function BackUpWithholding: Double;
    function NetAmount: Double;
    function NetWages: Double;
    function CalcFederalTaxableWages: Double;
    function CalcEeSdiTaxableWages(const State: String; const CalcYtdEeSdiTaxableWages: Double): Double;
    function GetEeSdiTaxableWages(const State: String): Double;
    function CalcEeSdiGrossWages(const State: String): Double;
    function GetEeSdiGrossWages(const State: String): Double;
    function CalcEeSdiTax(const State: String; const CalcYTDTaxWages: Double): Double;
    function CalcStateTaxableWages(const State: String): Double;
    function GetStateTaxableWages(const State: String): Double;
    function GetStateGrossWages(const State: String): Double;
    function CalcStateGrossWages(const State: String): Double;
    function GetFederalTaxableWages: Double;
    procedure SetFederalTaxableWages(const Value: Double);
    function GetEeOasdiTips: Double;
    procedure SetEeOasdiTips(const Value: Double);
    function GetErOasdiTips: Double;
    procedure SetErOasdiTips(const Value: Double);
    function GetSerialNumber: Integer;
    function CheckDate: TDateTime;
    function CoNbr: Integer;
    function PrNbr: Integer;
    function GetCompany: IevCompany;
    property Lines[Index: Integer]: IevLine read GetLine;
    property Earnings[Index: Integer]: IevLine read GetEarning;
    property Deductions[Index: Integer]: IevLine read GetDeduction;
    property SuiTaxes[Index: Integer]: IevSuiTax read GetSuiTax;
    property LocalTaxes[Index: Integer]: IevLocalTax read GetLocalTax;
    property StateTaxes[Index: Integer]: IevStateTax read GetStateTaxes;
    property StateTax[Index: String]: IevStateTax read GetStateTax;
    property SerialNumber: Integer read GetSerialNumber;
    property FederalTaxableWages: Double read GetFederalTaxableWages write SetFederalTaxableWages;
    property FederalGrossWages: Double read GetFederalGrossWages write SetFederalGrossWages;
    property EeOasdiTaxableWages: Double read GetEeOasdiTaxableWages write SetEeOasdiTaxableWages;
    property EeOasdiTips: Double read GetEeOasdiTips write SetEeOasdiTips;
    property EeOasdiGrossWages: Double read GetEeOasdiGrossWages write SetEeOasdiGrossWages;
    property ErOasdiTaxableWages: Double read GetErOasdiTaxableWages write SetErOasdiTaxableWages;
    property ErOasdiTips: Double read GetErOasdiTips write SetErOasdiTips;
    property EeOasdiTax: Double read GetEeOasdiTax write SetEeOasdiTax;
    property ErOasdiGrossWages: Double read GetErOasdiGrossWages write SetErOasdiGrossWages;
    property ErOasdiTax: Double read GetErOasdiTax write SetErOasdiTax;
    property ReciprocateSui: Boolean read GetReciprocateSui;
    property Company: IevCompany read GetCompany;
  end;

  IevPerson = interface
  ['{A47F1698-3CEE-4AF3-9267-B3B55DCEA223}']
    procedure RedistributeNegativeCheckLines(const CheckDate: TDateTime);
    procedure RestoreOriginalCheckLines;
    function GetHtml: IisStringList;
    function GetSuiList(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): IisListOfValues;
    function StatesCount(const AEeNbr: Integer): Integer;
    function CalcEeSdiTax(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function EeSdiTax(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeSdiTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function EeSdiTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeSdiGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function EeSdiGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcStateTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function StateTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcStateGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function StateGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcFederalTaxableWages(const AEeNbr: Integer): Double;
    function FederalTaxableWages(const AEeNbr: Integer): Double;
    function EeOasdiTaxWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeOasdiTaxTips(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeOasdiTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeOasdiTaxWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeOasdiTaxTips(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function ErMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function ErMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function ErMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcErMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcErMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcErMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function FuiGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function FuiTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function FuiTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcFuiGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcFuiTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcFuiTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function SuiGrossWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function SuiTaxableWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function SuiTax(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcSuiGrossWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcSuiTaxableWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime; const DebugList: IisParamsCollection = nil): Double;
    function CalcSuiTax(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function GetCheck(Index: Integer): IevCheck;
    function GetQecCheck(EeNbr: Integer): IevCheck;
    function CoNbr(EeNbr: Integer): Integer;
    function CustomEmployeeNumber(EeNbr: Integer): String;
    function CheckCount: Integer;
    function GetState(EeNbr, Index: Integer): String;
    function EeHasState(const EeNbr: Integer; const State: String): Boolean;
    function ClPersonNbr: Integer;
    function GetEeNbr(Index: Integer): Integer;
    function EeCount: Integer;
    function HasEeNbr(const AEeNbr: Integer): Boolean;
    property Checks[Index: Integer]: IevCheck read GetCheck;
    property State[EeNbr: Integer; Index: Integer]: String read GetState;
    property EeNbr[Index: Integer]: Integer read GetEeNbr;
    property QecCheck[EeNbr: Integer]: IevCheck read GetQecCheck;
  end;

  IevHistData = interface
  ['{2E3FE06A-64A8-4861-8194-52DB7C35B563}']
    function CoPayFrequencies(const CoNbr: Integer; const CheckDate: TDateTime): Char;
    function ClReciprocateSui(const CheckDate: TDateTime = 0): Boolean;
    function GetCheck(const ACheckNbr: Integer): IevCheck;
    function GetPerson(const AEeNbr: Integer; const CheckDate: TDateTime): IevPerson;
    function IsEmployeeSui(const ASySuiNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEmployeeLocalTax(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalState(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function SyLocalType(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function SyLocalName(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function CoLocalState(const ACoLocalsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function CoSyLocalsNbr(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function CoStateState(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): String;
    function IsCoStateExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean; overload;
    function IsCoStateExempt(const ACoNbr: Integer; const AState: String; const CheckDate: TDateTime = 0): Boolean; overload;
    function IsCoStateEeSdiExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoStateErSdiExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoStateSuiExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function GetCoStatesNbr(const ACoNbr: Integer; const State: String; const CheckDate: TDateTime = 0): Integer;
    function ClEdCodeType(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function ClEdDescription(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function IsClEdFederalExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdEeOasdiExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdErOasdiExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdEeMedicareExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdErMedicareExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdErFuiExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExempt(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptEeOasdi(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptErOasdi(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptEeMedicare(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptErMedicare(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFuiExempt(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFuiRateOverride(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function CoFuiRateOverride(const CoNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function GetEeCoStatesNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function IsEdCodeClStateExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClEeSdiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeFederalExempt(const EdCode: String; const CheckDate: TDateTime = 0): Boolean;
    function IsEdCodeEeOasdiExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeErOasdiExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeSyLocalExempt(const EdCode: String; const SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function IsEdCodeEeMedicareExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeErMedicareExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeFuiExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeStateExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean; overload;
    function IsEdCodeStateExempt(const AState: String; const EdCode: String; const CheckDate: TDateTime): Boolean; overload;
    function IsEdCodeEeSdiExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsCoSySuiInactive(const CoNbr, SySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function IsCoSuiInactive(const CoSuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CoSySuiRate(const CoNbr, SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function GetCoSuiNbr(const ACoNbr, SySuiNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function IsErSui(const SySuiNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeFederalExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeEeOasdiExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeErOasdiExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeEeMedicareExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeErMedicareExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeErFuiExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeFuiRateCreditOverride(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function EeFuiRateCreditOverride(const EeNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function EeHomeEeStateNbr(const EeNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function CoSuiTaxReturnCode(const CoSuiNbr: Integer; const CheckDate: TDateTime = 0): String;
    function CoSuiReimburser(const CoSuiNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SySuiMaxWage(const SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function GetCoSySuiNbr(const CoSuiNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function SySuiFillerMaxWage(const SySuiNbr: Integer; const DefValue: DOuble; const CheckDate: TDateTime): Double;
    function CoSuiMaxWage(const CoSuiNbr: Integer; const CheckDate: TDateTime): Double;
    function CoSuiRate(const CoSuiNbr: Integer; const CheckDate: TDateTime): Double;
    function IsSySuiInactive(const SySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CalcCompanySuiRate(const CoNbr, SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function GetSuiEmployeeCount(const CoNbr, SySuiNbr: Integer; const EndDate: TDateTime): Integer;
    function GetCoSuiCoStatesNbr(const CoSuiNbr: Integer; const CheckDate: TDateTime): Integer;
    function IsEeStateExempt(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Boolean;
    function EeStateEeSdiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function GetEeStateCoStateNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeStateSdiApplyCoStateNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeStatesNbr(const AEeNbr, ACoStateNbr: Integer; const CheckDate: TDateTime): Integer; overload;
    function GetEeStatesNbr(const AEeNbr: Integer; const AState: String; const CheckDate: TDateTime): Integer; overload;
    function OasdiWageLimit(const CheckDate: TDateTime): Double;
    function EeCoNbr(const EeNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function GetSyStateNbr(const State: String; const CheckDate: TDateTime): Integer;
    function MedicareWageLimit(const CheckDate: TDateTime): Double;
    function MedicareRate(const CheckDate: TDateTime): Double;
    function MedicareThresholdValue(const CheckDate: TDateTime): Double;
    function MedicareThresholdRate(const CheckDate: TDateTime): Double;
    function IsCoLocalExempt(const CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeLocalExempt(const EeLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function GetEeLocalCoLocalTaxNbr(const EeLocalsNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function JobHasLocalTax(const CoJobsNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function TeamHasLocalTax(const CoTeamNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function DepartmentHasLocalTax(const CoDepartmentNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function BranchHasLocalTax(const CoBranchNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function DivisionHasLocalTax(const CoDivisionNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalFollowStateEdExempt(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalCalcMethod(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Char;
    function SyLocalMonthlyMaximum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalWageMaximum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalWageMinimum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalRate(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalTaxMinimum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalHasTaxMinimum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalQuarterlyMinimumThreshold(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function SyLocalMiscAmount(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function SyLocalMinimumWage(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function EeOasdiRate(const CheckDate: TDateTime): Double;
    function ErOasdiRate(const CheckDate: TDateTime): Double;
    function FuiWageLimit(const CheckDate: TDateTime): Double;
    function FuiRateReal(const CheckDate: TDateTime): Double;
    function FuiRateCredit(const CheckDate: TDateTime): Double;
    function IsEdCodeStateExemptEmployerSdi(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function SyStatesErSdiRate(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesEeSdiRate(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesWeeklySdiTaxCap(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesErSdiMaximumWage(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesEeSdiMaximumWage(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function IsEdCodeEeSuiExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeErSuiExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClErSdiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClEeSuiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClErSuiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function EeStateNumberWithholdingAllow(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SuiType(const SySuiNbr: Integer; const CheckDate: TDateTime): String;
    function SuiTaxName(const SySuiNbr: Integer; const CheckDate: TDateTime): String;
    function SySuiStateNbr(const SySuiNbr: Integer; const CheckDate: TDateTime): Integer;
    function TotalSuiTaxableWages(const CoNbr: Integer; const SySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function SuiBasedOnHours(const SySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function SuiGlobalRate(const SySuiNbr: Integer; const CheckDate: TDateTime): Variant;
    function SuiAdditionalAssessmentRate(const SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function SuiAdditionalAssessmentAmount(const SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesSuiReciprocate(const SyStatesNbr: Integer; const CheckDate: TDateTime): String;
    function ReciprocateSui(const SySuiNbr, SySuiNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoLocalSelfAdjustTax(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CoLocalTaxNbr(const CoNbr, SyLocalsNbr: Integer; const CheckDate: TDateTime): Integer;
    function CoLocalTaxRate(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Variant;
    function IsClEdLocalExempt(const ClEdsNbr, SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function IsClEdLocalInclude(const ClEdsNbr, SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function GetEeStatesErSdiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function GetEeStatesEeSdiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function GetEeStatesErSuiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function EeStateEeSuiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function EeStatesForSui(const EeNbr, CoStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeStateSuiApplyCoStateNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function EeLocalIncludeInPretax(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function EeLocalDeduct(const EeLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function EeLocalExemptExclude(const EeLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function EeLocalPercentageOfTaxWages(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function EeLocalOverrideLocalTaxType(const EeLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function EeLocalOverrideLocalTaxValue(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function EeLocalMiscellaneousAmount(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function GetEeLocalsNbr(const EeNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Integer;
    function CoCommonPaymaster(const CoNbr: Integer;const CheckDate: TDateTime = 0): Variant;
    function CombineOASDI(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CombineStateSui(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CombineMedicare(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CombineFui(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineOASDI(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineStateSui(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineMedicare(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineFui(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function IgnoreFICA(const EeNbr: Integer; const CheckDate: TDateTime): Boolean;
  end;

  TevHistData = class(TisInterfacedObject, IevHistData)
  private
    FClient: IisParamsCollection;
    FSui: IisParamsCollection;
    FLocals: IisParamsCollection;
    FCoLocalTax: IisParamsCollection;
    FCoLocalTax1: IisListOfValues;
    FCoStates: IisParamsCollection;
    FCoStates1: IisParamsCollection;
    FClEds: IisParamsCollection;
    FClEdsLocalsExemptions: IisParamsCollection;
    FCompanies: IisParamsCollection;
    FEeStates: IisParamsCollection;
    FEeStates1: IisParamsCollection;
    FEeStates2: IisParamsCollection;
    FEdCodes: IisParamsCollection;
    FCoSui: IisParamsCollection;
    FCoSui1: IisParamsCollection;
    FSySui: IisParamsCollection;
    FEe: IisParamsCollection;
    FStateExempt: IisParamsCollection;
    FFedTaxTable: IisListOfValues;
    FSyStates: IisListOfValues;
    FStates: IisParamsCollection;
    FEeLocals: IisParamsCollection;
    FEeLocals1: IisListOfValues;
    FDivision: IisParamsCollection;
    FBranch: IisParamsCollection;
    FDepartment: IisParamsCollection;
    FTeam: IisParamsCollection;
    FJobs: IisParamsCollection;
    FSyLocalExempt: IisParamsCollection;
    FPeople: IisListOfValues;
    FSuiEmployeeCount: IisListOfValues;
    FSuiTotalWages: IisListOfValues;
    procedure LoadSyLocals(const ASyLocalsNbr: Integer; const CheckDate: TDateTime);
  protected
    function TotalSuiTaxableWages(const CoNbr: Integer; const SySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function IgnoreFICA(const EeNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CoCommonPaymaster(const CoNbr: Integer;const CheckDate: TDateTime = 0): Variant;
    function CoPayFrequencies(const CoNbr: Integer; const CheckDate: TDateTime): Char;
    function CombineOASDI(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CombineStateSui(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CombineMedicare(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CombineFui(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineOASDI(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineStateSui(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineMedicare(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function CoCombineFui(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function ClReciprocateSui(const CheckDate: TDateTime = 0): Boolean;
    function GetCheck(const ACheckNbr: Integer): IevCheck;
    function GetPerson(const AEeNbr: Integer; const CheckDate: TDateTime): IevPerson;
    function IsEmployeeSui(const ASySuiNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEmployeeLocalTax(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalState(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function CoLocalTaxRate(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Variant;
    function CoLocalSelfAdjustTax(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
    function SyLocalType(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function SyLocalName(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function CoLocalState(const ACoLocalsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function CoSyLocalsNbr(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function CoStateState(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): String;
    function IsCoStateExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean; overload;
    function IsCoStateExempt(const ACoNbr: Integer; const AState: String; const CheckDate: TDateTime = 0): Boolean; overload;
    function IsCoStateEeSdiExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoStateErSdiExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoStateSuiExempt(const ACoStatesNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function GetCoStatesNbr(const ACoNbr: Integer; const State: String; const CheckDate: TDateTime = 0): Integer;
    function EeOasdiRate(const CheckDate: TDateTime): Double;
    function ErOasdiRate(const CheckDate: TDateTime): Double;
    function FuiWageLimit(const CheckDate: TDateTime): Double;
    function FuiRateReal(const CheckDate: TDateTime): Double;
    function FuiRateCredit(const CheckDate: TDateTime): Double;
    function OasdiWageLimit(const CheckDate: TDateTime): Double;
    function ClEdCodeType(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): String;
//    function GetClEdNbr(const EdCode: String; const CheckDate: TDateTime = 0): Integer;
    function ClEdDescription(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): String;
    function IsClEdFederalExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdEeOasdiExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdErOasdiExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdEeMedicareExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdErMedicareExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsClEdErFuiExempt(const ClEdsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExempt(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptEeOasdi(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptErOasdi(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptEeMedicare(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFederalExemptErMedicare(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFuiExempt(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsCoFuiRateOverride(const CoNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function CoFuiRateOverride(const CoNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function GetEeCoStatesNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function GetEeStatesErSdiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function GetEeStatesEeSdiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function GetEeStatesErSuiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function IsEdCodeFederalExempt(const EdCode: String; const CheckDate: TDateTime = 0): Boolean;
    function IsEdCodeEeOasdiExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeErOasdiExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeSyLocalExempt(const EdCode: String; const SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function IsEdCodeEeMedicareExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeErMedicareExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeFuiExempt(const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeStateExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean; overload;
    function IsEdCodeStateExempt(const AState: String; const EdCode: String; const CheckDate: TDateTime): Boolean; overload;
    function IsEdCodeEeSdiExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeEeSuiExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeErSuiExempt(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function IsCoSySuiInactive(const CoNbr, SySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function IsCoSuiInactive(const CoSuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CoSySuiRate(const CoNbr, SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function GetCoSuiNbr(const ACoNbr, SySuiNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function IsErSui(const SySuiNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SuiType(const SySuiNbr: Integer; const CheckDate: TDateTime): String;
    function SuiTaxName(const SySuiNbr: Integer; const CheckDate: TDateTime): String;
    function SuiAdditionalAssessmentRate(const SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function SuiAdditionalAssessmentAmount(const SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function SuiBasedOnHours(const SySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function SuiGlobalRate(const SySuiNbr: Integer; const CheckDate: TDateTime): Variant;
    function IsEeFederalExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeEeOasdiExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeErOasdiExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeEeMedicareExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeErMedicareExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeErFuiExempt(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeFuiRateCreditOverride(const EeNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function EeFuiRateCreditOverride(const EeNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function EeHomeEeStateNbr(const EeNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function EeCoNbr(const EeNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function CoSuiTaxReturnCode(const CoSuiNbr: Integer; const CheckDate: TDateTime = 0): String;
    function CoSuiReimburser(const CoSuiNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SySuiMaxWage(const SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function GetCoSySuiNbr(const CoSuiNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function SySuiFillerMaxWage(const SySuiNbr: Integer; const DefValue: DOuble; const CheckDate: TDateTime): Double;
    function CoSuiMaxWage(const CoSuiNbr: Integer; const CheckDate: TDateTime): Double;
    function CoSuiRate(const CoSuiNbr: Integer; const CheckDate: TDateTime): Double;
    function IsSySuiInactive(const SySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
    function CalcCompanySuiRate(const CoNbr, SySuiNbr: Integer; const CheckDate: TDateTime): Double;
    function GetSuiEmployeeCount(const CoNbr, SySuiNbr: Integer; const EndDate: TDateTime): Integer;
    function GetCoSuiCoStatesNbr(const CoSuiNbr: Integer; const CheckDate: TDateTime): Integer;
    function SySuiStateNbr(const SySuiNbr: Integer; const CheckDate: TDateTime): Integer;
    function IsEeStateExempt(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Boolean;
    function EeStateNumberWithholdingAllow(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function IsEdCodeStateExemptEmployerSdi(const SyStatesNbr: Integer; const EdCode: String; const CheckDate: TDateTime): Boolean;
    function EeStateEeSdiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function EeStateEeSuiExemptExclude(const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
    function GetEeStateCoStateNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeStateSdiApplyCoStateNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeStateSuiApplyCoStateNbr(const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeStatesNbr(const AEeNbr, ACoStateNbr: Integer; const CheckDate: TDateTime): Integer; overload;
    function GetEeStatesNbr(const AEeNbr: Integer; const AState: String; const CheckDate: TDateTime): Integer; overload;
    function GetSyStateNbr(const State: String; const CheckDate: TDateTime): Integer;
    function ClEmployeeExemptExcludeState(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): String;
    function IsEdCodeClStateExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClEeSdiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClEeSuiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClErSuiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function IsEdCodeClErSdiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
    function MedicareWageLimit(const CheckDate: TDateTime): Double;
    function MedicareRate(const CheckDate: TDateTime): Double;
    function MedicareThresholdValue(const CheckDate: TDateTime): Double;
    function MedicareThresholdRate(const CheckDate: TDateTime): Double;
    function IsCoLocalExempt(const CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function IsEeLocalExempt(const EeLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function EeLocalIncludeInPretax(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function EeLocalDeduct(const EeLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function EeLocalExemptExclude(const EeLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function EeLocalOverrideLocalTaxType(const EeLocalsNbr: Integer; const CheckDate: TDateTime): String;
    function EeLocalOverrideLocalTaxValue(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function EeLocalMiscellaneousAmount(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function EeLocalPercentageOfTaxWages(const EeLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesSuiReciprocate(const SyStatesNbr: Integer; const CheckDate: TDateTime): String;
    function ReciprocateSui(const SySuiNbr, SySuiNbr1: Integer; const CheckDate: TDateTime): Boolean;
    function GetEeLocalCoLocalTaxNbr(const EeLocalsNbr: Integer; const CheckDate: TDateTime = 0): Integer;
    function JobHasLocalTax(const CoJobsNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function TeamHasLocalTax(const CoTeamNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function DepartmentHasLocalTax(const CoDepartmentNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function BranchHasLocalTax(const CoBranchNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function DivisionHasLocalTax(const CoDivisionNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalFollowStateEdExempt(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalCalcMethod(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Char;
    function SyLocalMonthlyMaximum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalWageMaximum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalWageMinimum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalRate(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalTaxMinimum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Double;
    function SyLocalHasTaxMinimum(const ASyLocalsNbr: Integer; const CheckDate: TDateTime = 0): Boolean;
    function SyLocalQuarterlyMinimumThreshold(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function SyLocalMiscAmount(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function SyLocalMinimumWage(const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesErSdiRate(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesEeSdiRate(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesWeeklySdiTaxCap(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesErSdiMaximumWage(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function SyStatesEeSdiMaximumWage(const SyStatesNbr: Integer; const CheckDate: TDateTime): Double;
    function CoLocalTaxNbr(const CoNbr, SyLocalsNbr: Integer; const CheckDate: TDateTime): Integer;
    function IsClEdLocalExempt(const ClEdsNbr, SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function IsClEdLocalInclude(const ClEdsNbr, SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
    function EeStatesForSui(const EeNbr, CoStatesNbr: Integer; const CheckDate: TDateTime): Integer;
    function GetEeLocalsNbr(const EeNbr, CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Integer;
  public
    constructor Create;
  end;

function DateBetween(const MyDate, StartDate, EndDate: TDateTime): Boolean;

implementation

function DateBetween(const MyDate, StartDate, EndDate: TDateTime): Boolean;
begin
  Result := (Trunc(MyDate) >= Trunc(StartDate)) and (Trunc(MyDate) <= Trunc(EndDate));
end;

type

  TevCompany = class(TisInterfacedObject, IevCompany)
  protected
    FEvo: TevHistData;
    FCoNbr: Integer;
    FCheckDate: TDateTime;
    function CoNbr: Integer;
    function CoCombineStateSui(const ACoNbr: Integer): Boolean;
  public
    constructor Create(const AEvo: TevHistData; const ACoNbr: Integer; const ACheckDate: TDateTime);
  end;


  TevLine = class(TisInterfacedObject, IevLine)
  protected
    FNbr: Integer;
    FClEdNbr: Integer;
    FEDCode: String;
    FAmount: Double;
    FState: String;
    FSdiState: String;
    FSuiState: String;
    FCheckDate: TDateTime;
    FEvo: TevHistData;
    FDivisionNbr: Integer;
    FBranchNbr: Integer;
    FDepartmentNbr: Integer;
    FTeamNbr: Integer;
    FJobNbr: Integer;

    function GetClone: IInterface;

    function GetDivisionNbr: Integer;
    function GetBranchNbr: Integer;
    function GetDepartmentNbr: Integer;
    function GetTeamNbr: Integer;
    function GetJobNbr: Integer;

    function IsEarning: Boolean;
    function IsDeduction: Boolean;
    function GetSuiState: String;
    function GetEDCode: String;
    function GetClEdNbr: Integer;
    function GetAmount: Double;
    procedure SetAmount(const Value: Double);
    function GetValue: Double;
    function GetCheckValue: Double;
    function GetNbr: Integer;
    function GetState: String;
    function GetSdiState: String;
    function Taxable: Boolean;
    function SystemLevelStateExempt: Boolean;
    function ClientLevelStateExempt: Boolean;
    function SystemLevelFederalExempt: Boolean;
    function ClientLevelFederalExempt: Boolean;
    function SystemLevelEeMedicareExempt: Boolean;
    function ClientLevelEeMedicareExempt: Boolean;
    function SystemLevelErMedicareExempt: Boolean;
    function ClientLevelErMedicareExempt: Boolean;
    function SystemLevelEeOasdiExempt: Boolean;
    function ClientLevelEeOasdiExempt: Boolean;
    function SystemLevelEeSdiExempt: Boolean;
    function ClientLevelEeSdiExempt: Boolean;
    function SystemLevelErOasdiExempt: Boolean;
    function ClientLevelErOasdiExempt: Boolean;
    function SystemLevelFuiExempt: Boolean;
    function ClientLevelFuiExempt: Boolean;
  public
    constructor Create(const AEvo: TevHistData; const ANbr, AClEdNbr: Integer; const AAmount: Double;
      const AEDCode, AState, ASdiState, ASuiState: String;
      const ADivisionNbr, ABranchNbr, ADepartmentNbr, ATeamNbr, AJobNbr: Integer;
      const ACheckDate: TDateTime);
  end;

  TevStateTax = class(TisInterfacedObject, IevStateTax)
  private
    FState: String;
    FStateTax: Double;
    FEeSdiTax: Double;
    FStateTaxableWages: Double;
    FStateGrossWages: Double;
    FEeSdiTaxableWages: Double;
    FEeSdiGrossWages: Double;
    FYtdEeSdiTaxableWages: Double;
  protected
    function GetStateTaxableWages: Double;
    function GetStateGrossWages: Double;
    function GetEeSdiTaxableWages: Double;
    function GetEeSdiGrossWages: Double;
    function GetState: String;
    function GetStateTax: Double;
    function GetEeSdiTax: Double;
    function GetYtdEeSdiTaxableWages: Double;
    procedure SetYtdEeSdiTaxableWages(const Value: Double);
  public
    constructor Create(const AState: String; const AStateTax, AEeSdiTax, AStateTaxableWages,
      AEeSdiTaxableWages, AEeSdiGrossWages, AStateGrossWages: Double);
  end;

  TevSuiTax = class(TisInterfacedObject, IevSuiTax)
  private
    FEvo: TevHistData;
    FTax, FTaxWages, FGrossWages: Double;
    FSySuiNbr: Integer;
  protected
    function GetName: String;
    function GetTax: Double;
    function GetTaxWages: Double;
    function GetGrossWages: Double;
    function GetSySuiNbr: Integer;
    function IsEmployeeTax: Boolean;
    function IsEmployerTax: Boolean;
  public
    constructor Create(const AEvo: TevHistData; const ASySuiNbr: Integer; const ATax, ATaxWages, AGrossWages: Double);
  end;

  TevLocalTax = class(TisInterfacedObject, IevLocalTax)
  private
    FState: String;
    FTax: Double;
  protected
    function GetTax: Double;
    function GetState: String;
  public
    constructor Create(const AState: String; const ATax: Double);
  end;

  TevPerson = class(TisInterfacedObject, IevPerson)
  private
    FEvo: TevHistData;
    FChecks: IisListOfValues;
    FEeNbr: Integer;
    FClPersonNbr: Integer;
    FStateTaxes: IisParamsCollection;
    FEeNumbers: IisParamsCollection;
    FCheckDate: TDateTime;
    FHtml: String;
  protected
    procedure RedistributeNegativeCheckLines(const CheckDate: TDateTime);
    procedure RestoreOriginalCheckLines;
    function GetHtml: IisStringList;

    function StatesCount(const AEeNbr: Integer): Integer;
    function GetState(EeNbr, Index: Integer): String;
    function EeHasState(const EeNbr: Integer; const State: String): Boolean;

    function EeOasdiTaxWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeOasdiTaxTips(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeOasdiTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function EeSdiTax(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;

    function CalcEeOasdiTaxWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeOasdiTaxTips(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function ClPersonNbr: Integer;
    function GetEeNbr(Index: Integer): Integer;
    function EeCount: Integer;
    function HasEeNbr(const AEeNbr: Integer): Boolean;

    function CoNbr(EeNbr: Integer): Integer;
    function CustomEmployeeNumber(EeNbr: Integer): String;

    function CalcFederalTaxableWages(const AEeNbr: Integer): Double;
    function FederalTaxableWages(const AEeNbr: Integer): Double;

    function EeMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function EeMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function ErMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function ErMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function ErMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcErMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcErMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcErMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function FuiGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function FuiTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function FuiTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    
    function CalcFuiGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcFuiTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcFuiTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function SuiGrossWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function SuiTaxableWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function SuiTax(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function GetSuiList(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): IisListOfValues;
    function CalcSuiGrossWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
    function CalcSuiTaxableWages(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime; const DebugList: IisParamsCollection = nil): Double;
    function CalcSuiTax(const AEeNbr: Integer; const ASySuiNbr: Integer; const DateFrom, DateTo: TDateTime): Double;

    function CalcStateTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function StateTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcStateGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function StateGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;

    function CalcEeSdiTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function EeSdiTaxableWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeSdiTax(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function CalcEeSdiGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
    function EeSdiGrossWages(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;

    function GetCheck(Index: Integer): IevCheck;
    function GetQecCheck(EeNbr: Integer): IevCheck;
    property Checks[Index: Integer]: IevCheck read GetCheck;
    function CheckCount: Integer;
    property State[EeNbr: Integer; Index: Integer]: String read GetState;
  public
    constructor LoadFromDB(const AEvo: TevHistData; const AEeNbr: Integer; const CheckDate: TDateTime);
  end;

  TevCheck = class(TisInterfacedObject, IevCheck)
  protected
    FEvo: TevHistData;
    FEeNbr: Integer;
    FCheckNbr: Integer;
    FOLines: IisInterfaceList;
    FLines: IisInterfaceList;
    FEarnings: IisInterfaceList;
    FDeductions: IisInterfaceList;
    FStateTaxes: IisInterfaceList;
    FSuiTaxes: IisInterfaceList;
    FLocalTaxes: IisInterfaceList;
    FFederalTax: Double;
    FEeMedicareTax: Double;
    FEeMedicareGrossWages: Double;
    FEeMedicareTaxableWages: Double;

    FErMedicareTax: Double;
    FErMedicareGrossWages: Double;
    FErMedicareTaxableWages: Double;

    FFuiTax: Double;
    FFuiGrossWages: Double;
    FFuiTaxableWages: Double;

    FEeEICTax: Double;
    FBackUpWithholding: Double;
    FCheckDate: TDateTime;
    FFederalTaxableWages: Double;
    FFederalGrossWages: Double;
    FEeOasdiTaxableWages: Double;
    FErOasdiTaxableWages: Double;
    FEeOasdiTips: Double;
    FErOasdiTips: Double;
    FEeOasdiTax: Double;
    FErOasdiTax: Double;
    FCoNbr, FPrNbr: Integer;
    FCommonPaymasterNbr: Integer;
    FSerialNumber: Integer;
    FEeOasdiGrossWages: Double;
    FErOasdiGrossWages: Double;
    FTaxFrequency: String;
    FNetWages: Double;
    FCompany: IevCompany;

    FReciprocateSui: Boolean;
    function GetCompany: IevCompany;
    procedure AddSui(const ASySuiNbr: Integer; GrossWages, TaxWages, Tax: Double);
    function GetReciprocateSui: Boolean;
    function CheckDate: TDateTime;
    function CalcStateTaxableWages(const State: String): Double;
    function CalcEeSdiTaxableWages(const State: String; const CalcYtdEeSdiTaxableWages: Double): Double;
    function GetStateGrossWages(const State: String): Double;
    function CalcStateGrossWages(const State: String): Double;
    function CalcEeSdiGrossWages(const State: String): Double;
    function GetEeSdiGrossWages(const State: String): Double;
    function GetStateTaxableWages(const State: String): Double;
    function GetEeSdiTaxableWages(const State: String): Double;
    function EeNbr: Integer;
    function CoNbr: Integer;
    function PrNbr: Integer;
    function CheckNbr: Integer;
    function CalcFederalTaxableWages: Double;
    function GetFederalTaxableWages: Double;
    procedure SetFederalGrossWages(const Value: Double);
    function GetFederalGrossWages: Double;
    procedure SetFederalTaxableWages(const Value: Double);
    function GetEeOasdiTaxableWages: Double;
    procedure SetEeOasdiTaxableWages(const Value: Double);
    function GetErOasdiTaxableWages: Double;
    procedure SetErOasdiTaxableWages(const Value: Double);
    function GetEeOasdiTips: Double;
    procedure SetEeOasdiTips(const Value: Double);
    function GetErOasdiTips: Double;
    procedure SetErOasdiTips(const Value: Double);
    function GetEeOasdiGrossWages: Double;
    procedure SetEeOasdiGrossWages(const Value: Double);
    function GetErOasdiGrossWages: Double;
    procedure SetErOasdiGrossWages(const Value: Double);
    function CalcEeOasdiWages: Double;
    function CalcErOasdiWages: Double;
    function CalcEeOasdiTips: Double;
    function CalcErOasdiTips: Double;
    function GetEeOasdiTax: Double;
    procedure SetEeOasdiTax(const Value: Double);
    function GetErOasdiTax: Double;
    procedure SetErOasdiTax(const Value: Double);
    function CalcEeSdiTax(const State: String; const CalcYTDTaxWages: Double): Double;
    function GetEarning(Index: Integer): IevLine;
    function GetDeduction(Index: Integer): IevLine;
    function EarningsCount: Integer;
    function DeductionsCount: Integer;
    function FederalTax: Double;

    function EeMedicareTax: Double;
    function EeMedicareGrossWages: Double;
    function EeMedicareTaxableWages: Double;
    function CalcEeMedicareTaxableWages(const AYTDWages: Double): Double;
    function CalcEeMedicareGrossWages: Double;
    function CalcEeMedicareTax(const AYTDWage:Double): Double;

    function ErMedicareTax: Double;
    function ErMedicareGrossWages: Double;
    function ErMedicareTaxableWages: Double;
    function CalcErMedicareGrossWages: Double;
    function CalcErMedicareTaxableWages(const AYTDWages:Double): Double;
    function CalcErMedicareTax(const AYTDWage:Double): Double;

    function FuiTax: Double;
    function FuiGrossWages: Double;
    function FuiTaxableWages: Double;
    function CalcFuiTaxableWages(const AYTDWages: Double): Double;
    function CalcFuiGrossWages: Double;
    function CalcFuiTax(const AYTDWage:Double): Double;

    function GetLine(Index: Integer): IevLine;

    function EeEICTax: Double;
    function BackUpWithholding: Double;
    function StateTaxCount: Integer;
    function GetStateTaxes(Index: Integer): IevStateTax;
    function GetStateTax(Index: String): IevStateTax;
    function SuiCount: Integer;
    function GetSuiTax(Index: Integer): IevSuiTax;
    function GetLocalTax(Index: Integer): IevLocalTax;
    function LocalsCount: Integer;
    function LinesCount: Integer;
    function NetAmount: Double;
    function NetWages: Double;
    function GetSerialNumber: Integer;
    property Lines[Index: Integer]: IevLine read GetLine;
    property Earnings[Index: Integer]: IevLine read GetEarning;
    property Deductions[Index: Integer]: IevLine read GetDeduction;
    property StateTaxes[Index: Integer]: IevStateTax read GetStateTaxes;
    property StateTax[Index: String]: IevStateTax read GetStateTax;
    property SuiTaxes[Index: Integer]: IevSuiTax read GetSuiTax;
    property LocalTaxes[Index: Integer]: IevLocalTax read GetLocalTax;
    function TypeIsTips(const ACode: String): Boolean;
    procedure RestoreOriginalCheckLines;
  public
    constructor LoadFromDb(const AEvo: TevHistData; const ACheckNbr: Integer);
    constructor CreateEmpty(const AEvo: TevHistData; const AEeNbr: Integer; const ACheckDate: TDateTime);
  end;

{ TevHistData }

constructor TevHistData.Create;
begin
  inherited Create;
  FSui := TisParamsCollection.Create;
  FLocals := TisParamsCollection.Create;
  FClient := TisParamsCollection.Create;
  FCoLocalTax := TisParamsCollection.Create;
  FCoLocalTax1 := TisListOfValues.Create;
  FCoStates := TisParamsCollection.Create;
  FCoStates1 := TisParamsCollection.Create;
  FClEds := TisParamsCollection.Create;
  FClEdsLocalsExemptions := TisParamsCollection.Create;
  FCompanies := TisParamsCollection.Create;
  FEeStates := TisParamsCollection.Create;
  FEeStates1 := TisParamsCollection.Create;
  FEeStates2 := TisParamsCollection.Create;
  FEdCodes := TisParamsCollection.Create;
  FCoSui := TisParamsCollection.Create;
  FCoSui1 := TisParamsCollection.Create;
  FSySui := TisParamsCollection.Create;
  FEe := TisParamsCollection.Create;
  FStateExempt := TisParamsCollection.Create;
  FFedTaxTable := TisListOfValues.Create;
  FSyStates := TisListOfValues.Create;
  FStates := TisParamsCollection.Create;
  FEeLocals := TisParamsCollection.Create;
  FDivision := TisParamsCollection.Create;
  FBranch := TisParamsCollection.Create;
  FDepartment := TisParamsCollection.Create;
  FTeam := TisParamsCollection.Create;
  FJobs := TisParamsCollection.Create;
  FSyLocalExempt := TisParamsCollection.Create;
  FEeLocals1 := TisListOfValues.Create;
  FPeople := TisListOfValues.Create;
  FSuiEmployeeCount := TisListOfValues.Create;
  FSuiTotalWages := TisListOfValues.Create;
end;

function TevHistData.GetCheck(const ACheckNbr: Integer): IevCheck;
begin
  Result := TevCheck.LoadFromDb(Self, ACheckNbr);
end;

function TevHistData.IsEmployeeLocalTax(
  const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := V.Value['TAX_TYPE'] = 'E';
end;

function TevHistData.IsEmployeeSui(
  const ASySuiNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ASySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSui.AddParams(ParamName);
  if not V.ValueExists('EE_ER_OR_EE_OTHER_OR_ER_OTHER') then
  begin
    Q := TevQuery.CreateAsOf('select EE_ER_OR_EE_OTHER_OR_ER_OTHER from SY_SUI where {AsOfDate<SY_SUI>} and SY_SUI_NBR='+IntToStr(ASySuiNbr), CheckDate);
    V.AddValue('EE_ER_OR_EE_OTHER_OR_ER_OTHER', Q.Result.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').AsString);
  end;
  Result := V.Value['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = 'E';
end;

function TevHistData.SyLocalState(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FLocals.AddParams(ParamName);
  if not V.ValueExists('STATE') then
  begin
    Q := TevQuery.CreateAsOf('select s.STATE, x.TAX_TYPE from SY_LOCALS x '+
    '  join SY_STATES s on {AsOfDate<s>} and s.SY_STATES_NBR=x.SY_STATES_NBR'+
    '  where {AsOfDate<x>} and x.SY_LOCALS_NBR='+IntToStr(ASyLocalsNbr), CheckDate);
    V.AddValue('STATE', Q.Result.FieldByName('STATE').AsString);
    V.AddValue('TAX_TYPE', Q.Result.FieldByName('TAX_TYPE').AsString);
  end;
  Result := V.Value['STATE'];
end;

function TevHistData.CoLocalTaxRate(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Variant;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoLocalTaxNbr) + ':' + DateToStr(CheckDate);
  V := FCoLocalTax.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoLocalTax.AddParams(ParamName);
  if not V.ValueExists('TAX_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select x.TAX_RATE from CO_LOCAL_TAX x '+
    '  where {AsOfDate<x>} and x.CO_LOCAL_TAX_NBR='+IntToStr(ACoLocalTaxNbr), CheckDate);
    V.AddValue('TAX_RATE', Q.Result['TAX_RATE']);
  end;
  Result := V.Value['TAX_RATE'];
end;

function TevHistData.CoLocalSelfAdjustTax(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoLocalTaxNbr) + ':' + DateToStr(CheckDate);
  V := FCoLocalTax.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoLocalTax.AddParams(ParamName);
  if not V.ValueExists('SELF_ADJUST_TAX') then
  begin
    Q := TevQuery.CreateAsOf('select x.SELF_ADJUST_TAX from CO_LOCAL_TAX x '+
    '  where {AsOfDate<x>} and x.CO_LOCAL_TAX_NBR='+IntToStr(ACoLocalTaxNbr), CheckDate);
    V.AddValue('SELF_ADJUST_TAX', Q.Result.FieldByName('SELF_ADJUST_TAX').AsString);
  end;
  Result := V.Value['SELF_ADJUST_TAX'] = 'Y';
end;

function TevHistData.CoSyLocalsNbr(const ACoLocalTaxNbr: Integer; const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoLocalTaxNbr) + ':' + DateToStr(CheckDate);
  V := FCoLocalTax.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoLocalTax.AddParams(ParamName);
  if not V.ValueExists('SY_LOCALS_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select x.SY_LOCALS_NBR from CO_LOCAL_TAX x '+
    '  where {AsOfDate<x>} and x.CO_LOCAL_TAX_NBR='+IntToStr(ACoLocalTaxNbr), CheckDate);
    V.AddValue('SY_LOCALS_NBR', Q.Result.FieldByName('SY_LOCALS_NBR').AsString);
  end;
  Result := V.Value['SY_LOCALS_NBR'];
end;

function TevHistData.CoLocalState(const ACoLocalsNbr: Integer; const CheckDate: TDateTime): String;
begin
  Result := SyLocalState(CoSyLocalsNbr(ACoLocalsNbr, CheckDate), CheckDate);
end;

function TevHistData.CoStateState(const ACoStatesNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoStatesNbr) + ':' + DateToStr(CheckDate);
  V := FCoStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoStates.AddParams(ParamName);
  if not V.ValueExists('STATE') then
  begin
    Q := TevQuery.CreateAsOf('select STATE from CO_STATES where CO_STATES_NBR='+IntToStr(ACoStatesNbr)+' and {AsOfDate<CO_STATES>}', CheckDate);
    V.AddValue('STATE', Q.Result.FieldByName('STATE').AsString);
  end;
  Result := V.Value['STATE'];
end;

function TevHistData.IsCoStateEeSdiExempt(const ACoStatesNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoStatesNbr) + ':' + DateToStr(CheckDate);
  V := FCoStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoStates.AddParams(ParamName);
  if not V.ValueExists('EE_SDI_EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select EE_SDI_EXEMPT from CO_STATES where CO_STATES_NBR='+IntToStr(ACoStatesNbr)+' and {AsOfDate<CO_STATES>}', CheckDate);
    V.AddValue('EE_SDI_EXEMPT', Q.Result.FieldByName('EE_SDI_EXEMPT').AsString);
  end;
  Result := V.Value['EE_SDI_EXEMPT'] = 'Y';
end;

function TevHistData.IsCoStateErSdiExempt(const ACoStatesNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoStatesNbr) + ':' + DateToStr(CheckDate);
  V := FCoStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoStates.AddParams(ParamName);
  if not V.ValueExists('ER_SDI_EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select ER_SDI_EXEMPT from CO_STATES where CO_STATES_NBR='+IntToStr(ACoStatesNbr)+' and {AsOfDate<CO_STATES>}', CheckDate);
    V.AddValue('ER_SDI_EXEMPT', Q.Result.FieldByName('ER_SDI_EXEMPT').AsString);
  end;
  Result := V.Value['ER_SDI_EXEMPT'] = 'Y';
end;

function TevHistData.IsCoStateSuiExempt(const ACoStatesNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoStatesNbr) + ':' + DateToStr(CheckDate);
  V := FCoStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoStates.AddParams(ParamName);
  if not V.ValueExists('SUI_EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select SUI_EXEMPT from CO_STATES where CO_STATES_NBR='+IntToStr(ACoStatesNbr)+' and {AsOfDate<CO_STATES>}', CheckDate);
    V.AddValue('SUI_EXEMPT', Q.Result.FieldByName('SUI_EXEMPT').AsString);
  end;
  Result := V.Value['SUI_EXEMPT'] = 'Y';
end;

function TevHistData.IsCoStateExempt(const ACoStatesNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoStatesNbr) + ':' + DateToStr(CheckDate);
  V := FCoStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoStates.AddParams(ParamName);
  if not V.ValueExists('STATE_EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select STATE_EXEMPT from CO_STATES where CO_STATES_NBR='+IntToStr(ACoStatesNbr)+' and {AsOfDate<CO_STATES>}', CheckDate);
    V.AddValue('STATE_EXEMPT', Q.Result.FieldByName('STATE_EXEMPT').AsString);
  end;
  Result := V.Value['STATE_EXEMPT'] = 'Y';
end;

function TevHistData.GetCoStatesNbr(const ACoNbr: Integer;
  const State: String; const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoNbr)+':'+State + ':' + DateToStr(CheckDate);
  V := FCoStates1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoStates1.AddParams(ParamName);
  if not V.ValueExists('CO_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CO_STATES_NBR from CO_STATES where CO_NBR='+IntToStr(ACoNbr)+' and STATE='''+State+''' and {AsOfDate<CO_STATES>}', CheckDate);
    if Q.Result.FieldByName('CO_STATES_NBR').IsNull then
    begin
      Q := TevQuery.CreateAsOf('select CUSTOM_COMPANY_NUMBER from CO where CO_NBR='+IntToStr(ACoNbr)+' and {AsOfDate<CO>}', CheckDate);
      raise Exception.Create('State '+State+' is not set up for company ' + Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString);
    end;
    V.AddValue('CO_STATES_NBR', Q.Result.FieldByName('CO_STATES_NBR').AsString);
  end;
  Result := V.Value['CO_STATES_NBR'];
end;

function TevHistData.ClEdCodeType(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('E_D_CODE_TYPE') then
  begin
    Q := TevQuery.CreateAsOf('select E_D_CODE_TYPE from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('E_D_CODE_TYPE', Q.Result.FieldByName('E_D_CODE_TYPE').AsString);
  end;
  Result := V.Value['E_D_CODE_TYPE'];
end;

function TevHistData.ClEdDescription(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('DESCRIPTION') then
  begin
    Q := TevQuery.CreateAsOf('select DESCRIPTION from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('DESCRIPTION', Q.Result.FieldByName('DESCRIPTION').AsString);
  end;
  Result := V.Value['DESCRIPTION'];
end;

function TevHistData.IsClEdFederalExempt(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('EE_EXEMPT_EXCLUDE_FEDERAL') then
  begin
    Q := TevQuery.CreateAsOf('select EE_EXEMPT_EXCLUDE_FEDERAL from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('EE_EXEMPT_EXCLUDE_FEDERAL', Q.Result.FieldByName('EE_EXEMPT_EXCLUDE_FEDERAL').AsString);
  end;
  Result := V.Value['EE_EXEMPT_EXCLUDE_FEDERAL'] = 'E';
end;

function TevHistData.IsClEdEeOasdiExempt(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('EE_EXEMPT_EXCLUDE_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select EE_EXEMPT_EXCLUDE_OASDI from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('EE_EXEMPT_EXCLUDE_OASDI', Q.Result.FieldByName('EE_EXEMPT_EXCLUDE_OASDI').AsString);
  end;
  Result := V.Value['EE_EXEMPT_EXCLUDE_OASDI'] = 'E';
end;

function TevHistData.IsClEdErOasdiExempt(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('ER_EXEMPT_EXCLUDE_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select ER_EXEMPT_EXCLUDE_OASDI from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('ER_EXEMPT_EXCLUDE_OASDI', Q.Result.FieldByName('ER_EXEMPT_EXCLUDE_OASDI').AsString);
  end;
  Result := V.Value['ER_EXEMPT_EXCLUDE_OASDI'] = 'E';
end;

function TevHistData.IsClEdLocalExempt(const ClEdsNbr, SyLocalsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + IntToStr(SyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FClEdsLocalsExemptions.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEdsLocalsExemptions.AddParams(ParamName);
  if not V.ValueExists('') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EXCLUDE from CL_E_D_LOCAL_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+
    ' and SY_LOCALS_NBR='+IntToStr(SyLocalsNbr) +
    ' and {AsOfDate<CL_E_D_LOCAL_EXMPT_EXCLD>}', CheckDate);
    V.AddValue('EXEMPT_EXCLUDE', Q.Result.FieldByName('EXEMPT_EXCLUDE').AsString);
  end;
  Result := V.Value['EXEMPT_EXCLUDE'] = 'E';
end;

function TevHistData.IsClEdEeMedicareExempt(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('EE_EXEMPT_EXCLUDE_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_EXEMPT_EXCLUDE_MEDICARE from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('EE_EXEMPT_EXCLUDE_MEDICARE', Q.Result.FieldByName('EE_EXEMPT_EXCLUDE_MEDICARE').AsString);
  end;
  Result := V.Value['EE_EXEMPT_EXCLUDE_MEDICARE'] = 'E';
end;

function TevHistData.IsClEdErMedicareExempt(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('ER_EXEMPT_EXCLUDE_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select ER_EXEMPT_EXCLUDE_MEDICARE from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('ER_EXEMPT_EXCLUDE_MEDICARE', Q.Result.FieldByName('ER_EXEMPT_EXCLUDE_MEDICARE').AsString);
  end;
  Result := V.Value['ER_EXEMPT_EXCLUDE_MEDICARE'] = 'E';
end;

function TevHistData.IsClEdErFuiExempt(const ClEdsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + DateToStr(CheckDate);
  V := FClEds.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEds.AddParams(ParamName);
  if not V.ValueExists('ER_EXEMPT_EXCLUDE_FUI') then
  begin
    Q := TevQuery.CreateAsOf('select ER_EXEMPT_EXCLUDE_FUI from CL_E_DS where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('ER_EXEMPT_EXCLUDE_FUI', Q.Result.FieldByName('ER_EXEMPT_EXCLUDE_FUI').AsString);
  end;
  Result := V.Value['ER_EXEMPT_EXCLUDE_FUI'] = 'E';
end;

function TevHistData.CoCommonPaymaster(const CoNbr: Integer;
  const CheckDate: TDateTime): Variant;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('CL_COMMON_PAYMASTER_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CL_COMMON_PAYMASTER_NBR from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('CL_COMMON_PAYMASTER_NBR', Q.Result['CL_COMMON_PAYMASTER_NBR']);
  end;
  Result := V.Value['CL_COMMON_PAYMASTER_NBR'];
end;

function TevHistData.CoPayFrequencies(const CoNbr: Integer;
  const CheckDate: TDateTime): Char;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName, s: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('PAY_FREQUENCIES') then
  begin
    Q := TevQuery.CreateAsOf('select PAY_FREQUENCIES from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('PAY_FREQUENCIES', Q.Result['PAY_FREQUENCIES']);
  end;
  s := V.Value['PAY_FREQUENCIES'];
  Result := s[1];
end;

function TevHistData.CombineStateSui(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  PAYMASTER_NBR: Variant;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('CL_COMMON_PAYMASTER_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CL_COMMON_PAYMASTER_NBR from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('CL_COMMON_PAYMASTER_NBR', Q.Result.FieldByName('CL_COMMON_PAYMASTER_NBR').AsString);
  end;
  PAYMASTER_NBR := V.Value['CL_COMMON_PAYMASTER_NBR'];
  if VarIsNull(PAYMASTER_NBR) then
    Result := False
  else
  begin
    if not V.ValueExists('COMBINE_STATE_SUI') then
    begin
      Q := TevQuery.CreateAsOf('select COMBINE_STATE_SUI from CL_COMMON_PAYMASTER where CL_COMMON_PAYMASTER_NBR='+VarToStr(PAYMASTER_NBR)+' and {AsOfDate<CL_COMMON_PAYMASTER>}', CheckDate);
      V.AddValue('COMBINE_STATE_SUI', Q.Result.FieldByName('COMBINE_STATE_SUI').AsString);
    end;
    Result := V.Value['COMBINE_STATE_SUI'] = 'Y';
  end;
end;

function TevHistData.CombineOASDI(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  PAYMASTER_NBR: Variant;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('CL_COMMON_PAYMASTER_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CL_COMMON_PAYMASTER_NBR from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('CL_COMMON_PAYMASTER_NBR', Q.Result.FieldByName('CL_COMMON_PAYMASTER_NBR').AsString);
  end;
  PAYMASTER_NBR := V.Value['CL_COMMON_PAYMASTER_NBR'];
  if VarIsNull(PAYMASTER_NBR) then
    Result := False
  else
  begin
    if not V.ValueExists('COMBINE_OASDI') then
    begin
      Q := TevQuery.CreateAsOf('select COMBINE_OASDI from CL_COMMON_PAYMASTER where CL_COMMON_PAYMASTER_NBR='+VarToStr(PAYMASTER_NBR)+' and {AsOfDate<CL_COMMON_PAYMASTER>}', CheckDate);
      V.AddValue('COMBINE_OASDI', Q.Result.FieldByName('COMBINE_OASDI').AsString);
    end;
    Result := V.Value['COMBINE_OASDI'] = 'Y';
  end;
end;

function TevHistData.CombineFui(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  PAYMASTER_NBR: Variant;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('CL_COMMON_PAYMASTER_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CL_COMMON_PAYMASTER_NBR from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('CL_COMMON_PAYMASTER_NBR', Q.Result.FieldByName('CL_COMMON_PAYMASTER_NBR').AsString);
  end;
  PAYMASTER_NBR := V.Value['CL_COMMON_PAYMASTER_NBR'];
  if VarIsNull(PAYMASTER_NBR) then
    Result := False
  else
  begin
    if not V.ValueExists('COMBINE_FUI') then
    begin
      Q := TevQuery.CreateAsOf('select COMBINE_FUI from CL_COMMON_PAYMASTER where CL_COMMON_PAYMASTER_NBR='+VarToStr(PAYMASTER_NBR)+' and {AsOfDate<CL_COMMON_PAYMASTER>}', CheckDate);
      V.AddValue('COMBINE_FUI', Q.Result.FieldByName('COMBINE_FUI').AsString);
    end;
    Result := V.Value['COMBINE_FUI'] = 'Y';
  end;
end;

function TevHistData.CombineMedicare(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  PAYMASTER_NBR: Variant;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('CL_COMMON_PAYMASTER_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CL_COMMON_PAYMASTER_NBR from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('CL_COMMON_PAYMASTER_NBR', Q.Result.FieldByName('CL_COMMON_PAYMASTER_NBR').AsString);
  end;
  PAYMASTER_NBR := V.Value['CL_COMMON_PAYMASTER_NBR'];
  if VarIsNull(PAYMASTER_NBR) then
    Result := False
  else
  begin
    if not V.ValueExists('COMBINE_MEDICARE') then
    begin
      Q := TevQuery.CreateAsOf('select COMBINE_MEDICARE from CL_COMMON_PAYMASTER where CL_COMMON_PAYMASTER_NBR='+VarToStr(PAYMASTER_NBR)+' and {AsOfDate<CL_COMMON_PAYMASTER>}', CheckDate);
      V.AddValue('COMBINE_MEDICARE', Q.Result.FieldByName('COMBINE_MEDICARE').AsString);
    end;
    Result := V.Value['COMBINE_MEDICARE'] = 'Y';
  end;
end;

function TevHistData.CoCombineMedicare(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
var
  PMNbr, PMNbr1: Variant;
begin
  if CoNbr = CoNbr1 then
    Result := True
  else
  begin
    PMNbr := CoCommonPaymaster(CoNbr, CheckDate);
    if VarIsNull(PMNbr) then
      Result := False
    else
    begin
      PMNbr1 := CoCommonPaymaster(CoNbr1, CheckDate);
      if VarIsNull(PMNbr) then
        Result := False
      else
        Result := (PMNbr = PMNbr1) and CombineMedicare(CoNbr, CheckDate);
    end;
  end;
end;

function TevHistData.CoCombineFui(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
var
  PMNbr, PMNbr1: Variant;
begin
  if CoNbr = CoNbr1 then
    Result := True
  else
  begin
    PMNbr := CoCommonPaymaster(CoNbr, CheckDate);
    if VarIsNull(PMNbr) then
      Result := False
    else
    begin
      PMNbr1 := CoCommonPaymaster(CoNbr1, CheckDate);
      if VarIsNull(PMNbr) then
        Result := False
      else
        Result := (PMNbr = PMNbr1) and CombineFui(CoNbr, CheckDate);
    end;
  end;
end;

function TevHistData.CoCombineStateSui(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
var
  PMNbr, PMNbr1: Variant;
begin
  if CoNbr = CoNbr1 then
    Result := True
  else
  begin
    PMNbr := CoCommonPaymaster(CoNbr, CheckDate);
    if VarIsNull(PMNbr) then
      Result := False
    else
    begin
      PMNbr1 := CoCommonPaymaster(CoNbr1, CheckDate);
      if VarIsNull(PMNbr) then
        Result := False
      else
        Result := (PMNbr = PMNbr1) and CombineStateSui(CoNbr, CheckDate);
    end;
  end;
end;

function TevHistData.IsCoFederalExemptEeOasdi(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FED_TAX_EXEMPT_EE_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select FED_TAX_EXEMPT_EE_OASDI from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FED_TAX_EXEMPT_EE_OASDI', Q.Result.FieldByName('FED_TAX_EXEMPT_EE_OASDI').AsString);
  end;
  Result := V.Value['FED_TAX_EXEMPT_EE_OASDI'] = 'Y';
end;

function TevHistData.IsCoFederalExempt(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FEDERAL_TAX_EXEMPT_STATUS') then
  begin
    Q := TevQuery.CreateAsOf('select FEDERAL_TAX_EXEMPT_STATUS from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FEDERAL_TAX_EXEMPT_STATUS', Q.Result.FieldByName('FEDERAL_TAX_EXEMPT_STATUS').AsString);
  end;
  Result := V.Value['FEDERAL_TAX_EXEMPT_STATUS'] = 'Y';
end;

function TevHistData.IsCoFederalExemptErOasdi(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FED_TAX_EXEMPT_ER_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select FED_TAX_EXEMPT_ER_OASDI from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FED_TAX_EXEMPT_ER_OASDI', Q.Result.FieldByName('FED_TAX_EXEMPT_ER_OASDI').AsString);
  end;
  Result := V.Value['FED_TAX_EXEMPT_ER_OASDI'] = 'Y';
end;

function TevHistData.IsCoFederalExemptEeMedicare(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FED_TAX_EXEMPT_EE_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select FED_TAX_EXEMPT_EE_MEDICARE from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FED_TAX_EXEMPT_EE_MEDICARE', Q.Result.FieldByName('FED_TAX_EXEMPT_EE_MEDICARE').AsString);
  end;
  Result := V.Value['FED_TAX_EXEMPT_EE_MEDICARE'] = 'Y';
end;

function TevHistData.IsCoFederalExemptErMedicare(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FED_TAX_EXEMPT_ER_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select FED_TAX_EXEMPT_ER_MEDICARE from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FED_TAX_EXEMPT_ER_MEDICARE', Q.Result.FieldByName('FED_TAX_EXEMPT_ER_MEDICARE').AsString);
  end;
  Result := V.Value['FED_TAX_EXEMPT_ER_MEDICARE'] = 'Y';
end;

function TevHistData.IsCoFuiExempt(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FUI_TAX_EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_TAX_EXEMPT from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FUI_TAX_EXEMPT', Q.Result.FieldByName('FUI_TAX_EXEMPT').AsString);
  end;
  Result := V.Value['FUI_TAX_EXEMPT'] = 'Y';
end;

function TevHistData.IsCoFuiRateOverride(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FUI_RATE_OVERRIDE') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_RATE_OVERRIDE from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FUI_RATE_OVERRIDE', Q.Result.FieldByName('FUI_RATE_OVERRIDE').Value);
  end;
  Result := not VarIsNull(V.Value['FUI_RATE_OVERRIDE']);
end;

function TevHistData.CoFuiRateOverride(const CoNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + DateToStr(CheckDate);
  V := FCompanies.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCompanies.AddParams(ParamName);
  if not V.ValueExists('FUI_RATE_OVERRIDE') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_RATE_OVERRIDE from CO where CO_NBR='+IntToStr(CoNbr)+' and {AsOfDate<CO>}', CheckDate);
    V.AddValue('FUI_RATE_OVERRIDE', Q.Result.FieldByName('FUI_RATE_OVERRIDE').Value);
  end;
  Result := V.Value['FUI_RATE_OVERRIDE'];
end;

function TevHistData.GetEeCoStatesNbr(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('CO_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CO_STATES_NBR from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('CO_STATES_NBR', Q.Result.FieldByName('CO_STATES_NBR').Value);
  end;
  Result := V.Value['CO_STATES_NBR'];
end;

function TevHistData.GetEeStatesErSdiExemptExclude(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('ER_SDI_EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select ER_SDI_EXEMPT_EXCLUDE from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('ER_SDI_EXEMPT_EXCLUDE', Q.Result.FieldByName('ER_SDI_EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['ER_SDI_EXEMPT_EXCLUDE'];
end;

function TevHistData.IsEeStateExempt(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('STATE_EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select STATE_EXEMPT_EXCLUDE from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('STATE_EXEMPT_EXCLUDE', Q.Result.FieldByName('STATE_EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['STATE_EXEMPT_EXCLUDE'] = 'E';
end;

function TevHistData.EeStateNumberWithholdingAllow(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('STATE_NUMBER_WITHHOLDING_ALLOW') then
  begin
    Q := TevQuery.CreateAsOf('select STATE_NUMBER_WITHHOLDING_ALLOW from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('STATE_NUMBER_WITHHOLDING_ALLOW', Q.Result.FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').Value);
  end;
  Result := ConvertNull(V.Value['STATE_NUMBER_WITHHOLDING_ALLOW'], 0);
end;


function TevHistData.IsEdCodeEeOasdiExempt(const EdCode: String;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYEE_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYEE_OASDI from SY_FED_EXEMPTIONS where E_D_CODE_TYPE='''+EDCode+''' and {AsOfDate<SY_FED_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYEE_OASDI', Q.Result.FieldByName('EXEMPT_EMPLOYEE_OASDI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYEE_OASDI'] = 'Y';
end;

function TevHistData.IsEdCodeErOasdiExempt(const EdCode: String;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_OASDI from SY_FED_EXEMPTIONS where E_D_CODE_TYPE='''+EDCode+''' and {AsOfDate<SY_FED_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_OASDI', Q.Result.FieldByName('EXEMPT_EMPLOYER_OASDI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_OASDI'] = 'Y';
end;

function TevHistData.IsEdCodeFederalExempt(const EdCode: String;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_FEDERAL') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_FEDERAL from SY_FED_EXEMPTIONS where E_D_CODE_TYPE='''+EDCode+''' and {AsOfDate<SY_FED_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_FEDERAL', Q.Result.FieldByName('EXEMPT_FEDERAL').Value);
  end;
  Result := V.Value['EXEMPT_FEDERAL'] = 'E';
end;

function TevHistData.IsEdCodeEeMedicareExempt(const EdCode: String;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYEE_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYEE_MEDICARE from SY_FED_EXEMPTIONS where E_D_CODE_TYPE='''+EDCode+''' and {AsOfDate<SY_FED_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYEE_MEDICARE', Q.Result.FieldByName('EXEMPT_EMPLOYEE_MEDICARE').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYEE_MEDICARE'] = 'Y';
end;

function TevHistData.IsEdCodeErMedicareExempt(const EdCode: String;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_MEDICARE from SY_FED_EXEMPTIONS where E_D_CODE_TYPE='''+EDCode+''' and {AsOfDate<SY_FED_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_MEDICARE', Q.Result.FieldByName('EXEMPT_EMPLOYER_MEDICARE').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_MEDICARE'] = 'Y';
end;

function TevHistData.IsEdCodeFuiExempt(const EdCode: String;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_FUI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_FUI from SY_FED_EXEMPTIONS where E_D_CODE_TYPE='''+EDCode+''' and {AsOfDate<SY_FED_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_FUI', Q.Result.FieldByName('EXEMPT_FUI').Value);
  end;
  Result := V.Value['EXEMPT_FUI'] = 'Y';
end;

function TevHistData.IsCoSySuiInactive(const CoNbr, SySuiNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr)+':'+IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui.AddParams(ParamName);
  if not V.ValueExists('FINAL_TAX_RETURN') then
  begin
    Q := TevQuery.CreateAsOf('select FINAL_TAX_RETURN from CO_SUI where CO_NBR='+IntToStr(CoNbr)+' and SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('FINAL_TAX_RETURN', Q.Result.FieldByName('FINAL_TAX_RETURN').Value);
  end;
  Result := VarIsNull(V.Value['FINAL_TAX_RETURN']) or (V.Value['FINAL_TAX_RETURN'] = 'Y');
end;

function TevHistData.CoSySuiRate(const CoNbr, SySuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr)+':'+IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui.AddParams(ParamName);
  if not V.ValueExists('RATE') then
  begin
    Q := TevQuery.CreateAsOf('select RATE from CO_SUI where CO_NBR='+IntToStr(CoNbr)+' and SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('RATE', Q.Result.FieldByName('RATE').Value);
  end;
  Result := V.Value['RATE'];
end;

function TevHistData.IsCoSuiInactive(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('FINAL_TAX_RETURN') then
  begin
    Q := TevQuery.CreateAsOf('select FINAL_TAX_RETURN from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('FINAL_TAX_RETURN', Q.Result.FieldByName('FINAL_TAX_RETURN').Value);
  end;
  Result := VarIsNull(V.Value['FINAL_TAX_RETURN']) or (V.Value['FINAL_TAX_RETURN'] = 'Y');
end;

function TevHistData.GetCoSuiNbr(const ACoNbr, SySuiNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ACoNbr)+':'+IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui.AddParams(ParamName);
  if not V.ValueExists('CO_SUI_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CO_SUI_NBR from CO_SUI where CO_NBR='+IntToStr(ACoNbr)+' and SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('CO_SUI_NBR', Q.Result.FieldByName('CO_SUI_NBR').Value);
  end;
  Result := V.Value['CO_SUI_NBR'];
end;

function TevHistData.IsErSui(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  Val: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('EE_ER_OR_EE_OTHER_OR_ER_OTHER') then
  begin
    Q := TevQuery.CreateAsOf('select EE_ER_OR_EE_OTHER_OR_ER_OTHER from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('EE_ER_OR_EE_OTHER_OR_ER_OTHER', Q.Result.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').Value);
  end;
  Val := V.Value['EE_ER_OR_EE_OTHER_OR_ER_OTHER'];

  if Val[1] in [GROUP_BOX_ER, GROUP_BOX_ER_OTHER] then
    Result := True
  else
    Result := False;
end;

function TevHistData.SuiTaxName(const SySuiNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('SUI_TAX_NAME') then
  begin
    Q := TevQuery.CreateAsOf('select SUI_TAX_NAME from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('SUI_TAX_NAME', Q.Result.FieldByName('SUI_TAX_NAME').Value);
  end;
  Result := V.Value['SUI_TAX_NAME'];
end;

function TevHistData.SuiType(const SySuiNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('EE_ER_OR_EE_OTHER_OR_ER_OTHER') then
  begin
    Q := TevQuery.CreateAsOf('select EE_ER_OR_EE_OTHER_OR_ER_OTHER from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('EE_ER_OR_EE_OTHER_OR_ER_OTHER', Q.Result.FieldByName('EE_ER_OR_EE_OTHER_OR_ER_OTHER').Value);
  end;
  Result := V.Value['EE_ER_OR_EE_OTHER_OR_ER_OTHER'];
end;

function TevHistData.SuiAdditionalAssessmentRate(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('FUTURE_DEFAULT_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select FUTURE_DEFAULT_RATE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('FUTURE_DEFAULT_RATE', Q.Result.FieldByName('FUTURE_DEFAULT_RATE').Value);
  end;
  Result := ConvertNull(V.Value['FUTURE_DEFAULT_RATE'], 0);
end;

function TevHistData.SuiAdditionalAssessmentAmount(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('FUTURE_MAXIMUM_WAGE') then
  begin
    Q := TevQuery.CreateAsOf('select FUTURE_MAXIMUM_WAGE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('FUTURE_MAXIMUM_WAGE', Q.Result.FieldByName('FUTURE_MAXIMUM_WAGE').Value);
  end;
  Result := ConvertNull(V.Value['FUTURE_MAXIMUM_WAGE'], 0);
end;


function TevHistData.SuiBasedOnHours(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('USE_MISC_TAX_RETURN_CODE') then
  begin
    Q := TevQuery.CreateAsOf('select USE_MISC_TAX_RETURN_CODE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('USE_MISC_TAX_RETURN_CODE', Q.Result.FieldByName('USE_MISC_TAX_RETURN_CODE').Value);
  end;
  Result := V.Value['USE_MISC_TAX_RETURN_CODE'] = 'Y';
end;

function TevHistData.SuiGlobalRate(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Variant;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('GLOBAL_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select GLOBAL_RATE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('GLOBAL_RATE', Q.Result.FieldByName('GLOBAL_RATE').Value);
  end;
  Result := V.Value['GLOBAL_RATE'];
end;


function TevHistData.SySuiMaxWage(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('MAXIMUM_WAGE') then
  begin
    Q := TevQuery.CreateAsOf('select MAXIMUM_WAGE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('MAXIMUM_WAGE', Q.Result.FieldByName('MAXIMUM_WAGE').Value);
  end;
  Result := V.Value['MAXIMUM_WAGE'];
end;

function TevHistData.SySuiFillerMaxWage(const SySuiNbr: Integer; const DefValue: Double;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  Val: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('ALTERNATE_TAXABLE_WAGE_BASE') then
  begin
    Q := TevQuery.CreateAsOf('select ALTERNATE_TAXABLE_WAGE_BASE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('ALTERNATE_TAXABLE_WAGE_BASE', Q.Result.FieldByName('ALTERNATE_TAXABLE_WAGE_BASE').Value);
  end;
  Val := V.Value['ALTERNATE_TAXABLE_WAGE_BASE'];
  Result := ConvertNull(Val, DefValue);
end;

function TevHistData.IsEeFederalExempt(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EXCLUDE_EE_FED') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EXCLUDE_EE_FED from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('EXEMPT_EXCLUDE_EE_FED', Q.Result.FieldByName('EXEMPT_EXCLUDE_EE_FED').Value);
  end;
  Result := V.Value['EXEMPT_EXCLUDE_EE_FED'] = 'E';
end;

function TevHistData.IsEeEeOasdiExempt(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYEE_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYEE_OASDI from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYEE_OASDI', Q.Result.FieldByName('EXEMPT_EMPLOYEE_OASDI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYEE_OASDI'] = 'Y';
end;

function TevHistData.IsEeErOasdiExempt(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_OASDI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_OASDI from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_OASDI', Q.Result.FieldByName('EXEMPT_EMPLOYER_OASDI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_OASDI'] = 'Y';
end;

function TevHistData.IsEeEeMedicareExempt(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYEE_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYEE_MEDICARE from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYEE_MEDICARE', Q.Result.FieldByName('EXEMPT_EMPLOYEE_MEDICARE').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYEE_MEDICARE'] = 'Y';
end;

function TevHistData.IsEeErMedicareExempt(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_MEDICARE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_MEDICARE from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_MEDICARE', Q.Result.FieldByName('EXEMPT_EMPLOYER_MEDICARE').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_MEDICARE'] = 'Y';
end;

function TevHistData.IsEeErFuiExempt(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_FUI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_FUI from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_FUI', Q.Result.FieldByName('EXEMPT_EMPLOYER_FUI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_FUI'] = 'Y';
end;

function TevHistData.IsEeFuiRateCreditOverride(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('FUI_RATE_CREDIT_OVERRIDE') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_RATE_CREDIT_OVERRIDE from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('FUI_RATE_CREDIT_OVERRIDE', Q.Result.FieldByName('FUI_RATE_CREDIT_OVERRIDE').Value);
  end;
  Result := not VarIsNull(V.Value['FUI_RATE_CREDIT_OVERRIDE']);
end;

function TevHistData.EeFuiRateCreditOverride(const EeNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('FUI_RATE_CREDIT_OVERRIDE') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_RATE_CREDIT_OVERRIDE from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('FUI_RATE_CREDIT_OVERRIDE', Q.Result.FieldByName('FUI_RATE_CREDIT_OVERRIDE').Value);
  end;
  Result := V.Value['FUI_RATE_CREDIT_OVERRIDE'];
end;

function TevHistData.EeHomeEeStateNbr(const EeNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('HOME_TAX_EE_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select HOME_TAX_EE_STATES_NBR from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('HOME_TAX_EE_STATES_NBR', Q.Result.FieldByName('HOME_TAX_EE_STATES_NBR').Value);
  end;
  Result := V.Value['HOME_TAX_EE_STATES_NBR'];
end;

function TevHistData.CoSuiTaxReturnCode(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('TAX_RETURN_CODE') then
  begin
    Q := TevQuery.CreateAsOf('select TAX_RETURN_CODE from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('TAX_RETURN_CODE', Q.Result.FieldByName('TAX_RETURN_CODE').Value);
  end;
  Result := ConvertNull(V.Value['TAX_RETURN_CODE'], '');
end;

function TevHistData.CoSuiReimburser(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('SUI_REIMBURSER') then
  begin
    Q := TevQuery.CreateAsOf('select SUI_REIMBURSER from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('SUI_REIMBURSER', Q.Result.FieldByName('SUI_REIMBURSER').Value);
  end;
  Result := V.Value['SUI_REIMBURSER'] = 'Y';
end;

function TevHistData.CoSuiMaxWage(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
  SySuiNbr: Integer;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('ALTERNATE_WAGE') then
  begin
    Q := TevQuery.CreateAsOf('select ALTERNATE_WAGE from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('ALTERNATE_WAGE', Q.Result.FieldByName('ALTERNATE_WAGE').Value);
  end;
  SySuiNbr := GetCoSySuiNbr(CoSuiNbr, CheckDate);
  Result := SySuiMaxWage(SySuiNbr, CheckDate);
  if V.Value['ALTERNATE_WAGE'] = 'Y' then
    Result := SySuiFillerMaxWage(SySuiNbr, Result, CheckDate);
  if (CoSuiTaxReturnCode(CoSuiNbr, CheckDate) = 'N') or CoSuiReimburser(CoSuiNbr, CheckDate) then
    Result := MaxInt;
end;

function TevHistData.CoSuiRate(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('RATE') then
  begin
    Q := TevQuery.CreateAsOf('select RATE from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('RATE', Q.Result.FieldByName('RATE').Value);
  end;
  Result := V.Value['RATE'];
end;

function TevHistData.GetCoSySuiNbr(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('SY_SUI_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select SY_SUI_NBR from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('SY_SUI_NBR', Q.Result.FieldByName('SY_SUI_NBR').Value);
  end;
  Result := V.Value['SY_SUI_NBR'];
end;

function TevHistData.GetCoSuiCoStatesNbr(const CoSuiNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoSuiNbr) + ':' + DateToStr(CheckDate);
  V := FCoSui1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoSui1.AddParams(ParamName);
  if not V.ValueExists('CO_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CO_STATES_NBR from CO_SUI where CO_SUI_NBR='+IntToStr(CoSuiNbr)+' and {AsOfDate<CO_SUI>}', CheckDate);
    V.AddValue('CO_STATES_NBR', Q.Result.FieldByName('CO_STATES_NBR').Value);
  end;
  Result := V.Value['CO_STATES_NBR'];
end;

function TevHistData.SySuiStateNbr(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('SY_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select SY_STATES_NBR from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('SY_STATES_NBR', Q.Result.FieldByName('SY_STATES_NBR').Value);
  end;
  Result := V.Value['SY_STATES_NBR'];
end;

function TevHistData.IsSySuiInactive(const SySuiNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SySuiNbr) + ':' + DateToStr(CheckDate);
  V := FSySui.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSySui.AddParams(ParamName);
  if not V.ValueExists('SUI_ACTIVE') then
  begin
    Q := TevQuery.CreateAsOf('select SUI_ACTIVE from SY_SUI where SY_SUI_NBR='+IntToStr(SySuiNbr)+' and {AsOfDate<SY_SUI>}', CheckDate);
    V.AddValue('SUI_ACTIVE', Q.Result.FieldByName('SUI_ACTIVE').Value);
  end;
  Result := V.Value['SUI_ACTIVE'] = 'N';
end;

function TevHistData.IsEdCodeStateExemptEmployerSdi(const SyStatesNbr: Integer;
  const EdCode: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr)+':'+EdCode + ':' + DateToStr(CheckDate);
  V := FStateExempt.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStateExempt.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_SDI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_SDI from SY_STATE_EXEMPTIONS where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and E_D_CODE_TYPE=''' + EdCode + ''' and {AsOfDate<SY_STATE_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_SDI', Q.Result.FieldByName('EXEMPT_EMPLOYER_SDI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_SDI'] = 'Y';
end;

function TevHistData.IsEdCodeStateExempt(const SyStatesNbr: Integer;
  const EdCode: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr)+':'+EdCode + ':' + DateToStr(CheckDate);
  V := FStateExempt.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStateExempt.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_STATE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_STATE from SY_STATE_EXEMPTIONS where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and E_D_CODE_TYPE=''' + EdCode + ''' and {AsOfDate<SY_STATE_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_STATE', Q.Result.FieldByName('EXEMPT_STATE').Value);
  end;
  Result := V.Value['EXEMPT_STATE'] = 'E';
end;

function TevHistData.IsEdCodeEeSdiExempt(const SyStatesNbr: Integer;
  const EdCode: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr)+':'+EdCode + ':' + DateToStr(CheckDate);
  V := FStateExempt.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStateExempt.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYEE_SDI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYEE_SDI from SY_STATE_EXEMPTIONS where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and E_D_CODE_TYPE=''' + EdCode + ''' and {AsOfDate<SY_STATE_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYEE_SDI', Q.Result.FieldByName('EXEMPT_EMPLOYEE_SDI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYEE_SDI'] = 'Y';
end;

function TevHistData.IsEdCodeEeSuiExempt(const SyStatesNbr: Integer;
  const EdCode: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr)+':'+EdCode + ':' + DateToStr(CheckDate);
  V := FStateExempt.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStateExempt.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYEE_SUI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYEE_SUI from SY_STATE_EXEMPTIONS where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and E_D_CODE_TYPE=''' + EdCode + ''' and {AsOfDate<SY_STATE_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYEE_SUI', Q.Result.FieldByName('EXEMPT_EMPLOYEE_SUI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYEE_SUI'] = 'Y';
end;

function TevHistData.IsEdCodeErSuiExempt(const SyStatesNbr: Integer;
  const EdCode: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr)+':'+EdCode + ':' + DateToStr(CheckDate);
  V := FStateExempt.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStateExempt.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EMPLOYER_SUI') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EMPLOYER_SUI from SY_STATE_EXEMPTIONS where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and E_D_CODE_TYPE=''' + EdCode + ''' and {AsOfDate<SY_STATE_EXEMPTIONS>}', CheckDate);
    V.AddValue('EXEMPT_EMPLOYER_SUI', Q.Result.FieldByName('EXEMPT_EMPLOYER_SUI').Value);
  end;
  Result := V.Value['EXEMPT_EMPLOYER_SUI'] = 'Y';
end;

function TevHistData.EeStateEeSdiExemptExclude(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('EE_SDI_EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_SDI_EXEMPT_EXCLUDE from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('EE_SDI_EXEMPT_EXCLUDE', Q.Result.FieldByName('EE_SDI_EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['EE_SDI_EXEMPT_EXCLUDE'];
end;

function TevHistData.EeStateEeSuiExemptExclude(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('EE_SUI_EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_SUI_EXEMPT_EXCLUDE from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('EE_SUI_EXEMPT_EXCLUDE', Q.Result.FieldByName('EE_SUI_EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['EE_SUI_EXEMPT_EXCLUDE'];
end;

function TevHistData.GetEeStateCoStateNbr(const AEeStatesNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('CO_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CO_STATES_NBR from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('CO_STATES_NBR', Q.Result.FieldByName('CO_STATES_NBR').Value);
  end;
  Result := V.Value['CO_STATES_NBR'];
end;

function TevHistData.GetEeStatesNbr(const AEeNbr, ACoStateNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeNbr)+':'+IntToStr(ACoStateNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates1.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates1.AddParams(ParamName);
  if not V.ValueExists('EE_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select EE_STATES_NBR from EE_STATES where EE_NBR='+IntToStr(AEeNbr)+
      ' and CO_STATES_NBR='+IntToStr(ACoStateNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('EE_STATES_NBR', Q.Result.FieldByName('EE_STATES_NBR').Value);
  end;
  Result := V.Value['EE_STATES_NBR'];
end;

function TevHistData.SyLocalType(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FLocals.AddParams(ParamName);
  if not V.ValueExists('LOCAL_TYPE') then
  begin
    Q := TevQuery.CreateAsOf('select x.LOCAL_TYPE from SY_LOCALS x '+
    '  where {AsOfDate<x>} and x.SY_LOCALS_NBR='+IntToStr(ASyLocalsNbr), CheckDate);
    V.AddValue('LOCAL_TYPE', Q.Result.FieldByName('LOCAL_TYPE').AsString);
  end;
  Result := V.Value['LOCAL_TYPE'];
end;

function TevHistData.GetPerson(const AEeNbr: Integer; const CheckDate: TDateTime): IevPerson;
var
  Q: IevQuery;
begin
  Q := TevQuery.Create('select CL_PERSON_NBR from EE where EE_NBR='+IntToStr(AEeNbr)+' and {AsOfNow<EE>}');
  if FPeople.ValueExists(Q.Result.FieldByName('CL_PERSON_NBR').AsString) then
    Result := IInterface(FPeople.Value[Q.Result.FieldByName('CL_PERSON_NBR').AsString]) as IevPerson
  else
  begin
    Result := TevPerson.LoadFromDB(Self, AEeNbr, CheckDate);
    FPeople.AddValue(Q.Result.FieldByName('CL_PERSON_NBR').AsString, Result);
  end;
end;

(*
function TevHistData.GetClEdNbr(const EdCode: String;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists('CL_E_DS_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CL_E_DS_NBR from CL_E_DS '+
    ' where E_D_CODE_TYPE=''' + EdCode + ''' and {AsOfDate<CL_E_DS>}', CheckDate);
    V.AddValue('CL_E_DS_NBR', Q.Result.FieldByName('CL_E_DS_NBR').Value);
  end;
  Result := V.Value['CL_E_DS_NBR'];
end;
*)

function TevHistData.FuiRateCredit(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('FUI_RATE_CREDIT') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_RATE_CREDIT from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('FUI_RATE_CREDIT', Q.Result['FUI_RATE_CREDIT']);
  end;
  Result := FFedTaxTable.Value['FUI_RATE_CREDIT'];
end;

function TevHistData.FuiRateReal(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('FUI_RATE_REAL') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_RATE_REAL from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('FUI_RATE_REAL', Q.Result['FUI_RATE_REAL']);
  end;
  Result := FFedTaxTable.Value['FUI_RATE_REAL'];
end;

function TevHistData.FuiWageLimit(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('FUI_WAGE_LIMIT') then
  begin
    Q := TevQuery.CreateAsOf('select FUI_WAGE_LIMIT from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('FUI_WAGE_LIMIT', Q.Result['FUI_WAGE_LIMIT']);
  end;
  Result := FFedTaxTable.Value['FUI_WAGE_LIMIT'];
end;

function TevHistData.EeOasdiRate(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('EE_OASDI_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_OASDI_RATE from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('EE_OASDI_RATE', Q.Result['EE_OASDI_RATE']);
  end;
  Result := FFedTaxTable.Value['EE_OASDI_RATE'];
end;

function TevHistData.ErOasdiRate(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('ER_OASDI_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select ER_OASDI_RATE from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('ER_OASDI_RATE', Q.Result['ER_OASDI_RATE']);
  end;
  Result := FFedTaxTable.Value['ER_OASDI_RATE'];
end;

function TevHistData.OasdiWageLimit(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('OASDI_WAGE_LIMIT') then
  begin
    Q := TevQuery.CreateAsOf('select OASDI_WAGE_LIMIT from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('OASDI_WAGE_LIMIT', Q.Result['OASDI_WAGE_LIMIT']);
  end;
  Result := FFedTaxTable.Value['OASDI_WAGE_LIMIT'];
end;

function TevHistData.MedicareThresholdRate(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('EE_MED_TH_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_MED_TH_RATE from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('EE_MED_TH_RATE', Q.Result['EE_MED_TH_RATE']);
  end;
  Result := FFedTaxTable.Value['EE_MED_TH_RATE'];
end;

function TevHistData.MedicareThresholdValue(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('EE_MED_TH_LIMIT') then
  begin
    Q := TevQuery.CreateAsOf('select EE_MED_TH_LIMIT from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('EE_MED_TH_LIMIT', Q.Result['EE_MED_TH_LIMIT']);
  end;
  Result := FFedTaxTable.Value['EE_MED_TH_LIMIT'];
end;

function TevHistData.MedicareRate(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('MEDICARE_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select MEDICARE_RATE from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('MEDICARE_RATE', Q.Result['MEDICARE_RATE']);
  end;
  Result := FFedTaxTable.Value['MEDICARE_RATE'];
end;

function TevHistData.MedicareWageLimit(const CheckDate: TDateTime): Double;
var
  Q: IevQuery;
begin
  if not FFedTaxTable.ValueExists('MEDICARE_WAGE_LIMIT') then
  begin
    Q := TevQuery.CreateAsOf('select MEDICARE_WAGE_LIMIT from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}', CheckDate);
    FFedTaxTable.AddValue('MEDICARE_WAGE_LIMIT', Q.Result['MEDICARE_WAGE_LIMIT']);
  end;
  Result := FFedTaxTable.Value['MEDICARE_WAGE_LIMIT'];
end;

function TevHistData.IsCoStateExempt(const ACoNbr: Integer;
  const AState: String; const CheckDate: TDateTime): Boolean;
begin
  Result := IsCoStateExempt(GetCoStatesNbr(ACoNbr, AState, CheckDate), CheckDate);
end;

function TevHistData.GetEeStatesNbr(const AEeNbr: Integer; const AState: String;
  const CheckDate: TDateTime): Integer;
begin
  Result := GetEeStatesNbr(AEeNbr, GetCoStatesNbr(EeCoNbr(AEeNbr, CheckDate), AState, CheckDate), CheckDate);
end;

function TevHistData.EeCoNbr(const EeNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('CO_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select CO_NBR from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('CO_NBR', Q.Result.FieldByName('CO_NBR').Value);
  end;
  Result := V.Value['CO_NBR'];
end;

function TevHistData.IsEdCodeStateExempt(const AState, EdCode: String;
  const CheckDate: TDateTime): Boolean;
begin
  Result := IsEdCodeStateExempt(GetSyStateNbr(AState, CheckDate), EDCode, CheckDate);
end;

function TevHistData.GetSyStateNbr(const State: String;
  const CheckDate: TDateTime): Integer;
var
  Q: IevQuery;
begin
  if not FSyStates.ValueExists(State) then
  begin
    Q := TevQuery.CreateAsOf('select SY_STATES_NBR from SY_STATES where {AsOfDate<SY_STATES>} and STATE='''+State+'''', CheckDate);
    FSyStates.AddValue(State, Q.Result['SY_STATES_NBR']);
  end;
  Result := FSyStates.Value[State];
end;

function TevHistData.ClEmployeeExemptExcludeState(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdNbr) + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists(State+':EMPLOYEE_EXEMPT_EXCLUDE_STATE') then
  begin
    Q := TevQuery.CreateAsOf('select EMPLOYEE_EXEMPT_EXCLUDE_STATE from CL_E_D_STATE_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdNbr)+
    ' and SY_STATES_NBR='+IntTOStr(GetSyStateNbr(State, CheckDate))+
    ' and {AsOfDate<CL_E_D_STATE_EXMPT_EXCLD>}', CheckDate);
    V.AddValue(State+':EMPLOYEE_EXEMPT_EXCLUDE_STATE', Q.Result.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_STATE').Value);
  end;
  Result := ConvertNull(V.Value[State+':EMPLOYEE_EXEMPT_EXCLUDE_STATE'], '');
end;

function TevHistData.IsEdCodeClStateExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
begin
  Result := ClEmployeeExemptExcludeState(ClEdNbr, State, CheckDate) = GROUP_BOX_EXE;
end;

function TevHistData.IsEdCodeClEeSuiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdNbr) + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists(State+':EMPLOYEE_EXEMPT_EXCLUDE_SUI') then
  begin
    Q := TevQuery.CreateAsOf('select EMPLOYEE_EXEMPT_EXCLUDE_SUI from CL_E_D_STATE_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdNbr)+
    ' and SY_STATES_NBR='+IntTOStr(GetSyStateNbr(State, CheckDate))+
    ' and {AsOfDate<CL_E_D_STATE_EXMPT_EXCLD>}', CheckDate);
    V.AddValue(State+':EMPLOYEE_EXEMPT_EXCLUDE_SUI', Q.Result.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SUI').Value);
  end;
  Result := V.Value[State+':EMPLOYEE_EXEMPT_EXCLUDE_SUI'] = GROUP_BOX_EXE;
end;

function TevHistData.IsEdCodeClErSuiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdNbr) + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists(State+':EMPLOYER_EXEMPT_EXCLUDE_SUI') then
  begin
    Q := TevQuery.CreateAsOf('select EMPLOYER_EXEMPT_EXCLUDE_SUI from CL_E_D_STATE_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdNbr)+
    ' and SY_STATES_NBR='+IntTOStr(GetSyStateNbr(State, CheckDate))+
    ' and {AsOfDate<CL_E_D_STATE_EXMPT_EXCLD>}', CheckDate);
    V.AddValue(State+':EMPLOYER_EXEMPT_EXCLUDE_SUI', Q.Result.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SUI').Value);
  end;
  Result := V.Value[State+':EMPLOYER_EXEMPT_EXCLUDE_SUI'] = GROUP_BOX_EXE;
end;

function TevHistData.IsEdCodeClEeSdiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdNbr) + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists(State+':EMPLOYEE_EXEMPT_EXCLUDE_SDI') then
  begin
    Q := TevQuery.CreateAsOf('select EMPLOYEE_EXEMPT_EXCLUDE_SDI from CL_E_D_STATE_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdNbr)+
    ' and SY_STATES_NBR='+IntTOStr(GetSyStateNbr(State, CheckDate))+
    ' and {AsOfDate<CL_E_D_STATE_EXMPT_EXCLD>}', CheckDate);
    V.AddValue(State+':EMPLOYEE_EXEMPT_EXCLUDE_SDI', Q.Result.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_SDI').Value);
  end;
  Result := V.Value[State+':EMPLOYEE_EXEMPT_EXCLUDE_SDI'] = GROUP_BOX_EXE;
end;

function TevHistData.IsEdCodeClErSdiExempt(const ClEdNbr: Integer; const State: String; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdNbr) + ':' + DateToStr(CheckDate);
  V := FEdCodes.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEdCodes.AddParams(ParamName);
  if not V.ValueExists(State+':EMPLOYER_EXEMPT_EXCLUDE_SDI') then
  begin
    Q := TevQuery.CreateAsOf('select EMPLOYER_EXEMPT_EXCLUDE_SDI from CL_E_D_STATE_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdNbr)+
    ' and SY_STATES_NBR='+IntTOStr(GetSyStateNbr(State, CheckDate))+
    ' and {AsOfDate<CL_E_D_STATE_EXMPT_EXCLD>}', CheckDate);
    V.AddValue(State+':EMPLOYER_EXEMPT_EXCLUDE_SDI', Q.Result.FieldByName('EMPLOYER_EXEMPT_EXCLUDE_SDI').Value);
  end;
  Result := V.Value[State+':EMPLOYER_EXEMPT_EXCLUDE_SDI'] = GROUP_BOX_EXE;
end;

function TevHistData.IsCoLocalExempt(const CoLocalTaxNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoLocalTaxNbr) + ':' + DateToStr(CheckDate);
  V := FCoLocalTax.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FCoLocalTax.AddParams(ParamName);
  if not V.ValueExists('EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT, SY_LOCALS_NBR from CO_LOCAL_TAX where CO_LOCAL_TAX_NBR='+IntToStr(CoLocalTaxNbr)+
    ' and {AsOfDate<CO_LOCAL_TAX>}', CheckDate);
    V.AddValue('EXEMPT', Q.Result.FieldByName('EXEMPT').Value);
    V.AddValue('SY_LOCALS_NBR', Q.Result.FieldByName('SY_LOCALS_NBR').Value);
  end;
  Result := V.Value['EXEMPT'] = 'Y';
end;

function TevHistData.IsEeLocalExempt(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EXCLUDE, CO_LOCAL_TAX_NBR from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('EXEMPT_EXCLUDE', Q.Result.FieldByName('EXEMPT_EXCLUDE').Value);
    V.AddValue('CO_LOCAL_TAX_NBR', Q.Result.FieldByName('CO_LOCAL_TAX_NBR').Value);
  end;
  Result := V.Value['EXEMPT_EXCLUDE'] = 'E';
end;

function TevHistData.EeLocalIncludeInPretax(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('INCLUDE_IN_PRETAX') then
  begin
    Q := TevQuery.CreateAsOf('select INCLUDE_IN_PRETAX from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('INCLUDE_IN_PRETAX', Q.Result.FieldByName('INCLUDE_IN_PRETAX').Value);
  end;
  Result := V.Value['INCLUDE_IN_PRETAX'] = 'Y';
end;

function TevHistData.EeLocalDeduct(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('DEDUCT') then
  begin
    Q := TevQuery.CreateAsOf('select DEDUCT from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('DEDUCT', Q.Result.FieldByName('DEDUCT').Value);
  end;
  Result := V.Value['DEDUCT'];
end;

function TevHistData.EeLocalExemptExclude(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EXCLUDE from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('EXEMPT_EXCLUDE', Q.Result.FieldByName('EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['EXEMPT_EXCLUDE'];
end;

function TevHistData.EeLocalOverrideLocalTaxType(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('OVERRIDE_LOCAL_TAX_TYPE') then
  begin
    Q := TevQuery.CreateAsOf('select OVERRIDE_LOCAL_TAX_TYPE from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('OVERRIDE_LOCAL_TAX_TYPE', Q.Result.FieldByName('OVERRIDE_LOCAL_TAX_TYPE').Value);
  end;
  Result := V.Value['OVERRIDE_LOCAL_TAX_TYPE'];
end;

function TevHistData.EeLocalOverrideLocalTaxValue(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('OVERRIDE_LOCAL_TAX_VALUE') then
  begin
    Q := TevQuery.CreateAsOf('select OVERRIDE_LOCAL_TAX_VALUE from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('OVERRIDE_LOCAL_TAX_VALUE', Q.Result.FieldByName('OVERRIDE_LOCAL_TAX_VALUE').Value);
  end;
  Result := ConvertNull(V.Value['OVERRIDE_LOCAL_TAX_VALUE'],0);
end;

function TevHistData.EeLocalMiscellaneousAmount(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('MISCELLANEOUS_AMOUNT') then
  begin
    Q := TevQuery.CreateAsOf('select MISCELLANEOUS_AMOUNT from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('MISCELLANEOUS_AMOUNT', Q.Result.FieldByName('MISCELLANEOUS_AMOUNT').Value);
  end;
  Result := ConvertNull(V.Value['MISCELLANEOUS_AMOUNT'],0);
end;

function TevHistData.EeLocalPercentageOfTaxWages(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('PERCENTAGE_OF_TAX_WAGES') then
  begin
    Q := TevQuery.CreateAsOf('select PERCENTAGE_OF_TAX_WAGES from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('PERCENTAGE_OF_TAX_WAGES', Q.Result.FieldByName('PERCENTAGE_OF_TAX_WAGES').Value);
  end;
  Result := ConvertNull(V.Value['PERCENTAGE_OF_TAX_WAGES'], 0);
end;

function TevHistData.SyStatesSuiReciprocate(const SyStatesNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr) + ':' + DateToStr(CheckDate);
  V := FStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStates.AddParams(ParamName);
  if not V.ValueExists('SUI_RECIPROCATE') then
  begin
    Q := TevQuery.CreateAsOf('select SUI_RECIPROCATE from SY_STATES where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and {AsOfDate<SY_STATES>}', CheckDate);
    V.AddValue('SUI_RECIPROCATE', Q.Result.FieldByName('SUI_RECIPROCATE').Value);
  end;
  Result := V.Value['SUI_RECIPROCATE'];
end;

function TevHistData.SyStatesErSdiRate(const SyStatesNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr) + ':' + DateToStr(CheckDate);
  V := FStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStates.AddParams(ParamName);
  if not V.ValueExists('ER_SDI_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select ER_SDI_RATE from SY_STATES where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and {AsOfDate<SY_STATES>}', CheckDate);
    V.AddValue('ER_SDI_RATE', Q.Result.FieldByName('ER_SDI_RATE').Value);
  end;
  Result := ConvertNull(V.Value['ER_SDI_RATE'],0);
end;

function TevHistData.GetEeLocalCoLocalTaxNbr(const EeLocalsNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FEeLocals.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeLocals.AddParams(ParamName);
  if not V.ValueExists('CO_LOCAL_TAX_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EXCLUDE, CO_LOCAL_TAX_NBR from EE_LOCALS where EE_LOCALS_NBR='+IntToStr(EeLocalsNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    V.AddValue('EXEMPT_EXCLUDE', Q.Result.FieldByName('EXEMPT_EXCLUDE').Value);
    V.AddValue('CO_LOCAL_TAX_NBR', Q.Result.FieldByName('CO_LOCAL_TAX_NBR').Value);
  end;
  Result := V.Value['CO_LOCAL_TAX_NBR'];
end;

function TevHistData.JobHasLocalTax(const CoJobsNbr,
  CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  L: IisListOfValues;
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoJobsNbr) + ':' + DateToStr(CheckDate);
  V := FJobs.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FJobs.AddParams(ParamName);

  if not V.ValueExists('LOCALS') then
  begin
    L := TisListOfValues.Create;
    V.AddValue('LOCALS', L);

    Q := TevQuery.CreateAsOf('select CO_LOCAL_TAX_NBR from CO_JOBS_LOCALS where CO_JOBS_NBR='+IntToStr(CoJobsNbr)+
    ' and {AsOfDate<CO_JOBS_LOCALS>}', CheckDate);

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      L.AddValue(Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString, 1);
      Q.Result.Next;
    end;

  end
  else
    L := IInterface(V.Value['LOCALS']) as IisListOfValues;

  Result := L.ValueExists(IntToStr(CoLocalTaxNbr));
end;

function TevHistData.TeamHasLocalTax(const CoTeamNbr,
  CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  L: IisListOfValues;
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoTeamNbr) + ':' + DateToStr(CheckDate);
  V := FTeam.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FTeam.AddParams(ParamName);

  if not V.ValueExists('LOCALS') then
  begin
    L := TisListOfValues.Create;
    V.AddValue('LOCALS', L);

    Q := TevQuery.CreateAsOf('select CO_LOCAL_TAX_NBR from CO_TEAM_LOCALS where CO_TEAM_NBR='+IntToStr(CoTeamNbr)+
    ' and {AsOfDate<CO_TEAM_LOCALS>}', CheckDate);

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      L.AddValue(Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString, 1);
      Q.Result.Next;
    end;

  end
  else
    L := IInterface(V.Value['LOCALS']) as IisListOfValues;

  Result := L.ValueExists(IntToStr(CoLocalTaxNbr));
end;

function TevHistData.DepartmentHasLocalTax(const CoDepartmentNbr,
  CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  L: IisListOfValues;
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoDepartmentNbr) + ':' + DateToStr(CheckDate);
  V := FDepartment.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FDepartment.AddParams(ParamName);

  if not V.ValueExists('LOCALS') then
  begin
    L := TisListOfValues.Create;
    V.AddValue('LOCALS', L);

    Q := TevQuery.CreateAsOf('select CO_LOCAL_TAX_NBR from CO_DEPARTMENT_LOCALS where CO_DEPARTMENT_NBR='+IntToStr(CoDepartmentNbr)+
    ' and {AsOfDate<CO_DEPARTMENT_LOCALS>}', CheckDate);

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      L.AddValue(Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString, 1);
      Q.Result.Next;
    end;

  end
  else
    L := IInterface(V.Value['LOCALS']) as IisListOfValues;

  Result := L.ValueExists(IntToStr(CoLocalTaxNbr));
end;

function TevHistData.BranchHasLocalTax(const CoBranchNbr,
  CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  L: IisListOfValues;
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoBranchNbr) + ':' + DateToStr(CheckDate);
  V := FBranch.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FBranch.AddParams(ParamName);

  if not V.ValueExists('LOCALS') then
  begin
    L := TisListOfValues.Create;
    V.AddValue('LOCALS', L);

    Q := TevQuery.CreateAsOf('select CO_LOCAL_TAX_NBR from CO_BRANCH_LOCALS where CO_BRANCH_NBR='+IntToStr(CoBranchNbr)+
    ' and {AsOfDate<CO_BRANCH_LOCALS>}', CheckDate);

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      L.AddValue(Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString, 1);
      Q.Result.Next;
    end;

  end
  else
    L := IInterface(V.Value['LOCALS']) as IisListOfValues;

  Result := L.ValueExists(IntToStr(CoLocalTaxNbr));
end;

function TevHistData.DivisionHasLocalTax(const CoDivisionNbr,
  CoLocalTaxNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  L: IisListOfValues;
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoDivisionNbr) + ':' + DateToStr(CheckDate);
  V := FDivision.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FDivision.AddParams(ParamName);

  if not V.ValueExists('LOCALS') then
  begin
    L := TisListOfValues.Create;
    V.AddValue('LOCALS', L);

    Q := TevQuery.CreateAsOf('select CO_LOCAL_TAX_NBR from CO_DIVISION_LOCALS where CO_DIVISION_NBR='+IntToStr(CoDivisionNbr)+
    ' and {AsOfDate<CO_DIVISION_LOCALS>}', CheckDate);

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      L.AddValue(Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString, 1);
      Q.Result.Next;
    end;

  end
  else
    L := IInterface(V.Value['LOCALS']) as IisListOfValues;

  Result := L.ValueExists(IntToStr(CoLocalTaxNbr));
end;

function TevHistData.IsEdCodeSyLocalExempt(const EdCode: String;
   const SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := EdCode + ':' + IntToStr(SyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FSyLocalExempt.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FSyLocalExempt.AddParams(ParamName);
  if not V.ValueExists('EXEMPT') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT from SY_LOCAL_EXEMPTIONS where {AsOfDate<SY_LOCAL_EXEMPTIONS>} and SY_LOCALS_NBR='+IntToStr(SyLocalsNbr)+
    ' and E_D_CODE_TYPE='''+EdCode+'''', CheckDate);
    V.AddValue('EXEMPT', Q.Result.FieldByName('EXEMPT').AsString);
  end;
  Result := V.Value['EXEMPT'] = 'E';
end;

function TevHistData.SyLocalFollowStateEdExempt(
  const ASyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := V.Value['PRINT_RETURN_IF_ZERO'] = 'Y';
end;

function TevHistData.SyLocalCalcMethod(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Char;
var
  V: IisListOfValues;
  ParamName, s: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  s := V.Value['CALCULATION_METHOD'];
  Result := s[1];
end;

function TevHistData.SyLocalMonthlyMaximum(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := V.Value['PAY_WITH_STATE'] = 'Y';
end;

function TevHistData.SyLocalWageMaximum(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := ConvertNull(V.Value['WAGE_MAXIMUM'], 0);
end;

function TevHistData.SyLocalWageMinimum(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := ConvertNull(V.Value['WAGE_MINIMUM'], 0);
end;

procedure TevHistData.LoadSyLocals(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime);
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  if not Assigned(V) then
  begin
    V := FLocals.AddParams(ParamName);
    Q := TevQuery.CreateAsOf('select s.STATE, x.TAX_TYPE, x.PRINT_RETURN_IF_ZERO, x.CALCULATION_METHOD,'+
    '  x.PAY_WITH_STATE, x.WAGE_MAXIMUM, x.WAGE_MINIMUM, x.TAX_RATE, x.QUARTERLY_MINIMUM_THRESHOLD, '+
    '  x.MISCELLANEOUS_AMOUNT, x.LOCAL_MINIMUM_WAGE, x.NAME, x.TAX_MAXIMUM, x.QUARTERLY_MINIMUM_THRESHOLD '+
    '  from SY_LOCALS x '+
    '  join SY_STATES s on {AsOfDate<s>} and s.SY_STATES_NBR=x.SY_STATES_NBR'+
    '  where {AsOfDate<x>} and x.SY_LOCALS_NBR='+IntToStr(ASyLocalsNbr), CheckDate);

    V.AddValue('STATE', Q.Result.FieldByName('STATE').AsString);
    V.AddValue('TAX_TYPE', Q.Result.FieldByName('TAX_TYPE').AsString);
    V.AddValue('PRINT_RETURN_IF_ZERO', Q.Result.FieldByName('PRINT_RETURN_IF_ZERO').AsString);
    V.AddValue('CALCULATION_METHOD', Q.Result.FieldByName('CALCULATION_METHOD').AsString);
    V.AddValue('PAY_WITH_STATE', Q.Result.FieldByName('PAY_WITH_STATE').AsString);
    V.AddValue('WAGE_MAXIMUM', Q.Result['WAGE_MAXIMUM']);
    V.AddValue('WAGE_MINIMUM', Q.Result['WAGE_MINIMUM']);
    V.AddValue('TAX_RATE', Q.Result['TAX_RATE']);
    V.AddValue('TAX_MAXIMUM', Q.Result['TAX_MAXIMUM']);
    V.AddValue('QUARTERLY_MINIMUM_THRESHOLD', Q.Result['QUARTERLY_MINIMUM_THRESHOLD']);
    V.AddValue('MISCELLANEOUS_AMOUNT', Q.Result['MISCELLANEOUS_AMOUNT']);
    V.AddValue('LOCAL_MINIMUM_WAGE', Q.Result['LOCAL_MINIMUM_WAGE']);
    V.AddValue('NAME', Q.Result['NAME']);
  end;
end;

function TevHistData.SyLocalName(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := V.Value['NAME'];
end;

function TevHistData.SyLocalMinimumWage(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := ConvertNull(V.Value['LOCAL_MINIMUM_WAGE'], 0);
end;

function TevHistData.SyLocalMiscAmount(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := ConvertNull(V.Value['MISCELLANEOUS_AMOUNT'], 0);
end;

function TevHistData.SyLocalQuarterlyMinimumThreshold(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := ConvertNull(V.Value['QUARTERLY_MINIMUM_THRESHOLD'], 0);
end;

function TevHistData.SyLocalRate(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := ConvertNull(V.Value['TAX_RATE'], 0);
end;

function TevHistData.SyLocalTaxMinimum(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := V.Value['TAX_MAXIMUM'];
end;

function TevHistData.SyLocalHasTaxMinimum(const ASyLocalsNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  ParamName: String;
begin
  LoadSyLocals(ASyLocalsNbr, CheckDate);
  ParamName := IntToStr(ASyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FLocals.ParamsByName(ParamName);
  Result := not VarIsNull(V.Value['TAX_MAXIMUM']);
end;

function TevHistData.SyStatesEeSdiRate(const SyStatesNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr) + ':' + DateToStr(CheckDate);
  V := FStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStates.AddParams(ParamName);
  if not V.ValueExists('EE_SDI_RATE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_SDI_RATE from SY_STATES where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and {AsOfDate<SY_STATES>}', CheckDate);
    V.AddValue('EE_SDI_RATE', Q.Result.FieldByName('EE_SDI_RATE').Value);
  end;
  Result := ConvertNull(V.Value['EE_SDI_RATE'],0);
end;

function TevHistData.SyStatesWeeklySdiTaxCap(const SyStatesNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr) + ':' + DateToStr(CheckDate);
  V := FStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStates.AddParams(ParamName);
  if not V.ValueExists('WEEKLY_SDI_TAX_CAP') then
  begin
    Q := TevQuery.CreateAsOf('select WEEKLY_SDI_TAX_CAP from SY_STATES where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and {AsOfDate<SY_STATES>}', CheckDate);
    V.AddValue('WEEKLY_SDI_TAX_CAP', Q.Result.FieldByName('WEEKLY_SDI_TAX_CAP').Value);
  end;
  Result := ConvertNull(V.Value['WEEKLY_SDI_TAX_CAP'],0);
end;

function TevHistData.SyStatesErSdiMaximumWage(const SyStatesNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr) + ':' + DateToStr(CheckDate);
  V := FStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStates.AddParams(ParamName);
  if not V.ValueExists('ER_SDI_MAXIMUM_WAGE') then
  begin
    Q := TevQuery.CreateAsOf('select ER_SDI_MAXIMUM_WAGE from SY_STATES where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and {AsOfDate<SY_STATES>}', CheckDate);
    V.AddValue('ER_SDI_MAXIMUM_WAGE', Q.Result.FieldByName('ER_SDI_MAXIMUM_WAGE').Value);
  end;
  Result := ConvertNull(V.Value['ER_SDI_MAXIMUM_WAGE'],0);
end;

function TevHistData.SyStatesEeSdiMaximumWage(const SyStatesNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(SyStatesNbr) + ':' + DateToStr(CheckDate);
  V := FStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FStates.AddParams(ParamName);
  if not V.ValueExists('EE_SDI_MAXIMUM_WAGE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_SDI_MAXIMUM_WAGE from SY_STATES where SY_STATES_NBR='+IntToStr(SyStatesNbr)+
    ' and {AsOfDate<SY_STATES>}', CheckDate);
    V.AddValue('EE_SDI_MAXIMUM_WAGE', Q.Result.FieldByName('EE_SDI_MAXIMUM_WAGE').Value);
  end;
  Result := ConvertNull(V.Value['EE_SDI_MAXIMUM_WAGE'],0);
end;

function TevHistData.CoLocalTaxNbr(const CoNbr, SyLocalsNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(CoNbr) + ':' + IntToStr(SyLocalsNbr) + ':' + DateToStr(CheckDate);
  if not FCoLocalTax1.ValueExists(ParamName) then
  begin
    Q := TevQuery.CreateAsOf('select CO_LOCAL_TAX_NBR from CO_LOCAL_TAX where CO_NBR='+IntToStr(CoNbr)+
    ' and SY_LOCALS_NBR='+IntToStr(SyLocalsNbr)+
    ' and {AsOfDate<CO_LOCAL_TAX>}', CheckDate);
    FCoLocalTax1.AddValue(ParamName, Q.Result.FieldByName('CO_LOCAL_TAX_NBR').Value);
  end;
  Result := FCoLocalTax1.Value[ParamName];
end;

function TevHistData.ClReciprocateSui(
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := DateToStr(CheckDate);
  V := FClient.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClient.AddParams(ParamName);
  if not V.ValueExists('RECIPROCATE_SUI') then
  begin
    Q := TevQuery.CreateAsOf('select RECIPROCATE_SUI from CL where '+
    ' {AsOfDate<CL>}', CheckDate);
    V.AddValue('RECIPROCATE_SUI', Q.Result.FieldByName('RECIPROCATE_SUI').Value);
  end;
  Result := V.Value['RECIPROCATE_SUI'] = 'Y';
end;

function TevHistData.GetEeStatesErSuiExemptExclude(
  const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('ER_SUI_EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select ER_SUI_EXEMPT_EXCLUDE from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('ER_SUI_EXEMPT_EXCLUDE', Q.Result.FieldByName('ER_SUI_EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['ER_SUI_EXEMPT_EXCLUDE'];
end;

function TevHistData.GetEeStateSdiApplyCoStateNbr(
  const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('SDI_APPLY_CO_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select SDI_APPLY_CO_STATES_NBR from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('SDI_APPLY_CO_STATES_NBR', Q.Result.FieldByName('SDI_APPLY_CO_STATES_NBR').Value);
  end;
  Result := V.Value['SDI_APPLY_CO_STATES_NBR'];
end;

function TevHistData.GetEeStateSuiApplyCoStateNbr(
  const AEeStatesNbr: Integer; const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('SUI_APPLY_CO_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select SUI_APPLY_CO_STATES_NBR from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('SUI_APPLY_CO_STATES_NBR', Q.Result.FieldByName('SUI_APPLY_CO_STATES_NBR').Value);
  end;
  Result := V.Value['SUI_APPLY_CO_STATES_NBR'];
end;

function TevHistData.EeStatesForSui(const EeNbr, CoStatesNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + IntToStr(CoStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates2.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates2.AddParams(ParamName);
  if not V.ValueExists('EE_STATES_NBR') then
  begin
    Q := TevQuery.CreateAsOf('select EE_STATES_NBR from EE_STATES where EE_NBR='+IntToStr(EeNbr)+
    ' and SUI_APPLY_CO_STATES_NBR='+IntToStr(CoStatesNbr)+
    ' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('EE_STATES_NBR', Q.Result.FieldByName('EE_STATES_NBR').Value);
  end;
  Result := V.Value['EE_STATES_NBR'];
end;

function TevHistData.GetEeLocalsNbr(const EeNbr, CoLocalTaxNbr: Integer;
  const CheckDate: TDateTime): Integer;
var
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + IntToStr(CoLocalTaxNbr) + ':' + DateToStr(CheckDate);
  if not FEeLocals1.ValueExists(ParamName) then
  begin
    Q := TevQuery.CreateAsOf('select EE_LOCALS_NBR from EE_LOCALS where EE_NBR='+IntToStr(EeNbr)+
    ' and CO_LOCAL_TAX_NBR='+IntToStr(CoLocalTaxNbr)+
    ' and {AsOfDate<EE_LOCALS>}', CheckDate);
    FEeLocals1.AddValue(ParamName, Q.Result.FieldByName('EE_LOCALS_NBR').Value);
  end;
  Result := FEeLocals1.Value[ParamName];
end;

function TevHistData.IgnoreFICA(const EeNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(EeNbr) + ':' + DateToStr(CheckDate);
  V := FEe.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEe.AddParams(ParamName);
  if not V.ValueExists('MAKEUP_FICA_ON_CLEANUP_PR') then
  begin
    Q := TevQuery.CreateAsOf('select MAKEUP_FICA_ON_CLEANUP_PR from EE where EE_NBR='+IntToStr(EeNbr)+' and {AsOfDate<EE>}', CheckDate);
    V.AddValue('MAKEUP_FICA_ON_CLEANUP_PR', Q.Result.FieldByName('MAKEUP_FICA_ON_CLEANUP_PR').Value);
  end;
  Result := V.Value['MAKEUP_FICA_ON_CLEANUP_PR'] = 'Y';
end;

function TevHistData.CoCombineOASDI(const CoNbr, CoNbr1: Integer; const CheckDate: TDateTime): Boolean;
var
  PMNbr, PMNbr1: Variant;
begin
  if CoNbr = CoNbr1 then
    Result := True
  else
  begin
    PMNbr := CoCommonPaymaster(CoNbr, CheckDate);
    if VarIsNull(PMNbr) then
      Result := False
    else
    begin
      PMNbr1 := CoCommonPaymaster(CoNbr1, CheckDate);
      if VarIsNull(PMNbr) then
        Result := False
      else
        Result := (PMNbr = PMNbr1) and CombineOASDI(CoNbr, CheckDate);
    end;
  end;
end;

function TevHistData.ReciprocateSui(const SySuiNbr, SySuiNbr1: Integer;
  const CheckDate: TDateTime): Boolean;
var
  ReciprocateClient: Boolean;
  SuiType1, SuiType2: String;
//  ErSui1, ErSui2: Boolean;
  StateNbr1, StateNbr2: Integer;
begin
  ReciprocateClient := ClReciprocateSui(CheckDate);
  SuiType1 := SuiType(SySuiNbr, CheckDate);
  SuiType2 := SuiType(SySuiNbr1, CheckDate);
//  ErSui1 := IsErSui(SySuiNbr, CheckDate);
//  ErSui2 := IsErSui(SySuiNbr1, CheckDate);
  StateNbr1 := SySuiStateNbr(SySuiNbr, CheckDate);
  StateNbr2 := SySuiStateNbr(SySuiNbr1, CheckDate); 

  Result := (SySuiNbr <> SySuiNbr1)
    and ReciprocateClient
//    and (SuiType1 = SuiType2)
//    and not (SuiType1[1] in [GROUP_BOX_EE_OTHER, GROUP_BOX_ER_OTHER])
//    and not (SuiType2[1] in [GROUP_BOX_EE_OTHER, GROUP_BOX_ER_OTHER])
//    and (ErSui1 = ErSui2)
    and (StateNbr1 <> StateNbr2)
    and (SyStatesSuiReciprocate(SySuiStateNbr(SySuiNbr, CheckDate), CheckDate) = RECIPROCATE_YES)
        or ((SyStatesSuiReciprocate(SySuiStateNbr(SySuiNbr, CheckDate), CheckDate) = RECIPROCATE_WITH_REC_STATES)
          and (SyStatesSuiReciprocate(SySuiNbr1,CheckDate) = RECIPROCATE_YES));
end;

function TevHistData.GetSuiEmployeeCount(const CoNbr, SySuiNbr: Integer; const EndDate: TDateTime): Integer;
var
  ParamName: String;
  Q: IevQuery;
begin
  ParamName := IntToStr(CoNbr) + ':' + IntToStr(SySuiNbr) + ':' + DateToStr(EndDate);

  if FSuiEmployeeCount.ValueExists(ParamName) then
    Result := FSuiEmployeeCount.Value[ParamName]
  else
  begin
    Q := TevQuery.Create('TotalSUIEmployeesByPeriod');
    Q.Param['MidDate']      := EncodeDate(GetYear(EndDate), GetMonth(EndDate), 12);
    Q.Param['CoNbr']        := CoNbr;
    Q.Macro['SUINbrValues'] := IntToStr(SySuiNbr);
    Result := ConvertNull(Q.Result['EE_COUNT'], 0);

    Q := TevQuery.Create('TotalSUIEmployeesByPeriod');
    Q.Param['MidDate']      := EncodeDate(GetYear(EndDate), GetMonth(EndDate) - 1, 12);
    Q.Param['CoNbr']        := CoNbr;
    Q.Macro['SUINbrValues'] := IntToStr(SySuiNbr);
    Result := Result + ConvertNull(Q.Result['EE_COUNT'], 0);

    Q := TevQuery.Create('TotalSUIEmployeesByPeriod');
    Q.Param['MidDate']      := EncodeDate(GetYear(EndDate), GetMonth(EndDate) - 2, 12);
    Q.Param['CoNbr']        := CoNbr;
    Q.Macro['SUINbrValues'] := IntToStr(SySuiNbr);
    Result := Result + ConvertNull(Q.Result['EE_COUNT'], 0);
    FSuiEmployeeCount.AddValue(ParamName, Result);
  end;
end;

function TevHistData.CalcCompanySuiRate(const CoNbr, SySuiNbr: Integer;
  const CheckDate: TDateTime): Double;
var
  SUIRate2: Double;
begin

  if not VarIsNull(SuiGlobalRate(SySuiNbr, CheckDate)) then
    Result := SuiGlobalRate(SySuiNbr, CheckDate)
  else
    Result := CoSuiRate(GetCoSuiNbr(CoNbr, SySuiNbr, CheckDate), CheckDate);

  if (SySuiNbr = IL_SUI_SY_SUI_NBR) and (TotalSuiTaxableWages(CoNbr, SySuiNbr, GetBeginQuarter(CheckDate), GetEndQuarter(CheckDate)) < 50000) then  // Illinois ER SUI
  begin
    SUIRate2 := 0;
    if not IsCoSySuiInactive(CoNbr, IL_FUND_BUILDING_SY_SUI_NBR, CheckDate) then
    begin
      if not VarIsNull(SuiGlobalRate(IL_FUND_BUILDING_SY_SUI_NBR, CheckDate)) then
        SUIRate2 := SuiGlobalRate(IL_FUND_BUILDING_SY_SUI_NBR, CheckDate)
      else
        SUIRate2 := CoSySuiRate(CoNbr, IL_FUND_BUILDING_SY_SUI_NBR, CheckDate);
    end;
    if Result + SUIRate2 > 0.054 then
      Result := 0.054 - SUIRate2;
    if Result < 0 then
      Result := 0;
  end;
  if SySuiNbr = MA_ER_MED_SY_SUI_NBR then // Massachusetts Healthcare ER SUI
    if GetSuiEmployeeCount(CoNbr, SySuiNbr, GetEndQuarter(CheckDate)) / 3 < 6 then
      Result := 0;
end;

function TevHistData.TotalSuiTaxableWages(const CoNbr, SySuiNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  ParamName: String;
  Q: IevQuery;
begin
  // for Illinois ER SUI only
  ParamName := IntToStr(CoNbr)+':'+IntToStr(SySuiNbr)+':'+DateToStr(DateFrom)+':'+DateToStr(DateTo);
  if FSuiTotalWages.ValueExists(ParamName) then
    Result := FSuiTotalWages.Value[ParamName]
  else
  begin
    Q := TevQuery.Create('TotalSUITaxableWages');
    Q.Param['BeginDate'] := DateFrom;
    Q.Param['EndDate'] := DateTo;
    Q.Param['CoNbr'] := CoNbr;
    Q.Macro['SUINbrValues'] := IntToStr(SySuiNbr);
    Result := ConvertNull(Q.Result['TAXABLE_WAGES'], 0);
    FSuiTotalWages.AddValue(ParamName, Result);
  end;
end;

function TevHistData.GetEeStatesEeSdiExemptExclude(
  const AEeStatesNbr: Integer; const CheckDate: TDateTime): String;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(AEeStatesNbr) + ':' + DateToStr(CheckDate);
  V := FEeStates.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FEeStates.AddParams(ParamName);
  if not V.ValueExists('EE_SDI_EXEMPT_EXCLUDE') then
  begin
    Q := TevQuery.CreateAsOf('select EE_SDI_EXEMPT_EXCLUDE from EE_STATES where EE_STATES_NBR='+IntToStr(AEeStatesNbr)+' and {AsOfDate<EE_STATES>}', CheckDate);
    V.AddValue('EE_SDI_EXEMPT_EXCLUDE', Q.Result.FieldByName('EE_SDI_EXEMPT_EXCLUDE').Value);
  end;
  Result := V.Value['EE_SDI_EXEMPT_EXCLUDE'];
end;


function TevHistData.IsClEdLocalInclude(const ClEdsNbr,
  SyLocalsNbr: Integer; const CheckDate: TDateTime): Boolean;
var
  V: IisListOfValues;
  Q: IevQuery;
  ParamName: String;
begin
  ParamName := IntToStr(ClEdsNbr) + ':' + IntToStr(SyLocalsNbr) + ':' + DateToStr(CheckDate);
  V := FClEdsLocalsExemptions.ParamsByName(ParamName);
  if not Assigned(V) then
    V := FClEdsLocalsExemptions.AddParams(ParamName);
  if not V.ValueExists('') then
  begin
    Q := TevQuery.CreateAsOf('select EXEMPT_EXCLUDE from CL_E_D_LOCAL_EXMPT_EXCLD where CL_E_DS_NBR='+IntToStr(ClEdsNbr)+
    ' and SY_LOCALS_NBR='+IntToStr(SyLocalsNbr) +
    ' and {AsOfDate<CL_E_D_LOCAL_EXMPT_EXCLD>}', CheckDate);
    V.AddValue('EXEMPT_EXCLUDE', Q.Result.FieldByName('EXEMPT_EXCLUDE').AsString);
  end;
  Result := V.Value['EXEMPT_EXCLUDE'] = 'I';
end;

{ TevLine }

constructor TevLine.Create(const AEvo: TevHistData; const ANbr, AClEdNbr: Integer;
  const AAmount: Double; const AEDCode, AState, ASdiState, ASuiState: String;
  const ADivisionNbr, ABranchNbr, ADepartmentNbr, ATeamNbr, AJobNbr: Integer;
  const ACheckDate: TDateTime);
begin
  inherited Create;
  FEvo := AEvo;
  FNbr := ANbr;
  FClEdNbr := AClEdNbr;
  FAmount := AAmount;
  FEDCode := AEDCode;
  FState := AState;
  FSdiState := ASdiState;
  FCheckDate := ACheckDate;
  FSuiState := ASuiState;
  FDivisionNbr := ADivisionNbr;
  FBranchNbr := ABranchNbr;
  FDepartmentNbr := ADepartmentNbr;
  FTeamNbr := ATeamNbr;
  FJobNbr := AJobNbr;
end;

function TevLine.SystemLevelFederalExempt: Boolean;
begin
  Result := FEvo.IsEdCodeFederalExempt(FEDCode, FCheckDate);
end;

function TevLine.GetAmount: Double;
begin
  Result := FAmount;
end;

function TevLine.ClientLevelStateExempt: Boolean;
begin
  Result := FEvo.IsEdCodeClStateExempt(FClEdNbr, FState, FCheckDate)
end;

function TevLine.GetEDCode: String;
begin
  Result := FEDCode;
end;

function TevLine.GetNbr: Integer;
begin
  Result := FNbr;
end;

function TevLine.GetState: String;
begin
  Result := FState;
end;

function TevLine.GetSdiState: String;
begin
  Result := FSdiState;
end;

function TevLine.SystemLevelStateExempt: Boolean;
begin
  if FEvo.ClEmployeeExemptExcludeState(FClEdNbr, FState, FCheckDate) = 'I' then
    Result := False
  else
    Result := FEvo.IsEdCodeStateExempt(FState, FEDCode, FCheckDate)
end;

function TevLine.GetValue: Double;
begin
  if IsDeduction then
    Result := -FAmount
  else
    Result := FAmount;
end;

procedure TevLine.SetAmount(const Value: Double);
begin
  FAmount := Value;
end;

function TevLine.Taxable: Boolean;
begin
  Result := EvUtils.TypeIsTaxable(FEDCode);
end;

function TevLine.ClientLevelFederalExempt: Boolean;
begin
  Result := FEvo.IsClEdFederalExempt(FClEdNbr, FCheckDate)
end;

function TevLine.SystemLevelEeMedicareExempt: Boolean;
begin
  Result := FEvo.IsEdCodeEeMedicareExempt(FEDCode, FCheckDate);
end;

function TevLine.ClientLevelEeMedicareExempt: Boolean;
begin
  Result := FEvo.IsClEdEeMedicareExempt(FClEdNbr, FCheckDate)
end;

function TevLine.ClientLevelEeOasdiExempt: Boolean;
begin
  Result := FEvo.IsClEdEeOasdiExempt(FClEdNbr, FCheckDate);
end;

function TevLine.SystemLevelEeOasdiExempt: Boolean;
begin
  Result := FEvo.IsEdCodeEeOasdiExempt(FEdCode, FCheckDate);
end;

function TevLine.ClientLevelErOasdiExempt: Boolean;
begin
  Result := FEvo.IsClEdErOasdiExempt(FClEdNbr, FCheckDate);
end;

function TevLine.SystemLevelErOasdiExempt: Boolean;
begin
  Result := FEvo.IsEdCodeErOasdiExempt(FEdCode, FCheckDate);
end;

function TevLine.ClientLevelEeSdiExempt: Boolean;
begin
  Result := FEvo.IsEdCodeClEeSdiExempt(FClEdNbr, FState, FCheckDate);
end;

function TevLine.SystemLevelEeSdiExempt: Boolean;
begin
  Result := FEvo.IsEdCodeEeSdiExempt(FEvo.GetSyStateNbr(FState, FCheckDate), FEdCode, FCheckDate);
end;

function TevLine.ClientLevelErMedicareExempt: Boolean;
begin
  Result := FEvo.IsClEdErMedicareExempt(FClEdNbr, FCheckDate)
end;

function TevLine.SystemLevelErMedicareExempt: Boolean;
begin
  Result := FEvo.IsEdCodeErMedicareExempt(FEDCode, FCheckDate);
end;

function TevLine.ClientLevelFuiExempt: Boolean;
begin
  Result := FEvo.IsClEdErFuiExempt(FClEdNbr, FCheckDate)
end;

function TevLine.SystemLevelFuiExempt: Boolean;
begin
  Result := FEvo.IsEdCodeFuiExempt(FEDCode, FCheckDate);
end;

function TevLine.GetSuiState: String;
begin
  Result := FSuiState;
end;

function TevLine.GetClEdNbr: Integer;
begin
  Result := FClEdNbr;
end;

function TevLine.IsDeduction: Boolean;
begin
  Result := copy(FEDCode,1,1) = 'D';
end;

function TevLine.IsEarning: Boolean;
begin
  Result := not IsDeduction;
end;

function TevLine.GetCheckValue: Double;
begin
  if TypeIsTaxableMemo(FEDCode) or (FEDCode = ED_MU_TIPPED_CREDIT_MAKEUP) then
    Result := 0
  else
  if IsDeduction then
    Result := -FAmount
  else
    Result := FAmount;
end;

function TevLine.GetBranchNbr: Integer;
begin
  Result := FBranchNbr;
end;

function TevLine.GetDepartmentNbr: Integer;
begin
  Result := FDepartmentNbr;
end;

function TevLine.GetDivisionNbr: Integer;
begin
  Result := FDivisionNbr;
end;

function TevLine.GetJobNbr: Integer;
begin
  Result := FJobNbr;
end;

function TevLine.GetTeamNbr: Integer;
begin
  Result := FTeamNbr;
end;

function TevLine.GetClone: IInterface;
begin
  Result := TevLine.Create(FEvo, FNbr, FClEdNbr, FAmount, FEDCode, FState, FSdiState, FSuiState, FDivisionNbr, FBranchNbr, FDepartmentNbr, FTeamNbr, FJobNbr, FCheckDate);
end;

{ TevCheck }

function TevCheck.BackUpWithholding: Double;
begin
  Result := FBackUpWithholding;
end;

constructor TevCheck.LoadFromDb(const AEvo: TevHistData; const ACheckNbr: Integer);
var
  Q: IevQuery;
  D: IevDataset;
  L: IevLine;
  ST: IevStateTax;
  SUI: IevSuiTax;
  LT: IevLocalTax;
  s, s1, s2: String;
  i: Integer;
begin
  inherited Create;
  FEvo := AEvo;
  FCheckNbr := ACheckNbr;
  FLines := TisInterfaceList.Create;
  FOLines := TisInterfaceList.Create;
  FEarnings := TisInterfaceList.Create;
  FDeductions := TisInterfaceList.Create;
  FStateTaxes := TisInterfaceList.Create;
  FSuiTaxes := TisInterfaceList.Create;
  FLocalTaxes := TisInterfaceList.Create;

  Q := TevQuery.Create(
  '  select x.PR_NBR, x.EE_NBR, c.CHECK_DATE, c.CO_NBR, k.CL_COMMON_PAYMASTER_NBR, c.PAYROLL_TYPE, x.PAYMENT_SERIAL_NUMBER, x.TAX_FREQUENCY, x.RECIPROCATE_SUI '+
  '  from PR_CHECK x'+
  '  join PR c on c.PR_NBR=x.PR_NBR'+
  '  join CO k on k.CO_NBR=c.CO_NBR'+
  '  where {AsOfNow<x>}'+
  '  and {AsOfNow<c>}'+
  '  and {AsOfNow<k>}'+
  '  and x.PR_CHECK_NBR='+IntToStr(ACheckNbr));
  FCheckDate := Q.Result['CHECK_DATE'];
  FTaxFrequency := Q.Result['TAX_FREQUENCY'];

  FEeNbr := Q.Result['EE_NBR'];
  FCoNbr := Q.Result['CO_NBR'];

  FCompany := TevCompany.Create(AEvo, Q.Result['CO_NBR'], Q.Result['CHECK_DATE']);

  FPrNbr := Q.Result['PR_NBR'];
  FSerialNumber := Q.Result['PAYMENT_SERIAL_NUMBER'];
  if not Q.Result.FieldByName('CL_COMMON_PAYMASTER_NBR').IsNull then
    FCommonPaymasterNbr := Q.Result['CL_COMMON_PAYMASTER_NBR']
  else
    FCommonPaymasterNbr := MaxInt;

  FReciprocateSui := Q.Result['RECIPROCATE_SUI'] = 'Y';

  Q := TevQuery.Create('select x.FEDERAL_TAX, x.EE_MEDICARE_TAX, x.EE_EIC_TAX, x.OR_CHECK_BACK_UP_WITHHOLDING,'+
  ' x.FEDERAL_TAXABLE_WAGES, x.EE_OASDI_TAXABLE_WAGES, x.EE_OASDI_TAX, x.ER_OASDI_TAX, x.ER_OASDI_GROSS_WAGES, x.ER_OASDI_TAXABLE_TIPS, '+
  ' x.EE_MEDICARE_GROSS_WAGES, x.EE_MEDICARE_TAXABLE_WAGES, x.EE_OASDI_TAXABLE_TIPS, x.EE_OASDI_GROSS_WAGES, x.ER_OASDI_TAXABLE_WAGES, x.NET_WAGES, ' +
  ' x.FEDERAL_GROSS_WAGES, x.ER_MEDICARE_TAX, x.ER_MEDICARE_GROSS_WAGES, x.ER_MEDICARE_TAXABLE_WAGES, x.ER_FUI_TAX, x.ER_FUI_GROSS_WAGES, x.ER_FUI_TAXABLE_WAGES '+
  ' from PR_CHECK x where {AsOfNow<x>} and x.PR_CHECK_NBR='+IntToStr(ACheckNbr));
  D := Q.Result;

  FNetWages := D.FieldByName('NET_WAGES').AsFloat;
  FEeOasdiGrossWages := D.FieldByName('EE_OASDI_GROSS_WAGES').AsFloat;
  FErOasdiGrossWages := D.FieldByName('ER_OASDI_GROSS_WAGES').AsFloat;
  FEeOasdiTaxableWages := D.FieldByName('EE_OASDI_TAXABLE_WAGES').AsFloat;
  FErOasdiTaxableWages := D.FieldByName('ER_OASDI_TAXABLE_WAGES').AsFloat;
  FEeOasdiTips := D.FieldByName('EE_OASDI_TAXABLE_TIPS').AsFloat;
  FErOasdiTips := D.FieldByName('ER_OASDI_TAXABLE_TIPS').AsFloat;
  FEeOasdiTax := D.FieldByName('EE_OASDI_TAX').AsFloat;
  FErOasdiTax := D.FieldByName('ER_OASDI_TAX').AsFloat;
  FFederalTaxableWages := D.FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat;
  FFederalGrossWages := D.FieldByName('FEDERAL_GROSS_WAGES').AsFloat;
  FFederalTax := D.FieldByName('FEDERAL_TAX').AsFloat;

  FEeMedicareTax := D.FieldByName('EE_MEDICARE_TAX').AsFloat;
  FEeMedicareGrossWages := D.FieldByName('EE_MEDICARE_GROSS_WAGES').AsFloat;
  FEeMedicareTaxableWages := D.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsFloat;

  FErMedicareTax := D.FieldByName('ER_MEDICARE_TAX').AsFloat;
  FErMedicareGrossWages := D.FieldByName('ER_MEDICARE_GROSS_WAGES').AsFloat;
  FErMedicareTaxableWages := D.FieldByName('ER_MEDICARE_TAXABLE_WAGES').AsFloat;

  FFuiTax := D.FieldByName('ER_FUI_TAX').AsFloat;
  FFuiGrossWages := D.FieldByName('ER_FUI_GROSS_WAGES').AsFloat;
  FFuiTaxableWages := D.FieldByName('ER_FUI_TAXABLE_WAGES').AsFloat;

  FEeEICTax := D.FieldByName('EE_EIC_TAX').AsFloat;
  FBackUpWithholding := D.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsFloat;

  Q := TevQuery.Create('select c.STATE, x.STATE_TAX, x.EE_SDI_TAX, x.STATE_TAXABLE_WAGES, x.EE_SDI_TAXABLE_WAGES, x.EE_SDI_GROSS_WAGES, x.STATE_GROSS_WAGES '+
  '  from PR_CHECK_STATES x'+
  '  join EE_STATES e on {AsOfNow<e>} and e.EE_STATES_NBR=x.EE_STATES_NBR '+
  '  join CO_STATES c on {AsOfNow<c>} and c.CO_STATES_NBR=e.CO_STATES_NBR '+
  '  where {AsOfNow<x>} and x.PR_CHECK_NBR='+IntToStr(ACheckNbr));
  D := Q.Result;

  D.First;
  while not D.Eof do
  begin
    ST := TevStateTax.Create(D['STATE'], D.FieldByName('STATE_TAX').AsFloat,
      D.FieldByName('EE_SDI_TAX').AsFloat, D.FieldByName('STATE_TAXABLE_WAGES').AsFloat,
      D.FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat, D.FieldByName('EE_SDI_GROSS_WAGES').AsFloat,
      D.FieldByName('STATE_GROSS_WAGES').AsFloat);
    FStateTaxes.Add(ST);
    D.Next;
  end;

  Q := TevQuery.Create(
  '  select c.E_D_CODE_TYPE, x.AMOUNT, x.PR_CHECK_LINES_NBR, x.EE_STATES_NBR, x.CL_E_DS_NBR, x.EE_SUI_STATES_NBR, '+
  '  x.CO_DIVISION_NBR, x.CO_BRANCH_NBR, x.CO_DEPARTMENT_NBR, x.CO_TEAM_NBR, x. CO_JOBS_NBR ' +
  '  from PR_CHECK_LINES x'+
  '  join CL_E_DS c on c.CL_E_DS_NBR=x.CL_E_DS_NBR'+
  '  where {AsOfNow<x>}'+
  '  and {AsOfNow<c>}'+
  '  and x.PR_CHECK_NBR='+IntToStr(ACheckNbr) + '  order by x.AMOUNT');
  D := Q.Result;
  D.First;
  while not D.Eof do
  begin
    if not D.FieldByName('EE_STATES_NBR').IsNull then
    begin
      s := AEvo.CoStateState(AEvo.GetEeCoStatesNbr(D['EE_STATES_NBR'], FCheckDate), FCheckDate);
      s1 := AEvo.CoStateState(AEvo.GetEeStateSdiApplyCoStateNbr(D['EE_STATES_NBR'], FCheckDate), FCheckDate);
    end
    else
    begin
      s := AEvo.CoStateState(AEvo.GetEeCoStatesNbr(AEvo.EeHomeEeStateNbr(FEeNbr, FCheckDate), FCheckDate), FCheckDate);
      s1 := AEvo.CoStateState(AEvo.GetEeStateSdiApplyCoStateNbr(AEvo.EeHomeEeStateNbr(FEeNbr, FCheckDate), FCheckDate), FCheckDate);
    end;

    if not D.FieldByName('EE_SUI_STATES_NBR').IsNull then
      s2 := AEvo.CoStateState(AEvo.GetEeStateSuiApplyCoStateNbr(D['EE_SUI_STATES_NBR'], FCheckDate), FCheckDate)
    else
    if not D.FieldByName('EE_STATES_NBR').IsNull then
      s2 := AEvo.CoStateState(AEvo.GetEeStateSuiApplyCoStateNbr(D['EE_STATES_NBR'], FCheckDate), FCheckDate)
    else
      s2 := AEvo.CoStateState(AEvo.GetEeStateSuiApplyCoStateNbr(AEvo.EeHomeEeStateNbr(FEeNbr, FCheckDate), FCheckDate), FCheckDate);

    L := TevLine.Create(AEvo, D['PR_CHECK_LINES_NBR'], D['CL_E_DS_NBR'], D.FieldByName('AMOUNT').AsFloat, D['E_D_CODE_TYPE'], s, s1, s2,
      ConvertNull(D['CO_DIVISION_NBR'], 0),
      ConvertNull(D['CO_BRANCH_NBR'],0),
      ConvertNull(D['CO_DEPARTMENT_NBR'],0),
      ConvertNull(D['CO_TEAM_NBR'],0),
      ConvertNull(D['CO_JOBS_NBR'],0),
      FCheckDate);

    if copy(D.FieldByName('E_D_CODE_TYPE').AsString,1,1) = 'D' then
      FDeductions.Add(L)
    else
      FEarnings.Add(L);

    if not Assigned(StateTax[L.State]) then
    begin
      ST := TevStateTax.Create(L.State, 0, 0, 0, 0, 0, 0);
      FStateTaxes.Add(ST);
    end;

    Q.Result.Next;
  end;

  for i := 0 to FDeductions.Count - 1 do
    FOLines.Add(FDeductions.Items[i]);

  for i := 0 to FEarnings.Count - 1 do
    FOLines.Add(FEarnings.Items[i]);

  for i := 0 to FOLines.Count - 1 do
    FLines.Add((FOLines.Items[i] as IevLine).GetClone);

  Q := TevQuery.Create('select c.SY_SUI_NBR, x.SUI_TAX, x.SUI_TAXABLE_WAGES, x.SUI_GROSS_WAGES from PR_CHECK_SUI x'+
  '  join CO_SUI c on {AsOfNow<c>} and c.CO_SUI_NBR=x.CO_SUI_NBR '+
  '  where {AsOfNow<x>} and x.PR_CHECK_NBR='+IntToStr(ACheckNbr));
  D := Q.Result;

  D.First;
  while not D.Eof do
  begin
    SUI := TevSuiTax.Create(AEvo, D.FieldByName('SY_SUI_NBR').AsInteger, D.FieldByName('SUI_TAX').AsFloat,
      D.FieldByName('SUI_TAXABLE_WAGES').AsFloat, D.FieldByName('SUI_GROSS_WAGES').AsFloat);
    FSuiTaxes.Add(SUI);
    D.Next;
  end;

  Q := TevQuery.Create('select c.SY_LOCALS_NBR, x.LOCAL_TAX, e.CO_LOCAL_TAX_NBR'+
  '  from PR_CHECK_LOCALS x'+
  '  join EE_LOCALS e on {AsOfNow<e>} and e.EE_LOCALS_NBR=x.EE_LOCALS_NBR '+
  '  join CO_LOCAL_TAX c on {AsOfNow<c>} and c.CO_LOCAL_TAX_NBR=e.CO_LOCAL_TAX_NBR '+
  '  where {AsOfNow<x>} and x.PR_CHECK_NBR='+IntToStr(ACheckNbr));
  D := Q.Result;

  D.First;
  while not D.Eof do
  begin
    LT := TevLocalTax.Create(FEvo.CoLocalState(D['CO_LOCAL_TAX_NBR']), D.FieldByName('LOCAL_TAX').AsFloat);
    if FEvo.IsEmployeeLocalTax(D['SY_LOCALS_NBR']) then
      FLocalTaxes.Add(LT);
    D.Next;
  end;

end;

function TevCheck.SuiCount: Integer;
begin
  Result := FSuiTaxes.Count;
end;

function TevCheck.GetSuiTax(Index: Integer): IevSuiTax;
begin
  Result := FSuiTaxes[Index] as IevSuiTax;
end;

function TevCheck.DeductionsCount: Integer;
begin
  Result := FDeductions.Count;
end;

function TevCheck.EarningsCount: Integer;
begin
  Result := FEarnings.Count;
end;

function TevCheck.EeEICTax: Double;
begin
  Result := FEeEICTax;
end;

function TevCheck.EeMedicareTax: Double;
begin
  Result := FEeMedicareTax;
end;

function TevCheck.GetEeOASDITax: Double;
begin
  Result := FEeOasdiTax;
end;

procedure TevCheck.SetEeOasdiTax(const Value: Double);
begin
  FEeOasdiTax := Value;
end;

function TevCheck.FederalTax: Double;
begin
  Result := FFederalTax;
end;

function TevCheck.GetDeduction(Index: Integer): IevLine;
begin
  Result := FDeductions[Index] as IevLine;
end;

function TevCheck.GetEarning(Index: Integer): IevLine;
begin
  Result := FEarnings[Index] as IevLine;
end;

function TevCheck.GetStateTax(Index: String): IevStateTax;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to FStateTaxes.Count - 1 do
    if (FStateTaxes[i] as IevStateTax).State = Index then
      Result := FStateTaxes[i] as IevStateTax;
  if not Assigned(Result) then
    Result := TevStateTax.Create(Index, 0, 0, 0, 0, 0, 0);
end;

function TevCheck.StateTaxCount: Integer;
begin
  Result := FStateTaxes.Count;
end;

function TevCheck.GetLocalTax(Index: Integer): IevLocalTax;
begin
  Result := FLocalTaxes[Index] as IevLocalTax;
end;

function TevCheck.LocalsCount: Integer;
begin
  Result := FLocalTaxes.Count;
end;

function TevCheck.NetAmount: Double;
var
  i: Integer;
begin
  Result := 0;
  
  for i := 0 to EarningsCount - 1 do
    if not TypeIsTaxableMemo(Earnings[i].EDCode)
    and not (Earnings[i].EDCode[1] = 'M') then
      Result := Result + Earnings[i].Value;

  for i := 0 to DeductionsCount - 1 do
    if not TypeIsTaxableMemo(Deductions[i].EDCode)
    and not (Deductions[i].EDCode[1] = 'M') then
      Result := Result - Deductions[i].Amount;

  Result := Result - FederalTax - FEeOASDITax -
    EeMedicareTax - EeEICTax - BackUpWithholding;

  for i := 0 to StateTaxCount - 1 do
    Result := Result - StateTaxes[i].StateTax - StateTaxes[i].EeSdiTax;

  for i := 0 to SuiCount - 1 do
    if SuiTaxes[i].IsEmployeeTax then
      Result := Result - SuiTaxes[i].Tax;
      
  for i := 0 to LocalsCount - 1 do
    Result := Result - LocalTaxes[i].Tax;
end;

function TevCheck.CalcFederalTaxableWages: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsEeFederalExempt(FEeNbr, FCheckDate)
  and not FEvo.IsCoFederalExempt(FCoNbr, FCheckDate) then
  begin
    for i := 0 to LinesCount - 1 do
      if Lines[i].Taxable
      and not Lines[i].SystemLevelFederalExempt
      and not Lines[i].ClientLevelFederalExempt then
        Result := Result + Lines[i].Value;

  end;
end;

function TevCheck.EeNbr: Integer;
begin
  Result := FEeNbr;
end;

function TevCheck.CalcStateTaxableWages(const State: String): Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoStateExempt(FCoNbr, State, FCheckDate) and not FEvo.IsEeStateExempt(FEvo.GetEeStatesNbr(FEeNbr, State, FCheckDate), FCheckDate) then
  begin

    for i := 0 to LinesCount - 1 do
    begin
      if (Lines[i].State = State) and Lines[i].Taxable
      and not Lines[i].SystemLevelStateExempt
      and not Lines[i].ClientLevelStateExempt
      then
        Result := Result + Lines[i].Value;
    end;

  end;
end;

function TevCheck.CalcEeSdiTaxableWages(const State: String; const CalcYtdEeSdiTaxableWages: Double): Double;
var
  i: Integer;
  EeStatesNbr: Integer;
  SdiWageLimit: Double;
begin
  Result := 0;

  if not FEvo.GetPerson(FEeNbr, FCheckDate).EeHasState(FEeNbr, State) then
    Exit;

  if (State = 'NY') or (State = 'HI') then {SDIs for NY and HI are done as a deduction}
    Exit;

  EeStatesNbr := FEvo.GetEeStatesNbr(FEeNbr, State, FCheckDate);

  if (State = 'NM') and (FEvo.EeStateEeSdiExemptExclude(EeStatesNbr, FCheckDate) = 'X') then
    Exit;

  if (FEvo.SyStatesEeSdiRate(FEvo.GetSyStateNbr(State, FCheckDate), FCheckDate) <> 0)
  and not FEvo.IsCoStateEeSdiExempt(FEvo.GetCoStatesNbr(FCoNbr, State, FCheckDate), FCheckDate)
  and not (FEvo.EeStateEeSdiExemptExclude(EeStatesNbr, CheckDate) = 'E') then
  begin
    for i := 0 to LinesCount - 1 do
    begin
      if (Lines[i].SdiState = State) and Lines[i].Taxable
      and not Lines[i].SystemLevelEeSdiExempt
      and not Lines[i].ClientLevelEeSdiExempt
      then
        Result := Result + Lines[i].Value;
    end;

  end;

  SdiWageLimit := FEvo.SyStatesEeSdiMaximumWage(FEvo.GetSyStateNbr(State, FCheckDate), FCheckDate);
  if Result + CalcYtdEeSdiTaxableWages > SdiWageLimit then
  begin
    if SdiWageLimit - CalcYtdEeSdiTaxableWages < Result then
      Result := SdiWageLimit - CalcYtdEeSdiTaxableWages;

    if Result < 0 then
      Result := 0;
  end;

end;

function TevCheck.GetStateTaxableWages(const State: String): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to FStateTaxes.Count - 1 do
    if (FStateTaxes[i] as IevStateTax).State = State then
      Result := Result + (FStateTaxes[i] as IevStateTax).StateTaxableWages;
end;

function TevCheck.GetEeSdiTaxableWages(const State: String): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to FStateTaxes.Count - 1 do
    if (FStateTaxes[i] as IevStateTax).State = State then
      Result := Result + (FStateTaxes[i] as IevStateTax).EeSdiTaxableWages;
end;

function TevCheck.GetStateTaxes(Index: Integer): IevStateTax;
begin
  Result := FStateTaxes[Index] as IevStateTax;
end;

function TevCheck.GetSerialNumber: Integer;
begin
  Result := FSerialNumber;
end;

function TevCheck.EeMedicareGrossWages: Double;
begin
  Result := FEeMedicareGrossWages;
end;

function TevCheck.CalcEeMedicareGrossWages: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFederalExemptEeMedicare(FCoNbr, FCheckDate)
  and not FEvo.IsEeEeMedicareExempt(FEeNbr, FCheckDate) then
  begin

    for i := 0 to LinesCount - 1 do
    begin
      if Lines[i].Taxable
      and not Lines[i].SystemLevelEeMedicareExempt
      and not Lines[i].ClientLevelEeMedicareExempt
      then
        Result := Result + Lines[i].Value;
    end;

  end;
end;

function TevCheck.EeMedicareTaxableWages: Double;
begin
  Result := FEeMedicareTaxableWages;
end;

function TevCheck.CalcEeMedicareTaxableWages(const AYTDWages: Double): Double;
var
  i: Integer;
  MedicareWageLimit: Double;
begin
  Result := 0;

  if not FEvo.IsCoFederalExemptEeMedicare(FCoNbr, FCheckDate)
  and not FEvo.IsEeEeMedicareExempt(FEeNbr, FCheckDate) then
  begin

    for i := 0 to LinesCount - 1 do
    begin
      if Lines[i].Taxable
      and not Lines[i].SystemLevelEeMedicareExempt
      and not Lines[i].ClientLevelEeMedicareExempt
      then
        Result := Result + Lines[i].Value;
    end;

  end;

  if CompareDate(FCheckDate, EncodeDate(2012, 12, 31)) = GreaterThanValue then
  begin
    MedicareWageLimit := FEvo.MedicareWageLimit(FCheckDate);
    if AYTDWages > MedicareWageLimit then
      Result := 0
    else
    if Result + AYTDWages > MedicareWageLimit then
    begin
      Result := MedicareWageLimit - AYTDWages;
      if Result < 0 then
        Result := 0;
    end;
  end;

end;

function TevCheck.CalcEeMedicareTax(const AYTDWage: Double): Double;
var
  WagesBelowThreshold: Double;
  WagesOverThreshold: Double;
  TaxWages: Double;
  MedicareThresholdValue: Double;
begin
  TaxWages := CalcEeMedicareTaxableWages(AYTDWage);
  WagesBelowThreshold := TaxWages;
  WagesOverThreshold := 0;
  if CompareDate(FCheckDate, EncodeDate(2012, 12, 31)) = GreaterThanValue then
  begin
     MedicareThresholdValue := FEvo.MedicareThresholdValue(FCheckDate);
     if AYTDWage > MedicareThresholdValue then
     begin
       WagesBelowThreshold := 0;
       WagesOverThreshold := TaxWages;
     end
     else
     if TaxWages + AYTDWage > MedicareThresholdValue then
     begin
       WagesOverThreshold := TaxWages + AYTDWage - MedicareThresholdValue;
       WagesBelowThreshold := TaxWages - WagesOverThreshold;
       Assert(WagesOverThreshold > -0.005, 'WagesOverThreshold');
       Assert(WagesBelowThreshold > -0.005, 'WagesOverThreshold');
     end;
  end;
  Result := WagesBelowThreshold * FEvo.MedicareRate(FCheckDate) + WagesOverThreshold * FEvo.MedicareThresholdRate(FCheckDate);
end;

function TevCheck.CheckDate: TDateTime;
begin
  Result := FCheckDate;
end;

function TevCheck.GetEeOasdiTips: Double;
begin
  Result := FEeOasdiTips;
end;

function TevCheck.CoNbr: Integer;
begin
  Result := FCoNbr;
end;

function TevCheck.CalcEeOasdiTips: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFederalExemptEeOasdi(FCoNbr, FCheckDate)
  and not FEvo.IsEeEeOasdiExempt(FEeNbr, FCheckDate) then
  begin
    for i := 0 to LinesCount - 1 do
    begin
      if Lines[i].Taxable
      and not Lines[i].SystemLevelEeOasdiExempt
      and not Lines[i].ClientLevelEeOasdiExempt
      and TypeIsTips(Lines[i].EDCode)
      then
        Result := Result + Lines[i].Value;
    end;
  end;
end;

function TevCheck.CalcEeOasdiWages: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFederalExemptEeOasdi(FCoNbr, FCheckDate)
  and not FEvo.IsEeEeOasdiExempt(FEeNbr, FCheckDate) then
  begin
    for i := 0 to LinesCount - 1 do
    begin
      if Lines[i].Taxable
      and not Lines[i].SystemLevelEeOasdiExempt
      and not Lines[i].ClientLevelEeOasdiExempt
      and not TypeIsTips(Lines[i].EDCode)
      then
        Result := Result + Lines[i].Value;
    end;
  end;
end;

function TevCheck.GetEeOasdiGrossWages: Double;
begin
  Result := FEeOasdiGrossWages;
end;

function TevCheck.CalcErOasdiWages: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFederalExemptErOasdi(FCoNbr, FCheckDate)
  and not FEvo.IsEeErOasdiExempt(FEeNbr, FCheckDate) then
  begin
    for i := 0 to LinesCount - 1 do
    begin
      if Lines[i].Taxable
      and not Lines[i].SystemLevelErOasdiExempt
      and not Lines[i].ClientLevelErOasdiExempt
      and not TypeIsTips(Lines[i].EDCode)
      then
        Result := Result + Lines[i].Value;
    end;
  end;
end;

function TevCheck.CalcErOasdiTips: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFederalExemptErOasdi(FCoNbr, FCheckDate)
  and not FEvo.IsEeErOasdiExempt(FEeNbr, FCheckDate) then
  begin
    for i := 0 to LinesCount - 1 do
    begin
      if Lines[i].Taxable
      and not Lines[i].SystemLevelErOasdiExempt
      and not Lines[i].ClientLevelErOasdiExempt
      and TypeIsTips(Lines[i].EDCode)
      then
        Result := Result + Lines[i].Value;
    end;
  end;
end;

function TevCheck.GetErOasdiTax: Double;
begin
  Result := FErOasdiTax;
end;

procedure TevCheck.SetErOasdiTax(const Value: Double);
begin
  FErOasdiTax := Value;
end;

function TevCheck.GetErOasdiGrossWages: Double;
begin
  Result := FErOasdiGrossWages;
end;

procedure TevCheck.SetErOasdiGrossWages(const Value: Double);
begin
  FErOasdiGrossWages := Value;
end;

function TevCheck.GetErOasdiTaxableWages: Double;
begin
  Result := FErOasdiTaxableWages;
end;

procedure TevCheck.SetErOasdiTaxableWages(const Value: Double);
begin
  FErOasdiTaxableWages := Value;
end;

function TevCheck.GetErOasdiTips: Double;
begin
  Result := FErOasdiTips;
end;

procedure TevCheck.SetErOasdiTips(const Value: Double);
begin
  FErOasdiTips := Value;
end;

constructor TevCheck.CreateEmpty(const AEvo: TevHistData; const AEeNbr: Integer;
  const ACheckDate: TDateTime);
var
  Q: IevQuery;
begin
  inherited Create;
  FEvo := AEvo;
  FCheckNbr := 0;

  FLines := TisInterfaceList.Create;
  FOLines := TisInterfaceList.Create;
  FEarnings := TisInterfaceList.Create;
  FDeductions := TisInterfaceList.Create;
  FStateTaxes := TisInterfaceList.Create;
  FSuiTaxes := TisInterfaceList.Create;
  FLocalTaxes := TisInterfaceList.Create;

  Q := TevQuery.Create(
  '  select k.CO_NBR, k.CL_COMMON_PAYMASTER_NBR '+
  '  from EE x'+
  '  join CO k on k.CO_NBR=x.CO_NBR'+
  '  where {AsOfNow<x>}'+
  '  and {AsOfNow<k>}'+
  '  and x.EE_NBR='+IntToStr(AEeNbr));
  FCheckDate := ACheckDate;

  FEeNbr := AEeNbr;
  FCoNbr := Q.Result['CO_NBR'];
  FCompany := TevCompany.Create(AEvo, Q.Result['CO_NBR'], ACheckDate);  
  FSerialNumber := 0;
  if not Q.Result.FieldByName('CL_COMMON_PAYMASTER_NBR').IsNull then
    FCommonPaymasterNbr := Q.Result['CL_COMMON_PAYMASTER_NBR']
  else
    FCommonPaymasterNbr := MaxInt;

  FEeOasdiGrossWages := 0;
  FErOasdiGrossWages := 0;
  FEeOasdiTaxableWages := 0;
  FErOasdiTaxableWages := 0;
  FEeOasdiTips := 0;
  FErOasdiTips := 0;
  FEeOasdiTax := 0;
  FErOasdiTax := 0;
  FFederalTaxableWages := 0;
  FFederalGrossWages := 0;   
  FFederalTax := 0;
  FEeMedicareTax := 0;
  FEeMedicareGrossWages := 0;
  FEeMedicareTaxableWages := 0;
  FEeEICTax := 0;
  FBackUpWithholding := 0;

end;

function TevCheck.GetFederalTaxableWages: Double;
begin
  Result := FFederalTaxableWages;
end;

procedure TevCheck.SetFederalTaxableWages(const Value: Double);
begin
  FFederalTaxableWages := Value;
end;

function TevCheck.GetFederalGrossWages: Double;
begin
  Result := FFederalGrossWages;
end;

procedure TevCheck.SetFederalGrossWages(const Value: Double);
begin
  FFederalGrossWages := Value;
end;

function TevCheck.GetEeOasdiTaxableWages: Double;
begin
  Result := FEeOasdiTaxableWages;
end;

procedure TevCheck.SetEeOasdiTaxableWages(const Value: Double);
begin
  FEeOasdiTaxableWages := Value;
end;

procedure TevCheck.SetEeOasdiTips(const Value: Double);
begin
  FEeOasdiTips := Value;
end;

procedure TevCheck.SetEeOasdiGrossWages(const Value: Double);
begin
  FEeOasdiGrossWages := Value;
end;

function TevCheck.CalcEeSdiTax(const State: String; const CalcYTDTaxWages: Double): Double;
var
  TaxableWages, TaxLimit: Double;
begin
  Result := 0;
  if FEvo.EeStateEeSdiExemptExclude(FEvo.GetEeStatesNbr(FEeNbr, State, FCheckDate), FCheckDate) <> 'X' then
  begin
    TaxableWages := CalcEeSdiTaxableWages(State, CalcYTDTaxWages);
    if (TaxableWages <> 0) then
    begin
      if (State = 'NM') then
      begin
        if GetQuarterNumber(FCheckDate) <> GetQuarterNumber(ctx_PayrollCalculation.GetNextCheckDate(FCheckDate, FEvo.CoPayFrequencies(FCoNbr, FCheckDate))) then
          Result := FEvo.SyStatesEeSdiRate(FEvo.GetSyStateNbr(State, FCheckDate), FCheckDate);
      end
      else
      begin
        Result := TaxableWages * FEvo.SyStatesEeSdiRate(FEvo.GetSyStateNbr(State, FCheckDate), FCheckDate);
        TaxLimit := FEvo.SyStatesWeeklySdiTaxCap(FEvo.GetSyStateNbr(State, FCheckDate), FCheckDate) * 52 / GetTaxFreq(FTaxFrequency);
        if Result > TaxLimit then
          Result := TaxLimit;
      end;
    end;
  end;  
end;

function TevCheck.TypeIsTips(const ACode: String): Boolean;
begin
  if CompareDate(FCheckDate, EncodeDate(2013, 12, 31)) = GreaterThanValue then
    Result := TypeIsTipsAfter2013(ACode)
  else
    Result := TypeIsTipsBefore2014(ACode);
end;

function TevCheck.CalcErMedicareGrossWages: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFederalExemptErMedicare(FCoNbr, FCheckDate)
  and not FEvo.IsEeErMedicareExempt(FEeNbr, FCheckDate) then
  begin

    for i := 0 to EarningsCount - 1 do
    begin
      if Earnings[i].Taxable
      and not Earnings[i].SystemLevelErMedicareExempt
      and not Earnings[i].ClientLevelErMedicareExempt
      then
        Result := Result + Earnings[i].Amount;
    end;

    for i := 0 to DeductionsCount - 1 do
    begin
      if Deductions[i].Taxable
      and not Deductions[i].SystemLevelErMedicareExempt
      and not Deductions[i].ClientLevelErMedicareExempt
      then
        Result := Result - Deductions[i].Amount;
    end;

  end;
end;

function TevCheck.CalcErMedicareTax(const AYTDWage: Double): Double;
var
  TaxWages: Double;
begin
  TaxWages := CalcErMedicareTaxableWages(AYTDWage);
  Result := TaxWages * FEvo.MedicareRate(FCheckDate);
end;

function TevCheck.CalcErMedicareTaxableWages(
  const AYTDWages: Double): Double;
var
  i: Integer;
  MedicareWageLimit: Double;
begin
  Result := 0;

  if not FEvo.IsCoFederalExemptErMedicare(FCoNbr, FCheckDate)
  and not FEvo.IsEeErMedicareExempt(FEeNbr, FCheckDate) then
  begin

    for i := 0 to EarningsCount - 1 do
    begin
      if Earnings[i].Taxable
      and not Earnings[i].SystemLevelErMedicareExempt
      and not Earnings[i].ClientLevelErMedicareExempt
      then
        Result := Result + Earnings[i].Amount;
    end;

    for i := 0 to DeductionsCount - 1 do
    begin
      if Deductions[i].Taxable
      and not Deductions[i].SystemLevelErMedicareExempt
      and not Deductions[i].ClientLevelErMedicareExempt
      then
        Result := Result - Deductions[i].Amount;
    end;

  end;

  MedicareWageLimit := FEvo.MedicareWageLimit(FCheckDate);
  if AYTDWages > MedicareWageLimit then
    Result := 0
  else
  if Result + AYTDWages > MedicareWageLimit then
  begin
    Result := MedicareWageLimit - AYTDWages;
    if Result < 0 then
      Result := 0;
  end;

end;

function TevCheck.ErMedicareGrossWages: Double;
begin
  Result := FErMedicareGrossWages;
end;

function TevCheck.ErMedicareTax: Double;
begin
  Result := FErMedicareTax;
end;

function TevCheck.ErMedicareTaxableWages: Double;
begin
  Result := FErMedicareTaxableWages;
end;

function TevCheck.CalcFuiGrossWages: Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoFuiExempt(FCoNbr, FCheckDate)
  and not FEvo.IsEeErFuiExempt(FEeNbr, FCheckDate) then
  begin

    for i := 0 to EarningsCount - 1 do
    begin
      if Earnings[i].Taxable
      and not Earnings[i].SystemLevelFuiExempt
      and not Earnings[i].ClientLevelFuiExempt
      then
        Result := Result + Earnings[i].Amount;
    end;

    for i := 0 to DeductionsCount - 1 do
    begin
      if Deductions[i].Taxable
      and not Deductions[i].SystemLevelFuiExempt
      and not Deductions[i].ClientLevelFuiExempt
      then
        Result := Result - Deductions[i].Amount;
    end;

  end;
end;

function TevCheck.CalcFuiTax(const AYTDWage: Double): Double;
var
  TaxWages: Double;
  FuiRate: Double;
begin
  TaxWages := CalcFuiTaxableWages(AYTDWage);

  if AYTDWage + TaxWages < TaxWages then
  begin
    TaxWages := AYTDWage + TaxWages;
    if TaxWages < 0 then
      TaxWages := 0;
  end;

  if FEvo.IsEeFuiRateCreditOverride(FEeNbr, FCheckDate) then
    FUIRate := FEvo.FuiRateReal(FCheckDate) - FEvo.EeFuiRateCreditOverride(FEeNbr, FCheckDate)
  else
  if FEvo.IsCoFuiRateOverride(FCoNbr, FCheckDate) then
    FUIRate := FEvo.FuiRateReal(FCheckDate) - FEvo.CoFuiRateOverride(FCoNbr, FCheckDate)
  else
    FUIRate := FEvo.FuiRateReal(FCheckDate) - FEvo.FuiRateCredit(FCheckDate);

  Result := TaxWages * FUIRate;
end;

function TevCheck.CalcFuiTaxableWages(const AYTDWages: Double): Double;
var
  i: Integer;
  FuiWageLimit: Double;
begin
  Result := 0;

  if not FEvo.IsCoFuiExempt(FCoNbr, FCheckDate)
  and not FEvo.IsEeErFuiExempt(FEeNbr, FCheckDate) then
  begin

    for i := 0 to EarningsCount - 1 do
    begin
      if Earnings[i].Taxable
      and not Earnings[i].SystemLevelFuiExempt
      and not Earnings[i].ClientLevelFuiExempt
      then
        Result := Result + Earnings[i].Amount;
    end;

    for i := 0 to DeductionsCount - 1 do
    begin
      if Deductions[i].Taxable
      and not Deductions[i].SystemLevelFuiExempt
      and not Deductions[i].ClientLevelFuiExempt
      then
        Result := Result - Deductions[i].Amount;
    end;

  end;

  FuiWageLimit := FEvo.FuiWageLimit(FCheckDate);
  if AYTDWages > FuiWageLimit then
    Result := 0
  else
  if Result + AYTDWages > FuiWageLimit then
  begin
    Result := FuiWageLimit - AYTDWages;
    if Result < 0 then
      Result := 0;
  end;

end;

function TevCheck.FuiGrossWages: Double;
begin
  Result := FFuiGrossWages;
end;

function TevCheck.FuiTax: Double;
begin
  Result := FFuiTax;
end;

function TevCheck.FuiTaxableWages: Double;
begin
  Result := FFuiTaxableWages;
end;

function TevCheck.GetReciprocateSui: Boolean;
begin
  Result := FReciprocateSui;
end;

function TevCheck.GetLine(Index: Integer): IevLine;
begin
  Result := FLines[Index] as IevLine;
end;

function TevCheck.LinesCount: Integer;
begin
  Result := FLines.Count;
end;

procedure TevCheck.AddSui(const ASySuiNbr: Integer; GrossWages, TaxWages,
  Tax: Double);
begin
  FSuiTaxes.Add(TevSuiTax.Create(FEvo, ASySuiNbr, Tax, TaxWages, GrossWages));
end;

function TevCheck.NetWages: Double;
begin
  Result := FNetWages;
end;

function TevCheck.PrNbr: Integer;
begin
  Result := FPrNbr;
end;

function TevCheck.CheckNbr: Integer;
begin
  Result := FCheckNbr;
end;

function TevCheck.CalcEeSdiGrossWages(const State: String): Double;
var
  i: Integer;
  EeStatesNbr: Integer;
begin
  Result := 0;

  if FEvo.GetEeStatesEeSdiExemptExclude(FEvo.GetEeStatesNbr(FEeNbr, State, FCheckDate), FCheckDate) = 'E' then
    Exit;

  if not FEvo.GetPerson(FEeNbr, FCheckDate).EeHasState(FEeNbr, State) then
    Exit;

  if (State = 'NY') or (State = 'HI') then {SDIs for NY and HI are done as a deduction}
    Exit;

  EeStatesNbr := FEvo.GetEeStatesNbr(FEeNbr, State, FCheckDate);

  if (State = 'NM') and (FEvo.EeStateEeSdiExemptExclude(EeStatesNbr, FCheckDate) = 'X') then
    Exit;

  if (FEvo.SyStatesEeSdiRate(FEvo.GetSyStateNbr(State, FCheckDate), FCheckDate) <> 0)
  and not FEvo.IsCoStateEeSdiExempt(FEvo.GetCoStatesNbr(FCoNbr, State, FCheckDate), FCheckDate) then
  begin
    for i := 0 to EarningsCount - 1 do
    begin
      if (Earnings[i].SdiState = State) and Earnings[i].Taxable
      and not Earnings[i].SystemLevelEeSdiExempt
      and not Earnings[i].ClientLevelEeSdiExempt
      then
        Result := Result + Earnings[i].Amount;
    end;

    for i := 0 to DeductionsCount - 1 do
    begin
      if (Deductions[i].SdiState = State) and Deductions[i].Taxable
      and not Deductions[i].SystemLevelEeSdiExempt
      and not Deductions[i].ClientLevelEeSdiExempt
      then
        Result := Result - Deductions[i].Amount;
    end;

  end;

end;

function TevCheck.GetEeSdiGrossWages(const State: String): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to FStateTaxes.Count - 1 do
    if (FStateTaxes[i] as IevStateTax).State = State then
      Result := Result + (FStateTaxes[i] as IevStateTax).EeSdiGrossWages;
end;

function TevCheck.CalcStateGrossWages(const State: String): Double;
var
  i: Integer;
begin
  Result := 0;
  if not FEvo.IsCoStateExempt(FCoNbr, State, FCheckDate) and not FEvo.IsEeStateExempt(FEvo.GetEeStatesNbr(FEeNbr, State, FCheckDate), FCheckDate) then
  for i := 0 to LinesCount - 1 do
  begin
    if (Lines[i].State = State) and Lines[i].Taxable
      and not Lines[i].SystemLevelStateExempt
      and not Lines[i].ClientLevelStateExempt
    then
      Result := Result + Lines[i].Value;
  end;
end;

function TevCheck.GetStateGrossWages(const State: String): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to FStateTaxes.Count - 1 do
    if (FStateTaxes[i] as IevStateTax).State = State then
      Result := Result + (FStateTaxes[i] as IevStateTax).StateGrossWages;
end;

procedure TevCheck.RestoreOriginalCheckLines;
var
  L: IevLine;
  i: Integer;
begin
  FLines.Clear;
  FDeductions.Clear;
  FEarnings.Clear;
  for i := 0 to FOLines.Count - 1 do
  begin
    L := FOLines.Items[i] as IevLine;
    L := L.GetClone as IevLine;
    FLines.Add(L);
    if copy(L.EDCode,1,1) = 'D' then
      FDeductions.Add(L)
    else
      FEarnings.Add(L);
  end;
end;

function TevCheck.GetCompany: IevCompany;
begin
  Result := FCompany;
end;

{ TevStateTax }

constructor TevStateTax.Create(const AState: String; const AStateTax,
  AEeSdiTax, AStateTaxableWages, AEeSdiTaxableWages, AEeSdiGrossWages,
  AStateGrossWages: Double);
begin
  inherited Create;
  FState := AState;
  FStateTax := AStateTax;
  FEeSdiTax := AEeSdiTax;
  FStateTaxableWages := AStateTaxableWages;
  FEeSdiTaxableWages := AEeSdiTaxableWages;
  FEeSdiGrossWages := AEeSdiGrossWages;
  FStateGrossWages := AStateGrossWages;
end;

function TevStateTax.GetEeSdiTax: Double;
begin
  Result := FEeSdiTax;
end;

function TevStateTax.GetState: String;
begin
  Result := FState;
end;

function TevStateTax.GetStateTax: Double;
begin
  Result := FStateTax;
end;

function TevStateTax.GetEeSdiTaxableWages: Double;
begin
  Result := FEeSdiTaxableWages;
end;

function TevStateTax.GetStateTaxableWages: Double;
begin
  Result := FStateTaxableWages;
end;

function TevStateTax.GetYtdEeSdiTaxableWages: Double;
begin
  Result := FYtdEeSdiTaxableWages;
end;

procedure TevStateTax.SetYtdEeSdiTaxableWages(const Value: Double);
begin
  FYtdEeSdiTaxableWages := Value;
end;

function TevStateTax.GetEeSdiGrossWages: Double;
begin
  Result := FEeSdiGrossWages;
end;

function TevStateTax.GetStateGrossWages: Double;
begin
  Result := FStateGrossWages;
end;

{ TevSuiTax }

function TevSuiTax.GetTax: Double;
begin
  Result := FTax;
end;

constructor TevSuiTax.Create(const AEvo: TevHistData; const ASySuiNbr: Integer; const ATax, ATaxWages, AGrossWages: Double);
begin
  inherited Create;
  FEvo := AEvo;
  FTax := ATax;
  FTaxWages := ATaxWages;
  FGrossWages := AGrossWages;
  FSySuiNbr := ASySuiNbr;
end;

function TevSuiTax.GetName: String;
begin
  Result := FEvo.SuiTaxName(FSySuiNbr, Now);
end;

function TevSuiTax.GetSySuiNbr: Integer;
begin
  Result := FSySuiNbr;
end;

function TevSuiTax.IsEmployeeTax: Boolean;
begin
  Result := not FEvo.IsErSui(FSySuiNbr, Now);
end;

function TevSuiTax.IsEmployerTax: Boolean;
begin
  Result := FEvo.IsErSui(FSySuiNbr, Now);
end;

function TevSuiTax.GetTaxWages: Double;
begin
  Result := FTaxWages;
end;

function TevSuiTax.GetGrossWages: Double;
begin
  Result := FGrossWages;
end;

{ TevLocalTax }

constructor TevLocalTax.Create(const AState: String; const ATax: Double);
begin
  inherited Create;
  FState := AState;
  FTax := ATax;
end;

function TevLocalTax.GetState: String;
begin
  Result := FState;
end;

function TevLocalTax.GetTax: Double;
begin
  Result := FTax;
end;

{ TevPerson }

constructor TevPerson.LoadFromDB(const AEvo: TevHistData; const AEeNbr: Integer;
  const CheckDate: TDateTime);
var
  Q: IevQuery;
  Check: IevCheck;
  i: Integer;
  V: IisListOfValues;
  StatesSdi: IisListOfValues;
  Name: String;
begin
  inherited Create;
  FCheckDate := CheckDate;
  FEeNumbers := TisParamsCollection.Create;
  FStateTaxes := TisParamsCollection.Create;
  FEeNbr := AEeNbr;
  FEvo := AEvo;
  FChecks := TisListOfValues.Create;
  Q := TevQuery.Create('select CL_PERSON_NBR from EE where EE_NBR='+IntToStr(AEeNbr)+' and {AsOfNow<EE>}');
  FClPersonNbr := Q.Result['CL_PERSON_NBR'];

  Q := TevQuery.Create('select EE_NBR, CO_NBR, CUSTOM_EMPLOYEE_NUMBER from EE where CL_PERSON_NBR='+IntToStr(FClPersonNbr)+' and {AsOfNow<EE>}');
  Q.Result.First;
  while not Q.Result.Eof do
  begin
    V := FEeNumbers.AddParams(Q.Result.FieldByName('EE_NBR').AsString);
    V.AddValue('CO_NBR', Q.Result.FieldByName('CO_NBR').AsInteger);
    V.AddValue('CUSTOM_EMPLOYEE_NUMBER', Q.Result.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
    Q.Result.Next;
  end;

  Q := TevQuery.Create('select c.PR_CHECK_NBR '+
  '  from PR_CHECK c '+
  '  join EE e on e.EE_NBR=c.EE_NBR '+
  '  join PR p on p.PR_NBR=c.PR_NBR '+
  '  where e.CL_PERSON_NBR='+IntToStr(FClPersonNbr)+' and {AsOfNow<c>} and {AsOfNow<e>} and {AsOfNow<p>}'+
  '  and p.STATUS = ''P'' and p.CHECK_DATE between :BEGIN_DATE and :END_DATE and c.CHECK_STATUS<>''V'' '+
  '  and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') '+
  '  order by p.CHECK_DATE, p.RUN_NUMBER, c.PR_CHECK_NBR');
  Q.Param['BEGIN_DATE'] := GetBeginYear(CheckDate);
  Q.Param['END_DATE'] := CheckDate;

  StatesSdi := TisListOfValues.Create;

  Q.Result.First;
  while not Q.Result.Eof do
  begin
    Check := TevCheck.LoadFromDb(AEvo, Q.Result['PR_CHECK_NBR']);
    for i := 0 to Check.StateTaxCount - 1 do
    begin
      V := FStateTaxes.ParamsByName(IntToStr(Check.EeNbr));
      if not Assigned(V) then
        V := FStateTaxes.AddParams(IntToStr(Check.EeNbr));
      if not V.ValueExists(Check.StateTaxes[i].State) then
        V.AddValue(Check.StateTaxes[i].State, 0);
      if not StatesSdi.ValueExists(Check.StateTaxes[i].State) then
        StatesSdi.AddValue(Check.StateTaxes[i].State, 0);
    end;

    for i := 0 to Check.StateTaxCount - 1 do
    begin
      Check.StateTaxes[i].YtdEeSdiTaxableWages := StatesSdi.Value[Check.StateTaxes[i].State];
      StatesSdi.Value[Check.StateTaxes[i].State] := StatesSdi.Value[Check.StateTaxes[i].State] + Check.StateTaxes[i].EeSdiTaxableWages;
    end;

    Name := IntToStr(YearOf(Check.CheckDate));
    if MonthOf(Check.CheckDate) < 10 then
      Name := Name + '0';
    Name := Name + IntToStr(MonthOf(Check.CheckDate));
    if DayOf(Check.CheckDate) < 10 then
      Name := Name + '0';
    Name := Name + IntToStr(DayOf(Check.CheckDate)) + Q.Result.FieldByName('PR_CHECK_NBR').AsString;

    FChecks.AddValue(Name, Check);
    Q.Result.Next;
  end;
  FChecks.Sort;
end;

procedure TevPerson.RedistributeNegativeCheckLines(const CheckDate: TDateTime);
var
  i, j, k, l: Integer;
  Check, CCheck: IevCheck;
  Line, CLine: IevLine;
  Amount: Double;
begin
  // redistribute negative check lines to adjust the original check lines
  for i := FChecks.Count - 1 downto 0 do
  begin
    Check := GetCheck(i);
    if DateBetween(Check.CheckDate, GetBeginYear(CheckDate), GetEndQuarter(CheckDate)) then
    for j := 0 to Check.LinesCount - 1 do
    begin
      Line := Check.Lines[j];
      if Line.Amount < 0 then
      begin
        Amount := Line.Amount;
        Line.Amount := 0;
        for k := i downto 0 do
        begin
          CCheck := GetCheck(k);
          if (CCheck.EeNbr = Check.EeNbr) and DateBetween(CCheck.CheckDate, GetBeginYear(CheckDate), GetEndQuarter(CheckDate)) then
          begin
            for l := 0 to CCheck.LinesCount - 1 do
            begin
              CLine := CCheck.Lines[l];
              if (CLine.ClEdNbr = Line.ClEdNbr)
              and (CLine.DivisionNbr = Line.DivisionNbr)
              and (CLine.BranchNbr = Line.BranchNbr)
              and (CLine.DepartmentNbr = Line.DepartmentNbr)
              and (CLine.TeamNbr = Line.TeamNbr)
              and (CLine.JobNbr = Line.JobNbr)
              and (CLine.State = Line.State)
              and (CLine.SuiState = Line.SuiState)
              and (CLine.SdiState = Line.SdiState) then
              begin
                if CLine.Amount > 0 then
                begin
                  CLine.Amount := CLine.Amount + Amount;
                  if CLine.Amount < 0 then
                  begin
                    Amount := CLine.Amount;
                    CLine.Amount := 0;
                  end
                  else
                  begin
                    Amount := 0;
                    break;
                  end;
                end;
              end;
            end;
          end;
          if Amount >= 0 then
            break;
        end;

        Line.Amount := Amount;
      end;
    end
  end;
end;

function TevPerson.GetCheck(Index: Integer): IevCheck;
begin
  Result := IInterface(FChecks.Values[Index].Value) as IevCheck;
end;

function TevPerson.CheckCount: Integer;
begin
  Result := FChecks.Count;
end;

function TevPerson.CalcFederalTaxableWages(const AEeNbr: Integer): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if Checks[i].EeNbr = AEeNbr then
      Result := Result + Checks[i].CalcFederalTaxableWages;
end;

function TevPerson.FederalTaxableWages(const AEeNbr: Integer): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if Checks[i].EeNbr = AEeNbr then
      Result := Result + Checks[i].FederalTaxableWages;
end;

function TevPerson.CalcEeSdiTaxableWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
  TaxableWages, CalcYTDTaxWages: Double;
begin
  Result := 0;
  CalcYTDTaxWages := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) or Checks[i].Company.CoCombineStateSui(CoNbr(AEENbr)) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
      begin
        TaxableWages := Checks[i].CalcEeSdiTaxableWages(State, CalcYTDTaxWages);
        CalcYTDTaxWages := CalcYTDTaxWages + TaxableWages;
        if DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
          Result := Result + TaxableWages;
      end;
    end;
  end;
end;

function TevPerson.CalcStateTaxableWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
        Result := Result + Checks[i].CalcStateTaxableWages(State);
    end;
  end;
end;

function TevPerson.GetState(EeNbr, Index: Integer): String;
var
  V: IisListOfValues;
begin
  V := FStateTaxes.ParamsByName(IntToStr(EeNbr));
  Result := V.Values[Index].Name;
end;

function TevPerson.StatesCount(const AEeNbr: Integer): Integer;
var
  V: IisListOfValues;
begin
  V := FStateTaxes.ParamsByName(IntToStr(AEeNbr));
  if Assigned(V) then
    Result := V.Count
  else
    Result := 0;
end;

function TevPerson.EeSdiTaxableWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if ((Checks[i].EeNbr = AEeNbr)  or Checks[i].Company.CoCombineStateSui(CoNbr(AEENbr))) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
        Result := Result + Checks[i].GetEeSdiTaxableWages(State);
    end;
  end;
end;

function TevPerson.StateTaxableWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
        Result := Result + Checks[i].GetStateTaxableWages(State);
    end;
  end;
end;

function TevPerson.EeMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].EeMedicareGrossWages;
end;

function TevPerson.CalcEeMedicareGrossWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].CalcEeMedicareGrossWages;
end;

function TevPerson.EeMedicareTaxableWages(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].EeMedicareTaxableWages;
end;

function TevPerson.CalcEeMedicareTaxableWages(
  const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  YTDWages: Double;
begin
  Result := 0;
  YTDWages := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineMedicare(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
        Result := Result + Checks[i].CalcEeMedicareTaxableWages(YTDWages);
      YTDWages := YTDWages + Checks[i].CalcEeMedicareTaxableWages(YTDWages);
    end;  
  end;
end;

function TevPerson.CalcEeMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  YTDWage: Double;
  CalcTax: Double;
begin
  Result := 0;
  YTDWage := 0;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineMedicare(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      CalcTax := Checks[i].CalcEeMedicareTax(YTDWage);
      YTDWage := YTDWage + Checks[i].CalcEeMedicareTaxableWages(YTDWage);
      if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      begin
        Result := Result + CalcTax;
      end;
    end;  
  end;

end;

function TevPerson.EeMedicareTax(const AEeNbr: Integer; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      Result := Result + Checks[i].EeMedicareTax;
    end;
  end;
end;

function TevPerson.EeOasdiTax(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].EeOasdiTax;
end;

function TevPerson.EeOasdiTaxTips(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].EeOasdiTips;
end;

function TevPerson.EeOasdiTaxWages(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].EeOasdiTaxableWages;
end;

function TevPerson.ClPersonNbr: Integer;
begin
  Result := FClPersonNbr;
end;

function TevPerson.EeCount: Integer;
begin
  Result := FEeNumbers.Count;
end;

function TevPerson.GetEeNbr(Index: Integer): Integer;
begin
  Result := StrToInt(FEeNumbers.ParamName(Index));
end;

function TevPerson.CalcEeOasdiTaxWages(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) then
      Result := Result + Checks[i].CalcEeOasdiWages;
end;

function TevPerson.CalcEeOasdiTaxTips(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) then
      Result := Result + Checks[i].CalcEeOasdiTips;
end;

function TevPerson.CoNbr(EeNbr: Integer): Integer;
begin
  Result := FEeNumbers.ParamsByName(IntToStr(EeNbr)).Value['CO_NBR'];
end;

function TevPerson.GetQecCheck(EeNbr: Integer): IevCheck;
begin
  if FEeNumbers.ParamsByName(IntToStr(EeNbr)).ValueExists('QEC_CHECK') then
    Result := IInterface(FEeNumbers.ParamsByName(IntToStr(EeNbr)).Value['QEC_CHECK']) as IevCheck
  else
  begin
    Result := TevCheck.CreateEmpty(FEvo, EeNbr, FCheckDate);
    FEeNumbers.ParamsByName(IntToStr(EeNbr)).AddValue('QEC_CHECK', Result);
    FChecks.AddValue('QEC_CHECK', Result);
  end;
end;

function TevPerson.EeSdiTax(const AEeNbr: Integer; const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if ((Checks[i].EeNbr = AEeNbr) or Checks[i].Company.CoCombineStateSui(CoNbr(AEENbr)))and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      Result := Result + Checks[i].StateTax[State].EeSdiTax;
    end;
  end;
end;

function TevPerson.CalcEeSdiTax(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  CalcYTDTaxableWage: Double;
begin
  Result := 0;
  CalcYTDTaxableWage := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) or Checks[i].Company.CoCombineStateSui(CoNbr(AEENbr)) then
    begin
      if EeHasState(Checks[i].EeNbr, State) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
        Result := Result + Checks[i].CalcEeSdiTax(State, CalcYTDTaxableWage);
      if EeHasState(Checks[i].EeNbr, State) then
        CalcYTDTaxableWage := CalcYTDTaxableWage + Checks[i].CalcEeSdiTaxableWages(State, CalcYTDTaxableWage);
    end;
  end;
end;

function TevPerson.EeHasState(const EeNbr: Integer;
  const State: String): Boolean;
var
  V: IisListOfValues;
begin
  V := FStateTaxes.ParamsByName(IntToStr(EeNbr));
  if Assigned(V) then
    Result := V.ValueExists(State)
  else
    Result := False;
end;

function TevPerson.CalcErMedicareGrossWages(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].CalcErMedicareGrossWages;
end;

function TevPerson.CalcErMedicareTax(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
  YTDWage: Double;
  CalcTax: Double;
begin
  Result := 0;
  YTDWage := 0;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineMedicare(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      CalcTax := Checks[i].CalcErMedicareTax(YTDWage);
      YTDWage := YTDWage + Checks[i].CalcErMedicareTaxableWages(YTDWage);
      if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      begin
        Result := Result + CalcTax;
      end;
    end;  
  end;

end;

function TevPerson.CalcErMedicareTaxableWages(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  YTDWages: Double;
begin
  Result := 0;
  YTDWages := 0;
  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineMedicare(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
        Result := Result + Checks[i].CalcErMedicareTaxableWages(YTDWages);
      YTDWages := YTDWages + Checks[i].CalcErMedicareTaxableWages(YTDWages);
    end;  
  end;
end;

function TevPerson.ErMedicareGrossWages(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].ErMedicareGrossWages;
end;

function TevPerson.ErMedicareTax(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      Result := Result + Checks[i].ErMedicareTax;
    end;
  end;
end;

function TevPerson.ErMedicareTaxableWages(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].ErMedicareTaxableWages;
end;

function TevPerson.CalcFuiGrossWages(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].CalcFuiGrossWages;
end;

function TevPerson.CalcFuiTax(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
  YTDWage, CheckWages: Double;
  CalcTax: Double;
begin
  Result := 0;
  YTDWage := 0;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineFui(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      CheckWages := Checks[i].CalcFuiTaxableWages(YTDWage);
      if CheckWages < 0 then
        YTDWage := YTDWage + CheckWages;
    end;
  end;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineFui(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      CheckWages := Checks[i].CalcFuiTaxableWages(YTDWage);
      if CheckWages > 0 then
      begin
        CalcTax := Checks[i].CalcFuiTax(YTDWage);
        YTDWage := YTDWage + CheckWages;
        if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
        begin
          Result := Result + CalcTax;
        end;
      end;  
    end;
  end;

end;

function TevPerson.CalcFuiTaxableWages(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  YTDWages, CheckWages: Double;
begin
  Result := 0;
  YTDWages := 0;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineFui(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      CheckWages := Checks[i].CalcFuiTaxableWages(YTDWages);
      if CheckWages < 0 then
      begin
        if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
          Result := Result + CheckWages;
        YTDWages := YTDWages + CheckWages;
      end;
    end;
  end;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or FEvo.CoCombineFui(CoNbr(AEENbr), CoNbr(Checks[i].EeNbr), DateTo) then
    begin
      CheckWages := Checks[i].CalcFuiTaxableWages(YTDWages);
      if CheckWages > 0 then
      begin
        if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
          Result := Result + CheckWages;
        YTDWages := YTDWages + CheckWages;
      end;
    end;
  end;
end;

function TevPerson.FuiGrossWages(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].FuiGrossWages;
end;

function TevPerson.FuiTax(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      Result := Result + Checks[i].FuiTax;
    end;
  end;
end;

function TevPerson.FuiTaxableWages(const AEeNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
      Result := Result + Checks[i].FuiTaxableWages;
end;

function TevPerson.HasEeNbr(const AEeNbr: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to FEeNumbers.Count - 1 do
  begin
    if StrToInt(FEeNumbers.ParamName(i)) = AEeNbr then
    begin
      Result := True;
      break;
    end;
  end;
end;

function TevPerson.CalcSuiGrossWages(const AEeNbr, ASySuiNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i, j, k: Integer;
  Check: IevCheck;
  SuiState: String;
  Amount, CheckAmount, CheckWages: Double;
  EEStateNumber, CoSuiNbr: Integer;
  Line: IevLine;
begin
  Result := 0;

  SuiState := FEvo.CoStateState(FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(CoNbr(AEeNbr), ASySuiNbr, DateTo), DateTo), DateTo);

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    Check := Checks[i];
    CheckAmount := 0;
    CheckWages := 0;
    if Check.EeNbr = AEeNbr then
    begin
      if FEvo.SuiBasedOnHours(ASySuiNbr, Check.CheckDate) then
      begin
        if DateBetween(Check.CheckDate, DateFrom, DateTo) and (Check.LinesCount > 0) then
        for j := 0 to Check.SuiCount - 1 do
        begin
          if (Check.SuiTaxes[j].SySuiNbr = ASySuiNbr) then
          begin
            CheckAmount := CheckAmount + Check.SuiTaxes[j].GrossWages;
          end;
        end;
      end
      else
      begin
        for k := 0 to Check.LinesCount - 1 do
        begin
          Line := Check.Lines[k];

          if (SuiState = Line.SuiState) then
          begin
            Amount := 0;
            if Line.Taxable
            or ((ASySuiNbr = NV_BUSINESS_SY_SUI_NBR) and ((Line.EDCode = 'M1') or (Line.EDCode = 'M4')) and (FEvo.ClEdDescription(Line.ClEdNbr, Check.CheckDate) = 'ERNEV')) then
            begin
              if (Line.EDCode = 'M1') or (Line.EDCode = 'M4') then
                Amount := - Line.Value
              else
              if FEvo.SuiType(ASySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
              begin
                if not FEvo.IsEdCodeEeSuiExempt(FEvo.GetSyStateNbr(Line.SuiState, Check.CheckDate), Line.EDCode, Check.CheckDate) then
                begin
                  if not FEvo.IsEdCodeClEeSuiExempt(Line.ClEdNbr, Line.SuiState, Check.CheckDate)
                  or (((ASySuiNbr = NV_BUSINESS_SY_SUI_NBR) or (ASySuiNbr = NV_BUSINESS_FINANCIAL_SY_SUI_NBR)) and (Line.EDCode = 'E7') and (FEvo.ClEdDescription(Line.ClEdNbr, Check.CheckDate) = 'ERNEV')) then
                    Amount := Line.Value;
                end;
              end
              else
              begin
                if not FEvo.IsEdCodeErSuiExempt(FEvo.GetSyStateNbr(Line.SuiState, Check.CheckDate), Line.EDCode, Check.CheckDate) and not FEvo.IsEdCodeClErSuiExempt(Line.ClEdNbr, Line.SuiState, Check.CheckDate) then
                  Amount := Line.Value;
              end;
            end;

            EEStateNumber := FEvo.EeStatesForSui(Check.EeNbr, FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(Check.CoNbr, ASySuiNbr, Check.CheckDate), Check.CheckDate), Check.CheckDate);

            CoSuiNbr := FEvo.GetCoSuiNbr(Check.CoNbr, ASySuiNbr, Check.CheckDate);

            if FEvo.IsCoStateSuiExempt(FEvo.GetCoSuiCoStatesNbr(CoSuiNbr, Check.CheckDate), Check.CheckDate)
              or FEvo.IsSySuiInactive(ASySuiNbr,  Check.CheckDate)
              or FEvo.IsCoSuiInactive(CoSuiNbr, Check.CheckDate) then
            begin
              Amount := 0;
            end;

            if FEvo.SuiType(ASySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
            begin
              if FEvo.EeStateEeSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'E' then
                Amount := 0
              else
              if (ASySuiNbr = NJ_EE_SDI_SY_SUI_NBR) and (FEvo.EeStateEeSdiExemptExclude(EEStateNumber, Check.CheckDate) = 'E') then
                Amount := 0;
            end
            else
            begin
              if FEvo.GetEeStatesErSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'E' then
                Amount := 0
              else
              if (ASySuiNbr = NJ_ER_SDI_SY_SUI_NBR) and (FEvo.GetEeStatesErSdiExemptExclude(EEStateNumber, Check.CheckDate) = 'E') then
                Amount := 0;
            end;

            CheckWages := CheckWages + Amount;

            if DateBetween(Check.CheckDate, DateFrom, DateTo) then
              CheckAmount := CheckAmount + Amount;

          end;
        end;
      end;
    end;

    Result := Result + CheckAmount;

  end;

end;

function TevPerson.CalcSuiTaxableWages(const AEeNbr, ASySuiNbr: Integer;
  const DateFrom, DateTo: TDateTime; const DebugList: IisParamsCollection = nil): Double;
var
  i, j, k: Integer;
  Check: IevCheck;
  MaxWages, TmpMaxWages, YTDWages: Double;
  RecipSuiList: IisListOfValues;
  SuiState: String;
  Amount, CheckAmount, CheckWages: Double;
  TmpSySuiNbr, TmpEeNbr: Integer;
  TmpState: String;
  EEStateNumber, TmpCoSuiNbr: Integer;
  Line: IevLine;
  EmployeeCoNbr: Integer;
  SuiLines: IisParamsCollection;
  SuiItem: IisListOfValues;
  GrossWages: Double;

  function RecipSui(const State: String): Integer;
  begin
    Result := 0;
    if RecipSuiList.ValueExists(State) then
      Result := RecipSuiList.Value[State];
  end;

begin

  RestoreOriginalCheckLines;
  RedistributeNegativeCheckLines(DateTo);

  SuiLines := TisParamsCollection.Create;

  YTDWages := 0;

  Result := 0;
  EmployeeCoNbr := CoNbr(AEeNbr);

  SuiState := FEvo.CoStateState(FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(EmployeeCoNbr, ASySuiNbr, DateTo), DateTo), DateTo);

  MaxWages := FEvo.CoSuiMaxWage(FEvo.GetCoSuiNbr(EmployeeCoNbr, ASySuiNbr, DateTo), DateTo);

  RecipSuiList := TisListOfValues.Create;

  for i := CheckCount - 1 downto 0 do
  begin
    Check := Checks[i];
    if ((Check.CoNbr = EmployeeCoNbr) or Check.Company.CoCombineStateSui(EmployeeCoNbr)) then
    begin
      for j := Check.SuiCount - 1 downto 0 do
      begin
        if Check.SuiTaxes[j].SySuiNbr <> ASySuiNbr then
        begin
          TmpState := FEvo.CoStateState(FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(Check.CoNbr, Check.SuiTaxes[j].SySuiNbr, DateTo), DateTo), DateTo);
          if (TmpState <> SuiState) and not RecipSuiList.ValueExists(TmpState) then
          begin
            if FEvo.ReciprocateSui(ASySuiNbr, Check.SuiTaxes[j].SySuiNbr, Check.CheckDate) then
            begin
              RecipSuiList.AddValue(TmpState, Check.SuiTaxes[j].SySuiNbr);
            end;
          end;
        end;
      end;
    end;
  end;

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    Check := Checks[i];

    if DateBetween(Check.CheckDate, DateFrom, DateTo)
    and not ((ASySuiNbr = MA_ER_MED_SY_SUI_NBR) and (FEvo.CalcCompanySuiRate(CoNbr(Check.EeNbr), ASySuiNbr, Check.CheckDate) = 0))
    and ((Check.CoNbr = EmployeeCoNbr) or Check.Company.CoCombineStateSui(EmployeeCoNbr)) then
    begin
      if FEvo.SuiBasedOnHours(ASySuiNbr, Check.CheckDate) then
      begin
        CheckAmount := 0;
        CheckWages := 0;
        for j := 0 to Check.SuiCount - 1 do
        begin
          if (Check.SuiTaxes[j].SySuiNbr = ASySuiNbr) then
          begin
            CheckWages := CheckWages + Check.SuiTaxes[j].TaxWages;
            if (Check.EeNbr = AEeNbr) then
              CheckAmount := CheckAmount + Check.SuiTaxes[j].TaxWages;
          end;
        end;
        SuiItem := SuiLines.AddParams;
        SuiItem.AddValue('SySuiNbr', ASySuiNbr);
        SuiItem.AddValue('EeNbr', Check.EeNbr);
        SuiItem.AddValue('Amount', CheckAmount);
      end
      else
      begin
        CheckAmount := 0;
        CheckWages := 0;
        for k := 0 to Check.LinesCount - 1 do
        begin
          Line := Check.Lines[k];
          if SuiState <> Line.SuiState then
          begin
            TmpSySuiNbr := RecipSui(Line.SuiState);
            if TmpSySuiNbr <> 0 then
            begin
              TmpState := FEvo.CoStateState(FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(Check.CoNbr, TmpSySuiNbr, DateTo), DateTo), DateTo);
            end
            else
            begin
              TmpState := '';
              TmpSySuiNbr := ASySuiNbr;
            end;
          end
          else
          begin
            TmpSySuiNbr := ASySuiNbr;
            TmpState := '';
          end;

          Amount := 0;
          if (SuiState = Line.SuiState) or (TmpState <> '') then
          begin
            if Line.Taxable
            or ((TmpSySuiNbr = NV_BUSINESS_SY_SUI_NBR) and ((Line.EDCode = 'M1') or (Line.EDCode = 'M4')) and (FEvo.ClEdDescription(Line.ClEdNbr, Check.CheckDate) = 'ERNEV')) then
            begin
              if (Line.EDCode = 'M1') or (Line.EDCode = 'M4') then
                Amount := - Line.Value
              else
              if FEvo.SuiType(TmpSySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
              begin
                if not FEvo.IsEdCodeEeSuiExempt(FEvo.GetSyStateNbr(Line.SuiState, Check.CheckDate), Line.EDCode, Check.CheckDate) then
                begin
                  if not FEvo.IsEdCodeClEeSuiExempt(Line.ClEdNbr, Line.SuiState, Check.CheckDate)
                  or (((TmpSySuiNbr = NV_BUSINESS_SY_SUI_NBR) or (TmpSySuiNbr = NV_BUSINESS_FINANCIAL_SY_SUI_NBR)) and (Line.EDCode = 'E7') and (FEvo.ClEdDescription(Line.ClEdNbr, Check.CheckDate) = 'ERNEV')) then
                  begin
                    Amount := Line.Value;
                  end;
                end;
              end
              else
              begin
                if not FEvo.IsEdCodeErSuiExempt(FEvo.GetSyStateNbr(Line.SuiState, Check.CheckDate), Line.EDCode, Check.CheckDate) then
                begin
                  if not FEvo.IsEdCodeClErSuiExempt(Line.ClEdNbr, Line.SuiState, Check.CheckDate) then
                  begin
                    Amount := Line.Value;
                  end;
                end;
              end;
            end;

            EEStateNumber := FEvo.EeStatesForSui(Check.EeNbr, FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(Check.CoNbr, TmpSySuiNbr, Check.CheckDate), Check.CheckDate), Check.CheckDate);

            TmpCoSuiNbr := FEvo.GetCoSuiNbr(Check.CoNbr, TmpSySuiNbr, Check.CheckDate);

            if FEvo.IsCoStateSuiExempt(FEvo.GetCoSuiCoStatesNbr(TmpCoSuiNbr, Check.CheckDate), Check.CheckDate)
              or FEvo.IsSySuiInactive( TmpSySuiNbr,  Check.CheckDate)
              or FEvo.IsCoSuiInactive(TmpCoSuiNbr, Check.CheckDate) then
            begin
              Amount := 0;
            end;

            if FEvo.SuiType(TmpSySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
            begin
              if FEvo.EeStateEeSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'E' then
                Amount := 0
              else
              if (TmpSySuiNbr = NJ_EE_SDI_SY_SUI_NBR) and (FEvo.EeStateEeSdiExemptExclude(EEStateNumber, Check.CheckDate) = 'E') then
                Amount := 0;
            end
            else
            begin
              if FEvo.GetEeStatesErSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'E' then
                Amount := 0
              else
              if (TmpSySuiNbr = NJ_ER_SDI_SY_SUI_NBR) and (FEvo.GetEeStatesErSdiExemptExclude(EEStateNumber, Check.CheckDate) = 'E') then
                Amount := 0;
            end;

            SuiItem := SuiLines.AddParams;
            SuiItem.AddValue('SySuiNbr', TmpSySuiNbr);
            SuiItem.AddValue('EeNbr', Check.EeNbr);
            SuiItem.AddValue('Amount', Amount);
            SuiItem.AddValue('CheckDate', Check.CheckDate);
            SuiItem.AddValue('CoNbr', Check.CoNbr);

            if (Amount <> 0) then
            begin
              TmpMaxWages := FEvo.CoSuiMaxWage(FEvo.GetCoSuiNbr(Check.CoNbr, TmpSySuiNbr, DateTo), DateTo);
              if TmpMaxWages <> 0 then
              begin
                if (YTDWages + CheckWages + Amount) > TmpMaxWages then
                begin
                  Amount := TmpMaxWages - (YTDWages + CheckWages);
                  if Amount < 0 then
                    Amount := 0;
                end;
              end
              else
                Amount := 0;
            end;

            CheckWages := CheckWages + Amount;

            if (TmpState = '') and (Check.EeNbr = AEeNbr) then
              CheckAmount := CheckAmount + Amount;

          end;
        end;
      end;

      if MaxWages > 0.005 then
      begin
        if YTDWages + CheckWages > MaxWages then
        begin
          if DateBetween(Check.CheckDate, DateFrom, DateTo) and (Check.EeNbr = AEeNbr) then
          begin
            if CheckAmount > (MaxWages - YTDWages) then
              CheckAmount := MaxWages - YTDWages;
            if CheckAmount < 0 then
              CheckAmount := 0;
          end;
        end;
      end;

      YTDWages := YTDWages + CheckWages;
      Result := Result + CheckAmount;

    end;
  end;

  // getting rid of negative wages
  for i := SuiLines.Count - 1 downto 0 do
  begin
    SuiItem := SuiLines[i];
    GrossWages := SuiItem.Value['Amount'];
    TmpSySuiNbr := SuiItem.Value['SySuiNbr'];
    TmpEeNbr := SuiItem.Value['EeNbr'];
    if GrossWages < - 0.005 then
    begin
      SuiItem.Value['Amount'] := 0;
      for j := i - 1 downto 0 do
      begin
        SuiItem := SuiLines[j];
        if (TmpSySuiNbr = SuiItem.Value['SySuiNbr'])
        and (TmpEeNbr = SuiItem.Value['EeNbr']) then
        begin
          if SuiItem.Value['Amount'] > 0 then
          begin
            SuiItem.Value['Amount'] := SuiItem.Value['Amount'] + GrossWages;
            if SuiItem.Value['Amount'] < 0 then
            begin
              GrossWages := SuiItem.Value['Amount'];
              SuiItem.Value['Amount'] := 0;
            end
            else
            begin
              GrossWages := 0;
              break;
            end;
          end;
        end;
      end;
      if GrossWages < -0.005 then
      begin
        SuiItem := SuiLines[i];
        SuiItem.Value['Amount'] := GrossWages;
      end;
    end;
  end;

  i := 0;
  while i < SuiLines.Count do
  begin
    SuiItem := SuiLines[i];
    if SuiItem.Value['Amount'] = 0 then
      SuiLines.DeleteParams(i)
    else
      i := i + 1;
  end;

  // applying limit
  GrossWages := 0;
  for i := 0 to SuiLines.Count - 1 do
  begin
    SuiItem := SuiLines[i];
    if SuiItem.Value['SySuiNbr'] = ASySuiNbr then
    begin
      if GrossWages + SuiItem.Value['Amount'] > MaxWages then
      begin
        if MaxWages - GrossWages > 0.005 then
          SuiItem.Value['Amount'] := MaxWages - GrossWages
        else
          SuiItem.Value['Amount'] := 0;
      end;
      GrossWages := GrossWages + SuiItem.Value['Amount'];
    end
    else
    begin
      TmpMaxWages := FEvo.CoSuiMaxWage(FEvo.GetCoSuiNbr(SuiItem.Value['CoNbr'], SuiItem.Value['SySuiNbr'], DateTo), DateTo);
      if TmpMaxWages > 0.005 then
      begin
        if GrossWages + SuiItem.Value['Amount'] > TmpMaxWages then
        begin
          SuiItem.Value['Amount'] := TmpMaxWages - GrossWages;
          if SuiItem.Value['Amount'] < 0 then
            SuiItem.Value['Amount'] := 0;
        end;
        GrossWages := GrossWages + SuiItem.Value['Amount'];
      end
      else
        GrossWages := GrossWages + SuiItem.Value['Amount'];
    end;
  end;

  Result := 0;

  for i := 0 to SuiLines.Count - 1 do
  begin
    SuiItem := SuiLines[i];
    if (SuiItem.Value['SySuiNbr'] = ASySuiNbr)
    and (SuiItem.Value['EeNbr'] = AEeNbr) then
      Result := Result + SuiItem.Value['Amount'];
  end;

end;


function TevPerson.CalcSuiTax(const AEeNbr, ASySuiNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i, j, k: Integer;
  Check: IevCheck;
  SuiState: String;
  Amount, CheckAmount, Tax, Rate: Double;
  EEStateNumber, TmpCoSuiNbr: Integer;
  Line: IevLine;
  CalculatedTaxableWages: Double;

begin
  Result := 0;
  CalculatedTaxableWages := CalcSuiTaxableWages(AEeNbr, ASySuiNbr, DateFrom, DateTo);

  RestoreOriginalCheckLines;

  SuiState := FEvo.CoStateState(FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(CoNbr(AEeNbr), ASySuiNbr, DateTo), DateTo), DateTo);

  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    Check := Checks[i];
    CheckAmount := 0;

    if Check.EeNbr = AEeNbr then
    begin
      if FEvo.SuiBasedOnHours(ASySuiNbr, Check.CheckDate) then
      begin
        for j := 0 to Check.SuiCount - 1 do
        begin
          if (Check.SuiTaxes[j].SySuiNbr = ASySuiNbr) then
            CheckAmount := CheckAmount + Check.SuiTaxes[j].TaxWages;
        end;
      end
      else
      begin
        for k := 0 to Check.LinesCount - 1 do
        begin
          Line := Check.Lines[k];

          Amount := 0;

          if SuiState = Line.SuiState then
          begin
            if Line.Taxable
            or ((ASySuiNbr = NV_BUSINESS_SY_SUI_NBR) and ((Line.EDCode = 'M1') or (Line.EDCode = 'M4')) and (FEvo.ClEdDescription(Line.ClEdNbr, Check.CheckDate) = 'ERNEV')) then
            begin
              if (Line.EDCode = 'M1') or (Line.EDCode = 'M4') then
                Amount := - Line.Value
              else
              if FEvo.SuiType(ASySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
              begin
                if not FEvo.IsEdCodeEeSuiExempt(FEvo.GetSyStateNbr(Line.SuiState, Check.CheckDate), Line.EDCode, Check.CheckDate) then
                begin
                  if not FEvo.IsEdCodeClEeSuiExempt(Line.ClEdNbr, Line.SuiState, Check.CheckDate)
                  or (((ASySuiNbr = NV_BUSINESS_SY_SUI_NBR) or (ASySuiNbr = NV_BUSINESS_FINANCIAL_SY_SUI_NBR)) and (Line.EDCode = 'E7') and (FEvo.ClEdDescription(Line.ClEdNbr, Check.CheckDate) = 'ERNEV')) then
                  begin
                    Amount := Line.Value;
                  end;
                end;
              end
              else
              begin
                if not FEvo.IsEdCodeErSuiExempt(FEvo.GetSyStateNbr(Line.SuiState, Check.CheckDate), Line.EDCode, Check.CheckDate) then
                begin
                  if not FEvo.IsEdCodeClErSuiExempt(Line.ClEdNbr, Line.SuiState, Check.CheckDate) then
                  begin
                    Amount := Line.Value;
                  end;
                end;
              end;
            end;

            EEStateNumber := FEvo.EeStatesForSui(Check.EeNbr, FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(Check.CoNbr, ASySuiNbr, Check.CheckDate), Check.CheckDate), Check.CheckDate);

            TmpCoSuiNbr := FEvo.GetCoSuiNbr(Check.CoNbr, ASySuiNbr, Check.CheckDate);

            if FEvo.IsCoStateSuiExempt(FEvo.GetCoSuiCoStatesNbr(TmpCoSuiNbr, Check.CheckDate), Check.CheckDate)
              or FEvo.IsSySuiInactive(ASySuiNbr,  Check.CheckDate)
              or FEvo.IsCoSuiInactive(TmpCoSuiNbr, Check.CheckDate) then
            begin
              Amount := 0;
            end;

            if FEvo.SuiType(ASySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
            begin
              if FEvo.EeStateEeSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'E' then
                Amount := 0
              else
              if (ASySuiNbr = NJ_EE_SDI_SY_SUI_NBR) and (FEvo.EeStateEeSdiExemptExclude(EEStateNumber, Check.CheckDate) = 'E') then
                Amount := 0;
            end
            else
            begin
              if FEvo.GetEeStatesErSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'E' then
                Amount := 0
              else
              if (ASySuiNbr = NJ_ER_SDI_SY_SUI_NBR) and (FEvo.GetEeStatesErSdiExemptExclude(EEStateNumber, Check.CheckDate) = 'E') then
                Amount := 0;
            end;

            CheckAmount := CheckAmount + Amount;

          end;
        end;
      end;
    end;

    if ASySuiNbr = MA_ER_MED_SY_SUI_NBR then
    begin
      if FEvo.CalcCompanySuiRate(Check.CoNbr, ASySuiNbr, Check.CheckDate) = 0 then
        CheckAmount := 0;
    end;

    if CheckAmount > 0 then
    begin

      if CalculatedTaxableWages < CheckAmount then
      begin
        CheckAmount := CalculatedTaxableWages;
        CalculatedTaxableWages := 0;
      end
      else
      begin
        CalculatedTaxableWages := CalculatedTaxableWages - CheckAmount;
      end;

      Rate := FEvo.CalcCompanySuiRate(Check.CoNbr, ASySuiNbr, Check.CheckDate);
      Tax := CheckAmount * Rate;
      Tax := Tax + Tax * FEvo.SuiAdditionalAssessmentRate(ASySuiNbr, Check.CheckDate) + FEvo.SuiAdditionalAssessmentAmount(ASySuiNbr, Check.CheckDate);

      EEStateNumber := FEvo.EeStatesForSui(Check.EeNbr, FEvo.GetCoSuiCoStatesNbr(FEvo.GetCoSuiNbr(Check.CoNbr, ASySuiNbr, Check.CheckDate), Check.CheckDate), Check.CheckDate);

      if FEvo.SuiType(ASySuiNbr, Check.CheckDate)[1] in [GROUP_BOX_EE, GROUP_BOX_EE_OTHER] then
      begin
        if (FEvo.EeStateEeSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'X')
        or ((ASySuiNbr = NJ_EE_SDI_SY_SUI_NBR) and (FEvo.EeStateEeSdiExemptExclude(EEStateNumber, DateTo) = 'X')) then
          Tax := 0;
      end
      else
      begin
        if (FEvo.GetEeStatesErSuiExemptExclude(EEStateNumber, Check.CheckDate) = 'X')
        or ((ASySuiNbr = NJ_ER_SDI_SY_SUI_NBR) and (FEvo.GetEeStatesErSdiExemptExclude(EEStateNumber, DateTo) = 'X')) then
          Tax := 0;
      end;

      Result := Result + Tax;

    end;

  end;

  Result := RoundTwo(Result);

end;

function TevPerson.SuiGrossWages(const AEeNbr, ASySuiNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i, j: Integer;
  Check: IevCheck;
begin
  Result := 0;

  for i := 0 to CheckCount - 1 do
  begin
    Check := Checks[i];
    if DateBetween(Check.CheckDate, DateFrom, DateTo) and ((Check.EeNbr = AEeNbr)) then
    begin
      for j := 0 to Check.SuiCount - 1 do
      begin
        if (Check.SuiTaxes[j].SySuiNbr = ASySuiNbr) then
          Result := Result + Check.SuiTaxes[j].GrossWages;
      end;
    end;
  end;

end;

function TevPerson.SuiTax(const AEeNbr, ASySuiNbr: Integer; const DateFrom,
  DateTo: TDateTime): Double;
var
  i, j: Integer;
  Check: IevCheck;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  begin
    Check := Checks[i];
    if DateBetween(Check.CheckDate, DateFrom, DateTo) and (Check.EeNbr = AEeNbr) then
    begin
      for j := 0 to Check.SuiCount - 1 do
      begin
        if (Check.SuiTaxes[j].SySuiNbr = ASySuiNbr) then
          Result := Result + Check.SuiTaxes[j].Tax;
      end;
    end;
  end;
end;

function TevPerson.GetSuiList(const AEeNbr: Integer;
  const DateFrom, DateTo: TDateTime): IisListOfValues;
var
  i, j, k: Integer;
  Check: IevCheck;
begin

  Result := TisListOfValues.Create;

  for i := CheckCount - 1 downto 0 do
  begin
    Check := Checks[i];
    if DateBetween(Check.CheckDate, DateFrom, DateTo) and (Check.EeNbr = AEeNbr) then
    begin
      for j := Check.SuiCount - 1 downto 0 do
      begin
        if not Result.ValueExists(IntToStr(Check.SuiTaxes[j].SySuiNbr)) then
        begin
          for k := 0 to Result.Count - 1 do
          begin
            if FEvo.ReciprocateSui(StrToInt(Result.Values[k].Name), Check.SuiTaxes[j].SySuiNbr, Check.CheckDate) then
            begin
              Result.AddValue(IntToStr(Check.SuiTaxes[j].SySuiNbr), StrToInt(Result.Values[k].Name));
              break;
            end;
          end;
          if not Result.ValueExists(IntToStr(Check.SuiTaxes[j].SySuiNbr)) then
            Result.AddValue(IntToStr(Check.SuiTaxes[j].SySuiNbr), Check.SuiTaxes[j].SySuiNbr);
        end;
      end;
    end;
  end;

end;

function TevPerson.SuiTaxableWages(const AEeNbr, ASySuiNbr: Integer;
  const DateFrom, DateTo: TDateTime): Double;
var
  i, j: Integer;
  Check: IevCheck;
begin
  Result := 0;

  for i := 0 to CheckCount - 1 do
  begin
    Check := Checks[i];
    if DateBetween(Check.CheckDate, DateFrom, DateTo) and (Check.EeNbr = AEeNbr) then
    begin
      for j := 0 to Check.SuiCount - 1 do
      begin
        if (Check.SuiTaxes[j].SySuiNbr = ASySuiNbr) then
          Result := Result + Check.SuiTaxes[j].TaxWages;
      end;
    end;
  end;

end;

function TevPerson.CustomEmployeeNumber(EeNbr: Integer): String;
begin
  Result := FEeNumbers.ParamsByName(IntToStr(EeNbr)).Value['CUSTOM_EMPLOYEE_NUMBER'];
end;

function TevPerson.GetHtml: IisStringList;
var
  Check: IevCheck;
  Line: IevLine;
  i, j: Integer;
  s: String;
begin

  Result := TisStringList.Create;

  if FHtml <> '' then
  begin
    Result.Text := FHtml;
    exit;
  end;

  s := '<table>';

  for i := 0 to CheckCount - 1 do
  begin
    Check := Checks[i];
    s := s + '<tr>';
    s := s + '<b>';
    s := s + '<td>';
    s := s + IntToStr(Check.EeNbr);
    s := s + '</td>';
    s := s + '<td>';
    s := s + DateToStr(Check.CheckDate);
    s := s + '</td>';
    s := s + '</tr>';
    s := s + '</b>';
    for j := 0 to Check.LinesCount - 1 do
    begin
      Line := Check.Lines[j];
      s := s + '<tr>';
      s := s + '<td>';
      s := s + Line.EDCode;
      s := s + '</td>';
      s := s + '<td>';
      s := s + FloatToStr(Line.Amount);
      s := s + '</td>';
      s := s + '</tr>';
    end;
    s := s + '<tr>';
    s := s + '<b>';
    s := s + '<td>';
    s := s + 'Fed wages calc' + FloatToStr(Check.CalcFederalTaxableWages);
    s := s + '</td>';
    s := s + '<td>';
    s := s + 'Fed wages actual' + FloatToStr(Check.FederalTaxableWages);
    s := s + '</td>';
    s := s + '</tr>';
    s := s + '</b>';

  end;


  s := s + '</table>';
  Result.Text := s;

end;

function TevPerson.CalcEeSdiGrossWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) or Checks[i].Company.CoCombineStateSui(CoNbr(AEeNbr)) then
    begin
      if EeHasState(Checks[i].EeNbr, State) then
      begin
        StateTax := Checks[i].StateTax[State];
        if Assigned(StateTax) then
          Result := Result + Checks[i].CalcEeSdiGrossWages(State);
      end;
    end;
  end;
end;

function TevPerson.EeSdiGrossWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if ((Checks[i].EeNbr = AEeNbr) or Checks[i].Company.CoCombineStateSui(CoNbr(AEeNbr))) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
        Result := Result + Checks[i].GetEeSdiGrossWages(State);
    end;
  end;
end;

function TevPerson.CalcStateGrossWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
        Result := Result + Checks[i].CalcStateGrossWages(State);
    end;
  end;
end;

function TevPerson.StateGrossWages(const AEeNbr: Integer;
  const State: String; const DateFrom, DateTo: TDateTime): Double;
var
  i: Integer;
  StateTax: IevStateTax;
begin
  Result := 0;
  for i := 0 to CheckCount - 1 do
  if DateBetween(Checks[i].CheckDate, GetBeginYear(DateTo), DateTo) then
  begin
    if (Checks[i].EeNbr = AEeNbr) and DateBetween(Checks[i].CheckDate, DateFrom, DateTo) then
    begin
      StateTax := Checks[i].StateTax[State];
      if Assigned(StateTax) then
        Result := Result + Checks[i].GetStateGrossWages(State);
    end;
  end;
end;

procedure TevPerson.RestoreOriginalCheckLines;
var
  i: Integer;
  Check: IevCheck;
begin
  for i := FChecks.Count - 1 downto 0 do
  begin
    Check := GetCheck(i);
    Check.RestoreOriginalCheckLines;
  end;
end;

{ TevCompany }

function TevCompany.CoCombineStateSui(const ACoNbr: Integer): Boolean;
begin
  Result := FEvo.CoCombineStateSui(FCoNbr, ACoNbr, FCheckDate);
end;

function TevCompany.CoNbr: Integer;
begin
  Result := FCoNbr;
end;

constructor TevCompany.Create(const AEvo: TevHistData; const ACoNbr: Integer; const ACheckDate: TDateTime);
begin
  inherited Create;
  FEvo := AEvo;
  FCoNbr := ACoNbr;
  FCheckDate := ACheckDate;
end;

end.


