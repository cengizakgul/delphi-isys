// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_DM_HISTORICAL_TAXES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SFieldCodeValues, EvTypes, EvContext,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvUIComponents, EvClientDataSet;

type
  TDM_HISTORICAL_TAXES = class(TDataModule)
    SY_FED_EXEMPTIONS: TevClientDataSet;
    SY_FED_TAX_TABLE_BRACKETS: TevClientDataSet;
    SY_FED_TAX_TABLE: TevClientDataSet;
    SY_STATES: TevClientDataSet;
    SY_STATE_MARITAL_STATUS: TevClientDataSet;
    SY_STATE_TAX_CHART: TevClientDataSet;
    SY_STATE_EXEMPTIONS: TevClientDataSet;
    SY_SUI: TevClientDataSet;
    SY_LOCALS: TevClientDataSet;
    SY_LOCAL_MARITAL_STATUS: TevClientDataSet;
    SY_LOCAL_TAX_CHART: TevClientDataSet;
    SY_LOCAL_EXEMPTIONS: TevClientDataSet;
    CL_E_D_STATE_EXMPT_EXCLD: TevClientDataSet;
    CL_E_DS: TevClientDataSet;
    CL_E_D_LOCAL_EXMPT_EXCLD: TevClientDataSet;
    CO_SUI: TevClientDataSet;
    CO_LOCAL_TAX: TevClientDataSet;
    EE_LOCALS: TevClientDataSet;
    EE: TevClientDataSet;
    EE_STATES: TevClientDataSet;
    CO_STATES: TevClientDataSet;
    procedure DM_HISTORICAL_TAXESCreate(Sender: TObject);
  private
    FLastClientNbr: TDateTime;
    FLastAsOfDate: TDateTime;
  public
    procedure LoadDataAsOfDate(AsOfDate: TDateTime);
    function LastAsOfDate: TDateTime;
    function LocalTaxType(const SyLocalsNbr: Integer): String;
    function LocalType(const SyLocalsNbr: Integer): String;
    function FollowStateExemptions(const SyLocalsNbr: Integer): Boolean;
    function LocalStatesNbr(const SyLocalsNbr: Integer): Integer;
    function LocalSyExempt(const SyLocalsNbr: Integer; const EDCodeType: String): Boolean;
    function LocalSyExclude(const SyLocalsNbr: Integer; const EDCodeType: String): Boolean;
    function LocalClExclude(const SyLocalsNbr: Integer; const CL_E_DS_NBR: Integer): Boolean;
    function LocalClInclude(const SyLocalsNbr: Integer; const CL_E_DS_NBR: Integer): Boolean;
    function LocalClExempt(const SyLocalsNbr: Integer; const CL_E_DS_NBR: Integer): Boolean;
    function IncludeInPretax(const EeLocalsNbr: Integer): Boolean;
    function SelfAdjustLocalTax(const CoLocalTaxNbr: Integer): Boolean;
    function IsEmployerLocalTax(const SyLocalsNbr: Integer): Boolean;
    function MinimumHoursWorkedPer(const SyLocalsNbr: Integer): Char;
    function MinimumHoursWorked(const SyLocalsNbr: Integer): Double;
    function PercentageOfLocalTaxWages(const EeLocalsNbr: Integer): Double;
    function LocalExcludedOnEmployeeLevel(const EeLocalsNbr: Integer): Boolean;
    function WageMinimum(const SyLocalsNbr: Integer): Double;
    function CoLocalTaxNbr(const EeLocalsNbr: Integer): Integer;
    function DeductEeLocal(const EeLocalsNbr: Integer): String;
    function IsAct32(const EeLocalsNbr: Integer): Boolean;
    function IsPhiladelphia(const EeLocalsNbr: Integer): Boolean;    
  end;

function DM_HISTORICAL_TAXES: TDM_HISTORICAL_TAXES;
function HT: TDM_HISTORICAL_TAXES;

implementation

uses
  EvUtils, SDDClasses, Variants, EvConsts;

{$R *.DFM}

function TDM_HISTORICAL_TAXES.IsAct32(const EeLocalsNbr: Integer): Boolean;
var
  SyLocalsNbr: Integer;
begin
  SyLocalsNbr := CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr(EeLocalsNbr), 'SY_LOCALS_NBR');
  Result := (SY_STATES.Lookup('SY_STATES_NBR', LocalStatesNbr(SyLocalsNbr), 'STATE') = 'PA')
    and ((LocalType(SyLocalsNbr) = 'R') or (LocalType(SyLocalsNbr) = 'N') or (LocalType(SyLocalsNbr) = 'S'))
    and (SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'COMBINE_FOR_TAX_PAYMENTS') = 'Y');
end;

function TDM_HISTORICAL_TAXES.IsPhiladelphia(const EeLocalsNbr: Integer): Boolean;
var
  SyLocalsNbr: Integer;
begin
  SyLocalsNbr := CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr(EeLocalsNbr), 'SY_LOCALS_NBR');
  Result := (SY_STATES.Lookup('SY_STATES_NBR', LocalStatesNbr(SyLocalsNbr), 'STATE') = 'PA')
    and ((LocalType(SyLocalsNbr) = 'R') or (LocalType(SyLocalsNbr) = 'N') or (LocalType(SyLocalsNbr) = 'S'))
    and (SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'COMBINE_FOR_TAX_PAYMENTS') = 'N');
end;

function TDM_HISTORICAL_TAXES.DeductEeLocal(const EeLocalsNbr: Integer): String;
begin
  Result := EE_LOCALS.Lookup('EE_LOCALS_NBR', EeLocalsNbr, 'DEDUCT');
end;

function TDM_HISTORICAL_TAXES.CoLocalTaxNbr(const EeLocalsNbr: Integer): Integer;
begin
  Result := EE_LOCALS.Lookup('EE_LOCALS_NBR', EeLocalsNbr, 'CO_LOCAL_TAX_NBR');
end;

function TDM_HISTORICAL_TAXES.WageMinimum(const SyLocalsNbr: Integer): Double;
begin
  Result := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'WAGE_MINIMUM'), 0);
end;

function TDM_HISTORICAL_TAXES.LocalExcludedOnEmployeeLevel(const EeLocalsNbr: Integer): Boolean;
begin
  Result := HT.EE_LOCALS.Lookup('EE_LOCALS_NBR', EeLocalsNbr, 'EXEMPT_EXCLUDE') = 'X';
end;

function TDM_HISTORICAL_TAXES.PercentageOfLocalTaxWages(const EeLocalsNbr: Integer): Double;
begin
  Result := ConvertNull(EE_LOCALS.Lookup('EE_LOCALS_NBR', EeLocalsNbr, 'PERCENTAGE_OF_TAX_WAGES'), 0);
end;

function TDM_HISTORICAL_TAXES.MinimumHoursWorked(const SyLocalsNbr: Integer): Double;
begin
  Result := ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'MINIMUM_HOURS_WORKED'), 0);
end;

function TDM_HISTORICAL_TAXES.MinimumHoursWorkedPer(const SyLocalsNbr: Integer): Char;
begin
  Result := String(ConvertNull(HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'MINIMUM_HOURS_WORKED_PER'), ' '))[1];
end;

function TDM_HISTORICAL_TAXES.IsEmployerLocalTax(const SyLocalsNbr: Integer): Boolean;
begin
  Result := LocalTaxType(SyLocalsNbr) = GROUP_BOX_ER;
end;

function TDM_HISTORICAL_TAXES.SelfAdjustLocalTax(const CoLocalTaxNbr: Integer): Boolean;
begin
  Result := CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr, 'SELF_ADJUST_TAX') = 'Y';
end;

function TDM_HISTORICAL_TAXES.LocalType(const SyLocalsNbr: Integer): String;
begin
  Result := SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'LOCAL_TYPE');
end;

function TDM_HISTORICAL_TAXES.LocalTaxType(const SyLocalsNbr: Integer): String;
begin
  Result := SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'TAX_TYPE');
end;

function TDM_HISTORICAL_TAXES.FollowStateExemptions(const SyLocalsNbr: Integer): Boolean;
begin
  Result := SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'PRINT_RETURN_IF_ZERO') = 'Y';
end;

function TDM_HISTORICAL_TAXES.LocalStatesNbr(const SyLocalsNbr: Integer): Integer;
begin
  Result := SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'SY_STATES_NBR');
end;

function TDM_HISTORICAL_TAXES.LocalSyExempt(const SyLocalsNbr: Integer; const EDCodeType: String): Boolean;
begin
  if FollowStateExemptions(SyLocalsNbr) then
    Result := (SY_STATE_EXEMPTIONS.NewLookup('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([LocalStatesNbr(SyLocalsNbr), EDCodeType]), 'EXEMPT_STATE') = GROUP_BOX_EXEMPT)
  else
    Result := (SY_LOCAL_EXEMPTIONS.Lookup('SY_LOCALS_NBR;E_D_CODE_TYPE', VarArrayOf([SyLocalsNbr, EDCodeType]), 'EXEMPT') = GROUP_BOX_EXEMPT);
end;

function TDM_HISTORICAL_TAXES.LocalSyExclude(const SyLocalsNbr: Integer; const EDCodeType: String): Boolean;
begin
  Result := SY_LOCAL_EXEMPTIONS.Lookup('SY_LOCALS_NBR;E_D_CODE_TYPE', VarArrayOf([SyLocalsNbr, EDCodeType]), 'EXEMPT') = GROUP_BOX_EXCLUDE;
end;

function TDM_HISTORICAL_TAXES.LocalClExclude(const SyLocalsNbr: Integer; const CL_E_DS_NBR: Integer): Boolean;
begin
  Result := HT.CL_E_D_LOCAL_EXMPT_EXCLD.Lookup('SY_LOCALS_NBR;CL_E_DS_NBR', VarArrayOf([SyLocalsNbr, CL_E_DS_NBR]), 'EXEMPT_EXCLUDE') = GROUP_BOX_EXCLUDE;
end;

function TDM_HISTORICAL_TAXES.LocalClExempt(const SyLocalsNbr: Integer; const CL_E_DS_NBR: Integer): Boolean;
begin
  Result := CL_E_D_LOCAL_EXMPT_EXCLD.Lookup('SY_LOCALS_NBR;CL_E_DS_NBR', VarArrayOf([SyLocalsNbr, CL_E_DS_NBR]), 'EXEMPT_EXCLUDE') = GROUP_BOX_EXEMPT;
end;

function DM_HISTORICAL_TAXES: TDM_HISTORICAL_TAXES;
begin
  Result := TDM_HISTORICAL_TAXES(ctx_PayrollCalculation.DM_HISTORICAL_TAXES);
end;

function TDM_HISTORICAL_TAXES.IncludeInPretax(const EeLocalsNbr: Integer): Boolean;
begin
  Result := EE_LOCALS.Lookup('EE_LOCALS_NBR', EeLocalsNbr, 'INCLUDE_IN_PRETAX') = 'Y';
end;

function HT: TDM_HISTORICAL_TAXES;
begin
  Result := DM_HISTORICAL_TAXES;
end;

procedure TDM_HISTORICAL_TAXES.DM_HISTORICAL_TAXESCreate(Sender: TObject);
var
  i: Integer;
begin
  FLastAsOfDate := 0;
  FLastClientNbr := 0;
  for i := 0 to ComponentCount-1 do
    if Components[i] is TevClientDataSet then
      TevClientDataSet(Components[i]).AutoAddIndexes := True;
end;

function TDM_HISTORICAL_TAXES.LastAsOfDate: TDateTime;
begin
  Result := FLastAsOfDate;
end;

procedure TDM_HISTORICAL_TAXES.LoadDataAsOfDate(AsOfDate: TDateTime);
var
  I: Integer;
  lDataSet, lGlobalDataSet: TevClientDataSet;
  lTableClass: TddTableClass;
begin
  for I := 0 to ComponentCount - 1 do
    if Components[I] is TevClientDataSet then
    begin
      lDataSet := TevClientDataSet(Components[I]);
      if lDataSet.Active then
        if AsOfDate <> FLastAsOfDate then
          lDataSet.Close
        else if (ctx_DataAccess.ClientID <> FLastClientNbr)
        and (Copy(lDataSet.Name, 1, 3) <> 'SY_')
        and (Copy(lDataSet.Name, 1, 3) <> 'SB_') then
          lDataSet.Close
        else
        begin
          lTableClass := TddTableClass(FindClass('T'+ lDataSet.Name));
          Assert(Assigned(lTableClass));
          lGlobalDataSet := ctx_DataAccess.GetDataSet(lTableClass);
          Assert(Assigned(lGlobalDataSet));
          if lGlobalDataSet.LastTimeModified > lDataSet.LastTimeOpened then
            lDataSet.Close;
        end;
      if not lDataSet.Active then
        with TExecDSWrapper.Create('SelectHist') do
        begin
          SetMacro('Columns', '*');
          SetMacro('TableName', lDataSet.Name);
          SetParam('StatusDate', AsOfDate);
          if Copy(lDataSet.Name, 1, 3) = 'SY_' then
            ctx_DataAccess.GetSyCustomData(lDataSet, AsVariant)
          else if Copy(lDataSet.Name, 1, 3) = 'SB_' then
            ctx_DataAccess.GetSbCustomData(lDataSet, AsVariant)
          else
            ctx_DataAccess.GetCustomData(lDataSet, AsVariant);
        end;
    end;
  FLastAsOfDate := AsOfDate;
  FLastClientNbr := ctx_DataAccess.ClientID;
end;

function TDM_HISTORICAL_TAXES.LocalClInclude(const SyLocalsNbr,
  CL_E_DS_NBR: Integer): Boolean;
begin
  Result := HT.CL_E_D_LOCAL_EXMPT_EXCLD.Lookup('SY_LOCALS_NBR;CL_E_DS_NBR', VarArrayOf([SyLocalsNbr, CL_E_DS_NBR]), 'EXEMPT_EXCLUDE') = GROUP_BOX_INCLUDE;
end;

end.
