// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCheckLineManager;

interface

uses
  Classes,  EvClientDataSet;

type

  LineToAdd = record
    EDCodeNbr: Integer;
    Amount: Real;
    DivisionNbr: Variant;
    BranchNbr: Variant;
    DepartmentNbr: Variant;
    TeamNbr: Variant;
    JobNbr: Variant;
    StateNbr: Variant;
    SUINbr: Variant;
    WCcodeNbr: Variant;
    ShiftNbr: Variant;
    PieceNbr: Variant;
    LineItemDate: Variant;
  end;

  ptrLineToAdd = ^LineToAdd;

  TLinesToAdd = class
  private
    LinesList: TList;
    function GetLinesCount: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property Count: Integer read GetLinesCount;
    procedure Add(EDCodeNbr: Integer; Amount: Real; LineToCopy: TevClientDataSet);
    procedure Delete;
    function GetEDCodeNbr: Integer;
    function GetAmount: Real;
    procedure AssignFields(CheckLine: TevClientDataSet);
  end;

implementation

constructor TLinesToAdd.Create;
begin
  LinesList := TList.Create;
end;

destructor TLinesToAdd.Destroy;
var
  i: Integer;
begin
  for i := 0 to LinesList.Count - 1 do
    Dispose(ptrLineToAdd(LinesList[i]));
  LinesList.Free;
  inherited;
end;

function TLinesToAdd.GetLinesCount: Integer;
begin
  Result := LinesList.Count;
end;

procedure TLinesToAdd.Add(EDCodeNbr: Integer; Amount: Real; LineToCopy: TevClientDataSet);
var
  TempLineToAdd: ptrLineToAdd;
begin
  New(TempLineToAdd);
  TempLineToAdd^.EDCodeNbr := EDCodeNbr;
  TempLineToAdd^.Amount := Amount;
  TempLineToAdd^.DivisionNbr := LineToCopy.FieldByName('CO_DIVISION_NBR').Value;
  TempLineToAdd^.BranchNbr := LineToCopy.FieldByName('CO_BRANCH_NBR').Value;
  TempLineToAdd^.DepartmentNbr := LineToCopy.FieldByName('CO_DEPARTMENT_NBR').Value;
  TempLineToAdd^.TeamNbr := LineToCopy.FieldByName('CO_TEAM_NBR').Value;
  TempLineToAdd^.JobNbr := LineToCopy.FieldByName('CO_JOBS_NBR').Value;
  TempLineToAdd^.StateNbr := LineToCopy.FieldByName('EE_STATES_NBR').Value;
  TempLineToAdd^.SUINbr := LineToCopy.FieldByName('EE_SUI_STATES_NBR').Value;
  TempLineToAdd^.WCcodeNbr := LineToCopy.FieldByName('CO_WORKERS_COMP_NBR').Value;
  TempLineToAdd^.ShiftNbr := LineToCopy.FieldByName('CO_SHIFTS_NBR').Value;
  TempLineToAdd^.PieceNbr := LineToCopy.FieldByName('CL_PIECES_NBR').Value;
  TempLineToAdd^.LineItemDate := LineToCopy.FieldByName('LINE_ITEM_DATE').Value;
  LinesList.Add(TempLineToAdd);
end;

procedure TLinesToAdd.Delete;
begin
  Dispose(ptrLineToAdd(LinesList[0]));
  LinesList.Delete(0);
end;

function TLinesToAdd.GetEDCodeNbr: Integer;
begin
  Result := ptrLineToAdd(LinesList.Items[0])^.EDCodeNbr;
end;

function TLinesToAdd.GetAmount: Real;
begin
  Result := ptrLineToAdd(LinesList.Items[0])^.Amount;
end;

procedure TLinesToAdd.AssignFields(CheckLine: TevClientDataSet);
begin
  with CheckLine do
  begin
    FieldByName('CO_DIVISION_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.DivisionNbr;
    FieldByName('CO_BRANCH_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.BranchNbr;
    FieldByName('CO_DEPARTMENT_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.DepartmentNbr;
    FieldByName('CO_TEAM_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.TeamNbr;
    FieldByName('CO_JOBS_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.JobNbr;
    FieldByName('EE_STATES_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.StateNbr;
    FieldByName('EE_SUI_STATES_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.SUINbr;
    FieldByName('CO_WORKERS_COMP_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.WCcodeNbr;
    FieldByName('CO_SHIFTS_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.ShiftNbr;
    FieldByName('CL_PIECES_NBR').Value := ptrLineToAdd(LinesList.Items[0])^.PieceNbr;
    FieldByName('LINE_ITEM_DATE').Value := ptrLineToAdd(LinesList.Items[0])^.LineItemDate;
  end;
end;

end.
