unit EvExchangeBasicFunctions;

interface

uses
  Windows, SysUtils, EvUtils,  Classes, IsBaseClasses, EvCommonInterfaces, IEMap,
  EvContext;

type
  TEvExchangeFunctionsBasic = class(TIEDefinitionBase, IEvoXDefinition)
  private
    FImpEngine: IIEMap;
    FRowValues: IisListOfValues;
    FCurrentPackageField: IEvExchangePackageField;
    FCustomWarnings: IisStringList;
    FChangeLogList: IisListOfValues;
    FCurrentCustomCompanyNumber: String;
    FCurrentCoNbr: integer;
    FCurrentClientId: integer;
  protected
    function FindRecordInChangeLog(const AOperation: Char; const AClientNbr: integer;
      const ATableName: String; const ARecordNbr: integer): IevExchangeChangeRecord;
    function GetCoNbr(const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray): Variant;
    function GetEeNbr(const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray): Variant;
    function GetClPersonNbr(const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray): Variant;
    function FindIndexByName(const KeyTables, KeyFields: TStringArray; TableName, FieldName: String): integer;
    procedure FindAndAddKeyIfNotExists(var AKeyTables, AKeyFields: TStringArray; var AKeyValues: TVariantArray;
      const ATableName, AFieldName: String; const AValue: Variant);

    // IEvoXDefinition
    function  GetImpEngine: IIEMap;
    function  GetRowValues: IisListOfValues;
    procedure SetRowValues(const AValue: IisListOfValues);
    function  GetCurrentPackageField: IEvExchangePackageField;
    procedure SetCurrentPackageField(const APackageField: IEvExchangePackageField);
    function  GetCustomWarnings: IisStringList;
    procedure SetCustomWarnings(const ACustomWarnings: IisStringList);
    function  GetChangeLogList: IisListOfValues;
  public
    constructor Create(const AIEMap: IIEMap; const AChangeLogList: IisListOfValues); reintroduce;
  published
    function GetFirstWord(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function GetLastWord(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function GetCharFromSecondWord(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function ConvertPhone(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function ConvertSSN(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
  end;

implementation

uses Variants, isBasicUtils;

{ TEvExchangeFunctionsBasic }

function TEvExchangeFunctionsBasic.GetFirstWord(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := Table;
    OFields[0] := Field;
    if (Value <> null) and (Pos(' ', Value) > 0) then
      OValues[0] := Copy(Value, 1, Pred(Pos(' ', Value)))
    else
      OValues[0] := Value;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeFunctionsBasic.GetLastWord(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  val: String;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := Table;
    OFields[0] := Field;
    val := VarToStr(Value);
    if (Value <> null) and (Pos(' ', val) > 0) then
      OValues[0] := GetPrevStrValue(val, ' ')
    else
      OValues[0] := val;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeFunctionsBasic.GetCharFromSecondWord(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 1);
    Setlength(OFields, 1);
    SetLength(OValues, 1);
    OFields[0] := Field;
    OTables[0] := Table;
    if (Value <> null) and (Pos(' ', Value) > 0) then
      OValues[0] := Copy(Value, Succ(Pos(' ', Value)), Length(Value))
    else
      OValues[0] := null;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeFunctionsBasic.ConvertPhone(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  val: String;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := Table;
    OFields[0] := Field;
    if Value = null then
      OValues[0] := null
    else
    begin
      val := vartostr( Value );
      val := StringReplace(val, '.', '-', [rfReplaceAll]);
      if (Length(val) > 4) and (Copy(val, Length(val) - 4, 1) <> '-') then
        Insert('-', val, Length(val) - 3);
      if (Length(val) > 8) and (val[Length(val) - 8] in ['0'..'9']) then
        Insert('-', val, Length(val) - 7)
      else if (Length(val) > 8) and (val[Length(val) - 8] = ')') then
        Insert(' ', val, Length(val) - 7);

      OValues[0] := val;
    end;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeFunctionsBasic.ConvertSSN(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  val: String;
begin
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := Table;
    OFields[0] := Field;
    if Value = null then
      OValues[0] := null
    else
    begin
      val := Trim(vartostr( Value ));
      CheckCondition(Length(val) >= 9, 'Cannot convert string to SSN. Wrong length of string: ' + val);
      val := Copy(val, 1, 3) + '-' + Copy(val, 4, 2) + '-' + Copy(val, 6,4);
      OValues[0] := val;
    end;
  end
  else
    Assert(false, 'It is not export function');
end;

constructor TEvExchangeFunctionsBasic.Create(const AIEMap: IIEMap; const AChangeLogList: IisListOfValues);
begin
  inherited Create;

  CheckCondition(Assigned(AIEMap), 'Internal error. Import engine object is not assigned');
  FImpEngine := AIEMap;
  CheckCondition(Assigned(AChangeLogList), 'Internal error. Change Log object is not assigned');
  FChangeLogList := AChangeLogList;

  FCurrentCoNbr := -1;
  FCurrentClientId := -1;
end;

function TEvExchangeFunctionsBasic.FindRecordInChangeLog(const AOperation: Char; const AClientNbr: integer;
  const ATableName: String;  const ARecordNbr: integer): IevExchangeChangeRecord;
var
  i: integer;
  LogRecord: IevExchangeChangeRecord;
begin
  Result := nil;
  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;
    if (LogRecord.GetOperation = AOperation) and (LogRecord.GetClientNbr = AClientNbr) and
      (LogRecord.GetTableName = ATableName) and (LogRecord.GetRecordNbr = ARecordNbr) then
    begin
      Result := LogRecord;
      exit;
    end;
  end;
end;

function TEvExchangeFunctionsBasic.GetChangeLogList: IisListOfValues;
begin
  Result := FChangeLogList;
end;

function TEvExchangeFunctionsBasic.GetCurrentPackageField: IEvExchangePackageField;
begin
  Result := FCurrentPackageField;
end;

function TEvExchangeFunctionsBasic.GetCustomWarnings: IisStringList;
begin
  Result := FCustomWarnings;
end;

function TEvExchangeFunctionsBasic.GetImpEngine: IIEMap;
begin
  Result := FImpEngine;
end;

function TEvExchangeFunctionsBasic.GetRowValues: IisListOfValues;
begin
  Result := FRowValues;
end;

procedure TEvExchangeFunctionsBasic.SetCurrentPackageField(const APackageField: IEvExchangePackageField);
begin
  FCurrentPackageField := APackageField;
end;

procedure TEvExchangeFunctionsBasic.SetCustomWarnings(const ACustomWarnings: IisStringList);
begin
  FCustomWarnings := ACustomWarnings;
end;

procedure TEvExchangeFunctionsBasic.SetRowValues(const AValue: IisListOfValues);
begin
  FRowValues := AValue;
end;

function TEvExchangeFunctionsBasic.GetCoNbr(const KeyTables, KeyFields: TStringArray; const KeyValues: TVariantArray): Variant;
var
  index: integer;
begin
  Result := null;

  index := FindIndexByName(KeyTables, KeyFields, 'CO', 'CUSTOM_COMPANY_NUMBER');
  if index <> -1 then
  begin
    if (VarToStr(KeyValues[index]) <> FCurrentCustomCompanyNumber) or (FCurrentClientId <> ctx_DataAccess.ClientId) then
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO'), 'CUSTOM_COMPANY_NUMBER=''' + VarToStr(KeyValues[index]) + '''');
      Result := GetImpEngine.IDS('CO').FieldValues['CO_NBR'];
      CheckCondition(Result <> null, 'CO_NBR cannot be null');
      FCurrentCustomCompanyNumber := KeyValues[index];
      FCurrentCoNbr := Result;
      FCurrentClientId := ctx_DataAccess.ClientId;
    end
    else
      Result := FCurrentCoNbr;
  end
  else
    raise Exception.Create('CUSTOM_COMPANY_NUMBER is required');
end;

function TEvExchangeFunctionsBasic.FindIndexByName(const KeyTables, KeyFields: TStringArray; TableName, FieldName: String): integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to High(KeyTables) do
    if (KeyTables[i] = TableName) and (KeyFields[i] = FieldName) then
    begin
      Result := i;
      break;
    end;
end;

procedure TEvExchangeFunctionsBasic.FindAndAddKeyIfNotExists(var AKeyTables, AKeyFields: TStringArray; var AKeyValues: TVariantArray;
  const ATableName, AFieldName: String; const AValue: Variant);
var
  index: integer;
begin
  index := FindIndexByName(AKeyTables, AKeyFields, ATableName, AFieldName);
  if index = -1 then
  begin
     // it's a trick because I need to have this field in list of key fields but I cannot include it to package due IEMap error in RestrictKey
      SetLength(AKeyTables, Succ(Length(AKeyTables)));
      SetLength(AKeyFields, Succ(Length(AKeyFields)));
      SetLength(AKeyValues, Succ(Length(AKeyValues)));

      AKeyTables[High(AKeyTables)] := ATableName;
      AKeyFields[High(AKeyFields)] := AFieldName;
      AKeyValues[High(AKeyValues)] := AValue;
  end;
end;

function TEvExchangeFunctionsBasic.GetEeNbr(const KeyTables,
  KeyFields: TStringArray; const KeyValues: TVariantArray): Variant;
var
  CoNbr: Variant;
  index: integer;
begin
  Result := null;
  CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

  index := FindIndexByName(KeyTables, KeyFields, 'EE', 'CUSTOM_EMPLOYEE_NUMBER');
  if index <> -1 then
  begin
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('EE'), 'CO_NBR=' + VarToStr(CoNbr) + ' and CUSTOM_EMPLOYEE_NUMBER=''' + VarToStr(KeyValues[index]) + '''');
    Result := GetImpEngine.IDS('EE').FieldValues['EE_NBR'];
  end
  else
    raise Exception.Create('CUSTOM_EMPLOYEE_NUMBER is required');
end;

function TEvExchangeFunctionsBasic.GetClPersonNbr(const KeyTables,
  KeyFields: TStringArray; const KeyValues: TVariantArray): Variant;
var
  CoNbr: Variant;
  index: integer;
begin
  Result := null;
  CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

  index := FindIndexByName(KeyTables, KeyFields, 'EE', 'CUSTOM_EMPLOYEE_NUMBER');
  if index <> -1 then
  begin
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('EE'), 'CO_NBR=' + VarToStr(CoNbr) + ' and CUSTOM_EMPLOYEE_NUMBER=''' + VarToStr(KeyValues[index]) + '''');
    Result := GetImpEngine.IDS('EE').FieldValues['CL_PERSON_NBR'];
  end
  else
    raise Exception.Create('CUSTOM_EMPLOYEE_NUMBER is required');
end;

end.
