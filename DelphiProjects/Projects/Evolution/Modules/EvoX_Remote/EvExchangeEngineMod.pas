unit EvExchangeEngineMod;

interface

implementation

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvExchangeEeEngine,
  isBasicUtils, EvExchangeConsts, EvMainboard, EvContext, EvExchangeYTDPrEngine,
  EvExchangeGenericPrEngine, EvExchangeMultiBatchPrEngine, EvExchangeCLEngine;

type
  TEvExchangeEngineMod = class(TisInterfacedObject, IevExchangeEngine)
  protected
    // IEvExchangeEngine implementation
    function  Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile; const APackage: IevExchangePackage;
      const AOptions: IisStringListRO; const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues;  // input dataset can be changed after import!!!
    function  CheckMapFile(const AMapFile: IEvExchangeMapFile;  const APackage: IEvExchangePackage; const AOptions: IisStringListRO): IisListOfValues;
  end;

function GetEvoXEngine: IevExchangeEngine;
begin
  if Context.License.EvoX then
    Result := TEvExchangeEngineMod.Create
  else
    Result := nil;  
end;

{ TEvExchangeEngineMod }

function TEvExchangeEngineMod.CheckMapFile(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage;
  const AOptions: IisStringListRO): IisListOfValues;
var
  Engine: IevExchangeEngine;
begin

  if AnsiSameText(APackage.GetEngineName, EvoXEEPackageEngine) then
    Engine := TEvExchangeEeEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoXYTDPRPackageEngine)  then
    Engine := TEvExchangeYTDPrEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoGenericPrPackageEngine) then
    Engine := TEvExchangeGenericPrEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoMultiBatchPrPackageEngine) then
    Engine := TEvExchangeMultiBatchPrEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoClientPackageEngine) then
    Engine := TEvExchangeCLEngine.Create
  else
    raise Exception.Create('Unknown name of engine:"' + APackage.GetEngineName + '"');

  Result := Engine.CheckMapFile(AMapFile, APackage, AOptions);
end;

function TEvExchangeEngineMod.Import(const AInputData: IevDataSet;
  const AMapFile: IEvExchangeMapFile; const APackage: IevExchangePackage;
  const AOptions: IisStringListRO; const ACustomCoNbr: String;
  AEffectiveDate: TDateTime): IisListOfValues;
var
  Engine: IevExchangeEngine;
begin

  if AnsiSameText(APackage.GetEngineName, EvoXEEPackageEngine) then
    Engine := TEvExchangeEeEngine.Create
  else if AnsiSameText(APackage.GetEngineName, EvoXYTDPRPackageEngine) then
    Engine := TEvExchangeYTDPrEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoGenericPrPackageEngine) then
    Engine := TEvExchangeGenericPrEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoMultiBatchPrPackageEngine) then
    Engine := TEvExchangeMultiBatchPrEngine.Create
  else if AnsiSametext(APackage.GetEngineName, EvoClientPackageEngine) then
    Engine := TEvExchangeCLEngine.Create
  else
    raise Exception.Create('Unknown name of engine:"' + APackage.GetEngineName + '"');


  Result := Engine.Import(AInputData, AMapFile, APackage, AOptions, ACustomCoNbr, AEffectiveDate);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEvoXEngine, IevExchangeEngine, 'EvoX Remote Module');
end.

