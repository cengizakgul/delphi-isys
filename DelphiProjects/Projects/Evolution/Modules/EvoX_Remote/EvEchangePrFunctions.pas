unit EvEchangePrFunctions;

interface

uses
  IEMap, EvUtils, SysUtils, EvConsts, db, classes, EvTypes, Variants, isBasicUtils, IsBaseClasses,
  EvCommonInterfaces, EvExchangeBasicFunctions;

type
  TEvExchangePrFunctions = class(TEvExchangeFunctionsBasic, IEvoXDefinition)
  private
  protected
  published
    function FindDivisionNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function FindBranchNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function FindDepartmentNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function FindTeamNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcAgency(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCoShift(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcClPiece(IsImport: Boolean; const Table,
      Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
  public
  end;

implementation

uses kbmMemTable, EvExchangeConsts, EvExchangeUtils;

{ TEvExchangePrFunctions }

function TEvExchangePrFunctions.CalcAgency(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  SyAgencyNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CL_AGENCY_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SB_AGENCY'), 'NAME=''' + VarToStr(Value) + '''');
      SyAgencyNbr := GetImpEngine.IDS('SB_AGENCY').FieldValues['SB_AGENCY_NBR'];
      CheckCondition(SyAgencyNbr <> null, 'Agency not found in SB_AGENCY table. Value: "' + VarToStr(Value) + '"');

      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_AGENCY'), 'SB_AGENCY_NBR=' + VarToStr(SyAgencyNbr));
      OValues[0] := GetImpEngine.IDS('CL_AGENCY').FieldValues['CL_AGENCY_NBR'];
      CheckCondition(OValues[0], 'Cannot find Agency in CL_AGENCY table. Value is "' + VarToStr(Value) + '"' + '. SB_AGENCY_NBR=' + IntToStr(SyAgencyNbr));
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangePrFunctions.CalcClPiece(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  ClPieceNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CL_PIECES_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_PIECES'), 'NAME=''' + VarToStr(Value) + '''');
      ClPieceNbr := GetImpEngine.IDS('CL_PIECES').FieldValues['CL_PIECES_NBR'];
      CheckCondition(ClPieceNbr <> null, 'Cannot find Piece in CL_PIECES table. Name is "' + VarToStr(Value) + '"');
      OValues[0] := ClPieceNbr;
    end;
    
    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangePrFunctions.CalcCoShift(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoShiftNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CO_SHIFTS_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_SHIFTS'), 'NAME=''' + VarToStr(Value) + ''' and CO_NBR=' + VarToStr(CoNbr));
      CoShiftNbr := GetImpEngine.IDS('CO_SHIFTS').FieldValues['CO_SHIFTS_NBR'];
      CheckCondition(CoShiftNbr <> null, 'Cannot find Shift in CO_SHIFTS table. Name is "' + VarToStr(Value) + '"');
      OValues[0] := CoShiftNbr;
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangePrFunctions.FindBranchNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoDivisionNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CO_BRANCH_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      CoDivisionNbr := GetRowValues.Value[EvoXYTDDivision + ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)];
      CheckCondition(CoDivisionNbr <> null, 'Division cannot be empty if branch is mapped');

      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_BRANCH'), 'CUSTOM_BRANCH_NUMBER=''' + PadStringLeft(Trim(VarToStr(Value)), ' ', 20) +
        ''' and CO_DIVISION_NBR=' + VarToStr(CoDivisionNbr));
      CheckCondition(GetImpEngine.IDS('CO_BRANCH').RecordCount < 2, 'Found too many branches for the same division. Branch is "' + VarToStr(Value) + '"');
      OValues[0] := GetImpEngine.IDS('CO_BRANCH').FieldValues['CO_BRANCH_NBR'];
      CheckCondition(OValues[0] <> null, 'Cannot find Branch in CO_BRANCH table. Value is "' + VarToStr(Value) + '"');
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangePrFunctions.FindDepartmentNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoBranchNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CO_DEPARTMENT_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      CoBranchNbr := GetRowValues.Value[EvoXYTDBranch + ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)];
      CheckCondition(CoBranchNbr <> null, 'Branch cannot be empty if department is mapped');

      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_DEPARTMENT'), 'CUSTOM_DEPARTMENT_NUMBER=''' + PadStringLeft(Trim(VarToStr(Value)), ' ', 20) +
        '''  and CO_BRANCH_NBR=' + VarToStr(CoBranchNbr));
      CheckCondition(GetImpEngine.IDS('CO_DEPARTMENT').RecordCount < 2, 'Found too many departments for the same branch. Department is "' + VarToStr(Value) + '"');
      OValues[0] := GetImpEngine.IDS('CO_DEPARTMENT').FieldValues['CO_DEPARTMENT_NBR'];
      CheckCondition(OValues[0] <> null, 'Cannot find Department in CO_DEPARTMENT table. Value is "' + VarToStr(Value) + '"');
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangePrFunctions.FindDivisionNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CO_DIVISION_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_DIVISION'), 'CUSTOM_DIVISION_NUMBER=''' + PadStringLeft(Trim(VarToStr(Value)), ' ', 20) +
        ''' and CO_NBR=' + VarToStr(CoNbr));
      OValues[0] := GetImpEngine.IDS('CO_DIVISION').FieldValues['CO_DIVISION_NBR'];
      CheckCondition(OValues[0] <> null, 'Cannot find Division in CO_DIVISION table. Value is "' + VarToStr(Value) + '"');
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangePrFunctions.FindTeamNbr(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoDepartmentNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'PR_CHECK_LINES';
    OFields[0] := 'CO_TEAM_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      CoDepartmentNbr := GetRowValues.Value[EvoXYTDDepartment + ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)];
      CheckCondition(CoDepartmentNbr <> null, 'Department cannot be empty if team is mapped');

      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_TEAM'), 'CUSTOM_TEAM_NUMBER=''' + PadStringLeft(Trim(VarToStr(Value)), ' ', 20) +
        ''' and CO_DEPARTMENT_NBR=' + VarToStr(CoDepartmentNbr));
      CheckCondition(GetImpEngine.IDS('CO_TEAM').RecordCount < 2, 'Found too many teams for the same department. Team is "' + VarToStr(Value) + '"');
      OValues[0] := GetImpEngine.IDS('CO_TEAM').FieldValues['CO_TEAM_NBR'];
      CheckCondition(OValues[0] <> null, 'Cannot find Team in CO_TEAM table. Value is "' + VarToStr(Value) + '"');
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

end.
