unit EvEchangeEeFunctions;

interface

uses
  IEMap, EvUtils, SysUtils, EvConsts, db, classes, EvTypes, Variants, isBasicUtils, IsBaseClasses,
  EvCommonInterfaces, EvExchangeBasicFunctions, EvDataset, EvContext;

type
  TEvExchangeEeFunctions = class(TEvExchangeFunctionsBasic, IEvoXDefinition)
  private
    procedure AddALD_DBDTKeys(var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray);
    procedure DoEELocalsCheckConditionsForWorkFields(const ADisplayFieldName: String; const ACoLocalTaxNbr: integer);
  protected
  published
    function ValidateMaritalStatus(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function HomeState(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function HomeSUI(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function HomeSDI(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcEELocalsNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcScheduledEDsNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcWorkersCompensation(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcTOAType(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCoShift(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcClPiece(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcAdditionalInfo(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcAdditionalInfoValue(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcALDJobs(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcALDWorkersCompensation(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcALDPercentage(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcDefaultJob(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function GenerateKeyForDirectDeposit(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcMultipleScheduledEDGroup(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcWCCode(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCompanyHomeState(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcSchoolNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcDisabilityNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcSkillNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCoHrPropertyNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCoHrPerformanceRatingNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcEESchoolNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcEECourseNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCoHrCarNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCoHrAttendanceNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcClBenefitRatesNbrForEE(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcBenefitSubtype(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcBenefitSubtypeForEE(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcPositionGrade(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcAcaBenefit(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcAcaLowestCostBenefitSubtype(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcEeBenefitsNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcClPersonDependentsNbr(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function DependentBenefitsAvailable(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function EELocalsCheckWorkState(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function EELocalsCheckConditionsForWorkFields(IsImport: Boolean; const Table, Field: string;
      Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
  public
  end;

implementation

uses kbmMemTable, EvExchangeConsts, EvExchangeUtils;

{ TEvExchangeEeFunctions }

procedure TEvExchangeEeFunctions.AddALD_DBDTKeys(var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray);
var
  GroupPrefix: String;
begin
  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  // division
  if GetRowValues.ValueExists(EvoXEEDivision + GroupPrefix) and (GetRowValues.Value[EvoXEEDivision + GroupPrefix] <> null) then
  begin
    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := 'EE_AUTOLABOR_DISTRIBUTION';
    KeyFields[High(KeyFields)] := 'CO_DIVISION_NBR';
    KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEEDivision + GroupPrefix];
    // Branch
    if GetRowValues.ValueExists(EvoXEEBranch + GroupPrefix) and (GetRowValues.Value[EvoXEEBranch + GroupPrefix] <> null) then
    begin
      SetLength(KeyTables, Succ(Length(KeyTables)));
      SetLength(KeyFields, Succ(Length(KeyFields)));
      SetLength(KeyValues, Succ(Length(KeyValues)));
      KeyTables[High(KeyTables)] := 'EE_AUTOLABOR_DISTRIBUTION';
      KeyFields[High(KeyFields)] := 'CO_BRANCH_NBR';
      KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEEBranch + GroupPrefix];
      // department
      if GetRowValues.ValueExists(EvoXEEDepartment + GroupPrefix) and (GetRowValues.Value[EvoXEEDepartment + GroupPrefix] <> null) then
      begin
        SetLength(KeyTables, Succ(Length(KeyTables)));
        SetLength(KeyFields, Succ(Length(KeyFields)));
        SetLength(KeyValues, Succ(Length(KeyValues)));
        KeyTables[High(KeyTables)] := 'EE_AUTOLABOR_DISTRIBUTION';
        KeyFields[High(KeyFields)] := 'CO_DEPARTMENT_NBR';
        KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEEDepartment + GroupPrefix];
        // team
        if GetRowValues.ValueExists(EvoXEETeam + GroupPrefix) and (GetRowValues.Value[EvoXEETeam + GroupPrefix] <> null) then
        begin
          SetLength(KeyTables, Succ(Length(KeyTables)));
          SetLength(KeyFields, Succ(Length(KeyFields)));
          SetLength(KeyValues, Succ(Length(KeyValues)));
          KeyTables[High(KeyTables)] := 'EE_AUTOLABOR_DISTRIBUTION';
          KeyFields[High(KeyFields)] := 'CO_TEAM_NBR';
          KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEETeam + GroupPrefix];
        end;
      end;
    end;
  end;
end;

function TEvExchangeEeFunctions.CalcAcaBenefit(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  CoNbr: variant;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE';
    OFields[0] := 'ACA_CO_BENEFIT_SUBTYPE_NBR';
    if VarToStr(Value) = '' then
    begin
      OValues[0] := null;
      exit;
    end;

    CheckCondition(GetRowValues.ValueExists(EvoXEEAcaLowestCostBenefit + GroupPrefix),
      'Field "' + EvoXEEAcaLowestCostBenefit + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXEEAcaLowestCostBenefit + GroupPrefix] <> null, 'Value of "' +
      EvoXEEAcaLowestCostBenefit + '" cannot be null, if you import into "' + GetCurrentPackageField.GetDisplayName + '"');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_BENEFITS'), 'BENEFIT_NAME=''' + VarToStr(Value) + ''' and CO_NBR=' + VarToStr(CoNbr));
    OValues[0] := GetImpEngine.IDS('CO_BENEFITS').FieldValues['CO_BENEFITS_NBR'];
    if OValues[0] = null then
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_BENEFITS" table');

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcAcaLowestCostBenefitSubtype(
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  BenefitsNbr: Variant;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE';
    OFields[0] := 'ACA_CO_BENEFIT_SUBTYPE_NBR';
    if VarToStr(Value) = '' then
    begin
      OValues[0] := null;
      exit;
    end;

    CheckCondition(GetRowValues.ValueExists(EvoXEEAcaBenefit + GroupPrefix),
      'Field "' + EvoXEEAcaBenefit + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXEEAcaBenefit + GroupPrefix] <> null, 'Value of "' +
      EvoXEEAcaBenefit + '" cannot be null, if you import into "' + GetCurrentPackageField.GetDisplayName + '"');
    BenefitsNbr := GetRowValues.Value[EvoXEEAcaBenefit + GroupPrefix];

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_BENEFIT_SUBTYPE'), 'DESCRIPTION=''' + VarToStr(Value) +
      ''' and ACA_LOWEST_COST = ''Y'' and CO_BENEFITS_NBR=' + VarToStr(BenefitsNbr));
    OValues[0] := GetImpEngine.IDS('CO_BENEFIT_SUBTYPE').FieldValues['CO_BENEFIT_SUBTYPE_NBR'];
    if OValues[0] = null then
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_BENEFIT_SUBTYPE" table for CO_BENEFITS_NBR=' + VarToStr(BenefitsNbr) + ' and marked as Lowest Cost');

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcAdditionalInfo(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoAddInfoNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_ADDITIONAL_INFO';
    OFields[0] := 'CO_ADDITIONAL_INFO_NAMES_NBR';

    CheckCondition(Value <> null, 'Additional Info - Field cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_ADDITIONAL_INFO_NAMES'), 'NAME=''' + VarToStr(Value) + ''' and CO_NBR=' + VarToStr(CoNbr));
    CoAddInfoNbr := GetImpEngine.IDS('CO_ADDITIONAL_INFO_NAMES').FieldValues['CO_ADDITIONAL_INFO_NAMES_NBR'];
    CheckCondition(CoAddInfoNbr <> null, 'Cannot find Name in CO_ADDITIONAL_INFO_NAMES table. Name is "' + VarToStr(Value) + '"');
    OValues[0] := CoAddInfoNbr;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_ADDITIONAL_INFO', 'CO_ADDITIONAL_INFO_NAMES_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcAdditionalInfoValue(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoAddInfoNbr, CoAddInfoValueNbr: Variant;
  index: integer;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    CoAddInfoNbr := null;

    index := FindIndexByName(KeyTables, KeyFields, 'EE_ADDITIONAL_INFO', 'CO_ADDITIONAL_INFO_NAMES_NBR');
    if index <> -1 then
      CoAddInfoNbr := KeyValues[index]
    else
      raise Exception.Create('CO_ADDITIONAL_INFO_NAMES_NBR is required');

    CheckCondition(CoAddInfoNbr <> null, 'CO_ADDITIONAL_INFO_NAMES_NBR cannot be null');

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_ADDITIONAL_INFO';
    OFields[0] := 'CO_ADDITIONAL_INFO_VALUES_NBR';

    CheckCondition(Value <> null, 'Additional Info - Value cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_ADDITIONAL_INFO_VALUES'), 'INFO_VALUE=''' + VarToStr(Value) +
      ''' and CO_NBR=' + VarToStr(CoNbr) + ' and CO_ADDITIONAL_INFO_NAMES_NBR=' + VarToStr(CoAddInfoNbr));
    CoAddInfoValueNbr := GetImpEngine.IDS('CO_ADDITIONAL_INFO_VALUES').FieldValues['CO_ADDITIONAL_INFO_VALUES_NBR'];
    CheckCondition(CoAddInfoValueNbr <> null, 'Cannot find Name in CO_ADDITIONAL_INFO_VALUES table. Value is "' + VarToStr(Value) + '"');
    OValues[0] := CoAddInfoValueNbr;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcALDJobs(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoJobsNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    CoJobsNbr := null;

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_AUTOLABOR_DISTRIBUTION';
    OFields[0] := 'CO_JOBS_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_JOBS'), 'DESCRIPTION=''' + VarToStr(Value) +
        ''' and CO_NBR=' + VarToStr(CoNbr));
      CoJobsNbr := GetImpEngine.IDS('CO_JOBS').FieldValues['CO_JOBS_NBR'];
      CheckCondition(CoJobsNbr <> null, 'Cannot find Description in CO_JOBS table. Value is "' + VarToStr(Value) + '"');
      OValues[0] := CoJobsNbr;

      // I wasn't able to add D/B/D/T fields to package as key fields because I don't know how many of them are mapped...
      // in other words, the list of key fields can vary...
      AddALD_DBDTKeys(KeyTables, KeyFields, KeyValues);
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcALDPercentage(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_AUTOLABOR_DISTRIBUTION';
    OFields[0] := 'PERCENTAGE';
    OValues[0] := Value;
    CheckCondition(Value <> null, 'Percentage cannot be null');

    // I wasn't able to add D/B/D/T fields to package as key fields because I don't know how many of them are mapped...
    // in other words, the list of key fields can vary...
    AddALD_DBDTKeys(KeyTables, KeyFields, KeyValues);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcALDWorkersCompensation(
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := CalcWorkersCompensation(IsImport, Table, Field, Value, KeyTables, KeyFields, KeyValues, OTables,
    OFields, OValues);
  // I wasn't able to add D/B/D/T fields to package as key fields because I don't know how many of them are mapped...
  // in other words, the list of key fields can vary...
  if Result then
  begin
    OTables[0] := 'EE_AUTOLABOR_DISTRIBUTION';
    AddALD_DBDTKeys(KeyTables, KeyFields, KeyValues);
  end;
end;

function TEvExchangeEeFunctions.CalcBenefitSubtype(IsImport: Boolean; const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  CoBenefitsNbr: integer;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_SCHEDULED_E_DS';
    OFields[0] := 'CO_BENEFIT_SUBTYPE_NBR';
    if VarToStr(Value) = '' then
    begin
      OValues[0] := null;
      exit;
    end;

    CheckCondition(GetRowValues.ValueExists(EvoXSchedEDBenefitReference + GroupPrefix),
      'Field "' + EvoXSchedEDBenefitReference + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXSchedEDBenefitReference + GroupPrefix] <> null, 'Value of "' +
      EvoXSchedEDBenefitReference + '" cannot be null, if you import into "' + GetCurrentPackageField.GetDisplayName + '"');
    CoBenefitsNbr := GetRowValues.Value[EvoXSchedEDBenefitReference + GroupPrefix];

    // checking that EE Benefit Reference is not mapped or it is null
    if GetRowValues.ValueExists(EvoXEEBenefitReference + GroupPrefix) then
      CheckCondition(GetRowValues.Value[EvoXEEBenefitReference + GroupPrefix] = null,
        'Fields "' + EvoXEEBenefitReference + '" and "' + GetCurrentPackageField.GetDisplayName + '" cannot be imported together');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_BENEFIT_SUBTYPE'), 'DESCRIPTION=''' + VarToStr(Value) +
      ''' and CO_BENEFITS_NBR=' + VarToStr(CoBenefitsNbr));
    OValues[0] := GetImpEngine.IDS('CO_BENEFIT_SUBTYPE').FieldValues['CO_BENEFIT_SUBTYPE_NBR'];
    if OValues[0] = null then
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_BENEFIT_SUBTYPE" table for CO_BENEFITS_NBR=' + VarToStr(CoBenefitsNbr));

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcBenefitSubtypeForEE(IsImport: Boolean; const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  CoBenefitsNbr: integer;
  CoBenefitsSubtype: Variant;
  EENbr: Variant;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_SCHEDULED_E_DS';
    OFields[0] := 'EE_BENEFITS_NBR';
    if VarToStr(Value) = '' then
    begin
      OValues[0] := null;
      exit;
    end;

    CheckCondition(GetRowValues.ValueExists(EvoXSchedEDBenefitReference + GroupPrefix),
      'Field "' + EvoXSchedEDBenefitReference + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXSchedEDBenefitReference + GroupPrefix] <> null, 'Value of "' +
      EvoXSchedEDBenefitReference + '" cannot be null, if you import into "' + GetCurrentPackageField.GetDisplayName + '"');
    CoBenefitsNbr := GetRowValues.Value[EvoXSchedEDBenefitReference + GroupPrefix];

    // checking that Amount Type is not mapped or it is null
    if GetRowValues.ValueExists(EvoXSchedEDBenefitAmountType + GroupPrefix) then
      CheckCondition(GetRowValues.Value[EvoXSchedEDBenefitAmountType + GroupPrefix] = null,
        'Fields "' + EvoXSchedEDBenefitAmountType + '" and "' + GetCurrentPackageField.GetDisplayName + '" cannot be imported together');

    // calculating benefits_subtype
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_BENEFIT_SUBTYPE'), 'DESCRIPTION=''' + VarToStr(Value) +
      ''' and CO_BENEFITS_NBR=' + VarToStr(CoBenefitsNbr));
    CoBenefitsSubtype := GetImpEngine.IDS('CO_BENEFIT_SUBTYPE').FieldValues['CO_BENEFIT_SUBTYPE_NBR'];
    if CoBenefitsSubtype = null then
      raise Exception.Create('Value "' + VarToStr(Value) + '" was not found in "CO_BENEFIT_SUBTYPE" table for CO_BENEFITS_NBR=' + VarToStr(CoBenefitsNbr));

    EENbr := GetEeNbr(KeyTables, KeyFields, KeyValues);

    CheckCondition(EENbr <> null, 'EE_NBR cannot be null');

    // calculatin EE_BENEFITS_NBR
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('EE_BENEFITS'), 'EE_NBR=''' + VarToStr(EENbr) +
      ''' and CO_BENEFIT_SUBTYPE_NBR=' + VarToStr(CoBenefitsSubtype));
    OValues[0] := GetImpEngine.IDS('EE_BENEFITS').FieldValues['EE_BENEFITS_NBR'];
    if OValues[0] = null then
      GetCustomWarnings.Add('Proper record was not found in "EE_BENEFITS" table for "' + VarToStr(Value) + '"');

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcClBenefitRatesNbrForEE(IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  CoBenefitsNbr: Variant;
begin
  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    Assert(false, 'Logical error. Field "' + GetCurrentPackageField.GetExchangeFieldName + '" should belongs to Group');

  CheckCondition(GetRowValues.ValueExists(EvoXBenefitReferenceForEE + GroupPrefix), 'Field "Benefit Reference' + GroupPrefix + '" should be mapped ("Employee Benefits" group of fields)');

  Result := True;
  if IsImport then
  begin
    CheckCondition(GetRowValues.ValueExists(EvoXBenefitReferenceForEE + GroupPrefix),
      'Field "' + EvoXBenefitReferenceForEE + '" should be mapped');
    CoBenefitsNbr := GetRowValues.Value[EvoXBenefitReferenceForEE + GroupPrefix];

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_BENEFITS';
    OFields[0] := 'CO_BENEFIT_SUBTYPE_NBR';

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_BENEFIT_SUBTYPE'), 'DESCRIPTION=''' + VarToStr(Value) +
      ''' and CO_BENEFITS_NBR=' + VarToStr(CoBenefitsNbr));
    OValues[0] := GetImpEngine.IDS('CO_BENEFIT_SUBTYPE').FieldValues['CO_BENEFIT_SUBTYPE_NBR'];
    CheckCondition(OValues[0] <> null, 'Value "' + VarToStr(Value) + '" was not found in "CO_BENEFIT_SUBTYPE" table for CO_BENEFITS_NBR=' + VarToStr(CoBenefitsNbr));
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcClPersonDependentsNbr(
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  LastName, ClPersonNbr, EeBenefitsNbr: Variant;
  MaxDependents: integer;
  Q: IevQuery;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_BENEFICIARY';
    OFields[0] := 'CL_PERSON_DEPENDENTS_NBR';

    if GetCurrentPackageField.GetExchangeFieldName <> (EvoXAssignDependentFirstName + GroupPrefix) then
    begin
      // CL_PERSON_DEPENDENTS_NBR has already been mapped in EvoXAssignDependentFirstName
      // so just get that value and exit
      OValues[0] := GetRowValues.Value[EvoXAssignDependentFirstName + GroupPrefix];
      Result := True;
      exit;
    end;

    ClPersonNbr := GetClPersonNbr(KeyTables, KeyFields, KeyValues);

    CheckCondition(GetRowValues.ValueExists(EvoXAssignDependentLastName + GroupPrefix),
      'Field "' + EvoXAssignDependentLastName + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXAssignDependentLastName + GroupPrefix] <> null, 'Value of "' +
      EvoXAssignDependentLastName + '" cannot be null');
    LastName := GetRowValues.Value[EvoXAssignDependentLastName + GroupPrefix];

{    CheckCondition(GetRowValues.ValueExists(EvoXEeBenefitRef + GroupPrefix),
      'Field "' + EvoXEeBenefitRef + '" should be mapped');}
    CheckCondition(Value <> null, 'Value of "' +
      GetCurrentPackageField.GetDisplayName + '" cannot be null');

    //GetCoBenefitRateNumber
    CheckCondition(GetRowValues.ValueExists(EvoXEeBenefitAmountType + GroupPrefix),
      'Field "' + EvoXEeBenefitAmountType + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXEeBenefitAmountType + GroupPrefix] <> null, 'Value of "' +
      EvoXEeBenefitAmountType + '" cannot be null');
    EeBenefitsNbr := GetRowValues.Value[EvoXEeBenefitAmountType + GroupPrefix];

    Q := TevQuery.Create('select a.Benefit_Effective_Date, a.Expiration_Date, a.Co_Benefit_Subtype_Nbr from EE_BENEFITS a ' +
      ' where {AsOfNow<a>} and a.Ee_Benefits_Nbr = :EeBenefitsNbr ');
    Q.Params.AddValue('EeBenefitsNbr', EeBenefitsNbr);
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'EE Benefit was not found in "EE_BENEFITS" table');

    MaxDependents := GetCoBenefitRateNumberMaxDependents(Q.Result['CO_BENEFIT_SUBTYPE_NBR'],
      ConvertNull(Q.Result['BENEFIT_EFFECTIVE_DATE'],Now), ConvertNull(Q.Result['EXPIRATION_DATE'], Now));

    CheckCondition(MaxDependents > 0, 'Dependent "' + VarToStr(Value) + ' ' + VarToStr(LastName) + '" is not available to be assigned. Company Benefit Rate Max Dependents should be greater zero and Start Date should be less than EE Benefit Effective Start Date');

    if GetRowValues.ValueExists(EvoXAssignDependentDOB + GroupPrefix) and (GetRowValues.Value[EvoXAssignDependentDOB + GroupPrefix] <> null) then
    begin
      Q := TevQuery.Create('select a.CL_PERSON_DEPENDENTS_NBR from CL_PERSON_DEPENDENTS a ' +
        ' where {AsOfNow<a>} and a.CL_PERSON_NBR=:ClPersonNbr and a.Person_Type = ''D'' ' +
        ' and a.FIRST_NAME = :FirstName ' +
        ' and a.LAST_NAME = :LastName ' +
        ' and a.DATE_OF_BIRTH = :DOB');
      Q.Params.AddValue('DOB', GetRowValues.Value[EvoXAssignDependentDOB + GroupPrefix]);
    end
    else begin
      Q := TevQuery.Create('select a.CL_PERSON_DEPENDENTS_NBR from CL_PERSON_DEPENDENTS a ' +
        ' where {AsOfNow<a>} and a.CL_PERSON_NBR=:ClPersonNbr and a.Person_Type = ''D'' ' +
        ' and a.FIRST_NAME = :FirstName ' +
        ' and a.LAST_NAME = :LastName');
    end;

    Q.Params.AddValue('ClPersonNbr', ClPersonNbr);
    Q.Params.AddValue('FirstName', VarToStr(Value));
    Q.Params.AddValue('LastName', VarToStr(LastName));
    Q.Execute;

    CheckCondition(Q.Result.RecordCount > 0, 'There is no available Dependent: ' + VarToStr(Value) + ' ' + VarToStr(LastName));

    OValues[0] := Q.Result['CL_PERSON_DEPENDENTS_NBR'];

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcClPiece(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  ClPieceNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_PIECE_WORK';
    OFields[0] := 'CL_PIECES_NBR';

    CheckCondition(Value <> null, 'Piece Name cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_PIECES'), 'NAME=''' + VarToStr(Value) + '''');
    ClPieceNbr := GetImpEngine.IDS('CL_PIECES').FieldValues['CL_PIECES_NBR'];
    CheckCondition(ClPieceNbr <> null, 'Cannot find Piece in CL_PIECES table. Name is "' + VarToStr(Value) + '"');
    OValues[0] := ClPieceNbr;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_PIECE_WORK', 'CL_PIECES_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcCoHrAttendanceNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_HR_ATTENDANCE';
    OFields[0] := 'CO_HR_ATTENDANCE_TYPES_NBR';

    CheckCondition(Value <> null, 'Description of attendance cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_HR_ATTENDANCE_TYPES'), 'ATTENDANCE_DESCRIPTION=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CO_HR_ATTENDANCE_TYPES').FieldValues['CO_HR_ATTENDANCE_TYPES_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find course by name in CO_HR_ATTENDANCE_TYPES table using ATTENDANCE_DESCRIPTION = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_HR_ATTENDANCE', 'CO_HR_ATTENDANCE_TYPES_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcCoHrCarNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_HR_CAR';
    OFields[0] := 'CO_HR_CAR_NBR';

    CheckCondition(Value <> null, 'VIN cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_HR_CAR'), 'VIN_NUMBER=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CO_HR_CAR').FieldValues['CO_HR_CAR_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find vehicle by VIN in CO_HR_CAR table using VIN = ' + VarToStr(Value));
    CheckCondition(GetImpEngine.IDS('CO_HR_CAR').RecordCount = 1, 'Cannot identify unique vehicle by its VIN in CO_HR_CAR table using VIN = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_HR_CAR', 'CO_HR_CAR_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcCoHrPerformanceRatingNbr(
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_HR_PERFORMANCE_RATINGS';
    OFields[0] := 'CO_HR_PERFORMANCE_RATINGS_NBR';

    CheckCondition(Value <> null, 'Description of performance rating cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_HR_PERFORMANCE_RATINGS'), 'DESCRIPTION=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CO_HR_PERFORMANCE_RATINGS').FieldValues['CO_HR_PERFORMANCE_RATINGS_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find performance rating by description in CO_HR_PERFORMANCE_RATINGS table using DESCRIPTION = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_HR_PERFORMANCE_RATINGS', 'CO_HR_PERFORMANCE_RATINGS_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcCoHrPropertyNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_HR_PROPERTY_TRACKING';
    OFields[0] := 'CO_HR_PROPERTY_NBR';

    CheckCondition(Value <> null, 'Description of property cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_HR_PROPERTY'), 'PROPERTY_DESCRIPTION=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CO_HR_PROPERTY').FieldValues['CO_HR_PROPERTY_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find property by description in CO_HR_PROPERTY table using DESCRIPTION = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_HR_PROPERTY_TRACKING', 'CO_HR_PROPERTY_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcCompanyHomeState(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
begin
  Result := True;
  GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'));
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'CO_STATES_NBR';
    if GetImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([CoNbr, Value]), [loCaseInsensitive]) then
      OValues[0] := GetImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
    begin
      OValues[0] := null;
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_STATES" table.');
    end;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CO_STATES', 'CO_STATES_NBR', OValues[0]);
    KeyValues[High(KeyValues)] := OValues[0];
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcCoShift(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoShiftNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_WORK_SHIFTS';
    OFields[0] := 'CO_SHIFTS_NBR';

    CheckCondition(Value <> null, 'Shift Name cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_SHIFTS'), 'NAME=''' + VarToStr(Value) + ''' and CO_NBR=' + VarToStr(CoNbr));
    CoShiftNbr := GetImpEngine.IDS('CO_SHIFTS').FieldValues['CO_SHIFTS_NBR'];
    CheckCondition(CoShiftNbr <> null, 'Cannot find Shift in CO_SHIFTS table. Name is "' + VarToStr(Value) + '"');
    OValues[0] := CoShiftNbr;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_WORK_SHIFTS', 'CO_SHIFTS_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcDefaultJob(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoJobsNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    CoJobsNbr := null;

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE';
    OFields[0] := 'CO_JOBS_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_JOBS'), 'DESCRIPTION=''' + VarToStr(Value) +
        ''' and CO_NBR=' + VarToStr(CoNbr));
      CheckCondition(GetImpEngine.IDS('CO_JOBS').RecordCount < 2, 'There are more than one record in CO_JOBS table with the same "Job Code": ' +
        '"' + VarToStr(Value) + '"');
      CoJobsNbr := GetImpEngine.IDS('CO_JOBS').FieldValues['CO_JOBS_NBR'];
      CheckCondition(CoJobsNbr <> null, 'Cannot find Job Code in CO_JOBS table. Value is "' + VarToStr(Value) + '"');
      OValues[0] := CoJobsNbr;
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcDisabilityNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_HR_PERSON_HANDICAPS';
    OFields[0] := 'SY_HR_HANDICAPS_NBR';

    CheckCondition(Value <> null, 'Description of disability cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SY_HR_HANDICAPS'), 'DESCRIPTION=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('SY_HR_HANDICAPS').FieldValues['SY_HR_HANDICAPS_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find disability by description in SY_HR_HANDICAPS table using DESCRIPTION = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CL_HR_PERSON_HANDICAPS', 'SY_HR_HANDICAPS_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcEeBenefitsNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  BenefitType: String;
  EeNbr: Variant;
  Q: IevQuery;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_BENEFICIARY';
    OFields[0] := 'EE_BENEFITS_NBR';

    if GetCurrentPackageField.GetExchangeFieldName <> (EvoXEeBenefitRef + GroupPrefix) then
    begin
      // EE_BENEFITS_NBR has already been mapped in EvoXEeBenefitAmountType
      // so just get that value and exit
      OValues[0] := GetRowValues.Value[EvoXEeBenefitRef + GroupPrefix];//GetRowValues.Value[EvoXEeBenefitAmountType + GroupPrefix];
      Result := True;
      exit;
    end;

    EeNbr := GetEeNbr(KeyTables, KeyFields, KeyValues);

    CheckCondition(GetRowValues.ValueExists(EvoXEeBenefitRef + GroupPrefix),
      'Field "' + EvoXEeBenefitRef + '" should be mapped');

    CheckCondition(GetRowValues.ValueExists(EvoXEeBenefitAmountType + GroupPrefix),
      'Field "' + EvoXEeBenefitAmountType + '" should be mapped');
    CheckCondition(GetRowValues.Value[EvoXEeBenefitAmountType + GroupPrefix] <> null, 'Value of "' +
      EvoXEeBenefitAmountType + '" cannot be null');

    BenefitType := GetRowValues.Value[EvoXEeBenefitAmountType + GroupPrefix];

{    CheckCondition(GetRowValues.ValueExists(EvoXEeBenefitRef + GroupPrefix),
      'Field "' + EvoXEeBenefitRef + '" should be mapped');}
    CheckCondition(Value <> null, 'Value of "' +
      GetCurrentPackageField.GetDisplayName + '" cannot be null');

    if GetRowValues.ValueExists(EvoXEeBenefitStartDate + GroupPrefix) and (GetRowValues.Value[EvoXEeBenefitStartDate + GroupPrefix] <> null) then
    begin
      Q := TevQuery.Create('select a.EE_BENEFITS_NBR from EE_BENEFITS a ' +
        ' join Co_Benefit_Subtype st on a.Co_Benefit_Subtype_Nbr = st.Co_Benefit_Subtype_Nbr ' +
        ' join Co_Benefits b on a.Co_Benefits_Nbr = b.Co_Benefits_Nbr ' +
        ' where {AsOfNow<a>} and {AsOfNow<st>} and {AsOfNow<b>} and st.DESCRIPTION=:description ' +
        ' and a.Ee_Nbr = :EeNbr ' +
        ' and a.Benefit_Effective_Date = :BenefitStartDate ' +
        ' and b.BENEFIT_NAME = :BenefitName');
      Q.Params.AddValue('BenefitStartDate', GetRowValues.Value[EvoXEeBenefitStartDate + GroupPrefix]);
    end
    else begin
      Q := TevQuery.Create('select a.EE_BENEFITS_NBR from EE_BENEFITS a ' +
        ' join Co_Benefit_Subtype st on a.Co_Benefit_Subtype_Nbr = st.Co_Benefit_Subtype_Nbr ' +
        ' join Co_Benefits b on a.Co_Benefits_Nbr = b.Co_Benefits_Nbr ' +
        ' where {AsOfNow<a>} and {AsOfNow<st>} and {AsOfNow<b>} and st.DESCRIPTION=:description ' +
        ' and a.Ee_Nbr = :EeNbr ' +
        ' and b.BENEFIT_NAME = :BenefitName');
    end;

    Q.Params.AddValue('EeNbr', EeNbr);
    Q.Params.AddValue('description', BenefitType);
    Q.Params.AddValue('BenefitName', VarToStr(Value));
    Q.Execute;

    if Q.Result.RecordCount = 0 then
      GetCustomWarnings.Add('EE Benefit was not found in "EE_BENEFITS" table');
{    if Q.Result.RecordCount > 1 then
      GetCustomWarnings.Add('Not unique pair of Aca Lowest Cost Benefit and Aca Benefit found in "CO_BENEFIT_SUBTYPE" table');}
    OValues[0] := Q.Result['EE_BENEFITS_NBR'];

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcEECourseNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_HR_CO_PROVIDED_EDUCATN';
    OFields[0] := 'CL_HR_COURSE_NBR';

    CheckCondition(Value <> null, 'Name of course cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_HR_COURSE'), 'NAME=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CL_HR_COURSE').FieldValues['CL_HR_COURSE_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find course by name in CL_HR_COURSE table using NAME = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_HR_CO_PROVIDED_EDUCATN', 'CL_HR_COURSE_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcEELocalsNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  SyLocalsNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_LOCALS';
    OFields[0] := 'CO_LOCAL_TAX_NBR';

    CheckCondition(Value <> null, 'Name of local tax cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SY_LOCALS'), 'NAME=''' + VarToStr(Value) + '''');
    SyLocalsNbr := GetImpEngine.IDS('SY_LOCALS').FieldValues['SY_LOCALS_NBR'];
    CheckCondition(SyLocalsNbr <> null, 'Cannot find tax by its name in SY_LOCALS table. Name is "' + VarToStr(Value) + '"');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_LOCAL_TAX'), 'CO_NBR = '''+ VarToStr(CoNbr)+'''');
    OValues[0] := GetImpEngine.IDS('CO_LOCAL_TAX').Lookup('SY_LOCALS_NBR', SyLocalsNbr, 'CO_LOCAL_TAX_NBR');
    CheckCondition(OValues[0] <> null, 'Cannot find local tax in CO_LOCAL_TAX table using SY_LOCALS_NBR = ' + VarToStr(SyLocalsNbr));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_LOCALS', 'CO_LOCAL_TAX_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcEESchoolNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_HR_CO_PROVIDED_EDUCATN';
    OFields[0] := 'CL_HR_SCHOOL_NBR';

    CheckCondition(Value <> null, 'Name of school cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_HR_SCHOOL'), 'NAME=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CL_HR_SCHOOL').FieldValues['CL_HR_SCHOOL_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find school by name in CL_HR_SCHOOL table using NAME = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_HR_CO_PROVIDED_EDUCATN', 'CL_HR_SCHOOL_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcMultipleScheduledEDGroup(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_SCHEDULED_E_DS';
    OFields[0] := 'SCHEDULED_E_D_GROUPS_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_E_D_GROUPS'), 'NAME=''' + VarToStr(Value) + '''');
      if GetImpEngine.IDS('CL_E_D_GROUPS').RecordCount > 0 then
        OValues[0] := GetImpEngine.IDS('CL_E_D_GROUPS').FieldValues['CL_E_D_GROUPS_NBR']
      else
      begin
        OValues[0] := null;
        GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CL_E_D_GROUPS" table.');
      end;
    end;

    if OValues[0] <> null then
      CheckCondition(GetRowValues.ValueExists(EvoXPriorityField) and (GetRowValues.Value[EvoXPriorityField] <> null),
        'Field "' + EvoXPriorityField + '" is mandatory when "Multiple Scheduled E/D Group" is imported');

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcPositionGrade(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  PositionNbr: Variant;
  Q: IevQuery;
begin
  Result := false;

  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    GroupPrefix := '';

  if IsImport then
  begin
    CheckCondition(GetRowValues.ValueExists(EvoXPayGradePositiion + GroupPrefix),
      'Field "' + EvoXPayGradePositiion + '" should be mapped');
    PositionNbr := GetRowValues.Value[EvoXPayGradePositiion + GroupPrefix];
    CheckCondition(PositionNbr <> null, '"' + EvoXPayGradePositiion + '" field must have a value');

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_RATES';
    OFields[0] := 'CO_HR_POSITION_GRADES_NBR';

    Q := TevQuery.Create('select CO_HR_POSITION_GRADES_NBR from CO_HR_POSITION_GRADES a ' +
      ' where {AsOfNow<a>} and a.CO_HR_SALARY_GRADES_NBR in (select CO_HR_SALARY_GRADES_NBR from CO_HR_SALARY_GRADES b ' +
      ' where {AsOfNow<b>} and b.DESCRIPTION=:grade_description) ' +
      ' and a.CO_HR_POSITIONS_NBR = :position_nbr');
    Q.Params.AddValue('grade_description', VarToStr(Value));
    Q.Params.AddValue('position_nbr', PositionNbr);
    Q.Execute;

    if Q.Result.RecordCount = 0 then
      GetCustomWarnings.Add('Pair of Grade and Position were not found in "CO_HR_POSITION_GRADES" table');
    if Q.Result.RecordCount > 1 then
      GetCustomWarnings.Add('Not unique pair of Grade and Position found in "CO_HR_POSITION_GRADES" table');
    OValues[0] := Q.Result['CO_HR_POSITION_GRADES_NBR'];

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcScheduledEDsNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  ClEDNbr, CoEdCodesNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_SCHEDULED_E_DS';
    OFields[0] := 'CL_E_DS_NBR';

    CheckCondition(Value <> null, 'E/D code cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_E_DS'), 'CUSTOM_E_D_CODE_NUMBER=''' + VarToStr(Value) + '''');
    ClEDNbr := GetImpEngine.IDS('CL_E_DS').FieldValues['CL_E_DS_NBR'];
    CheckCondition(ClEDNbr <> null, 'Cannot find ED by its code in CL_E_DS table. Code is "' + VarToStr(Value) + '"');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_E_D_CODES'), 'CO_NBR = '''+ VarToStr(CoNbr)+'''');
    CoEdCodesNbr := GetImpEngine.IDS('CO_E_D_CODES').Lookup('CL_E_DS_NBR', ClEDNbr, 'CO_E_D_CODES_NBR');
    CheckCondition(CoEdCodesNbr <> null, 'This ED code not set up for company. Code is "' + VarToStr(Value) + '"');
    OValues[0] := ClEDNbr;

    if (GetImpEngine.IDS('CL_E_DS').FieldValues['E_D_CODE_TYPE'] <> null)
       and (GetImpEngine.IDS('CL_E_DS').FieldValues['E_D_CODE_TYPE'] = 'DR') then  // see #73746
      CheckCondition(GetRowValues.ValueExists(EvoXGarnishmentIDPackageField) and (GetRowValues.Value[EvoXGarnishmentIDPackageField] <> null),
        'Field "' + EvoXGarnishmentIDPackageField + '" is mandatory when "E/D Code Type" = ''DR''');

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_SCHEDULED_E_DS', 'CL_E_DS_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcSchoolNbr(IsImport: Boolean; const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_HR_PERSON_EDUCATION';
    OFields[0] := 'CL_HR_SCHOOL_NBR';

    CheckCondition(Value <> null, 'Name of school cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_HR_SCHOOL'), 'NAME=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CL_HR_SCHOOL').FieldValues['CL_HR_SCHOOL_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find school by name in CL_HR_SCHOOL table using NAME = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CL_HR_PERSON_EDUCATION', 'CL_HR_SCHOOL_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcSkillNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_HR_PERSON_SKILLS';
    OFields[0] := 'CL_HR_SKILLS_NBR';

    CheckCondition(Value <> null, 'Description of skill cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_HR_SKILLS'), 'DESCRIPTION=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CL_HR_SKILLS').FieldValues['CL_HR_SKILLS_NBR'];
    CheckCondition(OValues[0] <> null, 'Cannot find skill by description in CL_HR_SKILLS table using DESCRIPTION = ' + VarToStr(Value));

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CL_HR_PERSON_SKILLS', 'CL_HR_SKILLS_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcTOAType(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoTOANbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_TIME_OFF_ACCRUAL';
    OFields[0] := 'CO_TIME_OFF_ACCRUAL_NBR';

    CheckCondition(Value <> null, 'Type cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_TIME_OFF_ACCRUAL'), 'DESCRIPTION=''' + VarToStr(Value) + ''' and CO_NBR=' + VarToStr(CoNbr));
    CoTOANbr := GetImpEngine.IDS('CO_TIME_OFF_ACCRUAL').FieldValues['CO_TIME_OFF_ACCRUAL_NBR'];
    CheckCondition(CoTOANbr <> null, 'Cannot find TOA in CO_TIME_OFF_ACCRUAL table. Description is "' + VarToStr(Value) + '"');
    OValues[0] := CoTOANbr;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'EE_TIME_OFF_ACCRUAL', 'CO_TIME_OFF_ACCRUAL_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcWCCode(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoNbr, EENbr: Variant;
  HomeEEStatesNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    EENbr := GetEeNbr(KeyTables, KeyFields, KeyValues);

    CheckCondition(EENbr <> null, 'EE_NBR cannot be null');

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_RATES';
    OFields[0] := 'CO_WORKERS_COMP_NBR';

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_WORKERS_COMP'),
        'WORKERS_COMP_CODE=''' + VarToStr(Value) + ''' and CO_NBR=' + VarToStr(CoNbr));
      if GetImpEngine.IDS('CO_WORKERS_COMP').RecordCount > 0 then
      begin
        if GetImpEngine.IDS('CO_WORKERS_COMP').RecordCount > 1 then
        begin
          // looking at EE.Home_Tax_EE_States_Nbr to make a choise
          HomeEEStatesNbr := GetImpEngine.IDS('EE').FieldValues['HOME_TAX_EE_STATES_NBR'];
          if HomeEEStatesNbr = null then
          begin
            GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'), 'CO_NBR=' + VarToStr(CoNbr));
            CheckCondition(GetImpEngine.IDS('CO_STATES').RecordCount > 0, 'No set up of Company States');
            if GetRowValues.ValueExists(EEHomeState) then
            begin
              CheckCondition(GetImpEngine.IDS('CO_STATES').Locate('STATE', VarArrayOf([GetRowValues.Value[EEHomeState]]), [loCaseInsensitive]),
                'Cannot find mapped EE State in CO_STATES table. Value is "' + VarToStr(GetRowValues.Value[EEHomeState]) + '"');
              HomeEEStatesNbr := GetImpEngine.IDS('CO_STATES').FieldValues['CO_STATES_NBR'];
            end
            else
            begin
              GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO'), 'CO_NBR=' + VarToStr(CoNbr));
              HomeEEStatesNbr := GetImpEngine.IDS('CO').FieldValues['HOME_CO_STATES_NBR'];
            end;
          end
          else
          begin
            GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('EE_STATES'), 'EE_STATES_NBR=' + VarToStr(HomeEEStatesNbr));
            HomeEEStatesNbr := GetImpEngine.IDS('EE_STATES').FieldValues['CO_STATES_NBR'];
          end;
          OValues[0] := GetImpEngine.IDS('CO_WORKERS_COMP').Lookup('CO_STATES_NBR', HomeEEStatesNbr, 'CO_WORKERS_COMP_NBR');
          CheckCondition(OValues[0] <> null, 'WC Code not set up for the employee''s Tax Home State. WC Code is "' +
            VarToStr(Value) + '"');
        end
        else
          OValues[0] := GetImpEngine.IDS('CO_WORKERS_COMP').FieldValues['CO_WORKERS_COMP_NBR'];
      end
      else
      begin
        OValues[0] := null;
        GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_WORKERS_COMP" table.');
      end;
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.CalcWorkersCompensation(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr, EENbr, HomeEEStatesNbr, HomeStateNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    EeNbr := GetEeNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE';
    OFields[0] := 'CO_WORKERS_COMP_NBR';

    CheckCondition(Value <> null, 'WC Code cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_WORKERS_COMP'), 'CO_NBR=' + VarToStr(CoNbr) + ' and WORKERS_COMP_CODE=''' + VarToStr(Value) + '''');
    CheckCondition(GetImpEngine.IDS('CO_WORKERS_COMP').RecordCount > 0, 'WC Code not found in CO_WORKERS_COMP table. Code: ' + VarToStr(Value));
    if GetImpEngine.IDS('CO_WORKERS_COMP').RecordCount = 1 then
      // only one such code exists
      OValues[0] := GetImpEngine.IDS('CO_WORKERS_COMP').FieldValues['CO_WORKERS_COMP_NBR']
    else
    begin
      // many codes exists
      CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
      if GetRowValues.ValueExists(EEHomeState) then
      begin
        // home state is mapped, so will use it
        // because Home State field is after EE Default WC Code field in package, it's value is not mapped yet...
        GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'), 'CO_NBR=' + VarToStr(CoNbr));
        CheckCondition(GetImpEngine.IDS('CO_STATES').RecordCount > 0, 'No set up of Company States');
        CheckCondition(GetImpEngine.IDS('CO_STATES').Locate('STATE', VarArrayOf([GetRowValues.Value[EEHomeState]]), [loCaseInsensitive]),
          'Cannot find mapped EE Home State in CO_STATES table. Value is "' + VarToStr(GetRowValues.Value[EEHomeState]) + '"');
        HomeStateNbr := GetImpEngine.IDS('CO_STATES').FieldValues['CO_STATES_NBR'];

        OValues[0] := GetImpEngine.IDS('CO_WORKERS_COMP').Lookup('CO_STATES_NBR', HomeStateNbr, 'CO_WORKERS_COMP_NBR');
        CheckCondition(OValues[0] <> null, 'WC Code not set up for the mapped EE Home State. WC Code is "' +
          VarToStr(Value) + '"' + '. EE Home State is ' + VarToStr(GetRowValues.Value[EEHomeState]));
      end
      else
      begin
        // looking for Home State
        if EENbr <> null then
        begin
          HomeEEStatesNbr := GetImpEngine.IDS('EE').FieldValues['HOME_TAX_EE_STATES_NBR'];
          CheckCondition(HomeEEStatesNbr <> null, 'Default home tax state not set up. Cannot convert W/C code');
          GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('EE_STATES'), 'EE_STATES_NBR = ' + VarToStr(HomeEEStatesNbr)
            + ' and EE_NBR=' + VarToStr(EENbr));
          CheckCondition(GetImpEngine.IDS('EE_STATES').RecordCount > 0, 'Cannot find state in EE_STATES. EE_NBR='
            + VarToStr(EENbr) + '. HOME_TAX_EE_STATES_NBR=' + VarToStr(HomeEEStatesNbr));
          HomeStateNbr := GetImpEngine.IDS('EE_STATES').FieldValues['CO_STATES_NBR'];
          OValues[0] := GetImpEngine.IDS('CO_WORKERS_COMP').Lookup('CO_STATES_NBR', HomeStateNbr, 'CO_WORKERS_COMP_NBR');
          CheckCondition(OValues[0] <> null, 'WC Code not set up for the current HOME_TAX_EE_STATES_NBR. WC Code is "' +
            VarToStr(Value) + '"' + '. HOME_TAX_EE_STATES_NBR is ' + VarToStr(HomeStateNbr));
        end
        else
        begin
          // new employee, use Company Home state, this is required field for new employee so it must be mapped
          CheckCondition(GetRowValues.ValueExists(CompanyHomeStatePackageField), '"EE State" has to be mapped to WC Code calculation for new employee');

          GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'), 'CO_NBR=' + VarToStr(CoNbr));
          CheckCondition(GetImpEngine.IDS('CO_STATES').RecordCount > 0, 'No set up of Company States');
          CheckCondition(GetImpEngine.IDS('CO_STATES').Locate('STATE', VarArrayOf([GetRowValues.Value[CompanyHomeStatePackageField]]), [loCaseInsensitive]),
            'Cannot find mapped EE State in CO_STATES table. Value is "' + VarToStr(GetRowValues.Value[CompanyHomeStatePackageField]) + '"');
          HomeStateNbr := GetImpEngine.IDS('CO_STATES').FieldValues['CO_STATES_NBR'];

          OValues[0] := GetImpEngine.IDS('CO_WORKERS_COMP').Lookup('CO_STATES_NBR', HomeStateNbr, 'CO_WORKERS_COMP_NBR');
          CheckCondition(OValues[0] <> null, 'WC Code not set up for the mapped EE State. WC Code is "' +
            VarToStr(Value) + '"' + '. EE State is ' + VarToStr(GetRowValues.Value[CompanyHomeStatePackageField]));
        end;
      end;
    end;

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.DependentBenefitsAvailable(IsImport: Boolean; const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
begin
  Result := false;
  if IsImport then
  begin
    CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
    CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

    if  GetCurrentPackageField.IsBelongsToGroup then
      GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
    else
      GroupPrefix := '';

    if VarToStr(Value) = GROUP_BOX_YES then
    begin
      CheckCondition(GetRowValues.ValueExists(EvoXDepBenefAvailableDate + GroupPrefix), 'Field "' + EvoXDepBenefAvailableDate + GroupPrefix + '" should be mapped and have value');
      CheckCOndition(GetRowValues.Value[EvoXDepBenefAvailableDate + GroupPrefix] <> null, 'Field "' + EvoXDepBenefAvailableDate + GroupPrefix + '" should have value');
    end;

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE';
    OFields[0] := 'DEPENDENT_BENEFITS_AVAILABLE';
    OValues[0] := Value;

    Result := True;
  end
  else
    Assert(false, 'It is not export function');
end;

procedure TEvExchangeEeFunctions.DoEELocalsCheckConditionsForWorkFields(const ADisplayFieldName: String; const ACoLocalTaxNbr: integer);
var
  iSysLocalsNbr: integer;
  Q: IevQuery;
begin
  GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_LOCAL_TAX'));
  CheckCondition(GetImpEngine.IDS('CO_LOCAL_TAX').Locate('CO_LOCAL_TAX_NBR', ACoLocalTaxNbr, []),
    'Cannot find record in CO_LOCAL_TAX table. CO_LOCAL_TAX_NBR=' + IntToStr(ACoLocalTaxNbr));
  iSysLocalsNbr := GetImpEngine.IDS('CO_LOCAL_TAX')['SY_LOCALS_NBR'];
  Q := TevQuery.Create('select SY_LOCALS_NBR, LOCAL_TYPE, a.SY_STATES_NBR, STATE from SY_LOCALS a, SY_STATES b ' +
    ' where SY_LOCALS_NBR=:p1 and {AsOfNow<a>} and {AsOfNow<b>} and b.SY_STATES_NBR=a.SY_STATES_NBR');
  Q.Params.AddValue('p1', iSysLocalsNbr);
  Q.Execute;
  CheckCondition(Q.Result.RecordCount = 1, 'Cannot find record in SY_LOCALS and SY_STATES');
  CheckCondition(Q.Result['LOCAL_TYPE'] = LOCAL_TYPE_INC_NON_RESIDENTIAL,
    'Field "' + ADisplayFieldName + '" could be imported, if LOCAL_TYPE = "' + LOCAL_TYPE_INC_NON_RESIDENTIAL +'"');
  CheckCondition(Q.Result['STATE'] = 'PA',
    'Field "' + ADisplayFieldName + '" could be imported, if state is "PA"');
end;

function TEvExchangeEeFunctions.EELocalsCheckConditionsForWorkFields(IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
  CurrentField: IEvExchangePackageField;
  CoLocalTaxNbr: Variant;
begin
  Result := False;
  if IsImport then
  begin
    CurrentField := GetCurrentPackageField;
    CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
    CheckCondition(CurrentField <> nil, 'Internal error: Pointer to current package field is not assigned');

    if  GetCurrentPackageField.IsBelongsToGroup then
      GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
    else
      GroupPrefix := '';

    if Value <> null then
    begin
      CheckCondition(GetRowValues.ValueExists(EvoXEELocal + GroupPrefix),
        'Field "' + EvoXEELocal + '" should be mapped');
      CoLocalTaxNbr := GetRowValues.Value[EvoXEELocal + GroupPrefix];
      CheckCondition(CoLocalTaxNbr <> null, 'Field "' + EvoXEELocal + '" cannot be NULL');
      DoEELocalsCheckConditionsForWorkFields(CurrentField.GetDisplayName, CoLocalTaxNbr);
    end;  

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_LOCALS';
    OFields[0] := CurrentField.GetEvFieldName;
    OValues[0] := Value;

    Result := True;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.EELocalsCheckWorkState(IsImport: Boolean;  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  CoNbr: Variant;
  CoLocalTaxNbr: Variant;
  GroupPrefix: String;
begin
  Result := false;

  GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'));

  if IsImport then
  begin
    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_LOCALS';
    OFields[0] := 'WORK_STATE';

    if GetImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([CoNbr, Value]), [loCaseInsensitive]) then
    begin
      OValues[0] := GetImpEngine.IDS('CO_STATES')['STATE'];
      if  GetCurrentPackageField.IsBelongsToGroup then
        GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
      else
        GroupPrefix := '';
      CheckCondition(GetRowValues.ValueExists(EvoXEELocal + GroupPrefix),
        'Field "' + EvoXEELocal + '" should be mapped');
      CoLocalTaxNbr := GetRowValues.Value[EvoXEELocal + GroupPrefix];
      CheckCondition(CoLocalTaxNbr <> null, 'Field "' + EvoXEELocal + '" cannot be NULL');
      DoEELocalsCheckConditionsForWorkFields(GetCurrentPackageField.GetDisplayName, CoLocalTaxNbr);
    end
    else
    begin
      OValues[0] := null;
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_STATES" table.');
    end;

    Result := True;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.GenerateKeyForDirectDeposit(
  IsImport: Boolean; const Table, Field: string; Value: Variant;
  var KeyTables, KeyFields: TStringArray; var KeyValues: TVariantArray;
  out OTables, OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  GroupPrefix: String;
begin
  CheckCondition(GetRowValues <> nil, 'Internal error: Pointer to row''s data not assigned');
  CheckCondition(GetCurrentPackageField <> nil, 'Internal error: Pointer to current package field is not assigned');

  if  GetCurrentPackageField.IsBelongsToGroup then
    GroupPrefix := ' ' + RecordNumberToStr(GetCurrentPackageField.GetRecordNumber)
  else
    Assert(false, 'Logical error. Field "' + GetCurrentPackageField.GetExchangeFieldName + '" should belongs to Group');

  CheckCondition(GetRowValues.ValueExists(EvoXEEDDAbaNumber + GroupPrefix), 'Field "ABA Number' + GroupPrefix + '" should be mapped');
  CheckCondition(GetRowValues.ValueExists(EvoXEEDDBankAccountName + GroupPrefix), 'Field "Bank Account Name' + GroupPrefix + '" should be mapped');
  CheckCondition(GetRowValues.ValueExists(EvoXEEDDAccountType + GroupPrefix), 'Field "Account Type' + GroupPrefix + '" should be mapped');

  Result := True;
  if IsImport then
  begin

    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := 'EE_DIRECT_DEPOSIT';
    KeyFields[High(KeyFields)] := 'EE_ABA_NUMBER';
    KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEEDDAbaNumber + GroupPrefix];

    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := 'EE_DIRECT_DEPOSIT';
    KeyFields[High(KeyFields)] := 'EE_BANK_ACCOUNT_NUMBER';
    KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEEDDBankAccountName + GroupPrefix];

    SetLength(KeyTables, Succ(Length(KeyTables)));
    SetLength(KeyFields, Succ(Length(KeyFields)));
    SetLength(KeyValues, Succ(Length(KeyValues)));
    KeyTables[High(KeyTables)] := 'EE_DIRECT_DEPOSIT';
    KeyFields[High(KeyFields)] := 'EE_BANK_ACCOUNT_TYPE';
    KeyValues[High(KeyValues)] := GetRowValues.Value[EvoXEEDDAccountType + GroupPrefix];

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_DIRECT_DEPOSIT';
    OValues[0] := Value;

    if GetCurrentPackageField.GetExchangeFieldName = EvoXEEDDAbaNumber + GroupPrefix then
    begin
      OFields[0] := 'EE_ABA_NUMBER';
      ValidateABANumber(OValues[0]);
    end
    else if GetCurrentPackageField.GetExchangeFieldName = EvoXEEDDBankAccountName + GroupPrefix then
      OFields[0] := 'EE_BANK_ACCOUNT_NUMBER'
    else if GetCurrentPackageField.GetExchangeFieldName = EvoXEEDDAccountType + GroupPrefix then
      OFields[0] := 'EE_BANK_ACCOUNT_TYPE'
    else
      Assert(false, 'GenerateKeyForDirectDeposit function is called for wrong field: ' + GetCurrentPackageField.GetExchangeFieldName);
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.HomeSDI(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := True;
  GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'));
  if IsImport then
  begin
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO'), KeyFields[0] + '=''' + KeyValues[0] + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'SDI_APPLY_CO_STATES_NBR';
    if GetImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([GetImpEngine.IDS('CO')['CO_NBR'], Value]), [loCaseInsensitive]) then
      OValues[0] := GetImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
    begin
      OValues[0] := Null;
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_STATES" table.');
    end;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CO_STATES', 'CO_STATES_NBR', OValues[0]);
    KeyValues[High(KeyValues)] := OValues[0];
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.HomeState(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
  CoStatesNbr, EENbr, CoNbr: Variant;
  LogRecord: IevExchangeChangeRecord;
  AdditionalParam: IEMapValue;
begin
  // this function is called when import for EE and EE_STATES tables is done (for current row of imput file)
  Result := True;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE';
    OFields[0] := 'HOME_TAX_EE_STATES_NBR';

    CoNbr := GetCoNbr(KeyTables, KeyFields, KeyValues);
    EeNbr := GetEeNbr(KeyTables, KeyFields, KeyValues);

    CheckCondition(EeNbr <> null, 'EE_NBR cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'));
    if GetImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([CoNbr, Value]), [loCaseInsensitive]) then
      CoStatesNbr := GetImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
      raise Exception.Create('State "' + Value + '" not found in CO_STATES');

    if Value = null then
      OValues[0] := null
    else
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('EE_STATES'), 'EE_NBR=' + VarToStr(EENbr));
      if GetImpEngine.IDS('EE_STATES').Locate('CO_STATES_NBR', CoStatesnbr, [loCaseInsensitive]) then
        OValues[0] := GetImpEngine.IDS('EE_STATES').FieldValues['EE_STATES_NBR']
      else
      begin
        OValues[0] := null;
        LogRecord := FindRecordInChangeLog('I', ctx_DataAccess.ClientID, 'EE', EENbr);
        if not Assigned(LogRecord) then
          GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "EE_STATES" table.')  // it's not a new employee
        else
        begin
          // this is new employee
          AdditionalParam := TIEMapValue.Create;
          AdditionalParam.Value := CoStatesNbr;
          LogRecord.GetAdditionalParams.AddValue('CO_STATES_NBR', AdditionalParam);
        end;
      end;
    end;
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.HomeSUI(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := True;
  GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'));
  if IsImport then
  begin
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO'), KeyFields[0] + '=''' + KeyValues[0] + '''');
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'SUI_APPLY_CO_STATES_NBR';
    if GetImpEngine.IDS('CO_STATES').Locate('CO_NBR;STATE', VarArrayOf([GetImpEngine.IDS('CO')['CO_NBR'], Value]), [loCaseInsensitive]) then
      OValues[0] := GetImpEngine.IDS('CO_STATES')['CO_STATES_NBR']
    else
    begin
      OValues[0] := null;
      GetCustomWarnings.Add('Value "' + VarToStr(Value) + '" was not found in "CO_STATES" table.');
    end;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CO_STATES', 'CO_STATES_NBR', OValues[0]);
    KeyValues[High(KeyValues)] := OValues[0];
  end
  else
    Assert(false, 'It is not export function');
end;

function TEvExchangeEeFunctions.ValidateMaritalStatus(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  index: Integer;
  StateNbr: Variant;
begin
  Result := false;
  if IsImport then
  begin
    StateNbr := Null;

    index := FindIndexByName(KeyTables, KeyFields, 'CO_STATES', 'CO_STATES_NBR');
    if index <> -1 then
      StateNbr := KeyValues[index];

    CheckCondition(not VarIsNull(StateNbr), 'Value for "EE State" package field is not setup on Company level');

    SetLength(OTables, 2);
    SetLength(OFields, 2);
    SetLength(OValues, 2);
    OTables[0] := 'EE_STATES';
    OFields[0] := 'STATE_MARITAL_STATUS';
    OValues[0] := Value;
    CheckCondition(VarToStr(OValues[0]) <> '', 'Value of "' + GetCurrentPackageField.GetDisplayName + '" cannot be empty');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CO_STATES'), 'CO_STATES_NBR = '''+ VarToStr(StateNbr)+'''');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SY_STATES'), 'STATE = '''+ GetImpEngine.IDS('CO_STATES').FieldByName('STATE').AsString + '''');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + GetImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
    OTables[1] := 'EE_STATES';
    OFields[1] := 'SY_STATE_MARITAL_STATUS_NBR';
    OValues[1] := GetImpEngine.IDS('SY_STATE_MARITAL_STATUS').Lookup('STATUS_TYPE', OValues[0], 'SY_STATE_MARITAL_STATUS_NBR');

    CheckCondition(OValues[1] <> null, 'Marital status "' + VarToStr(OValues[0]) + '" not found in SY_STATE_MARITAL_STATUS table for "' +
      GetImpEngine.IDS('CO_STATES').FieldByName('STATE').AsString + '" state');

    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

end.
