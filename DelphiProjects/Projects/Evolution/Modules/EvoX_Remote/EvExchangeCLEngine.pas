unit EvExchangeCLEngine;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvUtils,
     EvConsts, EvStreamUtils, EvTypes, isLogFile, EvContext, isBasicUtils, Variants, DB,
     IEMap, EvExchangeEngine,  EvExchangeDefaultsCalculator,
     EvExchangeResultLog, isThreadManager, EvClientDataSet,EvExchangeConsts, EvExchangeClFunction;

type

  TEvExchangeCLEngine = class(TEvExchangeEngine, IevExchangeEngine)
  private
    FKeyFieldsList: IisStringList;
    FNewCLNbr : integer;
    FNewCLCode,FCLName : string;
    FCLReadOnlyMode : shortstring;
    FIsNewCL : Boolean;
    procedure UpdateAddressBillingAndDeliveryMethod;
  protected
    procedure DoOnConstruction; override;
    function  FormatErrorMessageForRow(const AErrorMessage: String): String; override;
    procedure InitListOfKeyFieldsWithCalculatedDefaults; override;
    function  GetInputDataSortIndexFieldNames: String; override;
    function  GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO; override;

    function  ComputeCalculatedFields(const ADataSet: TevClientDataSet): boolean; override;
    procedure FillDataSetWithDefaultValues(const ADataSet: TevClientDataSet; const AStage: TEvEchangeDefaultsStages); override;
    procedure CalculateDefaultsForKeyFields(const ARowValues: IisListOfValues); override;
    procedure AddAdditionalParamsToLog(const ALogRecord: IevExchangeChangeRecord; const APostedParams: IisListOfValues); override;
    procedure BeforeImport; override;
    procedure AfterImport; override;
    procedure DoImport; override;
  end;

implementation

{ TEvExchangeCLEngine }
uses evDataSet, SDataStructure,IsTypes,EvExchangeUtils;

procedure TEvExchangeCLEngine.AddAdditionalParamsToLog(
  const ALogRecord: IevExchangeChangeRecord;
  const APostedParams: IisListOfValues);
begin
  inherited;

end;

procedure TEvExchangeCLEngine.AfterImport;
begin
  inherited;
end;

procedure TEvExchangeCLEngine.BeforeImport;
begin
end;

procedure TEvExchangeCLEngine.CalculateDefaultsForKeyFields(
  const ARowValues: IisListOfValues);
var
  Value: Variant;
  PackageField: IEvExchangePackageField;
  GroupRecordNumbers: TisDynIntegerArray;
  i: integer;
begin
  // those field required tag <calcDefault>True</calcDefault>
  if AnsiSameText(Fpackage.GetName, EvoXBasicClientPackage) then
  begin
    // Cl Delivery Group Stuff - set default value
    PackageField := FPackage.GetFieldByName(CLDeliveryStuffPackField);
    GroupRecordNumbers := FPackage.GetGroupRecordNumbers(PackageField.GetGroupName);
    for i := Low(GroupRecordNumbers) to High(GroupRecordNumbers) do
    begin
      if ARowValues.ValueExists(CLDeliveryStuffPackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])) then
      begin
        Value:= ARowValues.Value[CLDeliveryStuffPackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])];
        if (VarToStr(Value) = '') then
           ARowValues.AddValue(CLDeliveryStuffPackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i]), 'Y');
      end;
    end;
    // Cl Billing Name
    PackageField := FPackage.GetFieldByName(CLBillingNamePackField);
    GroupRecordNumbers := FPackage.GetGroupRecordNumbers(PackageField.GetGroupName);
    for i := Low(GroupRecordNumbers) to High(GroupRecordNumbers) do
    begin
      if ARowValues.ValueExists(CLBillingNamePackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])) then
      begin
        Value:= ARowValues.Value[CLBillingNamePackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])];
        if (VarToStr(Value) = '') then
           ARowValues.AddValue(CLBillingNamePackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i]), FCLName);
      end;
    end;

    // Delivery Method Destination Name
    PackageField := FPackage.GetFieldByName(CLDeliveryMethodDestinationNamePackField);
    GroupRecordNumbers := FPackage.GetGroupRecordNumbers(PackageField.GetGroupName);
    for i := Low(GroupRecordNumbers) to High(GroupRecordNumbers) do
    begin
      if ARowValues.ValueExists(CLDeliveryMethodDestinationNamePackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])) then
      begin
        Value:= ARowValues.Value[CLDeliveryMethodDestinationNamePackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])];
        if (VarToStr(Value) = '') then
           ARowValues.AddValue(CLDeliveryMethodDestinationNamePackField + ' ' + RecordNumberToStr(GroupRecordNumbers[i]), FCLName);
      end;
    end;

    // Cl Filler 41,1 - Use State Reciprocate Laws on W2
    PackageField := FPackage.GetFieldByName(CLFiller_411PackField);
    if ARowValues.ValueExists(CLFiller_411PackField) then
    begin
      Value:= ARowValues.Value[CLFiller_411PackField];
      if (VarToStr(Value) = '') then
        ARowValues.AddValue(CLFiller_411PackField, 'N');
    end;

    //Automatic Save
    PackageField := FPackage.GetFieldByName(CLAutomaticSavePackField);
    if ARowValues.ValueExists(CLAutomaticSavePackField) then
    begin
      Value:= ARowValues.Value[CLAutomaticSavePackField];
      if (VarToStr(Value) = '') then
        ARowValues.AddValue(CLAutomaticSavePackField, 0);
    end;

    //Read_only
    PackageField := FPackage.GetFieldByName(CLReadOnlyPackField);
    if ARowValues.ValueExists(CLReadOnlyPackField) then
    begin
      FCLReadOnlyMode := VarToStrDef(ARowValues.Value[CLReadOnlyPackField], 'N');
      //To create CL, read_only is 'N', It will updated FCLReadOnlyMode in the last minute
      ARowValues.AddValue(CLReadOnlyPackField, 'N');
    end;
  end;
end;


function TEvExchangeCLEngine.ComputeCalculatedFields(
  const ADataSet: TevClientDataSet): boolean;
begin
   Result := false;
end;

procedure TEvExchangeCLEngine.UpdateAddressBillingAndDeliveryMethod;
var
  qCL: IevQuery;

  procedure DoUpdate (const provName: string);
  var
    Cds: TevClientDataset;
  begin
    Cds:= TevClientDataset.Create(nil);
    try
      Cds.ProviderName := provName;
      Cds.SkipFieldCheck := True;
      Cds.DataRequired('ALL');
      Cds.First;
      if AnsiSameText(provName,'CL_PROV') then
      begin
        Cds.Edit;
        Cds.FieldByName('READ_ONLY').asString :=  FCLReadOnlyMode;
        Cds.Post;
      end
      else
      begin
        while not Cds.Eof do
        begin
          Cds.Edit;
          if Trim(Cds.FieldByName('NAME').asString)= '' then
             Cds.FieldByName('NAME').asString := qCL.Result.FieldByName('NAME').asString;
          if Trim(Cds.FieldByName('ADDRESS1').asString)= '' then
             Cds.FieldByName('ADDRESS1').asString := qCL.Result.FieldByName('ADDRESS1').asString;
          if Trim(Cds.FieldByName('ADDRESS2').asString)= '' then
             Cds.FieldByName('ADDRESS2').asString := qCL.Result.FieldByName('ADDRESS2').asString;
          if Trim(Cds.FieldByName('CITY').asString)= '' then
             Cds.FieldByName('CITY').asString := qCL.Result.FieldByName('CITY').asString;
          if Trim(Cds.FieldByName('STATE').asString)= '' then
             Cds.FieldByName('STATE').asString := qCL.Result.FieldByName('STATE').asString;
          if Trim(Cds.FieldByName('ZIP_CODE').asString)= '' then
             Cds.FieldByName('ZIP_CODE').asString := qCL.Result.FieldByName('ZIP_CODE').asString;
          Cds.Post;
          Cds.Next;
        end;
      end;
      ctx_DataAccess.PostDataSets([Cds]);
    finally
      Cds.Free;
    end;
  end;
begin
  qCL := TevQuery.Create('select NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP_CODE from cl a where {AsOfNow<a>} ');
  qCL.Execute;
  CheckCondition(qCL.Result.RecordCount = 1, 'Cannot find Address Info record in CL table.');

  DOUpdate(DM_Client.CL_BILLING.ProviderName);
  DOUpdate(DM_Client.CL_DELIVERY_METHOD.ProviderName);

  if (FCLReadOnlyMode <> '') and (FCLReadOnlyMode <> 'N') then
    DOUpdate(DM_Client.CL.ProviderName);
end;

procedure TEvExchangeCLEngine.DoImport;
var
  CustomClientField,ClientNameField: IevEchangeMapField;
  Q: IevQuery;
begin
  inherited;

  if FErrorsList.Count > 0 then
    exit;

  FIsNewCL := True;
  // import
  try
    FillListOfMappedTables;
    FImpEngine.StartMappingI(GetMapDefs, FFunctionsDefinitions as IIEDefinition, false);
    try
      CustomClientField := FMapFile.GetFieldByExchangeName(NewCustomClientNbrPackageField);
      ClientNameField := FMapFile.GetFieldByExchangeName(NewClientNamePackageField);

      FInputData.First;
      while not FInputData.eof do
      begin
        try
          if Assigned(CustomClientField) then
          begin
            FNewCLCode :=  FInputData.FieldByName(CustomClientField.GetInputFieldName).Value;
            CheckCondition(FNewCLCode <> '', 'Value of Client Code field is empty');

            FCLName :=  FInputData.FieldByName(ClientNameField.GetInputFieldName).Value;

            //To find the exisitng Client
            Q := TevQuery.Create('Select CL_NBR from TMP_CL where CUSTOM_CLIENT_NUMBER = ''' + FNewCLCode + '''');
            Q.Execute;
            FNewCLNbr := Q.Result.Fields[0].AsInteger;
            FIsNewCL := FNewCLNbr = 0;

            if FIsNewCL then
            begin
              FNewCLNbr := ctx_DBAccess.CreateClientDB(FInputData);
              if FNewCLNbr <= 0 then
                 raise Exception.Create('Cannot create Client Database(Client Code:'+FNewCLCode+')');
            end;

            ctx_DataAccess.OpenClient(FNewCLNbr);
            FImpEngine.ClearDataSetsAndVariables;
            ImportRow;

            UpdateAddressBillingAndDeliveryMethod;

            Inc(FImportedCount);
          end;
        except
          on EE:Exception do
          begin
            FErrorsList.AddEvent(etError, FormatErrorMessageForRow(EE.Message));
            if FIsNewCL then
            begin
              try
                ctx_DBAccess.DeleteClientDB(FNewCLNbr);
              except
                ;
              end;
            end;
          end;
        end;
        AbortIfCurrentThreadTaskTerminated;
        FInputData.Next;
      end;  // while
    finally
      FImpEngine.EndMapping;
    end;
  except
    on E: EAbort do
      raise;
    on E:Exception do
      FErrorsList.AddEvent(etFatalError, 'Exception in DOImport procedure. Error: ' + E.Message);
  end;

  // post processing
  try
    AfterImport;
  except
    on E:Exception do
      FErrorsList.AddEvent(etError, 'Exception in AfterImport procedure. Error: ' + E.Message);
  end;

  FImportResult.AddValue(EvoXInputRowsAmount, FRowsCount);
  FImportResult.AddValue(EvoXImportedRowsAmount, FImportedCount);
  FImportResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
  FImportResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
  FImportResult.AddValue(EvoXExtErrorsList, FErrorsList);
end;

procedure TEvExchangeCLEngine.DoOnConstruction;
begin
  inherited;
  FFunctionsDefinitions := TEvExchangeClFunctions.Create(FImpEngine, FChangeLogList);

end;

procedure TEvExchangeCLEngine.FillDataSetWithDefaultValues(
  const ADataSet: TevClientDataSet;
  const AStage: TEvEchangeDefaultsStages);
begin
  inherited;
end;

function TEvExchangeCLEngine.FormatErrorMessageForRow(
  const AErrorMessage: String): String;
begin
 Result := 'Error: ' + StringReplace(AErrorMessage, #13#10, '  ', [rfReplaceAll]);
 Result := 'Client Internal NBR"' + IntTostr(FNewCLNbr) + '" ' +
           'Client Code"' + FNewCLCode + '" ' + Result;
end;

function TEvExchangeCLEngine.GetInputDataSortIndexFieldNames: String;
var
  CustomClientField: IevEchangeMapField;
begin
  Result := '';
  CustomClientField := FMapFile.GetFieldByExchangeName(NewCustomClientNbrPackageField);

  if Assigned(CustomClientField) then
    Result := CustomClientField.GetInputFieldName;
end;


function TEvExchangeCLEngine.GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO;
begin
  Result := FKeyFieldsList as IisStringListRO;
end;

procedure TEvExchangeCLEngine.InitListOfKeyFieldsWithCalculatedDefaults;
begin
  inherited;
  FKeyFieldsList := TisStringList.Create;
end;

end.
