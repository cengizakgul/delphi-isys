unit EvExchangeYTDPrEngine;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvUtils,
     EvConsts, EvStreamUtils, EvTypes, isLogFile, EvContext, isBasicUtils, Variants, DB,
     EvEchangePrFunctions, IEMap, EvExchangeEngine,  EvExchangeDefaultsCalculator,
     EvExceptions, isThreadManager, EvClientDataSet;

type

  // YTD payroll engine
  TEvExchangeYTDPrEngine = class(TEvExchangeEngine, IevExchangeEngine)
  private
    FPayrollCheckDate: Variant;
    FKeyFieldsList: IisStringList;
    FPayrollRunNbr: Variant;

    FCompanies: TevClientDataSet;
    FPayrolls: TevClientDataSet;
    FPayrollBatch: TevClientDataSet;
    FPayrollChecks: TevClientDataSet;
    FCheckLines: TevClientDataSet;
    FCheckStates: TevClientDataSet;
    FCheckLocals: TevClientDataSet;
    FCheckSuis: TevClientDataSet;
    PR: TevClientDataSet;
    PR_BATCH: TevClientDataSet;
    PR_CHECK: TevClientDataSet;
    PR_CHECK_LINES: TevClientDataSet;
    PR_CHECK_STATES: TevClientDataSet;
    PR_CHECK_LOCALS: TevClientDataSet;
    PR_CHECK_SUI: TevClientDataSet;
  protected
    procedure DoOnConstruction; override;
    procedure FreeDataSets;

    function  FormatErrorMessageForRow(const AErrorMessage: String): String; override;
    procedure InitListOfKeyFieldsWithCalculatedDefaults; override;
    function  GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO; override;
    procedure CalculateDefaultsForKeyFields(const ARowValues: IisListOfValues); override;
    procedure AddAdditionalParamsToLog(const ALogRecord: IevExchangeChangeRecord; const APostedParams: IisListOfValues); override;
    procedure ParseImportOptions; override;
    function  GetInputDataSortIndexFieldNames: String; override;

    procedure BeforeImport; override;
    procedure PostUpdates(AUpdateArray: TUpdateRecordArray; const ARowMappedValues: IisListOfValues); override;
    procedure AfterImport; override;

    procedure CheckInputDataWithMap; override;

    procedure FillDataSetWithDefaultValues(const ADataSet: TevClientDataSet; const AStage: TEvEchangeDefaultsStages); override;
    function  ComputeCalculatedFields(const ADataSet: TevClientDataSet): boolean; override;
    procedure DoCheckMapFile; override;
    procedure ImportRow; override;
    procedure DoImport; override;
  public
    destructor Destroy; override;
  end;

implementation

uses EvExchangeConsts, EvExchangeEEConsts, EvDataset, isTypes, EvExchangeUtils, SDDClasses, SDataStructure,
  kbmMemTable, SDataDictclient;

{ TEvExchangeYTDPrEngine }

procedure TEvExchangeYTDPrEngine.AddAdditionalParamsToLog(const ALogRecord: IevExchangeChangeRecord;
  const APostedParams: IisListOfValues);
begin
  inherited;

end;

procedure TEvExchangeYTDPrEngine.CalculateDefaultsForKeyFields(
  const ARowValues: IisListOfValues);
begin
  inherited;
  if AnsiSameText(Fpackage.GetName, EvoXYTDPrPackage) then
  begin
    if not ARowValues.ValueExists(EvoPrRunNumber) then
      ARowValues.AddValue(EvoPrRunNumber, EvoXPrRunNbrDefaultValue)
    else if ARowValues.Value[EvoPrRunNumber] = null then
      ARowValues.Value[EvoPrRunNumber] := EvoXPrRunNbrDefaultValue;
  end;
end;

procedure TEvExchangeYTDPrEngine.CheckInputDataWithMap;
begin
  inherited;

end;

function TEvExchangeYTDPrEngine.ComputeCalculatedFields(const ADataSet: TevClientDataSet): boolean;
begin
  Result := false;
end;

destructor TEvExchangeYTDPrEngine.Destroy;
begin
  FreeDataSets;

  inherited;
end;

procedure TEvExchangeYTDPrEngine.DoCheckMapFile;
begin
  inherited;

end;

procedure TEvExchangeYTDPrEngine.DoOnConstruction;
begin
  inherited;
  FPayrollRunNbr := null;
  FPayrollCheckDate := null;

  FFunctionsDefinitions := TEvExchangePrFunctions.Create(FImpEngine, FChangeLogList);
end;

procedure TEvExchangeYTDPrEngine.FillDataSetWithDefaultValues(const ADataSet: TevClientDataSet;
  const AStage: TEvEchangeDefaultsStages);
begin
  inherited;
end;

function TEvExchangeYTDPrEngine.FormatErrorMessageForRow(const AErrorMessage: String): String;
begin
  Result := 'Error: "' + StringReplace(AErrorMessage, #13#10, '  ', [rfReplaceAll]) + '". All data for this payroll will be rolled back';
  if VarToStr(FPayrollRunNbr) <> '' then
    Result := 'Payroll Run Nbr "' + VarToStr(FPayrollRunNbr) + '" ' + Result;
  if VarToStr(FPayrollCheckDate) <> '' then
    Result := 'Payroll Check Date "' + VarToStr(FPayrollCheckDate) + '" ' + Result;
  if FCustomCoNbr <> '' then
    Result := 'CustomCo "' + FCustomCoNbr + '" ' + Result;
  Result := 'Row#' + IntToStr(FInputData.vclDataSet.RecNo)  + ' ' + Result;
end;

function TEvExchangeYTDPrEngine.GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO;
begin
  Result := FKeyFieldsList as IisStringListRO;
end;

procedure TEvExchangeYTDPrEngine.ImportRow;
begin
  inherited;

end;

procedure TEvExchangeYTDPrEngine.InitListOfKeyFieldsWithCalculatedDefaults;
begin
  inherited;
  FKeyFieldsList := TisStringList.Create;

  if AnsiSameText(Fpackage.GetName, EvoXYTDPrPackage) then
  begin
    FkeyFieldsList.Add(EvoPrRunNumber);
  end;
end;

procedure TEvExchangeYTDPrEngine.ParseImportOptions;
begin
  inherited;
end;

procedure TEvExchangeYTDPrEngine.BeforeImport;

  function GetTable(const TableName: String): TevClientDataSet;
  var
    Tbl: TClass;
  begin
    Tbl := GetClass('T' + TableName);
    Result := TddTableClass(Tbl).Create(nil);
    Result.ProviderName := TableName + '_PROV';
    Result.SkipFieldCheck := True;
  end;

begin
  inherited;

  FreeDataSets;

  PR := GetTable('PR');
  PR_BATCH := GetTable('PR_BATCH');
  PR_CHECK := GetTable('PR_CHECK');
  PR_CHECK_LINES := GetTable('PR_CHECK_LINES');
  PR_CHECK_STATES := GetTable('PR_CHECK_STATES');
  PR_CHECK_SUI := GetTable('PR_CHECK_SUI');
  PR_CHECK_LOCALS := GetTable('PR_CHECK_LOCALS');

  FCompanies := TevClientDataSet.Create(nil);
  FCompanies.ProviderName := 'CL_CUSTOM_PROV';
  FCompanies.FieldDefs.Add('CL_NBR', ftInteger);
  FCompanies.FieldDefs.Add('CO_NBR', ftInteger);
  FCompanies.FieldDefs.Add('CUSTOM_COMPANY_NUMBER', ftString, 20);
  FCompanies.CreateDataSet;

  FPayrolls := TevClientDataSet.Create(nil);
  FPayrolls.ProviderName := 'CL_CUSTOM_PROV';
  FPayrolls.FieldDefs.Add('CL_NBR', ftInteger);
  FPayrolls.FieldDefs.Add('CO_NBR', ftInteger);
  FPayrolls.FieldDefs.Add('PR_NBR', ftInteger);
  FPayrolls.FieldDefs.Add('CHECK_DATE', ftDateTime);
  FPayrolls.FieldDefs.Add('RUN_NUMBER', ftInteger);
  FPayrolls.FieldDefs.Add('REAL_RUN_NUMBER', ftInteger);  
  FPayrolls.CreateDataSet;

  FPayrollBatch := TevClientDataSet.Create(nil);
  FPayrollBatch.ProviderName := 'CL_CUSTOM_PROV';
  FPayrollBatch.FieldDefs.Add('CL_NBR', ftInteger);
  FPayrollBatch.FieldDefs.Add('CO_NBR', ftInteger);
  FPayrollBatch.FieldDefs.Add('PR_NBR', ftInteger);
  FPayrollBatch.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
  FPayrollBatch.FieldDefs.Add('PERIOD_BEGIN_DATE', ftDateTime);
  FPayrollBatch.FieldDefs.Add('PERIOD_END_DATE', ftDateTime);
  FPayrollBatch.CreateDataSet;

  FPayrollChecks := TevClientDataSet.Create(nil);
  FPayrollChecks.ProviderName := 'CL_CUSTOM_PROV';
  FPayrollChecks.FieldDefs.Add('CL_NBR', ftInteger);
  FPayrollChecks.FieldDefs.Add('CO_NBR', ftInteger);
  FPayrollChecks.FieldDefs.Add('PR_NBR', ftInteger);
  FPayrollChecks.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
  FPayrollChecks.FieldDefs.Add('EE_NBR', ftInteger);
  FPayrollChecks.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER',ftString, 9);
  FPayrollChecks.FieldDefs.Add('FIRST_NAME', ftString, 20);
  FPayrollChecks.FieldDefs.Add('LAST_NAME', ftString, 30);
  FPayrollChecks.FieldDefs.Add('SOCIAL_SECURITY_NUMBER', ftString, 11);
  FPayrollChecks.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
  FPayrollChecks.CreateDataSet;

  FCheckLines := TevClientDataSet.Create(nil);
  FCheckLines.ProviderName := 'CL_CUSTOM_PROV';
  FCheckLines.FieldDefs.Add('CL_NBR', ftInteger);
  FCheckLines.FieldDefs.Add('CO_NBR', ftInteger);
  FCheckLines.FieldDefs.Add('PR_NBR', ftInteger);
  FCheckLines.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
  FCheckLines.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
  FCheckLines.CreateDataSet;

  FCheckStates := TevClientDataSet.Create(nil);
  FCheckStates.ProviderName := 'CL_CUSTOM_PROV';
  FCheckStates.FieldDefs.Add('CL_NBR', ftInteger);
  FCheckStates.FieldDefs.Add('CO_NBR', ftInteger);
  FCheckStates.FieldDefs.Add('PR_NBR', ftInteger);
  FCheckStates.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
  FCheckStates.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
  FCheckStates.CreateDataSet;

  FCheckLocals := TevClientDataSet.Create(nil);
  FCheckLocals.ProviderName := 'CL_CUSTOM_PROV';
  FCheckLocals.FieldDefs.Add('CL_NBR', ftInteger);
  FCheckLocals.FieldDefs.Add('CO_NBR', ftInteger);
  FCheckLocals.FieldDefs.Add('PR_NBR', ftInteger);
  FCheckLocals.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
  FCheckLocals.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
  FCheckLocals.CreateDataSet;

  FCheckSuis := TevClientDataSet.Create(nil);
  FCheckSuis.ProviderName := 'CL_CUSTOM_PROV';
  FCheckSuis.FieldDefs.Add('CL_NBR', ftInteger);
  FCheckSuis.FieldDefs.Add('CO_NBR', ftInteger);
  FCheckSuis.FieldDefs.Add('PR_NBR', ftInteger);
  FCheckSuis.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
  FCheckSuis.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
  FCheckSuis.CreateDataSet;

end;

procedure TEvExchangeYTDPrEngine.PostUpdates(AUpdateArray: TUpdateRecordArray; const ARowMappedValues: IisListOfValues);

  function GetValue(const ATableName, AFieldName: String): Variant;
  var
    i: Integer;
  begin
    Result := Null;
    for i := Low(AUpdateArray) to High(AUpdateArray) do
    begin
      if AnsiSameText(AUpdateArray[i].Table, ATableName)
      and AnsiSameText(AUpdateArray[i].Field, AFieldName) then
      begin
        Result := AUpdateArray[i].Value;
        break;
      end;
    end;
  end;

  function GetGroupValue(const ATableName, AFieldName: String; const GroupNumber: Integer): Variant;
  var
    i: Integer;
  begin
    Result := Null;
    for i := Low(AUpdateArray) to High(AUpdateArray) do
    begin
      if AnsiSameText(AUpdateArray[i].Table, ATableName)
      and AnsiSameText(AUpdateArray[i].Field, AFieldName)
      and (AUpdateArray[i].GroupRecordNumber = GroupNumber) then
      begin
        Result := AUpdateArray[i].Value;
        break;
      end;
    end;
  end;

var
  CO_NBR, PR_NBR, PR_BATCH_NBR, EE_NBR, PR_CHECK_NBR,
  CL_E_DS_NBR, CO_STATES_NBR, EE_STATES_NBR, EE_LOCALS_NBR,
  CO_SUI_NBR, SY_LOCALS_NBR, CO_LOCAL_TAX_NBR, SY_STATES_NBR,
  SY_SUI_NBR: Integer;
  CustomCompanyNumber: String;
  CustomEmployeeNumber, FirstName, LastName, SSN, CustomEdCodeNumber: Variant;
  CheckDate, PeriodBeginDate, PeriodEndDate: TDateTime;
  RunNumber, ImportRunNumber: Variant;
  CO_DIVISION_NBR, CO_BRANCH_NBR, CO_DEPARTMENT_NBR, CO_TEAM_NBR: Variant;
  StateTaxCount, SuiCount, LocalTaxCount, CheckLinesCount, i : Integer;
  State, SuiName, LocalName: String;
  PayrollComments : Variant;
  s: String;
  BlobData: IisStream;
begin
  CustomCompanyNumber := GetValue('CO', 'CUSTOM_COMPANY_NUMBER');
  if not FCompanies.Locate('CL_NBR;CUSTOM_COMPANY_NUMBER', VarArrayOf([ctx_DataAccess.ClientID, CustomCompanyNumber]), []) then
  begin
    DM_COMPANY.CO.DataRequired('');
    if not DM_COMPANY.CO.Locate('CUSTOM_COMPANY_NUMBER', CustomCompanyNumber, []) then
      raise EevException.Create('Custom Company Number "'+CustomCompanyNumber+'" not found.');
    FCompanies.Insert;
    FCompanies.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := CustomCompanyNumber;
    FCompanies.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.CO_NBR.AsInteger;
    FCompanies.FieldByName('CL_NBR').AsInteger := ctx_DataAccess.ClientID;
    FCompanies.Post;
  end;
  CO_NBR := FCompanies.FieldByName('CO_NBR').AsInteger;
  DM_COMPANY.CO.Locate('CO_NBR', CO_NBR, []);

  CheckDate := GetValue('PR', 'CHECK_DATE');
  ImportRunNumber := GetValue('PR', 'RUN_NUMBER');

  if VarIsNull(ImportRunNumber) then
    ImportRunNumber := 1;

  if not FPayrolls.Locate('CL_NBR;CO_NBR;CHECK_DATE;RUN_NUMBER', VarArrayOf([ctx_DataAccess.CLientID, CO_NBR, CheckDate, ImportRunNumber]), []) then
  begin
    PR.DataRequired('');

    RunNumber := ImportRunNumber;

    while PR.Locate('CO_NBR;CHECK_DATE;RUN_NUMBER', VarArrayOf([CO_NBR, CheckDate, RunNumber]), []) do
      RunNumber := RunNumber + 1;

    PR.Append;
    PR.FieldByName('RUN_NUMBER').AsInteger := RunNumber;
    PR.FieldByName('CO_NBR').AsInteger := CO_NBR;
    PR.FieldByName('PAYROLL_TYPE').AsString := PAYROLL_TYPE_IMPORT;
    PR.FieldByName('SCHEDULED').AsString := 'N';
    PR.FieldByName('CHECK_DATE').AsDateTime := CheckDate;
    PR.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
    PR.FieldByName('EXCLUDE_AGENCY').AsString := 'Y';
    PR.FieldByName('EXCLUDE_ACH').AsString := 'Y';
    PR.FieldByName('EXCLUDE_BILLING').AsString := 'Y';
    PR.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString := GROUP_BOX_BOTH2;
    PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := DM_COMPANY.CO.FieldByName('EXCLUDE_R_C_B_0R_N').AsString;
    PR.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
    PR.FieldByName('SCHEDULED_CALL_IN_DATE').AsDateTime := CheckDate;
    PR.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := CheckDate;
    PR.FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := CheckDate;
    PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := DM_COMPANY.CO.FieldByName('EXCLUDE_R_C_B_0R_N').AsString;
    // if EXCLUDE_ACH = 'Y' , should be all PAYMENT_TYPE_NONE
    PR.FieldByName('TAX_IMPOUND').AsString := PAYMENT_TYPE_NONE;
    PR.FieldByName('TRUST_IMPOUND').AsString := PAYMENT_TYPE_NONE;
    PR.FieldByName('BILLING_IMPOUND').AsString := PAYMENT_TYPE_NONE;
    PR.FieldByName('DD_IMPOUND').AsString := PAYMENT_TYPE_NONE;
    PR.FieldByName('WC_IMPOUND').AsString := PAYMENT_TYPE_NONE;
    PR.FieldByName('STATUS_DATE').AsDateTime := SysTime;

    PayrollComments := GetValue('PR', 'PAYROLL_COMMENTS');
    if not VarIsNull(PayrollComments) then
    begin
      if (PayrollComments = '') then
        PR.FieldByName('PAYROLL_COMMENTS').Clear
      else
      begin
        BlobData := TEvDualStreamHolder.Create;
        s := PayrollComments;
        BlobData.WriteBuffer(s[1], Length(s));
        BlobData.Position := 0;
        TBlobField(PR.FieldByName('PAYROLL_COMMENTS')).LoadFromStream(BlobData.RealStream);
      end;
    end;

    PR.Post;
    ctx_DataAccess.PostDataSets([PR]);

    PR_NBR := PR.FieldByName('PR_NBR').AsInteger;

    FPayrolls.Insert;
    FPayrolls.FieldByName('CL_NBR').AsInteger := ctx_DataAccess.ClientID;
    FPayrolls.FieldByName('CO_NBR').AsInteger := CO_NBR;
    FPayrolls.FieldByName('PR_NBR').AsInteger := PR_NBR;
    FPayrolls.FieldByName('CHECK_DATE').AsDateTime := CheckDate;
    FPayrolls.FieldByName('RUN_NUMBER').AsInteger := ImportRunNumber;
    FPayrolls.FieldByName('REAL_RUN_NUMBER').AsInteger := RunNumber;
    FPayrolls.Post;

  end
  else
    PR_NBR := FPayrolls.FieldByName('PR_NBR').AsInteger;

  PeriodBeginDate := GetValue('PR_BATCH', 'PERIOD_BEGIN_DATE');
  PeriodEndDate := GetValue('PR_BATCH', 'PERIOD_END_DATE');

  if not FPayrollBatch.Locate('CL_NBR;PR_NBR;PERIOD_BEGIN_DATE;PERIOD_END_DATE',
    VarArrayOf([ctx_DataAccess.ClientID, FPayrolls['PR_NBR'], PeriodBeginDate, PeriodEndDate]), []) then
  begin
    PR_BATCH.DataRequired('');
    PR_BATCH.Insert;

    PR_BATCH.FieldByName('PR_NBR').AsInteger := PR_NBR;
    PR_BATCH.FieldByName('FREQUENCY').Value := Copy(ctx_PayrollCalculation.GetCompanyPayFrequencies, Pos(#9, ctx_PayrollCalculation.GetCompanyPayFrequencies) + 1, 1);
    PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := PeriodBeginDate;
    PR_BATCH.FieldByName('PERIOD_END_DATE').Value := PeriodEndDate;
    PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
    PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;

    PR_BATCH.FieldByName('PAY_SALARY').Value := 'N';
    PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N';
    PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'Y';
    PR_BATCH.Post;

    ctx_DataAccess.PostDataSets([PR_BATCH]);

    PR_BATCH_NBR := PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;

    FPayrollBatch.Insert;
    FPayrollBatch.FieldByName('CL_NBR').AsInteger := ctx_DataAccess.ClientID;
    FPayrollBatch.FieldByName('CO_NBR').AsInteger := CO_NBR;
    FPayrollBatch.FieldByName('PR_NBR').AsInteger := PR_NBR;
    FPayrollBatch.FieldByName('PR_BATCH_NBR').AsInteger := PR_BATCH_NBR;
    FPayrollBatch.FieldByName('PERIOD_BEGIN_DATE').AsDateTime := PeriodBeginDate;
    FPayrollBatch.FieldByName('PERIOD_END_DATE').AsDateTime := PeriodEndDate;
    FPayrollBatch.Post;
  end
  else
    PR_BATCH_NBR := FPayrollBatch.FieldByName('PR_BATCH_NBR').AsInteger;

  EE_NBR := -1;
  CustomEmployeeNumber := GetValue('EE', 'CUSTOM_EMPLOYEE_NUMBER');
  if not VarIsNull(CustomEmployeeNumber) then
  begin
    if FPayrollChecks.Locate('CL_NBR;PR_NBR;PR_BATCH_NBR;CUSTOM_EMPLOYEE_NUMBER',
      VarArrayOf([ctx_DataAccess.CLientID, PR_NBR, PR_BATCH_NBR, CustomEmployeeNumber]), []) then
      EE_NBR := FPayrollChecks.FieldByName('EE_NBR').AsInteger
    else
    begin
      DM_EMPLOYEE.EE.DataRequired('');
      if DM_EMPLOYEE.EE.Locate('CO_NBR;CUSTOM_EMPLOYEE_NUMBER',
          VarArrayOf([CO_NBR, CustomEmployeeNumber]), [loPartialKey]) then
        EE_NBR := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger
      else
        raise EevException.Create('EE Code "'+CustomEmployeeNumber+'" not found.');
    end;
  end
  else
  begin
    FirstName := GetValue('CL_PERSON', 'FIRST_NAME');
    LastName := GetValue('CL_PERSON', 'LAST_NAME');
    if FPayrollChecks.Locate('CL_NBR;PR_NBR;PR_BATCH_NBR;FIRST_NAME;LAST_NAME',
      VarArrayOf([ctx_DataAccess.CLientID, PR_NBR, PR_BATCH_NBR, FirstName, LastName]), []) then
      EE_NBR := FPayrollChecks.FieldByName('EE_NBR').AsInteger
    else
    begin
      if not VarIsNull(FirstName) and not VarIsNull(LastName) then
      begin
        DM_CLIENT.CL_PERSON.DataRequired('');
        if not DM_CLIENT.CL_PERSON.Locate('FIRST_NAME;LAST_NAME',
          VarArrayOf([FirstName, LastName]), []) then
          raise EevException.Create('Employee '+FirstName+' '+LastName+' not found.');

        DM_EMPLOYEE.EE.DataRequired('');
        if DM_EMPLOYEE.EE.Locate('CO_NBR;CL_PERSON_NBR',
            VarArrayOf([CO_NBR, DM_CLIENT.CL_PERSON['CL_PERSON_NBR']]), []) then
          EE_NBR := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger
        else
          raise EevException.Create('Employee '+FirstName+' '+LastName+' not found.');

      end
      else
      begin
        SSN := GetValue('CL_PERSON', 'SOCIAL_SECURITY_NUMBER');
        if FPayrollChecks.Locate('CL_NBR;PR_NBR;PR_BATCH_NBR;SOCIAL_SECURITY_NUMBER',
          VarArrayOf([ctx_DataAccess.ClientID, PR_NBR, PR_BATCH_NBR, SSN]), []) then
          EE_NBR := FPayrollChecks.FieldByName('EE_NBR').AsInteger
        else
        if not VarIsNull(SSN) then
        begin
          DM_CLIENT.CL_PERSON.DataRequired('');
          if not DM_CLIENT.CL_PERSON.Locate('SOCIAL_SECURITY_NUMBER',
            VarArrayOf([SSN]), []) then
            raise EevException.Create('SSN '+SSN+' not found.');
          DM_EMPLOYEE.EE.DataRequired('');
          if DM_EMPLOYEE.EE.Locate('CO_NBR;CL_PERSON_NBR',
              VarArrayOf([CO_NBR, DM_CLIENT.CL_PERSON['CL_PERSON_NBR']]), []) then
            EE_NBR := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger
          else
            raise EevException.Create('SSN '+SSN+' not found.');
        end;
      end;
    end;
  end;

  Assert(DM_EMPLOYEE.EE.Locate('EE_NBR', EE_NBR, []));

  if not FPayrollChecks.Locate('CL_NBR;PR_NBR;PR_BATCH_NBR;EE_NBR',
    VarArrayOf([ctx_DataAccess.CLientID, PR_NBR, PR_BATCH_NBR, EE_NBR]), []) then
  begin
    PR_CHECK.DataRequired('PR_NBR='+IntToStr(PR_NBR));
    PR_CHECK.Insert;
    PR_CHECK.FieldByName('EE_NBR').Value := EE_NBR;
    PR_CHECK.FieldByName('PR_NBR').Value := PR_NBR;
    PR_CHECK.FieldByName('PR_BATCH_NBR').Value := PR_BATCH_NBR;
    PR_CHECK.FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_MANUAL;
    PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
    PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := CheckDate;
    PR_CHECK.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('TAX_FREQUENCY').AsString := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').AsString;
    PR_CHECK.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_FEDERAL').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').AsString := GROUP_BOX_YES;
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_OASDI').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYER_OASDI').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYER_MEDICARE').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_EIC').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('EXCLUDE_EMPLOYER_FUI').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').AsString := GROUP_BOX_NO;
    PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := ctx_PayrollCalculation.GetNextPaymentSerialNbr;
    PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := ctx_PayrollCalculation.GetCustomBankAccountNumber(PR_CHECK);
    if not VarIsNull(GetValue('PR_CHECK', 'OR_CHECK_FEDERAL_VALUE')) then
    begin
      PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value := GetValue('PR_CHECK', 'OR_CHECK_FEDERAL_VALUE');
      PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
    end
    else
      PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;;
    if not VarIsNull(GetValue('PR_CHECK', 'OR_CHECK_OASDI')) then
      PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := GetValue('PR_CHECK', 'OR_CHECK_OASDI');
    if not VarIsNull(GetValue('PR_CHECK', 'OR_CHECK_MEDICARE')) then
      PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := GetValue('PR_CHECK', 'OR_CHECK_MEDICARE');
    if not VarIsNull(GetValue('PR_CHECK', 'OR_CHECK_EIC')) then
      PR_CHECK.FieldByName('OR_CHECK_EIC').Value := GetValue('PR_CHECK', 'OR_CHECK_EIC');
    if not VarIsNull(GetValue('PR_CHECK', 'OR_CHECK_BACK_UP_WITHHOLDING')) then
      PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value := GetValue('PR_CHECK', 'OR_CHECK_BACK_UP_WITHHOLDING');

    s := VarToStr(GetValue('CL_BLOB', 'DATA'));
    if s <> '' then
      PR_CHECK.UpdateBlobData('NOTES_NBR', s);

    PR_CHECK.Post;
    ctx_DataAccess.PostDataSets([DM_CLIENT.CL_BLOB, PR_CHECK]);
    PR_CHECK_NBR := PR_CHECK.FIeldByName('PR_CHECK_NBR').AsInteger;

    FPayrollChecks.Insert;
    FPayrollChecks.FieldByName('CL_NBR').AsInteger := ctx_DataAccess.ClientID;
    FPayrollChecks.FieldByName('CO_NBR').AsInteger := CO_NBR;
    FPayrollChecks.FieldByName('PR_NBR').AsInteger := PR_NBR;
    FPayrollChecks.FieldByName('PR_BATCH_NBR').AsInteger := PR_BATCH_NBR;
    FPayrollChecks.FieldByName('EE_NBR').AsInteger := EE_NBR;
    FPayrollChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString := CustomEmployeeNumber;
    FPayrollChecks.FieldByName('FIRST_NAME').AsString := DM_CLIENT.CL_PERSON.FieldByName('FIRST_NAME').AsString;
    FPayrollChecks.FieldByName('LAST_NAME').AsString := DM_CLIENT.CL_PERSON.FieldByName('LAST_NAME').AsString;
    FPayrollChecks.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
    FPayrollChecks.FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK_NBR;
    FPayrollChecks.Post;

  end
  else
    PR_CHECK_NBR := FPayrollChecks.FieldByName('PR_CHECK_NBR').AsInteger;

  StateTaxCount := 0;
  SuiCount := 0;
  LocalTaxCount := 0;
  CheckLinesCount := 0;

  for i := Low(AUpdateArray) to High(AUpdateArray) do
  begin
    if (AUpdateArray[i].Table = 'PR_CHECK_STATES')
    and (AUpdateArray[i].Field = 'EE_STATES_NBR') then
    begin
      if StateTaxCount < AUpdateArray[i].GroupRecordNumber then
        StateTaxCount := AUpdateArray[i].GroupRecordNumber;
    end
    else
    if (AUpdateArray[i].Table = 'PR_CHECK_SUI')
    and (AUpdateArray[i].Field = 'CO_SUI_NBR') then
    begin
      if SuiCount < AUpdateArray[i].GroupRecordNumber then
        SuiCount := AUpdateArray[i].GroupRecordNumber;
    end
    else
    if (AUpdateArray[i].Table = 'PR_CHECK_LINES')
    and (AUpdateArray[i].Field = 'CL_E_DS_NBR') then
    begin
      if CheckLinesCount < AUpdateArray[i].GroupRecordNumber then
        CheckLinesCount := AUpdateArray[i].GroupRecordNumber;
    end
    else
    if (AUpdateArray[i].Table = 'PR_CHECK_LOCALS')
    and (AUpdateArray[i].Field = 'EE_LOCALS_NBR') then
    begin
      if LocalTaxCount < AUpdateArray[i].GroupRecordNumber then
        LocalTaxCount := AUpdateArray[i].GroupRecordNumber;
    end;
  end;

  for i := 1 to CheckLinesCount do
  begin
    CustomEdCodeNumber := GetGroupValue('PR_CHECK_LINES', 'CL_E_DS_NBR', i);

    if not VarIsNull(CustomEdCodeNumber) then
    begin

      DM_CLIENT.CL_E_DS.DataRequired('');

      if DM_CLIENT.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', CustomEdCodeNumber, []) then
        CL_E_DS_NBR := DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger
      else
        raise EevException.Create('E/D Code "'+CustomEdCodeNumber+'" not found.');

      CO_DIVISION_NBR := GetGroupValue('PR_CHECK_LINES', 'CO_DIVISION_NBR', i);
      CO_BRANCH_NBR := GetGroupValue('PR_CHECK_LINES', 'CO_BRANCH_NBR', i);
      CO_DEPARTMENT_NBR := GetGroupValue('PR_CHECK_LINES', 'CO_DEPARTMENT_NBR', i);
      CO_TEAM_NBR := GetGroupValue('PR_CHECK_LINES', 'CO_TEAM_NBR', i);

      if not VarIsNull(CO_TEAM_NBR) then
      begin
        DM_CLIENT.CO_TEAM.DataRequired('');
        if not DM_CLIENT.CO_TEAM.Locate('CO_TEAM_NBR', CO_TEAM_NBR, []) then
          raise EevException.Create('CO_TEAM_NBR "'+CO_TEAM_NBR+'" not found.');
        if DM_CLIENT.CO_TEAM.CO_DEPARTMENT_NBR.Value <> CO_DEPARTMENT_NBR then
          raise EevException.Create('CO_TEAM_NBR "'+CO_TEAM_NBR+'" not linked to department.');
      end;

      if not VarIsNull(CO_DEPARTMENT_NBR) then
      begin
        DM_CLIENT.CO_DEPARTMENT.DataRequired('');
        if not DM_CLIENT.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', CO_DEPARTMENT_NBR, []) then
          raise EevException.Create('CO_DEPARTMENT_NBR "'+CO_DEPARTMENT_NBR+'" not found.');
        if DM_CLIENT.CO_DEPARTMENT.CO_BRANCH_NBR.Value <> CO_BRANCH_NBR then
          raise EevException.Create('CO_DEPARTMENT_NBR "'+CO_DEPARTMENT_NBR+'" not linked to branch.');
      end;

      if not VarIsNull(CO_BRANCH_NBR) then
      begin
        DM_CLIENT.CO_BRANCH.DataRequired('');
        if not DM_CLIENT.CO_BRANCH.Locate('CO_BRANCH_NBR', CO_BRANCH_NBR, []) then
          raise EevException.Create('CO_BRANCH_NBR "'+CO_BRANCH_NBR+'" not found.');
        if DM_CLIENT.CO_BRANCH.CO_DIVISION_NBR.Value <> CO_DIVISION_NBR then
          raise EevException.Create('CO_BRANCH_NBR "'+CO_BRANCH_NBR+'" not linked to division.');
      end;

      PR_CHECK_LINES.DataRequired('PR_NBR='+IntToStr(PR_NBR));
      PR_CHECK_LINES.Insert;
      PR_CHECK_LINES.FieldByName('PR_NBR').AsInteger := PR_NBR;
      PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK_NBR;
      PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger := CL_E_DS_NBR;
      PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := CHECK_LINE_TYPE_USER;
      if not VarIsNull(GetGroupValue('PR_CHECK_LINES', 'AMOUNT', i)) then
        PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat := GetGroupValue('PR_CHECK_LINES', 'AMOUNT', i);
      if not VarIsNull(GetGroupValue('PR_CHECK_LINES', 'HOURS_OR_PIECES', i)) then
        PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').AsFloat := GetGroupValue('PR_CHECK_LINES', 'HOURS_OR_PIECES', i);
      if not VarIsNull(CO_DIVISION_NBR) then
        PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value := CO_DIVISION_NBR;
      if not VarIsNull(CO_BRANCH_NBR) then
        PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value := CO_BRANCH_NBR;
      if not VarIsNull(CO_DEPARTMENT_NBR) then
        PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value := CO_DEPARTMENT_NBR;
      if not VarIsNull(CO_TEAM_NBR) then
        PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value := CO_TEAM_NBR;

      if not VarIsNull(GetGroupValue('PR_CHECK_LINES', 'LINE_ITEM_DATE', i)) then
        PR_CHECK_LINES.FieldByName('LINE_ITEM_DATE').Value := GetGroupValue('PR_CHECK_LINES', 'LINE_ITEM_DATE', i);

      if not VarIsNull(GetGroupValue('PR_CHECK_LINES', 'LINE_ITEM_END_DATE', i)) then
        PR_CHECK_LINES.FieldByName('LINE_ITEM_END_DATE').Value := GetGroupValue('PR_CHECK_LINES', 'LINE_ITEM_END_DATE', i);

      if not VarIsNull(GetGroupValue('PR_CHECK_LINES', 'CO_JOBS_NBR', i)) then
      begin
        DM_COMPANY.CO_JOBS.DataRequired('');
        if not DM_COMPANY.CO_JOBS.Locate('CO_NBR;DESCRIPTION', VarArrayOf([CO_NBR, GetGroupValue('PR_CHECK_LINES', 'CO_JOBS_NBR', i)]), []) then
        begin
          DM_COMPANY.CO_JOBS.Append;
          DM_COMPANY.CO_JOBS['CO_NBR'] := CO_NBR;
          DM_COMPANY.CO_JOBS['DESCRIPTION'] := GetGroupValue('PR_CHECK_LINES', 'CO_JOBS_NBR', i);
          DM_COMPANY.CO_JOBS['JOB_ACTIVE'] := 'Y';
          DM_COMPANY.CO_JOBS['CERTIFIED'] := 'N';
          DM_COMPANY.CO_JOBS['TRUE_DESCRIPTION'] := GetGroupValue('PR_CHECK_LINES', 'CO_JOBS_NBR', i);
          DM_COMPANY.CO_JOBS['STATE_CERTIFIED'] := 'N';
          DM_COMPANY.CO_JOBS.Post;
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS]);
        end
        else
        begin
          if DM_COMPANY.CO_JOBS.JOB_ACTIVE.AsString <> GROUP_BOX_YES then
          begin
            DM_COMPANY.CO_JOBS.Edit;
            DM_COMPANY.CO_JOBS['JOB_ACTIVE'] := GROUP_BOX_YES;
            DM_COMPANY.CO_JOBS.Post;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS]);
          end;
        end;
        PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').Value := DM_COMPANY.CO_JOBS.CO_JOBS_NBR.Value;
      end;

      if DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').IsNull then
        raise EevException.Create('The Employee does not have a home TAX state attatched.');
      PR_CHECK_LINES.FieldByName('EE_STATES_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsInteger;
      PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').AsInteger := PR_CHECK_LINES.FieldByName('EE_STATES_NBR').AsInteger;

      PR_CHECK_LINES.FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_IGNORE;
      PR_CHECK_LINES.Post;
      ctx_DataAccess.PostDataSets([PR_CHECK_LINES]);
    end;


  end;


  for i := 1 to StateTaxCount do
  begin
    State := GetGroupValue('PR_CHECK_STATES', 'EE_STATES_NBR', i);
    if not VarIsNull(State) then
    begin
      DM_COMPANY.CO_STATES.DataRequired('');

      if not DM_COMPANY.CO_STATES.Locate('CO_NBR;STATE',VarArrayOf([CO_NBR, State]),[]) then
        raise EevException.Create('CO_STATES_NBR not found.');

      CO_STATES_NBR := DM_COMPANY.CO_STATES.CO_STATES_NBR.AsInteger;

      DM_EMPLOYEE.EE_STATES.DataRequired('');
      if not DM_EMPLOYEE.EE_STATES.Locate('EE_NBR;CO_STATES_NBR',VarArrayOf([EE_NBR, CO_STATES_NBR]), []) then
        raise EevException.Create('Employee #'+DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString+' has no '+State+' state setup.');

      EE_STATES_NBR := DM_EMPLOYEE.EE_STATES.EE_STATES_NBR.AsInteger;

      PR_CHECK_STATES.DataRequired('PR_NBR='+IntToStr(PR_NBR));
      PR_CHECK_STATES.Insert;
      PR_CHECK_STATES.FieldByName('PR_NBR').AsInteger := PR_NBR;
      PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK_NBR;
      PR_CHECK_STATES.FieldByName('EE_STATES_NBR').AsInteger := EE_STATES_NBR;
      PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').AsString := GROUP_BOX_YES;

      PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').AsString := GROUP_BOX_NO;
      PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').AsString := GROUP_BOX_NO;
      PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').AsString := GROUP_BOX_NO;
      PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').AsString := GROUP_BOX_NO;
      PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;

      PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsFloat := GetGroupValue('PR_CHECK_STATES', 'STATE_OVERRIDE_VALUE', i);
      if not VarIsNull(GetGroupValue('PR_CHECK_STATES', 'EE_SDI_OVERRIDE', i)) then
        PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').AsFloat := GetGroupValue('PR_CHECK_STATES', 'EE_SDI_OVERRIDE', i);
      PR_CHECK_STATES.Post;
      ctx_DataAccess.PostDataSets([PR_CHECK_STATES]);
    end;
  end;

  for i := 1 to SuiCount do
  begin
    State := GetGroupValue('PR_CHECK_SUI', 'CO_SUI_NBR', i);
    if not VarIsNull(State) then
    begin
      SuiName := GetGroupValue('SY_SUI', 'SUI_TAX_NAME', i);

      DM_SYSTEM.SY_STATES.DataRequired('');
      if not DM_SYSTEM.SY_STATES.Locate('STATE', State,[]) then
        raise EevException.Create('SY_STATES_NBR not found.');
      SY_STATES_NBR := DM_SYSTEM.SY_STATES.SY_STATES_NBR.AsInteger;

      DM_SYSTEM.SY_SUI.DataRequired('');
      if not DM_SYSTEM.SY_SUI.Locate('SY_STATES_NBR;SUI_TAX_NAME', VarArrayOf([SY_STATES_NBR, SuiName]),[]) then
        raise EevException.Create('SY_SUI_NBR ['+State+'/'+SuiName+'] not found.');
      SY_SUI_NBR := DM_SYSTEM.SY_SUI.SY_SUI_NBR.AsInteger;

      DM_COMPANY.CO_SUI.DataRequired('');

      if not DM_COMPANY.CO_SUI.Locate('CO_NBR;SY_SUI_NBR',VarArrayOf([CO_NBR, SY_SUI_NBR]),[]) then
        raise EevException.Create('CO_SUI_NBR not found.');

      CO_SUI_NBR := DM_COMPANY.CO_SUI.CO_SUI_NBR.AsInteger;
      CO_STATES_NBR := DM_COMPANY.CO_SUI.CO_STATES_NBR.AsInteger;

      DM_EMPLOYEE.EE_STATES.DataRequired('');
      if not DM_EMPLOYEE.EE_STATES.Locate('EE_NBR;SUI_APPLY_CO_STATES_NBR',VarArrayOf([EE_NBR, CO_STATES_NBR]), []) then
        raise EevException.Create(State + ' SUI state is not set up for EE #'+
          FPayrollChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);

      PR_CHECK_SUI.DataRequired('PR_NBR='+IntToStr(PR_NBR));
      PR_CHECK_SUI.Insert;
      PR_CHECK_SUI.FieldByName('PR_NBR').AsInteger := PR_NBR;
      PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK_NBR;
      PR_CHECK_SUI.FieldByName('CO_SUI_NBR').AsInteger := CO_SUI_NBR;
      PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').AsFloat := GetGroupValue('PR_CHECK_SUI', 'OVERRIDE_AMOUNT', i);
      PR_CHECK_SUI.Post;
      ctx_DataAccess.PostDataSets([PR_CHECK_SUI]);
    end;  
  end;

  for i := 1 to LocalTaxCount do
  begin
    State := GetGroupValue('PR_CHECK_LOCALS', 'EE_LOCALS_NBR', i);
    if not VarIsNull(State) then
    begin
      LocalName := GetGroupValue('SY_LOCALS', 'NAME', i);

      DM_SYSTEM.SY_STATES.DataRequired('');
      if not DM_SYSTEM.SY_STATES.Locate('STATE', State,[]) then
        raise EevException.Create('SY_STATES_NBR not found.');
      SY_STATES_NBR := DM_SYSTEM.SY_STATES.SY_STATES_NBR.AsInteger;

      DM_SYSTEM.SY_LOCALS.DataRequired('');
      if not DM_SYSTEM.SY_LOCALS.Locate('SY_STATES_NBR;NAME', VarArrayOf([SY_STATES_NBR, LocalName]),[]) then
        raise EevException.Create('Employee #'+DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString+' has no '+
          LocalName+' local setup.');
      SY_LOCALS_NBR := DM_SYSTEM.SY_LOCALS.SY_LOCALS_NBR.AsInteger;

      DM_COMPANY.CO_LOCAL_TAX.DataRequired('');

      if not DM_COMPANY.CO_LOCAL_TAX.Locate('CO_NBR;SY_LOCALS_NBR',VarArrayOf([CO_NBR, SY_LOCALS_NBR]),[]) then
        raise EevException.Create('Employee #'+DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString+' has no '+
          LocalName+' local setup.');

      CO_LOCAL_TAX_NBR := DM_COMPANY.CO_LOCAL_TAX.CO_LOCAL_TAX_NBR.AsInteger;

      DM_COMPANY.EE_LOCALS.DataRequired('');

      if not DM_COMPANY.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR',VarArrayOf([EE_NBR, CO_LOCAL_TAX_NBR]),[]) then
        raise EevException.Create('Employee #'+DM_EMPLOYEE.EE.CUSTOM_EMPLOYEE_NUMBER.AsString+' has no '+
          LocalName+' local setup.');

      EE_LOCALS_NBR := DM_COMPANY.EE_LOCALS.EE_LOCALS_NBR.AsInteger;

      PR_CHECK_LOCALS.DataRequired('1=2');
      PR_CHECK_LOCALS.Insert;
      PR_CHECK_LOCALS.FieldByName('PR_NBR').AsInteger := PR_NBR;
      PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK_NBR;
      PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger := EE_LOCALS_NBR;
      PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := 0;
      PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := GetGroupValue('PR_CHECK_LOCALS', 'OVERRIDE_AMOUNT', i);
      PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value := 0;
      PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
      PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsFloat := GetGroupValue('PR_CHECK_LOCALS', 'OVERRIDE_AMOUNT', i);

      // Reportable
      s := VarToStr(GetGroupValue('PR_CHECK_LOCALS', 'REPORTABLE', i));
      if s <> '' then
        PR_CHECK_LOCALS.FieldByName('REPORTABLE').Value := s
      else
        PR_CHECK_LOCALS.FieldByName('REPORTABLE').Value := 'Y';

      s := VarToStr(GetGroupValue('PR_CHECK_LOCALS', 'NONRES_EE_LOCALS_NBR', i));
      if s <> '' then
      begin
        if not DM_SYSTEM.SY_LOCALS.Locate('SY_STATES_NBR;NAME', VarArrayOf([SY_STATES_NBR, s]),[]) then
          raise EevException.Create(State + ' ' + s + ' not found. EE#'+
            FPayrollChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        SY_LOCALS_NBR := DM_SYSTEM.SY_LOCALS.SY_LOCALS_NBR.AsInteger;

        if not DM_COMPANY.CO_LOCAL_TAX.Locate('CO_NBR;SY_LOCALS_NBR',VarArrayOf([CO_NBR, SY_LOCALS_NBR]),[]) then
          raise EevException.Create(State + ' ' + s + ' is not set up on the company level. EE#'+
            FPayrollChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        CO_LOCAL_TAX_NBR := DM_COMPANY.CO_LOCAL_TAX.CO_LOCAL_TAX_NBR.AsInteger;

        if not DM_COMPANY.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR',VarArrayOf([EE_NBR, CO_LOCAL_TAX_NBR]),[]) then
          raise EevException.Create(State + ' ' + s + ' is not set up for employee #'+
            FPayrollChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);

        PR_CHECK_LOCALS.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger := DM_COMPANY.EE_LOCALS.EE_LOCALS_NBR.AsInteger;
      end;

      // Co Location
      s := VarToStr(GetGroupValue('PR_CHECK_LOCALS', 'CO_LOCATIONS_NBR', i));
      if s <> '' then
      begin
        DM_COMPANY.CO_LOCATIONS.DataRequired('');
        if not DM_COMPANY.CO_LOCATIONS.Locate('CO_NBR;ACCOUNT_NUMBER',VarArrayOf([CO_NBR, s]),[]) then
          raise EevException.Create(s + ' location is not found. EE#'+
            FPayrollChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        PR_CHECK_LOCALS.FieldByName('CO_LOCATIONS_NBR').AsInteger := DM_COMPANY.CO_LOCATIONS.CO_LOCATIONS_NBR.Value;
      end;

      PR_CHECK_LOCALS.Post;
      ctx_DataAccess.PostDataSets([PR_CHECK_LOCALS]);
    end;  
  end;

end;

procedure TEvExchangeYTDPrEngine.AfterImport;
begin
  inherited;

  FreeDataSets;
end;

procedure TEvExchangeYTDPrEngine.FreeDataSets;
begin
  if Assigned(PR) then
    FreeAndNil(PR);
  if Assigned(PR_BATCH) then
    FreeAndNil(PR_BATCH);
  if Assigned(PR_CHECK) then
    FreeAndNil(PR_CHECK);
  if Assigned(PR_CHECK_LINES) then
    FreeAndNil(PR_CHECK_LINES);
  if Assigned(PR_CHECK_STATES) then
    FreeAndNil(PR_CHECK_STATES);
  if Assigned(PR_CHECK_SUI) then
    FreeAndNil(PR_CHECK_SUI);
  if Assigned(PR_CHECK_LOCALS) then
    FreeAndNil(PR_CHECK_LOCALS);

  if Assigned(FCompanies) then
    FreeAndNil(FCompanies);
  if Assigned(FPayrolls) then
    FreeAndNil(FPayrolls);
  if Assigned(FPayrollBatch) then
    FreeAndNil(FPayrollBatch);
  if Assigned(FPayrollChecks) then
    FreeAndNil(FPayrollChecks);
  if Assigned(FCheckLines) then
    FreeAndNil(FCheckLines);
  if Assigned(FCheckStates) then
    FreeAndNil(FCheckStates);
  if Assigned(FCheckLocals) then
    FreeAndNil(FCheckLocals);
  if Assigned(FCheckSuis) then
    FreeAndNil(FCheckSuis);
end;

procedure TEvExchangeYTDPrEngine.DoImport;
type
  TPayroll = record
    CustomCoNbr: Variant;
    CheckDate: Variant;
    PrRunNbr: Variant;
    FirstRecord: integer;
  end;
  TPayrollArray = array of TPayroll;

var
  CustomCoNbrField: String;
  CheckDateField: String;
  PrRunNbrField: String;
  CustomCoNbr: Variant;
  PayrollRecords: TPayrollArray;
  Q: IevQuery;
  i: integer;
  ErrorFound: boolean;

  procedure AssignCustomCoNbr;
  begin
    if FCustomCompanyAssigned then
      CustomCoNbr :=  FInputData.FieldByName(CustomCoNbrField).Value
    else
      CustomCoNbr := FCustomCoNbr;
  end;

  function GetRunNumberValueFromField: Variant;
  begin
    if (PrRunNbrField <> '') and (Trim(VarToStr(FInputData.FieldByName(PrRunNbrField).Value)) <> '') then
      Result := FInputData.FieldByName(PrRunNbrField).Value
    else
      Result := EvoXPrRunNbrDefaultValue;
  end;

begin
  inherited;
  if FErrorsList.Count > 0 then
    exit;

  // import, data sorted by company and check date
  try
    FillListOfMappedTables;
    FImpEngine.StartMappingI(GetMapDefs, FFunctionsDefinitions as IIEDefinition, false);
    try
      if FCustomCompanyAssigned then
        CustomCoNbrField := FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField).GetInputFieldName
      else
        CustomCoNbrField := '';
      CheckDateField := FMapFile.GetFieldByExchangeName(EvoXYTDCheckDate).GetInputFieldName;

      if FMapFile.GetFieldByExchangeName(EvoPrRunNumber) <> nil then
        PrRunNbrField := FMapFile.GetFieldByExchangeName(EvoPrRunNumber).GetInputFieldName
      else
        PrRunNbrField := '';

      BeforeImport;

      // preparing list of payrolls, data has to be sorted by CustomCoNbr, Checkdate and RunNumber
      SetLength(PayrollRecords, 0);
      FInputData.First;
      while not FInputData.eof do
      begin
        CheckCondition(Trim(VarToStr(FInputData.FieldByName(CheckDateField).Value)) <> '', 'Value of Check Date field is empty');

        AssignCustomCoNbr;
        CheckCondition(Trim(VarToStr(CustomCoNbr)) <> '', 'Value of CustomCoNbr field is null');

        if Length(PayrollRecords) = 0 then
        begin
          SetLength(PayrollRecords, Succ(Length(PayrollRecords)));
          PayrollRecords[High(PayrollRecords)].CustomCoNbr := CustomCoNbr;
          PayrollRecords[High(PayrollRecords)].CheckDate := FInputData.FieldByName(CheckDateField).Value;

          PayrollRecords[High(PayrollRecords)].PrRunNbr := GetRunNumberValueFromField;

          PayrollRecords[High(PayrollRecords)].FirstRecord := FInputData.RecNo;
        end
        else
        begin
          if (PayrollRecords[High(PayrollRecords)].CustomCoNbr <> CustomCoNbr) or
            (PayrollRecords[High(PayrollRecords)].CheckDate <> FInputData.FieldByName(CheckDateField).Value) or
            (PayrollRecords[High(PayrollRecords)].PrRunNbr <> GetRunNumberValueFromField) then
          begin
            SetLength(PayrollRecords, Succ(Length(PayrollRecords)));
            PayrollRecords[High(PayrollRecords)].CustomCoNbr := CustomCoNbr;
            PayrollRecords[High(PayrollRecords)].CheckDate := FInputData.FieldByName(CheckDateField).Value;
            PayrollRecords[High(PayrollRecords)].PrRunNbr := GetRunNumberValueFromField;
            PayrollRecords[High(PayrollRecords)].FirstRecord := FInputData.RecNo;
          end;
        end;

        FInputData.Next;
      end;  // while

      // actual import
      for i := Low(PayrollRecords) to High(PayrollRecords) do
      begin
        FInputData.First;
        FInputData.vclDataSet.RecNo := PayrollRecords[i].FirstRecord;
        AssignCustomCoNbr;
        FCustomCoNbr := CustomCoNbr;

        // finding company and open client
        Q := TevQuery.Create('SELECT CL_NBR, CO_NBR FROM TMP_CO WHERE CUSTOM_COMPANY_NUMBER=''' + CustomCoNbr + '''');
        Q.Execute;
        CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CUSTOM_COMPANY_NUMBER=''' + CustomCoNbr + '''');
        ctx_DataAccess.OpenClient(Q.Result.Fields[0].AsInteger);
        FCoNbr := Q.Result.Fields[1].AsInteger;

        // as of date logic

        ErrorFound := false;
        ctx_DataAccess.StartNestedTransaction([dbtClient]);
        try
          while not FInputData.Eof do
          begin
            FPayrollCheckDate := FInputData.FieldByName(CheckDateField).Value;
            FPayrollRunNbr := GetRunNumberValueFromField;
            AssignCustomCoNbr;
            if (PayrollRecords[i].CustomCoNbr <> CustomCoNbr) or (PayrollRecords[i].CheckDate <> FInputData.FieldByName(CheckDateField).Value) or
              (PayrollRecords[i].PrRunNbr <> GetRunNumberValueFromField) then
              break;
            try
              ImportRow;
              Inc(FImportedCount);
            except
              on E: Exception do
              begin
                FErrorsList.AddEvent(etError, FormatErrorMessageForRow(E.Message));
                ErrorFound := true;
              end;
            end;
            FInputData.Next;
          end;  // while;

          if not ErrorFound then
            ctx_DataAccess.CommitNestedTransaction
          else
            ctx_DataAccess.RollbackNestedTransaction;

          AbortIfCurrentThreadTaskTerminated;
        except
          on E: Exception do
          begin
            ctx_DataAccess.RollbackNestedTransaction;
            FErrorsList.AddEvent(etError, FormatErrorMessageForRow(E.Message));
          end;
        end;
      end;  // for
    finally
      FImpEngine.EndMapping;
    end;
  except
    on E: EAbort do
      raise;
    on E:Exception do
      FErrorsList.AddEvent(etFatalError, 'Exception in DOImport procedure. Error: ' + E.Message);
  end;

  // post processing
  try
    AfterImport;
  except
    on E:Exception do
      FErrorsList.AddEvent(etError, 'Exception in AfterImport procedure. Error: ' + E.Message);
  end;

  FImportResult.AddValue(EvoXInputRowsAmount, FRowsCount);
  FImportResult.AddValue(EvoXImportedRowsAmount, FImportedCount);
  FImportResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
  FImportResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
  FImportResult.AddValue(EvoXExtErrorsList, FErrorsList);
end;

function TEvExchangeYTDPrEngine.GetInputDataSortIndexFieldNames: String;
var
  CustomCompanyField: IevEchangeMapField;
  PayrollCheckDateField: IevEchangeMapField;
  PayrollRunNbrField: IevEchangeMapField;
begin
  Result := '';
  PayrollCheckDateField := FMapFile.GetFieldByExchangeName(EvoXYTDCheckDate);
  CheckCondition(Assigned(PayrollCheckDateField), 'Check Date has to be mapped');
  PayrollRunNbrField := FMapFile.GetFieldByExchangeName(EvoPrRunNumber);

  if FCustomCompanyAssigned then
  begin
    CustomCompanyField := FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField);
    Result := CustomCompanyField.GetInputFieldName + ';' + PayrollCheckDateField.GetInputFieldName;
    if PayrollRunNbrField <> nil then
      Result := Result + ';' + PayrollRunNbrField.GetInputFieldName;
  end
  else
    Result := PayrollCheckDateField.GetInputFieldName;
end;

end.

