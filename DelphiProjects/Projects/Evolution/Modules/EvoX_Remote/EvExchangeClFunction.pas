unit EvExchangeClFunction;

interface

uses
  IEMap, EvUtils, SysUtils, EvConsts, db, classes, EvTypes, Variants, isBasicUtils, IsBaseClasses,
  EvCommonInterfaces, EvExchangeBasicFunctions;

type
  TEvExchangeClFunctions = class(TEvExchangeFunctionsBasic, IEvoXDefinition)
  private
  protected
  published
    function CalcClBanksNbr(IsImport: Boolean; const Table,
      Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcClAgencyNbr(IsImport: Boolean; const Table,
      Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
      var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
      out OValues: TVariantArray): Boolean;
    function CalcCLCustomerServiceRep(IsImport: Boolean;
      const Table, Field: string; Value: Variant; var KeyTables,
      KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
      OFields: TStringArray; out OValues: TVariantArray): Boolean;
    function CalcCLFiller41_1(IsImport: Boolean;
      const Table, Field: string; Value: Variant; var KeyTables,
      KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
      OFields: TStringArray; out OValues: TVariantArray): Boolean;
    function CalcClDeliveryGroupNbr(IsImport: Boolean;
      const Table, Field: string; Value: Variant; var KeyTables,
      KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
      OFields: TStringArray; out OValues: TVariantArray): Boolean;
    function CalcPensionClAgencyNbr(IsImport: Boolean;
      const Table, Field: string; Value: Variant; var KeyTables,
      KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
      OFields: TStringArray; out OValues: TVariantArray): Boolean;
    function LimitIntLength(IsImport: Boolean;
      const Table, Field: string; Value: Variant; var KeyTables,
      KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
      OFields: TStringArray; out OValues: TVariantArray): Boolean;
    function AddCLBillingExtraField(IsImport: Boolean;
      const Table, Field: string; Value: Variant; var KeyTables,
      KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
      OFields: TStringArray; out OValues: TVariantArray): Boolean;

  public
  end;

implementation

uses kbmMemTable, EvExchangeConsts, EvExchangeUtils;

{ TEvExchangeClFunctions }

function TEvExchangeClFunctions.CalcClAgencyNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
 SBAgencyNBr : integer;
begin
  Result := false;
  if IsImport then
  begin

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_Agency';
    OFields[0] := 'SB_Agency_NBR';

    CheckCondition(Value <> null, 'Agency Name - Field cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SB_AGENCY'), 'NAME=''' + VarToStr(Value) + '''');
    SBAgencyNBr := ConvertNull(GetImpEngine.IDS('SB_AGENCY').FieldValues['SB_AGENCY_NBR'],0);
    CheckCondition(SBAgencyNBr <> 0, ' not found in SB_AGNECY table. Value: "' + VarToStr(Value) + '"');
    OValues[0] := SBAgencyNBr;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CL_AGENCY', 'SB_AGENCY_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;

function TEvExchangeClFunctions.CalcClDeliveryGroupNbr(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_Agency';
    OFields[0] := 'CL_DELIVERY_GROUP_NBR';

    CheckCondition(Value <> null, 'Delivery Group - Field cannot be null');
    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_DELIVERY_GROUP'), 'DELIVERY_GROUP_DESCRIPTION=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('CL_DELIVERY_GROUP').FieldValues['CL_DELIVERY_GROUP_NBR'];

    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;

function TEvExchangeClFunctions.CalcClBanksNbr(IsImport: Boolean; const Table,
  Field: string; Value: Variant; var KeyTables, KeyFields: TStringArray;
  var KeyValues: TVariantArray; out OTables, OFields: TStringArray;
  out OValues: TVariantArray): Boolean;
var
 SBBanksNBr : integer;
begin
  Result := false;
  if IsImport then
  begin

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_BANK_ACCOUNT';
    OFields[0] := 'SB_BANKS_NBR';

    CheckCondition(Value <> null, 'Bank - Field cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SB_BANKS'), 'NAME=''' + VarToStr(Value) + '''');
    SBBanksNBr := ConvertNull(GetImpEngine.IDS('SB_BANKS').FieldValues['SB_BANKS_NBR'],0);
    CheckCondition(SBBanksNBr <> 0, ' not found in SB_BANKS table. Value: "' + VarToStr(Value) + '"');
    OValues[0] := SBBanksNBr;

    FindAndAddKeyIfNotExists(KeyTables, KeyFields, KeyValues, 'CL_BANK_ACCOUNT', 'SB_BANKS_NBR', OValues[0]);

    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;

function TEvExchangeClFunctions.CalcCLCustomerServiceRep(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin

    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL';
    OFields[0] := 'CUSTOMER_SERVICE_SB_USER_NBR';

    CheckCondition(Value <> null, 'Customer Service Rep - Field cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SB_USER'), 'LAST_NAME=''' + VarToStr(Value) + '''');
    OValues[0] := GetImpEngine.IDS('SB_USER').FieldValues['SB_USER_NBR'];
    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;

function TEvExchangeClFunctions.CalcPensionClAgencyNbr(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  SBAgencyNBr : Variant;
begin
  Result := false;
  if IsImport then
  begin
    //Not Required
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_Pension';
    OFields[0] := 'CL_Agency_NBR';

    CheckCondition(Value <> null, 'Client Agency Name - Field cannot be null');

    GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('SB_AGENCY'), 'NAME=''' + VarToStr(Value) + '''');
    SBAgencyNBr := GetImpEngine.IDS('SB_AGENCY').FieldValues['SB_AGENCY_NBR'];

    if (SBAgencyNBr <> null) then
    begin
      GetImpEngine.CurrentDataRequired(GetImpEngine.IDS('CL_AGENCY'), 'SB_AGENCY_NBR=''' + IntToStr(SBAgencyNBr) + '''');
      OValues[0] := GetImpEngine.IDS('CL_AGENCY').FieldValues['CL_AGENCY_NBR'];
    end;
    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;

function TEvExchangeClFunctions.LimitIntLength(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
 s:string;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_BANK_ACCOUNT';
    OFields[0] := 'NEXT_CHECK_NUMBER';
    s := Value;
    s:= copy(s,1,9);
    OValues[0] := StrToIntDef(s,0);
    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;
function TEvExchangeClFunctions.AddCLBillingExtraField(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := 'CL_BILLING';
    OFields[0] := 'INVOICE_SEPARATELY';
    OValues[0] := 'P';
    Result := true;
  end
  else
    Assert(false, 'It is not export function');
end;

 //Use State Reciprocate Laws on W2
function TEvExchangeClFunctions.CalcCLFiller41_1(IsImport: Boolean;
  const Table, Field: string; Value: Variant; var KeyTables,
  KeyFields: TStringArray; var KeyValues: TVariantArray; out OTables,
  OFields: TStringArray; out OValues: TVariantArray): Boolean;
var
  sValue : string;
begin
  Result := false;
  if IsImport then
  begin
    SetLength(OTables, 1);
    SetLength(OFields, 1);
    SetLength(OValues, 1);
    OTables[0] := Table;
    OFields[0] := Field;

    if VarToStr(Value) = '' then
       Value := 'N';

    sValue :=  putintofiller(sValue,VarToStr(Value),41,1);
    OValues[0] := sValue;
    Result := true;
  end
  else
    Assert(false, 'It is not export function');

end;

end.
