unit pxy_EvoX_Remote;

interface

implementation

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvProxy, EvExchangeConsts, EvMainboard,
  EvTransportInterfaces, evRemoteMethods;

type
  TEvExchangeRemoteEngineProxy = class(TevProxyModule, IevExchangeEngine)
  protected
    function  Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile; const APackage: IevExchangePackage;
      const AOptions: IisStringListRO; const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues;  // input dataset can be changed after import!!!
    function  CheckMapFile(const AMapFile: IEvExchangeMapFile;  const APackage: IEvExchangePackage; const AOptions: IisStringListRO): IisListOfValues;
  end;

function GetRemoteEvoXEngineProxy: IevExchangeEngine;
begin
  Result := TEvExchangeRemoteEngineProxy.Create;
end;

{ TEvExchangeRemoteEngineProxy }

function TEvExchangeRemoteEngineProxy.CheckMapFile(const AMapFile: IEvExchangeMapFile;
  const APackage: IEvExchangePackage; const AOptions: IisStringListRO): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_EVOX_ENGINE_CHECKMAPFILE);
  D.Params.AddValue('Package', APackage);
  D.Params.AddValue('MapFile', AMapFile);
  D.Params.AddValue('Options', AOptions);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TEvExchangeRemoteEngineProxy.Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile;
  const APackage: IevExchangePackage; const AOptions: IisStringListRO; const ACustomCoNbr: String;
  AEffectiveDate: TDateTime): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_EVOX_ENGINE_IMPORT);
  D.Params.AddValue('InputData', AInputData);
  D.Params.AddValue('MapFile', AMapFile);
  D.Params.AddValue('Package', APackage);
  D.Params.AddValue('Options', AOptions);
  D.Params.AddValue('CustomCoNbr', ACustomCoNbr);
  D.Params.AddValue('EffectiveDate', AEffectiveDate);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRemoteEvoXEngineProxy, IevExchangeEngine, 'EvoX Engine Remote Module');

end.
