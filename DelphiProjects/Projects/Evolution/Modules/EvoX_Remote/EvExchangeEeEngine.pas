unit EvExchangeEeEngine;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvUtils,
     EvConsts, EvStreamUtils, EvTypes, isLogFile, EvContext, isBasicUtils, Variants, DB,
     EvEchangeEeFunctions, IEMap, EvExchangeEngine,  EvExchangeDefaultsCalculator,
     EvExchangeResultLog, isThreadManager, EvClientDataSet;

type

  TEvExchangeEeEngine = class(TEvExchangeEngine, IevExchangeEngine)
  private
    FKeyFieldsList: IisStringList;
    FStateMaritalStatus: IisStringList;
    FCoDBDTLevel: Char;

    procedure InitStateMaritalStatus;
  protected
    procedure DoOnConstruction; override;

    function  FormatErrorMessageForRow(const AErrorMessage: String): String; override;
    procedure InitListOfKeyFieldsWithCalculatedDefaults; override;
    function  GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO; override;
    procedure CalculateDefaultsForKeyFields(const ARowValues: IisListOfValues); override;
    procedure AddAdditionalParamsToLog(const ALogRecord: IevExchangeChangeRecord; const APostedParams: IisListOfValues); override;
    function  GetInputDataSortIndexFieldNames: String; override;
    procedure CheckValuesBeforeChange(const ATableName: String; const AOperation: char; const AOldValues: TevClientDataSet; const ANewValues: IisListOfValues); override;

    procedure SetPrimaryRate;
    procedure CheckAutoPayShift;
    procedure CheckALDPercentage;
    procedure DefaultEEHomeState;
    procedure CheckOverrideFedTaxValue;
    procedure CheckOverrideStateTaxValue;
    procedure AddDetaultRate;
    procedure AddTOAandEDs;
    procedure AfterImport; override;

    procedure CheckInputDataWithMap; override;

    procedure FillDataSetWithDefaultValues(const ADataSet: TevClientDataSet; const AStage: TEvEchangeDefaultsStages); override;
    function  ComputeCalculatedFields(const ADataSet: TevClientDataSet): boolean; override;
    procedure DoCheckMapFile; override;
    procedure DoImport; override;
  public
    destructor Destroy; override;
  end;

implementation

uses EvExchangeConsts, EvExchangeEEConsts, EvDataset, isTypes, EvExchangeUtils, SDDClasses, SDataStructure,
  kbmMemTable, ISErrorUtils;

{ TEvExchangeEeEngine }

destructor TEvExchangeEeEngine.Destroy;
begin

  inherited;
end;

procedure TEvExchangeEeEngine.DoOnConstruction;
begin
  inherited;
  FFunctionsDefinitions := TEvExchangeEeFunctions.Create(FImpEngine, FChangeLogList);

  FStateMaritalStatus := TisStringList.Create;
  InitStateMaritalStatus;
end;

procedure TEvExchangeEeEngine.CheckInputDataWithMap;
begin
  inherited;
end;

procedure TEvExchangeEeEngine.DoCheckMapFile;
var
  bFlag: boolean;
  i: integer;
  MapField: IevEchangeMapField;
  FieldsList: IisListOfValues;
  PackageField: IEvExchangePackageField;
  bCanEditTaxExemptions: boolean;
  sField, sRestrictedFields: String;
begin
  inherited;

  try
    // checking for CUSTOM_COMPANY_NUMBER if FCoNbr = -1 and mapped fields not only for CL_PERSON table
    if (not FNotCheckCustomCompanyNbr) and (FCoNbr = -1) then
    begin
      bFlag := false;
      FieldsList := FMapFile.GetFieldsList;
      for i := 0 to FieldsList.Count - 1 do
      begin
        try
          PackageField := FPackage.GetFieldByName(FieldsList.Values[i].Name);
          if not AnsiSameText(PackageField.GetEvTableName, 'CL_PERSON') then
          begin
            bFlag := true;
            break;
          end;
        except
          // skipping errors if package field not found, such errors are processed by ancestor
        end;
      end;

      if bFlag then
      begin
        MapField := FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField);
        if not Assigned(MapField) then
          FErrorsList.AddEvent(etFatalError, 'You should map field for "Custom Company Nbr" package field');
      end;
    end;

    // checking that user has right 'Ability to change Tax Exemptions" for some fields
    FieldsList := FMapFile.GetFieldsList;
    sRestrictedFields := 'EE.FUI_Rate_Credit_Override EE.EXEMPT_EXCLUDE_EE_FED EE.Exempt_Employee_OASDI EE.Exempt_Employer_OASDI' +
      ' EE.Exempt_Employee_Medicare EE.Exempt_Employer_Medicare EE.Exempt_Employer_FUI EE_States.State_Exempt_Exclude' +
      ' EE_States.EE_SDI_Exempt_Exclude EE_States.EE_SUI_Exempt_Exclude EE_States.ER_SDI_Exempt_Exclude EE_States.ER_SUI_Exempt_Exclude' +
      ' EE_Locals.Exempt_Exclude';
    sRestrictedFields := UpperCase(sRestrictedFields);
    bCanEditTaxExemptions := ctx_AccountRights.Functions.GetState('USER_CAN_EDIT_TAX_EXEMPTIONS') = stEnabled;
    for i := 0 to FieldsList.Count - 1 do
    begin
      MapField := IInterface(FieldsList.Values[i].Value) as IevEchangeMapField;
      try
        PackageField := IInterface(FPackage.GetFieldsList.Value[MapField.GetExchangeFieldName]) as IEvExchangePackageField;
      except
      end;
      if Assigned(PackageField) then
      begin
        sField := UpperCase(PackageField.GetEvTableName + '.' + PackageField.GetEvFieldName);
        if (Pos(sField, sRestrictedFields) > 0) and (not bCanEditTaxExemptions) then
          FErrorsList.AddEvent(etFatalError, 'EvoExchange field "' + MapField.GetExchangeFieldName + '". Import to this field is not allowed because user has no security rights. "Ability to change Tax Exemptions" function is disabled');
      end;
    end;

  finally
    FCheckMapResult.Value[EvoXAllMessagesAmount] := FErrorsList.Count;
    FCheckMapResult.Value[EvoXRealErrorAmount] := FErrorsList.GetAmountOfErrors;
  end;
end;

function TEvExchangeEeEngine.FormatErrorMessageForRow(const AErrorMessage: String): String;
var
  MapField: IevEchangeMapField;
  InputValue: Variant;
begin
  Result := 'Error: ' + StringReplace(AErrorMessage, #13#10, '  ', [rfReplaceAll]);
  MapField := FMapFile.GetFieldByExchangeName(EECodeFieldName);
  if Assigned(MapField) then
    try
      InputValue := FInputData.vclDataSet.Fields.FieldByName(MapField.GetInputFieldName).Value;
      if InputValue <> null then
        Result := 'EECode "' + VarToStr(InputValue) + '" ' + Result;
    except
    end;
  if FCustomCoNbr <> '' then
    Result := 'CustomCo "' + FCustomCoNbr + '" ' + Result;
  Result := 'Row#' + IntToStr(FInputData.vclDataSet.RecNo)  + ' ' + Result;
end;

procedure TEvExchangeEeEngine.FillDataSetWithDefaultValues(const ADataSet: TevClientDataSet; const AStage: TEvEchangeDefaultsStages);
begin
  if AStage = EvoxStBefore then  // before mapped values are posted
    FDefaultsCalculator.FillTableBefore(FCoNbr, ADataSet)
  else if AStage = EvoxStAfter then  // after mapped values are posted
    FDefaultsCalculator.FillTableAfter(FCoNbr, ADataSet)
  else
    raise Exception.Create('Unknown stage: "' + IntToStr(Integer(AStage)) + '"');
end;

procedure TEvExchangeEeEngine.CalculateDefaultsForKeyFields(const ARowValues: IisListOfValues);
var
  Value: Variant;
  PackageField: IEvExchangePackageField;
  GroupRecordNumbers: TisDynIntegerArray;
  i: integer;

  function CalculateStateMaritalStatus: Variant;
  var
    S: String;
    i: integer;
  begin
    Result := null;
    if ARowValues.ValueExists(CompanyHomeStatePackageField) then
      if ARowValues.Value[CompanyHomeStatePackageField] <> null then
      begin
        S := Trim(UpperCase(ARowValues.Value[CompanyHomeStatePackageField]));
        i := FStateMaritalStatus.IndexOfName(S);
        CheckCondition(i <> -1, 'Cannot find state in list of states with default marital statuses: "' + S +
          '". You have to map field "' + StateMaritalStatusPackageField + '"');
        Result := FStateMaritalStatus.ValueFromIndex[i];
      end;
  end;

  function AutoIncrementCustomEENumber:variant;
    var
      maxcen:integer;
      QCo, QEe: IevQuery;
  begin
      result:=null;
      QCo := TevQuery.Create('SELECT AUTO_INCREMENT FROM CO WHERE {AsOfNow<CO>} AND CO_NBR=' + IntToStr(FCoNbr));
      QCo.Execute;
      CheckCondition(QCo.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(FCoNbr));

      if QCo.Result.Fields[0].Value <> null then
       if QCo.Result.Fields[0].AsInteger > 0 then
       begin
          QEe := TevQuery.Create('SELECT CUSTOM_EMPLOYEE_NUMBER FROM EE WHERE {AsOfNow<EE>} AND CO_NBR=' + IntToStr(FCoNbr) +
                                  ' order by CUSTOM_EMPLOYEE_NUMBER ');
          QEe.Execute;
          maxcen := 0;
          QEe.Result.Last;
          if QEe.Result.Fields[0].Value <> null then
             maxcen := strtoint(QEe.Result.Fields[0].Value);
          result := maxcen + QCo.Result.Fields[0].AsInteger;
       end;
  end;

begin
  if AnsiSameText(Fpackage.GetName, EvoXEBasicEEPackage) then
  begin
    // State Marital Status
    if not ARowValues.ValueExists(StateMaritalStatusPackageField) then
    begin
      Value := CalculateStateMaritalStatus;
      if Value <> null then  // if value is null, it's ok. This mean that "CompanyHomeStatePackageField" is not mapped, what is allowed, if you are not importing to EE_STATES
        ARowValues.AddValue(StateMaritalStatusPackageField, Value);
    end;
    
    // Custom EE Number
    if ARowValues.ValueExists(CustomEENbrPackageField) and
       (Trim(ARowValues.Value[CustomEENbrPackageField]) = '-1') then
    begin
      Value := AutoIncrementCustomEENumber;
      ARowValues.AddValue(CustomEENbrPackageField, PreFormatOfInputData('ConvertToCustomEENumber', Value));
    end;

    // Rate Numbers
    PackageField := FPackage.GetFieldByName(RateNumberPackageField);
    GroupRecordNumbers := FPackage.GetGroupRecordNumbers(PackageField.GetGroupName);
    for i := Low(GroupRecordNumbers) to High(GroupRecordNumbers) do
    begin
      if not ARowValues.ValueExists(RateNumberPackageField + ' ' + RecordNumberToStr(GroupRecordNumbers[i])) then
        ARowValues.AddValue(RateNumberPackageField + ' ' + RecordNumberToStr(GroupRecordNumbers[i]),
          GroupRecordNumbers[i]);
    end;
  end;  
end;

function TEvExchangeEeEngine.GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO;
begin
  Result := FKeyFieldsList as IisStringListRO;
end;

procedure TEvExchangeEeEngine.InitStateMaritalStatus;
begin
  FStateMaritalStatus.Add('AK=S');
  FStateMaritalStatus.Add('AL=S');
  FStateMaritalStatus.Add('AR=A');
  FStateMaritalStatus.Add('AZ=37');
  FStateMaritalStatus.Add('CA=SM');
  FStateMaritalStatus.Add('CO=S');
  FStateMaritalStatus.Add('CT=A');
  FStateMaritalStatus.Add('DC=S');
  FStateMaritalStatus.Add('DE=S');
  FStateMaritalStatus.Add('FL=S');
  FStateMaritalStatus.Add('GA=A');
  FStateMaritalStatus.Add('GU=S');
  FStateMaritalStatus.Add('HI=S');
  FStateMaritalStatus.Add('IA=S');
  FStateMaritalStatus.Add('ID=S');
  FStateMaritalStatus.Add('IL=S');
  FStateMaritalStatus.Add('IN=0');
  FStateMaritalStatus.Add('KS=S');
  FStateMaritalStatus.Add('KY=S');
  FStateMaritalStatus.Add('LA=0');
  FStateMaritalStatus.Add('MA=0');
  FStateMaritalStatus.Add('MD=1');
  FStateMaritalStatus.Add('ME=S');
  FStateMaritalStatus.Add('MI=SM');
  FStateMaritalStatus.Add('MN=S');
  FStateMaritalStatus.Add('MO=S');
  FStateMaritalStatus.Add('MS=S0');
  FStateMaritalStatus.Add('MT=S');
  FStateMaritalStatus.Add('NC=S');
  FStateMaritalStatus.Add('ND=S');
  FStateMaritalStatus.Add('NE=S');
  FStateMaritalStatus.Add('NH=S');
  FStateMaritalStatus.Add('NJ=A');
  FStateMaritalStatus.Add('NM=S');
  FStateMaritalStatus.Add('NV=S');
  FStateMaritalStatus.Add('NY=S');
  FStateMaritalStatus.Add('OH=SM');
  FStateMaritalStatus.Add('OK=S');
  FStateMaritalStatus.Add('OR=S');
  FStateMaritalStatus.Add('PA=SM');
  FStateMaritalStatus.Add('PR=S');
  FStateMaritalStatus.Add('RI=S');
  FStateMaritalStatus.Add('SC=0');
  FStateMaritalStatus.Add('SD=S');
  FStateMaritalStatus.Add('TN=S');
  FStateMaritalStatus.Add('TX=S');
  FStateMaritalStatus.Add('UT=S');
  FStateMaritalStatus.Add('VA=SM');
  FStateMaritalStatus.Add('VI=S');
  FStateMaritalStatus.Add('VT=S');
  FStateMaritalStatus.Add('WA=S');
  FStateMaritalStatus.Add('WI=S');
  FStateMaritalStatus.Add('WV=S');
  FStateMaritalStatus.Add('WY=S');
end;

procedure TEvExchangeEeEngine.InitListOfKeyFieldsWithCalculatedDefaults;
var
  i: integer;
  PackageField: IEvExchangePackageField;
begin
  FKeyFieldsList := TisStringList.Create;

  if AnsiSameText(Fpackage.GetName, EvoXEBasicEEPackage) then
  begin
    FkeyFieldsList.Add(StateMaritalStatusPackageField);

    // adding goup record fields
    for i := 0 to FPackage.GetFieldsList.COunt - 1 do
    begin
      PackageField := IInterface(FPackage.GetFieldsList.Values[i].Value) as IEvExchangePackageField;
      // Rate Number
      if PackageField.IsBelongsToGroup and (not PackageField.IsGroupDefinitionField) and
        (PackageField.GetDefExchangeFieldName = RateNumberPackageField) then
        FKeyFieldsList.Add(PackageField.GetExchangeFieldName);
    end;
  end;  
end;

procedure TEvExchangeEeEngine.AfterImport;
var
  oldClientNbr: integer;

begin
  inherited;

  if FImportedCount > 0 then
  begin
    oldClientNbr := ctx_DataAccess.ClientId;
    try
      // setting up Primary_Rate for the lowerest Rate_Number
      try
        SetPrimaryRate;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot set up primary rates. Error: ' + E.Message + #13#10 + GetStack);
      end;

      // checking Auto Shift
      try
        CheckAutoPayShift;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot check Auto Shifts. Error: ' + E.Message + #13#10 + GetStack);
      end;

      // checking that sum of ALD percentage is 100%
      try
        CheckALDPercentage;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot check sum of ALD percentages. Error: ' + E.Message + #13#10 + GetStack);
      end;

      // defaulting EE Home State Field
      try
        DefaultEEHomeState;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot default EE Home States. Error: ' + E.Message + #13#10 + GetStack);
      end;

      // checking rule for OVERRIDE_FED_TAX_TYPE and OVERRIDE_FED_TAX_VALUE
      try
        CheckOverrideFedTaxValue;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot check OVERRIDE_FED_TAX_TYPEs and OVERRIDE_FED_TAX_VALUEs. Error: ' + E.Message + #13#10 + GetStack);
      end;

      // adding Rate for new employees if not exists
      try
        AddDetaultRate;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot add Rate for new employee. Error: ' + E.Message + #13#10 + GetStack);
      end;

      // adding TOA and EDs for new EEs
      try
        AddTOAandEDs;
      except
        on E:Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot add TOA and EDs for new employee. Error: ' + E.Message + #13#10 + GetStack);
      end;
    finally
      ctx_DataAccess.OpenClient(oldClientNbr);
    end;
  end;
end;

procedure TEvExchangeEeEngine.SetPrimaryRate;
var
  prevClNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  bFirstRecord: boolean;
  ListOfEE: IevDataSet;
  dsEERates: TevClientDataSet;
  SortIndex, RatesIndex: String;
  Tbl: TClass;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // New EEs
    if (LogRecord.GetOperation = 'I') and (LogRecord.GetTableName = 'EE') then
    begin
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] := LogRecord.GetRecordNbr;
        ListOfEE.Post;
      end;
    end;

    // New EE_RATES with filled up EE_NBR
    if (LogRecord.GetOperation = 'I') and (LogRecord.GetTableName = 'EE_RATES') and
      (LogRecord.GetAdditionalParams.ValueExists('EE_NBR')) then
    begin
      // checking that it does not exists
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetAdditionalParams.Value['EE_NBR']]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] := LogRecord.GetAdditionalParams.Value['EE_NBR'];
        ListOfEE.Post;
      end;
    end;
  end; // for i
  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    // setting Primary Rates
    Tbl := GetClass('TEE_RATES');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE_RATES');
    dsEERates := TddTableClass(Tbl).Create(nil);
    try
      dsEERates.SkipFieldCheck := True;
      dsEERates.ProviderName := 'EE_RATES_PROV';

      prevClNbr := -1;
      ListOfEE.First;
      while not ListOfEE.Eof do
      begin
        // opening CLient
        if ListOfEE['ClNbr'] <> prevClNbr then
        begin
          prevClNbr := ListOfEE['ClNbr'];
          ctx_DataAccess.OpenClient(prevClNbr);
        end;

        dsEERates.Active := false;

        dsEERates.DataRequired('EE_NBR=' + IntToStr(ListOfEE['EENbr']));
        if not dsEERates.Locate('PRIMARY_RATE', 'Y', []) then
        begin
          try
            RatesIndex := RandomString(10);
            dsEERates.AddIndex(RatesIndex, 'RATE_NUMBER', []);
            dsEERates.IndexDefs.Update;
            dsEERates.IndexName := RatesIndex;
            try
              bFirstRecord := true;
              dsEERates.First;
              while not dsEERates.eof do
              begin
                dsEERates.Edit;
                try
                  if bFirstRecord then
                  begin
                    bFirstRecord := false;
                    if dsEERates['PRIMARY_RATE'] <> 'Y' then
                      dsEERates['PRIMARY_RATE'] := 'Y';
                  end
                  else
                  begin
                    if dsEERates['PRIMARY_RATE'] <> 'N' then
                      dsEERates['PRIMARY_RATE'] := 'N';
                  end;
                  dsEERates.Post;
                except
                  on E: Exception do
                  begin
                    dsEERates.Cancel;
                    raise;
                  end;
                end;

                dsEERates.Next;
              end;  // while
            finally
              dsEERates.IndexName := '';
              dsEERates.DeleteIndex(RatesIndex);
              dsEERates.IndexDefs.Update;
            end;

            ctx_DataAccess.StartNestedTransaction([dbtClient]);
            try
              ctx_DataAccess.PostDataSets([dsEERates]);
              ctx_DataAccess.CommitNestedTransaction;
            except
              on E : Exception do
              begin
                ctx_DataAccess.RollbackNestedTransaction;
                raise;
              end;
            end;
          except
            on EE: Exception do
              FErrorsList.AddEvent(etError, 'After import processing. Cannot set up primary rate. EE_NBR=' + IntTOSTr(ListOfEE['EENbr'])
                + '.  Error: ' + EE.Message + #13#10 + GetStack);
          end;
        end;  // if

        ListOfEE.Next;
      end;
    finally
      dsEERates.Free;
    end;
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.AddAdditionalParamsToLog(const ALogRecord: IevExchangeChangeRecord;
  const APostedParams: IisListOfValues);
begin
  inherited;

  // adding EE_NBR
  if (ALogRecord.GetTableName = 'EE_RATES') and (ALogRecord.GetOperation = 'I') then
  begin
    if ApostedParams.ValueExists('EE_NBR') then
      ALogRecord.GetAdditionalParams.AddValue('EE_NBR', (IInterface(APostedParams.Value['EE_NBR']) as IEMapValue).Value);
  end;

  // adding AUTOPAY_CO_SHIFTS_NBR
  if (ALogRecord.GetTableName = 'EE') and (ALogRecord.GetOperation = 'U') then
  begin
    if ApostedParams.ValueExists('AUTOPAY_CO_SHIFTS_NBR') then
      ALogRecord.GetAdditionalParams.AddValue('AUTOPAY_CO_SHIFTS_NBR', (IInterface(APostedParams.Value['AUTOPAY_CO_SHIFTS_NBR']) as IEMapValue).Value);
  end;

  // adding EE_NBR and CO_NBR for EE_AUTOLABOR_DISTRIBUTION table
  if (ALogRecord.GetTableName = 'EE_AUTOLABOR_DISTRIBUTION') and
    ((ALogRecord.GetOperation = 'I') or (ALogRecord.GetOperation = 'U')) then
  begin
    if ApostedParams.ValueExists('EE_NBR') then
      ALogRecord.GetAdditionalParams.AddValue('EE_NBR', (IInterface(APostedParams.Value['EE_NBR']) as IEMapValue).Value);
  end;

  // adding OVERRIDE_FED_TAX_TYPE for check of OVERRIDE_FED_TAX_VALUE
  if (ALogRecord.GetTableName = 'EE') and (APostedParams.ValueExists('OVERRIDE_FED_TAX_TYPE'))
    and (((IInterface(APostedParams.Value['OVERRIDE_FED_TAX_TYPE']) as IEMapValue).Value) <> null) then
    ALogRecord.GetAdditionalParams.AddValue('OVERRIDE_FED_TAX_TYPE', (IInterface(APostedParams.Value['OVERRIDE_FED_TAX_TYPE']) as IEMapValue).Value)
  else if (ALogRecord.GetOperation = 'U') and (ALogRecord.GetTableName = 'EE')
    and (APostedParams.ValueExists('OVERRIDE_FED_TAX_VALUE'))
    and ((IInterface(APostedParams.Value['OVERRIDE_FED_TAX_VALUE']) as IEMapValue).Value = null) then
    ALogRecord.GetAdditionalParams.AddValue('OVERRIDE_FED_TAX_VALUE', (IInterface(APostedParams.Value['OVERRIDE_FED_TAX_VALUE']) as IEMapValue).Value);

  // adding OVERRIDE_STATE_TAX_TYPE for check of OVERRIDE_STATE_TAX_VALUE
  if (ALogRecord.GetTableName = 'EE_STATES') and (APostedParams.ValueExists('OVERRIDE_STATE_TAX_TYPE'))
    and (((IInterface(APostedParams.Value['OVERRIDE_STATE_TAX_TYPE']) as IEMapValue).Value) <> null) then
    ALogRecord.GetAdditionalParams.AddValue('OVERRIDE_STATE_TAX_TYPE', (IInterface(APostedParams.Value['OVERRIDE_STATE_TAX_TYPE']) as IEMapValue).Value)
  else if (ALogRecord.GetOperation = 'U') and (ALogRecord.GetTableName = 'EE_STATES')
    and (APostedParams.ValueExists('OVERRIDE_STATE_TAX_VALUE'))
    and ((IInterface(APostedParams.Value['OVERRIDE_STATE_TAX_VALUE']) as IEMapValue).Value = null) then
    ALogRecord.GetAdditionalParams.AddValue('OVERRIDE_STATE_TAX_VALUE', (IInterface(APostedParams.Value['OVERRIDE_STATE_TAX_VALUE']) as IEMapValue).Value);

  // CO_NBR for new Employee
  if (ALogRecord.GetTableName = 'EE') and (ALogRecord.GetOperation = 'I') then
    ALogRecord.GetAdditionalParams.AddValue('CO_NBR', (IInterface(APostedParams.Value['CO_NBR']) as IEMapValue).Value);
end;

procedure TEvExchangeEeEngine.CheckAutoPayShift;
var
  prevClNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEE: IevDataSet;
  dsEE: TevClientDataSet;
  SortIndex: String;
  Q: IevQuery;
  Tbl: TClass;
  bFound: boolean;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // Updated EE with filled up AUTOPAY_CO_SHIFTS_NBR
    if (LogRecord.GetOperation = 'U') and (LogRecord.GetTableName = 'EE') and
      (LogRecord.GetAdditionalParams.ValueExists('AUTOPAY_CO_SHIFTS_NBR')) then
    begin
      // checking that it does not exists
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] :=  LogRecord.GetRecordNbr;
        ListOfEE.Post;
      end;
    end;
  end; // for i
  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    // checking that Auto SHift is peresented in EE_WORK_SHIFTS
    Tbl := GetClass('TEE');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE');
    dsEE := TddTableClass(Tbl).Create(nil);
    try
      dsEE.SkipFieldCheck := True;
      dsEE.ProviderName := 'EE_PROV';

      prevClNbr := -1;
      ListOfEE.First;
      while not ListOfEE.Eof do
      begin
        // opening CLient
        if ListOfEE['ClNbr'] <> prevClNbr then
        begin
          prevClNbr := ListOfEE['ClNbr'];
          ctx_DataAccess.OpenClient(prevClNbr);
          dsEE.Active := false;
          dsEE.DataRequired();
          dsEE.SortOn('EE_NBR', []);
        end;

        try
          bFound := dsEE.Locate('EE_NBR', ListOfEE['EENbr'], []);
          CheckCOndition(bFound, 'Cannot find record in EE table. EE_NBR=' + IntToSTr(ListOfEE['EENbr']));
          if dsEE['AUTOPAY_CO_SHIFTS_NBR'] <> null then
          begin
            Q := TevQuery.Create('SELECT CO_SHIFTS_NBR FROM EE_WORK_SHIFTS a WHERE {AsOfNow<a>} AND a.CO_SHIFTS_NBR=' + IntToStr(dsEE['AUTOPAY_CO_SHIFTS_NBR']));
            if Q.Result.RecordCount = 0 then
            begin
              FErrorsList.AddEvent(etWarning, '"EE Shift - Auto Shift" is out of list of possible values in EE_WORK_SHIFTS table. Value is ' + IntToStr(dsEE['AUTOPAY_CO_SHIFTS_NBR']) +
                '. It will be cleared');
              dsEE.Edit;
              try
                dsEE['AUTOPAY_CO_SHIFTS_NBR'] := null;
                dsEE.Post;
              except
                on E : Exception do
                begin
                  dsEE.Cancel;
                  raise;
                end;
              end;

              ctx_DataAccess.StartNestedTransaction([dbtClient]);
              try
                ctx_DataAccess.PostDataSets([dsEE]);
                ctx_DataAccess.CommitNestedTransaction;
              except
                on E : Exception do
                begin
                  ctx_DataAccess.RollbackNestedTransaction;
                  raise;
                end;
              end;  // try for commit
            end;  // if Q.Result.RecordCount = 0
          end;  // if dsEE['AUTOPAY_CO_SHIFTS_NBR'] <> null
        except
          on EE: Exception do
            FErrorsList.AddEvent(etError, 'After import processing. Cannot check auto-pay shift. EE_NBR=' + IntToStr(ListOfEE['EENbr'])
              + '.  Error: ' + EE.Message + #13#10 + GetStack);
        end;

        ListOfEE.Next;
      end;
    finally
      dsEE.Free;
    end;
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.CheckALDPercentage;
const
  eps: double = 1e-3; //gdy
var
  prevClNbr, CoNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEE: IevDataSet;
  SortIndex: String;
  Q: IevQuery;
  PercTotal: Double;
  CustomCo, CustomEE: String;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // New or Updated records in EE_AUTOLABOR_DISTRIBUTION
    if ((LogRecord.GetOperation = 'I') or (LogRecord.GetOperation = 'U'))
      and (LogRecord.GetTableName = 'EE_AUTOLABOR_DISTRIBUTION') then
    begin
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr,
        LogRecord.GetAdditionalParams.Value['EE_NBR']]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] := LogRecord.GetAdditionalParams.Value['EE_NBR'];
        ListOfEE.Post;
      end;
    end;
  end; // for i
  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    // checking that SUM(percentage) = 100%
    prevClNbr := -1;
    ListOfEE.First;
    while not ListOfEE.Eof do
    begin
      // opening CLient
      if ListOfEE['ClNbr'] <> prevClNbr then
      begin
        prevClNbr := ListOfEE['ClNbr'];
        ctx_DataAccess.OpenClient(prevClNbr);
      end;

      try
        Q := TevQuery.Create('select SUM(PERCENTAGE) from EE_AUTOLABOR_DISTRIBUTION a where {AsOfNow<a>} and a.EE_NBR=' +
          IntToStr(ListOfEE['EENbr']));
        Q.Execute;
        if Q.Result.Fields[0].Value <> null then
        begin
          PercTotal := RoundAll(Q.Result.Fields[0].AsFloat, 6);
          if (abs(PercTotal) > eps) and (abs(PercTotal - 100.0) > eps) then
          begin
            Q := TEvQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, CO_NBR from EE a where {AsOfNow<a>} and a.EE_NBR=' +
              IntToStr(ListOfEE['EENbr']));
            Q.Execute;
            CustomEE := Q.result.Fields[0].AsString;
            CoNbr := Q.result.Fields[1].AsInteger;
            Q := TEvQuery.Create('select CUSTOM_COMPANY_NUMBER from CO a where {AsOfNow<a>} and a.CO_NBR=' +
              IntToStr(CoNbr));
            Q.Execute;
            CustomCo := Q.result.Fields[0].AsString;
            FErrorsList.AddEvent(etWarning, 'CustomCo "' + CustomCo + '" EECode "' + CustomEE + '"' +
              ' ALD should equal 100%. Add ' + FloatToStr(RoundAll(100.0 - PercTotal, 6)) + '%.' +
              ' Record was successfully imported, but you should update the record accordingly.');
          end;
        end;
      except
        on EE: Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot check ALD percentage. EE_NBR=' + IntToStr(ListOfEE['EENbr'])
            + '.  Error: ' + EE.Message + #13#10 + GetStack);
      end;

      ListOfEE.Next;
    end;
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.DefaultEEHomeState;
var
  prevClNbr: integer;
  i: integer;
  index: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEE: IevDataSet;
  dsEE, dsEEStates: TevClientDataSet;
  Tbl: TClass;
  SortIndex: String;
  Q: IevQuery;
  CoHomeState: Variant;
  S: String;
  oldCoNbr: integer;
  NewEStatesNbr: Variant;
  bFound: boolean;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('CoStatesNbr', ftInteger, 0, false);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // New EEs
    if (LogRecord.GetOperation = 'I') and (LogRecord.GetTableName = 'EE') then
    begin
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] := LogRecord.GetRecordNbr;
        if LogRecord.GetAdditionalParams.ValueExists('CO_STATES_NBR') then
          ListOfEE['CoStatesNbr'] := (IInterface(LogRecord.GetAdditionalParams.Value['CO_STATES_NBR']) as IEMapValue).Value;
        ListOfEE.Post;
      end;
    end;
  end; // for i
  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    // setting EE Home State
    Tbl := GetClass('TEE');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE');
    dsEE := TddTableClass(Tbl).Create(nil);
    try
      dsEE.SkipFieldCheck := True;
      dsEE.ProviderName := 'EE_PROV';

      prevClNbr := -1;
      ListOfEE.First;
      while not ListOfEE.Eof do
      begin
        // opening CLient
        if ListOfEE['ClNbr'] <> prevClNbr then
        begin
          prevClNbr := ListOfEE['ClNbr'];
          ctx_DataAccess.OpenClient(prevClNbr);
          dsEE.Active := false;
          dsEE.DataRequired();
          dsEE.SortOn('EE_NBR', []);
        end;

        try
          bFound := dsEE.Locate('EE_NBR', ListOfEE['EENbr'], []);
          CheckCondition(bFound, 'Cannot find record in EE table. EE_NBR=' + IntToStr(ListOfEE['EENbr']));
          if dsEE['HOME_TAX_EE_STATES_NBR'] = null then
          begin
            if ListOfEE['CoStatesNbr'] = null then
            begin
              Q := TevQuery.Create('SELECT a.HOME_CO_STATES_NBR FROM CO a WHERE {AsOfNow<a>} AND a.CO_NBR=:P_CO_NBR');
              Q.Params.AddValue('P_CO_NBR', dsEE['CO_NBR']);
              Q.Execute;
              CheckCondition(Q.Result.RecordCount > 0, 'Cannot find company. CO_NBR=' + VarToStr(dsEE['CO_NBR']));
              CoHomeState := Q.Result.Fields[0].Value;
            end
            else
              CoHomeState := ListOfEE['CoStatesNbr'];

            if CoHomeState <> null then
            begin
              ctx_DataAccess.StartNestedTransaction([dbtClient]);
              try
                Q := TevQuery.Create('SELECT a.EE_STATES_NBR FROM EE_STATES a WHERE {AsOfNow<a>} AND a.EE_NBR=:P_EE_NBR AND a.CO_STATES_NBR=:P_CO_STATES_NBR');
                Q.Params.AddValue('P_EE_NBR', ListOfEE['EENbr']);
                Q.Params.AddValue('P_CO_STATES_NBR', CoHomeState);
                Q.Execute;

                if Q.Result.RecordCount = 0 then
                begin
                  // we have to add default state record to EE_STATES table
                  Tbl := GetClass('TEE_STATES');
                  CheckCondition(Assigned(Tbl), 'Cannot get class: TEE_STATES');
                  dsEEStates := TddTableClass(Tbl).Create(nil);
                  try
                    dsEEStates.SkipFieldCheck := True;
                    dsEEStates.ProviderName := 'EE_STATES_PROV';

                    dsEEStates.Active := false;
                    dsEEStates.DataRequired('1=2');

                    dsEEStates.Append;
                    dsEEStates['EE_NBR'] := ListOfEE['EENbr'];
                    dsEEStates['CO_STATES_NBR'] := CoHomeState;

                    Q := TevQuery.Create('SELECT STATE FROM CO_STATES a WHERE {AsOfNow<a>} AND a.CO_STATES_NBR=:P_CO_STATES_NBR');
                    Q.Params.AddValue('P_CO_STATES_NBR', CoHomeState);
                    Q.Execute;
                    CheckCondition(Q.Result.RecordCount > 0, 'Cannot find state in CO_STATES table. CO_STATES_NBR=' + VarToStr(CoHomeState));
                    S := VarToStr(Q.Result['STATE']);
                    index := FStateMaritalStatus.IndexOfName(S);
                    CheckCondition(index <> -1, 'Cannot find state in list: "' + S + '"');
                    dsEEStates['STATE_MARITAL_STATUS'] := FStateMaritalStatus.ValueFromIndex[index];

                    Q := TevQuery.Create('SELECT SY_STATE_MARITAL_STATUS_NBR FROM SY_STATE_MARITAL_STATUS a WHERE {AsOfNow<a>} AND a.STATUS_TYPE=:P_STATUS_TYPE');
                    S := PadStringRight(FStateMaritalStatus.ValueFromIndex[index], ' ', 2);
                    Q.Params.AddValue('P_STATUS_TYPE', S);
                    Q.Execute;
                    CheckCondition(Q.Result.RecordCount > 0, 'Cannot find SY_STATE_MARITAL_STATUS_NBR in SY_STATE_MARITAL_STATUS table. STATUS_TYPE="' + S + '"');
                    dsEEStates['SY_STATE_MARITAL_STATUS_NBR'] := Q.Result['SY_STATE_MARITAL_STATUS_NBR'];

                    oldCoNbr := FCoNbr;
                    try
                      FCoNbr := dsEE['CO_NBR'];
                      FillDataSetWithDefaultValues(dsEEStates, EvoxStBefore);
                    finally
                      FCoNbr := oldCoNbr;
                    end;
                    dsEEStates.Post;

                    ctx_DataAccess.PostDataSets([dsEEStates]);

                    Q := TevQuery.Create('SELECT EE_STATES_NBR FROM EE_STATES a WHERE {AsOfNow<a>} AND a.EE_NBR=:P_EE_NBR AND a.CO_STATES_NBR=:P_CO_STATES_NBR');
                    Q.Params.AddValue('P_EE_NBR', ListOfEE['EENbr']);
                    Q.Params.AddValue('P_CO_STATES_NBR', CoHomeState);
                    Q.Execute;
                    CheckCondition(Q.Result.RecordCount > 0, 'Cannot find state in EE_STATES table. EE_NBR=' + VarToStr(ListOfEE['EENbr']) +
                      ' CO_STATES_NBR=' + VarToStr(CoHomeState));
                    NewEStatesNbr := Q.Result['EE_STATES_NBR'];
                  finally
                    FreeAndNil(dsEEStates);
                  end;
                end
                else
                  NewEStatesNbr := Q.Result['EE_STATES_NBR'];

                dsEE.Edit;
                try
                  dsEE['HOME_TAX_EE_STATES_NBR'] := NewEStatesNbr;
                  dsEE.Post;
                except
                  on E: Exception do
                  begin
                    dsEE.Cancel;
                    raise;
                  end;
                end;

                ctx_DataAccess.PostDataSets([dsEE]);

                ctx_DataAccess.CommitNestedTransaction;
              except
                on E : Exception do
                begin
                  ctx_DataAccess.RollbackNestedTransaction;
                  raise;
                end;
              end;  // try
            end; // company has default state
          end;  // EE Home State is null
        except
          on EE: Exception do
            FErrorsList.AddEvent(etError, 'After import processing. Cannot set default home state. EE_NBR=' + IntToStr(ListOfEE['EENbr'])
              + '.  Error: ' + EE.Message + #13#10 + GetStack);
        end;

        ListOfEE.Next;
      end;  // while

    finally
      FreeAndNil(dsEE);
    end;
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.CheckOverrideFedTaxValue;
var
  prevClNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEE: IevDataSet;
  dsEE: TevClientDataSet;
  SortIndex: String;
  Tbl: TClass;
  bFound: boolean;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // Inserts or Updated EE with filled up OVERRIDE_FED_TAX_TYPE or OVERRIDE_FED_TAX_VALUE
    if (LogRecord.GetTableName = 'EE') and
      (LogRecord.GetAdditionalParams.ValueExists('OVERRIDE_FED_TAX_TYPE') or LogRecord.GetAdditionalParams.ValueExists('OVERRIDE_FED_TAX_VALUE')) then
    begin
      // checking that it does not exists
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] :=  LogRecord.GetRecordNbr;
        ListOfEE.Post;
      end;
    end;
  end; // for i
  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    Tbl := GetClass('TEE');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE');
    dsEE := TddTableClass(Tbl).Create(nil);
    try
      dsEE.SkipFieldCheck := True;
      dsEE.ProviderName := 'EE_PROV';

      prevClNbr := -1;
      ListOfEE.First;
      while not ListOfEE.Eof do
      begin
        // opening CLient
        if ListOfEE['ClNbr'] <> prevClNbr then
        begin
          prevClNbr := ListOfEE['ClNbr'];
          ctx_DataAccess.OpenClient(prevClNbr);
          dsEE.Active := false;
          dsEE.DataRequired();
          dsEE.SortOn('EE_NBR', []);
        end;

        try
          bFound := dsEE.Locate('EE_NBR', ListOfEE['EENbr'], []);
          CheckCondition(bFound, 'Cannot find record in EE table. EE_NBR=' + IntToStr(ListOfEE['EENbr']));
          if (dsEE['OVERRIDE_FED_TAX_TYPE'] <> null) and
            (dsEE['OVERRIDE_FED_TAX_TYPE'] <> OVERRIDE_VALUE_TYPE_NONE) then
          begin
            if dsEE['OVERRIDE_FED_TAX_VALUE'] = null then
            begin
              FErrorsList.AddEvent(etWarning, '"Override Fed Tax Value" cannot be empty, if "Override Fed Tax Type" is not NONE. It will be filled up with 0');
              dsEE.Edit;
              try
                dsEE['OVERRIDE_FED_TAX_VALUE'] := 0;
                dsEE.Post;
              except
                on E: Exception do
                begin
                  dsEE.Cancel;
                  raise;
                end;
              end;

              ctx_DataAccess.StartNestedTransaction([dbtClient]);
              try
                ctx_DataAccess.PostDataSets([dsEE]);
                ctx_DataAccess.CommitNestedTransaction;
              except
                on E : Exception do
                begin
                  ctx_DataAccess.RollbackNestedTransaction;
                  raise;
                end;
              end;
            end;
          end;
        except
          on EE: Exception do
            FErrorsList.AddEvent(etError, 'After import processing. Cannot check override fed tax value. EE_NBR=' + IntToStr(ListOfEE['EENbr'])
              + '.  Error: ' + EE.Message + #13#10 + GetStack);
        end;

        ListOfEE.Next;
      end;
    finally
      dsEE.Free;
    end;
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

function TEvExchangeEeEngine.ComputeCalculatedFields(const ADataSet: TevClientDataSet): boolean;
var
  EERate, ERRate, PremiumAmount: Extended;
begin
  Result := false;
  if AnsiSameText(ADataSet.TableName, 'EE_BENEFITS') then
  begin
    EERate := 0;
    ERRate := 0;
    PremiumAmount := 0;
    if ADataSet['EE_RATE'] <> null then
      EERate := ADataSet['EE_RATE'];
    if ADataSet['ER_RATE'] <> null then
      ERRate := ADataSet['ER_RATE'];
    if ADataSet['TOTAL_PREMIUM_AMOUNT'] <> null then
      PremiumAmount := ADataSet['TOTAL_PREMIUM_AMOUNT'];
    if not ExtendedCompare((EERate + ERRate), coEqual, PremiumAmount) then
    begin
      ADataSet.Edit;
      ADataSet['TOTAL_PREMIUM_AMOUNT'] := EERate + ERRate;
      if ADataSet['EE_RATE'] = null then
        ADataSet['EE_RATE'] := 0;
      if ADataSet['ER_RATE'] = null then
        ADataSet['ER_RATE'] := 0;
      try
        ADataSet.Post;
        Result := true;
      except
        ADataSet.Cancel;
        raise;
      end;
    end;
  end;
end;

procedure TEvExchangeEeEngine.CheckOverrideStateTaxValue;
var
  prevClNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEEStates: IevDataSet;
  dsEEStates: TevClientDataSet;
  SortIndex: String;
  Tbl: TClass;
  bFound: boolean;
begin
  // filling list of EE
  ListOfEEStates := TEvDataSet.Create;
  ListOfEEStates.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEEStates.vclDataSet.FieldDefs.Add('EEStatesNbr', ftInteger, 0, true);
  ListOfEEStates.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // Inserts or Updated EE with filled up OVERRIDE_STATE_TAX_TYPE or OVERRIDE_STATE_TAX_VALUE
    if (LogRecord.GetTableName = 'EE_STATES') and
      (LogRecord.GetAdditionalParams.ValueExists('OVERRIDE_STATE_TAX_TYPE') or LogRecord.GetAdditionalParams.ValueExists('OVERRIDE_STATE_TAX_VALUE')) then
    begin
      // checking that it does not exists
      if not ListOfEEStates.vclDataSet.Locate('ClNbr;EEStatesNbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEEStates.Append;
        ListOfEEStates['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEEStates['EEStatesNbr'] :=  LogRecord.GetRecordNbr;
        ListOfEEStates.Post;
      end;
    end;
  end; // for i
  if ListOfEEStates.RecordCount = 0 then
    exit;

  // sorting ListOfEEStates
  SortIndex := RandomString(10);
  (ListOfEEStates.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEEStates.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEEStates.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    Tbl := GetClass('TEE_STATES');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE_STATES');
    dsEEStates := TddTableClass(Tbl).Create(nil);
    try
      dsEEStates.SkipFieldCheck := True;
      dsEEStates.ProviderName := 'EE_STATES_PROV';

      prevClNbr := -1;
      ListOfEEStates.First;
      while not ListOfEEStates.Eof do
      begin
        // opening CLient
        if ListOfEEStates['ClNbr'] <> prevClNbr then
        begin
          prevClNbr := ListOfEEStates['ClNbr'];
          ctx_DataAccess.OpenClient(prevClNbr);
          dsEEStates.Active := false;
          dsEEStates.DataRequired();
          dsEEStates.SortOn('EE_STATES_NBR', []);
        end;

        try
          bFound := dsEEStates.Locate('EE_STATES_NBR', ListOfEEStates['EEStatesNbr'], []);
          CheckCondition(bFound, 'Cannot find employee''s state. EE_STATES_NBR=' + IntToStr(ListOfEEStates['EEStatesNbr']));
          if (dsEEStates['OVERRIDE_STATE_TAX_TYPE'] <> null) and
            (dsEEStates['OVERRIDE_STATE_TAX_TYPE'] <> OVERRIDE_VALUE_TYPE_NONE) then
          begin
            if dsEEStates['OVERRIDE_STATE_TAX_VALUE'] = null then
            begin
              FErrorsList.AddEvent(etWarning, '"Override State Tax Value" cannot be empty, if "Override State Tax Type" is not NONE. It will be filled up with 0');
              dsEEStates.Edit;
              try
                dsEEStates['OVERRIDE_STATE_TAX_VALUE'] := 0;
                dsEEStates.Post;
              except
                on E: Exception do
                begin
                  dsEEStates.Cancel;
                  raise;
                end;
              end;

              ctx_DataAccess.StartNestedTransaction([dbtClient]);
              try
                ctx_DataAccess.PostDataSets([dsEEStates]);
                ctx_DataAccess.CommitNestedTransaction;
              except
                on E : Exception do
                begin
                  ctx_DataAccess.RollbackNestedTransaction;
                  raise;
                end;
              end;
            end;
          end;
        except
          on EE: Exception do
            FErrorsList.AddEvent(etError, 'After import processing. Cannot check override state tax value. EE_STATES_NBR=' + IntToStr(ListOfEEStates['EEStatesNbr'])
              + '.  Error: ' + EE.Message + #13#10 + GetStack);
        end;

        ListOfEEStates.Next;
      end;
    finally
      dsEEStates.Free;
    end;
  finally
    (ListOfEEStates.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEEStates.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEEStates.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.AddDetaultRate;
var
  prevClNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEE: IevDataSet;
  dsEERates: TevClientDataSet;
  SortIndex: String;
  Tbl: TClass;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // New EEs
    if (LogRecord.GetOperation = 'I') and (LogRecord.GetTableName = 'EE') then
    begin
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] := LogRecord.GetRecordNbr;
        ListOfEE.Post;
      end;
    end;
  end;

  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    // looking for Rates and add if needed
    Tbl := GetClass('TEE_RATES');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE_RATES');
    dsEERates := TddTableClass(Tbl).Create(nil);
    try
      dsEERates.SkipFieldCheck := True;
      dsEERates.ProviderName := 'EE_RATES_PROV';

      prevClNbr := -1;
      ListOfEE.First;
      while not ListOfEE.Eof do
      begin
        // opening CLient
        if ListOfEE['ClNbr'] <> prevClNbr then
        begin
          prevClNbr := ListOfEE['ClNbr'];
          ctx_DataAccess.OpenClient(prevClNbr);
        end;

        dsEERates.Active := false;

        dsEERates.DataRequired('EE_NBR=' + IntToStr(ListOfEE['EENbr']));
        try
          if dsEERates.RecordCount = 0 then
          begin
            try
              dsEERates.Append;
              dsEERates['EE_NBR'] := ListOfEE['EENbr'];
              dsEERates['RATE_NUMBER'] := 1;
              dsEERates['PRIMARY_RATE'] := 'Y';
              dsEERates['RATE_AMOUNT'] := 0;
              dsEERates.Post;
            except
              on E: Exception do
              begin
                dsEERates.Cancel;
                raise;
              end;
            end;

            ctx_DataAccess.StartNestedTransaction([dbtClient]);
            try
              ctx_DataAccess.PostDataSets([dsEERates]);
              ctx_DataAccess.CommitNestedTransaction;
            except
              on E: Exception do
              begin
                ctx_DataAccess.RollbackNestedTransaction;
                raise;
              end;
            end;
          end;  // if
        except
          on EE: Exception do
            FErrorsList.AddEvent(etError, 'After import processing. Cannot add rate for employee. EENbr = ' + IntToStr(ListOfEE['EENbr'])
              + ' Error: ' + EE.Message + #13#10 + GetStack);
        end;

        ListOfEE.Next;
      end;  // while
    finally
      dsEERates.Free;
    end;
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.DoImport;
var
  OldCustomCoNbr: String;
  CustomCoNbrField: String;
  CustomCoNbr: Variant;
  Q: IevQuery;
begin
  inherited;
  if FErrorsList.Count > 0 then
    exit;

  // import
  try
    FillListOfMappedTables;
    FImpEngine.StartMappingI(GetMapDefs, FFunctionsDefinitions as IIEDefinition, false);
    try
      OldCustomCoNbr := '';
      if FCustomCompanyAssigned then
        CustomCoNbrField := FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField).GetInputFieldName
      else
        CustomCoNbrField := '';

      BeforeImport;

      FInputData.First;
      while not FInputData.eof do
      begin
        try
          // multicompany or multiclient
          if FCustomCompanyAssigned then
          begin
            CustomCoNbr :=  FInputData.FieldByName(CustomCoNbrField).Value;
            CheckCondition(CustomCoNbr <> null, 'Value of CustomCoNbr field is null');
            FCustomCoNbr := CustomCoNbr;
            if FCustomCoNbr <> OldCustomCoNbr then
            begin
              // calculating CoNbr and ClNbr
              Q := TevQuery.Create('SELECT CL_NBR, CO_NBR FROM TMP_CO WHERE CUSTOM_COMPANY_NUMBER=''' + FCustomCoNbr + '''');
              Q.Execute;
              CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CUSTOM_COMPANY_NUMBER=''' + FCustomCoNbr + '''');
              ctx_DataAccess.OpenClient(Q.Result.Fields[0].AsInteger);
              FImpEngine.ClearDataSetsAndVariables;
              FCoNbr := Q.Result.Fields[1].AsInteger;
              OldCustomCoNbr := FCustomCoNbr;

              Q := TevQuery.Create('SELECT DBDT_LEVEL FROM CO a WHERE {AsOfNow<a>} AND a.CO_NBR=' + IntToStr(FCoNbr));
              Q.Execute;
              CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(FCoNbr));
              FCoDBDTLevel := Q.Result.Fields[0].AsString[1];

              // as of date logic
            end;
          end;

          // actual import
          ImportRow;
          Inc(FImportedCount);
        except
          on EE:Exception do
            FErrorsList.AddEvent(etError, FormatErrorMessageForRow(EE.Message));
        end;
        AbortIfCurrentThreadTaskTerminated;
        FInputData.Next;
      end;  // while
    finally
      FImpEngine.EndMapping;
    end;
  except
    on E: EAbort do
      raise;
    on E:Exception do
      FErrorsList.AddEvent(etFatalError, 'Exception in DOImport procedure. Error: ' + E.Message);
  end;

  // post processing
  try
    AfterImport;
  except
    on E:Exception do
      FErrorsList.AddEvent(etError, 'Exception in AfterImport procedure. Error: ' + E.Message);
  end;

  FImportResult.AddValue(EvoXInputRowsAmount, FRowsCount);
  FImportResult.AddValue(EvoXImportedRowsAmount, FImportedCount);
  FImportResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
  FImportResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
  FImportResult.AddValue(EvoXExtErrorsList, FErrorsList);
end;

function TEvExchangeEeEngine.GetInputDataSortIndexFieldNames: String;
var
  CustomCompanyField: IevEchangeMapField;
begin
  Result := '';
  if FCustomCompanyAssigned then
  begin
    CustomCompanyField := FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField);
    Result := CustomCompanyField.GetInputFieldName;
  end;
end;

procedure TEvExchangeEeEngine.AddTOAandEDs;
var
  prevClNbr: integer;
  i: integer;
  LogRecord: IevExchangeChangeRecord;
  ListOfEE: IevDataSet;
  SortIndex: String;
begin
  // filling list of EE
  ListOfEE := TEvDataSet.Create;
  ListOfEE.vclDataSet.FieldDefs.Add('ClNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('CoNbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.FieldDefs.Add('EENbr', ftInteger, 0, true);
  ListOfEE.vclDataSet.Active := true;

  for i := 0 to FChangeLogList.Count - 1 do
  begin
    LogRecord := IInterface(FChangeLogList.Values[i].Value) as IevExchangeChangeRecord;

    // New EEs
    if (LogRecord.GetOperation = 'I') and (LogRecord.GetTableName = 'EE') then
    begin
      if not ListOfEE.vclDataSet.Locate('ClNbr;EEnbr', VarArrayOf([LogRecord.GetClientNbr, LogRecord.GetRecordNbr]), []) then
      begin
        ListOfEE.Append;
        ListOfEE['ClNbr'] := LogRecord.GetClientNbr;
        ListOfEE['EENbr'] := LogRecord.GetRecordNbr;
        ListOfEE['CoNbr'] := LogRecord.GetAdditionalParams.Value['CO_NBR'];
        ListOfEE.Post;
      end;
    end;
  end;

  if ListOfEE.RecordCount = 0 then
    exit;

  // sorting ListOfEE
  SortIndex := RandomString(10);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, 'ClNbr', []);
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;

  try
    prevClNbr := -1;
    ListOfEE.First;
    while not ListOfEE.Eof do
    begin
      // opening CLient
      if ListOfEE['ClNbr'] <> prevClNbr then
      begin
        prevClNbr := ListOfEE['ClNbr'];
        ctx_DataAccess.OpenClient(prevClNbr);
      end;

      try
        Context.RemoteMiscRoutines.AddTOAForNewEE(ListOfEE['CoNbr'], ListOfEE['EENbr']);
      except
        on E: Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot add TOA for employee. EENbr = ' + IntToStr(ListOfEE['EENbr'])
            + ' Error: ' + E.Message + #13#10 + GetStack);
      end;

      try
        Context.RemoteMiscRoutines.AddEDsForNewEE(ListOfEE['CoNbr'], ListOfEE['EENbr']);
      except
        on E: Exception do
          FErrorsList.AddEvent(etError, 'After import processing. Cannot add EDs for employee. EENbr = ' + IntToStr(ListOfEE['EENbr'])
            + ' Error: ' + E.Message + #13#10 + GetStack);
      end;

      ListOfEE.Next;
    end; // while
  finally
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexName := '';
    (ListOfEE.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
    (ListOfEE.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
  end;
end;

procedure TEvExchangeEeEngine.CheckValuesBeforeChange(const ATableName: String; const AOperation: char;
  const AOldValues: TevClientDataSet; const ANewValues: IisListOfValues);
var
  Nbr: Variant;
begin
  inherited;

  if ATablename = 'EE' then
  begin
    if AOperation = 'I' then
    begin
      if FCoDBDTLevel in [CLIENT_LEVEL_DIVISION, CLIENT_LEVEL_BRANCH, CLIENT_LEVEL_DEPT, CLIENT_LEVEL_TEAM] then
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_DIVISION_NBR')) <> null,
          '"' + ATableName + '" table. Division is required due company''s DBDT setup');
      if FCoDBDTLevel in [CLIENT_LEVEL_BRANCH, CLIENT_LEVEL_DEPT, CLIENT_LEVEL_TEAM] then
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_BRANCH_NBR')) <> null,
          '"' + ATableName + '" table. Branch is required due company''s DBDT setup');
      if FCoDBDTLevel in [CLIENT_LEVEL_DEPT, CLIENT_LEVEL_TEAM] then
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_DEPARTMENT_NBR')) <> null,
          '"' + ATableName + '" table. Department is required due company''s DBDT setup');
      if FCoDBDTLevel in [CLIENT_LEVEL_TEAM] then
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_TEAM_NBR')) <> null,
          '"' + ATableName + '" table. Team is required due company''s DBDT setup');
    end
    else if AOperation = 'U' then
    begin
      Nbr := GetIEMapFieldValue(ANewValues.GetValueByName('CO_DIVISION_NBR'));
      if (Nbr = null) and (Nbr <> AOldValues['CO_DIVISION_NBR']) then
      begin
        if FCoDBDTLevel in [CLIENT_LEVEL_DIVISION, CLIENT_LEVEL_BRANCH, CLIENT_LEVEL_DEPT, CLIENT_LEVEL_TEAM] then
          CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_DIVISION_NBR')) <> null,
            '"' + ATableName + '" table. Division is required due company''s DBDT setup');
      end;

      Nbr := GetIEMapFieldValue(ANewValues.GetValueByName('CO_BRANCH_NBR'));
      if (Nbr = null) and (Nbr <> AOldValues['CO_BRANCH_NBR']) then
      begin
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_BRANCH_NBR')) <> null,
          '"' + ATableName + '" table. Branch is required due company''s DBDT setup');
      end;

      Nbr := GetIEMapFieldValue(ANewValues.GetValueByName('CO_DEPARTMENT_NBR'));
      if (Nbr = null) and (Nbr <> AOldValues['CO_DEPARTMENT_NBR']) then
      begin
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_DEPARTMENT_NBR')) <> null,
          '"' + ATableName + '" table. Department is required due company''s DBDT setup');
      end;

      Nbr := GetIEMapFieldValue(ANewValues.GetValueByName('CO_TEAM_NBR'));
      if (Nbr = null) and (Nbr <> AOldValues['CO_TEAM_NBR']) then
      begin
        CheckCondition(GetIEMapFieldValue(ANewValues.GetValueByName('CO_TEAM_NBR')) <> null,
          '"' + ATableName + '" table. Team is required due company''s DBDT setup');
      end;
    end;
  end;
end;

end.
