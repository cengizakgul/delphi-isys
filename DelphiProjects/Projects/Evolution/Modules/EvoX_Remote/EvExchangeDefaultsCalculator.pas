unit EvExchangeDefaultsCalculator;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvUtils,
     EvConsts, EvStreamUtils, EvTypes, isBasicUtils, Variants, DB,
      EvClientDataSet, EvContext;

type
  IEvExchangeDefCalculator = interface
  ['{DB205A78-A7F7-409F-A4AA-B7ED8CF799C7}']
    procedure FillTableBefore(const ACoNbr: integer; const ADataSet: TevClientDataSet); // before mapped values are posted
    procedure FillTableAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet);  // after mapped values are posted
  end;

  TEvExchangeDefCalculator = class(TisInterfacedObject, IEvExchangeDefCalculator)
  private
    FClNbr: integer;

    FClPersonCoNbr: integer;
    //CL_PERSON table's defaults
    FClPersonCity: Variant;
    FClPersonState: Variant;
    FClPersonZipCode: Variant;
    FClPersonResidentialStateNbr: Variant;

    FEECoNbr: integer;
    // EE table's defaults
    FEEPayFrequency: Variant;
    FEECurrentHireDate: TDateTime;
    FSelfServeEnabled: variant;
    FTimeOffEnabled: variant;
    FBenefitsEnabled: variant;
    FDDEnabled: variant;
    FEEAcaStandardHours: Variant;
    FEEAcaStatus: Variant;
    FEEBenefitsEligible: Variant;
    FEEAcaForm: Variant;
    FEEAcaOfferCode: Variant;
    FEEAcaReliefCode: Variant;

    FEEStatesCoNbr: integer;
    // EE_STATES table's defaults
    FEEStatesSuiApplyCoStatesNbr: Variant;
    FEEStatesSdiApplyCoStatesNbr: Variant;

    FEERatesCoNbr: integer;
    // EE_RATES table's defaults

    FEETOACoNbr: integer;
    // EE_TIME_OFF_ACCRUAL table's defaults

    FClPersonDependentsCoNbr: integer;
    // EE_TIME_OFF_ACCRUAL table's defaults

    FEEHrAttendanceCoNbr: integer;
    // EE_HR_ATTENDANCE table's defaults

    FEEEmergencyContactsCoNbr: integer;
    // EE_EMERGENCY_CONTACTS table's defaults

    procedure InitClPerson; virtual;
    procedure InitClPersonForCompany; virtual;

    procedure InitEE; virtual;
    procedure InitEEForCompany; virtual;

    procedure InitEEStates; virtual;
    procedure InitEEStatesForCompany; virtual;

    procedure InitEERates; virtual;
    procedure InitEERatesForCompany; virtual;

    procedure InitEETOA; virtual;
    procedure InitEETOAForCompany; virtual;

    procedure InitCLPersonDependents; virtual;
    procedure InitCLPersonDependentsForCompany; virtual;

    procedure InitEEHrAttendance; virtual;
    procedure InitEEHrAttendanceForCompany; virtual;

    procedure InitEEEmergencyContacts; virtual;
    procedure InitEEEmergencyContactsForCompany; virtual;
  protected
    procedure DoOnConstruction; override;
    procedure InitAllTables;
    procedure CheckClNbr;

    procedure FillCLPerson(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEE(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEEStates(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEERates(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEETOA(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillClPersonDependents(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEEHrAttendance(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEEEmergencyContacts(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;

    procedure FillEEPiecesAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEEWorkShiftsAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEEScheduledEDAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;
    procedure FillEELocalsAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet); virtual;

    // IEvExchangeDefaultsCalculator implementation
    procedure FillTableBefore(const ACoNbr: integer; const ADataSet: TevClientDataSet);
    procedure FillTableAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet);
  public
    destructor Destroy; override;
  end;

implementation

uses
  EvDataset;

{ TEvExchangeDefCalculator }

procedure TEvExchangeDefCalculator.CheckClNbr;
begin
  if ctx_DataAccess.ClientId <> FClNbr then
  begin
    FClNbr := ctx_DataAccess.ClientId;
    InitAllTables;
  end;
end;

destructor TEvExchangeDefCalculator.Destroy;
begin

  inherited;
end;

procedure TEvExchangeDefCalculator.DoOnConstruction;
begin
  inherited;
  FClNbr := -1;
  InitAllTables;
end;

procedure TEvExchangeDefCalculator.FillCLPerson(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FClPersonCoNbr then
  begin
    FCLPersonCoNbr := ACoNbr;
    InitClPersonForCompany;
  end;

  ADataSet['ADDRESS1'] := 'NEED ADDRESS';
  ADataSet['CITY'] := FClPersonCity;
  ADataSet['STATE'] := FClPersonState;
  ADataSet['ZIP_CODE'] := FClPersonZipCode;
  ADataSet['RESIDENTIAL_STATE_NBR'] := FClPersonResidentialStateNbr;
end;

procedure TEvExchangeDefCalculator.FillClPersonDependents(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FClPersonDependentsCoNbr then
  begin
    FClPersonDependentsCoNbr := ACoNbr;
    InitCLPersonDependentsForCompany;
  end;

  ADataSet['GENDER'] := 'U';
  ADataSet['RELATION_TYPE'] := 'C';
end;

procedure TEvExchangeDefCalculator.FillEE(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FEECoNbr then
  begin
    FEECoNbr := ACoNbr;
    InitEEForCompany;
  end;

  ADataSet['PAY_FREQUENCY'] := FEEPayFrequency;
  ADataSet['CURRENT_HIRE_DATE'] := FEECurrentHireDate;
  ADataSet['SELFSERVE_ENABLED'] := FSelfServeEnabled;
  ADataSet['TIME_OFF_ENABLED'] := FTimeOffEnabled;
  ADataSet['BENEFITS_ENABLED'] := FBenefitsEnabled;
  ADataSet['ACA_STANDARD_HOURS'] := FEEAcaStandardHours;
  ADataSet['ACA_STATUS'] := FEEAcaStatus;
  ADataSet['BENEFITS_ELIGIBLE'] := FEEBenefitsEligible;
  ADataSet['DIRECT_DEPOSIT_ENABLED'] := FDDEnabled;
  ADataSet['ACA_TYPE'] := FEEAcaForm;
  ADataSet['SY_FED_ACA_OFFER_CODES_NBR'] := FEEAcaOfferCode;
  ADataSet['SY_FED_ACA_RELIEF_CODES_NBR'] := FEEAcaReliefCode;
end;

procedure TEvExchangeDefCalculator.FillEEEmergencyContacts(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FEEEmergencyContactsCoNbr then
  begin
    FEEEmergencyContactsCoNbr := ACoNbr;
    InitEEEmergencyContactsForCompany;
  end;

  ADataSet['CONTACT_PRIORITY'] := 1;
end;

procedure TEvExchangeDefCalculator.FillEEHrAttendance(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FEEHrAttendanceCoNbr then
  begin
    FEEHrAttendanceCoNbr := ACoNbr;
    InitEEHrAttendanceForCompany;
  end;

  ADataSet['ATTENDANCE_TYPE_NUMBER'] := 1;
end;

procedure TEvExchangeDefCalculator.FillEELocalsAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet);
var
  qCoLocalsTax, qSyLocals: IevQuery;
begin
  if ADataset['EE_COUNTY'] = null then
  begin
    qCoLocalsTax := TevQuery.Create('select SY_LOCALS_NBR from CO_LOCAL_TAX a where a.CO_LOCAL_TAX_NBR=:p1 and {AsOfNow<a>} ');
    qCoLocalsTax.Params.AddValue('p1', ADataset['CO_LOCAL_TAX_NBR']);
    qCoLocalsTax.Execute;
    CheckCondition(qCoLocalsTax.Result.RecordCount = 1, 'Cannot find record in CO_LOCAL_TAX table. CO_LOCAL_TAX_NBR = ' + VarToStr(ADataset['CO_LOCAL_TAX_NBR']));
    qSyLocals := TevQuery.Create('select b.COUNTY_NAME from SY_LOCALS a, SY_COUNTY b ' +
      'where {AsOfNow<a>} and {AsOfNow<b>} and a.SY_COUNTY_NBR=b.SY_COUNTY_NBR and ' +
      'SY_LOCALS_NBR=:p1');
    qSyLocals.Params.AddValue('p1', qCoLocalsTax.Result['SY_LOCALS_NBR']);
    qSyLocals.Execute;
    if (qSyLocals.Result.RecordCount > 0) and (qSyLocals.Result['COUNTY_NAME'] <> null) then
      ADataset['EE_COUNTY'] := qSyLocals.Result['COUNTY_NAME'];
  end;
end;

procedure TEvExchangeDefCalculator.FillEEPiecesAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet);
var
  Q: IevQuery;
begin
  if ((ADataSet['RATE_QUANTITY'] = null) or (ADataSet['RATE_AMOUNT'] = null)) and (ADataSet['CL_PIECES_NBR'] <> null) then
  begin
    Q := TevQuery.Create('SELECT DEFAULT_RATE, DEFAULT_QUANTITY FROM CL_PIECES a WHERE {AsOfNow<a>} and a.CL_PIECES_NBR=' + IntToStr(ADataSet['CL_PIECES_NBR']));
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Piece code not found. CL_PIECES_NBR=' + IntToStr(ADataSet['CL_PIECES_NBR']));
    if ADataSet['RATE_QUANTITY'] = null then
      ADataSet['RATE_QUANTITY'] := Q.Result.FieldValues['DEFAULT_QUANTITY'];
    if ADataSet['RATE_AMOUNT'] = null then
      ADataSet['RATE_AMOUNT'] := Q.Result.FieldValues['DEFAULT_RATE'];
  end;
end;

procedure TEvExchangeDefCalculator.FillEERates(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FEERatesCoNbr then
  begin
    FEERatesCoNbr := ACoNbr;
    InitEERatesForCompany;
  end;

  ADataSet['RATE_NUMBER'] := 1;
end;

procedure TEvExchangeDefCalculator.FillEEScheduledEDAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet);
var
  Q, Q1: IevQuery;
  sScheduledDefaults, sUsedAsBenefit: String;
begin
  CheckCondition(ADataSet['CL_E_DS_NBR'] <> null, 'CL_E_DS_NBR is null');
  Q := TevQuery.Create('SELECT a.* FROM CL_E_DS a WHERE {AsOfNow<a>} and a.CL_E_DS_NBR=' + IntToStr(ADataSet['CL_E_DS_NBR']));
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'EDCode not found. CL_E_DS_NBR=' + IntToStr(ADataSet['CL_E_DS_NBR']));
  sScheduledDefaults := Q.Result.FieldByName('SCHEDULED_DEFAULTS').Value;

  if ADataSet['CALCULATION_TYPE'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['CALCULATION_TYPE'] := CALC_METHOD_FIXED
    else
      ADataSet['CALCULATION_TYPE'] := Q.Result.FieldByName('SD_CALCULATION_METHOD').Value;
  end;

  if sScheduledDefaults = 'Y' then
  begin
    if ADataSet['AMOUNT'] = null then
      ADataSet['AMOUNT'] := Q.Result.FieldByName('SD_AMOUNT').Value;
    if  ADataSet['PERCENTAGE'] = null then
      ADataSet['PERCENTAGE'] := Q.Result.FieldByName('SD_RATE').Value;
  end;

  if ADataSet['FREQUENCY'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['FREQUENCY'] := SCHED_FREQ_DAILY
    else
      ADataSet['FREQUENCY'] := Q.Result.FieldByName('SD_FREQUENCY').Value;
  end;

  if ADataSet['PLAN_TYPE'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['PLAN_TYPE'] := MONTH_NUMBER_NONE
    else
      ADataSet['PLAN_TYPE'] := Q.Result.FieldByName('SD_PLAN_TYPE').Value;
  end;

  if ADataSet['WHICH_CHECKS'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['WHICH_CHECKS'] := GROUP_BOX_ALL
    else
      ADataSet['WHICH_CHECKS'] := Q.Result.FieldByName('SD_WHICH_CHECKS').Value;
  end;

  if ADataSet['EFFECTIVE_START_DATE'] = null then
  begin
    if (sScheduledDefaults = 'N') or (Q.Result.FieldByName('SD_EFFECTIVE_START_DATE').Value = null) then
      ADataSet['EFFECTIVE_START_DATE'] := Trunc(SysTime)
    else
      ADataSet['EFFECTIVE_START_DATE'] := Q.Result.FieldByName('SD_EFFECTIVE_START_DATE').Value;
  end;

  if (ADataSet['PRIORITY_NUMBER'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['PRIORITY_NUMBER'] := Q.Result.FieldByName('SD_PRIORITY_NUMBER').Value;

  if ADataSet['EE_BENEFITS_NBR'] = null then
  begin
    Q1 := TevQuery.Create('SELECT CL_E_DS_NBR, USED_AS_BENEFIT, CO_BENEFIT_SUBTYPE_NBR FROM CO_E_D_CODES a WHERE {AsOfNow<a>} and a.CL_E_DS_NBR=' + IntToStr(ADataSet['CL_E_DS_NBR']));
    Q1.Execute;
    if Q1.Result.RecordCount > 0 then
    begin
      sUsedAsBenefit := Q1.Result.FieldByName('USED_AS_BENEFIT').Value;
      if (ADataSet['CO_BENEFIT_SUBTYPE_NBR'] = null) and (sScheduledDefaults = 'Y') then
        ADataSet['CO_BENEFIT_SUBTYPE_NBR'] := Q1.Result.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value;
    end;
  end
  else
    ADataSet['CO_BENEFIT_SUBTYPE_NBR'] := null;

  if sScheduledDefaults = 'N' then
  begin
    if ADataSet['EXCLUDE_WEEK_1'] = null then
      ADataSet['EXCLUDE_WEEK_1'] := 'N';
    if ADataSet['EXCLUDE_WEEK_2'] = null then
      ADataSet['EXCLUDE_WEEK_2'] := 'N';
    if ADataSet['EXCLUDE_WEEK_3'] = null then
      ADataSet['EXCLUDE_WEEK_3'] := 'N';
    if ADataSet['EXCLUDE_WEEK_4'] = null then
      ADataSet['EXCLUDE_WEEK_4'] := 'N';
    if ADataSet['EXCLUDE_WEEK_5'] = null then
      ADataSet['EXCLUDE_WEEK_5'] := 'N';
  end
  else
  begin
    if ADataSet['EXCLUDE_WEEK_1'] = null then
      ADataSet['EXCLUDE_WEEK_1'] := Q.Result.FieldByName('SD_EXCLUDE_WEEK_1').Value;
    if ADataSet['EXCLUDE_WEEK_2'] = null then
      ADataSet['EXCLUDE_WEEK_2'] := Q.Result.FieldByName('SD_EXCLUDE_WEEK_2').Value;
    if ADataSet['EXCLUDE_WEEK_3'] = null then
      ADataSet['EXCLUDE_WEEK_3'] := Q.Result.FieldByName('SD_EXCLUDE_WEEK_3').Value;
    if ADataSet['EXCLUDE_WEEK_4'] = null then
      ADataSet['EXCLUDE_WEEK_4'] := Q.Result.FieldByName('SD_EXCLUDE_WEEK_4').Value;
    if ADataSet['EXCLUDE_WEEK_5'] = null then
      ADataSet['EXCLUDE_WEEK_5'] := Q.Result.FieldByName('SD_EXCLUDE_WEEK_5').Value;
  end;

  if ADataSet['ALWAYS_PAY'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['ALWAYS_PAY'] := ALWAYS_PAY_NO
    else
      ADataSet['ALWAYS_PAY'] := Q.Result.FieldByName('SD_ALWAYS_PAY').Value;
  end;

  if ADataSet['DEDUCTIONS_TO_ZERO'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['DEDUCTIONS_TO_ZERO'] := 'N'
    else
      ADataSet['DEDUCTIONS_TO_ZERO'] := Q.Result.FieldByName('SD_DEDUCTIONS_TO_ZERO').Value;
  end;

  if (ADataSet['MAX_AVERAGE_HOURLY_WAGE_RATE'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['MAX_AVERAGE_HOURLY_WAGE_RATE'] := Q.Result.FieldByName('SD_MAX_AVERAGE_HOURLY_WAGE_RATE').Value;

  if (ADataSet['THRESHOLD_AMOUNT'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['THRESHOLD_AMOUNT'] := Q.Result.FieldByName('SD_THRESHOLD_AMOUNT').Value;

  if ADataSet['USE_PENSION_LIMIT'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['USE_PENSION_LIMIT'] := 'N'
    else
      ADataSet['USE_PENSION_LIMIT'] := Q.Result.FieldByName('SD_USE_PENSION_LIMIT').Value;
  end;

  if (ADataSet['CL_E_D_GROUPS_NBR'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['CL_E_D_GROUPS_NBR'] := Q.Result.FieldByName('CL_E_D_GROUPS_NBR').Value;

  if (ADataSet['CL_AGENCY_NBR'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['CL_AGENCY_NBR'] := Q.Result.FieldByName('DEFAULT_CL_AGENCY_NBR').Value;

  if (ADataSet['MAX_AVG_AMT_CL_E_D_GROUPS_NBR'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['MAX_AVG_AMT_CL_E_D_GROUPS_NBR'] := Q.Result.FieldByName('SD_MAX_AVG_AMT_GRP_NBR').Value;

  if (ADataSet['MAX_AVG_HRS_CL_E_D_GROUPS_NBR'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['MAX_AVG_HRS_CL_E_D_GROUPS_NBR'] := Q.Result.FieldByName('SD_MAX_AVG_HRS_GRP_NBR').Value;

  if (ADataSet['THRESHOLD_E_D_GROUPS_NBR'] = null) and (sScheduledDefaults = 'Y') then
    ADataSet['THRESHOLD_E_D_GROUPS_NBR'] := Q.Result.FieldByName('SD_THRESHOLD_E_D_GROUPS_NBR').Value;

  if ADataSet['DEDUCT_WHOLE_CHECK'] = null then
  begin
    if sScheduledDefaults = 'N' then
      ADataSet['DEDUCT_WHOLE_CHECK'] := 'N'
    else
      ADataSet['DEDUCT_WHOLE_CHECK'] := Q.Result.FieldByName('SD_DEDUCT_WHOLE_CHECK').Value;
  end;
end;

procedure TEvExchangeDefCalculator.FillEEStates(const ACoNbr: integer;  const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FEEStatesCoNbr then
  begin
    FEEStatesCoNbr := ACoNbr;
    InitEEStatesForCompany;
  end;

  ADataSet['SUI_APPLY_CO_STATES_NBR'] := FEEStatesSuiApplyCoStatesNbr;
  ADataSet['SDI_APPLY_CO_STATES_NBR'] := FEEStatesSdiApplyCoStatesNbr;
end;

procedure TEvExchangeDefCalculator.FillEETOA(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  if ACoNbr <> FEETOACoNbr then
  begin
    FEETOACoNbr := ACoNbr;
    InitEETOAForCompany;
  end;

  ADataSet.FieldByName('CURRENT_ACCRUED').Value := 0;
  ADataSet.FieldByName('CURRENT_USED').Value := 0;
  ADataSet['MARKED_ACCRUED'] := 0;
  ADataSet['MARKED_USED'] := 0;
end;

procedure TEvExchangeDefCalculator.FillEEWorkShiftsAfter(const ACoNbr: integer; const ADataSet: TevClientDataSet);
var
  Q: IevQuery;
begin
  if ((ADataSet['SHIFT_RATE'] = null) or (ADataSet['SHIFT_PERCENTAGE'] = null)) and (ADataSet['CO_SHIFTS_NBR'] <> null) then
  begin
    Q := TevQuery.Create('SELECT DEFAULT_AMOUNT, DEFAULT_PERCENTAGE FROM CO_SHIFTS a WHERE {AsOfNow<a>} and a.CO_SHIFTS_NBR=' + IntToStr(ADataSet['CO_SHIFTS_NBR']));
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Piece code not found. CO_SHIFTS_NBR=' + IntToStr(ADataSet['CO_SHIFTS_NBR']));
    if ADataSet['SHIFT_RATE'] = null then
      ADataSet['SHIFT_RATE'] := Q.Result.FieldValues['DEFAULT_AMOUNT'];
    if ADataSet['SHIFT_PERCENTAGE'] = null then
      ADataSet['SHIFT_PERCENTAGE'] := Q.Result.FieldValues['DEFAULT_PERCENTAGE'];
  end;
end;

procedure TEvExchangeDefCalculator.FillTableAfter(const ACoNbr: integer;
  const ADataSet: TevClientDataSet);
begin
  CheckClNbr;
  if AnsiSameText(ADataSet.TableName, 'EE_PIECE_WORK') then
  begin
    FillEEPiecesAfter(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_SCHEDULED_E_DS') then
  begin
    FillEEScheduledEDAfter(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_WORK_SHIFTS') then
  begin
    FillEEWorkShiftsAfter(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_LOCALS') then
  begin
    FillEELocalsAfter(ACoNbr, ADataSet);
  end;
end;

procedure TEvExchangeDefCalculator.FillTableBefore(const ACoNbr: integer; const ADataSet: TevClientDataSet);
begin
  CheckClNbr;
  if AnsiSameText(ADataSet.TableName, 'CL_PERSON') then
  begin
    FillCLPerson(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE') then
  begin
    FillEE(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_RATES') then
  begin
    FillEERates(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_STATES') then
  begin
    FillEEStates(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'CL_PERSON_DEPENDENTS') then
  begin
    FillClPersonDependents(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_HR_ATTENDANCE') then
  begin
    FillEEHrAttendance(ACoNbr, ADataSet);
  end
  else if AnsiSameText(ADataSet.TableName, 'EE_TIME_OFF_ACCRUAL') then
  begin
    FillEETOA(ACoNbr, ADataSet);
  end;
end;

procedure TEvExchangeDefCalculator.InitAllTables;
begin
  InitClPerson;
  InitEE;
  InitEEStates;
  InitEERates;
  InitEETOA;
  InitCLPersonDependents;
  InitEEHrAttendance;
  InitEEEmergencyContacts;
end;

procedure TEvExchangeDefCalculator.InitClPerson;
begin
  FClPersonCoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitCLPersonDependents;
begin
 FClPersonDependentsCoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitCLPersonDependentsForCompany;
begin

end;

procedure TEvExchangeDefCalculator.InitClPersonForCompany;
var
  Q: IevQuery;
  sState: String;
  iHomeCoStatesNbr: integer;
begin
  // City, State, Zip, HOME_CO_STATES_NBR for Residential_state_nbr
  Q := TevQuery.Create('SELECT CITY, STATE, ZIP_CODE, HOME_CO_STATES_NBR FROM CO a WHERE {AsOfNow<a>} and a.CO_NBR=' +IntToStr(FClPersonCoNbr));
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(FClPersonCoNbr));
  FClPersonCity := Q.Result.Fields[0].Value;
  FClPersonState := Q.Result.Fields[1].Value;
  FClPersonZipCode := Q.Result.Fields[2].Value;

  FClPersonResidentialStateNbr := null;
  if Q.Result.Fields[3].Value <> null then
  begin
    iHomeCoStatesNbr := Q.Result.Fields[3].Value;
    Q := TevQuery.Create('SELECT STATE FROM CO_STATES a WHERE {AsOfNow<a>} and a.CO_STATES_NBR=' +IntToStr(iHomeCoStatesNbr));
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'Company state not found. CO_STATES_NBR=' + IntToStr(iHomeCoStatesNbr));
    sState := Q.Result.Fields[0].Value;
    Q := TevQuery.Create('SELECT SY_STATES_NBR FROM SY_STATES a WHERE {AsOfNow<a>} and a.STATE=''' + sState + '''');
    Q.Execute;
    CheckCondition(Q.Result.RecordCount > 0, 'State not found in SY_STATES. STATE=' + sState);
    FClPersonResidentialStateNbr := Q.Result.Fields[0].Value;
  end;
end;

procedure TEvExchangeDefCalculator.InitEE;
begin
  FEECoNbr := -1;

  // CURRENT_HIRE_DATE
  FEECurrentHireDate := Trunc(Systime);
end;

procedure TEvExchangeDefCalculator.InitEEEmergencyContacts;
begin
  FEEEmergencyContactsCoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitEEEmergencyContactsForCompany;
begin

end;

procedure TEvExchangeDefCalculator.InitEEForCompany;
var
  Q: IevQuery;
begin
  // PAY_FREQUENCY
  Q := TevQuery.Create('SELECT PAY_FREQUENCY_HOURLY_DEFAULT, ENABLE_ESS, ENABLE_TIME_OFF, ' +
    'ENABLE_BENEFITS, ACA_STANDARD_HOURS, ACA_DEFAULT_STATUS, BENEFITS_ELIGIBLE_DEFAULT, ' +
    'ENABLE_DD, ACA_FORM_DEFAULT, SY_FED_ACA_OFFER_CODES_NBR, SY_FED_ACA_RELIEF_CODES_NBR ' +
    'FROM CO a WHERE {AsOfNow<a>} and a.CO_NBR=' +IntToStr(FEECoNbr));
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(FEECoNbr));
  FEEPayFrequency := Q.Result.Fields[0].Value;
  FEEAcaStandardHours := Q.Result.Fields[4].Value;
  FEEAcaStatus := Q.Result.Fields[5].Value;
  FEEBenefitsEligible := Q.Result.Fields[6].Value;
  FEEAcaForm := Q.Result.Fields[8].Value;
  FEEAcaOfferCode := Q.Result.Fields[9].Value;
  FEEAcaReliefCode := Q.Result.Fields[10].Value;

  if Q.Result.Fields[1].AsString =  GROUP_BOX_YES then
    FSelfServeEnabled := GROUP_BOX_FULL_ACCESS
  else if Q.Result.Fields[1].AsString =  GROUP_BOX_READONLY then
    FSelfServeEnabled := GROUP_BOX_YES
  else if Q.Result.Fields[1].AsString =  GROUP_BOX_NO then
    FSelfServeEnabled := GROUP_BOX_NO;

  if Q.Result.Fields[2].AsString =  GROUP_BOX_YES then
    FTimeOffEnabled := GROUP_BOX_FULL_ACCESS
  else if Q.Result.Fields[2].AsString =  GROUP_BOX_READONLY then
    FTimeOffEnabled := GROUP_BOX_YES
  else if Q.Result.Fields[2].AsString =  GROUP_BOX_NO then
    FTimeOffEnabled := GROUP_BOX_NO;

  if Q.Result.Fields[3].AsString =  GROUP_BOX_YES then
    FBenefitsEnabled := GROUP_BOX_FULL_ACCESS
  else if Q.Result.Fields[3].AsString =  GROUP_BOX_READONLY then
    FBenefitsEnabled := GROUP_BOX_YES
  else if Q.Result.Fields[3].AsString =  GROUP_BOX_NO then
    FBenefitsEnabled := GROUP_BOX_NO;

  if Q.Result.Fields[7].AsString =  GROUP_BOX_YES then
    FDDEnabled := GROUP_BOX_FULL_ACCESS
  else if Q.Result.Fields[7].AsString =  GROUP_BOX_READONLY then
    FDDEnabled := GROUP_BOX_YES
  else if Q.Result.Fields[7].AsString =  GROUP_BOX_NO then
    FDDEnabled := GROUP_BOX_NO;
end;

procedure TEvExchangeDefCalculator.InitEEHrAttendance;
begin
  FEEHrAttendanceCoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitEEHrAttendanceForCompany;
begin

end;

procedure TEvExchangeDefCalculator.InitEERates;
begin
  FEERatesCoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitEERatesForCompany;
begin

end;

procedure TEvExchangeDefCalculator.InitEEStates;
begin
  FEEStatesCoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitEEStatesForCompany;
var
  Q: IevQuery;
begin
  // CO_STATES_NBR
  Q := TevQuery.Create('SELECT HOME_CO_STATES_NBR, HOME_SUI_CO_STATES_NBR, HOME_SDI_CO_STATES_NBR FROM CO a WHERE {AsOfNow<a>} and a.CO_NBR=' +IntToStr(FEEStatesCoNbr));
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CO_NBR=' + IntToStr(FEEStatesCoNbr));
  FEEStatesSuiApplyCoStatesNbr := Q.Result.Fields[1].Value;
  FEEStatesSdiApplyCoStatesNbr := Q.Result.Fields[2].Value;
end;

procedure TEvExchangeDefCalculator.InitEETOA;
begin
  FEETOACoNbr := -1;
end;

procedure TEvExchangeDefCalculator.InitEETOAForCompany;
begin

end;

end.
