unit EvExchangeEngine;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvUtils,
     EvConsts, EvStreamUtils, EvTypes, isLogFile, EvContext, isBasicUtils, Variants, DB,
     EvEchangeEeFunctions, IEMap,  EvDataset, EvExchangeDefaultsCalculator,
     EvExchangeResultLog, EvClientDataSet;

type
  TevExchangeChangeRecord = class(TisInterfacedObject, IevExchangeChangeRecord)
  private
    FOperation: Char;
    FClientNbr: integer;
    FTableName: String;
    FRecordNbr: integer;
    FAdditionalParams: IisListOfValues;
  protected
    procedure DoOnConstruction; override;

    // IevExchangeChangeRecord;
    function  GetOperation: Char;
    function  GetClientNbr: integer;
    function  GetTableName: String;
    function  GetRecordNbr: integer;
    function  GetAdditionalParams: IisListOfValues;
  public
    constructor Create(const AOperation: Char; const AClientNbr: integer; const ATableName: String;
      const ARecordNbr: integer); reintroduce;
    destructor Destroy; override;
  end;

  TEvExchangeHandleNulls = (tIgnore, tReplace, tSpecial);

  TEvExchangeEngine = class(TisInterfacedObject, IevExchangeEngine)
  private
  protected
    FCoNbr: integer;
    FCustomCoNbr: String;
    FEffectiveDate: TDateTime;
    FFunctionsDefinitions: IEvoXDefinition;
    FErrorsList: IEvExchangeResultLog;
    FInputData: IevDataSet;
    FMapFile: IEvExchangeMapFile;
    FPackage: IevExchangePackage;
    FOptions: IisStringListRO;
    FImportedCount: integer;
    FRowsCount: integer;
    FCheckMapResult: IisListOfValues;
    FImportResult: IisListOfValues;
    FNotCheckCustomCompanyNbr: boolean;
    FCustomCompanyAssigned: boolean;
    FDefaultsCalculator: IEvExchangeDefCalculator;
    FMappedTablesList: IisStringList;
    FImpEngine: IIEMap;
    FChangeLogList: IisListOfValues;

    FHandleNullsOption: TEvExchangeHandleNulls;
    FNullSpecialString: String;
    procedure DoOnConstruction; override;

    procedure FillListOfMappedTables;
    function  CheckFieldAgainsListOfMappedTables(const AExchangeFieldName: String): boolean;
    function  GetMapDefs: TEvImportMapRecords;
    procedure ParseImportOptions; virtual;
    function  ValueIsSpecialNullString(const AValue: Variant): boolean;
    function  GetPackageFieldName(const APackageField: IEvExchangePackageField): String;
    function  GenerateName(const ACl: integer; const ACnt: integer): String;

    function  PreFormatOfInputData(const AFunctionName: String; const AValue: Variant): Variant; virtual;
    function  FormatErrorMessageForRow(const AErrorMessage: String): String; virtual; abstract;
    procedure FillDataSetWithDefaultValues(const ADataSet: TevClientDataSet; const AStage: TEvEchangeDefaultsStages); virtual; abstract;
    function  ComputeCalculatedFields(const ADataSet: TevClientDataSet): boolean; virtual; abstract;  // returns true, if anything is changed in dataset
    procedure CheckValuesBeforeChange(const ATableName: String; const AOperation: char; const AOldValues: TevClientDataSet; const ANewValues: IisListOfValues); virtual;
    procedure InitListOfKeyFieldsWithCalculatedDefaults; virtual; abstract;
    function  GetListOfKeyFieldsWithCalculatedDefaults: IisStringListRO; virtual; abstract;
    procedure CalculateDefaultsForKeyFields(const ARowValues: IisListOfValues); virtual; abstract;
    procedure AddAdditionalParamsToLog(const ALogRecord: IevExchangeChangeRecord; const APostedParams: IisListOfValues); virtual; abstract;
    function  GetInputDataSortIndexFieldNames: String; virtual; abstract;
    procedure AddWarningIfLookupFailed(const APackageField: IEvExchangePackageField; const AOldValue, ANewValue: Variant; AUsedMapRecord: IEvImportMapRecord); virtual;

    procedure FillChangeLog(const AOperation: Char; const AClientNbr: integer; const ATableName: String;
      const ARecordNbr: integer; const APostedParams: IisListOfValues); virtual;  // AOperation could be 'I' or 'U'
    procedure CheckValueAgainstLOV(const AValue: Variant;
      const APackageField: IEvExchangePackageField);  virtual;
    procedure CheckInputDataWithMap; virtual;
    function  DoTrim(const AValue: Variant): Variant; virtual;
    procedure DoImport; virtual;
    procedure DoCheckMapFile; virtual;
    procedure ImportRow; virtual;
    procedure DeleteUnmappedGroupRecords(const AMapFile: IEvExchangeMapFile;  const APackage: IEvExchangePackage);
    procedure AfterImport; virtual;
    procedure BeforeImport; virtual;
    procedure PostUpdates(AUpdateArray: TUpdateRecordArray; const ARowMappedValues: IislistOfValues); virtual;

    // IEvExchangeEngine implementation
    function  Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile; const APackage: IevExchangePackage;
      const AOptions: IisStringListRO; const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues;  // input dataset can be changed after import!!!
    function  CheckMapFile(const AMapFile: IEvExchangeMapFile;  const APackage: IEvExchangePackage; const AOptions: IisStringListRO): IisListOfValues;
  public
    destructor Destroy; override;
  end;

implementation

uses EvImportMapRecord, EvExchangeUtils, kbmMemTable, isTypes, isThreadManager,
  rwEngineTypes, ISErrorUtils, StrUtils, EvExchangeConsts;

{ TEvExchangeEngine }

procedure TEvExchangeEngine.CheckInputDataWithMap;
var
  MapField: IevEchangeMapField;
  MapFields: IisListOfValues;
  DistinctFieldsList: IisStringList;
  i, j: integer;
  bFlag: boolean;
begin
  // comparing amount of distinct fields in map file with amount of fields in input dataset
  DistinctFieldsList := TisStringList.Create;
  MapFields := FMapFile.GetFieldsList;
  for i := 0 to MapFields.Count - 1 do
  begin
    MapField := IInterface(MapFields.Values[i].Value) as IevEchangeMapField;
    if DistinctFieldsList.IndexOf(MapField.GetInputFieldName) = -1 then
      DistinctFieldsList.Add(MapField.GetInputFieldName);
  end;
  if FInputData.vclDataset.FieldDefs.Count < DistinctFieldsList.Count then
    FErrorsList.AddEvent(etFatalError, 'Input file has less fields than it supposed. Input fields: '
      + IntToStr(FInputData.vclDataset.FieldDefs.Count) + ' Supposed to have: ' + IntToStr(FMapFile.GetFieldsList.Count));

  // checking fields by names
  for i := 0 to FMapFile.GetFieldsList.Count - 1 do
  begin
    MapField := IInterface(FMapFile.getFieldsList.Values[i].Value) as IevEchangeMapField;
    bFlag := False;
    for j := 0 to FInputData.vclDataset.FieldDefs.Count - 1 do
    begin
      if  AnsiSameText(FInputData.vclDataset.FieldDefs[j].Name, MapField.getInputFieldName) then
      begin
        bFlag := true;
        break;
      end;
    end;  // for j
    if not bFlag then
      FErrorsList.AddEvent(etFatalError, 'Cannot find map field "' + MapField.getInputFieldName + '" in input dataset.');

    // checking that AsOfDate input field is presented in input dataset
    if MapField.GetAsOfDateEnabled and (MapField.GetAsOfDateSource = stMappedField) then
    begin
      bFlag := False;
      for j := 0 to FInputData.vclDataset.FieldDefs.Count - 1 do
      begin
        if  AnsiSameText(FInputData.vclDataset.FieldDefs[j].Name, MapField.GetAsOfDateField) then
        begin
          bFlag := true;
          break;
        end;
      end;  // for j
      if not bFlag then
        FErrorsList.AddEvent(etFatalError, 'Field "' + MapField.GetAsOfDateField + '" is used as effective date source. Cannot find this field in input dataset.');
    end;
  end;  // for i
end;

function TEvExchangeEngine.CheckMapFile(const AMapFile: IEvExchangeMapFile;
  const APackage: IEvExchangePackage;
  const AOptions: IisStringListRO): IisListOfValues;
begin
  FCoNbr := -1;
  FCustomCoNbr := '';
  FInputData := nil;
  FMapFile := AMapFile;
  FPackage := APackage;
  FOptions := AOptions;
  FImportedCount := 0;
  FRowsCount := 0;

  FErrorsList := TEvExchangeResultLog.Create;
  FErrorsList.Clear;

  FCheckMapResult := TisListOfValues.Create;
  FChangeLogList.Clear;

  FImportResult := nil;
  FEffectiveDate := SysTime;
  FNotCheckCustomCompanyNbr := true;
  FCustomCompanyAssigned := false;
  FDefaultsCalculator := nil;

  DeleteUnmappedGroupRecords(FMapFile, FPackage);
  InitListOfKeyFieldsWithCalculatedDefaults;

  DOCheckMapFile;

  Result := FCheckMapResult;
end;

destructor TEvExchangeEngine.Destroy;
begin

  inherited;
end;

procedure TEvExchangeEngine.DoCheckMapFile;
var
  i, j: integer;
  MapField, RelatedMapField: IevEchangeMapField;
  PackageField, RelatedPackageField: IEvExchangePackageField;
  PackageFields: IisListOfValues;
  KeyValues: IisStringListRO;
  TablesList: IisStringList;
  KeyFieldsWithCalcdefaults: IisStringListRO;
begin
  KeyFieldsWithCalcdefaults := GetListOfKeyFieldsWithCalculatedDefaults;

  if not AnsiSameText(FMapFile.GetPackageName, FPackage.GetName) then
  begin
    FErrorsList.AddEvent(etFatalError, 'Package''s name in map file "' + FMapFile.GetPackageName +
      '" does not match current package name "' + FPackage.GetName + '"');
    exit;
  end;

  if not (FPackage.GetVersion >= FMapFile.GetPackageVersion) then
  begin
    FErrorsList.AddEvent(etFatalError, 'Package has version less than required by map file. Packge version is '
      + IntToStr(FPackage.GetVersion) + ' Required version is ' + IntToStr(FMapFile.GetPackageVersion));
    exit;
  end;

  TablesList := TisStringList.Create;

  for i := 0 to FMapFile.GetFieldsList.Count - 1 do
  begin
    // checking that all mapped field has proper fields in package
    MapField := IInterface(FMapFile.GetFieldsList.Values[i].Value) as IevEchangeMapField;
    try
      PackageField := IInterface(FPackage.GetFieldsList.Value[MapField.GetExchangeFieldName]) as IEvExchangePackageField;
    except
      on E:Exception do
      begin
        FErrorsList.AddEvent(etFatalError, 'Field "' + MapField.GetExchangeFieldName + '" is mapped but it is not found in package');
        PackageField := nil;
      end;
    end;

    if Assigned(PackageField) then
    begin
      // filling list of tables
      if (not (PackageField.IsKeyField)) and (TablesList.IndexOf(PackageField.GetEvTableName) = -1) then
        TablesList.Add(PackageField.GetEvTableName);

      // checking that all related field are included in map
      if PackageField.GetRelatedParentField <> '' then
      begin
        RelatedMapField := FMapFile.GetFieldByExchangeName(PackageField.GetRelatedParentField);
        if not Assigned(RelatedMapField) then
          FErrorsList.AddEvent(etFatalError, 'Input field "' + MapField.GetInputFieldName + '" is mapped to "' + PackageField.GetExchangeFieldName + '" package field. This package''s field is related with "' + PackageField.GetRelatedParentField +
            '" which is not included in map');
      end;

      // creating list of used key
      KeyValues := PackageField.GetKeyValues;
      for j := 0 to KeyValues.Count - 1 do
        if (not AnsiSameText(KeyValues[j], UseSelfValue)) and (not AnsiSameText(KeyValues[j], UseNullValue))
          and (not AnsiSameText(KeyValues[j], CustomCompanyNbrPackageField)) then
        begin
          RelatedMapField := FMapFile.GetFieldByExchangeName(KeyValues[j]);
          RelatedPackageField := FPackage.GetFieldByName(KeyValues[j]);
          if (not Assigned(RelatedMapField)) and AnsiSameText(PackageField.GetEvTableName, RelatedPackageField.GetEvTableName)
            and (KeyFieldsWithCalcdefaults.IndexOf(RelatedPackageField.GetExchangeFieldName) = -1) then
            FErrorsList.AddEvent(etFatalError, 'EvoExchange field "' + MapField.GetExchangeFieldName + '". You should map input field for ' + RelatedPackageField.GetDisplayName + ' package field');
        end;

      // checking AsOfDate rules
      if MapField.GetAsOfDateEnabled then
      begin
        if not PackageField.GetAsOfDateEnabled then
          FErrorsList.AddEvent(etFatalError, 'EvoExchange field "' + MapField.GetExchangeFieldName + '". AsOfDate logic is enabled for field in map file but it is disabled in package')
        else
        begin
          if MapField.GetAsOfDateSource = stMappedField then
          begin
            if Trim(MapField.GetAsOfDateField) = '' then
              FErrorsList.AddEvent(etFatalError, 'EvoExchange field "' + MapField.GetExchangeFieldName + '". Input field name cannot be empty for this type of AsOfDate');
            if Trim(MapField.GetAsOfDateFieldDateFormat) = '' then
              FErrorsList.AddEvent(etFatalError, 'EvoExchange field "' + MapField.GetExchangeFieldName + '". Date format string cannot be empty for this type of AsOfDate');
          end;
        end;
      end;
    end;
  end;

  if FMapFile.GetFieldsList.Count = 0 then
    FErrorsList.AddEvent(etFatalError, 'No fields are mapped');

  PackageFields := FPackage.GetFieldsList;
  for i := 0 to PackageFields.Count - 1 do
  begin
    PackageField := IInterface(PackageFields.Values[i].Value) as IEvExchangePackageField;
    if PackageField.IsGroupDefinitionField then
      continue;

    // checking that all always required package's fields are mapped
    if PackageField.GetAlwaysRequired then
    begin
      MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);
      if (not Assigned(MapField)) then
        FErrorsList.AddEvent(etFatalError, 'You should map input field for "' + PackageField.GetDisplayName + '" package field because it is required.');
    end;
    // checking RequiredIfAnyMappedForTable fields
    if PackageField.GetRequiredIfAnyMappedForTable then
    begin
      if TablesList.IndexOf(PackageField.GetEvTableName) <> - 1 then
      begin
        MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);
        if (not Assigned(MapField)) then
          FErrorsList.AddEvent(etFatalError, 'You should map input field for "' + PackageField.GetDisplayName + '" package field because it is required for "' +
            PackageField.GetEvTableName + '" table.');
      end;
    end;
    // checking RequiredIfAnyMappedForOtherTable fields
    if PackageField.GetRequiredIfAnyMappedForOtherTable <> '' then
    begin
      if TablesList.IndexOf(PackageField.GetRequiredIfAnyMappedForOtherTable) <> - 1 then
      begin
        MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);
        if (not Assigned(MapField)) then
          FErrorsList.AddEvent(etFatalError, 'You should map input field for "' + PackageField.GetDisplayName + '" package field because it is required, if anything is mapped for "' +
            PackageField.GetRequiredIfAnyMappedForOtherTable + '" table.');
      end;
    end;
  end;

  // checking that all required fields are mapped within used tables
{ commented for #68220, then it will be restored
  PackageFields := FPackage.GetFieldsList;
  for i := 0 to TablesList.Count - 1 do
    for j := 0 to PackageFields.Count - 1 do
    begin
      PackageField := IInterface(PackageFields.Values[j].Value) as IEvExchangePackageField;
      if AnsiSameText(PackageField.GetEvTableName, TablesList[i]) then
        if (PackageField.GetRequiredByDD) and (not PackageField.GetIgnoreRequiredByDD) then
        begin
          MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);
          if (not Assigned(MapField)) and (PackageField.GetDefaultValue = '') and (not PackageField.IsCalcDefault) then
            FErrorsList.AddEvent(etFatalError, 'Package field "' + PackageField.GetExchangeFieldName + '" is required by data dictionary. No input field mapped to it.');
        end;
    end;
}

  FCheckMapResult.AddValue(EvoXExtErrorsList, FErrorsList);
  FCheckMapResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
  FCheckMapResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
end;

procedure TEvExchangeEngine.DoOnConstruction;
begin
  inherited;
  FMappedTablesList := TisStringList.Create;
  FChangeLogList := TisListOfValues.Create;
  FImpEngine := TIEMap.Create(sEvoXImport, FillDataSetWithDefaultValues, FillChangeLog, ComputeCalculatedFields, CheckValuesBeforeChange);
end;

function TEvExchangeEngine.GetMapDefs: TEvImportMapRecords;
var
  i, j: integer;
  PackageField: IEvExchangePackageField;
  PackageFields: IisListOfValues;
  MapField: IevEchangeMapField;
  MapFieldMap, PackageFieldMap, ResultMap: IEvImportMapRecord;
  tmpList: TStringList;

  function PrepareString(const AInputString: String): String;
  begin
    Result := StringReplace(AInputString, '"', '""', [rfReplaceAll]);
    Result := '"' + Result + '"';
  end;
begin
  SetLength(Result, 0);
  PackageFields := FPackage.GetFieldsList;

  for i := 0 to PackageFields.Count - 1 do
  begin
    PackageField := IInterface(PackageFields.Values[i].Value) as IEvExchangePackageField;
    PackageFieldMap := PackageField.GetMapRecord(PackageField.GetPackageMapRecordName);

    MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);
    if Assigned(MapField) then
    begin
      MapFieldMap := MapField.GetMapRecord;

      ResultMap := nil;
      if (MapFieldMap.MapType = mtpCustom) and (PackageFieldMap.MapType = mtpLookup) then
        raise Exception.Create('Illegal combination of map types: mtpCustom and mtpLookup')
      else if (MapFieldMap.MapType = mtpCustom) and (PackageFieldMap.MapType = mtpDirect) then
        ResultMap := MapFieldMap
      else if (MapFieldMap.MapType = mtpCustom) and (PackageFieldMap.MapType = mtpCustom) then
        ResultMap := PackageFieldMap
      else if (MapFieldMap.MapType = mtpDirect) and (PackageFieldMap.MapType = mtpDirect) then
        ResultMap := MapFieldMap
      else if (MapFieldMap.MapType = mtpDirect) and (PackageFieldMap.MapType = mtpCustom) then
        ResultMap := PackageFieldMap
      else if (MapFieldMap.MapType = mtpDirect) and (PackageFieldMap.MapType = mtpLookup) then
        ResultMap := PackageFieldMap
      else if (MapFieldMap.MapType = mtpMap) and (PackageFieldMap.MapType = mtpCustom) then
      begin
        // reformatting map values
        ResultMap := TEvImportMapRecord.CreateEmpty;
        ResultMap.SetMapRecordName(MapFieldMap.GetMapRecordName);
        ResultMap.Description := MapFieldMap.Description;
        ResultMap.SetVisibleToUser(MapFieldMap.IsVisibleToUser);
        ResultMap.MapType := mtpMapCustom;
        ResultMap.MapValues := MapFieldMap.MapValues;
        ResultMap.MapDefault := MapFieldMap.MapDefault;
        ResultMap.FuncName := PackageFieldMap.FuncName;
        tmpList := TStringList.Create;
        try
          tmpList.Text := ResultMap.MapValues;
          ResultMap.MapValues := '';
          for j := 0 to tmpList.Count - 1 do
          begin
            ResultMap.MapValues := ResultMap.MapValues + PrepareString(tmpList[j]);
            if j < (tmpList.Count - 1) then
              ResultMap.MapValues := ResultMap.MapValues + ',';
          end;
        finally
          FreeAndnil(tmpList);
        end;
      end
      else if (MapFieldMap.MapType = mtpMap) and (PackageFieldMap.MapType = mtpDirect) then
      begin
        // reformatting map values
        ResultMap := TEvImportMapRecord.CreateEmpty;
        ResultMap.SetMapRecordName(MapFieldMap.GetMapRecordName);
        ResultMap.Description := MapFieldMap.Description;
        ResultMap.SetVisibleToUser(MapFieldMap.IsVisibleToUser);
        ResultMap.MapType := mtpMap;
        ResultMap.MapValues := MapFieldMap.MapValues;
        ResultMap.MapDefault := MapFieldMap.MapDefault;
        tmpList := TStringList.Create;
        try
          tmpList.Text := ResultMap.MapValues;
          ResultMap.MapValues := '';
          for j := 0 to tmpList.Count - 1 do
          begin
            ResultMap.MapValues := ResultMap.MapValues + PrepareString(tmpList[j]);
            if j < (tmpList.Count - 1) then
              ResultMap.MapValues := ResultMap.MapValues + ',';
          end;
        finally
          FreeAndnil(tmpList);
        end;
      end
      else if (MapFieldMap.MapType = mtpMap) and (PackageFieldMap.MapType = mtpLookup) then
      begin
        // reformatting map values
        ResultMap := TEvImportMapRecord.CreateEmpty;
        ResultMap.SetMapRecordName(MapFieldMap.GetMapRecordName);
        ResultMap.Description := MapFieldMap.Description;
        ResultMap.SetVisibleToUser(MapFieldMap.IsVisibleToUser);
        ResultMap.MapType := mtpMapLookup;
        ResultMap.MLookupTable := PackageFieldMap.LookupTable;
        ResultMap.MLookupField := PackageFieldMap.LookupField;
        ResultMap.MMapValues := MapFieldMap.MapValues;
        ResultMap.MMapDefault := MapFieldMap.MapDefault;
        tmpList := TStringList.Create;
        try
          tmpList.Text := ResultMap.MMapValues;
          ResultMap.MMapValues := '';
          for j := 0 to tmpList.Count - 1 do
          begin
            ResultMap.MMapValues := ResultMap.MMapValues + '"' + tmpList[j] + '"';
            if j < (tmpList.Count - 1) then
              ResultMap.MMapValues := ResultMap.MMapValues + ',';
          end;
        finally
          FreeAndnil(tmpList);
        end;
      end
      else
        Assert(false, 'Unknown combination of map types');
    end
    else
    begin
      // generating map record for not mapped key fields
      if (GetListOfKeyFieldsWithCalculatedDefaults.IndexOf(PackageField.GetExchangeFieldName) <> -1) and
        CheckFieldAgainsListOfMappedTables(PackageField.GetExchangeFieldName) then
        ResultMap := PackageFieldMap
      else
        continue;  
    end;

    if PackageField.GetUseEvoNamesForMap then
    begin
      ResultMap.ITable := PackageField.GetEvTableName;
      ResultMap.IField := PackageField.GetEvFieldName;
    end
    else
    begin
      ResultMap.ITable := '';
      ResultMap.IField := PackageField.GetExchangeFieldName;
    end;

    ResultMap.GroupName := PackageField.GetGroupName;
    ResultMap.RecordNumber := PackageField.GetRecordNumber;
    ResultMap.ETable := PackageField.GetEvTableName;
    ResultMap.EField := PackageField.GetEvFieldName;
    ResultMap.KeyField := PackageFieldMap.KeyField;
    ResultMap.KeyLookupTable := PackageFieldMap.KeyLookupTable;
    ResultMap.KeyLookupField := PackageFieldMap.KeyLookupField;
    SetLength(Result, Succ(Length(Result)));
    Result[High(Result)] := ResultMap;
  end;  // for i
end;

function TEvExchangeEngine.Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile;
  const APackage: IevExchangePackage; const AOptions: IisStringListRO;
  const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues;
var
  Q: IevQuery;
  OldClNbr, ClNbr: integer;
  CustomCompanyField: IevEchangeMapField;
  SortIndex: String;
  sIndexFields: String;

  procedure SimulateResult(const S: String);
  begin
    FErrorsList.AddEvent(etFatalError, S);
    FImportResult.AddValue(EvoXInputRowsAmount, FRowsCount);
    FImportResult.AddValue(EvoXImportedRowsAmount, FImportedCount);
    FImportResult.AddValue(EvoXExtErrorsList, FErrorsList);
    FImportResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
    FImportResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
  end;
begin
  CheckCondition(Assigned(AMapFile), 'Object with map data is not assigned');
  CheckCondition(Assigned(AInputData), 'Object with input data is not assigned');
  CheckCondition(Assigned(APackage), 'Package object is not assigned');

  FInputData := AInputData;
  FMapFile := AMapFile;
  FPackage := APackage;
  FOptions := AOptions;
  FImportedCount := 0;
  FNotCheckCustomCompanyNbr := false;
  FDefaultsCalculator := TEvExchangeDefCalculator.Create;
  ParseImportOptions;

  DeleteUnmappedGroupRecords(FMapFile, FPackage);
  InitListOfKeyFieldsWithCalculatedDefaults;

  FRowsCount := FInputData.vclDataSet.RecordCount;

  FErrorsList := TEvExchangeResultLog.Create;
  FErrorsList.Clear;

  FImportResult := TisListOfValues.Create;
  FCheckMapResult := TisListOfValues.Create;
  FChangeLogList.Clear;

  if AEffectiveDate <> 0 then
    FEffectiveDate := AEffectiveDate
  else
    FEffectiveDate := SysTime;

  if ctx_AccountRights.Functions.GetState('USER_CAN_IMPORT_USING_EVOX') <> stEnabled then
  begin
    SimulateResult('User has no rights to import using EvoX');
    Result := FImportResult;
    exit;
  end;

  try

    if Contains(Fpackage.GetName, EvoXClient) then
    begin
      CheckCondition(Assigned(FMapFile.GetFieldByExchangeName(NewCustomClientNbrPackageField)), 'Client Code Field has to be assigned');
    end
    else
    begin
      CustomCompanyField := FMapFile.GetFieldByExchangeName(CustomCompanyNbrPackageField);
      FCustomCompanyAssigned := Assigned(CustomCompanyField);
      if not FCustomCompanyAssigned then
        CheckCondition(ACustomCoNbr <> '', 'CoNbr has to be assigned');
    end;

    // sorting input data by CustomCompanyNbr or by CustomCompanyNbr and PayrollCheckdate
    SortIndex := '';
    sIndexFields := GetInputDataSortIndexFieldNames;

    if sIndexFields <> '' then
    begin
      SortIndex := RandomString(10);
      try
        (FInputData.vclDataSet as TkbmCustomMemTable).AddIndex(SortIndex, sIndexFields, []);
      except
        SimulateResult('"' + sIndexFields + '" field(s) does not exist in input file. Import cancelled');
        Result := FImportResult;
        exit;
      end;
      (FInputData.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
      (FInputData.vclDataSet as TkbmCustomMemTable).IndexName := SortIndex;
    end;

    try
        OldClNbr := ctx_DataAccess.ClientId;
        try
          if not Contains(Fpackage.GetName, EvoXClient) then
          begin
            FCoNbr := -1;
            FCustomCoNbr := ACustomCoNbr;

            // opening proper client database
            if not FCustomCompanyAssigned then
            begin
              // calculating CoNbr and ClNbr
              Q := TevQuery.Create('SELECT CL_NBR, CO_NBR FROM TMP_CO WHERE CUSTOM_COMPANY_NUMBER=''' + FCustomCoNbr + '''');
              Q.Execute;
              CheckCondition(Q.Result.RecordCount > 0, 'Company not found. CUSTOM_COMPANY_NUMBER=''' + FCustomCoNbr + '''');
              ClNbr := Q.Result.Fields[0].AsInteger;
              if ClNbr <> OldClNbr then
              begin
                ctx_DataAccess.OpenClient(ClNbr);
                FImpEngine.ClearDataSetsAndVariables;
              end;
              FCoNbr := Q.Result.Fields[1].AsInteger;
            end;
          end;

          try
            DoImport;
          finally
            Result := FImportResult;
          end;
        finally
          ctx_DataAccess.OpenClient(OldClNbr);
        end;
    finally
      if SortIndex <> '' then
      begin
        (FInputData.vclDataSet as TkbmCustomMemTable).IndexName := '';
        (FInputData.vclDataSet as TkbmCustomMemTable).DeleteIndex(SortIndex);
        (FInputData.vclDataSet as TkbmCustomMemTable).IndexDefs.Update;
      end;
    end;
  except
    on E: Exception do
    begin
      // creating a fake result with error stack
      SimulateResult('Import finished with exception: ' + E.Message + #13#10 + GetStack);
      Result := FImportResult;
    end;
  end;
end;

procedure TEvExchangeEngine.DoImport;
begin
  // checking map file
  DoCheckMapFile;
  if FErrorsList.Count > 0 then
  begin
    FErrorsList.AddEvent(etInformation, 'Map file has errors. Import cancelled');
    FImportResult.AddValue(EvoXInputRowsAmount, FRowsCount);
    FImportResult.AddValue(EvoXImportedRowsAmount, 0);
    FImportResult.AddValue(EvoXExtErrorsList, FErrorsList);
    FImportResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
    FImportResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
    exit;
  end;

  // checking that Fielddefs are  agree with Map fields
  CheckInputDataWithMap;

  if FErrorsList.Count > 0 then
  begin
    FImportResult.AddValue(EvoXInputRowsAmount, FRowsCount);
    FImportResult.AddValue(EvoXImportedRowsAmount, 0);
    FImportResult.AddValue(EvoXExtErrorsList, FErrorsList);
    FImportResult.AddValue(EvoXAllMessagesAmount, FErrorsList.Count);
    FImportResult.AddValue(EvoXRealErrorAmount, FErrorsList.GetAmountOfErrors);
    exit;
  end;

  AbortIfCurrentThreadTaskTerminated;
end;

procedure TEvExchangeEngine.ImportRow;
var
  i, l, m, n: integer;
  MapFields: IisListOfValues;
  MapField: IevEchangeMapField;
  PackageField: IEvExchangePackageField;
  PackageFields: IisListOfValues;
  RowValues: IisListOfValues;
  PackageKeyTables: IisStringListRO;
  PackageKeyFields: IisStringListRO;
  PackageKeyValues: IisStringListRO;
  bMappingResult: boolean;

  OTables, OFields: TStringArray;
  OValues: TVariantArray;
  KeyTables, KeyFields: TStringArray;
  KeyValues: TVariantArray;

  InputValue, ConvertedInputValue: Variant;
  UpdateRecord, ClearedUpdateRecord: TUpdateRecordArray;
  v: Variant;
  sTable, sField: String;
  sMappedExchangeFieldName: String;
  UsedMapRecord: IEvImportMapRecord;

  oldMapFile: IEvExchangeMapFile;
  oldPackage: IevExchangePackage;
  GroupsList: IisListOfValues;
  GroupRecordNumbers: TisDynIntegerArray;
  OneGroup: IEvExchangePackageGroup;
  bRecordIsEmpty: boolean;
  CustomFunctionsWarnings: IisStringList;
  PackageTablesList: IisStringList;
  iLogCount: integer;

  procedure AddWarningsFromCustomFunctions(const APackageField: IEvExchangePackageField; const ACustomFunctionsWarnings: IisStringList);
  var
    i: integer;
    S: String;
  begin
    for i := 0 to ACustomFunctionsWarnings.Count - 1 do
    begin
      S := 'Package field: "' + GetPackageFieldName(APackageField) + '". ' + ACustomFunctionsWarnings[i] + EvoXLookupFailedMessage;
      FErrorsList.AddEvent(etWarning, FormatErrorMessageForRow(S));
    end;
  end;

  function UpdateEffectiveDateIfNeeded: TDateTime;
  var
    InputEffectiveDate: Variant;
  begin
    Result := 0;
    if Assigned(MapField) then
      if MapField.GetAsOfDateEnabled then
      begin
        if MapField.GetAsOfDateSource = stDateEditor then
          Result := MapField.GetAsOfDateValue
        else
        begin
          InputEffectiveDate := FInputData.vclDataSet.Fields.FieldByName(MapField.GetAsOfDateField).Value;
          CheckCondition(InputEffectiveDate <> null, 'Input field''s value cannot be null because it is used as source of effective dates. Fieldname: "' + MapField.GetAsOfDateField +'"');
          Result := ConvertVariantToType(InputEffectiveDate, ddtDateTime, MapField.GetAsOfDateFieldDateFormat, 'hh:nn:ss');
        end;
      end;
  end;

  function CleanUpUpdateRecords(AUpdateRecord: TUpdateRecordArray): TUpdateRecordArray;
  var
   j: integer;
  begin
    SetLength(Result, 0);
    for j := Low(AUpdateRecord) to High(AUpdateRecord) do
      if not AUpdateRecord[j].NotPostThisRecord then
      begin
        SetLength(Result, Succ(Length(Result)));
        Result[High(Result)].KeyTables := Copy(AUpdateRecord[j].KeyTables);
        Result[High(Result)].KeyFields := Copy(AUpdateRecord[j].KeyFields);
        Result[High(Result)].KeyValues := Copy(AUpdateRecord[j].KeyValues);
        Result[High(Result)].Table := AUpdateRecord[j].Table;
        Result[High(Result)].Field := AUpdateRecord[j].Field;
        Result[High(Result)].oldValue := AUpdateRecord[j].oldValue;
        Result[High(Result)].Value := AUpdateRecord[j].Value;
        Result[High(Result)].EffectiveDate := AUpdateRecord[j].EffectiveDate;
        Result[High(Result)].GroupRecordNumber := AUpdateRecord[j].GroupRecordNumber;
        Result[High(Result)].UpdateAsOfDate := AUpdateRecord[j].UpdateAsOfDate;
        Result[High(Result)].NotPostThisRecord := AUpdateRecord[j].NotPostThisRecord;
      end;
  end;

  procedure MoveMappedValues;
  var
    j, k: integer;
  begin
    for j := Low(OTables) to High(OTables) do
    begin
      SetLength(UpdateRecord, Succ(Length(UpdateRecord)));
      SetLength(UpdateRecord[High(UpdateRecord)].KeyTables, Length(KeyTables));
      SetLength(UpdateRecord[High(UpdateRecord)].KeyFields, Length(KeyFields));
      SetLength(UpdateRecord[High(UpdateRecord)].KeyValues, Length(KeyValues));
      for k := 0 to High(KeyTables) do
      begin
        UpdateRecord[High(UpdateRecord)].KeyTables[k] := KeyTables[k];
        UpdateRecord[High(UpdateRecord)].KeyFields[k] := KeyFields[k];
        UpdateRecord[High(UpdateRecord)].KeyValues[k] := KeyValues[k];
      end;
      UpdateRecord[High(UpdateRecord)].Table := OTables[j];
      UpdateRecord[High(UpdateRecord)].Field := OFields[j];
      UpdateRecord[High(UpdateRecord)].Value := OValues[j];
      UpdateRecord[High(UpdateRecord)].EffectiveDate := FEffectiveDate;
      UpdateRecord[High(UpdateRecord)].UpdateAsOfDate := UpdateEffectiveDateIfNeeded;

      if PackageField.GetNotPostThisFieldFlag then
        UpdateRecord[High(UpdateRecord)].NotPostThisRecord := true
      else
        UpdateRecord[High(UpdateRecord)].NotPostThisRecord := false;

      if PackageField.IsBelongsToGroup then
        UpdateRecord[High(UpdateRecord)].GroupRecordNumber := PackageField.GetRecordNumber
      else
        UpdateRecord[High(UpdateRecord)].GroupRecordNumber := -1;
    end;
  end;

begin

  iLogCount := FChangeLogList.Count;
  try
    RowValues := TisListOfValues.Create;
    SetLength(UpdateRecord, 0);
    CustomFunctionsWarnings := TisStringList.Create;

    oldMapFile := FMapFile;
    oldPackage := FPackage;
    FMapFile := ((FMapFile as IisInterfacedObject).GetClone) as IEvExchangeMapFile;
    FPackage := ((FPackage as IisInterfacedObject).GetClone) as IevExchangePackage;
    try
      ///////////////////////////////////////////////////////////////////////////////
      // stage 1 - deleting mapped fields and package group records if all mapped values are null
      ///////////////////////////////////////////////////////////////////////////////
      MapFields := FMapFile.GetFieldsList;
      GroupsList := FPackage.GetGroupsList;
      for i := 0 to GroupsList.Count - 1 do
      begin
        OneGroup := IInterface(GroupsList.Values[i].Value) as IEvExchangePackageGroup;
        GroupRecordNumbers := FPackage.GetGroupRecordNumbers(OneGroup.GetName);
        PackageFields := FPackage.GetFieldsDefsByGroupName(OneGroup.GetName);
        for l := Low(GroupRecordNumbers) to High(GroupRecordNumbers) do
        begin
          bRecordIsEmpty := true;
          for m := 0 to PackageFields.Count - 1 do
          begin
            PackageField := IInterface(PackageFields.Values[m].Value) as IEvExchangePackageField;
            MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName + ' ' + RecordNumberToStr(GroupRecordNumbers[l]));
            if Assigned(MapField) then
            begin
              InputValue := FInputData.vclDataSet.Fields.FieldByName(MapField.GetInputFieldName).Value;
              if (InputValue <> null) and (InputValue <> '') and
                not ((FHandleNullsOption = tSpecial) and ValueIsSpecialNullString(InputValue)) then
              begin
                bRecordIsEmpty := false;
                break;
              end;
            end;
          end;  // for m

          if bRecordIsEmpty then
          begin
            // deleting record in package and fields in map file
            for m := 0 to PackageFields.Count - 1 do
            begin
              PackageField := IInterface(PackageFields.Values[m].Value) as IEvExchangePackageField;
              MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName + ' ' + RecordNumberToStr(GroupRecordNumbers[l]));
              if Assigned(MapField) then
                FMapFile.DeleteFieldByExchangeName(MapField.GetExchangeFieldName);
            end;
            FPackage.DeleteRecordFromGroup(OneGroup.GetName, GroupRecordNumbers[l]);
          end;  // m
        end;  // for l
      end;  // i

      ///////////////////////////////////////////////////////////////////////////////
      // stage 2 - calculating values for mapped fields
      ///////////////////////////////////////////////////////////////////////////////
      MapFields := FMapFile.GetFieldsList;
      PackageFields := FPackage.GetFieldsList;
      for i := 0 to MapFields.Count - 1 do
      begin
        MapField := IInterface(MapFields.Values[i].Value) as IevEchangeMapField;
        PackageField := FPackage.GetFieldByName(MapField.GetExchangeFieldName);

        InputValue := FInputData.vclDataSet.Fields.FieldByName(MapField.GetInputFieldName).Value;
        if (InputValue = null) or (InputValue = '') then
        begin
          if PackageField.GetRequiredByDD and
            (MapField.GetInputFieldName <> 'Check Number') then //reso #95628, if Check Number is mapped but is Empty it will be automatically set with ctx_PayrollCalculation.GetNextPaymentSerialNbr, see EvExchangeGenericPrEngine
            CheckCondition((PackageField.GetDefaultValue <> '') or (PackageField.IsCalcDefault) or (not PackageField.GetCheckRequiredByDD),
              'Package field: "' + GetPackageFieldName(PackageField) + '" is required. But input data is null ');

          RowValues.AddValue(MapField.GetExchangeFieldName, null);
        end
        else
        begin
          // converted value to destination type
          if (FHandleNullsOption <> tSpecial) or not ValueIsSpecialNullString(InputValue) then
          begin
            try
              if (MapField.GetMapRecord.MapType = mtpDirect) and
                (PackageField.GetMapRecord(PackageField.GetPackageMapRecordName).MapType = mtpDirect) then
                ConvertedInputValue := ConvertVariantToType(InputValue, MapField.GetFieldType, MapField.GetDateFormat, MapField.GetTimeFormat)
              else
                ConvertedInputValue := InputValue;
            except
              on E:Exception do
              begin
                raise Exception.Create('Input field: "' + MapField.GetInputFieldName + '". Package field: "' + MapField.GetExchangeFieldName +
                  '". Conversion error: ' + E.Message);
              end;
            end;

            // hardcoded formating of field if necessary
            if PackageField.GetTrimInputSpaces then
              ConvertedInputValue := DoTrim(ConvertedInputValue);

            ConvertedInputValue := PreFormatOfInputData(PackageField.GetPreFormatFunctionName, ConvertedInputValue);

            // storing value
            RowValues.AddValue(MapField.GetExchangeFieldName, ConvertedInputValue);
          end
          else
          begin
            if PackageField.GetRequiredByDD then
              CheckCondition((PackageField.GetDefaultValue <> '') or (PackageField.IsCalcDefault),
                'Package field: "' + GetPackageFieldName(PackageField) + '" is required. But input data is null');
            RowValues.AddValue(MapField.GetExchangeFieldName, InputValue);
          end;
        end;
      end;  // for i

      // adding CUSTOM_COMPANY_NBR is this field is missed but we have CoNbr as parameter
      if (not RowValues.ValueExists(CustomCompanyNbrPackageField)) and (FCustomCoNbr <> '') then
        RowValues.AddValue(CustomCompanyNbrPackageField, FCustomCoNbr);

      // adding default values for key fields if they not mapped
      CalculateDefaultsForKeyFields(RowValues);

      ///////////////////////////////////////////////////////////////////////////////
      // loop by Ev and Virtial tables
      ///////////////////////////////////////////////////////////////////////////////
{      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try}
        PackageTablesList := FPackage.GetListOfEvAndVirtualTables;
        CheckCondition(PackageTablesList.Count > 0, 'List of tables in package is empty');
        for n := 0 to PackageTablesList.Count - 1 do
        begin
          PackageFields := FPackage.GetFieldsForEvOrVirtualTable(PackageTablesList[n]);
          SetLength(UpdateRecord, 0);

          ///////////////////////////////////////////////////////////////////////////////
          // stage 3 - mapping values and updating stored values by new ones
          ///////////////////////////////////////////////////////////////////////////////
          for i := 0 to PackageFields.Count - 1 do
          begin
            PackageField := IInterface(PackageFields.Values[i].Value) as IEvExchangePackageField;
            MapField := FMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);

            if Assigned(MapField) then
              sMappedExchangeFieldName := MapField.GetExchangeFieldName
            else
            begin
              if (GetListOfKeyFieldsWithCalculatedDefaults.IndexOf(PackageField.GetExchangeFieldName) = -1) or
                (not CheckFieldAgainsListOfMappedTables(PackageField.GetExchangeFieldName)) then
                continue
              else
              begin
                // simulating that key field with default value is mapped
                sMappedExchangeFieldName := PackageField.GetExchangeFieldName;
              end;
            end;

            // considering how to work with null values
            if RowValues.Value[sMappedExchangeFieldName] = null then
            begin
              if (FHandleNullsOption = tIgnore) or (FHandleNullsOption = tSpecial) then
                Continue;
            end
            else
            begin
              if (FHandleNullsOption = tSpecial) and ValueIsSpecialNullString(RowValues.Value[sMappedExchangeFieldName]) then
                RowValues.Value[sMappedExchangeFieldName] := null;
            end;

            // filling KeyTables, KeyFields, KeyValues
            PackageKeyTables := PackageField.GetKeyTables;
            PackageKeyFields := PackageField.GetKeyFields;
            PackageKeyValues := PackageField.GetKeyValues;

            SetLength(KeyTables, 0);
            SetLength(KeyFields, 0);
            SetLength(KeyValues, 0);

            for l := 0 to PackageKeyTables.Count - 1 do
            begin
              if PackageKeyValues[l] = UseSelfValue then
              begin
                v := RowValues.Value[sMappedExchangeFieldName];
                CheckCondition(v <> null, 'Input value is null for key package field: "' + sMappedExchangeFieldName + '"');
              end
              else if PackageKeyValues[l] = UseNullValue then
              begin
                v := null;
              end
              else
              begin
                if RowValues.ValueExists(PackageKeyValues[l]) then
                  v := RowValues.Value[PackageKeyValues[l]]
                else
                  v := null;
              end;

              if (v <> null) or (PackageKeyValues[l] = UseNullValue)then
              begin
                SetLength(KeyTables, Succ(Length(KeyTables)));
                SetLength(KeyFields, Succ(Length(KeyFields)));
                SetLength(KeyValues, Succ(Length(KeyValues)));

                KeyTables[High(KeyTables)] := PackageKeyTables[l];
                KeyFields[High(KeyFields)] := PackageKeyFields[l];
                KeyValues[High(KeyValues)] := v;
              end;
            end;  // l

            if PackageField.GetUseEvoNamesForMap then
            begin
              sTable := PackageField.GetEvTableName;
              sField := PackageField.GetEvFieldName;
            end
            else
            begin
              sTable := '';
              sField := PackageField.GetExchangeFieldName;
            end;

            if Assigned(FFunctionsDefinitions) then
            begin
              FFunctionsDefinitions.SetRowValues(RowValues);
              FFunctionsDefinitions.SetCurrentPackageField(PackageField);
              CustomFunctionsWarnings.Clear;
              FFunctionsDefinitions.SetCustomWarnings(CustomFunctionsWarnings);
            end;
            try
              try
                bMappingResult := FImpEngine.MapValue(True, sTable, sField, RowValues.Value[PackageField.GetExchangeFieldName],
                   KeyTables, KeyFields, KeyValues, OTables, OFields, OValues, UsedMapRecord, PackageField.GetGroupName, PackageField.GetRecordNumber);
              except
                on E: Exception do
                  raise Exception.Create('Package field: "' + GetPackageFieldName(PackageField) + '". ' + E.Message);
              end;

              if bMappingResult then
              begin
                // update value in RowValues
                for l := Low(OTables) to High(OTables) do
                  if AnsiSameText(OTables[l], PackageField.GetEvTableName) and AnsiSameText(OFields[l], PackageField.GetEvFieldName) then
                  begin
                    AddWarningIfLookupFailed(PackageField,  RowValues.Value[PackageField.GetExchangeFieldName], OValues[l], UsedMapRecord);
                    AddWarningsFromCustomFunctions(PackageField, CustomFunctionsWarnings);
                    RowValues.Value[PackageField.GetExchangeFieldName] := OValues[l];
                    CheckValueAgainstLOV(RowValues.Value[PackageField.GetExchangeFieldName], PackageField);
                    break;
                  end;

                // store
                MoveMappedValues;
              end
              else
                Assert(false, 'MapValue returned false. Package field: "' + GetPackageFieldName(PackageField) + '"');
            finally
              if Assigned(FFunctionsDefinitions) then
              begin
                FFunctionsDefinitions.SetRowValues(nil);
                FFunctionsDefinitions.SetCurrentPackageField(nil);
              end;
            end;
          end;  // i

          ///////////////////////////////////////////////////////////////////////////////
          // stage 4 - update Key values in all update records with new values from RowValues
          ///////////////////////////////////////////////////////////////////////////////
          for i := Low(UpdateRecord) to High(UpdateRecord) do
          begin
            for l := 0 to RowValues.Count - 1 do
            begin
              PackageField := FPackage.GetFieldByName(RowValues.Values[l].Name);

{              if (PackageField.IsBelongsToGroup) and
                (GetListOfKeyFieldsWithCalculatedDefaults.IndexOf(PackageField.GetExchangeFieldName) <> -1) then // possible problem - we cannot update key values after mapping but I see no way
                begin
                  continue;
                end;}
              for m := Low(UpdateRecord[i].KeyTables) to High(UpdateRecord[i].KeyTables) do
              begin
                if AnsiSameText(UpdateRecord[i].KeyTables[m], PackageField.GetEvTableName) and
                  AnsiSameText(UpdateRecord[i].KeyFields[m], PackageField.GetEvFieldName) then
                begin
                  if not PackageField.IsBelongsToGroup then
                  begin
                    CheckCondition(RowValues.Values[l].Value <> null, 'Nulls not allowed for key values. Package field: ' + GetPackageFieldName(PackageField) +
                      ' Table: ' + PackageField.GetEvTableName + ' Field: ' + PackageField.GetEvFieldName);
                    UpdateRecord[i].KeyValues[m] := RowValues.Values[l].Value;
                  end
                  else
                  begin
                    if PackageField.GetRecordNumber = UpdateRecord[i].GroupRecordNumber then
                    begin
                      CheckCondition(RowValues.Values[l].Value <> null, 'Nulls not allowed for key values. Package field: ' + GetPackageFieldName(PackageField) +
                        ' Table: ' + PackageField.GetEvTableName + ' Field: ' + PackageField.GetEvFieldName);
                      UpdateRecord[i].KeyValues[m] := RowValues.Values[l].Value;
                    end;
                  end;
                  break;
                end;
              end; // for m
            end;  // for l

            // reso #95657, Ee Hr Data package, Ee Property Tracking Group
            // the fields that reperesent logic key "Property, Issue Date, Serial Number" should be imported
            // even if there are no other fields provided
            if (UpdateRecord[i].Table = 'EE_HR_PROPERTY_TRACKING') and UpdateRecord[i].NotPostThisRecord then
            begin
              l := 0;
              for m := Low(UpdateRecord[i].KeyFields) to High(UpdateRecord[i].KeyFields) do
                if (UpdateRecord[i].KeyFields[m] = 'CO_HR_PROPERTY_NBR') or (UpdateRecord[i].KeyFields[m] = 'ISSUED_DATE') or
                  (UpdateRecord[i].KeyFields[m] = 'SERIAL_NUMBER') then
                  Inc(l);
              if l = 3 then
                UpdateRecord[i].NotPostThisRecord := False;
            end;
          end;

          // Post changes for current table
          if Length(UpdateRecord) > 0 then
          begin
            ClearedUpdateRecord := CleanUpUpdateRecords(UpdateRecord);
            PostUpdates(ClearedUpdateRecord, RowValues);
          end;
        end;  // for n
{      except
        on E: Exception do
        begin
          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      end;
      ctx_DataAccess.CommitNestedTransaction;}
    finally
      FMapFile := oldMapFile;
      FPackage := oldPackage;
    end;
  except
    on E: Exception do
    begin
      for i := FChangeLogList.Count downto (iLogCount + 1) do
        FChangeLogList.DeleteValue(i - 1);
      raise;
    end;
  end;
end;

procedure TEvExchangeEngine.CheckValueAgainstLOV(const AValue: Variant; const APackageField: IEvExchangePackageField);
var
  FieldValues : IisListOfValues;
  i: integer;
begin
  if VarIsArray(AValue) or (AValue = null) then exit;
  CheckCondition(Assigned(APAckageField), 'Package field object is not assigned');

  FieldValues := APackageField.GetFieldValues;
  if FieldValues.Count > 0 then
  begin
    for i := 0 to FieldValues.Count - 1 do
    begin
      if AnsiSameStr(VarToStr(AValue), VarToStr(FieldValues.Values[i].Name)) then
        exit;
    end;
    raise Exception.Create('Package field "' + GetPackageFieldName(APackageField) +
      '" has impossible value: ' + VarToStr(AValue));
  end;
end;

function TEvExchangeEngine.DoTrim(const AValue: Variant): Variant;
var
  S: String;
begin
  Result := AValue;
  if Result = null then
    exit;
  S := Result;
  Result := Trim(S);
end;

procedure TEvExchangeEngine.FillListOfMappedTables;
var
  i: integer;
  MapField: IevEchangeMapField;
  PackageField: IEvExchangePackageField;
begin
  FMappedTablesList.Clear;
  for i := 0 to FMapFile.GetFieldsList.Count - 1 do
  begin
    MapField := IInterface(FMapFile.GetFieldsList.Values[i].Value) as IevEchangeMapField;
    PackageField := IInterface(FPackage.GetFieldsList.Value[MapField.GetExchangeFieldName]) as IEvExchangePackageField;
    if FMappedTablesList.IndexOf(PackageField.GetEvTableName) = -1 then
      FMappedTablesList.Add(PackageField.GetEvTableName);
  end;
end;

function TEvExchangeEngine.CheckFieldAgainsListOfMappedTables(const AExchangeFieldName: String): boolean;
var
  PackageField: IEvExchangePackageField;
begin
  PackageField := FPackage.GetFieldByName(AExchangeFieldName);
  Result := FMappedTablesList.IndexOf(PackageField.GetEvTableName) <> -1;
end;

procedure TEvExchangeEngine.DeleteUnmappedGroupRecords(const AMapFile: IEvExchangeMapFile;  const APackage: IEvExchangePackage);
var
  PackageField: IEvExchangePackageField;
  MapField: IevEchangeMapField;
  i, j, k: integer;
  Group: IEvExchangePackageGroup;
  GroupRecordNumbers: TisDynIntegerArray;
  bFound: boolean;
begin
  CheckCondition(Assigned(AMapFile), 'Map file not assigned');
  CheckCondition(Assigned(APackage), 'Package not assigned');

  for i := 0 to APackage.GetGroupsList.Count - 1 do
  begin
    Group := IInterface(APackage.GetGroupsList.Values[i].Value) as IEvExchangePackageGroup;

    GroupRecordNumbers := APackage.GetGroupRecordNumbers(Group.GetName);
    for j := High(GroupRecordNumbers) downto Low(GroupRecordNumbers) do
    begin
      bFound := false;
      for k := 0 to APackage.GetFieldsList.Count - 1 do
      begin
        PackageField := IInterface(APackage.GetFieldsList.Values[k].Value) as IEvExchangePackageField;
        if (PackageField.GetGroupName = Group.GetName) and (not PackageField.IsGroupDefinitionField)
          and (PackageField.GetRecordNumber = GroupRecordNumbers[j]) then
        begin
          MapField := AMapFile.GetFieldByExchangeName(PackageField.GetExchangeFieldName);
          if Assigned(MapField) then
          begin
            bFound := true;
            break;
          end;
        end;
      end;  // for k
      if not bFound then
        APackage.DeleteRecordFromGroup(Group.GetName, GroupRecordNumbers[j]);
    end;  // for j
  end;  // for i
end;

procedure TEvExchangeEngine.FillChangeLog(const AOperation: Char; const AClientNbr: integer;
  const ATableName: String; const ARecordNbr: integer; const APostedParams: IisListOfValues);
var
  LogRecord: IevExchangeChangeRecord;
begin
  LogRecord := TevExchangeChangeRecord.Create(AOperation, AClientNbr, ATableName, ARecordNbr);
  AddAdditionalParamsToLog(LogRecord, APostedParams);
  FChangeLogList.AddValue(GenerateName(AClientNbr, FChangeLogList.Count + 1), LogRecord);
end;

procedure TEvExchangeEngine.AfterImport;
begin

end;

procedure TEvExchangeEngine.ParseImportOptions;
var
  MapFileOptions: IisStringListRO;
  sNullOption, sNullString: String;
begin
  FHandleNullsOption := tIgnore;
  FNullSpecialString := '';

  // options as call's parameter are override options from map file
  MapFileOptions := FMapFile.GetMapFileOptions as IisStringListRO;

  // Null values handling
  sNullOption := EvoXIgnoreNulls;
  sNullString := '';
  if Assigned(FOptions) and (FOptions.IndexOfName(EvoXHowToHandleNullsOption) <> -1) then
    sNullOption := FOptions.Values[EvoXHowToHandleNullsOption]
  else if MapFileOptions.IndexOfName(EvoXHowToHandleNullsOption) <> -1 then
    sNullOption := MapFileOptions.Values[EvoXHowToHandleNullsOption];

  if Assigned(FOptions) and (FOptions.IndexOfName(EvoXSpecialNullStringOption) <> -1) then
    sNullString := FOptions.Values[EvoXSpecialNullStringOption]
  else if MapFileOptions.IndexOfName(EvoXSpecialNullStringOption) <> -1 then
    sNullString := MapFileOptions.Values[EvoXSpecialNullStringOption];

  if sNullOption = EvoXIgnoreNulls then
    FHandleNullsOption := tIgnore
  else if sNullOption = EvoXReplaceValueByNull then
    FHandleNullsOption := tReplace
  else if sNullOption = EvoXUseSpecialNullString then
  begin
    FHandleNullsOption := tSpecial;
    FNullSpecialString := sNullString;
    CheckCondition(FNullSpecialString <> '', 'Empty string cannot be used as special null string''s value');
  end
  else
    raise Exception.Create('Unknown value for how to handle nulls option: "' + sNullOption + '"');
end;

function TEvExchangeEngine.ValueIsSpecialNullString(const AValue: Variant): boolean;
begin
  Result := false;
  CheckCondition(FHandleNullsOption = tSpecial, 'Internal error. How to handle nulls options has wrong value'); 
  if AnsiSameStr(VarToStr(AValue), FNullSpecialString) then
    Result := true;
end;

function TEvExchangeEngine.GetPackageFieldName(const APackageField: IEvExchangePackageField): String;
begin
  CheckCondition(Assigned(APackageField), 'Package field object should be assigned');
  if APackageField.GetDisplayName <> '' then
    Result := APackageField.GetDisplayName
  else
    Result := APackageField.GetExchangeFieldName;
end;

procedure TEvExchangeEngine.PostUpdates(AUpdateArray: TUpdateRecordArray; const ARowMappedValues: IislistOfValues);
begin
  FImpEngine.PostUpdates(AUpdateArray, ARowMappedValues);
end;

procedure TEvExchangeEngine.BeforeImport;
begin

end;

function TEvExchangeEngine.PreFormatOfInputData(const AFunctionName: String; const AValue: Variant): Variant;
var
  S: String;
begin
  Result := AValue;
  if AValue = null then
    exit;

  if AnsiContainsText(AFunctionName, 'PadStringLeft20')  then
  begin
    S := AValue;
    S := PadStringLeft(Trim(S), ' ', 20);
    Result := S;
    exit;
  end;
  if AnsiContainsText(AFunctionName, 'ConvertToCustomEENumber') then
  begin
    if AValue <> null then
      Result := PadStringLeft(VarToStr(AValue), ' ', 9)
    else
      Result := AValue;
    exit;
  end;
end;

function TEvExchangeEngine.GenerateName(const ACl, ACnt: integer): String;
var
  sClNbr: String;
  sCnt: String;
begin
  Result := '';
  sClNbr := intToStr(ACl);
  sClNbr := StringOfChar('0', 10 - Length(sClNbr)) + sClNbr;

  sCnt := intToStr(ACnt);
  sCnt := StringOfChar('0', 10 - Length(sCnt)) + sCnt;

  Result := sClNbr + '#' + sCnt;
end;

procedure TEvExchangeEngine.AddWarningIfLookupFailed(const APackageField: IEvExchangePackageField; const AOldValue,
  ANewValue: Variant; AUsedMapRecord: IEvImportMapRecord);
var
  S: String;
begin
  // adding error if lookup failed
  if VarIsNull(ANewValue) and not VarIsNull(AOldValue) and ((AUsedMapRecord.MapType = mtpLookup) or (AUsedMapRecord.MapType = mtpMapLookup)) then
  begin
    S := 'Package field: "' + GetPackageFieldName(APackageField) + '".';
    S := S + ' Value "' + VarToStr(AOldValue) + '" was not found "';

    //Tracy's note:I found an issue where the error and warning message is using the internal table and field path instead of a GUI or Exchange Field Name
    //There is a ticket where we are going to update error messages to no longer use internal table and field paths through out evolution classic
    //reason for comment out
   (*
     S:= S+ 'in ';
    if AUsedMapRecord.MapType = mtpLookup then
    begin
      S := S + AUsedMapRecord.LookupTable + '"';
      if AUsedMapRecord.LookupTable1 <> '' then
        S := S + ', "' + AUsedMapRecord.LookupTable1 + '" tables.'
      else
        S := S + ' table.';
    end
    else
      S := S + AUsedMapRecord.MLookupTable + '" table.';
   *)
    S := S + EvoXLookupFailedMessage;
    FErrorsList.AddEvent(etWarning, FormatErrorMessageForRow(S));
  end;
end;

procedure TEvExchangeEngine.CheckValuesBeforeChange(const ATableName: String; const AOperation: char;
  const AOldValues: TevClientDataSet; const ANewValues: IisListOfValues);
begin
  // don't change any value and don't change dataset's status
end;

{ TevExchangeChangeRecord }

constructor TevExchangeChangeRecord.Create(const AOperation: Char;
  const AClientNbr: integer; const ATableName: String; const ARecordNbr: integer);
begin
  inherited Create;

  CheckCondition((AOperation = 'I') or (AOperation = 'U'), 'Value of operation parameter is ' + AOperation + '. It is out of range (I, U)');
  FOperation := AOperation;
  FTableName := UpperCase(ATableName);
  FClientNbr := AClientNbr;
  FRecordNbr := ARecordNbr;
end;

destructor TevExchangeChangeRecord.Destroy;
begin

  inherited;
end;

procedure TevExchangeChangeRecord.DoOnConstruction;
begin
  inherited;
  FAdditionalParams := TisListOfValues.Create;
end;

function TevExchangeChangeRecord.GetAdditionalParams: IisListOfValues;
begin
  Result := FAdditionalParams;
end;

function TevExchangeChangeRecord.GetClientNbr: integer;
begin
  Result := FClientNbr;
end;

function TevExchangeChangeRecord.GetOperation: Char;
begin
  Result := FOperation;
end;

function TevExchangeChangeRecord.GetRecordNbr: integer;
begin
  Result := FRecordNbr;
end;

function TevExchangeChangeRecord.GetTableName: String;
begin
  Result := FTableName;
end;

end.
