unit EvDashboardDataProviderMod;

interface

implementation

uses SysUtils, DB, sPayrollReport,
     isBaseClasses, isSettings, isBasicUtils, EvStreamUtils, isTypes, EvConsts,
     EvCommonInterfaces, EvMainboard, EvDataSet, EvContext;

type
  TevDashboardDataProvider = class(TisInterfacedObject, IevDashboardDataProvider, IevDashboardDataProviderExt)
  private
    FStorage: IisSettings;
  protected
    procedure AfterConstruction; override;
    procedure StoreReportData(const AData: String);
    function  GetCompanyReports(const ACl_Nbr, ACo_Nbr: Integer; RemoteUser: boolean): IisListOfValues;
    function  GetCompanyMessages: IisListOfValues;
  end;


function GetDashboardDataProvider: IevDashboardDataProvider;
begin
  Result := TevDashboardDataProvider.Create;
end;


{ TevDashboardDataProvider }

procedure TevDashboardDataProvider.AfterConstruction;
begin
  inherited;
  FStorage := TisSettingsSQLite.Create(AppDataFolder + 'DashboardCache.dat');
end;

function TevDashboardDataProvider.GetCompanyMessages: IisListOfValues;
const
  Stereotypes: array [Low(TevUserAccountType)..High(TevUserAccountType)] of string = ('', '', STEREOTYPE_LOCAL, STEREOTYPE_REMOTE, STEREOTYPE_WEB, '', STEREOTYPE_LOCAL);
var
  Q: IevQuery;
  i: integer;
begin
  Q:= TevQuery.Create('SELECT data FROM sb_user_messages t WHERE t.user_stereotype = :stereotype AND {AsOfNow<t>} ORDER BY order_number');
  Q.Params.AddValue('stereotype', Stereotypes[Context.UserAccount.AccountType]);
  Q.Execute;

  Result := TisListOfValues.Create;
  Q.Result.First;
  i := 0;
  while not Q.Result.Eof do
  begin
    if i < 5 then
      Result.AddValue(IntToStr(Q.Result.RecNo), Q.Result.Fields[0].AsString);
    i := i + 1;  
    Q.Result.Next;
  end;
end;

function TevDashboardDataProvider.GetCompanyReports(const ACl_Nbr, ACo_Nbr: Integer; RemoteUser: boolean): IisListOfValues;
var
  Id, s: String;
  L: IisStringListRO;
  i: Integer;
  Q: IevQuery;
  RunParams: TevPrRepPrnSettings;

  function GetCoReportsNbr(const aStr: string): string;
  begin
    Result := '-1';
    if Pos('_', aStr) > 0 then
      Result := Copy(aStr, Pos('_', aStr) + 1, Length(aStr));
  end;

  function GetCoReportsNbrInt(const aStr: string): integer;
  var
    val: integer;
  begin
    if TryStrToInt(GetCoReportsNbr( aStr ), val) then
      Result := val
    else
      Result := -1;
  end;

begin
  Result := TisListOfValues.Create;
  ctx_DataAccess.OpenClient(ACl_Nbr);

  Id := IntToStr(ACl_Nbr) + '\' + IntToStr(ACo_Nbr);
  FStorage.Lock;
  try
    L := FStorage.GetChildNodes(Id);

    s := '';
    for i := 0 to L.Count - 1 do
    if i = 0 then
      s := GetCoReportsNbr(L[i])
    else
      s := s + ',' + GetCoReportsNbr(L[i]);

    if s <> '' then
    begin
      Q := TevQuery.Create('select CO_REPORTS_NBR, RUN_PARAMS' +
        ' from CO_REPORTS a where {AsOfNow<a>} and CO_REPORTS_NBR in (' + s + ') ' +
        ' and CO_NBR=:P_CO_NBR');
      Q.Params.AddValue('P_CO_NBR', ACo_Nbr);
      Q.Execute;

      RunParams := TevPrRepPrnSettings.Create(nil);
      try
        for i := 0 to L.Count - 1 do
        if Q.Result.Locate('CO_REPORTS_NBR', GetCoReportsNbrInt(L[i]), []) then
        begin
          if not Q.Result.FieldByName('RUN_PARAMS').IsNull then
            try
              RunParams.ReadFromBlobField(TBlobField(Q.Result.FieldByName('RUN_PARAMS')))
            except
              RunParams.Clear;
            end
          else
            RunParams.Clear;

          if RunParams.DisplayOnDashboard and not (RunParams.HideForRemotes and RemoteUser) then
          begin
            s := FStorage.AsString[Id + '\' + L[i] + '\Data'];
            Result.AddValue(L[i], s);
          end;
        end;
      finally
        FreeAndNil(RunParams);
      end;
    end;  
  finally
    FStorage.Unlock;
  end;
end;

procedure TevDashboardDataProvider.StoreReportData(const AData: String);
var
  s, sLine, Id: String;
  LineValues: TisDynStringArray;
  CacheDate, ReportDate: TDateTime;
begin
// Header format:  CL NBR,CO NBR,REPORT NBR,CO REPORTS NBR,DATE

  s := AData;
  sLine := GetNextStrValue(s, #13);
  GetNextStrValue(s, #10);
  LineValues := ReadDelimitedText(sLine, ',');
  Assert(Length(LineValues) >= 5);
  Id := LineValues[0] + '\' + LineValues[1] + '\' + LineValues[2] + '_' + LineValues[3];
  ReportDate := TextToDate(LineValues[4]);

  FStorage.Lock;
  try
    CacheDate := FStorage.AsDateTime[Id + '\Date'];

    if CacheDate <= ReportDate then
    begin
      FStorage.AsDateTime[Id + '\Date'] := ReportDate;
      FStorage.AsString[Id + '\Data'] := s;
    end;
  finally
    FStorage.Unlock;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetDashboardDataProvider, IevDashboardDataProvider, 'Dashboard Data Provider');
end.
