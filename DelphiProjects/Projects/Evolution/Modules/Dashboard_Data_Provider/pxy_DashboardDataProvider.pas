// Proxy Module "Dashboard Data Provider"
unit pxy_DashboardDataProvider;

interface

implementation

uses EvCommonInterfaces, isBaseClasses, EvMainboard, evRemoteMethods, EvTransportDatagrams,
     evProxy, EvTransportInterfaces;

type
  TevDashboardDataProviderProxy = class(TevProxyModule, IevDashboardDataProvider)
  protected
    function GetCompanyReports(const ACl_Nbr, ACo_Nbr: Integer; RemoteUser: boolean): IisListOfValues;
    function GetCompanyMessages: IisListOfValues;
  end;


function GetDashboardDataProviderProxy: IevDashboardDataProvider;
begin
  Result := TevDashboardDataProviderProxy.Create;
end;

{ TevDashboardDataProviderProxy }

function TevDashboardDataProviderProxy.GetCompanyMessages: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DASHBOARD_DATAPROVIDER_GETCOMPANYMESSAGES);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevDashboardDataProviderProxy.GetCompanyReports(const ACl_Nbr, ACo_Nbr: Integer; RemoteUser: boolean): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_DASHBOARD_DATAPROVIDER_GETCOMPANYREPORTS);
  D.Params.AddValue('ACl_Nbr', ACl_Nbr);
  D.Params.AddValue('ACo_Nbr', ACo_Nbr);
  D.Params.AddValue('RemoteUser', RemoteUser);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetDashboardDataProviderProxy, IevDashboardDataProvider, 'Dashboard Data Provider');

end.


