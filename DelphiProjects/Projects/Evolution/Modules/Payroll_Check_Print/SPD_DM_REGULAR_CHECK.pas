// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_DM_REGULAR_CHECK;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, EvTypes, Variants, EvBasicUtils, StrUtils, EvCommonInterfaces, EvClasses,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents, EvDataset,
  ISDataAccessComponents, ISZippingRoutines, EvContext, EvExceptions, EvUIComponents, EvClientDataSet;

const
  sPayStub = 'PayStub';
  sTitle = 'title';
  sEmployeeName = 'employeeName';
  sCompanyName = 'companyName';
  sCompanyName1 = 'CompanyName1';
  sAddress1 = 'Address1';
  sAddress2 = 'Address2';
  sCity = 'City';
  sState = 'State';
  sZipCode = 'Zip_Code';
  sPhone1 = 'Phone1';
  sDetails = 'details';
  sCompanyNumber = 'companyNumber';
  sEmployeeNumber = 'employeeNumber';
  sHireDate = 'hireDate';
  sPeriodBegin = 'periodBegin';
  sPeriodEnd = 'periodEnd';
  sCheckDate = 'checkDate';
  sCheckNumber = 'checkNumber';
  sDivision = 'division';
  sBranch = 'branch';
  sDepartment = 'department';
  sTeam = 'team';
  sEmployeeSSN = 'EmployeeSSN';
  sLeftNote = 'leftNote';
  sRightNote = 'rightNote';
  sTotalEarnHours = 'totalEarnHours';
  sTotalEarnCurrent = 'totalEarnCurrent';
  sTotalEarnYTD = 'totalEarnYTD';
  sTotalDedCurrent = 'totalDedCurrent';
  sTotalDedYTD = 'totalDedYTD';
  sTotalNetPay = 'netPay';
  sTotalDirDep = 'totalDirDep';
  sTotalCheckAmtCurrent = 'checkAmountCurrent';
  sTotalCheckAmtYTD = 'checkAmountYTD';
  sLines = 'lines';
  sCheckLine = 'checkLine';
  sEarnDescription = 'earnDescription';
  sEarnLocation = 'earnLocation';
  sEarnRate = 'earnRate';
  sEarnHours = 'earnHours';
  sEarnCurrent = 'earnCurrent';
  sEarnPunchIn = 'earnPunchIn';
  sEarnPunchOut = 'earnPunchOut';
  sEarnYTD = 'earnYTD';
  sDedDescription = 'dedDescription';
  sDedCurrent = 'dedCurrent';
  sDedYTD = 'dedYTD';

type
  TCheckStubRecord = record
    EarnCode: string;
    EarnDescr: string;
    Location: string;
    Rate: string;
    Hours: string;
    EarnCurrent: string;
    EarnYTD: string;
    DedDescr: string;
    DedCurrent: string;
    DedYTD: string;
    PunchIn: String;
    PunchOut: String;
  end;

  PCheckStubRecord = ^TCheckStubRecord;

  TCheckStub = class
  private
    MyRecords: TList;
  public
    NextLineToPrint: Integer;
    TotalHours: Double;
    TotalEarnings: Double;
    YTDEarnings: Double;
    TotalDeductions: Double;
    YTDDeductions: Double;
    TotalDirDep: Double;
    constructor Create;
    destructor Destroy; override;
    function Count: Integer;
    procedure Clear;
    procedure AddEarning(Code, Description, Location, Rate: string;
      Hours, Current, YTD: Variant; PunchIn, PunchOut: string; SumYTD: boolean = True);
    procedure AddDeduction(Description: String; Current, YTD: Variant);
    procedure ExtractRecord(Position: Integer; var aRecord: TCheckStubRecord);
  end;

  TDM_REGULAR_CHECK = class(TDataModule)
    CHECK_DATA: TevClientDataSet;
    CHECK_STUB_DATA: TevClientDataSet;
    CHECK_DATACheckNbr: TIntegerField;
    CHECK_DATABankName: TStringField;
    CHECK_DATABankAddress: TStringField;
    CHECK_DATABankTopABANumber: TStringField;
    CHECK_DATABankBottomABANumber: TStringField;
    CHECK_DATAMICRLine: TStringField;
    CHECK_DATACheckAmount: TStringField;
    CHECK_DATACheckAmountSpelled: TStringField;
    CHECK_DATAEntityName: TStringField;
    CHECK_DATAEntityAddress1: TStringField;
    CHECK_DATAEntityAddress2: TStringField;
    CHECK_DATAEntityAddress3: TStringField;
    CHECK_DATAPaymentSerialNumber: TStringField;
    CHECK_DATACheckAmountSecurity: TStringField;
    CHECK_DATADivisionNumber: TStringField;
    CHECK_DATABranchNumber: TStringField;
    CHECK_DATADepartmentNumber: TStringField;
    CHECK_DATATeamNumber: TStringField;
    CHECK_DATACheckMessage: TStringField;
    CHECK_DATAEmployeeName: TStringField;
    CHECK_DATAEmployeeAddress1: TStringField;
    CHECK_DATAEmployeeAddress2: TStringField;
    CHECK_DATAEmployeeAddress3: TStringField;
    CHECK_DATAEmployeeSSN: TStringField;
    CHECK_DATATotalHours: TStringField;
    CHECK_DATATotalEarnings: TStringField;
    CHECK_DATATotalYTDEarnings: TStringField;
    CHECK_DATATotalDeductions: TStringField;
    CHECK_DATATotalYTDDeductions: TStringField;
    CHECK_DATANet: TStringField;
    CHECK_DATAYTDNet: TStringField;
    CHECK_DATATotalDirDep: TStringField;
    CHECK_DATANetAndDirDep: TStringField;
    CHECK_DATAEmployeeHireDate: TStringField;
    CHECK_DATAPeriodBeginDate: TStringField;
    CHECK_DATAPeriodEndDate: TStringField;
    CHECK_DATACheckDate: TStringField;
    CHECK_DATACompanyName: TStringField;
    CHECK_DATACompanyNumber: TStringField;
    CHECK_DATAEmployeeNumber: TStringField;
    CHECK_DATACheckNumberOrVoucher: TStringField;
    CHECK_STUB_DATACheckNbr: TIntegerField;
    CHECK_STUB_DATAStubLineNbr: TIntegerField;
    CHECK_STUB_DATAEarningDesc: TStringField;
    CHECK_STUB_DATALocation: TStringField;
    CHECK_STUB_DATARate: TStringField;
    CHECK_STUB_DATAHours: TStringField;
    CHECK_STUB_DATAEarningCurrent: TStringField;
    CHECK_STUB_DATAEarningYTD: TStringField;
    CHECK_DATACheckComments: TMemoField;
    CHECK_DATAMICRHorizontalAdjustment: TIntegerField;
    wwcsChecks: TevClientDataSet;
    FEDERAL_YTD: TevClientDataSet;
    STATE_YTD: TevClientDataSet;
    SUI_YTD: TevClientDataSet;
    LOCAL_YTD: TevClientDataSet;
    CL_E_DS_YTD: TevClientDataSet;
    CHECK_STUB_DATADeductionDesc: TStringField;
    CHECK_STUB_DATADeductionCurrent: TStringField;
    CHECK_STUB_DATADeductionYTD: TStringField;
    TIME_OFF_DATA: TevClientDataSet;
    TIME_OFF_DATACheckNbr: TIntegerField;
    TIME_OFF_DATATimeOffNbr: TIntegerField;
    TIME_OFF_DATATimeOffLine: TStringField;
    CL_E_DS_YTDEE_NBR: TIntegerField;
    CL_E_DS_YTDCL_E_DS_NBR: TIntegerField;
    CL_E_DS_YTDYTD: TCurrencyField;
    STATE_YTDEE_STATES_NBR: TIntegerField;
    STATE_YTDSTATE_TAX: TCurrencyField;
    STATE_YTDEE_SDI_TAXABLE_WAGES: TCurrencyField;
    STATE_YTDSTATE_TAXABLE_WAGES: TCurrencyField;
    STATE_YTDEE_SDI_TAX: TCurrencyField;
    SUI_YTDEE_NBR: TIntegerField;
    SUI_YTDCO_SUI_NBR: TIntegerField;
    SUI_YTDSUI_TAX: TCurrencyField;
    SUI_YTDSUI_TAXABLE_WAGES: TCurrencyField;
    LOCAL_YTDEE_LOCALS_NBR: TIntegerField;
    LOCAL_YTDLOCAL_TAX: TCurrencyField;
    LOCAL_YTDLOCAL_TAXABLE_WAGE: TCurrencyField;
    CHECK_STUB_DATAEarningCode: TStringField;
    CL_E_DS_YTDCUSTOM_E_D_CODE_NUMBER: TStringField;
    CHECK_DATALogo_Nbr: TIntegerField;
    CHECK_DATASignature_Nbr: TIntegerField;
    CHECK_DATASignature_Db: TStringField;
    CHECK_DATAOBCCheckMessage: TStringField;
    CHECK_DATAEE_NBR: TIntegerField;
    cdTimeOff: TevClientDataSet;
    SUIConsolidation: TevClientDataSet;
    SUIConsolidationSUI_TAX: TCurrencyField;
    SUIConsolidationSUI_TAXABLE_WAGES: TCurrencyField;
    SUIConsolidationYTD_SUI_TAX: TCurrencyField;
    SUIConsolidationSY_STATES_NBR: TIntegerField;
    SUIConsolidationSTATE: TStringField;
    CHECK_STUB_DATAPunchIn: TStringField;
    CHECK_STUB_DATAPunchOut: TStringField;
    cdsCheckDataLite: TevClientDataSet;
    IntegerField1: TIntegerField;
    cdsCheckDataLiteYTDNet: TFloatField;
    cdsCheckDataLiteBankLevel: TStringField;
    cdsCheckDataLiteAccountNbr: TIntegerField;
    cdsCheckDataLiteDirDep: TFloatField;
    cdsCheckDataLiteEE_NBR: TIntegerField;
    cdsCheckDataLiteVoucher: TIntegerField;
    cdsTOALite: TevClientDataSet;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    cdsDeductionsLite: TevClientDataSet;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    cdsDeductionsLiteAmount: TFloatField;
    cdsDeductionsLiteYTD: TFloatField;
    cdsEarningsLite: TevClientDataSet;
    IntegerField4: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    cdsEarningsLiteHours: TFloatField;
    cdsEarningsLiteRate: TStringField;
    cdsEarningsLiteClEDsNbr: TIntegerField;
    cdsEarningsLiteaType: TIntegerField;
    cdsEarningsLiteLocation: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FPayrollNbr: Integer;
    FCheckForm: string;
    FCheckStubLinesNbr: Integer;
    FCheckNbr: Integer; // check order number in CHECK_DATA dataset: 1, 2, 3 etc
    FCheckStubNbr: Integer;
    FTimeOffNbr: Integer;
    FCollateChecks: Boolean;
    FOverflowTimeOff: Boolean;
    FShowLaborDistribution: string;
    FShowYTD: boolean;
    FShowEdYtd: Boolean;
    FShowYTDTaxWages: boolean;
    FShowRates: boolean;
    FShowDirDep: boolean;
    FReversePrinting: boolean;
    FShowShortfalls: boolean;
    FShowPunchDetails: boolean;
    FCalculateRates: Boolean;
    FPrCheckNbr: integer;
    FxmlData: string;
    FLiteDatasets: boolean;
    CheckStub: TCheckStub;

    procedure AddCheckStubRecordToXmlData(aCheckStubRecord: TCheckStubRecord);
    procedure PrintTaxesDeductions(Description: String; Current, YTD: Double);
    procedure PrintDeduction(DeductionNbr: Integer; Current, YTD: Double);

    function ShowWage(const AYTDWages: double; ACurrentWages: double): double;
    function GetStateMaritalStatus: string;
    function GetEeTaxOverrides: string;

    procedure AddDeduction(aDescription: string; Amount, YTD: Variant);
    procedure AddEarning(Code, Description, Location, Rate: string;
      Hours, Current, YTD: Variant; PunchIn, PunchOut: string; CLEDsNbr, aType: integer; SumYTD: boolean = True);

    procedure AddLocalTaxes;
    procedure AddSuiTaxes;
    procedure AddStateTaxes;
    procedure AddFederalTaxes;
    procedure AddDeductions(aPrCheckLines: TevClientDataSet);
    procedure AddEarnings(aPrCheckLines: TevClientDataSet);
    procedure AddTOA(CurrentTOA: boolean);

    procedure AddXMLHeader(EEName, CoName, aCompanyName, aAddress1, aAddress2, aCity, aState, aZip_Code, aPhone1: string);
    procedure AddXMLDetails(aCoNumber, aEENumber, aEEHireDate, aPeriodBeginDate,
      aPeriodEndDate, aCheckDate, aPaymentSerialNumber, aDivNumber, aBrNumber, aDepNumber, aTmNumber,
      aCheckComment, aEmployeeSSN: string);
    procedure AddXMLTotals(tHours, tEarnings, tYTDEarnings, tDeductions, tYTDDeductions,
      tNetPay, tDirDep, tCheckAmount, tCheckYTD: string);
    procedure WriteXMLStreamToCheck;
    function ProcessStringForXML(aStr: string): string;

    procedure PrepareReportData(PR_CHECK_LINES: TevClientDataSet; CurrentTOA: boolean; ForcePrintVoucher: boolean; DontPrintBankInfo: Boolean; CheckDate: TDateTime = 0);
    procedure PrepareReportDataLite(PR_CHECK_LINES: TevClientDataSet; CurrentTOA: boolean; ForcePrintVoucher: boolean);
  public
    { Public declarations }
    property CheckStubLinesNbr: Integer read FCheckStubLinesNbr write FCheckStubLinesNbr;
    procedure PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckForm: String; PR_CHECK_LINES: TevClientDataSet;
      CurrentTOA: boolean = false; CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
      ForcePrintVoucher: Boolean = False; LiteDatasets: boolean = False);
  end;

implementation

uses
  EvUtils, SDataStructure, EvConsts, SPrintingRoutines, SDataDictclient;

const
  sAssertString = 'Can not find the value: Source field - %s, Lookup Field - %s, Value - %s, Custom EE Number - %s';

{$R *.DFM}

function GetBankAccountNumberAsOfDate(PR_CHECK: TevClientDataSet; var AccountNbr: Integer): Boolean; // True-SB Bank Account; False-CL Bank Account
var
  BankAcctLevel, sTableName: String;
  V: Variant;
  StatusDate: TDateTime;
  CO: TevClientDataSet;
begin
  CO := TevClientDataSet.Create(nil);
  try
    Result := True;
    if not DM_EMPLOYEE.EE.Active then
      DM_EMPLOYEE.EE.DataRequired('ALL');

    if DM_PAYROLL.PR_CHECK.IndexFieldNames <> 'PR_CHECK_NBR' then
      DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
    Assert(DM_PAYROLL.PR_CHECK.FindKey([PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger]), Format(sAssertString, ['wwcsChecks.PR_CHECK_NBR', 'PR_CHECK.PR_CHECK_NBR', PR_CHECK.FieldByName('PR_CHECK_NBR').AsString, PR_CHECK.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

    if DM_PAYROLL.PR.PROCESS_DATE.IsNull then
      StatusDate := Now
    else
      StatusDate := DM_PAYROLL.PR.PROCESS_DATE.AsDateTime;

    CO.ProviderName := 'CL_CUSTOM_PROV';
    with TExecDSWrapper.Create('SelectHistNBR') do
    begin
      SetMacro('TableName', 'CO');
      SetMacro('Columns', '*');
      SetMacro('NbrField', 'CO');
      SetMacro('StatusDate', ':StatusDate');
      SetParam('StatusDate', StatusDate);
      SetParam('RecordNbr', DM_COMPANY.CO.CO_NBR.AsInteger);
      CO.DataRequest(AsVariant);
    end;
    CO.Open;

    if (PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) and (not CO.FieldByName('MANUAL_CL_BANK_ACCOUNT_NBR').IsNull) then
    begin
      Result := False;
      AccountNbr := CO.FieldByName('MANUAL_CL_BANK_ACCOUNT_NBR').AsInteger;
    end
    else
    if (CO.FieldByName('OBC').Value = 'Y') and (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_VOID]) then
    begin
      if CO.FieldByName('SB_OBC_ACCOUNT_NBR').IsNull then
        raise EInconsistentData.CreateHelp('The company uses OBC, but OBC service bureau bank account is not setup.', IDH_InconsistentData);
      AccountNbr := CO.FieldByName('SB_OBC_ACCOUNT_NBR').AsInteger;
    end
    else
    if (CO.FieldByName('TRUST_SERVICE').Value = TRUST_SERVICE_ALL) and (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_VOID]) then
    begin
      if CO.FieldByName('TRUST_SB_ACCOUNT_NBR').IsNull then
        raise EInconsistentData.CreateHelp('The company is trust, but trust service bureau bank account is not setup.', IDH_InconsistentData);
      AccountNbr := CO.FieldByName('TRUST_SB_ACCOUNT_NBR').AsInteger;
    end
    else
    begin
      Result := False;
      BankAcctLevel := CO.FieldByName('BANK_ACCOUNT_LEVEL').Value;
      if BankAcctLevel = CLIENT_LEVEL_COMPANY then
      begin
        AccountNbr := CO.FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').AsInteger;
      end
      else
      begin
        V := ctx_PayrollCalculation.GetHomeDBDTFieldValue('PAYROLL_CL_BANK_ACCOUNT_NBR', BankAcctLevel, PR_CHECK.FieldByName('EE_NBR').AsInteger,
          DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM, DM_EMPLOYEE.EE, True);
        if CO.FieldByName('BREAK_CHECKS_BY_DBDT').Value <> 'N' then
        begin
          case BankAcctLevel[1] of
            CLIENT_LEVEL_DIVISION:
              sTableName := 'CO_DIVISION';
            CLIENT_LEVEL_BRANCH:
              sTableName := 'CO_BRANCH';
            CLIENT_LEVEL_DEPT:
              sTableName := 'CO_DEPARTMENT';
            CLIENT_LEVEL_TEAM:
              sTableName := 'CO_TEAM';
          end;
          DM_PAYROLL.PR_CHECK_LINES.SaveState;
          try
            if not DM_PAYROLL.PR_CHECK_LINES.Active then
              DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
            DM_PAYROLL.PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
            DM_PAYROLL.PR_CHECK_LINES.Filtered := True;
            DM_PAYROLL.PR_CHECK_LINES.First;
            while not DM_PAYROLL.PR_CHECK_LINES.EOF do
            begin
              if not DM_PAYROLL.PR_CHECK_LINES.FieldByName(sTableName + '_NBR').IsNull then
              begin
                ctx_DataAccess.GetDataSet(GetDDTableClassByName(sTableName)).DataRequired('ALL');
                V := ctx_DataAccess.GetDataSet(GetDDTableClassByName(sTableName)).Lookup(sTableName + '_NBR',
                  DM_PAYROLL.PR_CHECK_LINES.FieldByName(sTableName + '_NBR').Value, 'PAYROLL_CL_BANK_ACCOUNT_NBR');
                Break;
              end;
              DM_PAYROLL.PR_CHECK_LINES.Next;
            end;
          finally
            DM_PAYROLL.PR_CHECK_LINES.LoadState;
          end;
        end;
        if VarIsNull(V) then
          raise EInconsistentData.CreateHelp('You have chosen a bank account level other than Company and you do not have a payroll bank account setup on that level!', IDH_InconsistentData);
        AccountNbr := V;
      end;
    end;
  finally
    CO.Free;
  end;
end;

{ TCheckStub }

constructor TCheckStub.Create;
begin
  MyRecords := TList.Create;
end;

destructor TCheckStub.Destroy;
begin
  Clear;
  MyRecords.Free;
  inherited;
end;

procedure TCheckStub.AddEarning(Code, Description, Location, Rate: string;
  Hours, Current, YTD: Variant; PunchIn, PunchOut: string; SumYTD: boolean = True);
var
  NewCheckStubRecord: PCheckStubRecord;
  Position: Integer;
begin
  if MyRecords.Count = 0 then
  begin
    New(NewCheckStubRecord);
    MyRecords.Add(NewCheckStubRecord);
    Position := MyRecords.Count - 1;
  end
  else
  begin
    if PCheckStubRecord(MyRecords[MyRecords.Count - 1]).EarnDescr = '' then
    begin
      Position := MyRecords.Count - 1;
      while Position > 0 do
      begin
        if PCheckStubRecord(MyRecords[Position - 1])^.DedDescr <> '' then
          Break;
        Dec(Position);
      end;
    end
    else
    begin
      New(NewCheckStubRecord);
      MyRecords.Add(NewCheckStubRecord);
      Position := MyRecords.Count - 1;
    end;
  end;

  if VarIsNull(Current) then
    PCheckStubRecord(MyRecords[Position]).EarnCurrent := ''
  else begin
    TotalEarnings := TotalEarnings + VarToFloat( Current );
    PCheckStubRecord(MyRecords[Position]).EarnCurrent := FloatToStrF( VarToFloat( Current ), ffFixed, 15, 2);
  end;

  if VarIsNull(YTD) then
    PCheckStubRecord(MyRecords[Position]).EarnYTD := ''
  else begin
    if SumYTD then
      YTDEarnings := YTDEarnings + VarToFloat( YTD );
    PCheckStubRecord(MyRecords[Position]).EarnYTD := FloatToStrF( VarToFloat( YTD ), ffFixed, 15, 2);
  end;

  if VarIsNull(Hours) then
    PCheckStubRecord(MyRecords[Position]).Hours := ''
  else begin
    TotalHours := TotalHours + VarToFloat( Hours );
    PCheckStubRecord(MyRecords[Position]).Hours := FloatToStrF( VarToFloat( Hours ), ffFixed, 15, 2);
  end;

  PCheckStubRecord(MyRecords[Position]).EarnCode := Code;
  PCheckStubRecord(MyRecords[Position]).EarnDescr := Description;
  PCheckStubRecord(MyRecords[Position]).Location := Location;
  PCheckStubRecord(MyRecords[Position]).Rate := Rate;
  PCheckStubRecord(MyRecords[Position]).PunchIn := PunchIn;
  PCheckStubRecord(MyRecords[Position]).PunchOut := PunchOut;
end;

procedure TCheckStub.AddDeduction(Description: String; Current, YTD: Variant);
var
  NewCheckStubRecord: PCheckStubRecord;
  Position: Integer;
begin
  if MyRecords.Count = 0 then
  begin
    New(NewCheckStubRecord);
    MyRecords.Add(NewCheckStubRecord);
    Position := MyRecords.Count - 1;
  end
  else
  begin
    if PCheckStubRecord(MyRecords[MyRecords.Count - 1]).DedDescr = '' then
    begin
      Position := MyRecords.Count - 1;
      while Position > 0 do
      begin
        if PCheckStubRecord(MyRecords[Position - 1]).DedDescr <> '' then
          Break;
        Dec(Position);
      end;
    end
    else
    begin
      New(NewCheckStubRecord);
      MyRecords.Add(NewCheckStubRecord);
      Position := MyRecords.Count - 1;
    end;
  end;

  PCheckStubRecord(MyRecords[Position]).DedDescr := Description;

  if VarIsNull(Current) then
    PCheckStubRecord(MyRecords[Position]).DedCurrent := ''
  else begin
    TotalDeductions := TotalDeductions + VarToFloat( Current );
    PCheckStubRecord(MyRecords[Position]).DedCurrent := FloatToStrF( VarToFloat( Current ), ffFixed, 15, 2);
  end;

  if VarIsNull(YTD) then
    PCheckStubRecord(MyRecords[Position]).DedYTD := ''
  else begin
    YTDDeductions := YTDDeductions + VarToFloat( YTD );
    PCheckStubRecord(MyRecords[Position]).DedYTD := FloatToStrF( VarToFloat( YTD ), ffFixed, 15, 2);
  end;
end;

procedure TCheckStub.Clear;
var
  I: Integer;
begin
  for I := MyRecords.Count - 1 downto 0 do
    Dispose(PCheckStubRecord(MyRecords[I]));
  MyRecords.Clear;

  NextLineToPrint := 0;
  TotalHours := 0;
  TotalEarnings := 0;
  YTDEarnings := 0;
  TotalDeductions := 0;
  YTDDeductions := 0;
  TotalDirDep := 0;
end;

function TCheckStub.Count: Integer;
begin
  Result := MyRecords.Count;
end;

procedure TCheckStub.ExtractRecord(Position: Integer;
  var aRecord: TCheckStubRecord);
begin
  aRecord := PCheckStubRecord(MyRecords[Position])^;
end;

{ TDM_REGULAR_CHECK }

procedure TDM_REGULAR_CHECK.DataModuleCreate(Sender: TObject);
begin
  FCheckStubLinesNbr := 11;
  FCheckNbr := 0;
  FCheckStubNbr := 0;
  FTimeOffNbr := 0;
  FCollateChecks := False;
  FShowLaborDistribution := AUTO_LABOR_DIST_DO_NOT_SUPPRESS;
  CheckStub := TCheckStub.Create;
  CHECK_DATA.CreateDataSet;
  CHECK_STUB_DATA.CreateDataSet;
  TIME_OFF_DATA.CreateDataSet;

  cdsCheckDataLite.CreateDataSet;
  cdsTOALite.CreateDataSet;
  cdsDeductionsLite.CreateDataSet;
  cdsEarningsLite.CreateDataSet;

  DM_SERVICE_BUREAU.SB.Activate;
end;

procedure TDM_REGULAR_CHECK.DataModuleDestroy(Sender: TObject);
begin
  CheckStub.Free;
end;

procedure TDM_REGULAR_CHECK.PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckForm: String; PR_CHECK_LINES: TevClientDataSet;
  CurrentTOA: boolean = false; CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
  ForcePrintVoucher: Boolean = False; LiteDatasets: boolean = False);
var
  InStatement: String;
  I, it: Integer;
  PrCheckNbrI, ClEDSI, AmountI, AmountI1, AmountI2, AmountI3: Integer;
  ds: TevClientDataSet;
  a: array of array of array of Currency;
begin
  FCheckForm := CheckForm;
  FCollateChecks := (DM_COMPANY.CO.FieldByName('COLLATE_CHECKS').Value = 'Y');
  FOverflowTimeOff := (CheckForm[1] = CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW) or (CheckForm[1] = CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW);
  FShowLaborDistribution := DM_COMPANY.CO.FieldByName('AUTO_LABOR_DIST_LEVEL').AsString;
  FShowYTD := (DM_COMPANY.CO.FieldByName('SHOW_YTD_ON_CHECK').Value = 'Y');
  FShowYTDTaxWages := (FloatToStr(DM_COMPANY.CO.FieldByName('PAYRATE_PRECISION').AsFloat) = FloatToStr(9.87));
  FShowRates := (DM_COMPANY.CO.FieldByName('SHOW_RATES_ON_CHECKS').Value = 'Y');
  FShowDirDep := (DM_COMPANY.CO.FieldByName('SHOW_DIR_DEP_NBR_ON_CHECKS').Value = 'Y');
  FReversePrinting := (DM_COMPANY.CO.FieldByName('REVERSE_CHECK_PRINTING').Value = 'Y');
  FShowShortfalls := DM_COMPANY.CO.SHOW_SHORTFALL_CHECK.Value = GROUP_BOX_YES;
  FShowPunchDetails := DM_COMPANY.CO.SHOW_TIME_CLOCK_PUNCH.Value = GROUP_BOX_YES;
  FCalculateRates := (DM_COMPANY.CO.SHOW_RATE_BY_HOURS.Value = GROUP_BOX_YES)
    and (Length(FCheckForm) > 0) and (FCheckForm[1] in ['9','E','F','I','8','J','K','Q','W','X','Z','6','Y','S','1','2','3','U','5']);

  if (PayrollNbr = 0) and (CheckNumbers = nil) then
    Exit;

  FPayrollNbr := PayrollNbr;

  begin
    if ctx_DataAccess.CUSTOM_VIEW.Active then ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('Checks') do
    begin
      if (PayrollNbr <> 0) and ((CheckNumbers = Nil) or (CheckNumbers <> Nil) and (CheckNumbers.Count > 1)) then
        SetMacro('FilterCondition', 'R.PR_NBR='+IntToStr(PayrollNbr)+' and')
      else
      begin
        InStatement := '';
        for I := 0 to CheckNumbers.Count - 2 do
          InStatement := InStatement + CheckNumbers.Strings[I] + ', ';
        InStatement := InStatement + CheckNumbers.Strings[CheckNumbers.Count - 1];
        if InStatement = '' then
          Exit;
        SetMacro('FilterCondition', 'C.PR_CHECK_NBR in ('+ InStatement +') and');
      end;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    wwcsChecks.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
    ctx_DataAccess.CUSTOM_VIEW.Close;

    wwcsChecks.First;
    while not wwcsChecks.EOF do
    begin
      if (CheckNumbers <> Nil) and (CheckNumbers.IndexOf(wwcsChecks.FieldByName('PR_CHECK_NBR').AsString) = -1) then
        wwcsChecks.Delete
      else
      begin
        if (DM_COMPANY.CO.FieldByName('PRINT_MANUAL_CHECK_STUBS').Value = 'N') and
          ((not SkipCheckStubCheck) and (wwcsChecks.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER, CHECK_TYPE2_3RD_PARTY])) then
            wwcsChecks.Delete
        else
          wwcsChecks.Next;
      end;
    end;

    if wwcsChecks.RecordCount = 0 then
      Exit;

    wwcsChecks.First;
  end;

  wwcsChecks.IndexFieldNames := ctx_PayrollCalculation.ConstructCheckSortIndex;

  if CheckForm[1] in [CHECK_FORM_PRESSURE_SEAL, CHECK_FORM_PRESSURE_SEAL_LEGAL,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT,
                      CHECK_FORM_LETTER_BTM, CHECK_FORM_LETTER_BTM_RATE, CHECK_FORM_LETTER_BTM_FF,
                      CHECK_FORM_LETTER_BTM_STUB_ONLY,
                      CHECK_FORM_LETTER_BTM_WITH_ADDR,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE,
                      CHECK_FORM_PRESSURE_SEAL_GREATLAND,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_FF,
                      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD] then
    // do not collate
  else if FCollateChecks then
    SplitDataSetForCollation(wwcsChecks);

// Get YTD data
  if wwcsChecks.RecordCount < 5 then
  begin
    InStatement := '';
    wwcsChecks.First;
    while not wwcsChecks.EOF do
    begin
      InStatement := InStatement + wwcsChecks.FieldByName('EE_NBR').AsString + ', ';
      wwcsChecks.Next;
    end;
    Delete(InStatement, Length(InStatement) - 1, 2);
  end;

  with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
  begin
    SetMacro('Table1', 'PR_CHECK');
    SetMacro('Table2', 'PR');
    SetMacro('Columns', 't1.PR_CHECK_NBR, t1.EE_NBR');
    SetMacro('JoinField', 'PR');
    SetMacro('Condition', '(t2.pr_nbr=:PrNbr or t2.CO_NBR=:CoNbr and t2.status in (''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
             '(t2.run_number>=:StartRN AND t2.check_date=:StartDate OR t2.check_date>:StartDate) AND ' +
             '(t2.run_number<=:EndRN AND t2.check_date=:EndDate OR t2.check_date<:EndDate))');
    SetParam('StartDate', GetBeginYear(wwcsChecks.FieldByName('CHECK_DATE').AsDateTime));
    SetParam('EndDate', wwcsChecks.FieldByName('CHECK_DATE').AsDateTime);
    SetParam('StartRN', 1);
    SetParam('EndRN', wwcsChecks.FieldByName('RUN_NUMBER').AsInteger);
    SetParam('CoNbr', wwcsChecks.FieldByName('CO_NBR').AsInteger);
    SetParam('PrNbr', PayrollNbr);
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;

  ctx_DataAccess.CUSTOM_VIEW.IndexFieldNames := 'PR_CHECK_NBR';
  try
    ds := PR_CHECK_LINES;
    ctx_DataAccess.CUSTOM_VIEW.First;
    PR_CHECK_LINES.IndexFieldNames := 'PR_CHECK_NBR';
    PR_CHECK_LINES.First;
    PrCheckNbrI := PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Index;
    ClEDSI := PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Index;
    AmountI := PR_CHECK_LINES.FieldByName('AMOUNT').Index;
    SetLength(a, 0);

    while not ds.Eof do
    begin
      while not ctx_DataAccess.CUSTOM_VIEW.Eof and
            (ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value < ds.Fields[PrCheckNbrI].Value) do
        ctx_DataAccess.CUSTOM_VIEW.Next;

      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value = ds.Fields[PrCheckNbrI].Value then
      begin
        if ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger > Length(a) then
          SetLength(a, ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger);
        if ds.Fields[ClEDSI].AsInteger > Length(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)]) then
          SetLength(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)], ds.Fields[ClEDSI].AsInteger);
        if Length(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)]) = 0 then
          SetLength(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)], 1);
        a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)][0] :=
          a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)][0] + ds.Fields[AmountI].AsCurrency
      end;
      ds.Next
    end;

    if DM_CLIENT.CL_E_DS.IndexFieldNames <> 'CL_E_DS_NBR' then
      DM_CLIENT.CL_E_DS.IndexFieldNames := 'CL_E_DS_NBR';

    CL_E_DS_YTD.CreateDataSet;
    for i := 0 to High(a) do
      for it := 0 to High(a[i]) do
        if Length(a[i][it]) > 0 then
        begin
          CL_E_DS_YTD.Append;
          CL_E_DS_YTD.Fields[0].Value := Succ(i);
          CL_E_DS_YTD.Fields[1].Value := Succ(it);
          Assert(DM_CLIENT.CL_E_DS.FindKey([Succ(it)]), Format(sAssertString, ['CL_E_DS_YTD.CL_E_DS_NBR', 'CL_E_DS.CL_E_DS_NBR', IntToStr(Succ(it))]));
          CL_E_DS_YTD.Fields[2].Value := DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER'];
          CL_E_DS_YTD.Fields[3].Value := a[i][it][0];
          CL_E_DS_YTD.Post;
        end;

    with TExecDSWrapper.Create('FederalTaxesYTDData2') do
    begin
      SetParam('StartDate', GetBeginYear(wwcsChecks.FieldByName('CHECK_DATE').AsDateTime));
      SetParam('EndDate', wwcsChecks.FieldByName('CHECK_DATE').AsDateTime);
      SetParam('StartRN', 1);
      SetParam('EndRN', wwcsChecks.FieldByName('RUN_NUMBER').AsInteger);
      SetParam('CoNbr', wwcsChecks.FieldByName('CO_NBR').AsInteger);
      SetParam('PrNbr', PayrollNbr);
      if wwcsChecks.RecordCount < 5 then
        SetMacro('Filter', 'and C.EE_NBR in (' + InStatement + ')');
      ctx_DataAccess.GetCustomData(FEDERAL_YTD, AsVariant);
    end;

    ds := DM_PAYROLL.PR_CHECK_STATES;
    ctx_DataAccess.CUSTOM_VIEW.First;
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_STATES.First;
    PrCheckNbrI := DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').Index;
    ClEDSI := DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Index;
    AmountI := DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').Index;
    AmountI1 := DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Index;
    AmountI2 := DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Index;
    AmountI3 := DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Index;
    SetLength(a, 0);

    while not ds.Eof do
    begin
      while not ctx_DataAccess.CUSTOM_VIEW.Eof and
            (ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value < ds.Fields[PrCheckNbrI].Value) do
        ctx_DataAccess.CUSTOM_VIEW.Next;

      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value = ds.Fields[PrCheckNbrI].Value then
      begin
        if ds.Fields[ClEDSI].Value > Length(a) then
          SetLength(a, ds.Fields[ClEDSI].AsInteger);
        if Length(a[Pred(ds.Fields[ClEDSI].AsInteger)]) = 0 then
          SetLength(a[Pred(ds.Fields[ClEDSI].AsInteger)], 1);
        if Length(a[Pred(ds.Fields[ClEDSI].AsInteger)][0]) = 0 then
          SetLength(a[Pred(ds.Fields[ClEDSI].AsInteger)][0], 4);
        a[Pred(ds.Fields[ClEDSI].AsInteger)][0][0] := a[Pred(ds.Fields[ClEDSI].AsInteger)][0][0] + ds.Fields[AmountI].AsCurrency;
        a[Pred(ds.Fields[ClEDSI].AsInteger)][0][1] := a[Pred(ds.Fields[ClEDSI].AsInteger)][0][1] + ds.Fields[AmountI1].AsCurrency;
        a[Pred(ds.Fields[ClEDSI].AsInteger)][0][2] := a[Pred(ds.Fields[ClEDSI].AsInteger)][0][2] + ds.Fields[AmountI2].AsCurrency;
        a[Pred(ds.Fields[ClEDSI].AsInteger)][0][3] := a[Pred(ds.Fields[ClEDSI].AsInteger)][0][3] + ds.Fields[AmountI3].AsCurrency;
      end;
      ds.Next
    end;

    STATE_YTD.CreateDataSet;
    for i := 0 to High(a) do
      if Length(a[i]) > 0 then
      begin
        STATE_YTD.Append;
        STATE_YTD.Fields[0].Value := Succ(i);
        STATE_YTD.Fields[1].Value := a[i][0][0];
        STATE_YTD.Fields[2].Value := a[i][0][1];
        STATE_YTD.Fields[3].Value := a[i][0][2];
        STATE_YTD.Fields[4].Value := a[i][0][3];
        STATE_YTD.Post;
      end;

    ds := DM_PAYROLL.PR_CHECK_SUI;
    ctx_DataAccess.CUSTOM_VIEW.First;
    DM_PAYROLL.PR_CHECK_SUI.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_SUI.First;
    PrCheckNbrI := DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').Index;
    ClEDSI := DM_PAYROLL.PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Index;
    AmountI := DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').Index;
    AmountI1 := DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Index;
    SetLength(a, 0);

    while not ds.Eof do
    begin
      while not ctx_DataAccess.CUSTOM_VIEW.Eof and
            (ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value < ds.Fields[PrCheckNbrI].Value) do
        ctx_DataAccess.CUSTOM_VIEW.Next;

      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value = ds.Fields[PrCheckNbrI].Value then
      begin
        if ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger > Length(a) then
          SetLength(a, ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger);
        if ds.Fields[ClEDSI].AsInteger > Length(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)]) then
          SetLength(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)], ds.Fields[ClEDSI].AsInteger);
        if Length(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)]) = 0 then
          SetLength(a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)], 2);
        a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)][0] :=
          a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)][0] + ds.Fields[AmountI].AsCurrency;
        a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)][1] :=
          a[Pred(ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsInteger)][Pred(ds.Fields[ClEDSI].AsInteger)][1] + ds.Fields[AmountI1].AsCurrency
      end;
      ds.Next
    end;

    SUI_YTD.CreateDataSet;
    for i := 0 to High(a) do
      for it := 0 to High(a[i]) do
        if Length(a[i][it]) > 0 then
        begin
          SUI_YTD.Append;
          SUI_YTD.Fields[0].Value := Succ(i);
          SUI_YTD.Fields[1].Value := Succ(it);
          SUI_YTD.Fields[2].Value := a[i][it][0];
          SUI_YTD.Fields[3].Value := a[i][it][1];
          SUI_YTD.Post;
        end;

    ds := DM_PAYROLL.PR_CHECK_LOCALS;
    ctx_DataAccess.CUSTOM_VIEW.First;
    DM_PAYROLL.PR_CHECK_LOCALS.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_LOCALS.First;
    PrCheckNbrI := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Index;
    ClEDSI := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Index;
    AmountI := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Index;
    AmountI1 := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Index;
    SetLength(a, 0);

    while not ds.Eof do
    begin
      while not ctx_DataAccess.CUSTOM_VIEW.Eof and
            (ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value < ds.Fields[PrCheckNbrI].Value) do
        ctx_DataAccess.CUSTOM_VIEW.Next;

      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value = ds.Fields[PrCheckNbrI].Value then
      begin
        if ds.Fields[ClEDSI].Value > Length(a) then
          SetLength(a, ds.Fields[ClEDSI].AsInteger);
        if Length(a[Pred(ds.Fields[ClEDSI].AsInteger)]) = 0 then
          SetLength(a[Pred(ds.Fields[ClEDSI].AsInteger)], 1);
        if Length(a[Pred(ds.Fields[ClEDSI].AsInteger)][0]) = 0 then
          SetLength(a[Pred(ds.Fields[ClEDSI].AsInteger)][0], 2);
        a[Pred(ds.Fields[ClEDSI].AsInteger)][0][0] := a[Pred(ds.Fields[ClEDSI].AsInteger)][0][0] + ds.Fields[AmountI].AsCurrency;
        a[Pred(ds.Fields[ClEDSI].AsInteger)][0][1] := a[Pred(ds.Fields[ClEDSI].AsInteger)][0][1] + ds.Fields[AmountI1].AsCurrency
      end;
      ds.Next
    end;

    LOCAL_YTD.CreateDataSet;
    for i := 0 to High(a) do
      if Length(a[i]) > 0 then
      begin
        LOCAL_YTD.Append;
        LOCAL_YTD.Fields[0].Value := Succ(i);
        LOCAL_YTD.Fields[1].Value := a[i][0][0];
        LOCAL_YTD.Fields[2].Value := a[i][0][1];
        LOCAL_YTD.Post;
      end;

    a := nil;
  finally
    ctx_DataAccess.CUSTOM_VIEW.IndexFieldNames := ''
  end;
  // Done getting YTD data

  FLiteDatasets := LiteDatasets;
  if LiteDatasets then
    PrepareReportDataLite(PR_CHECK_LINES, CurrentTOA, ForcePrintVoucher)
  else
    PrepareReportData(PR_CHECK_LINES, CurrentTOA, ForcePrintVoucher, DontPrintBankInfo, CheckDate);
end;

procedure TDM_REGULAR_CHECK.PrepareReportData(PR_CHECK_LINES: TevClientDataSet; CurrentTOA: boolean; ForcePrintVoucher: boolean;
  DontPrintBankInfo: Boolean; CheckDate: TDateTime = 0);
var
  SBBankNbr, BankAcctNbr, MICRCheckNbrStart, MICRBankAcctStart, i: Integer;
  Offset: Integer;
  TempStr, ShowDBDTAddress, DBDTLevel, ManualClBankAcctNbr, s, sAbaNbr: String;
  NetDirectDeposit, InOverflow, MemoPrinted, ManualCheck, bIsProc, ShowClientName: Boolean;
  aCheckStubRecord: TCheckStubRecord;
  tempList: TStringList;
  TempVar: Variant;
  OldPunchInDate, OldPunchOutDate, PunchInDate, PunchOutDate: TDateTime;
  EmployeeSsnForXml, CompanyNameForXml, Address1ForXml, Address2ForXml,
  CityForXml, StateForXml, Zip_CodeForXml, Phone1ForXml: String;
  ODBBankCheck : string;
begin
  InOverflow := False;
  ManualClBankAcctNbr := '';

  if FReversePrinting then
    wwcsChecks.Last
  else
    wwcsChecks.First;

  while not (wwcsChecks.EOF and not FReversePrinting or wwcsChecks.BOF and FReversePrinting) do
  begin
    DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + wwcsChecks.FieldByName('CO_NBR').AsString);
    if DM_EMPLOYEE.EE.IndexFieldNames <> 'EE_NBR' then
      DM_EMPLOYEE.EE.IndexFieldNames := 'EE_NBR';
    Assert(DM_EMPLOYEE.EE.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]), Format(sAssertString, ['wwcsChecks.EE_NBR', 'DM_EMPLOYEE.EE.EE_NBR', wwcsChecks.FieldByName('EE_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

    NetDirectDeposit := False;
    if wwcsChecks.FieldByName('NET_WAGES').Value = 0 then
      NetDirectDeposit := True;

    ManualCheck := (wwcsChecks.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER])
      and (wwcsChecks.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PENDING);
    Inc(FCheckNbr);
    CHECK_DATA.Insert;
    CHECK_DATA.FieldByName('CheckNbr').Value := FCheckNbr;
    CHECK_DATA.FieldByName('EE_NBR').Value := wwcsChecks.FieldByName('EE_NBR').Value;
    CHECK_DATA.FieldByName('EmployeeHireDate').Value := wwcsChecks.FieldByName('CURRENT_HIRE_DATE').Value;
    CHECK_DATA.FieldByName('EmployeeNumber').Value := wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').Value;
    CHECK_DATA.FieldByName('CompanyNumber').Value := wwcsChecks.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    CHECK_DATA.FieldByName('CheckDate').Value := iff(CheckDate = 0, wwcsChecks.FieldByName('CHECK_DATE').Value, CheckDate);
    CHECK_DATA.FieldByName('PeriodBeginDate').Value := wwcsChecks.FieldByName('PERIOD_BEGIN_DATE').Value;
    CHECK_DATA.FieldByName('PeriodEndDate').Value := wwcsChecks.FieldByName('PERIOD_END_DATE').Value;
    CHECK_DATA.FieldByName('PaymentSerialNumber').Value := wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').Value;
    CHECK_DATA.FieldByName('MICRHorizontalAdjustment').Value := DM_SERVICE_BUREAU.SB.FieldByName('MICR_HORIZONTAL_ADJUSTMENT').Value;

    if not InOverflow then
      CheckStub.Clear;

    if NetDirectDeposit then
      CHECK_DATA.FieldByName('CheckNumberOrVoucher').Value := 'Memo'
    else if not DontPrintBankInfo then
      CHECK_DATA.FieldByName('CheckNumberOrVoucher').Value := wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString
    else
      CHECK_DATA.FieldByName('CheckNumberOrVoucher').Value := '';


    if NetDirectDeposit then
      CHECK_DATA.FieldByName('CheckAmount').Value := '$************'
    else
    if ManualCheck then
      CHECK_DATA.FieldByName('CheckAmount').Value := '$************'
    else
      CHECK_DATA.FieldByName('CheckAmount').Value := '$' + PadStringLeft(FloatToStrF(wwcsChecks.FieldByName('NET_WAGES').Value, ffNumber, 15, 2), '*', 12);

    if InOverflow then
    begin
      CHECK_DATA.FieldByName('CheckAmount').Value := '$************';
      CHECK_DATA.FieldByName('CheckAmountSpelled').Value := '********************************';
    end
    else
      CHECK_DATA.FieldByName('CheckAmountSpelled').Value := GetNumberText(wwcsChecks.FieldByName('NET_WAGES').Value);

    DM_CLIENT.CL.Activate;
    if ((DM_CLIENT.CL.FieldByName('SECURITY_FONT').AsString = 'Y') or (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value = 'Y')) and (not
      InOverflow) and (wwcsChecks.FieldByName('NET_WAGES').AsInteger >= 0) then
      CHECK_DATA.FieldByName('CheckAmountSecurity').Value := ConvertAmountForSecurityFont(wwcsChecks.FieldByName('NET_WAGES').Value)
    else
      CHECK_DATA.FieldByName('CheckAmountSecurity').Value := '';

    ODBBankCheck := GetOBCBankAccountInfo;

    if NetDirectDeposit or ManualCheck then
      CHECK_DATA.FieldByName('CheckMessage').Value := 'NON NEGOTIABLE'
    else if (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value = 'Y') and (DM_SERVICE_BUREAU.SB.AR_EXPORT_FORMAT.Value = OBC_BBT_MONEYGRAM) then
      CHECK_DATA.FieldByName('CheckMessage').Value := ''
    else if (ODBBankCheck = OBC_CA_UNION_BANK) and (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') then
    begin
      CHECK_DATA.FieldByName('CheckMessage').Value := 'Void After 6 Months';
      if (Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString) <> '') and
        (Length(Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString)) <= 7) then
        CHECK_DATA.FieldByName('CheckMessage').Value := Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString) + ', ' + CHECK_DATA.FieldByName('CheckMessage').Value;
    end
    else if (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') and not (wwcsChecks.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER]) and
      (ODBBankCheck <> OBC_BBT_MONEYGRAM) then
        if ODBBankCheck = OBC_DENVER_NONE then
          CHECK_DATA.FieldByName('CheckMessage').Value := 'OFFICIAL CHECK'
        else if ODBBankCheck = OBC_WELLS_CASHIERS then
          CHECK_DATA.FieldByName('CheckMessage').Value := 'CASHIERS CHECK'
        else
          CHECK_DATA.FieldByName('CheckMessage').Value := 'TELLER CHECK'
    else
      CHECK_DATA.FieldByName('CheckMessage').Value := ConvertNull(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').Value, '');

    if not NetDirectDeposit and not (wwcsChecks.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER]) and
       (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') then
    begin
      ODBBankCheck := GetOBCBankAccountInfo;
      CHECK_DATA.FieldByName('OBCCheckMessage').Value := 'Issued by Integrated Payment Systems Inc. Englewood, CO 800-223-7520';
      if ODBBankCheck = OBC_WELLS_CASHIERS then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := ''
      else if ODBBankCheck = OBC_WELLS_FARGO_NONE then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := CHECK_DATA.FieldByName('OBCCheckMessage').Value + #13'Payable through Wells Fargo Bank Ltd., N.A. Los Angeles CA'
      else if ODBBankCheck = OBC_DENVER_NONE then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := CHECK_DATA.FieldByName('OBCCheckMessage').Value + #13'Wells Fargo, Los Angeles, California'
      else if ODBBankCheck = OBC_BBT_MONEYGRAM then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := ''
      else if ODBBankCheck = OBC_CA_UNION_BANK then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := 'UNION BANK OF CALIFORNIA' +
          #13'7108 N. Fresno Street, Fresno, CA 93720'
      else
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := CHECK_DATA.FieldByName('OBCCheckMessage').Value + ', To Citibank, NA., Buffalo, NY';
    end;

    with DM_COMPANY, DM_EMPLOYEE do
    begin
      CO_DIVISION.DataRequired('ALL');
      if CO_DIVISION.IndexFieldNames <> 'CO_DIVISION_NBR' then
        CO_DIVISION.IndexFieldNames := 'CO_DIVISION_NBR';
      CO_BRANCH.DataRequired('ALL');
      if CO_BRANCH.IndexFieldNames <> 'CO_BRANCH_NBR' then
        CO_BRANCH.IndexFieldNames := 'CO_BRANCH_NBR';
      CO_DEPARTMENT.DataRequired('ALL');
      if CO_DEPARTMENT.IndexFieldNames <> 'CO_DEPARTMENT_NBR' then
        CO_DEPARTMENT.IndexFieldNames := 'CO_DEPARTMENT_NBR';
      CO_TEAM.DataRequired('ALL');
      if CO_TEAM.IndexFieldNames <> 'CO_TEAM_NBR' then
        CO_TEAM.IndexFieldNames := 'CO_TEAM_NBR';
    end;

    ShowDBDTAddress := '';
    DBDTLevel := DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString;
    with DM_COMPANY, DM_EMPLOYEE do
    repeat
      if ShowDBDTAddress <> '' then
        DBDTLevel := IntToStr(StrToInt(DBDTLevel) - 1);
      case DBDTLevel[1] of
      CLIENT_LEVEL_DIVISION:
        begin
          Assert(CO_DIVISION.FindKey([EE.FieldByName('CO_DIVISION_NBR').Value]), Format(sAssertString, ['EE.CO_DIVISION_NBR', 'CO_DIVISION.CO_DIVISION_NBR', EE.FieldByName('CO_DIVISION_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_DIVISION['PRINT_DIV_ADDRESS_ON_CHECKS'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end;
      CLIENT_LEVEL_BRANCH:
        begin
          Assert(CO_BRANCH.FindKey([EE.FieldByName('CO_BRANCH_NBR').Value]), Format(sAssertString, ['EE.CO_BRANCH_NBR', 'CO_BRANCH.CO_BRANCH_NBR', EE.FieldByName('CO_BRANCH_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_BRANCH['PRINT_BRANCH_ADDRESS_ON_CHECK'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end;
      CLIENT_LEVEL_DEPT:
        begin
          Assert(CO_DEPARTMENT.FindKey([EE.FieldByName('CO_DEPARTMENT_NBR').Value]), Format(sAssertString, ['EE.CO_DEPARTMENT_NBR', 'CO_DEPARTMENT.CO_DEPARTMENT_NBR', EE.FieldByName('CO_DEPARTMENT_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_DEPARTMENT['PRINT_DEPT_ADDRESS_ON_CHECKS'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end;
      CLIENT_LEVEL_TEAM:
        begin
          Assert(CO_TEAM.FindKey([EE.FieldByName('CO_TEAM_NBR').Value]), Format(sAssertString, ['EE.CO_TEAM_NBR', 'CO_TEAM.CO_TEAM_NBR', EE.FieldByName('CO_TEAM_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_TEAM['PRINT_GROUP_ADDRESS_ON_CHECK'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end
      else
        ShowDBDTAddress := EXCLUDE_REPT_AND_ADDRESS_ON_CHECK;
      end;
    until (DBDTLevel = CLIENT_LEVEL_COMPANY) or (ShowDBDTAddress = INCLUDE_REPT_AND_ADDRESS_ON_CHECK);

    ShowClientName := DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES;
    if ShowClientName then
      CHECK_DATA.FieldByName('EntityName').Value := DM_CLIENT.CL.NAME.Value
    else if ShowDBDTAddress = INCLUDE_REPT_AND_ADDRESS_ON_CHECK then
      with DM_COMPANY, DM_EMPLOYEE do
        CHECK_DATA.FieldByName('EntityName').Value := ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('NAME', DBDTLevel,
          wwcsChecks.FieldByName('EE_NBR').Value, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '')
    else
      CHECK_DATA.FieldByName('EntityName').Value := wwcsChecks.FieldByName('NAME').AsString;
    CHECK_DATA.FieldByName('CompanyName').Value := CHECK_DATA.FieldByName('EntityName').Value;

    if InOverflow then
      CHECK_DATA.FieldByName('EntityAddress1').Value := ''
    else
    begin
      if ShowClientName then
        CHECK_DATA.FieldByName('EntityAddress1').Value := DM_CLIENT.CL.ADDRESS1.Value
      else if ShowDBDTAddress = INCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        with DM_COMPANY, DM_EMPLOYEE do
          CHECK_DATA.FieldByName('EntityAddress1').Value := ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('ADDRESS1', DBDTLevel,
            wwcsChecks.FieldByName('EE_NBR').Value, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '')
      else
        CHECK_DATA.FieldByName('EntityAddress1').Value := wwcsChecks.Fields[19].AsString;
    end;

    CHECK_DATA.FieldByName('EntityAddress3').Value := '';
    if InOverflow then
      CHECK_DATA.FieldByName('EntityAddress2').Value := ''
    else
    begin
      if ShowClientName then
      begin
        if DM_CLIENT.CL.ADDRESS2.Value = '' then
          CHECK_DATA.FieldByName('EntityAddress2').Value := DM_CLIENT.CL.CITY.AsString + ', ' + DM_CLIENT.CL.STATE.AsString + ' ' + DM_CLIENT.CL.ZIP_CODE.AsString
        else
        begin
          CHECK_DATA.FieldByName('EntityAddress2').Value := DM_CLIENT.CL.ADDRESS2.Value;
          CHECK_DATA.FieldByName('EntityAddress3').Value := DM_CLIENT.CL.CITY.AsString + ', ' + DM_CLIENT.CL.STATE.AsString + ' ' + DM_CLIENT.CL.ZIP_CODE.AsString;
        end;
      end
      else if ShowDBDTAddress = INCLUDE_REPT_AND_ADDRESS_ON_CHECK then
      begin
        with DM_COMPANY, DM_EMPLOYEE do
          TempVar := ctx_PayrollCalculation.GetHomeDBDTFieldValue('ADDRESS2', DBDTLevel, wwcsChecks.FieldByName('EE_NBR').Value,
            CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True);
        if ConvertNull(TempVar, '') = '' then
          with DM_COMPANY, DM_EMPLOYEE do
          begin
            CHECK_DATA.FieldByName('EntityAddress2').Value := ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('CITY', DBDTLevel,
              wwcsChecks.FieldByName('EE_NBR').Value, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '') + ', '
              + ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('STATE', DBDTLevel, wwcsChecks.FieldByName('EE_NBR').Value,
                CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '') + ' '
              + ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('ZIP_CODE', DBDTLevel, wwcsChecks.FieldByName('EE_NBR').Value,
                CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '');
          end
        else
          with DM_COMPANY, DM_EMPLOYEE do
          begin
            CHECK_DATA.FieldByName('EntityAddress2').Value := TempVar;
            CHECK_DATA.FieldByName('EntityAddress3').Value := ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('CITY', DBDTLevel,
              wwcsChecks.FieldByName('EE_NBR').Value, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '') + ', '
              + ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('STATE', DBDTLevel, wwcsChecks.FieldByName('EE_NBR').Value,
                CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '') + ' '
              + ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('ZIP_CODE', DBDTLevel, wwcsChecks.FieldByName('EE_NBR').Value,
                CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '');
          end;
      end
      else
      begin
        if ConvertNull(wwcsChecks.Fields[20].Value, '') = '' then
          CHECK_DATA.FieldByName('EntityAddress2').Value := wwcsChecks.Fields[21].AsString + ', ' + wwcsChecks.Fields[22].AsString + ' ' + wwcsChecks.Fields[23].AsString
        else
        begin
          CHECK_DATA.FieldByName('EntityAddress2').Value := wwcsChecks.Fields[20].AsString;
          CHECK_DATA.FieldByName('EntityAddress3').Value := wwcsChecks.Fields[21].AsString + ', ' + wwcsChecks.Fields[22].AsString + ' ' + wwcsChecks.Fields[23].AsString;
        end;
      end;
    end;

    CHECK_DATA.FieldByName('EmployeeName').Value := wwcsChecks.FieldByName('FIRST_NAME').AsString + ' ' + wwcsChecks.FieldByName('MIDDLE_INITIAL').AsString + ' '
      + wwcsChecks.FieldByName('LAST_NAME').AsString;
    if Trim(wwcsChecks.FieldByName('EE_ADDRESS1').AsString) = '' then
    begin
      if InOverflow then
        CHECK_DATA.FieldByName('EmployeeAddress1').Value := ''
      else
        CHECK_DATA.FieldByName('EmployeeAddress1').Value := wwcsChecks.Fields[9].AsString;
      CHECK_DATA.FieldByName('EmployeeAddress3').Value := '';
      if InOverflow then
        CHECK_DATA.FieldByName('EmployeeAddress2').Value := ''
      else
      begin
        if ConvertNull(wwcsChecks.Fields[10].Value, '') = '' then
          CHECK_DATA.FieldByName('EmployeeAddress2').Value := wwcsChecks.Fields[11].AsString + ', ' + wwcsChecks.Fields[12].AsString + ' ' + wwcsChecks.Fields[13].AsString
        else
        begin
          CHECK_DATA.FieldByName('EmployeeAddress2').Value := wwcsChecks.Fields[10].AsString;
          CHECK_DATA.FieldByName('EmployeeAddress3').Value := wwcsChecks.Fields[11].AsString + ', ' + wwcsChecks.Fields[12].AsString + ' ' + wwcsChecks.Fields[13].AsString;
        end;
      end;
    end
    else
    begin
      if InOverflow then
        CHECK_DATA.FieldByName('EmployeeAddress1').Value := ''
      else
        CHECK_DATA.FieldByName('EmployeeAddress1').Value := wwcsChecks.FieldByName('EE_ADDRESS1').AsString;
      CHECK_DATA.FieldByName('EmployeeAddress3').Value := '';
      if InOverflow then
        CHECK_DATA.FieldByName('EmployeeAddress2').Value := ''
      else
      begin
        if ConvertNull(wwcsChecks.FieldByName('EE_ADDRESS2').Value, '') = '' then
          CHECK_DATA.FieldByName('EmployeeAddress2').Value := wwcsChecks.FieldByName('EE_CITY').AsString + ', '
          + wwcsChecks.FieldByName('EE_STATE').AsString + ' ' + wwcsChecks.FieldByName('EE_ZIP_CODE').AsString
        else
        begin
          CHECK_DATA.FieldByName('EmployeeAddress2').Value := wwcsChecks.FieldByName('EE_ADDRESS2').AsString;
          CHECK_DATA.FieldByName('EmployeeAddress3').Value := wwcsChecks.FieldByName('EE_CITY').AsString + ', '
          + wwcsChecks.FieldByName('EE_STATE').AsString + ' ' + wwcsChecks.FieldByName('EE_ZIP_CODE').AsString;
        end;
      end;
    end;

    if DM_COMPANY.CO.FieldByName('SHOW_SS_NUMBER_ON_CHECK').Value = 'Y' then
      CHECK_DATA.FieldByName('EmployeeSSN').Value := wwcsChecks.FieldByName('SOCIAL_SECURITY_NUMBER').Value
    else if DM_COMPANY.CO.FieldByName('SHOW_SS_NUMBER_ON_CHECK').Value = GROUP_BOX_LAST4 then
      CHECK_DATA.FieldByName('EmployeeSSN').Value := 'XXX-XX-' + Copy(wwcsChecks.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, 8, 4)
    else
      CHECK_DATA.FieldByName('EmployeeSSN').Value := '';

    if not VarIsNull(wwcsChecks.FieldByName('CO_DIVISION_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_DIVISION.FindKey([wwcsChecks.FieldByName('CO_DIVISION_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_DIVISION_NBR', 'CO_DIVISION.CO_DIVISION_NBR', wwcsChecks.FieldByName('CO_DIVISION_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_DIVISION['PRINT_DIV_ADDRESS_ON_CHECKS'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        CHECK_DATA.FieldByName('DivisionNumber').Value := Trim(ConvertNull(DM_COMPANY.CO_DIVISION['CUSTOM_DIVISION_NUMBER'], ''))
      else
        CHECK_DATA.FieldByName('DivisionNumber').Value := '';
    end
    else
      CHECK_DATA.FieldByName('DivisionNumber').Value := '';
    if not VarIsNull(wwcsChecks.FieldByName('CO_BRANCH_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_BRANCH.FindKey([wwcsChecks.FieldByName('CO_BRANCH_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_BRANCH_NBR', 'CO_BRANCH.CO_BRANCH_NBR', wwcsChecks.FieldByName('CO_BRANCH_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_BRANCH['PRINT_BRANCH_ADDRESS_ON_CHECK'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        CHECK_DATA.FieldByName('BranchNumber').Value := Trim(ConvertNull(DM_COMPANY.CO_BRANCH['CUSTOM_BRANCH_NUMBER'], ''))
      else
        CHECK_DATA.FieldByName('BranchNumber').Value := '';
    end
    else
      CHECK_DATA.FieldByName('BranchNumber').Value := '';
    if not VarIsNull(wwcsChecks.FieldByName('CO_DEPARTMENT_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_DEPARTMENT.FindKey([wwcsChecks.FieldByName('CO_DEPARTMENT_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_DEPARTMENT_NBR', 'CO_DEPARTMENT.CO_DEPARTMENT_NBR', wwcsChecks.FieldByName('CO_DEPARTMENT_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_DEPARTMENT['PRINT_DEPT_ADDRESS_ON_CHECKS'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        CHECK_DATA.FieldByName('DepartmentNumber').Value := Trim(ConvertNull(DM_COMPANY.CO_DEPARTMENT['CUSTOM_DEPARTMENT_NUMBER'], ''))
      else
        CHECK_DATA.FieldByName('DepartmentNumber').Value := '';
    end
    else
      CHECK_DATA.FieldByName('DepartmentNumber').Value := '';
    if not VarIsNull(wwcsChecks.FieldByName('CO_TEAM_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_TEAM.FindKey([wwcsChecks.FieldByName('CO_TEAM_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_TEAM_NBR', 'CO_TEAM.CO_TEAM_NBR', wwcsChecks.FieldByName('CO_TEAM_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_TEAM['PRINT_GROUP_ADDRESS_ON_CHECK'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        CHECK_DATA.FieldByName('TeamNumber').Value := Trim(ConvertNull(DM_COMPANY.CO_TEAM['CUSTOM_TEAM_NUMBER'], ''))
      else
        CHECK_DATA.FieldByName('TeamNumber').Value := '';
    end
    else
      CHECK_DATA.FieldByName('TeamNumber').Value := '';

   DM_CLIENT.CL_BANK_ACCOUNT.Activate;
    if DM_CLIENT.CL_BANK_ACCOUNT.IndexFieldNames <> 'CL_BANK_ACCOUNT_NBR' then
      DM_CLIENT.CL_BANK_ACCOUNT.IndexFieldNames := 'CL_BANK_ACCOUNT_NBR';
    if GetBankAccountNumberAsOfDate(wwcsChecks, BankAcctNbr) then
    begin
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
      if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.IndexFieldNames <> 'SB_BANK_ACCOUNTS_NBR' then
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.IndexFieldNames := 'SB_BANK_ACCOUNTS_NBR';
      Assert(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FindKey([BankAcctNbr]), Format(sAssertString, ['BankAcctNbr', 'SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS_NBR', IntToStr(BankAcctNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      SBBankNbr := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS['SB_BANKS_NBR'];
      if not NetDirectDeposit and not InOverflow and not ManualCheck then
        if DM_COMPANY.CO.FieldByName('REMOTE_OF_CLIENT').Value <> PRINT_SIGNATURE_ON_MISC_CHECKS then
        begin
          if DontPrintBankInfo then
            CHECK_DATA.FieldByName('Signature_Nbr').Value := -1
          else begin
            CHECK_DATA.FieldByName('Signature_Nbr').Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS['SIGNATURE_SB_BLOB_NBR'];
            CHECK_DATA.FieldByName('Signature_Db').Value := 'B'
          end;
        end
    end
    else
    begin
      Assert(DM_CLIENT.CL_BANK_ACCOUNT.FindKey([BankAcctNbr]), Format(sAssertString, ['BankAcctNbr', 'CL_BANK_ACCOUNT.CL_BANK_ACCOUNT_NBR', IntToStr(BankAcctNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      SBBankNbr := DM_CLIENT.CL_BANK_ACCOUNT['SB_BANKS_NBR'];
      if not NetDirectDeposit and not InOverflow and not ManualCheck then
        if DM_COMPANY.CO.FieldByName('REMOTE_OF_CLIENT').Value <> PRINT_SIGNATURE_ON_MISC_CHECKS then
        begin
          if DontPrintBankInfo then
            CHECK_DATA.FieldByName('Signature_Nbr').Value := -1
          else begin
            CHECK_DATA.FieldByName('Signature_Nbr').Value := DM_CLIENT.CL_BANK_ACCOUNT['SIGNATURE_CL_BLOB_NBR'];
            CHECK_DATA.FieldByName('Signature_Db').Value := 'C'
          end;
        end;
    end;

    if not (InOverflow or DontPrintBankInfo) then
      CHECK_DATA.FieldByName('Logo_Nbr').Value := DM_CLIENT.CL_BANK_ACCOUNT['LOGO_CL_BLOB_NBR'];

    DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');
    if DM_SERVICE_BUREAU.SB_BANKS.IndexFieldNames <> 'SB_BANKS_NBR' then
      DM_SERVICE_BUREAU.SB_BANKS.IndexFieldNames := 'SB_BANKS_NBR';
    Assert(DM_SERVICE_BUREAU.SB_BANKS.FindKey([SBBankNbr]), Format(sAssertString, ['SBBankNbr', 'SB_BANKS.SB_BANKS_NBR', IntToStr(SBBankNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));


    if DontPrintBankInfo then
    begin
      CHECK_DATA.FieldByName('BankName').Value := '';
      CHECK_DATA.FieldByName('BankAddress').Value := '';
    end
    else begin
      s := Trim(VarToStr(DM_SERVICE_BUREAU.SB_BANKS['ADDRESS1']) + ' ' +
                VarToStr(DM_SERVICE_BUREAU.SB_BANKS['ADDRESS2']));
      if s <> '' then
        s := s + ', ';
      s := s +
          VarToStr(DM_SERVICE_BUREAU.SB_BANKS['CITY']) + ', ' +
          VarToStr(DM_SERVICE_BUREAU.SB_BANKS['STATE']){ + ' ' +
          VarToStr(DM_SERVICE_BUREAU.SB_BANKS['ZIP_CODE'])};
      CHECK_DATA.FieldByName('BankName').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS['PRINT_NAME']);
      CHECK_DATA.FieldByName('BankAddress').Value := s;
    end;

    if NetDirectDeposit or InOverflow or ManualCheck or DontPrintBankInfo then
      CHECK_DATA.FieldByName('MICRLine').Value := ''
    else
    begin
      sAbaNbr := ConvertNull(wwcsChecks.FieldByName('ABA_NUMBER').AsString,VarToStr(DM_SERVICE_BUREAU.SB_BANKS['ABA_NUMBER']));
      MICRCheckNbrStart := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS['MICR_CHECK_NUMBER_START_POSITN'], 0);
      MICRBankAcctStart := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS['MICR_ACCOUNT_START_POSITION'], 0);

      ODBBankCheck := GetOBCBankAccountInfo;
      if (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') and not (wwcsChecks.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_3RD_PARTY, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER]) and
         (ODBBankCheck <> OBC_WELLS_FARGO_NONE) and
         (ODBBankCheck <> OBC_WELLS_CASHIERS) and
         (ODBBankCheck <> OBC_BBT_MONEYGRAM) then
      begin
        if ODBBankCheck = OBC_CITIBANK_NONE then
        begin
          TempStr := StringOfChar(' ', MICRBankAcctStart) + '>' + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString + '>';
          TempStr := TempStr + ' |' + sAbaNbr + '|';
          TempStr := TempStr + StringOfChar(' ', MICRCheckNbrStart + 1) + '2500' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 9) + '9>';
        end
        else if ODBBankCheck = OBC_DENVER_NONE then
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 9) + '>';
          TempStr := TempStr + '   |' + sAbaNbr
            + '|4174175646>' + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString;
        end
        else if ODBBankCheck = OBC_CA_UNION_BANK then
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + ' >' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 10) + '>';
          TempStr := TempStr + ' |' + sAbaNbr
            + '|' + StringOfChar(' ', MICRBankAcctStart) + ' ' + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString + '>';
        end
        else
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 9) + '>';
          TempStr := TempStr + '   |' + sAbaNbr
            + '>' + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString;
        end
      end
      else if (ODBBankCheck = OBC_BBT_MONEYGRAM) then
      begin
        if (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value <> 'N') then
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '   >' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 8) + '>';
          TempStr := TempStr + ' |' + sAbaNbr
            + '|' + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString + '>';
        end
        else begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '   >' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 6) + '>';
          TempStr := TempStr + ' |' + sAbaNbr
            + '|' + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString + '>';
        end;
      end
      else begin
        TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 6) + '>';
        TempStr := TempStr + ' |' + sAbaNbr
          + '|' + StringOfChar(' ', MICRBankAcctStart + 1);

        if (wwcsChecks.FieldByName('CHECK_TYPE').AsString[1] <> CHECK_TYPE2_MANUAL) or (DM_COMPANY.CO.FieldByName('MANUAL_CL_BANK_ACCOUNT_NBR').IsNull) then
          TempStr := TempStr + wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString + '>'
        else begin
          if ManualClBankAcctNbr = '' then
            ManualClBankAcctNbr := VarToStr(DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO.FieldByName('MANUAL_CL_BANK_ACCOUNT_NBR').AsInteger, 'CUSTOM_BANK_ACCOUNT_NUMBER'));
          TempStr := TempStr + ManualClBankAcctNbr + '>';
        end;

        if TempStr[Length(TempStr) - 1] = '~' then
          Delete(TempStr, Length(TempStr) - 1, 2);
      end;

      CHECK_DATA.FieldByName('MICRLine').Value := TempStr;
    end;
    if DontPrintBankInfo then
    begin
      CHECK_DATA.FieldByName('BankTopABANumber').Value := '';
      CHECK_DATA.FieldByName('BankBottomABANumber').Value := '';
    end
    else begin
      CHECK_DATA.FieldByName('BankTopABANumber').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS['TOP_ABA_NUMBER']);
      CHECK_DATA.FieldByName('BankBottomABANumber').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS['BOTTOM_ABA_NUMBER']);
    end;
    CHECK_DATA.FieldByName('CheckComments').Value := wwcsChecks.FieldByName('CHECK_COMMENTS').Value;

    if not InOverflow then
    begin
      if (DM_COMPANY.CO.SHOW_SS_NUMBER_ON_CHECK.Value = 'Y')
      or (DM_COMPANY.CO.SHOW_SS_NUMBER_ON_CHECK.Value = GROUP_BOX_LAST4) then
        EmployeeSsnForXml := 'XXX-XX-' + copy(wwcsChecks.FieldByName('SOCIAL_SECURITY_NUMBER').AsString,8,4)
      else
        EmployeeSsnForXml := '';

      if DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString = 'Y' then
      begin
        CompanyNameForXml := DM_CLIENT.CL.NAME.AsString;
        Address1ForXml := DM_CLIENT.CL.ADDRESS1.AsString;
        Address2ForXml := DM_CLIENT.CL.ADDRESS2.AsString;
        CityForXml := DM_CLIENT.CL.CITY.AsString;
        StateForXml := DM_CLIENT.CL.STATE.AsString;
        Zip_CodeForXml := DM_CLIENT.CL.ZIP_CODE.AsString;
        Phone1ForXml := DM_CLIENT.CL.PHONE1.AsString;
      end
      else begin
        //company level
        CompanyNameForXml := DM_COMPANY.CO.NAME.AsString;
        Address1ForXml := DM_COMPANY.CO.ADDRESS1.AsString;
        Address2ForXml := DM_COMPANY.CO.ADDRESS2.AsString;
        CityForXml := DM_COMPANY.CO.CITY.AsString;
        StateForXml := DM_COMPANY.CO.STATE.AsString;
        Zip_CodeForXml := DM_COMPANY.CO.ZIP_CODE.AsString;
        Phone1ForXml := '';
        DM_COMPANY.CO_PHONE.DataRequired('CO_NBR=' + DM_COMPANY.CO.CO_NBR.AsString);
        DM_COMPANY.CO_PHONE.First;
        while not DM_COMPANY.CO_PHONE.Eof do
        begin
          if DM_COMPANY.CO_PHONE.PHONE_TYPE.AsString = 'P' then
          begin
            Phone1ForXml := DM_COMPANY.CO_PHONE.PHONE_NUMBER.AsString;
            break;
          end;
          DM_COMPANY.CO_PHONE.Next;
        end;

        if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_COMPANY))
           and not CHECK_DATA.FieldByName('DivisionNumber').IsNull
           and (Trim(DM_COMPANY.CO_DIVISION.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_DIVISION.ADDRESS1.AsString)<>'')
           and (DM_COMPANY.CO_DIVISION.PRINT_DIV_ADDRESS_ON_CHECKS.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
            CompanyNameForXml := DM_COMPANY.CO_DIVISION.NAME.AsString;
            Address1ForXml := DM_COMPANY.CO_DIVISION.ADDRESS1.AsString;
            Address2ForXml := DM_COMPANY.CO_DIVISION.ADDRESS2.AsString;
            CityForXml := DM_COMPANY.CO_DIVISION.CITY.AsString;
            StateForXml := DM_COMPANY.CO_DIVISION.STATE.AsString;
            Zip_CodeForXml := DM_COMPANY.CO_DIVISION.ZIP_CODE.AsString;
            Phone1ForXml := DM_COMPANY.CO_DIVISION.PHONE1.AsString;
        end;

        if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_DIVISION))
           and not CHECK_DATA.FieldByName('BranchNumber').IsNull
           and (Trim(DM_COMPANY.CO_BRANCH.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_BRANCH.ADDRESS1.AsString)<>'')
           and (DM_COMPANY.CO_BRANCH.PRINT_BRANCH_ADDRESS_ON_CHECK.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
            CompanyNameForXml := DM_COMPANY.CO_BRANCH.NAME.AsString;
            Address1ForXml := DM_COMPANY.CO_BRANCH.ADDRESS1.AsString;
            Address2ForXml := DM_COMPANY.CO_BRANCH.ADDRESS2.AsString;
            CityForXml := DM_COMPANY.CO_BRANCH.CITY.AsString;
            StateForXml := DM_COMPANY.CO_BRANCH.STATE.AsString;
            Zip_CodeForXml := DM_COMPANY.CO_BRANCH.ZIP_CODE.AsString;
            Phone1ForXml := DM_COMPANY.CO_BRANCH.PHONE1.AsString;
        end;

        if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_BRANCH))
           and not CHECK_DATA.FieldByName('DepartmentNumber').IsNull
           and (Trim(DM_COMPANY.CO_DEPARTMENT.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_DEPARTMENT.ADDRESS1.AsString)<>'')
           and (DM_COMPANY.CO_DEPARTMENT.PRINT_DEPT_ADDRESS_ON_CHECKS.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
            CompanyNameForXml := DM_COMPANY.CO_DEPARTMENT.NAME.AsString;
            Address1ForXml := DM_COMPANY.CO_DEPARTMENT.ADDRESS1.AsString;
            Address2ForXml := DM_COMPANY.CO_DEPARTMENT.ADDRESS2.AsString;
            CityForXml := DM_COMPANY.CO_DEPARTMENT.CITY.AsString;
            StateForXml := DM_COMPANY.CO_DEPARTMENT.STATE.AsString;
            Zip_CodeForXml := DM_COMPANY.CO_DEPARTMENT.ZIP_CODE.AsString;
            Phone1ForXml := DM_COMPANY.CO_DEPARTMENT.PHONE1.AsString;
        end;

        if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_DEPT))
           and not CHECK_DATA.FieldByName('TeamNumber').IsNull
           and (Trim(DM_COMPANY.CO_TEAM.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_TEAM.ADDRESS1.AsString)<>'')
           and (DM_COMPANY.CO_TEAM.PRINT_GROUP_ADDRESS_ON_CHECK.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
            CompanyNameForXml := DM_COMPANY.CO_TEAM.NAME.AsString;
            Address1ForXml := DM_COMPANY.CO_TEAM.ADDRESS1.AsString;
            Address2ForXml := DM_COMPANY.CO_TEAM.ADDRESS2.AsString;
            CityForXml := DM_COMPANY.CO_TEAM.CITY.AsString;
            StateForXml := DM_COMPANY.CO_TEAM.STATE.AsString;
            Zip_CodeForXml := DM_COMPANY.CO_TEAM.ZIP_CODE.AsString;
            Phone1ForXml := DM_COMPANY.CO_TEAM.PHONE1.AsString;
        end;
      end;

      AddXMLHeader(CHECK_DATA.FieldByName('EmployeeName').AsString,
        CHECK_DATA.FieldByName('CompanyName').AsString,
        CompanyNameForXml,
        Address1ForXml,
        Address2ForXml,
        CityForXml,
        StateForXml,
        Zip_CodeForXml,
        Phone1ForXml);
    end;

  // Check stub
    DM_COMPANY.CO_JOBS.DataRequired('CO_NBR=' + wwcsChecks.FieldByName('CO_NBR').AsString);
    if DM_COMPANY.CO_JOBS.IndexFieldNames <> 'CO_JOBS_NBR' then
      DM_COMPANY.CO_JOBS.IndexFieldNames := 'CO_JOBS_NBR';

    FPrCheckNbr := wwcsChecks.FieldByName('PR_CHECK_NBR').Value;

    AddTOA(CurrentTOA);

    if DM_CLIENT.CL_E_DS.IndexFieldNames <> 'CL_E_DS_NBR' then
      DM_CLIENT.CL_E_DS.IndexFieldNames := 'CL_E_DS_NBR';

    if not InOverflow then
    begin
      AddXMLDetails(CHECK_DATA.FieldByName('CompanyNumber').AsString,
        CHECK_DATA.FieldByName('EmployeeNumber').AsString,
        CHECK_DATA.FieldByName('EmployeeHireDate').AsString,
        CHECK_DATA.FieldByName('PeriodBeginDate').AsString,
        CHECK_DATA.FieldByName('PeriodEndDate').AsString,
        CHECK_DATA.FieldByName('CheckDate').AsString,
        CHECK_DATA.FieldByName('PaymentSerialNumber').AsString,
        CHECK_DATA.FieldByName('DivisionNumber').AsString,
        CHECK_DATA.FieldByName('BranchNumber').AsString,
        CHECK_DATA.FieldByName('DepartmentNumber').AsString,
        CHECK_DATA.FieldByName('TeamNumber').AsString,
        CHECK_DATA.FieldByName('CheckComments').AsString,
        EmployeeSsnForXml
        );
    end;

    if not InOverflow then
    with PR_CHECK_LINES do
    begin
      if IndexFieldNames <> 'PR_CHECK_NBR;CL_E_DS_NBR;RATE_OF_PAY;CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR;CO_JOBS_NBR;PUNCH_IN;PUNCH_OUT' then
        IndexFieldNames := 'PR_CHECK_NBR;CL_E_DS_NBR;RATE_OF_PAY;CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR;CO_JOBS_NBR;PUNCH_IN;PUNCH_OUT';

      if CL_E_DS_YTD.IndexFieldNames <> 'EE_NBR;CUSTOM_E_D_CODE_NUMBER' then
        CL_E_DS_YTD.IndexFieldNames := 'EE_NBR;CUSTOM_E_D_CODE_NUMBER';
      CL_E_DS_YTD.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]);

      AddEarnings(PR_CHECK_LINES);

      if DM_PAYROLL.PR_CHECK.IndexFieldNames <> 'PR_CHECK_NBR' then
        DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
      Assert(DM_PAYROLL.PR_CHECK.FindKey([FPrCheckNbr]), Format(sAssertString, ['wwcsChecks.PR_CHECK_NBR', 'PR_CHECK.PR_CHECK_NBR', IntToStr(FPrCheckNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

      AddFederalTaxes;
      AddStateTaxes;
      AddSuiTaxes;
      AddLocalTaxes;

      AddDeductions(PR_CHECK_LINES);
    end;

  // Print stub lines
    Offset := 0;

    if not InOverflow then
      FxmlData := FxmlData + '<lines>';
    while (CheckStub.NextLineToPrint < CheckStub.Count) and (Offset < FCheckStubLinesNbr) do
    begin
      CheckStub.ExtractRecord(CheckStub.NextLineToPrint, aCheckStubRecord);

      Inc(FCheckStubNbr);
      CHECK_STUB_DATA.Insert;
      CHECK_STUB_DATA.FieldByName('CheckNbr').Value := FCheckNbr;
      CHECK_STUB_DATA.FieldByName('StubLineNbr').Value := FCheckStubNbr;
      CHECK_STUB_DATA.FieldByName('EarningCode').Value := aCheckStubRecord.EarnCode;
      CHECK_STUB_DATA.FieldByName('EarningDesc').Value := aCheckStubRecord.EarnDescr;
      CHECK_STUB_DATA.FieldByName('Location').Value := aCheckStubRecord.Location;
      CHECK_STUB_DATA.FieldByName('Rate').Value := aCheckStubRecord.Rate;
      CHECK_STUB_DATA.FieldByName('Hours').Value := aCheckStubRecord.Hours;
      CHECK_STUB_DATA.FieldByName('EarningCurrent').Value := aCheckStubRecord.EarnCurrent;
      CHECK_STUB_DATA.FieldByName('EarningYTD').Value := aCheckStubRecord.EarnYTD;
      CHECK_STUB_DATA.FieldByName('DeductionDesc').Value := aCheckStubRecord.DedDescr;
      CHECK_STUB_DATA.FieldByName('DeductionCurrent').Value := aCheckStubRecord.DedCurrent;
      CHECK_STUB_DATA.FieldByName('DeductionYTD').Value := aCheckStubRecord.DedYTD;
      if FShowPunchDetails then
      begin
        CHECK_STUB_DATA.FieldByName('PunchIn').Value := aCheckStubRecord.PunchIn;
        CHECK_STUB_DATA.FieldByName('PunchOut').Value := aCheckStubRecord.PunchOut;
      end
      else
      begin
        CHECK_STUB_DATA.FieldByName('PunchIn').Value := '';
        CHECK_STUB_DATA.FieldByName('PunchOut').Value := '';
      end;
      CHECK_STUB_DATA.Post;

      AddCheckStubRecordToXmlData(aCheckStubRecord);

      Inc(Offset);
      Inc(CheckStub.NextLineToPrint);
    end;

    if (CheckStub.NextLineToPrint < CheckStub.Count) and (not FCollateChecks) then
      InOverflow := True
    else
      InOverflow := False;

    // add missing check lines to xmlData (ticket #42636) in case of CollateChecks = True
    if (CheckStub.NextLineToPrint < CheckStub.Count) and not InOverflow then
    while (CheckStub.NextLineToPrint < CheckStub.Count) do
    begin
      CheckStub.ExtractRecord(CheckStub.NextLineToPrint, aCheckStubRecord);
      AddCheckStubRecordToXmlData(aCheckStubRecord);
      Inc(CheckStub.NextLineToPrint);
    end;

  // Print totals
    if InOverflow then
      CHECK_DATA.FieldByName('TotalHours').Value := ''
    else
      CHECK_DATA.FieldByName('TotalHours').Value := FloatToStrF(CheckStub.TotalHours, ffFixed, 15, 2);

    if InOverflow then
      CHECK_DATA.FieldByName('TotalEarnings').Value := ''
    else
      CHECK_DATA.FieldByName('TotalEarnings').Value := FloatToStrF(CheckStub.TotalEarnings, ffFixed, 15, 2);

    if FShowYTD and (not InOverflow) then
      CHECK_DATA.FieldByName('TotalYTDEarnings').Value := FloatToStrF(CheckStub.YTDEarnings, ffFixed, 15, 2)
    else
      CHECK_DATA.FieldByName('TotalYTDEarnings').Value := '';

    if InOverflow then
      CHECK_DATA.FieldByName('TotalDeductions').Value := ''
    else
      CHECK_DATA.FieldByName('TotalDeductions').Value := FloatToStrF(CheckStub.TotalDeductions, ffFixed, 15, 2);

    if FShowYTD and (not InOverflow) then
      CHECK_DATA.FieldByName('TotalYTDDeductions').Value := FloatToStrF(CheckStub.YTDDeductions, ffFixed, 15, 2)
    else
      CHECK_DATA.FieldByName('TotalYTDDeductions').Value := '';

    if InOverflow then
      CHECK_DATA.FieldByName('Net').Value := ''
    else
      CHECK_DATA.FieldByName('Net').Value := FloatToStrF(wwcsChecks.FieldByName('NET_WAGES').AsFloat, ffFixed, 15, 2);

    if FShowYTD and (not InOverflow) and FEDERAL_YTD.FindKey([wwcsChecks['EE_NBR']]) then
      CHECK_DATA.FieldByName('YTDNet').Value := FloatToStrF(FEDERAL_YTD['NET_WAGES'], ffFixed, 15, 2)
    else
      CHECK_DATA.FieldByName('YTDNet').Value := '';

    if InOverflow then
      CHECK_DATA.FieldByName('TotalDirDep').Value := ''
    else
      CHECK_DATA.FieldByName('TotalDirDep').Value := FloatToStrF(CheckStub.TotalDirDep, ffFixed, 15, 2);

    if InOverflow then
      CHECK_DATA.FieldByName('NetAndDirDep').Value := ''
    else
      CHECK_DATA.FieldByName('NetAndDirDep').Value := FloatToStrF(wwcsChecks.FieldByName('NET_WAGES').AsFloat + CheckStub.TotalDirDep, ffFixed, 15, 2);

    if not InOverflow then
    begin
      AddXMLTotals(
        CHECK_DATA.FieldByName('TotalHours').AsString,
        CHECK_DATA.FieldByName('TotalEarnings').AsString,
        CHECK_DATA.FieldByName('TotalYTDEarnings').AsString,
        CHECK_DATA.FieldByName('TotalDeductions').AsString,
        CHECK_DATA.FieldByName('TotalYTDDeductions').AsString,
        CHECK_DATA.FieldByName('NetAndDirDep').AsString,
        CHECK_DATA.FieldByName('TotalDirDep').AsString,
        CHECK_DATA.FieldByName('Net').AsString,
        CHECK_DATA.FieldByName('YTDNet').AsString);

      WriteXMLStreamToCheck;
    end;

    if NetDirectDeposit and (DM_EMPLOYEE.EE.FieldByName('PRINT_VOUCHER').Value = 'N') and not ForcePrintVoucher then
      CHECK_DATA.Cancel
    else
      CHECK_DATA.Post;

    if not InOverflow then
    begin
      if FReversePrinting then
        wwcsChecks.Prior
      else
        wwcsChecks.Next;
    end;
  end;

  ctx_DataAccess.UnlockPR;
  bIsProc := ctx_DataAccess.PayrollIsProcessing;
  ctx_DataAccess.PayrollIsProcessing := True;
  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    try
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK]);
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  finally
    ctx_DataAccess.PayrollIsProcessing := bIsProc;
    ctx_DataAccess.LockPR(True);
  end;
end;

procedure TDM_REGULAR_CHECK.AddCheckStubRecordToXmlData(
  aCheckStubRecord: TCheckStubRecord);
var
  amount, hours, rate: Real;
  RateStr: String;
begin
  amount := StrToFloatDef(aCheckStubRecord.EarnCurrent, 0);
  hours := StrToFloatDef(aCheckStubRecord.Hours, 0);
  rate := StrToFloatDef(aCheckStubRecord.Rate, 0);

  if FCalculateRates and (amount > 0.001) {and (rate > 0.001)} and (hours > 0.001) then
  begin
    rate := amount / hours;
  end;

  if DM_CLIENT.CL_E_DS['FILLER'] = 'Y' then
    RateStr := FloatToStrF(rate, ffFixed, 15, 4)
  else
    RateStr := FloatToStrF(rate, ffFixed, 15, 2);

  FxmlData := FxmlData + '<checkLine>';
  FxmlData := FxmlData + '<earnDescription>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.EarnDescr)) + '</earnDescription>';
  FxmlData := FxmlData + '<earnLocation>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.Location)) + '</earnLocation>';
  FxmlData := FxmlData + '<earnRate>' + AnsiToUtf8(ProcessStringForXML(RateStr)) + '</earnRate>';
  FxmlData := FxmlData + '<earnHours>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.Hours)) + '</earnHours>';
  FxmlData := FxmlData + '<earnCurrent>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.EarnCurrent)) + '</earnCurrent>';
  FxmlData := FxmlData + '<earnPunchIn>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.PunchIn)) + '</earnPunchIn>';
  FxmlData := FxmlData + '<earnPunchOut>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.PunchOut)) + '</earnPunchOut>';
  FxmlData := FxmlData + '<earnYTD>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.EarnYTD)) + '</earnYTD>';
  FxmlData := FxmlData + '<dedDescription>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.DedDescr)) + '</dedDescription>';
  FxmlData := FxmlData + '<dedCurrent>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.DedCurrent)) + '</dedCurrent>';
  FxmlData := FxmlData + '<dedYTD>' + AnsiToUtf8(ProcessStringForXML(aCheckStubRecord.DedYTD)) + '</dedYTD>';
  FxmlData := FxmlData + '</checkLine>';
end;

procedure TDM_REGULAR_CHECK.AddDeduction(aDescription: string; Amount,
  YTD: Variant);
begin
  CheckStub.AddDeduction(aDescription, Amount, YTD);
  if FLiteDatasets then
  begin
    cdsDeductionsLite.Insert;
    cdsDeductionsLite.FieldByName('CheckNbr').Value := FPrCheckNbr;
    cdsDeductionsLite.FieldByName('Description').Value := aDescription;
    cdsDeductionsLite.FieldByName('Amount').Value := Amount;
    cdsDeductionsLite.FieldByName('YTD').Value := YTD;
    cdsDeductionsLite.Post;
  end;
end;

procedure TDM_REGULAR_CHECK.AddDeductions(aPrCheckLines: TevClientDataSet);
var
  EDCode: string;
  ShortfallAmount, LineAmount, YTD: Double;
  OldEDNbr: Integer;

  procedure AddDeductionWithEmptyAmount;
  begin
    if FShowYTD then
    begin
      if DM_CLIENT.CL_E_DS['SHOW_YTD_ON_CHECKS'] = 'Y' then
      begin
        YTD := ConvertNull(CL_E_DS_YTD.FieldByName('YTD').Value, 0);
        if YTD <> 0 then
          AddDeduction(DM_CLIENT.CL_E_DS['DESCRIPTION'], Null, YTD);
      end;
    end;
  end;
begin
// Deductions
  ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_DIRECT_DEPOSIT, wwcsChecks.FieldByName('CO_NBR').AsInteger);
  if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.IndexFieldNames <> 'EE_DIRECT_DEPOSIT_NBR' then
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.IndexFieldNames := 'EE_DIRECT_DEPOSIT_NBR';
  LineAmount := 0;
  OldEDNbr := 0;
  ShortfallAmount := 0;
  CL_E_DS_YTD.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]);
  while not CL_E_DS_YTD.EOF and (wwcsChecks.FieldByName('EE_NBR').Value = CL_E_DS_YTD.FieldByName('EE_NBR').Value) do
  begin
    Assert(DM_CLIENT.CL_E_DS.FindKey([CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]), Format(sAssertString, ['CL_E_DS_YTD.CL_E_DS_NBR', 'CL_E_DS.CL_E_DS_NBR', CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
    EDCode := DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'];
    if EDCode[1] = 'D' then
    begin
      if aPrCheckLines.FindKey([FPrCheckNbr, CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]) then
      begin
        while (aPrCheckLines.FieldByName('CL_E_DS_NBR').Value = CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value) and
              (aPrCheckLines.FieldByName('PR_CHECK_NBR').Value = wwcsChecks.FieldByName('PR_CHECK_NBR').Value) and
              (not aPrCheckLines.EOF) do
        begin
          if aPrCheckLines.FieldByName('CL_E_DS_NBR').Value <> OldEDNbr then
          begin
            if LineAmount <> 0 then
            begin
              YTD := ConvertNull(CL_E_DS_YTD.FieldByName('YTD').Value, 0);
              Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              FShowEdYtd := DM_CLIENT.CL_E_DS['SHOW_YTD_ON_CHECKS'] = 'Y';
              if DM_CLIENT.CL_E_DS['SHOW_ED_ON_CHECKS'] = 'Y' then
                PrintDeduction(OldEDNbr, LineAmount, YTD);
              if DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_DIRECT_DEPOSIT then
                CheckStub.TotalDirDep := CheckStub.TotalDirDep + LineAmount;
            end;
            if FShowShortfalls and (ShortfallAmount <> 0) then
              AddDeduction('(' + DM_CLIENT.CL_E_DS['DESCRIPTION'] + ' Shortfall ' + FloatToStrF(ShortfallAmount, ffFixed, 15, 2) + ')', Null, Null);
            OldEDNbr := aPrCheckLines.FieldByName('CL_E_DS_NBR').Value;
            LineAmount := 0;
            ShortfallAmount := 0;
          end;
          LineAmount := LineAmount + ConvertNull(aPrCheckLines.FieldByName('AMOUNT').Value, 0);
          ShortfallAmount := ShortfallAmount + ConvertNull(aPrCheckLines.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER').Value, 0);
          aPrCheckLines.Next;
        end;

        if LineAmount <> 0 then
        begin
          Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));        
          YTD := ConvertNull(CL_E_DS_YTD.FieldByName('YTD').Value, 0);
          FShowEdYtd := DM_CLIENT.CL_E_DS['SHOW_YTD_ON_CHECKS'] = 'Y';
          if DM_CLIENT.CL_E_DS['SHOW_ED_ON_CHECKS'] = 'Y' then
            PrintDeduction(OldEDNbr, LineAmount, YTD);

          if DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_DIRECT_DEPOSIT then
            CheckStub.TotalDirDep := CheckStub.TotalDirDep + LineAmount;
          if FShowShortfalls and (ShortfallAmount <> 0) then
            AddDeduction('(Shortfall ' + FloatToStrF(ShortfallAmount, ffFixed, 15, 2) + ')', Null, Null);
          LineAmount := 0;
        end
        else
          AddDeductionWithEmptyAmount;
        if FShowShortfalls and (ShortfallAmount <> 0) then
          AddDeduction('(' + DM_CLIENT.CL_E_DS['DESCRIPTION'] + ' Shortfall ' + FloatToStrF(ShortfallAmount, ffFixed, 15, 2) + ')', Null, Null);
        ShortfallAmount := 0;
      end
      else
        AddDeductionWithEmptyAmount;
    end;
    CL_E_DS_YTD.Next;
  end;
end;

procedure TDM_REGULAR_CHECK.AddEarning(Code, Description, Location,
  Rate: string; Hours, Current, YTD: Variant; PunchIn, PunchOut: string;
  CLEDsNbr, aType: integer; SumYTD: boolean = True);
begin
  CheckStub.AddEarning(Code, Description, Location, Rate, Hours, Current, YTD, PunchIn, PunchOut, SumYTD);
  if FLiteDatasets then
  begin
    cdsEarningsLite.Insert;
    cdsEarningsLite.FieldByName('CheckNbr').Value := FPrCheckNbr;
    cdsEarningsLite.FieldByName('Amount').Value := Current;
    cdsEarningsLite.FieldByName('YTD').Value := YTD;
    cdsEarningsLite.FieldByName('Rate').Value := Rate;
    cdsEarningsLite.FieldByName('Hours').Value := Hours;
    cdsEarningsLite.FieldByName('Location').Value := Location;
    cdsEarningsLite.FieldByName('ClEDsNbr').Value := ClEDsNbr;
    cdsEarningsLite.FieldByName('EType').Value := aType; //(1 - earning, 2 -memo, 3-shiftdiff)
    cdsEarningsLite.Post;
  end;
end;

procedure TDM_REGULAR_CHECK.AddEarnings(aPrCheckLines: TevClientDataSet);
var
  LineAmount, LineAmountWoShiftDiff, LineHours, OldRate, OTRate, YTD, FedMinimumWage, StateMinimumWage: Double;
  OldEDNbr, OldDivNbr, OldBrchNbr, OldDeptNbr, OldTeamNbr, OldJobNbr, OldShiftNbr: Integer;
  EDCode: String;
  PrintedEarningsList: TStringList;
  OldPunchInDate, OldPunchOutDate, PunchInDate, PunchOutDate: TDateTime;
  MemoPrinted: Boolean;
  Memo: String;

  function ConstructLocation: string;
  begin
    if FShowLaborDistribution = AUTO_LABOR_DIST_DO_NOT_SUPPRESS then // Show Localtion or Job.
    begin
      if (OldJobNbr <> 0) then
      begin
        DM_EMPLOYEE.CO_JOBS.Activate;
        Assert(DM_EMPLOYEE.CO_JOBS.FindKey([OldJobNbr]), Format(sAssertString, ['OldJobNbr', 'CO_JOBS.CO_JOBS_NBR', IntToStr(OldJobNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
        Result := Copy(DM_EMPLOYEE.CO_JOBS.DESCRIPTION.AsString, 1, 25);
      end
      else
      begin
        DM_EMPLOYEE.CO_DIVISION.Activate;
        DM_EMPLOYEE.CO_BRANCH.Activate;
        DM_EMPLOYEE.CO_DEPARTMENT.Activate;
        DM_EMPLOYEE.CO_TEAM.Activate;
        if DM_EMPLOYEE.CO_DIVISION.FindKey([OldDivNbr]) then
          Result := copy(Trim(DM_EMPLOYEE.CO_DIVISION.CUSTOM_DIVISION_NUMBER.AsString),1,6);
        if DM_EMPLOYEE.CO_BRANCH.FindKey([OldBrchNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_BRANCH.CUSTOM_BRANCH_NUMBER.AsString),1,6);
        if DM_EMPLOYEE.CO_DEPARTMENT.FindKey([OldDeptNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_DEPARTMENT.CUSTOM_DEPARTMENT_NUMBER.AsString),1,6);
        if DM_EMPLOYEE.CO_TEAM.FindKey([OldTeamNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_TEAM.CUSTOM_TEAM_NUMBER.AsString),1,6);
      end;
    end
    else
    if FShowLaborDistribution = AUTO_LABOR_DIST_DO_NOT_SUPPRESS_PLUS then // Show Location and Job
    begin
      DM_EMPLOYEE.CO_DIVISION.Activate;
      DM_EMPLOYEE.CO_BRANCH.Activate;
      DM_EMPLOYEE.CO_DEPARTMENT.Activate;
      DM_EMPLOYEE.CO_TEAM.Activate;
      DM_EMPLOYEE.CO_JOBS.Activate;
      if (OldJobNbr <> 0) then
      begin
        if DM_EMPLOYEE.CO_DIVISION.FindKey([OldDivNbr]) then
          Result := copy(Trim(DM_EMPLOYEE.CO_DIVISION.CUSTOM_DIVISION_NUMBER.AsString),1,5);
        if DM_EMPLOYEE.CO_BRANCH.FindKey([OldBrchNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_BRANCH.CUSTOM_BRANCH_NUMBER.AsString),1,5);
        if DM_EMPLOYEE.CO_DEPARTMENT.FindKey([OldDeptNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_DEPARTMENT.CUSTOM_DEPARTMENT_NUMBER.AsString),1,5);
        if DM_EMPLOYEE.CO_TEAM.FindKey([OldTeamNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_TEAM.CUSTOM_TEAM_NUMBER.AsString),1,5);
        Assert(DM_EMPLOYEE.CO_JOBS.FindKey([OldJobNbr]), Format(sAssertString, ['OldJobNbr', 'CO_JOBS.CO_JOBS_NBR', IntToStr(OldJobNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
        if Trim(Result) <> '' then
          Result := Copy(DM_EMPLOYEE.CO_JOBS.DESCRIPTION.AsString, 1, 5) + '/' + Result
        else
          Result := Copy(DM_EMPLOYEE.CO_JOBS.DESCRIPTION.AsString, 1, 25);
      end
      else
      begin
        if DM_EMPLOYEE.CO_DIVISION.FindKey([OldDivNbr]) then
          Result := copy(Trim(DM_EMPLOYEE.CO_DIVISION.CUSTOM_DIVISION_NUMBER.AsString),1,6);
        if DM_EMPLOYEE.CO_BRANCH.FindKey([OldBrchNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_BRANCH.CUSTOM_BRANCH_NUMBER.AsString),1,6);
        if DM_EMPLOYEE.CO_DEPARTMENT.FindKey([OldDeptNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_DEPARTMENT.CUSTOM_DEPARTMENT_NUMBER.AsString),1,6);
        if DM_EMPLOYEE.CO_TEAM.FindKey([OldTeamNbr]) then
          Result := Result + '/' + copy(Trim(DM_EMPLOYEE.CO_TEAM.CUSTOM_TEAM_NUMBER.AsString),1,6);
      end;
    end
    else
      Result := '';  // Suppress
  end;

  procedure AddEarningToCheckStub(IsMemo: Boolean = False; ShiftDescription: String = '');
  var
    TempDescr, RateStr: string;
    lYTD: Variant;
    aType: integer;
  begin
    aType := 1;
    if ShiftDescription <> '' then
      aType := 3
    else if IsMemo then
      aType := 2;

    lYTD := Null;
    RateStr := '';
    if FShowYTD then
    begin
      if DM_CLIENT.CL_E_DS['SHOW_YTD_ON_CHECKS'] = 'Y' then
      begin
        if PrintedEarningsList.IndexOfName(IntToStr(OldEDNbr)) = -1 then
        begin
          lYTD := ConvertNull(CL_E_DS_YTD.FieldByName('YTD').Value, 0);
          PrintedEarningsList.Add(IntToStr(OldEDNbr) + '=' + FloatToStr(lYTD));
          if aType > 1 then
            CheckStub.YTDEarnings := CheckStub.YTDEarnings - lYTD;
        end
        else
          lYTD := 0;
      end;
    end;

    TempDescr := '';
    if ShiftDescription <> '' then
      TempDescr := ShiftDescription
    else if FShowLaborDistribution <> AUTO_LABOR_DIST_SUPPRESS then
      TempDescr := ConstructLocation;

    if not IsMemo then
    begin
      if FShowRates then
      begin
        // rate need to be calculated for Waitstaff Overtime, reso ticket #50671
        if (DM_CLIENT.CL_E_DS.FieldByName('E_D_CODE_TYPE').AsString = ED_OEARN_WAITSTAFF_OVERTIME) or
          (DM_CLIENT.CL_E_DS.FieldByName('E_D_CODE_TYPE').AsString = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) then
        begin
          if (LineHours <> 0) and (LineAmountWoShiftDiff <> 0) then
            RateStr := FloatToStrF(RoundAll(LineAmountWoShiftDiff/LineHours, 2), ffFixed, 15, 2)
          else
            RateStr := FloatToStrF(0, ffFixed, 15, 2);
        end
        else begin
          if DM_CLIENT.CL_E_DS['FILLER'] = 'Y' then
            RateStr := FloatToStrF(RoundAll(OldRate, 4), ffFixed, 15, 4)
          else
            RateStr := FloatToStrF(RoundAll(OldRate, 2), ffFixed, 15, 2);
        end;
      end;
    end;

    AddEarning(DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER'], DM_CLIENT.CL_E_DS['DESCRIPTION'], TempDescr,
      RateStr, LineHours, LineAmount, lYTD, FormatDateTime('mm/dd/yyyy hh:nn:ss', PunchInDate),
      FormatDateTime('mm/dd/yyyy hh:nn:ss', PunchOutDate), DM_CLIENT.CL_E_DS['CL_E_Ds_NBR'], aType);
    if aType > 1 then
    begin
      CheckStub.TotalHours := CheckStub.TotalHours - LineHours;
      CheckStub.TotalEarnings := CheckStub.TotalEarnings - LineAmount;
    end;
  end;

begin
  PrintedEarningsList := TStringList.Create;
  with aPrCheckLines do
  try
  // Earnings
    LineHours := 0;
    LineAmount := 0;
    LineAmountWoShiftDiff := 0;
    OldRate := 0;
    OldEDNbr := 0;
    OldDivNbr := 0;
    OldBrchNbr := 0;
    OldDeptNbr := 0;
    OldTeamNbr := 0;
    OldJobNbr := 0;
    OldPunchInDate := 0;
    OldPunchOutDate := 0;

    DM_SYSTEM_STATE.SY_STATES.Activate;
    if DM_SYSTEM_STATE.SY_STATES.IndexFieldNames <> 'STATE' then
      DM_SYSTEM_STATE.SY_STATES.IndexFieldNames := 'STATE';
    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
    while not CL_E_DS_YTD.EOF and (wwcsChecks.FieldByName('EE_NBR').Value = CL_E_DS_YTD.FieldByName('EE_NBR').Value) do
    begin
      Assert(DM_CLIENT.CL_E_DS.FindKey([CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]), Format(sAssertString, ['CL_E_DS_YTD.CL_E_DS_NBR', 'CL_E_DS.CL_E_DS_NBR', CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      EDCode := DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'];
      if EDCode[1] = 'E' then
      begin
        if FindKey([wwcsChecks.FieldByName('PR_CHECK_NBR').Value, CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]) then
        begin
          OTRate := 1;
          if EDCode <> ED_OEARN_WAITSTAFF_OVERTIME then
            OTRate := ConvertNull(DM_CLIENT.CL_E_DS['REGULAR_OT_RATE'], 1);
          while (FieldByName('CL_E_DS_NBR').Value = CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value)
          and (FieldByName('PR_CHECK_NBR').Value = wwcsChecks.FieldByName('PR_CHECK_NBR').Value) and (not EOF) do
          begin
            if (EDCode = ED_OEARN_WAITSTAFF_OVERTIME) or (EDCode = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) then
            begin
              Assert(DM_EMPLOYEE.EE_STATES.FindKey([FieldByName('EE_STATES_NBR').Value]), Format(sAssertString, ['PR_CHECK_LINES.EE_STATES_NBR', 'EE_STATES.EE_STATES_NBR', FieldByName('EE_STATES_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              Assert(DM_COMPANY.CO_STATES.FindKey([DM_EMPLOYEE.EE_STATES['CO_STATES_NBR']]), Format(sAssertString, ['EE_STATES.CO_STATES_NBR', 'CO_STATES.CO_STATES_NBR', DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              Assert(DM_SYSTEM_STATE.SY_STATES.FindKey([DM_COMPANY.CO_STATES['STATE']]), Format(sAssertString, ['CO_STATES.STATE', 'SY_STATES.STATE', DM_COMPANY.CO_STATES.FieldByName('STATE').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              if (EDCode = ED_OEARN_WAITSTAFF_OVERTIME) or (EDCode = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME) then
              begin
                FedMinimumWage := ConvertNull(DM_EMPLOYEE.EE.FieldByName('OVERRIDE_FEDERAL_MINIMUM_WAGE').Value, 0);
                StateMinimumWage := ConvertNull(DM_EMPLOYEE.EE_STATES.FieldByName('OVERRIDE_STATE_MINIMUM_WAGE').Value, 0);
                if StateMinimumWage = 0 then
                  StateMinimumWage := FedMinimumWage;
                if FedMinimumWage = 0 then
                  FedMinimumWage := ConvertNull(DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('FEDERAL_MINIMUM_WAGE').Value, 0);

// plymouth todo    if StateMinimumWage = 0 then
//                  StateMinimumWage := ConvertNull(DM_SYSTEM_STATE.SY_STATES['NEXT_STATE_MINIMUM_WAGE'], 0);
//                if (EDCode = ED_OEARN_WAITSTAFF_OVERTIME) or ((EDCode = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME)
//                and (FieldByName('RATE_OF_PAY').Value < FedMinimumWage) and (FieldByName('RATE_OF_PAY').Value < DM_SYSTEM_STATE.SY_STATES['NEXT_STATE_MINIMUM_WAGE'])) then
                if (EDCode = ED_OEARN_WAITSTAFF_OVERTIME) or ((EDCode = ED_OEARN_WAITSTAFF_OR_REGULAR_OVERTIME)
                and (FieldByName('RATE_OF_PAY').Value < FedMinimumWage) ) then
                if StateMinimumWage > FedMinimumWage then
                  OTRate := 0.5 * StateMinimumWage / FieldByName('RATE_OF_PAY').Value + 1
                else
                  OTRate := 0.5 * FedMinimumWage / FieldByName('RATE_OF_PAY').Value + 1;
              end;
            end;

            if (FieldByName('CL_E_DS_NBR').Value <> OldEDNbr) or (ConvertNull(FieldByName('RATE_OF_PAY').Value, 0) * OTRate <> OldRate)
            or ((FShowLaborDistribution <> AUTO_LABOR_DIST_SUPPRESS) and ((ConvertNull(FieldByName('CO_DIVISION_NBR').Value, 0) <> OldDivNbr)
            or (ConvertNull(FieldByName('CO_BRANCH_NBR').Value, 0) <> OldBrchNbr)
            or (ConvertNull(FieldByName('CO_DEPARTMENT_NBR').Value, 0) <> OldDeptNbr)
            or (ConvertNull(FieldByName('CO_TEAM_NBR').Value, 0) <> OldTeamNbr)
            or (ConvertNull(FieldByName('CO_JOBS_NBR').Value, 0) <> OldJobNbr)
            or (ConvertNull(FieldByName('PUNCH_IN').Value, 0) <> OldPunchInDate)
            or (ConvertNull(FieldByName('PUNCH_OUT').Value, 0) <> OldPunchOutDate))) then
            begin
              if (LineAmount <> 0) or (LineHours <> 0) then
              begin
                Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
                if DM_CLIENT.CL_E_DS['SHOW_ED_ON_CHECKS'] = 'Y' then
                  AddEarningToCheckStub;
                LineAmount := 0;
                LineAmountWoShiftDiff := 0;
                PunchInDate := 0;
                PunchOutDate := 0;
              end;
              OldEDNbr := FieldByName('CL_E_DS_NBR').Value;
              OldRate := ConvertNull(FieldByName('RATE_OF_PAY').Value, 0) * OTRate;
              OldDivNbr := ConvertNull(FieldByName('CO_DIVISION_NBR').Value, 0);
              OldBrchNbr := ConvertNull(FieldByName('CO_BRANCH_NBR').Value, 0);
              OldDeptNbr := ConvertNull(FieldByName('CO_DEPARTMENT_NBR').Value, 0);
              OldTeamNbr := ConvertNull(FieldByName('CO_TEAM_NBR').Value, 0);
              OldJobNbr := ConvertNull(FieldByName('CO_JOBS_NBR').Value, 0);
              OldPunchInDate := ConvertNull(FieldByName('PUNCH_IN').Value, 0);
              OldPunchOutDate := ConvertNull(FieldByName('PUNCH_OUT').Value, 0);
              LineHours := 0;
            end;
            if ((DM_CLIENT.CL_E_DS['UPDATE_HOURS'] = SHOW_ED_HOURS_ON_BOTH) or (DM_CLIENT.CL_E_DS['UPDATE_HOURS'] = SHOW_ED_HOURS_ON_CHECK)) then
              LineHours := LineHours + ConvertNull(FieldByName('HOURS_OR_PIECES').Value, 0);
            LineAmount := LineAmount + ConvertNull(FieldByName('AMOUNT').Value, 0);
            LineAmountWoShiftDiff := LineAmountWoShiftDiff + ConvertNull(FieldByName('AMOUNT').Value, 0) - ConvertNull(FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value, 0);
            PunchInDate := ConvertNull(FieldByName('PUNCH_IN').Value, 0);
            PunchOutDate := ConvertNull(FieldByName('PUNCH_OUT').Value, 0);
            Next;
          end;
          if (LineAmount <> 0) or (LineHours <> 0) then
          begin
            Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
            if DM_CLIENT.CL_E_DS['SHOW_ED_ON_CHECKS'] = 'Y' then
              AddEarningToCheckStub;
            LineAmount := 0;
            LineAmountWoShiftDiff := 0;
            LineHours := 0;
            PunchInDate := 0;
            PunchOutDate := 0;
          end;
        end
        else
        begin
          if FShowYTD then
          begin
            if DM_CLIENT.CL_E_DS['SHOW_YTD_ON_CHECKS'] = 'Y' then
            begin
              YTD := ConvertNull(CL_E_DS_YTD.FieldByName('YTD').Value, 0);
              if YTD <> 0 then
                AddEarning(DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER'], DM_CLIENT.CL_E_DS['DESCRIPTION'], '', '', null, null, YTD, '', '',
                DM_CLIENT.CL_E_DS['CL_E_Ds_NBR'], 1);
            end;
          end;
        end;
      end;
      CL_E_DS_YTD.Next;
    end;

  // Memos
    LineHours := 0;
    LineAmount := 0;
    LineAmountWoShiftDiff := 0;
    OldEDNbr := 0;
    MemoPrinted := False;
    CL_E_DS_YTD.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]);
    while not CL_E_DS_YTD.EOF and (wwcsChecks.FieldByName('EE_NBR').Value = CL_E_DS_YTD.FieldByName('EE_NBR').Value) do
    begin
      Assert(DM_CLIENT.CL_E_DS.FindKey([CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]), Format(sAssertString, ['CL_E_DS_YTD.CL_E_DS_NBR', 'CL_E_DS.CL_E_DS_NBR', CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      EDCode := DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'];
      if EDCode[1] = 'M' then
      begin
        if FindKey([wwcsChecks.FieldByName('PR_CHECK_NBR').Value, CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]) then
        begin
          while (FieldByName('CL_E_DS_NBR').Value = CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value) and
                (FieldByName('PR_CHECK_NBR').Value = wwcsChecks.FieldByName('PR_CHECK_NBR').Value) and
                (not EOF) do
          begin
            if (FieldByName('CL_E_DS_NBR').Value <> OldEDNbr)
            or ((FShowLaborDistribution <> AUTO_LABOR_DIST_SUPPRESS) and ((ConvertNull(FieldByName('CO_DIVISION_NBR').Value, 0) <> OldDivNbr)
            or (ConvertNull(FieldByName('CO_BRANCH_NBR').Value, 0) <> OldBrchNbr)
            or (ConvertNull(FieldByName('CO_DEPARTMENT_NBR').Value, 0) <> OldDeptNbr)
            or (ConvertNull(FieldByName('CO_TEAM_NBR').Value, 0) <> OldTeamNbr)
            or (ConvertNull(FieldByName('CO_JOBS_NBR').Value, 0) <> OldJobNbr))) then
            begin
              if ((LineAmount <> 0) or (LineHours <> 0)) and
                 DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]) and
                 (DM_CLIENT.CL_E_DS['SHOW_ED_ON_CHECKS'] = 'Y') then
              begin
                if not MemoPrinted then
                begin
                  CheckStub.AddEarning('', 'MEMOS', '', '', Null, Null, Null, '','');
                  MemoPrinted := True;
                end;
                AddEarningToCheckStub(True);
              end;
              OldEDNbr := FieldByName('CL_E_DS_NBR').Value;
              OldDivNbr := ConvertNull(FieldByName('CO_DIVISION_NBR').Value, 0);
              OldBrchNbr := ConvertNull(FieldByName('CO_BRANCH_NBR').Value, 0);
              OldDeptNbr := ConvertNull(FieldByName('CO_DEPARTMENT_NBR').Value, 0);
              OldTeamNbr := ConvertNull(FieldByName('CO_TEAM_NBR').Value, 0);
              OldJobNbr := ConvertNull(FieldByName('CO_JOBS_NBR').Value, 0);
              LineHours := 0;
              LineAmount := 0;
            end;
            if ((DM_CLIENT.CL_E_DS['UPDATE_HOURS'] = SHOW_ED_HOURS_ON_BOTH) or (DM_CLIENT.CL_E_DS['UPDATE_HOURS'] = SHOW_ED_HOURS_ON_CHECK)) then
              LineHours := LineHours + ConvertNull(FieldByName('HOURS_OR_PIECES').Value, 0);
            LineAmount := LineAmount + ConvertNull(FieldByName('AMOUNT').Value, 0);
            Next;
          end;
          if (LineAmount <> 0) or (LineHours <> 0) then
          begin
            Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
            if DM_CLIENT.CL_E_DS['SHOW_ED_ON_CHECKS'] = 'Y' then
            begin
              if not MemoPrinted then
              begin
                CheckStub.AddEarning('', 'MEMOS', '', '', Null, Null, Null, '', '');
                MemoPrinted := True;
              end;
              AddEarningToCheckStub(True);
            end;
            LineAmount := 0;
            LineHours := 0;
          end;
        end
        else
        begin
          if FShowYTD then
          begin
            OldEDNbr := CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value;
            if OldEDNbr <> 0 then
              Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
            if DM_CLIENT.CL_E_DS['SHOW_YTD_ON_CHECKS'] = 'Y' then
            begin
              YTD := ConvertNull(CL_E_DS_YTD.FieldByName('YTD').Value, 0);
              if YTD <> 0 then
              begin
                if not MemoPrinted then
                begin
                  CheckStub.AddEarning('', 'MEMOS', '', '', Null, Null, Null, '', '');
                  MemoPrinted := True;
                end;
                AddEarning(DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER'], DM_CLIENT.CL_E_DS['DESCRIPTION'], '', '', Null, Null, YTD, FormatDateTime('mm/dd/yyyy hh:nn:ss', PunchInDate), FormatDateTime('mm/dd/yyyy hh:nn:ss', PunchOutDate), DM_CLIENT.CL_E_DS['CL_E_DS_NBR'], 2, False)
              end;
            end;
          end;
        end;
      end;
      CL_E_DS_YTD.Next;
    end;

  // Shift Differentials
    if DM_COMPANY.CO.FieldByName('SHOW_SHIFTS_ON_CHECK').Value = 'Y' then
    begin
      DM_COMPANY.CO_SHIFTS.DataRequired('ALL');
      DM_EMPLOYEE.EE_WORK_SHIFTS.DataRequired('ALL');
      if DM_EMPLOYEE.EE_WORK_SHIFTS.IndexFieldNames <> 'EE_NBR;CO_SHIFTS_NBR' then
        DM_EMPLOYEE.EE_WORK_SHIFTS.IndexFieldNames := 'EE_NBR;CO_SHIFTS_NBR';
      if IndexFieldNames <> 'PR_CHECK_NBR;CL_E_DS_NBR;CO_SHIFTS_NBR' then
        IndexFieldNames := 'PR_CHECK_NBR;CL_E_DS_NBR;CO_SHIFTS_NBR';
      LineHours := 0;
      LineAmount := 0;
      LineAmountWoShiftDiff := 0;
      OldEDNbr := 0;
      OldShiftNbr := 0;
      FindKey([wwcsChecks.FieldByName('PR_CHECK_NBR').Value]);
      while not EOF and (wwcsChecks.FieldByName('PR_CHECK_NBR').Value = FieldByName('PR_CHECK_NBR').Value) do
      begin
        if ConvertNull(FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value, 0) <> 0 then
        begin
          if (FieldByName('CL_E_DS_NBR').Value <> OldEDNbr) or (FieldByName('CO_SHIFTS_NBR').Value <> OldShiftNbr) then
          begin
            if OldEDNbr = 0 then
            begin
              if FCalculateRates then
                Memo := 'Memo: Included in Rates'
              else
                Memo := 'Memo: Included in Current';
              CheckStub.AddEarning('', 'SHIFT DIFF.', Memo, '', Null, Null, Null, '', '');
            end;
            if (LineAmount <> 0) or (LineHours <> 0) then
            begin
              Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              AddEarningToCheckStub(False, DM_COMPANY.CO_SHIFTS.Lookup('CO_SHIFTS_NBR', OldShiftNbr, 'NAME'));
            end;
            OldEDNbr := FieldByName('CL_E_DS_NBR').Value;
            OldShiftNbr := FieldByName('CO_SHIFTS_NBR').Value;
            Assert(DM_EMPLOYEE.EE_WORK_SHIFTS.FindKey([wwcsChecks.FieldByName('EE_NBR').Value, FieldByName('CO_SHIFTS_NBR').Value]), Format(sAssertString, ['wwcsChecks.EE_NBR;PR_CHECK_LINES.CO_SHIFTS_NBR', 'EE_WORK_SHIFTS.EE_NBR;CO_SHIFTS_NBR', wwcsChecks.FieldByName('EE_NBR').AsString + ';' + FieldByName('CO_SHIFTS_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
            OldRate := ConvertNull(DM_EMPLOYEE.EE_WORK_SHIFTS['SHIFT_PERCENTAGE'], 0);
            if OldRate = 0 then
            begin
  //                  Assert(DM_CLIENT.CL_E_DS.FindKey([CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').Value]), Format(sAssertString, ['CL_E_DS_YTD.CL_E_DS_NBR', 'CL_E_DS.CL_E_DS_NBR', CL_E_DS_YTD.FieldByName('CL_E_DS_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
              if DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_OEARN_OVERTIME then
                OldRate := DM_EMPLOYEE.EE_WORK_SHIFTS['SHIFT_RATE'] * DM_CLIENT.CL_E_DS['REGULAR_OT_RATE']
              else
                OldRate := DM_EMPLOYEE.EE_WORK_SHIFTS['SHIFT_RATE'];
            end;
            LineAmount := 0;
            LineHours := 0;
          end;
          if ((DM_CLIENT.CL_E_DS['UPDATE_HOURS'] = SHOW_ED_HOURS_ON_BOTH) or (DM_CLIENT.CL_E_DS['UPDATE_HOURS'] = SHOW_ED_HOURS_ON_CHECK)) then
            LineHours := LineHours + ConvertNull(FieldByName('HOURS_OR_PIECES').Value, 0);
          LineAmount := LineAmount + ConvertNull(FieldByName('E_D_DIFFERENTIAL_AMOUNT').Value, 0);
          Next;
        end
        else
          Next;
      end;

      if (LineAmount <> 0) or (LineHours <> 0) then
      begin
        if OldEDNbr = 0 then
          CheckStub.AddEarning('', 'SHIFT DIFF.', '', '', Null, Null, Null, '','');
        Assert(DM_CLIENT.CL_E_DS.FindKey([OldEDNbr]), Format(sAssertString, ['OldEDNbr', 'CL_E_DS.CL_E_DS_NBR', IntToStr(OldEDNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
        AddEarningToCheckStub(False, DM_COMPANY.CO_SHIFTS.Lookup('CO_SHIFTS_NBR', OldShiftNbr, 'NAME'));
        LineAmount := 0;
        LineHours := 0;
      end;
    end;
  finally
    PrintedEarningsList.Free;
  end;
end;

procedure TDM_REGULAR_CHECK.AddFederalTaxes;
var
  TempStr: string;
begin
  FShowEdYtd := True;
  TempStr := '';
  if FEDERAL_YTD.IndexFieldNames <> 'EE_NBR' then
    FEDERAL_YTD.IndexFieldNames := 'EE_NBR';

  if not FEDERAL_YTD.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]) then
  begin
    FEDERAL_YTD.Filter := 'EE_NBR=0';
    FEDERAL_YTD.Filtered := True;
  end;

  // Federal Taxes
  if ConvertNull(DM_PAYROLL.PR_CHECK['FEDERAL_TAX'], 0) <> 0 then
  begin
    if DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] <> 0 then
    begin
      if DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] = OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT then
        TempStr := FloatToStrF(DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] * (-1), ffFixed, 15, 2);
      if DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] = OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT then
        TempStr := FloatToStrF(DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] * (-1), ffFixed, 15, 2) + '%';
    end;
    PrintTaxesDeductions('Fed (' + wwcsChecks.FieldByName('FEDERAL_MARITAL_STATUS').AsString
      + '/' + wwcsChecks.FieldByName('NUMBER_OF_DEPENDENTS').AsString + ') ' + TempStr + ' ('
      + FloatToStrF( ShowWage( FEDERAL_YTD.FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')'
      , DM_PAYROLL.PR_CHECK['FEDERAL_TAX'], FEDERAL_YTD.FieldByName('FEDERAL_TAX').AsFloat);
  end
  else
    AddDeduction('Fed (' + wwcsChecks.FieldByName('FEDERAL_MARITAL_STATUS').AsString
      + '/' + wwcsChecks.FieldByName('NUMBER_OF_DEPENDENTS').AsString + ') ('
      + FloatToStrF(ShowWage(FEDERAL_YTD.FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')',
      0, FEDERAL_YTD.FieldByName('FEDERAL_TAX').AsFloat);

  if FShowShortfalls and (DM_PAYROLL.PR_CHECK.FEDERAL_SHORTFALL.Value <> 0) then
    AddDeduction('(Federal Shortfall ' + FloatToStrF(DM_PAYROLL.PR_CHECK.FEDERAL_SHORTFALL.Value, ffFixed, 15, 2) + ')', Null, Null);

// OASDI Taxes
  if ConvertNull(DM_PAYROLL.PR_CHECK['EE_OASDI_TAX'], 0) <> 0 then
    PrintTaxesDeductions('OASDI (' + FloatToStrF( ShowWage(FEDERAL_YTD.FieldByName('EE_OASDI_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').AsFloat +
      DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').AsFloat), ffFixed, 15, 2)
      + ')', DM_PAYROLL.PR_CHECK['EE_OASDI_TAX'], FEDERAL_YTD.FieldByName('EE_OASDI_TAX').AsFloat)
  else if FShowYTD and (FEDERAL_YTD.FieldByName('EE_OASDI_TAX').AsFloat <> 0) then
    AddDeduction('OASDI (' + FloatToStrF( ShowWage(FEDERAL_YTD.FieldByName('EE_OASDI_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').AsFloat +
      DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').AsFloat), ffFixed, 15, 2)
      + ')', Null, FEDERAL_YTD.FieldByName('EE_OASDI_TAX').AsFloat);

// Medicare Taxes
  if ConvertNull(DM_PAYROLL.PR_CHECK['EE_MEDICARE_TAX'], 0) <> 0 then
    PrintTaxesDeductions('Medicare (' + FloatToStrF( ShowWage(FEDERAL_YTD.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2)
      + ')', DM_PAYROLL.PR_CHECK['EE_MEDICARE_TAX'], FEDERAL_YTD.FieldByName('EE_MEDICARE_TAX').AsFloat)
  else if FShowYTD and (FEDERAL_YTD.FieldByName('EE_MEDICARE_TAX').AsFloat <> 0) then
    AddDeduction('Medicare (' + FloatToStrF( ShowWage(FEDERAL_YTD.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2)
      + ')', Null, FEDERAL_YTD.FieldByName('EE_MEDICARE_TAX').AsFloat);

// EIC Taxes
  if ConvertNull(DM_PAYROLL.PR_CHECK['EE_EIC_TAX'], 0) <> 0 then
    PrintTaxesDeductions('EIC', DM_PAYROLL.PR_CHECK['EE_EIC_TAX'] * (-1), FEDERAL_YTD.FieldByName('EE_EIC_TAX').AsFloat)
  else if FShowYTD and (FEDERAL_YTD.FieldByName('EE_EIC_TAX').AsFloat <> 0) then
    AddDeduction('EIC', Null, FEDERAL_YTD.FieldByName('EE_EIC_TAX').AsFloat);

// Backup Withholding Taxes
  if ConvertNull(DM_PAYROLL.PR_CHECK['OR_CHECK_BACK_UP_WITHHOLDING'], 0) <> 0 then
    PrintTaxesDeductions('Backup W/H', DM_PAYROLL.PR_CHECK['OR_CHECK_BACK_UP_WITHHOLDING'], FEDERAL_YTD.FieldByName('BACK_UP_WITHHOLDING').AsFloat)
  else if FShowYTD and (FEDERAL_YTD.FieldByName('BACK_UP_WITHHOLDING').AsFloat <> 0) then
    AddDeduction('Backup W/H', Null, FEDERAL_YTD.FieldByName('BACK_UP_WITHHOLDING').AsFloat);

  if FEDERAL_YTD.Filtered then
  begin
    FEDERAL_YTD.Filter := '';
    FEDERAL_YTD.Filtered := False;
  end;
end;

procedure TDM_REGULAR_CHECK.AddLocalTaxes;
var
  TaxWage, Tax: Double;
begin
  FShowEdYtd := True;
  if DM_COMPANY.CO_LOCAL_TAX.IndexFieldNames <> 'CO_LOCAL_TAX_NBR' then
    DM_COMPANY.CO_LOCAL_TAX.IndexFieldNames := 'CO_LOCAL_TAX_NBR';
  if DM_PAYROLL.PR_CHECK_LOCALS.IndexFieldNames <> 'PR_CHECK_NBR;EE_LOCALS_NBR' then
    DM_PAYROLL.PR_CHECK_LOCALS.IndexFieldNames := 'PR_CHECK_NBR;EE_LOCALS_NBR';

  ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_LOCALS, wwcsChecks.FieldByName('CO_NBR').AsInteger);
  if DM_EMPLOYEE.EE_LOCALS.IndexFieldNames <> 'EE_NBR' then
    DM_EMPLOYEE.EE_LOCALS.IndexFieldNames := 'EE_NBR';
  DM_EMPLOYEE.EE_LOCALS.FindKey([DM_PAYROLL.PR_CHECK['EE_NBR']]);

  DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
  if DM_SYSTEM_LOCAL.SY_LOCALS.IndexFieldNames <> 'SY_LOCALS_NBR' then
    DM_SYSTEM_LOCAL.SY_LOCALS.IndexFieldNames := 'SY_LOCALS_NBR';

  if LOCAL_YTD.IndexFieldNames <> 'EE_LOCALS_NBR' then
    LOCAL_YTD.IndexFieldNames := 'EE_LOCALS_NBR';

  while not DM_EMPLOYEE.EE_LOCALS.EOF and (DM_EMPLOYEE.EE_LOCALS['EE_NBR'] = DM_PAYROLL.PR_CHECK['EE_NBR']) do
  begin
    if LOCAL_YTD.FindKey([DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').Value]) then
    begin
      Assert(DM_COMPANY.CO_LOCAL_TAX.FindKey([DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR']]), Format(sAssertString, ['EE_LOCALS.CO_LOCAL_TAX_NBR', 'CO_LOCAL_TAX.CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      Assert(DM_SYSTEM_LOCAL.SY_LOCALS.FindKey([DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR']]), Format(sAssertString, ['CO_LOCAL_TAX.SY_LOCALS_NBR', 'SY_LOCALS.SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_SYSTEM_LOCAL.SY_LOCALS['TAX_TYPE'] = GROUP_BOX_EE then
      begin
        if DM_PAYROLL.PR_CHECK_LOCALS.FindKey([FPrCheckNbr, DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').Value]) then
        begin
          TaxWage := 0;
          Tax := 0;
          while not DM_PAYROLL.PR_CHECK_LOCALS.Eof do
          begin
            if (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger <> FPrCheckNbr)
            or (DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger <> DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger) then
              break;
            Tax     := Tax     + DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsFloat;
            TaxWage := TaxWage + DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').AsFloat;
            DM_PAYROLL.PR_CHECK_LOCALS.Next;
          end;

          if (Tax <> 0) then
            PrintTaxesDeductions(DM_SYSTEM_LOCAL.SY_LOCALS['NAME'] + '('
              + FloatToStrF(ShowWage(LOCAL_YTD.FieldByName('LOCAL_TAXABLE_WAGE').AsFloat, TaxWage), ffFixed, 15, 2) + ')'
              , Tax, LOCAL_YTD.FieldByName('LOCAL_TAX').AsFloat)
          else
          begin
            if FShowYTD and (LOCAL_YTD.FieldByName('LOCAL_TAX').AsFloat <> 0) then
              AddDeduction(DM_SYSTEM_LOCAL.SY_LOCALS['NAME'] + '('
              + FloatToStrF(ShowWage(LOCAL_YTD.FieldByName('LOCAL_TAXABLE_WAGE').AsFloat, TaxWage), ffFixed, 15, 2) + ')',
              Null, LOCAL_YTD.FieldByName('LOCAL_TAX').AsFloat);
          end;
          if FShowShortfalls and (DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_SHORTFALL.Value <> 0) then
            AddDeduction('(' + DM_SYSTEM_LOCAL.SY_LOCALS['NAME'] + ' Shortfall ' + FloatToStrF(DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_SHORTFALL.Value, ffFixed, 15, 2) + ')', Null, Null);
        end
        else if FShowYTD and (LOCAL_YTD.FieldByName('LOCAL_TAX').AsFloat <> 0) then
          AddDeduction(DM_SYSTEM_LOCAL.SY_LOCALS['NAME'] + '('
          + FloatToStrF(ShowWage(LOCAL_YTD.FieldByName('LOCAL_TAXABLE_WAGE').AsFloat, 0), ffFixed, 15, 2) + ')',
          Null, LOCAL_YTD.FieldByName('LOCAL_TAX').AsFloat);
      end;
    end;
    DM_EMPLOYEE.EE_LOCALS.Next;
  end;
end;

procedure TDM_REGULAR_CHECK.AddStateTaxes;
var
  s, TempStr: string;
begin
  FShowEdYtd := True;
  s := '';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Activate;
  if DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.IndexFieldNames <> 'SY_STATES_NBR;STATUS_TYPE' then
    DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.IndexFieldNames := 'SY_STATES_NBR;STATUS_TYPE';
  if DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames <> 'PR_CHECK_NBR; EE_STATES_NBR' then
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := 'PR_CHECK_NBR; EE_STATES_NBR';
  if DM_EMPLOYEE.EE_STATES.IndexFieldNames <> 'EE_NBR' then
    DM_EMPLOYEE.EE_STATES.IndexFieldNames := 'EE_NBR';

  DM_EMPLOYEE.EE_STATES.FindKey([DM_PAYROLL.PR_CHECK['EE_NBR']]);

  if STATE_YTD.IndexFieldNames <> 'EE_STATES_NBR' then
    STATE_YTD.IndexFieldNames := 'EE_STATES_NBR';

  while not DM_EMPLOYEE.EE_STATES.EOF and
        (DM_EMPLOYEE.EE_STATES['EE_NBR'] = DM_PAYROLL.PR_CHECK['EE_NBR']) do
  begin
    if STATE_YTD.FindKey([DM_EMPLOYEE.EE_STATES.FieldByName('EE_STATES_NBR').Value]) then
    begin
      Assert(DM_COMPANY.CO_STATES.FindKey([DM_EMPLOYEE.EE_STATES['CO_STATES_NBR']]), Format(sAssertString, ['EE_STATES.CO_STATES_NBR', 'CO_STATES.CO_STATES_NBR', DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

      if DM_PAYROLL.PR_CHECK_STATES.FindKey([FPrCheckNbr, DM_EMPLOYEE.EE_STATES.FieldByName('EE_STATES_NBR').Value]) then
      begin
        if ConvertNull(DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').Value, 0) <> 0 then
        begin
          TempStr := '';
          if DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsFloat <> 0 then
          begin
            if DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value = OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT then
              TempStr := FloatToStrF(DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value * (-1), ffFixed, 15, 2);
            if DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value = OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT then
              TempStr := FloatToStrF(DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value * (-1), ffFixed, 15, 2) + '%';
          end;
          PrintTaxesDeductions(DM_COMPANY.CO_STATES['STATE'] + ' ('
            + GetStateMaritalStatus
            + '/' + VarToStr(ConvertNull(DM_EMPLOYEE.EE_STATES.FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').Value, 0))
            + ') ' + TempStr + ' (' + FloatToStrF( ShowWage(STATE_YTD.FieldByName('STATE_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')'
            , DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').Value, STATE_YTD.FieldByName('STATE_TAX').AsFloat);
        end
        else
        begin
          if FShowYTD and (STATE_YTD.FieldByName('STATE_TAX').AsFloat <> 0) then
            AddDeduction(DM_COMPANY.CO_STATES['STATE'] + ' ('
              + GetStateMaritalStatus
              + '/' + VarToStr(ConvertNull(DM_EMPLOYEE.EE_STATES.FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').Value, 0))
              + ') (' + FloatToStrF(ShowWage(STATE_YTD.FieldByName('STATE_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')',
              Null, STATE_YTD.FieldByName('STATE_TAX').AsFloat);
        end;
        if FShowShortfalls and (DM_PAYROLL.PR_CHECK_STATES.STATE_SHORTFALL.Value <> 0) then
          AddDeduction('(' + DM_COMPANY.CO_STATES['STATE'] + ' State Shortfall ' + FloatToStrF(DM_PAYROLL.PR_CHECK_STATES.STATE_SHORTFALL.Value, ffFixed, 15, 2) + ')', Null, Null);

// SDI Taxes
        s := DM_COMPANY.CO_STATES['STATE'] + ' SDI ';
        if DM_COMPANY.CO_STATES['STATE'] = 'NM' then
          s := s + '(Workers Comp) ';
        if ConvertNull(DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value, 0) <> 0 then
        begin
          PrintTaxesDeductions(s + '('
            + FloatToStrF( ShowWage(STATE_YTD.FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')'
            , DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value, STATE_YTD.FieldByName('EE_SDI_TAX').AsFloat);
        end
        else
        begin
          if FShowYTD and (STATE_YTD.FieldByName('EE_SDI_TAX').AsFloat <> 0) then
            AddDeduction(s + '('
              + FloatToStrF( ShowWage(STATE_YTD.FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')',
              Null, STATE_YTD.FieldByName('EE_SDI_TAX').AsFloat);
        end;
        if FShowShortfalls and (DM_PAYROLL.PR_CHECK_STATES.EE_SDI_SHORTFALL.Value <> 0) then
          AddDeduction('(' + DM_COMPANY.CO_STATES['STATE'] + ' SDI Shortfall ' + FloatToStrF(DM_PAYROLL.PR_CHECK_STATES.EE_SDI_SHORTFALL.Value, ffFixed, 15, 2) + ')', Null, Null);
      end
      else
      begin
        s := DM_COMPANY.CO_STATES['STATE'] + ' SDI ';
        if DM_COMPANY.CO_STATES['STATE'] = 'NM' then
          s := s + '(Workers Comp) ';
        if FShowYTD and (STATE_YTD.FieldByName('STATE_TAX').AsFloat <> 0) then
          AddDeduction(DM_COMPANY.CO_STATES['STATE'] + ' ('
            + GetStateMaritalStatus
            + '/' + VarToStr(ConvertNull(DM_EMPLOYEE.EE_STATES.FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').Value, 0))
            + ') (' + FloatToStrF(ShowWage(STATE_YTD.FieldByName('STATE_TAXABLE_WAGES').AsFloat, 0), ffFixed, 15, 2) + ')',
            Null, STATE_YTD.FieldByName('STATE_TAX').AsFloat);
        if FShowYTD and (STATE_YTD.FieldByName('EE_SDI_TAX').AsFloat <> 0) then
          AddDeduction(s + '('
            + FloatToStrF(ShowWage(STATE_YTD.FieldByName('EE_SDI_TAXABLE_WAGES').AsFloat, 0), ffFixed, 15, 2) + ')',
            Null, STATE_YTD.FieldByName('EE_SDI_TAX').AsFloat);
      end;
    end;
    DM_EMPLOYEE.EE_STATES.Next;
  end;
end;

procedure TDM_REGULAR_CHECK.AddSuiTaxes;
begin
  FShowEdYtd := True;
  if DM_COMPANY.CO_SUI.IndexFieldNames <> 'CO_SUI_NBR' then
    DM_COMPANY.CO_SUI.IndexFieldNames := 'CO_SUI_NBR';

  if DM_PAYROLL.PR_CHECK_SUI.IndexFieldNames <> 'PR_CHECK_NBR;CO_SUI_NBR' then
    DM_PAYROLL.PR_CHECK_SUI.IndexFieldNames := 'PR_CHECK_NBR;CO_SUI_NBR';

  if SUI_YTD.IndexFieldNames <> 'EE_NBR;CO_SUI_NBR' then
    SUI_YTD.IndexFieldNames := 'EE_NBR;CO_SUI_NBR';
  SUI_YTD.FindKey([wwcsChecks['EE_NBR']]);

  DM_SYSTEM_STATE.SY_SUI.Activate;
  if DM_SYSTEM_STATE.SY_SUI.IndexFieldNames <> 'SY_SUI_NBR' then
    DM_SYSTEM_STATE.SY_SUI.IndexFieldNames := 'SY_SUI_NBR';

  if DM_SYSTEM_STATE.SY_STATES.IndexFieldNames <> 'SY_STATES_NBR' then
    DM_SYSTEM_STATE.SY_STATES.IndexFieldNames := 'SY_STATES_NBR';

  if DM_COMPANY.CO.SUMMARIZE_SUI.Value = GROUP_BOX_NO then
    while not SUI_YTD.EOF and (wwcsChecks['EE_NBR'] = SUI_YTD['EE_NBR']) do
    begin
      Assert(DM_COMPANY.CO_SUI.FindKey([SUI_YTD['CO_SUI_NBR']]), Format(sAssertString, ['SUI_YTD.CO_SUI_NBR', 'CO_SUI.CO_SUI_NBR', SUI_YTD.FieldByName('CO_SUI_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      Assert(DM_SYSTEM_STATE.SY_SUI.FindKey([DM_COMPANY.CO_SUI['SY_SUI_NBR']]), Format(sAssertString, ['SY_SUI.SY_SUI_NBR', 'CO_SUI.SY_SUI_NBR', DM_COMPANY.CO_SUI.FieldByName('SY_SUI_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

      if not Is_ER_SUI(DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER']) then
      begin
        if DM_PAYROLL.PR_CHECK_SUI.FindKey([FPrCheckNbr, SUI_YTD['CO_SUI_NBR']]) and
           (ConvertNull(DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').Value, 0) <> 0) then
        begin
          PrintTaxesDeductions(DM_SYSTEM_STATE.SY_SUI['SUI_TAX_NAME'] + '('
            + FloatToStrF( ShowWage(SUI_YTD.FieldByName('SUI_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')'
            , DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').Value, SUI_YTD.FieldByName('SUI_TAX').AsFloat);
        end
        else if FShowYTD and (SUI_YTD.FieldByName('SUI_TAX').AsFloat <> 0) then
          AddDeduction(DM_SYSTEM_STATE.SY_SUI['SUI_TAX_NAME'] + '('
          + FloatToStrF(ShowWage(SUI_YTD.FieldByName('SUI_TAXABLE_WAGES').AsFloat, DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').AsFloat), ffFixed, 15, 2) + ')',
          Null, SUI_YTD.FieldByName('SUI_TAX').AsFloat);
      end;
      SUI_YTD.Next;
    end
  else begin
    SUIConsolidation.FieldDefs.Clear;
    SUIConsolidation.CreateDataSet;
    try
      while not SUI_YTD.EOF and (wwcsChecks['EE_NBR'] = SUI_YTD['EE_NBR']) do
      begin
        Assert(DM_COMPANY.CO_SUI.FindKey([SUI_YTD['CO_SUI_NBR']]), Format(sAssertString, ['SUI_YTD.CO_SUI_NBR', 'CO_SUI.CO_SUI_NBR', SUI_YTD.FieldByName('CO_SUI_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
        Assert(DM_SYSTEM_STATE.SY_SUI.FindKey([DM_COMPANY.CO_SUI['SY_SUI_NBR']]), Format(sAssertString, ['SY_SUI.SY_SUI_NBR', 'CO_SUI.SY_SUI_NBR', DM_COMPANY.CO_SUI.FieldByName('SY_SUI_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
        Assert(DM_SYSTEM_STATE.SY_STATES.FindKey([DM_SYSTEM_STATE.SY_SUI.SY_STATES_NBR.Value]), Format(sAssertString, ['SY_STATES.SY_STATES_NBR', 'SY_SUI.SY_STATES_NBR', DM_SYSTEM_STATE.SY_SUI.SY_STATES_NBR.AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

        if not Is_ER_SUI(DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER']) then
        begin
          if SUIConsolidation.FindKey([DM_SYSTEM_STATE.SY_SUI.SY_STATES_NBR.Value]) then
            SUIConsolidation.Edit
          else
          begin
            SUIConsolidation.Append;
            SUIConsolidation['SY_STATES_NBR'] := DM_SYSTEM_STATE.SY_SUI.SY_STATES_NBR.Value;
            SUIConsolidation['STATE'] := DM_SYSTEM_STATE.SY_STATES.STATE.Value;
          end;
          if DM_PAYROLL.PR_CHECK_SUI.FindKey([FPrCheckNbr, SUI_YTD['CO_SUI_NBR']]) then
            SUIConsolidation['SUI_TAX'] := SUIConsolidation.FieldByName('SUI_TAX').AsCurrency + DM_PAYROLL.PR_CHECK_SUI.SUI_TAX.Value;
          SUIConsolidation['YTD_SUI_TAX'] := SUIConsolidation.FieldByName('YTD_SUI_TAX').AsCurrency + SUI_YTD.FieldByName('SUI_TAX').AsCurrency;
          if SUIConsolidation.FieldByName('SUI_TAXABLE_WAGES').IsNull or
             (DM_SYSTEM_STATE.SY_SUI.EE_ER_OR_EE_OTHER_OR_ER_OTHER.Value = GROUP_BOX_EE) then
            SUIConsolidation['SUI_TAXABLE_WAGES'] := SUI_YTD['SUI_TAXABLE_WAGES'];
          SUIConsolidation.Post;
        end;
        SUI_YTD.Next;
      end;

      SUIConsolidation.First;
      while not SUIConsolidation.Eof do
      begin
        if SUIConsolidation.FieldByName('SUI_TAX').AsCurrency <> 0 then
        begin
          PrintTaxesDeductions(SUIConsolidation['STATE'] + ' EE SUI('
            + FloatToStrF(SUIConsolidation.FieldByName('SUI_TAXABLE_WAGES').AsCurrency, ffFixed, 15, 2) + ')'
            , SUIConsolidation.FieldByName('SUI_TAX').AsCurrency, SUIConsolidation.FieldByName('YTD_SUI_TAX').AsFloat);
        end
        else if FShowYTD and (SUIConsolidation.FieldByName('YTD_SUI_TAX').AsCurrency <> 0) then
          AddDeduction(SUIConsolidation['STATE'] + ' EE SUI('
          + FloatToStrF(SUIConsolidation.FieldByName('SUI_TAXABLE_WAGES').AsFloat, ffFixed, 15, 2) + ')',
          Null, SUIConsolidation.FieldByName('YTD_SUI_TAX').AsFloat);

        SUIConsolidation.Next;
      end;
    finally
      SUIConsolidation.Close;
    end;
  end;
end;

procedure TDM_REGULAR_CHECK.AddTOA(CurrentTOA: boolean);
var
  TempStr, ShowTimeOffCode, s: string;
  Offset, ProbationPeriod, DigitsAfterDec, i, k: integer;
  PTODivisor : Double;
  Q: IevQuery;

  MaxAccrualDate, HireDate: TDatetime;

  procedure InsertTOALine(aDS: TevClientDataSet; aCheckNbr: integer; aLine: string);
  begin
    aDS.Insert;
    aDS.FieldByName('CheckNbr').Value := aCheckNbr;
    aDS.FieldByName('TimeOffLine').Value := aLine;
    if Assigned(aDS.FindField('TimeOffNbr')) then
      aDS.FieldByName('TimeOffNbr').Value := FTimeOffNbr;
    aDS.Post;
  end;

  function GetTOALine(aStr: string): string;
  begin
    Result := LeftStr(aStr, 43);
  end;

  function GetTOALineAllowedLength: integer;
  begin
    if Pos(FCheckForm, CHECK_FORM_PRESSURE_SEAL + CHECK_FORM_PRESSURE_SEAL_LEGAL + CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT +
      CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW + CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE + CHECK_FORM_PRESSURE_SEAL_GREATLAND +
      CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD +
      CHECK_FORM_PRESSURE_SEAL_LEGAL_FF + CHECK_FORM_PRESSURE_SEAL_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_NEW +
      CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE_NEW +
      CHECK_FORM_PRESSURE_SEAL_GREATLAND_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_NEW +
      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_FF_NEW+CHECK_FORM_PRESSURE_SEAL_LEGAL_ANSI_NEW) = 0 then

      Result := 42
    else
      Result := 32;
  end;

  procedure InsertTOA;
  begin
    if Trim(TempStr) <> '' then
    begin
      if FLiteDatasets then // generate "lite" datasets
        InsertTOALine(cdsTOALite, FPrCheckNbr, GetTOALine(TempStr))
      else begin
        Inc(FTimeOffNbr);
        if Offset < 6 then
          InsertTOALine(TIME_OFF_DATA, FCheckNbr, GetTOALine(TempStr))
        else
          CHECK_DATA.FieldByName('CheckComments').Value := TempStr + #13#10 + CHECK_DATA.FieldByName('CheckComments').Value;
      end;
    end;  
  end;
begin
  Offset := 0;
  TempStr := GetEeTaxOverrides;
  if TempStr <> '' then
  begin
    Offset := 1;
    Inc(FTimeOffNbr);
    if FLiteDatasets then // generate "lite" datasets
      InsertTOALine(cdsTOALite, FPrCheckNbr, GetTOALine(TempStr))
    else
      InsertTOALine(TIME_OFF_DATA, FCheckNbr, GetTOALine(TempStr));
  end;
// Time off accrual
  cdTimeOff.Close;

  if CurrentTOA then
  begin
    Q := TEvQuery.Create('GenericSelect');
    Q.Macro['COLUMNS'] := 'max(ACCRUAL_DATE) MAX_ACCRUAL_DATE';
    Q.Macro['TABLENAME'] := 'EE_TIME_OFF_ACCRUAL_OPER';
    MaxAccrualDate := VarToDateTime(ConvertNull(Q.Result['MAX_ACCRUAL_DATE'], SysTime));
  end
  else
    MaxAccrualDate := SysTime;

  with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
  begin
    SetMacro('COLUMNS', 'CHECK_DATE, PROCESS_DATE');
    SetMacro('TABLENAME', 'PR');
    SetMacro('NBRFIELD', 'PR');
    SetParam('RecordNbr', FPayrollNbr);
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
  with TExecDSWrapper.Create('TimeOffPrBalances') do
  begin
    if CurrentTOA then
    begin
      SetParam('StatusDate', SysTime);
      SetParam('CheckDate', MaxAccrualDate);
    end
    else begin
      SetParam('StatusDate', VarToDateTime(ConvertNull(ctx_DataAccess.CUSTOM_VIEW['PROCESS_DATE'], SysTime)));
      SetParam('CheckDate', VarToDateTime(ctx_DataAccess.CUSTOM_VIEW['CHECK_DATE']));
    end;
    SetParam('ThisPayDate', VarToDateTime(ctx_DataAccess.CUSTOM_VIEW['CHECK_DATE']));
    SetParam('EeNbr', wwcsChecks['EE_NBR']);
    cdTimeOff.IndexFieldNames := 'EE_NBR;DESCRIPTION';
    ctx_DataAccess.GetCustomData(cdTimeOff, AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;

  cdTimeOff.First;
  while not cdTimeOff.Eof do
  begin
    ShowTimeOffCode := cdTimeOff['SHOW_USED_BALANCE_ON_CHECK'];
    DigitsAfterDec := cdTimeOff.FieldByName('DIGITS_AFTER_DECIMAL').AsInteger;
    if DigitsAfterDec > 10 then
      DigitsAfterDec := 10;
    ProbationPeriod := ConvertNull(cdTimeOff['PROBATION_PERIOD_MONTHS'], 0);
    if cdTimeOff.FieldByName('EFFECTIVE_ACCRUAL_DATE').IsNull then
    begin
      if DM_EMPLOYEE.EE.FieldByName('CURRENT_HIRE_DATE').IsNull then
        HireDate := 0
      else
        HireDate := DM_EMPLOYEE.EE.FieldByName('CURRENT_HIRE_DATE').AsDateTime;
    end
    else
      HireDate := cdTimeOff.FieldByName('EFFECTIVE_ACCRUAL_DATE').AsDateTime;
    if (ShowTimeOffCode <> TIME_OFF_SHOW_NONE) and ((ProbationPeriod = 0) or (ProbationPeriod <> 0)
    and (IncMonth(HireDate, ProbationPeriod) <= wwcsChecks.FieldByName('PERIOD_END_DATE').AsDateTime)) then
    begin
      TempStr := cdTimeOff.FieldByName('Description').AsString;
      TempStr := PadStringLeft(TempStr, ' ', 16);
      TempStr := Copy(TempStr, 1, 16);
      TempStr := Trim(TempStr);
      TempStr := TempStr + '  ';
      PTODivisor := ConvertNull(cdTimeOff['DIVISOR'], 1);
      if PTODivisor = 0 then
        raise EInconsistentData.CreateHelp('You can not have zero value in Divisor field of Co Time Off Accrual table!', IDH_InconsistentData);
      if (ShowTimeOffCode <> TIME_OFF_SHOW_UNUSED) and (ShowTimeOffCode <> TIME_OFF_SHOW_BALPRIOR_ACCRUED_USED_BALAFTER) then
      begin
        if ShowTimeOffCode <> TIME_OFF_SHOW_USED then
        begin
          if (ShowTimeOffCode[1] in [TIME_OFF_SHOW_ACCRUED_THIS_PAY, TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY_UNUSED, TIME_OFF_SHOW_ACCRUED_THIS_PAY_UNUSED, TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY]) then
            TempStr := TempStr + FloatToStrF(cdTimeOff.FieldByName('ACCRUED_THIS_PAY').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec)
          else
            TempStr := TempStr + FloatToStrF(cdTimeOff.FieldByName('ACCRUED').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec);
          if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
            TempStr := TempStr + ' Accr';
        end;
        if ShowTimeOffCode[1] in [TIME_OFF_SHOW_ACCRUED_USED, TIME_OFF_SHOW_ACCRUED_USED_UNUSED, TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY, TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY_UNUSED] then
        begin
          if (ShowTimeOffCode[1] in [TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY_UNUSED, TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY]) then
            TempStr := TempStr + '-' + FloatToStrF(cdTimeOff.FieldByName('USED_THIS_PAY').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec)
          else
            TempStr := TempStr + '-' + FloatToStrF(cdTimeOff.FieldByName('USED').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec);
          if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
            TempStr := TempStr + ' Used';
        end;
        if ShowTimeOffCode = TIME_OFF_SHOW_USED then
        begin
          TempStr := TempStr + FloatToStrF(cdTimeOff.FieldByName('USED').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec);
          if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
            TempStr := TempStr + ' Used';
        end;
      end;
      if ShowTimeOffCode[1] in [TIME_OFF_SHOW_ACCRUED_USED_UNUSED, TIME_OFF_SHOW_ACCRUED_UNUSED, TIME_OFF_SHOW_UNUSED, TIME_OFF_SHOW_ACCRUED_USED_THIS_PAY_UNUSED, TIME_OFF_SHOW_ACCRUED_THIS_PAY_UNUSED] then
      begin
        TempStr := TempStr + '=' + FloatToStrF((cdTimeOff.FieldByName('ACCRUED').AsFloat - cdTimeOff.FieldByName('USED').AsFloat ) /
          PTODivisor, ffFixed, 15, DigitsAfterDec);
        if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
          TempStr := TempStr + ' Bal';
      end;

      if ShowTimeOffCode[1] = TIME_OFF_SHOW_BALPRIOR_ACCRUED_USED_BALAFTER then
      begin
        TempStr := TempStr + FloatToStrF((cdTimeOff.FieldByName('ACCRUED').AsFloat - cdTimeOff.FieldByName('USED').AsFloat - cdTimeOff.FieldByName('ACCRUED_THIS_PAY').AsFloat + cdTimeOff.FieldByName('USED_THIS_PAY').AsFloat) /
          PTODivisor, ffFixed, 15, DigitsAfterDec);
        if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
          TempStr := TempStr + ' BalPr';

        TempStr := TempStr + '+' + FloatToStrF(cdTimeOff.FieldByName('ACCRUED_THIS_PAY').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec);
        if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
          TempStr := TempStr + ' Acc';

        TempStr := TempStr + '-' + FloatToStrF(cdTimeOff.FieldByName('USED_THIS_PAY').AsFloat / PTODivisor, ffFixed, 15, DigitsAfterDec) + ' ';

        if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
          TempStr := TempStr + 'Used';

        TempStr := TempStr + '=' + FloatToStrF((cdTimeOff.FieldByName('ACCRUED').AsFloat - cdTimeOff.FieldByName('USED').AsFloat ) /
          PTODivisor, ffFixed, 15, DigitsAfterDec);
        if DM_SERVICE_BUREAU.SB.PAY_TAX_FROM_PAYABLES.Value = GROUP_BOX_YES then
          TempStr := TempStr + ' BalAf';

        s := TempStr;
        // total symbols count after splitting TOA line by sublines and
        // adding 5 spaces in the beginning of each subline starting with second one
        k := ((Length(s) div GetTOALineAllowedLength) * (GetTOALineAllowedLength +5) + (Length(s) mod GetTOALineAllowedLength));

        for i := 1 to (k div GetTOALineAllowedLength) do
        begin
          TempStr := Copy(s, 1, GetTOALineAllowedLength);
          s := '          ' + Copy(s, GetTOALineAllowedLength + 1, Length(s));

          InsertTOA;
          Inc(Offset);
          if (Offset = 6) and not FOverflowTimeOff then
            Break;
        end;
        TempStr := s;
      end;

      TempStr := TempStr + ' ' + ConvertNull(cdTimeOff['DIVISOR_DESCRIPTION'], 'HOURS');

      InsertTOA;
      Inc(Offset);
      if (Offset = 6) and not FOverflowTimeOff then
        Break;
    end;
    cdTimeOff.Next;
  end;

// Targets
  ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_SCHEDULED_E_DS, wwcsChecks.FieldByName('CO_NBR').AsInteger, 'T1.SCHEDULED_E_D_ENABLED <> ''N''');
  if DM_CLIENT.CL_E_DS.IndexFieldNames <> 'CUSTOM_E_D_CODE_NUMBER' then
    DM_CLIENT.CL_E_DS.IndexFieldNames := 'CUSTOM_E_D_CODE_NUMBER';
  if DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames <> 'EE_NBR;CL_E_DS_NBR' then
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.IndexFieldNames := 'EE_NBR;CL_E_DS_NBR';
  if DM_COMPANY.CO.FieldByName('CHECK_TYPE').Value <> TARGETS_NEVER_PRINT then
  begin
    if (Offset < 6) or FOverflowTimeOff then
    with DM_EMPLOYEE.EE_SCHEDULED_E_DS do
    begin
      DM_CLIENT.CL_E_DS.First;
      while not DM_CLIENT.CL_E_DS.EOF do
      begin
        FindKey([wwcsChecks.FieldByName('EE_NBR').Value, DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').Value]);
        while not EOF and (wwcsChecks.FieldByName('EE_NBR').Value = FieldByName('EE_NBR').Value)
        and (DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').Value = FieldByName('CL_E_DS_NBR').Value) do
        begin
          if ((ConvertNull(FieldByName('BALANCE').Value, 0) <> 0) or (ConvertNull(FieldByName('TARGET_AMOUNT').Value, 0) <> 0))
            and ((DM_COMPANY.CO.FieldByName('CHECK_TYPE').Value = TARGETS_ALWAYS_PRINT) or ((DM_COMPANY.CO.FieldByName('CHECK_TYPE').Value = TARGETS_PRINT_TILL_REACHED)
            and (FieldByName('BALANCE').Value <> FieldByName('TARGET_AMOUNT').Value))) then
          begin
            if Trim(DM_CLIENT.CL_E_DS.FieldByName('TARGET_DESCRIPTION').AsString) = '' then
              TempStr := DM_CLIENT.CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString
            else
              TempStr := DM_CLIENT.CL_E_DS.FieldByName('TARGET_DESCRIPTION').AsString;

            if DM_CLIENT.CL_E_DS.SHOW_WHICH_BALANCE.Value = GROUP_BOX_YES then
              TempStr := TempStr + ' Balance Remaining $' + FloatToStrF(TARGET_AMOUNT.Value - BALANCE.AsFloat, ffFixed, 15, 2)
            else
              TempStr := TempStr + ' Balance Paid $' + FloatToStrF(FieldByName('BALANCE').AsFloat, ffFixed, 15, 2);

            InsertTOA;

            Inc(Offset);
            if (Offset = 6) and not FOverflowTimeOff then
              Break;
          end;
          Next;
        end;
        if (Offset = 6) and not FOverflowTimeOff then
          Break;
        DM_CLIENT.CL_E_DS.Next;
      end;
    end;
  end;
end;

procedure TDM_REGULAR_CHECK.AddXMLDetails(aCoNumber, aEENumber,
  aEEHireDate, aPeriodBeginDate, aPeriodEndDate, aCheckDate,
  aPaymentSerialNumber, aDivNumber, aBrNumber, aDepNumber, aTmNumber,
  aCheckComment, aEmployeeSSN: string);
var
  tempList: TStrings;
  i: integer;

  procedure AddToaNodes(aDS: TevClientDataSet; aFilter: string);
  begin
    aDS.Filter := aFilter;
    aDS.Filtered := True;
    aDS.First;
    while not aDS.Eof do
    begin
      FxmlData := FxmlData + '<leftNote' + IntToStr(aDS.RecNo) + '>' + AnsiToUtf8(ProcessStringForXML(aDS.FieldByName('TimeOffLine').AsString)) + '</leftNote' + IntToStr(aDS.RecNo) + '>';
      aDS.Next;
    end;
    aDS.Filtered := False;
  end;

begin
  FxmlData := FxmlData + '<details>';
  FxmlData := FxmlData + '<companyNumber>' + AnsiToUtf8(ProcessStringForXML(aCoNumber)) + '</companyNumber>';
  FxmlData := FxmlData + '<employeeNumber>' + AnsiToUtf8(ProcessStringForXML(aEeNumber)) + '</employeeNumber>';
  FxmlData := FxmlData + '<hireDate>' + AnsiToUtf8(ProcessStringForXML(aEEHireDate)) + '</hireDate>';
  FxmlData := FxmlData + '<periodBegin>' + AnsiToUtf8(ProcessStringForXML(aPeriodBeginDate)) + '</periodBegin>';
  FxmlData := FxmlData + '<periodEnd>' + AnsiToUtf8(ProcessStringForXML(aPeriodEndDate)) + '</periodEnd>';
  FxmlData := FxmlData + '<checkDate>' + AnsiToUtf8(ProcessStringForXML(aCheckDate)) + '</checkDate>';
  FxmlData := FxmlData + '<checkNumber>' + AnsiToUtf8(ProcessStringForXML(aPaymentSerialNumber)) + '</checkNumber>';
  FxmlData := FxmlData + '<division>' + AnsiToUtf8(ProcessStringForXML(aDivNumber)) + '</division>';
  FxmlData := FxmlData + '<branch>' + AnsiToUtf8(ProcessStringForXML(aBrNumber)) + '</branch>';
  FxmlData := FxmlData + '<department>' + AnsiToUtf8(ProcessStringForXML(aDepNumber)) + '</department>';
  FxmlData := FxmlData + '<team>' + AnsiToUtf8(ProcessStringForXML(aTmNumber)) + '</team>';
  FxmlData := FxmlData + '<EmployeeSSN>' + AnsiToUtf8(ProcessStringForXML(aEmployeeSSN)) + '</EmployeeSSN>';

  if FLiteDatasets then
    AddToaNodes(cdsTOALite, 'CheckNbr=' + IntToStr(FPrCheckNbr))
  else
    AddToaNodes(TIME_OFF_DATA, 'CheckNbr=' + IntToStr(FCheckNbr));

  tempList := TStringList.Create;
  try
    tempList.Text := aCheckComment;

    for i := 0 to Pred(tempList.Count) do
      FxmlData := FxmlData + '<rightNote' + IntToStr(Succ(i)) + '>' + AnsiToUtf8(ProcessStringForXML(tempList[i])) + '</rightNote' + IntToStr(Succ(i)) + '>';
  finally
    tempList.Free;
  end;

  FxmlData := FxmlData + '</details>';
end;

procedure TDM_REGULAR_CHECK.AddXMLHeader(EEName, CoName, aCompanyName, aAddress1, aAddress2, aCity, aState, aZip_Code, aPhone1: string);
begin
  FxmlData := '<?xml version="1.0" encoding="UTF-8" ?>';
  FxmlData := FxmlData + '<Paystub>';

  FxmlData := FxmlData + '<title>';
  FxmlData := FxmlData + '<employeeName>' + AnsiToUtf8(ProcessStringForXML(EEName)) + '</employeeName>';
  FxmlData := FxmlData + '<companyName>' + AnsiToUtf8(ProcessStringForXML(CoName)) + '</companyName>';
  FxmlData := FxmlData + '<CompanyName1>' + AnsiToUtf8(ProcessStringForXML(aCompanyName)) + '</CompanyName1>';
  FxmlData := FxmlData + '<Address1>' + AnsiToUtf8(ProcessStringForXML(aAddress1)) + '</Address1>';
  FxmlData := FxmlData + '<Address2>' + AnsiToUtf8(ProcessStringForXML(aAddress2)) + '</Address2>';
  FxmlData := FxmlData + '<City>' + AnsiToUtf8(ProcessStringForXML(aCity)) + '</City>';
  FxmlData := FxmlData + '<State>' + AnsiToUtf8(ProcessStringForXML(aState)) + '</State>';
  FxmlData := FxmlData + '<Zip_Code>' + AnsiToUtf8(ProcessStringForXML(aZip_Code)) + '</Zip_Code>';
  FxmlData := FxmlData + '<Phone1>' + AnsiToUtf8(ProcessStringForXML(aPhone1)) + '</Phone1>';

  FxmlData := FxmlData + '</title>';
end;

procedure TDM_REGULAR_CHECK.AddXMLTotals(tHours, tEarnings, tYTDEarnings,
  tDeductions, tYTDDeductions, tNetPay, tDirDep, tCheckAmount,
  tCheckYTD: string);
begin
  FxmlData := FxmlData + '<totalEarnHours>' + tHours + '</totalEarnHours>';
  FxmlData := FxmlData + '<totalEarnCurrent>' + tEarnings + '</totalEarnCurrent>';
  FxmlData := FxmlData + '<totalEarnYTD>' + tYTDEarnings + '</totalEarnYTD>';
  FxmlData := FxmlData + '<totalDedCurrent>' + tDeductions + '</totalDedCurrent>';
  FxmlData := FxmlData + '<totalDedYTD>' + tYTDDeductions + '</totalDedYTD>';
  FxmlData := FxmlData + '<netPay>' + tNetPay + '</netPay>';
  FxmlData := FxmlData + '<totalDirDep>' + tDirDep + '</totalDirDep>';
  FxmlData := FxmlData + '<checkAmountCurrent>' + tCheckAmount + '</checkAmountCurrent>';
  FxmlData := FxmlData + '<checkAmountYTD>' + tCheckYTD + '</checkAmountYTD>';
  FxmlData := FxmlData + '</lines>';
  FxmlData := FxmlData + '</Paystub>';
end;

function TDM_REGULAR_CHECK.GetEeTaxOverrides: string;
begin
  with DM_EMPLOYEE.EE do
  if not FieldByName('OVERRIDE_FED_TAX_VALUE').IsNull and (FieldByName('OVERRIDE_FED_TAX_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
  begin
    Result := 'Fed OR ';
    if FieldByName('OVERRIDE_FED_TAX_TYPE').AsString[1] in [OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT, OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT] then
      Result := Result + 'addl ';
    if FieldByName('OVERRIDE_FED_TAX_TYPE').AsString[1] in [OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT, OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT] then
      Result := Result + '$' + FloatToStrF(FieldByName('OVERRIDE_FED_TAX_VALUE').Value, ffFixed, 15, 2)
    else
      Result := Result + FloatToStr(FieldByName('OVERRIDE_FED_TAX_VALUE').Value) + '%';
  end;
  ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_STATES, wwcsChecks.FieldByName('CO_NBR').AsInteger);
  if DM_EMPLOYEE.EE_STATES.IndexFieldNames <> 'EE_STATES_NBR' then
    DM_EMPLOYEE.EE_STATES.IndexFieldNames := 'EE_STATES_NBR';

  if DM_COMPANY.CO_STATES.IndexFieldNames <> 'CO_STATES_NBR' then
    DM_COMPANY.CO_STATES.IndexFieldNames := 'CO_STATES_NBR';

  Assert(DM_EMPLOYEE.EE_STATES.FindKey([DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').Value]), Format(sAssertString, ['EE.HOME_TAX_EE_STATES_NBR', 'EE_STATES.EE_STATES_NBR', DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
  with DM_EMPLOYEE.EE_STATES do
  if not FieldByName('OVERRIDE_STATE_TAX_VALUE').IsNull and (FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
  begin
    if Result <> '' then
      Result := Result + '; ';
    Assert(DM_COMPANY.CO_STATES.FindKey([FieldByName('CO_STATES_NBR').Value]), Format(sAssertString, ['EE_STATES.CO_STATES_NBR', 'CO_STATES.CO_STATES_NBR', FieldByName('CO_STATES_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
    Result := Result + DM_COMPANY.CO_STATES['STATE'] + ' OR ';
    if FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString[1] in [OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT, OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT] then
      Result := Result + 'addl ';
    if FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString[1] in [OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT, OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT] then
      Result := Result + '$' + FloatToStrF(FieldByName('OVERRIDE_STATE_TAX_VALUE').Value, ffFixed, 15, 2)
    else
      Result := Result + FloatToStr(FieldByName('OVERRIDE_STATE_TAX_VALUE').Value) + '%';
  end;
end;

function TDM_REGULAR_CHECK.GetStateMaritalStatus: string;
begin
  if DM_EMPLOYEE.EE_STATES['State_Lookup'] <> 'MD' then
    Result := DM_EMPLOYEE.EE_STATES.FieldByName('STATE_MARITAL_STATUS').AsString
  else
  begin
    Assert(DM_COMPANY.CO_STATES.FindKey([DM_EMPLOYEE.EE_STATES['CO_STATES_NBR']]));
    Assert(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.FindKey([DM_COMPANY.CO_STATES['SY_STATES_NBR'], DM_EMPLOYEE.EE_STATES['STATE_MARITAL_STATUS']]));
    Result := LeftStr(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS['STATUS_DESCRIPTION'], 4);
  end;
end;

procedure TDM_REGULAR_CHECK.PrintDeduction(DeductionNbr: Integer; Current,
  YTD: Double);
var
  Description, DirDepNbr: string;
  finish: boolean;
begin
  Description := DM_CLIENT.CL_E_DS['DESCRIPTION'];
  if FShowDirDep then
  begin
    with DM_EMPLOYEE do
    begin
      DirDepNbr := '';
      Finish := False;
      if EE_SCHEDULED_E_DS.FindKey([wwcsChecks.FieldByName('EE_NBR').Value, DeductionNbr]) then
      repeat
        if not (EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').IsNull) and
          (EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_END_DATE').Value < wwcsChecks.FieldByName('PROCESS_DATE').Value) then
        begin
          EE_SCHEDULED_E_DS.Next;
          if EE_SCHEDULED_E_DS.EOF or (EE_SCHEDULED_E_DS.FieldByName('EE_NBR').Value <> wwcsChecks.FieldByName('EE_NBR').Value)
          or (EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').Value <> DeductionNbr) then
            Finish := True;
        end
        else begin
          if EE_DIRECT_DEPOSIT.FindKey([EE_SCHEDULED_E_DS['EE_DIRECT_DEPOSIT_NBR']]) then
            DirDepNbr := ConvertNull(EE_DIRECT_DEPOSIT['EE_BANK_ACCOUNT_NUMBER'], '');
          Finish := True;
        end;
      until Finish;
    end;
    if DirDepNbr <> '' then
    begin
      if Length(DirDepNbr) > 9 then
        DirDepNbr := Copy(DirDepNbr, Length(DirDepNbr) - 8, 9);
      DirDepNbr[Length(DirDepNbr)] := 'X';
      if Length(DirDepNbr) > 1 then
        DirDepNbr[Length(DirDepNbr) - 1] := 'X';
      if Length(DirDepNbr) > 2 then
        DirDepNbr[Length(DirDepNbr) - 2] := 'X';
      if Length(DirDepNbr) > 3 then
        DirDepNbr[Length(DirDepNbr) - 3] := 'X';
      Description := Copy(Description, 1, 15) + ' ' + DirDepNbr;
    end;
  end;
  PrintTaxesDeductions(Description, Current, YTD);
end;

procedure TDM_REGULAR_CHECK.PrintTaxesDeductions(Description: String;
  Current, YTD: Double);
begin
  if FShowYtd and FShowEdYtd then
    AddDeduction(Description, Current, YTD)
  else
    AddDeduction(Description, Current, Null);
end;

function TDM_REGULAR_CHECK.ShowWage(const AYTDWages: double;
  ACurrentWages: double): double;
begin
  if FShowYTDTaxWages then
    Result := AYTDWages
  else
    Result := ACurrentWages;
end;

procedure TDM_REGULAR_CHECK.WriteXMLStreamToCheck;
var
  xmlStream, blobStream: TStringStream;
begin
  Assert(DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value = wwcsChecks.FieldByName('PR_CHECK_NBR').Value);
  Assert(DM_PAYROLL.PR.PR_NBR.Value = DM_PAYROLL.PR_CHECK.PR_NBR.Value);

  ctx_DataAccess.UnlockPRSimple;
  xmlStream := TStringStream.Create(FxmlData);
  blobStream := TStringStream.Create('');
  try
    DeflateStream(xmlStream, blobStream);
    DM_PAYROLL.PR_CHECK.Edit;
    DM_PAYROLL.PR_CHECK.UpdateBlobData('DATA_NBR', blobStream.DataString);
    DM_PAYROLL.PR_CHECK.Post;
  finally
    blobStream.Free;
    xmlStream.Free;
    ctx_DataAccess.LockPRSimple;
  end;
end;

procedure TDM_REGULAR_CHECK.PrepareReportDataLite(
  PR_CHECK_LINES: TevClientDataSet; CurrentTOA,
  ForcePrintVoucher: boolean);
var
  BankAcctNbr: Integer;
  ShowDBDTAddress, DBDTLevel, CoName,
  DivNumber, BrNumber, DepNumber, TmNumber: String;
  bIsProc, ShowClientName: Boolean;
  aCheckStubRecord: TCheckStubRecord;
  EmployeeSsnForXml, CompanyNameForXml, Address1ForXml, Address2ForXml,
  CityForXml, StateForXml, Zip_CodeForXml, Phone1ForXml: String;
begin
  DM_CLIENT.CL.Activate;

  if FReversePrinting then
    wwcsChecks.Last
  else
    wwcsChecks.First;
  while not (wwcsChecks.EOF and not FReversePrinting or wwcsChecks.BOF and FReversePrinting) do
  begin
    DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + wwcsChecks.FieldByName('CO_NBR').AsString);
    if DM_EMPLOYEE.EE.IndexFieldNames <> 'EE_NBR' then
      DM_EMPLOYEE.EE.IndexFieldNames := 'EE_NBR';
    Assert(DM_EMPLOYEE.EE.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]), Format(sAssertString, ['wwcsChecks.EE_NBR', 'DM_EMPLOYEE.EE.EE_NBR', wwcsChecks.FieldByName('EE_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

    FPrCheckNbr := wwcsChecks.FieldByName('PR_CHECK_NBR').Value;

    cdsCheckDataLite.Insert;
    cdsCheckDataLite.FieldByName('CheckNbr').Value := FPrCheckNbr;
    cdsCheckDataLite.FieldByName('EE_NBR').Value := wwcsChecks.FieldByName('EE_NBR').Value;
    cdsCheckDataLite.FieldByName('Voucher').Value := iff(wwcsChecks.FieldByName('NET_WAGES').AsFloat = 0, 1, 0);

    CheckStub.Clear;

    with DM_COMPANY, DM_EMPLOYEE do
    begin
      CO_DIVISION.DataRequired('ALL');
      if CO_DIVISION.IndexFieldNames <> 'CO_DIVISION_NBR' then
        CO_DIVISION.IndexFieldNames := 'CO_DIVISION_NBR';
      CO_BRANCH.DataRequired('ALL');
      if CO_BRANCH.IndexFieldNames <> 'CO_BRANCH_NBR' then
        CO_BRANCH.IndexFieldNames := 'CO_BRANCH_NBR';
      CO_DEPARTMENT.DataRequired('ALL');
      if CO_DEPARTMENT.IndexFieldNames <> 'CO_DEPARTMENT_NBR' then
        CO_DEPARTMENT.IndexFieldNames := 'CO_DEPARTMENT_NBR';
      CO_TEAM.DataRequired('ALL');
      if CO_TEAM.IndexFieldNames <> 'CO_TEAM_NBR' then
        CO_TEAM.IndexFieldNames := 'CO_TEAM_NBR';
    end;

    ShowDBDTAddress := '';
    DBDTLevel := DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString;
    with DM_COMPANY, DM_EMPLOYEE do
    repeat
      if ShowDBDTAddress <> '' then
        DBDTLevel := IntToStr(StrToInt(DBDTLevel) - 1);
      case DBDTLevel[1] of
      CLIENT_LEVEL_DIVISION:
        begin
          Assert(CO_DIVISION.FindKey([EE.FieldByName('CO_DIVISION_NBR').Value]), Format(sAssertString, ['EE.CO_DIVISION_NBR', 'CO_DIVISION.CO_DIVISION_NBR', EE.FieldByName('CO_DIVISION_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_DIVISION['PRINT_DIV_ADDRESS_ON_CHECKS'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end;
      CLIENT_LEVEL_BRANCH:
        begin
          Assert(CO_BRANCH.FindKey([EE.FieldByName('CO_BRANCH_NBR').Value]), Format(sAssertString, ['EE.CO_BRANCH_NBR', 'CO_BRANCH.CO_BRANCH_NBR', EE.FieldByName('CO_BRANCH_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_BRANCH['PRINT_BRANCH_ADDRESS_ON_CHECK'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end;
      CLIENT_LEVEL_DEPT:
        begin
          Assert(CO_DEPARTMENT.FindKey([EE.FieldByName('CO_DEPARTMENT_NBR').Value]), Format(sAssertString, ['EE.CO_DEPARTMENT_NBR', 'CO_DEPARTMENT.CO_DEPARTMENT_NBR', EE.FieldByName('CO_DEPARTMENT_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_DEPARTMENT['PRINT_DEPT_ADDRESS_ON_CHECKS'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end;
      CLIENT_LEVEL_TEAM:
        begin
          Assert(CO_TEAM.FindKey([EE.FieldByName('CO_TEAM_NBR').Value]), Format(sAssertString, ['EE.CO_TEAM_NBR', 'CO_TEAM.CO_TEAM_NBR', EE.FieldByName('CO_TEAM_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
          ShowDBDTAddress := ConvertNull(CO_TEAM['PRINT_GROUP_ADDRESS_ON_CHECK'], EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
        end
      else
        ShowDBDTAddress := EXCLUDE_REPT_AND_ADDRESS_ON_CHECK;
      end;
    until (DBDTLevel = CLIENT_LEVEL_COMPANY) or (ShowDBDTAddress = INCLUDE_REPT_AND_ADDRESS_ON_CHECK);

    ShowClientName := DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES;
    if ShowClientName then
      CoName := DM_CLIENT.CL.NAME.Value
    else if ShowDBDTAddress = INCLUDE_REPT_AND_ADDRESS_ON_CHECK then
      with DM_COMPANY, DM_EMPLOYEE do
        CoName := ConvertNull(ctx_PayrollCalculation.GetHomeDBDTFieldValue('NAME', DBDTLevel,
          wwcsChecks.FieldByName('EE_NBR').Value, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE, True), '')
    else
      CoName := wwcsChecks.FieldByName('NAME').AsString;


    if not VarIsNull(wwcsChecks.FieldByName('CO_DIVISION_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_DIVISION.FindKey([wwcsChecks.FieldByName('CO_DIVISION_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_DIVISION_NBR', 'CO_DIVISION.CO_DIVISION_NBR', wwcsChecks.FieldByName('CO_DIVISION_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_DIVISION['PRINT_DIV_ADDRESS_ON_CHECKS'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        DivNumber := Trim(ConvertNull(DM_COMPANY.CO_DIVISION['CUSTOM_DIVISION_NUMBER'], ''));
    end;

    if not VarIsNull(wwcsChecks.FieldByName('CO_BRANCH_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_BRANCH.FindKey([wwcsChecks.FieldByName('CO_BRANCH_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_BRANCH_NBR', 'CO_BRANCH.CO_BRANCH_NBR', wwcsChecks.FieldByName('CO_BRANCH_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_BRANCH['PRINT_BRANCH_ADDRESS_ON_CHECK'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        BrNumber := Trim(ConvertNull(DM_COMPANY.CO_BRANCH['CUSTOM_BRANCH_NUMBER'], ''));
    end;

    if not VarIsNull(wwcsChecks.FieldByName('CO_DEPARTMENT_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_DEPARTMENT.FindKey([wwcsChecks.FieldByName('CO_DEPARTMENT_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_DEPARTMENT_NBR', 'CO_DEPARTMENT.CO_DEPARTMENT_NBR', wwcsChecks.FieldByName('CO_DEPARTMENT_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_DEPARTMENT['PRINT_DEPT_ADDRESS_ON_CHECKS'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        DepNumber := Trim(ConvertNull(DM_COMPANY.CO_DEPARTMENT['CUSTOM_DEPARTMENT_NUMBER'], ''))
    end;

    if not VarIsNull(wwcsChecks.FieldByName('CO_TEAM_NBR').Value) then
    begin
      Assert(DM_COMPANY.CO_TEAM.FindKey([wwcsChecks.FieldByName('CO_TEAM_NBR').Value]), Format(sAssertString, ['wwcsChecks.CO_TEAM_NBR', 'CO_TEAM.CO_TEAM_NBR', wwcsChecks.FieldByName('CO_TEAM_NBR').AsString, wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));
      if DM_COMPANY.CO_TEAM['PRINT_GROUP_ADDRESS_ON_CHECK'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
        TmNumber := Trim(ConvertNull(DM_COMPANY.CO_TEAM['CUSTOM_TEAM_NUMBER'], ''))
    end;

    DM_CLIENT.CL_BANK_ACCOUNT.Activate;
    if DM_CLIENT.CL_BANK_ACCOUNT.IndexFieldNames <> 'CL_BANK_ACCOUNT_NBR' then
      DM_CLIENT.CL_BANK_ACCOUNT.IndexFieldNames := 'CL_BANK_ACCOUNT_NBR';

    if GetBankAccountNumberAsOfDate(wwcsChecks, BankAcctNbr) then
//    if ctx_PayrollCalculation.GetBankAccountNumber(wwcsChecks, BankAcctNbr) then
      cdsCheckDataLite.FieldByName('BankLevel').Value := 'B'
    else
      cdsCheckDataLite.FieldByName('BankLevel').Value := 'C';
    cdsCheckDataLite.FieldByName('AccountNbr').Value := BankAcctNbr;


  // Check stub
    DM_COMPANY.CO_JOBS.DataRequired('CO_NBR=' + wwcsChecks.FieldByName('CO_NBR').AsString);
    if DM_COMPANY.CO_JOBS.IndexFieldNames <> 'CO_JOBS_NBR' then
      DM_COMPANY.CO_JOBS.IndexFieldNames := 'CO_JOBS_NBR';

    AddTOA(CurrentTOA);

    if DM_CLIENT.CL_E_DS.IndexFieldNames <> 'CL_E_DS_NBR' then
      DM_CLIENT.CL_E_DS.IndexFieldNames := 'CL_E_DS_NBR';

    if (DM_COMPANY.CO.FieldByName('SHOW_SS_NUMBER_ON_CHECK').Value = 'Y')
    or (DM_COMPANY.CO.FieldByName('SHOW_SS_NUMBER_ON_CHECK').Value = GROUP_BOX_LAST4) then
      EmployeeSsnForXml := 'XXX-XX-' + copy(wwcsChecks.FieldByName('SOCIAL_SECURITY_NUMBER').AsString,8,4)
    else
      EmployeeSsnForXml := '';

    if DM_CLIENT.CL.PRINT_CLIENT_NAME.AsString = 'Y' then
    begin
      CompanyNameForXml := DM_CLIENT.CL.NAME.AsString;
      Address1ForXml := DM_CLIENT.CL.ADDRESS1.AsString;
      Address2ForXml := DM_CLIENT.CL.ADDRESS2.AsString;
      CityForXml := DM_CLIENT.CL.CITY.AsString;
      StateForXml := DM_CLIENT.CL.STATE.AsString;
      Zip_CodeForXml := DM_CLIENT.CL.ZIP_CODE.AsString;
      Phone1ForXml := DM_CLIENT.CL.PHONE1.AsString;
    end
    else begin
      // Company level
      CompanyNameForXml := DM_COMPANY.CO.NAME.AsString;
      Address1ForXml := DM_COMPANY.CO.ADDRESS1.AsString;
      Address2ForXml := DM_COMPANY.CO.ADDRESS2.AsString;
      CityForXml := DM_COMPANY.CO.CITY.AsString;
      StateForXml := DM_COMPANY.CO.STATE.AsString;
      Zip_CodeForXml := DM_COMPANY.CO.ZIP_CODE.AsString;
      Phone1ForXml := '';
      DM_COMPANY.CO_PHONE.DataRequired('CO_NBR=' + DM_COMPANY.CO.CO_NBR.AsString);
      DM_COMPANY.CO_PHONE.First;
      while not DM_COMPANY.CO_PHONE.Eof do
      begin
        if DM_COMPANY.CO_PHONE.PHONE_TYPE.AsString = 'P' then
        begin
          Phone1ForXml := DM_COMPANY.CO_PHONE.PHONE_NUMBER.AsString;
          break;
        end;
        DM_COMPANY.CO_PHONE.Next;
      end;

      if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_COMPANY))
         and (DivNumber <> '')
         and (Trim(DM_COMPANY.CO_DIVISION.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_DIVISION.ADDRESS1.AsString)<>'')
         and (DM_COMPANY.CO_DIVISION.PRINT_DIV_ADDRESS_ON_CHECKS.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
          CompanyNameForXml := DM_COMPANY.CO_DIVISION.NAME.AsString;
          Address1ForXml := DM_COMPANY.CO_DIVISION.ADDRESS1.AsString;
          Address2ForXml := DM_COMPANY.CO_DIVISION.ADDRESS2.AsString;
          CityForXml := DM_COMPANY.CO_DIVISION.CITY.AsString;
          StateForXml := DM_COMPANY.CO_DIVISION.STATE.AsString;
          Zip_CodeForXml := DM_COMPANY.CO_DIVISION.ZIP_CODE.AsString;
          Phone1ForXml := DM_COMPANY.CO_DIVISION.PHONE1.AsString;
      end;

      if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_DIVISION))
         and (BrNumber <> '')
         and (Trim(DM_COMPANY.CO_BRANCH.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_BRANCH.ADDRESS1.AsString)<>'')
         and (DM_COMPANY.CO_BRANCH.PRINT_BRANCH_ADDRESS_ON_CHECK.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
          CompanyNameForXml := DM_COMPANY.CO_BRANCH.NAME.AsString;
          Address1ForXml := DM_COMPANY.CO_BRANCH.ADDRESS1.AsString;
          Address2ForXml := DM_COMPANY.CO_BRANCH.ADDRESS2.AsString;
          CityForXml := DM_COMPANY.CO_BRANCH.CITY.AsString;
          StateForXml := DM_COMPANY.CO_BRANCH.STATE.AsString;
          Zip_CodeForXml := DM_COMPANY.CO_BRANCH.ZIP_CODE.AsString;
          Phone1ForXml := DM_COMPANY.CO_BRANCH.PHONE1.AsString;
      end;

      if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_BRANCH))
         and (DepNumber <> '')
         and (Trim(DM_COMPANY.CO_DEPARTMENT.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_DEPARTMENT.ADDRESS1.AsString)<>'')
         and (DM_COMPANY.CO_DEPARTMENT.PRINT_DEPT_ADDRESS_ON_CHECKS.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
          CompanyNameForXml := DM_COMPANY.CO_DEPARTMENT.NAME.AsString;
          Address1ForXml := DM_COMPANY.CO_DEPARTMENT.ADDRESS1.AsString;
          Address2ForXml := DM_COMPANY.CO_DEPARTMENT.ADDRESS2.AsString;
          CityForXml := DM_COMPANY.CO_DEPARTMENT.CITY.AsString;
          StateForXml := DM_COMPANY.CO_DEPARTMENT.STATE.AsString;
          Zip_CodeForXml := DM_COMPANY.CO_DEPARTMENT.ZIP_CODE.AsString;
          Phone1ForXml := DM_COMPANY.CO_DEPARTMENT.PHONE1.AsString;
      end;

      if (DM_COMPANY.CO.DBDT_LEVEL.Asinteger > StrToInt(CLIENT_LEVEL_DEPT))
         and (TmNumber <> '')
         and (Trim(DM_COMPANY.CO_TEAM.NAME.AsString)<>'') and (Trim(DM_COMPANY.CO_TEAM.ADDRESS1.AsString)<>'')
         and (DM_COMPANY.CO_TEAM.PRINT_GROUP_ADDRESS_ON_CHECK.AsString = INCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
          CompanyNameForXml := DM_COMPANY.CO_TEAM.NAME.AsString;
          Address1ForXml := DM_COMPANY.CO_TEAM.ADDRESS1.AsString;
          Address2ForXml := DM_COMPANY.CO_TEAM.ADDRESS2.AsString;
          CityForXml := DM_COMPANY.CO_TEAM.CITY.AsString;
          StateForXml := DM_COMPANY.CO_TEAM.STATE.AsString;
          Zip_CodeForXml := DM_COMPANY.CO_TEAM.ZIP_CODE.AsString;
          Phone1ForXml := DM_COMPANY.CO_TEAM.PHONE1.AsString;
      end;
    end;

    AddXMLHeader(
      wwcsChecks.FieldByName('FIRST_NAME').AsString + ' ' + wwcsChecks.FieldByName('MIDDLE_INITIAL').AsString + ' ' + wwcsChecks.FieldByName('LAST_NAME').AsString,
        CompanyNameForXml, //CoName, <- ticket #96196, ESS take the DBDT name from CompanyName tag, but should take from CompanyName1
        CompanyNameForXml,
        Address1ForXml,
        Address2ForXml,
        CityForXml,
        StateForXml,
        Zip_CodeForXml,
        Phone1ForXml);

    AddXMLDetails(wwcsChecks.FieldByName('CUSTOM_COMPANY_NUMBER').AsString,
      wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString,
      wwcsChecks.FieldByName('CURRENT_HIRE_DATE').AsString,
      wwcsChecks.FieldByName('PERIOD_BEGIN_DATE').AsString,
      wwcsChecks.FieldByName('PERIOD_END_DATE').AsString,
      wwcsChecks.FieldByName('CHECK_DATE').AsString,
      wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString,
      DivNumber,
      BrNumber,
      DepNumber,
      TmNumber,
      wwcsChecks.FieldByName('CHECK_COMMENTS').AsString,
      EmployeeSsnForXml
      );

    with PR_CHECK_LINES do
    begin
      if IndexFieldNames <> 'PR_CHECK_NBR;CL_E_DS_NBR;RATE_OF_PAY;CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR;CO_JOBS_NBR;PUNCH_IN;PUNCH_OUT' then
        IndexFieldNames := 'PR_CHECK_NBR;CL_E_DS_NBR;RATE_OF_PAY;CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR;CO_JOBS_NBR;PUNCH_IN;PUNCH_OUT';

      if CL_E_DS_YTD.IndexFieldNames <> 'EE_NBR;CUSTOM_E_D_CODE_NUMBER' then
        CL_E_DS_YTD.IndexFieldNames := 'EE_NBR;CUSTOM_E_D_CODE_NUMBER';
      CL_E_DS_YTD.FindKey([wwcsChecks.FieldByName('EE_NBR').Value]);

      AddEarnings(PR_CHECK_LINES);

      if DM_PAYROLL.PR_CHECK.IndexFieldNames <> 'PR_CHECK_NBR' then
        DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
      Assert(DM_PAYROLL.PR_CHECK.FindKey([FPrCheckNbr]), Format(sAssertString, ['wwcsChecks.PR_CHECK_NBR', 'PR_CHECK.PR_CHECK_NBR', IntToStr(FPrCheckNbr), wwcsChecks.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString]));

      AddFederalTaxes;
      AddStateTaxes;
      AddSuiTaxes;
      AddLocalTaxes;

      AddDeductions(PR_CHECK_LINES);
    end;

    cdsCheckDataLite.FieldByName('DirDep').Value := CheckStub.TotalDirDep;
    cdsCheckDataLite.FieldByName('YTDNet').Value := FEDERAL_YTD['NET_WAGES'];

    FxmlData := FxmlData + '<lines>';
    while (CheckStub.NextLineToPrint < CheckStub.Count) do
    begin
      CheckStub.ExtractRecord(CheckStub.NextLineToPrint, aCheckStubRecord);
      AddCheckStubRecordToXmlData(aCheckStubRecord);

      Inc(CheckStub.NextLineToPrint);
    end;

    AddXmlTotals(
      FloatToStrF(CheckStub.TotalHours, ffFixed, 15, 2),
      FloatToStrF(CheckStub.TotalEarnings, ffFixed, 15, 2),
      FloatToStrF(CheckStub.YTDEarnings, ffFixed, 15, 2),
      FloatToStrF(CheckStub.TotalDeductions, ffFixed, 15, 2),
      FloatToStrF(CheckStub.YTDDeductions, ffFixed, 15, 2),
      FloatToStrF(wwcsChecks.FieldByName('NET_WAGES').AsFloat + CheckStub.TotalDirDep, ffFixed, 15, 2),
      FloatToStrF(CheckStub.TotalDirDep, ffFixed, 15, 2),
      FloatToStrF(wwcsChecks.FieldByName('NET_WAGES').AsFloat, ffFixed, 15, 2),
      FloatToStrF(FEDERAL_YTD.FieldByName('NET_WAGES').AsFloat, ffFixed, 15, 2));

    WriteXMLStreamToCheck;

    if (wwcsChecks.FieldByName('NET_WAGES').Value = 0) and (DM_EMPLOYEE.EE.FieldByName('PRINT_VOUCHER').Value = 'N') and not ForcePrintVoucher then
      cdsCheckDataLite.Cancel
    else
      cdsCheckDataLite.Post;

    if FReversePrinting then
      wwcsChecks.Prior
    else
      wwcsChecks.Next;
  end;

  ctx_DataAccess.UnlockPR;
  bIsProc := ctx_DataAccess.PayrollIsProcessing;
  ctx_DataAccess.PayrollIsProcessing := True;
  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    try
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK]);
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  finally
    ctx_DataAccess.PayrollIsProcessing := bIsProc;
    ctx_DataAccess.LockPR(True);
  end;
end;

function TDM_REGULAR_CHECK.ProcessStringForXML(aStr: string): string;
begin
  Result := StringReplace( StringReplace(aStr, '&', '&amp;', [rfReplaceAll]), '<', '&lt;', [rfReplaceAll])
end;

end.
