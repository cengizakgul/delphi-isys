object DM_REGULAR_CHECK: TDM_REGULAR_CHECK
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 385
  Top = 199
  Height = 479
  Width = 741
  object CHECK_DATA: TevClientDataSet
    Left = 40
    Top = 48
    object CHECK_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object CHECK_DATAEntityName: TStringField
      FieldName = 'EntityName'
      Size = 40
    end
    object CHECK_DATAEntityAddress1: TStringField
      FieldName = 'EntityAddress1'
      Size = 40
    end
    object CHECK_DATAEntityAddress2: TStringField
      FieldName = 'EntityAddress2'
      Size = 40
    end
    object CHECK_DATAEntityAddress3: TStringField
      FieldName = 'EntityAddress3'
      Size = 40
    end
    object CHECK_DATABankName: TStringField
      FieldName = 'BankName'
      Size = 30
    end
    object CHECK_DATABankAddress: TStringField
      FieldName = 'BankAddress'
      Size = 60
    end
    object CHECK_DATABankTopABANumber: TStringField
      FieldName = 'BankTopABANumber'
    end
    object CHECK_DATABankBottomABANumber: TStringField
      FieldName = 'BankBottomABANumber'
    end
    object CHECK_DATAMICRLine: TStringField
      FieldName = 'MICRLine'
      Size = 50
    end
    object CHECK_DATACheckAmount: TStringField
      FieldName = 'CheckAmount'
      Size = 60
    end
    object CHECK_DATACheckAmountSpelled: TStringField
      DisplayWidth = 110
      FieldName = 'CheckAmountSpelled'
      Size = 110
    end
    object CHECK_DATACheckAmountSecurity: TStringField
      FieldName = 'CheckAmountSecurity'
    end
    object CHECK_DATAPaymentSerialNumber: TStringField
      FieldName = 'PaymentSerialNumber'
    end
    object CHECK_DATACheckNumberOrVoucher: TStringField
      FieldName = 'CheckNumberOrVoucher'
    end
    object CHECK_DATADivisionNumber: TStringField
      FieldName = 'DivisionNumber'
    end
    object CHECK_DATABranchNumber: TStringField
      FieldName = 'BranchNumber'
    end
    object CHECK_DATADepartmentNumber: TStringField
      FieldName = 'DepartmentNumber'
    end
    object CHECK_DATATeamNumber: TStringField
      FieldName = 'TeamNumber'
    end
    object CHECK_DATACheckMessage: TStringField
      FieldName = 'CheckMessage'
      Size = 40
    end
    object CHECK_DATAEmployeeName: TStringField
      FieldName = 'EmployeeName'
      Size = 40
    end
    object CHECK_DATAEmployeeAddress1: TStringField
      FieldName = 'EmployeeAddress1'
      Size = 40
    end
    object CHECK_DATAEmployeeAddress2: TStringField
      FieldName = 'EmployeeAddress2'
      Size = 40
    end
    object CHECK_DATAEmployeeAddress3: TStringField
      FieldName = 'EmployeeAddress3'
      Size = 40
    end
    object CHECK_DATAEmployeeSSN: TStringField
      FieldName = 'EmployeeSSN'
    end
    object CHECK_DATAEmployeeNumber: TStringField
      FieldName = 'EmployeeNumber'
      Size = 10
    end
    object CHECK_DATAEmployeeHireDate: TStringField
      FieldName = 'EmployeeHireDate'
    end
    object CHECK_DATATotalHours: TStringField
      FieldName = 'TotalHours'
    end
    object CHECK_DATATotalEarnings: TStringField
      FieldName = 'TotalEarnings'
    end
    object CHECK_DATATotalYTDEarnings: TStringField
      FieldName = 'TotalYTDEarnings'
    end
    object CHECK_DATATotalDeductions: TStringField
      FieldName = 'TotalDeductions'
    end
    object CHECK_DATATotalYTDDeductions: TStringField
      FieldName = 'TotalYTDDeductions'
    end
    object CHECK_DATANet: TStringField
      FieldName = 'Net'
    end
    object CHECK_DATAYTDNet: TStringField
      FieldName = 'YTDNet'
    end
    object CHECK_DATATotalDirDep: TStringField
      FieldName = 'TotalDirDep'
    end
    object CHECK_DATANetAndDirDep: TStringField
      FieldName = 'NetAndDirDep'
    end
    object CHECK_DATAPeriodBeginDate: TStringField
      FieldName = 'PeriodBeginDate'
    end
    object CHECK_DATAPeriodEndDate: TStringField
      FieldName = 'PeriodEndDate'
    end
    object CHECK_DATACheckDate: TStringField
      FieldName = 'CheckDate'
    end
    object CHECK_DATACompanyName: TStringField
      FieldName = 'CompanyName'
      Size = 40
    end
    object CHECK_DATACompanyNumber: TStringField
      FieldName = 'CompanyNumber'
    end
    object CHECK_DATACheckComments: TMemoField
      FieldName = 'CheckComments'
      BlobType = ftMemo
    end
    object CHECK_DATAMICRHorizontalAdjustment: TIntegerField
      FieldName = 'MICRHorizontalAdjustment'
    end
    object CHECK_DATALogo_Nbr: TIntegerField
      FieldName = 'Logo_Nbr'
    end
    object CHECK_DATASignature_Nbr: TIntegerField
      FieldName = 'Signature_Nbr'
    end
    object CHECK_DATASignature_Db: TStringField
      FieldName = 'Signature_Db'
      Size = 1
    end
    object CHECK_DATAOBCCheckMessage: TStringField
      FieldName = 'OBCCheckMessage'
      Size = 255
    end
    object CHECK_DATAEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
  end
  object CHECK_STUB_DATA: TevClientDataSet
    FieldDefs = <
      item
        Name = 'StubLineNbr'
        DataType = ftInteger
      end
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'EarningCode'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EarningDesc'
        DataType = ftString
        Size = 18
      end
      item
        Name = 'Location'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Rate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Hours'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'EarningCurrent'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EarningYTD'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DeductionDesc'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'DeductionCurrent'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DeductionYTD'
        DataType = ftString
        Size = 20
      end>
    Left = 136
    Top = 48
    object CHECK_STUB_DATAStubLineNbr: TIntegerField
      FieldName = 'StubLineNbr'
    end
    object CHECK_STUB_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object CHECK_STUB_DATAEarningCode: TStringField
      FieldName = 'EarningCode'
    end
    object CHECK_STUB_DATAEarningDesc: TStringField
      FieldName = 'EarningDesc'
      Size = 18
    end
    object CHECK_STUB_DATALocation: TStringField
      FieldName = 'Location'
      Size = 40
    end
    object CHECK_STUB_DATARate: TStringField
      FieldName = 'Rate'
      Size = 10
    end
    object CHECK_STUB_DATAHours: TStringField
      FieldName = 'Hours'
      Size = 10
    end
    object CHECK_STUB_DATAEarningCurrent: TStringField
      FieldName = 'EarningCurrent'
    end
    object CHECK_STUB_DATAEarningYTD: TStringField
      FieldName = 'EarningYTD'
    end
    object CHECK_STUB_DATADeductionDesc: TStringField
      FieldName = 'DeductionDesc'
      Size = 30
    end
    object CHECK_STUB_DATADeductionCurrent: TStringField
      FieldName = 'DeductionCurrent'
    end
    object CHECK_STUB_DATADeductionYTD: TStringField
      FieldName = 'DeductionYTD'
    end
    object CHECK_STUB_DATAPunchIn: TStringField
      FieldName = 'PunchIn'
      Size = 22
    end
    object CHECK_STUB_DATAPunchOut: TStringField
      FieldName = 'PunchOut'
      Size = 22
    end
  end
  object wwcsChecks: TevClientDataSet
    Left = 40
    Top = 136
  end
  object FEDERAL_YTD: TevClientDataSet
    Left = 94
    Top = 122
  end
  object STATE_YTD: TevClientDataSet
    Left = 150
    Top = 138
    object STATE_YTDEE_STATES_NBR: TIntegerField
      FieldName = 'EE_STATES_NBR'
    end
    object STATE_YTDSTATE_TAX: TCurrencyField
      FieldName = 'STATE_TAX'
    end
    object STATE_YTDSTATE_TAXABLE_WAGES: TCurrencyField
      FieldName = 'STATE_TAXABLE_WAGES'
    end
    object STATE_YTDEE_SDI_TAX: TCurrencyField
      FieldName = 'EE_SDI_TAX'
    end
    object STATE_YTDEE_SDI_TAXABLE_WAGES: TCurrencyField
      FieldName = 'EE_SDI_TAXABLE_WAGES'
    end
  end
  object SUI_YTD: TevClientDataSet
    Left = 206
    Top = 122
    object SUI_YTDEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object SUI_YTDCO_SUI_NBR: TIntegerField
      FieldName = 'CO_SUI_NBR'
    end
    object SUI_YTDSUI_TAX: TCurrencyField
      FieldName = 'SUI_TAX'
    end
    object SUI_YTDSUI_TAXABLE_WAGES: TCurrencyField
      FieldName = 'SUI_TAXABLE_WAGES'
    end
  end
  object LOCAL_YTD: TevClientDataSet
    Left = 262
    Top = 138
    object LOCAL_YTDEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object LOCAL_YTDLOCAL_TAX: TCurrencyField
      FieldName = 'LOCAL_TAX'
    end
    object LOCAL_YTDLOCAL_TAXABLE_WAGE: TCurrencyField
      FieldName = 'LOCAL_TAXABLE_WAGE'
    end
  end
  object CL_E_DS_YTD: TevClientDataSet
    Left = 310
    Top = 122
    object CL_E_DS_YTDEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object CL_E_DS_YTDCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object CL_E_DS_YTDCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object CL_E_DS_YTDYTD: TCurrencyField
      FieldName = 'YTD'
    end
  end
  object TIME_OFF_DATA: TevClientDataSet
    Left = 240
    Top = 48
    object TIME_OFF_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object TIME_OFF_DATATimeOffNbr: TIntegerField
      FieldName = 'TimeOffNbr'
    end
    object TIME_OFF_DATATimeOffLine: TStringField
      FieldName = 'TimeOffLine'
      Size = 60
    end
  end
  object cdTimeOff: TevClientDataSet
    Left = 400
    Top = 128
  end
  object SUIConsolidation: TevClientDataSet
    IndexFieldNames = 'SY_STATES_NBR'
    PictureMasks.Strings = (
      'SUI_TAX'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9'T'#9'F'
      
        'SUI_TAXABLE_WAGES'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1' +
        '[#]}'#9'T'#9'F'
      
        'YTD_SUI_TAX'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9'T' +
        #9'F')
    Left = 208
    Top = 208
    object SUIConsolidationSUI_TAX: TCurrencyField
      FieldName = 'SUI_TAX'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SUIConsolidationSUI_TAXABLE_WAGES: TCurrencyField
      FieldName = 'SUI_TAXABLE_WAGES'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SUIConsolidationYTD_SUI_TAX: TCurrencyField
      FieldName = 'YTD_SUI_TAX'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SUIConsolidationSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object SUIConsolidationSTATE: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'STATE'
      Size = 2
    end
  end
  object cdsCheckDataLite: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'YTDNet'
        DataType = ftFloat
      end
      item
        Name = 'BankLevel'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AccountNbr'
        DataType = ftInteger
      end
      item
        Name = 'DirDep'
        DataType = ftFloat
      end
      item
        Name = 'EE_NBR'
        DataType = ftInteger
      end>
    Left = 540
    Top = 48
    object IntegerField1: TIntegerField
      FieldName = 'CheckNbr'
    end
    object cdsCheckDataLiteYTDNet: TFloatField
      FieldName = 'YTDNet'
    end
    object cdsCheckDataLiteBankLevel: TStringField
      FieldName = 'BankLevel'
      Size = 1
    end
    object cdsCheckDataLiteAccountNbr: TIntegerField
      FieldName = 'AccountNbr'
    end
    object cdsCheckDataLiteDirDep: TFloatField
      FieldName = 'DirDep'
    end
    object cdsCheckDataLiteEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsCheckDataLiteVoucher: TIntegerField
      FieldName = 'Voucher'
    end
  end
  object cdsTOALite: TevClientDataSet
    Left = 540
    Top = 93
    object IntegerField2: TIntegerField
      FieldName = 'CheckNbr'
    end
    object StringField1: TStringField
      FieldName = 'TimeOffLine'
      Size = 60
    end
  end
  object cdsDeductionsLite: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 30
      end>
    Left = 540
    Top = 143
    object IntegerField3: TIntegerField
      FieldName = 'CheckNbr'
    end
    object StringField2: TStringField
      FieldName = 'Description'
      Size = 30
    end
    object cdsDeductionsLiteAmount: TFloatField
      FieldName = 'Amount'
    end
    object cdsDeductionsLiteYTD: TFloatField
      FieldName = 'YTD'
    end
  end
  object cdsEarningsLite: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'Amount'
        DataType = ftFloat
      end
      item
        Name = 'YTD'
        DataType = ftFloat
      end
      item
        Name = 'Hours'
        DataType = ftFloat
      end
      item
        Name = 'Rate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ClEDsNbr'
        DataType = ftInteger
      end>
    Left = 540
    Top = 193
    object IntegerField4: TIntegerField
      FieldName = 'CheckNbr'
    end
    object FloatField1: TFloatField
      FieldName = 'Amount'
    end
    object FloatField2: TFloatField
      FieldName = 'YTD'
    end
    object cdsEarningsLiteHours: TFloatField
      FieldName = 'Hours'
    end
    object cdsEarningsLiteRate: TStringField
      FieldName = 'Rate'
      Size = 10
    end
    object cdsEarningsLiteClEDsNbr: TIntegerField
      FieldName = 'ClEDsNbr'
    end
    object cdsEarningsLiteaType: TIntegerField
      FieldName = 'EType'
    end
    object cdsEarningsLiteLocation: TStringField
      FieldName = 'Location'
      Size = 30
    end
  end
end
