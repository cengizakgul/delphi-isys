object DM_MISC_CHECK: TDM_MISC_CHECK
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 322
  Top = 33
  Height = 540
  Width = 783
  object CHECK_DATA: TevClientDataSet
    Left = 56
    Top = 32
    object CHECK_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object CHECK_DATACompanyName: TStringField
      FieldName = 'CompanyName'
      Size = 40
    end
    object CHECK_DATACompanyAddress1: TStringField
      FieldName = 'CompanyAddress1'
      Size = 40
    end
    object CHECK_DATACompanyAddress2: TStringField
      FieldName = 'CompanyAddress2'
      Size = 40
    end
    object CHECK_DATACompanyAddress3: TStringField
      FieldName = 'CompanyAddress3'
      Size = 40
    end
    object CHECK_DATAEIN: TStringField
      FieldName = 'EIN'
    end
    object CHECK_DATABankName: TStringField
      FieldName = 'BankName'
      Size = 40
    end
    object CHECK_DATABankAddress: TStringField
      FieldName = 'BankAddress'
      Size = 60
    end
    object CHECK_DATABankTopABANumber: TStringField
      FieldName = 'BankTopABANumber'
    end
    object CHECK_DATABankBottomABANumber: TStringField
      FieldName = 'BankBottomABANumber'
    end
    object CHECK_DATACheckMessage: TStringField
      FieldName = 'CheckMessage'
      Size = 40
    end
    object CHECK_DATACheckNumberOrVoucher: TStringField
      FieldName = 'CheckNumberOrVoucher'
    end
    object CHECK_DATACheckAmount: TStringField
      FieldName = 'CheckAmount'
    end
    object CHECK_DATACheckAmountSpelled: TStringField
      DisplayWidth = 110
      FieldName = 'CheckAmountSpelled'
      Size = 110
    end
    object CHECK_DATACheckAmountSecurity: TStringField
      FieldName = 'CheckAmountSecurity'
    end
    object CHECK_DATACheckTotal: TStringField
      FieldName = 'CheckTotal'
    end
    object CHECK_DATAMICRLine: TStringField
      FieldName = 'MICRLine'
      Size = 50
    end
    object CHECK_DATAEntityName: TStringField
      FieldName = 'EntityName'
      Size = 40
    end
    object CHECK_DATAEntityAddress1: TStringField
      FieldName = 'EntityAddress1'
      Size = 40
    end
    object CHECK_DATAEntityAddress2: TStringField
      FieldName = 'EntityAddress2'
      Size = 40
    end
    object CHECK_DATAEntityAddress3: TStringField
      FieldName = 'EntityAddress3'
      Size = 40
    end
    object CHECK_DATACompanyNameAndNumber: TStringField
      FieldName = 'CompanyNameAndNumber'
      Size = 60
    end
    object CHECK_DATACheckName: TStringField
      DisplayWidth = 80
      FieldName = 'CheckName'
      Size = 80
    end
    object CHECK_DATACheckDate: TStringField
      FieldName = 'CheckDate'
    end
    object CHECK_DATAPaymentSerialNumber: TStringField
      FieldName = 'PaymentSerialNumber'
    end
    object CHECK_DATAStubDetails: TMemoField
      FieldName = 'StubDetails'
      BlobType = ftMemo
    end
    object CHECK_DATASignature: TBlobField
      FieldName = 'Signature'
    end
    object CHECK_DATAMICRHorizontalAdjustment: TIntegerField
      FieldName = 'MICRHorizontalAdjustment'
    end
    object CHECK_DATAOBCCheckMessage: TStringField
      FieldName = 'OBCCheckMessage'
      Size = 255
    end
    object CHECK_DATAMiscCheckMessage: TStringField
      FieldName = 'MiscCheckMessage'
      Size = 255
    end
    object CHECK_DATAAgencyMemo1: TStringField
      FieldName = 'AgencyMemo1'
      Size = 40
    end
    object CHECK_DATAAgencyMemo2: TStringField
      FieldName = 'AgencyMemo2'
      Size = 40
    end
  end
  object wwcsChecks: TevClientDataSet
    Left = 56
    Top = 88
  end
end
