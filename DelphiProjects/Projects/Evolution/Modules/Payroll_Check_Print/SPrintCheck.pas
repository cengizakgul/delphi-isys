// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPrintCheck;

interface

uses
  Classes, Forms, SysUtils, EvTypes, DB, Variants, EvBasicUtils, EvClientDataSet,
  EvDataAccessComponents, EvContext, EvExceptions, isBaseClasses, EvStreamUtils,
  SimpleXML;

type
  TCheckStubXmlConverter = class
    FBlobData: string;
    FxmlCheckStub: IXmlDocument;
    FdsCheck: TEvClientDataSet;
    FdsCheckLine: TEvClientDataSet;
    FdsCheckComments: TEvClientDataSet;
    FdsTOA: TEvClientDataSet;
  private
    procedure FillCheckLinesData;
    procedure AddTOAData(xmlLeftNoteNode: IXmlNode);
    procedure AddCheckCommentsData(xmlRightNoteNode: IXmlNode);
    procedure PutChildTextNodesToDS(const aNodeName: string; aDS: TDataset);
    procedure GetXmlCheckStubFromBlob;
    procedure PrepareDatasets;
    procedure FillDatasets;
  public
    constructor Create(const BlobData: string);
    // CheckStubXML - "packed" string from PR_CHECK.Override_Check_Notes blob
    destructor Destroy; override;

    function GetDatasetArray: variant;
  end;

procedure PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckType: TCheckType; CurrentTOA, PrintReports: Boolean; CheckForm: string;
  Preview: Boolean = False; CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
  HideBackground: Boolean = False; ForcePrintVoucher: Boolean = False);

function CheckStubBlobToDatasets(const BlobData: string): variant;

implementation

uses
  SPD_DM_REGULAR_CHECK, SPD_DM_MISC_CHECK, EvConsts, SDataStructure, EvUtils, SReportSettings,
  SFieldCodeValues, isZippingRoutines, EvCommonInterfaces, EvDataset;

procedure PrintTaxCoupon(const PrMiscCheckNbr, CouponSyReportNbr: Integer; Preview: Boolean);
var
  ReportParams: TrwReportParams;
  prv: TrwReportDestination;
begin
  ReportParams := TrwReportParams.Create;
  try
    ReportParams.Add('Companies', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger]));
    ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('CheckNbr', IntToStr(PrMiscCheckNbr));

    if Preview then
      prv := rdtPreview
    else
      prv := rdtPrinter;

    if DM_SYSTEM_MISC.SY_REPORTS.Locate('SY_REPORTS_NBR', CouponSyReportNbr, []) then
    begin
      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').AsInteger,
          CH_DATABASE_SYSTEM, ReportParams);
      finally
        ctx_RWLocalEngine.EndGroup(prv);
      end;
    end;

  finally
    ReportParams.Free;
  end;
end;

procedure PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckType: TCheckType; CurrentTOA, PrintReports: Boolean; CheckForm: string; Preview: Boolean = False;
  CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False; HideBackground: Boolean = False;
  ForcePrintVoucher: Boolean = False);
var
  ReportParams: TrwReportParams;
  prv: TrwReportDestination;
  DM_REGULAR_CHECK: TDM_REGULAR_CHECK;
  DM_MISC_CHECK: TDM_MISC_CHECK;
  v: Variant;
  PR_CHECK_LINES: TevClientDataSet;
  s: string;
  aPrCheckNbrs: Variant;
  i: integer;

  function PreparePrCheckNbrs(aCheckTable: string): boolean;
  var k: integer;
  begin
    Result := True;
    if Assigned(CheckNumbers) and (CheckNumbers.Count > 0) then
    begin
      aPrCheckNbrs := VarArrayCreate([0, CheckNumbers.Count - 1], varVariant);
      for k := 0 to CheckNumbers.Count - 1 do
        aPrCheckNbrs[k] := CheckNumbers[k];
    end
    else
      try
        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('COLUMNS', aCheckTable + '_NBR');
          SetMacro('TABLENAME', aCheckTable);
          if aCheckTable <> 'PR_MISCELLANEOUS_CHECKS' then
            SetMacro('Condition', 'PR_NBR=:PrNbr')
          else
            case CheckType of
              ctAgency:
                SetMacro('Condition', 'PR_NBR=:PrNbr and MISCELLANEOUS_CHECK_TYPE in (''' + MISC_CHECK_TYPE_AGENCY + ''', ''' + MISC_CHECK_TYPE_AGENCY_VOID + ''')');
              ctTax:
                SetMacro('Condition', 'PR_NBR=:PrNbr and MISCELLANEOUS_CHECK_TYPE in (''' + MISC_CHECK_TYPE_TAX + ''', ''' + MISC_CHECK_TYPE_TAX_VOID + ''')');
              ctBilling:
                SetMacro('Condition', 'PR_NBR=:PrNbr and MISCELLANEOUS_CHECK_TYPE in (''' + MISC_CHECK_TYPE_BILLING + ''', ''' + MISC_CHECK_TYPE_BILLING_VOID + ''')');
            end;
          SetParam('PrNbr', PayrollNbr);
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        end;
        ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
        Result := ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0;
        aPrCheckNbrs := VarArrayCreate([0, ctx_DataAccess.CUSTOM_VIEW.RecordCount - 1], varVariant);
        i := 0;
        ctx_DataAccess.CUSTOM_VIEW.First;
        while not ctx_DataAccess.CUSTOM_VIEW.eof do
        begin
          aPrCheckNbrs[i] := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsString;
          ctx_DataAccess.CUSTOM_VIEW.Next;
          Inc(i);
        end;
      finally
        ctx_DataAccess.CUSTOM_VIEW.Close;
      end;
  end;

  procedure PrintTaxCouponEx(aMiscCheckNbr: string);
  var
    TaxCouponReportNbr: integer;
  begin
    try
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('COLUMNS', 'TAX_COUPON_SY_REPORTS_NBR');
        SetMacro('TABLENAME', 'PR_MISCELLANEOUS_CHECKS');
        SetMacro('Condition', 'PR_MISCELLANEOUS_CHECKS_NBR=:Nbr');
        SetParam('Nbr', aMiscCheckNbr);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
      ctx_DataAccess.CUSTOM_VIEW.First;
      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsString = '' then
        TaxCouponReportNbr := -1
      else
        TaxCouponReportNbr := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger;
    finally
      ctx_DataAccess.CUSTOM_VIEW.Close;
    end;

    if TaxCouponReportNbr > -1 then
      PrintTaxCoupon( StrToInt(aMiscCheckNbr), TaxCouponReportNbr, Preview);
  end;

  function BureauCheck: boolean;
  begin
    Result := (CheckForm[1] = CHECK_FORM_BUREAU_DESIGNER_CHECK) or (CheckForm[1] = CHECK_FORM_BUREAU_NEW_CHECK);
  end;

  function OldCheckForm(aCheckForm: string): boolean;
  begin
    Result :=
      (Pos(aCheckForm, CHECK_FORM_REGULAR_NEW + CHECK_FORM_REGULAR_TOP_NEW + CHECK_FORM_LETTER_BTM_NEW + CHECK_FORM_LETTER_TOP_NEW +
                       CHECK_FORM_LETTER_BTM_SPANISH_NEW + CHECK_FORM_LETTER_BTM_RATE_NEW + CHECK_FORM_LETTER_BTM_FF_NEW + CHECK_FORM_LETTER_BTM_2_STUB_NEW +
                       CHECK_FORM_LETTER_BTM_WITH_ADDR_NEW + CHECK_FORM_PRESSURE_SEAL_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_NEW +
                       CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE_NEW + CHECK_FORM_PRESSURE_SEAL_GREATLAND_NEW +
                       CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD_NEW +
                       CHECK_FORM_PRESSURE_SEAL_LEGAL_FF_NEW + CHECK_FORM_LETTER_BTM_STUB_ONLY_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW +
                       CHECK_FORM_LETTER_BTM_STUB_PUNCH+CHECK_FORM_PRESSURE_SEAL_LEGAL_ANSI_NEW) = 0)
      and not BureauCheck;
  end;

  function CheckDesignerLicenseValid: boolean;
  var
    Key: IisListOfValues;
  begin
    Key := Context.License.FindAPIKey(tvkEvoCheckDesigner);
    Result := Assigned(Key);
  end;

begin
  DM_SYSTEM_MISC.SY_REPORTS.DataRequired('ALL');

  if BureauCheck then
  begin
    if (CheckForm[1] = CHECK_FORM_BUREAU_DESIGNER_CHECK) and not CheckDesignerLicenseValid then
      raise EevException.CreateHelp('Check Designer License is invalid', 0);

    s := Copy(CheckForm, 2, Length(CheckForm));
    if Trim(s) = '' then
    begin
      if CheckType = ctRegular then
        s := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 65, 6)
      else
        s := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 71, 6);
      // get report Number, the first symbol is CHECK_FORM_BUREAU_...
      s := Copy(s, 2, Length(s));
    end;

    if TryStrToInt(s, i) then
      DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.DataRequired('SB_REPORT_WRITER_REPORTS_NBR=''' + s + '''')
    else
      i := 0;

    if (Trim(s) = '') or (i = 0) or (DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.RecordCount = 0) then
      raise EMissingReport.CreateHelp('SB Check Report (B' + s + ') does not exist in the database!', IDH_MissingReport);
  end
  else if (CheckType = ctRegular) then
    ctx_DataAccess.LocateCheckReport(CheckForm, True)
  else
    ctx_DataAccess.LocateCheckReport(CheckForm, False);

  if Preview then
    prv := rdtPreview
  else
    prv := rdtPrinter;

  if (CheckType = ctRegular)  then
  begin
    DM_EMPLOYEE.EE.LookupsEnabled := False;
    DM_EMPLOYEE.EE_RATES.LookupsEnabled := False;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.LookupsEnabled := False;
    DM_PAYROLL.PR.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := False;
    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.LookupsEnabled := False;

    ReportParams := TrwReportParams.Create;
    DM_REGULAR_CHECK := TDM_REGULAR_CHECK.Create(Nil);
    PR_CHECK_LINES := TevClientDataSet.Create(nil);
    try
      DM_PAYROLL.PR.DataRequired('PR_NBR = ' + IntToStr(PayrollNbr));
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('TableName', 'PR');
        SetMacro('Columns', 'PR_NBR');
        SetMacro('Condition', '(pr_nbr=:PrNbr or CO_NBR=:CoNbr and status in (''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + ''') and ' +
                 '(run_number>=:StartRN AND check_date=:StartDate OR check_date>:StartDate) AND ' +
                 '(run_number<=:EndRN AND check_date=:EndDate OR check_date<:EndDate))');
        SetParam('StartDate', GetBeginYear(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime));
        SetParam('EndDate', DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime);
        SetParam('StartRN', 1);
        SetParam('EndRN', DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsInteger);
        SetParam('CoNbr', DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger);
        SetParam('PrNbr', PayrollNbr);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;
      Assert(ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0);

      s := '';
      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        s := s + ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_NBR').AsString + ',';
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
      Delete(s, Length(s), 1);

      PR_CHECK_LINES.ProviderName := 'PR_CHECK_LINES_PROV';
      DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR = ' + IntToStr(PayrollNbr));
      PR_CHECK_LINES.DataRequired('PR_NBR in (' + s + ')');
      DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_NBR in (' + s + ')');
      DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_NBR in (' + s + ')');
      DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_NBR in (' + s + ')');
      DM_CLIENT.CL_E_DS.DataRequired('ALL');


      case CheckForm[1] of
      CHECK_FORM_PRESSURE_SEAL, CHECK_FORM_LETTER_BTM_SPANISH:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 10;
      CHECK_FORM_PRESSURE_SEAL_LEGAL,
       CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW,
       CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 21;
      CHECK_FORM_REGULAR_TOP:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 11;
      CHECK_FORM_LETTER_BTM, CHECK_FORM_LETTER_BTM_STUB_ONLY, CHECK_FORM_LETTER_BTM_STUB_PUNCH:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 38;
      CHECK_FORM_LETTER_BTM_RATE:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 30;
      CHECK_FORM_LETTER_BTM_FF:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 38;
      CHECK_FORM_PRESSURE_SEAL_GREATLAND:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 9;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 20;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 15;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 15;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_FF:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 31;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 15;
      CHECK_FORM_LETTER_BTM_WITH_ADDR:
        DM_REGULAR_CHECK.CheckStubLinesNbr := 14;
      else
        DM_REGULAR_CHECK.CheckStubLinesNbr := 11;
      end;

      if OldCheckForm(CheckForm) then
        DM_REGULAR_CHECK.PrintChecks(PayrollNbr, CheckNumbers, CheckForm, PR_CHECK_LINES,
          CurrentTOA, CheckDate, SkipCheckStubCheck, DontPrintBankInfo, ForcePrintVoucher)

      else
        DM_REGULAR_CHECK.PrintChecks(PayrollNbr, CheckNumbers, CheckForm, PR_CHECK_LINES,
          CurrentTOA, CheckDate, SkipCheckStubCheck, DontPrintBankInfo, ForcePrintVoucher, True);

      DM_PAYROLL.PR_CHECK.Close;
      DM_PAYROLL.PR_CHECK_LINES.Close;
      DM_PAYROLL.PR_CHECK_STATES.Close;
      DM_PAYROLL.PR_CHECK_SUI.Close;
      DM_PAYROLL.PR_CHECK_LOCALS.Close;
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Close;

      if PrintReports and ((DM_REGULAR_CHECK.CHECK_DATA.RecordCount > 0) or (DM_REGULAR_CHECK.cdsCheckDataLite.RecordCount > 0)) then
      begin
        if (DM_REGULAR_CHECK.CHECK_DATA.RecordCount > 0) then
        begin
          v := DataSetsToVarArray([DM_REGULAR_CHECK.CHECK_DATA, DM_REGULAR_CHECK.CHECK_STUB_DATA, DM_REGULAR_CHECK.TIME_OFF_DATA]);
          DM_REGULAR_CHECK.CHECK_DATA.Close;
          DM_REGULAR_CHECK.CHECK_STUB_DATA.Close;
          DM_REGULAR_CHECK.TIME_OFF_DATA.Close;
        end
        else if (DM_REGULAR_CHECK.cdsCheckDataLite.RecordCount > 0) then
        begin
          v := DataSetsToVarArray([DM_REGULAR_CHECK.cdsCheckDataLite, DM_REGULAR_CHECK.cdsTOALite, DM_REGULAR_CHECK.cdsEarningsLite, DM_REGULAR_CHECK.cdsDeductionsLite]);
          DM_REGULAR_CHECK.cdsCheckDataLite.Close;
          DM_REGULAR_CHECK.cdsTOALite.Close;
          DM_REGULAR_CHECK.cdsEarningsLite.Close;
          DM_REGULAR_CHECK.cdsDeductionsLite.Close;
          ReportParams.Add('DontPrintBankInfo', DontPrintBankInfo);
          ReportParams.Add('CheckDate', CheckDate);
        end;
        ReportParams.Add('DataSets', v);
        ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));
        ReportParams.Add('HideBackground', HideBackground);
        v := null;
        ctx_RWLocalEngine.StartGroup;
        try
          if BureauCheck then
            ctx_RWLocalEngine.CalcPrintReport(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').Value,
              CH_DATABASE_SERVICE_BUREAU, ReportParams)
          else
            ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
              CH_DATABASE_SYSTEM, ReportParams);
          ReportParams.Clear
        finally
          ctx_RWLocalEngine.EndGroup(prv);
        end;
      end;

    finally
      PR_CHECK_LINES.Free;
      ReportParams.Free;
      DM_REGULAR_CHECK.Free;
      DM_EMPLOYEE.EE.LookupsEnabled := True;
      DM_EMPLOYEE.EE_RATES.LookupsEnabled := True;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.LookupsEnabled := True;
      DM_PAYROLL.PR.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := True;
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.LookupsEnabled := True;
    end;
  end

  // new misc checks calculated in RW
  else if ((CheckForm[1] = CHECK_FORM_BUREAU_DESIGNER_CHECK) or (CheckForm[1] = CHECK_FORM_BUREAU_NEW_CHECK) or (MiscCheckReportNbrs[CheckForm[1]] >= MiscCheckReportNbrs[CHECK_FORM_REGULAR_NEW]))
    and PreparePrCheckNbrs('PR_MISCELLANEOUS_CHECKS') then
  begin
    ReportParams := TrwReportParams.Create;
    try
      ReportParams.Add('CheckNbrs', aPrCheckNbrs);
      ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));
      ReportParams.Add('DontPrintBankInfo', DontPrintBankInfo);
      ReportParams.Add('HideBackground', HideBackground);
      ReportParams.Add('CheckDate', CheckDate);
      ctx_RWLocalEngine.StartGroup;
      try
        if BureauCheck then
          ctx_RWLocalEngine.CalcPrintReport(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').Value,
            CH_DATABASE_SERVICE_BUREAU, ReportParams)
        else
          ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
            CH_DATABASE_SYSTEM, ReportParams).OverrideVmrMiscTaxCheckType := CheckType;
      finally
        ctx_RWLocalEngine.EndGroup(prv, '', DM_COMPANY.CO.CO_NBR.AsInteger, DM_COMPANY.PR.PR_NBR.AsInteger);
      end;

      if CheckType = ctTax then // printing coupons
      for i := VarArrayLowBound(aPrCheckNbrs, 1) to VarArrayHighBound(aPrCheckNbrs, 1) do
        PrintTaxCouponEx(aPrCheckNbrs[i]);
    finally
      ReportParams.Free;
    end;
  end
  else
  begin
    DM_MISC_CHECK := TDM_MISC_CHECK.Create(Nil);

    ReportParams := TrwReportParams.Create;
    try
      case CheckForm[1] of
      CHECK_FORM_PRESSURE_SEAL:
        DM_MISC_CHECK.CheckStubLinesNbr := 12;
      CHECK_FORM_PRESSURE_SEAL_LEGAL,
       CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW,
       CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT:
        DM_MISC_CHECK.CheckStubLinesNbr := 20;
      CHECK_FORM_REGULAR_TOP:
        DM_MISC_CHECK.CheckStubLinesNbr := 11;
      CHECK_FORM_LETTER_BTM:
        DM_MISC_CHECK.CheckStubLinesNbr := 44;
      CHECK_FORM_LETTER_BTM_FF:
        DM_MISC_CHECK.CheckStubLinesNbr := 42;
      CHECK_FORM_PRESSURE_SEAL_GREATLAND:
        DM_MISC_CHECK.CheckStubLinesNbr := 9;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE:
        DM_MISC_CHECK.CheckStubLinesNbr := 19;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND:
        DM_MISC_CHECK.CheckStubLinesNbr := 18;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL:
        DM_MISC_CHECK.CheckStubLinesNbr := 18;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_FF:
        DM_MISC_CHECK.CheckStubLinesNbr := 30;
      CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD:
        DM_MISC_CHECK.CheckStubLinesNbr := 18;
      else
        DM_MISC_CHECK.CheckStubLinesNbr := 12;
      end;
      case CheckType of
        ctAgency:
          DM_MISC_CHECK.PrintAgencyChecks(PayrollNbr, CheckNumbers, CheckDate);
        ctTax:
          DM_MISC_CHECK.PrintTaxChecks(PayrollNbr, CheckNumbers, CheckDate);
        ctBilling:
          DM_MISC_CHECK.PrintBillingChecks(PayrollNbr, CheckNumbers, CheckDate);
      end;

      ReportParams.Add('DataSets', DataSetsToVarArray([DM_MISC_CHECK.CHECK_DATA]));

      if DM_MISC_CHECK.CHECK_DATA.RecordCount > 0 then
      begin
        ctx_RWLocalEngine.StartGroup;
        try
          ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
            CH_DATABASE_SYSTEM, ReportParams).OverrideVmrMiscTaxCheckType := CheckType;
        finally
          ctx_RWLocalEngine.EndGroup(prv, '', DM_COMPANY.CO.CO_NBR.AsInteger, DM_COMPANY.PR.PR_NBR.AsInteger);
        end;
      end;

      case CheckType of // printing coupons
        ctTax:
          with DM_MISC_CHECK.wwcsChecks do
          begin
            First;
            while not Eof do
            begin
              if not VarIsNull(FieldValues['TAX_COUPON_SY_REPORTS_NBR']) then
                PrintTaxCoupon(FieldValues['PR_MISCELLANEOUS_CHECKS_NBR'], FieldValues['TAX_COUPON_SY_REPORTS_NBR'], Preview);
              Next;
            end;
          end;
      end;
    finally
      ReportParams.Free;
      DM_MISC_CHECK.Free;
    end;
  end;
end;

function CheckStubBlobToDatasets(const BlobData: string): variant;
var
  cnv: TCheckStubXmlConverter;
begin
  cnv := TCheckStubXmlConverter.Create(BlobData);
  try
    Result := cnv.GetDatasetArray;
  finally
    FreeAndNil(cnv);
  end;
end;

{ TCheckStubXmlConverter }

constructor TCheckStubXmlConverter.Create(const BlobData: string);
begin
  FBlobData := BlobData;
  FdsCheck := TEvClientDataSet.Create(nil);
  FdsCheckLine := TEvClientDataSet.Create(nil);
  FdsCheckComments := TEvClientDataSet.Create(nil);
  FdsTOA := TEvClientDataSet.Create(nil);

  PrepareDatasets;
end;

destructor TCheckStubXmlConverter.Destroy;
begin
  FdsCheck.Close;
  FdsCheckLine.Close;
  FdsTOA.Close;
  FdsCheckComments.Close;

  FreeAndNil(FdsCheck);
  FreeAndNil(FdsCheckLine);
  FreeAndNil(FdsCheckComments);
  FreeAndNil(FdsTOA);
  inherited;
end;

procedure TCheckStubXmlConverter.AddCheckCommentsData(xmlRightNoteNode: IXmlNode);
begin
  FdsCheckComments.Append;
  try
    FdsCheckComments.FieldByName(sRightNote).Value :=  xmlRightNoteNode.TypedValue;
    FdsCheckComments.Post;
  except
    FdsCheckComments.Cancel;
  end;
end;

procedure TCheckStubXmlConverter.FillCheckLinesData;
var
  i, j: integer;
  xmlNode, xmlCheckLine: IXmlNode;
begin
  xmlNode := FxmlCheckStub.DocumentElement.SelectSingleNode(sLines);
  Assert(Assigned(xmlNode), 'Tag <' + sLines + '> not found!');

  for i := 0 to xmlNode.ChildNodes.Count - 1 do
  begin
    xmlCheckLine := xmlNode.ChildNodes.Item[i];
    if (xmlCheckLine.ItemType = NODE_ELEMENT) and (xmlCheckLine.NodeName = sCheckLine) then
    begin
      FdsCheckLine.Append;
      try
        for j := 0 to xmlCheckLine.ChildNodes.Count - 1 do
          if (xmlCheckLine.ChildNodes.Item[j].ItemType = NODE_ELEMENT) then
          begin
            if (xmlCheckLine.ChildNodes.Item[j].ChildNodes.Count = 1) and (xmlCheckLine.ChildNodes.Item[j].ChildNodes.Item[0].ItemType = NODE_TEXT) then
              FdsCheckLine.FieldByName( xmlCheckLine.ChildNodes.Item[j].NodeName ).Value := xmlCheckLine.ChildNodes.Item[j].TypedValue;
          end;
        FdsCheckLine.Post;    
      except
        FdsCheckLine.Cancel;
      end;
    end;
  end;
end;

procedure TCheckStubXmlConverter.FillDatasets;
begin
  if Assigned(FxmlCheckStub) then
  begin
    // fill check data
    FdsCheck.Append;
    try
      // load data from <Title>
      PutChildTextNodesToDS(sTitle, FdsCheck);
      // load data from <Details>
      PutChildTextNodesToDS(sDetails, FdsCheck);
      // load data from <Lines>
      PutChildTextNodesToDS(sLines, FdsCheck);

      FdsCheck.Post;
    except
      FdsCheck.Cancel;
    end;

    FillCheckLinesData;
  end;
end;

procedure TCheckStubXmlConverter.GetXmlCheckStubFromBlob;
var
  xmlStream, packedStream: IisStream;
begin
  xmlStream := TisStream.Create;
  packedStream := TisStream.Create;
  packedStream.SetAsString( FBlobData );
  InflateStream( packedStream.RealStream, xmlStream.RealStream );
  FxmlCheckStub := LoadXmlDocumentFromXML( xmlStream.AsString );
end;

procedure TCheckStubXmlConverter.PrepareDatasets;
begin
  FdsCheck.FieldDefs.Add(sEmployeeName, ftString, 50 );
  FdsCheck.FieldDefs.Add(sCompanyName, ftString, 50 );
  FdsCheck.FieldDefs.Add(sCompanyName1, ftString, 50 );
  FdsCheck.FieldDefs.Add(sAddress1, ftString, 50 );
  FdsCheck.FieldDefs.Add(sAddress2, ftString, 50 );
  FdsCheck.FieldDefs.Add(sCity, ftString, 20 );
  FdsCheck.FieldDefs.Add(sState, ftString, 2 );
  FdsCheck.FieldDefs.Add(sZipCode, ftString, 10 );
  FdsCheck.FieldDefs.Add(sPhone1, ftString, 15 );
  FdsCheck.FieldDefs.Add(sCompanyNumber, ftString, 25 );
  FdsCheck.FieldDefs.Add(sEmployeeNumber, ftString, 25 );
  FdsCheck.FieldDefs.Add(sHireDate, ftString, 10 );
  FdsCheck.FieldDefs.Add(sPeriodBegin, ftString, 10 );
  FdsCheck.FieldDefs.Add(sPeriodEnd, ftString, 10 );
  FdsCheck.FieldDefs.Add(sCheckDate, ftString, 10 );
  FdsCheck.FieldDefs.Add(sCheckNumber, ftString, 20 );
  FdsCheck.FieldDefs.Add(sDivision, ftString, 20 );
  FdsCheck.FieldDefs.Add(sBranch, ftString, 20 );
  FdsCheck.FieldDefs.Add(sDepartment, ftString, 20 );
  FdsCheck.FieldDefs.Add(sTeam, ftString, 20 );
  FdsCheck.FieldDefs.Add(sEmployeeSSN, ftString, 11 );
  FdsCheck.FieldDefs.Add(sTotalEarnHours, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalEarnCurrent, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalEarnYTD, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalDedCurrent, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalDedYTD, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalNetPay, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalDirDep, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalCheckAmtCurrent, ftString, 15 );
  FdsCheck.FieldDefs.Add(sTotalCheckAmtYTD, ftString, 15 );
  FdsCheck.CreateDataSet;

  FdsCheckLine.FieldDefs.Add(sEarnDescription, ftString, 50 );
  FdsCheckLine.FieldDefs.Add(sEarnLocation, ftString, 50 );
  FdsCheckLine.FieldDefs.Add(sEarnRate, ftString, 15 );
  FdsCheckLine.FieldDefs.Add(sEarnHours, ftString, 15 );
  FdsCheckLine.FieldDefs.Add(sEarnCurrent, ftString, 15 );
  FdsCheckLine.FieldDefs.Add(sEarnPunchIn, ftString, 25 );
  FdsCheckLine.FieldDefs.Add(sEarnPunchOut, ftString, 25 );
  FdsCheckLine.FieldDefs.Add(sEarnYTD, ftString, 15 );
  FdsCheckLine.FieldDefs.Add(sDedDescription, ftString, 50 );
  FdsCheckLine.FieldDefs.Add(sDedCurrent, ftString, 15 );
  FdsCheckLine.FieldDefs.Add(sDedYTD, ftString, 15 );
  FdsCheckLine.CreateDataSet;

//  FdsCheckComments.FieldDefs.Add(sRightNote, ftString, 50 ); // SYS-794 - cropping of check comments
  FdsCheckComments.FieldDefs.Add(sRightNote, ftMemo, 50 );
  FdsCheckComments.CreateDataSet;

  FdsTOA.FieldDefs.Add(sLeftNote, ftString, 50 );
  FdsTOA.CreateDataSet;
end;

procedure TCheckStubXmlConverter.PutChildTextNodesToDS(
  const aNodeName: string; aDS: TDataset);
var
  i: integer;
  xmlNode: IXmlNode;
begin
  xmlNode := FxmlCheckStub.DocumentElement.SelectSingleNode(aNodeName);

  Assert(Assigned(xmlNode), 'Tag <' + aNodeName + '> not found!');
  for i := 0 to xmlNode.ChildNodes.Count - 1 do
    if (xmlNode.ChildNodes.Item[i].ItemType = NODE_ELEMENT) then
    begin
      if (xmlNode.ChildNodes.Item[i].ChildNodes.Count = 1) and (xmlNode.ChildNodes.Item[i].ChildNodes.Item[0].ItemType = NODE_TEXT) then
      begin
        if (Copy(xmlNode.ChildNodes.Item[i].NodeName, 1, Length(sLeftNote)) = sLeftNote) then
          AddTOAData(xmlNode.ChildNodes.Item[i])
        else if (Copy(xmlNode.ChildNodes.Item[i].NodeName, 1, Length(sRightNote)) = sRightNote) then
          AddCheckCommentsData(xmlNode.ChildNodes.Item[i])
        else
          aDS.FieldByName( xmlNode.ChildNodes.Item[i].NodeName ).Value := xmlNode.ChildNodes.Item[i].TypedValue;
      end;
    end;
end;

procedure TCheckStubXmlConverter.AddTOAData(xmlLeftNoteNode: IXmlNode);
begin
  FdsTOA.Append;
  try
    FdsTOA.FieldByName(sLeftNote).Value :=  xmlLeftNoteNode.TypedValue;
    FdsTOA.Post;
  except
    FdsTOA.Cancel;
  end;
end;

function TCheckStubXmlConverter.GetDatasetArray: variant;
begin
  try
    GetXmlCheckStubFromBlob;
    FillDatasets;
    Result := DataSetsToVarArray([FdsCheck, FdsCheckLine, FdsTOA, FdsCheckComments]);
  except
    Result := null;
  end;
end;

end.

