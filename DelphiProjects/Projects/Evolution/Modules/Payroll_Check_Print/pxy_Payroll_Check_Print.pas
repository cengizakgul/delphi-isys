// Proxy Module "Payroll Check Printing"
unit pxy_Payroll_Check_Print;

interface

implementation

uses EvMainboard, EvCommonInterfaces, isBaseClasses, Classes, EvTypes, evRemoteMethods,
     EvTransportDatagrams, evProxy, EvTransportInterfaces, EvStreamUtils;

type
  TPrCheckPrintingProxy = class(TevProxyModule, IevPayrollCheckPrint)
  protected
    procedure PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckType: TCheckType;
      CurrentTOA, PrintReports: Boolean; CheckForm: string; Preview: Boolean = False;
      CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
      HideBackground: Boolean = False; ForcePrintVoucher: Boolean = False);
    function CheckStubBlobToDatasets(const BlobData: string): variant;
  end;

function GetPrCheckPrintingProxy: IevPayrollCheckPrint;
begin
  Result := TPrCheckPrintingProxy.Create;
end;


{ TPrCheckPrintingProxy }

procedure TPrCheckPrintingProxy.PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList;
  CheckType: TCheckType; CurrentTOA, PrintReports: Boolean; CheckForm: string; Preview: Boolean = False;
  CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
  HideBackground: Boolean = False; ForcePrintVoucher: Boolean = False);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_CHECK_PRINT_PRINTCHECKS);
  D.Params.AddValue('PayrollNbr', PayrollNbr);
  D.Params.AddValue('CheckNumbers.Text', CheckNumbers.Text);
  D.Params.AddValue('CheckType', Ord(CheckType));
  D.Params.AddValue('CurrentTOA', CurrentTOA);
  D.Params.AddValue('PrintReports', PrintReports);
  D.Params.AddValue('CheckForm', CheckForm);
  D.Params.AddValue('Preview', Preview);
  D.Params.AddValue('CheckDate', CheckDate);
  D.Params.AddValue('SkipCheckStubCheck', SkipCheckStubCheck);
  D.Params.AddValue('DontPrintBankInfo', DontPrintBankInfo);
  D.Params.AddValue('HideBackground', HideBackground);
  D.Params.AddValue('ForcePrintVoucher', ForcePrintVoucher);

  D := SendRequest(D);
  CheckNumbers.Text := D.Params.Value['CheckNumbers.Text'];
end;

function TPrCheckPrintingProxy.CheckStubBlobToDatasets(const BlobData: string): variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_CHECK_PRINT_CHECKSTUBBLOBTODATASETS);
  D.Params.AddValue('BlobData', BlobData);

  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPrCheckPrintingProxy, IevPayrollCheckPrint, 'Payroll Check Printing');


end.
