// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPrCheckPrintingMod;

interface

uses
  ComCtrls,  StdCtrls, Classes, isBaseClasses,
  SysUtils, DB, SPrintCheck, EvUtils, EvTypes, Graphics, EvCommonInterfaces,
  EvContext, EvMainboard, EvStreamUtils;

implementation

type
  TPrCheckPrinting = class(TisInterfacedObject, IevPayrollCheckPrint)
  protected
    procedure PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList; CheckType: TCheckType;
      CurrentTOA, PrintReports: Boolean; CheckForm: string; Preview: Boolean = False;
      CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
      HideBackground: Boolean = False; ForcePrintVoucher: Boolean = False);
    function CheckStubBlobToDatasets(const BlobData: string): variant;
  end;

function GetPrCheckPrinting: IevPayrollCheckPrint;
begin
  Result := TPrCheckPrinting.Create;
end;


{ TPrCheckPrinting }

procedure TPrCheckPrinting.PrintChecks(PayrollNbr: Integer; CheckNumbers: TStringList;
  CheckType: TCheckType; CurrentTOA, PrintReports: Boolean; CheckForm: string; Preview: Boolean = False;
  CheckDate: TDateTime = 0; SkipCheckStubCheck: Boolean = False; DontPrintBankInfo: Boolean = False;
  HideBackground: Boolean = False; ForcePrintVoucher: Boolean = False);
begin
  SPrintCheck.PrintChecks(PayrollNbr, CheckNumbers, CheckType, CurrentTOA, PrintReports,
    CheckForm, Preview, CheckDate, SkipCheckStubCheck, DontPrintBankInfo, HideBackground, ForcePrintVoucher);
end;

function TPrCheckPrinting.CheckStubBlobToDatasets(const BlobData: string): variant;
begin
  Result := SPrintCheck.CheckStubBlobToDatasets(BlobData);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPrCheckPrinting, IevPayrollCheckPrint, 'Payroll Check Printing');

end.
