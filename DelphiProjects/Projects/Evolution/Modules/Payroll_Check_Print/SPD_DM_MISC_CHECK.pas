// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_DM_MISC_CHECK;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvTypes, Variants, kbmMemTable, EvContext,
  ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, EvExceptions, EvUIComponents, EvClientDataSet;

type
  TMiscCheckType = (mcAgency, mcTax, mcBilling);

  TDM_MISC_CHECK = class(TDataModule)
    CHECK_DATA: TevClientDataSet;
    CHECK_DATACheckNbr: TIntegerField;
    CHECK_DATACompanyName: TStringField;
    CHECK_DATACompanyAddress1: TStringField;
    CHECK_DATACompanyAddress2: TStringField;
    CHECK_DATACompanyAddress3: TStringField;
    CHECK_DATABankName: TStringField;
    CHECK_DATABankAddress: TStringField;
    CHECK_DATABankTopABANumber: TStringField;
    CHECK_DATABankBottomABANumber: TStringField;
    CHECK_DATACheckMessage: TStringField;
    CHECK_DATACheckNumberOrVoucher: TStringField;
    CHECK_DATACheckAmount: TStringField;
    CHECK_DATACheckAmountSpelled: TStringField;
    CHECK_DATAEIN: TStringField;
    CHECK_DATACheckAmountSecurity: TStringField;
    CHECK_DATAMICRLine: TStringField;
    CHECK_DATASignature: TBlobField;
    CHECK_DATAEntityName: TStringField;
    CHECK_DATAEntityAddress1: TStringField;
    CHECK_DATAEntityAddress2: TStringField;
    CHECK_DATAEntityAddress3: TStringField;
    CHECK_DATACompanyNameAndNumber: TStringField;
    CHECK_DATACheckTotal: TStringField;
    CHECK_DATACheckName: TStringField;
    CHECK_DATAStubDetails: TMemoField;
    CHECK_DATACheckDate: TStringField;
    CHECK_DATAPaymentSerialNumber: TStringField;
    CHECK_DATAMICRHorizontalAdjustment: TIntegerField;
    wwcsChecks: TevClientDataSet;
    CHECK_DATAOBCCheckMessage: TStringField;
    CHECK_DATAMiscCheckMessage: TStringField;
    CHECK_DATAAgencyMemo1: TStringField;
    CHECK_DATAAgencyMemo2: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FCurrentCheckType: TMiscCheckType;
    FCheckStubLinesNbr: Integer;
    FCheckNbr: Integer;
    procedure PrepareReportData(DontPrintBankInfo: boolean; CheckDate: TDateTime = 0);
  public
    { Public declarations }
    property CheckStubLinesNbr: Integer read FCheckStubLinesNbr write FCheckStubLinesNbr;
    procedure PrintAgencyChecks(PayrollNbr: Integer; MiscCheckNumbers: TStringList; CheckDate: TDateTime = 0; DontPrintBankInfo: boolean = False);
    procedure PrintTaxChecks(PayrollNbr: Integer; MiscCheckNumbers: TStringList; CheckDate: TDateTime = 0; DontPrintBankInfo: boolean = False);
    procedure PrintBillingChecks(PayrollNbr: Integer; MiscCheckNumbers: TStringList; CheckDate: TDateTime = 0; DontPrintBankInfo: boolean = False);
  end;

implementation

uses
  EvConsts, SPrintingRoutines, EvUtils, SDataStructure;

{$R *.DFM}

procedure TDM_MISC_CHECK.DataModuleCreate(Sender: TObject);
begin
  FCheckNbr := 0;
  FCheckStubLinesNbr := 12;
  CHECK_DATA.CreateDataSet;
  DM_SERVICE_BUREAU.SB.Activate;
end;

procedure TDM_MISC_CHECK.PrintAgencyChecks(PayrollNbr: Integer; MiscCheckNumbers: TStringList; CheckDate: TDateTime = 0;
  DontPrintBankInfo: boolean = False);
var
  InStatement: string;
  I: Integer;
begin
  if (PayrollNbr = 0) and (MiscCheckNumbers = nil) then Exit;
  FCurrentCheckType := mcAgency;

  if ctx_DataAccess.CUSTOM_VIEW.Active then ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('AgencyChecks') do
  begin
    SetMacro('CheckType', '''' + MISC_CHECK_TYPE_AGENCY + ''', ''' + MISC_CHECK_TYPE_AGENCY_VOID + '''');
    SetMacro('Columns', 'P.CHECK_DATE, M.MISCELLANEOUS_CHECK_AMOUNT, M.PAYMENT_SERIAL_NUMBER, M.OVERRIDE_INFORMATION, ' +
      'M.PR_MISCELLANEOUS_CHECKS_NBR, A.SB_AGENCY_NBR, C.CUSTOM_COMPANY_NUMBER, C.NAME, C.ADDRESS1, C.ADDRESS2, C.CITY, ' +
      'C.STATE, C.ZIP_CODE, M.CUSTOM_PR_BANK_ACCT_NUMBER, M.MISCELLANEOUS_CHECK_TYPE, M.CL_AGENCY_NBR, M.ABA_NUMBER, A.PAYMENT_TYPE, A.CLIENT_AGENCY_CUSTOM_FIELD');
    if PayrollNbr = 0 then
    begin
      InStatement := '';
      for I := 0 to MiscCheckNumbers.Count - 2 do
        InStatement := InStatement + MiscCheckNumbers.Strings[I] + ', ';
      InStatement := InStatement + MiscCheckNumbers.Strings[MiscCheckNumbers.Count - 1];
      if InStatement = '' then Exit;
      SetMacro('Filter', 'M.PR_MISCELLANEOUS_CHECKS_NBR in ('+ InStatement +') and');
    end
    else
      SetMacro('Filter', 'P.PR_NBR='+IntToStr(PayrollNbr)+' and');

    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  wwcsChecks.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  if wwcsChecks.RecordCount = 0 then
    Exit;

  PrepareReportData(DontPrintBankInfo, CheckDate);
end;

procedure TDM_MISC_CHECK.PrintTaxChecks(PayrollNbr: Integer; MiscCheckNumbers: TStringList; CheckDate: TDateTime = 0;
  DontPrintBankInfo: boolean = False);
var
  InStatement: string;
  I: Integer;
begin
  if (PayrollNbr = 0) and (MiscCheckNumbers = nil) then Exit;
  FCurrentCheckType := mcTax;

  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('TaxChecks') do
  begin
    SetMacro('CheckType', '''' + MISC_CHECK_TYPE_TAX + ''', ''' + MISC_CHECK_TYPE_TAX_VOID + '''');
    SetMacro('Columns', 'P.CHECK_DATE, M.MISCELLANEOUS_CHECK_AMOUNT, M.PAYMENT_SERIAL_NUMBER, M.OVERRIDE_INFORMATION, ' +
      'M.SY_GLOBAL_AGENCY_NBR, M.PR_MISCELLANEOUS_CHECKS_NBR, C.CUSTOM_COMPANY_NUMBER, C.NAME, C.ADDRESS1, C.ADDRESS2, ' +
      'C.CITY, C.STATE, C.ZIP_CODE, M.CUSTOM_PR_BANK_ACCT_NUMBER, M.MISCELLANEOUS_CHECK_TYPE, M.ABA_NUMBER, M.SY_GL_AGENCY_FIELD_OFFICE_NBR, M.TAX_COUPON_SY_REPORTS_NBR');
    if PayrollNbr = 0 then
    begin
      InStatement := '';
      for I := 0 to MiscCheckNumbers.Count - 2 do
        InStatement := InStatement + MiscCheckNumbers.Strings[I] + ', ';
      InStatement := InStatement + MiscCheckNumbers.Strings[MiscCheckNumbers.Count - 1];
      if InStatement = '' then Exit;
      SetMacro('Filter', 'M.PR_MISCELLANEOUS_CHECKS_NBR in (' + InStatement + ') and M.MISCELLANEOUS_CHECK_AMOUNT>=0 and ');
    end
    else
      SetMacro('Filter', 'P.PR_NBR=' + IntToStr(PayrollNbr) + ' and M.MISCELLANEOUS_CHECK_AMOUNT>=0 and ');
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  wwcsChecks.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  if wwcsChecks.RecordCount = 0 then
    Exit;

  PrepareReportData(DontPrintBankInfo, CheckDate);
end;

procedure TDM_MISC_CHECK.PrintBillingChecks(PayrollNbr: Integer; MiscCheckNumbers: TStringList; CheckDate: TDateTime = 0;
  DontPrintBankInfo: boolean = False);
var
  InStatement: string;
  I: Integer;
begin
  if (PayrollNbr = 0) and (MiscCheckNumbers = nil) then Exit;
  FCurrentCheckType := mcBilling;


  if ctx_DataAccess.CUSTOM_VIEW.Active then ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('TaxChecks') do
  begin
    SetMacro('CheckType', '''' + MISC_CHECK_TYPE_BILLING + ''', ''' + MISC_CHECK_TYPE_BILLING_VOID + '''');
    SetMacro('Columns', 'P.CHECK_DATE, M.MISCELLANEOUS_CHECK_AMOUNT, M.PAYMENT_SERIAL_NUMBER, M.OVERRIDE_INFORMATION, ' +
      'M.SY_GLOBAL_AGENCY_NBR, M.PR_MISCELLANEOUS_CHECKS_NBR, C.CUSTOM_COMPANY_NUMBER, C.NAME, C.ADDRESS1, C.ADDRESS2, ' +
      'C.CITY, C.STATE, C.ZIP_CODE, M.CUSTOM_PR_BANK_ACCT_NUMBER, M.MISCELLANEOUS_CHECK_TYPE, M.ABA_NUMBER, P.PR_NBR, ' +
      'M.SY_GL_AGENCY_FIELD_OFFICE_NBR, M.EFTPS_TAX_TYPE');
    if PayrollNbr = 0 then
    begin
      InStatement := '';
      for I := 0 to MiscCheckNumbers.Count - 2 do
        InStatement := InStatement + MiscCheckNumbers.Strings[I] + ', ';
      InStatement := InStatement + MiscCheckNumbers.Strings[MiscCheckNumbers.Count - 1];
      if InStatement = '' then Exit;
      SetMacro('Filter', 'M.PR_MISCELLANEOUS_CHECKS_NBR in (' + InStatement + ') and (M.MISCELLANEOUS_CHECK_AMOUNT>=0 or (M.MISCELLANEOUS_CHECK_AMOUNT<0 and M.EFTPS_TAX_TYPE in (''T'',''R''))) and ');
    end
    else
      SetMacro('Filter', 'P.PR_NBR=' + IntToStr(PayrollNbr) + ' and (M.MISCELLANEOUS_CHECK_AMOUNT>=0 or (M.MISCELLANEOUS_CHECK_AMOUNT<0 and M.EFTPS_TAX_TYPE in (''T'',''R''))) and ');
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  wwcsChecks.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  if wwcsChecks.RecordCount = 0 then
    Exit;

  PrepareReportData(DontPrintBankInfo, CheckDate);
end;

procedure TDM_MISC_CHECK.PrepareReportData(DontPrintBankInfo: boolean; CheckDate: TDateTime = 0);
var
  SBBankNbr, GLAgencyNbr, GLAgencyFieldOfficeNbr, BankAcctNbr, MICRCheckNbrStart, MICRBankAcctStart: Integer;
  TempVar: Variant;
  Header, EIN, TempStr, BankAccountNumber, sAbaNumber: String;
  NetDirectDeposit, ReversePrinting, RefundCheck: Boolean;
  s: string;
 ODBBankCheck : String;
begin
  ReversePrinting := (DM_COMPANY.CO.FieldByName('REVERSE_CHECK_PRINTING').Value = 'Y');
  if ReversePrinting then
    wwcsChecks.Last
  else
    wwcsChecks.First;
  while not (wwcsChecks.EOF and not ReversePrinting or wwcsChecks.BOF and ReversePrinting) do
  begin
    Inc(FCheckNbr);
    CHECK_DATA.Insert;
    CHECK_DATA.FieldByName('CheckNbr').Value := FCheckNbr;
    CHECK_DATA.FieldByName('CheckDate').Value := iff(CheckDate = 0, wwcsChecks.FieldByName('CHECK_DATE').Value, CheckDate);
    if not DontPrintBankInfo then
      CHECK_DATA.FieldByName('PaymentSerialNumber').Value := wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').Value;
    CHECK_DATA.FieldByName('MICRHorizontalAdjustment').Value := DM_SERVICE_BUREAU.SB.FieldByName('MICR_HORIZONTAL_ADJUSTMENT').Value;

    RefundCheck := False;
    if (FCurrentCheckType = mcBilling) and (wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value < 0) then
      RefundCheck := True;

    NetDirectDeposit := False;
    if (FCurrentCheckType = mcAgency) and (wwcsChecks.FieldByName('PAYMENT_TYPE').Value = Payment_Type_DirectDeposit) then
      NetDirectDeposit := True;

    if (FCurrentCheckType = mcAgency) and
       ((Pos('S', wwcsChecks.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0) or
       (Pos('B', wwcsChecks.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0)) then
    begin
      with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
      begin
        SetMacro('Table1', 'PR_CHECK');
        SetMacro('Table2', 'PR_CHECK_LINES');
        SetMacro('Columns', 'Distinct t1.EE_NBR');
        SetMacro('JoinField', 'PR_CHECK');
        SetMacro('NBRFIELD', 'PR_MISCELLANEOUS_CHECKS');
        SetParam('RecordNbr', wwcsChecks.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsInteger);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;
      if ctx_DataAccess.CUSTOM_VIEW.RecordCount = 1 then
      begin
        DM_EMPLOYEE.EE.DataRequired();
        Assert(DM_EMPLOYEE.EE.Locate('EE_NBR', ctx_DataAccess.CUSTOM_VIEW['EE_NBR'], []));
        if (Pos('S', wwcsChecks.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0) then
          CHECK_DATA.FieldByName('MiscCheckMessage').AsString := DM_EMPLOYEE.EE['SOCIAL_SECURITY_NUMBER'] + ' ' + DM_EMPLOYEE.EE['Employee_Name_Calculate']
        else
          CHECK_DATA.FieldByName('MiscCheckMessage').AsString := DM_EMPLOYEE.EE['Employee_Name_Calculate'];
      end;
    end;

    if NetDirectDeposit then
      CHECK_DATA.FieldByName('CheckNumberOrVoucher').Value := 'Memo'
    else if not DontPrintBankInfo then
      CHECK_DATA.FieldByName('CheckNumberOrVoucher').Value := wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').Value
    else
      CHECK_DATA.FieldByName('CheckNumberOrVoucher').Value := '';

    if NetDirectDeposit then
      CHECK_DATA.FieldByName('CheckAmount').Value := '$************'
    else
    if RefundCheck then
      CHECK_DATA.FieldByName('CheckAmount').Value := '$' + PadStringLeft(FloatToStrF(Abs(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value), ffNumber, 15, 2), '*', 12)
    else
      CHECK_DATA.FieldByName('CheckAmount').Value := '$' + PadStringLeft(FloatToStrF(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value, ffNumber, 15, 2), '*', 12);

    if NetDirectDeposit then
      CHECK_DATA.FieldByName('CheckAmountSpelled').Value := GetNumberText(0)
    else
    if RefundCheck then
      CHECK_DATA.FieldByName('CheckAmountSpelled').Value := GetNumberText(Abs(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value))
    else
      CHECK_DATA.FieldByName('CheckAmountSpelled').Value := GetNumberText(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value);
    DM_CLIENT.CL.Activate;
    if ((DM_CLIENT.CL.FieldByName('SECURITY_FONT').AsString = 'Y') or (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value = 'Y')) and
    ((wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsInteger >= 0) or RefundCheck) and
    ((FCurrentCheckType <> mcAgency) or ((FCurrentCheckType = mcAgency) and not NetDirectDeposit)) then
      CHECK_DATA.FieldByName('CheckAmountSecurity').Value := ConvertAmountForSecurityFont(Abs(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value))
    else
      CHECK_DATA.FieldByName('CheckAmountSecurity').Value := '';

    ODBBankCheck := GetOBCBankAccountInfo;

    if NetDirectDeposit then
      CHECK_DATA.FieldByName('CheckMessage').Value := 'NON NEGOTIABLE'
//    else if DM_SERVICE_BUREAU.SB.AR_EXPORT_FORMAT.Value = OBC_BBT_MONEYGRAM then
//      CHECK_DATA.FieldByName('CheckMessage').Value := DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString
    else if (DM_SERVICE_BUREAU.SB.AR_EXPORT_FORMAT.Value = OBC_CA_UNION_BANK) then
    begin
      if (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') then
      begin
        CHECK_DATA.FieldByName('CheckMessage').Value := 'Void After 6 Months';
        if (Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString) <> '') and
          (Length(Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString)) <= 7) then
          CHECK_DATA.FieldByName('CheckMessage').Value := Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString) + ', ' + CHECK_DATA.FieldByName('CheckMessage').Value;
      end
      else CHECK_DATA.FieldByName('CheckMessage').Value := Trim(DM_COMPANY.CO.FieldByName('CHECK_MESSAGE').AsString);
    end
    else if (FCurrentCheckType = mcAgency) and (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') then
      if ODBBankCheck = OBC_DENVER_NONE then
        CHECK_DATA.FieldByName('CheckMessage').Value := 'OFFICIAL CHECK'
      else if ODBBankCheck = OBC_WELLS_CASHIERS then
        CHECK_DATA.FieldByName('CheckMessage').Value := 'CASHIERS CHECK'
      else
        CHECK_DATA.FieldByName('CheckMessage').Value := 'TELLER CHECK'
    else if (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value = 'Y') and (ODBBankCheck <> OBC_BBT_MONEYGRAM) then
      CHECK_DATA.FieldByName('CheckMessage').Value := 'VOID AFTER 60 DAYS'
    else
      CHECK_DATA.FieldByName('CheckMessage').Value := '';
    if not NetDirectDeposit and (FCurrentCheckType = mcAgency) and (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') then
    begin
      CHECK_DATA.FieldByName('OBCCheckMessage').Value := 'Issued by Integrated Payment Systems Inc. Englewood, CO 800-223-7520';
      if ODBBankCheck = OBC_WELLS_CASHIERS then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := ''
      else if ODBBankCheck = OBC_WELLS_FARGO_NONE then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := CHECK_DATA.FieldByName('OBCCheckMessage').Value + #13'Payable through Wells Fargo Bank Ltd., N.A. Los Angeles CA'
      else if ODBBankCheck = OBC_DENVER_NONE then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := CHECK_DATA.FieldByName('OBCCheckMessage').Value + #13'Wells Fargo, Los Angeles, California'
      else if ODBBankCheck = OBC_BBT_MONEYGRAM then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := ''
      else if ODBBankCheck = OBC_CA_UNION_BANK then
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := 'UNION BANK OF CALIFORNIA' +
          #13'7108 N. Fresno Street, Fresno, CA 93720'
      else
        CHECK_DATA.FieldByName('OBCCheckMessage').Value := CHECK_DATA.FieldByName('OBCCheckMessage').Value + ', To Citibank, NA., Buffalo, NY';
    end;

    if RefundCheck then
    begin
      if (FCurrentCheckType = mcBilling) and
         not DM_COMPANY.CO.SB_ACCOUNTANT_NBR.IsNull and
         (DM_COMPANY.CO.BILLING_CHECK_CPA.Value = GROUP_BOX_YES) then
      begin
        DM_SERVICE_BUREAU.SB_ACCOUNTANT.DataRequired('SB_ACCOUNTANT_NBR=' + DM_COMPANY.CO.SB_ACCOUNTANT_NBR.AsString);
        CHECK_DATA.FieldByName('CompanyName').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.NAME.AsString;
        CHECK_DATA.FieldByName('CompanyAddress1').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.ADDRESS1.AsString;
        CHECK_DATA.FieldByName('CompanyAddress3').Value := '';
        if DM_SERVICE_BUREAU.SB_ACCOUNTANT.ADDRESS2.Value = '' then
          CHECK_DATA.FieldByName('CompanyAddress2').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.CITY.AsString + ', ' + DM_SERVICE_BUREAU.SB_ACCOUNTANT.STATE.AsString + ' ' +
            DM_SERVICE_BUREAU.SB_ACCOUNTANT.ZIP_CODE.AsString
        else
        begin
          CHECK_DATA.FieldByName('CompanyAddress2').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.ADDRESS2.AsString;
          CHECK_DATA.FieldByName('CompanyAddress3').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.CITY.AsString + ', ' + DM_SERVICE_BUREAU.SB_ACCOUNTANT.STATE.AsString + ' ' +
            DM_SERVICE_BUREAU.SB_ACCOUNTANT.ZIP_CODE.AsString;
        end;
      end
      else
      begin
        CHECK_DATA.FieldByName('CompanyName').Value := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString;
        CHECK_DATA.FieldByName('CompanyAddress1').Value := DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS1').AsString;
        CHECK_DATA.FieldByName('CompanyAddress2').Value := DM_SERVICE_BUREAU.SB.FieldByName('CITY').Value;
        if CHECK_DATA.FieldByName('CompanyAddress2').Value <> '' then
          CHECK_DATA.FieldByName('CompanyAddress2').Value := CHECK_DATA.FieldByName('CompanyAddress2').Value + ', ';
        CHECK_DATA.FieldByName('CompanyAddress2').Value := CHECK_DATA.FieldByName('CompanyAddress2').Value + DM_SERVICE_BUREAU.SB.FieldByName('STATE').AsString + ' ' +
          DM_SERVICE_BUREAU.SB.FieldByName('ZIP_CODE').AsString;
        CHECK_DATA.FieldByName('CompanyAddress3').Value := '';
        TempVar := DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS2').Value;
        if ConvertNull(TempVar, '') <> '' then
        begin
          CHECK_DATA.FieldByName('CompanyAddress3').Value := CHECK_DATA.FieldByName('CompanyAddress2').Value;
          CHECK_DATA.FieldByName('CompanyAddress2').Value := TempVar;
        end;
      end;
    end
    else if (FCurrentCheckType = mcAgency) and (DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES) then
    begin
      CHECK_DATA.FieldByName('CompanyName').Value := DM_CLIENT.CL.NAME.Value;
      CHECK_DATA.FieldByName('CompanyAddress1').Value := DM_CLIENT.CL.ADDRESS1.Value;
      CHECK_DATA.FieldByName('CompanyAddress3').Value := '';
      if DM_CLIENT.CL.ADDRESS2.Value = '' then
        CHECK_DATA.FieldByName('CompanyAddress2').Value := DM_CLIENT.CL.CITY.AsString + ', ' + DM_CLIENT.CL.STATE.AsString + ' ' +
          DM_CLIENT.CL.ZIP_CODE.AsString
      else
      begin
        CHECK_DATA.FieldByName('CompanyAddress2').Value := DM_CLIENT.CL.ADDRESS2.AsString;
        CHECK_DATA.FieldByName('CompanyAddress3').Value := DM_CLIENT.CL.CITY.AsString + ', ' + DM_CLIENT.CL.STATE.AsString + ' ' +
          DM_CLIENT.CL.ZIP_CODE.AsString;
      end;
    end
    else
    begin
      CHECK_DATA.FieldByName('CompanyName').Value := wwcsChecks.FieldByName('NAME').Value;
      CHECK_DATA.FieldByName('CompanyAddress1').Value := wwcsChecks.FieldByName('ADDRESS1').Value;
      CHECK_DATA.FieldByName('CompanyAddress3').Value := '';
      if ConvertNull(wwcsChecks.FieldByName('ADDRESS2').Value, '') = '' then
        CHECK_DATA.FieldByName('CompanyAddress2').Value := wwcsChecks.FieldByName('CITY').AsString + ', ' + wwcsChecks.FieldByName('STATE').AsString + ' ' +
          wwcsChecks.FieldByName('ZIP_CODE').AsString
      else
      begin
        CHECK_DATA.FieldByName('CompanyAddress2').Value := wwcsChecks.FieldByName('ADDRESS2').AsString;
        CHECK_DATA.FieldByName('CompanyAddress3').Value := wwcsChecks.FieldByName('CITY').AsString + ', ' + wwcsChecks.FieldByName('STATE').AsString + ' ' +
          wwcsChecks.FieldByName('ZIP_CODE').AsString;
      end;
    end;

    DM_CLIENT.CL_BANK_ACCOUNT.Activate;
    DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');

    if FCurrentCheckType = mcAgency then
    begin
      DM_SERVICE_BUREAU.SB_AGENCY.Activate;
      CHECK_DATA.FieldByName('EntityName').Value := DM_SERVICE_BUREAU.SB_AGENCY.Lookup('SB_AGENCY_NBR', wwcsChecks.FieldByName('SB_AGENCY_NBR').Value, 'PRINT_NAME');
      CHECK_DATA.FieldByName('EntityAddress1').Value := ConvertNull(DM_SERVICE_BUREAU.SB_AGENCY.Lookup('SB_AGENCY_NBR', wwcsChecks.FieldByName('SB_AGENCY_NBR').Value,
        'ADDRESS1'), '');

      CHECK_DATA.FieldByName('EntityAddress2').Value := ConvertNull(DM_SERVICE_BUREAU.SB_AGENCY.Lookup('SB_AGENCY_NBR', wwcsChecks.FieldByName('SB_AGENCY_NBR').Value,
        'CITY'), '');
      if CHECK_DATA.FieldByName('EntityAddress2').Value <> '' then
        CHECK_DATA.FieldByName('EntityAddress2').Value := CHECK_DATA.FieldByName('EntityAddress2').Value + ', ';
      CHECK_DATA.FieldByName('EntityAddress2').Value := CHECK_DATA.FieldByName('EntityAddress2').Value + ConvertNull(DM_SERVICE_BUREAU.SB_AGENCY.Lookup('SB_AGENCY_NBR',
        wwcsChecks.FieldByName('SB_AGENCY_NBR').Value, 'STATE'), '')
        + ' ' + ConvertNull(DM_SERVICE_BUREAU.SB_AGENCY.Lookup('SB_AGENCY_NBR', wwcsChecks.FieldByName('SB_AGENCY_NBR').Value, 'ZIP_CODE'),
          '');
      CHECK_DATA.FieldByName('EntityAddress3').Value := '';
      TempVar := DM_SERVICE_BUREAU.SB_AGENCY.Lookup('SB_AGENCY_NBR', wwcsChecks.FieldByName('SB_AGENCY_NBR').Value, 'ADDRESS2');
      if ConvertNull(TempVar, '') <> '' then
      begin
        CHECK_DATA.FieldByName('EntityAddress3').Value := CHECK_DATA.FieldByName('EntityAddress2').Value;
        CHECK_DATA.FieldByName('EntityAddress2').Value := TempVar;
      end;
    end;

    if FCurrentCheckType = mcTax then
    begin
      if wwcsChecks.FieldByName('SY_GLOBAL_AGENCY_NBR').IsNull and wwcsChecks.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').IsNull then
      begin
        if DM_COMPANY.CO.FieldByName('TAX_SERVICE').Value = 'Y' then
        begin
          DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate; 
          SBBankNbr := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR',
            DM_COMPANY.CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').Value, 'SB_BANKS_NBR');
        end
        else
          SBBankNbr := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', DM_COMPANY.CO.FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value, 'SB_BANKS_NBR');

        CHECK_DATA.FieldByName('EntityName').Value := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'PRINT_NAME');
        CHECK_DATA.FieldByName('EntityAddress1').Value := '';
        CHECK_DATA.FieldByName('EntityAddress2').Value := '';
        CHECK_DATA.FieldByName('EntityAddress3').Value := '';
{        CHECK_DATA.FieldByName('EntityAddress1').Value := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ADDRESS1'), '');

        CHECK_DATA.FieldByName('EntityAddress2').Value := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'CITY'), '')
          + ', ' + ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'STATE'), '')
          + ' ' + ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ZIP_CODE'), '');
        CHECK_DATA.FieldByName('EntityAddress3').Value := '';
        TempVar := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ADDRESS2');
        if ConvertNull(TempVar, '') <> '' then
        begin
          CHECK_DATA.FieldByName('EntityAddress3').Value := CHECK_DATA.FieldByName('EntityAddress2').Value;
          CHECK_DATA.FieldByName('EntityAddress2').Value := TempVar;
        end;}
      end
      else
      begin
        DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Activate;
        DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Activate;

        if not wwcsChecks.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').IsNull then
        begin
          GLAgencyFieldOfficeNbr := wwcsChecks.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').Value;
          GLAgencyNbr := DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr,
            'SY_GLOBAL_AGENCY_NBR');
        end
        else
        begin
          GLAgencyNbr := wwcsChecks.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger;
          try
            GLAgencyFieldOfficeNbr := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', GLAgencyNbr,
              'TAX_PAYMENT_FIELD_OFFICE_NBR');
          except
            raise EInconsistentData.CreateHelp('Global agency ' + DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', GLAgencyNbr, 'AGENCY_NAME')
              + ' does not have a tax payment field office setup.', IDH_InconsistentData);
          end;
        end;

        if VarIsNull(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr,
          'ADDITIONAL_NAME')) then
        begin
          CHECK_DATA.FieldByName('EntityName').Value := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', GLAgencyNbr, 'AGENCY_NAME');
        end
        else
        begin
          CHECK_DATA.FieldByName('EntityName').Value := DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr,
            'ADDITIONAL_NAME');
        end;
        CHECK_DATA.FieldByName('EntityAddress1').Value := ConvertNull(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR',
          GLAgencyFieldOfficeNbr, 'ADDRESS1'), '');

        CHECK_DATA.FieldByName('EntityAddress2').Value := ConvertNull(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr, 'CITY'), '')
          + ', ' + ConvertNull(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr, 'STATE'), '')
          + ' ' + ConvertNull(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr, 'ZIP_CODE'), '');
        CHECK_DATA.FieldByName('EntityAddress3').Value := '';
        TempVar := DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Lookup('SY_GL_AGENCY_FIELD_OFFICE_NBR', GLAgencyFieldOfficeNbr, 'ADDRESS2');
        if ConvertNull(TempVar, '') <> '' then
        begin
          CHECK_DATA.FieldByName('EntityAddress3').Value := CHECK_DATA.FieldByName('EntityAddress2').Value;
          CHECK_DATA.FieldByName('EntityAddress2').Value := TempVar;
        end;
      end;
    end;

    if FCurrentCheckType = mcBilling then
    begin
      if RefundCheck then
      begin
        CHECK_DATA.FieldByName('EntityName').Value := wwcsChecks.FieldByName('NAME').Value;
        CHECK_DATA.FieldByName('EntityAddress1').Value := wwcsChecks.FieldByName('ADDRESS1').Value;
        CHECK_DATA.FieldByName('EntityAddress3').Value := '';
        if ConvertNull(wwcsChecks.FieldByName('ADDRESS2').Value, '') = '' then
          CHECK_DATA.FieldByName('EntityAddress2').Value := wwcsChecks.FieldByName('CITY').AsString + ', ' + wwcsChecks.FieldByName('STATE').AsString + ' ' +
            wwcsChecks.FieldByName('ZIP_CODE').AsString
        else
        begin
          CHECK_DATA.FieldByName('EntityAddress2').Value := wwcsChecks.FieldByName('ADDRESS2').AsString;
          CHECK_DATA.FieldByName('EntityAddress3').Value := wwcsChecks.FieldByName('CITY').AsString + ', ' + wwcsChecks.FieldByName('STATE').AsString + ' ' +
            wwcsChecks.FieldByName('ZIP_CODE').AsString;
        end;
      end
      else if not DM_COMPANY.CO.SB_ACCOUNTANT_NBR.IsNull and (DM_COMPANY.CO.BILLING_CHECK_CPA.Value = GROUP_BOX_YES) then
      begin
        DM_SERVICE_BUREAU.SB_ACCOUNTANT.DataRequired('SB_ACCOUNTANT_NBR=' + DM_COMPANY.CO.SB_ACCOUNTANT_NBR.AsString);
        CHECK_DATA.FieldByName('EntityName').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.NAME.AsString;
        CHECK_DATA.FieldByName('EntityAddress1').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.ADDRESS1.AsString;
        CHECK_DATA.FieldByName('EntityAddress3').Value := '';
        if DM_SERVICE_BUREAU.SB_ACCOUNTANT.ADDRESS2.Value = '' then
          CHECK_DATA.FieldByName('EntityAddress2').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.CITY.AsString + ', ' + DM_SERVICE_BUREAU.SB_ACCOUNTANT.STATE.AsString + ' ' +
            DM_SERVICE_BUREAU.SB_ACCOUNTANT.ZIP_CODE.AsString
        else
        begin
          CHECK_DATA.FieldByName('EntityAddress2').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.ADDRESS2.AsString;
          CHECK_DATA.FieldByName('EntityAddress3').Value := DM_SERVICE_BUREAU.SB_ACCOUNTANT.CITY.AsString + ', ' + DM_SERVICE_BUREAU.SB_ACCOUNTANT.STATE.AsString + ' ' +
            DM_SERVICE_BUREAU.SB_ACCOUNTANT.ZIP_CODE.AsString;
        end;
      end
      else
      begin
        CHECK_DATA.FieldByName('EntityName').Value := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString;
        CHECK_DATA.FieldByName('EntityAddress1').Value := DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS1').AsString;

        CHECK_DATA.FieldByName('EntityAddress2').Value := DM_SERVICE_BUREAU.SB.FieldByName('CITY').Value;
        if CHECK_DATA.FieldByName('EntityAddress2').Value <> '' then
          CHECK_DATA.FieldByName('EntityAddress2').Value := CHECK_DATA.FieldByName('EntityAddress2').Value + ', ';
        CHECK_DATA.FieldByName('EntityAddress2').Value := CHECK_DATA.FieldByName('EntityAddress2').Value + DM_SERVICE_BUREAU.SB.FieldByName('STATE').AsString + ' ' +
          DM_SERVICE_BUREAU.SB.FieldByName('ZIP_CODE').AsString;
        CHECK_DATA.FieldByName('EntityAddress3').Value := '';
        TempVar := DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS2').Value;
        if ConvertNull(TempVar, '') <> '' then
        begin
          CHECK_DATA.FieldByName('EntityAddress3').Value := CHECK_DATA.FieldByName('EntityAddress2').Value;
          CHECK_DATA.FieldByName('EntityAddress2').Value := TempVar;
        end;
      end;
    end;

    if ctx_PayrollCalculation.GetBankAccountNumberForMisc(wwcsChecks, BankAcctNbr) then
    begin
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
      SBBankNbr := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, 'SB_BANKS_NBR');
      // if check is voided then this account should go to Micr Line, ticket #50673
      BankAccountNumber := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
      if (not (NetDirectDeposit or DontPrintBankInfo)) and (DM_COMPANY.CO.FieldByName('REMOTE_OF_CLIENT').Value <> PRINT_SIGNATURE_ON_PAYROLL_CHECKS) then
      begin
        if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, []) then
          CHECK_DATA.UpdateBlobData('Signature', DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.GetBlobData('SIGNATURE_SB_BLOB_NBR'))
        else
          CHECK_DATA.FieldByName('Signature').Clear;
      end;
    end
    else
    begin
      SBBankNbr := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'SB_BANKS_NBR');
      BankAccountNumber := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
      if (not (NetDirectDeposit or DontPrintBankInfo)) and (DM_COMPANY.CO.FieldByName('REMOTE_OF_CLIENT').Value <> PRINT_SIGNATURE_ON_PAYROLL_CHECKS) then
      begin
        if DM_CLIENT.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', BankAcctNbr, []) then
          CHECK_DATA.UpdateBlobData('Signature', DM_CLIENT.CL_BANK_ACCOUNT.GetBlobData('SIGNATURE_CL_BLOB_NBR'))
        else
          CHECK_DATA.FieldByName('Signature').Clear;
      end;
    end;

    CHECK_DATA.FieldByName('CompanyNameAndNumber').Value := wwcsChecks.FieldByName('NAME').AsString + ' - ' + wwcsChecks.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
    if RefundCheck then
      CHECK_DATA.FieldByName('CheckTotal').Value := FloatToStrF(Abs(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsFloat), ffFixed, 15, 2)
    else
      CHECK_DATA.FieldByName('CheckTotal').Value := FloatToStrF(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsFloat, ffFixed, 15, 2);
    if not DontPrintBankInfo then
    begin
      CHECK_DATA.FieldByName('BankName').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'PRINT_NAME'));
      s := Trim(VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ADDRESS1')) + ' ' +
                VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ADDRESS2')));
      if s <> '' then
        s := s + ', ';
      s := s +
        VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'CITY')) + ', ' +
        VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'STATE')){ + ' ' +
        VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ZIP_CODE'))};
      CHECK_DATA.FieldByName('BankAddress').Value := s;
    end;

    if NetDirectDeposit or DontPrintBankInfo then
      CHECK_DATA.FieldByName('MICRLine').Value := ''
    else
    begin
      sAbaNumber := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ABA_NUMBER'));
      MICRCheckNbrStart := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'MICR_CHECK_NUMBER_START_POSITN'), 0);
      MICRBankAcctStart := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'MICR_ACCOUNT_START_POSITION'), 0);
      if Pos(wwcsChecks.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString, 'GXI') = 0 then
        // not voided check, show CUSTOM_PR_BANK_ACCT_NUMBER from check
        BankAccountNumber := wwcsChecks.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString;

      ODBBankCheck := GetOBCBankAccountInfo;

      if (FCurrentCheckType = mcAgency) and
         (DM_SERVICE_BUREAU.SB.AR_EXPORT_FORMAT.Value = OBC_BBT_MONEYGRAM) then
      begin
        if (DM_COMPANY.CO.FieldByName('TRUST_SERVICE').Value <> 'N') then
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '  >' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 8) + '>';
          TempStr := TempStr + ' |' + sAbaNumber + '|' + BankAccountNumber + '>';
        end
        else begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '  >' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 6) + '>';
          TempStr := TempStr + ' |' + sAbaNumber + '|' + BankAccountNumber + '>';
        end;
      end
      else if (FCurrentCheckType = mcAgency) and (DM_COMPANY.CO.FieldByName('OBC').Value = 'Y') and
         (ODBBankCheck <> OBC_WELLS_FARGO_NONE) and
         (ODBBankCheck <> OBC_WELLS_CASHIERS) and
         (ODBBankCheck <> OBC_BBT_MONEYGRAM) then
      begin
        if ODBBankCheck = OBC_CITIBANK_NONE then
        begin
          TempStr := StringOfChar(' ', MICRBankAcctStart) + '>' + BankAccountNumber + '>';
          TempStr := TempStr + ' |' + sAbaNumber + '|';
          TempStr := TempStr + StringOfChar(' ', MICRCheckNbrStart + 1) + '2500' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 9) + '9>'
        end
        else if ODBBankCheck = OBC_DENVER_NONE then
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 9) + '>';
          TempStr := TempStr + '   |' + sAbaNumber + '|4174175646>' + BankAccountNumber;
        end
        else if ODBBankCheck = OBC_CA_UNION_BANK then
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + ' >' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 10) + '>';
          TempStr := TempStr + ' |' + sAbaNumber + '|' + StringOfChar(' ', MICRBankAcctStart) + ' ' + BankAccountNumber + '>';
        end
        else
        begin
          TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 9) + '>';
          TempStr := TempStr + '   |' + sAbaNumber + '>' + BankAccountNumber;
        end
      end
      else begin
        TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(wwcsChecks.FieldByName('PAYMENT_SERIAL_NUMBER').AsString, '0', 6) + '>';
        TempStr := TempStr + ' |' + sAbaNumber + '|' + StringOfChar(' ', MICRBankAcctStart + 1) + BankAccountNumber + '>';
        if TempStr[Length(TempStr) - 1] = '~' then
          Delete(TempStr, Length(TempStr) - 1, 2);
      end;

      CHECK_DATA.FieldByName('MICRLine').Value := TempStr;
    end;

    if DontPrintBankInfo then
    begin
      CHECK_DATA.FieldByName('BankTopABANumber').Value := '';
      CHECK_DATA.FieldByName('BankBottomABANumber').Value := '';
    end
    else begin
      CHECK_DATA.FieldByName('BankTopABANumber').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'TOP_ABA_NUMBER'));
      CHECK_DATA.FieldByName('BankBottomABANumber').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'BOTTOM_ABA_NUMBER'));
    end;

    if not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Active or not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Locate('PR_MISCELLANEOUS_CHECKS_NBR',
    wwcsChecks.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value, []) then
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR=' + wwcsChecks.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);

    if FCurrentCheckType = mcTax then
    begin
      CHECK_DATA.FieldByName('StubDetails').Value := CreateTaxCheckStubDetails(Header, EIN);
      CHECK_DATA.FieldByName('EIN').Value := EIN;
    end
    else
      CHECK_DATA.FieldByName('EIN').Value := '';

    if FCurrentCheckType = mcBilling then
    begin
      DM_COMPANY.CO_BILLING_HISTORY.Activate;
      if DM_COMPANY.CO_BILLING_HISTORY.Locate('PR_NBR', wwcsChecks.FieldByName('PR_NBR').Value, []) then
        CHECK_DATA.FieldByName('StubDetails').Value := 'Invoice Date: ' + DateToStr(DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_DATE').AsDateTime)
          + #13#10 + 'Invoice #' + DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_NUMBER').AsString
      else
        CHECK_DATA.FieldByName('StubDetails').Value := 'No Invoice. Manual Billing Check.';
    end;

    if Trim(wwcsChecks.FieldByName('OVERRIDE_INFORMATION').AsString) <> '' then
      CHECK_DATA.FieldByName('StubDetails').Value := wwcsChecks.FieldByName('OVERRIDE_INFORMATION').AsString
    else
    begin
      if FCurrentCheckType = mcAgency then
        CHECK_DATA.FieldByName('StubDetails').Value := CreateAgencyCheckStubDetails(FCheckStubLinesNbr,
          (Pos('B', wwcsChecks.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').AsString) > 0) );  // 'B' means Block SSN
    end;

    if FCurrentCheckType = mcAgency then
      CHECK_DATA.FieldByName('CheckName').Value := 'Agency Check';
    if FCurrentCheckType = mcTax then
      CHECK_DATA.FieldByName('CheckName').Value := 'Tax Check          ' + Header;
    if FCurrentCheckType = mcBilling then
      CHECK_DATA.FieldByName('CheckName').Value := 'Billing Check';
    if RefundCheck then
      CHECK_DATA.FieldByName('CheckName').Value := 'Billing Refund Check';

    if FCurrentCheckType = mcAgency then
    begin
      DM_CLIENT.CL_AGENCY.Activate;
      CHECK_DATA.FieldByName('AgencyMemo1').Value := DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR', wwcsChecks.FieldByName('CL_AGENCY_NBR').Value, 'CHECK_MEMO_LINE_1');
      CHECK_DATA.FieldByName('AgencyMemo2').Value := DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR', wwcsChecks.FieldByName('CL_AGENCY_NBR').Value, 'CHECK_MEMO_LINE_2');
    end
    else begin
      CHECK_DATA.FieldByName('AgencyMemo1').Value := '';
      CHECK_DATA.FieldByName('AgencyMemo2').Value := '';
    end;
    CHECK_DATA.Post;
    if ReversePrinting then
      wwcsChecks.Prior
    else
      wwcsChecks.Next;
  end;
end;

end.
