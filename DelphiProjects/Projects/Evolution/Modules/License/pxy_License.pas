// Proxy Module "License"
unit pxy_License;

interface

implementation

uses EvCommonInterfaces, isBaseClasses, EvMainboard, evRemoteMethods, EvTransportDatagrams,
     evProxy, EvTransportInterfaces, Math, evBasicUtils, evConsts, evContext, evClasses, EvTypes;

type
  TevLicenseProxy = class(TevProxyModule, IevLicense)
  private
    FLock: IisLock;
    FLicenseInfo: IisListOfValues;
    procedure CacheLicense;
  protected
    procedure DoOnConstruction; override;
    procedure Refresh;
    procedure Check;
    function  GetLicenseInfo: IisListOfValues;
    function  ADPImport: Boolean;
    function  WebPayroll: Boolean;
    function  HR: Boolean;
    function  VMR: Boolean;
    function  PaysuiteImport: Boolean;
    function  CTSReports: Boolean;
    function  SelfServe: Boolean;
    function  EvoX: Boolean;
    function  Benefits: Boolean;
    function  FindAPIKeyByName(const AAPIKey: TisGUID): IisListOfValues;
    function  FindAPIKey(const AAPIKey: TevVendorKeyType): IisListOfValues;
  public
  end;


function GetLicenseProxy: IevLicense;
begin
  Result := TevLicenseProxy.Create;
end;


{ TevLicenseProxy }

function TevLicenseProxy.ADPImport: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['ADPImport'];
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.Benefits: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['Benefits'];
  finally
    FLock.Unlock;
  end;
end;

procedure TevLicenseProxy.CacheLicense;
var
  D: IevDatagram;
begin
  if not Assigned(FLicenseInfo) then
  begin
    D := CreateDatagram(METHOD_LICENSE_GETLICENSEINFO);

    // IMPORTANT! EXCEPTION OF RULES!!!
    // Guest account is internal and doesn't require authentication by security
    // otherwise system would lockup by security authentication feedback
    D.Header.User := EncodeUserAtDomain(sGuestUserName, Context.UserAccount.Domain);
    D.Header.Password := '';

    D := SendRequest(D, False);
    FLicenseInfo := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
  end;
end;

procedure TevLicenseProxy.Check;
begin
  FLock.Lock;
  try
    CacheLicense;
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.CTSReports: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['CTSReports'];
  finally
    FLock.Unlock;
  end;
end;

procedure TevLicenseProxy.DoOnConstruction;
begin
  inherited;
  FLock := TisLock.Create;
end;

function TevLicenseProxy.EvoX: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['EvoX'];
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.FindAPIKey(const AAPIKey: TevVendorKeyType): IisListOfValues;
begin
  Result := FindAPIKeyByName(APIVendorKeys[AAPIKey].AppKey);
end;

function TevLicenseProxy.FindAPIKeyByName(const AAPIKey: TisGUID): IisListOfValues;
var
  APIKeysList: IisListOfValues;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := nil;
    if FLicenseInfo.ValueExists('APIKeys') then
    begin
      APIKeysList := IInterface(FLicenseInfo.Value['APIKeys']) as IisListOfValues;
      if Assigned(APIKeysList) and APIKeysList.ValueExists(AAPIKey) then
        Result := IInterface(APIKeysList.Value[AAPIKey]) as IisListOfValues;
    end;
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.GetLicenseInfo: IisListOfValues;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo;
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.HR: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['HR'];
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.PaysuiteImport: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['PaysuiteImport'];
  finally
    FLock.Unlock;
  end;
end;

procedure TevLicenseProxy.Refresh;
begin
  FLock.Lock;
  try
    FLicenseInfo := nil;
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.SelfServe: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['SelfServe'];
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.VMR: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['VMR'];
  finally
    FLock.Unlock;
  end;
end;

function TevLicenseProxy.WebPayroll: Boolean;
begin
  FLock.Lock;
  try
    CacheLicense;
    Result := FLicenseInfo.Value['WebPayroll'];
  finally
    FLock.Unlock;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetLicenseProxy, IevLicense, 'License');

end.


