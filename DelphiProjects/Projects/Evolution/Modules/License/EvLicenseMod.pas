unit EvLicenseMod;

interface

uses Classes, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvContext, OnGuard, OgUtil,
     SEncryptionRoutines, EvLicenseInfo, Math, isBasicUtils, EvStreamUtils, isLogFile,
     evClasses, evTypes;


implementation

type
  TevLicense = class(TisInterfacedObject, IevLicense)
  private
    FCached: Boolean;
    FADPImport: Boolean;
    FWebPayroll: Boolean;
    FHR: Boolean;
    FVMR: Boolean;
    FPaysuiteImport: Boolean;
    FCTSReports: Boolean;
    FSelfServe: Boolean;
    FEvoX: Boolean;
    FBenefits: Boolean;
    FAPIKeys: IisListOfValues;
    procedure CacheLicense;
  protected
    procedure DoOnConstruction; override;
    procedure Refresh;
    procedure Check;
    function  GetLicenseInfo: IisListOfValues;
    function  ADPImport: Boolean;
    function  WebPayroll: Boolean;
    function  HR: Boolean;
    function  VMR: Boolean;
    function  PaysuiteImport: Boolean;
    function  CTSReports: Boolean;
    function  SelfServe: Boolean;
    function  EvoX: Boolean;
    function  Benefits: Boolean;
    function  FindAPIKeyByName(const AAPIKey: TisGUID): IisListOfValues;
    function  FindAPIKey(const AAPIKey: TevVendorKeyType): IisListOfValues;
  public
  end;


  TLicenseChecker = class(TOgSerialNumberCode)
  protected
    procedure DoOnGetKey(var Key: TKey); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;


function GetLicense: IevLicense;
begin
  Result := TevLicense.Create;
end;


{ TevLicense }

function TevLicense.ADPImport: Boolean;
begin
  Lock;
  try
    CacheLicense;
    Result := FADPImport;
  finally
    Unlock;
  end;
end;

function TevLicense.Benefits: Boolean;
begin
  Lock;
  try
    CacheLicense;
    Result := FBenefits;
  finally
    Unlock;
  end;
end;

procedure TevLicense.CacheLicense;
var
  Checker: TLicenseChecker;
  LI, SN: Cardinal;
  i: Integer;
  Status: TCodeStatus;
  sAPIKeys, sEncryptedAPIKeys, sHexEncryptedAPIKeys: String;
  sEncryptKey, sRegCode : String;
  APIKeysStream: IevDualStream;
begin
  if FCached then
    Exit;

  // Checking license  
  Checker := TLicenseChecker.Create(nil);
  try
    Status := Checker.CheckCode(False);

    if Status = ogValidCode then
    begin
      LI := Cardinal(Checker.GetValue);
      SN := Cardinal(LI div Trunc(IntPower(2, LicenseOptionMaxCount)));
      CheckCondition(IntToStr(SN) = ctx_DomainInfo.LicenseInfo.SerialNumber, 'License is invalid');

      LI := LI mod Trunc(IntPower(2, LicenseOptionMaxCount));

      for i := 0 to LIC_TOP do
        if LI and Trunc(IntPower(2, i)) <> 0 then
          case i of
            LIC_ADP:  FADPImport := True;
            LIC_WEB:  FWebPayroll := True;
            LIC_HR:   FHR := True;
            LIC_VMR:  FVMR := True;
            LIC_PS:   FPaysuiteImport := True;
            LIC_CTS:  FCTSReports := True;
            LIC_SLFS: FSelfServe := True;
            LIC_EVOX: FEvoX := True;
            LIC_BENEFIT: FBenefits := True;
          end;
    end

    else
    begin
      ErrorIf(Status = ogCodeExpired, 'License expired');
      CheckCondition(False, 'License is invalid');
    end;

  finally
    Checker.Free;
  end;


  // loading API keys
  FAPIKeys := TisListOfValues.Create;
  sRegCode := ctx_DomainInfo.LicenseInfo.Code;
  sHexEncryptedAPIKeys := Copy(sRegCode, 17, Length(sRegCode) - 16);
  if sHexEncryptedAPIKeys <> '' then
    try
      sEncryptedAPIKeys := BinToStr(sHexEncryptedAPIKeys);
      sEncryptKey := Copy(sRegCode, 1, 16);
      EnsureKeyLength(sEncryptKey);
      sAPIKeys := DecryptString(sEncryptedAPIKeys, sEncryptKey);

      APIKeysStream := TevDualStreamHolder.Create;
      APIKeysStream.AsString := sAPIKeys;
      APIKeysStream.Position := 0;
      FAPIKeys.ReadFromStream(APIKeysStream);
    except
      on E:Exception do
        mb_LogFile.AddContextEvent(etError, 'License', 'API keys are invalid', E.Message);
    end;

  FCached := True;
end;

procedure TevLicense.Check;
begin
  Lock;
  try
    CacheLicense;
  finally
    Unlock;
  end;
end;

function TevLicense.CTSReports: Boolean;
begin
  Lock;
  try
    CacheLicense;
    Result := FCTSReports;
  finally
    Unlock;
  end;
end;

procedure TevLicense.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

function TevLicense.EvoX: Boolean;
begin
  Lock;
  try
    CacheLicense;
    Result := FEvoX;
  finally
    Unlock;
  end;
end;

function TevLicense.FindAPIKeyByName(const AAPIKey: TisGUID): IisListOfValues;
begin
  Lock;
  try
    CacheLicense;
    Result := nil;
    if FAPIKeys.ValueExists(AAPIKey) then
      Result := IInterface(FAPIKeys.Value[AAPIKey]) as IisListOfValues;
  finally
    Unlock;
  end;
end;

function TevLicense.FindAPIKey(const AAPIKey: TevVendorKeyType): IisListOfValues;
begin
  Result := FindAPIKeyByName(APIVendorKeys[AAPIKey].AppKey);
end;

function TevLicense.GetLicenseInfo: IisListOfValues;
begin
  Lock;
  try
    CacheLicense;
    Result := TisListOfValues.Create;
    Result.AddValue('SerialNumber', ctx_DomainInfo.LicenseInfo.SerialNumber);
    Result.AddValue('ADPImport', FADPImport);
    Result.AddValue('WebPayroll', FWebPayroll);
    Result.AddValue('HR', FHR);
    Result.AddValue('VMR', FVMR);
    Result.AddValue('PaysuiteImport', FPaysuiteImport);
    Result.AddValue('CTSReports', FCTSReports);
    Result.AddValue('SelfServe', FSelfServe);
    Result.AddValue('EvoX', FEvoX);
    Result.AddValue('Benefits', FBenefits);
    Result.AddValue('APIKeys', FAPIKeys.GetClone);
  finally
    Unlock;
  end;
end;

function TevLicense.HR: Boolean;
begin
  Lock;
  try
    CacheLicense;
    Result := FHR;
  finally
    Unlock;
  end;
end;

function TevLicense.PaysuiteImport: Boolean;
begin
  Lock;
  try
    CacheLicense;  
    Result := FPaysuiteImport;
  finally
    Unlock;
  end;
end;

procedure TevLicense.Refresh;
begin
  Lock;
  try
    FCached := False;
  finally
    Unlock;
  end;
end;

function TevLicense.SelfServe: Boolean;
begin
  Lock;
  try
    CacheLicense;
    Result := FSelfServe;
  finally
    Unlock;
  end;
end;

function TevLicense.VMR: Boolean;
begin
  Lock;
  try
    CacheLicense;  
    Result := FVMR;
  finally
    Unlock;
  end;
end;

function TevLicense.WebPayroll: Boolean;
begin
  Lock;
  try
    CacheLicense;  
    Result := FWebPayroll;
  finally
    Unlock;
  end;
end;


{ TLicenseChecker }

constructor TLicenseChecker.Create(AOwner: TComponent);
var
  N: Cardinal;
begin
  inherited;
  AutoCheck := False;
  StoreCode := True;
  StoreModifier := True;

  N := Mainboard.MachineInfo.ID;
  Modifier := BufferToHex(N, SizeOf(N));

  // Acual code is stored in fists 16 characters. The rest is API
  Code := Copy(ctx_DomainInfo.LicenseInfo.Code, 1, 16);
end;

procedure TLicenseChecker.DoOnGetKey(var Key: TKey);
begin
  Key := cKey;
end;


initialization
  Mainboard.ModuleRegister.RegisterModule(@GetLicense, IevLicense, 'License');


end.

