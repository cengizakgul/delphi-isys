// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvLicenseInfo;

interface

uses OnGuard;

type
  TLicenseDescr = record
    ShortDesc: ShortString;
    Description: ShortString;
  end;

const
  LicenseOptionMaxCount = 16;
  cKey: TKey = ($F2, $D0, $DD, $FE, $13, $3E, $C2, $C4, $13, $19, $22, $1C, $04, $5D, $BA, $84);


//*********************
  LIC_ADP = 0;
  LIC_WEB = 1;
  LIC_HR = 2;
  LIC_VMR = 3;
  LIC_PS = 4;
  LIC_CTS = 5;
  LIC_SLFS = 6;
  LIC_EVOX = 7;
  LIC_BENEFIT = 8;
// Don't forget to change LIC_TOP if you add a new license!
// lDescr and lDefaults must be in the same order as the list of licenses above
  LIC_TOP = LIC_BENEFIT;
//*********************

  lDescr: array [0..LIC_TOP] of TLicenseDescr =
    (
    (ShortDesc: 'ADP'; Description: 'ADP Import'),
    (ShortDesc: 'WEB'; Description: 'Web Payroll'),
    (ShortDesc: 'HR'; Description: 'HR Module'),
    (ShortDesc: 'VMR'; Description: 'Virtual Mailroom'),
    (ShortDesc: 'PS'; Description: 'Paysuite Import'),
    (ShortDesc: 'CTS'; Description: 'CTS Report'),
    (ShortDesc: 'SLFS'; Description: 'Self serve'),
    (ShortDesc: 'EVOX'; Description: 'Evolution Exchange'),
    (ShortDesc: 'BENEF'; Description: 'Benefits Enrollment')
    );

// what should be enabled by default
  lDefaults: array [0..LIC_TOP] of Boolean =
    (
     {ADP} False,
     {WEB} False,
     {HR}  True,
     {VMR} True,
     {PS}  False,
     {CTS} False,
     {SLFS}False,
     {EVOX}False,
     {BENEF}False
    );

  DefaultLicenseFrequency: Integer = 1;
  DefaultLicenseExpiration: Integer = 7;
  DefaultReportGracePeriod: Integer = 45;
  OfflineClientExpiration = 200;

implementation

end.
