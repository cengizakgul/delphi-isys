unit EvGlobalCallbacksBrk;

interface

uses Classes, SysUtils, isBaseClasses, EvCommonInterfaces, EvGlobalCallbacks, EvTransportInterfaces,
     EvTransportDatagrams, EvRemoteMethods, EvMainboard, EvBasicUtils, isBasicUtils, isLogFile,
     isErrorUtils, EvContext, EvConsts, isAppIDs;

implementation

type
  TevBrokerGlobalCallbacks = class(TevGlobalCallbacks, IevProxyModule)
  private
    procedure Broadcast(const ADatagram: IevDatagram; const ADispatcher: IevDatagramDispatcher; const aUsers: String; const ASessions: IisStringList);
    procedure BroadcastToClients(const ADatagram: IevDatagram; const aUsers: String; const ASessions: IisStringList);
    procedure BroadcastToServers(const ADatagram: IevDatagram);
    procedure BroadcastToServices(const ADatagram: IevDatagram; const AAppID: array of String);
    procedure DoNotifySecurityChange(const aChangedItem: String);
    procedure DoNotifyTaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
    procedure DoNotifyGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure DoNotifyGlobalSettingsChange(const aChangedItem: String);
    procedure DoNotifyPopupMessage(const aText, aFromUser, aToUser: String);
  protected
    procedure SecurityChange(const aChangedItem: String); override;
    procedure TaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent;
                             const ATaskID: TisGUID); override;
    procedure GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char); override;
    procedure GlobalSettingsChange(const aChangedItem: String); override;
    procedure PopupMessage(const aText, aFromUser, aToUser: String); override;
    procedure OnDisconnect;
    function  Connected: Boolean;
  end;


function GetGlobalCallbacks: IevGlobalCallbacks;
begin
  Result := TevBrokerGlobalCallbacks.Create;
end;


{ TevBrokerGlobalCallbacks }

procedure TevBrokerGlobalCallbacks.OnDisconnect;
begin
end;

procedure TevBrokerGlobalCallbacks.Broadcast(const ADatagram: IevDatagram;
  const ADispatcher: IevDatagramDispatcher; const aUsers: String; const ASessions: IisStringList);
var
  i: Integer;
  Session: IevSession;
  bAllUsers, bAllDomain, bOneUser: Boolean;
  sU: String;
  L: IisList;
begin
  if Assigned(ASessions) then
  begin
    for i := 0 to ASessions.Count - 1 do
    begin
      Session := ADispatcher.FindSessionByID(ASessions[i]);
      if Assigned(Session) then
      begin
        ADatagram.Header.SessionID := Session.SessionID;
        try
          Session.SendDatagram(ADatagram);
        except
          on E: Exception do
            Mainboard.LogFile.AddEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, E.Message, GetStack);
        end;
      end
      else
        UnsubscribeAllItems(ASessions[i]);
    end;
  end

  else
  begin
    bAllUsers := False;
    bAllDomain := False;
    bOneUser := False;

    if (aUsers = '') or AnsiSameText(aUsers, 'Everyone') then
    begin
      bAllUsers := True;
      sU := '';
    end
    else if StartsWith(aUsers, 'Everyone@') then
    begin
      bAllDomain := True;
      sU := aUsers;
      GetNextStrValue(sU, 'Everyone');
    end
    else
    begin
      bOneUser := True;
      sU := aUsers;
    end;

    L := ADispatcher.GetSessions;
    for i := 0 to L.Count - 1 do
    begin
      Session := L[i] as IevSession;
      if bAllUsers or
         bAllDomain and FinishesWith(Session.Login, sU) or
         bOneUser and AnsiSameText(Session.Login, sU) then
      begin
        ADatagram.Header.SessionID := Session.SessionID;

        try
          Session.SendDatagram(ADatagram);
        except
          on E: Exception do
            Mainboard.LogFile.AddEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, E.Message, GetStack);
        end;
      end;
    end;
  end;
end;

procedure TevBrokerGlobalCallbacks.BroadcastToClients(const ADatagram: IevDatagram;
  const aUsers: String; const ASessions: IisStringList);
begin
  if Mainboard.TCPServer <>  nil then
    Broadcast(ADatagram, Mainboard.TCPServer.DatagramDispatcher, aUsers, ASessions);
end;

procedure TevBrokerGlobalCallbacks.BroadcastToServers(const ADatagram: IevDatagram);
begin
  if Mainboard.TCPClient <>  nil then
    Broadcast(ADatagram, Mainboard.TCPClient.DatagramDispatcher, '', nil);
end;

procedure TevBrokerGlobalCallbacks.GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
  inherited;
  DoNotifyGlobalFlagChange(AFlagInfo, AOperation);
end;

procedure TevBrokerGlobalCallbacks.GlobalSettingsChange(const aChangedItem: String);
begin
  inherited;
  DoNotifyGlobalSettingsChange(aChangedItem);
end;

procedure TevBrokerGlobalCallbacks.SecurityChange(const aChangedItem: String);
begin
  inherited;
  DoNotifySecurityChange(aChangedItem);
end;

procedure TevBrokerGlobalCallbacks.TaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent;
  const ATaskID: TisGUID);
begin
  inherited;
  DoNotifyTaskQueueEvent(AUserAtDomain, AEvent, ATaskID);
end;

procedure TevBrokerGlobalCallbacks.DoNotifyGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
var
  D: IevDatagram;
  S1, S2: IisStringList;
begin
  S1 := GetSubscribers(cetGlobalFlagEvent, AFlagInfo.FlagName);
  S2 := GetSubscribers(cetGlobalFlagEvent, AFlagInfo.FlagType + '.*');
  S1.Duplicates := dupIgnore; 
  S1.AddStrings(S2);

  if S1.Count > 0 then
  begin
    D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_GLOBALFLAGCHANGE);
    D.Params.AddValue('AFlagInfo', AFlagInfo);
    D.Params.AddValue('AOperation', Integer(Byte(AOperation)));

    D.Header.User := EncodeUserAtDomain(sGuestUserName, ctx_DomainInfo.DomainName);
    BroadcastToClients(D, '', S1);
  end;
end;

procedure TevBrokerGlobalCallbacks.DoNotifyGlobalSettingsChange(const aChangedItem: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_GLOBALSETTINGSCHANGE);
  D.Params.AddValue('aChangedItem', aChangedItem);
  D.Header.User := EncodeUserAtDomain(sGuestUserName, ctx_DomainInfo.DomainName);

  if AnsiSameText(aChangedItem, 'License') then
  begin
    BroadcastToClients(D, EncodeUserAtDomain('Everyone', ctx_DomainInfo.DomainName), nil);
    BroadcastToServers(D);
  end

  else if AnsiSameText(aChangedItem, 'SystemAccount') or AnsiSameText(aChangedItem, 'AdminAccount') or
          AnsiSameText(aChangedItem, 'EMail') or StartsWith(aChangedItem, 'DB.') then
  begin
    BroadcastToServices(D, [EvoADRClientAppInfo.AppID, EvoADRServerAppInfo.AppID]);
    BroadcastToServers(D);
  end;
end;

procedure TevBrokerGlobalCallbacks.DoNotifySecurityChange(
  const aChangedItem: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_SECURITYCHANGE);
  D.Params.AddValue('AChangedItem', aChangedItem);
  D.Header.User := EncodeUserAtDomain(sGuestUserName, ctx_DomainInfo.DomainName);
  BroadcastToServers(D);
  BroadcastToClients(D, EncodeUserAtDomain('Everyone', ctx_DomainInfo.DomainName), nil);
end;

procedure TevBrokerGlobalCallbacks.DoNotifyTaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent;
  const ATaskID: TisGUID);
var
  D: IevDatagram;
  sUser, sDomain: String;
  S: IisStringList;
begin
  S := GetSubscribers(cetTaskQueueEvent, AUserAtDomain);

  if S.Count > 0 then
  begin
    D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_TASKQUEUEEVENT);
    D.Params.AddValue('AUserAtDomain', AUserAtDomain);
    D.Params.AddValue('AEvent', Integer(Ord(AEvent)));
    D.Params.AddValue('ATaskID', ATaskID);

    DecodeUserAtDomain(AUserAtDomain, sUser, sDomain);
    D.Header.User := EncodeUserAtDomain(sGuestUserName, sDomain);
    BroadcastToClients(D, '', S);
  end;
end;

procedure TevBrokerGlobalCallbacks.PopupMessage(const aText, aFromUser, aToUser: String);
begin
  inherited;
  DoNotifyPopupMessage(aText, aFromUser, aToUser);
end;

procedure TevBrokerGlobalCallbacks.DoNotifyPopupMessage(const aText, aFromUser, aToUser: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_POPUPMESSAGE);
  D.Params.AddValue('AText', aText);
  D.Params.AddValue('AFromUser', aFromUser);
  D.Params.AddValue('AToUser', aToUser);

  BroadcastToClients(D, aToUser, nil);
end;

function TevBrokerGlobalCallbacks.Connected: Boolean;
begin
  Result := True;
end;

procedure TevBrokerGlobalCallbacks.BroadcastToServices(const ADatagram: IevDatagram; const AAppID: array of String);
var
  Sessions: IisStringList;
  L: IisList;
  i, j: Integer;
  S: IevSession;
begin
  if Mainboard.TCPServer <>  nil then
  begin
    L := Mainboard.TCPServer.DatagramDispatcher.GetSessions;
    Sessions := TisStringList.Create;
    for i := 0 to L.Count - 1 do
    begin
      S := L[i] as IevSession;
      for j := Low(AAppID) to High(AAppID) do
        if S.AppID = AAppID[j] then
        begin
          Sessions.Add(S.SessionID);
          break;
        end;
      S := nil;  
    end;

    Broadcast(ADatagram, Mainboard.TCPServer.DatagramDispatcher, '', Sessions);
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalCallbacks, IevGlobalCallbacks, 'Global Callbacks');

end.