unit EvGlobalCallbacks;

interface

uses SysUtils, isBaseClasses, isBasicUtils, EvMainboard, EvCommonInterfaces, EvContext, EvConsts,
     EvBasicUtils;

type
  TevGlobalCallbacks = class(TisInterfacedObject, IevGlobalCallbacks)
  private
    FSubscribers: IisStringList;
    function  EventItemToKey(const AEventType: TevCBEventType; const AItemID: String): String;
  protected
    procedure DoOnConstruction; override;
    function  GetSubscribers(const AEventType: TevCBEventType; const AItemID: String): IisStringList;
    procedure UnsubscribeAllItems(const ASubscriberID: String);

    procedure Subscribe(const ASubscriberID: String; const AEventType: TevCBEventType; const AItemID: String); virtual;
    procedure Unsubscribe(const ASubscriberID: String; const AEventType: TevCBEventType; const AItemID: String); virtual;

    procedure NotifySecurityChange(const aChangedItem: String); virtual;
    procedure NotifyTaskSchedulerChange(const aChangedItem: String); virtual;
    procedure NotifyTaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID); virtual;
    procedure NotifyGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char); virtual;
    procedure NotifyGlobalSettingsChange(const aChangedItem: String); virtual;
    procedure NotifyPopupMessage(const aText, aFromUser, aToUser: String); virtual;
    procedure NotifySetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean); virtual;

    procedure SecurityChange(const aChangedItem: String); virtual;
    procedure TaskSchedulerChange(const aChangedItem: String); virtual;
    procedure TaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent;
                             const ATaskID: TisGUID); virtual;
    procedure GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char); virtual;
    procedure GlobalSettingsChange(const aChangedItem: String); virtual;
    procedure PopupMessage(const aText, aFromUser, aToUsers: String); virtual;
    procedure SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean); virtual;
  end;

implementation


{ TevGlobalCallbacks }

procedure TevGlobalCallbacks.GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
end;

procedure TevGlobalCallbacks.GlobalSettingsChange(const aChangedItem: String);
var
  L: IisList;
  i: Integer;
begin
  if AnsiSameText(aChangedItem, 'License') then
  begin
    // refresh cached licenses
    L := Mainboard.ContextManager.FindDomainContexts(ctx_DomainInfo.DomainName);

    Mainboard.ContextManager.StoreThreadContext;
    try
      for i := 0 to L.Count - 1 do
      begin
        Mainboard.ContextManager.SetThreadContext(L[i] as IevContext);
        Context.License.Refresh;
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end

  else if AnsiSameText(aChangedItem, 'SystemAccount') or AnsiSameText(aChangedItem, 'AdminAccount') then
    ctx_Security.Refresh('');

  if Supports(mb_GlobalSettings, IevProxyModule) then
    (mb_GlobalSettings as IevProxyModule).OnDisconnect; // Drop cached data
end;


procedure TevGlobalCallbacks.NotifyGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
  GlobalFlagChange(AFlagInfo, AOperation);
end;

procedure TevGlobalCallbacks.NotifyGlobalSettingsChange(const aChangedItem: String);
begin
  GlobalSettingsChange(aChangedItem);
end;

procedure TevGlobalCallbacks.NotifyTaskSchedulerChange(const aChangedItem: String);
begin
  TaskSchedulerChange(aChangedItem);
end;

procedure TevGlobalCallbacks.NotifySecurityChange(const aChangedItem: String);
begin
  SecurityChange(aChangedItem);
end;

procedure TevGlobalCallbacks.NotifyTaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent;
  const ATaskID: TisGUID);
begin
  TaskQueueEvent(AUserAtDomain, AEvent, ATaskID);
end;

procedure TevGlobalCallbacks.TaskSchedulerChange(const aChangedItem: String);
begin
  if mb_TaskScheduler <> nil then
    mb_TaskScheduler.RefreshTaskList;
end;

procedure TevGlobalCallbacks.SecurityChange(const aChangedItem: String);
var
  L: IisList;
  i: Integer;
begin
  if Context = nil then
    Exit;
    
  L := Mainboard.ContextManager.FindDomainContexts(ctx_DomainInfo.DomainName);

  Mainboard.ContextManager.StoreThreadContext;
  try
    for i := 0 to L.Count - 1 do
    begin
      Mainboard.ContextManager.SetThreadContext(L[i] as IevContext);
      ctx_Security.Refresh(aChangedItem);
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevGlobalCallbacks.TaskQueueEvent(const AUserAtDomain: String;
  const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
begin
end;

procedure TevGlobalCallbacks.NotifyPopupMessage(const aText, aFromUser, aToUser: String);
begin
  PopupMessage(aText, aFromUser, aToUser);
end;

procedure TevGlobalCallbacks.PopupMessage(const aText, aFromUser, aToUsers: String);
begin
end;

procedure TevGlobalCallbacks.DoOnConstruction;
begin
  inherited;
  FSubscribers := TisStringList.CreateUnique(True);
end;

function TevGlobalCallbacks.GetSubscribers(const AEventType: TevCBEventType; const AItemID: String): IisStringList;
var
  i: Integer;
  SL: IisStringList;
  sKey: String;
begin
  sKey := EventItemToKey(AEventType, AItemID);
  Result := TisStringList.Create;

  FSubscribers.Lock;
  try
    i := FSubscribers.IndexOf(sKey);
    if i <> -1 then
    begin
      SL := FSubscribers.Objects[i] as IisStringList;
      if Assigned(SL) then
        Result.AddStrings(SL);
    end;

  finally
    FSubscribers.Unlock;
  end;
end;

procedure TevGlobalCallbacks.UnsubscribeAllItems(const ASubscriberID: String);
var
  i, j: Integer;
  SL: IisStringList;
begin
  FSubscribers.Lock;
  try
    for i := FSubscribers.Count - 1 downto 0 do
    begin
      SL := FSubscribers.Objects[i] as IisStringList;
      j := SL.IndexOf(ASubscriberID);
      if j <> - 1 then
        SL.Delete(j);
      if SL.Count = 0 then
        FSubscribers.Delete(i);
    end;

  finally
    FSubscribers.Unlock;
  end;
end;

procedure TevGlobalCallbacks.Subscribe(const ASubscriberID: String;
  const AEventType: TevCBEventType; const AItemID: String);
var
  i: Integer;
  sKey: String;
begin
  sKey := EventItemToKey(AEventType, AItemID);
  FSubscribers.Lock;
  try
    i := FSubscribers.Add(sKey);
    if FSubscribers.Objects[i] = nil then
      FSubscribers.Objects[i] := TisStringList.CreateUnique(False);

    (FSubscribers.Objects[i] as IisStringList).Add(ASubscriberID);
  finally
    FSubscribers.Unlock;
  end;
end;

procedure TevGlobalCallbacks.Unsubscribe(const ASubscriberID: String;
  const AEventType: TevCBEventType; const AItemID: String);
var
  i, j: Integer;
  SL: IisStringList;
  sKey: String;
begin
  if AEventType = cetAnyEvent then
    UnsubscribeAllItems(ASubscriberID)
  else
  begin
    sKey := EventItemToKey(AEventType, AItemID);
    FSubscribers.Lock;
    try
      i := FSubscribers.IndexOf(sKey);
      if i <> -1 then
      begin
        SL := FSubscribers.Objects[i] as IisStringList;
        if Assigned(SL) then
        begin
          j := SL.IndexOf(ASubscriberID);
          if j <> - 1 then
            SL.Delete(j);
          if SL.Count = 0 then
            FSubscribers.Delete(i);
        end;
      end;
    finally
      FSubscribers.Unlock;
    end;
  end;
end;

function TevGlobalCallbacks.EventItemToKey(const AEventType: TevCBEventType; const AItemID: String): String;
begin
  Result := IntToStr(Ord(AEventType)) + ' ' + AItemID;
end;

procedure TevGlobalCallbacks.NotifySetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean);
begin
  SetDBMaintenance(aDBName, aMaintenance, aADRContext);
end;

procedure TevGlobalCallbacks.SetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean);
var
  OldADRContext: Boolean;
begin
  OldADRContext := ctx_DBAccess.GetADRContext;
  try
    ctx_DBAccess.SetADRContext(aADRContext);
    ctx_DBAccess.SetDBMaintenance(aDBName, aMaintenance);
  finally
    ctx_DBAccess.SetADRContext(OldADRContext);
  end;
end;

end.
