// Module "Global Callbacks"
unit EvGlobalCallbacksCltMod;

interface

uses
  SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvGlobalCallbacks, EvContext,
  EvTransportDatagrams, EvTransportInterfaces, EvRemoteMethods, EvBasicUtils, EvInitApp;

implementation

type
  TevClientGlobalCallbacks = class(TevGlobalCallbacks)
  protected
    procedure Subscribe(const ASubscriberID: String; const AEventType: TevCBEventType; const AItemID: String); override;
    procedure Unsubscribe(const ASubscriberID: String; const AEventType: TevCBEventType; const AItemID: String); override;
    procedure NotifySecurityChange(const aChangedItem: String); override;
    procedure TaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent;
                             const ATaskID: TisGUID); override;
    procedure GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char); override;
    procedure GlobalSettingsChange(const aChangedItem: String); override;
    procedure PopupMessage(const aText, aFromUser, aToUsers: String); override;
  end;


function GetGlobalCallbacks: IevGlobalCallbacks;
begin
  Result := TevClientGlobalCallbacks.Create;
end;

procedure TevClientGlobalCallbacks.GlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
  inherited;
  if ctx_EvolutionGUI <> nil then
    ctx_EvolutionGUI.OnGlobalFlagChange(AFlagInfo, AOperation);
end;

procedure TevClientGlobalCallbacks.GlobalSettingsChange(const aChangedItem: String);
begin
  inherited;
  if AnsiSameText(aChangedItem, 'License') then
    SecurityChange('');
end;

procedure TevClientGlobalCallbacks.NotifySecurityChange(const aChangedItem: String);
var
  D: IevDatagram;
begin
  if isStandalone then
    SecurityChange(aChangedItem)
  else
  begin
    // TODO!!!
    // This is a workaround! This notification should be generated in DB Access on RP side
    D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_SECURITYCHANGE);
    D.Header.User := EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain);
    D.Header.Password := Context.UserAccount.Password;
    D.Header.ContextID := Context.GetID;
    D.Header.SessionID := Mainboard.TCPClient.Session.SessionID;
    D.Params.AddValue('AChangedItem', aChangedItem);
    Mainboard.TCPClient.Session.SendDatagram(D);
  end;
end;

procedure TevClientGlobalCallbacks.PopupMessage(const aText, aFromUser, aToUsers: String);
begin
  inherited;
  if ctx_EvolutionGUI <> nil then
    ctx_EvolutionGUI.OnPopupMessage(aText, aFromUser, aToUsers);
end;

procedure TevClientGlobalCallbacks.Subscribe(const ASubscriberID: String;
  const AEventType: TevCBEventType; const AItemID: String);
var
  D: IevDatagram;
begin
  if isStandalone then
    inherited
  else
  begin
    D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_SUBSCRIBE);
    D.Header.User := EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain);
    D.Header.Password := Context.UserAccount.Password;
    D.Header.ContextID := Context.GetID;
    D.Header.SessionID := Mainboard.TCPClient.Session.SessionID;
    D.Params.AddValue('ASubscriberID', ASubscriberID);
    D.Params.AddValue('AEventType', AEventType);
    D.Params.AddValue('AItemID', AItemID);
    D := Mainboard.TCPClient.Session.SendRequest(D);
  end;
end;

procedure TevClientGlobalCallbacks.TaskQueueEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
var
  P: IisListOfValues;
begin
  inherited;
  if (ctx_EvolutionGUI <> nil) and (ctx_EvolutionGUI.GUIContext <> nil) and
     AnsiSameText(AUserAtDomain,
       EncodeUserAtDomain(ctx_EvolutionGUI.GUIContext.UserAccount.User,
                          ctx_EvolutionGUI.GUIContext.UserAccount.Domain)) then
    case AEvent of
      tqeTaskListChange:   ctx_EvolutionGUI.CheckTaskQueueStatus;
      tqeTaskReadyToPrint: ctx_EvolutionGUI.PrintTask(ATaskID);
    end;

  P := TisListOfValues.Create;
  P.AddValue('UserAtDomain', AUserAtDomain);
  P.AddValue('Event', AEvent);
  P.AddValue('TaskID', ATaskID);

  Mainboard.Messenger.Broadcast('GlobalCallbacks.TaskQueueEvent', P);
end;

procedure TevClientGlobalCallbacks.Unsubscribe(const ASubscriberID: String;
  const AEventType: TevCBEventType; const AItemID: String);
var
  D: IevDatagram;
begin
  if isStandalone then
    inherited
  else
  begin
    D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_UNSUBSCRIBE);
    D.Header.User := EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain);
    D.Header.Password := Context.UserAccount.Password;
    D.Header.ContextID := Context.GetID;
    D.Header.SessionID := Mainboard.TCPClient.Session.SessionID;
    D.Params.AddValue('ASubscriberID', ASubscriberID);
    D.Params.AddValue('AEventType', AEventType);
    D.Params.AddValue('AItemID', AItemID);
    D := Mainboard.TCPClient.Session.SendRequest(D);
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalCallbacks, IevGlobalCallbacks, 'Global Callbacks');

end.
