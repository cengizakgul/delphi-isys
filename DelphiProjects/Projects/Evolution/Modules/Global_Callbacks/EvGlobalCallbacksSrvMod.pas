// Module "Global Callbacks"
unit EvGlobalCallbacksSrvMod;

interface

uses
  SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, EvGlobalCallbacks, EvTransportInterfaces,
  EvTransportDatagrams, EvRemoteMethods, isLogFile, isErrorUtils, EvBasicUtils, EvConsts,
  EvContext;

implementation

type
  TevServerGlobalCallbacks = class(TevGlobalCallbacks)
  private
    procedure Broadcast(const ADatagram: IevDatagram; const ADispatcher: IevDatagramDispatcher);
    procedure BroadcastToClients(const ADatagram: IevDatagram);
  protected
    procedure NotifySecurityChange(const aChangedItem: String); override;
    procedure NotifyTaskSchedulerChange(const aChangedItem: String); override;
    procedure NotifySetDBMaintenance(const aDBName: String; const aMaintenance: Boolean; const aADRContext: Boolean); override;
  end;


function GetGlobalCallbacks: IevGlobalCallbacks;
begin
  Result := TevServerGlobalCallbacks.Create;
end;

{ TevServerGlobalCallbacks }

procedure TevServerGlobalCallbacks.Broadcast(const ADatagram: IevDatagram;
  const ADispatcher: IevDatagramDispatcher);
var
  i: Integer;
  Session: IevSession;
  L: IisList;
begin
  ADatagram.Header.User := EncodeUserAtDomain(sGuestUserName, ctx_DomainInfo.DomainName);

  L := ADispatcher.GetSessions;
  for i := 0 to L.Count - 1 do
  begin
    Session := L[i] as IevSession;
    ADatagram.Header.SessionID := Session.SessionID;

    try
      Session.SendDatagram(ADatagram);
    except
      on E: Exception do
        Mainboard.LogFile.AddEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, E.Message, GetStack);
    end;
  end;
end;

procedure TevServerGlobalCallbacks.BroadcastToClients(const ADatagram: IevDatagram);
begin
  if Mainboard.TCPServer <>  nil then
    Broadcast(ADatagram, Mainboard.TCPServer.DatagramDispatcher);
end;

procedure TevServerGlobalCallbacks.NotifySecurityChange(const aChangedItem: String);
var
  D: IevDatagram;
begin
  inherited;
  D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_SECURITYCHANGE);
  D.Params.AddValue('AChangedItem', aChangedItem);
  BroadcastToClients(D);
end;

procedure TevServerGlobalCallbacks.NotifySetDBMaintenance(const aDBName: String; const aMaintenance: Boolean;
  const aADRContext: Boolean);
var
  D: IevDatagram;
begin
  inherited;
  D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_SETDBMAINTENANCE);
  D.Params.AddValue('aDBName', aDBName);
  D.Params.AddValue('aMaintenance', aMaintenance);
  D.Params.AddValue('aADRContext', aADRContext);
  BroadcastToClients(D);
end;

procedure TevServerGlobalCallbacks.NotifyTaskSchedulerChange(const aChangedItem: String);
var
  D: IevDatagram;
begin
  inherited;
  D := TevDatagram.Create(METHOD_GLOBALCALLBACKS_TASKSCHEDULERCHANGE);
  D.Params.AddValue('AChangedItem', aChangedItem);
  BroadcastToClients(D);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalCallbacks, IevGlobalCallbacks, 'Global Callbacks');

end.
