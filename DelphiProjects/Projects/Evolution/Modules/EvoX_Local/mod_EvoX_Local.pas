unit mod_EvoX_Local;

interface

uses
  EvExchangeMod,
  EvImportMapRecord,
  EvExchangePackage,
  EvExchangePackageExt,
  EvExchangeMapFile,
  EvExchangeMapFileExt,
  EvExchangeXMLUtils,
  EvCSVReader,
  EvFixedReader;

implementation

end.
