unit EvFixedReader;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvConsts, EvStreamUtils, EvTypes, ISBasicUtils, DB, EvExchangeConsts,isLogFile;

const
  UnusedFieldPrefix = 'Unused ';

type
  ErrorInFieldException = class(Exception);

  IFixedReaderFieldDefinition = interface
  ['{B0D7617D-5FCD-4196-A08E-F59EE441438F}']
    function  GetAsString: String;
    procedure SetFromString(const AFieldDefinition: String); // format of string: FieldName:Start-End
    function  GetName: String;
    procedure SetName(const AName: String);
    function  GetStartPos: integer;
    function  GetEndPos: integer;
    procedure SetPositions(const AStartPos: integer; const AEndPos: integer);
    function  GetLength: integer;
    function  StartPosAsString: String;
  end;

  TFixedReaderFieldDefinition = class(TisInterfacedObject, IFixedReaderFieldDefinition)
  private
    FName: String;
    FStart, FEnd: integer;
  protected
    procedure CheckAndSwapPos;

    // IFixedReaderFieldDefinition
    function  GetAsString: String;
    procedure SetFromString(const AFieldDefinition: String);
    function  GetName: String;
    procedure SetName(const AName: String);
    function  GetStartPos: integer;
    procedure SetPositions(const AStartPos: integer; const AEndPos: integer);
    function  GetEndPos: integer;
    function  GetLength: integer;
    function  StartPosAsString: String;
  public
    destructor Destroy; override;
  end;

  IevFixedReader = interface
  ['{7546640F-5BF7-49EF-99D2-96B2033970A7}']
    procedure CheckForOverlapping(const AFieldsDefinitions: IisListOfValues);
    function  ParseStringWithFieldsDefinitions(const AStringWithFieldsDefinitions: String): IisListOfValues;
    function  FieldDefinitionsAsString(const AFieldsDefinitions: IisListOfValues): String;
  end;

  TevFixedReader = class(TisInterfacedObject, IEvExchangeDataReader, IevFixedReader)
  private
    FSupportedFormatNames: IisStringList;
    FSupportedFormatFileExt: IisStringList;
    FDescriptionOfOptions: IisStringList;
    FDataReaderOptions: IisStringList;
    FFieldDefs: IisListOfValues;  // list is ordered by Start position!!!!
  protected
    procedure DoOnConstruction; override;
    function  InternalReadData(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog;
      const AMapFile: IEvExchangeMapFile = nil; const AAmountOfRows: integer = -1; const AItisExample: boolean = false): IEvDataset;
    function  GetNextRowData(const AStream: IevDualStream; const AItisExample: boolean = false): IisListOfValues;
    function  GetAllRowsData(const AStream: IevDualStream; const AAmountOfRows: integer = -1;
      const AItisExample: boolean = false): IisListOfValues;
    function  GetMaxFieldSize(AAllRowsData: IisListOfValues; AFieldIndex: integer) : integer;
    function FindFieldDefByName(const AFieldName: String): IFixedReaderFieldDefinition;

    // IEvExchangeDataReader implementation
    function  GetSupportedFormatNames: IisStringListRO;
    function  GetSupportedFormatFileExt: IisStringListRO;
    function  GetDescriptionOfOptions: IisStringListRO;
    function  GetDataReaderOptions: IisStringList;
    procedure SetDataReaderOptions(const AOptions: IisStringList);
    procedure SetDefaultDataReaderOptions;
    function  ReadExampleFromFile(const AFileName: String; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer = 1): IevDataSet;
    function  ReadExampleFromStream(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer = 1): IevDataSet;
    function  ReadDataFromFile(const AFileName: String; const AMapFile : IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
    function  ReadDataFromStream(const AStream: IevDualStream; const AMapFile : IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
    function  GetReaderName : String;
    // IevFixedReader
    procedure CheckForOverlapping(const AFieldsDefinitions: IisListOfValues);
    function  ParseStringWithFieldsDefinitions(const AStringWithFieldsDefinitions: String): IisListOfValues;
    function  FieldDefinitionsAsString(const AFieldsDefinitions: IisListOfValues): String;
  public
    destructor Destroy; override;
  end;

implementation

uses EvDataset, EvUtils;

{ TevFixedReader }

destructor TevFixedReader.Destroy;
begin
  FSupportedFormatNames.Clear;
  FSupportedFormatFileExt.Clear;
  FDescriptionOfOptions.Clear;
  FDataReaderOptions.Clear;
  inherited;
end;

procedure TevFixedReader.DoOnConstruction;
begin
  inherited;
  FSupportedFormatNames := TisStringList.Create;
  FSupportedFormatNames.Add('ASCIIFixed=ASCII files, all records have the same length ');

  FSupportedFormatFileExt := TisStringList.Create;
  FSupportedFormatFileExt.Add('*.txt=Text files');
  FSupportedFormatFileExt.Add('*.prn=Text files');
  FSupportedFormatFileExt.Add('*.*=All files');

  FDescriptionOfOptions := TisStringList.Create;
  FDescriptionOfOptions.Add(fieldsList + '=List of fields and theirs positions in format: FieldName:Start-End; FieldName:Start-End; etc.');

  FDataReaderOptions := TisStringList.Create;
  SetDefaultDataReaderOptions;

  FFieldDefs := TisListOfValues.Create;
end;

function TevFixedReader.GetDescriptionOfOptions: IisStringListRO;
begin
  Result := FDescriptionOfOptions as IisStringListRO;
end;

function TevFixedReader.GetReaderName: String;
begin
  Result := sASCIIFixedReaderName;
end;

function TevFixedReader.GetSupportedFormatFileExt: IisStringListRO;
begin
  Result := FSupportedFormatFileExt as IisStringListRO;
end;

function TevFixedReader.GetSupportedFormatNames: IisStringListRO;
begin
  Result := FSupportedFormatNames as IisStringListRO;
end;

function TevFixedReader.ReadExampleFromFile(const AFileName: String; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer): IevDataSet;
var
  InputStream : IevDualStream;
begin
  CheckCondition(FileExists(AFileName), 'File does not exist :' + AFileName);
  InputStream := TevDualStreamHolder.CreateInMemory;
  InputStream.LoadFromFile(AFileName);
  Result := ReadExampleFromStream(InputStream, AErrorsList, AAmountOfRows);
end;

function TevFixedReader.ReadExampleFromStream(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer): IevDataSet;
begin
  CheckCondition(Assigned(AErrorsList), 'Errors list object is not assigned');
  Result := InternalReadData(AStream, AErrorsList, nil, AAmountOfRows, true);
end;

function TevFixedReader.ReadDataFromFile(const AFileName: String; const AMapFile: IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
var
  InputStream : IevDualStream;
begin
  CheckCondition(FileExists(AFileName), 'File does not exist :' + AFileName);
  InputStream := TevDualStreamHolder.CreateInMemory;
  InputStream.LoadFromFile(AFileName);
  Result := ReadDataFromStream(InputStream, AMapFile, AErrorsList);
end;

function TevFixedReader.ReadDataFromStream(const AStream: IevDualStream; const AMapFile: IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
begin
  CheckCondition(Assigned(AMapFile), 'Map file object is not assigned');
  CheckCondition(Assigned(AErrorsList), 'Errors list object is not assigned');
  Result := InternalReadData(AStream, AErrorsList, AMapFile);
end;

procedure TevFixedReader.SetDataReaderOptions(const AOptions: IisStringList);
var
  i, j: integer;
  sNewValue, sOldValue: String;
  found : boolean;
begin
  if AOptions.Count = 0 then
    SetDefaultDataReaderOptions
  else
    for i := 0 to AOptions.Count - 1 do
    begin
      found := false;
      for j := 0 to FDataReaderOptions.Count - 1 do
      begin
        if CompareText(AOptions.Names[i], FDataReaderOptions.Names[j]) = 0 then
        begin
          found := true;
          sNewValue := AOptions.Values[AOptions.Names[i]];
          sOldValue := FDataReaderOptions.Values[FDataReaderOptions.Names[j]];
          if sOldValue <> sNewValue then
          begin
            if CompareText(AOptions.Names[i], fieldsList)<> 0  then
              CheckCondition(((sNewValue = '0') or (sNewValue = '1')), 'Option: "' + AOptions.Names[i] + '" Unsupported value: ' + sNewValue)
            else
              CheckCondition(Trim(sNewValue) <> '', 'Option: "' + AOptions.Names[i] + '" Unsupported value: ' + sNewValue);

            FDataReaderOptions.Values[FDataReaderOptions.Names[j]] := sNewValue;

            if CompareText(AOptions.Names[i], fieldsList) = 0 then
              FFieldDefs := ParseStringWithFieldsDefinitions(Trim(FDataReaderOptions.Values[fieldsList]));
          end;
          break;
        end;
      end;  // j
      CheckCondition(found, 'Unsupported option: ' + AOptions.Names[i]);
    end;  // i

end;

function TevFixedReader.InternalReadData(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AMapFile: IEvExchangeMapFile = nil;
 const AAmountOfRows: integer = -1;  const AItisExample: boolean = false): IEvDataset;
var
  CurrentRowData: IisListOFValues;
  AllRowsData: IisListOfValues;
  maxFieldSize: integer;
  fieldName : String;
  i, j, start, cnt: integer;
  formatSettings: TFormatSettings;
  S, sValue, sFieldName: String;
  FieldDef: IFixedReaderFieldDefinition;
begin
  Result := TevDataSet.Create;

  AErrorsList.Clear;
  FillChar(formatSettings, SizeOf(formatSettings), 0);
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
  CheckCondition(Assigned(Astream), 'Input stream is not assigned');
  CheckCondition(AStream.Size > 0, 'Input stream is empty');

  // reading data
  try
    AllRowsData := GetAllRowsData(AStream, AAmountOfRows, AItisExample);
  except
    on E: Exception do
    begin
      AErrorsList.AddEvent(etError, E.Message);
      Exit;
    end;
  end;
  CheckCondition(AllRowsData.Count > 0, 'No input data found');

  // defining fields according to input data, all fields has the same type - string
  if AItIsExample then
  begin
    CurrentRowData := IInterface(AllRowsData.Values[0].Value) as IisListOfValues;
    if FFieldDefs.Count = 0 then
    begin
      // as one big field
      CheckCondition(CurrentRowData.Count = 1, 'No fields defined but data has more than one field');
      maxFieldSize := GetMaxFieldSize(AllRowsData, 0);
      fieldName := UnusedFieldPrefix + '(1-' + IntToStr(maxFieldSize) + ')';
      Result.vclDataSet.FieldDefs.Add(fieldName, ftString, maxFieldSize, false);
    end
    else
    begin
      // as many fields as it was read
      for i := 0 to CurrentRowData.Count - 1 do
      begin
        sFieldName := CurrentRowData.Values[i].Name;
        if Pos(UnusedFieldPrefix, sFieldName) <> 0 then
        begin
          S := CurrentRowData.Values[i].Value;
          Result.vclDataSet.FieldDefs.Add(sFieldName, ftString, Length(S), false);
        end
        else
        begin
          FieldDef := FindFieldDefByName(sFieldName);
          Result.vclDataSet.FieldDefs.Add(FieldDef.GetName, ftString, FieldDef.GetLength, false);
        end;
      end;
    end;
  end
  else
  begin
    // according to fields definition
    CheckCondition(FFieldDefs.Count > 0, 'There''s no fields defined');
    for i := 0  to FFieldDefs.Count - 1 do
    begin
      FieldDef := IInterface(FFieldDefs.Values[i].Value) as IFixedReaderFieldDefinition;
      Result.vclDataSet.FieldDefs.Add(FieldDef.GetName, ftString, FieldDef.GetLength, false);
    end;
  end;

  // storing data into dataset
  start := 0;
  cnt := 0;
  Result.vclDataSet.Active := true;
  for i := start to AllRowsData.Count - 1 do
  begin
    if (AAmountOfRows >= 0) and (cnt >= AAmountOfRows) then
      exit;
    try
      CurrentRowData := IInterface(AllRowsData.Values[i].Value) as IisListOfValues;
      if not AItisExample then
        CheckCondition(CurrentRowData.Count >= Result.vclDataSet.FieldDefs.Count, 'Input file has less fields than it supposed. Input fields: '
          + IntToStr(CurrentRowData.Count) + ' Supposed to have: ' + IntToStr(Result.vclDataSet.FieldDefs.Count));
      Result.Append;
      for j := 0 to Result.vclDataSet.FieldDefs.Count - 1 do
      begin
        if AItIsExample and (j > (CurrentRowData.Count - 1)) then
          continue;
        try
          sValue := CurrentRowData.Values[j].Value;
          if Result.vclDataSet.FieldDefs[j].DataType = ftDateTime then
          begin
            CheckCondition(Assigned(AMapFile), 'Cannot find date format due map file is not assigned');
            formatSettings.ShortDateFormat := (IInterface(AMapFile.GetFieldsList.Values[j].Value) as IevEchangeMapField).GetDateFormat;
            formatSettings.ShortTimeFormat := (IInterface(AMapFile.GetFieldsList.Values[j].Value) as IevEchangeMapField).GetTimeFormat;
            Result.vclDataSet.Fields[j].Value := StrToDateTime(sValue, formatSettings);
          end
          else
          if Result.vclDataSet.FieldDefs[j].DataType = ftBoolean then
            if (UpperCase(Trim(sValue)) = 'TRUE') or (Trim(sValue) = '1') then
              Result.vclDataSet.Fields[j].Value := true
            else
              Result.vclDataSet.Fields[j].Value := false
          else
            Result.vclDataSet.Fields[j].Value := TrimRight(sValue);
        except
          on E: Exception do
          begin
            AErrorsList.AddEvent(etError, 'Row# ' + IntToStr(i + 1) + ' Field# ' + IntToStr(j + 1) + ' Value: "' + sValue + '". Error: ' + E.Message);
            raise ErrorInFieldException.Create(AErrorsList.Items[AErrorsList.Count-1].GetText);
          end;
        end;
      end;  // j
      Result.Post;
    except
      on E: ErrorInFieldException do
        Result.vclDataSet.Cancel;
    end;
    Inc(cnt);
  end;  // i
end;

function TevFixedReader.GetMaxFieldSize(AAllRowsData: IisListOfValues; AFieldIndex: integer) : integer;
var
  max, current, i: integer;
  CurrentRowData: IisListOfValues;
  S: String;
begin
  result := EvoXMaxUnknownFieldSize;

  max := 0;
  for i := 0 to AAllRowsData.Count - 1 do
  begin
    CurrentRowData := IInterface(AAllRowsData.Values[i].Value) as IisListOfValues;
    S := CurrentRowData.Values[AFieldindex].Value;
    current := Length(S);
    if current > max then
      max := current;
  end;

  if max <> 0 then
    Result := max;
end;

procedure TevFixedReader.SetDefaultDataReaderOptions;
begin
  FDataReaderOptions.Clear;
  FDataReaderOptions.Add(fieldsList + '=');
end;

function TevFixedReader.GetNextRowData(const AStream: IevDualStream; const AItisExample: boolean = false): IisListOfValues;
var
  sRowData, sFieldData: String;
  i, iUnusedPos, iRowLength: integer;
  FieldDef: IFixedReaderFieldDefinition;
begin
  Result := TisListOfValues.Create;

  sRowData := AStream.ReadLn;
  iRowLength := Length(sRowData);
  if iRowLength = 0 then
    exit;

  if not AItisExample then
  begin
    // it's NOT example, reading only data fields
    CheckCondition(FFieldDefs.Count > 0, 'There''s no fields defined');
    for i := 0 to FFieldDefs.Count - 1 do
    begin
      FieldDef := IInterface(FFieldDefs.Values[i].Value) as IFixedReaderFieldDefinition;
      sFieldData := Copy(SRowData, FieldDef.GetStartPos,  FieldDef.GetLength);
      Result.AddValue(FieldDef.GetName, sFieldData);
    end;
  end
  else
  begin
    // it is example, reading data fields and unused space
    if FFieldDefs.Count = 0 then
      Result.AddValue(UnusedFieldPrefix + UnusedFieldPrefix + '(1-' + IntToStr(Length(sRowData)) + ')', sRowData)
    else
    begin
      iUnusedPos := 1;
      //  FFieldDefs is sorted by start position!
      for i := 0 to FFieldDefs.Count - 1 do
      begin
        FieldDef := IInterface(FFieldDefs.Values[i].Value) as IFixedReaderFieldDefinition;
        if FieldDef.GetStartPos > iUnusedPos then
        begin
          sFieldData := Copy(SRowData, iUnusedPos,  FieldDef.GetStartPos - iUnusedPos);
          Result.AddValue(UnusedFieldPrefix + '(' + IntToStr(iUnusedPos) + '-' +
            IntToStr(FieldDef.GetStartPos - 1) + ')', sFieldData);
        end;
        iUnusedPos := FieldDef.GetEndPos + 1;
        sFieldData := Copy(SRowData, FieldDef.GetStartPos,  FieldDef.GetLength);
        Result.AddValue(FieldDef.GetName, sFieldData);
      end;  // i
      // reading rest of string if it exists
      if iUnusedPos <= iRowLength then
      begin
        sFieldData := Copy(SRowData, iUnusedPos, iRowLength);
        Result.AddValue(UnusedFieldPrefix + '(' + IntToStr(iUnusedPos) + '-' + IntToStr(iRowLength) + ')', sFieldData);
      end;
    end;
  end;
end;

function TevFixedReader.GetAllRowsData(const AStream: IevDualStream; const AAmountOfRows: integer = -1;
  const AItisExample: boolean = false): IisListOfValues;
var
  CurrentRowData: IisListOfValues;
  RowNumber: integer;
  LastChar: Char;
  Pos: integer;
begin
  CheckCondition(AStream.Size > 0, 'File is empty!');

  // removing trailing empty rows
  AStream.Position := 0;
  Pos := -1;
  while not AStream.eos do
  begin
    LastChar := char(AStream.ReadByte);
    if (LastChar <> #13) and (LastChar <> #10) then
      Pos := -1
    else
      if Pos = -1 then
        Pos := AStream.Position;
  end;
  if Pos <> -1 then
    AStream.Size := Pos;

  // adding #13 to end of stream if it is not there
  AStream.Position := AStream.Size - 1;
  LastChar := char(AStream.ReadByte);
  if (LastChar <> #13) and (LastChar <> #10) then
    AStream.WriteByte(byte(#13));

  Result := TisListOfValues.Create;
  AStream.Position := 0;
  RowNumber := 0;

  while true do
  begin
    try
      CurrentRowData := GetNextRowData(AStream, AItisExample);
    except
      on E: Exception do
        raise Exception.Create('Row# ' + IntToStr(RowNumber + 1) + '. Error: ' + E.Message);
    end;
    if RowNumber = 0 then
      CheckCondition(CurrentRowData.Count > 0, 'Input has no data');
    if CurrentRowData.Count = 0 then
      exit;
    if (AAmountOfRows <> -1) and (RowNumber >= AAmountOfRows) then
      exit;
    Result.AddValue(IntToStr(Result.Count), CurrentRowData);
    Inc(RowNumber);
  end;
end;

function TevFixedReader.GetDataReaderOptions: IisStringList;
begin
  Result := TisStringList.Create;
  Result.Text := FDataReaderOptions.Text;
end;

function TevFixedReader.FieldDefinitionsAsString(const AFieldsDefinitions: IisListOfValues): String;
var
  i: integer;
  FieldDef: IFixedReaderFieldDefinition;
begin
  Result := '';

  CheckForOverlapping(AFieldsDefinitions);

  AFieldsDefinitions.Sort;
  for i := 0 to AFieldsDefinitions.Count - 1 do
  begin
    FieldDef := IInterface(AFieldsDefinitions.Values[i].Value) as IFixedReaderFieldDefinition;
    Result := Result + FieldDef.GetAsString;
    if i < AFieldsDefinitions.Count - 1 then
      Result := Result + '; ';
  end;
end;

function TevFixedReader.ParseStringWithFieldsDefinitions(const AStringWithFieldsDefinitions: String): IisListOfValues;
var
  FieldDef: IFixedReaderFieldDefinition;
  S, sField: String;
begin
  Result := TisListOfValues.Create;
  S := Trim(AStringWithFieldsDefinitions);
  CheckCondition(S <> '', 'String with field definitions cannot be empty');

  repeat
    FieldDef := TFixedReaderFieldDefinition.Create;
    sField := GetNextStrValue(S, ';');
    if sField <> '' then
    begin
      FieldDef.SetFromString(Trim(sField));
      CheckCondition(not Result.ValueExists(FieldDef.GetName), 'Field definition with name "' + FieldDef.GetName + '" already exists');
      Result.AddValue(FieldDef.StartPosAsString, FieldDef);
    end;
    S := Trim(S);
  until S = '';

  CheckForOverlapping(Result);

  Result.Sort;
end;

procedure TevFixedReader.CheckForOverlapping(const AFieldsDefinitions: IisListOfValues);
var
  FieldDef, FieldDef1: IFixedReaderFieldDefinition;
  i, j: integer;
begin
  // checking for overlapping
  AFieldsDefinitions.Sort;
  for i := 0 to AFieldsDefinitions.Count - 1 do
  begin
    FieldDef := IInterface(AFieldsDefinitions.Values[i].Value) as IFixedReaderFieldDefinition;
    for j := i + 1 to AFieldsDefinitions.Count - 1 do
    begin
      FieldDef1 := IInterface(AFieldsDefinitions.Values[j].Value) as IFixedReaderFieldDefinition;
      if ((FieldDef1.GetStartPos >= FieldDef.GetStartPos) and (FieldDef1.GetStartPos <= FieldDef.GetEndPos)) or
        ((FieldDef1.GetEndPos >= FieldDef.GetStartPos) and (FieldDef1.GetEndPos <= FieldDef.GetEndPos)) then
        raise Exception.Create('Fields are overlapped. Field1: "' + FieldDef.GetName + '", Field2: "' + FieldDef1.GetName + '"');
    end;
  end;
end;

function TevFixedReader.FindFieldDefByName(const AFieldName: String): IFixedReaderFieldDefinition;
var
  i: integer;
  FieldDef: IFixedReaderFieldDefinition;
begin
  Result := nil;

  for i := 0 to FFieldDefs.Count - 1 do
  begin
    FieldDef := IInterface(FFieldDefs.Values[i].Value) as IFixedReaderFieldDefinition;
    if FieldDef.GetName = AFieldName then
    begin
      Result := FieldDef;
      exit;
    end;
  end;
end;

{ TFixedReaderFieldDefinition }

procedure TFixedReaderFieldDefinition.CheckAndSwapPos;
var
  Position: integer;
begin
  if FEnd < FStart then
  begin
    Position := FEnd;
    FEnd := FStart;
    FStart := Position;
  end;
end;

destructor TFixedReaderFieldDefinition.Destroy;
begin
  inherited;
end;

function TFixedReaderFieldDefinition.GetAsString: String;
begin
  CheckCondition(FName <> '', 'Field name is empty');
  Result := FName + ':' + IntToStr(FStart) + '-' + IntToStr(FEnd);
end;

function TFixedReaderFieldDefinition.GetEndPos: integer;
begin
  Result := FEnd;
end;

function TFixedReaderFieldDefinition.GetLength: integer;
begin
  Result := FEnd - FStart + 1;
end;

function TFixedReaderFieldDefinition.GetName: String;
begin
  Result := FName;
end;

function TFixedReaderFieldDefinition.GetStartPos: integer;
begin
  Result := FStart;
end;

procedure TFixedReaderFieldDefinition.SetFromString(const AFieldDefinition: String);
var
  Position: integer;
  S, sName, sStart, SEnd: String;
  iStart, iEnd: integer;
begin
  Position := Pos(':', AFieldDefinition);
  CheckCondition(Position <> 0, 'Cannot find ":" character in order to figure out field''s name. String: ' + AFieldDefinition);
  sName := Copy(AFieldDefinition, 1, Position - 1);
  CheckCondition(sName <> '', 'Field name cannot be empty');
  SetName(sName);

  S := Copy(AFieldDefinition, Position + 1, Length(AFieldDefinition));
  Position := Pos('-', S);
  CheckCondition(Position <> 0, 'Cannot find "-" character in order to figure out field''s positions. String: ' + S);

  sStart := Trim(Copy(S, 1, Position - 1));
  CheckCondition(Length(sStart) > 0, 'Cannot figure out the start position');
  try
    iStart := StrToInt(sStart);
  except
    on E:Exception do
      raise Exception.Create('Start position is not integer value: ' + sStart);
  end;

  sEnd := Trim(Copy(S, Position + 1, Length(S)));
  CheckCondition(Length(sEnd) > 0, 'Cannot figure out the end position');
  try
    iEnd := StrToInt(sEnd);
  except
    on E:Exception do
      raise Exception.Create('End position is not integer value: ' + sEnd);
  end;

  SetPositions(iStart, iEnd);
end;

procedure TFixedReaderFieldDefinition.SetName(const AName: String);
begin
  CheckCondition(Pos(':', AName) = 0, 'Field name cannot include ":" characters');
  CheckCondition(Pos(';', AName) = 0, 'Field name cannot include ";" characters');
  FName := Trim(AName);
end;

procedure TFixedReaderFieldDefinition.SetPositions(const AStartPos,
  AEndPos: integer);
begin
  CheckCondition(AStartPos > 0, 'Start position cannot be below 1');
  FStart := AStartPos;

  CheckCondition(AEndPos > 0, 'End position cannot be below 1');
  FEnd := AEndPos;

  CheckAndSwapPos;
end;

function TFixedReaderFieldDefinition.StartPosAsString: String;
begin
  Result := PadStringLeft(IntToStr(FStart), '0', 6);
end;

end.
