unit EvExchangeMod;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvConsts, EvStreamUtils, EvTypes, isLogFile, EvExchangeConsts, 
     isBasicUtils,  evContext, EvUtils;

implementation

uses EvCSVReader, EvExchangePackage, EvExchangePackageExt, EvDataset, DB, EvExchangeUtils, EvInitApp,
  EvFixedReader, Variants;

const
  EvoXPkgTag = 'EVOX_PACKAGE';

type
  TEvExchangeMod = class(TisInterfacedObject, IevExchange)
  private
    FPackagesList: IisListOfValues;
    FReadersList: IisStringList;
  protected
    procedure DoOnConstruction; override;

    // IEvExchange implementation
    function   GetEvExchangePackagesList : IisStringListRO;
    function   GetEvExchangePackage(const APackageName : String): IEvExchangePackage;
    function   GetDataReadersList : IisStringListRO;
    function   GetDataReader(const ADataReaderName : String) : IEvExchangeDataReader;
    procedure  UpgradeMapFileToLastPackageVer(const AMapFile: IEvExchangeMapFile);
    procedure  PreparePackageGroupRecords(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage);
    procedure  UpgradeDataTypesInMapFile(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage);
    function   Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile; const AOptions: IisStringListRO;
      const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues; // input dataset will be changed after import!!!
    function   CheckMapFile(const AMapFile: IEvExchangeMapFile; const AOptions: IisStringListRO): IisListOfValues;
  public
    destructor Destroy; override;
  end;

function GetEvExchange: IInterface;
begin
  if Context.License.EvoX then
    Result := TEvExchangeMod.Create
  else
    Result := nil;
end;

{ TEvExchangeMod }

function TEvExchangeMod.CheckMapFile(const AMapFile: IEvExchangeMapFile; const AOptions: IisStringListRO): IisListOfValues;
var
  Pkg: IEvExchangePackage;
  Engine: IEvExchangeEngine;
begin
  CheckCondition(Assigned(AMapFile), 'Object with map data is not assigned');

  try
    Pkg := GetEvExchangePackage(AMapFile.GetPackageName);
  except
    on E: Exception do
      raise Exception.Create('Cannot find package with name: ' + AMapFile.GetPackageName);
  end;

  Engine := Context.EvolutionExchangeEngine;

  CheckCondition(Assigned(Engine), 'Cannot find import engine with name: ' + Pkg.GetEngineName);

  PreparePackageGroupRecords(AMapFile, Pkg);
  Result := Engine.CheckMapFile(AMapFile, Pkg, AOptions);
end;

destructor TEvExchangeMod.Destroy;
begin
  FPackagesList.Clear;
  FReadersList.Clear;
  inherited;
end;

procedure TEvExchangeMod.DoOnConstruction;
var
  Package: IEvExchangePackage;
  Q: IevQuery;
  s: String;
begin
  inherited;
  FPackagesList := TisListOfValues.Create;
  Q := TevQuery.Create('SELECT STORAGE_DATA FROM SY_STORAGE WHERE {AsOfNow<SY_STORAGE>} AND TAG LIKE ''' + EvoXPkgTag + '%''');
  if Q.Result.RecordCount > 0 then
  begin
    Q.Result.First;
    while not Q.Result.eof do
    begin
      s := TBlobField(Q.Result.FieldByName('STORAGE_DATA')).AsString;
      try
        Package := TEvExchangePackageExt.Create;
       (Package as IEvExchangePackageExt).LoadFromXMLString(s);
        FPackagesList.AddValue(Package.GetName, Package);
      except
        on E: Exception do
          mb_LogFile.AddEvent(etError, 'EvExchange', 'Error during intialization of package. Package name: ' + Package.GetName,
            'Record# ' + IntToStr(Q.Result.RecNo) + '. Error: "' + E.Message + '"');
      end;
      Q.Result.Next;
    end;
  end;

  FReadersList := TisStringList.Create;
  FreadersList.Add(sCSVReaderName);
  FreadersList.Add(sASCIIFixedReaderName);
end;

function TEvExchangeMod.GetDataReader(const ADataReaderName: String): IEvExchangeDataReader;
begin
  Result := nil;
  if ADataReaderName = sCSVReaderName then
    Result := TevCSVReader.Create;
  if ADataReaderName = sASCIIFixedReaderName then
    Result := TevFixedReader.Create as IEvExchangeDataReader;
end;

function TEvExchangeMod.GetDataReadersList: IisStringListRO;
begin
  Result := FReadersList as IisStringListRO;
end;

function TEvExchangeMod.GetEvExchangePackage(const APackageName: String): IEvExchangePackage;
begin
  Result :=  (IInterface(FPackagesList.Value[APackageName]) as IEvExchangePackage).GetClone as IEvExchangePackage;
end;

function TEvExchangeMod.GetEvExchangePackagesList: IisStringListRO;
var
  i: integer;
  tmpList: IisStringList;
begin
  tmpList := TisStringList.Create;
  for i := 0 to FPackagesList.Count - 1 do
    tmpList.Add(FPackagesList.Values[i].Name);
  Result := tmpList as IisStringListRO;  
end;

function TEvExchangeMod.Import(const AInputData: IevDataSet; const AMapFile: IEvExchangeMapFile;
  const AOptions: IisStringListRO; const ACustomCoNbr: String = ''; AEffectiveDate: TDateTime = 0): IisListOfValues;
var
  Pkg: IEvExchangePackage;
  Engine: IEvExchangeEngine;
  EffectiveDate: TDateTime;
begin
  CheckCondition(Assigned(AMapFile), 'Object with map data is not assigned');
  CheckCondition(Assigned(AInputData), 'Object with input data is not assigned');

  if AEffectiveDate = 0 then
    EffectiveDate := SysTime
  else
    EffectiveDate := AEffectiveDate;

  try
    Pkg := GetEvExchangePackage(AMapFile.GetPackageName);
  except
    on E: Exception do
      raise Exception.Create('Cannot find package with name: ' + AMapFile.GetPackageName);
  end;

  CheckCondition(Assigned(Pkg), 'Cannot find package with name: ' + AMapFile.GetPackageName);
  Engine := Context.EvolutionExchangeEngine;
  CheckCondition(Assigned(Engine), 'Cannot find import engine with name: ' + Pkg.GetEngineName);

  PreparePackageGroupRecords(AMapFile, Pkg);
  Result := Engine.Import(AInputData, AMapFile, Pkg, AOptions, ACustomCoNbr, EffectiveDate);
end;

procedure TEvExchangeMod.PreparePackageGroupRecords(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage);
var
  i, j: integer;
  PackageField: IEvExchangePackageField;
  MapField: IevEchangeMapField;
  FieldName: String;
  SortedMapFieldsList: IisStringList;
begin
  // next code depends on using ExchangeFieldName as Name for Map's list of fields
  SortedMapFieldsList := TisStringList.Create;
  for i := 0 to AMapFile.GetFieldsList.Count - 1 do
    SortedMapFieldsList.Add(AMapFile.GetFieldsList.Values[i].Name);
  SortedMapFieldsList.Sort;

  for i := 0 to SortedMapFieldsList.Count - 1 do
  begin
    MapField := IInterface(AMapFile.GetFieldsList.Value[SortedMapFieldsList[i]]) as IevEchangeMapField;
    if APackage.GetFieldsList.ValueExists(MapField.GetExchangeFieldName) then
      continue;

    // field not found in package, it could be only group record field, let's create appropriate group record
    CheckCondition(MapField.GetGroupRecordNumber > 0, 'Group record number is not assigned for input field "' + MapField.GetInputFieldName + '"');
    j := Pos(RecordNumberToStr(MapField.GetGroupRecordNumber), MapField.GetExchangeFieldName);
    CheckCondition(j > 0, 'Cannot find record number in package field''s name "' + MapField.GetExchangeFieldName + '" for input field "' + MapField.GetInputFieldName + '"');
    FieldName := Trim(Copy(MapField.GetExchangeFieldName, 1, j - 1));
    PackageField := APackage.GetFieldByName(FieldName);
    CheckCondition(Assigned(PackageField), 'Cannot find package field "' + FieldName + '" for input field "' + MapField.GetInputFieldName + '"');
    CheckCondition(PackageField.IsGroupDefinitionField, 'Package field "' + FieldName + '" is not group definition field. Input field "' + MapField.GetInputFieldName + '"');
    APackage.AddRecordWithNumberToGroup(PackageField.GetGroupName, MapField.GetGroupRecordNumber);
  end;
end;

procedure TEvExchangeMod.UpgradeDataTypesInMapFile(const AMapFile: IEvExchangeMapFile; const APackage: IEvExchangePackage);
var
  i: integer;
  PackageField: IEvExchangePackageField;
  MapField: IevEchangeMapField;
  MapFields: IisListOfValues;
begin
  MapFields := AMapFile.GetFieldsList;
  for i := MapFields.Count - 1 downto 0 do
  begin
    MapField := IInterface(MapFields.Values[i].Value) as IevEchangeMapField;
    PackageField := APackage.GetFieldByName(MapField.GetExchangeFieldName);
    if MapField.GetFieldType <> PackageField.GetType then
    begin
      MapField.SetFieldType(PackageField.GetType);
      MapField.SetFieldSize(PackageField.GetFieldSize);
    end;
  end;
end;

procedure TEvExchangeMod.UpgradeMapFileToLastPackageVer(const AMapFile: IEvExchangeMapFile);
var
  Pkg: IEvExchangePackage;
  MapFields: IisListOfValues;
  MapField: IevEchangeMapField;
  PackageField: IEvExchangePackageField;
  sExchangeFieldName, SGroupPrefix: String;
  i: integer;
begin
  try
    Pkg := GetEvExchangePackage(AMapFile.GetPackageName);
  except
    on E: Exception do
      raise Exception.Create('Cannot find package with name: ' + AMapFile.GetPackageName);
  end;

  if AMapFile.GetPackageVersion > Pkg.GetVersion then
    exit;

  // assigning map fields to group fields
  if (AMapFile.GetPackageVersion < 8) and AnsiSameText(Pkg.GetName, EvoXEBasicEEPackage) then
  begin
    MapFields := AMapFile.GetFieldsList;
    for i := MapFields.Count - 1 downto 0 do
    begin
      MapField := IInterface(MapFields.Values[i].Value) as IevEchangeMapField;
      PackageField := Pkg.GetFieldByName(MapField.GetExchangeFieldName);
      if PackageField.IsBelongsToGroup and PackageField.IsGroupDefinitionField then
      begin
        AMapFile.DeleteFieldByExchangeName(MapField.GetExchangeFieldName);
        MapField.SetExchangeFieldName(PackageField.GetExchangeFieldName + ' ' + RecordNumberToStr(1));
        MapField.SetGroupRecordNumber(1);
        AMapFile.AddField(MapField);
      end;
    end;
    AMapFile.SetPackageInformation(Pkg);
  end;

  // deleting fields which belongs to "Dependents Benefits" group of fields
  if (AMapFile.GetPackageVersion < 6) and AnsiSameText(Pkg.GetName, EvoXHrPackage) then
  begin
    MapFields := AMapFile.GetFieldsList;
    for i := MapFields.Count - 1 downto 0 do
    begin
      MapField := IInterface(MapFields.Values[i].Value) as IevEchangeMapField;
      if MapField.GetGroupRecordNumber <> 0 then
      begin
        sGroupPrefix := RecordNumberToStr(MapField.GetGroupRecordNumber);
        sExchangeFieldName := StringReplace(MapField.GetExchangeFieldName, ' ' + sGroupPrefix, '', [rfReplaceAll	]);
        PackageField := Pkg.GetFieldByName(sExchangeFieldName);
        if PackageField.IsBelongsToGroup and AnsiSameText(PackageField.GetGroupName, EvoXHRDependentsBenefits) then
          AMapFile.DeleteFieldByExchangeName(MapField.GetExchangeFieldName);
      end;
    end;
  end;

  // renaming "EE Benefit reference" to "EE Benefit Amount Type"
  if (AMapFile.GetPackageVersion < 6) and AnsiSameText(Pkg.GetName, EvoXScheduledEDPackage) then
  begin
    MapFields := AMapFile.GetFieldsList;
    for i := MapFields.Count - 1 downto 0 do
    begin
      MapField := IInterface(MapFields.Values[i].Value) as IevEchangeMapField;
      if AnsiSameText(MapField.GetExchangeFieldName, 'EE Benefit Reference') then
      begin
        MapField.SetExchangeFieldName('EE Benefit Amount Type');
        AMapFile.DeleteFieldByExchangeName('EE Benefit Reference');
        AMapFile.AddField(MapField);
        break;
      end;
    end;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEvExchange, IevExchange, 'EvoX Local Module');
end.
