unit EvCSVReader;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvConsts, EvStreamUtils, EvTypes, ISBasicUtils, DB, EvExchangeConsts, isLogFile;

type
  ErrorInFieldException = class(Exception);

  TevCSVReader = class(TisInterfacedObject, IEvExchangeDataReader)
  private
    FSupportedFormatNames: IisStringList;
    FSupportedFormatFileExt: IisStringList;
    FDescriptionOfOptions: IisStringList;
    FDataReaderOptions: IisStringList;
  protected
    procedure DoOnConstruction; override;
    function  InternalReadData(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AMapFile: IEvExchangeMapFile = nil; const AAmountOfRows: integer = -1): IEvDataset;
    function  GetNextRowData(const AStream: IevDualStream): IisStringList;
    function  GetAllRowsData(const AStream: IevDualStream; const ATheSameNumberOfFields: boolean = true): IisListOfValues;
    function  GetDelimiter: String;
    function  isDoubleQuoteRequired: boolean;
    function  GetDoubleQuoteChar: Char;
    function  GetNamesInFirstRow: boolean;
    function  GetMaxFieldSize(AAllRowsData: IisListOfValues; AFieldIndex: integer) : integer;

    // IEvExchangeDataReader implementation
    function  GetSupportedFormatNames: IisStringListRO;
    function  GetSupportedFormatFileExt: IisStringListRO;
    function  GetDescriptionOfOptions: IisStringListRO;
    function  GetDataReaderOptions: IisStringList;
    procedure SetDataReaderOptions(const AOptions: IisStringList);
    procedure SetDefaultDataReaderOptions;
    function  ReadExampleFromFile(const AFileName: String; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer = 1): IevDataSet;
    function  ReadExampleFromStream(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer = 1): IevDataSet;
    function  ReadDataFromFile(const AFileName: String; const AMapFile : IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
    function  ReadDataFromStream(const AStream: IevDualStream; const AMapFile : IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
    function  GetReaderName : String;
  public
    destructor Destroy; override;
  end;

implementation

uses EvDataset;

{ TevCSVReader }

destructor TevCSVReader.Destroy;
begin
  FSupportedFormatNames.Clear;
  FSupportedFormatFileExt.Clear;
  FDescriptionOfOptions.Clear;
  FDataReaderOptions.Clear;
  inherited;
end;

procedure TevCSVReader.DoOnConstruction;
begin
  inherited;
  FSupportedFormatNames := TisStringList.Create;
  FSupportedFormatNames.Add('CSV=Comma separated values');

  FSupportedFormatFileExt := TisStringList.Create;
  FSupportedFormatFileExt.Add('*.csv=CSV files');
  FSupportedFormatFileExt.Add('*.txt=Text files');
  FSupportedFormatFileExt.Add('*.prn=Text files');

  FDescriptionOfOptions := TisStringList.Create;
  FDescriptionOfOptions.Add(fieldNamesInFirstRow + '=The first row of input file contains field names, possible values: 0 (default) or 1');
  FDescriptionOfOptions.Add(fieldDelimiter + '=Field delimiter character, possible values: 0 - comma (default), 1 - tab');
  FDescriptionOfOptions.Add(DoubleQuoteChar + '=Double quote character, possible values: 0 - " (default), 1 - ''');
  FDescriptionOfOptions.Add(DoubleQuoteRequired + '=all fields should be enclosed in double quotes , possible values: 0 - no (default), 1 - yes');

  FDataReaderOptions := TisStringList.Create;
  SetDefaultDataReaderOptions;
end;

function TevCSVReader.GetDescriptionOfOptions: IisStringListRO;
begin
  Result := FDescriptionOfOptions as IisStringListRO;
end;

function TevCSVReader.GetReaderName: String;
begin
  Result := sCSVReaderName;
end;

function TevCSVReader.GetSupportedFormatFileExt: IisStringListRO;
begin
  Result := FSupportedFormatFileExt as IisStringListRO;
end;

function TevCSVReader.GetSupportedFormatNames: IisStringListRO;
begin
  Result := FSupportedFormatNames as IisStringListRO;
end;

function TevCSVReader.ReadExampleFromFile(const AFileName: String; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer): IevDataSet;
var
  InputStream : IevDualStream;
begin
  CheckCondition(FileExists(AFileName), 'File does not exist :' + AFileName);
  InputStream := TevDualStreamHolder.CreateInMemory;
  InputStream.LoadFromFile(AFileName);
  Result := ReadExampleFromStream(InputStream, AErrorsList, AAmountOfRows);
end;

function TevCSVReader.ReadExampleFromStream(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AAmountOfRows: integer): IevDataSet;
begin
  CheckCondition(Assigned(AErrorsList), 'Errors list object is not assigned');
  Result := InternalReadData(AStream, AErrorsList, nil, AAmountOfRows);
end;

function TevCSVReader.ReadDataFromFile(const AFileName: String; const AMapFile: IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
var
  InputStream : IevDualStream;
begin
  CheckCondition(FileExists(AFileName), 'File does not exist :' + AFileName);
  InputStream := TevDualStreamHolder.CreateInMemory;
  InputStream.LoadFromFile(AFileName);
  Result := ReadDataFromStream(InputStream, AMapFile, AErrorsList);
end;

function TevCSVReader.ReadDataFromStream(const AStream: IevDualStream; const AMapFile: IEvExchangeMapFile; const AErrorsList: IEvExchangeResultLog): IevDataSet;
begin
  CheckCondition(Assigned(AMapFile), 'Map file object is not assigned');
  CheckCondition(Assigned(AErrorsList), 'Errors list object is not assigned');
  Result := InternalReadData(AStream, AErrorsList, AMapFile);
end;

procedure TevCSVReader.SetDataReaderOptions(const AOptions: IisStringList);
var
  i, j: integer;
  sNewValue, sOldValue: String;
  found : boolean;
begin
  if AOptions.Count = 0 then
    SetDefaultDataReaderOptions
  else
    for i := 0 to AOptions.Count - 1 do
    begin
      found := false;
      for j := 0 to FDataReaderOptions.Count - 1 do
      begin
        if CompareText(AOptions.Names[i], FDataReaderOptions.Names[j]) = 0 then
        begin
          found := true;
          sNewValue := AOptions.Values[AOptions.Names[i]];
          sOldValue := FDataReaderOptions.Values[FDataReaderOptions.Names[j]];
          if sOldValue <> sNewValue then
          begin
            CheckCondition(((sNewValue = '0') or (sNewValue = '1')), 'Unsupported option''s value: ' + sNewValue);
            FDataReaderOptions.Values[FDataReaderOptions.Names[j]] := sNewValue;
          end;
          break;
        end;
      end;  // j
      CheckCondition(found, 'Unsupported option: ' + AOptions.Names[i]);
    end;  // i
end;

function TevCSVReader.InternalReadData(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog; const AMapFile: IEvExchangeMapFile = nil;
 const AAmountOfRows: integer = -1): IEvDataset;
var
  CurrentRowData: IisStringList;
  AllRowsData: IisListOfValues;
  maxFieldSize: integer;
  fieldName : String;
  i, j, start, cnt: integer;
  formatSettings: TFormatSettings;
begin
  Result := TevDataSet.Create;

  AErrorsList.Clear;
  FillChar(formatSettings, SizeOf(formatSettings), 0);
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
  CheckCondition(Assigned(Astream), 'Input stream is not assigned');
  CheckCondition(AStream.Size > 0, 'Input stream is empty');

  try
    AllRowsData := GetAllRowsData(AStream);
  except
    on E: Exception do
    begin
      AErrorsList.AddEvent(etError, E.Message);
      Exit;
    end;
  end;
  CheckCondition(AllRowsData.Count > 0, 'No input data found');

  // defining fields according to input data, all fields has the same type - string
  CurrentRowData := IInterface(AllRowsData.Values[0].Value) as IisStringList;
  for i := 0 to CurrentRowData.Count - 1 do
  begin
    maxFieldSize := GetMaxFieldSize(AllRowsData, i);
    if GetNamesInFirstRow then
      fieldName := Trim(CurrentRowData[i])
    else
      fieldName := '#' + IntToStr(i+1);
    Result.vclDataSet.FieldDefs.Add(fieldName, ftString, maxFieldSize, false);
  end;

  // storing data into dataset
  if GetNamesInFirstRow then
    start := 1
  else
    start := 0;

  cnt := 0;
  Result.vclDataSet.Active := true;
  for i := start to AllRowsData.Count - 1 do
  begin
    if (AAmountOfRows >= 0) and (cnt >= AAmountOfRows) then
      exit;
    try
      CurrentRowData := IInterface(AllRowsData.Values[i].Value) as IisStringList;
      CheckCondition(CurrentRowData.Count >= Result.vclDataSet.FieldDefs.Count, 'Input file has less fields than it supposed. Input fields: '
        + IntToStr(CurrentRowData.Count) + ' Supposed to have: ' + IntToStr(Result.vclDataSet.FieldDefs.Count));
      Result.Append;
      for j := 0 to Result.vclDataSet.FieldDefs.Count - 1 do
      begin
        try
          if Result.vclDataSet.FieldDefs[j].DataType = ftDateTime then
          begin
            CheckCondition(Assigned(AMapFile), 'Cannot find date format due map file is not assigned');
            formatSettings.ShortDateFormat := (IInterface(AMapFile.GetFieldsList.Values[j].Value) as IevEchangeMapField).GetDateFormat;
            formatSettings.ShortTimeFormat := (IInterface(AMapFile.GetFieldsList.Values[j].Value) as IevEchangeMapField).GetTimeFormat;
            Result.vclDataSet.Fields[j].Value := StrToDateTime(CurrentRowData[j], formatSettings);
          end
          else
          if Result.vclDataSet.FieldDefs[j].DataType = ftBoolean then
            if (UpperCase(Trim(CurrentRowData[j])) = 'TRUE') or (Trim(CurrentRowData[j]) = '1') then
              Result.vclDataSet.Fields[j].Value := true
            else
              Result.vclDataSet.Fields[j].Value := false
          else
            Result.vclDataSet.Fields[j].Value := CurrentRowData[j];
        except
          on E: Exception do
          begin
            AErrorsList.AddEvent(etError, 'Row# ' + IntToStr(i + 1) + ' Field# ' + IntToStr(j + 1) + ' Value: "' + CurrentRowData[j] + '". Error: ' + E.Message);
            raise ErrorInFieldException.Create(AErrorsList.Items[AErrorsList.Count-1].GetText);
          end;
        end;
      end;  // j
      Result.Post;
    except
      on E: ErrorInFieldException do
        Result.vclDataSet.Cancel;
    end;
    Inc(cnt);
  end;  // i
end;

function TevCSVReader.GetMaxFieldSize(AAllRowsData: IisListOfValues; AFieldIndex: integer) : integer;
var
  start, max, current, i: integer;
  CurrentRowData: IisStringList;
begin
  result := EvoXMaxUnknownFieldSize;
  if GetNamesInFirstRow then
    start := 1
  else
    start := 0;

  max := 0;
  for i := start to AAllRowsData.Count - 1 do
  begin
    CurrentRowData := IInterface(AAllRowsData.Values[i].Value) as IisStringList;
    current := Length(CurrentRowData[AFieldindex]);
    if current > max then
      max := current;
  end;

  if max <> 0 then
    Result := max;
end;

procedure TevCSVReader.SetDefaultDataReaderOptions;
begin
  FDataReaderOptions.Clear;
  FDataReaderOptions.Add(fieldDelimiter + '=0');
  FDataReaderOptions.Add(fieldNamesInFirstRow + '=0');
  FDataReaderOptions.Add(doubleQuoteChar + '=0');
  FDataReaderOptions.Add(doubleQuoteRequired + '=0');
end;

// See RFC 4180 for description of supported CSV format
function TevCSVReader.GetNextRowData(const AStream: IevDualStream): IisStringList;
var
  Delimiter: String;
  sFieldData: String;
  DQ: Char;
  DQRequired: boolean;
  EndOfRow: boolean;

  function GetNextChar: Char;
  var
    NextChar: Char;
    oldPos: integer;
  begin
    Result := #0;
    if not AStream.eos then
    begin
      Result := Char(Astream.ReadByte);
      if (Result = #13) or (Result = #10) then
      begin
        Result := #13;
        if not AStream.eos then
        begin
          oldPos := AStream.Position;
          NextChar := Char(AStream.ReadByte);
          AStream.Position := oldPos;
          if (NextChar = #13) or (NextChar = #10) then
            AStream.Position := AStream.Position + 1;
        end;
      end;
    end;
  end;

  function CheckNextChar: Char;
  var
    oldPos: integer;
  begin
    Result := #0;
    if not AStream.eos then
    begin
      oldPos := AStream.Position;
      Result := GetNextChar;
      AStream.Position := oldPos;
    end;
  end;

  procedure SkipNextTillDelimiter(var AEndOfRow: boolean);
  var
    Data: char;
  begin
    while not AStream.eos do
    begin
      Data := GetNextChar;
      if Data = Delimiter then
        exit;
      if (Data = #0) or (Data = #13) then
      begin
        AEndOfRow := true;
        exit;
      end;
    end;
  end;

  procedure SkipSpacesTillDQ(var AEndOfRow: boolean);
  var
    Data: char;
  begin
    while not AStream.eos do
    begin
      Data := CheckNextChar;
      if Data = ' ' then
        GetNextChar
      else
      if Data = DQ then
        exit
      else
      if Data = Delimiter then
        exit
      else
      if (Data = #0) or (Data = #13) then
      begin
        AEndOfRow := true;
        exit;
      end
      else
        GetNextChar;
    end;
  end;

  function ReadFieldData(var AEndOfRow: boolean): String;
  var
    Data, NextData: Char;
    FieldIsDoubleQuoted: boolean;
  begin
    Result := '';
    FieldIsDoubleQuoted := false;

    Data := GetNextChar;
    while Data <> #0 do
    begin
      if not FieldIsDoubleQuoted then
      begin
        if Data = Delimiter then
          exit;
        if (Data = #13) then
        begin
          AEndOfRow := true;
          exit;
        end;
        if (Result = '') then
        begin
          if Data = DQ then
            FieldIsDoubleQuoted := true
          else
            if not DQRequired then
              Result := Result + String(Data)
            else
              SkipSpacesTillDQ(AEndOfRow);
        end
        else
          if Data <> DQ then
            Result := Result + String(Data)
          else
            raise Exception.Create('Fields should be enclosed with double quotes in order to double quotes may appear inside the fields');
      end
      else
      begin
        if Data = DQ then
        begin
          NextData := CheckNextChar;
          if NextData = DQ then
          begin
            AStream.Position := AStream.Position + 1;
            Result := Result + String(DQ);
          end
          else
          begin
            SkipNextTillDelimiter(AEndOfRow);
            exit;
          end;
        end
        else
        if Data = #13 then
          Result := Result + #13#10
        else
          Result := Result + String(Data)
      end;

      Data := GetNextChar;
    end;  // while
    if Data = #0 then
      AEndOfRow := true;
  end;

begin
  Result := TisStringList.Create;
  Delimiter := GetDelimiter;
  DQ := GetDoubleQuoteChar;
  DQRequired := isDoubleQuoteRequired;

  EndOfRow := false;
  while (not EndOfRow) and (not AStream.eos) do
  begin
    sFieldData := ReadFieldData(EndOfRow);
    Result.Add(sFieldData)
  end;
end;

function TevCSVReader.GetAllRowsData(const AStream: IevDualStream; const ATheSameNumberOfFields: boolean = true): IisListOfValues;
var
  CurrentRowData: IisStringList;
  FieldsCount: integer;
  RowNumber: integer;
  LastChar: Char;
  Pos: integer;
begin
  CheckCondition(AStream.Size > 0, 'File is empty!');

  // removing trailing empty rows
  AStream.Position := 0;
  Pos := -1;
  while not AStream.eos do
  begin
    LastChar := char(AStream.ReadByte);
    if (LastChar <> #13) and (LastChar <> #10) then
      Pos := -1
    else
      if Pos = -1 then
        Pos := AStream.Position;
  end;
  if Pos <> -1 then
    AStream.Size := Pos;

  // adding #13 to end of stream if it is not there
  AStream.Position := AStream.Size - 1;
  LastChar := char(AStream.ReadByte);
  if (LastChar <> #13) and (LastChar <> #10) then
    AStream.WriteByte(byte(#13));

  Result := TisListOfValues.Create;
  AStream.Position := 0;
  RowNumber := 0;
  FieldsCount := 0;

  while true do
  begin
    try
      CurrentRowData := GetNextRowData(AStream);
    except
      on E: Exception do
        raise Exception.Create('Row# ' + IntToStr(RowNumber + 1) + '. Error: ' + E.Message);
    end;
    if RowNumber = 0 then
    begin
      CheckCondition(CurrentRowData.Count > 0, 'Input has no data');
      FieldsCount := CurrentRowData.Count;
    end;
    if CurrentRowData.Count = 0 then
      exit;
    if ATheSameNumberOfFields then
      CheckCondition(FieldsCount = CurrentRowData.Count, 'All records should have the same amount of fields');
    Result.AddValue(IntToStr(Result.Count), CurrentRowData);
    Inc(RowNumber);
  end;
end;

function TevCSVReader.GetDelimiter: String;
begin
  if FDataReaderOptions.Values[fieldDelimiter] = '1' then
    Result := #9
  else
    Result := ',';
end;

function TevCSVReader.GetNamesInFirstRow: boolean;
begin
  if FDataReaderOptions.Values[fieldNamesInFirstRow] = '1' then
    Result := true
  else
    Result := false;
end;

function TevCSVReader.GetDoubleQuoteChar: Char;
begin
  if FDataReaderOptions.Values[doubleQuoteChar] = '0' then
    Result := '"'
  else
    Result := '''';
end;

function TevCSVReader.isDoubleQuoteRequired: boolean;
begin
  if FDataReaderOptions.Values[doubleQuoteRequired] = '1' then
    Result := true
  else
    Result := false;
end;

function TevCSVReader.GetDataReaderOptions: IisStringList;
begin
  Result := TisStringList.Create;
  Result.Text := FDataReaderOptions.Text;
end;

end.
