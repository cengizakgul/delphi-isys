// Proxy Module "Time Source"
unit EvTimeSourcePxy;

interface

implementation

uses SysUtils, Windows, isBaseClasses, EvCommonInterfaces, EvMainboard, EvTransportDatagrams,
     evRemoteMethods, EvStreamUtils, DateUtils, EvConsts, evProxy, EvTransportInterfaces;

type
  TevTimeSourceProxy = class(TevProxyModule, IevTimeSource)
  private
    FTimeDiscrepancy: TDateTime;
    FLastSyncTime: TDateTime;
    FLastRequestLocalTime: TDateTime;
    FLastRequestTicks: Cardinal;
  protected
    function GetDateTime: TDateTime;
  end;


function GetTimeSourceProxy: IevTimeSource;
begin
  Result := TevTimeSourceProxy.Create;
end;


{ TevTimeSource }

function TevTimeSourceProxy.GetDateTime: TDateTime;
var
  D, Res: IevDatagram;
  dLocalTime, dServerTime: TDateTime;
  RoundTripTimeMsec, PevRoundTripTimeMsec: Cardinal;
  i: Integer;
begin
  // synchronize every 5 mins or if local time was adjusted
  if (MinutesBetween(Now, FLastSyncTime) >= 5) or
     (Abs(Abs(GetTickCount - FLastRequestTicks) - MilliSecondsBetween(Now, FLastRequestLocalTime)) > 500) then
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(sGuestUserName, '');
      D := CreateDatagram(METHOD_TIMESOURCE_GETDATETIME);
      PevRoundTripTimeMsec := MaxInt;
      FTimeDiscrepancy := 0;
      for i := 1 to 3 do
      begin
        dLocalTime := Now;

        RoundTripTimeMsec := GetTickCount;
        Res := SendRequest(D, False);
        RoundTripTimeMsec := GetTickCount - RoundTripTimeMsec;

        dServerTime := Res.Params.Value['Result'];
        if PevRoundTripTimeMsec > RoundTripTimeMsec then
        begin
          FTimeDiscrepancy := IncMilliSecond(dServerTime, -Integer(RoundTripTimeMsec div 2)) - dLocalTime;
          PevRoundTripTimeMsec := RoundTripTimeMsec;
        end;
        if RoundTripTimeMsec < 100 then
          break;
      end;

      FLastSyncTime := Now;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

  FLastRequestLocalTime := Now;
  FLastRequestTicks := GetTickCount;
  Result := FLastRequestLocalTime + FTimeDiscrepancy;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTimeSourceProxy, IevTimeSource, 'Time Source');

end.