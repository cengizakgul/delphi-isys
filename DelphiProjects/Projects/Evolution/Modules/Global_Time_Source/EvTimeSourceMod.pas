unit EvTimeSourceMod;

interface

uses Classes, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard;


implementation

type
  TevTimeSource = class(TisInterfacedObject, IevTimeSource)
  protected
    function GetDateTime: TDateTime;
  end;


function GetTimeSource: IevTimeSource;
begin
  Result := TevTimeSource.Create;
end;


{ TevTimeSource }

function TevTimeSource.GetDateTime: TDateTime;
begin
  Result := Now;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTimeSource, IevTimeSource, 'Time Source');


end.
