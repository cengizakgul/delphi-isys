// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit rwLocalReportMod;

interface

uses Windows, Messages, SysUtils, Classes, Controls,
     EvTypes, DB, EvUtils,  EvStreamUtils, SReportSettings, isErrorUtils,
     Forms, EvConsts, ActnList, Dialogs, ISBasicClasses, isBaseClasses, rwEvUtils,
     EvReportWriterProxy, EvDataAccessComponents, ISDataAccessComponents, EvCommonInterfaces,
     EvContext, rwInputFormContainerFrm, rwPreviewContainerFrm, rwModalPreviewFormFrm, EvMainboard,
     EvExceptions, EvUIUtils, EvClientDataSet, isAppIDs, ISBasicUtils;

type

  TrwLocalEngineObject = class(TisInterfacedObject, IevRWLocalEngine)
  private
    FRWProxy: IevReportWriterProxy;
    FGroupCounter: Integer;
    FRepList:      TrwReportList;
    FLastExceptions: String;
    FLastWarnings: String;
    FUnPrintedReportsStorage: TrwReportResults;

    function  IsGroupStarted: Boolean;
    procedure GetReportNbrByName(const aName: string; out Nbr: Integer; out aLevel: string);
    function  PrinterForChecks: TevRWPrinterRec;
    function  PrinterForReports: TevRWPrinterRec;
    function  PrinterSetting(const APrinterName: String): TevRWPrinterRec;
    function  SaveASCIIResult(AResults: TrwReportResults): String;
    function  PrepareResultForPreview(AResults: TrwReportResults): TrwReportResults;
    procedure DoEndGroup(const ADestination: TrwReportDestination; const Descr: string;
               const OverrideCoNbr: Integer; const OverridePrNbr: Integer;
               const OverrideEventDate: TDateTime);
    function  CalcQueueReports(ARepList: TrwReportList; ADestination: TrwReportDestination): TisGUID;
    function PrinterForFile: TevRWPrinterRec;
  protected
    procedure DoOnConstruction; override;
    function  rwProxy: IevReportWriterProxy;

  public
    destructor  Destroy; override;

    function  ReportWriteProxy: IInterface;
    procedure BeginWork;
    procedure EndWork;
    procedure SetUnprintedReportsStorage(const AValue: TrwReportResults);
    procedure CompileReport(AData: IEvDualStream; var AClassName: String; var AAncestorClassName: String);
    procedure Preview(AResults: TrwReportResults; AParent: TrwPreviewContainer = nil; const ASecuredPreview: Boolean = False); overload;
    procedure Preview(AResults: IisStream); overload;
    procedure Print(AResults: TrwReportResults; const APrinterName: String = ''); overload;
    procedure Print(AResults: IisStream; const APrinterName: String = ''); overload;
    procedure Print(const AQueueTaskID: TisGUID; const APrinterName: String = ''); overload;
    procedure SaveToFile(AResults: TrwReportResults); overload;
    procedure SaveToFile(AResults: IisStream); overload;
    function  ConvertRWAtoPDF(const ARWAData: IEvDualStream; const APDFInfoRec: TrwPDFDocInfo): IEvDualStream;

    function  StartGroup: Variant;
    function  CalcPrintReport(const aName: string; const AReportParams: TrwReportParams; const ACaption: String = ''): TrwRepParams; overload;
    function  CalcPrintReport(ANBR: Integer; ALevel: string; AReportParams: TrwReportParams; ACaption: String = ''): TrwRepParams; overload;
    function  CalcPrintReport(const Rep: TrwRepParams): TrwRepParams; overload;
    function  EndGroup(ADestination: TrwReportDestination; const Descr: string = '';
              const OverrideCoNbr: Integer = 0; const OverridePrNbr: Integer = 0;
              const OverrideEventDate: TDateTime = 0; const PrReprintHistoryNBR: Integer = 0): IisStream;
    function  EndGroupForTask(ADestination: TrwReportDestination; const Descr: string = ''; const OverrideCoNbr: Integer = 0;
      const OverridePrNbr: Integer = 0; const OverrideEventDate: TDateTime = 0): TisGUID;
    function  ReportsOfGroup: TrwReportList;

    function  ShowInputForm(Report_NBR: Integer; DBType: string; AParent: TrwInputFormContainer; AParams: TrwReportParams; ARunTime: Boolean = True): Boolean;
    function  ShowAncestorInputForm(AClassName: string; AParent: TrwInputFormContainer; AParams: TrwReportParams; ARunTime: Boolean = True): Boolean;
    function  ShowModalInputForm(Report_NBR: Integer; DBType: string; AParams: TrwReportParams; ARunTime: Boolean = True; AShowWarning: Boolean = True): TModalResult;
    function  CloseInputForm(ACloseResult: TModalResult; AParams: TrwReportParams): Boolean;
    function  GetInputFormBoundRect: TRect;

    function  ClassNameToDescr(AClassName: String): string;
    function  RWClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
    function  GetRepAncestor(Report_NBR: Integer; DBType: string): String;
    function  ReportsByAncestor(const AAncestors: array of string; const ALevels: string = 'SBC'; AOwner: TComponent = nil): TevClientDataSet;

    procedure LogicalQuery(ASQL: String; AParams: TParams; AResult: TISBasicClientDataSet);
    function  GetLastExceptions: String;
    function  GetLastWarnings: String;

    procedure GetReportResources(Report_NBR: Integer; DBType: String; AData: IEvDualStream);

    function  ConvertOldPreviewFormat(AStream: IEvDualStream): Boolean;
    procedure MakeSecureRWA(var AData: IEvDualStream; const PreviewPassword, PrintPassword: String);

    function  MergeGraphicReports(AResults: TrwReportResults; const AReportName: string): TrwReportResults;

    procedure RunQBQuery(AData: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer; AParams: TParams; AResultDataSet: TEvBasicClientDataSet; ARemote: Boolean = False);
 end;


implementation

function GetRWLocalEngine: IevRWLocalEngine;
begin
  Result := TrwLocalEngineObject.Create;
end;


{ TrwLocalEngineObject }

procedure TrwLocalEngineObject.CompileReport(AData: IEvDualStream; var AClassName: String; var AAncestorClassName: String);
begin
  rwProxy.CompileReport(AData, AClassName, AAncestorClassName);
end;


function TrwLocalEngineObject.CalcPrintReport(ANBR: Integer; ALevel: string; AReportParams: TrwReportParams; ACaption: String = ''): TrwRepParams;
var
  s: String;
begin
  if ALevel = CH_DATABASE_CLIENT then
    s := ALevel + IntToStr(ctx_DataAccess.ClientID)  
  else
    s := ALevel;

  Result := FRepList.AddReport(ANBR, s, AReportParams);
  Result.Caption := ACaption;
  Result.Params.Add('Report_Nbr', ALevel + IntToStr(ANBR));
end;


function TrwLocalEngineObject.StartGroup: Variant;
begin
  if not IsGroupStarted then
  begin
    FRepList.Clear;
    FGroupCounter := 1;
    FLastExceptions := '';
  end
  else
  begin
    Result := True;
    Inc(FGroupCounter);
  end;
end;


function TrwLocalEngineObject.IsGroupStarted: Boolean;
begin
  Result := FGroupCounter > 0;
end;


function TrwLocalEngineObject.EndGroup(ADestination: TrwReportDestination; const Descr: string = ''; const OverrideCoNbr: Integer = 0;
const OverridePrNbr: Integer = 0; const OverrideEventDate: TDateTime = 0; const PrReprintHistoryNBR: Integer = 0): IisStream;
var
  RepRes: TrwReportResults;

begin
  Result := nil;

  DoEndGroup(ADestination, Descr, OverrideCoNbr, OverridePrNbr, OverrideEventDate);
  if FGroupCounter = 0 then
    try
      if ADestination = rdtCancel then
        Exit;

       if ADestination = rdtPrepareVMR then
         ADestination := rdtNone;

      RepRes := nil;
      try
        RepRes := ctx_RWRemoteEngine.RunReports2(FRepList);
        FLastExceptions := RepRes.GetAllExceptions;
        FLastWarnings := RepRes.GetAllWarnings;

      except
        on E: Exception do
          FLastExceptions := MakeFullError(E);
      end;

      try
        if (ADestination in [rdtPrinter, rdtPreview]) and (GetLastExceptions <> '') and not UnAttended then
          raise EevException.CreateDetailed('Report finished with errors', GetLastExceptions);

        if (ADestination in [rdtPrinter, rdtPreview]) and (GetLastWarnings <> '') and not UnAttended then
          EvMessage(GetLastWarnings, mtWarning, [mbOK]);

        if not Assigned(RepRes) then
          Exit;

        RepRes.PrReprintHistoryNBR := PrReprintHistoryNBR;

        if ADestination = rdtPreview then
          Preview(RepRes)

        else if ADestination = rdtPrinter then
          Print(RepRes)

        else if ADestination = rdtVMR then
        begin
          Result := RepRes.GetAsStream;
          if ctx_VmrEngine <> nil then
          begin
            if ctx_VmrEngine.VmrPostProcessReportResult(Result) and not UnAttended then
              EvMessage('Some of the reports have been submitted to mail room');
          end;
          Print(Result)
        end

        else
          Result := RepRes.GetAsStream;

        if Assigned(Result) then
          Result.Position := 0;

      finally
        RepRes.Free;
      end;

    finally
      FRepList.Clear;
    end
end;


destructor TrwLocalEngineObject.Destroy;
begin
  FreeAndNil(FRepList);
  inherited;
end;


function TrwLocalEngineObject.CloseInputForm(ACloseResult: TModalResult; AParams: TrwReportParams): Boolean;
begin
  Result := rwProxy.CloseInputForm(ACloseResult, AParams);
end;


function TrwLocalEngineObject.ShowInputForm(Report_NBR: Integer; DBType: string; AParent: TrwInputFormContainer; AParams: TrwReportParams; ARunTime: Boolean = True): Boolean;
var
  RepData: IEvDualStream;
begin
  CheckLocalMode;

  ctx_StartWait;
  try
    RepData := TEvDualStreamHolder.Create;
    GetReportResources(Report_NBR, DBType, RepData);

    Result := rwProxy.ShowInputForm(RepData, AParams, AParent, ARunTime);
  finally
    ctx_EndWait;
  end;
end;


function TrwLocalEngineObject.ShowModalInputForm(Report_NBR: Integer; DBType: string; AParams: TrwReportParams; ARunTime: Boolean = True; AShowWarning: Boolean = True): TModalResult;
var
  RepData: IEvDualStream;
begin
  CheckLocalMode;

  ctx_StartWait;
  try
    RepData := TEvDualStreamHolder.Create;
    GetReportResources(Report_NBR, DBType, RepData);

    Result := rwProxy.ShowModalInputForm(RepData, AParams, ARunTime);

    if Result = mrNone then
      if AShowWarning then
        EvMessage('There are no parameters for this report')
      else if ARunTime then
        Result := mrOk;

  finally
    ctx_EndWait;
  end;
end;


procedure TrwLocalEngineObject.BeginWork;
begin
  rwProxy.StartSession;
end;

procedure TrwLocalEngineObject.EndWork;
begin
  rwProxy.EndSession;
end;


function TrwLocalEngineObject.GetRepAncestor(Report_NBR: Integer; DBType: string): String;
var
  MS: IisStream;
  rt: TReportType;
  mt: string;
  rn, rc: string;
  h: String;
  i, n: Integer;
  sLevel: String;
begin
  Result := '';

  if DBType = CH_DATABASE_CLIENT then
    sLevel := DBType + IntToStr(ctx_DataAccess.ClientID)
  else
    sLevel := DBType;  

  MS := TisStream.Create;
  try
    GetReportRes(Report_NBR, sLevel, True, rt, rn, MS, mt, rc, Result);
  except
    Exit;
  end;

  if (Result = '') and (MS.Size > 0) then
  begin
    MS.Position := 0;
    SetLength(h, MS.Size);
    MS.ReadBuffer(h[1], MS.Size);
    i := Pos('_LibComponent'#6, h);
    if not((i = 0) or (i > 50)) then
    begin
      n := Ord(h[i+14]);
      Result := Copy(h, i+15, n);
    end;
  end;
end;


function TrwLocalEngineObject.ClassNameToDescr(AClassName: String): string;
begin
  Result := rwProxy.ClassNameToDescr(AClassName);
end;


function TrwLocalEngineObject.ShowAncestorInputForm(AClassName: string; AParent: TrwInputFormContainer; AParams: TrwReportParams; ARunTime: Boolean = True): Boolean;
begin
  CheckLocalMode;

  ctx_StartWait;
  try
    Result := rwProxy.ShowAncestorInputForm(AClassName, AParams, AParent, ARunTime);
  finally
    ctx_EndWait;
  end;
end;


procedure TrwLocalEngineObject.Preview(AResults: TrwReportResults; AParent: TrwPreviewContainer = nil; const ASecuredPreview: Boolean = False);
var
  GrouppedResults: TrwReportResults;
  Frm: TrwModalPreviewForm;
begin
  CheckLocalMode;

  GrouppedResults := PrepareResultForPreview(AResults);
  try
    if Assigned(AParent) then
      rwProxy.Preview(GrouppedResults, AParent, ASecuredPreview)
    else
    begin
      Frm := TrwModalPreviewForm.Create(Application);
      try
        rwProxy.Preview(GrouppedResults, Frm.rwPreviewContainer, ASecuredPreview);
        Frm.ShowModal;
      finally
        Frm.Free;
      end;
    end;
  finally
    FreeAndNil(GrouppedResults);
  end;
end;

function TrwLocalEngineObject.PrepareResultForPreview(AResults: TrwReportResults): TrwReportResults;
var
  i, j: Integer;
  P: Pointer;
  h: String;
  L: TStringList;
  RepRes: TrwReportResult;
  TmpRes: IEvDualStream;
  PrnInfo: TevRWPrinterRec;
begin
  Result := TrwReportResults.Create;

  L := TStringList.Create;
  try
    L.AddObject('All Graphic Reports', Pointer(Ord(rtUnknown)));
    j := 0;
    for i := 0 to AResults.Count - 1 do
    begin
      if (Length(AResults[i].ErrorMessage) > 0) or not Assigned(AResults[i].Data) or (AResults[i].Data.Size = 0) then
        Continue;

      Inc(j);

      P := Pointer(Ord(AResults[i].ReportType));
      if L.IndexOfObject(P) = -1 then
      begin
        case AResults[i].ReportType of
          rtReport:      h := 'Reports';
          rtCheck:       h := 'Checks';
          rtTaxReturn:   h := 'Tax Returns';
          rtTaxCoupon:   h := 'Tax Cupons';
          rtMultiClient: h := 'MultiClient';
          rtHR:          h := 'HR';
        else
          h := '';
        end;

        if h <> '' then
          L.AddObject(h, P);
      end;
    end;

    if j <= 1 then
      L.Clear

    else if L.Count = 2 then
    begin
      L.Objects[0] := L.Objects[1];
      L.Delete(1);
    end;

    for i := 0 to L.Count - 1 do
    begin
      if TReportType(Integer(L.Objects[i])) = rtCheck then
        PrnInfo := PrinterForChecks
      else
        PrnInfo := PrinterForReports;

      TmpRes := rwProxy.MergeRWResult(AResults, [TReportType(Integer(L.Objects[i]))], PrnInfo, L[i]);

      if Assigned(TmpRes) then
      begin
        RepRes := Result.Add;
        RepRes.Data := TmpRes;
        RepRes.ReportName := L[i];
        RepRes.ReportType := TReportType(Integer(L.Objects[i]));
      end;
    end;

  finally
    FreeAndNil(L);
  end;

  for i := 0 to AResults.Count - 1 do
  begin
    if (Length(AResults[i].ErrorMessage) > 0) or not Assigned(AResults[i].Data) then
      Continue;

    RepRes := Result.Add;
    RepRes.Assign(AResults[i]);
  end;

  for i := 0 to Result.Count - 1 do
  begin
    if Result[i].ReportType = rtASCIIFile then
    begin
      Result[i].PrinterInfo := PrinterForFile;
      Result[i].SecurePreviewMode := False;
    end

    else if Result[i].ReportType = rtCheck then
    begin
      Result[i].PrinterInfo := PrinterForChecks;
      Result[i].SecurePreviewMode := True;
    end

    else
    begin
      Result[i].PrinterInfo := PrinterForReports;
      Result[i].SecurePreviewMode := False;
    end;
  end;
end;


procedure TrwLocalEngineObject.Preview(AResults: IisStream);
var
  Res: TrwReportResults;
begin
  Res := TrwReportResults.Create;
  try
    AResults.Position := 0;
    Res.SetFromStream(AResults);
    Preview(Res);
  finally
    Res.Free;
  end;
end;


function TrwLocalEngineObject.SaveASCIIResult(AResults: TrwReportResults): String;
var
  i: Integer;
  h: String;
  Dlg: TSaveDialog;
  FS: TEvFileStream;
begin
  Result := '';

  for i := 0 to AResults.Count - 1 do
    if (AResults[i].ReportType = rtASCIIFile) and (Length(AResults[i].ErrorMessage) = 0) and Assigned(AResults[i].Data) then
    begin
      h := Trim(AResults[i].FileName);

      try
        if ((Length(h) = 0) or not UnAttended and mb_AppSettings.AsBoolean['Settings\AlwaysShowASCIIFileDialog'])  then
        begin
          CheckLocalMode;

          Dlg := TSaveDialog.Create(nil);
          try
            Dlg.FileName := h;
            Dlg.Title := 'Save ASCII result "' + AResults[i].ReportName + '"';
            Dlg.Options := Dlg.Options - [ofOverwritePrompt];
            if Dlg.Execute then
            begin
              h := Trim(Dlg.FileName);
              if FileExists(h) then
                if EvMessage('File exists! Do you want to add results to the existing file?',
                 mtConfirmation, [mbYes, mbNo]) = mrYes then
                   h := h + '+';
            end;
          finally
            Dlg.Free;
          end;
        end;

        if Length(h) > 0 then
        begin
          ForceDirectories(ExtractFilePath(h));

          if h[Length(h)] = '+' then
          begin
            Delete(h, Length(h), 1);
            FS := TEvFileStream.Create(h, fmOpenWrite);
            FS.Position := FS.Size;
          end
          else
            FS := TEvFileStream.Create(h, fmCreate);

          try
            AResults[i].Data.Position := 0;
            FS.CopyFrom(AResults[i].Data.RealStream, AResults[i].Data.Size);
          finally
            FS.Free;
            Result := Result + h + #13;
          end;
        end;

      except
        on E: Exception do
        begin
          E.Message := 'Report: ' + AResults[i].ReportName + #13 +
                       'Cannot save ASCII file to "' + h + '"' + #13 +
                       E.Message;
          raise;
        end;
      end;
    end;
end;


procedure TrwLocalEngineObject.Print(AResults: TrwReportResults; const APrinterName: String);

  procedure DoStoreUnprinted(const AResults: TrwReportResults);
  begin
    if not Assigned(FUnPrintedReportsStorage) then
      Exit;
    FUnPrintedReportsStorage.AddReportResults(AResults);
  end;

  procedure DoRWPrint(AResults: TrwReportResults; ReportTypes: TReportTypes; PrinterInfo: TevRWPrinterRec; const PrintJobName: String);
  begin
    if PrinterInfo.PrinterName = VMR_USER_SIDE_PRINTER_NAME then
      DoStoreUnprinted(AResults)
    else
      rwProxy.Print(AResults, ReportTypes, PrinterInfo, PrintJobName);
  end;

  procedure DoPrint;
  var
    PrnInfo: TevRWPrinterRec;
    PrinterList: IevVMRPrintersList;
    VMRPrnInfo: IevVMRPrinterInfo;
  begin
    if (AResults.PrinterName <> '') and (ctx_VmrRemote <> nil) then  //VMR
    begin
      PrinterList := ctx_VmrRemote.GetPrinterList(AResults.location);
      VMRPrnInfo := PrinterList.FindPrinter(AResults.PrinterName);
      if not Assigned(VMRPrnInfo) then
        raise EevException.CreateFmt('Printer "%s" not found', [AResults.PrinterName]);

      PrnInfo.Duplexing := rdpDefault;
      PrnInfo.PrinterName := VMRPrnInfo.Name;
      PrnInfo.PrinterHorizontalOffset := VMRPrnInfo.HOffset;
      PrnInfo.PrinterVerticalOffset   := VMRPrnInfo.VOffset;
      DoRWPrint(AResults, [], PrnInfo, 'Evolution Reports (VMR)');
    end
    else
    begin
      if APrinterName <> '' then
        DoRWPrint(AResults, [rtUnknown, rtReport, rtCheck, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR], PrinterSetting(APrinterName), 'Evolution Reports')
      else
      begin
        if AnsiSameText(PrinterForChecks.PrinterName, PrinterForReports.PrinterName) then
          DoRWPrint(AResults, [rtUnknown, rtReport, rtCheck, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR], PrinterSetting(PrinterForReports.PrinterName), 'Evolution Reports')
        else
        begin
          DoRWPrint(AResults, [rtCheck], PrinterForChecks, 'Evolution Checks');
          DoRWPrint(AResults, [rtUnknown, rtReport, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR], PrinterForReports, 'Evolution Reports');
        end;
      end;
      SaveASCIIResult(AResults);
    end;
  end;

var
  fl: Boolean;
begin
  fl := mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview'];

  if UnAttended then
  begin
    if not fl then
      DoPrint
    else
      DoStoreUnprinted(AResults)
  end

  else
    if fl then
    begin
      DoStoreUnprinted(AResults);
      Preview(AResults);
    end
    else
      DoPrint;
end;

procedure TrwLocalEngineObject.Print(AResults: IisStream; const APrinterName: String);
var
  Res: TrwReportResults;
begin
  if Assigned(AResults) then
  begin
    Res := TrwReportResults.Create;
    try
      AResults.Position := 0;
      Res.SetFromStream(AResults);
      Print(Res, APrinterName);
    finally
      Res.Free;
    end;
  end;
end;


function TrwLocalEngineObject.CalcQueueReports(ARepList: TrwReportList; ADestination: TrwReportDestination): TisGUID;
var
  qTask: IevRunReportTask;
  i: Integer;
begin
  qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_RUN_REPORT, True) as IevRunReportTask;

  qTask.Caption := '';
  for i := 0 to ARepList.Count - 1 do
  begin
    if i > 0 then
      qTask.Caption := qTask.Caption + ', ';
    qTask.Caption := qTask.Caption + ARepList[i].Caption;
  end;
  if Length(qTask.Caption) > 255 then
    qTask.Caption := Copy(qTask.Caption, 1, 255);

  qTask.Reports := ARepList.GetAsStream;
  qTask.Destination := ADestination;

  if AppID = EvoRequestProcAppInfo.AppID then
    mb_TaskQueue.AddTask(qTask)
  else
  if ctx_EvolutionGUI <> nil then
    ctx_EvolutionGUI.AddTaskQueue(qTask)
  else
    mb_TaskQueue.AddTask(qTask);

  Result := qTask.ID;
end;


procedure TrwLocalEngineObject.LogicalQuery(ASQL: String; AParams: TParams;  AResult: TISBasicClientDataSet);
begin
  // needs to be implemented by someone's demand
end;


function TrwLocalEngineObject.GetLastExceptions: String;
begin
  Result := FLastExceptions;
end;

function TrwLocalEngineObject.GetLastWarnings: String;
begin
  Result := FLastWarnings;
end;

procedure TrwLocalEngineObject.GetReportResources(Report_NBR: Integer; DBType: String; AData: IEvDualStream);
var
  lReportType: TReportType;
  lReportName, rc, ac: String;
  mt: string;
  sLevel: String;
begin
  if DBType = CH_DATABASE_CLIENT then
    sLevel := DBType + IntToStr(ctx_DataAccess.ClientID)
  else
    sLevel := DBType;

  GetReportRes(Report_NBR, sLevel, True, lReportType, lReportName, AData, mt, rc, ac);
end;

function TrwLocalEngineObject.ConvertOldPreviewFormat(AStream: IEvDualStream): Boolean;
begin
//  Result := ConvertOldFormat(AStream); old crap I guess
  Result := False;
end;


function TrwLocalEngineObject.ReportsByAncestor(const AAncestors: array of string; const ALevels: string; AOwner: TComponent): TevClientDataSet;

  procedure AddLevel(const ALev: String; const ALevPrif: String);
  var
    DS: TEvBasicClientDataSet;
    j: Integer;
    fl: Boolean;
    PrevClass: String;
  begin
    DS := TEvBasicClientDataSet.Create(nil);
    try
      CustomQuery(DS, 'select ' +
        ALevPrif + '_report_writer_reports_nbr, report_description, class_name, ancestor_class_name from ' +
        ALevPrif + '_report_writer_reports where {AsOfNow<'+ ALevPrif + '_report_writer_reports>} order by ancestor_class_name', ALev, nil);

      DS.First;
      PrevClass := '';
      fl := False;
      while not DS.Eof do
      begin
        if Length(AAncestors) > 0 then
        begin
          if not AnsiSameText(PrevClass, DS.Fields[3].AsString) then
          begin
            fl := False;
            PrevClass := DS.Fields[3].AsString;
            for j := Low(AAncestors) to High(AAncestors) do
              if RWClassInheritsFrom(PrevClass, AAncestors[j]) then
              begin
                fl := True;
                break;
              end;
          end
        end
        else
          fl := True;

        if fl then
        begin
          Result.Append;
          Result.Fields[0].AsString := ALev;
          Result.Fields[1].AsInteger := DS.Fields[0].AsInteger;
          Result.Fields[2].AsString := DS.Fields[1].AsString;
          Result.Fields[3].AsString := DS.Fields[2].AsString;
          Result.Fields[4].AsString := DS.Fields[3].AsString;
          Result.Post;
        end;

        DS.Next;
      end;

    finally
      DS.Free;
    end;
  end;

begin
  Result := TevClientDataSet.Create(AOwner);
  try
    Result.FieldDefs.Add('Level', ftString, 1);
    Result.FieldDefs.Add('Nbr', ftInteger);
    Result.FieldDefs.Add('Description', ftString, 64);
    Result.FieldDefs.Add('Class_Name', ftString, 40);
    Result.FieldDefs.Add('Ancestor_Class_Name', ftString, 40);
    Result.CreateDataSet;

    if Pos(CH_DATABASE_SYSTEM, ALevels) <> 0 then
      AddLevel(CH_DATABASE_SYSTEM, 'SY');

    if Pos(CH_DATABASE_SERVICE_BUREAU, ALevels) <> 0 then
      AddLevel(CH_DATABASE_SERVICE_BUREAU, 'SB');

    if Pos(CH_DATABASE_CLIENT, ALevels) <> 0 then
      AddLevel(CH_DATABASE_CLIENT, 'CL');

    Result.First;

  except
    Result.Free;
    raise;
  end;
end;


procedure TrwLocalEngineObject.MakeSecureRWA(var AData: IEvDualStream; const PreviewPassword, PrintPassword: String);
begin
  AData := rwProxy.MakeSecureRWA(AData, PreviewPassword, PrintPassword);
end;

function TrwLocalEngineObject.MergeGraphicReports(
  AResults: TrwReportResults; const AReportName: string): TrwReportResults;
var
  PrinterInfo:TevRWPrinterRec;
  RepRes : TrwReportResult;
  i:integer;
begin
    PrinterInfo := PrinterForFile;
    Result := TrwReportResults.Create;
    RepRes := TrwReportResult.Create(nil);
    RepRes.Data := rwProxy.MergeRWResult(AResults,[rtUnknown, rtReport, rtCheck, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR],PrinterInfo,'Graphics reports');
    RepRes.ReportName := AReportName;
    if RepRes.Data <> nil then
      RepRes.Collection := Result
    else
      RepRes.Free;
    for i := AResults.Count-1 downto 0 do
    begin
       if not (AResults[i].ReportType in [rtUnknown, rtReport, rtCheck, rtTaxReturn, rtTaxCoupon, rtMultiClient, rtHR]) then
          AResults[i].Collection := Result;
    end;
end;


function TrwLocalEngineObject.CalcPrintReport(const Rep: TrwRepParams): TrwRepParams;
begin
  Rep.Collection := FRepList;
  Result := Rep;
end;

function TrwLocalEngineObject.rwProxy: IevReportWriterProxy;
begin
  Result := FRWProxy;
end;

function TrwLocalEngineObject.RWClassInheritsFrom(const AClassName, AAncestorClassName: String): Boolean;
begin
  Result := rwProxy.RWClassInheritsFrom(AClassName, AAncestorClassName);
end;

function TrwLocalEngineObject.ConvertRWAtoPDF(const ARWAData: IEvDualStream; const APDFInfoRec: TrwPDFDocInfo): IEvDualStream;
begin
  Result := rwProxy.ConvertRWAtoPDF(ARWAData, APDFInfoRec);
end;

function TrwLocalEngineObject.CalcPrintReport(const aName: string; const AReportParams: TrwReportParams; const ACaption: String): TrwRepParams;
var
  Nbr: Integer;
  aLevel: string;
begin
  GetReportNbrByName(aName, Nbr, aLevel);
  Result := CalcPrintReport(Nbr, aLevel, AReportParams, ACaption);
end;

procedure TrwLocalEngineObject.GetReportNbrByName(const aName: string; out Nbr: Integer; out aLevel: string);

  function OpenReport(const ds: TevClientDataSet; const sPrefix: string): Boolean;
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('TableName', sPrefix + '_REPORT_WRITER_REPORTS');
      SetMacro('Columns', sPrefix + '_REPORT_WRITER_REPORTS_NBR');
      SetMacro('Condition', 'UpperCase(REPORT_DESCRIPTION)=''' + UpperCase(aName) + '''');
      ds.Close;
      ds.DataRequest(AsVariant);
      ds.Open;
      Result := ds.RecordCount <> 0;
    end;
  end;

begin
  Nbr := 0;
  aLevel := '';
  if OpenReport(ctx_DataAccess.CUSTOM_VIEW, 'CL') then
  begin
    Nbr := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger;
    aLevel := CH_DATABASE_CLIENT + IntToStr(ctx_DataAccess.ClientID);
  end
  else if OpenReport(ctx_DataAccess.SB_CUSTOM_VIEW, 'SB') then
  begin
    Nbr := ctx_DataAccess.SB_CUSTOM_VIEW.Fields[0].AsInteger;
    aLevel := CH_DATABASE_SERVICE_BUREAU;
  end
  else if OpenReport(ctx_DataAccess.SY_CUSTOM_VIEW, 'SY') then
  begin
    Nbr := ctx_DataAccess.SY_CUSTOM_VIEW.Fields[0].AsInteger;
    aLevel := CH_DATABASE_SYSTEM;
  end;
end;

function TrwLocalEngineObject.GetInputFormBoundRect: TRect;
begin
  Result := rwProxy.GetInputFormBoundRect;
end;


function TrwLocalEngineObject.PrinterForChecks: TevRWPrinterRec;
begin
  Result := PrinterSetting(mb_AppSettings['Settings\ChecksPrinter']);
end;

function TrwLocalEngineObject.PrinterForReports: TevRWPrinterRec;
begin
  Result := PrinterSetting(mb_AppSettings['Settings\ReportsPrinter']);
end;

function TrwLocalEngineObject.ReportWriteProxy: IInterface;
begin
  Result := rwProxy;
end;

procedure TrwLocalEngineObject.RunQBQuery(AData: IEvDualStream; AClientNbr, ACompanyNbr: Integer; AParams: TParams; AResultDataSet: TEvBasicClientDataSet; ARemote: Boolean = False);
var
  ResStream, Query: IEvDualStream;
  EvoParams: TEvBasicParams;
  i: Integer;
begin
  if ARemote then
  begin
    Query := TEvDualStreamHolder.Create;
    AData.Position := 0;
    Query.WriteStream(AData);

    EvoParams := TEvBasicParams.Create;
    try
      if Assigned(AParams) then
        for i := 0 to AParams.Count - 1 do
          EvoParams.AddParam(AParams[i].Name, AParams[i].Value);

      EvoParams.Position := 0;
      Query.WriteStream(EvoParams.Stream);
    finally
      FreeAndNil(EvoParams);
    end;

    Query.Position := 0;
    try
      ResStream := ctx_RWRemoteEngine.RunQBQuery(Query, AClientNbr, ACompanyNbr);
    except
      on E: Exception do
        FLastExceptions := MakeFullError(E);
    end;

    if (GetLastExceptions <> '') and not UnAttended then
      raise EevException.CreateDetailed('QB query finished with error', GetLastExceptions);

    ResStream.Position := 0;
    AResultDataSet.LoadFromStream(ResStream.RealStream);
  end

  else
    rwProxy.RunQueryBuilderQuery(AData, AClientNbr, ACompanyNbr, AParams, AResultDataSet);
end;

function TrwLocalEngineObject.PrinterForFile: TevRWPrinterRec;
begin
  Result.Duplexing := rdpDefault;
  Result.PrinterName := '';
  Result.PrinterHorizontalOffset := 0;
  Result.PrinterVerticalOffset   := 0;
end;

procedure TrwLocalEngineObject.DoOnConstruction;
begin
  inherited;
  FGroupCounter := 0;
  FRepList := TrwReportList.Create;
  FRWProxy := GetRWProxy;
end;


procedure TrwLocalEngineObject.Print(const AQueueTaskID: TisGUID; const APrinterName: String);
var
  Tsk: IevTask;
begin
  Tsk := mb_TaskQueue.GetTask(AQueueTaskID);
  if Supports(Tsk, IevPrintableTask) then
    Print((Tsk as IevPrintableTask).ReportResults, APrinterName);
end;

function TrwLocalEngineObject.EndGroupForTask(ADestination: TrwReportDestination; const Descr: string;
   const OverrideCoNbr, OverridePrNbr: Integer; const OverrideEventDate: TDateTime): TisGUID;
begin
  Result := '';
  DoEndGroup(ADestination, Descr, OverrideCoNbr, OverridePrNbr, OverrideEventDate);

  if FGroupCounter = 0 then
    try
      if ADestination = rdtCancel then
        Exit;

      if ADestination = rdtPrepareVMR then
        ADestination := rdtNone;

      if not UnAttended and mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview'] then
        ADestination := rdtPreview;

      Result := CalcQueueReports(FRepList, ADestination);
    finally
      FRepList.Clear;
    end
end;

procedure TrwLocalEngineObject.DoEndGroup(const ADestination: TrwReportDestination; const Descr: string;
  const OverrideCoNbr, OverridePrNbr: Integer; const OverrideEventDate: TDateTime);
var
  i: Integer;

  procedure DumpRWParams;
  var
    h: String;
    i: Integer;
  begin
    h := '';
    for i := 0 to FRepList.Count - 1 do
      h := h + DumpParams(FRepList[i].Params, FRepList[i].Caption) + #13;

    if not Unattended then
      EvMessage('Report parameters are dumped into files:'#13 + h, mtInformation, [mbOK]);
  end;

begin
  Dec(FGroupCounter);

  if FGroupCounter = 0 then
  begin
    if ADestination = rdtCancel then
      Exit;

    FRepList.Descr := Descr;
    if (OverrideCoNbr <> 0) or (OverridePrNbr <> 0) then
      for i := 0 to FRepList.Count-1 do
      begin
        if OverrideCoNbr <> 0 then
          FRepList[i].VmrCoNbr := OverrideCoNbr;
        if OverridePrNbr <> 0 then
          FRepList[i].VmrPrNbr := OverridePrNbr;
        if OverrideEventDate <> 0 then
          FRepList[i].VmrEventDate := OverrideEventDate;
      end;

    if mb_AppSettings.AsBoolean['Settings\DumpRWParams'] then
      DumpRWParams;

    if ADestination in [rdtVMR, rdtPrepareVMR] then
      if ctx_VmrEngine <> nil then
        ctx_VmrEngine.VmrPreProcessReportList(FRepList);
  end;
end;

function TrwLocalEngineObject.ReportsOfGroup: TrwReportList;
begin
  Result := FRepList;
end;

procedure TrwLocalEngineObject.SaveToFile(AResults: IisStream);
var
  Res: TrwReportResults;
begin
  if Assigned(AResults) then
  begin
    Res := TrwReportResults.Create;
    try
      AResults.Position := 0;
      Res.SetFromStream(AResults);
      SaveToFile(Res);
    finally
      Res.Free;
    end;
  end;
end;

procedure TrwLocalEngineObject.SaveToFile(AResults: TrwReportResults);
begin
  SaveASCIIResult(AResults);
end;

function TrwLocalEngineObject.PrinterSetting(const APrinterName: String): TevRWPrinterRec;
var
  Prn: IevPrinterInfo;
begin
  with Result do
  begin
    PrinterName := APrinterName;
    Duplexing := TrwDuplexPrintType(mb_AppSettings.AsInteger['Settings\DuplexPrinting']);

    Prn := Mainboard.MachineInfo.Printers.FindPrinter(APrinterName);
    if Assigned(Prn) then
    begin
      PrinterVerticalOffset := Prn.VOffset;
      PrinterHorizontalOffset := Prn.HOffset;
    end
    else
    begin
      PrinterVerticalOffset := 0;
      PrinterHorizontalOffset := 0;
    end;
  end;
end;

procedure TrwLocalEngineObject.SetUnprintedReportsStorage(const AValue: TrwReportResults);
begin
  FUnPrintedReportsStorage := AValue;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRWLocalEngine, IevRWLocalEngine, 'RW Local Module');

end.
