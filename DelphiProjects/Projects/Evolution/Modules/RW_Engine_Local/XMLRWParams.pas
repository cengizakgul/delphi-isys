unit XMLRWParams;
interface
uses
   Windows, SysUtils, Math, StrUtils, Classes, TypInfo, Variants, DateUtils
  ,DB
  //, DBClient, MidasLib // commented out as introducing mutithread-unsafe dependencies to Midas DCU and Microsoft XML DOM
  ,EvStreamUtils
  ,SReportSettings
  ,isBaseClasses
  ,SimpleXML
  ,isExceptions
  ,EvClientDataset
  ;
type
   TXMLRWConvertor = class(TObject) // we do not need instantiate it ever; all methods are methods of class
   private
    class function WinVarTypeToRwType(iValue: integer): integer;
    class function RwVarTypeToWin(iValue: integer): integer;
    class function CloneField(Source: TField; AOwner: TComponent): TField;
    class function GenerateArrVar(ArrValNode: SimpleXML.IXMLNode): Variant;
    class procedure ParseParNode( const ParNode: SimpleXML.IXMLNode; var NodeOfName, NodeOfType, NodeOfValue, NodeOfStore, NodeArrValue,NodeOfRwType,NodeOfWinType:SimpleXML.IXMLNode);
    class procedure PushParNodeToObject(ParNode: SimpleXML.IXMLNode; RWParInstance: TrwReportParams);
    class function XSTRToDateTime(const s: String): TDateTime;
    class procedure WorkArrayParam(V: Variant; Name: string; parIndex:integer; iiParNode: SimpleXML.IXMLElement);
    class function DateTimeToXSTR(const u: TDateTime): String;
    class function VariantTypeAsCSharpType(varVar: Variant):String;
    class function DatasetToXMLString(varDataset:TDataset):string;
   public
     class procedure CopyFields(SourceDataset, DestDataset: TDataset; doAdd: Boolean);
     class function CSharpTypeAsVarTypeInt(StrType: String):Integer;
     class function  VariantTypeAsInt(varVar: Variant):integer;
     class procedure ReportParametersFromXML( const XMLRWParameters:String; const RWParInstance:TrwReportParams );
     class procedure SaveReportParametersToXML( const RWParInstance:TrwReportParams; const outStream: iIsStream );
   end;

implementation
  type
  TrwRPType = (rwvUnknown, rwvInteger, rwvBoolean, rwvString, rwvFloat, rwvDate, rwvArray, rwvPointer, rwvCurrency, rwvVariant, // original RW types
               rwvExtraStr1, rwvExtraStr2, rwvExtraBool); // compensation for problems on .NET side
  TVarPair=record
            SysVarType: TVarType;
            RwVarType: TrwRPType;
           end;

  const
  WinVarToRW: array[0..18] of TVarPair =
  (
     ( SysVarType: varSmallInt; RWVarType: rwvInteger),
     ( SysVarType: varInteger;  RWVarType: rwvInteger),
     ( SysVarType: varSingle;   RWVarType: rwvFloat ),
     ( SysVarType: varDouble;      RWVarType: rwvFloat ),
     ( SysVarType: varCurrency; RWVarType: rwvCurrency),

     ( SysVarType: varDate;     RWVarType: rwvDate),
     ( SysVarType: varOleStr;   RWVarType: rwvString),
     ( SysVarType: varDispatch; RWVarType: rwvPointer),
     ( SysVarType: varBoolean;  RWVarType: rwvBoolean),
     ( SysVarType: varVariant;  RWVarType: rwvVariant),

     ( SysVarType: varUnknown;  RWVarType: rwvUnknown),
     ( SysVarType: varShortInt; RWVarType: rwvInteger),
     ( SysVarType: varByte;     RWVarType: rwvInteger),
     ( SysVarType: varWord;     RWVarType: rwvInteger),
     ( SysVarType: varLongWord; RWVarType: rwvInteger),
     ( SysVarType: varInt64;    RWVarType: rwvFloat),

     ( SysVarType: varString;   RWVarType: rwvString),
     ( SysVarType: 258;         RWVarType: rwvString), // ASCII string- new in Delphi 10
     ( SysVarType: varArray;    RWVarType: rwvArray)
  );

  RwVarToWin: array[0..11] of TVarPair =
  (
     ( SysVarType: varUnknown;  RWVarType: rwvUnknown),   // 0
     ( SysVarType: varInteger;  RWVarType: rwvInteger),   // 1
     ( SysVarType: varBoolean;  RWVarType: rwvBoolean),   // 2
     ( SysVarType: varString;   RWVarType: rwvString),    // 3
     ( SysVarType: varDouble;   RWVarType: rwvFloat ),    // 4
     ( SysVarType: varDate;     RWVarType: rwvDate),      // 5
     ( SysVarType: varArray;    RWVarType: rwvArray),     // 6
     ( SysVarType: varDispatch; RWVarType: rwvPointer),   // 7
     ( SysVarType: varCurrency; RWVarType: rwvCurrency),  // 8
     ( SysVarType: varVariant;  RWVarType: rwvVariant),   // 9
     ( SysVarType: varString;   RWVarType: rwvExtraStr1), // 10 - unused
     ( SysVarType: varBoolean;  RWVarType: rwvExtraBool)  // 11 - extra bool for .NET problem compensation
  );


procedure StreamToVariant(const FStream: TStream; var Result: Variant); overload;
var
  VarType: Integer;
  I, DimCount, ElemCount, DimElemCount, ElemSize, Len: Integer;
  StrLen: Integer;
  ArrayData: Pointer;
  ArrayDims: array of Integer;
  IntStream: IEvDualStream;
begin
  FStream.ReadBuffer(VarType, SizeOf(Integer));
  if (VarType and varArray) <> 0 then
  begin
    FStream.ReadBuffer(DimCount, SizeOf(DimCount));
    SetLength(ArrayDims, DimCount * 2);
    ElemCount:= 1;
    for I:= 0 to DimCount - 1 do
    begin
      FStream.ReadBuffer(DimElemCount , SizeOf(DimElemCount));
      ElemCount:= ElemCount * DimElemCount;
      ArrayDims[I * 2]:= 0;
      ArrayDims[I * 2 + 1]:= DimElemCount - 1;
    end;
    Result:= VarArrayCreate(ArrayDims, VarType and varTypeMask);
    try
      ArrayData:= VarArrayLock(Result);
      for I:= 0 to ElemCount - 1 do
      begin
        case VarType and varTypeMask of
          varShortInt, varByte:
          begin
            ElemSize:= SizeOf(Byte);
            FStream.ReadBuffer(PByte(ArrayData)^, ElemSize);
          end;
          varSmallint, varWord:
          begin
            ElemSize:= SizeOf(SmallInt);
            FStream.ReadBuffer(PSmallInt(ArrayData)^, ElemSize);
          end;
          varInteger, varLongWord:
          begin
            ElemSize:= SizeOf(Integer);
            FStream.ReadBuffer(PInteger(ArrayData)^, ElemSize);
          end;
          varSingle:
          begin
            ElemSize:= SizeOf(Single);
            FStream.ReadBuffer(PSingle(ArrayData)^, ElemSize);
          end;
          varDouble:
          begin
            ElemSize:= SizeOf(Double);
            FStream.ReadBuffer(PDouble(ArrayData)^, ElemSize);
          end;
          varCurrency:
          begin
            ElemSize:= SizeOf(Currency);
            FStream.ReadBuffer(Currency(ArrayData^), ElemSize);
          end;
          varDate:
          begin
            ElemSize:= SizeOf(TDateTime);
            FStream.ReadBuffer(TDateTime(ArrayData^), ElemSize);
          end;
          varOleStr:
          begin
            ElemSize:= SizeOf(PWideChar);
            FStream.ReadBuffer(Len, SizeOf(Len));
            PWideString(ArrayData)^ := WideString(StringOfChar(' ', Len));
            FStream.ReadBuffer(PWideString(ArrayData)^[1], Len * SizeOf(WideChar));
          end;
{         varString:    This array item type is not supported by Delphi!
          begin
            ElemSize:= SizeOf(PChar);
            FStream.ReadBuffer(Len, SizeOf(Len));
            string(ArrayData^) := StringOfChar(' ', Len);
            FStream.Read(PChar(ArrayData)^, Len * SizeOf(Char));
          end;
}
          varBoolean:
          begin
            ElemSize:= SizeOf(WordBool);
            FStream.ReadBuffer(WordBool(ArrayData^), ElemSize);
          end;
          varVariant:
          begin
            ElemSize:= SizeOf(TVarData);
            Variant(PVarData(ArrayData)^) := Unassigned;
            StreamToVariant(FStream, Variant(PVarData(ArrayData)^));
          end;
        else
          raise EisException.Create('Unsupported type: ' + IntToStr(VarType));
          ElemSize := 0;
        end;
        ArrayData:= Pointer(Integer(ArrayData) + ElemSize);
      end;
    finally
      VarArrayUnlock(Result);
    end;
  end
  else
  begin
    case VarType and varTypeMask of
    varEmpty:
      Result := Unassigned;
    varNull:
      Result := null;
    varSmallint:
      FStream.ReadBuffer(TVarData(Result).VSmallint , SizeOf(TVarData(Result).VSmallint));
    varShortint:
      FStream.ReadBuffer(TVarData(Result).VShortint , SizeOf(TVarData(Result).VShortint));
    varInteger:
      FStream.ReadBuffer(TVarData(Result).VInteger , SizeOf(TVarData(Result).VInteger));
    varInt64:
      FStream.ReadBuffer(TVarData(Result).VInt64 , SizeOf(TVarData(Result).VInt64));
    varSingle:
      FStream.ReadBuffer(TVarData(Result).VSingle, SizeOf(TVarData(Result).VSingle));
    varDouble:
      FStream.ReadBuffer(TVarData(Result).VDouble, SizeOf(TVarData(Result).VDouble));
    varCurrency:
      FStream.ReadBuffer(TVarData(Result).VCurrency, SizeOf(TVarData(Result).VCurrency));
    varDate:
      FStream.ReadBuffer(TVarData(Result).VDate, SizeOf(TVarData(Result).VDate));
    varBoolean:
      FStream.ReadBuffer(TVarData(Result).VBoolean, SizeOf(TVarData(Result).VBoolean));
    varByte:
      FStream.ReadBuffer(TVarData(Result).VByte, SizeOf(TVarData(Result).VByte));
    varWord:
      FStream.ReadBuffer(TVarData(Result).VWord, SizeOf(TVarData(Result).VWord));
    varLongWord:
      FStream.ReadBuffer(TVarData(Result).VLongWord, SizeOf(TVarData(Result).VLongWord));
    varOleStr:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      Result := WideString(StringOfChar(' ', StrLen));
      FStream.Read(TVarData(Result).VOleStr^, StrLen*SizeOf(WideChar));
    end;
    varString:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      Result := StringOfChar(' ', StrLen);
      FStream.Read(TVarData(Result).VString^, StrLen*SizeOf(Char));
    end;
    varUnknown:
    begin
      FStream.Read(StrLen, SizeOf(StrLen));
      if StrLen = -1 then
        IntStream := nil
      else
      begin
        IntStream := TEvDualStreamHolder.Create(StrLen);
        if StrLen > 0 then
        begin
          IntStream.RealStream.CopyFrom(FStream, StrLen);
          IntStream.Position := 0;
        end;
      end;

      Result := IntStream;
      IntStream := nil;
    end;

    else
      raise EisException.Create('Unsupported type: ' + IntToStr(VarType));
    end;
    TVarData(Result).VType := VarType;
  end;
end;


function StreamToVariant(const FStream: TStream): Variant; overload;
var
  ver: byte;
begin
  FStream.ReadBuffer(ver, SizeOf(ver));
  Assert(ver = $FF);
  StreamToVariant(FStream, Result);
end;


{ TXMLRWConvertor }

class function TXMLRWConvertor.CloneField(Source: TField; AOwner: TComponent): TField;
 
  procedure SetProp(Name: string);
  var V: variant;
      PropInfo: PPropInfo;
  begin
   PropInfo := TypInfo.GetPropInfo(Source, Name);
   if PropInfo <> nil then
     try V := TypInfo.GetPropValue(Source,Name);
      if not VarIsNull(V) then
         TypInfo.SetPropValue(Result,Name,V); 
     except
      ; //just kill exception
     end;
  end;
 
begin
  Result := TFieldClass(Source.ClassType).Create(AOwner);
 
  Result.Alignment              := Source.Alignment;
  Result.AutoGenerateValue      := Source.AutoGenerateValue;
  Result.CustomConstraint       := Source.CustomConstraint;
  Result.ConstraintErrorMessage := Source.ConstraintErrorMessage;
  Result.DefaultExpression      := Source.DefaultExpression;
  Result.DisplayLabel           := Source.DisplayLabel;
  Result.DisplayWidth           := Source.DisplayWidth;
  Result.FieldKind              := Source.FieldKind;
  Result.FieldName              := Source.FieldName;
  Result.ImportedConstraint     := Source.ImportedConstraint;
  Result.LookupDataSet          := Source.LookupDataSet;
  Result.LookupKeyFields        := Source.LookupKeyFields;
  Result.LookupResultField      := Source.LookupResultField;
  Result.KeyFields              := Source.KeyFields;
  Result.LookupCache            := Source.LookupCache;
  Result.ProviderFlags          := Source.ProviderFlags;
  Result.ReadOnly               := Source.ReadOnly;
  Result.Required               := Source.Required;
  Result.Visible                := Source.Visible;
 
  SetProp('EditMask');
  SetProp('FixedChar');
  SetProp('Size');
  SetProp('Transliterate');
  SetProp('DisplayFormat');
  SetProp('EditFormat');
  SetProp('Currency');
  SetProp('MaxValue');
  SetProp('MinValue');
  SetProp('Precision');
  SetProp('DisplayValues');
  SetProp('BlobType');
  SetProp('ObjectType');
  SetProp('IncludeObjectField');
  SetProp('ReferenceTableName');
  SetProp('Active');
  SetProp('Expression');
  SetProp('GroupingLevel');
  SetProp('IndexName');
end;

class procedure TXMLRWConvertor.ParseParNode(const ParNode: IXMLNode;
  var NodeOfName, NodeOfType, NodeOfValue, NodeOfStore, NodeArrValue,NodeOfRwType,NodeOfWinType: IXMLNode
                                            );
var
    iNode: integer;
begin
  for iNode := 0 to ParNode.ChildNodes.Count - 1 do
  begin
     if AnsiCompareStr('VarType', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeOfType := ParNode.ChildNodes.Item[iNode];
     if AnsiCompareStr('VarValue', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeOfValue := ParNode.ChildNodes.Item[iNode];
     if AnsiCompareStr('Name', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeOfName := ParNode.ChildNodes.Item[iNode];
     if AnsiCompareStr('Store', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeOfStore := ParNode.ChildNodes.Item[iNode];
     if AnsiCompareStr('Value', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeArrValue := ParNode.ChildNodes.Item[iNode];

     if AnsiCompareStr('RWVarType', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeOfRwType := ParNode.ChildNodes.Item[iNode];
     if AnsiCompareStr('WinVarType', ParNode.ChildNodes.Item[iNode].NodeName)= 0 then
       NodeOfRwType := ParNode.ChildNodes.Item[iNode];
  end;
  if Not Assigned(NodeOfName) then
    Raise EisException.Create('Missed Name for <RwGlobalVariable> ', False, -1, 0);
//  if Not Assigned(NodeOfType) then
//    Raise EisException.Create('Missed VarType for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
  if Not Assigned(NodeOfValue) then
    Raise EisException.Create('Missed VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
  if Not Assigned(NodeOfStore) then
    Raise EisException.Create('Missed Store for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
  if (AnsiCompareText(NodeOfStore.Text, 'true') <> 0) AND (AnsiCompareText(NodeOfStore.Text, 'false') <> 0) then
     Raise EisException.Create('Non-boolean VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);

  if NOT( Assigned( NodeOfType) or Assigned(NodeOfRwType) or Assigned(NodeOfWinType)) then
    Raise EisException.Create('Missed VarType and RWVarType and WinVarType for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);

end;

class function TXMLRWConvertor.DateTimeToXSTR(const u: TDateTime): String;
var
  formatSettings : TFormatSettings;
begin
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, formatSettings);
  result := FormatDateTime('yyyy-mm-dd', u, formatSettings)+'T'+ FormatDateTime('hh:nn:ss', u, formatSettings);

end;

class function TXMLRWConvertor.XSTRToDateTime(const s: String): TDateTime;
var
	aPos: Integer;

	function FetchTo(aStop: Char): Integer;
	var
		i: Integer;
	begin
		i := aPos;
		while (i <= Length(s)) and (s[i] in ['0'..'9']) do
			Inc(i);
		if i > aPos then
			Result := StrToInt(Copy(s, aPos, i - aPos))
		else
			Result := 0;
		if (i <= Length(s)) and (s[i] = aStop) then
			aPos := i + 1
		else
			aPos := Length(s) + 1;
	end;

var
	y, m, d, h, n, ss: Integer;
begin
	aPos := 1;
  if Pos('/',s) > 0 then // mm/dd/yyyy format
  begin
  	m := FetchTo('/');
    d := FetchTo('/');
      if PosEx('T', s, aPos-1) > 0 then
        y := FetchTo('T')
      else
        y := FetchTo(' ');
  end
  else
  begin
	  y := FetchTo('-');
    m := FetchTo('-');
    d := FetchTo('T'); // yyyy-mm-dd format
  end;
	h := FetchTo(':');
  n := FetchTo(':');
  ss := FetchTo(':');

  if (Pos('PM', AnsiUpperCase(s)) > 0 ) and (h <= 12 ) and (Pos('/',s) > 0) then
    h := h + 12;
	Result := EncodeDateTime(y, m, d, h, n, ss, 0);
end;


class procedure TXMLRWConvertor.PushParNodeToObject(ParNode: SimpleXML.IXMLNode;
  RWParInstance:TrwReportParams);

    function CheckXMLVarType(XNodeOfType,NodeOfName : SimpleXML.IXMLNode) : integer;
    var
//      sValue: AnsiString;
      iErr: integer;
      sType: AnsiString;
    begin
       sType := XNodeOftype.Text;
       Val(sType, Result, iErr);
       if iErr <> 0 then
             Raise EisException.Create('Non-integer VarType for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
    end;

var
  V: Variant;
  NodeOfName: SimpleXML.IXMLNode;
  NodeOfType: SimpleXML.IXMLNode;
  NodeOfValue: SimpleXML.IXMLNode;
  NodeOfStore: SimpleXML.IXMLNode;
  NodeArrValue: SimpleXML.IXMLNode;
  NodeOfRwType,NodeOfWinType: SimpleXML.IXMLNode;
//  sType :String;
  iWinVarType: integer;
  iErr: integer;
  sValue: string;
  bStore: boolean;
  iValue: integer;
  currValue: Currency;
  doubleValue: double;
  dDateValue: TDateTime;
begin
   ParseParNode(ParNode,NodeOfName, NodeOfType, NodeOfValue, NodeOfStore, NodeArrValue,NodeOfRwType,NodeOfWinType);

   // now detect type of variable passed :
   try
     if Assigned(NodeOfRwType) then
       iWinVarType := RWVarTypeToWin(CheckXMLVarType(NodeOfRwType,NodeOfName))

     else
       if Assigned(NodeOfWinType) then
         iWinVarType := CheckXMLVarType(NodeOfWinType,NodeOfName)
       else
         iWinVarType := CheckXMLVarType(NodeOfType,NodeOfName); //original approach

     sValue := AnsiString(NodeOfvalue.Text);
     bStore := (AnsiCompareText(NodeOfStore.Text,'true') = 0) ;

     if Assigned(NodeArrValue) then
       if NodeArrValue.AttrExists('ms:type')
          and
          (CompareText(NodeArrValue.GetAttr('ms:type') , 'Object[]') = 0)
       then // for arrays parameter VarValue ignored
         iWinVarType := varArray;
   except
     on E: Exception do
              Raise EisException.Create( E.Message + ' for  <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
   end;

   case iWinVarType of
       varBoolean:
         begin
             if (AnsiCompareText(sValue, 'true') <> 0) AND (AnsiCompareText(sValue, 'false') <> 0) then
                Raise EisException.Create('Non-boolean VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
             V := (AnsiCompareText(sValue,'true') = 0);
             RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varSmallint,varInteger,varByte,varWord, varLongWord:
         begin
           Val(sValue, iValue, iErr);
           if iErr <> 0 then
              Raise EisException.Create('Non-integer VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
           V := iValue;
           RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varString, varOleStr, 258:
         begin
           V := TrimRight( AnsiString(sValue));
           RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varCurrency:
         begin
           Val(sValue, doubleValue, iErr);
           if iErr <> 0 then
              Raise EisException.Create('Non-currency VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
           currValue := RoundTo(doubleValue, -2);
           V := currValue;
           RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varSingle, varDouble:
         begin
           Val(sValue, doubleValue, iErr);
           if iErr <> 0 then
              Raise EisException.Create('Non-float VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
           V := doubleValue;
           RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varDate:
         begin
           dDateValue := -10000.0;
           Assert(dDateValue = -10000.0); // to get rid of hint
           dDateValue := XSTRToDateTime(sValue);
           if dDateValue = -10000.0 then
              Raise EisException.Create('Non-date VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
           V := dDateValue;
           RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varNull, varEmpty:
         begin
           if Length(sValue) > 0 then
              Raise EisException.Create('Non-empty VarValue for <RwGlobalVariable> ' + NodeOfName.Text, False, -1, 0);
           V := Null;
           RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
       varArray:
         begin
            V := GenerateArrVar(NodeArrValue);
            RWParInstance.Add(NodeOfName.Text, V, bStore);
         end;
   end;
end;

class procedure TXMLRWConvertor.ReportParametersFromXML(
  const XMLRWParameters: String; const RWParInstance:TrwReportParams );
var
  XDoc: SimpleXML.IXMLDocument;
  Wrap: SimpleXML.IXMLNode;
  ParList: SimpleXML.IXmlNodeList;
  iPar:integer;
  ParNode: SimpleXML.IXMLNode;
begin
   RWParInstance.Clear();
   if Length(Trim(XMLRWParameters)) = 0 then
     Exit;
   XDoc := SimpleXML.LoadXmlDocumentFromXML(XMLRWParameters);
   Wrap := XDoc.DocumentElement.SelectSingleNode('RwGlobalVariables');
   if NOT Assigned (Wrap) then
      Exit;

   ParList := Wrap.ChildNodes;
   for iPar := 0 to ParList.Count - 1 do
   begin
      ParNode := ParList.Item[iPar];
      if AnsiCompareStr(ParNode.NodeName, 'RwGlobalVariable') = 0 then
        PushParNodeToObject(ParNode, RWParInstance);
   end;
end;

class function TXMLRWConvertor.GenerateArrVar(
  ArrValNode: IXMLNode): Variant;
var
  ValList: iIsList;
  itemNode: SimpleXML.iXMLElement;
  iNode: integer;
  VElem: Variant;
  sAttrType: string;
  iVal : integer;
  iErr: integer;
  { commented out as related to multithread-unsafe Midas DCU and MS XML DOM
  cld: TClientDataset;
  iiDat: IisStream;
  iiDS: IevDataSetHolder;
  iField: integer;
  }
begin
    Result := null;
    if NOT Assigned(ArrValNode) then
      Exit;
    Result := VarArrayCreate([0, 0], VarVariant);

    ValList := ArrValNode.SelectNodes('/ArrayItem');
    if ValList.Count = 0 then
    begin
      Exit;
    end;

    Result := VarArrayCreate([0, ValList.Count - 1], varVariant);
    for iNode := 0 to ValList.Count - 1 do
    begin
       itemNode := ValList[iNode] as SimpleXML.IXMLElement;
       if NOT itemNode.AttrExists('ms:type') then
         Raise EisException.Create('Missed ms:type attr for ./RwGlobalVariable/Value/ArrayItem ' + ArrValNode.ParentNode.SelectSingleNode('Name').Text, False, -1, 0);
       sAttrType := itemNode.GetAttribute('ms:type').AsString;

       if CompareText(sAttrType, 'String') = 0 then
       begin
         VElem := itemNode.Text;
         Result[iNode] := VElem;
         continue;
       end;
       if CompareText(sAttrType, 'Int32') = 0 then
       begin
          Val(itemNode.Text, iVal, iErr);
          if iErr <> 0 then
            Raise EisException.Create('Non-integer Int32 value for ./RwGlobalVariable/Value/ArrayItem ' + ArrValNode.ParentNode.SelectSingleNode('Name').Text, False, -1, 0);
           VElem := iVal;
           Result[iNode] := VElem;
           continue;
       end;
       if CompareText(sAttrType, 'Boolean') = 0 then
       begin
         if CompareText(itemNode.Text, 'True') = 0 then
         begin
           VElem := True;
           Result[iNode] := VElem;
           continue;
         end;
         if CompareText(itemNode.Text, 'False') = 0 then
         begin
           VElem := False;
           Result[iNode] := VElem;
           continue;
         end;
         Raise EisException.Create('Non-Boolean value for ./RwGlobalVariable/Value/ArrayItem ' + ArrValNode.ParentNode.SelectSingleNode('Name').Text, False, -1, 0);
       end;

       if CompareText(sAttrType, 'Object') = 0 then
       begin
          { // commented out as introducing multithread-problematic dependencies to Midas DCU and MSXML
            //
          if itemNode.ChildNodes.Count = 0 then
            Raise EisException.Create('Empty value for  Object type ./RwGlobalVariable/Value/ArrayItem ' + ArrValNode.ParentNode.SelectSingleNode('Name').Text, False, -1, 0);

          cld := TClientDataset.Create(nil);
          try
             iiDat := TisStream.CreateInMemory();
             iiDat.AsString := '<?xml version="1.0" encoding="ISO-8859-1"?>' + itemNode.ChildNodes[0].XML;
             iiDat.Position := 0;
             cld.LoadFromStream(iiDat.RealStream);
             iiDS := TevDataSetHolder.Create;
             CopyFields(cld, iiDS.DataSet, True);
             cld.First;
             iiDS.DataSet.CreateDataSet;
             iiDS.DataSet.Open;
             while NOT cld.EOF do
             begin
                 iiDS.DataSet.Append;
                 for iField:=0 to cld.Fieldcount - 1 do
                   iiDS.DataSet.Fields[iField].Value := cld.Fields[iField].Value;
                 cld.Next;
                 iiDS.DataSet.Post;
             end;
             iiDS.DataSet.First;
             Result[iNode] := iiDS.Dataset.Data;
             continue;
          finally
            cld.Free;
          end;
          }
          continue;
       end;

       Raise EisException.Create('Missed ms:type attr for ./RwGlobalVariable/Value/ArrayItem ' + ArrValNode.ParentNode.SelectSingleNode('Name').Text, False, -1, 0);
    end; // of for-cycle by array elements
end;

class procedure TXMLRWConvertor.SaveReportParametersToXML(
  const RWParInstance: TrwReportParams; const outStream: iIsStream);
var
  XDoc: SimpleXML.IXMLDocument;
  iiElem: SimpleXML.IXMLElement;
  iiParNode: SimpleXML.IXMLElement;
  iiParPart: SimpleXML.IXMLElement;
  i: integer;
  parInst: TrwReportParam;
begin
  OutStream.Clear;
  XDoc := SimpleXML.CreateXmlDocument(	'InputParams', 	'1.0',	'ISO-8859-1',	nil);
  iiElem := XDoc.DocumentElement.AppendElement('RwReportParams');
  iiElem.SetAttr('xmlns:ev', 'https://evolutionhcm.com/XMLSchema');
  iiElem.SetAttr('xmlns:ms', 'https://microsoft.com/XMLSchema');
  iiElem := XDoc.DocumentElement.AppendElement('RwGlobalVariables');
  iiElem.SetAttr('xmlns:ev', 'https://evolutionhcm.com/XMLSchema');
  iiElem.SetAttr('xmlns:ms', 'https://microsoft.com/XMLSchema');
  for i := 0 to RWParInstance.Count-1 do
  begin
    parInst := RWParInstance.Items[i];

    iiParNode := iiElem.AppendElement('RwGlobalVariable');
    iiParPart := iiParNode.AppendElement('Name');
    iiParPart.SetAttr('ms:type', 'String');
    iiParPart.Text := parInst.Name;


    iiParPart := iiParNode.AppendElement('WinVarType');
    iiParPart.SetAttr('ev:type', 'WinVarTypeKind');
    iiParPart.Text := IntToStr(VariantTypeAsInt(parInst.Value));

    iiParPart := iiParNode.AppendElement('RWVarType');
    iiParPart.SetAttr('ev:type', 'RwVarTypeKind');
    iiParPart.Text := IntToStr( WinVarTypeToRwType(VariantTypeAsInt(parInst.Value)));

    iiParPart := iiParNode.AppendElement('Store');
    iiParPart.SetAttr('ms:type', 'Boolean');
    if parInst.Store then
      iiParPart.Text := 'true'
    else
      iiParPart.Text := 'false'
    ;

    // possible situations: DataTime, Array, varUnknown = Object, all others
    if VarIsArray(parInst.Value) then
    begin
        iiParPart := iiParNode.AppendElement('VarValue');
        iiParPart.SetAttr('ms:type', 'Array');
        if (VarArrayHighBound(ParInst.Value, 1) = 0)
           and
           VarIsEmpty(ParInst.Value[0])
        then
        begin
          iiParPart := iiParNode.AppendElement('Value');
          iiParPart.SetAttr('ms:type', 'Object[]');
          Continue; // empty param array - do not contains child nodes at all, and always Object[]
        end
        else
          WorkArrayParam( parInst.Value, Parinst.Name, i, iiParNode);
    end
    else
    begin
      iiParPart := iiParNode.AppendElement('VarValue');
      iiParPart.SetAttr('ms:type', VariantTypeAsCSharpType(parInst.Value)  );
      case VariantTypeAsInt(parInst.Value) of
        varDate : iiParPart.Text := DatetimeToXSTR( TDateTime(parInst.Value));
      else
        iiParPart.Text := VarToStr(parInst.Value);
      end;
    end;

  end; // end for-cycle by parameters
  Xdoc.Save(outStream);
end;

class function TXMLRWConvertor.VariantTypeAsInt(varVar: Variant):integer;
var
  typeX : integer;
  basicType  : Integer;

begin
  TypeX := 0;
  if VarIsArray(varVar) then
  begin
     Result :=  $2000; //'varArray';
     Exit;
  end;
  // Get the Variant basic type :
  // this means excluding array or indirection modifiers
  basicType := VarType(varVar) and VarTypeMask;

  // Set a string to match the type
  case basicType of
    varEmpty     : typeX := $0001; //'varEmpty'; - null instead
    varNull      : typeX := $0001; //'varNull';
    varSmallInt  : typeX := $0002; //'varSmallInt';
    varInteger   : typeX := $0003; //'varInteger';
    varSingle    : typeX := $0004; //'varSingle';
    varDouble    : typeX := $0005; //'varDouble';
    varCurrency  : typeX := $0006; //'varCurrency';
    varDate      : typeX := $0007; //'varDate';
    varOleStr    : typeX := $0008; //'varOleStr';
    varDispatch  : typeX := $0009; //'varDispatch';
    varError     : typeX := $000A; //'varError';
    varBoolean   : typeX := $000B; //'varBoolean';
    varVariant   : typeX := $000C; //'varVariant';
    varUnknown   : typeX := $000D; //'varUnknown';  reference to IUnknown pointer
    varShortInt  : typeX := $0010; //'varShortInt';
    varByte      : typeX := $0011; //'varByte';
    varWord      : typeX := $0012; //'varWord';
    varLongWord  : typeX := $0013; //'varLongWord';
    varInt64     : typeX := $0014; //'varInt64';
    varStrArg    : typeX := $0048; //'varStrArg';
    varString    : typeX := $0100; //'varString';
    varAny       : typeX := $0101; //'varAny';
//    varTypeMask  : typeX := 'varTypeMask';
  end;
  Result :=  typeX;
end;

class function TXMLRWConvertor.VariantTypeAsCSharpType(varVar: Variant):String;
var
  typeX : String;
  basicType  : Integer;
begin

  if VarIsArray(varVar) then
  begin
     Result :=  'Array';//$2000; //'varArray';
     Exit;
  end;
  // Get the Variant basic type :
  // this means excluding array or indirection modifiers
  basicType := VarType(varVar) and VarTypeMask;
  // Set a string to match the type
  case basicType of
    varEmpty     : typeX := 'Null'; //$0001; //'varEmpty'; - null instead
    varNull      : typeX := 'Null'; //$0001; //'varNull';
    varSmallInt  : typeX := 'Int16';//$0002; //'varSmallInt';
    varInteger   : typeX := 'Int32';//$0003; //'varInteger';
    varSingle    : typeX := 'Single';//$0004; //'varSingle';
    varDouble    : typeX := 'Double';//$0005; //'varDouble';
    varCurrency  : typeX := 'Double';//$0006; //'varCurrency';
    varDate      : typeX := 'DateTime';//$0007; //'varDate';
    varOleStr    : typeX := 'WideString';//$0008; //'varOleStr';
    varDispatch  : typeX := 'Interface';//$0009; //'varDispatch';
    varError     : typeX := 'Error';//$000A; //'varError';
    varBoolean   : typeX := 'Boolean';//;$000B; //'varBoolean';
    varVariant   : typeX := 'Variant';//$000C; //'varVariant';
    varUnknown   : typeX := 'Object';//$000D; //'varUnknown';
    varByte      : typeX := 'Byte';//$0011; //'varByte';
    varWord      : typeX := 'UInt16';//$0012; //'varWord';
    varLongWord  : typeX := 'UInt32';//$0013; //'varLongWord';
    varInt64     : typeX := 'Int64';//$0014; //'varInt64';
//    varStrArg    : typeX := $0048; //'varStrArg'; - internal use in COM
    varString    : typeX := 'String';//$0100; //'varString';
//    varAny       : typeX := $0101; //'varAny'; - not used
//    varTypeMask  : typeX := 'varTypeMask';
  end;
  Result :=  typeX;
end;


class function TXMLRWConvertor.CSharpTypeAsVarTypeInt(StrType: String):Integer;
const
  ListStrAll: array[0..18] of String =
 (   'Null'      //$0001; //'varEmpty'; - null instead
    ,'Int16'     //varSmallInt  : typeX := 'Int16;'//$0002; //'varSmallInt';
    ,'Int32'     //varInteger   : typeX := 'Int32;'//$0003; //'varInteger';
    ,'Single'    //varSingle    : typeX := 'Single';//$0004; //'varSingle';
    ,'Double'    //varDouble    : typeX := 'Double';//$0005; //'varDouble';
    ,'Currency'  //varCurrency  : typeX := 'Currency';//$0006; //'varCurrency';
    ,'DateTime'  //varDate      : typeX := 'DateTime';//$0007; //'varDate';
    ,'WideString'//varOleStr    : typeX := 'WideString';//$0008; //'varOleStr';
    ,'Interface' //varDispatch  : typeX := 'Interface';//$0009; //'varDispatch';
    ,'Error'     //varError     : typeX := 'Error';//$000A; //'varError';
    ,'Boolean'   //varBoolean   : typeX := 'Boolean';//;$000B; //'varBoolean';
    ,'Variant'   //varVariant   : typeX := 'Variant';//$000C; //'varVariant';
    ,'Object'    //varUnknown   : typeX := 'Object';//$000D; //'varUnknown';
    ,'Byte'      //varByte      : typeX := 'Byte';//$0011; //'varByte';
    ,'Int16'    //varWord      : typeX := 'UInt16';//$0012; //'varWord';
    ,'Int32'    //varLongWord  : typeX := 'UInt32';//$0013; //'varLongWord';
    ,'Int64'     //varInt64     : typeX := 'Int64';//$0014; //'varInt64';
//    varStrArg    : typeX := $0048; //'varStrArg'; - internal use in COM
    ,'Sring'    //varString    : typeX := 'String'; $0100;
    ,'Array'     // varArray    : $2000;
    )
    ;
  ListIntAll: Array[0..18] of Integer = ($0001,$0002,$0003,$0004,$0005,$0006,$0007,$0008, $0009,$000A,$000B,$000C,$000D,$0011,$0012,$0013,$0014,$0100,$2000);
var
 i: integer;
begin
  i :=   AnsiIndexStr(StrType, ListStrAll);
  if i < 0 then
     Raise EisException.Create('Unknown string as Var Type: "' + StrType +'"', False, -1, 0);
  Result := ListIntAll[i];
end;


class procedure TXMLRWConvertor.CopyFields(SourceDataset, DestDataset: TDataset; doAdd: Boolean);
var i: integer;
    Fld: TField;
begin
  if not doAdd then DestDataset.Fields.Clear;
  for i:=0 to SourceDataset.Fields.Count-1 do
    begin
    if Assigned(DestDataset.Fields.FindField(SourceDataset.Fields[i].FieldName)) then
       Continue;
    Fld := CloneField(SourceDataset.Fields[i], DestDataset.Fields.Dataset);
    Fld.DataSet := DestDataset.Fields.Dataset;
    end;
end;

class function TXMLRWConvertor.DatasetToXMLString(
  varDataset: TDataset): string;
{
var
    cld: TClientDataset;
    i: integer;
    iiStr: IisStream;
    sOut:string;
}
begin
  Result := '';
  {   commented out as introducing unwanted miltithread-unsafe dependencies to Midas DCU and MS XMLDOM
  cld := TClientDataset.Create(nil);
  try
    CopyFields(varDataset, cld, true);
    cld.CreateDataSet;
    varDataset.Open;
    varDataset.First;
    cld.Open;
    while NOT varDataset.EOF do
    begin
       cld.Append;
       for i:=0 to varDataset.Fieldcount - 1 do
         cld.Fields[i].Value := varDataset.Fields[i].Value;
       varDataset.Next;
    end;
    cld.MergeChangeLog;
    iIstr := TisStream.CreateInMemory;
    cld.SaveToStream( iIstr.RealStream ,dfXML);
    sOut := iIstr.AsString;
    Result := sOut;
  finally
    cld.Close;
    cld.Free;
  end;
  }
end;

class procedure TXMLRWConvertor.WorkArrayParam(V : Variant; Name:string;  parIndex:integer; iiParNode: SimpleXML.IXMLElement);
var
   U: Variant;
   i:integer;
   iStream: IIsStream;
   iDs: IevDataSetHolder ;
   iiParPart: SimpleXML.IXMLElement;
   iiArrItem: SimpleXML.IXMLElement;
   sDX: string;
   iiDatXML: SimpleXml.IXMLDocument;
begin
  Assert(VarIsArray(V));
    iiParPart := iiParNode.AppendElement('Value');
  for i := VarArrayLowBound(V, 1) to VarArrayHighBound(V, 1) do
  begin
    U := V[i];
    case VariantTypeAsInt(U) of
      varUnknown:
        begin
          iiParPart.SetAttr('ms:type', 'Object[]');


          SysUtils.Supports(U, IIsStream, iStream); // type transform - call of QueryInterface
          iDS := TevDataSetHolder.Create;
          IStream.Position := 0;
          iDS.DataSet.LoadFromUniversalStream(iStream);
          sDX := DatasetToXMLString(iDs.Dataset);

           iiArrItem := iiParPart.AppendElement('ArrayItem');
           iiArrItem.SetAttr('ms:type', 'Object');
           if length(sDX) > 0 then
           begin
             iiDatXml := LoadXmlDocumentFromXML(sDX);
             iiArrItem.AppendChild (iiDatXML.DocumentElement.CloneNode(True));
           end;
        end;
      varString, varOleStr:
          begin
           iiParPart.SetAttr('ms:type', 'String[]'); // override parent
           iiArrItem := iiParPart.AppendElement('ArrayItem');
           iiArrItem.SetAttr('ms:type', 'String');
           iiArrItem.Text := VarToStr(U);
          end;
      varDate:
          begin
           iiParPart.SetAttr('ms:type', 'DateTime[]'); // override parent
           iiArrItem := iiParPart.AppendElement('ArrayItem');
           iiArrItem.SetAttr('ms:type', 'DateTime');
           iiArrItem.Text := DateTimeToXSTR(TDateTime(U));
          end;
      varArray:
         Raise EisException.Create('Arrays of Arrays are not expected in parameters: element "' + iiParNode.NodeName +'"', False, -1, 0);
//          PlaceArrayMemo(Memo, U)
    else
       begin
           iiParPart.SetAttr('ms:type', VariantTypeAsCSharpType(U) + '[]'); // override parent
         iiArrItem := iiParPart.AppendElement('ArrayItem');
         iiArrItem.SetAttr('ms:type', VariantTypeAsCSharpType(U));
         iiArrItem.Text := VarToStr(U);
       end
    end; // of case of type statement
  end; // of for-cycle by elements of array
end;


class function TXMLRWConvertor.WinVarTypeToRwType(
  iValue: integer): integer;
var
  i: integer;
begin
  for i := 0 to High(WinVarToRW) do
    if iValue = WinVarToRW[i].SysVarType then
    begin
       Result := Ord(WinVarToRW[i].RwVarType);
       Exit;
    end;
  Raise Exception.Create('Wrong Variant Type: '+IntToStr(iValue));
end;

class function TXMLRWConvertor.RwVarTypeToWin(iValue: integer): integer;
begin
  if (iValue > High(RwVarToWin))
     or
     (iValue < 0)
  then
    Raise Exception.Create('Wrong RWVarType: '+IntToStr(iValue));
  Result := RwVarToWin[iValue].SysVarType;
end;

end.
