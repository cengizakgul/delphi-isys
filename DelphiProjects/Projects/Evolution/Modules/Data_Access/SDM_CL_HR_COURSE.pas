// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_HR_COURSE;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses;

type
  TDM_CL_HR_COURSE = class(TDM_ddTable)
    CL_HR_COURSE: TCL_HR_COURSE;
    CL_HR_COURSECL_HR_COURSE_NBR: TIntegerField;
    CL_HR_COURSENAME: TStringField;
    CL_HR_COURSERENEWAL_STATUS : TStringField;
    CL_HR_COURSERENEWAL_STATUS_DESC: TStringField;
    procedure CL_HR_COURSECalcFields(DataSet: TDataSet);
    procedure CL_HR_COURSEBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation
uses EvUtils, SFieldCodeValues, EvExceptions;

{$R *.DFM}

procedure TDM_CL_HR_COURSE.CL_HR_COURSECalcFields(DataSet: TDataSet);
begin
  inherited;
  CL_HR_COURSERENEWAL_STATUS_DESC.AsString := ReturnDescription(RenewalStatus_ComboChoices, CL_HR_COURSERENEWAL_STATUS.AsString);

end;

procedure TDM_CL_HR_COURSE.CL_HR_COURSEBeforePost(DataSet: TDataSet);
begin
  if ( DataSet.FieldByName('NAME').IsNull ) or
     ( Trim(DataSet.FieldByName('NAME').asString) = '' )
  then
    raise EevException.Create( 'Course cannot have empty value' );
end;

initialization
  RegisterClass( TDM_CL_HR_COURSE );

finalization
  UnregisterClass( TDM_CL_HR_COURSE );

end.
