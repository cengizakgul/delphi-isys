inherited DM_CO_AUTO_ENLIST_RETURNS: TDM_CO_AUTO_ENLIST_RETURNS
  OldCreateOrder = True
  Left = 525
  Top = 114
  Height = 204
  Width = 464
  object CO_AUTO_ENLIST_RETURNS: TCO_AUTO_ENLIST_RETURNS
    ProviderName = 'CO_AUTO_ENLIST_RETURNS_PROV'
    OnCalcFields = CO_AUTO_ENLIST_RETURNSCalcFields
    Left = 86
    Top = 32
    object CO_AUTO_ENLIST_RETURNSCO_AUTO_ENLIST_RETURNS_NBR: TIntegerField
      FieldName = 'CO_AUTO_ENLIST_RETURNS_NBR'
    end
    object CO_AUTO_ENLIST_RETURNSCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
    end
    object CO_AUTO_ENLIST_RETURNSPROCESS_TYPE: TStringField
      FieldName = 'PROCESS_TYPE'
      Size = 1
    end
    object CO_AUTO_ENLIST_RETURNSAGENCY_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'AGENCY_NAME'
      Size = 40
      Calculated = True
    end
    object CO_AUTO_ENLIST_RETURNSADDITIONAL_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'ADDITIONAL_NAME'
      Size = 40
      Calculated = True
    end
    object CO_AUTO_ENLIST_RETURNSNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAME'
      Size = 60
      Calculated = True
    end
    object CO_AUTO_ENLIST_RETURNSSProcess: TStringField
      FieldKind = fkCalculated
      FieldName = 'SProcess'
      Calculated = True
    end
    object CO_AUTO_ENLIST_RETURNSAgencyName: TStringField
      FieldKind = fkCalculated
      FieldName = 'AgencyName'
      Size = 40
      Calculated = True
    end
    object CO_AUTO_ENLIST_RETURNSSY_GLOBAL_AGENCY_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
      Calculated = True
    end
    object CO_AUTO_ENLIST_RETURNSReturnFrequency: TStringField
      FieldKind = fkCalculated
      FieldName = 'ReturnFrequency'
      Size = 12
      Calculated = True
    end
  end
end
