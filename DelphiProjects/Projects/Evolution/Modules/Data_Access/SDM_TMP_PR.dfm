inherited DM_TMP_PR: TDM_TMP_PR
  OldCreateOrder = False
  Left = 272
  Top = 131
  Height = 480
  Width = 696
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 123
    Top = 192
  end
  object TMP_PR: TTMP_PR
    Aggregates = <>
    Params = <>
    ProviderName = 'TMP_PR_PROV'
    ValidateWithMask = True
    Left = 120
    Top = 80
    object TMP_PRPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Origin = '"TMP_PR"."PR_NBR"'
      Required = True
    end
    object TMP_PRCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"TMP_PR"."CL_NBR"'
      Required = True
    end
    object TMP_PRCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"TMP_PR"."CO_NBR"'
      Required = True
    end
    object TMP_PRRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
      Origin = '"TMP_PR"."RUN_NUMBER"'
      Required = True
    end
    object TMP_PRCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
      Origin = '"TMP_PR"."CHECK_DATE"'
      Required = True
    end
    object TMP_PRSCHEDULED: TStringField
      FieldName = 'SCHEDULED'
      Origin = '"TMP_PR"."SCHEDULED"'
      Required = True
      Size = 1
    end
    object TMP_PRTYPE: TStringField
      FieldName = 'PAYROLL_TYPE'
      Origin = '"TMP_PR"."PAYROLL_TYPE"'
      Required = True
      Size = 1
    end
    object TMP_PRSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = '"TMP_PR"."STATUS"'
      Required = True
      Size = 1
    end
    object TMP_PRAPPROVED_BY_FINANCE: TStringField
      FieldName = 'APPROVED_BY_FINANCE'
      FixedChar = True
      Size = 1
    end
    object TMP_PRAPPROVED_BY_MANAGEMENT: TStringField
      FieldName = 'APPROVED_BY_MANAGEMENT'
      FixedChar = True
      Size = 1
    end
    object TMP_PRAPPROVED_BY_TAX: TStringField
      FieldName = 'APPROVED_BY_TAX'
      FixedChar = True
      Size = 1
    end
    object TMP_PRPROCESS_DATE: TDateTimeField
      FieldName = 'PROCESS_DATE'
    end
    object TMP_PRSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object TMP_PRClient_Number_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Client_Number_Lookup'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Lookup = True
    end
    object TMP_PRCompany_Number_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Company_Number_Lookup'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Lookup = True
    end
    object TMP_PRCompany_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Company_Name_Lookup'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object TMP_PRProcess_Priority: TIntegerField
      FieldKind = fkLookup
      FieldName = 'Process_Priority'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'PROCESS_PRIORITY'
      KeyFields = 'CL_NBR;CO_NBR'
      Lookup = True
    end
    object TMP_PRSelected: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Selected'
      Size = 1
    end
  end
end
