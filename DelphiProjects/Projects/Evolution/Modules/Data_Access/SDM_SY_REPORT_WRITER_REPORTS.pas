// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_REPORT_WRITER_REPORTS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, EvConsts;

type
  TDM_SY_REPORT_WRITER_REPORTS = class(TDM_ddTable)
    SY_REPORT_WRITER_REPORTS: TSY_REPORT_WRITER_REPORTS;
    SY_REPORT_WRITER_REPORTSSY_REPORT_WRITER_REPORTS_NBR: TIntegerField;
    SY_REPORT_WRITER_REPORTSREPORT_DESCRIPTION: TStringField;
    SY_REPORT_WRITER_REPORTSREPORT_TYPE: TStringField;
    SY_REPORT_WRITER_REPORTSREPORT_FILE: TBlobField;
    SY_REPORT_WRITER_REPORTSNOTES: TBlobField;
    SY_REPORT_WRITER_REPORTSRWDescription: TStringField;
    SY_REPORT_WRITER_REPORTSREPORT_STATUS: TStringField;
    SY_REPORT_WRITER_REPORTSMEDIA_TYPE: TStringField;
    SY_REPORT_WRITER_REPORTSCLASS_NAME: TStringField;
    SY_REPORT_WRITER_REPORTSANCESTOR_CLASS_NAME: TStringField;
    procedure SY_REPORT_WRITER_REPORTSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SY_REPORT_WRITER_REPORTS: TDM_SY_REPORT_WRITER_REPORTS;

implementation

{$R *.DFM}

procedure TDM_SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTSCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if SY_REPORT_WRITER_REPORTSNOTES.AsString <> LAZY_BLOB then
    SY_REPORT_WRITER_REPORTSRWDescription.AsString := SY_REPORT_WRITER_REPORTSNOTES.AsString; 
end;

initialization
  RegisterClass(TDM_SY_REPORT_WRITER_REPORTS);

finalization
  UnregisterClass(TDM_SY_REPORT_WRITER_REPORTS);

end.
