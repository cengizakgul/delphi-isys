inherited DM_SB_GLOBAL_AGENCY_CONTACTS: TDM_SB_GLOBAL_AGENCY_CONTACTS
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 80
    Top = 120
  end
  object SB_GLOBAL_AGENCY_CONTACTS: TSB_GLOBAL_AGENCY_CONTACTS
    Aggregates = <>
    Params = <>
    ProviderName = 'SB_GLOBAL_AGENCY_CONTACTS_PROV'
    OnNewRecord = SB_GLOBAL_AGENCY_CONTACTSNewRecord
    PictureMasks.Strings = (
      'PHONE'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F')
    ValidateWithMask = True
    Left = 80
    Top = 56
    object SB_GLOBAL_AGENCY_CONTACTSSB_GLOBAL_AGENCY_CONTACTS_NBR: TIntegerField
      FieldName = 'SB_GLOBAL_AGENCY_CONTACTS_NBR'
    end
    object SB_GLOBAL_AGENCY_CONTACTSSY_GLOBAL_AGENCY_NBR: TIntegerField
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
    end
    object SB_GLOBAL_AGENCY_CONTACTSCONTACT_NAME: TStringField
      FieldName = 'CONTACT_NAME'
      Size = 30
    end
    object SB_GLOBAL_AGENCY_CONTACTSPHONE: TStringField
      FieldName = 'PHONE'
    end
    object SB_GLOBAL_AGENCY_CONTACTSFAX: TStringField
      FieldName = 'FAX'
    end
    object SB_GLOBAL_AGENCY_CONTACTSE_MAIL_ADDRESS: TStringField
      FieldName = 'E_MAIL_ADDRESS'
      Size = 80
    end
    object SB_GLOBAL_AGENCY_CONTACTSNOTES: TBlobField
      FieldName = 'NOTES'
      BlobType = ftBlob
      Size = 1
    end
  end
end
