// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_PERSON_DOCUMENTS;

interface

uses
  SDM_ddTable, SDataDictClient,
  SysUtils, Classes, DB,   kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses;


type
  TDM_CL_PERSON_DOCUMENTS = class(TDM_ddTable)
    CL_PERSON_DOCUMENTS: TCL_PERSON_DOCUMENTS;
    procedure CL_PERSON_DOCUMENTSAfterPost(DataSet: TDataSet);
    procedure CL_PERSON_DOCUMENTSAfterCancel(DataSet: TDataSet);
    procedure CL_PERSON_DOCUMENTSAfterEdit(DataSet: TDataSet);
  private
  public
  end;


implementation

{$R *.dfm}

procedure TDM_CL_PERSON_DOCUMENTS.CL_PERSON_DOCUMENTSAfterPost(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('DOCUMENT').Required := true;
end;

procedure TDM_CL_PERSON_DOCUMENTS.CL_PERSON_DOCUMENTSAfterCancel(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('DOCUMENT').Required := true;
end;

procedure TDM_CL_PERSON_DOCUMENTS.CL_PERSON_DOCUMENTSAfterEdit(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('DOCUMENT').Required := false;
  // CL_PERSON_DOCUMENTS.DOCUMENT is 'omittedField' and required simultaneously
  // So when a user posts record she tries to post Null value in required field
  // Note that this constraint still works when user posts just inserted record
end;

initialization
  RegisterClass(TDM_CL_PERSON_DOCUMENTS);

finalization
  UnregisterClass(TDM_CL_PERSON_DOCUMENTS);

end.
