// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_DEPARTMENT;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Wwdatsrc, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, 
  SDDClasses, Variants, EvContext, EvUIComponents, EvClientDataSet,
  EvDataAccessComponents;

type
  TDM_CO_DEPARTMENT = class(TDM_ddTable)
    CO_DEPARTMENT: TCO_DEPARTMENT;
    CO_DEPARTMENTCO_DEPARTMENT_NBR: TIntegerField;
    CO_DEPARTMENTCO_BRANCH_NBR: TIntegerField;
    CO_DEPARTMENTCUSTOM_DEPARTMENT_NUMBER: TStringField;
    CO_DEPARTMENTNAME: TStringField;
    CO_DEPARTMENTADDRESS1: TStringField;
    CO_DEPARTMENTADDRESS2: TStringField;
    CO_DEPARTMENTCITY: TStringField;
    CO_DEPARTMENTSTATE: TStringField;
    CO_DEPARTMENTZIP_CODE: TStringField;
    CO_DEPARTMENTCONTACT1: TStringField;
    CO_DEPARTMENTPHONE1: TStringField;
    CO_DEPARTMENTDESCRIPTION1: TStringField;
    CO_DEPARTMENTCONTACT2: TStringField;
    CO_DEPARTMENTPHONE2: TStringField;
    CO_DEPARTMENTDESCRIPTION2: TStringField;
    CO_DEPARTMENTFAX: TStringField;
    CO_DEPARTMENTFAX_DESCRIPTION: TStringField;
    CO_DEPARTMENTE_MAIL_ADDRESS: TStringField;
    CO_DEPARTMENTHOME_CO_STATES_NBR: TIntegerField;
    CO_DEPARTMENTPAY_FREQUENCY_HOURLY: TStringField;
    CO_DEPARTMENTPAY_FREQUENCY_SALARY: TStringField;
    CO_DEPARTMENTCL_TIMECLOCK_IMPORTS_NBR: TIntegerField;
    CO_DEPARTMENTPRINT_DEPT_ADDRESS_ON_CHECKS: TStringField;
    CO_DEPARTMENTPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DEPARTMENTCL_BILLING_NBR: TIntegerField;
    CO_DEPARTMENTBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DEPARTMENTTAX_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DEPARTMENTDD_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DEPARTMENTCO_WORKERS_COMP_NBR: TIntegerField;
    CO_DEPARTMENTUNION_CL_E_DS_NBR: TIntegerField;
    CO_DEPARTMENTOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CO_DEPARTMENTOVERRIDE_PAY_RATE: TFloatField;
    CO_DEPARTMENTDEPARTMENT_NOTES: TBlobField;
    CO_DEPARTMENTFILLER: TStringField;
    CO_DEPARTMENTHOME_STATE_TYPE: TStringField;
    CO_DEPARTMENTGENERAL_LEDGER_TAG: TStringField;
    CO_DEPARTMENTCL_DELIVERY_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTPR_CHECK_MB_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTPR_EE_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField;
    CO_DEPARTMENTWORKSITE_ID: TStringField;
    CO_DEPARTMENTAccountNumber: TStringField;
    CO_DEPARTMENTPrimary: TStringField;
    procedure CO_DEPARTMENTBeforePost(DataSet: TDataSet);
    procedure CO_DEPARTMENTCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TDM_CO_DEPARTMENT.CO_DEPARTMENTBeforePost(DataSet: TDataSet);
var
 s: String;
 bLookup, bCalcFields : Boolean;
begin

  bLookup := DM_COMPANY.CO_DEPARTMENT.LookupsEnabled;
  bCalcFields := DM_COMPANY.CO_DEPARTMENT.AlwaysCallCalcFields;

  DM_COMPANY.CO_DEPARTMENT.AlwaysCallCalcFields := false;
  DM_COMPANY.CO_DEPARTMENT.LookupsEnabled := false;
  try
    HandleCustomNumber(DataSet.FieldByName('CUSTOM_DEPARTMENT_NUMBER'), 20);

    if Trim(DataSet.FieldByName('AccountNumber').AsString) <> '' then
    begin
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('AccountNumber').AsString, 1, 20);
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('Primary').AsString, 21, 1);
    end
    else
    begin
      if not DataSet.FieldByName('FILLER').IsNull then
      begin
        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '', 1, 20);

        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString,21, 1);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '',21,  1);
        DataSet.FieldByName('Primary').AsString := '';
      end;
    end;
  finally
    DM_COMPANY.CO_DEPARTMENT.AlwaysCallCalcFields := bCalcFields;
    DM_COMPANY.CO_DEPARTMENT.LookupsEnabled := bLookup;
  end;


end;

procedure TDM_CO_DEPARTMENT.CO_DEPARTMENTCalcFields(DataSet: TDataSet);
var 
 s : String;
begin
  inherited;

  if not DataSet.FieldByName('FILLER').IsNull then
  begin
    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
    if trim(s) <> '' then
      Dataset.FieldByName('AccountNumber').AsString := s;

    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 21, 1);
    if trim(s) <> '' then
      Dataset.FieldByName('Primary').AsString := s;
  end;
end;

initialization
  RegisterClass(TDM_CO_DEPARTMENT);

finalization
  UnregisterClass(TDM_CO_DEPARTMENT);

end.
