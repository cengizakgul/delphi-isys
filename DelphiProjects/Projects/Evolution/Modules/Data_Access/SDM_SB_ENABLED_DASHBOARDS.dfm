inherited DM_SB_ENABLED_DASHBOARDS: TDM_SB_ENABLED_DASHBOARDS
  OldCreateOrder = True
  Left = 632
  Top = 228
  Height = 330
  Width = 347
  object SB_ENABLED_DASHBOARDS: TSB_ENABLED_DASHBOARDS
    ProviderName = 'SB_ENABLED_DASHBOARDS_PROV'
    OnCalcFields = SB_ENABLED_DASHBOARDSCalcFields
    Left = 96
    Top = 120
    object SB_ENABLED_DASHBOARDSSB_ENABLED_DASHBOARDS_NBR: TIntegerField
      FieldName = 'SB_ENABLED_DASHBOARDS_NBR'
    end
    object SB_ENABLED_DASHBOARDSDASHBOARD_LEVEL: TStringField
      FieldName = 'DASHBOARD_LEVEL'
      Size = 1
    end
    object SB_ENABLED_DASHBOARDSDASHBOARD_NBR: TIntegerField
      FieldName = 'DASHBOARD_NBR'
    end
    object SB_ENABLED_DASHBOARDSDESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRIPTION'
      Size = 80
      Calculated = True
    end
    object SB_ENABLED_DASHBOARDSLEVEL_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'LEVEL_DESC'
      Size = 8
      Calculated = True
    end
    object SB_ENABLED_DASHBOARDSSY_ANALYTICS_TIER_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SY_ANALYTICS_TIER_NBR'
      Calculated = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 184
    Top = 40
  end
end
