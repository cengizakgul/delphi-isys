inherited DM_CL_HR_PERSON_HANDICAPS: TDM_CL_HR_PERSON_HANDICAPS
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object CL_HR_PERSON_HANDICAPS: TCL_HR_PERSON_HANDICAPS
    ProviderName = 'CL_HR_PERSON_HANDICAPS_PROV'
    Left = 136
    Top = 112
    object CL_HR_PERSON_HANDICAPSCL_HR_PERSON_HANDICAPS_NBR: TIntegerField
      FieldName = 'CL_HR_PERSON_HANDICAPS_NBR'
    end
    object CL_HR_PERSON_HANDICAPSCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object CL_HR_PERSON_HANDICAPSSY_HR_HANDICAPS_NBR: TIntegerField
      FieldName = 'SY_HR_HANDICAPS_NBR'
    end
    object CL_HR_PERSON_HANDICAPSHandicap: TStringField
      FieldKind = fkLookup
      FieldName = 'Handicap'
      LookupDataSet = DM_SY_HR_HANDICAPS.SY_HR_HANDICAPS
      LookupKeyFields = 'SY_HR_HANDICAPS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SY_HR_HANDICAPS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 280
    Top = 88
  end
end
