inherited DM_SY_FED_TAX_TABLE_BRACKETS: TDM_SY_FED_TAX_TABLE_BRACKETS
  Height = 177
  Width = 302
  object SY_FED_TAX_TABLE_BRACKETS: TSY_FED_TAX_TABLE_BRACKETS
    ProviderName = 'SY_FED_TAX_TABLE_BRACKETS_PROV'
    PictureMasks.Strings = (
      
        'GREATER_THAN_VALUE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F'
      
        'LESS_THAN_VALUE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'NEXT_GREATER_THAN_VALUE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_LESS_THAN_VALUE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F')
    Left = 92
    Top = 28
    object SY_FED_TAX_TABLE_BRACKETSGREATER_THAN_VALUE: TFloatField
      FieldName = 'GREATER_THAN_VALUE'
    end
    object SY_FED_TAX_TABLE_BRACKETSLESS_THAN_VALUE: TFloatField
      FieldName = 'LESS_THAN_VALUE'
    end
    object SY_FED_TAX_TABLE_BRACKETSMARITAL_STATUS: TStringField
      FieldName = 'MARITAL_STATUS'
      Size = 1
    end
    object SY_FED_TAX_TABLE_BRACKETSPERCENTAGE: TFloatField
      FieldName = 'PERCENTAGE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_FED_TAX_TABLE_BRACKETSSY_FED_TAX_TABLE_BRACKETS_NBR: TIntegerField
      FieldName = 'SY_FED_TAX_TABLE_BRACKETS_NBR'
    end
  end
end
