inherited DM_SB_VENDOR: TDM_SB_VENDOR
  OldCreateOrder = True
  Left = 630
  Top = 227
  Height = 330
  Width = 347
  object SB_VENDOR: TSB_VENDOR
    ProviderName = 'SB_VENDOR_PROV'
    OnCalcFields = SB_VENDORCalcFields
    Left = 56
    Top = 48
    object SB_VENDORSB_VENDOR_NBR: TIntegerField
      FieldName = 'SB_VENDOR_NBR'
    end
    object SB_VENDORLEVEL_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'LEVEL_DESC'
      Size = 8
      Calculated = True
    end
    object SB_VENDORNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAME'
      Size = 80
      Calculated = True
    end
    object SB_VENDORVENDOR_NBR: TIntegerField
      FieldName = 'VENDOR_NBR'
    end
    object SB_VENDORVENDORS_LEVEL: TStringField
      FieldName = 'VENDORS_LEVEL'
      Size = 1
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 184
    Top = 40
  end
end
