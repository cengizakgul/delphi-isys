// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_GROUP;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses;

type
  TDM_CO_GROUP = class(TDM_ddTable)
    CO_GROUP: TCO_GROUP;
    CO_GROUPCO_GROUP_NBR: TIntegerField;
    CO_GROUPCO_NBR: TIntegerField;
    CO_GROUPGROUP_TYPE: TStringField;
    CO_GROUPGROUP_NAME: TStringField;
    CO_GROUPGROUP_TYPE_DESC: TStringField;
    procedure CO_GROUPCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation
uses EvUtils, SFieldCodeValues;

{$R *.DFM}
procedure TDM_CO_GROUP.CO_GROUPCalcFields(DataSet: TDataSet);
begin
  inherited;
   CO_GROUPGROUP_TYPE_DESC.AsString := ReturnDescription(ESS_GroupType_ComboChoices, CO_GROUPGROUP_TYPE.AsString);
end;

initialization
  RegisterClass( TDM_CO_GROUP );

finalization
  UnregisterClass( TDM_CO_GROUP );

end.
