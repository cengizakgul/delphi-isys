// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_DELIVERY_SERVICE;

interface

uses
  SDM_ddTable, SDataDictBureau,
  SysUtils, Classes, DB,   SDataStructure, SDDClasses, SDataDictsystem,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SB_DELIVERY_SERVICE = class(TDM_ddTable)
    SB_DELIVERY_SERVICE: TSB_DELIVERY_SERVICE;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_DELIVERY_SERVICESB_DELIVERY_SERVICE_NBR: TIntegerField;
    SB_DELIVERY_SERVICESY_DELIVERY_SERVICE_NBR: TStringField;
    SB_DELIVERY_SERVICESY_DELIVERY_SERVICE_NBR2: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_SB_DELIVERY_SERVICE);

finalization
  UnregisterClass(TDM_SB_DELIVERY_SERVICE);

end.
