// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_LOCAL_EXEMPTIONS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   SDataAccessCommon, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses;

type
  TDM_SY_LOCAL_EXEMPTIONS = class(TDM_ddTable)
    SY_LOCAL_EXEMPTIONS: TSY_LOCAL_EXEMPTIONS;
    SY_LOCAL_EXEMPTIONSEXEMPT: TStringField;
    SY_LOCAL_EXEMPTIONSE_D_CODE_TYPE: TStringField;
    SY_LOCAL_EXEMPTIONSSY_LOCALS_NBR: TIntegerField;
    SY_LOCAL_EXEMPTIONSSY_LOCAL_EXEMPTIONS_NBR: TIntegerField;
    SY_LOCAL_EXEMPTIONSEDDescription: TStringField;
    procedure SY_LOCAL_EXEMPTIONSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TDM_SY_LOCAL_EXEMPTIONS.SY_LOCAL_EXEMPTIONSCalcFields(
  DataSet: TDataSet);
begin
  DataSet['EDDescription'] := GetEDTypeDescription(DataSet.FieldByName('E_D_CODE_TYPE').AsString);
end;

initialization
  RegisterClass(TDM_SY_LOCAL_EXEMPTIONS);

finalization
  UnregisterClass(TDM_SY_LOCAL_EXEMPTIONS);

end.
