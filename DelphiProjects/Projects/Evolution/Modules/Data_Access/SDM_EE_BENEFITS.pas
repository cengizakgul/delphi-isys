// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_BENEFITS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, evUtils, Variants,
  EvClientDataSet;

type
  TDM_EE_BENEFITS = class(TDM_ddTable)
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EE_BENEFITS: TEE_BENEFITS;
    EE_BENEFITSCOBRA_FILING_TYPE: TStringField;
    EE_BENEFITSDEDUCTION_FREQUENCY: TStringField;
    EE_BENEFITSEE_NBR: TIntegerField;
    EE_BENEFITSENROLLMENT_STATUS: TStringField;
    EE_BENEFITSTOTAL_PREMIUM_AMOUNT: TFloatField;
    EE_BENEFITSBENEFIT_EFFECTIVE_DATE: TDateField;
    EE_BENEFITSCOBRA_STATUS: TStringField;
    EE_BENEFITSBENEFIT_TYPE: TStringField;
    EE_BENEFITSEXPIRATION_DATE: TDateTimeField;
    EE_BENEFITSER_RATE: TFloatField;
    EE_BENEFITSEE_RATE: TFloatField;
    EE_BENEFITSCOBRA_RATE: TFloatField;
    DM_COMPANY: TDM_COMPANY;
    EE_BENEFITSCO_BENEFITS_NBR: TIntegerField;
    EE_BENEFITSEE_BENEFITS_NBR: TIntegerField;
    EE_BENEFITSCO_BENEFIT_SUBTYPE_NBR: TIntegerField;
    EE_BENEFITSBenefit: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}
initialization
  RegisterClass( TDM_EE_BENEFITS );

finalization
  UnregisterClass( TDM_EE_BENEFITS );

end.
