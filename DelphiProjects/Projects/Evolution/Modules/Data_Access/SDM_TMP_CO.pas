// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_TMP_CO;

interface

uses
  SDM_ddTable, SDataDictTemp,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SFieldCodeValues, EvUtils,
  SDataStructure, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet,
  SDataDictbureau;

type
  TDM_TMP_CO = class(TDM_ddTable)
    TMP_CO: TTMP_CO;
    TMP_COACH_SB_BANK_ACCOUNT_NBR: TIntegerField;
    TMP_COAUTO_ENLIST: TStringField;
    TMP_COCL_CO_CONSOLIDATION_NBR: TIntegerField;
    TMP_COCL_NBR: TIntegerField;
    TMP_COCO_NBR: TIntegerField;
    TMP_COCUSTOMER_SERVICE_SB_USER_NBR: TIntegerField;
    TMP_COCUSTOM_COMPANY_NUMBER: TStringField;
    TMP_CODBA: TStringField;
    TMP_COEFTPS_ENROLLMENT_DATE: TDateTimeField;
    TMP_COEFTPS_ENROLLMENT_NUMBER: TStringField;
    TMP_COEFTPS_ENROLLMENT_STATUS: TStringField;
    TMP_COEFTPS_SEQUENCE_NUMBER: TStringField;
    TMP_COFEDERAL_TAX_DEPOSIT_FREQUENCY: TStringField;
    TMP_COFEDERAL_TAX_PAYMENT_METHOD: TStringField;
    TMP_COFEIN: TStringField;
    TMP_CONAME: TStringField;
    TMP_COOBC: TStringField;
    TMP_COPROCESS_PRIORITY: TIntegerField;
    TMP_COSTART_DATE: TDateField;
    TMP_COSY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField;
    TMP_COTAX_SERVICE: TStringField;
    TMP_COTERMINATION_CODE: TStringField;
    TMP_COTERMINATION_DATE: TDateField;
    TMP_COTRUST_SERVICE: TStringField;
    TMP_COTERMINATION_CODE_DESC: TStringField;
    TMP_COCUSTOM_CLIENT_NUMBER: TStringField;
    DM_TEMPORARY: TDM_TEMPORARY;
    TMP_COUserLastName: TStringField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    TMP_COUserFirstName: TStringField;
    TMP_COSelected: TStringField;
    TMP_COHOME_CO_STATES_NBR: TIntegerField;
    TMP_COHomeState: TStringField;
    evcsTmpStatesLookup: TevClientDataSet;
    evcsTmpStatesLookupCL_NBR: TIntegerField;
    evcsTmpStatesLookupCO_STATES_NBR: TIntegerField;
    evcsTmpStatesLookupSTATE: TStringField;
    procedure TMP_COCalcFields(DataSet: TDataSet);
    procedure TMP_COPrepareLookups(var Lookups: TLookupArray);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

constructor TDM_TMP_CO.Create(AOwner: TComponent);
begin
  Tag := Integer(GetCurrentThreadId);
  inherited;
end;

procedure TDM_TMP_CO.TMP_COCalcFields(DataSet: TDataSet);
begin
  TMP_COTERMINATION_CODE_DESC.AsString := ReturnDescription(Termination_ComboChoices, TMP_COTERMINATION_CODE.AsString);
end;

procedure TDM_TMP_CO.TMP_COPrepareLookups(var Lookups: TLookupArray);
begin
  if not evcsTmpStatesLookup.Active then
    ctx_DataAccess.OpenDataSets([evcsTmpStatesLookup]);

  if not DM_SERVICE_BUREAU.SB_USER.Active then
    ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_USER]);

  inherited;

end;

initialization
  RegisterClass(TDM_TMP_CO);

finalization
  UnregisterClass(TDM_TMP_CO);

end.
