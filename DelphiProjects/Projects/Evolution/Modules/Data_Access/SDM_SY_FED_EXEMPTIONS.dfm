inherited DM_SY_FED_EXEMPTIONS: TDM_SY_FED_EXEMPTIONS
  Left = 491
  Top = 233
  Height = 358
  Width = 498
  object SY_FED_EXEMPTIONS: TSY_FED_EXEMPTIONS
    ProviderName = 'SY_FED_EXEMPTIONS_PROV'
    OnCalcFields = SY_FED_EXEMPTIONSCalcFields
    Left = 64
    Top = 32
    object SY_FED_EXEMPTIONSEXEMPT_EMPLOYEE_EIC: TStringField
      FieldName = 'EXEMPT_EMPLOYEE_EIC'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSEXEMPT_EMPLOYEE_MEDICARE: TStringField
      FieldName = 'EXEMPT_EMPLOYEE_MEDICARE'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSEXEMPT_EMPLOYEE_OASDI: TStringField
      FieldName = 'EXEMPT_EMPLOYEE_OASDI'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSEXEMPT_EMPLOYER_MEDICARE: TStringField
      FieldName = 'EXEMPT_EMPLOYER_MEDICARE'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSEXEMPT_EMPLOYER_OASDI: TStringField
      FieldName = 'EXEMPT_EMPLOYER_OASDI'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSEXEMPT_FEDERAL: TStringField
      FieldName = 'EXEMPT_FEDERAL'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSEXEMPT_FUI: TStringField
      FieldName = 'EXEMPT_FUI'
      Required = True
      Size = 1
    end
    object SY_FED_EXEMPTIONSE_D_CODE_TYPE: TStringField
      FieldName = 'E_D_CODE_TYPE'
      Required = True
      Size = 2
    end
    object SY_FED_EXEMPTIONSSY_FED_EXEMPTIONS_NBR: TIntegerField
      FieldName = 'SY_FED_EXEMPTIONS_NBR'
      Required = True
    end
    object SY_FED_EXEMPTIONSW2_BOX: TStringField
      FieldName = 'W2_BOX'
      Size = 4
    end
    object SY_FED_EXEMPTIONSEDDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'EDDescription'
      Size = 60
    end
  end
end
