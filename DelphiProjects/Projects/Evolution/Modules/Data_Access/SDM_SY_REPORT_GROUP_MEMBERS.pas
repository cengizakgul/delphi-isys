unit SDM_SY_REPORT_GROUP_MEMBERS;

interface

uses
  SysUtils, Classes, SDDClasses, SDataDictSystem, SDM_ddTable,
  SDataStructure, DB, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDM_SY_REPORT_WRITER_REPORTS, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SY_REPORT_GROUP_MEMBERS = class(TDM_ddTable)
    SY_REPORT_GROUP_MEMBERS: TSY_REPORT_GROUP_MEMBERS;
    SY_REPORT_GROUP_MEMBERSSY_REPORT_GROUPS_NBR: TIntegerField;
    SY_REPORT_GROUP_MEMBERSSY_REPORT_GROUP_MEMBERS_NBR: TIntegerField;
    SY_REPORT_GROUP_MEMBERSSY_REPORT_WRITER_REPORTS_NBR: TIntegerField;
    SY_REPORT_GROUP_MEMBERSSY_REPORT_GROUP_NAME_Lookup: TStringField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  private
  public

  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_SY_REPORT_GROUP_MEMBERS);

finalization
  UnregisterClass(TDM_SY_REPORT_GROUP_MEMBERS);

end.
