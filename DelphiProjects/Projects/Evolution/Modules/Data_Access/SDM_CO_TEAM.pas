// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_TEAM;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses, Wwdatsrc, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, 
  EvDataAccessComponents, Variants, EvContext, EvUIComponents, EvClientDataSet;

type
  TDM_CO_TEAM = class(TDM_ddTable)
    CO_TEAM: TCO_TEAM;
    CO_TEAMCO_TEAM_NBR: TIntegerField;
    CO_TEAMCO_DEPARTMENT_NBR: TIntegerField;
    CO_TEAMCUSTOM_TEAM_NUMBER: TStringField;
    CO_TEAMNAME: TStringField;
    CO_TEAMADDRESS1: TStringField;
    CO_TEAMADDRESS2: TStringField;
    CO_TEAMCITY: TStringField;
    CO_TEAMSTATE: TStringField;
    CO_TEAMZIP_CODE: TStringField;
    CO_TEAMCONTACT1: TStringField;
    CO_TEAMPHONE1: TStringField;
    CO_TEAMDESCRIPTION1: TStringField;
    CO_TEAMCONTACT2: TStringField;
    CO_TEAMPHONE2: TStringField;
    CO_TEAMDESCRIPTION2: TStringField;
    CO_TEAMFAX: TStringField;
    CO_TEAMFAX_DESCRIPTION: TStringField;
    CO_TEAME_MAIL_ADDRESS: TStringField;
    CO_TEAMHOME_CO_STATES_NBR: TIntegerField;
    CO_TEAMPAY_FREQUENCY_HOURLY: TStringField;
    CO_TEAMPAY_FREQUENCY_SALARY: TStringField;
    CO_TEAMCL_TIMECLOCK_IMPORTS_NBR: TIntegerField;
    CO_TEAMPRINT_GROUP_ADDRESS_ON_CHECK: TStringField;
    CO_TEAMPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_TEAMCL_BILLING_NBR: TIntegerField;
    CO_TEAMBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_TEAMTAX_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_TEAMDD_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_TEAMUNION_CL_E_DS_NBR: TIntegerField;
    CO_TEAMOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CO_TEAMOVERRIDE_PAY_RATE: TFloatField;
    CO_TEAMCO_WORKERS_COMP_NBR: TIntegerField;
    CO_TEAMTEAM_NOTES: TBlobField;
    CO_TEAMHOME_STATE_TYPE: TStringField;
    CO_TEAMGENERAL_LEDGER_TAG: TStringField;
    CO_TEAMCL_DELIVERY_GROUP_NBR: TIntegerField;
    CO_TEAMPR_CHECK_MB_GROUP_NBR: TIntegerField;
    CO_TEAMPR_EE_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_TEAMPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField;
    CO_TEAMTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    CO_TEAMPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_TEAMPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField;
    CO_TEAMFILLER: TStringField;
    CO_TEAMWORKSITE_ID: TStringField;
    CO_TEAMAccountNumber: TStringField;
    CO_TEAMPrimary: TStringField;
    procedure CO_TEAMBeforePost(DataSet: TDataSet);
    procedure CO_TEAMCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_TEAM: TDM_CO_TEAM;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TDM_CO_TEAM.CO_TEAMBeforePost(DataSet: TDataSet);
var
 s: String;
 bLookup, bCalcFields : Boolean;
begin

  bLookup := DM_COMPANY.CO_TEAM.LookupsEnabled;
  bCalcFields := DM_COMPANY.CO_TEAM.AlwaysCallCalcFields;

  DM_COMPANY.CO_TEAM.AlwaysCallCalcFields := false;
  DM_COMPANY.CO_TEAM.LookupsEnabled := false;
  try
    HandleCustomNumber(DataSet.FieldByName('CUSTOM_TEAM_NUMBER'), 20);
    if Trim(DataSet.FieldByName('AccountNumber').AsString) <> '' then
    begin
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('AccountNumber').AsString, 1, 20);
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('Primary').AsString, 21, 1);
    end
    else
    begin
      if not DataSet.FieldByName('FILLER').IsNull then
      begin
        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '', 1, 20);

        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString,21, 1);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '',21,  1);
        DataSet.FieldByName('Primary').AsString := '';
      end;
    end;
  finally
    DM_COMPANY.CO_TEAM.AlwaysCallCalcFields := bCalcFields;
    DM_COMPANY.CO_TEAM.LookupsEnabled := bLookup;
  end;


end;

procedure TDM_CO_TEAM.CO_TEAMCalcFields(DataSet: TDataSet);
var 
 s : String;
begin
  inherited;

  if not DataSet.FieldByName('FILLER').IsNull then
  begin
    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
    if trim(s) <> '' then
      Dataset.FieldByName('AccountNumber').AsString := s;

    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 21, 1);
    if trim(s) <> '' then
      Dataset.FieldByName('Primary').AsString := s;
  end;
end;

initialization
  RegisterClass(TDM_CO_TEAM);

finalization
  UnregisterClass(TDM_CO_TEAM);

end.
