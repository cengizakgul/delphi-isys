// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_JOBS_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure;  

type
  TDM_CO_JOBS_LOCALS = class(TDM_ddTable)
    CO_JOBS_LOCALS: TCO_JOBS_LOCALS;
    DM_COMPANY: TDM_COMPANY;
    CO_JOBS_LOCALSCO_JOBS_LOCALS_NBR: TIntegerField;
    CO_JOBS_LOCALSCO_JOBS_NBR: TIntegerField;
    CO_JOBS_LOCALSCO_LOCAL_TAX_NBR: TIntegerField;
    CO_JOBS_LOCALSLocalName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass( TDM_CO_JOBS_LOCALS );

finalization
  UnregisterClass( TDM_CO_JOBS_LOCALS );

end.
