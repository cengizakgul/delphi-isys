// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_PIECE_WORK;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses;

type
  TDM_EE_PIECE_WORK = class(TDM_ddTable)
    DM_CLIENT: TDM_CLIENT;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EE_PIECE_WORK: TEE_PIECE_WORK;
    EE_PIECE_WORKCL_PIECES_NBR: TIntegerField;
    EE_PIECE_WORKEE_PIECE_WORK_NBR: TIntegerField;
    EE_PIECE_WORKEE_NBR: TIntegerField;
    EE_PIECE_WORKRATE_QUANTITY: TFloatField;
    EE_PIECE_WORKRATE_AMOUNT: TFloatField;
    EE_PIECE_WORKName: TStringField;

    procedure EE_PIECE_WORKCL_PIECES_NBRChange(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  evUtils;
{$R *.DFM}

//gdy

procedure TDM_EE_PIECE_WORK.EE_PIECE_WORKCL_PIECES_NBRChange(
  Sender: TField);
const
  requiredCondition = '';
var
  v: Variant;
  Q, A: TField;
begin
  if EE_PIECE_WORK.State = dsInsert then
  begin
    Q := EE_PIECE_WORK.FieldByName('RATE_QUANTITY');
    A := EE_PIECE_WORK.FieldByName('RATE_AMOUNT');
    if Q.IsNull or A.IsNull then
    begin
      if not DM_CLIENT.CL_PIECES.Active then
        DM_CLIENT.CL_PIECES.DataRequired(requiredCondition );
      if (DM_CLIENT.CL_PIECES.State = dsBrowse) and
          DSConditionsEqual( DM_CLIENT.CL_PIECES.RetrieveCondition, requiredCondition ) then
      begin
        v := DM_CLIENT.CL_PIECES.Lookup( 'CL_PIECES_NBR', EE_PIECE_WORK['CL_PIECES_NBR'], 'DEFAULT_QUANTITY;DEFAULT_RATE' );
        if VarIsArray(v) then
        begin
          if Q.IsNull then
            Q.Value := v[0];
          if A.IsNull then
            A.Value := v[1];
        end;
      end;
    end;
  end;
end;

initialization
  RegisterClass( TDM_EE_PIECE_WORK );

finalization
  UnregisterClass( TDM_EE_PIECE_WORK );

end.
