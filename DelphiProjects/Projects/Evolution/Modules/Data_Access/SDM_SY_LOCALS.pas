// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_LOCALS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, EvUtils, SFieldCodeValues, EvClientDataSet;

type
  TDM_SY_LOCALS = class(TDM_ddTable)
    SY_LOCALS: TSY_LOCALS;
    SY_LOCALSNAME: TStringField;
    SY_LOCALSLocalState: TStringField;
    SY_LOCALSLocalTypeDescription: TStringField;
    SY_LOCALSLOCAL_TYPE: TStringField;
    SY_LOCALSSY_LOCALS_NBR: TIntegerField;
    SY_LOCALSSY_STATES_NBR: TIntegerField;
    SY_LOCALSZIP_CODE: TStringField;
    SY_LOCALSLOCAL_MINIMUM_WAGE: TFloatField;
    SY_LOCALSSY_COUNTY_NBR: TIntegerField;
    SY_LOCALSLOCAL_TAX_IDENTIFIER: TStringField;
    SY_LOCALSAGENCY_NUMBER: TIntegerField;
    SY_LOCALSTAX_RATE: TFloatField;
    SY_LOCALSTAX_AMOUNT: TFloatField;
    SY_LOCALSTAX_MAXIMUM: TFloatField;
    SY_LOCALSWAGE_MAXIMUM: TFloatField;
    SY_LOCALSWEEKLY_TAX_CAP: TFloatField;
    SY_LOCALSMINIMUM_HOURS_WORKED: TFloatField;
    SY_LOCALSMINIMUM_HOURS_WORKED_PER: TStringField;
    SY_LOCALSCALCULATION_METHOD: TStringField;
    SY_LOCALSMISCELLANEOUS_AMOUNT: TFloatField;
    SY_LOCALSPRINT_RETURN_IF_ZERO: TStringField;
    SY_LOCALSUSE_MISC_TAX_RETURN_CODE: TStringField;
    SY_LOCALSSY_LOCAL_TAX_PMT_AGENCY_NBR: TIntegerField;
    SY_LOCALSSY_TAX_PMT_REPORT_NBR: TIntegerField;
    SY_LOCALSSY_LOCAL_REPORTING_AGENCY_NBR: TIntegerField;
    SY_LOCALSTAX_DEPOSIT_FOLLOW_STATE: TStringField;
    SY_LOCALSFILLER: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    SY_LOCALSLOCAL_ACTIVE: TStringField;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    SY_LOCALSCountyName: TStringField;
    SY_LOCALSROUND_TO_NEAREST_DOLLAR: TStringField;
    SY_LOCALSTAX_TYPE: TStringField;
    SY_LOCALSTAX_FREQUENCY: TStringField;
    SY_LOCALSW2_BOX: TStringField;
    SY_LOCALSPAY_WITH_STATE: TStringField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SY_LOCALSAgencyName: TStringField;
    SY_LOCALSLocalTypeDescriptionNew: TStringField;
    SY_LOCALSPSDCode: TStringField;
    procedure SY_LOCALSCalcFields(DataSet: TDataSet);
    procedure SY_LOCALSNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}

constructor TDM_SY_LOCALS.Create(AOwner: TComponent);
begin
  Tag := Integer(GetCurrentThreadId);
  inherited;
end;

procedure TDM_SY_LOCALS.SY_LOCALSCalcFields(DataSet: TDataSet);
begin
  SY_LOCALSLocalTypeDescription.AsString := SY_LOCALSNAME.AsString + SY_LOCALSLOCAL_TYPE.AsString + 'X';
  SY_LOCALSLocalTypeDescriptionNew.AsString :=
        ReturnDescription(LocalType_ComboChoices, SY_LOCALSLOCAL_TYPE.AsString);

  if DataSet.FieldByName('SY_STATES_NBR').AsInteger = 41 then
    DataSet.FieldByName('PSDCode').AsString := DataSet.FieldByName('LOCAL_TAX_IDENTIFIER').AsString;
    
end;

procedure TDM_SY_LOCALS.SY_LOCALSNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('SY_STATES_NBR').AsString :=
    DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString;
  DataSet.FieldByName('SY_COUNTY_NBR').AsString :=
    DM_SYSTEM_LOCAL.SY_COUNTY.FieldByName('SY_COUNTY_NBR').AsString;
end;

initialization
  RegisterClass(TDM_SY_LOCALS);

finalization
  UnregisterClass(TDM_SY_LOCALS);

end.
