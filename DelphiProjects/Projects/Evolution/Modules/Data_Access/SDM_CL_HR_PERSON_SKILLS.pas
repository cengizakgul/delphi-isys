// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_HR_PERSON_SKILLS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure;  

type
  TDM_CL_HR_PERSON_SKILLS = class(TDM_ddTable)
    CL_HR_PERSON_SKILLS: TCL_HR_PERSON_SKILLS;
    DM_CLIENT: TDM_CLIENT;
    CL_HR_PERSON_SKILLSCL_HR_PERSON_SKILLS_NBR: TIntegerField;
    CL_HR_PERSON_SKILLSCL_HR_SKILLS_NBR: TIntegerField;
    CL_HR_PERSON_SKILLSCL_PERSON_NBR: TIntegerField;
    CL_HR_PERSON_SKILLSNOTES: TBlobField;
    CL_HR_PERSON_SKILLSYEARS_EXPERIENCE: TFloatField;
    CL_HR_PERSON_SKILLSSkill: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass( TDM_CL_HR_PERSON_SKILLS );

finalization
  UnregisterClass( TDM_CL_HR_PERSON_SKILLS );

end.
