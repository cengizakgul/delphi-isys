// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_E_D_GROUP_CODES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet;

type
  TDM_CL_E_D_GROUP_CODES = class(TDM_ddTable)
    CL_E_D_GROUP_CODES: TCL_E_D_GROUP_CODES;
    CL_E_D_GROUP_CODESCL_E_D_GROUP_CODES_NBR: TIntegerField;
    CL_E_D_GROUP_CODESCL_E_DS_NBR: TIntegerField;
    CL_E_D_GROUP_CODESCL_E_D_GROUPS_NBR: TIntegerField;
    CL_E_D_GROUP_CODESCodeDescription: TStringField;
    CL_E_D_GROUP_CODESED_Code_Lookup: TStringField;
    CL_E_D_GROUP_CODESCustom_Code_lookup: TStringField;
    DM_CLIENT: TDM_CLIENT;
  private
  public
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CL_E_D_GROUP_CODES);

finalization
  UnregisterClass(TDM_CL_E_D_GROUP_CODES);

end.
