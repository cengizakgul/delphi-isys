inherited DM_CO_UNIONS: TDM_CO_UNIONS
  OldCreateOrder = False
  Left = 249
  Top = 191
  Height = 480
  Width = 696
  object DM_CLIENT: TDM_CLIENT
    Left = 136
    Top = 144
  end
  object CO_UNIONS: TCO_UNIONS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_UNIONS_PROV'
    ValidateWithMask = True
    Left = 136
    Top = 80
    object CO_UNIONSCO_UNIONS_NBR: TIntegerField
      FieldName = 'CO_UNIONS_NBR'
    end
    object CO_UNIONSCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_UNIONSCL_UNION_NBR: TIntegerField
      FieldName = 'CL_UNION_NBR'
    end
    object CO_UNIONSUnion_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Union_Name'
      LookupDataSet = DM_CL_UNION.CL_UNION
      LookupKeyFields = 'CL_UNION_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_UNION_NBR'
      Size = 40
      Lookup = True
    end
  end
end
