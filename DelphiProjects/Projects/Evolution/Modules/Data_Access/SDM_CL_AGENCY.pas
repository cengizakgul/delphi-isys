// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_AGENCY;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, EvConsts,
  SDataStructure, Db,   Variants, EvUtils, EvTypes,
  SDDClasses, SDataDictbureau, kbmMemTable, ISKbmMemDataSet, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, EvExceptions,
  EvClientDataSet;

type
  TDM_CL_AGENCY = class(TDM_ddTable)
    CL_AGENCY: TCL_AGENCY;
    CL_AGENCYAgency_Name: TStringField;
    CL_AGENCYAgencyAddress: TStringField;
    CL_AGENCYCL_AGENCY_NBR: TIntegerField;
    CL_AGENCYSB_AGENCY_NBR: TIntegerField;
    CL_AGENCYREPORT_METHOD: TStringField;
    CL_AGENCYFREQUENCY: TStringField;
    CL_AGENCYMONTH_NUMBER: TStringField;
    CL_AGENCYCL_BANK_ACCOUNT_NBR: TIntegerField;
    CL_AGENCYPAYMENT_TYPE: TStringField;
    CL_AGENCYCL_DELIVERY_GROUP_NBR: TIntegerField;
    CL_AGENCYCLIENT_AGENCY_CUSTOM_FIELD: TStringField;
    CL_AGENCYNOTES: TBlobField;
    CL_AGENCYFILLER: TStringField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    CL_AGENCYGL_TAG: TStringField;
    procedure CL_AGENCYCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure CL_AGENCYBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CL_AGENCY.CL_AGENCYCalcFields(DataSet: TDataSet);
begin
  with dm_service_bureau.SB_AGENCY do
  begin
    if Active
    and ShadowDataSet.CachedFindKey(CL_AGENCYSB_AGENCY_NBR.AsInteger) then
    begin
      CL_AGENCYAgencyAddress.AsString := ShadowDataSet.FieldByName('Address1').AsString+ ','+
        ShadowDataSet.FieldByName('City').AsString+ ','+
        ShadowDataSet.FieldByName('State').AsString+ ','+
        ShadowDataSet.FieldByName('Zip_Code').AsString;
    end
    else
      CL_AGENCYAgencyAddress.AsString := '';
  end;
end;

procedure TDM_CL_AGENCY.DataModuleCreate(Sender: TObject);
begin
  dm_service_bureau.SB_AGENCY;
end;

procedure TDM_CL_AGENCY.CL_AGENCYBeforePost(DataSet: TDataSet);
begin
  if DataSet['PAYMENT_TYPE'] = Payment_Type_DirectDeposit then
  begin
    dm_service_bureau.SB_AGENCY.Activate;
    Assert(dm_service_bureau.SB_AGENCY.ShadowDataSet.CachedFindKey(DataSet.FieldByName('SB_AGENCY_NBR').AsInteger));
    if dm_service_bureau.SB_AGENCY.ShadowDataSet.FieldByName('SB_BANKS_NBR').IsNull then
      raise EInconsistentData.Create('Direct deposit payment type can not be used for an agency with no bank attached');
  end;
end;

initialization
  RegisterClass(TDM_CL_AGENCY);

finalization
  UnregisterClass(TDM_CL_AGENCY);

end.
