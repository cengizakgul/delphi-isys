// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_CHECK_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses;

type
  TDM_PR_CHECK_LOCALS = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_CHECK_LOCALS: TPR_CHECK_LOCALS;
    PR_CHECK_LOCALSPR_CHECK_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSPR_CHECK_NBR: TIntegerField;
    PR_CHECK_LOCALSEE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSLOCAL_TAXABLE_WAGE: TFloatField;
    PR_CHECK_LOCALSLOCAL_TAX: TFloatField;
    PR_CHECK_LOCALSEXCLUDE_LOCAL: TStringField;
    PR_CHECK_LOCALSOVERRIDE_AMOUNT: TFloatField;
    PR_CHECK_LOCALSLOCAL_SHORTFALL: TFloatField;
    PR_CHECK_LOCALSLocal_Name_Lookup: TStringField;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    PR_CHECK_LOCALSPR_NBR: TIntegerField;
    PR_CHECK_LOCALSLOCAL_GROSS_WAGES: TFloatField;
    DM_COMPANY: TDM_COMPANY;
    PR_CHECK_LOCALSCO_LOCATIONS_NBR: TIntegerField;
    PR_CHECK_LOCALSLocation: TStringField;
    PR_CHECK_LOCALSNonRes: TStringField;
    PR_CHECK_LOCALSNONRES_EE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LOCALSREPORTABLE: TStringField;
    procedure PR_CHECK_LOCALSBeforeDelete(DataSet: TDataSet);
    procedure PR_CHECK_LOCALSBeforeEdit(DataSet: TDataSet);
    procedure PR_CHECK_LOCALSBeforeInsert(DataSet: TDataSet);
    procedure PR_CHECK_LOCALSBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_CHECK_LOCALS: TDM_PR_CHECK_LOCALS;

implementation

uses
  SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_CHECK_LOCALS.PR_CHECK_LOCALSBeforeDelete(
  DataSet: TDataSet);
begin
  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_CHECK_LOCALS.PR_CHECK_LOCALSBeforeEdit(DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_CHECK_LOCALS.PR_CHECK_LOCALSBeforeInsert(
  DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure TDM_PR_CHECK_LOCALS.PR_CHECK_LOCALSBeforePost(DataSet: TDataSet);
begin
  if (DataSet.State = dsInsert) and DataSet.FieldByName('PR_NBR').IsNull then
    DataSet.FieldByName('PR_NBR').AsInteger := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
end;

initialization
  RegisterClass(TDM_PR_CHECK_LOCALS);

finalization
  UnregisterClass(TDM_PR_CHECK_LOCALS);

end.
