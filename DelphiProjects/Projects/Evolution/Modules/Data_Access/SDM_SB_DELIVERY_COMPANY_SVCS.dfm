inherited DM_SB_DELIVERY_COMPANY_SVCS: TDM_SB_DELIVERY_COMPANY_SVCS
  OldCreateOrder = False
  Left = 376
  Top = 181
  Height = 406
  Width = 512
  object SB_DELIVERY_COMPANY_SVCS: TSB_DELIVERY_COMPANY_SVCS
    Aggregates = <>
    Params = <>
    ProviderName = 'SB_DELIVERY_COMPANY_SVCS_PROV'
    OnCalcFields = SB_DELIVERY_COMPANY_SVCSCalcFields
    PictureMasks.Strings = (
      
        'REFERENCE_FEE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F')
    ValidateWithMask = True
    Left = 104
    Top = 56
    object SB_DELIVERY_COMPANY_SVCSSB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_COMPANY_SVCS_NBR'
    end
    object SB_DELIVERY_COMPANY_SVCSSB_DELIVERY_COMPANY_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_COMPANY_NBR'
    end
    object SB_DELIVERY_COMPANY_SVCSDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object SB_DELIVERY_COMPANY_SVCSREFERENCE_FEE: TFloatField
      FieldName = 'REFERENCE_FEE'
    end
    object SB_DELIVERY_COMPANY_SVCSDeliveryCompanyName: TStringField
      FieldKind = fkLookup
      FieldName = 'DeliveryCompanyName'
      LookupDataSet = DM_SB_DELIVERY_COMPANY.SB_DELIVERY_COMPANY
      LookupKeyFields = 'SB_DELIVERY_COMPANY_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SB_DELIVERY_COMPANY_NBR'
      Lookup = True
    end
    object SB_DELIVERY_COMPANY_SVCSNameAndService: TStringField
      DisplayWidth = 40
      FieldKind = fkCalculated
      FieldName = 'NameAndService'
      Size = 40
      Calculated = True
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 104
    Top = 144
  end
end
