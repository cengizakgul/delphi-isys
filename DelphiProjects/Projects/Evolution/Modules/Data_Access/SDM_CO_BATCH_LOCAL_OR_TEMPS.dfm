inherited DM_CO_BATCH_LOCAL_OR_TEMPS: TDM_CO_BATCH_LOCAL_OR_TEMPS
  OldCreateOrder = False
  Left = 253
  Top = 107
  Height = 480
  Width = 696
  object DM_COMPANY: TDM_COMPANY
    Left = 192
    Top = 80
  end
  object CO_BATCH_LOCAL_OR_TEMPS: TCO_BATCH_LOCAL_OR_TEMPS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_BATCH_LOCAL_OR_TEMPS_PROV'
    PictureMasks.Strings = (
      
        'OVERRIDE_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F')
    ValidateWithMask = True
    Left = 192
    Top = 136
    object CO_BATCH_LOCAL_OR_TEMPSCO_BATCH_LOCAL_OR_TEMPS_NBR: TIntegerField
      FieldName = 'CO_BATCH_LOCAL_OR_TEMPS_NBR'
    end
    object CO_BATCH_LOCAL_OR_TEMPSCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object CO_BATCH_LOCAL_OR_TEMPSOVERRIDE_AMOUNT: TFloatField
      FieldName = 'OVERRIDE_AMOUNT'
    end
    object CO_BATCH_LOCAL_OR_TEMPSEXCLUDE_LOCAL: TStringField
      FieldName = 'EXCLUDE_LOCAL'
      Size = 1
    end
    object CO_BATCH_LOCAL_OR_TEMPSCO_PR_CHECK_TEMPLATES_NBR: TIntegerField
      FieldName = 'CO_PR_CHECK_TEMPLATES_NBR'
    end
    object CO_BATCH_LOCAL_OR_TEMPSLocalName: TStringField
      FieldKind = fkLookup
      FieldName = 'LocalName'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Lookup = True
    end
  end
end
