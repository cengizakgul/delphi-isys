// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_REPORT_WRITER_REPORTS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_CL_REPORT_WRITER_REPORTS = class(TDM_ddTable)
    CL_REPORT_WRITER_REPORTS: TCL_REPORT_WRITER_REPORTS;
    CL_REPORT_WRITER_REPORTSSB_REPORT_WRITER_REPORTS_NBR: TIntegerField;
    CL_REPORT_WRITER_REPORTSREPORT_DESCRIPTION: TStringField;
    CL_REPORT_WRITER_REPORTSREPORT_TYPE: TStringField;
    CL_REPORT_WRITER_REPORTSREPORT_FILE: TBlobField;
    CL_REPORT_WRITER_REPORTSNOTES: TBlobField;
    CL_REPORT_WRITER_REPORTSRWDescription: TStringField;
    CL_REPORT_WRITER_REPORTSMEDIA_TYPE: TStringField;
    CL_REPORT_WRITER_REPORTSCLASS_NAME: TStringField;
    CL_REPORT_WRITER_REPORTSANCESTOR_CLASS_NAME: TStringField;
    procedure CL_REPORT_WRITER_REPORTSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CL_REPORT_WRITER_REPORTS: TDM_CL_REPORT_WRITER_REPORTS;

implementation

{$R *.DFM}

procedure TDM_CL_REPORT_WRITER_REPORTS.CL_REPORT_WRITER_REPORTSCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  CL_REPORT_WRITER_REPORTSRWDescription.AsString := CL_REPORT_WRITER_REPORTSNOTES.AsString;
end;

initialization
  RegisterClass(TDM_CL_REPORT_WRITER_REPORTS);

finalization
  UnregisterClass(TDM_CL_REPORT_WRITER_REPORTS);

end.
