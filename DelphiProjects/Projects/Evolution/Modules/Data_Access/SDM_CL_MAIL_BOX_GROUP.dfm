inherited DM_CL_MAIL_BOX_GROUP: TDM_CL_MAIL_BOX_GROUP
  Left = 514
  Top = 233
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 152
    Top = 24
  end
  object CL_MAIL_BOX_GROUP: TCL_MAIL_BOX_GROUP
    ProviderName = 'CL_MAIL_BOX_GROUP_PROV'
    Left = 40
    Top = 16
    object CL_MAIL_BOX_GROUPCL_MAIL_BOX_GROUP_NBR: TIntegerField
      FieldName = 'CL_MAIL_BOX_GROUP_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.CL_MAIL_BOX_GROUP_NBR'
      Required = True
    end
    object CL_MAIL_BOX_GROUPDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.DESCRIPTION'
      Required = True
      Size = 40
    end
    object CL_MAIL_BOX_GROUPREQUIRED: TStringField
      FieldName = 'REQUIRED'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.REQUIRED'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_MAIL_BOX_GROUPNOTIFICATION_EMAIL: TStringField
      FieldName = 'NOTIFICATION_EMAIL'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.NOTIFICATION_EMAIL'
      Size = 80
    end
    object CL_MAIL_BOX_GROUPSB_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SB_COVER_LETTER_REPORT_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.SB_COVER_LETTER_REPORT_NBR'
    end
    object CL_MAIL_BOX_GROUPSY_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SY_COVER_LETTER_REPORT_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.SY_COVER_LETTER_REPORT_NBR'
    end
    object CL_MAIL_BOX_GROUPSEC_METHOD_ACTIVE_FROM: TDateField
      FieldName = 'SEC_METHOD_ACTIVE_FROM'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.SEC_METHOD_ACTIVE_FROM'
    end
    object CL_MAIL_BOX_GROUPSEC_METHOD_ACTIVE_TO: TDateField
      FieldName = 'SEC_METHOD_ACTIVE_TO'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.SEC_METHOD_ACTIVE_TO'
    end
    object CL_MAIL_BOX_GROUPPRIM_SB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'PRIM_SB_DELIVERY_METHOD_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.PRIM_SB_DELIVERY_METHOD_NBR'
    end
    object CL_MAIL_BOX_GROUPSEC_SB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SEC_SB_DELIVERY_METHOD_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.SEC_SB_DELIVERY_METHOD_NBR'
    end
    object CL_MAIL_BOX_GROUPPRIM_SB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'PRIM_SB_MEDIA_TYPE_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.PRIM_SB_MEDIA_TYPE_NBR'
    end
    object CL_MAIL_BOX_GROUPSEC_SB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SEC_SB_MEDIA_TYPE_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.SEC_SB_MEDIA_TYPE_NBR'
    end
    object CL_MAIL_BOX_GROUPUP_LEVEL_MAIL_BOX_GROUP_NBR: TIntegerField
      FieldName = 'UP_LEVEL_MAIL_BOX_GROUP_NBR'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.UP_LEVEL_MAIL_BOX_GROUP_NBR'
    end
    object CL_MAIL_BOX_GROUPAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      Origin = 'CL_MAIL_BOX_GROUP_HIST.AUTO_RELEASE_TYPE'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_MAIL_BOX_GROUPluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'PRIM_SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object CL_MAIL_BOX_GROUPluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'PRIM_SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object CL_MAIL_BOX_GROUPluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'PRIM_SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object CL_MAIL_BOX_GROUPNOTE: TStringField
      FieldName = 'NOTE'
      Size = 512
    end
  end
end
