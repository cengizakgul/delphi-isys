unit SDM_CO_VENDOR;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses;

type
  TDM_CO_VENDOR = class(TDM_ddTable)
    DM_CLIENT: TDM_CLIENT;
    CO_VENDOR: TCO_VENDOR;
    CO_VENDORSB_VENDOR_NBR: TIntegerField;
    CO_VENDORVENDOR_NAME: TStringField;
    CO_VENDORCO_VENDOR_NBR: TIntegerField;
    CO_VENDORCO_NBR: TIntegerField;
    procedure CO_VENDORCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_VENDOR: TDM_CO_VENDOR;

implementation

{$R *.DFM}

procedure TDM_CO_VENDOR.CO_VENDORCalcFields(DataSet: TDataSet);
begin
  inherited;

  if not DM_SERVICE_BUREAU.SB_VENDOR.Active then
     DM_SERVICE_BUREAU.SB_VENDOR.DataRequired('ALL');

  if CO_VENDORSB_VENDOR_NBR.AsInteger > 0  then
     CO_VENDORVENDOR_NAME.AsString :=
          DM_SERVICE_BUREAU.SB_VENDOR.ShadowDataSet.CachedLookup(CO_VENDORSB_VENDOR_NBR.AsInteger, 'NAME');
end;

initialization
  RegisterClass(TDM_CO_VENDOR);

finalization
  UnregisterClass(TDM_CO_VENDOR);

end.
