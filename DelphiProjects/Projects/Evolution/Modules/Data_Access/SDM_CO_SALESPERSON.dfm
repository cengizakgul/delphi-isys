inherited DM_CO_SALESPERSON: TDM_CO_SALESPERSON
  OldCreateOrder = False
  Left = 193
  Top = 136
  Height = 480
  Width = 696
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 192
    Top = 104
  end
  object CO_SALESPERSON: TCO_SALESPERSON
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_SALESPERSON_PROV'
    PictureMasks.Strings = (
      
        'CHARGEBACK'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]' +
        ']}'#9'T'#9'F'
      
        'COMMISSION_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_COMMISSION_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PROJECTED_ANNUAL_COMMISSION'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F')
    ValidateWithMask = True
    Left = 192
    Top = 168
    object CO_SALESPERSONCO_SALESPERSON_NBR: TIntegerField
      FieldName = 'CO_SALESPERSON_NBR'
    end
    object CO_SALESPERSONCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_SALESPERSONSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
    end
    object CO_SALESPERSONCOMMISSION_PERCENTAGE: TFloatField
      FieldName = 'COMMISSION_PERCENTAGE'
    end
    object CO_SALESPERSONNEXT_COMMISSION_PERCENTAGE: TFloatField
      FieldName = 'NEXT_COMMISSION_PERCENTAGE'
    end
    object CO_SALESPERSONNEXT_COMMISSION_PERCENT_DATE: TDateTimeField
      FieldName = 'NEXT_COMMISSION_PERCENT_DATE'
    end
    object CO_SALESPERSONLAST_VISIT_DATE: TDateField
      FieldName = 'LAST_VISIT_DATE'
    end
    object CO_SALESPERSONTYPE: TStringField
      FieldName = 'SALESPERSON_TYPE'
      Size = 1
    end
    object CO_SALESPERSONCHARGEBACK: TFloatField
      FieldName = 'CHARGEBACK'
    end
    object CO_SALESPERSONPROJECTED_ANNUAL_COMMISSION: TFloatField
      FieldName = 'PROJECTED_ANNUAL_COMMISSION'
    end
    object CO_SALESPERSONNOTES: TBlobField
      FieldName = 'NOTES'
      BlobType = ftBlob
      Size = 8
    end
    object CO_SALESPERSONName: TStringField
      FieldKind = fkLookup
      FieldName = 'Name'
      LookupDataSet = DM_SB_USER.SB_USER
      LookupKeyFields = 'SB_USER_NBR'
      LookupResultField = 'LAST_NAME'
      KeyFields = 'SB_USER_NBR'
      Lookup = True
    end
  end
end
