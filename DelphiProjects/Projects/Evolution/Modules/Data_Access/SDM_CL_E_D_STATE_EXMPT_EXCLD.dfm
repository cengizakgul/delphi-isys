inherited DM_CL_E_D_STATE_EXMPT_EXCLD: TDM_CL_E_D_STATE_EXMPT_EXCLD
  OldCreateOrder = False
  Left = 97
  Top = 363
  Height = 267
  Width = 509
  object CL_E_D_STATE_EXMPT_EXCLD: TCL_E_D_STATE_EXMPT_EXCLD
    Aggregates = <>
    Params = <>
    ProviderName = 'CL_E_D_STATE_EXMPT_EXCLD_PROV'
    ValidateWithMask = True
    Left = 81
    Top = 26
    object CL_E_D_STATE_EXMPT_EXCLDCL_E_D_STATE_EXMPT_EXCLD_NBR: TIntegerField
      FieldName = 'CL_E_D_STATE_EXMPT_EXCLD_NBR'
    end
    object CL_E_D_STATE_EXMPT_EXCLDCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object CL_E_D_STATE_EXMPT_EXCLDSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATE: TStringField
      FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_STATE'
      OnValidate = CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate
      Size = 1
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_SDI: TStringField
      FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_SDI'
      OnValidate = CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate
      Size = 1
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_SUI: TStringField
      FieldName = 'EMPLOYEE_EXEMPT_EXCLUDE_SUI'
      OnValidate = CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate
      Size = 1
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_STATE_OR_VALUE: TFloatField
      FieldName = 'EMPLOYEE_STATE_OR_VALUE'
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_STATE_OR_TYPE: TStringField
      FieldName = 'EMPLOYEE_STATE_OR_TYPE'
      Size = 1
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYER_EXEMPT_EXCLUDE_SDI: TStringField
      FieldName = 'EMPLOYER_EXEMPT_EXCLUDE_SDI'
      OnValidate = CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate
      Size = 1
    end
    object CL_E_D_STATE_EXMPT_EXCLDEMPLOYER_EXEMPT_EXCLUDE_SUI: TStringField
      FieldName = 'EMPLOYER_EXEMPT_EXCLUDE_SUI'
      OnValidate = CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate
      Size = 1
    end
    object CL_E_D_STATE_EXMPT_EXCLDState_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'State_Lookup'
      LookupDataSet = DM_SY_STATES.SY_STATES
      LookupKeyFields = 'SY_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'SY_STATES_NBR'
      Size = 2
      Lookup = True
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 78
    Top = 87
  end
end
