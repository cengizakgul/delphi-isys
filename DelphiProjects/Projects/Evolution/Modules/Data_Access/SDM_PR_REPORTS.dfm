inherited DM_PR_REPORTS: TDM_PR_REPORTS
  OldCreateOrder = False
  Left = 324
  Top = 106
  Height = 540
  Width = 783
  object PR_REPORTS: TPR_REPORTS
    Aggregates = <>
    Params = <>
    ProviderName = 'PR_REPORTS_PROV'
    ValidateWithMask = True
    Left = 56
    Top = 48
    object PR_REPORTSPR_REPORTS_NBR: TIntegerField
      FieldName = 'PR_REPORTS_NBR'
    end
    object PR_REPORTSPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_REPORTSCO_REPORTS_NBR: TIntegerField
      FieldName = 'CO_REPORTS_NBR'
    end
    object PR_REPORTSReportName: TStringField
      FieldKind = fkLookup
      FieldName = 'ReportName'
      LookupDataSet = DM_CO_REPORTS.CO_REPORTS
      LookupKeyFields = 'CO_REPORTS_NBR'
      LookupResultField = 'CUSTOM_NAME'
      KeyFields = 'CO_REPORTS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 152
    Top = 48
  end
end
