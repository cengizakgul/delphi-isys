// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_SUI;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SY_SUI = class(TDM_ddTable)
    SY_SUI: TSY_SUI;
    SY_SUINEW_COMPANY_DEFAULT_RATE: TFloatField;
    SY_SUIGLOBAL_RATE: TFloatField;
    SY_SUIFUTURE_DEFAULT_RATE: TFloatField;
    SY_SUIMAXIMUM_WAGE: TFloatField;
    SY_SUIFUTURE_MAXIMUM_WAGE: TFloatField;
    SY_SUIALTERNATE_TAXABLE_WAGE_BASE: TFloatField;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_SUI);

finalization
  UnregisterClass(TDM_SY_SUI);

end.
 
