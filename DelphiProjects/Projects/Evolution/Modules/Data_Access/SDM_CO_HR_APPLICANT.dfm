inherited DM_CO_HR_APPLICANT: TDM_CO_HR_APPLICANT
  OldCreateOrder = True
  Left = 318
  Top = 201
  Height = 211
  Width = 369
  object CO_HR_APPLICANT: TCO_HR_APPLICANT
    ProviderName = 'CO_HR_APPLICANT_PROV'
    FieldDefs = <
      item
        Name = 'APPLICANT_CONTACTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'APPLICANT_CONTACTED_BY'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'APPLICANT_CONTACTED_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'APPLICANT_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'APPLICATION_DATE'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'CL_PERSON_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_BRANCH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DEPARTMENT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DIVISION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_APPLICANT_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_CAR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_RECRUITERS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_REFERRALS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_JOBS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_TEAM_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DATE_POSITION_ACCEPTED'
        DataType = ftDateTime
      end
      item
        Name = 'DATE_POSITION_OFFERED'
        DataType = ftDateTime
      end
      item
        Name = 'DESIRED_RATE'
        DataType = ftFloat
      end
      item
        Name = 'DESIRED_REGULAR_OR_TEMPORARY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DESIRED_SALARY'
        DataType = ftFloat
      end
      item
        Name = 'DESIRED_SHIFT'
        DataType = ftInteger
      end
      item
        Name = 'GED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GED_DATE'
        DataType = ftDate
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'POSITION_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SECURITY_CLEARANCE'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'START_DATE'
        DataType = ftDate
      end
      item
        Name = 'FirstName'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'LastName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'SSNLookup'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'APPLICANT_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_HR_SUPERVISORS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'Division_Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Team_Name'
        DataType = ftString
        Size = 20
      end>
    OnCalcFields = CO_HR_APPLICANTCalcFields
    OnNewRecord = CO_HR_APPLICANTNewRecord
    OnPrepareLookups = CO_HR_APPLICANTPrepareLookups
    OnAddLookups = CO_HR_APPLICANTAddLookups
    OnUnPrepareLookups = CO_HR_APPLICANTUnPrepareLookups
    Left = 64
    Top = 72
    object CO_HR_APPLICANTAPPLICANT_CONTACTED: TStringField
      FieldName = 'APPLICANT_CONTACTED'
      Origin = '"CO_HR_APPLICANT_HIST"."APPLICANT_CONTACTED"'
      Size = 1
    end
    object CO_HR_APPLICANTAPPLICANT_CONTACTED_BY: TStringField
      FieldName = 'APPLICANT_CONTACTED_BY'
      Origin = '"CO_HR_APPLICANT_HIST"."APPLICANT_CONTACTED_BY"'
      Size = 10
    end
    object CO_HR_APPLICANTAPPLICANT_CONTACTED_DATE: TDateTimeField
      FieldName = 'APPLICANT_CONTACTED_DATE'
      Origin = '"CO_HR_APPLICANT_HIST"."APPLICANT_CONTACTED_DATE"'
    end
    object CO_HR_APPLICANTAPPLICANT_STATUS: TStringField
      FieldName = 'APPLICANT_STATUS'
      Origin = '"CO_HR_APPLICANT_HIST"."APPLICANT_STATUS"'
      Size = 1
    end
    object CO_HR_APPLICANTName_Calculate: TStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'Name_Calculate'
      Size = 50
      Calculated = True
    end
    object CO_HR_APPLICANTE_MAIL_Calc: TStringField
      FieldKind = fkCalculated
      FieldName = 'E_MAIL_Calc'
      Size = 40
      Calculated = True
    end
    object CO_HR_APPLICANTAPPLICATION_DATE: TDateField
      FieldName = 'APPLICATION_DATE'
      Origin = '"CO_HR_APPLICANT_HIST"."APPLICATION_DATE"'
      Required = True
    end
    object CO_HR_APPLICANTCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CL_PERSON_NBR"'
      Required = True
    end
    object CO_HR_APPLICANTCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_BRANCH_NBR"'
    end
    object CO_HR_APPLICANTCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_DEPARTMENT_NBR"'
    end
    object CO_HR_APPLICANTCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_DIVISION_NBR"'
    end
    object CO_HR_APPLICANTCO_HR_APPLICANT_NBR: TIntegerField
      FieldName = 'CO_HR_APPLICANT_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_HR_APPLICANT_NBR"'
      Required = True
    end
    object CO_HR_APPLICANTCO_HR_CAR_NBR: TIntegerField
      FieldName = 'CO_HR_CAR_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_HR_CAR_NBR"'
    end
    object CO_HR_APPLICANTCO_HR_RECRUITERS_NBR: TIntegerField
      FieldName = 'CO_HR_RECRUITERS_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_HR_RECRUITERS_NBR"'
    end
    object CO_HR_APPLICANTCO_HR_REFERRALS_NBR: TIntegerField
      FieldName = 'CO_HR_REFERRALS_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_HR_REFERRALS_NBR"'
    end
    object CO_HR_APPLICANTCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_JOBS_NBR"'
    end
    object CO_HR_APPLICANTCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_NBR"'
      Required = True
    end
    object CO_HR_APPLICANTCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_TEAM_NBR"'
    end
    object CO_HR_APPLICANTDATE_POSITION_ACCEPTED: TDateTimeField
      FieldName = 'DATE_POSITION_ACCEPTED'
      Origin = '"CO_HR_APPLICANT_HIST"."DATE_POSITION_ACCEPTED"'
    end
    object CO_HR_APPLICANTDATE_POSITION_OFFERED: TDateTimeField
      FieldName = 'DATE_POSITION_OFFERED'
      Origin = '"CO_HR_APPLICANT_HIST"."DATE_POSITION_OFFERED"'
    end
    object CO_HR_APPLICANTDESIRED_RATE: TFloatField
      FieldName = 'DESIRED_RATE'
      Origin = '"CO_HR_APPLICANT_HIST"."DESIRED_RATE"'
      DisplayFormat = '#,##0.00'
    end
    object CO_HR_APPLICANTDESIRED_REGULAR_OR_TEMPORARY: TStringField
      FieldName = 'DESIRED_REGULAR_OR_TEMPORARY'
      Origin = '"CO_HR_APPLICANT_HIST"."DESIRED_REGULAR_OR_TEMPORARY"'
      Size = 1
    end
    object CO_HR_APPLICANTDESIRED_SALARY: TFloatField
      FieldName = 'DESIRED_SALARY'
      Origin = '"CO_HR_APPLICANT_HIST"."DESIRED_SALARY"'
      DisplayFormat = '#,##0.00'
    end
    object CO_HR_APPLICANTDESIRED_SHIFT: TIntegerField
      FieldName = 'DESIRED_SHIFT'
      Origin = '"CO_HR_APPLICANT_HIST"."DESIRED_SHIFT"'
    end
    object CO_HR_APPLICANTGED: TStringField
      FieldName = 'GED'
      Origin = '"CO_HR_APPLICANT_HIST"."GED"'
      Size = 1
    end
    object CO_HR_APPLICANTGED_DATE: TDateField
      FieldName = 'GED_DATE'
      Origin = '"CO_HR_APPLICANT_HIST"."GED_DATE"'
    end
    object CO_HR_APPLICANTNOTES: TBlobField
      FieldName = 'NOTES'
      Origin = '"CO_HR_APPLICANT_HIST"."NOTES"'
      Size = 8
    end
    object CO_HR_APPLICANTPOSITION_STATUS: TStringField
      FieldName = 'POSITION_STATUS'
      Origin = '"CO_HR_APPLICANT_HIST"."POSITION_STATUS"'
      Size = 1
    end
    object CO_HR_APPLICANTSECURITY_CLEARANCE: TStringField
      FieldName = 'SECURITY_CLEARANCE'
      Origin = '"CO_HR_APPLICANT_HIST"."SECURITY_CLEARANCE"'
    end
    object CO_HR_APPLICANTSTART_DATE: TDateField
      FieldName = 'START_DATE'
      Origin = '"CO_HR_APPLICANT_HIST"."START_DATE"'
    end
    object CO_HR_APPLICANTFirstName: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'FirstName'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'FIRST_NAME'
      KeyFields = 'CL_PERSON_NBR'
    end
    object CO_HR_APPLICANTLastName: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'LastName'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'LAST_NAME'
      KeyFields = 'CL_PERSON_NBR'
      Size = 30
    end
    object CO_HR_APPLICANTSSNLookup: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SSNLookup'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'SOCIAL_SECURITY_NUMBER'
      KeyFields = 'CL_PERSON_NBR'
      Size = 11
    end
    object CO_HR_APPLICANTAPPLICANT_TYPE: TStringField
      FieldName = 'APPLICANT_TYPE'
      Origin = '"CO_HR_APPLICANT_HIST"."APPLICANT_TYPE"'
      Required = True
      Size = 1
    end
    object CO_HR_APPLICANTCO_HR_SUPERVISORS_NBR: TIntegerField
      FieldName = 'CO_HR_SUPERVISORS_NBR'
      Origin = '"CO_HR_APPLICANT_HIST"."CO_HR_SUPERVISORS_NBR"'
    end
    object CO_HR_APPLICANTBranch_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Branch_Name'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_BRANCH_NBR'
      Lookup = True
    end
    object CO_HR_APPLICANTDepartment_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Department_Name'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Lookup = True
    end
    object CO_HR_APPLICANTDivision_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Division_Name'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DIVISION_NBR'
      Lookup = True
    end
    object CO_HR_APPLICANTTeam_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'Team_Name'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_TEAM_NBR'
      Lookup = True
    end
    object CO_HR_APPLICANTJobs_Nbr: TStringField
      FieldKind = fkLookup
      FieldName = 'Jobs_Desc'
      LookupDataSet = DM_CO_JOBS.CO_JOBS
      LookupKeyFields = 'CO_JOBS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_JOBS_NBR'
      Lookup = True
    end
    object CO_HR_APPLICANTPosition: TStringField
      FieldKind = fkLookup
      FieldName = 'Position'
      LookupDataSet = DM_CO_HR_POSITIONS.CO_HR_POSITIONS
      LookupKeyFields = 'CO_HR_POSITIONS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_HR_POSITIONS_NBR'
      Lookup = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 264
    Top = 104
  end
end
