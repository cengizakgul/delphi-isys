// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_REPORT_WRITER_REPORTS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SB_REPORT_WRITER_REPORTS = class(TDM_ddTable)
    SB_REPORT_WRITER_REPORTS: TSB_REPORT_WRITER_REPORTS;
    SB_REPORT_WRITER_REPORTSSB_REPORT_WRITER_REPORTS_NBR: TIntegerField;
    SB_REPORT_WRITER_REPORTSREPORT_DESCRIPTION: TStringField;
    SB_REPORT_WRITER_REPORTSREPORT_TYPE: TStringField;
    SB_REPORT_WRITER_REPORTSREPORT_FILE: TBlobField;
    SB_REPORT_WRITER_REPORTSNOTES: TBlobField;
    SB_REPORT_WRITER_REPORTSRWDescription: TStringField;
    SB_REPORT_WRITER_REPORTSMEDIA_TYPE: TStringField;
    SB_REPORT_WRITER_REPORTSCLASS_NAME: TStringField;
    SB_REPORT_WRITER_REPORTSANCESTOR_CLASS_NAME: TStringField;
    procedure SB_REPORT_WRITER_REPORTSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_REPORT_WRITER_REPORTS: TDM_SB_REPORT_WRITER_REPORTS;

implementation

{$R *.DFM}

procedure TDM_SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTSCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  SB_REPORT_WRITER_REPORTSRWDescription.AsString := SB_REPORT_WRITER_REPORTSNOTES.AsString;
end;

initialization
  RegisterClass(TDM_SB_REPORT_WRITER_REPORTS);

finalization
  UnregisterClass(TDM_SB_REPORT_WRITER_REPORTS);

end.
