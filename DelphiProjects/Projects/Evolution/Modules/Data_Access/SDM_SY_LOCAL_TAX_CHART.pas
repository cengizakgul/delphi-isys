// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_LOCAL_TAX_CHART;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SDataStructure,  Db, Classes,  Forms, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents;

type
  TDM_SY_LOCAL_TAX_CHART = class(TDM_ddTable)
    SY_LOCAL_TAX_CHART: TSY_LOCAL_TAX_CHART;
    SY_LOCAL_TAX_CHARTSY_LOCAL_TAX_CHART_NBR: TIntegerField;
    SY_LOCAL_TAX_CHARTSY_LOCALS_NBR: TIntegerField;
    SY_LOCAL_TAX_CHARTSY_LOCAL_MARITAL_STATUS_NBR: TIntegerField;
    SY_LOCAL_TAX_CHARTMINIMUM: TFloatField;
    SY_LOCAL_TAX_CHARTMAXIMUM: TFloatField;
    SY_LOCAL_TAX_CHARTPERCENTAGE: TFloatField;
    SY_LOCAL_TAX_CHARTType: TStringField;
    SY_LOCAL_TAX_CHARTSY_STATE_MARITAL_STATUS_NBR: TIntegerField;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    SY_LOCAL_TAX_CHARTMARITAL_STATUS_DESCRPTION: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_LOCAL_TAX_CHART);

finalization
  UnregisterClass(TDM_SY_LOCAL_TAX_CHART);

end.
