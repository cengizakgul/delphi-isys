inherited DM_CO_LOCAL_TAX: TDM_CO_LOCAL_TAX
  OldCreateOrder = True
  Left = 568
  Top = 196
  Height = 267
  Width = 440
  object CO_LOCAL_TAX: TCO_LOCAL_TAX
    ProviderName = 'CO_LOCAL_TAX_PROV'
    PictureMasks.Strings = (
      
        'MISCELLANEOUS_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F')
    FieldDefs = <
      item
        Name = 'CO_LOCAL_TAX_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'SY_LOCALS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EXEMPT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LOCAL_EIN_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TAX_RETURN_CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'TAX_RATE'
        DataType = ftFloat
      end
      item
        Name = 'TAX_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'SY_LOCAL_DEPOSIT_FREQ_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'MISCELLANEOUS_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'PAYMENT_METHOD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FINAL_TAX_RETURN'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EFT_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'EFT_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EFT_PIN_NUMBER'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'LOCAL_ACTIVE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SELF_ADJUST_TAX'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AUTOCREATE_ON_NEW_HIRE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_STATES_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'APPLIED_FOR'
        DataType = ftString
        Size = 1
      end>
    AfterInsert = CO_LOCAL_TAXAfterInsert
    BeforePost = CO_LOCAL_TAXBeforePost
    OnCalcFields = CO_LOCAL_TAXCalcFields
    Left = 64
    Top = 32
    object CO_LOCAL_TAXLocalName: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'LocalName'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_LOCAL_TAXlocalState: TStringField
      DisplayLabel = 'LocalState'
      DisplayWidth = 2
      FieldKind = fkLookup
      FieldName = 'localState'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'LocalState'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 2
      Lookup = True
    end
    object CO_LOCAL_TAXCO_LOCAL_TAX_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_LOCAL_TAX_NBR'
      Origin = 'CO_LOCAL_TAX_HIST.CO_LOCAL_TAX_NBR'
      Required = True
      Visible = False
    end
    object CO_LOCAL_TAXCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = 'CO_LOCAL_TAX_HIST.CO_NBR'
      Required = True
      Visible = False
    end
    object CO_LOCAL_TAXSY_LOCALS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_LOCALS_NBR'
      Origin = 'CO_LOCAL_TAX_HIST.SY_LOCALS_NBR'
      Required = True
      Visible = False
    end
    object CO_LOCAL_TAXEXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT'
      Origin = 'CO_LOCAL_TAX_HIST.EXEMPT'
      Required = True
      Visible = False
      OnValidate = CO_LOCAL_TAXEXEMPTValidate
      Size = 1
    end
    object CO_LOCAL_TAXLOCAL_EIN_NUMBER: TStringField
      DisplayWidth = 15
      FieldName = 'LOCAL_EIN_NUMBER'
      Origin = 'CO_LOCAL_TAX_HIST.LOCAL_EIN_NUMBER'
      Required = True
      Visible = False
      OnValidate = CO_LOCAL_TAXEXEMPTValidate
      Size = 15
    end
    object CO_LOCAL_TAXTAX_RETURN_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_CODE'
      Origin = 'CO_LOCAL_TAX_HIST.TAX_RETURN_CODE'
      Visible = False
      Size = 10
    end
    object CO_LOCAL_TAXTAX_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'TAX_RATE'
      Origin = 'CO_LOCAL_TAX_HIST.TAX_RATE'
      Visible = False
      DisplayFormat = '#,##0.00######'
    end
    object CO_LOCAL_TAXTAX_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'TAX_AMOUNT'
      Origin = 'CO_LOCAL_TAX_HIST.TAX_AMOUNT'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object CO_LOCAL_TAXSY_LOCAL_DEPOSIT_FREQ_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_LOCAL_DEPOSIT_FREQ_NBR'
      Origin = 'CO_LOCAL_TAX_HIST.SY_LOCAL_DEPOSIT_FREQ_NBR'
      Required = True
      Visible = False
    end
    object CO_LOCAL_TAXMISCELLANEOUS_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'MISCELLANEOUS_AMOUNT'
      Origin = 'CO_LOCAL_TAX_HIST.MISCELLANEOUS_AMOUNT'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object CO_LOCAL_TAXPAYMENT_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'PAYMENT_METHOD'
      Origin = 'CO_LOCAL_TAX_HIST.PAYMENT_METHOD'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_LOCAL_TAXFINAL_TAX_RETURN: TStringField
      DisplayWidth = 1
      FieldName = 'FINAL_TAX_RETURN'
      Origin = 'CO_LOCAL_TAX_HIST.FINAL_TAX_RETURN'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_LOCAL_TAXEFT_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'EFT_NAME'
      Origin = 'CO_LOCAL_TAX_HIST.EFT_NAME'
      Visible = False
      Size = 40
    end
    object CO_LOCAL_TAXEFT_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'EFT_STATUS'
      Origin = 'CO_LOCAL_TAX_HIST.EFT_STATUS'
      Visible = False
      Size = 1
    end
    object CO_LOCAL_TAXFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Origin = 'CO_LOCAL_TAX_HIST.FILLER'
      Visible = False
      Size = 512
    end
    object CO_LOCAL_TAXGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = 'CO_LOCAL_TAX_HIST.GENERAL_LEDGER_TAG'
      Visible = False
    end
    object CO_LOCAL_TAXEFT_PIN_NUMBER: TStringField
      DisplayWidth = 10
      FieldName = 'EFT_PIN_NUMBER'
      Origin = 'CO_LOCAL_TAX_HIST.EFT_PIN_NUMBER'
      Visible = False
      Size = 10
    end
    object CO_LOCAL_TAXOFFSET_GL_TAG: TStringField
      FieldName = 'OFFSET_GL_TAG'
      Origin = 'CO_LOCAL_TAX_HIST.OFFSET_GL_TAG'
    end
    object CO_LOCAL_TAXLOCAL_ACTIVE: TStringField
      FieldName = 'LOCAL_ACTIVE'
      Origin = 'CO_LOCAL_TAX_HIST.LOCAL_ACTIVE'
      Required = True
      Size = 1
    end
    object CO_LOCAL_TAXSELF_ADJUST_TAX: TStringField
      FieldName = 'SELF_ADJUST_TAX'
      Origin = 'CO_LOCAL_TAX_HIST.SELF_ADJUST_TAX'
      Required = True
      Size = 1
    end
    object CO_LOCAL_TAXAUTOCREATE_ON_NEW_HIRE: TStringField
      FieldName = 'AUTOCREATE_ON_NEW_HIRE'
      Origin = 'CO_LOCAL_TAX_HIST.AUTOCREATE_ON_NEW_HIRE'
      Required = True
      Size = 1
    end
    object CO_LOCAL_TAXSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
      Origin = 'CO_LOCAL_TAX_HIST.SY_STATES_NBR'
      Required = True
    end
    object CO_LOCAL_TAXsystemRate: TFloatField
      FieldKind = fkLookup
      FieldName = 'systemRate'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'TAX_RATE'
      KeyFields = 'SY_LOCALS_NBR'
      DisplayFormat = '#,##0.00######'
      Lookup = True
    end
    object CO_LOCAL_TAXcompanyRate: TFloatField
      FieldKind = fkCalculated
      FieldName = 'companyRate'
      DisplayFormat = '#,##0.00######'
      Calculated = True
    end
    object CO_LOCAL_TAXAPPLIED_FOR: TStringField
      FieldName = 'APPLIED_FOR'
      Size = 1
    end
    object CO_LOCAL_TAXCOUNTYName: TStringField
      FieldKind = fkLookup
      FieldName = 'COUNTYName'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'CountyName'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_LOCAL_TAXLocalTypeDesc: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'LocalTypeDesc'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'LocalTypeDescriptionNew'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 30
      Lookup = True
    end
    object CO_LOCAL_TAXDEDUCT: TStringField
      FieldName = 'DEDUCT'
      Size = 1
    end
    object CO_LOCAL_TAXLOCAL_TYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'LOCAL_TYPE'
      Size = 1
      Calculated = True
    end
    object CO_LOCAL_TAXCombineForTaxPayments: TStringField
      FieldKind = fkCalculated
      FieldName = 'CombineForTaxPayments'
      Size = 1
      Calculated = True
    end
    object CO_LOCAL_TAXPSDCode: TStringField
      FieldKind = fkCalculated
      FieldName = 'PSDCode'
      Calculated = True
    end
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 68
    Top = 81
  end
end
