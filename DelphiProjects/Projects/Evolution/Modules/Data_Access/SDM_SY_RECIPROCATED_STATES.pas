unit SDM_SY_RECIPROCATED_STATES;

interface

uses
  SysUtils, Classes, SDM_ddTable, DB, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, EvDataAccessComponents,  SDDClasses,
  SDataDictSystem, SDataStructure;

type
  TDM_SY_RECIPROCATED_STATES = class(TDM_ddTable)
    SY_RECIPROCATED_STATES: TSY_RECIPROCATED_STATES;
    PARTICIPANT_STATE_Lookup: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    SY_RECIPROCATED_STATESPARTICIPANT_STATE_NBR: TIntegerField;
    SY_RECIPROCATED_STATESMAIN_STATE_NBR: TIntegerField;
    SY_RECIPROCATED_STATESSY_RECIPROCATED_STATES_NBR: TIntegerField;
  private
  public
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_SY_RECIPROCATED_STATES);

finalization
  UnregisterClass(TDM_SY_RECIPROCATED_STATES);

end.
