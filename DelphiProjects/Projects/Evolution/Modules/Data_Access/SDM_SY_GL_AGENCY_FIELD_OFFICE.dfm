inherited DM_SY_GL_AGENCY_FIELD_OFFICE: TDM_SY_GL_AGENCY_FIELD_OFFICE
  OldCreateOrder = False
  Left = 276
  Top = 107
  Height = 222
  Width = 468
  object SY_GL_AGENCY_FIELD_OFFICE: TSY_GL_AGENCY_FIELD_OFFICE
    Aggregates = <>
    Params = <>
    ProviderName = 'SY_GL_AGENCY_FIELD_OFFICE_PROV'
    PictureMasks.Strings = (
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    ValidateWithMask = True
    Left = 94
    Top = 31
    object SY_GL_AGENCY_FIELD_OFFICEADDITIONAL_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'ADDITIONAL_NAME'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."ADDITIONAL_NAME"'
      Size = 40
    end
    object SY_GL_AGENCY_FIELD_OFFICEADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."ADDRESS1"'
      Required = True
      Size = 30
    end
    object SY_GL_AGENCY_FIELD_OFFICEADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."ADDRESS2"'
      Size = 30
    end
    object SY_GL_AGENCY_FIELD_OFFICEATTENTION_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'ATTENTION_NAME'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."ATTENTION_NAME"'
      Size = 40
    end
    object SY_GL_AGENCY_FIELD_OFFICECITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."CITY"'
      Required = True
    end
    object SY_GL_AGENCY_FIELD_OFFICECONTACT1: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT1'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."CONTACT1"'
      Size = 30
    end
    object SY_GL_AGENCY_FIELD_OFFICECONTACT2: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT2'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."CONTACT2"'
      Size = 30
    end
    object SY_GL_AGENCY_FIELD_OFFICEDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object SY_GL_AGENCY_FIELD_OFFICEDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object SY_GL_AGENCY_FIELD_OFFICEFAX: TStringField
      DisplayWidth = 20
      FieldName = 'FAX'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."FAX"'
    end
    object SY_GL_AGENCY_FIELD_OFFICEFAX_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX_DESCRIPTION'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."FAX_DESCRIPTION"'
      Size = 10
    end
    object SY_GL_AGENCY_FIELD_OFFICEFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."FILLER"'
      Size = 512
    end
    object SY_GL_AGENCY_FIELD_OFFICEPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."PHONE1"'
    end
    object SY_GL_AGENCY_FIELD_OFFICEPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."PHONE2"'
    end
    object SY_GL_AGENCY_FIELD_OFFICESTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."STATE"'
      Required = True
      Size = 2
    end
    object SY_GL_AGENCY_FIELD_OFFICESY_GLOBAL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."SY_GLOBAL_AGENCY_NBR"'
      Required = True
    end
    object SY_GL_AGENCY_FIELD_OFFICESY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."SY_GL_AGENCY_FIELD_OFFICE_NBR"'
      Required = True
    end
    object SY_GL_AGENCY_FIELD_OFFICEZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."ZIP_CODE"'
      Required = True
      Size = 10
    end
    object SY_GL_AGENCY_FIELD_OFFICEAGENCY_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'AGENCY_NAME'
      LookupDataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
      LookupKeyFields = 'SY_GLOBAL_AGENCY_NBR'
      LookupResultField = 'AGENCY_NAME'
      KeyFields = 'SY_GLOBAL_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object SY_GL_AGENCY_FIELD_OFFICECATEGORY_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'CATEGORY_TYPE'
      Origin = '"SY_GL_AGENCY_FIELD_OFFICE_HIST"."CATEGORY_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 248
    Top = 32
  end
end
