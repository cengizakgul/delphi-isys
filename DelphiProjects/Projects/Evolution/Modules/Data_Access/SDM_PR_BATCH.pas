// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_BATCH;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, Variants, EvContext,
  EvClientDataSet;

type
  TDM_PR_BATCH = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_BATCH: TPR_BATCH;
    PR_BATCHPR_BATCH_NBR: TIntegerField;
    PR_BATCHPR_NBR: TIntegerField;
    PR_BATCHCO_PR_CHECK_TEMPLATES_NBR: TIntegerField;
    PR_BATCHCO_PR_FILTERS_NBR: TIntegerField;
    PR_BATCHPERIOD_BEGIN_DATE: TDateField;
    PR_BATCHPERIOD_BEGIN_DATE_STATUS: TStringField;
    PR_BATCHPERIOD_END_DATE: TDateField;
    PR_BATCHPERIOD_END_DATE_STATUS: TStringField;
    PR_BATCHFREQUENCY: TStringField;
    PR_BATCHPAY_SALARY: TStringField;
    PR_BATCHPAY_STANDARD_HOURS: TStringField;
    PR_BATCHLOAD_DBDT_DEFAULTS: TStringField;
    PR_BATCHFILLER: TStringField;
    PR_BATCHTemplate_Name_Lookup: TStringField;
    DM_COMPANY: TDM_COMPANY;
    PR_BATCHEXCLUDE_TIME_OFF: TStringField;
    procedure PR_BATCHAfterCancel(DataSet: TDataSet);
    procedure PR_BATCHAfterInsert(DataSet: TDataSet);
    procedure PR_BATCHBeforeDelete(DataSet: TDataSet);
    procedure PR_BATCHBeforeEdit(DataSet: TDataSet);
    procedure PR_BATCHBeforeInsert(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure PR_BATCHBeforePost(DataSet: TDataSet);
  public
    { Public declarations }
  end;

implementation

uses
  EvUtils, SFieldCodeValues, SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_BATCH.PR_BATCHAfterCancel(DataSet: TDataSet);
begin
//  ctx_DataAccess.NewBatchInserted := False;
  ctx_DataAccess.TemplateNumber := 0;
end;

procedure TDM_PR_BATCH.PR_BATCHAfterInsert(DataSet: TDataSet);
begin
//  ctx_DataAccess.NewBatchInserted := True;
end;

procedure TDM_PR_BATCH.PR_BATCHBeforeDelete(DataSet: TDataSet);
begin
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_BATCH_NBR=' + DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.AsString);
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.First;
  while not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Eof do
  begin
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Next;
  end;

  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_BATCH.PR_BATCHBeforeEdit(DataSet: TDataSet);
begin
//  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_BATCH.PR_BATCHBeforeInsert(DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure TDM_PR_BATCH.DataModuleCreate(Sender: TObject);
begin
  ctx_DataAccess.NewBatchInserted := False;
end;

procedure TDM_PR_BATCH.PR_BATCHBeforePost(DataSet: TDataSet);
begin
  if not DataSet.FieldByName('PERIOD_BEGIN_DATE').IsNull then
    DataSet.FieldByName('PERIOD_BEGIN_DATE').AsDateTime := Trunc(DataSet.FieldByName('PERIOD_BEGIN_DATE').AsFloat);
  if not DataSet.FieldByName('PERIOD_END_DATE').IsNull then
    DataSet.FieldByName('PERIOD_END_DATE').AsDateTime := Trunc(DataSet.FieldByName('PERIOD_END_DATE').AsFloat);
end;

initialization
  RegisterClass(TDM_PR_BATCH);

finalization
  UnregisterClass(TDM_PR_BATCH);

end.
