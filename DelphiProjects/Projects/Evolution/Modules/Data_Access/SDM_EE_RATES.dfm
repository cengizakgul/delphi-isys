inherited DM_EE_RATES: TDM_EE_RATES
  OldCreateOrder = True
  Left = 615
  Top = 209
  Height = 309
  Width = 559
  object EE_RATES: TEE_RATES
    ProviderName = 'EE_RATES_PROV'
    AggregatesActive = True
    AfterOpen = EE_RATESAfterOpen
    AfterInsert = EE_RATESAfterInsert
    BeforePost = EE_RATESBeforePost
    BeforeDelete = EE_RATESBeforeDelete
    Left = 45
    Top = 29
    object EE_RATESEE_RATES_NBR: TIntegerField
      FieldName = 'EE_RATES_NBR'
    end
    object EE_RATESCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object EE_RATESCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object EE_RATESCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object EE_RATESCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object EE_RATESEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_RATESRATE_NUMBER: TIntegerField
      FieldName = 'RATE_NUMBER'
      OnValidate = EE_RATESRATE_NUMBERValidate
    end
    object EE_RATESPRIMARY_RATE: TStringField
      FieldName = 'PRIMARY_RATE'
      OnValidate = EE_RATESRATE_NUMBERValidate
      Size = 1
    end
    object EE_RATESRATE_AMOUNT: TFloatField
      FieldName = 'RATE_AMOUNT'
      OnValidate = EE_RATESRATE_NUMBERValidate
      DisplayFormat = '#,##0.00##'
    end
    object EE_RATESCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
    end
    object EE_RATESCO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
    end
    object EE_RATESCO_HR_POSITIONS_NBR: TIntegerField
      FieldName = 'CO_HR_POSITIONS_NBR'
    end
    object EE_RATESCO_HR_POSITION_GRADES: TIntegerField
      FieldName = 'CO_HR_POSITION_GRADES'
    end
    object EE_RATESRNUM: TAggregateField
      FieldName = 'RateNumMax'
      Active = True
      Expression = 'max(RATE_NUMBER)'
    end
    object EE_RATESPrRateMax: TAggregateField
      FieldName = 'PrRateMax'
      Active = True
      Expression = 'max(PRIMARY_RATE)'
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 184
    Top = 40
  end
end
