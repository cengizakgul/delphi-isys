// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_BANK_ACCOUNT;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  EvUtils, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents, EvContext,
  ISDataAccessComponents, SDataDictbureau, EvUIComponents, EvClientDataSet;

type
  TDM_CL_BANK_ACCOUNT = class(TDM_ddTable)
    CL_BANK_ACCOUNT: TCL_BANK_ACCOUNT;
    CL_BANK_ACCOUNTCUSTOM_BANK_ACCOUNT_NUMBER: TStringField;
    CL_BANK_ACCOUNTaba_nbr_lookup: TStringField;
    CL_BANK_ACCOUNTPrint_Name_Lookup: TStringField;
    CL_BANK_ACCOUNTBank_Name: TStringField;
    CL_BANK_ACCOUNTBANK_ACCOUNT_TYPE: TStringField;
    CL_BANK_ACCOUNTCL_BANK_ACCOUNT_NBR: TIntegerField;
    CL_BANK_ACCOUNTSB_BANKS_NBR: TIntegerField;
    CL_BANK_ACCOUNTBEGINNING_BALANCE: TFloatField;
    CL_BANK_ACCOUNTNEXT_CHECK_NUMBER: TIntegerField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    CL_BANK_ACCOUNTSIGNATURE_CL_BLOB_NBR: TIntegerField;
    CL_BANK_ACCOUNTLOGO_CL_BLOB_NBR: TIntegerField;
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CL_BANK_ACCOUNT);

finalization
  UnregisterClass(TDM_CL_BANK_ACCOUNT);

end.
