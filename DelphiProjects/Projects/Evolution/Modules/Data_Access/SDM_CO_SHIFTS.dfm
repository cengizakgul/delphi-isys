inherited DM_CO_SHIFTS: TDM_CO_SHIFTS
  OldCreateOrder = True
  Left = 413
  Top = 226
  Height = 417
  Width = 571
  object CO_SHIFTS: TCO_SHIFTS
    ProviderName = 'CO_SHIFTS_PROV'
    PictureMasks.Strings = (
      
        'DEFAULT_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]}'#9'T'#9'F'
      
        'DEFAULT_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F')
    BeforePost = CO_SHIFTSBeforePost
    Left = 67
    Top = 26
    object CO_SHIFTSCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
      Origin = '"CO_SHIFTS_HIST"."CL_E_D_GROUPS_NBR"'
    end
    object CO_SHIFTSCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"CO_SHIFTS_HIST"."CO_NBR"'
      Required = True
    end
    object CO_SHIFTSCO_SHIFTS_NBR: TIntegerField
      FieldName = 'CO_SHIFTS_NBR'
      Origin = '"CO_SHIFTS_HIST"."CO_SHIFTS_NBR"'
      Required = True
    end
    object CO_SHIFTSDEFAULT_AMOUNT: TFloatField
      FieldName = 'DEFAULT_AMOUNT'
      Origin = '"CO_SHIFTS_HIST"."DEFAULT_AMOUNT"'
      DisplayFormat = '#,##0.00####'
    end
    object CO_SHIFTSDEFAULT_PERCENTAGE: TFloatField
      FieldName = 'DEFAULT_PERCENTAGE'
      Origin = '"CO_SHIFTS_HIST"."DEFAULT_PERCENTAGE"'
      DisplayFormat = '#,##0.00####'
    end
    object CO_SHIFTSNAME: TStringField
      FieldName = 'NAME'
      Origin = '"CO_SHIFTS_HIST"."NAME"'
      Required = True
      Size = 40
    end
    object CO_SHIFTSEDGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGroupName'
      LookupDataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
      LookupKeyFields = 'CL_E_D_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_E_D_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_SHIFTSAUTO_CREATE_ON_NEW_HIRE: TStringField
      FieldName = 'AUTO_CREATE_ON_NEW_HIRE'
      FixedChar = True
      Size = 1
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 400
    Top = 48
  end
end
