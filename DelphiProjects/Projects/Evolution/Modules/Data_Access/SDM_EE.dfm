inherited DM_EE: TDM_EE
  OldCreateOrder = True
  Left = 382
  Top = 193
  Height = 251
  Width = 416
  object DM_CLIENT: TDM_CLIENT
    Left = 57
    Top = 69
  end
  object EE: TEE
    ProviderName = 'EE_PROV'
    AggregatesActive = True
    PictureMasks.Strings = (
      'STATE'#9'*{&,@}'#9'T'#9'T')
    FieldDefs = <
      item
        Name = 'Employee_LastName_Calculate'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Employee_FirstName_Calculate'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Employee_MI_Calculate'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Employee_Name_Calculate'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Trim_Number'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'CUSTOM_DIVISION_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_BRANCH_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_DEPARTMENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_TEAM_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_TEAM_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_DEPARTMENT_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_BRANCH_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_DIVISION_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'COMPANY_OR_INDIVIDUAL_NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_MARITAL_STATUS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMBER_OF_DEPENDENTS'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CURRENT_TERMINATION_CODE_DESC'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'DISTRIBUTION_CODE_1099R'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'TAX_AMT_DETERMINED_1099R'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TOTAL_DISTRIBUTION_1099R'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PENSION_PLAN_1099R'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MAKEUP_FICA_ON_CLEANUP_PR'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_HR_EEO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SECONDARY_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'CO_PR_CHECK_TEMPLATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GENERATE_SECOND_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PR_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_EE_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_EE_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'HIGHLY_COMPENSATED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CORPORATE_OFFICER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_JOBS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_WORKERS_COMP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_SALARY_GRADES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_ENABLED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'EE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CL_PERSON_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_DIVISION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_BRANCH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DEPARTMENT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TEAM_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_EMPLOYEE_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 9
      end
      item
        Name = 'CO_HR_APPLICANT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_RECRUITERS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_REFERRALS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TIME_CLOCK_NUMBER'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'ORIGINAL_HIRE_DATE'
        DataType = ftDate
      end
      item
        Name = 'CURRENT_HIRE_DATE'
        DataType = ftDate
      end
      item
        Name = 'NEW_HIRE_REPORT_SENT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CURRENT_TERMINATION_DATE'
        DataType = ftDate
      end
      item
        Name = 'CURRENT_TERMINATION_CODE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_HR_SUPERVISORS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SALARY_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'STANDARD_HOURS'
        DataType = ftFloat
      end
      item
        Name = 'AUTOPAY_CO_SHIFTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_POSITIONS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'POSITION_EFFECTIVE_DATE'
        DataType = ftDate
      end
      item
        Name = 'POSITION_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_HR_PERFORMANCE_RATINGS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'REVIEW_DATE'
        DataType = ftDate
      end
      item
        Name = 'NEXT_REVIEW_DATE'
        DataType = ftDate
      end
      item
        Name = 'PAY_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NEXT_RAISE_DATE'
        DataType = ftDate
      end
      item
        Name = 'NEXT_RAISE_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'NEXT_RAISE_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'NEXT_RAISE_RATE'
        DataType = ftFloat
      end
      item
        Name = 'NEXT_PAY_FREQUENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TIPPED_DIRECTLY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ALD_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DISTRIBUTE_TAXES'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_PAY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_DELIVERY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'WORKERS_COMP_WAGE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'GROUP_TERM_POLICY_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'CO_UNIONS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EIC'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FLSA_EXEMPT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_PENSION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_DEFERRED_COMP'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_DECEASED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_STATUTORY_EMPLOYEE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_LEGAL_REP'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'HOME_TAX_EE_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EXEMPT_EMPLOYEE_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXEMPT_EMPLOYEE_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXEMPT_EXCLUDE_EE_FED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXEMPT_EMPLOYER_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXEMPT_EMPLOYER_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXEMPT_EMPLOYER_FUI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OVERRIDE_FED_TAX_VALUE'
        DataType = ftFloat
      end
      item
        Name = 'OVERRIDE_FED_TAX_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GOV_GARNISH_PRIOR_CHILD_SUPPT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SECURITY_CLEARANCE'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CALCULATED_SALARY'
        DataType = ftFloat
      end
      item
        Name = 'BADGE_ID'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'ON_CALL_FROM'
        DataType = ftDateTime
      end
      item
        Name = 'ON_CALL_TO'
        DataType = ftDateTime
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'ADDRESS1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ADDRESS2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CITY'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ZIP_CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'COUNTY'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'PHONE1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'PHONE2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'PHONE3'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION3'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'BASE_RETURNS_ON_THIS_EE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'E_MAIL_ADDRESS_CALC'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'GTL_NUMBER_OF_HOURS'
        DataType = ftInteger
      end
      item
        Name = 'GTL_RATE'
        DataType = ftFloat
      end
      item
        Name = 'E_MAIL_ADDRESS'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'SELFSERVE_ENABLED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SELFSERVE_USERNAME'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'SELFSERVE_PASSWORD'
        DataType = ftString
        Size = 64
      end>
    BeforePost = EEBeforePost
    BeforeDelete = EEBeforeDelete
    OnCalcFields = EECalcFields
    OnNewRecord = EENewRecord
    OnPrepareLookups = EEPrepareLookups
    OnAddLookups = EEAddLookups
    OnUnPrepareLookups = EEUnPrepareLookups
    Left = 58
    Top = 17
    object EEEmployee_LastName_Calculate: TStringField
      DisplayWidth = 15
      FieldKind = fkInternalCalc
      FieldName = 'Employee_LastName_Calculate'
    end
    object EEEmployee_FirstName_Calculate: TStringField
      DisplayWidth = 15
      FieldKind = fkInternalCalc
      FieldName = 'Employee_FirstName_Calculate'
    end
    object EEEmployee_MI_Calculate: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Employee_MI_Calculate'
      Size = 1
    end
    object EEEmployee_Name_Calculate: TStringField
      DisplayWidth = 50
      FieldKind = fkInternalCalc
      FieldName = 'Employee_Name_Calculate'
      Size = 50
    end
    object EEGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"EE_HIST"."GENERAL_LEDGER_TAG"'
    end
    object EETrim_Number: TStringField
      Alignment = taRightJustify
      DisplayLabel = 'Aligned EE Code'
      DisplayWidth = 9
      FieldKind = fkInternalCalc
      FieldName = 'Trim_Number'
      Size = 9
    end
    object EECUSTOM_DIVISION_NUMBER: TStringField
      DisplayWidth = 20
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DIVISION_NUMBER'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'CUSTOM_DIVISION_NUMBER'
      KeyFields = 'CO_DIVISION_NBR'
    end
    object EECUSTOM_BRANCH_NUMBER: TStringField
      DisplayWidth = 20
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_BRANCH_NUMBER'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'CUSTOM_BRANCH_NUMBER'
      KeyFields = 'CO_BRANCH_NBR'
    end
    object EECUSTOM_DEPARTMENT_NUMBER: TStringField
      DisplayWidth = 20
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DEPARTMENT_NUMBER'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'CUSTOM_DEPARTMENT_NUMBER'
      KeyFields = 'CO_DEPARTMENT_NBR'
    end
    object EECUSTOM_TEAM_NUMBER: TStringField
      DisplayWidth = 20
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_TEAM_NUMBER'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'CUSTOM_TEAM_NUMBER'
      KeyFields = 'CO_TEAM_NBR'
    end
    object EECUSTOM_COMPANY_NUMBER: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'CUSTOM_COMPANY_NUMBER'
      LookupDataSet = DM_CO.CO
      LookupKeyFields = 'CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CO_NBR'
      Lookup = True
    end
    object EECUSTOM_TEAM_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_TEAM_NAME'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_TEAM_NBR'
      Size = 40
    end
    object EECUSTOM_DEPARTMENT_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DEPARTMENT_NAME'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Size = 40
    end
    object EECUSTOM_BRANCH_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_BRANCH_NAME'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_BRANCH_NBR'
      Size = 40
    end
    object EECUSTOM_DIVISION_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DIVISION_NAME'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DIVISION_NBR'
      Size = 40
    end
    object EECUSTOM_COMPANY_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CUSTOM_COMPANY_NAME'
      LookupDataSet = DM_CO.CO
      LookupKeyFields = 'CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_NBR'
      Size = 40
      Lookup = True
    end
    object EECOMPANY_OR_INDIVIDUAL_NAME: TStringField
      DisplayWidth = 1
      FieldName = 'COMPANY_OR_INDIVIDUAL_NAME'
      Origin = '"EE_HIST"."COMPANY_OR_INDIVIDUAL_NAME"'
      Required = True
      OnChange = EECOMPANY_OR_INDIVIDUAL_NAMEChange
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEFEDERAL_MARITAL_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'FEDERAL_MARITAL_STATUS'
      Origin = '"EE_HIST"."FEDERAL_MARITAL_STATUS"'
      Required = True
      Size = 1
    end
    object EENUMBER_OF_DEPENDENTS: TIntegerField
      DisplayWidth = 10
      FieldName = 'NUMBER_OF_DEPENDENTS'
      Origin = '"EE_HIST"."NUMBER_OF_DEPENDENTS"'
      Required = True
    end
    object EECURRENT_TERMINATION_CODE_DESC: TStringField
      DisplayWidth = 80
      FieldKind = fkInternalCalc
      FieldName = 'CURRENT_TERMINATION_CODE_DESC'
      Size = 80
    end
    object EEDISTRIBUTION_CODE_1099R: TStringField
      DisplayWidth = 4
      FieldName = 'DISTRIBUTION_CODE_1099R'
      Origin = '"EE_HIST"."DISTRIBUTION_CODE_1099R"'
      Size = 4
    end
    object EETAX_AMT_DETERMINED_1099R: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_AMT_DETERMINED_1099R'
      Origin = '"EE_HIST"."TAX_AMT_DETERMINED_1099R"'
      Required = True
      Size = 1
    end
    object EETOTAL_DISTRIBUTION_1099R: TStringField
      DisplayWidth = 1
      FieldName = 'TOTAL_DISTRIBUTION_1099R'
      Origin = '"EE_HIST"."TOTAL_DISTRIBUTION_1099R"'
      Required = True
      Size = 1
    end
    object EEPENSION_PLAN_1099R: TStringField
      DisplayWidth = 1
      FieldName = 'PENSION_PLAN_1099R'
      Origin = '"EE_HIST"."PENSION_PLAN_1099R"'
      Required = True
      Size = 1
    end
    object EEMAKEUP_FICA_ON_CLEANUP_PR: TStringField
      DisplayWidth = 1
      FieldName = 'MAKEUP_FICA_ON_CLEANUP_PR'
      Origin = '"EE_HIST"."MAKEUP_FICA_ON_CLEANUP_PR"'
      Required = True
      Size = 1
    end
    object EESY_HR_EEO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_HR_EEO_NBR'
      Origin = '"EE_HIST"."SY_HR_EEO_NBR"'
    end
    object EESECONDARY_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'SECONDARY_NOTES'
      Origin = '"EE_HIST"."SECONDARY_NOTES"'
      Size = 8
    end
    object EECO_PR_CHECK_TEMPLATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PR_CHECK_TEMPLATES_NBR'
      Origin = '"EE_HIST"."CO_PR_CHECK_TEMPLATES_NBR"'
    end
    object EEGENERATE_SECOND_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'GENERATE_SECOND_CHECK'
      Origin = '"EE_HIST"."GENERATE_SECOND_CHECK"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EEDivisionNumber: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'DivisionNumber'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'CUSTOM_DIVISION_NUMBER'
      KeyFields = 'CO_DIVISION_NBR'
      Lookup = True
    end
    object EEBranchNumber: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'BranchNumber'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'CUSTOM_BRANCH_NUMBER'
      KeyFields = 'CO_BRANCH_NBR'
      Lookup = True
    end
    object EEDepartmentNumber: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'DepartmentNumber'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'CUSTOM_DEPARTMENT_NUMBER'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Lookup = True
    end
    object EETeamNumber: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'TeamNumber'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'CUSTOM_TEAM_NUMBER'
      KeyFields = 'CO_TEAM_NBR'
      Lookup = True
    end
    object EEPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
    end
    object EEPR_EE_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_MB_GROUP_NBR'
    end
    object EEPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
    end
    object EETAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
    end
    object EEHIGHLY_COMPENSATED: TStringField
      DisplayWidth = 1
      FieldName = 'HIGHLY_COMPENSATED'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EECORPORATE_OFFICER: TStringField
      DisplayWidth = 1
      FieldName = 'CORPORATE_OFFICER'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EECO_JOBS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_JOBS_NBR'
    end
    object EECO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
    end
    object EECO_HR_SALARY_GRADES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_SALARY_GRADES_NBR'
    end
    object EEEE_ENABLED: TStringField
      DisplayWidth = 1
      FieldName = 'EE_ENABLED'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EESTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"EE_HIST"."STATE"'
      Size = 2
    end
    object EEEE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_NBR'
      Origin = '"EE_HIST"."EE_NBR"'
      Required = True
      Visible = False
    end
    object EESOCIAL_SECURITY: TStringField
      DisplayWidth = 11
      FieldKind = fkLookup
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      LookupDataSet = DM_CL_PERSON.CL_PERSON
      LookupKeyFields = 'CL_PERSON_NBR'
      LookupResultField = 'SOCIAL_SECURITY_NUMBER'
      KeyFields = 'CL_PERSON_NBR'
      Visible = False
      Size = 11
      Lookup = True
    end
    object EECL_PERSON_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_PERSON_NBR'
      Origin = '"EE_HIST"."CL_PERSON_NBR"'
      Required = True
      Visible = False
    end
    object EECO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = '"EE_HIST"."CO_NBR"'
      Required = True
      Visible = False
    end
    object EECO_DIVISION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DIVISION_NBR'
      Origin = '"EE_HIST"."CO_DIVISION_NBR"'
      Visible = False
      OnChange = DBDTChange
    end
    object EECO_BRANCH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BRANCH_NBR'
      Origin = '"EE_HIST"."CO_BRANCH_NBR"'
      Visible = False
      OnChange = DBDTChange
    end
    object EECO_DEPARTMENT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = '"EE_HIST"."CO_DEPARTMENT_NBR"'
      Visible = False
      OnChange = DBDTChange
    end
    object EECO_TEAM_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TEAM_NBR'
      Origin = '"EE_HIST"."CO_TEAM_NBR"'
      Visible = False
      OnChange = DBDTChange
    end
    object EECUSTOM_EMPLOYEE_NUMBER: TStringField
      Alignment = taRightJustify
      DisplayWidth = 6
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Origin = '"EE_HIST"."CUSTOM_EMPLOYEE_NUMBER"'
      Required = True
      Visible = False
      Size = 9
    end
    object EECO_HR_APPLICANT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_APPLICANT_NBR'
      Origin = '"EE_HIST"."CO_HR_APPLICANT_NBR"'
      Visible = False
    end
    object EECO_HR_RECRUITERS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_RECRUITERS_NBR'
      Origin = '"EE_HIST"."CO_HR_RECRUITERS_NBR"'
      Visible = False
    end
    object EECO_HR_REFERRALS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_REFERRALS_NBR'
      Origin = '"EE_HIST"."CO_HR_REFERRALS_NBR"'
      Visible = False
    end
    object EETIME_CLOCK_NUMBER: TStringField
      DisplayWidth = 9
      FieldName = 'TIME_CLOCK_NUMBER'
      Origin = '"EE_HIST"."TIME_CLOCK_NUMBER"'
      Visible = False
      Size = 9
    end
    object EEORIGINAL_HIRE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'ORIGINAL_HIRE_DATE'
      Origin = '"EE_HIST"."ORIGINAL_HIRE_DATE"'
      Visible = False
    end
    object EECURRENT_HIRE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CURRENT_HIRE_DATE'
      Origin = '"EE_HIST"."CURRENT_HIRE_DATE"'
      Visible = False
    end
    object EENEW_HIRE_REPORT_SENT: TStringField
      DisplayWidth = 1
      FieldName = 'NEW_HIRE_REPORT_SENT'
      Origin = '"EE_HIST"."NEW_HIRE_REPORT_SENT"'
      Required = True
      Visible = False
      Size = 1
    end
    object EECURRENT_TERMINATION_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CURRENT_TERMINATION_DATE'
      Origin = '"EE_HIST"."CURRENT_TERMINATION_DATE"'
      Visible = False
    end
    object EECURRENT_TERMINATION_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'CURRENT_TERMINATION_CODE'
      Origin = '"EE_HIST"."CURRENT_TERMINATION_CODE"'
      Required = True
      Visible = False
      Size = 1
    end
    object EECO_HR_SUPERVISORS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_SUPERVISORS_NBR'
      Origin = '"EE_HIST"."CO_HR_SUPERVISORS_NBR"'
      Visible = False
    end
    object EESALARY_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'SALARY_AMOUNT'
      Origin = '"EE_HIST"."SALARY_AMOUNT"'
      Visible = False
      OnChange = EESALARY_AMOUNTChange
      DisplayFormat = '#,##0.00'
      EditFormat = '####0.00'
    end
    object EESTANDARD_HOURS: TFloatField
      DisplayWidth = 10
      FieldName = 'STANDARD_HOURS'
      Origin = '"EE_HIST"."STANDARD_HOURS"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EEAUTOPAY_CO_SHIFTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'AUTOPAY_CO_SHIFTS_NBR'
      Origin = '"EE_HIST"."AUTOPAY_CO_SHIFTS_NBR"'
      Visible = False
    end
    object EECO_HR_POSITIONS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_POSITIONS_NBR'
      Origin = '"EE_HIST"."CO_HR_POSITIONS_NBR"'
      Visible = False
    end
    object EEPOSITION_EFFECTIVE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'POSITION_EFFECTIVE_DATE'
      Origin = '"EE_HIST"."POSITION_EFFECTIVE_DATE"'
      Visible = False
    end
    object EEPOSITION_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'POSITION_STATUS'
      Origin = '"EE_HIST"."POSITION_STATUS"'
      Visible = False
      Size = 1
    end
    object EECO_HR_PERFORMANCE_RATINGS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_HR_PERFORMANCE_RATINGS_NBR'
      Origin = '"EE_HIST"."CO_HR_PERFORMANCE_RATINGS_NBR"'
      Visible = False
    end
    object EEREVIEW_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'REVIEW_DATE'
      Origin = '"EE_HIST"."REVIEW_DATE"'
      Visible = False
    end
    object EENEXT_REVIEW_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'NEXT_REVIEW_DATE'
      Origin = '"EE_HIST"."NEXT_REVIEW_DATE"'
      Visible = False
    end
    object EEPAY_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY'
      Origin = '"EE_HIST"."PAY_FREQUENCY"'
      Required = True
      Visible = False
      Size = 1
    end
    object EENEXT_RAISE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'NEXT_RAISE_DATE'
      Origin = '"EE_HIST"."NEXT_RAISE_DATE"'
      Visible = False
    end
    object EENEXT_RAISE_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'NEXT_RAISE_AMOUNT'
      Origin = '"EE_HIST"."NEXT_RAISE_AMOUNT"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EENEXT_RAISE_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'NEXT_RAISE_PERCENTAGE'
      Origin = '"EE_HIST"."NEXT_RAISE_PERCENTAGE"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EENEXT_RAISE_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'NEXT_RAISE_RATE'
      Origin = '"EE_HIST"."NEXT_RAISE_RATE"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EENEXT_PAY_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'NEXT_PAY_FREQUENCY'
      Origin = '"EE_HIST"."NEXT_PAY_FREQUENCY"'
      Visible = False
      Size = 1
    end
    object EETIPPED_DIRECTLY: TStringField
      DisplayWidth = 1
      FieldName = 'TIPPED_DIRECTLY'
      Origin = '"EE_HIST"."TIPPED_DIRECTLY"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEALD_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'ALD_CL_E_D_GROUPS_NBR'
      Origin = '"EE_HIST"."ALD_CL_E_D_GROUPS_NBR"'
      Visible = False
    end
    object EEDISTRIBUTE_TAXES: TStringField
      DisplayWidth = 1
      FieldName = 'DISTRIBUTE_TAXES'
      Origin = '"EE_HIST"."DISTRIBUTE_TAXES"'
      Required = True
      Visible = False
      Size = 1
    end
    object EECO_PAY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PAY_GROUP_NBR'
      Origin = '"EE_HIST"."CO_PAY_GROUP_NBR"'
      Visible = False
    end
    object EECL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Origin = '"EE_HIST"."CL_DELIVERY_GROUP_NBR"'
      Visible = False
    end
    object EEWORKERS_COMP_WAGE_LIMIT: TFloatField
      DisplayWidth = 10
      FieldName = 'WORKERS_COMP_WAGE_LIMIT'
      Origin = '"EE_HIST"."WORKERS_COMP_WAGE_LIMIT"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EEGROUP_TERM_POLICY_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'GROUP_TERM_POLICY_AMOUNT'
      Origin = '"EE_HIST"."GROUP_TERM_POLICY_AMOUNT"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EECO_UNIONS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_UNIONS_NBR'
      Origin = '"EE_HIST"."CO_UNIONS_NBR"'
      Visible = False
    end
    object EEEIC: TStringField
      DisplayWidth = 1
      FieldName = 'EIC'
      Origin = '"EE_HIST"."EIC"'
      Visible = False
      Size = 1
    end
    object EEFLSA_EXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'FLSA_EXEMPT'
      Origin = '"EE_HIST"."FLSA_EXEMPT"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEW2_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'W2_TYPE'
      Origin = '"EE_HIST"."W2_TYPE"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEW2_PENSION: TStringField
      DisplayWidth = 1
      FieldName = 'W2_PENSION'
      Origin = '"EE_HIST"."W2_PENSION"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEW2_DEFERRED_COMP: TStringField
      DisplayWidth = 1
      FieldName = 'W2_DEFERRED_COMP'
      Origin = '"EE_HIST"."W2_DEFERRED_COMP"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEW2_DECEASED: TStringField
      DisplayWidth = 1
      FieldName = 'W2_DECEASED'
      Origin = '"EE_HIST"."W2_DECEASED"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEW2_STATUTORY_EMPLOYEE: TStringField
      DisplayWidth = 1
      FieldName = 'W2_STATUTORY_EMPLOYEE'
      Origin = '"EE_HIST"."W2_STATUTORY_EMPLOYEE"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEW2_LEGAL_REP: TStringField
      DisplayWidth = 1
      FieldName = 'W2_LEGAL_REP'
      Origin = '"EE_HIST"."W2_LEGAL_REP"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEHOME_TAX_EE_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_TAX_EE_STATES_NBR'
      Origin = '"EE_HIST"."HOME_TAX_EE_STATES_NBR"'
      Visible = False
    end
    object EEEXEMPT_EMPLOYEE_OASDI: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EMPLOYEE_OASDI'
      Origin = '"EE_HIST"."EXEMPT_EMPLOYEE_OASDI"'
      Required = True
      Visible = False
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEEXEMPT_EMPLOYEE_MEDICARE: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EMPLOYEE_MEDICARE'
      Origin = '"EE_HIST"."EXEMPT_EMPLOYEE_MEDICARE"'
      Required = True
      Visible = False
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEEXEMPT_EXCLUDE_EE_FED: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EXCLUDE_EE_FED'
      Origin = '"EE_HIST"."EXEMPT_EXCLUDE_EE_FED"'
      Required = True
      Visible = False
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEEXEMPT_EMPLOYER_OASDI: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EMPLOYER_OASDI'
      Origin = '"EE_HIST"."EXEMPT_EMPLOYER_OASDI"'
      Required = True
      Visible = False
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEEXEMPT_EMPLOYER_MEDICARE: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EMPLOYER_MEDICARE'
      Origin = '"EE_HIST"."EXEMPT_EMPLOYER_MEDICARE"'
      Required = True
      Visible = False
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEEXEMPT_EMPLOYER_FUI: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EMPLOYER_FUI'
      Origin = '"EE_HIST"."EXEMPT_EMPLOYER_FUI"'
      Required = True
      Visible = False
      OnValidate = EECOMPANY_OR_INDIVIDUAL_NAMEValidate
      Size = 1
    end
    object EEOVERRIDE_FED_TAX_VALUE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_FED_TAX_VALUE'
      Origin = '"EE_HIST"."OVERRIDE_FED_TAX_VALUE"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EEOVERRIDE_FED_TAX_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'OVERRIDE_FED_TAX_TYPE'
      Origin = '"EE_HIST"."OVERRIDE_FED_TAX_TYPE"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEGOV_GARNISH_PRIOR_CHILD_SUPPT: TStringField
      DisplayWidth = 1
      FieldName = 'GOV_GARNISH_PRIOR_CHILD_SUPPT'
      Origin = '"EE_HIST"."GOV_GARNISH_PRIOR_CHILD_SUPPT"'
      Visible = False
      Size = 1
    end
    object EESECURITY_CLEARANCE: TStringField
      DisplayWidth = 20
      FieldName = 'SECURITY_CLEARANCE'
      Origin = '"EE_HIST"."SECURITY_CLEARANCE"'
      Visible = False
    end
    object EECALCULATED_SALARY: TFloatField
      DisplayWidth = 10
      FieldName = 'CALCULATED_SALARY'
      Origin = '"EE_HIST"."CALCULATED_SALARY"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object EEBADGE_ID: TStringField
      DisplayWidth = 15
      FieldName = 'BADGE_ID'
      Origin = '"EE_HIST"."BADGE_ID"'
      Visible = False
      Size = 15
    end
    object EEON_CALL_FROM: TDateTimeField
      DisplayWidth = 10
      FieldName = 'ON_CALL_FROM'
      Origin = '"EE_HIST"."ON_CALL_FROM"'
      Visible = False
    end
    object EEON_CALL_TO: TDateTimeField
      DisplayWidth = 10
      FieldName = 'ON_CALL_TO'
      Origin = '"EE_HIST"."ON_CALL_TO"'
      Visible = False
    end
    object EENOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Origin = '"EE_HIST"."NOTES"'
      Visible = False
      Size = 8
    end
    object EEFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Origin = '"EE_HIST"."FILLER"'
      Visible = False
      Size = 512
    end
    object EEADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"EE_HIST"."ADDRESS1"'
      Visible = False
      Size = 30
    end
    object EEADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"EE_HIST"."ADDRESS2"'
      Visible = False
      Size = 30
    end
    object EECITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"EE_HIST"."CITY"'
      Visible = False
    end
    object EEZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"EE_HIST"."ZIP_CODE"'
      Visible = False
      Size = 10
    end
    object EECOUNTY: TStringField
      DisplayWidth = 10
      FieldName = 'COUNTY'
      Origin = '"EE_HIST"."COUNTY"'
      Visible = False
      Size = 10
    end
    object EEPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"EE_HIST"."PHONE1"'
      Visible = False
    end
    object EEDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"EE_HIST"."DESCRIPTION1"'
      Visible = False
      Size = 10
    end
    object EEPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"EE_HIST"."PHONE2"'
      Visible = False
    end
    object EEDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"EE_HIST"."DESCRIPTION2"'
      Visible = False
      Size = 10
    end
    object EEPHONE3: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE3'
      Origin = '"EE_HIST"."PHONE3"'
      Visible = False
    end
    object EEDESCRIPTION3: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION3'
      Origin = '"EE_HIST"."DESCRIPTION3"'
      Visible = False
      Size = 10
    end
    object EEBASE_RETURNS_ON_THIS_EE: TStringField
      DisplayWidth = 1
      FieldName = 'BASE_RETURNS_ON_THIS_EE'
      Origin = '"EE_HIST"."BASE_RETURNS_ON_THIS_EE"'
      Required = True
      Visible = False
      Size = 1
    end
    object EEE_MAIL_ADDRESS_CALC: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'E_MAIL_ADDRESS_CALC'
      Size = 80
    end
    object EEGTL_NUMBER_OF_HOURS: TIntegerField
      FieldName = 'GTL_NUMBER_OF_HOURS'
    end
    object EEGTL_RATE: TFloatField
      FieldName = 'GTL_RATE'
    end
    object EEE_MAIL_ADDRESS: TStringField
      FieldName = 'E_MAIL_ADDRESS'
      Size = 80
    end
    object EESELFSERVE_USERNAME: TStringField
      FieldName = 'SELFSERVE_USERNAME'
      Size = 64
    end
    object EESELFSERVE_PASSWORD: TStringField
      FieldName = 'SELFSERVE_PASSWORD'
      Size = 64
    end
    object EESELFSERVE_ENABLED: TStringField
      FieldName = 'SELFSERVE_ENABLED'
      Size = 1
    end
    object EEPRINT_VOUCHER: TStringField
      FieldName = 'PRINT_VOUCHER'
      Size = 1
    end
    object EEPosition_Desc: TStringField
      FieldKind = fkLookup
      FieldName = 'Position_Desc'
      LookupDataSet = DM_CO_HR_POSITIONS.CO_HR_POSITIONS
      LookupKeyFields = 'CO_HR_POSITIONS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_HR_POSITIONS_NBR'
      Lookup = True
    end
    object EEACA_STATUS: TStringField
      FieldName = 'ACA_STATUS'
      Size = 1
    end
    object EEACA_STATUS_desc: TStringField
      FieldKind = fkCalculated
      FieldName = 'ACA_STATUS_desc'
      Size = 25
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 126
    Top = 72
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 128
    Top = 24
  end
end
