// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_SCHEDULED_EVENT;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses;

type
  TDM_PR_SCHEDULED_EVENT = class(TDM_ddTable)
    PR_SCHEDULED_EVENT: TPR_SCHEDULED_EVENT;
    DM_COMPANY: TDM_COMPANY;
    DM_PAYROLL: TDM_PAYROLL;
    procedure PR_SCHEDULED_EVENTNewRecord(DataSet: TDataSet);
    procedure PR_SCHEDULED_EVENTAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_SCHEDULED_EVENT: TDM_PR_SCHEDULED_EVENT;

implementation

{$R *.DFM}

procedure TDM_PR_SCHEDULED_EVENT.PR_SCHEDULED_EVENTNewRecord(
  DataSet: TDataSet);
begin
  DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('STATUS').AsString := 'N';
  DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('EVENT_DATE').AsDateTime := 0;
end;

procedure TDM_PR_SCHEDULED_EVENT.PR_SCHEDULED_EVENTAfterOpen(
  DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('SCHEDULED_CHECK_DATE').Required := True;
end;

initialization
  RegisterClass(TDM_PR_SCHEDULED_EVENT);

finalization
  UnregisterClass(TDM_PR_SCHEDULED_EVENT);

end.
