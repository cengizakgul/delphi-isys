// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_CHECK_LINES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, evutils, EvTypes, EvContext,
  Variants, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, EvDataAccessComponents, ISDataAccessComponents, EvExceptions,
  EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TDBDTLookUpArray = array of integer;

  TDM_PR_CHECK_LINES = class(TDM_ddTable,IDBDTCache,IPrCheckLinesLoggerSetter)
    DM_PAYROLL: TDM_PAYROLL;
    PR_CHECK_LINES: TPR_CHECK_LINES;
    PR_CHECK_LINESPR_CHECK_LINES_NBR: TIntegerField;
    PR_CHECK_LINESPR_CHECK_NBR: TIntegerField;
    PR_CHECK_LINESLINE_TYPE: TStringField;
    PR_CHECK_LINESLINE_ITEM_DATE: TDateField;
    PR_CHECK_LINESCO_DIVISION_NBR: TIntegerField;
    PR_CHECK_LINESCO_BRANCH_NBR: TIntegerField;
    PR_CHECK_LINESCO_DEPARTMENT_NBR: TIntegerField;
    PR_CHECK_LINESCO_TEAM_NBR: TIntegerField;
    PR_CHECK_LINESCO_JOBS_NBR: TIntegerField;
    PR_CHECK_LINESCO_WORKERS_COMP_NBR: TIntegerField;
    PR_CHECK_LINESCL_AGENCY_NBR: TIntegerField;
    PR_CHECK_LINESRATE_OF_PAY: TFloatField;
    PR_CHECK_LINESAGENCY_STATUS: TStringField;
    PR_CHECK_LINESHOURS_OR_PIECES: TFloatField;
    PR_CHECK_LINESAMOUNT: TFloatField;
    PR_CHECK_LINESE_D_DIFFERENTIAL_AMOUNT: TFloatField;
    PR_CHECK_LINESCO_SHIFTS_NBR: TIntegerField;
    PR_CHECK_LINESCL_E_DS_NBR: TIntegerField;
    PR_CHECK_LINESCL_PIECES_NBR: TIntegerField;
    PR_CHECK_LINESEE_SCHEDULED_E_DS_NBR: TIntegerField;
    PR_CHECK_LINESDEDUCTION_SHORTFALL_CARRYOVER: TFloatField;
    PR_CHECK_LINESFILLER: TStringField;
    PR_CHECK_LINESEE_STATES_NBR: TIntegerField;
    PR_CHECK_LINESEE_SUI_STATES_NBR: TIntegerField;
    PR_CHECK_LINESRATE_NUMBER: TIntegerField;
    PR_CHECK_LINESPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField;
    DM_CLIENT: TDM_CLIENT;
    PR_CHECK_LINESE_D_Code_Lookup: TStringField;
    PR_CHECK_LINESE_D_Description_Lookup: TStringField;
    PR_CHECK_LINESCUSTOM_DIVISION_NUMBER: TStringField;
    PR_CHECK_LINESCUSTOM_BRANCH_NUMBER: TStringField;
    PR_CHECK_LINESCUSTOM_DEPARTMENT_NUMBER: TStringField;
    PR_CHECK_LINESCUSTOM_TEAM_NUMBER: TStringField;
    PR_CHECK_LINESCO_JOBS_NBR_DESC: TStringField;
    DM_COMPANY: TDM_COMPANY;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    PR_CHECK_LINESSorting: TStringField;
    PR_CHECK_LINESSorting2: TIntegerField;
    PR_CHECK_LINESPR_NBR: TIntegerField;
    PR_CHECK_LINESSTATE_LOOKUP: TStringField;
    PR_CHECK_LINESSUI_STATE_LOOKUP: TStringField;
    PR_CHECK_LINESCL_AGENCY_LOOKUP: TStringField;
    PR_CHECK_LINESCO_WORKERS_COMP_LOOKUP: TStringField;
    PR_CHECK_LINESCO_SHIFTS_LOOKUP: TStringField;
    PR_CHECK_LINESCL_PIECES_LOOKUP: TStringField;
    PR_CHECK_LINESLINE_ITEM_END_DATE: TDateField;
    PR_CHECK_LINESUSER_OVERRIDE: TStringField;
    PR_CHECK_LINESLineTypeSort: TStringField;
    PR_CHECK_LINESPUNCH_IN: TDateTimeField;
    PR_CHECK_LINESPUNCH_OUT: TDateTimeField;
    procedure DataModuleCreate(Sender: TObject);
    procedure PR_CHECK_LINESAfterOpen(DataSet: TDataSet);
    procedure PR_CHECK_LINESAfterPost(DataSet: TDataSet);
    procedure PR_CHECK_LINESBeforeDelete(DataSet: TDataSet);
    procedure PR_CHECK_LINESBeforeEdit(DataSet: TDataSet);
    procedure PR_CHECK_LINESBeforeInsert(DataSet: TDataSet);
    procedure PR_CHECK_LINESBeforePost(DataSet: TDataSet);
    procedure PR_CHECK_LINESCalcFields(DataSet: TDataSet);
    procedure PR_CHECK_LINESAfterCommit(DataSet: TDataSet);
    procedure PR_CHECK_LINESPrepareLookups(var Lookups: TLookupArray);

//begin gdy5
    procedure PR_CHECK_LINESCO_BRANCH_NBRChange(Sender: TField);
    procedure PR_CHECK_LINESCO_DEPARTMENT_NBRChange(Sender: TField);
    procedure PR_CHECK_LINESCO_TEAM_NBRChange(Sender: TField);
    procedure PR_CHECK_LINESCO_DIVISION_NBRChange(Sender: TField);
    procedure PR_CHECK_LINESAMOUNTValidate(Sender: TField);
    procedure PR_CHECK_LINESBeforeClose(DataSet: TDataSet);
    procedure PR_CHECK_LINESCL_E_DS_NBRChange(Sender: TField);

  public
    {IPR_CHECK_LINES_LOGGER}
    LogEvent : TOnPrCheckLinesLogMessage;
    procedure SetLogger(ev:TOnPrCheckLinesLogMessage);


    destructor Destroy; override;

    {IDBDTCache}
    procedure EnableDBDTCache;
    procedure DisableDBDTCache;
    procedure FilterDivision( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
    procedure FilterBranch( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
    procedure FilterDepartment( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);
    procedure FilterTeam( ctx: TDBDTFilterContext; DataSet: TDataSet; var Accept: boolean);

  private
    FCompany: Integer;
    FDivisions: TDBDTLookUpArray;
    FBranches: TDBDTLookUpArray;
    FDepartments: TDBDTLookUpArray;
    FTeams: TDBDTLookUpArray;
    FGoingUp: Boolean;
    FOldCLEDNbr: Integer;
    FFlipSign: Boolean;

    function LookupInArray(var lkup: TDBDTLookUpArray; key: integer; var value:integer ): boolean;overload;
    function LookupInArray(var lkup: TDBDTLookUpArray; key: TField; var value:integer ): boolean;overload;
    function PresentInArray(var lkup: TDBDTLookUpArray; key: integer ): boolean;

//end gdy5

  end;

implementation

uses
  EvConsts, SDataAccessCommon, SDM_PR_MISCELLANEOUS_CHECKS;

{$R *.DFM}

procedure TDM_PR_CHECK_LINES.DataModuleCreate(Sender: TObject);
begin
  ctx_DataAccess.NextInternalCheckLineNbr := 0;
  ctx_DataAccess.LastInternalCheckLineNbr := -1;
  ctx_DataAccess.RecalcLineBeforePost := True;
  FOldCLEDNbr := 0;
  DM_CLIENT.CL_AGENCY;
  DM_CLIENT.CL_PIECES;
  with DM_COMPANY do
  begin
    CO_DIVISION;
    CO_BRANCH;
    CO_DEPARTMENT;
    CO_TEAM;
    CO_JOBS;
    CO_WORKERS_COMP;
    CO_SHIFTS;
  end;
  DM_EMPLOYEE.EE_STATES;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESAfterOpen(DataSet: TDataSet);
begin
  ctx_PayrollCalculation.AssignTempCheckLines(TevClientDataSet(DataSet));
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESAfterPost(DataSet: TDataSet);
begin
  ctx_PayrollCalculation.AddExtraCheckLines(TevClientDataSet(DataSet));
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESBeforeDelete(DataSet: TDataSet);
begin
  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESBeforeEdit(DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
  FOldCLEDNbr := DataSet.FieldByName('CL_E_DS_NBR').AsInteger;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESBeforeInsert(DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
  FOldCLEDNbr := 0;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESBeforePost(DataSet: TDataSet);
var
  bOldVal, LookupsWereEnabled: Boolean;
  CodeType: String; 
begin
  if (DataSet.State = dsInsert) and DataSet.FieldByName('PR_NBR').IsNull then
    DataSet.FieldByName('PR_NBR').AsInteger := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;

  if ctx_DataAccess.VoidingAgencyCheck then
    Exit;

  if DataSet.FieldByName('CL_E_DS_NBR').IsNull then
    raise EUpdateError.CreateHelp('E/D code can''t be empty', IDH_ConsistencyViolation);

  bOldVal := ctx_DataAccess.Validate;
  ctx_DataAccess.Validate := False;

  if FFlipSign then
  begin
    PR_CHECK_LINES.FieldByName('AMOUNT').Value := -PR_CHECK_LINES.FieldByName('AMOUNT').Value;
    FFlipSign := False;
  end;

  LookupsWereEnabled := DM_PAYROLL.PR_CHECK.LookupsEnabled or DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled;

  if LookupsWereEnabled then
  begin
    DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;
  end;

  if not ctx_DataAccess.PayrollIsProcessing and ctx_DataAccess.RecalcLineBeforePost
  and (Dataset.FieldByName('LINE_TYPE').AsString <> CHECK_LINE_TYPE_SCHEDULED)
  and (not Dataset.FieldByName('CO_DIVISION_NBR').IsNull
  or not Dataset.FieldByName('CO_BRANCH_NBR').IsNull or not Dataset.FieldByName('CO_DEPARTMENT_NBR').IsNull
  or not Dataset.FieldByName('CO_TEAM_NBR').IsNull) and (Dataset.FieldByName('USER_OVERRIDE').Value = GROUP_BOX_NO) then
    Dataset.FieldByName('USER_OVERRIDE').Value := GROUP_BOX_YES;

  with DM_PAYROLL do
  try
    if ctx_DataAccess.NewBatchInserted and (ctx_DataAccess.NextInternalCheckLineNbr <= ctx_DataAccess.LastInternalCheckLineNbr) then
    begin
      DataSet.FieldByName('PR_CHECK_LINES_NBR').AsInteger := ctx_DataAccess.NextInternalCheckLineNbr;
      ctx_DataAccess.NextInternalCheckLineNbr := ctx_DataAccess.NextInternalCheckLineNbr + 1;
    end;

    if (PR_CHECK.FieldByName('PR_CHECK_NBR').Value = PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value) or (DataSet.State = dsInsert) then
    begin
      if (FOldCLEDNbr <> 0) and (FOldCLEDNbr <> PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger) and (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER) then
      begin
        PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := Null;
        FOldCLEDNbr := PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger;
      end;
      ctx_PayrollCalculation.CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_EMPLOYEE.EE_SCHEDULED_E_DS);
    end;

    if (PR_CHECK.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_VOID) and (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value <>
      CHECK_LINE_TYPE_SHORTFALL) then
    begin
      if PR_CHECK.FieldByName('PR_CHECK_NBR').Value <> PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value then
        PR_CHECK.Locate('PR_CHECK_NBR', PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value, []);

      DM_CLIENT.CL_E_DS.DataRequired('ALL');
      DM_COMPANY.CO.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
      if not DM_EMPLOYEE.EE.Active // RE#24496 "Open with filter" generates different format and dataset should not be reopened
      or (Copy(DM_EMPLOYEE.EE.RetrieveCondition, 1, Length('CO_NBR=')) = 'CO_NBR=') then
        DM_EMPLOYEE.EE.EnsureDSCondition('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

      if not (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD]) then
      begin
        CodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE');
        if (CodeType = ED_DED_LEVY) and (Dataset.FieldByName('LINE_TYPE').AsString <> CHECK_LINE_TYPE_SCHEDULED) then
          raise Exception.Create('Adjusting amounts for Levy code type is not allowed. Please update the scheduled E/D and refresh the check.');
      end;

      if not PR_CHECK.FieldByName('CHECK_TYPE').IsNull
      and not ((PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD])
      and (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER)) and ctx_DataAccess.RecalcLineBeforePost then
      begin
        DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired('ALL');
        DM_CLIENT.CL_PIECES.DataRequired('ALL');
        DM_COMPANY.CO_SHIFTS.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
        ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_RATES, PR.FieldByName('CO_NBR').AsInteger);
        ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_WORK_SHIFTS, PR.FieldByName('CO_NBR').AsInteger);
        ctx_PayrollCalculation.CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_E_D_GROUP_CODES, DM_CLIENT.CL_PERSON,
          DM_CLIENT.CL_PIECES, DM_COMPANY.CO_SHIFTS, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
          DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_WORK_SHIFTS, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
          DM_SYSTEM_STATE.SY_STATES,LogEvent);
      end;
    end;
    if not PR_CHECK_LINES.FieldByName('AMOUNT').IsNull then
      PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo(PR_CHECK_LINES.FieldByName('AMOUNT').Value);

    if (PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP) or (PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_BACKDATED) then
      PR_CHECK_LINES.FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_IGNORE;
  finally
    ctx_DataAccess.Validate := bOldVal;
    if LookupsWereEnabled then
    begin
      DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
    end;
  end;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESCalcFields(DataSet: TDataSet);
var
  bCheckEmployeePositioned: Boolean;
begin
  if TevClientDataSet(DataSet).LookupsEnabled then
  begin
    if PR_CHECK_LINES.UseLookupCache then
    begin
      if not PR_CHECK_LINESCO_DIVISION_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_DIVISION_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_DIVISION_NBR.AsString, DM_COMPANY.CO_DIVISION, 'CO_DIVISION_NBR', 'CUSTOM_DIVISION_NUMBER');
      if not PR_CHECK_LINESCO_BRANCH_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_BRANCH_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_BRANCH_NBR.AsString, DM_COMPANY.CO_BRANCH, 'CO_BRANCH_NBR', 'CUSTOM_BRANCH_NUMBER');
      if not PR_CHECK_LINESCO_DEPARTMENT_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_DEPARTMENT_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_DEPARTMENT_NBR.AsString, DM_COMPANY.CO_DEPARTMENT, 'CO_DEPARTMENT_NBR', 'CUSTOM_DEPARTMENT_NUMBER');
      if not PR_CHECK_LINESCO_TEAM_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_TEAM_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_TEAM_NBR.AsString, DM_COMPANY.CO_TEAM, 'CO_TEAM_NBR', 'CUSTOM_TEAM_NUMBER');
      PR_CHECK_LINESSTATE_LOOKUP.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESEE_STATES_NBR.AsString, DM_EMPLOYEE.EE_STATES, 'EE_STATES_NBR', 'STATE_LOOKUP');
      PR_CHECK_LINESSUI_STATE_LOOKUP.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESEE_SUI_STATES_NBR.AsString, DM_EMPLOYEE.EE_STATES, 'EE_STATES_NBR', 'SUI_STATE_LOOKUP');
      PR_CHECK_LINESCL_AGENCY_LOOKUP.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCL_AGENCY_NBR.AsString, DM_CLIENT.CL_AGENCY, 'CL_AGENCY_NBR', 'AGENCY_NAME');
      PR_CHECK_LINESCO_WORKERS_COMP_LOOKUP.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_WORKERS_COMP_NBR.AsString, DM_COMPANY.CO_WORKERS_COMP, 'CO_WORKERS_COMP_NBR', 'WORKERS_COMP_CODE');
      PR_CHECK_LINESCO_SHIFTS_LOOKUP.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_SHIFTS_NBR.AsString, DM_COMPANY.CO_SHIFTS, 'CO_SHIFTS_NBR', 'NAME');
      PR_CHECK_LINESCL_PIECES_LOOKUP.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCL_PIECES_NBR.AsString, DM_CLIENT.CL_PIECES, 'CL_PIECES_NBR', 'NAME');
      PR_CHECK_LINESCO_JOBS_NBR_DESC.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(PR_CHECK_LINESCO_JOBS_NBR.AsString, DM_COMPANY.CO_JOBS, 'CO_JOBS_NBR', 'DESCRIPTION');
    end
    else
    begin
      bCheckEmployeePositioned := DM_PAYROLL.PR_CHECK.Active
        and (DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'] = PR_CHECK_LINESPR_CHECK_NBR.AsInteger)
        and (DM_PAYROLL.PR_CHECK['EE_NBR'] = DM_EMPLOYEE.EE['EE_NBR']);

      if PR_CHECK_LINESCO_DIVISION_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_DIVISION_NUMBER.Clear
      else
        if bCheckEmployeePositioned
        and (PR_CHECK_LINESCO_DIVISION_NBR.AsInteger = DM_EMPLOYEE.EE['CO_DIVISION_NBR']) then
          PR_CHECK_LINESCUSTOM_DIVISION_NUMBER.Value := DM_PAYROLL.PR_CHECK['CUSTOM_DIVISION_NUMBER']
        else
        if DM_COMPANY.CO_DIVISION.Active
        and DM_COMPANY.CO_DIVISION.ShadowDataSet.CachedFindKey(PR_CHECK_LINESCO_DIVISION_NBR.AsInteger)
        and (DM_COMPANY.CO_DIVISION.ShadowDataSet['PRINT_DIV_ADDRESS_ON_CHECKS'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
          PR_CHECK_LINESCUSTOM_DIVISION_NUMBER.Value := DM_COMPANY.CO_DIVISION.ShadowDataSet['CUSTOM_DIVISION_NUMBER'];

      if PR_CHECK_LINESCO_BRANCH_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_BRANCH_NUMBER.Clear
      else
        if bCheckEmployeePositioned
        and (PR_CHECK_LINESCO_BRANCH_NBR.AsInteger = DM_EMPLOYEE.EE['CO_BRANCH_NBR']) then
          PR_CHECK_LINESCUSTOM_BRANCH_NUMBER.Value := DM_PAYROLL.PR_CHECK['CUSTOM_BRANCH_NUMBER']
        else
        if DM_COMPANY.CO_BRANCH.Active
        and DM_COMPANY.CO_BRANCH.ShadowDataSet.CachedFindKey(PR_CHECK_LINESCO_BRANCH_NBR.AsInteger)
        and (DM_COMPANY.CO_BRANCH.ShadowDataSet['PRINT_BRANCH_ADDRESS_ON_CHECK'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
          PR_CHECK_LINESCUSTOM_BRANCH_NUMBER.Value := DM_COMPANY.CO_BRANCH.ShadowDataSet['CUSTOM_BRANCH_NUMBER'];

      if PR_CHECK_LINESCO_DEPARTMENT_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_DEPARTMENT_NUMBER.Clear
      else
        if bCheckEmployeePositioned
        and (PR_CHECK_LINESCO_DEPARTMENT_NBR.AsInteger = DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR']) then
          PR_CHECK_LINESCUSTOM_DEPARTMENT_NUMBER.Value := DM_PAYROLL.PR_CHECK['CUSTOM_DEPARTMENT_NUMBER']
        else
        if DM_COMPANY.CO_DEPARTMENT.Active
        and DM_COMPANY.CO_DEPARTMENT.ShadowDataSet.CachedFindKey(PR_CHECK_LINESCO_DEPARTMENT_NBR.AsInteger)
        and (DM_COMPANY.CO_DEPARTMENT.ShadowDataSet['PRINT_DEPT_ADDRESS_ON_CHECKS'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
          PR_CHECK_LINESCUSTOM_DEPARTMENT_NUMBER.Value := DM_COMPANY.CO_DEPARTMENT.ShadowDataSet['CUSTOM_DEPARTMENT_NUMBER'];

      if PR_CHECK_LINESCO_TEAM_NBR.IsNull then
        PR_CHECK_LINESCUSTOM_TEAM_NUMBER.Clear
      else
        if bCheckEmployeePositioned
        and (PR_CHECK_LINESCO_TEAM_NBR.AsInteger = DM_EMPLOYEE.EE['CO_TEAM_NBR']) then
          PR_CHECK_LINESCUSTOM_TEAM_NUMBER.Value := DM_PAYROLL.PR_CHECK['CUSTOM_TEAM_NUMBER']
        else
        if DM_COMPANY.CO_TEAM.Active
        and DM_COMPANY.CO_TEAM.ShadowDataSet.CachedFindKey(PR_CHECK_LINESCO_TEAM_NBR.AsInteger)
        and (DM_COMPANY.CO_TEAM.ShadowDataSet['PRINT_GROUP_ADDRESS_ON_CHECK'] <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
          PR_CHECK_LINESCUSTOM_TEAM_NUMBER.Value := DM_COMPANY.CO_TEAM.ShadowDataSet['CUSTOM_TEAM_NUMBER'];

      if DM_COMPANY.CO_JOBS.Active then
        PR_CHECK_LINESCO_JOBS_NBR_DESC.AsString := DM_COMPANY.CO_JOBS.ShadowDataSet.CachedLookup(PR_CHECK_LINESCO_JOBS_NBR.AsInteger, 'DESCRIPTION');
      if DM_EMPLOYEE.EE_STATES.Active
      and DM_EMPLOYEE.EE_STATES.ShadowDataSet.CachedFindKey(PR_CHECK_LINESEE_STATES_NBR.AsInteger) then
      begin
        PR_CHECK_LINESSTATE_LOOKUP.AsString := DM_EMPLOYEE.EE_STATES.ShadowDataSet.FieldByName('STATE_LOOKUP').AsString;
      end;
      // SUI State lookup. Reso#49113. Andrew
      if DM_EMPLOYEE.EE_STATES.Active
      and DM_EMPLOYEE.EE_STATES.ShadowDataSet.CachedFindKey(PR_CHECK_LINESEE_SUI_STATES_NBR.AsInteger) then
      begin
         PR_CHECK_LINESSUI_STATE_LOOKUP.AsString := DM_EMPLOYEE.EE_STATES.ShadowDataSet.FieldByName('SUI_STATE_LOOKUP').AsString;
      end;
      if DM_CLIENT.CL_AGENCY.Active then
        PR_CHECK_LINESCL_AGENCY_LOOKUP.AsString := DM_CLIENT.CL_AGENCY.ShadowDataSet.CachedLookup(PR_CHECK_LINESCL_AGENCY_NBR.AsInteger, 'AGENCY_NAME');

      if DM_COMPANY.CO_WORKERS_COMP.Active then
        PR_CHECK_LINESCO_WORKERS_COMP_LOOKUP.AsString := DM_COMPANY.CO_WORKERS_COMP.ShadowDataSet.CachedLookup(PR_CHECK_LINESCO_WORKERS_COMP_NBR.AsInteger, 'WORKERS_COMP_CODE');

      if DM_COMPANY.CO_SHIFTS.Active then
        PR_CHECK_LINESCO_SHIFTS_LOOKUP.AsString := DM_COMPANY.CO_SHIFTS.ShadowDataSet.CachedLookup(PR_CHECK_LINESCO_SHIFTS_NBR.AsInteger, 'NAME');

      if DM_CLIENT.CL_PIECES.Active then
        PR_CHECK_LINESCL_PIECES_LOOKUP.AsString := DM_CLIENT.CL_PIECES.ShadowDataSet.CachedLookup(PR_CHECK_LINESCL_PIECES_NBR.AsInteger, 'NAME');
    end;
  end;
  if TevClientDataSet(DataSet).LookupsEnabled or (DataSet.Tag > 0) then
  begin
    if PR_CHECK_LINESPR_CHECK_LINES_NBR.AsInteger < 0 then
    begin
      PR_CHECK_LINESSorting.Value := 'Y';
      PR_CHECK_LINESSorting2.Value := -PR_CHECK_LINESPR_CHECK_LINES_NBR.AsInteger;
    end
    else
    begin
      PR_CHECK_LINESSorting.Value := 'N';
      PR_CHECK_LINESSorting2.Value := PR_CHECK_LINESPR_CHECK_LINES_NBR.AsInteger;
    end;
    if (PR_CHECK_LINESLINE_TYPE.AsString <> CHECK_LINE_TYPE_USER) and
       (PR_CHECK_LINESLINE_TYPE.AsString <> CHECK_LINE_TYPE_DISTR_USER) and
       (Trim(PR_CHECK_LINESLINE_TYPE.AsString) <> '') then
      PR_CHECK_LINESLineTypeSort.Value := CHECK_LINE_TYPE_SCHEDULED
    else
      PR_CHECK_LINESLineTypeSort.Value := CHECK_LINE_TYPE_USER;
  end;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESAfterCommit(DataSet: TDataSet);
begin
//  ctx_PayrollCalculation.AssignTempCheckLines;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESPrepareLookups(
  var Lookups: TLookupArray);
var
  iPos: Integer;
begin
  iPos := Length(Lookups);
  SetLength(Lookups, iPos + 11);
  with Lookups[iPos] do
  begin
    DataSet := DM_COMPANY.CO_DIVISION;
    sKeyField := 'CO_DIVISION_NBR';
    sLookupResultField := 'CUSTOM_DIVISION_NUMBER';
  end;
  with Lookups[iPos + 1] do
  begin
    DataSet := DM_COMPANY.CO_BRANCH;
    sKeyField := 'CO_BRANCH_NBR';
    sLookupResultField := 'CUSTOM_BRANCH_NUMBER';
  end;
  with Lookups[iPos + 2] do
  begin
    DataSet := DM_COMPANY.CO_DEPARTMENT;
    sKeyField := 'CO_DEPARTMENT_NBR';
    sLookupResultField := 'CUSTOM_DEPARTMENT_NUMBER';
  end;
  with Lookups[iPos + 3] do
  begin
    DataSet := DM_COMPANY.CO_TEAM;
    sKeyField := 'CO_TEAM_NBR';
    sLookupResultField := 'CUSTOM_TEAM_NUMBER';
  end;
  with Lookups[iPos + 4] do
  begin
    DataSet := DM_COMPANY.CO_JOBS;
    sKeyField := 'CO_JOBS_NBR';
    sLookupResultField := 'DESCRIPTION';
  end;
  with Lookups[iPos + 5] do
  begin
    DataSet := DM_EMPLOYEE.EE_STATES;
    sKeyField := 'EE_STATES_NBR';
    sLookupResultField := 'STATE_LOOKUP';
  end;
  with Lookups[iPos + 6] do
  begin
    DataSet := DM_CLIENT.CL_AGENCY;
    sKeyField := 'CL_AGENCY_NBR';
    sLookupResultField := 'AGENCY_NAME';
  end;
  with Lookups[iPos + 7] do
  begin
    DataSet := DM_COMPANY.CO_WORKERS_COMP;
    sKeyField := 'CO_WORKERS_COMP_NBR';
    sLookupResultField := 'WORKERS_COMP_CODE';
  end;
  with Lookups[iPos + 8] do
  begin
    DataSet := DM_COMPANY.CO_SHIFTS;
    sKeyField := 'CO_SHIFTS_NBR';
    sLookupResultField := 'NAME';
  end;
  with Lookups[iPos + 9] do
  begin
    DataSet := DM_CLIENT.CL_PIECES;
    sKeyField := 'CL_PIECES_NBR';
    sLookupResultField := 'NAME';
  end;
 // Reso #49113. Andrew
  with Lookups[iPos + 10] do
  begin
    DataSet := DM_EMPLOYEE.EE_STATES;
    sKeyField := 'EE_STATES_NBR';
    sLookupResultField := 'SUI_STATE_LOOKUP';
  end;
end;


// begin gdy5

destructor TDM_PR_CHECK_LINES.Destroy;
begin
  DisableDBDTCache;
  inherited;
end;

//gdy6
procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESCO_DIVISION_NBRChange(
  Sender: TField);
begin
//  ODS('PR_CHECK_LINESCO_DIVISION_NBRChange');
  if not FGoingUp then
  begin
    if not PR_CHECK_LINESCO_BRANCH_NBR.IsNull then
      PR_CHECK_LINESCO_BRANCH_NBR.Clear;
  end;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESCO_BRANCH_NBRChange( Sender: TField);
var
  val: integer;
begin
// ODS('PR_CHECK_LINESCO_BRANCH_NBRChange');
 if not FGoingUp then
 begin
   if not PR_CHECK_LINESCO_DEPARTMENT_NBR.IsNull then
     PR_CHECK_LINESCO_DEPARTMENT_NBR.Clear;
 end;

 if LookupInArray(FBranches,PR_CHECK_LINESCO_BRANCH_NBR, val ) and
   (PR_CHECK_LINESCO_DIVISION_NBR.Value <> val) then
   try
     FGoingUp := true;
     PR_CHECK_LINESCO_DIVISION_NBR.Value := val;
   finally
     FGoingUp := false;
   end;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESCO_DEPARTMENT_NBRChange( Sender: TField);
var
  val: integer;
begin
// ODS('PR_CHECK_LINESCO_DEPARTMENT_NBRChange');
 if not FGoingUp then
 begin
   if not PR_CHECK_LINESCO_TEAM_NBR.IsNull then
     PR_CHECK_LINESCO_TEAM_NBR.Clear;
 end;

 if LookupInArray(FDepartments,PR_CHECK_LINESCO_DEPARTMENT_NBR,val) and
   (PR_CHECK_LINESCO_BRANCH_NBR.Value <> val) then
   try
     FGoingUp := true;
     PR_CHECK_LINESCO_BRANCH_NBR.Value := val;
   finally
     FGoingUp := false;
   end;

end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESCO_TEAM_NBRChange( Sender: TField);
var
  val: integer;
begin
// ODS('PR_CHECK_LINESCO_TEAM_NBRChange');
 if LookupInArray(FTeams,PR_CHECK_LINESCO_TEAM_NBR,val) and
   (PR_CHECK_LINESCO_DEPARTMENT_NBR.Value <> val) then
   try
     FGoingUp := true;
     PR_CHECK_LINESCO_DEPARTMENT_NBR.Value := val;
   finally
     FGoingUp := false;
   end;
end;



function TDM_PR_CHECK_LINES.LookupInArray(var lkup: TDBDTLookUpArray;
  key: TField; var value: integer): boolean;
begin
  if key.IsNull then
    Result := false
  else
    Result := LookupInArray( lkup, key.AsInteger, value );
end;


procedure TDM_PR_CHECK_LINES.DisableDBDTCache;
begin
  FDivisions := nil;
  FBranches := nil;
  FDepartments := nil;
  FTeams := nil;
  PR_CHECK_LINES.CO_BRANCH_NBR.OnChange := nil;
  PR_CHECK_LINES.CO_DEPARTMENT_NBR.OnChange := nil;
  PR_CHECK_LINES.CO_TEAM_NBR.OnChange := nil;
  PR_CHECK_LINES.CO_DIVISION_NBR.OnChange := nil;
end;

procedure TDM_PR_CHECK_LINES.EnableDBDTCache;
var
  company: TDBDTLookUpArray;

  procedure PopulateArray( var lkup: TDBDTLookUpArray; ds:TevClientDataSet; key,value:string; var parent: TDBDTLookUpArray  );
  var
    i: integer;
    clone: TevClientDataSet;
  begin
    clone := TevClientDataSet.Create(self);
    try
      clone.CloneCursor( ds, true );
      clone.First;
      i := 0;
      SetLength(lkup,clone.RecordCount*2);
      while not clone.Eof do
      begin
        lkup[i] := clone[key];
        lkup[i+1] := clone[value];
        if PresentInArray( parent, lkup[i+1] ) then
          inc(i,2);
        clone.Next;
      end;
      SetLength(lkup,i);
    finally
      clone.Free;
    end;
  end;

begin
  DisableDBDTCache;
  FCompany := DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
  SetLength( company, 2 );
  company[0] := FCompany;
  FGoingUp := false;

  PopulateArray( FDivisions, DM_COMPANY.CO_DIVISION, 'CO_DIVISION_NBR','CO_NBR', company );
  PopulateArray( FBranches, DM_COMPANY.CO_BRANCH, 'CO_BRANCH_NBR','CO_DIVISION_NBR', FDivisions );
  PopulateArray( FDepartments, DM_COMPANY.CO_DEPARTMENT, 'CO_DEPARTMENT_NBR','CO_BRANCH_NBR', FBranches );
  PopulateArray( FTeams, DM_COMPANY.CO_TEAM, 'CO_TEAM_NBR','CO_DEPARTMENT_NBR',FDepartments);

  PR_CHECK_LINES.CO_BRANCH_NBR.OnChange := PR_CHECK_LINESCO_BRANCH_NBRChange;
  PR_CHECK_LINES.CO_DEPARTMENT_NBR.OnChange := PR_CHECK_LINESCO_DEPARTMENT_NBRChange;
  PR_CHECK_LINES.CO_TEAM_NBR.OnChange := PR_CHECK_LINESCO_TEAM_NBRChange;
  PR_CHECK_LINES.CO_DIVISION_NBR.OnChange := PR_CHECK_LINESCO_DIVISION_NBRChange;
end;



function TDM_PR_CHECK_LINES.LookupInArray(var lkup: TDBDTLookUpArray; key: integer; var value:integer ): boolean;
var
  i, len: integer;
begin
  i := 0;
  len := Length(lkup);
  Result := false;
  if assigned(lkup) then
    while i < len do
    begin
      if lkup[i] = key then begin
        value := lkup[i+1];
        Result := true;
        break;
      end;
      inc( i, 2 );
    end;
end;

function TDM_PR_CHECK_LINES.PresentInArray(var lkup: TDBDTLookUpArray;
  key: integer): boolean;
var
  dummy: integer;
begin
  Result := LookupInArray( lkup, key, dummy );
end;



procedure TDM_PR_CHECK_LINES.FilterDivision(ctx: TDBDTFilterContext;
  DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (FCompany = DataSet['CO_NBR']) and (DataSet.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
end;


procedure TDM_PR_CHECK_LINES.FilterBranch(ctx: TDBDTFilterContext;
  DataSet: TDataSet; var Accept: boolean);
var
  parent: integer;
begin
  parent := DataSet.FieldByName('CO_DIVISION_NBR').AsInteger;
  Accept := PresentInArray(FDivisions, parent) and
            (ctx.DivisionIsNull or (ctx.Division = parent)) and (DataSet.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
end;

procedure TDM_PR_CHECK_LINES.FilterDepartment(ctx: TDBDTFilterContext;
  DataSet: TDataSet; var Accept: boolean);
var
  parent,division: integer;
begin
  parent := DataSet.FieldByName('CO_BRANCH_NBR').AsInteger;
  Accept := LookupInArray( FBranches, parent, division ) and
            ( ctx.BranchIsNull or (ctx.Branch = parent) ) and (DataSet.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
  if Accept then
    Accept := ctx.DivisionIsNull or (ctx.Division = division);

end;


procedure TDM_PR_CHECK_LINES.FilterTeam(ctx: TDBDTFilterContext;
  DataSet: TDataSet; var Accept: boolean);
var
  parent, division, branch: integer;
begin

  parent := DataSet.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
  Accept := LookupInArray( FDepartments, parent, branch ) and
            ( ctx.DepartmentIsNull or (ctx.Department = parent) ) and (DataSet.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
  if Accept then
    Accept := ctx.BranchIsNull or (ctx.Branch = branch );
  if Accept then
    Accept := LookupInArray( FBranches, branch, division ) and
              (ctx.DivisionIsNull or (ctx.Division = division));

end;
//end gdy5

procedure TDM_PR_CHECK_LINES.SetLogger(ev:TOnPrCheckLinesLogMessage);
begin
     LogEvent := ev;
end;
procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESAMOUNTValidate(Sender: TField);
begin
  if ctx_DataAccess.Validate and not Sender.IsNull and (not ctx_DataAccess.RecalcLineBeforePost or (Sender.DataSet.State = dsEdit)
  or (Sender.DataSet.State = dsInsert) and (DM_PAYROLL.PR_CHECK.CHECK_TYPE.Value[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD]))
  and ((DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', Sender.DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_DED_REIMBURSEMENT) or
  (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', Sender.DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE') = ED_MEMO_COBRA_CREDIT)) then
    FFlipSign := True;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESBeforeClose(DataSet: TDataSet);
begin
  if Context <> nil then  // destruction situation
    ctx_PayrollCalculation.ReleaseTempCheckLines;
end;

procedure TDM_PR_CHECK_LINES.PR_CHECK_LINESCL_E_DS_NBRChange(
  Sender: TField);
begin
  Sender.DataSet.FieldByName('LINE_TYPE').Clear;
  Sender.DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').Clear;
end;

initialization
  RegisterClass(TDM_PR_CHECK_LINES);

finalization
  UnregisterClass(TDM_PR_CHECK_LINES);

end.
