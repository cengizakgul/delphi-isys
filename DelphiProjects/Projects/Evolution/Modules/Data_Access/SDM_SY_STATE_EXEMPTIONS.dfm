inherited DM_SY_STATE_EXEMPTIONS: TDM_SY_STATE_EXEMPTIONS
  Left = 399
  Top = 258
  Height = 424
  Width = 565
  object SY_STATE_EXEMPTIONS: TSY_STATE_EXEMPTIONS
    ProviderName = 'SY_STATE_EXEMPTIONS_PROV'
    OnCalcFields = SY_STATE_EXEMPTIONSCalcFields
    Left = 64
    Top = 40
    object SY_STATE_EXEMPTIONSEXEMPT_EMPLOYEE_SDI: TStringField
      FieldName = 'EXEMPT_EMPLOYEE_SDI'
      Size = 1
    end
    object SY_STATE_EXEMPTIONSEXEMPT_EMPLOYEE_SUI: TStringField
      FieldName = 'EXEMPT_EMPLOYEE_SUI'
      Size = 1
    end
    object SY_STATE_EXEMPTIONSEXEMPT_EMPLOYER_SDI: TStringField
      FieldName = 'EXEMPT_EMPLOYER_SDI'
      Size = 1
    end
    object SY_STATE_EXEMPTIONSEXEMPT_EMPLOYER_SUI: TStringField
      FieldName = 'EXEMPT_EMPLOYER_SUI'
      Size = 1
    end
    object SY_STATE_EXEMPTIONSEXEMPT_STATE: TStringField
      FieldName = 'EXEMPT_STATE'
      Size = 1
    end
    object SY_STATE_EXEMPTIONSE_D_CODE_TYPE: TStringField
      FieldName = 'E_D_CODE_TYPE'
      Size = 2
    end
    object SY_STATE_EXEMPTIONSSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object SY_STATE_EXEMPTIONSSY_STATE_EXEMPTIONS_NBR: TIntegerField
      FieldName = 'SY_STATE_EXEMPTIONS_NBR'
    end
    object SY_STATE_EXEMPTIONSEDDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'EDDescription'
      Size = 60
    end
  end
end
