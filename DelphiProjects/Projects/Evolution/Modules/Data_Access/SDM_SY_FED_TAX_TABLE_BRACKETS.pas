// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_FED_TAX_TABLE_BRACKETS;

interface

uses
  SDM_ddTable, SDataDictSystem,
   Db, Classes,  Forms, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses;

type
  TDM_SY_FED_TAX_TABLE_BRACKETS = class(TDM_ddTable)
    SY_FED_TAX_TABLE_BRACKETS: TSY_FED_TAX_TABLE_BRACKETS;
    SY_FED_TAX_TABLE_BRACKETSGREATER_THAN_VALUE: TFloatField;
    SY_FED_TAX_TABLE_BRACKETSLESS_THAN_VALUE: TFloatField;
    SY_FED_TAX_TABLE_BRACKETSMARITAL_STATUS: TStringField;
    SY_FED_TAX_TABLE_BRACKETSPERCENTAGE: TFloatField;
    SY_FED_TAX_TABLE_BRACKETSSY_FED_TAX_TABLE_BRACKETS_NBR: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_FED_TAX_TABLE_BRACKETS);

finalization
  UnregisterClass(TDM_SY_FED_TAX_TABLE_BRACKETS);

end.

