// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_SERVICES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_PR_SERVICES = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_SERVICES: TPR_SERVICES;
    procedure PR_SERVICESBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_SERVICES: TDM_PR_SERVICES;

implementation

uses
  SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_SERVICES.PR_SERVICESBeforeEdit(DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

initialization
  RegisterClass(TDM_PR_SERVICES);

finalization
  UnregisterClass(TDM_PR_SERVICES);

end.
