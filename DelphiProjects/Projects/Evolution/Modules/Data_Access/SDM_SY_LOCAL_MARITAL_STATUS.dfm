inherited DM_SY_LOCAL_MARITAL_STATUS: TDM_SY_LOCAL_MARITAL_STATUS
  Height = 249
  Width = 221
  object SY_LOCAL_MARITAL_STATUS: TSY_LOCAL_MARITAL_STATUS
    ProviderName = 'SY_LOCAL_MARITAL_STATUS_PROV'
    Left = 92
    Top = 29
    object SY_LOCAL_MARITAL_STATUSSY_LOCAL_MARITAL_STATUS_NBR: TIntegerField
      FieldName = 'SY_LOCAL_MARITAL_STATUS_NBR'
    end
    object SY_LOCAL_MARITAL_STATUSSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
    object SY_LOCAL_MARITAL_STATUSSY_STATE_MARITAL_STATUS_NBR: TIntegerField
      FieldName = 'SY_STATE_MARITAL_STATUS_NBR'
    end
    object SY_LOCAL_MARITAL_STATUSSTANDARD_DEDUCTION_AMOUNT: TFloatField
      FieldName = 'STANDARD_DEDUCTION_AMOUNT'
    end
    object SY_LOCAL_MARITAL_STATUSSTANDARD_EXEMPTION_ALLOW: TFloatField
      FieldName = 'STANDARD_EXEMPTION_ALLOW'
    end
    object SY_LOCAL_MARITAL_STATUSFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object SY_LOCAL_MARITAL_STATUSSTATUS_DESCRIPTION: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'STATUS_DESCRIPTION'
      LookupDataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
      LookupKeyFields = 'SY_STATE_MARITAL_STATUS_NBR'
      LookupResultField = 'STATUS_DESCRIPTION'
      KeyFields = 'SY_STATE_MARITAL_STATUS_NBR'
      Size = 40
      Lookup = True
    end
    object SY_LOCAL_MARITAL_STATUSSTATUS_TYPE: TStringField
      FieldKind = fkLookup
      FieldName = 'STATUS_TYPE'
      LookupDataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
      LookupKeyFields = 'SY_STATE_MARITAL_STATUS_NBR'
      LookupResultField = 'STATUS_TYPE'
      KeyFields = 'SY_STATE_MARITAL_STATUS_NBR'
      Size = 2
      Lookup = True
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 96
    Top = 93
  end
end
