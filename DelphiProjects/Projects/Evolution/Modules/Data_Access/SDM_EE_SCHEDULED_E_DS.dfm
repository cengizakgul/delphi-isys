inherited DM_EE_SCHEDULED_E_DS: TDM_EE_SCHEDULED_E_DS
  OldCreateOrder = True
  Left = 463
  Top = 227
  Height = 437
  Width = 592
  object EE_SCHEDULED_E_DS: TEE_SCHEDULED_E_DS
    ProviderName = 'EE_SCHEDULED_E_DS_PROV'
    FieldDefs = <
      item
        Name = 'EE_SCHEDULED_E_DS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CL_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_E_DS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'PLAN_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'MINIMUM_WAGE_MULTIPLIER'
        DataType = ftFloat
      end
      item
        Name = 'TARGET_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_WEEK_1'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_WEEK_2'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_WEEK_3'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_WEEK_4'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_WEEK_5'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EFFECTIVE_START_DATE'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'EFFECTIVE_END_DATE'
        DataType = ftDate
      end
      item
        Name = 'MINIMUM_PAY_PERIOD_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'MINIMUM_PAY_PERIOD_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'MIN_PPP_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'MAXIMUM_PAY_PERIOD_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'MAXIMUM_PAY_PERIOD_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'MAX_PPP_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'ANNUAL_MAXIMUM_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'TARGET_ACTION'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMBER_OF_TARGETS_REMAINING'
        DataType = ftInteger
      end
      item
        Name = 'WHICH_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CALCULATION_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_CHILD_SUPPORT_CASES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHILD_SUPPORT_STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'MAXIMUM_GARNISH_PERCENT'
        DataType = ftFloat
      end
      item
        Name = 'TAKE_HOME_PAY'
        DataType = ftFloat
      end
      item
        Name = 'EXPRESSION'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'ALWAYS_PAY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_DIRECT_DEPOSIT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DEDUCT_WHOLE_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BALANCE'
        DataType = ftFloat
      end
      item
        Name = 'PRIORITY_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SCHEDULED_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GARNISNMENT_ID'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'THRESHOLD_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'THRESHOLD_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BENEFIT_AMOUNT_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SCHEDULED_E_D_ENABLED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'cAMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'cPERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'CO_BENEFIT_SUBTYPE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'USE_PENSION_LIMIT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MAX_AVERAGE_HOURLY_WAGE_RATE'
        DataType = ftFloat
      end
      item
        Name = 'EE_BENEFITS_NBR'
        DataType = ftInteger
      end>
    BeforeEdit = EE_SCHEDULED_E_DSBeforeEdit
    BeforePost = EE_SCHEDULED_E_DSBeforePost
    AfterPost = EE_SCHEDULED_E_DSAfterPost
    BeforeDelete = EE_SCHEDULED_E_DSBeforeDelete
    AfterDelete = EE_SCHEDULED_E_DSAfterPost
    OnCalcFields = EE_SCHEDULED_E_DSCalcFields
    OnNewRecord = EE_SCHEDULED_E_DSNewRecord
    Left = 67
    Top = 21
    object EE_SCHEDULED_E_DSEE_SCHEDULED_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_SCHEDULED_E_DS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EE_SCHEDULED_E_DS_NBR"'
      Required = True
    end
    object EE_SCHEDULED_E_DSEE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EE_NBR"'
      Required = True
    end
    object EE_SCHEDULED_E_DSCL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_AGENCY_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."CL_AGENCY_NBR"'
    end
    object EE_SCHEDULED_E_DSCL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_DS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."CL_E_DS_NBR"'
      Required = True
    end
    object EE_SCHEDULED_E_DSCO_NBR: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CO_NBR'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CO_NBR'
      KeyFields = 'EE_NBR'
      Lookup = True
    end
    object EE_SCHEDULED_E_DSPLAN_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'PLAN_TYPE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."PLAN_TYPE"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSAMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."AMOUNT"'
      DisplayFormat = '#,##0.00####'
    end
    object EE_SCHEDULED_E_DSPERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'PERCENTAGE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."PERCENTAGE"'
      OnChange = EE_SCHEDULED_E_DSPERCENTAGEChange
      DisplayFormat = '#,##0.00####'
    end
    object EE_SCHEDULED_E_DSMINIMUM_WAGE_MULTIPLIER: TFloatField
      DisplayWidth = 10
      FieldName = 'MINIMUM_WAGE_MULTIPLIER'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MINIMUM_WAGE_MULTIPLIER"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSTARGET_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'TARGET_AMOUNT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."TARGET_AMOUNT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSFREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'FREQUENCY'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."FREQUENCY"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEXCLUDE_WEEK_1: TStringField
      DisplayWidth = 1
      FieldName = 'EXCLUDE_WEEK_1'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EXCLUDE_WEEK_1"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEXCLUDE_WEEK_2: TStringField
      DisplayWidth = 1
      FieldName = 'EXCLUDE_WEEK_2'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EXCLUDE_WEEK_2"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEXCLUDE_WEEK_3: TStringField
      DisplayWidth = 1
      FieldName = 'EXCLUDE_WEEK_3'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EXCLUDE_WEEK_3"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEXCLUDE_WEEK_4: TStringField
      DisplayWidth = 1
      FieldName = 'EXCLUDE_WEEK_4'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EXCLUDE_WEEK_4"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEXCLUDE_WEEK_5: TStringField
      DisplayWidth = 1
      FieldName = 'EXCLUDE_WEEK_5'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EXCLUDE_WEEK_5"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEFFECTIVE_START_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'EFFECTIVE_START_DATE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EFFECTIVE_START_DATE"'
      Required = True
    end
    object EE_SCHEDULED_E_DSEFFECTIVE_END_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'EFFECTIVE_END_DATE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EFFECTIVE_END_DATE"'
    end
    object EE_SCHEDULED_E_DSMINIMUM_PAY_PERIOD_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'MINIMUM_PAY_PERIOD_AMOUNT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MINIMUM_PAY_PERIOD_AMOUNT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSMINIMUM_PAY_PERIOD_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'MINIMUM_PAY_PERIOD_PERCENTAGE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MINIMUM_PAY_PERIOD_PERCENTAGE"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSMIN_PPP_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'MIN_PPP_CL_E_D_GROUPS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MIN_PPP_CL_E_D_GROUPS_NBR"'
    end
    object EE_SCHEDULED_E_DSMAXIMUM_PAY_PERIOD_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_PAY_PERIOD_AMOUNT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MAXIMUM_PAY_PERIOD_AMOUNT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSMAXIMUM_PAY_PERIOD_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_PAY_PERIOD_PERCENTAGE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MAXIMUM_PAY_PERIOD_PERCENTAGE"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSMAX_PPP_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'MAX_PPP_CL_E_D_GROUPS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MAX_PPP_CL_E_D_GROUPS_NBR"'
    end
    object EE_SCHEDULED_E_DSANNUAL_MAXIMUM_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'ANNUAL_MAXIMUM_AMOUNT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."ANNUAL_MAXIMUM_AMOUNT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSTARGET_ACTION: TStringField
      DisplayWidth = 1
      FieldName = 'TARGET_ACTION'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."TARGET_ACTION"'
      Size = 1
    end
    object EE_SCHEDULED_E_DSNUMBER_OF_TARGETS_REMAINING: TIntegerField
      DisplayWidth = 10
      FieldName = 'NUMBER_OF_TARGETS_REMAINING'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."NUMBER_OF_TARGETS_REMAINING"'
    end
    object EE_SCHEDULED_E_DSWHICH_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'WHICH_CHECKS'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."WHICH_CHECKS"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSCALCULATION_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'CALCULATION_TYPE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."CALCULATION_TYPE"'
      Size = 1
    end
    object EE_SCHEDULED_E_DSCL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_D_GROUPS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."CL_E_D_GROUPS_NBR"'
    end
    object EE_SCHEDULED_E_DSEE_CHILD_SUPPORT_CASES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_CHILD_SUPPORT_CASES_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EE_CHILD_SUPPORT_CASES_NBR"'
    end
    object EE_SCHEDULED_E_DSCHILD_SUPPORT_STATE: TStringField
      DisplayWidth = 2
      FieldName = 'CHILD_SUPPORT_STATE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."CHILD_SUPPORT_STATE"'
      Size = 2
    end
    object EE_SCHEDULED_E_DSMAXIMUM_GARNISH_PERCENT: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_GARNISH_PERCENT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."MAXIMUM_GARNISH_PERCENT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSTAKE_HOME_PAY: TFloatField
      DisplayWidth = 10
      FieldName = 'TAKE_HOME_PAY'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."TAKE_HOME_PAY"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSEXPRESSION: TBlobField
      DisplayWidth = 10
      FieldName = 'EXPRESSION'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EXPRESSION"'
      Size = 8
    end
    object EE_SCHEDULED_E_DSALWAYS_PAY: TStringField
      DisplayWidth = 1
      FieldName = 'ALWAYS_PAY'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."ALWAYS_PAY"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSEE_DIRECT_DEPOSIT_NBR: TIntegerField
      FieldName = 'EE_DIRECT_DEPOSIT_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."EE_DIRECT_DEPOSIT_NBR"'
    end
    object EE_SCHEDULED_E_DSDEDUCT_WHOLE_CHECK: TStringField
      FieldName = 'DEDUCT_WHOLE_CHECK'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."DEDUCT_WHOLE_CHECK"'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSBALANCE: TFloatField
      FieldName = 'BALANCE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."BALANCE"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'Description'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CL_E_DS_NBR'
      Size = 40
      Lookup = True
    end
    object EE_SCHEDULED_E_DSCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object EE_SCHEDULED_E_DSE_D_CODE_TYPE: TStringField
      FieldKind = fkLookup
      FieldName = 'E_D_CODE_TYPE'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'E_D_CODE_TYPE'
      KeyFields = 'CL_E_DS_NBR'
      Size = 2
      Lookup = True
    end
    object EE_SCHEDULED_E_DSSkip_Hours: TStringField
      FieldKind = fkLookup
      FieldName = 'Skip_Hours'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'SKIP_HOURS'
      KeyFields = 'CL_E_DS_NBR'
      Size = 1
      Lookup = True
    end
    object EE_SCHEDULED_E_DSPRIORITY_NUMBER: TIntegerField
      FieldName = 'PRIORITY_NUMBER'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."PRIORITY_NUMBER"'
    end
    object EE_SCHEDULED_E_DSSCHEDULED_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'SCHEDULED_E_D_GROUPS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."SCHEDULED_E_D_GROUPS_NBR"'
    end
    object EE_SCHEDULED_E_DSGARNISNMENT_ID: TStringField
      FieldName = 'GARNISNMENT_ID'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."GARNISNMENT_ID"'
    end
    object EE_SCHEDULED_E_DSTHRESHOLD_AMOUNT: TFloatField
      FieldName = 'THRESHOLD_AMOUNT'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."THRESHOLD_AMOUNT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_SCHEDULED_E_DSTHRESHOLD_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'THRESHOLD_E_D_GROUPS_NBR'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."THRESHOLD_E_D_GROUPS_NBR"'
    end
    object EE_SCHEDULED_E_DSBENEFIT_AMOUNT_TYPE: TStringField
      FieldName = 'BENEFIT_AMOUNT_TYPE'
      Origin = '"EE_SCHEDULED_E_DS_HIST"."BENEFIT_AMOUNT_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSSCHEDULED_E_D_ENABLED: TStringField
      FieldName = 'SCHEDULED_E_D_ENABLED'
      FixedChar = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSCO_BENEFIT_SUBTYPE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object EE_SCHEDULED_E_DSUSE_PENSION_LIMIT: TStringField
      FieldName = 'USE_PENSION_LIMIT'
      Required = True
      Size = 1
    end
    object EE_SCHEDULED_E_DSMAX_AVERAGE_HOURLY_WAGE_RATE: TFloatField
      FieldName = 'MAX_AVERAGE_HOURLY_WAGE_RATE'
      DisplayFormat = '#,####0.0000'
    end
    object EE_SCHEDULED_E_DSEE_BENEFITS_NBR: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object EE_SCHEDULED_E_DScAmount: TFloatField
      FieldKind = fkCalculated
      FieldName = 'cAmount'
      Calculated = True
    end
    object EE_SCHEDULED_E_DScPERCENTAGE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'cPERCENTAGE'
      Calculated = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 69
    Top = 87
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 168
    Top = 96
  end
end
