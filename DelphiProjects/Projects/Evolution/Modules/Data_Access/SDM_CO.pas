// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvConsts, SDataStructure, Variants, EvContext,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet;

type
  TDM_CO = class(TDM_ddTable)
    CO: TCO;
    COCO_NBR: TIntegerField;
    COCUSTOM_COMPANY_NUMBER: TStringField;
    CONAME: TStringField;
    CODBA: TStringField;
    COADDRESS1: TStringField;
    COADDRESS2: TStringField;
    COCITY: TStringField;
    COSTATE: TStringField;
    COZIP_CODE: TStringField;
    COCOUNTY: TStringField;
    COLEGAL_NAME: TStringField;
    COLEGAL_ADDRESS1: TStringField;
    COLEGAL_ADDRESS2: TStringField;
    COLEGAL_CITY: TStringField;
    COLEGAL_STATE: TStringField;
    COLEGAL_ZIP_CODE: TStringField;
    COE_MAIL_ADDRESS: TStringField;
    COREFERRED_BY: TStringField;
    COSB_REFERRALS_NBR: TIntegerField;
    COSTART_DATE: TDateField;
    COTERMINATION_DATE: TDateField;
    COTERMINATION_CODE: TStringField;
    COTERMINATION_NOTES: TBlobField;
    COSB_ACCOUNTANT_NBR: TIntegerField;
    COACCOUNTANT_CONTACT: TStringField;
    COPRINT_CPA: TStringField;
    COUNION_CL_E_DS_NBR: TIntegerField;
    COCL_COMMON_PAYMASTER_NBR: TIntegerField;
    COCL_CO_CONSOLIDATION_NBR: TIntegerField;
    COFEIN: TStringField;
    COREMOTE: TStringField;
    COBUSINESS_START_DATE: TDateField;
    COBUSINESS_TYPE: TStringField;
    COCORPORATION_TYPE: TStringField;
    CONATURE_OF_BUSINESS: TBlobField;
    CORESTAURANT: TStringField;
    CODAYS_OPEN: TIntegerField;
    COTIME_OPEN: TDateTimeField;
    COTIME_CLOSE: TDateTimeField;
    COCOMPANY_NOTES: TBlobField;
    COSUCCESSOR_COMPANY: TStringField;
    COMEDICAL_PLAN: TStringField;
    CORETIREMENT_AGE: TIntegerField;
    COPAY_FREQUENCIES: TStringField;
    COGENERAL_LEDGER_FORMAT_STRING: TStringField;
    COCL_BILLING_NBR: TIntegerField;
    CODBDT_LEVEL: TStringField;
    COBILLING_LEVEL: TStringField;
    COBANK_ACCOUNT_LEVEL: TStringField;
    COBILLING_SB_BANK_ACCOUNT_NBR: TIntegerField;
    COTAX_SB_BANK_ACCOUNT_NBR: TIntegerField;
    COPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CODEBIT_NUMBER_DAYS_PRIOR_PR: TIntegerField;
    COTAX_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CODEBIT_NUMBER_OF_DAYS_PRIOR_TAX: TIntegerField;
    COBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CODEBIT_NUMBER_DAYS_PRIOR_BILL: TIntegerField;
    CODD_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CODEBIT_NUMBER_OF_DAYS_PRIOR_DD: TIntegerField;
    COHOME_CO_STATES_NBR: TIntegerField;
    COHOME_SDI_CO_STATES_NBR: TIntegerField;
    COHOME_SUI_CO_STATES_NBR: TIntegerField;
    COTAX_SERVICE: TStringField;
    COTAX_SERVICE_START_DATE: TDateField;
    COTAX_SERVICE_END_DATE: TDateField;
    COUSE_DBA_ON_TAX_RETURN: TStringField;
    COWORKERS_COMP_CL_AGENCY_NBR: TIntegerField;
    COWORKERS_COMP_POLICY_ID: TStringField;
    COW_COMP_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CODEBIT_NUMBER_DAYS_PRIOR_WC: TIntegerField;
    COW_COMP_SB_BANK_ACCOUNT_NBR: TIntegerField;
    COEFTPS_ENROLLMENT_STATUS: TStringField;
    COEFTPS_ENROLLMENT_DATE: TDateTimeField;
    COEFTPS_SEQUENCE_NUMBER: TStringField;
    COEFTPS_ENROLLMENT_NUMBER: TStringField;
    COEFTPS_NAME: TStringField;
    COPIN_NUMBER: TStringField;
    COACH_SB_BANK_ACCOUNT_NBR: TIntegerField;
    COTRUST_SERVICE: TStringField;
    COTRUST_SB_ACCOUNT_NBR: TIntegerField;
    COTRUST_SERVICE_START_DATE: TDateField;
    COTRUST_SERVICE_END_DATE: TDateField;
    COOBC: TStringField;
    COSB_OBC_ACCOUNT_NBR: TIntegerField;
    COOBC_START_DATE: TDateField;
    COOBC_END_DATE: TDateField;
    COSY_FED_REPORTING_AGENCY_NBR: TIntegerField;
    COSY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField;
    COFEDERAL_TAX_DEPOSIT_FREQUENCY: TStringField;
    COFEDERAL_TAX_TRANSFER_METHOD: TStringField;
    COFEDERAL_TAX_PAYMENT_METHOD: TStringField;
    COFUI_SY_TAX_PAYMENT_AGENCY: TIntegerField;
    COFUI_SY_TAX_REPORT_AGENCY: TIntegerField;
    COEXTERNAL_TAX_EXPORT: TStringField;
    CONON_PROFIT: TStringField;
    COFEDERAL_TAX_EXEMPT_STATUS: TStringField;
    COFED_TAX_EXEMPT_EE_OASDI: TStringField;
    COFED_TAX_EXEMPT_ER_OASDI: TStringField;
    COFED_TAX_EXEMPT_EE_MEDICARE: TStringField;
    COFED_TAX_EXEMPT_ER_MEDICARE: TStringField;
    COFUI_TAX_DEPOSIT_FREQUENCY: TStringField;
    COFUI_TAX_EXEMPT: TStringField;
    COFUI_RATE_OVERRIDE: TFloatField;
    COPRIMARY_SORT_FIELD: TStringField;
    COSECONDARY_SORT_FIELD: TStringField;
    COFISCAL_YEAR_END: TIntegerField;
    COSIC_CODE: TIntegerField;
    CODEDUCTIONS_TO_ZERO: TStringField;
    COAUTOPAY_COMPANY: TStringField;
    COPAY_FREQUENCY_HOURLY_DEFAULT: TStringField;
    COPAY_FREQUENCY_SALARY_DEFAULT: TStringField;
    COCO_CHECK_PRIMARY_SORT: TStringField;
    COCO_CHECK_SECONDARY_SORT: TStringField;
    COCL_DELIVERY_GROUP_NBR: TIntegerField;
    COANNUAL_CL_DELIVERY_GROUP_NBR: TIntegerField;
    COQUARTER_CL_DELIVERY_GROUP_NBR: TIntegerField;
    COSMOKER_DEFAULT: TStringField;
    COAUTO_LABOR_DIST_SHOW_DEDUCTS: TStringField;
    COAUTO_LABOR_DIST_LEVEL: TStringField;
    CODISTRIBUTE_DEDUCTIONS_DEFAULT: TStringField;
    COCO_WORKERS_COMP_NBR: TIntegerField;
    COAUTO_INCREMENT: TFloatField;
    COMAXIMUM_GROUP_TERM_LIFE: TFloatField;
    COWITHHOLDING_DEFAULT: TStringField;
    COAPPLY_MISC_LIMIT_TO_1099: TStringField;
    COPAYRATE_PRECISION: TFloatField;
    COAVERAGE_HOURS: TFloatField;
    COCHECK_TYPE: TStringField;
    COMAXIMUM_HOURS_ON_CHECK: TFloatField;
    COMAXIMUM_DOLLARS_ON_CHECK: TFloatField;
    COMAKE_UP_TAX_DEDUCT_SHORTFALLS: TStringField;
    COSHOW_SHIFTS_ON_CHECK: TStringField;
    COSHOW_YTD_ON_CHECK: TStringField;
    COSHOW_EIN_NUMBER_ON_CHECK: TStringField;
    COSHOW_SS_NUMBER_ON_CHECK: TStringField;
    COTIME_OFF_ACCRUAL: TStringField;
    COCHECK_FORM: TStringField;
    COPRINT_MANUAL_CHECK_STUBS: TStringField;
    COCREDIT_HOLD: TStringField;
    COCL_TIMECLOCK_IMPORTS_NBR: TIntegerField;
    COPAYROLL_PASSWORD: TStringField;
    COREMOTE_OF_CLIENT: TStringField;
    COHARDWARE_O_S: TStringField;
    CONETWORK: TStringField;
    COMODEM_SPEED: TStringField;
    COMODEM_CONNECTION_TYPE: TStringField;
    CONETWORK_ADMINISTRATOR: TStringField;
    CONETWORK_ADMINISTRATOR_PHONE: TStringField;
    CONETWORK_ADMIN_PHONE_TYPE: TStringField;
    COEXTERNAL_NETWORK_ADMINISTRATOR: TStringField;
    COTRANSMISSION_DESTINATION: TIntegerField;
    COLAST_CALL_IN_DATE: TDateTimeField;
    COSETUP_COMPLETED: TStringField;
    COFIRST_MONTHLY_PAYROLL_DAY: TIntegerField;
    COSECOND_MONTHLY_PAYROLL_DAY: TIntegerField;
    COFILLER: TStringField;
    COCOLLATE_CHECKS: TStringField;
    CODISCOUNT_RATE: TFloatField;
    COMOD_RATE: TFloatField;
    COPROCESS_PRIORITY: TIntegerField;
    COCHECK_MESSAGE: TStringField;
    COCUSTOMER_SERVICE_SB_USER_NBR: TIntegerField;
    COINVOICE_NOTES: TBlobField;
    COTAX_COVER_LETTER_NOTES: TBlobField;
    COFINAL_TAX_RETURN: TStringField;
    COGENERAL_LEDGER_TAG: TStringField;
    COBILLING_GENERAL_LEDGER_TAG: TStringField;
    COFEDERAL_GENERAL_LEDGER_TAG: TStringField;
    COEE_OASDI_GENERAL_LEDGER_TAG: TStringField;
    COER_OASDI_GENERAL_LEDGER_TAG: TStringField;
    COEE_MEDICARE_GENERAL_LEDGER_TAG: TStringField;
    COER_MEDICARE_GENERAL_LEDGER_TAG: TStringField;
    COFUI_GENERAL_LEDGER_TAG: TStringField;
    COEIC_GENERAL_LEDGER_TAG: TStringField;
    COBACKUP_W_GENERAL_LEDGER_TAG: TStringField;
    CONET_PAY_GENERAL_LEDGER_TAG: TStringField;
    COTAX_IMP_GENERAL_LEDGER_TAG: TStringField;
    COTRUST_IMP_GENERAL_LEDGER_TAG: TStringField;
    COTHRD_P_TAX_GENERAL_LEDGER_TAG: TStringField;
    COTHRD_P_CHK_GENERAL_LEDGER_TAG: TStringField;
    COTRUST_CHK_GENERAL_LEDGER_TAG: TStringField;
    COFEDERAL_OFFSET_GL_TAG: TStringField;
    COEE_OASDI_OFFSET_GL_TAG: TStringField;
    COER_OASDI_OFFSET_GL_TAG: TStringField;
    COEE_MEDICARE_OFFSET_GL_TAG: TStringField;
    COER_MEDICARE_OFFSET_GL_TAG: TStringField;
    COFUI_OFFSET_GL_TAG: TStringField;
    COEIC_OFFSET_GL_TAG: TStringField;
    COBACKUP_W_OFFSET_GL_TAG: TStringField;
    COTRUST_IMP_OFFSET_GL_TAG: TStringField;
    COAUTO_ENLIST: TStringField;
    COBILLING_EXP_GL_TAG: TStringField;
    COER_OASDI_EXP_GL_TAG: TStringField;
    COER_MEDICARE_EXP_GL_TAG: TStringField;
    COCALCULATE_LOCALS_FIRST: TStringField;
    COMINIMUM_TAX_THRESHOLD: TFloatField;
    COLAST_PREPROCESS_MESSAGE: TStringField;
    COLAST_PREPROCESS: TDateTimeField;
    COLAST_FUI_CORRECTION: TDateTimeField;
    COLAST_QUARTER_END_CORRECTION: TDateTimeField;
    COLAST_SUI_CORRECTION: TDateTimeField;
    COCHARGE_COBRA_ADMIN_FEE: TStringField;
    COCOBRA_ELIGIBILITY_CONFIRM_DAYS: TIntegerField;
    COCOBRA_FEE_DAY_OF_MONTH_DUE: TIntegerField;
    COCOBRA_NOTIFICATION_DAYS: TIntegerField;
    COSS_DISABILITY_ADMIN_FEE: TFloatField;
    COSHOW_RATES_ON_CHECKS: TStringField;
    COSHOW_DIR_DEP_NBR_ON_CHECKS: TStringField;
    DM_CLIENT: TDM_CLIENT;
    COFED_943_TAX_DEPOSIT_FREQUENCY: TStringField;
    COFED_945_TAX_DEPOSIT_FREQUENCY: TStringField;
    COIMPOUND_WORKERS_COMP: TStringField;
    COLOCK_DATE: TDateField;
    COREVERSE_CHECK_PRINTING: TStringField;
    COCHECK_TIME_OFF_AVAIL: TStringField;
    COPR_CHECK_MB_GROUP_NBR: TIntegerField;
    COPR_REPORT_MB_GROUP_NBR: TIntegerField;
    COPR_REPORT_SECOND_MB_GROUP_NBR: TIntegerField;
    COTAX_CHECK_MB_GROUP_NBR: TIntegerField;
    COTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    COTAX_RETURN_MB_GROUP_NBR: TIntegerField;
    COTAX_RETURN_SECOND_MB_GROUP_NBR: TIntegerField;
    CONAME_ON_INVOICE: TStringField;
    COMISC_CHECK_FORM: TStringField;
    COAGENCY_CHECK_MB_GROUP_NBR: TIntegerField;
    COHOLD_RETURN_QUEUE: TStringField;
    CONUMBER_OF_INVOICE_COPIES: TIntegerField;
    COPRORATE_FLAT_FEE_FOR_DBDT: TStringField;
    COBREAK_CHECKS_BY_DBDT: TStringField;
    COINITIAL_EFFECTIVE_DATE: TDateField;
    COSB_OTHER_SERVICE_NBR: TIntegerField;
    COSHOW_SHORTFALL_CHECK: TStringField;
    COSUMMARIZE_SUI: TStringField;
    COSHOW_TIME_CLOCK_PUNCH: TStringField;
    COINVOICE_DISCOUNT: TFloatField;
    CODISCOUNT_START_DATE: TDateField;
    CODISCOUNT_END_DATE: TDateField;
    procedure CONewRecord(DataSet: TDataSet);
    procedure CODEBIT_NUMBER_DAYS_PRIOR_PRChange(Sender: TField);
    procedure COSY_FED_REPORTING_AGENCY_NBRChange(Sender: TField);
    procedure COSY_FED_TAX_PAYMENT_AGENCY_NBRChange(Sender: TField);
    procedure COAnyBank_Account_NbrChange(Sender: TField);
    procedure COFEDERAL_TAX_EXEMPT_STATUSValidate(Sender: TField);
  private
  public
  end;

implementation

uses evutils;
{$R *.DFM}

procedure TDM_CO.CONewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('INITIAL_EFFECTIVE_DATE').AsDateTime := BeginOfTime;
  DataSet.FieldByName('TAX_SERVICE').AsString := GROUP_BOX_YES;
  DataSet.FieldByName('OBC').AsString := GROUP_BOX_NO;
  DataSet.FieldByName('APPLY_MISC_LIMIT_TO_1099').AsString := GROUP_BOX_YES;
  if not DM_CLIENT.CL_BILLING.Active then
    DM_CLIENT.CL_BILLING.DataRequired('' );
  if (TevClientDataSet(DM_CLIENT.CL_BILLING).State = dsBrowse) and
      DSConditionsEqual( DM_CLIENT.CL_BILLING.RetrieveCondition, '' ) then
  begin
    if DM_CLIENT.CL_BILLING.RecordCount = 1 then
      COCL_BILLING_NBR.Value := DM_CLIENT.CL_BILLING['CL_BILLING_NBR'];
  end;

  if not DM_SERVICE_BUREAU.SB.Active then
    DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  DataSet.FieldValues['ENFORCE_EE_DOB'] := DM_SERVICE_BUREAU.SB.FieldValues['ENFORCE_EE_DOB_DEFAULT'];

end;

procedure TDM_CO.CODEBIT_NUMBER_DAYS_PRIOR_PRChange(Sender: TField);
begin
  with TField(Sender) do
    with DataSet do
      if State in [dsInsert] then
      begin
        FieldByName('DEBIT_NUMBER_OF_DAYS_PRIOR_TAX').AsInteger := Value;
        FieldByName('DEBIT_NUMBER_OF_DAYS_PRIOR_DD').AsInteger := Value;
        FieldByName('DEBIT_NUMBER_DAYS_PRIOR_BILL').AsInteger := Value;
        FieldByName('DEBIT_NUMBER_DAYS_PRIOR_WC').AsInteger := Value;
      end;
end;

procedure TDM_CO.COSY_FED_REPORTING_AGENCY_NBRChange(Sender: TField);
begin
  if COFUI_SY_TAX_REPORT_AGENCY.IsNull then
    COFUI_SY_TAX_REPORT_AGENCY.Value := COSY_FED_REPORTING_AGENCY_NBR.Value;
end;

procedure TDM_CO.COSY_FED_TAX_PAYMENT_AGENCY_NBRChange(Sender: TField);
begin
  if COFUI_SY_TAX_PAYMENT_AGENCY.IsNull then
    COFUI_SY_TAX_PAYMENT_AGENCY.Value := COSY_FED_TAX_PAYMENT_AGENCY_NBR.Value;
end;

procedure TDM_CO.COAnyBank_Account_NbrChange(Sender: TField);
const
  accounts: array [0..4] of string = (
      'PAYROLL_CL_BANK_ACCOUNT_NBR',
      'TAX_CL_BANK_ACCOUNT_NBR',
      'DD_CL_BANK_ACCOUNT_NBR',
      'BILLING_CL_BANK_ACCOUNT_NBR',
      'W_COMP_CL_BANK_ACCOUNT_NBR'
  );
var
  i: integer;

  function IsAllOtherNull: boolean;
  var
    i: integer;
  begin
    Result := true;
    for i := low(accounts) to high(accounts) do
      if ( accounts[i] <> Sender.FieldName ) and not CO.fieldByName( accounts[i] ).IsNull then
      begin
        Result := false;
        break;
      end;
  end;
begin
  if IsAllOtherNull and (Sender.Value <> null) then
    for i := low(accounts) to high(accounts) do
      if accounts[i] <> Sender.FieldName then
        CO.fieldByName( accounts[i] ).Value := Sender.Value;
end;


procedure TDM_CO.COFEDERAL_TAX_EXEMPT_STATUSValidate(Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

initialization
  RegisterClass(TDM_CO);

finalization
  UnregisterClass(TDM_CO);

end.


