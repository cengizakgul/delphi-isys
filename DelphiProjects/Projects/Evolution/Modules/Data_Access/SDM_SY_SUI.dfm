inherited DM_SY_SUI: TDM_SY_SUI
  OldCreateOrder = True
  Left = 704
  Top = 272
  Height = 479
  Width = 741
  object SY_SUI: TSY_SUI
    ProviderName = 'SY_SUI_PROV'
    Left = 120
    Top = 56
    object SY_SUINEW_COMPANY_DEFAULT_RATE: TFloatField
      FieldName = 'NEW_COMPANY_DEFAULT_RATE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_SUIGLOBAL_RATE: TFloatField
      FieldName = 'GLOBAL_RATE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_SUIFUTURE_DEFAULT_RATE: TFloatField
      FieldName = 'FUTURE_DEFAULT_RATE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_SUIMAXIMUM_WAGE: TFloatField
      FieldName = 'MAXIMUM_WAGE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_SUIFUTURE_MAXIMUM_WAGE: TFloatField
      FieldName = 'FUTURE_MAXIMUM_WAGE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_SUIALTERNATE_TAXABLE_WAGE_BASE: TFloatField
      FieldName = 'ALTERNATE_TAXABLE_WAGE_BASE'
      DisplayFormat = '#,##0.00##'
    end
  end
end
