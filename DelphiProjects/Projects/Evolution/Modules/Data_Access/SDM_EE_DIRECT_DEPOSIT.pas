// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_DIRECT_DEPOSIT;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  evUtils, evTypes, EvClientDataSet;


type
  TDM_EE_DIRECT_DEPOSIT = class(TDM_ddTable)
    EE_DIRECT_DEPOSIT: TEE_DIRECT_DEPOSIT;
    DM_COMPANY: TDM_COMPANY;
    procedure EE_DIRECT_DEPOSITBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



implementation

{$R *.DFM}

procedure TDM_EE_DIRECT_DEPOSIT.EE_DIRECT_DEPOSITBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  DataSet.UpdateRecord;
  if DataSet.FieldByName('IN_PRENOTE_DATE').IsNull then
    DataSet.FieldByName('IN_PRENOTE_DATE').Value := Now;

  if (DataSet.FieldByName('IN_PRENOTE').AsString = 'Y')
  and (DataSet.FieldByName('IN_PRENOTE').OldValue = 'N') then
    DataSet.FieldByName('IN_PRENOTE_DATE').Value := Now;

  ValidateABANumber(DataSet['EE_ABA_NUMBER']);
end;

initialization
  RegisterClass( TDM_EE_DIRECT_DEPOSIT );

finalization
  UnregisterClass( TDM_EE_DIRECT_DEPOSIT );

end.
