inherited DM_CO_REPORTS: TDM_CO_REPORTS
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 602
  Top = 283
  Height = 540
  Width = 783
  object CO_REPORTS: TCO_REPORTS
    ProviderName = 'CO_REPORTS_PROV'
    FieldDefs = <
      item
        Name = 'RWDescription'
        DataType = ftString
        Size = 8000
      end
      item
        Name = 'CO_REPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'REPORT_WRITER_REPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'REPORT_LEVEL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RUN_PARAMS'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'INPUT_PARAMS'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'OVERRIDE_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'ndescription'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Priority'
        DataType = ftInteger
      end>
    OnCalcFields = CO_REPORTSCalcFields
    BlobsLoadMode = blmPreload
    Left = 32
    Top = 24
    object CO_REPORTSRWDescription: TStringField
      DisplayWidth = 8000
      FieldKind = fkInternalCalc
      FieldName = 'RWDescription'
      Size = 8000
    end
    object CO_REPORTSCO_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_REPORTS_NBR'
    end
    object CO_REPORTSCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object CO_REPORTSCUSTOM_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'CUSTOM_NAME'
      Size = 40
    end
    object CO_REPORTSREPORT_WRITER_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'REPORT_WRITER_REPORTS_NBR'
    end
    object CO_REPORTSREPORT_LEVEL: TStringField
      DisplayWidth = 1
      FieldName = 'REPORT_LEVEL'
      FixedChar = True
      Size = 1
    end
    object CO_REPORTSRUN_PARAMS: TBlobField
      DisplayWidth = 10
      FieldName = 'RUN_PARAMS'
      Size = 8
    end
    object CO_REPORTSINPUT_PARAMS2: TBlobField
      DisplayWidth = 10
      FieldName = 'INPUT_PARAMS'
      Size = 8
    end
    object CO_REPORTSOVERRIDE_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_MB_GROUP_NBR'
    end
    object CO_REPORTSndescription: TStringField
      DisplayWidth = 60
      FieldKind = fkInternalCalc
      FieldName = 'ndescription'
      Size = 60
    end
    object CO_REPORTSPriority: TIntegerField
      DisplayWidth = 10
      FieldKind = fkInternalCalc
      FieldName = 'Priority'
    end
    object CO_REPORTSHideForRemotes: TStringField
      DisplayWidth = 1
      FieldKind = fkCalculated
      FieldName = 'HideForRemotes'
      Size = 1
      Calculated = True
    end
    object CO_REPORTSFAVORITE: TStringField
      DisplayLabel = 'Favorite'
      DisplayWidth = 5
      FieldName = 'FAVORITE'
      FixedChar = True
      Size = 1
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 272
    Top = 112
  end
end
