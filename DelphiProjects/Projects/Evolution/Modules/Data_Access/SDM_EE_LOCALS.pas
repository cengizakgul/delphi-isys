// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  SDataStructure, Db,   EvUtils, EvConsts, EvTypes,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDataDicttemp, EvExceptions, SFieldCodeValues, EvContext, EvUIComponents, EvClientDataSet;

type
  TDM_EE_LOCALS = class(TDM_ddTable)
    EE_LOCALS: TEE_LOCALS;
    EE_LOCALSEE_LOCALS_NBR: TIntegerField;
    EE_LOCALSEE_NBR: TIntegerField;
    EE_LOCALSCO_LOCAL_TAX_NBR: TIntegerField;
    EE_LOCALSEXEMPT_EXCLUDE: TStringField;
    EE_LOCALSMISCELLANEOUS_AMOUNT: TFloatField;
    EE_LOCALSFILLER: TStringField;
    EE_LOCALSLocal_Name_Lookup: TStringField;
    EE_LOCALSEE_COUNTY: TStringField;
    EE_LOCALSEE_TAX_CODE: TStringField;
    DM_COMPANY: TDM_COMPANY;
    DM_CLIENT: TDM_CLIENT;
    EE_LOCALSPERCENTAGE_OF_TAX_WAGES: TFloatField;
    EE_LOCALSDEDUCT: TStringField;
    EE_LOCALSLOCAL_ENABLED: TStringField;
    EE_LOCALSOVERRIDE_LOCAL_TAX_TYPE: TStringField;
    EE_LOCALSOVERRIDE_LOCAL_TAX_VALUE: TFloatField;
    EE_LOCALSLOCAL_TYPE: TStringField;
    EE_LOCALSTaxTypeDesc: TStringField;
    EE_LOCALSSY_STATES_NBR: TIntegerField;
    EE_LOCALSLOCALTYPEDESC: TStringField;
    EE_LOCALSCO_LOCATIONS_ACCOUNTNUMBER: TStringField;
    EE_LOCALSState: TStringField;
    procedure EE_LOCALSBeforeDelete(DataSet: TDataSet);
    procedure EE_LOCALSNewRecord(DataSet: TDataSet);
    procedure EE_LOCALSBeforePost(DataSet: TDataSet);
    procedure EE_LOCALSEXEMPT_EXCLUDEValidate(Sender: TField);
    procedure EE_LOCALSCO_LOCAL_TAX_NBRChange(Sender: TField);
    procedure EE_LOCALSCalcFields(DataSet: TDataSet);//gdy
  private
    { Private declarations }
    procedure CheckState;
  public
    { Public declarations }
  end;

implementation

uses SDataDictsystem;

{$R *.DFM}





procedure TDM_EE_LOCALS.EE_LOCALSBeforeDelete(DataSet: TDataSet);
begin
  DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('EE_LOCALS_NBR=' + IntToStr(DataSet['EE_LOCALS_NBR']));
  if DM_PAYROLL.PR_CHECK_LOCALS.RecordCount > 0 then
    raise EUpdateError.CreateHelp('This local has WAGES attached and can not be delete!', IDH_ConsistencyViolation);
end;

procedure TDM_EE_LOCALS.EE_LOCALSNewRecord(DataSet: TDataSet);
begin
  if DM_EMPLOYEE.EE.FieldByName('COMPANY_OR_INDIVIDUAL_NAME').AsString = GROUP_BOX_COMPANY then //if EE marked 1099
    DataSet.FindField('EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT //then EE_LOCALS->Exempt
end;

procedure TDM_EE_LOCALS.EE_LOCALSBeforePost(DataSet: TDataSet);
begin

  if not ctx_DataAccess.Importing and not ctx_DataAccess.Copying and (DataSet.State in [dsInsert, dsEdit])  then
  begin


{  //   ROLLBACK all change, because Des wanted..

    if (EE_LOCALS.OVERRIDE_LOCAL_TAX_TYPE.AsString = OVERRIDE_VALUE_TYPE_NONE) then
      begin
        EE_LOCALSOVERRIDE_LOCAL_TAX_Gui_Value.Clear;
        EE_LOCALS.OVERRIDE_LOCAL_TAX_VALUE.Clear;
      end;

    if ((EE_LOCALS.OVERRIDE_LOCAL_TAX_TYPE.AsString = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT) or
       (EE_LOCALS.OVERRIDE_LOCAL_TAX_TYPE.AsString = OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT)) then
        begin
         if not EE_LOCALSOVERRIDE_LOCAL_TAX_Gui_Value.IsNull then
          EE_LOCALS.OVERRIDE_LOCAL_TAX_VALUE.AsVariant := EE_LOCALSOVERRIDE_LOCAL_TAX_Gui_Value.Value  / 100
        end
      else
        if not EE_LOCALSOVERRIDE_LOCAL_TAX_Gui_Value.IsNull then
         EE_LOCALS.OVERRIDE_LOCAL_TAX_VALUE.AsVariant := EE_LOCALSOVERRIDE_LOCAL_TAX_Gui_Value.Value;
}


    if (EE_LOCALS.OVERRIDE_LOCAL_TAX_TYPE.AsString <> OVERRIDE_VALUE_TYPE_NONE) and
       EE_LOCALS.OVERRIDE_LOCAL_TAX_VALUE.IsNull then
      raise EUpdateError.CreateHelp('You must enter local override value', IDH_ConsistencyViolation);

   end;
  if not ctx_DataAccess.Importing and not ctx_DataAccess.Copying then
    CheckState;

end;

procedure TDM_EE_LOCALS.CheckState;
var
  V: Variant;
  state: integer;
  costate: integer;
  StatesClone: TEvClientDataSet;
  Found: boolean;

  procedure AddState( costate: integer );
  const
    FieldsToCopy = 'STATE_EXEMPT_EXCLUDE;OVERRIDE_STATE_TAX_TYPE;EE_SDI_EXEMPT_EXCLUDE;'+
     'ER_SDI_EXEMPT_EXCLUDE;EE_SUI_EXEMPT_EXCLUDE;ER_SUI_EXEMPT_EXCLUDE;RECIPROCAL_METHOD;'+
     'IMPORTED_MARITAL_STATUS;SUI_APPLY_CO_STATES_NBR;SDI_APPLY_CO_STATES_NBR';
  var
    oldIsCopying: boolean;
  begin
    if not DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').IsNull and
       StatesClone.Locate( 'EE_STATES_NBR', DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsString,[]) then
    begin
      oldIsCopying := ctx_DataAccess.Copying;
      ctx_DataAccess.Copying := true;
      try
        DM_EMPLOYEE.EE_STATES.Insert;
        DM_EMPLOYEE.EE_STATES['CO_STATES_NBR'] := costate;
        DM_EMPLOYEE.EE_STATES['STATE_MARITAL_STATUS'] :=  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS['STATUS_TYPE'];
        DM_EMPLOYEE.EE_STATES['SY_STATE_MARITAL_STATUS_NBR'] :=  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS['SY_STATE_MARITAL_STATUS_NBR'];
        DM_EMPLOYEE.EE_STATES[FieldsToCopy] := StatesClone[FieldsToCopy];
        DM_EMPLOYEE.EE_STATES.Post;
        ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_STATES,DM_EMPLOYEE.EE]);
      finally
        ctx_DataAccess.Copying := oldIsCopying;
      end;
    end;
  end;
begin
  Found := false;
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  V := DM_COMPANY.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', EE_LOCALSCO_LOCAL_TAX_NBR.Value, 'SY_STATES_NBR' );
  if not VarIsNull(V) then
  begin
    state := V;
    StatesClone := TEvClientDataSet.Create( nil );
    try
      if not DM_EMPLOYEE.EE_STATES.Active or (UpperCase(StringReplace(DM_EMPLOYEE.EE_STATES.RetrieveCondition, ' ', '', [rfReplaceAll])) <> 'EE_NBR=' + EE_LOCALSEE_NBR.AsString) then
        DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR=' + EE_LOCALSEE_NBR.AsString);
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      StatesClone.CloneCursor( DM_EMPLOYEE.EE_STATES, false, false );
      StatesClone.First;
      while not StatesClone.Eof do
      begin
        V := DM_COMPANY.CO_STATES.Lookup( 'CO_STATES_NBR', StatesClone.FieldByName('CO_STATES_NBR').AsString, 'SY_STATES_NBR');
        if not VarIsNull( V ) and ( V = state ) then
        begin
          Found := true;
          break;
        end;
        StatesClone.Next;
      end;
      if not Found then
      begin
        V := DM_COMPANY.CO_STATES.Lookup( 'SY_STATES_NBR', state, 'CO_STATES_NBR' );
        if not VarIsNull(V) then
        begin
          costate := V;
          DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.DataRequired('ALL');
          DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition := 'SY_STATES_NBR=' + IntToStr(state);
          AddState( costate );
        end;
      end
    finally
      StatesClone.Free;
    end;
  end;
end;

procedure TDM_EE_LOCALS.EE_LOCALSEXEMPT_EXCLUDEValidate(Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

procedure TDM_EE_LOCALS.EE_LOCALSCO_LOCAL_TAX_NBRChange(Sender: TField);
var cotax: TevShadowClientDataSet;
begin
  inherited;

  if not ctx_DataAccess.Importing and not ctx_DataAccess.Copying  then
  begin
    cotax := DM_COMPANY.CO_LOCAL_TAX.ShadowDataSet;
    cotax.Locate('CO_LOCAL_TAX_NBR',EE_LOCALS.CO_LOCAL_TAX_NBR.Value,[]);
    if EE_LOCALS.FILLER.IsNull then
       EE_LOCALS.FILLER.AsVariant := cotax.FieldByName('FILLER').Value;
    if EE_LOCALS.EE_TAX_CODE.IsNull then
       EE_LOCALS.EE_TAX_CODE.AsVariant := cotax.FieldByName('TAX_RETURN_CODE').Value;
  end;
end;

procedure TDM_EE_LOCALS.EE_LOCALSCalcFields(DataSet: TDataSet);
begin
  inherited;

  if not DM_COMPANY.CO_LOCAL_TAX.Active then
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
  if not DM_SYSTEM_LOCAL.SY_LOCALS.Active then
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
  if not DM_SYSTEM_STATE.SY_STATES.Active then
      DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');

  EE_LOCALSLOCAL_TYPE.AsString := DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.CachedLookup(StrToIntDef(DM_COMPANY.CO_LOCAL_TAX.ShadowDataSet.CachedLookup(EE_LOCALSCO_LOCAL_TAX_NBR.AsInteger,'SY_LOCALS_NBR'),0) ,'LOCAL_TYPE');
  EE_LOCALSLOCALTYPEDESC.AsString := ReturnDescription(LocalType_ComboChoices,  EE_LOCALSLOCAL_TYPE.AsString);

  EE_LOCALSSY_STATES_NBR.AsString := DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.CachedLookup(StrToIntDef(DM_COMPANY.CO_LOCAL_TAX.ShadowDataSet.CachedLookup(EE_LOCALSCO_LOCAL_TAX_NBR.AsInteger,'SY_LOCALS_NBR'),0) ,'SY_STATES_NBR');
  if not EE_LOCALSSY_STATES_NBR.IsNull then
      EE_LOCALSState.AsString := DM_SYSTEM_STATE.SY_STATES.ShadowDataSet.CachedLookup(EE_LOCALSSY_STATES_NBR.AsInteger,'STATE');

  EE_LOCALSTaxTypeDesc.AsString := ReturnDescription(OverrideValueType_ComboChoices, EE_LOCALSOVERRIDE_LOCAL_TAX_TYPE.AsString);
end;

initialization
  RegisterClass(TDM_EE_LOCALS);

finalization
  UnregisterClass(TDM_EE_LOCALS);

end.
