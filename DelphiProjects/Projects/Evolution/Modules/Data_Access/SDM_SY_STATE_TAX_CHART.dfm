inherited DM_SY_STATE_TAX_CHART: TDM_SY_STATE_TAX_CHART
  Left = 460
  Top = 185
  Height = 242
  Width = 413
  object SY_STATE_TAX_CHART: TSY_STATE_TAX_CHART
    ProviderName = 'SY_STATE_TAX_CHART_PROV'
    PictureMasks.Strings = (
      
        'MINIMUM'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]])' +
        ',[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9 +
        'T'#9'F'
      
        'MAXIMUM'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]])' +
        ',[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9 +
        'T'#9'F'
      
        'PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]' +
        ']}'#9'T'#9'F'
      
        'NEXT_MINIMUM'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]}'#9'T'#9'F'
      
        'NEXT_MAXIMUM'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]}'#9'T'#9'F'
      
        'NEXT_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F')
    Left = 73
    Top = 19
    object SY_STATE_TAX_CHARTMARITAL_STATUS_DESCRPTION: TStringField
      DisplayLabel = 'MARITAL_STATUS'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'MARITAL_STATUS_DESCRPTION'
      LookupDataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
      LookupKeyFields = 'SY_STATE_MARITAL_STATUS_NBR'
      LookupResultField = 'STATUS_DESCRIPTION'
      KeyFields = 'SY_STATE_MARITAL_STATUS_NBR'
      Size = 40
      Lookup = True
    end
    object SY_STATE_TAX_CHARTMINIMUM: TFloatField
      DisplayWidth = 10
      FieldName = 'MINIMUM'
    end
    object SY_STATE_TAX_CHARTMAXIMUM: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM'
    end
    object SY_STATE_TAX_CHARTPERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'PERCENTAGE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_STATE_TAX_CHARTSY_STATE_MARITAL_STATUS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATE_MARITAL_STATUS_NBR'
      Visible = False
    end
    object SY_STATE_TAX_CHARTSY_STATE_TAX_CHART_NBR: TIntegerField
      FieldName = 'SY_STATE_TAX_CHART_NBR'
      Visible = False
    end
    object SY_STATE_TAX_CHARTSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
      Visible = False
    end
    object SY_STATE_TAX_CHARTTYPE: TStringField
      FieldName = 'ENTRY_TYPE'
      Size = 1
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 69
    Top = 81
  end
end
