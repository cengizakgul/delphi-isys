// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_STATE_TAX_LIABILITIES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  EvTypes, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvExceptions,
  EvContext, EvClientDataSet;

type
  TDM_CO_STATE_TAX_LIABILITIES = class(TDM_ddTable)
    CO_STATE_TAX_LIABILITIES: TCO_STATE_TAX_LIABILITIES;
    CO_STATE_TAX_LIABILITIESTYPE: TStringField;
    CO_STATE_TAX_LIABILITIESAMOUNT: TFloatField;
    CO_STATE_TAX_LIABILITIESCO_STATE_TAX_LIABILITIES_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESCO_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESCO_STATES_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESDUE_DATE: TDateField;
    CO_STATE_TAX_LIABILITIESSTATUS: TStringField;
    CO_STATE_TAX_LIABILITIESSTATUS_DATE: TDateTimeField;
    CO_STATE_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField;
    CO_STATE_TAX_LIABILITIESPR_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESNOTES: TBlobField;
    CO_STATE_TAX_LIABILITIESFILLER: TStringField;
    CO_STATE_TAX_LIABILITIESState: TStringField;
    CO_STATE_TAX_LIABILITIESTHIRD_PARTY: TStringField;
    CO_STATE_TAX_LIABILITIESCHECK_DATE: TDateField;
    DM_COMPANY: TDM_COMPANY;
    CO_STATE_TAX_LIABILITIESFIELD_OFFICE_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESACH_KEY: TStringField;
    CO_STATE_TAX_LIABILITIESIMPOUNDED: TStringField;
    CO_STATE_TAX_LIABILITIESSY_STATE_DEPOSIT_FREQ_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESTAX_COUPON_SY_REPORTS_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESPERIOD_BEGIN_DATE: TDateField;
    CO_STATE_TAX_LIABILITIESPERIOD_END_DATE: TDateField;
    CO_STATE_TAX_LIABILITIESADJ_PERIOD_END_DATE: TDateField;
    CO_STATE_TAX_LIABILITIEScSY_STATE_DEPOSIT_FREQ_NBR: TIntegerField;
    CO_STATE_TAX_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField;
    procedure CO_STATE_TAX_LIABILITIESNewRecord(DataSet: TDataSet);
    procedure CO_STATE_TAX_LIABILITIESBeforePost(DataSet: TDataSet);
    procedure CO_STATE_TAX_LIABILITIESBeforeEdit(DataSet: TDataSet);
    procedure CO_STATE_TAX_LIABILITIESBeforeDelete(DataSet: TDataSet);
    procedure CO_STATE_TAX_LIABILITIESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIESNewRecord(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('CO_NBR').Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
end;

procedure TDM_CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIESBeforePost(
  DataSet: TDataSet);
begin

 //   Added By CA..

  if DataSet.FieldByName('AMOUNT').IsNull then
     raise EUpdateError.CreateHelp('Amount is a required field', IDH_ConsistencyViolation);


  if DataSet.FieldByName('CHECK_DATE').IsNull then
    raise EUpdateError.CreateHelp('Check date is a required field', IDH_ConsistencyViolation);
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;

end;

procedure TDM_CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIESBeforeEdit(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIESBeforeDelete(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIESCalcFields(
  DataSet: TDataSet);
var
 s : String;
begin
  inherited;


  if not DataSet.FieldByName('FILLER').IsNull then
  begin
    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 12, 8);
    if trim(s) <> '' then
      Dataset.FieldByName('cSY_STATE_DEPOSIT_FREQ_NBR').AsString := s;
  end;

  s := ExtractFromFiller(Dataset.fieldByName('FILLER').AsString , 41, 10);
  if (s <> '') then
     Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := StrToDate(s)
  else
    if not Dataset.FieldByName('PERIOD_END_DATE').IsNull then
       Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := Dataset.FieldByName('PERIOD_END_DATE').AsDateTime;

end;

initialization
  RegisterClass(TDM_CO_STATE_TAX_LIABILITIES);

finalization
  UnregisterClass(TDM_CO_STATE_TAX_LIABILITIES);

end.
