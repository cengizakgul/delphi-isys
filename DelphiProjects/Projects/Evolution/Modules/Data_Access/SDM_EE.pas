// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  Db,   SDataStructure, EvUIUtils, EvConsts, SFieldCodeValues,
  SDDClasses, SDataDictClient, SDM_ddTable, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, ISDataAccessComponents, EvDataAccessComponents, EvBasicUtils,
  SDataDicttemp, EvExceptions,EvCommonInterfaces,EvDataSet, EvUtils, EvUIComponents, EvClientDataSet;

type
  TDM_EE = class(TDM_ddTable)
    EE: TEE;
    EEEmployee_Name_Calculate: TStringField;
    EEEE_NBR: TIntegerField;
    EESOCIAL_SECURITY: TStringField;
    EECL_PERSON_NBR: TIntegerField;
    EECO_NBR: TIntegerField;
    EECO_DIVISION_NBR: TIntegerField;
    EECO_BRANCH_NBR: TIntegerField;
    EECO_DEPARTMENT_NBR: TIntegerField;
    EECO_TEAM_NBR: TIntegerField;
    EECUSTOM_EMPLOYEE_NUMBER: TStringField;
    EECO_HR_APPLICANT_NBR: TIntegerField;
    EECO_HR_RECRUITERS_NBR: TIntegerField;
    EECO_HR_REFERRALS_NBR: TIntegerField;
    EETIME_CLOCK_NUMBER: TStringField;
    EEORIGINAL_HIRE_DATE: TDateField;
    EECURRENT_HIRE_DATE: TDateField;
    EENEW_HIRE_REPORT_SENT: TStringField;
    EECURRENT_TERMINATION_DATE: TDateField;
    EECURRENT_TERMINATION_CODE: TStringField;
    EECO_HR_SUPERVISORS_NBR: TIntegerField;
    EESALARY_AMOUNT: TFloatField;
    EESTANDARD_HOURS: TFloatField;
    EEAUTOPAY_CO_SHIFTS_NBR: TIntegerField;
    EECO_HR_POSITIONS_NBR: TIntegerField;
    EEPOSITION_EFFECTIVE_DATE: TDateField;
    EEPOSITION_STATUS: TStringField;
    EECO_HR_PERFORMANCE_RATINGS_NBR: TIntegerField;
    EEREVIEW_DATE: TDateField;
    EENEXT_REVIEW_DATE: TDateField;
    EEPAY_FREQUENCY: TStringField;
    EENEXT_RAISE_DATE: TDateField;
    EENEXT_RAISE_AMOUNT: TFloatField;
    EENEXT_RAISE_PERCENTAGE: TFloatField;
    EENEXT_RAISE_RATE: TFloatField;
    EENEXT_PAY_FREQUENCY: TStringField;
    EETIPPED_DIRECTLY: TStringField;
    EEALD_CL_E_D_GROUPS_NBR: TIntegerField;
    EEDISTRIBUTE_TAXES: TStringField;
    EECO_PAY_GROUP_NBR: TIntegerField;
    EECL_DELIVERY_GROUP_NBR: TIntegerField;
    EEWORKERS_COMP_WAGE_LIMIT: TFloatField;
    EEGROUP_TERM_POLICY_AMOUNT: TFloatField;
    EECO_UNIONS_NBR: TIntegerField;
    EEEIC: TStringField;
    EEFLSA_EXEMPT: TStringField;
    EEW2_TYPE: TStringField;
    EEW2_PENSION: TStringField;
    EEW2_DEFERRED_COMP: TStringField;
    EEW2_DECEASED: TStringField;
    EEW2_STATUTORY_EMPLOYEE: TStringField;
    EEW2_LEGAL_REP: TStringField;
    EEHOME_TAX_EE_STATES_NBR: TIntegerField;
    EEEXEMPT_EMPLOYEE_OASDI: TStringField;
    EEEXEMPT_EMPLOYEE_MEDICARE: TStringField;
    EEEXEMPT_EXCLUDE_EE_FED: TStringField;
    EEEXEMPT_EMPLOYER_OASDI: TStringField;
    EEEXEMPT_EMPLOYER_MEDICARE: TStringField;
    EEEXEMPT_EMPLOYER_FUI: TStringField;
    EEOVERRIDE_FED_TAX_VALUE: TFloatField;
    EEOVERRIDE_FED_TAX_TYPE: TStringField;
    EEGOV_GARNISH_PRIOR_CHILD_SUPPT: TStringField;
    EESECURITY_CLEARANCE: TStringField;
    EECALCULATED_SALARY: TFloatField;
    EEBADGE_ID: TStringField;
    EEON_CALL_FROM: TDateTimeField;
    EEON_CALL_TO: TDateTimeField;
    EENOTES: TBlobField;
    EEFILLER: TStringField;
    EEADDRESS1: TStringField;
    EEADDRESS2: TStringField;
    EECITY: TStringField;
    EESTATE: TStringField;
    EEZIP_CODE: TStringField;
    EECOUNTY: TStringField;
    EEPHONE1: TStringField;
    EEDESCRIPTION1: TStringField;
    EEPHONE2: TStringField;
    EEDESCRIPTION2: TStringField;
    EEPHONE3: TStringField;
    EEDESCRIPTION3: TStringField;
    EEBASE_RETURNS_ON_THIS_EE: TStringField;
    EEGENERAL_LEDGER_TAG: TStringField;
    DM_CLIENT: TDM_CLIENT;
    EETrim_Number: TStringField;
    EECUSTOM_DIVISION_NUMBER: TStringField;
    DM_COMPANY: TDM_COMPANY;
    EECUSTOM_BRANCH_NUMBER: TStringField;
    EECUSTOM_DEPARTMENT_NUMBER: TStringField;
    EECUSTOM_TEAM_NUMBER: TStringField;
    EECUSTOM_COMPANY_NUMBER: TStringField;
    EEEmployee_LastName_Calculate: TStringField;
    EEEmployee_FirstName_Calculate: TStringField;
    EECUSTOM_COMPANY_NAME: TStringField;
    EECUSTOM_DIVISION_NAME: TStringField;
    EECUSTOM_BRANCH_NAME: TStringField;
    EECUSTOM_DEPARTMENT_NAME: TStringField;
    EECUSTOM_TEAM_NAME: TStringField;
    EECOMPANY_OR_INDIVIDUAL_NAME: TStringField;
    EEFEDERAL_MARITAL_STATUS: TStringField;
    EENUMBER_OF_DEPENDENTS: TIntegerField;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EECURRENT_TERMINATION_CODE_DESC: TStringField;
    EEDISTRIBUTION_CODE_1099R: TStringField;
    EETAX_AMT_DETERMINED_1099R: TStringField;
    EETOTAL_DISTRIBUTION_1099R: TStringField;
    EEPENSION_PLAN_1099R: TStringField;
    EEMAKEUP_FICA_ON_CLEANUP_PR: TStringField;
    EESY_HR_EEO_NBR: TIntegerField;
    EESECONDARY_NOTES: TBlobField;
    EECO_PR_CHECK_TEMPLATES_NBR: TIntegerField;
    EEGENERATE_SECOND_CHECK: TStringField;
    EEDivisionNumber: TStringField;
    EEBranchNumber: TStringField;
    EEDepartmentNumber: TStringField;
    EETeamNumber: TStringField;
    EEPR_CHECK_MB_GROUP_NBR: TIntegerField;
    EEPR_EE_REPORT_MB_GROUP_NBR: TIntegerField;
    EEPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField;
    EETAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    EEHIGHLY_COMPENSATED: TStringField;
    EECORPORATE_OFFICER: TStringField;
    EECO_JOBS_NBR: TIntegerField;
    EECO_WORKERS_COMP_NBR: TIntegerField;
    EECO_HR_SALARY_GRADES_NBR: TIntegerField;
    EEEE_ENABLED: TStringField;
    EEE_MAIL_ADDRESS_CALC: TStringField;
    EEEmployee_MI_Calculate: TStringField;
    EEGTL_NUMBER_OF_HOURS: TIntegerField;
    EEGTL_RATE: TFloatField;
    EEE_MAIL_ADDRESS: TStringField;
    EESELFSERVE_USERNAME: TStringField;
    EESELFSERVE_PASSWORD: TStringField;
    EESELFSERVE_ENABLED: TStringField;
    EEPRINT_VOUCHER: TStringField;
    EEPosition_Desc: TStringField;
    EEACA_STATUS: TStringField;
    EEACA_STATUS_desc: TStringField;
    procedure EEBeforeDelete(DataSet: TDataSet);
    procedure EEBeforePost(DataSet: TDataSet);
    procedure EENewRecord(DataSet: TDataSet);
    procedure EECalcFields(DataSet: TDataSet);
    procedure EESALARY_AMOUNTChange(Sender: TField);
    procedure EECOMPANY_OR_INDIVIDUAL_NAMEChange(Sender: TField);
    procedure EEPrepareLookups(var Lookups: TLookupArray);
    procedure EEAddLookups(var A: TArrayDS);
    procedure EECOMPANY_OR_INDIVIDUAL_NAMEValidate(Sender: TField);
    procedure DBDTChange(Sender: TField);
    procedure EEUnPrepareLookups;
//    procedure EEAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    FDBDT: array [1..4] of Integer;
  public
    { Public declarations }
  end;

implementation

uses Math, evTypes, isDataSet, EvContext;

{$R *.DFM}

procedure TDM_EE.EEBeforeDelete(DataSet: TDataSet);
begin
  with DM_EMPLOYEE do
    if EE_TIME_OFF_ACCRUAL.Active then
      while EE_TIME_OFF_ACCRUAL.Locate('EE_NBR', DataSet['EE_NBR'], []) do
        EE_TIME_OFF_ACCRUAL.Delete;
end;

procedure TDM_EE.EEBeforePost(DataSet: TDataSet);
//begin gdy
  procedure AutoCreateLocals;
  var
    bm: TBookMark;
    cotax: TCO_LOCAL_TAX;
    HomeState, SuiState: Integer;
    NewHireLocal: Integer;

    procedure DoTransfer;
    var
      locals: TEE_LOCALS;
    begin
      DM_EMPLOYEE.EE_LOCALS.LookupsEnabled := False;
      DM_EMPLOYEE.EE_LOCALS.DisableControls;
      try
        locals := DM_EMPLOYEE.EE_LOCALS;
        if not locals.Locate('CO_LOCAL_TAX_NBR',cotax.fieldByName('CO_LOCAL_TAX_NBR').Value,[]) then
        begin
          locals.Insert;
          locals.CO_LOCAL_TAX_NBR.Value := cotax.fieldByName('CO_LOCAL_TAX_NBR').Value;
          DM_SYSTEM.SY_LOCALS.Locate('SY_LOCALS_NBR', cotax.FieldByName('SY_LOCALS_NBR').AsString, []);
          locals.EE_COUNTY.AsString := DM_SYSTEM.SY_COUNTY.Lookup('SY_COUNTY_NBR', DM_SYSTEM.SY_LOCALS.FIELDBYNAME('SY_COUNTY_NBR').AsString, 'COUNTY_NAME');
          if EE['COMPANY_OR_INDIVIDUAL_NAME'] <> GROUP_BOX_COMPANY then
            locals.EXEMPT_EXCLUDE.Value := GROUP_BOX_INCLUDE
          else
            locals.EXEMPT_EXCLUDE.Value := GROUP_BOX_EXEMPT;

          locals.OVERRIDE_LOCAL_TAX_TYPE.Value := OVERRIDE_VALUE_TYPE_NONE;
          locals.MISCELLANEOUS_AMOUNT.AsVariant := cotax.fieldByName('MISCELLANEOUS_AMOUNT').Value;
          locals.FILLER.AsVariant := cotax.fieldByName('FILLER').Value;
          locals.EE_TAX_CODE.AsVariant := cotax.fieldByName('TAX_RETURN_CODE').Value;
          locals.DEDUCT.AsString := GetCoLocalTaxDeduct(cotax.fieldByName('DEDUCT').AsString,cotax.fieldByName('LOCAL_TYPE').AsString );

          locals.Post;
        end;
      finally
        DM_EMPLOYEE.EE_LOCALS.EnableControls;
        DM_EMPLOYEE.EE_LOCALS.LookupsEnabled := True;
      end;
    end;

  begin
    DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+DM_EMPLOYEE.EE.FieldByName('CO_NBR').AsString);
    cotax := DM_COMPANY.CO_LOCAL_TAX;
    NewHireLocal := 0;

    DM_COMPANY.CO.DataRequired('CO_NBR=' + DM_EMPLOYEE.EE.FieldByName('CO_NBR').AsString);
    case DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1] of
    CLIENT_LEVEL_DIVISION:
      if DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', EECO_DIVISION_NBR.Value, []) then
        NewHireLocal := DM_COMPANY.CO_DIVISION.NEW_HIRE_CO_LOCAL_TAX_NBR.Value;
    CLIENT_LEVEL_BRANCH:
      if DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', EECO_BRANCH_NBR.Value, []) then
        NewHireLocal := DM_COMPANY.CO_BRANCH.NEW_HIRE_CO_LOCAL_TAX_NBR.Value;
    CLIENT_LEVEL_DEPT:
      if DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', EECO_DEPARTMENT_NBR.Value, []) then
        NewHireLocal := DM_COMPANY.CO_DEPARTMENT.NEW_HIRE_CO_LOCAL_TAX_NBR.Value;
    CLIENT_LEVEL_TEAM:
      if DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', EECO_TEAM_NBR.Value, []) then
        NewHireLocal := DM_COMPANY.CO_TEAM.NEW_HIRE_CO_LOCAL_TAX_NBR.Value;
    end;

    bm := cotax.GetBookMark;
    try
      if NewHireLocal <> 0 then
      begin
        Assert(cotax.Locate('CO_LOCAL_TAX_NBR', NewHireLocal, []));
        DoTransfer;
      end;

      cotax.First;
      while not cotax.EOF do
      begin
        if not DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').IsNull then
        begin
          HomeState := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR;CO_NBR', VarArrayOf([DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').Value, DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'SY_STATES_NBR');
          SuiState := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR;CO_NBR', VarArrayOf([DM_EMPLOYEE.EE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').Value, DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'SY_STATES_NBR');
          if (cotax.FieldByName('LOCAL_ACTIVE').AsString = GROUP_BOX_YES) and
             (cotax.CO_LOCAL_TAX_NBR.Value <> NewHireLocal) and
             ((HomeState = COTAX.FieldByName('SY_STATES_NBR').AsInteger) or
              (SuiState = COTAX.FieldByName('SY_STATES_NBR').AsInteger)) and
             (cotax.FieldByName('AUTOCREATE_ON_NEW_HIRE').AsString = GROUP_BOX_YES) then
            DoTransfer;
        end;
        cotax.Next;
      end;
    finally
      try
        cotax.GotoBookMark( bm );
      finally
        cotax.FreeBookMark( bm );
      end;
    end;
  end;
//end gdy

  procedure AddTO;
  begin
    DM_COMPANY.CO_TIME_OFF_ACCRUAL.Close;
    DM_COMPANY.CO_TIME_OFF_ACCRUAL.ClientID := ctx_DataAccess.ClientID;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.ClientID := ctx_DataAccess.ClientID;
    DM_COMPANY.CO_TIME_OFF_ACCRUAL.RetrieveCondition := 'CO_NBR=' + IntToStr(EE['CO_NBR']);
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.RetrieveCondition := '';
    ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_TIME_OFF_ACCRUAL, DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL]);
    ctx_DataAccess.AddTOAForEE;
  end;

  procedure AddED;
  begin
    DM_CLIENT.CL_E_DS.ClientID := ctx_DataAccess.ClientID;
    DM_COMPANY.CO_E_D_CODES.ClientID := ctx_DataAccess.ClientID;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.ClientID := ctx_DataAccess.ClientID;
    DM_CLIENT.CL_E_DS.RetrieveCondition := '';
    DM_COMPANY.CO_E_D_CODES.RetrieveCondition := 'CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
    DM_COMPANY.CO_E_D_CODES.SetOpenCondition('');
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.RetrieveCondition := '';

    // DM_COMPANY.CO_E_D_CODES is opened separatly because ctx_DataAccess.OpenDataSets has bug, sometimes this dataset was empty
    // code's been changed by mmakarkin, ticket 47945
    DM_COMPANY.CO_E_D_CODES.DataRequired(DM_COMPANY.CO_E_D_CODES.RetrieveCondition);
    ctx_DataAccess.OpenDataSets([DM_CLIENT.CL_E_DS, {DM_COMPANY.CO_E_D_CODES,} DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_STATES]);
    ctx_DataAccess.AddEdsForEE;
  end;

  procedure AddShifts;
  begin
    DM_COMPANY.CO_SHIFTS.Close;
    DM_COMPANY.CO_SHIFTS.ClientID := ctx_DataAccess.ClientID;
    DM_EMPLOYEE.EE_WORK_SHIFTS.ClientID := ctx_DataAccess.ClientID;
    DM_COMPANY.CO_SHIFTS.RetrieveCondition := 'CO_NBR=' + IntToStr(EE['CO_NBR']);
    DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := '';
    ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_SHIFTS, DM_EMPLOYEE.EE_WORK_SHIFTS]);
    with DM_COMPANY.CO_SHIFTS, DM_EMPLOYEE do
    begin
      First;
      while not EOF do
      begin
        if (FieldValues['AUTO_CREATE_ON_NEW_HIRE'] = GROUP_BOX_YES)
        and not EE_WORK_SHIFTS.Locate('EE_NBR;CO_SHIFTS_NBR', VarArrayOf([EE['EE_NBR'], FieldValues['CO_SHIFTS_NBR']]), []) then
        begin
          EE_WORK_SHIFTS.Append;
          EE_WORK_SHIFTS['EE_NBR'] := EE['EE_NBR'];
          EE_WORK_SHIFTS['CO_SHIFTS_NBR'] := FieldValues['CO_SHIFTS_NBR'];
          EE_WORK_SHIFTS.Post;
        end;
        Next;
      end;
    end;
  end;

  procedure AssignWC;
  begin
    if not EECO_WORKERS_COMP_NBR.IsNull then
      Exit;
    DM_COMPANY.CO.DataRequired('CO_NBR=' + DM_EMPLOYEE.EE.FieldByName('CO_NBR').AsString);
    case DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1] of
    CLIENT_LEVEL_DIVISION:
        if (DM_COMPANY.CO_DIVISION.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
           DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', EECO_DIVISION_NBR.Value, 'CO_WORKERS_COMP_NBR');
    CLIENT_LEVEL_BRANCH:
        if (DM_COMPANY.CO_BRANCH.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', EECO_BRANCH_NBR.Value, 'CO_WORKERS_COMP_NBR');
    CLIENT_LEVEL_DEPT:
        if (DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', EECO_DEPARTMENT_NBR.Value, 'CO_WORKERS_COMP_NBR');
    CLIENT_LEVEL_TEAM:
        if (DM_COMPANY.CO_TEAM.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', EECO_TEAM_NBR.Value, 'CO_WORKERS_COMP_NBR');
    end;
  end;
  procedure CheckDBDT;
  var
    L:variant;
  begin
     DM_COMPANY.CO.AsOfDate := EE.AsOfDate;
     DM_COMPANY.CO.Open;
     L := DM_COMPANY.CO['DBDT_LEVEL'];

     if (L>0) then
     begin
      DM_COMPANY.CO_DIVISION.AsOfDate :=  EE.AsOfDate;
      DM_COMPANY.CO_DIVISION.Open;
      if not DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR',EECO_DIVISION_NBR.Value,[]) then
         raise EevException.Create('Can not create/update  employee.  Missing division. EE# ' + EECUSTOM_EMPLOYEE_NUMBER.AsString);
     end ;

     if (L>1) then
     begin
      DM_COMPANY.CO_BRANCH.AsOfDate := EE.AsOfDate;
      DM_COMPANY.CO_BRANCH.Open;
      if not DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR',EECO_BRANCH_NBR.Value,[]) then
         raise EevException.Create('Can not create employee. Missing branch. EE# ' + EECUSTOM_EMPLOYEE_NUMBER.AsString);
    end;

    if (L>2) then
    begin
      DM_COMPANY.CO_DEPARTMENT.AsOfDate := EE.AsOfDate;
      DM_COMPANY.CO_DEPARTMENT.Open;
      if not DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR',EECO_DEPARTMENT_NBR.Value,[]) then
         raise EevException.Create('Can not create employee. Missing department. EE# ' + EECUSTOM_EMPLOYEE_NUMBER.AsString);
    end;

    if (L>3) then
    begin
      DM_COMPANY.CO_TEAM.AsOfDate := EE.AsOfDate;
      DM_COMPANY.CO_TEAM.Open;
      if not DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR',EECO_TEAM_NBR.Value,[]) then
         raise EevException.Create('Can not create employee. Missing team.  EE# ' + EECUSTOM_EMPLOYEE_NUMBER.AsString);
    end;
  end;
begin
  inherited;

  if DM_CLIENT.CL_PERSON.Filtered then
    DM_CLIENT.CL_PERSON.Filtered := False;


//   control part for SELFSERVE_USERNAME
  if (DataSet.State in [dsInsert, dsEdit]) and ((Trim(EESELFSERVE_USERNAME.AsString) <> '') and
     (not EESELFSERVE_USERNAME.IsNull) and (EESELFSERVE_USERNAME.OldValue <> EESELFSERVE_USERNAME.NewValue)) then
  begin
    if  ctx_DataAccess.TEMP_CUSTOM_VIEW.Active then ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelectWithCondition') do
    begin
      SetMacro('Columns', 'EE_NBR, CL_NBR, CL_PERSON_NBR, CO_NBR, SELFSERVE_USERNAME');
      SetMacro('TableName', 'TMP_EE');
      // To save O'Connell as a user name
      SetMacro('Condition', 'SELFSERVE_USERNAME = '+ QuotedStr(EESELFSERVE_USERNAME.AsString));
      ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.first;
      while not ctx_DataAccess.TEMP_CUSTOM_VIEW.Eof do
      begin
        if (ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('EE_NBR').AsInteger <> EEEE_NBR.AsInteger) or
           (ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CL_PERSON_NBR').AsInteger <> EECL_PERSON_NBR.AsInteger) or
           (ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CO_NBR').AsInteger <> EECO_NBR.AsInteger)
             then
               raise EUpdateError.CreateHelp('User Name already exists...', IDH_ConsistencyViolation);
        ctx_DataAccess.TEMP_CUSTOM_VIEW.next;
      end;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
    end;
  end;

  if (DataSet.State in [dsInsert, dsEdit]) and not ctx_DataAccess.Copying and not ctx_DataAccess.Importing then
    if (EE.OVERRIDE_FED_TAX_TYPE.AsString <> OVERRIDE_VALUE_TYPE_NONE) and
       EE.OVERRIDE_FED_TAX_VALUE.IsNull then
      raise EUpdateError.CreateHelp('You must enter federal override value', IDH_ConsistencyViolation);


  if (DataSet.State in [dsInsert]) and not ctx_DataAccess.Copying then
  begin
    AddTO;
    AddED;
    AddShifts;
    AssignWC;
    if not ctx_DataAccess.Importing then
      AutoCreateLocals;
  end;

  if DataSet.State in [dsInsert, dsEdit] then
  begin
    HandleCustomNumber(DataSet.FieldByName('CUSTOM_EMPLOYEE_NUMBER'), 9);

    if (DataSet['CURRENT_TERMINATION_CODE'] <> EE_TERM_ACTIVE) and
       (DataSet['CURRENT_TERMINATION_CODE'] <> EE_TERM_LEAVE) and
       (DataSet['CURRENT_TERMINATION_CODE'] <> EE_TERM_MILITARY_LEAVE) and
       (DataSet['CURRENT_TERMINATION_CODE'] <> EE_TERM_FMLA) and
       (DataSet['CURRENT_TERMINATION_CODE'] <> EE_TERM_SEASONAL) then
    begin
      if DataSet['SELFSERVE_ENABLED'] =  GROUP_BOX_FULL_ACCESS then
         DataSet['SELFSERVE_ENABLED'] := GROUP_BOX_YES;

      if DataSet['TIME_OFF_ENABLED'] =  GROUP_BOX_FULL_ACCESS then
         DataSet['TIME_OFF_ENABLED'] := GROUP_BOX_YES;

      if DataSet['BENEFITS_ENABLED'] =  GROUP_BOX_FULL_ACCESS then
         DataSet['BENEFITS_ENABLED'] := GROUP_BOX_YES;

      if DataSet['DIRECT_DEPOSIT_ENABLED'] =  GROUP_BOX_FULL_ACCESS then
         DataSet['DIRECT_DEPOSIT_ENABLED'] := GROUP_BOX_YES;
    end;
  end;

  /// DBDT checking part
  CheckDBDT;
end;

procedure TDM_EE.EENewRecord(DataSet: TDataSet);
  procedure AutoIncrementCustomNumber;
  var
    delta:integer;
    maxcen, iCode:integer;
    Q: IevQuery;
  begin
    DM_COMPANY.CO.DataRequired('CO_NBR=' + EE.FieldByName('CO_NBR').AsString);
    delta := DM_COMPANY.CO.FieldByName('AUTO_INCREMENT').AsInteger;
    if delta > 0 then
    begin
      Q := TevQuery.CreateFmt(
          '/*Cl_*/' +
          'EXECUTE BLOCK'#13 +
          'RETURNS (CustomEmployeeNumber VARCHAR(20))'#13 +
          'AS'#13 +
          'DECLARE VARIABLE CoNbr INTEGER;'#13 +
          'BEGIN'#13 +
          '  CoNbr = %d;'#13 +
          ''#13 +
          '   select first 1 CUSTOM_EMPLOYEE_NUMBER from EE where '#13 +
          '         CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL '#13 +
          '         and co_nbr = :CoNbr '#13 +
          '        order by CUSTOM_EMPLOYEE_NUMBER desc '#13 +
          '        into :CustomEmployeeNumber;'#13 +
          ''#13 +
          '  SUSPEND;'#13 +
          'END',
            [EE.FieldByName('CO_NBR').AsInteger]);
          Q.Execute;
          if (Q.Result.RecordCount >0)  and ( Q.Result.Fields[0].Value <> null ) then
          begin
              val(Q.Result.Fields[0].Value, maxcen, iCode);
              if iCode = 0 then
                 DataSet.FieldByName( 'CUSTOM_EMPLOYEE_NUMBER' ).AsInteger := maxcen+delta;
          end;
    end;
  end;
begin

  DataSet.FieldByName('CL_PERSON_NBR').AsString := DM_CLIENT.CL_PERSON.FieldByName('CL_PERSON_NBR').AsString;
  DataSet.FieldByName('CO_NBR').AsString := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
  DataSet.FieldByName('PAY_FREQUENCY').AsString := DM_COMPANY.CO.FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  DataSet.FieldByName('FEDERAL_MARITAL_STATUS').AsString := DM_COMPANY.CO.FieldByName('WITHHOLDING_DEFAULT').AsString;
  DataSet.FieldByName('W2_TYPE').AsString := DM_COMPANY.CO.FieldByName('ANNUAL_FORM_TYPE').AsString;
  DataSet.FieldByName('ACA_STATUS').AsString := DM_COMPANY.CO.FieldByName('ACA_DEFAULT_STATUS').AsString;
  DataSet['ACA_STANDARD_HOURS']:= DM_COMPANY.CO['ACA_STANDARD_HOURS'];
  DataSet.FieldByName('PRINT_VOUCHER').AsString := DM_COMPANY.CO.FieldByName('EE_PRINT_VOUCHER_DEFAULT').AsString;
  DataSet.FieldByName('BENEFITS_ELIGIBLE').AsString := DM_COMPANY.CO.FieldByName('BENEFITS_ELIGIBLE_DEFAULT').AsString;
  DataSet.FieldByName('ACA_TYPE').AsString := DM_COMPANY.CO.FieldByName('ACA_FORM_DEFAULT').AsString;
  DataSet.FieldByName('ACA_SAFE_HARBOR_TYPE').AsString := DM_COMPANY.CO.FieldByName('ACA_SAFE_HARBOR_TYPE_DEFAULT').AsString;
  DataSet.FieldByName('SY_FED_ACA_OFFER_CODES_NBR').AsVariant := DM_COMPANY.CO.FieldByName('SY_FED_ACA_OFFER_CODES_NBR').AsVariant;
  DataSet.FieldByName('SY_FED_ACA_RELIEF_CODES_NBR').AsVariant := DM_COMPANY.CO.FieldByName('SY_FED_ACA_RELIEF_CODES_NBR').AsVariant;

  if DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString =  CO_DISTRIBUTE_BOTH then
    DataSet.FieldByName('DISTRIBUTE_TAXES').AsString := DISTRIBUTE_BOTH
  else
    DataSet.FieldByName('DISTRIBUTE_TAXES').AsString := DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString;


  if DM_COMPANY.CO.FieldByName('ENABLE_ESS').AsString =  GROUP_BOX_YES then
    DataSet.FieldByName('SELFSERVE_ENABLED').AsString := GROUP_BOX_FULL_ACCESS
  else if DM_COMPANY.CO.FieldByName('ENABLE_ESS').AsString =  GROUP_BOX_READONLY then
    DataSet.FieldByName('SELFSERVE_ENABLED').AsString := GROUP_BOX_YES
  else if DM_COMPANY.CO.FieldByName('ENABLE_ESS').AsString =  GROUP_BOX_NO then
    DataSet.FieldByName('SELFSERVE_ENABLED').AsString := GROUP_BOX_NO;

  if DM_COMPANY.CO.FieldByName('ENABLE_TIME_OFF').AsString =  GROUP_BOX_YES then
    DataSet.FieldByName('TIME_OFF_ENABLED').AsString := GROUP_BOX_FULL_ACCESS
  else if DM_COMPANY.CO.FieldByName('ENABLE_TIME_OFF').AsString =  GROUP_BOX_READONLY then
    DataSet.FieldByName('TIME_OFF_ENABLED').AsString := GROUP_BOX_YES
  else if DM_COMPANY.CO.FieldByName('ENABLE_TIME_OFF').AsString =  GROUP_BOX_NO then
    DataSet.FieldByName('TIME_OFF_ENABLED').AsString := GROUP_BOX_NO;

  if DM_COMPANY.CO.FieldByName('ENABLE_BENEFITS').AsString =  GROUP_BOX_YES then
    DataSet.FieldByName('BENEFITS_ENABLED').AsString := GROUP_BOX_FULL_ACCESS
  else if DM_COMPANY.CO.FieldByName('ENABLE_BENEFITS').AsString =  GROUP_BOX_READONLY then
    DataSet.FieldByName('BENEFITS_ENABLED').AsString := GROUP_BOX_YES
  else if DM_COMPANY.CO.FieldByName('ENABLE_BENEFITS').AsString =  GROUP_BOX_NO then
    DataSet.FieldByName('BENEFITS_ENABLED').AsString := GROUP_BOX_NO;

  if DM_COMPANY.CO.FieldByName('ENABLE_DD').AsString =  GROUP_BOX_YES then
    DataSet.FieldByName('DIRECT_DEPOSIT_ENABLED').AsString := GROUP_BOX_FULL_ACCESS
  else if DM_COMPANY.CO.FieldByName('ENABLE_DD').AsString =  GROUP_BOX_READONLY then
    DataSet.FieldByName('DIRECT_DEPOSIT_ENABLED').AsString := GROUP_BOX_YES
  else if DM_COMPANY.CO.FieldByName('ENABLE_DD').AsString =  GROUP_BOX_NO then
    DataSet.FieldByName('DIRECT_DEPOSIT_ENABLED').AsString := GROUP_BOX_NO;

  if EE.Tag = 0 then
    AutoIncrementCustomNumber;
end;

procedure TDM_EE.EECalcFields(DataSet: TDataSet);
  procedure ProcessDBDT(const cd: TevClientDataSet; const Value: Integer; const FlagFieldIndex: Integer;
    const FieldNumber, FieldName: TField); register;
  begin
    if (Value <> 0)
    and cd.Active then
      with cd.ShadowDataSet do
        if CachedFindKey(Value)
        and (Fields[FlagFieldIndex].AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
          FieldNumber.AsString := Fields[FieldNumber.Tag].AsString;
          FieldName.AsString := Fields[FieldName.Tag].AsString;
        end
        else
        begin
          FieldNumber.Clear;
          FieldName.Clear;
        end
    else
    begin
      FieldNumber.Clear;
      FieldName.Clear;
    end
  end;
var
  sF, sL, sMI: string;
  clp: TevClientDataSet;
  clp_nbr, s: string;
begin
  if not TevClientDataSet(Dataset).LookupsEnabled then
    Exit;

  clp := DM_CLIENT.CL_PERSON;
  if not TevClientDataSet(DataSet).UseLookupCache then
  begin
    if clp.ShadowDataSet.FindKey([EECL_PERSON_NBR.AsInteger]) then
    begin
      sF := clp.ShadowDataSet.Fields[EEEmployee_FirstName_Calculate.Tag].AsString;
      sL := clp.ShadowDataSet.Fields[EEEmployee_LastName_Calculate.Tag].AsString;
      sMI := clp.ShadowDataSet.Fields[EEEmployee_MI_Calculate.Tag].AsString;
      EEEmployee_LastName_Calculate.AsString := trim(sL);
      EEEmployee_FirstName_Calculate.AsString := trim(sF);
      EEEmployee_MI_Calculate.AsString := trim(sMI);
      EEEmployee_Name_Calculate.AsString := Trim(sL + ' ' + sF + ' ' + sMI);//RE 22412 - added MI
      EEE_MAIL_ADDRESS_CALC.AsString := clp.ShadowDataSet.Fields[EEE_MAIL_ADDRESS_CALC.Tag].AsString;
    end;
    ProcessDBDT(DM_COMPANY.CO_DIVISION, EECO_DIVISION_NBR.AsInteger, FDBDT[1], EECUSTOM_DIVISION_NUMBER, EECUSTOM_DIVISION_NAME);
    ProcessDBDT(DM_COMPANY.CO_BRANCH, EECO_BRANCH_NBR.AsInteger, FDBDT[2], EECUSTOM_BRANCH_NUMBER, EECUSTOM_BRANCH_NAME);
    ProcessDBDT(DM_COMPANY.CO_DEPARTMENT, EECO_DEPARTMENT_NBR.AsInteger, FDBDT[3], EECUSTOM_DEPARTMENT_NUMBER, EECUSTOM_DEPARTMENT_NAME);
    ProcessDBDT(DM_COMPANY.CO_TEAM, EECO_TEAM_NBR.AsInteger, FDBDT[4], EECUSTOM_TEAM_NUMBER, EECUSTOM_TEAM_NAME);
  end
  else
  begin
    clp_nbr := EECL_PERSON_NBR.AsString;
    sL := TevClientDataSet(DataSet).FindCachedLookupValue(clp_nbr, clp, 'CL_PERSON_NBR', 'LAST_NAME');
    sF := TevClientDataSet(DataSet).FindCachedLookupValue(clp_nbr, clp, 'CL_PERSON_NBR', 'FIRST_NAME');
    sMI:= TevClientDataSet(DataSet).FindCachedLookupValue(clp_nbr, clp, 'CL_PERSON_NBR', 'MIDDLE_INITIAL');
    EEEmployee_LastName_Calculate.AsString := Trim(sL);
    EEEmployee_FirstName_Calculate.AsString := Trim(sF);
    EEEmployee_MI_Calculate.AsString := Trim(sMI);
    EEEmployee_Name_Calculate.AsString := Trim(sL + ' ' + sF + ' ' + sMI); //RE 22412 - added MI
    EEE_MAIL_ADDRESS_CALC.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(clp_nbr, clp, 'CL_PERSON_NBR', 'E_MAIL_ADDRESS');
    s := EECO_DIVISION_NBR.AsString;
    if (s <> '') and (TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_DIVISION, 'CO_DIVISION_NBR', 'PRINT_DIV_ADDRESS_ON_CHECKS') <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
    begin
      EECUSTOM_DIVISION_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_DIVISION, 'CO_DIVISION_NBR', 'CUSTOM_DIVISION_NUMBER');
      EECUSTOM_DIVISION_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_DIVISION, 'CO_DIVISION_NBR', 'NAME');
    end;
    s := EECO_BRANCH_NBR.AsString;
    if (s <> '') and (TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_BRANCH, 'CO_BRANCH_NBR', 'PRINT_BRANCH_ADDRESS_ON_CHECK') <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
    begin
      EECUSTOM_BRANCH_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_BRANCH, 'CO_BRANCH_NBR', 'CUSTOM_BRANCH_NUMBER');
      EECUSTOM_BRANCH_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_BRANCH, 'CO_BRANCH_NBR', 'NAME');
    end;
    s := EECO_DEPARTMENT_NBR.AsString;
    if (s <> '') and (TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_DEPARTMENT, 'CO_DEPARTMENT_NBR', 'PRINT_DEPT_ADDRESS_ON_CHECKS') <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
    begin
      EECUSTOM_DEPARTMENT_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_DEPARTMENT, 'CO_DEPARTMENT_NBR', 'CUSTOM_DEPARTMENT_NUMBER');
      EECUSTOM_DEPARTMENT_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_DEPARTMENT, 'CO_DEPARTMENT_NBR', 'NAME');
    end;
    s := EECO_TEAM_NBR.AsString;
    if (s <> '') and (TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_TEAM, 'CO_TEAM_NBR', 'PRINT_GROUP_ADDRESS_ON_CHECK') <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
    begin
      EECUSTOM_TEAM_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_TEAM, 'CO_TEAM_NBR', 'CUSTOM_TEAM_NUMBER');
      EECUSTOM_TEAM_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, DM_COMPANY.CO_TEAM, 'CO_TEAM_NBR', 'NAME');
    end;
  end;
  EETrim_Number.AsString := Trim(EECUSTOM_EMPLOYEE_NUMBER.AsString);
  EECURRENT_TERMINATION_CODE_DESC.AsString := ReturnDescription(EE_TerminationCode_ComboChoices, EECURRENT_TERMINATION_CODE.AsString);
  EEACA_STATUS_desc.AsString := ReturnDescription(ACAStatus_ComboChoices, EEACA_STATUS.AsString);

end;

procedure TDM_EE.EESALARY_AMOUNTChange(Sender: TField);
begin
  if not DM_CLIENT.CL_E_DS.Active then
    DM_CLIENT.CL_E_DS.DataRequired();
  if DM_CLIENT.CL_E_DS.Locate('E_D_CODE_TYPE', ED_OEARN_SALARY, []) then
    SetEESchedEDChanged(DM_CLIENT.CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString);
end;

procedure TDM_EE.EECOMPANY_OR_INDIVIDUAL_NAMEChange(Sender: TField);
begin
//SEG BEGIN
  with (Sender as TField) do
    if DataSet.State in [dsInsert] then
    begin
      if DataSet['COMPANY_OR_INDIVIDUAL_NAME'] = GROUP_BOX_COMPANY then //if EE marked 1099
      begin
        DataSet['EXEMPT_EXCLUDE_EE_FED'] := GROUP_BOX_EXEMPT;     //default on EE->Exempt
        DataSet['EXEMPT_EMPLOYEE_OASDI'] := GROUP_BOX_YES;    //default->Exempt
        DataSet['EXEMPT_EMPLOYEE_MEDICARE'] := GROUP_BOX_YES; //default->Exempt
        DataSet['EXEMPT_EMPLOYER_OASDI'] := GROUP_BOX_YES;          //default->Exempt
        DataSet['EXEMPT_EMPLOYER_MEDICARE'] := GROUP_BOX_YES;       //default->Exempt
        DataSet['EXEMPT_EMPLOYER_FUI'] := GROUP_BOX_YES;      //default->Exempt

        with DM_EMPLOYEE do
          if EE_STATES.State in [dsInsert] then
        begin
          EE_STATES['STATE_EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT; //then EE_STATES->Exempt
          EE_STATES['EE_SDI_EXEMPT_EXCLUDE']:= GROUP_BOX_EXEMPT; //then EE_SUI->Exempt
          EE_STATES['EE_SUI_EXEMPT_EXCLUDE']:= GROUP_BOX_EXEMPT; //then EE_SUI->Exempt
          EE_STATES['ER_SDI_EXEMPT_EXCLUDE']:= GROUP_BOX_EXEMPT; //then ER_SUI->Exempt
          EE_STATES['ER_SUI_EXEMPT_EXCLUDE']:= GROUP_BOX_EXEMPT; //then ER_SUI->Exempt
        end;
      end
    end;
//SEG END
end;

procedure TDM_EE.EEPrepareLookups(var Lookups: TLookupArray);
var
  iPos: Integer;
begin
  iPos := Length(Lookups);
  SetLength(Lookups, iPos + 16);
  with Lookups[iPos] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'FIRST_NAME';
  end;
  with Lookups[iPos + 1] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'LAST_NAME';
  end;
  with Lookups[iPos + 2] do
  begin
    DataSet := DM_COMPANY.CO_DIVISION;
    sKeyField := 'CO_DIVISION_NBR';
    sLookupResultField := 'PRINT_DIV_ADDRESS_ON_CHECKS';
  end;
  with Lookups[iPos + 3] do
  begin
    DataSet := DM_COMPANY.CO_DIVISION;
    sKeyField := 'CO_DIVISION_NBR';
    sLookupResultField := 'CUSTOM_DIVISION_NUMBER';
  end;
  with Lookups[iPos + 4] do
  begin
    DataSet := DM_COMPANY.CO_DIVISION;
    sKeyField := 'CO_DIVISION_NBR';
    sLookupResultField := 'NAME';
  end;
  with Lookups[iPos + 5] do
  begin
    DataSet := DM_COMPANY.CO_BRANCH;
    sKeyField := 'CO_BRANCH_NBR';
    sLookupResultField := 'PRINT_BRANCH_ADDRESS_ON_CHECK';
  end;
  with Lookups[iPos + 6] do
  begin
    DataSet := DM_COMPANY.CO_BRANCH;
    sKeyField := 'CO_BRANCH_NBR';
    sLookupResultField := 'CUSTOM_BRANCH_NUMBER';
  end;
  with Lookups[iPos + 7] do
  begin
    DataSet := DM_COMPANY.CO_BRANCH;
    sKeyField := 'CO_BRANCH_NBR';
    sLookupResultField := 'NAME';
  end;
  with Lookups[iPos + 8] do
  begin
    DataSet := DM_COMPANY.CO_DEPARTMENT;
    sKeyField := 'CO_DEPARTMENT_NBR';
    sLookupResultField := 'PRINT_DEPT_ADDRESS_ON_CHECKS';
  end;
  with Lookups[iPos + 9] do
  begin
    DataSet := DM_COMPANY.CO_DEPARTMENT;
    sKeyField := 'CO_DEPARTMENT_NBR';
    sLookupResultField := 'CUSTOM_DEPARTMENT_NUMBER';
  end;
  with Lookups[iPos + 10] do
  begin
    DataSet := DM_COMPANY.CO_DEPARTMENT;
    sKeyField := 'CO_DEPARTMENT_NBR';
    sLookupResultField := 'NAME';
  end;
  with Lookups[iPos + 11] do
  begin
    DataSet := DM_COMPANY.CO_TEAM;
    sKeyField := 'CO_TEAM_NBR';
    sLookupResultField := 'PRINT_GROUP_ADDRESS_ON_CHECK';
  end;
  with Lookups[iPos + 12] do
  begin
    DataSet := DM_COMPANY.CO_TEAM;
    sKeyField := 'CO_TEAM_NBR';
    sLookupResultField := 'CUSTOM_TEAM_NUMBER';
  end;
  with Lookups[iPos + 13] do
  begin
    DataSet := DM_COMPANY.CO_TEAM;
    sKeyField := 'CO_TEAM_NBR';
    sLookupResultField := 'NAME';
  end;
  with Lookups[iPos + 14] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'E_MAIL_ADDRESS';
  end;
  with Lookups[iPos + 15] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'MIDDLE_INITIAL';
  end;
end;

procedure TDM_EE.EEAddLookups(var A: TArrayDS);
begin
  AddDS(DM_COMPANY.CO_DIVISION, A);
  AddDS(DM_COMPANY.CO_BRANCH, A);
  AddDS(DM_COMPANY.CO_DEPARTMENT, A);
  AddDS(DM_COMPANY.CO_TEAM, A);
end;

procedure TDM_EE.EECOMPANY_OR_INDIVIDUAL_NAMEValidate(Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock;
end;

procedure TDM_EE.DBDTChange(Sender: TField);
begin
  DM_COMPANY.CO.DataRequired('CO_NBR=' + DM_EMPLOYEE.EE.FieldByName('CO_NBR').AsString);
  if (Sender.FieldName = 'CO_DIVISION_NBR') and ((DM_COMPANY.CO.FieldByName('DBDT_LEVEL').Value <> CLIENT_LEVEL_DIVISION)
  or VarIsNull(DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', EECO_DIVISION_NBR.Value, 'CO_WORKERS_COMP_NBR'))) then
    Exit;
  if (Sender.FieldName = 'CO_BRANCH_NBR') and ((DM_COMPANY.CO.FieldByName('DBDT_LEVEL').Value <> CLIENT_LEVEL_BRANCH)
  or VarIsNull(DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', EECO_BRANCH_NBR.Value, 'CO_WORKERS_COMP_NBR'))) then
    Exit;
  if (Sender.FieldName = 'CO_DEPARTMENT_NBR') and ((DM_COMPANY.CO.FieldByName('DBDT_LEVEL').Value <> CLIENT_LEVEL_DEPT)
  or VarIsNull(DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', EECO_DEPARTMENT_NBR.Value, 'CO_WORKERS_COMP_NBR'))) then
    Exit;
  if (Sender.FieldName = 'CO_TEAM_NBR') and ((DM_COMPANY.CO.FieldByName('DBDT_LEVEL').Value <> CLIENT_LEVEL_TEAM)
  or VarIsNull(DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', EECO_TEAM_NBR.Value, 'CO_WORKERS_COMP_NBR'))) then
    Exit;
  if not Unattended and (TevClientDataSet(DM_EMPLOYEE.EE).State = dsEdit) and (EvMessage('Do you want Workers Compensation Code to be changed as well?', mtConfirmation, [mbYes, mbNo]) = mrNo) then
    Exit;


  if (Sender.FieldName = 'CO_DIVISION_NBR') and
     (DM_COMPANY.CO_DIVISION.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
    EECO_WORKERS_COMP_NBR.Value := DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', EECO_DIVISION_NBR.Value, 'CO_WORKERS_COMP_NBR');
  if (Sender.FieldName = 'CO_BRANCH_NBR') and
     (DM_COMPANY.CO_BRANCH.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
    EECO_WORKERS_COMP_NBR.Value := DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', EECO_BRANCH_NBR.Value, 'CO_WORKERS_COMP_NBR');
  if (Sender.FieldName = 'CO_DEPARTMENT_NBR') and
     (DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
    EECO_WORKERS_COMP_NBR.Value := DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', EECO_DEPARTMENT_NBR.Value, 'CO_WORKERS_COMP_NBR');
  if (Sender.FieldName = 'CO_TEAM_NBR') and
     (DM_COMPANY.CO_TEAM.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
    EECO_WORKERS_COMP_NBR.Value := DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', EECO_TEAM_NBR.Value, 'CO_WORKERS_COMP_NBR');
end;

procedure TDM_EE.EEUnPrepareLookups;
begin
  FDBDT[1] := DM_COMPANY.CO_DIVISION.ShadowDataSet.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').Index;
  FDBDT[2] := DM_COMPANY.CO_BRANCH.ShadowDataSet.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').Index;
  FDBDT[3] := DM_COMPANY.CO_DEPARTMENT.ShadowDataSet.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').Index;
  FDBDT[4] := DM_COMPANY.CO_TEAM.ShadowDataSet.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').Index;
  EE.SetLookupFieldTag(EECUSTOM_DIVISION_NUMBER, DM_COMPANY.CO_DIVISION);
  EE.SetLookupFieldTag(EECUSTOM_DIVISION_NAME, 'NAME', DM_COMPANY.CO_DIVISION);
  EE.SetLookupFieldTag(EECUSTOM_BRANCH_NUMBER, DM_COMPANY.CO_BRANCH);
  EE.SetLookupFieldTag(EECUSTOM_BRANCH_NAME, 'NAME', DM_COMPANY.CO_BRANCH);
  EE.SetLookupFieldTag(EECUSTOM_DEPARTMENT_NUMBER, DM_COMPANY.CO_DEPARTMENT);
  EE.SetLookupFieldTag(EECUSTOM_DEPARTMENT_NAME, 'NAME', DM_COMPANY.CO_DEPARTMENT);
  EE.SetLookupFieldTag(EECUSTOM_TEAM_NUMBER, DM_COMPANY.CO_TEAM);
  EE.SetLookupFieldTag(EECUSTOM_TEAM_NAME, 'NAME', DM_COMPANY.CO_TEAM);
  EE.SetLookupFieldTag(EEEmployee_FirstName_Calculate, 'FIRST_NAME', DM_CLIENT.CL_PERSON);
  EE.SetLookupFieldTag(EEEmployee_LastName_Calculate, 'LAST_NAME', DM_CLIENT.CL_PERSON);
  EE.SetLookupFieldTag(EEEmployee_MI_Calculate, 'MIDDLE_INITIAL', DM_CLIENT.CL_PERSON);
  EE.SetLookupFieldTag(EEE_MAIL_ADDRESS_CALC,  'E_MAIL_ADDRESS', DM_CLIENT.CL_PERSON);
end;

initialization
  RegisterClass(TDM_EE);

finalization
  UnregisterClass(TDM_EE);

end.
