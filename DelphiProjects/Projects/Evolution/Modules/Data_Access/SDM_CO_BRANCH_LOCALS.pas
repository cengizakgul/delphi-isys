// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_BRANCH_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_BRANCH_LOCALS = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    CO_BRANCH_LOCALS: TCO_BRANCH_LOCALS;
    CO_BRANCH_LOCALSCO_BRANCH_LOCALS_NBR: TIntegerField;
    CO_BRANCH_LOCALSCO_BRANCH_NBR: TIntegerField;
    CO_BRANCH_LOCALSCO_LOCAL_TAX_NBR: TIntegerField;
    CO_BRANCH_LOCALSLocalName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_BRANCH_LOCALS);

finalization
  UnregisterClass(TDM_CO_BRANCH_LOCALS);

end.
