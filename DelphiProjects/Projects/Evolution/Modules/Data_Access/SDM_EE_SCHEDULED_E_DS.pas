// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_SCHEDULED_E_DS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   EvUIUtils, EvTypes, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents, EvCommonInterfaces, EvDataset,
  EvDataAccessComponents, isVCLBugFix, EvExceptions, Variants, evContext, EvUtils, EvUIComponents, EvClientDataSet;

type
  TDM_EE_SCHEDULED_E_DS = class(TDM_ddTable)
    EE_SCHEDULED_E_DS: TEE_SCHEDULED_E_DS;
    EE_SCHEDULED_E_DSEE_SCHEDULED_E_DS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSEE_NBR: TIntegerField;
    EE_SCHEDULED_E_DSCL_AGENCY_NBR: TIntegerField;
    EE_SCHEDULED_E_DSCL_E_DS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSPLAN_TYPE: TStringField;
    EE_SCHEDULED_E_DSAMOUNT: TFloatField;
    EE_SCHEDULED_E_DSPERCENTAGE: TFloatField;
    EE_SCHEDULED_E_DSMINIMUM_WAGE_MULTIPLIER: TFloatField;
    EE_SCHEDULED_E_DSTARGET_AMOUNT: TFloatField;
    EE_SCHEDULED_E_DSFREQUENCY: TStringField;
    EE_SCHEDULED_E_DSEXCLUDE_WEEK_1: TStringField;
    EE_SCHEDULED_E_DSEXCLUDE_WEEK_2: TStringField;
    EE_SCHEDULED_E_DSEXCLUDE_WEEK_3: TStringField;
    EE_SCHEDULED_E_DSEXCLUDE_WEEK_4: TStringField;
    EE_SCHEDULED_E_DSEXCLUDE_WEEK_5: TStringField;
    EE_SCHEDULED_E_DSEFFECTIVE_START_DATE: TDateField;
    EE_SCHEDULED_E_DSEFFECTIVE_END_DATE: TDateField;
    EE_SCHEDULED_E_DSMINIMUM_PAY_PERIOD_AMOUNT: TFloatField;
    EE_SCHEDULED_E_DSMINIMUM_PAY_PERIOD_PERCENTAGE: TFloatField;
    EE_SCHEDULED_E_DSMIN_PPP_CL_E_D_GROUPS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSMAXIMUM_PAY_PERIOD_AMOUNT: TFloatField;
    EE_SCHEDULED_E_DSMAXIMUM_PAY_PERIOD_PERCENTAGE: TFloatField;
    EE_SCHEDULED_E_DSMAX_PPP_CL_E_D_GROUPS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSANNUAL_MAXIMUM_AMOUNT: TFloatField;
    EE_SCHEDULED_E_DSTARGET_ACTION: TStringField;
    EE_SCHEDULED_E_DSNUMBER_OF_TARGETS_REMAINING: TIntegerField;
    EE_SCHEDULED_E_DSWHICH_CHECKS: TStringField;
    EE_SCHEDULED_E_DSCALCULATION_TYPE: TStringField;
    EE_SCHEDULED_E_DSCL_E_D_GROUPS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSEE_CHILD_SUPPORT_CASES_NBR: TIntegerField;
    EE_SCHEDULED_E_DSCHILD_SUPPORT_STATE: TStringField;
    EE_SCHEDULED_E_DSMAXIMUM_GARNISH_PERCENT: TFloatField;
    EE_SCHEDULED_E_DSTAKE_HOME_PAY: TFloatField;
    EE_SCHEDULED_E_DSEXPRESSION: TBlobField;
    EE_SCHEDULED_E_DSALWAYS_PAY: TStringField;
    EE_SCHEDULED_E_DSEE_DIRECT_DEPOSIT_NBR: TIntegerField;
    EE_SCHEDULED_E_DSDEDUCT_WHOLE_CHECK: TStringField;
    EE_SCHEDULED_E_DSBALANCE: TFloatField;
    EE_SCHEDULED_E_DSDescription: TStringField;
    EE_SCHEDULED_E_DSCUSTOM_E_D_CODE_NUMBER: TStringField;
    EE_SCHEDULED_E_DSE_D_CODE_TYPE: TStringField;
    EE_SCHEDULED_E_DSSkip_Hours: TStringField;
    DM_CLIENT: TDM_CLIENT;
    EE_SCHEDULED_E_DSPRIORITY_NUMBER: TIntegerField;
    EE_SCHEDULED_E_DSSCHEDULED_E_D_GROUPS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSGARNISNMENT_ID: TStringField;
    EE_SCHEDULED_E_DSTHRESHOLD_AMOUNT: TFloatField;
    EE_SCHEDULED_E_DSTHRESHOLD_E_D_GROUPS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSBENEFIT_AMOUNT_TYPE: TStringField;
    EE_SCHEDULED_E_DSSCHEDULED_E_D_ENABLED: TStringField;
    DM_COMPANY: TDM_COMPANY;
    EE_SCHEDULED_E_DSUSE_PENSION_LIMIT: TStringField;
    EE_SCHEDULED_E_DSMAX_AVERAGE_HOURLY_WAGE_RATE: TFloatField;
    EE_SCHEDULED_E_DSCO_BENEFIT_SUBTYPE_NBR: TIntegerField;
    EE_SCHEDULED_E_DSEE_BENEFITS_NBR: TIntegerField;
    EE_SCHEDULED_E_DSCO_NBR: TIntegerField;
    EE_SCHEDULED_E_DScAmount: TFloatField;
    EE_SCHEDULED_E_DScPERCENTAGE: TFloatField;
    procedure EE_SCHEDULED_E_DSNewRecord(DataSet: TDataSet);
    procedure EE_SCHEDULED_E_DSBeforePost(DataSet: TDataSet);
    procedure EE_SCHEDULED_E_DSAfterPost(DataSet: TDataSet);
    procedure EE_SCHEDULED_E_DSBeforeEdit(DataSet: TDataSet);
    procedure EE_SCHEDULED_E_DSBeforeDelete(DataSet: TDataSet);
    procedure EE_SCHEDULED_E_DSPERCENTAGEChange(Sender: TField);
    procedure EE_SCHEDULED_E_DSCalcFields(DataSet: TDataSet);
  private
    FOldEDType: string;
    procedure CheckGarnish;
  public
    { Public declarations }
  end;

implementation

uses
  EvConsts;

{$R *.DFM}

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSNewRecord(
  DataSet: TDataSet);
begin
  EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := Date;
  DataSet.FieldByName('DEDUCTIONS_TO_ZERO').value:=DM_COMPANY.CO.fieldbyname('DEDUCTIONS_TO_ZERO').Value;

end;

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSBeforePost(
  DataSet: TDataSet);

  procedure CheckEffectiveDateLimits;
  var
    s, sTmp: String;
    Q: IevQuery;
  begin

    if DataSet.State in [dsEdit] then
       sTmp := ' and EE_SCHEDULED_E_DS_NBR <> '+ DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsString;

    s:= 'select EFFECTIVE_START_DATE, EFFECTIVE_END_DATE ' +
           ' from EE_SCHEDULED_E_DS where {AsOfNow<EE_SCHEDULED_E_DS>} and ' +
           ' CL_E_DS_NBR= :cledsnbr AND EE_NBR= :eenbr ' + sTmp +
           ' order by EFFECTIVE_START_DATE desc ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('cledsnbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS.CL_E_DS_NBR.AsInteger);
    Q.Params.AddValue('eenbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS.EE_NBR.AsInteger);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    begin
      if Q.Result.Locate('EFFECTIVE_START_DATE', DataSet.FieldByName('EFFECTIVE_START_DATE').Value, []) then
        raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation)
      else
      begin
        Q.Result.First;
        while not Q.Result.Eof do
        begin
          if ((( DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               ( DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime <= Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
              or
               ((DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                (Q.Result.FieldByName('EFFECTIVE_END_DATE').IsNull)))  then // EXCEPTION WRONG_EFFECTIVE_START_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;
          if (((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               (DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime <= Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
               or
              ((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               (Q.Result.FieldByName('EFFECTIVE_END_DATE').IsNull))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;
          if ((DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime < Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
             (((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime > Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime) and (Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime <> 0)) or
               (DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime = 0))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);          
          end;

          Q.Result.Next;
        end;
      end;
    end;
  end;

begin
  if not ctx_DataAccess.PayrollIsProcessing and not ctx_DataAccess.Importing then
    with DataSet do
    begin
      if (FieldByName('E_D_CODE_TYPE').AsString <> ED_DIRECT_DEPOSIT) and
         (FieldByName('Skip_Hours').AsString = 'Y') and
         (FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger <> 0) then
        raise EUpdateError.CreateHelp('You can''t attach direct deposit account to this E/D', IDH_ConsistencyViolation)
      else if (FieldByName('E_D_CODE_TYPE').AsString <> ED_DIRECT_DEPOSIT) and
              (FieldByName('Skip_Hours').AsString = 'N') and
              (FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger <> 0) then
      begin
        if not Unattended then
          if (EvMessage('Are you sure you want attach direct deposit account to this E/D?', mtConfirmation, mbOKCancel) = mrCancel) then
            AbortEx;
      end
      else if (FieldByName('E_D_CODE_TYPE').AsString = ED_DIRECT_DEPOSIT) and
              (FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger = 0) then
        raise EUpdateError.CreateHelp('You must attach direct deposit account to this E/D', IDH_ConsistencyViolation)
      else if (FieldByName('E_D_CODE_TYPE').AsString = ED_DED_CHILD_SUPPORT) and
              (FieldByName('EE_CHILD_SUPPORT_CASES_NBR').AsInteger = 0) then
      begin
        if not Unattended then
          EvMessage('Don''t forget to set-up Child Support case on this employee', mtConfirmation, [mbOK]);
      end
      else if (FieldByName('E_D_CODE_TYPE').AsString <> ED_DED_CHILD_SUPPORT) and
              (FieldByName('EE_CHILD_SUPPORT_CASES_NBR').AsInteger <> 0) then
        raise EUpdateError.CreateHelp('You can''t attach Child Support case to this E/D', IDH_ConsistencyViolation);

      if (State = dsEdit) and
         (FOldEDType = ED_DED_GARNISH) and
         (FieldByName('E_D_CODE_TYPE').AsString <> ED_DED_GARNISH) then
        CheckGarnish;

      if State in [dsEdit, dsInsert] then
        CheckEffectiveDateLimits;

    end;
end;

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSAfterPost(
  DataSet: TDataSet);
begin
  SetEESchedEDChanged(DataSet.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString);
end;

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSBeforeEdit(
  DataSet: TDataSet);
begin
  FOldEDType := DataSet.FieldByName('E_D_CODE_TYPE').AsString;
end;

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSBeforeDelete(
  DataSet: TDataSet);
begin
  if not ctx_DataAccess.PayrollIsProcessing and not ctx_DataAccess.Importing then
    if DataSet.FieldByName('E_D_CODE_TYPE').AsString = ED_DED_GARNISH then
      CheckGarnish;
end;

procedure TDM_EE_SCHEDULED_E_DS.CheckGarnish;
begin
  with TExecDSWrapper.Create('GenericSelect2CurrentNbrWithCondition') do
  begin
    SetMacro('Columns', 'count(*)');
    SetMacro('Table1', 'pr_check');
    SetMacro('Table2', 'pr_check_lines');
    SetMacro('JoinField', 'pr_check');
    SetMacro('NbrField', 'ee_scheduled_e_ds');
    SetParam('RecordNbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR']);
    SetMacro('Condition', 't1.ee_nbr = :EeNbr');
    SetParam('EeNbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR']);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  try
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      raise EUpdateError.CreateHelp('You can''t change or delete this E/D because it''s used in check lines', IDH_ConsistencyViolation);
  finally
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;
end;

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSPERCENTAGEChange(
  Sender: TField);
begin
  with EE_SCHEDULED_E_DS do
    if TypeIsPension(FieldByName('E_D_CODE_TYPE').AsString) or
      (FieldByName('E_D_CODE_TYPE').AsString = ED_ST_DEFERRED_COMP) or
      (FieldByName('E_D_CODE_TYPE').AsString = ED_ST_STATE_PENSION) then
    begin
      ctx_DataAccess.OpenDataSets( [DM_CLIENT.CL_PENSION, DM_CLIENT.CL_E_DS] ); //??
      if not FieldByName('PERCENTAGE').IsNull and
        DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value,[]) and
        DM_CLIENT.CL_PENSION.Locate('CL_PENSION_NBR',DM_CLIENT.CL_E_DS['CL_PENSION_NBR'],[]) and
        (DM_CLIENT.CL_PENSION.FieldByName('MAX_ALLOWED_PERCENTAGE').AsString <> '') and
        (FieldByName('PERCENTAGE').Value > DM_CLIENT.CL_PENSION['MAX_ALLOWED_PERCENTAGE']) then
          if not Unattended then
            if EvMessage(Format('This is more than the EE Percentage Maximum (%s%%). Are you sure you want to do this?',[DM_CLIENT.CL_PENSION.FieldByName('MAX_ALLOWED_PERCENTAGE').AsString]), mtConfirmation, [mbYes, mbNo]) <> mrYes then
            begin
              FieldByName('PERCENTAGE').Clear;
              AbortEx;
            end
    end;
end;

procedure TDM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DSCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if EE_SCHEDULED_E_DSCO_BENEFIT_SUBTYPE_NBR.IsNull and EE_SCHEDULED_E_DSEE_BENEFITS_NBR.IsNull then
  begin
    EE_SCHEDULED_E_DScAMOUNT.Assign(EE_SCHEDULED_E_DSAMOUNT);
    EE_SCHEDULED_E_DScPERCENTAGE.Assign(EE_SCHEDULED_E_DSPERCENTAGE);
  end
  else
  begin
    EE_SCHEDULED_E_DScAmount.Clear;
    EE_SCHEDULED_E_DScPERCENTAGE.Clear;
  end;
end;

initialization
  RegisterClass(TDM_EE_SCHEDULED_E_DS);

finalization
  UnregisterClass(TDM_EE_SCHEDULED_E_DS);

end.
