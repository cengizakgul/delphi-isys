inherited DM_CL_E_D_LOCAL_EXMPT_EXCLD: TDM_CL_E_D_LOCAL_EXMPT_EXCLD
  OldCreateOrder = False
  Left = 276
  Top = 107
  Height = 218
  Width = 457
  object CL_E_D_LOCAL_EXMPT_EXCLD: TCL_E_D_LOCAL_EXMPT_EXCLD
    Aggregates = <>
    Params = <>
    ProviderName = 'CL_E_D_LOCAL_EXMPT_EXCLD_PROV'
    ValidateWithMask = True
    Left = 75
    Top = 19
    object CL_E_D_LOCAL_EXMPT_EXCLDCL_E_D_LOCAL_EXMPT_EXCLD_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_D_LOCAL_EXMPT_EXCLD_NBR'
    end
    object CL_E_D_LOCAL_EXMPT_EXCLDCL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_DS_NBR'
    end
    object CL_E_D_LOCAL_EXMPT_EXCLDSY_LOCALS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_LOCALS_NBR'
    end
    object CL_E_D_LOCAL_EXMPT_EXCLDEXEMPT_EXCLUDE: TStringField
      DisplayWidth = 1
      FieldName = 'EXEMPT_EXCLUDE'
      OnValidate = CL_E_D_LOCAL_EXMPT_EXCLDEXEMPT_EXCLUDEValidate
      Size = 1
    end
    object CL_E_D_LOCAL_EXMPT_EXCLDLocal_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name_Lookup'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_LOCALS_NBR'
      Visible = False
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 72
    Top = 78
  end
end
