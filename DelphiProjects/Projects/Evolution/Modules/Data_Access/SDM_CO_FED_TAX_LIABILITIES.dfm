inherited DM_CO_FED_TAX_LIABILITIES: TDM_CO_FED_TAX_LIABILITIES
  OldCreateOrder = True
  Height = 149
  Width = 240
  object CO_FED_TAX_LIABILITIES: TCO_FED_TAX_LIABILITIES
    ProviderName = 'CO_FED_TAX_LIABILITIES_PROV'
    PictureMasks.Strings = (
      
        'AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),' +
        '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9'T' +
        #9'F')
    ControlType.Strings = (
      'TAX_TYPE;CustomEdit;wwDBComboBox1')
    BeforeEdit = CO_FED_TAX_LIABILITIESBeforeEdit
    BeforePost = CO_FED_TAX_LIABILITIESBeforePost
    BeforeDelete = CO_FED_TAX_LIABILITIESBeforeDelete
    OnCalcFields = CO_FED_TAX_LIABILITIESCalcFields
    Left = 76
    Top = 31
    object CO_FED_TAX_LIABILITIESTYPE: TStringField
      DisplayWidth = 15
      FieldName = 'TAX_TYPE'
      Required = True
      Size = 1
    end
    object CO_FED_TAX_LIABILITIESAMOUNT: TFloatField
      DisplayWidth = 15
      FieldName = 'AMOUNT'
      Required = True
      currency = True
    end
    object CO_FED_TAX_LIABILITIESCO_FED_TAX_LIABILITIES_NBR: TIntegerField
      FieldName = 'CO_FED_TAX_LIABILITIES_NBR'
      Required = True
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Required = True
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESDUE_DATE: TDateField
      FieldName = 'DUE_DATE'
      Required = True
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESSTATUS: TStringField
      FieldName = 'STATUS'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_FED_TAX_LIABILITIESSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField
      FieldName = 'ADJUSTMENT_TYPE'
      Visible = False
      Size = 1
    end
    object CO_FED_TAX_LIABILITIESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField
      FieldName = 'IMPOUND_CO_BANK_ACCT_REG_NBR'
      Visible = False
    end
    object CO_FED_TAX_LIABILITIESNOTES: TBlobField
      FieldName = 'NOTES'
      Visible = False
      Size = 8
    end
    object CO_FED_TAX_LIABILITIESFILLER: TStringField
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object CO_FED_TAX_LIABILITIESTHIRD_PARTY: TStringField
      FieldName = 'THIRD_PARTY'
      Required = True
      Size = 1
    end
    object CO_FED_TAX_LIABILITIESCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object CO_FED_TAX_LIABILITIESIMPOUNDED: TStringField
      FieldName = 'IMPOUNDED'
      Required = True
      Size = 1
    end
    object CO_FED_TAX_LIABILITIESACH_KEY: TStringField
      FieldName = 'ACH_KEY'
      Size = 11
    end
    object CO_FED_TAX_LIABILITIESPERIOD_BEGIN_DATE: TDateField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object CO_FED_TAX_LIABILITIESPERIOD_END_DATE: TDateField
      FieldName = 'PERIOD_END_DATE'
    end
    object CO_FED_TAX_LIABILITIESADJ_PERIOD_END_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'ADJ_PERIOD_END_DATE'
    end
    object CO_FED_TAX_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CALC_PERIOD_END_DATE'
      Calculated = True
    end
  end
end
