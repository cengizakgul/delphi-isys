inherited DM_PR_CHECK_STATES: TDM_PR_CHECK_STATES
  OldCreateOrder = True
  Left = 240
  Top = 107
  Height = 480
  Width = 696
  object DM_PAYROLL: TDM_PAYROLL
    Left = 216
    Top = 96
  end
  object PR_CHECK_STATES: TPR_CHECK_STATES
    ProviderName = 'PR_CHECK_STATES_PROV'
    BeforeInsert = PR_CHECK_STATESBeforeInsert
    BeforeEdit = PR_CHECK_STATESBeforeEdit
    BeforePost = PR_CHECK_STATESBeforePost
    BeforeDelete = PR_CHECK_STATESBeforeDelete
    Left = 216
    Top = 160
    object PR_CHECK_STATESPR_CHECK_STATES_NBR: TIntegerField
      FieldName = 'PR_CHECK_STATES_NBR'
      Origin = '"PR_CHECK_STATES"."PR_CHECK_STATES_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object PR_CHECK_STATESPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
      Origin = '"PR_CHECK_STATES"."PR_CHECK_NBR"'
      Required = True
    end
    object PR_CHECK_STATESEE_STATES_NBR: TIntegerField
      FieldName = 'EE_STATES_NBR'
      Origin = '"PR_CHECK_STATES"."EE_STATES_NBR"'
      Required = True
    end
    object PR_CHECK_STATESTAX_AT_SUPPLEMENTAL_RATE: TStringField
      FieldName = 'TAX_AT_SUPPLEMENTAL_RATE'
      Origin = '"PR_CHECK_STATES"."TAX_AT_SUPPLEMENTAL_RATE"'
      Required = True
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_STATE: TStringField
      FieldName = 'EXCLUDE_STATE'
      Origin = '"PR_CHECK_STATES"."EXCLUDE_STATE"'
      Required = True
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_SDI: TStringField
      FieldName = 'EXCLUDE_SDI'
      Origin = '"PR_CHECK_STATES"."EXCLUDE_SDI"'
      Required = True
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_SUI: TStringField
      FieldName = 'EXCLUDE_SUI'
      Origin = '"PR_CHECK_STATES"."EXCLUDE_SUI"'
      Required = True
      Size = 1
    end
    object PR_CHECK_STATESEXCLUDE_ADDITIONAL_STATE: TStringField
      FieldName = 'EXCLUDE_ADDITIONAL_STATE'
      Origin = '"PR_CHECK_STATES"."EXCLUDE_ADDITIONAL_STATE"'
      Required = True
      Size = 1
    end
    object PR_CHECK_STATESSTATE_OVERRIDE_TYPE: TStringField
      FieldName = 'STATE_OVERRIDE_TYPE'
      Origin = '"PR_CHECK_STATES"."STATE_OVERRIDE_TYPE"'
      Required = True
      Size = 1
    end
    object PR_CHECK_STATESSTATE_OVERRIDE_VALUE: TFloatField
      FieldName = 'STATE_OVERRIDE_VALUE'
      Origin = '"PR_CHECK_STATES"."STATE_OVERRIDE_VALUE"'
    end
    object PR_CHECK_STATESSTATE_SHORTFALL: TFloatField
      FieldName = 'STATE_SHORTFALL'
      Origin = '"PR_CHECK_STATES"."STATE_SHORTFALL"'
    end
    object PR_CHECK_STATESSTATE_TAXABLE_WAGES: TFloatField
      FieldName = 'STATE_TAXABLE_WAGES'
      Origin = '"PR_CHECK_STATES"."STATE_TAXABLE_WAGES"'
    end
    object PR_CHECK_STATESSTATE_TAX: TFloatField
      FieldName = 'STATE_TAX'
      Origin = '"PR_CHECK_STATES"."STATE_TAX"'
    end
    object PR_CHECK_STATESEE_SDI_TAXABLE_WAGES: TFloatField
      FieldName = 'EE_SDI_TAXABLE_WAGES'
      Origin = '"PR_CHECK_STATES"."EE_SDI_TAXABLE_WAGES"'
    end
    object PR_CHECK_STATESEE_SDI_TAX: TFloatField
      FieldName = 'EE_SDI_TAX'
      Origin = '"PR_CHECK_STATES"."EE_SDI_TAX"'
    end
    object PR_CHECK_STATESER_SDI_TAXABLE_WAGES: TFloatField
      FieldName = 'ER_SDI_TAXABLE_WAGES'
      Origin = '"PR_CHECK_STATES"."ER_SDI_TAXABLE_WAGES"'
    end
    object PR_CHECK_STATESER_SDI_TAX: TFloatField
      FieldName = 'ER_SDI_TAX'
      Origin = '"PR_CHECK_STATES"."ER_SDI_TAX"'
    end
    object PR_CHECK_STATESEE_SDI_SHORTFALL: TFloatField
      FieldName = 'EE_SDI_SHORTFALL'
      Origin = '"PR_CHECK_STATES"."EE_SDI_SHORTFALL"'
    end
    object PR_CHECK_STATESEE_SDI_OVERRIDE: TFloatField
      FieldName = 'EE_SDI_OVERRIDE'
      Origin = '"PR_CHECK_STATES"."EE_SDI_OVERRIDE"'
    end
    object PR_CHECK_STATESState_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'State_Lookup'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'EE_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object PR_CHECK_STATESSDI_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'SDI_Lookup'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'SDI_State_Lookup'
      KeyFields = 'EE_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object PR_CHECK_STATESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_CHECK_STATESSTATE_GROSS_WAGES: TFloatField
      FieldName = 'STATE_GROSS_WAGES'
    end
    object PR_CHECK_STATESEE_SDI_GROSS_WAGES: TFloatField
      FieldName = 'EE_SDI_GROSS_WAGES'
    end
    object PR_CHECK_STATESER_SDI_GROSS_WAGES: TFloatField
      FieldName = 'ER_SDI_GROSS_WAGES'
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 288
    Top = 96
  end
end
