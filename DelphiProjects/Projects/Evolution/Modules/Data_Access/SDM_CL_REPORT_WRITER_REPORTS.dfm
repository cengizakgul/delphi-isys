inherited DM_CL_REPORT_WRITER_REPORTS: TDM_CL_REPORT_WRITER_REPORTS
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object CL_REPORT_WRITER_REPORTS: TCL_REPORT_WRITER_REPORTS
    ProviderName = 'CL_REPORT_WRITER_REPORTS_PROV'
    OnCalcFields = CL_REPORT_WRITER_REPORTSCalcFields
    Left = 86
    Top = 36
    object CL_REPORT_WRITER_REPORTSSB_REPORT_WRITER_REPORTS_NBR: TIntegerField
      FieldName = 'CL_REPORT_WRITER_REPORTS_NBR'
      Origin = '"CL_REPORT_WRITER_REPORTS_HIST"."CL_REPORT_WRITER_REPORTS_NBR"'
      Required = True
    end
    object CL_REPORT_WRITER_REPORTSREPORT_DESCRIPTION: TStringField
      FieldName = 'REPORT_DESCRIPTION'
      Origin = '"CL_REPORT_WRITER_REPORTS_HIST"."REPORT_DESCRIPTION"'
      Required = True
      Size = 40
    end
    object CL_REPORT_WRITER_REPORTSREPORT_TYPE: TStringField
      FieldName = 'REPORT_TYPE'
      Origin = '"CL_REPORT_WRITER_REPORTS_HIST"."REPORT_TYPE"'
      Required = True
      Size = 1
    end
    object CL_REPORT_WRITER_REPORTSREPORT_FILE: TBlobField
      FieldName = 'REPORT_FILE'
      Origin = '"CL_REPORT_WRITER_REPORTS_HIST"."REPORT_FILE"'
      Size = 8
    end
    object CL_REPORT_WRITER_REPORTSNOTES: TBlobField
      FieldName = 'NOTES'
      Origin = '"CL_REPORT_WRITER_REPORTS_HIST"."NOTES"'
      Size = 8
    end
    object CL_REPORT_WRITER_REPORTSRWDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'RWDescription'
      Size = 8000
    end
    object CL_REPORT_WRITER_REPORTSMEDIA_TYPE: TStringField
      FieldName = 'MEDIA_TYPE'
      Origin = '"CL_REPORT_WRITER_REPORTS_HIST"."MEDIA_TYPE"'
      Required = True
      Size = 2
    end
    object CL_REPORT_WRITER_REPORTSCLASS_NAME: TStringField
      FieldName = 'CLASS_NAME'
      Size = 40
    end
    object CL_REPORT_WRITER_REPORTSANCESTOR_CLASS_NAME: TStringField
      FieldName = 'ANCESTOR_CLASS_NAME'
      Size = 40
    end
  end
end
