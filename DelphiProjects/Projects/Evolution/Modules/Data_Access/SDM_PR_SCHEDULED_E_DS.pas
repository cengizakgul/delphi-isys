// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_SCHEDULED_E_DS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_PR_SCHEDULED_E_DS = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_SCHEDULED_E_DS: TPR_SCHEDULED_E_DS;
    PR_SCHEDULED_E_DSPR_SCHEDULED_E_DS_NBR: TIntegerField;
    PR_SCHEDULED_E_DSPR_NBR: TIntegerField;
    PR_SCHEDULED_E_DSCO_E_D_CODES_NBR: TIntegerField;
    PR_SCHEDULED_E_DSEXCLUDE: TStringField;
    PR_SCHEDULED_E_DSED_Lookup: TStringField;
    DM_COMPANY: TDM_COMPANY;
    procedure PR_SCHEDULED_E_DSBeforeDelete(DataSet: TDataSet);
    procedure PR_SCHEDULED_E_DSBeforeEdit(DataSet: TDataSet);
    procedure PR_SCHEDULED_E_DSBeforeInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_SCHEDULED_E_DS: TDM_PR_SCHEDULED_E_DS;

implementation

uses
  SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_SCHEDULED_E_DS.PR_SCHEDULED_E_DSBeforeDelete(
  DataSet: TDataSet);
begin
  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_SCHEDULED_E_DS.PR_SCHEDULED_E_DSBeforeEdit(
  DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_SCHEDULED_E_DS.PR_SCHEDULED_E_DSBeforeInsert(
  DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

initialization
  RegisterClass(TDM_PR_SCHEDULED_E_DS);

finalization
  UnregisterClass(TDM_PR_SCHEDULED_E_DS);

end.
