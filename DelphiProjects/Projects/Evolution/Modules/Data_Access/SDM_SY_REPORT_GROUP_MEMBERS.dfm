inherited DM_SY_REPORT_GROUP_MEMBERS: TDM_SY_REPORT_GROUP_MEMBERS
  OldCreateOrder = True
  Left = 522
  Top = 333
  Height = 154
  object SY_REPORT_GROUP_MEMBERS: TSY_REPORT_GROUP_MEMBERS
    ProviderName = 'SY_REPORT_GROUP_MEMBERS_PROV'
    FieldDefs = <
      item
        Name = 'SY_REPORT_GROUPS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORT_GROUP_MEMBERS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORT_WRITER_REPORTS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end>
    Left = 88
    Top = 16
    object SY_REPORT_GROUP_MEMBERSSY_REPORT_GROUPS_NBR: TIntegerField
      FieldName = 'SY_REPORT_GROUPS_NBR'
      Required = True
    end
    object SY_REPORT_GROUP_MEMBERSSY_REPORT_GROUP_MEMBERS_NBR: TIntegerField
      FieldName = 'SY_REPORT_GROUP_MEMBERS_NBR'
      Required = True
    end
    object SY_REPORT_GROUP_MEMBERSSY_REPORT_WRITER_REPORTS_NBR: TIntegerField
      DisplayLabel = 'Nbr'
      FieldName = 'SY_REPORT_WRITER_REPORTS_NBR'
      Required = True
    end
    object SY_REPORT_GROUP_MEMBERSSY_REPORT_GROUP_NAME_Lookup: TStringField
      DisplayLabel = 'Group Name'
      FieldKind = fkLookup
      FieldName = 'SY_REPORT_GROUP_NAME_Lookup'
      LookupDataSet = DM_SY_REPORT_GROUPS.SY_REPORT_GROUPS
      LookupKeyFields = 'SY_REPORT_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_REPORT_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 88
    Top = 72
  end
end
