// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_E_D_LOCAL_EXMPT_EXCLD;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvUtils, EvContext;

type
  TDM_CL_E_D_LOCAL_EXMPT_EXCLD = class(TDM_ddTable)
    CL_E_D_LOCAL_EXMPT_EXCLD: TCL_E_D_LOCAL_EXMPT_EXCLD;
    CL_E_D_LOCAL_EXMPT_EXCLDCL_E_D_LOCAL_EXMPT_EXCLD_NBR: TIntegerField;
    CL_E_D_LOCAL_EXMPT_EXCLDCL_E_DS_NBR: TIntegerField;
    CL_E_D_LOCAL_EXMPT_EXCLDSY_LOCALS_NBR: TIntegerField;
    CL_E_D_LOCAL_EXMPT_EXCLDEXEMPT_EXCLUDE: TStringField;
    CL_E_D_LOCAL_EXMPT_EXCLDLocal_Name_Lookup: TStringField;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    procedure CL_E_D_LOCAL_EXMPT_EXCLDEXEMPT_EXCLUDEValidate(
      Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CL_E_D_LOCAL_EXMPT_EXCLD.CL_E_D_LOCAL_EXMPT_EXCLDEXEMPT_EXCLUDEValidate(
  Sender: TField);
begin
  DM_COMPANY.CO.DataRequired();
  DM_COMPANY.CO.First;
  while not DM_COMPANY.CO.Eof do
  begin
    ctx_DataAccess.CheckCompanyLock;
    DM_COMPANY.CO.Next
  end
end;

initialization
  RegisterClass(TDM_CL_E_D_LOCAL_EXMPT_EXCLD);

finalization
  UnregisterClass(TDM_CL_E_D_LOCAL_EXMPT_EXCLD);

end.
