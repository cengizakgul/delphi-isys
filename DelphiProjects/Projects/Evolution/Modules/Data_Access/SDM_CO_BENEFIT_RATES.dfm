inherited DM_CO_BENEFIT_RATES: TDM_CO_BENEFIT_RATES
  OldCreateOrder = True
  Left = 756
  Top = 232
  Height = 250
  Width = 371
  object CO_BENEFIT_RATES: TCO_BENEFIT_RATES
    ProviderName = 'CO_BENEFIT_RATES_PROV'
    BeforePost = CO_BENEFIT_RATESBeforePost
    OnCalcFields = CO_BENEFIT_RATESCalcFields
    Left = 56
    Top = 24
    object CO_BENEFIT_RATEScTotalAmount: TFloatField
      FieldKind = fkCalculated
      FieldName = 'cTotalAmount'
      Calculated = True
    end
    object CO_BENEFIT_RATEScEEPortion: TFloatField
      FieldKind = fkCalculated
      FieldName = 'cEEPortion'
      Calculated = True
    end
    object CO_BENEFIT_RATEScERPortion: TFloatField
      FieldKind = fkCalculated
      FieldName = 'cERPortion'
      Calculated = True
    end
    object CO_BENEFIT_RATESBENEFIT_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'Description'
      LookupDataSet = DM_CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE
      LookupKeyFields = 'CO_BENEFIT_SUBTYPE_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_BENEFIT_SUBTYPE_NBR'
      Lookup = True
    end
    object CO_BENEFIT_RATESEE_RATE: TFloatField
      FieldName = 'EE_RATE'
    end
    object CO_BENEFIT_RATESER_RATE: TFloatField
      FieldName = 'ER_RATE'
    end
    object CO_BENEFIT_RATESCOBRA_RATE: TFloatField
      FieldName = 'COBRA_RATE'
    end
    object CO_BENEFIT_RATESCO_BENEFIT_SUBTYPE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object CO_BENEFIT_RATESCO_BENEFIT_RATES_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_RATES_NBR'
    end
    object CO_BENEFIT_RATESRATE_TYPE: TStringField
      FieldName = 'RATE_TYPE'
      Size = 1
    end
    object CO_BENEFIT_RATESMAX_DEPENDENTS: TSmallintField
      FieldName = 'MAX_DEPENDENTS'
    end
    object CO_BENEFIT_RATESPERIOD_BEGIN: TDateField
      FieldName = 'PERIOD_BEGIN'
    end
    object CO_BENEFIT_RATESPERIOD_END: TDateField
      FieldName = 'PERIOD_END'
    end
    object CO_BENEFIT_RATESEDGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGroupName'
      LookupDataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
      LookupKeyFields = 'CL_E_D_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_E_D_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_RATESCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
    end
    object CO_BENEFIT_RATESCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object CO_BENEFIT_RATEScPERIOD_END: TDateField
      FieldKind = fkCalculated
      FieldName = 'cPERIOD_END'
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 208
    Top = 32
  end
end
