// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_STATES;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SY_STATES = class(TDM_ddTable)
    SY_STATES: TSY_STATES;
    SY_STATESSY_STATES_NBR: TIntegerField;
    SY_STATESSTATE: TStringField;
    SY_STATESNAME: TStringField;
    SY_STATESSTATE_MINIMUM_WAGE: TFloatField;
    SY_STATESSTATE_WITHHOLDING: TStringField;
    SY_STATESSTATE_TIP_CREDIT: TFloatField;
    SY_STATESSUPPLEMENTAL_WAGES_PERCENTAGE: TFloatField;
    SY_STATESSTATE_EFT_TYPE: TStringField;
    SY_STATESEFT_NOTES: TBlobField;
    SY_STATESPRINT_STATE_RETURN_IF_ZERO: TStringField;
    SY_STATESSUI_EFT_TYPE: TStringField;
    SY_STATESSUI_EFT_NOTES: TBlobField;
    SY_STATESSDI_RECIPROCATE: TStringField;
    SY_STATESSUI_RECIPROCATE: TStringField;
    SY_STATESWEEKLY_SDI_TAX_CAP: TFloatField;
    SY_STATESEE_SDI_MAXIMUM_WAGE: TFloatField;
    SY_STATESER_SDI_MAXIMUM_WAGE: TFloatField;
    SY_STATESEE_SDI_RATE: TFloatField;
    SY_STATESER_SDI_RATE: TFloatField;
    SY_STATESSHOW_S125_IN_GROSS_WAGES_SUI: TStringField;
    SY_STATESPRINT_SUI_RETURN_IF_ZERO: TStringField;
    SY_STATESMAXIMUM_GARNISHMENT_PERCENT: TFloatField;
    SY_STATESGARNISH_MIN_WAGE_MULTIPLIER: TFloatField;
    SY_STATESPAYROLL: TStringField;
    SY_STATESSALES: TStringField;
    SY_STATESCORPORATE: TStringField;
    SY_STATESRAILROAD: TStringField;
    SY_STATESUNEMPLOYMENT: TStringField;
    SY_STATESOTHER: TStringField;
    SY_STATESUIFSA_NEW_HIRE_REPORTING: TStringField;
    SY_STATESDD_CHILD_SUPPORT: TStringField;
    SY_STATESUSE_STATE_MISC_TAX_RETURN_CODE: TStringField;
    SY_STATESSALES_TAX_PERCENTAGE: TFloatField;
    SY_STATESSY_STATE_TAX_PMT_AGENCY_NBR: TIntegerField;
    SY_STATESSY_TAX_PMT_REPORT_NBR: TIntegerField;
    SY_STATESSY_STATE_REPORTING_AGENCY_NBR: TIntegerField;
    SY_STATESPAY_SDI_WITH: TStringField;
    SY_STATESFILLER: TStringField;
    SY_STATESTAX_DEPOSIT_FOLLOW_FEDERAL: TStringField;
    SY_STATESROUND_TO_NEAREST_DOLLAR: TStringField;
    SY_STATESSUI_TAX_PAYMENT_TYPE_CODE: TStringField;
    SY_STATESINC_SUI_TAX_PAYMENT_CODE: TStringField;
    SY_STATESFIPS_CODE: TStringField;
    SY_STATESPRINT_W2: TStringField;
    SY_STATESRECIPROCITY_TYPE: TStringField;
    SY_STATESW2_BOX: TStringField;
    SY_STATESINACTIVATE_MARITAL_STATUS: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_STATES);

finalization
  UnregisterClass(TDM_SY_STATES);

end.
 
