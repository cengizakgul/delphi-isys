// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_TIME_OFF_ACCRUAL_OPER;

interface

uses
  SDM_ddTable, SDataDictClient,
  SysUtils, Classes, DB;  

type
  TDM_EE_TIME_OFF_ACCRUAL_OPER = class(TDM_ddTable)
    EE_TIME_OFF_ACCRUAL_OPER: TEE_TIME_OFF_ACCRUAL_OPER;
    procedure EE_TIME_OFF_ACCRUAL_OPERAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses SDataStructure;

procedure TDM_EE_TIME_OFF_ACCRUAL_OPER.EE_TIME_OFF_ACCRUAL_OPERAfterInsert(
  DataSet: TDataSet);
begin
  DataSet['EE_TIME_OFF_ACCRUAL_NBR'] := DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL['EE_TIME_OFF_ACCRUAL_NBR'];
  DataSet['ACCRUED'] := 0;
  DataSet['USED'] := 0;
end;

initialization
  RegisterClass(TDM_EE_TIME_OFF_ACCRUAL_OPER);

finalization
  UnregisterClass(TDM_EE_TIME_OFF_ACCRUAL_OPER);

end.
