inherited DM_SY_FED_REPORTING_AGENCY: TDM_SY_FED_REPORTING_AGENCY
  OldCreateOrder = False
  Left = 339
  Top = 268
  Height = 295
  Width = 407
  object SY_FED_REPORTING_AGENCY: TSY_FED_REPORTING_AGENCY
    Aggregates = <>
    Params = <>
    ProviderName = 'SY_FED_REPORTING_AGENCY_PROV'
    ValidateWithMask = True
    Left = 78
    Top = 56
    object SY_FED_REPORTING_AGENCYNAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'AGENCY_NAME'
      LookupDataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
      LookupKeyFields = 'SY_GLOBAL_AGENCY_NBR'
      LookupResultField = 'AGENCY_NAME'
      KeyFields = 'SY_GLOBAL_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object SY_FED_REPORTING_AGENCYSY_GLOBAL_AGENCY_NBR: TIntegerField
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
      Visible = False
    end
    object SY_FED_REPORTING_AGENCYSY_FED_REPORTING_AGENCY_NBR: TIntegerField
      FieldName = 'SY_FED_REPORTING_AGENCY_NBR'
      Visible = False
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 78
    Top = 114
  end
end
