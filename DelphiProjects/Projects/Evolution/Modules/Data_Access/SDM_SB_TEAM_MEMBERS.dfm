inherited DM_SB_TEAM_MEMBERS: TDM_SB_TEAM_MEMBERS
  OldCreateOrder = False
  Left = 142
  Top = 167
  Height = 540
  Width = 783
  object SB_TEAM_MEMBERS: TSB_TEAM_MEMBERS
    Aggregates = <>
    Params = <>
    ProviderName = 'SB_TEAM_MEMBERS_PROV'
    OnCalcFields = SB_TEAM_MEMBERSCalcFields
    ValidateWithMask = True
    Left = 72
    Top = 48
    object SB_TEAM_MEMBERSSB_TEAM_MEMBERS_NBR: TIntegerField
      FieldName = 'SB_TEAM_MEMBERS_NBR'
    end
    object SB_TEAM_MEMBERSSB_TEAM_NBR: TIntegerField
      FieldName = 'SB_TEAM_NBR'
    end
    object SB_TEAM_MEMBERSSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
    end
    object SB_TEAM_MEMBERSUserName: TStringField
      FieldKind = fkCalculated
      FieldName = 'UserName'
      Size = 80
      Calculated = True
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 216
    Top = 56
  end
end
