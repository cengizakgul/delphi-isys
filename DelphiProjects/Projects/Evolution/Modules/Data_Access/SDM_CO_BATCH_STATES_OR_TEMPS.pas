// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_BATCH_STATES_OR_TEMPS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure;  

type
  TDM_CO_BATCH_STATES_OR_TEMPS = class(TDM_ddTable)
    CO_BATCH_STATES_OR_TEMPS: TCO_BATCH_STATES_OR_TEMPS;
    DM_COMPANY: TDM_COMPANY;
    CO_BATCH_STATES_OR_TEMPSCO_BATCH_STATES_OR_TEMPS_NBR: TIntegerField;
    CO_BATCH_STATES_OR_TEMPSCO_STATES_NBR: TIntegerField;
    CO_BATCH_STATES_OR_TEMPSTAX_AT_SUPPLEMENTAL_RATE: TStringField;
    CO_BATCH_STATES_OR_TEMPSEXCLUDE_STATE: TStringField;
    CO_BATCH_STATES_OR_TEMPSEXCLUDE_SDI: TStringField;
    CO_BATCH_STATES_OR_TEMPSEXCLUDE_SUI: TStringField;
    CO_BATCH_STATES_OR_TEMPSSTATE_OVERRIDE_TYPE: TStringField;
    CO_BATCH_STATES_OR_TEMPSSTATE_OVERRIDE_VALUE: TFloatField;
    CO_BATCH_STATES_OR_TEMPSCO_PR_CHECK_TEMPLATES_NBR: TIntegerField;
    CO_BATCH_STATES_OR_TEMPSEXCLUDE_ADDITIONAL_STATE: TStringField;
    CO_BATCH_STATES_OR_TEMPSState: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BATCH_STATES_OR_TEMPS: TDM_CO_BATCH_STATES_OR_TEMPS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_BATCH_STATES_OR_TEMPS);

finalization
  UnregisterClass(TDM_CO_BATCH_STATES_OR_TEMPS);

end.
