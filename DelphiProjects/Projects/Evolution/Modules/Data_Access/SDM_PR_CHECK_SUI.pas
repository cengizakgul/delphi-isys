// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_CHECK_SUI;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_PR_CHECK_SUI = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_CHECK_SUI: TPR_CHECK_SUI;
    PR_CHECK_SUIPR_CHECK_SUI_NBR: TIntegerField;
    PR_CHECK_SUIPR_CHECK_NBR: TIntegerField;
    PR_CHECK_SUICO_SUI_NBR: TIntegerField;
    PR_CHECK_SUISUI_TAXABLE_WAGES: TFloatField;
    PR_CHECK_SUISUI_TAX: TFloatField;
    PR_CHECK_SUIOVERRIDE_AMOUNT: TFloatField;
    PR_CHECK_SUISUI_Lookup: TStringField;
    DM_COMPANY: TDM_COMPANY;
    PR_CHECK_SUISUI_GROSS_WAGES: TFloatField;
    PR_CHECK_SUIPR_NBR: TIntegerField;
    procedure PR_CHECK_SUIBeforeDelete(DataSet: TDataSet);
    procedure PR_CHECK_SUIBeforeEdit(DataSet: TDataSet);
    procedure PR_CHECK_SUIBeforeInsert(DataSet: TDataSet);
    procedure PR_CHECK_SUIBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_CHECK_SUI: TDM_PR_CHECK_SUI;

implementation

uses
  SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_CHECK_SUI.PR_CHECK_SUIBeforeDelete(DataSet: TDataSet);
begin
  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_CHECK_SUI.PR_CHECK_SUIBeforeEdit(DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_CHECK_SUI.PR_CHECK_SUIBeforeInsert(DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure TDM_PR_CHECK_SUI.PR_CHECK_SUIBeforePost(DataSet: TDataSet);
begin
  if (DataSet.State = dsInsert) and DataSet.FieldByName('PR_NBR').IsNull then
    DataSet.FieldByName('PR_NBR').AsInteger := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
end;

initialization
  RegisterClass(TDM_PR_CHECK_SUI);

finalization
  UnregisterClass(TDM_PR_CHECK_SUI);

end.
