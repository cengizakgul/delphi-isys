inherited DM_CO_GENERAL_LEDGER: TDM_CO_GENERAL_LEDGER
  OldCreateOrder = True
  Left = 248
  Top = 107
  Height = 480
  Width = 696
  object CO_GENERAL_LEDGER: TCO_GENERAL_LEDGER
    ProviderName = 'CO_GENERAL_LEDGER_PROV'
    BeforePost = CO_GENERAL_LEDGERBeforePost
    OnCalcFields = CO_GENERAL_LEDGERCalcFields
    OnNewRecord = CO_GENERAL_LEDGERNewRecord
    Left = 192
    Top = 128
    object CO_GENERAL_LEDGERCO_GENERAL_LEDGER_NBR: TIntegerField
      FieldName = 'CO_GENERAL_LEDGER_NBR'
    end
    object CO_GENERAL_LEDGERCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_GENERAL_LEDGERLEVEL_TYPE: TStringField
      FieldName = 'LEVEL_TYPE'
      Size = 1
    end
    object CO_GENERAL_LEDGERLEVEL_NBR: TIntegerField
      FieldName = 'LEVEL_NBR'
    end
    object CO_GENERAL_LEDGERDATA_TYPE: TStringField
      FieldName = 'DATA_TYPE'
      Size = 1
    end
    object CO_GENERAL_LEDGERDATA_NBR: TIntegerField
      FieldName = 'DATA_NBR'
    end
    object CO_GENERAL_LEDGERGENERAL_LEDGER_FORMAT: TStringField
      FieldName = 'GENERAL_LEDGER_FORMAT'
      Size = 30
    end
    object CO_GENERAL_LEDGERLevel_Name: TStringField
      FieldKind = fkCalculated
      FieldName = 'Level_Name'
      Size = 40
      Calculated = True
    end
    object CO_GENERAL_LEDGERData_Name: TStringField
      FieldKind = fkCalculated
      FieldName = 'Data_Name'
      Size = 40
      Calculated = True
    end
    object CO_GENERAL_LEDGERLevelType_Name: TStringField
      FieldKind = fkCalculated
      FieldName = 'LevelType_Name'
      Calculated = True
    end
    object CO_GENERAL_LEDGERDataType_Name: TStringField
      FieldKind = fkCalculated
      FieldName = 'DataType_Name'
      Calculated = True
    end
  end
end
