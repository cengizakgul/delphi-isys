inherited DM_SY_LOCAL_EXEMPTIONS: TDM_SY_LOCAL_EXEMPTIONS
  Left = 342
  Top = 261
  Height = 437
  Width = 570
  object SY_LOCAL_EXEMPTIONS: TSY_LOCAL_EXEMPTIONS
    ProviderName = 'SY_LOCAL_EXEMPTIONS_PROV'
    OnCalcFields = SY_LOCAL_EXEMPTIONSCalcFields
    Left = 72
    Top = 56
    object SY_LOCAL_EXEMPTIONSEXEMPT: TStringField
      FieldName = 'EXEMPT'
      Size = 1
    end
    object SY_LOCAL_EXEMPTIONSE_D_CODE_TYPE: TStringField
      FieldName = 'E_D_CODE_TYPE'
      Size = 2
    end
    object SY_LOCAL_EXEMPTIONSSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
    object SY_LOCAL_EXEMPTIONSSY_LOCAL_EXEMPTIONS_NBR: TIntegerField
      FieldName = 'SY_LOCAL_EXEMPTIONS_NBR'
    end
    object SY_LOCAL_EXEMPTIONSEDDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'EDDescription'
      Size = 60
    end
  end
end
