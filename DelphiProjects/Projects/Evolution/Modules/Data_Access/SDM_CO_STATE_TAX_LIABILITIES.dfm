inherited DM_CO_STATE_TAX_LIABILITIES: TDM_CO_STATE_TAX_LIABILITIES
  OldCreateOrder = True
  Left = 419
  Top = 162
  Height = 479
  Width = 741
  object CO_STATE_TAX_LIABILITIES: TCO_STATE_TAX_LIABILITIES
    ProviderName = 'CO_STATE_TAX_LIABILITIES_PROV'
    PictureMasks.Strings = (
      
        'AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),' +
        '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9'T' +
        #9'F')
    ControlType.Strings = (
      'TAX_TYPE;CustomEdit;wwDBComboBox1')
    BeforeEdit = CO_STATE_TAX_LIABILITIESBeforeEdit
    BeforePost = CO_STATE_TAX_LIABILITIESBeforePost
    BeforeDelete = CO_STATE_TAX_LIABILITIESBeforeDelete
    OnCalcFields = CO_STATE_TAX_LIABILITIESCalcFields
    OnNewRecord = CO_STATE_TAX_LIABILITIESNewRecord
    Left = 79
    Top = 16
    object CO_STATE_TAX_LIABILITIESTYPE: TStringField
      DisplayWidth = 15
      FieldName = 'TAX_TYPE'
      Required = True
      Size = 1
    end
    object CO_STATE_TAX_LIABILITIESAMOUNT: TFloatField
      DisplayWidth = 15
      FieldName = 'AMOUNT'
      Required = True
      currency = True
    end
    object CO_STATE_TAX_LIABILITIESCO_STATE_TAX_LIABILITIES_NBR: TIntegerField
      FieldName = 'CO_STATE_TAX_LIABILITIES_NBR'
      Required = True
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Required = True
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESCO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
      Required = True
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESDUE_DATE: TDateField
      FieldName = 'DUE_DATE'
      Required = True
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESSTATUS: TStringField
      FieldName = 'STATUS'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_STATE_TAX_LIABILITIESSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField
      FieldName = 'ADJUSTMENT_TYPE'
      Visible = False
      Size = 1
    end
    object CO_STATE_TAX_LIABILITIESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField
      FieldName = 'IMPOUND_CO_BANK_ACCT_REG_NBR'
      Visible = False
    end
    object CO_STATE_TAX_LIABILITIESNOTES: TBlobField
      FieldName = 'NOTES'
      Visible = False
      Size = 8
    end
    object CO_STATE_TAX_LIABILITIESFILLER: TStringField
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object CO_STATE_TAX_LIABILITIESState: TStringField
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object CO_STATE_TAX_LIABILITIESTHIRD_PARTY: TStringField
      FieldName = 'THIRD_PARTY'
      Required = True
      Size = 1
    end
    object CO_STATE_TAX_LIABILITIESCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object CO_STATE_TAX_LIABILITIESFIELD_OFFICE_NBR: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'FIELD_OFFICE_NBR'
    end
    object CO_STATE_TAX_LIABILITIESACH_KEY: TStringField
      FieldName = 'ACH_KEY'
      Size = 11
    end
    object CO_STATE_TAX_LIABILITIESIMPOUNDED: TStringField
      FieldName = 'IMPOUNDED'
      Required = True
      Size = 1
    end
    object CO_STATE_TAX_LIABILITIESSY_STATE_DEPOSIT_FREQ_NBR: TIntegerField
      FieldName = 'SY_STATE_DEPOSIT_FREQ_NBR'
    end
    object CO_STATE_TAX_LIABILITIESTAX_COUPON_SY_REPORTS_NBR: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'TAX_COUPON_SY_REPORTS_NBR'
    end
    object CO_STATE_TAX_LIABILITIESPERIOD_BEGIN_DATE: TDateField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object CO_STATE_TAX_LIABILITIESPERIOD_END_DATE: TDateField
      FieldName = 'PERIOD_END_DATE'
    end
    object CO_STATE_TAX_LIABILITIESADJ_PERIOD_END_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'ADJ_PERIOD_END_DATE'
    end
    object CO_STATE_TAX_LIABILITIEScSY_STATE_DEPOSIT_FREQ_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cSY_STATE_DEPOSIT_FREQ_NBR'
      Calculated = True
    end
    object CO_STATE_TAX_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CALC_PERIOD_END_DATE'
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 78
    Top = 78
  end
end
