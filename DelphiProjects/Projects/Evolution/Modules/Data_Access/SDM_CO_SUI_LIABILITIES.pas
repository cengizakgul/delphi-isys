// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_SUI_LIABILITIES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, EvContext, EvClientDataSet;

type
  TDM_CO_SUI_LIABILITIES = class(TDM_ddTable)
    CO_SUI_LIABILITIES: TCO_SUI_LIABILITIES;
    CO_SUI_LIABILITIESAMOUNT: TFloatField;
    CO_SUI_LIABILITIESCO_SUI_LIABILITIES_NBR: TIntegerField;
    CO_SUI_LIABILITIESCO_NBR: TIntegerField;
    CO_SUI_LIABILITIESCO_SUI_NBR: TIntegerField;
    CO_SUI_LIABILITIESDUE_DATE: TDateField;
    CO_SUI_LIABILITIESSTATUS: TStringField;
    CO_SUI_LIABILITIESSTATUS_DATE: TDateTimeField;
    CO_SUI_LIABILITIESADJUSTMENT_TYPE: TStringField;
    CO_SUI_LIABILITIESPR_NBR: TIntegerField;
    CO_SUI_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField;
    CO_SUI_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField;
    CO_SUI_LIABILITIESNOTES: TBlobField;
    CO_SUI_LIABILITIESFILLER: TStringField;
    CO_SUI_LIABILITIESSUI_Name: TStringField;
    CO_SUI_LIABILITIESSUI_State: TStringField;
    CO_SUI_LIABILITIESTHIRD_PARTY: TStringField;
    CO_SUI_LIABILITIESCHECK_DATE: TDateField;
    CO_SUI_LIABILITIESACH_KEY: TStringField;
    CO_SUI_LIABILITIESIMPOUNDED: TStringField;
    CO_SUI_LIABILITIESPERIOD_BEGIN_DATE: TDateField;
    CO_SUI_LIABILITIESPERIOD_END_DATE: TDateField;
    CO_SUI_LIABILITIESTAX_COUPON_SY_REPORTS_NBR: TIntegerField;
    CO_SUI_LIABILITIESADJ_PERIOD_END_DATE: TDateField;
    DM_COMPANY: TDM_COMPANY;
    CO_SUI_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField;
    procedure CO_SUI_LIABILITIESNewRecord(DataSet: TDataSet);
    procedure CO_SUI_LIABILITIESBeforePost(DataSet: TDataSet);
    procedure CO_SUI_LIABILITIESBeforeEdit(DataSet: TDataSet);
    procedure CO_SUI_LIABILITIESBeforeDelete(DataSet: TDataSet);
    procedure CO_SUI_LIABILITIESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_SUI_LIABILITIES.CO_SUI_LIABILITIESNewRecord(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('CO_NBR').Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
end;

procedure TDM_CO_SUI_LIABILITIES.CO_SUI_LIABILITIESBeforePost(
  DataSet: TDataSet);
begin

 //   Added By CA..

  if DataSet.FieldByName('AMOUNT').IsNull then
     raise EUpdateError.CreateHelp('Amount is a required field', IDH_ConsistencyViolation);


  if DataSet.FieldByName('CHECK_DATE').IsNull then
    raise EUpdateError.CreateHelp('Check date is a required field', IDH_ConsistencyViolation);
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;

end;

procedure TDM_CO_SUI_LIABILITIES.CO_SUI_LIABILITIESBeforeEdit(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_SUI_LIABILITIES.CO_SUI_LIABILITIESBeforeDelete(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True); 
end;

procedure TDM_CO_SUI_LIABILITIES.CO_SUI_LIABILITIESCalcFields(
  DataSet: TDataSet);
var
 s : string;
begin
  inherited;
  s := ExtractFromFiller(Dataset.fieldByName('FILLER').AsString , 41, 10);
  if (s <> '') then
     Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := StrToDate(s)
  else
    if not Dataset.FieldByName('PERIOD_END_DATE').IsNull then
       Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := Dataset.FieldByName('PERIOD_END_DATE').AsDateTime;
end;

initialization
  RegisterClass(TDM_CO_SUI_LIABILITIES);

finalization
  UnregisterClass(TDM_CO_SUI_LIABILITIES);

end.
