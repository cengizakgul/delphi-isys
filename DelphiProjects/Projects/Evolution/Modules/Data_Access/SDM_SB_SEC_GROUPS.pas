// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_SEC_GROUPS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, EvContext;

type
  TDM_SB_SEC_GROUPS = class(TDM_ddTable)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_SEC_GROUPS: TSB_SEC_GROUPS;
    procedure SB_SEC_GROUPSBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  end;

var
  DM_SB_SEC_GROUPS: TDM_SB_SEC_GROUPS;

implementation

{$R *.DFM}

uses EvUtils;

constructor TDM_SB_SEC_GROUPS.Create(AOwner: TComponent);
begin
  Tag := Integer(GetCurrentThreadId);
  inherited;
end;

procedure TDM_SB_SEC_GROUPS.SB_SEC_GROUPSBeforePost(DataSet: TDataSet);
begin
  if Dataset.State = dsInsert then
  begin
    ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS]);
    DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Insert;
    DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_SEC_GROUPS_NBR'] := Dataset['SB_SEC_GROUPS_NBR'];
    DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_USER_NBR'] := Context.UserAccount.InternalNbr;
  end;
end;

initialization
  RegisterClass(TDM_SB_SEC_GROUPS);

finalization
  UnregisterClass(TDM_SB_SEC_GROUPS);

end.
