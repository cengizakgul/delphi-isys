inherited DM_CO_E_D_CODES: TDM_CO_E_D_CODES
  Height = 230
  Width = 132
  object CO_E_D_CODES: TCO_E_D_CODES
    ProviderName = 'CO_E_D_CODES_PROV'
    Left = 41
    Top = 21
    object CO_E_D_CODESED_Lookup: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'ED_Lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object CO_E_D_CODESCO_E_D_CODES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_E_D_CODES_NBR'
      Origin = '"CO_E_D_CODES_HIST"."CO_E_D_CODES_NBR"'
      Required = True
      Visible = False
    end
    object CO_E_D_CODESCL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_DS_NBR'
      Origin = '"CO_E_D_CODES_HIST"."CL_E_DS_NBR"'
      Required = True
      Visible = False
    end
    object CO_E_D_CODESCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = '"CO_E_D_CODES_HIST"."CO_NBR"'
      Required = True
      Visible = False
    end
    object CO_E_D_CODESCodeDescription: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CodeDescription'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CL_E_DS_NBR'
      Visible = False
      Size = 40
      Lookup = True
    end
    object CO_E_D_CODESGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"CO_E_D_CODES_HIST"."GENERAL_LEDGER_TAG"'
      Visible = False
    end
    object CO_E_D_CODESDISTRIBUTE: TStringField
      DisplayWidth = 1
      FieldName = 'DISTRIBUTE'
      Origin = '"CO_E_D_CODES_HIST"."DISTRIBUTE"'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_E_D_CODESE_D_Code_Type: TStringField
      FieldKind = fkLookup
      FieldName = 'E_D_Code_Type'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'E_D_CODE_TYPE'
      KeyFields = 'CL_E_DS_NBR'
      Size = 2
      Lookup = True
    end
    object CO_E_D_CODESGL_OFFSET: TStringField
      FieldName = 'GL_OFFSET'
      Size = 15
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 42
    Top = 69
  end
end
