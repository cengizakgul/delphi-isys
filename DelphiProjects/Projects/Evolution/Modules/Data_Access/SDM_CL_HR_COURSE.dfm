inherited DM_CL_HR_COURSE: TDM_CL_HR_COURSE
  OldCreateOrder = True
  Left = 227
  Top = 208
  Height = 540
  Width = 783
  object CL_HR_COURSE: TCL_HR_COURSE
    ProviderName = 'CL_HR_COURSE_PROV'
    BeforePost = CL_HR_COURSEBeforePost
    OnCalcFields = CL_HR_COURSECalcFields
    Left = 72
    Top = 32
    object CL_HR_COURSECL_HR_COURSE_NBR: TIntegerField
      FieldName = 'CL_HR_COURSE_NBR'
    end
    object CL_HR_COURSENAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object CL_HR_COURSERENEWAL_STATUS: TStringField
      FieldName = 'RENEWAL_STATUS'
      Size = 1
    end
    object CL_HR_COURSERENEWAL_STATUS_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'RENEWAL_STATUS_DESC'
      Calculated = True
    end
  end
end
