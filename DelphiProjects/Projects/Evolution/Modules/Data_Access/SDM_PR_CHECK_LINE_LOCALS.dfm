inherited DM_PR_CHECK_LINE_LOCALS: TDM_PR_CHECK_LINE_LOCALS
  OldCreateOrder = False
  Left = 240
  Top = 107
  Height = 480
  Width = 696
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 112
  end
  object PR_CHECK_LINE_LOCALS: TPR_CHECK_LINE_LOCALS
    Aggregates = <>
    Params = <>
    ProviderName = 'PR_CHECK_LINE_LOCALS_PROV'
    BeforeInsert = PR_CHECK_LINE_LOCALSBeforeInsert
    BeforeEdit = PR_CHECK_LINE_LOCALSBeforeEdit
    BeforeDelete = PR_CHECK_LINE_LOCALSBeforeDelete
    OnNewRecord = PR_CHECK_LINE_LOCALSNewRecord
    ValidateWithMask = True
    Left = 200
    Top = 176
    object PR_CHECK_LINE_LOCALSPR_CHECK_LINE_LOCALS_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINE_LOCALS_NBR'
    end
    object PR_CHECK_LINE_LOCALSPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
    end
    object PR_CHECK_LINE_LOCALSEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object PR_CHECK_LINE_LOCALSLocal_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name_Lookup'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object PR_CHECK_LINE_LOCALSEXEMPT_EXCLUDE: TStringField
      FieldName = 'EXEMPT_EXCLUDE'
      Size = 1
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 272
    Top = 112
  end
end
