inherited DM_CO_DIV_PR_BATCH_DEFLT_ED: TDM_CO_DIV_PR_BATCH_DEFLT_ED
  Left = 253
  Top = 107
  Height = 480
  Width = 696
  object DM_COMPANY: TDM_COMPANY
    Left = 200
    Top = 120
  end
  object CO_DIV_PR_BATCH_DEFLT_ED: TCO_DIV_PR_BATCH_DEFLT_ED
    ProviderName = 'CO_DIV_PR_BATCH_DEFLT_ED_PROV'
    BeforePost = CO_DIV_PR_BATCH_DEFLT_EDBeforePost
    Left = 200
    Top = 168
    object CO_DIV_PR_BATCH_DEFLT_EDCO_DIV_PR_BATCH_DEFLT_ED_NBR: TIntegerField
      FieldName = 'CO_DIV_PR_BATCH_DEFLT_ED_NBR'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDCO_E_D_CODES_NBR: TIntegerField
      FieldName = 'CO_E_D_CODES_NBR'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDSALARY_HOURLY_OR_BOTH: TStringField
      FieldName = 'SALARY_HOURLY_OR_BOTH'
      Size = 1
    end
    object CO_DIV_PR_BATCH_DEFLT_EDOVERRIDE_RATE: TFloatField
      FieldName = 'OVERRIDE_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDOVERRIDE_EE_RATE_NUMBER: TIntegerField
      FieldName = 'OVERRIDE_EE_RATE_NUMBER'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDOVERRIDE_HOURS: TFloatField
      FieldName = 'OVERRIDE_HOURS'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDOVERRIDE_AMOUNT: TFloatField
      FieldName = 'OVERRIDE_AMOUNT'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDOVERRIDE_LINE_ITEM_DATE: TDateField
      FieldName = 'OVERRIDE_LINE_ITEM_DATE'
    end
    object CO_DIV_PR_BATCH_DEFLT_EDFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object CO_DIV_PR_BATCH_DEFLT_EDED_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'ED_Lookup'
      LookupDataSet = DM_CO_E_D_CODES.CO_E_D_CODES
      LookupKeyFields = 'CO_E_D_CODES_NBR'
      LookupResultField = 'ED_Lookup'
      KeyFields = 'CO_E_D_CODES_NBR'
      Lookup = True
    end
    object CO_DIV_PR_BATCH_DEFLT_EDFILLTRIM: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'FILLTRIM'
      Size = 3
    end
    object CO_DIV_PR_BATCH_DEFLT_EDCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
    end
  end
end
