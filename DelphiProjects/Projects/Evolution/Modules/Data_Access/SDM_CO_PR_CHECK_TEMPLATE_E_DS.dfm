inherited DM_CO_PR_CHECK_TEMPLATE_E_DS: TDM_CO_PR_CHECK_TEMPLATE_E_DS
  OldCreateOrder = False
  Left = 343
  Top = 187
  Height = 540
  Width = 783
  object CO_PR_CHECK_TEMPLATE_E_DS: TCO_PR_CHECK_TEMPLATE_E_DS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_PR_CHECK_TEMPLATE_E_DS_PROV'
    ValidateWithMask = True
    Left = 48
    Top = 64
    object CO_PR_CHECK_TEMPLATE_E_DSCO_PR_CHECK_TEMPLATE_E_DS_NBR: TIntegerField
      FieldName = 'CO_PR_CHECK_TEMPLATE_E_DS_NBR'
      Origin = '"CO_PR_CHECK_TEMPLATE_E_DS_HIST"."CO_PR_CHECK_TEMPLATE_E_DS_NBR"'
      Required = True
    end
    object CO_PR_CHECK_TEMPLATE_E_DSCO_PR_CHECK_TEMPLATES_NBR: TIntegerField
      FieldName = 'CO_PR_CHECK_TEMPLATES_NBR'
      Origin = '"CO_PR_CHECK_TEMPLATE_E_DS_HIST"."CO_PR_CHECK_TEMPLATES_NBR"'
      Required = True
    end
    object CO_PR_CHECK_TEMPLATE_E_DSCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
      Origin = '"CO_PR_CHECK_TEMPLATE_E_DS_HIST"."CL_E_DS_NBR"'
      Required = True
    end
    object CO_PR_CHECK_TEMPLATE_E_DSCode: TStringField
      FieldKind = fkLookup
      FieldName = 'Code'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object CO_PR_CHECK_TEMPLATE_E_DSDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'Description'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CL_E_DS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 48
    Top = 8
  end
end
