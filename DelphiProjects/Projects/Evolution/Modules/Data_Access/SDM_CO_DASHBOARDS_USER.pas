unit SDM_CO_DASHBOARDS_USER;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses;

type
  TDM_CO_DASHBOARDS_USER = class(TDM_ddTable)
    DM_CLIENT: TDM_CLIENT;
    cO_DASHBOARDS_USER: TCO_DASHBOARDS_USER;
    CO_DASHBOARDS_USERCO_DASHBOARDS_NBR: TIntegerField;
    CO_DASHBOARDS_USERCO_NBR: TIntegerField;
    CO_DASHBOARDS_USERSB_USER_NBR: TIntegerField;
    CO_DASHBOARDS_USERUSER_ID: TStringField;
    procedure CO_DASHBOARDS_USERCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_DASHBOARDS_USER: TDM_CO_DASHBOARDS_USER;

implementation

uses SDataDictbureau;

{$R *.DFM}


procedure TDM_CO_DASHBOARDS_USER.CO_DASHBOARDS_USERCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if not DM_SERVICE_BUREAU.SB_USER.Active then
     DM_SERVICE_BUREAU.SB_USER.DataRequired('ALL');

  if CO_DASHBOARDS_USERSB_USER_NBR.AsInteger > 0  then
     CO_DASHBOARDS_USERUSER_ID.AsString :=
          DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.CachedLookup(CO_DASHBOARDS_USERSB_USER_NBR.AsInteger, 'USER_ID');

end;

initialization
  RegisterClass(TDM_CO_DASHBOARDS_USER);

finalization
  UnregisterClass(TDM_CO_DASHBOARDS_USER);

end.
