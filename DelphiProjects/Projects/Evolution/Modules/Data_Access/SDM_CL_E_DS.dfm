inherited DM_CL_E_DS: TDM_CL_E_DS
  OldCreateOrder = True
  Left = 540
  Top = 228
  Height = 479
  Width = 741
  object CL_E_DS: TCL_E_DS
    ProviderName = 'CL_E_DS_PROV'
    PictureMasks.Strings = (
      
        'OVERRIDE_FED_TAX_VALUE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PAY_PERIOD_MINIMUM_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PAY_PERIOD_MINIMUM_PERCENT_OF'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PAY_PERIOD_MAXIMUM_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PAY_PERIOD_MAXIMUM_PERCENT_OF'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'ANNUAL_MAXIMUM'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]}'#9'T'#9'F'
      
        'MATCH_FIRST_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]}'#9'T'#9'F'
      
        'MATCH_FIRST_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F'
      
        'REGULAR_OT_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'OVERRIDE_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'PIECEWORK_MINIMUM_WAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]}'#9'T'#9'F'
      
        'MATCH_SECOND_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'MATCH_SECOND_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]}'#9'T'#9'F'
      
        'FLAT_MATCH_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      
        'SD_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]' +
        ']),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]' +
        '}'#9'T'#9'F'
      
        'SD_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]])' +
        ',[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9 +
        'T'#9'F')
    FieldDefs = <
      item
        Name = 'CUSTOM_E_D_CODE_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'E_D_CODE_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CL_E_DS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SKIP_HOURS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OVERRIDE_RATE'
        DataType = ftFloat
      end
      item
        Name = 'OVERRIDE_EE_RATE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'OVERRIDE_FED_TAX_VALUE'
        DataType = ftFloat
      end
      item
        Name = 'OVERRIDE_FED_TAX_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'UPDATE_HOURS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DEFAULT_CL_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_EXEMPT_EXCLUDE_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_EXEMPT_EXCLUDE_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_EXEMPT_EXCLUDE_FEDERAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_EXEMPT_EXCLUDE_EIC'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ER_EXEMPT_EXCLUDE_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ER_EXEMPT_EXCLUDE_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ER_EXEMPT_EXCLUDE_FUI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_BOX'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'PAY_PERIOD_MINIMUM_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'PAY_PERIOD_MINIMUM_PERCENT_OF'
        DataType = ftFloat
      end
      item
        Name = 'MIN_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAY_PERIOD_MAXIMUM_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'PAY_PERIOD_MAXIMUM_PERCENT_OF'
        DataType = ftFloat
      end
      item
        Name = 'MAX_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'ANNUAL_MAXIMUM'
        DataType = ftFloat
      end
      item
        Name = 'CL_UNION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_PENSION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_BENEFITS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PRIORITY_TO_EXCLUDE'
        DataType = ftInteger
      end
      item
        Name = 'MAKE_UP_DEDUCTION_SHORTFALL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MATCH_FIRST_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'MATCH_FIRST_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'MATCH_SECOND_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'MATCH_SECOND_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'FLAT_MATCH_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'REGULAR_OT_RATE'
        DataType = ftFloat
      end
      item
        Name = 'OT_ALL_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PIECE_CL_E_D_GROUPS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PIECEWORK_MINIMUM_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'PW_MIN_WAGE_MAKE_UP_METHOD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OFFSET_CL_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SHOW_YTD_ON_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SCHEDULED_DEFAULTS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EFFECTIVE_START_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SD_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'SD_RATE'
        DataType = ftFloat
      end
      item
        Name = 'SD_CALCULATION_METHOD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EXPRESSION'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'SD_AUTO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_ROUND'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EXCLUDE_WEEK_1'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EXCLUDE_WEEK_2'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EXCLUDE_WEEK_3'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EXCLUDE_WEEK_4'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SD_EXCLUDE_WEEK_5'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'OVERRIDE_RATE_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_ON_INPUT_WORKSHEET'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_STATE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GL_OFFSET_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SHOW_ED_ON_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'APPLY_BEFORE_TAXES'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DEFAULT_HOURS'
        DataType = ftFloat
      end>
    Left = 50
    Top = 13
    object CL_E_DSCUSTOM_E_D_CODE_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
      Origin = '"CL_E_DS_HIST"."CUSTOM_E_D_CODE_NUMBER"'
      Required = True
    end
    object CL_E_DSDESCRIPTION: TStringField
      DisplayWidth = 40
      FieldName = 'DESCRIPTION'
      Origin = '"CL_E_DS_HIST"."DESCRIPTION"'
      Required = True
      Size = 40
    end
    object CL_E_DSE_D_CODE_TYPE: TStringField
      DisplayWidth = 2
      FieldName = 'E_D_CODE_TYPE'
      Origin = '"CL_E_DS_HIST"."E_D_CODE_TYPE"'
      Required = True
      Visible = False
      Size = 2
    end
    object CL_E_DSCL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_DS_NBR'
      Origin = '"CL_E_DS_HIST"."CL_E_DS_NBR"'
      Required = True
      Visible = False
    end
    object CL_E_DSCL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_D_GROUPS_NBR'
      Origin = '"CL_E_DS_HIST"."CL_E_D_GROUPS_NBR"'
      Visible = False
    end
    object CL_E_DSSKIP_HOURS: TStringField
      DisplayWidth = 1
      FieldName = 'SKIP_HOURS'
      Origin = '"CL_E_DS_HIST"."SKIP_HOURS"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSOVERRIDE_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_RATE'
      Origin = '"CL_E_DS_HIST"."OVERRIDE_RATE"'
      Visible = False
      DisplayFormat = '##0.####'
      EditFormat = '##0.####'
    end
    object CL_E_DSOVERRIDE_EE_RATE_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_EE_RATE_NUMBER'
      Origin = '"CL_E_DS_HIST"."OVERRIDE_EE_RATE_NUMBER"'
      Visible = False
    end
    object CL_E_DSOVERRIDE_FED_TAX_VALUE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_FED_TAX_VALUE'
      Origin = '"CL_E_DS_HIST"."OVERRIDE_FED_TAX_VALUE"'
      Visible = False
    end
    object CL_E_DSOVERRIDE_FED_TAX_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'OVERRIDE_FED_TAX_TYPE'
      Origin = '"CL_E_DS_HIST"."OVERRIDE_FED_TAX_TYPE"'
      Visible = False
      Size = 1
    end
    object CL_E_DSUPDATE_HOURS: TStringField
      DisplayWidth = 1
      FieldName = 'UPDATE_HOURS'
      Origin = '"CL_E_DS_HIST"."UPDATE_HOURS"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSDEFAULT_CL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEFAULT_CL_AGENCY_NBR'
      Origin = '"CL_E_DS_HIST"."DEFAULT_CL_AGENCY_NBR"'
      Visible = False
    end
    object CL_E_DSEE_EXEMPT_EXCLUDE_OASDI: TStringField
      DisplayWidth = 1
      FieldName = 'EE_EXEMPT_EXCLUDE_OASDI'
      Origin = '"CL_E_DS_HIST"."EE_EXEMPT_EXCLUDE_OASDI"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSEE_EXEMPT_EXCLUDE_MEDICARE: TStringField
      DisplayWidth = 1
      FieldName = 'EE_EXEMPT_EXCLUDE_MEDICARE'
      Origin = '"CL_E_DS_HIST"."EE_EXEMPT_EXCLUDE_MEDICARE"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSEE_EXEMPT_EXCLUDE_FEDERAL: TStringField
      DisplayWidth = 1
      FieldName = 'EE_EXEMPT_EXCLUDE_FEDERAL'
      Origin = '"CL_E_DS_HIST"."EE_EXEMPT_EXCLUDE_FEDERAL"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSEE_EXEMPT_EXCLUDE_EIC: TStringField
      DisplayWidth = 1
      FieldName = 'EE_EXEMPT_EXCLUDE_EIC'
      Origin = '"CL_E_DS_HIST"."EE_EXEMPT_EXCLUDE_EIC"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSER_EXEMPT_EXCLUDE_OASDI: TStringField
      DisplayWidth = 1
      FieldName = 'ER_EXEMPT_EXCLUDE_OASDI'
      Origin = '"CL_E_DS_HIST"."ER_EXEMPT_EXCLUDE_OASDI"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSER_EXEMPT_EXCLUDE_MEDICARE: TStringField
      DisplayWidth = 1
      FieldName = 'ER_EXEMPT_EXCLUDE_MEDICARE'
      Origin = '"CL_E_DS_HIST"."ER_EXEMPT_EXCLUDE_MEDICARE"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSER_EXEMPT_EXCLUDE_FUI: TStringField
      DisplayWidth = 1
      FieldName = 'ER_EXEMPT_EXCLUDE_FUI'
      Origin = '"CL_E_DS_HIST"."ER_EXEMPT_EXCLUDE_FUI"'
      Required = True
      Visible = False
      OnValidate = CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate
      Size = 1
    end
    object CL_E_DSW2_BOX: TStringField
      DisplayWidth = 4
      FieldName = 'W2_BOX'
      Origin = '"CL_E_DS_HIST"."W2_BOX"'
      Visible = False
      Size = 4
    end
    object CL_E_DSPAY_PERIOD_MINIMUM_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'PAY_PERIOD_MINIMUM_AMOUNT'
      Origin = '"CL_E_DS_HIST"."PAY_PERIOD_MINIMUM_AMOUNT"'
      Visible = False
    end
    object CL_E_DSPAY_PERIOD_MINIMUM_PERCENT_OF: TFloatField
      DisplayWidth = 10
      FieldName = 'PAY_PERIOD_MINIMUM_PERCENT_OF'
      Origin = '"CL_E_DS_HIST"."PAY_PERIOD_MINIMUM_PERCENT_OF"'
      Visible = False
    end
    object CL_E_DSMIN_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'MIN_CL_E_D_GROUPS_NBR'
      Origin = '"CL_E_DS_HIST"."MIN_CL_E_D_GROUPS_NBR"'
      Visible = False
    end
    object CL_E_DSPAY_PERIOD_MAXIMUM_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'PAY_PERIOD_MAXIMUM_AMOUNT'
      Origin = '"CL_E_DS_HIST"."PAY_PERIOD_MAXIMUM_AMOUNT"'
      Visible = False
    end
    object CL_E_DSPAY_PERIOD_MAXIMUM_PERCENT_OF: TFloatField
      DisplayWidth = 10
      FieldName = 'PAY_PERIOD_MAXIMUM_PERCENT_OF'
      Origin = '"CL_E_DS_HIST"."PAY_PERIOD_MAXIMUM_PERCENT_OF"'
      Visible = False
    end
    object CL_E_DSMAX_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'MAX_CL_E_D_GROUPS_NBR'
      Origin = '"CL_E_DS_HIST"."MAX_CL_E_D_GROUPS_NBR"'
      Visible = False
    end
    object CL_E_DSANNUAL_MAXIMUM: TFloatField
      DisplayWidth = 10
      FieldName = 'ANNUAL_MAXIMUM'
      Origin = '"CL_E_DS_HIST"."ANNUAL_MAXIMUM"'
      Visible = False
    end
    object CL_E_DSCL_UNION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_UNION_NBR'
      Origin = '"CL_E_DS_HIST"."CL_UNION_NBR"'
      Visible = False
    end
    object CL_E_DSCL_PENSION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_PENSION_NBR'
      Origin = '"CL_E_DS_HIST"."CL_PENSION_NBR"'
      Visible = False
    end
    object CL_E_DSPRIORITY_TO_EXCLUDE: TIntegerField
      DisplayWidth = 10
      FieldName = 'PRIORITY_TO_EXCLUDE'
      Origin = '"CL_E_DS_HIST"."PRIORITY_TO_EXCLUDE"'
      Visible = False
    end
    object CL_E_DSMAKE_UP_DEDUCTION_SHORTFALL: TStringField
      DisplayWidth = 1
      FieldName = 'MAKE_UP_DEDUCTION_SHORTFALL'
      Origin = '"CL_E_DS_HIST"."MAKE_UP_DEDUCTION_SHORTFALL"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSMATCH_FIRST_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'MATCH_FIRST_PERCENTAGE'
      Origin = '"CL_E_DS_HIST"."MATCH_FIRST_PERCENTAGE"'
      Visible = False
    end
    object CL_E_DSMATCH_FIRST_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'MATCH_FIRST_AMOUNT'
      Origin = '"CL_E_DS_HIST"."MATCH_FIRST_AMOUNT"'
      Visible = False
    end
    object CL_E_DSMATCH_SECOND_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'MATCH_SECOND_PERCENTAGE'
      Origin = '"CL_E_DS_HIST"."MATCH_SECOND_PERCENTAGE"'
      Visible = False
    end
    object CL_E_DSMATCH_SECOND_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'MATCH_SECOND_AMOUNT'
      Origin = '"CL_E_DS_HIST"."MATCH_SECOND_AMOUNT"'
      Visible = False
    end
    object CL_E_DSFLAT_MATCH_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'FLAT_MATCH_AMOUNT'
      Origin = '"CL_E_DS_HIST"."FLAT_MATCH_AMOUNT"'
      Visible = False
    end
    object CL_E_DSREGULAR_OT_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'REGULAR_OT_RATE'
      Origin = '"CL_E_DS_HIST"."REGULAR_OT_RATE"'
      Visible = False
      DisplayFormat = '##0.####'
      EditFormat = '##0.####'
    end
    object CL_E_DSOT_ALL_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'OT_ALL_CL_E_D_GROUPS_NBR'
      Origin = '"CL_E_DS_HIST"."OT_ALL_CL_E_D_GROUPS_NBR"'
      Visible = False
    end
    object CL_E_DSPIECE_CL_E_D_GROUPS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PIECE_CL_E_D_GROUPS_NBR'
      Origin = '"CL_E_DS_HIST"."PIECE_CL_E_D_GROUPS_NBR"'
      Visible = False
    end
    object CL_E_DSPIECEWORK_MINIMUM_WAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'PIECEWORK_MINIMUM_WAGE'
      Origin = '"CL_E_DS_HIST"."PIECEWORK_MINIMUM_WAGE"'
      Visible = False
      DisplayFormat = '######.#####'
    end
    object CL_E_DSPW_MIN_WAGE_MAKE_UP_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'PW_MIN_WAGE_MAKE_UP_METHOD'
      Origin = '"CL_E_DS_HIST"."PW_MIN_WAGE_MAKE_UP_METHOD"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSTIP_MINIMUM_WAGE_MAKE_UP_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE'
      Origin = '"CL_E_DS_HIST"."TIP_MINIMUM_WAGE_MAKE_UP_TYPE"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSOFFSET_CL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'OFFSET_CL_E_DS_NBR'
      Origin = '"CL_E_DS_HIST"."OFFSET_CL_E_DS_NBR"'
      Visible = False
    end
    object CL_E_DSCL_3_PARTY_SICK_PAY_ADMIN_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR'
      Origin = '"CL_E_DS_HIST"."CL_3_PARTY_SICK_PAY_ADMIN_NBR"'
      Visible = False
    end
    object CL_E_DSSHOW_YTD_ON_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_YTD_ON_CHECKS'
      Origin = '"CL_E_DS_HIST"."SHOW_YTD_ON_CHECKS"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSCHEDULED_DEFAULTS: TStringField
      DisplayWidth = 1
      FieldName = 'SCHEDULED_DEFAULTS'
      Origin = '"CL_E_DS_HIST"."SCHEDULED_DEFAULTS"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'SD_FREQUENCY'
      Origin = '"CL_E_DS_HIST"."SD_FREQUENCY"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EFFECTIVE_START_DATE: TDateTimeField
      DisplayWidth = 10
      FieldName = 'SD_EFFECTIVE_START_DATE'
      Origin = '"CL_E_DS_HIST"."SD_EFFECTIVE_START_DATE"'
      Visible = False
    end
    object CL_E_DSSD_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'SD_AMOUNT'
      Origin = '"CL_E_DS_HIST"."SD_AMOUNT"'
      Visible = False
      DisplayFormat = '######0.######'
      EditFormat = '######0.######'
    end
    object CL_E_DSSD_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'SD_RATE'
      Origin = '"CL_E_DS_HIST"."SD_RATE"'
      Visible = False
      DisplayFormat = '##0.######'
      EditFormat = '##0.######'
    end
    object CL_E_DSSD_CALCULATION_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'SD_CALCULATION_METHOD'
      Origin = '"CL_E_DS_HIST"."SD_CALCULATION_METHOD"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EXPRESSION: TBlobField
      DisplayWidth = 10
      FieldName = 'SD_EXPRESSION'
      Origin = '"CL_E_DS_HIST"."SD_EXPRESSION"'
      Visible = False
      Size = 8
    end
    object CL_E_DSSD_AUTO: TStringField
      DisplayWidth = 1
      FieldName = 'SD_AUTO'
      Origin = '"CL_E_DS_HIST"."SD_AUTO"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_ROUND: TStringField
      DisplayWidth = 1
      FieldName = 'SD_ROUND'
      Origin = '"CL_E_DS_HIST"."SD_ROUND"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EXCLUDE_WEEK_1: TStringField
      DisplayWidth = 1
      FieldName = 'SD_EXCLUDE_WEEK_1'
      Origin = '"CL_E_DS_HIST"."SD_EXCLUDE_WEEK_1"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EXCLUDE_WEEK_2: TStringField
      DisplayWidth = 1
      FieldName = 'SD_EXCLUDE_WEEK_2'
      Origin = '"CL_E_DS_HIST"."SD_EXCLUDE_WEEK_2"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EXCLUDE_WEEK_3: TStringField
      DisplayWidth = 1
      FieldName = 'SD_EXCLUDE_WEEK_3'
      Origin = '"CL_E_DS_HIST"."SD_EXCLUDE_WEEK_3"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EXCLUDE_WEEK_4: TStringField
      DisplayWidth = 1
      FieldName = 'SD_EXCLUDE_WEEK_4'
      Origin = '"CL_E_DS_HIST"."SD_EXCLUDE_WEEK_4"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSSD_EXCLUDE_WEEK_5: TStringField
      DisplayWidth = 1
      FieldName = 'SD_EXCLUDE_WEEK_5'
      Origin = '"CL_E_DS_HIST"."SD_EXCLUDE_WEEK_5"'
      Required = True
      Visible = False
      Size = 1
    end
    object CL_E_DSFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Origin = '"CL_E_DS_HIST"."FILLER"'
      Visible = False
      Size = 512
    end
    object CL_E_DSOVERRIDE_RATE_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'OVERRIDE_RATE_TYPE'
      Origin = '"CL_E_DS_HIST"."OVERRIDE_RATE_TYPE"'
      Required = True
      Size = 1
    end
    object CL_E_DSSHOW_ON_INPUT_WORKSHEET: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_ON_INPUT_WORKSHEET'
      Origin = '"CL_E_DS_HIST"."SHOW_ON_INPUT_WORKSHEET"'
      Required = True
      Size = 1
    end
    object CL_E_DSSY_STATE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATE_NBR'
      Origin = '"CL_E_DS_HIST"."SY_STATE_NBR"'
    end
    object CL_E_DSGL_OFFSET_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GL_OFFSET_TAG'
      Origin = '"CL_E_DS_HIST"."GL_OFFSET_TAG"'
    end
    object CL_E_DSSHOW_ED_ON_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_ED_ON_CHECKS'
      Origin = '"CL_E_DS_HIST"."SHOW_ED_ON_CHECKS"'
      Required = True
      Size = 1
    end
    object CL_E_DSNOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Origin = '"CL_E_DS_HIST"."NOTES"'
      Size = 8
    end
    object CL_E_DSAPPLY_BEFORE_TAXES: TStringField
      DisplayWidth = 1
      FieldName = 'APPLY_BEFORE_TAXES'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSDEFAULT_HOURS: TFloatField
      DisplayWidth = 10
      FieldName = 'DEFAULT_HOURS'
    end
    object CL_E_DSWHICH_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'WHICH_CHECKS'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSMONTH_NUMBER: TStringField
      DisplayWidth = 1
      FieldName = 'MONTH_NUMBER'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSSD_PLAN_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'SD_PLAN_TYPE'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSSD_WHICH_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'SD_WHICH_CHECKS'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSSD_PRIORITY_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'SD_PRIORITY_NUMBER'
    end
    object CL_E_DSSD_ALWAYS_PAY: TStringField
      DisplayWidth = 1
      FieldName = 'SD_ALWAYS_PAY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSSD_DEDUCTIONS_TO_ZERO: TStringField
      DisplayWidth = 1
      FieldName = 'SD_DEDUCTIONS_TO_ZERO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_E_DSSD_MAX_AVG_AMT_GRP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SD_MAX_AVG_AMT_GRP_NBR'
    end
    object CL_E_DSSD_MAX_AVG_HRS_GRP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SD_MAX_AVG_HRS_GRP_NBR'
    end
    object CL_E_DSSD_MAX_AVERAGE_HOURLY_WAGE_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'SD_MAX_AVERAGE_HOURLY_WAGE_RATE'
      DisplayFormat = '#,####0.0000'
    end
    object CL_E_DSSD_THRESHOLD_E_D_GROUPS_NBR: TIntegerField
      DisplayLabel = 'Threshold E/D Group'
      DisplayWidth = 10
      FieldName = 'SD_THRESHOLD_E_D_GROUPS_NBR'
    end
    object CL_E_DSSD_THRESHOLD_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'SD_THRESHOLD_AMOUNT'
    end
    object CL_E_DSSD_USE_PENSION_LIMIT: TStringField
      DisplayWidth = 1
      FieldName = 'SD_USE_PENSION_LIMIT'
      Required = True
      FixedChar = True
      Size = 1
    end
  end
end
