// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_LOCAL_TAX_LIABILITIES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   EvTypes, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvExceptions,
  EvContext, EvClientDataSet;

type
  TDM_CO_LOCAL_TAX_LIABILITIES = class(TDM_ddTable)
    CO_LOCAL_TAX_LIABILITIES: TCO_LOCAL_TAX_LIABILITIES;
    CO_LOCAL_TAX_LIABILITIESCO_LOCAL_TAX_LIABILITIES_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESCO_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESCO_LOCAL_TAX_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESDUE_DATE: TDateField;
    CO_LOCAL_TAX_LIABILITIESSTATUS: TStringField;
    CO_LOCAL_TAX_LIABILITIESSTATUS_DATE: TDateTimeField;
    CO_LOCAL_TAX_LIABILITIESAMOUNT: TFloatField;
    CO_LOCAL_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField;
    CO_LOCAL_TAX_LIABILITIESPR_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESNOTES: TBlobField;
    CO_LOCAL_TAX_LIABILITIESFILLER: TStringField;
    CO_LOCAL_TAX_LIABILITIESLocal_Name: TStringField;
    CO_LOCAL_TAX_LIABILITIESLocal_State: TStringField;
    CO_LOCAL_TAX_LIABILITIESTHIRD_PARTY: TStringField;
    CO_LOCAL_TAX_LIABILITIESCHECK_DATE: TDateField;
    DM_COMPANY: TDM_COMPANY;
    CO_LOCAL_TAX_LIABILITIESStateDepFreq: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESACH_KEY: TStringField;
    CO_LOCAL_TAX_LIABILITIESIMPOUNDED: TStringField;
    CO_LOCAL_TAX_LIABILITIESTAX_COUPON_SY_REPORTS_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESPERIOD_BEGIN_DATE: TDateField;
    CO_LOCAL_TAX_LIABILITIESPERIOD_END_DATE: TDateField;
    CO_LOCAL_TAX_LIABILITIESADJ_PERIOD_END_DATE: TDateField;
    CO_LOCAL_TAX_LIABILITIESNONRES_CO_LOCAL_TAX_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESpaymentCO_LOCAL_TAX_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESCO_LOCATIONS_NBR: TIntegerField;
    CO_LOCAL_TAX_LIABILITIESACCOUNT_NUMBER: TStringField;
    CO_LOCAL_TAX_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField;
    procedure CO_LOCAL_TAX_LIABILITIESNewRecord(DataSet: TDataSet);
    procedure CO_LOCAL_TAX_LIABILITIESBeforePost(DataSet: TDataSet);
    procedure CO_LOCAL_TAX_LIABILITIESBeforeEdit(DataSet: TDataSet);
    procedure CO_LOCAL_TAX_LIABILITIESBeforeDelete(DataSet: TDataSet);
    procedure CO_LOCAL_TAX_LIABILITIESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIESNewRecord(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('CO_NBR').Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
end;

procedure TDM_CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIESBeforePost(
  DataSet: TDataSet);
begin
  if DataSet.FieldByName('AMOUNT').IsNull then
     raise EUpdateError.CreateHelp('Amount is a required field', IDH_ConsistencyViolation);

  if DataSet.FieldByName('CHECK_DATE').IsNull then
    raise EUpdateError.CreateHelp('Check date is a required field', IDH_ConsistencyViolation);
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);

  if DataSet.FieldByName('STATUS_DATE').IsNull then
     DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

procedure TDM_CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIESBeforeEdit(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIESBeforeDelete(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIESCalcFields(
  DataSet: TDataSet);
var
 s : string;  
begin
  inherited;
  Dataset.FieldByName('paymentCO_LOCAL_TAX_NBR').AsInteger := Dataset.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  if not DataSet.FieldByName('NONRES_CO_LOCAL_TAX_NBR').IsNull then
    Dataset.FieldByName('paymentCO_LOCAL_TAX_NBR').AsInteger := Dataset.FieldByName('NONRES_CO_LOCAL_TAX_NBR').AsInteger;

  s := ExtractFromFiller(Dataset.fieldByName('FILLER').AsString , 41, 10);
  if (s <> '') then
     Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := StrToDate(s)
  else
    if not Dataset.FieldByName('PERIOD_END_DATE').IsNull then
       Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := Dataset.FieldByName('PERIOD_END_DATE').AsDateTime;


end;

initialization
  RegisterClass(TDM_CO_LOCAL_TAX_LIABILITIES);

finalization
  UnregisterClass(TDM_CO_LOCAL_TAX_LIABILITIES);

end.
