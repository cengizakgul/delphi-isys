inherited DM_SB_ENLIST_GROUPS: TDM_SB_ENLIST_GROUPS
  OldCreateOrder = True
  Left = 525
  Top = 114
  Height = 204
  Width = 464
  object SB_ENLIST_GROUPS: TSB_ENLIST_GROUPS
    ProviderName = 'SB_ENLIST_GROUPS_PROV'
    OnCalcFields = SB_ENLIST_GROUPSCalcFields
    Left = 86
    Top = 32
    object SB_ENLIST_GROUPSMEDIA_TYPE: TStringField
      FieldName = 'MEDIA_TYPE'
      Size = 2
    end
    object SB_ENLIST_GROUPSPROCESS_TYPE: TStringField
      FieldName = 'PROCESS_TYPE'
      Size = 1
    end
    object SB_ENLIST_GROUPSSB_ENLIST_GROUPS_NBR: TIntegerField
      FieldName = 'SB_ENLIST_GROUPS_NBR'
    end
    object SB_ENLIST_GROUPSSY_REPORT_GROUPS_NBR: TIntegerField
      FieldName = 'SY_REPORT_GROUPS_NBR'
    end
    object SB_ENLIST_GROUPSReportGroups: TStringField
      FieldKind = fkCalculated
      FieldName = 'ReportGroups'
      Size = 40
      Calculated = True
    end
    object SB_ENLIST_GROUPSProcess: TStringField
      DisplayWidth = 12
      FieldKind = fkCalculated
      FieldName = 'Process'
      Size = 12
      Calculated = True
    end
    object SB_ENLIST_GROUPSNMediaType: TStringField
      FieldKind = fkCalculated
      FieldName = 'NMediaType'
      Size = 40
      Calculated = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 208
    Top = 40
  end
end
