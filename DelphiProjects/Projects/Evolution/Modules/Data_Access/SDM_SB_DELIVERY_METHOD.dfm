inherited DM_SB_DELIVERY_METHOD: TDM_SB_DELIVERY_METHOD
  Left = 540
  Top = 303
  Height = 174
  Width = 231
  object SB_DELIVERY_METHOD: TSB_DELIVERY_METHOD
    ProviderName = 'SB_DELIVERY_METHOD_PROV'
    Left = 48
    Top = 24
    object SB_DELIVERY_METHODSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object SB_DELIVERY_METHODSY_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SY_DELIVERY_METHOD_NBR'
    end
    object SB_DELIVERY_METHODluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SY_DELIVERY_METHOD.SY_DELIVERY_METHOD
      LookupKeyFields = 'SY_DELIVERY_METHOD_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object SB_DELIVERY_METHODCLASS_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'CLASS_NAME'
      LookupDataSet = DM_SY_DELIVERY_METHOD.SY_DELIVERY_METHOD
      LookupKeyFields = 'SY_DELIVERY_METHOD_NBR'
      LookupResultField = 'CLASS_NAME'
      KeyFields = 'SY_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object SB_DELIVERY_METHODluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SY_DELIVERY_METHOD.SY_DELIVERY_METHOD
      LookupKeyFields = 'SY_DELIVERY_METHOD_NBR'
      LookupResultField = 'SERVICE_NAME'
      KeyFields = 'SY_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 128
    Top = 80
  end
end
