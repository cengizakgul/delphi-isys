// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_LOCAL_MARITAL_STATUS;

interface

uses
  SDM_ddTable, SDataDictSystem,
   SDataStructure, Db, Classes,  Forms, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents;

type
  TDM_SY_LOCAL_MARITAL_STATUS = class(TDM_ddTable)
    SY_LOCAL_MARITAL_STATUS: TSY_LOCAL_MARITAL_STATUS;
    SY_LOCAL_MARITAL_STATUSSY_LOCAL_MARITAL_STATUS_NBR: TIntegerField;
    SY_LOCAL_MARITAL_STATUSSY_LOCALS_NBR: TIntegerField;
    SY_LOCAL_MARITAL_STATUSSY_STATE_MARITAL_STATUS_NBR: TIntegerField;
    SY_LOCAL_MARITAL_STATUSSTANDARD_DEDUCTION_AMOUNT: TFloatField;
    SY_LOCAL_MARITAL_STATUSSTANDARD_EXEMPTION_ALLOW: TFloatField;
    SY_LOCAL_MARITAL_STATUSFILLER: TStringField;
    SY_LOCAL_MARITAL_STATUSSTATUS_DESCRIPTION: TStringField;
    SY_LOCAL_MARITAL_STATUSSTATUS_TYPE: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_LOCAL_MARITAL_STATUS);

finalization
  UnregisterClass(TDM_SY_LOCAL_MARITAL_STATUS);

end.
