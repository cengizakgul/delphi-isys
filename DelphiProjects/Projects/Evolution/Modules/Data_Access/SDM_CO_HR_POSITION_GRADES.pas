unit SDM_CO_HR_POSITION_GRADES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils, EvClientDataSet;

type
  TDM_CO_HR_POSITION_GRADES = class(TDM_ddTable)
    CO_HR_POSITION_GRADES: TCO_HR_POSITION_GRADES;
    CO_HR_POSITION_GRADESCO_HR_POSITIONS_NBR: TIntegerField;
    CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR: TIntegerField;
    CO_HR_POSITION_GRADESDESCRIPTION: TStringField;
    CO_HR_POSITION_GRADESMINIMUM_PAY: TCurrencyField;
    CO_HR_POSITION_GRADESMID_PAY: TCurrencyField;
    CO_HR_POSITION_GRADESMAXIMUM_PAY: TCurrencyField;
    DM_COMPANY: TDM_COMPANY;
    CO_HR_POSITION_GRADESCO_HR_POSITION_GRADES_NBR: TIntegerField;
    procedure CO_HR_POSITION_GRADESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_HR_POSITION_GRADES: TDM_CO_HR_POSITION_GRADES;

implementation

{$R *.DFM}

procedure TDM_CO_HR_POSITION_GRADES.CO_HR_POSITION_GRADESCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR.AsInteger > 0)  and
     (DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) then
  begin
    if not DM_COMPANY.CO_HR_SALARY_GRADES.Active then
       DM_COMPANY.CO_HR_SALARY_GRADES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

    CO_HR_POSITION_GRADESDESCRIPTION.AsVariant :=
        DM_COMPANY.CO_HR_SALARY_GRADES.Lookup('CO_HR_SALARY_GRADES_NBR', CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR.AsVariant, 'DESCRIPTION');
    CO_HR_POSITION_GRADESMINIMUM_PAY.AsVariant :=
        DM_COMPANY.CO_HR_SALARY_GRADES.LookUp('CO_HR_SALARY_GRADES_NBR', CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR.AsVariant, 'MINIMUM_PAY');
    CO_HR_POSITION_GRADESMID_PAY.AsVariant :=
        DM_COMPANY.CO_HR_SALARY_GRADES.LookUp('CO_HR_SALARY_GRADES_NBR', CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR.AsVariant, 'MID_PAY');
    CO_HR_POSITION_GRADESMAXIMUM_PAY.AsVariant :=
        DM_COMPANY.CO_HR_SALARY_GRADES.LookUp('CO_HR_SALARY_GRADES_NBR', CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR.AsVariant, 'MAXIMUM_PAY');
  end;

end;

initialization
  RegisterClass(TDM_CO_HR_POSITION_GRADES);

finalization
  UnregisterClass(TDM_CO_HR_POSITION_GRADES);

end.
