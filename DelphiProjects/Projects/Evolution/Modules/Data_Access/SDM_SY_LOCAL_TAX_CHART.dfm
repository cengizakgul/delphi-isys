inherited DM_SY_LOCAL_TAX_CHART: TDM_SY_LOCAL_TAX_CHART
  Left = 367
  Top = 562
  Height = 266
  Width = 426
  object SY_LOCAL_TAX_CHART: TSY_LOCAL_TAX_CHART
    ProviderName = 'SY_LOCAL_TAX_CHART_PROV'
    ClientID = -1
    Left = 78
    Top = 36
    object SY_LOCAL_TAX_CHARTMARITAL_STATUS_DESCRPTION: TStringField
      DisplayLabel = 'MARITAL_STATUS'
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'MARITAL_STATUS_DESCRPTION'
      LookupDataSet = DM_SY_LOCAL_MARITAL_STATUS.SY_LOCAL_MARITAL_STATUS
      LookupKeyFields = 'SY_LOCAL_MARITAL_STATUS_NBR'
      LookupResultField = 'STATUS_DESCRIPTION'
      KeyFields = 'SY_LOCAL_MARITAL_STATUS_NBR'
      Lookup = True
    end
    object SY_LOCAL_TAX_CHARTSY_LOCAL_TAX_CHART_NBR: TIntegerField
      FieldName = 'SY_LOCAL_TAX_CHART_NBR'
    end
    object SY_LOCAL_TAX_CHARTSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
    object SY_LOCAL_TAX_CHARTSY_LOCAL_MARITAL_STATUS_NBR: TIntegerField
      FieldName = 'SY_LOCAL_MARITAL_STATUS_NBR'
    end
    object SY_LOCAL_TAX_CHARTMINIMUM: TFloatField
      FieldName = 'MINIMUM'
    end
    object SY_LOCAL_TAX_CHARTMAXIMUM: TFloatField
      FieldName = 'MAXIMUM'
    end
    object SY_LOCAL_TAX_CHARTPERCENTAGE: TFloatField
      FieldName = 'PERCENTAGE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_LOCAL_TAX_CHARTType: TStringField
      FieldKind = fkLookup
      FieldName = 'LocalType'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'LOCAL_TYPE'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 2
      Lookup = True
    end
    object SY_LOCAL_TAX_CHARTSY_STATE_MARITAL_STATUS_NBR: TIntegerField
      FieldKind = fkLookup
      FieldName = 'SY_STATE_MARITAL_STATUS_NBR'
      LookupDataSet = DM_SY_LOCAL_MARITAL_STATUS.SY_LOCAL_MARITAL_STATUS
      LookupKeyFields = 'SY_LOCAL_MARITAL_STATUS_NBR'
      LookupResultField = 'SY_STATE_MARITAL_STATUS_NBR'
      KeyFields = 'SY_LOCAL_MARITAL_STATUS_NBR'
      Lookup = True
    end
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 87
    Top = 111
  end
end
