inherited DM_CL: TDM_CL
  OldCreateOrder = True
  Left = 279
  Top = 107
  Height = 236
  Width = 400
  object CL: TCL
    ProviderName = 'CL_PROV'
    PictureMasks.Strings = (
      'ZIP_CODE'#9'*5{#,?}[-*4{#,?}]'#9'T'#9'F'
      'FAX1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'FAX2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    FieldDefs = <
      item
        Name = 'ACCOUNTANT_CONTACT'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ADDRESS1'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ADDRESS2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'AUTO_SAVE_MINUTES'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CITY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CL_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CONTACT1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CONTACT2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CSR_SB_TEAM_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOMER_SERVICE_SB_USER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_CLIENT_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DESCRIPTION2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ENABLE_HR'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'E_MAIL_ADDRESS1'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'E_MAIL_ADDRESS2'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'FAX1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FAX1_DESCRIPTION'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FAX2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FAX2_DESCRIPTION'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'LEASING_COMPANY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PHONE1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PHONE2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PRINT_CPA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RECIPROCATE_SUI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SB_ACCOUNTANT_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SECURITY_FONT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'START_DATE'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'STATE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'TERMINATION_CODE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TERMINATION_DATE'
        DataType = ftDate
      end
      item
        Name = 'TERMINATION_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'ZIP_CODE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'AGENCY_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_REPORT_SECOND_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_EE_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_RETURN_SECOND_MB_GROUP_NBR'
        DataType = ftInteger
      end>
    OnNewRecord = CLNewRecord
    Left = 54
    Top = 36
    object CLACCOUNTANT_CONTACT: TStringField
      DisplayWidth = 40
      FieldName = 'ACCOUNTANT_CONTACT'
      Origin = '"CL_HIST"."ACCOUNTANT_CONTACT"'
      Size = 40
    end
    object CLADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"CL_HIST"."ADDRESS1"'
      Required = True
      Size = 30
    end
    object CLADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"CL_HIST"."ADDRESS2"'
      Size = 30
    end
    object CLAUTO_SAVE_MINUTES: TIntegerField
      DisplayWidth = 10
      FieldName = 'AUTO_SAVE_MINUTES'
      Origin = '"CL_HIST"."AUTO_SAVE_MINUTES"'
      Required = True
    end
    object CLCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CL_HIST"."CITY"'
      Required = True
    end
    object CLCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
      Origin = '"CL_HIST"."CL_NBR"'
      Required = True
    end
    object CLCONTACT1: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT1'
      Origin = '"CL_HIST"."CONTACT1"'
      Size = 30
    end
    object CLCONTACT2: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT2'
      Origin = '"CL_HIST"."CONTACT2"'
      Size = 30
    end
    object CLCSR_SB_TEAM_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CSR_SB_TEAM_NBR'
      Origin = '"CL_HIST"."CSR_SB_TEAM_NBR"'
    end
    object CLCUSTOMER_SERVICE_SB_USER_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CUSTOMER_SERVICE_SB_USER_NBR'
      Origin = '"CL_HIST"."CUSTOMER_SERVICE_SB_USER_NBR"'
    end
    object CLCUSTOM_CLIENT_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_CLIENT_NUMBER'
      Origin = '"CL_HIST"."CUSTOM_CLIENT_NUMBER"'
      Required = True
    end
    object CLDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"CL_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object CLDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"CL_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object CLENABLE_HR: TStringField
      DisplayWidth = 1
      FieldName = 'ENABLE_HR'
      Origin = '"CL_HIST"."ENABLE_HR"'
      Required = True
      Size = 1
    end
    object CLE_MAIL_ADDRESS1: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS1'
      Origin = '"CL_HIST"."E_MAIL_ADDRESS1"'
      Size = 80
    end
    object CLE_MAIL_ADDRESS2: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS2'
      Origin = '"CL_HIST"."E_MAIL_ADDRESS2"'
      Size = 80
    end
    object CLFAX1: TStringField
      DisplayWidth = 20
      FieldName = 'FAX1'
      Origin = '"CL_HIST"."FAX1"'
    end
    object CLFAX1_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX1_DESCRIPTION'
      Origin = '"CL_HIST"."FAX1_DESCRIPTION"'
      Size = 10
    end
    object CLFAX2: TStringField
      DisplayWidth = 20
      FieldName = 'FAX2'
      Origin = '"CL_HIST"."FAX2"'
    end
    object CLFAX2_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX2_DESCRIPTION'
      Origin = '"CL_HIST"."FAX2_DESCRIPTION"'
      Size = 10
    end
    object CLFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CL_HIST"."FILLER"'
      Size = 512
    end
    object CLLEASING_COMPANY: TStringField
      DisplayWidth = 1
      FieldName = 'LEASING_COMPANY'
      Origin = '"CL_HIST"."LEASING_COMPANY"'
      Required = True
      Size = 1
    end
    object CLNAME: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = '"CL_HIST"."NAME"'
      Required = True
      Size = 40
    end
    object CLPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"CL_HIST"."PHONE1"'
    end
    object CLPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"CL_HIST"."PHONE2"'
    end
    object CLPRINT_CPA: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_CPA'
      Origin = '"CL_HIST"."PRINT_CPA"'
      Required = True
      Size = 1
    end
    object CLRECIPROCATE_SUI: TStringField
      DisplayWidth = 1
      FieldName = 'RECIPROCATE_SUI'
      Origin = '"CL_HIST"."RECIPROCATE_SUI"'
      Required = True
      Size = 1
    end
    object CLSB_ACCOUNTANT_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_ACCOUNTANT_NUMBER'
      Origin = '"CL_HIST"."SB_ACCOUNTANT_NUMBER"'
    end
    object CLSECURITY_FONT: TStringField
      DisplayWidth = 1
      FieldName = 'SECURITY_FONT'
      Origin = '"CL_HIST"."SECURITY_FONT"'
      Required = True
      Size = 1
    end
    object CLSTART_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'START_DATE'
      Origin = '"CL_HIST"."START_DATE"'
      Required = True
    end
    object CLSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CL_HIST"."STATE"'
      Required = True
      Size = 2
    end
    object CLTERMINATION_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'TERMINATION_CODE'
      Origin = '"CL_HIST"."TERMINATION_CODE"'
      Size = 1
    end
    object CLTERMINATION_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'TERMINATION_DATE'
      Origin = '"CL_HIST"."TERMINATION_DATE"'
    end
    object CLTERMINATION_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'TERMINATION_NOTES'
      Origin = '"CL_HIST"."TERMINATION_NOTES"'
      Size = 8
    end
    object CLZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CL_HIST"."ZIP_CODE"'
      Required = True
      Size = 10
    end
    object CLAGENCY_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'AGENCY_CHECK_MB_GROUP_NBR'
    end
    object CLPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
    end
    object CLPR_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_REPORT_MB_GROUP_NBR'
    end
    object CLPR_REPORT_SECOND_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_REPORT_SECOND_MB_GROUP_NBR'
    end
    object CLTAX_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CHECK_MB_GROUP_NBR'
    end
    object CLTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
    end
    object CLTAX_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_MB_GROUP_NBR'
    end
    object CLTAX_RETURN_SECOND_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_SECOND_MB_GROUP_NBR'
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 160
    Top = 40
  end
end
