inherited DM_CO_TIME_OFF_ACCRUAL_RATES: TDM_CO_TIME_OFF_ACCRUAL_RATES
  OldCreateOrder = False
  Left = 215
  Top = 205
  Height = 540
  Width = 783
  object CO_TIME_OFF_ACCRUAL_RATES: TCO_TIME_OFF_ACCRUAL_RATES
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_TIME_OFF_ACCRUAL_RATES_PROV'
    ValidateWithMask = True
    Left = 78
    Top = 33
    object CO_TIME_OFF_ACCRUAL_RATESCO_TIME_OFF_ACCRUAL_RATES_NBR: TIntegerField
      FieldName = 'CO_TIME_OFF_ACCRUAL_RATES_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."CO_TIME_OFF_ACCRUAL_RATES_NBR"'
      Required = True
    end
    object CO_TIME_OFF_ACCRUAL_RATESCO_TIME_OFF_ACCRUAL_NBR: TIntegerField
      FieldName = 'CO_TIME_OFF_ACCRUAL_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."CO_TIME_OFF_ACCRUAL_NBR"'
      Required = True
    end
    object CO_TIME_OFF_ACCRUAL_RATESMINIMUM_MONTH_NUMBER: TFloatField
      FieldName = 'MINIMUM_MONTH_NUMBER'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."MINIMUM_MONTH_NUMBER"'
    end
    object CO_TIME_OFF_ACCRUAL_RATESMAXIMUM_MONTH_NUMBER: TFloatField
      FieldName = 'MAXIMUM_MONTH_NUMBER'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."MAXIMUM_MONTH_NUMBER"'
    end
    object CO_TIME_OFF_ACCRUAL_RATESHOURS: TFloatField
      FieldName = 'HOURS'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."HOURS"'
      DisplayFormat = '#,##0.00####'
    end
    object CO_TIME_OFF_ACCRUAL_RATESMAXIMUM_CARRYOVER: TFloatField
      FieldName = 'MAXIMUM_CARRYOVER'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."MAXIMUM_CARRYOVER"'
    end
    object CO_TIME_OFF_ACCRUAL_RATESANNUAL_ACCRUAL_MAXIMUM: TFloatField
      FieldName = 'ANNUAL_ACCRUAL_MAXIMUM'
      Origin = '"CO_TIME_OFF_ACCRUAL_RATES_HIST"."ANNUAL_ACCRUAL_MAXIMUM"'
    end
    object CO_TIME_OFF_ACCRUAL_RATESRateDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'RateDescription'
      LookupDataSet = DM_CO_TIME_OFF_ACCRUAL.CO_TIME_OFF_ACCRUAL
      LookupKeyFields = 'CO_TIME_OFF_ACCRUAL_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_TIME_OFF_ACCRUAL_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 248
    Top = 72
  end
end
