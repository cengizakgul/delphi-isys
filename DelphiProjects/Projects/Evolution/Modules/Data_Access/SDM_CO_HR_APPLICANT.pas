// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_HR_APPLICANT;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   Variants, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvUIComponents,
  EvUtils, EvClientDataSet;
  
type
  TDM_CO_HR_APPLICANT = class(TDM_ddTable)
    CO_HR_APPLICANT: TCO_HR_APPLICANT;
    DM_CLIENT: TDM_CLIENT;
    CO_HR_APPLICANTAPPLICANT_CONTACTED: TStringField;
    CO_HR_APPLICANTAPPLICANT_CONTACTED_BY: TStringField;
    CO_HR_APPLICANTAPPLICANT_CONTACTED_DATE: TDateTimeField;
    CO_HR_APPLICANTAPPLICANT_STATUS: TStringField;
    CO_HR_APPLICANTAPPLICATION_DATE: TDateField;
    CO_HR_APPLICANTCL_PERSON_NBR: TIntegerField;
    CO_HR_APPLICANTCO_BRANCH_NBR: TIntegerField;
    CO_HR_APPLICANTCO_DEPARTMENT_NBR: TIntegerField;
    CO_HR_APPLICANTCO_DIVISION_NBR: TIntegerField;
    CO_HR_APPLICANTCO_HR_APPLICANT_NBR: TIntegerField;
    CO_HR_APPLICANTCO_HR_CAR_NBR: TIntegerField;
    CO_HR_APPLICANTCO_HR_RECRUITERS_NBR: TIntegerField;
    CO_HR_APPLICANTCO_HR_REFERRALS_NBR: TIntegerField;
    CO_HR_APPLICANTCO_JOBS_NBR: TIntegerField;
    CO_HR_APPLICANTCO_NBR: TIntegerField;
    CO_HR_APPLICANTCO_TEAM_NBR: TIntegerField;
    CO_HR_APPLICANTDATE_POSITION_ACCEPTED: TDateTimeField;
    CO_HR_APPLICANTDATE_POSITION_OFFERED: TDateTimeField;
    CO_HR_APPLICANTDESIRED_RATE: TFloatField;
    CO_HR_APPLICANTDESIRED_REGULAR_OR_TEMPORARY: TStringField;
    CO_HR_APPLICANTDESIRED_SALARY: TFloatField;
    CO_HR_APPLICANTDESIRED_SHIFT: TIntegerField;
    CO_HR_APPLICANTGED: TStringField;
    CO_HR_APPLICANTGED_DATE: TDateField;
    CO_HR_APPLICANTNOTES: TBlobField;
    CO_HR_APPLICANTPOSITION_STATUS: TStringField;
    CO_HR_APPLICANTSECURITY_CLEARANCE: TStringField;
    CO_HR_APPLICANTSTART_DATE: TDateField;
    CO_HR_APPLICANTFirstName: TStringField;
    CO_HR_APPLICANTLastName: TStringField;
    CO_HR_APPLICANTSSNLookup: TStringField;
    CO_HR_APPLICANTAPPLICANT_TYPE: TStringField;
    CO_HR_APPLICANTCO_HR_SUPERVISORS_NBR: TIntegerField;
    CO_HR_APPLICANTName_Calculate: TStringField;
    CO_HR_APPLICANTE_MAIL_Calc: TStringField;
    CO_HR_APPLICANTBranch_Name: TStringField;
    CO_HR_APPLICANTDepartment_Name: TStringField;
    CO_HR_APPLICANTDivision_Name: TStringField;
    CO_HR_APPLICANTTeam_Name: TStringField;
    CO_HR_APPLICANTJobs_Nbr: TStringField;
    CO_HR_APPLICANTPosition: TStringField;
    procedure CO_HR_APPLICANTNewRecord(DataSet: TDataSet);
    procedure CO_HR_APPLICANTCalcFields(DataSet: TDataSet);
    procedure CO_HR_APPLICANTAddLookups(var A: TArrayDS);
    procedure CO_HR_APPLICANTPrepareLookups(var Lookups: TLookupArray);
    procedure CO_HR_APPLICANTUnPrepareLookups;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

procedure TDM_CO_HR_APPLICANT.CO_HR_APPLICANTNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('CL_PERSON_NBR').AsString := DM_CLIENT.CL_PERSON.FieldByName('CL_PERSON_NBR').AsString;
end;

procedure TDM_CO_HR_APPLICANT.CO_HR_APPLICANTCalcFields(DataSet: TDataSet);
begin
  if not TevClientDataSet(Dataset).LookupsEnabled then
    Exit;

   if not DM_CLIENT.CL_PERSON.Active then
      DM_CLIENT.CL_PERSON.DataRequired('ALL');

   if CO_HR_APPLICANTCL_PERSON_NBR.AsInteger > 0 then
   begin
     if DM_CLIENT.CL_PERSON.ShadowDataSet.Locate('CL_PERSON_NBR', CO_HR_APPLICANTCL_PERSON_NBR.Value, []) then
     begin
        CO_HR_APPLICANTLastName.AsVariant := DM_CLIENT.CL_PERSON.ShadowDataSet.FieldValues['LAST_NAME'];
        CO_HR_APPLICANTFirstName.AsVariant := DM_CLIENT.CL_PERSON.ShadowDataSet.FieldValues['FIRST_NAME'];
        CO_HR_APPLICANTName_Calculate.AsString := Trim(DM_CLIENT.CL_PERSON.ShadowDataSet.fieldByName('LAST_NAME').AsString + ' ' + DM_CLIENT.CL_PERSON.ShadowDataSet.fieldByName('FIRST_NAME').AsString);
        CO_HR_APPLICANTSSNLookup.AsVariant := DM_CLIENT.CL_PERSON.ShadowDataSet.FieldValues['SOCIAL_SECURITY_NUMBER'];
        CO_HR_APPLICANTE_MAIL_Calc.AsVariant := DM_CLIENT.CL_PERSON.ShadowDataSet.FieldValues['E_MAIL_ADDRESS'];
     end;
   end;
end;

procedure TDM_CO_HR_APPLICANT.CO_HR_APPLICANTAddLookups(var A: TArrayDS);
begin
  AddDS(DM_CLIENT.CL_PERSON, A);
end;

procedure TDM_CO_HR_APPLICANT.CO_HR_APPLICANTPrepareLookups(
  var Lookups: TLookupArray);
var
  iPos: Integer;
begin
  iPos := Length(Lookups);
  SetLength(Lookups, iPos + 4);
  with Lookups[iPos] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'FIRST_NAME';
  end;
  with Lookups[iPos + 1] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'LAST_NAME';
  end;
  with Lookups[iPos + 2] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'SOCIAL_SECURITY_NUMBER';
  end;
  with Lookups[iPos + 3] do
  begin
    DataSet := DM_CLIENT.CL_PERSON;
    sKeyField := 'CL_PERSON_NBR';
    sLookupResultField := 'E_MAIL_ADDRESS';
  end;
end;

procedure TDM_CO_HR_APPLICANT.CO_HR_APPLICANTUnPrepareLookups;
begin
  CO_HR_APPLICANT.SetLookupFieldTag(CO_HR_APPLICANTLastName, 'LAST_NAME', DM_CLIENT.CL_PERSON);
  CO_HR_APPLICANT.SetLookupFieldTag(CO_HR_APPLICANTFirstName, 'FIRST_NAME', DM_CLIENT.CL_PERSON);
  CO_HR_APPLICANT.SetLookupFieldTag(CO_HR_APPLICANTSSNLookup, 'SOCIAL_SECURITY_NUMBER', DM_CLIENT.CL_PERSON);
  CO_HR_APPLICANT.SetLookupFieldTag(CO_HR_APPLICANTE_MAIL_Calc, 'E_MAIL_ADDRESS', DM_CLIENT.CL_PERSON);
end;

initialization
  RegisterClass( TDM_CO_HR_APPLICANT );

finalization
  UnregisterClass( TDM_CO_HR_APPLICANT );

end.
