inherited DM_CO_LOCAL_TAX_LIABILITIES: TDM_CO_LOCAL_TAX_LIABILITIES
  OldCreateOrder = True
  Left = 539
  Top = 185
  Height = 479
  Width = 741
  object CO_LOCAL_TAX_LIABILITIES: TCO_LOCAL_TAX_LIABILITIES
    ProviderName = 'CO_LOCAL_TAX_LIABILITIES_PROV'
    PictureMasks.Strings = (
      
        'AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),' +
        '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9'T' +
        #9'F')
    BeforeEdit = CO_LOCAL_TAX_LIABILITIESBeforeEdit
    BeforePost = CO_LOCAL_TAX_LIABILITIESBeforePost
    BeforeDelete = CO_LOCAL_TAX_LIABILITIESBeforeDelete
    OnCalcFields = CO_LOCAL_TAX_LIABILITIESCalcFields
    OnNewRecord = CO_LOCAL_TAX_LIABILITIESNewRecord
    Left = 80
    Top = 26
    object CO_LOCAL_TAX_LIABILITIESCO_LOCAL_TAX_LIABILITIES_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_LIABILITIES_NBR'
      Required = True
    end
    object CO_LOCAL_TAX_LIABILITIESCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Required = True
    end
    object CO_LOCAL_TAX_LIABILITIESCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
      Required = True
    end
    object CO_LOCAL_TAX_LIABILITIESDUE_DATE: TDateField
      FieldName = 'DUE_DATE'
      Required = True
    end
    object CO_LOCAL_TAX_LIABILITIESSTATUS: TStringField
      FieldName = 'STATUS'
      Required = True
      Size = 1
    end
    object CO_LOCAL_TAX_LIABILITIESSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object CO_LOCAL_TAX_LIABILITIESAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      Required = True
      currency = True
    end
    object CO_LOCAL_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField
      FieldName = 'ADJUSTMENT_TYPE'
      Size = 1
    end
    object CO_LOCAL_TAX_LIABILITIESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField
      FieldName = 'IMPOUND_CO_BANK_ACCT_REG_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESNOTES: TBlobField
      FieldName = 'NOTES'
      Size = 8
    end
    object CO_LOCAL_TAX_LIABILITIESFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object CO_LOCAL_TAX_LIABILITIESLocal_Name: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'Local_Name'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 40
      Lookup = True
    end
    object CO_LOCAL_TAX_LIABILITIESLocal_State: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_State'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'localState'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 2
      Lookup = True
    end
    object CO_LOCAL_TAX_LIABILITIESTHIRD_PARTY: TStringField
      FieldName = 'THIRD_PARTY'
      Required = True
      Size = 1
    end
    object CO_LOCAL_TAX_LIABILITIESCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object CO_LOCAL_TAX_LIABILITIESStateDepFreq: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'FIELD_OFFICE_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESACH_KEY: TStringField
      FieldName = 'ACH_KEY'
      Size = 11
    end
    object CO_LOCAL_TAX_LIABILITIESIMPOUNDED: TStringField
      FieldName = 'IMPOUNDED'
      Required = True
      Size = 1
    end
    object CO_LOCAL_TAX_LIABILITIESTAX_COUPON_SY_REPORTS_NBR: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'TAX_COUPON_SY_REPORTS_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESPERIOD_BEGIN_DATE: TDateField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object CO_LOCAL_TAX_LIABILITIESPERIOD_END_DATE: TDateField
      FieldName = 'PERIOD_END_DATE'
    end
    object CO_LOCAL_TAX_LIABILITIESADJ_PERIOD_END_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'ADJ_PERIOD_END_DATE'
    end
    object CO_LOCAL_TAX_LIABILITIESLocal_Type: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Type'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LOCAL_TYPE'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Lookup = True
    end
    object CO_LOCAL_TAX_LIABILITIESNONRES_CO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'NONRES_CO_LOCAL_TAX_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESpaymentCO_LOCAL_TAX_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'paymentCO_LOCAL_TAX_NBR'
      Calculated = True
    end
    object CO_LOCAL_TAX_LIABILITIESCO_LOCATIONS_NBR: TIntegerField
      FieldName = 'CO_LOCATIONS_NBR'
    end
    object CO_LOCAL_TAX_LIABILITIESACCOUNT_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'ACCOUNT_NUMBER'
      LookupDataSet = DM_CO_LOCATIONS.CO_LOCATIONS
      LookupKeyFields = 'CO_LOCATIONS_NBR'
      LookupResultField = 'ACCOUNT_NUMBER'
      KeyFields = 'CO_LOCATIONS_NBR'
      Size = 30
      Lookup = True
    end
    object CO_LOCAL_TAX_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CALC_PERIOD_END_DATE'
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 81
    Top = 96
  end
end
