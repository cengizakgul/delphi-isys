inherited DM_CO_DEPARTMENT_LOCALS: TDM_CO_DEPARTMENT_LOCALS
  OldCreateOrder = False
  Left = 201
  Top = 273
  Height = 375
  Width = 544
  object DM_COMPANY: TDM_COMPANY
    Left = 112
    Top = 104
  end
  object CO_DEPARTMENT_LOCALS: TCO_DEPARTMENT_LOCALS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_DEPARTMENT_LOCALS_PROV'
    ValidateWithMask = True
    Left = 96
    Top = 8
    object CO_DEPARTMENT_LOCALSLocalName: TStringField
      DisplayLabel = 'Local Name'
      FieldKind = fkLookup
      FieldName = 'LocalName'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 40
      Lookup = True
    end
    object CO_DEPARTMENT_LOCALSCO_DEPARTMENT_LOCALS_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_LOCALS_NBR'
    end
    object CO_DEPARTMENT_LOCALSCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object CO_DEPARTMENT_LOCALSCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
  end
end
