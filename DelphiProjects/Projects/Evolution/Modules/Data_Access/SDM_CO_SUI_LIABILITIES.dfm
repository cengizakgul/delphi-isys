inherited DM_CO_SUI_LIABILITIES: TDM_CO_SUI_LIABILITIES
  OldCreateOrder = True
  Left = 363
  Top = 170
  Height = 240
  Width = 400
  object CO_SUI_LIABILITIES: TCO_SUI_LIABILITIES
    ProviderName = 'CO_SUI_LIABILITIES_PROV'
    PictureMasks.Strings = (
      
        'AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),' +
        '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9'T' +
        #9'F')
    BeforeEdit = CO_SUI_LIABILITIESBeforeEdit
    BeforePost = CO_SUI_LIABILITIESBeforePost
    BeforeDelete = CO_SUI_LIABILITIESBeforeDelete
    OnCalcFields = CO_SUI_LIABILITIESCalcFields
    OnNewRecord = CO_SUI_LIABILITIESNewRecord
    Left = 67
    Top = 24
    object CO_SUI_LIABILITIESAMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
      Required = True
      currency = True
    end
    object CO_SUI_LIABILITIESCO_SUI_LIABILITIES_NBR: TIntegerField
      FieldName = 'CO_SUI_LIABILITIES_NBR'
      Required = True
      Visible = False
    end
    object CO_SUI_LIABILITIESCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Required = True
      Visible = False
    end
    object CO_SUI_LIABILITIESCO_SUI_NBR: TIntegerField
      FieldName = 'CO_SUI_NBR'
      Required = True
      Visible = False
    end
    object CO_SUI_LIABILITIESDUE_DATE: TDateField
      FieldName = 'DUE_DATE'
      Required = True
      Visible = False
    end
    object CO_SUI_LIABILITIESSTATUS: TStringField
      FieldName = 'STATUS'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_SUI_LIABILITIESSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
      Visible = False
    end
    object CO_SUI_LIABILITIESADJUSTMENT_TYPE: TStringField
      FieldName = 'ADJUSTMENT_TYPE'
      Visible = False
      Size = 1
    end
    object CO_SUI_LIABILITIESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Visible = False
    end
    object CO_SUI_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
      Visible = False
    end
    object CO_SUI_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField
      FieldName = 'IMPOUND_CO_BANK_ACCT_REG_NBR'
      Visible = False
    end
    object CO_SUI_LIABILITIESNOTES: TBlobField
      FieldName = 'NOTES'
      Visible = False
      Size = 8
    end
    object CO_SUI_LIABILITIESFILLER: TStringField
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object CO_SUI_LIABILITIESSUI_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'SUI_Name'
      LookupDataSet = DM_CO_SUI.CO_SUI
      LookupKeyFields = 'CO_SUI_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_SUI_NBR'
      Lookup = True
    end
    object CO_SUI_LIABILITIESSUI_State: TStringField
      FieldKind = fkLookup
      FieldName = 'SUI_State'
      LookupDataSet = DM_CO_SUI.CO_SUI
      LookupKeyFields = 'CO_SUI_NBR'
      LookupResultField = 'State'
      KeyFields = 'CO_SUI_NBR'
      Size = 2
      Lookup = True
    end
    object CO_SUI_LIABILITIESTHIRD_PARTY: TStringField
      FieldName = 'THIRD_PARTY'
      Required = True
      Size = 1
    end
    object CO_SUI_LIABILITIESCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object CO_SUI_LIABILITIESACH_KEY: TStringField
      FieldName = 'ACH_KEY'
      Size = 11
    end
    object CO_SUI_LIABILITIESIMPOUNDED: TStringField
      FieldName = 'IMPOUNDED'
      Required = True
      Size = 1
    end
    object CO_SUI_LIABILITIESPERIOD_BEGIN_DATE: TDateField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object CO_SUI_LIABILITIESPERIOD_END_DATE: TDateField
      FieldName = 'PERIOD_END_DATE'
    end
    object CO_SUI_LIABILITIESTAX_COUPON_SY_REPORTS_NBR: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'TAX_COUPON_SY_REPORTS_NBR'
    end
    object CO_SUI_LIABILITIESADJ_PERIOD_END_DATE: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'ADJ_PERIOD_END_DATE'
    end
    object CO_SUI_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CALC_PERIOD_END_DATE'
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 64
    Top = 96
  end
end
