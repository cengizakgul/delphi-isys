inherited DM_SY_STATE_MARITAL_STATUS: TDM_SY_STATE_MARITAL_STATUS
  OldCreateOrder = True
  Left = 401
  Top = 199
  Height = 295
  Width = 331
  object SY_STATE_MARITAL_STATUS: TSY_STATE_MARITAL_STATUS
    ProviderName = 'SY_STATE_MARITAL_STATUS_PROV'
    PictureMasks.Strings = (
      
        'STANDARD_DEDUCTION_PCNT_GROSS'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_STAND_DEDUCT_PCNT_GROSS'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'STANDARD_DEDUCTION_MIN_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'STANDARD_DEDUCTION_MAX_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'STANDARD_DEDUCTION_FLAT_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*' +
        '#]},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'STANDARD_PER_EXEMPTION_ALLOW'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'DEDUCT_FEDERAL_MAXIMUM_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PER_DEPENDENT_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'PERSONAL_TAX_CREDIT_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'TAX_CREDIT_PER_DEPENDENT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'TAX_CREDIT_PER_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'HIGH_INCOME_PER_EXEMPT_ALLOW'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'DEFINED_HIGH_INCOME_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'MINIMUM_TAXABLE_INCOME'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]}'#9'T'#9'F'
      
        'ADDITIONAL_EXEMPT_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'ADDITIONAL_DEDUCTION_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*' +
        '#]},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'DEDUCT_FICA__MAXIMUM_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'BLIND_EXEMPTION_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_STAND_DEDUCT_MIN_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_STAND_DEDUCT_MAX_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_STAND_DEDUCT_FLAT_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_STANDARD_PER_EXEMPT_ALLOW'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*' +
        '#]},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_DEDUCT_FED_MAX_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_PER_DEPENDENT_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_PERSONAL_TAX_CREDIT_AMT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_TAX_CREDIT_PER_DEPENDENT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_TAX_CREDIT_PER_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_HIGH_INCOME_PER_E_A'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_HIGH_INCOME_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_MINIMUM_TAXABLE_INCOME'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_ADD_EXEMPT_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_ADD_DEDUCTION_ALLOWANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_DEDUCT_FICA_MAX_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_BLIND_EXEMPTION_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F')
    Left = 92
    Top = 68
    object SY_STATE_MARITAL_STATUSADDITIONAL_DEDUCTION_ALLOWANCE: TFloatField
      FieldName = 'ADDITIONAL_DEDUCTION_ALLOWANCE'
    end
    object SY_STATE_MARITAL_STATUSADDITIONAL_EXEMPT_ALLOWANCE: TFloatField
      FieldName = 'ADDITIONAL_EXEMPT_ALLOWANCE'
    end
    object SY_STATE_MARITAL_STATUSBLIND_EXEMPTION_AMOUNT: TFloatField
      FieldName = 'BLIND_EXEMPTION_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSDEDUCT_FEDERAL: TStringField
      FieldName = 'DEDUCT_FEDERAL'
      Size = 1
    end
    object SY_STATE_MARITAL_STATUSDEDUCT_FEDERAL_MAXIMUM_AMOUNT: TFloatField
      FieldName = 'DEDUCT_FEDERAL_MAXIMUM_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSDEDUCT_FICA: TStringField
      FieldName = 'DEDUCT_FICA'
      Size = 1
    end
    object SY_STATE_MARITAL_STATUSDEDUCT_FICA__MAXIMUM_AMOUNT: TFloatField
      FieldName = 'DEDUCT_FICA__MAXIMUM_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSDEFINED_HIGH_INCOME_AMOUNT: TFloatField
      FieldName = 'DEFINED_HIGH_INCOME_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object SY_STATE_MARITAL_STATUSHIGH_INCOME_PER_EXEMPT_ALLOW: TFloatField
      FieldName = 'HIGH_INCOME_PER_EXEMPT_ALLOW'
    end
    object SY_STATE_MARITAL_STATUSMINIMUM_TAXABLE_INCOME: TFloatField
      FieldName = 'MINIMUM_TAXABLE_INCOME'
    end
    object SY_STATE_MARITAL_STATUSPERSONAL_EXEMPTIONS: TIntegerField
      FieldName = 'PERSONAL_EXEMPTIONS'
    end
    object SY_STATE_MARITAL_STATUSPERSONAL_TAX_CREDIT_AMOUNT: TFloatField
      FieldName = 'PERSONAL_TAX_CREDIT_AMOUNT'
      DisplayFormat = '#,##0.00####'
    end
    object SY_STATE_MARITAL_STATUSPER_DEPENDENT_ALLOWANCE: TFloatField
      FieldName = 'PER_DEPENDENT_ALLOWANCE'
    end
    object SY_STATE_MARITAL_STATUSSTANDARD_DEDUCTION_FLAT_AMOUNT: TFloatField
      FieldName = 'STANDARD_DEDUCTION_FLAT_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSSTANDARD_DEDUCTION_MAX_AMOUNT: TFloatField
      FieldName = 'STANDARD_DEDUCTION_MAX_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSSTANDARD_DEDUCTION_MIN_AMOUNT: TFloatField
      FieldName = 'STANDARD_DEDUCTION_MIN_AMOUNT'
    end
    object SY_STATE_MARITAL_STATUSSTANDARD_DEDUCTION_PCNT_GROSS: TFloatField
      FieldName = 'STANDARD_DEDUCTION_PCNT_GROSS'
    end
    object SY_STATE_MARITAL_STATUSSTANDARD_PER_EXEMPTION_ALLOW: TFloatField
      FieldName = 'STANDARD_PER_EXEMPTION_ALLOW'
    end
    object SY_STATE_MARITAL_STATUSSTATE_PERCENT_OF_FEDERAL: TFloatField
      FieldName = 'STATE_PERCENT_OF_FEDERAL'
      DisplayFormat = '#,##0.00###'
    end
    object SY_STATE_MARITAL_STATUSSTATUS_DESCRIPTION: TStringField
      FieldName = 'STATUS_DESCRIPTION'
      Size = 40
    end
    object SY_STATE_MARITAL_STATUSSTATUS_TYPE: TStringField
      DisplayWidth = 4
      FieldName = 'STATUS_TYPE'
      Size = 4
    end
    object SY_STATE_MARITAL_STATUSSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object SY_STATE_MARITAL_STATUSSY_STATE_MARITAL_STATUS_NBR: TIntegerField
      FieldName = 'SY_STATE_MARITAL_STATUS_NBR'
    end
    object SY_STATE_MARITAL_STATUSTAX_CREDIT_PER_ALLOWANCE: TFloatField
      FieldName = 'TAX_CREDIT_PER_ALLOWANCE'
    end
    object SY_STATE_MARITAL_STATUSTAX_CREDIT_PER_DEPENDENT: TFloatField
      FieldName = 'TAX_CREDIT_PER_DEPENDENT'
    end
  end
end
