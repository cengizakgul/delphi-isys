inherited DM_PR: TDM_PR
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object DM_COMPANY: TDM_COMPANY
    Left = 200
    Top = 80
  end
  object PR: TPR
    ProviderName = 'PR_PROV'
    IndexName = 'SORT'
    IndexDefs = <
      item
        Name = 'SORT'
        DescFields = 'CHECK_DATE'
        Fields = 'CHECK_DATE;RUN_NUMBER;STATUS'
        Options = [ixCaseInsensitive]
      end>
    BeforeEdit = PRBeforeEdit
    BeforePost = PRBeforePost
    AfterPost = PRAfterPost
    AfterCancel = PRAfterCancel
    BeforeDelete = PRBeforeDelete
    AfterDelete = PRAfterDelete
    OnNewRecord = PRNewRecord
    OnDeleteError = PRDeleteError
    Left = 200
    Top = 144
  end
end
