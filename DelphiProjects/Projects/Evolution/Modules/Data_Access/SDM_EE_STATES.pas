// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_STATES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  EvUtils, EvConsts, EvTypes,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictsystem, EvExceptions,
  EvContext, EvUIUtils, EvClientDataSet;

type
  TDM_EE_STATES = class(TDM_ddTable)
    EE_STATES: TEE_STATES;
    EE_STATESCO_STATES_NBR: TIntegerField;
    EE_STATESSTATE_MARITAL_STATUS: TStringField;
    EE_STATESSTATE_NUMBER_WITHHOLDING_ALLOW: TIntegerField;
    EE_STATESSTATE_EXEMPT_EXCLUDE: TStringField;
    EE_STATESOVERRIDE_STATE_TAX_VALUE: TFloatField;
    EE_STATESOVERRIDE_STATE_TAX_TYPE: TStringField;
    EE_STATESSDI_APPLY_CO_STATES_NBR: TIntegerField;
    EE_STATESSUI_APPLY_CO_STATES_NBR: TIntegerField;
    EE_STATESEE_SDI_EXEMPT_EXCLUDE: TStringField;
    EE_STATESER_SDI_EXEMPT_EXCLUDE: TStringField;
    EE_STATESEE_SUI_EXEMPT_EXCLUDE: TStringField;
    EE_STATESER_SUI_EXEMPT_EXCLUDE: TStringField;
    EE_STATESRECIPROCAL_METHOD: TStringField;
    EE_STATESRECIPROCAL_CO_STATES_NBR: TIntegerField;
    EE_STATESRECIPROCAL_AMOUNT_PERCENTAGE: TFloatField;
    EE_STATESEE_STATES_NBR: TIntegerField;
    EE_STATESEE_NBR: TIntegerField;
    EE_STATESFILLER: TStringField;
    EE_STATESState_Lookup: TStringField;
    EE_STATESEE_COUNTY: TStringField;
    EE_STATESEE_TAX_CODE: TStringField;
    DM_COMPANY: TDM_COMPANY;
    DM_CLIENT: TDM_CLIENT;
    EE_STATESIMPORTED_MARITAL_STATUS: TStringField;
    EE_STATESSUI_State_Lookup: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    EE_STATESCALCULATE_TAXABLE_WAGES_1099: TStringField;
    EE_STATESSDI_State_Lookup: TStringField;
    EE_STATESReciprocal_State_Lookup: TStringField;
    EE_STATESSocCodeMask: TStringField;
    procedure EE_STATESBeforeDelete(DataSet: TDataSet);
    procedure EE_STATESBeforePost(DataSet: TDataSet);
    procedure EE_STATESNewRecord(DataSet: TDataSet);
    procedure EE_STATESAfterInsert(DataSet: TDataSet);
    procedure EE_STATESSTATE_EXEMPT_EXCLUDEValidate(Sender: TField);
    procedure EE_STATESSTATE_MARITAL_STATUSValidate(Sender: TField);
    procedure EE_STATESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses Variants;

{$R *.DFM}

procedure TDM_EE_STATES.EE_STATESBeforeDelete(DataSet: TDataSet);

begin
  DM_PAYROLL.PR_CHECK_STATES.DataRequired('EE_STATES_NBR=' + IntToStr(EE_STATES['EE_STATES_NBR']));
  if DM_PAYROLL.PR_CHECK_STATES.RecordCount > 0 then
    raise EUpdateError.CreateHelp('This state has WAGES attached and can not be delete!', IDH_ConsistencyViolation);

  DM_EMPLOYEE.EE.Activate;
  if DM_EMPLOYEE.EE['EE_NBR'] <> DataSet['EE_NBR'] then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DataSet['EE_NBR'], []);

end;

procedure TDM_EE_STATES.EE_STATESBeforePost(DataSet: TDataSet);
var
  bEdit: Boolean;
  bSaved: Boolean;
  V: Variant;
  SocCode: string;
begin
  bSaved := False;

  //TS:  08/15/2006 issue 39575 Set field default for non nullable field.
  if (DataSet.State in [dsInsert, dsEdit]) then
  begin
     SocCode := DataSet.FieldByName('SocCodeMask').AsString;
     if SocCode = '' then
       DataSet['Soc_Code'] :=  null
     else // remove Dash (format XX-XXXX)
       DataSet['Soc_Code'] := Copy(SocCode, 1, 2) + Copy(SocCode, 4, 4);

    //Make sure Client person filter is not set.... //EE_BASE seems to be setting it to 0
    //for some reason, I assume there has to a reason anyway...
    if DM_CLIENT.CL_PERSON.Filtered then
      DM_CLIENT.CL_PERSON.Filtered := False;


    V := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DataSet['CO_STATES_NBR'], 'SY_STATES_NBR');
    if not VarIsNull(V) then
    begin
      V := DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([V, DataSet['STATE_MARITAL_STATUS']]), 'SY_STATE_MARITAL_STATUS_NBR');
      if not VarIsNull(V) then
        DataSet['SY_STATE_MARITAL_STATUS_NBR'] := V;
    end;
  end;

  with DM_EMPLOYEE do
  begin
    if EE.Active then
    begin
      if (TevClientDataSet(EE).State = dsBrowse) and
         (EE.EE_NBR.Value <> EE_STATES.EE_NBR.Value) then
      begin
        EE.SaveStateAndReset;
        Assert(EE.Locate('EE_NBR', EE_STATES.EE_NBR.Value, []));
        bSaved := True;
      end;
      try
        if EE.FieldByName('HOME_TAX_EE_STATES_NBR').IsNull then
        begin
          bEdit := TevClientDataSet(EE).State in [dsInsert, dsEdit];
          if not bEdit then
            EE.Edit;
          EE.FieldByName('HOME_TAX_EE_STATES_NBR').Assign(DataSet.FieldByName('EE_STATES_NBR'));
          if not bEdit then
            EE.Post;
        end;

      finally
        if bSaved then
          EE.LoadState;
      end;
    end;
  end;

  if not DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').IsNull then
  begin
    DM_COMPANY.CO_STATES.Activate;
    if DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DataSet['SUI_APPLY_CO_STATES_NBR'], 'USE_STATE_FOR_SUI') <> GROUP_BOX_YES then
      raise EUpdateError.CreateHelp('This state can not be used for SUI.', IDH_ConsistencyViolation);
  end;

  if (DataSet.State in [dsInsert, dsEdit]) and not ctx_DataAccess.Copying and not ctx_DataAccess.Importing then
    if (EE_STATES.OVERRIDE_STATE_TAX_TYPE.AsString <> OVERRIDE_VALUE_TYPE_NONE) and
       EE_STATES.OVERRIDE_STATE_TAX_VALUE.IsNull then
      raise EUpdateError.CreateHelp('You must enter state override value', IDH_ConsistencyViolation);

end;

procedure TDM_EE_STATES.EE_STATESNewRecord(DataSet: TDataSet);
begin
  if DM_EMPLOYEE.EE.FieldByName('COMPANY_OR_INDIVIDUAL_NAME').AsString = GROUP_BOX_COMPANY  then
  begin
    DataSet.FindField('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT; //then EE_STATES->Exempt
    DataSet.FindField('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT; //then EE_SUI->Exempt
    DataSet.FindField('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT; //then EE_SUI->Exempt
    DataSet.FindField('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT; //then ER_SUI->Exempt
    DataSet.FindField('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT; //then ER_SUI->Exempt
  end;
end;

procedure TDM_EE_STATES.EE_STATESAfterInsert(DataSet: TDataSet);
var
  retrcond: string;
begin
  if not ctx_DataAccess.Importing then
  begin
    retrcond :=  'SY_STATES_NBR=' + DM_SYSTEM_STATE.SY_STATES.FieldByNAme('SY_STATES_NBR').AsString;
    if not DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Active then
      DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.DataRequired(retrcond );
    //Assert( (DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.State = dsBrowse) and DSConditionsEqual( DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition, retrcond ) )
    if (DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.State = dsBrowse) and
        DSConditionsEqual( DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition, retrcond ) then
    begin
      if (DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RecordCount = 1) and (not DataSet.FieldByName('CO_STATES_NBR').IsNull) then
      begin
        DataSet.FieldByName('STATE_MARITAL_STATUS').Assign(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE'));

      end;
    end;
  end;
  DataSet.FieldByName('IMPORTED_MARITAL_STATUS').Assign(DM_EMPLOYEE.EE.FieldByName('FEDERAL_MARITAL_STATUS'));
end;

procedure TDM_EE_STATES.EE_STATESSTATE_EXEMPT_EXCLUDEValidate(
  Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

procedure TDM_EE_STATES.EE_STATESSTATE_MARITAL_STATUSValidate(
  Sender: TField);
var
  V: Variant;
begin
  //TS:  08/15/2006 issue 39575 Set field default for non nullable field.
  if (DM_EMPLOYEE.EE_STATES.State in [dsInsert, dsEdit]) then
  begin
    V := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DM_EMPLOYEE.EE_STATES['CO_STATES_NBR'], 'SY_STATES_NBR');
    if not VarIsNull(V) then
    begin
      V := DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([V, DM_EMPLOYEE.EE_STATES['STATE_MARITAL_STATUS']]), 'SY_STATE_MARITAL_STATUS_NBR');
      if not VarIsNull(V) then
        DM_EMPLOYEE.EE_STATES['SY_STATE_MARITAL_STATUS_NBR'] := V;
    end;
  end;

  inherited;
  
end;

procedure TDM_EE_STATES.EE_STATESCalcFields(DataSet: TDataSet);
var
  s: string;
begin
  inherited;
  if not DataSet.FieldByName('SOC_CODE').IsNull then
  begin
    s := Trim(DataSet.FieldByName('SOC_CODE').AsString);
    if Length(s) > 2 then
      EE_STATESSocCodeMask.AsString := Copy(s, 1, 2) + '-' + Copy(s, 3, 4)
    else
      EE_STATESSocCodeMask.AsString := s;
  end;
end;

initialization
  RegisterClass(TDM_EE_STATES);

finalization
  UnregisterClass(TDM_EE_STATES);

end.
