// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_USER;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDataDicttemp, SDataDictclient, EvContext, EvUIComponents, EvClientDataSet;

type
  TDM_SB_USER = class(TDM_ddTable)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_USER: TSB_USER;
    SB_USERSB_USER_NBR: TIntegerField;
    SB_USERUSER_ID: TStringField;
    SB_USERUSER_SIGNATURE: TBlobField;
    SB_USERLAST_NAME: TStringField;
    SB_USERFIRST_NAME: TStringField;
    SB_USERMIDDLE_INITIAL: TStringField;
    SB_USERACTIVE_USER: TStringField;
    SB_USERDEPARTMENT: TStringField;
    SB_USERPASSWORD_CHANGE_DATE: TDateTimeField;
    SB_USERSECURITY_LEVEL: TStringField;
    SB_USERUSER_UPDATE_OPTIONS: TStringField;
    SB_USERUSER_FUNCTIONS: TStringField;
    SB_USERUSER_PASSWORD: TStringField;
    SB_USEREMAIL_ADDRESS: TStringField;
    SB_USERCL_NBR: TIntegerField;
    SB_USERClientNumber: TStringField;
    DM_CLIENT: TDM_CLIENT;
    DM_TEMPORARY: TDM_TEMPORARY;
    SB_USERSB_ACCOUNTANT_NBR: TIntegerField;
    SB_USERStereoType: TStringField;
    procedure SB_USERCalcFields(DataSet: TDataSet);
    procedure SB_USERAddLookups(var A: TArrayDS);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses
  evutils, SFieldCodeValues;
{$R *.DFM}


procedure TDM_SB_USER.SB_USERCalcFields(DataSet: TDataSet);
var
  s: string;
begin
  s := ExtractFromFiller(DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8);
  if (s <> '') and ( s <> '-1' ) and ( s <> '-2' ) then
  begin
    if not DM_TEMPORARY.TMP_CL.Active then DM_TEMPORARY.TMP_CL.Open;
    SB_USERClientNumber.AsString := DM_TEMPORARY.TMP_CL.ShadowDataSet.CachedLookup(StrToIntDef(s, 0), 'CUSTOM_CLIENT_NUMBER');
  end
  else
        SB_USERClientNumber.AsString := '';

    // Stereo Type Field Defination
    // UserFunction = -1    : Local
    // UserFunction = -2    : Web
    // UserFunction  has not any value : Remote

  SB_USERStereoType.AsString := '';
  if  s = '-1' Then SB_USERStereoType.AsString := 'Internal';
  if  s = '-2' Then SB_USERStereoType.AsString := 'Web';
  if (s <> '') and ( s <> '-1' ) and ( s <> '-2' ) then SB_USERStereoType.AsString := 'Remote';
end;

procedure TDM_SB_USER.SB_USERAddLookups(var A: TArrayDS);
begin
  AddDS(DM_TEMPORARY.TMP_CL, A);
end;

constructor TDM_SB_USER.Create(AOwner: TComponent);
begin
  Tag := Integer(GetCurrentThreadId);
  inherited;
end;

initialization
  RegisterClass( TDM_SB_USER );

finalization
  UnregisterClass( TDM_SB_USER );

end.
