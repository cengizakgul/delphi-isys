inherited DM_TMP_CO_MANUAL_ACH: TDM_TMP_CO_MANUAL_ACH
  OldCreateOrder = False
  Left = 538
  Top = 233
  Height = 350
  Width = 404
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 112
    Top = 200
  end
  object TMP_CO_MANUAL_ACH: TTMP_CO_MANUAL_ACH
    Aggregates = <>
    Params = <>
    ProviderName = 'TMP_CO_MANUAL_ACH_PROV'
    ValidateWithMask = True
    Left = 71
    Top = 72
    object TMP_CO_MANUAL_ACHCO_MANUAL_ACH_NBR: TIntegerField
      FieldName = 'CO_MANUAL_ACH_NBR'
    end
    object TMP_CO_MANUAL_ACHCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object TMP_CO_MANUAL_ACHCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object TMP_CO_MANUAL_ACHEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object TMP_CO_MANUAL_ACHDEBIT_DATE: TDateField
      FieldName = 'DEBIT_DATE'
    end
    object TMP_CO_MANUAL_ACHCREDIT_DATE: TDateField
      FieldName = 'CREDIT_DATE'
    end
    object TMP_CO_MANUAL_ACHSTATUS: TStringField
      FieldName = 'STATUS'
      FixedChar = True
      Size = 1
    end
    object TMP_CO_MANUAL_ACHSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object TMP_CO_MANUAL_ACHAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      currency = True
    end
    object TMP_CO_MANUAL_ACHPURPOSE_NOTES: TBlobField
      FieldName = 'PURPOSE_NOTES'
      BlobType = ftBlob
      Size = 1
    end
    object TMP_CO_MANUAL_ACHFROM_CO_NBR: TIntegerField
      FieldName = 'FROM_CO_NBR'
    end
    object TMP_CO_MANUAL_ACHFROM_EE_NBR: TIntegerField
      FieldName = 'FROM_EE_NBR'
    end
    object TMP_CO_MANUAL_ACHFROM_SB_BANK_ACCOUNTS_NBR: TIntegerField
      FieldName = 'FROM_SB_BANK_ACCOUNTS_NBR'
    end
    object TMP_CO_MANUAL_ACHFROM_CL_BANK_ACCOUNT_NBR: TIntegerField
      FieldName = 'FROM_CL_BANK_ACCOUNT_NBR'
    end
    object TMP_CO_MANUAL_ACHFROM_EE_DIRECT_DEPOSIT_NBR: TIntegerField
      FieldName = 'FROM_EE_DIRECT_DEPOSIT_NBR'
    end
    object TMP_CO_MANUAL_ACHTO_CO_NBR: TIntegerField
      FieldName = 'TO_CO_NBR'
    end
    object TMP_CO_MANUAL_ACHTO_EE_NBR: TIntegerField
      FieldName = 'TO_EE_NBR'
    end
    object TMP_CO_MANUAL_ACHTO_SB_BANK_ACCOUNTS_NBR: TIntegerField
      FieldName = 'TO_SB_BANK_ACCOUNTS_NBR'
    end
    object TMP_CO_MANUAL_ACHTO_CL_BANK_ACCOUNT_NBR: TIntegerField
      FieldName = 'TO_CL_BANK_ACCOUNT_NBR'
    end
    object TMP_CO_MANUAL_ACHTO_EE_DIRECT_DEPOSIT_NBR: TIntegerField
      FieldName = 'TO_EE_DIRECT_DEPOSIT_NBR'
    end
    object TMP_CO_MANUAL_ACHCompanyNumber: TStringField
      FieldKind = fkLookup
      FieldName = 'CompanyNumber'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Lookup = True
    end
    object TMP_CO_MANUAL_ACHCompanyName: TStringField
      FieldKind = fkLookup
      FieldName = 'CompanyName'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
  end
end
