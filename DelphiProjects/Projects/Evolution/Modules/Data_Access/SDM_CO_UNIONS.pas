// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_UNIONS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_UNIONS = class(TDM_ddTable)
    DM_CLIENT: TDM_CLIENT;
    CO_UNIONS: TCO_UNIONS;
    CO_UNIONSCO_UNIONS_NBR: TIntegerField;
    CO_UNIONSCO_NBR: TIntegerField;
    CO_UNIONSCL_UNION_NBR: TIntegerField;
    CO_UNIONSUnion_Name: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_UNIONS: TDM_CO_UNIONS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_UNIONS);

finalization
  UnregisterClass(TDM_CO_UNIONS);

end.
