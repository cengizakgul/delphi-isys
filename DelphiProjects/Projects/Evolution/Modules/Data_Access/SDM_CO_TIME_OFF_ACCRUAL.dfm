inherited DM_CO_TIME_OFF_ACCRUAL: TDM_CO_TIME_OFF_ACCRUAL
  OldCreateOrder = False
  Left = 608
  Top = 209
  Height = 274
  Width = 359
  object CO_TIME_OFF_ACCRUAL: TCO_TIME_OFF_ACCRUAL
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_TIME_OFF_ACCRUAL_PROV'
    ValidateWithMask = True
    Left = 88
    Top = 80
    object CO_TIME_OFF_ACCRUALCO_TIME_OFF_ACCRUAL_NBR: TIntegerField
      FieldName = 'CO_TIME_OFF_ACCRUAL_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."CO_TIME_OFF_ACCRUAL_NBR"'
      Required = True
    end
    object CO_TIME_OFF_ACCRUALCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."CO_NBR"'
      Required = True
    end
    object CO_TIME_OFF_ACCRUALDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."DESCRIPTION"'
      Required = True
      Size = 40
    end
    object CO_TIME_OFF_ACCRUALCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."CL_E_DS_NBR"'
      Required = True
    end
    object CO_TIME_OFF_ACCRUALCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."CL_E_D_GROUPS_NBR"'
    end
    object CO_TIME_OFF_ACCRUALFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."FREQUENCY"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALACCRUAL_MONTH_NUMBER: TStringField
      FieldName = 'ACCRUAL_MONTH_NUMBER'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ACCRUAL_MONTH_NUMBER"'
      Size = 2
    end
    object CO_TIME_OFF_ACCRUALPAYROLL_OF_THE_MONTH_TO_ACCRUE: TStringField
      FieldName = 'PAYROLL_OF_THE_MONTH_TO_ACCRUE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."PAYROLL_OF_THE_MONTH_TO_ACCRUE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALCALCULATION_METHOD: TStringField
      FieldName = 'CALCULATION_METHOD'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."CALCULATION_METHOD"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALANNUAL_MAXIMUM_HOURS_TO_ACCRUE: TFloatField
      FieldName = 'ANNUAL_MAXIMUM_HOURS_TO_ACCRUE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ANNUAL_MAXIMUM_HOURS_TO_ACCRUE"'
    end
    object CO_TIME_OFF_ACCRUALMINIMUM_HOURS_BEFORE_ACCRUAL: TFloatField
      FieldName = 'MINIMUM_HOURS_BEFORE_ACCRUAL'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."MINIMUM_HOURS_BEFORE_ACCRUAL"'
    end
    object CO_TIME_OFF_ACCRUALPROBATION_PERIOD_MONTHS: TFloatField
      FieldName = 'PROBATION_PERIOD_MONTHS'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."PROBATION_PERIOD_MONTHS"'
    end
    object CO_TIME_OFF_ACCRUALACCRUAL_ACTIVE: TStringField
      FieldName = 'ACCRUAL_ACTIVE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ACCRUAL_ACTIVE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALAUTO_CREATE_ON_NEW_HIRE: TStringField
      FieldName = 'AUTO_CREATE_ON_NEW_HIRE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."AUTO_CREATE_ON_NEW_HIRE"'
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALSHOW_USED_BALANCE_ON_CHECK: TStringField
      FieldName = 'SHOW_USED_BALANCE_ON_CHECK'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."SHOW_USED_BALANCE_ON_CHECK"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALANNUAL_RESET_CODE: TStringField
      FieldName = 'ANNUAL_RESET_CODE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ANNUAL_RESET_CODE"'
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALRESET_DATE: TDateField
      FieldName = 'RESET_DATE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."RESET_DATE"'
    end
    object CO_TIME_OFF_ACCRUALDIVISOR: TFloatField
      FieldName = 'DIVISOR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."DIVISOR"'
    end
    object CO_TIME_OFF_ACCRUALDIVISOR_DESCRIPTION: TStringField
      FieldName = 'DIVISOR_DESCRIPTION'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."DIVISOR_DESCRIPTION"'
      Size = 6
    end
    object CO_TIME_OFF_ACCRUALRLLOVR_CO_TIME_OFF_ACCRUAL_NBR: TIntegerField
      FieldName = 'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."RLLOVR_CO_TIME_OFF_ACCRUAL_NBR"'
    end
    object CO_TIME_OFF_ACCRUALROLLOVER_DATE: TDateField
      FieldName = 'ROLLOVER_DATE'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ROLLOVER_DATE"'
    end
    object CO_TIME_OFF_ACCRUALUSED_CL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'USED_CL_E_D_GROUPS_NBR'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."USED_CL_E_D_GROUPS_NBR"'
    end
    object CO_TIME_OFF_ACCRUALROLLOVER_FREQ: TStringField
      FieldName = 'ROLLOVER_FREQ'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ROLLOVER_FREQ"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALROLLOVER_MONTH_NUMBER: TIntegerField
      FieldName = 'ROLLOVER_MONTH_NUMBER'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ROLLOVER_MONTH_NUMBER"'
    end
    object CO_TIME_OFF_ACCRUALROLLOVER_PAYROLL_OF_MONTH: TStringField
      FieldName = 'ROLLOVER_PAYROLL_OF_MONTH'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ROLLOVER_PAYROLL_OF_MONTH"'
      Required = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALROLLOVER_OFFSET: TIntegerField
      FieldName = 'ROLLOVER_OFFSET'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."ROLLOVER_OFFSET"'
    end
    object CO_TIME_OFF_ACCRUALMAXIMUM_HOURS_TO_ACCRUE_ON: TFloatField
      FieldName = 'MAXIMUM_HOURS_TO_ACCRUE_ON'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."MAXIMUM_HOURS_TO_ACCRUE_ON"'
    end
    object CO_TIME_OFF_ACCRUALCHECK_TIME_OFF_AVAIL: TStringField
      FieldName = 'CHECK_TIME_OFF_AVAIL'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."CHECK_TIME_OFF_AVAIL"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALFILLER: TStringField
      FieldName = 'FILLER'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."FILLER"'
      FixedChar = True
      Size = 512
    end
    object CO_TIME_OFF_ACCRUALCustomEDCodeNumber: TStringField
      FieldKind = fkLookup
      FieldName = 'CustomEDCodeNumber'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object CO_TIME_OFF_ACCRUALEDGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGroupName'
      LookupDataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
      LookupKeyFields = 'CL_E_D_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_E_D_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_TIME_OFF_ACCRUALRESET_PAYROLL_OF_MONTH: TStringField
      FieldName = 'RESET_PAYROLL_OF_MONTH'
      Origin = '"CO_TIME_OFF_ACCRUAL_HIST"."RESET_PAYROLL_OF_MONTH"'
      Required = True
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALRESET_MONTH_NUMBER: TIntegerField
      FieldName = 'RESET_MONTH_NUMBER'
    end
    object CO_TIME_OFF_ACCRUALRESET_OFFSET: TIntegerField
      FieldName = 'RESET_OFFSET'
    end
    object CO_TIME_OFF_ACCRUALACCRUE_ON_STANDARD_HOURS: TStringField
      FieldName = 'ACCRUE_ON_STANDARD_HOURS'
      Size = 1
    end
    object CO_TIME_OFF_ACCRUALAUTO_CREATE_FOR_STATUSES: TStringField
      FieldName = 'AUTO_CREATE_FOR_STATUSES'
    end
    object CO_TIME_OFF_ACCRUALCO_HR_ATTENDANCE_TYPES_NBR: TIntegerField
      FieldName = 'CO_HR_ATTENDANCE_TYPES_NBR'
    end
    object CO_TIME_OFF_ACCRUALPROCESS_ORDER: TStringField
      FieldName = 'PROCESS_ORDER'
      Size = 1
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 256
    Top = 24
  end
end
