inherited DM_SB_AGENCY_REPORTS: TDM_SB_AGENCY_REPORTS
  OldCreateOrder = False
  Left = 192
  Top = 107
  Height = 480
  Width = 696
  object SB_AGENCY_REPORTS: TSB_AGENCY_REPORTS
    Aggregates = <>
    Params = <>
    ProviderName = 'SB_AGENCY_REPORTS_PROV'
    ValidateWithMask = True
    Left = 84
    Top = 52
    object SB_AGENCY_REPORTSReportDescriptionLookup: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'Report Description Lookup'
      LookupDataSet = DM_SB_REPORTS.SB_REPORTS
      LookupKeyFields = 'SB_REPORTS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SB_REPORTS_NBR'
      Size = 40
      Lookup = True
    end
    object SB_AGENCY_REPORTSSB_AGENCY_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_AGENCY_REPORTS_NBR'
      Visible = False
    end
    object SB_AGENCY_REPORTSSB_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_AGENCY_NBR'
      Visible = False
    end
    object SB_AGENCY_REPORTSSB_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_REPORTS_NBR'
      Visible = False
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 88
    Top = 120
  end
end
