// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_ENABLED_DASHBOARDS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  SysUtils, Classes, DB,   SDataStructure, SDDClasses, SDataDictsystem,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, EvConsts;

type
  TDM_SB_ENABLED_DASHBOARDS = class(TDM_ddTable)
    SB_ENABLED_DASHBOARDS: TSB_ENABLED_DASHBOARDS;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_ENABLED_DASHBOARDSDASHBOARD_LEVEL: TStringField;
    SB_ENABLED_DASHBOARDSDESCRIPTION: TStringField;
    SB_ENABLED_DASHBOARDSLEVEL_DESC: TStringField;
    SB_ENABLED_DASHBOARDSDASHBOARD_NBR: TIntegerField;
    SB_ENABLED_DASHBOARDSSB_ENABLED_DASHBOARDS_NBR: TIntegerField;
    SB_ENABLED_DASHBOARDSSY_ANALYTICS_TIER_NBR: TIntegerField;
    procedure SB_ENABLED_DASHBOARDSCalcFields(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}


procedure TDM_SB_ENABLED_DASHBOARDS.SB_ENABLED_DASHBOARDSCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if SB_ENABLED_DASHBOARDSDASHBOARD_NBR.AsInteger > 0  then
  begin
   if not DM_SYSTEM_MISC.SY_DASHBOARDS.Active then
       DM_SYSTEM_MISC.SY_DASHBOARDS.DataRequired('ALL');

   SB_ENABLED_DASHBOARDSLEVEL_DESC.AsString := 'System';
   if DM_SYSTEM_MISC.SY_DASHBOARDS.Locate('SY_DASHBOARDS_NBR', SB_ENABLED_DASHBOARDSDASHBOARD_NBR.AsInteger,[]) then
   begin
     SB_ENABLED_DASHBOARDSDESCRIPTION.AsString := DM_SYSTEM_MISC.SY_DASHBOARDS.FieldByName('DESCRIPTION').AsString;
     SB_ENABLED_DASHBOARDSSY_ANALYTICS_TIER_NBR.AsInteger := DM_SYSTEM_MISC.SY_DASHBOARDS.FieldByName('SY_ANALYTICS_TIER_NBR').AsInteger;     
   end;
  end;
end;

initialization
  RegisterClass(TDM_SB_ENABLED_DASHBOARDS);

finalization
  UnregisterClass(TDM_SB_ENABLED_DASHBOARDS);

end.
