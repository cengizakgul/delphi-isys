// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_DELIVERY_COMPANY_SVCS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure,   Variants;

type
  TDM_SB_DELIVERY_COMPANY_SVCS = class(TDM_ddTable)
    SB_DELIVERY_COMPANY_SVCS: TSB_DELIVERY_COMPANY_SVCS;
    SB_DELIVERY_COMPANY_SVCSSB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField;
    SB_DELIVERY_COMPANY_SVCSSB_DELIVERY_COMPANY_NBR: TIntegerField;
    SB_DELIVERY_COMPANY_SVCSDESCRIPTION: TStringField;
    SB_DELIVERY_COMPANY_SVCSREFERENCE_FEE: TFloatField;
    SB_DELIVERY_COMPANY_SVCSDeliveryCompanyName: TStringField;
    SB_DELIVERY_COMPANY_SVCSNameAndService: TStringField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure SB_DELIVERY_COMPANY_SVCSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_DELIVERY_COMPANY_SVCS: TDM_SB_DELIVERY_COMPANY_SVCS;

implementation

{$R *.DFM}

procedure TDM_SB_DELIVERY_COMPANY_SVCS.SB_DELIVERY_COMPANY_SVCSCalcFields(
  DataSet: TDataSet);
begin
  SB_DELIVERY_COMPANY_SVCSNameAndService.AsString :=
    DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY.ShadowDataSet.CachedLookup(SB_DELIVERY_COMPANY_SVCSSB_DELIVERY_COMPANY_NBR.AsInteger, 'NAME')+ ' - '+
    SB_DELIVERY_COMPANY_SVCSDESCRIPTION.AsString;
end;

initialization
  RegisterClass(TDM_SB_DELIVERY_COMPANY_SVCS);

finalization
  UnregisterClass(TDM_SB_DELIVERY_COMPANY_SVCS);

end.
