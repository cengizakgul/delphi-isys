// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_MANUAL_ACH;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvConsts, EvTypes,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvUtils, EvExceptions, EvContext, EvClientDataSet;

type
  TDM_CO_MANUAL_ACH = class(TDM_ddTable)
    CO_MANUAL_ACH: TCO_MANUAL_ACH;
    CO_MANUAL_ACHTO_EE_CHILD_SUPPORT_CASES_NBR: TIntegerField;
    procedure CO_MANUAL_ACHBeforeEdit(DataSet: TDataSet);
    procedure CO_MANUAL_ACHBeforePost(DataSet: TDataSet);
    procedure CO_MANUAL_ACHBeforeDelete(DataSet: TDataSet);
    procedure CO_MANUAL_ACHCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CO_MANUAL_ACH.CO_MANUAL_ACHBeforeEdit(DataSet: TDataSet);
begin
  if (DataSet.FieldByName('STATUS').AsString[1] in [COMMON_REPORT_STATUS__COMPLETED, COMMON_REPORT_STATUS__DELETED]) and ctx_DataAccess.CheckACHstatus then
    raise EUpdateError.CreateHelp('Can not change processed Manual ACH!', IDH_ConsistencyViolation);
end;

procedure TDM_CO_MANUAL_ACH.CO_MANUAL_ACHBeforePost(DataSet: TDataSet);
begin
  if (DataSet.FieldByName('FROM_SB_BANK_ACCOUNTS_NBR').IsNull) and (DataSet.FieldByName('FROM_CL_BANK_ACCOUNT_NBR').IsNull) and
    (DataSet.FieldByName('FROM_EE_DIRECT_DEPOSIT_NBR').IsNull) then
    raise EUpdateError.CreateHelp('"FROM" bank account must be defined!', IDH_ConsistencyViolation);
  if (DataSet.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').IsNull) and (DataSet.FieldByName('TO_CL_BANK_ACCOUNT_NBR').IsNull) and
    (DataSet.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').IsNull) then
    raise EUpdateError.CreateHelp('"TO" bank account must be defined!', IDH_ConsistencyViolation);

  if DataSet.FieldByName('STATUS').IsNull then
    DataSet.FieldByName('STATUS').AsString := COMMON_REPORT_STATUS__PENDING;

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

procedure TDM_CO_MANUAL_ACH.CO_MANUAL_ACHBeforeDelete(DataSet: TDataSet);
begin
  if DataSet.FieldByName('STATUS').AsString[1] in [COMMON_REPORT_STATUS__COMPLETED, COMMON_REPORT_STATUS__DELETED] then
    raise EUpdateError.CreateHelp('Can not delete processed Manual ACH!', IDH_ConsistencyViolation);
end;

procedure TDM_CO_MANUAL_ACH.CO_MANUAL_ACHCalcFields(DataSet: TDataSet);
var
  TmpStr: String;
begin
  inherited;
  TmpStr := Dataset.FieldByName('FILLER').AsString;
  TmpStr := Trim(ExtractFromFiller(TmpStr, 11,10));
  if TmpStr = '' then
    CO_MANUAL_ACHTO_EE_CHILD_SUPPORT_CASES_NBR.Clear
  else
    CO_MANUAL_ACHTO_EE_CHILD_SUPPORT_CASES_NBR.AsString := TmpStr;
end;

initialization
  RegisterClass(TDM_CO_MANUAL_ACH);

finalization
  UnregisterClass(TDM_CO_MANUAL_ACH);

end.
