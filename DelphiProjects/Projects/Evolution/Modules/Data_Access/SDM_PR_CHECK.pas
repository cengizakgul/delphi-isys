// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_CHECK;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, EvContext,
  Variants, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, EvDataAccessComponents, ISDataAccessComponents, EvExceptions, EvUIComponents, EvClientDataSet;

type
  TDM_PR_CHECK = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_CHECK: TPR_CHECK;
    PR_CHECKPR_CHECK_NBR: TIntegerField;
    PR_CHECKEE_NBR: TIntegerField;
    PR_CHECKPR_NBR: TIntegerField;
    PR_CHECKPR_BATCH_NBR: TIntegerField;
    PR_CHECKCUSTOM_PR_BANK_ACCT_NUMBER: TStringField;
    PR_CHECKPAYMENT_SERIAL_NUMBER: TIntegerField;
    PR_CHECKCHECK_TYPE: TStringField;
    PR_CHECKSALARY: TFloatField;
    PR_CHECKEXCLUDE_DD: TStringField;
    PR_CHECKEXCLUDE_DD_EXCEPT_NET: TStringField;
    PR_CHECKEXCLUDE_TIME_OFF_ACCURAL: TStringField;
    PR_CHECKEXCLUDE_AUTO_DISTRIBUTION: TStringField;
    PR_CHECKEXCLUDE_ALL_SCHED_E_D_CODES: TStringField;
    PR_CHECKEXCLUDE_SCH_E_D_FROM_AGCY_CHK: TStringField;
    PR_CHECKEXCLUDE_SCH_E_D_EXCEPT_PENSION: TStringField;
    PR_CHECKPRORATE_SCHEDULED_E_DS: TStringField;
    PR_CHECKOR_CHECK_FEDERAL_VALUE: TFloatField;
    PR_CHECKOR_CHECK_FEDERAL_TYPE: TStringField;
    PR_CHECKOR_CHECK_OASDI: TFloatField;
    PR_CHECKOR_CHECK_MEDICARE: TFloatField;
    PR_CHECKOR_CHECK_EIC: TFloatField;
    PR_CHECKOR_CHECK_BACK_UP_WITHHOLDING: TFloatField;
    PR_CHECKEXCLUDE_FEDERAL: TStringField;
    PR_CHECKEXCLUDE_ADDITIONAL_FEDERAL: TStringField;
    PR_CHECKEXCLUDE_EMPLOYEE_OASDI: TStringField;
    PR_CHECKEXCLUDE_EMPLOYER_OASDI: TStringField;
    PR_CHECKEXCLUDE_EMPLOYEE_MEDICARE: TStringField;
    PR_CHECKEXCLUDE_EMPLOYER_MEDICARE: TStringField;
    PR_CHECKEXCLUDE_EMPLOYEE_EIC: TStringField;
    PR_CHECKEXCLUDE_EMPLOYER_FUI: TStringField;
    PR_CHECKTAX_AT_SUPPLEMENTAL_RATE: TStringField;
    PR_CHECKTAX_FREQUENCY: TStringField;
    PR_CHECKOVERRIDE_CHECK_NOTES: TBlobField;
    PR_CHECKCHECK_STATUS: TStringField;
    PR_CHECKSTATUS_CHANGE_DATE: TDateTimeField;
    PR_CHECKGROSS_WAGES: TFloatField;
    PR_CHECKNET_WAGES: TFloatField;
    PR_CHECKFEDERAL_TAXABLE_WAGES: TFloatField;
    PR_CHECKFEDERAL_TAX: TFloatField;
    PR_CHECKEE_OASDI_TAXABLE_WAGES: TFloatField;
    PR_CHECKEE_OASDI_TAXABLE_TIPS: TFloatField;
    PR_CHECKEE_OASDI_TAX: TFloatField;
    PR_CHECKEE_MEDICARE_TAXABLE_WAGES: TFloatField;
    PR_CHECKEE_MEDICARE_TAX: TFloatField;
    PR_CHECKEE_EIC_TAX: TFloatField;
    PR_CHECKER_OASDI_TAXABLE_WAGES: TFloatField;
    PR_CHECKER_OASDI_TAXABLE_TIPS: TFloatField;
    PR_CHECKER_OASDI_TAX: TFloatField;
    PR_CHECKER_MEDICARE_TAXABLE_WAGES: TFloatField;
    PR_CHECKER_MEDICARE_TAX: TFloatField;
    PR_CHECKER_FUI_TAXABLE_WAGES: TFloatField;
    PR_CHECKER_FUI_TAX: TFloatField;
    PR_CHECKEXCLUDE_FROM_AGENCY: TStringField;
    PR_CHECKFEDERAL_SHORTFALL: TFloatField;
    PR_CHECKFILLER: TStringField;
    PR_CHECKEE_Custom_Number: TStringField;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    PR_CHECKEmployee_Name_Calculate: TStringField;
    PR_CHECKSOCIAL_SECURITY_NUMBER: TStringField;
    PR_CHECKCUSTOM_COMPANY_NUMBER: TStringField;
    PR_CHECKCUSTOM_DEPARTMENT_NUMBER: TStringField;
    PR_CHECKCUSTOM_DIVISION_NUMBER: TStringField;
    PR_CHECKCUSTOM_BRANCH_NUMBER: TStringField;
    PR_CHECKCUSTOM_TEAM_NUMBER: TStringField;
    PR_CHECKCUSTOM_COMPANY_NAME: TStringField;
    PR_CHECKCUSTOM_DIVISION_NAME: TStringField;
    PR_CHECKCUSTOM_BRANCH_NAME: TStringField;
    PR_CHECKCUSTOM_DEPARTMENT_NAME: TStringField;
    PR_CHECKCUSTOM_TEAM_NAME: TStringField;
    PR_CHECKER_FUI_GROSS_WAGES: TFloatField;
    PR_CHECKCHECK_TYPE_945: TStringField;
    PR_CHECKFEDERAL_GROSS_WAGES: TFloatField;
    PR_CHECKEE_OASDI_GROSS_WAGES: TFloatField;
    PR_CHECKER_OASDI_GROSS_WAGES: TFloatField;
    PR_CHECKEE_MEDICARE_GROSS_WAGES: TFloatField;
    PR_CHECKER_MEDICARE_GROSS_WAGES: TFloatField;
    PR_CHECKSortTag: TIntegerField;
    PR_CHECKSortCheckType: TStringField;
    PR_CHECKCALCULATE_OVERRIDE_TAXES: TStringField;
    PR_CHECKDISABLE_SHORTFALLS: TStringField;
    procedure PR_CHECKAfterCancel(DataSet: TDataSet);
    procedure PR_CHECKAfterDelete(DataSet: TDataSet);
    procedure PR_CHECKAfterInsert(DataSet: TDataSet);
    procedure PR_CHECKBeforeDelete(DataSet: TDataSet);
    procedure PR_CHECKBeforeEdit(DataSet: TDataSet);
    procedure PR_CHECKBeforeInsert(DataSet: TDataSet);
    procedure PR_CHECKBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure PR_CHECKAfterOpen(DataSet: TDataSet);
    procedure PR_CHECKCalcFields(DataSet: TDataSet);
    procedure PR_CHECKNewRecord(DataSet: TDataSet);
    procedure PR_CHECKPrepareLookups(var Lookups: TLookupArray);
    procedure PR_CHECKDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure PR_CHECKUnPrepareLookups;
  private
    procedure CreatePRCheckDetails(CreatingNewBatch: Boolean);
  end;

procedure CheckCheckBeforePost(DataSet: TDataSet);

var
  DM_PR_CHECK: TDM_PR_CHECK;

implementation

{$R *.DFM}

uses
  EvUtils, EvConsts, SDataAccessCommon, EvStreamUtils;

procedure TDM_PR_CHECK.CreatePRCheckDetails(CreatingNewBatch: Boolean);
begin
  with DM_PAYROLL do
  begin
    ctx_DataAccess.Validate := False;
    try
      try
        ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
          PR,
          PR_BATCH,
          PR_CHECK,
          PR_CHECK_LINES,
          PR_CHECK_STATES,
          PR_CHECK_SUI,
          PR_CHECK_LOCALS,
          PR_CHECK_LINE_LOCALS,
          PR_SCHEDULED_E_DS,
          DM_CLIENT.CL_E_DS,
          DM_CLIENT.CL_PERSON,
          DM_CLIENT.CL_PENSION,
          DM_COMPANY.CO,
          DM_COMPANY.CO_E_D_CODES,
          DM_COMPANY.CO_STATES,
          DM_EMPLOYEE.EE,
          DM_EMPLOYEE.EE_SCHEDULED_E_DS,
          DM_EMPLOYEE.EE_STATES,
          DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
          DM_SYSTEM_STATE.SY_STATES,
          False,
          False,
          False,
          False,
          CreatingNewBatch);
      except
        if PR_CHECK_LINES.Active then
          PR_CHECK_LINES.CancelUpdates;
        if PR_CHECK_LOCALS.Active then
          PR_CHECK_LOCALS.CancelUpdates;
        if PR_CHECK_SUI.Active then
          PR_CHECK_SUI.CancelUpdates;
        if PR_CHECK_STATES.Active then
          PR_CHECK_STATES.CancelUpdates;
        if PR_CHECK_LINE_LOCALS.Active then
          PR_CHECK_LINE_LOCALS.CancelUpdates;
        if ctx_DataAccess.CUSTOM_VIEW.Active then
          ctx_DataAccess.CUSTOM_VIEW.CancelUpdates;
        raise;
      end;
    finally
      ctx_DataAccess.Validate := True;
    end;

    if not ctx_DataAccess.NewBatchInserted and not ctx_DataAccess.TempCommitDisable then
    begin
      if (PR_CHECK_LOCALS.Active) and (PR_CHECK_SUI.Active) and (PR_CHECK_STATES.Active) and (PR_CHECK_LINE_LOCALS.Active) then
        ctx_DataAccess.PostDataSets([PR_CHECK_LINES, PR_CHECK_LOCALS, PR_CHECK_SUI, PR_CHECK_STATES, PR_CHECK_LINE_LOCALS])
      else
        if PR_CHECK_LINES.Active then
          ctx_DataAccess.PostDataSets([PR_CHECK_LINES]);
    end;
  end;
end;

procedure TDM_PR_CHECK.PR_CHECKAfterCancel(DataSet: TDataSet);
begin
  ctx_DataAccess.NewCheckInserted := False;
  ctx_DataAccess.TemplateNumber := 0;
end;

procedure TDM_PR_CHECK.PR_CHECKAfterDelete(DataSet: TDataSet);
begin
  ctx_DataAccess.DeletingVoidCheck := False;
  if ctx_DataAccess.CUSTOM_VIEW.ChangeCount > 0 then
    ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
end;

procedure TDM_PR_CHECK.PR_CHECKAfterInsert(DataSet: TDataSet);
begin
  if ctx_DataAccess.NewBatchInserted and (ctx_DataAccess.NextInternalCheckNbr <= ctx_DataAccess.LastInternalCheckNbr) then
  begin
    DataSet.FieldByName('PR_CHECK_NBR').AsInteger := ctx_DataAccess.NextInternalCheckNbr;
    ctx_DataAccess.NextInternalCheckNbr := ctx_DataAccess.NextInternalCheckNbr + 1;
  end;
  ctx_DataAccess.NewCheckInserted := True;
  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
      DataSet.FieldByName('CALCULATE_OVERRIDE_TAXES').Value := GROUP_BOX_NO;
end;

procedure TDM_PR_CHECK.PR_CHECKBeforeDelete(DataSet: TDataSet);
var
  VoidCheckNbr: String;
  PrNbr, ChNbr: Integer;
begin
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_CHECK_NBR=' + Dataset.FieldByName('PR_CHECK_NBR').AsString);
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Filter := 'PR_CHECK_NBR=' + Dataset.FieldByName('PR_CHECK_NBR').AsString;
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Filtered := True; 
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.First;
  while not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Eof do
  begin
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.First;
  end;
  ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER]);

  PayrollBeforeDelete(DataSet);

  if DataSet.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
  begin
    VoidCheckNbr := Trim(ExtractFromFiller(PR_CHECK.FieldByName('FILLER').AsString, 1, 8));
    if VoidCheckNbr <> '' then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try
        if Active then
          Close;
        with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
        begin
          SetMacro('NbrField', 'pr_check');
          SetMacro('Columns', '*');
          SetMacro('TableName', 'pr_check');
          SetParam('RecordNbr', StrToInt(VoidCheckNbr));
          DataRequest(AsVariant);
        end;
        Open;
        First;
        ChNbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
        Close;
        with TExecDSWrapper.Create('pr_check;GenericSelectCurrentNBR') do
        begin
          SetMacro('NbrField', 'pr_check');
          SetMacro('Columns', '*');
          SetMacro('TableName', 'pr_check');
          SetParam('RecordNbr', StrToInt(VoidCheckNbr));
          DataRequest(AsVariant);
        end;
        Open;
        First;
        PrNbr := FieldByName('PR_NBR').AsInteger;
        ctx_DataAccess.SetPayrollLockInTransaction(PrNbr, True);
        try
          Edit;
          FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
          Post;
          ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
        finally
          ctx_DataAccess.SetPayrollLockInTransaction(PrNbr, False);
          DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', ChNbr, []);
        end;
        ctx_DataAccess.CommitNestedTransaction;
      except
        on E: Exception do
        begin
          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      end;
    end;
  end;
  if DataSet.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
    ctx_DataAccess.DeletingVoidCheck := True;
end;

procedure TDM_PR_CHECK.PR_CHECKBeforeEdit(DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_CHECK.PR_CHECKBeforeInsert(DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure CheckCheckBeforePost(DataSet: TDataSet);
begin
  if (DM_PAYROLL.PR.Lookup('PR_NBR', DataSet.FieldByName('PR_NBR').Value, 'STATUS') = PAYROLL_STATUS_PROCESSED)
  or (DM_PAYROLL.PR.Lookup('PR_NBR', DataSet.FieldByName('PR_NBR').Value, 'STATUS') = PAYROLL_STATUS_VOIDED) then
    Exit;
  if (DataSet.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) and
     DataSet.FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull then
    raise EUpdateError.CreateHelp('You must enter federal override value', IDH_ConsistencyViolation);

  if DataSet.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD] then
  begin
    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_SETUP then
      raise EUpdateError.CreateHelp('You can not have Setup YTD or QTD check types on this payroll.', IDH_ConsistencyViolation);
  end
  else
  begin
    if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP) and (DataSet.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_3RD_PARTY) then
      raise EUpdateError.CreateHelp('You can not have this check type on a Setup payroll.', IDH_ConsistencyViolation);
  end;
end;

procedure TDM_PR_CHECK.PR_CHECKBeforePost(DataSet: TDataSet);
begin
  if ctx_DataAccess.SkipPrCheckPostCheck then
    Exit;

  with ctx_PayrollCalculation do
  begin
    if (DM_PAYROLL.PR.Lookup('PR_NBR', DataSet.FieldByName('PR_NBR').Value, 'STATUS') = PAYROLL_STATUS_PROCESSED)
    or (DM_PAYROLL.PR.Lookup('PR_NBR', DataSet.FieldByName('PR_NBR').Value, 'STATUS') = PAYROLL_STATUS_VOIDED) then
      Exit;

    CheckCheckBeforePost(DataSet);

    if VarIsNull(DataSet.FieldByName('PR_NBR').Value) then
      DataSet.FieldByName('PR_NBR').Value := DM_PAYROLL.PR.FieldByName('PR_NBR').Value;
    if VarIsNull(DataSet.FieldByName('PR_BATCH_NBR').Value) then
      DataSet.FieldByName('PR_BATCH_NBR').Value := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').Value;
    if VarIsNull(DataSet.FieldByName('CHECK_STATUS').Value) then
      DataSet.FieldByName('CHECK_STATUS').Value := CHECK_STATUS_OUTSTANDING;

    if DataSet.FieldByName('STATUS_CHANGE_DATE').IsNull then DataSet.FieldByName('STATUS_CHANGE_DATE').Value := SysTime;


    if ctx_DataAccess.NewCheckInserted and (DataSet.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_MANUAL) then
      DataSet.FieldByName('CALCULATE_OVERRIDE_TAXES').Value := GROUP_BOX_NO;


    if ctx_DataAccess.NewCheckInserted and (DataSet.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID) then
      CreatePRCheckDetails(False)
    else
    begin
      if DataSet.FieldByName('PAYMENT_SERIAL_NUMBER').IsNull then
        DataSet.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GetNextPaymentSerialNbr;

      if PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').IsNull then
      begin
        DM_COMPANY.CO.Activate;
        PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := GetCustomBankAccountNumber(PR_CHECK);
        PR_CHECK.FieldByName('ABA_NUMBER').Value := GetABANumber;
      end;

      if (not ctx_DataAccess.CheckCalcInProgress) and (not ctx_DataAccess.NewBatchInserted) and (not ctx_DataAccess.NewCheckInserted)
      and (not ctx_DataAccess.PayrollIsProcessing) and not (DM_PAYROLL.PR_CHECK_LINES.State in [dsInsert, dsEdit]) then
        ctx_PayrollCalculation.BlockEverythingOnCheck(PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_EMPLOYEE.EE_SCHEDULED_E_DS, not ctx_DataAccess.TempCommitDisable);
    end;
  end;

end;

procedure TDM_PR_CHECK.DataModuleCreate(Sender: TObject);
begin
  ctx_DataAccess.NextInternalCheckNbr := 0;
  ctx_DataAccess.LastInternalCheckNbr := -1;
  ctx_DataAccess.NewCheckInserted := False;
  ctx_DataAccess.TemplateNumber := 0;
  ctx_DataAccess.CheckCalcInProgress := False;
  ctx_DataAccess.DeletingVoidCheck := False;
  DM_EMPLOYEE.EE;
end;

procedure TDM_PR_CHECK.PR_CHECKAfterOpen(DataSet: TDataSet);
var
  s: string;
begin
  s := '';

  if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString < CLIENT_LEVEL_COMPANY_NAME then
  begin
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_COMPANY then
      s := s + 'CUSTOM_COMPANY_NUMBER;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_DIVISION then
      s := s + 'CUSTOM_DIVISION_NUMBER;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_BRANCH then
      s := s + 'CUSTOM_BRANCH_NUMBER;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_DEPT then
      s := s + 'CUSTOM_DEPARTMENT_NUMBER;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_TEAM then
      s := s + 'CUSTOM_TEAM_NUMBER;';
  end
  else
  begin
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_COMPANY_NAME then
      s := s + 'CUSTOM_COMPANY_NAME;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_DIVISION_NAME then
      s := s + 'CUSTOM_DIVISION_NAME;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_BRANCH_NAME then
      s := s + 'CUSTOM_BRANCH_NAME;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_DEPT_NAME then
      s := s + 'CUSTOM_DEPARTMENT_NAME;';
    if DM_COMPANY.CO.FieldByName('SECONDARY_SORT_FIELD').AsString >= CLIENT_LEVEL_TEAM_NAME then
      s := s + 'CUSTOM_TEAM_NAME;';
  end;

  if DM_COMPANY.CO.FieldByName('PRIMARY_SORT_FIELD').AsString = SORT_FIELD_EE_Code then
    s := s + 'EE_Custom_Number;'
  else if DM_COMPANY.CO.FieldByName('PRIMARY_SORT_FIELD').AsString = SORT_FIELD_SS_Nbr then
    s := s + 'SOCIAL_SECURITY_NUMBER;'
  else if DM_COMPANY.CO.FieldByName('PRIMARY_SORT_FIELD').AsString = SORT_FIELD_ALPHA then
    s := s + 'Employee_Name_Calculate;';

  if Length(s) <> 0 then
    s := Copy(s, 1, Pred(Length(s)));

  TevClientDataSet(DataSet).IndexFieldNames := s;
  DataSet.First;
end;

procedure TDM_PR_CHECK.PR_CHECKCalcFields(DataSet: TDataSet);
var
  s: string;
  i: Integer;
  ee: TEvClientDataSet;
begin
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_VOID then
    PR_CHECKSortCheckType.Value := 'A';
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_VOUCHER then
    PR_CHECKSortCheckType.Value := 'B';
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_MANUAL then
  begin
    if PR_CHECKOR_CHECK_OASDI.IsNull
    and PR_CHECKOR_CHECK_MEDICARE.IsNull
    and PR_CHECKOR_CHECK_FEDERAL_VALUE.IsNull
    and PR_CHECKOR_CHECK_EIC.IsNull
    and PR_CHECKOR_CHECK_BACK_UP_WITHHOLDING.IsNull then
      PR_CHECKSortCheckType.Value := 'D'
    else
      PR_CHECKSortCheckType.Value := 'C';
  end;
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_YTD then
    PR_CHECKSortCheckType.Value := 'E';
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_QTD then
    PR_CHECKSortCheckType.Value := 'F';
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_REGULAR then
  begin
    if PR_CHECKOR_CHECK_OASDI.IsNull
    and PR_CHECKOR_CHECK_MEDICARE.IsNull
    and PR_CHECKOR_CHECK_FEDERAL_VALUE.IsNull
    and PR_CHECKOR_CHECK_EIC.IsNull
    and PR_CHECKOR_CHECK_BACK_UP_WITHHOLDING.IsNull then
      PR_CHECKSortCheckType.Value := 'H'
    else
      PR_CHECKSortCheckType.Value := 'G';
  end;
  if PR_CHECKCHECK_TYPE.Value = CHECK_TYPE2_3RD_PARTY then
    PR_CHECKSortCheckType.Value := 'I';
  if TevClientDataSet(DataSet).LookupsEnabled then
  begin
    i := PR_CHECKPR_CHECK_NBR.AsInteger;
    if i >= 0 then
      PR_CHECKSortTag.AsInteger := i
    else
      PR_CHECKSortTag.AsInteger := 100000000- i;
    ee := DM_EMPLOYEE.EE;
    if not TevClientDataSet(DataSet).UseLookupCache then
    begin
      if ee.Active then
        if ee.ShadowDataSet.FindKey([PR_CHECKEE_NBR.AsInteger]) then
        begin
          PR_CHECKEE_Custom_Number.AsString := ee.ShadowDataSet.Fields[PR_CHECKEE_Custom_Number.Tag].AsString;
          PR_CHECKEmployee_Name_Calculate.AsString := ee.ShadowDataSet.Fields[PR_CHECKEmployee_Name_Calculate.Tag].AsString;
          PR_CHECKSOCIAL_SECURITY_NUMBER.AsString := ee.ShadowDataSet.Fields[PR_CHECKSOCIAL_SECURITY_NUMBER.Tag].AsString;
          PR_CHECKCUSTOM_COMPANY_NUMBER.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_COMPANY_NUMBER.Tag].AsString;
          PR_CHECKCUSTOM_DIVISION_NUMBER.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_DIVISION_NUMBER.Tag].AsString;
          PR_CHECKCUSTOM_BRANCH_NUMBER.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_BRANCH_NUMBER.Tag].AsString;
          PR_CHECKCUSTOM_DEPARTMENT_NUMBER.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_DEPARTMENT_NUMBER.Tag].AsString;
          PR_CHECKCUSTOM_TEAM_NUMBER.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_TEAM_NUMBER.Tag].AsString;
          PR_CHECKCUSTOM_COMPANY_NAME.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_COMPANY_NAME.Tag].AsString;
          PR_CHECKCUSTOM_DIVISION_NAME.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_DIVISION_NAME.Tag].AsString;
          PR_CHECKCUSTOM_BRANCH_NAME.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_BRANCH_NAME.Tag].AsString;
          PR_CHECKCUSTOM_DEPARTMENT_NAME.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_DEPARTMENT_NAME.Tag].AsString;
          PR_CHECKCUSTOM_TEAM_NAME.AsString := ee.ShadowDataSet.Fields[PR_CHECKCUSTOM_TEAM_NAME.Tag].AsString;
        end;
    end
    else
    begin
      s := PR_CHECKEE_NBR.AsString;
      PR_CHECKEE_Custom_Number.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_EMPLOYEE_NUMBER');
      PR_CHECKEmployee_Name_Calculate.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'Employee_Name_Calculate');
      PR_CHECKSOCIAL_SECURITY_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'SOCIAL_SECURITY_NUMBER');
      PR_CHECKCUSTOM_COMPANY_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_COMPANY_NUMBER');
      PR_CHECKCUSTOM_DIVISION_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_DIVISION_NUMBER');
      PR_CHECKCUSTOM_BRANCH_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_BRANCH_NUMBER');
      PR_CHECKCUSTOM_DEPARTMENT_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_DEPARTMENT_NUMBER');
      PR_CHECKCUSTOM_TEAM_NUMBER.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_TEAM_NUMBER');
      PR_CHECKCUSTOM_COMPANY_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_COMPANY_NAME');
      PR_CHECKCUSTOM_DIVISION_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_DIVISION_NAME');
      PR_CHECKCUSTOM_BRANCH_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_BRANCH_NAME');
      PR_CHECKCUSTOM_DEPARTMENT_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_DEPARTMENT_NAME');
      PR_CHECKCUSTOM_TEAM_NAME.AsString := TevClientDataSet(DataSet).FindCachedLookupValue(s, ee, 'EE_NBR', 'CUSTOM_TEAM_NAME');
    end;
  end;
end;

procedure TDM_PR_CHECK.PR_CHECKNewRecord(DataSet: TDataSet);
begin
  DataSet['PR_NBR'] := DM_PAYROLL.PR_BATCH['PR_NBR'];
end;

procedure TDM_PR_CHECK.PR_CHECKPrepareLookups(var Lookups: TLookupArray);
var
  iPos: Integer;
begin
  iPos := Length(Lookups);
  SetLength(Lookups, iPos + 13);
  with Lookups[iPos] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_EMPLOYEE_NUMBER';
  end;
  with Lookups[iPos + 1] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'Employee_Name_Calculate';
  end;
  with Lookups[iPos + 2] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'SOCIAL_SECURITY_NUMBER';
  end;
  with Lookups[iPos + 3] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_COMPANY_NUMBER';
  end;
  with Lookups[iPos + 4] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_DIVISION_NUMBER';
  end;
  with Lookups[iPos + 5] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_BRANCH_NUMBER';
  end;
  with Lookups[iPos + 6] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_DEPARTMENT_NUMBER';
  end;
  with Lookups[iPos + 7] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_TEAM_NUMBER';
  end;
  with Lookups[iPos + 8] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_COMPANY_NAME';
  end;
  with Lookups[iPos + 9] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_DIVISION_NAME';
  end;
  with Lookups[iPos + 10] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_BRANCH_NAME';
  end;
  with Lookups[iPos + 11] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_DEPARTMENT_NAME';
  end;
  with Lookups[iPos + 12] do
  begin
    DataSet := DM_EMPLOYEE.EE;
    sKeyField := 'EE_NBR';
    sLookupResultField := 'CUSTOM_TEAM_NAME';
  end;
end;

procedure TDM_PR_CHECK.PR_CHECKDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  ctx_DataAccess.DeletingVoidCheck := False;
end;

procedure TDM_PR_CHECK.PR_CHECKUnPrepareLookups;
begin
  PR_CHECK.SetLookupFieldTag(PR_CHECKEE_Custom_Number, 'CUSTOM_EMPLOYEE_NUMBER', DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKEmployee_Name_Calculate, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKSOCIAL_SECURITY_NUMBER, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_COMPANY_NUMBER, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_DIVISION_NUMBER, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_BRANCH_NUMBER, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_DEPARTMENT_NUMBER, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_TEAM_NUMBER, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_COMPANY_NAME, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_DIVISION_NAME, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_BRANCH_NAME, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_DEPARTMENT_NAME, DM_EMPLOYEE.EE);
  PR_CHECK.SetLookupFieldTag(PR_CHECKCUSTOM_TEAM_NAME, DM_EMPLOYEE.EE);
end;

initialization
  RegisterClass(TDM_PR_CHECK);

finalization
  UnregisterClass(TDM_PR_CHECK);

end.

