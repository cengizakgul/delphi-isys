inherited DM_EE_WORK_SHIFTS: TDM_EE_WORK_SHIFTS
  OldCreateOrder = True
  Left = 259
  Top = 213
  Height = 479
  Width = 741
  object EE_WORK_SHIFTS: TEE_WORK_SHIFTS
    ProviderName = 'EE_WORK_SHIFTS_PROV'
    BeforePost = EE_WORK_SHIFTSBeforePost
    BeforeDelete = EE_WORK_SHIFTSBeforeDelete
    Left = 40
    Top = 56
    object EE_WORK_SHIFTSCO_SHIFTS_NBR: TIntegerField
      FieldName = 'CO_SHIFTS_NBR'
      OnChange = EE_WORK_SHIFTSCO_SHIFTS_NBRChange
    end
    object EE_WORK_SHIFTSEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_WORK_SHIFTSEE_WORK_SHIFTS_NBR: TIntegerField
      FieldName = 'EE_WORK_SHIFTS_NBR'
    end
    object EE_WORK_SHIFTSSHIFT_PERCENTAGE: TFloatField
      FieldName = 'SHIFT_PERCENTAGE'
      DisplayFormat = '#,##0.00#####'
    end
    object EE_WORK_SHIFTSSHIFT_RATE: TFloatField
      FieldName = 'SHIFT_RATE'
      DisplayFormat = '#,###0.000###'
    end
    object EE_WORK_SHIFTSNameLKUP: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'NameLKUP'
      LookupDataSet = DM_CO_SHIFTS.CO_SHIFTS
      LookupKeyFields = 'CO_SHIFTS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_SHIFTS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 168
    Top = 64
  end
end
