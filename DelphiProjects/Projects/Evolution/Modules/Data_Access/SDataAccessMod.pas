// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDataAccessMod;

interface

uses
  Controls, SysUtils, Forms, Classes, Windows, isBaseClasses, EvCommonInterfaces,
  isDataAccessComponents,  SDDClasses, EvMainboard, EvContext, Variants,
  SDM_ddTable, EvConsts, isBasicUtils, TypInfo, DB, EvTypes, EvExceptions, DateUtils,
  EvStreamUtils, EvTransportInterfaces, ISZippingRoutines, isTypes, EvClientDataSet,
  EvBasicUtils, SEncryptionRoutines, EvClasses;


implementation

uses EvUtils, SDataStructure, EvDataset, EvDataAccessComponents, EvInitApp, SFieldCodeValues,
     EvSQLite;

type

  TDataAccessComp = class(TisInterfacedObject, IevDataAccess)
  private
    FHolder: TComponent;
    FDataSetCache: array [1..2000] of TddTable;
    FAsOfDate: TisDate;
    FPayrollIsLocked: Boolean;
    FCheckMaxAmountAndHours: Boolean;
    FRecalcLineBeforePost: Boolean;
    FNewBatchInserted: Boolean;
    FValidate: Boolean;
    FPayrollIsProcessing: Boolean;
    FImporting: Boolean;
    FCopying: Boolean;
    FNewCheckInserted: Boolean;
    FCheckCalcInProgress: Boolean;
    FTemplateNumber: Integer;
    FNextInternalCheckNbr: Integer;
    FLastInternalCheckNbr: Integer;
    FNextInternalCheckLineNbr: Integer;
    FLastInternalCheckLineNbr: Integer;
    FVoidingAgencyCheck: Boolean;
    FDeletingVoidCheck: Boolean;
    FTempCommitDisable: Boolean;
    FEESchedEDChanged: string;
    FSkipPrCheckPostCheck: Boolean;
    FCheckACHstatus: Boolean;
    FPrCheckLineLocations: IevDataset;
    FPrCheckLineLocationsM: IevDataset;
    FSQLite: ISQLite;
    FChangingDataSets: TList;
    FPostingDataSets: TList;
    FDataSetChanges: IisList;
    FCachedValues: IisListOfValues;
    FIsForTaxCalculator: Boolean;
    procedure OnCachedDatasetDestroy(Index: Integer);
    function  GetDM(const AName: String): TDM_ddTable;
    function  CreateDataSet(const ddTableClass: TddTableClass): TddTable;
    procedure ApplyKeyMap(const AKeyMap: IisValueMap);
    procedure OnDestroyDS(Sender: TObject);
    procedure ClearDSList;
    procedure CollectChanges(const ADataSet: TEvClientDataSet);
    procedure PostCollectedChanges;
    procedure ResolveCircularReferences;
    procedure GetCustomProvData(const cd, cv: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    function  GetFieldFilter(const d: TevClientDataSet; const FieldName: string; const prefix : string = ''): string;
    function  GetDataSetAsOfDate(const ADataSet: TEvBasicClientDataSet): TisDate;
    function  CalcQecHash(const CoNbr: Integer; const bDate, eDate: TDateTime): String;
  protected
    procedure DoOnConstruction; override;

    procedure StartNestedTransaction(const ADBTypes: TevDBTypes; const ATransactionName: String = '');
    function  CommitNestedTransaction: IisValueMap;
    procedure RollbackNestedTransaction;
    procedure OpenDataSets(const ADatasets: array of TevClientDataSet);
    procedure PostDataSets(const ADatasets: array of TevClientDataSet; const AResolveCircularReferences: Boolean = False);
    procedure CancelDataSets(const DS: array of TevClientDataSet);
    procedure Activate(const DS: array of TevClientDataSet);
    function  GetDataSet(const ddTableClass: TddTableClass): TddTable;
    function  GetCachedDataSet(const ClassIndex: Integer): TddTable;
    function  GetHolder: TComponent;
    procedure OpenClient(const AClientNbr: Integer);
    function  ClientID: Integer;
    procedure ClearCache(const ADBType: TevDBTypes = []);
    procedure ClearCachedData;
    function  GetAsOfDate: TisDate;
    procedure SetAsOfDate(const AValue: TisDate);
    function  GetCheckMaxAmountAndHours: Boolean;
    procedure SetCheckMaxAmountAndHours(const AValue: Boolean);
    function  GetRecalcLineBeforePost: Boolean;
    procedure SetRecalcLineBeforePost(const AValue: Boolean);
    function  GetNewBatchInserted: Boolean;
    procedure SetNewBatchInserted(const AValue: Boolean);
    function  GetValidate: Boolean;
    procedure SetValidate(const AValue: Boolean);
    procedure UnlockPR;
    procedure LockPR(const AllOK: Boolean);
    function  QecRecommended(const CoNbr: Integer; const CheckDate: TDateTime): Boolean;
    procedure DoOnQecFinished(const CoNbr: Integer; const CheckDate: TDateTime);
    procedure LockPRSimple;
    procedure UnlockPRSimple;
    function  PayrollLocked: Boolean;
    function  GetPayrollIsProcessing: Boolean;
    procedure SetPayrollIsProcessing(const AValue: Boolean);
    function  GetImporting: Boolean;
    procedure SetImporting(const AValue: Boolean);
    function  GetCopying: Boolean;
    procedure SetCopying(const AValue: Boolean);
    function  GetNewCheckInserted: Boolean;
    procedure SetNewCheckInserted(const AValue: Boolean);
    function  GetCheckCalcInProgress: Boolean;
    procedure SetCheckCalcInProgress(const AValue: Boolean);
    function  GetTemplateNumber: Integer;
    procedure SetTemplateNumber(const AValue: Integer);
    function  GetNextInternalCheckNbr: Integer;
    procedure SetNextInternalCheckNbr(const AValue: Integer);
    function  GetLastInternalCheckNbr: Integer;
    procedure SetLastInternalCheckNbr(const AValue: Integer);
    function  GetNextInternalCheckLineNbr: Integer;
    procedure SetNextInternalCheckLineNbr(const AValue: Integer);
    function  GetLastInternalCheckLineNbr: Integer;
    procedure SetLastInternalCheckLineNbr(const AValue: Integer);
    function  GetVoidingAgencyCheck: Boolean;
    procedure SetVoidingAgencyCheck(const AValue: Boolean);
    function  GetDeletingVoidCheck: Boolean;
    procedure SetDeletingVoidCheck(const AValue: Boolean);
    function  GetTempCommitDisable: Boolean;
    procedure SetTempCommitDisable(const AValue: Boolean);
    function  GetEESchedEDChanged: string;
    procedure SetEESchedEDChanged(const AValue: string);
    function  GetSkipPrCheckPostCheck: Boolean;
    procedure SetSkipPrCheckPostCheck(const AValue: Boolean);
    function  GetCheckACHstatus: Boolean;
    procedure SetCheckACHstatus(const AValue: Boolean);
    procedure AddTOAForEE;
    procedure AddEdsForEE;
    function  GetPrCheckLineLocations: IevDataset;
    function  GetPrCheckLineLocationsM: IevDataset;
    procedure OpenEEDataSetForCompany(aDataSet: TevClientDataSet; CoNbr: Integer; const Condition: string = '');
    procedure CheckCompanyLock(const CoNbr: Integer = 0; dAsOfDate: TDateTime = 0; const DoNotReOpen: Boolean = False; CheckForTaxPmtsFlag: boolean = False);
    procedure GetCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    procedure GetSyCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    procedure GetSbCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    procedure GetTmpCustomData(const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
    function  CUSTOM_VIEW: TevClientDataSet;
    function  SY_CUSTOM_VIEW: TevClientDataSet;
    function  SB_CUSTOM_VIEW: TevClientDataSet;
    function  TEMP_CUSTOM_VIEW: TevClientDataSet;
    function  HISTORY_VIEW: TevClientDataSet;
    function  GetCustomViewByddDatabase(const ddDatabaseClass: TddDatabaseClass): TevClientDataSet;
    function  DelayedCommit: Boolean;
    procedure LocateCheckReport(const CheckForm: String; const RegularCheck: Boolean);
    procedure FillSBCheckList(aRegularChecks: TStrings; aMiscChecks: TStrings);
    procedure UnlinkLiabilitiesFromDeposit(const d: TEvClientDataSet; const AFilter: string);
    function  TaxPaymentRollback(const SbTaxPaymentNbr, i : Integer): Boolean;
    function  CheckSbTaxPaymentstatus(const SbTaxPaymentNbr : integer): Boolean;
    function  GetClCoReadOnlyRight: Boolean;
    function  GetClReadOnlyRight: Boolean;
    function  GetCoReadOnlyRight: Boolean;
    function  GetEEReadOnlyRight: Boolean;    
    procedure SetPayrollLockInTransaction(const APrNbr: Integer; const AUnlocked: Boolean);
    function  GetSQLite: ISQLite;
    procedure SetSQLite(const Value: ISQLite);
    function  GetIsForTaxCalculator: Boolean;
    procedure SetIsForTaxCalculator(const Value: Boolean);
  public
    destructor Destroy; override;
  end;


  TDestroyCallBack = class(TComponent)
  public
    Index: Integer;
    CallBack: procedure (Index: Integer) of object;
    destructor Destroy; override;
  end;


function GetDataAccess: IInterface;
begin
  Result := TDataAccessComp.Create;
end;

// This function is used by TReader when form, frame or another data module gets created
function FindDM(const AName: string): TComponent;
begin
  if Context <> nil then
    Result := TDataAccessComp((ctx_DataAccess as IisInterfacedObject).GetImplementation).GetDM(AName)
  else
    Result := nil;  
end;

{ TDataAccessComp }

procedure TDataAccessComp.ClearCache(const ADBType: TevDBTypes);
var
  i: Integer;
  T: TddTable;
begin
  ClearCachedData;

  for i := Low(FDataSetCache) to High(FDataSetCache) do
  begin
    T := FDataSetCache[i];
    if Assigned(T) and ((ADBType = []) or (T.GetDBType in ADBType)) then
      FDataSetCache[i].Close;
  end;

  FPrCheckLineLocations := nil;
  FPrCheckLineLocationsM := nil;
end;

function TDataAccessComp.CreateDataSet(const ddTableClass: TddTableClass): TddTable;
type
  TDMClass = class of TDM_ddTable;
var
  TDM: TDMClass;
  DM: TDM_ddTable;
  ds: TddTable;
  i: Integer;
  sName: string;
  TypeData: PTypeData;
  PropList: PPropList;
  Count: Integer;
  FClass: TClass;
  t: TStringList;
  f: TField;
  Ind: Integer;
begin
  sName := ddTableClass.ClassName;
  Delete(sName, 1, 1);
  DM := GetDM('DM_' + sName);

  if not Assigned(DM) then
  begin
    TDM := TDMClass(GetClass('TDM_' + sName));
    if Assigned(TDM) then
    begin
      DM := TDM.Create(FHolder);
      ds := DM.FindComponent(sName) as TddTable;
    end
    else
    begin
      DM := TDM_ddTable.Create(FHolder);
      ds := ddTableClass.Create(DM);
    end;
    DM.Name := 'DM_' + sName;

    if not Assigned(ds) then
      raise Exception.Create('Cannot find dataset ' + sName + ' in class ' + DM.ClassName);

    TypeData := GetTypeData(ds.ClassInfo);
    if TypeData.PropCount <> 0 then
    begin
      GetMem(PropList, SizeOf(PPropInfo) * TypeData.PropCount);
      try
        Count := GetPropList(ds.ClassInfo, [tkClass], PropList);
        t := TStringList.Create;
        try
          t.Assign(ds.FieldList);
          for i := 0 to Pred(Count) do
          begin
            FClass := GetTypeData(PropList[i]^.PropType^).ClassType;
            if FClass.InheritsFrom(TField) and (FClass <> TDatasetField) then
            begin
              f := ds.FindField(PropList[i]^.Name);
              if not Assigned(f) then
              begin
                f := TFieldClass(FClass).Create(DM);
                f.FieldName := PropList[i]^.Name;
                f.Name := ds.Name + f.FieldName;
                f.DataSet := ds;
              end;
              with f do
              begin
                Required := ds.GetIsRequiredField(FieldName)
//PLYMOUTH: probably should be just deleted
{                           and
                            (Pos(';' + FieldName + ';', ';' + ds.RequiredOverride + ';') = 0) and
                            (FieldName <> ds.StatusField) and
                            (FieldName <> ds.StatusDateField)
}
                            ;

                DisplayLabel := ds.GetFieldDisplayLabel(FieldName);
                Size := ds.GetFieldSize(FieldName);
              end;
              Ind := t.IndexOf(f.FieldName);
              if Ind >= 0 then
                t.Delete(Ind);
            end;
          end;
          for i := 0 to Pred(t.Count) do
          begin
            f := ds.FindField(t[i]);
            if Assigned(f) and (f.FieldKind = fkData) then
              f.Free;
          end;
        finally
          t.Free;
        end;
      finally
        FreeMem(PropList);
      end;
    end;
  end;

  Result := DM.FindComponent(sName) as TddTable;
end;

destructor TDataAccessComp.Destroy;
begin
  FreeAndNil(FHolder);
  FreeAndNil(FPostingDataSets);
  FreeAndNil(FChangingDataSets);
  inherited;
end;

procedure TDataAccessComp.DoOnConstruction;
begin
  inherited;
  FHolder := TComponent.Create(nil);
  FChangingDataSets := TList.Create;
  FPostingDataSets := TList.Create;
  FPayrollIsLocked := True;
  FIsForTaxCalculator := False;
  FCheckACHstatus := True;
  FDataSetChanges := TisList.Create;
  FCachedValues := TisListOfValues.Create;
end;

function TDataAccessComp.GetAsOfDate: TisDate;
begin
  if FAsOfDate = EmptyDate then
    Result := SysDate
  else
    Result := FAsOfDate;
end;

function TDataAccessComp.GetCachedDataSet(const ClassIndex: Integer): TddTable;
begin
  if (ClassIndex >= Low(FDataSetCache)) and (ClassIndex <= High(FDataSetCache)) then
    Result := FDataSetCache[ClassIndex]
  else
    Result := nil;
end;

function TDataAccessComp.GetDataSet(const ddTableClass: TddTableClass): TddTable;
var
  Index: Integer;
  Caller: TDestroyCallBack;
begin
  Index := ddTableClass.GetClassIndex;
  Assert((Index >= Low(FDataSetCache)) and (Index <= High(FDataSetCache)));
  if FDataSetCache[Index] = nil then
  begin
    FDataSetCache[Index] := CreateDataSet(ddTableClass);
    Caller := TDestroyCallBack.Create(FDataSetCache[Index]);
    Caller.Index := Index;
    Caller.CallBack := OnCachedDatasetDestroy;
  end;
  Result := FDataSetCache[Index];
end;

function TDataAccessComp.GetDM(const AName: String): TDM_ddTable;
begin
  Result := FHolder.FindComponent(AName) as TDM_ddTable;
end;

function TDataAccessComp.GetHolder: TComponent;
begin
  Result := FHolder;
end;

procedure TDataAccessComp.OnCachedDatasetDestroy(Index: Integer);
begin
  FDataSetCache[Index] := nil;
end;

procedure TDataAccessComp.OpenClient(const AClientNbr: Integer);
var
  i: Integer;
  T: TddTable;
begin
  if ctx_DBAccess.CurrentClientNbr <> AClientNbr then
  begin
    for i := Low(FDataSetCache) to High(FDataSetCache) do
    begin
      T := FDataSetCache[i];
      if Assigned(T) then
      begin
        if T.GetDBType = dbtClient then
          T.Close;
        if (T.ClientId >= 0) and (T.ClientId <> AClientNbr) then
          T.ClientId := AClientNbr;
      end;
    end;

    FPrCheckLineLocations := nil;
    FPrCheckLineLocationsM := nil;
    ClearCachedData;

    ctx_DBAccess.CurrentClientNbr := AClientNbr;
  end;
end;

procedure TDataAccessComp.SetAsOfDate(const AValue: TisDate);
begin
  if FAsOfDate <> AValue then
  begin
    FAsOfDate := AValue;
    ClearCache([dbtSystem, dbtClient, dbtBureau]);
  end;
end;

procedure TDataAccessComp.LockPR(const AllOK: Boolean);
begin
  FPayrollIsLocked := True;
  if AllOK then
  begin
    SetPayrollLockInTransaction(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger, False);
    CommitNestedTransaction;
  end
  else
    RollbackNestedTransaction;
end;

procedure TDataAccessComp.LockPRSimple;
begin
  FPayrollIsLocked := True;
end;

function TDataAccessComp.PayrollLocked: Boolean;
begin
  Result := FPayrollIsLocked;
end;

procedure TDataAccessComp.UnlockPR;
begin
  StartNestedTransaction([dbtClient]);
  FPayrollIsLocked := False;
  SetPayrollLockInTransaction(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger, True);
end;

procedure TDataAccessComp.UnlockPRSimple;
begin
  FPayrollIsLocked := False;
end;

function TDataAccessComp.GetCheckMaxAmountAndHours: Boolean;
begin
  Result := FCheckMaxAmountAndHours;
end;

procedure TDataAccessComp.SetCheckMaxAmountAndHours(const AValue: Boolean);
begin
  FCheckMaxAmountAndHours := AValue;
end;

function TDataAccessComp.GetRecalcLineBeforePost: Boolean;
begin
  Result := FRecalcLineBeforePost;
end;

procedure TDataAccessComp.SetRecalcLineBeforePost(const AValue: Boolean);
begin
  FRecalcLineBeforePost := AValue;
end;


function TDataAccessComp.GetNewBatchInserted: Boolean;
begin
  Result := FNewBatchInserted;  
end;

procedure TDataAccessComp.SetNewBatchInserted(const AValue: Boolean);
begin
 FNewBatchInserted := AValue;
end;

function TDataAccessComp.GetValidate: Boolean;
begin
  Result := FValidate;
end;

procedure TDataAccessComp.SetValidate(const AValue: Boolean);
begin
  FValidate := AValue;
end;

function TDataAccessComp.GetPayrollIsProcessing: Boolean;
begin
  Result := FPayrollIsProcessing;
end;

procedure TDataAccessComp.SetPayrollIsProcessing(const AValue: Boolean);
begin
  FPayrollIsProcessing := AValue;
end;

function TDataAccessComp.GetImporting: Boolean;
begin
  Result := FImporting;
end;

procedure TDataAccessComp.SetImporting(const AValue: Boolean);
begin
  FImporting := AValue;
end;

function TDataAccessComp.GetCopying: Boolean;
begin
  Result := FCopying;
end;

procedure TDataAccessComp.SetCopying(const AValue: Boolean);
begin
  FCopying := AValue;
end;

function TDataAccessComp.GetNewCheckInserted: Boolean;
begin
  Result := FNewCheckInserted;
end;

procedure TDataAccessComp.SetNewCheckInserted(const AValue: Boolean);
begin
  FNewCheckInserted := AValue;
end;

function TDataAccessComp.GetCheckCalcInProgress: Boolean;
begin
  Result := FCheckCalcInProgress;
end;

procedure TDataAccessComp.SetCheckCalcInProgress(const AValue: Boolean);
begin
  FCheckCalcInProgress := AValue;
end;

function TDataAccessComp.GetTemplateNumber: Integer;
begin
  Result := FTemplateNumber;
end;

procedure TDataAccessComp.SetTemplateNumber(const AValue: Integer);
begin
  FTemplateNumber := AValue;
end;

function TDataAccessComp.GetNextInternalCheckNbr: Integer;
begin
  Result := FNextInternalCheckNbr;
end;

procedure TDataAccessComp.SetNextInternalCheckNbr(const AValue: Integer);
begin
  FNextInternalCheckNbr := AValue;
end;

function TDataAccessComp.GetLastInternalCheckNbr: Integer;
begin
  Result := FLastInternalCheckNbr;
end;

procedure TDataAccessComp.SetLastInternalCheckNbr(const AValue: Integer);
begin
  FLastInternalCheckNbr := AValue;
end;

function TDataAccessComp.GetNextInternalCheckLineNbr: Integer;
begin
  Result := FNextInternalCheckLineNbr;
end;

procedure TDataAccessComp.SetNextInternalCheckLineNbr(const AValue: Integer);
begin
  FNextInternalCheckLineNbr := AValue;
end;

function TDataAccessComp.GetLastInternalCheckLineNbr: Integer;
begin
  Result := FLastInternalCheckLineNbr;
end;

procedure TDataAccessComp.SetLastInternalCheckLineNbr(const AValue: Integer);
begin
  FLastInternalCheckLineNbr := AValue;
end;

function TDataAccessComp.GetVoidingAgencyCheck: Boolean;
begin
  Result := FVoidingAgencyCheck;
end;

procedure TDataAccessComp.SetVoidingAgencyCheck(const AValue: Boolean);
begin
  FVoidingAgencyCheck := AValue;
end;

function TDataAccessComp.GetDeletingVoidCheck: Boolean;
begin
  Result := FDeletingVoidCheck;
end;

procedure TDataAccessComp.SetDeletingVoidCheck(const AValue: Boolean);
begin
  FDeletingVoidCheck := AValue;
end;

function TDataAccessComp.GetTempCommitDisable: Boolean;
begin
  Result := FTempCommitDisable;
end;

procedure TDataAccessComp.SetTempCommitDisable(const AValue: Boolean);
begin
  FTempCommitDisable := AValue;
end;

function TDataAccessComp.GetEESchedEDChanged: string;
begin
  Result := FEESchedEDChanged;
end;

procedure TDataAccessComp.SetEESchedEDChanged(const AValue: string);
begin
  FEESchedEDChanged := AValue;
end;

function TDataAccessComp.GetSkipPrCheckPostCheck: Boolean;
begin
  Result := FSkipPrCheckPostCheck;
end;

procedure TDataAccessComp.SetSkipPrCheckPostCheck(const AValue: Boolean);
begin
  FSkipPrCheckPostCheck := AValue;
end;

function TDataAccessComp.GetCheckACHstatus: Boolean;
begin
  Result := FCheckACHstatus;
end;

procedure TDataAccessComp.SetCheckACHstatus(const AValue: Boolean);
begin
  FCheckACHstatus := AValue;
end;

procedure TDataAccessComp.AddTOAForEE;
begin
  with DM_COMPANY.CO_TIME_OFF_ACCRUAL, DM_EMPLOYEE do
  begin
    First;
    while not Eof do
    begin
      if (FieldValues['AUTO_CREATE_ON_NEW_HIRE'] = GROUP_BOX_YES) and
         not EE_TIME_OFF_ACCRUAL.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([EE['EE_NBR'], FieldValues['CO_TIME_OFF_ACCRUAL_NBR']]), []) and
         (AUTO_CREATE_FOR_STATUSES.IsNull or (Pos(EE.POSITION_STATUS.Value, AUTO_CREATE_FOR_STATUSES.Value) > 0)) then
      begin
        EE_TIME_OFF_ACCRUAL.Append;
        EE_TIME_OFF_ACCRUAL['EE_NBR'] := EE['EE_NBR'];
        EE_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'] := FieldValues['CO_TIME_OFF_ACCRUAL_NBR'];
        EE_TIME_OFF_ACCRUAL['CURRENT_ACCRUED'] := 0;
        EE_TIME_OFF_ACCRUAL['CURRENT_USED'] := 0;
        EE_TIME_OFF_ACCRUAL.Post;
      end;
      Next;
    end;
  end;
end;

procedure TDataAccessComp.AddEdsForEE;
var
  HomeState: String;
begin
  with DM_CLIENT.CL_E_DS, DM_COMPANY, DM_EMPLOYEE do
  begin
    CO_E_D_CODES.First;
    while not CO_E_D_CODES.Eof do
    begin
      if Locate('CL_E_DS_NBR', CO_E_D_CODES.FieldByName('CL_E_DS_NBR').Value, []) then
      begin
        if (FieldValues['SD_AUTO'] = GROUP_BOX_YES) and (FieldValues['SCHEDULED_DEFAULTS'] = GROUP_BOX_YES) and (EE.FieldValues['COMPANY_OR_INDIVIDUAL_NAME'] = GROUP_BOX_INDIVIDUAL) then
        begin
          HomeState := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR;CO_NBR', VarArrayOf([EE_STATES.FieldByName('CO_STATES_NBR').Value, DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'STATE');
          if (FieldByName('E_D_CODE_TYPE').AsString <> 'DZ') and (not EE_SCHEDULED_E_DS.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([EE['EE_NBR'], FieldValues['CL_E_DS_NBR']]), [])) then
          begin

            if (HomeState <> 'NM') and ((FieldByName('E_D_CODE_TYPE').AsString = 'D^') or (FieldByName('E_D_CODE_TYPE').AsString = 'M0')) then
            begin
              CO_E_D_CODES.Next;
              Continue;
            end;

            EE_SCHEDULED_E_DS.Append;
            EE_SCHEDULED_E_DS['EE_NBR'] := EE['EE_NBR'];
            EE_SCHEDULED_E_DS['CL_E_DS_NBR'] := FieldValues['CL_E_DS_NBR'];
            if (FieldByName('E_D_CODE_TYPE').AsString = 'D^') or (FieldByName('E_D_CODE_TYPE').AsString = 'M0') then
              EE_SCHEDULED_E_DS['PLAN_TYPE'] := MONTH_NUMBER_3
            else
              EE_SCHEDULED_E_DS['PLAN_TYPE'] := MONTH_NUMBER_NONE;
            EE_SCHEDULED_E_DS['FREQUENCY'] := FieldValues['SD_FREQUENCY'];
            EE_SCHEDULED_E_DS['EXCLUDE_WEEK_1'] := FieldValues['SD_EXCLUDE_WEEK_1'];
            EE_SCHEDULED_E_DS['EXCLUDE_WEEK_2'] := FieldValues['SD_EXCLUDE_WEEK_2'];
            EE_SCHEDULED_E_DS['EXCLUDE_WEEK_3'] := FieldValues['SD_EXCLUDE_WEEK_3'];
            EE_SCHEDULED_E_DS['EXCLUDE_WEEK_4'] := FieldValues['SD_EXCLUDE_WEEK_4'];
            EE_SCHEDULED_E_DS['EXCLUDE_WEEK_5'] := FieldValues['SD_EXCLUDE_WEEK_5'];

            EE_SCHEDULED_E_DS['AMOUNT'] := FieldValues['SD_AMOUNT'];
            EE_SCHEDULED_E_DS['PERCENTAGE'] := FieldValues['SD_RATE'];
            EE_SCHEDULED_E_DS['CALCULATION_TYPE'] := FieldValues['SD_CALCULATION_METHOD'];
            if not VARISNULL(FieldValues['SD_EFFECTIVE_START_DATE']) then
              EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := FieldValues['SD_EFFECTIVE_START_DATE']
            else
              EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := FormatDateTime('mm/dd/yyyy', now);
            if (FieldByName('E_D_CODE_TYPE').AsString = 'D^') or (FieldByName('E_D_CODE_TYPE').AsString = 'M0') then
              EE_SCHEDULED_E_DS['WHICH_CHECKS'] := GROUP_BOX_LAST
            else
              EE_SCHEDULED_E_DS['WHICH_CHECKS'] := GROUP_BOX_ALL;
            EE_SCHEDULED_E_DS['DEDUCT_WHOLE_CHECK'] := GROUP_BOX_NO;
            EE_SCHEDULED_E_DS['ALWAYS_PAY'] := ALWAYS_PAY_NO;
            EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'] := FieldValues['CL_E_D_GROUPS_NBR'];
            EE_SCHEDULED_E_DS['CL_AGENCY_NBR'] := FieldValues['DEFAULT_CL_AGENCY_NBR'];
            EE_SCHEDULED_E_DS['EXPRESSION'] := FieldValues['SD_EXPRESSION'];
            //////////////////////////////////////////////////////////////////////////
            EE_SCHEDULED_E_DS['WHICH_CHECKS'] := FieldValues['SD_WHICH_CHECKS'];
            EE_SCHEDULED_E_DS['PLAN_TYPE'] := FieldValues['SD_PLAN_TYPE'];
            EE_SCHEDULED_E_DS['PRIORITY_NUMBER'] := FieldValues['SD_PRIORITY_NUMBER'];
            EE_SCHEDULED_E_DS['ALWAYS_PAY'] := FieldValues['SD_ALWAYS_PAY'];
            EE_SCHEDULED_E_DS['MAX_AVG_AMT_CL_E_D_GROUPS_NBR'] := FieldValues['SD_MAX_AVG_AMT_GRP_NBR'];
            EE_SCHEDULED_E_DS['MAX_AVG_HRS_CL_E_D_GROUPS_NBR'] := FieldValues['SD_MAX_AVG_HRS_GRP_NBR'];
            EE_SCHEDULED_E_DS['MAX_AVERAGE_HOURLY_WAGE_RATE'] := FieldValues['SD_MAX_AVERAGE_HOURLY_WAGE_RATE'];
            EE_SCHEDULED_E_DS['THRESHOLD_E_D_GROUPS_NBR'] := FieldValues['SD_THRESHOLD_E_D_GROUPS_NBR'];
            EE_SCHEDULED_E_DS['THRESHOLD_AMOUNT'] := FieldValues['SD_THRESHOLD_AMOUNT'];
            EE_SCHEDULED_E_DS['DEDUCTIONS_TO_ZERO'] := FieldValues['SD_DEDUCTIONS_TO_ZERO'];
            //////////////////////////////////////////////////////////////////////////
            EE_SCHEDULED_E_DS.Post;
          end;
          if (FieldByName('E_D_CODE_TYPE').AsString = 'DZ') then
          begin
            if (HomeState = 'NY') and (not EE_SCHEDULED_E_DS.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([EE['EE_NBR'], FieldValues['CL_E_DS_NBR']]), [])) then
            begin
              EE_SCHEDULED_E_DS.Append;
              EE_SCHEDULED_E_DS['EE_NBR'] := EE['EE_NBR'];
              EE_SCHEDULED_E_DS['CL_E_DS_NBR'] := FieldValues['CL_E_DS_NBR'];
              EE_SCHEDULED_E_DS['PLAN_TYPE'] := MONTH_NUMBER_NONE;
              EE_SCHEDULED_E_DS['FREQUENCY'] := FieldValues['SD_FREQUENCY'];
              EE_SCHEDULED_E_DS['EXCLUDE_WEEK_1'] := FieldValues['SD_EXCLUDE_WEEK_1'];
              EE_SCHEDULED_E_DS['EXCLUDE_WEEK_2'] := FieldValues['SD_EXCLUDE_WEEK_2'];
              EE_SCHEDULED_E_DS['EXCLUDE_WEEK_3'] := FieldValues['SD_EXCLUDE_WEEK_3'];
              EE_SCHEDULED_E_DS['EXCLUDE_WEEK_4'] := FieldValues['SD_EXCLUDE_WEEK_4'];
              EE_SCHEDULED_E_DS['EXCLUDE_WEEK_5'] := FieldValues['SD_EXCLUDE_WEEK_5'];

              EE_SCHEDULED_E_DS['AMOUNT'] := FieldValues['SD_AMOUNT'];
              EE_SCHEDULED_E_DS['PERCENTAGE'] := FieldValues['SD_RATE'];
              EE_SCHEDULED_E_DS['CALCULATION_TYPE'] := FieldValues['SD_CALCULATION_METHOD'];
              if not VARISNULL(FieldValues['SD_EFFECTIVE_START_DATE']) then
                EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := FieldValues['SD_EFFECTIVE_START_DATE']
              else
                EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := FormatDateTime('mm/dd/yyyy', now);
              EE_SCHEDULED_E_DS['WHICH_CHECKS'] := GROUP_BOX_ALL;
              EE_SCHEDULED_E_DS['DEDUCT_WHOLE_CHECK'] := GROUP_BOX_NO;
              EE_SCHEDULED_E_DS['ALWAYS_PAY'] := GROUP_BOX_NO;
              EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'] := FieldValues['CL_E_D_GROUPS_NBR'];
              EE_SCHEDULED_E_DS['CL_AGENCY_NBR'] := FieldValues['DEFAULT_CL_AGENCY_NBR'];
              EE_SCHEDULED_E_DS['EXPRESSION'] := FieldValues['SD_EXPRESSION'];
              //////////////////////////////////////////////////////////////////////////
              EE_SCHEDULED_E_DS['WHICH_CHECKS'] := FieldValues['SD_WHICH_CHECKS'];
              EE_SCHEDULED_E_DS['PLAN_TYPE'] := FieldValues['SD_PLAN_TYPE'];
              EE_SCHEDULED_E_DS['PRIORITY_NUMBER'] := FieldValues['SD_PRIORITY_NUMBER'];
              EE_SCHEDULED_E_DS['ALWAYS_PAY'] := FieldValues['SD_ALWAYS_PAY'];
              EE_SCHEDULED_E_DS['MAX_AVG_AMT_CL_E_D_GROUPS_NBR'] := FieldValues['SD_MAX_AVG_AMT_GRP_NBR'];
              EE_SCHEDULED_E_DS['MAX_AVG_HRS_CL_E_D_GROUPS_NBR'] := FieldValues['SD_MAX_AVG_HRS_GRP_NBR'];
              EE_SCHEDULED_E_DS['MAX_AVERAGE_HOURLY_WAGE_RATE'] := FieldValues['SD_MAX_AVERAGE_HOURLY_WAGE_RATE'];
              EE_SCHEDULED_E_DS['THRESHOLD_E_D_GROUPS_NBR'] := FieldValues['SD_THRESHOLD_E_D_GROUPS_NBR'];
              EE_SCHEDULED_E_DS['THRESHOLD_AMOUNT'] := FieldValues['SD_THRESHOLD_AMOUNT'];
              EE_SCHEDULED_E_DS['DEDUCTIONS_TO_ZERO'] := FieldValues['SD_DEDUCTIONS_TO_ZERO'];
              //////////////////////////////////////////////////////////////////////////
              EE_SCHEDULED_E_DS.Post;
            end;
          end;
        end;
      end;
      CO_E_D_CODES.Next;
    end;
  end;
end;

function TDataAccessComp.GetPrCheckLineLocations: IevDataset;
begin
  if not Assigned(FPrCheckLineLocations) then
  begin
    FPrCheckLineLocations := TevDataset.Create;
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('PR_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('CO_LOCAL_TAX_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('SY_LOCALS_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('CALCULATE', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('ISRES', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('DBDT_ATTACHED', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('WORK_ADDRESS_OVR', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('EE_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('PR_CHECK_LINES_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('EE_LOCALS_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('BLOCKED', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('ISLOSER', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('EXCLUDED', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('NONRES_EE_LOCALS_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('DIVISION_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('BRANCH_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('DEPARTMENT_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('TEAM_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('JOBS_NBR', ftInteger);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('LOCATION_LEVEL', ftString, 1);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('CO_LOCATIONS_NBR', ftInteger);    
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('AMOUNT', ftFloat);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('RATE', ftFloat);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('LOCAL_GROSS_WAGES', ftFloat);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('LOCAL_TAXABLE_WAGES', ftFloat);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('LOCAL_TAX', ftFloat);
    FPrCheckLineLocations.VCLDataset.FieldDefs.Add('OVERRIDE_AMOUNT', ftFloat);
    FPrCheckLineLocations.VCLDataset.Open;
    FPrCheckLineLocations.IndexFieldNames := 'PR_CHECK_NBR;CO_LOCAL_TAX_NBR';
  end;
  Result := FPrCheckLineLocations;
end;

function TDataAccessComp.GetPrCheckLineLocationsM: IevDataset;
begin
  if not Assigned(FPrCheckLineLocationsM) then
  begin
    FPrCheckLineLocationsM := TevDataset.Create;
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('PR_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('CO_LOCAL_TAX_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('SY_LOCALS_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('CALCULATE', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('ISRES', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('DBDT_ATTACHED', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('WORK_ADDRESS_OVR', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('EE_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('PR_CHECK_LINES_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('EE_LOCALS_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('BLOCKED', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('ISLOSER', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('EXCLUDED', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('NONRES_EE_LOCALS_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('DIVISION_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('BRANCH_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('DEPARTMENT_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('TEAM_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('JOBS_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('LOCATION_LEVEL', ftString, 1);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('CO_LOCATIONS_NBR', ftInteger);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('AMOUNT', ftFloat);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('RATE', ftFloat);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('LOCAL_GROSS_WAGES', ftFloat);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('LOCAL_TAXABLE_WAGES', ftFloat);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('LOCAL_TAX', ftFloat);
    FPrCheckLineLocationsM.VCLDataset.FieldDefs.Add('OVERRIDE_AMOUNT', ftFloat);
    FPrCheckLineLocationsM.VCLDataset.Open;
    FPrCheckLineLocationsM.IndexFieldNames := 'PR_CHECK_NBR;CO_LOCAL_TAX_NBR';
  end;
  Result := FPrCheckLineLocationsM;
end;

function TDataAccessComp.CommitNestedTransaction: IisValueMap;
var
  i: Integer;
  DS: TevClientDataSet;
begin
  Result := ctx_DBAccess.CommitTransaction;

  for i := 0 to FPostingDataSets.Count - 1 do
  begin
    DS := TObject(FPostingDataSets[i]) as TevClientDataSet;
    DS.DoAfterChangesApply;
  end;

  if not ctx_DBAccess.InTransaction then
  begin
    if Assigned(Result) then
      ApplyKeyMap(Result);
    ClearDSList;      
  end;
end;

procedure TDataAccessComp.RollbackNestedTransaction;
begin
  ctx_DBAccess.RollbackTransaction;
  if not ctx_DBAccess.InTransaction then
    ClearDSList;
end;

procedure TDataAccessComp.StartNestedTransaction(
  const ADBTypes: TevDBTypes; const ATransactionName: String);
begin
  ctx_DBAccess.StartTransaction(ADBTypes, ATransactionName);
end;

procedure TDataAccessComp.ApplyKeyMap(const AKeyMap: IisValueMap);
var
  i: Integer;
  DS: TevClientDataSet;
begin
  // Patch key values for datasets
  for i := 0  to FChangingDataSets.Count - 1 do
  begin
    DS := TObject(FChangingDataSets[i]) as TevClientDataSet;
    if DS.Active then
      DS.Reconcile(AKeyMap);
  end;
end;

procedure TDataAccessComp.OnDestroyDS(Sender: TObject);
var
  i: Integer;
begin
  i := FChangingDataSets.IndexOf(Sender);
  if i <> -1 then
  begin
    (TObject(FChangingDataSets[i]) as TevClientDataSet).OnDestroy := nil;
    FChangingDataSets.Delete(i);
  end;
end;

procedure TDataAccessComp.ClearDSList;
var
  i: Integer;
  DS: TevClientDataSet;
begin
  for i := 0  to FChangingDataSets.Count - 1 do
  begin
    DS := TObject(FChangingDataSets[i]) as TevClientDataSet;
    DS.OnDestroy := nil;
  end;
  FChangingDataSets.Clear;
end;

procedure TDataAccessComp.PostDataSets(const ADatasets: array of TevClientDataSet; const AResolveCircularReferences: Boolean = False);
var
  i: Integer;
  lClNbr: Integer;
  DBTypes: TevDBTypes;
  LockFlag: IevGlobalFlagInfo;
begin
// Make sure that payroll datasets that are committed do not belong to a company that has a payroll being processed
  if not GetPayrollIsProcessing then
    for i := Low(ADatasets) to High(ADatasets) do
      if Copy(ADatasets[i].Name, 1, 2) = 'PR' then
        if Assigned(DM_PAYROLL.PR) and DM_PAYROLL.PR.Active and (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> '')then
        begin
          if not (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT]) then
          begin
            LockFlag := mb_GlobalFlagsManager.GetLockedFlag(GF_CO_PAYROLL_OPERATION, IntToStr(ClientID) + '_' + DM_PAYROLL.PR.CO_NBR.AsString);
            if Assigned(LockFlag) then
              raise ELockedResource.CreateHelp('A payroll for this company is being processed by ' + LockFlag.UserName +
                '. You can not make payroll changes.', IDH_LockedResource);
          end;
          Break;
        end;

  if Length(ADatasets) > 0 then
  begin
    ctx_StartWait;

    for i := Low(ADatasets) to High(ADatasets) do
      ADatasets[i].DisableControls;

    try
      lClNbr := 0;
      DBTypes := [];

      for i := Low(ADatasets) to High(ADatasets) do
        with ADatasets[i] do
          if Active and (ChangeCount > 0) then
          begin
            if ClientID > 0 then
              if lClNbr = 0 then
                lClNbr := ClientID
              else
                Assert(lClNbr = ClientID, 'Can''t save data for multiple clients!');

            Include(DBTypes, GetDBTypeByTableName(TableName));
          end;

     if DBTypes = [] then  // no changes
       Exit;

      StartNestedTransaction(DBTypes);
      try
        for i := Low(ADatasets) to High(ADatasets) do
          CollectChanges(ADatasets[i]);

        if AResolveCircularReferences then
          ResolveCircularReferences;

        PostCollectedChanges;

        CommitNestedTransaction;
      except
        RollbackNestedTransaction;
        raise;
      end;

    finally
      FPostingDataSets.Clear;
      FDataSetChanges.Clear;

      for i := Low(ADatasets) to High(ADatasets) do
        ADatasets[i].EnableControls;

      ctx_EndWait;
    end;
  end;
end;

procedure TDataAccessComp.OpenEEDataSetForCompany(
  aDataSet: TevClientDataSet; CoNbr: Integer; const Condition: string);
var
  sCondition, sQueryName: string;
begin
  sCondition := 'CO_NBR = '+ IntToStr(CoNbr);
  if Trim(Condition) <> '' then
    sCondition := sCondition + ' AND (' + Condition + ')';
  if aDataSet.Active and (aDataSet.ClientID = ClientID)
  and (aDataSet.OpenCondition = sCondition) then
    Exit;
  aDataSet.ClientID := ClientID;

  if Condition = '' then
    sQueryName := 'GenericSelect2HistNbr'
  else
    sQueryName := 'GenericSelect2HistNbrWithCondition';

  with TExecDSWrapper.Create(sQueryName) do
  begin
    SetMacro('Columns', 'T1.*');
    SetMacro('TABLE1', aDataSet.Name);
    SetMacro('TABLE2', 'EE');
    SetMacro('NBRFIELD', 'CO');
    SetMacro('JOINFIELD', 'EE');
    SetParam('RecordNbr', CoNbr);
    SetParam('StatusDate', GetAsOfDate);
    SetMacro('CONDITION', Condition);
    GetCustomData(aDataSet, AsVariant);
  end;

  aDataSet.RetrieveCondition := '';
  aDataSet.SetOpenCondition(sCondition);
end;

procedure TDataAccessComp.CheckCompanyLock(const CoNbr: Integer;
  dAsOfDate: TDateTime; const DoNotReOpen: Boolean;
  CheckForTaxPmtsFlag: boolean);
var
  LockDate: TDateTime;
  LockTaxPmtsFlag:boolean;
  TermCode:string;
  CoName:string;
begin
  if dAsOfDate = 0 then
    if GetAsOfDate <> 0 then
      dAsOfDate := GetAsOfDate
    else
      dAsOfDate := SysDate;

  if CoNbr = 0 then
  begin
    Assert(DM_COMPANY.CO.Active);
    LockDate := ConvertNull(DM_COMPANY.CO['LOCK_DATE'], 0);
    CoName := ConvertNull(DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'],'');
    LockTaxPmtsFlag := ConvertNull(DM_COMPANY.CO['QTR_LOCK_FOR_TAX_PMTS'], 'Y')='Y';
    TermCode := ConvertNull(DM_COMPANY.CO['TERMINATION_CODE'], '');
  end

  else
  begin
    if DM_COMPANY.CO.Active
    and (DM_COMPANY.CO.ClientID = ClientID)
    and (DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'CO_NBR') = CoNbr) then
    begin
      LockDate := ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'LOCK_DATE'), 0);
      LockTaxPmtsFlag := ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr,'QTR_LOCK_FOR_TAX_PMTS'), 'Y')='Y';
      TermCode := ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'TERMINATION_CODE'), '');
      CoName := ConvertNull(DM_COMPANY.CO.Lookup('CO_NBR', CoNbr, 'CUSTOM_COMPANY_NUMBER'), 0);
    end
    else
    begin
      if DoNotReOpen then
      begin
        DM_COMPANY.CO.Activate;
        Assert(DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []));
      end
      else
        DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(CoNbr));
      Assert(DM_COMPANY.CO.RecordCount > 0);
      LockDate := ConvertNull(DM_COMPANY.CO['LOCK_DATE'], 0);
      LockTaxPmtsFlag := ConvertNull(DM_COMPANY.CO['QTR_LOCK_FOR_TAX_PMTS'], 'Y')='Y';
      TermCode := ConvertNull(DM_COMPANY.CO['TERMINATION_CODE'], '');
      CoName := DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'];
    end
  end;

  if TermCode = 'T' then
    exit;

  if (LockDate <> 0) and (LockDate >= dAsOfDate)  then
    if (not CheckForTaxPmtsFlag ) or  (CheckForTaxPmtsFlag  and LockTaxPmtsFlag = true) then
      raise EPeriodIsLocked.CreateHelp('The company "'+CoName+'" is locked up to ' + DateToStr(LockDate) + '. You can not make this change.', IDH_PeriodIsLocked);
end;

procedure TDataAccessComp.OpenDataSets(const ADatasets: array of TevClientDataSet);

  function CheckIfChildDataSet(const ds: TEvClientDataSet): Boolean;
  begin
    Result := (ds.RetrieveCondition <> '') and (ds.RetrieveCondition[1] = '@');
  end;

  function CheckDSIfPresented(const DataSet: TevClientDataSet; const aDS: TArrayDS): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := Low(aDS) to High(aDS) do
      if aDS[i] = DataSet then
      begin
        Result := True;
        Break;
      end;
  end;

  function GetUpLinkDataSet(const ds: TEvClientDataSet): TddTable;
  const
    Delim = '@';
  var
    s: string;
  begin
    s := ds.RetrieveCondition;
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    s := FetchToken(s, Delim);
    Result := GetCachedDataSet(StrToIntDef(s, 0));
    Assert(Assigned(Result));
  end;

  function GetUpLinkFieldName(const ds: TEvClientDataSet): string;
  const
    Delim = '@';
  var
    s: string;
  begin
    s := ds.RetrieveCondition;
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    Result := FetchToken(s, Delim);
  end;

  function GetUpLinkCondition(const ds: TEvClientDataSet): string;
  const
    Delim = '@';
  var
    s: string;
  begin
    s := ds.RetrieveCondition;
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    FetchToken(s, Delim);
    Result := FetchToken(s, Delim);
  end;

  function GetBasicQueryText(DS: TevClientDataset): string;
  var
    UplinkDataSet: TevClientDataSet;
    UplinkFieldName, UpLinkCondition: string;
  begin
    Result := '';
    if not Assigned(DS) then
      Exit;

      if CheckIfChildDataSet(DS) then
      begin
        UplinkDataSet := GetUpLinkDataSet(DS);
        UplinkFieldName := GetUpLinkFieldName(DS);
        UpLinkCondition := GetUpLinkCondition(DS);
        if not UplinkDataSet.Active then
          Result := 'SELECT=' + AnsiQuotedStr(UpLinkCondition, '"')+
            ',UPFIELD=' + UplinkFieldName+ ',UPPROVIDER='+ UplinkDataSet.ProviderName
        else
        if UpLinkCondition = '' then
          Result := 'SELECT=' + AnsiQuotedStr(UplinkFieldCondition(UplinkFieldName, UplinkDataSet), '"')
        else
          Result := 'SELECT=' + AnsiQuotedStr(UplinkFieldCondition(UplinkFieldName, UplinkDataSet)+ ' and ('+ UpLinkCondition+ ')', '"');
      end
      else
      begin
        if UpperCase(DS.RetrieveCondition) = 'ALL' then
          Result := 'SELECT='
        else
          Result := 'SELECT=' + AnsiQuotedStr(DS.RetrieveCondition, '"');
      end;

      if DS.LoadValuesFieldList.Count > 0 then
        Result := Result+ ','+ 'LOADVALS='+ AnsiQuotedStr(DS.LoadValuesFieldList.CommaText, '"');

      if DS.OpenFieldList <> '' then
        Result := Result+ ','+ 'FIELDLIST='+ AnsiQuotedStr(DS.OpenFieldList, '"');
  end;

var
  i: Integer;
  Count: Integer;
  DS, aDS: TArrayDS;
  ActiveList: array of Boolean;
  p: TGetDataParams;
  r: TGetDataResults;
  rs: IisStream;
  lDS: TEvClientDataSet;
  Conditions :IisStringList;
begin
  if Context.UserAccount = nil then Exit;

  DS := MakeArrayDS(ADatasets);

  if Length(DS) > 0 then
  begin
    for i := Low(DS) to High(DS) do
    with DS[i] do
    begin
      if not Active and
         (ClientID = 0) and
         (GetddDatabaseByTableName(TableName) = TDM_CLIENT) then
        ClientID := Self.ClientID;
    end;

    aDS := MakeArrayDS(DS);
    for i := High(aDS) downto Low(aDS) do
    begin
      if aDS[i].OpenWithLookups then
        aDS[i].AddLookupDS(DS);
      if CheckIfChildDataSet(aDS[i]) then
      begin
        lDS := GetUpLinkDataSet(aDS[i]);
        if not lDS.Active then
        begin
          Assert(CheckDSIfPresented(lDS, DS));
          lDS.LoadValuesFieldList.Append(GetUpLinkFieldName(aDS[i]));
        end;
      end;
    end;

    Count := 0;
    for i := Low(DS) to High(DS) do
      if not DS[i].Active then
        Inc(Count);
    if Count = 0 then
      Exit;

    SetLength(ActiveList, Length(DS));
    p := TGetDataParams.Create;
    ctx_StartWait;
    try
      for i := Low(DS) to High(DS) do
      begin
        with DS[i] do
        begin
          if not Active and
             (ClientID = 0) and
             (GetddDatabaseByTableName(TableName) = TDM_CLIENT) then
            ClientID := Self.ClientID;
        end;

        ActiveList[i] := DS[i].Active;
        if not DS[i].Active then
        begin
          DS[i].DisableControls;

          if not Assigned(DS[i].CustomDataRequest) then
          begin
            p.Write(DS[i].ProviderName, DS[i].ClientID, TExecDSWrapper.Create(GetBasicQueryText(DS[i]), GetDataSetAsOfDate(DS[i]), DS[i].BlobsLoadMode <> blmPreload));
            DS[i].LoadValuesFieldList.Clear;
          end
          else
          begin
            p.Write(DS[i].ProviderName, DS[i].ClientID, DS[i].CustomDataRequest);
            DS[i].CustomDataRequest := nil;
          end;
        end;
      end;

      if p.WriteCounter > 0 then
      begin
        rs := ctx_DBAccess.GetDataSets(p);
        r := TGetDataResults.Create(rs);
        Conditions := TisStringList.Create;
        try
          // PrepareLookups may change RetrieveCondition which will effect OpenCondition
          for i := Low(DS) to High(DS) do
            Conditions.Add(DS[i].RetrieveCondition);

          for i := Low(DS) to High(DS) do
            if not ActiveList[i] then
              r.Read(DS[i], False, GetDataSetAsOfDate(DS[i]));

        finally
          r.Free;
          // Restore true OpenCondition
          for i := Low(DS) to High(DS) do
            if not ActiveList[i] then
              DS[i].SetOpenCondition(Conditions.Strings[i]);
        end;
      end;
    finally
      ctx_EndWait;
      p.Free;
      for i := Low(DS) to High(DS) do
        if not ActiveList[i] then
          DS[i].EnableControls;
    end;
  end;
end;


procedure TDataAccessComp.CancelDataSets(const DS: array of TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    if DS[i].Active then
      DS[i].CancelUpdates;
end;

procedure TDataAccessComp.Activate(const DS: array of TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    DS[i].Activate;
end;

procedure TDataAccessComp.GetCustomProvData(const cd, cv: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
var
  p: TGetDataParams;
  r: TGetDataResults;
begin
  p := TGetDataParams.Create;
  ctx_StartWait;
  try
    p.Write(cv.ProviderName, ClientID, sQuery);
    r := TGetDataResults.Create(ctx_DBAccess.GetDatasets(p));
    try
      r.Read(cd, True, sQuery.Params.TryGetValue('StatusDate', 0));
    finally
      r.Free;
    end;
  finally
    p.Free;
    ctx_EndWait;
  end;
end;

procedure TDataAccessComp.GetCustomData(const cd: TEvBasicClientDataSet;
  sQuery: IExecDSWrapper);
begin
  GetCustomProvData(cd, CUSTOM_VIEW, sQuery);
end;

procedure TDataAccessComp.GetSbCustomData(const cd: TEvBasicClientDataSet;
  sQuery: IExecDSWrapper);
begin
  GetCustomProvData(cd, SB_CUSTOM_VIEW, sQuery);
end;

procedure TDataAccessComp.GetSyCustomData(const cd: TEvBasicClientDataSet;
  sQuery: IExecDSWrapper);
begin
  GetCustomProvData(cd, SY_CUSTOM_VIEW, sQuery);
end;

procedure TDataAccessComp.GetTmpCustomData(
  const cd: TEvBasicClientDataSet; sQuery: IExecDSWrapper);
begin
  GetCustomProvData(cd, TEMP_CUSTOM_VIEW, sQuery);
end;

function TDataAccessComp.CUSTOM_VIEW: TevClientDataSet;
begin
  Result := GetHolder.FindComponent('CL_CUSTOM_VIEW') as TevClientDataSet;

  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(GetHolder);
    Result.Name := 'CL_CUSTOM_VIEW';
    Result.ProviderName := 'CL_CUSTOM_PROV';
  end;

  Result.ClientID := ClientID;
end;

function TDataAccessComp.HISTORY_VIEW: TevClientDataSet;
begin
  Result := GetHolder.FindComponent('HISTORY_VIEW') as TevClientDataSet;

  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(GetHolder);
    Result.Name := 'HISTORY_VIEW';
    Result.ProviderName := 'HISTORY_VIEW';
    Result.ClientID := -1;
  end;
end;

function TDataAccessComp.SB_CUSTOM_VIEW: TevClientDataSet;
begin
  Result := GetHolder.FindComponent('SB_CUSTOM_VIEW') as TevClientDataSet;

  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(GetHolder);
    Result.Name := 'SB_CUSTOM_VIEW';
    Result.ProviderName := 'SB_CUSTOM_PROV';
    Result.ClientID := -1;
  end;
end;

function TDataAccessComp.SY_CUSTOM_VIEW: TevClientDataSet;
begin
  Result := GetHolder.FindComponent('SY_CUSTOM_VIEW') as TevClientDataSet;

  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(GetHolder);
    Result.Name := 'SY_CUSTOM_VIEW';
    Result.ProviderName := 'SY_CUSTOM_PROV';
    Result.ClientID := -1;
  end;
end;

function TDataAccessComp.TEMP_CUSTOM_VIEW: TevClientDataSet;
begin
  Result := GetHolder.FindComponent('TEMP_CUSTOM_VIEW') as TevClientDataSet;

  if not Assigned(Result) then
  begin
    Result := TevClientDataSet.Create(GetHolder);
    Result.Name := 'TEMP_CUSTOM_VIEW';
    Result.ProviderName := 'TEMP_CUSTOM_PROV';
    Result.ClientID := -1;
  end;
end;

function TDataAccessComp.GetCustomViewByddDatabase(
  const ddDatabaseClass: TddDatabaseClass): TevClientDataSet;
begin
  Result := nil;
  case ddDatabaseClass.GetDBType of
    dbtClient: Result := CUSTOM_VIEW;
    dbtTemporary: Result := TEMP_CUSTOM_VIEW;
    dbtSystem: Result := SY_CUSTOM_VIEW;
    dbtBureau: Result := SB_CUSTOM_VIEW;
  else
    Assert(False, 'Class ' + ddDatabaseClass.ClassName + ' does not exist');
  end;
end;

function TDataAccessComp.DelayedCommit: Boolean;
begin
  Result := GetTempCommitDisable or
            not IsStandalone and
            ((Mainboard.TCPClient.CompressionLevel > clNone) or
            (Mainboard.TCPClient.EncryptionType > etNone));
end;

procedure TDataAccessComp.LocateCheckReport(const CheckForm: String; const RegularCheck: Boolean);
begin
  Assert(CheckForm <> '');
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_TYPE=''' + SYSTEM_REPORT_TYPE_CHECK + '''');
  if RegularCheck then
  begin
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('SY_REPORT_WRITER_REPORTS_NBR', RegularCheckReportNbrs[CheckForm[1]], []) then
      raise EMissingReport.CreateHelp('"'+ ReturnDescription(CheckForm_ComboChoices, CheckForm[1])+ '" report does not exist in the database!', IDH_MissingReport);
  end
  else
  begin
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('SY_REPORT_WRITER_REPORTS_NBR', MiscCheckReportNbrs[CheckForm[1]], []) then
      raise EMissingReport.CreateHelp('"Misc '+ ReturnDescription(CheckForm_ComboChoices, CheckForm[1])+ '" report does not exist in the database!', IDH_MissingReport);
  end;
end;

procedure TDataAccessComp.FillSBCheckList(aRegularChecks, aMiscChecks: TStrings);
var
  Report_Ancestor: string;
begin
  // list of checks created in Check designer and stored on the SB level

  if Assigned(aRegularChecks) or Assigned(aMiscChecks) then
  begin
    if SB_CUSTOM_VIEW.Active then
      SB_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', 'SB_REPORT_WRITER_REPORTS_NBR, report_description');
      SetMacro('TableName', 'SB_REPORT_WRITER_REPORTS');
      SetMacro('Condition', 'report_type = ''' + SYSTEM_REPORT_TYPE_CHECK + '''');
      SB_CUSTOM_VIEW.DataRequest(AsVariant);
      SB_CUSTOM_VIEW.Open;
    end;
    SB_CUSTOM_VIEW.First;
    while not SB_CUSTOM_VIEW.Eof do
    begin
      Report_Ancestor := ctx_RWLocalEngine.GetRepAncestor(
                    SB_CUSTOM_VIEW.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsInteger,
                    CH_DATABASE_SERVICE_BUREAU);

      if Assigned(aRegularChecks) then
      begin
        if Report_Ancestor = 'TrmlEeCheckReport' then
          aRegularChecks.Append(SB_CUSTOM_VIEW.FieldByName('REPORT_DESCRIPTION').AsString +
            aRegularChecks.NameValueSeparator +
            CHECK_FORM_BUREAU_DESIGNER_CHECK + SB_CUSTOM_VIEW.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsString)
        else if Report_Ancestor = 'TrwlCustomPrCheck' then
          aRegularChecks.Append(SB_CUSTOM_VIEW.FieldByName('REPORT_DESCRIPTION').AsString +
            aRegularChecks.NameValueSeparator +
            CHECK_FORM_BUREAU_NEW_CHECK + SB_CUSTOM_VIEW.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsString);
      end;
      if Assigned(aMiscChecks) then
      begin
        if Report_Ancestor = 'TrmlMiscCheckReport' then
          aMiscChecks.Append( SB_CUSTOM_VIEW.FieldByName('REPORT_DESCRIPTION').AsString +
            aMiscChecks.NameValueSeparator +
            CHECK_FORM_BUREAU_DESIGNER_CHECK + SB_CUSTOM_VIEW.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsString)
        else if Report_Ancestor = 'TrwlCustomMiscCheck' then
          aMiscChecks.Append( SB_CUSTOM_VIEW.FieldByName('REPORT_DESCRIPTION').AsString +
            aMiscChecks.NameValueSeparator +
            CHECK_FORM_BUREAU_NEW_CHECK + SB_CUSTOM_VIEW.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsString);
      end;
      SB_CUSTOM_VIEW.Next;
    end;
    SB_CUSTOM_VIEW.Close;
  end;
end;


function TDataAccessComp.CheckSbTaxPaymentstatus(const SbTaxPaymentNbr: integer): Boolean;
begin
  Result := False;
  DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Close;
  DM_SERVICE_BUREAU.SB_TAX_PAYMENT.DataRequired('SB_TAX_PAYMENT_NBR = ' + IntToStr(SbTaxPaymentNbr));
  if DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Locate('SB_TAX_PAYMENT_NBR', SbTaxPaymentNbr, []) then
   if DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS').AsString <> SB_TAX_PAYMENT_ROLLBACK then
        Result := True;
end;

function TDataAccessComp.TaxPaymentRollback(const SbTaxPaymentNbr, i: Integer): Boolean;
var
  dTaxDeposit,
  dCoTaxDeposit,
  dTaxPaymentAch,
  dPr,
  dPrMiscCheck,
  dCoBankAccRegDetails : TevClientDataSet;

const
  woMessage1 = 'Rolling back. Please wait...';
var
  j, tmpClientId : Integer;
  s : String;
  CLList : TStringList;
  sTaxDepositFilter : String;
  bSbClient : Boolean;
  bRollbacked : Boolean;
begin
   bRollbacked := false;
   tmpClientId := ctx_DataAccess.ClientID;

   DM_SERVICE_BUREAU.SB.DataRequired('ALL');

   dTaxDeposit := TevClientDataSet.Create(nil);
   dCoTaxDeposit := TevClientDataSet.Create(nil);
   dCoTaxDeposit.ProviderName := DM_COMPANY.CO_TAX_DEPOSITS.ProviderName;
   dTaxPaymentAch := TevClientDataSet.Create(nil);
   dTaxPaymentAch.ProviderName := DM_COMPANY.CO_TAX_PAYMENT_ACH.ProviderName;
   dPr := TevClientDataSet.Create(nil);
   dPr.ProviderName := DM_PAYROLL.PR.ProviderName;
   dPrMiscCheck := TevClientDataSet.Create(nil);
   dPrMiscCheck.ProviderName := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.ProviderName;
   dCoBankAccRegDetails := TevClientDataSet.Create(nil);
   dCoBankAccRegDetails.ProviderName := DM_COMPANY.CO_BANK_ACC_REG_DETAILS.ProviderName;

   try
     s := 'select CL_NBR, CO_TAX_DEPOSITS_NBR from TMP_CO_TAX_DEPOSITS where SB_TAX_PAYMENT_NBR = ' + IntToStr(SbTaxPaymentNbr);
     with TExecDSWrapper.Create(s) do
        GetTmpCustomData(dTaxDeposit, AsVariant);

     dTaxDeposit.IndexFieldNames := 'CL_NBR;CO_TAX_DEPOSITS_NBR';

     CLList := dTaxDeposit.GetFieldValueList('CL_NBR',True);
     for j := 0 to CLList.Count - 1 do
     begin
       if StrToInt(CLList[j]) > 0 then
       begin
          ctx_UpdateWait(woMessage1, i);
          OpenClient(StrToInt(CLList[j]));
          StartNestedTransaction([dbtClient]);

          try
            bSbClient := false;
            if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger = StrToInt(CLList[j]) then
               bSbClient := True;

            dTaxDeposit.Filter := 'CL_NBR = ' + CLList[j];
            dTaxDeposit.Filtered := True;

            sTaxDepositFilter := GetFieldFilter(dTaxDeposit, 'CO_TAX_DEPOSITS_NBR');

            dTaxDeposit.MergeChangeLog;
            with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
            begin
              SetMacro('TABLENAME', 'CO_TAX_DEPOSITS');
              SetMacro('COLUMNS', 'DISTINCT CO_TAX_DEPOSITS_NBR');
              SetMacro('CONDITION', sTaxDepositFilter);
              GetCustomData(dCoTaxDeposit, AsVariant);
            end;

            with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
            begin
              SetMacro('TABLENAME', 'CO_TAX_DEPOSITS');
              SetMacro('COLUMNS', 'DISTINCT PR_NBR');
              SetMacro('CONDITION', sTaxDepositFilter + ' and PR_NBR is not NULL');
              GetCustomData(dPr, AsVariant);
            end;
            with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
            begin
              SetMacro('TABLENAME', 'CO_TAX_PAYMENT_ACH');
              SetMacro('COLUMNS', 'DISTINCT CO_TAX_PAYMENT_ACH_NBR');
              SetMacro('CONDITION', sTaxDepositFilter + ' and CO_TAX_DEPOSITS_NBR is not NULL');
              GetCustomData(dTaxPaymentAch, AsVariant);
            end;
            with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
            begin
              SetMacro('TABLENAME', 'PR_MISCELLANEOUS_CHECKS');
              SetMacro('COLUMNS', 'DISTINCT PR_MISCELLANEOUS_CHECKS_NBR');
              SetMacro('CONDITION', sTaxDepositFilter + ' and CO_TAX_DEPOSITS_NBR is not NULL');
              GetCustomData(dPrMiscCheck, AsVariant);
            end;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('('+ GetFieldFilter(dTaxPaymentAch, 'CO_TAX_PAYMENT_ACH_NBR')+ ') or ('+
                   GetFieldFilter(dPrMiscCheck, 'PR_MISCELLANEOUS_CHECKS_NBR')+ ') or ('+
                   sTaxDepositFilter + ')');

            if bSbClient then
            begin
              with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
              begin
                SetMacro('TABLENAME', 'CO_BANK_ACC_REG_DETAILS');
                SetMacro('COLUMNS', 'DISTINCT CO_BANK_ACC_REG_DETAILS_NBR');
                SetMacro('CONDITION', GetFieldFilter(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, 'CO_BANK_ACCOUNT_REGISTER_NBR'));
                GetCustomData(dCoBankAccRegDetails, AsVariant);
              end;
              while dCoBankAccRegDetails.RecordCount > 0 do
                dCoBankAccRegDetails.Delete;
            end;

            while DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.RecordCount > 0 do
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Delete;
            while dTaxPaymentAch.RecordCount > 0 do
              dTaxPaymentAch.Delete;
            dPr.First;
            while not dPr.Eof do
            begin
              SetPayrollLockInTransaction(dPr['PR_NBR'], True);
              dPr.Next;
            end;
            while dPr.RecordCount > 0 do
              dPr.Delete;
            while dPrMiscCheck.RecordCount > 0 do
              dPrMiscCheck.Delete;
            while dCoTaxDeposit.RecordCount > 0 do
              dCoTaxDeposit.Delete;

            UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_FED_TAX_LIABILITIES, sTaxDepositFilter);
            UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_STATE_TAX_LIABILITIES, sTaxDepositFilter);
            UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_SUI_LIABILITIES, sTaxDepositFilter);
            UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, sTaxDepositFilter);

            PostDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES]);
            PostDataSets([DM_COMPANY.CO_STATE_TAX_LIABILITIES]);
            PostDataSets([DM_COMPANY.CO_SUI_LIABILITIES]);
            PostDataSets([DM_COMPANY.CO_LOCAL_TAX_LIABILITIES]);
            PostDataSets([dCoBankAccRegDetails]);
            PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
            PostDataSets([dTaxPaymentAch]);
            PostDataSets([dPrMiscCheck]);
            PostDataSets([dCoTaxDeposit]);
            PostDataSets([dPr]);

            CommitNestedTransaction;
          except
            on E: Exception do
            begin
              RollbackNestedTransaction;
              bRollbacked := True;
              E.Message := 'CL#'+ IntToStr(ClientID)+ ' failed with error: '+ E.Message;
              raise;
            end;
          end;
       end;
     end;
   finally
     Result := not bRollbacked;
     if Result then
     begin
       DM_SERVICE_BUREAU.SB_TAX_PAYMENT.DataRequired('SB_TAX_PAYMENT_NBR = ' + IntToStr(SbTaxPaymentNbr));
       if DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('SB_TAX_PAYMENT_NBR').AsInteger = SbTaxPaymentNbr then
       begin
         DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Edit;
         DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS').AsString := SB_TAX_PAYMENT_ROLLBACK;
         DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS_DATE').AsDateTime := Now;
         DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Post;
         PostDataSets([DM_SERVICE_BUREAU.SB_TAX_PAYMENT]);
       end;
     end;
     dTaxDeposit.Free;
     dCoTaxDeposit.Free;
     dPrMiscCheck.Free;
     dPr.Free;
     dTaxPaymentAch.Free;
     dCoBankAccRegDetails.Free;
     OpenClient(tmpClientId);
   end;
end;

procedure TDataAccessComp.UnlinkLiabilitiesFromDeposit(const d: TEvClientDataSet; const AFilter: string);
var
  fStatus : String;
  ld: TEvClientDataSet;
begin
    ld := TevClientDataSet.Create(nil);
    try
     ld.ProviderName := d.ProviderName;
     ld.IndexFieldNames := d.Name+ '_NBR';

     with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
     begin
        SetMacro('TABLENAME', d.Name);
        SetMacro('COLUMNS', '*');
        SetMacro('CONDITION', AFilter);
        GetCustomData(ld, AsVariant);
     end;
     if ld.RecordCount > 0 then
     begin
        ld.First;
        while not ld.Eof do
        begin
          ld.Edit;
          ld.FieldByName('CO_TAX_DEPOSITS_NBR').Clear;

          fStatus := ld.FieldByName('PREV_STATUS').AsString;

          if (((ld.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_PENDING) or
                (ld.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED)) and
                (ld.FieldByName('IMPOUNDED').AsString = GROUP_BOX_YES) ) then
                  fStatus := TAX_DEPOSIT_STATUS_IMPOUNDED;

          if ld.FieldByName('STATUS').AsString = TAX_DEPOSIT_STATUS_NSF then
                fStatus := TAX_DEPOSIT_STATUS_NSF;

          ld.FieldByName('STATUS').AsString := fStatus;
          ld.FieldByName('PREV_STATUS').AsString := TAX_DEPOSIT_STATUS_DEFAULT_PREV_STATUS;
          ld.Post;
          ld.Next;
        end;
        PostDataSets(MakeArrayDS([ld]));
     end;
    finally
      ld.Free;
    end;
end;


function TDataAccessComp.GetFieldFilter(const d: TevClientDataSet;
  const FieldName, prefix: string): string;
var
  pList: TStringList;
begin
    pList := d.GetFieldValueList(FieldName,True);
    try
     result :=  BuildINsqlStatement(prefix + FieldName, pList.CommaText);
    finally
      pList.Free;
      if  result = '' then
       result := prefix + FieldName + ' in ( -1 )';
    end;
end;

function TDataAccessComp.GetClCoReadOnlyRight: Boolean;
begin
  Result := False;
  if GetClReadOnlyRight then
     Result := True
  else
  if GetCoReadOnlyRight then
     Result := True;
end;

function TDataAccessComp.GetClReadOnlyRight: Boolean;
var
  Q :IEvQuery;
  V: IisNamedValue;
begin
  V := FCachedValues.FindValue('CL.READ_ONLY');
  if Assigned(V) then
    Result := V.Value
  else
  begin
    Result := False;
    if (ClientID > 0) then
    begin
      Q := TEvQuery.Create('SELECT READ_ONLY FROM CL WHERE {AsOfNow<CL>}');
      Result := (Q.Result.Fields[0].AsString = READ_ONLY_MAINTENANCE) or ((Q.Result.Fields[0].AsString = READ_ONLY_READ_ONLY) and CheckReadOnlyClientFunction);
      FCachedValues.AddValue('CL.READ_ONLY', Result);
    end;
  end;
end;

function TDataAccessComp.GetCoReadOnlyRight: Boolean;
begin
  Result := False;
  if (ClientID > 0) and Assigned(DM_COMPANY.CO) then
   if DM_COMPANY.CO.FieldByName('CREDIT_HOLD').AsString = CREDIT_HOLD_LEVEL_READONLY then
      Result := CheckReadOnlyCompanyFunction;
end;

function TDataAccessComp.GetEEReadOnlyRight: Boolean;
begin
  Result := False;
  if (ClientID > 0) and Assigned(DM_COMPANY.CO) then
   if DM_COMPANY.CO.FieldByName('ENABLE_HR').AsString = GROUP_BOX_ADVANCED_HR then
      Result := True;
end;

function TDataAccessComp.ClientID: Integer;
begin
  Result := ctx_DBAccess.CurrentClientNbr;
end;

function TDataAccessComp.GetDataSetAsOfDate(const ADataSet: TEvBasicClientDataSet): TisDate;
begin
  Result := ADataSet.AsOfDate;
  if Result = 0 then
    Result := GetAsOfDate;
end;

procedure TDataAccessComp.SetPayrollLockInTransaction(const APrNbr: Integer; const AUnlocked: Boolean);
var
  Params: IisListOfValues;
begin
  Params := TisListOfValues.Create;
  Params.AddValue('PrNbr', APrNbr);
  Params.AddValue('Unlocked', AUnlocked);

  ctx_DBAccess.RunInTransaction('SetPayrollLock', Params);
end;

function TDataAccessComp.GetSQLite: ISQLite;
begin
  if not Assigned(FSQLite) then
    FSQLite := TSQLite.Create(':memory:');
  Result := FSQLite;
end;

procedure TDataAccessComp.SetSQLite(const Value: ISQLite);
begin
  FSQLite := Value;
end;

function TDataAccessComp.CalcQecHash(const CoNbr: Integer;
  const bDate, eDate: TDateTime): String;
var
  Q: IevQuery;
  HashStream: IisStream;

  procedure PutToHashStream(const TableName: String; const FieldNames: String; const Cond: String = '');
  begin
    Q := TEvQuery.Create('select #FIELDS from #TABLE where EFFECTIVE_DATE  <= :END_DATE and EFFECTIVE_UNTIL >= :BEGIN_DATE #COND');
    Q.Macro['TABLE']  := TableName;
    Q.Macro['FIELDS'] := FieldNames;
    Q.Param['END_DATE'] := eDate;
    Q.Param['BEGIN_DATE'] := bDate;
    Q.Macro['COND'] := Cond;
    HashStream.WriteStream(Q.Result.AsStream);
  end;

begin

  HashStream := TisStream.Create;
  PutToHashStream('SY_FED_EXEMPTIONS', 'SY_FED_EXEMPTIONS_NBR, effective_date, E_D_CODE_TYPE, EXEMPT_FEDERAL, EXEMPT_FUI, EXEMPT_EMPLOYEE_OASDI, EXEMPT_EMPLOYER_OASDI, EXEMPT_EMPLOYEE_MEDICARE, EXEMPT_EMPLOYER_MEDICARE, EXEMPT_EMPLOYEE_EIC');
  PutToHashStream('SY_STATE_EXEMPTIONS','SY_STATE_EXEMPTIONS_NBR, effective_date, E_D_CODE_TYPE, EXEMPT_STATE, EXEMPT_EMPLOYEE_SDI, EXEMPT_EMPLOYER_SDI, EXEMPT_EMPLOYEE_SUI, EXEMPT_EMPLOYER_SUI');
  PutToHashStream('SY_LOCAL_EXEMPTIONS', 'SY_LOCAL_EXEMPTIONS_NBR, effective_date, E_D_CODE_TYPE, EXEMPT');
  PutToHashStream('SY_SUI', 'SY_SUI_NBR, effective_date, MAXIMUM_WAGE');
  PutToHashStream('SY_LOCALS', 'SY_LOCALS_NBR, effective_date, WAGE_MAXIMUM, effective_date, effective_until');

  PutToHashStream('CO', 'CO_NBR,effective_date, FEDERAL_TAX_EXEMPT_STATUS, FED_TAX_EXEMPT_EE_OASDI, FED_TAX_EXEMPT_ER_OASDI, FED_TAX_EXEMPT_EE_MEDICARE, FED_TAX_EXEMPT_ER_MEDICARE, FUI_TAX_EXEMPT', 'and CO_NBR='+IntToStr(CoNbr));
  PutToHashStream('CO_SUI', 'CO_SUI_NBR, effective_date, SY_SUI_NBR', 'and CO_NBR='+IntToStr(CoNbr));
  PutToHashStream('CO_LOCAL_TAX', 'CO_LOCAL_TAX_NBR, effective_date, EXEMPT', 'and CO_NBR='+IntToStr(CoNbr));
  PutToHashStream('CO_STATES', 'CO_STATES_NBR, effective_date, STATE_EXEMPT, SUI_EXEMPT, EE_SDI_EXEMPT', 'and CO_NBR='+IntToStr(CoNbr));
  PutToHashStream('EE', 'EE_NBR, effective_date, EXEMPT_EMPLOYEE_OASDI, EXEMPT_EMPLOYEE_MEDICARE, EXEMPT_EXCLUDE_EE_FED, EXEMPT_EMPLOYER_OASDI, EXEMPT_EMPLOYER_MEDICARE, EXEMPT_EMPLOYER_FUI', 'and CO_NBR='+IntToStr(CoNbr));
  PutToHashStream('EE_LOCALS', 'EE_LOCALS_NBR, EE_NBR, effective_date, EXEMPT_EXCLUDE', ' and EE_NBR in (select EE_NBR from EE where CO_NBR='+IntToStr(CoNbr)+' and {AsOfNow<EE>})');
  PutToHashStream('EE_STATES', 'EE_STATES_NBR, EE_NBR, effective_date, STATE_EXEMPT_EXCLUDE, EE_SDI_EXEMPT_EXCLUDE, ER_SDI_EXEMPT_EXCLUDE, EE_SUI_EXEMPT_EXCLUDE, ER_SUI_EXEMPT_EXCLUDE', ' and EE_NBR in (select EE_NBR from EE where CO_NBR='+IntToStr(CoNbr)+' and {AsOfNow<EE>})');
  PutToHashStream('CL_E_DS', 'CL_E_DS_NBR, effective_date, E_D_CODE_TYPE, EE_EXEMPT_EXCLUDE_OASDI, EE_EXEMPT_EXCLUDE_MEDICARE, EE_EXEMPT_EXCLUDE_FEDERAL, EE_EXEMPT_EXCLUDE_EIC, ER_EXEMPT_EXCLUDE_OASDI, ER_EXEMPT_EXCLUDE_MEDICARE, ER_EXEMPT_EXCLUDE_FUI');
  PutToHashStream('CL_E_D_LOCAL_EXMPT_EXCLD', 'CL_E_D_LOCAL_EXMPT_EXCLD_NBR, effective_date, EXEMPT_EXCLUDE');
  PutToHashStream('CL_E_D_STATE_EXMPT_EXCLD', 'CL_E_D_STATE_EXMPT_EXCLD_NBR, effective_date, EMPLOYEE_EXEMPT_EXCLUDE_STATE, EMPLOYEE_EXEMPT_EXCLUDE_SDI, EMPLOYEE_EXEMPT_EXCLUDE_SUI, EMPLOYER_EXEMPT_EXCLUDE_SDI, EMPLOYER_EXEMPT_EXCLUDE_SUI');

  Result := CalcMD5(HashStream.RealStream);

end;

function TDataAccessComp.QecRecommended(const CoNbr: Integer;
  const CheckDate: TDateTime): Boolean;
var
  Q: IevQuery;
  StoredHash, ActualHash: String;

begin

  Q := TevQuery.Create('select PR_SETUP_HASH from CO_QEC_RUN where CO_NBR=:CO_NBR and QUARTER_BEGIN_DATE=:QUARTER_BEGIN_DATE and {AsOfNow<CO_QEC_RUN>}');
  Q.Param['CO_NBR'] := CoNbr;
  Q.Param['QUARTER_BEGIN_DATE'] := GetBeginQuarter(CheckDate);
  if Q.Result.Eof then
  begin
    Result := True;
    Q := TevQuery.Create('select PR_SETUP_HASH from CO_QEC_RUN where CO_NBR=:CO_NBR and QUARTER_BEGIN_DATE=:QUARTER_BEGIN_DATE and {AsOfNow<CO_QEC_RUN>}');
    Q.Param['CO_NBR'] := CoNbr;
    Q.Param['QUARTER_BEGIN_DATE'] := GetBeginQuarter(GetBeginQuarter(CheckDate)-1);
    if not Q.Result.Eof then
    begin
      StoredHash := Q.Result['PR_SETUP_HASH'];
      if  StoredHash = CalcQecHash(CoNbr, GetBeginQuarter(GetBeginQuarter(CheckDate)-1), GetEndQuarter(CheckDate)) then
      begin
        DoOnQecFinished(CoNbr,CheckDate);
        Result := false;
      end;
    end;
    Exit;
  end
  else
    StoredHash := Q.Result['PR_SETUP_HASH'];

  ActualHash := CalcQecHash(CoNbr, GetBeginQuarter(CheckDate), GetEndQuarter(CheckDate));

  Result := ActualHash <> StoredHash;

end;

procedure TDataAccessComp.DoOnQecFinished(const CoNbr: Integer;
  const CheckDate: TDateTime);
var
  T: IevTable;
  ActualHash: String;
begin
  ActualHash := CalcQecHash(CoNbr, GetBeginQuarter(CheckDate), GetEndQuarter(CheckDate));
  T := TevTable.CreateAsQuery('CO_QEC_RUN', 'select PR_SETUP_HASH, CO_QEC_RUN_NBR, QUARTER_BEGIN_DATE, CO_NBR from CO_QEC_RUN '+
    ' where CO_NBR='+IntToStr(CoNbr)+' and QUARTER_BEGIN_DATE='''+DateToStr(GetBeginQuarter(CheckDate))+''' and {AsOfNow<CO_QEC_RUN>}');
  T.Open;
  T.First;
  if T.Eof then
  begin
    T.Insert;
    T.FieldByName('PR_SETUP_HASH').AsString := ActualHash;
    T.FieldByName('QUARTER_BEGIN_DATE').AsDateTime := GetBeginQuarter(CheckDate);
    T.FieldByName('CO_NBR').AsInteger := CoNbr;
    T.Post;
    T.SaveChanges;
  end
  else
  if T.FieldByName('PR_SETUP_HASH').AsString <> ActualHash then
  begin
    T.Edit;
    T.FieldByName('PR_SETUP_HASH').AsString := ActualHash;
    T.Post;
    T.SaveChanges;
  end;
end;

procedure TDataAccessComp.CollectChanges(const ADataSet: TEvClientDataSet);
var
  ChangePacket: IevDataChangePacket;
begin
  Assert(ctx_DBAccess.InTransaction);

  ChangePacket := ADataSet.GetChangePacket;
  if Assigned(ChangePacket) then
  begin
    FPostingDataSets.Add(ADataSet);
    FDataSetChanges.Add(ChangePacket);

    if FChangingDataSets.IndexOf(ADataSet) = -1 then
    begin
      Assert(ADataSet is TevClientDataset);
      ADataSet.OnDestroy := OnDestroyDS;
      FChangingDataSets.Add(ADataSet);
    end;
  end;
end;

procedure TDataAccessComp.PostCollectedChanges;
var
  i: Integer;
begin
  for i := 0 to FDataSetChanges.Count - 1 do
    ctx_DBAccess.ApplyDataChangePacket(FDataSetChanges[i] as IevDataChangePacket);
end;

procedure TDataAccessComp.ResolveCircularReferences;
type
   TCircularRefsBreakPoint = record
     Table: String;
     Field: String;
   end;
const
  // Items must be grouped by Table
  CircularRefsBreakPoints: array [0..0] of TCircularRefsBreakPoint =
    ((Table: 'EE'; Field: 'HOME_TAX_EE_STATES_NBR'));
var
  i, j: Integer;
  changePacket, preChangePacket, postChangePacket: IevDataChangePacket;

  procedure CheckItem(AChangeItem: IevDataChangeItem);
  var
    i: Integer;
    val: IisNamedValue;
    pChangeItem: IevDataChangeItem;
  begin
    if AChangeItem.ChangeType = dctInsertRecord then
    begin
      for i := 0 to High(CircularRefsBreakPoints) do
        if AnsiSameText(AChangeItem.DataSource, CircularRefsBreakPoints[i].Table) then
        begin
          val := AChangeItem.Fields.FindValue(CircularRefsBreakPoints[i].Field);
          if Assigned(val) and (val.Value <> Null) then
          begin
            pChangeItem := postChangePacket.NewUpdateFields(AChangeItem.DataSource, AChangeItem.Key);
            pChangeItem.Fields.AddValue(val.Name, val.Value);
            pChangeItem.Fields.AddValue('EFFECTIVE_DATE', BeginOfTime); // this indicates that all rec_versions should be updated
            AChangeItem.Fields.DeleteValue(val.Name);
          end;
        end;
    end

    else if AChangeItem.ChangeType = dctDeleteRecord then
    begin
      for i := 0 to High(CircularRefsBreakPoints) do
        if AnsiSameText(AChangeItem.DataSource, CircularRefsBreakPoints[i].Table) then
        begin
          if not Assigned(pChangeItem) then
            pChangeItem := preChangePacket.NewUpdateFields(AChangeItem.DataSource, AChangeItem.Key);
          pChangeItem.Fields.AddValue(CircularRefsBreakPoints[i].Field, Null);
          pChangeItem.Fields.AddValue('EFFECTIVE_DATE', BeginOfTime); // this indicates that all rec_versions should be updated
        end
        else
          pChangeItem := nil;
    end;
  end;

begin
  preChangePacket := TevDataChangePacket.Create;;
  postChangePacket := TevDataChangePacket.Create;

  for i := 0 to FDataSetChanges.Count - 1 do
  begin
    changePacket := FDataSetChanges[i] as IevDataChangePacket;

    for j := 0 to changePacket.Count - 1 do
      CheckItem(changePacket[j]);
  end;

  if preChangePacket.Count > 0 then
    FDataSetChanges.Insert(0, preChangePacket);

  if postChangePacket.Count > 0 then
    FDataSetChanges.Add(postChangePacket);
end;

procedure TDataAccessComp.ClearCachedData;
begin
  FCachedValues.Clear;
end;

function TDataAccessComp.GetIsForTaxCalculator: Boolean;
begin
  Result := FIsForTaxCalculator;
end;

procedure TDataAccessComp.SetIsForTaxCalculator(const Value: Boolean);
begin
  FIsForTaxCalculator := Value;
end;

{ TDestroyCallBack }

destructor TDestroyCallBack.Destroy;
begin
  CallBack(Index);
  inherited;
end;

initialization
  Classes.RegisterFindGlobalComponentProc(FindDM);
  Mainboard.ModuleRegister.RegisterModule(@GetDataAccess, IevDataAccess, 'Data Access');

finalization
  Classes.UnregisterFindGlobalComponentProc(FindDM);


end.


