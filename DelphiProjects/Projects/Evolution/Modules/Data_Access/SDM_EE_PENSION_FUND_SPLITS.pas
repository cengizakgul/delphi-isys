// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_PENSION_FUND_SPLITS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_EE_PENSION_FUND_SPLITS = class(TDM_ddTable)
    EE_PENSION_FUND_SPLITS: TEE_PENSION_FUND_SPLITS;
    EE_PENSION_FUND_SPLITSEE_PENSION_FUND_SPLITS_NBR: TIntegerField;
    EE_PENSION_FUND_SPLITSEE_NBR: TIntegerField;
    EE_PENSION_FUND_SPLITSCL_FUNDS_NBR: TIntegerField;
    EE_PENSION_FUND_SPLITSCL_E_DS_NBR: TIntegerField;
    EE_PENSION_FUND_SPLITSPERCENTAGE: TFloatField;
    EE_PENSION_FUND_SPLITSEMPLOYEE_OR_EMPLOYER: TStringField;
    EE_PENSION_FUND_SPLITSFunds_lookup: TStringField;
    EE_PENSION_FUND_SPLITSE_D_Code_lookup: TStringField;
    DM_CLIENT: TDM_CLIENT;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_EE_PENSION_FUND_SPLITS);

finalization
  UnregisterClass(TDM_EE_PENSION_FUND_SPLITS);

end.
