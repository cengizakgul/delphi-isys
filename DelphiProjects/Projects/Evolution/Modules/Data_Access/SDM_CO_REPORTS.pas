// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_REPORTS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau ,EvStreamUtils, SPayrollReport, EvUIComponents,
  EvClientDataSet;

type


  TDM_CO_REPORTS = class(TDM_ddTable)
    CO_REPORTS: TCO_REPORTS;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    CO_REPORTSRWDescription: TStringField;
    CO_REPORTSCO_REPORTS_NBR: TIntegerField;
    CO_REPORTSCO_NBR: TIntegerField;
    CO_REPORTSCUSTOM_NAME: TStringField;
    CO_REPORTSREPORT_WRITER_REPORTS_NBR: TIntegerField;
    CO_REPORTSREPORT_LEVEL: TStringField;
    CO_REPORTSRUN_PARAMS: TBlobField;
    CO_REPORTSINPUT_PARAMS: TBlobField;
    CO_REPORTSOVERRIDE_MB_GROUP_NBR: TIntegerField;
    CO_REPORTSndescription: TStringField;
    CO_REPORTSPriority: TIntegerField;
    CO_REPORTSHideForRemotes: TStringField;
    CO_REPORTSFAVORITE: TStringField;
    procedure CO_REPORTSCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  private
    FRunParams: TevPrRepPrnSettings;
  public
  end;


implementation

{$R *.DFM}

procedure TDM_CO_REPORTS.CO_REPORTSCalcFields(DataSet: TDataSet);

begin
  CO_REPORTSNDescription.AsString := CO_REPORTSCUSTOM_NAME.AsString + ' (' +
    CO_REPORTSREPORT_LEVEL.AsString + CO_REPORTSREPORT_WRITER_REPORTS_NBR.AsString +')';

  if not CO_REPORTS.RUN_PARAMS.IsNull then
  begin
    FRunParams.ReadFromBlobField(CO_REPORTSRUN_PARAMS);
    CO_REPORTSPriority.Value := FRunParams.priority;
    //MR added for reso# 37567
    if FRunParams.HideForRemotes then
      CO_REPORTSHideForRemotes.Value := 'Y'
    else
      CO_REPORTSHideForRemotes.Value := 'N';
  end;

end;

procedure TDM_CO_REPORTS.DataModuleCreate(Sender: TObject);
begin
  inherited;
  FRunParams := TevPrRepPrnSettings.Create(nil);
end;

procedure TDM_CO_REPORTS.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FRunParams.Free;
end;

initialization
  RegisterClass( TDM_CO_REPORTS );

finalization
  UnregisterClass( TDM_CO_REPORTS );

end.
