inherited DM_EE_AUTOLABOR_DISTRIBUTION: TDM_EE_AUTOLABOR_DISTRIBUTION
  OldCreateOrder = True
  Left = 423
  Top = 260
  Height = 249
  Width = 382
  object EE_AUTOLABOR_DISTRIBUTION: TEE_AUTOLABOR_DISTRIBUTION
    ProviderName = 'EE_AUTOLABOR_DISTRIBUTION_PROV'
    AggregatesActive = True
    BeforePost = EE_AUTOLABOR_DISTRIBUTIONBeforePost
    OnNewRecord = EE_AUTOLABOR_DISTRIBUTIONNewRecord
    Left = 93
    Top = 24
    object EE_AUTOLABOR_DISTRIBUTIONEE_AUTOLABOR_DISTRIBUTION_NBR: TIntegerField
      FieldName = 'EE_AUTOLABOR_DISTRIBUTION_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.EE_AUTOLABOR_DISTRIBUTION_NBR'
      Required = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_NBR'
      Required = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_DIVISION_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_BRANCH_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_DEPARTMENT_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_TEAM_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.EE_NBR'
      Required = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_JOBS_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONCO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CO_WORKERS_COMP_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.CL_E_D_GROUPS_NBR'
    end
    object EE_AUTOLABOR_DISTRIBUTIONPERCENTAGE: TFloatField
      FieldName = 'PERCENTAGE'
      Origin = 'EE_AUTOLABOR_DISTRIBUTION_HIST.PERCENTAGE'
      Required = True
      DisplayFormat = '#,##0.00####'
    end
    object EE_AUTOLABOR_DISTRIBUTIONCUSTOM_DIVISION_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_DIVISION_NUMBER'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'CUSTOM_DIVISION_NUMBER'
      KeyFields = 'CO_DIVISION_NBR'
      Lookup = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONCUSTOM_BRANCH_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_BRANCH_NUMBER'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'CUSTOM_BRANCH_NUMBER'
      KeyFields = 'CO_BRANCH_NBR'
      Lookup = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONCUSTOM_DEPARTMENT_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_DEPARTMENT_NUMBER'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'CUSTOM_DEPARTMENT_NUMBER'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Lookup = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONCUSTOM_TEAM_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_TEAM_NUMBER'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'CUSTOM_TEAM_NUMBER'
      KeyFields = 'CO_TEAM_NBR'
      Lookup = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONJobDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'JobDescription'
      LookupDataSet = DM_CO_JOBS.CO_JOBS
      LookupKeyFields = 'CO_JOBS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_JOBS_NBR'
      Size = 40
      Lookup = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONWorkersComp: TStringField
      FieldKind = fkLookup
      FieldName = 'WorkersComp'
      LookupDataSet = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
      LookupKeyFields = 'CO_WORKERS_COMP_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_WORKERS_COMP_NBR'
      Size = 40
      Lookup = True
    end
    object EE_AUTOLABOR_DISTRIBUTIONEDGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGroupName'
      LookupDataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
      LookupKeyFields = 'CL_E_D_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_E_D_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 224
    Top = 32
  end
end
