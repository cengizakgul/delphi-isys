unit SDM_CO_BENEFIT_PKG_DETAIL;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils, SFieldCodeValues,
  EvClientDataSet;

type
  TDM_CO_BENEFIT_PKG_DETAIL = class(TDM_ddTable)
    CO_BENEFIT_PKG_DETAIL: TCO_BENEFIT_PKG_DETAIL;
    CO_BENEFIT_PKG_DETAILPackageName: TStringField;
    CO_BENEFIT_PKG_DETAILBenefitName: TStringField;
    DM_COMPANY: TDM_COMPANY;
    CO_BENEFIT_PKG_DETAILCO_BENEFIT_PACKAGE_NBR: TIntegerField;
    CO_BENEFIT_PKG_DETAILCO_BENEFIT_PKG_DETAIL_NBR: TIntegerField;
    CO_BENEFIT_PKG_DETAILCO_BENEFITS_NBR: TIntegerField;
    CO_BENEFIT_PKG_DETAILCategoryName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BENEFIT_PKG_DETAIL: TDM_CO_BENEFIT_PKG_DETAIL;

implementation

{$R *.DFM}


initialization
  RegisterClass(TDM_CO_BENEFIT_PKG_DETAIL);

finalization
  UnregisterClass(TDM_CO_BENEFIT_PKG_DETAIL);

end.
