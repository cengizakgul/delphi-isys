// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_ENLIST_GROUPS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, SDataStructure, evUtils,
  SFieldCodeValues, EvClientDataSet;

type
  TDM_SB_ENLIST_GROUPS = class(TDM_ddTable)
    SB_ENLIST_GROUPS: TSB_ENLIST_GROUPS;
    SB_ENLIST_GROUPSSY_REPORT_GROUPS_NBR: TIntegerField;
    SB_ENLIST_GROUPSReportGroups: TStringField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_ENLIST_GROUPSMEDIA_TYPE: TStringField;
    SB_ENLIST_GROUPSPROCESS_TYPE: TStringField;
    SB_ENLIST_GROUPSSB_ENLIST_GROUPS_NBR: TIntegerField;
    SB_ENLIST_GROUPSProcess: TStringField;
    SB_ENLIST_GROUPSNMediaType: TStringField;
    procedure SB_ENLIST_GROUPSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_SB_ENLIST_GROUPS.SB_ENLIST_GROUPSCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  SB_ENLIST_GROUPSProcess.AsString := 'Don''t Enlist';
  if SB_ENLIST_GROUPSPROCESS_TYPE.AsString = 'A' then
  begin
    SB_ENLIST_GROUPSProcess.AsString := 'Enlist';
    SB_ENLIST_GROUPSNMediaType.AsString :=
        ReturnDescription(MediaType_ComboChoices, SB_ENLIST_GROUPSMEDIA_TYPE.AsString);
  end;


  if not DM_SYSTEM_MISC.SY_REPORT_GROUPS.Active then
      DM_SYSTEM_MISC.SY_REPORT_GROUPS.DataRequired('');

  if SB_ENLIST_GROUPSSY_REPORT_GROUPS_NBR.AsInteger > 0  then
   SB_ENLIST_GROUPSReportGroups.AsString := DM_SYSTEM_MISC.SY_REPORT_GROUPS.ShadowDataSet.CachedLookup(SB_ENLIST_GROUPSSY_REPORT_GROUPS_NBR.AsInteger,'NAME');

     
end;

initialization
  RegisterClass(TDM_SB_ENLIST_GROUPS);

finalization
  UnregisterClass(TDM_SB_ENLIST_GROUPS);

end.
