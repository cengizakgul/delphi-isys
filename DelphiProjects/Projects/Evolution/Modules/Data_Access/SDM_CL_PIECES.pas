// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_PIECES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, EvExceptions;

type
  TDM_CL_PIECES = class(TDM_ddTable)
    CL_PIECES: TCL_PIECES;
    CL_PIECESDEFAULT_RATE: TFloatField;
    procedure CL_PIECESBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  evtypes;
{$R *.DFM}
//gdy
procedure TDM_CL_PIECES.CL_PIECESBeforePost(DataSet: TDataSet);
begin
  if DataSet.FieldByName('NAME').IsNull or ( Trim(DataSet.FieldByName('NAME').AsString) = '' ) then
    raise EUpdateError.CreateHelp('Piece name is a required field', IDH_ConsistencyViolation);
end;

initialization
  RegisterClass( TDM_CL_PIECES );

finalization
  UnregisterClass( TDM_CL_PIECES );

end.
