// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_SHIFTS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvExceptions;

type
  TDM_CO_SHIFTS = class(TDM_ddTable)
    CO_SHIFTS: TCO_SHIFTS;
    CO_SHIFTSCL_E_D_GROUPS_NBR: TIntegerField;
    CO_SHIFTSCO_NBR: TIntegerField;
    CO_SHIFTSCO_SHIFTS_NBR: TIntegerField;
    CO_SHIFTSDEFAULT_AMOUNT: TFloatField;
    CO_SHIFTSDEFAULT_PERCENTAGE: TFloatField;
    CO_SHIFTSNAME: TStringField;
    CO_SHIFTSEDGroupName: TStringField;
    DM_CLIENT: TDM_CLIENT;
    CO_SHIFTSAUTO_CREATE_ON_NEW_HIRE: TStringField;
    procedure CO_SHIFTSBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CO_SHIFTS.CO_SHIFTSBeforePost(DataSet: TDataSet);
begin
  if DataSet.FieldByName('DEFAULT_PERCENTAGE').IsNull and
     DataSet.FieldByName('DEFAULT_AMOUNT').IsNull then
    raise EInconsistentData.Create('Default % or Default Amount must be entered');
end;

initialization
  RegisterClass(TDM_CO_SHIFTS);

finalization
  UnregisterClass(TDM_CO_SHIFTS);

end.
