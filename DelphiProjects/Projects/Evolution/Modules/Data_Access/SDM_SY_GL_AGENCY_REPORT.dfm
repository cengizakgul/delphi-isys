inherited DM_SY_GL_AGENCY_REPORT: TDM_SY_GL_AGENCY_REPORT
  OldCreateOrder = True
  Left = 283
  Top = 161
  Height = 479
  Width = 741
  object SY_GL_AGENCY_REPORT: TSY_GL_AGENCY_REPORT
    ProviderName = 'SY_GL_AGENCY_REPORT_PROV'
    PictureMasks.Strings = (
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F')
    OnCalcFields = SY_GL_AGENCY_REPORTCalcFields
    Left = 74
    Top = 76
    object SY_GL_AGENCY_REPORTDESCRIPTION: TStringField
      DisplayWidth = 40
      FieldKind = fkCalculated
      FieldName = 'DESCRIPTION'
      Size = 40
      Calculated = True
    end
    object SY_GL_AGENCY_REPORTSY_GL_AGENCY_REPORT_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
      Visible = False
    end
    object SY_GL_AGENCY_REPORTSYSTEM_TAX_TYPE: TStringField
      FieldName = 'SYSTEM_TAX_TYPE'
      Visible = False
      Size = 1
    end
    object SY_GL_AGENCY_REPORTRETURN_FREQUENCY: TStringField
      FieldName = 'RETURN_FREQUENCY'
      Visible = False
      Size = 1
    end
    object SY_GL_AGENCY_REPORTDEPOSIT_FREQUENCY: TStringField
      FieldName = 'DEPOSIT_FREQUENCY'
      Visible = False
      Size = 1
    end
    object SY_GL_AGENCY_REPORTSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
      Visible = False
    end
    object SY_GL_AGENCY_REPORTWHEN_DUE_TYPE: TStringField
      FieldName = 'WHEN_DUE_TYPE'
      Visible = False
      Size = 1
    end
    object SY_GL_AGENCY_REPORTNUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField
      FieldName = 'NUMBER_OF_DAYS_FOR_WHEN_DUE'
      Visible = False
    end
    object SY_GL_AGENCY_REPORTENLIST_AUTOMATICALLY: TStringField
      FieldName = 'ENLIST_AUTOMATICALLY'
      Visible = False
      Size = 1
    end
    object SY_GL_AGENCY_REPORTPRINT_WHEN: TStringField
      FieldName = 'PRINT_WHEN'
      Visible = False
      Size = 1
    end
    object SY_GL_AGENCY_REPORTFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object SY_GL_AGENCY_REPORTTAX_SERVICE_FILTER: TStringField
      FieldName = 'TAX_SERVICE_FILTER'
      Size = 1
    end
    object SY_GL_AGENCY_REPORTCONSOLIDATED_FILTER: TStringField
      FieldName = 'CONSOLIDATED_FILTER'
      Size = 1
    end
    object SY_GL_AGENCY_REPORTEND_EFFECTIVE_MONTH: TIntegerField
      FieldName = 'END_EFFECTIVE_MONTH'
    end
    object SY_GL_AGENCY_REPORTBEGIN_EFFECTIVE_MONTH: TIntegerField
      FieldName = 'BEGIN_EFFECTIVE_MONTH'
    end
    object SY_GL_AGENCY_REPORTTAX_RETURN_ACTIVE: TStringField
      FieldName = 'TAX_RETURN_ACTIVE'
      Size = 1
    end
    object SY_GL_AGENCY_REPORTSY_REPORTS_GROUP_NBR: TIntegerField
      FieldName = 'SY_REPORTS_GROUP_NBR'
    end
  end
end
