inherited DM_CO_PHONE: TDM_CO_PHONE
  OldCreateOrder = True
  Height = 149
  Width = 132
  object CO_PHONE: TCO_PHONE
    ProviderName = 'CO_PHONE_PROV'
    PictureMasks.Strings = (
      'PHONE_NUMBER'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F')
    OnCalcFields = CO_PHONECalcFields
    Left = 83
    Top = 23
    object CO_PHONEdescPhoneType: TStringField
      FieldKind = fkCalculated
      FieldName = 'descPhoneType'
      Size = 30
      Calculated = True
    end
    object CO_PHONEPHONE_TYPE: TStringField
      FieldName = 'PHONE_TYPE'
      Size = 1
    end
  end
end
