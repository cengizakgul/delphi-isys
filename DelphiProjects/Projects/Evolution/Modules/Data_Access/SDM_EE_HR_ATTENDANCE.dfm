inherited DM_EE_HR_ATTENDANCE: TDM_EE_HR_ATTENDANCE
  Left = 248
  Top = 94
  Height = 540
  Width = 783
  object EE_HR_ATTENDANCE: TEE_HR_ATTENDANCE
    ProviderName = 'EE_HR_ATTENDANCE_PROV'
    OnNewRecord = EE_HR_ATTENDANCENewRecord
    Left = 88
    Top = 40
    object EE_HR_ATTENDANCEATTENDANCE_TYPE_NUMBER: TIntegerField
      FieldName = 'ATTENDANCE_TYPE_NUMBER'
    end
    object EE_HR_ATTENDANCECO_HR_ATTENDANCE_TYPES_NBR: TIntegerField
      FieldName = 'CO_HR_ATTENDANCE_TYPES_NBR'
    end
    object EE_HR_ATTENDANCEEE_HR_ATTENDANCE_NBR: TIntegerField
      FieldName = 'EE_HR_ATTENDANCE_NBR'
    end
    object EE_HR_ATTENDANCEEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_HR_ATTENDANCEEXCUSED: TStringField
      FieldName = 'EXCUSED'
      Size = 1
    end
    object EE_HR_ATTENDANCEHOURS_USED: TFloatField
      FieldName = 'HOURS_USED'
      DisplayFormat = '#,##0.00'
    end
    object EE_HR_ATTENDANCENOTES: TBlobField
      FieldName = 'NOTES'
      Size = 8
    end
    object EE_HR_ATTENDANCEPERIOD_FROM: TDateField
      FieldName = 'PERIOD_FROM'
    end
    object EE_HR_ATTENDANCEPERIOD_TO: TDateField
      FieldName = 'PERIOD_TO'
    end
    object EE_HR_ATTENDANCEWARNING_ISSUED: TStringField
      FieldName = 'WARNING_ISSUED'
      Size = 1
    end
    object EE_HR_ATTENDANCEAttendanceDescr: TStringField
      FieldKind = fkLookup
      FieldName = 'AttendanceDescr'
      LookupDataSet = DM_CO_HR_ATTENDANCE_TYPES.CO_HR_ATTENDANCE_TYPES
      LookupKeyFields = 'CO_HR_ATTENDANCE_TYPES_NBR'
      LookupResultField = 'ATTENDANCE_DESCRIPTION'
      KeyFields = 'CO_HR_ATTENDANCE_TYPES_NBR'
      Size = 40
      Lookup = True
    end
    object EE_HR_ATTENDANCEPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 240
    Top = 64
  end
end
