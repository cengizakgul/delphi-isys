// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_REPORTS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataStructure, evUtils, EvClientDataSet;

type
  TDM_SY_REPORTS = class(TDM_ddTable)
    SY_REPORTS: TSY_REPORTS;
    SY_REPORTSSY_REPORTS_NBR: TIntegerField;
    SY_REPORTSDESCRIPTION: TStringField;
    SY_REPORTSACTIVE_REPORT: TStringField;
    SY_REPORTSSY_REPORT_WRITER_REPORTS_NBR: TIntegerField;
    SY_REPORTSNDescription: TStringField;
    SY_REPORTSFILLER: TStringField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    procedure SY_REPORTSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SY_REPORTS: TDM_SY_REPORTS;

implementation

{$R *.dfm}

procedure TDM_SY_REPORTS.SY_REPORTSCalcFields(DataSet: TDataSet);
begin
  SY_REPORTSNDescription.AsString := SY_REPORTSDescription.AsString + ' (S' + SY_REPORTSSY_REPORT_WRITER_REPORTS_NBR.AsString + ')';
end;

initialization
  RegisterClass(TDM_SY_REPORTS);

finalization
  UnregisterClass(TDM_SY_REPORTS);

end.
