// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_E_D_STATE_EXMPT_EXCLD;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   EvUtils, EvContext;

type
  TDM_CL_E_D_STATE_EXMPT_EXCLD = class(TDM_ddTable)
    CL_E_D_STATE_EXMPT_EXCLD: TCL_E_D_STATE_EXMPT_EXCLD;
    CL_E_D_STATE_EXMPT_EXCLDCL_E_D_STATE_EXMPT_EXCLD_NBR: TIntegerField;
    CL_E_D_STATE_EXMPT_EXCLDCL_E_DS_NBR: TIntegerField;
    CL_E_D_STATE_EXMPT_EXCLDSY_STATES_NBR: TIntegerField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATE: TStringField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_SDI: TStringField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_SUI: TStringField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_STATE_OR_VALUE: TFloatField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_STATE_OR_TYPE: TStringField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYER_EXEMPT_EXCLUDE_SDI: TStringField;
    CL_E_D_STATE_EXMPT_EXCLDEMPLOYER_EXEMPT_EXCLUDE_SUI: TStringField;
    CL_E_D_STATE_EXMPT_EXCLDState_Lookup: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    procedure CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate(
      Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CL_E_D_STATE_EXMPT_EXCLD.CL_E_D_STATE_EXMPT_EXCLDEMPLOYEE_EXEMPT_EXCLUDE_STATEValidate(
  Sender: TField);
begin
  DM_COMPANY.CO.DataRequired();
  DM_COMPANY.CO.First;
  while not DM_COMPANY.CO.Eof do
  begin
    ctx_DataAccess.CheckCompanyLock;
    DM_COMPANY.CO.Next
  end
end;

initialization
  RegisterClass(TDM_CL_E_D_STATE_EXMPT_EXCLD);

finalization
  UnregisterClass(TDM_CL_E_D_STATE_EXMPT_EXCLD);

end.
