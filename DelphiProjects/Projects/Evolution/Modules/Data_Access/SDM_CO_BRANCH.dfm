inherited DM_CO_BRANCH: TDM_CO_BRANCH
  OldCreateOrder = True
  Left = 347
  Top = 206
  Height = 480
  Width = 696
  object CO_BRANCH: TCO_BRANCH
    ProviderName = 'CO_BRANCH_PROV'
    PictureMasks.Strings = (
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      
        'OVERRIDE_PAY_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    FieldDefs = <
      item
        Name = 'CO_BRANCH_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_DIVISION_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_BRANCH_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ADDRESS1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ADDRESS2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CITY'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'ZIP_CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CONTACT1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PHONE1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CONTACT2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PHONE2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FAX'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FAX_DESCRIPTION'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'E_MAIL_ADDRESS'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'HOME_CO_STATES_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'HOME_STATE_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_TIMECLOCK_IMPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAY_FREQUENCY_HOURLY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAY_FREQUENCY_SALARY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRINT_BRANCH_ADDRESS_ON_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_BILLING_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BILLING_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DD_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_WORKERS_COMP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'UNION_CL_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'OVERRIDE_PAY_RATE'
        DataType = ftFloat
      end
      item
        Name = 'OVERRIDE_EE_RATE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CL_DELIVERY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BRANCH_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'PR_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_EE_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_EE_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_DBDT_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
        DataType = ftInteger
      end>
    BeforePost = CO_BRANCHBeforePost
    OnCalcFields = CO_BRANCHCalcFields
    BlobsLoadMode = blmManualLoad
    Left = 160
    Top = 120
    object CO_BRANCHCO_BRANCH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BRANCH_NBR'
      Origin = '"CO_BRANCH_HIST"."CO_BRANCH_NBR"'
      Required = True
    end
    object CO_BRANCHCO_DIVISION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DIVISION_NBR'
      Origin = '"CO_BRANCH_HIST"."CO_DIVISION_NBR"'
      Required = True
    end
    object CO_BRANCHCUSTOM_BRANCH_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_BRANCH_NUMBER'
      Origin = '"CO_BRANCH_HIST"."CUSTOM_BRANCH_NUMBER"'
      Required = True
    end
    object CO_BRANCHNAME: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = '"CO_BRANCH_HIST"."NAME"'
      Size = 40
    end
    object CO_BRANCHADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"CO_BRANCH_HIST"."ADDRESS1"'
      Size = 30
    end
    object CO_BRANCHADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"CO_BRANCH_HIST"."ADDRESS2"'
      Size = 30
    end
    object CO_BRANCHCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CO_BRANCH_HIST"."CITY"'
    end
    object CO_BRANCHSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CO_BRANCH_HIST"."STATE"'
      FixedChar = True
      Size = 2
    end
    object CO_BRANCHZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CO_BRANCH_HIST"."ZIP_CODE"'
      Size = 10
    end
    object CO_BRANCHCONTACT1: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT1'
      Origin = '"CO_BRANCH_HIST"."CONTACT1"'
      Size = 30
    end
    object CO_BRANCHPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"CO_BRANCH_HIST"."PHONE1"'
    end
    object CO_BRANCHDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"CO_BRANCH_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object CO_BRANCHCONTACT2: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT2'
      Origin = '"CO_BRANCH_HIST"."CONTACT2"'
      Size = 30
    end
    object CO_BRANCHPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"CO_BRANCH_HIST"."PHONE2"'
    end
    object CO_BRANCHDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"CO_BRANCH_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object CO_BRANCHFAX: TStringField
      DisplayWidth = 20
      FieldName = 'FAX'
      Origin = '"CO_BRANCH_HIST"."FAX"'
    end
    object CO_BRANCHFAX_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX_DESCRIPTION'
      Origin = '"CO_BRANCH_HIST"."FAX_DESCRIPTION"'
      Size = 10
    end
    object CO_BRANCHE_MAIL_ADDRESS: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS'
      Origin = '"CO_BRANCH_HIST"."E_MAIL_ADDRESS"'
      Size = 80
    end
    object CO_BRANCHHOME_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_CO_STATES_NBR'
      Origin = '"CO_BRANCH_HIST"."HOME_CO_STATES_NBR"'
      Required = True
    end
    object CO_BRANCHHOME_STATE_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'HOME_STATE_TYPE'
      Origin = '"CO_BRANCH_HIST"."HOME_STATE_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_BRANCHCL_TIMECLOCK_IMPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_TIMECLOCK_IMPORTS_NBR'
      Origin = '"CO_BRANCH_HIST"."CL_TIMECLOCK_IMPORTS_NBR"'
    end
    object CO_BRANCHPAY_FREQUENCY_HOURLY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_HOURLY'
      Origin = '"CO_BRANCH_HIST"."PAY_FREQUENCY_HOURLY"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_BRANCHPAY_FREQUENCY_SALARY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_SALARY'
      Origin = '"CO_BRANCH_HIST"."PAY_FREQUENCY_SALARY"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_BRANCHPRINT_BRANCH_ADDRESS_ON_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_BRANCH_ADDRESS_ON_CHECK'
      Origin = '"CO_BRANCH_HIST"."PRINT_BRANCH_ADDRESS_ON_CHECK"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_BRANCHPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_BRANCH_HIST"."PAYROLL_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_BRANCHCL_BILLING_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_BILLING_NBR'
      Origin = '"CO_BRANCH_HIST"."CL_BILLING_NBR"'
    end
    object CO_BRANCHBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BILLING_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_BRANCH_HIST"."BILLING_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_BRANCHTAX_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_BRANCH_HIST"."TAX_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_BRANCHDD_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DD_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_BRANCH_HIST"."DD_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_BRANCHCO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"CO_BRANCH_HIST"."CO_WORKERS_COMP_NBR"'
    end
    object CO_BRANCHUNION_CL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'UNION_CL_E_DS_NBR'
      Origin = '"CO_BRANCH_HIST"."UNION_CL_E_DS_NBR"'
    end
    object CO_BRANCHGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"CO_BRANCH_HIST"."GENERAL_LEDGER_TAG"'
    end
    object CO_BRANCHOVERRIDE_PAY_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_PAY_RATE'
      Origin = '"CO_BRANCH_HIST"."OVERRIDE_PAY_RATE"'
    end
    object CO_BRANCHOVERRIDE_EE_RATE_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_EE_RATE_NUMBER'
      Origin = '"CO_BRANCH_HIST"."OVERRIDE_EE_RATE_NUMBER"'
    end
    object CO_BRANCHCL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_BRANCH_HIST"."CL_DELIVERY_GROUP_NBR"'
    end
    object CO_BRANCHBRANCH_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'BRANCH_NOTES'
      Origin = '"CO_BRANCH_HIST"."BRANCH_NOTES"'
      Size = 8
    end
    object CO_BRANCHFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CO_BRANCH_HIST"."FILLER"'
      Size = 512
    end
    object CO_BRANCHPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
    end
    object CO_BRANCHPR_EE_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_MB_GROUP_NBR'
    end
    object CO_BRANCHPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
    end
    object CO_BRANCHTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
    end
    object CO_BRANCHPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_MB_GROUP_NBR'
    end
    object CO_BRANCHPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
    end
    object CO_BRANCHWORKSITE_ID: TStringField
      FieldName = 'WORKSITE_ID'
    end
    object CO_BRANCHAccountNumber: TStringField
      FieldKind = fkCalculated
      FieldName = 'AccountNumber'
      Calculated = True
    end
    object CO_BRANCHPrimary: TStringField
      FieldKind = fkCalculated
      FieldName = 'Primary'
      Size = 1
      Calculated = True
    end
  end
end
