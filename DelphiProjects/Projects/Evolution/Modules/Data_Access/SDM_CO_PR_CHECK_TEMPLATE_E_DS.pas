// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_PR_CHECK_TEMPLATE_E_DS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_CO_PR_CHECK_TEMPLATE_E_DS = class(TDM_ddTable)
    CO_PR_CHECK_TEMPLATE_E_DS: TCO_PR_CHECK_TEMPLATE_E_DS;
    DM_CLIENT: TDM_CLIENT;
    CO_PR_CHECK_TEMPLATE_E_DSCO_PR_CHECK_TEMPLATE_E_DS_NBR: TIntegerField;
    CO_PR_CHECK_TEMPLATE_E_DSCO_PR_CHECK_TEMPLATES_NBR: TIntegerField;
    CO_PR_CHECK_TEMPLATE_E_DSCL_E_DS_NBR: TIntegerField;
    CO_PR_CHECK_TEMPLATE_E_DSCode: TStringField;
    CO_PR_CHECK_TEMPLATE_E_DSDescription: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_PR_CHECK_TEMPLATE_E_DS: TDM_CO_PR_CHECK_TEMPLATE_E_DS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_PR_CHECK_TEMPLATE_E_DS);

finalization
  UnregisterClass(TDM_CO_PR_CHECK_TEMPLATE_E_DS);

end.
