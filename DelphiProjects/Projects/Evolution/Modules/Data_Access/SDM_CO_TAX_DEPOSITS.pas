// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_TAX_DEPOSITS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, EvContext, EvClientDataSet;

type
  TDM_CO_TAX_DEPOSITS = class(TDM_ddTable)
    CO_TAX_DEPOSITS: TCO_TAX_DEPOSITS;
    CO_TAX_DEPOSITSTAX_PAYMENT_REFERENCE_NUMBER: TStringField;
    CO_TAX_DEPOSITSSTATUS: TStringField;
    CO_TAX_DEPOSITSPayrollDescription: TStringField;
    CO_TAX_DEPOSITSPR_NBR: TIntegerField;
    CO_TAX_DEPOSITSCO_TAX_DEPOSITS_NBR: TIntegerField;
    CO_TAX_DEPOSITSCO_NBR: TIntegerField;
    CO_TAX_DEPOSITSFILLER: TStringField;
    CO_TAX_DEPOSITSSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    CO_TAX_DEPOSITSSTATUS_DATE: TDateTimeField;
    CO_TAX_DEPOSITSDEPOSIT_TYPE: TStringField;
    CO_TAX_DEPOSITSBarAmount: TCurrencyField;
    procedure CO_TAX_DEPOSITSBeforePost(DataSet: TDataSet);
    procedure CO_TAX_DEPOSITSBeforeDelete(DataSet: TDataSet);
    procedure CO_TAX_DEPOSITSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_TAX_DEPOSITS.CO_TAX_DEPOSITSBeforePost(DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('STATUS_DATE').AsDateTime, True);

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;


end;

procedure TDM_CO_TAX_DEPOSITS.CO_TAX_DEPOSITSBeforeDelete(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('STATUS_DATE').AsDateTime, True);
end;

procedure TDM_CO_TAX_DEPOSITS.CO_TAX_DEPOSITSCalcFields(DataSet: TDataSet);
var
  FillerAmount: string;

begin
  inherited;
  if not DataSet.FieldByName('FILLER').IsNull then
  begin
    FillerAmount := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 15);
    DataSet.FieldByName('BarAmount').AsCurrency := StrToCurrDef(Trim(FillerAmount), 0);
  end;

end;

initialization
  RegisterClass(TDM_CO_TAX_DEPOSITS);

finalization
  UnregisterClass(TDM_CO_TAX_DEPOSITS);

end.
