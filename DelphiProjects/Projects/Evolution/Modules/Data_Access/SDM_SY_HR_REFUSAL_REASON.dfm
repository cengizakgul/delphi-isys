inherited DM_SY_HR_REFUSAL_REASON: TDM_SY_HR_REFUSAL_REASON
  OldCreateOrder = True
  Left = 520
  Top = 200
  Height = 479
  Width = 741
  object SY_HR_REFUSAL_REASON: TSY_HR_REFUSAL_REASON
    ProviderName = 'SY_HR_REFUSAL_REASON_PROV'
    Left = 280
    Top = 104
    object SY_HR_REFUSAL_REASONSY_HR_REFUSAL_REASON_NBR: TIntegerField
      FieldName = 'SY_HR_REFUSAL_REASON_NBR'
    end
    object SY_HR_REFUSAL_REASONSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object SY_HR_REFUSAL_REASONHEALTHCARE_COVERAGE: TStringField
      FieldName = 'HEALTHCARE_COVERAGE'
      Size = 1
    end
    object SY_HR_REFUSAL_REASONStateName: TStringField
      FieldKind = fkLookup
      FieldName = 'StateName'
      LookupDataSet = DM_SY_STATES.SY_STATES
      LookupKeyFields = 'SY_STATES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_STATES_NBR'
      Lookup = True
    end
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 280
    Top = 176
  end
end
