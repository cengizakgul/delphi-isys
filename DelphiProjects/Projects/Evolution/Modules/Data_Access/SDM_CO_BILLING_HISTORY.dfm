inherited DM_CO_BILLING_HISTORY: TDM_CO_BILLING_HISTORY
  Left = 456
  Top = 161
  Height = 336
  Width = 400
  object DM_PAYROLL: TDM_PAYROLL
    Left = 64
    Top = 32
  end
  object CO_BILLING_HISTORY: TCO_BILLING_HISTORY
    ProviderName = 'CO_BILLING_HISTORY_PROV'
    BeforePost = CO_BILLING_HISTORYBeforePost
    Left = 64
    Top = 152
    object CO_BILLING_HISTORYCO_BILLING_HISTORY_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."CO_BILLING_HISTORY_NBR"'
      Required = True
    end
    object CO_BILLING_HISTORYINVOICE_NUMBER: TIntegerField
      FieldName = 'INVOICE_NUMBER'
      Origin = '"CO_BILLING_HISTORY_HIST"."INVOICE_NUMBER"'
      Required = True
    end
    object CO_BILLING_HISTORYCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."CO_NBR"'
      Required = True
    end
    object CO_BILLING_HISTORYINVOICE_DATE: TDateTimeField
      FieldName = 'INVOICE_DATE'
      Origin = '"CO_BILLING_HISTORY_HIST"."INVOICE_DATE"'
      Required = True
    end
    object CO_BILLING_HISTORYPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."PR_NBR"'
    end
    object CO_BILLING_HISTORYEXPORTED_TO_A_R: TStringField
      FieldName = 'EXPORTED_TO_A_R'
      Origin = '"CO_BILLING_HISTORY_HIST"."EXPORTED_TO_A_R"'
      Required = True
      Size = 1
    end
    object CO_BILLING_HISTORYSALES_TAX_AMOUNT: TFloatField
      FieldName = 'SALES_TAX_AMOUNT'
      Origin = '"CO_BILLING_HISTORY_HIST"."SALES_TAX_AMOUNT"'
    end
    object CO_BILLING_HISTORYCHECK_SERIAL_NUMBER: TIntegerField
      FieldName = 'CHECK_SERIAL_NUMBER'
      Origin = '"CO_BILLING_HISTORY_HIST"."CHECK_SERIAL_NUMBER"'
    end
    object CO_BILLING_HISTORYBILLING_CO_BANK_ACCOUNT_NBR: TIntegerField
      FieldName = 'BILLING_CO_BANK_ACCOUNT_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."BILLING_CO_BANK_ACCOUNT_NBR"'
    end
    object CO_BILLING_HISTORYNOTES: TBlobField
      FieldName = 'NOTES'
      Origin = '"CO_BILLING_HISTORY_HIST"."NOTES"'
      Size = 8
    end
    object CO_BILLING_HISTORYCheckDate: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'CheckDate'
      LookupDataSet = DM_PR.PR
      LookupKeyFields = 'PR_NBR'
      LookupResultField = 'CHECK_DATE'
      KeyFields = 'PR_NBR'
      Lookup = True
    end
    object CO_BILLING_HISTORYSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
      Origin = '"CO_BILLING_HISTORY_HIST"."STATUS_DATE"'
    end
    object CO_BILLING_HISTORYSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = '"CO_BILLING_HISTORY_HIST"."STATUS"'
      Required = True
      Size = 1
    end
    object CO_BILLING_HISTORYRunNumber: TIntegerField
      FieldKind = fkLookup
      FieldName = 'RunNumber'
      LookupDataSet = DM_PR.PR
      LookupKeyFields = 'PR_NBR'
      LookupResultField = 'RUN_NUMBER'
      KeyFields = 'PR_NBR'
      Lookup = True
    end
    object CO_BILLING_HISTORYCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."CO_DIVISION_NBR"'
    end
    object CO_BILLING_HISTORYCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."CO_BRANCH_NBR"'
    end
    object CO_BILLING_HISTORYCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."CO_DEPARTMENT_NBR"'
    end
    object CO_BILLING_HISTORYCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
      Origin = '"CO_BILLING_HISTORY_HIST"."CO_TEAM_NBR"'
    end
    object CO_BILLING_HISTORYFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
  end
end
