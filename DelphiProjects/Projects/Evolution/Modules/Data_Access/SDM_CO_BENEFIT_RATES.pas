unit SDM_CO_BENEFIT_RATES;

interface

uses
  SysUtils, Classes, SDM_ddTable, SDataDictClient, DB, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
   SDDClasses, EvConsts, SDataStructure, EvUtils, Variants, EvClientDataSet;

type
  TDM_CO_BENEFIT_RATES = class(TDM_ddTable)
    CO_BENEFIT_RATES: TCO_BENEFIT_RATES;
    CO_BENEFIT_RATEScTotalAmount: TFloatField;
    CO_BENEFIT_RATEScEEPortion: TFloatField;
    CO_BENEFIT_RATEScERPortion: TFloatField;
    CO_BENEFIT_RATESBENEFIT_NAME: TStringField;
    CO_BENEFIT_RATESEE_RATE: TFloatField;
    CO_BENEFIT_RATESER_RATE: TFloatField;
    CO_BENEFIT_RATESCOBRA_RATE: TFloatField;
    CO_BENEFIT_RATESCO_BENEFIT_SUBTYPE_NBR: TIntegerField;
    DM_COMPANY: TDM_COMPANY;
    CO_BENEFIT_RATESCO_BENEFIT_RATES_NBR: TIntegerField;
    CO_BENEFIT_RATESRATE_TYPE: TStringField;
    CO_BENEFIT_RATESMAX_DEPENDENTS: TSmallintField;
    CO_BENEFIT_RATESPERIOD_BEGIN: TDateField;
    CO_BENEFIT_RATESPERIOD_END: TDateField;
    CO_BENEFIT_RATESEDGroupName: TStringField;
    CO_BENEFIT_RATESCL_E_D_GROUPS_NBR: TIntegerField;
    CO_BENEFIT_RATEScPERIOD_END: TDateField;
    CO_BENEFIT_RATESCO_BENEFITS_NBR: TIntegerField;
    procedure CO_BENEFIT_RATESCalcFields(DataSet: TDataSet);
    procedure CO_BENEFIT_RATESBeforePost(DataSet: TDataSet);
  private
    FCalculatingFields: Boolean;
  public
  end;

implementation

{$R *.dfm}

procedure TDM_CO_BENEFIT_RATES.CO_BENEFIT_RATESCalcFields(DataSet: TDataSet);
begin

  if (not CO_BENEFIT_RATESPERIOD_END.IsNull) and
      (CO_BENEFIT_RATESPERIOD_END.Value <> 73051) and
       (CO_BENEFIT_RATES.State <> dsBrowse) then   //  "1/1/2100"
     CO_BENEFIT_RATEScPERIOD_END.Value  := CO_BENEFIT_RATESPERIOD_END.Value;

  if not FCalculatingFields and (CO_BENEFIT_RATES.RATE_TYPE.AsString = BENEFIT_RATES_RATE_TYPE_AMOUNT) and (CO_BENEFIT_RATES.State <> dsBrowse) then
  begin
    FCalculatingFields := True;
    try
      CO_BENEFIT_RATES['cTotalAmount'] := CO_BENEFIT_RATES.EE_RATE.AsFloat + CO_BENEFIT_RATES.ER_RATE.AsFloat;
      if CO_BENEFIT_RATES['cTotalAmount'] = 0 then
      begin
        CO_BENEFIT_RATES['cEEPortion'] := 0;
        CO_BENEFIT_RATES['cERPortion'] := 0;
      end
      else
      begin
        CO_BENEFIT_RATES['cEEPortion'] := (CO_BENEFIT_RATES.EE_RATE.AsFloat/CO_BENEFIT_RATES['cTotalAmount'])*100;
        CO_BENEFIT_RATES['cERPortion'] := (CO_BENEFIT_RATES.ER_RATE.AsFloat/CO_BENEFIT_RATES['cTotalAmount'])*100
      end;
    finally
      FCalculatingFields := False;
    end;
  end;
end;

procedure TDM_CO_BENEFIT_RATES.CO_BENEFIT_RATESBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  if (CO_BENEFIT_RATESRATE_TYPE.AsString <> BENEFIT_RATES_RATE_TYPE_PERCENT) then
    DM_COMPANY.CO_BENEFIT_RATES['CL_E_D_GROUPS_NBR'] :=Null;

  if (CO_BENEFIT_RATESPERIOD_END.Value <> CO_BENEFIT_RATEScPERIOD_END.Value) then
  begin
   if (CO_BENEFIT_RATEScPERIOD_END.IsNull) then
     CO_BENEFIT_RATESPERIOD_END.Value  := 73051
   else
     CO_BENEFIT_RATESPERIOD_END.Value  := CO_BENEFIT_RATEScPERIOD_END.Value;
  end;

end;

initialization
  RegisterClass(TDM_CO_BENEFIT_RATES);

finalization
  UnregisterClass(TDM_CO_BENEFIT_RATES);

end.
