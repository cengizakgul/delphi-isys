inherited DM_SY_RECIPROCATED_STATES: TDM_SY_RECIPROCATED_STATES
  OldCreateOrder = True
  Left = 513
  Top = 349
  Height = 202
  Width = 424
  object SY_RECIPROCATED_STATES: TSY_RECIPROCATED_STATES
    ProviderName = 'SY_RECIPROCATED_STATES_PROV'
    Left = 88
    Top = 24
    object SY_RECIPROCATED_STATESPARTICIPANT_STATE_NBR: TIntegerField
      FieldName = 'PARTICIPANT_STATE_NBR'
    end
    object SY_RECIPROCATED_STATESMAIN_STATE_NBR: TIntegerField
      FieldName = 'MAIN_STATE_NBR'
    end
    object SY_RECIPROCATED_STATESSY_RECIPROCATED_STATES_NBR: TIntegerField
      FieldName = 'SY_RECIPROCATED_STATES_NBR'
    end
    object PARTICIPANT_STATE_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'PARTICIPANT_STATE_Lookup'
      LookupDataSet = DM_SY_STATES.SY_STATES
      LookupKeyFields = 'SY_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'PARTICIPANT_STATE_NBR'
      Size = 2
      Lookup = True
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 88
    Top = 80
  end
end
