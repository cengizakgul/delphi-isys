inherited DM_CO_BENEFIT_PKG_ASMNT: TDM_CO_BENEFIT_PKG_ASMNT
  OldCreateOrder = True
  Left = 520
  Top = 200
  Height = 479
  Width = 741
  object CO_BENEFIT_PKG_ASMNT: TCO_BENEFIT_PKG_ASMNT
    ProviderName = 'CO_BENEFIT_PKG_ASMNT_PROV'
    Left = 392
    Top = 80
    object CO_BENEFIT_PKG_ASMNTCO_BENEFIT_PACKAGE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_PACKAGE_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTCO_BENEFIT_PKG_ASMNT_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_PKG_ASMNT_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTPackageName: TStringField
      FieldKind = fkLookup
      FieldName = 'PackageName'
      LookupDataSet = DM_CO_BENEFIT_PACKAGE.CO_BENEFIT_PACKAGE
      LookupKeyFields = 'CO_BENEFIT_PACKAGE_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_BENEFIT_PACKAGE_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_ASMNTCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTCO_GROUP_NBR: TIntegerField
      FieldName = 'CO_GROUP_NBR'
    end
    object CO_BENEFIT_PKG_ASMNTDivisionName: TStringField
      FieldKind = fkLookup
      FieldName = 'DivisionName'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DIVISION_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_ASMNTBranchName: TStringField
      FieldKind = fkLookup
      FieldName = 'BranchName'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_BRANCH_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_ASMNTDepartmentName: TStringField
      FieldKind = fkLookup
      FieldName = 'DepartmentName'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_ASMNTTeamName: TStringField
      FieldKind = fkLookup
      FieldName = 'TeamName'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_TEAM_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_ASMNTGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'GroupName'
      LookupDataSet = DM_CO_GROUP.CO_GROUP
      LookupKeyFields = 'CO_GROUP_NBR'
      LookupResultField = 'GROUP_NAME'
      KeyFields = 'CO_GROUP_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 240
    Top = 72
  end
end
