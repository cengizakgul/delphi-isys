// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_HR_PERSON_EDUCATION;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_CL_HR_PERSON_EDUCATION = class(TDM_ddTable)
    CL_HR_PERSON_EDUCATION: TCL_HR_PERSON_EDUCATION;
    CL_HR_PERSON_EDUCATIONCL_HR_PERSON_EDUCATION_NBR: TIntegerField;
    CL_HR_PERSON_EDUCATIONCL_PERSON_NBR: TIntegerField;
    CL_HR_PERSON_EDUCATIONDEGREE_LEVEL: TStringField;
    CL_HR_PERSON_EDUCATIONEND_DATE: TDateField;
    CL_HR_PERSON_EDUCATIONGPA: TFloatField;
    CL_HR_PERSON_EDUCATIONGRADUATION_DATE: TDateField;
    CL_HR_PERSON_EDUCATIONMAJOR: TStringField;
    CL_HR_PERSON_EDUCATIONSTART_DATE: TDateField;
    CL_HR_PERSON_EDUCATIONSchoolName: TStringField;
    CL_HR_PERSON_EDUCATIONCL_HR_SCHOOL_NBR: TIntegerField;
    DM_CLIENT: TDM_CLIENT;
    CL_HR_PERSON_EDUCATIONNOTES: TBlobField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass( TDM_CL_HR_PERSON_EDUCATION );

finalization
  UnregisterClass( TDM_CL_HR_PERSON_EDUCATION );

end.
