inherited DM_CO_SERVICES: TDM_CO_SERVICES
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object CO_SERVICES: TCO_SERVICES
    ProviderName = 'CO_SERVICES_PROV'
    PictureMasks.Strings = (
      'EFFECTIVE_START_DATE'#9'#[#]/#[#]/##[##]'#9'T'#9'F'
      'EFFECTIVE_END_DATE'#9'#[#]/#[#]/##[##]'#9'T'#9'F'
      'DISCOUNT_START_DATE'#9'#[#]/#[#]/##[##]'#9'T'#9'F')
    AfterOpen = CO_SERVICESAfterRefresh
    BeforePost = CO_SERVICESBeforePost
    OnCalcFields = CO_SERVICESCalcFields
    Left = 216
    Top = 128
    object CO_SERVICESCO_SERVICES_NBR: TIntegerField
      FieldName = 'CO_SERVICES_NBR'
      Origin = '"CO_SERVICES_HIST"."CO_SERVICES_NBR"'
      Required = True
    end
    object CO_SERVICESCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"CO_SERVICES_HIST"."CO_NBR"'
      Required = True
    end
    object CO_SERVICESSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
      Origin = '"CO_SERVICES_HIST"."SB_SERVICES_NBR"'
      Required = True
    end
    object CO_SERVICESNAME: TStringField
      FieldName = 'NAME'
      Origin = '"CO_SERVICES_HIST"."NAME"'
      Required = True
      Size = 40
    end
    object CO_SERVICESEFFECTIVE_START_DATE: TDateField
      FieldName = 'EFFECTIVE_START_DATE'
      Origin = '"CO_SERVICES_HIST"."EFFECTIVE_START_DATE"'
    end
    object CO_SERVICESEFFECTIVE_END_DATE: TDateField
      FieldName = 'EFFECTIVE_END_DATE'
      Origin = '"CO_SERVICES_HIST"."EFFECTIVE_END_DATE"'
    end
    object CO_SERVICESSALES_TAX: TStringField
      FieldName = 'SALES_TAX'
      Origin = '"CO_SERVICES_HIST"."SALES_TAX"'
      Required = True
      Size = 1
    end
    object CO_SERVICESOVERRIDE_FLAT_FEE: TFloatField
      FieldName = 'OVERRIDE_FLAT_FEE'
      Origin = '"CO_SERVICES_HIST"."OVERRIDE_FLAT_FEE"'
    end
    object CO_SERVICESDISCOUNT: TFloatField
      FieldName = 'DISCOUNT'
      Origin = '"CO_SERVICES_HIST"."DISCOUNT"'
    end
    object CO_SERVICESDISCOUNT_TYPE: TStringField
      FieldName = 'DISCOUNT_TYPE'
      Origin = '"CO_SERVICES_HIST"."DISCOUNT_TYPE"'
      Required = True
      Size = 1
    end
    object CO_SERVICESDISCOUNT_START_DATE: TDateField
      FieldName = 'DISCOUNT_START_DATE'
      Origin = '"CO_SERVICES_HIST"."DISCOUNT_START_DATE"'
    end
    object CO_SERVICESFILLER: TStringField
      FieldName = 'FILLER'
      Origin = '"CO_SERVICES_HIST"."FILLER"'
      Size = 512
    end
    object CO_SERVICESFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Origin = '"CO_SERVICES_HIST"."FREQUENCY"'
      Required = True
      Size = 1
    end
    object CO_SERVICESWEEK_NUMBER: TStringField
      FieldName = 'WEEK_NUMBER'
      Origin = '"CO_SERVICES_HIST"."WEEK_NUMBER"'
      Size = 1
    end
    object CO_SERVICESMONTH_NUMBER: TStringField
      FieldName = 'MONTH_NUMBER'
      Origin = '"CO_SERVICES_HIST"."MONTH_NUMBER"'
      Size = 2
    end
    object CO_SERVICESSort: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Sort'
      Size = 3
    end
    object CO_SERVICESDISCOUNT_END_DATE: TDateField
      FieldName = 'DISCOUNT_END_DATE'
      Origin = '"CO_SERVICES_HIST"."DISCOUNT_END_DATE"'
    end
    object CO_SERVICESCO_E_D_CODES_NBR: TIntegerField
      FieldName = 'CO_E_D_CODES_NBR'
    end
  end
end
