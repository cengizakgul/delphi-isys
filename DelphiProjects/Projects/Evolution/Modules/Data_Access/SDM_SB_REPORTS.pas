// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_REPORTS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses;

type
  TDM_SB_REPORTS = class(TDM_ddTable)
    SB_REPORTS: TSB_REPORTS;
    SB_REPORTSRWDescription: TStringField;
    SB_REPORTSSB_REPORTS_NBR: TIntegerField;
    SB_REPORTSREPORT_WRITER_REPORTS_NBR: TIntegerField;
    SB_REPORTSREPORT_LEVEL: TStringField;
    SB_REPORTSDESCRIPTION: TStringField;
    SB_REPORTSCOMMENTS: TBlobField;
    SB_REPORTSINPUT_PARAMS: TBlobField;
    SB_REPORTSNDescription: TStringField;
    procedure SB_REPORTSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_REPORTS: TDM_SB_REPORTS;

implementation

{$R *.DFM}

procedure TDM_SB_REPORTS.SB_REPORTSCalcFields(DataSet: TDataSet);
begin
  SB_REPORTSNDescription.AsString := SB_REPORTSDescription.AsString + ' (' +
                             SB_REPORTSREPORT_LEVEL.AsString +
                             SB_REPORTSREPORT_WRITER_REPORTS_NBR.AsString + ')';
end;

initialization
  RegisterClass(TDM_SB_REPORTS);

finalization
  UnregisterClass(TDM_SB_REPORTS);

end.
