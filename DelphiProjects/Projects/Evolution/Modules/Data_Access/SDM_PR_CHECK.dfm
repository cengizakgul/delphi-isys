inherited DM_PR_CHECK: TDM_PR_CHECK
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 540
  Top = 146
  Height = 479
  Width = 741
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 88
  end
  object PR_CHECK: TPR_CHECK
    ProviderName = 'PR_CHECK_PROV'
    FieldDefs = <
      item
        Name = 'PR_CHECK_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'PR_BATCH_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_PR_BANK_ACCT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PAYMENT_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SALARY'
        DataType = ftFloat
      end
      item
        Name = 'EXCLUDE_DD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_DD_EXCEPT_NET'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_TIME_OFF_ACCURAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_AUTO_DISTRIBUTION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_ALL_SCHED_E_D_CODES'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRORATE_SCHEDULED_E_DS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OR_CHECK_FEDERAL_VALUE'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_FEDERAL_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OR_CHECK_OASDI'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_MEDICARE'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_EIC'
        DataType = ftFloat
      end
      item
        Name = 'OR_CHECK_BACK_UP_WITHHOLDING'
        DataType = ftFloat
      end
      item
        Name = 'EXCLUDE_FEDERAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_ADDITIONAL_FEDERAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYEE_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYER_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYEE_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYER_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYEE_EIC'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXCLUDE_EMPLOYER_FUI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TAX_AT_SUPPLEMENTAL_RATE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TAX_FREQUENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OVERRIDE_CHECK_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'CHECK_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_CHANGE_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'NET_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'FEDERAL_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'FEDERAL_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_TAXABLE_TIPS'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EE_MEDICARE_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_MEDICARE_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EE_EIC_TAX'
        DataType = ftFloat
      end
      item
        Name = 'ER_OASDI_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_OASDI_TAXABLE_TIPS'
        DataType = ftFloat
      end
      item
        Name = 'ER_OASDI_TAX'
        DataType = ftFloat
      end
      item
        Name = 'ER_MEDICARE_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_MEDICARE_TAX'
        DataType = ftFloat
      end
      item
        Name = 'ER_FUI_TAXABLE_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_FUI_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EXCLUDE_FROM_AGENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CHECK_COMMENTS'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'FEDERAL_SHORTFALL'
        DataType = ftFloat
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'Employee_Name_Calculate'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'SOCIAL_SECURITY_NUMBER'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_DIVISION_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_BRANCH_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_DEPARTMENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_TEAM_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_COMPANY_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_DIVISION_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_BRANCH_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_DEPARTMENT_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_TEAM_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ER_FUI_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'CHECK_TYPE_945'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_OASDI_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'EE_MEDICARE_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'ER_MEDICARE_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'SortTag'
        DataType = ftInteger
      end
      item
        Name = 'SortCheckType'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CALCULATE_OVERRIDE_TAXES'
        DataType = ftString
        Size = 1
      end>
    AfterOpen = PR_CHECKAfterOpen
    BeforeInsert = PR_CHECKBeforeInsert
    AfterInsert = PR_CHECKAfterInsert
    BeforeEdit = PR_CHECKBeforeEdit
    BeforePost = PR_CHECKBeforePost
    AfterCancel = PR_CHECKAfterCancel
    BeforeDelete = PR_CHECKBeforeDelete
    AfterDelete = PR_CHECKAfterDelete
    OnCalcFields = PR_CHECKCalcFields
    OnNewRecord = PR_CHECKNewRecord
    OnDeleteError = PR_CHECKDeleteError
    DeleteChildren = True
    BlobsLoadMode = blmManualLoad
    OnPrepareLookups = PR_CHECKPrepareLookups
    OnUnPrepareLookups = PR_CHECKUnPrepareLookups
    Left = 200
    Top = 152
    object PR_CHECKPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
      Origin = '"PR_CHECK"."PR_CHECK_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object PR_CHECKEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Origin = '"PR_CHECK"."EE_NBR"'
    end
    object PR_CHECKPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Origin = '"PR_CHECK"."PR_NBR"'
      Required = True
    end
    object PR_CHECKPR_BATCH_NBR: TIntegerField
      FieldName = 'PR_BATCH_NBR'
      Origin = '"PR_CHECK"."PR_BATCH_NBR"'
      Required = True
    end
    object PR_CHECKCUSTOM_PR_BANK_ACCT_NUMBER: TStringField
      FieldName = 'CUSTOM_PR_BANK_ACCT_NUMBER'
      Origin = '"PR_CHECK"."CUSTOM_PR_BANK_ACCT_NUMBER"'
    end
    object PR_CHECKPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
      Origin = '"PR_CHECK"."PAYMENT_SERIAL_NUMBER"'
    end
    object PR_CHECKCHECK_TYPE: TStringField
      FieldName = 'CHECK_TYPE'
      Origin = '"PR_CHECK"."CHECK_TYPE"'
      Required = True
      Size = 1
    end
    object PR_CHECKSALARY: TFloatField
      FieldName = 'SALARY'
      Origin = '"PR_CHECK"."SALARY"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEXCLUDE_DD: TStringField
      FieldName = 'EXCLUDE_DD'
      Origin = '"PR_CHECK"."EXCLUDE_DD"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_DD_EXCEPT_NET: TStringField
      FieldName = 'EXCLUDE_DD_EXCEPT_NET'
      Origin = '"PR_CHECK"."EXCLUDE_DD_EXCEPT_NET"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_TIME_OFF_ACCURAL: TStringField
      FieldName = 'EXCLUDE_TIME_OFF_ACCURAL'
      Origin = '"PR_CHECK"."EXCLUDE_TIME_OFF_ACCURAL"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_AUTO_DISTRIBUTION: TStringField
      FieldName = 'EXCLUDE_AUTO_DISTRIBUTION'
      Origin = '"PR_CHECK"."EXCLUDE_AUTO_DISTRIBUTION"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_ALL_SCHED_E_D_CODES: TStringField
      FieldName = 'EXCLUDE_ALL_SCHED_E_D_CODES'
      Origin = '"PR_CHECK"."EXCLUDE_ALL_SCHED_E_D_CODES"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_SCH_E_D_FROM_AGCY_CHK: TStringField
      FieldName = 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'
      Origin = '"PR_CHECK"."EXCLUDE_SCH_E_D_FROM_AGCY_CHK"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_SCH_E_D_EXCEPT_PENSION: TStringField
      FieldName = 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'
      Origin = '"PR_CHECK"."EXCLUDE_SCH_E_D_EXCEPT_PENSION"'
      Required = True
      Size = 1
    end
    object PR_CHECKPRORATE_SCHEDULED_E_DS: TStringField
      FieldName = 'PRORATE_SCHEDULED_E_DS'
      Origin = '"PR_CHECK"."PRORATE_SCHEDULED_E_DS"'
      Required = True
      Size = 1
    end
    object PR_CHECKOR_CHECK_FEDERAL_VALUE: TFloatField
      FieldName = 'OR_CHECK_FEDERAL_VALUE'
      Origin = '"PR_CHECK"."OR_CHECK_FEDERAL_VALUE"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKOR_CHECK_FEDERAL_TYPE: TStringField
      FieldName = 'OR_CHECK_FEDERAL_TYPE'
      Origin = '"PR_CHECK"."OR_CHECK_FEDERAL_TYPE"'
      Required = True
      Size = 1
    end
    object PR_CHECKOR_CHECK_OASDI: TFloatField
      FieldName = 'OR_CHECK_OASDI'
      Origin = '"PR_CHECK"."OR_CHECK_OASDI"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKOR_CHECK_MEDICARE: TFloatField
      FieldName = 'OR_CHECK_MEDICARE'
      Origin = '"PR_CHECK"."OR_CHECK_MEDICARE"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKOR_CHECK_EIC: TFloatField
      FieldName = 'OR_CHECK_EIC'
      Origin = '"PR_CHECK"."OR_CHECK_EIC"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKOR_CHECK_BACK_UP_WITHHOLDING: TFloatField
      FieldName = 'OR_CHECK_BACK_UP_WITHHOLDING'
      Origin = '"PR_CHECK"."OR_CHECK_BACK_UP_WITHHOLDING"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEXCLUDE_FEDERAL: TStringField
      FieldName = 'EXCLUDE_FEDERAL'
      Origin = '"PR_CHECK"."EXCLUDE_FEDERAL"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_ADDITIONAL_FEDERAL: TStringField
      FieldName = 'EXCLUDE_ADDITIONAL_FEDERAL'
      Origin = '"PR_CHECK"."EXCLUDE_ADDITIONAL_FEDERAL"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYEE_OASDI: TStringField
      FieldName = 'EXCLUDE_EMPLOYEE_OASDI'
      Origin = '"PR_CHECK"."EXCLUDE_EMPLOYEE_OASDI"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYER_OASDI: TStringField
      FieldName = 'EXCLUDE_EMPLOYER_OASDI'
      Origin = '"PR_CHECK"."EXCLUDE_EMPLOYER_OASDI"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYEE_MEDICARE: TStringField
      FieldName = 'EXCLUDE_EMPLOYEE_MEDICARE'
      Origin = '"PR_CHECK"."EXCLUDE_EMPLOYEE_MEDICARE"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYER_MEDICARE: TStringField
      FieldName = 'EXCLUDE_EMPLOYER_MEDICARE'
      Origin = '"PR_CHECK"."EXCLUDE_EMPLOYER_MEDICARE"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYEE_EIC: TStringField
      FieldName = 'EXCLUDE_EMPLOYEE_EIC'
      Origin = '"PR_CHECK"."EXCLUDE_EMPLOYEE_EIC"'
      Required = True
      Size = 1
    end
    object PR_CHECKEXCLUDE_EMPLOYER_FUI: TStringField
      FieldName = 'EXCLUDE_EMPLOYER_FUI'
      Origin = '"PR_CHECK"."EXCLUDE_EMPLOYER_FUI"'
      Required = True
      Size = 1
    end
    object PR_CHECKTAX_AT_SUPPLEMENTAL_RATE: TStringField
      FieldName = 'TAX_AT_SUPPLEMENTAL_RATE'
      Origin = '"PR_CHECK"."TAX_AT_SUPPLEMENTAL_RATE"'
      Required = True
      Size = 1
    end
    object PR_CHECKTAX_FREQUENCY: TStringField
      FieldName = 'TAX_FREQUENCY'
      Origin = '"PR_CHECK"."TAX_FREQUENCY"'
      Size = 1
    end
    object PR_CHECKOVERRIDE_CHECK_NOTES: TBlobField
      FieldName = 'OVERRIDE_CHECK_NOTES'
      Origin = '"PR_CHECK"."OVERRIDE_CHECK_NOTES"'
      Size = 8
    end
    object PR_CHECKCHECK_STATUS: TStringField
      FieldName = 'CHECK_STATUS'
      Origin = '"PR_CHECK"."CHECK_STATUS"'
      Size = 1
    end
    object PR_CHECKSTATUS_CHANGE_DATE: TDateTimeField
      FieldName = 'STATUS_CHANGE_DATE'
      Origin = '"PR_CHECK"."STATUS_CHANGE_DATE"'
    end
    object PR_CHECKGROSS_WAGES: TFloatField
      FieldName = 'GROSS_WAGES'
      Origin = '"PR_CHECK"."GROSS_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKNET_WAGES: TFloatField
      FieldName = 'NET_WAGES'
      Origin = '"PR_CHECK"."NET_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKFEDERAL_TAXABLE_WAGES: TFloatField
      FieldName = 'FEDERAL_TAXABLE_WAGES'
      Origin = '"PR_CHECK"."FEDERAL_TAXABLE_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKFEDERAL_TAX: TFloatField
      FieldName = 'FEDERAL_TAX'
      Origin = '"PR_CHECK"."FEDERAL_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEE_OASDI_TAXABLE_WAGES: TFloatField
      FieldName = 'EE_OASDI_TAXABLE_WAGES'
      Origin = '"PR_CHECK"."EE_OASDI_TAXABLE_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEE_OASDI_TAXABLE_TIPS: TFloatField
      FieldName = 'EE_OASDI_TAXABLE_TIPS'
      Origin = '"PR_CHECK"."EE_OASDI_TAXABLE_TIPS"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEE_OASDI_TAX: TFloatField
      FieldName = 'EE_OASDI_TAX'
      Origin = '"PR_CHECK"."EE_OASDI_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEE_MEDICARE_TAXABLE_WAGES: TFloatField
      FieldName = 'EE_MEDICARE_TAXABLE_WAGES'
      Origin = '"PR_CHECK"."EE_MEDICARE_TAXABLE_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEE_MEDICARE_TAX: TFloatField
      FieldName = 'EE_MEDICARE_TAX'
      Origin = '"PR_CHECK"."EE_MEDICARE_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEE_EIC_TAX: TFloatField
      FieldName = 'EE_EIC_TAX'
      Origin = '"PR_CHECK"."EE_EIC_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_OASDI_TAXABLE_WAGES: TFloatField
      FieldName = 'ER_OASDI_TAXABLE_WAGES'
      Origin = '"PR_CHECK"."ER_OASDI_TAXABLE_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_OASDI_TAXABLE_TIPS: TFloatField
      FieldName = 'ER_OASDI_TAXABLE_TIPS'
      Origin = '"PR_CHECK"."ER_OASDI_TAXABLE_TIPS"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_OASDI_TAX: TFloatField
      FieldName = 'ER_OASDI_TAX'
      Origin = '"PR_CHECK"."ER_OASDI_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_MEDICARE_TAXABLE_WAGES: TFloatField
      FieldName = 'ER_MEDICARE_TAXABLE_WAGES'
      Origin = '"PR_CHECK"."ER_MEDICARE_TAXABLE_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_MEDICARE_TAX: TFloatField
      FieldName = 'ER_MEDICARE_TAX'
      Origin = '"PR_CHECK"."ER_MEDICARE_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_FUI_TAXABLE_WAGES: TFloatField
      FieldName = 'ER_FUI_TAXABLE_WAGES'
      Origin = '"PR_CHECK"."ER_FUI_TAXABLE_WAGES"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKER_FUI_TAX: TFloatField
      FieldName = 'ER_FUI_TAX'
      Origin = '"PR_CHECK"."ER_FUI_TAX"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKEXCLUDE_FROM_AGENCY: TStringField
      FieldName = 'EXCLUDE_FROM_AGENCY'
      Origin = '"PR_CHECK"."EXCLUDE_FROM_AGENCY"'
      Required = True
      Size = 1
    end
    object PR_CHECKFEDERAL_SHORTFALL: TFloatField
      FieldName = 'FEDERAL_SHORTFALL'
      Origin = '"PR_CHECK"."FEDERAL_SHORTFALL"'
      DisplayFormat = '#,##0.00'
    end
    object PR_CHECKFILLER: TStringField
      FieldName = 'FILLER'
      Origin = '"PR_CHECK"."FILLER"'
      Size = 512
    end
    object PR_CHECKEE_Custom_Number: TStringField
      FieldKind = fkLookup
      FieldName = 'EE_Custom_Number'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_EMPLOYEE_NUMBER'
      KeyFields = 'EE_NBR'
      Size = 9
      Lookup = True
    end
    object PR_CHECKEmployee_Name_Calculate: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Employee_Name_Calculate'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'Employee_Name_Calculate'
      KeyFields = 'EE_NBR'
      Size = 50
    end
    object PR_CHECKSOCIAL_SECURITY_NUMBER: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'SOCIAL_SECURITY_NUMBER'
      KeyFields = 'EE_NBR'
      Size = 10
    end
    object PR_CHECKCUSTOM_COMPANY_NUMBER: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_COMPANY_NUMBER'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'EE_NBR'
    end
    object PR_CHECKCUSTOM_DIVISION_NUMBER: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DIVISION_NUMBER'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_DIVISION_NUMBER'
      KeyFields = 'EE_NBR'
    end
    object PR_CHECKCUSTOM_BRANCH_NUMBER: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_BRANCH_NUMBER'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_BRANCH_NUMBER'
      KeyFields = 'EE_NBR'
    end
    object PR_CHECKCUSTOM_DEPARTMENT_NUMBER: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DEPARTMENT_NUMBER'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_DEPARTMENT_NUMBER'
      KeyFields = 'EE_NBR'
    end
    object PR_CHECKCUSTOM_TEAM_NUMBER: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_TEAM_NUMBER'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_TEAM_NUMBER'
      KeyFields = 'EE_NBR'
    end
    object PR_CHECKCUSTOM_COMPANY_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_COMPANY_NAME'
      Size = 40
    end
    object PR_CHECKCUSTOM_DIVISION_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DIVISION_NAME'
      Size = 40
    end
    object PR_CHECKCUSTOM_BRANCH_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_BRANCH_NAME'
      Size = 40
    end
    object PR_CHECKCUSTOM_DEPARTMENT_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_DEPARTMENT_NAME'
      Size = 40
    end
    object PR_CHECKCUSTOM_TEAM_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_TEAM_NAME'
      Size = 40
    end
    object PR_CHECKER_FUI_GROSS_WAGES: TFloatField
      FieldName = 'ER_FUI_GROSS_WAGES'
      Origin = '"PR_CHECK"."ER_FUI_GROSS_WAGES"'
    end
    object PR_CHECKCHECK_TYPE_945: TStringField
      FieldName = 'CHECK_TYPE_945'
      Origin = '"PR_CHECK"."CHECK_TYPE_945"'
      Required = True
      Size = 1
    end
    object PR_CHECKFEDERAL_GROSS_WAGES: TFloatField
      FieldName = 'FEDERAL_GROSS_WAGES'
    end
    object PR_CHECKEE_OASDI_GROSS_WAGES: TFloatField
      FieldName = 'EE_OASDI_GROSS_WAGES'
    end
    object PR_CHECKER_OASDI_GROSS_WAGES: TFloatField
      FieldName = 'ER_OASDI_GROSS_WAGES'
    end
    object PR_CHECKEE_MEDICARE_GROSS_WAGES: TFloatField
      FieldName = 'EE_MEDICARE_GROSS_WAGES'
    end
    object PR_CHECKER_MEDICARE_GROSS_WAGES: TFloatField
      FieldName = 'ER_MEDICARE_GROSS_WAGES'
    end
    object PR_CHECKSortTag: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SortTag'
    end
    object PR_CHECKSortCheckType: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SortCheckType'
      Size = 1
    end
    object PR_CHECKCALCULATE_OVERRIDE_TAXES: TStringField
      FieldName = 'CALCULATE_OVERRIDE_TAXES'
      Size = 1
    end
    object PR_CHECKDISABLE_SHORTFALLS: TStringField
      FieldName = 'DISABLE_SHORTFALLS'
      Size = 1
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 296
    Top = 88
  end
end
