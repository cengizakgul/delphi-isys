// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_WORKERS_COMP;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_CO_WORKERS_COMP = class(TDM_ddTable)
    CO_WORKERS_COMP: TCO_WORKERS_COMP;
    CO_WORKERS_COMPCO_WORKERS_COMP_NBR: TIntegerField;
    CO_WORKERS_COMPCO_NBR: TIntegerField;
    CO_WORKERS_COMPCO_STATES_NBR: TIntegerField;
    CO_WORKERS_COMPDESCRIPTION: TStringField;
    CO_WORKERS_COMPRATE: TFloatField;
    CO_WORKERS_COMPEXPERIENCE_RATING: TFloatField;
    CO_WORKERS_COMPCL_E_D_GROUPS_NBR: TIntegerField;
    CO_WORKERS_COMPCo_State_Lookup: TStringField;
    DM_COMPANY: TDM_COMPANY;
    CO_WORKERS_COMPStateName: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    CO_WORKERS_COMPSyState: TIntegerField;
    CO_WORKERS_COMPEDGroupName: TStringField;
    DM_CLIENT: TDM_CLIENT;
    CO_WORKERS_COMPGL_TAG: TStringField;
    CO_WORKERS_COMPGL_OFFSET_TAG: TStringField;
    CO_WORKERS_COMPSUBTRACT_PREMIUM: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_WORKERS_COMP);

finalization
  UnregisterClass(TDM_CO_WORKERS_COMP);

end.
