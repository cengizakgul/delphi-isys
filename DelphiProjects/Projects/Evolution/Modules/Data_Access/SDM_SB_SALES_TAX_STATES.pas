// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_SALES_TAX_STATES;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, SDDClasses;

type
  TDM_SB_SALES_TAX_STATES = class(TDM_ddTable)
    SB_SALES_TAX_STATES: TSB_SALES_TAX_STATES;
    SB_SALES_TAX_STATESSB_SALES_TAX_STATES_NBR: TIntegerField;
    SB_SALES_TAX_STATESSTATE: TStringField;
    SB_SALES_TAX_STATESSTATE_TAX_ID: TStringField;
    SB_SALES_TAX_STATESSALES_TAX_PERCENTAGE: TFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_SALES_TAX_STATES: TDM_SB_SALES_TAX_STATES;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SB_SALES_TAX_STATES);

finalization
  UnregisterClass(TDM_SB_SALES_TAX_STATES);

end.
