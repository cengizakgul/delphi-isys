// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_ENLIST_GROUPS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, SDataStructure, evUtils,
  SDataDictclient, SFieldCodeValues, EvClientDataSet;

type
  TDM_CO_ENLIST_GROUPS = class(TDM_ddTable)
    CO_ENLIST_GROUPS: TCO_ENLIST_GROUPS;
    CO_ENLIST_GROUPSSY_REPORT_GROUPS_NBR: TIntegerField;
    CO_ENLIST_GROUPSReportGroups: TStringField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    CO_ENLIST_GROUPSMEDIA_TYPE: TStringField;
    CO_ENLIST_GROUPSPROCESS_TYPE: TStringField;
    CO_ENLIST_GROUPSCO_ENLIST_GROUPS_NBR: TIntegerField;
    CO_ENLIST_GROUPSProcess: TStringField;
    CO_ENLIST_GROUPSNMediaType: TStringField;
    procedure CO_ENLIST_GROUPSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CO_ENLIST_GROUPS.CO_ENLIST_GROUPSCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  CO_ENLIST_GROUPSProcess.AsString := 'Don''t Enlist';
  if CO_ENLIST_GROUPSPROCESS_TYPE.AsString = 'A' then
  begin
    CO_ENLIST_GROUPSProcess.AsString := 'Enlist';
    CO_ENLIST_GROUPSNMediaType.AsString :=
        ReturnDescription(MediaType_ComboChoices, CO_ENLIST_GROUPSMEDIA_TYPE.AsString);
  end;

  if not DM_SYSTEM_MISC.SY_REPORT_GROUPS.Active then
      DM_SYSTEM_MISC.SY_REPORT_GROUPS.DataRequired('');

  if CO_ENLIST_GROUPSSY_REPORT_GROUPS_NBR.AsInteger > 0  then
   CO_ENLIST_GROUPSReportGroups.AsString := DM_SYSTEM_MISC.SY_REPORT_GROUPS.ShadowDataSet.CachedLookup(CO_ENLIST_GROUPSSY_REPORT_GROUPS_NBR.AsInteger,'NAME');
end;

initialization
  RegisterClass(TDM_CO_ENLIST_GROUPS);

finalization
  UnregisterClass(TDM_CO_ENLIST_GROUPS);

end.
