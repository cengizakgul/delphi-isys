// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_FED_TAX_PAYMENT_AGENCY;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_SY_FED_TAX_PAYMENT_AGENCY = class(TDM_ddTable)
    SY_FED_TAX_PAYMENT_AGENCY: TSY_FED_TAX_PAYMENT_AGENCY;
    SY_FED_TAX_PAYMENT_AGENCYNAME: TStringField;
    SY_FED_TAX_PAYMENT_AGENCYSY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField;
    SY_FED_TAX_PAYMENT_AGENCYSY_REPORTS_NBR: TIntegerField;
    SY_FED_TAX_PAYMENT_AGENCYREPORT_DESCRIPTION: TStringField;
    SY_FED_TAX_PAYMENT_AGENCYSY_GLOBAL_AGENCY_NBR: TIntegerField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_FED_TAX_PAYMENT_AGENCY);

finalization
  UnregisterClass(TDM_SY_FED_TAX_PAYMENT_AGENCY);

end.
