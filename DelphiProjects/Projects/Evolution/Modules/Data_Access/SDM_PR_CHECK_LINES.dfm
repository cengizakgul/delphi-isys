inherited DM_PR_CHECK_LINES: TDM_PR_CHECK_LINES
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 212
  Top = 221
  Height = 479
  Width = 741
  object DM_PAYROLL: TDM_PAYROLL
    Left = 36
    Top = 12
  end
  object PR_CHECK_LINES: TPR_CHECK_LINES
    ProviderName = 'PR_CHECK_LINES_PROV'
    AfterOpen = PR_CHECK_LINESAfterOpen
    BeforeClose = PR_CHECK_LINESBeforeClose
    BeforeInsert = PR_CHECK_LINESBeforeInsert
    BeforeEdit = PR_CHECK_LINESBeforeEdit
    BeforePost = PR_CHECK_LINESBeforePost
    AfterPost = PR_CHECK_LINESAfterPost
    BeforeDelete = PR_CHECK_LINESBeforeDelete
    OnCalcFields = PR_CHECK_LINESCalcFields
    AlwaysCallCalcFields = True
    AfterCommit = PR_CHECK_LINESAfterCommit
    OnPrepareLookups = PR_CHECK_LINESPrepareLookups
    Left = 40
    Top = 69
    object PR_CHECK_LINESPR_CHECK_LINES_NBR: TIntegerField
      FieldName = 'PR_CHECK_LINES_NBR'
      Origin = '"PR_CHECK_LINES"."PR_CHECK_LINES_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object PR_CHECK_LINESPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
      Origin = '"PR_CHECK_LINES"."PR_CHECK_NBR"'
      Required = True
    end
    object PR_CHECK_LINESLINE_TYPE: TStringField
      FieldName = 'LINE_TYPE'
      Origin = '"PR_CHECK_LINES"."LINE_TYPE"'
      Size = 1
    end
    object PR_CHECK_LINESLINE_ITEM_DATE: TDateField
      FieldName = 'LINE_ITEM_DATE'
      Origin = '"PR_CHECK_LINES"."LINE_ITEM_DATE"'
    end
    object PR_CHECK_LINESCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
      Origin = '"PR_CHECK_LINES"."CO_DIVISION_NBR"'
    end
    object PR_CHECK_LINESCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
      Origin = '"PR_CHECK_LINES"."CO_BRANCH_NBR"'
    end
    object PR_CHECK_LINESCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = '"PR_CHECK_LINES"."CO_DEPARTMENT_NBR"'
    end
    object PR_CHECK_LINESCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
      Origin = '"PR_CHECK_LINES"."CO_TEAM_NBR"'
    end
    object PR_CHECK_LINESCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
      Origin = '"PR_CHECK_LINES"."CO_JOBS_NBR"'
    end
    object PR_CHECK_LINESCO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"PR_CHECK_LINES"."CO_WORKERS_COMP_NBR"'
    end
    object PR_CHECK_LINESCL_AGENCY_NBR: TIntegerField
      FieldName = 'CL_AGENCY_NBR'
      Origin = '"PR_CHECK_LINES"."CL_AGENCY_NBR"'
    end
    object PR_CHECK_LINESRATE_OF_PAY: TFloatField
      FieldName = 'RATE_OF_PAY'
      Origin = '"PR_CHECK_LINES"."RATE_OF_PAY"'
      DisplayFormat = '#,##0.00####'
    end
    object PR_CHECK_LINESAGENCY_STATUS: TStringField
      FieldName = 'AGENCY_STATUS'
      Origin = '"PR_CHECK_LINES"."AGENCY_STATUS"'
      Size = 1
    end
    object PR_CHECK_LINESHOURS_OR_PIECES: TFloatField
      FieldName = 'HOURS_OR_PIECES'
      Origin = '"PR_CHECK_LINES"."HOURS_OR_PIECES"'
    end
    object PR_CHECK_LINESAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      Origin = '"PR_CHECK_LINES"."AMOUNT"'
      OnValidate = PR_CHECK_LINESAMOUNTValidate
    end
    object PR_CHECK_LINESE_D_DIFFERENTIAL_AMOUNT: TFloatField
      FieldName = 'E_D_DIFFERENTIAL_AMOUNT'
      Origin = '"PR_CHECK_LINES"."E_D_DIFFERENTIAL_AMOUNT"'
    end
    object PR_CHECK_LINESCO_SHIFTS_NBR: TIntegerField
      FieldName = 'CO_SHIFTS_NBR'
      Origin = '"PR_CHECK_LINES"."CO_SHIFTS_NBR"'
    end
    object PR_CHECK_LINESCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
      Origin = '"PR_CHECK_LINES"."CL_E_DS_NBR"'
      Required = True
      OnChange = PR_CHECK_LINESCL_E_DS_NBRChange
    end
    object PR_CHECK_LINESCL_PIECES_NBR: TIntegerField
      FieldName = 'CL_PIECES_NBR'
      Origin = '"PR_CHECK_LINES"."CL_PIECES_NBR"'
    end
    object PR_CHECK_LINESEE_SCHEDULED_E_DS_NBR: TIntegerField
      FieldName = 'EE_SCHEDULED_E_DS_NBR'
      Origin = '"PR_CHECK_LINES"."EE_SCHEDULED_E_DS_NBR"'
    end
    object PR_CHECK_LINESDEDUCTION_SHORTFALL_CARRYOVER: TFloatField
      FieldName = 'DEDUCTION_SHORTFALL_CARRYOVER'
      Origin = '"PR_CHECK_LINES"."DEDUCTION_SHORTFALL_CARRYOVER"'
    end
    object PR_CHECK_LINESFILLER: TStringField
      FieldName = 'FILLER'
      Origin = '"PR_CHECK_LINES"."FILLER"'
      Size = 512
    end
    object PR_CHECK_LINESEE_STATES_NBR: TIntegerField
      FieldName = 'EE_STATES_NBR'
      Origin = '"PR_CHECK_LINES"."EE_STATES_NBR"'
    end
    object PR_CHECK_LINESEE_SUI_STATES_NBR: TIntegerField
      FieldName = 'EE_SUI_STATES_NBR'
      Origin = '"PR_CHECK_LINES"."EE_SUI_STATES_NBR"'
    end
    object PR_CHECK_LINESRATE_NUMBER: TIntegerField
      FieldName = 'RATE_NUMBER'
      Origin = '"PR_CHECK_LINES"."RATE_NUMBER"'
    end
    object PR_CHECK_LINESPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField
      FieldName = 'PR_MISCELLANEOUS_CHECKS_NBR'
      Origin = '"PR_CHECK_LINES"."PR_MISCELLANEOUS_CHECKS_NBR"'
    end
    object PR_CHECK_LINESE_D_Code_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'E_D_Code_Lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object PR_CHECK_LINESE_D_Description_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'E_D_Description_Lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CL_E_DS_NBR'
      Size = 40
      Lookup = True
    end
    object PR_CHECK_LINESCUSTOM_TEAM_NUMBER: TStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'CO_TEAM_NBR_DESC'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_TEAM_NUMBER'
      KeyFields = 'EE_NBR'
      Calculated = True
    end
    object PR_CHECK_LINESCUSTOM_DEPARTMENT_NUMBER: TStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'CO_DEPARTMENT_NBR_DESC'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_DEPARTMENT_NUMBER'
      KeyFields = 'EE_NBR'
      Calculated = True
    end
    object PR_CHECK_LINESCUSTOM_BRANCH_NUMBER: TStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'CO_BRANCH_NBR_DESC'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_BRANCH_NUMBER'
      KeyFields = 'EE_NBR'
      Calculated = True
    end
    object PR_CHECK_LINESCUSTOM_DIVISION_NUMBER: TStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'CO_DIVISION_NBR_DESC'
      LookupDataSet = DM_EE.EE
      LookupKeyFields = 'EE_NBR'
      LookupResultField = 'CUSTOM_DIVISION_NUMBER'
      KeyFields = 'EE_NBR'
      Calculated = True
    end
    object PR_CHECK_LINESCO_JOBS_NBR_DESC: TStringField
      DisplayWidth = 15
      FieldKind = fkInternalCalc
      FieldName = 'CO_JOBS_NBR_DESC'
      Size = 40
    end
    object PR_CHECK_LINESSTATE_LOOKUP: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'STATE_LOOKUP'
      Size = 50
    end
    object PR_CHECK_LINESSUI_STATE_LOOKUP: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SUI_STATE_LOOKUP'
    end
    object PR_CHECK_LINESCL_AGENCY_LOOKUP: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CL_AGENCY_LOOKUP'
      Size = 50
    end
    object PR_CHECK_LINESCO_WORKERS_COMP_LOOKUP: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CO_WORKERS_COMP_LOOKUP'
      Size = 50
    end
    object PR_CHECK_LINESCO_SHIFTS_LOOKUP: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CO_SHIFTS_LOOKUP'
      Size = 50
    end
    object PR_CHECK_LINESCL_PIECES_LOOKUP: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CL_PIECES_LOOKUP'
      Size = 50
    end
    object PR_CHECK_LINESSorting: TStringField
      Alignment = taRightJustify
      FieldKind = fkInternalCalc
      FieldName = 'Sorting1'
      Size = 1
    end
    object PR_CHECK_LINESSorting2: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Sorting2'
    end
    object PR_CHECK_LINESPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_CHECK_LINESLINE_ITEM_END_DATE: TDateField
      FieldName = 'LINE_ITEM_END_DATE'
    end
    object PR_CHECK_LINESUSER_OVERRIDE: TStringField
      FieldName = 'USER_OVERRIDE'
      Size = 1
    end
    object PR_CHECK_LINESLineTypeSort: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'LineTypeSort'
      Size = 1
    end
    object PR_CHECK_LINESPUNCH_IN: TDateTimeField
      FieldName = 'PUNCH_IN'
    end
    object PR_CHECK_LINESPUNCH_OUT: TDateTimeField
      FieldName = 'PUNCH_OUT'
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 108
    Top = 12
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 174
    Top = 45
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 177
    Top = 102
  end
end
