// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_MAIL_BOX_GROUP;

interface

uses
  SDM_ddTable, SDataDictClient,
  SysUtils, Classes, DB,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses, SDataDictbureau;

type
  TDM_CL_MAIL_BOX_GROUP = class(TDM_ddTable)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    CL_MAIL_BOX_GROUP: TCL_MAIL_BOX_GROUP;
    CL_MAIL_BOX_GROUPCL_MAIL_BOX_GROUP_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPDESCRIPTION: TStringField;
    CL_MAIL_BOX_GROUPREQUIRED: TStringField;
    CL_MAIL_BOX_GROUPNOTIFICATION_EMAIL: TStringField;
    CL_MAIL_BOX_GROUPSB_COVER_LETTER_REPORT_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPSY_COVER_LETTER_REPORT_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPSEC_METHOD_ACTIVE_FROM: TDateField;
    CL_MAIL_BOX_GROUPSEC_METHOD_ACTIVE_TO: TDateField;
    CL_MAIL_BOX_GROUPPRIM_SB_DELIVERY_METHOD_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPSEC_SB_DELIVERY_METHOD_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPPRIM_SB_MEDIA_TYPE_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPSEC_SB_MEDIA_TYPE_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPUP_LEVEL_MAIL_BOX_GROUP_NBR: TIntegerField;
    CL_MAIL_BOX_GROUPAUTO_RELEASE_TYPE: TStringField;
    CL_MAIL_BOX_GROUPluMethodName: TStringField;
    CL_MAIL_BOX_GROUPluMediaName: TStringField;
    CL_MAIL_BOX_GROUPluServiceName: TStringField;
    CL_MAIL_BOX_GROUPNOTE: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_CL_MAIL_BOX_GROUP);

finalization
  UnregisterClass(TDM_CL_MAIL_BOX_GROUP);

end.
