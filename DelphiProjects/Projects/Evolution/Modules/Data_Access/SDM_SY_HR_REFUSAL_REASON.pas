unit SDM_SY_HR_REFUSAL_REASON;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils;

type
  TDM_SY_HR_REFUSAL_REASON = class(TDM_ddTable)
    SY_HR_REFUSAL_REASON: TSY_HR_REFUSAL_REASON;
    DM_SYSTEM_HR: TDM_SYSTEM_HR;
    SY_HR_REFUSAL_REASONSY_HR_REFUSAL_REASON_NBR: TIntegerField;
    SY_HR_REFUSAL_REASONSY_STATES_NBR: TIntegerField;
    SY_HR_REFUSAL_REASONHEALTHCARE_COVERAGE: TStringField;
    SY_HR_REFUSAL_REASONStateName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SY_HR_REFUSAL_REASON: TDM_SY_HR_REFUSAL_REASON;

implementation

{$R *.DFM}



initialization
  RegisterClass(TDM_SY_HR_REFUSAL_REASON);

finalization
  UnregisterClass(TDM_SY_HR_REFUSAL_REASON);

end.
