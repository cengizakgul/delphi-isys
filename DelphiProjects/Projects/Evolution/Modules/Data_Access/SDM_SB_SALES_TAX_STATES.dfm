inherited DM_SB_SALES_TAX_STATES: TDM_SB_SALES_TAX_STATES
  OldCreateOrder = True
  Left = 446
  Top = 142
  Height = 480
  Width = 696
  object SB_SALES_TAX_STATES: TSB_SALES_TAX_STATES
    ProviderName = 'SB_SALES_TAX_STATES_PROV'
    PictureMasks.Strings = (
      
        'SALES_TAX_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    Left = 84
    Top = 104
    object SB_SALES_TAX_STATESSB_SALES_TAX_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_SALES_TAX_STATES_NBR'
    end
    object SB_SALES_TAX_STATESSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      FixedChar = True
      Size = 2
    end
    object SB_SALES_TAX_STATESSTATE_TAX_ID: TStringField
      DisplayWidth = 19
      FieldName = 'STATE_TAX_ID'
      Size = 19
    end
    object SB_SALES_TAX_STATESSALES_TAX_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'SALES_TAX_PERCENTAGE'
      DisplayFormat = '#,##0.00####'
    end
  end
end
