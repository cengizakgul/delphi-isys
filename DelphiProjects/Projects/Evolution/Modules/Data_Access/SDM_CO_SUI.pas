// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_SUI;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  EvUtils, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, SDataDictsystem, ISDataAccessComponents,
  EvDataAccessComponents, EvUIUtils, EvClientDataSet;

type
  TDM_CO_SUI = class(TDM_ddTable)
    CO_SUI: TCO_SUI;
    CO_SUIDESCRIPTION: TStringField;
    CO_SUICO_SUI_NBR: TIntegerField;
    CO_SUICO_NBR: TIntegerField;
    CO_SUICO_STATES_NBR: TIntegerField;
    CO_SUISY_SUI_NBR: TIntegerField;
    CO_SUITAX_RETURN_CODE: TStringField;
    CO_SUIRATE: TFloatField;
    CO_SUIPAYMENT_METHOD: TStringField;
    CO_SUIFILLER: TStringField;
    CO_SUIState: TStringField;
    CO_SUIFINAL_TAX_RETURN: TStringField;
    DM_COMPANY: TDM_COMPANY;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    CO_SUIGL_TAG: TStringField;
    CO_SUISUI_ACTIVE: TStringField;
    CO_SUIAPPLIED_FOR: TStringField;
    CO_SUIGL_OFFSET_TAG: TStringField;
    CO_SUISUI_Name: TStringField;
    CO_SUIEE_Or_ER: TStringField;
    procedure CO_SUINewRecord(DataSet: TDataSet);
    procedure CO_SUISY_SUI_NBRChange(Sender: TField);
    procedure CO_SUIBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
 EvConsts;
{$R *.DFM}

procedure TDM_CO_SUI.CO_SUINewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('CO_STATES_NBR').Value := DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').Value;
  DataSet.FieldByName('PAYMENT_METHOD').Value := TAX_PAYMENT_METHOD_CHECK;
  DataSet.FieldByName('RATE').AsFloat := 0;
end;

procedure TDM_CO_SUI.CO_SUISY_SUI_NBRChange(Sender: TField);
begin
  if (CO_SUI.State = dsInsert) and DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', Sender.Value, [] ) then
  begin
    CO_SUI['DESCRIPTION'] := DM_SYSTEM_STATE.SY_SUI['SUI_TAX_NAME'];
    CO_SUI['RATE'] := DM_SYSTEM_STATE.SY_SUI['NEW_COMPANY_DEFAULT_RATE'];
    if CO_SUI.FieldByName('RATE').IsNull then
      CO_SUI['RATE'] := 0;
  end;
end;

procedure TDM_CO_SUI.CO_SUIBeforePost(DataSet: TDataSet);
begin
  inherited;
    if CO_SUI.FieldByName('RATE').IsNull then
      CO_SUI['RATE'] := 0;
end;

initialization
  RegisterClass(TDM_CO_SUI);

finalization
  UnregisterClass(TDM_CO_SUI);

end.
