unit SDM_CO_BENEFIT_STATES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils;

type
  TDM_CO_BENEFIT_STATES = class(TDM_ddTable)
    CO_BENEFIT_STATES: TCO_BENEFIT_STATES;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    CO_BENEFIT_STATESCO_BENEFIT_STATES_NBR: TIntegerField;
    CO_BENEFIT_STATESSY_STATES_NBR: TIntegerField;
    CO_BENEFIT_STATESCO_BENEFITS_NBR: TIntegerField;
    CO_BENEFIT_STATESstate: TStringField;
    CO_BENEFIT_STATESname: TStringField;
    procedure CO_BENEFIT_STATESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BENEFIT_STATES: TDM_CO_BENEFIT_STATES;

implementation

{$R *.DFM}


procedure TDM_CO_BENEFIT_STATES.CO_BENEFIT_STATESCalcFields(
  DataSet: TDataSet);
begin
  inherited;
   if not DM_SYSTEM_STATE.SY_STATES.Active then
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');

   if (CO_BENEFIT_STATESSY_STATES_NBR.AsInteger > 0) and
      (DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR',CO_BENEFIT_STATESSY_STATES_NBR.AsInteger, [])) then
   begin
     CO_BENEFIT_STATESstate.AsString := DM_SYSTEM_STATE.SY_STATES.FieldByName('STATE').AsString;
     CO_BENEFIT_STATESname.AsString  := DM_SYSTEM_STATE.SY_STATES.FieldByName('NAME').AsString;
   end;

end;

initialization
  RegisterClass(TDM_CO_BENEFIT_STATES);

finalization
  UnregisterClass(TDM_CO_BENEFIT_STATES);

end.
