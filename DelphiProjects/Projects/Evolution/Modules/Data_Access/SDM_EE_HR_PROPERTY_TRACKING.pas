// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_HR_PROPERTY_TRACKING;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_EE_HR_PROPERTY_TRACKING = class(TDM_ddTable)
    EE_HR_PROPERTY_TRACKING: TEE_HR_PROPERTY_TRACKING;
    EE_HR_PROPERTY_TRACKINGCO_HR_PROPERTY_NBR: TIntegerField;
    EE_HR_PROPERTY_TRACKINGEE_HR_PROPERTY_TRACKING_NBR: TIntegerField;
    EE_HR_PROPERTY_TRACKINGEE_NBR: TIntegerField;
    EE_HR_PROPERTY_TRACKINGISSUED_DATE: TDateField;
    EE_HR_PROPERTY_TRACKINGLOST_DATE: TDateField;
    EE_HR_PROPERTY_TRACKINGRENEWAL_DATE: TDateField;
    EE_HR_PROPERTY_TRACKINGRETURNED_DATE: TDateField;
    EE_HR_PROPERTY_TRACKINGProperty: TStringField;
    DM_COMPANY: TDM_COMPANY;
  private
    { Private declarations }
  public
    { Public declarations }
  end;



implementation

{$R *.DFM}

initialization
  RegisterClass( TDM_EE_HR_PROPERTY_TRACKING );

finalization
  UnregisterClass( TDM_EE_HR_PROPERTY_TRACKING );

end.
