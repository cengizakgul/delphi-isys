inherited DM_EE_TIME_OFF_ACCRUAL: TDM_EE_TIME_OFF_ACCRUAL
  OldCreateOrder = True
  Left = 504
  Top = 236
  Height = 235
  Width = 322
  object EE_TIME_OFF_ACCRUAL: TEE_TIME_OFF_ACCRUAL
    ProviderName = 'EE_TIME_OFF_ACCRUAL_PROV'
    AfterInsert = EE_TIME_OFF_ACCRUALAfterInsert
    BeforePost = EE_TIME_OFF_ACCRUALBeforePost
    DeleteChildren = True
    Left = 63
    Top = 30
    object EE_TIME_OFF_ACCRUALEE_TIME_OFF_ACCRUAL_NBR: TIntegerField
      FieldName = 'EE_TIME_OFF_ACCRUAL_NBR'
      Origin = '"EE_TIME_OFF_ACCRUAL"."EE_TIME_OFF_ACCRUAL_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object EE_TIME_OFF_ACCRUALEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Origin = '"EE_TIME_OFF_ACCRUAL"."EE_NBR"'
      Required = True
    end
    object EE_TIME_OFF_ACCRUALCO_TIME_OFF_ACCRUAL_NBR: TIntegerField
      FieldName = 'CO_TIME_OFF_ACCRUAL_NBR'
      Origin = '"EE_TIME_OFF_ACCRUAL"."CO_TIME_OFF_ACCRUAL_NBR"'
      Required = True
    end
    object EE_TIME_OFF_ACCRUALCURRENT_ACCRUED: TFloatField
      FieldName = 'CURRENT_ACCRUED'
      Origin = '"EE_TIME_OFF_ACCRUAL"."CURRENT_ACCRUED"'
      DisplayFormat = '#,##0.00####'
    end
    object EE_TIME_OFF_ACCRUALCURRENT_USED: TFloatField
      FieldName = 'CURRENT_USED'
      Origin = '"EE_TIME_OFF_ACCRUAL"."CURRENT_USED"'
      DisplayFormat = '#,##0.00####'
    end
    object EE_TIME_OFF_ACCRUALEFFECTIVE_ACCRUAL_DATE: TDateField
      FieldName = 'EFFECTIVE_ACCRUAL_DATE'
      Origin = '"EE_TIME_OFF_ACCRUAL"."EFFECTIVE_ACCRUAL_DATE"'
    end
    object EE_TIME_OFF_ACCRUALNEXT_ACCRUE_DATE: TDateField
      FieldName = 'NEXT_ACCRUE_DATE'
      Origin = '"EE_TIME_OFF_ACCRUAL"."NEXT_ACCRUE_DATE"'
    end
    object EE_TIME_OFF_ACCRUALANNUAL_ACCRUAL_MAXIMUM: TFloatField
      FieldName = 'ANNUAL_ACCRUAL_MAXIMUM'
      Origin = '"EE_TIME_OFF_ACCRUAL"."ANNUAL_ACCRUAL_MAXIMUM"'
    end
    object EE_TIME_OFF_ACCRUALOVERRIDE_RATE: TFloatField
      FieldName = 'OVERRIDE_RATE'
      Origin = '"EE_TIME_OFF_ACCRUAL"."OVERRIDE_RATE"'
      DisplayFormat = '#,##0.00####'
    end
    object EE_TIME_OFF_ACCRUALRLLOVR_CO_TIME_OFF_ACCRUAL_NBR: TIntegerField
      FieldName = 'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR'
      Origin = '"EE_TIME_OFF_ACCRUAL"."RLLOVR_CO_TIME_OFF_ACCRUAL_NBR"'
    end
    object EE_TIME_OFF_ACCRUALROLLOVER_DATE: TDateField
      FieldName = 'ROLLOVER_DATE'
      Origin = '"EE_TIME_OFF_ACCRUAL"."ROLLOVER_DATE"'
    end
    object EE_TIME_OFF_ACCRUALNEXT_RESET_DATE: TDateField
      FieldName = 'NEXT_RESET_DATE'
      Origin = '"EE_TIME_OFF_ACCRUAL"."NEXT_RESET_DATE"'
    end
    object EE_TIME_OFF_ACCRUALDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'Description'
      LookupDataSet = DM_CO_TIME_OFF_ACCRUAL.CO_TIME_OFF_ACCRUAL
      LookupKeyFields = 'CO_TIME_OFF_ACCRUAL_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_TIME_OFF_ACCRUAL_NBR'
      Size = 40
      Lookup = True
    end
    object EE_TIME_OFF_ACCRUALJUST_RESET: TStringField
      FieldName = 'JUST_RESET'
      Origin = '"EE_TIME_OFF_ACCRUAL"."JUST_RESET"'
      Required = True
      Size = 1
    end
    object EE_TIME_OFF_ACCRUALMARKED_ACCRUED: TFloatField
      FieldName = 'MARKED_ACCRUED'
    end
    object EE_TIME_OFF_ACCRUALMARKED_USED: TFloatField
      FieldName = 'MARKED_USED'
    end
    object EE_TIME_OFF_ACCRUALSTATUS: TStringField
      FieldName = 'STATUS'
      FixedChar = True
      Size = 1
    end
    object EE_TIME_OFF_ACCRUALACCRUED: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'ACCRUED'
    end
    object EE_TIME_OFF_ACCRUALUSED: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'USED'
    end
    object EE_TIME_OFF_ACCRUALBALANCE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'BALANCE'
    end
    object EE_TIME_OFF_ACCRUALREASON: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'REASON'
      Size = 40
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 168
    Top = 32
  end
end
