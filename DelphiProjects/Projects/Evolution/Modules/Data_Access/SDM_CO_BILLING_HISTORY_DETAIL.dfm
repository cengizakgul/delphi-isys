inherited DM_CO_BILLING_HISTORY_DETAIL: TDM_CO_BILLING_HISTORY_DETAIL
  OldCreateOrder = False
  Left = 365
  Top = 265
  Height = 300
  Width = 389
  object CO_BILLING_HISTORY_DETAIL: TCO_BILLING_HISTORY_DETAIL
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_BILLING_HISTORY_DETAIL_PROV'
    OnCalcFields = CO_BILLING_HISTORY_DETAILCalcFields
    ValidateWithMask = True
    Left = 81
    Top = 28
    object CO_BILLING_HISTORY_DETAILCO_BILLING_HISTORY_DETAIL_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_DETAIL_NBR'
    end
    object CO_BILLING_HISTORY_DETAILCO_BILLING_HISTORY_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_NBR'
    end
    object CO_BILLING_HISTORY_DETAILCO_SERVICES_NBR: TIntegerField
      FieldName = 'CO_SERVICES_NBR'
    end
    object CO_BILLING_HISTORY_DETAILQUANTITY: TFloatField
      FieldName = 'QUANTITY'
      DisplayFormat = '#,##0.00'
    end
    object CO_BILLING_HISTORY_DETAILAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object CO_BILLING_HISTORY_DETAILTAX: TFloatField
      FieldName = 'TAX'
      DisplayFormat = '#,##0.00'
    end
    object CO_BILLING_HISTORY_DETAILDISCOUNT: TFloatField
      FieldName = 'DISCOUNT'
      DisplayFormat = '#,##0.00'
    end
    object CO_BILLING_HISTORY_DETAILNAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NAME'
      LookupDataSet = DM_CO_SERVICES.CO_SERVICES
      LookupKeyFields = 'CO_SERVICES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_SERVICES_NBR'
      Size = 40
    end
    object CO_BILLING_HISTORY_DETAILSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
    end
    object CO_BILLING_HISTORY_DETAILCO_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'CO_NAME'
      Size = 40
    end
    object CO_BILLING_HISTORY_DETAILSB_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SB_NAME'
      Size = 40
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 84
    Top = 90
  end
end
