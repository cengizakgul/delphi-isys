// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_BENEFICIARY;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, evUtils, Variants,
  SFieldCodeValues, EvClientDataSet;

type
  TDM_EE_BENEFICIARY = class(TDM_ddTable)
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EE_BENEFICIARY: TEE_BENEFICIARY;
    DM_COMPANY: TDM_COMPANY;
    EE_BENEFICIARYEE_BENEFICIARY_NBR: TIntegerField;
    EE_BENEFICIARYEE_BENEFITS_NBR: TIntegerField;
    EE_BENEFICIARYCL_PERSON_DEPENDENTS_NBR: TIntegerField;
    EE_BENEFICIARYPERCENTAGE: TSmallintField;
    EE_BENEFICIARYPRIMARY_BENEFICIARY: TStringField;
    EE_BENEFICIARYBENEFIT_NAME: TStringField;
    EE_BENEFICIARYCATEGORY_NAME: TStringField;
    EE_BENEFICIARYSTART_DATE: TDateTimeField;
    EE_BENEFICIARYFIRST_NAME: TStringField;
    EE_BENEFICIARYLAST_NAME: TStringField;
    EE_BENEFICIARYRELATION_TYPE: TStringField;
    EE_BENEFICIARYDATE_OF_BIRTH: TDateTimeField;
    EE_BENEFICIARYSOCIAL_SECURITY_NUMBER: TStringField;
    EE_BENEFICIARYPCP: TStringField;
    procedure EE_BENEFICIARYCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

procedure TDM_EE_BENEFICIARY.EE_BENEFICIARYCalcFields(DataSet: TDataSet);
begin
  inherited;

   if not DM_COMPANY.EE_BENEFITS.Active then
      DM_COMPANY.EE_BENEFITS.DataRequired('ALL');
   if not DM_COMPANY.CO_BENEFITS.Active then
      DM_COMPANY.CO_BENEFITS.DataRequired('ALL');
   if not DM_COMPANY.CO_BENEFIT_CATEGORY.Active then
      DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('ALL');

    if (EE_BENEFICIARY.EE_BENEFITS_NBR.AsInteger > 0)  then
      if DM_COMPANY.EE_BENEFITS.Locate('EE_BENEFITS_NBR', EE_BENEFICIARY.EE_BENEFITS_NBR.Value, []) then
        if DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', DM_COMPANY.EE_BENEFITS.fieldByName('CO_BENEFITS_NBR').Value, []) then
        begin
          EE_BENEFICIARYBENEFIT_NAME.AsString := DM_COMPANY.CO_BENEFITS.fieldByName('BENEFIT_NAME').AsString;
          EE_BENEFICIARYSTART_DATE.AsDateTime := DM_COMPANY.EE_BENEFITS.fieldByName('BENEFIT_EFFECTIVE_DATE').AsDateTime;

          if DM_COMPANY.CO_BENEFIT_CATEGORY.Locate('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS.FieldByName('CO_BENEFIT_CATEGORY_NBR').Value, []) then
          begin
             EE_BENEFICIARYCATEGORY_NAME.AsString :=
                ReturnDescription(Benefit_Category_Type_ComboChoices, DM_COMPANY.CO_BENEFIT_CATEGORY.fieldByName('CATEGORY_TYPE').AsString);

          end;
        end;
end;

initialization
  RegisterClass( TDM_EE_BENEFICIARY );

finalization
  UnregisterClass( TDM_EE_BENEFICIARY );

end.
