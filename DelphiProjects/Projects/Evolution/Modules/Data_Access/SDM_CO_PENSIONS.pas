// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_PENSIONS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_PENSIONS = class(TDM_ddTable)
    DM_CLIENT: TDM_CLIENT;
    CO_PENSIONS: TCO_PENSIONS;
    CO_PENSIONSCO_PENSIONS_NBR: TIntegerField;
    CO_PENSIONSCO_NBR: TIntegerField;
    CO_PENSIONSCL_PENSION_NBR: TIntegerField;
    CO_PENSIONSFILLER: TStringField;
    CO_PENSIONSPensionName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_PENSIONS: TDM_CO_PENSIONS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_PENSIONS);

finalization
  UnregisterClass(TDM_CO_PENSIONS);

end.
