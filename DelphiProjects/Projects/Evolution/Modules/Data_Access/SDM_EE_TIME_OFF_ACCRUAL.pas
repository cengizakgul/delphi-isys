// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_TIME_OFF_ACCRUAL;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, isVCLBugFix, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_EE_TIME_OFF_ACCRUAL = class(TDM_ddTable)
    EE_TIME_OFF_ACCRUAL: TEE_TIME_OFF_ACCRUAL;
    EE_TIME_OFF_ACCRUALEE_TIME_OFF_ACCRUAL_NBR: TIntegerField;
    EE_TIME_OFF_ACCRUALEE_NBR: TIntegerField;
    EE_TIME_OFF_ACCRUALCO_TIME_OFF_ACCRUAL_NBR: TIntegerField;
    EE_TIME_OFF_ACCRUALCURRENT_ACCRUED: TFloatField;
    EE_TIME_OFF_ACCRUALCURRENT_USED: TFloatField;
    EE_TIME_OFF_ACCRUALEFFECTIVE_ACCRUAL_DATE: TDateField;
    EE_TIME_OFF_ACCRUALNEXT_ACCRUE_DATE: TDateField;
    EE_TIME_OFF_ACCRUALANNUAL_ACCRUAL_MAXIMUM: TFloatField;
    EE_TIME_OFF_ACCRUALOVERRIDE_RATE: TFloatField;
    EE_TIME_OFF_ACCRUALRLLOVR_CO_TIME_OFF_ACCRUAL_NBR: TIntegerField;
    EE_TIME_OFF_ACCRUALROLLOVER_DATE: TDateField;
    EE_TIME_OFF_ACCRUALNEXT_RESET_DATE: TDateField;
    DM_COMPANY: TDM_COMPANY;
    EE_TIME_OFF_ACCRUALDescription: TStringField;
    EE_TIME_OFF_ACCRUALJUST_RESET: TStringField;
    EE_TIME_OFF_ACCRUALMARKED_ACCRUED: TFloatField;
    EE_TIME_OFF_ACCRUALMARKED_USED: TFloatField;
    EE_TIME_OFF_ACCRUALSTATUS: TStringField;
    EE_TIME_OFF_ACCRUALACCRUED: TFloatField;
    EE_TIME_OFF_ACCRUALUSED: TFloatField;
    EE_TIME_OFF_ACCRUALBALANCE: TFloatField;
    EE_TIME_OFF_ACCRUALREASON: TStringField;
    procedure EE_TIME_OFF_ACCRUALBeforePost(DataSet: TDataSet);
    procedure EE_TIME_OFF_ACCRUALAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
    FAccruedAdjReason: string;
    FUsedAdjReason: string;
  public
    { Public declarations }
  end;

var
  DM_EE_TIME_OFF_ACCRUAL: TDM_EE_TIME_OFF_ACCRUAL;

implementation

{$R *.DFM}

uses EvUIUtils, EvConsts, Math, Variants;

procedure TDM_EE_TIME_OFF_ACCRUAL.EE_TIME_OFF_ACCRUALBeforePost(
  DataSet: TDataSet);
  function CallEvDialog(const Field: string; var Value: string): Boolean;
  begin
    Value := VarToStr(DataSet['REASON']);
    if Value <> '' then
      Result := True
    else
    repeat
      Result := EvDialog(
      'You are about to adjust '+ Field+ ' value',
      'Please provide reason for the adjustment and press Ok or press Cancel if you do not wish to make the adjustment.'#13#13+
      'Adjustment reason (28 char max):', Value);
    until (Length(Value) > 0) or not Result;
  end;
begin
  if DataSet.Tag = 0 then
  try
    DataSet.Tag := 1;
    if ((DataSet.State = dsInsert) and (DataSet.FieldByName('ACCRUED').NewValue <> 0))
    or ((DataSet.State = dsEdit) and (DataSet.FieldByName('ACCRUED').NewValue <> DataSet.FieldByName('ACCRUED').OldValue)) then
      if CallEvDialog('ACCRUED', FAccruedAdjReason) then
      begin
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Append;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['ACCRUAL_DATE'] := Date;
        if DataSet.State = dsInsert then
          DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['ACCRUED'] := DataSet.FieldByName('ACCRUED').NewValue
        else
          DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['ACCRUED'] := DataSet.FieldByName('ACCRUED').NewValue- DataSet.FieldByName('ACCRUED').OldValue;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['NOTE'] := 'Manual Adj: '+ FAccruedAdjReason;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['OPERATION_CODE'] := EE_TOA_OPER_CODE_MANUAL_ADJ;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
      end
      else
        AbortEx;
    if ((DataSet.State = dsInsert) and (DataSet.FieldByName('USED').NewValue <> 0))
    or ((DataSet.State = dsEdit) and (DataSet.FieldByName('USED').NewValue <> DataSet.FieldByName('USED').OldValue)) then
      if CallEvDialog('USED', FUsedAdjReason) then
      begin
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Append;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['ACCRUAL_DATE'] := Date;
        if DataSet.State = dsInsert then
          DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['USED'] := DataSet.FieldByName('USED').NewValue
        else
          DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['USED'] := DataSet.FieldByName('USED').NewValue- DataSet.FieldByName('USED').OldValue;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['NOTE'] := 'Manual Adj: '+ FUsedAdjReason;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['OPERATION_CODE'] := EE_TOA_OPER_CODE_MANUAL_ADJ;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
      end
      else
        AbortEx;
  finally
    DataSet.Tag := 0;
  end;
end;

procedure TDM_EE_TIME_OFF_ACCRUAL.EE_TIME_OFF_ACCRUALAfterInsert(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('CURRENT_ACCRUED').Value := 0;
  DataSet.FieldByName('CURRENT_USED').Value := 0;
  DataSet.FieldByName('ACCRUED').Value := 0;
  DataSet.FieldByName('USED').Value := 0;
  DataSet.FieldByName('BALANCE').Value := 0;
  DataSet['MARKED_ACCRUED'] := 0;
  DataSet['MARKED_USED'] := 0;
end;

initialization
  RegisterClass(TDM_EE_TIME_OFF_ACCRUAL);

finalization
  UnregisterClass(TDM_EE_TIME_OFF_ACCRUAL);

end.
