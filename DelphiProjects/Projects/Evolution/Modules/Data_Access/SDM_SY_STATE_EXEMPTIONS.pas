// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_STATE_EXEMPTIONS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   SDataAccessCommon, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses;

type
  TDM_SY_STATE_EXEMPTIONS = class(TDM_ddTable)
    SY_STATE_EXEMPTIONS: TSY_STATE_EXEMPTIONS;
    SY_STATE_EXEMPTIONSEXEMPT_EMPLOYEE_SDI: TStringField;
    SY_STATE_EXEMPTIONSEXEMPT_EMPLOYEE_SUI: TStringField;
    SY_STATE_EXEMPTIONSEXEMPT_EMPLOYER_SDI: TStringField;
    SY_STATE_EXEMPTIONSEXEMPT_EMPLOYER_SUI: TStringField;
    SY_STATE_EXEMPTIONSEXEMPT_STATE: TStringField;
    SY_STATE_EXEMPTIONSE_D_CODE_TYPE: TStringField;
    SY_STATE_EXEMPTIONSSY_STATES_NBR: TIntegerField;
    SY_STATE_EXEMPTIONSSY_STATE_EXEMPTIONS_NBR: TIntegerField;
    SY_STATE_EXEMPTIONSEDDescription: TStringField;
    procedure SY_STATE_EXEMPTIONSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TDM_SY_STATE_EXEMPTIONS.SY_STATE_EXEMPTIONSCalcFields(
  DataSet: TDataSet);
begin
  DataSet['EDDescription'] := GetEDTypeDescription(DataSet.FieldByName('E_D_CODE_TYPE').AsString);
end;

initialization
  RegisterClass(TDM_SY_STATE_EXEMPTIONS);

finalization
  UnregisterClass(TDM_SY_STATE_EXEMPTIONS);

end.
