// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_BRANCH;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Wwdatsrc, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, 
  SDDClasses, Variants, EvContext, EvUIComponents, EvClientDataSet,
  EvDataAccessComponents;

type
  TDM_CO_BRANCH = class(TDM_ddTable)
    CO_BRANCH: TCO_BRANCH;
    CO_BRANCHCO_BRANCH_NBR: TIntegerField;
    CO_BRANCHCO_DIVISION_NBR: TIntegerField;
    CO_BRANCHCUSTOM_BRANCH_NUMBER: TStringField;
    CO_BRANCHNAME: TStringField;
    CO_BRANCHADDRESS1: TStringField;
    CO_BRANCHADDRESS2: TStringField;
    CO_BRANCHCITY: TStringField;
    CO_BRANCHSTATE: TStringField;
    CO_BRANCHZIP_CODE: TStringField;
    CO_BRANCHCONTACT1: TStringField;
    CO_BRANCHPHONE1: TStringField;
    CO_BRANCHDESCRIPTION1: TStringField;
    CO_BRANCHCONTACT2: TStringField;
    CO_BRANCHPHONE2: TStringField;
    CO_BRANCHDESCRIPTION2: TStringField;
    CO_BRANCHFAX: TStringField;
    CO_BRANCHFAX_DESCRIPTION: TStringField;
    CO_BRANCHE_MAIL_ADDRESS: TStringField;
    CO_BRANCHHOME_CO_STATES_NBR: TIntegerField;
    CO_BRANCHHOME_STATE_TYPE: TStringField;
    CO_BRANCHCL_TIMECLOCK_IMPORTS_NBR: TIntegerField;
    CO_BRANCHPAY_FREQUENCY_HOURLY: TStringField;
    CO_BRANCHPAY_FREQUENCY_SALARY: TStringField;
    CO_BRANCHPRINT_BRANCH_ADDRESS_ON_CHECK: TStringField;
    CO_BRANCHPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_BRANCHCL_BILLING_NBR: TIntegerField;
    CO_BRANCHBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_BRANCHTAX_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_BRANCHDD_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_BRANCHCO_WORKERS_COMP_NBR: TIntegerField;
    CO_BRANCHUNION_CL_E_DS_NBR: TIntegerField;
    CO_BRANCHGENERAL_LEDGER_TAG: TStringField;
    CO_BRANCHOVERRIDE_PAY_RATE: TFloatField;
    CO_BRANCHOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CO_BRANCHCL_DELIVERY_GROUP_NBR: TIntegerField;
    CO_BRANCHBRANCH_NOTES: TBlobField;
    CO_BRANCHFILLER: TStringField;
    CO_BRANCHPR_CHECK_MB_GROUP_NBR: TIntegerField;
    CO_BRANCHPR_EE_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_BRANCHPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField;
    CO_BRANCHTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    CO_BRANCHPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_BRANCHPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField;
    CO_BRANCHWORKSITE_ID: TStringField;
    CO_BRANCHAccountNumber: TStringField;
    CO_BRANCHPrimary: TStringField;
    procedure CO_BRANCHBeforePost(DataSet: TDataSet);
    procedure CO_BRANCHCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BRANCH: TDM_CO_BRANCH;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TDM_CO_BRANCH.CO_BRANCHBeforePost(DataSet: TDataSet);
var
 s: String;
 bLookup, bCalcFields : Boolean;
begin

  bLookup := DM_COMPANY.CO_BRANCH.LookupsEnabled;
  bCalcFields := DM_COMPANY.CO_BRANCH.AlwaysCallCalcFields;

  DM_COMPANY.CO_BRANCH.AlwaysCallCalcFields := false;
  DM_COMPANY.CO_BRANCH.LookupsEnabled := false;
  try
    HandleCustomNumber(DataSet.FieldByName('CUSTOM_BRANCH_NUMBER'), 20);

    if Trim(DataSet.FieldByName('AccountNumber').AsString) <> '' then
    begin
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('AccountNumber').AsString, 1, 20);
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('Primary').AsString, 21, 1);
    end
    else
    begin
      if not DataSet.FieldByName('FILLER').IsNull then
      begin
        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '', 1, 20);

        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString,21, 1);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '',21,  1);
        DataSet.FieldByName('Primary').AsString := '';
      end;
    end;
  finally
    DM_COMPANY.CO_BRANCH.AlwaysCallCalcFields := bCalcFields;
    DM_COMPANY.CO_BRANCH.LookupsEnabled := bLookup;
  end;

end;

procedure TDM_CO_BRANCH.CO_BRANCHCalcFields(DataSet: TDataSet);
var
 s : String;
begin
  inherited;

  if not DataSet.FieldByName('FILLER').IsNull then
  begin
    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
    if trim(s) <> '' then
      Dataset.FieldByName('AccountNumber').AsString := s;

    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 21, 1);
    if trim(s) <> '' then
      Dataset.FieldByName('Primary').AsString := s;
  end;
end;

initialization
  RegisterClass(TDM_CO_BRANCH);

finalization
  UnregisterClass(TDM_CO_BRANCH);

end.
