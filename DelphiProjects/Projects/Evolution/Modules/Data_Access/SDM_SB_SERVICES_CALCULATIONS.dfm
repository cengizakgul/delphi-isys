inherited DM_SB_SERVICES_CALCULATIONS: TDM_SB_SERVICES_CALCULATIONS
  OldCreateOrder = False
  Left = 540
  Top = 233
  Height = 350
  Width = 402
  object SB_SERVICES_CALCULATIONS: TSB_SERVICES_CALCULATIONS
    Aggregates = <>
    Params = <>
    ProviderName = 'SB_SERVICES_CALCULATIONS_PROV'
    PictureMasks.Strings = (
      
        'FLAT_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']]}'#9'T'#9'F'
      
        'MINIMUM_QUANTITY'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]}'#9'T'#9'F'
      
        'NEXT_MAXIMUM_QUANTITY'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_PER_ITEM_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F')
    ValidateWithMask = True
    Left = 116
    Top = 96
    object SB_SERVICES_CALCULATIONSSB_SERVICES_CALCULATIONS_NBR: TIntegerField
      FieldName = 'SB_SERVICES_CALCULATIONS_NBR'
    end
    object SB_SERVICES_CALCULATIONSSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
    end
    object SB_SERVICES_CALCULATIONSMINIMUM_QUANTITY: TFloatField
      FieldName = 'MINIMUM_QUANTITY'
      DisplayFormat = '#0'
    end
    object SB_SERVICES_CALCULATIONSNEXT_MINIMUM_QUANTITY: TFloatField
      FieldName = 'NEXT_MINIMUM_QUANTITY'
      DisplayFormat = '#0'
    end
    object SB_SERVICES_CALCULATIONSNEXT_MIN_QUANTITY_BEGIN_DATE: TDateTimeField
      FieldName = 'NEXT_MIN_QUANTITY_BEGIN_DATE'
    end
    object SB_SERVICES_CALCULATIONSMAXIMUM_QUANTITY: TFloatField
      FieldName = 'MAXIMUM_QUANTITY'
      DisplayFormat = '#0'
    end
    object SB_SERVICES_CALCULATIONSNEXT_MAXIMUM_QUANTITY: TFloatField
      FieldName = 'NEXT_MAXIMUM_QUANTITY'
      DisplayFormat = '#0'
    end
    object SB_SERVICES_CALCULATIONSNEXT_MAX_QUANTITY_BEGIN_DATE: TDateTimeField
      FieldName = 'NEXT_MAX_QUANTITY_BEGIN_DATE'
    end
    object SB_SERVICES_CALCULATIONSPER_ITEM_RATE: TFloatField
      FieldName = 'PER_ITEM_RATE'
    end
    object SB_SERVICES_CALCULATIONSNEXT_PER_ITEM_RATE: TFloatField
      FieldName = 'NEXT_PER_ITEM_RATE'
    end
    object SB_SERVICES_CALCULATIONSNEXT_PER_ITEM_BEGIN_DATE: TDateTimeField
      FieldName = 'NEXT_PER_ITEM_BEGIN_DATE'
    end
    object SB_SERVICES_CALCULATIONSFLAT_AMOUNT: TFloatField
      FieldName = 'FLAT_AMOUNT'
    end
    object SB_SERVICES_CALCULATIONSNEXT_FLAT_AMOUNT: TFloatField
      FieldName = 'NEXT_FLAT_AMOUNT'
    end
    object SB_SERVICES_CALCULATIONSNEXT_FLAT_AMOUNT_BEGIN_DATE: TDateTimeField
      FieldName = 'NEXT_FLAT_AMOUNT_BEGIN_DATE'
    end
  end
end
