// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_CHECK_LINE_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_PR_CHECK_LINE_LOCALS = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_CHECK_LINE_LOCALS: TPR_CHECK_LINE_LOCALS;
    PR_CHECK_LINE_LOCALSPR_CHECK_LINE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LINE_LOCALSPR_CHECK_LINES_NBR: TIntegerField;
    PR_CHECK_LINE_LOCALSEE_LOCALS_NBR: TIntegerField;
    PR_CHECK_LINE_LOCALSLocal_Name_Lookup: TStringField;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    PR_CHECK_LINE_LOCALSEXEMPT_EXCLUDE: TStringField;
    procedure PR_CHECK_LINE_LOCALSBeforeDelete(DataSet: TDataSet);
    procedure PR_CHECK_LINE_LOCALSBeforeEdit(DataSet: TDataSet);
    procedure PR_CHECK_LINE_LOCALSBeforeInsert(DataSet: TDataSet);
    procedure PR_CHECK_LINE_LOCALSNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_CHECK_LINE_LOCALS: TDM_PR_CHECK_LINE_LOCALS;

implementation

uses
  SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALSBeforeDelete(
  DataSet: TDataSet);
begin
  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALSBeforeEdit(
  DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALSBeforeInsert(
  DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure TDM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALSNewRecord(
  DataSet: TDataSet);
begin
  DataSet['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
  DataSet['EXEMPT_EXCLUDE'] := DM_EMPLOYEE.EE_LOCALS['EXEMPT_EXCLUDE'];
end;

initialization
  RegisterClass(TDM_PR_CHECK_LINE_LOCALS);

finalization
  UnregisterClass(TDM_PR_CHECK_LINE_LOCALS);

end.
