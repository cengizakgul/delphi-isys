// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_AGENCY_REPORTS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_SB_AGENCY_REPORTS = class(TDM_ddTable)
    SB_AGENCY_REPORTS: TSB_AGENCY_REPORTS;
    SB_AGENCY_REPORTSReportDescriptionLookup: TStringField;
    SB_AGENCY_REPORTSSB_AGENCY_REPORTS_NBR: TIntegerField;
    SB_AGENCY_REPORTSSB_AGENCY_NBR: TIntegerField;
    SB_AGENCY_REPORTSSB_REPORTS_NBR: TIntegerField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_AGENCY_REPORTS: TDM_SB_AGENCY_REPORTS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SB_AGENCY_REPORTS);

finalization
  UnregisterClass(TDM_SB_AGENCY_REPORTS);

end.
