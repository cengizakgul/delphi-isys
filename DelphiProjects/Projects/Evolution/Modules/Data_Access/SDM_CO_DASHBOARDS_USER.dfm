inherited DM_CO_DASHBOARDS_USER: TDM_CO_DASHBOARDS_USER
  OldCreateOrder = True
  Left = 251
  Top = 192
  Height = 480
  Width = 696
  object DM_CLIENT: TDM_CLIENT
    Left = 136
    Top = 144
  end
  object CO_DASHBOARDS_USER: TCO_DASHBOARDS_USER
    ProviderName = 'CO_DASHBOARDS_USER_PROV'
    OnCalcFields = CO_DASHBOARDS_USERCalcFields
    Left = 136
    Top = 80
    object CO_DASHBOARDS_USERCO_DASHBOARDS_NBR: TIntegerField
      FieldName = 'CO_DASHBOARDS_NBR'
    end
    object CO_DASHBOARDS_USERCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_DASHBOARDS_USERSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
    end
    object CO_DASHBOARDS_USERUSER_ID: TStringField
      FieldKind = fkCalculated
      FieldName = 'USER_ID'
      Size = 128
      Calculated = True
    end
  end
end
