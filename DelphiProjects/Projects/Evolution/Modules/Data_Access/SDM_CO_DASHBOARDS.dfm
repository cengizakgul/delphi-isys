inherited DM_CO_DASHBOARDS: TDM_CO_DASHBOARDS
  OldCreateOrder = True
  Left = 507
  Top = 192
  Height = 480
  Width = 696
  object DM_CLIENT: TDM_CLIENT
    Left = 136
    Top = 144
  end
  object CO_DASHBOARDS: TCO_DASHBOARDS
    ProviderName = 'CO_DASHBOARDS_PROV'
    OnCalcFields = CO_DASHBOARDSCalcFields
    Left = 136
    Top = 80
    object CO_DASHBOARDSCO_DASHBOARDS_NBR: TIntegerField
      FieldName = 'CO_DASHBOARDS_NBR'
    end
    object CO_DASHBOARDSDASHBOARD_LEVEL: TStringField
      FieldName = 'DASHBOARD_LEVEL'
      Size = 1
    end
    object CO_DASHBOARDSDASHBOARD_NBR: TIntegerField
      FieldName = 'DASHBOARD_NBR'
    end
    object CO_DASHBOARDSDESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRIPTION'
      Size = 80
      Calculated = True
    end
    object CO_DASHBOARDSLEVEL_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'LEVEL_DESC'
      Size = 8
      Calculated = True
    end
  end
end
