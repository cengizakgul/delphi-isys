inherited DM_CO_PR_FILTERS: TDM_CO_PR_FILTERS
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object CO_PR_FILTERS: TCO_PR_FILTERS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_PR_FILTERS_PROV'
    BeforePost = CO_PR_FILTERSBeforePost
    ValidateWithMask = True
    Left = 45
    Top = 27
    object CO_PR_FILTERSCO_PR_FILTERS_NBR: TIntegerField
      FieldName = 'CO_PR_FILTERS_NBR'
    end
    object CO_PR_FILTERSCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_PR_FILTERSNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object CO_PR_FILTERSCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object CO_PR_FILTERSCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object CO_PR_FILTERSCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object CO_PR_FILTERSCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object CO_PR_FILTERSCO_PAY_GROUP_NBR: TIntegerField
      FieldName = 'CO_PAY_GROUP_NBR'
    end
    object CO_PR_FILTERSCUSTOM_TEAM_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_TEAM_NUMBER'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'CUSTOM_TEAM_NUMBER'
      KeyFields = 'CO_TEAM_NBR'
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_DEPARTMENT_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_DEPARTMENT_NUMBER'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'CUSTOM_DEPARTMENT_NUMBER'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_BRANCH_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_BRANCH_NUMBER'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'CUSTOM_BRANCH_NUMBER'
      KeyFields = 'CO_BRANCH_NBR'
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_DIVISION_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_DIVISION_NUMBER'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'CUSTOM_DIVISION_NUMBER'
      KeyFields = 'CO_DIVISION_NBR'
      Lookup = True
    end
    object CO_PR_FILTERSPAY_GROUP_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'PAY_GROUP_NAME'
      LookupDataSet = DM_CO_PAY_GROUP.CO_PAY_GROUP
      LookupKeyFields = 'CO_PAY_GROUP_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_PAY_GROUP_NBR'
      Size = 40
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_DIVISION_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_DIVISION_NAME'
      LookupDataSet = DM_CO_DIVISION.CO_DIVISION
      LookupKeyFields = 'CO_DIVISION_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DIVISION_NBR'
      Size = 40
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_BRANCH_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_BRANCH_NAME'
      LookupDataSet = DM_CO_BRANCH.CO_BRANCH
      LookupKeyFields = 'CO_BRANCH_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_BRANCH_NBR'
      Size = 40
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_DEPARTMENT_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_DEPARTMENT_NAME'
      LookupDataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
      LookupKeyFields = 'CO_DEPARTMENT_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_DEPARTMENT_NBR'
      Size = 40
      Lookup = True
    end
    object CO_PR_FILTERSCUSTOM_TEAM_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_TEAM_NAME'
      LookupDataSet = DM_CO_TEAM.CO_TEAM
      LookupKeyFields = 'CO_TEAM_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_TEAM_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 42
    Top = 90
  end
end
