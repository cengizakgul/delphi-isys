// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_TEAM_MEMBERS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure,   Variants, EvClientDataSet;

type
  TDM_SB_TEAM_MEMBERS = class(TDM_ddTable)
    SB_TEAM_MEMBERS: TSB_TEAM_MEMBERS;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_TEAM_MEMBERSSB_TEAM_MEMBERS_NBR: TIntegerField;
    SB_TEAM_MEMBERSSB_TEAM_NBR: TIntegerField;
    SB_TEAM_MEMBERSSB_USER_NBR: TIntegerField;
    SB_TEAM_MEMBERSUserName: TStringField;
    procedure SB_TEAM_MEMBERSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_TEAM_MEMBERS: TDM_SB_TEAM_MEMBERS;

implementation

{$R *.DFM}

procedure TDM_SB_TEAM_MEMBERS.SB_TEAM_MEMBERSCalcFields(DataSet: TDataSet);
begin
  if not TevClientDataSet(Dataset).LookupsEnabled then
    Exit;
  if DM_SERVICE_BUREAU.SB_USER.Active
  and DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.CachedFindKey(SB_TEAM_MEMBERSSB_USER_NBR.AsInteger) then
    SB_TEAM_MEMBERSUserName.AsString := DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.FieldByName('LAST_NAME').AsString+
      ', '+ DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.FieldByName('FIRST_NAME').AsString+
      ' '+ DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.FieldByName('MIDDLE_INITIAL').AsString;
end;

initialization
  RegisterClass(TDM_SB_TEAM_MEMBERS);

finalization
  UnregisterClass(TDM_SB_TEAM_MEMBERS);

end.
