inherited DM_CO_MANUAL_ACH: TDM_CO_MANUAL_ACH
  OldCreateOrder = True
  Left = 692
  Top = 202
  Height = 479
  Width = 741
  object CO_MANUAL_ACH: TCO_MANUAL_ACH
    ProviderName = 'CO_MANUAL_ACH_PROV'
    BeforeEdit = CO_MANUAL_ACHBeforeEdit
    BeforePost = CO_MANUAL_ACHBeforePost
    BeforeDelete = CO_MANUAL_ACHBeforeDelete
    OnCalcFields = CO_MANUAL_ACHCalcFields
    Left = 64
    Top = 32
    object CO_MANUAL_ACHTO_EE_CHILD_SUPPORT_CASES_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TO_EE_CHILD_SUPPORT_CASES_NBR'
      Calculated = True
    end
  end
end
