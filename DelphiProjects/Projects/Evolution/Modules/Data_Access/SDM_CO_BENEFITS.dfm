inherited DM_CO_BENEFITS: TDM_CO_BENEFITS
  OldCreateOrder = True
  Left = 551
  Top = 173
  Height = 479
  Width = 741
  object CO_BENEFITS: TCO_BENEFITS
    ProviderName = 'CO_BENEFITS_PROV'
    OnCalcFields = CO_BENEFITSCalcFields
    Left = 96
    Top = 48
    object CO_BENEFITSCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object CO_BENEFITSCO_BENEFIT_CATEGORY_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_CATEGORY_NBR'
    end
    object CO_BENEFITSBENEFIT_NAME: TStringField
      FieldName = 'BENEFIT_NAME'
      Size = 60
    end
    object CO_BENEFITSCATEGORY_TYPE: TStringField
      FieldKind = fkLookup
      FieldName = 'CATEGORY_TYPE'
      LookupDataSet = DM_CO_BENEFIT_CATEGORY.CO_BENEFIT_CATEGORY
      LookupKeyFields = 'CO_BENEFIT_CATEGORY_NBR'
      LookupResultField = 'CATEGORY_TYPE'
      KeyFields = 'CO_BENEFIT_CATEGORY_NBR'
      Size = 1
      Lookup = True
    end
    object CO_BENEFITSCategoryName: TStringField
      FieldKind = fkCalculated
      FieldName = 'CategoryName'
      Size = 40
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 304
    Top = 64
  end
end
