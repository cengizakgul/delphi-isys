// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_BANK_ACCOUNT_REGISTER;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvExceptions,
  SDataDictbureau, EvContext, EvClientDataSet;

type
  TDM_CO_BANK_ACCOUNT_REGISTER = class(TDM_ddTable)
    CO_BANK_ACCOUNT_REGISTER: TCO_BANK_ACCOUNT_REGISTER;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure CO_BANK_ACCOUNT_REGISTERBeforePost(DataSet: TDataSet);
    procedure CO_BANK_ACCOUNT_REGISTERBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
    procedure CheckLockDate(const DataSet: TDataSet);
  public
    { Public declarations }
  end;

implementation

uses EvUtils, EvTypes;

{$R *.DFM}

procedure TDM_CO_BANK_ACCOUNT_REGISTER.CO_BANK_ACCOUNT_REGISTERBeforePost(
  DataSet: TDataSet);
begin
  CheckLockDate(DataSet);
end;

procedure TDM_CO_BANK_ACCOUNT_REGISTER.CO_BANK_ACCOUNT_REGISTERBeforeDelete(
  DataSet: TDataSet);
begin
  CheckLockDate(DataSet);
end;

procedure TDM_CO_BANK_ACCOUNT_REGISTER.CheckLockDate(const DataSet: TDataSet);
var
  FillerDate: string;
  LockDate: TDateTime;
begin
  if not VarIsNull(DataSet['TRANSACTION_EFFECTIVE_DATE']) and not VarIsNull(DataSet['SB_BANK_ACCOUNTS_NBR']) then
  begin
    ctx_DBAccess.EnableReadTransaction;
    try
       DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
    finally
       ctx_DBAccess.DisableReadTransaction;
    end;
    FillerDate := Trim(Copy(ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.LookUp('SB_BANK_ACCOUNTS_NBR', DataSet['SB_BANK_ACCOUNTS_NBR'], 'FILLER'), ''), 61, 10));
    if FillerDate <> '' then
    begin
      LockDate := StrToDateTimeDef(FillerDate, 0);
      if (LockDate <> 0) and (LockDate >= DataSet['TRANSACTION_EFFECTIVE_DATE']) then
        raise EPeriodIsLocked.CreateHelp('This account is locked up to '+ DateToStr(LockDate), IDH_PeriodIsLocked);
    end;
  end;

  if (DataSet.State in [dsEdit, dsInsert])
     and (DataSet.FieldByName('STATUS_DATE').IsNull)  then DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

initialization
  RegisterClass(TDM_CO_BANK_ACCOUNT_REGISTER);

finalization
  UnregisterClass(TDM_CO_BANK_ACCOUNT_REGISTER);

end.
