inherited DM_SB_BANKS: TDM_SB_BANKS
  OldCreateOrder = True
  Left = 573
  Top = 159
  Height = 479
  Width = 741
  object SB_BANKS: TSB_BANKS
    ProviderName = 'SB_BANKS_PROV'
    PictureMasks.Strings = (
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'ZIP_CODE'#9'*5{#,?}[-*4{#,?}]'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    BeforePost = SB_BANKSBeforePost
    OnCalcFields = SB_BANKSCalcFields
    Left = 88
    Top = 60
    object SB_BANKSABA_NUMBER: TStringField
      DisplayLabel = 'ABA Number'
      DisplayWidth = 9
      FieldName = 'ABA_NUMBER'
      Size = 9
    end
    object SB_BANKSNAME: TStringField
      DisplayLabel = 'Name'
      DisplayWidth = 40
      FieldName = 'NAME'
      Size = 40
    end
    object SB_BANKSCITY: TStringField
      DisplayLabel = 'City'
      DisplayWidth = 20
      FieldName = 'CITY'
    end
    object SB_BANKSSTATE: TStringField
      DisplayLabel = 'State'
      DisplayWidth = 2
      FieldName = 'STATE'
      Size = 2
    end
    object SB_BANKSBRANCH_IDENTIFIER: TStringField
      DisplayWidth = 9
      FieldName = 'BRANCH_IDENTIFIER'
      Size = 9
    end
    object SB_BANKSCtsId: TStringField
      DisplayWidth = 6
      FieldKind = fkInternalCalc
      FieldName = 'CtsId'
      OnValidate = SB_BANKSCtsIdValidate
      Size = 6
    end
    object SB_BANKSZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Visible = False
      Size = 10
    end
    object SB_BANKSSB_BANKS_NBR: TIntegerField
      FieldName = 'SB_BANKS_NBR'
      Visible = False
    end
    object SB_BANKSADDRESS1: TStringField
      FieldName = 'ADDRESS1'
      Visible = False
      Size = 30
    end
    object SB_BANKSADDRESS2: TStringField
      FieldName = 'ADDRESS2'
      Visible = False
      Size = 30
    end
    object SB_BANKSCONTACT1: TStringField
      FieldName = 'CONTACT1'
      Visible = False
      Size = 30
    end
    object SB_BANKSPHONE1: TStringField
      FieldName = 'PHONE1'
      Visible = False
    end
    object SB_BANKSDESCRIPTION1: TStringField
      FieldName = 'DESCRIPTION1'
      Visible = False
      Size = 10
    end
    object SB_BANKSCONTACT2: TStringField
      FieldName = 'CONTACT2'
      Visible = False
      Size = 30
    end
    object SB_BANKSPHONE2: TStringField
      FieldName = 'PHONE2'
      Visible = False
    end
    object SB_BANKSDESCRIPTION2: TStringField
      FieldName = 'DESCRIPTION2'
      Visible = False
      Size = 10
    end
    object SB_BANKSFAX: TStringField
      FieldName = 'FAX'
      Visible = False
    end
    object SB_BANKSFAX_DESCRIPTION: TStringField
      FieldName = 'FAX_DESCRIPTION'
      Visible = False
      Size = 10
    end
    object SB_BANKSE_MAIL_ADDRESS: TStringField
      FieldName = 'E_MAIL_ADDRESS'
      Visible = False
      Size = 80
    end
    object SB_BANKSTOP_ABA_NUMBER: TStringField
      FieldName = 'TOP_ABA_NUMBER'
      Visible = False
      Size = 10
    end
    object SB_BANKSBOTTOM_ABA_NUMBER: TStringField
      FieldName = 'BOTTOM_ABA_NUMBER'
      Visible = False
      Size = 10
    end
    object SB_BANKSADDENDA: TStringField
      FieldName = 'ADDENDA'
      Visible = False
      Size = 12
    end
    object SB_BANKSCHECK_TEMPLATE: TBlobField
      FieldName = 'CHECK_TEMPLATE'
      Visible = False
      Size = 8
    end
    object SB_BANKSUSE_CHECK_TEMPLATE: TStringField
      FieldName = 'USE_CHECK_TEMPLATE'
      Visible = False
      Size = 1
    end
    object SB_BANKSMICR_ACCOUNT_START_POSITION: TIntegerField
      FieldName = 'MICR_ACCOUNT_START_POSITION'
      Visible = False
    end
    object SB_BANKSMICR_CHECK_NUMBER_START_POSITN: TIntegerField
      FieldName = 'MICR_CHECK_NUMBER_START_POSITN'
      Visible = False
    end
    object SB_BANKSFILLER: TStringField
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object SB_BANKSPRINT_NAME: TStringField
      FieldName = 'PRINT_NAME'
      Visible = False
      Size = 40
    end
  end
end
