// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_E_DS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvUtils, SDataStructure, SDDClasses, kbmMemTable, EvContext,
  ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, EvClientDataSet;

type
  TDM_CL_E_DS = class(TDM_ddTable)
    CL_E_DS: TCL_E_DS;
    CL_E_DSCUSTOM_E_D_CODE_NUMBER: TStringField;
    CL_E_DSDESCRIPTION: TStringField;
    CL_E_DSE_D_CODE_TYPE: TStringField;
    CL_E_DSCL_E_DS_NBR: TIntegerField;
    CL_E_DSCL_E_D_GROUPS_NBR: TIntegerField;
    CL_E_DSSKIP_HOURS: TStringField;
    CL_E_DSOVERRIDE_RATE: TFloatField;
    CL_E_DSOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CL_E_DSOVERRIDE_FED_TAX_VALUE: TFloatField;
    CL_E_DSOVERRIDE_FED_TAX_TYPE: TStringField;
    CL_E_DSUPDATE_HOURS: TStringField;
    CL_E_DSDEFAULT_CL_AGENCY_NBR: TIntegerField;
    CL_E_DSEE_EXEMPT_EXCLUDE_OASDI: TStringField;
    CL_E_DSEE_EXEMPT_EXCLUDE_MEDICARE: TStringField;
    CL_E_DSEE_EXEMPT_EXCLUDE_FEDERAL: TStringField;
    CL_E_DSEE_EXEMPT_EXCLUDE_EIC: TStringField;
    CL_E_DSER_EXEMPT_EXCLUDE_OASDI: TStringField;
    CL_E_DSER_EXEMPT_EXCLUDE_MEDICARE: TStringField;
    CL_E_DSER_EXEMPT_EXCLUDE_FUI: TStringField;
    CL_E_DSW2_BOX: TStringField;
    CL_E_DSPAY_PERIOD_MINIMUM_AMOUNT: TFloatField;
    CL_E_DSPAY_PERIOD_MINIMUM_PERCENT_OF: TFloatField;
    CL_E_DSMIN_CL_E_D_GROUPS_NBR: TIntegerField;
    CL_E_DSPAY_PERIOD_MAXIMUM_AMOUNT: TFloatField;
    CL_E_DSPAY_PERIOD_MAXIMUM_PERCENT_OF: TFloatField;
    CL_E_DSMAX_CL_E_D_GROUPS_NBR: TIntegerField;
    CL_E_DSANNUAL_MAXIMUM: TFloatField;
    CL_E_DSCL_UNION_NBR: TIntegerField;
    CL_E_DSCL_PENSION_NBR: TIntegerField;
    CL_E_DSPRIORITY_TO_EXCLUDE: TIntegerField;
    CL_E_DSMAKE_UP_DEDUCTION_SHORTFALL: TStringField;
    CL_E_DSMATCH_FIRST_PERCENTAGE: TFloatField;
    CL_E_DSMATCH_FIRST_AMOUNT: TFloatField;
    CL_E_DSMATCH_SECOND_PERCENTAGE: TFloatField;
    CL_E_DSMATCH_SECOND_AMOUNT: TFloatField;
    CL_E_DSFLAT_MATCH_AMOUNT: TFloatField;
    CL_E_DSREGULAR_OT_RATE: TFloatField;
    CL_E_DSOT_ALL_CL_E_D_GROUPS_NBR: TIntegerField;
    CL_E_DSPIECE_CL_E_D_GROUPS_NBR: TIntegerField;
    CL_E_DSPIECEWORK_MINIMUM_WAGE: TFloatField;
    CL_E_DSPW_MIN_WAGE_MAKE_UP_METHOD: TStringField;
    CL_E_DSTIP_MINIMUM_WAGE_MAKE_UP_TYPE: TStringField;
    CL_E_DSOFFSET_CL_E_DS_NBR: TIntegerField;
    CL_E_DSCL_3_PARTY_SICK_PAY_ADMIN_NBR: TIntegerField;
    CL_E_DSSHOW_YTD_ON_CHECKS: TStringField;
    CL_E_DSSCHEDULED_DEFAULTS: TStringField;
    CL_E_DSSD_FREQUENCY: TStringField;
    CL_E_DSSD_EFFECTIVE_START_DATE: TDateTimeField;
    CL_E_DSSD_AMOUNT: TFloatField;
    CL_E_DSSD_RATE: TFloatField;
    CL_E_DSSD_CALCULATION_METHOD: TStringField;
    CL_E_DSSD_EXPRESSION: TBlobField;
    CL_E_DSSD_AUTO: TStringField;
    CL_E_DSSD_ROUND: TStringField;
    CL_E_DSSD_EXCLUDE_WEEK_1: TStringField;
    CL_E_DSSD_EXCLUDE_WEEK_2: TStringField;
    CL_E_DSSD_EXCLUDE_WEEK_3: TStringField;
    CL_E_DSSD_EXCLUDE_WEEK_4: TStringField;
    CL_E_DSSD_EXCLUDE_WEEK_5: TStringField;
    CL_E_DSFILLER: TStringField;
    CL_E_DSOVERRIDE_RATE_TYPE: TStringField;
    CL_E_DSSHOW_ON_INPUT_WORKSHEET: TStringField;
    CL_E_DSSY_STATE_NBR: TIntegerField;
    CL_E_DSGL_OFFSET_TAG: TStringField;
    CL_E_DSSHOW_ED_ON_CHECKS: TStringField;
    CL_E_DSNOTES: TBlobField;
    CL_E_DSAPPLY_BEFORE_TAXES: TStringField;
    CL_E_DSDEFAULT_HOURS: TFloatField;
    CL_E_DSWHICH_CHECKS: TStringField;
    CL_E_DSMONTH_NUMBER: TStringField;
    CL_E_DSSD_PLAN_TYPE: TStringField;
    CL_E_DSSD_WHICH_CHECKS: TStringField;
    CL_E_DSSD_PRIORITY_NUMBER: TIntegerField;
    CL_E_DSSD_ALWAYS_PAY: TStringField;
    CL_E_DSSD_DEDUCTIONS_TO_ZERO: TStringField;
    CL_E_DSSD_MAX_AVG_AMT_GRP_NBR: TIntegerField;
    CL_E_DSSD_MAX_AVG_HRS_GRP_NBR: TIntegerField;
    CL_E_DSSD_MAX_AVERAGE_HOURLY_WAGE_RATE: TFloatField;
    CL_E_DSSD_THRESHOLD_E_D_GROUPS_NBR: TIntegerField;
    CL_E_DSSD_THRESHOLD_AMOUNT: TFloatField;
    CL_E_DSSD_USE_PENSION_LIMIT: TStringField;
    procedure CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CL_E_DS.CL_E_DSEE_EXEMPT_EXCLUDE_OASDIValidate(
  Sender: TField);
begin
  DM_COMPANY.CO.DataRequired();
  DM_COMPANY.CO.First;
  while not DM_COMPANY.CO.Eof do
  begin
    ctx_DataAccess.CheckCompanyLock;
    DM_COMPANY.CO.Next
  end
end;

initialization
  RegisterClass(TDM_CL_E_DS);

finalization
  UnregisterClass(TDM_CL_E_DS);

end.
