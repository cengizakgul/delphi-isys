// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_PERSON;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvUtils, SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, EvContext,
  EvClientDataSet;

type
  TDM_CL_PERSON = class(TDM_ddTable)
    CL_PERSON: TCL_PERSON;
    CL_PERSONCL_PERSON_NBR: TIntegerField;
    CL_PERSONSOCIAL_SECURITY_NUMBER: TStringField;
    CL_PERSONEIN_OR_SOCIAL_SECURITY_NUMBER: TStringField;
    CL_PERSONFIRST_NAME: TStringField;
    CL_PERSONMIDDLE_INITIAL: TStringField;
    CL_PERSONLAST_NAME: TStringField;
    CL_PERSONW2_FIRST_NAME: TStringField;
    CL_PERSONW2_MIDDLE_NAME: TStringField;
    CL_PERSONW2_LAST_NAME: TStringField;
    CL_PERSONW2_NAME_SUFFIX: TStringField;
    CL_PERSONADDRESS1: TStringField;
    CL_PERSONADDRESS2: TStringField;
    CL_PERSONCITY: TStringField;
    CL_PERSONSTATE: TStringField;
    CL_PERSONZIP_CODE: TStringField;
    CL_PERSONCOUNTY: TStringField;
    CL_PERSONCOUNTRY: TStringField;
    CL_PERSONPHONE1: TStringField;
    CL_PERSONDESCRIPTION1: TStringField;
    CL_PERSONPHONE2: TStringField;
    CL_PERSONDESCRIPTION2: TStringField;
    CL_PERSONPHONE3: TStringField;
    CL_PERSONDESCRIPTION3: TStringField;
    CL_PERSONGENDER: TStringField;
    CL_PERSONETHNICITY: TStringField;
    CL_PERSONSMOKER: TStringField;
    CL_PERSONDRIVERS_LICENSE_NUMBER: TStringField;
    CL_PERSONDRIVERS_LICENSE_EXP_DATE: TDateField;
    CL_PERSONAUTO_INSURANCE_CARRIER: TStringField;
    CL_PERSONAUTO_INSURANCE_POLICY_NUMBER: TStringField;
    CL_PERSONAUTO_INSURANCE_POLICY_EXP_DATE: TDateField;
    CL_PERSONVISA_NUMBER: TStringField;
    CL_PERSONVETERAN: TStringField;
    CL_PERSONVETERAN_DISCHARGE_DATE: TDateField;
    CL_PERSONVIETNAM_VETERAN: TStringField;
    CL_PERSONDISABLED_VETERAN: TStringField;
    CL_PERSONMILITARY_RESERVE: TStringField;
    CL_PERSONI9_ON_FILE: TStringField;
    CL_PERSONPICTURE: TBlobField;
    CL_PERSONSY_HR_ETHNICITY_NBR: TIntegerField;
    CL_PERSONNOTES: TBlobField;
    CL_PERSONFILLER: TStringField;
    CL_PERSONCITIZENSHIP: TStringField;
    CL_PERSONVISA_TYPE: TStringField;
    CL_PERSONVISA_EXPIRATION_DATE: TDateField;
    CL_PERSONRESIDENTIAL_STATE_NBR: TIntegerField;
    CL_PERSONWEB_PASSWORD: TStringField;
    procedure CL_PERSONSOCIAL_SECURITY_NUMBERValidate(Sender: TField);
    procedure CL_PERSONEIN_OR_SOCIAL_SECURITY_NUMBERValidate(
      Sender: TField);
    procedure CL_PERSONBeforePost(DataSet: TDataSet);
  end;

implementation

{$R *.DFM}

uses EvConsts;

procedure TDM_CL_PERSON.CL_PERSONSOCIAL_SECURITY_NUMBERValidate(
  Sender: TField);
begin
  try
    ctx_DataAccess.CheckCompanyLock
  except
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('Table1', 'PR_CHECK');
      SetMacro('Table2', 'EE');
      SetMacro('JoinField', 'EE');
      SetMacro('Condition', 'CL_PERSON_NBR = :Nbr');
      SetMacro('Columns', 'sum(NET_WAGES)');
      SetParam('Nbr', CL_PERSON.FieldByName('CL_PERSON_NBR').AsInteger);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.CUSTOM_VIEW.Open;
      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsCurrency <> 0 then
        raise
    end
  end
end;

procedure TDM_CL_PERSON.CL_PERSONEIN_OR_SOCIAL_SECURITY_NUMBERValidate(
  Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

procedure TDM_CL_PERSON.CL_PERSONBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DataSet.FieldByName('SOCIAL_SECURITY_NUMBER').OldValue <> DataSet.FieldByName('SOCIAL_SECURITY_NUMBER').NewValue)
    and (DataSet.FieldByName('EIN_OR_SOCIAL_SECURITY_NUMBER').NewValue = GROUP_BOX_SSN) then
  begin
    if (not DM_CLIENT.CL.Active) or (DM_CLIENT.CL['CL_NBR'] <> ctx_DataAccess.ClientId) then
      DM_CLIENT.CL.DataRequired('CL_NBR=' + IntToStr(ctx_DataAccess.ClientId));
    if DM_CLIENT.CL['BLOCK_INVALID_SSN'] = 'Y' then
      CheckSSN(DataSet['SOCIAL_SECURITY_NUMBER']);
  end;
end;

initialization
  RegisterClass(TDM_CL_PERSON);

finalization
  UnregisterClass(TDM_CL_PERSON);

end.
