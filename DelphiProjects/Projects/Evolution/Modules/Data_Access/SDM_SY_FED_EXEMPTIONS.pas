// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_FED_EXEMPTIONS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   SDataAccessCommon, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses;

type
  TDM_SY_FED_EXEMPTIONS = class(TDM_ddTable)
    SY_FED_EXEMPTIONS: TSY_FED_EXEMPTIONS;
    SY_FED_EXEMPTIONSEXEMPT_EMPLOYEE_EIC: TStringField;
    SY_FED_EXEMPTIONSEXEMPT_EMPLOYEE_MEDICARE: TStringField;
    SY_FED_EXEMPTIONSEXEMPT_EMPLOYEE_OASDI: TStringField;
    SY_FED_EXEMPTIONSEXEMPT_EMPLOYER_MEDICARE: TStringField;
    SY_FED_EXEMPTIONSEXEMPT_EMPLOYER_OASDI: TStringField;
    SY_FED_EXEMPTIONSEXEMPT_FEDERAL: TStringField;
    SY_FED_EXEMPTIONSEXEMPT_FUI: TStringField;
    SY_FED_EXEMPTIONSE_D_CODE_TYPE: TStringField;
    SY_FED_EXEMPTIONSSY_FED_EXEMPTIONS_NBR: TIntegerField;
    SY_FED_EXEMPTIONSW2_BOX: TStringField;
    SY_FED_EXEMPTIONSEDDescription: TStringField;
    procedure SY_FED_EXEMPTIONSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TDM_SY_FED_EXEMPTIONS.SY_FED_EXEMPTIONSCalcFields(DataSet: TDataSet);
begin
  DataSet['EDDescription'] := GetEDTypeDescription(DataSet.FieldByName('E_D_CODE_TYPE').AsString);
end;

initialization
  RegisterClass(TDM_SY_FED_EXEMPTIONS);

finalization
  UnregisterClass(TDM_SY_FED_EXEMPTIONS);

end.
