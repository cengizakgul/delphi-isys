inherited DM_CO_DIVISION: TDM_CO_DIVISION
  OldCreateOrder = True
  Left = 373
  Top = 206
  Height = 480
  Width = 696
  object CO_DIVISION: TCO_DIVISION
    ProviderName = 'CO_DIVISION_PROV'
    PictureMasks.Strings = (
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      
        'OVERRIDE_PAY_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    FieldDefs = <
      item
        Name = 'CO_DIVISION_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_DIVISION_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ADDRESS1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ADDRESS2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CITY'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'ZIP_CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CONTACT1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PHONE1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CONTACT2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PHONE2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DESCRIPTION2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FAX'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FAX_DESCRIPTION'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'E_MAIL_ADDRESS'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'HOME_CO_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_TIMECLOCK_IMPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAY_FREQUENCY_HOURLY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAY_FREQUENCY_SALARY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRINT_DIV_ADDRESS_ON_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_BILLING_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BILLING_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DD_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_WORKERS_COMP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'OVERRIDE_PAY_RATE'
        DataType = ftFloat
      end
      item
        Name = 'OVERRIDE_EE_RATE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'UNION_CL_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DIVISION_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'HOME_STATE_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CL_DELIVERY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_EE_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_EE_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_DBDT_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
        DataType = ftInteger
      end>
    BeforePost = CO_DIVISIONBeforePost
    OnCalcFields = CO_DIVISIONCalcFields
    BlobsLoadMode = blmManualLoad
    Left = 104
    Top = 96
    object CO_DIVISIONCO_DIVISION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DIVISION_NBR'
      Origin = '"CO_DIVISION_HIST"."CO_DIVISION_NBR"'
      Required = True
    end
    object CO_DIVISIONCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = '"CO_DIVISION_HIST"."CO_NBR"'
      Required = True
    end
    object CO_DIVISIONCUSTOM_DIVISION_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_DIVISION_NUMBER'
      Origin = '"CO_DIVISION_HIST"."CUSTOM_DIVISION_NUMBER"'
      Required = True
    end
    object CO_DIVISIONNAME: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = '"CO_DIVISION_HIST"."NAME"'
      Size = 40
    end
    object CO_DIVISIONADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"CO_DIVISION_HIST"."ADDRESS1"'
      Size = 30
    end
    object CO_DIVISIONADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"CO_DIVISION_HIST"."ADDRESS2"'
      Size = 30
    end
    object CO_DIVISIONCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CO_DIVISION_HIST"."CITY"'
    end
    object CO_DIVISIONSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CO_DIVISION_HIST"."STATE"'
      Size = 2
    end
    object CO_DIVISIONZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CO_DIVISION_HIST"."ZIP_CODE"'
      Size = 10
    end
    object CO_DIVISIONCONTACT1: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT1'
      Origin = '"CO_DIVISION_HIST"."CONTACT1"'
      Size = 30
    end
    object CO_DIVISIONPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"CO_DIVISION_HIST"."PHONE1"'
    end
    object CO_DIVISIONDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"CO_DIVISION_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object CO_DIVISIONCONTACT2: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT2'
      Origin = '"CO_DIVISION_HIST"."CONTACT2"'
      Size = 30
    end
    object CO_DIVISIONPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"CO_DIVISION_HIST"."PHONE2"'
    end
    object CO_DIVISIONDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"CO_DIVISION_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object CO_DIVISIONFAX: TStringField
      DisplayWidth = 20
      FieldName = 'FAX'
      Origin = '"CO_DIVISION_HIST"."FAX"'
    end
    object CO_DIVISIONFAX_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX_DESCRIPTION'
      Origin = '"CO_DIVISION_HIST"."FAX_DESCRIPTION"'
      Size = 10
    end
    object CO_DIVISIONE_MAIL_ADDRESS: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS'
      Origin = '"CO_DIVISION_HIST"."E_MAIL_ADDRESS"'
      Size = 80
    end
    object CO_DIVISIONHOME_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_CO_STATES_NBR'
      Origin = '"CO_DIVISION_HIST"."HOME_CO_STATES_NBR"'
    end
    object CO_DIVISIONCL_TIMECLOCK_IMPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_TIMECLOCK_IMPORTS_NBR'
      Origin = '"CO_DIVISION_HIST"."CL_TIMECLOCK_IMPORTS_NBR"'
    end
    object CO_DIVISIONPAY_FREQUENCY_HOURLY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_HOURLY'
      Origin = '"CO_DIVISION_HIST"."PAY_FREQUENCY_HOURLY"'
      Required = True
      Size = 1
    end
    object CO_DIVISIONPAY_FREQUENCY_SALARY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_SALARY'
      Origin = '"CO_DIVISION_HIST"."PAY_FREQUENCY_SALARY"'
      Required = True
      Size = 1
    end
    object CO_DIVISIONPRINT_DIV_ADDRESS_ON_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_DIV_ADDRESS_ON_CHECKS'
      Origin = '"CO_DIVISION_HIST"."PRINT_DIV_ADDRESS_ON_CHECKS"'
      Required = True
      Size = 1
    end
    object CO_DIVISIONPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DIVISION_HIST"."PAYROLL_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DIVISIONCL_BILLING_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_BILLING_NBR'
      Origin = '"CO_DIVISION_HIST"."CL_BILLING_NBR"'
    end
    object CO_DIVISIONBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BILLING_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DIVISION_HIST"."BILLING_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DIVISIONTAX_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DIVISION_HIST"."TAX_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DIVISIONDD_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DD_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DIVISION_HIST"."DD_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DIVISIONCO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"CO_DIVISION_HIST"."CO_WORKERS_COMP_NBR"'
    end
    object CO_DIVISIONOVERRIDE_PAY_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_PAY_RATE'
      Origin = '"CO_DIVISION_HIST"."OVERRIDE_PAY_RATE"'
    end
    object CO_DIVISIONOVERRIDE_EE_RATE_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_EE_RATE_NUMBER'
      Origin = '"CO_DIVISION_HIST"."OVERRIDE_EE_RATE_NUMBER"'
    end
    object CO_DIVISIONUNION_CL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'UNION_CL_E_DS_NBR'
      Origin = '"CO_DIVISION_HIST"."UNION_CL_E_DS_NBR"'
    end
    object CO_DIVISIONDIVISION_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'DIVISION_NOTES'
      Origin = '"CO_DIVISION_HIST"."DIVISION_NOTES"'
      Size = 8
    end
    object CO_DIVISIONFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CO_DIVISION_HIST"."FILLER"'
      Size = 512
    end
    object CO_DIVISIONHOME_STATE_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'HOME_STATE_TYPE'
      Origin = '"CO_DIVISION_HIST"."HOME_STATE_TYPE"'
      Required = True
      Size = 1
    end
    object CO_DIVISIONGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"CO_DIVISION_HIST"."GENERAL_LEDGER_TAG"'
    end
    object CO_DIVISIONCL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_DIVISION_HIST"."CL_DELIVERY_GROUP_NBR"'
    end
    object CO_DIVISIONPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
    end
    object CO_DIVISIONPR_EE_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_MB_GROUP_NBR'
    end
    object CO_DIVISIONPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
    end
    object CO_DIVISIONTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
    end
    object CO_DIVISIONPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_MB_GROUP_NBR'
    end
    object CO_DIVISIONPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
    end
    object CO_DIVISIONWORKSITE_ID: TStringField
      FieldName = 'WORKSITE_ID'
    end
    object CO_DIVISIONAccountNumber: TStringField
      FieldKind = fkCalculated
      FieldName = 'AccountNumber'
      Calculated = True
    end
    object CO_DIVISIONPrimary: TStringField
      FieldKind = fkCalculated
      FieldName = 'Primary'
      Size = 1
      Calculated = True
    end
  end
end
