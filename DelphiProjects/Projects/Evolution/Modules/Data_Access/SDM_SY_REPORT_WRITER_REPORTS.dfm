inherited DM_SY_REPORT_WRITER_REPORTS: TDM_SY_REPORT_WRITER_REPORTS
  OldCreateOrder = True
  Left = 346
  Top = 158
  Height = 479
  Width = 741
  object SY_REPORT_WRITER_REPORTS: TSY_REPORT_WRITER_REPORTS
    ProviderName = 'SY_REPORT_WRITER_REPORTS_PROV'
    OnCalcFields = SY_REPORT_WRITER_REPORTSCalcFields
    Left = 86
    Top = 36
    object SY_REPORT_WRITER_REPORTSSY_REPORT_WRITER_REPORTS_NBR: TIntegerField
      FieldName = 'SY_REPORT_WRITER_REPORTS_NBR'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."SY_REPORT_WRITER_REPORTS_NBR"'
      Required = True
    end
    object SY_REPORT_WRITER_REPORTSREPORT_DESCRIPTION: TStringField
      FieldName = 'REPORT_DESCRIPTION'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."REPORT_DESCRIPTION"'
      Required = True
      Size = 40
    end
    object SY_REPORT_WRITER_REPORTSREPORT_TYPE: TStringField
      FieldName = 'REPORT_TYPE'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."REPORT_TYPE"'
      Required = True
      Size = 1
    end
    object SY_REPORT_WRITER_REPORTSREPORT_STATUS: TStringField
      FieldName = 'REPORT_STATUS'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."REPORT_STATUS"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SY_REPORT_WRITER_REPORTSREPORT_FILE: TBlobField
      FieldName = 'REPORT_FILE'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."REPORT_FILE"'
      Size = 8
    end
    object SY_REPORT_WRITER_REPORTSNOTES: TBlobField
      FieldName = 'NOTES'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."NOTES"'
      Size = 8
    end
    object SY_REPORT_WRITER_REPORTSRWDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'RWDescription'
      Size = 8000
    end
    object SY_REPORT_WRITER_REPORTSMEDIA_TYPE: TStringField
      FieldName = 'MEDIA_TYPE'
      Origin = '"SY_REPORT_WRITER_REPORTS_HIST"."MEDIA_TYPE"'
      Required = True
      Size = 2
    end
    object SY_REPORT_WRITER_REPORTSCLASS_NAME: TStringField
      FieldName = 'CLASS_NAME'
      Size = 40
    end
    object SY_REPORT_WRITER_REPORTSANCESTOR_CLASS_NAME: TStringField
      FieldName = 'ANCESTOR_CLASS_NAME'
      Size = 40
    end
  end
end
