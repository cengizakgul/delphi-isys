// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_BILLING_HISTORY;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  EvConsts, EvContext, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, evTypes, EvClientDataSet;

type
  TDM_CO_BILLING_HISTORY = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    CO_BILLING_HISTORY: TCO_BILLING_HISTORY;
    CO_BILLING_HISTORYCO_BILLING_HISTORY_NBR: TIntegerField;
    CO_BILLING_HISTORYINVOICE_NUMBER: TIntegerField;
    CO_BILLING_HISTORYCO_NBR: TIntegerField;
    CO_BILLING_HISTORYINVOICE_DATE: TDateTimeField;
    CO_BILLING_HISTORYPR_NBR: TIntegerField;
    CO_BILLING_HISTORYEXPORTED_TO_A_R: TStringField;
    CO_BILLING_HISTORYSALES_TAX_AMOUNT: TFloatField;
    CO_BILLING_HISTORYCHECK_SERIAL_NUMBER: TIntegerField;
    CO_BILLING_HISTORYBILLING_CO_BANK_ACCOUNT_NBR: TIntegerField;
    CO_BILLING_HISTORYNOTES: TBlobField;
    CO_BILLING_HISTORYCheckDate: TDateTimeField;
    CO_BILLING_HISTORYSTATUS_DATE: TDateTimeField;
    CO_BILLING_HISTORYSTATUS: TStringField;
    CO_BILLING_HISTORYRunNumber: TIntegerField;
    CO_BILLING_HISTORYCO_DIVISION_NBR: TIntegerField;
    CO_BILLING_HISTORYCO_BRANCH_NBR: TIntegerField;
    CO_BILLING_HISTORYCO_DEPARTMENT_NBR: TIntegerField;
    CO_BILLING_HISTORYCO_TEAM_NBR: TIntegerField;
    CO_BILLING_HISTORYFILLER: TStringField;
    procedure CO_BILLING_HISTORYBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TDM_CO_BILLING_HISTORY.CO_BILLING_HISTORYBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DataSet.State in [dsInsert]) and DataSet.FieldByName('INVOICE_NUMBER').IsNull then
    DataSet.FieldByName('INVOICE_NUMBER').Value := ctx_DBAccess.GetGeneratorValue('G_NEXT_INVOICE_NUMBER', dbtBureau, 1);

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

initialization
  RegisterClass(TDM_CO_BILLING_HISTORY);

finalization
  UnregisterClass(TDM_CO_BILLING_HISTORY);

end.
