// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_MEDIA_TYPE;

interface

uses
  SDM_ddTable, SDataDictBureau,
  SysUtils, Classes, DB,   SDataStructure, SDDClasses, SDataDictsystem,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SB_MEDIA_TYPE = class(TDM_ddTable)
    SB_MEDIA_TYPE: TSB_MEDIA_TYPE;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_MEDIA_TYPESB_MEDIA_TYPE_NBR: TIntegerField;
    SB_MEDIA_TYPESY_MEDIA_TYPE_NBR: TIntegerField;
    SB_MEDIA_TYPEluMediaName: TStringField;
    SB_MEDIA_TYPECLASS_NAME: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_SB_MEDIA_TYPE);

finalization
  UnregisterClass(TDM_SB_MEDIA_TYPE);

end.
