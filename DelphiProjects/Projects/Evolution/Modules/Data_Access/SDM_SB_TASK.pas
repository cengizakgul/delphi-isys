// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_TASK;

interface

uses
  SDM_ddTable, SDataDictBureau, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Forms, Dialogs, Variants, Db,  
  SDataStructure, EvUtils, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, EvContext, EvLegacy,
  EvDataAccessComponents, EvCommonInterfaces, EvStreamUtils, isBaseClasses, EvClientDataSet;

type
  TDM_SB_TASK = class(TDM_ddTable)
    SB_TASK: TSB_TASK;
    SB_TASKSB_TASK_NBR: TIntegerField;
    SB_TASKSB_USER_NBR: TIntegerField;
    SB_TASKSCHEDULE: TStringField;
    SB_TASKDESCRIPTION: TStringField;
    SB_TASKTASK: TBlobField;
    SB_TASKNAME: TStringField;
    SB_TASKSCHED_DESC: TStringField;
    SB_TASKUSER_NAME: TStringField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_TASKTaskCaption: TStringField;
    procedure SB_TASKCalcFields(DataSet: TDataSet);
    procedure SB_TASKAddLookups(var A: TArrayDS);
    procedure SB_TASKNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function GetTaskObj: IevTask;
  end;

implementation

{$R *.DFM}

procedure TDM_SB_TASK.SB_TASKCalcFields(DataSet: TDataSet);
var
  Tsk: IevTask;
begin
  SB_TASKName.AsString := MethodNameToTaskType(DataSet.FieldByName('Description').AsString);

  if DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.CachedFindKey(SB_TASKSB_USER_NBR.AsInteger) then
    SB_TASKUSER_NAME.AsString := DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.FieldByName('LAST_NAME').AsString+
      ' '+ DM_SERVICE_BUREAU.SB_USER.ShadowDataSet.FieldByName('FIRST_NAME').AsString;
  SB_TASKSched_desc.AsString := ScheduleToText(SB_TASKSchedule.AsString);

  Tsk := GetTaskObj;

  if Assigned(Tsk) then
    SB_TASKTaskCaption.AsString  := Tsk.Caption
  else
    SB_TASKTaskCaption.AsString  := '';
end;

procedure TDM_SB_TASK.SB_TASKAddLookups(var A: TArrayDS);
begin
  AddDS(DM_SERVICE_BUREAU.SB_USER, A);
end;

procedure TDM_SB_TASK.SB_TASKNewRecord(DataSet: TDataSet);
begin
  DataSet['SB_USER_NBR'] := Context.UserAccount.InternalNbr;
end;

function TDM_SB_TASK.GetTaskObj: IevTask;
begin;
  try
    Result := TaskFromBlobField(MethodNameToTaskType(SB_TASKDESCRIPTION.AsString), SB_TASKTASK);
  except
  end;
end;

procedure TDM_SB_TASK.DataModuleCreate(Sender: TObject);
begin
  inherited;

  // We need load blob data in advance because of SB_TASKTaskCaption calculated field
  // Lazy load logic would make this inefficient
  SB_TASK.BlobsLoadMode := blmPreload;
end;

initialization
  RegisterClass(TDM_SB_TASK);

finalization
  UnregisterClass(TDM_SB_TASK);

end.
