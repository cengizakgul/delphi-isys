// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_STATE_TAX_CHART;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_SY_STATE_TAX_CHART = class(TDM_ddTable)
    SY_STATE_TAX_CHART: TSY_STATE_TAX_CHART;
    SY_STATE_TAX_CHARTMARITAL_STATUS_DESCRPTION: TStringField;
    SY_STATE_TAX_CHARTMINIMUM: TFloatField;
    SY_STATE_TAX_CHARTMAXIMUM: TFloatField;
    SY_STATE_TAX_CHARTPERCENTAGE: TFloatField;
    SY_STATE_TAX_CHARTSY_STATE_MARITAL_STATUS_NBR: TIntegerField;
    SY_STATE_TAX_CHARTSY_STATE_TAX_CHART_NBR: TIntegerField;
    SY_STATE_TAX_CHARTSY_STATES_NBR: TIntegerField;
    SY_STATE_TAX_CHARTTYPE: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_STATE_TAX_CHART);

finalization
  UnregisterClass(TDM_SY_STATE_TAX_CHART);

end.
