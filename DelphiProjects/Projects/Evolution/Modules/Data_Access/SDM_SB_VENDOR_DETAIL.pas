// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_VENDOR_DETAIL;

interface

uses
  SDM_ddTable, SDataDictBureau,
  SysUtils, Classes, DB,   SDataStructure, SDDClasses, SDataDictsystem,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, EvConsts;

type
  TDM_SB_VENDOR_DETAIL = class(TDM_ddTable)
    SB_VENDOR_DETAIL: TSB_VENDOR_DETAIL;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_VENDOR_DETAILDETAIL_LEVEL_DESC: TStringField;
    SB_VENDOR_DETAILDETAIL_LEVEL: TStringField;
    SB_VENDOR_DETAILSB_VENDOR_DETAIL_NBR: TIntegerField;
    SB_VENDOR_DETAILSB_VENDOR_NBR: TIntegerField;
    SB_VENDOR_DETAILDETAIL: TStringField;
    SB_VENDOR_DETAILCO_DETAIL_REQUIRED: TStringField;
    procedure SB_VENDOR_DETAILCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}


procedure TDM_SB_VENDOR_DETAIL.SB_VENDOR_DETAILCalcFields(
  DataSet: TDataSet);
begin
  inherited;

   SB_VENDOR_DETAILDETAIL_LEVEL_DESC.AsString := 'Bureau';
   if SB_VENDOR_DETAILDETAIL_LEVEL.AsString[1] = LEVEL_COMPANY then
    SB_VENDOR_DETAILDETAIL_LEVEL_DESC.AsString :=  'Company';
end;

initialization
  RegisterClass(TDM_SB_VENDOR_DETAIL);

finalization
  UnregisterClass(TDM_SB_VENDOR_DETAIL);

end.
