inherited DM_EE_ADDITIONAL_INFO: TDM_EE_ADDITIONAL_INFO
  OldCreateOrder = True
  Left = 669
  Top = 195
  Height = 240
  Width = 223
  object EE_ADDITIONAL_INFO: TEE_ADDITIONAL_INFO
    ProviderName = 'EE_ADDITIONAL_INFO_PROV'
    Left = 60
    Top = 30
    object EE_ADDITIONAL_INFOEE_ADDITIONAL_INFO_NBR: TIntegerField
      FieldName = 'EE_ADDITIONAL_INFO_NBR'
    end
    object EE_ADDITIONAL_INFOEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_NAMES_NBR: TIntegerField
      FieldName = 'CO_ADDITIONAL_INFO_NAMES_NBR'
      OnChange = EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_NAMES_NBRChange
    end
    object EE_ADDITIONAL_INFOINFO_STRING: TStringField
      FieldName = 'INFO_STRING'
      Size = 40
    end
    object EE_ADDITIONAL_INFOINFO_DATE: TDateField
      FieldName = 'INFO_DATE'
    end
    object EE_ADDITIONAL_INFOINFO_AMOUNT: TFloatField
      FieldName = 'INFO_AMOUNT'
    end
    object EE_ADDITIONAL_INFOName: TStringField
      FieldKind = fkLookup
      FieldName = 'Name'
      LookupDataSet = DM_CO_ADDITIONAL_INFO_NAMES.CO_ADDITIONAL_INFO_NAMES
      LookupKeyFields = 'CO_ADDITIONAL_INFO_NAMES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_ADDITIONAL_INFO_NAMES_NBR'
      Size = 40
      Lookup = True
    end
    object EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_VALUES_NBR: TIntegerField
      FieldName = 'CO_ADDITIONAL_INFO_VALUES_NBR'
    end
    object EE_ADDITIONAL_INFOValue: TStringField
      FieldKind = fkLookup
      FieldName = 'Value'
      LookupDataSet = DM_CO_ADDITIONAL_INFO_VALUES.CO_ADDITIONAL_INFO_VALUES
      LookupKeyFields = 'CO_ADDITIONAL_INFO_VALUES_NBR'
      LookupResultField = 'INFO_VALUE'
      KeyFields = 'CO_ADDITIONAL_INFO_VALUES_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 63
    Top = 87
  end
end
