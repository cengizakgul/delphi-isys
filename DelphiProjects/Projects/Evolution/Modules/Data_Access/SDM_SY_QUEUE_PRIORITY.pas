// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_QUEUE_PRIORITY;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses;

type
  TDM_SY_QUEUE_PRIORITY = class(TDM_ddTable)
    SY_QUEUE_PRIORITY: TSY_QUEUE_PRIORITY;
    SY_QUEUE_PRIORITYSY_QUEUE_PRIORITY_NBR: TIntegerField;
    SY_QUEUE_PRIORITYTHREADS: TIntegerField;
    SY_QUEUE_PRIORITYMETHOD_NAME: TStringField;
    SY_QUEUE_PRIORITYPRIORITY: TIntegerField;
    procedure SY_QUEUE_PRIORITYTHREADSValidate(Sender: TField);
  private
  public
  end;

implementation

{$R *.DFM}

procedure TDM_SY_QUEUE_PRIORITY.SY_QUEUE_PRIORITYTHREADSValidate(
  Sender: TField);
begin
  inherited;
  if SY_QUEUE_PRIORITYTHREADS.IsNull then
    SY_QUEUE_PRIORITYTHREADS.AsInteger := 0;
end;

initialization
  RegisterClass(TDM_SY_QUEUE_PRIORITY);

finalization
  UnregisterClass(TDM_SY_QUEUE_PRIORITY);

end.
