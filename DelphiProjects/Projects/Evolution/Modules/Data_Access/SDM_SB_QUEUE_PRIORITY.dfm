inherited DM_SB_QUEUE_PRIORITY: TDM_SB_QUEUE_PRIORITY
  OldCreateOrder = True
  Left = 406
  Top = 468
  Height = 421
  Width = 614
  object SB_QUEUE_PRIORITY: TSB_QUEUE_PRIORITY
    ProviderName = 'SB_QUEUE_PRIORITY_PROV'
    Left = 57
    Top = 24
    object SB_QUEUE_PRIORITYSB_QUEUE_PRIORITY_NBR: TIntegerField
      FieldName = 'SB_QUEUE_PRIORITY_NBR'
      Origin = '"SB_QUEUE_PRIORITY_HIST"."SB_QUEUE_PRIORITY_NBR"'
      Required = True
    end
    object SB_QUEUE_PRIORITYTHREADS: TIntegerField
      FieldName = 'THREADS'
      Origin = '"SB_QUEUE_PRIORITY_HIST"."THREADS"'
      Required = True
      OnValidate = SB_QUEUE_PRIORITYTHREADSValidate
      DisplayFormat = '###'
    end
    object SB_QUEUE_PRIORITYMETHOD_NAME: TStringField
      FieldName = 'METHOD_NAME'
      Origin = '"SB_QUEUE_PRIORITY_HIST"."METHOD_NAME"'
      Required = True
      Size = 255
    end
    object SB_QUEUE_PRIORITYPRIORITY: TIntegerField
      FieldName = 'PRIORITY'
      Origin = '"SB_QUEUE_PRIORITY_HIST"."PRIORITY"'
      Required = True
    end
    object SB_QUEUE_PRIORITYSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
      Origin = '"SB_QUEUE_PRIORITY_HIST"."SB_USER_NBR"'
    end
    object SB_QUEUE_PRIORITYSB_SEC_GROUPS_NBR: TIntegerField
      FieldName = 'SB_SEC_GROUPS_NBR'
      Origin = '"SB_QUEUE_PRIORITY_HIST"."SB_SEC_GROUPS_NBR"'
    end
    object SB_QUEUE_PRIORITYUserName: TStringField
      FieldKind = fkLookup
      FieldName = 'UserName'
      LookupDataSet = DM_SB_USER.SB_USER
      LookupKeyFields = 'SB_USER_NBR'
      LookupResultField = 'USER_ID'
      KeyFields = 'SB_USER_NBR'
      Size = 128
      Lookup = True
    end
    object SB_QUEUE_PRIORITYGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'GroupName'
      LookupDataSet = DM_SB_SEC_GROUPS.SB_SEC_GROUPS
      LookupKeyFields = 'SB_SEC_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SB_SEC_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 201
    Top = 33
  end
end
