inherited DM_SB_MEDIA_TYPE: TDM_SB_MEDIA_TYPE
  Left = 336
  Top = 191
  Height = 330
  Width = 347
  object SB_MEDIA_TYPE: TSB_MEDIA_TYPE
    ProviderName = 'SB_MEDIA_TYPE_PROV'
    Left = 56
    Top = 48
    object SB_MEDIA_TYPESB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
    end
    object SB_MEDIA_TYPESY_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SY_MEDIA_TYPE_NBR'
    end
    object SB_MEDIA_TYPEluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SY_MEDIA_TYPE.SY_MEDIA_TYPE
      LookupKeyFields = 'SY_MEDIA_TYPE_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object SB_MEDIA_TYPECLASS_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'CLASS_NAME'
      LookupDataSet = DM_SY_MEDIA_TYPE.SY_MEDIA_TYPE
      LookupKeyFields = 'SY_MEDIA_TYPE_NBR'
      LookupResultField = 'CLASS_NAME'
      KeyFields = 'SY_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 184
    Top = 40
  end
end
