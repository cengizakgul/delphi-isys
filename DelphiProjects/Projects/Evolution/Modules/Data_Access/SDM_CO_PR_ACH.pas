// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_PR_ACH;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet;

type
  TDM_CO_PR_ACH = class(TDM_ddTable)
    CO_PR_ACH: TCO_PR_ACH;
    CO_PR_ACHCO_PR_ACH_NBR: TIntegerField;
    CO_PR_ACHCO_NBR: TIntegerField;
    CO_PR_ACHACH_DATE: TDateTimeField;
    CO_PR_ACHAMOUNT: TFloatField;
    CO_PR_ACHSTATUS: TStringField;
    CO_PR_ACHPR_NBR: TIntegerField;
    CO_PR_ACHNOTES: TBlobField;
    CO_PR_ACHFILLER: TStringField;
    CO_PR_ACHCheckDate: TDateTimeField;
    CO_PR_ACHSTATUS_DATE: TDateTimeField;
    DM_PAYROLL: TDM_PAYROLL;
    procedure CO_PR_ACHBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

uses EvUtils;

procedure TDM_CO_PR_ACH.CO_PR_ACHBeforePost(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('STATUS').OldValue <> DataSet.FieldByName('STATUS').Value then
    DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

initialization
  RegisterClass(TDM_CO_PR_ACH);

finalization
  UnregisterClass(TDM_CO_PR_ACH);

end.
