inherited DM_PR_CHECK_LOCALS: TDM_PR_CHECK_LOCALS
  OldCreateOrder = True
  Left = 339
  Top = 255
  Height = 480
  Width = 696
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 96
  end
  object PR_CHECK_LOCALS: TPR_CHECK_LOCALS
    ProviderName = 'PR_CHECK_LOCALS_PROV'
    FieldDefs = <
      item
        Name = 'PR_CHECK_LOCALS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EE_LOCALS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'LOCAL_TAXABLE_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'LOCAL_TAX'
        DataType = ftFloat
      end
      item
        Name = 'EXCLUDE_LOCAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OVERRIDE_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'LOCAL_SHORTFALL'
        DataType = ftFloat
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'LOCAL_GROSS_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'CO_LOCATIONS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NONRES_EE_LOCALS_NBR'
        DataType = ftInteger
      end>
    BeforeInsert = PR_CHECK_LOCALSBeforeInsert
    BeforeEdit = PR_CHECK_LOCALSBeforeEdit
    BeforePost = PR_CHECK_LOCALSBeforePost
    BeforeDelete = PR_CHECK_LOCALSBeforeDelete
    Left = 208
    Top = 160
    object PR_CHECK_LOCALSPR_CHECK_LOCALS_NBR: TIntegerField
      FieldName = 'PR_CHECK_LOCALS_NBR'
      Origin = '"PR_CHECK_LOCALS"."PR_CHECK_LOCALS_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object PR_CHECK_LOCALSPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
      Origin = '"PR_CHECK_LOCALS"."PR_CHECK_NBR"'
      Required = True
    end
    object PR_CHECK_LOCALSEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
      Origin = '"PR_CHECK_LOCALS"."EE_LOCALS_NBR"'
      Required = True
    end
    object PR_CHECK_LOCALSLOCAL_TAXABLE_WAGE: TFloatField
      FieldName = 'LOCAL_TAXABLE_WAGE'
      Origin = '"PR_CHECK_LOCALS"."LOCAL_TAXABLE_WAGE"'
    end
    object PR_CHECK_LOCALSLOCAL_TAX: TFloatField
      FieldName = 'LOCAL_TAX'
      Origin = '"PR_CHECK_LOCALS"."LOCAL_TAX"'
    end
    object PR_CHECK_LOCALSEXCLUDE_LOCAL: TStringField
      FieldName = 'EXCLUDE_LOCAL'
      Origin = '"PR_CHECK_LOCALS"."EXCLUDE_LOCAL"'
      Required = True
      Size = 1
    end
    object PR_CHECK_LOCALSOVERRIDE_AMOUNT: TFloatField
      FieldName = 'OVERRIDE_AMOUNT'
      Origin = '"PR_CHECK_LOCALS"."OVERRIDE_AMOUNT"'
    end
    object PR_CHECK_LOCALSLOCAL_SHORTFALL: TFloatField
      FieldName = 'LOCAL_SHORTFALL'
      Origin = '"PR_CHECK_LOCALS"."LOCAL_SHORTFALL"'
    end
    object PR_CHECK_LOCALSLocal_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name_Lookup'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object PR_CHECK_LOCALSPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_CHECK_LOCALSLOCAL_GROSS_WAGES: TFloatField
      FieldName = 'LOCAL_GROSS_WAGES'
    end
    object PR_CHECK_LOCALSCO_LOCATIONS_NBR: TIntegerField
      FieldName = 'CO_LOCATIONS_NBR'
    end
    object PR_CHECK_LOCALSLocation: TStringField
      FieldKind = fkLookup
      FieldName = 'Location'
      LookupDataSet = DM_CO_LOCATIONS.CO_LOCATIONS
      LookupKeyFields = 'CO_LOCATIONS_NBR'
      LookupResultField = 'ACCOUNT_NUMBER'
      KeyFields = 'CO_LOCATIONS_NBR'
      Lookup = True
    end
    object PR_CHECK_LOCALSNonRes: TStringField
      FieldKind = fkLookup
      FieldName = 'NonRes'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'NONRES_EE_LOCALS_NBR'
      Lookup = True
    end
    object PR_CHECK_LOCALSNONRES_EE_LOCALS_NBR: TIntegerField
      FieldName = 'NONRES_EE_LOCALS_NBR'
    end
    object PR_CHECK_LOCALSREPORTABLE: TStringField
      FieldName = 'REPORTABLE'
      Size = 1
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 288
    Top = 96
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 408
    Top = 88
  end
end
