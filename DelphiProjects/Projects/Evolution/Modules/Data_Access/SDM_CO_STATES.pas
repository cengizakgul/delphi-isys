// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_STATES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, EvContext, EvClientDataSet;

type
  TDM_CO_STATES = class(TDM_ddTable)
    CO_STATES: TCO_STATES;
    CO_STATESCO_NBR: TIntegerField;
    CO_STATESCO_STATES_NBR: TIntegerField;
    CO_STATESEE_SDI_GENERAL_LEDGER_TAG: TStringField;
    CO_STATESEE_SDI_OFFSET_GL_TAG: TStringField;
    CO_STATESER_SDI_GENERAL_LEDGER_TAG: TStringField;
    CO_STATESER_SDI_OFFSET_GL_TAG: TStringField;
    CO_STATESFILLER: TStringField;
    CO_STATESFINAL_TAX_RETURN: TStringField;
    CO_STATESIGNORE_STATE_TAX_DEP_THRESHOLD: TStringField;
    CO_STATESNOTES: TBlobField;
    CO_STATESSTATE: TStringField;
    CO_STATESSTATE_EFT_EIN: TStringField;
    CO_STATESSTATE_EFT_ENROLLMENT_STATUS: TStringField;
    CO_STATESSTATE_EFT_NAME: TStringField;
    CO_STATESSTATE_EFT_PIN_NUMBER: TStringField;
    CO_STATESSTATE_EIN: TStringField;
    CO_STATESSTATE_EXEMPT: TStringField;
    CO_STATESSTATE_GENERAL_LEDGER_TAG: TStringField;
    CO_STATESSTATE_NON_PROFIT: TStringField;
    CO_STATESSTATE_OFFSET_GL_TAG: TStringField;
    CO_STATESSTATE_SDI_EFT_EIN: TStringField;
    CO_STATESSTATE_SDI_EIN: TStringField;
    CO_STATESSTATE_TAX_DEPOSIT_METHOD: TStringField;
    CO_STATESSUI_EFT_EIN: TStringField;
    CO_STATESSUI_EFT_ENROLLMENT_STATUS: TStringField;
    CO_STATESSUI_EFT_NAME: TStringField;
    CO_STATESSUI_EFT_PIN_NUMBER: TStringField;
    CO_STATESSUI_EIN: TStringField;
    CO_STATESSUI_EXEMPT: TStringField;
    CO_STATESSUI_TAX_DEPOSIT_FREQUENCY: TStringField;
    CO_STATESSUI_TAX_DEPOSIT_METHOD: TStringField;
    CO_STATESSY_STATES_NBR: TIntegerField;
    CO_STATESSY_STATE_DEPOSIT_FREQ_NBR: TIntegerField;
    CO_STATESTAX_RETURN_CODE: TStringField;
    CO_STATESUSE_DBA_ON_TAX_RETURN: TStringField;
    CO_STATESUSE_STATE_FOR_SUI: TStringField;
    CO_STATESName: TStringField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    CO_STATESMO_TAX_CREDIT_ACTIVE: TStringField;
    CO_STATESEE_SDI_EXEMPT: TStringField;
    CO_STATESER_SDI_EXEMPT: TStringField;
    CO_STATESAPPLIED_FOR: TStringField;
    CO_STATESTaxCollectionDistrict: TIntegerField;
    CO_STATESTCD_DEPOSIT_FREQUENCY_NBR: TIntegerField;
    procedure CO_STATESAfterPost(DataSet: TDataSet);
    procedure CO_STATESBeforePost(DataSet: TDataSet);
    procedure CO_STATESAfterInsert(DataSet: TDataSet);
    procedure CO_STATESSDI_EXEMPTValidate(Sender: TField);
    procedure CO_STATESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FFirstAddedState: Boolean;
  public
    { Public declarations }
  end;

var
  DM_CO_STATES: TDM_CO_STATES;

implementation

{$R *.DFM}

uses
  EvUIUtils, EvUtils, EvConsts;

procedure TDM_CO_STATES.CO_STATESAfterPost(DataSet: TDataSet);
begin
  if FFirstAddedState and not Unattended then
    EvMessage('Don''t forget to go to Company Info Screen and add Home State',mtInformation,[mbOK]);
end;


procedure TDM_CO_STATES.CO_STATESBeforePost(DataSet: TDataSet);
begin
  DM_SYSTEM_STATE.SY_STATES.Activate;
  if VarIsNull(DataSet['STATE']) then
    raise EUpdateError.CreateHelp('Check if state is selected', IDH_ConsistencyViolation);
  Dataset['SY_STATES_NBR'] := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', DataSet['STATE'], 'SY_STATES_NBR');
  if VarIsNull(Dataset['SY_STATES_NBR']) then
    raise EUpdateError.CreateHelp('Can''t find state "'+ DataSet['STATE']+ '" in system table', IDH_ConsistencyViolation);
  FFirstAddedState := (DataSet.RecordCount = 1) and (DataSet.State = dsInsert);

  if (DataSet['STATE'] = 'MO') and (ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 21, 1) = '') then
    PutIntoFiller(DataSet.FieldByName('FILLER').AsString, GROUP_BOX_YES, 21, 1);
end;

procedure TDM_CO_STATES.CO_STATESAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('SY_STATES_NBR').Required := False;
end;

procedure TDM_CO_STATES.CO_STATESSDI_EXEMPTValidate(Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

procedure TDM_CO_STATES.CO_STATESCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.Active then
      DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.DataRequired('ALL');

  if (DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger > 0)  then
      if DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').Value, []) then
       DataSet.FieldByName('TaxCollectionDistrict').Value := DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.FieldByName('SY_GLOBAL_AGENCY_NBR').Value;
end;

initialization
  RegisterClass(TDM_CO_STATES);

finalization
  UnregisterClass(TDM_CO_STATES);

end.
