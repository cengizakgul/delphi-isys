inherited DM_CL_HR_PERSON_SKILLS: TDM_CL_HR_PERSON_SKILLS
  OldCreateOrder = False
  Left = 358
  Top = 168
  Height = 540
  Width = 783
  object CL_HR_PERSON_SKILLS: TCL_HR_PERSON_SKILLS
    Aggregates = <>
    Params = <>
    ProviderName = 'CL_HR_PERSON_SKILLS_PROV'
    ValidateWithMask = True
    Left = 104
    Top = 80
    object CL_HR_PERSON_SKILLSCL_HR_PERSON_SKILLS_NBR: TIntegerField
      FieldName = 'CL_HR_PERSON_SKILLS_NBR'
    end
    object CL_HR_PERSON_SKILLSCL_HR_SKILLS_NBR: TIntegerField
      FieldName = 'CL_HR_SKILLS_NBR'
    end
    object CL_HR_PERSON_SKILLSCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object CL_HR_PERSON_SKILLSNOTES: TBlobField
      FieldName = 'NOTES'
      BlobType = ftBlob
      Size = 8
    end
    object CL_HR_PERSON_SKILLSYEARS_EXPERIENCE: TFloatField
      FieldName = 'YEARS_EXPERIENCE'
      DisplayFormat = '#,##0.00'
    end
    object CL_HR_PERSON_SKILLSSkill: TStringField
      FieldKind = fkLookup
      FieldName = 'Skill'
      LookupDataSet = DM_CL_HR_SKILLS.CL_HR_SKILLS
      LookupKeyFields = 'CL_HR_SKILLS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CL_HR_SKILLS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 240
    Top = 56
  end
end
