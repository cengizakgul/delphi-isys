inherited DM_EE_HR_PROPERTY_TRACKING: TDM_EE_HR_PROPERTY_TRACKING
  OldCreateOrder = False
  Left = 379
  Top = 103
  Height = 540
  Width = 783
  object EE_HR_PROPERTY_TRACKING: TEE_HR_PROPERTY_TRACKING
    Aggregates = <>
    Params = <>
    ProviderName = 'EE_HR_PROPERTY_TRACKING_PROV'
    ValidateWithMask = True
    Left = 88
    Top = 56
    object EE_HR_PROPERTY_TRACKINGCO_HR_PROPERTY_NBR: TIntegerField
      FieldName = 'CO_HR_PROPERTY_NBR'
    end
    object EE_HR_PROPERTY_TRACKINGEE_HR_PROPERTY_TRACKING_NBR: TIntegerField
      FieldName = 'EE_HR_PROPERTY_TRACKING_NBR'
    end
    object EE_HR_PROPERTY_TRACKINGEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_HR_PROPERTY_TRACKINGISSUED_DATE: TDateField
      FieldName = 'ISSUED_DATE'
    end
    object EE_HR_PROPERTY_TRACKINGLOST_DATE: TDateField
      FieldName = 'LOST_DATE'
    end
    object EE_HR_PROPERTY_TRACKINGRENEWAL_DATE: TDateField
      FieldName = 'RENEWAL_DATE'
    end
    object EE_HR_PROPERTY_TRACKINGRETURNED_DATE: TDateField
      FieldName = 'RETURNED_DATE'
    end
    object EE_HR_PROPERTY_TRACKINGProperty: TStringField
      FieldKind = fkLookup
      FieldName = 'Property'
      LookupDataSet = DM_CO_HR_PROPERTY.CO_HR_PROPERTY
      LookupKeyFields = 'CO_HR_PROPERTY_NBR'
      LookupResultField = 'PROPERTY_DESCRIPTION'
      KeyFields = 'CO_HR_PROPERTY_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 256
    Top = 56
  end
end
