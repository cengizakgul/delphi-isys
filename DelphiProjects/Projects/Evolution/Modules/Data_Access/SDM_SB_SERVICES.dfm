inherited DM_SB_SERVICES: TDM_SB_SERVICES
  OldCreateOrder = True
  Left = 656
  Top = 237
  Height = 540
  Width = 783
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 240
    Top = 64
  end
  object SB_SERVICES: TSB_SERVICES
    ProviderName = 'SB_SERVICES_PROV'
    OnCalcFields = SB_SERVICESCalcFields
    Left = 408
    Top = 144
    object SB_SERVICESSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
    end
    object SB_SERVICESSERVICE_NAME: TStringField
      FieldName = 'SERVICE_NAME'
      Size = 40
    end
    object SB_SERVICESFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Size = 1
    end
    object SB_SERVICESAR_ACCT: TStringField
      FieldKind = fkCalculated
      FieldName = 'AR_ACCT'
      Size = 50
      Calculated = True
    end
    object SB_SERVICESINC_ACCT: TStringField
      FieldKind = fkCalculated
      FieldName = 'INC_ACCT'
      Size = 50
      Calculated = True
    end
    object SB_SERVICESITEM: TStringField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Size = 70
      Calculated = True
    end
    object SB_SERVICESMONTH_NUMBER: TStringField
      FieldName = 'MONTH_NUMBER'
      Size = 2
    end
    object SB_SERVICESSB_REPORTS_NBR: TIntegerField
      FieldName = 'SB_REPORTS_NBR'
    end
    object SB_SERVICESCOMMISSION: TStringField
      FieldName = 'COMMISSION'
      Size = 1
    end
    object SB_SERVICESSALES_TAXABLE: TStringField
      FieldName = 'SALES_TAXABLE'
      Size = 1
    end
    object SB_SERVICESFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
  end
end
