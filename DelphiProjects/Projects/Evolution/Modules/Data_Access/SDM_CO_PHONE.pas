// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_PHONE;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses, SFieldCodeValues, EvUtils;

type
  TDM_CO_PHONE = class(TDM_ddTable)
    CO_PHONE: TCO_PHONE;
    CO_PHONEdescPhoneType: TStringField;
    CO_PHONEPHONE_TYPE: TStringField;
    procedure CO_PHONECalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CO_PHONE.CO_PHONECalcFields(DataSet: TDataSet);
begin
  inherited;

  if not CO_PHONEPHONE_TYPE.isnull then
     CO_PHONEdescPhoneType.AsString :=
            ReturnDescription(Company_PhoneType_ComboChoices, CO_PHONEPHONE_TYPE.AsString);

end;

initialization
  RegisterClass(TDM_CO_PHONE);

finalization
  UnregisterClass(TDM_CO_PHONE);

end.

