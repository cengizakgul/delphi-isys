inherited DM_SB_REPORTS: TDM_SB_REPORTS
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object SB_REPORTS: TSB_REPORTS
    ProviderName = 'SB_REPORTS_PROV'
    OnCalcFields = SB_REPORTSCalcFields
    Left = 86
    Top = 36
    object SB_REPORTSRWDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'RWDescription'
      Size = 8000
    end
    object SB_REPORTSSB_REPORTS_NBR: TIntegerField
      FieldName = 'SB_REPORTS_NBR'
      Origin = '"SB_REPORTS_HIST"."SB_REPORTS_NBR"'
      Required = True
    end
    object SB_REPORTSREPORT_WRITER_REPORTS_NBR: TIntegerField
      FieldName = 'REPORT_WRITER_REPORTS_NBR'
      Origin = '"SB_REPORTS_HIST"."REPORT_WRITER_REPORTS_NBR"'
      Required = True
    end
    object SB_REPORTSREPORT_LEVEL: TStringField
      FieldName = 'REPORT_LEVEL'
      Origin = '"SB_REPORTS_HIST"."REPORT_LEVEL"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SB_REPORTSDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"SB_REPORTS_HIST"."DESCRIPTION"'
      Required = True
      Size = 40
    end
    object SB_REPORTSCOMMENTS: TBlobField
      FieldName = 'COMMENTS'
      Origin = '"SB_REPORTS_HIST"."COMMENTS"'
      Size = 8
    end
    object SB_REPORTSINPUT_PARAMS: TBlobField
      FieldName = 'INPUT_PARAMS'
      Origin = '"SB_REPORTS_HIST"."INPUT_PARAMS"'
      Size = 8
    end
    object SB_REPORTSNDescription: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NDescription'
      Size = 60
    end
  end
end
