inherited DM_CO_VENDOR: TDM_CO_VENDOR
  OldCreateOrder = True
  Left = 251
  Top = 192
  Height = 480
  Width = 696
  object DM_CLIENT: TDM_CLIENT
    Left = 136
    Top = 144
  end
  object CO_VENDOR: TCO_VENDOR
    ProviderName = 'CO_VENDOR_PROV'
    OnCalcFields = CO_VENDORCalcFields
    Left = 136
    Top = 80
    object CO_VENDORSB_VENDOR_NBR: TIntegerField
      FieldName = 'SB_VENDOR_NBR'
    end
    object CO_VENDORVENDOR_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'VENDOR_NAME'
      Size = 80
      Calculated = True
    end
    object CO_VENDORCO_VENDOR_NBR: TIntegerField
      FieldName = 'CO_VENDOR_NBR'
    end
    object CO_VENDORCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
  end
end
