// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_JOBS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, EvClientDataSet;

type
  TDM_CO_JOBS = class(TDM_ddTable)
    CO_JOBS: TCO_JOBS;
    CO_JOBSCERTIFIED: TStringField;
    CO_JOBSCO_JOBS_NBR: TIntegerField;
    CO_JOBSCO_NBR: TIntegerField;
    CO_JOBSCO_WORKERS_COMP_NBR: TIntegerField;
    CO_JOBSDESCRIPTION: TStringField;
    CO_JOBSFILLER: TStringField;
    CO_JOBSGENERAL_LEDGER_TAG: TStringField;
    CO_JOBSJOB_ACTIVE: TStringField;
    CO_JOBSMISC_JOB_CODE: TStringField;
    CO_JOBSRATE_PER_HOUR: TFloatField;
    CO_JOBSSTATE_CERTIFIED: TStringField;
    CO_JOBSTRUE_DESCRIPTION: TStringField;
    CO_JOBSHOME_CO_STATES_NBR: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}
initialization
  RegisterClass( TDM_CO_JOBS );

finalization
  UnregisterClass( TDM_CO_JOBS );

end.
