inherited DM_CO_BENEFIT_CATEGORY: TDM_CO_BENEFIT_CATEGORY
  OldCreateOrder = True
  Left = 520
  Top = 200
  Height = 479
  Width = 741
  object CO_BENEFIT_CATEGORY: TCO_BENEFIT_CATEGORY
    ProviderName = 'CO_BENEFIT_CATEGORY_PROV'
    OnCalcFields = CO_BENEFIT_CATEGORYCalcFields
    Left = 216
    Top = 192
    object CO_BENEFIT_CATEGORYCO_BENEFIT_CATEGORY_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_CATEGORY_NBR'
    end
    object CO_BENEFIT_CATEGORYCATEGORY_TYPE: TStringField
      FieldName = 'CATEGORY_TYPE'
      Size = 1
    end
    object CO_BENEFIT_CATEGORYName: TStringField
      FieldKind = fkCalculated
      FieldName = 'Name'
      Size = 30
      Calculated = True
    end
    object CO_BENEFIT_CATEGORYCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 200
    Top = 72
  end
end
