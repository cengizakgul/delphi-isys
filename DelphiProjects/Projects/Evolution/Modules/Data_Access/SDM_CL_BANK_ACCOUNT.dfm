inherited DM_CL_BANK_ACCOUNT: TDM_CL_BANK_ACCOUNT
  OldCreateOrder = True
  Left = 423
  Top = 117
  Height = 284
  Width = 471
  object CL_BANK_ACCOUNT: TCL_BANK_ACCOUNT
    ProviderName = 'CL_BANK_ACCOUNT_PROV'
    PictureMasks.Strings = (
      
        'BEGINNING_BALANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F')
    FieldDefs = <
      item
        Name = 'CUSTOM_BANK_ACCOUNT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'BANK_ACCOUNT_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_BANKS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BEGINNING_BALANCE'
        DataType = ftFloat
      end
      item
        Name = 'NEXT_CHECK_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SIGNATURE_CL_BLOB_NBR'
        DataType = ftInteger
      end
      item
        Name = 'LOGO_CL_BLOB_NBR'
        DataType = ftInteger
      end>
    Left = 48
    Top = 68
    object CL_BANK_ACCOUNTCUSTOM_BANK_ACCOUNT_NUMBER: TStringField
      DisplayLabel = 'Bank Account Number'
      DisplayWidth = 32
      FieldName = 'CUSTOM_BANK_ACCOUNT_NUMBER'
    end
    object CL_BANK_ACCOUNTaba_nbr_lookup: TStringField
      DisplayLabel = 'ABA Nbr'
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'aba_nbr_lookup'
      LookupDataSet = DM_SB_BANKS.SB_BANKS
      LookupKeyFields = 'SB_BANKS_NBR'
      LookupResultField = 'ABA_NUMBER'
      KeyFields = 'SB_BANKS_NBR'
      Lookup = True
    end
    object CL_BANK_ACCOUNTPrint_Name_Lookup: TStringField
      DisplayLabel = 'Print Name'
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'Print_Name_Lookup'
      LookupDataSet = DM_SB_BANKS.SB_BANKS
      LookupKeyFields = 'SB_BANKS_NBR'
      LookupResultField = 'PRINT_NAME'
      KeyFields = 'SB_BANKS_NBR'
      Lookup = True
    end
    object CL_BANK_ACCOUNTBank_Name: TStringField
      DisplayLabel = 'Bank Name'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'Bank_Name'
      LookupDataSet = DM_SB_BANKS.SB_BANKS
      LookupKeyFields = 'SB_BANKS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SB_BANKS_NBR'
      Visible = False
      Size = 40
      Lookup = True
    end
    object CL_BANK_ACCOUNTBANK_ACCOUNT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'BANK_ACCOUNT_TYPE'
      Visible = False
      Size = 1
    end
    object CL_BANK_ACCOUNTCL_BANK_ACCOUNT_NBR: TIntegerField
      FieldName = 'CL_BANK_ACCOUNT_NBR'
      Visible = False
    end
    object CL_BANK_ACCOUNTSB_BANKS_NBR: TIntegerField
      FieldName = 'SB_BANKS_NBR'
      Visible = False
    end
    object CL_BANK_ACCOUNTBEGINNING_BALANCE: TFloatField
      FieldName = 'BEGINNING_BALANCE'
      Visible = False
    end
    object CL_BANK_ACCOUNTNEXT_CHECK_NUMBER: TIntegerField
      FieldName = 'NEXT_CHECK_NUMBER'
      Visible = False
    end
    object CL_BANK_ACCOUNTSIGNATURE_CL_BLOB_NBR: TIntegerField
      FieldName = 'SIGNATURE_CL_BLOB_NBR'
    end
    object CL_BANK_ACCOUNTLOGO_CL_BLOB_NBR: TIntegerField
      FieldName = 'LOGO_CL_BLOB_NBR'
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 48
    Top = 129
  end
end
