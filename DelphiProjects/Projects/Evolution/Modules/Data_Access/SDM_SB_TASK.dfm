inherited DM_SB_TASK: TDM_SB_TASK
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 489
  Top = 367
  Height = 308
  Width = 498
  object SB_TASK: TSB_TASK
    ProviderName = 'SB_TASK_PROV'
    FieldDefs = <
      item
        Name = 'SB_TASK_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'SB_USER_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'SCHEDULE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DESCRIPTION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'TASK'
        Attributes = [faRequired]
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'SCHED_DESC'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'USER_NAME'
        DataType = ftString
        Size = 80
      end>
    OnCalcFields = SB_TASKCalcFields
    OnNewRecord = SB_TASKNewRecord
    DeleteChildren = True
    BlobsLoadMode = blmPreload
    OnAddLookups = SB_TASKAddLookups
    Left = 51
    Top = 48
    object SB_TASKSB_TASK_NBR: TIntegerField
      FieldName = 'SB_TASK_NBR'
      Origin = '"SB_TASK_HIST"."SB_TASK_NBR"'
      Required = True
    end
    object SB_TASKSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
      Origin = '"SB_TASK_HIST"."SB_USER_NBR"'
      Required = True
    end
    object SB_TASKSCHEDULE: TStringField
      FieldName = 'SCHEDULE'
      Origin = '"SB_TASK_HIST"."SCHEDULE"'
      Required = True
      Size = 255
    end
    object SB_TASKDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"SB_TASK_HIST"."DESCRIPTION"'
      Required = True
      Size = 255
    end
    object SB_TASKTASK: TBlobField
      FieldName = 'TASK'
      Origin = '"SB_TASK_HIST"."TASK"'
      Required = True
      Size = 8
    end
    object SB_TASKNAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NAME'
      Size = 255
    end
    object SB_TASKSCHED_DESC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SCHED_DESC'
      Size = 512
    end
    object SB_TASKUSER_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'USER_NAME'
      Size = 80
    end
    object SB_TASKTaskCaption: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TaskCaption'
      Size = 100
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 156
    Top = 51
  end
end
