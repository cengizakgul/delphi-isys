unit SDM_CO_BENEFIT_CATEGORY;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils, SFieldCodeValues,
  EvClientDataSet;

type
  TDM_CO_BENEFIT_CATEGORY = class(TDM_ddTable)
    CO_BENEFIT_CATEGORY: TCO_BENEFIT_CATEGORY;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    CO_BENEFIT_CATEGORYCO_BENEFIT_CATEGORY_NBR: TIntegerField;
    CO_BENEFIT_CATEGORYCATEGORY_TYPE: TStringField;
    CO_BENEFIT_CATEGORYName: TStringField;
    CO_BENEFIT_CATEGORYCO_NBR: TIntegerField;
    procedure CO_BENEFIT_CATEGORYCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BENEFIT_CATEGORY: TDM_CO_BENEFIT_CATEGORY;

implementation

{$R *.DFM}



procedure TDM_CO_BENEFIT_CATEGORY.CO_BENEFIT_CATEGORYCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if not CO_BENEFIT_CATEGORYCATEGORY_TYPE.IsNull then
      CO_BENEFIT_CATEGORYName.AsString :=
                ReturnDescription(Benefit_Category_Type_ComboChoices, CO_BENEFIT_CATEGORYCATEGORY_TYPE.AsString);

end;

initialization
  RegisterClass(TDM_CO_BENEFIT_CATEGORY);

finalization
  UnregisterClass(TDM_CO_BENEFIT_CATEGORY);

end.
