inherited DM_CO_GROUP: TDM_CO_GROUP
  OldCreateOrder = True
  Left = 151
  Top = 194
  Height = 540
  Width = 783
  object CO_GROUP: TCO_GROUP
    ProviderName = 'CO_GROUP_PROV'
    FieldDefs = <
      item
        Name = 'CO_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GROUP_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GROUP_NAME'
        DataType = ftString
        Size = 50
      end>
    OnCalcFields = CO_GROUPCalcFields
    Left = 72
    Top = 32
    object CO_GROUPCO_GROUP_NBR: TIntegerField
      FieldName = 'CO_GROUP_NBR'
    end
    object CO_GROUPCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_GROUPGROUP_TYPE: TStringField
      FieldName = 'GROUP_TYPE'
      Size = 1
    end
    object CO_GROUPGROUP_NAME: TStringField
      FieldName = 'GROUP_NAME'
      Size = 50
    end
    object CO_GROUPGROUP_TYPE_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'GROUP_TYPE_DESC'
      Calculated = True
    end
  end
end
