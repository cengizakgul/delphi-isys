// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_DELIVERY_METHOD;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, SDataStructure, DB, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  EvClientDataSet;

type
  TDM_SY_DELIVERY_METHOD = class(TDM_ddTable)
    SY_DELIVERY_METHOD: TSY_DELIVERY_METHOD;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SY_DELIVERY_METHODSY_DELIVERY_METHOD_NBR: TIntegerField;
    SY_DELIVERY_METHODSY_DELIVERY_SERVICE_NBR: TIntegerField;
    SY_DELIVERY_METHODCLASS_NAME: TStringField;
    SY_DELIVERY_METHODNAME: TStringField;
    SY_DELIVERY_METHODSERVICE_NAME: TStringField;
    SY_DELIVERY_METHODSERVICE_CLASS_NAME: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_SY_DELIVERY_METHOD);

finalization
  UnregisterClass(TDM_SY_DELIVERY_METHOD);

end.
