// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_DEPT_PR_BATCH_DEFLT_ED;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, EvUIComponents, EvClientDataSet;

type
  TDM_CO_DEPT_PR_BATCH_DEFLT_ED = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    CO_DEPT_PR_BATCH_DEFLT_ED: TCO_DEPT_PR_BATCH_DEFLT_ED;
    CO_DEPT_PR_BATCH_DEFLT_EDCO_DEPT_PR_BATCH_DEFLT_ED_NBR: TIntegerField;
    CO_DEPT_PR_BATCH_DEFLT_EDCO_DEPARTMENT_NBR: TIntegerField;
    CO_DEPT_PR_BATCH_DEFLT_EDCO_E_D_CODES_NBR: TIntegerField;
    CO_DEPT_PR_BATCH_DEFLT_EDSALARY_HOURLY_OR_BOTH: TStringField;
    CO_DEPT_PR_BATCH_DEFLT_EDOVERRIDE_RATE: TFloatField;
    CO_DEPT_PR_BATCH_DEFLT_EDOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CO_DEPT_PR_BATCH_DEFLT_EDOVERRIDE_HOURS: TFloatField;
    CO_DEPT_PR_BATCH_DEFLT_EDOVERRIDE_AMOUNT: TFloatField;
    CO_DEPT_PR_BATCH_DEFLT_EDOVERRIDE_LINE_ITEM_DATE: TDateField;
    CO_DEPT_PR_BATCH_DEFLT_EDFILLER: TStringField;
    CO_DEPT_PR_BATCH_DEFLT_EDED_Lookup: TStringField;
    CO_DEPT_PR_BATCH_DEFLT_EDFILLTRIM: TStringField;
    CO_DEPT_PR_BATCH_DEFLT_EDCO_JOBS_NBR: TIntegerField;
    procedure CO_DEPT_PR_BATCH_DEFLT_EDBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_DEPT_PR_BATCH_DEFLT_ED: TDM_CO_DEPT_PR_BATCH_DEFLT_ED;

implementation

{$R *.DFM}

procedure TDM_CO_DEPT_PR_BATCH_DEFLT_ED.CO_DEPT_PR_BATCH_DEFLT_EDBeforePost(
  DataSet: TDataSet);
var
  t: TevClientDataSet;
  iNext: integer;
begin
  if Trim(Copy(DataSet.FieldByName('FILLER').AsString, 1, 3)) = '' then
  begin
    t := TevClientDataSet.Create(Self);
    try
      t.CloneCursor(TevClientDataSet(DataSet), True);
      t.Filter := 'CO_DEPARTMENT_NBR=' + IntToStr(DM_COMPANY.CO_DEPARTMENT.FieldByName('CO_DEPARTMENT_NBR').AsInteger);
      t.Filtered := True;
      t.IndexFieldNames := 'FILLTRIM';
      t.Last;
      if t.FieldByName('FILLTRIM').AsString = '' then
        iNext := 0
      else
        iNext := t.FieldByName('FILLTRIM').AsInteger;
      DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [Succ(iNext)]) + Copy(DataSet.FieldByName('FILLER').AsString, 4,
        Length(DataSet.FieldByName('FILLER').AsString));
      DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [Succ(iNext)]);
    finally
      t.Free;
    end;
  end;
end;

initialization
  RegisterClass(TDM_CO_DEPT_PR_BATCH_DEFLT_ED);

finalization
  UnregisterClass(TDM_CO_DEPT_PR_BATCH_DEFLT_ED);

end.
