// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_FED_REPORTING_AGENCY;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_SY_FED_REPORTING_AGENCY = class(TDM_ddTable)
    SY_FED_REPORTING_AGENCY: TSY_FED_REPORTING_AGENCY;
    SY_FED_REPORTING_AGENCYNAME: TStringField;
    SY_FED_REPORTING_AGENCYSY_GLOBAL_AGENCY_NBR: TIntegerField;
    SY_FED_REPORTING_AGENCYSY_FED_REPORTING_AGENCY_NBR: TIntegerField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_SY_FED_REPORTING_AGENCY);

finalization
  UnregisterClass(TDM_SY_FED_REPORTING_AGENCY);

end.
