inherited DM_CO_WORKERS_COMP: TDM_CO_WORKERS_COMP
  OldCreateOrder = True
  Left = 316
  Top = 261
  Height = 479
  Width = 741
  object CO_WORKERS_COMP: TCO_WORKERS_COMP
    ProviderName = 'CO_WORKERS_COMP_PROV'
    PictureMasks.Strings = (
      
        'RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]' +
        ',({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-' +
        ']{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'EXPERIENCE_RATING'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F')
    Left = 216
    Top = 144
    object CO_WORKERS_COMPCO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"CO_WORKERS_COMP_HIST"."CO_WORKERS_COMP_NBR"'
      Required = True
    end
    object CO_WORKERS_COMPCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Origin = '"CO_WORKERS_COMP_HIST"."CO_NBR"'
      Required = True
    end
    object CO_WORKERS_COMPCO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
      Origin = '"CO_WORKERS_COMP_HIST"."CO_STATES_NBR"'
      Required = True
    end
    object CO_WORKERS_COMPDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"CO_WORKERS_COMP_HIST"."DESCRIPTION"'
      Required = True
      Size = 40
    end
    object CO_WORKERS_COMPRATE: TFloatField
      FieldName = 'RATE'
      Origin = '"CO_WORKERS_COMP_HIST"."RATE"'
      DisplayFormat = '#,##0.00####'
    end
    object CO_WORKERS_COMPEXPERIENCE_RATING: TFloatField
      FieldName = 'EXPERIENCE_RATING'
      Origin = '"CO_WORKERS_COMP_HIST"."EXPERIENCE_RATING"'
      DisplayFormat = '#,##0.00####'
    end
    object CO_WORKERS_COMPCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
      Origin = '"CO_WORKERS_COMP_HIST"."CL_E_D_GROUPS_NBR"'
    end
    object CO_WORKERS_COMPGL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GL_TAG'
    end
    object CO_WORKERS_COMPCo_State_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Co_State_Lookup'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object CO_WORKERS_COMPStateName: TStringField
      FieldKind = fkLookup
      FieldName = 'StateName'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'Name'
      KeyFields = 'CO_STATES_NBR'
      Lookup = True
    end
    object CO_WORKERS_COMPSyState: TIntegerField
      FieldKind = fkLookup
      FieldName = 'SyState'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'SY_STATES_NBR'
      KeyFields = 'CO_STATES_NBR'
      Lookup = True
    end
    object CO_WORKERS_COMPEDGroupName: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGroupName'
      LookupDataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
      LookupKeyFields = 'CL_E_D_GROUPS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_E_D_GROUPS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_WORKERS_COMPGL_OFFSET_TAG: TStringField
      FieldName = 'GL_OFFSET_TAG'
    end
    object CO_WORKERS_COMPSUBTRACT_PREMIUM: TStringField
      FieldName = 'SUBTRACT_PREMIUM'
      Size = 1
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 216
    Top = 80
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 328
    Top = 128
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 312
    Top = 40
  end
end
