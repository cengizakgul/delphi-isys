// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_REPORTS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure;  

type
  TDM_PR_REPORTS = class(TDM_ddTable)
    PR_REPORTS: TPR_REPORTS;
    DM_COMPANY: TDM_COMPANY;
    PR_REPORTSPR_REPORTS_NBR: TIntegerField;
    PR_REPORTSPR_NBR: TIntegerField;
    PR_REPORTSCO_REPORTS_NBR: TIntegerField;
    PR_REPORTSReportName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_REPORTS: TDM_PR_REPORTS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_PR_REPORTS);

finalization
  UnregisterClass(TDM_PR_REPORTS);

end.
