inherited DM_TMP_CO: TDM_TMP_CO
  OldCreateOrder = True
  Left = 576
  Top = 203
  Height = 214
  Width = 443
  object TMP_CO: TTMP_CO
    ProviderName = 'TMP_CO_PROV'
    FieldDefs = <
      item
        Name = 'ACH_SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'AUTO_ENLIST'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_CO_CONSOLIDATION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOMER_SERVICE_SB_USER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DBA'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'EFTPS_ENROLLMENT_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'EFTPS_ENROLLMENT_NUMBER'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'EFTPS_ENROLLMENT_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EFTPS_SEQUENCE_NUMBER'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'FEDERAL_TAX_DEPOSIT_FREQUENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_TAX_PAYMENT_METHOD'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'OBC'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PROCESS_PRIORITY'
        DataType = ftInteger
      end
      item
        Name = 'START_DATE'
        DataType = ftDate
      end
      item
        Name = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_SERVICE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TERMINATION_CODE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TERMINATION_DATE'
        DataType = ftDate
      end
      item
        Name = 'TRUST_SERVICE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TERMINATION_CODE_DESC'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Selected'
        DataType = ftString
        Size = 1
      end>
    OnCalcFields = TMP_COCalcFields
    OnPrepareLookups = TMP_COPrepareLookups
    Left = 45
    Top = 27
    object TMP_COACH_SB_BANK_ACCOUNT_NBR: TIntegerField
      FieldName = 'ACH_SB_BANK_ACCOUNT_NBR'
    end
    object TMP_COAUTO_ENLIST: TStringField
      FieldName = 'AUTO_ENLIST'
      Size = 1
    end
    object TMP_COCL_CO_CONSOLIDATION_NBR: TIntegerField
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
    end
    object TMP_COCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object TMP_COCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object TMP_COCUSTOMER_SERVICE_SB_USER_NBR: TIntegerField
      FieldName = 'CUSTOMER_SERVICE_SB_USER_NBR'
    end
    object TMP_COCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object TMP_COCUSTOM_CLIENT_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CUSTOM_CLIENT_NUMBER'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Lookup = True
    end
    object TMP_CODBA: TStringField
      FieldName = 'DBA'
      Size = 40
    end
    object TMP_COEFTPS_ENROLLMENT_DATE: TDateTimeField
      FieldName = 'EFTPS_ENROLLMENT_DATE'
    end
    object TMP_COEFTPS_ENROLLMENT_NUMBER: TStringField
      FieldName = 'EFTPS_ENROLLMENT_NUMBER'
      Size = 4
    end
    object TMP_COEFTPS_ENROLLMENT_STATUS: TStringField
      FieldName = 'EFTPS_ENROLLMENT_STATUS'
      Size = 1
    end
    object TMP_COEFTPS_SEQUENCE_NUMBER: TStringField
      FieldName = 'EFTPS_SEQUENCE_NUMBER'
      Size = 4
    end
    object TMP_COFEDERAL_TAX_DEPOSIT_FREQUENCY: TStringField
      FieldName = 'FEDERAL_TAX_DEPOSIT_FREQUENCY'
      Size = 1
    end
    object TMP_COFEDERAL_TAX_PAYMENT_METHOD: TStringField
      FieldName = 'FEDERAL_TAX_PAYMENT_METHOD'
      Size = 1
    end
    object TMP_COFEIN: TStringField
      FieldName = 'FEIN'
      Size = 9
    end
    object TMP_CONAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object TMP_COOBC: TStringField
      FieldName = 'OBC'
      Size = 1
    end
    object TMP_COPROCESS_PRIORITY: TIntegerField
      FieldName = 'PROCESS_PRIORITY'
    end
    object TMP_COSTART_DATE: TDateField
      FieldName = 'START_DATE'
    end
    object TMP_COSY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField
      FieldName = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
    end
    object TMP_COTAX_SERVICE: TStringField
      FieldName = 'TAX_SERVICE'
      Size = 1
    end
    object TMP_COTERMINATION_CODE: TStringField
      FieldName = 'TERMINATION_CODE'
      Size = 1
    end
    object TMP_COTERMINATION_DATE: TDateField
      FieldName = 'TERMINATION_DATE'
    end
    object TMP_COTRUST_SERVICE: TStringField
      FieldName = 'TRUST_SERVICE'
      Size = 1
    end
    object TMP_COTERMINATION_CODE_DESC: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TERMINATION_CODE_DESC'
      Size = 40
    end
    object TMP_COHOME_CO_STATES_NBR: TIntegerField
      FieldName = 'HOME_CO_STATES_NBR'
    end
    object TMP_COUserLastName: TStringField
      FieldKind = fkLookup
      FieldName = 'UserLastName'
      LookupDataSet = DM_SB_USER.SB_USER
      LookupKeyFields = 'SB_USER_NBR'
      LookupResultField = 'LAST_NAME'
      KeyFields = 'CUSTOMER_SERVICE_SB_USER_NBR'
      Size = 30
      Lookup = True
    end
    object TMP_COUserFirstName: TStringField
      FieldKind = fkLookup
      FieldName = 'UserFirstName'
      LookupDataSet = DM_SB_USER.SB_USER
      LookupKeyFields = 'SB_USER_NBR'
      LookupResultField = 'FIRST_NAME'
      KeyFields = 'CUSTOMER_SERVICE_SB_USER_NBR'
      Lookup = True
    end
    object TMP_COSelected: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Selected'
      Size = 1
    end
    object TMP_COHomeState: TStringField
      FieldKind = fkLookup
      FieldName = 'HomeState'
      LookupDataSet = evcsTmpStatesLookup
      LookupKeyFields = 'CL_NBR;CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'CL_NBR;HOME_CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 184
    Top = 72
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 160
    Top = 32
  end
  object evcsTmpStatesLookup: TevClientDataSet
    ProviderName = 'TMP_CO_STATES_PROV'
    Left = 288
    Top = 24
    object evcsTmpStatesLookupCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object evcsTmpStatesLookupCO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
    end
    object evcsTmpStatesLookupSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
  end
end
