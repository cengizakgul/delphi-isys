// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_CHECK_STATES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvConsts, EvTypes,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDDClasses, EvExceptions,
  ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_PR_CHECK_STATES = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_CHECK_STATES: TPR_CHECK_STATES;
    PR_CHECK_STATESPR_CHECK_STATES_NBR: TIntegerField;
    PR_CHECK_STATESPR_CHECK_NBR: TIntegerField;
    PR_CHECK_STATESEE_STATES_NBR: TIntegerField;
    PR_CHECK_STATESTAX_AT_SUPPLEMENTAL_RATE: TStringField;
    PR_CHECK_STATESEXCLUDE_STATE: TStringField;
    PR_CHECK_STATESEXCLUDE_SDI: TStringField;
    PR_CHECK_STATESEXCLUDE_SUI: TStringField;
    PR_CHECK_STATESEXCLUDE_ADDITIONAL_STATE: TStringField;
    PR_CHECK_STATESSTATE_OVERRIDE_TYPE: TStringField;
    PR_CHECK_STATESSTATE_OVERRIDE_VALUE: TFloatField;
    PR_CHECK_STATESSTATE_SHORTFALL: TFloatField;
    PR_CHECK_STATESSTATE_TAXABLE_WAGES: TFloatField;
    PR_CHECK_STATESSTATE_TAX: TFloatField;
    PR_CHECK_STATESEE_SDI_TAXABLE_WAGES: TFloatField;
    PR_CHECK_STATESEE_SDI_TAX: TFloatField;
    PR_CHECK_STATESER_SDI_TAXABLE_WAGES: TFloatField;
    PR_CHECK_STATESER_SDI_TAX: TFloatField;
    PR_CHECK_STATESEE_SDI_SHORTFALL: TFloatField;
    PR_CHECK_STATESEE_SDI_OVERRIDE: TFloatField;
    PR_CHECK_STATESState_Lookup: TStringField;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    PR_CHECK_STATESPR_NBR: TIntegerField;
    PR_CHECK_STATESSTATE_GROSS_WAGES: TFloatField;
    PR_CHECK_STATESEE_SDI_GROSS_WAGES: TFloatField;
    PR_CHECK_STATESER_SDI_GROSS_WAGES: TFloatField;
    PR_CHECK_STATESSDI_Lookup: TStringField;
    procedure PR_CHECK_STATESBeforeDelete(DataSet: TDataSet);
    procedure PR_CHECK_STATESBeforeEdit(DataSet: TDataSet);
    procedure PR_CHECK_STATESBeforeInsert(DataSet: TDataSet);
    procedure PR_CHECK_STATESBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_PR_CHECK_STATES: TDM_PR_CHECK_STATES;

implementation

uses
  SDataAccessCommon;

{$R *.DFM}

procedure TDM_PR_CHECK_STATES.PR_CHECK_STATESBeforeDelete(
  DataSet: TDataSet);
begin
  PayrollBeforeDelete(DataSet);
end;

procedure TDM_PR_CHECK_STATES.PR_CHECK_STATESBeforeEdit(DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_CHECK_STATES.PR_CHECK_STATESBeforeInsert(
  DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure TDM_PR_CHECK_STATES.PR_CHECK_STATESBeforePost(DataSet: TDataSet);
begin
  if (DataSet.State = dsInsert) and DataSet.FieldByName('PR_NBR').IsNull then
    DataSet.FieldByName('PR_NBR').AsInteger := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;

  if (DataSet.FieldByName('STATE_OVERRIDE_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) and
     DataSet.FieldByName('STATE_OVERRIDE_VALUE').IsNull then
    raise EUpdateError.CreateHelp('You must enter state override value', IDH_ConsistencyViolation);
end;

initialization
  RegisterClass(TDM_PR_CHECK_STATES);

finalization
  UnregisterClass(TDM_PR_CHECK_STATES);

end.
