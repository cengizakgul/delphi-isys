inherited DM_PR_BATCH: TDM_PR_BATCH
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 271
  Top = 189
  Height = 480
  Width = 696
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 88
  end
  object PR_BATCH: TPR_BATCH
    Aggregates = <>
    Params = <>
    ProviderName = 'PR_BATCH_PROV'
    BeforeInsert = PR_BATCHBeforeInsert
    AfterInsert = PR_BATCHAfterInsert
    BeforeEdit = PR_BATCHBeforeEdit
    BeforePost = PR_BATCHBeforePost
    AfterCancel = PR_BATCHAfterCancel
    BeforeDelete = PR_BATCHBeforeDelete
    ValidateWithMask = True
    DeleteChildren = True
    Left = 208
    Top = 160
    object PR_BATCHPR_BATCH_NBR: TIntegerField
      FieldName = 'PR_BATCH_NBR'
    end
    object PR_BATCHPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_BATCHCO_PR_CHECK_TEMPLATES_NBR: TIntegerField
      FieldName = 'CO_PR_CHECK_TEMPLATES_NBR'
    end
    object PR_BATCHCO_PR_FILTERS_NBR: TIntegerField
      FieldName = 'CO_PR_FILTERS_NBR'
    end
    object PR_BATCHPERIOD_BEGIN_DATE: TDateField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object PR_BATCHPERIOD_BEGIN_DATE_STATUS: TStringField
      FieldName = 'PERIOD_BEGIN_DATE_STATUS'
      Size = 1
    end
    object PR_BATCHPERIOD_END_DATE: TDateField
      FieldName = 'PERIOD_END_DATE'
    end
    object PR_BATCHPERIOD_END_DATE_STATUS: TStringField
      FieldName = 'PERIOD_END_DATE_STATUS'
      Size = 1
    end
    object PR_BATCHFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Size = 1
    end
    object PR_BATCHPAY_SALARY: TStringField
      FieldName = 'PAY_SALARY'
      Size = 1
    end
    object PR_BATCHPAY_STANDARD_HOURS: TStringField
      FieldName = 'PAY_STANDARD_HOURS'
      Size = 1
    end
    object PR_BATCHLOAD_DBDT_DEFAULTS: TStringField
      FieldName = 'LOAD_DBDT_DEFAULTS'
      Size = 1
    end
    object PR_BATCHFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object PR_BATCHTemplate_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Template_Name_Lookup'
      LookupDataSet = DM_CO_PR_CHECK_TEMPLATES.CO_PR_CHECK_TEMPLATES
      LookupKeyFields = 'CO_PR_CHECK_TEMPLATES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_PR_CHECK_TEMPLATES_NBR'
      Size = 40
      Lookup = True
    end
    object PR_BATCHEXCLUDE_TIME_OFF: TStringField
      FieldName = 'EXCLUDE_TIME_OFF'
      Size = 1
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 288
    Top = 88
  end
end
