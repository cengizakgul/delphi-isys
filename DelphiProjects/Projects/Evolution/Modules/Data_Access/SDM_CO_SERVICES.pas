// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_SERVICES;

interface

uses
  SDM_ddTable, SDataDictClient, EvUtils,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure,   kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, EvContext, EvClientDataSet;

type
  TDM_CO_SERVICES = class(TDM_ddTable)
    CO_SERVICES: TCO_SERVICES;
    CO_SERVICESCO_SERVICES_NBR: TIntegerField;
    CO_SERVICESCO_NBR: TIntegerField;
    CO_SERVICESSB_SERVICES_NBR: TIntegerField;
    CO_SERVICESNAME: TStringField;
    CO_SERVICESEFFECTIVE_START_DATE: TDateField;
    CO_SERVICESEFFECTIVE_END_DATE: TDateField;
    CO_SERVICESSALES_TAX: TStringField;
    CO_SERVICESOVERRIDE_FLAT_FEE: TFloatField;
    CO_SERVICESDISCOUNT: TFloatField;
    CO_SERVICESDISCOUNT_TYPE: TStringField;
    CO_SERVICESDISCOUNT_START_DATE: TDateField;
    CO_SERVICESFILLER: TStringField;
    CO_SERVICESFREQUENCY: TStringField;
    CO_SERVICESWEEK_NUMBER: TStringField;
    CO_SERVICESMONTH_NUMBER: TStringField;
    CO_SERVICESSort: TStringField;
    CO_SERVICESDISCOUNT_END_DATE: TDateField;
    CO_SERVICESCO_E_D_CODES_NBR: TIntegerField;
    procedure CO_SERVICESBeforePost(DataSet: TDataSet);
    procedure CO_SERVICESAfterRefresh(DataSet: TDataSet);
    procedure CO_SERVICESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    CoServicesSortTop: Integer;
  end;

implementation

{$R *.DFM}

procedure TDM_CO_SERVICES.CO_SERVICESBeforePost(DataSet: TDataSet);
begin
  if Trim(Copy(DataSet.FieldByName('Filler').AsString, 2, 3)) = '' then
  begin
    Inc(CoServicesSortTop);
    DataSet.FieldByName('Filler').AsString := Format('%1.1s', [Copy(DataSet.FieldByName('Filler').AsString, 1, 1)]) +
                                              Format('%3.3d', [CoServicesSortTop]) +
                                              Copy(DataSet.FieldByName('Filler').AsString, 5, Length(DataSet.FieldByName('Filler').AsString));
  end;
  Assert(Trim(Copy(DataSet.FieldByName('Filler').AsString, 2, 3)) <> '');
end;

procedure TDM_CO_SERVICES.CO_SERVICESAfterRefresh(DataSet: TDataSet);
var
  b: Boolean;
begin
  DataSet.DisableControls;
  b := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  try
    TevClientDataSet(DataSet).IndexFieldNames := '';
    CoServicesSortTop := 0;
    DataSet.First;
    while not DataSet.Eof do
    begin
      if (Copy(DataSet.FieldByName('Filler').AsString, 4, 1) = 'Y') or (Copy(DataSet.FieldByName('Filler').AsString, 4, 1) = 'N') then
      begin
        DataSet.Edit;
        DataSet.FieldByName('Filler').AsString := ' ' + DataSet.FieldByName('Filler').AsString;
        DataSet.Post;
      end;

      if (Trim(Copy(DataSet.FieldByName('Filler').AsString, 2, 3)) <> '') and
        (StrToInt(Trim(Copy(DataSet.FieldByName('Filler').AsString, 2, 3))) > CoServicesSortTop) then
        CoServicesSortTop := StrToInt(Trim(Copy(DataSet.FieldByName('Filler').AsString, 2, 3)));
      DataSet.Next;
    end;
    if TevClientDataSet(DataSet).ChangeCount > 0 then
      ctx_DataAccess.PostDataSets([TevClientDataSet(DataSet)]);
    TevClientDataSet(DataSet).IndexFieldNames := 'Sort';
    DataSet.First;
  finally
    ctx_DataAccess.TempCommitDisable := b;
    DataSet.EnableControls;
  end;
end;

procedure TDM_CO_SERVICES.CO_SERVICESCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('Sort').AsString := Copy(DataSet.FieldByName('Filler').AsString, 2, 3);
end;

initialization
  RegisterClass(TDM_CO_SERVICES);

finalization
  UnregisterClass(TDM_CO_SERVICES);

end.
