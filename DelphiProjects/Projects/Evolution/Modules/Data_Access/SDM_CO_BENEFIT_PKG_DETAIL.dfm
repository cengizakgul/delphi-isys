inherited DM_CO_BENEFIT_PKG_DETAIL: TDM_CO_BENEFIT_PKG_DETAIL
  OldCreateOrder = True
  Left = 520
  Top = 200
  Height = 479
  Width = 741
  object CO_BENEFIT_PKG_DETAIL: TCO_BENEFIT_PKG_DETAIL
    ProviderName = 'CO_BENEFIT_PKG_DETAIL_PROV'
    Left = 392
    Top = 80
    object CO_BENEFIT_PKG_DETAILPackageName: TStringField
      FieldKind = fkLookup
      FieldName = 'PackageName'
      LookupDataSet = DM_CO_BENEFIT_PACKAGE.CO_BENEFIT_PACKAGE
      LookupKeyFields = 'CO_BENEFIT_PACKAGE_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_BENEFIT_PACKAGE_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_DETAILBenefitName: TStringField
      FieldKind = fkLookup
      FieldName = 'BenefitName'
      LookupDataSet = DM_CO_BENEFITS.CO_BENEFITS
      LookupKeyFields = 'CO_BENEFITS_NBR'
      LookupResultField = 'BENEFIT_NAME'
      KeyFields = 'CO_BENEFITS_NBR'
      Size = 40
      Lookup = True
    end
    object CO_BENEFIT_PKG_DETAILCO_BENEFIT_PACKAGE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_PACKAGE_NBR'
    end
    object CO_BENEFIT_PKG_DETAILCO_BENEFIT_PKG_DETAIL_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_PKG_DETAIL_NBR'
    end
    object CO_BENEFIT_PKG_DETAILCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object CO_BENEFIT_PKG_DETAILCategoryName: TStringField
      FieldKind = fkLookup
      FieldName = 'CategoryName'
      LookupDataSet = DM_CO_BENEFITS.CO_BENEFITS
      LookupKeyFields = 'CO_BENEFITS_NBR'
      LookupResultField = 'CategoryName'
      KeyFields = 'CO_BENEFITS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 240
    Top = 72
  end
end
