inherited DM_CO_DEPARTMENT: TDM_CO_DEPARTMENT
  OldCreateOrder = True
  Left = 354
  Top = 218
  Height = 480
  Width = 696
  object CO_DEPARTMENT: TCO_DEPARTMENT
    ProviderName = 'CO_DEPARTMENT_PROV'
    PictureMasks.Strings = (
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      
        'OVERRIDE_PAY_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    BeforePost = CO_DEPARTMENTBeforePost
    OnCalcFields = CO_DEPARTMENTCalcFields
    BlobsLoadMode = blmManualLoad
    Left = 208
    Top = 144
    object CO_DEPARTMENTCO_DEPARTMENT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."CO_DEPARTMENT_NBR"'
      Required = True
    end
    object CO_DEPARTMENTCO_BRANCH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BRANCH_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."CO_BRANCH_NBR"'
      Required = True
    end
    object CO_DEPARTMENTCUSTOM_DEPARTMENT_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_DEPARTMENT_NUMBER'
      Origin = '"CO_DEPARTMENT_HIST"."CUSTOM_DEPARTMENT_NUMBER"'
      Required = True
    end
    object CO_DEPARTMENTNAME: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = '"CO_DEPARTMENT_HIST"."NAME"'
      Size = 40
    end
    object CO_DEPARTMENTADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"CO_DEPARTMENT_HIST"."ADDRESS1"'
      Size = 30
    end
    object CO_DEPARTMENTADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"CO_DEPARTMENT_HIST"."ADDRESS2"'
      Size = 30
    end
    object CO_DEPARTMENTCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CO_DEPARTMENT_HIST"."CITY"'
    end
    object CO_DEPARTMENTSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CO_DEPARTMENT_HIST"."STATE"'
      Size = 2
    end
    object CO_DEPARTMENTZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CO_DEPARTMENT_HIST"."ZIP_CODE"'
      Size = 10
    end
    object CO_DEPARTMENTCONTACT1: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT1'
      Origin = '"CO_DEPARTMENT_HIST"."CONTACT1"'
      Size = 30
    end
    object CO_DEPARTMENTPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"CO_DEPARTMENT_HIST"."PHONE1"'
    end
    object CO_DEPARTMENTDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"CO_DEPARTMENT_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object CO_DEPARTMENTCONTACT2: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT2'
      Origin = '"CO_DEPARTMENT_HIST"."CONTACT2"'
      Size = 30
    end
    object CO_DEPARTMENTPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"CO_DEPARTMENT_HIST"."PHONE2"'
    end
    object CO_DEPARTMENTDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"CO_DEPARTMENT_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object CO_DEPARTMENTFAX: TStringField
      DisplayWidth = 20
      FieldName = 'FAX'
      Origin = '"CO_DEPARTMENT_HIST"."FAX"'
    end
    object CO_DEPARTMENTFAX_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX_DESCRIPTION'
      Origin = '"CO_DEPARTMENT_HIST"."FAX_DESCRIPTION"'
      Size = 10
    end
    object CO_DEPARTMENTE_MAIL_ADDRESS: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS'
      Origin = '"CO_DEPARTMENT_HIST"."E_MAIL_ADDRESS"'
      Size = 80
    end
    object CO_DEPARTMENTHOME_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_CO_STATES_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."HOME_CO_STATES_NBR"'
    end
    object CO_DEPARTMENTPAY_FREQUENCY_HOURLY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_HOURLY'
      Origin = '"CO_DEPARTMENT_HIST"."PAY_FREQUENCY_HOURLY"'
      Required = True
      Size = 1
    end
    object CO_DEPARTMENTPAY_FREQUENCY_SALARY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_SALARY'
      Origin = '"CO_DEPARTMENT_HIST"."PAY_FREQUENCY_SALARY"'
      Required = True
      Size = 1
    end
    object CO_DEPARTMENTCL_TIMECLOCK_IMPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_TIMECLOCK_IMPORTS_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."CL_TIMECLOCK_IMPORTS_NBR"'
    end
    object CO_DEPARTMENTPRINT_DEPT_ADDRESS_ON_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_DEPT_ADDRESS_ON_CHECKS'
      Origin = '"CO_DEPARTMENT_HIST"."PRINT_DEPT_ADDRESS_ON_CHECKS"'
      Required = True
      Size = 1
    end
    object CO_DEPARTMENTPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."PAYROLL_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DEPARTMENTCL_BILLING_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_BILLING_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."CL_BILLING_NBR"'
    end
    object CO_DEPARTMENTBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BILLING_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."BILLING_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DEPARTMENTTAX_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."TAX_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DEPARTMENTDD_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DD_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."DD_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_DEPARTMENTCO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."CO_WORKERS_COMP_NBR"'
    end
    object CO_DEPARTMENTUNION_CL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'UNION_CL_E_DS_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."UNION_CL_E_DS_NBR"'
    end
    object CO_DEPARTMENTOVERRIDE_EE_RATE_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_EE_RATE_NUMBER'
      Origin = '"CO_DEPARTMENT_HIST"."OVERRIDE_EE_RATE_NUMBER"'
    end
    object CO_DEPARTMENTOVERRIDE_PAY_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_PAY_RATE'
      Origin = '"CO_DEPARTMENT_HIST"."OVERRIDE_PAY_RATE"'
    end
    object CO_DEPARTMENTDEPARTMENT_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'DEPARTMENT_NOTES'
      Origin = '"CO_DEPARTMENT_HIST"."DEPARTMENT_NOTES"'
      Size = 8
    end
    object CO_DEPARTMENTFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CO_DEPARTMENT_HIST"."FILLER"'
      Size = 512
    end
    object CO_DEPARTMENTHOME_STATE_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'HOME_STATE_TYPE'
      Origin = '"CO_DEPARTMENT_HIST"."HOME_STATE_TYPE"'
      Required = True
      Size = 1
    end
    object CO_DEPARTMENTGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"CO_DEPARTMENT_HIST"."GENERAL_LEDGER_TAG"'
    end
    object CO_DEPARTMENTCL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_DEPARTMENT_HIST"."CL_DELIVERY_GROUP_NBR"'
    end
    object CO_DEPARTMENTPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
    end
    object CO_DEPARTMENTPR_EE_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_MB_GROUP_NBR'
    end
    object CO_DEPARTMENTPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
    end
    object CO_DEPARTMENTTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
    end
    object CO_DEPARTMENTPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_MB_GROUP_NBR'
    end
    object CO_DEPARTMENTPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
    end
    object CO_DEPARTMENTWORKSITE_ID: TStringField
      FieldName = 'WORKSITE_ID'
    end
    object CO_DEPARTMENTAccountNumber: TStringField
      FieldKind = fkCalculated
      FieldName = 'AccountNumber'
      Calculated = True
    end
    object CO_DEPARTMENTPrimary: TStringField
      FieldKind = fkCalculated
      FieldName = 'Primary'
      Size = 1
      Calculated = True
    end
  end
end
