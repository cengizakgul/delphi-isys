// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_TEAM_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_TEAM_LOCALS = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    CO_TEAM_LOCALS: TCO_TEAM_LOCALS;
    CO_TEAM_LOCALSCO_LOCAL_TAX_NBR: TIntegerField;
    CO_TEAM_LOCALSCO_TEAM_LOCALS_NBR: TIntegerField;
    CO_TEAM_LOCALSCO_TEAM_NBR: TIntegerField;
    CO_TEAM_LOCALSLocalName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_TEAM_LOCALS);

finalization
  UnregisterClass(TDM_CO_TEAM_LOCALS);

end.
