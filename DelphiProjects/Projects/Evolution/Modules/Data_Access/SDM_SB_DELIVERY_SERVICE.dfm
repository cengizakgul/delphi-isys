inherited DM_SB_DELIVERY_SERVICE: TDM_SB_DELIVERY_SERVICE
  Left = 742
  Top = 203
  Height = 264
  Width = 265
  object SB_DELIVERY_SERVICE: TSB_DELIVERY_SERVICE
    ProviderName = 'SB_DELIVERY_SERVICE_PROV'
    Left = 64
    Top = 32
    object SB_DELIVERY_SERVICESB_DELIVERY_SERVICE_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_SERVICE_NBR'
      Origin = '"SB_DELIVERY_SERVICE_HIST"."SB_DELIVERY_SERVICE_NBR"'
      Required = True
    end
    object SB_DELIVERY_SERVICESY_DELIVERY_SERVICE_NBR2: TIntegerField
      FieldName = 'SY_DELIVERY_SERVICE_NBR'
      Origin = '"SB_DELIVERY_SERVICE_HIST"."SY_DELIVERY_SERVICE_NBR"'
      Required = True
    end
    object SB_DELIVERY_SERVICESY_DELIVERY_SERVICE_NBR: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SY_DELIVERY_SERVICE.SY_DELIVERY_SERVICE
      LookupKeyFields = 'SY_DELIVERY_SERVICE_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_DELIVERY_SERVICE_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 176
    Top = 32
  end
end
