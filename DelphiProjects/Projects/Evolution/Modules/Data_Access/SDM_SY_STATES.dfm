inherited DM_SY_STATES: TDM_SY_STATES
  OldCreateOrder = True
  Left = 704
  Top = 272
  Height = 479
  Width = 741
  object SY_STATES: TSY_STATES
    ProviderName = 'SY_STATES_PROV'
    PictureMasks.Strings = (
      
        'STATE_MINIMUM_WAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F'
      
        'SUPPLEMENTAL_WAGES_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'STATE_TIP_CREDIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]}'#9'T'#9'F'
      
        'WEEKLY_SDI_TAX_CAP'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F'
      
        'EE_SDI_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']]}'#9'T'#9'F'
      
        'EE_SDI_MAXIMUM_WAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]}'#9'T'#9'F'
      
        'ER_SDI_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']]}'#9'T'#9'F'
      
        'ER_SDI_MAXIMUM_WAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]}'#9'T'#9'F'
      
        'MAXIMUM_GARNISHMENT_PERCENT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'GARNISH_MIN_WAGE_MULTIPLIER'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'SALES_TAX_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    FieldDefs = <
      item
        Name = 'SY_STATES_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'STATE_MINIMUM_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'STATE_WITHHOLDING'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATE_TIP_CREDIT'
        DataType = ftFloat
      end
      item
        Name = 'SUPPLEMENTAL_WAGES_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'STATE_EFT_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EFT_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'PRINT_STATE_RETURN_IF_ZERO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SUI_EFT_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SUI_EFT_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'SDI_RECIPROCATE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SUI_RECIPROCATE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'WEEKLY_SDI_TAX_CAP'
        DataType = ftFloat
      end
      item
        Name = 'EE_SDI_MAXIMUM_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'ER_SDI_MAXIMUM_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'EE_SDI_RATE'
        DataType = ftFloat
      end
      item
        Name = 'ER_SDI_RATE'
        DataType = ftFloat
      end
      item
        Name = 'SHOW_S125_IN_GROSS_WAGES_SUI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRINT_SUI_RETURN_IF_ZERO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MAXIMUM_GARNISHMENT_PERCENT'
        DataType = ftFloat
      end
      item
        Name = 'GARNISH_MIN_WAGE_MULTIPLIER'
        DataType = ftFloat
      end
      item
        Name = 'PAYROLL'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'SALES'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'CORPORATE'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'RAILROAD'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'UNEMPLOYMENT'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'OTHER'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'UIFSA_NEW_HIRE_REPORTING'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DD_CHILD_SUPPORT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'USE_STATE_MISC_TAX_RETURN_CODE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SALES_TAX_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'SY_STATE_TAX_PMT_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_TAX_PMT_REPORT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_STATE_REPORTING_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAY_SDI_WITH'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'TAX_DEPOSIT_FOLLOW_FEDERAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ROUND_TO_NEAREST_DOLLAR'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SUI_TAX_PAYMENT_TYPE_CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'INC_SUI_TAX_PAYMENT_CODE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FIPS_CODE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'PRINT_W2'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RECIPROCITY_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2_BOX'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'INACTIVATE_MARITAL_STATUS'
        DataType = ftString
        Size = 1
      end>
    Left = 92
    Top = 12
    object SY_STATESSY_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATES_NBR'
      Required = True
    end
    object SY_STATESSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Size = 2
    end
    object SY_STATESNAME: TStringField
      DisplayWidth = 20
      FieldName = 'NAME'
      Required = True
    end
    object SY_STATESSTATE_MINIMUM_WAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'STATE_MINIMUM_WAGE'
    end
    object SY_STATESSTATE_WITHHOLDING: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_WITHHOLDING'
      Required = True
      Size = 1
    end
    object SY_STATESSTATE_TIP_CREDIT: TFloatField
      DisplayWidth = 10
      FieldName = 'STATE_TIP_CREDIT'
    end
    object SY_STATESSUPPLEMENTAL_WAGES_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'SUPPLEMENTAL_WAGES_PERCENTAGE'
      DisplayFormat = '#0.####'
    end
    object SY_STATESSTATE_EFT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_EFT_TYPE'
      Required = True
      Size = 1
    end
    object SY_STATESEFT_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'EFT_NOTES'
      Size = 8
    end
    object SY_STATESPRINT_STATE_RETURN_IF_ZERO: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_STATE_RETURN_IF_ZERO'
      Required = True
      Size = 1
    end
    object SY_STATESSUI_EFT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'SUI_EFT_TYPE'
      Required = True
      Size = 1
    end
    object SY_STATESSUI_EFT_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'SUI_EFT_NOTES'
      Size = 8
    end
    object SY_STATESSDI_RECIPROCATE: TStringField
      DisplayWidth = 1
      FieldName = 'SDI_RECIPROCATE'
      Required = True
      Size = 1
    end
    object SY_STATESSUI_RECIPROCATE: TStringField
      DisplayWidth = 1
      FieldName = 'SUI_RECIPROCATE'
      Required = True
      Size = 1
    end
    object SY_STATESWEEKLY_SDI_TAX_CAP: TFloatField
      DisplayWidth = 10
      FieldName = 'WEEKLY_SDI_TAX_CAP'
    end
    object SY_STATESEE_SDI_MAXIMUM_WAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'EE_SDI_MAXIMUM_WAGE'
    end
    object SY_STATESER_SDI_MAXIMUM_WAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'ER_SDI_MAXIMUM_WAGE'
    end
    object SY_STATESEE_SDI_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'EE_SDI_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_STATESER_SDI_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'ER_SDI_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_STATESSHOW_S125_IN_GROSS_WAGES_SUI: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_S125_IN_GROSS_WAGES_SUI'
      Required = True
      Size = 1
    end
    object SY_STATESPRINT_SUI_RETURN_IF_ZERO: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_SUI_RETURN_IF_ZERO'
      Required = True
      Size = 1
    end
    object SY_STATESMAXIMUM_GARNISHMENT_PERCENT: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_GARNISHMENT_PERCENT'
    end
    object SY_STATESGARNISH_MIN_WAGE_MULTIPLIER: TFloatField
      DisplayWidth = 10
      FieldName = 'GARNISH_MIN_WAGE_MULTIPLIER'
    end
    object SY_STATESPAYROLL: TStringField
      DisplayWidth = 8
      FieldName = 'PAYROLL'
      Size = 8
    end
    object SY_STATESSALES: TStringField
      DisplayWidth = 8
      FieldName = 'SALES'
      Size = 8
    end
    object SY_STATESCORPORATE: TStringField
      DisplayWidth = 8
      FieldName = 'CORPORATE'
      Size = 8
    end
    object SY_STATESRAILROAD: TStringField
      DisplayWidth = 8
      FieldName = 'RAILROAD'
      Size = 8
    end
    object SY_STATESUNEMPLOYMENT: TStringField
      DisplayWidth = 8
      FieldName = 'UNEMPLOYMENT'
      Size = 8
    end
    object SY_STATESOTHER: TStringField
      DisplayWidth = 8
      FieldName = 'OTHER'
      Size = 8
    end
    object SY_STATESUIFSA_NEW_HIRE_REPORTING: TStringField
      DisplayWidth = 1
      FieldName = 'UIFSA_NEW_HIRE_REPORTING'
      Required = True
      Size = 1
    end
    object SY_STATESDD_CHILD_SUPPORT: TStringField
      DisplayWidth = 1
      FieldName = 'DD_CHILD_SUPPORT'
      Required = True
      Size = 1
    end
    object SY_STATESUSE_STATE_MISC_TAX_RETURN_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'USE_STATE_MISC_TAX_RETURN_CODE'
      Required = True
      Size = 1
    end
    object SY_STATESSALES_TAX_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'SALES_TAX_PERCENTAGE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_STATESSY_STATE_TAX_PMT_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATE_TAX_PMT_AGENCY_NBR'
    end
    object SY_STATESSY_TAX_PMT_REPORT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_TAX_PMT_REPORT_NBR'
    end
    object SY_STATESSY_STATE_REPORTING_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATE_REPORTING_AGENCY_NBR'
    end
    object SY_STATESPAY_SDI_WITH: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_SDI_WITH'
      Required = True
      Size = 1
    end
    object SY_STATESFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Size = 512
    end
    object SY_STATESTAX_DEPOSIT_FOLLOW_FEDERAL: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_DEPOSIT_FOLLOW_FEDERAL'
      Required = True
      Size = 1
    end
    object SY_STATESROUND_TO_NEAREST_DOLLAR: TStringField
      DisplayWidth = 1
      FieldName = 'ROUND_TO_NEAREST_DOLLAR'
      Required = True
      Size = 1
    end
    object SY_STATESSUI_TAX_PAYMENT_TYPE_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'SUI_TAX_PAYMENT_TYPE_CODE'
      Size = 10
    end
    object SY_STATESINC_SUI_TAX_PAYMENT_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'INC_SUI_TAX_PAYMENT_CODE'
      Size = 1
    end
    object SY_STATESFIPS_CODE: TStringField
      DisplayWidth = 2
      FieldName = 'FIPS_CODE'
      Size = 2
    end
    object SY_STATESPRINT_W2: TStringField
      FieldName = 'PRINT_W2'
      Size = 1
    end
    object SY_STATESRECIPROCITY_TYPE: TStringField
      FieldName = 'RECIPROCITY_TYPE'
      Size = 1
    end
    object SY_STATESW2_BOX: TStringField
      FieldName = 'W2_BOX'
      Size = 10
    end
    object SY_STATESINACTIVATE_MARITAL_STATUS: TStringField
      FieldName = 'INACTIVATE_MARITAL_STATUS'
      Size = 1
    end
  end
end
