inherited DM_CO_VENDOR_DETAIL_VALUES: TDM_CO_VENDOR_DETAIL_VALUES
  OldCreateOrder = True
  Left = 252
  Top = 193
  Height = 480
  Width = 696
  object DM_CLIENT: TDM_CLIENT
    Left = 136
    Top = 144
  end
  object CO_VENDOR_DETAIL_VALUES: TCO_VENDOR_DETAIL_VALUES
    ProviderName = 'CO_VENDOR_DETAIL_VALUES_PROV'
    OnCalcFields = CO_VENDOR_DETAIL_VALUESCalcFields
    Left = 136
    Top = 80
    object CO_VENDOR_DETAIL_VALUESCO_VENDOR_DETAIL_VALUES_NBR: TIntegerField
      FieldName = 'CO_VENDOR_DETAIL_VALUES_NBR'
    end
    object CO_VENDOR_DETAIL_VALUESCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_VENDOR_DETAIL_VALUESSB_VENDOR_DETAIL_NBR: TIntegerField
      FieldName = 'SB_VENDOR_DETAIL_NBR'
    end
    object CO_VENDOR_DETAIL_VALUESSB_VENDOR_DETAIL_VALUES_NBR: TIntegerField
      FieldName = 'SB_VENDOR_DETAIL_VALUES_NBR'
    end
    object CO_VENDOR_DETAIL_VALUESDETAIL_VALUES: TStringField
      FieldName = 'DETAIL_VALUES'
      Size = 80
    end
    object CO_VENDOR_DETAIL_VALUESCO_VENDOR_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CO_VENDOR_NBR'
      Calculated = True
    end
    object CO_VENDOR_DETAIL_VALUESSB_DETAIL: TStringField
      FieldKind = fkCalculated
      FieldName = 'SB_DETAIL'
      Size = 40
      Calculated = True
    end
    object CO_VENDOR_DETAIL_VALUESSB_DETAIL_VALUES: TStringField
      FieldKind = fkCalculated
      FieldName = 'SB_DETAIL_VALUES'
      Size = 80
      Calculated = True
    end
  end
end
