inherited DM_CL_HR_PERSON_EDUCATION: TDM_CL_HR_PERSON_EDUCATION
  OldCreateOrder = True
  Height = 358
  Width = 397
  object CL_HR_PERSON_EDUCATION: TCL_HR_PERSON_EDUCATION
    ProviderName = 'CL_HR_PERSON_EDUCATION_PROV'
    Left = 104
    Top = 40
    object CL_HR_PERSON_EDUCATIONCL_HR_PERSON_EDUCATION_NBR: TIntegerField
      FieldName = 'CL_HR_PERSON_EDUCATION_NBR'
    end
    object CL_HR_PERSON_EDUCATIONCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object CL_HR_PERSON_EDUCATIONDEGREE_LEVEL: TStringField
      FieldName = 'DEGREE_LEVEL'
      Size = 1
    end
    object CL_HR_PERSON_EDUCATIONEND_DATE: TDateField
      FieldName = 'END_DATE'
    end
    object CL_HR_PERSON_EDUCATIONGPA: TFloatField
      FieldName = 'GPA'
    end
    object CL_HR_PERSON_EDUCATIONGRADUATION_DATE: TDateField
      FieldName = 'GRADUATION_DATE'
    end
    object CL_HR_PERSON_EDUCATIONMAJOR: TStringField
      FieldName = 'MAJOR'
      Size = 30
    end
    object CL_HR_PERSON_EDUCATIONCL_HR_SCHOOL_NBR: TIntegerField
      FieldName = 'CL_HR_SCHOOL_NBR'
    end
    object CL_HR_PERSON_EDUCATIONSTART_DATE: TDateField
      FieldName = 'START_DATE'
    end
    object CL_HR_PERSON_EDUCATIONSchoolName: TStringField
      FieldKind = fkLookup
      FieldName = 'SchoolName'
      LookupDataSet = DM_CL_HR_SCHOOL.CL_HR_SCHOOL
      LookupKeyFields = 'CL_HR_SCHOOL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_HR_SCHOOL_NBR'
      Size = 40
      Lookup = True
    end
    object CL_HR_PERSON_EDUCATIONNOTES: TBlobField
      FieldName = 'NOTES'
      Size = 8
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 288
    Top = 184
  end
end
