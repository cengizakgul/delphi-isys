unit SDM_CO_BENEFITS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils, SFieldCodeValues,
  EvClientDataSet;

type
  TDM_CO_BENEFITS = class(TDM_ddTable)
    CO_BENEFITS: TCO_BENEFITS;
    CO_BENEFITSCategoryName: TStringField;
    CO_BENEFITSCO_BENEFITS_NBR: TIntegerField;
    CO_BENEFITSCO_BENEFIT_CATEGORY_NBR: TIntegerField;
    CO_BENEFITSBENEFIT_NAME: TStringField;
    CO_BENEFITSCATEGORY_TYPE: TStringField;
    DM_COMPANY: TDM_COMPANY;
    procedure CO_BENEFITSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BENEFITS: TDM_CO_BENEFITS;

implementation

{$R *.DFM}




procedure TDM_CO_BENEFITS.CO_BENEFITSCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not CO_BENEFITSCATEGORY_TYPE.IsNull then
    CO_BENEFITSCategoryName.AsString :=
              ReturnDescription(Benefit_Category_Type_ComboChoices, CO_BENEFITSCATEGORY_TYPE.AsString);

end;

initialization
  RegisterClass(TDM_CO_BENEFITS);

finalization
  UnregisterClass(TDM_CO_BENEFITS);

end.
