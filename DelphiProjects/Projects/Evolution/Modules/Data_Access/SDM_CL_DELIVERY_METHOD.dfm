inherited DM_CL_DELIVERY_METHOD: TDM_CL_DELIVERY_METHOD
  Left = 356
  Top = 199
  Height = 302
  Width = 422
  object CL_DELIVERY_METHOD: TCL_DELIVERY_METHOD
    ProviderName = 'CL_DELIVERY_METHOD_PROV'
    PictureMasks.Strings = (
      'PHONE_NUMBER'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    Left = 69
    Top = 44
    object CL_DELIVERY_METHODServiceDescription_Lookup: TStringField
      DisplayLabel = 'Delivery Service'
      DisplayWidth = 38
      FieldKind = fkLookup
      FieldName = 'ServiceDescription_Lookup'
      LookupDataSet = DM_SB_DELIVERY_COMPANY_SVCS.SB_DELIVERY_COMPANY_SVCS
      LookupKeyFields = 'SB_DELIVERY_COMPANY_SVCS_NBR'
      LookupResultField = 'NameAndService'
      KeyFields = 'SB_DELIVERY_COMPANY_SVCS_NBR'
      Size = 40
      Lookup = True
    end
    object CL_DELIVERY_METHODNAME: TStringField
      DisplayLabel = 'Method Name'
      DisplayWidth = 40
      FieldName = 'NAME'
      Size = 40
    end
    object CL_DELIVERY_METHODSTATE: TStringField
      DisplayWidth = 3
      FieldName = 'STATE'
      Size = 3
    end
    object CL_DELIVERY_METHODCL_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'CL_DELIVERY_METHOD_NBR'
      Visible = False
    end
    object CL_DELIVERY_METHODSB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_COMPANY_SVCS_NBR'
      Visible = False
    end
    object CL_DELIVERY_METHODADDRESS1: TStringField
      FieldName = 'ADDRESS1'
      Visible = False
      Size = 30
    end
    object CL_DELIVERY_METHODADDRESS2: TStringField
      FieldName = 'ADDRESS2'
      Visible = False
      Size = 30
    end
    object CL_DELIVERY_METHODCITY: TStringField
      FieldName = 'CITY'
      Visible = False
    end
    object CL_DELIVERY_METHODZIP_CODE: TStringField
      FieldName = 'ZIP_CODE'
      Visible = False
      Size = 10
    end
    object CL_DELIVERY_METHODATTENTION: TStringField
      FieldName = 'ATTENTION'
      Visible = False
      Size = 40
    end
    object CL_DELIVERY_METHODPHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Visible = False
    end
    object CL_DELIVERY_METHODCOVER_SHEET_SB_REPORT_NBR: TIntegerField
      FieldName = 'COVER_SHEET_SB_REPORT_NBR'
      Visible = False
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 72
    Top = 111
  end
end
