// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_HR_CO_PROVIDED_EDUCATN;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  SDataStructure, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_EE_HR_CO_PROVIDED_EDUCATN = class(TDM_ddTable)
    EE_HR_CO_PROVIDED_EDUCATN: TEE_HR_CO_PROVIDED_EDUCATN;
    EE_HR_CO_PROVIDED_EDUCATNCHECK_ISSUED_BY: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNCHECK_NUMBER: TIntegerField;
    EE_HR_CO_PROVIDED_EDUCATNCOMPANY_COST: TFloatField;
    EE_HR_CO_PROVIDED_EDUCATNCOMPLETED: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNCOURSE: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNDATE_FINISH: TDateField;
    EE_HR_CO_PROVIDED_EDUCATNDATE_ISSUED: TDateField;
    EE_HR_CO_PROVIDED_EDUCATNDATE_START: TDateField;
    EE_HR_CO_PROVIDED_EDUCATNEE_HR_CO_PROVIDED_EDUCATN_NBR: TIntegerField;
    EE_HR_CO_PROVIDED_EDUCATNEE_NBR: TIntegerField;
    EE_HR_CO_PROVIDED_EDUCATNNOTES: TBlobField;
    EE_HR_CO_PROVIDED_EDUCATNTAXABLE: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNSchool: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNCL_HR_SCHOOL_NBR: TIntegerField;
    DM_CLIENT: TDM_CLIENT;
    EE_HR_CO_PROVIDED_EDUCATNLAST_REVIEW_DATE: TDateField;
    EE_HR_CO_PROVIDED_EDUCATNRENEWAL_STATUS: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNCOURSE_DESC: TStringField;
    EE_HR_CO_PROVIDED_EDUCATNCL_HR_COURSE_NBR: TIntegerField;
    procedure EE_HR_CO_PROVIDED_EDUCATNCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_EE_HR_CO_PROVIDED_EDUCATN.EE_HR_CO_PROVIDED_EDUCATNCalcFields(
  DataSet: TDataSet);
var
  aNBR : integer;
begin
  if not DM_CLIENT.CL_HR_COURSE.Active then
     DM_CLIENT.CL_HR_COURSE.DataRequired('ALL');

  aNBR := DataSet.FieldByName('CL_HR_COURSE_NBR').asInteger;
  EE_HR_CO_PROVIDED_EDUCATNCOURSE_dESC.AsString :=
          DM_CLIENT.CL_HR_COURSE.ShadowDataSet.CachedLookup(aNBR, 'NAME');
end;

initialization
  RegisterClass( TDM_EE_HR_CO_PROVIDED_EDUCATN );

finalization
  UnregisterClass( TDM_EE_HR_CO_PROVIDED_EDUCATN );

end.
