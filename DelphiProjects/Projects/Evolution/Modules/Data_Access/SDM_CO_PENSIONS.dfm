inherited DM_CO_PENSIONS: TDM_CO_PENSIONS
  OldCreateOrder = False
  Left = 280
  Top = 161
  Height = 480
  Width = 696
  object DM_CLIENT: TDM_CLIENT
    Left = 216
    Top = 144
  end
  object CO_PENSIONS: TCO_PENSIONS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_PENSIONS_PROV'
    ValidateWithMask = True
    Left = 216
    Top = 64
    object CO_PENSIONSCO_PENSIONS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PENSIONS_NBR'
      Visible = False
    end
    object CO_PENSIONSCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Visible = False
    end
    object CO_PENSIONSCL_PENSION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_PENSION_NBR'
      Visible = False
    end
    object CO_PENSIONSFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object CO_PENSIONSPensionName: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'PensionName'
      LookupDataSet = DM_CL_PENSION.CL_PENSION
      LookupKeyFields = 'CL_PENSION_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_PENSION_NBR'
      Size = 40
      Lookup = True
    end
  end
end
