inherited DM_CO_STATES: TDM_CO_STATES
  OldCreateOrder = True
  Left = 457
  Top = 162
  Height = 479
  Width = 741
  object CO_STATES: TCO_STATES
    ProviderName = 'CO_STATES_PROV'
    PictureMasks.Strings = (
      'STATE'#9'*{&,@}'#9'T'#9'T')
    AfterInsert = CO_STATESAfterInsert
    BeforePost = CO_STATESBeforePost
    AfterPost = CO_STATESAfterPost
    OnCalcFields = CO_STATESCalcFields
    Left = 56
    Top = 24
    object CO_STATESCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = '"CO_STATES_HIST"."CO_NBR"'
      Required = True
    end
    object CO_STATESCO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_STATES_NBR'
      Origin = '"CO_STATES_HIST"."CO_STATES_NBR"'
      Required = True
    end
    object CO_STATESEE_SDI_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EE_SDI_GENERAL_LEDGER_TAG'
      Origin = '"CO_STATES_HIST"."EE_SDI_GENERAL_LEDGER_TAG"'
    end
    object CO_STATESEE_SDI_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EE_SDI_OFFSET_GL_TAG'
      Origin = '"CO_STATES_HIST"."EE_SDI_OFFSET_GL_TAG"'
    end
    object CO_STATESER_SDI_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_SDI_GENERAL_LEDGER_TAG'
      Origin = '"CO_STATES_HIST"."ER_SDI_GENERAL_LEDGER_TAG"'
    end
    object CO_STATESER_SDI_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_SDI_OFFSET_GL_TAG'
      Origin = '"CO_STATES_HIST"."ER_SDI_OFFSET_GL_TAG"'
    end
    object CO_STATESFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CO_STATES_HIST"."FILLER"'
      Size = 512
    end
    object CO_STATESFINAL_TAX_RETURN: TStringField
      DisplayWidth = 1
      FieldName = 'FINAL_TAX_RETURN'
      Origin = '"CO_STATES_HIST"."FINAL_TAX_RETURN"'
      Required = True
      Size = 1
    end
    object CO_STATESIGNORE_STATE_TAX_DEP_THRESHOLD: TStringField
      DisplayWidth = 1
      FieldName = 'IGNORE_STATE_TAX_DEP_THRESHOLD'
      Origin = '"CO_STATES_HIST"."IGNORE_STATE_TAX_DEP_THRESHOLD"'
      Required = True
      Size = 1
    end
    object CO_STATESNOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Origin = '"CO_STATES_HIST"."NOTES"'
      Size = 8
    end
    object CO_STATESSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CO_STATES_HIST"."STATE"'
      Required = True
      Size = 2
    end
    object CO_STATESSTATE_EFT_EIN: TStringField
      DisplayWidth = 30
      FieldName = 'STATE_EFT_EIN'
      Origin = '"CO_STATES_HIST"."STATE_EFT_EIN"'
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 30
    end
    object CO_STATESSTATE_EFT_ENROLLMENT_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_EFT_ENROLLMENT_STATUS'
      Origin = '"CO_STATES_HIST"."STATE_EFT_ENROLLMENT_STATUS"'
      Required = True
      Size = 1
    end
    object CO_STATESSTATE_EFT_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'STATE_EFT_NAME'
      Origin = '"CO_STATES_HIST"."STATE_EFT_NAME"'
      Size = 40
    end
    object CO_STATESSTATE_EFT_PIN_NUMBER: TStringField
      DisplayWidth = 10
      FieldName = 'STATE_EFT_PIN_NUMBER'
      Origin = '"CO_STATES_HIST"."STATE_EFT_PIN_NUMBER"'
      Size = 10
    end
    object CO_STATESSTATE_EIN: TStringField
      DisplayWidth = 30
      FieldName = 'STATE_EIN'
      Origin = '"CO_STATES_HIST"."STATE_EIN"'
      Required = True
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 30
    end
    object CO_STATESSTATE_EXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_EXEMPT'
      Origin = '"CO_STATES_HIST"."STATE_EXEMPT"'
      Required = True
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 1
    end
    object CO_STATESSTATE_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'STATE_GENERAL_LEDGER_TAG'
      Origin = '"CO_STATES_HIST"."STATE_GENERAL_LEDGER_TAG"'
    end
    object CO_STATESSTATE_NON_PROFIT: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_NON_PROFIT'
      Origin = '"CO_STATES_HIST"."STATE_NON_PROFIT"'
      Required = True
      Size = 1
    end
    object CO_STATESSTATE_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'STATE_OFFSET_GL_TAG'
      Origin = '"CO_STATES_HIST"."STATE_OFFSET_GL_TAG"'
    end
    object CO_STATESSTATE_SDI_EFT_EIN: TStringField
      DisplayWidth = 30
      FieldName = 'STATE_SDI_EFT_EIN'
      Origin = '"CO_STATES_HIST"."STATE_SDI_EFT_EIN"'
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 30
    end
    object CO_STATESSTATE_SDI_EIN: TStringField
      DisplayWidth = 30
      FieldName = 'STATE_SDI_EIN'
      Origin = '"CO_STATES_HIST"."STATE_SDI_EIN"'
      Required = True
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 30
    end
    object CO_STATESSTATE_TAX_DEPOSIT_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_TAX_DEPOSIT_METHOD'
      Origin = '"CO_STATES_HIST"."STATE_TAX_DEPOSIT_METHOD"'
      Required = True
      Size = 1
    end
    object CO_STATESSUI_EFT_EIN: TStringField
      DisplayWidth = 30
      FieldName = 'SUI_EFT_EIN'
      Origin = '"CO_STATES_HIST"."SUI_EFT_EIN"'
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 30
    end
    object CO_STATESSUI_EFT_ENROLLMENT_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'SUI_EFT_ENROLLMENT_STATUS'
      Origin = '"CO_STATES_HIST"."SUI_EFT_ENROLLMENT_STATUS"'
      Required = True
      Size = 1
    end
    object CO_STATESSUI_EFT_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'SUI_EFT_NAME'
      Origin = '"CO_STATES_HIST"."SUI_EFT_NAME"'
      Size = 40
    end
    object CO_STATESSUI_EFT_PIN_NUMBER: TStringField
      DisplayWidth = 10
      FieldName = 'SUI_EFT_PIN_NUMBER'
      Origin = '"CO_STATES_HIST"."SUI_EFT_PIN_NUMBER"'
      Size = 10
    end
    object CO_STATESSUI_EIN: TStringField
      DisplayWidth = 30
      FieldName = 'SUI_EIN'
      Origin = '"CO_STATES_HIST"."SUI_EIN"'
      Required = True
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 30
    end
    object CO_STATESSUI_EXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'SUI_EXEMPT'
      Origin = '"CO_STATES_HIST"."SUI_EXEMPT"'
      Required = True
      OnValidate = CO_STATESSDI_EXEMPTValidate
      Size = 1
    end
    object CO_STATESSUI_TAX_DEPOSIT_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'SUI_TAX_DEPOSIT_FREQUENCY'
      Origin = '"CO_STATES_HIST"."SUI_TAX_DEPOSIT_FREQUENCY"'
      Required = True
      Size = 1
    end
    object CO_STATESSUI_TAX_DEPOSIT_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'SUI_TAX_DEPOSIT_METHOD'
      Origin = '"CO_STATES_HIST"."SUI_TAX_DEPOSIT_METHOD"'
      Required = True
      Size = 1
    end
    object CO_STATESSY_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATES_NBR'
      Origin = '"CO_STATES_HIST"."SY_STATES_NBR"'
      Required = True
    end
    object CO_STATESSY_STATE_DEPOSIT_FREQ_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATE_DEPOSIT_FREQ_NBR'
      Origin = '"CO_STATES_HIST"."SY_STATE_DEPOSIT_FREQ_NBR"'
      Required = True
    end
    object CO_STATESTAX_RETURN_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_CODE'
      Origin = '"CO_STATES_HIST"."TAX_RETURN_CODE"'
      Size = 10
    end
    object CO_STATESUSE_DBA_ON_TAX_RETURN: TStringField
      DisplayWidth = 1
      FieldName = 'USE_DBA_ON_TAX_RETURN'
      Origin = '"CO_STATES_HIST"."USE_DBA_ON_TAX_RETURN"'
      Required = True
      Size = 1
    end
    object CO_STATESUSE_STATE_FOR_SUI: TStringField
      DisplayWidth = 1
      FieldName = 'USE_STATE_FOR_SUI'
      Origin = '"CO_STATES_HIST"."USE_STATE_FOR_SUI"'
      Required = True
      Size = 1
    end
    object CO_STATESName: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'Name'
      LookupDataSet = DM_SY_STATES.SY_STATES
      LookupKeyFields = 'SY_STATES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_STATES_NBR'
      Lookup = True
    end
    object CO_STATESMO_TAX_CREDIT_ACTIVE: TStringField
      DisplayWidth = 1
      FieldName = 'MO_TAX_CREDIT_ACTIVE'
      Origin = '"CO_STATES_HIST"."MO_TAX_CREDIT_ACTIVE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_STATESEE_SDI_EXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'EE_SDI_EXEMPT'
      Origin = '"CO_STATES_HIST"."EE_SDI_EXEMPT"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_STATESER_SDI_EXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'ER_SDI_EXEMPT'
      Origin = '"CO_STATES_HIST"."ER_SDI_EXEMPT"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_STATESAPPLIED_FOR: TStringField
      FieldName = 'APPLIED_FOR'
      Size = 1
    end
    object CO_STATESTCD_DEPOSIT_FREQUENCY_NBR: TIntegerField
      FieldName = 'TCD_DEPOSIT_FREQUENCY_NBR'
    end
    object CO_STATESTaxCollectionDistrict: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TaxCollectionDistrict'
      Calculated = True
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 80
    Top = 112
  end
end
