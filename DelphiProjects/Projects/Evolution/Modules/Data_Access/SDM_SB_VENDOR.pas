// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_VENDOR;

interface

uses
  SDM_ddTable, SDataDictBureau,
  SysUtils, Classes, DB,   SDataStructure, SDDClasses, SDataDictsystem,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, EvConsts;

type
  TDM_SB_VENDOR = class(TDM_ddTable)
    SB_VENDOR: TSB_VENDOR;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_VENDORLEVEL_DESC: TStringField;
    SB_VENDORNAME: TStringField;
    SB_VENDORVENDOR_NBR: TIntegerField;
    SB_VENDORVENDORS_LEVEL: TStringField;
    SB_VENDORSB_VENDOR_NBR: TIntegerField;
    procedure SB_VENDORCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TDM_SB_VENDOR.SB_VENDORCalcFields(DataSet: TDataSet);
begin
  inherited;

  if SB_VENDORVENDOR_NBR.AsInteger > 0  then
  begin
   if not DM_SYSTEM_MISC.SY_VENDORS.Active then
       DM_SYSTEM_MISC.SY_VENDORS.DataRequired('ALL');

   if not DM_SERVICE_BUREAU.SB_CUSTOM_VENDORS.Active then
       DM_SERVICE_BUREAU.SB_CUSTOM_VENDORS.DataRequired('ALL');

   SB_VENDORLEVEL_DESC.AsString := 'System';
   if SB_VENDORVENDORS_LEVEL.AsString[1] = LEVEL_BUREAU then
   begin
      SB_VENDORLEVEL_DESC.AsString :=  'Bureau';
      SB_VENDORNAME.AsString := DM_SERVICE_BUREAU.SB_CUSTOM_VENDORS.ShadowDataSet.CachedLookup(SB_VENDORVENDOR_NBR.AsInteger, 'VENDOR_NAME');
   end
   else
      SB_VENDORNAME.AsString := DM_SYSTEM_MISC.SY_VENDORS.ShadowDataSet.CachedLookup(SB_VENDORVENDOR_NBR.AsInteger, 'VENDOR_NAME');
  end;

end;

initialization
  RegisterClass(TDM_SB_VENDOR);

finalization
  UnregisterClass(TDM_SB_VENDOR);

end.
