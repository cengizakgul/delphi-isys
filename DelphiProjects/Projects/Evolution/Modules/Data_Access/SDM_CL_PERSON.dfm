inherited DM_CL_PERSON: TDM_CL_PERSON
  OldCreateOrder = True
  Left = 467
  Top = 357
  Height = 263
  Width = 460
  object CL_PERSON: TCL_PERSON
    ProviderName = 'CL_PERSON_PROV'
    PictureMasks.Strings = (
      'STATE'#9'*{&,@}'#9'T'#9'T')
    BeforePost = CL_PERSONBeforePost
    Left = 66
    Top = 57
    object CL_PERSONCL_PERSON_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_PERSON_NBR'
      Origin = '"CL_PERSON_HIST"."CL_PERSON_NBR"'
      Required = True
    end
    object CL_PERSONSOCIAL_SECURITY_NUMBER: TStringField
      DisplayWidth = 11
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Origin = '"CL_PERSON_HIST"."SOCIAL_SECURITY_NUMBER"'
      Required = True
      OnValidate = CL_PERSONSOCIAL_SECURITY_NUMBERValidate
      Size = 11
    end
    object CL_PERSONEIN_OR_SOCIAL_SECURITY_NUMBER: TStringField
      DisplayWidth = 1
      FieldName = 'EIN_OR_SOCIAL_SECURITY_NUMBER'
      Origin = '"CL_PERSON_HIST"."EIN_OR_SOCIAL_SECURITY_NUMBER"'
      Required = True
      OnValidate = CL_PERSONEIN_OR_SOCIAL_SECURITY_NUMBERValidate
      FixedChar = True
      Size = 1
    end
    object CL_PERSONFIRST_NAME: TStringField
      DisplayWidth = 20
      FieldName = 'FIRST_NAME'
      Origin = '"CL_PERSON_HIST"."FIRST_NAME"'
      Required = True
    end
    object CL_PERSONMIDDLE_INITIAL: TStringField
      DisplayWidth = 1
      FieldName = 'MIDDLE_INITIAL'
      Origin = '"CL_PERSON_HIST"."MIDDLE_INITIAL"'
      FixedChar = True
      Size = 1
    end
    object CL_PERSONLAST_NAME: TStringField
      DisplayWidth = 30
      FieldName = 'LAST_NAME'
      Origin = '"CL_PERSON_HIST"."LAST_NAME"'
      Required = True
      Size = 30
    end
    object CL_PERSONW2_FIRST_NAME: TStringField
      DisplayWidth = 20
      FieldName = 'W2_FIRST_NAME'
      Origin = '"CL_PERSON_HIST"."W2_FIRST_NAME"'
    end
    object CL_PERSONW2_MIDDLE_NAME: TStringField
      DisplayWidth = 20
      FieldName = 'W2_MIDDLE_NAME'
      Origin = '"CL_PERSON_HIST"."W2_MIDDLE_NAME"'
    end
    object CL_PERSONW2_LAST_NAME: TStringField
      DisplayWidth = 30
      FieldName = 'W2_LAST_NAME'
      Origin = '"CL_PERSON_HIST"."W2_LAST_NAME"'
      Size = 30
    end
    object CL_PERSONW2_NAME_SUFFIX: TStringField
      DisplayWidth = 10
      FieldName = 'W2_NAME_SUFFIX'
      Origin = '"CL_PERSON_HIST"."W2_NAME_SUFFIX"'
      Size = 10
    end
    object CL_PERSONADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"CL_PERSON_HIST"."ADDRESS1"'
      Required = True
      Size = 30
    end
    object CL_PERSONADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"CL_PERSON_HIST"."ADDRESS2"'
      Size = 30
    end
    object CL_PERSONCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CL_PERSON_HIST"."CITY"'
      Required = True
    end
    object CL_PERSONSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CL_PERSON_HIST"."STATE"'
      Required = True
      FixedChar = True
      Size = 2
    end
    object CL_PERSONZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CL_PERSON_HIST"."ZIP_CODE"'
      Required = True
      Size = 10
    end
    object CL_PERSONCOUNTY: TStringField
      DisplayWidth = 30
      FieldName = 'COUNTY'
      Origin = '"CL_PERSON_HIST"."COUNTY"'
      Size = 30
    end
    object CL_PERSONCOUNTRY: TStringField
      DisplayWidth = 40
      FieldName = 'COUNTRY'
      Origin = '"CL_PERSON_HIST"."COUNTRY"'
      Size = 40
    end
    object CL_PERSONPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"CL_PERSON_HIST"."PHONE1"'
    end
    object CL_PERSONDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"CL_PERSON_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object CL_PERSONPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"CL_PERSON_HIST"."PHONE2"'
    end
    object CL_PERSONDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"CL_PERSON_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object CL_PERSONPHONE3: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE3'
      Origin = '"CL_PERSON_HIST"."PHONE3"'
    end
    object CL_PERSONDESCRIPTION3: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION3'
      Origin = '"CL_PERSON_HIST"."DESCRIPTION3"'
      Size = 10
    end
    object CL_PERSONGENDER: TStringField
      DisplayWidth = 1
      FieldName = 'GENDER'
      Origin = '"CL_PERSON_HIST"."GENDER"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONETHNICITY: TStringField
      DisplayWidth = 1
      FieldName = 'ETHNICITY'
      Origin = '"CL_PERSON_HIST"."ETHNICITY"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONSMOKER: TStringField
      DisplayWidth = 1
      FieldName = 'SMOKER'
      Origin = '"CL_PERSON_HIST"."SMOKER"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONDRIVERS_LICENSE_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'DRIVERS_LICENSE_NUMBER'
      Origin = '"CL_PERSON_HIST"."DRIVERS_LICENSE_NUMBER"'
    end
    object CL_PERSONDRIVERS_LICENSE_EXP_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'DRIVERS_LICENSE_EXP_DATE'
      Origin = '"CL_PERSON_HIST"."DRIVERS_LICENSE_EXP_DATE"'
    end
    object CL_PERSONAUTO_INSURANCE_CARRIER: TStringField
      DisplayWidth = 30
      FieldName = 'AUTO_INSURANCE_CARRIER'
      Origin = '"CL_PERSON_HIST"."AUTO_INSURANCE_CARRIER"'
      Size = 30
    end
    object CL_PERSONAUTO_INSURANCE_POLICY_NUMBER: TStringField
      DisplayWidth = 30
      FieldName = 'AUTO_INSURANCE_POLICY_NUMBER'
      Origin = '"CL_PERSON_HIST"."AUTO_INSURANCE_POLICY_NUMBER"'
      Size = 30
    end
    object CL_PERSONAUTO_INSURANCE_POLICY_EXP_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'AUTO_INSURANCE_POLICY_EXP_DATE'
      Origin = '"CL_PERSON_HIST"."AUTO_INSURANCE_POLICY_EXP_DATE"'
    end
    object CL_PERSONVISA_NUMBER: TStringField
      DisplayWidth = 15
      FieldName = 'VISA_NUMBER'
      Origin = '"CL_PERSON_HIST"."VISA_NUMBER"'
      Size = 15
    end
    object CL_PERSONVETERAN: TStringField
      DisplayWidth = 1
      FieldName = 'VETERAN'
      Origin = '"CL_PERSON_HIST"."VETERAN"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONVETERAN_DISCHARGE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'VETERAN_DISCHARGE_DATE'
      Origin = '"CL_PERSON_HIST"."VETERAN_DISCHARGE_DATE"'
    end
    object CL_PERSONVIETNAM_VETERAN: TStringField
      DisplayWidth = 1
      FieldName = 'VIETNAM_VETERAN'
      Origin = '"CL_PERSON_HIST"."VIETNAM_VETERAN"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONDISABLED_VETERAN: TStringField
      DisplayWidth = 1
      FieldName = 'DISABLED_VETERAN'
      Origin = '"CL_PERSON_HIST"."DISABLED_VETERAN"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONMILITARY_RESERVE: TStringField
      DisplayWidth = 1
      FieldName = 'MILITARY_RESERVE'
      Origin = '"CL_PERSON_HIST"."MILITARY_RESERVE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONI9_ON_FILE: TStringField
      DisplayWidth = 1
      FieldName = 'I9_ON_FILE'
      Origin = '"CL_PERSON_HIST"."I9_ON_FILE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONPICTURE: TBlobField
      DisplayWidth = 10
      FieldName = 'PICTURE'
      Origin = '"CL_PERSON_HIST"."PICTURE"'
      Size = 8
    end
    object CL_PERSONSY_HR_ETHNICITY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_HR_ETHNICITY_NBR'
      Origin = '"CL_PERSON_HIST"."SY_HR_ETHNICITY_NBR"'
    end
    object CL_PERSONNOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Origin = '"CL_PERSON_HIST"."NOTES"'
      Size = 8
    end
    object CL_PERSONFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CL_PERSON_HIST"."FILLER"'
      Size = 512
    end
    object CL_PERSONCITIZENSHIP: TStringField
      DisplayWidth = 30
      FieldName = 'CITIZENSHIP'
      Size = 30
    end
    object CL_PERSONVISA_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'VISA_TYPE'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CL_PERSONVISA_EXPIRATION_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'VISA_EXPIRATION_DATE'
    end
    object CL_PERSONRESIDENTIAL_STATE_NBR: TIntegerField
      FieldName = 'RESIDENTIAL_STATE_NBR'
    end
    object CL_PERSONWEB_PASSWORD: TStringField
      FieldName = 'WEB_PASSWORD'
    end
  end
end
