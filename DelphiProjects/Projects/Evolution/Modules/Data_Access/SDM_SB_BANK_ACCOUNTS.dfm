inherited DM_SB_BANK_ACCOUNTS: TDM_SB_BANK_ACCOUNTS
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 80
    Top = 224
  end
  object SB_BANK_ACCOUNTS: TSB_BANK_ACCOUNTS
    ProviderName = 'SB_BANK_ACCOUNTS_PROV'
    PictureMasks.Strings = (
      
        'BEGINNING_BALANCE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F')
    BlobsLoadMode = blmManualLoad
    Left = 80
    Top = 72
    object SB_BANK_ACCOUNTSBank_Name_Lookup: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'Bank_Name_Lookup'
      LookupDataSet = DM_SB_BANKS.SB_BANKS
      LookupKeyFields = 'SB_BANKS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SB_BANKS_NBR'
      Size = 40
      Lookup = True
    end
    object SB_BANK_ACCOUNTSSB_BANK_ACCOUNTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_BANK_ACCOUNTS_NBR'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."SB_BANK_ACCOUNTS_NBR"'
      Required = True
    end
    object SB_BANK_ACCOUNTSCUSTOM_BANK_ACCOUNT_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_BANK_ACCOUNT_NUMBER'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."CUSTOM_BANK_ACCOUNT_NUMBER"'
      Required = True
    end
    object SB_BANK_ACCOUNTSBANK_ACCOUNT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'BANK_ACCOUNT_TYPE'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."BANK_ACCOUNT_TYPE"'
      Required = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSNAME_DESCRIPTION: TStringField
      DisplayWidth = 40
      FieldName = 'NAME_DESCRIPTION'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."NAME_DESCRIPTION"'
      Size = 40
    end
    object SB_BANK_ACCOUNTSSB_BANKS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_BANKS_NBR'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."SB_BANKS_NBR"'
      Required = True
      Visible = False
    end
    object SB_BANK_ACCOUNTSACH_ORIGIN_SB_BANKS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'ACH_ORIGIN_SB_BANKS_NBR'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."ACH_ORIGIN_SB_BANKS_NBR"'
      Required = True
      Visible = False
    end
    object SB_BANK_ACCOUNTSBANK_RETURNS: TStringField
      DisplayWidth = 1
      FieldName = 'BANK_RETURNS'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."BANK_RETURNS"'
      Required = True
      Visible = False
      Size = 1
    end
    object SB_BANK_ACCOUNTSCUSTOM_HEADER_RECORD: TStringField
      DisplayWidth = 80
      FieldName = 'CUSTOM_HEADER_RECORD'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."CUSTOM_HEADER_RECORD"'
      Visible = False
      Size = 80
    end
    object SB_BANK_ACCOUNTSBEGINNING_BALANCE: TFloatField
      DisplayWidth = 10
      FieldName = 'BEGINNING_BALANCE'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."BEGINNING_BALANCE"'
      Visible = False
      DisplayFormat = '#,##0.00'
    end
    object SB_BANK_ACCOUNTSNEXT_AVAILABLE_CHECK_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'NEXT_AVAILABLE_CHECK_NUMBER'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."NEXT_AVAILABLE_CHECK_NUMBER"'
      Required = True
      Visible = False
    end
    object SB_BANK_ACCOUNTSEND_CHECK_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'END_CHECK_NUMBER'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."END_CHECK_NUMBER"'
      Visible = False
    end
    object SB_BANK_ACCOUNTSNEXT_BEGIN_CHECK_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'NEXT_BEGIN_CHECK_NUMBER'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."NEXT_BEGIN_CHECK_NUMBER"'
      Visible = False
    end
    object SB_BANK_ACCOUNTSNEXT_END_CHECK_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'NEXT_END_CHECK_NUMBER'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."NEXT_END_CHECK_NUMBER"'
      Visible = False
    end
    object SB_BANK_ACCOUNTSFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."FILLER"'
      Visible = False
      Size = 512
    end
    object SB_BANK_ACCOUNTSSUPPRESS_OFFSET_ACCOUNT: TStringField
      FieldName = 'SUPPRESS_OFFSET_ACCOUNT'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."SUPPRESS_OFFSET_ACCOUNT"'
      Required = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSBLOCK_NEGATIVE_CHECKS: TStringField
      FieldName = 'BLOCK_NEGATIVE_CHECKS'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."BLOCK_NEGATIVE_CHECKS"'
      Required = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSBATCH_FILER_ID: TStringField
      FieldName = 'BATCH_FILER_ID'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."BATCH_FILER_ID"'
      Size = 9
    end
    object SB_BANK_ACCOUNTSMASTER_INQUIRY_PIN: TStringField
      FieldName = 'MASTER_INQUIRY_PIN'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."MASTER_INQUIRY_PIN"'
      Size = 10
    end
    object SB_BANK_ACCOUNTSLOGO_SB_BLOB_NBR: TIntegerField
      FieldName = 'LOGO_SB_BLOB_NBR'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."LOGO_SB_BLOB_NBR"'
    end
    object SB_BANK_ACCOUNTSSIGNATURE_SB_BLOB_NBR: TIntegerField
      FieldName = 'SIGNATURE_SB_BLOB_NBR'
      Origin = '"SB_BANK_ACCOUNTS_HIST"."SIGNATURE_SB_BLOB_NBR"'
    end
    object SB_BANK_ACCOUNTSOPERATING_ACCOUNT: TStringField
      FieldName = 'OPERATING_ACCOUNT'
      FixedChar = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSBILLING_ACCOUNT: TStringField
      FieldName = 'BILLING_ACCOUNT'
      FixedChar = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSACH_ACCOUNT: TStringField
      FieldName = 'ACH_ACCOUNT'
      FixedChar = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSTRUST_ACCOUNT: TStringField
      FieldName = 'TRUST_ACCOUNT'
      FixedChar = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSTAX_ACCOUNT: TStringField
      FieldName = 'TAX_ACCOUNT'
      FixedChar = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSOBC_ACCOUNT: TStringField
      FieldName = 'OBC_ACCOUNT'
      FixedChar = True
      Size = 1
    end
    object SB_BANK_ACCOUNTSWORKERS_COMP_ACCOUNT: TStringField
      FieldName = 'WORKERS_COMP_ACCOUNT'
      FixedChar = True
      Size = 1
    end
  end
end
