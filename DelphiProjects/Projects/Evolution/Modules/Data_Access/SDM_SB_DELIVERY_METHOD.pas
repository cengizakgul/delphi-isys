// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_DELIVERY_METHOD;

interface

uses
  SDM_ddTable, SDataDictBureau,
  SysUtils, Classes, DB,   SDataStructure, SDDClasses, SDataDictsystem,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet;

type
  TDM_SB_DELIVERY_METHOD = class(TDM_ddTable)
    SB_DELIVERY_METHOD: TSB_DELIVERY_METHOD;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_DELIVERY_METHODSB_DELIVERY_METHOD_NBR: TIntegerField;
    SB_DELIVERY_METHODSY_DELIVERY_METHOD_NBR: TIntegerField;
    SB_DELIVERY_METHODluMethodName: TStringField;
    SB_DELIVERY_METHODCLASS_NAME: TStringField;
    SB_DELIVERY_METHODluServiceName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

initialization
  RegisterClass(TDM_SB_DELIVERY_METHOD);

finalization
  UnregisterClass(TDM_SB_DELIVERY_METHOD);

end.
