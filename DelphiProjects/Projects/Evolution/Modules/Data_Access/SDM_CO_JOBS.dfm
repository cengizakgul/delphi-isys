inherited DM_CO_JOBS: TDM_CO_JOBS
  OldCreateOrder = True
  Left = 266
  Top = 86
  Height = 540
  Width = 783
  object CO_JOBS: TCO_JOBS
    ProviderName = 'CO_JOBS_PROV'
    Left = 72
    Top = 32
    object CO_JOBSCERTIFIED: TStringField
      FieldName = 'CERTIFIED'
      Size = 1
    end
    object CO_JOBSCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
    end
    object CO_JOBSCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_JOBSCO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
    end
    object CO_JOBSDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object CO_JOBSFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object CO_JOBSGENERAL_LEDGER_TAG: TStringField
      FieldName = 'GENERAL_LEDGER_TAG'
    end
    object CO_JOBSJOB_ACTIVE: TStringField
      FieldName = 'JOB_ACTIVE'
      Size = 1
    end
    object CO_JOBSMISC_JOB_CODE: TStringField
      DisplayWidth = 40
      FieldName = 'MISC_JOB_CODE'
      Size = 40
    end
    object CO_JOBSRATE_PER_HOUR: TFloatField
      FieldName = 'RATE_PER_HOUR'
      DisplayFormat = '#,##0.00##'
    end
    object CO_JOBSSTATE_CERTIFIED: TStringField
      FieldName = 'STATE_CERTIFIED'
      Size = 1
    end
    object CO_JOBSTRUE_DESCRIPTION: TStringField
      FieldName = 'TRUE_DESCRIPTION'
      Size = 40
    end
    object CO_JOBSHOME_CO_STATES_NBR: TIntegerField
      FieldName = 'HOME_CO_STATES_NBR'
    end
  end
end
