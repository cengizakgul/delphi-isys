inherited DM_CO_TEAM: TDM_CO_TEAM
  OldCreateOrder = True
  Left = 519
  Top = 285
  Height = 480
  Width = 696
  object CO_TEAM: TCO_TEAM
    ProviderName = 'CO_TEAM_PROV'
    PictureMasks.Strings = (
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      'FAX'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE1'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      'PHONE2'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      
        'OVERRIDE_PAY_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    BeforePost = CO_TEAMBeforePost
    OnCalcFields = CO_TEAMCalcFields
    BlobsLoadMode = blmManualLoad
    Left = 176
    Top = 144
    object CO_TEAMCO_TEAM_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TEAM_NBR'
      Origin = '"CO_TEAM_HIST"."CO_TEAM_NBR"'
      Required = True
    end
    object CO_TEAMCO_DEPARTMENT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DEPARTMENT_NBR'
      Origin = '"CO_TEAM_HIST"."CO_DEPARTMENT_NBR"'
      Required = True
    end
    object CO_TEAMCUSTOM_TEAM_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_TEAM_NUMBER'
      Origin = '"CO_TEAM_HIST"."CUSTOM_TEAM_NUMBER"'
      Required = True
    end
    object CO_TEAMNAME: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = '"CO_TEAM_HIST"."NAME"'
      Size = 40
    end
    object CO_TEAMADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"CO_TEAM_HIST"."ADDRESS1"'
      Size = 30
    end
    object CO_TEAMADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"CO_TEAM_HIST"."ADDRESS2"'
      Size = 30
    end
    object CO_TEAMCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CO_TEAM_HIST"."CITY"'
    end
    object CO_TEAMSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CO_TEAM_HIST"."STATE"'
      Size = 2
    end
    object CO_TEAMZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CO_TEAM_HIST"."ZIP_CODE"'
      Size = 10
    end
    object CO_TEAMCONTACT1: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT1'
      Origin = '"CO_TEAM_HIST"."CONTACT1"'
      Size = 30
    end
    object CO_TEAMPHONE1: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE1'
      Origin = '"CO_TEAM_HIST"."PHONE1"'
    end
    object CO_TEAMDESCRIPTION1: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION1'
      Origin = '"CO_TEAM_HIST"."DESCRIPTION1"'
      Size = 10
    end
    object CO_TEAMCONTACT2: TStringField
      DisplayWidth = 30
      FieldName = 'CONTACT2'
      Origin = '"CO_TEAM_HIST"."CONTACT2"'
      Size = 30
    end
    object CO_TEAMPHONE2: TStringField
      DisplayWidth = 20
      FieldName = 'PHONE2'
      Origin = '"CO_TEAM_HIST"."PHONE2"'
    end
    object CO_TEAMDESCRIPTION2: TStringField
      DisplayWidth = 10
      FieldName = 'DESCRIPTION2'
      Origin = '"CO_TEAM_HIST"."DESCRIPTION2"'
      Size = 10
    end
    object CO_TEAMFAX: TStringField
      DisplayWidth = 20
      FieldName = 'FAX'
      Origin = '"CO_TEAM_HIST"."FAX"'
    end
    object CO_TEAMFAX_DESCRIPTION: TStringField
      DisplayWidth = 10
      FieldName = 'FAX_DESCRIPTION'
      Origin = '"CO_TEAM_HIST"."FAX_DESCRIPTION"'
      Size = 10
    end
    object CO_TEAME_MAIL_ADDRESS: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS'
      Origin = '"CO_TEAM_HIST"."E_MAIL_ADDRESS"'
      Size = 80
    end
    object CO_TEAMHOME_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_CO_STATES_NBR'
      Origin = '"CO_TEAM_HIST"."HOME_CO_STATES_NBR"'
      Required = True
    end
    object CO_TEAMPAY_FREQUENCY_HOURLY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_HOURLY'
      Origin = '"CO_TEAM_HIST"."PAY_FREQUENCY_HOURLY"'
      Required = True
      Size = 1
    end
    object CO_TEAMPAY_FREQUENCY_SALARY: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_SALARY'
      Origin = '"CO_TEAM_HIST"."PAY_FREQUENCY_SALARY"'
      Required = True
      Size = 1
    end
    object CO_TEAMCL_TIMECLOCK_IMPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_TIMECLOCK_IMPORTS_NBR'
      Origin = '"CO_TEAM_HIST"."CL_TIMECLOCK_IMPORTS_NBR"'
    end
    object CO_TEAMPRINT_GROUP_ADDRESS_ON_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_GROUP_ADDRESS_ON_CHECK'
      Origin = '"CO_TEAM_HIST"."PRINT_GROUP_ADDRESS_ON_CHECK"'
      Required = True
      Size = 1
    end
    object CO_TEAMPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_TEAM_HIST"."PAYROLL_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_TEAMCL_BILLING_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_BILLING_NBR'
      Origin = '"CO_TEAM_HIST"."CL_BILLING_NBR"'
    end
    object CO_TEAMBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BILLING_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_TEAM_HIST"."BILLING_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_TEAMTAX_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_TEAM_HIST"."TAX_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_TEAMDD_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DD_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_TEAM_HIST"."DD_CL_BANK_ACCOUNT_NBR"'
    end
    object CO_TEAMUNION_CL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'UNION_CL_E_DS_NBR'
      Origin = '"CO_TEAM_HIST"."UNION_CL_E_DS_NBR"'
    end
    object CO_TEAMOVERRIDE_EE_RATE_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_EE_RATE_NUMBER'
      Origin = '"CO_TEAM_HIST"."OVERRIDE_EE_RATE_NUMBER"'
    end
    object CO_TEAMOVERRIDE_PAY_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_PAY_RATE'
      Origin = '"CO_TEAM_HIST"."OVERRIDE_PAY_RATE"'
    end
    object CO_TEAMCO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"CO_TEAM_HIST"."CO_WORKERS_COMP_NBR"'
    end
    object CO_TEAMTEAM_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'TEAM_NOTES'
      Origin = '"CO_TEAM_HIST"."TEAM_NOTES"'
      Size = 8
    end
    object CO_TEAMHOME_STATE_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'HOME_STATE_TYPE'
      Origin = '"CO_TEAM_HIST"."HOME_STATE_TYPE"'
      Required = True
      Size = 1
    end
    object CO_TEAMGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"CO_TEAM_HIST"."GENERAL_LEDGER_TAG"'
    end
    object CO_TEAMCL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_TEAM_HIST"."CL_DELIVERY_GROUP_NBR"'
    end
    object CO_TEAMPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
    end
    object CO_TEAMPR_EE_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_MB_GROUP_NBR'
    end
    object CO_TEAMPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
    end
    object CO_TEAMTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
    end
    object CO_TEAMPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_MB_GROUP_NBR'
    end
    object CO_TEAMPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
    end
    object CO_TEAMFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Size = 512
    end
    object CO_TEAMWORKSITE_ID: TStringField
      FieldName = 'WORKSITE_ID'
    end
    object CO_TEAMAccountNumber: TStringField
      FieldKind = fkCalculated
      FieldName = 'AccountNumber'
      Calculated = True
    end
    object CO_TEAMPrimary: TStringField
      FieldKind = fkCalculated
      FieldName = 'Primary'
      Size = 1
      Calculated = True
    end
  end
end
