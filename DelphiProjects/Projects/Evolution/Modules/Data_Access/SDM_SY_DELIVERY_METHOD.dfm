inherited DM_SY_DELIVERY_METHOD: TDM_SY_DELIVERY_METHOD
  Left = 422
  Top = 209
  Height = 292
  Width = 347
  object SY_DELIVERY_METHOD: TSY_DELIVERY_METHOD
    ProviderName = 'SY_DELIVERY_METHOD_PROV'
    Left = 72
    Top = 56
    object SY_DELIVERY_METHODSY_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SY_DELIVERY_METHOD_NBR'
    end
    object SY_DELIVERY_METHODSY_DELIVERY_SERVICE_NBR: TIntegerField
      FieldName = 'SY_DELIVERY_SERVICE_NBR'
    end
    object SY_DELIVERY_METHODCLASS_NAME: TStringField
      FieldName = 'CLASS_NAME'
      Size = 40
    end
    object SY_DELIVERY_METHODNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object SY_DELIVERY_METHODSERVICE_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'SERVICE_NAME'
      LookupDataSet = DM_SY_DELIVERY_SERVICE.SY_DELIVERY_SERVICE
      LookupKeyFields = 'SY_DELIVERY_SERVICE_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_DELIVERY_SERVICE_NBR'
      Size = 40
      Lookup = True
    end
    object SY_DELIVERY_METHODSERVICE_CLASS_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'SERVICE_CLASS_NAME'
      LookupDataSet = DM_SY_DELIVERY_SERVICE.SY_DELIVERY_SERVICE
      LookupKeyFields = 'SY_DELIVERY_SERVICE_NBR'
      LookupResultField = 'CLASS_NAME'
      KeyFields = 'SY_DELIVERY_SERVICE_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 208
    Top = 72
  end
end
