// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_TMP_CO_MANUAL_ACH;

interface

uses
  SDM_ddTable, SDataDictTemp,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure; 

type
  TDM_TMP_CO_MANUAL_ACH = class(TDM_ddTable)
    DM_TEMPORARY: TDM_TEMPORARY;
    TMP_CO_MANUAL_ACH: TTMP_CO_MANUAL_ACH;
    TMP_CO_MANUAL_ACHCO_MANUAL_ACH_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHCL_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHCO_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHEE_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHDEBIT_DATE: TDateField;
    TMP_CO_MANUAL_ACHCREDIT_DATE: TDateField;
    TMP_CO_MANUAL_ACHSTATUS: TStringField;
    TMP_CO_MANUAL_ACHSTATUS_DATE: TDateTimeField;
    TMP_CO_MANUAL_ACHAMOUNT: TFloatField;
    TMP_CO_MANUAL_ACHPURPOSE_NOTES: TBlobField;
    TMP_CO_MANUAL_ACHFROM_CO_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHFROM_EE_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHFROM_SB_BANK_ACCOUNTS_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHFROM_CL_BANK_ACCOUNT_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHFROM_EE_DIRECT_DEPOSIT_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHTO_CO_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHTO_EE_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHTO_SB_BANK_ACCOUNTS_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHTO_CL_BANK_ACCOUNT_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHTO_EE_DIRECT_DEPOSIT_NBR: TIntegerField;
    TMP_CO_MANUAL_ACHCompanyNumber: TStringField;
    TMP_CO_MANUAL_ACHCompanyName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_TMP_CO_MANUAL_ACH: TDM_TMP_CO_MANUAL_ACH;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_TMP_CO_MANUAL_ACH);

finalization
  UnregisterClass(TDM_TMP_CO_MANUAL_ACH);

end.
