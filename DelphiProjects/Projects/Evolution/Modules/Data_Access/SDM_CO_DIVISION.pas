// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_DIVISION;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, SDDClasses, Wwdatsrc,
  ISBasicClasses, EvContext, EvUIComponents, EvClientDataSet,
  EvDataAccessComponents;

type
  TDM_CO_DIVISION = class(TDM_ddTable)
    CO_DIVISION: TCO_DIVISION;
    CO_DIVISIONCO_DIVISION_NBR: TIntegerField;
    CO_DIVISIONCO_NBR: TIntegerField;
    CO_DIVISIONCUSTOM_DIVISION_NUMBER: TStringField;
    CO_DIVISIONNAME: TStringField;
    CO_DIVISIONADDRESS1: TStringField;
    CO_DIVISIONADDRESS2: TStringField;
    CO_DIVISIONCITY: TStringField;
    CO_DIVISIONSTATE: TStringField;
    CO_DIVISIONZIP_CODE: TStringField;
    CO_DIVISIONCONTACT1: TStringField;
    CO_DIVISIONPHONE1: TStringField;
    CO_DIVISIONDESCRIPTION1: TStringField;
    CO_DIVISIONCONTACT2: TStringField;
    CO_DIVISIONPHONE2: TStringField;
    CO_DIVISIONDESCRIPTION2: TStringField;
    CO_DIVISIONFAX: TStringField;
    CO_DIVISIONFAX_DESCRIPTION: TStringField;
    CO_DIVISIONE_MAIL_ADDRESS: TStringField;
    CO_DIVISIONHOME_CO_STATES_NBR: TIntegerField;
    CO_DIVISIONCL_TIMECLOCK_IMPORTS_NBR: TIntegerField;
    CO_DIVISIONPAY_FREQUENCY_HOURLY: TStringField;
    CO_DIVISIONPAY_FREQUENCY_SALARY: TStringField;
    CO_DIVISIONPRINT_DIV_ADDRESS_ON_CHECKS: TStringField;
    CO_DIVISIONPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DIVISIONCL_BILLING_NBR: TIntegerField;
    CO_DIVISIONBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DIVISIONTAX_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DIVISIONDD_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DIVISIONCO_WORKERS_COMP_NBR: TIntegerField;
    CO_DIVISIONOVERRIDE_PAY_RATE: TFloatField;
    CO_DIVISIONOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CO_DIVISIONUNION_CL_E_DS_NBR: TIntegerField;
    CO_DIVISIONDIVISION_NOTES: TBlobField;
    CO_DIVISIONFILLER: TStringField;
    CO_DIVISIONHOME_STATE_TYPE: TStringField;
    CO_DIVISIONGENERAL_LEDGER_TAG: TStringField;
    CO_DIVISIONCL_DELIVERY_GROUP_NBR: TIntegerField;
    CO_DIVISIONPR_CHECK_MB_GROUP_NBR: TIntegerField;
    CO_DIVISIONPR_EE_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_DIVISIONPR_EE_REPORT_SEC_MB_GROUP_NBR: TIntegerField;
    CO_DIVISIONTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    CO_DIVISIONPR_DBDT_REPORT_MB_GROUP_NBR: TIntegerField;
    CO_DIVISIONPR_DBDT_REPORT_SC_MB_GROUP_NBR: TIntegerField;
    CO_DIVISIONWORKSITE_ID: TStringField;
    CO_DIVISIONAccountNumber: TStringField;
    CO_DIVISIONPrimary: TStringField;
    procedure CO_DIVISIONBeforePost(DataSet: TDataSet);
    procedure CO_DIVISIONCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_DIVISION: TDM_CO_DIVISION;

implementation

uses
  EvUtils, Variants;

{$R *.DFM}

procedure TDM_CO_DIVISION.CO_DIVISIONBeforePost(DataSet: TDataSet);
var
 s: String;
 bLookup, bCalcFields : Boolean;
begin

  bLookup := DM_COMPANY.CO_DIVISION.LookupsEnabled;
  bCalcFields := DM_COMPANY.CO_DIVISION.AlwaysCallCalcFields;

  DM_COMPANY.CO_DIVISION.AlwaysCallCalcFields := false;
  DM_COMPANY.CO_DIVISION.LookupsEnabled := false;
  try
    HandleCustomNumber(DataSet.FieldByName('CUSTOM_DIVISION_NUMBER'), 20);

    if Trim(DataSet.FieldByName('AccountNumber').AsString) <> '' then
    begin
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('AccountNumber').AsString, 1, 20);
       DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, DataSet.FieldByName('Primary').AsString, 21, 1);
    end
    else
    begin
      if not DataSet.FieldByName('FILLER').IsNull then
      begin
        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '', 1, 20);

        s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString,21, 1);
        if trim(s) <> '' then
           DataSet.FieldByName('FILLER').AsString := PutIntoFiller(DataSet.FieldByName('FILLER').AsString, '',21,  1);
        DataSet.FieldByName('Primary').AsString := '';
      end;
    end;
  finally
    DM_COMPANY.CO_DIVISION.AlwaysCallCalcFields := bCalcFields;
    DM_COMPANY.CO_DIVISION.LookupsEnabled := bLookup;
  end;


end;

procedure TDM_CO_DIVISION.CO_DIVISIONCalcFields(DataSet: TDataSet);
var
 s : String;
begin
  inherited;
  if not DataSet.FieldByName('FILLER').IsNull then
  begin
    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 20);
    if trim(s) <> '' then
      Dataset.FieldByName('AccountNumber').AsString := s;

    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 21, 1);
    if trim(s) <> '' then
      Dataset.FieldByName('Primary').AsString := s;
  end;
end;

initialization
  RegisterClass(TDM_CO_DIVISION);

finalization
  UnregisterClass(TDM_CO_DIVISION);

end.
