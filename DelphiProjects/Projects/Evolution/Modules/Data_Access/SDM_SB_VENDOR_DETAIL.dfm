inherited DM_SB_VENDOR_DETAIL: TDM_SB_VENDOR_DETAIL
  OldCreateOrder = True
  Left = 548
  Top = 203
  Height = 330
  Width = 347
  object SB_VENDOR_DETAIL: TSB_VENDOR_DETAIL
    ProviderName = 'SB_VENDOR_DETAIL_PROV'
    OnCalcFields = SB_VENDOR_DETAILCalcFields
    Left = 56
    Top = 48
    object SB_VENDOR_DETAILDETAIL_LEVEL_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'DETAIL_LEVEL_DESC'
      Size = 8
      Calculated = True
    end
    object SB_VENDOR_DETAILDETAIL_LEVEL: TStringField
      FieldName = 'DETAIL_LEVEL'
      Size = 1
    end
    object SB_VENDOR_DETAILSB_VENDOR_DETAIL_NBR: TIntegerField
      FieldName = 'SB_VENDOR_DETAIL_NBR'
    end
    object SB_VENDOR_DETAILSB_VENDOR_NBR: TIntegerField
      FieldName = 'SB_VENDOR_NBR'
    end
    object SB_VENDOR_DETAILDETAIL: TStringField
      FieldName = 'DETAIL'
      Size = 40
    end
    object SB_VENDOR_DETAILCO_DETAIL_REQUIRED: TStringField
      FieldName = 'CO_DETAIL_REQUIRED'
      Size = 1
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 184
    Top = 40
  end
end
