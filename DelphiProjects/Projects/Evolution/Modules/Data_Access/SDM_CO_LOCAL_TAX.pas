// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_LOCAL_TAX;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  SDataStructure,  EvTypes, Variants, EvExceptions,
  SDDClasses, SDataDictsystem, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvConsts, EvContext,
  EvClientDataSet;

type
  TDM_CO_LOCAL_TAX = class(TDM_ddTable)
    CO_LOCAL_TAX: TCO_LOCAL_TAX;
    CO_LOCAL_TAXLocalName: TStringField;
    CO_LOCAL_TAXlocalState: TStringField;
    CO_LOCAL_TAXCO_LOCAL_TAX_NBR: TIntegerField;
    CO_LOCAL_TAXCO_NBR: TIntegerField;
    CO_LOCAL_TAXSY_LOCALS_NBR: TIntegerField;
    CO_LOCAL_TAXEXEMPT: TStringField;
    CO_LOCAL_TAXLOCAL_EIN_NUMBER: TStringField;
    CO_LOCAL_TAXTAX_RETURN_CODE: TStringField;
    CO_LOCAL_TAXTAX_RATE: TFloatField;
    CO_LOCAL_TAXTAX_AMOUNT: TFloatField;
    CO_LOCAL_TAXSY_LOCAL_DEPOSIT_FREQ_NBR: TIntegerField;
    CO_LOCAL_TAXMISCELLANEOUS_AMOUNT: TFloatField;
    CO_LOCAL_TAXPAYMENT_METHOD: TStringField;
    CO_LOCAL_TAXFINAL_TAX_RETURN: TStringField;
    CO_LOCAL_TAXEFT_NAME: TStringField;
    CO_LOCAL_TAXEFT_STATUS: TStringField;
    CO_LOCAL_TAXFILLER: TStringField;
    CO_LOCAL_TAXGENERAL_LEDGER_TAG: TStringField;
    CO_LOCAL_TAXEFT_PIN_NUMBER: TStringField;
    CO_LOCAL_TAXOFFSET_GL_TAG: TStringField;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    CO_LOCAL_TAXLOCAL_ACTIVE: TStringField;
    CO_LOCAL_TAXSELF_ADJUST_TAX: TStringField;
    CO_LOCAL_TAXAUTOCREATE_ON_NEW_HIRE: TStringField;
    CO_LOCAL_TAXSY_STATES_NBR: TIntegerField;
    CO_LOCAL_TAXsystemRate: TFloatField;
    CO_LOCAL_TAXcompanyRate: TFloatField;
    CO_LOCAL_TAXAPPLIED_FOR: TStringField;
    CO_LOCAL_TAXCOUNTYName: TStringField;
    CO_LOCAL_TAXLocalTypeDesc: TStringField;
    CO_LOCAL_TAXDEDUCT: TStringField;
    CO_LOCAL_TAXLOCAL_TYPE: TStringField;
    CO_LOCAL_TAXCombineForTaxPayments: TStringField;
    CO_LOCAL_TAXPSDCode: TStringField;
    procedure CO_LOCAL_TAXBeforePost(DataSet: TDataSet);
    procedure CO_LOCAL_TAXAfterInsert(DataSet: TDataSet);
    procedure CO_LOCAL_TAXCalcFields(DataSet: TDataSet);
    procedure CO_LOCAL_TAXEXEMPTValidate(Sender: TField);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_LOCAL_TAX.CO_LOCAL_TAXBeforePost(DataSet: TDataSet);
begin
  DM_SYSTEM_LOCAL.SY_LOCALS.Activate;
  Dataset['SY_STATES_NBR'] := DM_SYSTEM_LOCAL.SY_LOCALS.Lookup('SY_LOCALS_NBR', Dataset['SY_LOCALS_NBR'], 'SY_STATES_NBR');
  if VarIsNull(DataSet['SY_STATES_NBR']) then
    raise EUpdateError.CreateHelp('Can''t find matching state in system table.'#13'Check if local is selected.', IDH_ConsistencyViolation);
end;

procedure TDM_CO_LOCAL_TAX.CO_LOCAL_TAXAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('SY_STATES_NBR').Required := False;
end;

procedure TDM_CO_LOCAL_TAX.CO_LOCAL_TAXCalcFields(DataSet: TDataSet);
begin
  if CO_LOCAL_TAXTAX_RATE.IsNull then
    CO_LOCAL_TAXcompanyRate.Value := CO_LOCAL_TAXsystemRate.Value
  else
    CO_LOCAL_TAXcompanyRate.AsFloat := CO_LOCAL_TAXTAX_RATE.AsFloat;

  if not DM_SYSTEM_LOCAL.SY_LOCALS.Active then
     DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');

  CO_LOCAL_TAXLOCAL_TYPE.AsString := DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.CachedLookup(CO_LOCAL_TAXSY_LOCALS_NBR.AsInteger,'LOCAL_TYPE');
  CO_LOCAL_TAXCombineForTaxPayments.AsString := DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.CachedLookup(CO_LOCAL_TAXSY_LOCALS_NBR.AsInteger,'COMBINE_FOR_TAX_PAYMENTS');
  if CO_LOCAL_TAXSY_STATES_NBR.AsInteger = 41 then
    CO_LOCAL_TAXPSDCode.AsString := DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.CachedLookup(CO_LOCAL_TAXSY_LOCALS_NBR.AsInteger,'LOCAL_TAX_IDENTIFIER');

end;

procedure TDM_CO_LOCAL_TAX.CO_LOCAL_TAXEXEMPTValidate(Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

initialization
  RegisterClass(TDM_CO_LOCAL_TAX);

finalization
  UnregisterClass(TDM_CO_LOCAL_TAX);

end.


