inherited DM_CO_HR_POSITION_GRADES: TDM_CO_HR_POSITION_GRADES
  OldCreateOrder = True
  Left = 520
  Top = 200
  Height = 479
  Width = 741
  object CO_HR_POSITION_GRADES: TCO_HR_POSITION_GRADES
    ProviderName = 'CO_HR_POSITION_GRADES_PROV'
    OnCalcFields = CO_HR_POSITION_GRADESCalcFields
    Left = 200
    Top = 120
    object CO_HR_POSITION_GRADESCO_HR_POSITION_GRADES_NBR: TIntegerField
      FieldName = 'CO_HR_POSITION_GRADES_NBR'
    end
    object CO_HR_POSITION_GRADESCO_HR_POSITIONS_NBR: TIntegerField
      FieldName = 'CO_HR_POSITIONS_NBR'
    end
    object CO_HR_POSITION_GRADESCO_HR_SALARY_GRADES_NBR: TIntegerField
      FieldName = 'CO_HR_SALARY_GRADES_NBR'
    end
    object CO_HR_POSITION_GRADESDESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRIPTION'
      LookupDataSet = DM_CO_HR_SALARY_GRADES.CO_HR_SALARY_GRADES
      Size = 40
      Calculated = True
    end
    object CO_HR_POSITION_GRADESMINIMUM_PAY: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'MINIMUM_PAY'
      LookupDataSet = DM_CO_HR_SALARY_GRADES.CO_HR_SALARY_GRADES
      Calculated = True
    end
    object CO_HR_POSITION_GRADESMID_PAY: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'MID_PAY'
      LookupDataSet = DM_CO_HR_SALARY_GRADES.CO_HR_SALARY_GRADES
      Calculated = True
    end
    object CO_HR_POSITION_GRADESMAXIMUM_PAY: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'MAXIMUM_PAY'
      LookupDataSet = DM_CO_HR_SALARY_GRADES.CO_HR_SALARY_GRADES
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 336
    Top = 80
  end
end
