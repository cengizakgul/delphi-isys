inherited DM_SY_FED_TAX_PAYMENT_AGENCY: TDM_SY_FED_TAX_PAYMENT_AGENCY
  OldCreateOrder = False
  Left = 271
  Top = 107
  Height = 249
  Width = 424
  object SY_FED_TAX_PAYMENT_AGENCY: TSY_FED_TAX_PAYMENT_AGENCY
    Aggregates = <>
    Params = <>
    ProviderName = 'SY_FED_TAX_PAYMENT_AGENCY_PROV'
    ValidateWithMask = True
    Left = 97
    Top = 27
    object SY_FED_TAX_PAYMENT_AGENCYNAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'AGENCY_NAME'
      LookupDataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
      LookupKeyFields = 'SY_GLOBAL_AGENCY_NBR'
      LookupResultField = 'AGENCY_NAME'
      KeyFields = 'SY_GLOBAL_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object SY_FED_TAX_PAYMENT_AGENCYSY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
      Visible = False
    end
    object SY_FED_TAX_PAYMENT_AGENCYSY_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_REPORTS_NBR'
      Visible = False
    end
    object SY_FED_TAX_PAYMENT_AGENCYREPORT_DESCRIPTION: TStringField
      FieldKind = fkLookup
      FieldName = 'REPORT_DESCRIPTION'
      LookupDataSet = DM_SY_REPORTS.SY_REPORTS
      LookupKeyFields = 'SY_REPORTS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SY_REPORTS_NBR'
      Visible = False
      Size = 40
      Lookup = True
    end
    object SY_FED_TAX_PAYMENT_AGENCYSY_GLOBAL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
      Visible = False
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 102
    Top = 96
  end
end
