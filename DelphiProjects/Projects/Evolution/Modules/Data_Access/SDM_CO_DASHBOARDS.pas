unit SDM_CO_DASHBOARDS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet,
  SDDClasses, Variants;

type
  TDM_CO_DASHBOARDS = class(TDM_ddTable)
    DM_CLIENT: TDM_CLIENT;
    CO_DASHBOARDS: TCO_DASHBOARDS;
    CO_DASHBOARDSDASHBOARD_LEVEL: TStringField;
    CO_DASHBOARDSDASHBOARD_NBR: TIntegerField;
    CO_DASHBOARDSDESCRIPTION: TStringField;
    CO_DASHBOARDSLEVEL_DESC: TStringField;
    CO_DASHBOARDSCO_DASHBOARDS_NBR: TIntegerField;
    procedure CO_DASHBOARDSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_DASHBOARDS: TDM_CO_DASHBOARDS;

implementation

uses SDataDictbureau;

{$R *.DFM}


procedure TDM_CO_DASHBOARDS.CO_DASHBOARDSCalcFields(DataSet: TDataSet);
begin
  inherited;


  if CO_DASHBOARDSDASHBOARD_NBR.AsInteger > 0  then
  begin
    if not DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Active then
       DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.DataRequired('ALL');

    CO_DASHBOARDSLEVEL_DESC.AsString:= 'System';

    if DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Locate('DASHBOARD_LEVEL;DASHBOARD_NBR',
                       VarArrayOf([CO_DASHBOARDSDASHBOARD_LEVEL.AsString, CO_DASHBOARDSDASHBOARD_NBR.AsVariant]),[]) then
        CO_DASHBOARDSDESCRIPTION.AsString :=
                 DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.FieldByName('DESCRIPTION').AsString;
  end;
end;

initialization
  RegisterClass(TDM_CO_DASHBOARDS);

finalization
  UnregisterClass(TDM_CO_DASHBOARDS);

end.
