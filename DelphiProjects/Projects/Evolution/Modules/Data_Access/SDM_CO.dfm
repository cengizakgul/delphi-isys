inherited DM_CO: TDM_CO
  OldCreateOrder = True
  Left = 378
  Top = 296
  Height = 479
  Width = 741
  object CO: TCO
    ProviderName = 'CO_PROV'
    PictureMasks.Strings = (
      'ZIP_CODE'#9'*5{#,?}[-*4{#,?}]'#9'T'#9'F'
      'LEGAL_ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      
        'MAXIMUM_HOURS_ON_CHECK'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]}'#9'T'#9'F'
      
        'MAXIMUM_DOLLARS_ON_CHECK'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]}'#9'T'#9'F'
      'NETWORK_ADMINISTRATOR_PHONE'#9'{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'#9'T'#9'F'
      
        'MAXIMUM_GROUP_TERM_LIFE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'AVERAGE_HOURS'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'PAYRATE_PRECISION'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      'FEIN'#9'*9{#}'#9'T'#9'T'
      'STATE'#9'*{&,@}'#9'T'#9'T')
    FieldDefs = <
      item
        Name = 'CO_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'DBA'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ADDRESS1'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ADDRESS2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CITY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'STATE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 2
      end
      item
        Name = 'ZIP_CODE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 10
      end
      item
        Name = 'COUNTY'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'LEGAL_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'LEGAL_ADDRESS1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'LEGAL_ADDRESS2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'LEGAL_CITY'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'LEGAL_STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'LEGAL_ZIP_CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'E_MAIL_ADDRESS'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'REFERRED_BY'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SB_REFERRALS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'START_DATE'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'TERMINATION_DATE'
        DataType = ftDate
      end
      item
        Name = 'TERMINATION_CODE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TERMINATION_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'SB_ACCOUNTANT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'ACCOUNTANT_CONTACT'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PRINT_CPA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'UNION_CL_E_DS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_COMMON_PAYMASTER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_CO_CONSOLIDATION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FEIN'
        Attributes = [faRequired]
        DataType = ftString
        Size = 9
      end
      item
        Name = 'REMOTE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BUSINESS_START_DATE'
        DataType = ftDate
      end
      item
        Name = 'BUSINESS_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CORPORATION_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NATURE_OF_BUSINESS'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'RESTAURANT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DAYS_OPEN'
        DataType = ftInteger
      end
      item
        Name = 'TIME_OPEN'
        DataType = ftDateTime
      end
      item
        Name = 'TIME_CLOSE'
        DataType = ftDateTime
      end
      item
        Name = 'COMPANY_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'SUCCESSOR_COMPANY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MEDICAL_PLAN'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RETIREMENT_AGE'
        DataType = ftInteger
      end
      item
        Name = 'PAY_FREQUENCIES'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GENERAL_LEDGER_FORMAT_STRING'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CL_BILLING_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'DBDT_LEVEL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BILLING_LEVEL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BANK_ACCOUNT_LEVEL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BILLING_SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'DEBIT_NUMBER_DAYS_PRIOR_PR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'TAX_CL_BANK_ACCOUNT_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'DEBIT_NUMBER_OF_DAYS_PRIOR_TAX'
        DataType = ftInteger
      end
      item
        Name = 'BILLING_CL_BANK_ACCOUNT_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'DEBIT_NUMBER_DAYS_PRIOR_BILL'
        DataType = ftInteger
      end
      item
        Name = 'DD_CL_BANK_ACCOUNT_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'DEBIT_NUMBER_OF_DAYS_PRIOR_DD'
        DataType = ftInteger
      end
      item
        Name = 'HOME_CO_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'HOME_SDI_CO_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'HOME_SUI_CO_STATES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_SERVICE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TAX_SERVICE_START_DATE'
        DataType = ftDate
      end
      item
        Name = 'TAX_SERVICE_END_DATE'
        DataType = ftDate
      end
      item
        Name = 'USE_DBA_ON_TAX_RETURN'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'WORKERS_COMP_CL_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'WORKERS_COMP_POLICY_ID'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'W_COMP_CL_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DEBIT_NUMBER_DAYS_PRIOR_WC'
        DataType = ftInteger
      end
      item
        Name = 'W_COMP_SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EFTPS_ENROLLMENT_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EFTPS_ENROLLMENT_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'EFTPS_SEQUENCE_NUMBER'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'EFTPS_ENROLLMENT_NUMBER'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'EFTPS_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PIN_NUMBER'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ACH_SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TRUST_SERVICE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TRUST_SB_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TRUST_SERVICE_START_DATE'
        DataType = ftDate
      end
      item
        Name = 'TRUST_SERVICE_END_DATE'
        DataType = ftDate
      end
      item
        Name = 'OBC'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SB_OBC_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'OBC_START_DATE'
        DataType = ftDate
      end
      item
        Name = 'OBC_END_DATE'
        DataType = ftDate
      end
      item
        Name = 'SY_FED_REPORTING_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FEDERAL_TAX_DEPOSIT_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_TAX_TRANSFER_METHOD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_TAX_PAYMENT_METHOD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FUI_SY_TAX_PAYMENT_AGENCY'
        DataType = ftInteger
      end
      item
        Name = 'FUI_SY_TAX_REPORT_AGENCY'
        DataType = ftInteger
      end
      item
        Name = 'EXTERNAL_TAX_EXPORT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NON_PROFIT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEDERAL_TAX_EXEMPT_STATUS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FED_TAX_EXEMPT_EE_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FED_TAX_EXEMPT_ER_OASDI'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FED_TAX_EXEMPT_EE_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FED_TAX_EXEMPT_ER_MEDICARE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FUI_TAX_DEPOSIT_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FUI_TAX_EXEMPT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FUI_RATE_OVERRIDE'
        DataType = ftFloat
      end
      item
        Name = 'PRIMARY_SORT_FIELD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SECONDARY_SORT_FIELD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FISCAL_YEAR_END'
        DataType = ftInteger
      end
      item
        Name = 'SIC_CODE'
        DataType = ftInteger
      end
      item
        Name = 'DEDUCTIONS_TO_ZERO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AUTOPAY_COMPANY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAY_FREQUENCY_HOURLY_DEFAULT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAY_FREQUENCY_SALARY_DEFAULT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_CHECK_PRIMARY_SORT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_CHECK_SECONDARY_SORT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_DELIVERY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'ANNUAL_CL_DELIVERY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'QUARTER_CL_DELIVERY_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SMOKER_DEFAULT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AUTO_LABOR_DIST_SHOW_DEDUCTS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AUTO_LABOR_DIST_LEVEL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DISTRIBUTE_DEDUCTIONS_DEFAULT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_WORKERS_COMP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'AUTO_INCREMENT'
        DataType = ftFloat
      end
      item
        Name = 'MAXIMUM_GROUP_TERM_LIFE'
        DataType = ftFloat
      end
      item
        Name = 'WITHHOLDING_DEFAULT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'APPLY_MISC_LIMIT_TO_1099'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PAYRATE_PRECISION'
        DataType = ftFloat
      end
      item
        Name = 'AVERAGE_HOURS'
        DataType = ftFloat
      end
      item
        Name = 'CHECK_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MAXIMUM_HOURS_ON_CHECK'
        DataType = ftFloat
      end
      item
        Name = 'MAXIMUM_DOLLARS_ON_CHECK'
        DataType = ftFloat
      end
      item
        Name = 'MAKE_UP_TAX_DEDUCT_SHORTFALLS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_SHIFTS_ON_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_YTD_ON_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_EIN_NUMBER_ON_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_SS_NUMBER_ON_CHECK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TIME_OFF_ACCRUAL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CHECK_FORM'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRINT_MANUAL_CHECK_STUBS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CREDIT_HOLD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_TIMECLOCK_IMPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PAYROLL_PASSWORD'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'REMOTE_OF_CLIENT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'HARDWARE_O_S'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NETWORK'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MODEM_SPEED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MODEM_CONNECTION_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NETWORK_ADMINISTRATOR'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'NETWORK_ADMINISTRATOR_PHONE'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NETWORK_ADMIN_PHONE_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXTERNAL_NETWORK_ADMINISTRATOR'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TRANSMISSION_DESTINATION'
        DataType = ftInteger
      end
      item
        Name = 'LAST_CALL_IN_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SETUP_COMPLETED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FIRST_MONTHLY_PAYROLL_DAY'
        DataType = ftInteger
      end
      item
        Name = 'SECOND_MONTHLY_PAYROLL_DAY'
        DataType = ftInteger
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'COLLATE_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DISCOUNT_RATE'
        DataType = ftFloat
      end
      item
        Name = 'MOD_RATE'
        DataType = ftFloat
      end
      item
        Name = 'PROCESS_PRIORITY'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CHECK_MESSAGE'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOMER_SERVICE_SB_USER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'INVOICE_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'TAX_COVER_LETTER_NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'FINAL_TAX_RETURN'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'BILLING_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FEDERAL_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EE_OASDI_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ER_OASDI_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EE_MEDICARE_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ER_MEDICARE_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FUI_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EIC_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'BACKUP_W_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NET_PAY_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TAX_IMP_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TRUST_IMP_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'THRD_P_TAX_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'THRD_P_CHK_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TRUST_CHK_GENERAL_LEDGER_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FEDERAL_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EE_OASDI_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ER_OASDI_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EE_MEDICARE_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ER_MEDICARE_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FUI_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EIC_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'BACKUP_W_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TRUST_IMP_OFFSET_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'AUTO_ENLIST'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BILLING_EXP_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ER_OASDI_EXP_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ER_MEDICARE_EXP_GL_TAG'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CALCULATE_LOCALS_FIRST'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MINIMUM_TAX_THRESHOLD'
        DataType = ftFloat
      end
      item
        Name = 'LAST_PREPROCESS_MESSAGE'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'LAST_PREPROCESS'
        DataType = ftDateTime
      end
      item
        Name = 'LAST_FUI_CORRECTION'
        DataType = ftDateTime
      end
      item
        Name = 'LAST_QUARTER_END_CORRECTION'
        DataType = ftDateTime
      end
      item
        Name = 'LAST_SUI_CORRECTION'
        DataType = ftDateTime
      end
      item
        Name = 'CHARGE_COBRA_ADMIN_FEE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'COBRA_ELIGIBILITY_CONFIRM_DAYS'
        DataType = ftInteger
      end
      item
        Name = 'COBRA_FEE_DAY_OF_MONTH_DUE'
        DataType = ftInteger
      end
      item
        Name = 'COBRA_NOTIFICATION_DAYS'
        DataType = ftInteger
      end
      item
        Name = 'SS_DISABILITY_ADMIN_FEE'
        DataType = ftFloat
      end
      item
        Name = 'SHOW_RATES_ON_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_DIR_DEP_NBR_ON_CHECKS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FED_943_TAX_DEPOSIT_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FED_945_TAX_DEPOSIT_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'IMPOUND_WORKERS_COMP'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LOCK_DATE'
        DataType = ftDate
      end
      item
        Name = 'REVERSE_CHECK_PRINTING'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CHECK_TIME_OFF_AVAIL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PR_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_REPORT_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_REPORT_SECOND_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_EE_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_RETURN_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_RETURN_SECOND_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME_ON_INVOICE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MISC_CHECK_FORM'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AGENCY_CHECK_MB_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'HOLD_RETURN_QUEUE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMBER_OF_INVOICE_COPIES'
        DataType = ftInteger
      end
      item
        Name = 'PRORATE_FLAT_FEE_FOR_DBDT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BREAK_CHECKS_BY_DBDT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INITIAL_EFFECTIVE_DATE'
        DataType = ftDate
      end
      item
        Name = 'SB_OTHER_SERVICE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SHOW_SHORTFALL_CHECK'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SUMMARIZE_SUI'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SHOW_TIME_CLOCK_PUNCH'
        DataType = ftString
        Size = 2
      end>
    OnNewRecord = CONewRecord
    Left = 52
    Top = 8
    object COCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = '"CO_HIST"."CO_NBR"'
      Required = True
    end
    object COCUSTOM_COMPANY_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
      Origin = '"CO_HIST"."CUSTOM_COMPANY_NUMBER"'
      Required = True
    end
    object CONAME: TStringField
      DisplayWidth = 75
      FieldName = 'NAME'
      Origin = '"CO_HIST"."NAME"'
      Required = True
      Size = 75
    end
    object CODBA: TStringField
      DisplayWidth = 75
      FieldName = 'DBA'
      Origin = '"CO_HIST"."DBA"'
      Size = 75
    end
    object COADDRESS1: TStringField
      DisplayWidth = 35
      FieldName = 'ADDRESS1'
      Origin = '"CO_HIST"."ADDRESS1"'
      Required = True
      Size = 35
    end
    object COADDRESS2: TStringField
      DisplayWidth = 35
      FieldName = 'ADDRESS2'
      Origin = '"CO_HIST"."ADDRESS2"'
      Size = 35
    end
    object COCITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"CO_HIST"."CITY"'
      Required = True
    end
    object COSTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"CO_HIST"."STATE"'
      Required = True
      Size = 2
    end
    object COZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"CO_HIST"."ZIP_CODE"'
      Required = True
      Size = 10
    end
    object COCOUNTY: TStringField
      DisplayWidth = 10
      FieldName = 'COUNTY'
      Origin = '"CO_HIST"."COUNTY"'
      Size = 10
    end
    object COLEGAL_NAME: TStringField
      DisplayWidth = 75
      FieldName = 'LEGAL_NAME'
      Origin = '"CO_HIST"."LEGAL_NAME"'
      Size = 75
    end
    object COLEGAL_ADDRESS1: TStringField
      DisplayWidth = 35
      FieldName = 'LEGAL_ADDRESS1'
      Origin = '"CO_HIST"."LEGAL_ADDRESS1"'
      Size = 35
    end
    object COLEGAL_ADDRESS2: TStringField
      DisplayWidth = 35
      FieldName = 'LEGAL_ADDRESS2'
      Origin = '"CO_HIST"."LEGAL_ADDRESS2"'
      Size = 35
    end
    object COLEGAL_CITY: TStringField
      DisplayWidth = 20
      FieldName = 'LEGAL_CITY'
      Origin = '"CO_HIST"."LEGAL_CITY"'
    end
    object COLEGAL_STATE: TStringField
      DisplayWidth = 2
      FieldName = 'LEGAL_STATE'
      Origin = '"CO_HIST"."LEGAL_STATE"'
      Size = 2
    end
    object COLEGAL_ZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'LEGAL_ZIP_CODE'
      Origin = '"CO_HIST"."LEGAL_ZIP_CODE"'
      Size = 10
    end
    object COE_MAIL_ADDRESS: TStringField
      DisplayWidth = 40
      FieldName = 'E_MAIL_ADDRESS'
      Origin = '"CO_HIST"."E_MAIL_ADDRESS"'
      Size = 80
    end
    object COREFERRED_BY: TStringField
      DisplayWidth = 40
      FieldName = 'REFERRED_BY'
      Origin = '"CO_HIST"."REFERRED_BY"'
      Size = 40
    end
    object COSB_REFERRALS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_REFERRALS_NBR'
      Origin = '"CO_HIST"."SB_REFERRALS_NBR"'
    end
    object COSTART_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'START_DATE'
      Origin = '"CO_HIST"."START_DATE"'
      Required = True
    end
    object COTERMINATION_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'TERMINATION_DATE'
      Origin = '"CO_HIST"."TERMINATION_DATE"'
    end
    object COTERMINATION_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'TERMINATION_CODE'
      Origin = '"CO_HIST"."TERMINATION_CODE"'
      Required = True
      Size = 1
    end
    object COTERMINATION_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'TERMINATION_NOTES'
      Origin = '"CO_HIST"."TERMINATION_NOTES"'
      Size = 8
    end
    object COSB_ACCOUNTANT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_ACCOUNTANT_NBR'
      Origin = '"CO_HIST"."SB_ACCOUNTANT_NBR"'
    end
    object COACCOUNTANT_CONTACT: TStringField
      DisplayWidth = 40
      FieldName = 'ACCOUNTANT_CONTACT'
      Origin = '"CO_HIST"."ACCOUNTANT_CONTACT"'
      Size = 40
    end
    object COPRINT_CPA: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_CPA'
      Origin = '"CO_HIST"."PRINT_CPA"'
      Required = True
      Size = 1
    end
    object COUNION_CL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'UNION_CL_E_DS_NBR'
      Origin = '"CO_HIST"."UNION_CL_E_DS_NBR"'
    end
    object COCL_COMMON_PAYMASTER_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_COMMON_PAYMASTER_NBR'
      Origin = '"CO_HIST"."CL_COMMON_PAYMASTER_NBR"'
    end
    object COCL_CO_CONSOLIDATION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
      Origin = '"CO_HIST"."CL_CO_CONSOLIDATION_NBR"'
    end
    object COFEIN: TStringField
      DisplayWidth = 9
      FieldName = 'FEIN'
      Origin = '"CO_HIST"."FEIN"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 9
    end
    object COREMOTE: TStringField
      DisplayWidth = 1
      FieldName = 'REMOTE'
      Origin = '"CO_HIST"."REMOTE"'
      Required = True
      Size = 1
    end
    object COBUSINESS_START_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'BUSINESS_START_DATE'
      Origin = '"CO_HIST"."BUSINESS_START_DATE"'
    end
    object COBUSINESS_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'BUSINESS_TYPE'
      Origin = '"CO_HIST"."BUSINESS_TYPE"'
      Size = 1
    end
    object COCORPORATION_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'CORPORATION_TYPE'
      Origin = '"CO_HIST"."CORPORATION_TYPE"'
      Required = True
      Size = 1
    end
    object CONATURE_OF_BUSINESS: TBlobField
      DisplayWidth = 10
      FieldName = 'NATURE_OF_BUSINESS'
      Origin = '"CO_HIST"."NATURE_OF_BUSINESS"'
      Size = 8
    end
    object CORESTAURANT: TStringField
      DisplayWidth = 1
      FieldName = 'RESTAURANT'
      Origin = '"CO_HIST"."RESTAURANT"'
      Required = True
      Size = 1
    end
    object CODAYS_OPEN: TIntegerField
      DisplayWidth = 10
      FieldName = 'DAYS_OPEN'
      Origin = '"CO_HIST"."DAYS_OPEN"'
    end
    object COTIME_OPEN: TDateTimeField
      DisplayWidth = 18
      FieldName = 'TIME_OPEN'
      Origin = '"CO_HIST"."TIME_OPEN"'
    end
    object COTIME_CLOSE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'TIME_CLOSE'
      Origin = '"CO_HIST"."TIME_CLOSE"'
    end
    object COCOMPANY_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'COMPANY_NOTES'
      Origin = '"CO_HIST"."COMPANY_NOTES"'
      Size = 8
    end
    object COSUCCESSOR_COMPANY: TStringField
      DisplayWidth = 1
      FieldName = 'SUCCESSOR_COMPANY'
      Origin = '"CO_HIST"."SUCCESSOR_COMPANY"'
      Required = True
      Size = 1
    end
    object COMEDICAL_PLAN: TStringField
      DisplayWidth = 1
      FieldName = 'MEDICAL_PLAN'
      Origin = '"CO_HIST"."MEDICAL_PLAN"'
      Required = True
      Size = 1
    end
    object CORETIREMENT_AGE: TIntegerField
      DisplayWidth = 10
      FieldName = 'RETIREMENT_AGE'
      Origin = '"CO_HIST"."RETIREMENT_AGE"'
    end
    object COPAY_FREQUENCIES: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCIES'
      Origin = '"CO_HIST"."PAY_FREQUENCIES"'
      Size = 1
    end
    object COGENERAL_LEDGER_FORMAT_STRING: TStringField
      DisplayWidth = 30
      FieldName = 'GENERAL_LEDGER_FORMAT_STRING'
      Origin = '"CO_HIST"."GENERAL_LEDGER_FORMAT_STRING"'
      Size = 30
    end
    object COCL_BILLING_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_BILLING_NBR'
      Origin = '"CO_HIST"."CL_BILLING_NBR"'
      Required = True
    end
    object CODBDT_LEVEL: TStringField
      DisplayWidth = 1
      FieldName = 'DBDT_LEVEL'
      Origin = '"CO_HIST"."DBDT_LEVEL"'
      Required = True
      Size = 1
    end
    object COBILLING_LEVEL: TStringField
      DisplayWidth = 1
      FieldName = 'BILLING_LEVEL'
      Origin = '"CO_HIST"."BILLING_LEVEL"'
      Required = True
      Size = 1
    end
    object COBANK_ACCOUNT_LEVEL: TStringField
      DisplayWidth = 1
      FieldName = 'BANK_ACCOUNT_LEVEL'
      Origin = '"CO_HIST"."BANK_ACCOUNT_LEVEL"'
      Required = True
      Size = 1
    end
    object COBILLING_SB_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BILLING_SB_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."BILLING_SB_BANK_ACCOUNT_NBR"'
    end
    object COTAX_SB_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_SB_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."TAX_SB_BANK_ACCOUNT_NBR"'
    end
    object COPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."PAYROLL_CL_BANK_ACCOUNT_NBR"'
      Required = True
      OnChange = COAnyBank_Account_NbrChange
    end
    object CODEBIT_NUMBER_DAYS_PRIOR_PR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEBIT_NUMBER_DAYS_PRIOR_PR'
      Origin = '"CO_HIST"."DEBIT_NUMBER_DAYS_PRIOR_PR"'
      Required = True
      OnChange = CODEBIT_NUMBER_DAYS_PRIOR_PRChange
    end
    object COTAX_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."TAX_CL_BANK_ACCOUNT_NBR"'
      Required = True
      OnChange = COAnyBank_Account_NbrChange
    end
    object CODEBIT_NUMBER_OF_DAYS_PRIOR_TAX: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEBIT_NUMBER_OF_DAYS_PRIOR_TAX'
      Origin = '"CO_HIST"."DEBIT_NUMBER_OF_DAYS_PRIOR_TAX"'
    end
    object COBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'BILLING_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."BILLING_CL_BANK_ACCOUNT_NBR"'
      Required = True
      OnChange = COAnyBank_Account_NbrChange
    end
    object CODEBIT_NUMBER_DAYS_PRIOR_BILL: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEBIT_NUMBER_DAYS_PRIOR_BILL'
      Origin = '"CO_HIST"."DEBIT_NUMBER_DAYS_PRIOR_BILL"'
    end
    object CODD_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'DD_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."DD_CL_BANK_ACCOUNT_NBR"'
      Required = True
      OnChange = COAnyBank_Account_NbrChange
    end
    object CODEBIT_NUMBER_OF_DAYS_PRIOR_DD: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEBIT_NUMBER_OF_DAYS_PRIOR_DD'
      Origin = '"CO_HIST"."DEBIT_NUMBER_OF_DAYS_PRIOR_DD"'
    end
    object COHOME_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_CO_STATES_NBR'
      Origin = '"CO_HIST"."HOME_CO_STATES_NBR"'
    end
    object COHOME_SDI_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_SDI_CO_STATES_NBR'
      Origin = '"CO_HIST"."HOME_SDI_CO_STATES_NBR"'
    end
    object COHOME_SUI_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'HOME_SUI_CO_STATES_NBR'
      Origin = '"CO_HIST"."HOME_SUI_CO_STATES_NBR"'
    end
    object COTAX_SERVICE: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      Origin = '"CO_HIST"."TAX_SERVICE"'
      Required = True
      Size = 1
    end
    object COTAX_SERVICE_START_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'TAX_SERVICE_START_DATE'
      Origin = '"CO_HIST"."TAX_SERVICE_START_DATE"'
    end
    object COTAX_SERVICE_END_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'TAX_SERVICE_END_DATE'
      Origin = '"CO_HIST"."TAX_SERVICE_END_DATE"'
    end
    object COUSE_DBA_ON_TAX_RETURN: TStringField
      DisplayWidth = 1
      FieldName = 'USE_DBA_ON_TAX_RETURN'
      Origin = '"CO_HIST"."USE_DBA_ON_TAX_RETURN"'
      Required = True
      Size = 1
    end
    object COWORKERS_COMP_CL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'WORKERS_COMP_CL_AGENCY_NBR'
      Origin = '"CO_HIST"."WORKERS_COMP_CL_AGENCY_NBR"'
    end
    object COWORKERS_COMP_POLICY_ID: TStringField
      DisplayWidth = 40
      FieldName = 'WORKERS_COMP_POLICY_ID'
      Origin = '"CO_HIST"."WORKERS_COMP_POLICY_ID"'
      Size = 40
    end
    object COW_COMP_CL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'W_COMP_CL_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."W_COMP_CL_BANK_ACCOUNT_NBR"'
      OnChange = COAnyBank_Account_NbrChange
    end
    object CODEBIT_NUMBER_DAYS_PRIOR_WC: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEBIT_NUMBER_DAYS_PRIOR_WC'
      Origin = '"CO_HIST"."DEBIT_NUMBER_DAYS_PRIOR_WC"'
    end
    object COW_COMP_SB_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'W_COMP_SB_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."W_COMP_SB_BANK_ACCOUNT_NBR"'
    end
    object COEFTPS_ENROLLMENT_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'EFTPS_ENROLLMENT_STATUS'
      Origin = '"CO_HIST"."EFTPS_ENROLLMENT_STATUS"'
      Size = 1
    end
    object COEFTPS_ENROLLMENT_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'EFTPS_ENROLLMENT_DATE'
      Origin = '"CO_HIST"."EFTPS_ENROLLMENT_DATE"'
    end
    object COEFTPS_SEQUENCE_NUMBER: TStringField
      DisplayWidth = 4
      FieldName = 'EFTPS_SEQUENCE_NUMBER'
      Origin = '"CO_HIST"."EFTPS_SEQUENCE_NUMBER"'
      Size = 4
    end
    object COEFTPS_ENROLLMENT_NUMBER: TStringField
      DisplayWidth = 4
      FieldName = 'EFTPS_ENROLLMENT_NUMBER'
      Origin = '"CO_HIST"."EFTPS_ENROLLMENT_NUMBER"'
      Size = 4
    end
    object COEFTPS_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'EFTPS_NAME'
      Origin = '"CO_HIST"."EFTPS_NAME"'
      Size = 40
    end
    object COPIN_NUMBER: TStringField
      DisplayWidth = 10
      FieldName = 'PIN_NUMBER'
      Origin = '"CO_HIST"."PIN_NUMBER"'
      Size = 10
    end
    object COACH_SB_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'ACH_SB_BANK_ACCOUNT_NBR'
      Origin = '"CO_HIST"."ACH_SB_BANK_ACCOUNT_NBR"'
    end
    object COTRUST_SERVICE: TStringField
      DisplayWidth = 1
      FieldName = 'TRUST_SERVICE'
      Origin = '"CO_HIST"."TRUST_SERVICE"'
      Required = True
      Size = 1
    end
    object COTRUST_SB_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TRUST_SB_ACCOUNT_NBR'
      Origin = '"CO_HIST"."TRUST_SB_ACCOUNT_NBR"'
    end
    object COTRUST_SERVICE_START_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'TRUST_SERVICE_START_DATE'
      Origin = '"CO_HIST"."TRUST_SERVICE_START_DATE"'
    end
    object COTRUST_SERVICE_END_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'TRUST_SERVICE_END_DATE'
      Origin = '"CO_HIST"."TRUST_SERVICE_END_DATE"'
    end
    object COOBC: TStringField
      DisplayWidth = 1
      FieldName = 'OBC'
      Origin = '"CO_HIST"."OBC"'
      Required = True
      Size = 1
    end
    object COSB_OBC_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_OBC_ACCOUNT_NBR'
      Origin = '"CO_HIST"."SB_OBC_ACCOUNT_NBR"'
    end
    object COOBC_START_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'OBC_START_DATE'
      Origin = '"CO_HIST"."OBC_START_DATE"'
    end
    object COOBC_END_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'OBC_END_DATE'
      Origin = '"CO_HIST"."OBC_END_DATE"'
    end
    object COSY_FED_REPORTING_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_FED_REPORTING_AGENCY_NBR'
      Origin = '"CO_HIST"."SY_FED_REPORTING_AGENCY_NBR"'
      OnChange = COSY_FED_REPORTING_AGENCY_NBRChange
    end
    object COSY_FED_TAX_PAYMENT_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
      Origin = '"CO_HIST"."SY_FED_TAX_PAYMENT_AGENCY_NBR"'
      OnChange = COSY_FED_TAX_PAYMENT_AGENCY_NBRChange
    end
    object COFEDERAL_TAX_DEPOSIT_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'FEDERAL_TAX_DEPOSIT_FREQUENCY'
      Origin = '"CO_HIST"."FEDERAL_TAX_DEPOSIT_FREQUENCY"'
      Required = True
      Size = 1
    end
    object COFEDERAL_TAX_TRANSFER_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'FEDERAL_TAX_TRANSFER_METHOD'
      Origin = '"CO_HIST"."FEDERAL_TAX_TRANSFER_METHOD"'
      Required = True
      Size = 1
    end
    object COFEDERAL_TAX_PAYMENT_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'FEDERAL_TAX_PAYMENT_METHOD'
      Origin = '"CO_HIST"."FEDERAL_TAX_PAYMENT_METHOD"'
      Required = True
      Size = 1
    end
    object COFUI_SY_TAX_PAYMENT_AGENCY: TIntegerField
      DisplayWidth = 10
      FieldName = 'FUI_SY_TAX_PAYMENT_AGENCY'
      Origin = '"CO_HIST"."FUI_SY_TAX_PAYMENT_AGENCY"'
    end
    object COFUI_SY_TAX_REPORT_AGENCY: TIntegerField
      DisplayWidth = 10
      FieldName = 'FUI_SY_TAX_REPORT_AGENCY'
      Origin = '"CO_HIST"."FUI_SY_TAX_REPORT_AGENCY"'
    end
    object COEXTERNAL_TAX_EXPORT: TStringField
      DisplayWidth = 1
      FieldName = 'EXTERNAL_TAX_EXPORT'
      Origin = '"CO_HIST"."EXTERNAL_TAX_EXPORT"'
      Required = True
      Size = 1
    end
    object CONON_PROFIT: TStringField
      DisplayWidth = 1
      FieldName = 'NON_PROFIT'
      Origin = '"CO_HIST"."NON_PROFIT"'
      Required = True
      Size = 1
    end
    object COFEDERAL_TAX_EXEMPT_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'FEDERAL_TAX_EXEMPT_STATUS'
      Origin = '"CO_HIST"."FEDERAL_TAX_EXEMPT_STATUS"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 1
    end
    object COFED_TAX_EXEMPT_EE_OASDI: TStringField
      DisplayWidth = 1
      FieldName = 'FED_TAX_EXEMPT_EE_OASDI'
      Origin = '"CO_HIST"."FED_TAX_EXEMPT_EE_OASDI"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 1
    end
    object COFED_TAX_EXEMPT_ER_OASDI: TStringField
      DisplayWidth = 1
      FieldName = 'FED_TAX_EXEMPT_ER_OASDI'
      Origin = '"CO_HIST"."FED_TAX_EXEMPT_ER_OASDI"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 1
    end
    object COFED_TAX_EXEMPT_EE_MEDICARE: TStringField
      DisplayWidth = 1
      FieldName = 'FED_TAX_EXEMPT_EE_MEDICARE'
      Origin = '"CO_HIST"."FED_TAX_EXEMPT_EE_MEDICARE"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 1
    end
    object COFED_TAX_EXEMPT_ER_MEDICARE: TStringField
      DisplayWidth = 1
      FieldName = 'FED_TAX_EXEMPT_ER_MEDICARE'
      Origin = '"CO_HIST"."FED_TAX_EXEMPT_ER_MEDICARE"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 1
    end
    object COFUI_TAX_DEPOSIT_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'FUI_TAX_DEPOSIT_FREQUENCY'
      Origin = '"CO_HIST"."FUI_TAX_DEPOSIT_FREQUENCY"'
      Required = True
      Size = 1
    end
    object COFUI_TAX_EXEMPT: TStringField
      DisplayWidth = 1
      FieldName = 'FUI_TAX_EXEMPT'
      Origin = '"CO_HIST"."FUI_TAX_EXEMPT"'
      Required = True
      OnValidate = COFEDERAL_TAX_EXEMPT_STATUSValidate
      Size = 1
    end
    object COFUI_RATE_OVERRIDE: TFloatField
      DisplayWidth = 10
      FieldName = 'FUI_RATE_OVERRIDE'
      Origin = '"CO_HIST"."FUI_RATE_OVERRIDE"'
      DisplayFormat = '#,##0.00##'
    end
    object COPRIMARY_SORT_FIELD: TStringField
      DisplayWidth = 1
      FieldName = 'PRIMARY_SORT_FIELD'
      Origin = '"CO_HIST"."PRIMARY_SORT_FIELD"'
      Required = True
      Size = 1
    end
    object COSECONDARY_SORT_FIELD: TStringField
      DisplayWidth = 1
      FieldName = 'SECONDARY_SORT_FIELD'
      Origin = '"CO_HIST"."SECONDARY_SORT_FIELD"'
      Required = True
      Size = 1
    end
    object COFISCAL_YEAR_END: TIntegerField
      DisplayWidth = 10
      FieldName = 'FISCAL_YEAR_END'
      Origin = '"CO_HIST"."FISCAL_YEAR_END"'
      DisplayFormat = '00/00'
    end
    object COSIC_CODE: TIntegerField
      DisplayWidth = 10
      FieldName = 'SIC_CODE'
      Origin = '"CO_HIST"."SIC_CODE"'
    end
    object CODEDUCTIONS_TO_ZERO: TStringField
      DisplayWidth = 1
      FieldName = 'DEDUCTIONS_TO_ZERO'
      Origin = '"CO_HIST"."DEDUCTIONS_TO_ZERO"'
      Required = True
      Size = 1
    end
    object COAUTOPAY_COMPANY: TStringField
      DisplayWidth = 1
      FieldName = 'AUTOPAY_COMPANY'
      Origin = '"CO_HIST"."AUTOPAY_COMPANY"'
      Required = True
      Size = 1
    end
    object COPAY_FREQUENCY_HOURLY_DEFAULT: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_HOURLY_DEFAULT'
      Origin = '"CO_HIST"."PAY_FREQUENCY_HOURLY_DEFAULT"'
      Required = True
      Size = 1
    end
    object COPAY_FREQUENCY_SALARY_DEFAULT: TStringField
      DisplayWidth = 1
      FieldName = 'PAY_FREQUENCY_SALARY_DEFAULT'
      Origin = '"CO_HIST"."PAY_FREQUENCY_SALARY_DEFAULT"'
      Required = True
      Size = 1
    end
    object COCO_CHECK_PRIMARY_SORT: TStringField
      DisplayWidth = 1
      FieldName = 'CO_CHECK_PRIMARY_SORT'
      Origin = '"CO_HIST"."CO_CHECK_PRIMARY_SORT"'
      Required = True
      Size = 1
    end
    object COCO_CHECK_SECONDARY_SORT: TStringField
      DisplayWidth = 1
      FieldName = 'CO_CHECK_SECONDARY_SORT'
      Origin = '"CO_HIST"."CO_CHECK_SECONDARY_SORT"'
      Required = True
      Size = 1
    end
    object COCL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_HIST"."CL_DELIVERY_GROUP_NBR"'
    end
    object COANNUAL_CL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'ANNUAL_CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_HIST"."ANNUAL_CL_DELIVERY_GROUP_NBR"'
    end
    object COQUARTER_CL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'QUARTER_CL_DELIVERY_GROUP_NBR'
      Origin = '"CO_HIST"."QUARTER_CL_DELIVERY_GROUP_NBR"'
    end
    object COSMOKER_DEFAULT: TStringField
      DisplayWidth = 1
      FieldName = 'SMOKER_DEFAULT'
      Origin = '"CO_HIST"."SMOKER_DEFAULT"'
      Required = True
      Size = 1
    end
    object COAUTO_LABOR_DIST_SHOW_DEDUCTS: TStringField
      DisplayWidth = 1
      FieldName = 'AUTO_LABOR_DIST_SHOW_DEDUCTS'
      Origin = '"CO_HIST"."AUTO_LABOR_DIST_SHOW_DEDUCTS"'
      Required = True
      Size = 1
    end
    object COAUTO_LABOR_DIST_LEVEL: TStringField
      DisplayWidth = 1
      FieldName = 'AUTO_LABOR_DIST_LEVEL'
      Origin = '"CO_HIST"."AUTO_LABOR_DIST_LEVEL"'
      Required = True
      Size = 1
    end
    object CODISTRIBUTE_DEDUCTIONS_DEFAULT: TStringField
      DisplayWidth = 1
      FieldName = 'DISTRIBUTE_DEDUCTIONS_DEFAULT'
      Origin = '"CO_HIST"."DISTRIBUTE_DEDUCTIONS_DEFAULT"'
      Required = True
      Size = 1
    end
    object COCO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"CO_HIST"."CO_WORKERS_COMP_NBR"'
    end
    object COAUTO_INCREMENT: TFloatField
      DisplayWidth = 10
      FieldName = 'AUTO_INCREMENT'
      Origin = '"CO_HIST"."AUTO_INCREMENT"'
      DisplayFormat = '#,##0.00'
    end
    object COMAXIMUM_GROUP_TERM_LIFE: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_GROUP_TERM_LIFE'
      Origin = '"CO_HIST"."MAXIMUM_GROUP_TERM_LIFE"'
      DisplayFormat = '#,##0.00'
    end
    object COWITHHOLDING_DEFAULT: TStringField
      DisplayWidth = 1
      FieldName = 'WITHHOLDING_DEFAULT'
      Origin = '"CO_HIST"."WITHHOLDING_DEFAULT"'
      Required = True
      Size = 1
    end
    object COAPPLY_MISC_LIMIT_TO_1099: TStringField
      DisplayWidth = 1
      FieldName = 'APPLY_MISC_LIMIT_TO_1099'
      Origin = '"CO_HIST"."APPLY_MISC_LIMIT_TO_1099"'
      Required = True
      Size = 1
    end
    object COPAYRATE_PRECISION: TFloatField
      DisplayWidth = 10
      FieldName = 'PAYRATE_PRECISION'
      Origin = '"CO_HIST"."PAYRATE_PRECISION"'
      DisplayFormat = '#,##0.00'
    end
    object COAVERAGE_HOURS: TFloatField
      DisplayWidth = 10
      FieldName = 'AVERAGE_HOURS'
      Origin = '"CO_HIST"."AVERAGE_HOURS"'
      DisplayFormat = '#,##0.00'
    end
    object COCHECK_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'CHECK_TYPE'
      Origin = '"CO_HIST"."CHECK_TYPE"'
      Required = True
      Size = 1
    end
    object COMAXIMUM_HOURS_ON_CHECK: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_HOURS_ON_CHECK'
      Origin = '"CO_HIST"."MAXIMUM_HOURS_ON_CHECK"'
      DisplayFormat = '#,##0.00'
    end
    object COMAXIMUM_DOLLARS_ON_CHECK: TFloatField
      DisplayWidth = 10
      FieldName = 'MAXIMUM_DOLLARS_ON_CHECK'
      Origin = '"CO_HIST"."MAXIMUM_DOLLARS_ON_CHECK"'
      DisplayFormat = '#,##0.00'
    end
    object COMAKE_UP_TAX_DEDUCT_SHORTFALLS: TStringField
      DisplayWidth = 1
      FieldName = 'MAKE_UP_TAX_DEDUCT_SHORTFALLS'
      Origin = '"CO_HIST"."MAKE_UP_TAX_DEDUCT_SHORTFALLS"'
      Required = True
      Size = 1
    end
    object COSHOW_SHIFTS_ON_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_SHIFTS_ON_CHECK'
      Origin = '"CO_HIST"."SHOW_SHIFTS_ON_CHECK"'
      Required = True
      Size = 1
    end
    object COSHOW_YTD_ON_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_YTD_ON_CHECK'
      Origin = '"CO_HIST"."SHOW_YTD_ON_CHECK"'
      Required = True
      Size = 1
    end
    object COSHOW_EIN_NUMBER_ON_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_EIN_NUMBER_ON_CHECK'
      Origin = '"CO_HIST"."SHOW_EIN_NUMBER_ON_CHECK"'
      Required = True
      Size = 1
    end
    object COSHOW_SS_NUMBER_ON_CHECK: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_SS_NUMBER_ON_CHECK'
      Origin = '"CO_HIST"."SHOW_SS_NUMBER_ON_CHECK"'
      Required = True
      Size = 1
    end
    object COTIME_OFF_ACCRUAL: TStringField
      DisplayWidth = 1
      FieldName = 'TIME_OFF_ACCRUAL'
      Origin = '"CO_HIST"."TIME_OFF_ACCRUAL"'
      Required = True
      Size = 1
    end
    object COCHECK_FORM: TStringField
      DisplayWidth = 1
      FieldName = 'CHECK_FORM'
      Origin = '"CO_HIST"."CHECK_FORM"'
      Required = True
      Size = 1
    end
    object COPRINT_MANUAL_CHECK_STUBS: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_MANUAL_CHECK_STUBS'
      Origin = '"CO_HIST"."PRINT_MANUAL_CHECK_STUBS"'
      Required = True
      Size = 1
    end
    object COCREDIT_HOLD: TStringField
      DisplayWidth = 1
      FieldName = 'CREDIT_HOLD'
      Origin = '"CO_HIST"."CREDIT_HOLD"'
      Required = True
      Size = 1
    end
    object COCL_TIMECLOCK_IMPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_TIMECLOCK_IMPORTS_NBR'
      Origin = '"CO_HIST"."CL_TIMECLOCK_IMPORTS_NBR"'
    end
    object COPAYROLL_PASSWORD: TStringField
      DisplayWidth = 10
      FieldName = 'PAYROLL_PASSWORD'
      Origin = '"CO_HIST"."PAYROLL_PASSWORD"'
      Size = 10
    end
    object COREMOTE_OF_CLIENT: TStringField
      DisplayWidth = 1
      FieldName = 'REMOTE_OF_CLIENT'
      Origin = '"CO_HIST"."REMOTE_OF_CLIENT"'
      Required = True
      Size = 1
    end
    object COHARDWARE_O_S: TStringField
      DisplayWidth = 1
      FieldName = 'HARDWARE_O_S'
      Origin = '"CO_HIST"."HARDWARE_O_S"'
      Required = True
      Size = 1
    end
    object CONETWORK: TStringField
      DisplayWidth = 1
      FieldName = 'NETWORK'
      Origin = '"CO_HIST"."NETWORK"'
      Required = True
      Size = 1
    end
    object COMODEM_SPEED: TStringField
      DisplayWidth = 1
      FieldName = 'MODEM_SPEED'
      Origin = '"CO_HIST"."MODEM_SPEED"'
      Required = True
      Size = 1
    end
    object COMODEM_CONNECTION_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'MODEM_CONNECTION_TYPE'
      Origin = '"CO_HIST"."MODEM_CONNECTION_TYPE"'
      Required = True
      Size = 1
    end
    object CONETWORK_ADMINISTRATOR: TStringField
      DisplayWidth = 40
      FieldName = 'NETWORK_ADMINISTRATOR'
      Origin = '"CO_HIST"."NETWORK_ADMINISTRATOR"'
      Size = 40
    end
    object CONETWORK_ADMINISTRATOR_PHONE: TStringField
      DisplayWidth = 20
      FieldName = 'NETWORK_ADMINISTRATOR_PHONE'
      Origin = '"CO_HIST"."NETWORK_ADMINISTRATOR_PHONE"'
    end
    object CONETWORK_ADMIN_PHONE_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'NETWORK_ADMIN_PHONE_TYPE'
      Origin = '"CO_HIST"."NETWORK_ADMIN_PHONE_TYPE"'
      Required = True
      Size = 1
    end
    object COEXTERNAL_NETWORK_ADMINISTRATOR: TStringField
      DisplayWidth = 1
      FieldName = 'EXTERNAL_NETWORK_ADMINISTRATOR'
      Origin = '"CO_HIST"."EXTERNAL_NETWORK_ADMINISTRATOR"'
      Required = True
      Size = 1
    end
    object COTRANSMISSION_DESTINATION: TIntegerField
      DisplayWidth = 10
      FieldName = 'TRANSMISSION_DESTINATION'
      Origin = '"CO_HIST"."TRANSMISSION_DESTINATION"'
    end
    object COLAST_CALL_IN_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'LAST_CALL_IN_DATE'
      Origin = '"CO_HIST"."LAST_CALL_IN_DATE"'
    end
    object COSETUP_COMPLETED: TStringField
      DisplayWidth = 1
      FieldName = 'SETUP_COMPLETED'
      Origin = '"CO_HIST"."SETUP_COMPLETED"'
      Required = True
      Size = 1
    end
    object COFIRST_MONTHLY_PAYROLL_DAY: TIntegerField
      DisplayWidth = 10
      FieldName = 'FIRST_MONTHLY_PAYROLL_DAY'
      Origin = '"CO_HIST"."FIRST_MONTHLY_PAYROLL_DAY"'
    end
    object COSECOND_MONTHLY_PAYROLL_DAY: TIntegerField
      DisplayWidth = 10
      FieldName = 'SECOND_MONTHLY_PAYROLL_DAY'
      Origin = '"CO_HIST"."SECOND_MONTHLY_PAYROLL_DAY"'
    end
    object COFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Origin = '"CO_HIST"."FILLER"'
      Size = 512
    end
    object COCOLLATE_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'COLLATE_CHECKS'
      Origin = '"CO_HIST"."COLLATE_CHECKS"'
      Required = True
      Size = 1
    end
    object CODISCOUNT_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'DISCOUNT_RATE'
      Origin = '"CO_HIST"."DISCOUNT_RATE"'
      DisplayFormat = '#,##0.00'
    end
    object COMOD_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'MOD_RATE'
      Origin = '"CO_HIST"."MOD_RATE"'
      DisplayFormat = '#,##0.00'
    end
    object COPROCESS_PRIORITY: TIntegerField
      DisplayWidth = 10
      FieldName = 'PROCESS_PRIORITY'
      Origin = '"CO_HIST"."PROCESS_PRIORITY"'
      Required = True
    end
    object COCHECK_MESSAGE: TStringField
      DisplayWidth = 40
      FieldName = 'CHECK_MESSAGE'
      Origin = '"CO_HIST"."CHECK_MESSAGE"'
      Size = 40
    end
    object COCUSTOMER_SERVICE_SB_USER_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CUSTOMER_SERVICE_SB_USER_NBR'
      Origin = '"CO_HIST"."CUSTOMER_SERVICE_SB_USER_NBR"'
    end
    object COINVOICE_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'INVOICE_NOTES'
      Origin = '"CO_HIST"."INVOICE_NOTES"'
      Size = 8
    end
    object COTAX_COVER_LETTER_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'TAX_COVER_LETTER_NOTES'
      Origin = '"CO_HIST"."TAX_COVER_LETTER_NOTES"'
      Size = 8
    end
    object COFINAL_TAX_RETURN: TStringField
      DisplayWidth = 1
      FieldName = 'FINAL_TAX_RETURN'
      Origin = '"CO_HIST"."FINAL_TAX_RETURN"'
      Required = True
      Size = 1
    end
    object COGENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."GENERAL_LEDGER_TAG"'
    end
    object COBILLING_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'BILLING_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."BILLING_GENERAL_LEDGER_TAG"'
    end
    object COFEDERAL_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'FEDERAL_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."FEDERAL_GENERAL_LEDGER_TAG"'
    end
    object COEE_OASDI_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EE_OASDI_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."EE_OASDI_GENERAL_LEDGER_TAG"'
    end
    object COER_OASDI_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_OASDI_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."ER_OASDI_GENERAL_LEDGER_TAG"'
    end
    object COEE_MEDICARE_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EE_MEDICARE_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."EE_MEDICARE_GENERAL_LEDGER_TAG"'
    end
    object COER_MEDICARE_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_MEDICARE_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."ER_MEDICARE_GENERAL_LEDGER_TAG"'
    end
    object COFUI_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'FUI_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."FUI_GENERAL_LEDGER_TAG"'
    end
    object COEIC_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EIC_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."EIC_GENERAL_LEDGER_TAG"'
    end
    object COBACKUP_W_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'BACKUP_W_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."BACKUP_W_GENERAL_LEDGER_TAG"'
    end
    object CONET_PAY_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'NET_PAY_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."NET_PAY_GENERAL_LEDGER_TAG"'
    end
    object COTAX_IMP_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'TAX_IMP_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."TAX_IMP_GENERAL_LEDGER_TAG"'
    end
    object COTRUST_IMP_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'TRUST_IMP_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."TRUST_IMP_GENERAL_LEDGER_TAG"'
    end
    object COTHRD_P_TAX_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'THRD_P_TAX_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."THRD_P_TAX_GENERAL_LEDGER_TAG"'
    end
    object COTHRD_P_CHK_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'THRD_P_CHK_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."THRD_P_CHK_GENERAL_LEDGER_TAG"'
    end
    object COTRUST_CHK_GENERAL_LEDGER_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'TRUST_CHK_GENERAL_LEDGER_TAG'
      Origin = '"CO_HIST"."TRUST_CHK_GENERAL_LEDGER_TAG"'
    end
    object COFEDERAL_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'FEDERAL_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."FEDERAL_OFFSET_GL_TAG"'
    end
    object COEE_OASDI_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EE_OASDI_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."EE_OASDI_OFFSET_GL_TAG"'
    end
    object COER_OASDI_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_OASDI_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."ER_OASDI_OFFSET_GL_TAG"'
    end
    object COEE_MEDICARE_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EE_MEDICARE_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."EE_MEDICARE_OFFSET_GL_TAG"'
    end
    object COER_MEDICARE_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_MEDICARE_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."ER_MEDICARE_OFFSET_GL_TAG"'
    end
    object COFUI_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'FUI_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."FUI_OFFSET_GL_TAG"'
    end
    object COEIC_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'EIC_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."EIC_OFFSET_GL_TAG"'
    end
    object COBACKUP_W_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'BACKUP_W_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."BACKUP_W_OFFSET_GL_TAG"'
    end
    object COTRUST_IMP_OFFSET_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'TRUST_IMP_OFFSET_GL_TAG'
      Origin = '"CO_HIST"."TRUST_IMP_OFFSET_GL_TAG"'
    end
    object COAUTO_ENLIST: TStringField
      DisplayWidth = 1
      FieldName = 'AUTO_ENLIST'
      Origin = '"CO_HIST"."AUTO_ENLIST"'
      Required = True
      Size = 1
    end
    object COBILLING_EXP_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'BILLING_EXP_GL_TAG'
      Origin = '"CO_HIST"."BILLING_EXP_GL_TAG"'
    end
    object COER_OASDI_EXP_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_OASDI_EXP_GL_TAG'
      Origin = '"CO_HIST"."ER_OASDI_EXP_GL_TAG"'
    end
    object COER_MEDICARE_EXP_GL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'ER_MEDICARE_EXP_GL_TAG'
      Origin = '"CO_HIST"."ER_MEDICARE_EXP_GL_TAG"'
    end
    object COCALCULATE_LOCALS_FIRST: TStringField
      DisplayWidth = 1
      FieldName = 'CALCULATE_LOCALS_FIRST'
      Origin = '"CO_HIST"."CALCULATE_LOCALS_FIRST"'
      Required = True
      Size = 1
    end
    object COMINIMUM_TAX_THRESHOLD: TFloatField
      DisplayWidth = 10
      FieldName = 'MINIMUM_TAX_THRESHOLD'
      Origin = '"CO_HIST"."MINIMUM_TAX_THRESHOLD"'
      DisplayFormat = '#,##0.00'
    end
    object COLAST_PREPROCESS_MESSAGE: TStringField
      DisplayWidth = 80
      FieldName = 'LAST_PREPROCESS_MESSAGE'
      Origin = '"CO_HIST"."LAST_PREPROCESS_MESSAGE"'
      Size = 80
    end
    object COLAST_PREPROCESS: TDateTimeField
      DisplayWidth = 18
      FieldName = 'LAST_PREPROCESS'
      Origin = '"CO_HIST"."LAST_PREPROCESS"'
    end
    object COLAST_FUI_CORRECTION: TDateTimeField
      DisplayWidth = 18
      FieldName = 'LAST_FUI_CORRECTION'
      Origin = '"CO_HIST"."LAST_FUI_CORRECTION"'
    end
    object COLAST_QUARTER_END_CORRECTION: TDateTimeField
      DisplayWidth = 18
      FieldName = 'LAST_QUARTER_END_CORRECTION'
      Origin = '"CO_HIST"."LAST_QUARTER_END_CORRECTION"'
    end
    object COLAST_SUI_CORRECTION: TDateTimeField
      DisplayWidth = 18
      FieldName = 'LAST_SUI_CORRECTION'
      Origin = '"CO_HIST"."LAST_SUI_CORRECTION"'
    end
    object COCHARGE_COBRA_ADMIN_FEE: TStringField
      DisplayWidth = 1
      FieldName = 'CHARGE_COBRA_ADMIN_FEE'
      Origin = '"CO_HIST"."CHARGE_COBRA_ADMIN_FEE"'
      Required = True
      Size = 1
    end
    object COCOBRA_ELIGIBILITY_CONFIRM_DAYS: TIntegerField
      DisplayWidth = 10
      FieldName = 'COBRA_ELIGIBILITY_CONFIRM_DAYS'
      Origin = '"CO_HIST"."COBRA_ELIGIBILITY_CONFIRM_DAYS"'
    end
    object COCOBRA_FEE_DAY_OF_MONTH_DUE: TIntegerField
      DisplayWidth = 10
      FieldName = 'COBRA_FEE_DAY_OF_MONTH_DUE'
      Origin = '"CO_HIST"."COBRA_FEE_DAY_OF_MONTH_DUE"'
    end
    object COCOBRA_NOTIFICATION_DAYS: TIntegerField
      DisplayWidth = 10
      FieldName = 'COBRA_NOTIFICATION_DAYS'
      Origin = '"CO_HIST"."COBRA_NOTIFICATION_DAYS"'
    end
    object COSS_DISABILITY_ADMIN_FEE: TFloatField
      DisplayWidth = 10
      FieldName = 'SS_DISABILITY_ADMIN_FEE'
      Origin = '"CO_HIST"."SS_DISABILITY_ADMIN_FEE"'
      DisplayFormat = '#,##0.00'
    end
    object COSHOW_RATES_ON_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_RATES_ON_CHECKS'
      Origin = '"CO_HIST"."SHOW_RATES_ON_CHECKS"'
      Required = True
      Size = 1
    end
    object COSHOW_DIR_DEP_NBR_ON_CHECKS: TStringField
      DisplayWidth = 1
      FieldName = 'SHOW_DIR_DEP_NBR_ON_CHECKS'
      Origin = '"CO_HIST"."SHOW_DIR_DEP_NBR_ON_CHECKS"'
      Required = True
      Size = 1
    end
    object COFED_943_TAX_DEPOSIT_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'FED_943_TAX_DEPOSIT_FREQUENCY'
      Origin = '"CO_HIST"."FED_943_TAX_DEPOSIT_FREQUENCY"'
      Required = True
      Size = 1
    end
    object COFED_945_TAX_DEPOSIT_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'FED_945_TAX_DEPOSIT_FREQUENCY'
      Origin = '"CO_HIST"."FED_945_TAX_DEPOSIT_FREQUENCY"'
      Required = True
      Size = 1
    end
    object COIMPOUND_WORKERS_COMP: TStringField
      DisplayWidth = 1
      FieldName = 'IMPOUND_WORKERS_COMP'
      Origin = '"CO_HIST"."IMPOUND_WORKERS_COMP"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object COLOCK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'LOCK_DATE'
      Origin = '"CO_HIST"."LOCK_DATE"'
    end
    object COREVERSE_CHECK_PRINTING: TStringField
      DisplayWidth = 1
      FieldName = 'REVERSE_CHECK_PRINTING'
      Origin = '"CO_HIST"."REVERSE_CHECK_PRINTING"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object COCHECK_TIME_OFF_AVAIL: TStringField
      DisplayWidth = 1
      FieldName = 'CHECK_TIME_OFF_AVAIL'
      Origin = '"CO_HIST"."CHECK_TIME_OFF_AVAIL"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object COPR_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_MB_GROUP_NBR'
      Origin = '"CO_HIST"."PR_CHECK_MB_GROUP_NBR"'
    end
    object COPR_REPORT_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_REPORT_MB_GROUP_NBR'
      Origin = '"CO_HIST"."PR_REPORT_MB_GROUP_NBR"'
    end
    object COPR_REPORT_SECOND_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_REPORT_SECOND_MB_GROUP_NBR'
      Origin = '"CO_HIST"."PR_REPORT_SECOND_MB_GROUP_NBR"'
    end
    object COTAX_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_CHECK_MB_GROUP_NBR'
      Origin = '"CO_HIST"."TAX_CHECK_MB_GROUP_NBR"'
    end
    object COTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_EE_RETURN_MB_GROUP_NBR'
      Origin = '"CO_HIST"."TAX_EE_RETURN_MB_GROUP_NBR"'
    end
    object COTAX_RETURN_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_MB_GROUP_NBR'
      Origin = '"CO_HIST"."TAX_RETURN_MB_GROUP_NBR"'
    end
    object COTAX_RETURN_SECOND_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_SECOND_MB_GROUP_NBR'
      Origin = '"CO_HIST"."TAX_RETURN_SECOND_MB_GROUP_NBR"'
    end
    object CONAME_ON_INVOICE: TStringField
      DisplayWidth = 1
      FieldName = 'NAME_ON_INVOICE'
      Origin = '"CO_HIST"."NAME_ON_INVOICE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object COMISC_CHECK_FORM: TStringField
      DisplayWidth = 1
      FieldName = 'MISC_CHECK_FORM'
      Origin = '"CO_HIST"."MISC_CHECK_FORM"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object COAGENCY_CHECK_MB_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'AGENCY_CHECK_MB_GROUP_NBR'
      Origin = '"CO_HIST"."AGENCY_CHECK_MB_GROUP_NBR"'
    end
    object COHOLD_RETURN_QUEUE: TStringField
      DisplayWidth = 1
      FieldName = 'HOLD_RETURN_QUEUE'
      Origin = '"CO_HIST"."HOLD_RETURN_QUEUE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CONUMBER_OF_INVOICE_COPIES: TIntegerField
      DisplayWidth = 10
      FieldName = 'NUMBER_OF_INVOICE_COPIES'
    end
    object COPRORATE_FLAT_FEE_FOR_DBDT: TStringField
      DisplayWidth = 1
      FieldName = 'PRORATE_FLAT_FEE_FOR_DBDT'
      FixedChar = True
      Size = 1
    end
    object COBREAK_CHECKS_BY_DBDT: TStringField
      FieldName = 'BREAK_CHECKS_BY_DBDT'
      Size = 1
    end
    object COINITIAL_EFFECTIVE_DATE: TDateField
      FieldName = 'INITIAL_EFFECTIVE_DATE'
    end
    object COSB_OTHER_SERVICE_NBR: TIntegerField
      FieldName = 'SB_OTHER_SERVICE_NBR'
    end
    object COSHOW_SHORTFALL_CHECK: TStringField
      FieldName = 'SHOW_SHORTFALL_CHECK'
      Size = 1
    end
    object COSUMMARIZE_SUI: TStringField
      FieldName = 'SUMMARIZE_SUI'
      Size = 1
    end
    object COSHOW_TIME_CLOCK_PUNCH: TStringField
      FieldName = 'SHOW_TIME_CLOCK_PUNCH'
      Size = 2
    end
    object COINVOICE_DISCOUNT: TFloatField
      FieldName = 'INVOICE_DISCOUNT'
    end
    object CODISCOUNT_START_DATE: TDateField
      FieldName = 'DISCOUNT_START_DATE'
    end
    object CODISCOUNT_END_DATE: TDateField
      FieldName = 'DISCOUNT_END_DATE'
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 200
    Top = 112
  end
end
