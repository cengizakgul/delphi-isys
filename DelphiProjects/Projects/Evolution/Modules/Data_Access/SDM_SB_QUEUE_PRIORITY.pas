// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_QUEUE_PRIORITY;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_SB_QUEUE_PRIORITY = class(TDM_ddTable)
    SB_QUEUE_PRIORITY: TSB_QUEUE_PRIORITY;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_QUEUE_PRIORITYSB_QUEUE_PRIORITY_NBR: TIntegerField;
    SB_QUEUE_PRIORITYTHREADS: TIntegerField;
    SB_QUEUE_PRIORITYMETHOD_NAME: TStringField;
    SB_QUEUE_PRIORITYPRIORITY: TIntegerField;
    SB_QUEUE_PRIORITYSB_USER_NBR: TIntegerField;
    SB_QUEUE_PRIORITYSB_SEC_GROUPS_NBR: TIntegerField;
    SB_QUEUE_PRIORITYUserName: TStringField;
    SB_QUEUE_PRIORITYGroupName: TStringField;
    procedure SB_QUEUE_PRIORITYTHREADSValidate(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_SB_QUEUE_PRIORITY.SB_QUEUE_PRIORITYTHREADSValidate(
  Sender: TField);
begin
  inherited;
  if SB_QUEUE_PRIORITYTHREADS.IsNull then
    SB_QUEUE_PRIORITYTHREADS.AsInteger := 0;
end;

initialization
  RegisterClass(TDM_SB_QUEUE_PRIORITY);

finalization
  UnregisterClass(TDM_SB_QUEUE_PRIORITY);

end.
