// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_E_D_CODES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_CO_E_D_CODES = class(TDM_ddTable)
    CO_E_D_CODES: TCO_E_D_CODES;
    CO_E_D_CODESED_Lookup: TStringField;
    CO_E_D_CODESCO_E_D_CODES_NBR: TIntegerField;
    CO_E_D_CODESCL_E_DS_NBR: TIntegerField;
    CO_E_D_CODESCO_NBR: TIntegerField;
    CO_E_D_CODESCodeDescription: TStringField;
    CO_E_D_CODESGENERAL_LEDGER_TAG: TStringField;
    CO_E_D_CODESDISTRIBUTE: TStringField;
    DM_CLIENT: TDM_CLIENT;
    CO_E_D_CODESE_D_Code_Type: TStringField;
    CO_E_D_CODESGL_OFFSET: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_E_D_CODES);

finalization
  UnregisterClass(TDM_CO_E_D_CODES);

end.
