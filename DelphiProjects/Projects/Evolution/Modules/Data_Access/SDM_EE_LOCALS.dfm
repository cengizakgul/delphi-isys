inherited DM_EE_LOCALS: TDM_EE_LOCALS
  OldCreateOrder = True
  Left = 423
  Top = 306
  Height = 479
  Width = 741
  object EE_LOCALS: TEE_LOCALS
    ProviderName = 'EE_LOCALS_PROV'
    FieldDefs = <
      item
        Name = 'EE_LOCALS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_LOCAL_TAX_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EXEMPT_EXCLUDE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MISCELLANEOUS_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'EE_COUNTY'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EE_TAX_CODE'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PERCENTAGE_OF_TAX_WAGES'
        DataType = ftFloat
      end
      item
        Name = 'DEDUCT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LOCAL_ENABLED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OVERRIDE_LOCAL_TAX_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OVERRIDE_LOCAL_TAX_VALUE'
        DataType = ftFloat
      end>
    BeforePost = EE_LOCALSBeforePost
    BeforeDelete = EE_LOCALSBeforeDelete
    OnCalcFields = EE_LOCALSCalcFields
    OnNewRecord = EE_LOCALSNewRecord
    Left = 55
    Top = 24
    object EE_LOCALSEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
      Origin = '"EE_LOCALS_HIST"."EE_LOCALS_NBR"'
      Required = True
    end
    object EE_LOCALSEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Origin = '"EE_LOCALS_HIST"."EE_NBR"'
      Required = True
    end
    object EE_LOCALSCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
      Origin = '"EE_LOCALS_HIST"."CO_LOCAL_TAX_NBR"'
      Required = True
      OnChange = EE_LOCALSCO_LOCAL_TAX_NBRChange
    end
    object EE_LOCALSEXEMPT_EXCLUDE: TStringField
      FieldName = 'EXEMPT_EXCLUDE'
      Origin = '"EE_LOCALS_HIST"."EXEMPT_EXCLUDE"'
      Required = True
      OnValidate = EE_LOCALSEXEMPT_EXCLUDEValidate
      Size = 1
    end
    object EE_LOCALSMISCELLANEOUS_AMOUNT: TFloatField
      FieldName = 'MISCELLANEOUS_AMOUNT'
      Origin = '"EE_LOCALS_HIST"."MISCELLANEOUS_AMOUNT"'
    end
    object EE_LOCALSFILLER: TStringField
      FieldName = 'FILLER'
      Origin = '"EE_LOCALS_HIST"."FILLER"'
      Size = 512
    end
    object EE_LOCALSLocal_Name_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Local_Name_Lookup'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 40
      Lookup = True
    end
    object EE_LOCALSEE_COUNTY: TStringField
      FieldName = 'EE_COUNTY'
      Origin = '"EE_LOCALS_HIST"."EE_COUNTY"'
    end
    object EE_LOCALSEE_TAX_CODE: TStringField
      FieldName = 'EE_TAX_CODE'
      Origin = '"EE_LOCALS_HIST"."EE_TAX_CODE"'
    end
    object EE_LOCALSPERCENTAGE_OF_TAX_WAGES: TFloatField
      FieldName = 'PERCENTAGE_OF_TAX_WAGES'
      Origin = '"EE_LOCALS_HIST"."PERCENTAGE_OF_TAX_WAGES"'
    end
    object EE_LOCALSDEDUCT: TStringField
      FieldName = 'DEDUCT'
      Origin = '"EE_LOCALS_HIST"."DEDUCT"'
      Required = True
      Size = 1
    end
    object EE_LOCALSLOCAL_ENABLED: TStringField
      FieldName = 'LOCAL_ENABLED'
      FixedChar = True
      Size = 1
    end
    object EE_LOCALSOVERRIDE_LOCAL_TAX_TYPE: TStringField
      FieldName = 'OVERRIDE_LOCAL_TAX_TYPE'
      Size = 1
    end
    object EE_LOCALSOVERRIDE_LOCAL_TAX_VALUE: TFloatField
      FieldName = 'OVERRIDE_LOCAL_TAX_VALUE'
      DisplayFormat = '#,##0.00######'
    end
    object EE_LOCALSLOCAL_TYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'LOCAL_TYPE'
      Size = 1
      Calculated = True
    end
    object EE_LOCALSTaxTypeDesc: TStringField
      FieldKind = fkCalculated
      FieldName = 'TaxTypeDesc'
      Calculated = True
    end
    object EE_LOCALSSY_STATES_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SY_STATES_NBR'
      Calculated = True
    end
    object EE_LOCALSLOCALTYPEDESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'LOCALTYPEDESC'
      Size = 30
      Calculated = True
    end
    object EE_LOCALSCO_LOCATIONS_ACCOUNTNUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'CO_LOCATIONS_ACCOUNTNUMBER'
      LookupDataSet = DM_CO_LOCATIONS.CO_LOCATIONS
      LookupKeyFields = 'CO_LOCATIONS_NBR'
      LookupResultField = 'ACCOUNT_NUMBER'
      KeyFields = 'CO_LOCATIONS_NBR'
      Visible = False
      Size = 30
      Lookup = True
    end
    object EE_LOCALSState: TStringField
      FieldKind = fkCalculated
      FieldName = 'State'
      Size = 2
      Calculated = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 54
    Top = 81
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 137
    Top = 81
  end
end
