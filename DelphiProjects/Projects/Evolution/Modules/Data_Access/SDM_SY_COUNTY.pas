// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_COUNTY;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_SY_COUNTY = class(TDM_ddTable)
    SY_COUNTY: TSY_COUNTY;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    procedure SY_COUNTYNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_SY_COUNTY.SY_COUNTYNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('SY_STATES_NBR').AsString :=
    DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString;
end;

initialization
  RegisterClass(TDM_SY_COUNTY);

finalization
  UnregisterClass(TDM_SY_COUNTY);

end.
