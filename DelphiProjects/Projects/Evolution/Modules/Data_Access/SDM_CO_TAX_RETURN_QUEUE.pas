// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_TAX_RETURN_QUEUE;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvContext, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvClientDataSet, SDDClasses ;

type
  TDM_CO_TAX_RETURN_QUEUE = class(TDM_ddTable)
    CO_TAX_RETURN_QUEUE: TCO_TAX_RETURN_QUEUE;
    procedure CO_TAX_RETURN_QUEUEBeforeEdit(DataSet: TDataSet);
    procedure CO_TAX_RETURN_QUEUEBeforePost(DataSet: TDataSet);
    procedure CO_TAX_RETURN_QUEUEBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_TAX_RETURN_QUEUE.CO_TAX_RETURN_QUEUEBeforeEdit(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('PERIOD_END_DATE').AsDateTime, True);
end;

procedure TDM_CO_TAX_RETURN_QUEUE.CO_TAX_RETURN_QUEUEBeforePost(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('PERIOD_END_DATE').AsDateTime, True);
  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;

end;

procedure TDM_CO_TAX_RETURN_QUEUE.CO_TAX_RETURN_QUEUEBeforeDelete(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('PERIOD_END_DATE').AsDateTime, True);
end;

initialization
  RegisterClass(TDM_CO_TAX_RETURN_QUEUE);

finalization
  UnregisterClass(TDM_CO_TAX_RETURN_QUEUE);

end.
