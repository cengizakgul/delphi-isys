inherited DM_PR_SCHEDULED_E_DS: TDM_PR_SCHEDULED_E_DS
  OldCreateOrder = False
  Left = 240
  Top = 107
  Height = 480
  Width = 696
  object DM_PAYROLL: TDM_PAYROLL
    Left = 216
    Top = 104
  end
  object PR_SCHEDULED_E_DS: TPR_SCHEDULED_E_DS
    Aggregates = <>
    Params = <>
    ProviderName = 'PR_SCHEDULED_E_DS_PROV'
    BeforeInsert = PR_SCHEDULED_E_DSBeforeInsert
    BeforeEdit = PR_SCHEDULED_E_DSBeforeEdit
    BeforeDelete = PR_SCHEDULED_E_DSBeforeDelete
    ValidateWithMask = True
    Left = 216
    Top = 168
    object PR_SCHEDULED_E_DSPR_SCHEDULED_E_DS_NBR: TIntegerField
      FieldName = 'PR_SCHEDULED_E_DS_NBR'
    end
    object PR_SCHEDULED_E_DSPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_SCHEDULED_E_DSCO_E_D_CODES_NBR: TIntegerField
      FieldName = 'CO_E_D_CODES_NBR'
    end
    object PR_SCHEDULED_E_DSEXCLUDE: TStringField
      FieldName = 'EXCLUDE'
      Size = 1
    end
    object PR_SCHEDULED_E_DSED_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'ED_Lookup'
      LookupDataSet = DM_CO_E_D_CODES.CO_E_D_CODES
      LookupKeyFields = 'CO_E_D_CODES_NBR'
      LookupResultField = 'ED_Lookup'
      KeyFields = 'CO_E_D_CODES_NBR'
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 296
    Top = 104
  end
end
