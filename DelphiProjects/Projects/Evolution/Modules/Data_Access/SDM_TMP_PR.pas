// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_TMP_PR;

interface

uses
  SDM_ddTable, SDataDictTemp,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_TMP_PR = class(TDM_ddTable)
    DM_TEMPORARY: TDM_TEMPORARY;
    TMP_PR: TTMP_PR;
    TMP_PRPR_NBR: TIntegerField;
    TMP_PRCL_NBR: TIntegerField;
    TMP_PRCO_NBR: TIntegerField;
    TMP_PRRUN_NUMBER: TIntegerField;
    TMP_PRCHECK_DATE: TDateField;
    TMP_PRSCHEDULED: TStringField;
    TMP_PRSTATUS: TStringField;
    TMP_PRClient_Number_Lookup: TStringField;
    TMP_PRCompany_Number_Lookup: TStringField;
    TMP_PRCompany_Name_Lookup: TStringField;
    TMP_PRProcess_Priority: TIntegerField;
    TMP_PRSelected: TStringField;
    TMP_PRAPPROVED_BY_FINANCE: TStringField;
    TMP_PRAPPROVED_BY_MANAGEMENT: TStringField;
    TMP_PRAPPROVED_BY_TAX: TStringField;
    TMP_PRPROCESS_DATE: TDateTimeField;
    TMP_PRSTATUS_DATE: TDateTimeField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_TMP_PR: TDM_TMP_PR;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_TMP_PR);

finalization
  UnregisterClass(TDM_TMP_PR);

end.
