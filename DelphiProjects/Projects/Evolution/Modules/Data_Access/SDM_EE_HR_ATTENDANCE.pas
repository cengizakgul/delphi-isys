// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_HR_ATTENDANCE;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet;

type
  TDM_EE_HR_ATTENDANCE = class(TDM_ddTable)
    EE_HR_ATTENDANCE: TEE_HR_ATTENDANCE;
    EE_HR_ATTENDANCEATTENDANCE_TYPE_NUMBER: TIntegerField;
    EE_HR_ATTENDANCECO_HR_ATTENDANCE_TYPES_NBR: TIntegerField;
    EE_HR_ATTENDANCEEE_HR_ATTENDANCE_NBR: TIntegerField;
    EE_HR_ATTENDANCEEE_NBR: TIntegerField;
    EE_HR_ATTENDANCEEXCUSED: TStringField;
    EE_HR_ATTENDANCEHOURS_USED: TFloatField;
    EE_HR_ATTENDANCENOTES: TBlobField;
    EE_HR_ATTENDANCEPERIOD_FROM: TDateField;
    EE_HR_ATTENDANCEPERIOD_TO: TDateField;
    EE_HR_ATTENDANCEWARNING_ISSUED: TStringField;
    EE_HR_ATTENDANCEAttendanceDescr: TStringField;
    DM_COMPANY: TDM_COMPANY;
    EE_HR_ATTENDANCEPR_NBR: TIntegerField;
    procedure EE_HR_ATTENDANCENewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}
procedure TDM_EE_HR_ATTENDANCE.EE_HR_ATTENDANCENewRecord(
  DataSet: TDataSet);
begin
  EE_HR_ATTENDANCEATTENDANCE_TYPE_NUMBER.AsInteger := 1;
end;

initialization
  RegisterClass( TDM_EE_HR_ATTENDANCE );

finalization
  UnregisterClass( TDM_EE_HR_ATTENDANCE );

end.
