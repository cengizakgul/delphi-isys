inherited DM_CO_PR_ACH: TDM_CO_PR_ACH
  Height = 256
  Width = 335
  object CO_PR_ACH: TCO_PR_ACH
    ProviderName = 'CO_PR_ACH_PROV'
    BeforePost = CO_PR_ACHBeforePost
    Left = 79
    Top = 18
    object CO_PR_ACHCO_PR_ACH_NBR: TIntegerField
      FieldName = 'CO_PR_ACH_NBR'
    end
    object CO_PR_ACHCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_PR_ACHACH_DATE: TDateTimeField
      FieldName = 'ACH_DATE'
    end
    object CO_PR_ACHAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object CO_PR_ACHSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object CO_PR_ACHPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object CO_PR_ACHNOTES: TBlobField
      FieldName = 'NOTES'
      Size = 1
    end
    object CO_PR_ACHFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object CO_PR_ACHCheckDate: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'CheckDate'
      LookupDataSet = DM_PR.PR
      LookupKeyFields = 'PR_NBR'
      LookupResultField = 'CHECK_DATE'
      KeyFields = 'PR_NBR'
      Lookup = True
    end
    object CO_PR_ACHSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 75
    Top = 75
  end
end
