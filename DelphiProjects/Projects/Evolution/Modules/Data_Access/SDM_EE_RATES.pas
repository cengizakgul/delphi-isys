// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_RATES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvUtils, EvConsts, EvDataAccessComponents,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvTypes, EvExceptions, SDataStructure, EvContext,
  EvClientDataSet;

type
  TDM_EE_RATES = class(TDM_ddTable)
    EE_RATES: TEE_RATES;
    EE_RATESEE_RATES_NBR: TIntegerField;
    EE_RATESCO_DIVISION_NBR: TIntegerField;
    EE_RATESCO_BRANCH_NBR: TIntegerField;
    EE_RATESCO_DEPARTMENT_NBR: TIntegerField;
    EE_RATESCO_TEAM_NBR: TIntegerField;
    EE_RATESEE_NBR: TIntegerField;
    EE_RATESRATE_NUMBER: TIntegerField;
    EE_RATESPRIMARY_RATE: TStringField;
    EE_RATESRATE_AMOUNT: TFloatField;
    EE_RATESCO_JOBS_NBR: TIntegerField;
    EE_RATESCO_WORKERS_COMP_NBR: TIntegerField;
    EE_RATESRNUM: TAggregateField;
    EE_RATESPrRateMax: TAggregateField;
    DM_COMPANY: TDM_COMPANY;
    EE_RATESCO_HR_POSITIONS_NBR: TIntegerField;
    EE_RATESCO_HR_POSITION_GRADES: TIntegerField;
    procedure EE_RATESAfterOpen(DataSet: TDataSet);
    procedure EE_RATESRATE_NUMBERValidate(Sender: TField);
    procedure EE_RATESBeforePost(DataSet: TDataSet);
    procedure EE_RATESBeforeDelete(DataSet: TDataSet);
    procedure EE_RATESAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_EE_RATES.EE_RATESAfterOpen(DataSet: TDataSet);
const
  ixName1 = 'ixEE_NBR_RATE_NUMBER';
  ixFields1 = 'EE_NBR;RATE_NUMBER';
begin
  with (DataSet as TEvBasicClientDataSet) do
  try
    DisableControls;
    IndexDefs.Update;
    if IndexDefs.IndexOf(ixName1) = -1 then
      AddIndex(ixName1, ixFields1, [ixUnique], '', '', 0);
    IndexDefs.Update;
  finally
    EnableControls;
  end
end;

procedure TDM_EE_RATES.EE_RATESRATE_NUMBERValidate(Sender: TField);
begin
  ctx_DataAccess.CheckCompanyLock
end;

procedure TDM_EE_RATES.EE_RATESBeforePost(DataSet: TDataSet);
begin
  if DataSet.State = dsInsert then
    ctx_DataAccess.CheckCompanyLock;

  if (DataSet.FieldByName('RATE_AMOUNT').IsNull) then
     raise EUpdateError.CreateHelp('Field Rate Amount must have a value', IDH_ConsistencyViolation);

end;

procedure TDM_EE_RATES.EE_RATESBeforeDelete(DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock
end;

// RE #20112
// Moved here from the OnNewRecord handler
// Never set default values in an OnNewRecord handler
// TDataSet.EndInsertAppend sets Modified to false and if CheckBrowseMode
// is called then it cancels updates if dataset is in Insert state
// but Modified is false
// -- gdy
procedure TDM_EE_RATES.EE_RATESAfterInsert(DataSet: TDataSet);
begin
  with DataSet do
  begin
    FindField('RATE_NUMBER').AsInteger := VarToInt(FindField('RateNumMax').Value) + 1;
    if RecordCount = 0 then
      FindField('PRIMARY_RATE').AsString := GROUP_BOX_YES
    else
      FindField('PRIMARY_RATE').AsString := GROUP_BOX_NO
  end;
end;

initialization
  RegisterClass(TDM_EE_RATES);

finalization
  UnregisterClass(TDM_EE_RATES);

end.
