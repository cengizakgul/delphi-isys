inherited DM_CO_TEAM_LOCALS: TDM_CO_TEAM_LOCALS
  OldCreateOrder = False
  Left = 314
  Top = 41
  Height = 375
  Width = 544
  object DM_COMPANY: TDM_COMPANY
    Left = 104
    Top = 40
  end
  object CO_TEAM_LOCALS: TCO_TEAM_LOCALS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_TEAM_LOCALS_PROV'
    ValidateWithMask = True
    Left = 40
    Top = 24
    object CO_TEAM_LOCALSLocalName: TStringField
      DisplayLabel = 'Local Name'
      FieldKind = fkLookup
      FieldName = 'LocalName'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 40
      Lookup = True
    end
    object CO_TEAM_LOCALSCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object CO_TEAM_LOCALSCO_TEAM_LOCALS_NBR: TIntegerField
      FieldName = 'CO_TEAM_LOCALS_NBR'
    end
    object CO_TEAM_LOCALSCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
  end
end
