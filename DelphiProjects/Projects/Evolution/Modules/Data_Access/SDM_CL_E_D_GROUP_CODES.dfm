inherited DM_CL_E_D_GROUP_CODES: TDM_CL_E_D_GROUP_CODES
  Height = 38
  Width = 132
  object CL_E_D_GROUP_CODES: TCL_E_D_GROUP_CODES
    ProviderName = 'CL_E_D_GROUP_CODES_PROV'
    Left = 63
    Top = 31
    object CL_E_D_GROUP_CODESCL_E_D_GROUP_CODES_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUP_CODES_NBR'
    end
    object CL_E_D_GROUP_CODESCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object CL_E_D_GROUP_CODESCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
    end
    object CL_E_D_GROUP_CODESCodeDescription: TStringField
      FieldKind = fkLookup
      FieldName = 'CodeDescription'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object CL_E_D_GROUP_CODESED_Code_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'ED_Code_Lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'E_D_CODE_TYPE'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
    object CL_E_D_GROUP_CODESCustom_Code_lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Custom_Code_lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 66
    Top = 90
  end
end
