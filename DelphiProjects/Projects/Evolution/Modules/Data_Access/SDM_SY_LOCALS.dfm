inherited DM_SY_LOCALS: TDM_SY_LOCALS
  OldCreateOrder = True
  Left = 621
  Top = 188
  Height = 309
  Width = 487
  object SY_LOCALS: TSY_LOCALS
    ProviderName = 'SY_LOCALS_PROV'
    PictureMasks.Strings = (
      'TAX_RATE'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*8[#]]},.#*7[#]}'#9'T'#9'F'
      
        'TAX_AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9'T'#9 +
        'F'
      
        'TAX_MAXIMUM'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9'T' +
        #9'F'
      
        'WAGE_MAXIMUM'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9 +
        'T'#9'F'
      
        'WEEKLY_TAX_CAP'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]' +
        '}'#9'T'#9'F'
      
        'MINIMUM_HOURS_WORKED'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.' +
        '#*1[#]}'#9'T'#9'F'
      
        'MISCELLANEOUS_AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.' +
        '#*1[#]}'#9'T'#9'F'
      
        'NEXT_TAX_RATE'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*8[#]]},.#*7[#]}' +
        #9'T'#9'F'
      
        'NEXT_TAX_AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#' +
        ']}'#9'T'#9'F'
      
        'NEXT_TAX_MAXIMUM'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[' +
        '#]}'#9'T'#9'F'
      
        'NEXT_WAGE_MAXIMUM'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1' +
        '[#]}'#9'T'#9'F'
      
        'NEXT_WEEKLY_TAX_CAP'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#' +
        '*1[#]}'#9'T'#9'F'
      
        'NEXT_MINIMUM_HOURS_WORKED'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#' +
        ']]},.#*1[#]}'#9'T'#9'F'
      
        'NEXT_MISCELLANEOUS_AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#' +
        ']]},.#*1[#]}'#9'T'#9'F'
      'ZIP_CODE'#9'*5{#}[-*4#]'#9'T'#9'F'
      
        'LOCAL_TAX_IDENTIFIER'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F'
      
        'LOCAL_MINIMUM_WAGE'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*' +
        '1[#]}'#9'T'#9'F')
    OnCalcFields = SY_LOCALSCalcFields
    OnNewRecord = SY_LOCALSNewRecord
    Left = 72
    Top = 64
    object SY_LOCALSNAME: TStringField
      DisplayLabel = 'LOCALITY_NAME'
      DisplayWidth = 40
      FieldName = 'NAME'
      Origin = '"SY_LOCALS_HIST"."NAME"'
      Required = True
      Size = 40
    end
    object SY_LOCALSLocalState: TStringField
      DisplayWidth = 2
      FieldKind = fkLookup
      FieldName = 'LocalState'
      LookupDataSet = DM_SY_STATES.SY_STATES
      LookupKeyFields = 'SY_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'SY_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object SY_LOCALSLocalTypeDescription: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'LocalTypeDescription'
      Calculated = True
    end
    object SY_LOCALSLOCAL_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'LOCAL_TYPE'
      Origin = '"SY_LOCALS_HIST"."LOCAL_TYPE"'
      Required = True
      Size = 1
    end
    object SY_LOCALSSY_LOCALS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_LOCALS_NBR'
      Origin = '"SY_LOCALS_HIST"."SY_LOCALS_NBR"'
      Required = True
      Visible = False
    end
    object SY_LOCALSSY_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_STATES_NBR'
      Origin = '"SY_LOCALS_HIST"."SY_STATES_NBR"'
      Required = True
      Visible = False
    end
    object SY_LOCALSZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"SY_LOCALS_HIST"."ZIP_CODE"'
      Visible = False
      Size = 10
    end
    object SY_LOCALSLOCAL_MINIMUM_WAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'LOCAL_MINIMUM_WAGE'
      Origin = '"SY_LOCALS_HIST"."LOCAL_MINIMUM_WAGE"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSSY_COUNTY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_COUNTY_NBR'
      Origin = '"SY_LOCALS_HIST"."SY_COUNTY_NBR"'
      Visible = False
    end
    object SY_LOCALSLOCAL_TAX_IDENTIFIER: TStringField
      DisplayWidth = 20
      FieldName = 'LOCAL_TAX_IDENTIFIER'
      Origin = '"SY_LOCALS_HIST"."LOCAL_TAX_IDENTIFIER"'
      Visible = False
    end
    object SY_LOCALSAGENCY_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'AGENCY_NUMBER'
      Origin = '"SY_LOCALS_HIST"."AGENCY_NUMBER"'
      Required = True
      Visible = False
    end
    object SY_LOCALSTAX_RATE: TFloatField
      DisplayWidth = 10
      FieldName = 'TAX_RATE'
      Origin = '"SY_LOCALS_HIST"."TAX_RATE"'
      Visible = False
      DisplayFormat = '#,##0.00######'
      EditFormat = '###0.00######'
    end
    object SY_LOCALSTAX_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'TAX_AMOUNT'
      Origin = '"SY_LOCALS_HIST"."TAX_AMOUNT"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSTAX_MAXIMUM: TFloatField
      DisplayWidth = 10
      FieldName = 'TAX_MAXIMUM'
      Origin = '"SY_LOCALS_HIST"."TAX_MAXIMUM"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSWAGE_MAXIMUM: TFloatField
      DisplayWidth = 10
      FieldName = 'WAGE_MAXIMUM'
      Origin = '"SY_LOCALS_HIST"."WAGE_MAXIMUM"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSWEEKLY_TAX_CAP: TFloatField
      DisplayWidth = 10
      FieldName = 'WEEKLY_TAX_CAP'
      Origin = '"SY_LOCALS_HIST"."WEEKLY_TAX_CAP"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSMINIMUM_HOURS_WORKED: TFloatField
      DisplayWidth = 10
      FieldName = 'MINIMUM_HOURS_WORKED'
      Origin = '"SY_LOCALS_HIST"."MINIMUM_HOURS_WORKED"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSMINIMUM_HOURS_WORKED_PER: TStringField
      DisplayWidth = 1
      FieldName = 'MINIMUM_HOURS_WORKED_PER'
      Origin = '"SY_LOCALS_HIST"."MINIMUM_HOURS_WORKED_PER"'
      Visible = False
      Size = 1
    end
    object SY_LOCALSCALCULATION_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'CALCULATION_METHOD'
      Origin = '"SY_LOCALS_HIST"."CALCULATION_METHOD"'
      Required = True
      Visible = False
      Size = 1
    end
    object SY_LOCALSMISCELLANEOUS_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'MISCELLANEOUS_AMOUNT'
      Origin = '"SY_LOCALS_HIST"."MISCELLANEOUS_AMOUNT"'
      Visible = False
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object SY_LOCALSPRINT_RETURN_IF_ZERO: TStringField
      DisplayWidth = 1
      FieldName = 'PRINT_RETURN_IF_ZERO'
      Origin = '"SY_LOCALS_HIST"."PRINT_RETURN_IF_ZERO"'
      Required = True
      Visible = False
      Size = 1
    end
    object SY_LOCALSUSE_MISC_TAX_RETURN_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'USE_MISC_TAX_RETURN_CODE'
      Origin = '"SY_LOCALS_HIST"."USE_MISC_TAX_RETURN_CODE"'
      Required = True
      Visible = False
      Size = 1
    end
    object SY_LOCALSSY_LOCAL_TAX_PMT_AGENCY_NBR: TIntegerField
      FieldName = 'SY_LOCAL_TAX_PMT_AGENCY_NBR'
      Origin = '"SY_LOCALS_HIST"."SY_LOCAL_TAX_PMT_AGENCY_NBR"'
      Visible = False
    end
    object SY_LOCALSSY_TAX_PMT_REPORT_NBR: TIntegerField
      FieldName = 'SY_TAX_PMT_REPORT_NBR'
      Origin = '"SY_LOCALS_HIST"."SY_TAX_PMT_REPORT_NBR"'
      Visible = False
    end
    object SY_LOCALSSY_LOCAL_REPORTING_AGENCY_NBR: TIntegerField
      FieldName = 'SY_LOCAL_REPORTING_AGENCY_NBR'
      Origin = '"SY_LOCALS_HIST"."SY_LOCAL_REPORTING_AGENCY_NBR"'
      Visible = False
    end
    object SY_LOCALSTAX_DEPOSIT_FOLLOW_STATE: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_DEPOSIT_FOLLOW_STATE'
      Origin = '"SY_LOCALS_HIST"."TAX_DEPOSIT_FOLLOW_STATE"'
      Required = True
      Visible = False
      Size = 1
    end
    object SY_LOCALSFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Origin = '"SY_LOCALS_HIST"."FILLER"'
      Visible = False
      Size = 512
    end
    object SY_LOCALSLOCAL_ACTIVE: TStringField
      FieldName = 'LOCAL_ACTIVE'
      Origin = '"SY_LOCALS_HIST"."LOCAL_ACTIVE"'
      Required = True
      Size = 1
    end
    object SY_LOCALSCountyName: TStringField
      FieldKind = fkLookup
      FieldName = 'CountyName'
      LookupDataSet = DM_SY_COUNTY.SY_COUNTY
      LookupKeyFields = 'SY_COUNTY_NBR'
      LookupResultField = 'COUNTY_NAME'
      KeyFields = 'SY_COUNTY_NBR'
      Size = 40
      Lookup = True
    end
    object SY_LOCALSROUND_TO_NEAREST_DOLLAR: TStringField
      FieldName = 'ROUND_TO_NEAREST_DOLLAR'
      Origin = '"SY_LOCALS_HIST"."ROUND_TO_NEAREST_DOLLAR"'
      Required = True
      Size = 1
    end
    object SY_LOCALSTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
      Origin = '"SY_LOCALS_HIST"."TAX_TYPE"'
      Required = True
      Size = 1
    end
    object SY_LOCALSTAX_FREQUENCY: TStringField
      FieldName = 'TAX_FREQUENCY'
      Origin = '"SY_LOCALS_HIST"."TAX_FREQUENCY"'
      Required = True
      Size = 1
    end
    object SY_LOCALSW2_BOX: TStringField
      FieldName = 'W2_BOX'
      Origin = '"SY_LOCALS_HIST"."W2_BOX"'
      Size = 10
    end
    object SY_LOCALSPAY_WITH_STATE: TStringField
      FieldName = 'PAY_WITH_STATE'
      Origin = '"SY_LOCALS_HIST"."PAY_WITH_STATE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SY_LOCALSAgencyName: TStringField
      FieldKind = fkLookup
      FieldName = 'AgencyName'
      LookupDataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
      LookupKeyFields = 'SY_GLOBAL_AGENCY_NBR'
      LookupResultField = 'AGENCY_NAME'
      KeyFields = 'SY_LOCAL_REPORTING_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object SY_LOCALSLocalTypeDescriptionNew: TStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'LocalTypeDescriptionNew'
      Size = 30
      Calculated = True
    end
    object SY_LOCALSPSDCode: TStringField
      FieldKind = fkCalculated
      FieldName = 'PSDCode'
      Calculated = True
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 72
    Top = 117
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 176
    Top = 64
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 184
    Top = 128
  end
end
