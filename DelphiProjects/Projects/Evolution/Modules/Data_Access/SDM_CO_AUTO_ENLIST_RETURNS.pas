// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_AUTO_ENLIST_RETURNS;

interface

uses
  SDM_ddTable, SDataDictSystem,
  SysUtils, Classes, DB,   kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, SDataStructure, evUtils,
  SDataDictclient, SFieldCodeValues;

type
  TDM_CO_AUTO_ENLIST_RETURNS = class(TDM_ddTable)
    CO_AUTO_ENLIST_RETURNSCO_AUTO_ENLIST_RETURNS_NBR: TIntegerField;
    CO_AUTO_ENLIST_RETURNSCO_NBR: TIntegerField;
    CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    CO_AUTO_ENLIST_RETURNSPROCESS_TYPE: TStringField;
    CO_AUTO_ENLIST_RETURNSAGENCY_NAME: TStringField;
    CO_AUTO_ENLIST_RETURNSADDITIONAL_NAME: TStringField;
    CO_AUTO_ENLIST_RETURNSNAME: TStringField;
    CO_AUTO_ENLIST_RETURNSSProcess: TStringField;
    CO_AUTO_ENLIST_RETURNSAgencyName: TStringField;
    CO_AUTO_ENLIST_RETURNSSY_GLOBAL_AGENCY_NBR: TIntegerField;
    CO_AUTO_ENLIST_RETURNSReturnFrequency: TStringField;
    procedure CO_AUTO_ENLIST_RETURNSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CO_AUTO_ENLIST_RETURNS.CO_AUTO_ENLIST_RETURNSCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  CO_AUTO_ENLIST_RETURNSSProcess.AsString := 'Enlist';
  if CO_AUTO_ENLIST_RETURNSPROCESS_TYPE.AsString = 'D' then
     CO_AUTO_ENLIST_RETURNSSProcess.AsString := 'Don''t Enlist';

  if not DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Active then
      DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.DataRequired('ALL');
  if not DM_SYSTEM_MISC.SY_REPORTS_GROUP.Active then
      DM_SYSTEM_MISC.SY_REPORTS_GROUP.DataRequired('ALL');
  if not DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Active then
      DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.DataRequired('ALL');
  if not DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Active then
      DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');

  if CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR.AsInteger > 0  then
  begin
   CO_AUTO_ENLIST_RETURNSNAME.AsString :=
    DM_SYSTEM_MISC.SY_REPORTS_GROUP.ShadowDataSet.CachedLookup(StrToIntDef(DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ShadowDataSet.CachedLookup(CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR.AsInteger,'SY_REPORTS_GROUP_NBR'),0) ,'NAME');

   CO_AUTO_ENLIST_RETURNSADDITIONAL_NAME.AsString :=
    DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(StrToIntDef(DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ShadowDataSet.CachedLookup(CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR.AsInteger,'SY_GL_AGENCY_FIELD_OFFICE_NBR'),0) ,'ADDITIONAL_NAME');

   CO_AUTO_ENLIST_RETURNSAgencyName.AsString :=
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(StrToIntDef(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(StrToIntDef(DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ShadowDataSet.CachedLookup(CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR.AsInteger,'SY_GL_AGENCY_FIELD_OFFICE_NBR'),0) ,'SY_GLOBAL_AGENCY_NBR'),0) ,'AGENCY_NAME');

   CO_AUTO_ENLIST_RETURNSSY_GLOBAL_AGENCY_NBR.AsInteger :=
       StrToIntDef(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(StrToIntDef(DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ShadowDataSet.CachedLookup(CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR.AsInteger,'SY_GL_AGENCY_FIELD_OFFICE_NBR'),0) ,'SY_GLOBAL_AGENCY_NBR'),0);

   CO_AUTO_ENLIST_RETURNSReturnFrequency.AsString :=
       ReturnDescription(ReturnFrequencyType_ComboChoices, DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ShadowDataSet.CachedLookup(CO_AUTO_ENLIST_RETURNSSY_GL_AGENCY_REPORT_NBR.AsInteger,'RETURN_FREQUENCY'));

  end;

end;

initialization
  RegisterClass(TDM_CO_AUTO_ENLIST_RETURNS);

finalization
  UnregisterClass(TDM_CO_AUTO_ENLIST_RETURNS);

end.
