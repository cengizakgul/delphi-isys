// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_BATCH_LOCAL_OR_TEMPS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_BATCH_LOCAL_OR_TEMPS = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    CO_BATCH_LOCAL_OR_TEMPS: TCO_BATCH_LOCAL_OR_TEMPS;
    CO_BATCH_LOCAL_OR_TEMPSCO_BATCH_LOCAL_OR_TEMPS_NBR: TIntegerField;
    CO_BATCH_LOCAL_OR_TEMPSCO_LOCAL_TAX_NBR: TIntegerField;
    CO_BATCH_LOCAL_OR_TEMPSOVERRIDE_AMOUNT: TFloatField;
    CO_BATCH_LOCAL_OR_TEMPSEXCLUDE_LOCAL: TStringField;
    CO_BATCH_LOCAL_OR_TEMPSCO_PR_CHECK_TEMPLATES_NBR: TIntegerField;
    CO_BATCH_LOCAL_OR_TEMPSLocalName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BATCH_LOCAL_OR_TEMPS: TDM_CO_BATCH_LOCAL_OR_TEMPS;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_BATCH_LOCAL_OR_TEMPS);

finalization
  UnregisterClass(TDM_CO_BATCH_LOCAL_OR_TEMPS);

end.
