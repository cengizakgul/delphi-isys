inherited DM_CO_SUI: TDM_CO_SUI
  OldCreateOrder = True
  Left = 410
  Top = 159
  Height = 479
  Width = 741
  object CO_SUI: TCO_SUI
    ProviderName = 'CO_SUI_PROV'
    PictureMasks.Strings = (
      
        'RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]' +
        ',({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-' +
        ']{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F')
    BeforePost = CO_SUIBeforePost
    OnNewRecord = CO_SUINewRecord
    Left = 52
    Top = 13
    object CO_SUIDESCRIPTION: TStringField
      DisplayWidth = 20
      FieldName = 'DESCRIPTION'
      Origin = '"CO_SUI_HIST"."DESCRIPTION"'
      Required = True
      Size = 40
    end
    object CO_SUIRATE: TFloatField
      DisplayWidth = 10
      FieldName = 'RATE'
      Origin = '"CO_SUI_HIST"."RATE"'
      Required = True
      Visible = False
      DisplayFormat = '#,##0.00######'
    end
    object CO_SUICO_SUI_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_SUI_NBR'
      Origin = '"CO_SUI_HIST"."CO_SUI_NBR"'
      Required = True
      Visible = False
    end
    object CO_SUICO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Origin = '"CO_SUI_HIST"."CO_NBR"'
      Required = True
      Visible = False
    end
    object CO_SUICO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_STATES_NBR'
      Origin = '"CO_SUI_HIST"."CO_STATES_NBR"'
      Required = True
      Visible = False
    end
    object CO_SUISY_SUI_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_SUI_NBR'
      Origin = '"CO_SUI_HIST"."SY_SUI_NBR"'
      Required = True
      Visible = False
      OnChange = CO_SUISY_SUI_NBRChange
    end
    object CO_SUITAX_RETURN_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'TAX_RETURN_CODE'
      Origin = '"CO_SUI_HIST"."TAX_RETURN_CODE"'
      Visible = False
      Size = 10
    end
    object CO_SUIPAYMENT_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'PAYMENT_METHOD'
      Origin = '"CO_SUI_HIST"."PAYMENT_METHOD"'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_SUIFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Origin = '"CO_SUI_HIST"."FILLER"'
      Visible = False
      Size = 512
    end
    object CO_SUIState: TStringField
      DisplayLabel = 'STATE'
      DisplayWidth = 2
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'CO_STATES_NBR'
      Visible = False
      Lookup = True
    end
    object CO_SUIFINAL_TAX_RETURN: TStringField
      FieldName = 'FINAL_TAX_RETURN'
      Origin = '"CO_SUI_HIST"."FINAL_TAX_RETURN"'
      Required = True
      Visible = False
      Size = 1
    end
    object CO_SUIGL_TAG: TStringField
      DisplayWidth = 20
      FieldName = 'GL_TAG'
    end
    object CO_SUISUI_ACTIVE: TStringField
      FieldName = 'SUI_ACTIVE'
      Required = True
      FixedChar = True
      Size = 1
    end
    object CO_SUIAPPLIED_FOR: TStringField
      FieldName = 'APPLIED_FOR'
      Size = 1
    end
    object CO_SUIGL_OFFSET_TAG: TStringField
      FieldName = 'GL_OFFSET_TAG'
    end
    object CO_SUISUI_Name: TStringField
      FieldKind = fkLookup
      FieldName = 'SUI_Name'
      LookupDataSet = DM_SY_SUI.SY_SUI
      LookupKeyFields = 'SY_SUI_NBR'
      LookupResultField = 'SUI_TAX_NAME'
      KeyFields = 'SY_SUI_NBR'
      Size = 40
      Lookup = True
    end
    object CO_SUIEE_Or_ER: TStringField
      FieldKind = fkLookup
      FieldName = 'EE_Or_ER'
      LookupDataSet = DM_SY_SUI.SY_SUI
      LookupKeyFields = 'SY_SUI_NBR'
      LookupResultField = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
      KeyFields = 'SY_SUI_NBR'
      Size = 1
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 51
    Top = 75
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 152
    Top = 32
  end
end
