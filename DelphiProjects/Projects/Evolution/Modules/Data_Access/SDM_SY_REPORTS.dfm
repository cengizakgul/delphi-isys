inherited DM_SY_REPORTS: TDM_SY_REPORTS
  OldCreateOrder = True
  Left = 570
  Top = 386
  Height = 257
  Width = 235
  object SY_REPORTS: TSY_REPORTS
    ProviderName = 'SY_REPORTS_PROV'
    FieldDefs = <
      item
        Name = 'SY_REPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ACTIVE_REPORT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_REPORT_WRITER_REPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'NDescription'
        DataType = ftString
        Size = 60
      end>
    OnCalcFields = SY_REPORTSCalcFields
    Left = 48
    Top = 24
    object SY_REPORTSSY_REPORTS_NBR: TIntegerField
      FieldName = 'SY_REPORTS_NBR'
    end
    object SY_REPORTSDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object SY_REPORTSACTIVE_REPORT: TStringField
      FieldName = 'ACTIVE_REPORT'
      FixedChar = True
      Size = 1
    end
    object SY_REPORTSSY_REPORT_WRITER_REPORTS_NBR: TIntegerField
      FieldName = 'SY_REPORT_WRITER_REPORTS_NBR'
    end
    object SY_REPORTSFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object SY_REPORTSNDescription: TStringField
      DisplayWidth = 60
      FieldKind = fkInternalCalc
      FieldName = 'NDescription'
      Size = 60
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 128
    Top = 64
  end
end
