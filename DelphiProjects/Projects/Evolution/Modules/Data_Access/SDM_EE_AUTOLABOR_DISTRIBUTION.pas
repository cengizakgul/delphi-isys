// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_AUTOLABOR_DISTRIBUTION;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvTypes,
  EvExceptions, EvClientDataSet;

type
  TDM_EE_AUTOLABOR_DISTRIBUTION = class(TDM_ddTable)
    EE_AUTOLABOR_DISTRIBUTION: TEE_AUTOLABOR_DISTRIBUTION;
    EE_AUTOLABOR_DISTRIBUTIONEE_AUTOLABOR_DISTRIBUTION_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_DIVISION_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_BRANCH_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_DEPARTMENT_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_TEAM_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONEE_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_JOBS_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCO_WORKERS_COMP_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONCL_E_D_GROUPS_NBR: TIntegerField;
    EE_AUTOLABOR_DISTRIBUTIONPERCENTAGE: TFloatField;
    EE_AUTOLABOR_DISTRIBUTIONCUSTOM_TEAM_NUMBER: TStringField;
    EE_AUTOLABOR_DISTRIBUTIONCUSTOM_DEPARTMENT_NUMBER: TStringField;
    EE_AUTOLABOR_DISTRIBUTIONCUSTOM_BRANCH_NUMBER: TStringField;
    EE_AUTOLABOR_DISTRIBUTIONCUSTOM_DIVISION_NUMBER: TStringField;
    DM_COMPANY: TDM_COMPANY;
    EE_AUTOLABOR_DISTRIBUTIONJobDescription: TStringField;
    EE_AUTOLABOR_DISTRIBUTIONWorkersComp: TStringField;
    EE_AUTOLABOR_DISTRIBUTIONEDGroupName: TStringField;
    procedure EE_AUTOLABOR_DISTRIBUTIONNewRecord(DataSet: TDataSet);
    procedure EE_AUTOLABOR_DISTRIBUTIONBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_EE_AUTOLABOR_DISTRIBUTION.EE_AUTOLABOR_DISTRIBUTIONNewRecord(
  DataSet: TDataSet);
begin
  DataSet['CO_NBR'] := DM_COMPANY.CO['CO_NBR'];
end;

procedure TDM_EE_AUTOLABOR_DISTRIBUTION.EE_AUTOLABOR_DISTRIBUTIONBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (DataSet.FieldByName('PERCENTAGE').IsNull) or ( DataSet.FieldByName('PERCENTAGE').AsFloat = 0 ) then
    raise EUpdateError.CreateHelp('Field "Percentage"  must be greater than 0.00%.', IDH_ConsistencyViolation);

end;

initialization
  RegisterClass(TDM_EE_AUTOLABOR_DISTRIBUTION);

finalization
  UnregisterClass(TDM_EE_AUTOLABOR_DISTRIBUTION);

end.



