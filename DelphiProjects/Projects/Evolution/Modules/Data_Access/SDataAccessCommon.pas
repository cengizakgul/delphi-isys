// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDataAccessCommon;

interface

uses
  DB, SysUtils, EvUtils, EvTypes, EvExceptions;

procedure PayrollBeforeInsert(DataSet: TDataSet);
procedure PayrollBeforeEdit(DataSet: TDataSet);
procedure PayrollBeforeDelete(DataSet: TDataSet);
function GetEDTypeDescription(EDType: string): string;

implementation

uses
  SDataStructure, EvConsts, SFieldCodeValues,  SSecurityInterface, EvContext;

procedure PayrollBeforeInsert(DataSet: TDataSet);
begin
  if not ctx_DataAccess.PayrollLocked then
    Exit;

  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED)
  or (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
    raise EUpdateError.CreateHelp('You can not change processed or voided payroll!', IDH_ConsistencyViolation);

  if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL) and
     (DataSet.Name <> 'PR') then
    raise EUpdateError.CreateHelp('You can not change reversal payroll!', IDH_ConsistencyViolation);

  if ctx_DataAccess.Validate and ((DataSet.Name = 'PR_CHECK_LINES') or (DataSet.Name = 'PR_CHECK_STATES')
  or (DataSet.Name = 'PR_CHECK_SUI') or (DataSet.Name = 'PR_CHECK_LOCALS') or (DataSet.Name = 'PR_CHECK_LINE_LOCALS')) then
  begin
    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
      raise EUpdateError.CreateHelp('You can not change void check!', IDH_ConsistencyViolation);
    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_MANUAL then
      VerifyManualCheckChange(DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').AsString, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
  end;
  if ctx_DataAccess.Validate and (DataSet.Name = 'PR_CHECK_LINES') then
  begin
    if DM_PAYROLL.PR.PAYROLL_TYPE.Value = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT then
      raise EUpdateError.CreateHelp('You can not add check lines to a QEC payroll', IDH_ConsistencyViolation);
  end
end;

procedure PayrollBeforeEdit(DataSet: TDataSet);
begin
  if not ctx_DataAccess.PayrollLocked then
    Exit;

  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED)
  or (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
    raise EUpdateError.CreateHelp('You can not change processed or voided payroll!', IDH_ConsistencyViolation);

  if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL) and
     (DataSet.Name <> 'PR') then
    raise EUpdateError.CreateHelp('You can not change reversal payroll!', IDH_ConsistencyViolation);

  if ctx_DataAccess.Validate and ((DataSet.Name = 'PR_CHECK') or (DataSet.Name = 'PR_CHECK_LINES') or (DataSet.Name = 'PR_CHECK_STATES')
  or (DataSet.Name = 'PR_CHECK_SUI') or (DataSet.Name = 'PR_CHECK_LOCALS') or (DataSet.Name = 'PR_CHECK_LINE_LOCALS')) then
  begin
    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
      raise EUpdateError.CreateHelp('You can not change void check!', IDH_ConsistencyViolation);
    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_MANUAL then
      VerifyManualCheckChange(DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').AsString, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
  end;
  if ctx_DataAccess.Validate and (DataSet.Name = 'PR_CHECK_LINES') then
    if DM_PAYROLL.PR.PAYROLL_TYPE.Value = PAYROLL_TYPE_IMPORT then
      raise EUpdateError.CreateHelp('You can not edit check lines in Import type payroll', IDH_ConsistencyViolation);

  if ctx_DataAccess.Validate and (DataSet.Name = 'PR_CHECK_LINES') then
  begin
    if DM_PAYROLL.PR.PAYROLL_TYPE.Value = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT then
      raise EUpdateError.CreateHelp('You can not edit check lines to a QEC payroll', IDH_ConsistencyViolation);
  end
end;

procedure PayrollBeforeDelete(DataSet: TDataSet);
var
  Nbr: Integer;
begin
  if ctx_DataAccess.Validate and ((DataSet.Name = 'PR') or (DataSet.Name = 'PR_BATCH')) then
    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') <> stEnabled then
    begin
      if DM_PAYROLL.PR_CHECK.Active then
        Nbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger
      else
        Nbr := 0;
      DM_PAYROLL.PR_CHECK.DisableControls;
      try
        if DataSet.Name = 'PR' then
          DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR = ' + DataSet.FieldByName('PR_NBR').AsString)
        else
          DM_PAYROLL.PR_CHECK.DataRequired('PR_BATCH_NBR = ' + DataSet.FieldByName('PR_BATCH_NBR').AsString);
        DM_PAYROLL.PR_CHECK.First;
        while not DM_PAYROLL.PR_CHECK.Eof do
        begin
          if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_MANUAL then
            VerifyManualCheckChange(DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').AsString, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
          DM_PAYROLL.PR_CHECK.Next;
        end;
      finally
        DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', Nbr, []);
        DM_PAYROLL.PR_CHECK.EnableControls;
      end;
    end;

  if not ctx_DataAccess.PayrollLocked then
    Exit;

  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED)
  or (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
    raise EUpdateError.CreateHelp('You can not delete from processed or voided payroll!', IDH_ConsistencyViolation);

  if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL) and
     (DataSet.Name <> 'PR') then
    raise EUpdateError.CreateHelp('You can not delete from reversal payroll!', IDH_ConsistencyViolation);

  if ctx_DataAccess.Validate and ((DataSet.Name = 'PR_CHECK') or (DataSet.Name = 'PR_CHECK_LINES') or (DataSet.Name = 'PR_CHECK_STATES')
  or (DataSet.Name = 'PR_CHECK_SUI') or (DataSet.Name = 'PR_CHECK_LOCALS') or (DataSet.Name = 'PR_CHECK_LINE_LOCALS')) then
  begin
    if (DataSet.Name <> 'PR_CHECK') and (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID)
    and (not ctx_DataAccess.DeletingVoidCheck) then
      raise EUpdateError.CreateHelp('You can not delete from void check!', IDH_ConsistencyViolation);
    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') <> stEnabled then
    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_MANUAL then
      VerifyManualCheckChange(DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').AsString, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
  end;
end;

function GetEDTypeDescription(EDType: string): string;
var
  i: Integer;
begin
  Result := '';
  i := Pred(Pos(#9 + EDType + #13, ED_ComboBoxChoices + #13));
  if i > 0 then
    repeat
      Result := ED_ComboBoxChoices[i] + Result;
      Dec(i);
    until (i = 0) or (ED_ComboBoxChoices[i] = #13);
end;

end.
