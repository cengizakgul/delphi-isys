// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_BANKS;

interface

uses
  SDM_ddTable, SDataDictBureau,
   Db, Classes,  Forms, SysUtils, Variants, EvExceptions,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses;

type
  TDM_SB_BANKS = class(TDM_ddTable)
    SB_BANKS: TSB_BANKS;
    SB_BANKSABA_NUMBER: TStringField;
    SB_BANKSNAME: TStringField;
    SB_BANKSCITY: TStringField;
    SB_BANKSSTATE: TStringField;
    SB_BANKSSB_BANKS_NBR: TIntegerField;
    SB_BANKSADDRESS1: TStringField;
    SB_BANKSADDRESS2: TStringField;
    SB_BANKSZIP_CODE: TStringField;
    SB_BANKSCONTACT1: TStringField;
    SB_BANKSPHONE1: TStringField;
    SB_BANKSDESCRIPTION1: TStringField;
    SB_BANKSCONTACT2: TStringField;
    SB_BANKSPHONE2: TStringField;
    SB_BANKSDESCRIPTION2: TStringField;
    SB_BANKSFAX: TStringField;
    SB_BANKSFAX_DESCRIPTION: TStringField;
    SB_BANKSE_MAIL_ADDRESS: TStringField;
    SB_BANKSTOP_ABA_NUMBER: TStringField;
    SB_BANKSBOTTOM_ABA_NUMBER: TStringField;
    SB_BANKSADDENDA: TStringField;
    SB_BANKSCHECK_TEMPLATE: TBlobField;
    SB_BANKSUSE_CHECK_TEMPLATE: TStringField;
    SB_BANKSMICR_ACCOUNT_START_POSITION: TIntegerField;
    SB_BANKSMICR_CHECK_NUMBER_START_POSITN: TIntegerField;
    SB_BANKSFILLER: TStringField;
    SB_BANKSPRINT_NAME: TStringField;
    SB_BANKSBRANCH_IDENTIFIER: TStringField;
    SB_BANKSCtsId: TStringField;
    procedure SB_BANKSBeforePost(DataSet: TDataSet);
    procedure SB_BANKSCalcFields(DataSet: TDataSet);
    procedure SB_BANKSCtsIdValidate(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_SB_BANKS: TDM_SB_BANKS;

implementation

uses EvUtils, EvTypes;
{$R *.DFM}

procedure TDM_SB_BANKS.SB_BANKSBeforePost(DataSet: TDataSet);
var
  L : integer;
begin
//SEG BEGIN
  L := Length(VarToStr(DataSet['ABA_NUMBER']));
  if (L <> 9) and (L <> 0) then
    raise EUpdateError.CreateHelp('The ABA Number must have 9 digits', IDH_ConsistencyViolation);

  if (VarToStr(DataSet['ABA_NUMBER']) <> '') and
     (HashTotalABAOk(VarToStr(DataSet['ABA_NUMBER'])) <> 0) then
    raise EInvalidHashTotal.CreateHelp('Invalid Hash Total of ABA_NUMBER', IDH_ConsistencyViolation);
//SEG END
end;

procedure TDM_SB_BANKS.SB_BANKSCalcFields(DataSet: TDataSet);
begin
  SB_BANKSCtsId.AsString := ExtractFromFiller(SB_BANKSFiller.AsString, 2, 6);
end;

procedure TDM_SB_BANKS.SB_BANKSCtsIdValidate(Sender: TField);
begin
  if Sender.AsString <> ExtractFromFiller(SB_BANKS.FieldByName('Filler').AsString, 2, 6) then
    SB_BANKS['Filler'] := PutIntoFiller(SB_BANKS.FieldByName('Filler').AsString, Sender.AsString, 2, 6);
end;

initialization
  RegisterClass(TDM_SB_BANKS);

finalization
  UnregisterClass(TDM_SB_BANKS);

end.
