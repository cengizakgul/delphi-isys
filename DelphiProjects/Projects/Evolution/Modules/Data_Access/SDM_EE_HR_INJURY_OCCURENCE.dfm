inherited DM_EE_HR_INJURY_OCCURRENCE: TDM_EE_HR_INJURY_OCCURRENCE
  Left = 77
  Top = 400
  Height = 540
  Width = 783
  object EE_HR_INJURY_OCCURRENCE: TEE_HR_INJURY_OCCURRENCE
    ProviderName = 'EE_HR_INJURY_OCCURRENCE_PROV'
    PictureMasks.Strings = (
      'STATE'#9'*{&,@}'#9'T'#9'T')
    Left = 104
    Top = 40
    object EE_HR_INJURY_OCCURRENCECASE_NUMBER: TStringField
      DisplayWidth = 15
      FieldName = 'CASE_NUMBER'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."CASE_NUMBER"'
      Required = True
      Size = 15
    end
    object EE_HR_INJURY_OCCURRENCECASE_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'CASE_STATUS'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."CASE_STATUS"'
      Required = True
      Size = 1
    end
    object EE_HR_INJURY_OCCURRENCECAUSE: TBlobField
      DisplayWidth = 10
      FieldName = 'CAUSE'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."CAUSE"'
      Size = 8
    end
    object EE_HR_INJURY_OCCURRENCECO_WORKERS_COMP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_WORKERS_COMP_NBR'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."CO_WORKERS_COMP_NBR"'
    end
    object EE_HR_INJURY_OCCURRENCEDATE_FILED: TDateField
      DisplayWidth = 10
      FieldName = 'DATE_FILED'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."DATE_FILED"'
    end
    object EE_HR_INJURY_OCCURRENCEDAYS_AWAY: TIntegerField
      DisplayWidth = 10
      FieldName = 'DAYS_AWAY'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."DAYS_AWAY"'
    end
    object EE_HR_INJURY_OCCURRENCEDAYS_RESTRICTED: TIntegerField
      DisplayWidth = 10
      FieldName = 'DAYS_RESTRICTED'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."DAYS_RESTRICTED"'
    end
    object EE_HR_INJURY_OCCURRENCEDEATH: TStringField
      DisplayWidth = 1
      FieldName = 'DEATH'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."DEATH"'
      Size = 1
    end
    object EE_HR_INJURY_OCCURRENCEEE_HR_INJURY_OCCURRENCE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_HR_INJURY_OCCURRENCE_NBR'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."EE_HR_INJURY_OCCURRENCE_NBR"'
      Required = True
    end
    object EE_HR_INJURY_OCCURRENCEEE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_NBR'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."EE_NBR"'
      Required = True
    end
    object EE_HR_INJURY_OCCURRENCEFIRST_AID: TStringField
      DisplayWidth = 1
      FieldName = 'FIRST_AID'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."FIRST_AID"'
      Size = 1
    end
    object EE_HR_INJURY_OCCURRENCELABOR_COST: TFloatField
      DisplayWidth = 10
      FieldName = 'LABOR_COST'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."LABOR_COST"'
      DisplayFormat = '#,##0.00'
    end
    object EE_HR_INJURY_OCCURRENCEMEDICAL_COST: TFloatField
      DisplayWidth = 10
      FieldName = 'MEDICAL_COST'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."MEDICAL_COST"'
      DisplayFormat = '#,##0.00'
    end
    object EE_HR_INJURY_OCCURRENCERESTRICTIONS: TBlobField
      DisplayWidth = 10
      FieldName = 'RESTRICTIONS'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."RESTRICTIONS"'
      Size = 8
    end
    object EE_HR_INJURY_OCCURRENCESPECIAL_NOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'SPECIAL_NOTES'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."SPECIAL_NOTES"'
      Size = 8
    end
    object EE_HR_INJURY_OCCURRENCEWC_CLAIM_NUMBER: TStringField
      DisplayWidth = 10
      FieldName = 'WC_CLAIM_NUMBER'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."WC_CLAIM_NUMBER"'
      Size = 15
    end
    object EE_HR_INJURY_OCCURRENCEWorkersComp: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'WorkersComp'
      LookupDataSet = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
      LookupKeyFields = 'CO_WORKERS_COMP_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_WORKERS_COMP_NBR'
      Size = 40
      Lookup = True
    end
    object EE_HR_INJURY_OCCURRENCEAnatomicCode: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'AnatomicCode'
      LookupDataSet = DM_SY_HR_OSHA_ANATOMIC_CODES.SY_HR_OSHA_ANATOMIC_CODES
      LookupKeyFields = 'SY_HR_OSHA_ANATOMIC_CODES_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SY_HR_OSHA_ANATOMIC_CODES_NBR'
      Size = 40
      Lookup = True
    end
    object EE_HR_INJURY_OCCURRENCEInjuryCode: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'InjuryCode'
      LookupDataSet = DM_SY_HR_INJURY_CODES.SY_HR_INJURY_CODES
      LookupKeyFields = 'SY_HR_INJURY_CODES_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SY_HR_INJURY_CODES_NBR'
      Size = 40
      Lookup = True
    end
    object EE_HR_INJURY_OCCURRENCESY_HR_INJURY_CODES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_HR_INJURY_CODES_NBR'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."SY_HR_INJURY_CODES_NBR"'
    end
    object EE_HR_INJURY_OCCURRENCESY_HR_OSHA_ANATOMIC_CODES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_HR_OSHA_ANATOMIC_CODES_NBR'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."SY_HR_OSHA_ANATOMIC_CODES_NBR"'
    end
    object EE_HR_INJURY_OCCURRENCEPHYSICIAN_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'PHYSICIAN_NAME'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."PHYSICIAN_NAME"'
      Size = 40
    end
    object EE_HR_INJURY_OCCURRENCEHOSPITAL_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'HOSPITAL_NAME'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."HOSPITAL_NAME"'
      Size = 40
    end
    object EE_HR_INJURY_OCCURRENCEADDRESS1: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS1'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."ADDRESS1"'
      Size = 30
    end
    object EE_HR_INJURY_OCCURRENCEADDRESS2: TStringField
      DisplayWidth = 30
      FieldName = 'ADDRESS2'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."ADDRESS2"'
      Size = 30
    end
    object EE_HR_INJURY_OCCURRENCECITY: TStringField
      DisplayWidth = 20
      FieldName = 'CITY'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."CITY"'
    end
    object EE_HR_INJURY_OCCURRENCESTATE: TStringField
      DisplayWidth = 2
      FieldName = 'STATE'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."STATE"'
      FixedChar = True
      Size = 2
    end
    object EE_HR_INJURY_OCCURRENCEZIP_CODE: TStringField
      DisplayWidth = 10
      FieldName = 'ZIP_CODE'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."ZIP_CODE"'
      Size = 10
    end
    object EE_HR_INJURY_OCCURRENCEEMERGENCY_ROOM: TStringField
      DisplayWidth = 1
      FieldName = 'EMERGENCY_ROOM'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."EMERGENCY_ROOM"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EE_HR_INJURY_OCCURRENCEOVERNIGHT_HOSPITALIZATION: TStringField
      DisplayWidth = 1
      FieldName = 'OVERNIGHT_HOSPITALIZATION'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."OVERNIGHT_HOSPITALIZATION"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EE_HR_INJURY_OCCURRENCEEMPLOYEE_ACTIVITY: TStringField
      DisplayWidth = 80
      FieldName = 'EMPLOYEE_ACTIVITY'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."EMPLOYEE_ACTIVITY"'
      Size = 80
    end
    object EE_HR_INJURY_OCCURRENCEOBJECT_DESCRIPTION: TStringField
      DisplayWidth = 80
      FieldName = 'OBJECT_DESCRIPTION'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."OBJECT_DESCRIPTION"'
      Size = 80
    end
    object EE_HR_INJURY_OCCURRENCEDATE_OF_DEATH: TDateField
      DisplayWidth = 10
      FieldName = 'DATE_OF_DEATH'
      Origin = '"EE_HR_INJURY_OCCURRENCE_HIST"."DATE_OF_DEATH"'
    end
    object EE_HR_INJURY_OCCURRENCEDATE_OCCURRED: TDateTimeField
      DisplayWidth = 18
      FieldName = 'DATE_OCCURRED'
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 272
    Top = 24
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 264
    Top = 128
  end
end
