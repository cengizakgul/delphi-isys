// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_DIVISION_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_DIVISION_LOCALS = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    CO_DIVISION_LOCALS: TCO_DIVISION_LOCALS;
    CO_DIVISION_LOCALSCO_DIVISION_LOCALS_NBR: TIntegerField;
    CO_DIVISION_LOCALSCO_DIVISION_NBR: TIntegerField;
    CO_DIVISION_LOCALSCO_LOCAL_TAX_NBR: TIntegerField;
    CO_DIVISION_LOCALSLocalNsme: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_DIVISION_LOCALS);

finalization
  UnregisterClass(TDM_CO_DIVISION_LOCALS);

end.
