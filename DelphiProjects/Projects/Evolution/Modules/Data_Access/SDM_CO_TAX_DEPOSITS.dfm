inherited DM_CO_TAX_DEPOSITS: TDM_CO_TAX_DEPOSITS
  OldCreateOrder = True
  Height = 217
  Width = 132
  object CO_TAX_DEPOSITS: TCO_TAX_DEPOSITS
    ProviderName = 'CO_TAX_DEPOSITS_PROV'
    BeforePost = CO_TAX_DEPOSITSBeforePost
    BeforeDelete = CO_TAX_DEPOSITSBeforeDelete
    OnCalcFields = CO_TAX_DEPOSITSCalcFields
    Left = 65
    Top = 15
    object CO_TAX_DEPOSITSTAX_PAYMENT_REFERENCE_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'TAX_PAYMENT_REFERENCE_NUMBER'
    end
    object CO_TAX_DEPOSITSSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object CO_TAX_DEPOSITSPayrollDescription: TStringField
      DisplayWidth = 40
      FieldKind = fkCalculated
      FieldName = 'PayrollDescription'
      Size = 40
      Calculated = True
    end
    object CO_TAX_DEPOSITSPR_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_NBR'
      Visible = False
    end
    object CO_TAX_DEPOSITSCO_TAX_DEPOSITS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_DEPOSITS_NBR'
      Visible = False
    end
    object CO_TAX_DEPOSITSCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Visible = False
    end
    object CO_TAX_DEPOSITSFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object CO_TAX_DEPOSITSSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
    object CO_TAX_DEPOSITSSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object CO_TAX_DEPOSITSDEPOSIT_TYPE: TStringField
      FieldName = 'DEPOSIT_TYPE'
      Size = 1
    end
    object CO_TAX_DEPOSITSBarAmount: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'BarAmount'
      Calculated = True
    end
  end
end
