// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR_MISCELLANEOUS_CHECKS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, EvContext, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, evTypes, EvUIComponents, EvClientDataSet;

type
  TDM_PR_MISCELLANEOUS_CHECKS = class(TDM_ddTable)
    DM_PAYROLL: TDM_PAYROLL;
    PR_MISCELLANEOUS_CHECKS: TPR_MISCELLANEOUS_CHECKS;
    PR_MISCELLANEOUS_CHECKSPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField;
    PR_MISCELLANEOUS_CHECKSPR_NBR: TIntegerField;
    PR_MISCELLANEOUS_CHECKSMISCELLANEOUS_CHECK_TYPE: TStringField;
    PR_MISCELLANEOUS_CHECKSMISCELLANEOUS_CHECK_AMOUNT: TFloatField;
    PR_MISCELLANEOUS_CHECKSCUSTOM_PR_BANK_ACCT_NUMBER: TStringField;
    PR_MISCELLANEOUS_CHECKSPAYMENT_SERIAL_NUMBER: TIntegerField;
    PR_MISCELLANEOUS_CHECKSCHECK_STATUS: TStringField;
    PR_MISCELLANEOUS_CHECKSSTATUS_CHANGE_DATE: TDateTimeField;
    PR_MISCELLANEOUS_CHECKSEFTPS_TAX_TYPE: TStringField;
    PR_MISCELLANEOUS_CHECKSFILLER: TStringField;
    PR_MISCELLANEOUS_CHECKSCL_AGENCY_NBR: TIntegerField;
    PR_MISCELLANEOUS_CHECKSOVERRIDE_INFORMATION: TBlobField;
    PR_MISCELLANEOUS_CHECKSCO_TAX_DEPOSITS_NBR: TIntegerField;
    PR_MISCELLANEOUS_CHECKSSY_GLOBAL_AGENCY_NBR: TIntegerField;
    PR_MISCELLANEOUS_CHECKSsb_name: TStringField;
    DM_CLIENT: TDM_CLIENT;
    PR_MISCELLANEOUS_CHECKSSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    PR_MISCELLANEOUS_CHECKSGl_Field_Pffice_State: TStringField;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    PR_MISCELLANEOUS_CHECKSTaxAgencyNAme: TStringField;
    PR_MISCELLANEOUS_CHECKSTAX_COUPON_SY_REPORTS_NBR: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure PR_MISCELLANEOUS_CHECKSBeforeDelete(DataSet: TDataSet);
    procedure PR_MISCELLANEOUS_CHECKSBeforeEdit(DataSet: TDataSet);
    procedure PR_MISCELLANEOUS_CHECKSBeforeInsert(DataSet: TDataSet);
    procedure PR_MISCELLANEOUS_CHECKSBeforePost(DataSet: TDataSet);
    procedure PR_MISCELLANEOUS_CHECKSAfterDelete(DataSet: TDataSet);
  private
    procedure CreatePRMiscCheckDetails(DataSet: TDataSet);
  end;

var
  DM_PR_MISCELLANEOUS_CHECKS: TDM_PR_MISCELLANEOUS_CHECKS;

implementation

uses
  EvUtils, EvConsts, SDataAccessCommon, SDM_PR_CHECK;

{$R *.DFM}

procedure TDM_PR_MISCELLANEOUS_CHECKS.CreatePRMiscCheckDetails(DataSet: TDataSet);
var
  CheckToVoid, BankAcctNbr, SbBankNbr: Integer;
  TempMiscCheck: TevClientDataSet;

  procedure VoidAgencyCheck;
  var
    TemtDataSet: TevClientDataSet;
  begin
    DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR=' + IntToStr(CheckToVoid));
    ctx_DataAccess.VoidingAgencyCheck := True;
    TemtDataSet := TevClientDataSet.Create(Nil);
    try
      with DM_PAYROLL.PR_CHECK_LINES do
      begin
        First;
        while not EOF do
        begin
          Edit;
          FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_PENDING;
          FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value := Null;
          Post;
          Next;
        end;
      end;
      // Unlock payrolls
      with TExecDSWrapper.Create('SelectPayrollsForAgencyCheck') do
      begin
        SetParam('PrNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').Value);
        SetParam('MiscCheckNbr', CheckToVoid);
        ctx_DataAccess.GetCustomData(TemtDataSet, AsVariant);
      end;
      with TemtDataSet do
      begin
        First;
        while not EOF do
        begin
          ctx_DataAccess.SetPayrollLockInTransaction(FieldByName('PR_NBR').AsInteger, True);
          Next;
        end;
      end;
      try
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINES]);
      finally
        // Lock payrolls
        with TExecDSWrapper.Create('SelectPayrollsForAgencyCheck') do
        begin
          SetParam('PrNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').Value);
          SetParam('MiscCheckNbr', CheckToVoid);
          ctx_DataAccess.GetCustomData(TemtDataSet, AsVariant);
        end;
        with TemtDataSet do
        begin
          First;
          while not EOF do
          begin
            ctx_DataAccess.SetPayrollLockInTransaction(FieldByName('PR_NBR').AsInteger, False);
            Next;
          end;
        end;
      end;
    finally
      TemtDataSet.Free;
      ctx_DataAccess.VoidingAgencyCheck := False;
    end;
  end;

  procedure VoidBillingCheck;
  var
    LocalTransactionManager: TTransactionManager;
  begin
    DM_COMPANY.CO_BILLING_HISTORY.DataRequired('PR_NBR=' + TempMiscCheck.FieldByName('PR_NBR').AsString);
    LocalTransactionManager := TTransactionManager.Create(nil);
    try
      LocalTransactionManager.Initialize([DM_PAYROLL.PR_CHECK_LINES]);
      with DM_COMPANY.CO_BILLING_HISTORY do
      begin
        First;
        while not EOF do
        begin
          Edit;
          FieldByName('PR_NBR').Value := Null;
          FieldByName('CHECK_SERIAL_NUMBER').Value := Null;
          FieldByName('BILLING_CO_BANK_ACCOUNT_NBR').Value := Null;
          FieldByName('STATUS').Value := CO_BILLING_STATUS_IGNORE;
          Post;
          Next;
        end;
      end;
      LocalTransactionManager.ApplyUpdates;
    finally
      LocalTransactionManager.Free;
    end;
  end;

begin
  with ctx_PayrollCalculation do
  begin
    DataSet.FieldByName('PR_NBR').Value := DM_PAYROLL.PR.FieldByName('PR_NBR').Value;
    if VarIsNull(DataSet.FieldByName('CHECK_STATUS').Value) then
    begin
      DataSet.FieldByName('CHECK_STATUS').Value := MISC_CHECK_STATUS_OUTSTANDING;
    end;

    if DataSet.FieldByName('STATUS_CHANGE_DATE').IsNull then DataSet.FieldByName('STATUS_CHANGE_DATE').Value := SysTime;

    if DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY_VOID,
    MISC_CHECK_TYPE_TAX_VOID, MISC_CHECK_TYPE_BILLING_VOID] then
    begin
      CheckToVoid := StrToInt(ExtractFromFiller(ConvertNull(DataSet.FieldByName('FILLER').Value, ''), 1, 8));
      TempMiscCheck := TevClientDataSet.Create(Nil);
      try
        TempMiscCheck.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
        with TExecDSWrapper.Create('pr_miscellaneous_checks;GenericSelectCurrentNBR') do
        begin
          SetMacro('Columns', '*');
          SetMacro('TableName', 'PR_MISCELLANEOUS_CHECKS');
          SetMacro('NbrField', 'PR_MISCELLANEOUS_CHECKS');
          SetParam('RecordNbr', CheckToVoid);
          TempMiscCheck.DataRequest(AsVariant);
        end;
        TempMiscCheck.Open;
        Assert(TempMiscCheck.RecordCount=1);
  // Copy Fields
        DataSet.FieldByName('CL_AGENCY_NBR').Value := TempMiscCheck.FieldByName('CL_AGENCY_NBR').Value;
        DataSet.FieldByName('SY_GLOBAL_AGENCY_NBR').Value := TempMiscCheck.FieldByName('SY_GLOBAL_AGENCY_NBR').Value;
        DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').Value := TempMiscCheck.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
        DataSet.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value := (-1) * ConvertNull(TempMiscCheck.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value, 0);
        DataSet.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := TempMiscCheck.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value;
        if TempMiscCheck.FieldByName('PAYMENT_SERIAL_NUMBER').AsInteger > 0 then
          DataSet.FieldByName('PAYMENT_SERIAL_NUMBER').Value := TempMiscCheck.FieldByName('PAYMENT_SERIAL_NUMBER').Value;
        DataSet.FieldByName('EFTPS_TAX_TYPE').Value := TempMiscCheck.FieldByName('EFTPS_TAX_TYPE').Value;

  // Mark prior check as void
        if TempMiscCheck.FieldByName('CHECK_STATUS').AsString <> MISC_CHECK_STATUS_VOID then
        begin
         ctx_DataAccess.StartNestedTransaction([dbtClient]);
         try
          case DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] of
            MISC_CHECK_TYPE_AGENCY_VOID:
              VoidAgencyCheck;
            MISC_CHECK_TYPE_TAX_VOID:
              if not DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
              begin
                ctx_DataAccess.UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_FED_TAX_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
                ctx_DataAccess.UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_STATE_TAX_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
                ctx_DataAccess.UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_SUI_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
                ctx_DataAccess.UnlinkLiabilitiesFromDeposit(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
              end;
            MISC_CHECK_TYPE_BILLING_VOID:
              VoidBillingCheck;
          end;

          ctx_DataAccess.SetPayrollLockInTransaction(TempMiscCheck.FieldByName('PR_NBR').AsInteger, True);
          try
            TempMiscCheck.Edit;
            TempMiscCheck.FieldByName('CHECK_STATUS').AsString := MISC_CHECK_STATUS_VOID;
            TempMiscCheck.Post;
            ctx_DataAccess.PostDataSets([TempMiscCheck]);
          finally
            ctx_DataAccess.SetPayrollLockInTransaction(TempMiscCheck.FieldByName('PR_NBR').AsInteger, False);
          end;

          ctx_DataAccess.CommitNestedTransaction;
         except
          ctx_DataAccess.RollbackNestedTransaction;
         raise;
         end;

        end;
      finally
        TempMiscCheck.Free;
      end;
// Clear Bank Account Register
      {if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.State <> dsEdit then
      begin
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR=' + IntToStr(CheckToVoid));
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
        while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
        begin
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Cleared;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CLEARED_DATE').Value := Now;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
        end;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
      end;}
    end;

    if DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').IsNull then
      DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_AGENCY;

//    if ConvertNull(DataSet.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value, '') = '' then
    begin
      if GetBankAccountNumberForMisc(PR_MISCELLANEOUS_CHECKS, BankAcctNbr) then
      begin
        ctx_DBAccess.EnableReadTransaction;
        try
          DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
        finally
          ctx_DBAccess.DisableReadTransaction;
        end;
        if ConvertNull(DataSet.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value, '') = '' then
          DataSet.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
        SbBankNbr := VarToInt(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', BankAcctNbr, 'SB_BANKS_NBR'));
      end
      else
      begin
        DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
        if ConvertNull(DataSet.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value, '') = '' then
          DataSet.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value :=
            DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'CUSTOM_BANK_ACCOUNT_NUMBER');
        SbBankNbr := VarToInt(DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', BankAcctNbr, 'SB_BANKS_NBR'));
      end;
      DM_SERVICE_BUREAU.SB_BANKS.Activate;
      if ConvertNull(DataSet.FieldByName('ABA_NUMBER').Value, '') = '' then
        DataSet.FieldByName('ABA_NUMBER').Value := DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SbBankNbr, 'ABA_NUMBER');
    end;

    if DataSet.FieldByName('PAYMENT_SERIAL_NUMBER').IsNull then
      DataSet.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GetNextPaymentSerialNbr;
  end;
end;

procedure TDM_PR_MISCELLANEOUS_CHECKS.DataModuleCreate(Sender: TObject);
begin
  ctx_DataAccess.VoidingAgencyCheck := False;
end;

procedure TDM_PR_MISCELLANEOUS_CHECKS.PR_MISCELLANEOUS_CHECKSBeforeDelete(
  DataSet: TDataSet);
var
  VoidCheckNbr: String;
  PrNbr: Integer;
  AllOK: Boolean;
begin
  PayrollBeforeDelete(DataSet);

  if DataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY_VOID, MISC_CHECK_TYPE_TAX_VOID,
  MISC_CHECK_TYPE_BILLING_VOID] then
  begin
    VoidCheckNbr := Trim(ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 8));
    if VoidCheckNbr <> '' then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      PrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
      if Active then
        Close;
      with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
      begin
        SetMacro('NbrField', 'PR_MISCELLANEOUS_CHECKS');
        SetMacro('Columns', '*');
        SetMacro('TableName', 'PR_MISCELLANEOUS_CHECKS');
        SetParam('RecordNbr', VoidCheckNbr);
        DataRequest(AsVariant);
      end;
      Open;
      First;
      DM_PAYROLL.PR.Locate('PR_NBR', FieldByName('PR_NBR').Value, []);
      ctx_DataAccess.UnlockPR;
      AllOK := False;
      try
        if Active then
          Close;
        with TExecDSWrapper.Create('PR_MISCELLANEOUS_CHECKS;GenericSelectCurrentNBR') do
        begin
          SetMacro('NbrField', 'PR_MISCELLANEOUS_CHECKS');
          SetMacro('Columns', '*');
          SetMacro('TableName', 'PR_MISCELLANEOUS_CHECKS');
          SetParam('RecordNbr', VoidCheckNbr);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        Edit;
        FieldByName('CHECK_STATUS').AsString := MISC_CHECK_STATUS_OUTSTANDING;
        Post;
        ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
        AllOK := True;
      finally
        ctx_DataAccess.LockPR(AllOK);
        DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []);
      end;
// Clear Bank Account Register
      {DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR=' + VoidCheckNbr);
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
      while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
      begin
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER])};
    end;
  end;
end;

procedure TDM_PR_MISCELLANEOUS_CHECKS.PR_MISCELLANEOUS_CHECKSBeforeEdit(
  DataSet: TDataSet);
begin
  PayrollBeforeEdit(DataSet);
end;

procedure TDM_PR_MISCELLANEOUS_CHECKS.PR_MISCELLANEOUS_CHECKSBeforeInsert(
  DataSet: TDataSet);
begin
  PayrollBeforeInsert(DataSet);
end;

procedure TDM_PR_MISCELLANEOUS_CHECKS.PR_MISCELLANEOUS_CHECKSBeforePost(
  DataSet: TDataSet);
begin
  if ctx_DataAccess.SkipPrCheckPostCheck then
    Exit;
  DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DisableControls;
  DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.LookupsEnabled := False;
  DM_PAYROLL.PR.DisableControls;
  DM_PAYROLL.PR.LookupsEnabled := False;
  try
    CreatePRMiscCheckDetails(DataSet);
  finally
    DM_PAYROLL.PR.EnableControls;
    DM_PAYROLL.PR.LookupsEnabled := True;
    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.EnableControls;
    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.LookupsEnabled := True;
  end;
end;

procedure TDM_PR_MISCELLANEOUS_CHECKS.PR_MISCELLANEOUS_CHECKSAfterDelete(
  DataSet: TDataSet);
begin
  if ctx_DataAccess.CUSTOM_VIEW.ChangeCount > 0 then
    ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
end;

initialization
  RegisterClass(TDM_PR_MISCELLANEOUS_CHECKS);

finalization
  UnregisterClass(TDM_PR_MISCELLANEOUS_CHECKS);

end.
