inherited DM_PR_MISCELLANEOUS_CHECKS: TDM_PR_MISCELLANEOUS_CHECKS
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 377
  Top = 225
  Height = 479
  Width = 741
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 112
  end
  object PR_MISCELLANEOUS_CHECKS: TPR_MISCELLANEOUS_CHECKS
    ProviderName = 'PR_MISCELLANEOUS_CHECKS_PROV'
    BeforeInsert = PR_MISCELLANEOUS_CHECKSBeforeInsert
    BeforeEdit = PR_MISCELLANEOUS_CHECKSBeforeEdit
    BeforePost = PR_MISCELLANEOUS_CHECKSBeforePost
    BeforeDelete = PR_MISCELLANEOUS_CHECKSBeforeDelete
    AfterDelete = PR_MISCELLANEOUS_CHECKSAfterDelete
    Left = 208
    Top = 168
    object PR_MISCELLANEOUS_CHECKSPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField
      FieldName = 'PR_MISCELLANEOUS_CHECKS_NBR'
    end
    object PR_MISCELLANEOUS_CHECKSPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object PR_MISCELLANEOUS_CHECKSMISCELLANEOUS_CHECK_TYPE: TStringField
      FieldName = 'MISCELLANEOUS_CHECK_TYPE'
      Size = 1
    end
    object PR_MISCELLANEOUS_CHECKSMISCELLANEOUS_CHECK_AMOUNT: TFloatField
      FieldName = 'MISCELLANEOUS_CHECK_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object PR_MISCELLANEOUS_CHECKSCUSTOM_PR_BANK_ACCT_NUMBER: TStringField
      FieldName = 'CUSTOM_PR_BANK_ACCT_NUMBER'
    end
    object PR_MISCELLANEOUS_CHECKSPAYMENT_SERIAL_NUMBER: TIntegerField
      FieldName = 'PAYMENT_SERIAL_NUMBER'
    end
    object PR_MISCELLANEOUS_CHECKSCHECK_STATUS: TStringField
      FieldName = 'CHECK_STATUS'
      Size = 1
    end
    object PR_MISCELLANEOUS_CHECKSSTATUS_CHANGE_DATE: TDateTimeField
      FieldName = 'STATUS_CHANGE_DATE'
    end
    object PR_MISCELLANEOUS_CHECKSEFTPS_TAX_TYPE: TStringField
      FieldName = 'EFTPS_TAX_TYPE'
      Size = 1
    end
    object PR_MISCELLANEOUS_CHECKSFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object PR_MISCELLANEOUS_CHECKSCL_AGENCY_NBR: TIntegerField
      FieldName = 'CL_AGENCY_NBR'
    end
    object PR_MISCELLANEOUS_CHECKSOVERRIDE_INFORMATION: TBlobField
      FieldName = 'OVERRIDE_INFORMATION'
      Size = 8
    end
    object PR_MISCELLANEOUS_CHECKSCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object PR_MISCELLANEOUS_CHECKSSY_GLOBAL_AGENCY_NBR: TIntegerField
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
    end
    object PR_MISCELLANEOUS_CHECKSsb_name: TStringField
      FieldKind = fkLookup
      FieldName = 'sb_name'
      LookupDataSet = DM_CL_AGENCY.CL_AGENCY
      LookupKeyFields = 'CL_AGENCY_NBR'
      LookupResultField = 'Agency_Name'
      KeyFields = 'CL_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object PR_MISCELLANEOUS_CHECKSSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
    object PR_MISCELLANEOUS_CHECKSGl_Field_Pffice_State: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Gl_Field_Office_State'
      LookupDataSet = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
      LookupKeyFields = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
      Size = 2
    end
    object PR_MISCELLANEOUS_CHECKSTaxAgencyNAme: TStringField
      FieldKind = fkLookup
      FieldName = 'TaxAgencyNAme'
      LookupDataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
      LookupKeyFields = 'SY_GLOBAL_AGENCY_NBR'
      LookupResultField = 'AGENCY_NAME'
      KeyFields = 'SY_GLOBAL_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object PR_MISCELLANEOUS_CHECKSTAX_COUPON_SY_REPORTS_NBR: TIntegerField
      FieldName = 'TAX_COUPON_SY_REPORTS_NBR'
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 280
    Top = 112
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 366
    Top = 129
  end
end
