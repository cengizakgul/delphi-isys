// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictbureau,
  SDataStructure, EvContext;

type
  TDM_CL = class(TDM_ddTable)
    CL: TCL;
    CLACCOUNTANT_CONTACT: TStringField;
    CLADDRESS1: TStringField;
    CLADDRESS2: TStringField;
    CLAUTO_SAVE_MINUTES: TIntegerField;
    CLCITY: TStringField;
    CLCL_NBR: TIntegerField;
    CLCONTACT1: TStringField;
    CLCONTACT2: TStringField;
    CLCSR_SB_TEAM_NBR: TIntegerField;
    CLCUSTOMER_SERVICE_SB_USER_NBR: TIntegerField;
    CLCUSTOM_CLIENT_NUMBER: TStringField;
    CLDESCRIPTION1: TStringField;
    CLDESCRIPTION2: TStringField;
    CLENABLE_HR: TStringField;
    CLE_MAIL_ADDRESS1: TStringField;
    CLE_MAIL_ADDRESS2: TStringField;
    CLFAX1: TStringField;
    CLFAX1_DESCRIPTION: TStringField;
    CLFAX2: TStringField;
    CLFAX2_DESCRIPTION: TStringField;
    CLFILLER: TStringField;
    CLLEASING_COMPANY: TStringField;
    CLNAME: TStringField;
    CLPHONE1: TStringField;
    CLPHONE2: TStringField;
    CLPRINT_CPA: TStringField;
    CLRECIPROCATE_SUI: TStringField;
    CLSB_ACCOUNTANT_NUMBER: TIntegerField;
    CLSECURITY_FONT: TStringField;
    CLSTART_DATE: TDateField;
    CLSTATE: TStringField;
    CLTERMINATION_CODE: TStringField;
    CLTERMINATION_DATE: TDateField;
    CLTERMINATION_NOTES: TBlobField;
    CLZIP_CODE: TStringField;
    CLAGENCY_CHECK_MB_GROUP_NBR: TIntegerField;
    CLPR_CHECK_MB_GROUP_NBR: TIntegerField;
    CLPR_REPORT_MB_GROUP_NBR: TIntegerField;
    CLPR_REPORT_SECOND_MB_GROUP_NBR: TIntegerField;
    CLTAX_CHECK_MB_GROUP_NBR: TIntegerField;
    CLTAX_EE_RETURN_MB_GROUP_NBR: TIntegerField;
    CLTAX_RETURN_MB_GROUP_NBR: TIntegerField;
    CLTAX_RETURN_SECOND_MB_GROUP_NBR: TIntegerField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure CLNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

uses EvUtils;

procedure TDM_CL.CLNewRecord(DataSet: TDataSet);
begin
  CLAUTO_SAVE_MINUTES.AsInteger := DM_SERVICE_BUREAU.SB.AUTO_SAVE_MINUTES.AsInteger;
end;

initialization
  RegisterClass(TDM_CL);

finalization
  UnregisterClass(TDM_CL);

end.
