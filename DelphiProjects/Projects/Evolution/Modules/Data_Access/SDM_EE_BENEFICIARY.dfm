inherited DM_EE_BENEFICIARY: TDM_EE_BENEFICIARY
  OldCreateOrder = True
  Left = 410
  Top = 177
  Height = 540
  Width = 783
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 288
    Top = 48
  end
  object EE_BENEFICIARY: TEE_BENEFICIARY
    ProviderName = 'EE_BENEFICIARY_PROV'
    FieldDefs = <
      item
        Name = 'EE_BENEFICIARY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_BENEFITS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_PERSON_DEPENDENTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PERCENTAGE'
        DataType = ftSmallint
      end
      item
        Name = 'PRIMARY_BENEFICIARY'
        DataType = ftString
        Size = 1
      end>
    OnCalcFields = EE_BENEFICIARYCalcFields
    Left = 96
    Top = 56
    object EE_BENEFICIARYEE_BENEFICIARY_NBR: TIntegerField
      FieldName = 'EE_BENEFICIARY_NBR'
    end
    object EE_BENEFICIARYEE_BENEFITS_NBR: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object EE_BENEFICIARYCL_PERSON_DEPENDENTS_NBR: TIntegerField
      FieldName = 'CL_PERSON_DEPENDENTS_NBR'
    end
    object EE_BENEFICIARYPERCENTAGE: TSmallintField
      FieldName = 'PERCENTAGE'
    end
    object EE_BENEFICIARYPRIMARY_BENEFICIARY: TStringField
      FieldName = 'PRIMARY_BENEFICIARY'
      Size = 1
    end
    object EE_BENEFICIARYBENEFIT_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'BENEFIT_NAME'
      Size = 40
      Calculated = True
    end
    object EE_BENEFICIARYCATEGORY_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'CATEGORY_NAME'
      Size = 30
      Calculated = True
    end
    object EE_BENEFICIARYSTART_DATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'START_DATE'
      Calculated = True
    end
    object EE_BENEFICIARYFIRST_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'FIRST_NAME'
      LookupDataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
      LookupKeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      LookupResultField = 'FIRST_NAME'
      KeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      Lookup = True
    end
    object EE_BENEFICIARYLAST_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'LAST_NAME'
      LookupDataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
      LookupKeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      LookupResultField = 'LAST_NAME'
      KeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      Size = 30
      Lookup = True
    end
    object EE_BENEFICIARYRELATION_TYPE: TStringField
      FieldKind = fkLookup
      FieldName = 'RELATION_TYPE'
      LookupDataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
      LookupKeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      LookupResultField = 'RELATION_TYPE'
      KeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      Size = 1
      Lookup = True
    end
    object EE_BENEFICIARYDATE_OF_BIRTH: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'DATE_OF_BIRTH'
      LookupDataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
      LookupKeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      LookupResultField = 'DATE_OF_BIRTH'
      KeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      Lookup = True
    end
    object EE_BENEFICIARYSOCIAL_SECURITY_NUMBER: TStringField
      FieldKind = fkLookup
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      LookupDataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
      LookupKeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      LookupResultField = 'SOCIAL_SECURITY_NUMBER'
      KeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      Size = 11
      Lookup = True
    end
    object EE_BENEFICIARYPCP: TStringField
      FieldKind = fkLookup
      FieldName = 'PCP'
      LookupDataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
      LookupKeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      LookupResultField = 'PCP'
      KeyFields = 'CL_PERSON_DEPENDENTS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 280
    Top = 184
  end
end
