inherited DM_EE_HR_CAR: TDM_EE_HR_CAR
  OldCreateOrder = False
  Left = 290
  Top = 257
  Height = 540
  Width = 783
  object EE_HR_CAR: TEE_HR_CAR
    Aggregates = <>
    Params = <>
    ProviderName = 'EE_HR_CAR_PROV'
    OnCalcFields = EE_HR_CARCalcFields
    ValidateWithMask = True
    Left = 64
    Top = 64
    object EE_HR_CARCO_HR_CAR_NBR: TIntegerField
      FieldName = 'CO_HR_CAR_NBR'
    end
    object EE_HR_CAREE_HR_CAR_NBR: TIntegerField
      FieldName = 'EE_HR_CAR_NBR'
    end
    object EE_HR_CAREE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_HR_CARCarDescr: TStringField
      FieldKind = fkCalculated
      FieldName = 'CarDescr'
      Size = 40
      Calculated = True
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 184
    Top = 40
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 248
    Top = 40
  end
end
