// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_HR_PERSON_HANDICAPS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, SDataStructure, SDDClasses, SDataDictsystem, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  EvClientDataSet;

type
  TDM_CL_HR_PERSON_HANDICAPS = class(TDM_ddTable)
    CL_HR_PERSON_HANDICAPS: TCL_HR_PERSON_HANDICAPS;
    DM_SYSTEM_HR: TDM_SYSTEM_HR;
    CL_HR_PERSON_HANDICAPSCL_HR_PERSON_HANDICAPS_NBR: TIntegerField;
    CL_HR_PERSON_HANDICAPSCL_PERSON_NBR: TIntegerField;
    CL_HR_PERSON_HANDICAPSSY_HR_HANDICAPS_NBR: TIntegerField;
    CL_HR_PERSON_HANDICAPSHandicap: TStringField;
  private
  public
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass( TDM_CL_HR_PERSON_HANDICAPS );

finalization
  UnregisterClass( TDM_CL_HR_PERSON_HANDICAPS );

end.
 
