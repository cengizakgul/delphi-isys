// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_FED_TAX_LIABILITIES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   EvTypes, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvExceptions, EvContext,
  EvClientDataSet;

type
  TDM_CO_FED_TAX_LIABILITIES = class(TDM_ddTable)
    CO_FED_TAX_LIABILITIES: TCO_FED_TAX_LIABILITIES;
    CO_FED_TAX_LIABILITIESTYPE: TStringField;
    CO_FED_TAX_LIABILITIESAMOUNT: TFloatField;
    CO_FED_TAX_LIABILITIESCO_FED_TAX_LIABILITIES_NBR: TIntegerField;
    CO_FED_TAX_LIABILITIESCO_NBR: TIntegerField;
    CO_FED_TAX_LIABILITIESDUE_DATE: TDateField;
    CO_FED_TAX_LIABILITIESSTATUS: TStringField;
    CO_FED_TAX_LIABILITIESSTATUS_DATE: TDateTimeField;
    CO_FED_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField;
    CO_FED_TAX_LIABILITIESPR_NBR: TIntegerField;
    CO_FED_TAX_LIABILITIESCO_TAX_DEPOSITS_NBR: TIntegerField;
    CO_FED_TAX_LIABILITIESIMPOUND_CO_BANK_ACCT_REG_NBR: TIntegerField;
    CO_FED_TAX_LIABILITIESNOTES: TBlobField;
    CO_FED_TAX_LIABILITIESFILLER: TStringField;
    CO_FED_TAX_LIABILITIESTHIRD_PARTY: TStringField;
    CO_FED_TAX_LIABILITIESCHECK_DATE: TDateField;
    CO_FED_TAX_LIABILITIESIMPOUNDED: TStringField;
    CO_FED_TAX_LIABILITIESACH_KEY: TStringField;
    CO_FED_TAX_LIABILITIESPERIOD_BEGIN_DATE: TDateField;
    CO_FED_TAX_LIABILITIESPERIOD_END_DATE: TDateField;
    CO_FED_TAX_LIABILITIESADJ_PERIOD_END_DATE: TDateField;
    CO_FED_TAX_LIABILITIESCALC_PERIOD_END_DATE: TDateTimeField;
    procedure CO_FED_TAX_LIABILITIESBeforePost(DataSet: TDataSet);
    procedure CO_FED_TAX_LIABILITIESBeforeEdit(DataSet: TDataSet);
    procedure CO_FED_TAX_LIABILITIESBeforeDelete(DataSet: TDataSet);
    procedure CO_FED_TAX_LIABILITIESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TDM_CO_FED_TAX_LIABILITIES.CO_FED_TAX_LIABILITIESBeforePost(
  DataSet: TDataSet);
begin

 //   Added By CA..

  if DataSet.FieldByName('AMOUNT').IsNull then
     raise EUpdateError.CreateHelp('Amount is a required field', IDH_ConsistencyViolation);


  if DataSet.FieldByName('CHECK_DATE').IsNull then
    raise EUpdateError.CreateHelp('Check date is a required field', IDH_ConsistencyViolation);
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

procedure TDM_CO_FED_TAX_LIABILITIES.CO_FED_TAX_LIABILITIESBeforeEdit(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_FED_TAX_LIABILITIES.CO_FED_TAX_LIABILITIESBeforeDelete(
  DataSet: TDataSet);
begin
  ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime, True, True);
end;

procedure TDM_CO_FED_TAX_LIABILITIES.CO_FED_TAX_LIABILITIESCalcFields(
  DataSet: TDataSet);
var
 s : string;
begin
  inherited;

  s := ExtractFromFiller(Dataset.fieldByName('FILLER').AsString , 41, 10);
  if (s <> '') then
     Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := StrToDate(s)
  else
    if not Dataset.FieldByName('PERIOD_END_DATE').IsNull then
       Dataset.FieldByName('CALC_PERIOD_END_DATE').AsDateTime := Dataset.FieldByName('PERIOD_END_DATE').AsDateTime;

end;

initialization
  RegisterClass(TDM_CO_FED_TAX_LIABILITIES);

finalization
  UnregisterClass(TDM_CO_FED_TAX_LIABILITIES);

end.
