inherited DM_CO_JOBS_LOCALS: TDM_CO_JOBS_LOCALS
  OldCreateOrder = False
  Left = 443
  Top = 103
  Height = 540
  Width = 783
  object CO_JOBS_LOCALS: TCO_JOBS_LOCALS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_JOBS_LOCALS_PROV'
    ValidateWithMask = True
    Left = 56
    Top = 40
    object CO_JOBS_LOCALSCO_JOBS_LOCALS_NBR: TIntegerField
      FieldName = 'CO_JOBS_LOCALS_NBR'
    end
    object CO_JOBS_LOCALSCO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
    end
    object CO_JOBS_LOCALSCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object CO_JOBS_LOCALSLocalName: TStringField
      DisplayLabel = 'Local Name'
      FieldKind = fkLookup
      FieldName = 'LocalName'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 240
    Top = 40
  end
end
