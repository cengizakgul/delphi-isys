inherited DM_CO_BENEFIT_STATES: TDM_CO_BENEFIT_STATES
  OldCreateOrder = True
  Left = 520
  Top = 200
  Height = 479
  Width = 741
  object CO_BENEFIT_STATES: TCO_BENEFIT_STATES
    ProviderName = 'CO_BENEFIT_STATES_PROV'
    FieldDefs = <
      item
        Name = 'SY_STATES_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_BENEFITS_NBR'
        DataType = ftInteger
      end>
    OnCalcFields = CO_BENEFIT_STATESCalcFields
    Left = 56
    Top = 24
    object CO_BENEFIT_STATESCO_BENEFIT_STATES_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_STATES_NBR'
    end
    object CO_BENEFIT_STATESSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object CO_BENEFIT_STATESCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object CO_BENEFIT_STATESstate: TStringField
      FieldKind = fkCalculated
      FieldName = 'state'
      Size = 2
      Calculated = True
    end
    object CO_BENEFIT_STATESname: TStringField
      FieldKind = fkCalculated
      FieldName = 'name'
      Calculated = True
    end
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 200
    Top = 72
  end
end
