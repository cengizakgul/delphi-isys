inherited DM_SB_USER: TDM_SB_USER
  OldCreateOrder = True
  Left = 560
  Top = 147
  Height = 540
  Width = 783
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 240
    Top = 64
  end
  object SB_USER: TSB_USER
    ProviderName = 'SB_USER_PROV'
    FieldDefs = <
      item
        Name = 'SB_USER_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'USER_ID'
        Attributes = [faRequired]
        DataType = ftString
        Size = 128
      end
      item
        Name = 'USER_SIGNATURE'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'LAST_NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FIRST_NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'MIDDLE_INITIAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ACTIVE_USER'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DEPARTMENT'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PASSWORD_CHANGE_DATE'
        Attributes = [faRequired]
        DataType = ftDateTime
      end
      item
        Name = 'SECURITY_LEVEL'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'USER_UPDATE_OPTIONS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 512
      end
      item
        Name = 'USER_FUNCTIONS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 512
      end
      item
        Name = 'USER_PASSWORD'
        Attributes = [faRequired]
        DataType = ftString
        Size = 32
      end
      item
        Name = 'EMAIL_ADDRESS'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'ClientNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB_ACCOUNTANT_NBR'
        DataType = ftInteger
      end>
    OnCalcFields = SB_USERCalcFields
    OnAddLookups = SB_USERAddLookups
    Left = 96
    Top = 48
    object SB_USERSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
      Origin = '"SB_USER_HIST"."SB_USER_NBR"'
      Required = True
    end
    object SB_USERUSER_ID: TStringField
      FieldName = 'USER_ID'
      Origin = '"SB_USER_HIST"."USER_ID"'
      Required = True
      Size = 128
    end
    object SB_USERUSER_SIGNATURE: TBlobField
      FieldName = 'USER_SIGNATURE'
      Origin = '"SB_USER_HIST"."USER_SIGNATURE"'
      Size = 8
    end
    object SB_USERLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = '"SB_USER_HIST"."LAST_NAME"'
      Required = True
      Size = 30
    end
    object SB_USERFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = '"SB_USER_HIST"."FIRST_NAME"'
      Required = True
    end
    object SB_USERMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Origin = '"SB_USER_HIST"."MIDDLE_INITIAL"'
      Size = 1
    end
    object SB_USERACTIVE_USER: TStringField
      FieldName = 'ACTIVE_USER'
      Origin = '"SB_USER_HIST"."ACTIVE_USER"'
      Required = True
      Size = 1
    end
    object SB_USERDEPARTMENT: TStringField
      FieldName = 'DEPARTMENT'
      Origin = '"SB_USER_HIST"."DEPARTMENT"'
      Required = True
      Size = 1
    end
    object SB_USERPASSWORD_CHANGE_DATE: TDateTimeField
      FieldName = 'PASSWORD_CHANGE_DATE'
      Origin = '"SB_USER_HIST"."PASSWORD_CHANGE_DATE"'
      Required = True
    end
    object SB_USERSECURITY_LEVEL: TStringField
      FieldName = 'SECURITY_LEVEL'
      Origin = '"SB_USER_HIST"."SECURITY_LEVEL"'
      Required = True
      Size = 1
    end
    object SB_USERUSER_UPDATE_OPTIONS: TStringField
      FieldName = 'USER_UPDATE_OPTIONS'
      Origin = '"SB_USER_HIST"."USER_UPDATE_OPTIONS"'
      Required = True
      Size = 512
    end
    object SB_USERUSER_FUNCTIONS: TStringField
      FieldName = 'USER_FUNCTIONS'
      Origin = '"SB_USER_HIST"."USER_FUNCTIONS"'
      Required = True
      Size = 512
    end
    object SB_USERUSER_PASSWORD: TStringField
      FieldName = 'USER_PASSWORD'
      Origin = '"SB_USER_HIST"."USER_PASSWORD"'
      Required = True
      Size = 32
    end
    object SB_USEREMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Origin = '"SB_USER_HIST"."EMAIL_ADDRESS"'
      Size = 80
    end
    object SB_USERCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"SB_USER_HIST"."CL_NBR"'
    end
    object SB_USERClientNumber: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'ClientNumber'
    end
    object SB_USERSB_ACCOUNTANT_NBR: TIntegerField
      FieldName = 'SB_ACCOUNTANT_NBR'
    end
    object SB_USERStereoType: TStringField
      FieldKind = fkCalculated
      FieldName = 'StereoType'
      Size = 10
      Calculated = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 272
    Top = 152
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 273
    Top = 219
  end
end
