inherited DM_EE_HR_PERFORMANCE_RATINGS: TDM_EE_HR_PERFORMANCE_RATINGS
  OldCreateOrder = True
  Left = 508
  Top = 193
  Height = 158
  Width = 321
  object EE_HR_PERFORMANCE_RATINGS: TEE_HR_PERFORMANCE_RATINGS
    ProviderName = 'EE_HR_PERFORMANCE_RATINGS_PROV'
    FieldDefs = <
      item
        Name = 'EE_HR_PERFORMANCE_RATINGS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_HR_PERFORMANCE_RATINGS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'OVERALL_RATING'
        DataType = ftInteger
      end
      item
        Name = 'REVIEW_DATE'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'CO_HR_SUPERVISORS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'CL_HR_REASON_CODES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'INCREASE_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'INCREASE_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INCREASE_EFFECTIVE_DATE'
        DataType = ftDate
      end
      item
        Name = 'RATE_NUMBER'
        DataType = ftInteger
      end>
    BeforePost = EE_HR_PERFORMANCE_RATINGSBeforePost
    Left = 88
    Top = 16
    object EE_HR_PERFORMANCE_RATINGSEE_HR_PERFORMANCE_RATINGS_NBR: TIntegerField
      FieldName = 'EE_HR_PERFORMANCE_RATINGS_NBR'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."EE_HR_PERFORMANCE_RATINGS_NBR"'
      Required = True
    end
    object EE_HR_PERFORMANCE_RATINGSEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."EE_NBR"'
      Required = True
    end
    object EE_HR_PERFORMANCE_RATINGSCO_HR_PERFORMANCE_RATINGS_NBR: TIntegerField
      FieldName = 'CO_HR_PERFORMANCE_RATINGS_NBR'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."CO_HR_PERFORMANCE_RATINGS_NBR"'
      Required = True
    end
    object EE_HR_PERFORMANCE_RATINGSOVERALL_RATING: TIntegerField
      FieldName = 'OVERALL_RATING'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."OVERALL_RATING"'
    end
    object EE_HR_PERFORMANCE_RATINGSREVIEW_DATE: TDateField
      FieldName = 'REVIEW_DATE'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."REVIEW_DATE"'
      Required = True
    end
    object EE_HR_PERFORMANCE_RATINGSCO_HR_SUPERVISORS_NBR: TIntegerField
      FieldName = 'CO_HR_SUPERVISORS_NBR'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."CO_HR_SUPERVISORS_NBR"'
    end
    object EE_HR_PERFORMANCE_RATINGSNOTES: TBlobField
      FieldName = 'NOTES'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."NOTES"'
      Size = 8
    end
    object EE_HR_PERFORMANCE_RATINGSCL_HR_REASON_CODES_NBR: TIntegerField
      FieldName = 'CL_HR_REASON_CODES_NBR'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."CL_HR_REASON_CODES_NBR"'
    end
    object EE_HR_PERFORMANCE_RATINGSINCREASE_AMOUNT: TFloatField
      FieldName = 'INCREASE_AMOUNT'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."INCREASE_AMOUNT"'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object EE_HR_PERFORMANCE_RATINGSINCREASE_TYPE: TStringField
      FieldName = 'INCREASE_TYPE'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."INCREASE_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EE_HR_PERFORMANCE_RATINGSINCREASE_EFFECTIVE_DATE: TDateField
      FieldName = 'INCREASE_EFFECTIVE_DATE'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."INCREASE_EFFECTIVE_DATE"'
    end
    object EE_HR_PERFORMANCE_RATINGSRATE_NUMBER: TIntegerField
      FieldName = 'RATE_NUMBER'
      Origin = '"EE_HR_PERFORMANCE_RATINGS_HIST"."RATE_NUMBER"'
    end
    object EE_HR_PERFORMANCE_RATINGSPerformanceLookup: TStringField
      FieldKind = fkLookup
      FieldName = 'PerformanceLookup'
      LookupDataSet = DM_CO_HR_PERFORMANCE_RATINGS.CO_HR_PERFORMANCE_RATINGS
      LookupKeyFields = 'CO_HR_PERFORMANCE_RATINGS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_HR_PERFORMANCE_RATINGS_NBR'
      Size = 40
      Lookup = True
    end
    object EE_HR_PERFORMANCE_RATINGSReasonLookup: TStringField
      FieldKind = fkLookup
      FieldName = 'ReasonLookup'
      LookupDataSet = DM_CL_HR_REASON_CODES.CL_HR_REASON_CODES
      LookupKeyFields = 'CL_HR_REASON_CODES_NBR'
      LookupResultField = 'Name'
      KeyFields = 'CL_HR_REASON_CODES_NBR'
      Size = 21
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 224
    Top = 64
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 224
    Top = 8
  end
end
