// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SY_GL_AGENCY_REPORT;

interface

uses
  SDM_ddTable, SDataDictSystem,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SFieldCodeValues, EvUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses;

type
  TDM_SY_GL_AGENCY_REPORT = class(TDM_ddTable)
    SY_GL_AGENCY_REPORT: TSY_GL_AGENCY_REPORT;
    SY_GL_AGENCY_REPORTDESCRIPTION: TStringField;
    SY_GL_AGENCY_REPORTSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    SY_GL_AGENCY_REPORTSYSTEM_TAX_TYPE: TStringField;
    SY_GL_AGENCY_REPORTRETURN_FREQUENCY: TStringField;
    SY_GL_AGENCY_REPORTDEPOSIT_FREQUENCY: TStringField;
    SY_GL_AGENCY_REPORTSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    SY_GL_AGENCY_REPORTWHEN_DUE_TYPE: TStringField;
    SY_GL_AGENCY_REPORTNUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField;
    SY_GL_AGENCY_REPORTENLIST_AUTOMATICALLY: TStringField;
    SY_GL_AGENCY_REPORTPRINT_WHEN: TStringField;
    SY_GL_AGENCY_REPORTFILLER: TStringField;
    SY_GL_AGENCY_REPORTTAX_SERVICE_FILTER: TStringField;
    SY_GL_AGENCY_REPORTCONSOLIDATED_FILTER: TStringField;
    SY_GL_AGENCY_REPORTEND_EFFECTIVE_MONTH: TIntegerField;
    SY_GL_AGENCY_REPORTBEGIN_EFFECTIVE_MONTH: TIntegerField;
    SY_GL_AGENCY_REPORTTAX_RETURN_ACTIVE: TStringField;
    SY_GL_AGENCY_REPORTSY_REPORTS_GROUP_NBR: TIntegerField;
    procedure SY_GL_AGENCY_REPORTCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_SY_GL_AGENCY_REPORT.SY_GL_AGENCY_REPORTCalcFields(
  DataSet: TDataSet);
var
  s: string;
begin
  inherited;
  s := ReturnDescription(TaxReturnType_ComboChoices, DataSet.FieldByName('SYSTEM_TAX_TYPE').AsString);
  if s <> '' then
    s := s + '- ';
  s := s + ReturnDescription(ReturnFrequencyType_ComboChoices, DataSet.FieldByName('RETURN_FREQUENCY').AsString);
  if s <> '' then
    s := s + '- ';
  s := s + ReturnDescription(ReturnLocalDepFrequencyType_ComboChoices, DataSet.FieldByName('DEPOSIT_FREQUENCY').AsString);
  SY_GL_AGENCY_REPORTdescription.AsString := s;
end;

initialization
  RegisterClass(TDM_SY_GL_AGENCY_REPORT);

finalization
  UnregisterClass(TDM_SY_GL_AGENCY_REPORT);

end.
