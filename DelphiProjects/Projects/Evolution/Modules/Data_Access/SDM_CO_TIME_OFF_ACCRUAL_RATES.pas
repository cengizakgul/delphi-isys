// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_TIME_OFF_ACCRUAL_RATES;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_TIME_OFF_ACCRUAL_RATES = class(TDM_ddTable)
    CO_TIME_OFF_ACCRUAL_RATES: TCO_TIME_OFF_ACCRUAL_RATES;
    CO_TIME_OFF_ACCRUAL_RATESCO_TIME_OFF_ACCRUAL_RATES_NBR: TIntegerField;
    CO_TIME_OFF_ACCRUAL_RATESCO_TIME_OFF_ACCRUAL_NBR: TIntegerField;
    CO_TIME_OFF_ACCRUAL_RATESMINIMUM_MONTH_NUMBER: TFloatField;
    CO_TIME_OFF_ACCRUAL_RATESMAXIMUM_MONTH_NUMBER: TFloatField;
    CO_TIME_OFF_ACCRUAL_RATESHOURS: TFloatField;
    CO_TIME_OFF_ACCRUAL_RATESMAXIMUM_CARRYOVER: TFloatField;
    CO_TIME_OFF_ACCRUAL_RATESANNUAL_ACCRUAL_MAXIMUM: TFloatField;
    CO_TIME_OFF_ACCRUAL_RATESRateDescription: TStringField;
    DM_COMPANY: TDM_COMPANY;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_TIME_OFF_ACCRUAL_RATES: TDM_CO_TIME_OFF_ACCRUAL_RATES;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_TIME_OFF_ACCRUAL_RATES);

finalization
  UnregisterClass(TDM_CO_TIME_OFF_ACCRUAL_RATES);

end.
