inherited DM_CL_AGENCY: TDM_CL_AGENCY
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 276
  Top = 107
  Height = 291
  Width = 400
  object CL_AGENCY: TCL_AGENCY
    ProviderName = 'CL_AGENCY_PROV'
    BeforePost = CL_AGENCYBeforePost
    OnCalcFields = CL_AGENCYCalcFields
    Left = 49
    Top = 23
    object CL_AGENCYAgency_Name: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'Agency_Name'
      LookupDataSet = DM_SB_AGENCY.SB_AGENCY
      LookupKeyFields = 'SB_AGENCY_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SB_AGENCY_NBR'
      Size = 40
      Lookup = True
    end
    object CL_AGENCYAgencyAddress: TStringField
      DisplayWidth = 62
      FieldKind = fkCalculated
      FieldName = 'AgencyAddress'
      Size = 62
      Calculated = True
    end
    object CL_AGENCYCL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_AGENCY_NBR'
      Visible = False
    end
    object CL_AGENCYSB_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_AGENCY_NBR'
      Visible = False
    end
    object CL_AGENCYREPORT_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'REPORT_METHOD'
      Visible = False
      Size = 1
    end
    object CL_AGENCYFREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'FREQUENCY'
      Visible = False
      Size = 1
    end
    object CL_AGENCYMONTH_NUMBER: TStringField
      DisplayWidth = 2
      FieldName = 'MONTH_NUMBER'
      Visible = False
      Size = 2
    end
    object CL_AGENCYCL_BANK_ACCOUNT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_BANK_ACCOUNT_NBR'
      Visible = False
    end
    object CL_AGENCYPAYMENT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'PAYMENT_TYPE'
      Visible = False
      Size = 1
    end
    object CL_AGENCYCL_DELIVERY_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_DELIVERY_GROUP_NBR'
      Visible = False
    end
    object CL_AGENCYCLIENT_AGENCY_CUSTOM_FIELD: TStringField
      DisplayWidth = 10
      FieldName = 'CLIENT_AGENCY_CUSTOM_FIELD'
      Visible = False
      Size = 10
    end
    object CL_AGENCYNOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Visible = False
      Size = 1
    end
    object CL_AGENCYFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object CL_AGENCYGL_TAG: TStringField
      FieldName = 'GL_TAG'
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 51
    Top = 81
  end
end
