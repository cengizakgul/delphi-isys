// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_GENERAL_LEDGER;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants,
  SDDClasses, EvClientDataSet, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_CO_GENERAL_LEDGER = class(TDM_ddTable)
    CO_GENERAL_LEDGER: TCO_GENERAL_LEDGER;
    CO_GENERAL_LEDGERCO_GENERAL_LEDGER_NBR: TIntegerField;
    CO_GENERAL_LEDGERCO_NBR: TIntegerField;
    CO_GENERAL_LEDGERLEVEL_TYPE: TStringField;
    CO_GENERAL_LEDGERLEVEL_NBR: TIntegerField;
    CO_GENERAL_LEDGERDATA_TYPE: TStringField;
    CO_GENERAL_LEDGERDATA_NBR: TIntegerField;
    CO_GENERAL_LEDGERGENERAL_LEDGER_FORMAT: TStringField;
    CO_GENERAL_LEDGERLevel_Name: TStringField;
    CO_GENERAL_LEDGERData_Name: TStringField;
    CO_GENERAL_LEDGERLevelType_Name: TStringField;
    CO_GENERAL_LEDGERDataType_Name: TStringField;
    procedure CO_GENERAL_LEDGERBeforePost(DataSet: TDataSet);
    procedure CO_GENERAL_LEDGERCalcFields(DataSet: TDataSet);
    procedure CO_GENERAL_LEDGERNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

uses
  EvConsts, SFieldCodeValues, EvUtils;

procedure TDM_CO_GENERAL_LEDGER.CO_GENERAL_LEDGERBeforePost(
  DataSet: TDataSet);
begin
  if DataSet.FieldByName('LEVEL_NBR').IsNull then
    DataSet.FieldByName('LEVEL_NBR').AsInteger := 0;
  if DataSet.FieldByName('DATA_NBR').IsNull then
    DataSet.FieldByName('DATA_NBR').AsInteger := 0;
end;

procedure TDM_CO_GENERAL_LEDGER.CO_GENERAL_LEDGERCalcFields(
  DataSet: TDataSet);
var
  sKey: string;
begin

  sKey := CO_GENERAL_LEDGERLEVEL_TYPE.AsString;
  CO_GENERAL_LEDGERLevel_Name.Clear;

  if (sKey <> '') and (CO_GENERAL_LEDGERLEVEL_NBR.AsInteger > 0) then
    case sKey[1] of
      GL_LEVEL_DIVISION:
      begin
        if not DM_COMPANY.CO_DIVISION.Active then
           DM_COMPANY.CO_DIVISION.DataRequired('');
        if DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', CO_GENERAL_LEDGERLEVEL_NBR.AsInteger, []) then
           CO_GENERAL_LEDGERLevel_Name.AsVariant := DM_COMPANY.CO_DIVISION.fieldByName('CUSTOM_DIVISION_NUMBER').AsVariant;
      end;
      GL_LEVEL_BRANCH:
      begin
        if not DM_COMPANY.CO_BRANCH.Active then
           DM_COMPANY.CO_BRANCH.DataRequired('');
        if DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', CO_GENERAL_LEDGERLEVEL_NBR.AsInteger, []) then
           CO_GENERAL_LEDGERLevel_Name.AsVariant := DM_COMPANY.CO_BRANCH.fieldByName('CUSTOM_BRANCH_NUMBER').AsVariant;
      end;
      GL_LEVEL_DEPT:
      begin
        if not DM_COMPANY.CO_DEPARTMENT.Active then
           DM_COMPANY.CO_DEPARTMENT.DataRequired('');
        if DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', CO_GENERAL_LEDGERLEVEL_NBR.AsInteger, []) then
           CO_GENERAL_LEDGERLevel_Name.AsVariant := DM_COMPANY.CO_DEPARTMENT.fieldByName('CUSTOM_DEPARTMENT_NUMBER').AsVariant;
      end;
      GL_LEVEL_TEAM:
      begin
        if not DM_COMPANY.CO_TEAM.Active then
           DM_COMPANY.CO_TEAM.DataRequired('');
        if DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', CO_GENERAL_LEDGERLEVEL_NBR.AsInteger, []) then
           CO_GENERAL_LEDGERLevel_Name.AsVariant := DM_COMPANY.CO_TEAM.fieldByName('CUSTOM_TEAM_NUMBER').AsVariant;
      end;
      GL_LEVEL_EMPLOYEE:
      begin
        if not DM_EMPLOYEE.EE.Active then
           DM_EMPLOYEE.EE.DataRequired('');
        if DM_COMPANY.EE.Locate('EE_NBR', CO_GENERAL_LEDGERLEVEL_NBR.AsInteger, []) then
           CO_GENERAL_LEDGERLevel_Name.AsVariant := DM_COMPANY.EE.fieldByName('CUSTOM_EMPLOYEE_NUMBER').AsVariant;
      end;
      GL_LEVEL_JOB:
      begin
        if not DM_COMPANY.CO_JOBS.Active then
           DM_COMPANY.CO_JOBS.DataRequired('');
        if DM_COMPANY.CO_JOBS.Locate('CO_JOBS_NBR', CO_GENERAL_LEDGERLEVEL_NBR.AsInteger, []) then
           CO_GENERAL_LEDGERLevel_Name.AsVariant := DM_COMPANY.CO_JOBS.fieldByName('Description').AsVariant;
      end;
    end;

  sKey := CO_GENERAL_LEDGERDATA_TYPE.AsString;
  CO_GENERAL_LEDGERData_Name.Clear;

  if (sKey <> '') and (DataSet.FieldByName('DATA_NBR').AsInteger > 0) then
    case sKey[1] of
      GL_DATA_ED, GL_DATA_ED_OFFSET:
      begin
        if not DM_COMPANY.CO_E_D_CODES.Active then
           DM_COMPANY.CO_E_D_CODES.DataRequired('');
        if DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', DataSet.FieldByName('DATA_NBR').AsInteger, []) then
           CO_GENERAL_LEDGERData_Name.AsVariant := DM_COMPANY.CO_E_D_CODES.fieldByName('ED_Lookup').AsVariant;
      end;
      GL_DATA_STATE, GL_DATA_EE_SDI, GL_DATA_ER_SDI,
      GL_DATA_STATE_OFFSET, GL_DATA_EE_SDI_OFFSET, GL_DATA_ER_SDI_OFFSET:
      begin
        if not DM_COMPANY.CO_STATES.Active then
           DM_COMPANY.CO_STATES.DataRequired('');
        if DM_COMPANY.CO_STATES.Locate('CO_STATES_NBR', DataSet.FieldByName('DATA_NBR').AsInteger, []) then
           CO_GENERAL_LEDGERData_Name.AsVariant := DM_COMPANY.CO_STATES.fieldByName('STATE').AsVariant;
      end;
      GL_DATA_EE_SUI, GL_DATA_ER_SUI,
      GL_DATA_EE_SUI_OFFSET, GL_DATA_ER_SUI_OFFSET:
      begin
        if not DM_COMPANY.CO_SUI.Active then
           DM_COMPANY.CO_SUI.DataRequired('');
        if DM_COMPANY.CO_SUI.Locate('CO_SUI_NBR', DataSet.FieldByName('DATA_NBR').AsInteger, []) then
           CO_GENERAL_LEDGERData_Name.AsVariant := DM_COMPANY.CO_SUI.fieldByName('SUI_Name').AsVariant;
      end;
      GL_DATA_WC_IMPOUND, GL_DATA_WC_IMPOUND_OFFSET:
      begin
        if not DM_COMPANY.CO_WORKERS_COMP.Active then
           DM_COMPANY.CO_WORKERS_COMP.DataRequired('');
        if DM_COMPANY.CO_WORKERS_COMP.Locate('CO_WORKERS_COMP_NBR', DataSet.FieldByName('DATA_NBR').AsInteger, []) then
           CO_GENERAL_LEDGERData_Name.AsVariant := DM_COMPANY.CO_WORKERS_COMP.fieldByName('WORKERS_COMP_CODE').AsVariant;
      end;
      GL_DATA_AGENCY_CHECK, GL_DATA_AGENCY_CHECK_OFFSET:
      begin
        if not DM_CLIENT.CL_AGENCY.Active then
           DM_CLIENT.CL_AGENCY.DataRequired('');
        if DM_COMPANY.CL_AGENCY.Locate('CL_AGENCY_NBR', DataSet.FieldByName('DATA_NBR').AsInteger, []) then
           CO_GENERAL_LEDGERData_Name.AsVariant := DM_COMPANY.CL_AGENCY.fieldByName('Agency_Name').AsVariant;
      end;
      GL_DATA_LOCAL, GL_DATA_LOCAL_OFFSET:
      begin
        if not DM_COMPANY.CO_LOCAL_TAX.Active then
           DM_COMPANY.CO_LOCAL_TAX.DataRequired('');
        if DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', DataSet.FieldByName('DATA_NBR').AsInteger, []) then
           CO_GENERAL_LEDGERData_Name.AsVariant := DM_COMPANY.CO_LOCAL_TAX.fieldByName('LocalName').AsVariant;
      end;
    end;

  CO_GENERAL_LEDGERLevelType_Name.AsString := ReturnDescription(GL_Level_ComboChoices, CO_GENERAL_LEDGERLevel_Type.AsString);
  CO_GENERAL_LEDGERDataType_Name.AsString := ReturnDescription(GL_Data_ComboChoices, CO_GENERAL_LEDGERData_Type.AsString);
end;

procedure TDM_CO_GENERAL_LEDGER.CO_GENERAL_LEDGERNewRecord(
  DataSet: TDataSet);
begin
  DataSet['GENERAL_LEDGER_FORMAT'] := DM_COMPANY.CO['GENERAL_LEDGER_FORMAT_STRING'];
end;

initialization
  RegisterClass(TDM_CO_GENERAL_LEDGER);

finalization
  UnregisterClass(TDM_CO_GENERAL_LEDGER);

end.
