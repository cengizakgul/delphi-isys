inherited DM_EE_BENEFITS: TDM_EE_BENEFITS
  OldCreateOrder = True
  Left = 455
  Top = 233
  Height = 540
  Width = 783
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 288
    Top = 48
  end
  object EE_BENEFITS: TEE_BENEFITS
    ProviderName = 'EE_BENEFITS_PROV'
    FieldDefs = <
      item
        Name = 'COBRA_FILING_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DEDUCTION_FREQUENCY'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'ENROLLMENT_STATUS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TOTAL_PREMIUM_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'BENEFIT_EFFECTIVE_DATE'
        DataType = ftDate
      end
      item
        Name = 'COBRA_STATUS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BENEFIT_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EXPIRATION_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'EE_RATE'
        DataType = ftFloat
      end
      item
        Name = 'ER_RATE'
        DataType = ftFloat
      end
      item
        Name = 'COBRA_RATE'
        DataType = ftFloat
      end
      item
        Name = 'CO_BENEFITS_NBR'
        DataType = ftInteger
      end>
    Left = 96
    Top = 56
    object EE_BENEFITSCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object EE_BENEFITSCOBRA_FILING_TYPE: TStringField
      FieldName = 'COBRA_FILING_TYPE'
      Origin = '"EE_BENEFITS_HIST"."COBRA_FILING_TYPE"'
      Required = True
      Size = 1
    end
    object EE_BENEFITSDEDUCTION_FREQUENCY: TStringField
      FieldName = 'DEDUCTION_FREQUENCY'
      Origin = '"EE_BENEFITS_HIST"."DEDUCTION_FREQUENCY"'
      Required = True
      Size = 1
    end
    object EE_BENEFITSEE_BENEFITS_NBR: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object EE_BENEFITSEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Origin = '"EE_BENEFITS_HIST"."EE_NBR"'
      Required = True
    end
    object EE_BENEFITSENROLLMENT_STATUS: TStringField
      FieldName = 'ENROLLMENT_STATUS'
      Origin = '"EE_BENEFITS_HIST"."ENROLLMENT_STATUS"'
      Required = True
      Size = 1
    end
    object EE_BENEFITSTOTAL_PREMIUM_AMOUNT: TFloatField
      FieldName = 'TOTAL_PREMIUM_AMOUNT'
      Origin = '"EE_BENEFITS_HIST"."TOTAL_PREMIUM_AMOUNT"'
      DisplayFormat = '#,##0.00'
    end
    object EE_BENEFITSBENEFIT_EFFECTIVE_DATE: TDateField
      FieldName = 'BENEFIT_EFFECTIVE_DATE'
      Origin = '"EE_BENEFITS_HIST"."BENEFIT_EFFECTIVE_DATE"'
    end
    object EE_BENEFITSCOBRA_STATUS: TStringField
      FieldName = 'COBRA_STATUS'
      Origin = '"EE_BENEFITS_HIST"."COBRA_STATUS"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EE_BENEFITSBENEFIT_TYPE: TStringField
      FieldName = 'BENEFIT_TYPE'
      Origin = '"EE_BENEFITS_HIST"."BENEFIT_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object EE_BENEFITSEXPIRATION_DATE: TDateTimeField
      FieldName = 'EXPIRATION_DATE'
    end
    object EE_BENEFITSCO_BENEFIT_SUBTYPE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object EE_BENEFITSEE_RATE: TFloatField
      FieldName = 'EE_RATE'
      DisplayFormat = '#,##0.00'
    end
    object EE_BENEFITSER_RATE: TFloatField
      FieldName = 'ER_RATE'
      DisplayFormat = '#,##0.00'
    end
    object EE_BENEFITSCOBRA_RATE: TFloatField
      FieldName = 'COBRA_RATE'
      DisplayFormat = '#,##0.00'
    end
    object EE_BENEFITSBenefit: TStringField
      FieldKind = fkLookup
      FieldName = 'Benefit'
      LookupDataSet = DM_CO_BENEFITS.CO_BENEFITS
      LookupKeyFields = 'CO_BENEFITS_NBR'
      LookupResultField = 'BENEFIT_NAME'
      KeyFields = 'CO_BENEFITS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 280
    Top = 184
  end
end
