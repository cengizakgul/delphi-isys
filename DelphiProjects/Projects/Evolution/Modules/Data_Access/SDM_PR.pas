// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_PR;

interface

uses
  SDM_ddTable, SDataDictClient, isBaseClasses,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants, EvBasicUtils,
  Db,   SDataStructure, SSecurityInterface, EvTypes, SDataAccessCommon,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, isVCLBugFix, EvDataSet, EvCommonInterfaces,
  ISDataAccessComponents, EvDataAccessComponents, EvContext, EvExceptions, DateUtils, EvUIComponents, EvClientDataSet;

type
  TDM_PR = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    PR: TPR;
    procedure PRBeforeDelete(DataSet: TDataSet);
    procedure PRBeforeEdit(DataSet: TDataSet);
    procedure PRBeforePost(DataSet: TDataSet);
    procedure PRNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure PRDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure PRAfterDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure PRAfterPost(DataSet: TDataSet);
    procedure PRAfterCancel(DataSet: TDataSet);
  private
    InNestedTransaction: Boolean;
    ChecksToVoid: TStringList;
    MiscChecksToVoid: TStringList;
    AllChecks: String;
    AllMiscChecks: String;
    procedure DetachPayrollFromCalendar;
  public
    { Public declarations }
  end;

var
  DM_PR: TDM_PR;

implementation

uses
  EvUtils, EvConsts, STimeOffUtils, EvUIUtils;

{$R *.DFM}

procedure TDM_PR.DetachPayrollFromCalendar;
var
  MyTransactionManager: TTransactionManager;
begin
  DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
  if DM_PAYROLL.PR_SCHEDULED_EVENT.Locate('PR_NBR', PR.FieldByName('PR_NBR').Value, []) then
  begin
    MyTransactionManager := TTransactionManager.Create(nil);
    try
      MyTransactionManager.Initialize([DM_PAYROLL.PR_SCHEDULED_EVENT]);
      DM_PAYROLL.PR_SCHEDULED_EVENT.Edit;
      DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_NBR').Value := Null;
      DM_PAYROLL.PR_SCHEDULED_EVENT.Post;
      MyTransactionManager.ApplyUpdates;
    finally
      MyTransactionManager.Free;
    end;
  end;
end;

procedure TDM_PR.PRBeforeDelete(DataSet: TDataSet);
var
  VoidCheckNbr: String;
  VoidPRNbr: Integer;
  sFilter: String;
  WasNegative: Boolean;
  Params: IisListOfValues;

  procedure DetachLiabilities(const aLiabilityDS: TevClientDataSet);
  begin
    aLiabilityDS.First;
    while not aLiabilityDS.EOF do
    begin
      aLiabilityDS.Edit;
      aLiabilityDS.FieldByName('CO_TAX_DEPOSITS_NBR').Clear;
      aLiabilityDS.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
      aLiabilityDS.Post;
      aLiabilityDS.Next;
    end;
  end;

begin
  ctx_DataAccess.StartNestedTransaction([dbtClient]);

  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_NBR=' + DM_PAYROLL.PR.PR_NBR.AsString);
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.First;
  while not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Eof do
  begin
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := Null;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Next;
  end;

  DM_COMPANY.EE_HR_ATTENDANCE.DataRequired('PR_NBR=' + DM_PAYROLL.PR.PR_NBR.AsString);
  while DM_COMPANY.EE_HR_ATTENDANCE.RecordCount > 0 do
    DM_COMPANY.EE_HR_ATTENDANCE.Delete;
  ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, DM_COMPANY.EE_HR_ATTENDANCE]);

  InNestedTransaction := True;
  ChecksToVoid.Clear;
  MiscChecksToVoid.Clear;
  AllChecks := '';
  AllMiscChecks := '';
  try
    if DataSet.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED] then
    begin
      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') <> stEnabled then
        raise EUpdateError.CreateHelp('You do not have rights to delete processed payroll!', IDH_ConsistencyViolation);

      if DataSet.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT then
        ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime);

// Update scheduled E/D balances
      DM_CLIENT.CO_BENEFITS.DataRequired('ALL');
      DM_CLIENT.CO_BENEFIT_RATES.DataRequired('ALL');
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_SCHEDULED_E_DS, DataSet.FieldByName('CO_NBR').AsInteger);
      OpenDataSetForPayroll(DM_PAYROLL.PR_CHECK_LINES, DataSet.FieldByName('PR_NBR').AsInteger);
      OpenDataSetForPayroll(DM_PAYROLL.PR_CHECK, DataSet.FieldByName('PR_NBR').AsInteger);

      DM_PAYROLL.PR_CHECK_LINES.First;
      while not DM_PAYROLL.PR_CHECK_LINES.Eof do
      begin
        if not DM_PAYROLL.PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
        begin
          Assert(DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', DM_CLIENT.PR_CHECK_LINES.PR_CHECK_NBR.Value, []));
          if DM_PAYROLL.PR_CHECK.UPDATE_BALANCE.AsString = GROUP_BOX_YES then
          begin
            if DM_CLIENT.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'], []) then
            if (ConvertNull(DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'], 0) <> 0) and (ConvertNull(DM_PAYROLL.PR_CHECK_LINES['AMOUNT'], 0) <> 0) then
            begin
              WasNegative := DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'] < 0;
                DM_CLIENT.EE_SCHEDULED_E_DS.Edit;
              DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'] := ConvertNull(DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'], 0) - DM_PAYROLL.PR_CHECK_LINES['AMOUNT'];
              if (not WasNegative and DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'] < 0) or (WasNegative and DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'] > 0) then
                DM_CLIENT.EE_SCHEDULED_E_DS['BALANCE'] := 0;
              ctx_DataAccess.PayrollIsProcessing := True;
              try
                DM_CLIENT.EE_SCHEDULED_E_DS.Post;
              finally
                ctx_DataAccess.PayrollIsProcessing := False;
              end;
            end;
          end;
        end;
        DM_PAYROLL.PR_CHECK_LINES.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_CLIENT.EE_SCHEDULED_E_DS]);

      DM_PAYROLL.PR_CHECK_LINES.Close;
      DetachPayrollFromCalendar;
// Detach liabilities from tax deposits
      DM_COMPANY.CO_TAX_DEPOSITS.DataRequired('PR_NBR=' + DataSet.FieldByName('PR_NBR').AsString);
      if DM_COMPANY.CO_TAX_DEPOSITS.RecordCount > 0 then
      begin
        sFilter := 'CO_TAX_DEPOSITS_NBR in ('+ GetFieldValueListString(DM_COMPANY.CO_TAX_DEPOSITS, 'CO_TAX_DEPOSITS_NBR')+ ')';
        PopulateDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
          DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES], sFilter, False);
        ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
          DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES]);
        DetachLiabilities(DM_COMPANY.CO_FED_TAX_LIABILITIES);
        DetachLiabilities(DM_COMPANY.CO_STATE_TAX_LIABILITIES);
        DetachLiabilities(DM_COMPANY.CO_SUI_LIABILITIES);
        DetachLiabilities(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES);
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
          DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES]);
      end;
      with DM_COMPANY do
      begin
        if CO_FED_TAX_LIABILITIES.Active then
          CO_FED_TAX_LIABILITIES.Close;
        if CO_STATE_TAX_LIABILITIES.Active then
          CO_STATE_TAX_LIABILITIES.Close;
        if CO_SUI_LIABILITIES.Active then
          CO_SUI_LIABILITIES.Close;
        if CO_LOCAL_TAX_LIABILITIES.Active then
          CO_LOCAL_TAX_LIABILITIES.Close;
      end;
    end
    else
    begin
      PayrollBeforeDelete(DataSet);
      DetachPayrollFromCalendar;
    end;

    if DataSet.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED] then
    begin
      Params := TisListOfValues.Create;
      Params.AddValue('PrNbr', DataSet.FieldByName('PR_NBR').AsInteger);
      Params.AddValue('AccrualDate',Date);
      Params.AddValue('VoidingPrNbr', Null);

      ctx_DBAccess.RunInTransaction('VoidPrTimeOff', Params);
    end;

    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Close;

    DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DataSet.FieldByName('PR_NBR').AsString);
    with DM_PAYROLL.PR_CHECK do
    begin
      First;
      while not EOF do
      begin
        if FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
        begin
          VoidCheckNbr := Trim(ExtractFromFiller(FieldByName('FILLER').AsString, 1, 8));
          if VoidCheckNbr <> '' then
            ChecksToVoid.Add(VoidCheckNbr);
        end;
        AllChecks := AllChecks + FieldByName('PR_CHECK_NBR').AsString + ',';
        Next;
      end;
    end;
    DM_PAYROLL.PR_CHECK.Close;
    if AllChecks <> '' then
      Delete(AllChecks, Length(AllChecks), 1);

    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=' + DataSet.FieldByName('PR_NBR').AsString);
    with DM_PAYROLL.PR_MISCELLANEOUS_CHECKS do
    begin
      First;
      while not EOF do
      begin
        if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY_VOID, MISC_CHECK_TYPE_TAX_VOID,
        MISC_CHECK_TYPE_BILLING_VOID] then
        begin
          VoidCheckNbr := Trim(ExtractFromFiller(FieldByName('FILLER').AsString, 1, 8));
          if VoidCheckNbr <> '' then
            MiscChecksToVoid.Add(VoidCheckNbr);
        end;
        AllMiscChecks := AllMiscChecks + FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString + ',';
        if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_TAX, MISC_CHECK_TYPE_TAX_VOID] then
          Delete
        else
          Next;
      end;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_MISCELLANEOUS_CHECKS]);
    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Close;
    if AllMiscChecks <> '' then
      Delete(AllMiscChecks, Length(AllMiscChecks), 1);

    if (DataSet.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_REVERSAL) and (Trim(ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 8)) <> '') then
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      VoidPRNbr := StrToInt(Trim(ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 1, 8)));
      ctx_DataAccess.SetPayrollLockInTransaction(VoidPRNbr, True);
      try
// Unvoid prior payroll
        if Active then
          Close;
        with TExecDSWrapper.Create('pr;GenericSelectCurrentNBR') do
        begin
          SetMacro('NbrField', 'PR');
          SetMacro('Columns', '*');
          SetMacro('TableName', 'PR');
          SetParam('RecordNbr', VoidPRNbr);
          DataRequest(AsVariant);
        end;
        Open;
        if RecordCount > 0 then
        begin
          First;
          Edit;
          FieldByName('STATUS').AsString := PAYROLL_STATUS_PROCESSED;
          Post;
          ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
        end;
// Unvoid prior payroll checks
        if Active then
          Close;
        with TExecDSWrapper.Create('pr_check;GenericSelectCurrentNBR') do
        begin
          SetMacro('NbrField', 'PR');
          SetMacro('Columns', '*');
          SetMacro('TableName', 'PR_CHECK');
          SetParam('RecordNbr', VoidPRNbr);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        while not EOF do
        begin
          Edit;
          FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
          Post;
          Next;
        end;
        ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
      finally
        ctx_DataAccess.SetPayrollLockInTransaction(VoidPRNbr, False);
      end;
    end;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    InNestedTransaction := False;
    raise;
  end;
end;

procedure TDM_PR.PRBeforeEdit(DataSet: TDataSet);
begin
  if ctx_DataAccess.PayrollLocked and ((PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED)
  or (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED)) then
    raise EUpdateError.CreateHelp('You can not change processed or voided payroll!', IDH_ConsistencyViolation);
end;

procedure TDM_PR.PRBeforePost(DataSet: TDataSet);
begin
  if (DataSet.Tag = 0) and {(DataSet.State = dsInsert) and }(DataSet.FieldByName('CHECK_DATE').AsDateTime <= Int(SysTime))
  and not (DataSet.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_SETUP, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT,
  PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT, PAYROLL_TYPE_IMPORT])
  and (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled) then
  begin
    DataSet.Cancel;
    raise EUpdateError.CreateHelp('You do not have rights to create backdated payroll!', IDH_ConsistencyViolation);
  end;

  if (((DataSet.State = dsInsert) or (DataSet.FieldByName('STATUS').AsString = PAYROLL_STATUS_COMPLETED))
  and (DataSet.FieldByName('CHECK_DATE').AsDateTime < GetBeginYear(Now))) and not ctx_DataAccess.PayrollIsProcessing
  and not (DataSet.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_SETUP, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT,
  PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT, PAYROLL_TYPE_IMPORT]) then
    if not Unattended then
      if EvMessage('Your check date is for a prior year. Would you like to proceed?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
      begin
        DataSet.Cancel;
        AbortEx;
      end;

  if (DataSet.State = dsInsert) and (DataSet.FieldByName('CHECK_DATE').AsDateTime <= Date)
  and (DataSet.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT) then
    ctx_DataAccess.CheckCompanyLock(DataSet.FieldByName('CO_NBR').AsInteger, DataSet.FieldByName('CHECK_DATE').AsDateTime);

  if not DataSet.FieldByName('CHECK_DATE').IsNull then
    DataSet.FieldByName('CHECK_DATE').AsDateTime := Trunc(DataSet.FieldByName('CHECK_DATE').AsFloat);

  if DataSet.FieldByName('RUN_NUMBER').IsNull then
    DataSet.FieldByName('RUN_NUMBER').Value := ctx_PayrollCalculation.GetNextRunNumber(DataSet.FieldByName('CHECK_DATE').Value);

  if DataSet.State = dsInsert then // Payroll was just created
  begin
    if (DataSet.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_REGULAR)
    and (DataSet.FieldByName('EXCLUDE_TIME_OFF').AsString = TIME_OFF_EXCLUDE_ACCRUAL_NONE) then
      DataSet.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
    if DataSet.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_SETUP then
    begin
      DM_PAYROLL.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := GROUP_BOX_BOTH2;
      if DataSet.FieldByName('EXCLUDE_R_C_B_0R_N').AsString = GROUP_BOX_NO then
        DataSet.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_BOTH2;
      DM_PAYROLL.PR.FieldByName('EXCLUDE_AGENCY').Value := GROUP_BOX_YES;
      DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value := GROUP_BOX_YES;
      DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').Value := GROUP_BOX_YES;
      if DataSet.FieldByName('EXCLUDE_TIME_OFF').AsString = TIME_OFF_EXCLUDE_ACCRUAL_NONE then
        DM_PAYROLL.PR.FieldByName('EXCLUDE_TIME_OFF').Value := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
    end;
    if DataSet.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_CL_CORRECTION then
    begin
      DataSet.FieldByName('APPROVED_BY_TAX').Value := PR_APPROVED_NO;
      DataSet.FieldByName('APPROVED_BY_FINANCE').Value := PR_APPROVED_NO;
      DataSet.FieldByName('APPROVED_BY_MANAGEMENT').Value := PR_APPROVED_NO;
    end;
  end;
  if (DataSet.State = dsInsert) or
  (DataSet.State = dsEdit) and (DataSet.FieldByName('CHECK_DATE').Value <> DataSet.FieldByName('CHECK_DATE').OldValue) then
    ctx_PayrollCalculation.AttachPayrollToCalendar(PR);
  if ctx_DataAccess.PayrollIsProcessing then
    DataSet.FieldByName('PROCESS_DATE').Value := SysTime;

  if DataSet.FieldByName('STATUS_DATE').IsNull then DataSet.FieldByName('STATUS_DATE').Value := SysTime;
end;

procedure TDM_PR.PRNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('CO_NBR').Value := DM_COMPANY.CO.FieldByName('CO_NBR').Value;
  DataSet.FieldByName('SCHEDULED_CALL_IN_DATE').Value := Date;
  DataSet.FieldByName('SCHEDULED_PROCESS_DATE').Value := Date;
  DataSet.FieldByName('SCHEDULED_DELIVERY_DATE').Value := Date;
  DataSet.FieldByName('EXCLUDE_R_C_B_0R_N').Value := DM_COMPANY.CO.FieldByName('EXCLUDE_R_C_B_0R_N').Value;
end;

procedure TDM_PR.DataModuleCreate(Sender: TObject);
begin
  ctx_DataAccess.Validate := True;
  ctx_DataAccess.PayrollIsProcessing := False;
  InNestedTransaction := False;
  ChecksToVoid := TStringList.Create;
  MiscChecksToVoid := TStringList.Create;
  AllChecks := '';
  AllMiscChecks := '';
end;

procedure TDM_PR.PRDeleteError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if InNestedTransaction then
  begin
    InNestedTransaction := False;
    ctx_DataAccess.RollbackNestedTransaction;
  end;
end;

procedure TDM_PR.PRAfterDelete(DataSet: TDataSet);
var
  PRList: TStringList;
  J, K: Integer;
  S: String;

  procedure AdjustVoidedChecks(TableName, Condition, NewStatus: String);
  var
    I: Integer;
  begin
    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', '*');
      SetMacro('TableName', TableName);
      SetMacro('Condition', Condition);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    ctx_DataAccess.CUSTOM_VIEW.First;
    while not ctx_DataAccess.CUSTOM_VIEW.EOF do
    begin
      if PRList.IndexOf(ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_NBR').AsString) = -1 then
        PRList.Add(ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_NBR').AsString);
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
    for I := 0 to PRList.Count - 1 do
      ctx_DataAccess.SetPayrollLockInTransaction(StrToInt(PRList[I]), True);
    try
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create(TableName + ';GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', TableName);
        SetMacro('Condition', Condition);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        ctx_DataAccess.CUSTOM_VIEW.Edit;
        ctx_DataAccess.CUSTOM_VIEW.FieldByName('CHECK_STATUS').AsString := NewStatus;
        ctx_DataAccess.CUSTOM_VIEW.Post;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
      ctx_DataAccess.PostDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    finally
      for I := 0 to PRList.Count - 1 do
        ctx_DataAccess.SetPayrollLockInTransaction(StrToInt(PRList[I]), False);
    end;
  end;

begin
  if InNestedTransaction then
  begin
    InNestedTransaction := False;
    PRList := TStringList.Create;
    try
      if ChecksToVoid.Count > 0 then
      begin
        AdjustVoidedChecks('PR_CHECK', 'PR_CHECK_NBR in (' + ChecksToVoid.CommaText + ')', CHECK_STATUS_OUTSTANDING);
        PRList.Clear;
      end;
      if MiscChecksToVoid.Count > 0 then
        AdjustVoidedChecks('PR_MISCELLANEOUS_CHECKS', 'PR_MISCELLANEOUS_CHECKS_NBR in (' + MiscChecksToVoid.CommaText + ')', MISC_CHECK_STATUS_OUTSTANDING);
    finally
      PRList.Free;
    end;
    while AllChecks <> '' do
    begin
      K := 1;
      S := AllChecks;
      for J := 1 to Length(AllChecks) do
      begin
        if AllChecks[J] = ',' then
          Inc(K);
        if K = 1500 then
        begin
          S := Copy(AllChecks, 1, J - 1);
          Delete(AllChecks, 1, J);
          Break;
        end;
      end;
      if S = AllChecks then
        AllChecks := '';
      if ctx_DataAccess.TEMP_CUSTOM_VIEW.Active then
        ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;

     // PLUMOUTH: WTF?
     // with TExecDSWrapper.Create(';GenericDeleteWithCondition') do
      with TExecDSWrapper.Create('DeleteAsOfDate') do
      begin
        SetMacro('TableName', 'TMP_CO_BANK_ACCOUNT_REGISTER');
        SetMacro('Condition', 'CL_NBR=' + IntToStr(ctx_DataAccess.ClientID) + ' and PR_CHECK_NBR in (' + S + ')');
        SetParam('StatusDate', Systime);
        ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
      end;


    end;
    while AllMiscChecks <> '' do
    begin
      K := 1;
      S := AllMiscChecks;
      for J := 1 to Length(AllMiscChecks) do
      begin
        if AllMiscChecks[J] = ',' then
          Inc(K);
        if K = 1500 then
        begin
          S := Copy(AllMiscChecks, 1, J - 1);
          Delete(AllMiscChecks, 1, J);
          Break;
        end;
      end;
      if S = AllMiscChecks then
        AllMiscChecks := '';
      if ctx_DataAccess.TEMP_CUSTOM_VIEW.Active then
        ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;

      // PLUMOUTH: WTF?
//      with TExecDSWrapper.Create(';GenericDeleteWithCondition') do
      with TExecDSWrapper.Create('DeleteAsOfDate') do
      begin
        SetMacro('TableName', 'TMP_CO_BANK_ACCOUNT_REGISTER');
        SetMacro('Condition', 'CL_NBR=' + IntToStr(ctx_DataAccess.ClientID) + ' and PR_MISCELLANEOUS_CHECKS_NBR in (' + S + ')');
        SetParam('StatusDate', Systime);
        ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
      end;
    end;
    ctx_DataAccess.CommitNestedTransaction;
  end;
end;

procedure TDM_PR.DataModuleDestroy(Sender: TObject);
begin
  ChecksToVoid.Free;
  MiscChecksToVoid.Free;
end;

procedure TDM_PR.PRAfterPost(DataSet: TDataSet);
var
  b: Boolean;
  Nbr: Integer;
  tidxName : string;
begin
  if (DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').Tag = 1) and (not Unattended) then
  begin
    DM_PAYROLL.PR_CHECK.CheckDSCondition('PR_NBR=' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger));
    ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR_CHECK]);
    if (DM_PAYROLL.PR_CHECK.RecordCount <> 0) and
       (EvMessage('Do you want to copy payroll check comments to checks?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      b := ctx_DataAccess.SkipPrCheckPostCheck;
      ctx_DataAccess.SkipPrCheckPostCheck := True;
      Nbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
      DM_PAYROLL.PR_CHECK.DisableControls;
      tidxName := DM_PAYROLL.PR_CHECK.IndexFieldNames;
      DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
      try
        DM_PAYROLL.PR_CHECK.First;
        while not DM_PAYROLL.PR_CHECK.Eof do
        begin
          DM_PAYROLL.PR_CHECK.Edit;
           //CHECK_COMMENTS
          DM_PAYROLL.PR_CHECK.UpdateBlobData('NOTES_NBR', DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').AsString);
          DM_PAYROLL.PR_CHECK.Post;
          DM_PAYROLL.PR_CHECK.Next;
        end;
      finally
        ctx_DataAccess.SkipPrCheckPostCheck := b;
        DM_PAYROLL.PR_CHECK.IndexFieldNames := tidxName;
        DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', Nbr, []);
        DM_PAYROLL.PR_CHECK.EnableControls;
      end;
    end;

    DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').Tag := 0;
  end;
end;

procedure TDM_PR.PRAfterCancel(DataSet: TDataSet);
begin
  DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').Tag := 0;
end;

initialization
  RegisterClass(TDM_PR);

finalization
  UnregisterClass(TDM_PR);

end.

