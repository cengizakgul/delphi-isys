// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_WORK_SHIFTS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, isVCLBugFix,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, EvContext, EvClientDataSet;

type
  TDM_EE_WORK_SHIFTS = class(TDM_ddTable)
    EE_WORK_SHIFTS: TEE_WORK_SHIFTS;
    EE_WORK_SHIFTSCO_SHIFTS_NBR: TIntegerField;
    EE_WORK_SHIFTSEE_NBR: TIntegerField;
    EE_WORK_SHIFTSEE_WORK_SHIFTS_NBR: TIntegerField;
    EE_WORK_SHIFTSSHIFT_PERCENTAGE: TFloatField;
    EE_WORK_SHIFTSSHIFT_RATE: TFloatField;
    DM_COMPANY: TDM_COMPANY;
    EE_WORK_SHIFTSNameLKUP: TStringField;
    procedure EE_WORK_SHIFTSBeforePost(DataSet: TDataSet);
    procedure EE_WORK_SHIFTSCO_SHIFTS_NBRChange(Sender: TField);
    procedure EE_WORK_SHIFTSBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  EvUIUtils, EvTypes, EvUtils;

{$R *.DFM}
procedure TDM_EE_WORK_SHIFTS.EE_WORK_SHIFTSBeforePost(DataSet: TDataSet);
var
  bEdit: Boolean;
begin
  if EE_WORK_SHIFTSSHIFT_PERCENTAGE.IsNull and EE_WORK_SHIFTSSHIFT_RATE.IsNull then
  begin
    EvErrMessage('Please enter a shift differential rate or percentage.');
    AbortEx;
  end;

  DM_EMPLOYEE.EE.Activate;
  if DM_EMPLOYEE.EE['EE_NBR'] <> DataSet['EE_NBR'] then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DataSet['EE_NBR'], []);
  if not DM_EMPLOYEE.EE.FieldByName('AUTOPAY_CO_SHIFTS_NBR').IsNull and
     (DM_EMPLOYEE.EE['AUTOPAY_CO_SHIFTS_NBR'] = DataSet.FieldByName('CO_SHIFTS_NBR').OldValue) then
  begin
    bEdit := TevClientDataSet(DM_EMPLOYEE.EE).State in [dsInsert, dsEdit];
    if not bEdit then
      DM_EMPLOYEE.EE.Edit;
    DM_EMPLOYEE.EE['AUTOPAY_CO_SHIFTS_NBR'] := DataSet['CO_SHIFTS_NBR'];
    if not bEdit then
      DM_EMPLOYEE.EE.Post;
  end;
end;

procedure TDM_EE_WORK_SHIFTS.EE_WORK_SHIFTSCO_SHIFTS_NBRChange(
  Sender: TField);
var
  v: variant;
begin
  if DM_EMPLOYEE.EE_WORK_SHIFTS.State = dsEdit then
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then
      Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('Columns', 'count(*) LINES_COUNT');
      SetMacro('Table1', 'PR_CHECK');
      SetMacro('Table2', 'PR_CHECK_LINES');
      SetMacro('JoinField', 'PR_CHECK');
      SetMacro('Condition', 'T2.CO_SHIFTS_NBR=' + EE_WORK_SHIFTSCO_SHIFTS_NBR.AsString);
      DataRequest(AsVariant);
    end;
    Open;
    if ConvertNull(FieldByName('LINES_COUNT').Value, 0) > 0 then
      raise EInconsistentData.CreateHelp('You can not change the reference to a company shift, because this EE shift is used in checks.', IDH_InconsistentData);
  end;

  if not DM_COMPANY.CO_SHIFTS.Active then
    DM_COMPANY.CO_SHIFTS.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  if DM_COMPANY.CO_SHIFTS.State = dsBrowse then
  begin
    V :=  DM_COMPANY.CO_SHIFTS.Lookup('CO_SHIFTS_NBR', EE_WORK_SHIFTSCO_SHIFTS_NBR.AsInteger, 'DEFAULT_AMOUNT;DEFAULT_PERCENTAGE');
    if VarIsArray(v) then
    begin
      if not VarIsNull( V[0] ) then
        EE_WORK_SHIFTSSHIFT_RATE.Value := V[0]
      else
        EE_WORK_SHIFTSSHIFT_RATE.Clear;
      if not VarIsNull( V[1] ) then
        EE_WORK_SHIFTSSHIFT_PERCENTAGE.Value := V[1]
      else
        EE_WORK_SHIFTSSHIFT_PERCENTAGE.Clear;
    end
  end;
end;

procedure TDM_EE_WORK_SHIFTS.EE_WORK_SHIFTSBeforeDelete(DataSet: TDataSet);
var
  bEdit: Boolean;
begin
  DM_EMPLOYEE.EE.Activate;
  if DM_EMPLOYEE.EE['EE_NBR'] <> DataSet['EE_NBR'] then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DataSet['EE_NBR'], []);
  if DM_EMPLOYEE.EE['AUTOPAY_CO_SHIFTS_NBR'] = DataSet['CO_SHIFTS_NBR'] then
  begin
    bEdit := TevClientDataSet(DM_EMPLOYEE.EE).State in [dsInsert, dsEdit];
    if not bEdit then
      DM_EMPLOYEE.EE.Edit;
    DM_EMPLOYEE.EE.FieldByName('AUTOPAY_CO_SHIFTS_NBR').Clear;
    if not bEdit then
      DM_EMPLOYEE.EE.Post;
  end;
end;

initialization
  RegisterClass( TDM_EE_WORK_SHIFTS );

finalization
  UnregisterClass( TDM_EE_WORK_SHIFTS );

end.
