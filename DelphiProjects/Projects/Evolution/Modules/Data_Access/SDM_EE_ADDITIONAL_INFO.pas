// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_ADDITIONAL_INFO;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents;

type
  TDM_EE_ADDITIONAL_INFO = class(TDM_ddTable)
    EE_ADDITIONAL_INFO: TEE_ADDITIONAL_INFO;
    EE_ADDITIONAL_INFOEE_ADDITIONAL_INFO_NBR: TIntegerField;
    EE_ADDITIONAL_INFOEE_NBR: TIntegerField;
    EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_NAMES_NBR: TIntegerField;
    EE_ADDITIONAL_INFOINFO_STRING: TStringField;
    EE_ADDITIONAL_INFOINFO_DATE: TDateField;
    EE_ADDITIONAL_INFOINFO_AMOUNT: TFloatField;
    EE_ADDITIONAL_INFOName: TStringField;
    DM_COMPANY: TDM_COMPANY;
    EE_ADDITIONAL_INFOValue: TStringField;
    EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_VALUES_NBR: TIntegerField;
    procedure EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_NAMES_NBRChange(
      Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_EE_ADDITIONAL_INFO.EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_NAMES_NBRChange(
  Sender: TField);
begin
  inherited;
  EE_ADDITIONAL_INFOCO_ADDITIONAL_INFO_VALUES_NBR.Clear;
end;

initialization
  RegisterClass(TDM_EE_ADDITIONAL_INFO);

finalization
  UnregisterClass(TDM_EE_ADDITIONAL_INFO);

end.
