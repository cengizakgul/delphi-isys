// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_DEPARTMENT_LOCALS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_DEPARTMENT_LOCALS = class(TDM_ddTable)
    DM_COMPANY: TDM_COMPANY;
    CO_DEPARTMENT_LOCALS: TCO_DEPARTMENT_LOCALS;
    CO_DEPARTMENT_LOCALSCO_DEPARTMENT_LOCALS_NBR: TIntegerField;
    CO_DEPARTMENT_LOCALSCO_DEPARTMENT_NBR: TIntegerField;
    CO_DEPARTMENT_LOCALSCO_LOCAL_TAX_NBR: TIntegerField;
    CO_DEPARTMENT_LOCALSLocalName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_DEPARTMENT_LOCALS);

finalization
  UnregisterClass(TDM_CO_DEPARTMENT_LOCALS);

end.
