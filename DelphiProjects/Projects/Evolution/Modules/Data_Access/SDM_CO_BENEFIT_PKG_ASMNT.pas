unit SDM_CO_BENEFIT_PKG_ASMNT;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, EvTypes, Variants, SDDClasses,
  SDataDictsystem, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, evUtils, SFieldCodeValues,
  EvClientDataSet;

type
  TDM_CO_BENEFIT_PKG_ASMNT = class(TDM_ddTable)
    CO_BENEFIT_PKG_ASMNT: TCO_BENEFIT_PKG_ASMNT;
    CO_BENEFIT_PKG_ASMNTPackageName: TStringField;
    DM_COMPANY: TDM_COMPANY;
    CO_BENEFIT_PKG_ASMNTCO_BENEFIT_PACKAGE_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTCO_BENEFIT_PKG_ASMNT_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTCO_DIVISION_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTCO_BRANCH_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTCO_DEPARTMENT_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTCO_TEAM_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTCO_GROUP_NBR: TIntegerField;
    CO_BENEFIT_PKG_ASMNTDivisionName: TStringField;
    CO_BENEFIT_PKG_ASMNTBranchName: TStringField;
    CO_BENEFIT_PKG_ASMNTDepartmentName: TStringField;
    CO_BENEFIT_PKG_ASMNTTeamName: TStringField;
    CO_BENEFIT_PKG_ASMNTGroupName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM_CO_BENEFIT_PKG_ASMNT: TDM_CO_BENEFIT_PKG_ASMNT;

implementation

{$R *.DFM}


initialization
  RegisterClass(TDM_CO_BENEFIT_PKG_ASMNT);

finalization
  UnregisterClass(TDM_CO_BENEFIT_PKG_ASMNT);

end.
