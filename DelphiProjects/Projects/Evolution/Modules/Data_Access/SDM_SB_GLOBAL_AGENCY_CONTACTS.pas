// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_GLOBAL_AGENCY_CONTACTS;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db;  

type
  TDM_SB_GLOBAL_AGENCY_CONTACTS = class(TDM_ddTable)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    SB_GLOBAL_AGENCY_CONTACTS: TSB_GLOBAL_AGENCY_CONTACTS;
    SB_GLOBAL_AGENCY_CONTACTSSB_GLOBAL_AGENCY_CONTACTS_NBR: TIntegerField;
    SB_GLOBAL_AGENCY_CONTACTSSY_GLOBAL_AGENCY_NBR: TIntegerField;
    SB_GLOBAL_AGENCY_CONTACTSCONTACT_NAME: TStringField;
    SB_GLOBAL_AGENCY_CONTACTSPHONE: TStringField;
    SB_GLOBAL_AGENCY_CONTACTSFAX: TStringField;
    SB_GLOBAL_AGENCY_CONTACTSE_MAIL_ADDRESS: TStringField;
    SB_GLOBAL_AGENCY_CONTACTSNOTES: TBlobField;
    procedure SB_GLOBAL_AGENCY_CONTACTSNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_SB_GLOBAL_AGENCY_CONTACTS.SB_GLOBAL_AGENCY_CONTACTSNewRecord(
  DataSet: TDataSet);
begin
  DataSet.FieldByName('SY_GLOBAL_AGENCY_NBR').Value :=
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('SY_GLOBAL_AGENCY_NBR').Value;
end;

initialization
  RegisterClass(TDM_SB_GLOBAL_AGENCY_CONTACTS);

finalization
  UnregisterClass(TDM_SB_GLOBAL_AGENCY_CONTACTS);

end.
