// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CL_DELIVERY_METHOD;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db, SDDClasses, SDataDictbureau, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  EvClientDataSet;

type
  TDM_CL_DELIVERY_METHOD = class(TDM_ddTable)
    CL_DELIVERY_METHOD: TCL_DELIVERY_METHOD;
    CL_DELIVERY_METHODServiceDescription_Lookup: TStringField;
    CL_DELIVERY_METHODNAME: TStringField;
    CL_DELIVERY_METHODCL_DELIVERY_METHOD_NBR: TIntegerField;
    CL_DELIVERY_METHODSB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField;
    CL_DELIVERY_METHODADDRESS1: TStringField;
    CL_DELIVERY_METHODADDRESS2: TStringField;
    CL_DELIVERY_METHODCITY: TStringField;
    CL_DELIVERY_METHODSTATE: TStringField;
    CL_DELIVERY_METHODZIP_CODE: TStringField;
    CL_DELIVERY_METHODATTENTION: TStringField;
    CL_DELIVERY_METHODPHONE_NUMBER: TStringField;
    CL_DELIVERY_METHODCOVER_SHEET_SB_REPORT_NBR: TIntegerField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CL_DELIVERY_METHOD);

finalization
  UnregisterClass(TDM_CL_DELIVERY_METHOD);

end.
