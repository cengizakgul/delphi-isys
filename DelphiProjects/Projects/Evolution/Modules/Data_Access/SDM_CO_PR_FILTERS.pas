// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_PR_FILTERS;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_PR_FILTERS = class(TDM_ddTable)
    CO_PR_FILTERS: TCO_PR_FILTERS;
    DM_COMPANY: TDM_COMPANY;
    CO_PR_FILTERSCO_PR_FILTERS_NBR: TIntegerField;
    CO_PR_FILTERSCO_NBR: TIntegerField;
    CO_PR_FILTERSNAME: TStringField;
    CO_PR_FILTERSCO_DIVISION_NBR: TIntegerField;
    CO_PR_FILTERSCO_BRANCH_NBR: TIntegerField;
    CO_PR_FILTERSCO_DEPARTMENT_NBR: TIntegerField;
    CO_PR_FILTERSCO_TEAM_NBR: TIntegerField;
    CO_PR_FILTERSCO_PAY_GROUP_NBR: TIntegerField;
    CO_PR_FILTERSCUSTOM_DIVISION_NUMBER: TStringField;
    CO_PR_FILTERSCUSTOM_BRANCH_NUMBER: TStringField;
    CO_PR_FILTERSCUSTOM_DEPARTMENT_NUMBER: TStringField;
    CO_PR_FILTERSCUSTOM_TEAM_NUMBER: TStringField;
    CO_PR_FILTERSPAY_GROUP_NAME: TStringField;
    CO_PR_FILTERSCUSTOM_DIVISION_NAME: TStringField;
    CO_PR_FILTERSCUSTOM_BRANCH_NAME: TStringField;
    CO_PR_FILTERSCUSTOM_DEPARTMENT_NAME: TStringField;
    CO_PR_FILTERSCUSTOM_TEAM_NAME: TStringField;
    procedure CO_PR_FILTERSBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDM_CO_PR_FILTERS.CO_PR_FILTERSBeforePost(DataSet: TDataSet);
begin
  if DataSet.FieldByName('CO_DIVISION_NBR').IsNull or
     DataSet.FieldByName('CO_BRANCH_NBR').IsNull or
     DataSet.FieldByName('CO_DEPARTMENT_NBR').IsNull then
    DataSet.FieldByName('CO_TEAM_NBR').Clear;
  if DataSet.FieldByName('CO_DIVISION_NBR').IsNull or
     DataSet.FieldByName('CO_BRANCH_NBR').IsNull then
    DataSet.FieldByName('CO_DEPARTMENT_NBR').Clear;
  if DataSet.FieldByName('CO_DIVISION_NBR').IsNull then
    DataSet.FieldByName('CO_BRANCH_NBR').Clear;
end;

initialization
  RegisterClass(TDM_CO_PR_FILTERS);

finalization
  UnregisterClass(TDM_CO_PR_FILTERS);

end.
