inherited DM_SY_FED_TAX_TABLE: TDM_SY_FED_TAX_TABLE
  OldCreateOrder = True
  Left = 276
  Top = 107
  Height = 200
  Width = 418
  object SY_FED_TAX_TABLE: TSY_FED_TAX_TABLE
    ProviderName = 'SY_FED_TAX_TABLE_PROV'
    PictureMasks.Strings = (
      
        'EXEMPTION_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]}'#9'T'#9'F'
      
        'SECOND_EIC_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      
        'SECOND_EIC_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]}'#9'T'#9'F'
      
        'FIRST_EIC_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F'
      
        'THIRD_EIC_ADDITIONAL_PERCENT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]' +
        '},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'FEDERAL_MINIMUM_WAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F'
      
        'FEDERAL_TIP_CREDIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F'
      
        'SUPPLEMENTAL_TAX_PERCENTAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]}' +
        ',.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'OASDI_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]' +
        ']]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]' +
        ']}'#9'T'#9'F'
      
        'OASDI_WAGE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]}'#9'T'#9'F'
      
        'MEDICARE_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'MEDICARE_WAGE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]}'#9'T'#9'F'
      
        'FUI_RATE_REAL'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'FUI_RATE_CREDIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'FUI_WAGE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]}'#9'T'#9'F'
      
        'SY_401K_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'SY_403B_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'SY_457_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]}'#9'T'#9'F'
      
        'SY_501C_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'SIMPLE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#' +
        ']]]}'#9'T'#9'F'
      
        'SEP_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]' +
        ']),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]' +
        '}'#9'T'#9'F'
      
        'DEFERRED_COMP_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]}'#9'T'#9'F'
      
        'FED_DEPOSIT_FREQ_THRESHOLD'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},' +
        '.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#' +
        '}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_FEDERAL_MINIMUM_WAGE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_FEDERAL_TIP_CREDIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*' +
        '#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_SUPPLEMENTAL_TAX_PERCENT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#' +
        ']},.#*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.' +
        '#*#}[E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_OASDI_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'NEXT_OASDI_WAGE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_MEDICARE_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[' +
        '[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_MEDICARE_WAGE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_FUTA_RATE_REAL'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_FUTA_RATE_CREDIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}' +
        '[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_FUTA_WAGE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_401K_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'NEXT_403B_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'NEXT_457_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]}'#9'T'#9'F'
      
        'NEXT_501C_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]}'#9'T'#9'F'
      
        'NEXT_SIMPLE_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[' +
        '+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]}'#9'T'#9'F'
      
        'NEXT_SEP_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-' +
        ']#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#' +
        '][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]}'#9'T'#9'F'
      
        'NEXT_DEFERRED_COMP_LIMIT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#' +
        '*#}[E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E' +
        '[[+,-]#[#][#]]]}'#9'T'#9'F'
      
        'NEXT_FDFT_AMOUNT'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#' +
        '[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[' +
        '#][#]]]}'#9'T'#9'F'
      
        'ER_OASDI_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F'
      
        'EE_OASDI_RATE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]' +
        '#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#]' +
        '[#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][' +
        '#]]]}'#9'T'#9'F')
    FieldDefs = <
      item
        Name = 'DEFERRED_COMP_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'EXEMPTION_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'FEDERAL_MINIMUM_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'FEDERAL_TIP_CREDIT'
        DataType = ftFloat
      end
      item
        Name = 'FED_DEPOSIT_FREQ_THRESHOLD'
        DataType = ftFloat
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'FIRST_EIC_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'FIRST_EIC_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'FUI_RATE_CREDIT'
        DataType = ftFloat
      end
      item
        Name = 'FUI_RATE_REAL'
        DataType = ftFloat
      end
      item
        Name = 'FUI_WAGE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'MEDICARE_RATE'
        DataType = ftFloat
      end
      item
        Name = 'MEDICARE_WAGE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'NFMW_EFFECTIVE_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'OASDI_RATE'
        DataType = ftFloat
      end
      item
        Name = 'OASDI_WAGE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SECOND_EIC_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'SECOND_EIC_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SEP_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SIMPLE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SUPPLEMENTAL_TAX_PERCENTAGE'
        DataType = ftFloat
      end
      item
        Name = 'SY_401K_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_403B_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_457_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_501C_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_FED_TAX_TABLE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'THIRD_EIC_ADDITIONAL_PERCENT'
        DataType = ftFloat
      end
      item
        Name = 'SY_CATCH_UP_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_DEPENDENT_CARE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_HSA_FAMILY_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'SY_HSA_SINGLE_LIMIT'
        DataType = ftFloat
      end
      item
        Name = 'EE_OASDI_RATE'
        DataType = ftFloat
      end
      item
        Name = 'ER_OASDI_RATE'
        DataType = ftFloat
      end
      item
        Name = 'EE_MED_TH_RATE'
        DataType = ftFloat
      end>
    Left = 65
    Top = 23
    object SY_FED_TAX_TABLEDEFERRED_COMP_LIMIT: TFloatField
      FieldName = 'DEFERRED_COMP_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEEXEMPTION_AMOUNT: TFloatField
      FieldName = 'EXEMPTION_AMOUNT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFEDERAL_MINIMUM_WAGE: TFloatField
      FieldName = 'FEDERAL_MINIMUM_WAGE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFEDERAL_TIP_CREDIT: TFloatField
      FieldName = 'FEDERAL_TIP_CREDIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFED_DEPOSIT_FREQ_THRESHOLD: TFloatField
      FieldName = 'FED_DEPOSIT_FREQ_THRESHOLD'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
    object SY_FED_TAX_TABLEFIRST_EIC_LIMIT: TFloatField
      FieldName = 'FIRST_EIC_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFIRST_EIC_PERCENTAGE: TFloatField
      FieldName = 'FIRST_EIC_PERCENTAGE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_FED_TAX_TABLEFUI_RATE_CREDIT: TFloatField
      FieldName = 'FUI_RATE_CREDIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFUI_RATE_REAL: TFloatField
      FieldName = 'FUI_RATE_REAL'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFUI_WAGE_LIMIT: TFloatField
      FieldName = 'FUI_WAGE_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEMEDICARE_RATE: TFloatField
      FieldName = 'MEDICARE_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEMEDICARE_WAGE_LIMIT: TFloatField
      FieldName = 'MEDICARE_WAGE_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLENFMW_EFFECTIVE_DATE: TDateTimeField
      FieldName = 'NFMW_EFFECTIVE_DATE'
    end
    object SY_FED_TAX_TABLEOASDI_RATE: TFloatField
      FieldName = 'OASDI_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEOASDI_WAGE_LIMIT: TFloatField
      FieldName = 'OASDI_WAGE_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESECOND_EIC_AMOUNT: TFloatField
      FieldName = 'SECOND_EIC_AMOUNT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESECOND_EIC_LIMIT: TFloatField
      FieldName = 'SECOND_EIC_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESEP_LIMIT: TFloatField
      FieldName = 'SEP_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESIMPLE_LIMIT: TFloatField
      FieldName = 'SIMPLE_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESUPPLEMENTAL_TAX_PERCENTAGE: TFloatField
      FieldName = 'SUPPLEMENTAL_TAX_PERCENTAGE'
      DisplayFormat = '#,##0.00####'
    end
    object SY_FED_TAX_TABLESY_401K_LIMIT: TFloatField
      FieldName = 'SY_401K_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESY_403B_LIMIT: TFloatField
      FieldName = 'SY_403B_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESY_457_LIMIT: TFloatField
      FieldName = 'SY_457_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESY_501C_LIMIT: TFloatField
      FieldName = 'SY_501C_LIMIT'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLESY_FED_TAX_TABLE_NBR: TIntegerField
      FieldName = 'SY_FED_TAX_TABLE_NBR'
      Required = True
    end
    object SY_FED_TAX_TABLETHIRD_EIC_ADDITIONAL_PERCENT: TFloatField
      FieldName = 'THIRD_EIC_ADDITIONAL_PERCENT'
      DisplayFormat = '#,##0.00####'
    end
    object SY_FED_TAX_TABLESY_CATCH_UP_LIMIT: TFloatField
      FieldName = 'SY_CATCH_UP_LIMIT'
    end
    object SY_FED_TAX_TABLESY_DEPENDENT_CARE_LIMIT: TFloatField
      FieldName = 'SY_DEPENDENT_CARE_LIMIT'
    end
    object SY_FED_TAX_TABLESY_HSA_FAMILY_LIMIT: TFloatField
      FieldName = 'SY_HSA_FAMILY_LIMIT'
    end
    object SY_FED_TAX_TABLESY_HSA_SINGLE_LIMIT: TFloatField
      FieldName = 'SY_HSA_SINGLE_LIMIT'
    end
    object SY_FED_TAX_TABLEEE_OASDI_RATE: TFloatField
      FieldName = 'EE_OASDI_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEER_OASDI_RATE: TFloatField
      FieldName = 'ER_OASDI_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEEE_MED_TH_RATE: TFloatField
      FieldName = 'EE_MED_TH_RATE'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEFED_POVERTY_LEVEL: TFloatField
      FieldName = 'FED_POVERTY_LEVEL'
      DisplayFormat = '#,##0.00##'
    end
    object SY_FED_TAX_TABLEACA_AFFORD_PERCENT: TCurrencyField
      FieldName = 'ACA_AFFORD_PERCENT'
      DisplayFormat = '#,##0.00##'
    end
  end
end
