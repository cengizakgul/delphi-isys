inherited DM_EE_PIECE_WORK: TDM_EE_PIECE_WORK
  OldCreateOrder = True
  Left = 128
  Top = 243
  Height = 540
  Width = 783
  object DM_CLIENT: TDM_CLIENT
    Left = 56
    Top = 48
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 152
    Top = 56
  end
  object EE_PIECE_WORK: TEE_PIECE_WORK
    ProviderName = 'EE_PIECE_WORK_PROV'
    Left = 88
    Top = 136
    object EE_PIECE_WORKCL_PIECES_NBR: TIntegerField
      FieldName = 'CL_PIECES_NBR'
      OnChange = EE_PIECE_WORKCL_PIECES_NBRChange
    end
    object EE_PIECE_WORKEE_PIECE_WORK_NBR: TIntegerField
      FieldName = 'EE_PIECE_WORK_NBR'
    end
    object EE_PIECE_WORKEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object EE_PIECE_WORKRATE_QUANTITY: TFloatField
      FieldName = 'RATE_QUANTITY'
    end
    object EE_PIECE_WORKRATE_AMOUNT: TFloatField
      FieldName = 'RATE_AMOUNT'
      DisplayFormat = '#,##0.00####'
    end
    object EE_PIECE_WORKName: TStringField
      FieldKind = fkLookup
      FieldName = 'Name'
      LookupDataSet = DM_CL_PIECES.CL_PIECES
      LookupKeyFields = 'CL_PIECES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_PIECES_NBR'
      Size = 40
      Lookup = True
    end
  end
end
