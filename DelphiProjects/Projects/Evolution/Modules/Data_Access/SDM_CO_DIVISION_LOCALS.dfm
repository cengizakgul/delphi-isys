inherited DM_CO_DIVISION_LOCALS: TDM_CO_DIVISION_LOCALS
  OldCreateOrder = False
  Left = 163
  Top = 195
  Height = 375
  Width = 544
  object DM_COMPANY: TDM_COMPANY
    Left = 152
    Top = 24
  end
  object CO_DIVISION_LOCALS: TCO_DIVISION_LOCALS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_DIVISION_LOCALS_PROV'
    ValidateWithMask = True
    Left = 56
    Top = 32
    object CO_DIVISION_LOCALSLocalNsme: TStringField
      DisplayLabel = 'Local Name'
      FieldKind = fkLookup
      FieldName = 'LocalName'
      LookupDataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
      LookupKeyFields = 'CO_LOCAL_TAX_NBR'
      LookupResultField = 'LocalName'
      KeyFields = 'CO_LOCAL_TAX_NBR'
      Size = 40
      Lookup = True
    end
    object CO_DIVISION_LOCALSCO_DIVISION_LOCALS_NBR: TIntegerField
      FieldName = 'CO_DIVISION_LOCALS_NBR'
    end
    object CO_DIVISION_LOCALSCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object CO_DIVISION_LOCALSCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
  end
end
