inherited DM_PR_CHECK_SUI: TDM_PR_CHECK_SUI
  OldCreateOrder = False
  Left = 429
  Top = 460
  Height = 480
  Width = 696
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 104
  end
  object PR_CHECK_SUI: TPR_CHECK_SUI
    Aggregates = <>
    Params = <>
    ProviderName = 'PR_CHECK_SUI_PROV'
    BeforeInsert = PR_CHECK_SUIBeforeInsert
    BeforeEdit = PR_CHECK_SUIBeforeEdit
    BeforePost = PR_CHECK_SUIBeforePost
    BeforeDelete = PR_CHECK_SUIBeforeDelete
    ValidateWithMask = True
    Left = 200
    Top = 168
    object PR_CHECK_SUIPR_CHECK_SUI_NBR: TIntegerField
      FieldName = 'PR_CHECK_SUI_NBR'
      Origin = '"PR_CHECK_SUI"."PR_CHECK_SUI_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object PR_CHECK_SUIPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
      Origin = '"PR_CHECK_SUI"."PR_CHECK_NBR"'
      Required = True
    end
    object PR_CHECK_SUICO_SUI_NBR: TIntegerField
      FieldName = 'CO_SUI_NBR'
      Origin = '"PR_CHECK_SUI"."CO_SUI_NBR"'
      Required = True
    end
    object PR_CHECK_SUISUI_TAXABLE_WAGES: TFloatField
      FieldName = 'SUI_TAXABLE_WAGES'
      Origin = '"PR_CHECK_SUI"."SUI_TAXABLE_WAGES"'
    end
    object PR_CHECK_SUISUI_TAX: TFloatField
      FieldName = 'SUI_TAX'
      Origin = '"PR_CHECK_SUI"."SUI_TAX"'
    end
    object PR_CHECK_SUIOVERRIDE_AMOUNT: TFloatField
      FieldName = 'OVERRIDE_AMOUNT'
      Origin = '"PR_CHECK_SUI"."OVERRIDE_AMOUNT"'
    end
    object PR_CHECK_SUISUI_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'SUI_Lookup'
      LookupDataSet = DM_CO_SUI.CO_SUI
      LookupKeyFields = 'CO_SUI_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_SUI_NBR'
      Size = 40
      Lookup = True
    end
    object PR_CHECK_SUISUI_GROSS_WAGES: TFloatField
      FieldName = 'SUI_GROSS_WAGES'
      Origin = '"PR_CHECK_SUI"."SUI_GROSS_WAGES"'
    end
    object PR_CHECK_SUIPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 280
    Top = 104
  end
end
