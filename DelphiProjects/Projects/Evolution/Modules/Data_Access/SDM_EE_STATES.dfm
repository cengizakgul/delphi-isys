inherited DM_EE_STATES: TDM_EE_STATES
  OldCreateOrder = True
  Left = 817
  Top = 160
  Height = 479
  Width = 741
  object EE_STATES: TEE_STATES
    ProviderName = 'EE_STATES_PROV'
    AfterInsert = EE_STATESAfterInsert
    BeforePost = EE_STATESBeforePost
    BeforeDelete = EE_STATESBeforeDelete
    OnCalcFields = EE_STATESCalcFields
    OnNewRecord = EE_STATESNewRecord
    Left = 57
    Top = 30
    object EE_STATESCO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_STATES_NBR'
    end
    object EE_STATESSTATE_MARITAL_STATUS: TStringField
      DisplayWidth = 4
      FieldName = 'STATE_MARITAL_STATUS'
      OnValidate = EE_STATESSTATE_MARITAL_STATUSValidate
      Size = 4
    end
    object EE_STATESSTATE_NUMBER_WITHHOLDING_ALLOW: TIntegerField
      DisplayWidth = 10
      FieldName = 'STATE_NUMBER_WITHHOLDING_ALLOW'
    end
    object EE_STATESSTATE_EXEMPT_EXCLUDE: TStringField
      DisplayWidth = 1
      FieldName = 'STATE_EXEMPT_EXCLUDE'
      OnValidate = EE_STATESSTATE_EXEMPT_EXCLUDEValidate
      Size = 1
    end
    object EE_STATESOVERRIDE_STATE_TAX_VALUE: TFloatField
      DisplayWidth = 10
      FieldName = 'OVERRIDE_STATE_TAX_VALUE'
    end
    object EE_STATESOVERRIDE_STATE_TAX_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'OVERRIDE_STATE_TAX_TYPE'
      Size = 1
    end
    object EE_STATESSDI_APPLY_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SDI_APPLY_CO_STATES_NBR'
    end
    object EE_STATESSUI_APPLY_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SUI_APPLY_CO_STATES_NBR'
    end
    object EE_STATESEE_SDI_EXEMPT_EXCLUDE: TStringField
      DisplayWidth = 1
      FieldName = 'EE_SDI_EXEMPT_EXCLUDE'
      OnValidate = EE_STATESSTATE_EXEMPT_EXCLUDEValidate
      Size = 1
    end
    object EE_STATESER_SDI_EXEMPT_EXCLUDE: TStringField
      DisplayWidth = 1
      FieldName = 'ER_SDI_EXEMPT_EXCLUDE'
      OnValidate = EE_STATESSTATE_EXEMPT_EXCLUDEValidate
      Size = 1
    end
    object EE_STATESEE_SUI_EXEMPT_EXCLUDE: TStringField
      DisplayWidth = 1
      FieldName = 'EE_SUI_EXEMPT_EXCLUDE'
      OnValidate = EE_STATESSTATE_EXEMPT_EXCLUDEValidate
      Size = 1
    end
    object EE_STATESER_SUI_EXEMPT_EXCLUDE: TStringField
      DisplayWidth = 1
      FieldName = 'ER_SUI_EXEMPT_EXCLUDE'
      OnValidate = EE_STATESSTATE_EXEMPT_EXCLUDEValidate
      Size = 1
    end
    object EE_STATESRECIPROCAL_METHOD: TStringField
      DisplayWidth = 1
      FieldName = 'RECIPROCAL_METHOD'
      Size = 1
    end
    object EE_STATESRECIPROCAL_CO_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'RECIPROCAL_CO_STATES_NBR'
    end
    object EE_STATESRECIPROCAL_AMOUNT_PERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'RECIPROCAL_AMOUNT_PERCENTAGE'
      DisplayFormat = '#,##0.####'
    end
    object EE_STATESEE_STATES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_STATES_NBR'
      Visible = False
    end
    object EE_STATESEE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_NBR'
      Visible = False
    end
    object EE_STATESFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object EE_STATESState_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'State_Lookup'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object EE_STATESSDI_State_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'SDI_State_Lookup'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'SDI_APPLY_CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object EE_STATESSUI_State_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'SUI_State_Lookup'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'SUI_APPLY_CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object EE_STATESReciprocal_State_Lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'Reciprocal_State_Lookup'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'RECIPROCAL_CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object EE_STATESEE_COUNTY: TStringField
      FieldName = 'EE_COUNTY'
    end
    object EE_STATESEE_TAX_CODE: TStringField
      FieldName = 'EE_TAX_CODE'
    end
    object EE_STATESIMPORTED_MARITAL_STATUS: TStringField
      FieldName = 'IMPORTED_MARITAL_STATUS'
      Size = 1
    end
    object EE_STATESCALCULATE_TAXABLE_WAGES_1099: TStringField
      FieldName = 'CALCULATE_TAXABLE_WAGES_1099'
      Size = 1
    end
    object EE_STATESSocCodeMask: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SocCodeMask'
      Size = 7
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 54
    Top = 81
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 141
    Top = 81
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 112
    Top = 192
  end
end
