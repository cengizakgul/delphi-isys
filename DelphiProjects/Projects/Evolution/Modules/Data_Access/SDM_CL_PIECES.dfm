inherited DM_CL_PIECES: TDM_CL_PIECES
  OldCreateOrder = True
  Left = 142
  Top = 106
  Height = 540
  Width = 782
  object CL_PIECES: TCL_PIECES
    ProviderName = 'CL_PIECES_PROV'
    BeforePost = CL_PIECESBeforePost
    Left = 64
    Top = 72
    object CL_PIECESDEFAULT_RATE: TFloatField
      FieldName = 'DEFAULT_RATE'
      DisplayFormat = '#,##0.00####'
    end
  end
end
