// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_HR_PERFORMANCE_RATINGS;

interface

uses
  SDM_ddTable, SDataDictClient,
  SysUtils, Classes, DB,   SDataStructure, EvUtils, EvTypes,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions;

type
  TDM_EE_HR_PERFORMANCE_RATINGS = class(TDM_ddTable)
    EE_HR_PERFORMANCE_RATINGS: TEE_HR_PERFORMANCE_RATINGS;
    EE_HR_PERFORMANCE_RATINGSEE_HR_PERFORMANCE_RATINGS_NBR: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSEE_NBR: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSCO_HR_PERFORMANCE_RATINGS_NBR: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSOVERALL_RATING: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSREVIEW_DATE: TDateField;
    EE_HR_PERFORMANCE_RATINGSCO_HR_SUPERVISORS_NBR: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSNOTES: TBlobField;
    EE_HR_PERFORMANCE_RATINGSCL_HR_REASON_CODES_NBR: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSINCREASE_AMOUNT: TFloatField;
    EE_HR_PERFORMANCE_RATINGSINCREASE_TYPE: TStringField;
    EE_HR_PERFORMANCE_RATINGSINCREASE_EFFECTIVE_DATE: TDateField;
    EE_HR_PERFORMANCE_RATINGSRATE_NUMBER: TIntegerField;
    EE_HR_PERFORMANCE_RATINGSPerformanceLookup: TStringField;
    DM_COMPANY: TDM_COMPANY;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EE_HR_PERFORMANCE_RATINGSReasonLookup: TStringField;
    procedure EE_HR_PERFORMANCE_RATINGSBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TDM_EE_HR_PERFORMANCE_RATINGS.EE_HR_PERFORMANCE_RATINGSBeforePost(
  DataSet: TDataSet);
begin
  DM_EMPLOYEE.EE.Activate;
  Assert((DM_EMPLOYEE.EE['EE_NBR'] = DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['EE_NBR']) or DM_EMPLOYEE.EE.Locate('EE_NBR', DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['EE_NBR'], []));
  if DataSet.FieldByName('Rate_Number').AsInteger = 0 then
  begin
    if DM_EMPLOYEE.EE.FieldByName('Salary_Amount').AsCurrency = 0 then
      raise EInconsistentData.CreateHelp('Employee ' + DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString + ' does not have a salary', IDH_InconsistentData);
  end
  else
  begin
    DM_EMPLOYEE.EE_RATES.Activate;
    if not DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER', DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['EE_NBR;RATE_NUMBER'], []) then
      raise EInconsistentData.CreateHelp('Employee ' + DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString + ' does not have a rate number ' + DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FieldByName('RATE_NUMBER').AsString, IDH_InconsistentData);
  end;
end;

initialization
  RegisterClass(TDM_EE_HR_PERFORMANCE_RATINGS);

finalization
  UnregisterClass(TDM_EE_HR_PERFORMANCE_RATINGS);

end.
