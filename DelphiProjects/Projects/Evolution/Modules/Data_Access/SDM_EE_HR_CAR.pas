// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_EE_HR_CAR;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db,   Variants;

type
  TDM_EE_HR_CAR = class(TDM_ddTable)
    EE_HR_CAR: TEE_HR_CAR;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EE_HR_CARCO_HR_CAR_NBR: TIntegerField;
    EE_HR_CAREE_HR_CAR_NBR: TIntegerField;
    EE_HR_CAREE_NBR: TIntegerField;
    DM_COMPANY: TDM_COMPANY;
    EE_HR_CARCarDescr: TStringField;
    procedure EE_HR_CARCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
evutils;
{$R *.DFM}

procedure TDM_EE_HR_CAR.EE_HR_CARCalcFields(DataSet: TDataSet);
begin
  EE_HR_CARCarDescr.Clear;
  if not DM_COMPANY.CO_HR_CAR.Active then
    DM_COMPANY.CO_HR_CAR.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  if DM_COMPANY.CO_HR_CAR.Active
  and DM_COMPANY.CO_HR_CAR.ShadowDataSet.CachedFindKey(EE_HR_CARCO_HR_CAR_NBR.AsInteger) then
    EE_HR_CARCarDescr.AsString := DM_COMPANY.CO_HR_CAR.ShadowDataSet.FieldByName('MODEL').AsString+ ' '+
      DM_COMPANY.CO_HR_CAR.ShadowDataSet.FieldByName('VIN_NUMBER').AsString;
end;

initialization
  RegisterClass( TDM_EE_HR_CAR );

finalization
  UnregisterClass( TDM_EE_HR_CAR );

end.
