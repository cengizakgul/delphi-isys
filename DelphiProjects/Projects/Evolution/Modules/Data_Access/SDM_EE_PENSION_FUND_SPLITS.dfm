inherited DM_EE_PENSION_FUND_SPLITS: TDM_EE_PENSION_FUND_SPLITS
  Height = 374
  Width = 426
  object EE_PENSION_FUND_SPLITS: TEE_PENSION_FUND_SPLITS
    ProviderName = 'EE_PENSION_FUND_SPLITS_PROV'
    Left = 78
    Top = 29
    object EE_PENSION_FUND_SPLITSEE_PENSION_FUND_SPLITS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_PENSION_FUND_SPLITS_NBR'
    end
    object EE_PENSION_FUND_SPLITSEE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'EE_NBR'
    end
    object EE_PENSION_FUND_SPLITSCL_FUNDS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_FUNDS_NBR'
    end
    object EE_PENSION_FUND_SPLITSCL_E_DS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_E_DS_NBR'
    end
    object EE_PENSION_FUND_SPLITSPERCENTAGE: TFloatField
      DisplayWidth = 10
      FieldName = 'PERCENTAGE'
    end
    object EE_PENSION_FUND_SPLITSEMPLOYEE_OR_EMPLOYER: TStringField
      DisplayWidth = 1
      FieldName = 'EMPLOYEE_OR_EMPLOYER'
      Size = 1
    end
    object EE_PENSION_FUND_SPLITSFunds_lookup: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'Funds_lookup'
      LookupDataSet = DM_CL_FUNDS.CL_FUNDS
      LookupKeyFields = 'CL_FUNDS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_FUNDS_NBR'
      Size = 40
      Lookup = True
    end
    object EE_PENSION_FUND_SPLITSE_D_Code_lookup: TStringField
      FieldKind = fkLookup
      FieldName = 'E_D_Code_lookup'
      LookupDataSet = DM_CL_E_DS.CL_E_DS
      LookupKeyFields = 'CL_E_DS_NBR'
      LookupResultField = 'CUSTOM_E_D_CODE_NUMBER'
      KeyFields = 'CL_E_DS_NBR'
      Lookup = True
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 75
    Top = 93
  end
end
