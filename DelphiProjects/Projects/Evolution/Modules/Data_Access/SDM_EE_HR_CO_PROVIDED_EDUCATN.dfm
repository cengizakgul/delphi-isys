inherited DM_EE_HR_CO_PROVIDED_EDUCATN: TDM_EE_HR_CO_PROVIDED_EDUCATN
  OldCreateOrder = True
  Left = 544
  Top = 291
  Height = 540
  Width = 783
  object EE_HR_CO_PROVIDED_EDUCATN: TEE_HR_CO_PROVIDED_EDUCATN
    ProviderName = 'EE_HR_CO_PROVIDED_EDUCATN_PROV'
    FieldDefs = <
      item
        Name = 'CHECK_ISSUED_BY'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'CHECK_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'COMPANY_COST'
        DataType = ftFloat
      end
      item
        Name = 'COMPLETED'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'COURSE'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'DATE_FINISH'
        DataType = ftDate
      end
      item
        Name = 'DATE_ISSUED'
        DataType = ftDate
      end
      item
        Name = 'DATE_START'
        DataType = ftDate
      end
      item
        Name = 'EE_HR_CO_PROVIDED_EDUCATN_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'EE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 8
      end
      item
        Name = 'TAXABLE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_HR_SCHOOL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'LAST_REVIEW_DATE'
        DataType = ftDate
      end
      item
        Name = 'RENEWAL_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_HR_COURSE_NBR'
        DataType = ftInteger
      end>
    OnCalcFields = EE_HR_CO_PROVIDED_EDUCATNCalcFields
    Left = 96
    Top = 40
    object EE_HR_CO_PROVIDED_EDUCATNCHECK_ISSUED_BY: TStringField
      FieldName = 'CHECK_ISSUED_BY'
      Size = 3
    end
    object EE_HR_CO_PROVIDED_EDUCATNCHECK_NUMBER: TIntegerField
      FieldName = 'CHECK_NUMBER'
    end
    object EE_HR_CO_PROVIDED_EDUCATNCOMPANY_COST: TFloatField
      FieldName = 'COMPANY_COST'
      DisplayFormat = '#,##0.00'
    end
    object EE_HR_CO_PROVIDED_EDUCATNCOMPLETED: TStringField
      FieldName = 'COMPLETED'
      Required = True
      Size = 1
    end
    object EE_HR_CO_PROVIDED_EDUCATNCOURSE: TStringField
      FieldName = 'COURSE'
      Size = 40
    end
    object EE_HR_CO_PROVIDED_EDUCATNDATE_FINISH: TDateField
      FieldName = 'DATE_FINISH'
    end
    object EE_HR_CO_PROVIDED_EDUCATNDATE_ISSUED: TDateField
      FieldName = 'DATE_ISSUED'
    end
    object EE_HR_CO_PROVIDED_EDUCATNDATE_START: TDateField
      FieldName = 'DATE_START'
    end
    object EE_HR_CO_PROVIDED_EDUCATNEE_HR_CO_PROVIDED_EDUCATN_NBR: TIntegerField
      FieldName = 'EE_HR_CO_PROVIDED_EDUCATN_NBR'
      Required = True
    end
    object EE_HR_CO_PROVIDED_EDUCATNEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Required = True
    end
    object EE_HR_CO_PROVIDED_EDUCATNNOTES: TBlobField
      FieldName = 'NOTES'
      Size = 8
    end
    object EE_HR_CO_PROVIDED_EDUCATNTAXABLE: TStringField
      FieldName = 'TAXABLE'
      Required = True
      Size = 1
    end
    object EE_HR_CO_PROVIDED_EDUCATNSchool: TStringField
      FieldKind = fkLookup
      FieldName = 'School'
      LookupDataSet = DM_CL_HR_SCHOOL.CL_HR_SCHOOL
      LookupKeyFields = 'CL_HR_SCHOOL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_HR_SCHOOL_NBR'
      Size = 40
      Lookup = True
    end
    object EE_HR_CO_PROVIDED_EDUCATNCL_HR_SCHOOL_NBR: TIntegerField
      FieldName = 'CL_HR_SCHOOL_NBR'
    end
    object EE_HR_CO_PROVIDED_EDUCATNLAST_REVIEW_DATE: TDateField
      FieldName = 'LAST_REVIEW_DATE'
    end
    object EE_HR_CO_PROVIDED_EDUCATNRENEWAL_STATUS: TStringField
      FieldName = 'RENEWAL_STATUS'
      Size = 1
    end
    object EE_HR_CO_PROVIDED_EDUCATNCOURSE_DESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'COURSE_DESC'
      Calculated = True
    end
    object EE_HR_CO_PROVIDED_EDUCATNCL_HR_COURSE_NBR: TIntegerField
      FieldName = 'CL_HR_COURSE_NBR'
    end
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 108
    Top = 150
  end
end
