inherited DM_SY_QUEUE_PRIORITY: TDM_SY_QUEUE_PRIORITY
  Left = 406
  Top = 468
  Height = 421
  Width = 614
  object SY_QUEUE_PRIORITY: TSY_QUEUE_PRIORITY
    ProviderName = 'SY_QUEUE_PRIORITY_PROV'
    Left = 57
    Top = 24
    object SY_QUEUE_PRIORITYSY_QUEUE_PRIORITY_NBR: TIntegerField
      FieldName = 'SY_QUEUE_PRIORITY_NBR'
      Origin = '"SY_QUEUE_PRIORITY_HIST"."SY_QUEUE_PRIORITY_NBR"'
      Required = True
    end
    object SY_QUEUE_PRIORITYTHREADS: TIntegerField
      FieldName = 'THREADS'
      Origin = '"SY_QUEUE_PRIORITY_HIST"."THREADS"'
      Required = True
      OnValidate = SY_QUEUE_PRIORITYTHREADSValidate
      DisplayFormat = '###'
    end
    object SY_QUEUE_PRIORITYMETHOD_NAME: TStringField
      FieldName = 'METHOD_NAME'
      Origin = '"SY_QUEUE_PRIORITY_HIST"."METHOD_NAME"'
      Required = True
      Size = 255
    end
    object SY_QUEUE_PRIORITYPRIORITY: TIntegerField
      FieldName = 'PRIORITY'
      Origin = '"SY_QUEUE_PRIORITY_HIST"."PRIORITY"'
      Required = True
    end
  end
end
