// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_CO_SALESPERSON;

interface

uses
  SDM_ddTable, SDataDictClient,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure;

type
  TDM_CO_SALESPERSON = class(TDM_ddTable)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    CO_SALESPERSON: TCO_SALESPERSON;
    CO_SALESPERSONCO_SALESPERSON_NBR: TIntegerField;
    CO_SALESPERSONCO_NBR: TIntegerField;
    CO_SALESPERSONSB_USER_NBR: TIntegerField;
    CO_SALESPERSONCOMMISSION_PERCENTAGE: TFloatField;
    CO_SALESPERSONNEXT_COMMISSION_PERCENTAGE: TFloatField;
    CO_SALESPERSONNEXT_COMMISSION_PERCENT_DATE: TDateTimeField;
    CO_SALESPERSONLAST_VISIT_DATE: TDateField;
    CO_SALESPERSONTYPE: TStringField;
    CO_SALESPERSONCHARGEBACK: TFloatField;
    CO_SALESPERSONPROJECTED_ANNUAL_COMMISSION: TFloatField;
    CO_SALESPERSONNOTES: TBlobField;
    CO_SALESPERSONName: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

initialization
  RegisterClass(TDM_CO_SALESPERSON);

finalization
  UnregisterClass(TDM_CO_SALESPERSON);

end.
