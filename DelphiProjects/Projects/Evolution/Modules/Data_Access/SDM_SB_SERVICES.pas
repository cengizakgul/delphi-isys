// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_SB_SERVICES;

interface

uses
  SDM_ddTable, SDataDictBureau,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   SDataStructure, Variants, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDataDicttemp, SDataDictclient, EvContext, EvUIComponents, EvClientDataSet;

type
  TDM_SB_SERVICES = class(TDM_ddTable)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_SERVICES: TSB_SERVICES;
    SB_SERVICESAR_ACCT: TStringField;
    SB_SERVICESINC_ACCT: TStringField;
    SB_SERVICESITEM: TStringField;
    SB_SERVICESSB_SERVICES_NBR: TIntegerField;
    SB_SERVICESSERVICE_NAME: TStringField;
    SB_SERVICESFREQUENCY: TStringField;
    SB_SERVICESMONTH_NUMBER: TStringField;
    SB_SERVICESSB_REPORTS_NBR: TIntegerField;
    SB_SERVICESCOMMISSION: TStringField;
    SB_SERVICESSALES_TAXABLE: TStringField;
    SB_SERVICESFILLER: TStringField;
    procedure SB_SERVICESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  evutils, SFieldCodeValues;
{$R *.DFM}


procedure TDM_SB_SERVICES.SB_SERVICESCalcFields(DataSet: TDataSet);
var
  s: string;
begin
  if (not DataSet.FieldByName('FILLER').IsNull) and not (DataSet.State in [dsEdit, dsInsert]) then
  begin
    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 10, 50);
    if trim(s) <> '' then
      Dataset.FieldByName('AR_ACCT').AsString := s;

    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 60, 50);
    if trim(s) <> '' then
      Dataset.FieldByName('INC_ACCT').AsString := s;

    s := ExtractFromFiller(DataSet.FieldByName('FILLER').AsString, 110,70);
    if trim(s) <> '' then
      Dataset.FieldByName('ITEM').AsString := s;
  end;
end;


initialization
  RegisterClass( TDM_SB_SERVICES );

finalization
  UnregisterClass( TDM_SB_SERVICES );

end.
