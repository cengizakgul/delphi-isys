inherited DM_CO_BATCH_STATES_OR_TEMPS: TDM_CO_BATCH_STATES_OR_TEMPS
  OldCreateOrder = False
  Left = 253
  Top = 107
  Height = 480
  Width = 696
  object CO_BATCH_STATES_OR_TEMPS: TCO_BATCH_STATES_OR_TEMPS
    Aggregates = <>
    Params = <>
    ProviderName = 'CO_BATCH_STATES_OR_TEMPS_PROV'
    PictureMasks.Strings = (
      
        'STATE_OVERRIDE_VALUE'#9'{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[' +
        'E[[+,-]#[#][#]]],({{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+' +
        ',-]#[#][#]]]),[-]{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,' +
        '-]#[#][#]]]}'#9'T'#9'F')
    ValidateWithMask = True
    Left = 200
    Top = 144
    object CO_BATCH_STATES_OR_TEMPSCO_BATCH_STATES_OR_TEMPS_NBR: TIntegerField
      FieldName = 'CO_BATCH_STATES_OR_TEMPS_NBR'
    end
    object CO_BATCH_STATES_OR_TEMPSCO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
    end
    object CO_BATCH_STATES_OR_TEMPSTAX_AT_SUPPLEMENTAL_RATE: TStringField
      FieldName = 'TAX_AT_SUPPLEMENTAL_RATE'
      Size = 1
    end
    object CO_BATCH_STATES_OR_TEMPSEXCLUDE_STATE: TStringField
      FieldName = 'EXCLUDE_STATE'
      Size = 1
    end
    object CO_BATCH_STATES_OR_TEMPSEXCLUDE_SDI: TStringField
      FieldName = 'EXCLUDE_SDI'
      Size = 1
    end
    object CO_BATCH_STATES_OR_TEMPSEXCLUDE_SUI: TStringField
      FieldName = 'EXCLUDE_SUI'
      Size = 1
    end
    object CO_BATCH_STATES_OR_TEMPSSTATE_OVERRIDE_TYPE: TStringField
      FieldName = 'STATE_OVERRIDE_TYPE'
      Size = 1
    end
    object CO_BATCH_STATES_OR_TEMPSSTATE_OVERRIDE_VALUE: TFloatField
      FieldName = 'STATE_OVERRIDE_VALUE'
    end
    object CO_BATCH_STATES_OR_TEMPSCO_PR_CHECK_TEMPLATES_NBR: TIntegerField
      FieldName = 'CO_PR_CHECK_TEMPLATES_NBR'
    end
    object CO_BATCH_STATES_OR_TEMPSEXCLUDE_ADDITIONAL_STATE: TStringField
      FieldName = 'EXCLUDE_ADDITIONAL_STATE'
      Size = 1
    end
    object CO_BATCH_STATES_OR_TEMPSState: TStringField
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_CO_STATES.CO_STATES
      LookupKeyFields = 'CO_STATES_NBR'
      LookupResultField = 'STATE'
      KeyFields = 'CO_STATES_NBR'
      Size = 2
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 200
    Top = 88
  end
end
