unit EvXmlRpcServerMod;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvMainboard, EvCommonInterfaces, XmlRpcServer, isBasicUtils,
     XmlRpcTypes, evTypes, EvDataset, EvUtils, evContext,  isErrorUtils, rwCustomDataDictionary,
     DB, Variants, SDDStreamClasses, EvStreamUtils, SReportSettings, EvConsts, ISZippingRoutines, isDataSet,
     isLogFile, isObjectPool, SDataStructure, SProcessTCImport, EvDataAccessComponents, StrUtils, EvLegacy,
     isSettings, EvInitApp, EvTransportShared, EvControllerInterfaces, EvTransportInterfaces, EvRemoteMethods,
     isAppIDs, SyncObjs, EvBasicUtils, isThreadManager, SDDClasses,
     evExceptions, isExceptions, EvExchangeResultLog, EvClasses, rwEngineTypes, EvClientDataSet;

const
  APIEventClass = 'API';
  StorageTableTimeout = 15;

implementation

uses EvESSCoStorageObject, EvExchangeMapFileExt, EvExchangeConsts, ActiveX,
  EvTOARequest, EvEEChangeRequest;

const
  REMOTABLE_CALC_PACKAGE = 4;
  REFRESH_EDS = 'REFRESHEDS';
  CALCULATE_TAXES = 'CALCULATETAXES';
  CALCULATE_CHECKLINES = 'CALCULATECHECKLINES';
  CREATE_CHECK = 'CREATECHECK';
  CREATE_MANUAL_CHECK = 'CREATEMANUALCHECK';
  CHANGECHECK_STATUS = 'CHANGECHECKSTATUS';
  NEXT_PAYROLLDATES = 'NEXTPAYROLLDATES';
  HOLIDAY_DAY = 'HOLIDAYDAY';
  TermsOfUseApprovalsFilename = 'TermsOfUseApprovals.dat';
  UsersCookiesFilename = 'UsersCookies.dat';
  CookieExpirationDays = 30;

type
  WrongSignatureException = class(Exception);
  WrongSessionException = class(Exception);
  WrongLicenseException = class(Exception);
  WrongReturnTypeException = class(Exception);
  WrongReportLevelException = class(Exception);
  WrongPackageNumberException = class(Exception);
  WrongAPIKeyException = class(Exception);

  UnsupportedParamTypeException = class(Exception);
  UnsupportedOutputParamTypeException = class(Exception);
  UnsupportedFunctionException = class(Exception);

  NotAllowedEEException = class(Exception);
  TableNameEmptyException = class(Exception);
  NotSSUserException = class(Exception);

  QueueTaskNotFoundException = class(Exception);
  QueueTaskNotFinishedException = class(Exception);
  CannotConvertNbrToIdException = class(Exception);
  CannotConvertIdToNbrException = class(Exception);

  UnknownParamTypeException = class(Exception);
  ResultWrongStructureException = class(Exception);
  ResultIsEmptyException = class(Exception);
  EmptyDatasetException = class(Exception);

  UnknownCoStorageTag = class(Exception);
  CannotGetDataDueTimeout = class(Exception);
  UnknownEEChangeRequestType = class(Exception);

  ESSNotEnabledException = class(Exception);
  SecurityCallsNotAllowed = class(Exception);
  ExpiredOrStolenCookie = class(Exception);

  TevRPCMethod = procedure (const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String) of object;
  PTevRPCMethod = ^TevRPCMethod;
  TTermsOfUse = (touDoNotCheck, touSetAccepted, touCheck);

  TevAPIKeyType = (apiWebClient, apiSelfServe, apiVendor);
  IevAPIKey = interface
  ['{D5DF1984-623F-4448-867D-2D8AC22D6DCF}']
    function GetAPIKey : TisGUID;
    function GetAPIKeyType : TevAPIKeyType;
    function GetVendorCustomNbr : integer;
    function GetVendorName : String;
    function GetApplicationName : String;

    procedure SetAPIKey(AAPIKey : TisGUID);
    procedure SetAPIKeyType(AAPIKeyType : TevAPIKeyType);
    procedure SetVendorCustomNbr(AVendorCustomNbr : integer);
    procedure SetVendorName(AVendorName : String);
    procedure SetApplicationName(AAplicationName : String);

    property APIKey : TisGUID read GetAPIKey write SetAPIKey;
    property APIKeyType : TevAPIKeyType read GetAPIKeyType write SetAPIKeyType;
    property ApplicationName : String read GetApplicationName write SetApplicationName;
    property VendorCustomNbr : integer read GetVendorCustomNbr write SetVendorCustomNbr; // 0 - is reserved for iSystems
    property VendorName : String read GetVendorName write SetVendorName;
  end;

  TevAPIKey = class(TisInterfacedObject, IevAPIKey)
  private
    FAPIKey : TisGUID;
    FAPIKeyType : TevAPIKeyType;
    FApplicationName : String;
    FVendorCustomNbr : integer;
    FVendorName : String;
  protected
    // IevAPIKey implementation
    procedure DoOnConstruction; override;
    function  GetAPIKey: TisGUID;
    function  GetAPIKeyType: TevAPIKeyType;
    function  GetVendorCustomNbr: integer;
    function  GetVendorName: String;
    function  GetApplicationName: String;
    procedure SetAPIKey(AAPIKey: TisGUID);
    procedure SetAPIKeyType(AAPIKeyType: TevAPIKeyType);
    procedure SetVendorCustomNbr(AVendorCustomNbr : integer);
    procedure SetVendorName(AVendorName: String);
    procedure SetApplicationName(AAplicationName: String);
    property  APIKey: TisGUID read GetAPIKey write SetAPIKey;
    property  APIKeyType: TevAPIKeyType read GetAPIKeyType write SetAPIKeyType;
    property  ApplicationName: String read GetApplicationName write SetApplicationName;
    property  VendorCustomNbr: integer read GetVendorCustomNbr write SetVendorCustomNbr;
    property  VendorName: String read GetVendorName write SetVendorName;
  public
    procedure WriteToStream(const AStream: IEvDualStream); override;
    procedure ReadFromStream(const AStream: IEvDualStream); override;

    class function GetTypeID: String; override;
  end;

  IUserRegisteredComputerInfo = interface
    ['{41C827D5-155C-42E0-9EA9-BD583103DB33}']
    function  GetCookieId: String;
    procedure SetCookieId(const AValue: String);
    function  GetComputerId: String;
    procedure SetComputerId(const AValue: String);
    function  GetToken: String;
    procedure SetToken(const AValue: String);
    function  GetLastAccessDate: TDateTime;
    procedure SetLastAccessdate(const AValue: TDateTime);
    function  GetUserComputerName: String;
    procedure SetUserComputerName(const AValue: String);
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
  end;

  TUserRegisteredComputerInfo = class(TisInterfacedObject, IUserRegisteredComputerInfo)
  private
    FCookieId: String;
    FComputerId: String;
    FToken: String;
    FLastAccessDate: TDateTime;
    FUserComputerName: String;
    FUserName: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);  override;

    // IUserRegisteredComputerInfo
    function  GetCookieId: String;
    procedure SetCookieId(const AValue: String);
    function  GetComputerId: String;
    procedure SetComputerId(const AValue: String);
    function  GetToken: String;
    procedure SetToken(const AValue: String);
    function  GetLastAccessDate: TDateTime;
    procedure SetLastAccessdate(const AValue: TDateTime);
    function  GetUserComputerName: String;
    procedure SetUserComputerName(const AValue: String);
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;

  IContextObjFromPool = interface
    ['{631C184F-EB83-48DE-B030-3414EE4B3211}']
    function  GetLicenseKey: IevAPIKey;
    procedure SetLicenseKey(ALicenseKey: IevAPIKey);
    function  GetContext: IevContext;
    procedure SetContext(AContext: IevContext);
    function  GetTermsOfUseAccepted: boolean;
    procedure SetTermsOfUseAccepted(const AValue: boolean);
  end;

  TContextObjFromPool = class(TisObjectFromPool, IisObjectFromPool, IContextObjFromPool)
  private
    FLicenseKey: IevAPIkey;
    FContext: IevContext;
    FTermsOfUseAccepted: boolean;
  public
    function  GetLicenseKey: IevAPIKey;
    procedure SetLicenseKey(ALicenseKey: IevAPIKey);
    function  GetContext: IevContext;
    procedure SetContext(AContext: IevContext);
    function  GetTermsOfUseAccepted: boolean;
    procedure SetTermsOfUseAccepted(const AValue: boolean);
    constructor Create(const AName: String); override;
    destructor Destroy; override;
  end;

  TevAAControl = class(TisInterfacedObject, IevAAControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
  end;

  TevAAController = class(TevCustomTCPServer, IevAppController)
  private
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;

  TevAAControllerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    FControl: IevAAControl;
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetSettings(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStackInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetSettings(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure DoOnConstruction; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure RegisterHandlers; override;
  end;

  TevXmlRPCServer = class(TisInterfacedObject, IevXmlRPCServer)
  private
    FRpcServer: TRpcServer;
    FMethods: TStringList;
    FContextPool : IisObjectPool;
    FLogAPIExceptions : boolean;
    FController: IevAppController;
    FBillingLog: IisListOfValues;
    FBillingSaverWatchDogId: TTaskId;
    FTermsOfUse: IisListOfValues;
    FTermOfseFilename: String;
    FUsersCookies: IisListOfValues;
    FUsersCookiesFilename: String;

    procedure RegisterMethods;
    procedure RPCHandler(const AMethodName: string; AList: IInterfaceList; AReturn: IRpcReturn);
    procedure RPCHandlerThread(const AParams: TTaskParamList);
    procedure SetCurrentContext(const AContextId: String; const ATermsOfUse: TTermsOfUse = touCheck);
    function  GetLicenseKeyFromContext(const AContextId: String) : IevAPIKey;
    procedure ClearCurrentContext;
    function  OldStructToDataSet(const AdsXml: IRpcStruct): TevClientDataSet;
    function  StructToDataSet(const AdsXml: IRpcStruct; out AMap: String): IevDataSet;
    function  DataSetToStruct(const ADataSet: IevDataSet; const sColumnMap: string; const ReturnTableNameFromMap: boolean = false): IRpcStruct; overload;
    procedure APICheckCondition(const ACondition: Boolean; AExceptClass: ExceptClass);
    function  APIPostDataSets(const ClientId: Integer; params: IRpcStruct): IRpcStruct;
    function  GetDataSet(const ClientId: Integer; sTableName, sTableCondition: string; const AsOfDate: TdateTime = 0): TevClientDataSet;
    function  ExecuteMethod(const functionName: string;  const param: IRpcArray): IRpcArray;
    function  TaskNbrToId(const ATaskNbr : String) : TisGUID;
    function  TaskIdToNbr(const ATaskId : TisGUID) : String;
    procedure CollectStatistics(AMethodName : String);
//    function  SendBillingInfo: boolean; // true if success
    procedure SaveUnsentBillingInfo;
    procedure LoadUnsentBillingInfo;
//    procedure BillingInfoSaverWatchDog(const Params : TTaskParamList);
    function  FindAPIKey(const AAPIKey: TisGUID): IevAPIKey;
    procedure EEChangeRequestsToRpcArray(const ARequestsList: IisInterfaceList; const ARpcArray: IRpcArray);
    function  ParamsToDataChangePacket(const AParams: IRPCStruct): IevDataChangePacket;
    function  DataChangePacketToParams(const AChangeRequestData: IevDataChangePacket): IRpcStruct;  // this is very simple converter which supports updates only
    function  RestoreStructureFromXML(const AXML: String): IRpcStruct;
    function  TOARequestToRpcStruct(const Requests: IisInterfaceList): IRPCArray;
    function  ProcessOldFormatEERequests(const ARequestsNbrs: IisStringList): IisStringList;
    function  ChangeUserPassword(const AContextId: String; const AuserName: String; const ANewPassword: String; const ADomain: String): String;
    function  CheckIfTermsOfUseAccepted(const AUserId: String; const AAPIKey: IevAPIKey): String;
    procedure UserAcceptedTermsOfUse(const AUserId: String; const ATimeStamp: TDateTime);
    procedure StoreTermsOfUse;
    function  FindAndUpdateCookie(const ACookieId: String; const AComputerId: String; const AToken: String): String;
    procedure StoreUsersCookies;
    function  IsUserCookieExpired(const ACookie: IUserRegisteredComputerInfo; const ANow: TDateTime): boolean;
    procedure CleanUpCookiesForUser(const AUserName: String);
    function  GetCookieId(const AUserName: String): String;

    // Methods
    procedure EE_GetW2List(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_GetW2(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_Connect(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_RegNewAccount(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_SetUserSecData(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);


    procedure SB_GetEmployee_E_DS(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_getEeScheduledEdAmount(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);


    procedure SB_Connect(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_OpenClient(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_TCImport(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_RunQBQuery(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetEmail(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetWCReportList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetPayrollTotals(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_SetUserSecData(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure SB_GetVMRReportList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetVMRReport (const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetManualTaxesListForCheck(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetRemoteVMRReportList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetRemoteVMRReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);


    procedure Disconnect(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure ChangePassword(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure ValidatePassword(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetSystemTime(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetAPIVersion(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetSequrityQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetSequrityExtQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetUserQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SetUserQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SetUserExtQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure ResetPassword(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetPasswordRules(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure CreateNewClientDB(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure ApplyDataChangePacket(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure ApplyManyDataChangePackets(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetSBInfo(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetDDTableInfo(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure AcceptTermsOfUse(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure RememberMe(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure ForgetMe(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetListOfRegisteredComputers(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure Legacy_OpenSQL(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure Legacy_OpenTable(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure Legacy_PostTableChanges(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure Legacy_DeleteTableValues(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure Legacy_ExecuteMethod(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure Legacy_ExecuteMethodCalc(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure SB_ReadCompanyStorage(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_WriteCompanyStorage(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_DeleteRecord(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_autoEnlistTOAForNewEE(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_autoEnlistEDsForNewEE(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetCoCalendarDefaults(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_CreateCoCalendar(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure EE_CreateEEChangeRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_GetMgrChangeRequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_GetEEChangeRequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_ProcessChangeRequests(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure EE_SaveTORequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_GetEETORequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_GetMgrTORequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure GetBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_StoreBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetBenefitRequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_SendEMailToHR(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetEeExistingBenefits(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_GetEeBenefitPackageAvaliableForEnrollment(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure DeleteBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetSummaryBenefitEnrollmentStatus(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_RevertBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_SubmitBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetBenefitRequestConfirmationReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_SendBenefitEMailToEE(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure GetEmptyBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_CloseBenefitEnrollment(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_ApproveBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetSwipeclockOneTimeURL(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetMRCOneTimeURL(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure TaskQueue_GetPDFReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_GetTaskList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_GetTaskSummary(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_DeleteTask(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_GetTaskDetails(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_IsTaskFinished(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_RunReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_RunPayrollReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_RunPreprocessPayroll(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_RunCreatePayroll(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_GetReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_EvoXImport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure TaskQueue_GetEvoXImportResults(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetReportParameters(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
    procedure SB_GetSysReportByNbr(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure SB_GetReportFileByNbrAndType(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure Payroll_GetHoliday(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure Security_GetAccountRights(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);

    procedure testHelloMethod(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
    procedure EE_ProcessTORequests(const AParams: IInterfaceList;
      const AReturn: IRpcReturn; const AMethodName: String);

    procedure GetEvBlobData(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
  protected
    procedure DoOnConstruction; override;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);

    function  GetSSLEnable: Boolean;
    procedure SetSSLEnable(const AValue: Boolean);

    function  GetSSLRootCertFile: String;
    procedure SetSSLRootCertFile(const AValue: String);
    function  GetSSLCertFile: String;
    procedure SetSSLCertFile(const AValue: String);
    function  GetSSLKeyFile: String;
    procedure SetSSLKeyFile(const AValue: String);

    function  GetRBHost: String;
    procedure SetRBHost(const AValue: String);
    function  GetPort: String;
    procedure SetPort(const APort: String);

    procedure ExceptionToLog(const AMethodName: String; const AException: Exception);
  public
    destructor Destroy; override;
  end;

function GetXMLRPCServer: IevXmlRPCServer;
begin
  Result := TevXmlRPCServer.Create;
end;

{ TevXmlRPCServer }

procedure TevXmlRPCServer.SB_Connect(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sServer, sUserName, sPassword, sDomain, sGUID: String;
  Context: IevContext;
  Res: IRpcStruct;
  ContextObjFromPool : IContextObjFromPool;
  APIKey : IevAPIKey;
  sTermsOfUse: String;
begin
//  evo.sb.connect(string LicenseKey, string evoServer, string user, string password [, string evoDomain])
//  Return: struct -{SESSION=Session ID(string),
//                   VER=version(string),
//                   USERID=UserID(int),
//                   FNAME=FirstName(string),
//                   LNAME=LastName(string)
//                   TERMSOFUSE=TermsOfUse (BLOB) - returned if user hasn't accepted Terms Of Use yet
  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  sServer := IRpcParameter(AParams[1]).AsString;
  sUserName := IRpcParameter(AParams[2]).AsString;
  sPassword := IRpcParameter(AParams[3]).AsString;

  if AParams.Count > 4 then
    sDomain := IRpcParameter(AParams[4]).AsString
  else
    sDomain := '';
  sUserName := EncodeUserAtDomain(sUserName, sDomain);

  Context := Mainboard.ContextManager.CreateThreadContext(sUserName, HashPassword(sPassword));

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);

  if APIKey.APIKeyType = apiWebClient then
  begin
    CheckCondition(Context.License.WebPayroll, 'There is no license for WebClient');

    if not (Context.UserAccount.AccountType = uatWeb) then
    begin
      AReturn.SetError(700, 'Error: This user is not registered as a web user');
      exit;
    end;
  end;

  if APIKey.APIKeyType = apiSelfServe then
  begin
    AReturn.SetError(700, 'Error: This API key is allowed for self-serve connect only');
    exit;
  end;

  // uncomment next code if you need to collect statistics
{  Context.Statistics.FileName := NormalizePath(ExtractFilePath(Context.Statistics.FileName)) +
    Context.GetID + '_' + FormatDateTime('mmddyy', Date) + ExtractFileExt(Context.Statistics.FileName);
  Context.Statistics.Enabled := True;
  Context.Statistics.DumpIntoFile := True; }

  Res := TRpcStruct.Create;
  Res.AddItem('SESSION', Context.GetID);
  Res.AddItem('VER', AppVersion);
  Res.AddItem('USERID', Context.UserAccount.InternalNbr);
  Res.AddItem('FNAME', Context.UserAccount.FirstName);
  Res.AddItem('LNAME', Context.UserAccount.LastName);
  AReturn.AddItem(Res);

  ContextObjFromPool := FContextPool.AcquireObject(Context.GetID) as IContextObjFromPool;
  ContextObjFromPool.SetContext(Context);
  ContextObjFromPool.SetLicenseKey(APIKey);
  sTermsOfUse := CheckIfTermsOfUseAccepted(IntToStr(Context.UserAccount.InternalNbr), APIKey);
  FContextPool.ReturnObject(ContextObjFromPool as IisObjectFromPool);
  CollectStatistics(AMethodName);

  if sTermsOfUse = '' then
    ContextObjFromPool.SetTermsOfUseAccepted(True)
  else
    Res.AddItemBase64Str('TERMSOFUSE', sTermsOfUse);
end;

procedure TevXmlRPCServer.TaskQueue_DeleteTask(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID, sTaskNbr: String;
  TaskInfo : IevTaskInfo;
  tmpId : TisGUID;
begin
//  evo.taskQueue.deleteTask(string sessionId, string taskNbr)
//  Return: boolean

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTaskNbr := IRpcParameter(AParams[1]).AsString;
  SetCurrentContext(sContextID);

  tmpId := TaskNbrToId(sTaskNbr);
  TaskInfo := mb_TaskQueue.GetTaskInfo(tmpId);
  APICheckCondition(Assigned(TaskInfo), QueueTaskNotFoundException);

  mb_TaskQueue.RemoveTask(tmpId);
  AReturn.AddItem(True);
end;

destructor TevXmlRPCServer.Destroy;
begin
  FreeAndNil(FRpcServer);
  FreeAndNil(FMethods);
  inherited;
end;

procedure TevXmlRPCServer.Disconnect(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  Res: Boolean;
  ContextObjFromPool : IContextObjFromPool;
begin
//  evo.disconnect(string sessionId)
//  Return: boolean - true if disconnected
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);
  sContextID := IRpcParameter(AParams[0]).AsString;

  Res := false;
  try
    SetCurrentContext(sContextID, touDoNotCheck);

    ContextObjFromPool := FContextPool.AcquireObject(Context.GetID) as IContextObjFromPool;
    if  Assigned(ContextObjFromPool.GetContext) then
    begin
      Res := true;
      ContextObjFromPool := nil;
      ClearCurrentContext;
    end;
  finally
    AReturn.AddItem(Res);
  end;
end;

procedure TevXmlRPCServer.DoOnConstruction;
begin
  inherited;                    

  if not IsStandalone then
  begin
    FController := TevAAController.Create;
    FController.Active := True;
  end;

  FMethods := TStringList.Create;
  FMethods.Duplicates := dupError;
  FMethods.CaseSensitive := False;
  FMethods.Sorted := True;

  FBillingLog := TisListOfValues.Create(nil, True);

  FRpcServer := TRpcServer.Create;
  FLogAPIExceptions := mb_AppConfiguration.GetValue('APIAdapter\LogAPIExceptions', False);

  RegisterMethods;
  FRpcServer.EnableIntrospect := True;

  FContextPool := TisObjectPool.Create('API Sessions', TContextObjFromPool);
  FContextPool.ObjectExpirationTime := 35; // mins
  FContextPool.MaxPoolSize := 10000;

  SetSSLRootCertFile(AppDir + 'root.pem');
  SetSSLCertFile(AppDir + 'cert.pem');
  SetSSLKeyFile(AppDir + 'key.pem');

  FTermsOfUse := TisListOfValues.Create(nil, True);
  FUsersCookies := TisListOfValues.Create(nil, True);
  FTermOfseFilename := NormalizePath(AppDir) + TermsOfUseApprovalsFilename;
  FUsersCookiesFilename := NormalizePath(AppDir) + UsersCookiesFilename;
end;

function TevXmlRPCServer.GetActive: Boolean;
begin
  Result := FRpcServer.Active;
end;

procedure TevXmlRPCServer.Legacy_OpenSQL(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  sSQL: String;
  iClientNbr: Integer;
  rpcStruct: IRpcStruct;
  sColumnMap: String;
  Q: IevQuery;
begin
//  evo.legacy.openSQL(string sessionId, string SQL, string columnMap (TableField=TableName[,TableField=TableName,...]), [integer clientId])
//  Deprecated! Return: struct - DataSet from the Evolution database'

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sSQL := IRpcParameter(AParams[1]).AsString;
  sColumnMap := IRpcParameter(AParams[2]).AsString;

  SetCurrentContext(sContextID);

  if AParams.Count > 3 then
  begin
    iClientNbr :=  IRpcParameter(AParams[3]).AsInteger;
    APICheckCondition((iClientNbr = ctx_DataAccess.ClientId) or (Context.UserAccount.AccountType <> uatSelfServe), NotAllowedEEException);
  end
  else
    iClientNbr := ctx_DataAccess.ClientID;

  ctx_DataAccess.OpenClient(iClientNbr);
  Q := TevQuery.Create(sSQL);
  Q.Execute;

  rpcStruct := TRpcStruct.Create;
  rpcStruct.AddItem('DATASET', DataSetToStruct(Q.Result, sColumnMap));

  AReturn.AddItem(rpcStruct);
end;

procedure TevXmlRPCServer.TaskQueue_GetPDFReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID, sTaskNbr: String;
  TskInf: IevTaskInfo;
  Res, Params: IevDualStream;
  tmpId : TisGUID;
begin
//  evo.taskQueue.getPDFReport(string sessionId, string taskNbr)
//  Return: base64 - PDF report from the Evolution task queue

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTaskNbr := IRpcParameter(AParams[1]).AsString;

  SetCurrentContext(sContextID);

  Res := nil;
  tmpId := TaskNbrToId(sTaskNbr);
  TskInf := mb_TaskQueue.GetTaskInfo(tmpId);

  APICheckCondition(Assigned(TskInf), QueueTaskNotFoundException);
  APICheckCondition(TskInf.State = tsFinished, QueueTaskNotFinishedException);

  if Assigned(TskInf) and (TskInf.State = tsFinished) then
  begin
    Params := TisStream.Create;
    Params.WriteString(tmpId);
    Res := Context.RemoteMiscRoutines.ReportToPDF(Params);
  end;

  if not Assigned(Res) then
    Res := TEvDualStreamHolder.Create;
  AReturn.AddItemBase64StrFromStream(Res.RealStream);
end;

procedure TevXmlRPCServer.GetSystemTime(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
begin
//  evo.getSystemTime(string sessionId)
//  Return: dateTime.iso8601 - time from the Evolution server

  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;

  SetCurrentContext(sContextID);

  AReturn.AddItemDateTime(SysTime);
end;

procedure TevXmlRPCServer.TaskQueue_GetTaskDetails(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID, sTaskNbr: String;
  Task: IevTask;
  Struct: IRpcStruct;
  TaskInfo: IevTaskInfo;
  tmpId : TisGUID;

  function GetTaskState(const ATaskState: TevTaskState): String;
  begin
    case ATaskState of
      tsInactive:  Result := 'Queued';
      tsWaiting:   Result := 'Waiting for resources';
      tsExecuting: Result := 'Executing';
      tsFinished:  Result := 'Finished';
    end;
  end;

begin
//  evo.taskQueue.getTaskDetails(string sessionId, string taskNbr)
//  Return: struct - <propertyName, propertyValue>

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTaskNbr := IRpcParameter(AParams[1]).AsString;
  SetCurrentContext(sContextID);

  tmpId := TaskNbrToId(sTaskNbr);
  TaskInfo := mb_TaskQueue.GetTaskInfo(tmpId);
  APICheckCondition(Assigned(TaskInfo), QueueTaskNotFoundException);
  APICheckCondition(TaskInfo.State = tsFinished, QueueTaskNotFinishedException);

  Task := mb_TaskQueue.GetTask(tmpId);
  APICheckCondition(Assigned(Task), QueueTaskNotFoundException);

  Struct := TRpcStruct.Create;
  Struct.AddItem('ID', Task.Nbr);
  Struct.AddItem('Nbr', Task.Nbr);
  Struct.AddItem('Type', Task.TaskType);
  Struct.AddItem('Caption', Task.Caption);
  Struct.AddItem('UserName', Task.User);
  Struct.AddItem('EvoDomain', Task.Domain);
  Struct.AddItem('Priority', Task.Priority);
  Struct.AddItem('SendEmail', Task.SendEmailNotification);
  Struct.AddItem('Email', Task.NotificationEmail);
  Struct.AddItem('Status', GetTaskState(Task.State));
  Struct.AddItem('ProgressText', Task.ProgressText);
  Struct.AddItem('LastUpdate', Task.LastUpdate);
  Struct.AddItem('Seen', Task.SeenByUser);
  Struct.AddItem('StartedAt', Task.FirstRunAt);
  Struct.AddItem('Log', Task.Log);
  Struct.AddItem('Exceptions', Task.Exceptions);
  Struct.AddItem('Warnings', Task.Warnings);
  Struct.AddItem('Notes', Task.Notes);

  if Task.TaskType = QUEUE_TASK_PROCESS_PAYROLL then
  begin
    Struct.AddItem('CoList', (Task as IevProcessPayrollTask).CoList);
    Struct.AddItem('PrList', (Task as IevProcessPayrollTask).PrList);
    Struct.AddItem('PrMessage', (Task as IevProcessPayrollTask).PRMessage);
  end

  else  if Task.TaskType = QUEUE_TASK_CREATE_PAYROLL then
  begin
    Struct.AddItem('ClNbr', (Task as IevCreatePayrollTask).ClNbr);
    Struct.AddItem('CoNbr', (Task as IevCreatePayrollTask).CONbr);
    Struct.AddItem('PrList', (Task as IevCreatePayrollTask).PrList);
  end

  else  if Task.TaskType = QUEUE_TASK_PREPROCESS_PAYROLL then
  begin
    Struct.AddItem('PrList', (Task as IevPreprocessPayrollTask).PrList);
    Struct.AddItem('PrMessage', (Task as IevPreprocessPayrollTask).PRMessage);
  end

  else  if Task.TaskType = QUEUE_TASK_REPRINT_PAYROLL then
  begin
    Struct.AddItem('ClientID', (Task as IevReprintPayrollTask).ClientID);
    Struct.AddItem('PrNbr', (Task as IevReprintPayrollTask).PrNbr);
  end;

  AReturn.AddItem(Struct);
end;

procedure TevXmlRPCServer.RegisterMethods;

   procedure RegisterMethod(const ASignature, AHelp: String; const AHandler: TevRPCMethod);
   var
     RpcMethodHandler: TRpcMethodHandler;
     s: String;
   begin
     s := ASignature;
     RpcMethodHandler := TRpcMethodHandler.Create;
     RpcMethodHandler.Name := GetNextStrValue(s, '(');
     RpcMethodHandler.Method := RPCHandler;
     RpcMethodHandler.Signature := ASignature;
     RpcMethodHandler.Help := AHelp;
     FRpcServer.RegisterMethodHandler(RpcMethodHandler);

     FMethods.AddObject(RpcMethodHandler.Name, @AHandler);
   end;

begin
  RegisterMethod('evo.sb.connect(string LicenseKey, string evoServer, string user, string password [, string evoDomain])',
                 'Return: struct -{SESSION=Session ID(string), VER=version(string) ' +
                 'USERID=UserID(int) FNAME=FirstName(string), LNAME=LastName(string)}',
                 SB_Connect);

  RegisterMethod('evo.ee.connect(string LicenseKey, string evoServer, string user, string password [, string evoDomain])',
                 'Return: struct -{SESSION=Session ID(string), VER=version(string) , CLNBR=ClientNBR(int), '+
                 'CONBR=CompanyNBR(int), CLPNBR=cl_person_nbr(int), EENBR=ee_nbr(int), ' +
                 'FNAME=FirstName(string), LNAME=LastName(string), '+
                 'COEMAIL=EmailAddress(string), CONAME=CompanyName(string)}',
                 EE_Connect);

  RegisterMethod('evo.ee.regNewAccount(string LicenseKey, string evoServer, string user, string password, string evoDomain, ' +
                 'string companyId, string SSN, int checkNumber, float checkTotalEarnings, [boolean EnableExtSecurity])',
                 'Return: struct - see "ee.connect" for more information',
                 EE_RegNewAccount);

  RegisterMethod('evo.disconnect(string sessionId)',
                 'Return: boolean - true if disconnected',
                 Disconnect);

  RegisterMethod('evo.sb.openClient(string sessionId, integer clientId, [integer companyId - deprecated parameter])',
                 'Return: boolean - true if successful',
                 SB_OpenClient);

  RegisterMethod('evo.changePassword(string sessionId, string oldPassword, string newPassword)',
                 'Return: boolean - true if successful',
                 ChangePassword);

  RegisterMethod('evo.validatePassword(string sessionId, string Password)',
                 'Return: boolean - true if successful',
                 ValidatePassword);

  RegisterMethod('evo.getSystemTime(string sessionId)',
                 'Return: dateTime.iso8601 - time from the Evolution server',
                 GetSystemTime);

  RegisterMethod('evo.legacy.openSQL(string sessionId, string SQL, string columnMap (TableField=TableName[,TableField=TableName,...]), [integer clientId])',
                 'Deprecated! Return: struct - DataSet from the Evolution database',
                 Legacy_OpenSQL);

  RegisterMethod('evo.legacy.openTable(string sessionId, string tableName, string condition, [integer clientId])',
                 'Deprecated! Return: struct - DataSet from the Evolution database',
                 Legacy_OpenTable);

  RegisterMethod('evo.legacy.postTableChanges(string sessionId, map<table, condition>)',
                 'Deprecated! Return: struct - key map',
                 Legacy_PostTableChanges);

  RegisterMethod('evo.taskQueue.runReport(string sessionId, string reportNumber, struct reportParams, [string reportlevel (S - system (default), B - bureau, C - client)])',
                 'Return: string Task ID',
                 TaskQueue_RunReport);

  RegisterMethod('evo.sb.GetReportParameters(string sessionId, integer  CoNbr,iReportNbr)',
                  'Return: struct - <parameterName, parameterValue>',
                 SB_GetReportParameters);


  RegisterMethod('evo.taskQueue.getPDFReport(string sessionId, string taskNbr)',
                 'Return: base64 - PDF report from the Evolution task queue',
                 TaskQueue_GetPDFReport);

  RegisterMethod('evo.sb.getSysReportByNbr(string sessionId, integer ReportNbr)',
                 'Return: base64 - RWR report from Evolution system level reports',
                 SB_GetSysReportByNbr);

  RegisterMethod('evo.sb.getReportFileByNbrAndType(string sessionId, integer ReportNbr, string ReportType)',
                 'Return: base64 - RWR report from Evolution system level reports',
                 SB_GetReportFileByNbrAndType);

  RegisterMethod('evo.ee.getW2(string sessionId, int W2NBR)',
                 'Return: base64 - PDF report from the Evolution task queue',
                 EE_GetW2);

  RegisterMethod('evo.ee.getW2List(string sessionId, int EE_NBR)',
                 'Return: struct - List of Employee W2 Reports',
                 EE_GetW2List);

  RegisterMethod('evo.sb.getEmployee_E_DS(string sessionId, int ee_nbr, string query)',
                 'Return: struct - List of Employee Scheduled_E_DS',
                 SB_GetEmployee_E_DS);

  RegisterMethod('evo.sb.getEeScheduledEdAmount(string sessionId, int cl_e_ds_nbr, ' +
                                       'int ee_benefits_nbr, int co_benefit_subtype_nbr, datetime  effective_start_date, ' +
                                       'datetime effective_end_date, float target_amount, ' +
                                       'float annual_maximum_amount, string frequency)',
                 'Return: struct - return scheduled E_DS Amount',
                  SB_getEeScheduledEdAmount);

  RegisterMethod('evo.taskQueue.getTaskList(string sessionId)',
                 'Return: struct - Evolution task queue as DataSet',
                 TaskQueue_GetTaskList);

  RegisterMethod('evo.taskQueue.deleteTask(string sessionId, string taskNbr)',
                 'Return: boolean - True',
                 TaskQueue_DeleteTask);

  RegisterMethod('evo.taskQueue.getTaskSummary(string sessionId, string taskNbr)',
                 'Return: struct - <propertyName, propertyValue>',
                 TaskQueue_GetTaskSummary);

  RegisterMethod('evo.taskQueue.getTaskDetails(string sessionId, string taskNbr)',
                 'Return: struct - <propertyName, propertyValue>',
                 TaskQueue_GetTaskDetails);

  RegisterMethod('evo.taskQueue.isTaskFinished(string sessionId, string taskNbr)',
                 'Return: Boolean',
                 TaskQueue_IsTaskFinished);

  RegisterMethod('evo.taskQueue.runPayrollReport(string sessionId, integer prNbr, datetime checkDate, string payrollChecks[, string taskCaption]))',
                 'Return: string - Task ID',
                 TaskQueue_RunPayrollReport);

  RegisterMethod('evo.taskQueue.runPreprocessPayroll(string sessionId, string prNbrList,[ string taskCaption])',
                 'Return: string - Task ID',
                 TaskQueue_RunPreprocessPayroll);

  RegisterMethod('evo.taskQueue.runCreatePayroll(string sessionId, struct taskParam)',
                 'Return: string - Task ID',
                 TaskQueue_RunCreatePayroll);

  RegisterMethod('evo.taskQueue.evoXImport(string sessionId, array of struct with two fields: string fileName and base64 fileData, base64str XMLMap)',
                 'Return: struct with two fields: string taskNbr, base64str ErrorLog',
                 TaskQueue_EvoXImport);

  RegisterMethod('evo.taskQueue.getEvoXImportResults(string sessionId, string taskNbr)',
                 'Return: array of structs',
                 TaskQueue_GetEvoXImportResults);

  RegisterMethod('evo.payroll.GetHoliday(string sessionId, date Date)',
                 'Return: string HolidayName',
                 Payroll_GetHoliday);

  RegisterMethod('evo.sb.TCImport(string sessionId, integer CompanyId, integer payrollId, Base64str importData, struct importOptions)',
                 'Return: struct',
                 SB_TCImport);

  RegisterMethod('evo.runQBQuery(string sessionId, Base64str QBQuery, string columnMap, [struct queryParams])',
                 'Return: struct',
                 SB_RunQBQuery);

  RegisterMethod('evo.taskQueue.getReport(string sessionId, string taskNbr)',
                 'Return: struct with two fields: ReportType (values: UNKNOWN, RWA, ASCII, ERROR) and Report (base64 encoded byte array)',
                 TaskQueue_GetReport);

  RegisterMethod('evo.legacy.deleteTableValues(string sessionId, struct Params)',
                 'Return: boolean',
                 Legacy_DeleteTableValues);

  RegisterMethod('evo.sb.deleteRecord(String sessionID, String tableName, int recordNbr)',
                 'Return: nothing',
                 SB_DeleteRecord);

  RegisterMethod('evo.legacy.executeMethod(string sessionId, integer packageNumber, string  functionName, array params)',
                 'Return: array',
                 Legacy_ExecuteMethod);

  RegisterMethod('evo.legacy.executeMethodCalc(string sessionId, integer packageNumber, string  functionName, array params)',
                 'Return: array',
                 Legacy_ExecuteMethodCalc);

  RegisterMethod('evo.getAPIVersion',
                 'Return: string',
                 GetAPIVersion);

  RegisterMethod('test.Hello(string AnyString)',
                 'Return: string - input string as result',
                 testHelloMethod);

  RegisterMethod('evo.sb.readCompanyStorage(string sessionId, integer CoNbr, string Tag)',
                 'Return: array of structures',
                 SB_ReadCompanyStorage);

  RegisterMethod('evo.sb.writeCompanyStorage(string sessionId, integer CoNbr, string Tag, array of struct Data)',
                 'Return: nothing',
                 SB_WriteCompanyStorage);

  RegisterMethod('evo.ee.createEEChangeRequest(string sessionId, integer EeNbr, string Type, map RequestData)',
                 'Return: nothing',
                 EE_CreateEEChangeRequest);

  RegisterMethod('evo.ee.processChangeRequests(string sessionId, array of EEChangeRequestNbr, boolean Operation)',
                 'Return: array of EEChangeRequestNbr which was processed with errors',
                 EE_ProcessChangeRequests);

  RegisterMethod('evo.ee.getEEChangeRequestList(string sessionId, integer Eenbr, String Type)',
                 'Return: array of data',
                 EE_GetEEChangeRequestList);

  RegisterMethod('evo.ee.getMgrChangeRequestList(string sessionId, integer MgrEenbr, String Type)',
                 'Return: array of data',
                 EE_GetMgrChangeRequestList);

  RegisterMethod('evo.ee.saveTORequest(string sessionId, string origin, integer EeNbr, integer TymeOffTypeNbr, string Notes, Array requestArray)',
                 'Return: True if successfully saved',
                 EE_SaveTORequest);

  RegisterMethod('evo.ee.getEETORequestList(string sessionId, integer Eenbr)',
                 'Return: array of structures',
                 EE_GetEETORequestList);

  RegisterMethod('evo.ee.getMgrTORequestList(string sessionId, integer MgrEenbr)',
                 'Return: array of data',
                 EE_GetMgrTORequestList);

  RegisterMethod('evo.ee.processTORequests(string sessionId, boolean Operation, array RequestArray)',
                 'Return: True if successfully processed',
                 EE_ProcessTORequests);

  RegisterMethod('evo.sb.getEMail(string sessionId, integer CoNbr)',
                 'Return: string',
                 SB_GetEmail);

  RegisterMethod('evo.security.getAccountRights(string sessionId)',
                 'Return: array of struct',
                 Security_GetAccountRights);

  RegisterMethod('evo.sb.getWCReportList(string sessionId, integer CoNbr, array ReportNbrs)',
                 'Return: struct',
                 SB_GetWCReportList);

  RegisterMethod('evo.sb.getVMRReport(string sessionId, array SelectedReportNbrs, boolean IsPrint, string aComment)',
                 'Return: base64',
                 SB_GetVMRReport);

  RegisterMethod('evo.sb.getVMRReportlist(string sessionId, integer pCONbr, datetime pCheckFromDate, datetime pCheckToDate, integer pVMRReportType)',
                 'Return: struct',
                 SB_GetVMRReportlist);

  RegisterMethod('evo.sb.getRemoteVMRReportList(string sessionId, string aLocation)',
                 'Return: struct',
                 SB_GetRemoteVMRReportList);

  RegisterMethod('evo.sb.getRemoteVMRReport(string sessionId, integer SBMailBoxContentNbr)',
                 'Return: struct',
                 SB_GetRemoteVMRReport);

  RegisterMethod('evo.sb.getPayrollTotals(string sessionId, integer PrNbr)',
                 'Return: structure',
                 SB_GetPayrollTotals);

  RegisterMethod('evo.sb.autoEnlistTOAForNewEE(string sessionID, integer CONbr, integer EENbr)',
                 'Return: nothing',
                 SB_autoEnlistTOAForNewEE);

  RegisterMethod('evo.sb.autoEnlistEDsForNewEE(string sessionID, integer CONbr, integer EENbr)',
                 'Return: structure',
                 SB_autoEnlistEDsForNewEE);

  RegisterMethod('evo.createNewClientDB(string sessionId)',
                 'Return: integer',
                 CreateNewClientDB);

  RegisterMethod('evo.applyDataChangePacket(string session Id, array of struct dataChangePacket, [DateTime AsOfDate])',
                 'Return: structure',
                 ApplyDataChangePacket);

  RegisterMethod('evo.applyManyDataChangePackets(string session Id, array of struct of struct dataChangePacket)',
                 'Return: structure',
                 ApplyManyDataChangePackets);

  RegisterMethod('evo.getSecurityQuestions(string LicenseKey [, string evoDomain])',
                 'Return: array of string',
                 GetSequrityQuestions);

  RegisterMethod('evo.getUserQuestions(string LicenseKey, string AUserName [, string evoDomain])',
                 'Return: array of string',
                 GetUserQuestions);

  RegisterMethod('evo.resetPassword(string LicenseKey, string UserName, array of struct QestionsAndAnswers, string NewPassowrd [, string domain])',
                 'Return: nothing',
                 ResetPassword);

  RegisterMethod('evo.setUserQuestions(string sessionid, array of struct)',
                 'Return: nothing',
                 SetUserQuestions);

  RegisterMethod('evo.ee.setUserSecData(string sessionId, struct)',
                 'Return: struct',
                 EE_SetUserSecData);

  RegisterMethod('evo.sb.setUserSecData(string sessionId, struct)',
                 'Return: struct',
                 SB_SetUserSecData);

  RegisterMethod('evo.getPasswordRules(string LicenseKey [, string evoDomain])',
                 'Return: string',
                 GetPasswordRules);
 RegisterMethod('evo.sb.getCoCalendarDefaults(string session Id, integer CoNbr)',
                 'Return: struct of struct',
                 SB_GetCoCalendarDefaults);

  RegisterMethod('evo.sb.createCoCalendar(string session Id, integer CoNbr, struct CoSettings, string BasedOn, string MoveCheckDate, string MoveCallInDate, string MoveDeliveryDate)',
                 'Return: array of string with warnings, if any',
                 SB_CreateCoCalendar);

  RegisterMethod('evo.getSBInfo(string domain)',
                 'Return: struct',
                 getSBInfo);

  RegisterMethod('evo.getBenefitRequest(string sessionId, integer requestNbr)',
                 'Return: struct',
                 GetBenefitRequest);

  RegisterMethod('evo.ee.storeBenefitRequest(string sessionId, struct requestData, integer requestNbr)',
                 'Return: integer RequestNbr',
                 EE_StoreBenefitRequest);

  RegisterMethod('evo.getBenefitRequestList(string sessionId, integer eeNbr)',
                 'Return: array of struct',
                 GetBenefitRequestList);

  RegisterMethod('evo.getDDTableInfo(string sessionId, string TableName [, string FieldName])',
                 'Return: struct of struct',
                 GetDDTableInfo);

  RegisterMethod('evo.ee.sendEMailToHR(string sessionId, BLOB Message, string Subject, [struct Attachements])',
                 'Return: array of strings',
                 EE_SendEMailToHR);

  RegisterMethod('evo.getEeExistingBenefits(string sessionId, integer EENbr, [boolean CurrentOnly = True])',
                 'Return: dataset',
                 GetEeExistingBenefits);

  RegisterMethod('evo.deleteBenefitRequest(string sessionId, integer requestNbr)',
                 'Return: nothing',
                 DeleteBenefitRequest);

  RegisterMethod('evo.getSummaryBenefitEnrollmentStatus(string sessionId, array EENbrs, string Type)',
                 'Return: dataset',
                 GetSummaryBenefitEnrollmentStatus);

  RegisterMethod('evo.ee.getEeBenefitPackageAvaliableForEnrollment(string sessionId, integer EENbr)',
                 'Return: struct with two datasets',
                 EE_GetEeBenefitPackageAvaliableForEnrollment);

  RegisterMethod('evo.sb.revertBenefitRequest(string sessionId, integer requestNbr)',
                 'Return: nothing',
                 SB_RevertBenefitRequest);

  RegisterMethod('evo.ee.submitBenefitRequest(string sessionId, integer requestNbr, struct signatureData)',
                 'Return: nothing',
                 EE_SubmitBenefitRequest);

  RegisterMethod('evo.getBenefitRequestConfirmationReport(string sessionId, integer requestNbr)',
                 'Return: blob',
                 GetBenefitRequestConfirmationReport);

  RegisterMethod('evo.sb.sendBenefitEMailToEE(string sessionId, integer EENbr, string Subject, BLOB Message[, int Requestnbr])',
                 'Return: nothing',
                 SB_SendBenefitEMailToEE);

  RegisterMethod('evo.getEmptyBenefitRequest(string sessionId)',
                 'Return: struct',
                 GetEmptyBenefitRequest);

  RegisterMethod('evo.sb.closeBenefitEnrollment(string sessionId, integer EENbr, string CathegoryType[, int Requestnbr])',
                 'Return: nothing',
                 SB_CloseBenefitEnrollment);

  RegisterMethod('evo.sb.approveBenefitRequest(string sessionId, int Requestnbr)',
                 'Return: nothing',
                 SB_ApproveBenefitRequest);

  RegisterMethod('evo.acceptTermsofUse(string sessionId, boolean Accepted)',
                 'Return: nothing',
                 AcceptTermsOfUse);

  RegisterMethod('evo.getSecurityExtQuestions(string LicenseKey [, string evoDomain])',
                 'Return: array of string',
                 GetSequrityExtQuestions);

  RegisterMethod('evo.setUserExtQuestions(string sessionid, array of struct)',
                 'Return: nothing',
                 SetUserExtQuestions);

  RegisterMethod('evo.rememberMe(string sessionId[, string usersComputerName]',
                 'Return: struct',
                 RememberMe);

  RegisterMethod('evo.forgetMe(string sessionId, string UserComputerName)]',
                 'Return: nothing',
                 ForgetMe);

  RegisterMethod('evo.getListOfRegisteredComputers(string sessionId)',
                 'Return: array of struct',
                 GetListOfRegisteredComputers);

  RegisterMethod('evo.sb.getSwipeclockOneTimeURL(string sessionId, integer CoNbr)',
                 'Return: string',
                 SB_GetSwipeclockOneTimeURL);

  RegisterMethod('evo.sb.getManualTaxesListForCheck(string sessionId, integer PrCheckNbr)',
                 'Return: struct with dataset',
                 SB_GetManualTaxesListForCheck);


  RegisterMethod('evo.sb.getMRCOneTimeURL(string sessionId, integer CONBR)',
                 'Return: string',
                 SB_GetMRCOneTimeURL);

  RegisterMethod('evo.getEvBlobData(string sessionId, string TableName, integer TableNbr, string BlobFieldName)',
                 'Return: base64',
                 GetEvBlobData);

end;

procedure TevXmlRPCServer.RPCHandler(const AMethodName: string; AList: IInterfaceList; AReturn: IRpcReturn);
var
  TaskID: TTaskID;
begin
  TaskID := GlobalThreadManager.RunTask(RPCHandlerThread, Self, MakeTaskParams([AMethodName, AList, AReturn]), 'API Call');
  GlobalThreadManager.WaitForTaskEnd(TaskID);
end;


procedure TevXmlRPCServer.RPCHandlerThread(const AParams: TTaskParamList);
var
  AMethodName: string;
  AList: IInterfaceList;
  AReturn: IRpcReturn;
  i: Integer;
  Proc: TMethod;
begin
  try
    AMethodName := AParams[0];
    AList := IInterface(AParams[1]) as IInterfaceList;
    AReturn := IInterface(AParams[2]) as IRpcReturn;

    try
      i := FMethods.IndexOf(AMethodName);
      CheckCondition(i <> -1, 'Unregistered method "' + AMethodName + '"');

      Proc.Data := Self;
      Proc.Code := FMethods.Objects[i];
      TevRPCMethod(Proc)(AList, AReturn, LowerCase(AMethodName));
    finally
      ClearCurrentContext;
    end;
  except
    on E: WrongSignatureException do
    begin
      AReturn.SetError(800, 'Wrong method signature');
    end;

    on E: WrongSessionException do
    begin
      AReturn.SetError(810, 'Wrong session');
    end;

    on E: WrongLicenseException do
    begin
      AReturn.SetError(800, 'Wrong license key');
    end;

    on E: WrongAPIKeyException do
    begin
      AReturn.SetError(800, 'Wrong API Key');
    end;

    on E: NotAllowedEEException do
    begin
      AReturn.SetError(810, 'Operation is not allowed in EE mode');
    end;

    on E: TableNameEmptyException do
    begin
      AReturn.SetError(800, 'No table name');
    end;

    on E: QueueTaskNotFoundException do
    begin
      AReturn.SetError(800, 'Task not found in queue');
    end;

    on E: QueueTaskNotFinishedException do
    begin
      AReturn.SetError(800, 'Task not finished yet');
    end;

    on E: NotSSUserException do
    begin
      AReturn.SetError(800, 'This user is not registered as a SelfServe user');
    end;

    on E: WrongReportLevelException do
    begin
      AReturn.SetError(810, 'Wrong report level');
    end;

    on E: UnsupportedParamTypeException do
    begin
      AReturn.SetError(811, 'dtArray and dtNone are unsupported parameter types');
    end;

    on E: UnknownParamTypeException do
    begin
      AReturn.SetError(811, 'Unsupported parameter type');
    end;

    on E: ResultWrongStructureException do
    begin
      AReturn.SetError(810, 'Result has wrong structure');
    end;

    on E: WrongReturnTypeException do
    begin
      AReturn.SetError(510, 'Wrong return type');
    end;

    on E: WrongPackageNumberException do
    begin
      AReturn.SetError(810, 'Package number should be 4 (REMOTABLE_CALC_PACKAGE)');
    end;

    on E: UnsupportedFunctionException do
    begin
      AReturn.SetError(810, 'Unsupported function');
    end;

    on E: ResultIsEmptyException do
    begin
      AReturn.SetError(810, 'Result is empty');
    end;

    on E: UnsupportedOutputParamTypeException do
    begin
      AReturn.SetError(810, 'Unsupported output parameter type');
    end;

    on E: EmptyDatasetException do
    begin
      AReturn.SetError(810, 'Dataset is empty');
    end;

    on E: CannotConvertNbrToIdException do
    begin
      AReturn.SetError(810, 'Cannot find TaskId for given TaskNbr');
    end;

    on E: CannotConvertIdToNbrException do
    begin
      AReturn.SetError(810, 'Cannot find TaskNbr for given TaskId');
    end;

    on E: SecurityCallsNotAllowed do
    begin
      AReturn.SetError(810, 'You have no rights to do security calls');
    end;

    on E: UnknownCoStorageTag do
    begin
      AReturn.SetError(800, 'Unknown Tag');
    end;

    on E: CannotGetDataDueTimeout do
    begin
      AReturn.SetError(810, 'Program was not able to read data because table is locked');
    end;

    on E: UnknownEEChangeRequestType do
    begin
      AReturn.SetError(800, 'Unknown type of employee change request');
    end;

    on E: ESSNotEnabledException do
    begin
      AReturn.SetError(800, 'SelfServe is not enabled for this employee');
    end;

    on E: ELockedAccount do
    begin
      AReturn.SetError(501, E.Message);
    end;

    on E: EBlockedAccount do
    begin
      AReturn.SetError(502, E.Message);
    end;

    on E: ExpiredOrStolenCookie do
    begin
      AReturn.SetError(812, 'Expired registration or stolen cookie');
    end;

    on E: Exception do
    begin
      AReturn.SetError(500, E.Message);
      ExceptionToLog(AMethodName, E);
    end;
  end;
end;

procedure TevXmlRPCServer.TaskQueue_RunReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  iReportNbr: integer;
  Params: IRpcStruct;
  ReportParams: TrwReportParams;
  sParamName: string;
  i: Integer;
  sRepLevel: String;
  sTaskID: TisGUID;
  sReportName : String;

  function ParamToVar(const AParam: IRpcCustomItem): Variant;
  var
    ArrayItem: IRpcArray;
    i: Integer;
  begin
    case AParam.DataType of
      dtString:   Result := AParam.AsString;
      dtBoolean:  Result := AParam.AsBoolean;
      dtDateTime: Result := AParam.AsDateTime;
      dtFloat:    Result := AParam.AsFloat;
      dtInteger:  Result := AParam.AsInteger;

      dtArray:
      begin
        arrayItem := AParam.AsArray;
        Result := VarArrayCreate([0, arrayItem.Count - 1], varVariant);
        for i := 0 to arrayItem.Count-1 do
          Result[i] := ParamToVar(arrayItem.Items[i]);
      end;
    else
      raise EevException.Create('Type is not supported');
    end;
  end;

begin
//  evo.taskQueue.runReport(string sessionId, string reportNumber, struct reportParams, [string reportlevel (S - system (default), B - bureau, C - client)])
//  Return: string taskNbr

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iReportNbr :=  IRpcParameter(AParams[1]).AsInteger;
  Params := IRpcParameter(AParams[2]).AsStruct;

  SetCurrentContext(sContextID);

  ReportParams := TrwReportParams.Create;
  try
    for i := 0 to Params.Count-1 do
    begin
      sParamName := Params.KeyList.Strings[i];
      ReportParams.Add(sParamName, ParamToVar(Params.Items[i]));
      if UpperCase(sParamName) = 'REPORTNAME' then
        sReportName := ParamToVar(Params.Items[i]);
    end;

    if AParams.Count >= 4 then
    begin
      sRepLevel := Copy(IRpcParameter(AParams[3]).AsString, 1, 1);
      APICheckCondition(Length(trim(sRepLevel)) > 0, WrongReportLevelException);
      APICheckCondition(sRepLevel[1] in [REPORT_LEVEL_SYSTEM, REPORT_LEVEL_BUREAU, REPORT_LEVEL_CLIENT], WrongReportLevelException);
    end
    else
      sRepLevel := REPORT_LEVEL_SYSTEM;

    ctx_RWLocalEngine.StartGroup;
    ctx_RWLocalEngine.CalcPrintReport(iReportNbr, sRepLevel, ReportParams, sReportName);
    sTaskID := ctx_RWLocalEngine.EndGroupForTask(rdtNone);
  finally
    reportParams.Free;
  end;

  AReturn.AddItem(TaskIdToNbr(sTaskID));
  CollectStatistics(AMethodName);
end;

procedure TevXmlRPCServer.SB_GetReportParameters(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iReportNbr, coNbr: Integer;
  ResultList: TrwReportParams;
  Param: TrwReportParam;
  sParam:string;
  rpcStruct: IRPCStruct;

  function GetCOReportParameters(const coNbr, aReportNbr: integer): TrwReportParams;
  var
    Q: IevQuery;
  begin
      Q := TevQuery.Create('select CO_REPORTS_NBR, INPUT_PARAMS from CO_REPORTS where CO_NBR = :coNbr and REPORT_WRITER_REPORTS_NBR = :Nbr');
      Q.Params.AddValue('coNbr', coNbr);
      Q.Params.AddValue('Nbr', aReportNbr);
      Q.Execute;
      Result := TrwReportParams.Create;
      if (Q.Result.RecordCount >0)  and (Q.Result.Fields[0].Value <> null ) then
      begin
        try
           Result.ReadFromBlobField(TBlobField(Q.Result.FieldByName('INPUT_PARAMS')));
        except
           Result.Clear;
        end;
      end;
  end;

begin
  // evo.SB.GetReportParameters(string sessionId, CONbr integer ReportNbr)
  // Return: array of structure
  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  coNbr := IRpcParameter(AParams[1]).AsInteger;
  iReportNbr := IRpcParameter(AParams[2]).AsInteger;
  sParam := IRpcParameter(AParams[3]).AsString;

  ResultList := GetCOReportParameters(coNbr, iReportNbr);

  rpcStruct := TRpcStruct.Create;

  Param := ResultList.ParamByName(sParam);
  if Assigned(Param) then
     rpcStruct.AddItem(sParam,VarToStr(Param.Value));

  AReturn.AddItem(rpcStruct);

end;

procedure TevXmlRPCServer.SetActive(const AValue: Boolean);
var
  tmpStream: IisStream;
  i: integer;
  Cookie: IUserRegisteredComputerInfo;
  iDeleted: integer;
begin
  FTermOfseFilename := NormalizePath(AppDir) + TermsOfUseApprovalsFilename;

  // stopping API billing watch dog, trying to send, then saving unsent data if necessary
  if FRpcServer.Active <> AValue then
  begin
    // loading unsent data and starting API billing watch dog
    if AValue then
    begin
      try
        FTermsOfUse.Lock;
        if FileExists(FTermOfseFilename) then
        begin
          try
            tmpStream := TisStream.CreateFromFile(FTermOfseFilename);
            FTermsOfUse.ReadFromStream(tmpStream);
          except
            on E: Exception do
            begin
              GlobalLogFile.AddEvent(etError, 'API', 'Cannot read terms of use information from "' + FTermOfseFilename
                + '" file', E.Message + #13#10 + GetErrorCallStack(E));
              FTermsOfUse.Clear;
            end;
          end;
        end;
      finally
        FTermsOfUse.Unlock;
      end;

      try
        FUsersCookies.Lock;
        if FileExists(FUsersCookiesFilename) then
        begin
          try
            tmpStream := TisStream.CreateFromFile(FUsersCookiesFilename);
            FUsersCookies.ReadFromStream(tmpStream);

            // cleanup
            iDeleted := 0;
            for i := FUsersCookies.Count - 1 downto 0 do
            begin
              Cookie := IInterface(FUsersCookies.Values[i].Value) as IUserRegisteredComputerInfo;
              if IsUserCookieExpired(Cookie, Now) then
              begin
                FUsersCookies.Delete(i);
                Inc(iDeleted);
              end;
            end;  // for
            if iDeleted > 0 then
              StoreUsersCookies;
          except
            on E: Exception do
            begin
              GlobalLogFile.AddEvent(etError, 'API', 'Cannot read users cookies from "' + FUsersCookiesFilename
                + '" file', E.Message + #13#10 + GetErrorCallStack(E));
              FUsersCookies.Clear;
            end;
          end;
        end;
      finally
        FUsersCookies.Unlock;
      end;

      if not isStandAlone then
      begin
        if Mainboard.TCPClient.Host = '' then
          Mainboard.TCPClient.Host := mb_AppConfiguration.AsString['General\RequestBroker'];
        if Mainboard.TCPClient.Port = '' then
          Mainboard.TCPClient.Port := IntToStr(TCPClient_Port);
      end;

      LoadUnsentBillingInfo;
      {todo : uncomment }
//      FBillingSaverWatchDogId := GlobalThreadManager.RunTask(BillingInfoSaverWatchDog, Self, MakeTaskParams([]), 'API billing info saver watchdog');

      if FRpcServer.ListenPort = 0 then
        FRpcServer.ListenPort := mb_AppConfiguration.GetValue('APIAdapter\XMLRPCPort', 9943);
      FRpcServer.SSLEnable := mb_AppConfiguration.GetValue('APIAdapter\SSL', True);
      FRpcServer.Active := True;
    end
    else
    begin
      FRpcServer.Active := False;

      StoreTermsOfUse;
      StoreUsersCookies;

      {todo : uncomment }
{      if FBillingSaverWatchDogId <> 0 then
        try
          GlobalThreadManager.TerminateTask(FBillingSaverWatchDogId);
          GlobalThreadManager.WaitForTaskEnd(FBillingSaverWatchDogId);
        except
        end; }
      FBillingSaverWatchDogId := 0;
//      SendBillingInfo;
      SaveUnsentBillingInfo;
    end;

  end;
end;

procedure TevXmlRPCServer.TaskQueue_GetTaskList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
const
  DSStruct: array [0..14] of TDSFieldDef = (
            (FieldName: 'Id';           DataType: ftInteger;  Size: 0;   Required: False),
            (FieldName: 'ClassName';    DataType: ftString;   Size: 255;    Required: False),
            (FieldName: 'Description';  DataType: ftString;   Size: 128;  Required: False),
            (FieldName: 'Caption';      DataType: ftString;   Size: 255;  Required: False),
            (FieldName: 'Status';       DataType: ftString;   Size: 255;  Required: False),
            (FieldName: 'Finished';     DataType: ftString;   Size: 1;    Required: False),
            (FieldName: 'Seen';         DataType: ftBoolean;  Size: 0;    Required: False),
            (FieldName: 'TimeStamp';    DataType: ftDateTime; Size: 0;    Required: False),
            (FieldName: 'Priority';     DataType: ftInteger;  Size: 0;    Required: False),
            (FieldName: 'Requests';     DataType: ftMemo;     Size: 0;    Required: False),
            (FieldName: 'UseEmail';     DataType: ftBoolean;  Size: 0;    Required: False),
            (FieldName: 'EMail';        DataType: ftString;   Size: 100;  Required: False),
            (FieldName: 'Message';      DataType: ftString;   Size: 255;  Required: False),
            (FieldName: 'UserName';     DataType: ftString;   Size: 128;  Required: False),
            (FieldName: 'SbId';         DataType: ftString;   Size: 128;  Required: False));

var
  sContextID: String;
  TaskInf: IevTaskInfo;
  TaskList: IisListOfValues;
  DS: IevDataSet;
  i: Integer;
  rpcStruct: IRpcStruct;
  sDomain, sUser : String;

  function GetTaskState: String;
  begin
    case TaskInf.State of
      tsInactive:  Result := 'Queued';
      tsWaiting:   Result := 'Waiting for resources';
      tsExecuting: Result := 'Executing';
      tsFinished:  Result := TaskInf.ProgressText;
    end;
  end;

  function GetTaskProgressText: String;
  begin
    if TaskInf.State = tsFinished then
      Result := ''
    else
      Result := TaskInf.ProgressText;
  end;

begin
//  evo.taskQueue.getTaskList(string sessionId)
//  Return: struct - Evolution task queue as DataSet
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  DS := TevDataSet.Create(DSStruct);
  TaskList := mb_TaskQueue.GetUserTaskInfoList;
  sDomain := Context.UserAccount.Domain;
  sUser := Context.UserAccount.User;

  for i := 0 to TaskList.Count - 1 do
  begin
    TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
    with TaskInf do
      if  AnsiSameText(sDomain, TaskInf.GetDomain) and AnsiSameText(sUser, TaskInf.GetUser) then
        DS.AppendRecord([Nbr, TaskTypeToMethodName(TaskType), TaskType, Caption, GetTaskState,
                         Iff(State = tsFinished, 'Y', 'N'),  SeenByUser,
                         LastUpdate, Priority, '', SendEmailNotification, NotificationEmail,
                         GetTaskProgressText, User, '']);
  end;

  rpcStruct := TRpcStruct.Create;
  rpcStruct.AddItem('DATASET', DataSetToStruct(DS, ''));

  AReturn.AddItem(rpcStruct);
end;

procedure TevXmlRPCServer.EE_Connect(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sServer, sUserName, sPassword, sDomain, sGUID: String;
  Context: IevContext;
  Cookie: IRpcStruct;
  Res: IRpcStruct;
  Q: IevQuery;
  ContextObjFromPool : IContextObjFromPool;
  APIKey : IevAPIKey;
  iEeNbr: integer;
  sGroups: String;
  iCoNbr: integer;
  // for ext security
  bExtSecurityOn: boolean;
  QAArray: IRpcArray;
  QAStruct: IRpcStruct;
  QAList: IisListOfValues;
  ExtQuestionsList: IisStringList;
  UserQuestions: IisStringList;
  iDaysToExpire: integer;
  PasswordChangeDate: TDateTime;
  ExtSecQuestions: IRpcArray;
  i: integer;
  sQuestion, sAnswer: String;
  sTermsOfUse: String;
  sCookieId, sComputerId, sToken, sNewToken: String;
begin
//  evo.ee.connect(string LicenseKey, string evoServer, string user, string password, string evoDomain, [array of struct QuestionsAndAnswers], [struct Cookies])
//  Return without ext security: struct -{SESSION=Session ID(string),
//                   VER=version(string) ,
//                   CLNBR=ClientNBR(int),
//                   CONBR=CompanyNBR(int),
//                   CLPNBR=cl_person_nbr(int),
//                   EENBR=ee_nbr(int),
//                   FNAME=FirstName(string),
//                   LNAME=LastName(string),
//                   COEMAIL=EmailAddress(string),
//                   CONAME=CompanyName(string)}
//                   TERMSOFUSE=TermsOfUse (BLOB) - returned if user hasn't accepted Terms Of Use yet
//                   COOKIEID (string)
//
//  Return with ext security: struct -{SESSION=Session ID(string) - could be 'No Session', if session is not created,
//                   VER=version(string) ,
//                   CLNBR=ClientNBR(int),
//                   CONBR=CompanyNBR(int),
//                   CLPNBR=cl_person_nbr(int),
//                   EENBR=ee_nbr(int),
//                   FNAME=FirstName(string),
//                   LNAME=LastName(string),
//                   COEMAIL=EmailAddress(string),
//                   CONAME=CompanyName(string)}
//                   boolean QUESTIONSSET
//                   string EEEMAIL from EE.E_MAIL_ADDRESS
//                   integer PWDEXPIRE - days to password's expiration
//                   array of string EXTQUESTIONS, if this field is missed in struct, there's no ext security; if array is empty, it means that user need to setup sequrity questions
//                   TERMSOFUSE=TermsOfUse (BLOB) - returned if user hasn't accepted Terms Of Use yet
//                   COOKIEID (string),
//                   NewToken(string) - may be returned if user registered computer

  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  sServer := IRpcParameter(AParams[1]).AsString;
  sUserName := IRpcParameter(AParams[2]).AsString;
  sPassword := IRpcParameter(AParams[3]).AsString;
  if AParams.Count > 4 then
  begin
    if IRpcParameter(AParams[4]).isString then
      sDomain := IRpcParameter(AParams[4]).AsString
    else
      raise Exception.Create('Error: the fifth parameter (domain) is not string type');
  end
  else
    sDomain := '';

  if AParams.Count > 5 then
  begin
    if AParams.Count > 6 then
    begin
      Cookie := IRpcParameter(AParams[6]).AsStruct;
      QAArray := nil;
      QAList := nil;
    end
    else
    begin
      Cookie := nil;
      QAArray := IRpcParameter(AParams[5]).AsArray;
      QAList := TisListOfValues.Create;
      for i := 0 to QAArray.Count - 1 do
      begin
        QAStruct := QAArray.Items[i].AsStruct;
        sQuestion := QAStruct.Keys['Q'].AsString;
        sAnswer := QAStruct.Keys['A'].AsString;
        QAList.AddValue(sQuestion, sAnswer);
      end;
    end;
    bExtSecurityOn := true;
  end
  else
  begin
    Cookie := nil;
    QAArray := nil;
    QAList := nil;
    bExtSecurityOn := false;
  end;

  Res := TRpcStruct.Create;

  if bExtSecurityOn then
  begin
    Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

    APIKey := FindAPIKey(sGUid);
    APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
    APICheckCondition((APIKey.APIKeyType = apiSelfServe) or (APIKey.APIKeyType = apiWebClient), WrongLicenseException);

    UserQuestions := Context.Security.GetQuestions('EE.' + sUserName);
    Res.AddItem('QUESTIONSSET', UserQuestions.Count <> 0);

    try
      ExtQuestionsList := Context.Security.GetExtQuestions('EE.' + sUserName, HashPassword(sPassword));
    except
      on E: ELockedAccount do
      begin
        if bExtSecurityOn then
          raise
        else
        begin
          CleanUpCookiesForUser(AnsiUpperCase('EE.' + EncodeUserAtDomain(sUserName, sDomain)));
          raise EBlockedAccount.Create('Account is blocked');
        end;
      end;

      on E: EBlockedAccount do
      begin
        CleanUpCookiesForUser(AnsiUpperCase('EE.' + EncodeUserAtDomain(sUserName, sDomain)));
        raise;
      end;
    end;

    ExtSecQuestions := TRpcArray.Create;

    if Assigned(ExtQuestionsList) then
    begin
      // Ext security questions presented
      for i := 0 to ExtQuestionsList.Count - 1 do
        ExtSecQuestions.AddItem(ExtQuestionsList[i]);

      if ExtSecQuestions.Count > 0 then
      begin
       if Assigned(QAList) and (QAList.Count > 0) then
       begin
         // user should answer questions, answers are provided, let's check them
         Context.Security.CheckExtAnswers('EE.' + sUserName, HashPassword(sPassword), QAList);
       end
       else
       begin
         if Assigned(Cookie) then
         begin
           sCookieId := Cookie.Keys['UserId'].AsString;
           sComputerId := Cookie.Keys['ComputerId'].AsString;
           sToken := Cookie.Keys['Token'].AsString;
           sNewToken := FindAndUpdateCookie(sCookieId, sComputerId, sToken);
           APICheckCondition(sNewToken <> '', ExpiredOrStolenCookie);
         end
         else
         begin
           // user should answer questions but no questions provided
           Res.AddItem('EXTQUESTIONS', ExtSecQuestions);
           Res.AddItem('COOKIEID', GetCookieID(AnsiUpperCase('EE.' + EncodeUserAtDomain(sUserName, sDomain))));
           AReturn.AddItem(Res);
           exit;
         end;
       end;
      end
      else
      begin
        // ext questions should be asked but they aren't set
        Res.AddItem('EXTQUESTIONS', ExtSecQuestions);
      end;
    end;
  end;

  sUserName := EncodeUserAtDomain(sUserName, sDomain);

  try
    Context := Mainboard.ContextManager.CreateThreadContext('EE.' + sUserName, HashPassword(sPassword));
  // uncomment next code if you need to collect statistics
{    Context.Statistics.FileName := NormalizePath(ExtractFilePath(Context.Statistics.FileName)) +
      Context.GetID + '_' + FormatDateTime('mmddyy', Date) + ExtractFileExt(Context.Statistics.FileName);
    Context.Statistics.Enabled := True;
    Context.Statistics.DumpIntoFile := True; }
  except
    on E: ELockedAccount do
    begin
      if bExtSecurityOn then
        raise
      else
      begin
        CleanUpCookiesForUser(AnsiUpperCase('EE.' + EncodeUserAtDomain(sUserName, sDomain)));
        raise EBlockedAccount.Create('Account is blocked');
      end;
    end;

    on E: EBlockedAccount do
    begin
      CleanUpCookiesForUser(AnsiUpperCase('EE.' + EncodeUserAtDomain(sUserName, sDomain)));
      raise;
    end;
  end;


  APICheckCondition(Context.UserAccount.AccountType = uatSelfServe, NotSSUserException);
  CheckCondition(Context.License.SelfServe, 'There is no license for SelfServe');

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
  APICheckCondition(APIKey.APIKeyType = apiSelfServe, WrongLicenseException);

  ctx_DataAccess.OpenClient(StrToIntDef(ctx_AccountRights.Clients.GetElementByIndex(0), 0));

  if Assigned(Cookie) then
  begin
    CheckCondition(GetCookieId(AnsiUpperCase('EE.' + sUserName)) = Cookie.Keys['UserId'].AsString, 'Session''s UserId does not match cookie''s UserId!');
    Res.AddItem('NewToken', sNewToken);
  end;

  // Get additional info
  Q := TevQuery.Create('SELECT CO.CO_NBR, CO.NAME CO_NAME, EE.CL_PERSON_NBR, EE.E_MAIL_ADDRESS '+
                       'FROM EE, CO WHERE {AsOfNow<EE>} AND {AsOfNow<CO>} AND '+
                       'EE.EE_NBR = :EE_NBR AND EE.CO_NBR = CO.CO_NBR');
  Q.Params.AddValue('EE_NBR', Abs(Context.UserAccount.InternalNbr));
  Q.Execute;

  Res.AddItem('SESSION', Context.GetID);
  Res.AddItem('VER', AppVersion);
  Res.AddItem('CLNBR', ctx_DataAccess.ClientID);
  iCoNbr := Q.Result.Fields.FieldByName('CO_NBR').AsInteger;
  Res.AddItem('CONBR', iCoNbr);
  Res.AddItem('CLPNBR', Q.Result.Fields.FieldByName('CL_PERSON_NBR').AsInteger);
  Res.AddItem('EENBR', Abs(Context.UserAccount.InternalNbr));
  iEeNbr := Abs(Context.UserAccount.InternalNbr);
  Res.AddItem('FNAME', Context.UserAccount.FirstName);
  Res.AddItem('LNAME', Context.UserAccount.LastName);
  Res.AddItem('CONAME', Q.Result.Fields.FieldByName('CO_NAME').AsString);
  Res.AddItem('COOKIEID', GetCookieId(AnsiUpperCase('EE.' + sUserName)));

  if bExtSecurityOn then
  begin
    Res.AddItem('EEEMAIL', Q.Result.Fields.FieldByName('E_MAIL_ADDRESS').AsString);
    PasswordChangeDate := Trunc(Context.UserAccount.PasswordChangeDate);
    iDaysToExpire := Trunc(PasswordChangeDate - Trunc(SysTime));
    Res.AddItem('PWDEXPIRE', iDaysToExpire);
  end;

  // calculating EMail
  Q := TevQuery.Create('select EMAIL_ADDRESS, PHONE_TYPE from CO_PHONE a '+
    ' where ({AsOfNow<a>}) and (EMAIL_ADDRESS is not null) and (CO_NBR=:P_CO_NBR) ' +
    ' and (PHONE_TYPE in (''P'', ''S'')) order by PHONE_TYPE desc');
  Q.Params.AddValue('P_CO_NBR', iCoNbr);
  Q.Execute;
  if Q.Result.RecordCount = 0 then
    Res.AddItem('COEMAIL', '')
  else
    Res.AddItem('COEMAIL', Q.Result.Fields.FieldByName('EMAIL_ADDRESS').AsString);

  // checkinf if ESS is enabled and calculating role
  APICheckCondition(ctx_AccountRights.Functions.GetState('ESS_EDIT') <> stDisabled, ESSNotEnabledException);
  Q := TevQuery.Create('select EE_NBR from CO_GROUP_MANAGER a where {AsOfNow<a>} and a.EE_NBR=:p_ee_nbr');
  Q.Params.AddValue('p_ee_nbr', iEENbr);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
  begin
    if ctx_AccountRights.Functions.GetState('ESS_EDIT') = stEnabled then
      Res.AddItem('ROLE', 'M')     // manager
    else
      Res.AddItem('ROLE', 'A');    // assistant
  end
  else
  begin
    if ctx_AccountRights.Functions.GetState('ESS_EDIT') = stEnabled then
      Res.AddItem('ROLE', 'E')  // employee
    else
      Res.AddItem('ROLE', 'V'); // employee RO
  end;

  // filling EE's groups list
  sGroups := '';
  Q := TevQuery.Create('select distinct GROUP_TYPE from CO_GROUP a ' +
    ' where {AsOfNow<a>} and a.CO_GROUP_NBR in ' +
    ' (select CO_GROUP_NBR from CO_GROUP_MEMBER b where {AsOfNow<b>} and b.EE_NBR=:p_ee_nbr)');
  Q.Params.AddValue('p_ee_nbr', iEENbr);
  Q.Execute;
  while not Q.Result.Eof do
  begin
    if Q.Result['GROUP_TYPE'] <> null then
      sGroups := sGroups + Q.Result['GROUP_TYPE'];
    Q.Result.Next;
  end;
  Res.AddItem('GROUPS', sGroups); // employee RO

  AReturn.AddItem(Res);

  ContextObjFromPool := FContextPool.AcquireObject(Context.GetID) as IContextObjFromPool;
  ContextObjFromPool.SetContext(Context);
  ContextObjFromPool.SetLicenseKey(APIKey);
  sTermsOfUse := CheckIfTermsOfUseAccepted(IntToStr(ctx_DataAccess.ClientId) + '.' + IntToStr(Context.UserAccount.InternalNbr), APIKey);
  FContextPool.ReturnObject(ContextObjFromPool as IisObjectFromPool);

  if sTermsOfUse = '' then
    ContextObjFromPool.SetTermsOfUseAccepted(True)
  else
    Res.AddItemBase64Str('TERMSOFUSE', sTermsOfUse);
end;

procedure TevXmlRPCServer.EE_RegNewAccount(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sServer, sUserName, sPassword, sDomain, sCompanyId, sSSN, sGUID:  String;
  iCheckNumber: Integer;
  fCheckTotalEarnings: Currency;
  Context: IevContext;
  EEConnectParams: IInterfaceList;
  EEParam: IRpcParameter;
  bEnableExtSecurity: boolean;
  PlaceHolderArray: IRPCArray;
begin
// evo.ee.regNewAccount(string LicenseKey, string evoServer, string user, string password, string evoDomain,
//                      string companyId, string SSN, int checkNumber, float checkTotalEarnings, [boolean EnableExtSecurity])
//
//  Return: struct - see 'ee.connect' for more information

  APICheckCondition(AParams.Count >= 9, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  sServer := IRpcParameter(AParams[1]).AsString;
  sUserName := IRpcParameter(AParams[2]).AsString;
  sPassword := IRpcParameter(AParams[3]).AsString;
  sDomain := IRpcParameter(AParams[4]).AsString;
  sCompanyId := IRpcParameter(AParams[5]).AsString;
  sSSN := IRpcParameter(AParams[6]).AsString;
  iCheckNumber :=  IRpcParameter(AParams[7]).AsInteger;
  fCheckTotalEarnings := IRpcParameter(AParams[8]).AsFloat;

  if AParams.Count > 9 then
    bEnableExtSecurity := IRpcParameter(AParams[9]).AsBoolean
  else
    bEnableExtSecurity := false;

  // Registration
  try
    Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');
    Context.Security.ValidatePassword(sPassword);
    Context.Security.CreateEEAccount(sUserName, HashPassword(sPassword), sCompanyId, sSSN, iCheckNumber, fCheckTotalEarnings);
  finally
    Context := nil;
    Mainboard.ContextManager.DestroyThreadContext;
  end;

  // Connect
  EEConnectParams := TInterfaceList.Create;
  EEParam := TRpcParameter.Create;
  EEParam.AsString := sGUID;
  EEConnectParams.Add(EEParam);

  EEParam := TRpcParameter.Create;
  EEParam.AsString := sServer;
  EEConnectParams.Add(EEParam);

  EEParam := TRpcParameter.Create;
  EEParam.AsString := sUserName;
  EEConnectParams.Add(EEParam);

  EEParam := TRpcParameter.Create;
  EEParam.AsString := sPassword;
  EEConnectParams.Add(EEParam);

  EEParam := TRpcParameter.Create;
  EEParam.AsString := sDomain;
  EEConnectParams.Add(EEParam);

  if bEnableExtSecurity then
  begin
    EEParam := TRpcParameter.Create;
    PlaceHolderArray := TRPCArray.Create;
    EEParam.AsArray := PlaceHolderArray;
    EEConnectParams.Add(EEParam);
  end;

  EE_Connect(EEConnectParams, AReturn, AMethodName);
end;

procedure TevXmlRPCServer.ChangePassword(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  sNewPassword: string;
begin
//  evo.changePassword(string sessionId, string oldPassword, string newPassword)
//  Return: boolean - true if successful
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sNewPassword := IRpcParameter(AParams[2]).AsString;

  SetCurrentContext(sContextID);

  Context.Security.ValidatePassword(sNewPassword);
  ChangeUserPassword(sContextId, Context.UserAccount.User, sNewPassword, Context.UserAccount.Domain);

  AReturn.AddItem(True);
end;

procedure TevXmlRPCServer.ValidatePassword(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  sPassword: string;
begin
//  evo.validatePassword(string sessionId, string password)
//  Return: boolean - true if successful
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sPassword := IRpcParameter(AParams[1]).AsString;

  SetCurrentContext(sContextID);
  Context.Security.ValidatePassword(sPassword);

  AReturn.AddItem(true);
end;

procedure TevXmlRPCServer.SetCurrentContext(const AContextId: String; const ATermsOfUse: TTermsOfUse = touCheck);
var
  Context: IevContext;
  ContextObjFromPool: IContextObjFromPool;
begin
  Context := Mainboard.ContextManager.FindContextByID(AContextId);

  APICheckCondition(Assigned(Context), WrongSessionException);

  // refreshing object in pool in order to prevent its deleting
  ContextObjFromPool := FContextPool.AcquireObject(AContextId) as IContextObjFromPool;
  FContextPool.ReturnObject(ContextObjFromPool as IisObjectFromPool);
  if ATermsOfUse = touSetAccepted then
    ContextObjFromPool.SetTermsOfUseAccepted(True)
  else
    if ATermsOfUse = touCheck then
      CheckCondition(ContextObjFromPool.GetTermsOfUseAccepted, 'User did not accept Terms Of Use');

  Mainboard.ContextManager.SetThreadContext(Context);

  if Context.UserAccount.AccountType = uatSelfServe then
    ctx_DataAccess.OpenClient(StrToIntDef(ctx_AccountRights.Clients.GetElementByIndex(0), 0));
end;

function TevXmlRPCServer.DataSetToStruct(const ADataSet: IevDataSet; const sColumnMap: string; const ReturnTableNameFromMap: boolean = false): IRpcStruct;
var
  i, j: Integer;
  ddField: TddsField;
  ddTable:  TDataDictTable;
  ddOneTable: TDataDictTable;
  sTableName: string;
  BlobData: IisStream;
  dsXML: IRpcStruct;
  rowXML: IRpcStruct;
  colXML: IRpcStruct;
  valXML: IRpcStruct;
  DBTypes: array [TFieldType] of string;
  SQLTypes: array [TFieldType] of integer;
begin
  DBTypes[ftBlob] := 'BLOB';
  SQLTypes[ftBlob] := 2004;
  DBTypes[ftMemo] := DBTypes[ftBlob];
  SQLTypes[ftMemo] := SQLTypes[ftBlob];
  DBTypes[ftString] := 'VARCHAR';
  SQLTypes[ftString] := 12;
  DBTypes[ftBoolean] := 'BOOLEAN';
  SQLTypes[ftBoolean] := 16;
  DBTypes[ftInteger] := 'INTEGER';
  SQLTypes[ftInteger] := 4;
  DBTypes[ftFloat] := 'NUMERIC';
  SQLTypes[ftFloat] := 2;
  DBTypes[ftCurrency] := DBTypes[ftFloat];
  SQLTypes[ftCurrency] := SQLTypes[ftFloat];
  DBTypes[ftDate] := 'DATE';
  SQLTypes[ftDate] := 91;
  DBTypes[ftTimeStamp] := 'TIMESTAMP';
  SQLTypes[ftTimeStamp] := 93;
  DBTypes[ftDateTime] := DBTypes[ftTimeStamp];
  SQLTypes[ftDateTime] := SQLTypes[ftTimeStamp];

  dsXML := TRpcStruct.Create;
  rowXML := TRpcStruct.Create;

  if Pos('*', sColumnMap)=1 then
  begin
    sTableName := GetNamedValue(sColumnMap, '*', ',');
    ddOneTable := EvoDataDictionary.Tables.TableByName(sTableName);
    CheckCondition(Assigned(ddOneTable), 'Table "' + sTableName + '" is not found');
  end
  else
  begin
    sTableName := '';
    ddOneTable := nil;
  end;

  // add metadata for each column in dataset
  for i := 0 to ADataSet.FieldCount - 1 do
  begin
    if Assigned(ddOneTable) then
      ddTable := ddOneTable
    else
    begin
      sTableName := GetNamedValue(sColumnMap, UpperCase(ADataSet.Fields[i].FieldName), ',');
      ddTable := EvoDataDictionary.Tables.TableByName(sTableName)
    end;

    if Assigned(ddTable) then
    begin
      ddField := TddsField(ddTable.Fields.FieldByName(ADataSet.Fields[i].FieldName));
      //CheckCondition(Assigned(ddField), 'Field "' + ddTable.Name + '.' + ADataSet.Fields[i].FieldName + '" is not found');
    end
    else
      ddField := nil;

    colXML := TRpcStruct.Create;

    // tablename
    if Assigned(ddTable) then
      colXML.AddItem('tb', ddTable.Name)
    else
    begin
      if ReturnTableNameFromMap and (sTableName <> '') then
        colXML.AddItem('tb', sTableName)
      else
        colXML.AddItem('tb', 'Custom');
    end;

    // field name
    if Assigned(ddField) then
      colXML.AddItem('nm', ddField.Name)
    else
      colXML.AddItem('nm', ADataSet.Fields[i].FieldName);

    // label
//TODO - displayName in ddField
    if Assigned(ddField) and (ddField.DisplayName <> '') then
      colXML.AddItem('lb', ddField.DisplayName)
    else
      colXML.AddItem('lb', ADataSet.Fields[i].DisplayName);

    // type
    colXML.AddItem('tp', SQLTypes[ADataSet.Fields[i].DataType]);

    // typename
    colXML.AddItem('tn', DBTypes[ADataSet.Fields[i].DataType]);

    // nullable
    if Assigned(ddField) then
      colXML.AddItem('nl', Ord(not ddField.Required))
    else
      colXML.AddItem('nl', Ord(not ADataSet.Fields[i].Required));

    // size
    colXML.AddItem('sz', ADataSet.Fields[i].Size);

    // precision
    if ADataSet.Fields[i].DataType = ftFloat then
      colXML.AddItem('pr', 18);

    // scale
    case ADataSet.Fields[i].DataType of
      ftCurrency:
        colXML.AddItem('sc', 4);
      ftFloat:
        colXML.AddItem('sc', 6);
    end;

    if Assigned(ddField) then
    begin
      valXML := TRpcStruct.Create;

      // fieldValues
      for j := 0 to ddField.FieldValues.Count - 1 do
          valXML.AddItem(ddField.FieldValues.Names[j], ddField.FieldValues.ValueFromIndex[j]);

      if valXml.Count > 0 then
        colXML.AddItem('fv', valXML);

      if ddField.DefaultValue <> '' then
        colXML.AddItem('dv', ddField.DefaultValue);

      // description
      if ddField.ContextHelp <> '' then
        colXML.AddItem('ds', ddField.ContextHelp);
    end;

    rowXML.AddItem(IntToStr(i), colXml);
  end;

  dsXml.AddItem('meta', rowXML);

  // add column values
  j := 0;
  valXML := TRpcStruct.Create;
  rowXML := TRpcStruct.Create;

  // process Rows
  ADataSet.First;
  while not ADataSet.Eof do
  begin
    colXML := TRpcStruct.Create;

    // process Columns
    for i := 0 to ADataSet.FieldCount - 1 do
    begin
      if not ADataSet.Fields[i].IsNull then
      begin
        case ADataSet.Fields[i].DataType of
        ftBlob, ftMemo:
          begin
            BlobData := TevClientDataSet(ADataSet.vclDataSet).GetBlobData(ADataSet.Fields[i].FieldName);
            if BlobData.Size > 0 then
              colXML.AddItemBase64StrFromStream(IntToStr(i), BlobData.RealStream);
          end;
        ftFloat, ftCurrency, ftBCD:
          colXML.AddItem(IntToStr(i), ADataSet.Fields[i].AsFloat);
        ftString, ftWideString:
          colXML.AddItem(IntToStr(i), ADataSet.Fields[i].AsString);
        ftBoolean:
          colXML.AddItem(IntToStr(i), ADataSet.Fields[i].AsBoolean);
        ftSmallint, ftInteger, ftWord, ftLargeint:
          colXML.AddItem(IntToStr(i), ADataSet.Fields[i].AsInteger);
        ftDate, ftTime, ftDateTime:
          colXML.AddItemDateTime(IntToStr(i), ADataSet.Fields[i].AsDateTime);
        else
          Assert(False, 'Conversion from variant type ' + IntToStr(VarType(ADataSet.Fields[i].Value)) + ' is not implemented');
        end;
      end;
    end;

    rowXml.AddItem(IntToStr(j), colXML);
    ADataSet.Next;
    Inc(j);
  end;

  dsXml.AddItem('val', rowXML);
  Result := dsXML;
end;

procedure TevXmlRPCServer.Legacy_OpenTable(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  sTableName: String;
  iClientNbr: Integer;
  rpcStruct: IRpcStruct;
  sColumnMap, sCondition: String;
//  Q: IevQuery;
  ds: TEvBasicClientDataSet;
  tempDS : IevDataSet;
begin
//  evo.legacy.openTable(string sessionId, string tableName, string condition, [integer clientId])
//  Deprecated! Return: struct - DataSet from the Evolution database'

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTableName := IRpcParameter(AParams[1]).AsString;

  APICheckCondition(sTableName <> '', TableNameEmptyException);

  sCondition := IRpcParameter(AParams[2]).AsString;

  SetCurrentContext(sContextID);

  if AParams.Count > 3 then
  begin
    iClientNbr :=  IRpcParameter(AParams[3]).AsInteger;
    APICheckCondition((iClientNbr = ctx_DataAccess.ClientId) or (Context.UserAccount.AccountType <> uatSelfServe), NotAllowedEEException);
  end
  else
    iClientNbr := ctx_DataAccess.ClientID;

  sColumnMap := '*=' + sTableName;

  ctx_DataAccess.OpenClient(iClientNbr);

  rpcStruct := TRpcStruct.Create;
  ds := GetDataSet(iClientNbr, sTableName, sCondition);
  try
    tempDS := TevDataSet.Create(ds);
    rpcStruct.AddItem('DATASET', DataSetToStruct(tempDS, sColumnMap));
    AReturn.AddItem(rpcStruct);
  finally
    FreeAndNil(ds);
  end;

//  rpcStruct.AddItem('DATASET', DataSetToStruct(Q.Result, sColumnMap));
//  AReturn.AddItem(rpcStruct);
end;


{******************************************************************
  evo.legacy.postTableChanges
      Posts data to a database
  @param  sessionId Evolution session Id

  @param param   EvoMappedRecord. Contains a map with table conditions and
                 field values. If map key string does not contain "__", it
                 is considered as a table name and map value is
                 considered as SQL condition to retrieve data from this
                 table before updating. If key string contains "__",
                 it is considered as a field and it should have one of
                 following formats: <table>__<field> or
                 <table>__<field>__<internal number>. First format is
                 useful when only one record will be retrieved under SQL
                 condition for this table. If internal number is negative
                 (new record), "-" can be substituted with "m". Value should
                 have a field value to post with appropriate value type.

  @return struct DataSet as <XML-RPC struct>
                 After execution will contain a conversion map from negative
                 internal numbers to positive internal numbers for new records.
                 Map key is an old negative internal number and map value is
                 a new positive internal number
******************************************************************}
procedure TevXmlRPCServer.Legacy_PostTableChanges(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  params: IRpcStruct;
  result: IRpcStruct;
begin
// evo.legacy.postTableChanges(string sessionId, struct EvoMappedRecord)
// Return: struct ConversionMap
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  params := IRpcParameter(AParams[1]).AsStruct;
  result := APIPostDataSets(ctx_DataAccess.ClientId, params);
  if not result.KeyExists('Error') then
    AReturn.AddItem(result)
  else
    AReturn.SetError(500, 'Evo Error : ' + result.Keys['Error'].AsString);
end;

procedure TevXmlRPCServer.EE_GetW2(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  iW2NBR: Integer;
  Results: IEvDualStream;
begin
//  evo.ee.getW2(string sessionId, int W2NBR)
//  Return: base64 - PDF report from the Evolution task queue

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iW2NBR := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Results := Context.RemoteMiscRoutines.GetW2(nil, IntToStr(iW2Nbr));

  if Assigned(Results) and (Results.Size>0) then
  begin
    AReturn.AddItemBase64StrFromStream(Results.RealStream);
    CollectStatistics(AMethodName);
  end;
end;

procedure TevXmlRPCServer.EE_GetW2List(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  pEENBR: Integer;
  Results: IEvDataSet;
  rpcStruct: IRPCStruct;
begin
// 'evo.ee.getW2List(string sessionId, int EE_NBR)',
// 'Return: struct - List of Employee W2 Reports',

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  pEENBR := IRpcParameter(AParams[1]).AsInteger;
  SetCurrentContext(sContextID);
  Results := Context.RemoteMiscRoutines.GetEEW2List(pEENBR);
  if Assigned(Results) then
  begin
    rpcStruct := TRpcStruct.Create;
    rpcStruct.AddItem('DATASET', DataSetToStruct(Results, ''));

    AReturn.AddItem(rpcStruct);
  end
  else
    raise Exception.Create('GetEEW2List function returned no data');
end;

procedure TevXmlRPCServer.SB_GetEmployee_E_DS(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  Results: IEvDataSet;
  rpcStruct: IRPCStruct;
  ee_nbr : integer;
  sColumnMap: String;
begin
// 'evo.ee.getEmployee_E_DS(string sessionId, int EE_NBR)',
// 'Return: struct - List of Employee Scheduled_D_DS',

// webClient call this function for one Employee, when it needs to call for many employee API needs to change. 

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sColumnMap :=
              'e_d_code_type=cl_e_ds,' +
              'custom_e_d_code_number=cl_e_ds,' +
              'description=cl_e_ds,' +
              'annual_maximum=cl_e_ds,' +
              'ee_benefits_nbr=ee_scheduled_e_ds,' +
              'co_benefit_subtype_nbr=ee_scheduled_e_ds,' +
              'use_pension_limit=ee_scheduled_e_ds,' +
              'threshold_amount=ee_scheduled_e_ds,' +
              'threshold_e_d_groups_nbr=ee_scheduled_e_ds,' +
              'maximum_pay_period_amount=ee_scheduled_e_ds,' +
              'maximum_pay_period_percentage=ee_scheduled_e_ds,' +
              'max_ppp_cl_e_d_groups_nbr=ee_scheduled_e_ds,' +
              'minimum_pay_period_amount=ee_scheduled_e_ds,' +
              'minimum_pay_period_percentage=ee_scheduled_e_ds,' +
              'min_ppp_cl_e_d_groups_nbr=ee_scheduled_e_ds,' +
              'annual_maximum_amount=ee_scheduled_e_ds,' +
              'maximum_garnish_percent=ee_scheduled_e_ds,' +
              'minimum_wage_multiplier=ee_scheduled_e_ds,' +
              'balance=ee_scheduled_e_ds,' +
              'target_amount=ee_scheduled_e_ds,' +
              'number_of_targets_remaining=ee_scheduled_e_ds,' +
              'target_action=ee_scheduled_e_ds,' +
              'scheduled_e_d_enabled=ee_scheduled_e_ds,' +
              'deductions_to_zero=ee_scheduled_e_ds,' +
              'always_pay=ee_scheduled_e_ds,' +
              'exclude_week_5=ee_scheduled_e_ds,' +
              'exclude_week_4=ee_scheduled_e_ds,' +
              'exclude_week_3=ee_scheduled_e_ds,' +
              'exclude_week_2=ee_scheduled_e_ds,' +
              'exclude_week_1=ee_scheduled_e_ds,' +
              'garnisnment_id=ee_scheduled_e_ds,' +
              'ee_child_support_cases_nbr=ee_scheduled_e_ds,' +
              'take_home_pay=ee_scheduled_e_ds,' +
              'deduct_whole_check=ee_scheduled_e_ds,' +
              'ee_direct_deposit_nbr=ee_scheduled_e_ds,' +
              'scheduled_e_d_groups_nbr=ee_scheduled_e_ds,' +
              'priority_number=ee_scheduled_e_ds,' +
              'effective_end_date=ee_scheduled_e_ds,' +
              'effective_start_date=ee_scheduled_e_ds,' +
              'cl_agency_nbr=ee_scheduled_e_ds,' +
              'cl_e_d_groups_nbr=ee_scheduled_e_ds,' +
              'which_checks=ee_scheduled_e_ds,' +
              'plan_type=ee_scheduled_e_ds,' +
              'frequency=ee_scheduled_e_ds,' +
              'percentage=ee_scheduled_e_ds,' +
              'amount=ee_scheduled_e_ds,' +
              'calculation_type=ee_scheduled_e_ds,' +
              'cl_e_ds_nbr=ee_scheduled_e_ds,' +
              'ee_scheduled_e_ds_nbr=ee_scheduled_e_ds,' +
              'null';

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  ee_nbr := IRpcParameter(AParams[1]).AsInteger;

  Results := Context.RemoteMiscRoutines.GetEEScheduled_E_DS(IntToStr(ee_nbr));

  if Assigned(Results) then
  begin
    rpcStruct := TRpcStruct.Create;
    rpcStruct.AddItem('DATASET', DataSetToStruct(Results, sColumnMap));

    AReturn.AddItem(rpcStruct);
  end
  else
    raise Exception.Create('GetEEScheduled_E_DS function returned no data');
end;

procedure TevXmlRPCServer.SB_getEeScheduledEdAmount(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  rpcStruct: IRPCStruct;
  functionParams: IRpcStruct;

  rate_value: Variant;
  rate_type: String;

  cl_e_ds_nbr, ee_benefits_nbr, co_benefit_subtype_nbr : integer;
  effective_start_date, effective_end_date : TDateTime;
  amount, target_amount, annual_maximum_amount  : variant;
  frequency : string;

begin
// 'evo.sb.getEeScheduledEdAmount(string sessionId, int cl_e_ds_nbr,
//                                       'int ee_benefits_nbr, int co_benefit_subtype_nbr, datetime  effective_start_date,
//                                       'datetime effective_end_date, float target_amount,
//                                       'float annual_maximum_amount, string frequency)
// 'Return: ...... - return Employee Scheduled_D_DS amount',

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  functionParams := IRpcParameter(AParams[1]).AsStruct;

  cl_e_ds_nbr := 0;
  if functionParams.KeyExists('cl_e_ds_nbr') then
    cl_e_ds_nbr := functionParams.Keys['cl_e_ds_nbr'].AsInteger;
  ee_benefits_nbr := 0;
  if functionParams.KeyExists('ee_benefits_nbr') then
    ee_benefits_nbr := functionParams.Keys['ee_benefits_nbr'].AsInteger;
  co_benefit_subtype_nbr := 0;
  if functionParams.KeyExists('co_benefit_subtype_nbr') then
    co_benefit_subtype_nbr := functionParams.Keys['co_benefit_subtype_nbr'].AsInteger;
  effective_start_date := 0;
  if functionParams.KeyExists('effective_start_date') then
    effective_start_date := functionParams.Keys['effective_start_date'].AsDateTime;
  effective_end_date := 0;
  if functionParams.KeyExists('effective_end_date') then
    effective_end_date := functionParams.Keys['effective_end_date'].AsDateTime;

  if functionParams.KeyExists('target_amount') then
    target_amount := functionParams.Keys['target_amount'].AsFloat;
  if functionParams.KeyExists('annual_maximum_amount') then
    annual_maximum_amount := functionParams.Keys['annual_maximum_amount'].AsFloat ;
  frequency := '';
  if functionParams.KeyExists('frequency') then
    frequency := functionParams.Keys['frequency'].AsString;

  amount := null;

  if (co_benefit_subtype_nbr > 0 ) then
     ctx_RemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(co_benefit_subtype_nbr, cl_e_ds_nbr, effective_start_date,
                                           effective_end_date, rate_type, rate_value)
  else
  if (ee_benefits_nbr > 0 ) then
     ctx_RemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(ee_benefits_nbr, cl_e_ds_nbr,
                                            effective_start_date, effective_end_date,
                                            amount, target_amount, annual_maximum_amount,
                                            frequency,
                                            rate_type, rate_value);

  rpcStruct := TRpcStruct.Create;

  if (rate_value <> Null) and (rate_type <> '') then
    case rate_type[1] of
    BENEFIT_RATES_RATE_TYPE_AMOUNT:
      rpcStruct.AddItem('Amount', VarToFloat(rate_value));
    BENEFIT_RATES_RATE_TYPE_PERCENT:
      rpcStruct.AddItem('Percentage', VarToFloat(rate_value));
    end;
  AReturn.AddItem(rpcStruct);
end;

procedure TevXmlRPCServer.GetDDTableInfo(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  sTableName, sFieldName: String;
  ddOneTable: TDataDictTable;
  ddField: TddsField;
  ResultStruct: IRPCStruct;
  FieldStruct: IRPCStruct;
  i, j: integer;
  DBTypes: array [TrwDDType] of string;
  SQLTypes: array [TrwDDType] of integer;
  valXML: IRPCArray;
begin
// 'evo.getDDTableInfo(string sessionId, string TableName [, string FieldName])',
// 'Return: struct of struct'
// if FieldName is empty, function returns information about all fields in table
//  structure of information about fields:
// 'tb' - table name, 'nm' - field name, 'lb' - display name, 'tp' - type (integer), 'tn' - type name
//  'nl' - nullable (boolean), 'dv' - default value, it could be missed, if there's no default (string),
// 'fv' - field values, which is array of strings: "key=value" (could be missed, if there's no list of values)
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTableName := AnsiUpperCase(IRpcParameter(AParams[1]).AsString);
  if AParams.Count > 2 then
    sFieldName := AnsiUpperCase(IRpcParameter(AParams[2]).AsString)
  else
    sFieldName := '';

  SetCurrentContext(sContextID);

  ddOneTable := EvoDataDictionary.Tables.TableByName(sTableName);
  CheckCondition(Assigned(ddOneTable), 'Table "' + sTableName + '" is not found in data dictionary');

  DBTypes[ddtUnknown] := 'UNKNOWN';
  SQLTypes[ddtUnknown] := -1;
  DBTypes[ddtBLOB] := 'BLOB';
  SQLTypes[ddtBLOB] := 2004;
  DBTypes[ddtMemo] := DBTypes[ddtBLOB];
  SQLTypes[ddtMemo] := SQLTypes[ddtBLOB];
  DBTypes[ddtString] := 'VARCHAR';
  SQLTypes[ddtString] := 12;
  DBTypes[ddtBoolean] := 'BOOLEAN';
  SQLTypes[ddtBoolean] := 16;
  DBTypes[ddtInteger] := 'INTEGER';
  SQLTypes[ddtInteger] := 4;
  DBTypes[ddtFloat] := 'NUMERIC';
  SQLTypes[ddtFloat] := 2;
  DBTypes[ddtCurrency] := DBTypes[ddtFloat];
  SQLTypes[ddtCurrency] := SQLTypes[ddtFloat];
  DBTypes[ddtDateTime] := 'DATE';
  SQLTypes[ddtDateTime] := 91;
  DBTypes[ddtArray] := 'ARRAY';
  SQLTypes[ddtArray] := 0;
  DBTypes[ddtGraphic] := DBTypes[ddtBLOB];
  SQLTypes[ddtGraphic] := SQLTypes[ddtBLOB];

  ResultStruct := TRPCStruct.Create;
  for i := 0 to ddOneTable.Fields.Count - 1 do
  begin
    if (sFieldName <> '') and (ddOneTable.Fields[i].Name <> sFieldName) then
      continue;
    FieldStruct := TRPCStruct.Create;
    ddField :=  TddsField(ddOneTable.Fields[i]);

    FieldStruct.AddItem('tb', ddOneTable.Name);
    FieldStruct.AddItem('nm', ddField.Name);
    FieldStruct.AddItem('lb', ddField.DisplayName);
    FieldStruct.AddItem('tp', SQLTypes[ddField.FieldType]);
    FieldStruct.AddItem('tn', DBTypes[ddField.FieldType]);
    FieldStruct.AddItem('nl', Ord(not ddField.Required));

    if ddField.DefaultValue <> '' then
      FieldStruct.AddItem('dv', ddField.DefaultValue);

    // fieldValues
    valXML := TRpcArray.Create;
    for j := 0 to ddField.FieldValues.Count - 1 do
      valXML.AddItem(ddField.FieldValues[j]);
    if valXml.Count > 0 then
      FieldStruct.AddItem('fv', valXML);

    ResultStruct.AddItem(ddField.Name, FieldStruct);
  end;

  AReturn.AddItem(ResultStruct);
end;

procedure TevXmlRPCServer.TaskQueue_GetTaskSummary(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID, sTaskNbr: String;
  TaskInfo: IevTaskInfo;
  Struct: IRpcStruct;

  function GetTaskState(const ATaskState: TevTaskState): String;
  begin
    case ATaskState of
      tsInactive:  Result := 'Queued';
      tsWaiting:   Result := 'Waiting for resources';
      tsExecuting: Result := 'Executing';
      tsFinished:  Result := 'Finished';
    end;
  end;

begin
//  evo.taskQueue.getTaskSummary(string sessionId, string taskNbr)
//  Return: struct - <propertyName, propertyValue>

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTaskNbr := IRpcParameter(AParams[1]).AsString;
  SetCurrentContext(sContextID);

  TaskInfo := mb_TaskQueue.GetTaskInfo(TaskNbrToId(sTaskNbr));
  APICheckCondition(Assigned(TaskInfo), QueueTaskNotFoundException);

  Struct := TRpcStruct.Create;
  Struct.AddItem('ID', TaskInfo.Nbr);
  Struct.AddItem('Nbr', TaskInfo.Nbr);
  Struct.AddItem('Type', TaskInfo.TaskType);
  Struct.AddItem('Caption', TaskInfo.Caption);
  Struct.AddItem('UserName', TaskInfo.User);
  Struct.AddItem('EvoDomain', TaskInfo.Domain);
  Struct.AddItem('Priority', TaskInfo.Priority);
  Struct.AddItem('SendEmail', TaskInfo.SendEmailNotification);
  Struct.AddItem('Email', TaskInfo.NotificationEmail);
  Struct.AddItem('Status', GetTaskState(TaskInfo.State));
  Struct.AddItem('ProgressText', TaskInfo.ProgressText);
  Struct.AddItem('LastUpdate', TaskInfo.LastUpdate);
  Struct.AddItem('Seen', TaskInfo.SeenByUser);
  Struct.AddItem('StartedAt', TaskInfo.FirstRunAt);

  AReturn.AddItem(Struct);
end;

procedure TevXmlRPCServer.TaskQueue_IsTaskFinished(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID, sTaskNbr: String;
  TaskInfo: IevTaskInfo;
begin
//  evo.taskQueue.getTaskIsFinished(string sessionId, string taskNbr)
//  Return: Boolean

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTaskNbr := IRpcParameter(AParams[1]).AsString;
  SetCurrentContext(sContextID);

  TaskInfo := mb_TaskQueue.GetTaskInfo(TaskNbrToId(sTaskNbr));
  AReturn.AddItem(TaskInfo.State = tsFinished);
end;

procedure TevXmlRPCServer.TaskQueue_RunPayrollReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  Task: IevReprintPayrollTask;
  iPrNbr: Integer;
  dCheckDate: TDateTime;
  sPayrollChecks, sTaskCaption: String;
  TaskInfo: IevTaskInfo;
  sTaskId : String;
begin
//  evo.taskQueue.runPayrollReport(string sessionId, integer prNbr, datetime checkDate, string payrollChecks[, string taskCaption])
//  Return: string - Task Nbr

  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iPrNbr := IRpcParameter(AParams[1]).AsInteger;
  dCheckDate := IRpcParameter(AParams[2]).AsDateTime;
  sPayrollChecks := IRpcParameter(AParams[3]).AsString;
  if AParams.Count > 4 then
    sTaskCaption := IRpcParameter(AParams[4]).AsString
  else
    sTaskCaption := 'runPayrollReport - Payroll: ' + IntToStr(iPrNbr) + '  Check: ' + sPayrollChecks;

  SetCurrentContext(sContextID);

  Task := mb_TaskQueue.CreateTask(QUEUE_TASK_REPRINT_PAYROLL, True) as IevReprintPayrollTask;
  Task.Caption := sTaskCaption;
  Task.CheckDate := dCheckDate;
  Task.ClientID := ctx_DataAccess.ClientID;
  Task.PrNbr := iPrNbr;
  Task.PayrollChecks := sPayrollChecks;
  Task.PrintReports := false;
  Task.PrintInvoices := false;
  Task.AgencyChecks := '';
  Task.TaxChecks := '';
  Task.BillingChecks := '';
  Task.CheckForm := 'B';
  Task.MiscCheckForm := 'R';
  Task.Destination := rdtPreview;

  mb_TaskQueue.AddTask(Task);
  TaskInfo := mb_TaskQueue.GetTaskInfo(Task.GetId);
  sTaskId := IntToStr(TaskInfo.Nbr);
  AReturn.AddItem(sTaskId);
  CollectStatistics(AMethodName);
end;

procedure TevXmlRPCServer.TaskQueue_RunPreprocessPayroll(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  Task: IevPreprocessPayrollTask;
  sPrList, sTaskCaption: String;
  TaskInfo: IevTaskInfo;
  sTaskId : String;
begin
//  evo.taskQueue.runPreprocessPayroll(string sessionId, string prNbrList, [ string taskCaption])
//  Return: string - Task Nbr

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sPrList := IRpcParameter(AParams[1]).AsString;
  if AParams.Count > 2 then
    sTaskCaption := IRpcParameter(AParams[2]).AsString
  else
    sTaskCaption := 'runPreprocessPayroll: ' + sPrList;

  SetCurrentContext(sContextID);

  Task := mb_TaskQueue.CreateTask(QUEUE_TASK_PREPROCESS_PAYROLL, True) as IevPreprocessPayrollTask;
  Task.Caption := sTaskCaption;
  Task.PrList := IntToStr(ctx_DataAccess.ClientId) + ';' + sPrList + ' ';

  mb_TaskQueue.AddTask(Task);
  TaskInfo := mb_TaskQueue.GetTaskInfo(Task.GetId);
  sTaskId := IntToStr(TaskInfo.Nbr);
  AReturn.AddItem(sTaskId);
  CollectStatistics(AMethodName);
end;

procedure TevXmlRPCServer.TaskQueue_RunCreatePayroll(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID: String;
  TaskParams: IRpcStruct;
  Task: IevCreatePayrollTask;
  tmpStream : IEvDualStream;
  TaskInfo: IevTaskInfo;
  sTaskId : String;

  function getEXCLUDE_R_C_B_0R_N(const clNbr, coNbr: integer):integer;
  var
    s: string;
    Q: IevQuery;
  begin
       result := 3;
       if clNbr > 0 then
       begin
         ctx_DataAccess.OpenClient(clNbr);

         s:= 'select CASE EXCLUDE_R_C_B_0R_N ' +
                'WHEN ''R'' THEN 0 ' +
                'WHEN ''C'' THEN 1 ' +
                'WHEN ''B'' THEN 2 ' +
                'WHEN ''N'' THEN 3 ' +
                'ELSE 3 ' +
              'END BlockChecks from CO where CO_NBR= :coNbr and {AsOfNow<CO>} ';
         Q := TevQuery.Create(s);
         Q.Params.AddValue('coNbr', coNbr);
         Q.Execute;
         if Q.Result.RecordCount > 0 then
            result := Q.Result.Fields[0].AsInteger;
       end;
  end;

begin
//  evo.taskQueue.runCreatePayroll(string sessionId, struct taskParam)
//  Return: string - Task Nbr

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  TaskParams := IRpcParameter(AParams[1]).AsStruct;
  SetCurrentContext(sContextID);

  tmpStream := TEvDualStreamHolder.Create;

  Task := mb_TaskQueue.CreateTask(QUEUE_TASK_CREATE_PAYROLL, True) as IevCreatePayrollTask;

  if TaskParams.KeyExists('CheckDate') then
    Task.CheckDate := TaskParams.Keys['CheckDate'].AsDateTime;
  if TaskParams.KeyExists('CheckDateReplace') then
    Task.CheckDateReplace := TaskParams.Keys['CheckDateReplace'].AsInteger;
  if TaskParams.KeyExists('SalaryOrHourly') then
    Task.SalaryOrHourly := TaskParams.Keys['SalaryOrHourly'].AsInteger;
  if TaskParams.KeyExists('BatchFreq') then
    Task.BatchFreq := TaskParams.Keys['BatchFreq'].AsString;
  if TaskParams.KeyExists('PeriodBeginDate') then
    Task.PeriodBeginDate := TaskParams.Keys['PeriodBeginDate'].AsDateTime;
  if TaskParams.KeyExists('PeriodEndDate') then
    Task.PeriodEndDate := TaskParams.Keys['PeriodEndDate'].AsDateTime;
  if TaskParams.KeyExists('EEList') then
    Task.EEList := TaskParams.Keys['EEList'].AsString;
  if TaskParams.KeyExists('BlockTaxDeposits') then
    Task.BlockTaxDeposits := TaskParams.Keys['BlockTaxDeposits'].AsInteger;
  if TaskParams.KeyExists('BlockChecks') then
    Task.BlockChecks := TaskParams.Keys['BlockChecks'].AsInteger;
  if TaskParams.KeyExists('BlockTimeOff') then
    Task.BlockTimeOff := TaskParams.Keys['BlockTimeOff'].AsInteger;
  if TaskParams.KeyExists('BlockAgencies') then
    Task.BlockAgencies := TaskParams.Keys['BlockAgencies'].AsInteger;
  if TaskParams.KeyExists('Block401K') then
    Task.Block401K := TaskParams.Keys['Block401K'].AsInteger;
  if TaskParams.KeyExists('BlockACH') then
    Task.BlockACH := TaskParams.Keys['BlockACH'].AsInteger;
  if TaskParams.KeyExists('BlockBilling') then
    Task.BlockBilling := TaskParams.Keys['BlockBilling'].AsInteger;
  if TaskParams.KeyExists('TcImportSourceFile') then
  begin
    TaskParams.Keys['TcImportSourceFile'].Base64StrSaveToStream(tmpStream.RealStream);
    tmpStream.Position := 0;
    Task.TcImportSourceFile := tmpStream;
  end;
  if TaskParams.KeyExists('TcLookupOption') then
    Task.TcLookupOption := TCImportLookupOption(TaskParams.Keys['TcLookupOption'].AsInteger);
  if TaskParams.KeyExists('TcDBDTOption') then
    Task.TcDBDTOption := TCImportDBDTOption(TaskParams.Keys['TcDBDTOption'].AsInteger);
  if TaskParams.KeyExists('TcFileFormat') then
    Task.TcFileFormat := TCFileFormatOption(TaskParams.Keys['TcFileFormat'].AsInteger);
  if TaskParams.KeyExists('TcFourDigitYear') then
    Task.TcFourDigitYear := TaskParams.Keys['TcFourDigitYear'].AsBoolean;
  if TaskParams.KeyExists('TcAutoImportJobCodes') then
    Task.TcAutoImportJobCodes := TaskParams.Keys['TcAutoImportJobCodes'].AsBoolean;
  if TaskParams.KeyExists('CLNbr') then
    Task.CLNbr := TaskParams.Keys['CLNbr'].AsInteger;
  if TaskParams.KeyExists('CONbr') then
    Task.CONbr := TaskParams.Keys['CONbr'].AsInteger;
  if TaskParams.KeyExists('Caption') then
    Task.Caption := TaskParams.Keys['Caption'].AsString
  else
    Task.Caption := 'runCreatePayroll : CO # ' + IntToStr(Task.CONbr);

  Task.BlockChecks := getEXCLUDE_R_C_B_0R_N(Task.CLNbr, Task.CONbr);

  mb_TaskQueue.AddTask(Task);
  TaskInfo := mb_TaskQueue.GetTaskInfo(Task.GetId);
  sTaskId := IntToStr(TaskInfo.Nbr);
  AReturn.AddItem(sTaskId);
  CollectStatistics(AMethodName);
end;

procedure TevXmlRPCServer.ClearCurrentContext;
begin
  Mainboard.ContextManager.DestroyThreadContext;
end;

function TevXmlRPCServer.GetSSLCertFile: String;
begin
  Result := FRpcServer.SSLCertFile;
end;

function TevXmlRPCServer.GetSSLKeyFile: String;
begin
  Result := FRpcServer.SSLKeyFile;
end;

function TevXmlRPCServer.GetSSLRootCertFile: String;
begin
  Result := FRpcServer.SSLRootCertFile;
end;

procedure TevXmlRPCServer.SetSSLCertFile(const AValue: String);
begin
  FRpcServer.SSLCertFile := AValue;
end;

procedure TevXmlRPCServer.SetSSLKeyFile(const AValue: String);
begin
  FRpcServer.SSLKeyFile := AValue;
end;

procedure TevXmlRPCServer.SetSSLRootCertFile(const AValue: String);
begin
  FRpcServer.SSLRootCertFile := AValue;
end;

function TevXmlRPCServer.GetSSLEnable: Boolean;
begin
  Result := FRpcServer.SSLEnable;
end;

procedure TevXmlRPCServer.SetSSLEnable(const AValue: Boolean);
begin
  if FRpcServer.SSLEnable <> AValue then
  begin
    FRpcServer.Active := False;
    FRpcServer.SSLEnable := AValue;
    FRpcServer.Active := True;
  end;    
end;

procedure TevXmlRPCServer.ExceptionToLog(const AMethodName: String; const AException: Exception);
begin
  try
    if FLogAPIExceptions then
      mb_LogFile.AddContextEvent(etError, APIEventClass, 'Exception in "' + AMethodName + '"', MakeFullError(AException));
  except
  end;
end;

procedure TevXmlRPCServer.SB_OpenClient(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  iClientNbr: Integer;
begin
//  evo.sb.openClient(string sessionId, integer clientId, [integer companyId - this parameter is deprecated])
//  Return: boolean
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iClientNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  ctx_DataAccess.OpenClient(iClientNbr);
  AReturn.AddItem(True);
end;

procedure TevXmlRPCServer.APICheckCondition(const ACondition: Boolean; AExceptClass: ExceptClass);
begin
  if not ACondition then
  begin
    if not Assigned(AExceptClass) then
      raise EevException.Create('APICheckCondition: exception class should be assigned!')
    else
      raise AExceptClass.Create('APIException');
  end;
end;

procedure TevXmlRPCServer.Payroll_GetHoliday(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  dDate : TDateTime;
  s : String;
begin
// evo.payroll.GetHoliday(string sessionId, date Date)
// Return: string HolidayName (holidayname or empty string)

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  dDate := IRpcParameter(AParams[1]).AsDateTime;

  SetCurrentContext(sContextID);

  s := Context.PayrollCalculation.HolidayDay(dDate);
  AReturn.AddItem(s);
end;

procedure TevXmlRPCServer.testHelloMethod(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
var
  s : String;
begin
// test.Hello(string AnyString)
// Return: string - input string as result

  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  s := IRpcParameter(AParams[0]).AsString;
  AReturn.AddItem(s);
end;

{******************************************************************
  SB_TCImport -  evo.sb.TCImport
    Imports Time Clock data
  @param  sessionId Evolution session Id
  @param  companyId  Custom company number
  @param  PayrollId  Custom payroll number
  @param  ImportData Base64str - input data to be imported in supported format
  @param ImportOptions - structure with these fiels:
                          "LookupOption" (integer): 0 - By Name, 1 - By Number, 2- By SSN;
                          "DBDTOption" (integer): 0 - Full, 1 - Smart;
                          "FourDigitYear" (boolean);
                          "FileFormat" (integer): 0 - Fixed,  1 - CommaDelimited;
                          "AutoImportJobCodes" (boolean);
                          "UseEmployeePayRates" (boolean)";
  @param  BatchId  batch number - not required param

  @return Base64str     structure with two fields:
                          "RejectedRecords" (integer) - amount of rejected records
                          "LogFile" (Base64) - output log file
******************************************************************}
procedure TevXmlRPCServer.SB_TCImport(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
const
  LookupOption = 'LookupOption';
  DBDTOption = 'DBDTOption';
  FourDigitYear = 'FourDigitYear';
  FileFormat = 'FileFormat';
  AutoImportJobCodes = 'AutoImportJobCodes';
  UseEmployeePayRates = 'UseEmployeePayRates';
  RejectedRecords = 'RejectedRecords';
  LogFile = 'LogFile';
  CompanyId = 'CompanyId';
  PayrollId = 'PayrollId';
  ClientNbr = 'ClientNbr';
  BatchNbr  = 'BatchNbr';
var
  QueryData, RemoteParams, Results: IEvDualStream;
  RpcImportOptions : IRpcStruct;
  ResultsStruct : IisListOfValues;

  tmpStruct : IRpcStruct;
  tmpStream : IEvDualStream;

  QueryParams : IisListOfValues;
  RejectedAmount : integer;
  pCompanyId : integer;
  pPayrollId : integer;
  pBatchId :integer;
  sContextID : String;
begin
// evo.sb.TCImport(string sessionId, integer CompanyId, integer payrollId, Base64str importData, struct importOptions,BatchNbr)
// Return: struct
  APICheckCondition((AParams.Count >= 5) and (AParams.Count <= 6), WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  QueryData := TEVDualStreamHolder.Create;
  pCompanyId := IRpcParameter(AParams[1]).AsInteger;
  pPayrollId := IRpcParameter(AParams[2]).AsInteger;

  IRpcParameter(AParams[3]).Base64StrSaveToStream(QueryData.RealStream);
  // adding input data to one stream
  QueryData.Position := 0;
  RemoteParams := TisStream.Create;
  RemoteParams.WriteStream(QueryData);

  RpcImportOptions := IRpcParameter(AParams[4]).AsStruct;

  if AParams.Count = 6 then
    pBatchId := IRpcParameter(AParams[5]).AsInteger
  else
    pBatchId := -1;

  // getting import parameters
  QueryParams := TisListOfValues.Create;
  if RpcImportOptions.KeyExists(LookupOption) then
    QueryParams.AddValue(LookupOption, RpcImportOptions.Keys[LookupOption].AsInteger)
  else
  begin
    AReturn.SetError(811, 'Parameter "' + LookupOption + '" not provided');
    exit;
  end;
  if RpcImportOptions.KeyExists(DBDTOption) then
    QueryParams.AddValue(DBDTOption, RpcImportOptions.Keys[DBDTOption].AsInteger)
  else
  begin
    AReturn.SetError(811, 'Parameter "' + DBDTOption + '" not provided');
    exit;
  end;
  if RpcImportOptions.KeyExists(FourDigitYear) then
    QueryParams.AddValue(FourDigitYear, RpcImportOptions.Keys[FourDigitYear].AsBoolean)
  else
  begin
    AReturn.SetError(811, 'Parameter "' + FourDigitYear + '" not provided');
    exit;
  end;
  if RpcImportOptions.KeyExists(FileFormat) then
    QueryParams.AddValue(FileFormat, RpcImportOptions.Keys[FileFormat].AsInteger)
  else
  begin
    AReturn.SetError(811, 'Parameter "' + FileFormat +'" not provided');
    exit;
  end;
  if RpcImportOptions.KeyExists(AutoImportJobCodes) then
    QueryParams.AddValue(AutoImportJobCodes, RpcImportOptions.Keys[AutoImportJobCodes].AsBoolean)
  else
  begin
    AReturn.SetError(811, 'Parameter "' + AutoImportJobCodes + '" not provided');
    exit;
  end;
  if RpcImportOptions.KeyExists(UseEmployeePayRates) then
    QueryParams.AddValue(UseEmployeePayRates, RpcImportOptions.Keys[UseEmployeePayRates].AsBoolean)
  else
  begin
    AReturn.SetError(811, 'Parameter "' + UseEmployeePayRates + '" not provided');
    exit;
  end;

  QueryParams.AddValue(CompanyId, pCompanyId);
  QueryParams.AddValue(PayrollId, pPayrollId);
  QueryParams.AddValue(ClientNbr, ctx_DataAccess.ClientId);
  QueryParams.AddValue(BatchNbr, pBatchId);

  // adding all options to one stream
  QueryParams.WriteToStream(RemoteParams);

  Results := Context.RemoteMiscRoutines.TCImport(RemoteParams);

  if Assigned(Results) and (Results.Size>0) then
  begin
    Results.Position := 0;
    ResultsStruct :=TisListOfValues.Create;
    ResultsStruct.ReadFromStream(Results);
    if (not ResultsStruct.ValueExists(RejectedRecords)) or (not ResultsStruct.ValueExists(LogFile)) then
      APICheckCondition(false, ResultWrongStructureException)
    else
    begin
      rejectedAmount := ResultsStruct.Value[RejectedRecords];
      tmpStruct := TRpcStruct.Create;
      tmpStruct.AddItem(RejectedRecords, rejectedAmount);
      tmpStream := IInterface(ResultsStruct.Value[LogFile]) as IEvDualStream;
      tmpStream.Position := 0;
      tmpStruct.AddItemBase64StrFromStream(LogFile, tmpStream.RealStream);
      AReturn.AddItem(tmpStruct);
      CollectStatistics(AMethodName);
    end;
  end
  else
    APICheckCondition(false, ResultIsEmptyException);
end;

{******************************************************************
  evo.runQBQuery
     Runs QueryBuilder Query
  @param  sessionId Evolution session Id
  @param  QBQuery Base64str - Query definiton
  @param  columnMap  string 'TableField=TableName[,TableField=TableName,...]'
  @param  queryParams Map with query parameters: <paramName, paramValue> - NOT required
  @return struct DataSet as <XML-RPC struct>
******************************************************************}
procedure TevXmlRPCServer.SB_RunQBQuery(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  QueryData, RemoteParams, Results: IEvDualStream;
  ColumnsMap : String;
  i : integer;
  Params: IRpcStruct;
  QueryParams: TEvBasicParams;
  paramName : String;
  ResDS: TEvClientDataSet;
  rpcStruct: IRpcStruct;
  tmpDS: IevDataset;
begin
// evo.runQBQuery(string sessionId, Base64str QBQuery, string columnMap, [struct queryParams])
// Return: struct - DataSet

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  QueryData := TEVDualStreamHolder.Create;
  IRpcParameter(AParams[1]).Base64StrSaveToStream(QueryData.RealStream);
  QueryData.Position := 0;
  RemoteParams := TisStream.Create;
  RemoteParams.WriteStream(QueryData);

  ColumnsMap := IRpcParameter(AParams[2]).AsString;

  if AParams.Count > 3 then
    params := IRpcParameter(AParams[3]).AsStruct
  else
    params := nil;

  QueryParams := TEvBasicParams.Create;
  try
    if Assigned(Params) then
    begin
      for i := 0 to params.Count-1 do
      begin
        paramName := params.KeyList.Strings[i];
        case params.Items[i].DataType of
          dtArray, dtNone:
            APICheckCondition(false, UnsupportedParamTypeException);
          dtString:
            QueryParams.AddParam(paramName, params.Items[i].AsString);
          dtBoolean:
            QueryParams.AddParam(paramName, params.Items[i].AsBoolean);
          dtDateTime:
            QueryParams.AddParam(paramName, params.Items[i].AsDateTime);
          dtFloat:
            QueryParams.AddParam(paramName, params.Items[i].AsFloat);
          dtInteger:
            QueryParams.AddParam(paramName, params.Items[i].AsInteger);
          else
            APICheckCondition(false, UnknownParamTypeException);
        end; // case
      end;  // for
    end;  // if

    QueryParams.Stream.Position := 0;
    RemoteParams.WriteStream(QueryParams.Stream);
  finally
    FreeAndNil(QueryParams);
  end;
  RemoteParams.Position := 0;

  Results := Context.RWRemoteEngine.RunQBQuery(RemoteParams, ctx_DataAccess.ClientId, 0);

  if Assigned(Results) then
  begin
    ResDS := TEvClientDataSet.Create(nil);
    try
      Results.Position := 0;
      TEvBasicClientDataSet(ResDS).LoadFromStream(Results.RealStream);
      tmpDS := TevDataSet.Create(ResDS);
      rpcStruct := TRpcStruct.Create;
      rpcStruct.AddItem('DATASET', DataSetToStruct(tmpDS, ColumnsMap));
      AReturn.AddItem(rpcStruct);
    finally
      FreeAndNil(ResDs);
    end;
    CollectStatistics(AMethodName);
  end
  else
    APICheckCondition(false, WrongReturnTypeException);
end;

{******************************************************************
  TaskQueue_GetReport  -  evo.taskQueue.getReport
     Returns type of reports format and native report from queue
  @param  sessionId      Evolution session ID
  @param  taskNbr        Queue task id for report
  @return struct of two fields:
     - ReportType (string: UNKNOWN, RWA, ASCII (keep in mind that XML is ASCII too), ERROR (ASCII text of error message)),
     - Report (report in native format from the Evolution task queue encoded as base64 byte array)
******************************************************************}
procedure TevXmlRPCServer.TaskQueue_GetReport(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  sTaskNbr : String;
  TskInf: IevTaskInfo;
  Tsk: IevTask;
  Res, stream : IEvDualStream;
  tmpStruct : IRpcStruct;
  reportResults : TrwReportResults;
  tmpId : TisGUID;
begin
//  evo.taskQueue.getReport(string sessionId, string taskNbr)
// Return: struct with two fields: ReportType (values: UNKNOWN, RWA, ASCII, ERROR) and Report (base64 encoded byte array)

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  sTaskNbr := IRpcParameter(AParams[1]).AsString;
  tmpId := TaskNbrToId(sTaskNbr);
  TskInf := mb_TaskQueue.GetTaskInfo(tmpId);

  APICheckCondition(Assigned(TskInf), QueueTaskNotFoundException);
  APICheckCondition(TskInf.State = tsFinished, QueueTaskNotFinishedException);

  tmpStruct := TRpcStruct.Create;

  Tsk := mb_TaskQueue.GetTask(tmpId);
  if Supports(Tsk, IevPrintableTask) then
  begin
    Res := (Tsk as IevPrintableTask).ReportResults;
    reportResults := TrwReportResults.Create(Res);
    try
      if reportResults.Items[0].ErrorMessage = '' then
      begin
        Case reportResults.Items[0].ReportType of
          rtUnknown : tmpStruct.AddItem('Type', 'UNKNOWN');
          rtASCIIFile : tmpStruct.AddItem('Type', 'ASCII');
          else
            tmpStruct.AddItem('Type', 'RWA');
        end;
        tmpStruct.AddItemBase64StrFromStream('Report', reportResults.Items[0].Data.RealStream);
      end
      else
      begin
        tmpStruct.AddItem('Type', 'ERROR');
        stream := TEvDualStreamHolder.Create;
        stream.WriteString(reportResults.Items[0].ErrorMessage);
        tmpStruct.AddItemBase64StrFromStream('Report', stream.realStream);
      end;
    finally
      if Assigned(reportResults) then reportResults.Free;
    end;
  end
  else
    APICheckCondition(false, ResultWrongStructureException);

  AReturn.AddItem(tmpStruct);
end;

{**************************************************
  Convert XML-RPC struct to Delphi DataSet
**************************************************}
function TevXmlRPCServer.OldStructToDataSet(const AdsXml: IRpcStruct): TevClientDataSet;
var
  i, j: Integer;
  BlobData: IevDualStream;

//  dd: TddsDataDictionary;
//  ddField: TddsField;
//  ddTable:  TDataDictTable;
  sTableName: string;
  sFieldName: string;
  FieldType: TFieldType;
  fType: Integer;
  fieldSize: Integer;
  fieldRequired: boolean;
  sColNbr: string;
  s: String;

  dsField: TField;
  rowXml: IRpcStruct;
  colXml: IRpcStruct;
  metaXml: IRpcStruct;
  valXml: IRpcStruct;
  dsXml: IRpcStruct;
begin
  dsXML := AdsXML;
  Result := TEvClientDataSet.Create(nil);
  try
//    if( (dsXml.KeyList.Strings[0] = 'meta') and (dsXml.KeyList.Strings[1] = 'val') ) then

    if ( (dsXml.KeyList.Strings[0] = 'DATASET') and
        (dsXml.Items[0].AsStruct.KeyList.Strings[0] = 'meta') and
        (dsXml.Items[0].AsStruct.KeyList.Strings[1] = 'val') ) then
    begin
dsXml := dsXml.Items[0].AsStruct;
      // get Data Dictionary from the storage
      //dd := ddList.GetDictionary(HandleToConnection(Handle).Version);
      // get meta info and create DS
      metaXml := dsXml.Items[0].AsStruct;
      for i := 0 to metaXML.Count-1 do
      begin
        colXml := metaXml.Items[i].AsStruct;
        sTableName := '';
        sFieldName := '';
        fType := 0;
        fieldSize := 0;
        fieldRequired := false;

        for j := 0 to colXml.Count-1 do
        begin
          if colXml.KeyList.Strings[j] = 'tb' then
            sTableName :=colXml.Items[j].AsString
          else if colXml.KeyList.Strings[j] = 'nm' then
            sFieldName :=colXml.Items[j].AsString
          else if colXml.KeyList.Strings[j] = 'tp' then
            fType := colXml.Items[j].AsInteger
          else if colXml.KeyList.Strings[j] = 'sz' then
            fieldSize :=colXml.Items[j].AsInteger
          else if colXml.KeyList.Strings[j] = 'nl' then
            fieldRequired := colXml.Items[j].AsInteger = 0;
        end; // for - field info

        if (sTableName = '') or (sFieldName = '') then
        begin
          // error: incorrect meta info
          Result.Free;
          Result := nil;
          Exit;
        end;

          FieldType := ftUnknown; // ???
          case fType of
            2004: FieldType := ftBlob;
            12:   FieldType := ftString;
            16:   FieldType := ftBoolean;
            4:    FieldType := ftInteger;
            2:    FieldType := ftFloat;
            91:   FieldType := ftDate;
            93:   FieldType := ftDateTime;  // not TimeStamp !
          end;
            //ddTable := dd.Tables.TableByName(sTableName);
            //ddField := TddsField(ddTable.Fields.FieldByName(sFieldName));
          Result.FieldDefs.Add(sFieldName, FieldType, fieldSize, fieldRequired );
            //break;

      end; // for - colimns

      Result.LogChanges := False;
      Result.CreateDataSet;

      // get values and fill out DS
      if dsXml.Items[1].isStruct then // if non-empty DS
      begin
        rowXML := dsXml.Items[1].AsStruct;
        for i := 0 to rowXML.Count-1 do   // rows
        begin
          valXml := rowXml.Items[i].AsStruct;
          Result.Append;
          for j := 0 to valXml.Count-1 do  // columns
          begin
            sColNbr := valXml.KeyList.Strings[j];
            sFieldName := metaXml.Keys[sColNbr].AsStruct.Keys['nm'].AsString;
            //ddField := TddsField(ddTable.Fields.FieldByName(sFieldName));
            //ddField.FieldType;
            //val := valXml.Items[j].AsString; //???

            //Result.Fields[j].Value :=
            dsField := Result.FieldByName(sFieldName);
                if not Assigned(valXml.Items[j]) then
                  dsField.Clear
                else
                  case dsField.DataType of
                    ftString:
                      dsField.Value := valXml.Items[j].AsString;
                    ftBoolean:
                      dsField.Value := valXml.Items[j].AsBoolean;
                    ftDateTime, ftDate, ftTimeStamp:
                      if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                        dsField.Clear
                      else
                        dsField.Value := valXml.Items[j].AsDateTime;
                    ftFloat, ftCurrency:
                      begin
                        if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                          dsField.Clear
                        else
                          dsField.Value := valXml.Items[j].AsFloat;
                      end;
                    ftInteger:
                      begin
                        if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                          dsField.Clear
                        else
                          dsField.Value := valXml.Items[j].AsInteger;
                      end;
                    ftBlob, ftMemo:
                    begin
                      if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                        dsField.Clear
                      else begin
                        BlobData := TEvDualStreamHolder.Create;
                        s := valXml.Items[j].AsBase64Str;
                        BlobData.WriteBuffer(s[1], Length(s));
                        BlobData.Position := 0;
                        TBlobField(dsField).LoadFromStream(BlobData.RealStream);
                        BlobData := nil;
                      end;
                    end

                  end; // case

          //        ds.Post;
          end; // for - columns
          Result.Post;
        end; // for - rows
      end; // if non-empty DS

    end
    else
    begin
      //error: incorrect dataset structure
      Result.Free;
      Result := nil;
      Exit;
    end;
  except
    Result.Free;
    raise;
  end;
end;

{******************************************************************
  evo.legacy.deleteTableValues
      Deletes data from a database table
  @param  sessionId Evolution session Id

  @param param   <XML-RPC struct> - Each entry contains a definition of what needs to be deleted for one table.
                 The entry key is a table name and the entry value is a set.
                 First entry in a set is a string with SQL condition which is used to open a table.
                 Other entries in a set are internal numbers that should be deleted from a table.
******************************************************************}
procedure TevXmlRPCServer.Legacy_DeleteTableValues(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  params : IRPCStruct;

  rpcArray: IRpcArray;
  TablesList: IisStringList;
  ConditionsList: IisStringList;
  RowNbrsList: IisStringList;
  tmpArray: IisListOfValues;
  sTableName, sTableCondition: string;
  dsFieldValue: integer;
  i, j: Integer;
begin
//  evo.legacy.deleteTableValues(string sessionId, struct Params)
// Return: boolean
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  params := IRpcParameter(AParams[1]).AsStruct;

  TablesList := TisStringList.Create;
  ConditionsList := TisStringList.Create;
  tmpArray := TisListOfValues.Create;

  for i := 0 to params.Count-1 do
  begin
    sTableName := params.KeyList.Strings[i];
    TablesList.Add(sTableName);
    rpcArray := params.Items[i].AsArray;
    sTableCondition := rpcArray.Items[0].AsString;
    ConditionsList.Add(sTableCondition);

    RowNbrsList := TisStringList.Create;
    tmpArray.AddValue(IntToStr(i), RowNbrsList);

    for j := 1 to rpcArray.Count - 1 do
    begin
      dsFieldValue := rpcArray.Items[j].AsInteger;
      RowNbrsList.Add(IntToStr(dsFieldValue));
    end;
  end;

  ctx_RemoteMiscRoutines.DeleteRowsInDataSets(ctx_DataAccess.ClientId, TablesList, ConditionsList, tmpArray);

  AReturn.AddItem(true);
end;

{**************************************************
  Retrieve DataSet from database
**************************************************}
{ todo: this function will be deleted on some day when all related API logic be moved to server side }
function TevXmlRPCServer.GetDataSet(const ClientId: Integer; sTableName, sTableCondition: string; const AsOfDate: TdateTime = 0): TevClientDataSet;
var
  DataSetParams: TGetDataParams;
  dataSet: TevClientDataSet;
  callResult: TGetDataResults;
  dsWrapper: IExecDSWrapper;
  ms: IisStream;
  DS: string;
begin
  DataSetParams := TGetDataParams.Create;
  dataSet := TevClientDataSet.Create(nil);
  try
//    if(Pos('?', sTableName)>0) then
    if sTableName = '' then
    begin
      // Prepare custom query
//      sTableName := Copy(sTableName, Pos('?', sTableName)+1, Length(sTableName)-1);
      sTableName := 'SQL_QUERY';
      dsWrapper := TExecDSWrapper.Create( sTableCondition );
      DS := StringReplace(UpperCase(sTableCondition),' ','', [rfReplaceAll]);
      if Pos('FROMSY_', DS)>0 then
        DS := 'SY_CUSTOM_PROV'
//      else if Pos('FROMSB_', DS)>0 then
      else if Pos('FROMSB', DS)>0 then
        DS := 'SB_CUSTOM_PROV'
      else if Pos('FROMTMP_', DS)>0 then
        DS := 'TEMP_CUSTOM_PROV'
      else
        DS := 'CL_CUSTOM_PROV';
      end

    else
    begin
      // Prepare one-table request with condition
      dsWrapper := TExecDSWrapper.Create('SELECT="' + Trim(sTableCondition) + '"');
      DS := UpperCase(sTableName) + '_PROV';
    end;

    DataSetParams.Write(DS, ClientId, dsWrapper);

    ms := Context.DBAccess.GetDataSets(DataSetParams);

    callResult := TGetDataResults.Create(ms);

    try
      callResult.Read(dataSet, True);
    finally
      callResult.Free;
    end;

    dataSet.Name := sTableName;
    dataSet.ProviderName := sTableName + '_PROV';
    Result := dataSet;
  finally
//    dsWrapper.Free;
    DataSetParams.Free;
  end;
end;

{******************************************************************************************
 Post changes to the database
  @param Handle       Evolution session handle
  @param ClientId       Internal client number
  @param param   Each map entry contains a definition of what needs to be updated for one table.
  @return RpcStrunct
******************************************************************************************}
function TevXmlRPCServer.APIPostDataSets(const ClientId: Integer; params: IRpcStruct): IRpcStruct;
var
  dd: TddsDataDictionary;
  ddTable:  TDataDictTable;
  dsList: TStringList;
  NbrMap : TStringList;
  i, j, k: Integer;
  sTableName, sTableCondition: string;
  dataSet: TevClientDataSet;
  iPos: Integer;
  sFieldName: string;
  ds: TevClientDataSet;
  Nbr, NewNbr: Integer;
  sDefaultValue: string;
  dsField: TField;
  CoAsOfDate: TDateTime;
  rpcStruct: IRpcStruct;
  BlobData: IevDualStream;
  s: String;
  aDS: TArrayDS;
  AsOfDatesList: IisListOfValues;
  SsnNbrsList: IisStringList;
  CLQuery: IevQuery;
  bFound: boolean;
  EeScheduledEDs: IisListOfValues;

  function GetNewKeyValue(ATableName : String) : integer;
  var
    TableName : String;
    DDTable: TddTableClass;
  begin
    TableName := UpperCase(ATableName);
    DDTable := GetddTableClassByName(TableName);
    CheckCondition(Assigned(DDTable), 'Cannot find table class by table name');
    Result := Context.DBAccess.GetGeneratorValue('G_' + TableName, DDTable.GetDBType, 1);
  end;

  procedure CheckEffectiveDateLimits(Dataset: TDataset);
  var
    s, sTmp: String;
    Q: IevQuery;
  begin

    if EeScheduledEDs.Value[Dataset.FieldByName('ee_scheduled_e_ds_nbr').AsString] then
       sTmp := ' and EE_SCHEDULED_E_DS_NBR <> '+ DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').AsString;

    s:= 'select EFFECTIVE_START_DATE, EFFECTIVE_END_DATE ' +
           ' from EE_SCHEDULED_E_DS where {AsOfNow<EE_SCHEDULED_E_DS>} and ' +
           ' CL_E_DS_NBR= :cledsnbr AND EE_NBR= :eenbr ' + sTmp +
           ' order by EFFECTIVE_START_DATE desc ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('cledsnbr', Dataset.FieldByName('CL_E_DS_NBR').AsInteger);
    Q.Params.AddValue('eenbr', Dataset.FieldByName('EE_NBR').AsInteger);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    begin
      if Q.Result.Locate('EFFECTIVE_START_DATE', DataSet.FieldByName('EFFECTIVE_START_DATE').Value, []) then
        raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation)
      else
      begin
        Q.Result.First;
        while not Q.Result.Eof do
        begin
          if ((( DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               ( DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime <= Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
              or
               ((DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                (Q.Result.FieldByName('EFFECTIVE_END_DATE').IsNull)))  then // EXCEPTION WRONG_EFFECTIVE_START_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;
          if (((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               (DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime <= Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
               or
              ((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime >= Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
               (Q.Result.FieldByName('EFFECTIVE_END_DATE').IsNull))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
          end;
          if ((DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime < Q.Result.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
             (((DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime > Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime) and (Q.Result.FieldByName('EFFECTIVE_END_DATE').AsDateTime <> 0)) or
               (DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime = 0))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
          begin
            raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);          
          end;

          Q.Result.Next;
        end;
      end;
    end;
  end;

begin
  sFieldName := '';
  AsOfDatesList := TisListOfValues.Create;
  EeScheduledEDs := TisListOfValues.Create;

  SsnNbrsList := TisStringList.Create;
  // filling list of AsOfDates for each table
  for i := 0 to params.Count-1 do
  begin
    sTableName := params.KeyList.Strings[i];
    iPos := Pos('__', sTableName);
    if iPos > 0 then
    begin
      sFieldName := LowerCase(Copy(sTableName, iPos + 2, 4096));
      if AnsiCompareText(sFieldName, 'EFFECTIVE_DATE') = 0 then
      begin
        sTableName := LowerCase(LeftStr(sTableName, Pred(iPos)));
        AsOfDatesList.AddValue(sTableName, params.Items[i].AsDateTime);
      end;
    end;
  end;

  NbrMap := TStringList.Create;
  try
    dd := EvoDataDictionary;
    dsList := TStringList.Create;
    try
      for i := 0 to params.Count-1 do
      begin
        sTableName := params.KeyList.Strings[i];

        if (Pos('CL_PERSON__CL_PERSON_NBR', UpperCase(sTableName)) > 0) and
            (params.Items[i].AsInteger < 0) and
             (ctx_AccountRights.Functions.GetState('FIELDS_SSN') <> stEnabled) then
              raise EevException.Create('This user does not have permission to add new Employee SSN Number to Database.');

                                 // EE_BANK_ACCOUNT_NUMBER
        if (Pos('EE_DIRECT_DEPOSIT__EE_DIRECT_DEPOSIT_NBR', UpperCase(sTableName)) > 0) and
            (params.Items[i].AsInteger < 0) and
             (ctx_AccountRights.Functions.GetState('FIELDS_EE_BANK_') <> stEnabled) then
              raise EevException.Create('This user does not have permission to add new Employee Bank Account to Database.');

        if Pos('__', sTableName) = 0 then
        begin
          if Assigned(params.Items[i]) then
            sTableCondition := params.Items[i].AsString
          else
            sTableCondition := '';

          if AsOfDatesList.ValueExists(LowerCase(sTableName)) then
            dataSet := GetDataSet(ClientId, sTableName, sTableCondition, AsOfDatesList.Value[LowerCase(sTableName)])
          else
            dataSet := GetDataSet(ClientId, sTableName, sTableCondition);
          dsList.AddObject(LowerCase(sTableName), dataSet);
        end;
      end;

      for i := 0 to params.Count-1 do
      begin
        sTableName := params.KeyList.Strings[i];
        iPos := Pos('__', sTableName);
        if iPos > 0 then
        begin
          sFieldName := LowerCase(Copy(sTableName, iPos + 2, 4096));
          if not ((AnsiCompareText(sFieldName, 'EFFECTIVE_DATE') = 0) or
                  (AnsiCompareText(sFieldName, 'EFFECTIVE_UNTIL') = 0)) then
          begin
            sTableName := LowerCase(LeftStr(sTableName, Pred(iPos)));
            iPos := dsList.IndexOf(sTableName);
            if iPos >= 0 then
            begin
              ds := TevClientDataSet(dsList.Objects[iPos]);
              iPos := Pos('__', sFieldName);
              if (iPos > 0) or (UpperCase(sFieldName) = UpperCase(sTableName + '_nbr')) then
              begin
                if iPos > 0 then
                begin
                  Nbr := StrToInt(StringReplace(Copy(sFieldName, iPos + 2, 4096), 'm', '-', []));
                  sFieldName := LeftStr(sFieldName, Pred(iPos));
                end
                else
                  Nbr := params.Items[i].AsInteger;

                if Nbr < 0 then  // new record
                begin
                  // replace negative numbers by real positive values
                  j := NbrMap.IndexOf(IntToStr(Nbr));
                  if j = - 1 then
                  begin
                    k := NbrMap.IndexOf(UpperCase(sTableName + '_nbr'));
                    if k = -1 then
                    begin
                      ds.Insert;
                      if ds.FieldByName(sTableName + '_nbr').AsInteger < 0 then
                        ds.FieldByName(sTableName + '_nbr').AsInteger := GetNewKeyValue(sTableName);
                      NewNbr := ds.FieldByName(sTableName + '_nbr').AsInteger;
                      NbrMap.AddObject(IntToStr(Nbr), Pointer(NewNbr));
                    end
                    else
                    begin
                      NewNbr := Integer(Pointer(NbrMap.Objects[k]));
                      NbrMap.Delete(k);
                      NbrMap.AddObject(IntToStr(Nbr), Pointer(NewNbr));
                      ds.Edit;
                    end;
                  end
                  else
                  begin
                    NewNbr := Integer(Pointer(NbrMap.Objects[j]));
                    if ds.Locate(sTableName + '_nbr', NewNbr, []) then
                      ds.Edit
                    else
                      raise EevException.Create('Cannot find a new record for update. ' + sTableName + '_nbr = ' + IntToStr(NewNbr));
                  end;
                end
                else  // not new record
                  if ds.Locate(sTableName + '_nbr', Nbr, []) then
                    ds.Edit
                  else
                    raise EevException.Create('Cannot find record for update. ' + sTableName + '_nbr = ' + IntToStr(nbr));
              end
              else
              begin
                ds.Edit;
                if ds.State = dsInsert then
                begin
                  if ds.FieldByName(sTableName + '_nbr').AsInteger < 0 then
                    ds.FieldByName(sTableName + '_nbr').AsInteger := GetNewKeyValue(sTableName);
                  NewNbr := ds.FieldByName(sTableName + '_nbr').AsInteger;
                  NbrMap.AddObject(UpperCase(sTableName + '_nbr') , Pointer(NewNbr));
                end;
              end;

              if ds.State = dsInsert then
              begin
                ddTable := dd.Tables.TableByName(sTableName);
                for j := 0 to Pred(ds.FieldCount) do
                begin
                  sDefaultValue := TddsField(ddTable.Fields.FieldByName(ds.Fields[j].FieldName)).DefaultValue;
                  if sDefaultValue <> '' then
                    ds.Fields[j].AsString := sDefaultValue;
                end;
              end;

              dsField := ds.FieldByName(sFieldName);
              if not Assigned(params.Items[i]) then
                dsField.Clear
              else
                case dsField.DataType of
                  ftString:
                    dsField.Value := params.Items[i].AsString;
                  ftBoolean:
                    dsField.Value := params.Items[i].AsBoolean;
                  ftDateTime, ftDate:
                    if params.Items[i].IsString and (params.Items[i].AsString = '') then
                      dsField.Clear
                    else
                      dsField.Value := params.Items[i].AsDateTime;
                  ftFloat, ftCurrency:
                    begin
                      if params.Items[i].IsString and (params.Items[i].AsString = '') then
                        dsField.Clear
                      else
                        dsField.Value := params.Items[i].AsFloat;
                    end;
                  ftInteger:
                    begin
                      if params.Items[i].IsString and (params.Items[i].AsString = '') then
                        dsField.Clear
                      else
                        dsField.Value := params.Items[i].AsInteger;
                    end;
                  ftBlob, ftMemo:
                  begin
                    if params.Items[i].IsString and (params.Items[i].AsString = '') then
                      dsField.Clear
                    else begin
                      BlobData := TEvDualStreamHolder.Create;
                      s := params.Items[i].AsBase64Str;
                      BlobData.WriteBuffer(s[1], Length(s));
                      BlobData.Position := 0;
                      TBlobField(dsField).LoadFromStream(BlobData.RealStream);
                      BlobData := nil;
                    end;
                  end
                end; // case

              // replace numbers for _nbr fields
              if (Pos('_NBR', UpperCase(sFieldName)) > 0) then
                if dsField.Value <> null then
                  if dsField.Value < 0 then
                  begin
                    j := NbrMap.IndexOf(UpperCase(sFieldName));
                    if j = - 1 then
                    begin
                      j := NbrMap.IndexOf(dsField.Value);
                      if j = - 1 then
                        raise EevException.Create('Cannot map foreign key value. Table: ' + sTableName + ', Field: ' + sFieldName + ', Value: ' + IntToStr(dsField.Value))
                      else
                        dsField.Value := Integer(Pointer(NbrMap.Objects[j]));
                    end
                    else
                     dsField.Value := Integer(Pointer(NbrMap.Objects[j]));
                  end;

              // hash SelfServ password
              if AnsiSameText(sTableName, 'EE') and AnsiSameText(sFieldName, 'SELFSERVE_PASSWORD') then
              begin
                if dsField.AsString <> '' then
                  dsField.AsString := HashPassword( dsField.AsString )
                else
                  dsField.Clear;
              end;

              // filling up list of CL_PERSON_NBR if SSN is changed
              if AnsiSameText(sTableName, 'CL_PERSON') and AnsiSameText(sFieldName, 'SOCIAL_SECURITY_NUMBER') then
                SsnNbrsList.Add(IntToStr(ds['CL_PERSON_NBR']));

              if (ds.State in [dsInsert, dsEdit]) and AnsiSameText(sTableName, 'ee_scheduled_e_ds') then
              begin
                if not EeScheduledEDs.ValueExists(ds.FieldByName('ee_scheduled_e_ds_nbr').AsString) then
                  EeScheduledEDs.AddValue(ds.FieldByName('ee_scheduled_e_ds_nbr').AsString, ds.State = dsEdit);
              end;

              // prevent posting unchanged fields
              if (ds.State = dsInsert) then
                ds.Post
              else
                if dsField.Value = dsField.OldValue then
                  ds.Cancel
                else
                  ds.Post;
            end
// TODO : commented for compatibility. Web client has a bug: it doesn''t send 'CO' table in tables list
{            else
              raise EevException.Create('Dataset''s name not found in list: ' + sTableName); }
          end;
        end;
      end;

      iPos := dsList.IndexOf('ee_scheduled_e_ds');
      if iPos >= 0 then
      begin
        ds := TevClientDataSet(dsList.Objects[iPos]);
        ds.First;
        while not ds.Eof do
        begin
          CheckEffectiveDateLimits(ds);
          ds.Next;
        end;
      end;

      iPos := dsList.IndexOf('co_billing_history');
      if iPos >= 0 then
      begin
        ds := TevClientDataSet(dsList.Objects[iPos]);
        ds.First;
        while(not ds.Eof) do
        begin
          if(ds.State = dsBrowse) then
            ds.Edit;
          if (ds.FieldByName('INVOICE_NUMBER').IsNull ) then
            ds.FieldByName('INVOICE_NUMBER').Value := ctx_DBAccess.GetGeneratorValue('G_NEXT_INVOICE_NUMBER', dbtBureau, 1);
          ds.Post;
          ds.Next;
        end;
      end;

      CoAsOfDate := 0;
      iPos := dsList.IndexOf('co');
      if iPos >= 0 then
      begin
        ds := TevClientDataSet(dsList.Objects[iPos]);

        if (ds.FieldByName('BUSINESS_START_DATE').AsDateTime = 0) or
           (SysTime < ds.FieldByName('BUSINESS_START_DATE').AsDateTime) then
          CoAsOfDate := ds.FieldByName('INITIAL_EFFECTIVE_DATE').AsDateTime;
      end;


      SetLength(aDS, dsList.Count);
      for i := 0 to Pred(dsList.Count) do
      begin
        aDS[i] := TevClientDataSet(dsList.Objects[i]);
        //TevClientDataSet(aDS[i]).CoAsOfDate := CoAsOfDate;
        TevClientDataSet(aDS[i]).AsOfDate := Trunc(CoAsOfDate);
      end;

      // checking SSN, if needed
      if SsnNbrsList.Count > 0 then
        for i := 0 to Length(aDS) do
          if AnsiSameText(aDS[i].Name, 'CL_PERSON') then
          begin
            CLQuery := TevQuery.Create('select BLOCK_INVALID_SSN from CL a where a.CL_NBR=' + IntToStr(ClientId) +
             ' and {AsOfNow<a>} ');
            CheckCondition(CLQuery.Result.RecordCount > 0, 'Client not found');
            if CLQuery.Result['BLOCK_INVALID_SSN'] = 'Y' then
            begin
              for j := 0 to SsnNbrsList.Count - 1 do
              begin
                bFound := aDS[i].Locate('CL_PERSON_NBR', StrToInt(SsnNbrsList[j]), []);
                CheckCondition(bFound, 'Cannot find record in CL_PERSON table in order to check SSN');
                if aDS[i]['EIN_OR_SOCIAL_SECURITY_NUMBER'] = GROUP_BOX_SSN then
                  CheckSSN(aDS[i]['SOCIAL_SECURITY_NUMBER']);
              end;
            end;  
            break;
          end;

      ctx_DataAccess.PostDataSets(aDS);

    finally
      for i := 0 to Pred(dsList.Count) do
        dsList.Objects[i].Free;
      dsList.Free;
    end;

    rpcStruct := TRpcStruct.Create;
    for i := 0 to NbrMap.Count - 1 do
      rpcStruct.AddItem(NbrMap[i], Integer(Pointer(NbrMap.Objects[i])));

  finally
    for i := 0 to Pred(NbrMap.Count) do
      NbrMap.Objects[i] := nil;
    NbrMap.Free;
  end;
  Result := rpcStruct;
end;

{******************************************************************
  evo.legacy.executeMethod
     Executes a function on the Evolution package server
  @param  sessionId      Evo session
  @param  packageNumber  Evolution package number
  @param  functionName   name of the Evolution remote function
  @param  param          Structure which contains a set of input
                         parameters for this method. The order in a set
                         should match the order of method input parameters
                         and types should be in compliance with types
                         of input parameters
  @return struct         Structure filled with method output parameters.
                         The order and the types in a structure will match the
                         order and the types of method output parameters.
******************************************************************}
procedure TevXmlRPCServer.Legacy_ExecuteMethod(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
var
  packageNumber: Integer;
  functionName: String;
  Params: IRpcArray;
  Results: IRpcArray;
  sContextID : String;
begin
// evo.legacy.executeMethod(string sessionId, integer packageNumber, string  functionName, array params)
// Return: array
  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  packageNumber := IRpcParameter(AParams[1]).AsInteger;
  functionName := UpperCase(IRpcParameter(AParams[2]).AsString);
  Params := IRpcParameter(AParams[3]).AsArray;

  APICheckCondition(packageNumber = REMOTABLE_CALC_PACKAGE, WrongPackageNumberException);
  APICheckCondition((functionName = CREATE_CHECK) or (functionName = CREATE_MANUAL_CHECK) or (functionName = CHANGECHECK_STATUS) or
    (functionName = NEXT_PAYROLLDATES) or (functionName = HOLIDAY_DAY), UnsupportedFunctionException);

  Results := ExecuteMethod(functionName, params);
  if not Assigned(Results) then
    Results := TRpcArray.Create;
  AReturn.AddItem(Results)
end;

{******************************************************************
  evo.legacy.executeMethodCalc
     Executes a tax calculator function on the Evolution package server
  @param  sessionId       Evo session
  @param  packageNumber   Evolution package number
  @param  functionName    Name of the Evolution remote function. Allowed:
                          "CalculateTaxes" ; "CalculateChecklines" ; "RefreshEDS"
  @param  coId            Company internal number
  @param  pr_check        Values to fill the temporary pr_check table
  @param  pr_check_lines  Values to fill the temporary pr_check table
  @param  pr_check_states Values to fill the temporary pr_check table
  @param  pr_check_sui    Values to fill the temporary pr_check table
  @param  pr_check_locals Values to fill the temporary pr_check table

  *** For method "CalculateTaxes" only:
  @param disableYTD         If true, disables YTD calculation
  @param disableShortfalls  If true, disable shortfalls calculation
  @param netToGross         If false, performs gross to net calculation base
                            on check information passed in result sets.
                            If true, performs net to gross calculation,
                            creating a check with net wages equal to net parameter
  @param net                If netToGross is true, this parameter defines
                            desirable net wages for this check

  @return Function returns a list of recalculated result sets:
      @retval Item[0]  pr_check            - for "CalculateTaxes" and "CalculateChecklines"
      @retval Item[1]  pr_check_lines      - for all three methods
      @retval Item[2]  pr_check_states     - for "CalculateTaxes" and "CalculateChecklines"
      @retval Item[3]  pr_check_sui        - for "CalculateTaxes" and "CalculateChecklines"
      @retval Item[4]  pr_check_locals     - for "CalculateTaxes" and "CalculateChecklines"
******************************************************************}
procedure TevXmlRPCServer.Legacy_ExecuteMethodCalc(const AParams: IInterfaceList;  const AReturn: IRpcReturn; const AMethodName : String);
var
  sContextID : String;
  functionName: String;
  packageNumber: Integer;
  Params: IRpcArray;
  Results: IRpcArray;
begin
// evo.legacy.executeMethodCalc(string sessionId, integer packageNumber, string  functionName, array params)
// Return: array
  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  packageNumber := IRpcParameter(AParams[1]).AsInteger;
  functionName := UpperCase(IRpcParameter(AParams[2]).AsString);
  Params := IRpcParameter(AParams[3]).AsArray;

  APICheckCondition(packageNumber = REMOTABLE_CALC_PACKAGE, WrongPackageNumberException);
  APICheckCondition((functionName = REFRESH_EDS) or (functionName = CALCULATE_TAXES) or
    (functionName = CALCULATE_CHECKLINES), UnsupportedFunctionException);

  Results := ExecuteMethod(functionName, params);
  if not Assigned(Results) then
    Results := TRpcArray.Create;
  AReturn.AddItem(Results)
end;

function TevXmlRPCServer.ExecuteMethod(const functionName: string;  const param: IRpcArray): IRpcArray;
var
  //outStruct: IRpcStruct;
  outParams: IRpcArray;

  vtype: TFieldType;
  dataSet: TevClientDataSet;
  i: Integer;
  Params, Results: IEvDualStream;
  rpcStruct: IRpcStruct;
  outMsg: string;
begin
  //outStruct := TRpcStruct.Create;
  Params := TisStream.Create;
  outMsg := '';
  for i := 0 to param.Count-1 do
  begin
//    if (param.Items[i].IsStruct and (param.Items[i].AsStruct.KeyList.Strings[0] = 'DATASET')) then
    if (param.Items[i].IsStruct) then
    begin
      Params.WriteByte(System.Byte(ftDataSet));
      dataSet := OldStructToDataSet(param.Items[i].AsStruct);
      if not Assigned(dataSet) then
        APICheckCondition(false, EmptyDatasetException)
      else
        try
          Params.WriteStream(dataSet.GetDataStream(False, False));
        finally
          dataSet.Free;
        end;
    end
    else
    begin
      case param.Items[i].DataType of
      dtString:
        begin
          Params.WriteByte(System.Byte(ftString));
          Params.WriteString(param.Items[i].AsString);
        end;
      dtBoolean:
        begin
          Params.WriteByte(System.Byte(ftBoolean));
          Params.WriteBoolean(param.Items[i].AsBoolean);
        end;
      dtDateTime:
        begin
          Params.WriteByte(System.Byte(ftDate));
          Params.WriteDouble(param.Items[i].AsDateTime);
        end;
      dtFloat:
        begin
          Params.WriteByte(System.Byte(ftFloat));
          Params.WriteDouble(param.Items[i].AsFloat);
        end;
      dtInteger:
        begin
          Params.WriteByte(System.Byte(ftInteger));
          Params.WriteInteger(param.Items[i].AsInteger);
        end;
      else
        APICheckCondition(false, UnknownParamTypeException);
      end; // case
    end;  // if
  end;  // for - param

  if functionName = Create_Check then
  begin
    Results := Context.RemoteMiscRoutines.CreateCheck(Params);
  end
  else if functionName = ChangeCheck_Status then
  begin
    Results := Context.RemoteMiscRoutines.ChangeCheckStatus(Params);
  end
  else if functionName = Create_Manual_Check then
  begin
    Results := Context.RemoteMiscRoutines.CreateManualCheck(Params);
  end
  else if functionName = Next_PayrollDates then
  begin
    Results := Context.RemoteMiscRoutines.NextPayrollDates(Params);
  end
  else if functionName = Holiday_Day then
  begin
    Results := Context.RemoteMiscRoutines.HolidayDay(Params);
  end
  else if functionName = Calculate_Checklines then
  begin
    Results := Context.RemoteMiscRoutines.CalculateChecklines(Params);
  end
  else if functionName = Calculate_Taxes then
  begin
    Results := Context.RemoteMiscRoutines.CalculateTaxes(Params);
  end
  else if functionName = Refresh_EDS then
  begin
    Results := Context.RemoteMiscRoutines.RefreshEDS(Params);
  end
  else
    APICheckCondition(false, UnsupportedFunctionException);

  if Assigned(Results) then
  begin
    outParams := TRpcArray.Create;
    Results.Position := 0;
    while not Results.EOS do
    begin
      vtype := TFieldType(Results.ReadByte);
      if vtype = ftDataSet then
      begin
        dataset := TevClientDataSet.Create(nil);
        try
          dataset.Name := Results.ReadString;
          Results.ReadInteger;
          dataSet.LoadFromUniversalStream(Results);
          //outParams.AddItem(DataSetToStruct(Handle, dataSet, '*='+dataSet.Name)); // ??? - columnMap
          rpcStruct := TRpcStruct.Create;
          rpcStruct.AddItem('DATASET', DataSetToStruct(TevDataset.Create(dataSet), '*='+dataSet.Name)); // ??? - columnMap
          outParams.AddItem(rpcStruct);
        finally
          dataset.Free;
        end;
      end
      else
      begin
        case vtype of
          ftDate:
              outParams.AddItemDateTime(TDateTime(Results.ReadDouble));
          ftString:
            outParams.AddItem(Results.ReadString);
          ftInteger:
            outParams.AddItem(Results.ReadInteger);
        else
          APICheckCondition(false, UnsupportedOutputParamTypeException);
        end;

      end; // if type
    end; // while

  end; // asiigned results

  Result := outParams;
end;

procedure TevXmlRPCServer.GetAPIVersion(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName : String);
begin
// evo.getAPIVersion
// Return: string
  AReturn.AddItem(APPVersion);
end;

function TevXmlRPCServer.TaskNbrToId(const ATaskNbr: String): TisGUID;
var
  TaskInf: IevTaskInfo;
  TaskList: IisListOfValues;
  i: Integer;
  sDomain, sUser : String;
begin
  TaskList := mb_TaskQueue.GetUserTaskInfoList;
  sDomain := Context.UserAccount.Domain;
  sUser := Context.UserAccount.User;

  for i := 0 to TaskList.Count - 1 do
  begin
    TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
    if  not (AnsiSameText(sDomain, TaskInf.GetDomain) and AnsiSameText(sUser, TaskInf.GetUser)) then
      continue;
    if TaskInf.Nbr = StrToInt(ATaskNbr) then
    begin
      result := TaskInf.ID;
      exit;
    end;
  end;
  APICheckCondition(false, CannotConvertNbrToIdException);
end;

function TevXmlRPCServer.TaskIdToNbr(const ATaskId: TisGUID): String;
var
  TaskList: IisListOfValues;
begin
  TaskList := mb_TaskQueue.GetUserTaskInfoList;
  if TaskList.ValueExists(ATaskId) then
    result := IntToStr((IInterface(TaskList.Value[ATaskId]) as IevTaskInfo).Nbr)
  else
    APICheckCondition(false, CannotConvertIdToNbrException);
end;

function TevXmlRPCServer.GetLicenseKeyFromContext(const AContextId: String): IevAPIKey;
var
  Context: IevContext;
  ContextObjFromPool : IContextObjFromPool;
begin
  ContextObjFromPool := FContextPool.AcquireObject(AContextId) as IContextObjFromPool;
  Context := ContextObjFromPool.GetContext;
  APICheckCondition(Assigned(Context), WrongSessionException);
  Result := ContextObjFromPool.GetLicenseKey;
  FContextPool.ReturnObject(ContextObjFromPool as IisObjectFromPool);
end;

function TevXmlRPCServer.GetPort: String;
begin
  Result := IntToStr(FRPCServer.ListenPort);
end;

procedure TevXmlRPCServer.SetPort(const APort: String);
begin
  if FRPCServer.ListenPort <> StrToInt(APort) then
  begin
    FRPCServer.Active := False;
    FRPCServer.ListenPort := StrToInt(APort);
    FRPCServer.Active := True;
  end;
end;

function TevXmlRPCServer.GetRBHost: String;
begin
  Result := Mainboard.TCPClient.Host;
end;

procedure TevXmlRPCServer.SetRBHost(const AValue: String);
begin
  if Mainboard.TCPClient.Host <> AValue then
  begin
    SetActive(False);
    Mainboard.TCPClient.Host := AValue;
    SetActive(True);
  end;
end;

procedure TevXmlRPCServer.CollectStatistics(AMethodName: String);
var
  ContextObjFromPool : IContextObjFromPool;
  APIKey : IevAPIKey;
  BillingDomain, BillingData : IisListOfValues;
  sDomainName : String;
begin
  exit; {todo : uncomment }
  // preparing information
  ContextObjFromPool := FContextPool.AcquireObject(Context.GetID) as IContextObjFromPool;
  try
    APIKey := ContextObjFromPool.GetLicenseKey;
    CheckCondition(Assigned(APIKey), 'API key object not assigned');
    if APIKey.APIKeyType <> apiVendor then
      Exit;
    CheckCondition(APIKey.APIKey <> '', 'API key string not assigned!');
  finally
    FContextPool.ReturnObject(ContextObjFromPool as IisObjectFromPool);
  end;
  CheckCondition(Trim(AMethodName) <> '', 'MethodName cannot be empty');
  sDomainName := Context.DomainInfo.DomainName;

  // saving statistics, it is grouped by domains
  FBillingLog.Lock;
  try
    if FBillingLog.ValueExists(sDomainName) then
      BillingDomain := IInterface(FBillingLog.Value[sDomainName]) as IisListOfValues
    else
    begin
      BillingDomain := TisListOfValues.Create;
      FBillingLog.AddValue(sDomainName, BillingDomain);
    end;
    BillingData := TisListOfValues.Create;
    BillingData.AddValue(sBillingAPIKey, APIKey.APIKey);
    BillingData.AddValue(sBillingUserName, LowerCase(Context.UserAccount.User));
    BillingData.AddValue(sBillingDate, SysTime);
    BillingData.AddValue(sBillingMethod, LowerCase(AMethodName));
    BillingData.AddValue(sBillingDomainName, sDomainName);
    BillingData.AddValue(sBillingAppName, APIKey.ApplicationName);
    BillingData.AddValue(sBillingVendorName, APIKey.VendorName);

    BillingDomain.AddValue(IntToStr(BillingDomain.Count), BillingData);
  finally
    FBillingLog.Unlock;
  end;
end;

procedure TevXmlRPCServer.SaveUnsentBillingInfo;
var
  UnsentDataStream : IevDualStream;
  sFileName : String;
begin
  try
    FBillingLog.Lock;
    try
      if FBillingLog.Count = 0 then
        Exit;
      sFileName := AppDir + 'SavedData.dat';
      if FileExists(sFileName) then
        DeleteFile(sFileName);
      UnsentDataStream := TevDualStreamHolder.CreateFromNewFile(sFileName);
      FBillingLog.WriteToStream(UnsentDataStream);
      mb_AppConfiguration.AsBoolean['APIAdapter\SavedData'] := true;
      FBillingLog.Clear;
    finally
      FBillingLog.Unlock;
    end;
  except
    on E : Exception do
      mb_LogFile.AddContextEvent(etError, APIEventClass, 'Exception in SaveUnsentBillingInfo procedure', MakeFullError(E));
  end;
end;

procedure TevXmlRPCServer.LoadUnsentBillingInfo;
var
  UnsentDataStream : IevDualStream;
  sFileName : String;
begin
  exit; {todo : uncomment }
  try
    if not mb_AppConfiguration.AsBoolean['APIAdapter\SavedData'] then
      Exit;

    CheckCondition(FBillingLog.Count = 0, 'Billing list is not empty. Count: ' + IntToStr(FBillingLog.Count));

    sFileName := AppDir + 'SavedData.dat';
    CheckCondition(FileExists(sFileName), 'File "' + sFileName +'" does not exist');
    UnsentDataStream := TevDualStreamHolder.CreateFromFile(sFileName);
    UnsentDataStream.Position := 0;
    FBillingLog.Lock;
    try
      FBillingLog.ReadFromStream(UnsentDataStream);
      UnsentDataStream := nil; // to free file
      mb_AppConfiguration.AsBoolean['APIAdapter\SavedData'] := false;
      DeleteFile(sFileName);
    finally
      FBillingLog.Unlock;
    end;
  except
    on E : Exception do
    begin
      mb_LogFile.AddContextEvent(etError, APIEventClass, 'Exception in LoadUnsentBillingInfo procedure', MakeFullError(E));
      mb_AppConfiguration.AsBoolean['APIAdapter\SavedData'] := false;
    end;
  end;
end;

{todo : uncomment }
{function TevXmlRPCServer.SendBillingInfo: boolean;
var
 i : integer;
 BillingForDomain : IisListOfValues;
 sDomainName : String;
begin
  result := true;

  FBillingLog.Lock;
  try
    // loop by domains
    for i := FBillingLog.Count - 1 downto 0 do
    begin
      BillingForDomain := IInterface(FBillingLog.Values[i].Value) as IisListOfValues;
      sDomainName := FBillingLog.Values[i].Name;
      try
        Mainboard.ContextManager.StoreThreadContext;
        try
          Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomainName), '');
          Context.RemoteMiscRoutines.SaveAPIBilling(BillingForDomain);
          FBillingLog.DeleteValue(sDomainName);
        finally
          Mainboard.ContextManager.RestoreThreadContext;
        end;
      except
        on E:Exception do
        begin
          result := false;
          mb_LogFile.AddContextEvent(etError, APIEventClass, 'Cannot save billing info for "' + sDomainName + '" domain.',
            MakeFullError(E));
        end;
      end;  // try except
    end;
  finally
    FBillingLog.Unlock;
  end;
end;
}

{todo : uncomment }
{procedure TevXmlRPCServer.BillingInfoSaverWatchDog(const Params: TTaskParamList);
const
  MaxAmount = 1000;
var
  bNotSendByAmount : boolean;  // this flag allows one sending by number of rows attempt per timeout interval (15 min)
  MinutesCount : integer;
  BillingRows : integer;
begin
  bNotSendByAmount := false;
  MinutesCount := 0;
  repeat
    try
      if MinutesCount >= 15 then 
      begin
        // saving by time
        bNotSendByAmount := false;
        MinutesCount := 0;
        SendBillingInfo;
      end
      else
        if not bNotSendByAmount then
        begin
          FBillingLog.Lock;
          try
            BillingRows := FBillingLog.Count;
          finally
            FBillingLog.Unlock;
          end;
          if BillingRows > MaxAmount then
          begin
            bNotSendByAmount := not SendBillingInfo;
            if not bNotSendByAmount then
              MinutesCount := 0;
          end;
        end;
      except
        on E:Exception do
          mb_LogFile.AddContextEvent(etError, APIEventClass, 'Exception in API billing watchdog',
            MakeFullError(E));
      end;
    Snooze(60000);
    Inc(MinutesCount);
  until CurrentThreadTaskTerminated;
end;
}

function TevXmlRPCServer.FindAPIKey(const AAPIKey: TisGUID): IevAPIKey;
const
  APIKey_AppName = 'APIKeyApplicationName';
  APIKey_VendorCustomNbr = 'APIKeyVendorCustomNbr';
  APIKey_VendorName = 'APIKeyVendorName';
var
  Key: IisListOfValues;

  // {5E465AFC-D0A1-424E-B21B-4351ECD6BFFF}
  function isItWebClientKey(AAPIKey: TisGUID): boolean;
  var
    tmp1, tmp2, tmp3 : String;
  begin
    tmp1 := '5E465AFC';
    tmp2 := '4351ECD6BFFF';
    tmp3 := '{' + tmp1 + '-' + 'D0A1' + '-' + '424E' + '-' + 'B21B' +'-';
    Result := (tmp3 + tmp2 + '}') = AAPIKey;
  end;

  // {83DF9064-A78C-414F-9FF9-2F4A5F8763EC}
  function isItSSKey(AAPIKey: TisGUID): boolean;
  var
    tmp1, tmp2, tmp3 : String;
  begin
    tmp1 := '83DF9064';
    tmp2 := '2F4A5F8763EC';
    tmp3 := '{' + tmp1 + '-' + 'A78C' + '-' + '414F' + '-' + '9FF9' + '-';
    Result := (tmp3 + tmp2 + '}') = AAPIKey;
  end;

begin
  Result := nil;

  if isItWebClientKey(AAPIKey) then
  begin
    Result := TevAPIKey.Create;
    Result.APIKey := 'WebClient';
    Result.APIKeyType := apiWebClient;
    Result.ApplicationName := 'Web Client';
    Result.VendorCustomNbr := 0;
    Result.VendorName := 'iSystems LLC';
    exit;
  end;

  if isItSSKey(AAPIKey) then
  begin
    Result := TevAPIKey.Create;
    Result.APIKey := 'SelfServe';
    Result.APIKeyType := apiSelfServe;
    Result.ApplicationName := 'Self Serve';
    Result.VendorCustomNbr := 0;
    Result.VendorName := 'iSystems LLC';
    exit;
  end;

  Key := Context.License.FindAPIKeyByName(AAPIKey);
  if Assigned(Key) then
  begin
    Result := TevAPIKey.Create;
    Result.APIKey := AAPIKey;
    Result.APIKeyType := apiVendor;
    Result.ApplicationName := Key.Value[APIKey_AppName];
    Result.VendorCustomNbr := Key.Value[APIKey_VendorCustomNbr];
    Result.VendorName := Key.Value[APIKey_VendorName];
  end;
end;


procedure TevXmlRPCServer.SB_ReadCompanyStorage(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iCoNbr: Integer;
  sTag: String;
  rpcArray: IRPCArray;
  rpcStruct: IRPCStruct;
  StorageTable: TevClientDataSet;
  bFound: boolean;
  ListFromBlob: IisListOfValues;
  tmpStream: IevDualStream;
  MessageObject: IEvESSMessage;
  LinkObject: IEvESSLink;
  EDConfigurationobject: IEvESSEDConfiguration;
  i: integer;
begin
  // evo.sb.readCompanyStorage(string sessionId, integer CoNbr, string Tag)
  // Return: array of structures
  // structures could be two types and has such fields:
  // 1. Message: N - Name (String), M - Message (String)
  // 2. Link: N - Name (String), T - Type (String), U - URL (String)

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iCoNbr := IRpcParameter(AParams[1]).AsInteger;
  sTag := IRpcParameter(AParams[2]).AsString;

  APICheckCondition((sTag = 'ESS_LINK') or (sTag = 'ESS_MSG') or (sTag = 'ESS_CONF') or (sTag = 'WC_QECOL'), UnknownCoStorageTag);

  SetCurrentContext(sContextID);

  rpcArray := TRPCArray.Create;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('CO_STORAGE'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired('CO_NBR=' + IntToStr(iCoNbr));

  bFound := StorageTable.Locate('CO_NBR; TAG', VarArrayOf([iCoNbr, sTag]), [loCaseInsensitive]);
  if bFound then
  begin
    tmpStream := StorageTable.GetBlobData('STORAGE_DATA');
    if Assigned(tmpStream) then
    begin
      ListFromBlob := TisListOfValues.Create;
      ListFromBlob.ReadFromStream(tmpStream);
      for i := 0 to ListFromBlob.Count - 1 do
      begin
        rpcStruct := TRPCStruct.Create;
        CheckCondition(sTag <> 'ESS_CONF', 'API Adapter does not support ESS_CONF now');
        if (sTag = 'ESS_MSG') then
        begin
          MessageObject := IInterface(ListFromBlob.Values[i].Value) as IEvESSMessage;
          rpcStruct.AddItem('N', MessageObject.GetName);
          rpcStruct.AddItem('M', MessageObject.GetMessage);
        end
        else if (sTag = 'ESS_LINK') then
        begin
          LinkObject := IInterface(ListFromBlob.Values[i].Value) as IEvESSLink;
          rpcStruct.AddItem('N', LinkObject.GetName);
          rpcStruct.AddItem('T', LinkObject.GetType);
          rpcStruct.AddItem('U', LinkObject.GetURL);
        end
        else if (sTag = 'WC_QECOL') then
        begin
          EDConfigurationObject := IInterface(ListFromBlob.Values[i].Value) as IEvESSEDConfiguration;
          rpcStruct.AddItem('C', EDConfigurationobject.GetCode);
          rpcStruct.AddItem('T', EDConfigurationobject.GetType);
          rpcStruct.AddItem('P', EDConfigurationobject.GetPosition);
        end
        else
          raise Exception.Create('Unsupported tag in ReadCompanyStorage: ' + sTag);

        rpcArray.AddItem(rpcStruct);
      end; // for i
    end; //if Assigned(tmpStream)
  end;  // if bFound

  AReturn.AddItem(rpcArray);
end;

procedure TevXmlRPCServer.SB_WriteCompanyStorage(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iCoNbr: Integer;
  sTag: String;
  rpcArray: IRPCArray;
  rpcStruct: IRPCStruct;
  StorageTable: TevClientDataSet;
  bFound: boolean;
  BlobField: TBlobField;
  ListForBlob: IisListOfValues;
  tmpStream: IevDualStream;
  MessageObject: IEvESSMessage;
  LinkObject: IEvESSLink;
  EDConfigurationObject: IEvESSEDConfiguration;
  i: integer;
begin
// evo.sb.writeCompanyStorage(string sessionId, integer CoNbr, string Tag, array of struct Data)
// Return: nothing

  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iCoNbr := IRpcParameter(AParams[1]).AsInteger;
  sTag := IRpcParameter(AParams[2]).AsString;
  rpcArray := IRpcParameter(AParams[3]).AsArray;

  APICheckCondition((sTag = 'ESS_LINK') or (sTag = 'ESS_MSG') or (sTag = 'ESS_CONF') or (sTag = 'WC_QECOL'), UnknownCoStorageTag);
  CheckCondition(sTag <> 'ESS_CONF', 'Tag ESS_CONF is not supported now');

  SetCurrentContext(sContextID);

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('CO_STORAGE'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;
  StorageTable.DataRequired('CO_NBR=' + IntToStr(iCoNbr));

  bFound := StorageTable.Locate('CO_NBR; TAG', VarArrayOf([iCoNbr, sTag]), [loCaseInsensitive]);
  if not bFound then
  begin
    StorageTable.Append;
    StorageTable['CO_NBR'] := iCoNbr;
    StorageTable['TAG'] := sTag;
  end
  else
    StorageTable.Edit;

  ListForBlob := TisListOfValues.Create;
  for i := 0 to rpcArray.Count - 1 do
  begin
    rpcStruct := rpcArray.Items[i].AsStruct;

    if sTag = 'ESS_MSG' then
    begin
      MessageObject := TEvESSMessage.Create;
      CheckCondition(rpcStruct.KeyExists('N'), 'Wrong structure of input data. Field "N" is missed');
      CheckCondition(rpcStruct.KeyExists('M'), 'Wrong structure of input data. Field "M" is missed');
      MessageObject.SetName(rpcStruct.Keys['N'].AsString);
      MessageObject.SetMessage(rpcStruct.Keys['M'].AsString);
      ListForBlob.AddValue(IntToStr(ListForBlob.Count), MessageObject);
    end
    else if sTag = 'ESS_LINK' then
    begin
      LinkObject := TEvESSLink.Create;
      CheckCondition(rpcStruct.KeyExists('N'), 'Wrong structure of input data. Field "N" is missed');
      CheckCondition(rpcStruct.KeyExists('T'), 'Wrong structure of input data. Field "T" is missed');
      CheckCondition(rpcStruct.KeyExists('U'), 'Wrong structure of input data. Field "U" is missed');
      LinkObject.SetName(rpcStruct.Keys['N'].AsString);
      LinkObject.SetType(rpcStruct.Keys['T'].AsString);
      LinkObject.SetURL(rpcStruct.Keys['U'].AsString);
      ListForBlob.AddValue(IntToStr(ListForBlob.Count), LinkObject);
    end
    else if sTag = 'WC_QECOL' then
    begin
      EDConfigurationObject := TEvESSEDConfiguration.Create;
      CheckCondition(rpcStruct.KeyExists('C'), 'Wrong structure of input data. Field "C" is missed');
      CheckCondition(rpcStruct.KeyExists('T'), 'Wrong structure of input data. Field "T" is missed');
      CheckCondition(rpcStruct.KeyExists('P'), 'Wrong structure of input data. Field "P" is missed');
      EDConfigurationObject.SetCode(rpcStruct.Keys['C'].AsInteger);
      EDConfigurationObject.SetType(rpcStruct.Keys['T'].AsString);
      EDConfigurationObject.SetPosition(rpcStruct.Keys['P'].AsInteger);
      ListForBlob.AddValue(IntToStr(ListForBlob.Count), EDConfigurationObject);
    end
    else
      raise Exception.Create('Unsupported tag in WriteCompanyStorage: ' + sTag);
  end;  // for i

  tmpStream := TevDualStreamHolder.CreateInMemory;
  ListForBlob.WriteToStream(tmpStream);
  BlobField := TBlobField(StorageTable.FieldByName('STORAGE_DATA'));
  BlobField.LoadFromStream(tmpStream.RealStream);
  StorageTable.Post;

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    on E:Exception do
    begin
      // rollback will be here in Manchester
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;
end;

procedure TevXmlRPCServer.EE_CreateEEChangeRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iEeNbr: Integer;
  sType: String;
  params: IRPCStruct;
  Request: IevEEChangeRequest;
begin
  // evo.ee.createEEChangeRequest(string sessionId, integer EeNbr, string Type, map RequestData)
  // Return: nothing
  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEeNbr := IRpcParameter(AParams[1]).AsInteger;
  sType := IRpcParameter(AParams[2]).AsString;
  params := IRpcParameter(AParams[3]).AsStruct;

  APICheckCondition((sType = 'H'), UnknownEEChangeRequestType);

  SetCurrentContext(sContextID);

  Request := TevEEChangeRequest.Create(iEeNbr);
  Request.EEChangeRequestType := EEChangeRequestStringToType(sType);
  Request.RequestData := ParamsToDataChangePacket(params);

  Context.RemoteMiscRoutines.CreateEEChangeRequest(Request);
end;

procedure TevXmlRPCServer.EE_GetEEChangeRequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iEeNbr: Integer;
  sType: String;
  rpcArray: IRpcArray;
  RequestsType: TevEEChangeRequestType;
  RequestsList: IisInterfaceList;
begin
  // evo.ee.getEEChangeRequestList(string sessionId, integer Eenbr, String Type)
  // Return: array of data
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEeNbr := IRpcParameter(AParams[1]).AsInteger;
  sType := IRpcParameter(AParams[2]).AsString;

  APICheckCondition((sType = 'H'), UnknownEEChangeRequestType);
  RequestsType := EEChangeRequestStringToType(sType);

  SetCurrentContext(sContextID);

  RequestsList := Context.RemoteMiscRoutines.GetEEChangeRequestList(iEeNbr, RequestsType);

  rpcArray := TRpcArray.Create;
  EEChangeRequestsToRpcArray(RequestsList, rpcArray);

  AReturn.AddItem(rpcArray);
end;

procedure TevXmlRPCServer.EE_GetMgrChangeRequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iEeNbr: Integer;
  sType: String;
  rpcArray: IRpcArray;
  RequestsType: TevEEChangeRequestType;
  RequestsList: IisInterfaceList;
begin
  // evo.ee.getMgrChangeRequestList(string sessionId, integer MgrEenbr, String Type)
  // Return: array of data
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEeNbr := IRpcParameter(AParams[1]).AsInteger;
  sType := IRpcParameter(AParams[2]).AsString;

  APICheckCondition((sType = 'H'), UnknownEEChangeRequestType);
  RequestsType := EEChangeRequestStringToType(sType);

  SetCurrentContext(sContextID);

  RequestsList := Context.RemoteMiscRoutines.GetEEChangeRequestListForMgr(iEeNbr, RequestsType);

  rpcArray := TRpcArray.Create;
  EEChangeRequestsToRpcArray(RequestsList, rpcArray);
  AReturn.AddItem(rpcArray);
end;

procedure TevXmlRPCServer.EE_ProcessChangeRequests(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  bOperation: boolean;
  params, errors: IRpcArray;
  RequestsNbrs, ErrorsList, OldFormatRequestsList: IisStringList;
  i: integer;
  iErrorRecNbr: integer;
begin
  // evo.ee.processChangeRequest(string sessionId, array of EEChangeRequestNbr, boolean Operation)
  // Return: array of EEChangeRequestNbr which was processed with errors
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  params := IRpcParameter(AParams[1]).AsArray;
  bOperation := IRpcParameter(AParams[2]).AsBoolean;

  SetCurrentContext(sContextID);

  RequestsNbrs := TisStringList.Create;
  for i := 0 to params.Count - 1 do
    RequestsNbrs.Add(IntToStr(Params.Items[i].AsInteger));

  ErrorsList := Context.RemoteMiscRoutines.ProcessEEChangeRequests(RequestsNbrs, bOperation);

  errors := TRPCArray.Create;
  OldFormatRequestsList := TisStringList.Create;
  for i := 0 to ErrorsList.Count - 1 do
  begin
    iErrorRecNbr := StrToInt(ErrorsList[i]);
    if iErrorRecNbr < 0  then
      OldFormatRequestsList.Add(intToStr(Abs(iErrorRecNbr)))
    else
      errors.AddItem(iErrorRecNbr);
  end;
  ErrorsList.Clear;

  //processing records in old format
  if OldFormatRequestsList.Count > 0 then
    ErrorsList := ProcessOldFormatEERequests(OldFormatRequestsList);

  // adding more errors if any happend
  for i := 0 to ErrorsList.Count - 1 do
    errors.AddItem(StrToInt(ErrorsList[i]));

  AReturn.AddItem(errors);
end;

procedure TevXmlRPCServer.EE_SaveTORequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  sOrigin: String;
  sNotes: String;
  iEeNbr, iTypeOffTypeNbr: Integer;
  RequestArray: IRpcArray;
  DetailData: IRPCStruct;
  Requests: IisInterfaceList;
  Request: IEvTOARequest;
  dDate: TdateTime;
  iNbr: integer;
  iHours: Integer;
  iMinutes: integer;
  bItIsCreation: boolean;
  i: integer;
begin
  // evo.ee.saveTORequest(string sessionId, string origin, integer EeNbr, integer TymeOffTypeNbr, string Notes, Array requestArray (it's array of structures))
  // Return: True if successfully saved
  APICheckCondition(AParams.Count >= 6, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sOrigin := IRpcParameter(AParams[1]).AsString;
  iEeNbr := IRpcParameter(AParams[2]).AsInteger;
  iTypeOffTypeNbr := IRpcParameter(AParams[3]).AsInteger;
  sNotes := IRpcParameter(AParams[4]).AsString;
  RequestArray := IRpcParameter(AParams[5]).AsArray;

  SetCurrentContext(sContextID);

  // checking origin
  CheckCondition((sOrigin = 'E') or (sOrigin = 'M'), 'Unknown origin:' + sOrigin);

  // checking request array for consistency and filling requests
  CheckCondition(RequestArray.Count > 0, 'Request is empty');
  bItIsCreation := false;
  Requests := TisInterfaceList.Create;
  for i := 0 to RequestArray.Count - 1 do
  begin
    DetailData := RequestArray.Items[i].AsStruct;
    iNbr := DetailData.Keys['N'].AsInteger;
    dDate := DetailData.Keys['D'].AsDateTime;
    iHours := DetailData.Keys['H'].AsInteger;
    if DetailData.KeyExists('M') then
      iMinutes := DetailData.Keys['M'].AsInteger
    else
      iMinutes := 0;
    if (i = 0) and (iNbr < 0) then
      bItIsCreation := true;
    if i > 0 then
    begin
      if (iNbr >= 0) and bItIsCreation then
        raise Exception.Create('Data inconsistency: you cannot insert and edit records at the same call');
      if (iNbr < 0) and not bItIsCreation then
        raise Exception.Create('Data inconsistency: you cannot insert and edit records at the same call');
    end;
    Request := TEvTOARequest.Create(iEeNbr, iTypeOffTypeNbr);
    Request.EETimeOffAccrualOperNbr := iNbr;
    Request.AccrualDate := dDate;
    Request.Hours := iHours;
    Request.Minutes := iMinutes;
    Request.Notes := sNotes;
    Request.Status := EE_TOA_OPER_CODE_REQUEST_PENDING;
    Requests.Add(Request);
  end;

  if bItIsCreation then
    Context.RemoteMiscRoutines.AddTOARequests(Requests)
  else
    Context.RemoteMiscRoutines.UpdateTOARequests(Requests);

  AReturn.AddItem(True);
end;


procedure TevXmlRPCServer.EE_GetMgrTORequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iMgrEeNbr: Integer;
  Requests: IisInterfaceList;
  RequestsArray: IRpcArray;
begin
  // evo.ee.getMgrTORequestList(string sessionId, integer MgrEenbr)
  // Return: array of data

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iMgrEeNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Requests := Context.RemoteMiscRoutines.GetTOARequestsForMgr(iMgrEENbr);
  RequestsArray := TOARequestToRpcStruct(Requests);

  AReturn.AddItem(RequestsArray);
end;


procedure TevXmlRPCServer.EE_GetEETORequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iEeNbr: Integer;
  Requests: IisInterfaceList;
  RequestsArray: IRpcArray;
begin
  // evo.ee.getEETORequestList(string sessionId, integer Eenbr)
  // Return: array of structures
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEeNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Requests := Context.RemoteMiscRoutines.GetTOARequestsForEE(iEENbr);
  RequestsArray := TOARequestToRpcStruct(Requests);

  AReturn.AddItem(RequestsArray);
end;


procedure TevXmlRPCServer.EE_ProcessTORequests(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  Requests: IRpcArray;
  Request: IRpcStruct;
  sNotes: String;
  Days: IRpcArray;
  Operation: TevTOAOperation;
  EETimeOffAccrualOperNbrs: IisStringList;
  i, j: integer;
begin
  // evo.ee.processTORequests(string sessionId, boolean Operation, array RequestArray)
  // Return: True if successfully processed
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;

  if IRpcParameter(AParams[1]).AsBoolean then
    Operation := toaApprove
  else
    Operation := toaDeny;
  Requests := IRpcParameter(AParams[2]).AsArray;

  SetCurrentContext(sContextID);

  for i := 0 to Requests.Count - 1 do
  begin
    sNotes := '';
    Request := Requests.Items[i].AsStruct;
    Days := Request.Keys['L'].AsArray;
    if Request.KeyExists('C') then
      sNotes := Trim(Request.Keys['C'].AsString);
    EETimeOffAccrualOperNbrs := TisStringList.Create;
    for j := 0 to Days.Count - 1 do
      EETimeOffAccrualOperNbrs.Add(IntToStr(Days.Items[j].AsInteger));
    CheckCondition(Days.Count > 0, 'Cannot process empty request');
    Context.RemoteMiscRoutines.ProcessTOARequests(Operation, sNotes, EETimeOffAccrualOperNbrs);
  end;

  AReturn.AddItem(true);
end;

function TevXmlRPCServer.RestoreStructureFromXML(const AXML: String): IRpcStruct;
var
  sXML: String;
  Parser : IRPCServerParser;
  List : IInterfaceList;
begin
  Parser := TRpcServerParser.Create;
  sXML := '<params><param>' + AXML + '</param></params>';
  Parser.Parse(sXML);
  List := Parser.GetParameters;
  if List.Count = 0 then
    raise Exception.Create('Wrong structure of source for parameter (struct not found): ' + #13#10 + AXML);
  Result := (List[0] as IRPCParameter).AsStruct;
  if (not Assigned(Result)) then
    raise Exception.Create('Wrong structure of source for parameter (struct not found): ' + #13#10 + AXML);
end;

procedure TevXmlRPCServer.TaskQueue_EvoXImport(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  rpcInputFiles: IRpcArray;
  rpcInputFile: IRpcStruct;
  sXMLMap: String;
  sDataErrors: String;
  sFileName: String;
  sTaskID: TisGUID;
  Package: IevExchangePackage;
  InputDataset: IevDataSet;
  InputDatasets: IisListOfValues;
  tmpStream: IevDualStream;
  dataReader: IEvExchangeDataReader;
  ErrorsList : IEvExchangeResultLog;
  ResultStruct: IrpcStruct;
  Task: IevEvoXImportTask;
  TaskInfo: IevTaskInfo;

  procedure MapFileWrapper;
  var
    MapFile: IEvExchangeMapFileExt;
    i: integer;
  begin
    MapFile := TEvExchangeMapFileExt.Create;
    MapFile.LoadFromXMLString(sXMLMap);

    Package := Context.EvolutionExchange.GetEvExchangePackage(MapFile.GetPackageName);
    CheckCondition(Assigned(Package), 'Cannot find package with name: ' + MapFile.GetPackageName);
    Context.EvolutionExchange.UpgradeMapFileToLastPackageVer(MapFile);
    Context.EvolutionExchange.PreparePackageGroupRecords(MapFile, Package);
    Context.EvolutionExchange.UpgradeDataTypesInMapFile(MapFile, Package);

    dataReader := Context.EvolutionExchange.GetDataReader(MapFile.GetDataSourceName);
    CheckCondition(Assigned(dataReader), 'Cannot find data reader with name: ' + MapFile.GetDataSourceName);
    dataReader.SetDataReaderOptions(MapFile.GetDataSourceOptions);

    ResultStruct := TrpcStruct.Create;

    sDataErrors := '';
    ErrorsList := TEvExchangeResultLog.Create;
    InputDatasets := TisListOfValues.Create;
    for i := 0 to rpcInputFiles.Count - 1 do begin
      rpcInputFile := rpcInputFiles[i].AsStruct;
      sFileName := rpcInputFile['fileName'].AsString;
      tmpStream := TevDualStreamHolder.Create;
      rpcInputFile['fileData'].Base64StrSaveToStream(tmpStream.RealStream);
      tmpStream.Position := 0;

      ErrorsList.Clear;
      InputDataSet := dataReader.ReadDataFromStream(tmpStream, MapFile, ErrorsList);

      if ErrorsList.Count > 0 then
        sDataErrors := sDataErrors + #13#10 + 'File name: ' + sFileName + #13#10 + ErrorsList.Text
      else
        InputDatasets.AddValue(sFileName, InputDataSet);
    end; // for

    if sDataErrors <> '' then
    begin
      ResultStruct.AddItem('taskNbr', '-1');
      ResultStruct.AddItemBase64Str('errorLog', sDataErrors);
      AReturn.AddItem(ResultStruct);
    end
    else
    begin
      Task := mb_TaskQueue.CreateTask(QUEUE_TASK_EVOX_IMPORT, True) as IevEvoXImportTask;
      Task.SetInputDataSets(InputDatasets);
      Task.SetMapFile(MapFile);
      Task.SetPackage(Package);
      Task.Caption := 'EvoX Import task created by API. ' + IntToStr(InputDataSets.Count) + ' file(s) to import';
      mb_TaskQueue.AddTask(Task);
      TaskInfo := mb_TaskQueue.GetTaskInfo(Task.GetId);
      sTaskId := IntToStr(TaskInfo.Nbr);

      ResultStruct.AddItem('taskNbr', sTaskId);
      ResultStruct.AddItemBase64Str('errorLog', '');
      AReturn.AddItem(ResultStruct);
     end;
  end;
begin
//  evo.taskQueue.evoXImport(
//     string sessionId,
//     array of struct with two fields:
//     {
//       - string fileName (MUST BE UNIQUE)
//       - base64 fileData
//     }
//     , base64str XMLMap)
//  Return: struct with two fields:
//    1. string taskNbr - it will be '-1' if error
//    2. base64str errorLog - it will be empty on success, or it will contain errors returned by data reader

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  rpcInputFiles := IRpcParameter(AParams[1]).AsArray;
  sXMLMap := IRpcParameter(AParams[2]).AsBase64Str;

  CheckCondition(rpcInputFiles.Count > 0, 'No data files provided');

  SetCurrentContext(sContextID);

  try
    CoInitialize(nil);
    MapFileWrapper;
  finally
    CoUninitialize;
  end;
end;

procedure TevXmlRPCServer.TaskQueue_GetEvoXImportResults(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID : String;
  sTaskNbr : String;
  TskInf: IevTaskInfo;
  Tsk: IevTask;
  tmpArray: IRPCArray;
  tmpStruct : IRpcStruct;
  tmpId : TisGUID;
  ListOfRes, Res: IisListOfValues;
  i: integer;
  inputRowsAmount, importedRowsAmount, messagesAmount: integer;
  errorsList: String;
begin
//  evo.taskQueue.getEvoXImportResults(string sessionId, string taskNbr)
//  Return: array of structs
//    {
//    integer inputRowsAmount,
//    integer importedRowsAmount,
//    base64str errorsList,
//    integer errorsAmount  - it is ALL MESSAGES amount NOT only errors!!!
//    integer realErrorsAmount
//    integer warningsAmount
//    integer informationAmount
//    }
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  sTaskNbr := IRpcParameter(AParams[1]).AsString;
  tmpId := TaskNbrToId(sTaskNbr);
  TskInf := mb_TaskQueue.GetTaskInfo(tmpId);

  APICheckCondition(Assigned(TskInf), QueueTaskNotFoundException);
  APICheckCondition(TskInf.State = tsFinished, QueueTaskNotFinishedException);

  tmpArray := TRpcArray.Create;

  Tsk := mb_TaskQueue.GetTask(tmpId);
  if Supports(Tsk, IevEvoXImportTask) then
  begin
    ListOfRes := (Tsk as IevEvoXImportTask).Results;
    for i := 0 to ListOfRes.Count - 1 do
    begin
      Res := IInterface(ListOfRes.Values[i].Value) as IisListOfValues;
      tmpStruct := TRPCStruct.Create;

      inputRowsAmount := Res.Value[EvoXInputRowsAmount];
      importedRowsAmount := Res.Value[EvoXImportedRowsAmount];
      messagesAmount := Res.Value[EvoXAllMessagesAmount];
      tmpStruct.AddItem('inputRowsAmount', inputRowsAmount);
      tmpStruct.AddItem('importedRowsAmount', importedRowsAmount);
      tmpStruct.AddItem('errorsAmount', messagesAmount);

      if Res.ValueExists(EvoXStringErrorsList) then
        errorsList := (IInterface(Res.Value[EvoXStringErrorsList]) as IisStringList).Text
      else
      begin
        errorsList := (IInterface(Res.Value[EvoXExtErrorsList]) as IEvExchangeResultLog).Text;
        tmpStruct.AddItem('realErrorsAmount', (IInterface(Res.Value[EvoXExtErrorsList]) as IEvExchangeResultLog).GetAmountOfErrors);
        tmpStruct.AddItem('warningsAmount', (IInterface(Res.Value[EvoXExtErrorsList]) as IEvExchangeResultLog).GetAmountOfWarnings);
        tmpStruct.AddItem('informationAmount', (IInterface(Res.Value[EvoXExtErrorsList]) as IEvExchangeResultLog).GetAmountOfinformation);
      end;
      tmpStruct.AddItemBase64Str('errorsList', errorsList);

      tmpArray.AddItem(tmpStruct);
    end;
  end
  else
    APICheckCondition(false, ResultWrongStructureException);

  AReturn.AddItem(tmpArray);
end;

// commented until it will be AppNext
procedure TevXmlRPCServer.SB_GetEmail(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId: String;
  CoNbr: integer;
  sEMail: String;
begin
// evo.sb.getEMail(string sessionId, integer CoNbr)
// Return: string
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  CoNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  sEMail := Context.RemoteMiscRoutines.GetSBEmail(CoNbr);
  AReturn.AddItem(sEMail);
end;

procedure TevXmlRPCServer.Security_GetAccountRights(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextId, S: String;
  APIKey: IEvAPIKey;
  SecurityElements: IevSecElements;
  rpcArray: IRpcArray;
  ModulesList, APIKeys, KeyInfo: IisListOfValues;
  ModuleInfoStruct, APIKeysStruct: IRpcStruct;
  i: integer;

  procedure SecElementsToArray(const AElementsType: String; const ASecurityElements: IevSecElements; const AArray: IRpcArray);
  var
    i: integer;
    rpcStruct: IRpcStruct;
    StateInfo: IevSecStateInfo;
  begin
    for i := 0 to ASecurityElements.Count - 1 do
    begin
      rpcStruct := TRpcStruct.Create;
      rpcStruct.AddItem('T', AElementsType);
      rpcStruct.AddItem('N', ASecurityElements.GetElementByIndex(i));
      StateInfo := ASecurityElements.GetStateInfo(i);
      rpcStruct.AddItem('S', StateInfo.State);
      AArray.AddItem(rpcStruct);
    end;
  end;

begin
// evo.security.getAccountRights(string sessionId)
// Return array of structs. Struct's fields are:
// 'T' -  Char - Type of the element: 'S' - Screens; 'F' - Functions; 'M' - Menus, 'L' - Modules list according licence
// 'S' - Char - State: 'N' - Disabled; 'R' - Read Only; 'Y' - Enabled (returns 'N' or 'Y' only for 'T'='L')
// 'N' - String - Name of the element (for 'T'='L' allowed values: 'ADPImport', 'WebPayroll', 'HR', 'VMR', 'PaysuiteImport', 'CTSReports', 'SelfServe', 'EvoX')

  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  APICheckCondition((APIKey.APIKeyType = apiWebClient) or (APIKey.APIKeyType = apiSelfServe), SecurityCallsNotAllowed);

  rpcArray := TRpcArray.Create;
  SecurityElements := Context.Security.AccountRights.Screens;
  SecElementsToArray(etScreen, SecurityElements, rpcArray);

  SecurityElements := Context.Security.AccountRights.Functions;
  SecElementsToArray(etFunc, SecurityElements, rpcArray);

  SecurityElements := Context.Security.AccountRights.Menus;
  SecElementsToArray(etMenu, SecurityElements, rpcArray);

  // adding information about licensed modules
  ModulesList := Context.License.GetLicenseInfo;
  if ModulesList.ValueExists('SerialNumber') then
    ModulesList.DeleteValue('SerialNumber');

  if ModulesList.ValueExists('APIKeys') then
  begin
    APIKeys := IInterface(ModulesList.Value['APIKeys']) as IisListOfValues;
    for i := 0 to APIKeys.Count - 1 do
    begin
      KeyInfo := IInterface(APIKeys[i].Value) as IisListOfValues;
      if Assigned(KeyInfo) and ( KeyInfo.TryGetValue('APIKeyApplicationName', '') <> '' ) then
      begin
        s := APIKeys[i].Name;
        APIKeysStruct := TRpcStruct.Create;
        APIKeysStruct.AddItem('T', 'L');
        APIKeysStruct.AddItem('N', s);
        APIKeysStruct.AddItem('S', stEnabled);
        rpcArray.AddItem(APIKeysStruct);
      end;
    end;
  end;

  if ModulesList.ValueExists('APIKeys') then
    ModulesList.DeleteValue('APIKeys');
  for i := 0 to ModulesList.Count - 1 do
  begin
    ModuleInfoStruct := TRpcStruct.Create;
    ModuleInfoStruct.AddItem('T', 'L');
    ModuleInfoStruct.AddItem('N', ModulesList[i].Name);
    if ModulesList[i].Value then
      ModuleInfoStruct.AddItem('S', stEnabled)
    else
      ModuleInfoStruct.AddItem('S', stDisabled);
    rpcArray.AddItem(ModuleInfoStruct);
  end;

  AReturn.AddItem(rpcArray);
end;  


function TevXmlRPCServer.TOARequestToRpcStruct(const Requests: IisInterfaceList): IRPCArray;
var
  i, j: integer;
  RequestStruct: IRpcStruct;
  RequestDaysArray: IRpcArray;
  RequestDayStruct: IRPCStruct;
  Request: IEvTOABusinessRequest;
  DayRequest: IEvTOARequest;
begin
  Result := TRpcArray.Create;
  for i := 0 to Requests.Count - 1 do
  begin
    Request := IInterface(Requests.Items[i]) as IEvTOABusinessRequest;
    RequestStruct := TRpcStruct.Create;
    RequestStruct.AddItem('E', Request.EENbr);
    RequestStruct.AddItem('FN', Request.FirstName);
    RequestStruct.AddItem('LN', Request.LastName);
    RequestStruct.AddItem('CN', Request.CustomEENumber);
    RequestStruct.AddItem('T', Request.EETimeOffAccrualNbr);
    RequestStruct.AddItem('B', Request.Balance);
    RequestStruct.AddItem('N', Request.Description);
    RequestStruct.AddItemDateTime('R', Request.Date);
    RequestStruct.AddItem('S', Request.Status);
    RequestStruct.AddItem('C', Request.Notes);

    RequestDaysArray := TRpcArray.Create;
    for j := 0 to Request.Items.Count - 1 do
    begin
      DayRequest := IInterface(Request.Items[j]) as IEvTOARequest;
      RequestDayStruct := TRPCStruct.Create;
      RequestDayStruct.AddItemDateTime('D', DayRequest.AccrualDate);
      RequestDayStruct.AddItem('H', DayRequest.Hours);
      RequestDayStruct.AddItem('M', DayRequest.Minutes);
      RequestDayStruct.AddItem('N', DayRequest.EETimeOffAccrualOperNbr);
      RequestDayStruct.AddItem('S', DayRequest.Notes);

      RequestDaysArray.AddItem(RequestDayStruct);
    end;  // for j
    RequestStruct.AddItem('L', RequestDaysArray);

    Result.AddItem(RequestStruct);
  end;  // for i
end;

procedure TevXmlRPCServer.SB_GetWCReportList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId: String;
  CoNbr: integer;
  ReportNumbers: IRpcArray;
  ReportNumbersList: IisStringList;
  APIkey: IevAPIKey;
  i: integer;
  ResultDataSet: IevDataset;
  RPCStruct: IRpcStruct;
begin
// evo.sb.GetWCReportList(string sessionId, integer CoNbr, array of REPOR_WRITER_REPORTS_NBR)
// Return: dataset
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  CoNbr := IRpcParameter(AParams[1]).AsInteger;
  ReportNumbers := IRpcParameter(AParams[2]).AsArray;
  CheckCondition(ReportNumbers.Count > 0, 'Array of REPOR_WRITER_REPORTS_NBR cannot be empty');

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  ReportNumbersList := TisStringList.Create;
  for i := 0 to ReportNumbers.Count - 1 do
    ReportNumbersList.Add(IntToStr(ReportNumbers.Items[i].AsInteger));

  ResultDataSet := Context.RemoteMiscRoutines.GetWCReportList(CoNbr, ReportNumbersList);

  rpcStruct := TRpcStruct.Create;
  rpcStruct.AddItem('DATASET', DataSetToStruct(ResultDataSet, ''));

  AReturn.AddItem(rpcStruct);
end;

procedure TevXmlRPCServer.SB_GetVMRReportList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId: String;
  aCONbr, aVMRReportType : integer;
  aCheckFromDate, aCheckToDate : TdateTime;
  APIkey: IevAPIKey;
  ResultDataSet: IevDataset;
  RPCStruct: IRpcStruct;
begin
  //evo.sb.getVMRReportlist(string sessionId, integer pCONbr, datetime pCheckFromDate,datetime pCheckToDate, integer pVMRReportType)
  //Return: datset

  APICheckCondition(AParams.Count >= 5, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  aCONbr := IRpcParameter(AParams[1]).AsInteger;
  aCheckFromDate := IRpcParameter(AParams[2]).AsdateTime;
  aCheckToDate := IRpcParameter(AParams[3]).AsdateTime;
  aVMRReportType := IRpcParameter(AParams[4]).AsInteger;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  CheckCondition(Context.License.VMR, 'VMR License is invalid');
  //CheckCondition((ctx_AccountRights.Functions.GetState('VMR') = stEnabled), 'VMR Security function is invalid');

  ResultDataSet := Context.VmrRemote.GetVMRReportList(ctx_DataAccess.ClientID, aCONbr, aCheckFromDate, aCheckToDate, aVMRReportType);

  if Assigned(ResultDataSet) then
  begin
    rpcStruct := TRpcStruct.Create;
    rpcStruct.AddItem('DATASET', DataSetToStruct(ResultDataSet, ''));

    AReturn.AddItem(rpcStruct);
  end;
//don't throw exception 
//  else
//    raise Exception.Create('GetVMRReportList function returned no data');
end;

procedure TevXmlRPCServer.SB_GetVMRReport(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId, sComment: String;
  SelectedRpts: IRpcArray;
  ReportNumbersList: IisStringList;
  APIkey: IevAPIKey;
  i : integer;
  Results: IEvDualStream;
  IsPrint : boolean;
begin
// evo.sb.getVMRReport(string sessionId, array SelectedReportNbrs, boolean IsPrint, string aComment)
// Return: base64

  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID   := IRpcParameter(AParams[0]).AsString;
  SelectedRpts := IRpcParameter(AParams[1]).AsArray;
  IsPrint      := IRpcParameter(AParams[2]).AsBoolean;
  sComment     := IRpcParameter(AParams[3]).AsString;

  CheckCondition(SelectedRpts.Count > 0, 'Array of REPORT LIST cannot be empty');

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  CheckCondition(Context.License.VMR, 'VMR License is invalid');
  //CheckCondition((ctx_AccountRights.Functions.GetState('VMR') = stEnabled), 'VMR Security function is invalid');

  ReportNumbersList := TisStringList.Create;
  for i := 0 to SelectedRpts.Count - 1 do
    ReportNumbersList.Add(IntToStr(SelectedRpts.Items[i].AsInteger));

  Results := Context.VmrRemote.GetVMRReport(ReportNumbersList, IsPrint, sComment);

  if Assigned(Results) and (Results.Size > 0) then
  begin
    AReturn.AddItemBase64StrFromStream(Results.RealStream);
  end
  else
    raise Exception.Create('GetVMRReport function returned no data');
end;

procedure TevXmlRPCServer.SB_GetRemoteVMRReportList(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId, ALocation: String;
  APIkey: IevAPIKey;
  ResultDataSet: IevDataset;
  RPCStruct: IRpcStruct;
begin
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  ALocation := IRpcParameter(AParams[1]).AsString;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  CheckCondition(Context.License.VMR, 'VMR License is invalid');
  CheckCondition(CheckRemoteVMRActive, 'Remote VMR is not active');

  ResultDataSet := Context.VmrRemote.GetRemoteVMRReportList(ALocation);

  if Assigned(ResultDataSet) then
  begin
    rpcStruct := TRpcStruct.Create;
    rpcStruct.AddItem('DATASET', DataSetToStruct(ResultDataSet, ''));

    AReturn.AddItem(rpcStruct);
  end;
end;

procedure TevXmlRPCServer.SB_GetRemoteVMRReport(
  const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextId: String;
  sSbMailBoxContentNbr: integer;
  APIkey: IevAPIKey;
  Results: IEvDualStream;
begin
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID   := IRpcParameter(AParams[0]).AsString;
  sSbMailBoxContentNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  CheckCondition(Context.License.VMR, 'VMR License is invalid');
  CheckCondition(CheckRemoteVMRActive, 'Remote VMR is not active');

  Results := Context.VmrRemote.GetRemoteVMRReport(sSbMailBoxContentNbr);

  if Assigned(Results) and (Results.Size > 0) then
  begin
    AReturn.AddItemBase64StrFromStream(Results.RealStream);
  end
  else
    raise Exception.Create('SB_GetRemoteVMRReport function returned no data');
end;

procedure TevXmlRPCServer.SB_GetPayrollTotals(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextId: String;
  PrNbr: integer;
  APIkey: IevAPIKey;
  Taxes, EDs: Variant;
  TaxesDataSet, EDsDataSet: IevDataset;
  TaxesRPCStruct, EDsRPCStruct: IRpcStruct;
  ResultStruct: IRPCStruct;
  Checks: Integer;
  Hours, GrossWages, NetWages: Currency;
begin
// evo.sb.getPayrollTotals(string sessionId, integer PrNbr)
// Return: structure with such fields:
// TaxTotals - dataset
// EDTotals - dataset
// Checks - integer
// Hours - Float
// GrossWages - Float
// NetWages - Float

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  PrNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  Context.RemoteMiscRoutines.GetPayrollTaxTotals(PrNbr, Taxes);
  Context.RemoteMiscRoutines.GetPayrollEDTotals(PrNbr, EDs, Checks, Hours, GrossWages, NetWages);

  TaxesDataSet := TevDataSet.Create;
  EDsDataSet := TevDataSet.Create;

  TaxesDataSet.Data := IInterface(Taxes) as IEvDualStream;
  EDsDataSet.Data := IInterface(EDs) as IEvDualStream;

  TaxesRPCStruct := TRPCStruct.Create;
  EDsRPCStruct := TRPCStruct.Create;

  TaxesRPCStruct.AddItem('DATASET', DataSetToStruct(TaxesDataSet, ''));
  EDsRPCStruct.AddItem('DATASET', DataSetToStruct(EDsDataSet, ''));

  ResultStruct := TRPCStruct.Create;
  ResultStruct.AddItem('TaxTotals', TaxesRPCStruct);
  ResultStruct.AddItem('EDTotals', EDsRPCStruct);
  ResultStruct.AddItem('Checks', Checks);
  ResultStruct.AddItem('Hours', Hours);
  ResultStruct.AddItem('GrossWages', GrossWages);
  ResultStruct.AddItem('NetWages', NetWages);
  
  AReturn.AddItem(ResultStruct);
end;

procedure TevXmlRPCServer.SB_DeleteRecord(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID : String;
  sTableName: String;
  sCondition: String;
  iRowNbr: integer;
  TablesList: IisStringList;
  ConditionsList: IisStringList;
  tmpArray: IisListOfValues;
  RowNbrsList: IisStringList;
begin
//  evo.legacy.deleteRecord(String sessionID, String tableName, int recordNbr)
// Return: nothing
// deletes one record in separated transaction
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  sTableName := IRpcParameter(AParams[1]).AsString;
  iRowNbr := IRpcParameter(AParams[2]).AsInteger;
  CheckCondition(iRowNbr > 0, 'RowNbr cannot be below zero');

  sCondition := AnsiUpperCase(sTableName) + '_NBR=' + IntToStr(iRowNbr);
  TablesList := TisStringList.Create;
  TablesList.Add(sTableName);
  ConditionsList := TisStringList.Create;
  ConditionsList.Add(sCondition);
  tmpArray := TisListOfValues.Create;
  RowNbrsList := TisStringList.Create;
  tmpArray.AddValue('0', RowNbrsList);
  RowNbrsList.Add(IntToStr(iRowNbr));

  ctx_RemoteMiscRoutines.DeleteRowsInDataSets(ctx_DataAccess.ClientId, TablesList, ConditionsList, tmpArray);
end;

procedure TevXmlRPCServer.SB_autoEnlistEDsForNewEE(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID : String;
  APIKey: IevAPIKey;
  iCoNbr, iEENbr: integer;
begin
  //evo.sb.autoEnlistEDsForNewEE(String sessionID, integer CONbr, integer EENbr)
  //Return: nothing
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  APICheckCondition(APIKey.APIKeyType = apiWebClient, WrongLicenseException);

  iCoNbr := IRpcParameter(AParams[1]).AsInteger;
  iEENbr := IRpcParameter(AParams[2]).AsInteger;

  ctx_RemoteMiscRoutines.AddEDsForNewEE(iCoNbr, iEENbr);
end;

procedure TevXmlRPCServer.SB_autoEnlistTOAForNewEE(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID : String;
  APIKey: IevAPIKey;
  iCoNbr, iEENbr: integer;
begin
  //evo.sb.autoEnlistTOAForNewEE(String sessionID, integer CONbr, integer EENbr)
  //Return: nothing
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  APICheckCondition(APIKey.APIKeyType = apiWebClient, WrongLicenseException);

  iCoNbr := IRpcParameter(AParams[1]).AsInteger;
  iEENbr := IRpcParameter(AParams[2]).AsInteger;

  ctx_RemoteMiscRoutines.AddTOAForNewEE(iCoNbr, iEENbr);
end;

procedure TevXmlRPCServer.EEChangeRequestsToRpcArray(const ARequestsList: IisInterfaceList; const ARpcArray: IRpcArray);
var
  i: integer;
  Request: IevEEChangeRequest;
  rpcStruct1, StructFromChangeData: IRpcStruct;
  S: String;

begin
  for i := 0 to ARequestsList.Count - 1 do
  begin
    Request := IInterface(ARequestsList[i]) as IevEEChangeRequest;
    rpcStruct1 := TRpcStruct.Create;
    rpcStruct1.AddItem('EE_CHANGE_REQUEST_NBR', Request.EEChangeRequestNbr);
    rpcStruct1.AddItem('EE_NBR', Request.EENbr);
    S := EEChangeRequestTypeToString(Request.EEChangeRequestType);
    rpcStruct1.AddItem('REQUEST_TYPE', S);
    rpcStruct1.AddItem('FN', Request.EEFirstName);
    rpcStruct1.AddItem('LN', Request.EELastName);
    rpcStruct1.AddItem('CN', Request.CustomEENumber);

    if Request.RequestData <> nil then
      StructFromChangeData := DataChangePacketToParams(Request.RequestData)
    else
      StructFromChangeData := RestoreStructureFromXML(Request.OldFormatRequestData);

    rpcStruct1.AddItem('REQUEST_DATA', StructFromChangeData);

    ARpcArray.AddItem(rpcStruct1);
  end;
end;

function TevXmlRPCServer.ParamsToDataChangePacket(const AParams: IRPCStruct): IevDataChangePacket;
var
  i: integer;
  sTableName, sFieldName: String;
  iPos: integer;
  Nbr: integer;
  Value: Variant;
  ChangeData: IevDataChangeItem;

  function FindUpdateChangeDataRecord(const AChangePacket: IevDataChangePacket; const ATable: String; const AKey: integer):IevDataChangeItem;
  var
    i: integer;
    ChangeData: IevDataChangeItem;
  begin
    Result := nil;
    for i := 0 to AChangePacket.Count - 1 do
    begin
      ChangeData := IInterface(AChangePacket.Items[i]) as IevDataChangeItem;
      if (ChangeData.Key = AKey) and AnsiSameText(ChangeData.DataSource, ATable) then
      begin
        Result := ChangeData;
        break;
      end;
    end;
  end;
begin
  // this is very simple converter, it accepts update requests only and ignore conditions.
  Result := TevDataChangePacket.Create;

  for i := 0 to AParams.Count-1 do
  begin
    sTableName := AParams.KeyList.Strings[i];
    iPos := Pos('__', sTableName);
    if iPos > 0 then
    begin
      sFieldName := UpperCase(Copy(sTableName, iPos + 2, 4096));
      if not ((AnsiCompareText(sFieldName, 'EFFECTIVE_DATE') = 0) or
              (AnsiCompareText(sFieldName, 'EFFECTIVE_UNTIL') = 0)) then
      begin
        sTableName := UpperCase(LeftStr(sTableName, Pred(iPos)));
        iPos := Pos('__', sFieldName);
        if (iPos > 0) or AnsiSameText(sFieldName, sTableName + '_NBR') then
        begin
          if iPos > 0 then
          begin
            Nbr := StrToInt(StringReplace(Copy(sFieldName, iPos + 2, 4096), 'M', '-', []));
            sFieldName := LeftStr(sFieldName, Pred(iPos));
          end
          else
            Nbr := AParams.Items[i].AsInteger;

          CheckCondition(Nbr >= 0, 'Inserts are not supported');

          if not Assigned(AParams.Items[i]) then
            Value := null
          else
            case AParams.Items[i].GetDataType of
              dtString:
                Value := AParams.Items[i].AsString;
              dtBoolean:
                Value := AParams.Items[i].AsBoolean;
              dtDateTime:
                Value := AParams.Items[i].AsDateTime;
              dtFloat:
                Value := AParams.Items[i].AsFloat;
              dtInteger:
                Value := AParams.Items[i].AsInteger;
              else
                raise Exception.Create('Not supported datatype: ' + IntToStr(Integer(AParams.Items[i].GetDataType)));
            end; // case

          ChangeData := FindUpdateChangeDataRecord(Result, sTableName, Nbr);
          if not Assigned(ChangeData) then
            ChangeData := Result.NewUpdateFields(sTableName, Nbr);
          ChangeData.Fields.AddValue(sFieldName, Value);
        end;
      end;
    end;
  end;  // for
end;

function TevXmlRPCServer.DataChangePacketToParams(const AChangeRequestData: IevDataChangePacket): IRpcStruct;
var
  i, j: integer;
  ChangeItem: IevDataChangeItem;
  iValue: integer;
  sValue: String;
  fValue: double;
  dValue: TDateTime;
  bValue: boolean;
  Value: Variant;
  sName: String;
  DDField: TddsField;
  ConditionsList: IisListOfValues;
  ConditionValue: IisNamedValue;
  indexOfValue: integer;
begin
  // this is very simple converter which supports updates only
  Result := TRpcStruct.Create;
  ConditionsList := TisListOfValues.Create;

  for i := 0 to AChangeRequestData.Count - 1 do
  begin
    ChangeItem := AChangeRequestData.GetItem(i);
    CheckCondition(ChangeItem.ChangeType = dctUpdateFields, 'This converter supports only update changes');
    for j := 0 to ChangeItem.Fields.Count - 1 do
    begin
      if (AnsiSameText('EFFECTIVE_DATE', ChangeItem.Fields.Values[j].Name)) or
         (AnsiSameText('EFFECTIVE_UNTIL', ChangeItem.Fields.Values[j].Name)) then
        continue;
      if AnsiSameText(ChangeItem.DataSource + '_NBR', ChangeItem.Fields.Values[j].Name) then
        continue;

      indexOfValue := ConditionsList.IndexOf(ChangeItem.DataSource);
      if indexOfValue < 0 then
        ConditionsList.AddValue(ChangeItem.DataSource, ChangeItem.DataSource + '_NBR=' + IntToStr(ChangeItem.Key))
      else
      begin
        ConditionValue := ConditionsList.Values[indexOfValue];
        sValue := ChangeItem.DataSource + '_NBR=' + IntToStr(ChangeItem.Key);
        if sValue <> ConditionValue.Value then
          ConditionValue.Value := ConditionValue.Value + ' AND ' + sValue;
      end;

      sName := UpperCase(ChangeItem.DataSource) + '__' + ChangeItem.Fields.Values[j].Name + '__' +
        IntToStr(ChangeItem.Key);
      Value :=  ChangeItem.Fields.Values[j].Value;
      if Value = null then
      begin
        try
          DDField := TddsField(EvoDataDictionary.Tables.TableByName(UpperCase(ChangeItem.DataSource)).Fields.FieldByName(UpperCase(ChangeItem.Fields.Values[j].Name)));
        except
          on E: Exception do
            raise Exception.Create('Cannot find information in data dictionary. Table: ' + ChangeItem.DataSource +
              '. Field: ' + ChangeItem.Fields.Values[j].Name + '. Exception: ' + E.Message);
        end;
        CheckCondition(Assigned(DDField), 'Cannot find information in data dictionary. Table: ' + ChangeItem.DataSource +
          '. Field: ' + ChangeItem.Fields.Values[j].Name);
        case DDField.FieldType of
          ddtInteger: Result.AddItem(sName, 0);

          ddtFloat, ddtCurrency: Result.AddItem(sName, 0.0);
          ddtDateTime:
            begin
              dValue := 0;
              Result.AddItem(sName, dValue);
            end;
          ddtString: Result.AddItem(sName, '');
          ddtBoolean: Result.AddItem(sName, false);
        else
          raise Exception.Create('Unsupported datatype: ' + IntToStr(Integer(DDField.FieldType)));
        end;
      end
      else
        case VarType(Value) of
        varSmallint, varInteger, varShortInt, varByte, varWord, varLongWord, varInt64:
          begin
            iValue := Value;
            Result.AddItem(sName, iValue);
          end;
        varSingle, varDouble:
          begin
            fValue := Value;
            Result.AddItem(sName, fValue);
          end;
        varDate:
          begin
            dValue := Value;
            Result.AddItem(sName, dValue);
          end;
        varBoolean:
          begin
            bValue := Value;
            Result.AddItem(sName, bValue);
          end;
        varString:
          begin
            sValue := Value;
            Result.AddItem(sName, sValue);
          end;
        else
          raise Exception.Create('Unsupported datatype: ' + IntToStr(Integer(VarType(Value))));
        end;  // case
    end;  // for j
  end;  // for i

  for i := 0 to ConditionsList.Count - 1 do
  begin
    sValue := ConditionsList.Values[i].Value;
    Result.AddItem(ConditionsList.Values[i].Name, sValue);
  end;
end;

function TevXmlRPCServer.ProcessOldFormatEERequests(const ARequestsNbrs: IisStringList): IisStringList;
var
  StorageTable: TevClientDataSet;
  sNbr: String;
  Request: IevEEChangeRequest;
  sXML: String;
  StructFromBlob: IRpcStruct;
  sCondition: String;
  i: integer;
begin
  // Returns record nbrs with errors
  Result := TisSTringList.Create;

  StorageTable := ctx_DataAccess.GetDataSet(GetddTableClassByName('EE_CHANGE_REQUEST'));
  StorageTable.Active := false;
  StorageTable.Filtered := false;

  sCondition := '';
  CheckCondition(ARequestsNbrs.Count > 0, 'List of olf format requests cannot be empty!');
  for i := 0 to ARequestsNbrs.Count - 1 do
    if i = 0 then
      sCondition := ARequestsNbrs[i]
    else
      sCondition := sCondition + ',' + ARequestsNbrs[i];

  StorageTable.DataRequired('EE_CHANGE_REQUEST_NBR IN (' + sCondition + ')');

  StorageTable.First;
  while not StorageTable.eof do
  begin
    // approving requests in old format
    sNbr := IntToStr(StorageTable['EE_CHANGE_REQUEST_NBR']);
    Request := LoadEEChangeRequestFromDataset(StorageTable);
    if Request.RequestData = nil then
    begin
      sXML := Request.OldFormatRequestData;
      try
        StructFromBlob := RestoreStructureFromXML(sXML);
        APIPostDataSets(ctx_DataAccess.ClientId, StructFromBlob);
        StorageTable.Delete;
      except
        on E:Exception do
        begin
          Result.Add(sNbr);
          StorageTable.Next;
        end;
      end;  // try
    end
    else
      StorageTable.Next;
  end;  // while

  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    ctx_DataAccess.PostDataSets([StorageTable]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    on E:Exception do
    begin
      // rollback be here in Manchester
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;
end;

procedure TevXmlRPCServer.CreateNewClientDB(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID : String;
  iNewClientNbr: integer;
begin
// evo.createNewClientDB(string sessionId)
// Return: integer - internal number of created client database
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  iNewClientNbr := ctx_DBAccess.CreateClientDB(nil);

  AReturn.AddItem(iNewClientNbr);
end;

procedure TevXmlRPCServer.ApplyDataChangePacket(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
const
  NullConstant = '#NULL#';
var
  sContextID : String;
  rpcInputPacket: IRpcArray;
  rpcRowStruct: IRpcStruct;
  InputPacket: IevDataChangePacket;
  DataChangeItem: IevDataChangeItem;
  sDataSource: String;
  sDataChangeType: String;
  iKey: integer;
  rpcNewFieldsValues: IRpcStruct;
  DataSourcesList: IisStringList;
  DDTable: TddTableClass;
  DBTypes: TevDBTypes;
  ResultMap: IRPCStruct;
  i: integer;
  KeyMap: IisValueMap;

  procedure CopyFieldsValues(const AStruct: IRPCStruct; const DataItems: IevDataChangeItem);
  var
    i: integer;
    sFieldName: String;
    FieldValue: Variant;
    Item: IRpcStructItem;
    KeysList: IisStringList;
  begin
    KeysList := AStruct.KeyList;

    for i := 0 to KeysList.Count - 1 do
    begin
      Item := AStruct.Keys[KeysList[i]];
      sFieldName := AnsiUpperCase(KeysList[i]);
      CheckCondition(Trim(sFieldName) <> '', 'Field name cannot be empty');

      if Item.IsString and (AnsiUpperCase(Item.AsString) = NullConstant) then
        FieldValue := null
      else
      begin
        case Item.GetDataType of
          dtString:
            FieldValue := Item.AsString;
          dtBoolean:
            FieldValue := Item.AsBoolean;
          dtDateTime:
            FieldValue := Item.AsDateTime;
          dtFloat:
            FieldValue := Item.AsFloat;
          dtInteger:
            FieldValue := Item.AsInteger;
          dtBase64:
            FieldValue := Item.AsBase64Str;
          else
            raise Exception.Create('Not supported datatype: ' + IntToStr(Integer(Item.GetDataType)));
        end; // case
      end;

      DataItems.Fields.AddValue(sFieldName, FieldValue);
    end;
  end;
begin
// evo.applyDataChangePacket(string session Id, array of struct dataChangePacket, [DateTime AsOfDate])
// returns structure, the first field contains old negative value, the second field contains new positive value
// don't pass AsOfDate in order to change AsOfNow!!!
// Where dataChangePacket has suct structure:
// On the top it is an array. Each element of array is a struct which contains dataChanges for one row.
// Struct contains fields:
//  'T' - change type, possible values: 'I' - insert, 'D' - delete, 'U' - update. Value is required.
//  'D' - datasource name (table name). Value is required
//  'K' - record_nbr, required for updates and deletes. Not used for inserts. FOR INSERT you should provide negative value of proper '_NBR' field in list of fields values. See 'F'.
//  'F' - structure which is a list of new values for fields. Structure's field name is table's field name, structure's value is new field value. Please pass '#NULL#' for update current value to the NULL value.
//  for deleting 'F' could be skipped
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  rpcInputPacket := IRpcParameter(AParams[1]).AsArray;
  InputPacket := TevDataChangePacket.Create;

  if AParams.Count > 2 then
    InputPacket.AsOfDate := IRpcParameter(AParams[2]).AsDateTime;

  CheckCondition(rpcInputPacket.Count > 0, 'Input packet cannot be empty!');
  DataSourcesList := TisStringList.Create;

  for i := 0 to rpcInputPacket.Count - 1 do
  begin
    rpcRowStruct := rpcInputPacket.Items[i].AsStruct;
    sDataChangeType := rpcRowStruct.Keys['T'].AsString;
    CheckCondition((sDataChangeType = 'I') or (sDataChangeType = 'U') or (sDataChangeType = 'D'), 'Unknown change type: ' + sDataChangeType);
    sDataSource := AnsiUpperCase(rpcRowStruct.Keys['D'].AsString);
    if DataSourcesList.IndexOf(sDataSource) = -1 then
      DataSourcesList.Add(sDataSource);

    if sDataChangeType = 'I' then
    begin
      DataChangeItem := InputPacket.NewInsertRecord(sDataSource);
      rpcNewFieldsValues := rpcRowStruct.Keys['F'].AsStruct;
      CopyFieldsValues(rpcNewFieldsValues, DataChangeItem);
    end
    else if sDataChangeType = 'U' then
    begin
      iKey := rpcRowStruct.Keys['K'].AsInteger;
      DataChangeItem := InputPacket.NewUpdateFields(sDataSource, iKey);
      rpcNewFieldsValues := rpcRowStruct.Keys['F'].AsStruct;
      CopyFieldsValues(rpcNewFieldsValues, DataChangeItem);
    end
    else if sDataChangeType = 'D' then
    begin
      iKey := rpcRowStruct.Keys['K'].AsInteger;
      DataChangeItem := InputPacket.NewDeleteRecord(sDataSource, iKey);
    end;
  end;  // for

  CheckCondition(DataSourcesList.Count > 0, 'Tables list cannot be empty');
  DBTypes := [];
  for i := 0 to DataSourcesList.Count - 1 do
  begin
    DDTable := GetddTableClassByName(DataSourcesList[i]);
    CheckCondition(Assigned(DDTable), 'Cannot find table class by table name: ' + DataSourcesList[i]);
    if (DDTable.GetDBType = dbtClient) and not (dbtClient in DBTypes) then
      DBTypes := DBTypes + [dbtClient]
    else if (DDTable.GetDBType = dbtBureau) and not (dbtBureau in DBTypes) then
      DBTypes := DBTypes + [dbtBureau]
    else if DDTable.GetDBType in [dbtUnknown, dbtUnspecified, dbtSystem, dbtTemporary] then
      raise Exception.Create('API allows to change client and service bureau databases only');
  end;

  ctx_DataAccess.StartNestedTransaction(DBTypes);
  try
    ctx_DBAccess.ApplyDataChangePacket(InputPacket);
    KeyMap := ctx_DataAccess.CommitNestedTransaction;
  except
    on E: Exception do
    begin
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;

  // creating map
  ResultMap := TRPCStruct.Create;
  for i := 0 to KeyMap.Count - 1 do
    ResultMap.AddItem(VarToStr(KeyMap[i].OrigValue), VarToStr(KeyMap[i].NewValue));

  AReturn.AddItem(ResultMap);
end;

procedure TevXmlRPCServer.SB_GetCoCalendarDefaults(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextId: String;
  iCoNbr: integer;
  ResultMainList, ResultTabList: IisListOfValues;
  ResultMainStruct, ResultTabStruct: IRpcStruct;
  i: integer;
begin
// evo.sb.getCoCalendarDefaults(string session Id, integer CoNbr)
// returns struct of struct
// client must be opened before calling this function
// the highest level structure contains these fields (each field is another structure). Fields are presented if company has proper frequency
// 'W' - weekly defaults, 'B' - bi-weekly defaults, 'S' - semi-monthly defaults, 'M' - monthly defaults, 'Q' - quartelry defaults.
// Structure of each field (fields with number 2 are presented for semi-monthly frequency only:
// - integer NUMBER_OF_DAYS_PRIOR, NUMBER_OF_DAYS_PRIOR2
// - string (contains time in format: hh:mmm:ss) CALL_IN_TIME, CALL_IN_TIME2
// - integer NUMBER_OF_DAYS_AFTER, NUMBER_OF_DAYS_AFTER2
// - string (contains time in format: hh:mmm:ss) DELIVERY_TIME, DELIVERY_TIME2
// - date LAST_REAL_SCHEDULED_CHECK_DATE, LAST_REAL_SCHED_CHECK_DATE2
// - date PERIOD_BEGIN_DATE, PERIOD_BEGIN_DATE2
// - date PERIOD_END_DATE, PERIOD_END_DATE2
// - integer NUMBER_OF_MONTHS
// - string FILLER (if FILLER[1] is 'R'- calendar is based on calendar day; 'B' - on business day)
// - string MOVE_CHECK_DATE, MOVE_CALLIN_DATE, MOVE_DELIVERY_DATE ('F' - Forwards, 'B' - Backwards, 'K' - Keep)
// If FILLER[1] is 'B' then MOVE_CALLIN_DATE, MOVE_DELIVERY_DATE don't matter

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iCoNbr := IRpcParameter(AParams[1]).AsInteger;
  SetCurrentContext(sContextID);

  ResultMainList := Context.RemoteMiscRoutines.GetCoCalendarDefaults(iCoNbr);
  ResultMainStruct := TRpcStruct.Create;
  for i := 0 to ResultMainList.Count - 1 do
  begin
    ResultTabStruct := TRpcStruct.Create;
    ResultMainStruct.AddItem(ResultMainList[i].Name, ResultTabStruct);
    ResultTabList := IInterface(ResultMainList[i].Value) as IisListOfValues;
    ResultTabStruct.AddItem('NUMBER_OF_DAYS_PRIOR', Integer(ResultTabList.Value['NUMBER_OF_DAYS_PRIOR']));
    ResultTabStruct.AddItem('CALL_IN_TIME', String(ResultTabList.Value['CALL_IN_TIME']));
    ResultTabStruct.AddItem('NUMBER_OF_DAYS_AFTER', Integer(ResultTabList.Value['NUMBER_OF_DAYS_AFTER']));
    ResultTabStruct.AddItem('DELIVERY_TIME', String(ResultTabList.Value['DELIVERY_TIME']));
    ResultTabStruct.AddItem('LAST_REAL_SCHEDULED_CHECK_DATE', TDateTime(ResultTabList.Value['LAST_REAL_SCHEDULED_CHECK_DATE']));
    ResultTabStruct.AddItem('PERIOD_BEGIN_DATE', TDateTime(ResultTabList.Value['PERIOD_BEGIN_DATE']));
    ResultTabStruct.AddItem('PERIOD_END_DATE', TDateTime(ResultTabList.Value['PERIOD_END_DATE']));
    ResultTabStruct.AddItem('NUMBER_OF_MONTHS', Integer(ResultTabList.Value['NUMBER_OF_MONTHS']));
    ResultTabStruct.AddItem('FILLER', String(ResultTabList.Value['FILLER']));
    ResultTabStruct.AddItem('MOVE_CHECK_DATE', String(ResultTabList.Value['MOVE_CHECK_DATE']));
    ResultTabStruct.AddItem('MOVE_CALLIN_DATE', String(ResultTabList.Value['MOVE_CALLIN_DATE']));
    ResultTabStruct.AddItem('MOVE_DELIVERY_DATE', String(ResultTabList.Value['MOVE_DELIVERY_DATE']));
    if ResultMainList[i].Name = 'S' then
    begin
      ResultTabStruct.AddItem('NUMBER_OF_DAYS_PRIOR2', Integer(ResultTabList.Value['NUMBER_OF_DAYS_PRIOR2']));
      ResultTabStruct.AddItem('CALL_IN_TIME2', String(ResultTabList.Value['CALL_IN_TIME2']));
      ResultTabStruct.AddItem('NUMBER_OF_DAYS_AFTER2', Integer(ResultTabList.Value['NUMBER_OF_DAYS_AFTER2']));
      ResultTabStruct.AddItem('DELIVERY_TIME2', String(ResultTabList.Value['DELIVERY_TIME2']));
      ResultTabStruct.AddItem('LAST_REAL_SCHEDULED_CHECK_DATE2', TDateTime(ResultTabList.Value['LAST_REAL_SCHEDULED_CHECK_DATE2']));
      ResultTabStruct.AddItem('PERIOD_BEGIN_DATE2', TDateTime(ResultTabList.Value['PERIOD_BEGIN_DATE2']));
      ResultTabStruct.AddItem('PERIOD_END_DATE2', TDateTime(ResultTabList.Value['PERIOD_END_DATE2']));
    end;
  end;

  AReturn.AddItem(ResultMainStruct);
end;

procedure TevXmlRPCServer.SB_CreateCoCalendar(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextId: String;
  iCoNbr: integer;
  CoSettings, OneFreqStruct: IRpcStruct;
  BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate: String;
  ListWithSettings: IisListOfValues;
  FreqSettings: IisListOfValues;
  WarningsList: IisStringList;
  ResultArray: IRpcArray;
  i: integer;
  sFreq: String;
begin
// evo.sb.createCoCalendar(string session Id, integer CoNbr, struct CoSettings, string BasedOn, string MoveCheckDate, string MoveCallInDate, string MoveDeliveryDate)
// returns array of string with warnings, if any
// client must be opened before calling this function
// BasedOn could have values: 'R'- calendar is based on calendar day; 'B' - on business day
// MoveCheckDate, MoveCallInDate, MoveDeliveryDate: 'F' - Forwards, 'B' - Backwards, 'K' - Keep
// If BasedOn is 'B' then MoveCallInDate, MoveDeliveryDate don't matter
// CoSettings is struct of struct
// the highest level structure contains these fields (each field is another structure). Fields are presented if company has proper frequency
// 'W' - weekly defaults, 'B' - bi-weekly defaults, 'S' - semi-monthly defaults, 'M' - monthly defaults, 'Q' - quartelry defaults.
// Structure of each field (fields with number 2 are presented for semi-monthly frequency only:
// - integer NUMBER_OF_DAYS_PRIOR, NUMBER_OF_DAYS_PRIOR2
// - string (contains time in format: hh:mmm:ss) CALL_IN_TIME, CALL_IN_TIME2
// - integer NUMBER_OF_DAYS_AFTER, NUMBER_OF_DAYS_AFTER2
// - string (contains time in format: hh:mmm:ss) DELIVERY_TIME, DELIVERY_TIME2
// - date LAST_REAL_SCHEDULED_CHECK_DATE, LAST_REAL_SCHED_CHECK_DATE2
// - date PERIOD_BEGIN_DATE, PERIOD_BEGIN_DATE2
// - date PERIOD_END_DATE, PERIOD_END_DATE2
// - integer NUMBER_OF_MONTHS
// - boolean CheckEndOfMonth, CheckEndOfMonth2
// - boolean PeriodEndOfMonth, PeriodEndOfMonth2

  APICheckCondition(AParams.Count >= 7, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iCoNbr := IRpcParameter(AParams[1]).AsInteger;
  SetCurrentContext(sContextID);

  CoSettings := IRpcParameter(AParams[2]).AsStruct;
  BasedOn := IRpcParameter(AParams[3]).AsString;
  CheckCondition((BasedOn = 'B') or (BasedOn = 'R'), 'Wrong value of BasedOn parameter: ' + BasedOn);
  MoveCheckDate := IRpcParameter(AParams[4]).AsString;
  if BasedOn = 'R' then
  begin
    MoveCallInDate := IRpcParameter(AParams[5]).AsString;
    MoveDeliveryDate := IRpcParameter(AParams[6]).AsString;
  end
  else
  begin
    MoveCallInDate := '';
    MoveDeliveryDate := '';
  end;

  ListWithSettings := TisListOfValues.Create;
  for i := 0 to CoSettings.KeyList.Count - 1 do
  begin
    sFreq := CoSettings.KeyList[i];
    OneFreqStruct := CoSettings.Keys[sFreq].AsStruct;
    FreqSettings := TisListOfValues.Create;
    ListWithSettings.AddValue(sFreq, FreqSettings);

    FreqSettings.AddValue('NUMBER_OF_DAYS_PRIOR', OneFreqStruct.Keys['NUMBER_OF_DAYS_PRIOR'].AsInteger);
    FreqSettings.AddValue('CALL_IN_TIME', OneFreqStruct.Keys['CALL_IN_TIME'].AsString);
    FreqSettings.AddValue('NUMBER_OF_DAYS_AFTER', OneFreqStruct.Keys['NUMBER_OF_DAYS_AFTER'].AsInteger);
    FreqSettings.AddValue('DELIVERY_TIME', OneFreqStruct.Keys['DELIVERY_TIME'].AsString);
    FreqSettings.AddValue('LAST_REAL_SCHEDULED_CHECK_DATE', OneFreqStruct.Keys['LAST_REAL_SCHEDULED_CHECK_DATE'].AsDateTime);
    FreqSettings.AddValue('PERIOD_BEGIN_DATE', OneFreqStruct.Keys['PERIOD_BEGIN_DATE'].AsDateTime);
    FreqSettings.AddValue('PERIOD_END_DATE', OneFreqStruct.Keys['PERIOD_END_DATE'].AsDateTime);
    FreqSettings.AddValue('NUMBER_OF_MONTHS', OneFreqStruct.Keys['NUMBER_OF_MONTHS'].AsInteger);
    FreqSettings.AddValue('CheckEndOfMonth', OneFreqStruct.Keys['CheckEndOfMonth'].AsBoolean);
    FreqSettings.AddValue('PeriodEndOfMonth', OneFreqStruct.Keys['PeriodEndOfMonth'].AsBoolean);
    if sFreq = 'S' then
    begin
      FreqSettings.AddValue('NUMBER_OF_DAYS_PRIOR2', OneFreqStruct.Keys['NUMBER_OF_DAYS_PRIOR2'].AsInteger);
      FreqSettings.AddValue('CALL_IN_TIME2', OneFreqStruct.Keys['CALL_IN_TIME2'].AsString);
      FreqSettings.AddValue('NUMBER_OF_DAYS_AFTER2', OneFreqStruct.Keys['NUMBER_OF_DAYS_AFTER2'].AsInteger);
      FreqSettings.AddValue('DELIVERY_TIME2', OneFreqStruct.Keys['DELIVERY_TIME2'].AsString);
      FreqSettings.AddValue('LAST_REAL_SCHED_CHECK_DATE2', OneFreqStruct.Keys['LAST_REAL_SCHED_CHECK_DATE2'].AsDateTime);
      FreqSettings.AddValue('PERIOD_BEGIN_DATE2', OneFreqStruct.Keys['PERIOD_BEGIN_DATE2'].AsDateTime);
      FreqSettings.AddValue('PERIOD_END_DATE2', OneFreqStruct.Keys['PERIOD_END_DATE2'].AsDateTime);
      FreqSettings.AddValue('CheckEndOfMonth2', OneFreqStruct.Keys['CheckEndOfMonth2'].AsBoolean);
      FreqSettings.AddValue('PeriodEndOfMonth2', OneFreqStruct.Keys['PeriodEndOfMonth2'].AsBoolean);
    end;
  end;

  WarningsList := Context.RemoteMiscRoutines.CreateCoCalendar(iConbr, ListWithSettings, BasedOn, MoveCheckDate, MoveCallInDate, MoveDeliveryDate);
  ResultArray := TRpcArray.Create;
  for i := 0 to WarningsList.Count - 1 do
    ResultArray.AddItem(WarningsList[i]);

  AReturn.AddItem(ResultArray);
end;

procedure TevXmlRPCServer.GetSequrityQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  Context: IevContext;
  APIKey : IevAPIKey;
  sDomain, sGUID: String;
  AvaliableQuestions: IisStringList;
  ResultAvaliableQuestions: IRpcArray;
  i: integer;
begin
// evo.getSecurityQuestions(string LicenseKey [, string evoDomain])
// Return array of strings: list of all avaliable questions for WebClient and ESS
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  if AParams.Count > 1 then
    sDomain := IRpcParameter(AParams[1]).AsString
  else
    sDomain := '';

  Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
  APICheckCondition((APIKey.APIKeyType = apiSelfServe) or (APIKey.APIKeyType = apiWebClient), WrongLicenseException);

  AvaliableQuestions := Context.Security.GetAvailableQuestions('');
  ResultAvaliableQuestions := TRpcArray.Create;
  for i := 0 to AvaliableQuestions.Count - 1 do
    ResultAvaliableQuestions.AddItem(AvaliableQuestions[i]);

  AReturn.AddItem(ResultAvaliableQuestions);
end;

procedure TevXmlRPCServer.GetUserQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  Context: IevContext;
  APIKey : IevAPIKey;
  sDomain, sGUID, sUserName: String;
  UserQuestions: IisStringList;
  ResultUserQuestions: IRpcArray;
  i: integer;
begin
// evo.getUserQuestions(string LicenseKey, string AUserName [, string evoDomain])
// Return array of strings.
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  sUsername := IRpcParameter(AParams[1]).AsString;
  if AParams.Count > 2 then
    sDomain := IRpcParameter(AParams[2]).AsString
  else
    sDomain := '';

  Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
  APICheckCondition((APIKey.APIKeyType = apiSelfServe) or (APIKey.APIKeyType = apiWebClient), WrongLicenseException);

  if APIKey.APIKeyType = apiSelfServe then
    sUserName := 'EE.' + sUserName;
  UserQuestions := Context.Security.GetQuestions(sUsername);
  ResultUserQuestions := TRpcArray.Create;
  for i := 0 to UserQuestions.Count - 1 do
    ResultUserQuestions.AddItem(UserQuestions[i]);

  AReturn.AddItem(ResultUserQuestions);
end;

procedure TevXmlRPCServer.ResetPassword(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  Context: IevContext;
  APIKey : IevAPIKey;
  sDomain, sGUID, sUserName, sNewPassword: String;
  QAArray: IRpcArray;
  QAStruct: IRpcStruct;
  QAList: IisListOfValues;
  sQuestion, sAnswer: String;
  i: integer;
begin
// evo.resetPassword(string Licensekey, string UserName, array of struct QestionsAndAnswers, string NewPassowrd [, string domain])
// array of struct QestionsAndAnswers contains 3 elements, each element is struct with two fields: 'Q' and 'A'
// return nothing
  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  sUsername := IRpcParameter(AParams[1]).AsString;
  QAArray := IRpcParameter(AParams[2]).AsArray;
  sNewPassword := IRpcParameter(AParams[3]).AsString;
  if AParams.Count > 4 then
    sDomain := IRpcParameter(AParams[4]).AsString
  else
    sDomain := '';

  Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
  APICheckCondition((APIKey.APIKeyType = apiSelfServe) or (APIKey.APIKeyType = apiWebClient), WrongLicenseException);

  if APIKey.APIKeyType = apiSelfServe then
    sUserName := 'EE.' + sUserName;

  QAList := TisListOfValues.Create;

  for i := 0 to QAArray.Count - 1 do
  begin
    QAStruct := QAArray.Items[i].AsStruct;
    sQuestion := QAStruct.Keys['Q'].AsString;
    sAnswer := QAStruct.Keys['A'].AsString;
    QAList.AddValue(sQuestion, sAnswer);
  end;

  Context.Security.ValidatePassword(sNewPassword);
  try
    Context.Security.ResetPassword(sUserName, QAList, sNewPassword);
  except
    on E: EBlockedAccount do
    begin
      CleanUpCookiesForUser(AnsiUpperCase(EncodeUserAtDomain(sUserName, sDomain)));
      raise;
    end;
  end;  
end;

procedure TevXmlRPCServer.SetUserQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  QAArray: IRpcArray;
  QAStruct: IRpcStruct;
  QAList: IisListOfValues;
  sQuestion, sAnswer: String;
  i: integer;
begin
// evo.setUserQuestions(string sessionid, array of struct)
// Returns nothing
// Struct contains two fields: 'Q' and 'A'. Dimention of array must be 3.
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  QAArray := IRpcParameter(AParams[1]).AsArray;

  SetCurrentContext(sContextID);

  QAList := TisListOfValues.Create;
  for i := 0 to QAArray.Count - 1 do
  begin
    QAStruct := QAArray.Items[i].AsStruct;
    sQuestion := QAStruct.Keys['Q'].AsString;
    sAnswer := QAStruct.Keys['A'].AsString;
    QAList.AddValue(sQuestion, sAnswer);
  end;

  Context.Security.SetQuestions(QAList);
end;

procedure TevXmlRPCServer.EE_SetUserSecData(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  APIKey: IevAPIKey;
  InputStruct: IRPCStruct;
  sNewEmail, sNewPassword: String;
  QAArray: IRpcArray;
  QAStruct: IRpcStruct;
  QAList: IisListOfValues;
  sQuestion, sAnswer: String;
  sNewSessionId: String;
  ResultStruct: IRpcStruct;
  SequrityQuestions: IisStringList;
  i: integer;
begin
// 'evo.ee.setUserSeqData(string sessionId, struct)',
// 'Return: struct - {SESSION=Session ID(string), it will be the same as input parameter, if you didn't change password}',
// struct MAY contain 3 fields:
//   - 'QandA' - 3 questions and answers
//   - 'P' - new password
//   - 'E' - new EE's Email
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sNewSessionId := sContextID;
  InputStruct := IRpcParameter(AParams[1]).AsStruct;
  SetCurrentContext(sContextID, touDoNotCheck);
  SequrityQuestions := Context.Security.GetQuestions(Context.UserAccount.User);
  if SequrityQuestions.Count > 0 then
    SetCurrentContext(sContextID, touCheck);

  APIKey := GetLicenseKeyFromContext(sContextID);
  APICheckCondition(APIKey.APIKeyType = apiSelfServe, WrongLicenseException);

  if InputStruct.KeyExists('P') then
  begin
    sNewPassword := InputStruct.Keys['P'].AsString;
    Context.Security.ValidatePassword(sNewPassword);
    sNewSessionId := ChangeUserPassword(sContextId, Context.UserAccount.User, sNewPassword, Context.UserAccount.Domain);
  end;

  if InputStruct.KeyExists('QandA') then
  begin
    QAArray := InputStruct.Keys['QandA'].AsArray;
    QAList := TisListOfValues.Create;

    for i := 0 to QAArray.Count - 1 do
    begin
      QAStruct := QAArray.Items[i].AsStruct;
      sQuestion := QAStruct.Keys['Q'].AsString;
      sAnswer := QAStruct.Keys['A'].AsString;
      QAList.AddValue(sQuestion, sAnswer);
    end;

    Context.Security.SetQuestions(QAList);
  end;

  if InputStruct.KeyExists('E') then
  begin
    sNewEmail := InputStruct.Keys['E'].AsString;
    Context.RemoteMiscRoutines.SetEeEmail(Abs(Context.UserAccount.InternalNbr), sNewEMail);
  end;

  ResultStruct := TRpcStruct.Create;
  ResultStruct.AddItem('SESSION', sNewSessionId);

  AReturn.AddItem(ResultStruct);
end;

function TevXmlRPCServer.ChangeUserPassword(const AContextId, AUsername, ANewPassword, ADomain: String): String;
var
  ContextObjFromPool, NewContextObjFromPool: IContextObjFromPool;
  userContext: IevContext;
  oldClientId: integer;
begin
  OldClientId := ctx_DataAccess.ClientID;
  Context.Security.ChangePassword(ANewPassword);

  ContextObjFromPool := FContextPool.AcquireObject(AContextID) as IContextObjFromPool;
  userContext := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(AUserName, ADomain), HashPassword(ANewPassword));
  ctx_DataAccess.OpenClient(OldClientId);

  NewContextObjFromPool := FContextPool.AcquireObject(userContext.GetID) as IContextObjFromPool;
  NewContextObjFromPool.SetContext(userContext);
  NewContextObjFromPool.SetLicenseKey(ContextObjFromPool.GetLicenseKey);
  NewContextObjFromPool.SetTermsOfUseAccepted(ContextObjFromPool.GetTermsOfUseAccepted);
  FContextPool.ReturnObject(NewContextObjFromPool as IisObjectFromPool);

  Result := userContext.GetId;
end;

procedure TevXmlRPCServer.GetPasswordRules(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  Context: IevContext;
  APIKey : IevAPIKey;
  sDomain, sGUID: String;
  PasswordRules: IisListOfValues;
  sPasswordRules: String;
begin
// evo.getPasswordRules(string LicenseKey [, string evoDomain])
// Return string which contains string: password rules as one string message
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  if AParams.Count > 1 then
    sDomain := IRpcParameter(AParams[1]).AsString
  else
    sDomain := '';

  Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
  APICheckCondition((APIKey.APIKeyType = apiSelfServe) or (APIKey.APIKeyType = apiWebClient), WrongLicenseException);

  PasswordRules := Context.Security.GetPasswordRules;
  sPasswordRules := PasswordRules.Value['Description'];

  AReturn.AddItem(sPasswordRules);
end;

procedure TevXmlRPCServer.SB_SetUserSecData(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  APIKey: IevAPIKey;
  InputStruct: IRPCStruct;
  sNewPassword: String;
  QAArray: IRpcArray;
  QAStruct: IRpcStruct;
  QAList: IisListOfValues;
  sQuestion, sAnswer: String;
  sNewSessionId: String;
  ResultStruct: IRpcStruct;
  i: integer;
  SequrityQuestions: IisStringList;
begin
// 'evo.sb.setUserSeqData(string sessionId, struct)',
// 'Return: struct - {SESSION=Session ID(string), it will be the same as input parameter, if you didn't change password}',
// struct MAY contain 2 fields:
//   - 'QandA' - 3 questions and answers
//   - 'P' - new password
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sNewSessionId := sContextID;
  InputStruct := IRpcParameter(AParams[1]).AsStruct;
  SetCurrentContext(sContextID, touDoNotCheck);
  SequrityQuestions := Context.Security.GetQuestions(Context.UserAccount.User);
  if SequrityQuestions.Count > 0 then
    SetCurrentContext(sContextID, touCheck);

  APIKey := GetLicenseKeyFromContext(sContextID);
  APICheckCondition(APIKey.APIKeyType = apiWebClient, WrongLicenseException);

  if InputStruct.KeyExists('P') then
  begin
    sNewPassword := InputStruct.Keys['P'].AsString;
    Context.Security.ValidatePassword(sNewPassword);
    sNewSessionId := ChangeUserPassword(sContextId, Context.UserAccount.User, sNewPassword,  Context.UserAccount.Domain);
  end;

  if InputStruct.KeyExists('QandA') then
  begin
    QAArray := InputStruct.Keys['QandA'].AsArray;
    QAList := TisListOfValues.Create;

    for i := 0 to QAArray.Count - 1 do
    begin
      QAStruct := QAArray.Items[i].AsStruct;
      sQuestion := QAStruct.Keys['Q'].AsString;
      sAnswer := QAStruct.Keys['A'].AsString;
      QAList.AddValue(sQuestion, sAnswer);
    end;

    Context.Security.SetQuestions(QAList);
  end;

  ResultStruct := TRpcStruct.Create;
  ResultStruct.AddItem('SESSION', sNewSessionId);

  AReturn.AddItem(ResultStruct);
end;

procedure TevXmlRPCServer.GetSBInfo(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  Context: IevContext;
  sDomain: String;
  SecurityRules: IisListOfValues;
  ResultStruct: IRPCStruct;
  bSbPasswordReset: Boolean;
  bSbExtLogin: Boolean;
  bEePasswordReset: Boolean;
  bEeExtLogin: Boolean;
begin
// 'evo.getSBInfo(string domain)'
// returns structure, with these fields:
// SbPasswordReset: Boolean
// SbExtLogin: Boolean
// EePasswordReset: Boolean
// EeExtLogin: Boolean
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);
  sDomain := IRpcParameter(AParams[0]).AsString;
  Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

  SecurityRules := Context.Security.GetSecurityRules;
  ResultStruct := TRPCStruct.Create;

  bSbPasswordReset := SecurityRules.Value['SbPasswordReset'];
  bSbExtLogin := SecurityRules.Value['SbExtLogin'];
  bEePasswordReset := SecurityRules.Value['EePasswordReset'];
  bEeExtLogin := SecurityRules.Value['EeExtLogin'];

  ResultStruct.AddItem('SbPasswordReset', bSbPasswordReset);
  ResultStruct.AddItem('SbExtLogin', bSbExtLogin);
  ResultStruct.AddItem('EePasswordReset', bEePasswordReset);
  ResultStruct.AddItem('EeExtLogin', bEeExtLogin);

  AReturn.AddItem(ResultStruct);
end;

procedure TevXmlRPCServer.GetBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iRequestNbr: integer;
  i: integer;
  DataSet: IevDataset;
  ResultRequestData: IRPCStruct;
  DatasetData: IRPCStruct;
  rpcStruct: IRpcStruct;
  StoredRequestData: IisListOfValues;
  iNbr: integer;
  sMap: String;
begin
// 'evo.getBenefitRequest(string sessionId, integer requestNbr)',
// 'Return: struct - each field of struct is dataset except: 'EE_NBR' and 'SIGNATURE_NBR'
//  if 'SIGNATURE_NBR' = -1, it means that it's null

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iRequestNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  StoredRequestData := Context.RemoteMiscRoutines.GetBenefitRequest(iRequestNbr);
  CheckCondition(StoredRequestData.Count > 0, 'Request data is empty');

  ResultRequestData := TRPCStruct.Create;

  for i := 0 to StoredRequestData.Count - 1 do
  begin
    if StoredRequestData.Values[i].Name = 'EE_NBR' then
    begin
      iNbr := StoredRequestData.Values[i].Value;
      ResultRequestData.AddItem(StoredRequestData.Values[i].Name, iNbr);
    end
    else if StoredRequestData.Values[i].Name = 'SIGNATURE_NBR' then
    begin
      if StoredRequestData.Values[i].Value = Null then
        iNbr := -1
      else
        iNbr := StoredRequestData.Values[i].Value;
      ResultRequestData.AddItem(StoredRequestData.Values[i].Name, iNbr);
    end
    else
    begin
      if not StartsWith(StoredRequestData.Values[i].Name, '#MAP#') then
      begin
        DataSet := IInterface(StoredRequestData.Values[i].Value) as IevDataset;
        if StoredRequestData.ValueExists('#MAP#' + StoredRequestData.Values[i].Name) then
          sMap := StoredRequestData.Value['#MAP#' + StoredRequestData.Values[i].Name]
        else
          sMap := '';
        DatasetData := DataSetToStruct(DataSet, sMap, True);
        rpcStruct := TRpcStruct.Create;
        rpcStruct.AddItem('DATASET', DatasetData);
        ResultRequestData.AddItem(StoredRequestData.Values[i].Name, rpcStruct);
      end;
    end;
  end;

  AReturn.AddItem(ResultRequestData);
end;

procedure TevXmlRPCServer.EE_StoreBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  RequestData: IRPCStruct;
  APIKey: IevAPIKey;
  iEENbr: integer;
  iRequestNbr: integer;
  OneDatasetData: IRPCStruct;
  DataSet: IevDataset;
  RequestStorage: IisListOfValues;
  i: integer;
  iNewRequestNbr: integer;
  sMap: String;
begin
// 'evo.ee.storeBenefitRequest(string sessionId, struct requestData, integer requestNbr)',
// if requestNbr < 0, it's a new request
// 'Return: integer RequestNbr'
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  RequestData := IRpcParameter(AParams[1]).AsStruct;
  iRequestNbr := IRpcParameter(AParams[2]).AsInteger;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiSelfServe, 'This call is allowed for ESS only');
  iEENbr := Abs(Context.UserAccount.InternalNbr);

  RequestStorage := TisListOfValues.Create;
  for i := 0 to RequestData.KeyList.Count - 1 do
  begin
    OneDatasetData := RequestData.Items[i].AsStruct;
    DataSet := StructToDataSet(OneDatasetData, sMap);
    RequestStorage.AddValue(RequestData.KeyList[i], DataSet); // data iself
    RequestStorage.AddValue('#MAP#' + RequestData.KeyList[i], sMap); // maping data for this dataset
  end;

  iNewRequestNbr := Context.RemoteMiscRoutines.StoreBenefitRequest(iEENbr, RequestStorage, iRequestNbr);
  AReturn.AddItem(iNewRequestNbr);
end;

procedure TevXmlRPCServer.GetBenefitRequestList(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iEENbr: integer;
  RequestsList: IisStringList;
  i: integer;
  ResultArray: IRPCArray;
  OneResultStruct: IRPCStruct;
  iNbr, iSign: integer;
  S: String;
begin
// 'evo.getBenefitRequestList(string sessionId, integer eeNbr)',
// 'Return: array of struct'
// each struct has two fields: 'Nbr' - EE_CHANGE_REQUEST_NBR and 'Sign' - SIGNATURE_NBR (-1 if NULL  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEENbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  RequestsList := Context.RemoteMiscRoutines.GetBenefitRequestList(iEENbr);
  ResultArray := TRPCArray.Create;

  for i := 0 to RequestsList.Count - 1 do
  begin
    S := RequestsList[i];
    iNbr := StrToInt(GetNextStrValue(S, ','));
    iSign := STrToInt(GetNextStrValue(S, ','));
    OneResultStruct := TRPCStruct.Create;
    OneResultStruct.AddItem('Nbr', iNbr);
    OneResultStruct.AddItem('Sign', iSign);
    ResultArray.AddItem(OneResultStruct);
  end;

  AReturn.AddItem(ResultArray);
end;

procedure TevXmlRPCServer.EE_SendEMailToHR(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  sSubject: String;
  sMessage: String;
  iEENbr: integer;
  APIKey: IevAPIKey;
  ResultList: IislistOfValues;
  ResultArray: IRPCArray;
  RpcAttachements: IRPCStruct;
  Attachements: IisListOfValues;
  BlobStream: IEvDualStream;
  BlobItem: IRpcStructItem;
  i: integer;
  S: String;
begin
// 'evo.ee.sendEMailToHR(string sessionId, BLOB Message, string Subject[, struct Attachements])',
// Attachements: name is filename, value is BLOB
// 'Return: array of strings. Each string contains: fisrt name and last name of HR person, whom is sent email'
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sMessage := IRpcParameter(AParams[1]).AsBase64Str;
  sSubject := IRpcParameter(AParams[2]).AsString;
  if AParams.Count > 3 then
    RpcAttachements := IRpcParameter(AParams[4]).AsStruct
  else
    RpcAttachements := nil;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiSelfServe, 'This call is allowed for ESS only');

  Attachements := TisListOfValues.Create;
  if Assigned(RpcAttachements) then
    for i := 0 to RpcAttachements.KeyList.Count - 1 do
    begin
      BlobStream := TEvDualStreamHolder.Create;
      BlobItem := RpcAttachements.Items[i];
      BlobItem.Base64StrSaveToStream(BlobStream.RealStream);
      Attachements.AddValue(RpcAttachements.KeyList[i], BlobStream);
    end;

  iEENbr := Abs(Context.UserAccount.InternalNbr);
  ResultList := Context.RemoteMiscRoutines.SendEMailToHR(iEENbr, sSubject, sMessage, Attachements);

  ResultArray := TRPCArray.Create;
  for i := 0 to ResultArray.Count - 1 do
  begin
    S := ResultList.Values[i].Value;
    S := GetNextStrValue(S, Chr(9));
    ResultArray.AddItem(S);
  end;

  AReturn.AddItem(ResultArray);
end;

function TevXmlRPCServer.StructToDataSet(const AdsXml: IRpcStruct; out AMap: String): IevDataSet;
var
  i, j: Integer;
  BlobData: IevDualStream;

  sTableName: string;
  sFieldName: string;
  FieldType: TFieldType;
  fType: Integer;
  fieldSize: Integer;
  fieldRequired: boolean;
  sColNbr: string;
  s: String;

  dsField: TField;
  rowXml: IRpcStruct;
  colXml: IRpcStruct;
  metaXml: IRpcStruct;
  valXml: IRpcStruct;
  dsXml: IRpcStruct;
begin
  AMap := '';
  dsXML := AdsXML;
  Result := TevDataSet.Create;
  if ( (dsXml.KeyList.Strings[0] = 'DATASET') and
      (dsXml.Items[0].AsStruct.KeyList.Strings[0] = 'meta') and
      (dsXml.Items[0].AsStruct.KeyList.Strings[1] = 'val') ) then
  begin
    dsXml := dsXml.Items[0].AsStruct;
    metaXml := dsXml.Items[0].AsStruct;
    for i := 0 to metaXML.Count-1 do
    begin
      colXml := metaXml.Items[i].AsStruct;
      sTableName := '';
      sFieldName := '';
      fType := 0;
      fieldSize := 0;
      fieldRequired := false;

      for j := 0 to colXml.Count-1 do
      begin
        if colXml.KeyList.Strings[j] = 'tb' then
          sTableName :=colXml.Items[j].AsString
        else if colXml.KeyList.Strings[j] = 'nm' then
          sFieldName :=colXml.Items[j].AsString
        else if colXml.KeyList.Strings[j] = 'tp' then
          fType := colXml.Items[j].AsInteger
        else if colXml.KeyList.Strings[j] = 'sz' then
          fieldSize :=colXml.Items[j].AsInteger
        else if colXml.KeyList.Strings[j] = 'nl' then
          fieldRequired := colXml.Items[j].AsInteger = 0;
      end; // for - field info

      CheckCondition(sFieldName <> '', 'Field name cannot be empty');
      if Trim(sTableName) <> '' then
      begin
        if Length(AMap) > 0 then
          AMap := AMap + ',' + sFieldName + '=' + sTableName
        else
          AMap := sFieldName + '=' + sTableName;
      end;

      FieldType := ftUnknown; // ???
      case fType of
        2004: FieldType := ftBlob;
        12:   FieldType := ftString;
        16:   FieldType := ftBoolean;
        4:    FieldType := ftInteger;
        2:    FieldType := ftFloat;
        91:   FieldType := ftDate;
        93:   FieldType := ftDateTime;  // not TimeStamp !
      end;
      Result.vclDataSet.FieldDefs.Add(sFieldName, FieldType, fieldSize, fieldRequired );
    end; // for - colimns

    Result.vclDataSet.Open;

    // get values and fill out DS
    if dsXml.Items[1].isStruct then // if non-empty DS
    begin
      rowXML := dsXml.Items[1].AsStruct;
      for i := 0 to rowXML.Count-1 do   // rows
      begin
        valXml := rowXml.Items[i].AsStruct;
        Result.Append;
        for j := 0 to valXml.Count-1 do  // columns
        begin
          sColNbr := valXml.KeyList.Strings[j];
          sFieldName := metaXml.Keys[sColNbr].AsStruct.Keys['nm'].AsString;
          dsField := Result.FieldByName(sFieldName);
              if not Assigned(valXml.Items[j]) then
                dsField.Clear
              else
                case dsField.DataType of
                  ftString:
                    dsField.Value := valXml.Items[j].AsString;
                  ftBoolean:
                    dsField.Value := valXml.Items[j].AsBoolean;
                  ftDateTime, ftDate, ftTimeStamp:
                    if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                      dsField.Clear
                    else
                      dsField.Value := valXml.Items[j].AsDateTime;
                  ftFloat, ftCurrency:
                    begin
                      if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                        dsField.Clear
                      else
                        dsField.Value := valXml.Items[j].AsFloat;
                    end;
                  ftInteger:
                    begin
                      if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                        dsField.Clear
                      else
                        dsField.Value := valXml.Items[j].AsInteger;
                    end;
                  ftBlob, ftMemo:
                  begin
                    if valXml.Items[j].IsString and (valXml.Items[j].AsString = '') then
                      dsField.Clear
                    else begin
                      BlobData := TEvDualStreamHolder.Create;
                      s := valXml.Items[j].AsBase64Str;
                      BlobData.WriteBuffer(s[1], Length(s));
                      BlobData.Position := 0;
                      TBlobField(dsField).LoadFromStream(BlobData.RealStream);
                      BlobData := nil;
                    end;
                  end
                end; // case
        end; // for - columns
        Result.Post;
      end; // for - rows
    end; // if non-empty DS
  end
  else
    raise Exception.Create('Wrong structure of data');
end;

procedure TevXmlRPCServer.GetEeExistingBenefits(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  iEeNbr: integer;
  bCurrentOnly: boolean;
  sContextID: String;
  ResultDataset: IevDataset;
  rpcStruct: IRpcStruct;
begin
// 'evo.getEeExistingBenefits(string sessionId, integer EENbr, [boolean CurrentOnly = True])',
// 'Return: dataset'
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEeNbr := IRpcParameter(AParams[1]).AsInteger;
  if AParams.Count > 2 then
    bCurrentOnly := IRpcParameter(AParams[2]).AsBoolean
  else
    bCurrentOnly := True;

  SetCurrentContext(sContextID);

  ResultDataset := Context.RemoteMiscRoutines.GetEeExistingBenefits(iEeNbr, bCurrentOnly);

  rpcStruct := TRpcStruct.Create;
  rpcStruct.AddItem('DATASET', DataSetToStruct(ResultDataset, ''));

  AReturn.AddItem(rpcStruct);
end;

procedure TevXmlRPCServer.DeleteBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iRequestNbr: integer;
begin
// 'evo.deleteBenefitRequest(string sessionId, integer requestNbr)',
// 'Return: nothing
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iRequestNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Context.RemoteMiscRoutines.DeleteBenefitRequest(iRequestNbr);
end;

procedure TevXmlRPCServer.EE_GetEeBenefitPackageAvaliableForEnrollment(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  iEeNbr: integer;
  sContextID: String;
  ResultList: IisListOfValues;
  ResultBenefitsDataset, ResultSubtypeDataset: IevDataset;
  DatasetStruct, ResultStruct: IRpcStruct;
begin
// 'evo.ee.getEeBenefitPackageAvaliableForEnrollment(string sessionId, integer EENbr)',
// 'Return: struct with two datasets'
// 'Fields: CO_BENEFITS, CO_BENEFIT_SUBTYPE; each field is dataset
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEeNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('getEeBenefitPackageAvaliableForEnrollment');
    ctx_Statistics.ActiveEvent.Properties.AddValue('EE_Nbr', iEENbr);
  end;

  try
    ResultList := Context.RemoteMiscRoutines.GetEeBenefitPackageAvaliableForEnrollment(iEeNbr, 0, '', False);
    ResultBenefitsDataset := IInterface(ResultList.Value['CO_BENEFITS']) as IevDataset;
    ResultSubtypeDataset := IInterface(ResultList.Value['CO_BENEFIT_SUBTYPE']) as IevDataset;

    ResultStruct := TRPCStruct.Create;

    DatasetStruct := TRpcStruct.Create;
    DatasetStruct.AddItem('DATASET', DataSetToStruct(ResultBenefitsDataset, ''));
    ResultStruct.AddItem('CO_BENEFITS', DatasetStruct);
    DatasetStruct := TRpcStruct.Create;
    DatasetStruct.AddItem('DATASET', DataSetToStruct(ResultSubtypeDataset, ''));
    ResultStruct.AddItem('CO_BENEFIT_SUBTYPE', DatasetStruct);

    AReturn.AddItem(ResultStruct);
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

procedure TevXmlRPCServer.GetSummaryBenefitEnrollmentStatus(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  rpcEENbrs: IRpcArray;
  EENbrList: IisStringList;
  sType: String;
  sContextID: String;
  ResultDataset: IevDataset;
  rpcStruct: IRpcStruct;
  i: integer;
  APIkey: IevAPIKey;
begin
// 'evo.getSummaryBenefitEnrollmentStatus(string sessionId, array EENbrs, string Type)',
// Type could be 'C' - completed, 'P' - in progress, 'N' - no elections
// 'Return: dataset'
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  rpcEENbrs := IRpcParameter(AParams[1]).AsArray;
  sType := IRpcParameter(AParams[2]).AsString;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiWebClient, 'This call is allowed for Web Client only');

  EENbrList := TisStringList.Create;
  for i := 0 to rpcEENbrs.Count - 1 do
    EENbrList.Add(IntToStr(rpcEENbrs.Items[i].AsInteger));

  if ctx_Statistics.Enabled then
  begin
    ctx_Statistics.BeginEvent('getSummaryBenefitEnrollmentStatus');
    ctx_Statistics.ActiveEvent.Properties.AddValue('EE Nbrs', EENbrList.Text);
    ctx_Statistics.ActiveEvent.Properties.AddValue('Type', sType);
  end;

  try
    ResultDataset := Context.RemoteMiscRoutines.GetSummaryBenefitEnrollmentStatus(EENbrList, sType);

    rpcStruct := TRpcStruct.Create;
    rpcStruct.AddItem('DATASET', DataSetToStruct(ResultDataset, ''));

    AReturn.AddItem(rpcStruct);
  finally
    if ctx_Statistics.Enabled then
      ctx_Statistics.EndEvent;
  end;
end;

procedure TevXmlRPCServer.SB_RevertBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iRequestNbr: integer;
begin
// 'evo.sb.revertBenefitRequest(string sessionId, integer requestNbr)',
// 'Return: nothing
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iRequestNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Context.RemoteMiscRoutines.RevertBenefitRequest(iRequestNbr);
end;

procedure TevXmlRPCServer.EE_SubmitBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iRequestNbr: integer;
  SignatureDataList: IisListOfValues;
  SignatureDataStruct: IRpcStruct;
  APIKey: IevAPIKey;
  sMessage: String;
begin
// 'evo.ee.submitBenefitRequest(string sessionId, integer requestNbr, struct signatureData)',
// 'Return: nothing
// signatureData contains such fields:
// string IP - IP address
// datetime SubmitDate (ignored for now)
// blob Message (ignored for now)
  SignatureDataList := TisListOfValues.Create;

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iRequestNbr := IRpcParameter(AParams[1]).AsInteger;
  SignatureDataStruct := IRpcParameter(AParams[2]).AsStruct;
  SignatureDataList.AddValue('IP', SignatureDataStruct.Keys['IP'].AsString);
  SignatureDataList.AddValue('SubmitDate', SignatureDataStruct.Keys['SubmitDate'].AsString);
  sMessage := SignatureDataStruct.Keys['Message'].AsBase64Str;
  SignatureDataList.AddValue('Message', sMessage);

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);
  CheckCondition(APIKey.APIKeyType = apiSelfServe, 'This call is allowed for ESS only');

  Context.RemoteMiscRoutines.SubmitBenefitRequest(iRequestNbr, SignatureDataList);
end;

procedure TevXmlRPCServer.GetBenefitRequestConfirmationReport(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iRequestNbr: integer;
  APIKey: IevAPIKey;
  ResStream: IisStream;
begin
// 'evo.getBenefitRequestConfirmationReport(string sessionId, integer requestNbr)',
// 'Return: BLOB
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iRequestNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  APIKey := GetLicenseKeyFromContext(sContextID);

  ResStream := Context.RemoteMiscRoutines.GetBenefitRequestConfirmationReport(iRequestNbr);
  ResStream.Position := 0;
  AReturn.AddItemBase64StrFromStream(ResStream.RealStream);
end;

procedure TevXmlRPCServer.SB_SendBenefitEMailToEE(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  sSubject: String;
  sMessage: String;
  sCategoryType: String;
  iEENbr: integer;
  iRequestNbr: integer;
begin
// 'evo.sb.sendBenefitEMailToEE(string sessionId, integer EENbr, string Subject, BLOB Message[, int Requestnbr], [string NotElectedCategory])',
// 'Return: nothing'
// where you must pass Requestnbr > 0 or NotElectedCategory
// NotElectedCategory = category type for not elected benefits  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEENbr := IRpcParameter(AParams[1]).AsInteger;
  sSubject := IRpcParameter(AParams[2]).AsString;
  sMessage := IRpcParameter(AParams[3]).AsBase64Str;

  if AParams.Count > 4 then
    iRequestNbr := IRpcParameter(AParams[4]).AsInteger
  else
    iRequestNbr := -1;

  if AParams.Count > 5 then
    sCategoryType := Trim(IRpcParameter(AParams[5]).AsString)
  else
    sCategoryType := '';

  SetCurrentContext(sContextID);

  Context.RemoteMiscRoutines.SendBenefitEmailToEE(iEENbr, sSubject, sMessage, iRequestNbr, sCategoryType);
end;

procedure TevXmlRPCServer.GetEmptyBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  i: integer;
  DataSet: IevDataset;
  ResultRequestData: IRPCStruct;
  DatasetData: IRPCStruct;
  rpcStruct: IRpcStruct;
  StoredRequestData: IisListOfValues;
  sMap: String;
begin
// 'evo.getEmptyBenefitRequest(string sessionId)',
// 'Return: struct - each field of struct is dataset
// EE_HR_BENEFIT_REQ_META_DATASET dataset will have one record with 'REQUEST_STATUS' = 'P'
// EE_HR_BENEFIT_REQ_DATASET, EE_HR_BENEFIT_REQ_DEPENDENT_DATASET and EE_HR_BENEFIT_REQ_BENEFICIARY_DATASET datasets will be empty
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;

  SetCurrentContext(sContextID);

  StoredRequestData := Context.RemoteMiscRoutines.GetEmptyBenefitRequest;
  CheckCondition(StoredRequestData.Count > 0, 'Request data is empty');

  ResultRequestData := TRPCStruct.Create;

  for i := 0 to StoredRequestData.Count - 1 do
  begin
    if not StartsWith(StoredRequestData.Values[i].Name, '#MAP#') then
    begin
      DataSet := IInterface(StoredRequestData.Values[i].Value) as IevDataset;
      if StoredRequestData.ValueExists('#MAP#' + StoredRequestData.Values[i].Name) then
        sMap := StoredRequestData.Value['#MAP#' + StoredRequestData.Values[i].Name]
      else
        sMap := '';
      DatasetData := DataSetToStruct(DataSet, sMap, True);
      rpcStruct := TRpcStruct.Create;
      rpcStruct.AddItem('DATASET', DatasetData);
      ResultRequestData.AddItem(StoredRequestData.Values[i].Name, rpcStruct);
    end;
  end;

  AReturn.AddItem(ResultRequestData);
end;

procedure TevXmlRPCServer.SB_CloseBenefitEnrollment(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  sCathegoryType: String;
  iEENbr: integer;
  iRequestNbr: integer;
begin
// 'evo.sb.closeBenefitEnrollment(string sessionId, integer EENbr, string CathegoryType[, int Requestnbr])',
// 'Return: nothing'
  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iEENbr := IRpcParameter(AParams[1]).AsInteger;
  sCathegoryType := IRpcParameter(AParams[2]).AsString;
  if AParams.Count > 3 then
    iRequestNbr := IRpcParameter(AParams[3]).AsInteger
  else
    iRequestNbr := -1;

  SetCurrentContext(sContextID);

  Context.RemoteMiscRoutines.CloseBenefitEnrollment(iEENbr, sCathegoryType, iRequestNbr);
end;

procedure TevXmlRPCServer.SB_ApproveBenefitRequest(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iRequestNbr: integer;
begin
// 'evo.sb.approveBenefitRequest(string sessionId, int Requestnbr)',
// 'Return: nothing'
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iRequestNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Context.RemoteMiscRoutines.ApproveBenefitRequest(iRequestNbr);
end;

function TevXmlRPCServer.CheckIfTermsOfUseAccepted(const AUserId: String; const AAPIKey: IevAPIKey): String;
var
  sTermsOfUse, sOldTermsOfUse: String;
  qSB, qSBHist: IevQuery;
  DateOfAccept: TDateTime;
begin
  // function return empty sting if user has accepted Terms of Use or if there's no Terms of Use
  // if user hasn't accepted or there's an updated Terms of use, it will return text of terms of use
  Result := '';

  if not (AAPIKey.APIKeyType in [apiWebClient, apiSelfServe]) then
    exit;

  if AAPIKey.APIKeyType = apiWebClient then
  begin
    qSB := TevQuery.Create('select WC_TERMS_OF_USE as Terms from SB a where {AsOfNow<a>} ');
    CheckCondition(Pos('.', AUserId) = 0, 'Wrong format of userid for WebClient: ' + AUserId);
  end;

  if AAPIKey.APIKeyType = apiSelfServe then
  begin
    qSB := TevQuery.Create('select ESS_TERMS_OF_USE as Terms from SB a where {AsOfNow<a>} ');
    CheckCondition(Pos('.', AUserId) > 0, 'Wrong format of userid for ESS: ' + AUserId);
  end;

  qSB.Execute;
  if (qSB.Result.RecordCount = 0) or (qSB.Result['Terms'] = null) then
    exit;

  sTermsOfUse := TBlobField(qSB.Result.FieldByName('Terms')).AsString;
  if Trim(StringReplace(sTermsOfUse, #13#10, '  ', [rfReplaceAll])) = '' then
    exit;

// TODO: Plymouth
// This logic must be redone as it relies on audit data
  FTermsOfUse.Lock;
  try
    if FTermsOfUse.ValueExists(AUserId) then
    begin
      if FTermsOfUse.Value[AUserId] = null then
      begin
        FTermsOfUse.DeleteValue(AUserId);
        Result := sTermsOfUse;
        StoreTermsOfUse;
        exit;
      end
      else
      begin
        DateOfAccept := FTermsOfUse.Value[AUserId];
        if AAPIKey.APIKeyType = apiWebClient then
          qSBHist := TevQuery.Create('select WC_TERMS_OF_USE as Terms from SB where {AsOfDate<SB>} ')
        else
          qSBHist := TevQuery.Create('select ESS_TERMS_OF_USE as Terms from SB where {AsOfDate<SB>}');
        qSBHist.Params.AddValue('StatusDate', DateOfAccept);
        qSBHist.Execute;
        if (qSBHist.Result.RecordCount = 0) or (qSBHist.Result['Terms'] = null) then
        begin
          FTermsOfUse.DeleteValue(AUserId);
          Result := sTermsOfUse;
          StoreTermsOfUse;
          exit;
        end;
        sOldTermsOfUse := TBlobField(qSBHist.Result.FieldByName('Terms')).AsString;
        if sOldTermsOfUse = sTermsOfUse then
          exit
        else
        begin
          FTermsOfUse.DeleteValue(AUserId);
          Result := sTermsOfUse;
          StoreTermsOfUse;
          exit;
        end;
      end;
    end
    else
    begin
      Result := sTermsOfUse;
      exit;
    end;
  finally
    FTermsOfUse.Unlock;
  end;
end;

procedure TevXmlRPCServer.StoreTermsOfUse;
var
  tmpStream: IisStream;
begin
  try
    FTermsOfUse.Lock;
    try
      if FileExists(FTermOfseFilename) then
        RenameFile(FTermOfseFilename, ChangeFileExt(FTermOfseFilename, '.bak'));
      tmpStream := TisStream.CreateFromNewFile(FTermOfseFilename);
      FTermsOfUse.WriteToStream(tmpStream);
    except
      on E: Exception do
        GlobalLogFile.AddEvent(etError, 'API', 'Cannot save terms of use information to "' + FTermOfseFilename
          + '" file', E.Message + #13#10 + GetErrorCallStack(E));
    end;
  finally
    FTermsOfUse.Unlock;
  end;
end;

procedure TevXmlRPCServer.UserAcceptedTermsOfUse(const AUserId: String; const ATimeStamp: TDateTime);
begin
  FTermsOfUse.Lock;
  try
    FTermsOfUse.AddValue(AUserId, ATimeStamp);
    StoreTermsOfUse;
  finally
    FTermsOfUse.Unlock;
  end;
end;

procedure TevXmlRPCServer.AcceptTermsOfUse(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  bAccepted: Boolean;
  dNow: TDateTime;
begin
//  evo.acceptTermsofUse(string sessionId, boolean Accepted)
//  Return: nothing
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);
  sContextID := IRpcParameter(AParams[0]).AsString;
  bAccepted := IRpcParameter(AParams[1]).AsBoolean;

  if bAccepted then
  begin
    dNow := Mainboard.TimeSource.GetDateTime;
    SetCurrentContext(sContextID, touSetAccepted);
    if Context.UserAccount.InternalNbr < 0 then
      UserAcceptedTermsOfUse(IntToStr(ctx_DataAccess.ClientId) + '.' + IntToStr(Context.UserAccount.InternalNbr), dNow) // ESS
    else
      UserAcceptedTermsOfUse(IntToStr(Context.UserAccount.InternalNbr), dNow); // WebClient
  end
  else
    Disconnect(AParams, AReturn, AMethodName);
end;

procedure TevXmlRPCServer.GetSequrityExtQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  Context: IevContext;
  APIKey : IevAPIKey;
  sDomain, sGUID: String;
  AvaliableQuestions: IisStringList;
  ResultAvaliableQuestions: IRpcArray;
  i: integer;
begin
// evo.getSecurityExtQuestions(string LicenseKey [, string evoDomain])
// Return array of strings: list of all avaliable extended questions for WebClient and ESS
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sGUID := IRpcParameter(AParams[0]).AsString;
  if AParams.Count > 1 then
    sDomain := IRpcParameter(AParams[1]).AsString
  else
    sDomain := '';

  Context := Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');

  APIKey := FindAPIKey(sGUid);
  APICheckCondition(Assigned(APIKey), WrongAPIKeyException);
  APICheckCondition((APIKey.APIKeyType = apiSelfServe) or (APIKey.APIKeyType = apiWebClient), WrongLicenseException);

  AvaliableQuestions := Context.Security.GetAvailableExtQuestions('');
  ResultAvaliableQuestions := TRpcArray.Create;
  for i := 0 to AvaliableQuestions.Count - 1 do
    ResultAvaliableQuestions.AddItem(AvaliableQuestions[i]);

  AReturn.AddItem(ResultAvaliableQuestions);
end;

procedure TevXmlRPCServer.SetUserExtQuestions(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  QAArray: IRpcArray;
  QAStruct: IRpcStruct;
  QAList: IisListOfValues;
  sQuestion, sAnswer: String;
  i: integer;
begin
// evo.setUserExtQuestions(string sessionid, array of struct)
// Returns nothing
// Struct contains two fields: 'Q' and 'A'. Dimention of array must be 2.
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  QAArray := IRpcParameter(AParams[1]).AsArray;

  SetCurrentContext(sContextID, touDoNotCheck);

  QAList := TisListOfValues.Create;
  for i := 0 to QAArray.Count - 1 do
  begin
    QAStruct := QAArray.Items[i].AsStruct;
    sQuestion := QAStruct.Keys['Q'].AsString;
    sAnswer := QAStruct.Keys['A'].AsString;
    QAList.AddValue(sQuestion, sAnswer);
  end;

  Context.Security.SetExtQuestions(QAList);
end;

procedure TevXmlRPCServer.StoreUsersCookies;
var
  tmpStream: IisStream;
begin
  try
    FUsersCookies.Lock;
    try
      if FileExists(FUsersCookiesFilename) then
        RenameFile(FUsersCookiesFilename, ChangeFileExt(FUsersCookiesFilename, '.bak'));
      tmpStream := TisStream.CreateFromNewFile(FUsersCookiesFilename);
      FUsersCookies.WriteToStream(tmpStream);
    except
      on E: Exception do
        GlobalLogFile.AddEvent(etError, 'API', 'Cannot save users cookies to "' + FUsersCookiesFilename
          + '" file', E.Message + #13#10 + GetErrorCallStack(E));
    end;
  finally
    FUsersCookies.Unlock;
  end;
end;

procedure TevXmlRPCServer.ApplyManyDataChangePackets(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID : String;
  rpcInputPackets: IRpcArray;
  rpcInputPacketEnvelope: IRpcStruct;
  rpcInputPacket: IRpcArray;
  i, j: integer;
  dAsOfDate: TDateTime;
  DataSourcesList: IisStringList;
  DDTable: TddTableClass;
  DBTypes: TevDBTypes;
  rpcRowStruct: IRpcStruct;
  sDataSource: String;
  EEConnectParams: IInterfaceList;
  EEParam: IRpcParameter;
  ReturnPlaceholder: IRpcReturn;
  ResultMap, OnePacketMap: IRpcStruct;
begin
// evo.applyManyDataChangePackets(string session Id, array of struct of ... dataChangePacket)
// returns structure, the first field contains old negative value, the second field contains new positive value
// Where dataChangePacket has suct structure:
// On the top it is an array. Each element of array is a struct which contains one data change packet.
// Top level struct contains such fields:
// 'S' - lower level struct
// 'A' - AsfDate: datetime. This field should be missed if changes are as of now
// Lower level struct contains fields:
//  'T' - change type, possible values: 'I' - insert, 'D' - delete, 'U' - update. Value is required.
//  'D' - datasource name (table name). Value is required
//  'K' - record_nbr, required for updates and deletes. Not used for inserts. FOR INSERT you should provide negative value of proper '_NBR' field in list of fields values. See 'F'.
//  'F' - structure which is a list of new values for fields. Structure's field name is table's field name, structure's value is new field value. Please pass '#NULL#' for update current value to the NULL value.
//  for deleting 'F' could be skipped
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  rpcInputPackets := IRpcParameter(AParams[1]).AsArray;
  CheckCondition(rpcInputPackets.Count > 0, 'Input packets list cannot be empty!');

  // preparin list of datasources and finding list of database types
  DataSourcesList := TisStringList.Create;
  for i := 0 to rpcInputPackets.Count - 1 do
  begin
    rpcInputPacketEnvelope := rpcInputPackets.Items[i].AsStruct;
    rpcInputPacket := rpcInputPacketEnvelope.Keys['S'].AsArray;
    for j := 0 to rpcInputPacket.Count - 1 do
    begin
      rpcRowStruct := rpcInputPacket.Items[j].AsStruct;
      sDataSource := AnsiUpperCase(rpcRowStruct.Keys['D'].AsString);
      if DataSourcesList.IndexOf(sDataSource) = -1 then
        DataSourcesList.Add(sDataSource);
    end;  // for j
  end;  // for i
  CheckCondition(DataSourcesList.Count > 0, 'Tables list cannot be empty');
  DBTypes := [];
  for i := 0 to DataSourcesList.Count - 1 do
  begin
    DDTable := GetddTableClassByName(DataSourcesList[i]);
    CheckCondition(Assigned(DDTable), 'Cannot find table class by table name: ' + DataSourcesList[i]);
    if (DDTable.GetDBType = dbtClient) and not (dbtClient in DBTypes) then
      DBTypes := DBTypes + [dbtClient]
    else if (DDTable.GetDBType = dbtBureau) and not (dbtBureau in DBTypes) then
      DBTypes := DBTypes + [dbtBureau]
    else if DDTable.GetDBType in [dbtUnknown, dbtUnspecified, dbtSystem, dbtTemporary] then
      raise Exception.Create('API allows to change client and service bureau databases only');
  end;

  // processing
  ResultMap := TRPCStruct.Create;
  ctx_DataAccess.StartNestedTransaction(DBTypes);
  try
    for i := 0 to rpcInputPackets.Count - 1 do
    begin
      rpcInputPacketEnvelope := rpcInputPackets.Items[i].AsStruct;
      rpcInputPacket := rpcInputPacketEnvelope.Keys['S'].AsArray;
      if rpcInputPacketEnvelope.KeyExists('A') then
        dAsOfDate := rpcInputPacketEnvelope.Keys['A'].AsDateTime
      else
        dAsOfDate := 0;

      EEConnectParams := TInterfaceList.Create;

      EEParam := TRpcParameter.Create;
      EEParam.AsString := sContextID;
      EEConnectParams.Add(EEParam);

      EEParam := TRpcParameter.Create;
      EEParam.AsArray := rpcInputPacket;
      EEConnectParams.Add(EEParam);

      if dAsOfDate <> 0 then
      begin
        EEParam := TRpcParameter.Create;
        EEParam.AsDateTime := dAsOfDate;
        EEConnectParams.Add(EEParam);
      end;

      ReturnPlaceholder := TRpcReturn.Create;
      ApplyDataChangePacket(EEConnectParams, ReturnPlaceholder, AMethodName);
      OnePacketMap := ReturnPlaceholder.Items[0].AsStruct;

      for j := 0 to OnePacketMap.Count - 1 do
      begin
        if not ResultMap.KeyExists(OnePacketMap.Items[j].Name) then
          ResultMap.AddItem(OnePacketMap.Items[j].Name, OnePacketMap.Items[j].AsInteger);
      end;  // for j
    end;  // for i

    ctx_DataAccess.CommitNestedTransaction;
  except
    on E: Exception do
    begin
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;
  AReturn.AddItem(ResultMap);
end;

procedure TevXmlRPCServer.RememberMe(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId: String;
  sCookieId, sComputerId, sToken, sUserComputerName, sUserName: String;
  ResultStruct: IRpcStruct;
  CookieInfo, ExistingCookie: IUserRegisteredComputerInfo;
  i: integer;
begin
// evo.rememberMe(string sessionId[, string usersComputerName])
// returns struct with 3 fields:
// 'UserId' - string
// 'ComputerId' - string
// 'Token' - string
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID, touDoNotCheck);

  if AParams.Count > 1 then
    sUserComputerName := IRpcParameter(AParams[1]).AsString
  else
    sUserComputerName := '';

  sUserName := AnsiUpperCase(EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
  sComputerId := GetUniqueID;
  sToken := GetUniqueID;

  FUsersCookies.Lock;
  try
    CookieInfo := TUserRegisteredComputerInfo.Create;
    CookieInfo.SetUserName(sUserName);
    CookieInfo.SetComputerId(sComputerId);
    CookieInfo.SetToken(sToken);
    CookieInfo.SetLastAccessdate(Now);
    CookieInfo.SetUserComputerName(sUserComputerName);

    sCookieId := '';
    for i := 0 to FUsersCookies.Count - 1 do
    begin
      ExistingCookie := IInterface(FUsersCookies.Values[i].Value) as IUserRegisteredComputerInfo;
      if ExistingCookie.GetUserName = sUserName then
      begin
        sCookieId := ExistingCookie.GetCookieId;
        break;
      end;
    end;
    if sCookieId = '' then
      sCookieId := GetUniqueID;

    CookieInfo.SetCookieId(sCookieId);
    FUsersCookies.AddValue(sCookieId + #13 + sComputerId, CookieInfo);
    StoreUsersCookies;
  finally
    FUsersCookies.Unlock;
  end;

  ResultStruct := TRpcStruct.Create;
  ResultStruct.AddItem('UserId', sCookieId);
  ResultStruct.AddItem('ComputerId', sComputerId);
  ResultStruct.AddItem('Token', sToken);

  AReturn.AddItem(ResultStruct);
end;

function TevXmlRPCServer.FindAndUpdateCookie(const ACookieId, AComputerId, AToken: String): String;
var
  Cookie: IUserRegisteredComputerInfo;
begin
  // if cookies doesn't exist function returns EMPTY string
  // if cookies exists, function returns NEW Token
  Result := '';

  FUsersCookies.Lock;
  try
    if FUsersCookies.ValueExists(ACookieId + #13 + AComputerId) then
    begin
      Cookie := IInterface(FUsersCookies.Value[ACookieId + #13 + AComputerId]) as IUserRegisteredComputerInfo;
      if Cookie.GetToken = AToken then
      begin
        if IsUserCookieExpired(Cookie, Now) then
          FUsersCookies.DeleteValue(ACookieId + #13 + AComputerId)
        else
        begin
          Result := GetUniqueID;
          Cookie.SetToken(Result);
          Cookie.SetLastAccessdate(Now);
        end;
        StoreUsersCookies;
      end;  
    end;
  finally
    FUsersCookies.Unlock;
  end;
end;

procedure TevXmlRPCServer.ForgetMe(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId: String;
  sUserComputerName: String;
  Cookie: IUserRegisteredComputerInfo;
  ResultArray: IRpcArray;
  i, cnt: integer;
  sUserName: String;
begin
// evo.forgetMe(string sessionId, string UserComputerName)
// returns nothing
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  sUserComputerName := IRpcParameter(AParams[1]).AsString;
  sUserName := AnsiUpperCase(EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));

  FUsersCookies.Lock;
  try
    ResultArray := TRpcArray.Create;

    cnt := 0;
    for i := FUsersCookies.Count - 1 downto 0 do
    begin
      Cookie := IInterface(FUsersCookies.Values[i].Value) as IUserRegisteredComputerInfo;
      if (Cookie.GetUserComputerName = sUserComputerName) and (Cookie.GetUserName = sUserName) then
      begin
        FUsersCookies.Delete(i);
        Inc(cnt);
      end;
    end;

    if cnt > 0 then
      StoreUsersCookies;
  finally
    FUsersCookies.Unlock;
  end;
end;

procedure TevXmlRPCServer.GetListOfRegisteredComputers(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextId: String;
  ResultArray: IRpcArray;
  Cookie: IUserRegisteredComputerInfo;
  sUserName: String;
  sComputerName: String;
  dExpirationDate: TDateTime;
  ResultStruct: IRpcStruct;
  i: integer;
begin
// evo.getListOfRegisteredComputers(string sessionId)
// returns array of structs
// fields: 'usersComputerName' (string), 'ExpirationDate' (datetime)
  APICheckCondition(AParams.Count >= 1, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  SetCurrentContext(sContextID);

  sUserName := AnsiUpperCase(EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));

  FUsersCookies.Lock;
  try
    ResultArray := TRpcArray.Create;

    for i := 0 to FUsersCookies.Count - 1 do
    begin
      Cookie := IInterface(FUsersCookies.Values[i].Value) as IUserRegisteredComputerInfo;
      if Cookie.GetUserName = sUserName then
      begin
        ResultStruct := TRpcStruct.Create;
        sComputerName := Cookie.GetUserComputerName;
        dExpirationDate := Cookie.GetLastAccessDate + CookieExpirationDays;
        ResultStruct.AddItem('usersComputerName', sComputerName);
        ResultStruct.AddItemDateTime('ExpirationDate', dExpirationDate);
        ResultArray.AddItem(ResultStruct);
      end;
    end;
  finally
    FUsersCookies.Unlock;
  end;

  AReturn.AddItem(ResultArray);
end;

function TevXmlRPCServer.IsUserCookieExpired(const ACookie: IUserRegisteredComputerInfo;
  const ANow: TDateTime): boolean;
begin
  Result := (ANow - ACookie.GetLastAccessDate) > CookieExpirationDays;
end;

procedure TevXmlRPCServer.CleanUpCookiesForUser(const AUserName: String);
var
  i: integer;
  Cookie: IUserRegisteredComputerInfo;
  cnt: integer;
begin
  FUsersCookies.Lock;
  try
    cnt := 0;
    for i := FUsersCookies.Count - 1 downto 0 do
    begin
      Cookie := IInterface(FUsersCookies.Values[i].Value) as IUserRegisteredComputerInfo;
      if Cookie.GetUserName = AUserName then
      begin
        FUsersCookies.Delete(i);
        Inc(cnt);
      end;
    end;

    if cnt > 0 then
      StoreUsersCookies;
  finally
    FUsersCookies.Unlock;
  end;
end;

function TevXmlRPCServer.GetCookieId(const AUserName: String): String;
var
  i: integer;
  Cookie: IUserRegisteredComputerInfo;
begin
  Result := '';

  FUsersCookies.Lock;
  try
    for i := 0 to FUsersCookies.Count - 1 do
    begin
      Cookie := IInterface(FUsersCookies.Values[i].Value) as IUserRegisteredComputerInfo;
      if Cookie.GetUserName = AUserName then
      begin
        Result := Cookie.GetCookieId;
        exit;
      end;
    end;
  finally
    FUsersCookies.Unlock;
  end;
end;

procedure TevXmlRPCServer.SB_GetSwipeclockOneTimeURL(const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID: String;
  iCoNbr: integer;
  sURL: String;
begin
// 'evo.sb.getSwipeclockOneTimeURL(string sessionId, integer CoNbr)',
// 'Return: String'
  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iCoNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  sURL := Context.SingleSignOn.GetSwipeclockOneTimeURL(iCoNbr);
  AReturn.AddItem(sURL);
end;


procedure TevXmlRPCServer.SB_GetManualTaxesListForCheck(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  iPrCheckNbr: Integer;
  rpcStruct: IRpcStruct;
  dsResult: IevDataset;
begin
//  evo.sb.getManualTaxesListForCheck(string sessionId, integer PrCheckNbr)
//  Return: struct with DataSet'

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  iPrCheckNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  dsResult := Context.RemoteMiscRoutines.GetManualTaxesListForCheck(iPrCheckNbr);

  rpcStruct := TRpcStruct.Create;
  rpcStruct.AddItem('DATASET', DataSetToStruct(dsResult, ''));

  AReturn.AddItem(rpcStruct);
end;

procedure TevXmlRPCServer.SB_GetMRCOneTimeURL( const AParams: IInterfaceList; const AReturn: IRpcReturn;  const AMethodName: String);
var
  sContextID : String;
  CONBR: Integer;
  sURL: String;
begin
//  evo.sb.getMRCOneTimeURL(string sessionId, integer CONBR )
//  Return: string

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  CONBR := IRpcParameter(AParams[1]).AsInteger;
  SetCurrentContext(sContextID);

  sURL := Context.SingleSignOn.GetMRCOneTimeURL(CONBR);
  AReturn.AddItem(sURL);

end;

procedure TevXmlRPCServer.GetEvBlobData(const AParams: IInterfaceList; const AReturn: IRpcReturn; const AMethodName: String);
var
  iTableNBR: Integer;
  sContextID, sTableName, sBlobFieldName: String;
  Tbl : IevTable;
  BlobStream : IisStream;
begin
//  evo.getEvBlobData(string sessionId, string TableName, integer TableNbr, string BlobFieldName)
//  Return: base64

  APICheckCondition(AParams.Count >= 4, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  sTableName := IRpcParameter(AParams[1]).AsString;
  iTableNBR := IRpcParameter(AParams[2]).AsInteger;
  sBlobFieldName := IRpcParameter(AParams[3]).AsString;
  SetCurrentContext(sContextID);

  Tbl := TevTable.Create(sTableName, sBlobFieldName, sTableName + '_NBR =' + IntTostr(iTableNbr));
  Tbl.Open;
  if Tbl.RecordCount = 1 then
    BlobStream := Tbl.GetBlobData(sBlobFieldName);

  if Assigned(BlobStream) and (BlobStream.Size > 0) then
  begin
    AReturn.AddItemBase64StrFromStream(BlobStream.RealStream);
  end
  else
    raise Exception.Create('GetEvBlobData function returned no data');
end;

procedure TevXmlRPCServer.SB_GetSysReportByNbr(const AParams: IInterfaceList;
  const AReturn: IRpcReturn; const AMethodName: String);
var
  sContextID: String;
  ReportNbr: integer;
  Res: IevDualStream;
begin
//  evo.sb.getSysReportByNbr(string sessionId, integer ReportNbr)
//  Return: base64 - RWR report from Evolution system level reports

  APICheckCondition(AParams.Count >= 2, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  ReportNbr := IRpcParameter(AParams[1]).AsInteger;

  SetCurrentContext(sContextID);

  Res := Context.RemoteMiscRoutines.GetSysReportByNbr(ReportNbr);

  if Assigned(Res) and (Res.Size > 0) then
  begin
    AReturn.AddItemBase64StrFromStream(Res.RealStream);
  end
  else
    raise Exception.Create('GetSysReportByNbr function returned no data');
end;

procedure TevXmlRPCServer.SB_GetReportFileByNbrAndType(
  const AParams: IInterfaceList; const AReturn: IRpcReturn;
  const AMethodName: String);
var
  sContextID, ReportType: String;
  ReportNbr: integer;
  Res: IevDualStream;
begin
//  evo.sb.getReportFileByNbrAndType(string sessionId, integer ReportNbr, string ReportType)
//  Return: base64 - RWR report from Evolution system level reports

  APICheckCondition(AParams.Count >= 3, WrongSignatureException);

  sContextID := IRpcParameter(AParams[0]).AsString;
  ReportNbr := IRpcParameter(AParams[1]).AsInteger;
  ReportType := IRpcParameter(AParams[2]).AsString;

  SetCurrentContext(sContextID);

  Res := Context.RemoteMiscRoutines.GetReportFileByNbrAndType(ReportNbr, ReportType);

  if Assigned(Res) and (Res.Size > 0) then
  begin
    AReturn.AddItemBase64StrFromStream(Res.RealStream);
  end
  else
    raise Exception.Create('GetReportFileByNbrAndType function returned no data');
end;

{ TContextObjFromPool }

constructor TContextObjFromPool.Create(const AName: String);
begin
  inherited;
  SetName(AName);
end;

destructor TContextObjFromPool.Destroy;
begin
  inherited;
end;

function TContextObjFromPool.GetContext: IevContext;
begin
  result := FContext;
end;

function TContextObjFromPool.GetLicenseKey: IevAPIKey;
begin
  result := FLicenseKey;
end;

function TContextObjFromPool.GetTermsOfUseAccepted: boolean;
begin
  Result := FTermsOfUseAccepted;
end;

procedure TContextObjFromPool.SetContext(AContext: IevContext);
begin
  FContext := AContext;
end;

procedure TContextObjFromPool.SetLicenseKey(ALicenseKey: IevAPIKey);
begin
  FLicenseKey := ALicensekey;
end;

procedure TContextObjFromPool.SetTermsOfUseAccepted(const AValue: boolean);
begin
  FTermsOfUseAccepted := AValue;
end;

{ TevAAController }

function TevAAController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevAAControllerDatagramDispatcher.Create;
end;

procedure TevAAController.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(AAController_Port));
  SetIPAddress('127.0.0.1');
end;


{ TevAAControl }

function TevAAControl.GetFilteredLogEvents(
  const AQuery: TEventQuery): IisParamsCollection;
var
  L: IisList;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  i: Integer;
begin
  L := GlobalLogFile.GetEvents(AQuery);
  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevAAControl.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  Result := TisListOfValues.Create;
  Event:= GlobalLogFile.Items[EventNbr];
  Result.AddValue('EventNbr', Event.Nbr);
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);
end;

function TevAAControl.GetLogEvents(const PageNbr, EventsPerPage: Integer;
  out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  GlobalLogFile.Lock;
  try
    L := GlobalLogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := GlobalLogFile.Count div EventsPerPage;
    if GlobalLogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

procedure TevAAControl.GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
begin
  APurgeRecThreshold := GlobalLogFile.PurgeRecThreshold;
  APurgeRecCount := GlobalLogFile.PurgeRecCount;
end;

function TevAAControl.GetSettings: IisListOfValues;
begin
  Result := TisListOfValues.Create;
  Result.AddValue('RequestBroker', Mainboard.XmlRPCServer.GetRBHost);
  Result.AddValue('XMLRPCPort', Mainboard.XmlRPCServer.GetPort);
  Result.AddValue('XMLRPCSSL', Mainboard.XmlRPCServer.SSLEnable);  
end;

function TevAAControl.GetStackInfo: String;
begin
  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      Result := GetStack(E);
  end;
end;

procedure TevAAControl.SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
begin
  mb_AppConfiguration.AsInteger['General\LogFilePurgeRecThreshold'] := APurgeRecThreshold;
  mb_AppConfiguration.AsInteger['General\LogFilePurgeRecCount'] := APurgeRecCount;

  GlobalLogFile.PurgeRecCount := APurgeRecCount;
  GlobalLogFile.PurgeRecThreshold := APurgeRecThreshold;
end;

procedure TevAAControl.SetSettings(const ASettings: IisListOfValues);
begin
  if ASettings.ValueExists('RequestBroker') then
  begin
    Mainboard.XmlRPCServer.SetRBHost(ASettings.Value['RequestBroker']);
    mb_AppConfiguration.SetValue('General\RequestBroker', Mainboard.XmlRPCServer.GetRBHost);
  end;

  if ASettings.ValueExists('XMLRPCPort') then
  begin
    Mainboard.XmlRPCServer.SetPort(ASettings.Value['XMLRPCPort']);
    mb_AppConfiguration.SetValue('APIAdapter\XMLRPCPort', Mainboard.XmlRPCServer.GetPort);
  end;

  if ASettings.ValueExists('SSL') then
  begin
    Mainboard.XmlRPCServer.SSLEnable := ASettings.Value['XMLRPCSSL'];
    mb_AppConfiguration.SetValue('APIAdapter\XMLRPCSSL', Mainboard.XmlRPCServer.SSLEnable);
  end;
end;


{ TevAAControllerDatagramDispatcher }

procedure TevAAControllerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_AACONTROLLER_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_AACONTROLLER_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_AACONTROLLER_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_AACONTROLLER_GETSETTINGS, dmGetSettings);
  AddHandler(METHOD_AACONTROLLER_SETSETTINGS, dmSetSettings);
  AddHandler(METHOD_AACONTROLLER_GETLOGFILETHRESHOLDINFO, dmGetLogFileThresholdInfo);
  AddHandler(METHOD_AACONTROLLER_SETLOGFILETHRESHOLDINFO, dmSetLogFileThresholdInfo);
  AddHandler(METHOD_AACONTROLLER_GETSTACKINFO, dmGetStackInfo);
end;


function TevAAControllerDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoDeployMgrAppInfo.AppID);
end;

procedure TevAAControllerDatagramDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := FControl.GetLogEventDetails(
    ADatagram.Params.Value['EventNbr']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevAAControllerDatagramDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  PageCount: Integer;
begin
  Res := FControl.GetLogEvents(
    ADatagram.Params.Value['PageNbr'],
    ADatagram.Params.Value['EventsPerPage'],
    PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevAAControllerDatagramDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

  Res := FControl.GetFilteredLogEvents(Query);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevAAControllerDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  FControl := TevAAControl.Create;
end;

procedure TevAAControllerDatagramDispatcher.dmGetSettings(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := FControl.GetSettings;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevAAControllerDatagramDispatcher.dmSetSettings(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IisListOfValues;
begin
  S := IInterface(ADatagram.Params.Value['ASettings']) as IisListOfValues;
  FControl.SetSettings(S);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevAAControllerDatagramDispatcher.dmGetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  FControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.Params.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TevAAControllerDatagramDispatcher.dmSetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.SetLogFileThresholdInfo(ADatagram.Params.Value['APurgeRecThreshold'],
    ADatagram.Params.Value['APurgeRecCount']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevAAControllerDatagramDispatcher.dmGetStackInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, FControl.GetStackInfo);
end;

{ TevAPIKey }

procedure TevAPIKey.DoOnConstruction;
begin
  inherited;
  FVendorCustomNbr := -1;
end;

function TevAPIKey.GetAPIKey: TisGUID;
begin
  result := FAPIKey;
end;

function TevAPIKey.GetAPIKeyType: TevAPIKeyType;
begin
  Result := FAPIKeyType;
end;

function TevAPIKey.GetApplicationName: String;
begin
  Result := FApplicationName;
end;

class function TevAPIKey.GetTypeID: String;
begin
  Result := 'APIKey';
end;

function TevAPIKey.GetVendorCustomNbr: integer;
begin
  Result := FVendorCustomNbr;
end;

function TevAPIKey.GetVendorName: String;
begin
  Result := FVendorName;
end;

procedure TevAPIKey.ReadFromStream(const AStream: IEvDualStream);
begin
  inherited;
  FAPIKey := AStream.ReadString;
  FAPIKeyType := TevAPIKeyType(AStream.ReadInteger);
  FApplicationName := AStream.ReadString;
  FVendorCustomNbr := AStream.ReadInteger;
  FVendorName := AStream.ReadString;
end;

procedure TevAPIKey.SetAPIKey(AAPIKey: TisGUID);
begin
  FAPIKey := AAPIKey;
end;

procedure TevAPIKey.SetAPIKeyType(AAPIKeyType: TevAPIKeyType);
begin
  FAPIKeyType := AAPIKeyType;
end;

procedure TevAPIKey.SetApplicationName(AAplicationName: String);
begin
  FApplicationName := AAplicationName;
end;

procedure TevAPIKey.SetVendorCustomNbr(AVendorCustomNbr: integer);
begin
  FVendorCustomNbr := AVendorCustomNbr;
end;

procedure TevAPIKey.SetVendorName(AVendorName: String);
begin
  FVendorName := AVendorName;
end;

procedure TevAPIKey.WriteToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FAPIKey);
  AStream.WriteInteger(Integer(FAPIKeyType));
  AStream.WriteString(FApplicationName);
  AStream.WriteInteger(FVendorCustomNbr);
  AStream.WriteString(FVendorName);
end;

{ TUserRegisteredComputerInfo }

function TUserRegisteredComputerInfo.GetComputerId: String;
begin
  Result := FComputerId;
end;

function TUserRegisteredComputerInfo.GetLastAccessDate: TDateTime;
begin
  Result := FLastAccessDate;
end;

function TUserRegisteredComputerInfo.GetToken: String;
begin
  Result := FToken;
end;

class function TUserRegisteredComputerInfo.GetTypeID: String;
begin
  Result := 'UserRegisteredComputerInfo';
end;

function TUserRegisteredComputerInfo.GetUserComputerName: String;
begin
  Result := FUserComputerName;
end;

function TUserRegisteredComputerInfo.GetCookieId: String;
begin
  Result := FCookieId;
end;

function TUserRegisteredComputerInfo.GetUserName: String;
begin
  Result := FUserName;
end;

procedure TUserRegisteredComputerInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FCookieId := AStream.ReadString;
  FComputerId := AStream.ReadString;
  FToken := AStream.ReadString;
  FUserComputerName := AStream.ReadString;
  FLastAccessDate := AStream.ReadDouble;
  FUserName := AStream.ReadString;
end;

procedure TUserRegisteredComputerInfo.SetComputerId(const AValue: String);
begin
  FComputerId := AValue;
end;

procedure TUserRegisteredComputerInfo.SetLastAccessdate(const AValue: TDateTime);
begin
  FLastAccessDate := AValue;
end;

procedure TUserRegisteredComputerInfo.SetToken(const AValue: String);
begin
  FToken := AValue;
end;

procedure TUserRegisteredComputerInfo.SetUserComputerName(const AValue: String);
begin
  FUserComputerName := AValue;
end;

procedure TUserRegisteredComputerInfo.SetCookieId(const AValue: String);
begin
  FCookieId := AValue;
end;

procedure TUserRegisteredComputerInfo.SetUserName(const AValue: String);
begin
  FUserName := AValue;
end;

procedure TUserRegisteredComputerInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FCookieId);
  AStream.WriteString(FComputerId);
  AStream.WriteString(FToken);
  AStream.WriteString(FUserComputerName);
  AStream.WriteDouble(FLastAccessDate);
  AStream.WriteString(FUserName);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetXMLRPCServer, IevXmlRPCServer, 'XML RPC Server');
  ObjectFactory.Register([TUserRegisteredComputerInfo]);

finalization
  SafeObjectFactoryUnRegister([TUserRegisteredComputerInfo]);
end.
