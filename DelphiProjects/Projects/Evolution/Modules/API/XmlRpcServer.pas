{*******************************************************}
{                                                       }
{ XML-RPC Library for Delphi, Kylix and DWPL (DXmlRpc)  }
{ XmlRpcServer.pas                                      }
{                                                       }
{ for Delphi 6, 7                                       }
{ Release 2.0.0                                         }
{ Copyright (c) 2001-2003 by Team-DelphiXml-Rpc         }
{ e-mail: team-dxmlrpc@dwp42.org                        }
{ www: http://sourceforge.net/projects/delphixml-rpc/   }
{                                                       }
{ The initial developer of the code is                  }
{   Clifford E. Baeseman, codepunk@codepunk.com         }
{                                                       }
{ This file may be distributed and/or modified under    }
{ the terms of the GNU Lesser General Public License    }
{ (LGPL) version 2.1 as published by the Free Software  }
{ Foundation and appearing in the included file         }
{ license.txt.                                          }
{                                                       }
{*******************************************************}
{
  $Header: /cvsroot/delphixml-rpc/dxmlrpc/source/XmlRpcServer.pas,v 1.1.1.1 2003/12/03 22:37:41 iwache Exp $
  ----------------------------------------------------------------------------

  $Log: XmlRpcServer.pas,v $
  Revision 1.1.1.1  2003/12/03 22:37:41  iwache
  Initial import of release 2.0.0

  ----------------------------------------------------------------------------
}
unit XmlRpcServer;

interface

uses
  SysUtils, Classes, Contnrs, SyncObjs,
  // #iwa-2003-11-09: IdCustomHTTPServer added for D7 support
//  IdSSlOpenSSL,
  IsSSL,
  Sockets,
  Winsock,

  XmlRpcCommon,
  LibXmlParser,
  XmlRpcTypes;

type
  { method handler procedure type }
  TRPCMethod = procedure(const MethodName: string;
      List: IInterfaceList; Return: IRpcReturn) of object;

  { method pointer object }
  TRpcMethodHandler = class(TObject)
    Name: string;
    Method: TRPCMethod;
    Help: string;
    Signature: string;
  end;

  IRPCServerParser = interface
  ['{34CE1C57-81EF-4103-A0B4-90475F2AA199}']
    procedure Parse(const Data: string);
    function GetParameters: IInterfaceList;
    procedure StartTag;
    procedure EndTag;
    procedure DataTag(const ATagType: TPartType);
    procedure SetMethodName(AMethodName : String);
    function GetMethodName : String;
    property RequestName: string read GetMethodName write SetMethodName;
  end;

  { server params parser }
  TRpcServerParser = class(TInterfacedObject, IRPCServerParser)
  private
    FParser: TXMLParser;
    FStack: IisStack;
    FLastTag: string;
    FName: string;
    FMethodName: string;
    FNames : array [0..100] of string;

    FValueTag : boolean;
    FOldStringValue : String;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Parse(const Data: string);
    function GetParameters: IInterfaceList;
    procedure StartTag;
    procedure EndTag;
    procedure DataTag(const ATagType: TPartType);
    procedure SetMethodName(AMethodName : String);
    function GetMethodName : String;
    property RequestName: string read GetMethodName write SetMethodName;
  end;

  { the actual server object }
  TRpcServer = class(TObject)
  private
    FServer: TTcpServer;
    FPort: Integer;
    // Take TObjectList instead of TList;
    // A free to TObjectList frees also its items.  14.8.2003 / mko
    FMethodList: TObjectList;
    FActive: Boolean;
    FIntrospect: Boolean;
    FSSLEnable: Boolean;
    FSSLRootCertFile: string;
    FSSLCertFile: string;
    FSSLKeyFile: string;
    FSSLContext: Pointer;

    {introspection extension methods }
    procedure SystemListMethods(const MethodName: string;
        List: IInterfaceList; Return: IRpcReturn);
    procedure SystemMethodHelp(const MethodName: string;
        List: IInterfaceList; Return: IRpcReturn);
    procedure SystemMethodSignature(const MethodName: string; List: IInterfaceList; Return: IRpcReturn);

    { end introspection extension methods }
    procedure RpcDataPosted(Sender: TObject;
        ClientSocket: TCustomIpClient);
    procedure SetActive(const Value: Boolean);
    procedure StartServer;
    procedure StopServer;
    function GetParser: IRpcServerParser;
  public
    constructor Create;
    destructor Destroy; override;
    property EnableIntrospect: Boolean read FIntrospect write FIntrospect;
    procedure RegisterMethodHandler(MethodHandler: TRpcMethodHandler);
    property ListenPort: Integer read FPort write FPort;
    property Active: Boolean read FActive write SetActive;

    property SSLEnable: Boolean read FSSLEnable write FSSLEnable;
    property SSLRootCertFile: string read FSSLRootCertFile write FSSLRootCertFile;
    property SSLCertFile: string read FSSLCertFile write FSSLCertFile;
    property SSLKeyFile: string read FSSLKeyFile write FSSLKeyFile;
  end;

implementation

uses
  EvMainBoard, isLogFile, ISErrorUtils, EvXmlRpcServerMod;

constructor TRpcServerParser.Create;
var
  i : integer;
begin
  FParser := TXMLParser.Create;
  FParser.Normalize := False;
  FStack := TisStack.Create;
  for i := Low(FNames) to High(FNames) do
    FNames[i] := '';
end;

destructor TRpcServerParser.Destroy;
begin
  if Assigned(FParser) then
    FParser.Free;
  inherited Destroy;
end;

procedure TRpcServerParser.DataTag(const ATagType: TPartType);
var
  Data: string;
begin
  Data := FParser.CurContent;

  if (Trim(Data) = '') and FValueTag then
  begin
    FOldStringValue := Data;
    Exit;
  end;

  { empty not allowed }
  if (Trim(Data) = '') and (FLastTag <> 'STRING') then
//  if Data = '' then
    Exit;

  // data contains not only spaces
  FValueTag := false;
  FOldStringValue := '';


  if (Data = FName) and (FLastTag = 'VALUE') and  not ((FStack.Count > 0) and Supports(FStack.Peek, IRpcStruct)) then
    Data := '';
  if (FStack.Count > 0) and Supports(FStack.Peek, IRpcStruct) and (Data = FName) and (FLastTag = 'VALUE') and (ATagType = ptEmptyTag) then
    Data := '';

  if (FLastTag = 'METHODNAME') then
    FMethodName := Data;

  {this will handle the default
   string pain in the ass}
  if FLastTag = 'VALUE' then
    FLastTag := 'STRING';

  {ugly null string hack}
  if (FLastTag = 'STRING') then
  begin
    if (Data = '[NULL]') then
      Data := '';
    if (Data = '[]') then
      Data := ' ';
  end;

  {if the tag was a struct name we will
   just store it for the next pass    }
  if FLastTag = 'NAME' then
  begin
    if (FStack.Count > 0) then
      FNames[FStack.Count-1] := Data;
    FName := Data;
    Exit;
  end;

  if (FStack.Count > 0) then
    if Supports(FStack.Peek, IRpcStruct) then
    begin
      if (FLastTag = 'STRING') then
        IRpcStruct(FStack.Peek).LoadRawData(dtString, FName, Data);
      if (FLastTag = 'INT') then
        IRpcStruct(FStack.Peek).LoadRawData(dtInteger, FName, Data);
      if (FLastTag = 'I4') then
//AY - changed from  dtDateTime
        IRpcStruct(FStack.Peek).LoadRawData(dtInteger, FName, Data);
      if (FLastTag = 'DOUBLE') then
        IRpcStruct(FStack.Peek).LoadRawData(dtFloat, FName, Data);
      if (FLastTag = 'DATETIME.ISO8601') then
        IRpcStruct(FStack.Peek).LoadRawData(dtDateTime, FName, Data);
      if (FLastTag = 'BASE64') then
        IRpcStruct(FStack.Peek).LoadRawData(dtBase64, FName, Data);
      if (FLastTag = 'BOOLEAN') then
        IRpcStruct(FStack.Peek).LoadRawData(dtBoolean, FName, Data);
    end;

  if (FStack.Count > 0) then
    if Supports(FStack.Peek, IRpcArray) then
    begin
      if (FLastTag = 'STRING') then
        IRpcArray(FStack.Peek).LoadRawData(dtString, Data);
      if (FLastTag = 'INT') then
        IRpcArray(FStack.Peek).LoadRawData(dtInteger, Data);
      if (FLastTag = 'I4') then
        IRpcArray(FStack.Peek).LoadRawData(dtInteger, Data);
      if (FLastTag = 'DOUBLE') then
        IRpcArray(FStack.Peek).LoadRawData(dtFloat, Data);
      if (FLastTag = 'DATETIME.ISO8601') then
        IRpcArray(FStack.Peek).LoadRawData(dtDateTime, Data);
      if (FLastTag = 'BASE64') then
        IRpcArray(FStack.Peek).LoadRawData(dtBase64, Data);
      if (FLastTag = 'BOOLEAN') then
        IRpcArray(FStack.Peek).LoadRawData(dtBoolean, Data);
    end;

  {here we are just getting a single value}
  if FStack.Count > 0 then
    if Supports(FStack.Peek, IRpcParameter) then
    begin
      if (FLastTag = 'STRING') then
        IRpcParameter(FStack.Peek).AsRawString := Data;
      if (FLastTag = 'INT') then
        IRpcParameter(FStack.Peek).AsInteger := StrToInt(Data);
      if (FLastTag = 'I4') then
        IRpcParameter(FStack.Peek).AsInteger := StrToInt(Data);
      if (FLastTag = 'DOUBLE') then
        IRpcParameter(FStack.Peek).AsFloat := StrToFloat(Data);
      if (FLastTag = 'DATETIME.ISO8601') then
        IRpcParameter(FStack.Peek).AsDateTime := IsoToDateTime(Data);
      if (FLastTag = 'BASE64') then
        IRpcParameter(FStack.Peek).AsBase64Raw := Data;
      if (FLastTag = 'BOOLEAN') then
        IRpcParameter(FStack.Peek).AsBoolean := StrToBool(Data);
    end;
end;

procedure TRpcServerParser.EndTag;
var
  Tag: string;
  rpcArray: IRpcArray;
  rpcStruct: IRpcStruct;
begin
  FLastTag := '';
  Tag := UpperCase(Trim(string(FParser.CurName)));

  if (Tag = 'VALUE') and FValueTag then
  begin
    if (FStack.Count > 0) then
    begin
      if Supports(FStack.Peek, IRpcStruct) then
        IRpcStruct(FStack.Peek).LoadRawData(dtString, FName, FOldStringValue);
      if Supports(FStack.Peek, IRpcArray) then
        IRpcArray(FStack.Peek).LoadRawData(dtString, FOldStringValue);
      if Supports(FStack.Peek, IRpcParameter) then
        IRpcParameter(FStack.Peek).AsRawString := FOldStringValue;
      FValueTag := false;
      FOldStringValue := '';
    end;
  end
  else
  begin
    FValueTag := false;
    FOldStringValue := '';
  end;

  if (Tag = 'ARRAY') then
    if Supports(FStack.Peek, IRpcArray) then
    begin
      rpcArray := IRpcArray(FStack.Pop);
      if Supports(FStack.Peek, IRpcParameter) then
        IRpcParameter(FStack.Peek).AsArray := rpcArray;
      if Supports(FStack.Peek, IRpcArray) then
        IRpcArray(FStack.Peek).AddItem(rpcArray);
      if Supports(FStack.Peek, IRpcStruct) then
        IRpcStruct(FStack.Peek).AddItem(FNames[FStack.Count-1], rpcArray);
    end;

  if (Tag = 'STRUCT') then
    if Supports(FStack.Peek, IRpcStruct) then
    begin
      rpcStruct := IRpcStruct(FStack.Pop);
      if Supports(FStack.Peek, IRpcParameter) then
        IRpcParameter(FStack.Peek).AsStruct := rpcStruct;
      if Supports(FStack.Peek, IRpcArray) then
        IRpcArray(FStack.Peek).AddItem(rpcStruct);
      if Supports(FStack.Peek, IRpcStruct) then
        IRpcStruct(FStack.Peek).AddItem(FNames[FStack.Count-1], rpcStruct);
    end;
end;

function TRpcServerParser.GetParameters: IInterfaceList;
var
  Index: Integer;
begin
  Result := TInterfaceList.Create;
  {we need to reverse the order of the items in the list
   to make the order appear correct at the called method}
  Result.Count := FStack.Count;
  for Index := FStack.Count - 1 downto 0 do
    Result[Index] := IRpcParameter(FStack.Pop);
end;

procedure TRpcServerParser.Parse(const Data: string);
var
  tmp : String;
begin
  tmp := FixEmptyString(Data);
  FParser.Normalize := False;
  FParser.LoadFromBuffer(PChar(tmp));     //CurContent
  FParser.Normalize := False;
//AY - empty valye as it holds value from prevous request and it checks at empty tag processing
  FName := '';

  FValueTag := false;
  FOldStringValue := '';

  FParser.StartScan;
  while FParser.Scan do
  begin
    case FParser.CurPartType of
      ptStartTag:
        StartTag;
      ptContent, ptEmptyTag:
        DataTag(FParser.CurPartType);
      ptEndTag:
        EndTag;
    end;
  end;
end;

procedure TRpcServerParser.StartTag;
var
  Tag: string;
  rpcParameter: IRpcParameter;
  rpcStruct: IRpcStruct;
  rpcArray: IRpcArray;
begin
  Tag := UpperCase(Trim(string(FParser.CurName)));

  if (Tag <> 'VALUE') or FValueTag then
  begin
    FValueTag := false;
    FOldStringValue := '';
  end;
  if (Tag = 'PARAM') then
  begin
    rpcParameter := TRpcParameter.Create;
    FStack.Push(rpcParameter);
  end;
  if (Tag = 'ARRAY') then
  begin
    rpcArray := TRpcArray.Create;
    FStack.Push(rpcArray);
  end;
  if (Tag = 'STRUCT') then
  begin
    rpcStruct := TRpcStruct.Create;
    FStack.Push(rpcStruct);
  end;
  if (Tag = 'VALUE') then
  begin
    FValueTag := true;
    FOldStringValue := '';
  end;

  FLastTag := Tag;
end;

constructor TRpcServer.Create;
begin
  inherited Create;
  FServer := TTcpServer.Create(nil);
  // 14.8.2003 / mko
  // FMethodList := TList.Create; Take TObjectList instead of TList;
  FMethodList := TObjectList.Create;
  FMethodList.OwnsObjects := True;
{
//AY - IP filetering
  FIPAcceptList := TStringList.Create;
  FIPDenyList := TStringList.Create;
  FIPLock := TCriticalSection.Create;
}
end;

procedure TRpcServer.RpcDataPosted(Sender: TObject;
  ClientSocket: TCustomIpClient);
var
  sCmd, sInputLine: String;
  ContentLength, Index: Integer;
  Parser: IRpcServerParser;
  List: IInterfaceList;
  UnparsedParams, RequestName: String;
  Found: Boolean;
  RpcReturn: IRpcReturn;
  SSL: Pointer;
  err: Integer;
  sRemoteHost: String;

  function FetchCaseInsensitive(var AInput: string; const ADelim: string = ' ';
   const ADelete: Boolean = True): String;
  var
    LPos: integer;
  begin
    if ADelim = #0 then begin
      // AnsiPos does not work with #0
      LPos := Pos(ADelim, AInput);
    end else begin
      //? may be AnsiUpperCase?
      LPos := Pos(UpperCase(ADelim), UpperCase(AInput));
    end;
    if LPos = 0 then begin
      Result := AInput;
      if ADelete then begin
        AInput := '';    {Do not Localize}
      end;
    end else begin
      Result := Copy(AInput, 1, LPos - 1);
      if ADelete then begin
        //This is faster than Delete(AInput, 1, LPos + Length(ADelim) - 1);
        AInput := Copy(AInput, LPos + Length(ADelim), MaxInt);
      end;
    end;
  end;

  function Fetch(var AInput: string; const ADelim: string = ' ';
   const ADelete: Boolean = True;
   const ACaseSensitive: Boolean = True): String;
  var
    LPos: integer;
  begin
    if ACaseSensitive then begin
      if ADelim = #0 then begin
        // AnsiPos does not work with #0
        LPos := Pos(ADelim, AInput);
      end else begin
        LPos := Pos(ADelim, AInput);
      end;
      if LPos = 0 then begin
        Result := AInput;
        if ADelete then begin
          AInput := '';    {Do not Localize}
        end;
      end
      else begin
        Result := Copy(AInput, 1, LPos - 1);
        if ADelete then begin
          //slower Delete(AInput, 1, LPos + Length(ADelim) - 1);
          AInput:=Copy(AInput, LPos + Length(ADelim), MaxInt);
        end;
      end;
    end else begin
      Result := FetchCaseInsensitive(AInput, ADelim, ADelete);
    end;
  end;

  function HttpResponse(const content: String): String;
  begin
    Result := 'HTTP/1.1 200 OK'#13#10+
               'Connection: Keep-Alive' + #13#10 +
                'Content-Type: text/xml'#13#10+
                'Content-Length: '+IntToStr(Length(content))+#13#10#13#10+
                content;
  end;

  function SSL_read_ln: String;
  var
    c: Char;
  begin
    Result := '';
    repeat
      err := SSL_read(SSL, @c, 1);
      if (err > 0) and not (c in [#13,#10]) then Result := Result + c;
    until not ((c <> #10) and (err > 0));
  end;

  function SocketReadLn: String;
  begin
    if FSSLEnable then
      Result := SSL_read_ln
    else
    begin
      if ClientSocket.WaitForData(-1) then
        Result := ClientSocket.Receiveln
      else
      begin
        ClientSocket.Disconnect;
        Result := '';
        err := -1;
      end;
    end;
  end;

  procedure SocketWriteLn(const Line: String);
  begin
    if FSSLEnable then
    begin
      SSL_write(SSL, PChar(Line), Length(Line));
      SSL_write(SSL, #13#10, 2);
    end
    else
      ClientSocket.SendLn(Line);
  end;

  procedure SocketReadBuf(var buf; len: Integer);
  var
    i, BytesToRead, BytesLeft: Integer;
    P: PChar;
  begin
    if FSSLEnable then
    begin
      P := @buf;
      i := 0;
      BytesLeft := len;
      while BytesLeft > 0 do
      begin
        if BytesLeft > 256 then
          BytesToRead := 256
        else
          BytesToRead := BytesLeft;
        BytesToRead := SSL_read(SSL, @P[i], BytesToRead);
        i := i + BytesToRead;
        if BytesToRead <= 0 then
          raise Exception.Create('XmlRPC server. SSL_read function returned error code: ' + IntToStr(BytesToRead));
        BytesLeft := BytesLeft - BytesToRead;
      end;
    end
    else
      ClientSocket.ReceiveBuf(buf, len);
  end;

begin
  RpcReturn := nil;
  sRemoteHost := 'Remote ' + ClientSocket.RemoteHost + ':' + ClientSocket.RemotePort;

  mb_LogFile.AddEvent(etInformation, APIEventClass, sRemoteHost + ' connected', '');

  if FSSLEnable then
  begin
    SSL := SSL_new(FSSLContext);
    SSL_set_fd(SSL, ClientSocket.Handle);
    err := SSL_accept(SSL);
    if err <= 0 then
    begin
      err := SSL_get_error(FSSLContext, err);
      raise Exception.Create('SSL_accept error: ' + OpenSSLLib.GetErrorStr(err));
    end;
  end
  else
    err := 1; // without SSL Function SocketReadLn do not change err variable ; init it
  try
    repeat
      try
        sInputLine := SocketReadLn;

        if err <= 0 then
          break;

        sCmd := UpperCase(Fetch(sInputLine, ' '));
        if sCmd = 'POST' then
        begin
          ContentLength := 0;
          repeat
            sInputLine := SocketReadLn;
            if Fetch(sInputLine, ':') = 'Content-Length' then
            begin
              ContentLength := StrToInt(Trim(sInputLine));
            end;
          until sInputLine = '';

          if ContentLength > 0 then
          begin
            try
              SetLength(UnparsedParams, ContentLength);
              SocketReadBuf(UnparsedParams[1], ContentLength);
              Parser := GetParser;
              Parser.Parse(UnparsedParams);
              List := Parser.GetParameters;
              RequestName := Parser.RequestName;
              Found := False;
              RpcReturn := TRpcReturn.Create;
              for Index := 0 to FMethodList.Count - 1 do
                if (TRpcMethodHandler(FMethodList[Index]).Name = RequestName) then
                begin
                  mb_LogFile.AddEvent(etInformation, APIEventClass, sRemoteHost + ' is executing method "' +
                    TRpcMethodHandler(FMethodList[Index]).Name + '"', 'Input length: ' + IntToStr(ContentLength));
                  TRpcMethodHandler(FMethodList[Index]).Method(
                    TRpcMethodHandler(FMethodList[Index]).Name, List, RpcReturn);
                  Found := True;
                  mb_LogFile.AddEvent(etInformation, APIEventClass, sRemoteHost + ' executed method "' +
                    TRpcMethodHandler(FMethodList[Index]).Name + '"', '');
                  Break;
                end;

              if not Found then
              begin
                RpcReturn.SetError(999, 'XML-RPC call not found');
                sCmd := HttpResponse(RpcReturn.ResponseXML);
                SocketWriteLn(sCmd);
                Exit;
              end;
              if RpcReturn.ErrorCode = 0 then
              begin
                sCmd := HttpResponse(RpcReturn.ResponseXML);
                SocketWriteLn(sCmd);
              end else
              begin
                sCmd := HttpResponse(RpcReturn.ResponseXML);
                SocketWriteLn(sCmd);
              end;

              mb_LogFile.AddEvent(etInformation, APIEventClass, sRemoteHost + ' received result of method "' +
                TRpcMethodHandler(FMethodList[Index]).Name + '"', 'Output length: ' + IntToStr(Length(sCmd)));
            except
              on E: Exception do
              begin
                RpcReturn.SetError(990, E.Message);
                sCmd := HttpResponse(RpcReturn.ResponseXML);
                SocketWriteLn(sCmd);
              end;
            end; // try
          end

          else
          begin
            ClientSocket.Disconnect;
            break;
          end; // if ContentLength > 0
        end;  // if sCmd = 'POST'

      except
        on E: Exception do
        begin
          if Assigned(RpcReturn) then
          begin
            RpcReturn.SetError(990, E.Message);
            sCmd := HttpResponse(RpcReturn.ResponseXML);
            SocketWriteLn(sCmd);
          end
          else
          begin
            mb_LogFile.AddEvent(etError, APIEventClass, sRemoteHost + '. Exception in RpcDataPosted:' + E.Message, GetStack);
            ClientSocket.Disconnect;
          end;
        end;
      end;
    until not ClientSocket.Connected;
    
  finally
    if FSSLEnable then
      SSL_free(SSL);
    mb_LogFile.AddEvent(etInformation, APIEventClass, sRemoteHost + ' disconnected', '');
  end;
end;

destructor TRpcServer.Destroy;
begin
//  FIPLock.Free;
  FMethodList.Free;
  FServer.Free;
//  FIPAcceptList.Free;
//  FIPDenyList.Free;
  inherited Destroy;
end;

function TRpcServer.GetParser: IRpcServerParser;
begin
  { dummy allocator }
  Result := TRpcServerParser.Create;
end;

procedure TRpcServer.RegisterMethodHandler(MethodHandler: TRpcMethodHandler);
begin
  FMethodList.Add(MethodHandler);
end;

procedure TRpcServer.SetActive(const Value: Boolean);
begin
  if (Factive <> Value) then
  begin
    if Value then
      StartServer
    else
      StopServer;
    FActive := Value;
  end;
end;

procedure TRpcServer.StartServer;
var
  MethodHandler: TRpcMethodHandler;
//AY  IdServerIOHandlerSSL: TIdServerIOHandlerSSL;
begin
  FServer.LocalPort := IntToStr(FPort);
  FServer.OnAccept := RpcDataPosted;
  FServer.ServerSocketThread.ThreadCacheSize := 100;

// TODO  FServer.SessionTimeOut := 10000;

//AY - Enable SSL - ???
{  if (FSSLEnable) then
  begin
    IdServerIOHandlerSSL := TIdServerIOHandlerSSL.Create(nil);
    IdServerIOHandlerSSL.SSLOptions.RootCertFile := FSSLRootCertFile;
    IdServerIOHandlerSSL.SSLOptions.CertFile := FSSLCertFile;
    IdServerIOHandlerSSL.SSLOptions.KeyFile := FSSLKeyFile;
    IdServerIOHandlerSSL.SSLOptions.Method := sslvSSLv3;
    //IdServerIOHandlerSSL.SSLOptions.Method := sslvSSLv23;
    //IdServerIOHandlerSSL.SSLOptions.Method := sslvTLSv1;
    FServer.IOHandler := IdServerIOHandlerSSL;
  end;
}

  if FSSLEnable then
    FSSLContext := OpenSSLLib.NewServerContext(FSSLRootCertFile, FSSLCertFile, FSSLKeyFile);

  {introspection extension}
  if FIntrospect then
  begin
    {list methods}
    MethodHandler := TRpcMethodHandler.Create;
    MethodHandler.Name := 'system.listMethods';
    MethodHandler.Method := SystemListMethods;
    MethodHandler.Signature := 'array (no params)';
    MethodHandler.Help := 'Returns a list of methods registered on the server';
    RegisterMethodHandler(MethodHandler);
    {system help}
    MethodHandler := TRpcMethodHandler.Create;
    MethodHandler.Name := 'system.methodHelp';
    MethodHandler.Method := SystemMethodHelp;
    MethodHandler.Signature := 'string (string method_name)';
    MethodHandler.Help := 'Returns help text supplied for the method';
    RegisterMethodHandler(MethodHandler);
    {system signature}
    MethodHandler := TRpcMethodHandler.Create;
    MethodHandler.Name := 'system.methodSignature';
    MethodHandler.Method := SystemMethodSignature;
    MethodHandler.Signature := 'array of arrays (string method_name)';
    MethodHandler.Help := 'Returns an array of signatures for the method';
    RegisterMethodHandler(MethodHandler);
  end;
//  FServer.LocalHost := 'localhost';
  FServer.Active := True;
end;

procedure TRpcServer.StopServer;
begin
  FServer.Active := False;
end;

{server introspection extensions}

procedure TRpcServer.SystemListMethods(const MethodName: 
    string; List: IInterfaceList; Return: IRpcReturn);
var
  RpcArray: IRpcArray;
  Index: Integer;
begin
  RpcArray := TRpcArray.Create;
  for Index := 0 to FMethodList.Count - 1 do
  begin
    if Pos('LEGACY.', UpperCase(TRpcMethodHandler(FMethodList[Index]).Name)) = 0 then
      RpcArray.AddItem(TRpcMethodHandler(FMethodList[Index]).Name);
  end;
  Return.AddItem(RpcArray);
end;

procedure TRpcServer.SystemMethodHelp(const MethodName:
    string; List: IInterfaceList; Return: IRpcReturn);
var
  Data: string;
  Index: Integer;
begin
  Data := Trim(IRpcParameter(List[0]).AsString);
  if Pos('LEGACY.', UpperCase(Data)) = 0 then
  begin
    for Index := 0 to FMethodList.Count - 1 do
      if (TRpcMethodHandler(FMethodList[Index]).Name = Data) then
        Data := TRpcMethodHandler(FMethodList[Index]).Help;
  end
  else
    Data := '';

  Return.AddItem(Data);
end;

procedure TRpcServer.SystemMethodSignature(const
    MethodName: string; List: IInterfaceList; Return: IRpcReturn);
var
  S: string;
  RpcArray: IRpcArray;
  Signatures: IRpcArray;
  Index: Integer;
begin
  S := Trim(IRpcParameter(List[0]).AsString);
  RpcArray := TRpcArray.Create;
  Signatures := TRpcArray.Create;
  if Pos('LEGACY.', UpperCase(S)) = 0 then
    for Index := 0 to FMethodList.Count - 1 do
      if (TRpcMethodHandler(FMethodList[Index]).Name = S) then
        Signatures.AddItem(TRpcMethodHandler(FMethodList[Index]).Signature);
  RpcArray.AddItem(Signatures);
  Return.AddItem(RpcArray);
end;

function TRpcServerParser.GetMethodName: String;
begin
  Result := FMethodName;
end;

procedure TRpcServerParser.SetMethodName(AMethodName: String);
begin
  FMethodName := AMethodName;
end;

end.

