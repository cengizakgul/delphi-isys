unit EvLocalTables;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, evExceptions, EvDataset, DB;

type
  TEvLocalTableNames = (CO_GROUP, CO_GROUP_MANAGER, CO_GROUP_MEMBER, EE_CHANGE_REQUEST,
    EE_TIMEOFF_REQUEST, CO_STORAGE);

  IEvLocalTable = interface
  ['{E1A1027F-DE86-4D9E-8C3D-0FD9F418D782}']
    function  GetClNbr: integer;
    function  GetTableName: TEvLocalTableNames;
    function  GetDataset: IevDataSet;
    function  isItVirtualTable: boolean;

    procedure RefreshData;
    procedure SaveData;
  end;

  IEvLocalTablesManager = interface
  ['{C12FF698-F9D3-4C23-960D-B94072A9DD13}']
    function  TryToRequestTable(const AClNbr: integer; const ATableName: TEvLocalTableNames; const ATimeoutInSec: integer): IEvLocalTable;
    procedure FreeTable(const ATableObject: IEvLocalTable);
  end;

  TEvLocalTable = class(TisInterfacedObject, IEvLocalTable)
  private
    FClNbr: integer;
    FTableName: TEvLocalTableNames;
    FRootStoragePath: String;
    FStorage: IevDataSet;
    FLastGeneratorValue: integer;
  protected
    procedure DoOnConstruction; override;
    procedure InitTableStructure;

    function  GetStorageFileName: String; virtual;
    function  GetStoragePath: String; virtual;

    procedure GetNextValue(DataSet: TDataSet);

    // IEvLocalTable
    function  GetClNbr: integer;
    function  GetTableName: TEvLocalTableNames;
    function  GetDataset: IevDataSet;
    function  isItVirtualTable: boolean; virtual;
    procedure RefreshData; virtual;
    procedure SaveData; virtual;
  public
    destructor  Destroy; override;
    constructor Create(const AClNbr: integer; const ATableName: TEvLocalTableNames;
      const ARootStoragePath: String); reintroduce;
  end;

  TEvVirtualLocalTable = class(TEvLocalTable, IEvLocalTable)
  private
  protected
    procedure ReadCoGroup;
    procedure ReadCoGroupManager;
    procedure ReadCoGroupMember;

    // IEvLocalTable
    function  GetStorageFileName: String; override;
    function  GetStoragePath: String; override;
    function  isItVirtualTable: boolean; override;
    procedure RefreshData; override;
    procedure SaveData; override;
  public
    destructor  Destroy; override;
  end;

  TEvLocalTablesManager = class(TisInterfacedObject, IEvLocalTablesManager)
  private
    FRootStoragePath: String;
    FLockList: IisListOfValues;
  protected
    procedure DoOnConstruction; override;
    function  CalculateNameForList(const AClNbr: integer; const ATableName: TEvLocalTableNames): String;

    //IEvLocalTablesManager
    function  TryToRequestTable(const AClNbr: integer; const ATableName: TEvLocalTableNames; const ATimeoutInSec: integer): IEvLocalTable;
    procedure FreeTable(const ATableObject: IEvLocalTable);
  public
    destructor  Destroy; override;
    constructor Create(const ARootStoragePath: String); reintroduce;
  end;

  function EvLocalTableNamesToString(const ATableName: TEvLocalTableNames): String;

implementation

uses ISBasicUtils, EvStreamUtils, isThreadManager;

function EvLocalTableNamesToString(const ATableName: TEvLocalTableNames): String;
begin
  Result := '';
  case ATableName of
    CO_GROUP: Result := 'CO_GROUP';
    CO_GROUP_MANAGER: Result := 'CO_GROUP_MANAGER';
    CO_GROUP_MEMBER: Result := 'CO_GROUP_MEMBER';
    EE_CHANGE_REQUEST: Result := 'EE_CHANGE_REQUEST';
    EE_TIMEOFF_REQUEST: Result := 'EE_TIMEOFF_REQUEST';
    CO_STORAGE: Result := 'CO_STORAGE';
  else
    Assert(false, 'Cannot find table''s name: ' + IntToStr(Integer(ATableName)));
  end;
end;

{ TEvLocalTable }

constructor TEvLocalTable.Create(const AClNbr: integer; const ATableName: TEvLocalTableNames;
  const ARootStoragePath: String);
begin
  inherited Create;

  CheckCondition(AClNbr > 0, 'Wrong client nbr: ' + IntToStr(AClNbr));
  FClNbr := AClNbr;
  FTableName := ATableName;
  FRootStoragePath := NormalizePath(ARootStoragePath);

  InitTableStructure;
  RefreshData;
end;

destructor TEvLocalTable.Destroy;
begin
  FStorage.vclDataSet.OnNewRecord := nil;
  FStorage := nil;

  inherited;
end;

procedure TEvLocalTable.DoOnConstruction;
begin
  inherited;
  FStorage := TevDataSet.Create;
  FStorage.vclDataSet.OnNewRecord := GetNextValue;

  FLastGeneratorValue := 1;
end;

function TEvLocalTable.GetClNbr: integer;
begin
  Result := FClNbr;
end;

function TEvLocalTable.GetDataset: IevDataSet;
begin
  Result := FStorage;
end;

function TEvLocalTable.GetStoragePath: String;
begin
  Result := FRootStoragePath + 'CL_' + IntToStr(FClNbr);
end;

function TEvLocalTable.GetTableName: TEvLocalTableNames;
begin
  Result := FTableName;
end;

function TEvLocalTable.GetStorageFileName: String;
begin
  Result := EvLocalTableNamesToString(FTableName) + '.dat';
end;

procedure TEvLocalTable.RefreshData;
var
  tmpStream, tmpStream1: IEvDualStream;
begin
  CheckCondition(not isItVirtualTable, 'Virtual tables not allowed');
  if not FileExists(GetStoragePath + '\' + GetStorageFileName) then
  begin
    FStorage.vclDataSet.Active := false;
    FStorage.vclDataSet.Active := true;
    FLastGeneratorValue := 1;
  end
  else
  begin
    tmpStream := TevDualStreamHolder.Create;
    tmpStream.LoadFromFile(GetStoragePath + '\' + GetStorageFileName);
    tmpStream.Position := 0;
    FLastGeneratorValue := tmpStream.ReadInteger;
    tmpStream1 := tmpStream.ReadStream(nil);

    FStorage.SetAsStream(tmpStream1);
  end;
end;

procedure TEvLocalTable.SaveData;
var
  tmpStream, tmpStream1: IevDualStream;
begin
  CheckCondition(ForceDirectories(GetStoragePath), 'Cannot create folder: ' + GetStoragePath);

  tmpStream := TevDualStreamholder.CreateInMemory;
  tmpStream.WriteInteger(FLastGeneratorValue);
  tmpStream1 := FStorage.AsStream;
  tmpStream1.Position := 0;
  tmpStream.WriteStream(tmpStream1);
  tmpStream.SaveToFile(GetStoragePath + '\' + GetStorageFileName);
end;

procedure TEvLocalTable.InitTableStructure;
begin
  FStorage.vclDataSet.Name := EvLocalTableNamesToString(FTableName);
  case FTableName of
  CO_GROUP:
    begin
      FStorage.vclDataSet.FieldDefs.Add('CO_GROUP_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('CO_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('GROUP_TYPE', ftString, 1, true);
      FStorage.vclDataSet.FieldDefs.Add('GROUP_NAME', ftString, 50, true);
    end;
  CO_GROUP_MANAGER:
    begin
      FStorage.vclDataSet.FieldDefs.Add('CO_GROUP_MANAGER_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('CO_GROUP_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('SEND_EMAIL', ftBoolean, 0, true);
    end;
  CO_GROUP_MEMBER:
    begin
      FStorage.vclDataSet.FieldDefs.Add('CO_GROUP_MEMBER_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('CO_GROUP_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, true);
    end;
  EE_CHANGE_REQUEST:
    begin
      FStorage.vclDataSet.FieldDefs.Add('EE_CHANGE_REQUEST_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('REQUEST_TYPE', ftString, 1, true);
      FStorage.vclDataSet.FieldDefs.Add('REQUEST_DATA', ftBlob, 0, false);
    end;
  EE_TIMEOFF_REQUEST:
    begin
      FStorage.vclDataSet.FieldDefs.Add('EE_TIMEOFF_REQUEST_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('EE_TIME_OFF_ACCRUAL_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('EE_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('START_DATE', ftDateTime, 0, false);
      FStorage.vclDataSet.FieldDefs.Add('END_DATE', ftDateTime, 0, false);
      FStorage.vclDataSet.FieldDefs.Add('HOURS', ftInteger, 0, false);
      FStorage.vclDataSet.FieldDefs.Add('STATUS', ftString, 1, true);
      FStorage.vclDataSet.FieldDefs.Add('NOTES', ftString, 50, false);
    end;
  CO_STORAGE:
    begin
      FStorage.vclDataSet.FieldDefs.Add('CO_STORAGE_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('CO_NBR', ftInteger, 0, true);
      FStorage.vclDataSet.FieldDefs.Add('TAG', ftString, 50, true);
      FStorage.vclDataSet.FieldDefs.Add('STORAGE_DATA', ftBlob, 0, false);
    end;
  else
    Assert(false, 'Cannot find table''s name: ' + IntToStr(Integer(FTableName)));
  end;
end;

procedure TEvLocalTable.GetNextValue(DataSet: TDataSet);
begin
  Inc(FLastGeneratorValue);
  case FTableName of
  CO_GROUP:
    begin
      DataSet['CO_GROUP_NBR'] := FLastGeneratorValue;
    end;
  CO_GROUP_MANAGER:
    begin
      DataSet['CO_GROUP_MANAGER_NBR'] := FLastGeneratorValue;
    end;
  CO_GROUP_MEMBER:
    begin
      DataSet['CO_GROUP_MEMBER_NBR'] := FLastGeneratorValue;
    end;
  EE_CHANGE_REQUEST:
    begin
      DataSet['EE_CHANGE_REQUEST_NBR'] := FLastGeneratorValue;
    end;
  EE_TIMEOFF_REQUEST:
    begin
      DataSet['EE_TIMEOFF_REQUEST_NBR'] := FLastGeneratorValue;
    end;
  CO_STORAGE:
    begin
      DataSet['CO_STORAGE_NBR'] := FLastGeneratorValue;
    end;
  else
    Assert(false, 'Cannot find table''s name: ' + IntToStr(Integer(FTableName)));
  end;
end;

{ TEvVirtualLocalTable }

destructor TEvVirtualLocalTable.Destroy;
begin

  inherited;
end;

function TEvVirtualLocalTable.GetStorageFileName: String;
begin
  Assert(false, 'Table has no real storage: ' + EvLocalTableNamesToString(FTableName));
end;

function TEvVirtualLocalTable.GetStoragePath: String;
begin
  Assert(false, 'Table has no real storage: ' + EvLocalTableNamesToString(FTableName));
end;

function TEvVirtualLocalTable.isItVirtualTable: boolean;
begin
  Result := true;
end;

procedure TEvVirtualLocalTable.ReadCoGroup;
begin

end;

procedure TEvVirtualLocalTable.ReadCoGroupManager;
begin

end;

procedure TEvVirtualLocalTable.ReadCoGroupMember;
begin

end;

procedure TEvVirtualLocalTable.RefreshData;
begin
  CheckCondition(isItVirtualTable, 'Real tables not allowed');
  FStorage.vclDataSet.Active := false;
  FStorage.vclDataSet.Active := true;
  FLastGeneratorValue := 1;

  case FTableName of
    CO_GROUP: ReadCoGroup;
    CO_GROUP_MANAGER: ReadCoGroupManager;
    CO_GROUP_MEMBER: ReadCoGroupMember;
    else
      raise Exception.Create('it is not virtual table');
  end;
end;

procedure TEvVirtualLocalTable.SaveData;
begin
  Assert(false, 'It is read only table: ' + EvLocalTableNamesToString(FTableName));
end;

function TEvLocalTable.isItVirtualTable: boolean;
begin
  Result := false;
end;

{ TEvLocalTablesManager }

function TEvLocalTablesManager.CalculateNameForList(const AClNbr: integer;
  const ATableName: TEvLocalTableNames): String;
begin
  Result := 'CL_' + IntToStr(AClNbr) + '#' + EvLocalTableNamesToString(ATableName);
end;

constructor TEvLocalTablesManager.Create(const ARootStoragePath: String);
begin
  inherited Create;

  FRootStoragePath:= NormalizePath(ARootStoragePath);
  ForceDirectories(FRootStoragePath);
end;

destructor TEvLocalTablesManager.Destroy;
begin

  inherited;
end;

procedure TEvLocalTablesManager.DoOnConstruction;
begin
  inherited;
  FLockList := TisListOfValues.Create;
end;

procedure TEvLocalTablesManager.FreeTable(const ATableObject: IEvLocalTable);
var
  S: String;
begin
  FLockList.Lock;
  try
    S := CalculateNameForList(ATableObject.GetClNbr, ATableObject.GetTableName);
    if FLockList.ValueExists(S) then
      FLocklist.DeleteValue(S)
    else
      raise Exception.Create('You tried to return object wich is not in the list');
  finally
    FLockList.Unlock;
  end;
end;

function TEvLocalTablesManager.TryToRequestTable(const AClNbr: integer; const ATableName: TEvLocalTableNames;
  const ATimeoutInSec: integer): IEvLocalTable;
var
  S: String;
  i: integer;
begin
  CheckCondition(AClNbr > 0, 'Wrong Client number: ' + intToStr(AClNbr));

  Result := nil;

  S := CalculateNameForList(AClNbr, ATableName);

  for i := 0 to (ATimeoutInSec - 1) * 2 do
  begin
    FLockList.Lock;
    try
      if not FLockList.ValueExists(S) then
      begin
      case ATableName of
        CO_GROUP, CO_GROUP_MANAGER, CO_GROUP_MEMBER:
          Result := TEvVirtualLocalTable.Create(AClNbr, ATableName, FRootStoragePath);
        else
          Result := TEvLocalTable.Create(AClNbr, ATableName, FRootStoragePath);
      end;

        FLockList.AddValue(S, Result);
        exit;
      end;
    finally
      FLockList.Unlock;
    end;
    Snooze(500);
  end;  // for i
end;

end.
