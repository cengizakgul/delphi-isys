// Module "Evolution API"
unit mod_API;

interface

uses
  EvXmlRpcServerMod,
  DIMime,
  LibXmlParser,
  XmlRpcTypes,
  XmlRpcCommon,
  XmlRpcServer,
  EvESSCoStorageObject,
  EvTOARequest;

implementation

end.