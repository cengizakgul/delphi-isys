unit EvESSCoStorageObject;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvStreamUtils;

type
  IEvESSMessage = interface
  ['{F38C0132-C109-4E5D-9ACE-8CCBED209071}']
    function  GetName: String;
    function  GetMessage: String;

    procedure SetName(const AName: String);
    procedure SetMessage(const AMessage: String);
  end;

  IEvESSLink = interface
  ['{9733C63B-D802-453C-95DC-61DAC525A077}']
    function  GetName: String;
    function  GetType: String;
    function  GetURL: String;

    procedure SetName(const AName: String);
    procedure SetType(const AType: String);
    procedure SetURL(const AURL: String);
  end;

  IEvESSEDConfiguration = interface
  ['{F66BAF40-7676-4237-AA80-7D9D47479E2C}']
    function  GetCode: Integer;
    function  GetType: String;
    function  GetPosition: integer;

    procedure SetCode(const ACode: Integer);
    procedure SetType(const AType: String);
    procedure SetPosition(const APosition: integer);
  end;

  TEvESSEDConfiguration = class(TisInterfacedObject, IEvESSEDConfiguration)
  private
    FCode: Integer;
    FType: String;
    FPosition: integer;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetCode: Integer;
    function  GetType: String;
    function  GetPosition: integer;
    procedure SetCode(const ACode: Integer);
    procedure SetType(const AType: String);
    procedure SetPosition(const APosition: integer);
  public
    class function GetTypeID: String; override;

    destructor Destroy; override;
  end;

  TEvESSMessage = class(TisInterfacedObject, IEvESSMessage)
  private
    FName: String;
    FMessage: String;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetName: String;
    function  GetMessage: String;
    procedure SetName(const AName: String);
    procedure SetMessage(const AMessage: String);
  public
    class function GetTypeID: String; override;

    destructor Destroy; override;
  end;

  TEvESSLink = class(TisInterfacedObject, IEvESSLink)
  private
    FName: String;
    FType: String;
    FURL: String;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetName: String;
    function  GetType: String;
    function  GetURL: String;
    procedure SetName(const AName: String);
    procedure SetType(const AType: String);
    procedure SetURL(const AURL: String);
  public
    class function GetTypeID: String; override;

    destructor Destroy; override;
  end;

implementation

{ TEvESSMessage }

destructor TEvESSMessage.Destroy;
begin

  inherited;
end;

procedure TEvESSMessage.DoOnConstruction;
begin
  inherited;

end;

function TEvESSMessage.GetMessage: String;
begin
  Result := FMessage;
end;

function TEvESSMessage.GetName: String;
begin
  Result := FName;
end;

class function TEvESSMessage.GetTypeID: String;
begin
  Result := 'EvESSMessage';
end;

procedure TEvESSMessage.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FName := AStream.ReadString;
  FMessage := AStream.ReadString;
end;

procedure TEvESSMessage.SetMessage(const AMessage: String);
begin
  FMessage := AMessage;
end;

procedure TEvESSMessage.SetName(const AName: String);
begin
  FName := AName;
end;

procedure TEvESSMessage.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FName);
  AStream.WriteString(FMessage);
end;

{ TEvESSLink }

destructor TEvESSLink.Destroy;
begin

  inherited;
end;

procedure TEvESSLink.DoOnConstruction;
begin
  inherited;

end;

function TEvESSLink.GetName: String;
begin
  Result := FName;
end;

function TEvESSLink.GetType: String;
begin
  Result := FType;
end;

class function TEvESSLink.GetTypeID: String;
begin
  Result := 'EvESSLink';
end;

function TEvESSLink.GetURL: String;
begin
  Result := FURL;
end;

procedure TEvESSLink.ReadSelfFromStream(const AStream: IEvDualStream;
  const ARevision: TisRevision);
begin
  inherited;
  FName := AStream.ReadString;
  FType := AStream.ReadString;
  FURL := AStream.ReadString;
end;

procedure TEvESSLink.SetName(const AName: String);
begin
  FName := AName;
end;

procedure TEvESSLink.SetType(const AType: String);
begin
  FType := AType;
end;

procedure TEvESSLink.SetURL(const AURL: String);
begin
  FURL := AURL;
end;

procedure TEvESSLink.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FName);
  AStream.WriteString(FType);
  AStream.WriteString(FURL);
end;

{ TEvESSEDConfiguration }

destructor TEvESSEDConfiguration.Destroy;
begin

  inherited;
end;

procedure TEvESSEDConfiguration.DoOnConstruction;
begin
  inherited;

end;

function TEvESSEDConfiguration.GetCode: Integer;
begin
  Result := FCode;
end;

function TEvESSEDConfiguration.GetPosition: integer;
begin
  Result := FPosition;
end;

function TEvESSEDConfiguration.GetType: String;
begin
  Result := FType;
end;

class function TEvESSEDConfiguration.GetTypeID: String;
begin
  Result := 'EvESSEDConfiguration';
end;

procedure TEvESSEDConfiguration.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FCode := AStream.ReadInteger;
  FType := AStream.ReadString;
  FPosition := AStream.ReadInteger;
end;

procedure TEvESSEDConfiguration.SetCode(const ACode: Integer);
begin
  FCode := ACode;
end;

procedure TEvESSEDConfiguration.SetPosition(const APosition: integer);
begin
  FPosition := APosition;
end;

procedure TEvESSEDConfiguration.SetType(const AType: String);
begin
  FType := AType;
end;

procedure TEvESSEDConfiguration.WriteSelfToStream(
  const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FCode);
  AStream.WriteString(FType);
  AStream.WriteInteger(FPosition);
end;

initialization
  ObjectFactory.Register([TEvESSMessage, TEvESSLink, TEvESSEDConfiguration]);

finalization
  SafeObjectFactoryUnRegister([TEvESSMessage, TEvESSLink, TEvESSEDConfiguration]);
end.
