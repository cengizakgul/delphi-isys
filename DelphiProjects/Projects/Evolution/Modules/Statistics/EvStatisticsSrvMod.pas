unit EvStatisticsSrvMod;

interface

uses isBaseClasses, isStatistics, EvMainboard, evCommonInterfaces;

implementation

type
  TevServerStatistics = class(TisStatistics, IevStatistics)
  private
    FEnabled: Boolean;
  protected
    function  GetEnabled: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetDumpIntoFile: Boolean;
    procedure SetDumpIntoFile(const AValue: Boolean);
    function  GetFileName: String;
    procedure SetFileName(const AValue: String);
  end;


function GetStatistics: IisStatistics;
begin
  Result := TevServerStatistics.Create;
end;

{ TevServerStatistics }

function TevServerStatistics.GetDumpIntoFile: Boolean;
begin
  Result := False;
// a plug
end;

function TevServerStatistics.GetEnabled: Boolean;
begin
  Result := FEnabled;
end;

function TevServerStatistics.GetFileName: String;
begin
  Result := '';
// a plug
end;

procedure TevServerStatistics.SetDumpIntoFile(const AValue: Boolean);
begin
// a plug
end;

procedure TevServerStatistics.SetEnabled(const AValue: Boolean);
begin
  FEnabled := AValue;
end;

procedure TevServerStatistics.SetFileName(const AValue: String);
begin
// a plug
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetStatistics, IevStatistics, 'Statistic Module');


end.
