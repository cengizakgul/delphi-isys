unit EvStatisticsCltMod;

interface

uses SysUtils, isBaseClasses, isStatistics, EvMainboard, evCommonInterfaces,
     isBasicUtils, isAppIDs, isThreadManager;

implementation

type
  TevFileWriteThread = class;

  TevClientStatistics = class(TisStatistics, IevStatistics)
  private
    FEnabled: Boolean;
    FThread: TevFileWriteThread;
    FDumpIntoFile: Boolean;
    FFileName: String;
    function GetDefaultFileName: String;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure OnFinishTopEvent; override;
    function  GetEnabled: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetDumpIntoFile: Boolean;
    procedure SetDumpIntoFile(const AValue: Boolean);
    function  GetFileName: String;
    procedure SetFileName(const AValue: String);
  end;

  // Write file in background so it won't slow down actual process
  TevFileWriteThread = class(TReactiveThread)
  private
    FOwner: TevClientStatistics;
    FFinishedEvents: IisInterfaceList;
  protected
    procedure DoWork; override;
  public
    constructor Create(const AOwner: TevClientStatistics); reintroduce;
  end;

function GetStatistics: IisStatistics;
begin
  Result := TevClientStatistics.Create;
end;

{ TevClientStatistics }

function TevClientStatistics.GetEnabled: Boolean;
begin
  Result := FEnabled;
end;

procedure TevClientStatistics.SetEnabled(const AValue: Boolean);
begin
  if AValue <> FEnabled then
  begin
    FEnabled := AValue;
    if not FEnabled then
      FreeAndNil(FThread);
  end;
end;

procedure TevClientStatistics.DoOnConstruction;
begin
  inherited;
end;

procedure TevClientStatistics.OnFinishTopEvent;
begin
  inherited;

  if GetDumpIntoFile then
  begin
    if not Assigned(FThread) then
      FThread := TevFileWriteThread.Create(Self);

    FThread.FFinishedEvents.Add(TopEvent);
    Clear;
    FThread.Wakeup;
  end;  
end;

procedure TevClientStatistics.DoOnDestruction;
begin
  SetEnabled(False);
  inherited;  
end;

function TevClientStatistics.GetDumpIntoFile: Boolean;
begin
  Result := FDumpIntoFile;
end;

function TevClientStatistics.GetFileName: String;
begin
  if FFileName = '' then
    Result := GetDefaultFileName
  else
    Result := FFileName;
end;

procedure TevClientStatistics.SetDumpIntoFile(const AValue: Boolean);
begin
  FDumpIntoFile := AValue;
end;

procedure TevClientStatistics.SetFileName(const AValue: String);
begin
  FFileName := AValue;
end;

function TevClientStatistics.GetDefaultFileName: String;
begin
  Result := AppDir + 'Statistics\' + AppInfoByID(AppID).Abbr + '_' + FormatDateTime('mmddyy', Date) + '.ips';
end;

{ TevFileWriteThread }

constructor TevFileWriteThread.Create(const AOwner: TevClientStatistics);
begin
  FOwner := AOwner;
  inherited Create('Statistic file writer');
  FFinishedEvents := TisInterfaceList.Create(nil, True);
end;

procedure TevFileWriteThread.DoWork;
var
  StatFile: IisStatisticsFile;
  InterProcessLock: IisLock;
  sFile: String;
  L: IisInterfaceList;
  i: Integer;
begin
  FFinishedEvents.Lock;
  try
    L := FFinishedEvents.GetClone;
    FFinishedEvents.Clear;
  finally
    FFinishedEvents.Unlock;
  end;

  sFile := FOwner.GetFileName;
  InterProcessLock := TisInterProcessLock.Create(sFile);
  InterProcessLock.Lock;
  try
    StatFile := TisStatisticsFile.Create(sFile);
    for i := 0 to L.Count - 1 do
      StatFile.AppendEvent(L[i] as IisStatEvent);
  finally
    StatFile := nil;
    InterProcessLock.Unlock;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetStatistics, IevStatistics, 'Statistic Module');


end.
