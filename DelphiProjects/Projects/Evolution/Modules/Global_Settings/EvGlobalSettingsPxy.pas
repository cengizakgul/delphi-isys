unit EvGlobalSettingsPxy;

interface

uses
  SysUtils, SyncObjs, isBaseClasses, isSettings, EvSettings, EvCommonInterfaces, EvMainboard,
  evConsts, EvTypes, isBasicUtils, SEncryptionRoutines, EvStreamUtils, EvProxy, evRemoteMethods,
  evTransportInterfaces, EvBasicUtils, isLogFile;

implementation

type
  TevGlobalSettings = class(TevProxyModule, IevSettings)
  private
    FLoaded: Boolean;
    FEnvironmentID: String;
    FDBConPoolSize: Integer;
    FDBAdminPassword: String;
    FDBUserPassword: String;
    FDomainInfoList: IevDomainInfoList;
    FEMailInfo: IevEMailInfo;
    procedure CheckLoad;
  protected
    procedure DoOnConstruction; override;
    procedure OnDisconnect; override;
    procedure Load;
    procedure Save;
    function  DomainInfoList: IevDomainInfoList;
    function  EMailInfo: IevEMailInfo;
    function  GetEnvironmentID: String;
    procedure SetEnvironmentID(const AValue: String);
    function  GetDBConPoolSize: Integer;
    procedure SetDBConPoolSize(const AValue: Integer);
    function  GetDBAdminPassword: String;
    procedure SetDBAdminPassword(const AValue: String);
    function  GetDBUserPassword: String;
    procedure SetDBUserPassword(const AValue: String);
    function  InternalRR: IisStringList;
  end;


function GetGlobalSettings: IevSettings;
begin
  Result := TevGlobalSettings.Create;
end;



{ TevGlobalSettings }

procedure TevGlobalSettings.CheckLoad;
begin
  Lock;
  try
    if not FLoaded then
      Load;
  finally
    Unlock;
  end;
end;

function TevGlobalSettings.DomainInfoList: IevDomainInfoList;
begin
  CheckLoad;
  Result := FDomainInfoList;
end;

procedure TevGlobalSettings.DoOnConstruction;
begin
  inherited;
  InitLock;

  // Placeholders
  FDomainInfoList := TevDomainInfoList.Create;
  FEMailInfo := TevEMailInfo.Create;
end;

function TevGlobalSettings.EMailInfo: IevEMailInfo;
begin
  CheckLoad;
  Result := FEMailInfo;
end;

function TevGlobalSettings.GetDBAdminPassword: String;
begin
  CheckLoad;
  Result := FDBAdminPassword;
end;

function TevGlobalSettings.GetDBConPoolSize: Integer;
begin
  CheckLoad;
  Result := FDBConPoolSize;
end;

function TevGlobalSettings.GetDBUserPassword: String;
begin
  CheckLoad;
  Result := FDBUserPassword;
end;

function TevGlobalSettings.GetEnvironmentID: String;
begin
  CheckLoad;
  Result := FEnvironmentID;
end;

function TevGlobalSettings.InternalRR: IisStringList;
begin
  NotRemotableMethod;
end;

procedure TevGlobalSettings.Load;
var
  D: IevDatagram;
  bSuccessful: Boolean;
begin
  bSuccessful := False;

  Lock;
  try
    Mainboard.ContextManager.StoreThreadContext;
    try
      // IMPORTANT! EXCEPTION OF RULES!!!
      // Guest account is internal and doesn't require authentication by security
      // otherwise system would lockup by security authentication feedback
      Mainboard.ContextManager.CreateThreadContext(sGuestUserName, sDefaultDomain);

      D := CreateDatagram(METHOD_GLOBALSETTINGS_GET);
      try
        D := SendRequest(D, False);
        bSuccessful := True;
      except
        // suppress all exceptions if no feedback request can be made at this moment
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

    if bSuccessful then
    begin
      FEnvironmentID := D.Params.Value['EnvironmentID'];
      FDBConPoolSize := D.Params.Value['DBConPoolSize'];
      FDBAdminPassword := D.Params.Value['DBAdminPassword'];
      FDBUserPassword := D.Params.Value['DBUserPassword'];      
      FEMailInfo := IInterface(D.Params.Value['EMailInfo']) as IevEMailInfo;
      FDomainInfoList := IInterface(D.Params.Value['DomainInfoList']) as IevDomainInfoList;

      FLoaded := True;
    end;

  finally
    Unlock;
  end;

  if bSuccessful then
    Mainboard.LogFile.AddEvent(etInformation, LOG_EVENT_CLASS_BUSINESSLOGIC, 'System configuration is refreshed', '');
end;

procedure TevGlobalSettings.OnDisconnect;
begin
  Lock;
  try
    FLoaded := False;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.Save;
begin
  NotRemotableMethod;
end;

procedure TevGlobalSettings.SetDBAdminPassword(const AValue: String);
begin
  NotRemotableMethod;
end;

procedure TevGlobalSettings.SetDBConPoolSize(const AValue: Integer);
begin
  NotRemotableMethod;
end;

procedure TevGlobalSettings.SetDBUserPassword(const AValue: String);
begin
  NotRemotableMethod;
end;

procedure TevGlobalSettings.SetEnvironmentID(const AValue: String);
begin
  NotRemotableMethod;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalSettings, IevSettings, 'Global Settings');

end.
