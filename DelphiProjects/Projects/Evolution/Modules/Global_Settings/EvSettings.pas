unit EvSettings;

interface

uses Classes, Windows, SysUtils, isBaseClasses, EvCommonInterfaces, EvUtils, isBasicUtils,
     EvConsts, EvStreamUtils, EvTypes, EvBasicUtils;

type
  // Evolution DB location handler

  TevDBLocation = class(TisInterfacedObject, IevDBLocation)
  private
    FDBType: TevDBType;
    FPath: String;
    FBeginRangeNbr: Cardinal;
    FEndRangeNbr: Cardinal;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetDBType: TevDBType;
    function  GetPath: String;
    function  GetBeginRangeNbr: Cardinal;
    function  GetEndRangeNbr: Cardinal;
    procedure SetDBType(const AValue: TevDBType);
    procedure SetPath(const AValue: String);
    procedure SetBeginRangeNbr(const AValue: Cardinal);
    procedure SetEndRangeNbr(const AValue: Cardinal);
  public
    class function GetTypeID: String; override;
  end;


  TevDBLocationList = class(TisCollection, IevDBLocationList)
  protected
    procedure DoOnConstruction; override;
    procedure Clear;
    function  Count: Integer;
    function  Add: IevDBLocation;
    procedure Delete(const ADBLocation: IevDBLocation);
    function  GetItem(AIndex: Integer): IevDBLocation;
    function  GetDBPath(const ADatabaseName: String): String;
  public
    class function GetTypeID: String; override;
  end;


  TevFileFolders = class(TisInterfacedObject, IevFileFolders)
  private
    FTaskQueueFolder: String;
    FVMRFolder: String;
    FACHFolder: String;
    FASCIIPrefix: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetTaskQueueFolder: String;
    procedure SetTaskQueueFolder(const AValue: String);
    function  GetVMRFolder: String;
    procedure SetVMRFolder(const AValue: String);
    function  GetACHFolder: String;
    procedure SetACHFolder(const AValue: String);
    function  GetASCIIPrefix: String;
    procedure SetASCIIPrefix(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;


  TevSystemAccountInfo = class(TisInterfacedObject, IevSystemAccountInfo)
  private
    FUserName: String;
    FPassword: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
    function  GetPassword: String;
  public
    class function GetTypeID: String; override;
  end;


  TevLicenseInfo = class(TisInterfacedObject, IevLicenseInfo)
  private
    FSerialNumber: String;
    FCode: String;
    FLastChange: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetSerialNumber: String;
    procedure SetSerialNumber(const AValue: String);
    function  GetCode: String;
    procedure SetCode(const AValue: String);
    function  GetLastChange: String;
    procedure SetLastChange(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;


  TevBrandInfo = class(TisInterfacedObject, IevBrandInfo)
  private
    FLogoPicture: TGraphicData;
    FTitlePicture: TGraphicData;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetLogoPicture: TGraphicData;
    function  GetTitlePicture: TGraphicData;
    procedure SetLogoPicture(const APicture: TGraphicData);
    procedure SetTitlePicture(const APicture: TGraphicData);
  public
    class function GetTypeID: String; override;
  end;


  TevDomainInfo = class(TisCollection, IevDomainInfo)
  private
    FDomainDescription: String;
    FDBChangesBroadcast: Boolean;
    FInternalUsersThroughRR: Boolean;
    FAdminAccount: String;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetDomainName: String;
    function  GetDomainDescription: String;
    procedure SetDomainName(const AValue: String);
    procedure SetDomainDescription(const AValue: String);
    function  GetDBChangesBroadcast: Boolean;
    procedure SetDBChangesBroadcast(const AValue: Boolean);
    function  DBLocationList: IevDBLocationList;
    function  ADRDBLocationList: IevDBLocationList;
    function  FileFolders: IevFileFolders;
    function  LicenseInfo: IevLicenseInfo;
    function  BrandInfo: IevBrandInfo;
    function  SystemAccountInfo: IevSystemAccountInfo;
    function  GetInternalUsersThroughRR: Boolean;
    procedure SetInternalUsersThroughRR(const AValue: Boolean);
    function  GetAdminAccount: String;
    procedure SetAdminAccount(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;


  TevDomainInfoList = class(TisCollection, IevDomainInfoList)
  protected
    procedure DoOnConstruction; override;
    function  Count: Integer;
    function  Add: IevDomainInfo;
    procedure Delete(const ADomain: IevDomainInfo);
    function  GetDomainInfo(const ADomainName: String): IevDomainInfo;
    function  GetItem(AIndex: Integer): IevDomainInfo;
  public
    class function GetTypeID: String; override;
  end;

  TevEMailInfo = class(TisInterfacedObject, IevEMailInfo)
  private
    FSMTPServer: String;
    FSMTPPort: String;
    FUserName: String;
    FPassword: String;
    FNotificationDomainName: String;
  protected
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetSMTPServer: String;
    procedure SetSMTPServer(const AValue: String);
    function  GetSMTPPort: String;
    procedure SetSMTPPort(const AValue: String);
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  GetNotificationDomainName: String;
    procedure SetNotificationDomainName(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;


implementation

{ TevDomainInfoList }

function TevDomainInfoList.Add: IevDomainInfo;
begin
  Result := TevDomainInfo.Create(Self, False) as IevDomainInfo;
end;

function TevDomainInfoList.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevDomainInfoList.Delete(const ADomain: IevDomainInfo);
begin
  RemoveChild(ADomain as IisInterfacedObject);
end;

procedure TevDomainInfoList.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

function TevDomainInfoList.GetDomainInfo(const ADomainName: String): IevDomainInfo;
begin
  Result := FindChildByName(ADomainName) as IevDomainInfo;
end;

function TevDomainInfoList.GetItem(AIndex: Integer): IevDomainInfo;
begin
  Result := GetChild(AIndex) as IevDomainInfo;
end;

class function TevDomainInfoList.GetTypeID: String;
begin
  Result := 'DomainInfoList';
end;

{ TevDBLocationList }

function TevDBLocationList.Add: IevDBLocation;
begin
  Result := TevDBLocation.Create(Self, False) as IevDBLocation;
end;

procedure TevDBLocationList.Clear;
begin
  RemoveAllChildren;
end;

function TevDBLocationList.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevDBLocationList.Delete(const ADBLocation: IevDBLocation);
begin
  RemoveChild(ADBLocation as IisInterfacedObject);
end;

procedure TevDBLocationList.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

function TevDBLocationList.GetDBPath(const ADatabaseName: String): String;
var
  i:Integer;
  dbType: TevDBType;
  ClNbr: Cardinal;
  s: String;
  Location: IevDBLocation;
  dbName, DomainName: String;
  O: IisCollection;
begin
  Result := '';

  Location := nil;
  dbType := dbtUnknown;
  ClNbr := 0;

  // Identify DB type
  if StartsWith(ADatabaseName, 'SY') then
  begin
    dbType := dbtSystem;
    dbName := 'SYSTEM.gdb';
  end

  else if StartsWith(ADatabaseName, 'S_') then
  begin
    dbType := dbtBureau;
    dbName := 'S_BUREAU.gdb';
  end

  else if StartsWith(ADatabaseName, 'T') then
  begin
    dbType := dbtTemporary;
    dbName := 'TMP_TBLS.gdb';
  end

  else if StartsWith(ADatabaseName, 'C') then
  begin
    dbType := dbtClient;
    s := ADatabaseName;
    GetNextStrValue(s, '_');
    s := GetNextStrValue(s, '.');
    if AnsiSameText(s, 'BASE') then
      dbName := 'CL_BASE.gdb'
    else
    begin
      ClNbr := StrToInt(s);
      dbName := 'CL_' + s + '.gdb';
    end;
  end

  else
    Assert(False);

  // looking for location
  for i := 0 to Count - 1 do
  begin
    if GetItem(i).DBType = dbType then
      if (dbType <> dbtClient) or (ClNbr >= GetItem(i).BeginRangeNbr) and (ClNbr <= GetItem(i).EndRangeNbr) then
      begin
        Location := GetItem(i);
        break;
      end;
  end;

  if not Assigned(Location) then
  begin
    // Assign default location
    for i := 0 to Count - 1 do
    begin
      if GetItem(i).DBType = dbtUnspecified then
      begin
        Location := GetItem(i);
        break;
      end;
    end;
  end;

  DomainName := '';
  if Assigned(Location) then
  begin
    Result := NormalizePath(Location.Path) + dbName;

    O := GetOwner;
    if Assigned(O) then
      DomainName := (O as IevDomainInfo).DomainName;
  end;
  CheckCondition(Result <> '', Format('DB location is unknown. DB: %s  Domain: %s', [dbName, DomainName]));
end;

function TevDBLocationList.GetItem(AIndex: Integer): IevDBLocation;
begin
  Result := GetChild(AIndex) as IevDBLocation;
end;

class function TevDBLocationList.GetTypeID: String;
begin
  Result := 'DBLocationList';
end;


{ TevDBLocation }

function TevDBLocation.GetBeginRangeNbr: Cardinal;
begin
  Result := FBeginRangeNbr;
end;

function TevDBLocation.GetDBType: TevDBType;
begin
  Result := FDBType;
  if FDBType <> dbtClient then
  begin
    FBeginRangeNbr := 0;
    FEndRangeNbr := 0;
  end;  
end;

function TevDBLocation.GetEndRangeNbr: Cardinal;
begin
  Result := FEndRangeNbr;
end;

function TevDBLocation.GetPath: String;
begin
  Result := FPath;
end;

procedure TevDBLocation.SetBeginRangeNbr(const AValue: Cardinal);
begin
  if FDBType = dbtClient then
    FBeginRangeNbr := AValue;
end;

procedure TevDBLocation.SetDBType(const AValue: TevDBType);
begin
  FDBType := AValue;
end;

procedure TevDBLocation.SetEndRangeNbr(const AValue: Cardinal);
begin
  if FDBType = dbtClient then
    FEndRangeNbr := AValue;
end;

procedure TevDBLocation.SetPath(const AValue: String);
begin
  FPath := AValue;
end;

class function TevDBLocation.GetTypeID: String;
begin
  Result := 'DBLocation';
end;


procedure TevDBLocation.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FDBType := TevDBType(AStream.ReadByte);
  FPath := AStream.ReadString;
  FBeginRangeNbr := AStream.ReadInteger;
  FEndRangeNbr := AStream.ReadInteger;
end;

procedure TevDBLocation.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteByte(Ord(FDBType));
  AStream.WriteString(FPath);
  AStream.WriteInteger(FBeginRangeNbr);
  AStream.WriteInteger(FEndRangeNbr);
end;

{ TevDomainInfo }

function TevDomainInfo.ADRDBLocationList: IevDBLocationList;
begin
  Result := GetChild(5) as IevDBLocationList;
end;

function TevDomainInfo.BrandInfo: IevBrandInfo;
begin
  Result := GetChild(4) as IevBrandInfo;
end;

function TevDomainInfo.DBLocationList: IevDBLocationList;
begin
  Result := GetChild(0) as IevDBLocationList;
end;

procedure TevDomainInfo.DoOnConstruction;
var
  Obj: IisInterfacedObject;
begin
  inherited;
  InitLock;

  Obj := TevDBLocationList.Create(Self, False);
  Obj := TevFileFolders.Create(Self, False);
  Obj := TevSystemAccountInfo.Create(Self, False);
  Obj := TevLicenseInfo.Create(Self, False);
  Obj := TevBrandInfo.Create(Self, False);
  Obj := TevDBLocationList.Create(Self, False);

  FInternalUsersThroughRR := True;
  FAdminAccount := sDefaultAdminUserName;
end;

function TevDomainInfo.FileFolders: IevFileFolders;
begin
  Result := GetChild(1) as IevFileFolders;
end;

function TevDomainInfo.GetAdminAccount: String;
begin
  Result := FAdminAccount;
end;

function TevDomainInfo.GetDBChangesBroadcast: Boolean;
begin
  Result := FDBChangesBroadcast;
end;

function TevDomainInfo.GetDomainDescription: String;
begin
  Result := FDomainDescription;
end;

function TevDomainInfo.GetDomainName: String;
begin
  Result := GetName;
end;

function TevDomainInfo.GetInternalUsersThroughRR: Boolean;
begin
  Result := FInternalUsersThroughRR;
end;

class function TevDomainInfo.GetTypeID: String;
begin
  Result := 'DomainInfo';
end;

function TevDomainInfo.LicenseInfo: IevLicenseInfo;
begin
  Result := GetChild(3) as IevLicenseInfo;
end;

procedure TevDomainInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  SetDomainName(AStream.ReadShortString);
  FDomainDescription := AStream.ReadShortString;
  FDBChangesBroadcast := AStream.ReadBoolean;
  FInternalUsersThroughRR := AStream.ReadBoolean;
  FAdminAccount := AStream.ReadShortString;  
end;

procedure TevDomainInfo.SetAdminAccount(const AValue: String);
begin
  if AValue = '' then
    FAdminAccount := sDefaultAdminUserName
  else
    FAdminAccount := AValue;
end;

procedure TevDomainInfo.SetDBChangesBroadcast(const AValue: Boolean);
begin
  FDBChangesBroadcast := AValue;
end;

procedure TevDomainInfo.SetDomainDescription(const AValue: String);
begin
  FDomainDescription := AValue;
end;

procedure TevDomainInfo.SetDomainName(const AValue: String);
begin
  SetName(AValue);
end;

procedure TevDomainInfo.SetInternalUsersThroughRR(const AValue: Boolean);
begin
  FInternalUsersThroughRR := AValue;
end;

function TevDomainInfo.SystemAccountInfo: IevSystemAccountInfo;
begin
  Result := GetChild(2) as IevSystemAccountInfo;
end;

procedure TevDomainInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(GetDomainName);
  AStream.WriteShortString(FDomainDescription);
  AStream.WriteBoolean(FDBChangesBroadcast);
  AStream.WriteBoolean(FInternalUsersThroughRR);
  AStream.WriteShortString(FAdminAccount);  
end;

{ TevFileFolders }

function TevFileFolders.GetACHFolder: String;
begin
  Result := FACHFolder;
end;

function TevFileFolders.GetASCIIPrefix: String;
begin
  Result := FASCIIPrefix;
end;

function TevFileFolders.GetTaskQueueFolder: String;
begin
  Result := FTaskQueueFolder;
end;

class function TevFileFolders.GetTypeID: String;
begin
  Result := 'FileFolders';
end;

function TevFileFolders.GetVMRFolder: String;
begin
  Result := FVMRFolder;
end;

procedure TevFileFolders.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FTaskQueueFolder := AStream.ReadString;
  FVMRFolder := AStream.ReadString;
  FACHFolder := AStream.ReadString;
  FASCIIPrefix := AStream.ReadString;
end;

procedure TevFileFolders.SetACHFolder(const AValue: String);
begin
  FACHFolder := AValue;
end;

procedure TevFileFolders.SetASCIIPrefix(const AValue: String);
begin
  FASCIIPrefix := AValue;
end;

procedure TevFileFolders.SetTaskQueueFolder(const AValue: String);
begin
  FTaskQueueFolder := AValue;
end;

procedure TevFileFolders.SetVMRFolder(const AValue: String);
begin
  FVMRFolder := AValue;
end;

procedure TevFileFolders.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FTaskQueueFolder);
  AStream.WriteString(FVMRFolder);
  AStream.WriteString(FACHFolder);
  AStream.WriteString(FASCIIPrefix);
end;

{ TevEMailInfo }

function TevEMailInfo.GetNotificationDomainName: String;
begin
  Result := FNotificationDomainName;
end;

function TevEMailInfo.GetPassword: String;
begin
  Result := FPassword;
end;

function TevEMailInfo.GetSMTPPort: String;
begin
  Result := FSMTPPort;
end;

function TevEMailInfo.GetSMTPServer: String;
begin
  Result := FSMTPServer;
end;

class function TevEMailInfo.GetTypeID: String;
begin
  Result := 'EMailInfo';
end;

function TevEMailInfo.GetUserName: String;
begin
  Result := FUserName;
end;

procedure TevEMailInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FSMTPServer := AStream.ReadShortString;
  FSMTPPort := AStream.ReadShortString;
  FUserName := AStream.ReadShortString;
  FNotificationDomainName := AStream.ReadShortString;
end;

procedure TevEMailInfo.SetNotificationDomainName(const AValue: String);
begin
  FNotificationDomainName := Trim(AValue);
  if Contains(FNotificationDomainName, '@') then
    GetNextStrValue(FNotificationDomainName, '@');
end;

procedure TevEMailInfo.SetPassword(const AValue: String);
begin
  FPassword := AValue;
end;

procedure TevEMailInfo.SetSMTPPort(const AValue: String);
begin
  FSMTPPort := AValue;
end;

procedure TevEMailInfo.SetSMTPServer(const AValue: String);
begin
  FSMTPServer := AValue;
end;

procedure TevEMailInfo.SetUserName(const AValue: String);
begin
  FUserName := AValue;
end;

procedure TevEMailInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FSMTPServer);
  AStream.WriteShortString(FSMTPPort);
  AStream.WriteShortString(FUserName);
  AStream.WriteShortString(FNotificationDomainName);
end;

{ TevSystemAccountInfo }

function TevSystemAccountInfo.GetPassword: String;
begin
  Result := FPassword;
end;

class function TevSystemAccountInfo.GetTypeID: String;
begin
  Result := 'SystemAccountInfo';
end;

function TevSystemAccountInfo.GetUserName: String;
begin
  Result := FUserName;
end;

procedure TevSystemAccountInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FUserName := AStream.ReadShortString;
  FPassword := AStream.ReadShortString;
end;

procedure TevSystemAccountInfo.SetUserName(const AValue: String);
begin
  if FUserName <> AValue then
    FPassword := HashPassword(GetUniqueID);
  FUserName := AValue;
end;

procedure TevSystemAccountInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FUserName);
  AStream.WriteShortString(FPassword);
end;

{ TevLicenseInfo }

function TevLicenseInfo.GetCode: String;
begin
  Result := FCode;
end;

function TevLicenseInfo.GetLastChange: String;
begin
  Result := FLastChange;
end;

function TevLicenseInfo.GetSerialNumber: String;
begin
  Result := FSerialNumber;
end;

class function TevLicenseInfo.GetTypeID: String;
begin
  Result := 'LicenseInfo';
end;

procedure TevLicenseInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FSerialNumber := AStream.ReadString;
  FCode := AStream.ReadString;
  FLastChange := AStream.ReadString;
end;

procedure TevLicenseInfo.SetCode(const AValue: String);
begin
  if FCode <> AValue then
  begin
    FCode := AValue;
    FLastChange := DateTimeToStr(Now);
  end;  
end;

procedure TevLicenseInfo.SetLastChange(const AValue: String);
begin
  FLastChange := AValue;
end;

procedure TevLicenseInfo.SetSerialNumber(const AValue: String);
begin
  FSerialNumber := AValue;
end;

procedure TevLicenseInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FSerialNumber);
  AStream.WriteString(FCode);
  AStream.WriteString(FLastChange);
end;

{ TevBrandInfo }

class function TevBrandInfo.GetTypeID: String;
begin
  Result := 'BrandInfo';
end;

function TevBrandInfo.GetLogoPicture: TGraphicData;
begin
  Result := FLogoPicture;
end;

procedure TevBrandInfo.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FLogoPicture.FileExt := AStream.ReadShortString;
  FLogoPicture.Data := AStream.ReadStream(nil, True);
  FTitlePicture.FileExt := AStream.ReadShortString;
  FTitlePicture.Data := AStream.ReadStream(nil, True);
end;

procedure TevBrandInfo.SetLogoPicture(const APicture: TGraphicData);
begin
  FLogoPicture := APicture;
end;

procedure TevBrandInfo.SetTitlePicture(const APicture: TGraphicData);
begin
  FTitlePicture := APicture;
end;

function TevBrandInfo.GetTitlePicture: TGraphicData;
begin
  Result := FTitlePicture;
end;

procedure TevBrandInfo.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FLogoPicture.FileExt);
  AStream.WriteStream(FLogoPicture.Data);
  AStream.WriteShortString(FTitlePicture.FileExt);
  AStream.WriteStream(FTitlePicture.Data);
end;


initialization
  ObjectFactory.Register([TevDBLocation, TevDBLocationList, TevFileFolders, TevDomainInfo,
    TevDomainInfoList, TevSystemAccountInfo,
    TevLicenseInfo, TevBrandInfo, TevEMailInfo]);

finalization
  SafeObjectFactoryUnRegister([TevDBLocation, TevDBLocationList, TevFileFolders, TevDomainInfo,
    TevDomainInfoList, TevSystemAccountInfo,
    TevLicenseInfo, TevBrandInfo, TevEMailInfo]);

end.
