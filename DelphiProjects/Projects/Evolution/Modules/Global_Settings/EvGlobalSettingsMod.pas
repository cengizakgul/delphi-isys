unit EvGlobalSettingsMod;

interface

uses
  SysUtils, isBaseClasses, isSettings, EvSettings, EvCommonInterfaces, EvMainboard, evConsts, EvTypes,
  isBasicUtils, SEncryptionRoutines, EvStreamUtils, evUtils, Classes, isLogFile;

implementation

type
  TevGlobalSettings = class(TisCollection, IevSettings)
  private
    FEnvironmentID: String;
    FDomainInfoList: IevDomainInfoList;
    FEMailInfo: IevEMailInfo;
    FDBConPoolSize: Integer;
    FDBAdminPassword: String;
    FDBUserPassword: String;
    FInternalRR: IisStringList;
    procedure LoadGeneral;
    procedure LoadEMail;
    procedure LoadDomains;
    procedure LoadDBInfo;
    procedure LoadMiscSystemAccount;
    procedure LoadMiscAdminAccount;
    procedure LoadMiscFileFolders;
    procedure LoadMiscGeneral;
    procedure LoadMiscLicense;
    procedure SaveGeneral;
    procedure SaveEMail;
    procedure SaveDomains;
    procedure SaveDBInfo;
    procedure SaveMiscSystemAccount;
    procedure SaveMiscAdminAccount;
    procedure SaveMiscFileFolders;
    procedure SaveMiscGeneral;
    procedure SaveMiscLicense;
  protected
    procedure DoOnConstruction; override;
    procedure WriteToStream(const AStream: IEvDualStream); override;
    procedure ReadFromStream(const AStream: IEvDualStream); override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure Load;
    procedure Save;
    function  DomainInfoList: IevDomainInfoList;
    function  EMailInfo: IevEMailInfo;
    function  GetEnvironmentID: String;
    procedure SetEnvironmentID(const AValue: String);
    function  GetDBConPoolSize: Integer;
    procedure SetDBConPoolSize(const AValue: Integer);
    function  GetDBAdminPassword: String;
    procedure SetDBAdminPassword(const AValue: String);
    function  GetDBUserPassword: String;
    procedure SetDBUserPassword(const AValue: String);
    function  InternalRR: IisStringList;
  end;

function GetGlobalSettings: IevSettings;
begin
  Result := TevGlobalSettings.Create;
end;


const
  aDBTypes: array [dbtUnspecified..dbtTemporary] of string = ('Default', 'System', 'ServiceBureau', 'Client', 'Temporary');


{ TevGlobalSettings }

function TevGlobalSettings.DomainInfoList: IevDomainInfoList;
begin
  Lock;
  try
    Result := FDomainInfoList;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.DoOnConstruction;
begin
  inherited;
  InitLock;
  Load;
end;

function TevGlobalSettings.EMailInfo: IevEMailInfo;
begin
  Lock;
  try
    Result := FEMailInfo;
  finally
    Unlock;
  end;
end;

function TevGlobalSettings.GetDBAdminPassword: String;
begin
  Lock;
  try
    Result := FDBAdminPassword;
  finally
    Unlock;
  end;
end;

function TevGlobalSettings.GetDBConPoolSize: Integer;
begin
  Lock;
  try
    Result := FDBConPoolSize;
  finally
    Unlock;
  end;
end;

function TevGlobalSettings.GetDBUserPassword: String;
begin
  Lock;
  try
    Result := FDBUserPassword;
  finally
    Unlock;
  end;
end;

function TevGlobalSettings.GetEnvironmentID: String;
begin
  Lock;
  try
    Result := FEnvironmentID;
  finally
    Unlock;
  end;
end;

function TevGlobalSettings.InternalRR: IisStringList;
begin
  Lock;
  try
    Result := FInternalRR;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.Load;
var
  DomainInfo: IevDomainInfo;
begin
  Lock;
  try
    FInternalRR := TisStringList.Create;
    FDomainInfoList := TevDomainInfoList.Create;
    FEMailInfo := TevEMailInfo.Create;

    LoadGeneral;
    LoadDomains;
    LoadMiscLicense;
    LoadMiscFileFolders;
    LoadMiscSystemAccount;
    LoadMiscAdminAccount;    
    LoadMiscGeneral;
    LoadDBInfo;
    LoadEMail;

    // Multi-domain environment should not allow use default domain 
    if DomainInfoList.Count > 1 then
    begin
      DomainInfo := DomainInfoList.GetDomainInfo(sDefaultDomain);
      DomainInfoList.Delete(DomainInfo);
      DomainInfo := DomainInfoList.Add;         // but Default domain should exist
      DomainInfo.DomainName := sDefaultDomain;
      DomainInfo.DomainDescription := 'Default SB';
    end;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.LoadDBInfo;

  procedure LoadDBLocations(const ADBLocations: IevDBLocationList; const ASection: String);
  var
    i: Integer;
    sRange, sDBName: String;
    DBType: TevDBType;
    DBLoc: IevDBLocation;
    ValList: IisStringListRO;
  begin                    
    ValList := mb_AppConfiguration.GetValueNames(ASection);
    for i := 0 to ValList.Count - 1 do
    begin
      DBLoc := ADBLocations.Add;

      sRange := ValList[i];
      sDBName := GetNextStrValue(sRange, '[');
      sRange := GetNextStrValue(sRange, ']');
      DBLoc.Path := mb_AppConfiguration.AsString[ASection + '\' + ValList[i]];

      for DBType := Low(aDBTypes) to High(aDBTypes) do
        if AnsiSameText(aDBTypes[DBType], sDBName) then
        begin
          DBLoc.DBType := DBType;
          break;
        end;

      if DbLoc.DBType = dbtClient then
      begin
        DBLoc.BeginRangeNbr := StrToInt(Trim(GetNextStrValue(sRange, '-')));
        DBLoc.EndRangeNbr := StrToInt(Trim(sRange));
      end;
    end;
  end;

var
  i: Integer;
  DomainInfo: IevDomainInfo;
  sSect: String;
begin
  // DB Path
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    sSect := 'DB\Path';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sSect := sSect + '\' + DomainInfo.DomainName;
    LoadDBLocations(DomainInfo.DBLocationList, sSect);

    sSect := 'DB\ADRPath';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sSect := sSect + '\' + DomainInfo.DomainName;
    LoadDBLocations(DomainInfo.ADRDBLocationList, sSect)
  end;
end;

procedure TevGlobalSettings.LoadDomains;
var
  ItemList: IisStringList;
  i: Integer;
  DomainInfo: IevDomainInfo;
  s: String;  

  procedure LoadBrandInfo(const AFileName: String; const ABrandInfo: IevBrandInfo);
  var
    fs: IevDualStream;

    function ReadPictureInfo: TGraphicData;
    var
      s: String;
      ms1, ms2: IEvDualStream;
      i: Integer;
    begin
      fs.ReadBuffer(i, SizeOf(i));
      if i > 0 then
      begin
        SetLength(s, 3);
        fs.ReadBuffer(s[1], 3);
        ms1 := TEvDualStreamHolder.Create;
        ms2 := TEvDualStreamHolder.Create;
        ms1.CopyFrom(fs, i);
        DecryptMemoryStream(ms1.RealStream, ms2.RealStream);
        if ms2.Size > 0 then
        begin
          Result.FileExt := s;
          Result.Data := ms2;
          ms2.Position := 0;
        end;
      end;
    end;

  begin
    fs := TEvDualStreamHolder.CreateFromFile(AFileName);
    ABrandInfo.LogoPicture := ReadPictureInfo;
    ABrandInfo.TitlePicture := ReadPictureInfo;
  end;

begin
  ItemList := mb_AppConfiguration.GetValueNames('Domain') as IisStringList;

  // The multi-domain feature is allowed only for iSystems!
  if not IsQAMode then
    for i := ItemList.Count - 1 downto 0 do
      if not AnsiSameText(ItemList[i], sDefaultDomain) then
        ItemList.Delete(i);

  for i := 0 to ItemList.Count - 1 do
    if DomainInfoList.GetDomainInfo(ItemList[i]) = nil then
    begin
      DomainInfo := DomainInfoList.Add;
      DomainInfo.DomainName := ItemList[i];
      DomainInfo.DomainDescription := mb_AppConfiguration.AsString['Domain\' + DomainInfo.DomainName];
    end;

  if DomainInfoList.GetDomainInfo(sDefaultDomain) = nil then
  begin
    DomainInfo := DomainInfoList.Add;
    DomainInfo.DomainName := sDefaultDomain;
    DomainInfo.DomainDescription := 'Default SB';
  end;

  // brand info
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    s := AppDir + DomainInfo.DomainName + '.brd';
    if not FileExists(s) then
      s := AppDir + 'Evolution.brd';
    if FileExists(s) then
      LoadBrandInfo(s, DomainInfo.BrandInfo);
  end;
end;

procedure TevGlobalSettings.LoadEMail;
begin
  EMailInfo.SMTPServer := mb_AppConfiguration.AsString['eMail\SMTPServer'];
  EMailInfo.SMTPPort := mb_AppConfiguration.AsString['eMail\SMTPPort'];
  EMailInfo.UserName := DecryptStringLocaly(BinToStr(mb_AppConfiguration.AsString['eMail\UserName']), '');
  EMailInfo.Password := DecryptStringLocaly(BinToStr(mb_AppConfiguration.AsString['eMail\Password']), '');
  EMailInfo.NotificationDomainName := mb_AppConfiguration.AsString['eMail\NotificationDomain'];
end;

procedure TevGlobalSettings.LoadGeneral;
begin
  FEnvironmentID := mb_AppConfiguration.GetValue('General\EnvironmentID', Mainboard.MachineInfo.IPaddress);
  FDBConPoolSize := mb_AppConfiguration.GetValue('General\DBConPoolSize', 100);
  FDBAdminPassword := DecryptStringLocaly(BinToStr(mb_AppConfiguration.AsString['General\DBAdminPassword']), '');
  FDBUserPassword := DecryptStringLocaly(BinToStr(mb_AppConfiguration.AsString['General\DBUserPassword']), '');  
  FInternalRR.CommaText := mb_AppConfiguration.AsString['General\InternalRR'];
end;

procedure TevGlobalSettings.LoadMiscAdminAccount;
var
  i: Integer;
  DomainInfo: IevDomainInfo;
  sVal: String;
begin
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    sVal := 'Misc\AdminAccount\';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sVal := sVal  + DomainInfo.DomainName + '\';

    DomainInfo.AdminAccount :=
      DecryptStringLocaly(BinToStr(mb_AppConfiguration.AsString[sVal + 'UserName']), sDefaultAdminUserName);
  end;
end;

procedure TevGlobalSettings.LoadMiscFileFolders;
var
  i: Integer;
  DomainInfo: IevDomainInfo;
  sVal: String;
begin
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    sVal := 'Misc\FileFolders\';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sVal := sVal  + DomainInfo.DomainName + '\';

    DomainInfo.FileFolders.TaskQueueFolder := mb_AppConfiguration.AsString[sVal + 'TaskQueue'];
    DomainInfo.FileFolders.VMRFolder := mb_AppConfiguration.AsString[sVal + 'VMR'];
    DomainInfo.FileFolders.ACHFolder := mb_AppConfiguration.AsString[sVal + 'ACH'];
    DomainInfo.FileFolders.ASCIIPrefix := mb_AppConfiguration.AsString[sVal + 'ASCII'];
  end;
end;

procedure TevGlobalSettings.LoadMiscGeneral;
var
  i: Integer;
  DomainInfo: IevDomainInfo;
  sVal: String;
begin
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    sVal := 'Misc\General\';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sVal := sVal  + DomainInfo.DomainName + '\';

    DomainInfo.DBChangesBroadcast := mb_AppConfiguration.AsBoolean[sVal + 'DBChangeBroadcast'];
    DomainInfo.InternalUsersThroughRR := mb_AppConfiguration.GetValue(sVal + 'IntUserThrRR', True);
  end;
end;

procedure TevGlobalSettings.LoadMiscLicense;
var
  i: Integer;
  DomainInfo: IevDomainInfo;
  sVal: String;
begin
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    sVal := 'Misc\License\';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sVal := sVal  + DomainInfo.DomainName + '\';

    DomainInfo.LicenseInfo.SerialNumber := mb_AppConfiguration.AsString[sVal + 'SerialNumber'];
    DomainInfo.LicenseInfo.Code := mb_AppConfiguration.AsString[sVal + 'Code'];
    DomainInfo.LicenseInfo.LastChange := mb_AppConfiguration.GetValue(sVal + 'LastChange', '');
  end;
end;

procedure TevGlobalSettings.LoadMiscSystemAccount;
var
  i: Integer;
  DomainInfo: IevDomainInfo;
  sVal: String;
begin
  for i := 0 to DomainInfoList.Count - 1 do
  begin
    DomainInfo := DomainInfoList[i];
    sVal := 'Misc\SystemAccount\';
    if not AnsiSameText(DomainInfo.DomainName, sDefaultDomain) then
      sVal := sVal  + DomainInfo.DomainName + '\';

    DomainInfo.SystemAccountInfo.UserName :=
      DecryptStringLocaly(BinToStr(mb_AppConfiguration.AsString[sVal + 'UserName']), '');
  end;
end;

procedure TevGlobalSettings.ReadFromStream(const AStream: IEvDualStream);
begin
  Lock;
  try
    inherited;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FEnvironmentID := AStream.ReadShortString;
  FDBConPoolSize := AStream.ReadInteger;
  FDBAdminPassword := AStream.ReadString;
  FDBUserPassword := AStream.ReadString;  
  FInternalRR.CommaText := AStream.ReadString;
  (FDomainInfoList as IisInterfacedObject).ReadFromStream(AStream);
  (FEMailInfo as IisInterfacedObject).ReadFromStream(AStream);
end;

procedure TevGlobalSettings.Save;
begin
  Lock;
  try
    SaveGeneral;
    SaveDomains;
    SaveMiscLicense;
    SaveMiscFileFolders;
    SaveMiscSystemAccount;
    SaveMiscAdminAccount;
    SaveMiscGeneral;
    SaveDBInfo;
    SaveEMail;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.SaveDBInfo;
var
  i: Integer;
  D: IevDomainInfo;
  sSect: String;
  UsedDBServers: IisStringList;

  procedure SaveDBLocations(const ADBLocations: IevDBLocationList; const ASection: String);
  var
    i: Integer;
    s, sValName: String;
  begin
    ADBLocations.Lock;
    try
      for i := 0 to ADBLocations.Count - 1 do
      begin
        sValName := aDBTypes[ADBLocations[i].DBType];
        if ADBLocations[i].DBType = dbtClient then
          sValName := sValName + '[' + IntToStr(ADBLocations[i].BeginRangeNbr) + '-' + IntToStr(ADBLocations[i].EndRangeNbr)+']';

        mb_AppConfiguration.AsString[ASection + sValName] := ADBLocations[i].Path;

        s := ADBLocations[i].Path;
        s := GetNextStrValue(s, ':');
        UsedDBServers.Add(s);
      end;
    finally
      ADBLocations.Unlock;
    end;
  end;

begin
  mb_AppConfiguration.DeleteNode('DB');

  UsedDBServers := TisStringList.CreateUnique;

  // [DB.Path.<Domain>]
  DomainInfoList.Lock;
  try
    for i := 0 to DomainInfoList.Count - 1 do
    begin
      D := DomainInfoList[i];
      sSect := 'DB\Path\';
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';
      SaveDBLocations(D.DBLocationList, sSect);

      sSect := 'DB\ADRPath\';
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';
      SaveDBLocations(D.ADRDBLocationList, sSect);
    end;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SaveDomains;
var
  i: Integer;
begin
  DomainInfoList.Lock;
  try
    mb_AppConfiguration.DeleteNode('Domain');

    for i := 0 to DomainInfoList.Count - 1 do
      mb_AppConfiguration.AsString['Domain\' + DomainInfoList[i].DomainName] := DomainInfoList[i].DomainDescription;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SaveEMail;
begin
  mb_AppConfiguration.AsString['eMail\SMTPServer'] := EMailInfo.SMTPServer;
  mb_AppConfiguration.AsString['eMail\SMTPPort'] := EMailInfo.SMTPPort;
  mb_AppConfiguration.AsString['eMail\UserName'] := StrToBin(EncryptStringLocaly(EMailInfo.UserName));
  mb_AppConfiguration.AsString['eMail\Password'] := StrToBin(EncryptStringLocaly(EMailInfo.Password));
  mb_AppConfiguration.AsString['eMail\NotificationDomain'] := EMailInfo.NotificationDomainName;
end;

procedure TevGlobalSettings.SaveGeneral;
begin
  if FEnvironmentID <> Mainboard.MachineInfo.IPaddress then
    mb_AppConfiguration.AsString['General\EnvironmentID'] := FEnvironmentID;

  mb_AppConfiguration.AsInteger['General\DBConPoolSize'] := FDBConPoolSize;
  mb_AppConfiguration.AsString['General\DBAdminPassword'] := StrToBin(EncryptStringLocaly(FDBAdminPassword));
  mb_AppConfiguration.AsString['General\DBUserPassword'] := StrToBin(EncryptStringLocaly(FDBUserPassword));
end;

procedure TevGlobalSettings.SaveMiscAdminAccount;
var
  i: Integer;
  D: IevDomainInfo;
  sSect: String;
begin
  // [Misc.AdminAccount.<Domain>]
  mb_AppConfiguration.DeleteNode('Misc\AdminAccount');

  DomainInfoList.Lock;
  try
    for i := 0 to DomainInfoList.Count - 1 do
    begin
      sSect := 'Misc\AdminAccount\';
      D := DomainInfoList[i];
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';

      mb_AppConfiguration.AsString[sSect + 'UserName'] := StrToBin(EncryptStringLocaly(D.AdminAccount));
    end;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SaveMiscFileFolders;
var
  i: Integer;
  D: IevDomainInfo;
  sSect: String;
begin
  // [Misc.FileFolders.<Domain>]
  mb_AppConfiguration.DeleteNode('Misc\FileFolders');

  DomainInfoList.Lock;
  try
    for i := 0 to DomainInfoList.Count - 1 do
    begin
      sSect := 'Misc\FileFolders\';
      D := DomainInfoList[i];
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';

      mb_AppConfiguration.AsString[sSect + 'TaskQueue'] := D.FileFolders.TaskQueueFolder;
      mb_AppConfiguration.AsString[sSect + 'VMR'] := D.FileFolders.VMRFolder;
      mb_AppConfiguration.AsString[sSect + 'ACH'] := D.FileFolders.ACHFolder;
      mb_AppConfiguration.AsString[sSect + 'ASCII'] := D.FileFolders.ASCIIPrefix;
    end;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SaveMiscGeneral;
var
  i: Integer;
  D: IevDomainInfo;
  sSect: String;
begin
  // [Misc.General.<Domain>]
  mb_AppConfiguration.DeleteNode('Misc\General');

  DomainInfoList.Lock;
  try
    for i := 0 to DomainInfoList.Count - 1 do
    begin
      sSect := 'Misc\General\';
      D := DomainInfoList[i];
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';

      mb_AppConfiguration.AsBoolean[sSect + 'DBChangeBroadcast'] := D.DBChangesBroadcast;
      mb_AppConfiguration.AsBoolean[sSect + 'IntUserThrRR'] := D.InternalUsersThroughRR;
    end;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SaveMiscLicense;
var
  i: Integer;
  D: IevDomainInfo;
  sSect: String;
begin
  // [Misc.License.<Domain>]
  mb_AppConfiguration.DeleteNode('Misc\License');

  DomainInfoList.Lock;
  try
    for i := 0 to DomainInfoList.Count - 1 do
    begin
      sSect := 'Misc\License\';
      D := DomainInfoList[i];
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';

      mb_AppConfiguration.AsString[sSect + 'SerialNumber'] := D.LicenseInfo.SerialNumber;
      mb_AppConfiguration.AsString[sSect + 'Code'] := D.LicenseInfo.Code;
      mb_AppConfiguration.AsString[sSect + 'LastChange'] := D.LicenseInfo.LastChange;
    end;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SaveMiscSystemAccount;
var
  i: Integer;
  D: IevDomainInfo;
  sSect: String;
begin
  // [Misc.SystemAccount.<Domain>]
  mb_AppConfiguration.DeleteNode('Misc\SystemAccount');

  DomainInfoList.Lock;
  try
    for i := 0 to DomainInfoList.Count - 1 do
    begin
      sSect := 'Misc\SystemAccount\';
      D := DomainInfoList[i];
      if not AnsiSameText(D.DomainName, sDefaultDomain) then
        sSect := sSect + D.DomainName + '\';

      mb_AppConfiguration.AsString[sSect + 'UserName'] := StrToBin(EncryptStringLocaly(D.SystemAccountInfo.UserName));
    end;
  finally
    DomainInfoList.Unlock;
  end;
end;

procedure TevGlobalSettings.SetDBAdminPassword(const AValue: String);
begin
  Lock;
  try
    FDBAdminPassword := AValue;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.SetDBConPoolSize(const AValue: Integer);
begin
  Lock;
  try
    FDBConPoolSize := AValue;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.SetDBUserPassword(const AValue: String);
begin
  Lock;
  try
    FDBUserPassword := AValue;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.SetEnvironmentID(const AValue: String);
begin
  Lock;
  try
    FEnvironmentID := AValue;
  finally
    Unlock;
  end;
end;

procedure TevGlobalSettings.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteShortString(FEnvironmentID);
  AStream.WriteInteger(FDBConPoolSize);
  AStream.WriteString(FDBAdminPassword);
  AStream.WriteString(FDBUserPassword);    
  AStream.WriteString(FInternalRR.CommaText);
  (FDomainInfoList as IisInterfacedObject).WriteToStream(AStream);
  (FEMailInfo as IisInterfacedObject).WriteToStream(AStream);
end;

procedure TevGlobalSettings.WriteToStream(const AStream: IEvDualStream);
begin
  Lock;
  try
    inherited;
  finally
    Unlock;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetGlobalSettings, IevSettings, 'Global Settings');

end.
