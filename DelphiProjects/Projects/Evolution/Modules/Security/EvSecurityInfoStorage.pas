unit EvSecurityInfoStorage;

interface

uses Classes, Windows, SyncObjs, SysUtils, IsBaseClasses, EvCommonInterfaces,   EvContext, EvConsts, DB, EvTypes,
     EvUtils, EvDataSet, EvStreamUtils, isZippingRoutines, isBasicUtils, SSecurityInterface, SPackageEntry, EvClasses,
     sSecurityBuilderClassDefs, SDataStructure, CustomSecurityAtoms, EvSecElement, EvMainboard, EvSecurityUtils,
     EvInitApp, EvBasicUtils, EvExceptions, isDataSet, Variants, DateUtils, SDDClasses, isLogFile, isErrorUtils, EvClientDataSet;

type
  IevSecInfoStorage = interface
  ['{6E50357D-0EC4-48D0-878C-42F9DF2730D6}']
    function  GetUserAccount(const ADomain, AUser, APassword: String; const AResetAttempts: Boolean): IevUserAccount; overload;
    function  GetUserAccount(const ADomain, AUser: String; AQuestionAnswerList: IisListOfValues): IevUserAccount; overload;
    function  GetAccountRights(const AccountID: String): IevSecAccountRights;
    function  GetAccountGroupRights(const AccountID: String): IevSecAccountRights;
    function  FindAccountRights(const AccountID: String): IevSecAccountRights;
    function  GetStructure(const AccountID: String): IevSecurityStructure;
    procedure SetStructure(const AccountID: String; const Struct: IevSecurityStructure);
    procedure Cleanup;
    procedure DropAccountRights(const AccountID: String);
    procedure DropUserAccount(const ADomain, AUser: String);
    procedure ChangeAccountPassword(const AUserAccount: IevUserAccount; const ANewPassword: String);
    function  GetQuestionAnswerList(const AUserName: String): IisListOfValues;
    function  GetExtQuestionAnswerList(const AUserName: String): IisListOfValues;
    function  GetAvailableQuestions(const AUserName: String): IisStringList;
    function  GetAvailableExtQuestions(const AUserName: String): IisStringList;
    function  GetQuestions(const AUserName: String): IisStringList;
    procedure SetQuestions(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
    procedure SetExtQuestions(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);    
    function  GetPasswordRules: IisListOfValues;
    function  GenerateNewPassword: String;
    procedure ValidatePassword(const APassword: String);
    function  GetExtQuestions(const AUserAccount: IevUserAccount): IisStringList;
    procedure CheckExtAnswers(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
    function  GetSecurityRules: IisListOfValues;
    procedure ResetSBAccount(const AUser: String);
  end;


  TevSecInfoStorage = class(TisCollection, IevSecInfoStorage)
  private
    FCS: TCriticalSection;
    FAccounts: IisStringList;
    FGuestAccRights: IevSecAccountRights;
    FSecurityStructureData: IevDualStream;
    FSecurityFieldsStructure: IisStringList;
    function  GetSecurityDefStructure: IevSecurityStructure;
    function  GetSecurityFieldsStructure: IisStringList;
    procedure LoadForGroup(const ASecElements: IevSecElements; const AType: Char; const AGroupNbr: Integer);
    procedure LoadForUser(const ASecElements: IevSecElements; const AType: Char; const AUserNbr: Integer; const AGroupsOnly: Boolean);
    procedure LoadForEE(const ASecElements: IevSecElements; const AType: Char; const AClNbr, AEENbr: Integer);
    procedure LoadSecElements(const ASecElements: IevSecElements; const AType: Char; const AccountID: String);
    procedure LoadSecFields(const ASecElements: IevSecFields; const AccountID: String);
    procedure LoadSecRecords(const ASecElements: IevSecRecords; const AccountID: String);
    procedure LoadSecClients(const ASecElements: IevSecClients; const AccountID: String);
    procedure SaveForGroup(const ASecElements: IevSecElements; const AType: Char; const AGroupNbr: Integer);
    procedure SaveForUser(const ASecElements: IevSecElements; const AType: Char; const AUserNbr: Integer);
    procedure SaveSecElements(const ASecElements: IevSecElements; const AType: Char);
  protected
    procedure DoOnConstruction; override;
    function  GetUserAccount(const ADomain, AUser, APassword: String; const AResetAttempts: Boolean): IevUserAccount; overload;
    function  GetUserAccount(const ADomain, AUser: String; AQuestionAnswerList: IisListOfValues): IevUserAccount; overload;
    function  GetAccountRights(const AccountID: String): IevSecAccountRights;
    function  GetAccountGroupRights(const AccountID: String): IevSecAccountRights;
    function  FindAccountRights(const AccountID: String): IevSecAccountRights;
    function  GetStructure(const AccountID: String): IevSecurityStructure;
    procedure SetStructure(const AccountID: String; const Struct: IevSecurityStructure);
    procedure Cleanup;
    procedure DropAccountRights(const AccountID: String);
    procedure DropUserAccount(const ADomain, AUser: String);
    procedure ChangeAccountPassword(const AUserAccount: IevUserAccount; const ANewPassword: String);
    function  GetQuestionAnswerList(const AUserName: String): IisListOfValues;
    function  GetExtQuestionAnswerList(const AUserName: String): IisListOfValues;
    function  GetAvailableQuestions(const AUserName: String): IisStringList;
    function  GetAvailableExtQuestions(const AUserName: String): IisStringList;
    function  GetQuestions(const AUserName: String): IisStringList;
    procedure SetQuestions(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
    procedure SetExtQuestions(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
    function  GetPasswordRules: IisListOfValues;
    function  GenerateNewPassword: String;
    procedure ValidatePassword(const APassword: String);
    function  GetExtQuestions(const AUserAccount: IevUserAccount): IisStringList;
    procedure CheckExtAnswers(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
    function  GetSecurityRules: IisListOfValues;
    procedure ResetSBAccount(const AUser: String);
  public
    class function GetTypeID: String; override;
    destructor Destroy; override;
  end;

var SecInfoStorage: IevSecInfoStorage;

implementation

uses sSecurityClassDefs, EvDataAccessComponents;

const
  MaxWrongPasswordAttempts = 3;
  MaxWrongAnswersAttempts = 6;
  QAListTag = '<QA>';

var
  FSecurityQuestions: IisStringList;
  FExtSecurityQuestions: IisStringList;

type
  TevRemoteUserAccount = class(TevUserAccount)
  private
    FEEClientNbr: Integer;
    FPwdRestoreEnabled: Boolean;
    FExtQARequired: Boolean;
    function  PasswordIsQAList: Boolean;
    procedure DoEEChangePassword(const ANewPassword: String);
    procedure DoSBChangePassword(const ANewPassword: String);
    procedure DoEEChangeQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure DoSBChangeQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure DoEEChangeExtQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure DoSBChangeExtQuestions(const AQuestionAnswerList: IisListOfValues);
    function  CheckPassword(const APassword: String): Boolean;
    function  DoEEGetExtQuestions: IisListOfValues;
    function  DoSBGetExtQuestions: IisListOfValues;
    procedure DoEECheckExtAnswers(const AQuestionAnswerList: IisListOfValues);
    procedure DoSBCheckExtAnswers(const AQuestionAnswerList: IisListOfValues);
    function  ExtAnswersCorrect(const AQuestions, AAnswers: IisListOfValues): Boolean;
    function  GetDSForEE(const ATableName: String): TevClientDataset;
    procedure RequestCacheRefresh;
    procedure SendAccountChangeEmail(const ALoginAttempts: Integer);
  protected
    procedure ReplicatorInitialize;
    procedure DBAInitialize;
    procedure ChangePassword(const ANewPassword: String);
    procedure ChangeQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure ChangeExtQuestions(const AQuestionAnswerList: IisListOfValues);
    function  GetExtQuestions: IisStringList;
    procedure CheckExtAnswers(const AQuestionAnswerList: IisListOfValues);
    procedure SBInitialize(const AResetAttempts: Boolean);
    procedure EEInitialize(const AResetAttempts: Boolean);
    procedure Initialize(const AResetAttempts: Boolean);
  end;


  TevRemoteSecAccountRights = class(TevSecAccountRights)
  private
    FInitialized: Boolean;
    FEmbeddedAccount: Boolean;
    procedure InitAdmin;
    procedure InitReplicator;
    procedure InitDBA;
    procedure InitSystemUser;
    procedure InitUser;
    procedure SaveChangesToDB; virtual;
    procedure Initialize; virtual;
  end;


  // Embedded accounts

  TevGuestSecAccountRights = class(TevRemoteSecAccountRights)
  private
    procedure SaveChangesToDB; override;
    procedure Initialize; override;
  end;



  // LEGACY!
  TevRemoteSecurityStructure = class(TevSecurityStructure)
  private
    // Legacy stuff. Some day we need to redo this
    procedure ProcessComponentRecurr(const Builder: ISchemeBuilder; const c: TComponent);
    procedure CreateHiddenFieldsAtoms(const Holder: TComponent);
    procedure ProvideContexts(const Sender: ISecurityAtom; var ContextRecords: TAtomContextRecordArray);
    procedure SecElementCallback(const Sender: TComponent);
    //
  protected
    function Rebuild: IevDualStream; override;
  end;


{ TevRemoteSecAccountRights }

procedure TevRemoteSecAccountRights.SaveChangesToDB;
begin
  if not FEmbeddedAccount then
  begin
    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).SaveSecElements(Menus, etMenu);
      TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).SaveSecElements(Screens, etScreen);
      TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).SaveSecElements(Functions, etFunc);

      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;
end;

procedure TevRemoteSecAccountRights.Initialize;
var
  sUserName: String;

  procedure InitLevel(out AUserName: String);
  var
    Q: IevQuery;
    s: String;
  begin
    // Is account group or user?
    // Domain.Nbr  for user
    // Domain.GNbr  for group
    AUserName := '';

    s := AccountID;
    GetNextStrValue(s, '.');
    if StartsWith(s, 'G') or FinishesWith(AccountID, '.G', True) then
      FLevel := slNone
    else if StartsWith(s, 'E') then
      FLevel := slEmployee
    else
    begin
      Q := TevQuery.Create('GenericSelectCurrentWithCondition');
      Q.Macros.AddValue('TABLENAME', 'SB_USER');
      Q.Macros.AddValue('COLUMNS', 'SECURITY_LEVEL, USER_ID');
      Q.Macros.AddValue('CONDITION', 'SB_USER_NBR = ' + s);
      Q.Execute;
      AUserName := Q.Result.Fields[1].AsString;

      s := ConvertNull(Q.Result.Fields[0].Value, ' ');
      case s[1] of
        USER_SEC_LEVEL_ADMINISTRATOR: FLevel := slAdministrator;
        USER_SEC_LEVEL_MANAGER:       FLevel := slManager;
        USER_SEC_LEVEL_SUPERVISOR:    FLevel := slSupervisor;
        USER_SEC_LEVEL_USER:          FLevel := slSBUser;
        USER_SEC_LEVEL_EMPLOYEE:      FLevel := slEmployee;
      else
        FLevel := slNone;
      end;
    end;
  end;

begin
  Lock;
  try
    if not FInitialized then
    begin
      FInitialized := True;
      try
        // below is trusted code, so we need to deactivate security so it can get acces to user info
        ctx_DBAccess.DisableSecurity;
        try
          InitLevel(sUserName);

          if AnsiSameText(sUserName, ctx_DomainInfo.AdminAccount) then
          begin
            FEmbeddedAccount := True;
            FLevel := slAdministrator; 
            InitAdmin;
          end
          else if IsADRUser(sUserName) then
          begin
            FEmbeddedAccount := True;
            InitReplicator;
          end
          else if IsDBAUser(sUserName) then
          begin
            FEmbeddedAccount := True;
            InitDBA;
          end
          else if (sUserName <> '') and AnsiSameText(sUserName, ctx_DomainInfo.SystemAccountInfo.UserName) then
          begin
            FEmbeddedAccount := True;
            InitSystemUser;
          end
          else
          begin
            FEmbeddedAccount := False;
            InitUser;
          end;

        finally
          ctx_DBAccess.EnableSecurity;
        end;
      except
        FInitialized := False;
        raise;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TevRemoteSecAccountRights.InitAdmin;
var
  SecStruct: IevSecurityStructure;
  Elements: IevSecElements;
  Atom: TSecurityAtom;
  i, j: Integer;
  Q: IevQuery;
  ElemType: Char;
  SecAccessLevel: Char;
  bFullAccess: Boolean;
begin
  SecStruct := ((SecInfoStorage as IisInterfacedObject).GetImplementation as TevSecInfoStorage).GetSecurityDefStructure;
  bFullAccess := IsQAMode and AnsiSameText(ctx_DomainInfo.DomainName, sDefaultDomain);

  // Security items
  for i := SecStruct.Obj.AtomCount - 1 downto 0 do
  begin
    Atom := SecStruct.Obj.Atoms[i];
    ElemType := Atom.AtomType[1];

    // Disable items controlled by iSystems
    Elements := GetElementsByType(ElemType);
    if Assigned(Elements) then
    begin
      if bFullAccess then
        SecAccessLevel := stEnabled
      else
      begin
        if (ElemType = etFunc) and
           ((Atom.Tag = 'ABILITY_EDIT_DD') or (Atom.Tag = 'ABILITY_EDIT_RWLIB')) then
          SecAccessLevel := stDisabled
        else if (ElemType = etScreen) and StartsWith(Atom.Tag, 'TEDIT_SY') then
        begin
          SecAccessLevel := stDisabled;
          for j := 0 to Atom.ContextCount - 1 do
            if Atom.Contexts[j].Tag = stEnabled then
            begin
              if j < Atom.ContextCount - 1 then
                SecAccessLevel := Atom.Contexts[j + 1].Tag[1]
              else
                SecAccessLevel := stEnabled;
              break;
            end;
        end
        else
         SecAccessLevel := stEnabled;
      end;

      if SecAccessLevel <> stDisabled then
        Elements.SetState(Atom.Tag, SecAccessLevel, False);
    end;
  end;

  // Clients
  Q := TevQuery.Create('SELECT cl_nbr FROM TMP_CL');
  Q.Execute;
  Elements := Clients;
  while not Q.Result.Eof do
  begin
    Elements.SetState(Q.Result.Fields[0].AsString, stEnabled, False);
    Q.Result.Next;
  end;
end;

procedure TevRemoteSecAccountRights.InitUser;
begin
  TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).LoadSecElements(Menus, etMenu, AccountID);
  TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).LoadSecElements(Screens, etScreen, AccountID);
  TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).LoadSecElements(Functions, etFunc, AccountID);
  TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).LoadSecClients(Clients, AccountID);
  TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).LoadSecFields(Fields, AccountID);
  TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).LoadSecRecords(Records, AccountID);
end;

procedure TevRemoteSecAccountRights.InitSystemUser;
var
  SecStruct: IevSecurityStructure;
  Elements: IevSecElements;
  Atom: TSecurityAtom;
  i: Integer;
  Q: IevQuery;
  ElemType: Char;
begin
  SecStruct := ((SecInfoStorage as IisInterfacedObject).GetImplementation as TevSecInfoStorage).GetSecurityDefStructure;

  // Security items
  Elements := Functions;
  for i := SecStruct.Obj.AtomCount - 1 downto 0 do
  begin
    Atom := SecStruct.Obj.Atoms[i];
    ElemType := Atom.AtomType[1];
    if ElemType = etFunc then
      Elements.SetState(Atom.Tag, stEnabled, False);
  end;

  // Clients
  Q := TevQuery.Create('SELECT cl_nbr FROM TMP_CL');
  Q.Execute;
  Elements := Clients;
  while not Q.Result.Eof do
  begin
    Elements.SetState(Q.Result.Fields[0].AsString, stEnabled, False);
    Q.Result.Next;
  end;
end;

procedure TevRemoteSecAccountRights.InitReplicator;
begin
  // so far nothing to do
end;

procedure TevRemoteSecAccountRights.InitDBA;
begin
  // so far nothing to do
end;

{ TevSecInfoStorage }

procedure TevSecInfoStorage.DoOnConstruction;
begin
  inherited;
  InitLock;
  FCS := TCriticalSection.Create;

  FAccounts := TisStringList.CreateAsIndex(True);

  FGuestAccRights := TevGuestSecAccountRights.Create(sGuestUserName);
  AddChild(FGuestAccRights as IisInterfacedObject);
end;

function TevSecInfoStorage.GetAccountRights(const AccountID: String): IevSecAccountRights;
var
  sUser: String;
begin
  sUser := AccountID;
  GetNextStrValue(sUser, '.');

  if AccountID = '' then
    Result := nil

  else if IsGuestUser(AccountID) or IsADRUser(sUser) or IsDBAUser(sUser) then
    Result := FGuestAccRights

  else
  begin
    if StartsWith(sUser, 'E') then
      Result := TevRemoteSecAccountRights.Create(AccountID)
    else
    begin
      FCS.Enter;
      try
        Result := FindAccountRights(AccountID);
        if not Assigned(Result) then
        begin
          Result := TevRemoteSecAccountRights.Create(AccountID);
          AddChild(Result as IisInterfacedObject);
        end;
      finally
        FCS.Leave;
      end;
    end;

    TevRemoteSecAccountRights((Result as IisInterfacedObject).GetImplementation).Initialize;
  end;
end;

function TevSecInfoStorage.GetStructure(const AccountID: String): IevSecurityStructure;
var
  Rights: IevSecAccountRights;
  Atom: TSecurityAtom;
  StateInfo: IevSecStateInfo;
  State: TevSecElementState;
  i, j: Integer;
  C: TSecurityContext;
  CCurrent: TSecurityContext;
  s: String;
  bGroupRights: Boolean;
begin
  // Get security schema
  Result := GetSecurityDefStructure;

  // Take off unavailable atoms (based on context)
  for i := Result.Obj.AtomCount - 1 downto 0 do
  begin
    Atom := Result.Obj.Atoms[i];
    State := ctx_AccountRights.GetElementState(Atom.AtomType[1], Atom.Tag);
    C := Atom.GetContextByTag(State);
    for j := Atom.ContextCount - 1 downto 0 do
      if Atom.Contexts[j].Priority > C.Priority then
      Begin
        CCurrent := Atom.CurrentContext;
        Atom.DeleteContext(j);
        Atom.CurrentContext := Atom.GetContextByTag(CCurrent.Tag);
      end;
  end;


  // Apply Account rights
  if AccountID <> '' then
  begin
    s := AccountID;
    GetNextStrValue(s, '.');
    bGroupRights := StartsWith(s, 'G');

    Rights := GetAccountRights(AccountID);

    for i := Result.Obj.AtomCount - 1 downto 0 do
    begin
      Atom := Result.Obj.Atoms[i];
      StateInfo := TevRemoteSecAccountRights((Rights as IisInterfacedObject).GetImplementation).GetElementStateInfo(Atom.AtomType[1], Atom.Tag);
      if not Assigned(StateInfo) then
        State := stDisabled
      else
        State := StateInfo.State;

      C := Atom.GetContextByTag(State);
      if Assigned(C) then
      begin
        Atom.CurrentContext := Atom.GetContextByTag(State);
        if not bGroupRights and (not Assigned(StateInfo) or StateInfo.FromGroup) then
          Atom.CurrentContext.MarkFromGroup(True);
      end
      else
      begin
        Result.Obj.DeleteAtom(Atom);
        Atom.Free;
      end;
    end;
  end;
end;

class function TevSecInfoStorage.GetTypeID: String;
begin
  Result := 'SecInfoStorage';
end;

function TevSecInfoStorage.GetSecurityDefStructure: IevSecurityStructure;
var
  Q: IevQuery;
  Scompr, Sdecompr: IEvDualStream;
begin
  FCS.Enter;
  try
    if not Assigned(FSecurityStructureData) then
    begin
      Scompr := TEvDualStreamHolder.CreateInMemory;
      Sdecompr := TEvDualStreamHolder.CreateInMemory;

      Q := TevQuery.Create('SecurityStructure');
      (Q.Result.FieldByName('Data') as TBlobField).SaveToStream(Scompr.RealStream);
      Q := nil;

      Scompr.Position := 0;
      InflateStream(Scompr.RealStream, Sdecompr.RealStream);
      Scompr := nil;
      Sdecompr.Position := 0;

      FSecurityStructureData := Sdecompr;
    end;

    Result := TevRemoteSecurityStructure.Create(FSecurityStructureData);
  finally
    FCS.Leave;
  end;
end;

function TevSecInfoStorage.FindAccountRights(const AccountID: String): IevSecAccountRights;
begin
  Result := FindChildByName(AccountID) as IevSecAccountRights;
end;

procedure TevSecInfoStorage.SetStructure(const AccountID: String; const Struct: IevSecurityStructure);
var
  Rights: IevSecAccountRights;
  Atom: TSecurityAtom;
  i: Integer;
begin
  if AccountID <> '' then
  begin
    // Set values
    Rights := GetAccountRights(AccountID);
    (Rights as IisCollection).Lock;
    try
      for i := 0 to Struct.Obj.AtomCount - 1 do
      begin
        Atom := Struct.Obj.Atoms[i];
        Rights.SetElementState(Atom.AtomType[1], Atom.Tag, Atom.CurrentContext.Tag[1], Atom.CurrentContext.IsFromGroup);
      end;

      TevRemoteSecAccountRights((Rights as IisInterfacedObject).GetImplementation).SaveChangesToDB;
    finally
     (Rights as IisCollection).UnLock;    
    end;
  end;
end;

function TevSecInfoStorage.GetSecurityFieldsStructure: IisStringList;
var
  Data: IevDualStream;
  Q: IevQuery;
  i, j, iMainCount, iDetailCount: Integer;
  Details: IisStringList;
  FldDesct: String;
  s: String;
begin
  FCS.Enter;
  try
    if not Assigned(FSecurityFieldsStructure) then
    begin
      Data := TEvDualStreamHolder.Create;
      Q := TevQuery.Create('SecurityFieldsStructure');
      (Q.Result.FieldByName('Data') as TBlobField).SaveToStream(Data.RealStream);
      Q := nil;

      FSecurityFieldsStructure := TisStringList.CreateAsIndex;
      Data.Position := 0;
      iMainCount := Data.ReadInteger;
      for i := 0 to iMainCount - 1 do
      begin
        s := Data.ReadString;
        Details := TisStringList.CreateAsIndex;
        FSecurityFieldsStructure.AddObject(s, Details);
        s := Data.ReadString; // skip name

        iDetailCount := Data.ReadInteger;
        for j := 0 to iDetailCount - 1 do
        begin
          s := Data.ReadString + '.' + Data.ReadString;
          FldDesct := Data.ReadString + ',' + IntToStr(Data.ReadInteger);
          Details.AddObject(s, TisVariant.CreateAs(FldDesct));
        end;
      end;
    end;

    Result := FSecurityFieldsStructure;
  finally
    FCS.Leave;
  end;
end;

destructor TevSecInfoStorage.Destroy;
begin
  FreeAndNil(FCS);
  inherited;
end;

procedure TevSecInfoStorage.LoadForGroup(const ASecElements: IevSecElements; const AType: Char; const AGroupNbr: Integer);
var
  Q: IevQuery;
  i: Integer;
  FieldTag: TField;
  FieldContext: TField;
  State: TevSecElementState;
  DefaultSec: IevSecurityStructure;
  A: TSecurityAtom;
  NewState, CurrState: TSecurityContext;
begin
  Q := TevQuery.Create('SecurityGroupRights');
  Q.Params.AddValue('SB_SEC_GROUPS_NBR', AGroupNbr);
  Q.Params.AddValue('SEC_ELEMENT_TYPE', AType);
  FieldTag := Q.Result.FieldByName('tag');
  FieldContext := Q.Result.FieldByName('context');

  DefaultSec := GetSecurityDefStructure;

  while not Q.Result.Eof do
  begin
    if (FieldTag.AsString <> '') and (FieldContext.AsString <> '') then
    begin
      A := DefaultSec.Obj.FindAtom(FieldTag.AsString, AType);
      if Assigned(A) then
      begin
        i := ASecElements.IndexOf(FieldTag.AsString);
        State := FieldContext.AsString[1];
        NewState := A.GetContextByTag(State);
        if Assigned(NewState) then
          if i = -1 then
          begin
            i := ASecElements.AddElement(FieldTag.AsString);
            ASecElements.SetStateByIndex(i, State, False);
          end
          else
          begin
            CurrState := A.GetContextByTag(ASecElements.GetStateInfo(i).State);
            if CurrState.Priority < NewState.Priority then
              ASecElements.SetStateByIndex(i, State, False);
          end;
      end;
    end;

    Q.Result.Next;
  end;
end;

procedure TevSecInfoStorage.LoadForUser(const ASecElements: IevSecElements;
  const AType: Char; const AUserNbr: Integer; const AGroupsOnly: Boolean);
var
  Q: IevQuery;
  i: Integer;
  FieldTag: TField;
  FieldContext: TField;
  FieldLevel: TField;
  State: TevSecElementState;
  DefaultSec: IevSecurityStructure;
  A: TSecurityAtom;
  NewState, CurrState: TSecurityContext;
  sTag: String;
begin
  Q := TevQuery.Create('SecurityUserRights');
  Q.Params.AddValue('SB_USER_NBR', AUserNbr);
  Q.Params.AddValue('SEC_ELEMENT_TYPE', AType);

  FieldTag := Q.Result.FieldByName('tag');
  FieldContext := Q.Result.FieldByName('context');
  FieldLevel := Q.Result.FieldByName('src');

  DefaultSec := GetSecurityDefStructure;

  while not Q.Result.Eof do
  begin
    sTag := StringReplace(FieldTag.AsString, '&', '', [rfReplaceAll]);
    if (sTag <> '') and (FieldContext.AsString <> '') then
    begin
      A := DefaultSec.Obj.FindAtom(sTag, AType);
      if Assigned(A) then
      begin
        i := ASecElements.IndexOf(sTag);
        State := FieldContext.AsString[1];
        NewState := A.GetContextByTag(State);
        if Assigned(NewState) then
          if i = -1 then
          begin
            if not AGroupsOnly or (FieldLevel.AsString = 'G') then
            begin
              i := ASecElements.AddElement(sTag);
              ASecElements.SetStateByIndex(i, State, FieldLevel.AsString = 'G');
            end;  
          end
          else
          begin
            CurrState := A.GetContextByTag(ASecElements.GetStateInfo(i).State);
            // Result is sorted, group data always goes first
            if (FieldLevel.AsString = 'G') and (CurrState.Priority < NewState.Priority) then
              ASecElements.SetStateByIndex(i, State, True)
            else
              if not AGroupsOnly then
                ASecElements.SetStateByIndex(i, State, False)
          end;
      end;
    end;

    Q.Result.Next;
  end;
end;

procedure TevSecInfoStorage.LoadSecElements(
  const ASecElements: IevSecElements; const AType: Char;
  const AccountID: String);
var
  s, n: String;
begin
  ASecElements.BeginChange;
  try
    ASecElements.Clear;

    // Is account group or user?
    // Domain.Nbr  for user
    // Domain.Nbr.G  for user groups only
    // Domain.GNbr  for group
    // Domain.ENbr  for Employee
    s := AccountID;
    CheckCondition(AnsiSameText(GetNextStrValue(s, '.'), Context.UserAccount.Domain), 'Access denied', ESecurity);
    if StartsWith(s, 'G') then
    begin
      GetNextStrValue(s, 'G');
      LoadForGroup(ASecElements, AType, StrToInt(s));
    end

    else if StartsWith(s, 'E') then
    begin
      GetNextStrValue(s, 'E');
      n := GetNextStrValue(s, '.');
      LoadForEE(ASecElements, AType, StrToInt(n), StrToInt(s));
    end

    else
    begin
      n := GetNextStrValue(s, '.');
      LoadForUser(ASecElements, AType, StrToInt(n), s = 'G');
    end;

  finally
    ASecElements.EndChange;
    ASecElements.ClearModified;
  end;
end;

procedure TevSecInfoStorage.SaveForGroup(const ASecElements: IevSecElements;
  const AType: Char; const AGroupNbr: Integer);
var
  i: Integer;
  DS: TevClientDataSet;
  sTag, sNewVal: String;
  s: String;
begin
  DS := TevClientDataSet.Create(nil);
  try
    DS.ProviderName := 'SB_CUSTOM_PROV';
    DS.CustomProviderTableName := 'SB_SEC_RIGHTS';
    DS.ClientID := -1;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', '*');
      SetMacro('TableName', 'SB_SEC_RIGHTS');
      SetMacro('Condition', 'SB_SEC_GROUPS_NBR = ' + IntToStr(AGroupNbr) + ' AND TAG STARTS ''' + AType + '''');
      DS.DataRequest(AsVariant);
    end;
    DS.Open;

    // Disabled items don't make sence for group level. Delete it!
    DS.First;
    while not DS.Eof do
    begin
      s := DS.FieldByName('TAG').AsString;
      GetNextStrValue(s, ';');
      if ASecElements.GetState(s) = stDisabled then
        DS.Delete
      else
        DS.Next;
    end;

    DS.IndexFieldNames := 'TAG';

    // apply changes
    for i := 0 to ASecElements.Count - 1 do
    begin
      sTag := AType + ';' + ASecElements.GetElementByIndex(i);
      sNewVal := ASecElements.GetStateInfo(i).State;

      if sNewVal = stDisabled then
        Continue;

      if DS.FindKey([sTag]) then
      begin
        if DS.FieldByName('CONTEXT').AsString <> sNewVal then
        begin
          DS.Edit;
          DS.FieldByName('CONTEXT').AsString := sNewVal;
          DS.Post;
        end;
      end
      else
      begin
        DS.Append;
        DS.FieldByName('SB_SEC_RIGHTS_NBR').AsInteger := ctx_DBAccess.GetNewKeyValue('SB_SEC_RIGHTS');
        DS.FieldByName('SB_SEC_GROUPS_NBR').AsInteger := AGroupNbr;
        DS.FieldByName('TAG').AsString := sTag;
        DS.FieldByName('CONTEXT').AsString := sNewVal;
        DS.Post;
      end;
    end;

    ctx_DataAccess.PostDataSets([DS]);
  finally
    DS.Free;
  end;

  ASecElements.ClearModified;
end;

procedure TevSecInfoStorage.SaveForUser(const ASecElements: IevSecElements;
  const AType: Char; const AUserNbr: Integer);
var
  i: Integer;
  GroupData: IevSecElements;
  DS: TevClientDataSet;
  sTag, s: String;
  NewStateInfo: IevSecStateInfo;
begin
  GroupData := TevSecElements.Create;
  LoadSecElements(GroupData, AType, ASecElements.AccountID + '.G');  // user group data

  DS := TevClientDataSet.Create(nil);
  try
    DS.ProviderName := 'SB_CUSTOM_PROV';
    DS.CustomProviderTableName := 'SB_SEC_RIGHTS';
    DS.ClientID := -1;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', '*');
      SetMacro('TableName', 'SB_SEC_RIGHTS');
      SetMacro('Condition', 'SB_USER_NBR = ' + IntToStr(AUserNbr) + ' AND TAG STARTS ''' + AType + '''');
      DS.DataRequest(AsVariant);
    end;
    DS.Open;

    // delete user rights exceptions
    DS.First;
    while not DS.Eof do
    begin
      s := DS.FieldByName('TAG').AsString;
      GetNextStrValue(s, ';');
      i := ASecElements.IndexOf(s);
      if (i = -1) or ASecElements.GetStateInfo(i).FromGroup then
        DS.Delete
      else
        DS.Next;
    end;

    // apply user rights exceptions
    DS.IndexFieldNames := 'TAG';
    for i := 0 to ASecElements.Count - 1 do
    begin
      NewStateInfo := ASecElements.GetStateInfo(i);
      sTag := AType + ';' + ASecElements.GetElementByIndex(i);

      if NewStateInfo.FromGroup then
        Continue;

      if DS.FindKey([sTag]) then
      begin
        if NewStateInfo.State <> DS.FieldByName('CONTEXT').AsString then
        begin
          DS.Edit;
          DS.FieldByName('CONTEXT').AsString := NewStateInfo.State;
          DS.Post;
         end;
      end

      else
      begin
        DS.Append;
        DS.FieldByName('SB_SEC_RIGHTS_NBR').AsInteger := ctx_DBAccess.GetNewKeyValue('SB_SEC_RIGHTS');
        DS.FieldByName('SB_USER_NBR').AsInteger := AUserNbr;
        DS.FieldByName('TAG').AsString := sTag;
        DS.FieldByName('CONTEXT').AsString := NewStateInfo.State;
        DS.Post;
      end;
    end;

    ctx_DataAccess.PostDataSets([DS]);
  finally
    DS.Free;
  end;

  ASecElements.ClearModified;
end;

procedure TevSecInfoStorage.SaveSecElements(const ASecElements: IevSecElements; const AType: Char);
var
  s: String;
begin
  if ASecElements.Modified then
  begin
    // Is account group or user?
    // Domain.Nbr  for user
    // Domain.GNbr  for group
    s := ASecElements.AccountID;
    GetNextStrValue(s, '.');
    if StartsWith(s, 'G') then
    begin
      GetNextStrValue(s, 'G');
      SaveForGroup(ASecElements, AType, StrToInt(s));
    end
    else
      SaveForUser(ASecElements, AType, StrToInt(s));

    ASecElements.ClearModified;
  end;
end;


procedure TevSecInfoStorage.LoadSecFields(const ASecElements: IevSecFields; const AccountID: String);
var
  FiledsInfo, Fields: IisStringList;
  Rights: IevSecAccountRights;
  i, j, k: Integer;
  State: TevSecElementState;
  s: String;
begin
  ASecElements.BeginChange;
  try
    ASecElements.Clear;
    // Is account group or user?
    // Domain.Nbr  for user
    // Domain.Nbr.G  for user groups only
    // Domain.GNbr  for group
    // Domain.ENbr  for Employee    
    s := AccountID;
    CheckCondition(AnsiSameText(GetNextStrValue(s, '.'), Context.UserAccount.Domain), 'Access denied', ESecurity);

    if StartsWith(s, 'E') then
      Exit;

    FiledsInfo := TevSecInfoStorage((SecInfoStorage as IisInterfacedObject).GetImplementation).GetSecurityFieldsStructure;
    Rights := SecInfoStorage.GetAccountRights(AccountID);

    for i  := 0 to FiledsInfo.Count - 1 do
    begin
      State := Rights.Functions.GetState(FiledsInfo[i]);

      if State <> stEnabled then
      begin
        Fields := FiledsInfo.Objects[i] as IisStringList;
        for j := 0 to Fields.Count - 1 do
        begin
          k := ASecElements.IndexOf(Fields[j]);
          if k = -1 then
          begin
            k := ASecElements.AddElement(Fields[j]);
            s := (Fields.Objects[j] as IisVariant).Value;
            ASecElements.GetSecFieldInfo(k).FieldType := GetNextStrValue(s, ',');
            ASecElements.GetSecFieldInfo(k).FieldSize := StrToInt(s);
          end;
          if ASecElements.GetStateInfo(k).State < State then
            ASecElements.SetStateByIndex(k, State, False);
        end
      end;
    end;

  finally
    ASecElements.EndChange;
    ASecElements.ClearModified;
  end;
end;

procedure TevSecInfoStorage.LoadSecClients(const ASecElements: IevSecClients; const AccountID: String);
var
  Q: IevQuery;
  s: String;
  Nbr, i: Integer;
  bGroup: Boolean;
begin
  ASecElements.BeginChange;
  try
    ASecElements.Clear;
    // Is account group or user?
    // Domain.Nbr  for user
    // Domain.GNbr  for group
    // Domain.ENbr  for Employee    
    s := AccountID;
    GetNextStrValue(s, '.');
    if StartsWith(s, 'G') then
    begin
      bGroup := True;
      GetNextStrValue(s, 'G');
      Nbr := StrToInt(s);
    end
    else if StartsWith(s, 'E') then
    begin
      GetNextStrValue(s, 'E');
      s := GetNextStrValue(s, '.');
      ASecElements.SetState(s, stEnabled, False);
      Exit;
    end
    else
    begin
      bGroup := False;
      s := GetNextStrValue(s, '.');  // to get rid of ".G"
      Nbr := StrToInt(s);
    end;

    if bGroup then
    begin
      Q := TevQuery.Create('SecurityGroupClients');
      Q.Params.AddValue('SB_SEC_GROUPS_NBR', Nbr);
    end
    else
    begin
      Q := TevQuery.Create('SecurityUserClients');
      Q.Params.AddValue('SB_USER_NBR', Nbr);
    end;

    while not Q.Result.Eof do
    begin
      i := ASecElements.AddElement(Q.Result.Fields[0].AsString);
      ASecElements.SetStateByIndex(i, stEnabled, False);
      Q.Result.Next;
    end;

  finally
    ASecElements.EndChange;
    ASecElements.ClearModified;
  end;
end;


procedure TevSecInfoStorage.LoadSecRecords(const ASecElements: IevSecRecords; const AccountID: String);
const
  aFltType: array [0..4] of string = ('CO', 'DV', 'BR', 'DP', 'TM');
var
  Q: IevQuery;
  s: String;
  AllClientCount, NoAccessClientCount, Nbr, i: Integer;
  bGroup: Boolean;
  Rights: IevSecAccountRights;
  sClients: String;
  ClientList: IisStringList;
begin
  ASecElements.BeginChange;
  try
    ASecElements.Clear;
    // Is account group or user?
    // Domain.Nbr  for user
    // Domain.GNbr  for group
    // Domain.ENbr  for Employee    
    s := AccountID;
    GetNextStrValue(s, '.');
    if StartsWith(s, 'G') then
    begin
      bGroup := True;
      GetNextStrValue(s, 'G');
      Nbr := StrToInt(s);
    end

    else if StartsWith(s, 'E') then
      Exit

    else
    begin
      bGroup := False;
      s := GetNextStrValue(s, '.');  // to get rid of ".G"
      Nbr := StrToInt(s);
    end;

    if bGroup then
    begin
      Q := TevQuery.Create('SecurityGroupRecords');
      Q.Params.AddValue('SB_SEC_GROUPS_NBR', Nbr);
    end
    else
    begin
      Q := TevQuery.Create('SecurityUserRecords');
      Q.Params.AddValue('SB_USER_NBR', Nbr);
    end;

    while not Q.Result.Eof do
    begin
      if Q.Result.FieldByName('Custom_Expr').AsString <> '' then
      begin
        s := 'C:' + Q.Result.FieldByName('CL_NBR').AsString + ':' +
             aFltType[Q.Result.FieldByName('Filter_Type').AsInteger];

        i := ASecElements.IndexOf(s);
        if i = -1 then
        begin
          i := ASecElements.AddElement(s);
          ASecElements.SetStateByIndex(i, stDisabled, False);
          ASecElements.GetSecRecordInfo(i).DBType := dbtClient;
          ASecElements.GetSecRecordInfo(i).ClientID := Q.Result.FieldByName('CL_NBR').AsInteger;
          ASecElements.GetSecRecordInfo(i).FilterType := aFltType[Q.Result.FieldByName('Filter_Type').AsInteger];
          ASecElements.GetSecRecordInfo(i).Filter := Q.Result.FieldByName('Custom_Expr').AsString;
        end
        else
          ASecElements.GetSecRecordInfo(i).Filter := ASecElements.GetSecRecordInfo(i).Filter + ',' + Q.Result.FieldByName('Custom_Expr').AsString;
      end;

      Q.Result.Next;
    end;


    // Add temp DB filter
    sClients := '';
    if not bGroup then
    begin
      // remote user
      Q := TevQuery.Create('SELECT USER_FUNCTIONS FROM sb_user WHERE {AsOfNow<sb_user>} AND SB_USER_NBR = ' + IntToStr(Nbr));
      s := ExtractFromFiller(Q.Result.FieldByName('USER_FUNCTIONS').AsString, 1, 8);

      if s <> '-1' then
      begin
        i := ASecElements.AddElement('B:0:UF:' + s);
        ASecElements.SetStateByIndex(i, stDisabled, False);
        ASecElements.GetSecRecordInfo(i).DBType := dbtBureau;
        ASecElements.GetSecRecordInfo(i).ClientID := 0;
        ASecElements.GetSecRecordInfo(i).FilterType := '-';
        ASecElements.GetSecRecordInfo(i).Filter := s;
      end;
    end;

    if sClients = '' then
    begin
      // sb user
      // All this crazy logic below is related to 64K sql size limitation
      Rights := SecInfoStorage.GetAccountRights(AccountID);
      if Rights.Clients.Count > 0 then
      begin
        Q := TevQuery.Create('SELECT Count(*) FROM TMP_CL');
        CheckCondition(Q.Result.Fields[0].AsInteger > 0, 'TMP_CL is empty. Please see administrator.');

        AllClientCount := Q.Result.Fields[0].AsInteger;
        NoAccessClientCount := AllClientCount - Rights.Clients.Count; // This is just an estimation because securyty can report access to not existing clients

        if NoAccessClientCount >= Rights.Clients.Count then
        begin
          // access to clients using IN logic
          sClients := '';
          for i := 0 to Rights.Clients.Count - 1 do
            AddStrValue(sClients, Rights.Clients.GetElementByIndex(i), ',');
          sClients := 'IN (' + sClients + ')';
        end

        else if Rights.Clients.Count < 5000 then
        begin
          // access to clients using NOT IN logic (easy way)
          ClientList := TisStringList.Create;
          ClientList.Capacity := Rights.Clients.Count;
          for i := 0 to Rights.Clients.Count - 1 do
            ClientList.Add(Rights.Clients.GetElementByIndex(i));
          sClients := BuildINsqlStatement('CL_NBR', ClientList);

          Q := TevQuery.Create('GenericSelectWithCondition');
          Q.Macros.AddValue('TABLENAME', 'TMP_CL');
          Q.Macros.AddValue('COLUMNS', 'CL_NBR');
          Q.Macros.AddValue('CONDITION', 'NOT ' + sClients);

          if Q.Result.RecordCount > 0 then
          begin
           ClientList := TisStringList.Create;
           ClientList.Capacity := Q.Result.RecordCount;
           while not Q.Result.Eof do
           begin
             ClientList.Add(Q.Result.Fields[0].AsString);
             Q.Result.Next;
           end;
           sClients := 'NOT IN (' + ClientList.CommaText + ')';
          end
          else
           sClients := '';
        end

        else
        begin
          // access to clients using NOT IN logic (worst case scenario)
          Q := TevQuery.Create('SELECT CL_NBR FROM TMP_CL');
          CheckCondition(Q.Result.RecordCount > 0, 'TMP_CL is empty. Please see administrator.');

          ClientList := TisStringList.Create;
          ClientList.Capacity := Q.Result.RecordCount;
          while not Q.Result.Eof do
          begin
            if not Rights.Clients.IsClientEnabled(Q.Result.Fields[0].AsInteger) then
              ClientList.Add(Q.Result.Fields[0].AsString);
            Q.Result.Next;
          end;
          if ClientList.Count > 0 then
            sClients := 'NOT IN (' + ClientList.CommaText + ')'
          else
            sClients := ''; // Access to all clients
        end;

      end
      else
        sClients := 'IN (0)'; // no access to any client
    end;

    i := ASecElements.AddElement('T:0:-:-');
    ASecElements.SetStateByIndex(i, stDisabled, False);
    ASecElements.GetSecRecordInfo(i).DBType := dbtTemporary;
    ASecElements.GetSecRecordInfo(i).ClientID := 0;
    ASecElements.GetSecRecordInfo(i).FilterType := '-';
    ASecElements.GetSecRecordInfo(i).Filter := sClients;

  finally
    ASecElements.EndChange;
    ASecElements.ClearModified;
  end;
end;


function TevSecInfoStorage.GetUserAccount(const ADomain, AUser, APassword: String; const AResetAttempts: Boolean): IevUserAccount;
var
  sUserID: String;
  i: Integer;
  Account: TevRemoteUserAccount;

  function CacheAccount: IevUserAccount;
  var
    i: Integer;
  begin
    // it's outside of CS because it's potentially long operation
    Result := TevRemoteUserAccount.Create(ADomain, AUser, APassword);
    TevRemoteUserAccount((Result as IisInterfacedObject).GetImplementation).Initialize(AResetAttempts);

    if AResetAttempts then
    begin
      FAccounts.Lock;
      try
        i := FAccounts.IndexOf(sUserID);  // make sure other threads haven't done this
        if i <> -1 then
          Result := FAccounts.Objects[i] as IevUserAccount
        else
          FAccounts.AddObject(sUserID, Result);
      finally
        FAccounts.UnLock;
      end;
    end;
  end;

begin
  sUserID := EncodeUserAtDomain(AUser, ADomain);

  FAccounts.Lock;
  try
    i := FAccounts.IndexOf(sUserID);
    if i <> -1 then
      Result := FAccounts.Objects[i] as IevUserAccount
    else
      Result := nil;
  finally
    FAccounts.UnLock;
  end;

  if not Assigned(Result) then
    Result := CacheAccount

  else
  begin
    Account := TevRemoteUserAccount((Result as IisInterfacedObject).GetImplementation);
    if not Account.CheckPassword(APassword) then
    begin
      // Password of the cached account is incorrect
      FAccounts.Lock;
      try
        i := FAccounts.IndexOf(sUserID);
        if i <> -1 then
          FAccounts.Delete(i); // delete it...
      finally
        FAccounts.UnLock;
      end;

      Result := CacheAccount; // ... and cache another one
    end;
  end;
end;


procedure TevSecInfoStorage.Cleanup;
begin
  FCS.Enter;
  try
    RemoveAllChildren;
    FAccounts.Clear;
    FSecurityStructureData := nil;
    FSecurityFieldsStructure := nil;
  finally
    FCS.Leave;
  end;
end;

procedure TevRemoteSecurityStructure.CreateHiddenFieldsAtoms(const Holder: TComponent);
var
  cdMaster, cdDetail: TevClientDataSet;
  m: IisStream;
  a: TevSecAtom;
  e: TevSecElement;
  Q: IevQuery;
begin
  Q := TevQuery.Create('SELECT STORAGE_DATA FROM SY_STORAGE WHERE {AsOfNow<SY_STORAGE>} AND TAG = ''' + FieldLevelStructureStorageTag + '''');

  if Q.Result.RecordCount = 1 then
  begin
    m := TisStream.Create;
    TBlobField(Q.Result.FieldByName('STORAGE_DATA')).SaveToStream(m.RealStream);
    DecodeFieldLevelSecurityStructure(m, cdMaster, cdDetail);
    try
      cdMaster.First;
      while not cdMaster.Eof do
      begin
        a := TevSecAtom.Create(Holder);
        a.AtomType := atFunc;
        a.AtomTag := cdMaster['TAG'];
        a.AtomCaption := cdMaster['NAME'];
        a.Contexts.Count;
        a.OnGetContextRecordArray := ProvideContexts;
        e := TevSecElement.Create(Holder);
        e.AtomComponent := a;
        e.ElementType := otFunction;
        e.ElementTag := a.AtomTag;
        cdDetail.First;
        while not cdDetail.Eof do
        begin
          if cdDetail['MAIN_NBR'] = cdMaster['MAIN_NBR'] then
          begin
            e := TevSecElement.Create(Holder);
            e.AtomComponent := a;
            e.ElementType := otDataField;
            e.ElementTag := cdDetail['TABLE_NAME'] + ':'+ cdDetail['FIELD_NAME'];
          end;
          cdDetail.Next;
        end;
        cdMaster.Next;
      end;
    finally
      cdMaster.Free;
      cdDetail.Free;
    end;
  end;
end;

procedure TevRemoteSecurityStructure.ProcessComponentRecurr(const Builder: ISchemeBuilder; const c: TComponent);

  procedure ProcessComponent(const c: TComponent);
  var
    SecurityAtom: ISecurityAtom;
    SecurityElement: ISecurityElement;
  begin
    c.GetInterface(ISecurityAtom, SecurityAtom);
    try
      if Assigned(SecurityAtom)
      and SecurityAtom.IsAtom then
        Builder.AddAtom(SecurityAtom);
    finally
      SecurityAtom := nil;
    end;
    c.GetInterface(ISecurityElement, SecurityElement);
    try
      if Assigned(SecurityElement) then
        Builder.AddElement(SecurityElement);
    finally
      SecurityElement := nil;
    end;
  end;

var
  i: Integer;
begin
  ProcessComponent(c);
  for i := 0 to c.ComponentCount-1 do
    ProcessComponentRecurr(Builder, c.Components[i]);
end;

procedure TevRemoteSecurityStructure.ProvideContexts(const Sender: ISecurityAtom; var ContextRecords: TAtomContextRecordArray);
begin
  SetLength(ContextRecords, 2);
  ContextRecords[0] := secFunctionOff;
  ContextRecords[1] := secFunctionOn;
end;

function TevRemoteSecurityStructure.Rebuild: IevDualStream;
var
  c, fc: TComponent;
  i, iL, j: Integer;
  Modules: IisList;
  Screen: IevScreenPackage;
  st: string;
  ma: TMenuSecAtom;
  ib: ISchemeBuilder;
  Builder: TBuilderSecurityAtomCollection;
  NewSec: TSecurityAtomCollection;
begin
  Builder := TBuilderSecurityAtomCollection.Create;
  try
    Supports(Builder, ISchemeBuilder, ib);
    // Create DataSet Security
    c := TComponent.Create(nil);
    try
      CreateDataSetAtoms(c);
      TdmCustomSecAtoms.Create(c);
      CreateHiddenFieldsAtoms(c);
      ProcessComponentRecurr(ib, c);
    finally
      c.Free;
    end;

    c := TComponent.Create(nil);
    try
      // Create Screens Security
      Modules := Mainboard.ModuleRegister.GetModules;
      for j := 0 to Modules.Count - 1 do
        if StartsWith((Modules[j] as IevModuleDescriptor).ModuleName, 'Screens ') then
        begin
          iL := Length(Structure);
          Screen := Mainboard.ModuleRegister.CreateModuleInstance((Modules[j] as IevModuleDescriptor).InstanceType) as IevScreenPackage;
          Screen.UserPackageStructure;

          for i := iL to High(Structure) do
          begin
            st := Copy(Structure[i].Path, 2, Length(Structure[i].Path));
            if Pos('\', st) > 0 then
              st := Copy(st, 1, Pred(Pos('\', st)));
            if Pos('|', st) > 0 then
              st := Copy(st, 1, Pred(Pos('|', st)));
            st := StringReplace(st, '&', '', [rfReplaceAll]);
            ma := FindMenuSecItem(st, c);

            if Structure[i].PClass <> '' then
            begin
              with TevSecElement.Create(ma) do
              begin
                AtomComponent := ma;
                ElementType := otScreen;
                ElementTag := Structure[i].PClass;
                Tag := i;
                OnSetState := SecElementCallback;
              end;

              fc := TComponentClass(FindClass(Structure[i].PClass)).Create(nil);
              try
                ProcessComponentRecurr(ib, fc);
              finally
                fc.Free;
              end;
            end;
          end;
        end;

      ProcessComponentRecurr(ib, c);
    finally
      c.Free;
    end;

    Result := TEvDualStreamHolder.Create;
    Builder.StreamStructureTo(Result);
  finally
    ib := nil;
    Builder.Free;
  end;

  Result.Position := 0;
  NewSec := TSecurityAtomCollection.CreateFromStream(Result);  // testing new structure
  NewSec.Free;
  Result.Position := 0;
end;

procedure TevRemoteSecurityStructure.SecElementCallback(const Sender: TComponent);
begin
  if Sender is TevSecElement then
    with TevSecElement(Sender) do
      if SecurityState = stDisabled then
      begin
        Structure[Tag].PClass := '';
      end;
end;


procedure TevSecInfoStorage.ChangeAccountPassword(const AUserAccount: IevUserAccount; const ANewPassword: String);
begin
  TevRemoteUserAccount((AUserAccount as IisInterfacedObject).GetImplementation).ChangePassword(ANewPassword);
end;

procedure TevSecInfoStorage.DropAccountRights(const AccountID: String);
var
  A: IevSecAccountRights;
begin
  A := FindAccountRights(AccountID);
  if Assigned(A) then
    RemoveChild(A as IisInterfacedObject);
end;

procedure TevSecInfoStorage.DropUserAccount(const ADomain, AUser: String);
var
  i: Integer;
begin
  FAccounts.Lock;
  try
    i := FAccounts.IndexOf(EncodeUserAtDomain(AUser, ADomain));
    if i <> -1 then
    begin
      DropAccountRights((FAccounts.Objects[i] as IevUserAccount).AccountID);
      FAccounts.Delete(i);
    end;
  finally
    FAccounts.UnLock;
  end;
end;

function TevSecInfoStorage.GetAccountGroupRights(const AccountID: String): IevSecAccountRights;
begin
  if AccountID = '' then
    Result := nil
  else if AnsiSameText(AccountID, sGuestUserName) then
    Result := FGuestAccRights
  else
  begin
    Result := TevRemoteSecAccountRights.Create(AccountID + '.G');
    TevRemoteSecAccountRights((Result as IisInterfacedObject).GetImplementation).Initialize;
  end;
end;

procedure TevSecInfoStorage.LoadForEE(const ASecElements: IevSecElements; const AType: Char;
  const AClNbr, AEENbr: Integer);
var
  Q: IevQuery;

  function CalculateStatus(const ACoLevel: String; const AEELevel: String): Char;
  begin
    if (AEELevel = 'F') and (ACoLevel = 'Y') then
      Result := stEnabled
    else if ((ACoLevel = 'R') and (AEELevel <> 'N')) or ((ACoLevel = 'Y') and (AEELevel = 'Y')) then
      Result := stReadOnly
    else
      Result := stDisabled;
  end;
begin
  if AType = etFunc then
  begin
    ctx_DataAccess.OpenClient(AClNbr);
    Q := TevQuery.Create('SELECT SELFSERVE_ENABLED, ENABLE_ESS, a.TIME_OFF_ENABLED, a.BENEFITS_ENABLED, ' +
      ' b.ENABLE_TIME_OFF, b.ENABLE_BENEFITS ' +
      ' FROM EE a, CO b WHERE {AsOfNow<a>} AND {AsOfNow<b>} AND A.EE_NBR=:EENbr AND A.CO_NBR=B.CO_NBR');
    Q.Params.AddValue('EENbr', AEENbr);
    Q.Execute;

    CheckCondition(Q.Result.RecordCount > 0, 'Employee not found. EE_NBR=' + IntToStr(AEENbr), ESecurity);
    ASecElements.SetState('ESS_EDIT', CalculateStatus(Q.Result.FieldByName('ENABLE_ESS').AsString, Q.Result.FieldByName('SELFSERVE_ENABLED').AsString), False);
    ASecElements.SetState('ESS_TIME_OFF_EDIT', CalculateStatus(Q.Result.FieldByName('ENABLE_TIME_OFF').AsString, Q.Result.FieldByName('TIME_OFF_ENABLED').AsString), False);
    ASecElements.SetState('ESS_BENEFITS_ENABLED_EDIT', CalculateStatus(Q.Result.FieldByName('ENABLE_BENEFITS').AsString, Q.Result.FieldByName('BENEFITS_ENABLED').AsString), False);
  end;
end;

function TevSecInfoStorage.GetUserAccount(const ADomain, AUser: String; AQuestionAnswerList: IisListOfValues): IevUserAccount;
var
  sQAString: String;
begin
  sQAString := QAListTag + AQuestionAnswerList.AsStream.AsString;
  Result := GetUserAccount(ADomain, AUser, sQAString, True);
end;

function TevSecInfoStorage.GetAvailableQuestions(const AUserName: String): IisStringList;
begin
  Result := FSecurityQuestions;
end;

function TevSecInfoStorage.GetQuestions(const AUserName: String): IisStringList;
var
  QAList: IisListOfValues;
  i: Integer;
begin
  QAList := GetQuestionAnswerList(AUserName);
  Result := TisStringList.Create;
  for i := 0 to QAList.Count - 1 do
    Result.Add(QAList[i].Name);
end;

function TevSecInfoStorage.GetQuestionAnswerList(const AUserName: String): IisListOfValues;
var
  Q: IevQuery;
  bUserFound: Boolean;
  AllQuestions: IisStringList;
  Cl_Nbr, EE_Nbr, i, QuestionNbr, LoginAttemts: Integer;
begin
  Context.DBAccess.DisableSecurity;
  try
    if StartsWith(AUserName, 'EE.') then
    begin
      Q := TevQuery.Create('SELECT CL_NBR, EE_NBR FROM TMP_EE WHERE Upper(SELFSERVE_USERNAME) = :UserName');
      Q.Params.AddValue('UserName', AnsiUpperCase(Copy(AUserName, 4, Length(AUserName))));
      Q.Execute;
      Cl_Nbr := Q.Result.Fields[0].AsInteger;
      EE_Nbr := Q.Result.Fields[1].AsInteger;

      ErrorIf(Q.Result.RecordCount > 1, 'This user name is not unique. Please recreate your account and try login again. If problem persists please contact service bureau.', ESecurity);
      bUserFound := Q.Result.RecordCount = 1;
      LoginAttemts := 0;

      if bUserFound then
      begin
        ctx_DataAccess.OpenClient(Cl_Nbr);

        Q := TevQuery.Create('GenericSelectCurrentWithCondition');
        Q.Macros.AddValue('TABLENAME', 'EE');
        Q.Macros.AddValue('COLUMNS', 'LOGIN_QUESTION1, LOGIN_QUESTION2, LOGIN_QUESTION3, LOGIN_ANSWER1, LOGIN_ANSWER2, LOGIN_ANSWER3, LOGIN_ATTEMPTS');
        Q.Macros.AddValue('CONDITION', 'EE_NBR = ' + IntToStr(EE_Nbr));
        Q.Execute;
        bUserFound := Q.Result.RecordCount = 1;
        LoginAttemts := Q.Result.Fields.Fieldbyname('LOGIN_ATTEMPTS').AsInteger;
      end

    end
    else
    begin
      Q := TevQuery.Create('GenericSelectCurrentWithCondition');
      Q.Macros.AddValue('TABLENAME', 'SB_USER');
      Q.Macros.AddValue('COLUMNS', 'LOGIN_QUESTION1, LOGIN_QUESTION2, LOGIN_QUESTION3, LOGIN_ANSWER1, LOGIN_ANSWER2, LOGIN_ANSWER3, WRONG_PSWD_ATTEMPTS');
      Q.Macros.AddValue('CONDITION', 'Upper(USER_ID) = :user_id');
      Q.Params.AddValue('user_id', AnsiUpperCase(AUserName));
      Q.Execute;

      bUserFound := Q.Result.RecordCount = 1;
      LoginAttemts := Q.Result.Fields.FieldByname('WRONG_PSWD_ATTEMPTS').AsInteger;
    end;

    CheckCondition(bUserFound, 'Invalid user name', ESecurity);
    ErrorIf(LoginAttemts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);

    Result := TisListOfValues.Create;
    AllQuestions := GetAvailableQuestions(AUserName);
    for i := 0 to 2 do
    begin
      QuestionNbr := Q.Result.Fields[i].AsInteger;
      if (QuestionNbr > 0) and (QuestionNbr <= AllQuestions.Count) and (Q.Result.Fields[i + 3].AsString <> '') then
        Result.AddValue(AllQuestions[QuestionNbr - 1], Q.Result.Fields[i + 3].AsString);
    end;

    if Result.Count <> 3 then
      Result.Clear;
  finally
    Context.DBAccess.EnableSecurity;
  end;
end;


procedure TevSecInfoStorage.SetQuestions(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
begin
  TevRemoteUserAccount((AUserAccount as IisInterfacedObject).GetImplementation).ChangeQuestions(AQuestionAnswerList);
end;

function TevSecInfoStorage.GetPasswordRules: IisListOfValues;
var
  Q: IevQuery;
  MinLength, MinLetters: Integer;
  MinNumbers, MinSymbols: Integer;
  Description: String;
begin
  Context.DBAccess.DisableSecurity;
  try
    Q := TevQuery.Create('SELECT pswd_min_length, pswd_force_mixed, user_password_duration_in_days, SB_URL, ENFORCE_EE_DOB_DEFAULT, ESS_LOGO  FROM sb WHERE {AsOfNow<sb>}');
    Q.Execute;

    MinLength := Q.Result.Fields[0].AsInteger;

    Result := TisListOfValues.Create;
    Description := '';

    if MinLength > 0 then
      AddStrValue(Description, Format('Minimum length must not be less than %d characters', [MinLength]), #13);

    if Q.Result.Fields[1].AsString = GROUP_BOX_YES then
    begin
      MinLetters := 1;
      MinNumbers := 1;
      MinSymbols := 1;

      AddStrValue(Description, Format('Must contain at least %d letter', [MinLetters]), #13);
      AddStrValue(Description, Format('Must contain at least %d number', [MinNumbers]), #13);
      AddStrValue(Description, Format('Must contain at least %d symbol', [MinSymbols]), #13);
    end
    else
    begin
      MinLetters := 0;
      MinNumbers := 0;
      MinSymbols := 0;
    end;

    Result.AddValue('Description', Description);
    Result.AddValue('MinLength', MinLength);
    Result.AddValue('MinLetters', MinLetters);
    Result.AddValue('MinNumbers', MinNumbers);
    Result.AddValue('MinSymbols', MinSymbols);
    Result.AddValue('LettersUpper', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    Result.AddValue('LettersLower', 'abcdefghijklmnopqrstuvwxyz');
    Result.AddValue('Numbers', '0123456789');
    Result.AddValue('Symbols', '~!@#$%^&*_-+=`|\(){}[]:;"''<>,.?/');

    Result.AddValue('MaxAgeInDays', Iff(Q.Result.Fields[2].AsInteger <= 0, 1, Q.Result.Fields[2].AsInteger));

    Result.AddValue('SB_URL', Q.Result.Fields[3].asString);
    Result.AddValue('ENFORCE_EE_DOB_DEFAULT', Q.Result.Fields[4].AsString);
    Result.AddValue('ESS_LOGO', Q.Result.Fields[5].asVariant);

  finally
    Context.DBAccess.EnableSecurity;
  end;
end;

function TevSecInfoStorage.GenerateNewPassword: String;
var
  Rules: IisListOfValues;
  iMinPasswordLength: integer;
  i: integer;
  sLetters, sNumbers, sSymbols: String;
  sNotMixedPassword: String;
begin
  Rules := GetPasswordRules;

  // Alyssa wants this
  Rules.Value['MinLetters'] := 1;
  Rules.Value['MinNumbers'] := 1;
  Rules.Value['MinSymbols'] := 1;

  Result := '';
  Randomize;

  if Rules.Value['MinLength'] < 8 then
    iMinPasswordLength := 8
  else
    iMinPasswordLength := Rules.Value['MinLength'];

  sNumbers := RandomString(Integer(Rules.Value['MinNumbers']), String(Rules.Value['Numbers']));
  sLetters := RandomString(Integer(Rules.Value['MinLetters']), String(Rules.Value['LettersUpper']) + String(Rules.Value['LettersLower']));
  sSymbols := RandomString(Integer(Rules.Value['MinSymbols']), String(Rules.Value['Symbols']));

  sNotMixedPassword := sNumbers + sLetters + sSymbols;
  if Length(sNotMixedPassword) < iMinPasswordLength then
    sNotMixedPassword := sNotMixedPassword + RandomString(iMinPasswordLength - Length(sNotMixedPassword), True);

  Result := '';
  while Length(sNotMixedPassword) > 0 do
  begin
    i := RandomInteger(1, Length(sNotMixedPassword) + 1);
    Result := Result + sNotMixedPassword[i];
    system.Delete(sNotMixedPassword, i, 1);
  end;
end;

procedure TevSecInfoStorage.ValidatePassword(const APassword: String);
var
  Rules: IisListOfValues;
  Res: Boolean;

  function CheckChars(const AChars: String; const ACount: Integer): Boolean;
  var
    n, i: Integer;
  begin
    n := 0;
    if ACount > 0 then
      for i := 1 to Length(APassword) do
        if Contains(AChars, APassword[i], True) then
          Inc(n);

    Result := n >= ACount;
  end;

begin
  ErrorIf(APassword = '', 'Password is empty', ESecurity);

  Rules := GetPasswordRules;

  if (Rules.Value['MinLength'] > 0) and (Length(APassword) < Rules.Value['MinLength']) then
    raise ESecurity.Create('Password length can not be less than ' + String(Rules.Value['MinLength']) + ' characters');

  Res := True;

  if Res then
    Res := CheckChars(String(Rules.Value['LettersUpper']) + String(Rules.Value['LettersLower']), Rules.Value['MinLetters']);

  if Res then
    Res := CheckChars(Rules.Value['Numbers'], Rules.Value['MinNumbers']);

  if Res then
    Res := CheckChars(Rules.Value['Symbols'], Rules.Value['MinSymbols']);

  if not Res then
    raise ESecurity.Create('Password does not meet complexity requirements');
end;


procedure TevSecInfoStorage.CheckExtAnswers(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
begin
  TevRemoteUserAccount((AUserAccount as IisInterfacedObject).GetImplementation).CheckExtAnswers(AQuestionAnswerList);
end;

function TevSecInfoStorage.GetExtQuestions(const AUserAccount: IevUserAccount): IisStringList;
begin
  Result := TevRemoteUserAccount((AUserAccount as IisInterfacedObject).GetImplementation).GetExtQuestions;
end;

function TevSecInfoStorage.GetSecurityRules: IisListOfValues;
var
  Q: IevQuery;
  ExtQARequired, PwdRestoreEnabled, SbExtLogin: Boolean;
begin
  Result := TisListOfValues.Create;

  Context.DBAccess.DisableSecurity;
  try
    Q := TevQuery.Create('SELECT ee_login_type FROM sb WHERE {AsOfNow<sb>}');
    Q.Execute;

    if Q.Result.Fields[0].AsString = LOGIN_TYPE_QUESTIONS then
    begin
      SbExtLogin:= True;
      ExtQARequired := True;
      PwdRestoreEnabled := False;
    end
    else if Q.Result.Fields[0].AsString = LOGIN_TYPE_QUESTIONS_RESTORE then
    begin
      SbExtLogin:= True;
      ExtQARequired := True;
      PwdRestoreEnabled := True;
    end
    else
    begin
      SbExtLogin:= False;    
      ExtQARequired := False;
      PwdRestoreEnabled := True;
    end;

    Result.AddValue('SbPasswordReset', True);
    Result.AddValue('SbExtLogin', SbExtLogin);
    Result.AddValue('EePasswordReset', PwdRestoreEnabled);
    Result.AddValue('EeExtLogin', ExtQARequired);
  finally
    Context.DBAccess.EnableSecurity;
  end;
end;

function TevSecInfoStorage.GetAvailableExtQuestions(const AUserName: String): IisStringList;
begin
  Result := FExtSecurityQuestions;
end;

function TevSecInfoStorage.GetExtQuestionAnswerList(const AUserName: String): IisListOfValues;
var
  Q: IevQuery;
  bUserFound: Boolean;
  AllQuestions: IisStringList;
  Cl_Nbr, EE_Nbr, i, QuestionNbr, LoginAttemts: Integer;
begin
  Context.DBAccess.DisableSecurity;
  try
    if StartsWith(AUserName, 'EE.') then
    begin
      Q := TevQuery.Create('SELECT CL_NBR, EE_NBR FROM TMP_EE WHERE Upper(SELFSERVE_USERNAME) = :UserName');
      Q.Params.AddValue('UserName', AnsiUpperCase(Copy(AUserName, 4, Length(AUserName))));
      Q.Execute;
      Cl_Nbr := Q.Result.Fields[0].AsInteger;
      EE_Nbr := Q.Result.Fields[1].AsInteger;

      ErrorIf(Q.Result.RecordCount > 1, 'This user name is not unique. Please recreate your account and try login again. If problem persists please contact service bureau.', ESecurity);
      bUserFound := Q.Result.RecordCount = 1;
      LoginAttemts := 0;

      if bUserFound then
      begin
        ctx_DataAccess.OpenClient(Cl_Nbr);

        Q := TevQuery.Create('GenericSelectCurrentWithCondition');
        Q.Macros.AddValue('TABLENAME', 'EE');
        Q.Macros.AddValue('COLUMNS', 'SEC_QUESTION1, SEC_QUESTION2, SEC_ANSWER1, SEC_ANSWER2, LOGIN_ATTEMPTS');
        Q.Macros.AddValue('CONDITION', 'EE_NBR = ' + IntToStr(EE_Nbr));
        Q.Execute;
        bUserFound := Q.Result.RecordCount = 1;
        LoginAttemts := Q.Result.Fields.FieldByName('LOGIN_ATTEMPTS').AsInteger;

      end

    end
    else
    begin
      Q := TevQuery.Create('GenericSelectCurrentWithCondition');
      Q.Macros.AddValue('TABLENAME', 'SB_USER');
      Q.Macros.AddValue('COLUMNS', 'SEC_QUESTION1, SEC_QUESTION2, SEC_ANSWER1, SEC_ANSWER2, WRONG_PSWD_ATTEMPTS');
      Q.Macros.AddValue('CONDITION', 'Upper(USER_ID) = :user_id');
      Q.Params.AddValue('user_id', AnsiUpperCase(AUserName));
      Q.Execute;

      bUserFound := Q.Result.RecordCount = 1;
      LoginAttemts := Q.Result.Fields.FieldByname('WRONG_PSWD_ATTEMPTS').AsInteger;
    end;

    CheckCondition(bUserFound, 'Invalid user name', ESecurity);
    ErrorIf(LoginAttemts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);

    Result := TisListOfValues.Create;
    AllQuestions := GetAvailableExtQuestions(AUserName);
    for i := 0 to 1 do
    begin
      QuestionNbr := Q.Result.Fields[i].AsInteger;
      if (QuestionNbr > 0) and (QuestionNbr <= AllQuestions.Count) and (Q.Result.Fields[i + 2].AsString <> '') then
        Result.AddValue(AllQuestions[QuestionNbr - 1], Q.Result.Fields[i + 2].AsString);
    end;

    if Result.Count <> 2 then
      Result.Clear;
  finally
    Context.DBAccess.EnableSecurity;
  end;
end;


procedure TevSecInfoStorage.SetExtQuestions(const AUserAccount: IevUserAccount; const AQuestionAnswerList: IisListOfValues);
begin
  TevRemoteUserAccount((AUserAccount as IisInterfacedObject).GetImplementation).ChangeExtQuestions(AQuestionAnswerList);
end;

procedure TevSecInfoStorage.ResetSBAccount(const AUser: String);
var
  T: IevTable;
begin
  // This mrthod must be called only under system account
  Assert(AnsiSameText(Context.UserAccount.User, ctx_DomainInfo.SystemAccountInfo.UserName));

  T := TevTable.Create('SB_USER',
    'WRONG_PSWD_ATTEMPTS, LOGIN_QUESTION1, LOGIN_ANSWER1, LOGIN_QUESTION2, LOGIN_ANSWER2, LOGIN_QUESTION3, LOGIN_ANSWER3, ' +
    'SEC_QUESTION1, SEC_ANSWER1, SEC_QUESTION2, SEC_ANSWER2',
    Format('UPPER(USER_ID) = ''%s''', [AnsiUpperCase(AUser)]));
  T.Open;

  if T.RecordCount = 1 then
  begin
    T.Edit;
    T['WRONG_PSWD_ATTEMPTS'] := 0;
    T['LOGIN_QUESTION1'] := Null;
    T['LOGIN_ANSWER1'] := Null;
    T['LOGIN_QUESTION2'] := Null;
    T['LOGIN_ANSWER2'] := Null;
    T['LOGIN_QUESTION3'] := Null;
    T['LOGIN_ANSWER3'] := Null;
    T['SEC_QUESTION1'] := Null;
    T['SEC_ANSWER1'] := Null;
    T['SEC_QUESTION2'] := Null;
    T['SEC_ANSWER2'] := Null;
    T.Post;
    T.SaveChanges;  
  end;
end;

{ TevRemoteUserAccount }

procedure TevRemoteUserAccount.Initialize(const AResetAttempts: Boolean);
begin
  // below is trusted code, so we need to deactivate security so it can get acces to user info
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, Domain), '');
    ctx_DBAccess.DisableSecurity;
    try
      if StartsWith(FUserName, 'EE.') then   // Checking for EE account (AKA Self-Serve)
        EEInitialize(AResetAttempts)
      else if IsADRUser(FUserName) then
        ReplicatorInitialize
      else if IsDBAUser(FUserName) then
        DBAInitialize
      else
        SBInitialize(AResetAttempts);
    finally
      ctx_DBAccess.EnableSecurity;
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRemoteUserAccount.EEInitialize(const AResetAttempts: Boolean);
var
  Q: IevQuery;
  ClPersonNbr, EENbr, CONbr, Attempts: Integer;
  s: String;
  ProvidedPassword: String;
  dsEE, dsCLPerson: TevClientDataSet;
  UseQAList: Boolean;
  SecRules: IisListOfValues;

  procedure UpdateWrongAttempts(const AValue: Integer);
  begin
    if AValue <= MaxWrongAnswersAttempts then
    begin
      dsEE.Edit;
      dsEE.Fields.FieldByName('LOGIN_ATTEMPTS').AsInteger := AValue;
      dsEE.Fields.FieldByName('LOGIN_DATE').AsDateTime := SysTime;
      dsEE.Post;
      ctx_DataAccess.PostDataSets([dsEE]);
      SendAccountChangeEmail(AValue);
    end;
  end;

begin
  CheckCondition(Context.License.SelfServe, 'There is no license for SelfServe', ESecurity);
  FAccountType := uatSelfServe;

  SecRules := SecInfoStorage.GetSecurityRules;
  FExtQARequired := SecRules.Value['EeExtLogin'];
  FPwdRestoreEnabled := SecRules.Value['EePasswordReset'];

  s := FUserName;
  Delete(s, 1, 3); // remove EE.

  Q := TevQuery.Create('SELECT CL_NBR, CL_PERSON_NBR, EE_NBR, CO_NBR FROM Tmp_Ee WHERE Upper(SELFSERVE_USERNAME) = :UserName');
  Q.Params.AddValue('UserName', AnsiUpperCase(s));
  Q.Execute;

  ProvidedPassword := FPassword;
  ErrorIf(Q.Result.RecordCount > 1, 'This user name is not unique. Please recreate your account and try login again. If problem persists please contact service bureau.', ESecurity);
  CheckCondition((Q.Result.RecordCount = 1), 'Invalid login or password', EInvalidLogin);

  ClPersonNbr := Q.Result.FieldValues['CL_PERSON_NBR'];
  EENbr := Q.Result.FieldValues['EE_NBR'];
  FInternalNbr := -EeNbr; // for audit purpose only. "-" indicates changes has been made by EE
  FEEClientNbr := Q.Result.FieldValues['CL_NBR'];
  CONbr := Q.Result.FieldValues['CO_NBR'];

  ctx_DataAccess.OpenClient(FEEClientNbr);

  // reading password as of now from EE table
  dsCLPerson := GetDSForEE('CL_PERSON');
  dsEE := GetDSForEE('EE');
  try
    dsEE.DataRequired('EE_NBR=' + IntToStr(EENbr));
    dsClPerson.DataRequired('CL_PERSON_NBR=' + IntToStr(ClPersonNbr));
    CheckCondition((dsEE.RecordCount = 1), 'Mismatch of TMP_EE and EE tables. Cannot find employee information.', ESecurity);
    CheckCondition((dsClPerson.RecordCount = 1), 'Mismatch of TMP_EE and CL_PERSON tables. Cannot find employee information.', ESecurity);

    Attempts := dsEE.Fields.FieldByName('LOGIN_ATTEMPTS').AsInteger;
    UseQAList := PasswordIsQAList;
    if UseQAList then
      CheckCondition(FPwdRestoreEnabled, 'Please contact administrator to reset password.', ESecurityViolation);

    ErrorIf(Attempts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);
    if not UseQAList then
      ErrorIf(Attempts >= MaxWrongPasswordAttempts, 'Account is locked. Security questions must be answered.', ELockedAccount);

    FUserName := 'EE.' + dsEE.FieldbyName('SELFSERVE_USERNAME').AsString;
    FFirstName := dsClPerson.FieldbyName('FIRST_NAME').AsString;
    FLastName := dsClPerson.FieldbyName('LAST_NAME').AsString;
    FEMail := dsEE.FieldbyName('E_MAIL_ADDRESS').AsString;
    if FEMail = '' then
      FEMail := dsClPerson.FieldbyName('E_MAIL_ADDRESS').AsString;
    FPasswordChangeDate := dsEE.FieldByName('SELFSERVE_LAST_LOGIN').AsDateTime;
    FSBAccountantNbr := 0;

    FPassword := dsEE.Fieldbyname('SELFSERVE_PASSWORD').AsString;
    if CheckPassword(ProvidedPassword) then
    begin
      if AResetAttempts and (Attempts <> 0)then
        UpdateWrongAttempts(0);
      Attempts := 0;
    end
    else
    begin
      if UseQAList and (Attempts < MaxWrongPasswordAttempts) then
        Attempts := MaxWrongPasswordAttempts;
      Inc(Attempts);

      if not FPwdRestoreEnabled and (Attempts >= MaxWrongPasswordAttempts) then
        Attempts := MaxWrongAnswersAttempts;

      UpdateWrongAttempts(Attempts);
      RequestCacheRefresh;
    end;

    if Attempts > 0 then
    begin
      ErrorIf(Attempts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);
      if UseQAList then
        raise EInvalidLogin.Create('Security answers do not match')
      else
      begin
        ErrorIf(Attempts >= MaxWrongPasswordAttempts, 'Account is locked. Security questions must be answered.', ELockedAccount);
        raise EInvalidLogin.Create('Invalid login or password');
      end;
    end;

    Q := TevQuery.Create('SELECT ENABLE_ESS FROM CO WHERE {AsOfNow<co>} and CO_NBR=:P_CO_NBR');
    Q.Params.AddValue('P_CO_NBR', CONbr);
    Q.Execute;
    CheckCondition(Q.Result.RecordCount = 1, 'Company not found or there is more than one company. CL_NBR: ' + IntToStr(FEEClientNbr) + ' CO_NBR: ' + IntToStr(CONbr), ESecurity);
    CheckCondition((Q.Result.Fields[0].AsString = 'Y') or (Q.Result.Fields[0].AsString = 'R'),
      'Company does not have Self Serve enabled', ESecurity);

    s := dsEE.FieldbyName('SELFSERVE_ENABLED').AsString;
    CheckCondition((s = 'Y') or (s = 'F'), 'Employee does not have Self Serve enabled', ESecurity);

    FAccountID := FDomain + '.E' + IntToStr(FEEClientNbr) + '.' + IntToStr(EENbr);
  finally
    FreeAndNil(dsEE);
    FreeAndNil(dsCLPerson);
  end;
end;


procedure TevRemoteUserAccount.SBInitialize(const AResetAttempts: Boolean);
var
  DS: TevClientDataSet;
  s: String;
  ProvidedPassword: String;
  UseQAList: Boolean;
  SecRules: IisListOfValues;
  Attempts: Integer;

  procedure UpdateWrongAttempts(const AValue: Integer);
  begin
    if AValue <= MaxWrongAnswersAttempts then
    begin
      DS.Edit;
      DS.FieldByName('WRONG_PSWD_ATTEMPTS').AsInteger := AValue;
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);

      SendAccountChangeEmail(AValue);
    end;
  end;

begin
  if (FUserName = '') or
      AnsiSameText(FUserName, sDefaultAdminUserName) and
      not AnsiSameText(ctx_DomainInfo.AdminAccount, sDefaultAdminUserName)
  then
    raise EInvalidLogin.Create('Invalid login or password');

  SecRules := SecInfoStorage.GetSecurityRules;
  FExtQARequired := SecRules.Value['SbExtLogin'];
  FPwdRestoreEnabled := SecRules.Value['SbPasswordReset'];

  // Checking Login and Password
  UseQAList := PasswordIsQAList;

  DS := TevClientDataSet.Create(nil);
  try
    DS.ProviderName := 'SB_CUSTOM_PROV';
    DS.CustomProviderTableName := 'SB_USER';
    DS.ClientID := -1;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', '*');
      SetMacro('TableName', 'SB_USER');
      SetMacro('Condition', 'Upper(USER_ID) = :user_id');
      SetParam('user_id', AnsiUpperCase(FUserName));
      DS.DataRequest(AsVariant);
    end;

    DS.Open;
    Attempts := DS.FieldByName('WRONG_PSWD_ATTEMPTS').AsInteger;

    if UseQAList then
      CheckCondition(FPwdRestoreEnabled, 'Please contact administrator to reset password.', ESecurityViolation);

    if DS.RecordCount <> 1 then
      if UseQAList then
        raise EInvalidLogin.Create('User name is not found')
      else
        raise EInvalidLogin.Create('Invalid login or password');
    CheckCondition(DS.FieldByName('ACTIVE_USER').AsString = 'Y', 'Account is inactive', ESecurity);

    ErrorIf(Attempts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);
    if not UseQAList then
      ErrorIf(Attempts >= MaxWrongPasswordAttempts, 'Account is locked. Security questions must be answered.', ELockedAccount);

    FUserName := DS.FieldByName('USER_ID').AsString;
    FInternalNbr := DS.FieldByName('SB_USER_NBR').AsInteger;
    FFirstName := DS.FieldByName('First_Name').AsString;
    FLastName := DS.FieldByName('Last_Name').AsString;
    FEMail := DS.FieldByName('EMAIL_ADDRESS').AsString;
    FSBAccountantNbr := DS.FieldByName('SB_ACCOUNTANT_NBR').AsInteger;
    FPasswordChangeDate := DS.FieldByName('PASSWORD_CHANGE_DATE').AsDateTime;

    s := ExtractFromFiller(DS.FieldByName('USER_FUNCTIONS').AsString, 1, 8);
    if s = '-1' then
    begin
      if AnsiSameText(FUserName, ctx_DomainInfo.AdminAccount) then
        FAccountType := uatSBAdmin
      else
        FAccountType := uatServiceBureau;
    end
    else if s = '-2' then
       FAccountType := uatWeb
    else
      FAccountType := uatRemote;

    ProvidedPassword :=  FPassword;
    if not AnsiSameText(FUserName, ctx_DomainInfo.SystemAccountInfo.UserName) then
      FPassword := DS.FieldByName('USER_PASSWORD').AsString;

    if CheckPassword(ProvidedPassword) then
    begin
      if AResetAttempts and (Attempts <> 0) then
        UpdateWrongAttempts(0);
      Attempts := 0;
    end
    else
    begin
      if UseQAList and (Attempts < MaxWrongPasswordAttempts) then
        Attempts := MaxWrongPasswordAttempts;
      Inc(Attempts);

      if not FPwdRestoreEnabled and (Attempts >= MaxWrongPasswordAttempts) then
        Attempts := MaxWrongAnswersAttempts;

      UpdateWrongAttempts(Attempts);
      RequestCacheRefresh;
    end;

    if Attempts > 0 then
    begin
      ErrorIf(Attempts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);
      if UseQAList then
        raise EInvalidLogin.Create('Security answers do not match')
      else
      begin
        ErrorIf(Attempts >= MaxWrongPasswordAttempts, 'Account is locked. Security questions must be answered.', ELockedAccount);
        raise EInvalidLogin.Create('Invalid login or password');
      end;
    end;

  finally
    DS.Free;
  end;

  FAccountID := FDomain + '.' + IntToStr(FInternalNbr);
end;

function  TevRemoteUserAccount.CheckPassword(const APassword: String): Boolean;

  function PasswordToQuestionAnswerList: IisListOfValues;
  var
    S: IisStream;
  begin
    if FPwdRestoreEnabled and StartsWith(APassword, QAListTag, True) then
    begin
      S := TisStream.Create;
      S.AsString := Copy(APassword, 5, Length(APassword));
      Result := TisListOfValues.Create;
      Result.AsStream := S;
    end
    else
      Result := nil;
  end;

var
  StoredQAList, QAList: IisListOfValues;
  Item: IisNamedValue;
  i: Integer;

begin
  QAList := PasswordToQuestionAnswerList;
  if Assigned(QAList) then
  begin
    // Security answers match
    StoredQAList := SecInfoStorage.GetQuestionAnswerList(User);
    Result := StoredQAList.Count > 0;
    for i := 0 to StoredQAList.Count - 1 do
    begin
      Item := StoredQAList.FindValue(QAList[i].Name);
      if not (Assigned(Item) and (Item.Value <> '') and AnsiSameStr(HashText(AnsiUpperCase(QAList[i].Value)), Item.Value)) then
      begin
        Result := False;
        break
      end;
    end;
  end
  
  else
    Result := AnsiSameStr(APassword, FPassword) or  // either user password match
              AnsiSameStr(APassword, ctx_DomainInfo.SystemAccountInfo.Password); // or system account password match
end;

procedure TevRemoteUserAccount.ChangePassword(const ANewPassword: String);
var
  Pass: String;
begin
  Pass := HashPassword(ANewPassword);

  if AccountType = uatSelfServe then
    DoEEChangePassword(Pass)
  else
    DoSBChangePassword(Pass);

  FPassword := Pass;
  SendAccountChangeEmail(-1);
end;

procedure TevRemoteUserAccount.DoEEChangePassword(const ANewPassword: String);
var
  DS: TevClientDataSet;
  sUser : String;
begin
  CheckCondition(StartsWith(User, 'EE.'), 'It is not EE user', ESecurity);
  ctx_DataAccess.OpenClient(FEEClientNbr);

  ctx_DBAccess.DisableSecurity;
  try
    // Update login information
    DS := GetDSForEE('EE');
    try
      DS.DataRequired('EE_NBR=' + IntToStr(-InternalNbr));

      sUser := Copy(User, 4, Length(User));
      CheckCondition((sUser = DS.FieldByName('SELFSERVE_USERNAME').AsString) and
                     CheckPassword(DS.FieldByName('SELFSERVE_PASSWORD').AsString),
                     'Current password is incorrect', EInvalidLogin);

      DS.Edit;
      DS.FieldByName('SELFSERVE_PASSWORD').AsString := ANewPassword;
      DS.FieldByName('SELFSERVE_LAST_LOGIN').AsDateTime := IncDay(DateOf(SysTime), SecInfoStorage.GetPasswordRules.Value['MaxAgeInDays']);      
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
    finally
      FreeAndNil(DS);
    end;
  finally
    ctx_DBAccess.EnableSecurity;
  end;

  FPassword := ANewPassword;
end;

procedure TevRemoteUserAccount.DoSBChangePassword(const ANewPassword: String);
var
  DS: TevClientDataSet;
begin
  ctx_DBAccess.DisableSecurity;
  try
    DS := TevClientDataSet.Create(nil);
    try
      DS.ProviderName := 'SB_CUSTOM_PROV';
      DS.CustomProviderTableName := 'SB_USER';

      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'SB_USER');
        SetMacro('Condition', 'Upper(USER_ID) = :user_id');
        SetParam('user_id', AnsiUpperCase(User));
        DS.DataRequest(AsVariant);
      end;
      DS.Open;

      CheckCondition(CheckPassword(DS.FieldByName('USER_PASSWORD').AsString), 'Current password is incorrect', EInvalidLogin);

      DS.Edit;
      DS.FieldByName('USER_PASSWORD').AsString := ANewPassword;
      DS.FieldByName('PASSWORD_CHANGE_DATE').AsDateTime := IncDay(DateOf(SysTime), SecInfoStorage.GetPasswordRules.Value['MaxAgeInDays']);
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
    finally
      FreeAndNil(DS);
    end;
  finally
    ctx_DBAccess.EnableSecurity;
  end;

  FPassword := ANewPassword;
end;

procedure TevRemoteUserAccount.ReplicatorInitialize;
begin
  FInternalNbr := MaxInt;
  FFirstName := '';
  FLastName := '';
  FEMail := '';
  FSBAccountantNbr := 0;
  FPasswordChangeDate := Now + 1000000;  // password never expires
  FAccountType := uatUnknown;
  FAccountID := FDomain + '.' + sADRUserName;
end;

function TevRemoteUserAccount.GetDSForEE(const ATableName: String): TevClientDataset;
var
  Tbl: TClass;
begin
  Tbl := GetClass('T' + ATableName);
  Result := TddTableClass(Tbl).Create(nil);
  try
    Result.SkipFieldCheck := True;
    Result.ProviderName := UpperCase(ATableName) + '_PROV';
    Result.ClientId := ctx_DataAccess.ClientId;
  except
    FreeAndNil(Result);
    raise;
  end;
end;


function TevRemoteUserAccount.PasswordIsQAList: Boolean;
begin
  Result := StartsWith(Password, QAListTag, True);
end;

procedure TevRemoteUserAccount.ChangeQuestions(const AQuestionAnswerList: IisListOfValues);
var
  AllQuestions: IisStringList;
  PreparedList: IisListOfValues;
  i, n: Integer;
begin
  PreparedList := TisListOfValues.Create;
  AllQuestions := SecInfoStorage.GetAvailableQuestions(User);
  for i := 0 to AQuestionAnswerList.Count - 1 do
  begin
    n := AllQuestions.IndexOf(AQuestionAnswerList[i].Name) + 1;
    CheckCondition(n > 0, 'Security question is unknown', ESecurity);

    CheckCondition(not PreparedList.ValueExists(IntToStr(n)), 'Security question must be unique', ESecurity);
    CheckCondition(Trim(AQuestionAnswerList[i].Value) <> '', 'Answers on security questions cannot be empty', ESecurity);

    PreparedList.AddValue(IntToStr(n), AQuestionAnswerList[i].Value);
  end;

  if AccountType = uatSelfServe then
    DoEEChangeQuestions(PreparedList)
  else
    DoSBChangeQuestions(PreparedList);

  SendAccountChangeEmail(-2);
end;

procedure TevRemoteUserAccount.DoEEChangeQuestions(const AQuestionAnswerList: IisListOfValues);
var
  DS: TevClientDataSet;
  sUser : String;
  Qnbr, Answer: Variant;
  i: Integer;
begin
  CheckCondition(StartsWith(User, 'EE.'), 'It is not EE user', ESecurity);
  ctx_DataAccess.OpenClient(FEEClientNbr);

  ctx_DBAccess.DisableSecurity;
  try
    // Update login information
    DS := GetDSForEE('EE');
    try
      DS.DataRequired('EE_NBR=' + IntToStr(-InternalNbr));
      sUser := Copy(User, 4, Length(User));
      CheckCondition((sUser = DS.FieldByName('SELFSERVE_USERNAME').AsString) and
                     CheckPassword(DS.FieldByName('SELFSERVE_PASSWORD').AsString),
                     'Current password is incorrect', EInvalidLogin);

      DS.Edit;
      for i := 1 to 3 do
      begin
        if AQuestionAnswerList.Count >= i then
        begin
          Qnbr := StrToInt(AQuestionAnswerList[i - 1].Name);
          Answer := HashText(AnsiUpperCase(AQuestionAnswerList[i - 1].Value));
        end
        else
        begin
          Qnbr := Null;
          Answer := Null;
        end;

        DS.FieldByName('LOGIN_QUESTION' + IntToStr(i)).Value := Qnbr;
        DS.FieldByName('LOGIN_ANSWER' + IntToStr(i)).Value := Answer;
      end;
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
    finally
      FreeAndNil(DS);
    end;
  finally
    ctx_DBAccess.EnableSecurity;
  end;
end;

procedure TevRemoteUserAccount.DoSBChangeQuestions(const AQuestionAnswerList: IisListOfValues);
var
  DS: TevClientDataSet;
  Qnbr, Answer: Variant;
  i: Integer;
begin
  ctx_DBAccess.DisableSecurity;
  try
    DS := TevClientDataSet.Create(nil);
    try
      DS.ProviderName := 'SB_CUSTOM_PROV';
      DS.CustomProviderTableName := 'SB_USER';

      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'SB_USER');
        SetMacro('Condition', 'Upper(USER_ID) = :user_id');
        SetParam('user_id', AnsiUpperCase(User));
        DS.DataRequest(AsVariant);
      end;
      DS.Open;

      CheckCondition(CheckPassword(DS.FieldByName('USER_PASSWORD').AsString), 'Current password is incorrect', EInvalidLogin);

      DS.Edit;
      for i := 1 to 3 do
      begin
        if AQuestionAnswerList.Count >= i then
        begin
          Qnbr := StrToInt(AQuestionAnswerList[i - 1].Name);
          Answer := HashText(AnsiUpperCase(AQuestionAnswerList[i - 1].Value));
        end
        else
        begin
          Qnbr := Null;
          Answer := Null;
        end;

        DS.FieldByName('LOGIN_QUESTION' + IntToStr(i)).Value := Qnbr;
        DS.FieldByName('LOGIN_ANSWER' + IntToStr(i)).Value := Answer;
      end;
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
    finally
      FreeAndNil(DS);
    end;
  finally
    ctx_DBAccess.EnableSecurity;
  end;
end;

procedure TevRemoteUserAccount.CheckExtAnswers(const AQuestionAnswerList: IisListOfValues);
begin
  if FExtQARequired then
  begin
    ctx_DBAccess.DisableSecurity;
    try
      if StartsWith(FUserName, 'EE.') then
        DoEeCheckExtAnswers(AQuestionAnswerList)
      else
        DoSbCheckExtAnswers(AQuestionAnswerList);
    finally
      ctx_DBAccess.EnableSecurity;
    end;
  end;
end;

function TevRemoteUserAccount.GetExtQuestions: IisStringList;
var
  QAList: IisListOfValues;
  i: Integer;
begin
  if not FExtQARequired then
  begin
    Result := nil;
    exit;
  end;

  if StartsWith(FUserName, 'EE.') then
    QAList := DoEeGetExtQuestions
  else
    QAList := DoSbGetExtQuestions;

  Result := TisStringList.Create;

  if QAList.Count > 0 then
    for i := 0 to QAList.Count - 1 do
      Result.Add(QAList[i].Name);
end;

procedure TevRemoteUserAccount.DoEECheckExtAnswers(const AQuestionAnswerList: IisListOfValues);
var
  Q: IevQuery;
  EENbr, Attempts : Integer;

  procedure UpdateWrongAttempts(const AValue: Integer);
  var
    T: IevTable;
  begin
    if AValue <= MaxWrongAnswersAttempts then
    begin
      T := TevTable.Create('EE', 'LOGIN_ATTEMPTS,LOGIN_DATE', 'EE_NBR =' + inttostr(EENbr));
      T.Open;
      if T.RecordCount = 1 then
      begin
        T.Edit;
        T.FieldByName('LOGIN_ATTEMPTS').AsInteger := AValue;
        T.FieldByName('LOGIN_DATE').AsDateTime := SysTime;
        T.Post;
        T.SaveChanges;
        SendAccountChangeEmail(AValue);
      end;
    end;
  end;

begin
  EENbr := - InternalNbr;
  ctx_DataAccess.OpenClient(FEEClientNbr);
  Q := TevQuery.Create('SELECT LOGIN_ATTEMPTS FROM EE WHERE {AsOfNow<EE>} and EE_NBR = ' + IntToStr(EENbr));
  Q.Execute;
  Attempts := Q.Result.Fields[0].AsInteger;

  if ExtAnswersCorrect(DoEEGetExtQuestions, AQuestionAnswerList) then
    Attempts := 0
  else
  begin
    Inc(Attempts);

    if not FPwdRestoreEnabled and (Attempts >= MaxWrongPasswordAttempts) then
      Attempts := MaxWrongAnswersAttempts;

    UpdateWrongAttempts(Attempts);
    RequestCacheRefresh;
  end;

  if Attempts > 0 then
  begin
    ErrorIf(Attempts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);
    ErrorIf(Attempts >= MaxWrongPasswordAttempts, 'Account is locked. Security questions must be answered.', ELockedAccount);
    raise EInvalidLogin.Create('Security answers do not match');
  end;
end;

function TevRemoteUserAccount.DoEEGetExtQuestions: IisListOfValues;
begin
  Result := DoSBGetExtQuestions;
end;

procedure TevRemoteUserAccount.DoSBCheckExtAnswers(const AQuestionAnswerList: IisListOfValues);
var
  DS: TevClientDataSet;
  Attempts: Integer;

  procedure UpdateWrongAttempts(const AValue: Integer);
  begin
    if AValue <= MaxWrongAnswersAttempts then
    begin
      DS.Edit;
      DS.FieldByName('WRONG_PSWD_ATTEMPTS').AsInteger := AValue;
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
      SendAccountChangeEmail(AValue);
    end;
  end;

begin
  DS := TevClientDataSet.Create(nil);
  try
    DS.ProviderName := 'SB_CUSTOM_PROV';
    DS.CustomProviderTableName := 'SB_USER';
    DS.ClientID := -1;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', '*');
      SetMacro('TableName', 'SB_USER');
      SetMacro('Condition', 'Upper(USER_ID) = :user_id');
      SetParam('user_id', AnsiUpperCase(FUserName));
      DS.DataRequest(AsVariant);
    end;
    DS.Open;

    if DS.RecordCount <> 1 then
      raise EInvalidLogin.Create('User name is not found');

    Attempts := DS.FieldByName('WRONG_PSWD_ATTEMPTS').AsInteger;

    if ExtAnswersCorrect(DoSBGetExtQuestions, AQuestionAnswerList) then
      Attempts := 0
    else
    begin
      Inc(Attempts);

      if not FPwdRestoreEnabled and (Attempts >= MaxWrongPasswordAttempts) then
        Attempts := MaxWrongAnswersAttempts;

      UpdateWrongAttempts(Attempts);
      RequestCacheRefresh;
    end;

    if Attempts > 0 then
    begin
      ErrorIf(Attempts >= MaxWrongAnswersAttempts, 'Account is blocked. Please contact administrator to reset password.', EBlockedAccount);
      ErrorIf(Attempts >= MaxWrongPasswordAttempts, 'Account is locked. Security questions must be answered.', ELockedAccount);
      raise EInvalidLogin.Create('Security answers do not match');
    end;

  finally
    DS.Free;
  end;
end;

function TevRemoteUserAccount.DoSBGetExtQuestions: IisListOfValues;
begin
  Result := SecInfoStorage.GetExtQuestionAnswerList(User);
end;

function TevRemoteUserAccount.ExtAnswersCorrect(const AQuestions, AAnswers: IisListOfValues): Boolean;
var
  i: Integer;
  Item: IisNamedValue;
begin
  Result := AAnswers.Count >= 2;
  if not Result then
    Exit;

  for i := 0 to AAnswers.Count - 1 do
  begin
    Item := AQuestions.FindValue(AAnswers[i].Name);
    if not Assigned(Item) or (HashText(AnsiUpperCase(AAnswers[i].Value)) <> Item.Value) then
    begin
      Result := False;
      break;
    end;
  end;
end;

procedure TevRemoteUserAccount.RequestCacheRefresh;
begin
  Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain(FUserName, FDomain));
end;

procedure TevRemoteUserAccount.SendAccountChangeEmail(const ALoginAttempts: Integer);
var
  sBody, sFrom: String;
begin
  if FEMail = '' then
    Exit;

  if ALoginAttempts = MaxWrongAnswersAttempts then
    sBody :=  'All login attempts are exhausted. Account is blocked. Please contact administrator to reset password.'
  else if ALoginAttempts = MaxWrongPasswordAttempts then
    sBody :=  'Three login attempts are exhausted. Account is locked. Security questions must be answered.'
  else if ALoginAttempts = -1 then
    sBody :=  'Your password has been successfully changed'
  else if ALoginAttempts = -2 then
    sBody :=  'Your security questions have been successfully changed'
  else if ALoginAttempts = -3 then
    sBody :=  'Your extended security questions have been successfully changed'
  else
    sBody := '';

  if sBody <> '' then
  begin
    { which email address sent to? }
    sBody := sBody + #13#10#13#10 + 'This message sent to: ' + FEmail + ' (' + FUsername + ')';

    { The "code" helps us locate the internal CL# and SB domain, especially on the SaaS }
    sBody := sBody + #13#10#13#10 + 'Code: ' + IntToStr(FEEClientNbr) + '@' + FDomain;

    if AccountType = uatWeb then
      sFrom := 'EvoWebClient'
    else if AccountType = uatSelfServe then
      sFrom := 'EvoSelfServe'
    else
      sFrom := 'Evo';

    try
      ctx_EMailer.SendQuickMail(FEMail, sFrom, sFrom + ' account alert', sBody);
    except
      on E: Exception do
        mb_LogFile.AddEvent(etError, 'Security', 'Cannot send e-mail notification', E.Message + #13#10 + GetStack(E));
    end;
  end;  
end;

procedure TevRemoteUserAccount.ChangeExtQuestions(const AQuestionAnswerList: IisListOfValues);
var
  AllQuestions: IisStringList;
  PreparedList: IisListOfValues;
  i, n: Integer;
begin
  PreparedList := TisListOfValues.Create;
  AllQuestions := SecInfoStorage.GetAvailableExtQuestions(User);
  for i := 0 to AQuestionAnswerList.Count - 1 do
  begin
    n := AllQuestions.IndexOf(AQuestionAnswerList[i].Name) + 1;
    CheckCondition(n > 0, 'Security question is unknown', ESecurity);

    CheckCondition(not PreparedList.ValueExists(IntToStr(n)), 'Security question must be unique', ESecurity);
    CheckCondition(Trim(AQuestionAnswerList[i].Value) <> '', 'Answers on security questions cannot be empty', ESecurity);

    PreparedList.AddValue(IntToStr(n), AQuestionAnswerList[i].Value);
  end;

  if AccountType = uatSelfServe then
    DoEEChangeExtQuestions(PreparedList)
  else
    DoSBChangeExtQuestions(PreparedList);

  SendAccountChangeEmail(-3);
end;

procedure TevRemoteUserAccount.DoEEChangeExtQuestions(const AQuestionAnswerList: IisListOfValues);
var
  DS: TevClientDataSet;
  sUser : String;
  Qnbr, Answer: Variant;
  i: Integer;
begin
  CheckCondition(StartsWith(User, 'EE.'), 'It is not EE user', ESecurity);
  ctx_DataAccess.OpenClient(FEEClientNbr);

  ctx_DBAccess.DisableSecurity;
  try
    // Update login information
    DS := GetDSForEE('EE');
    try
      DS.DataRequired('EE_NBR=' + IntToStr(-InternalNbr));
      sUser := Copy(User, 4, Length(User));
      CheckCondition((sUser = DS.FieldByName('SELFSERVE_USERNAME').AsString) and
                     CheckPassword(DS.FieldByName('SELFSERVE_PASSWORD').AsString),
                     'Current password is incorrect', EInvalidLogin);

      DS.Edit;
      for i := 1 to 2 do
      begin
        if AQuestionAnswerList.Count >= i then
        begin
          Qnbr := StrToInt(AQuestionAnswerList[i - 1].Name);
          Answer := HashText(AnsiUpperCase(AQuestionAnswerList[i - 1].Value));
        end
        else
        begin
          Qnbr := Null;
          Answer := Null;
        end;

        DS.FieldByName('SEC_QUESTION' + IntToStr(i)).Value := Qnbr;
        DS.FieldByName('SEC_ANSWER' + IntToStr(i)).Value := Answer;
      end;
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
    finally
      FreeAndNil(DS);
    end;
  finally
    ctx_DBAccess.EnableSecurity;
  end;
end;

procedure TevRemoteUserAccount.DoSBChangeExtQuestions(const AQuestionAnswerList: IisListOfValues);
var
  DS: TevClientDataSet;
  Qnbr, Answer: Variant;
  i: Integer;
begin
  ctx_DBAccess.DisableSecurity;
  try
    DS := TevClientDataSet.Create(nil);
    try
      DS.ProviderName := 'SB_CUSTOM_PROV';
      DS.CustomProviderTableName := 'SB_USER';

      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'SB_USER');
        SetMacro('Condition', 'Upper(USER_ID) = :user_id');
        SetParam('user_id', AnsiUpperCase(User));
        DS.DataRequest(AsVariant);
      end;
      DS.Open;

      CheckCondition(CheckPassword(DS.FieldByName('USER_PASSWORD').AsString), 'Current password is incorrect', EInvalidLogin);

      DS.Edit;
      for i := 1 to 2 do
      begin
        if AQuestionAnswerList.Count >= i then
        begin
          Qnbr := StrToInt(AQuestionAnswerList[i - 1].Name);
          Answer := HashText(AnsiUpperCase(AQuestionAnswerList[i - 1].Value));
        end
        else
        begin
          Qnbr := Null;
          Answer := Null;
        end;

        DS.FieldByName('SEC_QUESTION' + IntToStr(i)).Value := Qnbr;
        DS.FieldByName('SEC_ANSWER' + IntToStr(i)).Value := Answer;
      end;
      DS.Post;
      ctx_DataAccess.PostDataSets([DS]);
    finally
      FreeAndNil(DS);
    end;
  finally
    ctx_DBAccess.EnableSecurity;
  end;
end;

procedure TevRemoteUserAccount.DBAInitialize;
begin
  FInternalNbr := MaxInt - 1;
  FFirstName := '';
  FLastName := '';
  FEMail := '';
  FSBAccountantNbr := 0;
  FPasswordChangeDate := Now + 1000000;  // password never expires
  FAccountType := uatUnknown;
  FAccountID := FDomain + '.' + sDBAUserName;
end;

{ TevGuestSecAccountRights }

procedure TevGuestSecAccountRights.Initialize;
begin
  FInitialized := True;
end;

procedure TevGuestSecAccountRights.SaveChangesToDB;
begin
// nothing
end;

initialization
  SecInfoStorage := TevSecInfoStorage.Create;

  FSecurityQuestions := TisStringList.Create;
  FSecurityQuestions.Text :=
    'What was your childhood nickname?'#13 +
    'What is your father''s middle name?'#13 +
    'What is your grandmother''s first name?'#13 +
    'What was your high school mascot?'#13 +
    'What is your favorite pet''s name?'#13 +
    'What was your first car (make and model)?'#13 +
    'What is the name of the first company you worked for?'#13 +
    'What is the first name of your oldest niece/nephew?'#13 +
    'Where did you go on your honeymoon?'#13 +
    'What is the last name of your favorite teacher?';

  FExtSecurityQuestions := TisStringList.Create;
  FExtSecurityQuestions.Text :=
    'In what town does your oldest sibling live?'#13 +
    'What food have you always liked?'#13 +
    'Who was your favorite teacher in high school?'#13 +
    'What is the name of your first best friend?'#13 +
    'What is the name of the school where you attended first grade?'#13 +
    'In what city or town did you meet your spouse or significant other?'#13 +
    'In what town does your nearest sibling live?'#13 +
    'What is your oldest cousin''s first and last name?'#13 +
    'What is the name of the first person you kissed?'#13 +
    'In what city or town did your parents meet?'#13 +
    'In what city or town was your first job?'#13 +
    'On what street did you live in fourth grade?'#13 +
    'What is the middle name of your oldest child?'#13 +
    'In what city or town does your closest cousin live?';

finalization
  SecInfoStorage := nil;

end.

