// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSecurityPxy;

interface

uses
   Controls, Sysutils, Classes, Windows, isBaseClasses, EvCommonInterfaces,
   EvMainboard, EvContext, EvStreamUtils, IsBasicUtils, EvTypes, EvUtils, evProxy,
   EvConsts, EvTransportDatagrams, evRemoteMethods, EvClasses, EvTransportInterfaces,
   EvBasicUtils;

implementation

type
  TevSecurityProxy = class(TevProxyModule, IevSecurity)
  private
    FAccountRights: IevSecAccountRights;
    FSystemAccountRights: IevSecAccountRights;
    FGuestAccount: IevUserAccount;
    FSysAccRigthsCount: Integer;
    FNeedRefresh: Boolean;
    procedure PrepareSystemAccount;
  protected
    procedure DoOnConstruction; override;
    function  GetAuthorization: IevUserAccount;
    function  AccountRights: IevSecAccountRights;
    function  GetSecStructure: IevSecurityStructure;
    function  GetUserSecStructure(const AUserInternalNbr: Integer): IevSecurityStructure;
    function  GetGroupSecStructure(const ASecGroupInternalNbr: Integer): IevSecurityStructure;
    procedure SetGroupSecStructure(const ASecGroupInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
    procedure SetUserSecStructure(const AUserInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
    function  GetUserGroupLevelRights(const AUserInternalNbr: Integer): IevSecAccountRights;
    function  GetUserRights(const AUserInternalNbr: Integer): IevSecAccountRights;
    function  GetGroupRights(const ASecGroupInternalNbr: Integer): IevSecAccountRights;
    procedure CreateEEAccount(const AUserName, APassword, ACompanyNumber, ASSN: String;
                              const ACheckNumber: Integer; const ACheckTotalEarnings: Currency);
    procedure ChangePassword(const ANewPassword: String);
    procedure EnableSystemAccountRights;
    procedure DisableSystemAccountRights;
    procedure Refresh(const AReasonInfo: String);
    function  GetAvailableQuestions(const AUserName: String): IisStringList;
    function  GetAvailableExtQuestions(const AUserName: String): IisStringList;
    procedure SetQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure SetExtQuestions(const AQuestionAnswerList: IisListOfValues);
    function  GetQuestions(const AUserName: String): IisStringList;
    procedure ResetPassword(const AUserName: String; const AQuestionAnswerList: IisListOfValues; const ANewPassword: String);
    procedure CheckAnswers(const AUserName: String; const AQuestionAnswerList: IisListOfValues);
    function  GenerateNewPassword: String;
    procedure ValidatePassword(const APassword: String);
    function  GetPasswordRules: IisListOfValues;
    function  GetExtQuestions(const AUserName: String; const APassword: String): IisStringList;
    procedure CheckExtAnswers(const AUserName: String; const APassword: String; const AQuestionAnswerList: IisListOfValues);
    function  GetSecurityRules: IisListOfValues;
  end;


  TevGuestAccount = class(TevUserAccount)
  public
    constructor Create; reintroduce;
  end;


function GetSecurity: IevSecurity;
begin
  Result := TevSecurityProxy.Create;
end;


{ TevSecurityProxy }

function TevSecurityProxy.GetAuthorization: IevUserAccount;
var
  D: IevDatagram;
begin
  // checking for guest account
  if AnsiSameText(Context.UserAccount.User, sGuestUserName) then
  begin
    TevGuestAccount((FGuestAccount as IisInterfacedObject).GetImplementation).FDomain := Context.UserAccount.Domain;
    Result := FGuestAccount;
    FAccountRights := TevSecAccountRights.Create(sGuestUserName);
  end

  else
  begin
    if not IsADRUser(Context.UserAccount.User) then
      Context.License.Check;  // check that license is valid and not expired

    D := CreateDatagram(METHOD_SECURITY_GETAUTHORIZATION);
    D := SendRequest(D, False);

    Result := IInterface(D.Params.Value['Result']) as IevUserAccount;
    FAccountRights := IInterface(D.Params.Value['AccountRights']) as IevSecAccountRights;
  end;
end;


procedure TevSecurityProxy.DoOnConstruction;
begin
  inherited;
  FGuestAccount := TevGuestAccount.Create;
end;

function TevSecurityProxy.AccountRights: IevSecAccountRights;
begin
  if FNeedRefresh then
  begin
    FSystemAccountRights := nil;
    GetAuthorization;
    FNeedRefresh := False;
  end;

  if FSysAccRigthsCount > 0 then
  begin
    PrepareSystemAccount;
    Result := FSystemAccountRights
  end
  else
  begin
    if not Assigned(FAccountRights) then
      GetAuthorization;
    Result := FAccountRights;
  end;
end;

function TevSecurityProxy.GetGroupSecStructure(const ASecGroupInternalNbr: Integer): IevSecurityStructure;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETUGROUPSECSTRUCTURE);
  D.Params.AddValue('ASecGroupInternalNbr', ASecGroupInternalNbr);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevSecurityStructure;
end;

function TevSecurityProxy.GetUserSecStructure(const AUserInternalNbr: Integer): IevSecurityStructure;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETUSERSECSTRUCTURE);
  D.Params.AddValue('AUserInternalNbr', AUserInternalNbr);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevSecurityStructure;
end;

function TevSecurityProxy.GetSecStructure: IevSecurityStructure;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETSECSTRUCTURE);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevSecurityStructure;
end;

procedure TevSecurityProxy.CreateEEAccount(const AUserName, APassword, ACompanyNumber, ASSN: String;
  const ACheckNumber: Integer; const ACheckTotalEarnings: Currency);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_CREATEEEACCOUNT);
  D.Params.AddValue('AUserName', AUserName);
  D.Params.AddValue('APassword', APassword);
  D.Params.AddValue('ACompanyNumber', ACompanyNumber);
  D.Params.AddValue('ASSN', ASSN);
  D.Params.AddValue('ACheckNumber', ACheckNumber);
  D.Params.AddValue('ACheckTotalEarnings', ACheckTotalEarnings);
  D := SendRequest(D, False);
end;


procedure TevSecurityProxy.ChangePassword(const ANewPassword: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_CHANGEPASSWORD);
  D.Params.AddValue('ANewPassword', ANewPassword);
  D := SendRequest(D, False);
end;

procedure TevSecurityProxy.SetGroupSecStructure(const ASecGroupInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_SETGROUPSECSTRUCTURE);
  D.Params.AddValue('ASecGroupInternalNbr', ASecGroupInternalNbr);
  D.Params.AddValue('ASecStruct', ASecStruct);
  D := SendRequest(D, False);
end;

procedure TevSecurityProxy.SetUserSecStructure(const AUserInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_SETUSERSECSTRUCTURE);
  D.Params.AddValue('AUserInternalNbr', AUserInternalNbr);
  D.Params.AddValue('ASecStruct', ASecStruct);
  D := SendRequest(D, False);
end;

procedure TevSecurityProxy.DisableSystemAccountRights;
begin
  Dec(FSysAccRigthsCount);
  Assert(FSysAccRigthsCount >= 0);
end;

procedure TevSecurityProxy.EnableSystemAccountRights;
begin
  PrepareSystemAccount;
  Inc(FSysAccRigthsCount);
end;

procedure TevSecurityProxy.Refresh(const AReasonInfo: String);
var
  s: String;
begin
  if StartsWith(AReasonInfo, 'Rights ') then
  begin
    s := AReasonInfo;
    GetNextStrValue(s, 'Rights ');
    if AnsiSameText(Context.UserAccount.AccountID, s) then
      FNeedRefresh := True;
  end

  else if StartsWith(AReasonInfo, 'Account ') then
  begin
    s := AReasonInfo;
    GetNextStrValue(s, 'Account ');
    if AnsiSameText(s, EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain)) then
      FNeedRefresh := True;
  end

  else
    FNeedRefresh := True;
end;

procedure TevSecurityProxy.PrepareSystemAccount;
var
  D: IevDatagram;
begin
  if not Assigned(FSystemAccountRights) then
  begin
    D := CreateDatagram(METHOD_SECURITY_ENABLESYSTEMACCOUNTRIGHTS);
    D := SendRequest(D, False);
    FSystemAccountRights := IInterface(D.Params.Value['SystemAccountRights']) as IevSecAccountRights;
  end;
end;


function TevSecurityProxy.GetUserGroupLevelRights(const AUserInternalNbr: Integer): IevSecAccountRights;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETUGROUPLEVELRIGHTS);
  D.Params.AddValue('AUserInternalNbr', AUserInternalNbr);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevSecAccountRights;
end;

function TevSecurityProxy.GetGroupRights(const ASecGroupInternalNbr: Integer): IevSecAccountRights;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETGROUPRIGHTS);
  D.Params.AddValue('ASecGroupInternalNbr', ASecGroupInternalNbr);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevSecAccountRights;
end;

function TevSecurityProxy.GetUserRights(const AUserInternalNbr: Integer): IevSecAccountRights;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETUSERRIGHTS);
  D.Params.AddValue('AUserInternalNbr', AUserInternalNbr);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevSecAccountRights;
end;

procedure TevSecurityProxy.CheckAnswers(const AUserName: String;
  const AQuestionAnswerList: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_CHECKANSWERS);
  D.Params.AddValue('AUserName', AUserName);
  D.Params.AddValue('AQuestionAnswerList', AQuestionAnswerList);
  D := SendRequest(D, False);
end;

procedure TevSecurityProxy.CheckExtAnswers(const AUserName,
  APassword: String; const AQuestionAnswerList: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_CHECKEXTANSWERS);
  D.Params.AddValue('AUserName', AUserName);
  D.Params.AddValue('APassword', APassword);
  D.Params.AddValue('AQuestionAnswerList', AQuestionAnswerList);
  D := SendRequest(D, False);
end;

function TevSecurityProxy.GenerateNewPassword: String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GENERATENEWPASSWORD);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevSecurityProxy.GetAvailableQuestions(const AUserName: String): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETAVAILABLEQUESTIONS);
  D.Params.AddValue('AUserName', AUserName);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevSecurityProxy.GetExtQuestions(const AUserName, APassword: String): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETEXTQUESTIONS);
  D.Params.AddValue('AUserName', AUserName);
  D.Params.AddValue('APassword', APassword);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevSecurityProxy.GetPasswordRules: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETPASSWORDRULES);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevSecurityProxy.GetQuestions(const AUserName: String): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETQUESTIONS);
  D.Params.AddValue('AUserName', AUserName);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

procedure TevSecurityProxy.ResetPassword(const AUserName: String;
  const AQuestionAnswerList: IisListOfValues; const ANewPassword: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_RESETPASSWORD);
  D.Params.AddValue('AUserName', AUserName);
  D.Params.AddValue('AQuestionAnswerList', AQuestionAnswerList);
  D.Params.AddValue('ANewPassword', ANewPassword);
  D := SendRequest(D, False);
end;

procedure TevSecurityProxy.SetQuestions(const AQuestionAnswerList: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_SETQUESTIONS);
  D.Params.AddValue('AQuestionAnswerList', AQuestionAnswerList);
  D := SendRequest(D, False);
end;

procedure TevSecurityProxy.ValidatePassword(const APassword: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_VALIDATEPASSWORD);
  D.Params.AddValue('APassword', APassword);
  D := SendRequest(D, False);
end;

function TevSecurityProxy.GetSecurityRules: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETSECURITYRULES);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevSecurityProxy.GetAvailableExtQuestions(const AUserName: String): IisStringList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_GETAVAILABLEEXTQUESTIONS);
  D.Params.AddValue('AUserName', AUserName);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;


procedure TevSecurityProxy.SetExtQuestions(const AQuestionAnswerList: IisListOfValues);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_SECURITY_SETEXTQUESTIONS);
  D.Params.AddValue('AQuestionAnswerList', AQuestionAnswerList);
  D := SendRequest(D, False);
end;

{ TevGuestAccount }

constructor TevGuestAccount.Create;
begin
  inherited Create(sDefaultDomain, sGuestUserName, '');
  FInternalNbr := sGuestInternalNbr;
  FAccountType := uatGuest;
  FAccountID := sGuestUserName;
  FPasswordChangeDate := Now + 1000000;  // password never expires;  
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSecurity, IevSecurity, 'Security');

end.

