// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSecurityMod;

interface

uses
   Controls, Sysutils, Classes, Windows, DB,  SSecurityClassDefs, isBaseClasses, EvCommonInterfaces, EvExceptions,
   EvMainboard, EvContext, EvStreamUtils, IsBasicUtils, EvTypes, EvSecurityInfoStorage, EvUtils, EvDataSet,
   ISZippingRoutines, EvConsts, isDataSet, EvClasses, isLogFile, EvInitApp, StrUtils, EvBasicUtils, Variants,
   SDDClasses, EvClientDataSet;

implementation

type
  TSecurityComp = class(TisInterfacedObject, IevSecurity)
  private
    FAccountRights: IevSecAccountRights;
    FSystemAccount: IevUserAccount;
    FSystemAccountRights: IevSecAccountRights;
    FGuestAccount: IevUserAccount;
    FSysAccRigthsCount: Integer;
    FNeedRefresh: Boolean;
    procedure PrepareSystemAccount;
    procedure DoChangePassword(const AUserAccount: IevUserAccount; const ANewPassword: String);
  protected
    procedure DoOnConstruction; override;
    function  GetAuthorization: IevUserAccount;
    function  AccountRights: IevSecAccountRights;
    function  GetSecStructure: IevSecurityStructure;
    function  GetUserSecStructure(const AUserInternalNbr: Integer): IevSecurityStructure;
    function  GetGroupSecStructure(const ASecGroupInternalNbr: Integer): IevSecurityStructure;
    procedure SetGroupSecStructure(const ASecGroupInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
    procedure SetUserSecStructure(const AUserInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
    function  GetUserGroupLevelRights(const AUserInternalNbr: Integer): IevSecAccountRights;
    function  GetUserRights(const AUserInternalNbr: Integer): IevSecAccountRights;
    function  GetGroupRights(const ASecGroupInternalNbr: Integer): IevSecAccountRights;
    procedure CreateEEAccount(const AUserName, APassword, ACompanyNumber, ASSN: String;
                              const ACheckNumber: Integer; const ACheckTotalEarnings: Currency);
    procedure ChangePassword(const ANewPassword: String);
    procedure EnableSystemAccountRights;
    procedure DisableSystemAccountRights;
    procedure Refresh(const AReasonInfo: String);

    function  GetAvailableQuestions(const AUserName: String): IisStringList;
    function  GetAvailableExtQuestions(const AUserName: String): IisStringList;
    procedure SetQuestions(const AQuestionAnswerList: IisListOfValues);
    procedure SetExtQuestions(const AQuestionAnswerList: IisListOfValues);
    function  GetQuestions(const AUserName: String): IisStringList;
    procedure ResetPassword(const AUserName: String; const AQuestionAnswerList: IisListOfValues; const ANewPassword: String);
    procedure CheckAnswers(const AUserName: String; const AQuestionAnswerList: IisListOfValues);
    function  GenerateNewPassword: String;
    procedure ValidatePassword(const APassword: String);
    function  GetPasswordRules: IisListOfValues;
    function  GetExtQuestions(const AUserName: String; const APassword: String): IisStringList;
    procedure CheckExtAnswers(const AUserName: String; const APassword: String; const AQuestionAnswerList: IisListOfValues);
    function  GetSecurityRules: IisListOfValues;
  end;


  TevGuestAccount = class(TevUserAccount)
  public
    constructor Create; reintroduce;
  end;


function GetSecurity: IInterface;
begin
  Result := TSecurityComp.Create;
end;


{ TSecurityComp }

function TSecurityComp.GetAuthorization: IevUserAccount;
begin
  // checking for guest account
  if AnsiSameText(Context.UserAccount.User, sGuestUserName) then
  begin
    TevGuestAccount((FGuestAccount as IisInterfacedObject).GetImplementation).FDomain := Context.UserAccount.Domain;
    Result := FGuestAccount;
  end
  else
  begin
    if isStandalone then
      Context.License.Check;  // check that license is valid and not expired
    Result := SecInfoStorage.GetUserAccount(Context.UserAccount.Domain, Context.UserAccount.User, Context.UserAccount.Password, True);
  end;
end;

procedure TSecurityComp.DoOnConstruction;
begin
  inherited;
  FGuestAccount := TevGuestAccount.Create;
end;

function TSecurityComp.AccountRights: IevSecAccountRights;
begin
  if FNeedRefresh then
  begin
    FSystemAccountRights := nil;
    if Context.UserAccount.AccountID <> '' then
    begin
      FAccountRights := SecInfoStorage.GetAccountRights(Context.UserAccount.AccountID);
      FNeedRefresh := False;
    end;
  end;

  if FSysAccRigthsCount > 0 then
  begin
    PrepareSystemAccount;
    Result := FSystemAccountRights;
  end

  else
  begin
    if not Assigned(FAccountRights) then
    begin
      FAccountRights := SecInfoStorage.FindAccountRights(Context.UserAccount.AccountID);

      if not Assigned(FAccountRights) then
        FAccountRights := SecInfoStorage.GetAccountRights(Context.UserAccount.AccountID);

      if not Assigned(FAccountRights) then
        FAccountRights := SecInfoStorage.GetAccountRights(FGuestAccount.AccountID);
    end;

    Result := FAccountRights;
  end;
end;

function TSecurityComp.GetGroupSecStructure(const ASecGroupInternalNbr: Integer): IevSecurityStructure;
begin
  Result := SecInfoStorage.GetStructure(Context.UserAccount.Domain + '.G' + IntToStr(ASecGroupInternalNbr));
end;

function TSecurityComp.GetUserSecStructure(const AUserInternalNbr: Integer): IevSecurityStructure;
begin
  Result := SecInfoStorage.GetStructure(Context.UserAccount.Domain + '.' + IntToStr(AUserInternalNbr));
end;

function TSecurityComp.GetSecStructure: IevSecurityStructure;
begin
  Result := SecInfoStorage.GetStructure('');
end;

procedure TSecurityComp.CreateEEAccount(const AUserName, APassword, ACompanyNumber, ASSN: String;
  const ACheckNumber: Integer; const ACheckTotalEarnings: Currency);
var
  Q: IevQuery;
  bValidEE: Boolean;
  xmlStream: TStringStream;
  blobStream: TStringStream;
  xmlData: String;
  i: Integer;
  iEeNbr: Integer;
  DS: TevClientDataSet;
  SSN : String;
  iCLNbr : integer;
  oldUsername : String;
  Tbl: TClass;
begin
  Context.DBAccess.DisableSecurity;
  try
    // adding dashes because input doesn't have them
    SSN := LeftStr(ASSN, 3) + '-' + Copy(ASSN, 4, 2) + '-' + Copy(ASSN, 6, 4);

    // Define CL_NBR
    Q := TevQuery.Create('SELECT CL_NBR, CO_NBR FROM TMP_CO WHERE CUSTOM_COMPANY_NUMBER = :CompanyNumber');
    Q.Params.AddValue('CompanyNumber', ACompanyNumber);
    CheckCondition(Q.Result.RecordCount = 1, 'Provided employee information is not valid', ESecurity);

    iCLNbr := Q.Result.FieldByName('CL_NBR').AsInteger;
    ctx_DataAccess.OpenClient(iCLNbr);

    // check CO.Enable_ESS
    Q := TevQuery.Create('select ENABLE_ESS from CO where CUSTOM_COMPANY_NUMBER = :CompanyNumber and {AsOfNow<CO>}');
    Q.Params.AddValue('CompanyNumber', ACompanyNumber);
    CheckCondition(Q.Result.RecordCount = 1, 'Company not found or there is more than one company. CustomCompanyNumber: "' + ACompanyNumber + '"', ESecurity);
    CheckCondition(Q.Result.FieldByName('ENABLE_ESS').AsString <> 'N', 'SelfServe is disabled for this company. CustomCompanyNumber: "' + ACompanyNumber + '"', ESecurity);

    // Check PR information
    Q := TevQuery.Create(
      'SELECT e.EE_NBR, p.GROSS_WAGES, b.DATA ' +
      'FROM PR_CHECK p ' +
      'left outer join CL_BLOB b on p.DATA_NBR=b.CL_BLOB_NBR ' +
      'join EE e on p.EE_NBR=e.EE_NBR ' +
      'join CL_PERSON c on e.CL_PERSON_NBR=c.CL_PERSON_NBR ' +
      'WHERE c.SOCIAL_SECURITY_NUMBER = :SSN AND p.PAYMENT_SERIAL_NUMBER = :CheckNbr AND '+
      '{AsOfNow<c>} AND {AsOfNow<e>} AND {AsOfNow<p>} AND {AsOfNow<b>}');
    Q.Params.AddValue('SSN', SSN);
    Q.Params.AddValue('CheckNbr', ACheckNumber);
    if Q.Result.RecordCount = 0 then
    begin
      SSN := LeftStr(ASSN, 2) + '-' + Copy(ASSN, 4, MaxInt);  // EIN format
      Q.Params.AddValue('SSN', SSN);
      Q.Params.AddValue('CheckNbr', ACheckNumber);
      Q.Execute;
      CheckCondition(Q.Result.RecordCount = 1, 'Incorrect employee information', ESecurity);
    end
    else
      CheckCondition(Q.Result.RecordCount = 1, 'Found more than one employee with the same information', ESecurity);

    bValidEE := Abs(Q.Result.FieldByName('GROSS_WAGES').AsFloat - ACheckTotalEarnings) < 0.01;
    iEeNBR := Q.Result.FieldByName('EE_NBR').AsInteger;
                            //Old Field:PR_CHECK.OVERRIDE_CHECK_NOTES
    if not bValidEE and not Q.Result.FieldByName('DATA').IsNull then
    begin
      // get BLOB with paystub XML and read TotalEarnings value
      xmlStream := nil;
      blobStream := nil;
      try
        xmlStream := TStringStream.Create('');
        blobStream := TStringStream.Create(Q.Result.FieldByName('DATA').AsString);
        blobStream.Position := 0;
        InflateStream(blobStream, xmlStream);
        xmlStream.Position := 0;
        if xmlStream.Size > 1 then
        begin
          xmlData := xmlStream.DataString;
          i := Pos('<totalEarnCurrent>',xmlData);
          if (i>0) and (Pos('</totalEarnCurrent>',xmlData)>i+18) and
             (StrToFloat(Copy(xmlData, i+18, Pos('</totalEarnCurrent>',xmlData)-i-18)) = ACheckTotalEarnings) then
             bValidEE := True;
        end;
      finally
        blobStream.Free;
        xmlStream.Free;
      end;
    end;

    CheckCondition(bValidEE, 'Incorrect employee information', ESecurity);

    // check if the same SS_USERNAME exists for any employee in any client
    Q := TevQuery.Create('SELECT CL_NBR, EE_NBR FROM TMP_EE WHERE Upper(SELFSERVE_USERNAME) = :UserName');
    Q.Params.AddValue('UserName', AnsiUpperCase(AUserName));

    // if forgotten password
    if Q.Result.RecordCount = 1 then
      CheckCondition((Q.Result.FieldValues['CL_NBR'] = iCLNbr) and (Q.Result.FieldValues['EE_NBR'] = iEeNbr),
        'User name already exists. Please select another one.', ESecurity)
    else
      ErrorIf(Q.Result.RecordCount > 1, 'This user name is not unique. Please recreate your account and try login again. If problem persists please contact service bureau.', ESecurity);

    // Update login information as of now
    Tbl := GetClass('TEE');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TEE', ESecurity);
    DS := TddTableClass(Tbl).Create(nil);
    try
      DS.SkipFieldCheck := True;
      DS.ProviderName := 'EE_PROV';
      DS.ClientId := ctx_DataAccess.CLientId;

      DS.DataRequired('EE_NBR=' + IntToStr(iEeNBR));

      if DS['SELFSERVE_USERNAME'] <> null then
        oldUsername := DS['SELFSERVE_USERNAME']
      else
        oldUserName := '';

      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try
        DS.Edit;
        DS.FieldByName('SELFSERVE_USERNAME').AsString := AUserName;
        DS.FieldByName('SELFSERVE_PASSWORD').AsString := APassword;
        DS.FieldByName('LOGIN_QUESTION1').Clear;
        DS.FieldByName('LOGIN_QUESTION2').Clear;
        DS.FieldByName('LOGIN_QUESTION3').Clear;
        DS.FieldByName('LOGIN_ANSWER1').Clear;
        DS.FieldByName('LOGIN_ANSWER2').Clear;
        DS.FieldByName('LOGIN_ANSWER3').Clear;
        DS.FieldByName('SEC_QUESTION1').Clear;
        DS.FieldByName('SEC_ANSWER1').Clear;
        DS.FieldByName('SEC_QUESTION2').Clear;
        DS.FieldByName('SEC_ANSWER2').Clear;
        DS.Post;
        ctx_DataAccess.PostDataSets([DS]);
        ctx_DataAccess.CommitNestedTransaction;
      except
        ctx_DataAccess.RollbackNestedTransaction;
        raise;
      end;
      
      DS.Close;
    finally
      FreeAndNil(DS);
    end;
  finally
    Context.DBAccess.EnableSecurity;
  end;

  // sending notification in order to delete its previous information from security cache
  if trim(oldUsername) <> '' then
    Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain('EE.' + oldUsername, Context.UserAccount.Domain));
end;

procedure TSecurityComp.ChangePassword(const ANewPassword: String);
begin
  ValidatePassword(ANewPassword);
  DoChangePassword(Context.UserAccount, ANewPassword);
end;

procedure TSecurityComp.SetGroupSecStructure(const ASecGroupInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
begin
  SecInfoStorage.SetStructure(Context.UserAccount.Domain + '.G' + IntToStr(ASecGroupInternalNbr), ASecStruct);
end;

procedure TSecurityComp.SetUserSecStructure(const AUserInternalNbr: Integer; const ASecStruct: IevSecurityStructure);
begin
  SecInfoStorage.SetStructure(Context.UserAccount.Domain + '.' + IntToStr(AUserInternalNbr), ASecStruct);
end;

procedure TSecurityComp.DisableSystemAccountRights;
begin
  Dec(FSysAccRigthsCount);
  Assert(FSysAccRigthsCount >= 0);

  if FSysAccRigthsCount = 0 then
    ctx_DBAccess.SetChangedByNbr(0);
end;

procedure TSecurityComp.EnableSystemAccountRights;
begin
  PrepareSystemAccount;
  Inc(FSysAccRigthsCount);

  if FSysAccRigthsCount = 1 then
    ctx_DBAccess.SetChangedByNbr(FSystemAccount.InternalNbr);
end;

procedure TSecurityComp.Refresh(const AReasonInfo: String);
var
  s, u, d: String;
begin
  if StartsWith(AReasonInfo, 'Rights ') then
  begin
    s := AReasonInfo;
    GetNextStrValue(s, 'Rights ');
    SecInfoStorage.DropAccountRights(s);
  end
  else if StartsWith(AReasonInfo, 'Account ') then
  begin
    s := AReasonInfo;
    GetNextStrValue(s, 'Account ');
    DecodeUserAtDomain(s, u, d);
    SecInfoStorage.DropUserAccount(d, u);
  end
  else
    SecInfoStorage.Cleanup;

  FNeedRefresh := True;
end;

procedure TSecurityComp.PrepareSystemAccount;
begin
  if not Assigned(FSystemAccountRights) then
  begin
    try
      FSystemAccount := SecInfoStorage.GetUserAccount(ctx_DomainInfo.DomainName,
        ctx_DomainInfo.SystemAccountInfo.UserName, ctx_DomainInfo.SystemAccountInfo.Password, True);
    except
      on E: Exception do
      begin
        E.Message := 'Cannot switch to SU mode. ' + E.Message;
        Mainboard.LogFile.AddContextEvent(etError, 'Security', E.Message, '');
        raise;
      end;
    end;

    FSystemAccountRights := SecInfoStorage.GetAccountRights(FSystemAccount.AccountID);
  end;
end;

function TSecurityComp.GetUserGroupLevelRights(const AUserInternalNbr: Integer): IevSecAccountRights;
begin
  Result := SecInfoStorage.GetAccountGroupRights(Context.UserAccount.Domain + '.' + IntToStr(AUserInternalNbr));
end;

function TSecurityComp.GetUserRights(const AUserInternalNbr: Integer): IevSecAccountRights;
begin
  Result := SecInfoStorage.GetAccountRights(Context.UserAccount.Domain + '.' + IntToStr(AUserInternalNbr));
end;

function TSecurityComp.GetGroupRights(const ASecGroupInternalNbr: Integer): IevSecAccountRights;
begin
  Result := SecInfoStorage.GetAccountRights(Context.UserAccount.Domain + '.G' + IntToStr(ASecGroupInternalNbr));
end;

function TSecurityComp.GetAvailableQuestions(const AUserName: String): IisStringList;
begin
  Result := SecInfoStorage.GetAvailableQuestions(AUserName);
end;

function TSecurityComp.GetQuestions(const AUserName: String): IisStringList;
begin
  Result := SecInfoStorage.GetQuestions(AUserName);
end;

procedure TSecurityComp.ResetPassword(const AUserName: String; const AQuestionAnswerList: IisListOfValues; const ANewPassword: String);
var
  Account: IevUserAccount;
begin
  ValidatePassword(ANewPassword);

  if AnsiSameText(Context.UserAccount.User, ctx_DomainInfo.SystemAccountInfo.UserName) then
  begin
    // System account can reset user password without Q/A
    SecInfoStorage.ResetSBAccount(AUserName);
    Account := SecInfoStorage.GetUserAccount(Context.UserAccount.Domain, AUserName, Context.UserAccount.Password, False);
    DoChangePassword(Account, ANewPassword);
  end
  else
  begin
    Account := SecInfoStorage.GetUserAccount(Context.UserAccount.Domain, AUserName, AQuestionAnswerList);

    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(Account.User, Account.Domain), Account.Password);
      ctx_Security.ChangePassword(ANewPassword);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;
end;

procedure TSecurityComp.SetQuestions(const AQuestionAnswerList: IisListOfValues);
begin
  SecInfoStorage.SetQuestions(Context.UserAccount, AQuestionAnswerList);
  Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
end;

procedure TSecurityComp.CheckAnswers(const AUserName: String; const AQuestionAnswerList: IisListOfValues);
var
  Account: IevUserAccount;
begin
  Account := SecInfoStorage.GetUserAccount(Context.UserAccount.Domain, AUserName, AQuestionAnswerList);
end;

function TSecurityComp.GenerateNewPassword: String;
begin
  Result := SecInfoStorage.GenerateNewPassword;
end;

procedure TSecurityComp.ValidatePassword(const APassword: String);
begin
  SecInfoStorage.ValidatePassword(APassword);
end;

function TSecurityComp.GetPasswordRules: IisListOfValues;
begin
  Result := SecInfoStorage.GetPasswordRules;
end;

procedure TSecurityComp.CheckExtAnswers(const AUserName: String; const APassword: String; const AQuestionAnswerList: IisListOfValues);
var
  Account: IevUserAccount;
begin
  Account := SecInfoStorage.GetUserAccount(Context.UserAccount.Domain, AUserName, APassword, False);
  SecInfoStorage.CheckExtAnswers(Account, AQuestionAnswerList);
end;

function TSecurityComp.GetExtQuestions(const AUserName: String; const APassword: String): IisStringList;
var
  Account: IevUserAccount;
begin
  Account := SecInfoStorage.GetUserAccount(Context.UserAccount.Domain, AUserName, APassword, False);
  Result := SecInfoStorage.GetExtQuestions(Account);
end;

function TSecurityComp.GetSecurityRules: IisListOfValues;
begin
  Result := SecInfoStorage.GetSecurityRules;
end;

function TSecurityComp.GetAvailableExtQuestions(const AUserName: String): IisStringList;
begin
  Result := SecInfoStorage.GetAvailableExtQuestions(AUserName);
end;

procedure TSecurityComp.SetExtQuestions(const AQuestionAnswerList: IisListOfValues);
begin
  SecInfoStorage.SetExtQuestions(Context.UserAccount, AQuestionAnswerList);
  Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
end;

procedure TSecurityComp.DoChangePassword(const AUserAccount: IevUserAccount; const ANewPassword: String);
begin
  SecInfoStorage.ChangeAccountPassword(AUserAccount, ANewPassword);
  Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain(AUserAccount.User, AUserAccount.Domain));
end;

{ TevGuestAccount }

constructor TevGuestAccount.Create;
begin
  inherited Create(sDefaultDomain, sGuestUserName, '');
  FInternalNbr := sGuestInternalNbr;
  FAccountType := uatGuest;
  FAccountID := sGuestUserName;
  FPasswordChangeDate := Now + 1000000;  // password never expires;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSecurity, IevSecurity, 'Security');

end.

