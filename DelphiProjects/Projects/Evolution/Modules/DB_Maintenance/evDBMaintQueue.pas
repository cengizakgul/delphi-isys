unit evDBMaintQueue;

interface

uses Windows, SysUtils, isBaseClasses, evMainboard, EvCommonInterfaces, EvContext,
     evConsts, EvBasicUtils, isThreadManager, ISBasicUtils, evTypes, Math,
     isExceptions, isVCLBugFix, EvStreamUtils, ISErrorUtils, DateUtils, EvUtils,
     isLogFile;

type
  IevDBMaintQueue = interface(IisInterfacedObject)
  ['{913B15CF-ED99-4BB1-8781-985ABF036448}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetThreadsPerServer: IisListOfValues;
    procedure UpdateThreadsPerServer(const AHost: String; const AMaxThreads: Integer);
    function  GetOnQueueChanged: TOnQueueChangedEvent;
    procedure SetOnQueueChanged(const AValue: TOnQueueChangedEvent);
    procedure Clear;
    function  GetStatus: TevQueueStatus;
    function  GetItems(const AState: TevQueueItemStates): IisList;
    procedure PopulateWithDBPatchItems(const AAutoSweep: Boolean; const AAppVer: String; AAllowDowngrade: Boolean);
    procedure PopulateWithDBSweepItems;
    procedure PopulateWithTidyUpDBFoldersItems;
    procedure AddDBImportItem(const ASrcDatabase, ASrcDBPassword: String; const ASizeMB: Integer);
    procedure AddDBSweepItem(const ADatabase: String; const ASizeMB: Integer);
    procedure AddDBBackupItem(const ADatabase: String; const ASizeMB: Integer; const AFolder: String; const ACompression, AScramble: Boolean);
    procedure AddDBRestoreItem(const ADatabase: String; const ASizeMB: Integer; const ABackupFile: String);
    procedure AddDBCustomScriptItem(const ADatabase: String; const ASizeMB: Integer; const AScript: String; const ASweep: Boolean);
    property  ThreadsPerServer: IisListOfValues read GetThreadsPerServer;
    property  OnQueueChanged: TOnQueueChangedEvent read GetOnQueueChanged write SetOnQueueChanged;
    property  Active: Boolean read GetActive write SetActive;
  end;


  function  CreateDBPatchQueue: IevDBMaintQueue;

implementation

type
  TevQueueThread = class;

  TevDBMaintQueue = class(TisInterfacedObject, IevDBMaintQueue)
  private
    FTM: IThreadManager;
    FQueueThread: TevQueueThread;
    FQueuedList: IisCollection;
    FRunningList: IisCollection;
    FOnQueueChanged: TOnQueueChangedEvent;
    FThreadsPerServer: IisListOfValues;
    FAllItemsWeight: Cardinal;
    FFinishedItemsWeight: Cardinal;
    FEstimatedSpeed: Double;
    FStatus: TevQueueStatus;

    procedure AddItem(const AItem: IevDBItem);
    procedure MoveItemToTop(const AIndex: Integer);
    procedure MoveItemToBottom(const AIndex: Integer);
    procedure Notify(const AItem: IevDBItem; const AOperation: TevQCEOperation);
    procedure PopulateWithDBPatchItems(const AAutoSweep: Boolean; const AAppVer: String; AAllowDowngrade: Boolean);
    procedure PopulateWithDBSweepItems;
    procedure PopulateWithTidyUpDBFoldersItems;
    procedure AddDBImportItem(const ASrcDatabase, ASrcDBPassword: String; const ASizeMB: Integer);
    procedure AddDBSweepItem(const ADatabase: String; const ASizeMB: Integer);
    procedure AddDBBackupItem(const ADatabase: String; const ASizeMB: Integer; const AFolder: String; const ACompression, AScramble: Boolean);
    procedure AddDBRestoreItem(const ADatabase: String; const ASizeMB: Integer; const ABackupFile: String);
    procedure AddDBCustomScriptItem(const ADatabase: String; const ASizeMB: Integer; const AScript: String; const ASweep: Boolean);
    procedure Clear;
    function  GetStatus: TevQueueStatus;
    function  GetItems(const AState: TevQueueItemStates): IisList;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetThreadsPerServer: IisListOfValues;
    procedure UpdateThreadsPerServer(const AHost: String; const AMaxThreads: Integer);    
    function  GetOnQueueChanged: TOnQueueChangedEvent;
    procedure SetOnQueueChanged(const AValue: TOnQueueChangedEvent);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;


  TevDBItem = class(TisNamedObject, IevDBItem, IevContextCallbacks)
  private
    FQueue: TevDBMaintQueue;
    FParams: IisListOfValues;
    FDBName: String;
    FDBSizeMB: Cardinal;
    FDomainInfo: IevDomainInfo;
    FADRContext: Boolean;
    FDBLocation: String;
    FDBHost: String;
    FState: TevQueueItemState;
    FStatus: String;
    FDetails: String;
    FStartedAt: TDateTime;
    FFinishedAt: TDateTime;
    FProgress: Integer;

    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);

    function  Queue: TevDBMaintQueue;
    procedure RunInThread(const AParams: TTaskParamList);
    procedure Run;

    function  GetParams: IisListOfValues;
    function  GetDBName: String;
    function  GetDBSizeMB: Cardinal;
    function  GetDomain: String;
    function  GetADRContext: Boolean;
    function  GetDBLocation: String;
    function  GetDBHost: String;
    function  GetState: TevQueueItemState;
    procedure SetState(const AValue: TevQueueItemState);
    function  GetStartedAt: TDateTime;
    function  GetFinishedAt: TDateTime;
    function  GetStatus: String;
    procedure SetStatus(const AValue: String);
    function  GetDetails: String;
    function  GetProgress: Integer;

  protected
    procedure AfterConstruction; override;
    procedure Execute(); virtual; abstract;
  public
    constructor Create(const ADomainInfo: IevDomainInfo; const ADBName: String; const ADBSizeMB: Cardinal; const AADRContext: Boolean); reintroduce;
  end;


  TevQueueThread = class(TReactiveThread)
  private
    FQueue: TevDBMaintQueue;
    FStatusByHosts: IisListOfValues;
  protected
    procedure DoOnStart; override;
    procedure DoWork; override;
    procedure DoOnStop; override;
  public
    constructor Create(const AOwner: TevDBMaintQueue); reintroduce;
  end;


  TevDBImportItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;

  TevDBSweepItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;

  TevDBPatchItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;

  TevDBBackupItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;

  TevDBRestoreItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;

  TevDBCustomScriptItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;

  TevDBArchiveItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  end;


  TevCleanUpTempFilesItem = class(TevDBItem)
  protected
    procedure Execute(); override;
  public
    constructor Create(const ADomainInfo: IevDomainInfo); reintroduce;
  end;



function CreateDBPatchQueue: IevDBMaintQueue;
begin
  Result := TevDBMaintQueue.Create;
end;


{ TevDBMaintQueue }

procedure TevDBMaintQueue.AddDBBackupItem(const ADatabase: String; const ASizeMB: Integer;
  const AFolder: String; const ACompression, AScramble: Boolean);
var
  Item: IevDBItem;
begin
  Item := TevDBBackupItem.Create(ctx_DomainInfo, ADatabase, ASizeMB, False);
  Item.GetParams.AddValue('DestFolder', AFolder);
  Item.GetParams.AddValue('Compression', ACompression);
  Item.GetParams.AddValue('Scramble', AScramble);
  AddItem(Item);
end;

procedure TevDBMaintQueue.AddDBImportItem(const ASrcDatabase, ASrcDBPassword: String; const ASizeMB: Integer);
var
  Item: IevDBItem;
begin
  Item := TevDBImportItem.Create(ctx_DomainInfo, ChangeFileExt(ExtractFileNameUD(ASrcDatabase), ''), ASizeMB, False);
  Item.GetParams.AddValue('SourceDB', ASrcDatabase);
  Item.GetParams.AddValue('SourceDBPassword', ASrcDBPassword);
  AddItem(Item);
end;

procedure TevDBMaintQueue.AddDBSweepItem(const ADatabase: String; const ASizeMB: Integer);
var
  Item: IevDBItem;
begin
  Item := TevDBSweepItem.Create(ctx_DomainInfo, ADatabase, ASizeMB, False);
  AddItem(Item);
end;

procedure TevDBMaintQueue.AddItem(const AItem: IevDBItem);
var
  i, j: Integer;
  Item: IevDBItem;
begin
  FQueuedList.Lock;
  try
    j := -1;
    for i := 0 to FQueuedList.ChildCount - 1 do
    begin
      Item := FQueuedList.Children[i] as IevDBItem;
      if (Item.GetState = qisQueued) and (Item.GetDBSizeMB < AItem.GetDBSizeMB) then
      begin
        j := i;
        break;
      end;
    end;

    TevDBItem((AItem as IisInterfacedObject).GetImplementation).FQueue := Self;
    if j = -1 then
      FQueuedList.AddChild(AItem)
    else
      FQueuedList.InsertChild(j, AItem);

    Inc(FStatus.Queued);

    Inc(FAllItemsWeight, AItem.GetDBSizeMB);
  finally
    FQueuedList.Unlock;
  end;

  Notify(AItem, qceAdd);

  if GetActive then
    FQueueThread.Wakeup;
end;

procedure TevDBMaintQueue.AfterConstruction;
begin
  FQueuedList := TisCollection.Create(nil, True);
  FRunningList := TisCollection.Create(nil, True);

  FTM := TThreadManager.Create('DBPatchQueue');
  FTM.Capacity := 500;

  FThreadsPerServer := TisListOfValues.Create(nil, True);
end;

procedure TevDBMaintQueue.BeforeDestruction;
begin
  SetActive(False);
  inherited;
end;

procedure TevDBMaintQueue.Clear;
begin
  FQueuedList.RemoveAllChildren;
  FAllItemsWeight := 0;
  FFinishedItemsWeight := 0;
  FEstimatedSpeed := 0;
  FStatus.StartTime := 0;
  FStatus.Executing := 0;
  FStatus.Finished := 0;
  FStatus.Queued := 0;
  FStatus.Errored := 0;
  FStatus.Skipped := 0;  
  FStatus.TotalProgress := 0;
  FStatus.StartTime := 0;
  FStatus.ElapsedTimeSec := 0;
  FStatus.EstimatedTimeLeftSec := 0;
end;

function TevDBMaintQueue.GetActive: Boolean;
begin
  Result := Assigned(FQueueThread) and FQueueThread.IsActive;
end;

function TevDBMaintQueue.GetOnQueueChanged: TOnQueueChangedEvent;
begin
  Result := FOnQueueChanged;
end;

function TevDBMaintQueue.GetThreadsPerServer: IisListOfValues;
begin
  Result := FThreadsPerServer.GetClone;
end;

procedure TevDBMaintQueue.Notify(const AItem: IevDBItem; const AOperation: TevQCEOperation);
begin
  if Assigned(FOnQueueChanged) then
    FOnQueueChanged(AItem, AOperation);
end;

procedure TevDBMaintQueue.PopulateWithDBSweepItems;

  procedure RebuildForDomain(const ADomainInfo: IevDomainInfo);
  var
    DBList: IisStringList;
    Item: IevDBItem;
    i: Integer;
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sDBAUserName, ADomainInfo.DomainName), ADomainInfo.SystemAccountInfo.Password);
      ctx_DBAccess.DisableSecurity;

      ctx_DBAccess.TidyUpDBFolders;

      DBList := TisStringList.Create;
      DBList.CommaText := ctx_DBAccess.GetClientsList;
      for i := 0 to DBList.Count - 1 do
        DBList[i] := DB_Cl + DBList[i];

      DBList.Insert(0, DB_System);
      DBList.Insert(1, DB_S_Bureau);
//      DBList.Insert(2, DB_Cl_Base);
      DBList.Add(DB_TempTables);

      for i := 0 to DBList.Count - 1 do
      begin
        Item := TevDBSweepItem.Create(ADomainInfo, DBList[i], 0, False);
        AddItem(Item);
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

var
  i: Integer;
begin
  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
    RebuildForDomain(mb_GlobalSettings.DomainInfoList[i]);
end;

procedure TevDBMaintQueue.SetActive(const AValue: Boolean);
begin
  if GetActive <> AValue then
    if AValue then
      FQueueThread := TevQueueThread.Create(Self)
    else
      FreeAndNil(FQueueThread);
end;

procedure TevDBMaintQueue.SetOnQueueChanged(const AValue: TOnQueueChangedEvent);
begin
  FOnQueueChanged := AValue;
end;

{ This is highly inaccurate, so I comment it out for now. I may improve the algorithm later.    -Aleksey
function TevDBMaintQueue.GetStatus: TevQueueStatus;
var
  i: Integer;
  Item: IevDBItem;
  exProgress: Cardinal;
begin
  FQueuedList.Lock;
  try
    // Estimate executing items progress
    exProgress := 0;
    for i := 0 to FQueuedList.ChildCount - 1 do
    begin
      Item := FQueuedList.Children[i] as IevDBItem;

      if Item.GetState = qisQueued then
        break
      else if Item.GetState = qisExecuting then
        Inc(exProgress, Round(Item.GetDBSizeMB * Cardinal(Item.GetProgress) / 100));
    end;

    if GetActive then
      FStatus.ElapsedTimeSec := SecondsBetween(Now, FStatus.StartTime);

    Result := FStatus;
    Result.Active := GetActive;
  finally
    FQueuedList.Unlock;
  end;

  if FAllItemsWeight > 0 then
    Result.TotalProgress := Trunc(((FFinishedItemsWeight + exProgress) / FAllItemsWeight) * 100);

  FQueuedList.Lock;
  try
    Result := FStatus;
    Result.Active := GetActive;
  finally
    FQueuedList.Unlock;
  end;

  Result.TotalProgress := Trunc((Result.Queued + Result.Executing) / (Result.Queued + Result.Executing + Result.Finished + Result.Errored));

  Result.EstimatedTimeLeftSec := 0;

  if GetActive then
  begin
    if not FloatCompare(FEstimatedSpeed, coEqual, 0) then
      Result.EstimatedTimeLeftSec := Round((FAllItemsWeight - FFinishedItemsWeight) / FEstimatedSpeed);
  end;
end;
}

function TevDBMaintQueue.GetStatus: TevQueueStatus;
var
  n: Integer;
begin
  FQueuedList.Lock;
  try
    if GetActive then
      FStatus.ElapsedTimeSec := SecondsBetween(Now, FStatus.StartTime);

    Result := FStatus;
    Result.Active := GetActive;
  finally
    FQueuedList.Unlock;
  end;

  n := (Result.Queued + Result.Executing + Result.Finished + Result.Errored);
  if n > 0 then
    Result.TotalProgress := Trunc((Result.Finished + Result.Errored) / n * 100)
  else
     Result.TotalProgress := 0; 

  Result.EstimatedTimeLeftSec := 0;
end;

procedure TevDBMaintQueue.PopulateWithDBPatchItems(const AAutoSweep: Boolean; const AAppVer: String; AAllowDowngrade: Boolean);

  procedure RebuildForDomain(const ADomainInfo: IevDomainInfo);
  var
    DBList: IevDataSet;
    Item: IevDBItem;
    s: String;
  begin
    ctx_DBAccess.DisableSecurity;
    ctx_DBAccess.TidyUpDBFolders;

    DBList := ctx_DBAccess.GetDBFilesList;
    DBList.First;
    while not DBList.Eof do
    begin
      s := ExtractFileNameUD(DBList['file_name']);
      if not StartsWith(s, DB_Cl_Base) and (IsDebugMode or not StartsWith(s, DB_System)) then
      begin
        Item := TevDBPatchItem.Create(ADomainInfo, s, DBList['file_size'] div (1024 * 1024), ctx_DBAccess.GetADRContext);
        Item.GetParams.AddValue('Sweep', AAutoSweep);
        Item.GetParams.AddValue('AppVersion', AAppVer);
        Item.GetParams.AddValue('AllowDowngrade', AAllowDowngrade);
        AddItem(Item);
      end;
      DBList.Next;
    end;

    Item := TevDBPatchItem.Create(ADomainInfo, DB_Cl_Base + '.gdb', 25 * 1024 * 1024, ctx_DBAccess.GetADRContext);
    Item.GetParams.AddValue('Sweep', AAppVer <> '');
    Item.GetParams.AddValue('AppVersion', AAppVer);
    Item.GetParams.AddValue('AllowDowngrade', AAllowDowngrade);
    AddItem(Item);
  end;

var
  i: Integer;
begin
  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
    try
      Mainboard.ContextManager.StoreThreadContext;
      try
        Mainboard.ContextManager.CreateThreadContext(
          EncodeUserAtDomain(sDBAUserName, mb_GlobalSettings.DomainInfoList[i].DomainName),
          mb_GlobalSettings.DomainInfoList[i].SystemAccountInfo.Password);

        ctx_DBAccess.SetADRContext(False);
        RebuildForDomain(mb_GlobalSettings.DomainInfoList[i]);

        if mb_GlobalSettings.DomainInfoList[i].ADRDBLocationList.Count > 0 then
        begin
          ctx_DBAccess.SetADRContext(True);
          RebuildForDomain(mb_GlobalSettings.DomainInfoList[i]);
        end;
      finally
        Mainboard.ContextManager.RestoreThreadContext;
      end;
    except
      on E: Exception do
      begin
        if  mb_GlobalSettings.DomainInfoList.Count > 0 then
          mb_LogFile.AddEvent(etError, 'Internal', mb_GlobalSettings.DomainInfoList[i].DomainName + ': Cannot finish operation', E.Message)
        else
          raise;
      end;
    end
end;

procedure TevDBMaintQueue.PopulateWithTidyUpDBFoldersItems;
var
  i: Integer;
  Item: IevDBItem;
begin
  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
  begin
    Item := TevCleanUpTempFilesItem.Create(mb_GlobalSettings.DomainInfoList[i]);
    AddItem(Item);
  end;
end;

procedure TevDBMaintQueue.MoveItemToTop(const AIndex: Integer);
var
  Item: IevDBItem;
begin
  FQueuedList.Lock;
  try
    Item := FQueuedList.Children[AIndex] as IevDBItem;
    FQueuedList.MoveChild(AIndex, 0);
  finally
    FQueuedList.Unlock;
  end;

  Notify(Item, qceMoveToTop);
end;

procedure TevDBMaintQueue.MoveItemToBottom(const AIndex: Integer);
var
  Item: IevDBItem;
begin
  FQueuedList.Lock;
  try
    Item := FQueuedList.Children[AIndex] as IevDBItem;
    FQueuedList.MoveChild(AIndex, FQueuedList.ChildCount - 1);
  finally
    FQueuedList.Unlock;
  end;

  Notify(Item, qceMoveToBottom);
end;

function TevDBMaintQueue.GetItems(const AState: TevQueueItemStates): IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  FQueuedList.Lock;
  try
    for i := 0 to FQueuedList.ChildCount - 1 do
    begin
      if (FQueuedList.GetChild(i) as IevDBItem).GetState in AState then
        Result.Add(FQueuedList.GetChild(i));
    end;
  finally
    FQueuedList.Unlock;
  end
end;

procedure TevDBMaintQueue.AddDBRestoreItem(const ADatabase: String;
  const ASizeMB: Integer; const ABackupFile: String);
var
  Item: IevDBItem;
begin
  Item := TevDBRestoreItem.Create(ctx_DomainInfo, ADatabase, ASizeMB, False);
  Item.GetParams.AddValue('BackupFile', ABackupFile);
  AddItem(Item);
end;

procedure TevDBMaintQueue.AddDBCustomScriptItem(const ADatabase: String;
  const ASizeMB: Integer; const AScript: String; const ASweep: Boolean);
var
  Item: IevDBItem;
begin
  Item := TevDBCustomScriptItem.Create(ctx_DomainInfo, ADatabase, ASizeMB, False);
  Item.GetParams.AddValue('Script', AScript);
  Item.GetParams.AddValue('Sweep', ASweep);
  AddItem(Item);
end;

procedure TevDBMaintQueue.UpdateThreadsPerServer(const AHost: String; const AMaxThreads: Integer);
begin
  FThreadsPerServer.Lock;
  try
    FThreadsPerServer.AddValue(AHost, AMaxThreads);
  finally
    FThreadsPerServer.Unlock;
  end;

  if GetActive then
    FQueueThread.Wakeup;
end;


{ TevDBItem }

procedure TevDBItem.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
begin
end;

constructor TevDBItem.Create(const ADomainInfo: IevDomainInfo; const ADBName: String; const ADBSizeMB: Cardinal; const AADRContext: Boolean);
var
  s: String;
begin
  inherited Create();
  FDomainInfo := ADomainInfo;
  FADRContext := AADRContext;

  if FADRContext then
    FDBLocation := ADomainInfo.ADRDBLocationList.GetDBPath(ADBName)
  else
    FDBLocation := ADomainInfo.DBLocationList.GetDBPath(ADBName);

  s := FDBLocation;
  FDBHost := GetNextStrValue(s, ':');

  s := FDomainInfo.DomainName + ': ' + ADBName;
  if FADRContext then
    s := 'ADR:' + s;

  SetName(s);
  FDBName := ADBName;

  if ADBSizeMB = 0 then
    FDBSizeMB := 1 // 0 value would cause division by zero in total progress calculation
  else
    FDBSizeMB := ADBSizeMB;

  FStatus := 'Queued';
end;

procedure TevDBItem.EndWait;
begin
  InterlockedExchange(FProgress, 100);
  SetStatus('');
end;

function TevDBItem.GetDBLocation: String;
begin
  Result := FDBLocation;
end;

function TevDBItem.GetDBName: String;
begin
  Result := FDBName; 
end;

function TevDBItem.GetDetails: String;
begin
  Result := FDetails;
end;

function TevDBItem.GetDomain: String;
begin
  Result := FDomainInfo.DomainName;
end;

function TevDBItem.GetStatus: String;
begin
  Result := FStatus;
end;

function TevDBItem.GetState: TevQueueItemState;
begin
  Result := FState;
end;

function TevDBItem.Queue: TevDBMaintQueue;
begin
  Result := FQueue;
end;

procedure TevDBItem.Run;
begin
  FStartedAt := Now;
  SetState(qisExecuting);
  Queue.FTM.RunTask(RunInThread, Self, MakeTaskParams([]), 'Processing ' + GetName);
end;

procedure TevDBItem.RunInThread(const AParams: TTaskParamList);
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sDBAUserName, FDomainInfo.DomainName), FDomainInfo.SystemAccountInfo.Password);
      ctx_DBAccess.SetADRContext(FADRContext);
      ctx_DBAccess.DisableSecurity;
      Context.SetCallbacks(Self);

      ctx_StartWait('Preparing for execution');
      try
        Execute;
      finally
        ctx_EndWait;      
      end
    except
      on E: Exception do
      begin
        FStatus := 'Finished with errors';
        if E is EisException then
          FDetails := EisException(E).GetFullMessage
        else
          FDetails := E.Message + #13#13 + GetStack();
      end;
    end;

  finally
    Mainboard.ContextManager.RestoreThreadContext;
    SetState(qisFinished);
    FFinishedAt := Now;
    if Queue.GetActive then
      Queue.FQueueThread.Wakeup;
  end;
end;

procedure TevDBItem.SetStatus(const AValue: String);
begin
  if FStatus <> AValue then
  begin
    FStatus := AValue;
    Queue.Notify(Self, qceUpdate);
  end;
end;

procedure TevDBItem.SetState(const AValue: TevQueueItemState);
begin
  if FState <> AValue then
  begin
    FState := AValue;
    Queue.Notify(Self, qceUpdate);
  end;
end;

procedure TevDBItem.StartWait(const AText: string; AMaxProgress: Integer);
begin
  InterlockedExchange(FProgress, 0);
  SetStatus(AText);
end;

procedure TevDBItem.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  if AMaxProgress > 0 then
    InterlockedExchange(FProgress, Trunc(ACurrentProgress / AMaxProgress * 100));

  SetStatus(AText);
end;

procedure TevDBItem.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
end;

function TevDBItem.GetDBHost: String;
begin
  Result := FDBHost;
end;

function TevDBItem.GetFinishedAt: TDateTime;
begin
  Result := FFinishedAt;
end;

function TevDBItem.GetStartedAt: TDateTime;
begin
  Result := FStartedAt;
end;

procedure TevDBItem.AfterConstruction;
begin
  inherited;
  FParams := TisListOfValues.Create;
end;

function TevDBItem.GetParams: IisListOfValues;
begin
  Result := FParams;
end;

function TevDBItem.GetDBSizeMB: Cardinal;
begin
  Result := FDBSizeMB;
end;

function TevDBItem.GetProgress: Integer;
begin
  Result := InterlockedExchange(FProgress, FProgress);
end;

function TevDBItem.GetADRContext: Boolean;
begin
  Result := FADRContext;
end;

{ TevQueueThread }

constructor TevQueueThread.Create(const AOwner: TevDBMaintQueue);
begin
  FQueue := AOwner;
  inherited Create('DB Queue pulse');
end;

procedure TevQueueThread.DoOnStart;
var
  i: Integer;
  MinWeightItem, Item: IevDBItem;
  T: IisListOfValues;
begin
  inherited;

  FStatusByHosts :=  TisListOfValues.Create;

  FQueue.FQueuedList.Lock;
  try
    for i := 0 to FQueue.FQueuedList.ChildCount - 1 do
    begin
      Item := FQueue.FQueuedList.Children[i] as IevDBItem;
      FStatusByHosts.AddValue(Item.GetDBHost, 0);

      if not Assigned(MinWeightItem) or (MinWeightItem.GetDBSizeMB > Item.GetDBSizeMB) then
        MinWeightItem := Item;
    end;

    T := FQueue.GetThreadsPerServer;
    for i := 0 to FStatusByHosts.Count - 1 do
    begin
      if T.FindValue(FStatusByHosts[i].Name) = nil then
        FQueue.UpdateThreadsPerServer(FStatusByHosts[i].Name, 8);
    end;

    // Move a light weight item to the top to get calculation of estimated time faster
    if Assigned(MinWeightItem) then
    begin
      i := FQueue.FQueuedList.IndexOf(MinWeightItem);
      FQueue.MoveItemToTop(i);
    end;

    FQueue.FStatus.StartTime := Now;
    FQueue.FStatus.EstimatedTimeLeftSec := 0;
  finally
    FQueue.FQueuedList.Unlock;
  end;

  Wakeup;
end;

procedure TevQueueThread.DoOnStop;
begin
  inherited;
  FQueue.FTM.TerminateTask;
  FQueue.FTM.WaitForTaskEnd;
  mb_LogFile.AddEvent(etInformation, 'Queue', 'All items have been processed for ' + PeriodToString(FQueue.GetStatus.ElapsedTimeSec * 1000), '');
end;

procedure TevQueueThread.DoWork;
var
  i, maxTotalThreads: Integer;
  Item: IevDBItem;
  totalThreads: Integer;
  v: IisNamedValue;
  LatestFinishedTime: TDateTime;
  ThreadsPerServer: IisListOfValues;
begin
  ThreadsPerServer := TisListOfValues.Create;
  ThreadsPerServer.Assign(FQueue.GetThreadsPerServer); // Cache this value as it may change before completion this proc

  maxTotalThreads := 0;
  for i := 0 to ThreadsPerServer.Count - 1 do
    maxTotalThreads := maxTotalThreads + ThreadsPerServer[i].Value;

  totalThreads := 0;
  for i := 0 to FStatusByHosts.Count - 1 do
    Inc(totalThreads, Integer(FStatusByHosts.Values[i].Value));

  FQueue.FQueuedList.Lock;
  try
    // Check finished items
    i := 0;
    LatestFinishedTime := 0;
    while i <= FQueue.FQueuedList.ChildCount - 1 do
    begin
      AbortIfCurrentThreadTaskTerminated;

      Item := FQueue.FQueuedList.Children[i] as IevDBItem;

      if Item.GetState = qisQueued then
        break;

      if Item.GetState = qisFinished then
      begin
        Dec(FQueue.FStatus.Executing);

        v := FStatusByHosts.FindValue(Item.GetDBHost);
        v.Value := v.Value - 1;
        Dec(totalThreads);

        if Item.GetStatus = '' then
        begin
          Inc(FQueue.FFinishedItemsWeight, Item.GetDBSizeMB);

          if AnsiSameText(Item.GetDetails, 'Skipped') then
            Inc(FQueue.FStatus.Skipped)
          else
            Inc(FQueue.FStatus.Finished);
            
          FQueue.FQueuedList.Delete(i);
          FQueue.Notify(Item, qceDelete);

          if Item.GetFinishedAt > LatestFinishedTime then
            LatestFinishedTime := Item.GetFinishedAt;
        end
        else
        begin
          Dec(FQueue.FAllItemsWeight, Item.GetDBSizeMB);

          TevDBItem(Item.GetImplementation).FState := qisFinishedWithErrors;
          Inc(FQueue.FStatus.Errored);
          if i < FQueue.FQueuedList.ChildCount - 1 then
            FQueue.MoveItemToBottom(i)
          else
            Inc(i);
        end;
      end
      else
        Inc(i);
    end;


    if not DateTimeCompare(LatestFinishedTime, coEqual, 0) then
      FQueue.FEstimatedSpeed := FQueue.FFinishedItemsWeight / (MilliSecondsBetween(LatestFinishedTime, FQueue.FStatus.StartTime) / 1000);


    // Check queued items
    while (totalThreads < maxTotalThreads) and (i <= FQueue.FQueuedList.ChildCount - 1) do
    begin
      AbortIfCurrentThreadTaskTerminated;

      Item := FQueue.FQueuedList.Children[i] as IevDBItem;

      if Item.GetState <> qisQueued then
        break;

      v := FStatusByHosts.FindValue(Item.GetDBHost);
      if v.Value < ThreadsPerServer.Value[Item.GetDBHost] then
      begin
        FQueue.MoveItemToTop(i);
        TevDBItem(Item.GetImplementation).Run;
        Dec(FQueue.FStatus.Queued);
        Inc(FQueue.FStatus.Executing);

        v.Value := v.Value + 1;
        Inc(totalThreads);
      end;

      Inc(i);
    end;

  finally
    FQueue.FQueuedList.Unlock;
  end;

  if totalThreads = 0 then
    AbortEx;
end;

{ TevDBPatchItem }

procedure TevDBPatchItem.Execute;
var
  appVer: String;
  bRes: Boolean;
begin
  ctx_UpdateWait('Checking database');

  appVer := FParams.TryGetValue('AppVersion', '');
  if appVer = '' then
    bRes := ctx_DBAccess.SyncDBAndApp(FDBName)
  else
    bRes := ctx_DBAccess.PatchDB(FDBName, appVer, FParams.TryGetValue('AllowDowngrade', False));

  if bRes then
  begin
    mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been patched.', 'DB Path:  ' + FDBLocation);
    if FParams.Value['Sweep'] then
    begin
      ctx_UpdateWait('Sweeping database');
      ctx_DBAccess.SweepDB(FDBName);
      mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been swept.', 'DB Path:  ' + FDBLocation);
    end;
  end
  else
  begin
    FDetails := 'Skipped';
    mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' is up to date. No patching is required.', 'DB Path:  ' + FDBLocation);
  end;
end;

{ TevDBCustomScriptItem }

procedure TevDBCustomScriptItem.Execute;
begin
  ctx_UpdateWait('Custom Script Database');

  ctx_DBAccess.CustomScript(FDBName, FParams.Value['Script']);
  mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been scripted.', 'DB Path:  ' + FDBLocation);
  
  if FParams.Value['Sweep'] then
  begin
    ctx_UpdateWait('Sweeping database');
    ctx_DBAccess.SweepDB(FDBName);
    mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been swept.', 'DB Path:  ' + FDBLocation);
  end;
End;


{ TevDBImportItem }

procedure TevDBImportItem.Execute;
begin
  ctx_UpdateWait('Importing database');
  ctx_DBAccess.ImportDatabase(GetParams.Value['SourceDB'], GetParams.Value['SourceDBPassword']);
  mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been imported.', 'Source DB:  ' + GetParams.Value['SourceDB'] + #13 + 'Target DB:  ' + FDBLocation);
end;

{ TevDBSweepItem }

procedure TevDBSweepItem.Execute;
begin
  ctx_UpdateWait('Sweeping database');
  ctx_DBAccess.SweepDB(FDBName);
  mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been swept.', 'DB Path:  ' + FDBLocation);
end;

{ TevDBBackupItem }

procedure TevDBBackupItem.Execute;
var
  S: IisStream;
  fn, ext: String;
begin
  ctx_UpdateWait('Backing up database');

  S := ctx_DBAccess.BackupDB(FDBName, False, FParams.Value['Scramble'], FParams.Value['Compression']);
  ext := '.gbk';
  if FParams.Value['Compression'] then
    ext := ext + '.bz2';

  if FParams.Value['Scramble'] and StartsWith(FDBName, DB_CL) and not StartsWith(FDBName, DB_Cl_Base) then
  begin
    fn := FDBName;
    GetNextStrValue(fn, '_');
    fn := GetNextStrValue(fn, '.');
    fn := 'CL_' + IntToStr(600000000 + StrToInt(fn)) + ext;
  end
  else
    fn := ChangeFileExt(FDBName, ext);

  fn := NormalizePath(FParams.Value['DestFolder']) + fn;

  ctx_UpdateWait('Saving to result file');
  S.SaveToFile(fn);

  mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been backed up.', 'DB Path:  ' + FDBLocation + #13 + 'Backup file:  ' + fn);
end;

{ TevCleanUpTempFilesItem }

constructor TevCleanUpTempFilesItem.Create(const ADomainInfo: IevDomainInfo);
begin
  inherited Create(ADomainInfo, DB_Cl_Base, 0, False);
end;

procedure TevCleanUpTempFilesItem.Execute;
begin
  ctx_UpdateWait('Cleaning up database folders');
  ctx_DBAccess.TidyUpDBFolders;
end;

{ TevDBArchiveItem }

procedure TevDBArchiveItem.Execute;
begin
  ctx_UpdateWait('Archiving database');
//  ctx_DBAccess.ArchiveDB(FDBName);
end;

{ TevDBRestoreItem }

procedure TevDBRestoreItem.Execute;
var
  S: IisStream;
  Compressed: Boolean;
  ext: String;
begin
  ctx_UpdateWait('Restore database');

  ext := ExtractFileExt(FParams.Value['BackupFile']);
  Compressed := AnsiSameText(ext, '.bz2') or AnsiSameText(ext, '.zip');
  S := TisStream.CreateFromFile(FParams.Value['BackupFile']);

  ctx_DBAccess.RestoreDB(FDBName, S, Compressed);

  mb_LogFile.AddContextEvent(etInformation, 'Queue', FDBName + ' has been restored.', 'Backup file:  ' + FParams.Value['BackupFile'] + #13 + 'DB Path:  ' + FDBLocation);
end;

end.
